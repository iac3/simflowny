#include "boost/shared_ptr.hpp"
#include "SAMRAI/tbox/Utilities.h"
#include <string>
#include <math.h>
#include <vector>
#include "hdf5.h"
#include "SAMRAI/pdat/CellData.h"
#include "SAMRAI/pdat/CellGeometry.h"
#include "Particles.h"
#include "SAMRAI/hier/Index.h"
#include "SAMRAI/pdat/IndexData.h"


#define MIN(X,Y) ((X) < (Y) ? (X) : (Y))
#define MAX(X,Y) ((X) > (Y) ? (X) : (Y))
#define SIGN(X) (((X) > 0) - ((X) < 0))
#define greaterEq(a,b) ((fabs((a) - (b))/1.0E-15 > 10 ? false : (floor(fabs((a) - (b))/1.0E-15) < 1)) || (b)<(a))
#define lessEq(a,b) ((fabs((a) - (b))/1.0E-15 > 10 ? false: (floor(fabs((a) - (b))/1.0E-15) < 1)) || (a)<(b))
#define equalsEq(a,b) ((fabs((a) - (b))/1.0E-15 > 10 ? false: (floor(fabs((a) - (b))/1.0E-15) < 1)))
#define reducePrecision(x, p) (floor(((x) * pow(10, (p)) + 0.5)) / pow(10, (p)))




inline void calculateCoefficientsMapFile(double* coefs, double coord, double* position) {

	for (int i = 0; i < 5; i++) {
		coefs[i] = 0;
	}
	//Search for a perfect fit

	for (int i = 0; i < 5; i++) {
		if (position[i] == coord) {
			coefs[i] = 1;
			return;
		}
	}

	double x1 = position[0];
	double x2 = position[1];
	double x3 = position[2];
	double x4 = position[3];
	double x5 = position[4];

  	coefs[0] = (coord-x2)*(coord-x3)*(coord-x4)*(coord-x5) / ((x1-x2)*(x1-x3)*(x1-x4)*(x1-x5));
	coefs[1] = (coord-x1)*(coord-x3)*(coord-x4)*(coord-x5) / ((x2-x1)*(x2-x3)*(x2-x4)*(x2-x5));
	coefs[2] = (coord-x1)*(coord-x2)*(coord-x4)*(coord-x5) / ((x3-x1)*(x3-x2)*(x3-x4)*(x3-x5));
	coefs[3] = (coord-x1)*(coord-x2)*(coord-x3)*(coord-x5) / ((x4-x1)*(x4-x2)*(x4-x3)*(x4-x5));
	coefs[4] = (coord-x1)*(coord-x2)*(coord-x3)*(coord-x4) / ((x5-x1)*(x5-x2)*(x5-x3)*(x5-x4));
}

inline double readFromFile1D(hsize_t* size, double* position1, double* data, double coord1) {

	int xSize = size[0];

	double dx1   = position1[1] - position1[0];
	int ii  = int((coord1 - position1[0])/dx1);

	if (ii >= xSize) {
		TBOX_ERROR("Mapping 1D File"<< ": the input file does not have a value for coordinate "<<coord1);
	}

	int ib = std::min(std::max(ii-2,0),xSize-4);

	double coefs1[5];
	calculateCoefficientsMapFile(coefs1, coord1, &position1[ib]);

	double value = 0;
             for (int i = 0; i < 5; i++) {
             	value = value + coefs1[i] * data[ib + i];
             }
             return value;
}

inline double readFromFile2D(hsize_t* size, double* position1, double* position2, double** data, double coord1, double coord2) {

	int xSize = size[0];
	int ySize = size[1];

	double ddx = position1[1] - position1[0];
	double ddy = position2[1] - position2[0];

             int ii = int((coord1 - position1[0])/ddx);
             int jj = int((coord2 - position2[0])/ddy);


	if (ii >= xSize) {
		TBOX_ERROR("Mapping 2D File"<< ": the input file does not have a value for coordinate "<<coord1);
	}
	if (jj >= ySize) {
		TBOX_ERROR("Mapping 2D File"<< ": the input file does not have a value for coordinate "<<coord2);
	}
	
	int ib = std::min(std::max(ii-2,0),xSize-4);
             int jb = std::min(std::max(jj-2,0),ySize-4);

             double coefs1[5], coefs2[5];
	calculateCoefficientsMapFile(coefs1, coord1, &position1[ib]);
	calculateCoefficientsMapFile(coefs2, coord2, &position2[jb]);

	double value = 0;
             for (int i = 0; i < 5; i++) {
             	for (int j = 0; j < 5; j++) {
             		value = value + coefs1[i] * coefs2[j] * data[ib + i][jb + j];
             	}
             }
             return value;
}

inline double readFromFile3D(hsize_t* size, double* position1, double* position2, double* position3, double*** data, double coord1, double coord2, double coord3) {

	int xSize = size[0];
	int ySize = size[1];
	int zSize = size[2];

	double ddx = position1[1] - position1[0];
	double ddy = position2[1] - position2[0];
	double ddz = position3[1] - position3[0];

             int ii = int((coord1 - position1[0])/ddx);
             int jj = int((coord2 - position2[0])/ddy);
             int kk = int((coord3 - position3[0])/ddz);


	if (ii >= xSize) {
		TBOX_ERROR("Mapping 3D File"<< ": the input file does not have a value for coordinate "<<coord1);
	}
	if (jj >= ySize) {
		TBOX_ERROR("Mapping 3D File"<< ": the input file does not have a value for coordinate "<<coord2);
	}
	if (kk >= zSize) {
		TBOX_ERROR("Mapping 3D File"<< ": the input file does not have a value for coordinate "<<coord3);
	}

	
	int ib = std::min(std::max(ii-2,0),xSize-4);
             int jb = std::min(std::max(jj-2,0),ySize-4);
             int kb = std::min(std::max(kk-2,0),zSize-4);

             double coefs1[5], coefs2[5], coefs3[5];
	calculateCoefficientsMapFile(coefs1, coord1, &position1[ib]);
	calculateCoefficientsMapFile(coefs2, coord2, &position2[jb]);
	calculateCoefficientsMapFile(coefs3, coord3, &position3[kb]);

	double value = 0;
             for (int i = 0; i < 5; i++) {
             	for (int j = 0; j < 5; j++) {
             		for (int k = 0; k < 5; k++) {
             			value = value + coefs1[i] * coefs2[j] * coefs3[k] * data[ib + i][jb + j][kb + k];
             		}
             	}
             }
             return value;
}
