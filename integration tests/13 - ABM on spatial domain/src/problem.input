//Problem specific parameters
Problem {
	particles {
		//Print particles average in cells
		print_average = FALSE
		//Radius (in domain units)
		influence_radius = 1
		//Density
		density_par = 1
		//Particle distribution
		particle_distribution = "RANDOM"  //RANDOM, REGULAR or STAGGERED
	}
	eta = 0.18
	v0 = 0.5
	time_steps = 100
	random_seed = 0
}

Main {
	// output configuration
	output_interval = 1            // zero to turn off (In coarser level cycle units)
	timer_output_interval = 1            // zero to turn off
	// restart configuration
	start_from_restart = FALSE
	restart_interval = 0           // zero to turn off (In coarser level cycle units)
	restart_iteration = 0          // iteration to restart from (if start_from_restart = TRUE) (In coarser level cycle units)
	restart_dirname = "checkpoint.restart" //To store the checkpoints and to take the checkpoint for a restart
	// dt
	dt = 1.0E-5
	//Clustering type
	clustering_type = "BergerRigoutsos" // TileClustering or BergerRigoutsos
	//Load Balancer
	partitioner_type = "CascadePartitioner" // CascadePartitioner, TreeLoadBalancer or ChopAndPackLoadBalancer
}

FileWriter {
	full_dump {
		hdf5_dump_interval = 1         //In finer level cycle units
		hdf5_dump_dirname = "outputDir"
		variables = "speedx", "speedy", "num", "denom", "random", "neighbours", "theta"      // variables to dump
	}
}

TimeRefinementIntegrator{
}

CartesianGeometry {
	domain_boxes = [ (0, 0) , (99, 99) ]
	x_lo = 0.0, 0.0     // lower end of computational domain.
	x_up = 1.0, 1.0  // upper end of computational domain.
	periodic_dimension = 1, 1
}

StandardTagAndInitialize{
}

GriddingAlgorithm {
}

ChopAndPackLoadBalancer {
	//bin_pack_method = "SPATIAL"	//SPATIAL or GREEDY
	//ignore_level_box_union_is_single_box = TRUE
}

CascadePartitioner {
	//tile_size = 4,4,4
	//flexible_load_tolerance = 0.2
	//max_spread_procs = 500
}

TreeLoadBalancer {
	//tile_size = 4,4,4
	//flexible_load_tolerance = 0.2
	//max_cycle_spread_procs = 500
}

TileClustering {
	//tile_size = 4,4,4
}

BergerRigoutsos {
	//sort_output_nodes = TRUE
	//max_box_size = 100, 100, 100
	//combine_efficiency = 0.90
	//efficiency_tolerance = 0.90
}
TimerManager{
	timer_list = "OutputGeneration", "Regridding", "Step"
	print_threshold = 0
	print_processor = FALSE
	print_max = TRUE
}