#ifndef included_Particle
#define included_Particle

#include "SAMRAI/tbox/MessageStream.h"
#include "SAMRAI/hier/IntVector.h"
#include "SAMRAI/tbox/Database.h"
#include <vector>

/**
 * The SampleClass struct holds some dummy data and methods.  It's intent
 * is to indicate how a user could construct their own index data double.
 */
using namespace std;
using namespace SAMRAI;

class Particle
{
public:

	//Struct to order particle pointer neighbourhood in std::sort
	struct less_than
	{
		const bool operator()( Particle *a,  Particle * b) const {
			return a->getDistance() < b->getDistance();
		}
	};

	inline int GetExpoBase2(double d);

	bool	Equal(double d1, double d2);

	/**
	* The default constructor.
	*/
	Particle();

	/**
	* Constructor.
	*/
	Particle(const int region);

	/**
	* The complete constructor.
	*/
	Particle(const double fieldValue, const double* pos, const int region = 0);

	/**
	* Copy constructor.
	*/
	Particle(const Particle& data);
	Particle(const Particle& data, bool newId);

	~Particle();

	//Setters 
	void setId(const int id);
	void setFieldValue(string varName, double value);

	//Getters
	double getFieldValue(string varName);
	int getId();

	//Distance between particles
	double distance_p(Particle* data);
	double distance(Particle* data);
	double distance(double* point);


	bool same(const Particle& data);
	Particle* overlaps(const Particle& data);
	double getDistance();

	Particle& operator=(const Particle& data);

	bool operator==(const Particle& data);

	void packStream(tbox::MessageStream& stream);
	void unpackStream(tbox::MessageStream& stream, const hier::IntVector& offset);
	void getFromDatabase(std::shared_ptr<tbox::Database>& database, int particle);
	void putToDatabase(std::shared_ptr<tbox::Database>& database, int particle);

	static size_t size() {
		size_t bytes = tbox::MessageStream::getSizeof<double>(4 + 1 + 14) + tbox::MessageStream::getSizeof<int>(4);
		return(bytes);
	};

   	//Field value of the particle
	double speedx;
	double speedy;
	double num;
	double denom;
	double random;
	double neighbours;
	double theta;
	double speedx_p;
	double speedy_p;
	double num_p;
	double denom_p;
	double random_p;
	double neighbours_p;
	double theta_p;


   	//Positions of the particle (Global index position)
	double positionx_p;
	double positiony_p;
	double positionx;
	double positiony;


	//Region information
	int region;
	int interior;
	int newRegion; //The region to become when entering the domain (boundaries)

private:
	//Particle identifier
	int id;

	//Particle counter
	static int counter;

	//Temporal variable for order the particles in the neighbourhood of another particle
	double _distance;

};

#endif
