#include "boost/shared_ptr.hpp"
#include "Functions.h"
#include <string>
#include "SAMRAI/pdat/CellData.h"
#include "Problem.h"


#define MIN(X,Y) ((X) < (Y) ? (X) : (Y))
#define MAX(X,Y) ((X) > (Y) ? (X) : (Y))
#define SIGN(X) (((X) > 0) - ((X) < 0))


