#include "RefineClasses.h"
#include "TimeInterpolateOperator.h"
#include "TimeRefinementIntegrator.h"
#include "RefineSchedule.h"
#include "RefineAlgorithm.h"
#include "StandardRefineTransactionFactory.h"

#include "RefineTimeTransaction.h"

#include "SAMRAI/SAMRAI_config.h"

#include "SAMRAI/hier/Box.h"
#include "SAMRAI/geom/CartesianGridGeometry.h"
#include "SAMRAI/tbox/Database.h"
#include "SAMRAI/mesh/StandardTagAndInitStrategy.h"
#include "SAMRAI/hier/Index.h"
#include "SAMRAI/hier/Patch.h"
#include "SAMRAI/xfer/CoarsenAlgorithm.h"
#include "SAMRAI/xfer/CoarsenSchedule.h"
#include "MainRestartData.h"
#include "SAMRAI/algs/TimeRefinementLevelStrategy.h"
#include "Commons.h"
#include "SlicerDataWriter.h"
#include "SphereDataWriter.h"
#include "SAMRAI/appu/VisItDataWriter.h"
#include "IntegrateDataWriter.h"
#include "PointDataWriter.h"
#include "TimeInterpolator.h"

using namespace std;
using namespace SAMRAI;

#define DIMENSIONS 3



class Problem : 
   public mesh::StandardTagAndInitStrategy,
   public xfer::RefinePatchStrategy,
   public xfer::CoarsenPatchStrategy,
   public algs::TimeRefinementLevelStrategy
{
public:
	/*
	 * Constructor of the problem.
	 */
	Problem(
		const string& object_name,
		const tbox::Dimension& dim,
		std::shared_ptr<tbox::Database>& input_db,
		std::shared_ptr<geom::CartesianGridGeometry >& grid_geom, 
	   	std::shared_ptr<hier::PatchHierarchy >& patch_hierarchy, 
	   	MainRestartData& mrd,
		const double dt, 
		const bool init_from_files,
		const int console_output,
		const int timer_output,
		const int mesh_output_period,
		const vector<string> full_mesh_writer_variables,
		std::shared_ptr<appu::VisItDataWriter>& data_writer,
		const vector<int> slicer_output_period,
		const vector<set<string> > sliceVariables,
		vector<std::shared_ptr<SlicerDataWriter > >sliceWriters,
		const vector<int> sphere_output_period,
		const vector<set<string> > sphereVariables,
		vector<std::shared_ptr<SphereDataWriter > > sphereWriters,
		const vector<bool> slicer_analysis_output,
		const vector<bool> sphere_analysis_output,
		const vector<int> integration_output_period,
		const vector<set<string> > integralVariables,
		vector<std::shared_ptr<IntegrateDataWriter > > integrateDataWriters,
		const vector<bool> integration_analysis_output,
		const vector<int> point_output_period,
		const vector<set<string> > pointVariables,
		vector<std::shared_ptr<PointDataWriter > > pointDataWriters,
		const vector<bool> point_analysis_output);
  
	/*
	 * Destructor.
	 */
	~Problem();

	/*
	 * Block of subcycling inherited methods.
	 */
	void initializeLevelIntegrator(
	      const std::shared_ptr<mesh::GriddingAlgorithmStrategy>& gridding_alg);

	double getLevelDt(
	      const std::shared_ptr<hier::PatchLevel>& level,
	      const double dt_time,
	      const bool initial_time);

	double getMaxFinerLevelDt(
	      const int finer_level_number,
	      const double coarse_dt,
	      const hier::IntVector& ratio_to_coarser);

	double advanceLevel(
	      const std::shared_ptr<hier::PatchLevel>& level,
	      const std::shared_ptr<hier::PatchHierarchy>& hierarchy,
	      const double current_time,
	      const double new_time,
	      const bool first_step,
	      const bool last_step,
	      const bool regrid_advance = false);

	void standardLevelSynchronization(
	      const std::shared_ptr<hier::PatchHierarchy>& hierarchy,
	      const int coarsest_level,
	      const int finest_level,
	      const double sync_time,
	      const std::vector<double>& old_times);

	void synchronizeNewLevels(
	      const std::shared_ptr<hier::PatchHierarchy>& hierarchy,
	      const int coarsest_level,
	      const int finest_level,
	      const double sync_time,
	      const bool initial_time);

	void resetTimeDependentData(
	      const std::shared_ptr<hier::PatchLevel>& level,
	      const double new_time,
	      const bool can_be_refined);

	void resetDataToPreadvanceState(
	      const std::shared_ptr<hier::PatchLevel>& level);

	bool usingRefinedTimestepping() const
	{
	      return d_refinedTimeStepping;
	} 

	/*
	 * Register the current iteration in the class.
	 */
	void registerIteration(int iter) {
		simPlat_iteration = iter;
	}

	/*
	 * Initialize the data from a given level.
   	 */
	virtual void initializeLevelData(
		const std::shared_ptr<hier::PatchHierarchy >& hierarchy ,
		const int level_number ,
		const double init_data_time ,
		const bool can_be_refined ,
		const bool initial_time ,
		const std::shared_ptr<hier::PatchLevel >& old_level=std::shared_ptr<hier::PatchLevel>() ,
		const bool allocate_data = true);
	void postInit();

	/*
	 * Reset the hierarchy-dependent internal information.
	 */
	virtual void resetHierarchyConfiguration(
		const std::shared_ptr<hier::PatchHierarchy >& new_hierarchy ,
		int coarsest_level ,
		int finest_level);

	/*
	 * Checks the finalization conditions              
	 */
	bool checkFinalization(
		const double simPlat_time, 
		const double simPlat_dt);

	/*
	 * This method sets the physical boundary conditions.
	*/
	void setPhysicalBoundaryConditions(
		hier::Patch& patch,
		const double fill_time,
		const hier::IntVector& ghost_width_to_fill);
	/*
	 * Set up external plotter to plot internal data from this class.        
	 * Tell the plotter about the refinement ratios.  Register variables     
	 * appropriate for plotting.                                            
	 */
	int setupPlotterMesh(appu::VisItDataWriter &plotter ) const;
	int setupSlicePlotter(vector<std::shared_ptr<SlicerDataWriter> > &plotters) const;
	int setupSpherePlotter(vector<std::shared_ptr<SphereDataWriter> > &plotters) const;
	int setupIntegralPlotter(vector<std::shared_ptr<IntegrateDataWriter> > &plotters) const;
	int setupPointPlotter(vector<std::shared_ptr<PointDataWriter> > &plotters) const;


	/*
	 * Map data on a patch. This mapping is done only at the begining of the simulation.
	 */
	void mapDataOnPatch(const double time, const bool initial_time, const int ln, const std::shared_ptr< hier::PatchLevel >& level);

	/*
	 * Sets the limit for the checkstencil routine
	 */
	void setStencilLimits(std::shared_ptr< hier::Patch > patch, int i, int j, int k, int v) const;

	/*
	 * Checks if the point has a stencil width
	 */
	void checkStencil(std::shared_ptr< hier::Patch > patch, int i, int j, int k, int v) const;

	/*
	 * Flood-Fill algorithm
	 */
	void floodfill(std::shared_ptr< hier::Patch > patch, int i, int j, int k, int pred, int seg) const;


	/*
	 * FOV correction for AMR
	 */
	void correctFOVS(const std::shared_ptr< hier::PatchLevel >& level);



	/*
	 * Interphase mapping. Calculates the FOV and its variables.
	 */
	void interphaseMapping(const double time, const bool initial_time, const int ln, const std::shared_ptr< hier::PatchLevel >& level, const int remesh);




	/*
	 * Initialize data on a patch. This initialization is done only at the begining of the simulation.
	 */
	void initializeDataOnPatch(
		hier::Patch& patch,
		const double time,
       		const bool initial_time);

	/*
	 *  Cell tagging routine - tag cells that require refinement based on a provided condition. 
	 */
	void applyGradientDetector(
	   	const std::shared_ptr< hier::PatchHierarchy >& hierarchy, 
	   	const int level_number,
	   	const double time, 
	   	const int tag_index,
	   	const bool initial_time,
	   	const bool uses_richardson_extrapolation_too);

	/*
	* Return maximum stencil width needed for user-defined
	* data interpolation operations.  Default is to return
	* zero, assuming no user-defined operations provided.
	*/
	hier::IntVector getRefineOpStencilWidth(const tbox::Dimension &dim) const
	{
		return hier::IntVector::getZero(dim);
	}

	/*
	* Pre- and post-processing routines for implementing user-defined
	* spatial interpolation routines applied to variables.  The 
	* interpolation routines are used in the MOL AMR algorithm
	* for filling patch ghost cells before advancing data on a level
	* and after regridding a level to fill portions of the new level
	* from some coarser level.  These routines are called automatically
	* from within patch boundary filling schedules; thus, some concrete
	* function matching these signatures must be provided in the user's
	* patch model.  However, the routines only need to perform some 
	* operations when "USER_DEFINED_REFINE" is given as the interpolation 
	* method for some variable when the patch model registers variables
	* with the MOL integration algorithm, typically.  If the 
	* user does not provide operations that refine such variables in either 
	* of these routines, then they will not be refined.
	*
	* The order in which these operations are used in each patch 
	* boundary filling schedule is:
	* 
	* - \b (1) {Call user's preprocessRefine() routine.}
	* - \b (2) {Refine all variables with standard interpolation operators.}
	* - \b (3) {Call user's postprocessRefine() routine.}
	* 
	* 
	* Also, user routines that implement these functions must use 
	* data corresponding to the d_scratch context on both coarse and
	* fine patches.
	*/
	virtual void preprocessRefine(
		hier::Patch& fine,
		const hier::Patch& coarse,
                const hier::Box& fine_box,
                const hier::IntVector& ratio)
	{
		NULL_USE(fine);
		NULL_USE(coarse);
		NULL_USE(fine_box);
		NULL_USE(ratio);
	}
	virtual void postprocessRefine(
		hier::Patch& fine,
                const hier::Patch& coarse,
                const hier::Box& fine_box,
                const hier::IntVector& ratio)
	{
		NULL_USE(fine);
		NULL_USE(coarse);
		NULL_USE(fine_box);
		NULL_USE(ratio);
	}



	/*
	* Return maximum stencil width needed for user-defined
	* data coarsen operations.  Default is to return
	* zero, assuming no user-defined operations provided.
	*/
	hier::IntVector getCoarsenOpStencilWidth( const tbox::Dimension &dim ) const {
		return hier::IntVector::getZero(dim);
	}

	/*
	* Pre- and post-processing routines for implementing user-defined
	* spatial coarsening routines applied to variables.  The coarsening 
	* routines are used in the MOL AMR algorithm synchronizing 
	* coarse and fine levels when they have been integrated to the same
	* point.  These routines are called automatically from within the 
	* data synchronization coarsen schedules; thus, some concrete
	* function matching these signatures must be provided in the user's
	* patch model.  However, the routines only need to perform some
	* operations when "USER_DEFINED_COARSEN" is given as the coarsening
	* method for some variable when the patch model registers variables
	* with the MOL level integration algorithm, typically.  If the
	* user does not provide operations that coarsen such variables in either
	* of these routines, then they will not be coarsened.
	*
	* The order in which these operations are used in each coarsening
	* schedule is:
	* 
	* - \b (1) {Call user's preprocessCoarsen() routine.}
	* - \b (2) {Coarsen all variables with standard coarsening operators.}
	* - \b (3) {Call user's postprocessCoarsen() routine.}
	* 
	*
	* Also, user routines that implement these functions must use
	* corresponding to the d_new context on both coarse and fine patches
	* for time-dependent quantities.
	*/
	virtual void preprocessCoarsen(
		hier::Patch& coarse,
                const hier::Patch& fine,
                const hier::Box& coarse_box,
                const hier::IntVector& ratio)
	{
		NULL_USE(fine);
		NULL_USE(coarse);
		NULL_USE(coarse_box);
		NULL_USE(ratio);
	}
	virtual void postprocessCoarsen(
		hier::Patch& coarse,
                const hier::Patch& fine,
                const hier::Box& coarse_box,
                const hier::IntVector& ratio)
	{
		NULL_USE(fine);
		NULL_USE(coarse);
		NULL_USE(coarse_box);
		NULL_USE(ratio);
	}

	/*
	 * Computes the dt to be used
	 */
	double computeDt() const;



	/*
	 * Checks if the point has to be stalled
	 */
	bool checkStalled(std::shared_ptr< hier::Patch > patch, int i, int j, int k, int v) const;



	/*
	 * Saves restart information relevant in Problem class.
	 */
	void putToRestart(MainRestartData& mrd);

	/*
	 * Loads restart information relevant in Problem class.
	 */
	void getFromRestart(MainRestartData& mrd);

	/*
	 * Allocates internal variable memory
	 */
	void allocateAfterRestart();

private:	 
	//Variables for the refine and coarsen algorithms

	std::shared_ptr< xfer::RefineAlgorithm > d_bdry_fill_init;
	std::shared_ptr< xfer::RefineAlgorithm > d_bdry_post_coarsen;
	std::vector< std::shared_ptr< xfer::RefineSchedule > > d_bdry_sched_postCoarsen;
	std::shared_ptr<TimeInterpolator> time_interpolate_operator_mesh1;
	std::shared_ptr< xfer::RefineAlgorithm > d_bdry_fill_advance1;
	std::vector< std::shared_ptr< xfer::RefineSchedule > > d_bdry_sched_advance1;
	std::shared_ptr< xfer::RefineAlgorithm > d_bdry_fill_advance6;
	std::vector< std::shared_ptr< xfer::RefineSchedule > > d_bdry_sched_advance6;
	std::shared_ptr< xfer::RefineAlgorithm > d_bdry_fill_advance11;
	std::vector< std::shared_ptr< xfer::RefineSchedule > > d_bdry_sched_advance11;
	std::shared_ptr< xfer::RefineAlgorithm > d_bdry_fill_advance16;
	std::vector< std::shared_ptr< xfer::RefineSchedule > > d_bdry_sched_advance16;
	std::shared_ptr< xfer::RefineAlgorithm > d_bdry_fill_analysis1;
	std::vector< std::shared_ptr< xfer::RefineSchedule > > d_bdry_sched_analysis1;

	std::shared_ptr< xfer::RefineAlgorithm > d_mapping_fill;
	std::shared_ptr< xfer::RefineAlgorithm > d_tagging_fill;
	std::shared_ptr< xfer::CoarsenAlgorithm > d_coarsen_algorithm;
	std::vector< std::shared_ptr< xfer::CoarsenSchedule > > d_coarsen_schedule;

	std::shared_ptr< xfer::RefineAlgorithm > d_fill_new_level;

	//Object name
	std::string d_object_name;

	//Pointers to the grid geometry and the patch hierarchy
   	std::shared_ptr<geom::CartesianGridGeometry > d_grid_geometry;
	std::shared_ptr<hier::PatchHierarchy > d_patch_hierarchy;

	//Identifiers of the fields and auxiliary fields
	int d_gtd_xx_id, d_gtd_xy_id, d_gtd_xz_id, d_gtd_yy_id, d_gtd_yz_id, d_gtd_zz_id, d_Atd_xx_id, d_Atd_xy_id, d_Atd_xz_id, d_Atd_yy_id, d_Atd_yz_id, d_Atd_zz_id, d_Gamh_x_id, d_Gamh_y_id, d_Gamh_z_id, d_Betau_x_id, d_Betau_y_id, d_Betau_z_id, d_Bu_x_id, d_Bu_y_id, d_Bu_z_id, d_Alpha_id, d_chi_id, d_trK_id, d_theta_id, d_phiR_id, d_pheR_id, d_phiI_id, d_pheI_id, d_piR_id, d_peR_id, d_piI_id, d_peI_id, d_Rscalar_id, d_HamCon_id, d_MomCon_x_id, d_MomCon_y_id, d_MomCon_z_id, d_trA_id, d_detgtm1_id, d_M_ADM_surf_id, d_Jz_ADM_surf_id, d_N_Noetheri_id, d_N_Noethere_id, d_M_Komar_id, d_Jz_Komar_id, d_psi4R_id, d_psi4I_id, d_Z_x_id, d_Z_y_id, d_Z_z_id, d_rk1gtd_xx_id, d_rk1gtd_xy_id, d_rk1gtd_xz_id, d_rk1gtd_yy_id, d_rk1gtd_yz_id, d_rk1gtd_zz_id, d_rk1Atd_xx_id, d_rk1Atd_xy_id, d_rk1Atd_xz_id, d_rk1Atd_yy_id, d_rk1Atd_yz_id, d_rk1Atd_zz_id, d_rk1Gamh_x_id, d_rk1Gamh_y_id, d_rk1Gamh_z_id, d_rk1Betau_x_id, d_rk1Betau_y_id, d_rk1Betau_z_id, d_rk1Bu_x_id, d_rk1Bu_y_id, d_rk1Bu_z_id, d_rk1Alpha_id, d_rk1chi_id, d_rk1trK_id, d_rk1theta_id, d_rk1phiR_id, d_rk1pheR_id, d_rk1phiI_id, d_rk1pheI_id, d_rk1piR_id, d_rk1peR_id, d_rk1piI_id, d_rk1peI_id, d_rk2gtd_xx_id, d_rk2gtd_xy_id, d_rk2gtd_xz_id, d_rk2gtd_yy_id, d_rk2gtd_yz_id, d_rk2gtd_zz_id, d_rk2Atd_xx_id, d_rk2Atd_xy_id, d_rk2Atd_xz_id, d_rk2Atd_yy_id, d_rk2Atd_yz_id, d_rk2Atd_zz_id, d_rk2Gamh_x_id, d_rk2Gamh_y_id, d_rk2Gamh_z_id, d_rk2Betau_x_id, d_rk2Betau_y_id, d_rk2Betau_z_id, d_rk2Bu_x_id, d_rk2Bu_y_id, d_rk2Bu_z_id, d_rk2Alpha_id, d_rk2chi_id, d_rk2trK_id, d_rk2theta_id, d_rk2phiR_id, d_rk2pheR_id, d_rk2phiI_id, d_rk2pheI_id, d_rk2piR_id, d_rk2peR_id, d_rk2piI_id, d_rk2peI_id, d_rk3gtd_xx_id, d_rk3gtd_xy_id, d_rk3gtd_xz_id, d_rk3gtd_yy_id, d_rk3gtd_yz_id, d_rk3gtd_zz_id, d_rk3Atd_xx_id, d_rk3Atd_xy_id, d_rk3Atd_xz_id, d_rk3Atd_yy_id, d_rk3Atd_yz_id, d_rk3Atd_zz_id, d_rk3Gamh_x_id, d_rk3Gamh_y_id, d_rk3Gamh_z_id, d_rk3Betau_x_id, d_rk3Betau_y_id, d_rk3Betau_z_id, d_rk3Bu_x_id, d_rk3Bu_y_id, d_rk3Bu_z_id, d_rk3Alpha_id, d_rk3chi_id, d_rk3trK_id, d_rk3theta_id, d_rk3phiR_id, d_rk3pheR_id, d_rk3phiI_id, d_rk3pheI_id, d_rk3piR_id, d_rk3peR_id, d_rk3piI_id, d_rk3peI_id, d_d_i_gtd_xz_gtd_yy_gtd_yz_gtd_zz_Atd_xx_Atd_xy_Atd_xz_Atd_yy_Atd_yz_Atd_zz_Gamh_x_Gamh_y_Gamh_z_Betau_x_Betau_y_Betau_z_Bu_x_Bu_y_Bu_z_Alpha_chi_trK_theta_phiR_pheR_phiI_pheI_piR_peR_piI_peI_gtd_xx_gtd_xy_id, d_d_j_gtd_xz_gtd_yy_gtd_yz_gtd_zz_Atd_xx_Atd_xy_Atd_xz_Atd_yy_Atd_yz_Atd_zz_Gamh_x_Gamh_y_Gamh_z_Betau_x_Betau_y_Betau_z_Bu_x_Bu_y_Bu_z_Alpha_chi_trK_theta_phiR_pheR_phiI_pheI_piR_peR_piI_peI_gtd_xx_gtd_xy_id, d_d_k_gtd_xz_gtd_yy_gtd_yz_gtd_zz_Atd_xx_Atd_xy_Atd_xz_Atd_yy_Atd_yz_Atd_zz_Gamh_x_Gamh_y_Gamh_z_Betau_x_Betau_y_Betau_z_Bu_x_Bu_y_Bu_z_Alpha_chi_trK_theta_phiR_pheR_phiI_pheI_piR_peR_piI_peI_gtd_xx_gtd_xy_id, d_stalled_1_id, d_gtd_xx_p_id, d_gtd_xy_p_id, d_gtd_xz_p_id, d_gtd_yy_p_id, d_gtd_yz_p_id, d_gtd_zz_p_id, d_Atd_xx_p_id, d_Atd_xy_p_id, d_Atd_xz_p_id, d_Atd_yy_p_id, d_Atd_yz_p_id, d_Atd_zz_p_id, d_Gamh_x_p_id, d_Gamh_y_p_id, d_Gamh_z_p_id, d_Betau_x_p_id, d_Betau_y_p_id, d_Betau_z_p_id, d_Bu_x_p_id, d_Bu_y_p_id, d_Bu_z_p_id, d_Alpha_p_id, d_chi_p_id, d_trK_p_id, d_theta_p_id, d_phiR_p_id, d_pheR_p_id, d_phiI_p_id, d_pheI_p_id, d_piR_p_id, d_peR_p_id, d_piI_p_id, d_peI_p_id;

	//Parameter variables
	double dissipation_factor_Atd_xx;
	double dissipation_factor_Atd_xy;
	double dissipation_factor_gtd_zz;
	double dissipation_factor_Atd_xz;
	double dissipation_factor_Gamh_y;
	double dissipation_factor_Gamh_x;
	double Atd_xx_falloff;
	double dissipation_factor_Gamh_z;
	double p_kappa_z2;
	double p_kappa_z1;
	double bh1_px;
	double dissipation_factor_Bu_z;
	double bh1_py;
	double bh1_pz;
	double Bu_x_asymptotic;
	double Gamh_z_asymptotic;
	double gtd_yz_asymptotic;
	double Bu_x_falloff;
	double gtd_xz_asymptotic;
	double dissipation_factor_Bu_x;
	double dissipation_factor_Bu_y;
	double Betau_y_falloff;
	double gtd_xz_falloff;
	double lambda_f1;
	double lambda_f0;
	double lambda_f3;
	double lambda_f2;
	double pheR_falloff;
	double Atd_zz_falloff;
	double Gamh_z_falloff;
	double bh2_x;
	double piI_asymptotic;
	double bh2_z;
	double bh2_y;
	double trK_falloff;
	double chi_falloff;
	double dissipation_factor_theta;
	double gtd_yz_falloff;
	double peR_falloff;
	double gtd_xx_asymptotic;
	double piR_falloff;
	double Betau_y_asymptotic;
	double dissipation_factor_Atd_zz;
	double pheI_falloff;
	double peI_falloff;
	double dissipation_factor_Betau_z;
	double dissipation_factor_Betau_y;
	double dissipation_factor_Betau_x;
	double dissipation_factor_Alpha;
	double gtd_zz_falloff;
	double theta_asymptotic;
	double pheR_asymptotic;
	double p_feta;
	double Gamh_x_falloff;
	double dissipation_factor_peR;
	double eta_damping_exp;
	double phiR_asymptotic;
	double Gamh_y_asymptotic;
	double Betau_x_falloff;
	double Bu_y_falloff;
	double dissipation_factor_Atd_yy;
	double dissipation_factor_Atd_yz;
	double dissipation_factor_peI;
	double gtd_xy_falloff;
	double gtd_zz_asymptotic;
	double Alpha_asymptotic;
	double bh1_y;
	double bh2_py;
	double bh1_x;
	double bh2_pz;
	double dissipation_factor_pheR;
	double bh1_z;
	double Betau_x_asymptotic;
	double lambda_4;
	double Atd_yy_falloff;
	double lambda_3;
	double bh1_sx;
	double lambda_2;
	double trK0;
	double bh1_sy;
	double lambda_1;
	double bh2_px;
	double bh1_sz;
	double lambda_0;
	double phiR_falloff;
	double p_sfmass;
	double dissipation_factor_pheI;
	double Gamh_x_asymptotic;
	double R_0;
	double Atd_xy_asymptotic;
	double trK_asymptotic;
	double Atd_yy_asymptotic;
	double gtd_yy_asymptotic;
	double p_kappa_cc;
	double Betau_z_asymptotic;
	double Atd_xz_falloff;
	double theta_falloff;
	double pheI_asymptotic;
	double Bu_z_falloff;
	double gtd_xx_falloff;
	double phiI_asymptotic;
	double gtd_xy_asymptotic;
	double peR_asymptotic;
	double sfsigma;
	double dissipation_factor_chi;
	double Atd_yz_falloff;
	double dissipation_factor_gtd_xz;
	double dissipation_factor_gtd_xx;
	double dissipation_factor_gtd_xy;
	double piR_asymptotic;
	double peI_asymptotic;
	double Atd_xy_falloff;
	double dissipation_factor_trK;
	double bh1_mass;
	double tend;
	double Atd_xx_asymptotic;
	double bh2_sx;
	double bh2_sy;
	double bh2_sz;
	double bh2_mass;
	double dissipation_factor_piR;
	double piI_falloff;
	double dissipation_factor_gtd_yy;
	double Gamh_y_falloff;
	double dissipation_factor_gtd_yz;
	double Atd_zz_asymptotic;
	double dissipation_factor_piI;
	double Bu_z_asymptotic;
	double Alpha_falloff;
	double Betau_z_falloff;
	double chi_asymptotic;
	double Atd_xz_asymptotic;
	double Atd_yz_asymptotic;
	double dissipation_factor_phiR;
	double phiI_falloff;
	double chi_floor;
	double Bu_y_asymptotic;
	double gtd_yy_falloff;
	double dissipation_factor_phiI;


	//mapping fields
	int d_nonSync_regridding_tag_id, d_interior_regridding_value_id, d_interior_i_id, d_interior_j_id, d_interior_k_id, d_FOV_1_id, d_FOV_zLower_id, d_FOV_zUpper_id, d_FOV_yLower_id, d_FOV_yUpper_id, d_FOV_xLower_id, d_FOV_xUpper_id;

	//Stencils of the discretization method variable
	int d_ghost_width, d_regionMinThickness;

	//initial dt
	double initial_dt;

   	const tbox::Dimension d_dim;

	//Subcycling variable
   	bool d_refinedTimeStepping;

	//Current iteration
	int simPlat_iteration;

	//Initialization from files
	bool d_init_from_restart;

	//List of particle variables


	//Variables to dump
	vector<set<string> > d_sliceVariables;
	vector<set<string> > d_sphereVariables;

	vector<set<string> > d_integralVariables;
	int d_mask_id;
	vector<set<string> > d_pointVariables;


	//FileWriter
	vector<std::shared_ptr<SlicerDataWriter > > d_sliceWriters;
	vector<std::shared_ptr<SphereDataWriter > > d_sphereWriters;
	vector<int> d_slicer_output_period;
	vector<int> d_sphere_output_period;
	vector<int> next_slice_dump_iteration;
	vector<int> next_sphere_dump_iteration;
	vector<bool> analysis_slice_dump;
	vector<bool> analysis_sphere_dump;
	int viz_mesh_dump_interval;
	int next_mesh_dump_iteration;
	set<string> d_full_mesh_writer_variables;
	std::shared_ptr<appu::VisItDataWriter > d_visit_data_writer;
	vector<std::shared_ptr<IntegrateDataWriter > > d_integrateDataWriters;
	vector<int> d_integration_output_period;
	vector<int> next_integration_dump_iteration;
	vector<std::shared_ptr<PointDataWriter > > d_pointDataWriters;
	vector<int> d_point_output_period;
	vector<int> next_point_dump_iteration;
	vector<bool> analysis_integration_dump;
	vector<bool> analysis_point_dump;


	vector<int> current_iteration;
	vector<int> bo_substep_iteration;

	//Console output variables
	int d_output_interval;
	int next_console_output;
	int d_timer_output_interval;
	int next_timer_output;

	//regridding options
	std::shared_ptr<tbox::Database> regridding_db;
	bool d_regridding;
	std::string d_regridding_field, d_regridding_type, d_regridding_field_shadow;
	double d_regridding_threshold, d_regridding_compressionFactor, d_regridding_mOffset, d_regridding_error, d_regridding_buffer;
	int d_regridding_min_level, d_regridding_max_level;

	//Gets the coarser patch that includes the box.
	const std::shared_ptr<hier::Patch >& getCoarserPatch(
		const std::shared_ptr< hier::PatchLevel >& level,
		const hier::Box interior, 
		const hier::IntVector ratio);

	static bool Equals(double d1, double d2);
	static inline int GetExpoBase2(double d);
};


