import sys, getopt
import os
import numpy as np
import h5py
from scipy import interpolate

def writefields2D(folder, field, level):
    #Read mesh information
    f_sum = h5py.File(folder + '/summary.samrai', "r")
    nProcessors  = f_sum['/BASIC_INFO/number_processors']
    patchExtents = f_sum['/extents/patch_extents']
    patchMap     = f_sum['/extents/patch_map']
    varNames = f_sum['/BASIC_INFO/var_names']

    maximum = [0, 0]
    minimum = [999999, 999999]
    for i in range(len(patchExtents)):
        if (patchMap[i][2] == level):
            maximum[0] = max(maximum[0], patchExtents[i][1][0])
            maximum[1] = max(maximum[1], patchExtents[i][1][1])
            minimum[0] = min(minimum[0], patchExtents[i][0][0])
            minimum[1] = min(minimum[1], patchExtents[i][0][1])
    size = [(maximum[0] - minimum[0]) + 2 ,(maximum[1] - minimum[1]) + 2]
    data = np.zeros(shape=(size[0], size[1]))

    for iPatch in range(len(patchExtents)):
        if (patchMap[iPatch][2] == level):
            iProc = patchMap[iPatch][0]
            iProcStr = str(iProc).zfill(5)
            iPatchStr = str(patchMap[iPatch][3]).zfill(5)
            rangeX = (patchExtents[iPatch][0][0] - minimum[0], patchExtents[iPatch][1][0]+2 - minimum[0])
            rangeY = (patchExtents[iPatch][0][1] - minimum[1], patchExtents[iPatch][1][1]+2 - minimum[1])
            sizeX = patchExtents[iPatch][1][0] - patchExtents[iPatch][0][0] + 2;
            sizeY = patchExtents[iPatch][1][1] - patchExtents[iPatch][0][1] + 2;
            f_data = h5py.File(folder + '/processor_cluster.' + iProcStr + '.samrai', "r")
            tmp = f_data['/processor.' + iProcStr + '/level.' + str(level).zfill(5) + '/patch.' + iPatchStr + '/' + field]
            tmp = np.reshape(tmp, (sizeX,sizeY), order="F")
            data[rangeX[0]:rangeX[1],rangeY[0]:rangeY[1]] = tmp[:,:]
            f_data.close()

    data2 = data
    #Writing the variable to disk
    f = h5py.File(folder + '/' + field + '_' + str(level) + '.hdf5', "w")
    dset = f.create_dataset(field, data2.shape, dtype='float64')
    dset[:,:] = data2[:,:]
    f.close()
    f_sum.close()


def writefields3D(folder, field, level):
    #Read mesh information
    f_sum = h5py.File(folder + '/summary.samrai', "r")
    nProcessors  = f_sum['/BASIC_INFO/number_processors']
    patchExtents = f_sum['/extents/patch_extents']
    patchMap     = f_sum['/extents/patch_map']
    varNames = f_sum['/BASIC_INFO/var_names']
    print("XLO", f_sum['/BASIC_INFO/XLO'][...])
    print("DX", f_sum['/BASIC_INFO/dx'][...])

    maximum = [0, 0, 0]
    minimum = [999999, 999999, 999999]
    min_domain = [999999, 999999, 999999]
    for i in range(len(patchExtents)):
        if (patchMap[i][2] == level):
            maximum[0] = max(maximum[0], patchExtents[i][1][0])
            maximum[1] = max(maximum[1], patchExtents[i][1][1])
            maximum[2] = max(maximum[2], patchExtents[i][1][2])
            minimum[0] = min(minimum[0], patchExtents[i][0][0])
            minimum[1] = min(minimum[1], patchExtents[i][0][1])
            minimum[2] = min(minimum[2], patchExtents[i][0][2])
            min_domain[0] = min(min_domain[0], patchExtents[i][2][0])
            min_domain[1] = min(min_domain[1], patchExtents[i][2][1])
            min_domain[2] = min(min_domain[2], patchExtents[i][2][2])
    size = [(maximum[0] - minimum[0]) + 2 ,(maximum[1] - minimum[1]) + 2, (maximum[2] - minimum[2]) + 2]
    print("Patch minimum domain", min_domain)
    data = np.zeros(shape=(size[0], size[1], size[2]))

    for iPatch in range(len(patchExtents)):
        if (patchMap[iPatch][2] == level):
            iProc = patchMap[iPatch][0]
            iProcStr = str(iProc).zfill(5)
            iPatchStr = str(patchMap[iPatch][3]).zfill(5)
            rangeX = (patchExtents[iPatch][0][0] - minimum[0], patchExtents[iPatch][1][0]+2 - minimum[0])
            rangeY = (patchExtents[iPatch][0][1] - minimum[1], patchExtents[iPatch][1][1]+2 - minimum[1])
            rangeZ = (patchExtents[iPatch][0][2] - minimum[2], patchExtents[iPatch][1][2]+2 - minimum[2])
            sizeX = patchExtents[iPatch][1][0] - patchExtents[iPatch][0][0] + 2;
            sizeY = patchExtents[iPatch][1][1] - patchExtents[iPatch][0][1] + 2;
            sizeZ = patchExtents[iPatch][1][2] - patchExtents[iPatch][0][2] + 2;
            f_data = h5py.File(folder + '/processor_cluster.' + iProcStr + '.samrai', "r")
            tmp = f_data['/processor.' + iProcStr + '/level.' + str(level).zfill(5) + '/patch.' + iPatchStr + '/' + field]
            tmp = np.reshape(tmp, (sizeX,sizeY,sizeZ), order="F")
            data[rangeX[0]:rangeX[1],rangeY[0]:rangeY[1],rangeZ[0]:rangeZ[1]] = tmp[:,:]
            f_data.close()

    data2 = data
    #Writing the variable to disk
    f = h5py.File(folder + '/' + field + '_' + str(level) + '.hdf5', "w")
    dset = f.create_dataset(field, data2.shape, dtype='float64')
    dset[:,:,:] = data2[:,:,:]
    f.close()
    f_sum.close()


def main():


    for t in range(0, 400+1, 40):
        writefields2D("./src_half/outputDir_subcycling/visit_dump." + str(t).zfill(5), "K", 0)
        writefields2D("./src_full/outputDir_subcycling/visit_dump." + str(t).zfill(5), "K", 0)
        writefields2D("./src_full/outputDir_subcycling/visit_dump." + str(t).zfill(5), "auxField", 0)

    


if __name__ == "__main__":
    main()
