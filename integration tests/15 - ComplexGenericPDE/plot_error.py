#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as pl
import math
from pylab import *
mpl.rcParams['text.usetex']=True
#mpl.rcParams['text.latex.unicode']=True
mpl.rcParams['axes.titlesize'] = 20
import h5py


def readfield(folder, field, level):
    #Readig the variable to disk
    f= h5py.File(folder + "/" + field + '_' + str(level) + '.hdf5', "r")
    ds = f[field]
    data = np.empty(shape=ds.shape, dtype=ds.dtype)
    data[:] = ds[:]
    f.close()
    print(data.shape)
    return data


full = np.genfromtxt(fname="src_full/outputDir_integral/auxField_L2NORM")
t_full = full[:,0]
v_full = full[:,1]

half = np.genfromtxt(fname="src_half/outputDir_integral/K_L2NORM")
t_half = half[:,0]
v_half = half[:,1]

############ PLOTS ###############

print(np.abs(v_full-v_half))

pl.figure()
pl.plot(t_full, np.abs(v_full-v_half))
pl.yscale("log")
pl.savefig("error.png")
pl.close()

error = []
for t in range(0, 400+1, 40):
	k_full = readfield("src_full/outputDir_subcycling/visit_dump." + str(t).zfill(5), "auxField", 0)
	k_half = readfield("src_half/outputDir_subcycling/visit_dump." + str(t).zfill(5), "K", 0)

	k_full = k_full[50:,:]

	error_local = np.sum(np.abs(k_full-k_half))
	error.append(error_local)
	'''
	pl.figure()
	pl.imshow(np.abs(k_full-k_half))
	pl.colorbar()
	pl.savefig("t" + str(t) + ".png")
	pl.close()
	'''

pl.figure()
pl.plot(error)
pl.yscale("log")
pl.savefig("error.png")
pl.close()