#ifndef included_Particle_water
#define included_Particle_water

#include "SAMRAI/tbox/MessageStream.h"
#include "SAMRAI/hier/IntVector.h"
#include "SAMRAI/tbox/Database.h"
#include <vector>

/**
 * The SampleClass struct holds some dummy data and methods.  It's intent
 * is to indicate how a user could construct their own index data double.
 */
using namespace std;
using namespace SAMRAI;

class Particle_water
{
public:

	inline int GetExpoBase2(double d);

	bool	Equal(double d1, double d2);

	/**
	* The default constructor.
	*/
	Particle_water();

	/**
	* Constructor.
	*/
	Particle_water(const int region);

	/**
	* The complete constructor.
	*/
	Particle_water(const double fieldValue, const double* pos, const int region = 0);

	/**
	* Copy constructor.
	*/
	Particle_water(const Particle_water& data);
	Particle_water(const Particle_water& data, bool newId);

	~Particle_water();

	//Setters 
	void setId(const int id);
	void setProcId(const int procId);
	void setFieldValue(string varName, double value);

	bool hasField(string varName);

	//Getters
	double getFieldValue(string varName);
	int getId();
	int getProcId();

	//Distance between particles
	inline double distance(Particle_water* data)
	{
		double xd = (data->positionx - positionx);
		double yd = (data->positiony - positiony);
		return sqrt(xd * xd + yd * yd);
	}

	inline double distance(double* point)
	{
		double xd = (point[0] - positionx);
		double yd = (point[1] - positiony);
		return sqrt(xd * xd + yd * yd);
	}
	inline double distance_p(Particle_water* data)
	{
		double xd = (data->positionx_p - positionx_p);
		double yd = (data->positiony_p - positiony_p);
		return sqrt(xd * xd + yd * yd);
	}

	inline double distance_p(double* point)
	{
		double xd = (point[0] - positionx_p);
		double yd = (point[1] - positiony_p);
		return sqrt(xd * xd + yd * yd);
	}
	inline double distancepred(Particle_water* data)
	{
		double xd = (data->predpositionx - predpositionx);
		double yd = (data->predpositiony - predpositiony);
		return sqrt(xd * xd + yd * yd);
	}

	inline double distancepred(double* point)
	{
		double xd = (point[0] - predpositionx);
		double yd = (point[1] - predpositiony);
		return sqrt(xd * xd + yd * yd);
	}


	inline bool same(const Particle_water& data)
	{
		return !(!Equal(positionx, data.positionx) || !Equal(positiony, data.positiony) || region != data.region);
	}
	Particle_water* overlaps(const Particle_water& data);

	Particle_water& operator=(const Particle_water& data);

	inline bool operator==(const Particle_water& data)
	{
		return !((this != &data) || (id != data.id) || (procId != data.procId));
	}

	void packStream(tbox::MessageStream& stream);
	void unpackStream(tbox::MessageStream& stream, const hier::IntVector& offset);
	void getFromDatabase(std::shared_ptr<tbox::Database>& database, int particle);
	void putToDatabase(std::shared_ptr<tbox::Database>& database, int particle);

	static size_t size() {
		size_t bytes = tbox::MessageStream::getSizeof<double>(6 + 13) + tbox::MessageStream::getSizeof<int>(5);
		return(bytes);
	};

   	//Field value of the particle
	double rho;
	double rho_p;
	double vx;
	double vx_p;
	double vy;
	double vy_p;
	double fx;
	double fy;
	double p;
	double mass;
	double predrho;
	double predvx;
	double predvy;


   	//Positions of the particle (Global index position)
	double positionx;
	double positionx_p;
	double positiony;
	double positiony_p;
	double predpositionx;
	double predpositiony;


	//Region information
	int region;
	int interior;
	int newRegion; //The region to become when entering the domain (boundaries)

	//Particle identifier
	int id;
	//Creator processor identifier
	int procId;

private:

	//Particle counter
	static int counter;


};

#endif
