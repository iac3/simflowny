#ifndef included_Particles
#define included_Particles

#include "Particle_water.h"

#include "SAMRAI/pdat/CellIndex.h"
#include <vector>
#include <string>
#include <iostream>
#include "SAMRAI/tbox/MessageStream.h"
#include "SAMRAI/tbox/Database.h"

/**
 * The SampleClass struct holds some dummy data and methods.  It's intent
 * is to indicate how a user could construct their own index data type.
 */
using namespace std;
using namespace SAMRAI;

template<class T>
class Particles
{
public:
   /**
    * The default constructor.
    */
   Particles();

   /**
    * Copy constructor.
    */
   Particles(const Particles& data);

   /**
    * Asignment of time for particle variables.
    */
	inline void setTime_p(double time)
	{
		this->time_p = time;
	}
	inline void setTimepred(double time)
	{
		this->timepred = time;
	}
	inline void setTime(double time)
	{
		this->time = time;
	}

   /**
    * The assignment operator copies the data of the argument cell.
    */
   inline Particles& operator=(const Particles& data)
   {
      particleList = data.particleList;
      return(*this);
   }

   /**
    * The assignment operator copies the data of the argument cell.
    */
   bool operator==(const Particles& cell);


   ~Particles();

   //Calculates the average value of the cell from the values of the particles
   void calculateAverage();

   //Adds the particle to the list
   inline T* addParticle(const T& data)
   {
     particleList.emplace_back(data);
     return &particleList[particleList.size() - 1];
   }

   //delete particle from the list
   void deleteParticle(const T& data);

   inline int getNumberOfParticles()
   {
     return particleList.size();
   }

   bool contains(const T& data);
   T* overlaps(const T& data);

   inline vector<T> getParticleList()
   {
     return particleList;
   }

   //Gets the particle in the position i
   inline T* getParticle(const int index)
   {
      return &particleList[index];
   }

   inline void clearAllParticles() {
      particleList.clear();
   }
   /**
    * The copySourceItem() method allows SampleIndexData to be a templated
    * data type for IndexData - i.e. IndexData<SampleIndexData>.  In
    * addition to this method, the other methods that must be defined are
    * getDataStreamSize(), packStream(), unpackStream() for communication,
    * putToDatabase(), getFromDatabase for restart.  These are described
    * below. 
    */
   inline void copySourceItem(hier::Index& index,
                       const hier::IntVector& src_offset,
                       Particles& src_item)
   {
      particleList = src_item.particleList;
   }
   /**
    * The following functions enable parallel communication with SampleIndexDatas.
    * They are used in SAMRAI communication infrastructure to 
    * specify the number of bytes of data stored in each SampleIndexData object,
    * and to pack and unpack the data to the specified stream.
    */
   inline size_t getDataStreamSize()
   {
      size_t bytes = particleList.size() * T::size() + tbox::MessageStream::getSizeof<int>(1) + tbox::MessageStream::getSizeof<double>(3);

      return(bytes);
   }
   void packStream(tbox::MessageStream& stream);
   void unpackStream(tbox::MessageStream& stream, const hier::IntVector& offset);

   /**
    * These functions are used to read/write SampleIndexData data to/from 
    * restart.
    */
   void getFromRestart(std::shared_ptr<tbox::Database>& database);
   void putToRestart(std::shared_ptr<tbox::Database>& database);

	double time_p, timepred, time;

private:
   //Particle list inside a cell
   vector<T> particleList;

};

#endif
