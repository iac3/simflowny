#include "RefineClasses.h"
#include "TimeInterpolateOperator.h"
#include "TimeRefinementIntegrator.h"
#include "RefineSchedule.h"
#include "RefineAlgorithm.h"
#include "StandardRefineTransactionFactory.h"

#include "RefineTimeTransaction.h"

#include "SAMRAI/SAMRAI_config.h"

#include "SAMRAI/hier/Box.h"
#include "SAMRAI/geom/CartesianGridGeometry.h"
#include "SAMRAI/tbox/Database.h"
#include "SAMRAI/mesh/StandardTagAndInitStrategy.h"
#include "SAMRAI/hier/Index.h"
#include "SAMRAI/hier/Patch.h"
#include "SAMRAI/xfer/CoarsenAlgorithm.h"
#include "SAMRAI/xfer/CoarsenSchedule.h"
#include "MainRestartData.h"
#include "SAMRAI/algs/TimeRefinementLevelStrategy.h"
#include "Commons.h"
#include "Particles.h"
#include "SAMRAI/pdat/CellData.h"
#include "SAMRAI/pdat/IndexData.h"
#include "SAMRAI/pdat/CellVariable.h"
#include "SAMRAI/pdat/CellGeometry.h"
#include "ParticleDataWriter.h"
#include "ParticleRefine.h"
#include "TimeInterpolator.h"

using namespace std;
using namespace SAMRAI;

#define DIMENSIONS 2



class Problem : 
   public mesh::StandardTagAndInitStrategy,
   public xfer::RefinePatchStrategy,
   public xfer::CoarsenPatchStrategy,
   public algs::TimeRefinementLevelStrategy
{
public:
	/*
	 * Constructor of the problem.
	 */
	Problem(
		const string& object_name,
		const tbox::Dimension& dim,
		std::shared_ptr<tbox::Database>& input_db,
		std::shared_ptr<geom::CartesianGridGeometry >& grid_geom, 
	   	std::shared_ptr<hier::PatchHierarchy >& patch_hierarchy, 
	   	MainRestartData& mrd,
		const double dt, 
		const bool init_from_files,
		const int console_output,
		const int timer_output,
		const int particles_output_period,
		const vector<string> full_particle_writer_variables,
		std::shared_ptr<ParticleDataWriter>& particle_data_writer);
  
	/*
	 * Destructor.
	 */
	~Problem();

	/*
	 * Block of subcycling inherited methods.
	 */
	void initializeLevelIntegrator(
	      const std::shared_ptr<mesh::GriddingAlgorithmStrategy>& gridding_alg);

	double getLevelDt(
	      const std::shared_ptr<hier::PatchLevel>& level,
	      const double dt_time,
	      const bool initial_time);

	double getMaxFinerLevelDt(
	      const int finer_level_number,
	      const double coarse_dt,
	      const hier::IntVector& ratio_to_coarser);

	double advanceLevel(
	      const std::shared_ptr<hier::PatchLevel>& level,
	      const std::shared_ptr<hier::PatchHierarchy>& hierarchy,
	      const double current_time,
	      const double new_time,
	      const bool first_step,
	      const bool last_step,
	      const bool regrid_advance = false);

	void standardLevelSynchronization(
	      const std::shared_ptr<hier::PatchHierarchy>& hierarchy,
	      const int coarsest_level,
	      const int finest_level,
	      const double sync_time,
	      const std::vector<double>& old_times);

	void synchronizeNewLevels(
	      const std::shared_ptr<hier::PatchHierarchy>& hierarchy,
	      const int coarsest_level,
	      const int finest_level,
	      const double sync_time,
	      const bool initial_time);

	void resetTimeDependentData(
	      const std::shared_ptr<hier::PatchLevel>& level,
	      const double new_time,
	      const bool can_be_refined);

	void resetDataToPreadvanceState(
	      const std::shared_ptr<hier::PatchLevel>& level);

	bool usingRefinedTimestepping() const
	{
	      return d_refinedTimeStepping;
	} 

	/*
	 * Register the current iteration in the class.
	 */
	void registerIteration(int iter) {
		simPlat_iteration = iter;
	}

	/*
	 * Initialize the data from a given level.
   	 */
	virtual void initializeLevelData(
		const std::shared_ptr<hier::PatchHierarchy >& hierarchy ,
		const int level_number ,
		const double init_data_time ,
		const bool can_be_refined ,
		const bool initial_time ,
		const std::shared_ptr<hier::PatchLevel >& old_level=std::shared_ptr<hier::PatchLevel>() ,
		const bool allocate_data = true);

	/*
	 * Reset the hierarchy-dependent internal information.
	 */
	virtual void resetHierarchyConfiguration(
		const std::shared_ptr<hier::PatchHierarchy >& new_hierarchy ,
		int coarsest_level ,
		int finest_level);

	/*
	 * Checks the finalization conditions              
	 */
	bool checkFinalization(
		const double simPlat_time, 
		const double simPlat_dt);

	/*
	 * This method sets the physical boundary conditions.
	*/
	void setPhysicalBoundaryConditions(
		hier::Patch& patch,
		const double fill_time,
		const hier::IntVector& ghost_width_to_fill);
	/*
	 * Set up external plotter to plot internal data from this class.        
	 * Tell the plotter about the refinement ratios.  Register variables     
	 * appropriate for plotting.                                            
	 */
	int setupPlotterParticles(ParticleDataWriter &plotter ) const;


	/*
	 * Map data on a patch. This mapping is done only at the begining of the simulation.
	 */
	void mapDataOnPatch(const double time, const bool initial_time, const int ln, const std::shared_ptr< hier::PatchLevel >& level);

	/*
	 * Flood-Fill algorithm
	 */
	void floodfill(std::shared_ptr< hier::Patch > patch, int i, int j, int pred, int seg) const;
	template<class T>
	void floodfillParticles(const hier::Patch& patch, std::vector<double> particleSeparation, std::vector<double> domain_offset_factor, int i, int j, int pred, int seg) const;


	/*
	 * FOV correction for AMR
	 */
	void correctFOVS(const std::shared_ptr< hier::PatchLevel >& level);

	template<class T>
	void mapDataOnParticles(const std::shared_ptr< hier::PatchLevel >& level, std::string particleDistribution, std::vector<double> number_of_particles, std::vector<double> particleSeparation, std::vector<double> domain_offset_factor, std::vector<double> normal_mean, std::vector<double> normal_stddev, std::vector<double> box_min, std::vector<double> box_max);


	/*
	 * Interphase mapping. Calculates the FOV and its variables.
	 */
	void interphaseMapping(const double time, const bool initial_time, const int ln, const std::shared_ptr< hier::PatchLevel >& level, const int remesh);




	/*
	 * Initialize data on a patch. This initialization is done only at the begining of the simulation.
	 */
	void initializeDataOnPatch(
		hier::Patch& patch,
		const double time,
       		const bool initial_time);

	/*
	 *  Cell tagging routine - tag cells that require refinement based on a provided condition. 
	 */
	void applyGradientDetector(
	   	const std::shared_ptr< hier::PatchHierarchy >& hierarchy, 
	   	const int level_number,
	   	const double time, 
	   	const int tag_index,
	   	const bool initial_time,
	   	const bool uses_richardson_extrapolation_too);

	/*
	* Return maximum stencil width needed for user-defined
	* data interpolation operations.  Default is to return
	* zero, assuming no user-defined operations provided.
	*/
	hier::IntVector getRefineOpStencilWidth(const tbox::Dimension &dim) const
	{
		return hier::IntVector::getZero(dim);
	}

	/*
	* Pre- and post-processing routines for implementing user-defined
	* spatial interpolation routines applied to variables.  The 
	* interpolation routines are used in the MOL AMR algorithm
	* for filling patch ghost cells before advancing data on a level
	* and after regridding a level to fill portions of the new level
	* from some coarser level.  These routines are called automatically
	* from within patch boundary filling schedules; thus, some concrete
	* function matching these signatures must be provided in the user's
	* patch model.  However, the routines only need to perform some 
	* operations when "USER_DEFINED_REFINE" is given as the interpolation 
	* method for some variable when the patch model registers variables
	* with the MOL integration algorithm, typically.  If the 
	* user does not provide operations that refine such variables in either 
	* of these routines, then they will not be refined.
	*
	* The order in which these operations are used in each patch 
	* boundary filling schedule is:
	* 
	* - \b (1) {Call user's preprocessRefine() routine.}
	* - \b (2) {Refine all variables with standard interpolation operators.}
	* - \b (3) {Call user's postprocessRefine() routine.}
	* 
	* 
	* Also, user routines that implement these functions must use 
	* data corresponding to the d_scratch context on both coarse and
	* fine patches.
	*/
	virtual void preprocessRefine(
		hier::Patch& fine,
		const hier::Patch& coarse,
                const hier::Box& fine_box,
                const hier::IntVector& ratio)
	{
		NULL_USE(fine);
		NULL_USE(coarse);
		NULL_USE(fine_box);
		NULL_USE(ratio);
	}
	virtual void postprocessRefine(
		hier::Patch& fine,
                const hier::Patch& coarse,
                const hier::Box& fine_box,
                const hier::IntVector& ratio)
	{
		NULL_USE(fine);
		NULL_USE(coarse);
		NULL_USE(fine_box);
		NULL_USE(ratio);
	}



	/*
	* Return maximum stencil width needed for user-defined
	* data coarsen operations.  Default is to return
	* zero, assuming no user-defined operations provided.
	*/
	hier::IntVector getCoarsenOpStencilWidth( const tbox::Dimension &dim ) const {
		return hier::IntVector::getZero(dim);
	}

	/*
	* Pre- and post-processing routines for implementing user-defined
	* spatial coarsening routines applied to variables.  The coarsening 
	* routines are used in the MOL AMR algorithm synchronizing 
	* coarse and fine levels when they have been integrated to the same
	* point.  These routines are called automatically from within the 
	* data synchronization coarsen schedules; thus, some concrete
	* function matching these signatures must be provided in the user's
	* patch model.  However, the routines only need to perform some
	* operations when "USER_DEFINED_COARSEN" is given as the coarsening
	* method for some variable when the patch model registers variables
	* with the MOL level integration algorithm, typically.  If the
	* user does not provide operations that coarsen such variables in either
	* of these routines, then they will not be coarsened.
	*
	* The order in which these operations are used in each coarsening
	* schedule is:
	* 
	* - \b (1) {Call user's preprocessCoarsen() routine.}
	* - \b (2) {Coarsen all variables with standard coarsening operators.}
	* - \b (3) {Call user's postprocessCoarsen() routine.}
	* 
	*
	* Also, user routines that implement these functions must use
	* corresponding to the d_new context on both coarse and fine patches
	* for time-dependent quantities.
	*/
	virtual void preprocessCoarsen(
		hier::Patch& coarse,
                const hier::Patch& fine,
                const hier::Box& coarse_box,
                const hier::IntVector& ratio)
	{
		NULL_USE(fine);
		NULL_USE(coarse);
		NULL_USE(coarse_box);
		NULL_USE(ratio);
	}
	virtual void postprocessCoarsen(
		hier::Patch& coarse,
                const hier::Patch& fine,
                const hier::Box& coarse_box,
                const hier::IntVector& ratio)
	{
		NULL_USE(fine);
		NULL_USE(coarse);
		NULL_USE(coarse_box);
		NULL_USE(ratio);
	}

	/*
	 * Computes the dt to be used
	 */
	double computeDt() const;

	template<class T>
	void checkRegion(T* particle, const int newIndex0, const int newIndex1, const int index0, const int index1, const hier::Index boxfirst1, const hier::Index boxlast1);
	template<class T>
	void checkPosition(const std::shared_ptr<hier::Patch >& patch, int i, int j, Particles<T>* particles);
	template<class T>
	void moveParticles(const std::shared_ptr<hier::Patch > patch, std::shared_ptr< pdat::IndexData<Particles<T>, pdat::CellGeometry > > particleVariables, Particles<T>* part, T* particle, const int index0, const int index1, const double newPosi, const double newPosj, const double simPlat_dt, const double current_time, int pit, int step, double level_influenceRadius);
	void initCommonVars(const std::shared_ptr<hier::PatchHierarchy >& hierarchy);
	template<class T>
	void calculateCellInfluence(hier::Patch& patch) const;


	/*
	 * Checks if the point has to be stalled
	 */
	bool checkStalled(std::shared_ptr< hier::Patch > patch, int i, int j, int v) const;



	/*
	 * Saves restart information relevant in Problem class.
	 */
	void putToRestart(MainRestartData& mrd);

	/*
	 * Loads restart information relevant in Problem class.
	 */
	void getFromRestart(MainRestartData& mrd);

	/*
	 * Allocates internal variable memory
	 */
	void allocateAfterRestart();

private:	 
	//Variables for the refine and coarsen algorithms

	std::shared_ptr< xfer::RefineAlgorithm > d_bdry_fill_init;
	std::shared_ptr< xfer::RefineAlgorithm > d_bdry_post_coarsen;
	std::vector< std::shared_ptr< xfer::RefineSchedule > > d_bdry_sched_postCoarsen;
	std::shared_ptr<TimeInterpolator> time_interpolate_operator_mesh1;
	std::shared_ptr<TimeInterpolator> time_interpolate_operator_particles_water;
	ParticleRefine<Particle_water> * refine_particle_operator_water;
	std::shared_ptr< xfer::RefineAlgorithm > d_bdry_fill_advance;
	std::vector< std::shared_ptr< xfer::RefineSchedule > > d_bdry_sched_advance;

	std::shared_ptr< xfer::RefineAlgorithm > d_mapping_fill;
	std::shared_ptr< xfer::RefineAlgorithm > d_tagging_fill;
	std::shared_ptr< xfer::CoarsenAlgorithm > d_coarsen_algorithm;
	std::vector< std::shared_ptr< xfer::CoarsenSchedule > > d_coarsen_schedule;

	std::shared_ptr< xfer::RefineAlgorithm > d_fill_new_level, d_fill_new_level_der_aux;

	//Object name
	std::string d_object_name;

	//Pointers to the grid geometry and the patch hierarchy
   	std::shared_ptr<geom::CartesianGridGeometry > d_grid_geometry;
	std::shared_ptr<hier::PatchHierarchy > d_patch_hierarchy;

	//Identifiers of the fields and auxiliary fields
	int d_particleVariables_water_id;

	//Parameter variables
	double dissipation_factor_rho;
	double pfy;
	double rho0;
	double dissipation_factor_vy;
	double pfx;
	double dissipation_factor_vx;
	double mu;
	double c0;
	double Paux;
	double tend;
	int dimensions_empty;
	double particle_velocity_corrector_factor;
	double gamma;
	int dimensions_cube;

	//Particle variables
	double influenceRadius;
	string particleDistribution_water;
	std::vector<double> number_of_particles_water, particleSeparation_water, domain_offset_factor_water, normal_mean_water, normal_stddev_water, box_min_water, box_max_water;
	int mapStencil;
	int mapStencilx;
	std::vector<std::string> resultVars_x;
	double* vars_x;
	double particleSeparation_x;
	double xGlower, xGupper;
	int miniIndex, maxiIndex;
	int ilastG;
	int mapStencily;
	std::vector<std::string> resultVars_y;
	double* vars_y;
	double particleSeparation_y;
	double yGlower, yGupper;
	int minjIndex, maxjIndex;
	int jlastG;

	//mapping fields
	int d_nonSync_regridding_tag_id, d_interior_regridding_value_id, d_interior_i_id, d_interior_j_id, d_nonSyncP_id, d_region_id, d_FOV_1_id, d_FOV_xLower_id, d_FOV_xUpper_id, d_FOV_yLower_id, d_FOV_yUpper_id;

	//Stencils of the discretization method variable
	int d_ghost_width, d_regionMinThickness;

	//initial dt
	double initial_dt;

   	const tbox::Dimension d_dim;

	//Subcycling variable
   	bool d_refinedTimeStepping;

	//Current iteration
	int simPlat_iteration;

	//Initialization from files
	bool d_init_from_restart;

	//List of particle variables
	vector<string> particleVariableList_water;


	//Variables to dump



	//FileWriter
	int viz_particle_dump_interval;
	int next_particle_dump_iteration;
	set<string> d_full_particle_writer_variables;
	std::shared_ptr<ParticleDataWriter> d_particle_data_writer;


	vector<int> current_iteration;
	vector<int> bo_substep_iteration;

	//Console output variables
	int d_output_interval;
	int next_console_output;
	int d_timer_output_interval;
	int next_timer_output;

	//regridding options
	std::shared_ptr<tbox::Database> regridding_db;
	bool d_regridding;
	std::string d_regridding_field, d_regridding_type, d_regridding_field_shadow;
	double d_regridding_threshold, d_regridding_compressionFactor, d_regridding_mOffset, d_regridding_error, d_regridding_buffer;
	int d_regridding_min_level, d_regridding_max_level;

	//Gets the coarser patch that includes the box.
	const std::shared_ptr<hier::Patch >& getCoarserPatch(
		const std::shared_ptr< hier::PatchLevel >& level,
		const hier::Box interior, 
		const hier::IntVector ratio);

	static bool Equals(double d1, double d2);
	static inline int GetExpoBase2(double d);
};


