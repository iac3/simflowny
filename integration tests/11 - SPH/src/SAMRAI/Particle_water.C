#ifndef included_Particle_waterC
#define included_Particle_waterC

#include "Particle_water.h"
#include <string.h>
#include <math.h>
#include "SAMRAI/hier/Index.h"
#include <SAMRAI/tbox/SAMRAI_MPI.h>

using namespace std::string_literals;

inline int Particle_water::GetExpoBase2(double d)
{
	int i = 0;
	((short *)(&i))[0] = (((short *)(&d))[3] & (short)32752); // _123456789ab____ & 0111111111110000
	return (i >> 4) - 1023;
}
bool	Particle_water::Equal(double d1, double d2)
{
	if (d1 == d2)
		return true;
	int e1 = GetExpoBase2(d1);
	int e2 = GetExpoBase2(d2);
	int e3 = GetExpoBase2(d1 - d2);
	if ((e3 - e2 < -48) && (e3 - e1 < -48))
		return true;
	return false;
}

int Particle_water::counter = 0;

Particle_water::Particle_water()
{
	const tbox::SAMRAI_MPI& mpi(tbox::SAMRAI_MPI::getSAMRAIWorld());
	counter++;
	id = counter;
	procId = mpi.getRank();
	interior = 0;
}

Particle_water::Particle_water(const int region)
{
	const tbox::SAMRAI_MPI& mpi(tbox::SAMRAI_MPI::getSAMRAIWorld());
	this->region = region;
	this->newRegion = 0;
	this->interior = 0;
	counter++;
	this->id = counter;
	this->procId = mpi.getRank();
}

Particle_water::Particle_water(const double fieldValue, const double* pos, const int region)
{
	const tbox::SAMRAI_MPI& mpi(tbox::SAMRAI_MPI::getSAMRAIWorld());
	this->rho = fieldValue;
	this->rho_p = fieldValue;
	this->vx = fieldValue;
	this->vx_p = fieldValue;
	this->vy = fieldValue;
	this->vy_p = fieldValue;
	this->fx = fieldValue;
	this->fy = fieldValue;
	this->p = fieldValue;
	this->mass = fieldValue;
	this->predrho = fieldValue;
	this->predvx = fieldValue;
	this->predvy = fieldValue;

	positionx = pos[0];
	positiony = pos[1];

	this->region = region;
	this->newRegion = 0;
	this->interior = 0;
	counter++;
	this->id = counter;
	this->procId = mpi.getRank();
}

Particle_water::Particle_water(const Particle_water& data)
{
	this->rho = data.rho;
	this->rho_p = data.rho_p;
	this->vx = data.vx;
	this->vx_p = data.vx_p;
	this->vy = data.vy;
	this->vy_p = data.vy_p;
	this->fx = data.fx;
	this->fy = data.fy;
	this->p = data.p;
	this->mass = data.mass;
	this->predrho = data.predrho;
	this->predvx = data.predvx;
	this->predvy = data.predvy;

	this->positionx = data.positionx;
	this->positionx_p = data.positionx_p;
	this->positiony = data.positiony;
	this->positiony_p = data.positiony_p;
	this->predpositionx = data.predpositionx;
	this->predpositiony = data.predpositiony;

	region = data.region;
	newRegion = data.newRegion;
	id = data.id;
	procId = data.procId;
	interior = data.interior;
}

Particle_water::Particle_water(const Particle_water& data, bool newId)
{
	this->rho = data.rho;
	this->rho_p = data.rho_p;
	this->vx = data.vx;
	this->vx_p = data.vx_p;
	this->vy = data.vy;
	this->vy_p = data.vy_p;
	this->fx = data.fx;
	this->fy = data.fy;
	this->p = data.p;
	this->mass = data.mass;
	this->predrho = data.predrho;
	this->predvx = data.predvx;
	this->predvy = data.predvy;

	this->positionx = data.positionx;
	this->positionx_p = data.positionx_p;
	this->positiony = data.positiony;
	this->positiony_p = data.positiony_p;
	this->predpositionx = data.predpositionx;
	this->predpositiony = data.predpositiony;

	region = data.region;
	newRegion = data.newRegion;
	id = data.id;
	procId = data.procId;
	if (newId) {
		counter++;
		id = counter;
		const tbox::SAMRAI_MPI& mpi(tbox::SAMRAI_MPI::getSAMRAIWorld());
		procId = mpi.getRank();
	}
	interior = data.interior;
}

Particle_water::~Particle_water()
{
}

void Particle_water::setId(const int id)
{
	this->id = id;
}

void Particle_water::setProcId(const int procId)
{
	this->procId = procId;
}

double Particle_water::getFieldValue(string varName)
{
	if (varName == "rho")
		return rho;

	if (varName == "rho_p")
		return rho_p;

	if (varName == "vx")
		return vx;

	if (varName == "vx_p")
		return vx_p;

	if (varName == "vy")
		return vy;

	if (varName == "vy_p")
		return vy_p;

	if (varName == "fx")
		return fx;

	if (varName == "fy")
		return fy;

	if (varName == "p")
		return p;

	if (varName == "mass")
		return mass;

	if (varName == "predrho")
		return predrho;

	if (varName == "predvx")
		return predvx;

	if (varName == "predvy")
		return predvy;



	if (varName == "procId_water")
		return procId;

	if (varName == "id_water")
		return id;

	return -1;
}

bool Particle_water::hasField(string varName)
{
	if (varName == "rho")
		return true;

	if (varName == "rho_p")
		return true;

	if (varName == "vx")
		return true;

	if (varName == "vx_p")
		return true;

	if (varName == "vy")
		return true;

	if (varName == "vy_p")
		return true;

	if (varName == "fx")
		return true;

	if (varName == "fy")
		return true;

	if (varName == "p")
		return true;

	if (varName == "mass")
		return true;

	if (varName == "predrho")
		return true;

	if (varName == "predvx")
		return true;

	if (varName == "predvy")
		return true;


	return false;
}

void Particle_water::setFieldValue(string varName, double value)
{
	if (varName == "rho")
		rho = value;

	if (varName == "rho_p")
		rho_p = value;

	if (varName == "vx")
		vx = value;

	if (varName == "vx_p")
		vx_p = value;

	if (varName == "vy")
		vy = value;

	if (varName == "vy_p")
		vy_p = value;

	if (varName == "fx")
		fx = value;

	if (varName == "fy")
		fy = value;

	if (varName == "p")
		p = value;

	if (varName == "mass")
		mass = value;

	if (varName == "predrho")
		predrho = value;

	if (varName == "predvx")
		predvx = value;

	if (varName == "predvy")
		predvy = value;


}

int Particle_water::getId()
{
	return id;
}

int Particle_water::getProcId()
{
	return procId;
}

Particle_water* Particle_water::overlaps(const Particle_water& data)
{
	if (!Equal(positionx, data.positionx) || !Equal(positiony, data.positiony)) {
		return NULL;
	}
	return this;
}

Particle_water& Particle_water::operator=(const Particle_water& data)
{
	if (this != &data) {
		rho = data.rho;
		rho_p = data.rho_p;
		vx = data.vx;
		vx_p = data.vx_p;
		vy = data.vy;
		vy_p = data.vy_p;
		fx = data.fx;
		fy = data.fy;
		p = data.p;
		mass = data.mass;
		predrho = data.predrho;
		predvx = data.predvx;
		predvy = data.predvy;

		positionx = data.positionx;
		positionx_p = data.positionx_p;
		positiony = data.positiony;
		positiony_p = data.positiony_p;
		predpositionx = data.predpositionx;
		predpositiony = data.predpositiony;

		region = data.region;
		id = data.id;
		interior = data.interior;
		procId = data.procId;
	}
	return(*this);

}


void Particle_water::packStream(tbox::MessageStream& stream)
{
	int counter;
	int ibuffer1[5];
	double dbuffer1[6 + 13];
	//Field value
	dbuffer1[0] = rho;
	dbuffer1[1] = rho_p;
	dbuffer1[2] = vx;
	dbuffer1[3] = vx_p;
	dbuffer1[4] = vy;
	dbuffer1[5] = vy_p;
	dbuffer1[6] = fx;
	dbuffer1[7] = fy;
	dbuffer1[8] = p;
	dbuffer1[9] = mass;
	dbuffer1[10] = predrho;
	dbuffer1[11] = predvx;
	dbuffer1[12] = predvy;

	//position
	dbuffer1[13] = positionx;
	dbuffer1[14] = positionx_p;
	dbuffer1[15] = positiony;
	dbuffer1[16] = positiony_p;
	dbuffer1[17] = predpositionx;
	dbuffer1[18] = predpositiony;

	//region information
	ibuffer1[0] = region;
	//particle id
	ibuffer1[1] = id;
	ibuffer1[2] = interior;
	ibuffer1[3] = newRegion;
	ibuffer1[4] = procId;
	stream.pack(dbuffer1, 6 + 13);
	stream.pack(ibuffer1, 5);
}

void Particle_water::unpackStream(tbox::MessageStream& stream,
                                const hier::IntVector& offset)
{
	int counter;
	int ibuffer1[5];
	double dbuffer1[6 + 13];
	stream.unpack(dbuffer1, 6 + 13);
	stream.unpack(ibuffer1, 5);
	//Field value
	rho = dbuffer1[0];
	rho_p = dbuffer1[1];
	vx = dbuffer1[2];
	vx_p = dbuffer1[3];
	vy = dbuffer1[4];
	vy_p = dbuffer1[5];
	fx = dbuffer1[6];
	fy = dbuffer1[7];
	p = dbuffer1[8];
	mass = dbuffer1[9];
	predrho = dbuffer1[10];
	predvx = dbuffer1[11];
	predvy = dbuffer1[12];

	//position
	positionx = dbuffer1[13];
	positionx_p = dbuffer1[14];
	positiony = dbuffer1[15];
	positiony_p = dbuffer1[16];
	predpositionx = dbuffer1[17];
	predpositiony = dbuffer1[18];

	//region information
	region = ibuffer1[0];
	//particle id
	id = ibuffer1[1];
	interior = ibuffer1[2];
	newRegion = ibuffer1[3];
	procId = ibuffer1[4];
}

void Particle_water::putToDatabase(
   std::shared_ptr<tbox::Database>& database, int particle)
{
	char buffer [33];
	sprintf(buffer,"%d",particle);
	std::shared_ptr<tbox::Database> particle_db(database->putDatabase(buffer));
	int counter;
	int ibuffer1[5];
	double dbuffer1[6 + 13];
	//Field value
	dbuffer1[0] = rho;
	dbuffer1[1] = rho_p;
	dbuffer1[2] = vx;
	dbuffer1[3] = vx_p;
	dbuffer1[4] = vy;
	dbuffer1[5] = vy_p;
	dbuffer1[6] = fx;
	dbuffer1[7] = fy;
	dbuffer1[8] = p;
	dbuffer1[9] = mass;
	dbuffer1[10] = predrho;
	dbuffer1[11] = predvx;
	dbuffer1[12] = predvy;

	//position
	dbuffer1[13] = positionx;
	dbuffer1[14] = positionx_p;
	dbuffer1[15] = positiony;
	dbuffer1[16] = positiony_p;
	dbuffer1[17] = predpositionx;
	dbuffer1[18] = predpositiony;

	//region information
	ibuffer1[0] = region;
	//particle id
	ibuffer1[1] = id;
	ibuffer1[2] = interior;
	ibuffer1[3] = newRegion;
	ibuffer1[4] = procId;
	particle_db->putIntegerArray("ibuffer", ibuffer1, 5);
	particle_db->putDoubleArray("dbuffer", dbuffer1, 6 + 13);
}

void Particle_water::getFromDatabase(
   std::shared_ptr<tbox::Database>& database, int particle)
{
	char buffer [33];
	sprintf(buffer,"%d",particle);
	std::shared_ptr<tbox::Database> particle_db(database->getDatabase(buffer));
	int counter;
	int ibuffer1[5];
	double dbuffer1[6 + 13];
	particle_db->getIntegerArray("ibuffer", ibuffer1, 5);
	particle_db->getDoubleArray("dbuffer", dbuffer1, 6 + 13);
	//Field value
	rho = dbuffer1[0];
	rho_p = dbuffer1[1];
	vx = dbuffer1[2];
	vx_p = dbuffer1[3];
	vy = dbuffer1[4];
	vy_p = dbuffer1[5];
	fx = dbuffer1[6];
	fy = dbuffer1[7];
	p = dbuffer1[8];
	mass = dbuffer1[9];
	predrho = dbuffer1[10];
	predvx = dbuffer1[11];
	predvy = dbuffer1[12];

	//position
	positionx = dbuffer1[13];
	positionx_p = dbuffer1[14];
	positiony = dbuffer1[15];
	positiony_p = dbuffer1[16];
	predpositionx = dbuffer1[17];
	predpositiony = dbuffer1[18];


	//region information
	region = ibuffer1[0];
	//particle id
	id = ibuffer1[1];
	interior = ibuffer1[2];
	newRegion = ibuffer1[3];
	procId = ibuffer1[4];
}

#endif

