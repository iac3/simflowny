#ifndef included_ParticleDataWriter
#define included_ParticleDataWriter

#include <string>
#include "Particles.h"
#include "SAMRAI/pdat/CellGeometry.h"
#include "SAMRAI/pdat/IndexData.h"
#include "SAMRAI/tbox/Timer.h"
#include "SAMRAI/tbox/Array.h"
#include "SAMRAI/hier/PatchHierarchy.h"
#include "SAMRAI/hier/VariableDatabase.h"
#include <silo.h>
#include "SAMRAI/hier/IntVector.h"
#include "SAMRAI/geom/CartesianGridGeometry.h"

using namespace SAMRAI;

#ifndef VISIT_PARTICLES_MAX_NUMBER_COMPONENTS
#define VISIT_PARTICLES_MAX_NUMBER_COMPONENTS (100)
#endif

class ParticleDataWriter
{
public:
	ParticleDataWriter(const tbox::Dimension& dim, const std::string& object_name, const std::string& dump_directory_name, const bool plotAverages);

	~ParticleDataWriter();

	void writePlotData(const std::shared_ptr<geom::CartesianGridGeometry >& grid_geometry, const std::shared_ptr< hier::PatchHierarchy >& hierarchy, int time_step, double simulation_time);

/*
*************************************************************************
*                                                                       *
* Register plot variables                                               *
*                                                                       *
*************************************************************************
*/
template<class T>
void registerPlotVariable(const std::string& variable_name, const int patch_data_index)
{
    /*
     * Check for name conflicts with existing registered variables.
     */
	if (std::is_same<T, Particle_water>::value) {
		for (std::list<ParticleItem>::iterator ipi(d_plot_items_water.begin()); ipi != d_plot_items_water.end(); ipi++) {
			if (ipi->d_var_name == variable_name) {
				TBOX_ERROR("ParticleDataWriter::registerPlotVariable()"
				<< "    Attempting to register variable with name "
				<< variable_name << "    more than once." << std::endl);
			}
		}
	}


    /*
     * Create a plot item and initialize its characteristics.
     */
    ParticleItem plotitem;

    initializePlotItem(plotitem, variable_name, patch_data_index);

    d_number_visit_variables++;
	if (std::is_same<T, Particle_water>::value) {
		d_plot_items_water.push_back(plotitem);
	}

}

private:
   /*
    * Static boolean that specifies if the summary file (d_summary_filename)
    * has been opened.
    */
   static bool s_summary_file_opened;

	bool d_plotAverages;

	std::string d_object_name;

	int d_number_visit_variables;

	/*
	* hier::Variable data type  - double, integer
	*/
	enum variable_data_type {VISIT_DOUBLE = 1};

	/*
	* The following structure is used to store data about each item
	* to be written to a plot file.
	*
	*   d_var_name - string variable name:
	*   d_var_data_type - INT, DOUBLE
	*   d_patch_data_index - int patch data id
	*   d_depth - int depth of patch data
	*   d_visit_var_name - visit var name
	*/
	struct ParticleItem {
		std::string d_var_name;
		int d_patch_data_index;
		int d_depth;
		variable_data_type d_var_data_type;
		std::string d_visit_var_name;
      		std::vector<int> d_level_patch_data_index;
	};

	/*
	* Coordinate writing HDF plot files, both data and summary.
	*/
	void writeHDFFiles(const std::shared_ptr<geom::CartesianGridGeometry >& grid_geometry, const std::shared_ptr< hier::PatchHierarchy >& hierarchy, double simulation_time);

	/*
	* Utility routine to initialize a standard variable for
	* plotting based on user input.  Derived, coordinate, material,
	* and species data all use this method to initialize the variable
	* and then set specific characteristics in their appropriate
	* register methods.
	*/
	void initializePlotItem(ParticleItem& plotitem, const std::string& variable_name, const int patch_data_index);

	/*
	* Barrier functions to enable orderly writing of cluster files by
	* passing a baton from current writer to next proc in cluster.
	*/
	void  dumpWriteBarrierBegin();
	void  dumpWriteBarrierEnd();

	/*
	* Convert level number, patch number, block_number to global patch number.
	* Block number (block_number) is only relevant in multiblock problems.
	*/
	int getGlobalPatchNumber(const std::shared_ptr< hier::PatchHierarchy >& hierarchy, const int level_number, const int block_number, const int patch_number);

	/*
	* Write variable data to HDF file.
	*/
	void writeVisItVariablesToHDFFile(const std::shared_ptr<geom::CartesianGridGeometry >& grid_geometry, const std::shared_ptr< hier::PatchHierarchy >& hierarchy, int coarsest_level, int finest_level, double simulation_time);

	/*
	* Write summary data for VisIt to HDF file.
	*/
        void writeSummaryToHDFFile(std::string dump_dir_name, const std::shared_ptr<geom::CartesianGridGeometry >& grid_geometry, const std::shared_ptr< hier::PatchHierarchy >& hierarchy, int coarsest_plot_level, int finest_plot_level, double simulation_time);

	/*
	* Pack regular (i.e. NOT materials or species) and derived data into
	* the supplied HDF database for output.
	*/
	void packRegularAndDerivedData(const std::shared_ptr<geom::CartesianGridGeometry >& grid_geometry, const std::shared_ptr< hier::PatchHierarchy >& hierarchy, const int level_number, const int block_number, hier::Patch& patch, double simulation_time);

	// Write data to hdf5 file
	template<class T>
	void writeDouble(const std::string& key, const std::shared_ptr<geom::CartesianGridGeometry >& grid_geometry, const std::shared_ptr< hier::PatchHierarchy >& hierarchy, const int patch_data_id, const hier::Patch& patch, double simulation_time);

	/*
	* Directory into which VisIt files will be written.
	*/
	std::string d_top_level_directory_name;
	std::string d_current_dump_directory_name;
	std::string d_summary_filename;

	/*
	* std::vector of mesh-scaling ratios from each level to reference level
	* (i.e., coarsest level).
	*/
	std::vector< hier::IntVector > d_scaling_ratios;

	/*
	* Cluster information for parallel runs. A file_cluster
	* is a set of processors that all write VisIt data to
	* a single disk file.  d_processor_on_file_cluster[processorNumber]
	* returns the file_clusterNumber of processorNumber.
	* d_file_cluster_leader is controller of file_cluster.
	*/
	int d_number_file_clusters;
	int d_my_file_cluster_number;
	std::vector<int> d_processor_in_file_cluster_number;
	bool d_file_cluster_leader;
	int d_file_cluster_size;
	int d_my_rank_in_file_cluster;
	int d_number_files_this_file_cluster;

	/*
	* Hierarchy level information to write to a file.
	*/
	int d_number_levels;

	//! @brief Timer for writePlotData().
	std::shared_ptr<tbox::Timer> t_write_plot_data;

	/*
	*  tbox::List of scalar and vector variables registered with
	*  VisItDataWriter.
	*/
	std::list<ParticleItem> d_plot_items_water;


	/*
	* Time step number (passed in by user).
	*/
	int d_time_step_number;

	/*
	 * Dimension of object
	 */
	const tbox::Dimension d_dim;

	//SILO
	DBfile *db;
	DBfile *sumdb;
	bool create(const std::string& name, bool summary);
	bool open(const std::string& name);
	bool close(bool summary);
	bool putDatabase(const std::string& key, bool summary);
};
#endif
