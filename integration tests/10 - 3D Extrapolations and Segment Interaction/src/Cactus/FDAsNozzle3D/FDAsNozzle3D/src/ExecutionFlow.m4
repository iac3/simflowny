/*@@
  @file      ExecutionFlow.F
  @date      Fri Jun 07 09:05:03 CEST 2013
  @author    10
  @desc
             Execution flow for FDAsNozzle3D
  @enddesc
  @version 10
@@*/

define(CDi,(1.333333333d0 * ($4) - 1.333333333d0 * ($3) + ($2) * 0.166666667d0 - ($5) * 0.166666667d0) * 0.5d0 / deltax)dnl
define(CDj,(1.333333333d0 * ($4) - 1.333333333d0 * ($3) + ($2) * 0.166666667d0 - ($5) * 0.166666667d0) * 0.5d0 / deltay)dnl
define(CDk,(1.333333333d0 * ($4) - 1.333333333d0 * ($3) + ($2) * 0.166666667d0 - ($5) * 0.166666667d0) * 0.5d0 / deltaz)dnl
define(FDOCi,1.0d0 / (12.0d0 * deltax) * (($3(($5) - 2, ($6), ($7)) - 8.0d0 * $3(($5) - 1, ($6), ($7)) + 8.0d0 * $3(($5) + 1, ($6), ($7))) - $3(($5) + 2, ($6), ($7)) + 1.0d0 * (((-$4(($5) - 2, ($6), ($7))) * ($2(($5) - 1, ($6), ($7)) - $2(($5) - 2, ($6), ($7))) + 3.0d0 * $4(($5) - 1, ($6), ($7)) * ($2(($5), ($6), ($7)) - $2(($5) - 1, ($6), ($7)))) - 3.0d0 * $4(($5), ($6), ($7)) * ($2(($5) + 1, ($6), ($7)) - $2(($5), ($6), ($7))) + $4(($5) + 1, ($6), ($7)) * ($2(($5) + 2, ($6), ($7)) - $2(($5) + 1, ($6), ($7))))))dnl
define(FDOCj,1.0d0 / (12.0d0 * deltay) * (($3(($5), ($6) - 2, ($7)) - 8.0d0 * $3(($5), ($6) - 1, ($7)) + 8.0d0 * $3(($5), ($6) + 1, ($7))) - $3(($5), ($6) + 2, ($7)) + 1.0d0 * (((-$4(($5), ($6) - 2, ($7))) * ($2(($5), ($6) - 1, ($7)) - $2(($5), ($6) - 2, ($7))) + 3.0d0 * $4(($5), ($6) - 1, ($7)) * ($2(($5), ($6), ($7)) - $2(($5), ($6) - 1, ($7)))) - 3.0d0 * $4(($5), ($6), ($7)) * ($2(($5), ($6) + 1, ($7)) - $2(($5), ($6), ($7))) + $4(($5), ($6) + 1, ($7)) * ($2(($5), ($6) + 2, ($7)) - $2(($5), ($6) + 1, ($7))))))dnl
define(FDOCk,1.0d0 / (12.0d0 * deltaz) * (($3(($5), ($6), ($7) - 2) - 8.0d0 * $3(($5), ($6), ($7) - 1) + 8.0d0 * $3(($5), ($6), ($7) + 1)) - $3(($5), ($6), ($7) + 2) + 1.0d0 * (((-$4(($5), ($6), ($7) - 2)) * ($2(($5), ($6), ($7) - 1) - $2(($5), ($6), ($7) - 2)) + 3.0d0 * $4(($5), ($6), ($7) - 1) * ($2(($5), ($6), ($7)) - $2(($5), ($6), ($7) - 1))) - 3.0d0 * $4(($5), ($6), ($7)) * ($2(($5), ($6), ($7) + 1) - $2(($5), ($6), ($7))) + $4(($5), ($6), ($7) + 1) * ($2(($5), ($6), ($7) + 2) - $2(($5), ($6), ($7) + 1)))))dnl
define(RK3P1,$3(($5), ($6), ($7)) + deltat * ($2))dnl
define(RK3P2,1.0d0 / 4.0d0 * (3.0d0 * $3(($5), ($6), ($7)) + $4(($5), ($6), ($7)) + deltat * ($2)))dnl
define(RK3P3,1.0d0 / 3.0d0 * ($3(($5), ($6), ($7)) + 2.0d0 * $4(($5), ($6), ($7)) + 2.0d0 * deltat * ($2)))dnl
define(Firho_PlaneI,($2))dnl
define(Fjrho_PlaneI,($2))dnl
define(Fkrho_PlaneI,($2))dnl
define(Fimx_PlaneI,(($3) * ($3)) / ($2) + ($4))dnl
define(Fjmx_PlaneI,(($3) * ($4)) / ($2))dnl
define(Fkmx_PlaneI,(($3) * ($4)) / ($2))dnl
define(Fimy_PlaneI,(($4) * ($3)) / ($2))dnl
define(Fjmy_PlaneI,(($3) * ($3)) / ($2) + ($4))dnl
define(Fkmy_PlaneI,(($3) * ($4)) / ($2))dnl
define(Fimz_PlaneI,(($4) * ($3)) / ($2))dnl
define(Fjmz_PlaneI,(($4) * ($3)) / ($2))dnl
define(Fkmz_PlaneI,(($3) * ($3)) / ($2) + ($4))dnl

#include "cctk.h"
#include "cctk_Parameters.h"
#include "cctk_Arguments.h"
#include "cctk_Functions.h"

	subroutine FDAsNozzle3D_execution(CCTK_ARGUMENTS)

		implicit none

		DECLARE_CCTK_ARGUMENTS
		DECLARE_CCTK_PARAMETERS
		DECLARE_CCTK_FUNCTIONS

!		Declare local variables
!		-----------------

		INTEGER i, iStart, iEnd
		INTEGER j, jStart, jEnd
		INTEGER k, kStart, kEnd
		INTEGER istat, status, ReflectionCounter
		CCTK_REAL fluxAccNull_3, Q_ii_mx_1, Q_ij_mx_1, Q_ik_mx_1, Q_ii2_mx_1, Q_jj_mx_1, Q_kk_mx_1, Q_ii3_mx_1, Q_ji_mx_1, Q_ki_mx_1, Q_ji_my_1, Q_jj_my_1, Q_jk_my_1, Q_ii_my_1, Q_jj2_my_1, Q_kk_my_1, Q_ij_my_1, Q_jj3_my_1, Q_kj_my_1, Q_ki_mz_1, Q_kj_mz_1, Q_kk_mz_1, Q_ii_mz_1, Q_jj_mz_1, Q_kk2_mz_1, Q_ik_mz_1, Q_jk_mz_1, Q_kk3_mz_1, Pm2_ii_mx_1, Pm2_ij_mx_1, Pm2_ik_mx_1, Pm2_ii2_mx_1, Pm2_jj_mx_1, Pm2_kk_mx_1, Pm2_ii3_mx_1, Pm2_ji_mx_1, Pm2_ki_mx_1, Pm2_ji_my_1, Pm2_jj_my_1, Pm2_jk_my_1, Pm2_ii_my_1, Pm2_jj2_my_1, Pm2_kk_my_1, Pm2_ij_my_1, Pm2_jj3_my_1, Pm2_kj_my_1, Pm2_ki_mz_1, Pm2_kj_mz_1, Pm2_kk_mz_1, Pm2_ii_mz_1, Pm2_jj_mz_1, Pm2_kk2_mz_1, Pm2_ik_mz_1, Pm2_jk_mz_1, Pm2_kk3_mz_1, Pm1_ii_mx_1, Pm1_ij_mx_1, Pm1_ik_mx_1, Pm1_ii2_mx_1, Pm1_jj_mx_1, Pm1_kk_mx_1, Pm1_ii3_mx_1, Pm1_ji_mx_1, Pm1_ki_mx_1, Pm1_ji_my_1, Pm1_jj_my_1, Pm1_jk_my_1, Pm1_ii_my_1, Pm1_jj2_my_1, Pm1_kk_my_1, Pm1_ij_my_1, Pm1_jj3_my_1, Pm1_kj_my_1, Pm1_ki_mz_1, Pm1_kj_mz_1, Pm1_kk_mz_1, Pm1_ii_mz_1, Pm1_jj_mz_1, Pm1_kk2_mz_1, Pm1_ik_mz_1, Pm1_jk_mz_1, Pm1_kk3_mz_1, Pp1_ii_mx_1, Pp1_ij_mx_1, Pp1_ik_mx_1, Pp1_ii2_mx_1, Pp1_jj_mx_1, Pp1_kk_mx_1, Pp1_ii3_mx_1, Pp1_ji_mx_1, Pp1_ki_mx_1, Pp1_ji_my_1, Pp1_jj_my_1, Pp1_jk_my_1, Pp1_ii_my_1, Pp1_jj2_my_1, Pp1_kk_my_1, Pp1_ij_my_1, Pp1_jj3_my_1, Pp1_kj_my_1, Pp1_ki_mz_1, Pp1_kj_mz_1, Pp1_kk_mz_1, Pp1_ii_mz_1, Pp1_jj_mz_1, Pp1_kk2_mz_1, Pp1_ik_mz_1, Pp1_jk_mz_1, Pp1_kk3_mz_1, Pp2_ii_mx_1, Pp2_ij_mx_1, Pp2_ik_mx_1, Pp2_ii2_mx_1, Pp2_jj_mx_1, Pp2_kk_mx_1, Pp2_ii3_mx_1, Pp2_ji_mx_1, Pp2_ki_mx_1, Pp2_ji_my_1, Pp2_jj_my_1, Pp2_jk_my_1, Pp2_ii_my_1, Pp2_jj2_my_1, Pp2_kk_my_1, Pp2_ij_my_1, Pp2_jj3_my_1, Pp2_kj_my_1, Pp2_ki_mz_1, Pp2_kj_mz_1, Pp2_kk_mz_1, Pp2_ii_mz_1, Pp2_jj_mz_1, Pp2_kk2_mz_1, Pp2_ik_mz_1, Pp2_jk_mz_1, Pp2_kk3_mz_1, m_n_1, m_tx_1, m_ty_1, m_tz_1, c_x_1, cn_x_1, c_y_1, cn_y_1, c_z_1, cn_z_1, fluxAccrho_1, fluxAccmx_1, fluxAccmy_1, fluxAccmz_1, n_x_1, n_y_1, n_z_1, interaction_index_1, mod_normal_1, fluxAccm_n_1, fluxAccm_tx_1, fluxAccm_ty_1, fluxAccm_tz_1, c_1, cn_1, fluxAccwnp_3_1, fluxAccwnm_3_1, fluxAccwtx_3_1, fluxAccwty_3_1, fluxAccwtz_3_1


		CCTK_INT exitCond
		CCTK_REAL deltax
		CCTK_REAL deltay
		CCTK_REAL deltaz
		CCTK_REAL deltat

		deltax = 0.26268500089645386d0/(cctk_gsh(1) - 1)
		deltay = 0.01600000075995922d0/(cctk_gsh(2) - 1)
		deltaz = 0.01600000075995922d0/(cctk_gsh(3) - 1)
		deltat = CCTK_DELTA_TIME

!		Finalization condition
		if (cctk_time .ge. tend) then 
			call CCTK_Exit(exitCond , cctkGH, 0)
		end if

!		Evolution
		iStart = 1
		iEnd = cctk_lsh(1)
		jStart = 1
		jEnd = cctk_lsh(2)
		kStart = 1
		kEnd = cctk_lsh(3)
		do k = kStart, kEnd
			do j = jStart, jEnd
				do i = iStart, iEnd
					if ((FOV_3(i, j, k) .gt. 0 .or. (((i + 1 .le. cctk_lsh(1) .and. FOV_3(i + 1, j, k) .gt. 0) .or. (i + 2 .le. cctk_lsh(1) .and. FOV_3(i + 2, j, k) .gt. 0) .or. (j + 1 .le. cctk_lsh(2) .and. FOV_3(i, j + 1, k) .gt. 0) .or. (j + 2 .le. cctk_lsh(2) .and. FOV_3(i, j + 2, k) .gt. 0) .or. (k + 1 .le. cctk_lsh(3) .and. FOV_3(i, j, k + 1) .gt. 0) .or. (k + 2 .le. cctk_lsh(3) .and. FOV_3(i, j, k + 2) .gt. 0)) .or. ((i - 1 .ge. 1 .and. FOV_3(i - 1, j, k) .gt. 0) .or. (i - 2 .ge. 1 .and. FOV_3(i - 2, j, k) .gt. 0) .or. (j - 1 .ge. 1 .and. FOV_3(i, j - 1, k) .gt. 0) .or. (j - 2 .ge. 1 .and. FOV_3(i, j - 2, k) .gt. 0) .or. (k - 1 .ge. 1 .and. FOV_3(i, j, k - 1) .gt. 0) .or. (k - 2 .ge. 1 .and. FOV_3(i, j, k - 2) .gt. 0))))) then
					if ((((i .ge. 3) .and. (i .le. (cctk_lsh(1) - 3))) .and. ((j .ge. 3) .and. (j .le. (cctk_lsh(2) - 3)))) .and. ((k .ge. 3) .and. (k .le. (cctk_lsh(3) - 3)))) then
					Flux_extsubNullsupi_3(i, j, k) = 0.0d0
					Flux_extsubNullsupj_3(i, j, k) = 0.0d0
					Flux_extsubNullsupk_3(i, j, k) = 0.0d0
					end if
					Speedi_3(i, j, k) = ABS(1.0d0)
					Speedj_3(i, j, k) = ABS(1.0d0)
					Speedk_3(i, j, k) = ABS(1.0d0)
					end if
					if ((FOV_1(i, j, k) .gt. 0 .or. (((i + 1 .le. cctk_lsh(1) .and. FOV_1(i + 1, j, k) .gt. 0) .or. (i + 2 .le. cctk_lsh(1) .and. FOV_1(i + 2, j, k) .gt. 0) .or. (j + 1 .le. cctk_lsh(2) .and. FOV_1(i, j + 1, k) .gt. 0) .or. (j + 2 .le. cctk_lsh(2) .and. FOV_1(i, j + 2, k) .gt. 0) .or. (k + 1 .le. cctk_lsh(3) .and. FOV_1(i, j, k + 1) .gt. 0) .or. (k + 2 .le. cctk_lsh(3) .and. FOV_1(i, j, k + 2) .gt. 0)) .or. ((i - 1 .ge. 1 .and. FOV_1(i - 1, j, k) .gt. 0) .or. (i - 2 .ge. 1 .and. FOV_1(i - 2, j, k) .gt. 0) .or. (j - 1 .ge. 1 .and. FOV_1(i, j - 1, k) .gt. 0) .or. (j - 2 .ge. 1 .and. FOV_1(i, j - 2, k) .gt. 0) .or. (k - 1 .ge. 1 .and. FOV_1(i, j, k - 1) .gt. 0) .or. (k - 2 .ge. 1 .and. FOV_1(i, j, k - 2) .gt. 0)) .or. (((i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1) .and. FOV_1(int(i + 1), int(j - 1), k) .gt. 0) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2)) .and. FOV_1(int(i + 1), int(j + 1), k) .gt. 0) .or. ((i - 1 .ge. 1 .and. j - 1 .ge. 1) .and. FOV_1(int(i - 1), int(j - 1), k) .gt. 0) .or. ((i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2)) .and. FOV_1(int(i - 1), int(j + 1), k) .gt. 0) .or. ((i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1) .and. FOV_1(int(i + 1), int(j - 2), k) .gt. 0) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2)) .and. FOV_1(int(i + 1), int(j + 2), k) .gt. 0) .or. ((i - 1 .ge. 1 .and. j - 2 .ge. 1) .and. FOV_1(int(i - 1), int(j - 2), k) .gt. 0) .or. ((i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2)) .and. FOV_1(int(i - 1), int(j + 2), k) .gt. 0) .or. ((i + 1 .le. cctk_lsh(1) .and. k - 1 .ge. 1) .and. FOV_1(int(i + 1), j, int(k - 1)) .gt. 0) .or. ((i + 1 .le. cctk_lsh(1) .and. k + 1 .le. cctk_lsh(3)) .and. FOV_1(int(i + 1), j, int(k + 1)) .gt. 0) .or. ((i - 1 .ge. 1 .and. k - 1 .ge. 1) .and. FOV_1(int(i - 1), j, int(k - 1)) .gt. 0) .or. ((i - 1 .ge. 1 .and. k + 1 .le. cctk_lsh(3)) .and. FOV_1(int(i - 1), j, int(k + 1)) .gt. 0) .or. ((i + 1 .le. cctk_lsh(1) .and. k - 2 .ge. 1) .and. FOV_1(int(i + 1), j, int(k - 2)) .gt. 0) .or. ((i + 1 .le. cctk_lsh(1) .and. k + 2 .le. cctk_lsh(3)) .and. FOV_1(int(i + 1), j, int(k + 2)) .gt. 0) .or. ((i - 1 .ge. 1 .and. k - 2 .ge. 1) .and. FOV_1(int(i - 1), j, int(k - 2)) .gt. 0) .or. ((i - 1 .ge. 1 .and. k + 2 .le. cctk_lsh(3)) .and. FOV_1(int(i - 1), j, int(k + 2)) .gt. 0) .or. ((i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1) .and. FOV_1(int(i + 2), int(j - 1), k) .gt. 0) .or. ((i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2)) .and. FOV_1(int(i + 2), int(j + 1), k) .gt. 0) .or. ((i - 2 .ge. 1 .and. j - 1 .ge. 1) .and. FOV_1(int(i - 2), int(j - 1), k) .gt. 0) .or. ((i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2)) .and. FOV_1(int(i - 2), int(j + 1), k) .gt. 0) .or. ((i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1) .and. FOV_1(int(i + 2), int(j - 2), k) .gt. 0) .or. ((i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2)) .and. FOV_1(int(i + 2), int(j + 2), k) .gt. 0) .or. ((i - 2 .ge. 1 .and. j - 2 .ge. 1) .and. FOV_1(int(i - 2), int(j - 2), k) .gt. 0) .or. ((i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2)) .and. FOV_1(int(i - 2), int(j + 2), k) .gt. 0) .or. ((i + 2 .le. cctk_lsh(1) .and. k - 1 .ge. 1) .and. FOV_1(int(i + 2), j, int(k - 1)) .gt. 0) .or. ((i + 2 .le. cctk_lsh(1) .and. k + 1 .le. cctk_lsh(3)) .and. FOV_1(int(i + 2), j, int(k + 1)) .gt. 0) .or. ((i - 2 .ge. 1 .and. k - 1 .ge. 1) .and. FOV_1(int(i - 2), j, int(k - 1)) .gt. 0) .or. ((i - 2 .ge. 1 .and. k + 1 .le. cctk_lsh(3)) .and. FOV_1(int(i - 2), j, int(k + 1)) .gt. 0) .or. ((i + 2 .le. cctk_lsh(1) .and. k - 2 .ge. 1) .and. FOV_1(int(i + 2), j, int(k - 2)) .gt. 0) .or. ((i + 2 .le. cctk_lsh(1) .and. k + 2 .le. cctk_lsh(3)) .and. FOV_1(int(i + 2), j, int(k + 2)) .gt. 0) .or. ((i - 2 .ge. 1 .and. k - 2 .ge. 1) .and. FOV_1(int(i - 2), j, int(k - 2)) .gt. 0) .or. ((i - 2 .ge. 1 .and. k + 2 .le. cctk_lsh(3)) .and. FOV_1(int(i - 2), j, int(k + 2)) .gt. 0) .or. ((j + 1 .le. cctk_lsh(2) .and. k - 1 .ge. 1) .and. FOV_1(i, int(j + 1), int(k - 1)) .gt. 0) .or. ((j + 1 .le. cctk_lsh(2) .and. k + 1 .le. cctk_lsh(3)) .and. FOV_1(i, int(j + 1), int(k + 1)) .gt. 0) .or. ((j - 1 .ge. 1 .and. k - 1 .ge. 1) .and. FOV_1(i, int(j - 1), int(k - 1)) .gt. 0) .or. ((j - 1 .ge. 1 .and. k + 1 .le. cctk_lsh(3)) .and. FOV_1(i, int(j - 1), int(k + 1)) .gt. 0) .or. ((j + 1 .le. cctk_lsh(2) .and. k - 2 .ge. 1) .and. FOV_1(i, int(j + 1), int(k - 2)) .gt. 0) .or. ((j + 1 .le. cctk_lsh(2) .and. k + 2 .le. cctk_lsh(3)) .and. FOV_1(i, int(j + 1), int(k + 2)) .gt. 0) .or. ((j - 1 .ge. 1 .and. k - 2 .ge. 1) .and. FOV_1(i, int(j - 1), int(k - 2)) .gt. 0) .or. ((j - 1 .ge. 1 .and. k + 2 .le. cctk_lsh(3)) .and. FOV_1(i, int(j - 1), int(k + 2)) .gt. 0) .or. ((j + 2 .le. cctk_lsh(2) .and. k - 1 .ge. 1) .and. FOV_1(i, int(j + 2), int(k - 1)) .gt. 0) .or. ((j + 2 .le. cctk_lsh(2) .and. k + 1 .le. cctk_lsh(3)) .and. FOV_1(i, int(j + 2), int(k + 1)) .gt. 0) .or. ((j - 2 .ge. 1 .and. k - 1 .ge. 1) .and. FOV_1(i, int(j - 2), int(k - 1)) .gt. 0) .or. ((j - 2 .ge. 1 .and. k + 1 .le. cctk_lsh(3)) .and. FOV_1(i, int(j - 2), int(k + 1)) .gt. 0) .or. ((j + 2 .le. cctk_lsh(2) .and. k - 2 .ge. 1) .and. FOV_1(i, int(j + 2), int(k - 2)) .gt. 0) .or. ((j + 2 .le. cctk_lsh(2) .and. k + 2 .le. cctk_lsh(3)) .and. FOV_1(i, int(j + 2), int(k + 2)) .gt. 0) .or. ((j - 2 .ge. 1 .and. k - 2 .ge. 1) .and. FOV_1(i, int(j - 2), int(k - 2)) .gt. 0) .or. ((j - 2 .ge. 1 .and. k + 2 .le. cctk_lsh(3)) .and. FOV_1(i, int(j - 2), int(k + 2)) .gt. 0))))) then
					if ((((i .ge. 3) .and. (i .le. (cctk_lsh(1) - 3))) .and. ((j .ge. 3) .and. (j .le. (cctk_lsh(2) - 3)))) .and. ((k .ge. 3) .and. (k .le. (cctk_lsh(3) - 3)))) then
					Flux_extsubrhosupi_1(i, j, k) = Firho_PlaneI(CCTK_PASS_FTOF, extrapolatedsubmxsupi(i, j, k))
					Flux_extsubrhosupj_1(i, j, k) = Fjrho_PlaneI(CCTK_PASS_FTOF, extrapolatedsubmysupj(i, j, k))
					Flux_extsubrhosupk_1(i, j, k) = Fkrho_PlaneI(CCTK_PASS_FTOF, extrapolatedsubmzsupk(i, j, k))
					Flux_extsubmxsupi_1(i, j, k) = Fimx_PlaneI(CCTK_PASS_FTOF, extrapolatedsubrhosupi(i, j, k), extrapolatedsubmxsupi(i, j, k), extrapolatedsubpsupi(i, j, k))
					Flux_extsubmxsupj_1(i, j, k) = Fjmx_PlaneI(CCTK_PASS_FTOF, extrapolatedsubrhosupj(i, j, k), extrapolatedsubmxsupj(i, j, k), extrapolatedsubmysupj(i, j, k))
					Flux_extsubmxsupk_1(i, j, k) = Fkmx_PlaneI(CCTK_PASS_FTOF, extrapolatedsubrhosupk(i, j, k), extrapolatedsubmxsupk(i, j, k), extrapolatedsubmzsupk(i, j, k))
					Flux_extsubmysupi_1(i, j, k) = Fimy_PlaneI(CCTK_PASS_FTOF, extrapolatedsubrhosupi(i, j, k), extrapolatedsubmxsupi(i, j, k), extrapolatedsubmysupi(i, j, k))
					Flux_extsubmysupj_1(i, j, k) = Fjmy_PlaneI(CCTK_PASS_FTOF, extrapolatedsubrhosupj(i, j, k), extrapolatedsubmysupj(i, j, k), extrapolatedsubpsupj(i, j, k))
					Flux_extsubmysupk_1(i, j, k) = Fkmy_PlaneI(CCTK_PASS_FTOF, extrapolatedsubrhosupk(i, j, k), extrapolatedsubmysupk(i, j, k), extrapolatedsubmzsupk(i, j, k))
					Flux_extsubmzsupi_1(i, j, k) = Fimz_PlaneI(CCTK_PASS_FTOF, extrapolatedsubrhosupi(i, j, k), extrapolatedsubmxsupi(i, j, k), extrapolatedsubmzsupi(i, j, k))
					Flux_extsubmzsupj_1(i, j, k) = Fjmz_PlaneI(CCTK_PASS_FTOF, extrapolatedsubrhosupj(i, j, k), extrapolatedsubmysupj(i, j, k), extrapolatedsubmzsupj(i, j, k))
					Flux_extsubmzsupk_1(i, j, k) = Fkmz_PlaneI(CCTK_PASS_FTOF, extrapolatedsubrhosupk(i, j, k), extrapolatedsubmzsupk(i, j, k), extrapolatedsubpsupk(i, j, k))
					Q_ii_mx_1 = lambda
					Q_ij_mx_1 = lambda
					Q_ik_mx_1 = lambda
					Q_ii2_mx_1 = mu
					Q_jj_mx_1 = mu
					Q_kk_mx_1 = mu
					Q_ii3_mx_1 = mu
					Q_ji_mx_1 = mu
					Q_ki_mx_1 = mu
					Q_ji_my_1 = lambda
					Q_jj_my_1 = lambda
					Q_jk_my_1 = lambda
					Q_ii_my_1 = mu
					Q_jj2_my_1 = mu
					Q_kk_my_1 = mu
					Q_ij_my_1 = mu
					Q_jj3_my_1 = mu
					Q_kj_my_1 = mu
					Q_ki_mz_1 = lambda
					Q_kj_mz_1 = lambda
					Q_kk_mz_1 = lambda
					Q_ii_mz_1 = mu
					Q_jj_mz_1 = mu
					Q_kk2_mz_1 = mu
					Q_ik_mz_1 = mu
					Q_jk_mz_1 = mu
					Q_kk3_mz_1 = mu
					Pm2_ii_mx_1 = mx_p(i - 2, j, k) / rho_p(i - 2, j, k)
					Pm2_ij_mx_1 = my_p(i, j - 2, k) / rho_p(i, j - 2, k)
					Pm2_ik_mx_1 = mz_p(i, j, k - 2) / rho_p(i, j, k - 2)
					Pm2_ii2_mx_1 = mx_p(i - 2, j, k) / rho_p(i - 2, j, k)
					Pm2_jj_mx_1 = mx_p(i, j - 2, k) / rho_p(i, j - 2, k)
					Pm2_kk_mx_1 = mx_p(i, j, k - 2) / rho_p(i, j, k - 2)
					Pm2_ii3_mx_1 = mx_p(i - 2, j, k) / rho_p(i - 2, j, k)
					Pm2_ji_mx_1 = my_p(i - 2, j, k) / rho_p(i - 2, j, k)
					Pm2_ki_mx_1 = mz_p(i - 2, j, k) / rho_p(i - 2, j, k)
					Pm2_ji_my_1 = mx_p(i - 2, j, k) / rho_p(i - 2, j, k)
					Pm2_jj_my_1 = my_p(i, j - 2, k) / rho_p(i, j - 2, k)
					Pm2_jk_my_1 = mz_p(i, j, k - 2) / rho_p(i, j, k - 2)
					Pm2_ii_my_1 = my_p(i - 2, j, k) / rho_p(i - 2, j, k)
					Pm2_jj2_my_1 = my_p(i, j - 2, k) / rho_p(i, j - 2, k)
					Pm2_kk_my_1 = my_p(i, j, k - 2) / rho_p(i, j, k - 2)
					Pm2_ij_my_1 = mx_p(i, j - 2, k) / rho_p(i, j - 2, k)
					Pm2_jj3_my_1 = my_p(i, j - 2, k) / rho_p(i, j - 2, k)
					Pm2_kj_my_1 = mz_p(i, j - 2, k) / rho_p(i, j - 2, k)
					Pm2_ki_mz_1 = mx_p(i - 2, j, k) / rho_p(i - 2, j, k)
					Pm2_kj_mz_1 = my_p(i, j - 2, k) / rho_p(i, j - 2, k)
					Pm2_kk_mz_1 = mz_p(i, j, k - 2) / rho_p(i, j, k - 2)
					Pm2_ii_mz_1 = mz_p(i - 2, j, k) / rho_p(i - 2, j, k)
					Pm2_jj_mz_1 = mz_p(i, j - 2, k) / rho_p(i, j - 2, k)
					Pm2_kk2_mz_1 = mz_p(i, j, k - 2) / rho_p(i, j, k - 2)
					Pm2_ik_mz_1 = mx_p(i, j, k - 2) / rho_p(i, j, k - 2)
					Pm2_jk_mz_1 = my_p(i, j, k - 2) / rho_p(i, j, k - 2)
					Pm2_kk3_mz_1 = mz_p(i, j, k - 2) / rho_p(i, j, k - 2)
					Pm1_ii_mx_1 = mx_p(i - 1, j, k) / rho_p(i - 1, j, k)
					Pm1_ij_mx_1 = my_p(i, j - 1, k) / rho_p(i, j - 1, k)
					Pm1_ik_mx_1 = mz_p(i, j, k - 1) / rho_p(i, j, k - 1)
					Pm1_ii2_mx_1 = mx_p(i - 1, j, k) / rho_p(i - 1, j, k)
					Pm1_jj_mx_1 = mx_p(i, j - 1, k) / rho_p(i, j - 1, k)
					Pm1_kk_mx_1 = mx_p(i, j, k - 1) / rho_p(i, j, k - 1)
					Pm1_ii3_mx_1 = mx_p(i - 1, j, k) / rho_p(i - 1, j, k)
					Pm1_ji_mx_1 = my_p(i - 1, j, k) / rho_p(i - 1, j, k)
					Pm1_ki_mx_1 = mz_p(i - 1, j, k) / rho_p(i - 1, j, k)
					Pm1_ji_my_1 = mx_p(i - 1, j, k) / rho_p(i - 1, j, k)
					Pm1_jj_my_1 = my_p(i, j - 1, k) / rho_p(i, j - 1, k)
					Pm1_jk_my_1 = mz_p(i, j, k - 1) / rho_p(i, j, k - 1)
					Pm1_ii_my_1 = my_p(i - 1, j, k) / rho_p(i - 1, j, k)
					Pm1_jj2_my_1 = my_p(i, j - 1, k) / rho_p(i, j - 1, k)
					Pm1_kk_my_1 = my_p(i, j, k - 1) / rho_p(i, j, k - 1)
					Pm1_ij_my_1 = mx_p(i, j - 1, k) / rho_p(i, j - 1, k)
					Pm1_jj3_my_1 = my_p(i, j - 1, k) / rho_p(i, j - 1, k)
					Pm1_kj_my_1 = mz_p(i, j - 1, k) / rho_p(i, j - 1, k)
					Pm1_ki_mz_1 = mx_p(i - 1, j, k) / rho_p(i - 1, j, k)
					Pm1_kj_mz_1 = my_p(i, j - 1, k) / rho_p(i, j - 1, k)
					Pm1_kk_mz_1 = mz_p(i, j, k - 1) / rho_p(i, j, k - 1)
					Pm1_ii_mz_1 = mz_p(i - 1, j, k) / rho_p(i - 1, j, k)
					Pm1_jj_mz_1 = mz_p(i, j - 1, k) / rho_p(i, j - 1, k)
					Pm1_kk2_mz_1 = mz_p(i, j, k - 1) / rho_p(i, j, k - 1)
					Pm1_ik_mz_1 = mx_p(i, j, k - 1) / rho_p(i, j, k - 1)
					Pm1_jk_mz_1 = my_p(i, j, k - 1) / rho_p(i, j, k - 1)
					Pm1_kk3_mz_1 = mz_p(i, j, k - 1) / rho_p(i, j, k - 1)
					Pp1_ii_mx_1 = mx_p(i + 1, j, k) / rho_p(i + 1, j, k)
					Pp1_ij_mx_1 = my_p(i, j + 1, k) / rho_p(i, j + 1, k)
					Pp1_ik_mx_1 = mz_p(i, j, k + 1) / rho_p(i, j, k + 1)
					Pp1_ii2_mx_1 = mx_p(i + 1, j, k) / rho_p(i + 1, j, k)
					Pp1_jj_mx_1 = mx_p(i, j + 1, k) / rho_p(i, j + 1, k)
					Pp1_kk_mx_1 = mx_p(i, j, k + 1) / rho_p(i, j, k + 1)
					Pp1_ii3_mx_1 = mx_p(i + 1, j, k) / rho_p(i + 1, j, k)
					Pp1_ji_mx_1 = my_p(i + 1, j, k) / rho_p(i + 1, j, k)
					Pp1_ki_mx_1 = mz_p(i + 1, j, k) / rho_p(i + 1, j, k)
					Pp1_ji_my_1 = mx_p(i + 1, j, k) / rho_p(i + 1, j, k)
					Pp1_jj_my_1 = my_p(i, j + 1, k) / rho_p(i, j + 1, k)
					Pp1_jk_my_1 = mz_p(i, j, k + 1) / rho_p(i, j, k + 1)
					Pp1_ii_my_1 = my_p(i + 1, j, k) / rho_p(i + 1, j, k)
					Pp1_jj2_my_1 = my_p(i, j + 1, k) / rho_p(i, j + 1, k)
					Pp1_kk_my_1 = my_p(i, j, k + 1) / rho_p(i, j, k + 1)
					Pp1_ij_my_1 = mx_p(i, j + 1, k) / rho_p(i, j + 1, k)
					Pp1_jj3_my_1 = my_p(i, j + 1, k) / rho_p(i, j + 1, k)
					Pp1_kj_my_1 = mz_p(i, j + 1, k) / rho_p(i, j + 1, k)
					Pp1_ki_mz_1 = mx_p(i + 1, j, k) / rho_p(i + 1, j, k)
					Pp1_kj_mz_1 = my_p(i, j + 1, k) / rho_p(i, j + 1, k)
					Pp1_kk_mz_1 = mz_p(i, j, k + 1) / rho_p(i, j, k + 1)
					Pp1_ii_mz_1 = mz_p(i + 1, j, k) / rho_p(i + 1, j, k)
					Pp1_jj_mz_1 = mz_p(i, j + 1, k) / rho_p(i, j + 1, k)
					Pp1_kk2_mz_1 = mz_p(i, j, k + 1) / rho_p(i, j, k + 1)
					Pp1_ik_mz_1 = mx_p(i, j, k + 1) / rho_p(i, j, k + 1)
					Pp1_jk_mz_1 = my_p(i, j, k + 1) / rho_p(i, j, k + 1)
					Pp1_kk3_mz_1 = mz_p(i, j, k + 1) / rho_p(i, j, k + 1)
					Pp2_ii_mx_1 = mx_p(i + 2, j, k) / rho_p(i + 2, j, k)
					Pp2_ij_mx_1 = my_p(i, j + 2, k) / rho_p(i, j + 2, k)
					Pp2_ik_mx_1 = mz_p(i, j, k + 2) / rho_p(i, j, k + 2)
					Pp2_ii2_mx_1 = mx_p(i + 2, j, k) / rho_p(i + 2, j, k)
					Pp2_jj_mx_1 = mx_p(i, j + 2, k) / rho_p(i, j + 2, k)
					Pp2_kk_mx_1 = mx_p(i, j, k + 2) / rho_p(i, j, k + 2)
					Pp2_ii3_mx_1 = mx_p(i + 2, j, k) / rho_p(i + 2, j, k)
					Pp2_ji_mx_1 = my_p(i + 2, j, k) / rho_p(i + 2, j, k)
					Pp2_ki_mx_1 = mz_p(i + 2, j, k) / rho_p(i + 2, j, k)
					Pp2_ji_my_1 = mx_p(i + 2, j, k) / rho_p(i + 2, j, k)
					Pp2_jj_my_1 = my_p(i, j + 2, k) / rho_p(i, j + 2, k)
					Pp2_jk_my_1 = mz_p(i, j, k + 2) / rho_p(i, j, k + 2)
					Pp2_ii_my_1 = my_p(i + 2, j, k) / rho_p(i + 2, j, k)
					Pp2_jj2_my_1 = my_p(i, j + 2, k) / rho_p(i, j + 2, k)
					Pp2_kk_my_1 = my_p(i, j, k + 2) / rho_p(i, j, k + 2)
					Pp2_ij_my_1 = mx_p(i, j + 2, k) / rho_p(i, j + 2, k)
					Pp2_jj3_my_1 = my_p(i, j + 2, k) / rho_p(i, j + 2, k)
					Pp2_kj_my_1 = mz_p(i, j + 2, k) / rho_p(i, j + 2, k)
					Pp2_ki_mz_1 = mx_p(i + 2, j, k) / rho_p(i + 2, j, k)
					Pp2_kj_mz_1 = my_p(i, j + 2, k) / rho_p(i, j + 2, k)
					Pp2_kk_mz_1 = mz_p(i, j, k + 2) / rho_p(i, j, k + 2)
					Pp2_ii_mz_1 = mz_p(i + 2, j, k) / rho_p(i + 2, j, k)
					Pp2_jj_mz_1 = mz_p(i, j + 2, k) / rho_p(i, j + 2, k)
					Pp2_kk2_mz_1 = mz_p(i, j, k + 2) / rho_p(i, j, k + 2)
					Pp2_ik_mz_1 = mx_p(i, j, k + 2) / rho_p(i, j, k + 2)
					Pp2_jk_mz_1 = my_p(i, j, k + 2) / rho_p(i, j, k + 2)
					Pp2_kk3_mz_1 = mz_p(i, j, k + 2) / rho_p(i, j, k + 2)
					Flux_extsubmxsupi_1(i, j, k) = Flux_extsubmxsupi_1(i, j, k) - Q_ii_mx_1 * CDi(CCTK_PASS_FTOF, Pm2_ii_mx_1, Pm1_ii_mx_1, Pp1_ii_mx_1, Pp2_ii_mx_1)
					Flux_extsubmxsupi_1(i, j, k) = Flux_extsubmxsupi_1(i, j, k) - Q_ij_mx_1 * CDj(CCTK_PASS_FTOF, Pm2_ij_mx_1, Pm1_ij_mx_1, Pp1_ij_mx_1, Pp2_ij_mx_1)
					Flux_extsubmxsupi_1(i, j, k) = Flux_extsubmxsupi_1(i, j, k) - Q_ik_mx_1 * CDk(CCTK_PASS_FTOF, Pm2_ik_mx_1, Pm1_ik_mx_1, Pp1_ik_mx_1, Pp2_ik_mx_1)
					Flux_extsubmxsupi_1(i, j, k) = Flux_extsubmxsupi_1(i, j, k) - Q_ii2_mx_1 * CDi(CCTK_PASS_FTOF, Pm2_ii2_mx_1, Pm1_ii2_mx_1, Pp1_ii2_mx_1, Pp2_ii2_mx_1)
					Flux_extsubmxsupj_1(i, j, k) = Flux_extsubmxsupj_1(i, j, k) - Q_jj_mx_1 * CDj(CCTK_PASS_FTOF, Pm2_jj_mx_1, Pm1_jj_mx_1, Pp1_jj_mx_1, Pp2_jj_mx_1)
					Flux_extsubmxsupk_1(i, j, k) = Flux_extsubmxsupk_1(i, j, k) - Q_kk_mx_1 * CDk(CCTK_PASS_FTOF, Pm2_kk_mx_1, Pm1_kk_mx_1, Pp1_kk_mx_1, Pp2_kk_mx_1)
					Flux_extsubmxsupi_1(i, j, k) = Flux_extsubmxsupi_1(i, j, k) - Q_ii3_mx_1 * CDi(CCTK_PASS_FTOF, Pm2_ii3_mx_1, Pm1_ii3_mx_1, Pp1_ii3_mx_1, Pp2_ii3_mx_1)
					Flux_extsubmxsupj_1(i, j, k) = Flux_extsubmxsupj_1(i, j, k) - Q_ji_mx_1 * CDi(CCTK_PASS_FTOF, Pm2_ji_mx_1, Pm1_ji_mx_1, Pp1_ji_mx_1, Pp2_ji_mx_1)
					Flux_extsubmxsupk_1(i, j, k) = Flux_extsubmxsupk_1(i, j, k) - Q_ki_mx_1 * CDi(CCTK_PASS_FTOF, Pm2_ki_mx_1, Pm1_ki_mx_1, Pp1_ki_mx_1, Pp2_ki_mx_1)
					Flux_extsubmysupj_1(i, j, k) = Flux_extsubmysupj_1(i, j, k) - Q_ji_my_1 * CDi(CCTK_PASS_FTOF, Pm2_ji_my_1, Pm1_ji_my_1, Pp1_ji_my_1, Pp2_ji_my_1)
					Flux_extsubmysupj_1(i, j, k) = Flux_extsubmysupj_1(i, j, k) - Q_jj_my_1 * CDj(CCTK_PASS_FTOF, Pm2_jj_my_1, Pm1_jj_my_1, Pp1_jj_my_1, Pp2_jj_my_1)
					Flux_extsubmysupj_1(i, j, k) = Flux_extsubmysupj_1(i, j, k) - Q_jk_my_1 * CDk(CCTK_PASS_FTOF, Pm2_jk_my_1, Pm1_jk_my_1, Pp1_jk_my_1, Pp2_jk_my_1)
					Flux_extsubmysupi_1(i, j, k) = Flux_extsubmysupi_1(i, j, k) - Q_ii_my_1 * CDi(CCTK_PASS_FTOF, Pm2_ii_my_1, Pm1_ii_my_1, Pp1_ii_my_1, Pp2_ii_my_1)
					Flux_extsubmysupj_1(i, j, k) = Flux_extsubmysupj_1(i, j, k) - Q_jj2_my_1 * CDj(CCTK_PASS_FTOF, Pm2_jj2_my_1, Pm1_jj2_my_1, Pp1_jj2_my_1, Pp2_jj2_my_1)
					Flux_extsubmysupk_1(i, j, k) = Flux_extsubmysupk_1(i, j, k) - Q_kk_my_1 * CDk(CCTK_PASS_FTOF, Pm2_kk_my_1, Pm1_kk_my_1, Pp1_kk_my_1, Pp2_kk_my_1)
					Flux_extsubmysupi_1(i, j, k) = Flux_extsubmysupi_1(i, j, k) - Q_ij_my_1 * CDj(CCTK_PASS_FTOF, Pm2_ij_my_1, Pm1_ij_my_1, Pp1_ij_my_1, Pp2_ij_my_1)
					Flux_extsubmysupj_1(i, j, k) = Flux_extsubmysupj_1(i, j, k) - Q_jj3_my_1 * CDj(CCTK_PASS_FTOF, Pm2_jj3_my_1, Pm1_jj3_my_1, Pp1_jj3_my_1, Pp2_jj3_my_1)
					Flux_extsubmysupk_1(i, j, k) = Flux_extsubmysupk_1(i, j, k) - Q_kj_my_1 * CDj(CCTK_PASS_FTOF, Pm2_kj_my_1, Pm1_kj_my_1, Pp1_kj_my_1, Pp2_kj_my_1)
					Flux_extsubmzsupk_1(i, j, k) = Flux_extsubmzsupk_1(i, j, k) - Q_ki_mz_1 * CDi(CCTK_PASS_FTOF, Pm2_ki_mz_1, Pm1_ki_mz_1, Pp1_ki_mz_1, Pp2_ki_mz_1)
					Flux_extsubmzsupk_1(i, j, k) = Flux_extsubmzsupk_1(i, j, k) - Q_kj_mz_1 * CDj(CCTK_PASS_FTOF, Pm2_kj_mz_1, Pm1_kj_mz_1, Pp1_kj_mz_1, Pp2_kj_mz_1)
					Flux_extsubmzsupk_1(i, j, k) = Flux_extsubmzsupk_1(i, j, k) - Q_kk_mz_1 * CDk(CCTK_PASS_FTOF, Pm2_kk_mz_1, Pm1_kk_mz_1, Pp1_kk_mz_1, Pp2_kk_mz_1)
					Flux_extsubmzsupi_1(i, j, k) = Flux_extsubmzsupi_1(i, j, k) - Q_ii_mz_1 * CDi(CCTK_PASS_FTOF, Pm2_ii_mz_1, Pm1_ii_mz_1, Pp1_ii_mz_1, Pp2_ii_mz_1)
					Flux_extsubmzsupj_1(i, j, k) = Flux_extsubmzsupj_1(i, j, k) - Q_jj_mz_1 * CDj(CCTK_PASS_FTOF, Pm2_jj_mz_1, Pm1_jj_mz_1, Pp1_jj_mz_1, Pp2_jj_mz_1)
					Flux_extsubmzsupk_1(i, j, k) = Flux_extsubmzsupk_1(i, j, k) - Q_kk2_mz_1 * CDk(CCTK_PASS_FTOF, Pm2_kk2_mz_1, Pm1_kk2_mz_1, Pp1_kk2_mz_1, Pp2_kk2_mz_1)
					Flux_extsubmzsupi_1(i, j, k) = Flux_extsubmzsupi_1(i, j, k) - Q_ik_mz_1 * CDk(CCTK_PASS_FTOF, Pm2_ik_mz_1, Pm1_ik_mz_1, Pp1_ik_mz_1, Pp2_ik_mz_1)
					Flux_extsubmzsupj_1(i, j, k) = Flux_extsubmzsupj_1(i, j, k) - Q_jk_mz_1 * CDk(CCTK_PASS_FTOF, Pm2_jk_mz_1, Pm1_jk_mz_1, Pp1_jk_mz_1, Pp2_jk_mz_1)
					Flux_extsubmzsupk_1(i, j, k) = Flux_extsubmzsupk_1(i, j, k) - Q_kk3_mz_1 * CDk(CCTK_PASS_FTOF, Pm2_kk3_mz_1, Pm1_kk3_mz_1, Pp1_kk3_mz_1, Pp2_kk3_mz_1)
					end if
					m_n_1 = extrapolatedsubmxsupi(i, j, k)
					m_tx_1 = 0.0d0
					m_ty_1 = extrapolatedsubmysupi(i, j, k)
					m_tz_1 = extrapolatedsubmzsupi(i, j, k)
					c_x_1 = c0 * ((extrapolatedsubrhosupi(i, j, k) / rho0) ** ((gamma + (-1.0d0)) / 2.0d0))
					cn_x_1 = m_n_1 / extrapolatedsubrhosupi(i, j, k)
					m_n_1 = extrapolatedsubmysupj(i, j, k)
					m_tx_1 = extrapolatedsubmxsupj(i, j, k)
					m_ty_1 = 0.0d0
					m_tz_1 = extrapolatedsubmzsupj(i, j, k)
					c_y_1 = c0 * ((extrapolatedsubrhosupj(i, j, k) / rho0) ** ((gamma + (-1.0d0)) / 2.0d0))
					cn_y_1 = m_n_1 / extrapolatedsubrhosupj(i, j, k)
					m_n_1 = extrapolatedsubmzsupk(i, j, k)
					m_tx_1 = extrapolatedsubmxsupk(i, j, k)
					m_ty_1 = extrapolatedsubmysupk(i, j, k)
					m_tz_1 = 0.0d0
					c_z_1 = c0 * ((extrapolatedsubrhosupk(i, j, k) / rho0) ** ((gamma + (-1.0d0)) / 2.0d0))
					cn_z_1 = m_n_1 / extrapolatedsubrhosupk(i, j, k)
					Speedi_1(i, j, k) = MAX(ABS(c_x_1 + cn_x_1), ABS((-c_x_1) + cn_x_1), ABS(cn_x_1), ABS(cn_x_1), ABS(cn_x_1))
					Speedj_1(i, j, k) = MAX(ABS(c_y_1 + cn_y_1), ABS((-c_y_1) + cn_y_1), ABS(cn_y_1), ABS(cn_y_1), ABS(cn_y_1))
					Speedk_1(i, j, k) = MAX(ABS(c_z_1 + cn_z_1), ABS((-c_z_1) + cn_z_1), ABS(cn_z_1), ABS(cn_z_1), ABS(cn_z_1))
					end if
				end do
			end do
		end do
		do k = kStart, kEnd
			do j = jStart, jEnd
				do i = iStart, iEnd
!					Boundaries
		if ((FOV_xLower(i, j, k) .gt. 0) .and. ((i + 1 .le. cctk_lsh(1) .and. (FOV_3(int(i + 1), j, k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. (FOV_3(int(i + 2), j, k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. (FOV_3(int(i + 3), j, k) .gt. 0)))) then
				Flux_extsubNullsupi_3(4 - i, j, k) = 2.0d0 * Flux_extsubNullsupi_3(4 - i + 1, j, k) - Flux_extsubNullsupi_3(4 - i + 2, j, k)
		end if
		if ((FOV_xUpper(i, j, k) .gt. 0) .and. ((i - 1 .ge. 1 .and. (FOV_3(int(i - 1), j, k) .gt. 0)) .or. (i - 2 .ge. 1 .and. (FOV_3(int(i - 2), j, k) .gt. 0)) .or. (i - 3 .ge. 1 .and. (FOV_3(int(i - 3), j, k) .gt. 0)))) then
			Flux_extsubNullsupi_3(i, j, k) = 2.0d0 * Flux_extsubNullsupi_3(i - 1, j, k) - Flux_extsubNullsupi_3(i - 2, j, k)
		end if
		if ((FOV_yLower(i, j, k) .gt. 0) .and. ((j + 1 .le. cctk_lsh(2) .and. (FOV_3(i, int(j + 1), k) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. (FOV_3(i, int(j + 2), k) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. (FOV_3(i, int(j + 3), k) .gt. 0)))) then
				Flux_extsubNullsupj_3(i, 4 - j, k) = 2.0d0 * Flux_extsubNullsupj_3(i, 4 - j + 1, k) - Flux_extsubNullsupj_3(i, 4 - j + 2, k)
		end if
		if ((FOV_yUpper(i, j, k) .gt. 0) .and. ((j - 1 .ge. 1 .and. (FOV_3(i, int(j - 1), k) .gt. 0)) .or. (j - 2 .ge. 1 .and. (FOV_3(i, int(j - 2), k) .gt. 0)) .or. (j - 3 .ge. 1 .and. (FOV_3(i, int(j - 3), k) .gt. 0)))) then
			Flux_extsubNullsupj_3(i, j, k) = 2.0d0 * Flux_extsubNullsupj_3(i, j - 1, k) - Flux_extsubNullsupj_3(i, j - 2, k)
		end if
		if ((FOV_zLower(i, j, k) .gt. 0) .and. ((k + 1 .le. cctk_lsh(3) .and. (FOV_3(i, j, int(k + 1)) .gt. 0)) .or. (k + 2 .le. cctk_lsh(3) .and. (FOV_3(i, j, int(k + 2)) .gt. 0)) .or. (k + 3 .le. cctk_lsh(3) .and. (FOV_3(i, j, int(k + 3)) .gt. 0)))) then
				Flux_extsubNullsupk_3(i, j, 4 - k) = 2.0d0 * Flux_extsubNullsupk_3(i, j, 4 - k + 1) - Flux_extsubNullsupk_3(i, j, 4 - k + 2)
		end if
		if ((FOV_zUpper(i, j, k) .gt. 0) .and. ((k - 1 .ge. 1 .and. (FOV_3(i, j, int(k - 1)) .gt. 0)) .or. (k - 2 .ge. 1 .and. (FOV_3(i, j, int(k - 2)) .gt. 0)) .or. (k - 3 .ge. 1 .and. (FOV_3(i, j, int(k - 3)) .gt. 0)))) then
			Flux_extsubNullsupk_3(i, j, k) = 2.0d0 * Flux_extsubNullsupk_3(i, j, k - 1) - Flux_extsubNullsupk_3(i, j, k - 2)
		end if
		if (FOV_3(i, j, k) .gt. 0) then
			if ((d_i_rho(i, j, k) .ne. 0 .or. d_j_rho(i, j, k) .ne. 0 .or. d_k_rho(i, j, k) .ne. 0)) then
				if (FOV_1(int(i - d_i_rho(i, j, k)), int(j - d_j_rho(i, j, k)), int(k - d_k_rho(i, j, k))) .gt. 0) then
					call extrapolate_flux(CCTK_PASS_FTOF, d_i_rho, Flux_extsubrhosupi_1, Flux_extsubrhosupi_1, i, d_j_rho, Flux_extsubrhosupj_1, Flux_extsubrhosupj_1, j, d_k_rho, Flux_extsubrhosupk_1, Flux_extsubrhosupk_1, k, FOV_1)
				end if
			end if
			if ((d_i_mx(i, j, k) .ne. 0 .or. d_j_mx(i, j, k) .ne. 0 .or. d_k_mx(i, j, k) .ne. 0)) then
				if (FOV_1(int(i - d_i_mx(i, j, k)), int(j - d_j_mx(i, j, k)), int(k - d_k_mx(i, j, k))) .gt. 0) then
					call extrapolate_flux(CCTK_PASS_FTOF, d_i_mx, Flux_extsubmxsupi_1, Flux_extsubmxsupi_1, i, d_j_mx, Flux_extsubmxsupj_1, Flux_extsubmxsupj_1, j, d_k_mx, Flux_extsubmxsupk_1, Flux_extsubmxsupk_1, k, FOV_1)
				end if
			end if
			if ((d_i_my(i, j, k) .ne. 0 .or. d_j_my(i, j, k) .ne. 0 .or. d_k_my(i, j, k) .ne. 0)) then
				if (FOV_1(int(i - d_i_my(i, j, k)), int(j - d_j_my(i, j, k)), int(k - d_k_my(i, j, k))) .gt. 0) then
					call extrapolate_flux(CCTK_PASS_FTOF, d_i_my, Flux_extsubmysupi_1, Flux_extsubmysupi_1, i, d_j_my, Flux_extsubmysupj_1, Flux_extsubmysupj_1, j, d_k_my, Flux_extsubmysupk_1, Flux_extsubmysupk_1, k, FOV_1)
				end if
			end if
			if ((d_i_mz(i, j, k) .ne. 0 .or. d_j_mz(i, j, k) .ne. 0 .or. d_k_mz(i, j, k) .ne. 0)) then
				if (FOV_1(int(i - d_i_mz(i, j, k)), int(j - d_j_mz(i, j, k)), int(k - d_k_mz(i, j, k))) .gt. 0) then
					call extrapolate_flux(CCTK_PASS_FTOF, d_i_mz, Flux_extsubmzsupi_1, Flux_extsubmzsupi_1, i, d_j_mz, Flux_extsubmzsupj_1, Flux_extsubmzsupj_1, j, d_k_mz, Flux_extsubmzsupk_1, Flux_extsubmzsupk_1, k, FOV_1)
				end if
			end if
		end if
!					Boundaries
		if ((FOV_xLower(i, j, k) .gt. 0) .and. ((i + 1 .le. cctk_lsh(1) .and. (FOV_1(int(i + 1), j, k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. (FOV_1(int(i + 2), j, k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. (FOV_1(int(i + 3), j, k) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 1), k) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 1), k) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 2), k) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 2), k) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 3), k) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 3), k) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(int(i + 1), j, int(k + 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. k - 1 .ge. 1 .and. (FOV_1(int(i + 1), j, int(k - 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(int(i + 1), j, int(k + 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. k - 2 .ge. 1 .and. (FOV_1(int(i + 1), j, int(k - 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(int(i + 1), j, int(k + 3)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. k - 3 .ge. 1 .and. (FOV_1(int(i + 1), j, int(k - 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 1), k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 1), k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 2), k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 2), k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 3), k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 3), k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(int(i + 2), j, int(k + 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. k - 1 .ge. 1 .and. (FOV_1(int(i + 2), j, int(k - 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(int(i + 2), j, int(k + 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. k - 2 .ge. 1 .and. (FOV_1(int(i + 2), j, int(k - 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(int(i + 2), j, int(k + 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. k - 3 .ge. 1 .and. (FOV_1(int(i + 2), j, int(k - 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 1), k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 1), k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 2), k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 2), k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 3), k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 3), k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(int(i + 3), j, int(k + 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. k - 1 .ge. 1 .and. (FOV_1(int(i + 3), j, int(k - 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(int(i + 3), j, int(k + 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. k - 2 .ge. 1 .and. (FOV_1(int(i + 3), j, int(k - 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(int(i + 3), j, int(k + 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. k - 3 .ge. 1 .and. (FOV_1(int(i + 3), j, int(k - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j, k) .ne. 0 .or. d_j_rho(i, j, k) .ne. 0 .or. d_k_rho(i, j, k) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_rho, Flux_extsubrhosupi_1, Flux_extsubrhosupi_1, i, d_j_rho, Flux_extsubrhosupj_1, Flux_extsubrhosupj_1, j, d_k_rho, Flux_extsubrhosupk_1, Flux_extsubrhosupk_1, k, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j, k) .ne. 0 .or. d_j_mx(i, j, k) .ne. 0 .or. d_k_mx(i, j, k) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_mx, Flux_extsubmxsupi_1, Flux_extsubmxsupi_1, i, d_j_mx, Flux_extsubmxsupj_1, Flux_extsubmxsupj_1, j, d_k_mx, Flux_extsubmxsupk_1, Flux_extsubmxsupk_1, k, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j, k) .ne. 0 .or. d_j_my(i, j, k) .ne. 0 .or. d_k_my(i, j, k) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_my, Flux_extsubmysupi_1, Flux_extsubmysupi_1, i, d_j_my, Flux_extsubmysupj_1, Flux_extsubmysupj_1, j, d_k_my, Flux_extsubmysupk_1, Flux_extsubmysupk_1, k, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mz(i, j, k) .ne. 0 .or. d_j_mz(i, j, k) .ne. 0 .or. d_k_mz(i, j, k) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_mz, Flux_extsubmzsupi_1, Flux_extsubmzsupi_1, i, d_j_mz, Flux_extsubmzsupj_1, Flux_extsubmzsupj_1, j, d_k_mz, Flux_extsubmzsupk_1, Flux_extsubmzsupk_1, k, FOV_1)
			end if
		end if
		if ((FOV_xUpper(i, j, k) .gt. 0) .and. ((i - 1 .ge. 1 .and. (FOV_1(int(i - 1), j, k) .gt. 0)) .or. (i - 2 .ge. 1 .and. (FOV_1(int(i - 2), j, k) .gt. 0)) .or. (i - 3 .ge. 1 .and. (FOV_1(int(i - 3), j, k) .gt. 0)) .or. ((i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 1), k) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 1), k) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 2), k) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 2), k) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 3), k) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 3), k) .gt. 0)) .or. (i - 1 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(int(i - 1), j, int(k + 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. k - 1 .ge. 1 .and. (FOV_1(int(i - 1), j, int(k - 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(int(i - 1), j, int(k + 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. k - 2 .ge. 1 .and. (FOV_1(int(i - 1), j, int(k - 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(int(i - 1), j, int(k + 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. k - 3 .ge. 1 .and. (FOV_1(int(i - 1), j, int(k - 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 1), k) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 1), k) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 2), k) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 2), k) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 3), k) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 3), k) .gt. 0)) .or. (i - 2 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(int(i - 2), j, int(k + 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. k - 1 .ge. 1 .and. (FOV_1(int(i - 2), j, int(k - 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(int(i - 2), j, int(k + 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. k - 2 .ge. 1 .and. (FOV_1(int(i - 2), j, int(k - 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(int(i - 2), j, int(k + 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. k - 3 .ge. 1 .and. (FOV_1(int(i - 2), j, int(k - 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 1), k) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 1), k) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 2), k) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 2), k) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 3), k) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 3), k) .gt. 0)) .or. (i - 3 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(int(i - 3), j, int(k + 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. k - 1 .ge. 1 .and. (FOV_1(int(i - 3), j, int(k - 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(int(i - 3), j, int(k + 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. k - 2 .ge. 1 .and. (FOV_1(int(i - 3), j, int(k - 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(int(i - 3), j, int(k + 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. k - 3 .ge. 1 .and. (FOV_1(int(i - 3), j, int(k - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j, k) .ne. 0 .or. d_j_rho(i, j, k) .ne. 0 .or. d_k_rho(i, j, k) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_rho, Flux_extsubrhosupi_1, Flux_extsubrhosupi_1, i, d_j_rho, Flux_extsubrhosupj_1, Flux_extsubrhosupj_1, j, d_k_rho, Flux_extsubrhosupk_1, Flux_extsubrhosupk_1, k, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j, k) .ne. 0 .or. d_j_mx(i, j, k) .ne. 0 .or. d_k_mx(i, j, k) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_mx, Flux_extsubmxsupi_1, Flux_extsubmxsupi_1, i, d_j_mx, Flux_extsubmxsupj_1, Flux_extsubmxsupj_1, j, d_k_mx, Flux_extsubmxsupk_1, Flux_extsubmxsupk_1, k, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j, k) .ne. 0 .or. d_j_my(i, j, k) .ne. 0 .or. d_k_my(i, j, k) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_my, Flux_extsubmysupi_1, Flux_extsubmysupi_1, i, d_j_my, Flux_extsubmysupj_1, Flux_extsubmysupj_1, j, d_k_my, Flux_extsubmysupk_1, Flux_extsubmysupk_1, k, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mz(i, j, k) .ne. 0 .or. d_j_mz(i, j, k) .ne. 0 .or. d_k_mz(i, j, k) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_mz, Flux_extsubmzsupi_1, Flux_extsubmzsupi_1, i, d_j_mz, Flux_extsubmzsupj_1, Flux_extsubmzsupj_1, j, d_k_mz, Flux_extsubmzsupk_1, Flux_extsubmzsupk_1, k, FOV_1)
			end if
		end if
		if ((FOV_yLower(i, j, k) .gt. 0) .and. ((j + 1 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 1), k) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 2), k) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 3), k) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 1), k) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 1), k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 1), k) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 1), k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 1), k) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 1), k) .gt. 0)) .or. (j + 1 .le. cctk_lsh(2) .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 1), int(k + 1)) .gt. 0)) .or. (j + 1 .le. cctk_lsh(2) .and. k - 1 .ge. 1 .and. (FOV_1(i, int(j + 1), int(k - 1)) .gt. 0)) .or. (j + 1 .le. cctk_lsh(2) .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 1), int(k + 2)) .gt. 0)) .or. (j + 1 .le. cctk_lsh(2) .and. k - 2 .ge. 1 .and. (FOV_1(i, int(j + 1), int(k - 2)) .gt. 0)) .or. (j + 1 .le. cctk_lsh(2) .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 1), int(k + 3)) .gt. 0)) .or. (j + 1 .le. cctk_lsh(2) .and. k - 3 .ge. 1 .and. (FOV_1(i, int(j + 1), int(k - 3)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 2), k) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 2), k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 2), k) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 2), k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 2), k) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 2), k) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 2), int(k + 1)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. k - 1 .ge. 1 .and. (FOV_1(i, int(j + 2), int(k - 1)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 2), int(k + 2)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. k - 2 .ge. 1 .and. (FOV_1(i, int(j + 2), int(k - 2)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 2), int(k + 3)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. k - 3 .ge. 1 .and. (FOV_1(i, int(j + 2), int(k - 3)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 3), k) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 3), k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 3), k) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 3), k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 3), k) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 3), k) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 3), int(k + 1)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. k - 1 .ge. 1 .and. (FOV_1(i, int(j + 3), int(k - 1)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 3), int(k + 2)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. k - 2 .ge. 1 .and. (FOV_1(i, int(j + 3), int(k - 2)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 3), int(k + 3)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. k - 3 .ge. 1 .and. (FOV_1(i, int(j + 3), int(k - 3)) .gt. 0))))) then
				Flux_extsubrhosupj_1(i, 4 - j, k) = 2.0d0 * Flux_extsubrhosupj_1(i, 4 - j + 1, k) - Flux_extsubrhosupj_1(i, 4 - j + 2, k)
				Flux_extsubmxsupj_1(i, 4 - j, k) = 2.0d0 * Flux_extsubmxsupj_1(i, 4 - j + 1, k) - Flux_extsubmxsupj_1(i, 4 - j + 2, k)
				Flux_extsubmysupj_1(i, 4 - j, k) = 2.0d0 * Flux_extsubmysupj_1(i, 4 - j + 1, k) - Flux_extsubmysupj_1(i, 4 - j + 2, k)
				Flux_extsubmzsupj_1(i, 4 - j, k) = 2.0d0 * Flux_extsubmzsupj_1(i, 4 - j + 1, k) - Flux_extsubmzsupj_1(i, 4 - j + 2, k)
		end if
		if ((FOV_yUpper(i, j, k) .gt. 0) .and. ((j - 1 .ge. 1 .and. (FOV_1(i, int(j - 1), k) .gt. 0)) .or. (j - 2 .ge. 1 .and. (FOV_1(i, int(j - 2), k) .gt. 0)) .or. (j - 3 .ge. 1 .and. (FOV_1(i, int(j - 3), k) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 1), k) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 1), k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 1), k) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 1), k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 1), k) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 1), k) .gt. 0)) .or. (j - 1 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 1), int(k + 1)) .gt. 0)) .or. (j - 1 .ge. 1 .and. k - 1 .ge. 1 .and. (FOV_1(i, int(j - 1), int(k - 1)) .gt. 0)) .or. (j - 1 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 1), int(k + 2)) .gt. 0)) .or. (j - 1 .ge. 1 .and. k - 2 .ge. 1 .and. (FOV_1(i, int(j - 1), int(k - 2)) .gt. 0)) .or. (j - 1 .ge. 1 .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 1), int(k + 3)) .gt. 0)) .or. (j - 1 .ge. 1 .and. k - 3 .ge. 1 .and. (FOV_1(i, int(j - 1), int(k - 3)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 2), k) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 2), k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 2), k) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 2), k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 2), k) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 2), k) .gt. 0)) .or. (j - 2 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 2), int(k + 1)) .gt. 0)) .or. (j - 2 .ge. 1 .and. k - 1 .ge. 1 .and. (FOV_1(i, int(j - 2), int(k - 1)) .gt. 0)) .or. (j - 2 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 2), int(k + 2)) .gt. 0)) .or. (j - 2 .ge. 1 .and. k - 2 .ge. 1 .and. (FOV_1(i, int(j - 2), int(k - 2)) .gt. 0)) .or. (j - 2 .ge. 1 .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 2), int(k + 3)) .gt. 0)) .or. (j - 2 .ge. 1 .and. k - 3 .ge. 1 .and. (FOV_1(i, int(j - 2), int(k - 3)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 3), k) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 3), k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 3), k) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 3), k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 3), k) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 3), k) .gt. 0)) .or. (j - 3 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 3), int(k + 1)) .gt. 0)) .or. (j - 3 .ge. 1 .and. k - 1 .ge. 1 .and. (FOV_1(i, int(j - 3), int(k - 1)) .gt. 0)) .or. (j - 3 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 3), int(k + 2)) .gt. 0)) .or. (j - 3 .ge. 1 .and. k - 2 .ge. 1 .and. (FOV_1(i, int(j - 3), int(k - 2)) .gt. 0)) .or. (j - 3 .ge. 1 .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 3), int(k + 3)) .gt. 0)) .or. (j - 3 .ge. 1 .and. k - 3 .ge. 1 .and. (FOV_1(i, int(j - 3), int(k - 3)) .gt. 0))))) then
			Flux_extsubrhosupj_1(i, j, k) = 2.0d0 * Flux_extsubrhosupj_1(i, j - 1, k) - Flux_extsubrhosupj_1(i, j - 2, k)
			Flux_extsubmxsupj_1(i, j, k) = 2.0d0 * Flux_extsubmxsupj_1(i, j - 1, k) - Flux_extsubmxsupj_1(i, j - 2, k)
			Flux_extsubmysupj_1(i, j, k) = 2.0d0 * Flux_extsubmysupj_1(i, j - 1, k) - Flux_extsubmysupj_1(i, j - 2, k)
			Flux_extsubmzsupj_1(i, j, k) = 2.0d0 * Flux_extsubmzsupj_1(i, j - 1, k) - Flux_extsubmzsupj_1(i, j - 2, k)
		end if
		if ((FOV_zLower(i, j, k) .gt. 0) .and. ((k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, j, int(k + 1)) .gt. 0)) .or. (k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, j, int(k + 2)) .gt. 0)) .or. (k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, j, int(k + 3)) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(int(i + 1), j, int(k + 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(int(i - 1), j, int(k + 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(int(i + 2), j, int(k + 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(int(i - 2), j, int(k + 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(int(i + 3), j, int(k + 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(int(i - 3), j, int(k + 1)) .gt. 0)) .or. (j + 1 .le. cctk_lsh(2) .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 1), int(k + 1)) .gt. 0)) .or. (j - 1 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 1), int(k + 1)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 2), int(k + 1)) .gt. 0)) .or. (j - 2 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 2), int(k + 1)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 3), int(k + 1)) .gt. 0)) .or. (j - 3 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 3), int(k + 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(int(i + 1), j, int(k + 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(int(i - 1), j, int(k + 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(int(i + 2), j, int(k + 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(int(i - 2), j, int(k + 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(int(i + 3), j, int(k + 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(int(i - 3), j, int(k + 2)) .gt. 0)) .or. (j + 1 .le. cctk_lsh(2) .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 1), int(k + 2)) .gt. 0)) .or. (j - 1 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 1), int(k + 2)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 2), int(k + 2)) .gt. 0)) .or. (j - 2 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 2), int(k + 2)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 3), int(k + 2)) .gt. 0)) .or. (j - 3 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 3), int(k + 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(int(i + 1), j, int(k + 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(int(i - 1), j, int(k + 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(int(i + 2), j, int(k + 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(int(i - 2), j, int(k + 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(int(i + 3), j, int(k + 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(int(i - 3), j, int(k + 3)) .gt. 0)) .or. (j + 1 .le. cctk_lsh(2) .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 1), int(k + 3)) .gt. 0)) .or. (j - 1 .ge. 1 .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 1), int(k + 3)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 2), int(k + 3)) .gt. 0)) .or. (j - 2 .ge. 1 .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 2), int(k + 3)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 3), int(k + 3)) .gt. 0)) .or. (j - 3 .ge. 1 .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 3), int(k + 3)) .gt. 0))))) then
				Flux_extsubrhosupk_1(i, j, 4 - k) = 2.0d0 * Flux_extsubrhosupk_1(i, j, 4 - k + 1) - Flux_extsubrhosupk_1(i, j, 4 - k + 2)
				Flux_extsubmxsupk_1(i, j, 4 - k) = 2.0d0 * Flux_extsubmxsupk_1(i, j, 4 - k + 1) - Flux_extsubmxsupk_1(i, j, 4 - k + 2)
				Flux_extsubmysupk_1(i, j, 4 - k) = 2.0d0 * Flux_extsubmysupk_1(i, j, 4 - k + 1) - Flux_extsubmysupk_1(i, j, 4 - k + 2)
				Flux_extsubmzsupk_1(i, j, 4 - k) = 2.0d0 * Flux_extsubmzsupk_1(i, j, 4 - k + 1) - Flux_extsubmzsupk_1(i, j, 4 - k + 2)
		end if
		if ((FOV_zUpper(i, j, k) .gt. 0) .and. ((k - 1 .ge. 1 .and. (FOV_1(i, j, int(k - 1)) .gt. 0)) .or. (k - 2 .ge. 1 .and. (FOV_1(i, j, int(k - 2)) .gt. 0)) .or. (k - 3 .ge. 1 .and. (FOV_1(i, j, int(k - 3)) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. k - 1 .ge. 1 .and. (FOV_1(int(i + 1), j, int(k - 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. k - 1 .ge. 1 .and. (FOV_1(int(i - 1), j, int(k - 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. k - 1 .ge. 1 .and. (FOV_1(int(i + 2), j, int(k - 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. k - 1 .ge. 1 .and. (FOV_1(int(i - 2), j, int(k - 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. k - 1 .ge. 1 .and. (FOV_1(int(i + 3), j, int(k - 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. k - 1 .ge. 1 .and. (FOV_1(int(i - 3), j, int(k - 1)) .gt. 0)) .or. (j + 1 .le. cctk_lsh(2) .and. k - 1 .ge. 1 .and. (FOV_1(i, int(j + 1), int(k - 1)) .gt. 0)) .or. (j - 1 .ge. 1 .and. k - 1 .ge. 1 .and. (FOV_1(i, int(j - 1), int(k - 1)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. k - 1 .ge. 1 .and. (FOV_1(i, int(j + 2), int(k - 1)) .gt. 0)) .or. (j - 2 .ge. 1 .and. k - 1 .ge. 1 .and. (FOV_1(i, int(j - 2), int(k - 1)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. k - 1 .ge. 1 .and. (FOV_1(i, int(j + 3), int(k - 1)) .gt. 0)) .or. (j - 3 .ge. 1 .and. k - 1 .ge. 1 .and. (FOV_1(i, int(j - 3), int(k - 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. k - 2 .ge. 1 .and. (FOV_1(int(i + 1), j, int(k - 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. k - 2 .ge. 1 .and. (FOV_1(int(i - 1), j, int(k - 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. k - 2 .ge. 1 .and. (FOV_1(int(i + 2), j, int(k - 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. k - 2 .ge. 1 .and. (FOV_1(int(i - 2), j, int(k - 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. k - 2 .ge. 1 .and. (FOV_1(int(i + 3), j, int(k - 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. k - 2 .ge. 1 .and. (FOV_1(int(i - 3), j, int(k - 2)) .gt. 0)) .or. (j + 1 .le. cctk_lsh(2) .and. k - 2 .ge. 1 .and. (FOV_1(i, int(j + 1), int(k - 2)) .gt. 0)) .or. (j - 1 .ge. 1 .and. k - 2 .ge. 1 .and. (FOV_1(i, int(j - 1), int(k - 2)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. k - 2 .ge. 1 .and. (FOV_1(i, int(j + 2), int(k - 2)) .gt. 0)) .or. (j - 2 .ge. 1 .and. k - 2 .ge. 1 .and. (FOV_1(i, int(j - 2), int(k - 2)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. k - 2 .ge. 1 .and. (FOV_1(i, int(j + 3), int(k - 2)) .gt. 0)) .or. (j - 3 .ge. 1 .and. k - 2 .ge. 1 .and. (FOV_1(i, int(j - 3), int(k - 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. k - 3 .ge. 1 .and. (FOV_1(int(i + 1), j, int(k - 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. k - 3 .ge. 1 .and. (FOV_1(int(i - 1), j, int(k - 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. k - 3 .ge. 1 .and. (FOV_1(int(i + 2), j, int(k - 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. k - 3 .ge. 1 .and. (FOV_1(int(i - 2), j, int(k - 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. k - 3 .ge. 1 .and. (FOV_1(int(i + 3), j, int(k - 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. k - 3 .ge. 1 .and. (FOV_1(int(i - 3), j, int(k - 3)) .gt. 0)) .or. (j + 1 .le. cctk_lsh(2) .and. k - 3 .ge. 1 .and. (FOV_1(i, int(j + 1), int(k - 3)) .gt. 0)) .or. (j - 1 .ge. 1 .and. k - 3 .ge. 1 .and. (FOV_1(i, int(j - 1), int(k - 3)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. k - 3 .ge. 1 .and. (FOV_1(i, int(j + 2), int(k - 3)) .gt. 0)) .or. (j - 2 .ge. 1 .and. k - 3 .ge. 1 .and. (FOV_1(i, int(j - 2), int(k - 3)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. k - 3 .ge. 1 .and. (FOV_1(i, int(j + 3), int(k - 3)) .gt. 0)) .or. (j - 3 .ge. 1 .and. k - 3 .ge. 1 .and. (FOV_1(i, int(j - 3), int(k - 3)) .gt. 0))))) then
			Flux_extsubrhosupk_1(i, j, k) = 2.0d0 * Flux_extsubrhosupk_1(i, j, k - 1) - Flux_extsubrhosupk_1(i, j, k - 2)
			Flux_extsubmxsupk_1(i, j, k) = 2.0d0 * Flux_extsubmxsupk_1(i, j, k - 1) - Flux_extsubmxsupk_1(i, j, k - 2)
			Flux_extsubmysupk_1(i, j, k) = 2.0d0 * Flux_extsubmysupk_1(i, j, k - 1) - Flux_extsubmysupk_1(i, j, k - 2)
			Flux_extsubmzsupk_1(i, j, k) = 2.0d0 * Flux_extsubmzsupk_1(i, j, k - 1) - Flux_extsubmzsupk_1(i, j, k - 2)
		end if
		if (FOV_1(i, j, k) .gt. 0) then
			if ((d_i_Null(i, j, k) .ne. 0 .or. d_j_Null(i, j, k) .ne. 0 .or. d_k_Null(i, j, k) .ne. 0)) then
				if (FOV_3(int(i - d_i_Null(i, j, k)), int(j - d_j_Null(i, j, k)), int(k - d_k_Null(i, j, k))) .gt. 0) then
					call extrapolate_flux(CCTK_PASS_FTOF, d_i_Null, Flux_extsubNullsupi_3, Flux_extsubNullsupi_3, i, d_j_Null, Flux_extsubNullsupj_3, Flux_extsubNullsupj_3, j, d_k_Null, Flux_extsubNullsupk_3, Flux_extsubNullsupk_3, k, FOV_3)
				end if
			end if
		end if
				end do
			end do
		end do
		call CCTK_SyncGroup(status, cctkGH, "FDAsNozzle3D::block1")
		do k = kStart, kEnd
			do j = jStart, jEnd
				do i = iStart, iEnd
					if ((FOV_3(i, j, k) .gt. 0 .or. (((i + 1 .le. cctk_lsh(1) .and. FOV_3(i + 1, j, k) .gt. 0) .or. (j + 1 .le. cctk_lsh(2) .and. FOV_3(i, j + 1, k) .gt. 0) .or. (k + 1 .le. cctk_lsh(3) .and. FOV_3(i, j, k + 1) .gt. 0)) .or. ((i - 1 .ge. 1 .and. FOV_3(i - 1, j, k) .gt. 0) .or. (j - 1 .ge. 1 .and. FOV_3(i, j - 1, k) .gt. 0) .or. (k - 1 .ge. 1 .and. FOV_3(i, j, k - 1) .gt. 0)))) .and. (i + 1 .le. cctk_lsh(1) .and. i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. j - 1 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. k - 1 .ge. 1)) then
					Speedi_3(i, j, k) = MAX(Speedi_3(i, j, k), Speedi_3(i + 1, j, k))
					Speedj_3(i, j, k) = MAX(Speedj_3(i, j, k), Speedj_3(i, j + 1, k))
					Speedk_3(i, j, k) = MAX(Speedk_3(i, j, k), Speedk_3(i, j, k + 1))
					end if
					if ((FOV_1(i, j, k) .gt. 0 .or. (((i + 1 .le. cctk_lsh(1) .and. FOV_1(i + 1, j, k) .gt. 0) .or. (j + 1 .le. cctk_lsh(2) .and. FOV_1(i, j + 1, k) .gt. 0) .or. (k + 1 .le. cctk_lsh(3) .and. FOV_1(i, j, k + 1) .gt. 0)) .or. ((i - 1 .ge. 1 .and. FOV_1(i - 1, j, k) .gt. 0) .or. (j - 1 .ge. 1 .and. FOV_1(i, j - 1, k) .gt. 0) .or. (k - 1 .ge. 1 .and. FOV_1(i, j, k - 1) .gt. 0)))) .and. (i + 1 .le. cctk_lsh(1) .and. i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. j - 1 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. k - 1 .ge. 1)) then
					Speedi_1(i, j, k) = MAX(Speedi_1(i, j, k), Speedi_1(i + 1, j, k))
					Speedj_1(i, j, k) = MAX(Speedj_1(i, j, k), Speedj_1(i, j + 1, k))
					Speedk_1(i, j, k) = MAX(Speedk_1(i, j, k), Speedk_1(i, j, k + 1))
					end if
				end do
			end do
		end do
		call CCTK_SyncGroup(status, cctkGH, "FDAsNozzle3D::block2")
		do k = kStart, kEnd
			do j = jStart, jEnd
				do i = iStart, iEnd
					if ((FOV_3(i, j, k) .gt. 0) .and. (i + 2 .le. cctk_lsh(1) .and. i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. j - 2 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. k - 2 .ge. 1)) then
					fluxAccNull_3 = 0.0d0
					fluxAccNull_3 = fluxAccNull_3 - FDOCi(CCTK_PASS_FTOF, extrapolatedsubNullsupi, Flux_extsubNullsupi_3, Speedi_3, i, j, k)
					fluxAccNull_3 = fluxAccNull_3 - FDOCj(CCTK_PASS_FTOF, extrapolatedsubNullsupj, Flux_extsubNullsupj_3, Speedj_3, i, j, k)
					fluxAccNull_3 = fluxAccNull_3 - FDOCk(CCTK_PASS_FTOF, extrapolatedsubNullsupk, Flux_extsubNullsupk_3, Speedk_3, i, j, k)
					k1Null(i, j, k) = RK3P1(CCTK_PASS_FTOF, fluxAccNull_3, Null_p, 0, i, j, k)
					end if
					if ((FOV_1(i, j, k) .gt. 0) .and. (i + 2 .le. cctk_lsh(1) .and. i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. j - 2 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. k - 2 .ge. 1)) then
					fluxAccrho_1 = 0.0d0
					fluxAccmx_1 = rho_p(i, j, k) * fx_p(i, j, k)
					fluxAccmy_1 = rho_p(i, j, k) * fy_p(i, j, k)
					fluxAccmz_1 = rho_p(i, j, k) * fz_p(i, j, k)
					fluxAccrho_1 = fluxAccrho_1 - FDOCi(CCTK_PASS_FTOF, extrapolatedsubrhosupi, Flux_extsubrhosupi_1, Speedi_1, i, j, k)
					fluxAccrho_1 = fluxAccrho_1 - FDOCj(CCTK_PASS_FTOF, extrapolatedsubrhosupj, Flux_extsubrhosupj_1, Speedj_1, i, j, k)
					fluxAccrho_1 = fluxAccrho_1 - FDOCk(CCTK_PASS_FTOF, extrapolatedsubrhosupk, Flux_extsubrhosupk_1, Speedk_1, i, j, k)
					fluxAccmx_1 = fluxAccmx_1 - FDOCi(CCTK_PASS_FTOF, extrapolatedsubmxsupi, Flux_extsubmxsupi_1, Speedi_1, i, j, k)
					fluxAccmx_1 = fluxAccmx_1 - FDOCj(CCTK_PASS_FTOF, extrapolatedsubmxsupj, Flux_extsubmxsupj_1, Speedj_1, i, j, k)
					fluxAccmx_1 = fluxAccmx_1 - FDOCk(CCTK_PASS_FTOF, extrapolatedsubmxsupk, Flux_extsubmxsupk_1, Speedk_1, i, j, k)
					fluxAccmy_1 = fluxAccmy_1 - FDOCi(CCTK_PASS_FTOF, extrapolatedsubmysupi, Flux_extsubmysupi_1, Speedi_1, i, j, k)
					fluxAccmy_1 = fluxAccmy_1 - FDOCj(CCTK_PASS_FTOF, extrapolatedsubmysupj, Flux_extsubmysupj_1, Speedj_1, i, j, k)
					fluxAccmy_1 = fluxAccmy_1 - FDOCk(CCTK_PASS_FTOF, extrapolatedsubmysupk, Flux_extsubmysupk_1, Speedk_1, i, j, k)
					fluxAccmz_1 = fluxAccmz_1 - FDOCi(CCTK_PASS_FTOF, extrapolatedsubmzsupi, Flux_extsubmzsupi_1, Speedi_1, i, j, k)
					fluxAccmz_1 = fluxAccmz_1 - FDOCj(CCTK_PASS_FTOF, extrapolatedsubmzsupj, Flux_extsubmzsupj_1, Speedj_1, i, j, k)
					fluxAccmz_1 = fluxAccmz_1 - FDOCk(CCTK_PASS_FTOF, extrapolatedsubmzsupk, Flux_extsubmzsupk_1, Speedk_1, i, j, k)
					n_x_1 = 0.0d0
					n_y_1 = 0.0d0
					n_z_1 = 0.0d0
					interaction_index_1 = 0.0d0
					if (((n_x_1 .eq. 0.0d0) .and. (n_y_1 .eq. 0.0d0)) .and. (n_z_1 .eq. 0.0d0)) then
					if (FOV_3(int(i + 1.0d0), j, k) .gt. 0.0d0) then
					interaction_index_1 = 1.0d0
					n_x_1 = n_x_1 + deltax
					end if
					if (FOV_3(int(i - 1.0d0), j, k) .gt. 0.0d0) then
					interaction_index_1 = 1.0d0
					n_x_1 = n_x_1 - deltax
					end if
					if (FOV_3(i, int(j + 1.0d0), k) .gt. 0.0d0) then
					interaction_index_1 = 1.0d0
					n_y_1 = n_y_1 + deltay
					end if
					if (FOV_3(i, int(j - 1.0d0), k) .gt. 0.0d0) then
					interaction_index_1 = 1.0d0
					n_y_1 = n_y_1 - deltay
					end if
					if (FOV_3(i, j, int(k + 1.0d0)) .gt. 0.0d0) then
					interaction_index_1 = 1.0d0
					n_z_1 = n_z_1 + deltaz
					end if
					if (FOV_3(i, j, int(k - 1.0d0)) .gt. 0.0d0) then
					interaction_index_1 = 1.0d0
					n_z_1 = n_z_1 - deltaz
					end if
					end if
					if (((n_x_1 .eq. 0.0d0) .and. (n_y_1 .eq. 0.0d0)) .and. (n_z_1 .eq. 0.0d0)) then
					if (((((((((FOV_3(int(i + 1.0d0), int(j + 1.0d0), int(k + 1.0d0)) .gt. 0.0d0) .or. (FOV_3(int(i + 1.0d0), int(j + 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(int(i + 1.0d0), int(j + 1.0d0), k) .gt. 0.0d0)) .or. (FOV_3(int(i + 1.0d0), int(j - 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(int(i + 1.0d0), int(j - 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(int(i + 1.0d0), int(j - 1.0d0), k) .gt. 0.0d0)) .or. (FOV_3(int(i + 1.0d0), j, int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(int(i + 1.0d0), j, int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(int(i + 1.0d0), j, k) .gt. 0.0d0)) then
					interaction_index_1 = 1.0d0
					n_x_1 = n_x_1 + deltax
					end if
					if (((((((((FOV_3(int(i - 1.0d0), int(j + 1.0d0), int(k + 1.0d0)) .gt. 0.0d0) .or. (FOV_3(int(i - 1.0d0), int(j + 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(int(i - 1.0d0), int(j + 1.0d0), k) .gt. 0.0d0)) .or. (FOV_3(int(i - 1.0d0), int(j - 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(int(i - 1.0d0), int(j - 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(int(i - 1.0d0), int(j - 1.0d0), k) .gt. 0.0d0)) .or. (FOV_3(int(i - 1.0d0), j, int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(int(i - 1.0d0), j, int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(int(i - 1.0d0), j, k) .gt. 0.0d0)) then
					interaction_index_1 = 1.0d0
					n_x_1 = n_x_1 - deltax
					end if
					if (((((((((FOV_3(int(i + 1.0d0), int(j + 1.0d0), int(k + 1.0d0)) .gt. 0.0d0) .or. (FOV_3(int(i + 1.0d0), int(j + 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(int(i + 1.0d0), int(j + 1.0d0), k) .gt. 0.0d0)) .or. (FOV_3(int(i - 1.0d0), int(j + 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(int(i - 1.0d0), int(j + 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(int(i - 1.0d0), int(j + 1.0d0), k) .gt. 0.0d0)) .or. (FOV_3(i, int(j + 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(i, int(j + 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(i, int(j + 1.0d0), k) .gt. 0.0d0)) then
					interaction_index_1 = 1.0d0
					n_y_1 = n_y_1 + deltay
					end if
					if (((((((((FOV_3(int(i + 1.0d0), int(j - 1.0d0), int(k + 1.0d0)) .gt. 0.0d0) .or. (FOV_3(int(i + 1.0d0), int(j - 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(int(i + 1.0d0), int(j - 1.0d0), k) .gt. 0.0d0)) .or. (FOV_3(int(i - 1.0d0), int(j - 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(int(i - 1.0d0), int(j - 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(int(i - 1.0d0), int(j - 1.0d0), k) .gt. 0.0d0)) .or. (FOV_3(i, int(j - 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(i, int(j - 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(i, int(j - 1.0d0), k) .gt. 0.0d0)) then
					interaction_index_1 = 1.0d0
					n_y_1 = n_y_1 - deltay
					end if
					if (((((((((FOV_3(int(i + 1.0d0), int(j + 1.0d0), int(k + 1.0d0)) .gt. 0.0d0) .or. (FOV_3(int(i + 1.0d0), int(j - 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(int(i + 1.0d0), j, int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(int(i - 1.0d0), int(j + 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(int(i - 1.0d0), int(j - 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(int(i - 1.0d0), j, int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(i, int(j + 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(i, int(j - 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(i, j, int(k + 1.0d0)) .gt. 0.0d0)) then
					interaction_index_1 = 1.0d0
					n_z_1 = n_z_1 + deltaz
					end if
					if (((((((((FOV_3(int(i + 1.0d0), int(j + 1.0d0), int(k - 1.0d0)) .gt. 0.0d0) .or. (FOV_3(int(i + 1.0d0), int(j - 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(int(i + 1.0d0), j, int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(int(i - 1.0d0), int(j + 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(int(i - 1.0d0), int(j - 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(int(i - 1.0d0), j, int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(i, int(j + 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(i, int(j - 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(i, j, int(k - 1.0d0)) .gt. 0.0d0)) then
					interaction_index_1 = 1.0d0
					n_z_1 = n_z_1 - deltaz
					end if
					end if
					if (((n_x_1 .eq. 0.0d0) .and. (n_y_1 .eq. 0.0d0)) .and. (n_z_1 .eq. 0.0d0)) then
					if (FOV_xLower(int(i + 1.0d0), j, k) .gt. 0.0d0) then
					interaction_index_1 = 2.0d0
					n_x_1 = n_x_1 + deltax
					end if
					if (FOV_xLower(int(i - 1.0d0), j, k) .gt. 0.0d0) then
					interaction_index_1 = 2.0d0
					n_x_1 = n_x_1 - deltax
					end if
					if (FOV_xLower(i, int(j + 1.0d0), k) .gt. 0.0d0) then
					interaction_index_1 = 2.0d0
					n_y_1 = n_y_1 + deltay
					end if
					if (FOV_xLower(i, int(j - 1.0d0), k) .gt. 0.0d0) then
					interaction_index_1 = 2.0d0
					n_y_1 = n_y_1 - deltay
					end if
					if (FOV_xLower(i, j, int(k + 1.0d0)) .gt. 0.0d0) then
					interaction_index_1 = 2.0d0
					n_z_1 = n_z_1 + deltaz
					end if
					if (FOV_xLower(i, j, int(k - 1.0d0)) .gt. 0.0d0) then
					interaction_index_1 = 2.0d0
					n_z_1 = n_z_1 - deltaz
					end if
					end if
					if (((n_x_1 .eq. 0.0d0) .and. (n_y_1 .eq. 0.0d0)) .and. (n_z_1 .eq. 0.0d0)) then
					if (((((((((FOV_xLower(int(i + 1.0d0), int(j + 1.0d0), int(k + 1.0d0)) .gt. 0.0d0) .or. (FOV_xLower(int(i + 1.0d0), int(j + 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i + 1.0d0), int(j + 1.0d0), k) .gt. 0.0d0)) .or. (FOV_xLower(int(i + 1.0d0), int(j - 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i + 1.0d0), int(j - 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i + 1.0d0), int(j - 1.0d0), k) .gt. 0.0d0)) .or. (FOV_xLower(int(i + 1.0d0), j, int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i + 1.0d0), j, int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i + 1.0d0), j, k) .gt. 0.0d0)) then
					interaction_index_1 = 2.0d0
					n_x_1 = n_x_1 + deltax
					end if
					if (((((((((FOV_xLower(int(i - 1.0d0), int(j + 1.0d0), int(k + 1.0d0)) .gt. 0.0d0) .or. (FOV_xLower(int(i - 1.0d0), int(j + 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i - 1.0d0), int(j + 1.0d0), k) .gt. 0.0d0)) .or. (FOV_xLower(int(i - 1.0d0), int(j - 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i - 1.0d0), int(j - 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i - 1.0d0), int(j - 1.0d0), k) .gt. 0.0d0)) .or. (FOV_xLower(int(i - 1.0d0), j, int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i - 1.0d0), j, int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i - 1.0d0), j, k) .gt. 0.0d0)) then
					interaction_index_1 = 2.0d0
					n_x_1 = n_x_1 - deltax
					end if
					if (((((((((FOV_xLower(int(i + 1.0d0), int(j + 1.0d0), int(k + 1.0d0)) .gt. 0.0d0) .or. (FOV_xLower(int(i + 1.0d0), int(j + 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i + 1.0d0), int(j + 1.0d0), k) .gt. 0.0d0)) .or. (FOV_xLower(int(i - 1.0d0), int(j + 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i - 1.0d0), int(j + 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i - 1.0d0), int(j + 1.0d0), k) .gt. 0.0d0)) .or. (FOV_xLower(i, int(j + 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(i, int(j + 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(i, int(j + 1.0d0), k) .gt. 0.0d0)) then
					interaction_index_1 = 2.0d0
					n_y_1 = n_y_1 + deltay
					end if
					if (((((((((FOV_xLower(int(i + 1.0d0), int(j - 1.0d0), int(k + 1.0d0)) .gt. 0.0d0) .or. (FOV_xLower(int(i + 1.0d0), int(j - 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i + 1.0d0), int(j - 1.0d0), k) .gt. 0.0d0)) .or. (FOV_xLower(int(i - 1.0d0), int(j - 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i - 1.0d0), int(j - 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i - 1.0d0), int(j - 1.0d0), k) .gt. 0.0d0)) .or. (FOV_xLower(i, int(j - 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(i, int(j - 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(i, int(j - 1.0d0), k) .gt. 0.0d0)) then
					interaction_index_1 = 2.0d0
					n_y_1 = n_y_1 - deltay
					end if
					if (((((((((FOV_xLower(int(i + 1.0d0), int(j + 1.0d0), int(k + 1.0d0)) .gt. 0.0d0) .or. (FOV_xLower(int(i + 1.0d0), int(j - 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i + 1.0d0), j, int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i - 1.0d0), int(j + 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i - 1.0d0), int(j - 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i - 1.0d0), j, int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(i, int(j + 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(i, int(j - 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(i, j, int(k + 1.0d0)) .gt. 0.0d0)) then
					interaction_index_1 = 2.0d0
					n_z_1 = n_z_1 + deltaz
					end if
					if (((((((((FOV_xLower(int(i + 1.0d0), int(j + 1.0d0), int(k - 1.0d0)) .gt. 0.0d0) .or. (FOV_xLower(int(i + 1.0d0), int(j - 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i + 1.0d0), j, int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i - 1.0d0), int(j + 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i - 1.0d0), int(j - 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i - 1.0d0), j, int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(i, int(j + 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(i, int(j - 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(i, j, int(k - 1.0d0)) .gt. 0.0d0)) then
					interaction_index_1 = 2.0d0
					n_z_1 = n_z_1 - deltaz
					end if
					end if
					if (((n_x_1 .eq. 0.0d0) .and. (n_y_1 .eq. 0.0d0)) .and. (n_z_1 .eq. 0.0d0)) then
					if (FOV_xUpper(int(i + 1.0d0), j, k) .gt. 0.0d0) then
					interaction_index_1 = 3.0d0
					n_x_1 = n_x_1 + deltax
					end if
					if (FOV_xUpper(int(i - 1.0d0), j, k) .gt. 0.0d0) then
					interaction_index_1 = 3.0d0
					n_x_1 = n_x_1 - deltax
					end if
					if (FOV_xUpper(i, int(j + 1.0d0), k) .gt. 0.0d0) then
					interaction_index_1 = 3.0d0
					n_y_1 = n_y_1 + deltay
					end if
					if (FOV_xUpper(i, int(j - 1.0d0), k) .gt. 0.0d0) then
					interaction_index_1 = 3.0d0
					n_y_1 = n_y_1 - deltay
					end if
					if (FOV_xUpper(i, j, int(k + 1.0d0)) .gt. 0.0d0) then
					interaction_index_1 = 3.0d0
					n_z_1 = n_z_1 + deltaz
					end if
					if (FOV_xUpper(i, j, int(k - 1.0d0)) .gt. 0.0d0) then
					interaction_index_1 = 3.0d0
					n_z_1 = n_z_1 - deltaz
					end if
					end if
					if (((n_x_1 .eq. 0.0d0) .and. (n_y_1 .eq. 0.0d0)) .and. (n_z_1 .eq. 0.0d0)) then
					if (((((((((FOV_xUpper(int(i + 1.0d0), int(j + 1.0d0), int(k + 1.0d0)) .gt. 0.0d0) .or. (FOV_xUpper(int(i + 1.0d0), int(j + 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i + 1.0d0), int(j + 1.0d0), k) .gt. 0.0d0)) .or. (FOV_xUpper(int(i + 1.0d0), int(j - 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i + 1.0d0), int(j - 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i + 1.0d0), int(j - 1.0d0), k) .gt. 0.0d0)) .or. (FOV_xUpper(int(i + 1.0d0), j, int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i + 1.0d0), j, int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i + 1.0d0), j, k) .gt. 0.0d0)) then
					interaction_index_1 = 3.0d0
					n_x_1 = n_x_1 + deltax
					end if
					if (((((((((FOV_xUpper(int(i - 1.0d0), int(j + 1.0d0), int(k + 1.0d0)) .gt. 0.0d0) .or. (FOV_xUpper(int(i - 1.0d0), int(j + 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i - 1.0d0), int(j + 1.0d0), k) .gt. 0.0d0)) .or. (FOV_xUpper(int(i - 1.0d0), int(j - 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i - 1.0d0), int(j - 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i - 1.0d0), int(j - 1.0d0), k) .gt. 0.0d0)) .or. (FOV_xUpper(int(i - 1.0d0), j, int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i - 1.0d0), j, int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i - 1.0d0), j, k) .gt. 0.0d0)) then
					interaction_index_1 = 3.0d0
					n_x_1 = n_x_1 - deltax
					end if
					if (((((((((FOV_xUpper(int(i + 1.0d0), int(j + 1.0d0), int(k + 1.0d0)) .gt. 0.0d0) .or. (FOV_xUpper(int(i + 1.0d0), int(j + 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i + 1.0d0), int(j + 1.0d0), k) .gt. 0.0d0)) .or. (FOV_xUpper(int(i - 1.0d0), int(j + 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i - 1.0d0), int(j + 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i - 1.0d0), int(j + 1.0d0), k) .gt. 0.0d0)) .or. (FOV_xUpper(i, int(j + 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(i, int(j + 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(i, int(j + 1.0d0), k) .gt. 0.0d0)) then
					interaction_index_1 = 3.0d0
					n_y_1 = n_y_1 + deltay
					end if
					if (((((((((FOV_xUpper(int(i + 1.0d0), int(j - 1.0d0), int(k + 1.0d0)) .gt. 0.0d0) .or. (FOV_xUpper(int(i + 1.0d0), int(j - 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i + 1.0d0), int(j - 1.0d0), k) .gt. 0.0d0)) .or. (FOV_xUpper(int(i - 1.0d0), int(j - 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i - 1.0d0), int(j - 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i - 1.0d0), int(j - 1.0d0), k) .gt. 0.0d0)) .or. (FOV_xUpper(i, int(j - 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(i, int(j - 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(i, int(j - 1.0d0), k) .gt. 0.0d0)) then
					interaction_index_1 = 3.0d0
					n_y_1 = n_y_1 - deltay
					end if
					if (((((((((FOV_xUpper(int(i + 1.0d0), int(j + 1.0d0), int(k + 1.0d0)) .gt. 0.0d0) .or. (FOV_xUpper(int(i + 1.0d0), int(j - 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i + 1.0d0), j, int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i - 1.0d0), int(j + 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i - 1.0d0), int(j - 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i - 1.0d0), j, int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(i, int(j + 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(i, int(j - 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(i, j, int(k + 1.0d0)) .gt. 0.0d0)) then
					interaction_index_1 = 3.0d0
					n_z_1 = n_z_1 + deltaz
					end if
					if (((((((((FOV_xUpper(int(i + 1.0d0), int(j + 1.0d0), int(k - 1.0d0)) .gt. 0.0d0) .or. (FOV_xUpper(int(i + 1.0d0), int(j - 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i + 1.0d0), j, int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i - 1.0d0), int(j + 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i - 1.0d0), int(j - 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i - 1.0d0), j, int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(i, int(j + 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(i, int(j - 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(i, j, int(k - 1.0d0)) .gt. 0.0d0)) then
					interaction_index_1 = 3.0d0
					n_z_1 = n_z_1 - deltaz
					end if
					end if
					if (((n_x_1 .ne. 0.0d0) .or. (n_y_1 .ne. 0.0d0)) .or. (n_z_1 .ne. 0.0d0)) then
					mod_normal_1 = SQRT((n_x_1 ** 2.0d0 + n_y_1 ** 2.0d0) + n_z_1 ** 2.0d0)
					n_x_1 = n_x_1 / mod_normal_1
					n_y_1 = n_y_1 / mod_normal_1
					n_z_1 = n_z_1 / mod_normal_1
					end if
					if (interaction_index_1 .eq. 1.0d0) then
					fluxAccm_n_1 = fluxAccmx_1 * n_x_1 + fluxAccmy_1 * n_y_1 + fluxAccmz_1 * n_z_1
					fluxAccm_tx_1 = fluxAccmx_1 - (fluxAccmx_1 * n_x_1 + fluxAccmy_1 * n_y_1 + fluxAccmz_1 * n_z_1) * n_x_1
					fluxAccm_ty_1 = fluxAccmy_1 - (fluxAccmx_1 * n_x_1 + fluxAccmy_1 * n_y_1 + fluxAccmz_1 * n_z_1) * n_y_1
					fluxAccm_tz_1 = fluxAccmz_1 - (fluxAccmx_1 * n_x_1 + fluxAccmy_1 * n_y_1 + fluxAccmz_1 * n_z_1) * n_z_1
					m_n_1 = mx_p(i, j, k) * n_x_1 + my_p(i, j, k) * n_y_1 + mz_p(i, j, k) * n_z_1
					m_tx_1 = mx_p(i, j, k) - (mx_p(i, j, k) * n_x_1 + my_p(i, j, k) * n_y_1 + mz_p(i, j, k) * n_z_1) * n_x_1
					m_ty_1 = my_p(i, j, k) - (mx_p(i, j, k) * n_x_1 + my_p(i, j, k) * n_y_1 + mz_p(i, j, k) * n_z_1) * n_y_1
					m_tz_1 = mz_p(i, j, k) - (mx_p(i, j, k) * n_x_1 + my_p(i, j, k) * n_y_1 + mz_p(i, j, k) * n_z_1) * n_z_1
					c_1 = c0 * ((rho_p(i, j, k) / rho0) ** ((gamma + (-1.0d0)) / 2.0d0))
					cn_1 = m_n_1 / rho_p(i, j, k)
					if ((c_1 + cn_1) .gt. 0.0d0) then
					fluxAccwnp_3_1 = fluxAccrho_1 * (-((-c_1) + cn_1)) + fluxAccm_n_1
					else
					fluxAccwnp_3_1 = (-fluxAccm_n_1) + fluxAccrho_1 * (-((-c_1) + cn_1))
					end if
					if (((-c_1) + cn_1) .gt. 0.0d0) then
					fluxAccwnm_3_1 = fluxAccrho_1 * (-(c_1 + cn_1)) + fluxAccm_n_1
					else
					fluxAccwnm_3_1 = (-fluxAccm_n_1) + fluxAccrho_1 * (-(c_1 + cn_1))
					end if
					if (cn_1 .gt. 0.0d0) then
					fluxAccwtx_3_1 = fluxAccrho_1 * (-m_tx_1) / rho_p(i, j, k) + fluxAccm_tx_1
					else
					fluxAccwtx_3_1 = fluxAccrho_1 * (-m_tx_1) / rho_p(i, j, k)
					end if
					if (cn_1 .gt. 0.0d0) then
					fluxAccwty_3_1 = fluxAccrho_1 * (-m_ty_1) / rho_p(i, j, k) + fluxAccm_ty_1
					else
					fluxAccwty_3_1 = fluxAccrho_1 * (-m_ty_1) / rho_p(i, j, k)
					end if
					if (cn_1 .gt. 0.0d0) then
					fluxAccwtz_3_1 = fluxAccrho_1 * (-m_tz_1) / rho_p(i, j, k) + fluxAccm_tz_1
					else
					fluxAccwtz_3_1 = fluxAccrho_1 * (-m_tz_1) / rho_p(i, j, k)
					end if
					fluxAccrho_1 = fluxAccwnp_3_1 * 1.0d0 / (2.0d0 * c_1) + fluxAccwnm_3_1 * (-1.0d0) / (2.0d0 * c_1)
					fluxAccm_n_1 = fluxAccwnp_3_1 * (cn_1 / (2.0d0 * c_1) + 1.0d0 / 2.0d0) + fluxAccwnm_3_1 * (-(cn_1 / (2.0d0 * c_1) + (-1.0d0 / 2.0d0)))
					fluxAccm_tx_1 = (fluxAccwnp_3_1 * m_tx_1 / (2.0d0 * rho_p(i, j, k) * c_1) + fluxAccwnm_3_1 * (-m_tx_1) / (2.0d0 * rho_p(i, j, k) * c_1)) + fluxAccwtx_3_1
					fluxAccm_ty_1 = (fluxAccwnp_3_1 * m_ty_1 / (2.0d0 * rho_p(i, j, k) * c_1) + fluxAccwnm_3_1 * (-m_ty_1) / (2.0d0 * rho_p(i, j, k) * c_1)) + fluxAccwty_3_1
					fluxAccm_tz_1 = (fluxAccwnp_3_1 * m_tz_1 / (2.0d0 * rho_p(i, j, k) * c_1) + fluxAccwnm_3_1 * (-m_tz_1) / (2.0d0 * rho_p(i, j, k) * c_1)) + fluxAccwtz_3_1
					fluxAccmx_1 = fluxAccm_tx_1 + fluxAccm_n_1 * n_x_1
					fluxAccmy_1 = fluxAccm_ty_1 + fluxAccm_n_1 * n_y_1
					fluxAccmz_1 = fluxAccm_tz_1 + fluxAccm_n_1 * n_z_1
					end if
					if (interaction_index_1 .eq. 2.0d0) then
					fluxAccm_n_1 = fluxAccmx_1 * n_x_1 + fluxAccmy_1 * n_y_1 + fluxAccmz_1 * n_z_1
					fluxAccm_tx_1 = fluxAccmx_1 - (fluxAccmx_1 * n_x_1 + fluxAccmy_1 * n_y_1 + fluxAccmz_1 * n_z_1) * n_x_1
					fluxAccm_ty_1 = fluxAccmy_1 - (fluxAccmx_1 * n_x_1 + fluxAccmy_1 * n_y_1 + fluxAccmz_1 * n_z_1) * n_y_1
					fluxAccm_tz_1 = fluxAccmz_1 - (fluxAccmx_1 * n_x_1 + fluxAccmy_1 * n_y_1 + fluxAccmz_1 * n_z_1) * n_z_1
					m_n_1 = mx_p(i, j, k) * n_x_1 + my_p(i, j, k) * n_y_1 + mz_p(i, j, k) * n_z_1
					m_tx_1 = mx_p(i, j, k) - (mx_p(i, j, k) * n_x_1 + my_p(i, j, k) * n_y_1 + mz_p(i, j, k) * n_z_1) * n_x_1
					m_ty_1 = my_p(i, j, k) - (mx_p(i, j, k) * n_x_1 + my_p(i, j, k) * n_y_1 + mz_p(i, j, k) * n_z_1) * n_y_1
					m_tz_1 = mz_p(i, j, k) - (mx_p(i, j, k) * n_x_1 + my_p(i, j, k) * n_y_1 + mz_p(i, j, k) * n_z_1) * n_z_1
					c_1 = c0 * ((rho_p(i, j, k) / rho0) ** ((gamma + (-1.0d0)) / 2.0d0))
					cn_1 = m_n_1 / rho_p(i, j, k)
					if ((c_1 + cn_1) .gt. 0.0d0) then
					fluxAccwnp_3_1 = fluxAccrho_1 * (-((-c_1) + cn_1)) + fluxAccm_n_1
					else
					fluxAccwnp_3_1 = (-lambdain) * (m_n_1 + rho_p(i, j, k) * v0 * (1.0d0 - (((-0.008d0) + (j - (1 - cctk_lbnd(2))) * deltay) ** 2.0d0 + ((-0.008d0) + (k - (1 - cctk_lbnd(3))) * deltaz) ** 2.0d0) / ((FLOOR((0.006d0 * ABS((-0.008d0) + (j - (1 - cctk_lbnd(2))) * deltay)) / (SQRT(((-0.008d0) + (j - (1 - cctk_lbnd(2))) * deltay) ** 2.0d0 + ((-0.008d0) + (k - (1 - cctk_lbnd(3))) * deltaz) ** 2.0d0) * deltay) + 0.5d0) * deltay) ** 2.0d0 + (FLOOR((0.006d0 * ABS((-0.008d0) + (k - (1 - cctk_lbnd(3))) * deltaz)) / (SQRT(((-0.008d0) + (j - (1 - cctk_lbnd(2))) * deltay) ** 2.0d0 + ((-0.008d0) + (k - (1 - cctk_lbnd(3))) * deltaz) ** 2.0d0) * deltaz) + 0.5d0) * deltaz) ** 2.0d0)))
					end if
					if (((-c_1) + cn_1) .gt. 0.0d0) then
					fluxAccwnm_3_1 = fluxAccrho_1 * (-(c_1 + cn_1)) + fluxAccm_n_1
					else
					fluxAccwnm_3_1 = (-lambdain) * (m_n_1 + rho_p(i, j, k) * v0 * (1.0d0 - (((-0.008d0) + (j - (1 - cctk_lbnd(2))) * deltay) ** 2.0d0 + ((-0.008d0) + (k - (1 - cctk_lbnd(3))) * deltaz) ** 2.0d0) / ((FLOOR((0.006d0 * ABS((-0.008d0) + (j - (1 - cctk_lbnd(2))) * deltay)) / (SQRT(((-0.008d0) + (j - (1 - cctk_lbnd(2))) * deltay) ** 2.0d0 + ((-0.008d0) + (k - (1 - cctk_lbnd(3))) * deltaz) ** 2.0d0) * deltay) + 0.5d0) * deltay) ** 2.0d0 + (FLOOR((0.006d0 * ABS((-0.008d0) + (k - (1 - cctk_lbnd(3))) * deltaz)) / (SQRT(((-0.008d0) + (j - (1 - cctk_lbnd(2))) * deltay) ** 2.0d0 + ((-0.008d0) + (k - (1 - cctk_lbnd(3))) * deltaz) ** 2.0d0) * deltaz) + 0.5d0) * deltaz) ** 2.0d0)))
					end if
					if (cn_1 .gt. 0.0d0) then
					fluxAccwtx_3_1 = fluxAccrho_1 * (-m_tx_1) / rho_p(i, j, k) + fluxAccm_tx_1
					else
					fluxAccwtx_3_1 = (-lambdain) * (m_tx_1 - rho_p(i, j, k) * vt0)
					end if
					if (cn_1 .gt. 0.0d0) then
					fluxAccwty_3_1 = fluxAccrho_1 * (-m_ty_1) / rho_p(i, j, k) + fluxAccm_ty_1
					else
					fluxAccwty_3_1 = (-lambdain) * (m_ty_1 - rho_p(i, j, k) * vt0)
					end if
					if (cn_1 .gt. 0.0d0) then
					fluxAccwtz_3_1 = fluxAccrho_1 * (-m_tz_1) / rho_p(i, j, k) + fluxAccm_tz_1
					else
					fluxAccwtz_3_1 = (-lambdain) * (m_tz_1 - rho_p(i, j, k) * vt0)
					end if
					fluxAccrho_1 = fluxAccwnp_3_1 * 1.0d0 / (2.0d0 * c_1) + fluxAccwnm_3_1 * (-1.0d0) / (2.0d0 * c_1)
					fluxAccm_n_1 = fluxAccwnp_3_1 * (cn_1 / (2.0d0 * c_1) + 1.0d0 / 2.0d0) + fluxAccwnm_3_1 * (-(cn_1 / (2.0d0 * c_1) + (-1.0d0 / 2.0d0)))
					fluxAccm_tx_1 = (fluxAccwnp_3_1 * m_tx_1 / (2.0d0 * rho_p(i, j, k) * c_1) + fluxAccwnm_3_1 * (-m_tx_1) / (2.0d0 * rho_p(i, j, k) * c_1)) + fluxAccwtx_3_1
					fluxAccm_ty_1 = (fluxAccwnp_3_1 * m_ty_1 / (2.0d0 * rho_p(i, j, k) * c_1) + fluxAccwnm_3_1 * (-m_ty_1) / (2.0d0 * rho_p(i, j, k) * c_1)) + fluxAccwty_3_1
					fluxAccm_tz_1 = (fluxAccwnp_3_1 * m_tz_1 / (2.0d0 * rho_p(i, j, k) * c_1) + fluxAccwnm_3_1 * (-m_tz_1) / (2.0d0 * rho_p(i, j, k) * c_1)) + fluxAccwtz_3_1
					fluxAccmx_1 = fluxAccm_tx_1 + fluxAccm_n_1 * n_x_1
					fluxAccmy_1 = fluxAccm_ty_1 + fluxAccm_n_1 * n_y_1
					fluxAccmz_1 = fluxAccm_tz_1 + fluxAccm_n_1 * n_z_1
					end if
					if (interaction_index_1 .eq. 3.0d0) then
					fluxAccm_n_1 = fluxAccmx_1 * n_x_1 + fluxAccmy_1 * n_y_1 + fluxAccmz_1 * n_z_1
					fluxAccm_tx_1 = fluxAccmx_1 - (fluxAccmx_1 * n_x_1 + fluxAccmy_1 * n_y_1 + fluxAccmz_1 * n_z_1) * n_x_1
					fluxAccm_ty_1 = fluxAccmy_1 - (fluxAccmx_1 * n_x_1 + fluxAccmy_1 * n_y_1 + fluxAccmz_1 * n_z_1) * n_y_1
					fluxAccm_tz_1 = fluxAccmz_1 - (fluxAccmx_1 * n_x_1 + fluxAccmy_1 * n_y_1 + fluxAccmz_1 * n_z_1) * n_z_1
					m_n_1 = mx_p(i, j, k) * n_x_1 + my_p(i, j, k) * n_y_1 + mz_p(i, j, k) * n_z_1
					m_tx_1 = mx_p(i, j, k) - (mx_p(i, j, k) * n_x_1 + my_p(i, j, k) * n_y_1 + mz_p(i, j, k) * n_z_1) * n_x_1
					m_ty_1 = my_p(i, j, k) - (mx_p(i, j, k) * n_x_1 + my_p(i, j, k) * n_y_1 + mz_p(i, j, k) * n_z_1) * n_y_1
					m_tz_1 = mz_p(i, j, k) - (mx_p(i, j, k) * n_x_1 + my_p(i, j, k) * n_y_1 + mz_p(i, j, k) * n_z_1) * n_z_1
					c_1 = c0 * ((rho_p(i, j, k) / rho0) ** ((gamma + (-1.0d0)) / 2.0d0))
					cn_1 = m_n_1 / rho_p(i, j, k)
					if ((c_1 + cn_1) .gt. 0.0d0) then
					fluxAccwnp_3_1 = fluxAccrho_1 * (-((-c_1) + cn_1)) + fluxAccm_n_1
					else
					fluxAccwnp_3_1 = ((-c_1) + cn_1) * lambdaout * (rho_p(i, j, k) - rho0)
					end if
					if (((-c_1) + cn_1) .gt. 0.0d0) then
					fluxAccwnm_3_1 = fluxAccrho_1 * (-(c_1 + cn_1)) + fluxAccm_n_1
					else
					fluxAccwnm_3_1 = (c_1 + cn_1) * lambdaout * (rho_p(i, j, k) - rho0)
					end if
					if (cn_1 .gt. 0.0d0) then
					fluxAccwtx_3_1 = fluxAccrho_1 * (-m_tx_1) / rho_p(i, j, k) + fluxAccm_tx_1
					else
					fluxAccwtx_3_1 = m_tx_1 / rho_p(i, j, k) * lambdaout * (rho_p(i, j, k) - rho0)
					end if
					if (cn_1 .gt. 0.0d0) then
					fluxAccwty_3_1 = fluxAccrho_1 * (-m_ty_1) / rho_p(i, j, k) + fluxAccm_ty_1
					else
					fluxAccwty_3_1 = m_ty_1 / rho_p(i, j, k) * lambdaout * (rho_p(i, j, k) - rho0)
					end if
					if (cn_1 .gt. 0.0d0) then
					fluxAccwtz_3_1 = fluxAccrho_1 * (-m_tz_1) / rho_p(i, j, k) + fluxAccm_tz_1
					else
					fluxAccwtz_3_1 = m_tz_1 / rho_p(i, j, k) * lambdaout * (rho_p(i, j, k) - rho0)
					end if
					fluxAccrho_1 = fluxAccwnp_3_1 * 1.0d0 / (2.0d0 * c_1) + fluxAccwnm_3_1 * (-1.0d0) / (2.0d0 * c_1)
					fluxAccm_n_1 = fluxAccwnp_3_1 * (cn_1 / (2.0d0 * c_1) + 1.0d0 / 2.0d0) + fluxAccwnm_3_1 * (-(cn_1 / (2.0d0 * c_1) + (-1.0d0 / 2.0d0)))
					fluxAccm_tx_1 = (fluxAccwnp_3_1 * m_tx_1 / (2.0d0 * rho_p(i, j, k) * c_1) + fluxAccwnm_3_1 * (-m_tx_1) / (2.0d0 * rho_p(i, j, k) * c_1)) + fluxAccwtx_3_1
					fluxAccm_ty_1 = (fluxAccwnp_3_1 * m_ty_1 / (2.0d0 * rho_p(i, j, k) * c_1) + fluxAccwnm_3_1 * (-m_ty_1) / (2.0d0 * rho_p(i, j, k) * c_1)) + fluxAccwty_3_1
					fluxAccm_tz_1 = (fluxAccwnp_3_1 * m_tz_1 / (2.0d0 * rho_p(i, j, k) * c_1) + fluxAccwnm_3_1 * (-m_tz_1) / (2.0d0 * rho_p(i, j, k) * c_1)) + fluxAccwtz_3_1
					fluxAccmx_1 = fluxAccm_tx_1 + fluxAccm_n_1 * n_x_1
					fluxAccmy_1 = fluxAccm_ty_1 + fluxAccm_n_1 * n_y_1
					fluxAccmz_1 = fluxAccm_tz_1 + fluxAccm_n_1 * n_z_1
					end if
					k1rho(i, j, k) = RK3P1(CCTK_PASS_FTOF, fluxAccrho_1, rho_p, 0, i, j, k)
					k1mx(i, j, k) = RK3P1(CCTK_PASS_FTOF, fluxAccmx_1, mx_p, 0, i, j, k)
					k1my(i, j, k) = RK3P1(CCTK_PASS_FTOF, fluxAccmy_1, my_p, 0, i, j, k)
					k1mz(i, j, k) = RK3P1(CCTK_PASS_FTOF, fluxAccmz_1, mz_p, 0, i, j, k)
					k1p(i, j, k) = (Paux + ((k1rho(i, j, k) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					k1fx(i, j, k) = pfx
					k1fy(i, j, k) = pfy
					k1fz(i, j, k) = pfz
					end if
				end do
			end do
		end do
		call CCTK_SyncGroup(status, cctkGH, "FDAsNozzle3D::block3")
		do k = kStart, kEnd
			do j = jStart, jEnd
				do i = iStart, iEnd
					extrapolatedsubrhosupi(i, j, k) = k1rho(i, j, k)
					extrapolatedsubrhosupj(i, j, k) = k1rho(i, j, k)
					extrapolatedsubrhosupk(i, j, k) = k1rho(i, j, k)
					extrapolatedsubmxsupi(i, j, k) = k1mx(i, j, k)
					extrapolatedsubmxsupj(i, j, k) = k1mx(i, j, k)
					extrapolatedsubmxsupk(i, j, k) = k1mx(i, j, k)
					extrapolatedsubmysupi(i, j, k) = k1my(i, j, k)
					extrapolatedsubmysupj(i, j, k) = k1my(i, j, k)
					extrapolatedsubmysupk(i, j, k) = k1my(i, j, k)
					extrapolatedsubmzsupi(i, j, k) = k1mz(i, j, k)
					extrapolatedsubmzsupj(i, j, k) = k1mz(i, j, k)
					extrapolatedsubmzsupk(i, j, k) = k1mz(i, j, k)
					extrapolatedsubpsupi(i, j, k) = k1p(i, j, k)
					extrapolatedsubpsupj(i, j, k) = k1p(i, j, k)
					extrapolatedsubpsupk(i, j, k) = k1p(i, j, k)
					extrapolatedsubfxsupi(i, j, k) = k1fx(i, j, k)
					extrapolatedsubfxsupj(i, j, k) = k1fx(i, j, k)
					extrapolatedsubfxsupk(i, j, k) = k1fx(i, j, k)
					extrapolatedsubfysupi(i, j, k) = k1fy(i, j, k)
					extrapolatedsubfysupj(i, j, k) = k1fy(i, j, k)
					extrapolatedsubfysupk(i, j, k) = k1fy(i, j, k)
					extrapolatedsubfzsupi(i, j, k) = k1fz(i, j, k)
					extrapolatedsubfzsupj(i, j, k) = k1fz(i, j, k)
					extrapolatedsubfzsupk(i, j, k) = k1fz(i, j, k)
					extrapolatedsubNullsupi(i, j, k) = k1Null(i, j, k)
					extrapolatedsubNullsupj(i, j, k) = k1Null(i, j, k)
					extrapolatedsubNullsupk(i, j, k) = k1Null(i, j, k)
				end do
			end do
		end do
		do k = kStart, kEnd
			do j = jStart, jEnd
				do i = iStart, iEnd
!					Boundaries
		if ((FOV_xLower(i, j, k) .gt. 0) .and. ((i + 1 .le. cctk_lsh(1) .and. (FOV_3(int(i + 1), j, k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. (FOV_3(int(i + 2), j, k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. (FOV_3(int(i + 3), j, k) .gt. 0)))) then
				k1Null(4 - i, j, k) = 0.0d0
		end if
		if ((FOV_xUpper(i, j, k) .gt. 0) .and. ((i - 1 .ge. 1 .and. (FOV_3(int(i - 1), j, k) .gt. 0)) .or. (i - 2 .ge. 1 .and. (FOV_3(int(i - 2), j, k) .gt. 0)) .or. (i - 3 .ge. 1 .and. (FOV_3(int(i - 3), j, k) .gt. 0)))) then
			k1Null(i, j, k) = 0.0d0
		end if
		if ((FOV_yLower(i, j, k) .gt. 0) .and. ((j + 1 .le. cctk_lsh(2) .and. (FOV_3(i, int(j + 1), k) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. (FOV_3(i, int(j + 2), k) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. (FOV_3(i, int(j + 3), k) .gt. 0)))) then
				k1Null(i, 4 - j, k) = 0.0d0
		end if
		if ((FOV_yUpper(i, j, k) .gt. 0) .and. ((j - 1 .ge. 1 .and. (FOV_3(i, int(j - 1), k) .gt. 0)) .or. (j - 2 .ge. 1 .and. (FOV_3(i, int(j - 2), k) .gt. 0)) .or. (j - 3 .ge. 1 .and. (FOV_3(i, int(j - 3), k) .gt. 0)))) then
			k1Null(i, j, k) = 0.0d0
		end if
		if ((FOV_zLower(i, j, k) .gt. 0) .and. ((k + 1 .le. cctk_lsh(3) .and. (FOV_3(i, j, int(k + 1)) .gt. 0)) .or. (k + 2 .le. cctk_lsh(3) .and. (FOV_3(i, j, int(k + 2)) .gt. 0)) .or. (k + 3 .le. cctk_lsh(3) .and. (FOV_3(i, j, int(k + 3)) .gt. 0)))) then
				k1Null(i, j, 4 - k) = 0.0d0
		end if
		if ((FOV_zUpper(i, j, k) .gt. 0) .and. ((k - 1 .ge. 1 .and. (FOV_3(i, j, int(k - 1)) .gt. 0)) .or. (k - 2 .ge. 1 .and. (FOV_3(i, j, int(k - 2)) .gt. 0)) .or. (k - 3 .ge. 1 .and. (FOV_3(i, j, int(k - 3)) .gt. 0)))) then
			k1Null(i, j, k) = 0.0d0
		end if
		if (FOV_3(i, j, k) .gt. 0) then
			if ((d_i_rho(i, j, k) .ne. 0 .or. d_j_rho(i, j, k) .ne. 0 .or. d_k_rho(i, j, k) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_rho, k1rho, extrapolatedsubrhosupi, i, d_j_rho, k1rho, extrapolatedsubrhosupj, j, d_k_rho, k1rho, extrapolatedsubrhosupk, k, k1rho, FOV_1)
			end if
			if ((d_i_mx(i, j, k) .ne. 0 .or. d_j_mx(i, j, k) .ne. 0 .or. d_k_mx(i, j, k) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_mx, k1mx, extrapolatedsubmxsupi, i, d_j_mx, k1mx, extrapolatedsubmxsupj, j, d_k_mx, k1mx, extrapolatedsubmxsupk, k, k1mx, FOV_1)
			end if
			if ((d_i_my(i, j, k) .ne. 0 .or. d_j_my(i, j, k) .ne. 0 .or. d_k_my(i, j, k) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_my, k1my, extrapolatedsubmysupi, i, d_j_my, k1my, extrapolatedsubmysupj, j, d_k_my, k1my, extrapolatedsubmysupk, k, k1my, FOV_1)
			end if
			if ((d_i_mz(i, j, k) .ne. 0 .or. d_j_mz(i, j, k) .ne. 0 .or. d_k_mz(i, j, k) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_mz, k1mz, extrapolatedsubmzsupi, i, d_j_mz, k1mz, extrapolatedsubmzsupj, j, d_k_mz, k1mz, extrapolatedsubmzsupk, k, k1mz, FOV_1)
			end if
			if ((d_i_fx(i, j, k) .ne. 0 .or. d_j_fx(i, j, k) .ne. 0 .or. d_k_fx(i, j, k) .ne. 0)) then
				if (FOV_1(int(i - d_i_fx(i, j, k)), int(j - d_j_fx(i, j, k)), int(k - d_k_fx(i, j, k))) .gt. 0) then
					extrapolatedsubfxsupi(i, j, k) = pfx
					extrapolatedsubfxsupj(i, j, k) = pfx
					extrapolatedsubfxsupk(i, j, k) = pfx
					if (((d_i_fx(i, j, k) .ne. 0.0d0) .and. (d_j_fx(i, j, k) .ne. 0.0d0)) .and. (d_k_fx(i, j, k) .ne. 0.0d0)) then
					k1fx(i, j, k) = ((extrapolatedsubfxsupi(i, j, k) + extrapolatedsubfxsupj(i, j, k)) + extrapolatedsubfxsupk(i, j, k)) / 3.0d0
					else
					if (((d_i_fx(i, j, k) .ne. 0.0d0) .and. (d_j_fx(i, j, k) .ne. 0.0d0)) .and. (d_k_fx(i, j, k) .eq. 0.0d0)) then
					k1fx(i, j, k) = (extrapolatedsubfxsupi(i, j, k) + extrapolatedsubfxsupj(i, j, k)) / 2.0d0
					extrapolatedsubfxsupk(i, j, k) = k1fx(i, j, k)
					else
					if (((d_i_fx(i, j, k) .ne. 0.0d0) .and. (d_j_fx(i, j, k) .eq. 0.0d0)) .and. (d_k_fx(i, j, k) .ne. 0.0d0)) then
					k1fx(i, j, k) = (extrapolatedsubfxsupi(i, j, k) + extrapolatedsubfxsupk(i, j, k)) / 2.0d0
					extrapolatedsubfxsupj(i, j, k) = k1fx(i, j, k)
					else
					if (((d_i_fx(i, j, k) .ne. 0.0d0) .and. (d_j_fx(i, j, k) .eq. 0.0d0)) .and. (d_k_fx(i, j, k) .eq. 0.0d0)) then
					k1fx(i, j, k) = extrapolatedsubfxsupi(i, j, k)
					extrapolatedsubfxsupj(i, j, k) = k1fx(i, j, k)
					extrapolatedsubfxsupk(i, j, k) = k1fx(i, j, k)
					else
					if (((d_i_fx(i, j, k) .eq. 0.0d0) .and. (d_j_fx(i, j, k) .ne. 0.0d0)) .and. (d_k_fx(i, j, k) .ne. 0.0d0)) then
					k1fx(i, j, k) = (extrapolatedsubfxsupj(i, j, k) + extrapolatedsubfxsupk(i, j, k)) / 2.0d0
					extrapolatedsubfxsupi(i, j, k) = k1fx(i, j, k)
					else
					if (((d_i_fx(i, j, k) .eq. 0.0d0) .and. (d_j_fx(i, j, k) .ne. 0.0d0)) .and. (d_k_fx(i, j, k) .eq. 0.0d0)) then
					k1fx(i, j, k) = extrapolatedsubfxsupj(i, j, k)
					extrapolatedsubfxsupi(i, j, k) = k1fx(i, j, k)
					extrapolatedsubfxsupk(i, j, k) = k1fx(i, j, k)
					else
					if (((d_i_fx(i, j, k) .eq. 0.0d0) .and. (d_j_fx(i, j, k) .eq. 0.0d0)) .and. (d_k_fx(i, j, k) .ne. 0.0d0)) then
					k1fx(i, j, k) = extrapolatedsubfxsupk(i, j, k)
					extrapolatedsubfxsupi(i, j, k) = k1fx(i, j, k)
					extrapolatedsubfxsupj(i, j, k) = k1fx(i, j, k)
					end if
					end if
					end if
					end if
					end if
					end if
					end if
				end if
			end if
			if ((d_i_fy(i, j, k) .ne. 0 .or. d_j_fy(i, j, k) .ne. 0 .or. d_k_fy(i, j, k) .ne. 0)) then
				if (FOV_1(int(i - d_i_fy(i, j, k)), int(j - d_j_fy(i, j, k)), int(k - d_k_fy(i, j, k))) .gt. 0) then
					extrapolatedsubfysupi(i, j, k) = pfy
					extrapolatedsubfysupj(i, j, k) = pfy
					extrapolatedsubfysupk(i, j, k) = pfy
					if (((d_i_fy(i, j, k) .ne. 0.0d0) .and. (d_j_fy(i, j, k) .ne. 0.0d0)) .and. (d_k_fy(i, j, k) .ne. 0.0d0)) then
					k1fy(i, j, k) = ((extrapolatedsubfysupi(i, j, k) + extrapolatedsubfysupj(i, j, k)) + extrapolatedsubfysupk(i, j, k)) / 3.0d0
					else
					if (((d_i_fy(i, j, k) .ne. 0.0d0) .and. (d_j_fy(i, j, k) .ne. 0.0d0)) .and. (d_k_fy(i, j, k) .eq. 0.0d0)) then
					k1fy(i, j, k) = (extrapolatedsubfysupi(i, j, k) + extrapolatedsubfysupj(i, j, k)) / 2.0d0
					extrapolatedsubfysupk(i, j, k) = k1fy(i, j, k)
					else
					if (((d_i_fy(i, j, k) .ne. 0.0d0) .and. (d_j_fy(i, j, k) .eq. 0.0d0)) .and. (d_k_fy(i, j, k) .ne. 0.0d0)) then
					k1fy(i, j, k) = (extrapolatedsubfysupi(i, j, k) + extrapolatedsubfysupk(i, j, k)) / 2.0d0
					extrapolatedsubfysupj(i, j, k) = k1fy(i, j, k)
					else
					if (((d_i_fy(i, j, k) .ne. 0.0d0) .and. (d_j_fy(i, j, k) .eq. 0.0d0)) .and. (d_k_fy(i, j, k) .eq. 0.0d0)) then
					k1fy(i, j, k) = extrapolatedsubfysupi(i, j, k)
					extrapolatedsubfysupj(i, j, k) = k1fy(i, j, k)
					extrapolatedsubfysupk(i, j, k) = k1fy(i, j, k)
					else
					if (((d_i_fy(i, j, k) .eq. 0.0d0) .and. (d_j_fy(i, j, k) .ne. 0.0d0)) .and. (d_k_fy(i, j, k) .ne. 0.0d0)) then
					k1fy(i, j, k) = (extrapolatedsubfysupj(i, j, k) + extrapolatedsubfysupk(i, j, k)) / 2.0d0
					extrapolatedsubfysupi(i, j, k) = k1fy(i, j, k)
					else
					if (((d_i_fy(i, j, k) .eq. 0.0d0) .and. (d_j_fy(i, j, k) .ne. 0.0d0)) .and. (d_k_fy(i, j, k) .eq. 0.0d0)) then
					k1fy(i, j, k) = extrapolatedsubfysupj(i, j, k)
					extrapolatedsubfysupi(i, j, k) = k1fy(i, j, k)
					extrapolatedsubfysupk(i, j, k) = k1fy(i, j, k)
					else
					if (((d_i_fy(i, j, k) .eq. 0.0d0) .and. (d_j_fy(i, j, k) .eq. 0.0d0)) .and. (d_k_fy(i, j, k) .ne. 0.0d0)) then
					k1fy(i, j, k) = extrapolatedsubfysupk(i, j, k)
					extrapolatedsubfysupi(i, j, k) = k1fy(i, j, k)
					extrapolatedsubfysupj(i, j, k) = k1fy(i, j, k)
					end if
					end if
					end if
					end if
					end if
					end if
					end if
				end if
			end if
			if ((d_i_fz(i, j, k) .ne. 0 .or. d_j_fz(i, j, k) .ne. 0 .or. d_k_fz(i, j, k) .ne. 0)) then
				if (FOV_1(int(i - d_i_fz(i, j, k)), int(j - d_j_fz(i, j, k)), int(k - d_k_fz(i, j, k))) .gt. 0) then
					extrapolatedsubfzsupi(i, j, k) = pfz
					extrapolatedsubfzsupj(i, j, k) = pfz
					extrapolatedsubfzsupk(i, j, k) = pfz
					if (((d_i_fz(i, j, k) .ne. 0.0d0) .and. (d_j_fz(i, j, k) .ne. 0.0d0)) .and. (d_k_fz(i, j, k) .ne. 0.0d0)) then
					k1fz(i, j, k) = ((extrapolatedsubfzsupi(i, j, k) + extrapolatedsubfzsupj(i, j, k)) + extrapolatedsubfzsupk(i, j, k)) / 3.0d0
					else
					if (((d_i_fz(i, j, k) .ne. 0.0d0) .and. (d_j_fz(i, j, k) .ne. 0.0d0)) .and. (d_k_fz(i, j, k) .eq. 0.0d0)) then
					k1fz(i, j, k) = (extrapolatedsubfzsupi(i, j, k) + extrapolatedsubfzsupj(i, j, k)) / 2.0d0
					extrapolatedsubfzsupk(i, j, k) = k1fz(i, j, k)
					else
					if (((d_i_fz(i, j, k) .ne. 0.0d0) .and. (d_j_fz(i, j, k) .eq. 0.0d0)) .and. (d_k_fz(i, j, k) .ne. 0.0d0)) then
					k1fz(i, j, k) = (extrapolatedsubfzsupi(i, j, k) + extrapolatedsubfzsupk(i, j, k)) / 2.0d0
					extrapolatedsubfzsupj(i, j, k) = k1fz(i, j, k)
					else
					if (((d_i_fz(i, j, k) .ne. 0.0d0) .and. (d_j_fz(i, j, k) .eq. 0.0d0)) .and. (d_k_fz(i, j, k) .eq. 0.0d0)) then
					k1fz(i, j, k) = extrapolatedsubfzsupi(i, j, k)
					extrapolatedsubfzsupj(i, j, k) = k1fz(i, j, k)
					extrapolatedsubfzsupk(i, j, k) = k1fz(i, j, k)
					else
					if (((d_i_fz(i, j, k) .eq. 0.0d0) .and. (d_j_fz(i, j, k) .ne. 0.0d0)) .and. (d_k_fz(i, j, k) .ne. 0.0d0)) then
					k1fz(i, j, k) = (extrapolatedsubfzsupj(i, j, k) + extrapolatedsubfzsupk(i, j, k)) / 2.0d0
					extrapolatedsubfzsupi(i, j, k) = k1fz(i, j, k)
					else
					if (((d_i_fz(i, j, k) .eq. 0.0d0) .and. (d_j_fz(i, j, k) .ne. 0.0d0)) .and. (d_k_fz(i, j, k) .eq. 0.0d0)) then
					k1fz(i, j, k) = extrapolatedsubfzsupj(i, j, k)
					extrapolatedsubfzsupi(i, j, k) = k1fz(i, j, k)
					extrapolatedsubfzsupk(i, j, k) = k1fz(i, j, k)
					else
					if (((d_i_fz(i, j, k) .eq. 0.0d0) .and. (d_j_fz(i, j, k) .eq. 0.0d0)) .and. (d_k_fz(i, j, k) .ne. 0.0d0)) then
					k1fz(i, j, k) = extrapolatedsubfzsupk(i, j, k)
					extrapolatedsubfzsupi(i, j, k) = k1fz(i, j, k)
					extrapolatedsubfzsupj(i, j, k) = k1fz(i, j, k)
					end if
					end if
					end if
					end if
					end if
					end if
					end if
				end if
			end if
			if ((d_i_p(i, j, k) .ne. 0 .or. d_j_p(i, j, k) .ne. 0 .or. d_k_p(i, j, k) .ne. 0)) then
				if (FOV_1(int(i - d_i_p(i, j, k)), int(j - d_j_p(i, j, k)), int(k - d_k_p(i, j, k))) .gt. 0) then
					extrapolatedsubpsupi(i, j, k) = (Paux + ((extrapolatedsubrhosupi(i, j, k) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					extrapolatedsubpsupj(i, j, k) = (Paux + ((extrapolatedsubrhosupj(i, j, k) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					extrapolatedsubpsupk(i, j, k) = (Paux + ((extrapolatedsubrhosupk(i, j, k) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					if (((d_i_p(i, j, k) .ne. 0.0d0) .and. (d_j_p(i, j, k) .ne. 0.0d0)) .and. (d_k_p(i, j, k) .ne. 0.0d0)) then
					k1p(i, j, k) = ((extrapolatedsubpsupi(i, j, k) + extrapolatedsubpsupj(i, j, k)) + extrapolatedsubpsupk(i, j, k)) / 3.0d0
					else
					if (((d_i_p(i, j, k) .ne. 0.0d0) .and. (d_j_p(i, j, k) .ne. 0.0d0)) .and. (d_k_p(i, j, k) .eq. 0.0d0)) then
					k1p(i, j, k) = (extrapolatedsubpsupi(i, j, k) + extrapolatedsubpsupj(i, j, k)) / 2.0d0
					extrapolatedsubpsupk(i, j, k) = k1p(i, j, k)
					else
					if (((d_i_p(i, j, k) .ne. 0.0d0) .and. (d_j_p(i, j, k) .eq. 0.0d0)) .and. (d_k_p(i, j, k) .ne. 0.0d0)) then
					k1p(i, j, k) = (extrapolatedsubpsupi(i, j, k) + extrapolatedsubpsupk(i, j, k)) / 2.0d0
					extrapolatedsubpsupj(i, j, k) = k1p(i, j, k)
					else
					if (((d_i_p(i, j, k) .ne. 0.0d0) .and. (d_j_p(i, j, k) .eq. 0.0d0)) .and. (d_k_p(i, j, k) .eq. 0.0d0)) then
					k1p(i, j, k) = extrapolatedsubpsupi(i, j, k)
					extrapolatedsubpsupj(i, j, k) = k1p(i, j, k)
					extrapolatedsubpsupk(i, j, k) = k1p(i, j, k)
					else
					if (((d_i_p(i, j, k) .eq. 0.0d0) .and. (d_j_p(i, j, k) .ne. 0.0d0)) .and. (d_k_p(i, j, k) .ne. 0.0d0)) then
					k1p(i, j, k) = (extrapolatedsubpsupj(i, j, k) + extrapolatedsubpsupk(i, j, k)) / 2.0d0
					extrapolatedsubpsupi(i, j, k) = k1p(i, j, k)
					else
					if (((d_i_p(i, j, k) .eq. 0.0d0) .and. (d_j_p(i, j, k) .ne. 0.0d0)) .and. (d_k_p(i, j, k) .eq. 0.0d0)) then
					k1p(i, j, k) = extrapolatedsubpsupj(i, j, k)
					extrapolatedsubpsupi(i, j, k) = k1p(i, j, k)
					extrapolatedsubpsupk(i, j, k) = k1p(i, j, k)
					else
					if (((d_i_p(i, j, k) .eq. 0.0d0) .and. (d_j_p(i, j, k) .eq. 0.0d0)) .and. (d_k_p(i, j, k) .ne. 0.0d0)) then
					k1p(i, j, k) = extrapolatedsubpsupk(i, j, k)
					extrapolatedsubpsupi(i, j, k) = k1p(i, j, k)
					extrapolatedsubpsupj(i, j, k) = k1p(i, j, k)
					end if
					end if
					end if
					end if
					end if
					end if
					end if
				end if
			end if
		end if
!					Boundaries
		if ((FOV_xLower(i, j, k) .gt. 0) .and. ((i + 1 .le. cctk_lsh(1) .and. (FOV_1(int(i + 1), j, k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. (FOV_1(int(i + 2), j, k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. (FOV_1(int(i + 3), j, k) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 1), k) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 1), k) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 2), k) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 2), k) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 3), k) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 3), k) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(int(i + 1), j, int(k + 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. k - 1 .ge. 1 .and. (FOV_1(int(i + 1), j, int(k - 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(int(i + 1), j, int(k + 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. k - 2 .ge. 1 .and. (FOV_1(int(i + 1), j, int(k - 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(int(i + 1), j, int(k + 3)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. k - 3 .ge. 1 .and. (FOV_1(int(i + 1), j, int(k - 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 1), k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 1), k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 2), k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 2), k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 3), k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 3), k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(int(i + 2), j, int(k + 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. k - 1 .ge. 1 .and. (FOV_1(int(i + 2), j, int(k - 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(int(i + 2), j, int(k + 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. k - 2 .ge. 1 .and. (FOV_1(int(i + 2), j, int(k - 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(int(i + 2), j, int(k + 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. k - 3 .ge. 1 .and. (FOV_1(int(i + 2), j, int(k - 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 1), k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 1), k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 2), k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 2), k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 3), k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 3), k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(int(i + 3), j, int(k + 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. k - 1 .ge. 1 .and. (FOV_1(int(i + 3), j, int(k - 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(int(i + 3), j, int(k + 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. k - 2 .ge. 1 .and. (FOV_1(int(i + 3), j, int(k - 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(int(i + 3), j, int(k + 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. k - 3 .ge. 1 .and. (FOV_1(int(i + 3), j, int(k - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j, k) .ne. 0 .or. d_j_rho(i, j, k) .ne. 0 .or. d_k_rho(i, j, k) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_rho, k1rho, extrapolatedsubrhosupi, i, d_j_rho, k1rho, extrapolatedsubrhosupj, j, d_k_rho, k1rho, extrapolatedsubrhosupk, k, k1rho, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j, k) .ne. 0 .or. d_j_mx(i, j, k) .ne. 0 .or. d_k_mx(i, j, k) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_mx, k1mx, extrapolatedsubmxsupi, i, d_j_mx, k1mx, extrapolatedsubmxsupj, j, d_k_mx, k1mx, extrapolatedsubmxsupk, k, k1mx, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j, k) .ne. 0 .or. d_j_my(i, j, k) .ne. 0 .or. d_k_my(i, j, k) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_my, k1my, extrapolatedsubmysupi, i, d_j_my, k1my, extrapolatedsubmysupj, j, d_k_my, k1my, extrapolatedsubmysupk, k, k1my, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mz(i, j, k) .ne. 0 .or. d_j_mz(i, j, k) .ne. 0 .or. d_k_mz(i, j, k) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_mz, k1mz, extrapolatedsubmzsupi, i, d_j_mz, k1mz, extrapolatedsubmzsupj, j, d_k_mz, k1mz, extrapolatedsubmzsupk, k, k1mz, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_p(i, j, k) .ne. 0 .or. d_j_p(i, j, k) .ne. 0 .or. d_k_p(i, j, k) .ne. 0)) then
				if (FOV_1(int(i - d_i_p(i, j, k)), int(j - d_j_p(i, j, k)), int(k - d_k_p(i, j, k))) .gt. 0) then
					extrapolatedsubpsupi(i, j, k) = (Paux + ((extrapolatedsubrhosupi(i, j, k) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					extrapolatedsubpsupj(i, j, k) = (Paux + ((extrapolatedsubrhosupj(i, j, k) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					extrapolatedsubpsupk(i, j, k) = (Paux + ((extrapolatedsubrhosupk(i, j, k) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					if (((d_i_p(i, j, k) .ne. 0.0d0) .and. (d_j_p(i, j, k) .ne. 0.0d0)) .and. (d_k_p(i, j, k) .ne. 0.0d0)) then
					k1p(i, j, k) = ((extrapolatedsubpsupi(i, j, k) + extrapolatedsubpsupj(i, j, k)) + extrapolatedsubpsupk(i, j, k)) / 3.0d0
					else
					if (((d_i_p(i, j, k) .ne. 0.0d0) .and. (d_j_p(i, j, k) .ne. 0.0d0)) .and. (d_k_p(i, j, k) .eq. 0.0d0)) then
					k1p(i, j, k) = (extrapolatedsubpsupi(i, j, k) + extrapolatedsubpsupj(i, j, k)) / 2.0d0
					extrapolatedsubpsupk(i, j, k) = k1p(i, j, k)
					else
					if (((d_i_p(i, j, k) .ne. 0.0d0) .and. (d_j_p(i, j, k) .eq. 0.0d0)) .and. (d_k_p(i, j, k) .ne. 0.0d0)) then
					k1p(i, j, k) = (extrapolatedsubpsupi(i, j, k) + extrapolatedsubpsupk(i, j, k)) / 2.0d0
					extrapolatedsubpsupj(i, j, k) = k1p(i, j, k)
					else
					if (((d_i_p(i, j, k) .ne. 0.0d0) .and. (d_j_p(i, j, k) .eq. 0.0d0)) .and. (d_k_p(i, j, k) .eq. 0.0d0)) then
					k1p(i, j, k) = extrapolatedsubpsupi(i, j, k)
					extrapolatedsubpsupj(i, j, k) = k1p(i, j, k)
					extrapolatedsubpsupk(i, j, k) = k1p(i, j, k)
					else
					if (((d_i_p(i, j, k) .eq. 0.0d0) .and. (d_j_p(i, j, k) .ne. 0.0d0)) .and. (d_k_p(i, j, k) .ne. 0.0d0)) then
					k1p(i, j, k) = (extrapolatedsubpsupj(i, j, k) + extrapolatedsubpsupk(i, j, k)) / 2.0d0
					extrapolatedsubpsupi(i, j, k) = k1p(i, j, k)
					else
					if (((d_i_p(i, j, k) .eq. 0.0d0) .and. (d_j_p(i, j, k) .ne. 0.0d0)) .and. (d_k_p(i, j, k) .eq. 0.0d0)) then
					k1p(i, j, k) = extrapolatedsubpsupj(i, j, k)
					extrapolatedsubpsupi(i, j, k) = k1p(i, j, k)
					extrapolatedsubpsupk(i, j, k) = k1p(i, j, k)
					else
					if (((d_i_p(i, j, k) .eq. 0.0d0) .and. (d_j_p(i, j, k) .eq. 0.0d0)) .and. (d_k_p(i, j, k) .ne. 0.0d0)) then
					k1p(i, j, k) = extrapolatedsubpsupk(i, j, k)
					extrapolatedsubpsupi(i, j, k) = k1p(i, j, k)
					extrapolatedsubpsupj(i, j, k) = k1p(i, j, k)
					end if
					end if
					end if
					end if
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fx(i, j, k) .ne. 0 .or. d_j_fx(i, j, k) .ne. 0 .or. d_k_fx(i, j, k) .ne. 0)) then
				if (FOV_1(int(i - d_i_fx(i, j, k)), int(j - d_j_fx(i, j, k)), int(k - d_k_fx(i, j, k))) .gt. 0) then
					extrapolatedsubfxsupi(i, j, k) = pfx
					extrapolatedsubfxsupj(i, j, k) = pfx
					extrapolatedsubfxsupk(i, j, k) = pfx
					if (((d_i_fx(i, j, k) .ne. 0.0d0) .and. (d_j_fx(i, j, k) .ne. 0.0d0)) .and. (d_k_fx(i, j, k) .ne. 0.0d0)) then
					k1fx(i, j, k) = ((extrapolatedsubfxsupi(i, j, k) + extrapolatedsubfxsupj(i, j, k)) + extrapolatedsubfxsupk(i, j, k)) / 3.0d0
					else
					if (((d_i_fx(i, j, k) .ne. 0.0d0) .and. (d_j_fx(i, j, k) .ne. 0.0d0)) .and. (d_k_fx(i, j, k) .eq. 0.0d0)) then
					k1fx(i, j, k) = (extrapolatedsubfxsupi(i, j, k) + extrapolatedsubfxsupj(i, j, k)) / 2.0d0
					extrapolatedsubfxsupk(i, j, k) = k1fx(i, j, k)
					else
					if (((d_i_fx(i, j, k) .ne. 0.0d0) .and. (d_j_fx(i, j, k) .eq. 0.0d0)) .and. (d_k_fx(i, j, k) .ne. 0.0d0)) then
					k1fx(i, j, k) = (extrapolatedsubfxsupi(i, j, k) + extrapolatedsubfxsupk(i, j, k)) / 2.0d0
					extrapolatedsubfxsupj(i, j, k) = k1fx(i, j, k)
					else
					if (((d_i_fx(i, j, k) .ne. 0.0d0) .and. (d_j_fx(i, j, k) .eq. 0.0d0)) .and. (d_k_fx(i, j, k) .eq. 0.0d0)) then
					k1fx(i, j, k) = extrapolatedsubfxsupi(i, j, k)
					extrapolatedsubfxsupj(i, j, k) = k1fx(i, j, k)
					extrapolatedsubfxsupk(i, j, k) = k1fx(i, j, k)
					else
					if (((d_i_fx(i, j, k) .eq. 0.0d0) .and. (d_j_fx(i, j, k) .ne. 0.0d0)) .and. (d_k_fx(i, j, k) .ne. 0.0d0)) then
					k1fx(i, j, k) = (extrapolatedsubfxsupj(i, j, k) + extrapolatedsubfxsupk(i, j, k)) / 2.0d0
					extrapolatedsubfxsupi(i, j, k) = k1fx(i, j, k)
					else
					if (((d_i_fx(i, j, k) .eq. 0.0d0) .and. (d_j_fx(i, j, k) .ne. 0.0d0)) .and. (d_k_fx(i, j, k) .eq. 0.0d0)) then
					k1fx(i, j, k) = extrapolatedsubfxsupj(i, j, k)
					extrapolatedsubfxsupi(i, j, k) = k1fx(i, j, k)
					extrapolatedsubfxsupk(i, j, k) = k1fx(i, j, k)
					else
					if (((d_i_fx(i, j, k) .eq. 0.0d0) .and. (d_j_fx(i, j, k) .eq. 0.0d0)) .and. (d_k_fx(i, j, k) .ne. 0.0d0)) then
					k1fx(i, j, k) = extrapolatedsubfxsupk(i, j, k)
					extrapolatedsubfxsupi(i, j, k) = k1fx(i, j, k)
					extrapolatedsubfxsupj(i, j, k) = k1fx(i, j, k)
					end if
					end if
					end if
					end if
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fy(i, j, k) .ne. 0 .or. d_j_fy(i, j, k) .ne. 0 .or. d_k_fy(i, j, k) .ne. 0)) then
				if (FOV_1(int(i - d_i_fy(i, j, k)), int(j - d_j_fy(i, j, k)), int(k - d_k_fy(i, j, k))) .gt. 0) then
					extrapolatedsubfysupi(i, j, k) = pfy
					extrapolatedsubfysupj(i, j, k) = pfy
					extrapolatedsubfysupk(i, j, k) = pfy
					if (((d_i_fy(i, j, k) .ne. 0.0d0) .and. (d_j_fy(i, j, k) .ne. 0.0d0)) .and. (d_k_fy(i, j, k) .ne. 0.0d0)) then
					k1fy(i, j, k) = ((extrapolatedsubfysupi(i, j, k) + extrapolatedsubfysupj(i, j, k)) + extrapolatedsubfysupk(i, j, k)) / 3.0d0
					else
					if (((d_i_fy(i, j, k) .ne. 0.0d0) .and. (d_j_fy(i, j, k) .ne. 0.0d0)) .and. (d_k_fy(i, j, k) .eq. 0.0d0)) then
					k1fy(i, j, k) = (extrapolatedsubfysupi(i, j, k) + extrapolatedsubfysupj(i, j, k)) / 2.0d0
					extrapolatedsubfysupk(i, j, k) = k1fy(i, j, k)
					else
					if (((d_i_fy(i, j, k) .ne. 0.0d0) .and. (d_j_fy(i, j, k) .eq. 0.0d0)) .and. (d_k_fy(i, j, k) .ne. 0.0d0)) then
					k1fy(i, j, k) = (extrapolatedsubfysupi(i, j, k) + extrapolatedsubfysupk(i, j, k)) / 2.0d0
					extrapolatedsubfysupj(i, j, k) = k1fy(i, j, k)
					else
					if (((d_i_fy(i, j, k) .ne. 0.0d0) .and. (d_j_fy(i, j, k) .eq. 0.0d0)) .and. (d_k_fy(i, j, k) .eq. 0.0d0)) then
					k1fy(i, j, k) = extrapolatedsubfysupi(i, j, k)
					extrapolatedsubfysupj(i, j, k) = k1fy(i, j, k)
					extrapolatedsubfysupk(i, j, k) = k1fy(i, j, k)
					else
					if (((d_i_fy(i, j, k) .eq. 0.0d0) .and. (d_j_fy(i, j, k) .ne. 0.0d0)) .and. (d_k_fy(i, j, k) .ne. 0.0d0)) then
					k1fy(i, j, k) = (extrapolatedsubfysupj(i, j, k) + extrapolatedsubfysupk(i, j, k)) / 2.0d0
					extrapolatedsubfysupi(i, j, k) = k1fy(i, j, k)
					else
					if (((d_i_fy(i, j, k) .eq. 0.0d0) .and. (d_j_fy(i, j, k) .ne. 0.0d0)) .and. (d_k_fy(i, j, k) .eq. 0.0d0)) then
					k1fy(i, j, k) = extrapolatedsubfysupj(i, j, k)
					extrapolatedsubfysupi(i, j, k) = k1fy(i, j, k)
					extrapolatedsubfysupk(i, j, k) = k1fy(i, j, k)
					else
					if (((d_i_fy(i, j, k) .eq. 0.0d0) .and. (d_j_fy(i, j, k) .eq. 0.0d0)) .and. (d_k_fy(i, j, k) .ne. 0.0d0)) then
					k1fy(i, j, k) = extrapolatedsubfysupk(i, j, k)
					extrapolatedsubfysupi(i, j, k) = k1fy(i, j, k)
					extrapolatedsubfysupj(i, j, k) = k1fy(i, j, k)
					end if
					end if
					end if
					end if
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fz(i, j, k) .ne. 0 .or. d_j_fz(i, j, k) .ne. 0 .or. d_k_fz(i, j, k) .ne. 0)) then
				if (FOV_1(int(i - d_i_fz(i, j, k)), int(j - d_j_fz(i, j, k)), int(k - d_k_fz(i, j, k))) .gt. 0) then
					extrapolatedsubfzsupi(i, j, k) = pfz
					extrapolatedsubfzsupj(i, j, k) = pfz
					extrapolatedsubfzsupk(i, j, k) = pfz
					if (((d_i_fz(i, j, k) .ne. 0.0d0) .and. (d_j_fz(i, j, k) .ne. 0.0d0)) .and. (d_k_fz(i, j, k) .ne. 0.0d0)) then
					k1fz(i, j, k) = ((extrapolatedsubfzsupi(i, j, k) + extrapolatedsubfzsupj(i, j, k)) + extrapolatedsubfzsupk(i, j, k)) / 3.0d0
					else
					if (((d_i_fz(i, j, k) .ne. 0.0d0) .and. (d_j_fz(i, j, k) .ne. 0.0d0)) .and. (d_k_fz(i, j, k) .eq. 0.0d0)) then
					k1fz(i, j, k) = (extrapolatedsubfzsupi(i, j, k) + extrapolatedsubfzsupj(i, j, k)) / 2.0d0
					extrapolatedsubfzsupk(i, j, k) = k1fz(i, j, k)
					else
					if (((d_i_fz(i, j, k) .ne. 0.0d0) .and. (d_j_fz(i, j, k) .eq. 0.0d0)) .and. (d_k_fz(i, j, k) .ne. 0.0d0)) then
					k1fz(i, j, k) = (extrapolatedsubfzsupi(i, j, k) + extrapolatedsubfzsupk(i, j, k)) / 2.0d0
					extrapolatedsubfzsupj(i, j, k) = k1fz(i, j, k)
					else
					if (((d_i_fz(i, j, k) .ne. 0.0d0) .and. (d_j_fz(i, j, k) .eq. 0.0d0)) .and. (d_k_fz(i, j, k) .eq. 0.0d0)) then
					k1fz(i, j, k) = extrapolatedsubfzsupi(i, j, k)
					extrapolatedsubfzsupj(i, j, k) = k1fz(i, j, k)
					extrapolatedsubfzsupk(i, j, k) = k1fz(i, j, k)
					else
					if (((d_i_fz(i, j, k) .eq. 0.0d0) .and. (d_j_fz(i, j, k) .ne. 0.0d0)) .and. (d_k_fz(i, j, k) .ne. 0.0d0)) then
					k1fz(i, j, k) = (extrapolatedsubfzsupj(i, j, k) + extrapolatedsubfzsupk(i, j, k)) / 2.0d0
					extrapolatedsubfzsupi(i, j, k) = k1fz(i, j, k)
					else
					if (((d_i_fz(i, j, k) .eq. 0.0d0) .and. (d_j_fz(i, j, k) .ne. 0.0d0)) .and. (d_k_fz(i, j, k) .eq. 0.0d0)) then
					k1fz(i, j, k) = extrapolatedsubfzsupj(i, j, k)
					extrapolatedsubfzsupi(i, j, k) = k1fz(i, j, k)
					extrapolatedsubfzsupk(i, j, k) = k1fz(i, j, k)
					else
					if (((d_i_fz(i, j, k) .eq. 0.0d0) .and. (d_j_fz(i, j, k) .eq. 0.0d0)) .and. (d_k_fz(i, j, k) .ne. 0.0d0)) then
					k1fz(i, j, k) = extrapolatedsubfzsupk(i, j, k)
					extrapolatedsubfzsupi(i, j, k) = k1fz(i, j, k)
					extrapolatedsubfzsupj(i, j, k) = k1fz(i, j, k)
					end if
					end if
					end if
					end if
					end if
					end if
					end if
				end if
			end if
		end if
		if ((FOV_xUpper(i, j, k) .gt. 0) .and. ((i - 1 .ge. 1 .and. (FOV_1(int(i - 1), j, k) .gt. 0)) .or. (i - 2 .ge. 1 .and. (FOV_1(int(i - 2), j, k) .gt. 0)) .or. (i - 3 .ge. 1 .and. (FOV_1(int(i - 3), j, k) .gt. 0)) .or. ((i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 1), k) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 1), k) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 2), k) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 2), k) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 3), k) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 3), k) .gt. 0)) .or. (i - 1 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(int(i - 1), j, int(k + 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. k - 1 .ge. 1 .and. (FOV_1(int(i - 1), j, int(k - 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(int(i - 1), j, int(k + 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. k - 2 .ge. 1 .and. (FOV_1(int(i - 1), j, int(k - 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(int(i - 1), j, int(k + 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. k - 3 .ge. 1 .and. (FOV_1(int(i - 1), j, int(k - 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 1), k) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 1), k) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 2), k) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 2), k) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 3), k) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 3), k) .gt. 0)) .or. (i - 2 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(int(i - 2), j, int(k + 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. k - 1 .ge. 1 .and. (FOV_1(int(i - 2), j, int(k - 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(int(i - 2), j, int(k + 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. k - 2 .ge. 1 .and. (FOV_1(int(i - 2), j, int(k - 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(int(i - 2), j, int(k + 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. k - 3 .ge. 1 .and. (FOV_1(int(i - 2), j, int(k - 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 1), k) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 1), k) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 2), k) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 2), k) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 3), k) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 3), k) .gt. 0)) .or. (i - 3 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(int(i - 3), j, int(k + 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. k - 1 .ge. 1 .and. (FOV_1(int(i - 3), j, int(k - 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(int(i - 3), j, int(k + 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. k - 2 .ge. 1 .and. (FOV_1(int(i - 3), j, int(k - 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(int(i - 3), j, int(k + 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. k - 3 .ge. 1 .and. (FOV_1(int(i - 3), j, int(k - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j, k) .ne. 0 .or. d_j_rho(i, j, k) .ne. 0 .or. d_k_rho(i, j, k) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_rho, k1rho, extrapolatedsubrhosupi, i, d_j_rho, k1rho, extrapolatedsubrhosupj, j, d_k_rho, k1rho, extrapolatedsubrhosupk, k, k1rho, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j, k) .ne. 0 .or. d_j_mx(i, j, k) .ne. 0 .or. d_k_mx(i, j, k) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_mx, k1mx, extrapolatedsubmxsupi, i, d_j_mx, k1mx, extrapolatedsubmxsupj, j, d_k_mx, k1mx, extrapolatedsubmxsupk, k, k1mx, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j, k) .ne. 0 .or. d_j_my(i, j, k) .ne. 0 .or. d_k_my(i, j, k) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_my, k1my, extrapolatedsubmysupi, i, d_j_my, k1my, extrapolatedsubmysupj, j, d_k_my, k1my, extrapolatedsubmysupk, k, k1my, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mz(i, j, k) .ne. 0 .or. d_j_mz(i, j, k) .ne. 0 .or. d_k_mz(i, j, k) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_mz, k1mz, extrapolatedsubmzsupi, i, d_j_mz, k1mz, extrapolatedsubmzsupj, j, d_k_mz, k1mz, extrapolatedsubmzsupk, k, k1mz, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_p(i, j, k) .ne. 0 .or. d_j_p(i, j, k) .ne. 0 .or. d_k_p(i, j, k) .ne. 0)) then
				if (FOV_1(int(i - d_i_p(i, j, k)), int(j - d_j_p(i, j, k)), int(k - d_k_p(i, j, k))) .gt. 0) then
					extrapolatedsubpsupi(i, j, k) = (Paux + ((extrapolatedsubrhosupi(i, j, k) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					extrapolatedsubpsupj(i, j, k) = (Paux + ((extrapolatedsubrhosupj(i, j, k) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					extrapolatedsubpsupk(i, j, k) = (Paux + ((extrapolatedsubrhosupk(i, j, k) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					if (((d_i_p(i, j, k) .ne. 0.0d0) .and. (d_j_p(i, j, k) .ne. 0.0d0)) .and. (d_k_p(i, j, k) .ne. 0.0d0)) then
					k1p(i, j, k) = ((extrapolatedsubpsupi(i, j, k) + extrapolatedsubpsupj(i, j, k)) + extrapolatedsubpsupk(i, j, k)) / 3.0d0
					else
					if (((d_i_p(i, j, k) .ne. 0.0d0) .and. (d_j_p(i, j, k) .ne. 0.0d0)) .and. (d_k_p(i, j, k) .eq. 0.0d0)) then
					k1p(i, j, k) = (extrapolatedsubpsupi(i, j, k) + extrapolatedsubpsupj(i, j, k)) / 2.0d0
					extrapolatedsubpsupk(i, j, k) = k1p(i, j, k)
					else
					if (((d_i_p(i, j, k) .ne. 0.0d0) .and. (d_j_p(i, j, k) .eq. 0.0d0)) .and. (d_k_p(i, j, k) .ne. 0.0d0)) then
					k1p(i, j, k) = (extrapolatedsubpsupi(i, j, k) + extrapolatedsubpsupk(i, j, k)) / 2.0d0
					extrapolatedsubpsupj(i, j, k) = k1p(i, j, k)
					else
					if (((d_i_p(i, j, k) .ne. 0.0d0) .and. (d_j_p(i, j, k) .eq. 0.0d0)) .and. (d_k_p(i, j, k) .eq. 0.0d0)) then
					k1p(i, j, k) = extrapolatedsubpsupi(i, j, k)
					extrapolatedsubpsupj(i, j, k) = k1p(i, j, k)
					extrapolatedsubpsupk(i, j, k) = k1p(i, j, k)
					else
					if (((d_i_p(i, j, k) .eq. 0.0d0) .and. (d_j_p(i, j, k) .ne. 0.0d0)) .and. (d_k_p(i, j, k) .ne. 0.0d0)) then
					k1p(i, j, k) = (extrapolatedsubpsupj(i, j, k) + extrapolatedsubpsupk(i, j, k)) / 2.0d0
					extrapolatedsubpsupi(i, j, k) = k1p(i, j, k)
					else
					if (((d_i_p(i, j, k) .eq. 0.0d0) .and. (d_j_p(i, j, k) .ne. 0.0d0)) .and. (d_k_p(i, j, k) .eq. 0.0d0)) then
					k1p(i, j, k) = extrapolatedsubpsupj(i, j, k)
					extrapolatedsubpsupi(i, j, k) = k1p(i, j, k)
					extrapolatedsubpsupk(i, j, k) = k1p(i, j, k)
					else
					if (((d_i_p(i, j, k) .eq. 0.0d0) .and. (d_j_p(i, j, k) .eq. 0.0d0)) .and. (d_k_p(i, j, k) .ne. 0.0d0)) then
					k1p(i, j, k) = extrapolatedsubpsupk(i, j, k)
					extrapolatedsubpsupi(i, j, k) = k1p(i, j, k)
					extrapolatedsubpsupj(i, j, k) = k1p(i, j, k)
					end if
					end if
					end if
					end if
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fx(i, j, k) .ne. 0 .or. d_j_fx(i, j, k) .ne. 0 .or. d_k_fx(i, j, k) .ne. 0)) then
				if (FOV_1(int(i - d_i_fx(i, j, k)), int(j - d_j_fx(i, j, k)), int(k - d_k_fx(i, j, k))) .gt. 0) then
					extrapolatedsubfxsupi(i, j, k) = pfx
					extrapolatedsubfxsupj(i, j, k) = pfx
					extrapolatedsubfxsupk(i, j, k) = pfx
					if (((d_i_fx(i, j, k) .ne. 0.0d0) .and. (d_j_fx(i, j, k) .ne. 0.0d0)) .and. (d_k_fx(i, j, k) .ne. 0.0d0)) then
					k1fx(i, j, k) = ((extrapolatedsubfxsupi(i, j, k) + extrapolatedsubfxsupj(i, j, k)) + extrapolatedsubfxsupk(i, j, k)) / 3.0d0
					else
					if (((d_i_fx(i, j, k) .ne. 0.0d0) .and. (d_j_fx(i, j, k) .ne. 0.0d0)) .and. (d_k_fx(i, j, k) .eq. 0.0d0)) then
					k1fx(i, j, k) = (extrapolatedsubfxsupi(i, j, k) + extrapolatedsubfxsupj(i, j, k)) / 2.0d0
					extrapolatedsubfxsupk(i, j, k) = k1fx(i, j, k)
					else
					if (((d_i_fx(i, j, k) .ne. 0.0d0) .and. (d_j_fx(i, j, k) .eq. 0.0d0)) .and. (d_k_fx(i, j, k) .ne. 0.0d0)) then
					k1fx(i, j, k) = (extrapolatedsubfxsupi(i, j, k) + extrapolatedsubfxsupk(i, j, k)) / 2.0d0
					extrapolatedsubfxsupj(i, j, k) = k1fx(i, j, k)
					else
					if (((d_i_fx(i, j, k) .ne. 0.0d0) .and. (d_j_fx(i, j, k) .eq. 0.0d0)) .and. (d_k_fx(i, j, k) .eq. 0.0d0)) then
					k1fx(i, j, k) = extrapolatedsubfxsupi(i, j, k)
					extrapolatedsubfxsupj(i, j, k) = k1fx(i, j, k)
					extrapolatedsubfxsupk(i, j, k) = k1fx(i, j, k)
					else
					if (((d_i_fx(i, j, k) .eq. 0.0d0) .and. (d_j_fx(i, j, k) .ne. 0.0d0)) .and. (d_k_fx(i, j, k) .ne. 0.0d0)) then
					k1fx(i, j, k) = (extrapolatedsubfxsupj(i, j, k) + extrapolatedsubfxsupk(i, j, k)) / 2.0d0
					extrapolatedsubfxsupi(i, j, k) = k1fx(i, j, k)
					else
					if (((d_i_fx(i, j, k) .eq. 0.0d0) .and. (d_j_fx(i, j, k) .ne. 0.0d0)) .and. (d_k_fx(i, j, k) .eq. 0.0d0)) then
					k1fx(i, j, k) = extrapolatedsubfxsupj(i, j, k)
					extrapolatedsubfxsupi(i, j, k) = k1fx(i, j, k)
					extrapolatedsubfxsupk(i, j, k) = k1fx(i, j, k)
					else
					if (((d_i_fx(i, j, k) .eq. 0.0d0) .and. (d_j_fx(i, j, k) .eq. 0.0d0)) .and. (d_k_fx(i, j, k) .ne. 0.0d0)) then
					k1fx(i, j, k) = extrapolatedsubfxsupk(i, j, k)
					extrapolatedsubfxsupi(i, j, k) = k1fx(i, j, k)
					extrapolatedsubfxsupj(i, j, k) = k1fx(i, j, k)
					end if
					end if
					end if
					end if
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fy(i, j, k) .ne. 0 .or. d_j_fy(i, j, k) .ne. 0 .or. d_k_fy(i, j, k) .ne. 0)) then
				if (FOV_1(int(i - d_i_fy(i, j, k)), int(j - d_j_fy(i, j, k)), int(k - d_k_fy(i, j, k))) .gt. 0) then
					extrapolatedsubfysupi(i, j, k) = pfy
					extrapolatedsubfysupj(i, j, k) = pfy
					extrapolatedsubfysupk(i, j, k) = pfy
					if (((d_i_fy(i, j, k) .ne. 0.0d0) .and. (d_j_fy(i, j, k) .ne. 0.0d0)) .and. (d_k_fy(i, j, k) .ne. 0.0d0)) then
					k1fy(i, j, k) = ((extrapolatedsubfysupi(i, j, k) + extrapolatedsubfysupj(i, j, k)) + extrapolatedsubfysupk(i, j, k)) / 3.0d0
					else
					if (((d_i_fy(i, j, k) .ne. 0.0d0) .and. (d_j_fy(i, j, k) .ne. 0.0d0)) .and. (d_k_fy(i, j, k) .eq. 0.0d0)) then
					k1fy(i, j, k) = (extrapolatedsubfysupi(i, j, k) + extrapolatedsubfysupj(i, j, k)) / 2.0d0
					extrapolatedsubfysupk(i, j, k) = k1fy(i, j, k)
					else
					if (((d_i_fy(i, j, k) .ne. 0.0d0) .and. (d_j_fy(i, j, k) .eq. 0.0d0)) .and. (d_k_fy(i, j, k) .ne. 0.0d0)) then
					k1fy(i, j, k) = (extrapolatedsubfysupi(i, j, k) + extrapolatedsubfysupk(i, j, k)) / 2.0d0
					extrapolatedsubfysupj(i, j, k) = k1fy(i, j, k)
					else
					if (((d_i_fy(i, j, k) .ne. 0.0d0) .and. (d_j_fy(i, j, k) .eq. 0.0d0)) .and. (d_k_fy(i, j, k) .eq. 0.0d0)) then
					k1fy(i, j, k) = extrapolatedsubfysupi(i, j, k)
					extrapolatedsubfysupj(i, j, k) = k1fy(i, j, k)
					extrapolatedsubfysupk(i, j, k) = k1fy(i, j, k)
					else
					if (((d_i_fy(i, j, k) .eq. 0.0d0) .and. (d_j_fy(i, j, k) .ne. 0.0d0)) .and. (d_k_fy(i, j, k) .ne. 0.0d0)) then
					k1fy(i, j, k) = (extrapolatedsubfysupj(i, j, k) + extrapolatedsubfysupk(i, j, k)) / 2.0d0
					extrapolatedsubfysupi(i, j, k) = k1fy(i, j, k)
					else
					if (((d_i_fy(i, j, k) .eq. 0.0d0) .and. (d_j_fy(i, j, k) .ne. 0.0d0)) .and. (d_k_fy(i, j, k) .eq. 0.0d0)) then
					k1fy(i, j, k) = extrapolatedsubfysupj(i, j, k)
					extrapolatedsubfysupi(i, j, k) = k1fy(i, j, k)
					extrapolatedsubfysupk(i, j, k) = k1fy(i, j, k)
					else
					if (((d_i_fy(i, j, k) .eq. 0.0d0) .and. (d_j_fy(i, j, k) .eq. 0.0d0)) .and. (d_k_fy(i, j, k) .ne. 0.0d0)) then
					k1fy(i, j, k) = extrapolatedsubfysupk(i, j, k)
					extrapolatedsubfysupi(i, j, k) = k1fy(i, j, k)
					extrapolatedsubfysupj(i, j, k) = k1fy(i, j, k)
					end if
					end if
					end if
					end if
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fz(i, j, k) .ne. 0 .or. d_j_fz(i, j, k) .ne. 0 .or. d_k_fz(i, j, k) .ne. 0)) then
				if (FOV_1(int(i - d_i_fz(i, j, k)), int(j - d_j_fz(i, j, k)), int(k - d_k_fz(i, j, k))) .gt. 0) then
					extrapolatedsubfzsupi(i, j, k) = pfz
					extrapolatedsubfzsupj(i, j, k) = pfz
					extrapolatedsubfzsupk(i, j, k) = pfz
					if (((d_i_fz(i, j, k) .ne. 0.0d0) .and. (d_j_fz(i, j, k) .ne. 0.0d0)) .and. (d_k_fz(i, j, k) .ne. 0.0d0)) then
					k1fz(i, j, k) = ((extrapolatedsubfzsupi(i, j, k) + extrapolatedsubfzsupj(i, j, k)) + extrapolatedsubfzsupk(i, j, k)) / 3.0d0
					else
					if (((d_i_fz(i, j, k) .ne. 0.0d0) .and. (d_j_fz(i, j, k) .ne. 0.0d0)) .and. (d_k_fz(i, j, k) .eq. 0.0d0)) then
					k1fz(i, j, k) = (extrapolatedsubfzsupi(i, j, k) + extrapolatedsubfzsupj(i, j, k)) / 2.0d0
					extrapolatedsubfzsupk(i, j, k) = k1fz(i, j, k)
					else
					if (((d_i_fz(i, j, k) .ne. 0.0d0) .and. (d_j_fz(i, j, k) .eq. 0.0d0)) .and. (d_k_fz(i, j, k) .ne. 0.0d0)) then
					k1fz(i, j, k) = (extrapolatedsubfzsupi(i, j, k) + extrapolatedsubfzsupk(i, j, k)) / 2.0d0
					extrapolatedsubfzsupj(i, j, k) = k1fz(i, j, k)
					else
					if (((d_i_fz(i, j, k) .ne. 0.0d0) .and. (d_j_fz(i, j, k) .eq. 0.0d0)) .and. (d_k_fz(i, j, k) .eq. 0.0d0)) then
					k1fz(i, j, k) = extrapolatedsubfzsupi(i, j, k)
					extrapolatedsubfzsupj(i, j, k) = k1fz(i, j, k)
					extrapolatedsubfzsupk(i, j, k) = k1fz(i, j, k)
					else
					if (((d_i_fz(i, j, k) .eq. 0.0d0) .and. (d_j_fz(i, j, k) .ne. 0.0d0)) .and. (d_k_fz(i, j, k) .ne. 0.0d0)) then
					k1fz(i, j, k) = (extrapolatedsubfzsupj(i, j, k) + extrapolatedsubfzsupk(i, j, k)) / 2.0d0
					extrapolatedsubfzsupi(i, j, k) = k1fz(i, j, k)
					else
					if (((d_i_fz(i, j, k) .eq. 0.0d0) .and. (d_j_fz(i, j, k) .ne. 0.0d0)) .and. (d_k_fz(i, j, k) .eq. 0.0d0)) then
					k1fz(i, j, k) = extrapolatedsubfzsupj(i, j, k)
					extrapolatedsubfzsupi(i, j, k) = k1fz(i, j, k)
					extrapolatedsubfzsupk(i, j, k) = k1fz(i, j, k)
					else
					if (((d_i_fz(i, j, k) .eq. 0.0d0) .and. (d_j_fz(i, j, k) .eq. 0.0d0)) .and. (d_k_fz(i, j, k) .ne. 0.0d0)) then
					k1fz(i, j, k) = extrapolatedsubfzsupk(i, j, k)
					extrapolatedsubfzsupi(i, j, k) = k1fz(i, j, k)
					extrapolatedsubfzsupj(i, j, k) = k1fz(i, j, k)
					end if
					end if
					end if
					end if
					end if
					end if
					end if
				end if
			end if
		end if
		if ((FOV_yLower(i, j, k) .gt. 0) .and. ((j + 1 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 1), k) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 2), k) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 3), k) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 1), k) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 1), k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 1), k) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 1), k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 1), k) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 1), k) .gt. 0)) .or. (j + 1 .le. cctk_lsh(2) .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 1), int(k + 1)) .gt. 0)) .or. (j + 1 .le. cctk_lsh(2) .and. k - 1 .ge. 1 .and. (FOV_1(i, int(j + 1), int(k - 1)) .gt. 0)) .or. (j + 1 .le. cctk_lsh(2) .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 1), int(k + 2)) .gt. 0)) .or. (j + 1 .le. cctk_lsh(2) .and. k - 2 .ge. 1 .and. (FOV_1(i, int(j + 1), int(k - 2)) .gt. 0)) .or. (j + 1 .le. cctk_lsh(2) .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 1), int(k + 3)) .gt. 0)) .or. (j + 1 .le. cctk_lsh(2) .and. k - 3 .ge. 1 .and. (FOV_1(i, int(j + 1), int(k - 3)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 2), k) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 2), k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 2), k) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 2), k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 2), k) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 2), k) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 2), int(k + 1)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. k - 1 .ge. 1 .and. (FOV_1(i, int(j + 2), int(k - 1)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 2), int(k + 2)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. k - 2 .ge. 1 .and. (FOV_1(i, int(j + 2), int(k - 2)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 2), int(k + 3)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. k - 3 .ge. 1 .and. (FOV_1(i, int(j + 2), int(k - 3)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 3), k) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 3), k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 3), k) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 3), k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 3), k) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 3), k) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 3), int(k + 1)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. k - 1 .ge. 1 .and. (FOV_1(i, int(j + 3), int(k - 1)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 3), int(k + 2)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. k - 2 .ge. 1 .and. (FOV_1(i, int(j + 3), int(k - 2)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 3), int(k + 3)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. k - 3 .ge. 1 .and. (FOV_1(i, int(j + 3), int(k - 3)) .gt. 0))))) then
				k1rho(i, 4 - j, k) = 0.0d0
				k1mx(i, 4 - j, k) = 0.0d0
				k1my(i, 4 - j, k) = 0.0d0
				k1mz(i, 4 - j, k) = 0.0d0
				k1p(i, 4 - j, k) = 0.0d0
				k1fx(i, 4 - j, k) = 0.0d0
				k1fy(i, 4 - j, k) = 0.0d0
				k1fz(i, 4 - j, k) = 0.0d0
		end if
		if ((FOV_yUpper(i, j, k) .gt. 0) .and. ((j - 1 .ge. 1 .and. (FOV_1(i, int(j - 1), k) .gt. 0)) .or. (j - 2 .ge. 1 .and. (FOV_1(i, int(j - 2), k) .gt. 0)) .or. (j - 3 .ge. 1 .and. (FOV_1(i, int(j - 3), k) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 1), k) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 1), k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 1), k) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 1), k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 1), k) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 1), k) .gt. 0)) .or. (j - 1 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 1), int(k + 1)) .gt. 0)) .or. (j - 1 .ge. 1 .and. k - 1 .ge. 1 .and. (FOV_1(i, int(j - 1), int(k - 1)) .gt. 0)) .or. (j - 1 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 1), int(k + 2)) .gt. 0)) .or. (j - 1 .ge. 1 .and. k - 2 .ge. 1 .and. (FOV_1(i, int(j - 1), int(k - 2)) .gt. 0)) .or. (j - 1 .ge. 1 .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 1), int(k + 3)) .gt. 0)) .or. (j - 1 .ge. 1 .and. k - 3 .ge. 1 .and. (FOV_1(i, int(j - 1), int(k - 3)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 2), k) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 2), k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 2), k) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 2), k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 2), k) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 2), k) .gt. 0)) .or. (j - 2 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 2), int(k + 1)) .gt. 0)) .or. (j - 2 .ge. 1 .and. k - 1 .ge. 1 .and. (FOV_1(i, int(j - 2), int(k - 1)) .gt. 0)) .or. (j - 2 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 2), int(k + 2)) .gt. 0)) .or. (j - 2 .ge. 1 .and. k - 2 .ge. 1 .and. (FOV_1(i, int(j - 2), int(k - 2)) .gt. 0)) .or. (j - 2 .ge. 1 .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 2), int(k + 3)) .gt. 0)) .or. (j - 2 .ge. 1 .and. k - 3 .ge. 1 .and. (FOV_1(i, int(j - 2), int(k - 3)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 3), k) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 3), k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 3), k) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 3), k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 3), k) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 3), k) .gt. 0)) .or. (j - 3 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 3), int(k + 1)) .gt. 0)) .or. (j - 3 .ge. 1 .and. k - 1 .ge. 1 .and. (FOV_1(i, int(j - 3), int(k - 1)) .gt. 0)) .or. (j - 3 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 3), int(k + 2)) .gt. 0)) .or. (j - 3 .ge. 1 .and. k - 2 .ge. 1 .and. (FOV_1(i, int(j - 3), int(k - 2)) .gt. 0)) .or. (j - 3 .ge. 1 .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 3), int(k + 3)) .gt. 0)) .or. (j - 3 .ge. 1 .and. k - 3 .ge. 1 .and. (FOV_1(i, int(j - 3), int(k - 3)) .gt. 0))))) then
			k1rho(i, j, k) = 0.0d0
			k1mx(i, j, k) = 0.0d0
			k1my(i, j, k) = 0.0d0
			k1mz(i, j, k) = 0.0d0
			k1p(i, j, k) = 0.0d0
			k1fx(i, j, k) = 0.0d0
			k1fy(i, j, k) = 0.0d0
			k1fz(i, j, k) = 0.0d0
		end if
		if ((FOV_zLower(i, j, k) .gt. 0) .and. ((k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, j, int(k + 1)) .gt. 0)) .or. (k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, j, int(k + 2)) .gt. 0)) .or. (k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, j, int(k + 3)) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(int(i + 1), j, int(k + 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(int(i - 1), j, int(k + 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(int(i + 2), j, int(k + 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(int(i - 2), j, int(k + 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(int(i + 3), j, int(k + 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(int(i - 3), j, int(k + 1)) .gt. 0)) .or. (j + 1 .le. cctk_lsh(2) .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 1), int(k + 1)) .gt. 0)) .or. (j - 1 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 1), int(k + 1)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 2), int(k + 1)) .gt. 0)) .or. (j - 2 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 2), int(k + 1)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 3), int(k + 1)) .gt. 0)) .or. (j - 3 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 3), int(k + 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(int(i + 1), j, int(k + 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(int(i - 1), j, int(k + 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(int(i + 2), j, int(k + 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(int(i - 2), j, int(k + 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(int(i + 3), j, int(k + 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(int(i - 3), j, int(k + 2)) .gt. 0)) .or. (j + 1 .le. cctk_lsh(2) .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 1), int(k + 2)) .gt. 0)) .or. (j - 1 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 1), int(k + 2)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 2), int(k + 2)) .gt. 0)) .or. (j - 2 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 2), int(k + 2)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 3), int(k + 2)) .gt. 0)) .or. (j - 3 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 3), int(k + 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(int(i + 1), j, int(k + 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(int(i - 1), j, int(k + 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(int(i + 2), j, int(k + 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(int(i - 2), j, int(k + 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(int(i + 3), j, int(k + 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(int(i - 3), j, int(k + 3)) .gt. 0)) .or. (j + 1 .le. cctk_lsh(2) .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 1), int(k + 3)) .gt. 0)) .or. (j - 1 .ge. 1 .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 1), int(k + 3)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 2), int(k + 3)) .gt. 0)) .or. (j - 2 .ge. 1 .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 2), int(k + 3)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 3), int(k + 3)) .gt. 0)) .or. (j - 3 .ge. 1 .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 3), int(k + 3)) .gt. 0))))) then
				k1rho(i, j, 4 - k) = 0.0d0
				k1mx(i, j, 4 - k) = 0.0d0
				k1my(i, j, 4 - k) = 0.0d0
				k1mz(i, j, 4 - k) = 0.0d0
				k1p(i, j, 4 - k) = 0.0d0
				k1fx(i, j, 4 - k) = 0.0d0
				k1fy(i, j, 4 - k) = 0.0d0
				k1fz(i, j, 4 - k) = 0.0d0
		end if
		if ((FOV_zUpper(i, j, k) .gt. 0) .and. ((k - 1 .ge. 1 .and. (FOV_1(i, j, int(k - 1)) .gt. 0)) .or. (k - 2 .ge. 1 .and. (FOV_1(i, j, int(k - 2)) .gt. 0)) .or. (k - 3 .ge. 1 .and. (FOV_1(i, j, int(k - 3)) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. k - 1 .ge. 1 .and. (FOV_1(int(i + 1), j, int(k - 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. k - 1 .ge. 1 .and. (FOV_1(int(i - 1), j, int(k - 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. k - 1 .ge. 1 .and. (FOV_1(int(i + 2), j, int(k - 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. k - 1 .ge. 1 .and. (FOV_1(int(i - 2), j, int(k - 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. k - 1 .ge. 1 .and. (FOV_1(int(i + 3), j, int(k - 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. k - 1 .ge. 1 .and. (FOV_1(int(i - 3), j, int(k - 1)) .gt. 0)) .or. (j + 1 .le. cctk_lsh(2) .and. k - 1 .ge. 1 .and. (FOV_1(i, int(j + 1), int(k - 1)) .gt. 0)) .or. (j - 1 .ge. 1 .and. k - 1 .ge. 1 .and. (FOV_1(i, int(j - 1), int(k - 1)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. k - 1 .ge. 1 .and. (FOV_1(i, int(j + 2), int(k - 1)) .gt. 0)) .or. (j - 2 .ge. 1 .and. k - 1 .ge. 1 .and. (FOV_1(i, int(j - 2), int(k - 1)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. k - 1 .ge. 1 .and. (FOV_1(i, int(j + 3), int(k - 1)) .gt. 0)) .or. (j - 3 .ge. 1 .and. k - 1 .ge. 1 .and. (FOV_1(i, int(j - 3), int(k - 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. k - 2 .ge. 1 .and. (FOV_1(int(i + 1), j, int(k - 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. k - 2 .ge. 1 .and. (FOV_1(int(i - 1), j, int(k - 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. k - 2 .ge. 1 .and. (FOV_1(int(i + 2), j, int(k - 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. k - 2 .ge. 1 .and. (FOV_1(int(i - 2), j, int(k - 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. k - 2 .ge. 1 .and. (FOV_1(int(i + 3), j, int(k - 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. k - 2 .ge. 1 .and. (FOV_1(int(i - 3), j, int(k - 2)) .gt. 0)) .or. (j + 1 .le. cctk_lsh(2) .and. k - 2 .ge. 1 .and. (FOV_1(i, int(j + 1), int(k - 2)) .gt. 0)) .or. (j - 1 .ge. 1 .and. k - 2 .ge. 1 .and. (FOV_1(i, int(j - 1), int(k - 2)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. k - 2 .ge. 1 .and. (FOV_1(i, int(j + 2), int(k - 2)) .gt. 0)) .or. (j - 2 .ge. 1 .and. k - 2 .ge. 1 .and. (FOV_1(i, int(j - 2), int(k - 2)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. k - 2 .ge. 1 .and. (FOV_1(i, int(j + 3), int(k - 2)) .gt. 0)) .or. (j - 3 .ge. 1 .and. k - 2 .ge. 1 .and. (FOV_1(i, int(j - 3), int(k - 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. k - 3 .ge. 1 .and. (FOV_1(int(i + 1), j, int(k - 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. k - 3 .ge. 1 .and. (FOV_1(int(i - 1), j, int(k - 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. k - 3 .ge. 1 .and. (FOV_1(int(i + 2), j, int(k - 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. k - 3 .ge. 1 .and. (FOV_1(int(i - 2), j, int(k - 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. k - 3 .ge. 1 .and. (FOV_1(int(i + 3), j, int(k - 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. k - 3 .ge. 1 .and. (FOV_1(int(i - 3), j, int(k - 3)) .gt. 0)) .or. (j + 1 .le. cctk_lsh(2) .and. k - 3 .ge. 1 .and. (FOV_1(i, int(j + 1), int(k - 3)) .gt. 0)) .or. (j - 1 .ge. 1 .and. k - 3 .ge. 1 .and. (FOV_1(i, int(j - 1), int(k - 3)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. k - 3 .ge. 1 .and. (FOV_1(i, int(j + 2), int(k - 3)) .gt. 0)) .or. (j - 2 .ge. 1 .and. k - 3 .ge. 1 .and. (FOV_1(i, int(j - 2), int(k - 3)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. k - 3 .ge. 1 .and. (FOV_1(i, int(j + 3), int(k - 3)) .gt. 0)) .or. (j - 3 .ge. 1 .and. k - 3 .ge. 1 .and. (FOV_1(i, int(j - 3), int(k - 3)) .gt. 0))))) then
			k1rho(i, j, k) = 0.0d0
			k1mx(i, j, k) = 0.0d0
			k1my(i, j, k) = 0.0d0
			k1mz(i, j, k) = 0.0d0
			k1p(i, j, k) = 0.0d0
			k1fx(i, j, k) = 0.0d0
			k1fy(i, j, k) = 0.0d0
			k1fz(i, j, k) = 0.0d0
		end if
		if (FOV_1(i, j, k) .gt. 0) then
			if ((d_i_Null(i, j, k) .ne. 0 .or. d_j_Null(i, j, k) .ne. 0 .or. d_k_Null(i, j, k) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_Null, k1Null, extrapolatedsubNullsupi, i, d_j_Null, k1Null, extrapolatedsubNullsupj, j, d_k_Null, k1Null, extrapolatedsubNullsupk, k, k1Null, FOV_3)
			end if
		end if
				end do
			end do
		end do
		do k = kStart, kEnd
			do j = jStart, jEnd
				do i = iStart, iEnd
					if ((FOV_xLower(i, j, k) .gt. 0) .and. ((i + 1 .le. cctk_lsh(1) .and. (FOV_3(int(i + 1), j, k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. (FOV_3(int(i + 2), j, k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. (FOV_3(int(i + 3), j, k) .gt. 0)))) then
						extrapolatedsubrhosupi(i, j, k) = k1rho(i, j, k)
						extrapolatedsubrhosupj(i, j, k) = k1rho(i, j, k)
						extrapolatedsubrhosupk(i, j, k) = k1rho(i, j, k)
						extrapolatedsubmxsupi(i, j, k) = k1mx(i, j, k)
						extrapolatedsubmxsupj(i, j, k) = k1mx(i, j, k)
						extrapolatedsubmxsupk(i, j, k) = k1mx(i, j, k)
						extrapolatedsubmysupi(i, j, k) = k1my(i, j, k)
						extrapolatedsubmysupj(i, j, k) = k1my(i, j, k)
						extrapolatedsubmysupk(i, j, k) = k1my(i, j, k)
						extrapolatedsubmzsupi(i, j, k) = k1mz(i, j, k)
						extrapolatedsubmzsupj(i, j, k) = k1mz(i, j, k)
						extrapolatedsubmzsupk(i, j, k) = k1mz(i, j, k)
						extrapolatedsubNullsupi(i, j, k) = k1Null(i, j, k)
						extrapolatedsubNullsupj(i, j, k) = k1Null(i, j, k)
						extrapolatedsubNullsupk(i, j, k) = k1Null(i, j, k)
						extrapolatedsubfxsupi(i, j, k) = k1fx(i, j, k)
						extrapolatedsubfxsupj(i, j, k) = k1fx(i, j, k)
						extrapolatedsubfxsupk(i, j, k) = k1fx(i, j, k)
						extrapolatedsubfysupi(i, j, k) = k1fy(i, j, k)
						extrapolatedsubfysupj(i, j, k) = k1fy(i, j, k)
						extrapolatedsubfysupk(i, j, k) = k1fy(i, j, k)
						extrapolatedsubfzsupi(i, j, k) = k1fz(i, j, k)
						extrapolatedsubfzsupj(i, j, k) = k1fz(i, j, k)
						extrapolatedsubfzsupk(i, j, k) = k1fz(i, j, k)
						extrapolatedsubpsupi(i, j, k) = k1p(i, j, k)
						extrapolatedsubpsupj(i, j, k) = k1p(i, j, k)
						extrapolatedsubpsupk(i, j, k) = k1p(i, j, k)
					end if
					if ((FOV_xUpper(i, j, k) .gt. 0) .and. ((i - 1 .ge. 1 .and. (FOV_3(int(i - 1), j, k) .gt. 0)) .or. (i - 2 .ge. 1 .and. (FOV_3(int(i - 2), j, k) .gt. 0)) .or. (i - 3 .ge. 1 .and. (FOV_3(int(i - 3), j, k) .gt. 0)))) then
						extrapolatedsubrhosupi(i, j, k) = k1rho(i, j, k)
						extrapolatedsubrhosupj(i, j, k) = k1rho(i, j, k)
						extrapolatedsubrhosupk(i, j, k) = k1rho(i, j, k)
						extrapolatedsubmxsupi(i, j, k) = k1mx(i, j, k)
						extrapolatedsubmxsupj(i, j, k) = k1mx(i, j, k)
						extrapolatedsubmxsupk(i, j, k) = k1mx(i, j, k)
						extrapolatedsubmysupi(i, j, k) = k1my(i, j, k)
						extrapolatedsubmysupj(i, j, k) = k1my(i, j, k)
						extrapolatedsubmysupk(i, j, k) = k1my(i, j, k)
						extrapolatedsubmzsupi(i, j, k) = k1mz(i, j, k)
						extrapolatedsubmzsupj(i, j, k) = k1mz(i, j, k)
						extrapolatedsubmzsupk(i, j, k) = k1mz(i, j, k)
						extrapolatedsubNullsupi(i, j, k) = k1Null(i, j, k)
						extrapolatedsubNullsupj(i, j, k) = k1Null(i, j, k)
						extrapolatedsubNullsupk(i, j, k) = k1Null(i, j, k)
						extrapolatedsubfxsupi(i, j, k) = k1fx(i, j, k)
						extrapolatedsubfxsupj(i, j, k) = k1fx(i, j, k)
						extrapolatedsubfxsupk(i, j, k) = k1fx(i, j, k)
						extrapolatedsubfysupi(i, j, k) = k1fy(i, j, k)
						extrapolatedsubfysupj(i, j, k) = k1fy(i, j, k)
						extrapolatedsubfysupk(i, j, k) = k1fy(i, j, k)
						extrapolatedsubfzsupi(i, j, k) = k1fz(i, j, k)
						extrapolatedsubfzsupj(i, j, k) = k1fz(i, j, k)
						extrapolatedsubfzsupk(i, j, k) = k1fz(i, j, k)
						extrapolatedsubpsupi(i, j, k) = k1p(i, j, k)
						extrapolatedsubpsupj(i, j, k) = k1p(i, j, k)
						extrapolatedsubpsupk(i, j, k) = k1p(i, j, k)
					end if
					if ((FOV_yLower(i, j, k) .gt. 0) .and. ((j + 1 .le. cctk_lsh(2) .and. (FOV_3(i, int(j + 1), k) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. (FOV_3(i, int(j + 2), k) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. (FOV_3(i, int(j + 3), k) .gt. 0)))) then
						extrapolatedsubrhosupi(i, j, k) = k1rho(i, j, k)
						extrapolatedsubrhosupj(i, j, k) = k1rho(i, j, k)
						extrapolatedsubrhosupk(i, j, k) = k1rho(i, j, k)
						extrapolatedsubmxsupi(i, j, k) = k1mx(i, j, k)
						extrapolatedsubmxsupj(i, j, k) = k1mx(i, j, k)
						extrapolatedsubmxsupk(i, j, k) = k1mx(i, j, k)
						extrapolatedsubmysupi(i, j, k) = k1my(i, j, k)
						extrapolatedsubmysupj(i, j, k) = k1my(i, j, k)
						extrapolatedsubmysupk(i, j, k) = k1my(i, j, k)
						extrapolatedsubmzsupi(i, j, k) = k1mz(i, j, k)
						extrapolatedsubmzsupj(i, j, k) = k1mz(i, j, k)
						extrapolatedsubmzsupk(i, j, k) = k1mz(i, j, k)
						extrapolatedsubNullsupi(i, j, k) = k1Null(i, j, k)
						extrapolatedsubNullsupj(i, j, k) = k1Null(i, j, k)
						extrapolatedsubNullsupk(i, j, k) = k1Null(i, j, k)
						extrapolatedsubfxsupi(i, j, k) = k1fx(i, j, k)
						extrapolatedsubfxsupj(i, j, k) = k1fx(i, j, k)
						extrapolatedsubfxsupk(i, j, k) = k1fx(i, j, k)
						extrapolatedsubfysupi(i, j, k) = k1fy(i, j, k)
						extrapolatedsubfysupj(i, j, k) = k1fy(i, j, k)
						extrapolatedsubfysupk(i, j, k) = k1fy(i, j, k)
						extrapolatedsubfzsupi(i, j, k) = k1fz(i, j, k)
						extrapolatedsubfzsupj(i, j, k) = k1fz(i, j, k)
						extrapolatedsubfzsupk(i, j, k) = k1fz(i, j, k)
						extrapolatedsubpsupi(i, j, k) = k1p(i, j, k)
						extrapolatedsubpsupj(i, j, k) = k1p(i, j, k)
						extrapolatedsubpsupk(i, j, k) = k1p(i, j, k)
					end if
					if ((FOV_yUpper(i, j, k) .gt. 0) .and. ((j - 1 .ge. 1 .and. (FOV_3(i, int(j - 1), k) .gt. 0)) .or. (j - 2 .ge. 1 .and. (FOV_3(i, int(j - 2), k) .gt. 0)) .or. (j - 3 .ge. 1 .and. (FOV_3(i, int(j - 3), k) .gt. 0)))) then
						extrapolatedsubrhosupi(i, j, k) = k1rho(i, j, k)
						extrapolatedsubrhosupj(i, j, k) = k1rho(i, j, k)
						extrapolatedsubrhosupk(i, j, k) = k1rho(i, j, k)
						extrapolatedsubmxsupi(i, j, k) = k1mx(i, j, k)
						extrapolatedsubmxsupj(i, j, k) = k1mx(i, j, k)
						extrapolatedsubmxsupk(i, j, k) = k1mx(i, j, k)
						extrapolatedsubmysupi(i, j, k) = k1my(i, j, k)
						extrapolatedsubmysupj(i, j, k) = k1my(i, j, k)
						extrapolatedsubmysupk(i, j, k) = k1my(i, j, k)
						extrapolatedsubmzsupi(i, j, k) = k1mz(i, j, k)
						extrapolatedsubmzsupj(i, j, k) = k1mz(i, j, k)
						extrapolatedsubmzsupk(i, j, k) = k1mz(i, j, k)
						extrapolatedsubNullsupi(i, j, k) = k1Null(i, j, k)
						extrapolatedsubNullsupj(i, j, k) = k1Null(i, j, k)
						extrapolatedsubNullsupk(i, j, k) = k1Null(i, j, k)
						extrapolatedsubfxsupi(i, j, k) = k1fx(i, j, k)
						extrapolatedsubfxsupj(i, j, k) = k1fx(i, j, k)
						extrapolatedsubfxsupk(i, j, k) = k1fx(i, j, k)
						extrapolatedsubfysupi(i, j, k) = k1fy(i, j, k)
						extrapolatedsubfysupj(i, j, k) = k1fy(i, j, k)
						extrapolatedsubfysupk(i, j, k) = k1fy(i, j, k)
						extrapolatedsubfzsupi(i, j, k) = k1fz(i, j, k)
						extrapolatedsubfzsupj(i, j, k) = k1fz(i, j, k)
						extrapolatedsubfzsupk(i, j, k) = k1fz(i, j, k)
						extrapolatedsubpsupi(i, j, k) = k1p(i, j, k)
						extrapolatedsubpsupj(i, j, k) = k1p(i, j, k)
						extrapolatedsubpsupk(i, j, k) = k1p(i, j, k)
					end if
					if ((FOV_zLower(i, j, k) .gt. 0) .and. ((k + 1 .le. cctk_lsh(3) .and. (FOV_3(i, j, int(k + 1)) .gt. 0)) .or. (k + 2 .le. cctk_lsh(3) .and. (FOV_3(i, j, int(k + 2)) .gt. 0)) .or. (k + 3 .le. cctk_lsh(3) .and. (FOV_3(i, j, int(k + 3)) .gt. 0)))) then
						extrapolatedsubrhosupi(i, j, k) = k1rho(i, j, k)
						extrapolatedsubrhosupj(i, j, k) = k1rho(i, j, k)
						extrapolatedsubrhosupk(i, j, k) = k1rho(i, j, k)
						extrapolatedsubmxsupi(i, j, k) = k1mx(i, j, k)
						extrapolatedsubmxsupj(i, j, k) = k1mx(i, j, k)
						extrapolatedsubmxsupk(i, j, k) = k1mx(i, j, k)
						extrapolatedsubmysupi(i, j, k) = k1my(i, j, k)
						extrapolatedsubmysupj(i, j, k) = k1my(i, j, k)
						extrapolatedsubmysupk(i, j, k) = k1my(i, j, k)
						extrapolatedsubmzsupi(i, j, k) = k1mz(i, j, k)
						extrapolatedsubmzsupj(i, j, k) = k1mz(i, j, k)
						extrapolatedsubmzsupk(i, j, k) = k1mz(i, j, k)
						extrapolatedsubNullsupi(i, j, k) = k1Null(i, j, k)
						extrapolatedsubNullsupj(i, j, k) = k1Null(i, j, k)
						extrapolatedsubNullsupk(i, j, k) = k1Null(i, j, k)
						extrapolatedsubfxsupi(i, j, k) = k1fx(i, j, k)
						extrapolatedsubfxsupj(i, j, k) = k1fx(i, j, k)
						extrapolatedsubfxsupk(i, j, k) = k1fx(i, j, k)
						extrapolatedsubfysupi(i, j, k) = k1fy(i, j, k)
						extrapolatedsubfysupj(i, j, k) = k1fy(i, j, k)
						extrapolatedsubfysupk(i, j, k) = k1fy(i, j, k)
						extrapolatedsubfzsupi(i, j, k) = k1fz(i, j, k)
						extrapolatedsubfzsupj(i, j, k) = k1fz(i, j, k)
						extrapolatedsubfzsupk(i, j, k) = k1fz(i, j, k)
						extrapolatedsubpsupi(i, j, k) = k1p(i, j, k)
						extrapolatedsubpsupj(i, j, k) = k1p(i, j, k)
						extrapolatedsubpsupk(i, j, k) = k1p(i, j, k)
					end if
					if ((FOV_zUpper(i, j, k) .gt. 0) .and. ((k - 1 .ge. 1 .and. (FOV_3(i, j, int(k - 1)) .gt. 0)) .or. (k - 2 .ge. 1 .and. (FOV_3(i, j, int(k - 2)) .gt. 0)) .or. (k - 3 .ge. 1 .and. (FOV_3(i, j, int(k - 3)) .gt. 0)))) then
						extrapolatedsubrhosupi(i, j, k) = k1rho(i, j, k)
						extrapolatedsubrhosupj(i, j, k) = k1rho(i, j, k)
						extrapolatedsubrhosupk(i, j, k) = k1rho(i, j, k)
						extrapolatedsubmxsupi(i, j, k) = k1mx(i, j, k)
						extrapolatedsubmxsupj(i, j, k) = k1mx(i, j, k)
						extrapolatedsubmxsupk(i, j, k) = k1mx(i, j, k)
						extrapolatedsubmysupi(i, j, k) = k1my(i, j, k)
						extrapolatedsubmysupj(i, j, k) = k1my(i, j, k)
						extrapolatedsubmysupk(i, j, k) = k1my(i, j, k)
						extrapolatedsubmzsupi(i, j, k) = k1mz(i, j, k)
						extrapolatedsubmzsupj(i, j, k) = k1mz(i, j, k)
						extrapolatedsubmzsupk(i, j, k) = k1mz(i, j, k)
						extrapolatedsubNullsupi(i, j, k) = k1Null(i, j, k)
						extrapolatedsubNullsupj(i, j, k) = k1Null(i, j, k)
						extrapolatedsubNullsupk(i, j, k) = k1Null(i, j, k)
						extrapolatedsubfxsupi(i, j, k) = k1fx(i, j, k)
						extrapolatedsubfxsupj(i, j, k) = k1fx(i, j, k)
						extrapolatedsubfxsupk(i, j, k) = k1fx(i, j, k)
						extrapolatedsubfysupi(i, j, k) = k1fy(i, j, k)
						extrapolatedsubfysupj(i, j, k) = k1fy(i, j, k)
						extrapolatedsubfysupk(i, j, k) = k1fy(i, j, k)
						extrapolatedsubfzsupi(i, j, k) = k1fz(i, j, k)
						extrapolatedsubfzsupj(i, j, k) = k1fz(i, j, k)
						extrapolatedsubfzsupk(i, j, k) = k1fz(i, j, k)
						extrapolatedsubpsupi(i, j, k) = k1p(i, j, k)
						extrapolatedsubpsupj(i, j, k) = k1p(i, j, k)
						extrapolatedsubpsupk(i, j, k) = k1p(i, j, k)
					end if
					if ((FOV_xLower(i, j, k) .gt. 0) .and. ((i + 1 .le. cctk_lsh(1) .and. (FOV_1(int(i + 1), j, k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. (FOV_1(int(i + 2), j, k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. (FOV_1(int(i + 3), j, k) .gt. 0)))) then
						extrapolatedsubNullsupi(i, j, k) = k1Null(i, j, k)
						extrapolatedsubNullsupj(i, j, k) = k1Null(i, j, k)
						extrapolatedsubNullsupk(i, j, k) = k1Null(i, j, k)
					end if
					if ((FOV_xUpper(i, j, k) .gt. 0) .and. ((i - 1 .ge. 1 .and. (FOV_1(int(i - 1), j, k) .gt. 0)) .or. (i - 2 .ge. 1 .and. (FOV_1(int(i - 2), j, k) .gt. 0)) .or. (i - 3 .ge. 1 .and. (FOV_1(int(i - 3), j, k) .gt. 0)))) then
						extrapolatedsubNullsupi(i, j, k) = k1Null(i, j, k)
						extrapolatedsubNullsupj(i, j, k) = k1Null(i, j, k)
						extrapolatedsubNullsupk(i, j, k) = k1Null(i, j, k)
					end if
					if ((FOV_yLower(i, j, k) .gt. 0) .and. ((j + 1 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 1), k) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 2), k) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 3), k) .gt. 0)))) then
						extrapolatedsubrhosupi(i, j, k) = k1rho(i, j, k)
						extrapolatedsubrhosupj(i, j, k) = k1rho(i, j, k)
						extrapolatedsubrhosupk(i, j, k) = k1rho(i, j, k)
						extrapolatedsubmxsupi(i, j, k) = k1mx(i, j, k)
						extrapolatedsubmxsupj(i, j, k) = k1mx(i, j, k)
						extrapolatedsubmxsupk(i, j, k) = k1mx(i, j, k)
						extrapolatedsubmysupi(i, j, k) = k1my(i, j, k)
						extrapolatedsubmysupj(i, j, k) = k1my(i, j, k)
						extrapolatedsubmysupk(i, j, k) = k1my(i, j, k)
						extrapolatedsubmzsupi(i, j, k) = k1mz(i, j, k)
						extrapolatedsubmzsupj(i, j, k) = k1mz(i, j, k)
						extrapolatedsubmzsupk(i, j, k) = k1mz(i, j, k)
						extrapolatedsubNullsupi(i, j, k) = k1Null(i, j, k)
						extrapolatedsubNullsupj(i, j, k) = k1Null(i, j, k)
						extrapolatedsubNullsupk(i, j, k) = k1Null(i, j, k)
						extrapolatedsubfxsupi(i, j, k) = k1fx(i, j, k)
						extrapolatedsubfxsupj(i, j, k) = k1fx(i, j, k)
						extrapolatedsubfxsupk(i, j, k) = k1fx(i, j, k)
						extrapolatedsubfysupi(i, j, k) = k1fy(i, j, k)
						extrapolatedsubfysupj(i, j, k) = k1fy(i, j, k)
						extrapolatedsubfysupk(i, j, k) = k1fy(i, j, k)
						extrapolatedsubfzsupi(i, j, k) = k1fz(i, j, k)
						extrapolatedsubfzsupj(i, j, k) = k1fz(i, j, k)
						extrapolatedsubfzsupk(i, j, k) = k1fz(i, j, k)
						extrapolatedsubpsupi(i, j, k) = k1p(i, j, k)
						extrapolatedsubpsupj(i, j, k) = k1p(i, j, k)
						extrapolatedsubpsupk(i, j, k) = k1p(i, j, k)
					end if
					if ((FOV_yUpper(i, j, k) .gt. 0) .and. ((j - 1 .ge. 1 .and. (FOV_1(i, int(j - 1), k) .gt. 0)) .or. (j - 2 .ge. 1 .and. (FOV_1(i, int(j - 2), k) .gt. 0)) .or. (j - 3 .ge. 1 .and. (FOV_1(i, int(j - 3), k) .gt. 0)))) then
						extrapolatedsubrhosupi(i, j, k) = k1rho(i, j, k)
						extrapolatedsubrhosupj(i, j, k) = k1rho(i, j, k)
						extrapolatedsubrhosupk(i, j, k) = k1rho(i, j, k)
						extrapolatedsubmxsupi(i, j, k) = k1mx(i, j, k)
						extrapolatedsubmxsupj(i, j, k) = k1mx(i, j, k)
						extrapolatedsubmxsupk(i, j, k) = k1mx(i, j, k)
						extrapolatedsubmysupi(i, j, k) = k1my(i, j, k)
						extrapolatedsubmysupj(i, j, k) = k1my(i, j, k)
						extrapolatedsubmysupk(i, j, k) = k1my(i, j, k)
						extrapolatedsubmzsupi(i, j, k) = k1mz(i, j, k)
						extrapolatedsubmzsupj(i, j, k) = k1mz(i, j, k)
						extrapolatedsubmzsupk(i, j, k) = k1mz(i, j, k)
						extrapolatedsubNullsupi(i, j, k) = k1Null(i, j, k)
						extrapolatedsubNullsupj(i, j, k) = k1Null(i, j, k)
						extrapolatedsubNullsupk(i, j, k) = k1Null(i, j, k)
						extrapolatedsubfxsupi(i, j, k) = k1fx(i, j, k)
						extrapolatedsubfxsupj(i, j, k) = k1fx(i, j, k)
						extrapolatedsubfxsupk(i, j, k) = k1fx(i, j, k)
						extrapolatedsubfysupi(i, j, k) = k1fy(i, j, k)
						extrapolatedsubfysupj(i, j, k) = k1fy(i, j, k)
						extrapolatedsubfysupk(i, j, k) = k1fy(i, j, k)
						extrapolatedsubfzsupi(i, j, k) = k1fz(i, j, k)
						extrapolatedsubfzsupj(i, j, k) = k1fz(i, j, k)
						extrapolatedsubfzsupk(i, j, k) = k1fz(i, j, k)
						extrapolatedsubpsupi(i, j, k) = k1p(i, j, k)
						extrapolatedsubpsupj(i, j, k) = k1p(i, j, k)
						extrapolatedsubpsupk(i, j, k) = k1p(i, j, k)
					end if
					if ((FOV_zLower(i, j, k) .gt. 0) .and. ((k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, j, int(k + 1)) .gt. 0)) .or. (k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, j, int(k + 2)) .gt. 0)) .or. (k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, j, int(k + 3)) .gt. 0)))) then
						extrapolatedsubrhosupi(i, j, k) = k1rho(i, j, k)
						extrapolatedsubrhosupj(i, j, k) = k1rho(i, j, k)
						extrapolatedsubrhosupk(i, j, k) = k1rho(i, j, k)
						extrapolatedsubmxsupi(i, j, k) = k1mx(i, j, k)
						extrapolatedsubmxsupj(i, j, k) = k1mx(i, j, k)
						extrapolatedsubmxsupk(i, j, k) = k1mx(i, j, k)
						extrapolatedsubmysupi(i, j, k) = k1my(i, j, k)
						extrapolatedsubmysupj(i, j, k) = k1my(i, j, k)
						extrapolatedsubmysupk(i, j, k) = k1my(i, j, k)
						extrapolatedsubmzsupi(i, j, k) = k1mz(i, j, k)
						extrapolatedsubmzsupj(i, j, k) = k1mz(i, j, k)
						extrapolatedsubmzsupk(i, j, k) = k1mz(i, j, k)
						extrapolatedsubNullsupi(i, j, k) = k1Null(i, j, k)
						extrapolatedsubNullsupj(i, j, k) = k1Null(i, j, k)
						extrapolatedsubNullsupk(i, j, k) = k1Null(i, j, k)
						extrapolatedsubfxsupi(i, j, k) = k1fx(i, j, k)
						extrapolatedsubfxsupj(i, j, k) = k1fx(i, j, k)
						extrapolatedsubfxsupk(i, j, k) = k1fx(i, j, k)
						extrapolatedsubfysupi(i, j, k) = k1fy(i, j, k)
						extrapolatedsubfysupj(i, j, k) = k1fy(i, j, k)
						extrapolatedsubfysupk(i, j, k) = k1fy(i, j, k)
						extrapolatedsubfzsupi(i, j, k) = k1fz(i, j, k)
						extrapolatedsubfzsupj(i, j, k) = k1fz(i, j, k)
						extrapolatedsubfzsupk(i, j, k) = k1fz(i, j, k)
						extrapolatedsubpsupi(i, j, k) = k1p(i, j, k)
						extrapolatedsubpsupj(i, j, k) = k1p(i, j, k)
						extrapolatedsubpsupk(i, j, k) = k1p(i, j, k)
					end if
					if ((FOV_zUpper(i, j, k) .gt. 0) .and. ((k - 1 .ge. 1 .and. (FOV_1(i, j, int(k - 1)) .gt. 0)) .or. (k - 2 .ge. 1 .and. (FOV_1(i, j, int(k - 2)) .gt. 0)) .or. (k - 3 .ge. 1 .and. (FOV_1(i, j, int(k - 3)) .gt. 0)))) then
						extrapolatedsubrhosupi(i, j, k) = k1rho(i, j, k)
						extrapolatedsubrhosupj(i, j, k) = k1rho(i, j, k)
						extrapolatedsubrhosupk(i, j, k) = k1rho(i, j, k)
						extrapolatedsubmxsupi(i, j, k) = k1mx(i, j, k)
						extrapolatedsubmxsupj(i, j, k) = k1mx(i, j, k)
						extrapolatedsubmxsupk(i, j, k) = k1mx(i, j, k)
						extrapolatedsubmysupi(i, j, k) = k1my(i, j, k)
						extrapolatedsubmysupj(i, j, k) = k1my(i, j, k)
						extrapolatedsubmysupk(i, j, k) = k1my(i, j, k)
						extrapolatedsubmzsupi(i, j, k) = k1mz(i, j, k)
						extrapolatedsubmzsupj(i, j, k) = k1mz(i, j, k)
						extrapolatedsubmzsupk(i, j, k) = k1mz(i, j, k)
						extrapolatedsubNullsupi(i, j, k) = k1Null(i, j, k)
						extrapolatedsubNullsupj(i, j, k) = k1Null(i, j, k)
						extrapolatedsubNullsupk(i, j, k) = k1Null(i, j, k)
						extrapolatedsubfxsupi(i, j, k) = k1fx(i, j, k)
						extrapolatedsubfxsupj(i, j, k) = k1fx(i, j, k)
						extrapolatedsubfxsupk(i, j, k) = k1fx(i, j, k)
						extrapolatedsubfysupi(i, j, k) = k1fy(i, j, k)
						extrapolatedsubfysupj(i, j, k) = k1fy(i, j, k)
						extrapolatedsubfysupk(i, j, k) = k1fy(i, j, k)
						extrapolatedsubfzsupi(i, j, k) = k1fz(i, j, k)
						extrapolatedsubfzsupj(i, j, k) = k1fz(i, j, k)
						extrapolatedsubfzsupk(i, j, k) = k1fz(i, j, k)
						extrapolatedsubpsupi(i, j, k) = k1p(i, j, k)
						extrapolatedsubpsupj(i, j, k) = k1p(i, j, k)
						extrapolatedsubpsupk(i, j, k) = k1p(i, j, k)
					end if
				end do
			end do
		end do
		call CCTK_SyncGroup(status, cctkGH, "FDAsNozzle3D::block4")
		call CCTK_SyncGroup(status, cctkGH, "FDAsNozzle3D::block3")
		do k = kStart, kEnd
			do j = jStart, jEnd
				do i = iStart, iEnd
					if ((FOV_3(i, j, k) .gt. 0 .or. (((i + 1 .le. cctk_lsh(1) .and. FOV_3(i + 1, j, k) .gt. 0) .or. (i + 2 .le. cctk_lsh(1) .and. FOV_3(i + 2, j, k) .gt. 0) .or. (j + 1 .le. cctk_lsh(2) .and. FOV_3(i, j + 1, k) .gt. 0) .or. (j + 2 .le. cctk_lsh(2) .and. FOV_3(i, j + 2, k) .gt. 0) .or. (k + 1 .le. cctk_lsh(3) .and. FOV_3(i, j, k + 1) .gt. 0) .or. (k + 2 .le. cctk_lsh(3) .and. FOV_3(i, j, k + 2) .gt. 0)) .or. ((i - 1 .ge. 1 .and. FOV_3(i - 1, j, k) .gt. 0) .or. (i - 2 .ge. 1 .and. FOV_3(i - 2, j, k) .gt. 0) .or. (j - 1 .ge. 1 .and. FOV_3(i, j - 1, k) .gt. 0) .or. (j - 2 .ge. 1 .and. FOV_3(i, j - 2, k) .gt. 0) .or. (k - 1 .ge. 1 .and. FOV_3(i, j, k - 1) .gt. 0) .or. (k - 2 .ge. 1 .and. FOV_3(i, j, k - 2) .gt. 0))))) then
					if ((((i .ge. 3) .and. (i .le. (cctk_lsh(1) - 3))) .and. ((j .ge. 3) .and. (j .le. (cctk_lsh(2) - 3)))) .and. ((k .ge. 3) .and. (k .le. (cctk_lsh(3) - 3)))) then
					Flux_extsubNullsupi_3(i, j, k) = 0.0d0
					Flux_extsubNullsupj_3(i, j, k) = 0.0d0
					Flux_extsubNullsupk_3(i, j, k) = 0.0d0
					end if
					Speedi_3(i, j, k) = ABS(1.0d0)
					Speedj_3(i, j, k) = ABS(1.0d0)
					Speedk_3(i, j, k) = ABS(1.0d0)
					end if
					if ((FOV_1(i, j, k) .gt. 0 .or. (((i + 1 .le. cctk_lsh(1) .and. FOV_1(i + 1, j, k) .gt. 0) .or. (i + 2 .le. cctk_lsh(1) .and. FOV_1(i + 2, j, k) .gt. 0) .or. (j + 1 .le. cctk_lsh(2) .and. FOV_1(i, j + 1, k) .gt. 0) .or. (j + 2 .le. cctk_lsh(2) .and. FOV_1(i, j + 2, k) .gt. 0) .or. (k + 1 .le. cctk_lsh(3) .and. FOV_1(i, j, k + 1) .gt. 0) .or. (k + 2 .le. cctk_lsh(3) .and. FOV_1(i, j, k + 2) .gt. 0)) .or. ((i - 1 .ge. 1 .and. FOV_1(i - 1, j, k) .gt. 0) .or. (i - 2 .ge. 1 .and. FOV_1(i - 2, j, k) .gt. 0) .or. (j - 1 .ge. 1 .and. FOV_1(i, j - 1, k) .gt. 0) .or. (j - 2 .ge. 1 .and. FOV_1(i, j - 2, k) .gt. 0) .or. (k - 1 .ge. 1 .and. FOV_1(i, j, k - 1) .gt. 0) .or. (k - 2 .ge. 1 .and. FOV_1(i, j, k - 2) .gt. 0)) .or. (((i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1) .and. FOV_1(int(i + 1), int(j - 1), k) .gt. 0) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2)) .and. FOV_1(int(i + 1), int(j + 1), k) .gt. 0) .or. ((i - 1 .ge. 1 .and. j - 1 .ge. 1) .and. FOV_1(int(i - 1), int(j - 1), k) .gt. 0) .or. ((i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2)) .and. FOV_1(int(i - 1), int(j + 1), k) .gt. 0) .or. ((i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1) .and. FOV_1(int(i + 1), int(j - 2), k) .gt. 0) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2)) .and. FOV_1(int(i + 1), int(j + 2), k) .gt. 0) .or. ((i - 1 .ge. 1 .and. j - 2 .ge. 1) .and. FOV_1(int(i - 1), int(j - 2), k) .gt. 0) .or. ((i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2)) .and. FOV_1(int(i - 1), int(j + 2), k) .gt. 0) .or. ((i + 1 .le. cctk_lsh(1) .and. k - 1 .ge. 1) .and. FOV_1(int(i + 1), j, int(k - 1)) .gt. 0) .or. ((i + 1 .le. cctk_lsh(1) .and. k + 1 .le. cctk_lsh(3)) .and. FOV_1(int(i + 1), j, int(k + 1)) .gt. 0) .or. ((i - 1 .ge. 1 .and. k - 1 .ge. 1) .and. FOV_1(int(i - 1), j, int(k - 1)) .gt. 0) .or. ((i - 1 .ge. 1 .and. k + 1 .le. cctk_lsh(3)) .and. FOV_1(int(i - 1), j, int(k + 1)) .gt. 0) .or. ((i + 1 .le. cctk_lsh(1) .and. k - 2 .ge. 1) .and. FOV_1(int(i + 1), j, int(k - 2)) .gt. 0) .or. ((i + 1 .le. cctk_lsh(1) .and. k + 2 .le. cctk_lsh(3)) .and. FOV_1(int(i + 1), j, int(k + 2)) .gt. 0) .or. ((i - 1 .ge. 1 .and. k - 2 .ge. 1) .and. FOV_1(int(i - 1), j, int(k - 2)) .gt. 0) .or. ((i - 1 .ge. 1 .and. k + 2 .le. cctk_lsh(3)) .and. FOV_1(int(i - 1), j, int(k + 2)) .gt. 0) .or. ((i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1) .and. FOV_1(int(i + 2), int(j - 1), k) .gt. 0) .or. ((i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2)) .and. FOV_1(int(i + 2), int(j + 1), k) .gt. 0) .or. ((i - 2 .ge. 1 .and. j - 1 .ge. 1) .and. FOV_1(int(i - 2), int(j - 1), k) .gt. 0) .or. ((i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2)) .and. FOV_1(int(i - 2), int(j + 1), k) .gt. 0) .or. ((i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1) .and. FOV_1(int(i + 2), int(j - 2), k) .gt. 0) .or. ((i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2)) .and. FOV_1(int(i + 2), int(j + 2), k) .gt. 0) .or. ((i - 2 .ge. 1 .and. j - 2 .ge. 1) .and. FOV_1(int(i - 2), int(j - 2), k) .gt. 0) .or. ((i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2)) .and. FOV_1(int(i - 2), int(j + 2), k) .gt. 0) .or. ((i + 2 .le. cctk_lsh(1) .and. k - 1 .ge. 1) .and. FOV_1(int(i + 2), j, int(k - 1)) .gt. 0) .or. ((i + 2 .le. cctk_lsh(1) .and. k + 1 .le. cctk_lsh(3)) .and. FOV_1(int(i + 2), j, int(k + 1)) .gt. 0) .or. ((i - 2 .ge. 1 .and. k - 1 .ge. 1) .and. FOV_1(int(i - 2), j, int(k - 1)) .gt. 0) .or. ((i - 2 .ge. 1 .and. k + 1 .le. cctk_lsh(3)) .and. FOV_1(int(i - 2), j, int(k + 1)) .gt. 0) .or. ((i + 2 .le. cctk_lsh(1) .and. k - 2 .ge. 1) .and. FOV_1(int(i + 2), j, int(k - 2)) .gt. 0) .or. ((i + 2 .le. cctk_lsh(1) .and. k + 2 .le. cctk_lsh(3)) .and. FOV_1(int(i + 2), j, int(k + 2)) .gt. 0) .or. ((i - 2 .ge. 1 .and. k - 2 .ge. 1) .and. FOV_1(int(i - 2), j, int(k - 2)) .gt. 0) .or. ((i - 2 .ge. 1 .and. k + 2 .le. cctk_lsh(3)) .and. FOV_1(int(i - 2), j, int(k + 2)) .gt. 0) .or. ((j + 1 .le. cctk_lsh(2) .and. k - 1 .ge. 1) .and. FOV_1(i, int(j + 1), int(k - 1)) .gt. 0) .or. ((j + 1 .le. cctk_lsh(2) .and. k + 1 .le. cctk_lsh(3)) .and. FOV_1(i, int(j + 1), int(k + 1)) .gt. 0) .or. ((j - 1 .ge. 1 .and. k - 1 .ge. 1) .and. FOV_1(i, int(j - 1), int(k - 1)) .gt. 0) .or. ((j - 1 .ge. 1 .and. k + 1 .le. cctk_lsh(3)) .and. FOV_1(i, int(j - 1), int(k + 1)) .gt. 0) .or. ((j + 1 .le. cctk_lsh(2) .and. k - 2 .ge. 1) .and. FOV_1(i, int(j + 1), int(k - 2)) .gt. 0) .or. ((j + 1 .le. cctk_lsh(2) .and. k + 2 .le. cctk_lsh(3)) .and. FOV_1(i, int(j + 1), int(k + 2)) .gt. 0) .or. ((j - 1 .ge. 1 .and. k - 2 .ge. 1) .and. FOV_1(i, int(j - 1), int(k - 2)) .gt. 0) .or. ((j - 1 .ge. 1 .and. k + 2 .le. cctk_lsh(3)) .and. FOV_1(i, int(j - 1), int(k + 2)) .gt. 0) .or. ((j + 2 .le. cctk_lsh(2) .and. k - 1 .ge. 1) .and. FOV_1(i, int(j + 2), int(k - 1)) .gt. 0) .or. ((j + 2 .le. cctk_lsh(2) .and. k + 1 .le. cctk_lsh(3)) .and. FOV_1(i, int(j + 2), int(k + 1)) .gt. 0) .or. ((j - 2 .ge. 1 .and. k - 1 .ge. 1) .and. FOV_1(i, int(j - 2), int(k - 1)) .gt. 0) .or. ((j - 2 .ge. 1 .and. k + 1 .le. cctk_lsh(3)) .and. FOV_1(i, int(j - 2), int(k + 1)) .gt. 0) .or. ((j + 2 .le. cctk_lsh(2) .and. k - 2 .ge. 1) .and. FOV_1(i, int(j + 2), int(k - 2)) .gt. 0) .or. ((j + 2 .le. cctk_lsh(2) .and. k + 2 .le. cctk_lsh(3)) .and. FOV_1(i, int(j + 2), int(k + 2)) .gt. 0) .or. ((j - 2 .ge. 1 .and. k - 2 .ge. 1) .and. FOV_1(i, int(j - 2), int(k - 2)) .gt. 0) .or. ((j - 2 .ge. 1 .and. k + 2 .le. cctk_lsh(3)) .and. FOV_1(i, int(j - 2), int(k + 2)) .gt. 0))))) then
					if ((((i .ge. 3) .and. (i .le. (cctk_lsh(1) - 3))) .and. ((j .ge. 3) .and. (j .le. (cctk_lsh(2) - 3)))) .and. ((k .ge. 3) .and. (k .le. (cctk_lsh(3) - 3)))) then
					Flux_extsubrhosupi_1(i, j, k) = Firho_PlaneI(CCTK_PASS_FTOF, extrapolatedsubmxsupi(i, j, k))
					Flux_extsubrhosupj_1(i, j, k) = Fjrho_PlaneI(CCTK_PASS_FTOF, extrapolatedsubmysupj(i, j, k))
					Flux_extsubrhosupk_1(i, j, k) = Fkrho_PlaneI(CCTK_PASS_FTOF, extrapolatedsubmzsupk(i, j, k))
					Flux_extsubmxsupi_1(i, j, k) = Fimx_PlaneI(CCTK_PASS_FTOF, extrapolatedsubrhosupi(i, j, k), extrapolatedsubmxsupi(i, j, k), extrapolatedsubpsupi(i, j, k))
					Flux_extsubmxsupj_1(i, j, k) = Fjmx_PlaneI(CCTK_PASS_FTOF, extrapolatedsubrhosupj(i, j, k), extrapolatedsubmxsupj(i, j, k), extrapolatedsubmysupj(i, j, k))
					Flux_extsubmxsupk_1(i, j, k) = Fkmx_PlaneI(CCTK_PASS_FTOF, extrapolatedsubrhosupk(i, j, k), extrapolatedsubmxsupk(i, j, k), extrapolatedsubmzsupk(i, j, k))
					Flux_extsubmysupi_1(i, j, k) = Fimy_PlaneI(CCTK_PASS_FTOF, extrapolatedsubrhosupi(i, j, k), extrapolatedsubmxsupi(i, j, k), extrapolatedsubmysupi(i, j, k))
					Flux_extsubmysupj_1(i, j, k) = Fjmy_PlaneI(CCTK_PASS_FTOF, extrapolatedsubrhosupj(i, j, k), extrapolatedsubmysupj(i, j, k), extrapolatedsubpsupj(i, j, k))
					Flux_extsubmysupk_1(i, j, k) = Fkmy_PlaneI(CCTK_PASS_FTOF, extrapolatedsubrhosupk(i, j, k), extrapolatedsubmysupk(i, j, k), extrapolatedsubmzsupk(i, j, k))
					Flux_extsubmzsupi_1(i, j, k) = Fimz_PlaneI(CCTK_PASS_FTOF, extrapolatedsubrhosupi(i, j, k), extrapolatedsubmxsupi(i, j, k), extrapolatedsubmzsupi(i, j, k))
					Flux_extsubmzsupj_1(i, j, k) = Fjmz_PlaneI(CCTK_PASS_FTOF, extrapolatedsubrhosupj(i, j, k), extrapolatedsubmysupj(i, j, k), extrapolatedsubmzsupj(i, j, k))
					Flux_extsubmzsupk_1(i, j, k) = Fkmz_PlaneI(CCTK_PASS_FTOF, extrapolatedsubrhosupk(i, j, k), extrapolatedsubmzsupk(i, j, k), extrapolatedsubpsupk(i, j, k))
					Q_ii_mx_1 = lambda
					Q_ij_mx_1 = lambda
					Q_ik_mx_1 = lambda
					Q_ii2_mx_1 = mu
					Q_jj_mx_1 = mu
					Q_kk_mx_1 = mu
					Q_ii3_mx_1 = mu
					Q_ji_mx_1 = mu
					Q_ki_mx_1 = mu
					Q_ji_my_1 = lambda
					Q_jj_my_1 = lambda
					Q_jk_my_1 = lambda
					Q_ii_my_1 = mu
					Q_jj2_my_1 = mu
					Q_kk_my_1 = mu
					Q_ij_my_1 = mu
					Q_jj3_my_1 = mu
					Q_kj_my_1 = mu
					Q_ki_mz_1 = lambda
					Q_kj_mz_1 = lambda
					Q_kk_mz_1 = lambda
					Q_ii_mz_1 = mu
					Q_jj_mz_1 = mu
					Q_kk2_mz_1 = mu
					Q_ik_mz_1 = mu
					Q_jk_mz_1 = mu
					Q_kk3_mz_1 = mu
					Pm2_ii_mx_1 = k1mx(i - 2, j, k) / k1rho(i - 2, j, k)
					Pm2_ij_mx_1 = k1my(i, j - 2, k) / k1rho(i, j - 2, k)
					Pm2_ik_mx_1 = k1mz(i, j, k - 2) / k1rho(i, j, k - 2)
					Pm2_ii2_mx_1 = k1mx(i - 2, j, k) / k1rho(i - 2, j, k)
					Pm2_jj_mx_1 = k1mx(i, j - 2, k) / k1rho(i, j - 2, k)
					Pm2_kk_mx_1 = k1mx(i, j, k - 2) / k1rho(i, j, k - 2)
					Pm2_ii3_mx_1 = k1mx(i - 2, j, k) / k1rho(i - 2, j, k)
					Pm2_ji_mx_1 = k1my(i - 2, j, k) / k1rho(i - 2, j, k)
					Pm2_ki_mx_1 = k1mz(i - 2, j, k) / k1rho(i - 2, j, k)
					Pm2_ji_my_1 = k1mx(i - 2, j, k) / k1rho(i - 2, j, k)
					Pm2_jj_my_1 = k1my(i, j - 2, k) / k1rho(i, j - 2, k)
					Pm2_jk_my_1 = k1mz(i, j, k - 2) / k1rho(i, j, k - 2)
					Pm2_ii_my_1 = k1my(i - 2, j, k) / k1rho(i - 2, j, k)
					Pm2_jj2_my_1 = k1my(i, j - 2, k) / k1rho(i, j - 2, k)
					Pm2_kk_my_1 = k1my(i, j, k - 2) / k1rho(i, j, k - 2)
					Pm2_ij_my_1 = k1mx(i, j - 2, k) / k1rho(i, j - 2, k)
					Pm2_jj3_my_1 = k1my(i, j - 2, k) / k1rho(i, j - 2, k)
					Pm2_kj_my_1 = k1mz(i, j - 2, k) / k1rho(i, j - 2, k)
					Pm2_ki_mz_1 = k1mx(i - 2, j, k) / k1rho(i - 2, j, k)
					Pm2_kj_mz_1 = k1my(i, j - 2, k) / k1rho(i, j - 2, k)
					Pm2_kk_mz_1 = k1mz(i, j, k - 2) / k1rho(i, j, k - 2)
					Pm2_ii_mz_1 = k1mz(i - 2, j, k) / k1rho(i - 2, j, k)
					Pm2_jj_mz_1 = k1mz(i, j - 2, k) / k1rho(i, j - 2, k)
					Pm2_kk2_mz_1 = k1mz(i, j, k - 2) / k1rho(i, j, k - 2)
					Pm2_ik_mz_1 = k1mx(i, j, k - 2) / k1rho(i, j, k - 2)
					Pm2_jk_mz_1 = k1my(i, j, k - 2) / k1rho(i, j, k - 2)
					Pm2_kk3_mz_1 = k1mz(i, j, k - 2) / k1rho(i, j, k - 2)
					Pm1_ii_mx_1 = k1mx(i - 1, j, k) / k1rho(i - 1, j, k)
					Pm1_ij_mx_1 = k1my(i, j - 1, k) / k1rho(i, j - 1, k)
					Pm1_ik_mx_1 = k1mz(i, j, k - 1) / k1rho(i, j, k - 1)
					Pm1_ii2_mx_1 = k1mx(i - 1, j, k) / k1rho(i - 1, j, k)
					Pm1_jj_mx_1 = k1mx(i, j - 1, k) / k1rho(i, j - 1, k)
					Pm1_kk_mx_1 = k1mx(i, j, k - 1) / k1rho(i, j, k - 1)
					Pm1_ii3_mx_1 = k1mx(i - 1, j, k) / k1rho(i - 1, j, k)
					Pm1_ji_mx_1 = k1my(i - 1, j, k) / k1rho(i - 1, j, k)
					Pm1_ki_mx_1 = k1mz(i - 1, j, k) / k1rho(i - 1, j, k)
					Pm1_ji_my_1 = k1mx(i - 1, j, k) / k1rho(i - 1, j, k)
					Pm1_jj_my_1 = k1my(i, j - 1, k) / k1rho(i, j - 1, k)
					Pm1_jk_my_1 = k1mz(i, j, k - 1) / k1rho(i, j, k - 1)
					Pm1_ii_my_1 = k1my(i - 1, j, k) / k1rho(i - 1, j, k)
					Pm1_jj2_my_1 = k1my(i, j - 1, k) / k1rho(i, j - 1, k)
					Pm1_kk_my_1 = k1my(i, j, k - 1) / k1rho(i, j, k - 1)
					Pm1_ij_my_1 = k1mx(i, j - 1, k) / k1rho(i, j - 1, k)
					Pm1_jj3_my_1 = k1my(i, j - 1, k) / k1rho(i, j - 1, k)
					Pm1_kj_my_1 = k1mz(i, j - 1, k) / k1rho(i, j - 1, k)
					Pm1_ki_mz_1 = k1mx(i - 1, j, k) / k1rho(i - 1, j, k)
					Pm1_kj_mz_1 = k1my(i, j - 1, k) / k1rho(i, j - 1, k)
					Pm1_kk_mz_1 = k1mz(i, j, k - 1) / k1rho(i, j, k - 1)
					Pm1_ii_mz_1 = k1mz(i - 1, j, k) / k1rho(i - 1, j, k)
					Pm1_jj_mz_1 = k1mz(i, j - 1, k) / k1rho(i, j - 1, k)
					Pm1_kk2_mz_1 = k1mz(i, j, k - 1) / k1rho(i, j, k - 1)
					Pm1_ik_mz_1 = k1mx(i, j, k - 1) / k1rho(i, j, k - 1)
					Pm1_jk_mz_1 = k1my(i, j, k - 1) / k1rho(i, j, k - 1)
					Pm1_kk3_mz_1 = k1mz(i, j, k - 1) / k1rho(i, j, k - 1)
					Pp1_ii_mx_1 = k1mx(i + 1, j, k) / k1rho(i + 1, j, k)
					Pp1_ij_mx_1 = k1my(i, j + 1, k) / k1rho(i, j + 1, k)
					Pp1_ik_mx_1 = k1mz(i, j, k + 1) / k1rho(i, j, k + 1)
					Pp1_ii2_mx_1 = k1mx(i + 1, j, k) / k1rho(i + 1, j, k)
					Pp1_jj_mx_1 = k1mx(i, j + 1, k) / k1rho(i, j + 1, k)
					Pp1_kk_mx_1 = k1mx(i, j, k + 1) / k1rho(i, j, k + 1)
					Pp1_ii3_mx_1 = k1mx(i + 1, j, k) / k1rho(i + 1, j, k)
					Pp1_ji_mx_1 = k1my(i + 1, j, k) / k1rho(i + 1, j, k)
					Pp1_ki_mx_1 = k1mz(i + 1, j, k) / k1rho(i + 1, j, k)
					Pp1_ji_my_1 = k1mx(i + 1, j, k) / k1rho(i + 1, j, k)
					Pp1_jj_my_1 = k1my(i, j + 1, k) / k1rho(i, j + 1, k)
					Pp1_jk_my_1 = k1mz(i, j, k + 1) / k1rho(i, j, k + 1)
					Pp1_ii_my_1 = k1my(i + 1, j, k) / k1rho(i + 1, j, k)
					Pp1_jj2_my_1 = k1my(i, j + 1, k) / k1rho(i, j + 1, k)
					Pp1_kk_my_1 = k1my(i, j, k + 1) / k1rho(i, j, k + 1)
					Pp1_ij_my_1 = k1mx(i, j + 1, k) / k1rho(i, j + 1, k)
					Pp1_jj3_my_1 = k1my(i, j + 1, k) / k1rho(i, j + 1, k)
					Pp1_kj_my_1 = k1mz(i, j + 1, k) / k1rho(i, j + 1, k)
					Pp1_ki_mz_1 = k1mx(i + 1, j, k) / k1rho(i + 1, j, k)
					Pp1_kj_mz_1 = k1my(i, j + 1, k) / k1rho(i, j + 1, k)
					Pp1_kk_mz_1 = k1mz(i, j, k + 1) / k1rho(i, j, k + 1)
					Pp1_ii_mz_1 = k1mz(i + 1, j, k) / k1rho(i + 1, j, k)
					Pp1_jj_mz_1 = k1mz(i, j + 1, k) / k1rho(i, j + 1, k)
					Pp1_kk2_mz_1 = k1mz(i, j, k + 1) / k1rho(i, j, k + 1)
					Pp1_ik_mz_1 = k1mx(i, j, k + 1) / k1rho(i, j, k + 1)
					Pp1_jk_mz_1 = k1my(i, j, k + 1) / k1rho(i, j, k + 1)
					Pp1_kk3_mz_1 = k1mz(i, j, k + 1) / k1rho(i, j, k + 1)
					Pp2_ii_mx_1 = k1mx(i + 2, j, k) / k1rho(i + 2, j, k)
					Pp2_ij_mx_1 = k1my(i, j + 2, k) / k1rho(i, j + 2, k)
					Pp2_ik_mx_1 = k1mz(i, j, k + 2) / k1rho(i, j, k + 2)
					Pp2_ii2_mx_1 = k1mx(i + 2, j, k) / k1rho(i + 2, j, k)
					Pp2_jj_mx_1 = k1mx(i, j + 2, k) / k1rho(i, j + 2, k)
					Pp2_kk_mx_1 = k1mx(i, j, k + 2) / k1rho(i, j, k + 2)
					Pp2_ii3_mx_1 = k1mx(i + 2, j, k) / k1rho(i + 2, j, k)
					Pp2_ji_mx_1 = k1my(i + 2, j, k) / k1rho(i + 2, j, k)
					Pp2_ki_mx_1 = k1mz(i + 2, j, k) / k1rho(i + 2, j, k)
					Pp2_ji_my_1 = k1mx(i + 2, j, k) / k1rho(i + 2, j, k)
					Pp2_jj_my_1 = k1my(i, j + 2, k) / k1rho(i, j + 2, k)
					Pp2_jk_my_1 = k1mz(i, j, k + 2) / k1rho(i, j, k + 2)
					Pp2_ii_my_1 = k1my(i + 2, j, k) / k1rho(i + 2, j, k)
					Pp2_jj2_my_1 = k1my(i, j + 2, k) / k1rho(i, j + 2, k)
					Pp2_kk_my_1 = k1my(i, j, k + 2) / k1rho(i, j, k + 2)
					Pp2_ij_my_1 = k1mx(i, j + 2, k) / k1rho(i, j + 2, k)
					Pp2_jj3_my_1 = k1my(i, j + 2, k) / k1rho(i, j + 2, k)
					Pp2_kj_my_1 = k1mz(i, j + 2, k) / k1rho(i, j + 2, k)
					Pp2_ki_mz_1 = k1mx(i + 2, j, k) / k1rho(i + 2, j, k)
					Pp2_kj_mz_1 = k1my(i, j + 2, k) / k1rho(i, j + 2, k)
					Pp2_kk_mz_1 = k1mz(i, j, k + 2) / k1rho(i, j, k + 2)
					Pp2_ii_mz_1 = k1mz(i + 2, j, k) / k1rho(i + 2, j, k)
					Pp2_jj_mz_1 = k1mz(i, j + 2, k) / k1rho(i, j + 2, k)
					Pp2_kk2_mz_1 = k1mz(i, j, k + 2) / k1rho(i, j, k + 2)
					Pp2_ik_mz_1 = k1mx(i, j, k + 2) / k1rho(i, j, k + 2)
					Pp2_jk_mz_1 = k1my(i, j, k + 2) / k1rho(i, j, k + 2)
					Pp2_kk3_mz_1 = k1mz(i, j, k + 2) / k1rho(i, j, k + 2)
					Flux_extsubmxsupi_1(i, j, k) = Flux_extsubmxsupi_1(i, j, k) - Q_ii_mx_1 * CDi(CCTK_PASS_FTOF, Pm2_ii_mx_1, Pm1_ii_mx_1, Pp1_ii_mx_1, Pp2_ii_mx_1)
					Flux_extsubmxsupi_1(i, j, k) = Flux_extsubmxsupi_1(i, j, k) - Q_ij_mx_1 * CDj(CCTK_PASS_FTOF, Pm2_ij_mx_1, Pm1_ij_mx_1, Pp1_ij_mx_1, Pp2_ij_mx_1)
					Flux_extsubmxsupi_1(i, j, k) = Flux_extsubmxsupi_1(i, j, k) - Q_ik_mx_1 * CDk(CCTK_PASS_FTOF, Pm2_ik_mx_1, Pm1_ik_mx_1, Pp1_ik_mx_1, Pp2_ik_mx_1)
					Flux_extsubmxsupi_1(i, j, k) = Flux_extsubmxsupi_1(i, j, k) - Q_ii2_mx_1 * CDi(CCTK_PASS_FTOF, Pm2_ii2_mx_1, Pm1_ii2_mx_1, Pp1_ii2_mx_1, Pp2_ii2_mx_1)
					Flux_extsubmxsupj_1(i, j, k) = Flux_extsubmxsupj_1(i, j, k) - Q_jj_mx_1 * CDj(CCTK_PASS_FTOF, Pm2_jj_mx_1, Pm1_jj_mx_1, Pp1_jj_mx_1, Pp2_jj_mx_1)
					Flux_extsubmxsupk_1(i, j, k) = Flux_extsubmxsupk_1(i, j, k) - Q_kk_mx_1 * CDk(CCTK_PASS_FTOF, Pm2_kk_mx_1, Pm1_kk_mx_1, Pp1_kk_mx_1, Pp2_kk_mx_1)
					Flux_extsubmxsupi_1(i, j, k) = Flux_extsubmxsupi_1(i, j, k) - Q_ii3_mx_1 * CDi(CCTK_PASS_FTOF, Pm2_ii3_mx_1, Pm1_ii3_mx_1, Pp1_ii3_mx_1, Pp2_ii3_mx_1)
					Flux_extsubmxsupj_1(i, j, k) = Flux_extsubmxsupj_1(i, j, k) - Q_ji_mx_1 * CDi(CCTK_PASS_FTOF, Pm2_ji_mx_1, Pm1_ji_mx_1, Pp1_ji_mx_1, Pp2_ji_mx_1)
					Flux_extsubmxsupk_1(i, j, k) = Flux_extsubmxsupk_1(i, j, k) - Q_ki_mx_1 * CDi(CCTK_PASS_FTOF, Pm2_ki_mx_1, Pm1_ki_mx_1, Pp1_ki_mx_1, Pp2_ki_mx_1)
					Flux_extsubmysupj_1(i, j, k) = Flux_extsubmysupj_1(i, j, k) - Q_ji_my_1 * CDi(CCTK_PASS_FTOF, Pm2_ji_my_1, Pm1_ji_my_1, Pp1_ji_my_1, Pp2_ji_my_1)
					Flux_extsubmysupj_1(i, j, k) = Flux_extsubmysupj_1(i, j, k) - Q_jj_my_1 * CDj(CCTK_PASS_FTOF, Pm2_jj_my_1, Pm1_jj_my_1, Pp1_jj_my_1, Pp2_jj_my_1)
					Flux_extsubmysupj_1(i, j, k) = Flux_extsubmysupj_1(i, j, k) - Q_jk_my_1 * CDk(CCTK_PASS_FTOF, Pm2_jk_my_1, Pm1_jk_my_1, Pp1_jk_my_1, Pp2_jk_my_1)
					Flux_extsubmysupi_1(i, j, k) = Flux_extsubmysupi_1(i, j, k) - Q_ii_my_1 * CDi(CCTK_PASS_FTOF, Pm2_ii_my_1, Pm1_ii_my_1, Pp1_ii_my_1, Pp2_ii_my_1)
					Flux_extsubmysupj_1(i, j, k) = Flux_extsubmysupj_1(i, j, k) - Q_jj2_my_1 * CDj(CCTK_PASS_FTOF, Pm2_jj2_my_1, Pm1_jj2_my_1, Pp1_jj2_my_1, Pp2_jj2_my_1)
					Flux_extsubmysupk_1(i, j, k) = Flux_extsubmysupk_1(i, j, k) - Q_kk_my_1 * CDk(CCTK_PASS_FTOF, Pm2_kk_my_1, Pm1_kk_my_1, Pp1_kk_my_1, Pp2_kk_my_1)
					Flux_extsubmysupi_1(i, j, k) = Flux_extsubmysupi_1(i, j, k) - Q_ij_my_1 * CDj(CCTK_PASS_FTOF, Pm2_ij_my_1, Pm1_ij_my_1, Pp1_ij_my_1, Pp2_ij_my_1)
					Flux_extsubmysupj_1(i, j, k) = Flux_extsubmysupj_1(i, j, k) - Q_jj3_my_1 * CDj(CCTK_PASS_FTOF, Pm2_jj3_my_1, Pm1_jj3_my_1, Pp1_jj3_my_1, Pp2_jj3_my_1)
					Flux_extsubmysupk_1(i, j, k) = Flux_extsubmysupk_1(i, j, k) - Q_kj_my_1 * CDj(CCTK_PASS_FTOF, Pm2_kj_my_1, Pm1_kj_my_1, Pp1_kj_my_1, Pp2_kj_my_1)
					Flux_extsubmzsupk_1(i, j, k) = Flux_extsubmzsupk_1(i, j, k) - Q_ki_mz_1 * CDi(CCTK_PASS_FTOF, Pm2_ki_mz_1, Pm1_ki_mz_1, Pp1_ki_mz_1, Pp2_ki_mz_1)
					Flux_extsubmzsupk_1(i, j, k) = Flux_extsubmzsupk_1(i, j, k) - Q_kj_mz_1 * CDj(CCTK_PASS_FTOF, Pm2_kj_mz_1, Pm1_kj_mz_1, Pp1_kj_mz_1, Pp2_kj_mz_1)
					Flux_extsubmzsupk_1(i, j, k) = Flux_extsubmzsupk_1(i, j, k) - Q_kk_mz_1 * CDk(CCTK_PASS_FTOF, Pm2_kk_mz_1, Pm1_kk_mz_1, Pp1_kk_mz_1, Pp2_kk_mz_1)
					Flux_extsubmzsupi_1(i, j, k) = Flux_extsubmzsupi_1(i, j, k) - Q_ii_mz_1 * CDi(CCTK_PASS_FTOF, Pm2_ii_mz_1, Pm1_ii_mz_1, Pp1_ii_mz_1, Pp2_ii_mz_1)
					Flux_extsubmzsupj_1(i, j, k) = Flux_extsubmzsupj_1(i, j, k) - Q_jj_mz_1 * CDj(CCTK_PASS_FTOF, Pm2_jj_mz_1, Pm1_jj_mz_1, Pp1_jj_mz_1, Pp2_jj_mz_1)
					Flux_extsubmzsupk_1(i, j, k) = Flux_extsubmzsupk_1(i, j, k) - Q_kk2_mz_1 * CDk(CCTK_PASS_FTOF, Pm2_kk2_mz_1, Pm1_kk2_mz_1, Pp1_kk2_mz_1, Pp2_kk2_mz_1)
					Flux_extsubmzsupi_1(i, j, k) = Flux_extsubmzsupi_1(i, j, k) - Q_ik_mz_1 * CDk(CCTK_PASS_FTOF, Pm2_ik_mz_1, Pm1_ik_mz_1, Pp1_ik_mz_1, Pp2_ik_mz_1)
					Flux_extsubmzsupj_1(i, j, k) = Flux_extsubmzsupj_1(i, j, k) - Q_jk_mz_1 * CDk(CCTK_PASS_FTOF, Pm2_jk_mz_1, Pm1_jk_mz_1, Pp1_jk_mz_1, Pp2_jk_mz_1)
					Flux_extsubmzsupk_1(i, j, k) = Flux_extsubmzsupk_1(i, j, k) - Q_kk3_mz_1 * CDk(CCTK_PASS_FTOF, Pm2_kk3_mz_1, Pm1_kk3_mz_1, Pp1_kk3_mz_1, Pp2_kk3_mz_1)
					end if
					m_n_1 = extrapolatedsubmxsupi(i, j, k)
					m_tx_1 = 0.0d0
					m_ty_1 = extrapolatedsubmysupi(i, j, k)
					m_tz_1 = extrapolatedsubmzsupi(i, j, k)
					c_x_1 = c0 * ((extrapolatedsubrhosupi(i, j, k) / rho0) ** ((gamma + (-1.0d0)) / 2.0d0))
					cn_x_1 = m_n_1 / extrapolatedsubrhosupi(i, j, k)
					m_n_1 = extrapolatedsubmysupj(i, j, k)
					m_tx_1 = extrapolatedsubmxsupj(i, j, k)
					m_ty_1 = 0.0d0
					m_tz_1 = extrapolatedsubmzsupj(i, j, k)
					c_y_1 = c0 * ((extrapolatedsubrhosupj(i, j, k) / rho0) ** ((gamma + (-1.0d0)) / 2.0d0))
					cn_y_1 = m_n_1 / extrapolatedsubrhosupj(i, j, k)
					m_n_1 = extrapolatedsubmzsupk(i, j, k)
					m_tx_1 = extrapolatedsubmxsupk(i, j, k)
					m_ty_1 = extrapolatedsubmysupk(i, j, k)
					m_tz_1 = 0.0d0
					c_z_1 = c0 * ((extrapolatedsubrhosupk(i, j, k) / rho0) ** ((gamma + (-1.0d0)) / 2.0d0))
					cn_z_1 = m_n_1 / extrapolatedsubrhosupk(i, j, k)
					Speedi_1(i, j, k) = MAX(ABS(c_x_1 + cn_x_1), ABS((-c_x_1) + cn_x_1), ABS(cn_x_1), ABS(cn_x_1), ABS(cn_x_1))
					Speedj_1(i, j, k) = MAX(ABS(c_y_1 + cn_y_1), ABS((-c_y_1) + cn_y_1), ABS(cn_y_1), ABS(cn_y_1), ABS(cn_y_1))
					Speedk_1(i, j, k) = MAX(ABS(c_z_1 + cn_z_1), ABS((-c_z_1) + cn_z_1), ABS(cn_z_1), ABS(cn_z_1), ABS(cn_z_1))
					end if
				end do
			end do
		end do
		do k = kStart, kEnd
			do j = jStart, jEnd
				do i = iStart, iEnd
!					Boundaries
		if ((FOV_xLower(i, j, k) .gt. 0) .and. ((i + 1 .le. cctk_lsh(1) .and. (FOV_3(int(i + 1), j, k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. (FOV_3(int(i + 2), j, k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. (FOV_3(int(i + 3), j, k) .gt. 0)))) then
				Flux_extsubNullsupi_3(4 - i, j, k) = 2.0d0 * Flux_extsubNullsupi_3(4 - i + 1, j, k) - Flux_extsubNullsupi_3(4 - i + 2, j, k)
		end if
		if ((FOV_xUpper(i, j, k) .gt. 0) .and. ((i - 1 .ge. 1 .and. (FOV_3(int(i - 1), j, k) .gt. 0)) .or. (i - 2 .ge. 1 .and. (FOV_3(int(i - 2), j, k) .gt. 0)) .or. (i - 3 .ge. 1 .and. (FOV_3(int(i - 3), j, k) .gt. 0)))) then
			Flux_extsubNullsupi_3(i, j, k) = 2.0d0 * Flux_extsubNullsupi_3(i - 1, j, k) - Flux_extsubNullsupi_3(i - 2, j, k)
		end if
		if ((FOV_yLower(i, j, k) .gt. 0) .and. ((j + 1 .le. cctk_lsh(2) .and. (FOV_3(i, int(j + 1), k) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. (FOV_3(i, int(j + 2), k) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. (FOV_3(i, int(j + 3), k) .gt. 0)))) then
				Flux_extsubNullsupj_3(i, 4 - j, k) = 2.0d0 * Flux_extsubNullsupj_3(i, 4 - j + 1, k) - Flux_extsubNullsupj_3(i, 4 - j + 2, k)
		end if
		if ((FOV_yUpper(i, j, k) .gt. 0) .and. ((j - 1 .ge. 1 .and. (FOV_3(i, int(j - 1), k) .gt. 0)) .or. (j - 2 .ge. 1 .and. (FOV_3(i, int(j - 2), k) .gt. 0)) .or. (j - 3 .ge. 1 .and. (FOV_3(i, int(j - 3), k) .gt. 0)))) then
			Flux_extsubNullsupj_3(i, j, k) = 2.0d0 * Flux_extsubNullsupj_3(i, j - 1, k) - Flux_extsubNullsupj_3(i, j - 2, k)
		end if
		if ((FOV_zLower(i, j, k) .gt. 0) .and. ((k + 1 .le. cctk_lsh(3) .and. (FOV_3(i, j, int(k + 1)) .gt. 0)) .or. (k + 2 .le. cctk_lsh(3) .and. (FOV_3(i, j, int(k + 2)) .gt. 0)) .or. (k + 3 .le. cctk_lsh(3) .and. (FOV_3(i, j, int(k + 3)) .gt. 0)))) then
				Flux_extsubNullsupk_3(i, j, 4 - k) = 2.0d0 * Flux_extsubNullsupk_3(i, j, 4 - k + 1) - Flux_extsubNullsupk_3(i, j, 4 - k + 2)
		end if
		if ((FOV_zUpper(i, j, k) .gt. 0) .and. ((k - 1 .ge. 1 .and. (FOV_3(i, j, int(k - 1)) .gt. 0)) .or. (k - 2 .ge. 1 .and. (FOV_3(i, j, int(k - 2)) .gt. 0)) .or. (k - 3 .ge. 1 .and. (FOV_3(i, j, int(k - 3)) .gt. 0)))) then
			Flux_extsubNullsupk_3(i, j, k) = 2.0d0 * Flux_extsubNullsupk_3(i, j, k - 1) - Flux_extsubNullsupk_3(i, j, k - 2)
		end if
		if (FOV_3(i, j, k) .gt. 0) then
			if ((d_i_rho(i, j, k) .ne. 0 .or. d_j_rho(i, j, k) .ne. 0 .or. d_k_rho(i, j, k) .ne. 0)) then
				if (FOV_1(int(i - d_i_rho(i, j, k)), int(j - d_j_rho(i, j, k)), int(k - d_k_rho(i, j, k))) .gt. 0) then
					call extrapolate_flux(CCTK_PASS_FTOF, d_i_rho, Flux_extsubrhosupi_1, Flux_extsubrhosupi_1, i, d_j_rho, Flux_extsubrhosupj_1, Flux_extsubrhosupj_1, j, d_k_rho, Flux_extsubrhosupk_1, Flux_extsubrhosupk_1, k, FOV_1)
				end if
			end if
			if ((d_i_mx(i, j, k) .ne. 0 .or. d_j_mx(i, j, k) .ne. 0 .or. d_k_mx(i, j, k) .ne. 0)) then
				if (FOV_1(int(i - d_i_mx(i, j, k)), int(j - d_j_mx(i, j, k)), int(k - d_k_mx(i, j, k))) .gt. 0) then
					call extrapolate_flux(CCTK_PASS_FTOF, d_i_mx, Flux_extsubmxsupi_1, Flux_extsubmxsupi_1, i, d_j_mx, Flux_extsubmxsupj_1, Flux_extsubmxsupj_1, j, d_k_mx, Flux_extsubmxsupk_1, Flux_extsubmxsupk_1, k, FOV_1)
				end if
			end if
			if ((d_i_my(i, j, k) .ne. 0 .or. d_j_my(i, j, k) .ne. 0 .or. d_k_my(i, j, k) .ne. 0)) then
				if (FOV_1(int(i - d_i_my(i, j, k)), int(j - d_j_my(i, j, k)), int(k - d_k_my(i, j, k))) .gt. 0) then
					call extrapolate_flux(CCTK_PASS_FTOF, d_i_my, Flux_extsubmysupi_1, Flux_extsubmysupi_1, i, d_j_my, Flux_extsubmysupj_1, Flux_extsubmysupj_1, j, d_k_my, Flux_extsubmysupk_1, Flux_extsubmysupk_1, k, FOV_1)
				end if
			end if
			if ((d_i_mz(i, j, k) .ne. 0 .or. d_j_mz(i, j, k) .ne. 0 .or. d_k_mz(i, j, k) .ne. 0)) then
				if (FOV_1(int(i - d_i_mz(i, j, k)), int(j - d_j_mz(i, j, k)), int(k - d_k_mz(i, j, k))) .gt. 0) then
					call extrapolate_flux(CCTK_PASS_FTOF, d_i_mz, Flux_extsubmzsupi_1, Flux_extsubmzsupi_1, i, d_j_mz, Flux_extsubmzsupj_1, Flux_extsubmzsupj_1, j, d_k_mz, Flux_extsubmzsupk_1, Flux_extsubmzsupk_1, k, FOV_1)
				end if
			end if
		end if
!					Boundaries
		if ((FOV_xLower(i, j, k) .gt. 0) .and. ((i + 1 .le. cctk_lsh(1) .and. (FOV_1(int(i + 1), j, k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. (FOV_1(int(i + 2), j, k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. (FOV_1(int(i + 3), j, k) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 1), k) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 1), k) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 2), k) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 2), k) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 3), k) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 3), k) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(int(i + 1), j, int(k + 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. k - 1 .ge. 1 .and. (FOV_1(int(i + 1), j, int(k - 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(int(i + 1), j, int(k + 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. k - 2 .ge. 1 .and. (FOV_1(int(i + 1), j, int(k - 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(int(i + 1), j, int(k + 3)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. k - 3 .ge. 1 .and. (FOV_1(int(i + 1), j, int(k - 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 1), k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 1), k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 2), k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 2), k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 3), k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 3), k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(int(i + 2), j, int(k + 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. k - 1 .ge. 1 .and. (FOV_1(int(i + 2), j, int(k - 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(int(i + 2), j, int(k + 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. k - 2 .ge. 1 .and. (FOV_1(int(i + 2), j, int(k - 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(int(i + 2), j, int(k + 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. k - 3 .ge. 1 .and. (FOV_1(int(i + 2), j, int(k - 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 1), k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 1), k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 2), k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 2), k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 3), k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 3), k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(int(i + 3), j, int(k + 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. k - 1 .ge. 1 .and. (FOV_1(int(i + 3), j, int(k - 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(int(i + 3), j, int(k + 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. k - 2 .ge. 1 .and. (FOV_1(int(i + 3), j, int(k - 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(int(i + 3), j, int(k + 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. k - 3 .ge. 1 .and. (FOV_1(int(i + 3), j, int(k - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j, k) .ne. 0 .or. d_j_rho(i, j, k) .ne. 0 .or. d_k_rho(i, j, k) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_rho, Flux_extsubrhosupi_1, Flux_extsubrhosupi_1, i, d_j_rho, Flux_extsubrhosupj_1, Flux_extsubrhosupj_1, j, d_k_rho, Flux_extsubrhosupk_1, Flux_extsubrhosupk_1, k, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j, k) .ne. 0 .or. d_j_mx(i, j, k) .ne. 0 .or. d_k_mx(i, j, k) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_mx, Flux_extsubmxsupi_1, Flux_extsubmxsupi_1, i, d_j_mx, Flux_extsubmxsupj_1, Flux_extsubmxsupj_1, j, d_k_mx, Flux_extsubmxsupk_1, Flux_extsubmxsupk_1, k, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j, k) .ne. 0 .or. d_j_my(i, j, k) .ne. 0 .or. d_k_my(i, j, k) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_my, Flux_extsubmysupi_1, Flux_extsubmysupi_1, i, d_j_my, Flux_extsubmysupj_1, Flux_extsubmysupj_1, j, d_k_my, Flux_extsubmysupk_1, Flux_extsubmysupk_1, k, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mz(i, j, k) .ne. 0 .or. d_j_mz(i, j, k) .ne. 0 .or. d_k_mz(i, j, k) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_mz, Flux_extsubmzsupi_1, Flux_extsubmzsupi_1, i, d_j_mz, Flux_extsubmzsupj_1, Flux_extsubmzsupj_1, j, d_k_mz, Flux_extsubmzsupk_1, Flux_extsubmzsupk_1, k, FOV_1)
			end if
		end if
		if ((FOV_xUpper(i, j, k) .gt. 0) .and. ((i - 1 .ge. 1 .and. (FOV_1(int(i - 1), j, k) .gt. 0)) .or. (i - 2 .ge. 1 .and. (FOV_1(int(i - 2), j, k) .gt. 0)) .or. (i - 3 .ge. 1 .and. (FOV_1(int(i - 3), j, k) .gt. 0)) .or. ((i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 1), k) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 1), k) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 2), k) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 2), k) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 3), k) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 3), k) .gt. 0)) .or. (i - 1 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(int(i - 1), j, int(k + 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. k - 1 .ge. 1 .and. (FOV_1(int(i - 1), j, int(k - 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(int(i - 1), j, int(k + 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. k - 2 .ge. 1 .and. (FOV_1(int(i - 1), j, int(k - 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(int(i - 1), j, int(k + 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. k - 3 .ge. 1 .and. (FOV_1(int(i - 1), j, int(k - 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 1), k) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 1), k) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 2), k) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 2), k) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 3), k) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 3), k) .gt. 0)) .or. (i - 2 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(int(i - 2), j, int(k + 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. k - 1 .ge. 1 .and. (FOV_1(int(i - 2), j, int(k - 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(int(i - 2), j, int(k + 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. k - 2 .ge. 1 .and. (FOV_1(int(i - 2), j, int(k - 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(int(i - 2), j, int(k + 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. k - 3 .ge. 1 .and. (FOV_1(int(i - 2), j, int(k - 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 1), k) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 1), k) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 2), k) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 2), k) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 3), k) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 3), k) .gt. 0)) .or. (i - 3 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(int(i - 3), j, int(k + 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. k - 1 .ge. 1 .and. (FOV_1(int(i - 3), j, int(k - 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(int(i - 3), j, int(k + 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. k - 2 .ge. 1 .and. (FOV_1(int(i - 3), j, int(k - 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(int(i - 3), j, int(k + 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. k - 3 .ge. 1 .and. (FOV_1(int(i - 3), j, int(k - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j, k) .ne. 0 .or. d_j_rho(i, j, k) .ne. 0 .or. d_k_rho(i, j, k) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_rho, Flux_extsubrhosupi_1, Flux_extsubrhosupi_1, i, d_j_rho, Flux_extsubrhosupj_1, Flux_extsubrhosupj_1, j, d_k_rho, Flux_extsubrhosupk_1, Flux_extsubrhosupk_1, k, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j, k) .ne. 0 .or. d_j_mx(i, j, k) .ne. 0 .or. d_k_mx(i, j, k) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_mx, Flux_extsubmxsupi_1, Flux_extsubmxsupi_1, i, d_j_mx, Flux_extsubmxsupj_1, Flux_extsubmxsupj_1, j, d_k_mx, Flux_extsubmxsupk_1, Flux_extsubmxsupk_1, k, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j, k) .ne. 0 .or. d_j_my(i, j, k) .ne. 0 .or. d_k_my(i, j, k) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_my, Flux_extsubmysupi_1, Flux_extsubmysupi_1, i, d_j_my, Flux_extsubmysupj_1, Flux_extsubmysupj_1, j, d_k_my, Flux_extsubmysupk_1, Flux_extsubmysupk_1, k, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mz(i, j, k) .ne. 0 .or. d_j_mz(i, j, k) .ne. 0 .or. d_k_mz(i, j, k) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_mz, Flux_extsubmzsupi_1, Flux_extsubmzsupi_1, i, d_j_mz, Flux_extsubmzsupj_1, Flux_extsubmzsupj_1, j, d_k_mz, Flux_extsubmzsupk_1, Flux_extsubmzsupk_1, k, FOV_1)
			end if
		end if
		if ((FOV_yLower(i, j, k) .gt. 0) .and. ((j + 1 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 1), k) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 2), k) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 3), k) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 1), k) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 1), k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 1), k) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 1), k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 1), k) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 1), k) .gt. 0)) .or. (j + 1 .le. cctk_lsh(2) .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 1), int(k + 1)) .gt. 0)) .or. (j + 1 .le. cctk_lsh(2) .and. k - 1 .ge. 1 .and. (FOV_1(i, int(j + 1), int(k - 1)) .gt. 0)) .or. (j + 1 .le. cctk_lsh(2) .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 1), int(k + 2)) .gt. 0)) .or. (j + 1 .le. cctk_lsh(2) .and. k - 2 .ge. 1 .and. (FOV_1(i, int(j + 1), int(k - 2)) .gt. 0)) .or. (j + 1 .le. cctk_lsh(2) .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 1), int(k + 3)) .gt. 0)) .or. (j + 1 .le. cctk_lsh(2) .and. k - 3 .ge. 1 .and. (FOV_1(i, int(j + 1), int(k - 3)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 2), k) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 2), k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 2), k) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 2), k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 2), k) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 2), k) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 2), int(k + 1)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. k - 1 .ge. 1 .and. (FOV_1(i, int(j + 2), int(k - 1)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 2), int(k + 2)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. k - 2 .ge. 1 .and. (FOV_1(i, int(j + 2), int(k - 2)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 2), int(k + 3)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. k - 3 .ge. 1 .and. (FOV_1(i, int(j + 2), int(k - 3)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 3), k) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 3), k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 3), k) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 3), k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 3), k) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 3), k) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 3), int(k + 1)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. k - 1 .ge. 1 .and. (FOV_1(i, int(j + 3), int(k - 1)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 3), int(k + 2)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. k - 2 .ge. 1 .and. (FOV_1(i, int(j + 3), int(k - 2)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 3), int(k + 3)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. k - 3 .ge. 1 .and. (FOV_1(i, int(j + 3), int(k - 3)) .gt. 0))))) then
				Flux_extsubrhosupj_1(i, 4 - j, k) = 2.0d0 * Flux_extsubrhosupj_1(i, 4 - j + 1, k) - Flux_extsubrhosupj_1(i, 4 - j + 2, k)
				Flux_extsubmxsupj_1(i, 4 - j, k) = 2.0d0 * Flux_extsubmxsupj_1(i, 4 - j + 1, k) - Flux_extsubmxsupj_1(i, 4 - j + 2, k)
				Flux_extsubmysupj_1(i, 4 - j, k) = 2.0d0 * Flux_extsubmysupj_1(i, 4 - j + 1, k) - Flux_extsubmysupj_1(i, 4 - j + 2, k)
				Flux_extsubmzsupj_1(i, 4 - j, k) = 2.0d0 * Flux_extsubmzsupj_1(i, 4 - j + 1, k) - Flux_extsubmzsupj_1(i, 4 - j + 2, k)
		end if
		if ((FOV_yUpper(i, j, k) .gt. 0) .and. ((j - 1 .ge. 1 .and. (FOV_1(i, int(j - 1), k) .gt. 0)) .or. (j - 2 .ge. 1 .and. (FOV_1(i, int(j - 2), k) .gt. 0)) .or. (j - 3 .ge. 1 .and. (FOV_1(i, int(j - 3), k) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 1), k) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 1), k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 1), k) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 1), k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 1), k) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 1), k) .gt. 0)) .or. (j - 1 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 1), int(k + 1)) .gt. 0)) .or. (j - 1 .ge. 1 .and. k - 1 .ge. 1 .and. (FOV_1(i, int(j - 1), int(k - 1)) .gt. 0)) .or. (j - 1 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 1), int(k + 2)) .gt. 0)) .or. (j - 1 .ge. 1 .and. k - 2 .ge. 1 .and. (FOV_1(i, int(j - 1), int(k - 2)) .gt. 0)) .or. (j - 1 .ge. 1 .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 1), int(k + 3)) .gt. 0)) .or. (j - 1 .ge. 1 .and. k - 3 .ge. 1 .and. (FOV_1(i, int(j - 1), int(k - 3)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 2), k) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 2), k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 2), k) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 2), k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 2), k) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 2), k) .gt. 0)) .or. (j - 2 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 2), int(k + 1)) .gt. 0)) .or. (j - 2 .ge. 1 .and. k - 1 .ge. 1 .and. (FOV_1(i, int(j - 2), int(k - 1)) .gt. 0)) .or. (j - 2 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 2), int(k + 2)) .gt. 0)) .or. (j - 2 .ge. 1 .and. k - 2 .ge. 1 .and. (FOV_1(i, int(j - 2), int(k - 2)) .gt. 0)) .or. (j - 2 .ge. 1 .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 2), int(k + 3)) .gt. 0)) .or. (j - 2 .ge. 1 .and. k - 3 .ge. 1 .and. (FOV_1(i, int(j - 2), int(k - 3)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 3), k) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 3), k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 3), k) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 3), k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 3), k) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 3), k) .gt. 0)) .or. (j - 3 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 3), int(k + 1)) .gt. 0)) .or. (j - 3 .ge. 1 .and. k - 1 .ge. 1 .and. (FOV_1(i, int(j - 3), int(k - 1)) .gt. 0)) .or. (j - 3 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 3), int(k + 2)) .gt. 0)) .or. (j - 3 .ge. 1 .and. k - 2 .ge. 1 .and. (FOV_1(i, int(j - 3), int(k - 2)) .gt. 0)) .or. (j - 3 .ge. 1 .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 3), int(k + 3)) .gt. 0)) .or. (j - 3 .ge. 1 .and. k - 3 .ge. 1 .and. (FOV_1(i, int(j - 3), int(k - 3)) .gt. 0))))) then
			Flux_extsubrhosupj_1(i, j, k) = 2.0d0 * Flux_extsubrhosupj_1(i, j - 1, k) - Flux_extsubrhosupj_1(i, j - 2, k)
			Flux_extsubmxsupj_1(i, j, k) = 2.0d0 * Flux_extsubmxsupj_1(i, j - 1, k) - Flux_extsubmxsupj_1(i, j - 2, k)
			Flux_extsubmysupj_1(i, j, k) = 2.0d0 * Flux_extsubmysupj_1(i, j - 1, k) - Flux_extsubmysupj_1(i, j - 2, k)
			Flux_extsubmzsupj_1(i, j, k) = 2.0d0 * Flux_extsubmzsupj_1(i, j - 1, k) - Flux_extsubmzsupj_1(i, j - 2, k)
		end if
		if ((FOV_zLower(i, j, k) .gt. 0) .and. ((k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, j, int(k + 1)) .gt. 0)) .or. (k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, j, int(k + 2)) .gt. 0)) .or. (k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, j, int(k + 3)) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(int(i + 1), j, int(k + 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(int(i - 1), j, int(k + 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(int(i + 2), j, int(k + 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(int(i - 2), j, int(k + 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(int(i + 3), j, int(k + 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(int(i - 3), j, int(k + 1)) .gt. 0)) .or. (j + 1 .le. cctk_lsh(2) .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 1), int(k + 1)) .gt. 0)) .or. (j - 1 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 1), int(k + 1)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 2), int(k + 1)) .gt. 0)) .or. (j - 2 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 2), int(k + 1)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 3), int(k + 1)) .gt. 0)) .or. (j - 3 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 3), int(k + 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(int(i + 1), j, int(k + 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(int(i - 1), j, int(k + 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(int(i + 2), j, int(k + 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(int(i - 2), j, int(k + 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(int(i + 3), j, int(k + 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(int(i - 3), j, int(k + 2)) .gt. 0)) .or. (j + 1 .le. cctk_lsh(2) .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 1), int(k + 2)) .gt. 0)) .or. (j - 1 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 1), int(k + 2)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 2), int(k + 2)) .gt. 0)) .or. (j - 2 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 2), int(k + 2)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 3), int(k + 2)) .gt. 0)) .or. (j - 3 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 3), int(k + 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(int(i + 1), j, int(k + 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(int(i - 1), j, int(k + 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(int(i + 2), j, int(k + 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(int(i - 2), j, int(k + 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(int(i + 3), j, int(k + 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(int(i - 3), j, int(k + 3)) .gt. 0)) .or. (j + 1 .le. cctk_lsh(2) .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 1), int(k + 3)) .gt. 0)) .or. (j - 1 .ge. 1 .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 1), int(k + 3)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 2), int(k + 3)) .gt. 0)) .or. (j - 2 .ge. 1 .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 2), int(k + 3)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 3), int(k + 3)) .gt. 0)) .or. (j - 3 .ge. 1 .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 3), int(k + 3)) .gt. 0))))) then
				Flux_extsubrhosupk_1(i, j, 4 - k) = 2.0d0 * Flux_extsubrhosupk_1(i, j, 4 - k + 1) - Flux_extsubrhosupk_1(i, j, 4 - k + 2)
				Flux_extsubmxsupk_1(i, j, 4 - k) = 2.0d0 * Flux_extsubmxsupk_1(i, j, 4 - k + 1) - Flux_extsubmxsupk_1(i, j, 4 - k + 2)
				Flux_extsubmysupk_1(i, j, 4 - k) = 2.0d0 * Flux_extsubmysupk_1(i, j, 4 - k + 1) - Flux_extsubmysupk_1(i, j, 4 - k + 2)
				Flux_extsubmzsupk_1(i, j, 4 - k) = 2.0d0 * Flux_extsubmzsupk_1(i, j, 4 - k + 1) - Flux_extsubmzsupk_1(i, j, 4 - k + 2)
		end if
		if ((FOV_zUpper(i, j, k) .gt. 0) .and. ((k - 1 .ge. 1 .and. (FOV_1(i, j, int(k - 1)) .gt. 0)) .or. (k - 2 .ge. 1 .and. (FOV_1(i, j, int(k - 2)) .gt. 0)) .or. (k - 3 .ge. 1 .and. (FOV_1(i, j, int(k - 3)) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. k - 1 .ge. 1 .and. (FOV_1(int(i + 1), j, int(k - 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. k - 1 .ge. 1 .and. (FOV_1(int(i - 1), j, int(k - 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. k - 1 .ge. 1 .and. (FOV_1(int(i + 2), j, int(k - 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. k - 1 .ge. 1 .and. (FOV_1(int(i - 2), j, int(k - 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. k - 1 .ge. 1 .and. (FOV_1(int(i + 3), j, int(k - 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. k - 1 .ge. 1 .and. (FOV_1(int(i - 3), j, int(k - 1)) .gt. 0)) .or. (j + 1 .le. cctk_lsh(2) .and. k - 1 .ge. 1 .and. (FOV_1(i, int(j + 1), int(k - 1)) .gt. 0)) .or. (j - 1 .ge. 1 .and. k - 1 .ge. 1 .and. (FOV_1(i, int(j - 1), int(k - 1)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. k - 1 .ge. 1 .and. (FOV_1(i, int(j + 2), int(k - 1)) .gt. 0)) .or. (j - 2 .ge. 1 .and. k - 1 .ge. 1 .and. (FOV_1(i, int(j - 2), int(k - 1)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. k - 1 .ge. 1 .and. (FOV_1(i, int(j + 3), int(k - 1)) .gt. 0)) .or. (j - 3 .ge. 1 .and. k - 1 .ge. 1 .and. (FOV_1(i, int(j - 3), int(k - 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. k - 2 .ge. 1 .and. (FOV_1(int(i + 1), j, int(k - 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. k - 2 .ge. 1 .and. (FOV_1(int(i - 1), j, int(k - 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. k - 2 .ge. 1 .and. (FOV_1(int(i + 2), j, int(k - 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. k - 2 .ge. 1 .and. (FOV_1(int(i - 2), j, int(k - 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. k - 2 .ge. 1 .and. (FOV_1(int(i + 3), j, int(k - 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. k - 2 .ge. 1 .and. (FOV_1(int(i - 3), j, int(k - 2)) .gt. 0)) .or. (j + 1 .le. cctk_lsh(2) .and. k - 2 .ge. 1 .and. (FOV_1(i, int(j + 1), int(k - 2)) .gt. 0)) .or. (j - 1 .ge. 1 .and. k - 2 .ge. 1 .and. (FOV_1(i, int(j - 1), int(k - 2)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. k - 2 .ge. 1 .and. (FOV_1(i, int(j + 2), int(k - 2)) .gt. 0)) .or. (j - 2 .ge. 1 .and. k - 2 .ge. 1 .and. (FOV_1(i, int(j - 2), int(k - 2)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. k - 2 .ge. 1 .and. (FOV_1(i, int(j + 3), int(k - 2)) .gt. 0)) .or. (j - 3 .ge. 1 .and. k - 2 .ge. 1 .and. (FOV_1(i, int(j - 3), int(k - 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. k - 3 .ge. 1 .and. (FOV_1(int(i + 1), j, int(k - 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. k - 3 .ge. 1 .and. (FOV_1(int(i - 1), j, int(k - 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. k - 3 .ge. 1 .and. (FOV_1(int(i + 2), j, int(k - 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. k - 3 .ge. 1 .and. (FOV_1(int(i - 2), j, int(k - 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. k - 3 .ge. 1 .and. (FOV_1(int(i + 3), j, int(k - 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. k - 3 .ge. 1 .and. (FOV_1(int(i - 3), j, int(k - 3)) .gt. 0)) .or. (j + 1 .le. cctk_lsh(2) .and. k - 3 .ge. 1 .and. (FOV_1(i, int(j + 1), int(k - 3)) .gt. 0)) .or. (j - 1 .ge. 1 .and. k - 3 .ge. 1 .and. (FOV_1(i, int(j - 1), int(k - 3)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. k - 3 .ge. 1 .and. (FOV_1(i, int(j + 2), int(k - 3)) .gt. 0)) .or. (j - 2 .ge. 1 .and. k - 3 .ge. 1 .and. (FOV_1(i, int(j - 2), int(k - 3)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. k - 3 .ge. 1 .and. (FOV_1(i, int(j + 3), int(k - 3)) .gt. 0)) .or. (j - 3 .ge. 1 .and. k - 3 .ge. 1 .and. (FOV_1(i, int(j - 3), int(k - 3)) .gt. 0))))) then
			Flux_extsubrhosupk_1(i, j, k) = 2.0d0 * Flux_extsubrhosupk_1(i, j, k - 1) - Flux_extsubrhosupk_1(i, j, k - 2)
			Flux_extsubmxsupk_1(i, j, k) = 2.0d0 * Flux_extsubmxsupk_1(i, j, k - 1) - Flux_extsubmxsupk_1(i, j, k - 2)
			Flux_extsubmysupk_1(i, j, k) = 2.0d0 * Flux_extsubmysupk_1(i, j, k - 1) - Flux_extsubmysupk_1(i, j, k - 2)
			Flux_extsubmzsupk_1(i, j, k) = 2.0d0 * Flux_extsubmzsupk_1(i, j, k - 1) - Flux_extsubmzsupk_1(i, j, k - 2)
		end if
		if (FOV_1(i, j, k) .gt. 0) then
			if ((d_i_Null(i, j, k) .ne. 0 .or. d_j_Null(i, j, k) .ne. 0 .or. d_k_Null(i, j, k) .ne. 0)) then
				if (FOV_3(int(i - d_i_Null(i, j, k)), int(j - d_j_Null(i, j, k)), int(k - d_k_Null(i, j, k))) .gt. 0) then
					call extrapolate_flux(CCTK_PASS_FTOF, d_i_Null, Flux_extsubNullsupi_3, Flux_extsubNullsupi_3, i, d_j_Null, Flux_extsubNullsupj_3, Flux_extsubNullsupj_3, j, d_k_Null, Flux_extsubNullsupk_3, Flux_extsubNullsupk_3, k, FOV_3)
				end if
			end if
		end if
				end do
			end do
		end do
		call CCTK_SyncGroup(status, cctkGH, "FDAsNozzle3D::block1")
		do k = kStart, kEnd
			do j = jStart, jEnd
				do i = iStart, iEnd
					if ((FOV_3(i, j, k) .gt. 0 .or. (((i + 1 .le. cctk_lsh(1) .and. FOV_3(i + 1, j, k) .gt. 0) .or. (j + 1 .le. cctk_lsh(2) .and. FOV_3(i, j + 1, k) .gt. 0) .or. (k + 1 .le. cctk_lsh(3) .and. FOV_3(i, j, k + 1) .gt. 0)) .or. ((i - 1 .ge. 1 .and. FOV_3(i - 1, j, k) .gt. 0) .or. (j - 1 .ge. 1 .and. FOV_3(i, j - 1, k) .gt. 0) .or. (k - 1 .ge. 1 .and. FOV_3(i, j, k - 1) .gt. 0)))) .and. (i + 1 .le. cctk_lsh(1) .and. i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. j - 1 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. k - 1 .ge. 1)) then
					Speedi_3(i, j, k) = MAX(Speedi_3(i, j, k), Speedi_3(i + 1, j, k))
					Speedj_3(i, j, k) = MAX(Speedj_3(i, j, k), Speedj_3(i, j + 1, k))
					Speedk_3(i, j, k) = MAX(Speedk_3(i, j, k), Speedk_3(i, j, k + 1))
					end if
					if ((FOV_1(i, j, k) .gt. 0 .or. (((i + 1 .le. cctk_lsh(1) .and. FOV_1(i + 1, j, k) .gt. 0) .or. (j + 1 .le. cctk_lsh(2) .and. FOV_1(i, j + 1, k) .gt. 0) .or. (k + 1 .le. cctk_lsh(3) .and. FOV_1(i, j, k + 1) .gt. 0)) .or. ((i - 1 .ge. 1 .and. FOV_1(i - 1, j, k) .gt. 0) .or. (j - 1 .ge. 1 .and. FOV_1(i, j - 1, k) .gt. 0) .or. (k - 1 .ge. 1 .and. FOV_1(i, j, k - 1) .gt. 0)))) .and. (i + 1 .le. cctk_lsh(1) .and. i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. j - 1 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. k - 1 .ge. 1)) then
					Speedi_1(i, j, k) = MAX(Speedi_1(i, j, k), Speedi_1(i + 1, j, k))
					Speedj_1(i, j, k) = MAX(Speedj_1(i, j, k), Speedj_1(i, j + 1, k))
					Speedk_1(i, j, k) = MAX(Speedk_1(i, j, k), Speedk_1(i, j, k + 1))
					end if
				end do
			end do
		end do
		call CCTK_SyncGroup(status, cctkGH, "FDAsNozzle3D::block2")
		do k = kStart, kEnd
			do j = jStart, jEnd
				do i = iStart, iEnd
					if ((FOV_3(i, j, k) .gt. 0) .and. (i + 2 .le. cctk_lsh(1) .and. i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. j - 2 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. k - 2 .ge. 1)) then
					fluxAccNull_3 = 0.0d0
					fluxAccNull_3 = fluxAccNull_3 - FDOCi(CCTK_PASS_FTOF, extrapolatedsubNullsupi, Flux_extsubNullsupi_3, Speedi_3, i, j, k)
					fluxAccNull_3 = fluxAccNull_3 - FDOCj(CCTK_PASS_FTOF, extrapolatedsubNullsupj, Flux_extsubNullsupj_3, Speedj_3, i, j, k)
					fluxAccNull_3 = fluxAccNull_3 - FDOCk(CCTK_PASS_FTOF, extrapolatedsubNullsupk, Flux_extsubNullsupk_3, Speedk_3, i, j, k)
					k2Null(i, j, k) = RK3P2(CCTK_PASS_FTOF, fluxAccNull_3, Null_p, k1Null, i, j, k)
					end if
					if ((FOV_1(i, j, k) .gt. 0) .and. (i + 2 .le. cctk_lsh(1) .and. i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. j - 2 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. k - 2 .ge. 1)) then
					fluxAccrho_1 = 0.0d0
					fluxAccmx_1 = k1rho(i, j, k) * k1fx(i, j, k)
					fluxAccmy_1 = k1rho(i, j, k) * k1fy(i, j, k)
					fluxAccmz_1 = k1rho(i, j, k) * k1fz(i, j, k)
					fluxAccrho_1 = fluxAccrho_1 - FDOCi(CCTK_PASS_FTOF, extrapolatedsubrhosupi, Flux_extsubrhosupi_1, Speedi_1, i, j, k)
					fluxAccrho_1 = fluxAccrho_1 - FDOCj(CCTK_PASS_FTOF, extrapolatedsubrhosupj, Flux_extsubrhosupj_1, Speedj_1, i, j, k)
					fluxAccrho_1 = fluxAccrho_1 - FDOCk(CCTK_PASS_FTOF, extrapolatedsubrhosupk, Flux_extsubrhosupk_1, Speedk_1, i, j, k)
					fluxAccmx_1 = fluxAccmx_1 - FDOCi(CCTK_PASS_FTOF, extrapolatedsubmxsupi, Flux_extsubmxsupi_1, Speedi_1, i, j, k)
					fluxAccmx_1 = fluxAccmx_1 - FDOCj(CCTK_PASS_FTOF, extrapolatedsubmxsupj, Flux_extsubmxsupj_1, Speedj_1, i, j, k)
					fluxAccmx_1 = fluxAccmx_1 - FDOCk(CCTK_PASS_FTOF, extrapolatedsubmxsupk, Flux_extsubmxsupk_1, Speedk_1, i, j, k)
					fluxAccmy_1 = fluxAccmy_1 - FDOCi(CCTK_PASS_FTOF, extrapolatedsubmysupi, Flux_extsubmysupi_1, Speedi_1, i, j, k)
					fluxAccmy_1 = fluxAccmy_1 - FDOCj(CCTK_PASS_FTOF, extrapolatedsubmysupj, Flux_extsubmysupj_1, Speedj_1, i, j, k)
					fluxAccmy_1 = fluxAccmy_1 - FDOCk(CCTK_PASS_FTOF, extrapolatedsubmysupk, Flux_extsubmysupk_1, Speedk_1, i, j, k)
					fluxAccmz_1 = fluxAccmz_1 - FDOCi(CCTK_PASS_FTOF, extrapolatedsubmzsupi, Flux_extsubmzsupi_1, Speedi_1, i, j, k)
					fluxAccmz_1 = fluxAccmz_1 - FDOCj(CCTK_PASS_FTOF, extrapolatedsubmzsupj, Flux_extsubmzsupj_1, Speedj_1, i, j, k)
					fluxAccmz_1 = fluxAccmz_1 - FDOCk(CCTK_PASS_FTOF, extrapolatedsubmzsupk, Flux_extsubmzsupk_1, Speedk_1, i, j, k)
					n_x_1 = 0.0d0
					n_y_1 = 0.0d0
					n_z_1 = 0.0d0
					interaction_index_1 = 0.0d0
					if (((n_x_1 .eq. 0.0d0) .and. (n_y_1 .eq. 0.0d0)) .and. (n_z_1 .eq. 0.0d0)) then
					if (FOV_3(int(i + 1.0d0), j, k) .gt. 0.0d0) then
					interaction_index_1 = 1.0d0
					n_x_1 = n_x_1 + deltax
					end if
					if (FOV_3(int(i - 1.0d0), j, k) .gt. 0.0d0) then
					interaction_index_1 = 1.0d0
					n_x_1 = n_x_1 - deltax
					end if
					if (FOV_3(i, int(j + 1.0d0), k) .gt. 0.0d0) then
					interaction_index_1 = 1.0d0
					n_y_1 = n_y_1 + deltay
					end if
					if (FOV_3(i, int(j - 1.0d0), k) .gt. 0.0d0) then
					interaction_index_1 = 1.0d0
					n_y_1 = n_y_1 - deltay
					end if
					if (FOV_3(i, j, int(k + 1.0d0)) .gt. 0.0d0) then
					interaction_index_1 = 1.0d0
					n_z_1 = n_z_1 + deltaz
					end if
					if (FOV_3(i, j, int(k - 1.0d0)) .gt. 0.0d0) then
					interaction_index_1 = 1.0d0
					n_z_1 = n_z_1 - deltaz
					end if
					end if
					if (((n_x_1 .eq. 0.0d0) .and. (n_y_1 .eq. 0.0d0)) .and. (n_z_1 .eq. 0.0d0)) then
					if (((((((((FOV_3(int(i + 1.0d0), int(j + 1.0d0), int(k + 1.0d0)) .gt. 0.0d0) .or. (FOV_3(int(i + 1.0d0), int(j + 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(int(i + 1.0d0), int(j + 1.0d0), k) .gt. 0.0d0)) .or. (FOV_3(int(i + 1.0d0), int(j - 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(int(i + 1.0d0), int(j - 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(int(i + 1.0d0), int(j - 1.0d0), k) .gt. 0.0d0)) .or. (FOV_3(int(i + 1.0d0), j, int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(int(i + 1.0d0), j, int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(int(i + 1.0d0), j, k) .gt. 0.0d0)) then
					interaction_index_1 = 1.0d0
					n_x_1 = n_x_1 + deltax
					end if
					if (((((((((FOV_3(int(i - 1.0d0), int(j + 1.0d0), int(k + 1.0d0)) .gt. 0.0d0) .or. (FOV_3(int(i - 1.0d0), int(j + 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(int(i - 1.0d0), int(j + 1.0d0), k) .gt. 0.0d0)) .or. (FOV_3(int(i - 1.0d0), int(j - 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(int(i - 1.0d0), int(j - 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(int(i - 1.0d0), int(j - 1.0d0), k) .gt. 0.0d0)) .or. (FOV_3(int(i - 1.0d0), j, int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(int(i - 1.0d0), j, int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(int(i - 1.0d0), j, k) .gt. 0.0d0)) then
					interaction_index_1 = 1.0d0
					n_x_1 = n_x_1 - deltax
					end if
					if (((((((((FOV_3(int(i + 1.0d0), int(j + 1.0d0), int(k + 1.0d0)) .gt. 0.0d0) .or. (FOV_3(int(i + 1.0d0), int(j + 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(int(i + 1.0d0), int(j + 1.0d0), k) .gt. 0.0d0)) .or. (FOV_3(int(i - 1.0d0), int(j + 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(int(i - 1.0d0), int(j + 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(int(i - 1.0d0), int(j + 1.0d0), k) .gt. 0.0d0)) .or. (FOV_3(i, int(j + 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(i, int(j + 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(i, int(j + 1.0d0), k) .gt. 0.0d0)) then
					interaction_index_1 = 1.0d0
					n_y_1 = n_y_1 + deltay
					end if
					if (((((((((FOV_3(int(i + 1.0d0), int(j - 1.0d0), int(k + 1.0d0)) .gt. 0.0d0) .or. (FOV_3(int(i + 1.0d0), int(j - 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(int(i + 1.0d0), int(j - 1.0d0), k) .gt. 0.0d0)) .or. (FOV_3(int(i - 1.0d0), int(j - 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(int(i - 1.0d0), int(j - 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(int(i - 1.0d0), int(j - 1.0d0), k) .gt. 0.0d0)) .or. (FOV_3(i, int(j - 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(i, int(j - 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(i, int(j - 1.0d0), k) .gt. 0.0d0)) then
					interaction_index_1 = 1.0d0
					n_y_1 = n_y_1 - deltay
					end if
					if (((((((((FOV_3(int(i + 1.0d0), int(j + 1.0d0), int(k + 1.0d0)) .gt. 0.0d0) .or. (FOV_3(int(i + 1.0d0), int(j - 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(int(i + 1.0d0), j, int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(int(i - 1.0d0), int(j + 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(int(i - 1.0d0), int(j - 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(int(i - 1.0d0), j, int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(i, int(j + 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(i, int(j - 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(i, j, int(k + 1.0d0)) .gt. 0.0d0)) then
					interaction_index_1 = 1.0d0
					n_z_1 = n_z_1 + deltaz
					end if
					if (((((((((FOV_3(int(i + 1.0d0), int(j + 1.0d0), int(k - 1.0d0)) .gt. 0.0d0) .or. (FOV_3(int(i + 1.0d0), int(j - 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(int(i + 1.0d0), j, int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(int(i - 1.0d0), int(j + 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(int(i - 1.0d0), int(j - 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(int(i - 1.0d0), j, int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(i, int(j + 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(i, int(j - 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(i, j, int(k - 1.0d0)) .gt. 0.0d0)) then
					interaction_index_1 = 1.0d0
					n_z_1 = n_z_1 - deltaz
					end if
					end if
					if (((n_x_1 .eq. 0.0d0) .and. (n_y_1 .eq. 0.0d0)) .and. (n_z_1 .eq. 0.0d0)) then
					if (FOV_xLower(int(i + 1.0d0), j, k) .gt. 0.0d0) then
					interaction_index_1 = 2.0d0
					n_x_1 = n_x_1 + deltax
					end if
					if (FOV_xLower(int(i - 1.0d0), j, k) .gt. 0.0d0) then
					interaction_index_1 = 2.0d0
					n_x_1 = n_x_1 - deltax
					end if
					if (FOV_xLower(i, int(j + 1.0d0), k) .gt. 0.0d0) then
					interaction_index_1 = 2.0d0
					n_y_1 = n_y_1 + deltay
					end if
					if (FOV_xLower(i, int(j - 1.0d0), k) .gt. 0.0d0) then
					interaction_index_1 = 2.0d0
					n_y_1 = n_y_1 - deltay
					end if
					if (FOV_xLower(i, j, int(k + 1.0d0)) .gt. 0.0d0) then
					interaction_index_1 = 2.0d0
					n_z_1 = n_z_1 + deltaz
					end if
					if (FOV_xLower(i, j, int(k - 1.0d0)) .gt. 0.0d0) then
					interaction_index_1 = 2.0d0
					n_z_1 = n_z_1 - deltaz
					end if
					end if
					if (((n_x_1 .eq. 0.0d0) .and. (n_y_1 .eq. 0.0d0)) .and. (n_z_1 .eq. 0.0d0)) then
					if (((((((((FOV_xLower(int(i + 1.0d0), int(j + 1.0d0), int(k + 1.0d0)) .gt. 0.0d0) .or. (FOV_xLower(int(i + 1.0d0), int(j + 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i + 1.0d0), int(j + 1.0d0), k) .gt. 0.0d0)) .or. (FOV_xLower(int(i + 1.0d0), int(j - 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i + 1.0d0), int(j - 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i + 1.0d0), int(j - 1.0d0), k) .gt. 0.0d0)) .or. (FOV_xLower(int(i + 1.0d0), j, int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i + 1.0d0), j, int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i + 1.0d0), j, k) .gt. 0.0d0)) then
					interaction_index_1 = 2.0d0
					n_x_1 = n_x_1 + deltax
					end if
					if (((((((((FOV_xLower(int(i - 1.0d0), int(j + 1.0d0), int(k + 1.0d0)) .gt. 0.0d0) .or. (FOV_xLower(int(i - 1.0d0), int(j + 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i - 1.0d0), int(j + 1.0d0), k) .gt. 0.0d0)) .or. (FOV_xLower(int(i - 1.0d0), int(j - 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i - 1.0d0), int(j - 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i - 1.0d0), int(j - 1.0d0), k) .gt. 0.0d0)) .or. (FOV_xLower(int(i - 1.0d0), j, int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i - 1.0d0), j, int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i - 1.0d0), j, k) .gt. 0.0d0)) then
					interaction_index_1 = 2.0d0
					n_x_1 = n_x_1 - deltax
					end if
					if (((((((((FOV_xLower(int(i + 1.0d0), int(j + 1.0d0), int(k + 1.0d0)) .gt. 0.0d0) .or. (FOV_xLower(int(i + 1.0d0), int(j + 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i + 1.0d0), int(j + 1.0d0), k) .gt. 0.0d0)) .or. (FOV_xLower(int(i - 1.0d0), int(j + 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i - 1.0d0), int(j + 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i - 1.0d0), int(j + 1.0d0), k) .gt. 0.0d0)) .or. (FOV_xLower(i, int(j + 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(i, int(j + 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(i, int(j + 1.0d0), k) .gt. 0.0d0)) then
					interaction_index_1 = 2.0d0
					n_y_1 = n_y_1 + deltay
					end if
					if (((((((((FOV_xLower(int(i + 1.0d0), int(j - 1.0d0), int(k + 1.0d0)) .gt. 0.0d0) .or. (FOV_xLower(int(i + 1.0d0), int(j - 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i + 1.0d0), int(j - 1.0d0), k) .gt. 0.0d0)) .or. (FOV_xLower(int(i - 1.0d0), int(j - 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i - 1.0d0), int(j - 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i - 1.0d0), int(j - 1.0d0), k) .gt. 0.0d0)) .or. (FOV_xLower(i, int(j - 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(i, int(j - 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(i, int(j - 1.0d0), k) .gt. 0.0d0)) then
					interaction_index_1 = 2.0d0
					n_y_1 = n_y_1 - deltay
					end if
					if (((((((((FOV_xLower(int(i + 1.0d0), int(j + 1.0d0), int(k + 1.0d0)) .gt. 0.0d0) .or. (FOV_xLower(int(i + 1.0d0), int(j - 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i + 1.0d0), j, int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i - 1.0d0), int(j + 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i - 1.0d0), int(j - 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i - 1.0d0), j, int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(i, int(j + 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(i, int(j - 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(i, j, int(k + 1.0d0)) .gt. 0.0d0)) then
					interaction_index_1 = 2.0d0
					n_z_1 = n_z_1 + deltaz
					end if
					if (((((((((FOV_xLower(int(i + 1.0d0), int(j + 1.0d0), int(k - 1.0d0)) .gt. 0.0d0) .or. (FOV_xLower(int(i + 1.0d0), int(j - 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i + 1.0d0), j, int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i - 1.0d0), int(j + 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i - 1.0d0), int(j - 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i - 1.0d0), j, int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(i, int(j + 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(i, int(j - 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(i, j, int(k - 1.0d0)) .gt. 0.0d0)) then
					interaction_index_1 = 2.0d0
					n_z_1 = n_z_1 - deltaz
					end if
					end if
					if (((n_x_1 .eq. 0.0d0) .and. (n_y_1 .eq. 0.0d0)) .and. (n_z_1 .eq. 0.0d0)) then
					if (FOV_xUpper(int(i + 1.0d0), j, k) .gt. 0.0d0) then
					interaction_index_1 = 3.0d0
					n_x_1 = n_x_1 + deltax
					end if
					if (FOV_xUpper(int(i - 1.0d0), j, k) .gt. 0.0d0) then
					interaction_index_1 = 3.0d0
					n_x_1 = n_x_1 - deltax
					end if
					if (FOV_xUpper(i, int(j + 1.0d0), k) .gt. 0.0d0) then
					interaction_index_1 = 3.0d0
					n_y_1 = n_y_1 + deltay
					end if
					if (FOV_xUpper(i, int(j - 1.0d0), k) .gt. 0.0d0) then
					interaction_index_1 = 3.0d0
					n_y_1 = n_y_1 - deltay
					end if
					if (FOV_xUpper(i, j, int(k + 1.0d0)) .gt. 0.0d0) then
					interaction_index_1 = 3.0d0
					n_z_1 = n_z_1 + deltaz
					end if
					if (FOV_xUpper(i, j, int(k - 1.0d0)) .gt. 0.0d0) then
					interaction_index_1 = 3.0d0
					n_z_1 = n_z_1 - deltaz
					end if
					end if
					if (((n_x_1 .eq. 0.0d0) .and. (n_y_1 .eq. 0.0d0)) .and. (n_z_1 .eq. 0.0d0)) then
					if (((((((((FOV_xUpper(int(i + 1.0d0), int(j + 1.0d0), int(k + 1.0d0)) .gt. 0.0d0) .or. (FOV_xUpper(int(i + 1.0d0), int(j + 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i + 1.0d0), int(j + 1.0d0), k) .gt. 0.0d0)) .or. (FOV_xUpper(int(i + 1.0d0), int(j - 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i + 1.0d0), int(j - 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i + 1.0d0), int(j - 1.0d0), k) .gt. 0.0d0)) .or. (FOV_xUpper(int(i + 1.0d0), j, int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i + 1.0d0), j, int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i + 1.0d0), j, k) .gt. 0.0d0)) then
					interaction_index_1 = 3.0d0
					n_x_1 = n_x_1 + deltax
					end if
					if (((((((((FOV_xUpper(int(i - 1.0d0), int(j + 1.0d0), int(k + 1.0d0)) .gt. 0.0d0) .or. (FOV_xUpper(int(i - 1.0d0), int(j + 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i - 1.0d0), int(j + 1.0d0), k) .gt. 0.0d0)) .or. (FOV_xUpper(int(i - 1.0d0), int(j - 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i - 1.0d0), int(j - 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i - 1.0d0), int(j - 1.0d0), k) .gt. 0.0d0)) .or. (FOV_xUpper(int(i - 1.0d0), j, int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i - 1.0d0), j, int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i - 1.0d0), j, k) .gt. 0.0d0)) then
					interaction_index_1 = 3.0d0
					n_x_1 = n_x_1 - deltax
					end if
					if (((((((((FOV_xUpper(int(i + 1.0d0), int(j + 1.0d0), int(k + 1.0d0)) .gt. 0.0d0) .or. (FOV_xUpper(int(i + 1.0d0), int(j + 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i + 1.0d0), int(j + 1.0d0), k) .gt. 0.0d0)) .or. (FOV_xUpper(int(i - 1.0d0), int(j + 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i - 1.0d0), int(j + 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i - 1.0d0), int(j + 1.0d0), k) .gt. 0.0d0)) .or. (FOV_xUpper(i, int(j + 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(i, int(j + 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(i, int(j + 1.0d0), k) .gt. 0.0d0)) then
					interaction_index_1 = 3.0d0
					n_y_1 = n_y_1 + deltay
					end if
					if (((((((((FOV_xUpper(int(i + 1.0d0), int(j - 1.0d0), int(k + 1.0d0)) .gt. 0.0d0) .or. (FOV_xUpper(int(i + 1.0d0), int(j - 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i + 1.0d0), int(j - 1.0d0), k) .gt. 0.0d0)) .or. (FOV_xUpper(int(i - 1.0d0), int(j - 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i - 1.0d0), int(j - 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i - 1.0d0), int(j - 1.0d0), k) .gt. 0.0d0)) .or. (FOV_xUpper(i, int(j - 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(i, int(j - 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(i, int(j - 1.0d0), k) .gt. 0.0d0)) then
					interaction_index_1 = 3.0d0
					n_y_1 = n_y_1 - deltay
					end if
					if (((((((((FOV_xUpper(int(i + 1.0d0), int(j + 1.0d0), int(k + 1.0d0)) .gt. 0.0d0) .or. (FOV_xUpper(int(i + 1.0d0), int(j - 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i + 1.0d0), j, int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i - 1.0d0), int(j + 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i - 1.0d0), int(j - 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i - 1.0d0), j, int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(i, int(j + 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(i, int(j - 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(i, j, int(k + 1.0d0)) .gt. 0.0d0)) then
					interaction_index_1 = 3.0d0
					n_z_1 = n_z_1 + deltaz
					end if
					if (((((((((FOV_xUpper(int(i + 1.0d0), int(j + 1.0d0), int(k - 1.0d0)) .gt. 0.0d0) .or. (FOV_xUpper(int(i + 1.0d0), int(j - 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i + 1.0d0), j, int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i - 1.0d0), int(j + 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i - 1.0d0), int(j - 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i - 1.0d0), j, int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(i, int(j + 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(i, int(j - 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(i, j, int(k - 1.0d0)) .gt. 0.0d0)) then
					interaction_index_1 = 3.0d0
					n_z_1 = n_z_1 - deltaz
					end if
					end if
					if (((n_x_1 .ne. 0.0d0) .or. (n_y_1 .ne. 0.0d0)) .or. (n_z_1 .ne. 0.0d0)) then
					mod_normal_1 = SQRT((n_x_1 ** 2.0d0 + n_y_1 ** 2.0d0) + n_z_1 ** 2.0d0)
					n_x_1 = n_x_1 / mod_normal_1
					n_y_1 = n_y_1 / mod_normal_1
					n_z_1 = n_z_1 / mod_normal_1
					end if
					if (interaction_index_1 .eq. 1.0d0) then
					fluxAccm_n_1 = fluxAccmx_1 * n_x_1 + fluxAccmy_1 * n_y_1 + fluxAccmz_1 * n_z_1
					fluxAccm_tx_1 = fluxAccmx_1 - (fluxAccmx_1 * n_x_1 + fluxAccmy_1 * n_y_1 + fluxAccmz_1 * n_z_1) * n_x_1
					fluxAccm_ty_1 = fluxAccmy_1 - (fluxAccmx_1 * n_x_1 + fluxAccmy_1 * n_y_1 + fluxAccmz_1 * n_z_1) * n_y_1
					fluxAccm_tz_1 = fluxAccmz_1 - (fluxAccmx_1 * n_x_1 + fluxAccmy_1 * n_y_1 + fluxAccmz_1 * n_z_1) * n_z_1
					m_n_1 = k1mx(i, j, k) * n_x_1 + k1my(i, j, k) * n_y_1 + k1mz(i, j, k) * n_z_1
					m_tx_1 = k1mx(i, j, k) - (k1mx(i, j, k) * n_x_1 + k1my(i, j, k) * n_y_1 + k1mz(i, j, k) * n_z_1) * n_x_1
					m_ty_1 = k1my(i, j, k) - (k1mx(i, j, k) * n_x_1 + k1my(i, j, k) * n_y_1 + k1mz(i, j, k) * n_z_1) * n_y_1
					m_tz_1 = k1mz(i, j, k) - (k1mx(i, j, k) * n_x_1 + k1my(i, j, k) * n_y_1 + k1mz(i, j, k) * n_z_1) * n_z_1
					c_1 = c0 * ((k1rho(i, j, k) / rho0) ** ((gamma + (-1.0d0)) / 2.0d0))
					cn_1 = m_n_1 / k1rho(i, j, k)
					if ((c_1 + cn_1) .gt. 0.0d0) then
					fluxAccwnp_3_1 = fluxAccrho_1 * (-((-c_1) + cn_1)) + fluxAccm_n_1
					else
					fluxAccwnp_3_1 = (-fluxAccm_n_1) + fluxAccrho_1 * (-((-c_1) + cn_1))
					end if
					if (((-c_1) + cn_1) .gt. 0.0d0) then
					fluxAccwnm_3_1 = fluxAccrho_1 * (-(c_1 + cn_1)) + fluxAccm_n_1
					else
					fluxAccwnm_3_1 = (-fluxAccm_n_1) + fluxAccrho_1 * (-(c_1 + cn_1))
					end if
					if (cn_1 .gt. 0.0d0) then
					fluxAccwtx_3_1 = fluxAccrho_1 * (-m_tx_1) / k1rho(i, j, k) + fluxAccm_tx_1
					else
					fluxAccwtx_3_1 = fluxAccrho_1 * (-m_tx_1) / k1rho(i, j, k)
					end if
					if (cn_1 .gt. 0.0d0) then
					fluxAccwty_3_1 = fluxAccrho_1 * (-m_ty_1) / k1rho(i, j, k) + fluxAccm_ty_1
					else
					fluxAccwty_3_1 = fluxAccrho_1 * (-m_ty_1) / k1rho(i, j, k)
					end if
					if (cn_1 .gt. 0.0d0) then
					fluxAccwtz_3_1 = fluxAccrho_1 * (-m_tz_1) / k1rho(i, j, k) + fluxAccm_tz_1
					else
					fluxAccwtz_3_1 = fluxAccrho_1 * (-m_tz_1) / k1rho(i, j, k)
					end if
					fluxAccrho_1 = fluxAccwnp_3_1 * 1.0d0 / (2.0d0 * c_1) + fluxAccwnm_3_1 * (-1.0d0) / (2.0d0 * c_1)
					fluxAccm_n_1 = fluxAccwnp_3_1 * (cn_1 / (2.0d0 * c_1) + 1.0d0 / 2.0d0) + fluxAccwnm_3_1 * (-(cn_1 / (2.0d0 * c_1) + (-1.0d0 / 2.0d0)))
					fluxAccm_tx_1 = (fluxAccwnp_3_1 * m_tx_1 / (2.0d0 * k1rho(i, j, k) * c_1) + fluxAccwnm_3_1 * (-m_tx_1) / (2.0d0 * k1rho(i, j, k) * c_1)) + fluxAccwtx_3_1
					fluxAccm_ty_1 = (fluxAccwnp_3_1 * m_ty_1 / (2.0d0 * k1rho(i, j, k) * c_1) + fluxAccwnm_3_1 * (-m_ty_1) / (2.0d0 * k1rho(i, j, k) * c_1)) + fluxAccwty_3_1
					fluxAccm_tz_1 = (fluxAccwnp_3_1 * m_tz_1 / (2.0d0 * k1rho(i, j, k) * c_1) + fluxAccwnm_3_1 * (-m_tz_1) / (2.0d0 * k1rho(i, j, k) * c_1)) + fluxAccwtz_3_1
					fluxAccmx_1 = fluxAccm_tx_1 + fluxAccm_n_1 * n_x_1
					fluxAccmy_1 = fluxAccm_ty_1 + fluxAccm_n_1 * n_y_1
					fluxAccmz_1 = fluxAccm_tz_1 + fluxAccm_n_1 * n_z_1
					end if
					if (interaction_index_1 .eq. 2.0d0) then
					fluxAccm_n_1 = fluxAccmx_1 * n_x_1 + fluxAccmy_1 * n_y_1 + fluxAccmz_1 * n_z_1
					fluxAccm_tx_1 = fluxAccmx_1 - (fluxAccmx_1 * n_x_1 + fluxAccmy_1 * n_y_1 + fluxAccmz_1 * n_z_1) * n_x_1
					fluxAccm_ty_1 = fluxAccmy_1 - (fluxAccmx_1 * n_x_1 + fluxAccmy_1 * n_y_1 + fluxAccmz_1 * n_z_1) * n_y_1
					fluxAccm_tz_1 = fluxAccmz_1 - (fluxAccmx_1 * n_x_1 + fluxAccmy_1 * n_y_1 + fluxAccmz_1 * n_z_1) * n_z_1
					m_n_1 = k1mx(i, j, k) * n_x_1 + k1my(i, j, k) * n_y_1 + k1mz(i, j, k) * n_z_1
					m_tx_1 = k1mx(i, j, k) - (k1mx(i, j, k) * n_x_1 + k1my(i, j, k) * n_y_1 + k1mz(i, j, k) * n_z_1) * n_x_1
					m_ty_1 = k1my(i, j, k) - (k1mx(i, j, k) * n_x_1 + k1my(i, j, k) * n_y_1 + k1mz(i, j, k) * n_z_1) * n_y_1
					m_tz_1 = k1mz(i, j, k) - (k1mx(i, j, k) * n_x_1 + k1my(i, j, k) * n_y_1 + k1mz(i, j, k) * n_z_1) * n_z_1
					c_1 = c0 * ((k1rho(i, j, k) / rho0) ** ((gamma + (-1.0d0)) / 2.0d0))
					cn_1 = m_n_1 / k1rho(i, j, k)
					if ((c_1 + cn_1) .gt. 0.0d0) then
					fluxAccwnp_3_1 = fluxAccrho_1 * (-((-c_1) + cn_1)) + fluxAccm_n_1
					else
					fluxAccwnp_3_1 = (-lambdain) * (m_n_1 + k1rho(i, j, k) * v0 * (1.0d0 - (((-0.008d0) + (j - (1 - cctk_lbnd(2))) * deltay) ** 2.0d0 + ((-0.008d0) + (k - (1 - cctk_lbnd(3))) * deltaz) ** 2.0d0) / ((FLOOR((0.006d0 * ABS((-0.008d0) + (j - (1 - cctk_lbnd(2))) * deltay)) / (SQRT(((-0.008d0) + (j - (1 - cctk_lbnd(2))) * deltay) ** 2.0d0 + ((-0.008d0) + (k - (1 - cctk_lbnd(3))) * deltaz) ** 2.0d0) * deltay) + 0.5d0) * deltay) ** 2.0d0 + (FLOOR((0.006d0 * ABS((-0.008d0) + (k - (1 - cctk_lbnd(3))) * deltaz)) / (SQRT(((-0.008d0) + (j - (1 - cctk_lbnd(2))) * deltay) ** 2.0d0 + ((-0.008d0) + (k - (1 - cctk_lbnd(3))) * deltaz) ** 2.0d0) * deltaz) + 0.5d0) * deltaz) ** 2.0d0)))
					end if
					if (((-c_1) + cn_1) .gt. 0.0d0) then
					fluxAccwnm_3_1 = fluxAccrho_1 * (-(c_1 + cn_1)) + fluxAccm_n_1
					else
					fluxAccwnm_3_1 = (-lambdain) * (m_n_1 + k1rho(i, j, k) * v0 * (1.0d0 - (((-0.008d0) + (j - (1 - cctk_lbnd(2))) * deltay) ** 2.0d0 + ((-0.008d0) + (k - (1 - cctk_lbnd(3))) * deltaz) ** 2.0d0) / ((FLOOR((0.006d0 * ABS((-0.008d0) + (j - (1 - cctk_lbnd(2))) * deltay)) / (SQRT(((-0.008d0) + (j - (1 - cctk_lbnd(2))) * deltay) ** 2.0d0 + ((-0.008d0) + (k - (1 - cctk_lbnd(3))) * deltaz) ** 2.0d0) * deltay) + 0.5d0) * deltay) ** 2.0d0 + (FLOOR((0.006d0 * ABS((-0.008d0) + (k - (1 - cctk_lbnd(3))) * deltaz)) / (SQRT(((-0.008d0) + (j - (1 - cctk_lbnd(2))) * deltay) ** 2.0d0 + ((-0.008d0) + (k - (1 - cctk_lbnd(3))) * deltaz) ** 2.0d0) * deltaz) + 0.5d0) * deltaz) ** 2.0d0)))
					end if
					if (cn_1 .gt. 0.0d0) then
					fluxAccwtx_3_1 = fluxAccrho_1 * (-m_tx_1) / k1rho(i, j, k) + fluxAccm_tx_1
					else
					fluxAccwtx_3_1 = (-lambdain) * (m_tx_1 - k1rho(i, j, k) * vt0)
					end if
					if (cn_1 .gt. 0.0d0) then
					fluxAccwty_3_1 = fluxAccrho_1 * (-m_ty_1) / k1rho(i, j, k) + fluxAccm_ty_1
					else
					fluxAccwty_3_1 = (-lambdain) * (m_ty_1 - k1rho(i, j, k) * vt0)
					end if
					if (cn_1 .gt. 0.0d0) then
					fluxAccwtz_3_1 = fluxAccrho_1 * (-m_tz_1) / k1rho(i, j, k) + fluxAccm_tz_1
					else
					fluxAccwtz_3_1 = (-lambdain) * (m_tz_1 - k1rho(i, j, k) * vt0)
					end if
					fluxAccrho_1 = fluxAccwnp_3_1 * 1.0d0 / (2.0d0 * c_1) + fluxAccwnm_3_1 * (-1.0d0) / (2.0d0 * c_1)
					fluxAccm_n_1 = fluxAccwnp_3_1 * (cn_1 / (2.0d0 * c_1) + 1.0d0 / 2.0d0) + fluxAccwnm_3_1 * (-(cn_1 / (2.0d0 * c_1) + (-1.0d0 / 2.0d0)))
					fluxAccm_tx_1 = (fluxAccwnp_3_1 * m_tx_1 / (2.0d0 * k1rho(i, j, k) * c_1) + fluxAccwnm_3_1 * (-m_tx_1) / (2.0d0 * k1rho(i, j, k) * c_1)) + fluxAccwtx_3_1
					fluxAccm_ty_1 = (fluxAccwnp_3_1 * m_ty_1 / (2.0d0 * k1rho(i, j, k) * c_1) + fluxAccwnm_3_1 * (-m_ty_1) / (2.0d0 * k1rho(i, j, k) * c_1)) + fluxAccwty_3_1
					fluxAccm_tz_1 = (fluxAccwnp_3_1 * m_tz_1 / (2.0d0 * k1rho(i, j, k) * c_1) + fluxAccwnm_3_1 * (-m_tz_1) / (2.0d0 * k1rho(i, j, k) * c_1)) + fluxAccwtz_3_1
					fluxAccmx_1 = fluxAccm_tx_1 + fluxAccm_n_1 * n_x_1
					fluxAccmy_1 = fluxAccm_ty_1 + fluxAccm_n_1 * n_y_1
					fluxAccmz_1 = fluxAccm_tz_1 + fluxAccm_n_1 * n_z_1
					end if
					if (interaction_index_1 .eq. 3.0d0) then
					fluxAccm_n_1 = fluxAccmx_1 * n_x_1 + fluxAccmy_1 * n_y_1 + fluxAccmz_1 * n_z_1
					fluxAccm_tx_1 = fluxAccmx_1 - (fluxAccmx_1 * n_x_1 + fluxAccmy_1 * n_y_1 + fluxAccmz_1 * n_z_1) * n_x_1
					fluxAccm_ty_1 = fluxAccmy_1 - (fluxAccmx_1 * n_x_1 + fluxAccmy_1 * n_y_1 + fluxAccmz_1 * n_z_1) * n_y_1
					fluxAccm_tz_1 = fluxAccmz_1 - (fluxAccmx_1 * n_x_1 + fluxAccmy_1 * n_y_1 + fluxAccmz_1 * n_z_1) * n_z_1
					m_n_1 = k1mx(i, j, k) * n_x_1 + k1my(i, j, k) * n_y_1 + k1mz(i, j, k) * n_z_1
					m_tx_1 = k1mx(i, j, k) - (k1mx(i, j, k) * n_x_1 + k1my(i, j, k) * n_y_1 + k1mz(i, j, k) * n_z_1) * n_x_1
					m_ty_1 = k1my(i, j, k) - (k1mx(i, j, k) * n_x_1 + k1my(i, j, k) * n_y_1 + k1mz(i, j, k) * n_z_1) * n_y_1
					m_tz_1 = k1mz(i, j, k) - (k1mx(i, j, k) * n_x_1 + k1my(i, j, k) * n_y_1 + k1mz(i, j, k) * n_z_1) * n_z_1
					c_1 = c0 * ((k1rho(i, j, k) / rho0) ** ((gamma + (-1.0d0)) / 2.0d0))
					cn_1 = m_n_1 / k1rho(i, j, k)
					if ((c_1 + cn_1) .gt. 0.0d0) then
					fluxAccwnp_3_1 = fluxAccrho_1 * (-((-c_1) + cn_1)) + fluxAccm_n_1
					else
					fluxAccwnp_3_1 = ((-c_1) + cn_1) * lambdaout * (k1rho(i, j, k) - rho0)
					end if
					if (((-c_1) + cn_1) .gt. 0.0d0) then
					fluxAccwnm_3_1 = fluxAccrho_1 * (-(c_1 + cn_1)) + fluxAccm_n_1
					else
					fluxAccwnm_3_1 = (c_1 + cn_1) * lambdaout * (k1rho(i, j, k) - rho0)
					end if
					if (cn_1 .gt. 0.0d0) then
					fluxAccwtx_3_1 = fluxAccrho_1 * (-m_tx_1) / k1rho(i, j, k) + fluxAccm_tx_1
					else
					fluxAccwtx_3_1 = m_tx_1 / k1rho(i, j, k) * lambdaout * (k1rho(i, j, k) - rho0)
					end if
					if (cn_1 .gt. 0.0d0) then
					fluxAccwty_3_1 = fluxAccrho_1 * (-m_ty_1) / k1rho(i, j, k) + fluxAccm_ty_1
					else
					fluxAccwty_3_1 = m_ty_1 / k1rho(i, j, k) * lambdaout * (k1rho(i, j, k) - rho0)
					end if
					if (cn_1 .gt. 0.0d0) then
					fluxAccwtz_3_1 = fluxAccrho_1 * (-m_tz_1) / k1rho(i, j, k) + fluxAccm_tz_1
					else
					fluxAccwtz_3_1 = m_tz_1 / k1rho(i, j, k) * lambdaout * (k1rho(i, j, k) - rho0)
					end if
					fluxAccrho_1 = fluxAccwnp_3_1 * 1.0d0 / (2.0d0 * c_1) + fluxAccwnm_3_1 * (-1.0d0) / (2.0d0 * c_1)
					fluxAccm_n_1 = fluxAccwnp_3_1 * (cn_1 / (2.0d0 * c_1) + 1.0d0 / 2.0d0) + fluxAccwnm_3_1 * (-(cn_1 / (2.0d0 * c_1) + (-1.0d0 / 2.0d0)))
					fluxAccm_tx_1 = (fluxAccwnp_3_1 * m_tx_1 / (2.0d0 * k1rho(i, j, k) * c_1) + fluxAccwnm_3_1 * (-m_tx_1) / (2.0d0 * k1rho(i, j, k) * c_1)) + fluxAccwtx_3_1
					fluxAccm_ty_1 = (fluxAccwnp_3_1 * m_ty_1 / (2.0d0 * k1rho(i, j, k) * c_1) + fluxAccwnm_3_1 * (-m_ty_1) / (2.0d0 * k1rho(i, j, k) * c_1)) + fluxAccwty_3_1
					fluxAccm_tz_1 = (fluxAccwnp_3_1 * m_tz_1 / (2.0d0 * k1rho(i, j, k) * c_1) + fluxAccwnm_3_1 * (-m_tz_1) / (2.0d0 * k1rho(i, j, k) * c_1)) + fluxAccwtz_3_1
					fluxAccmx_1 = fluxAccm_tx_1 + fluxAccm_n_1 * n_x_1
					fluxAccmy_1 = fluxAccm_ty_1 + fluxAccm_n_1 * n_y_1
					fluxAccmz_1 = fluxAccm_tz_1 + fluxAccm_n_1 * n_z_1
					end if
					k2rho(i, j, k) = RK3P2(CCTK_PASS_FTOF, fluxAccrho_1, rho_p, k1rho, i, j, k)
					k2mx(i, j, k) = RK3P2(CCTK_PASS_FTOF, fluxAccmx_1, mx_p, k1mx, i, j, k)
					k2my(i, j, k) = RK3P2(CCTK_PASS_FTOF, fluxAccmy_1, my_p, k1my, i, j, k)
					k2mz(i, j, k) = RK3P2(CCTK_PASS_FTOF, fluxAccmz_1, mz_p, k1mz, i, j, k)
					k2p(i, j, k) = (Paux + ((k2rho(i, j, k) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					k2fx(i, j, k) = pfx
					k2fy(i, j, k) = pfy
					k2fz(i, j, k) = pfz
					end if
				end do
			end do
		end do
		call CCTK_SyncGroup(status, cctkGH, "FDAsNozzle3D::block8")
		do k = kStart, kEnd
			do j = jStart, jEnd
				do i = iStart, iEnd
					extrapolatedsubrhosupi(i, j, k) = k2rho(i, j, k)
					extrapolatedsubrhosupj(i, j, k) = k2rho(i, j, k)
					extrapolatedsubrhosupk(i, j, k) = k2rho(i, j, k)
					extrapolatedsubmxsupi(i, j, k) = k2mx(i, j, k)
					extrapolatedsubmxsupj(i, j, k) = k2mx(i, j, k)
					extrapolatedsubmxsupk(i, j, k) = k2mx(i, j, k)
					extrapolatedsubmysupi(i, j, k) = k2my(i, j, k)
					extrapolatedsubmysupj(i, j, k) = k2my(i, j, k)
					extrapolatedsubmysupk(i, j, k) = k2my(i, j, k)
					extrapolatedsubmzsupi(i, j, k) = k2mz(i, j, k)
					extrapolatedsubmzsupj(i, j, k) = k2mz(i, j, k)
					extrapolatedsubmzsupk(i, j, k) = k2mz(i, j, k)
					extrapolatedsubpsupi(i, j, k) = k2p(i, j, k)
					extrapolatedsubpsupj(i, j, k) = k2p(i, j, k)
					extrapolatedsubpsupk(i, j, k) = k2p(i, j, k)
					extrapolatedsubfxsupi(i, j, k) = k2fx(i, j, k)
					extrapolatedsubfxsupj(i, j, k) = k2fx(i, j, k)
					extrapolatedsubfxsupk(i, j, k) = k2fx(i, j, k)
					extrapolatedsubfysupi(i, j, k) = k2fy(i, j, k)
					extrapolatedsubfysupj(i, j, k) = k2fy(i, j, k)
					extrapolatedsubfysupk(i, j, k) = k2fy(i, j, k)
					extrapolatedsubfzsupi(i, j, k) = k2fz(i, j, k)
					extrapolatedsubfzsupj(i, j, k) = k2fz(i, j, k)
					extrapolatedsubfzsupk(i, j, k) = k2fz(i, j, k)
					extrapolatedsubNullsupi(i, j, k) = k2Null(i, j, k)
					extrapolatedsubNullsupj(i, j, k) = k2Null(i, j, k)
					extrapolatedsubNullsupk(i, j, k) = k2Null(i, j, k)
				end do
			end do
		end do
		do k = kStart, kEnd
			do j = jStart, jEnd
				do i = iStart, iEnd
!					Boundaries
		if ((FOV_xLower(i, j, k) .gt. 0) .and. ((i + 1 .le. cctk_lsh(1) .and. (FOV_3(int(i + 1), j, k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. (FOV_3(int(i + 2), j, k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. (FOV_3(int(i + 3), j, k) .gt. 0)))) then
				k2Null(4 - i, j, k) = 0.0d0
		end if
		if ((FOV_xUpper(i, j, k) .gt. 0) .and. ((i - 1 .ge. 1 .and. (FOV_3(int(i - 1), j, k) .gt. 0)) .or. (i - 2 .ge. 1 .and. (FOV_3(int(i - 2), j, k) .gt. 0)) .or. (i - 3 .ge. 1 .and. (FOV_3(int(i - 3), j, k) .gt. 0)))) then
			k2Null(i, j, k) = 0.0d0
		end if
		if ((FOV_yLower(i, j, k) .gt. 0) .and. ((j + 1 .le. cctk_lsh(2) .and. (FOV_3(i, int(j + 1), k) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. (FOV_3(i, int(j + 2), k) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. (FOV_3(i, int(j + 3), k) .gt. 0)))) then
				k2Null(i, 4 - j, k) = 0.0d0
		end if
		if ((FOV_yUpper(i, j, k) .gt. 0) .and. ((j - 1 .ge. 1 .and. (FOV_3(i, int(j - 1), k) .gt. 0)) .or. (j - 2 .ge. 1 .and. (FOV_3(i, int(j - 2), k) .gt. 0)) .or. (j - 3 .ge. 1 .and. (FOV_3(i, int(j - 3), k) .gt. 0)))) then
			k2Null(i, j, k) = 0.0d0
		end if
		if ((FOV_zLower(i, j, k) .gt. 0) .and. ((k + 1 .le. cctk_lsh(3) .and. (FOV_3(i, j, int(k + 1)) .gt. 0)) .or. (k + 2 .le. cctk_lsh(3) .and. (FOV_3(i, j, int(k + 2)) .gt. 0)) .or. (k + 3 .le. cctk_lsh(3) .and. (FOV_3(i, j, int(k + 3)) .gt. 0)))) then
				k2Null(i, j, 4 - k) = 0.0d0
		end if
		if ((FOV_zUpper(i, j, k) .gt. 0) .and. ((k - 1 .ge. 1 .and. (FOV_3(i, j, int(k - 1)) .gt. 0)) .or. (k - 2 .ge. 1 .and. (FOV_3(i, j, int(k - 2)) .gt. 0)) .or. (k - 3 .ge. 1 .and. (FOV_3(i, j, int(k - 3)) .gt. 0)))) then
			k2Null(i, j, k) = 0.0d0
		end if
		if (FOV_3(i, j, k) .gt. 0) then
			if ((d_i_rho(i, j, k) .ne. 0 .or. d_j_rho(i, j, k) .ne. 0 .or. d_k_rho(i, j, k) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_rho, k2rho, extrapolatedsubrhosupi, i, d_j_rho, k2rho, extrapolatedsubrhosupj, j, d_k_rho, k2rho, extrapolatedsubrhosupk, k, k2rho, FOV_1)
			end if
			if ((d_i_mx(i, j, k) .ne. 0 .or. d_j_mx(i, j, k) .ne. 0 .or. d_k_mx(i, j, k) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_mx, k2mx, extrapolatedsubmxsupi, i, d_j_mx, k2mx, extrapolatedsubmxsupj, j, d_k_mx, k2mx, extrapolatedsubmxsupk, k, k2mx, FOV_1)
			end if
			if ((d_i_my(i, j, k) .ne. 0 .or. d_j_my(i, j, k) .ne. 0 .or. d_k_my(i, j, k) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_my, k2my, extrapolatedsubmysupi, i, d_j_my, k2my, extrapolatedsubmysupj, j, d_k_my, k2my, extrapolatedsubmysupk, k, k2my, FOV_1)
			end if
			if ((d_i_mz(i, j, k) .ne. 0 .or. d_j_mz(i, j, k) .ne. 0 .or. d_k_mz(i, j, k) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_mz, k2mz, extrapolatedsubmzsupi, i, d_j_mz, k2mz, extrapolatedsubmzsupj, j, d_k_mz, k2mz, extrapolatedsubmzsupk, k, k2mz, FOV_1)
			end if
			if ((d_i_fx(i, j, k) .ne. 0 .or. d_j_fx(i, j, k) .ne. 0 .or. d_k_fx(i, j, k) .ne. 0)) then
				if (FOV_1(int(i - d_i_fx(i, j, k)), int(j - d_j_fx(i, j, k)), int(k - d_k_fx(i, j, k))) .gt. 0) then
					extrapolatedsubfxsupi(i, j, k) = pfx
					extrapolatedsubfxsupj(i, j, k) = pfx
					extrapolatedsubfxsupk(i, j, k) = pfx
					if (((d_i_fx(i, j, k) .ne. 0.0d0) .and. (d_j_fx(i, j, k) .ne. 0.0d0)) .and. (d_k_fx(i, j, k) .ne. 0.0d0)) then
					k2fx(i, j, k) = ((extrapolatedsubfxsupi(i, j, k) + extrapolatedsubfxsupj(i, j, k)) + extrapolatedsubfxsupk(i, j, k)) / 3.0d0
					else
					if (((d_i_fx(i, j, k) .ne. 0.0d0) .and. (d_j_fx(i, j, k) .ne. 0.0d0)) .and. (d_k_fx(i, j, k) .eq. 0.0d0)) then
					k2fx(i, j, k) = (extrapolatedsubfxsupi(i, j, k) + extrapolatedsubfxsupj(i, j, k)) / 2.0d0
					extrapolatedsubfxsupk(i, j, k) = k2fx(i, j, k)
					else
					if (((d_i_fx(i, j, k) .ne. 0.0d0) .and. (d_j_fx(i, j, k) .eq. 0.0d0)) .and. (d_k_fx(i, j, k) .ne. 0.0d0)) then
					k2fx(i, j, k) = (extrapolatedsubfxsupi(i, j, k) + extrapolatedsubfxsupk(i, j, k)) / 2.0d0
					extrapolatedsubfxsupj(i, j, k) = k2fx(i, j, k)
					else
					if (((d_i_fx(i, j, k) .ne. 0.0d0) .and. (d_j_fx(i, j, k) .eq. 0.0d0)) .and. (d_k_fx(i, j, k) .eq. 0.0d0)) then
					k2fx(i, j, k) = extrapolatedsubfxsupi(i, j, k)
					extrapolatedsubfxsupj(i, j, k) = k2fx(i, j, k)
					extrapolatedsubfxsupk(i, j, k) = k2fx(i, j, k)
					else
					if (((d_i_fx(i, j, k) .eq. 0.0d0) .and. (d_j_fx(i, j, k) .ne. 0.0d0)) .and. (d_k_fx(i, j, k) .ne. 0.0d0)) then
					k2fx(i, j, k) = (extrapolatedsubfxsupj(i, j, k) + extrapolatedsubfxsupk(i, j, k)) / 2.0d0
					extrapolatedsubfxsupi(i, j, k) = k2fx(i, j, k)
					else
					if (((d_i_fx(i, j, k) .eq. 0.0d0) .and. (d_j_fx(i, j, k) .ne. 0.0d0)) .and. (d_k_fx(i, j, k) .eq. 0.0d0)) then
					k2fx(i, j, k) = extrapolatedsubfxsupj(i, j, k)
					extrapolatedsubfxsupi(i, j, k) = k2fx(i, j, k)
					extrapolatedsubfxsupk(i, j, k) = k2fx(i, j, k)
					else
					if (((d_i_fx(i, j, k) .eq. 0.0d0) .and. (d_j_fx(i, j, k) .eq. 0.0d0)) .and. (d_k_fx(i, j, k) .ne. 0.0d0)) then
					k2fx(i, j, k) = extrapolatedsubfxsupk(i, j, k)
					extrapolatedsubfxsupi(i, j, k) = k2fx(i, j, k)
					extrapolatedsubfxsupj(i, j, k) = k2fx(i, j, k)
					end if
					end if
					end if
					end if
					end if
					end if
					end if
				end if
			end if
			if ((d_i_fy(i, j, k) .ne. 0 .or. d_j_fy(i, j, k) .ne. 0 .or. d_k_fy(i, j, k) .ne. 0)) then
				if (FOV_1(int(i - d_i_fy(i, j, k)), int(j - d_j_fy(i, j, k)), int(k - d_k_fy(i, j, k))) .gt. 0) then
					extrapolatedsubfysupi(i, j, k) = pfy
					extrapolatedsubfysupj(i, j, k) = pfy
					extrapolatedsubfysupk(i, j, k) = pfy
					if (((d_i_fy(i, j, k) .ne. 0.0d0) .and. (d_j_fy(i, j, k) .ne. 0.0d0)) .and. (d_k_fy(i, j, k) .ne. 0.0d0)) then
					k2fy(i, j, k) = ((extrapolatedsubfysupi(i, j, k) + extrapolatedsubfysupj(i, j, k)) + extrapolatedsubfysupk(i, j, k)) / 3.0d0
					else
					if (((d_i_fy(i, j, k) .ne. 0.0d0) .and. (d_j_fy(i, j, k) .ne. 0.0d0)) .and. (d_k_fy(i, j, k) .eq. 0.0d0)) then
					k2fy(i, j, k) = (extrapolatedsubfysupi(i, j, k) + extrapolatedsubfysupj(i, j, k)) / 2.0d0
					extrapolatedsubfysupk(i, j, k) = k2fy(i, j, k)
					else
					if (((d_i_fy(i, j, k) .ne. 0.0d0) .and. (d_j_fy(i, j, k) .eq. 0.0d0)) .and. (d_k_fy(i, j, k) .ne. 0.0d0)) then
					k2fy(i, j, k) = (extrapolatedsubfysupi(i, j, k) + extrapolatedsubfysupk(i, j, k)) / 2.0d0
					extrapolatedsubfysupj(i, j, k) = k2fy(i, j, k)
					else
					if (((d_i_fy(i, j, k) .ne. 0.0d0) .and. (d_j_fy(i, j, k) .eq. 0.0d0)) .and. (d_k_fy(i, j, k) .eq. 0.0d0)) then
					k2fy(i, j, k) = extrapolatedsubfysupi(i, j, k)
					extrapolatedsubfysupj(i, j, k) = k2fy(i, j, k)
					extrapolatedsubfysupk(i, j, k) = k2fy(i, j, k)
					else
					if (((d_i_fy(i, j, k) .eq. 0.0d0) .and. (d_j_fy(i, j, k) .ne. 0.0d0)) .and. (d_k_fy(i, j, k) .ne. 0.0d0)) then
					k2fy(i, j, k) = (extrapolatedsubfysupj(i, j, k) + extrapolatedsubfysupk(i, j, k)) / 2.0d0
					extrapolatedsubfysupi(i, j, k) = k2fy(i, j, k)
					else
					if (((d_i_fy(i, j, k) .eq. 0.0d0) .and. (d_j_fy(i, j, k) .ne. 0.0d0)) .and. (d_k_fy(i, j, k) .eq. 0.0d0)) then
					k2fy(i, j, k) = extrapolatedsubfysupj(i, j, k)
					extrapolatedsubfysupi(i, j, k) = k2fy(i, j, k)
					extrapolatedsubfysupk(i, j, k) = k2fy(i, j, k)
					else
					if (((d_i_fy(i, j, k) .eq. 0.0d0) .and. (d_j_fy(i, j, k) .eq. 0.0d0)) .and. (d_k_fy(i, j, k) .ne. 0.0d0)) then
					k2fy(i, j, k) = extrapolatedsubfysupk(i, j, k)
					extrapolatedsubfysupi(i, j, k) = k2fy(i, j, k)
					extrapolatedsubfysupj(i, j, k) = k2fy(i, j, k)
					end if
					end if
					end if
					end if
					end if
					end if
					end if
				end if
			end if
			if ((d_i_fz(i, j, k) .ne. 0 .or. d_j_fz(i, j, k) .ne. 0 .or. d_k_fz(i, j, k) .ne. 0)) then
				if (FOV_1(int(i - d_i_fz(i, j, k)), int(j - d_j_fz(i, j, k)), int(k - d_k_fz(i, j, k))) .gt. 0) then
					extrapolatedsubfzsupi(i, j, k) = pfz
					extrapolatedsubfzsupj(i, j, k) = pfz
					extrapolatedsubfzsupk(i, j, k) = pfz
					if (((d_i_fz(i, j, k) .ne. 0.0d0) .and. (d_j_fz(i, j, k) .ne. 0.0d0)) .and. (d_k_fz(i, j, k) .ne. 0.0d0)) then
					k2fz(i, j, k) = ((extrapolatedsubfzsupi(i, j, k) + extrapolatedsubfzsupj(i, j, k)) + extrapolatedsubfzsupk(i, j, k)) / 3.0d0
					else
					if (((d_i_fz(i, j, k) .ne. 0.0d0) .and. (d_j_fz(i, j, k) .ne. 0.0d0)) .and. (d_k_fz(i, j, k) .eq. 0.0d0)) then
					k2fz(i, j, k) = (extrapolatedsubfzsupi(i, j, k) + extrapolatedsubfzsupj(i, j, k)) / 2.0d0
					extrapolatedsubfzsupk(i, j, k) = k2fz(i, j, k)
					else
					if (((d_i_fz(i, j, k) .ne. 0.0d0) .and. (d_j_fz(i, j, k) .eq. 0.0d0)) .and. (d_k_fz(i, j, k) .ne. 0.0d0)) then
					k2fz(i, j, k) = (extrapolatedsubfzsupi(i, j, k) + extrapolatedsubfzsupk(i, j, k)) / 2.0d0
					extrapolatedsubfzsupj(i, j, k) = k2fz(i, j, k)
					else
					if (((d_i_fz(i, j, k) .ne. 0.0d0) .and. (d_j_fz(i, j, k) .eq. 0.0d0)) .and. (d_k_fz(i, j, k) .eq. 0.0d0)) then
					k2fz(i, j, k) = extrapolatedsubfzsupi(i, j, k)
					extrapolatedsubfzsupj(i, j, k) = k2fz(i, j, k)
					extrapolatedsubfzsupk(i, j, k) = k2fz(i, j, k)
					else
					if (((d_i_fz(i, j, k) .eq. 0.0d0) .and. (d_j_fz(i, j, k) .ne. 0.0d0)) .and. (d_k_fz(i, j, k) .ne. 0.0d0)) then
					k2fz(i, j, k) = (extrapolatedsubfzsupj(i, j, k) + extrapolatedsubfzsupk(i, j, k)) / 2.0d0
					extrapolatedsubfzsupi(i, j, k) = k2fz(i, j, k)
					else
					if (((d_i_fz(i, j, k) .eq. 0.0d0) .and. (d_j_fz(i, j, k) .ne. 0.0d0)) .and. (d_k_fz(i, j, k) .eq. 0.0d0)) then
					k2fz(i, j, k) = extrapolatedsubfzsupj(i, j, k)
					extrapolatedsubfzsupi(i, j, k) = k2fz(i, j, k)
					extrapolatedsubfzsupk(i, j, k) = k2fz(i, j, k)
					else
					if (((d_i_fz(i, j, k) .eq. 0.0d0) .and. (d_j_fz(i, j, k) .eq. 0.0d0)) .and. (d_k_fz(i, j, k) .ne. 0.0d0)) then
					k2fz(i, j, k) = extrapolatedsubfzsupk(i, j, k)
					extrapolatedsubfzsupi(i, j, k) = k2fz(i, j, k)
					extrapolatedsubfzsupj(i, j, k) = k2fz(i, j, k)
					end if
					end if
					end if
					end if
					end if
					end if
					end if
				end if
			end if
			if ((d_i_p(i, j, k) .ne. 0 .or. d_j_p(i, j, k) .ne. 0 .or. d_k_p(i, j, k) .ne. 0)) then
				if (FOV_1(int(i - d_i_p(i, j, k)), int(j - d_j_p(i, j, k)), int(k - d_k_p(i, j, k))) .gt. 0) then
					extrapolatedsubpsupi(i, j, k) = (Paux + ((extrapolatedsubrhosupi(i, j, k) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					extrapolatedsubpsupj(i, j, k) = (Paux + ((extrapolatedsubrhosupj(i, j, k) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					extrapolatedsubpsupk(i, j, k) = (Paux + ((extrapolatedsubrhosupk(i, j, k) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					if (((d_i_p(i, j, k) .ne. 0.0d0) .and. (d_j_p(i, j, k) .ne. 0.0d0)) .and. (d_k_p(i, j, k) .ne. 0.0d0)) then
					k2p(i, j, k) = ((extrapolatedsubpsupi(i, j, k) + extrapolatedsubpsupj(i, j, k)) + extrapolatedsubpsupk(i, j, k)) / 3.0d0
					else
					if (((d_i_p(i, j, k) .ne. 0.0d0) .and. (d_j_p(i, j, k) .ne. 0.0d0)) .and. (d_k_p(i, j, k) .eq. 0.0d0)) then
					k2p(i, j, k) = (extrapolatedsubpsupi(i, j, k) + extrapolatedsubpsupj(i, j, k)) / 2.0d0
					extrapolatedsubpsupk(i, j, k) = k2p(i, j, k)
					else
					if (((d_i_p(i, j, k) .ne. 0.0d0) .and. (d_j_p(i, j, k) .eq. 0.0d0)) .and. (d_k_p(i, j, k) .ne. 0.0d0)) then
					k2p(i, j, k) = (extrapolatedsubpsupi(i, j, k) + extrapolatedsubpsupk(i, j, k)) / 2.0d0
					extrapolatedsubpsupj(i, j, k) = k2p(i, j, k)
					else
					if (((d_i_p(i, j, k) .ne. 0.0d0) .and. (d_j_p(i, j, k) .eq. 0.0d0)) .and. (d_k_p(i, j, k) .eq. 0.0d0)) then
					k2p(i, j, k) = extrapolatedsubpsupi(i, j, k)
					extrapolatedsubpsupj(i, j, k) = k2p(i, j, k)
					extrapolatedsubpsupk(i, j, k) = k2p(i, j, k)
					else
					if (((d_i_p(i, j, k) .eq. 0.0d0) .and. (d_j_p(i, j, k) .ne. 0.0d0)) .and. (d_k_p(i, j, k) .ne. 0.0d0)) then
					k2p(i, j, k) = (extrapolatedsubpsupj(i, j, k) + extrapolatedsubpsupk(i, j, k)) / 2.0d0
					extrapolatedsubpsupi(i, j, k) = k2p(i, j, k)
					else
					if (((d_i_p(i, j, k) .eq. 0.0d0) .and. (d_j_p(i, j, k) .ne. 0.0d0)) .and. (d_k_p(i, j, k) .eq. 0.0d0)) then
					k2p(i, j, k) = extrapolatedsubpsupj(i, j, k)
					extrapolatedsubpsupi(i, j, k) = k2p(i, j, k)
					extrapolatedsubpsupk(i, j, k) = k2p(i, j, k)
					else
					if (((d_i_p(i, j, k) .eq. 0.0d0) .and. (d_j_p(i, j, k) .eq. 0.0d0)) .and. (d_k_p(i, j, k) .ne. 0.0d0)) then
					k2p(i, j, k) = extrapolatedsubpsupk(i, j, k)
					extrapolatedsubpsupi(i, j, k) = k2p(i, j, k)
					extrapolatedsubpsupj(i, j, k) = k2p(i, j, k)
					end if
					end if
					end if
					end if
					end if
					end if
					end if
				end if
			end if
		end if
!					Boundaries
		if ((FOV_xLower(i, j, k) .gt. 0) .and. ((i + 1 .le. cctk_lsh(1) .and. (FOV_1(int(i + 1), j, k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. (FOV_1(int(i + 2), j, k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. (FOV_1(int(i + 3), j, k) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 1), k) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 1), k) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 2), k) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 2), k) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 3), k) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 3), k) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(int(i + 1), j, int(k + 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. k - 1 .ge. 1 .and. (FOV_1(int(i + 1), j, int(k - 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(int(i + 1), j, int(k + 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. k - 2 .ge. 1 .and. (FOV_1(int(i + 1), j, int(k - 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(int(i + 1), j, int(k + 3)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. k - 3 .ge. 1 .and. (FOV_1(int(i + 1), j, int(k - 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 1), k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 1), k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 2), k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 2), k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 3), k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 3), k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(int(i + 2), j, int(k + 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. k - 1 .ge. 1 .and. (FOV_1(int(i + 2), j, int(k - 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(int(i + 2), j, int(k + 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. k - 2 .ge. 1 .and. (FOV_1(int(i + 2), j, int(k - 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(int(i + 2), j, int(k + 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. k - 3 .ge. 1 .and. (FOV_1(int(i + 2), j, int(k - 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 1), k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 1), k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 2), k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 2), k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 3), k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 3), k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(int(i + 3), j, int(k + 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. k - 1 .ge. 1 .and. (FOV_1(int(i + 3), j, int(k - 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(int(i + 3), j, int(k + 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. k - 2 .ge. 1 .and. (FOV_1(int(i + 3), j, int(k - 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(int(i + 3), j, int(k + 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. k - 3 .ge. 1 .and. (FOV_1(int(i + 3), j, int(k - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j, k) .ne. 0 .or. d_j_rho(i, j, k) .ne. 0 .or. d_k_rho(i, j, k) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_rho, k2rho, extrapolatedsubrhosupi, i, d_j_rho, k2rho, extrapolatedsubrhosupj, j, d_k_rho, k2rho, extrapolatedsubrhosupk, k, k2rho, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j, k) .ne. 0 .or. d_j_mx(i, j, k) .ne. 0 .or. d_k_mx(i, j, k) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_mx, k2mx, extrapolatedsubmxsupi, i, d_j_mx, k2mx, extrapolatedsubmxsupj, j, d_k_mx, k2mx, extrapolatedsubmxsupk, k, k2mx, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j, k) .ne. 0 .or. d_j_my(i, j, k) .ne. 0 .or. d_k_my(i, j, k) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_my, k2my, extrapolatedsubmysupi, i, d_j_my, k2my, extrapolatedsubmysupj, j, d_k_my, k2my, extrapolatedsubmysupk, k, k2my, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mz(i, j, k) .ne. 0 .or. d_j_mz(i, j, k) .ne. 0 .or. d_k_mz(i, j, k) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_mz, k2mz, extrapolatedsubmzsupi, i, d_j_mz, k2mz, extrapolatedsubmzsupj, j, d_k_mz, k2mz, extrapolatedsubmzsupk, k, k2mz, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_p(i, j, k) .ne. 0 .or. d_j_p(i, j, k) .ne. 0 .or. d_k_p(i, j, k) .ne. 0)) then
				if (FOV_1(int(i - d_i_p(i, j, k)), int(j - d_j_p(i, j, k)), int(k - d_k_p(i, j, k))) .gt. 0) then
					extrapolatedsubpsupi(i, j, k) = (Paux + ((extrapolatedsubrhosupi(i, j, k) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					extrapolatedsubpsupj(i, j, k) = (Paux + ((extrapolatedsubrhosupj(i, j, k) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					extrapolatedsubpsupk(i, j, k) = (Paux + ((extrapolatedsubrhosupk(i, j, k) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					if (((d_i_p(i, j, k) .ne. 0.0d0) .and. (d_j_p(i, j, k) .ne. 0.0d0)) .and. (d_k_p(i, j, k) .ne. 0.0d0)) then
					k2p(i, j, k) = ((extrapolatedsubpsupi(i, j, k) + extrapolatedsubpsupj(i, j, k)) + extrapolatedsubpsupk(i, j, k)) / 3.0d0
					else
					if (((d_i_p(i, j, k) .ne. 0.0d0) .and. (d_j_p(i, j, k) .ne. 0.0d0)) .and. (d_k_p(i, j, k) .eq. 0.0d0)) then
					k2p(i, j, k) = (extrapolatedsubpsupi(i, j, k) + extrapolatedsubpsupj(i, j, k)) / 2.0d0
					extrapolatedsubpsupk(i, j, k) = k2p(i, j, k)
					else
					if (((d_i_p(i, j, k) .ne. 0.0d0) .and. (d_j_p(i, j, k) .eq. 0.0d0)) .and. (d_k_p(i, j, k) .ne. 0.0d0)) then
					k2p(i, j, k) = (extrapolatedsubpsupi(i, j, k) + extrapolatedsubpsupk(i, j, k)) / 2.0d0
					extrapolatedsubpsupj(i, j, k) = k2p(i, j, k)
					else
					if (((d_i_p(i, j, k) .ne. 0.0d0) .and. (d_j_p(i, j, k) .eq. 0.0d0)) .and. (d_k_p(i, j, k) .eq. 0.0d0)) then
					k2p(i, j, k) = extrapolatedsubpsupi(i, j, k)
					extrapolatedsubpsupj(i, j, k) = k2p(i, j, k)
					extrapolatedsubpsupk(i, j, k) = k2p(i, j, k)
					else
					if (((d_i_p(i, j, k) .eq. 0.0d0) .and. (d_j_p(i, j, k) .ne. 0.0d0)) .and. (d_k_p(i, j, k) .ne. 0.0d0)) then
					k2p(i, j, k) = (extrapolatedsubpsupj(i, j, k) + extrapolatedsubpsupk(i, j, k)) / 2.0d0
					extrapolatedsubpsupi(i, j, k) = k2p(i, j, k)
					else
					if (((d_i_p(i, j, k) .eq. 0.0d0) .and. (d_j_p(i, j, k) .ne. 0.0d0)) .and. (d_k_p(i, j, k) .eq. 0.0d0)) then
					k2p(i, j, k) = extrapolatedsubpsupj(i, j, k)
					extrapolatedsubpsupi(i, j, k) = k2p(i, j, k)
					extrapolatedsubpsupk(i, j, k) = k2p(i, j, k)
					else
					if (((d_i_p(i, j, k) .eq. 0.0d0) .and. (d_j_p(i, j, k) .eq. 0.0d0)) .and. (d_k_p(i, j, k) .ne. 0.0d0)) then
					k2p(i, j, k) = extrapolatedsubpsupk(i, j, k)
					extrapolatedsubpsupi(i, j, k) = k2p(i, j, k)
					extrapolatedsubpsupj(i, j, k) = k2p(i, j, k)
					end if
					end if
					end if
					end if
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fx(i, j, k) .ne. 0 .or. d_j_fx(i, j, k) .ne. 0 .or. d_k_fx(i, j, k) .ne. 0)) then
				if (FOV_1(int(i - d_i_fx(i, j, k)), int(j - d_j_fx(i, j, k)), int(k - d_k_fx(i, j, k))) .gt. 0) then
					extrapolatedsubfxsupi(i, j, k) = pfx
					extrapolatedsubfxsupj(i, j, k) = pfx
					extrapolatedsubfxsupk(i, j, k) = pfx
					if (((d_i_fx(i, j, k) .ne. 0.0d0) .and. (d_j_fx(i, j, k) .ne. 0.0d0)) .and. (d_k_fx(i, j, k) .ne. 0.0d0)) then
					k2fx(i, j, k) = ((extrapolatedsubfxsupi(i, j, k) + extrapolatedsubfxsupj(i, j, k)) + extrapolatedsubfxsupk(i, j, k)) / 3.0d0
					else
					if (((d_i_fx(i, j, k) .ne. 0.0d0) .and. (d_j_fx(i, j, k) .ne. 0.0d0)) .and. (d_k_fx(i, j, k) .eq. 0.0d0)) then
					k2fx(i, j, k) = (extrapolatedsubfxsupi(i, j, k) + extrapolatedsubfxsupj(i, j, k)) / 2.0d0
					extrapolatedsubfxsupk(i, j, k) = k2fx(i, j, k)
					else
					if (((d_i_fx(i, j, k) .ne. 0.0d0) .and. (d_j_fx(i, j, k) .eq. 0.0d0)) .and. (d_k_fx(i, j, k) .ne. 0.0d0)) then
					k2fx(i, j, k) = (extrapolatedsubfxsupi(i, j, k) + extrapolatedsubfxsupk(i, j, k)) / 2.0d0
					extrapolatedsubfxsupj(i, j, k) = k2fx(i, j, k)
					else
					if (((d_i_fx(i, j, k) .ne. 0.0d0) .and. (d_j_fx(i, j, k) .eq. 0.0d0)) .and. (d_k_fx(i, j, k) .eq. 0.0d0)) then
					k2fx(i, j, k) = extrapolatedsubfxsupi(i, j, k)
					extrapolatedsubfxsupj(i, j, k) = k2fx(i, j, k)
					extrapolatedsubfxsupk(i, j, k) = k2fx(i, j, k)
					else
					if (((d_i_fx(i, j, k) .eq. 0.0d0) .and. (d_j_fx(i, j, k) .ne. 0.0d0)) .and. (d_k_fx(i, j, k) .ne. 0.0d0)) then
					k2fx(i, j, k) = (extrapolatedsubfxsupj(i, j, k) + extrapolatedsubfxsupk(i, j, k)) / 2.0d0
					extrapolatedsubfxsupi(i, j, k) = k2fx(i, j, k)
					else
					if (((d_i_fx(i, j, k) .eq. 0.0d0) .and. (d_j_fx(i, j, k) .ne. 0.0d0)) .and. (d_k_fx(i, j, k) .eq. 0.0d0)) then
					k2fx(i, j, k) = extrapolatedsubfxsupj(i, j, k)
					extrapolatedsubfxsupi(i, j, k) = k2fx(i, j, k)
					extrapolatedsubfxsupk(i, j, k) = k2fx(i, j, k)
					else
					if (((d_i_fx(i, j, k) .eq. 0.0d0) .and. (d_j_fx(i, j, k) .eq. 0.0d0)) .and. (d_k_fx(i, j, k) .ne. 0.0d0)) then
					k2fx(i, j, k) = extrapolatedsubfxsupk(i, j, k)
					extrapolatedsubfxsupi(i, j, k) = k2fx(i, j, k)
					extrapolatedsubfxsupj(i, j, k) = k2fx(i, j, k)
					end if
					end if
					end if
					end if
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fy(i, j, k) .ne. 0 .or. d_j_fy(i, j, k) .ne. 0 .or. d_k_fy(i, j, k) .ne. 0)) then
				if (FOV_1(int(i - d_i_fy(i, j, k)), int(j - d_j_fy(i, j, k)), int(k - d_k_fy(i, j, k))) .gt. 0) then
					extrapolatedsubfysupi(i, j, k) = pfy
					extrapolatedsubfysupj(i, j, k) = pfy
					extrapolatedsubfysupk(i, j, k) = pfy
					if (((d_i_fy(i, j, k) .ne. 0.0d0) .and. (d_j_fy(i, j, k) .ne. 0.0d0)) .and. (d_k_fy(i, j, k) .ne. 0.0d0)) then
					k2fy(i, j, k) = ((extrapolatedsubfysupi(i, j, k) + extrapolatedsubfysupj(i, j, k)) + extrapolatedsubfysupk(i, j, k)) / 3.0d0
					else
					if (((d_i_fy(i, j, k) .ne. 0.0d0) .and. (d_j_fy(i, j, k) .ne. 0.0d0)) .and. (d_k_fy(i, j, k) .eq. 0.0d0)) then
					k2fy(i, j, k) = (extrapolatedsubfysupi(i, j, k) + extrapolatedsubfysupj(i, j, k)) / 2.0d0
					extrapolatedsubfysupk(i, j, k) = k2fy(i, j, k)
					else
					if (((d_i_fy(i, j, k) .ne. 0.0d0) .and. (d_j_fy(i, j, k) .eq. 0.0d0)) .and. (d_k_fy(i, j, k) .ne. 0.0d0)) then
					k2fy(i, j, k) = (extrapolatedsubfysupi(i, j, k) + extrapolatedsubfysupk(i, j, k)) / 2.0d0
					extrapolatedsubfysupj(i, j, k) = k2fy(i, j, k)
					else
					if (((d_i_fy(i, j, k) .ne. 0.0d0) .and. (d_j_fy(i, j, k) .eq. 0.0d0)) .and. (d_k_fy(i, j, k) .eq. 0.0d0)) then
					k2fy(i, j, k) = extrapolatedsubfysupi(i, j, k)
					extrapolatedsubfysupj(i, j, k) = k2fy(i, j, k)
					extrapolatedsubfysupk(i, j, k) = k2fy(i, j, k)
					else
					if (((d_i_fy(i, j, k) .eq. 0.0d0) .and. (d_j_fy(i, j, k) .ne. 0.0d0)) .and. (d_k_fy(i, j, k) .ne. 0.0d0)) then
					k2fy(i, j, k) = (extrapolatedsubfysupj(i, j, k) + extrapolatedsubfysupk(i, j, k)) / 2.0d0
					extrapolatedsubfysupi(i, j, k) = k2fy(i, j, k)
					else
					if (((d_i_fy(i, j, k) .eq. 0.0d0) .and. (d_j_fy(i, j, k) .ne. 0.0d0)) .and. (d_k_fy(i, j, k) .eq. 0.0d0)) then
					k2fy(i, j, k) = extrapolatedsubfysupj(i, j, k)
					extrapolatedsubfysupi(i, j, k) = k2fy(i, j, k)
					extrapolatedsubfysupk(i, j, k) = k2fy(i, j, k)
					else
					if (((d_i_fy(i, j, k) .eq. 0.0d0) .and. (d_j_fy(i, j, k) .eq. 0.0d0)) .and. (d_k_fy(i, j, k) .ne. 0.0d0)) then
					k2fy(i, j, k) = extrapolatedsubfysupk(i, j, k)
					extrapolatedsubfysupi(i, j, k) = k2fy(i, j, k)
					extrapolatedsubfysupj(i, j, k) = k2fy(i, j, k)
					end if
					end if
					end if
					end if
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fz(i, j, k) .ne. 0 .or. d_j_fz(i, j, k) .ne. 0 .or. d_k_fz(i, j, k) .ne. 0)) then
				if (FOV_1(int(i - d_i_fz(i, j, k)), int(j - d_j_fz(i, j, k)), int(k - d_k_fz(i, j, k))) .gt. 0) then
					extrapolatedsubfzsupi(i, j, k) = pfz
					extrapolatedsubfzsupj(i, j, k) = pfz
					extrapolatedsubfzsupk(i, j, k) = pfz
					if (((d_i_fz(i, j, k) .ne. 0.0d0) .and. (d_j_fz(i, j, k) .ne. 0.0d0)) .and. (d_k_fz(i, j, k) .ne. 0.0d0)) then
					k2fz(i, j, k) = ((extrapolatedsubfzsupi(i, j, k) + extrapolatedsubfzsupj(i, j, k)) + extrapolatedsubfzsupk(i, j, k)) / 3.0d0
					else
					if (((d_i_fz(i, j, k) .ne. 0.0d0) .and. (d_j_fz(i, j, k) .ne. 0.0d0)) .and. (d_k_fz(i, j, k) .eq. 0.0d0)) then
					k2fz(i, j, k) = (extrapolatedsubfzsupi(i, j, k) + extrapolatedsubfzsupj(i, j, k)) / 2.0d0
					extrapolatedsubfzsupk(i, j, k) = k2fz(i, j, k)
					else
					if (((d_i_fz(i, j, k) .ne. 0.0d0) .and. (d_j_fz(i, j, k) .eq. 0.0d0)) .and. (d_k_fz(i, j, k) .ne. 0.0d0)) then
					k2fz(i, j, k) = (extrapolatedsubfzsupi(i, j, k) + extrapolatedsubfzsupk(i, j, k)) / 2.0d0
					extrapolatedsubfzsupj(i, j, k) = k2fz(i, j, k)
					else
					if (((d_i_fz(i, j, k) .ne. 0.0d0) .and. (d_j_fz(i, j, k) .eq. 0.0d0)) .and. (d_k_fz(i, j, k) .eq. 0.0d0)) then
					k2fz(i, j, k) = extrapolatedsubfzsupi(i, j, k)
					extrapolatedsubfzsupj(i, j, k) = k2fz(i, j, k)
					extrapolatedsubfzsupk(i, j, k) = k2fz(i, j, k)
					else
					if (((d_i_fz(i, j, k) .eq. 0.0d0) .and. (d_j_fz(i, j, k) .ne. 0.0d0)) .and. (d_k_fz(i, j, k) .ne. 0.0d0)) then
					k2fz(i, j, k) = (extrapolatedsubfzsupj(i, j, k) + extrapolatedsubfzsupk(i, j, k)) / 2.0d0
					extrapolatedsubfzsupi(i, j, k) = k2fz(i, j, k)
					else
					if (((d_i_fz(i, j, k) .eq. 0.0d0) .and. (d_j_fz(i, j, k) .ne. 0.0d0)) .and. (d_k_fz(i, j, k) .eq. 0.0d0)) then
					k2fz(i, j, k) = extrapolatedsubfzsupj(i, j, k)
					extrapolatedsubfzsupi(i, j, k) = k2fz(i, j, k)
					extrapolatedsubfzsupk(i, j, k) = k2fz(i, j, k)
					else
					if (((d_i_fz(i, j, k) .eq. 0.0d0) .and. (d_j_fz(i, j, k) .eq. 0.0d0)) .and. (d_k_fz(i, j, k) .ne. 0.0d0)) then
					k2fz(i, j, k) = extrapolatedsubfzsupk(i, j, k)
					extrapolatedsubfzsupi(i, j, k) = k2fz(i, j, k)
					extrapolatedsubfzsupj(i, j, k) = k2fz(i, j, k)
					end if
					end if
					end if
					end if
					end if
					end if
					end if
				end if
			end if
		end if
		if ((FOV_xUpper(i, j, k) .gt. 0) .and. ((i - 1 .ge. 1 .and. (FOV_1(int(i - 1), j, k) .gt. 0)) .or. (i - 2 .ge. 1 .and. (FOV_1(int(i - 2), j, k) .gt. 0)) .or. (i - 3 .ge. 1 .and. (FOV_1(int(i - 3), j, k) .gt. 0)) .or. ((i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 1), k) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 1), k) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 2), k) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 2), k) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 3), k) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 3), k) .gt. 0)) .or. (i - 1 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(int(i - 1), j, int(k + 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. k - 1 .ge. 1 .and. (FOV_1(int(i - 1), j, int(k - 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(int(i - 1), j, int(k + 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. k - 2 .ge. 1 .and. (FOV_1(int(i - 1), j, int(k - 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(int(i - 1), j, int(k + 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. k - 3 .ge. 1 .and. (FOV_1(int(i - 1), j, int(k - 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 1), k) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 1), k) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 2), k) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 2), k) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 3), k) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 3), k) .gt. 0)) .or. (i - 2 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(int(i - 2), j, int(k + 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. k - 1 .ge. 1 .and. (FOV_1(int(i - 2), j, int(k - 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(int(i - 2), j, int(k + 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. k - 2 .ge. 1 .and. (FOV_1(int(i - 2), j, int(k - 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(int(i - 2), j, int(k + 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. k - 3 .ge. 1 .and. (FOV_1(int(i - 2), j, int(k - 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 1), k) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 1), k) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 2), k) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 2), k) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 3), k) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 3), k) .gt. 0)) .or. (i - 3 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(int(i - 3), j, int(k + 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. k - 1 .ge. 1 .and. (FOV_1(int(i - 3), j, int(k - 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(int(i - 3), j, int(k + 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. k - 2 .ge. 1 .and. (FOV_1(int(i - 3), j, int(k - 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(int(i - 3), j, int(k + 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. k - 3 .ge. 1 .and. (FOV_1(int(i - 3), j, int(k - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j, k) .ne. 0 .or. d_j_rho(i, j, k) .ne. 0 .or. d_k_rho(i, j, k) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_rho, k2rho, extrapolatedsubrhosupi, i, d_j_rho, k2rho, extrapolatedsubrhosupj, j, d_k_rho, k2rho, extrapolatedsubrhosupk, k, k2rho, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j, k) .ne. 0 .or. d_j_mx(i, j, k) .ne. 0 .or. d_k_mx(i, j, k) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_mx, k2mx, extrapolatedsubmxsupi, i, d_j_mx, k2mx, extrapolatedsubmxsupj, j, d_k_mx, k2mx, extrapolatedsubmxsupk, k, k2mx, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j, k) .ne. 0 .or. d_j_my(i, j, k) .ne. 0 .or. d_k_my(i, j, k) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_my, k2my, extrapolatedsubmysupi, i, d_j_my, k2my, extrapolatedsubmysupj, j, d_k_my, k2my, extrapolatedsubmysupk, k, k2my, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mz(i, j, k) .ne. 0 .or. d_j_mz(i, j, k) .ne. 0 .or. d_k_mz(i, j, k) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_mz, k2mz, extrapolatedsubmzsupi, i, d_j_mz, k2mz, extrapolatedsubmzsupj, j, d_k_mz, k2mz, extrapolatedsubmzsupk, k, k2mz, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_p(i, j, k) .ne. 0 .or. d_j_p(i, j, k) .ne. 0 .or. d_k_p(i, j, k) .ne. 0)) then
				if (FOV_1(int(i - d_i_p(i, j, k)), int(j - d_j_p(i, j, k)), int(k - d_k_p(i, j, k))) .gt. 0) then
					extrapolatedsubpsupi(i, j, k) = (Paux + ((extrapolatedsubrhosupi(i, j, k) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					extrapolatedsubpsupj(i, j, k) = (Paux + ((extrapolatedsubrhosupj(i, j, k) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					extrapolatedsubpsupk(i, j, k) = (Paux + ((extrapolatedsubrhosupk(i, j, k) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					if (((d_i_p(i, j, k) .ne. 0.0d0) .and. (d_j_p(i, j, k) .ne. 0.0d0)) .and. (d_k_p(i, j, k) .ne. 0.0d0)) then
					k2p(i, j, k) = ((extrapolatedsubpsupi(i, j, k) + extrapolatedsubpsupj(i, j, k)) + extrapolatedsubpsupk(i, j, k)) / 3.0d0
					else
					if (((d_i_p(i, j, k) .ne. 0.0d0) .and. (d_j_p(i, j, k) .ne. 0.0d0)) .and. (d_k_p(i, j, k) .eq. 0.0d0)) then
					k2p(i, j, k) = (extrapolatedsubpsupi(i, j, k) + extrapolatedsubpsupj(i, j, k)) / 2.0d0
					extrapolatedsubpsupk(i, j, k) = k2p(i, j, k)
					else
					if (((d_i_p(i, j, k) .ne. 0.0d0) .and. (d_j_p(i, j, k) .eq. 0.0d0)) .and. (d_k_p(i, j, k) .ne. 0.0d0)) then
					k2p(i, j, k) = (extrapolatedsubpsupi(i, j, k) + extrapolatedsubpsupk(i, j, k)) / 2.0d0
					extrapolatedsubpsupj(i, j, k) = k2p(i, j, k)
					else
					if (((d_i_p(i, j, k) .ne. 0.0d0) .and. (d_j_p(i, j, k) .eq. 0.0d0)) .and. (d_k_p(i, j, k) .eq. 0.0d0)) then
					k2p(i, j, k) = extrapolatedsubpsupi(i, j, k)
					extrapolatedsubpsupj(i, j, k) = k2p(i, j, k)
					extrapolatedsubpsupk(i, j, k) = k2p(i, j, k)
					else
					if (((d_i_p(i, j, k) .eq. 0.0d0) .and. (d_j_p(i, j, k) .ne. 0.0d0)) .and. (d_k_p(i, j, k) .ne. 0.0d0)) then
					k2p(i, j, k) = (extrapolatedsubpsupj(i, j, k) + extrapolatedsubpsupk(i, j, k)) / 2.0d0
					extrapolatedsubpsupi(i, j, k) = k2p(i, j, k)
					else
					if (((d_i_p(i, j, k) .eq. 0.0d0) .and. (d_j_p(i, j, k) .ne. 0.0d0)) .and. (d_k_p(i, j, k) .eq. 0.0d0)) then
					k2p(i, j, k) = extrapolatedsubpsupj(i, j, k)
					extrapolatedsubpsupi(i, j, k) = k2p(i, j, k)
					extrapolatedsubpsupk(i, j, k) = k2p(i, j, k)
					else
					if (((d_i_p(i, j, k) .eq. 0.0d0) .and. (d_j_p(i, j, k) .eq. 0.0d0)) .and. (d_k_p(i, j, k) .ne. 0.0d0)) then
					k2p(i, j, k) = extrapolatedsubpsupk(i, j, k)
					extrapolatedsubpsupi(i, j, k) = k2p(i, j, k)
					extrapolatedsubpsupj(i, j, k) = k2p(i, j, k)
					end if
					end if
					end if
					end if
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fx(i, j, k) .ne. 0 .or. d_j_fx(i, j, k) .ne. 0 .or. d_k_fx(i, j, k) .ne. 0)) then
				if (FOV_1(int(i - d_i_fx(i, j, k)), int(j - d_j_fx(i, j, k)), int(k - d_k_fx(i, j, k))) .gt. 0) then
					extrapolatedsubfxsupi(i, j, k) = pfx
					extrapolatedsubfxsupj(i, j, k) = pfx
					extrapolatedsubfxsupk(i, j, k) = pfx
					if (((d_i_fx(i, j, k) .ne. 0.0d0) .and. (d_j_fx(i, j, k) .ne. 0.0d0)) .and. (d_k_fx(i, j, k) .ne. 0.0d0)) then
					k2fx(i, j, k) = ((extrapolatedsubfxsupi(i, j, k) + extrapolatedsubfxsupj(i, j, k)) + extrapolatedsubfxsupk(i, j, k)) / 3.0d0
					else
					if (((d_i_fx(i, j, k) .ne. 0.0d0) .and. (d_j_fx(i, j, k) .ne. 0.0d0)) .and. (d_k_fx(i, j, k) .eq. 0.0d0)) then
					k2fx(i, j, k) = (extrapolatedsubfxsupi(i, j, k) + extrapolatedsubfxsupj(i, j, k)) / 2.0d0
					extrapolatedsubfxsupk(i, j, k) = k2fx(i, j, k)
					else
					if (((d_i_fx(i, j, k) .ne. 0.0d0) .and. (d_j_fx(i, j, k) .eq. 0.0d0)) .and. (d_k_fx(i, j, k) .ne. 0.0d0)) then
					k2fx(i, j, k) = (extrapolatedsubfxsupi(i, j, k) + extrapolatedsubfxsupk(i, j, k)) / 2.0d0
					extrapolatedsubfxsupj(i, j, k) = k2fx(i, j, k)
					else
					if (((d_i_fx(i, j, k) .ne. 0.0d0) .and. (d_j_fx(i, j, k) .eq. 0.0d0)) .and. (d_k_fx(i, j, k) .eq. 0.0d0)) then
					k2fx(i, j, k) = extrapolatedsubfxsupi(i, j, k)
					extrapolatedsubfxsupj(i, j, k) = k2fx(i, j, k)
					extrapolatedsubfxsupk(i, j, k) = k2fx(i, j, k)
					else
					if (((d_i_fx(i, j, k) .eq. 0.0d0) .and. (d_j_fx(i, j, k) .ne. 0.0d0)) .and. (d_k_fx(i, j, k) .ne. 0.0d0)) then
					k2fx(i, j, k) = (extrapolatedsubfxsupj(i, j, k) + extrapolatedsubfxsupk(i, j, k)) / 2.0d0
					extrapolatedsubfxsupi(i, j, k) = k2fx(i, j, k)
					else
					if (((d_i_fx(i, j, k) .eq. 0.0d0) .and. (d_j_fx(i, j, k) .ne. 0.0d0)) .and. (d_k_fx(i, j, k) .eq. 0.0d0)) then
					k2fx(i, j, k) = extrapolatedsubfxsupj(i, j, k)
					extrapolatedsubfxsupi(i, j, k) = k2fx(i, j, k)
					extrapolatedsubfxsupk(i, j, k) = k2fx(i, j, k)
					else
					if (((d_i_fx(i, j, k) .eq. 0.0d0) .and. (d_j_fx(i, j, k) .eq. 0.0d0)) .and. (d_k_fx(i, j, k) .ne. 0.0d0)) then
					k2fx(i, j, k) = extrapolatedsubfxsupk(i, j, k)
					extrapolatedsubfxsupi(i, j, k) = k2fx(i, j, k)
					extrapolatedsubfxsupj(i, j, k) = k2fx(i, j, k)
					end if
					end if
					end if
					end if
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fy(i, j, k) .ne. 0 .or. d_j_fy(i, j, k) .ne. 0 .or. d_k_fy(i, j, k) .ne. 0)) then
				if (FOV_1(int(i - d_i_fy(i, j, k)), int(j - d_j_fy(i, j, k)), int(k - d_k_fy(i, j, k))) .gt. 0) then
					extrapolatedsubfysupi(i, j, k) = pfy
					extrapolatedsubfysupj(i, j, k) = pfy
					extrapolatedsubfysupk(i, j, k) = pfy
					if (((d_i_fy(i, j, k) .ne. 0.0d0) .and. (d_j_fy(i, j, k) .ne. 0.0d0)) .and. (d_k_fy(i, j, k) .ne. 0.0d0)) then
					k2fy(i, j, k) = ((extrapolatedsubfysupi(i, j, k) + extrapolatedsubfysupj(i, j, k)) + extrapolatedsubfysupk(i, j, k)) / 3.0d0
					else
					if (((d_i_fy(i, j, k) .ne. 0.0d0) .and. (d_j_fy(i, j, k) .ne. 0.0d0)) .and. (d_k_fy(i, j, k) .eq. 0.0d0)) then
					k2fy(i, j, k) = (extrapolatedsubfysupi(i, j, k) + extrapolatedsubfysupj(i, j, k)) / 2.0d0
					extrapolatedsubfysupk(i, j, k) = k2fy(i, j, k)
					else
					if (((d_i_fy(i, j, k) .ne. 0.0d0) .and. (d_j_fy(i, j, k) .eq. 0.0d0)) .and. (d_k_fy(i, j, k) .ne. 0.0d0)) then
					k2fy(i, j, k) = (extrapolatedsubfysupi(i, j, k) + extrapolatedsubfysupk(i, j, k)) / 2.0d0
					extrapolatedsubfysupj(i, j, k) = k2fy(i, j, k)
					else
					if (((d_i_fy(i, j, k) .ne. 0.0d0) .and. (d_j_fy(i, j, k) .eq. 0.0d0)) .and. (d_k_fy(i, j, k) .eq. 0.0d0)) then
					k2fy(i, j, k) = extrapolatedsubfysupi(i, j, k)
					extrapolatedsubfysupj(i, j, k) = k2fy(i, j, k)
					extrapolatedsubfysupk(i, j, k) = k2fy(i, j, k)
					else
					if (((d_i_fy(i, j, k) .eq. 0.0d0) .and. (d_j_fy(i, j, k) .ne. 0.0d0)) .and. (d_k_fy(i, j, k) .ne. 0.0d0)) then
					k2fy(i, j, k) = (extrapolatedsubfysupj(i, j, k) + extrapolatedsubfysupk(i, j, k)) / 2.0d0
					extrapolatedsubfysupi(i, j, k) = k2fy(i, j, k)
					else
					if (((d_i_fy(i, j, k) .eq. 0.0d0) .and. (d_j_fy(i, j, k) .ne. 0.0d0)) .and. (d_k_fy(i, j, k) .eq. 0.0d0)) then
					k2fy(i, j, k) = extrapolatedsubfysupj(i, j, k)
					extrapolatedsubfysupi(i, j, k) = k2fy(i, j, k)
					extrapolatedsubfysupk(i, j, k) = k2fy(i, j, k)
					else
					if (((d_i_fy(i, j, k) .eq. 0.0d0) .and. (d_j_fy(i, j, k) .eq. 0.0d0)) .and. (d_k_fy(i, j, k) .ne. 0.0d0)) then
					k2fy(i, j, k) = extrapolatedsubfysupk(i, j, k)
					extrapolatedsubfysupi(i, j, k) = k2fy(i, j, k)
					extrapolatedsubfysupj(i, j, k) = k2fy(i, j, k)
					end if
					end if
					end if
					end if
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fz(i, j, k) .ne. 0 .or. d_j_fz(i, j, k) .ne. 0 .or. d_k_fz(i, j, k) .ne. 0)) then
				if (FOV_1(int(i - d_i_fz(i, j, k)), int(j - d_j_fz(i, j, k)), int(k - d_k_fz(i, j, k))) .gt. 0) then
					extrapolatedsubfzsupi(i, j, k) = pfz
					extrapolatedsubfzsupj(i, j, k) = pfz
					extrapolatedsubfzsupk(i, j, k) = pfz
					if (((d_i_fz(i, j, k) .ne. 0.0d0) .and. (d_j_fz(i, j, k) .ne. 0.0d0)) .and. (d_k_fz(i, j, k) .ne. 0.0d0)) then
					k2fz(i, j, k) = ((extrapolatedsubfzsupi(i, j, k) + extrapolatedsubfzsupj(i, j, k)) + extrapolatedsubfzsupk(i, j, k)) / 3.0d0
					else
					if (((d_i_fz(i, j, k) .ne. 0.0d0) .and. (d_j_fz(i, j, k) .ne. 0.0d0)) .and. (d_k_fz(i, j, k) .eq. 0.0d0)) then
					k2fz(i, j, k) = (extrapolatedsubfzsupi(i, j, k) + extrapolatedsubfzsupj(i, j, k)) / 2.0d0
					extrapolatedsubfzsupk(i, j, k) = k2fz(i, j, k)
					else
					if (((d_i_fz(i, j, k) .ne. 0.0d0) .and. (d_j_fz(i, j, k) .eq. 0.0d0)) .and. (d_k_fz(i, j, k) .ne. 0.0d0)) then
					k2fz(i, j, k) = (extrapolatedsubfzsupi(i, j, k) + extrapolatedsubfzsupk(i, j, k)) / 2.0d0
					extrapolatedsubfzsupj(i, j, k) = k2fz(i, j, k)
					else
					if (((d_i_fz(i, j, k) .ne. 0.0d0) .and. (d_j_fz(i, j, k) .eq. 0.0d0)) .and. (d_k_fz(i, j, k) .eq. 0.0d0)) then
					k2fz(i, j, k) = extrapolatedsubfzsupi(i, j, k)
					extrapolatedsubfzsupj(i, j, k) = k2fz(i, j, k)
					extrapolatedsubfzsupk(i, j, k) = k2fz(i, j, k)
					else
					if (((d_i_fz(i, j, k) .eq. 0.0d0) .and. (d_j_fz(i, j, k) .ne. 0.0d0)) .and. (d_k_fz(i, j, k) .ne. 0.0d0)) then
					k2fz(i, j, k) = (extrapolatedsubfzsupj(i, j, k) + extrapolatedsubfzsupk(i, j, k)) / 2.0d0
					extrapolatedsubfzsupi(i, j, k) = k2fz(i, j, k)
					else
					if (((d_i_fz(i, j, k) .eq. 0.0d0) .and. (d_j_fz(i, j, k) .ne. 0.0d0)) .and. (d_k_fz(i, j, k) .eq. 0.0d0)) then
					k2fz(i, j, k) = extrapolatedsubfzsupj(i, j, k)
					extrapolatedsubfzsupi(i, j, k) = k2fz(i, j, k)
					extrapolatedsubfzsupk(i, j, k) = k2fz(i, j, k)
					else
					if (((d_i_fz(i, j, k) .eq. 0.0d0) .and. (d_j_fz(i, j, k) .eq. 0.0d0)) .and. (d_k_fz(i, j, k) .ne. 0.0d0)) then
					k2fz(i, j, k) = extrapolatedsubfzsupk(i, j, k)
					extrapolatedsubfzsupi(i, j, k) = k2fz(i, j, k)
					extrapolatedsubfzsupj(i, j, k) = k2fz(i, j, k)
					end if
					end if
					end if
					end if
					end if
					end if
					end if
				end if
			end if
		end if
		if ((FOV_yLower(i, j, k) .gt. 0) .and. ((j + 1 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 1), k) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 2), k) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 3), k) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 1), k) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 1), k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 1), k) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 1), k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 1), k) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 1), k) .gt. 0)) .or. (j + 1 .le. cctk_lsh(2) .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 1), int(k + 1)) .gt. 0)) .or. (j + 1 .le. cctk_lsh(2) .and. k - 1 .ge. 1 .and. (FOV_1(i, int(j + 1), int(k - 1)) .gt. 0)) .or. (j + 1 .le. cctk_lsh(2) .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 1), int(k + 2)) .gt. 0)) .or. (j + 1 .le. cctk_lsh(2) .and. k - 2 .ge. 1 .and. (FOV_1(i, int(j + 1), int(k - 2)) .gt. 0)) .or. (j + 1 .le. cctk_lsh(2) .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 1), int(k + 3)) .gt. 0)) .or. (j + 1 .le. cctk_lsh(2) .and. k - 3 .ge. 1 .and. (FOV_1(i, int(j + 1), int(k - 3)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 2), k) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 2), k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 2), k) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 2), k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 2), k) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 2), k) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 2), int(k + 1)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. k - 1 .ge. 1 .and. (FOV_1(i, int(j + 2), int(k - 1)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 2), int(k + 2)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. k - 2 .ge. 1 .and. (FOV_1(i, int(j + 2), int(k - 2)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 2), int(k + 3)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. k - 3 .ge. 1 .and. (FOV_1(i, int(j + 2), int(k - 3)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 3), k) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 3), k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 3), k) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 3), k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 3), k) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 3), k) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 3), int(k + 1)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. k - 1 .ge. 1 .and. (FOV_1(i, int(j + 3), int(k - 1)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 3), int(k + 2)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. k - 2 .ge. 1 .and. (FOV_1(i, int(j + 3), int(k - 2)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 3), int(k + 3)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. k - 3 .ge. 1 .and. (FOV_1(i, int(j + 3), int(k - 3)) .gt. 0))))) then
				k2rho(i, 4 - j, k) = 0.0d0
				k2mx(i, 4 - j, k) = 0.0d0
				k2my(i, 4 - j, k) = 0.0d0
				k2mz(i, 4 - j, k) = 0.0d0
				k2p(i, 4 - j, k) = 0.0d0
				k2fx(i, 4 - j, k) = 0.0d0
				k2fy(i, 4 - j, k) = 0.0d0
				k2fz(i, 4 - j, k) = 0.0d0
		end if
		if ((FOV_yUpper(i, j, k) .gt. 0) .and. ((j - 1 .ge. 1 .and. (FOV_1(i, int(j - 1), k) .gt. 0)) .or. (j - 2 .ge. 1 .and. (FOV_1(i, int(j - 2), k) .gt. 0)) .or. (j - 3 .ge. 1 .and. (FOV_1(i, int(j - 3), k) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 1), k) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 1), k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 1), k) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 1), k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 1), k) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 1), k) .gt. 0)) .or. (j - 1 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 1), int(k + 1)) .gt. 0)) .or. (j - 1 .ge. 1 .and. k - 1 .ge. 1 .and. (FOV_1(i, int(j - 1), int(k - 1)) .gt. 0)) .or. (j - 1 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 1), int(k + 2)) .gt. 0)) .or. (j - 1 .ge. 1 .and. k - 2 .ge. 1 .and. (FOV_1(i, int(j - 1), int(k - 2)) .gt. 0)) .or. (j - 1 .ge. 1 .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 1), int(k + 3)) .gt. 0)) .or. (j - 1 .ge. 1 .and. k - 3 .ge. 1 .and. (FOV_1(i, int(j - 1), int(k - 3)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 2), k) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 2), k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 2), k) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 2), k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 2), k) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 2), k) .gt. 0)) .or. (j - 2 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 2), int(k + 1)) .gt. 0)) .or. (j - 2 .ge. 1 .and. k - 1 .ge. 1 .and. (FOV_1(i, int(j - 2), int(k - 1)) .gt. 0)) .or. (j - 2 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 2), int(k + 2)) .gt. 0)) .or. (j - 2 .ge. 1 .and. k - 2 .ge. 1 .and. (FOV_1(i, int(j - 2), int(k - 2)) .gt. 0)) .or. (j - 2 .ge. 1 .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 2), int(k + 3)) .gt. 0)) .or. (j - 2 .ge. 1 .and. k - 3 .ge. 1 .and. (FOV_1(i, int(j - 2), int(k - 3)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 3), k) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 3), k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 3), k) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 3), k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 3), k) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 3), k) .gt. 0)) .or. (j - 3 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 3), int(k + 1)) .gt. 0)) .or. (j - 3 .ge. 1 .and. k - 1 .ge. 1 .and. (FOV_1(i, int(j - 3), int(k - 1)) .gt. 0)) .or. (j - 3 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 3), int(k + 2)) .gt. 0)) .or. (j - 3 .ge. 1 .and. k - 2 .ge. 1 .and. (FOV_1(i, int(j - 3), int(k - 2)) .gt. 0)) .or. (j - 3 .ge. 1 .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 3), int(k + 3)) .gt. 0)) .or. (j - 3 .ge. 1 .and. k - 3 .ge. 1 .and. (FOV_1(i, int(j - 3), int(k - 3)) .gt. 0))))) then
			k2rho(i, j, k) = 0.0d0
			k2mx(i, j, k) = 0.0d0
			k2my(i, j, k) = 0.0d0
			k2mz(i, j, k) = 0.0d0
			k2p(i, j, k) = 0.0d0
			k2fx(i, j, k) = 0.0d0
			k2fy(i, j, k) = 0.0d0
			k2fz(i, j, k) = 0.0d0
		end if
		if ((FOV_zLower(i, j, k) .gt. 0) .and. ((k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, j, int(k + 1)) .gt. 0)) .or. (k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, j, int(k + 2)) .gt. 0)) .or. (k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, j, int(k + 3)) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(int(i + 1), j, int(k + 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(int(i - 1), j, int(k + 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(int(i + 2), j, int(k + 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(int(i - 2), j, int(k + 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(int(i + 3), j, int(k + 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(int(i - 3), j, int(k + 1)) .gt. 0)) .or. (j + 1 .le. cctk_lsh(2) .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 1), int(k + 1)) .gt. 0)) .or. (j - 1 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 1), int(k + 1)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 2), int(k + 1)) .gt. 0)) .or. (j - 2 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 2), int(k + 1)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 3), int(k + 1)) .gt. 0)) .or. (j - 3 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 3), int(k + 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(int(i + 1), j, int(k + 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(int(i - 1), j, int(k + 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(int(i + 2), j, int(k + 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(int(i - 2), j, int(k + 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(int(i + 3), j, int(k + 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(int(i - 3), j, int(k + 2)) .gt. 0)) .or. (j + 1 .le. cctk_lsh(2) .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 1), int(k + 2)) .gt. 0)) .or. (j - 1 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 1), int(k + 2)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 2), int(k + 2)) .gt. 0)) .or. (j - 2 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 2), int(k + 2)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 3), int(k + 2)) .gt. 0)) .or. (j - 3 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 3), int(k + 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(int(i + 1), j, int(k + 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(int(i - 1), j, int(k + 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(int(i + 2), j, int(k + 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(int(i - 2), j, int(k + 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(int(i + 3), j, int(k + 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(int(i - 3), j, int(k + 3)) .gt. 0)) .or. (j + 1 .le. cctk_lsh(2) .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 1), int(k + 3)) .gt. 0)) .or. (j - 1 .ge. 1 .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 1), int(k + 3)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 2), int(k + 3)) .gt. 0)) .or. (j - 2 .ge. 1 .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 2), int(k + 3)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 3), int(k + 3)) .gt. 0)) .or. (j - 3 .ge. 1 .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 3), int(k + 3)) .gt. 0))))) then
				k2rho(i, j, 4 - k) = 0.0d0
				k2mx(i, j, 4 - k) = 0.0d0
				k2my(i, j, 4 - k) = 0.0d0
				k2mz(i, j, 4 - k) = 0.0d0
				k2p(i, j, 4 - k) = 0.0d0
				k2fx(i, j, 4 - k) = 0.0d0
				k2fy(i, j, 4 - k) = 0.0d0
				k2fz(i, j, 4 - k) = 0.0d0
		end if
		if ((FOV_zUpper(i, j, k) .gt. 0) .and. ((k - 1 .ge. 1 .and. (FOV_1(i, j, int(k - 1)) .gt. 0)) .or. (k - 2 .ge. 1 .and. (FOV_1(i, j, int(k - 2)) .gt. 0)) .or. (k - 3 .ge. 1 .and. (FOV_1(i, j, int(k - 3)) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. k - 1 .ge. 1 .and. (FOV_1(int(i + 1), j, int(k - 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. k - 1 .ge. 1 .and. (FOV_1(int(i - 1), j, int(k - 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. k - 1 .ge. 1 .and. (FOV_1(int(i + 2), j, int(k - 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. k - 1 .ge. 1 .and. (FOV_1(int(i - 2), j, int(k - 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. k - 1 .ge. 1 .and. (FOV_1(int(i + 3), j, int(k - 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. k - 1 .ge. 1 .and. (FOV_1(int(i - 3), j, int(k - 1)) .gt. 0)) .or. (j + 1 .le. cctk_lsh(2) .and. k - 1 .ge. 1 .and. (FOV_1(i, int(j + 1), int(k - 1)) .gt. 0)) .or. (j - 1 .ge. 1 .and. k - 1 .ge. 1 .and. (FOV_1(i, int(j - 1), int(k - 1)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. k - 1 .ge. 1 .and. (FOV_1(i, int(j + 2), int(k - 1)) .gt. 0)) .or. (j - 2 .ge. 1 .and. k - 1 .ge. 1 .and. (FOV_1(i, int(j - 2), int(k - 1)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. k - 1 .ge. 1 .and. (FOV_1(i, int(j + 3), int(k - 1)) .gt. 0)) .or. (j - 3 .ge. 1 .and. k - 1 .ge. 1 .and. (FOV_1(i, int(j - 3), int(k - 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. k - 2 .ge. 1 .and. (FOV_1(int(i + 1), j, int(k - 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. k - 2 .ge. 1 .and. (FOV_1(int(i - 1), j, int(k - 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. k - 2 .ge. 1 .and. (FOV_1(int(i + 2), j, int(k - 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. k - 2 .ge. 1 .and. (FOV_1(int(i - 2), j, int(k - 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. k - 2 .ge. 1 .and. (FOV_1(int(i + 3), j, int(k - 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. k - 2 .ge. 1 .and. (FOV_1(int(i - 3), j, int(k - 2)) .gt. 0)) .or. (j + 1 .le. cctk_lsh(2) .and. k - 2 .ge. 1 .and. (FOV_1(i, int(j + 1), int(k - 2)) .gt. 0)) .or. (j - 1 .ge. 1 .and. k - 2 .ge. 1 .and. (FOV_1(i, int(j - 1), int(k - 2)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. k - 2 .ge. 1 .and. (FOV_1(i, int(j + 2), int(k - 2)) .gt. 0)) .or. (j - 2 .ge. 1 .and. k - 2 .ge. 1 .and. (FOV_1(i, int(j - 2), int(k - 2)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. k - 2 .ge. 1 .and. (FOV_1(i, int(j + 3), int(k - 2)) .gt. 0)) .or. (j - 3 .ge. 1 .and. k - 2 .ge. 1 .and. (FOV_1(i, int(j - 3), int(k - 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. k - 3 .ge. 1 .and. (FOV_1(int(i + 1), j, int(k - 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. k - 3 .ge. 1 .and. (FOV_1(int(i - 1), j, int(k - 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. k - 3 .ge. 1 .and. (FOV_1(int(i + 2), j, int(k - 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. k - 3 .ge. 1 .and. (FOV_1(int(i - 2), j, int(k - 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. k - 3 .ge. 1 .and. (FOV_1(int(i + 3), j, int(k - 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. k - 3 .ge. 1 .and. (FOV_1(int(i - 3), j, int(k - 3)) .gt. 0)) .or. (j + 1 .le. cctk_lsh(2) .and. k - 3 .ge. 1 .and. (FOV_1(i, int(j + 1), int(k - 3)) .gt. 0)) .or. (j - 1 .ge. 1 .and. k - 3 .ge. 1 .and. (FOV_1(i, int(j - 1), int(k - 3)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. k - 3 .ge. 1 .and. (FOV_1(i, int(j + 2), int(k - 3)) .gt. 0)) .or. (j - 2 .ge. 1 .and. k - 3 .ge. 1 .and. (FOV_1(i, int(j - 2), int(k - 3)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. k - 3 .ge. 1 .and. (FOV_1(i, int(j + 3), int(k - 3)) .gt. 0)) .or. (j - 3 .ge. 1 .and. k - 3 .ge. 1 .and. (FOV_1(i, int(j - 3), int(k - 3)) .gt. 0))))) then
			k2rho(i, j, k) = 0.0d0
			k2mx(i, j, k) = 0.0d0
			k2my(i, j, k) = 0.0d0
			k2mz(i, j, k) = 0.0d0
			k2p(i, j, k) = 0.0d0
			k2fx(i, j, k) = 0.0d0
			k2fy(i, j, k) = 0.0d0
			k2fz(i, j, k) = 0.0d0
		end if
		if (FOV_1(i, j, k) .gt. 0) then
			if ((d_i_Null(i, j, k) .ne. 0 .or. d_j_Null(i, j, k) .ne. 0 .or. d_k_Null(i, j, k) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_Null, k2Null, extrapolatedsubNullsupi, i, d_j_Null, k2Null, extrapolatedsubNullsupj, j, d_k_Null, k2Null, extrapolatedsubNullsupk, k, k2Null, FOV_3)
			end if
		end if
				end do
			end do
		end do
		do k = kStart, kEnd
			do j = jStart, jEnd
				do i = iStart, iEnd
					if ((FOV_xLower(i, j, k) .gt. 0) .and. ((i + 1 .le. cctk_lsh(1) .and. (FOV_3(int(i + 1), j, k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. (FOV_3(int(i + 2), j, k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. (FOV_3(int(i + 3), j, k) .gt. 0)))) then
						extrapolatedsubrhosupi(i, j, k) = k2rho(i, j, k)
						extrapolatedsubrhosupj(i, j, k) = k2rho(i, j, k)
						extrapolatedsubrhosupk(i, j, k) = k2rho(i, j, k)
						extrapolatedsubmxsupi(i, j, k) = k2mx(i, j, k)
						extrapolatedsubmxsupj(i, j, k) = k2mx(i, j, k)
						extrapolatedsubmxsupk(i, j, k) = k2mx(i, j, k)
						extrapolatedsubmysupi(i, j, k) = k2my(i, j, k)
						extrapolatedsubmysupj(i, j, k) = k2my(i, j, k)
						extrapolatedsubmysupk(i, j, k) = k2my(i, j, k)
						extrapolatedsubmzsupi(i, j, k) = k2mz(i, j, k)
						extrapolatedsubmzsupj(i, j, k) = k2mz(i, j, k)
						extrapolatedsubmzsupk(i, j, k) = k2mz(i, j, k)
						extrapolatedsubNullsupi(i, j, k) = k2Null(i, j, k)
						extrapolatedsubNullsupj(i, j, k) = k2Null(i, j, k)
						extrapolatedsubNullsupk(i, j, k) = k2Null(i, j, k)
						extrapolatedsubfxsupi(i, j, k) = k2fx(i, j, k)
						extrapolatedsubfxsupj(i, j, k) = k2fx(i, j, k)
						extrapolatedsubfxsupk(i, j, k) = k2fx(i, j, k)
						extrapolatedsubfysupi(i, j, k) = k2fy(i, j, k)
						extrapolatedsubfysupj(i, j, k) = k2fy(i, j, k)
						extrapolatedsubfysupk(i, j, k) = k2fy(i, j, k)
						extrapolatedsubfzsupi(i, j, k) = k2fz(i, j, k)
						extrapolatedsubfzsupj(i, j, k) = k2fz(i, j, k)
						extrapolatedsubfzsupk(i, j, k) = k2fz(i, j, k)
						extrapolatedsubpsupi(i, j, k) = k2p(i, j, k)
						extrapolatedsubpsupj(i, j, k) = k2p(i, j, k)
						extrapolatedsubpsupk(i, j, k) = k2p(i, j, k)
					end if
					if ((FOV_xUpper(i, j, k) .gt. 0) .and. ((i - 1 .ge. 1 .and. (FOV_3(int(i - 1), j, k) .gt. 0)) .or. (i - 2 .ge. 1 .and. (FOV_3(int(i - 2), j, k) .gt. 0)) .or. (i - 3 .ge. 1 .and. (FOV_3(int(i - 3), j, k) .gt. 0)))) then
						extrapolatedsubrhosupi(i, j, k) = k2rho(i, j, k)
						extrapolatedsubrhosupj(i, j, k) = k2rho(i, j, k)
						extrapolatedsubrhosupk(i, j, k) = k2rho(i, j, k)
						extrapolatedsubmxsupi(i, j, k) = k2mx(i, j, k)
						extrapolatedsubmxsupj(i, j, k) = k2mx(i, j, k)
						extrapolatedsubmxsupk(i, j, k) = k2mx(i, j, k)
						extrapolatedsubmysupi(i, j, k) = k2my(i, j, k)
						extrapolatedsubmysupj(i, j, k) = k2my(i, j, k)
						extrapolatedsubmysupk(i, j, k) = k2my(i, j, k)
						extrapolatedsubmzsupi(i, j, k) = k2mz(i, j, k)
						extrapolatedsubmzsupj(i, j, k) = k2mz(i, j, k)
						extrapolatedsubmzsupk(i, j, k) = k2mz(i, j, k)
						extrapolatedsubNullsupi(i, j, k) = k2Null(i, j, k)
						extrapolatedsubNullsupj(i, j, k) = k2Null(i, j, k)
						extrapolatedsubNullsupk(i, j, k) = k2Null(i, j, k)
						extrapolatedsubfxsupi(i, j, k) = k2fx(i, j, k)
						extrapolatedsubfxsupj(i, j, k) = k2fx(i, j, k)
						extrapolatedsubfxsupk(i, j, k) = k2fx(i, j, k)
						extrapolatedsubfysupi(i, j, k) = k2fy(i, j, k)
						extrapolatedsubfysupj(i, j, k) = k2fy(i, j, k)
						extrapolatedsubfysupk(i, j, k) = k2fy(i, j, k)
						extrapolatedsubfzsupi(i, j, k) = k2fz(i, j, k)
						extrapolatedsubfzsupj(i, j, k) = k2fz(i, j, k)
						extrapolatedsubfzsupk(i, j, k) = k2fz(i, j, k)
						extrapolatedsubpsupi(i, j, k) = k2p(i, j, k)
						extrapolatedsubpsupj(i, j, k) = k2p(i, j, k)
						extrapolatedsubpsupk(i, j, k) = k2p(i, j, k)
					end if
					if ((FOV_yLower(i, j, k) .gt. 0) .and. ((j + 1 .le. cctk_lsh(2) .and. (FOV_3(i, int(j + 1), k) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. (FOV_3(i, int(j + 2), k) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. (FOV_3(i, int(j + 3), k) .gt. 0)))) then
						extrapolatedsubrhosupi(i, j, k) = k2rho(i, j, k)
						extrapolatedsubrhosupj(i, j, k) = k2rho(i, j, k)
						extrapolatedsubrhosupk(i, j, k) = k2rho(i, j, k)
						extrapolatedsubmxsupi(i, j, k) = k2mx(i, j, k)
						extrapolatedsubmxsupj(i, j, k) = k2mx(i, j, k)
						extrapolatedsubmxsupk(i, j, k) = k2mx(i, j, k)
						extrapolatedsubmysupi(i, j, k) = k2my(i, j, k)
						extrapolatedsubmysupj(i, j, k) = k2my(i, j, k)
						extrapolatedsubmysupk(i, j, k) = k2my(i, j, k)
						extrapolatedsubmzsupi(i, j, k) = k2mz(i, j, k)
						extrapolatedsubmzsupj(i, j, k) = k2mz(i, j, k)
						extrapolatedsubmzsupk(i, j, k) = k2mz(i, j, k)
						extrapolatedsubNullsupi(i, j, k) = k2Null(i, j, k)
						extrapolatedsubNullsupj(i, j, k) = k2Null(i, j, k)
						extrapolatedsubNullsupk(i, j, k) = k2Null(i, j, k)
						extrapolatedsubfxsupi(i, j, k) = k2fx(i, j, k)
						extrapolatedsubfxsupj(i, j, k) = k2fx(i, j, k)
						extrapolatedsubfxsupk(i, j, k) = k2fx(i, j, k)
						extrapolatedsubfysupi(i, j, k) = k2fy(i, j, k)
						extrapolatedsubfysupj(i, j, k) = k2fy(i, j, k)
						extrapolatedsubfysupk(i, j, k) = k2fy(i, j, k)
						extrapolatedsubfzsupi(i, j, k) = k2fz(i, j, k)
						extrapolatedsubfzsupj(i, j, k) = k2fz(i, j, k)
						extrapolatedsubfzsupk(i, j, k) = k2fz(i, j, k)
						extrapolatedsubpsupi(i, j, k) = k2p(i, j, k)
						extrapolatedsubpsupj(i, j, k) = k2p(i, j, k)
						extrapolatedsubpsupk(i, j, k) = k2p(i, j, k)
					end if
					if ((FOV_yUpper(i, j, k) .gt. 0) .and. ((j - 1 .ge. 1 .and. (FOV_3(i, int(j - 1), k) .gt. 0)) .or. (j - 2 .ge. 1 .and. (FOV_3(i, int(j - 2), k) .gt. 0)) .or. (j - 3 .ge. 1 .and. (FOV_3(i, int(j - 3), k) .gt. 0)))) then
						extrapolatedsubrhosupi(i, j, k) = k2rho(i, j, k)
						extrapolatedsubrhosupj(i, j, k) = k2rho(i, j, k)
						extrapolatedsubrhosupk(i, j, k) = k2rho(i, j, k)
						extrapolatedsubmxsupi(i, j, k) = k2mx(i, j, k)
						extrapolatedsubmxsupj(i, j, k) = k2mx(i, j, k)
						extrapolatedsubmxsupk(i, j, k) = k2mx(i, j, k)
						extrapolatedsubmysupi(i, j, k) = k2my(i, j, k)
						extrapolatedsubmysupj(i, j, k) = k2my(i, j, k)
						extrapolatedsubmysupk(i, j, k) = k2my(i, j, k)
						extrapolatedsubmzsupi(i, j, k) = k2mz(i, j, k)
						extrapolatedsubmzsupj(i, j, k) = k2mz(i, j, k)
						extrapolatedsubmzsupk(i, j, k) = k2mz(i, j, k)
						extrapolatedsubNullsupi(i, j, k) = k2Null(i, j, k)
						extrapolatedsubNullsupj(i, j, k) = k2Null(i, j, k)
						extrapolatedsubNullsupk(i, j, k) = k2Null(i, j, k)
						extrapolatedsubfxsupi(i, j, k) = k2fx(i, j, k)
						extrapolatedsubfxsupj(i, j, k) = k2fx(i, j, k)
						extrapolatedsubfxsupk(i, j, k) = k2fx(i, j, k)
						extrapolatedsubfysupi(i, j, k) = k2fy(i, j, k)
						extrapolatedsubfysupj(i, j, k) = k2fy(i, j, k)
						extrapolatedsubfysupk(i, j, k) = k2fy(i, j, k)
						extrapolatedsubfzsupi(i, j, k) = k2fz(i, j, k)
						extrapolatedsubfzsupj(i, j, k) = k2fz(i, j, k)
						extrapolatedsubfzsupk(i, j, k) = k2fz(i, j, k)
						extrapolatedsubpsupi(i, j, k) = k2p(i, j, k)
						extrapolatedsubpsupj(i, j, k) = k2p(i, j, k)
						extrapolatedsubpsupk(i, j, k) = k2p(i, j, k)
					end if
					if ((FOV_zLower(i, j, k) .gt. 0) .and. ((k + 1 .le. cctk_lsh(3) .and. (FOV_3(i, j, int(k + 1)) .gt. 0)) .or. (k + 2 .le. cctk_lsh(3) .and. (FOV_3(i, j, int(k + 2)) .gt. 0)) .or. (k + 3 .le. cctk_lsh(3) .and. (FOV_3(i, j, int(k + 3)) .gt. 0)))) then
						extrapolatedsubrhosupi(i, j, k) = k2rho(i, j, k)
						extrapolatedsubrhosupj(i, j, k) = k2rho(i, j, k)
						extrapolatedsubrhosupk(i, j, k) = k2rho(i, j, k)
						extrapolatedsubmxsupi(i, j, k) = k2mx(i, j, k)
						extrapolatedsubmxsupj(i, j, k) = k2mx(i, j, k)
						extrapolatedsubmxsupk(i, j, k) = k2mx(i, j, k)
						extrapolatedsubmysupi(i, j, k) = k2my(i, j, k)
						extrapolatedsubmysupj(i, j, k) = k2my(i, j, k)
						extrapolatedsubmysupk(i, j, k) = k2my(i, j, k)
						extrapolatedsubmzsupi(i, j, k) = k2mz(i, j, k)
						extrapolatedsubmzsupj(i, j, k) = k2mz(i, j, k)
						extrapolatedsubmzsupk(i, j, k) = k2mz(i, j, k)
						extrapolatedsubNullsupi(i, j, k) = k2Null(i, j, k)
						extrapolatedsubNullsupj(i, j, k) = k2Null(i, j, k)
						extrapolatedsubNullsupk(i, j, k) = k2Null(i, j, k)
						extrapolatedsubfxsupi(i, j, k) = k2fx(i, j, k)
						extrapolatedsubfxsupj(i, j, k) = k2fx(i, j, k)
						extrapolatedsubfxsupk(i, j, k) = k2fx(i, j, k)
						extrapolatedsubfysupi(i, j, k) = k2fy(i, j, k)
						extrapolatedsubfysupj(i, j, k) = k2fy(i, j, k)
						extrapolatedsubfysupk(i, j, k) = k2fy(i, j, k)
						extrapolatedsubfzsupi(i, j, k) = k2fz(i, j, k)
						extrapolatedsubfzsupj(i, j, k) = k2fz(i, j, k)
						extrapolatedsubfzsupk(i, j, k) = k2fz(i, j, k)
						extrapolatedsubpsupi(i, j, k) = k2p(i, j, k)
						extrapolatedsubpsupj(i, j, k) = k2p(i, j, k)
						extrapolatedsubpsupk(i, j, k) = k2p(i, j, k)
					end if
					if ((FOV_zUpper(i, j, k) .gt. 0) .and. ((k - 1 .ge. 1 .and. (FOV_3(i, j, int(k - 1)) .gt. 0)) .or. (k - 2 .ge. 1 .and. (FOV_3(i, j, int(k - 2)) .gt. 0)) .or. (k - 3 .ge. 1 .and. (FOV_3(i, j, int(k - 3)) .gt. 0)))) then
						extrapolatedsubrhosupi(i, j, k) = k2rho(i, j, k)
						extrapolatedsubrhosupj(i, j, k) = k2rho(i, j, k)
						extrapolatedsubrhosupk(i, j, k) = k2rho(i, j, k)
						extrapolatedsubmxsupi(i, j, k) = k2mx(i, j, k)
						extrapolatedsubmxsupj(i, j, k) = k2mx(i, j, k)
						extrapolatedsubmxsupk(i, j, k) = k2mx(i, j, k)
						extrapolatedsubmysupi(i, j, k) = k2my(i, j, k)
						extrapolatedsubmysupj(i, j, k) = k2my(i, j, k)
						extrapolatedsubmysupk(i, j, k) = k2my(i, j, k)
						extrapolatedsubmzsupi(i, j, k) = k2mz(i, j, k)
						extrapolatedsubmzsupj(i, j, k) = k2mz(i, j, k)
						extrapolatedsubmzsupk(i, j, k) = k2mz(i, j, k)
						extrapolatedsubNullsupi(i, j, k) = k2Null(i, j, k)
						extrapolatedsubNullsupj(i, j, k) = k2Null(i, j, k)
						extrapolatedsubNullsupk(i, j, k) = k2Null(i, j, k)
						extrapolatedsubfxsupi(i, j, k) = k2fx(i, j, k)
						extrapolatedsubfxsupj(i, j, k) = k2fx(i, j, k)
						extrapolatedsubfxsupk(i, j, k) = k2fx(i, j, k)
						extrapolatedsubfysupi(i, j, k) = k2fy(i, j, k)
						extrapolatedsubfysupj(i, j, k) = k2fy(i, j, k)
						extrapolatedsubfysupk(i, j, k) = k2fy(i, j, k)
						extrapolatedsubfzsupi(i, j, k) = k2fz(i, j, k)
						extrapolatedsubfzsupj(i, j, k) = k2fz(i, j, k)
						extrapolatedsubfzsupk(i, j, k) = k2fz(i, j, k)
						extrapolatedsubpsupi(i, j, k) = k2p(i, j, k)
						extrapolatedsubpsupj(i, j, k) = k2p(i, j, k)
						extrapolatedsubpsupk(i, j, k) = k2p(i, j, k)
					end if
					if ((FOV_xLower(i, j, k) .gt. 0) .and. ((i + 1 .le. cctk_lsh(1) .and. (FOV_1(int(i + 1), j, k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. (FOV_1(int(i + 2), j, k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. (FOV_1(int(i + 3), j, k) .gt. 0)))) then
						extrapolatedsubNullsupi(i, j, k) = k2Null(i, j, k)
						extrapolatedsubNullsupj(i, j, k) = k2Null(i, j, k)
						extrapolatedsubNullsupk(i, j, k) = k2Null(i, j, k)
					end if
					if ((FOV_xUpper(i, j, k) .gt. 0) .and. ((i - 1 .ge. 1 .and. (FOV_1(int(i - 1), j, k) .gt. 0)) .or. (i - 2 .ge. 1 .and. (FOV_1(int(i - 2), j, k) .gt. 0)) .or. (i - 3 .ge. 1 .and. (FOV_1(int(i - 3), j, k) .gt. 0)))) then
						extrapolatedsubNullsupi(i, j, k) = k2Null(i, j, k)
						extrapolatedsubNullsupj(i, j, k) = k2Null(i, j, k)
						extrapolatedsubNullsupk(i, j, k) = k2Null(i, j, k)
					end if
					if ((FOV_yLower(i, j, k) .gt. 0) .and. ((j + 1 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 1), k) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 2), k) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 3), k) .gt. 0)))) then
						extrapolatedsubrhosupi(i, j, k) = k2rho(i, j, k)
						extrapolatedsubrhosupj(i, j, k) = k2rho(i, j, k)
						extrapolatedsubrhosupk(i, j, k) = k2rho(i, j, k)
						extrapolatedsubmxsupi(i, j, k) = k2mx(i, j, k)
						extrapolatedsubmxsupj(i, j, k) = k2mx(i, j, k)
						extrapolatedsubmxsupk(i, j, k) = k2mx(i, j, k)
						extrapolatedsubmysupi(i, j, k) = k2my(i, j, k)
						extrapolatedsubmysupj(i, j, k) = k2my(i, j, k)
						extrapolatedsubmysupk(i, j, k) = k2my(i, j, k)
						extrapolatedsubmzsupi(i, j, k) = k2mz(i, j, k)
						extrapolatedsubmzsupj(i, j, k) = k2mz(i, j, k)
						extrapolatedsubmzsupk(i, j, k) = k2mz(i, j, k)
						extrapolatedsubNullsupi(i, j, k) = k2Null(i, j, k)
						extrapolatedsubNullsupj(i, j, k) = k2Null(i, j, k)
						extrapolatedsubNullsupk(i, j, k) = k2Null(i, j, k)
						extrapolatedsubfxsupi(i, j, k) = k2fx(i, j, k)
						extrapolatedsubfxsupj(i, j, k) = k2fx(i, j, k)
						extrapolatedsubfxsupk(i, j, k) = k2fx(i, j, k)
						extrapolatedsubfysupi(i, j, k) = k2fy(i, j, k)
						extrapolatedsubfysupj(i, j, k) = k2fy(i, j, k)
						extrapolatedsubfysupk(i, j, k) = k2fy(i, j, k)
						extrapolatedsubfzsupi(i, j, k) = k2fz(i, j, k)
						extrapolatedsubfzsupj(i, j, k) = k2fz(i, j, k)
						extrapolatedsubfzsupk(i, j, k) = k2fz(i, j, k)
						extrapolatedsubpsupi(i, j, k) = k2p(i, j, k)
						extrapolatedsubpsupj(i, j, k) = k2p(i, j, k)
						extrapolatedsubpsupk(i, j, k) = k2p(i, j, k)
					end if
					if ((FOV_yUpper(i, j, k) .gt. 0) .and. ((j - 1 .ge. 1 .and. (FOV_1(i, int(j - 1), k) .gt. 0)) .or. (j - 2 .ge. 1 .and. (FOV_1(i, int(j - 2), k) .gt. 0)) .or. (j - 3 .ge. 1 .and. (FOV_1(i, int(j - 3), k) .gt. 0)))) then
						extrapolatedsubrhosupi(i, j, k) = k2rho(i, j, k)
						extrapolatedsubrhosupj(i, j, k) = k2rho(i, j, k)
						extrapolatedsubrhosupk(i, j, k) = k2rho(i, j, k)
						extrapolatedsubmxsupi(i, j, k) = k2mx(i, j, k)
						extrapolatedsubmxsupj(i, j, k) = k2mx(i, j, k)
						extrapolatedsubmxsupk(i, j, k) = k2mx(i, j, k)
						extrapolatedsubmysupi(i, j, k) = k2my(i, j, k)
						extrapolatedsubmysupj(i, j, k) = k2my(i, j, k)
						extrapolatedsubmysupk(i, j, k) = k2my(i, j, k)
						extrapolatedsubmzsupi(i, j, k) = k2mz(i, j, k)
						extrapolatedsubmzsupj(i, j, k) = k2mz(i, j, k)
						extrapolatedsubmzsupk(i, j, k) = k2mz(i, j, k)
						extrapolatedsubNullsupi(i, j, k) = k2Null(i, j, k)
						extrapolatedsubNullsupj(i, j, k) = k2Null(i, j, k)
						extrapolatedsubNullsupk(i, j, k) = k2Null(i, j, k)
						extrapolatedsubfxsupi(i, j, k) = k2fx(i, j, k)
						extrapolatedsubfxsupj(i, j, k) = k2fx(i, j, k)
						extrapolatedsubfxsupk(i, j, k) = k2fx(i, j, k)
						extrapolatedsubfysupi(i, j, k) = k2fy(i, j, k)
						extrapolatedsubfysupj(i, j, k) = k2fy(i, j, k)
						extrapolatedsubfysupk(i, j, k) = k2fy(i, j, k)
						extrapolatedsubfzsupi(i, j, k) = k2fz(i, j, k)
						extrapolatedsubfzsupj(i, j, k) = k2fz(i, j, k)
						extrapolatedsubfzsupk(i, j, k) = k2fz(i, j, k)
						extrapolatedsubpsupi(i, j, k) = k2p(i, j, k)
						extrapolatedsubpsupj(i, j, k) = k2p(i, j, k)
						extrapolatedsubpsupk(i, j, k) = k2p(i, j, k)
					end if
					if ((FOV_zLower(i, j, k) .gt. 0) .and. ((k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, j, int(k + 1)) .gt. 0)) .or. (k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, j, int(k + 2)) .gt. 0)) .or. (k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, j, int(k + 3)) .gt. 0)))) then
						extrapolatedsubrhosupi(i, j, k) = k2rho(i, j, k)
						extrapolatedsubrhosupj(i, j, k) = k2rho(i, j, k)
						extrapolatedsubrhosupk(i, j, k) = k2rho(i, j, k)
						extrapolatedsubmxsupi(i, j, k) = k2mx(i, j, k)
						extrapolatedsubmxsupj(i, j, k) = k2mx(i, j, k)
						extrapolatedsubmxsupk(i, j, k) = k2mx(i, j, k)
						extrapolatedsubmysupi(i, j, k) = k2my(i, j, k)
						extrapolatedsubmysupj(i, j, k) = k2my(i, j, k)
						extrapolatedsubmysupk(i, j, k) = k2my(i, j, k)
						extrapolatedsubmzsupi(i, j, k) = k2mz(i, j, k)
						extrapolatedsubmzsupj(i, j, k) = k2mz(i, j, k)
						extrapolatedsubmzsupk(i, j, k) = k2mz(i, j, k)
						extrapolatedsubNullsupi(i, j, k) = k2Null(i, j, k)
						extrapolatedsubNullsupj(i, j, k) = k2Null(i, j, k)
						extrapolatedsubNullsupk(i, j, k) = k2Null(i, j, k)
						extrapolatedsubfxsupi(i, j, k) = k2fx(i, j, k)
						extrapolatedsubfxsupj(i, j, k) = k2fx(i, j, k)
						extrapolatedsubfxsupk(i, j, k) = k2fx(i, j, k)
						extrapolatedsubfysupi(i, j, k) = k2fy(i, j, k)
						extrapolatedsubfysupj(i, j, k) = k2fy(i, j, k)
						extrapolatedsubfysupk(i, j, k) = k2fy(i, j, k)
						extrapolatedsubfzsupi(i, j, k) = k2fz(i, j, k)
						extrapolatedsubfzsupj(i, j, k) = k2fz(i, j, k)
						extrapolatedsubfzsupk(i, j, k) = k2fz(i, j, k)
						extrapolatedsubpsupi(i, j, k) = k2p(i, j, k)
						extrapolatedsubpsupj(i, j, k) = k2p(i, j, k)
						extrapolatedsubpsupk(i, j, k) = k2p(i, j, k)
					end if
					if ((FOV_zUpper(i, j, k) .gt. 0) .and. ((k - 1 .ge. 1 .and. (FOV_1(i, j, int(k - 1)) .gt. 0)) .or. (k - 2 .ge. 1 .and. (FOV_1(i, j, int(k - 2)) .gt. 0)) .or. (k - 3 .ge. 1 .and. (FOV_1(i, j, int(k - 3)) .gt. 0)))) then
						extrapolatedsubrhosupi(i, j, k) = k2rho(i, j, k)
						extrapolatedsubrhosupj(i, j, k) = k2rho(i, j, k)
						extrapolatedsubrhosupk(i, j, k) = k2rho(i, j, k)
						extrapolatedsubmxsupi(i, j, k) = k2mx(i, j, k)
						extrapolatedsubmxsupj(i, j, k) = k2mx(i, j, k)
						extrapolatedsubmxsupk(i, j, k) = k2mx(i, j, k)
						extrapolatedsubmysupi(i, j, k) = k2my(i, j, k)
						extrapolatedsubmysupj(i, j, k) = k2my(i, j, k)
						extrapolatedsubmysupk(i, j, k) = k2my(i, j, k)
						extrapolatedsubmzsupi(i, j, k) = k2mz(i, j, k)
						extrapolatedsubmzsupj(i, j, k) = k2mz(i, j, k)
						extrapolatedsubmzsupk(i, j, k) = k2mz(i, j, k)
						extrapolatedsubNullsupi(i, j, k) = k2Null(i, j, k)
						extrapolatedsubNullsupj(i, j, k) = k2Null(i, j, k)
						extrapolatedsubNullsupk(i, j, k) = k2Null(i, j, k)
						extrapolatedsubfxsupi(i, j, k) = k2fx(i, j, k)
						extrapolatedsubfxsupj(i, j, k) = k2fx(i, j, k)
						extrapolatedsubfxsupk(i, j, k) = k2fx(i, j, k)
						extrapolatedsubfysupi(i, j, k) = k2fy(i, j, k)
						extrapolatedsubfysupj(i, j, k) = k2fy(i, j, k)
						extrapolatedsubfysupk(i, j, k) = k2fy(i, j, k)
						extrapolatedsubfzsupi(i, j, k) = k2fz(i, j, k)
						extrapolatedsubfzsupj(i, j, k) = k2fz(i, j, k)
						extrapolatedsubfzsupk(i, j, k) = k2fz(i, j, k)
						extrapolatedsubpsupi(i, j, k) = k2p(i, j, k)
						extrapolatedsubpsupj(i, j, k) = k2p(i, j, k)
						extrapolatedsubpsupk(i, j, k) = k2p(i, j, k)
					end if
				end do
			end do
		end do
		call CCTK_SyncGroup(status, cctkGH, "FDAsNozzle3D::block8")
		call CCTK_SyncGroup(status, cctkGH, "FDAsNozzle3D::block4")
		do k = kStart, kEnd
			do j = jStart, jEnd
				do i = iStart, iEnd
					if ((FOV_3(i, j, k) .gt. 0 .or. (((i + 1 .le. cctk_lsh(1) .and. FOV_3(i + 1, j, k) .gt. 0) .or. (i + 2 .le. cctk_lsh(1) .and. FOV_3(i + 2, j, k) .gt. 0) .or. (j + 1 .le. cctk_lsh(2) .and. FOV_3(i, j + 1, k) .gt. 0) .or. (j + 2 .le. cctk_lsh(2) .and. FOV_3(i, j + 2, k) .gt. 0) .or. (k + 1 .le. cctk_lsh(3) .and. FOV_3(i, j, k + 1) .gt. 0) .or. (k + 2 .le. cctk_lsh(3) .and. FOV_3(i, j, k + 2) .gt. 0)) .or. ((i - 1 .ge. 1 .and. FOV_3(i - 1, j, k) .gt. 0) .or. (i - 2 .ge. 1 .and. FOV_3(i - 2, j, k) .gt. 0) .or. (j - 1 .ge. 1 .and. FOV_3(i, j - 1, k) .gt. 0) .or. (j - 2 .ge. 1 .and. FOV_3(i, j - 2, k) .gt. 0) .or. (k - 1 .ge. 1 .and. FOV_3(i, j, k - 1) .gt. 0) .or. (k - 2 .ge. 1 .and. FOV_3(i, j, k - 2) .gt. 0))))) then
					if ((((i .ge. 3) .and. (i .le. (cctk_lsh(1) - 3))) .and. ((j .ge. 3) .and. (j .le. (cctk_lsh(2) - 3)))) .and. ((k .ge. 3) .and. (k .le. (cctk_lsh(3) - 3)))) then
					Flux_extsubNullsupi_3(i, j, k) = 0.0d0
					Flux_extsubNullsupj_3(i, j, k) = 0.0d0
					Flux_extsubNullsupk_3(i, j, k) = 0.0d0
					end if
					Speedi_3(i, j, k) = ABS(1.0d0)
					Speedj_3(i, j, k) = ABS(1.0d0)
					Speedk_3(i, j, k) = ABS(1.0d0)
					end if
					if ((FOV_1(i, j, k) .gt. 0 .or. (((i + 1 .le. cctk_lsh(1) .and. FOV_1(i + 1, j, k) .gt. 0) .or. (i + 2 .le. cctk_lsh(1) .and. FOV_1(i + 2, j, k) .gt. 0) .or. (j + 1 .le. cctk_lsh(2) .and. FOV_1(i, j + 1, k) .gt. 0) .or. (j + 2 .le. cctk_lsh(2) .and. FOV_1(i, j + 2, k) .gt. 0) .or. (k + 1 .le. cctk_lsh(3) .and. FOV_1(i, j, k + 1) .gt. 0) .or. (k + 2 .le. cctk_lsh(3) .and. FOV_1(i, j, k + 2) .gt. 0)) .or. ((i - 1 .ge. 1 .and. FOV_1(i - 1, j, k) .gt. 0) .or. (i - 2 .ge. 1 .and. FOV_1(i - 2, j, k) .gt. 0) .or. (j - 1 .ge. 1 .and. FOV_1(i, j - 1, k) .gt. 0) .or. (j - 2 .ge. 1 .and. FOV_1(i, j - 2, k) .gt. 0) .or. (k - 1 .ge. 1 .and. FOV_1(i, j, k - 1) .gt. 0) .or. (k - 2 .ge. 1 .and. FOV_1(i, j, k - 2) .gt. 0)) .or. (((i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1) .and. FOV_1(int(i + 1), int(j - 1), k) .gt. 0) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2)) .and. FOV_1(int(i + 1), int(j + 1), k) .gt. 0) .or. ((i - 1 .ge. 1 .and. j - 1 .ge. 1) .and. FOV_1(int(i - 1), int(j - 1), k) .gt. 0) .or. ((i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2)) .and. FOV_1(int(i - 1), int(j + 1), k) .gt. 0) .or. ((i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1) .and. FOV_1(int(i + 1), int(j - 2), k) .gt. 0) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2)) .and. FOV_1(int(i + 1), int(j + 2), k) .gt. 0) .or. ((i - 1 .ge. 1 .and. j - 2 .ge. 1) .and. FOV_1(int(i - 1), int(j - 2), k) .gt. 0) .or. ((i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2)) .and. FOV_1(int(i - 1), int(j + 2), k) .gt. 0) .or. ((i + 1 .le. cctk_lsh(1) .and. k - 1 .ge. 1) .and. FOV_1(int(i + 1), j, int(k - 1)) .gt. 0) .or. ((i + 1 .le. cctk_lsh(1) .and. k + 1 .le. cctk_lsh(3)) .and. FOV_1(int(i + 1), j, int(k + 1)) .gt. 0) .or. ((i - 1 .ge. 1 .and. k - 1 .ge. 1) .and. FOV_1(int(i - 1), j, int(k - 1)) .gt. 0) .or. ((i - 1 .ge. 1 .and. k + 1 .le. cctk_lsh(3)) .and. FOV_1(int(i - 1), j, int(k + 1)) .gt. 0) .or. ((i + 1 .le. cctk_lsh(1) .and. k - 2 .ge. 1) .and. FOV_1(int(i + 1), j, int(k - 2)) .gt. 0) .or. ((i + 1 .le. cctk_lsh(1) .and. k + 2 .le. cctk_lsh(3)) .and. FOV_1(int(i + 1), j, int(k + 2)) .gt. 0) .or. ((i - 1 .ge. 1 .and. k - 2 .ge. 1) .and. FOV_1(int(i - 1), j, int(k - 2)) .gt. 0) .or. ((i - 1 .ge. 1 .and. k + 2 .le. cctk_lsh(3)) .and. FOV_1(int(i - 1), j, int(k + 2)) .gt. 0) .or. ((i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1) .and. FOV_1(int(i + 2), int(j - 1), k) .gt. 0) .or. ((i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2)) .and. FOV_1(int(i + 2), int(j + 1), k) .gt. 0) .or. ((i - 2 .ge. 1 .and. j - 1 .ge. 1) .and. FOV_1(int(i - 2), int(j - 1), k) .gt. 0) .or. ((i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2)) .and. FOV_1(int(i - 2), int(j + 1), k) .gt. 0) .or. ((i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1) .and. FOV_1(int(i + 2), int(j - 2), k) .gt. 0) .or. ((i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2)) .and. FOV_1(int(i + 2), int(j + 2), k) .gt. 0) .or. ((i - 2 .ge. 1 .and. j - 2 .ge. 1) .and. FOV_1(int(i - 2), int(j - 2), k) .gt. 0) .or. ((i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2)) .and. FOV_1(int(i - 2), int(j + 2), k) .gt. 0) .or. ((i + 2 .le. cctk_lsh(1) .and. k - 1 .ge. 1) .and. FOV_1(int(i + 2), j, int(k - 1)) .gt. 0) .or. ((i + 2 .le. cctk_lsh(1) .and. k + 1 .le. cctk_lsh(3)) .and. FOV_1(int(i + 2), j, int(k + 1)) .gt. 0) .or. ((i - 2 .ge. 1 .and. k - 1 .ge. 1) .and. FOV_1(int(i - 2), j, int(k - 1)) .gt. 0) .or. ((i - 2 .ge. 1 .and. k + 1 .le. cctk_lsh(3)) .and. FOV_1(int(i - 2), j, int(k + 1)) .gt. 0) .or. ((i + 2 .le. cctk_lsh(1) .and. k - 2 .ge. 1) .and. FOV_1(int(i + 2), j, int(k - 2)) .gt. 0) .or. ((i + 2 .le. cctk_lsh(1) .and. k + 2 .le. cctk_lsh(3)) .and. FOV_1(int(i + 2), j, int(k + 2)) .gt. 0) .or. ((i - 2 .ge. 1 .and. k - 2 .ge. 1) .and. FOV_1(int(i - 2), j, int(k - 2)) .gt. 0) .or. ((i - 2 .ge. 1 .and. k + 2 .le. cctk_lsh(3)) .and. FOV_1(int(i - 2), j, int(k + 2)) .gt. 0) .or. ((j + 1 .le. cctk_lsh(2) .and. k - 1 .ge. 1) .and. FOV_1(i, int(j + 1), int(k - 1)) .gt. 0) .or. ((j + 1 .le. cctk_lsh(2) .and. k + 1 .le. cctk_lsh(3)) .and. FOV_1(i, int(j + 1), int(k + 1)) .gt. 0) .or. ((j - 1 .ge. 1 .and. k - 1 .ge. 1) .and. FOV_1(i, int(j - 1), int(k - 1)) .gt. 0) .or. ((j - 1 .ge. 1 .and. k + 1 .le. cctk_lsh(3)) .and. FOV_1(i, int(j - 1), int(k + 1)) .gt. 0) .or. ((j + 1 .le. cctk_lsh(2) .and. k - 2 .ge. 1) .and. FOV_1(i, int(j + 1), int(k - 2)) .gt. 0) .or. ((j + 1 .le. cctk_lsh(2) .and. k + 2 .le. cctk_lsh(3)) .and. FOV_1(i, int(j + 1), int(k + 2)) .gt. 0) .or. ((j - 1 .ge. 1 .and. k - 2 .ge. 1) .and. FOV_1(i, int(j - 1), int(k - 2)) .gt. 0) .or. ((j - 1 .ge. 1 .and. k + 2 .le. cctk_lsh(3)) .and. FOV_1(i, int(j - 1), int(k + 2)) .gt. 0) .or. ((j + 2 .le. cctk_lsh(2) .and. k - 1 .ge. 1) .and. FOV_1(i, int(j + 2), int(k - 1)) .gt. 0) .or. ((j + 2 .le. cctk_lsh(2) .and. k + 1 .le. cctk_lsh(3)) .and. FOV_1(i, int(j + 2), int(k + 1)) .gt. 0) .or. ((j - 2 .ge. 1 .and. k - 1 .ge. 1) .and. FOV_1(i, int(j - 2), int(k - 1)) .gt. 0) .or. ((j - 2 .ge. 1 .and. k + 1 .le. cctk_lsh(3)) .and. FOV_1(i, int(j - 2), int(k + 1)) .gt. 0) .or. ((j + 2 .le. cctk_lsh(2) .and. k - 2 .ge. 1) .and. FOV_1(i, int(j + 2), int(k - 2)) .gt. 0) .or. ((j + 2 .le. cctk_lsh(2) .and. k + 2 .le. cctk_lsh(3)) .and. FOV_1(i, int(j + 2), int(k + 2)) .gt. 0) .or. ((j - 2 .ge. 1 .and. k - 2 .ge. 1) .and. FOV_1(i, int(j - 2), int(k - 2)) .gt. 0) .or. ((j - 2 .ge. 1 .and. k + 2 .le. cctk_lsh(3)) .and. FOV_1(i, int(j - 2), int(k + 2)) .gt. 0))))) then
					if ((((i .ge. 3) .and. (i .le. (cctk_lsh(1) - 3))) .and. ((j .ge. 3) .and. (j .le. (cctk_lsh(2) - 3)))) .and. ((k .ge. 3) .and. (k .le. (cctk_lsh(3) - 3)))) then
					Flux_extsubrhosupi_1(i, j, k) = Firho_PlaneI(CCTK_PASS_FTOF, extrapolatedsubmxsupi(i, j, k))
					Flux_extsubrhosupj_1(i, j, k) = Fjrho_PlaneI(CCTK_PASS_FTOF, extrapolatedsubmysupj(i, j, k))
					Flux_extsubrhosupk_1(i, j, k) = Fkrho_PlaneI(CCTK_PASS_FTOF, extrapolatedsubmzsupk(i, j, k))
					Flux_extsubmxsupi_1(i, j, k) = Fimx_PlaneI(CCTK_PASS_FTOF, extrapolatedsubrhosupi(i, j, k), extrapolatedsubmxsupi(i, j, k), extrapolatedsubpsupi(i, j, k))
					Flux_extsubmxsupj_1(i, j, k) = Fjmx_PlaneI(CCTK_PASS_FTOF, extrapolatedsubrhosupj(i, j, k), extrapolatedsubmxsupj(i, j, k), extrapolatedsubmysupj(i, j, k))
					Flux_extsubmxsupk_1(i, j, k) = Fkmx_PlaneI(CCTK_PASS_FTOF, extrapolatedsubrhosupk(i, j, k), extrapolatedsubmxsupk(i, j, k), extrapolatedsubmzsupk(i, j, k))
					Flux_extsubmysupi_1(i, j, k) = Fimy_PlaneI(CCTK_PASS_FTOF, extrapolatedsubrhosupi(i, j, k), extrapolatedsubmxsupi(i, j, k), extrapolatedsubmysupi(i, j, k))
					Flux_extsubmysupj_1(i, j, k) = Fjmy_PlaneI(CCTK_PASS_FTOF, extrapolatedsubrhosupj(i, j, k), extrapolatedsubmysupj(i, j, k), extrapolatedsubpsupj(i, j, k))
					Flux_extsubmysupk_1(i, j, k) = Fkmy_PlaneI(CCTK_PASS_FTOF, extrapolatedsubrhosupk(i, j, k), extrapolatedsubmysupk(i, j, k), extrapolatedsubmzsupk(i, j, k))
					Flux_extsubmzsupi_1(i, j, k) = Fimz_PlaneI(CCTK_PASS_FTOF, extrapolatedsubrhosupi(i, j, k), extrapolatedsubmxsupi(i, j, k), extrapolatedsubmzsupi(i, j, k))
					Flux_extsubmzsupj_1(i, j, k) = Fjmz_PlaneI(CCTK_PASS_FTOF, extrapolatedsubrhosupj(i, j, k), extrapolatedsubmysupj(i, j, k), extrapolatedsubmzsupj(i, j, k))
					Flux_extsubmzsupk_1(i, j, k) = Fkmz_PlaneI(CCTK_PASS_FTOF, extrapolatedsubrhosupk(i, j, k), extrapolatedsubmzsupk(i, j, k), extrapolatedsubpsupk(i, j, k))
					Q_ii_mx_1 = lambda
					Q_ij_mx_1 = lambda
					Q_ik_mx_1 = lambda
					Q_ii2_mx_1 = mu
					Q_jj_mx_1 = mu
					Q_kk_mx_1 = mu
					Q_ii3_mx_1 = mu
					Q_ji_mx_1 = mu
					Q_ki_mx_1 = mu
					Q_ji_my_1 = lambda
					Q_jj_my_1 = lambda
					Q_jk_my_1 = lambda
					Q_ii_my_1 = mu
					Q_jj2_my_1 = mu
					Q_kk_my_1 = mu
					Q_ij_my_1 = mu
					Q_jj3_my_1 = mu
					Q_kj_my_1 = mu
					Q_ki_mz_1 = lambda
					Q_kj_mz_1 = lambda
					Q_kk_mz_1 = lambda
					Q_ii_mz_1 = mu
					Q_jj_mz_1 = mu
					Q_kk2_mz_1 = mu
					Q_ik_mz_1 = mu
					Q_jk_mz_1 = mu
					Q_kk3_mz_1 = mu
					Pm2_ii_mx_1 = k2mx(i - 2, j, k) / k2rho(i - 2, j, k)
					Pm2_ij_mx_1 = k2my(i, j - 2, k) / k2rho(i, j - 2, k)
					Pm2_ik_mx_1 = k2mz(i, j, k - 2) / k2rho(i, j, k - 2)
					Pm2_ii2_mx_1 = k2mx(i - 2, j, k) / k2rho(i - 2, j, k)
					Pm2_jj_mx_1 = k2mx(i, j - 2, k) / k2rho(i, j - 2, k)
					Pm2_kk_mx_1 = k2mx(i, j, k - 2) / k2rho(i, j, k - 2)
					Pm2_ii3_mx_1 = k2mx(i - 2, j, k) / k2rho(i - 2, j, k)
					Pm2_ji_mx_1 = k2my(i - 2, j, k) / k2rho(i - 2, j, k)
					Pm2_ki_mx_1 = k2mz(i - 2, j, k) / k2rho(i - 2, j, k)
					Pm2_ji_my_1 = k2mx(i - 2, j, k) / k2rho(i - 2, j, k)
					Pm2_jj_my_1 = k2my(i, j - 2, k) / k2rho(i, j - 2, k)
					Pm2_jk_my_1 = k2mz(i, j, k - 2) / k2rho(i, j, k - 2)
					Pm2_ii_my_1 = k2my(i - 2, j, k) / k2rho(i - 2, j, k)
					Pm2_jj2_my_1 = k2my(i, j - 2, k) / k2rho(i, j - 2, k)
					Pm2_kk_my_1 = k2my(i, j, k - 2) / k2rho(i, j, k - 2)
					Pm2_ij_my_1 = k2mx(i, j - 2, k) / k2rho(i, j - 2, k)
					Pm2_jj3_my_1 = k2my(i, j - 2, k) / k2rho(i, j - 2, k)
					Pm2_kj_my_1 = k2mz(i, j - 2, k) / k2rho(i, j - 2, k)
					Pm2_ki_mz_1 = k2mx(i - 2, j, k) / k2rho(i - 2, j, k)
					Pm2_kj_mz_1 = k2my(i, j - 2, k) / k2rho(i, j - 2, k)
					Pm2_kk_mz_1 = k2mz(i, j, k - 2) / k2rho(i, j, k - 2)
					Pm2_ii_mz_1 = k2mz(i - 2, j, k) / k2rho(i - 2, j, k)
					Pm2_jj_mz_1 = k2mz(i, j - 2, k) / k2rho(i, j - 2, k)
					Pm2_kk2_mz_1 = k2mz(i, j, k - 2) / k2rho(i, j, k - 2)
					Pm2_ik_mz_1 = k2mx(i, j, k - 2) / k2rho(i, j, k - 2)
					Pm2_jk_mz_1 = k2my(i, j, k - 2) / k2rho(i, j, k - 2)
					Pm2_kk3_mz_1 = k2mz(i, j, k - 2) / k2rho(i, j, k - 2)
					Pm1_ii_mx_1 = k2mx(i - 1, j, k) / k2rho(i - 1, j, k)
					Pm1_ij_mx_1 = k2my(i, j - 1, k) / k2rho(i, j - 1, k)
					Pm1_ik_mx_1 = k2mz(i, j, k - 1) / k2rho(i, j, k - 1)
					Pm1_ii2_mx_1 = k2mx(i - 1, j, k) / k2rho(i - 1, j, k)
					Pm1_jj_mx_1 = k2mx(i, j - 1, k) / k2rho(i, j - 1, k)
					Pm1_kk_mx_1 = k2mx(i, j, k - 1) / k2rho(i, j, k - 1)
					Pm1_ii3_mx_1 = k2mx(i - 1, j, k) / k2rho(i - 1, j, k)
					Pm1_ji_mx_1 = k2my(i - 1, j, k) / k2rho(i - 1, j, k)
					Pm1_ki_mx_1 = k2mz(i - 1, j, k) / k2rho(i - 1, j, k)
					Pm1_ji_my_1 = k2mx(i - 1, j, k) / k2rho(i - 1, j, k)
					Pm1_jj_my_1 = k2my(i, j - 1, k) / k2rho(i, j - 1, k)
					Pm1_jk_my_1 = k2mz(i, j, k - 1) / k2rho(i, j, k - 1)
					Pm1_ii_my_1 = k2my(i - 1, j, k) / k2rho(i - 1, j, k)
					Pm1_jj2_my_1 = k2my(i, j - 1, k) / k2rho(i, j - 1, k)
					Pm1_kk_my_1 = k2my(i, j, k - 1) / k2rho(i, j, k - 1)
					Pm1_ij_my_1 = k2mx(i, j - 1, k) / k2rho(i, j - 1, k)
					Pm1_jj3_my_1 = k2my(i, j - 1, k) / k2rho(i, j - 1, k)
					Pm1_kj_my_1 = k2mz(i, j - 1, k) / k2rho(i, j - 1, k)
					Pm1_ki_mz_1 = k2mx(i - 1, j, k) / k2rho(i - 1, j, k)
					Pm1_kj_mz_1 = k2my(i, j - 1, k) / k2rho(i, j - 1, k)
					Pm1_kk_mz_1 = k2mz(i, j, k - 1) / k2rho(i, j, k - 1)
					Pm1_ii_mz_1 = k2mz(i - 1, j, k) / k2rho(i - 1, j, k)
					Pm1_jj_mz_1 = k2mz(i, j - 1, k) / k2rho(i, j - 1, k)
					Pm1_kk2_mz_1 = k2mz(i, j, k - 1) / k2rho(i, j, k - 1)
					Pm1_ik_mz_1 = k2mx(i, j, k - 1) / k2rho(i, j, k - 1)
					Pm1_jk_mz_1 = k2my(i, j, k - 1) / k2rho(i, j, k - 1)
					Pm1_kk3_mz_1 = k2mz(i, j, k - 1) / k2rho(i, j, k - 1)
					Pp1_ii_mx_1 = k2mx(i + 1, j, k) / k2rho(i + 1, j, k)
					Pp1_ij_mx_1 = k2my(i, j + 1, k) / k2rho(i, j + 1, k)
					Pp1_ik_mx_1 = k2mz(i, j, k + 1) / k2rho(i, j, k + 1)
					Pp1_ii2_mx_1 = k2mx(i + 1, j, k) / k2rho(i + 1, j, k)
					Pp1_jj_mx_1 = k2mx(i, j + 1, k) / k2rho(i, j + 1, k)
					Pp1_kk_mx_1 = k2mx(i, j, k + 1) / k2rho(i, j, k + 1)
					Pp1_ii3_mx_1 = k2mx(i + 1, j, k) / k2rho(i + 1, j, k)
					Pp1_ji_mx_1 = k2my(i + 1, j, k) / k2rho(i + 1, j, k)
					Pp1_ki_mx_1 = k2mz(i + 1, j, k) / k2rho(i + 1, j, k)
					Pp1_ji_my_1 = k2mx(i + 1, j, k) / k2rho(i + 1, j, k)
					Pp1_jj_my_1 = k2my(i, j + 1, k) / k2rho(i, j + 1, k)
					Pp1_jk_my_1 = k2mz(i, j, k + 1) / k2rho(i, j, k + 1)
					Pp1_ii_my_1 = k2my(i + 1, j, k) / k2rho(i + 1, j, k)
					Pp1_jj2_my_1 = k2my(i, j + 1, k) / k2rho(i, j + 1, k)
					Pp1_kk_my_1 = k2my(i, j, k + 1) / k2rho(i, j, k + 1)
					Pp1_ij_my_1 = k2mx(i, j + 1, k) / k2rho(i, j + 1, k)
					Pp1_jj3_my_1 = k2my(i, j + 1, k) / k2rho(i, j + 1, k)
					Pp1_kj_my_1 = k2mz(i, j + 1, k) / k2rho(i, j + 1, k)
					Pp1_ki_mz_1 = k2mx(i + 1, j, k) / k2rho(i + 1, j, k)
					Pp1_kj_mz_1 = k2my(i, j + 1, k) / k2rho(i, j + 1, k)
					Pp1_kk_mz_1 = k2mz(i, j, k + 1) / k2rho(i, j, k + 1)
					Pp1_ii_mz_1 = k2mz(i + 1, j, k) / k2rho(i + 1, j, k)
					Pp1_jj_mz_1 = k2mz(i, j + 1, k) / k2rho(i, j + 1, k)
					Pp1_kk2_mz_1 = k2mz(i, j, k + 1) / k2rho(i, j, k + 1)
					Pp1_ik_mz_1 = k2mx(i, j, k + 1) / k2rho(i, j, k + 1)
					Pp1_jk_mz_1 = k2my(i, j, k + 1) / k2rho(i, j, k + 1)
					Pp1_kk3_mz_1 = k2mz(i, j, k + 1) / k2rho(i, j, k + 1)
					Pp2_ii_mx_1 = k2mx(i + 2, j, k) / k2rho(i + 2, j, k)
					Pp2_ij_mx_1 = k2my(i, j + 2, k) / k2rho(i, j + 2, k)
					Pp2_ik_mx_1 = k2mz(i, j, k + 2) / k2rho(i, j, k + 2)
					Pp2_ii2_mx_1 = k2mx(i + 2, j, k) / k2rho(i + 2, j, k)
					Pp2_jj_mx_1 = k2mx(i, j + 2, k) / k2rho(i, j + 2, k)
					Pp2_kk_mx_1 = k2mx(i, j, k + 2) / k2rho(i, j, k + 2)
					Pp2_ii3_mx_1 = k2mx(i + 2, j, k) / k2rho(i + 2, j, k)
					Pp2_ji_mx_1 = k2my(i + 2, j, k) / k2rho(i + 2, j, k)
					Pp2_ki_mx_1 = k2mz(i + 2, j, k) / k2rho(i + 2, j, k)
					Pp2_ji_my_1 = k2mx(i + 2, j, k) / k2rho(i + 2, j, k)
					Pp2_jj_my_1 = k2my(i, j + 2, k) / k2rho(i, j + 2, k)
					Pp2_jk_my_1 = k2mz(i, j, k + 2) / k2rho(i, j, k + 2)
					Pp2_ii_my_1 = k2my(i + 2, j, k) / k2rho(i + 2, j, k)
					Pp2_jj2_my_1 = k2my(i, j + 2, k) / k2rho(i, j + 2, k)
					Pp2_kk_my_1 = k2my(i, j, k + 2) / k2rho(i, j, k + 2)
					Pp2_ij_my_1 = k2mx(i, j + 2, k) / k2rho(i, j + 2, k)
					Pp2_jj3_my_1 = k2my(i, j + 2, k) / k2rho(i, j + 2, k)
					Pp2_kj_my_1 = k2mz(i, j + 2, k) / k2rho(i, j + 2, k)
					Pp2_ki_mz_1 = k2mx(i + 2, j, k) / k2rho(i + 2, j, k)
					Pp2_kj_mz_1 = k2my(i, j + 2, k) / k2rho(i, j + 2, k)
					Pp2_kk_mz_1 = k2mz(i, j, k + 2) / k2rho(i, j, k + 2)
					Pp2_ii_mz_1 = k2mz(i + 2, j, k) / k2rho(i + 2, j, k)
					Pp2_jj_mz_1 = k2mz(i, j + 2, k) / k2rho(i, j + 2, k)
					Pp2_kk2_mz_1 = k2mz(i, j, k + 2) / k2rho(i, j, k + 2)
					Pp2_ik_mz_1 = k2mx(i, j, k + 2) / k2rho(i, j, k + 2)
					Pp2_jk_mz_1 = k2my(i, j, k + 2) / k2rho(i, j, k + 2)
					Pp2_kk3_mz_1 = k2mz(i, j, k + 2) / k2rho(i, j, k + 2)
					Flux_extsubmxsupi_1(i, j, k) = Flux_extsubmxsupi_1(i, j, k) - Q_ii_mx_1 * CDi(CCTK_PASS_FTOF, Pm2_ii_mx_1, Pm1_ii_mx_1, Pp1_ii_mx_1, Pp2_ii_mx_1)
					Flux_extsubmxsupi_1(i, j, k) = Flux_extsubmxsupi_1(i, j, k) - Q_ij_mx_1 * CDj(CCTK_PASS_FTOF, Pm2_ij_mx_1, Pm1_ij_mx_1, Pp1_ij_mx_1, Pp2_ij_mx_1)
					Flux_extsubmxsupi_1(i, j, k) = Flux_extsubmxsupi_1(i, j, k) - Q_ik_mx_1 * CDk(CCTK_PASS_FTOF, Pm2_ik_mx_1, Pm1_ik_mx_1, Pp1_ik_mx_1, Pp2_ik_mx_1)
					Flux_extsubmxsupi_1(i, j, k) = Flux_extsubmxsupi_1(i, j, k) - Q_ii2_mx_1 * CDi(CCTK_PASS_FTOF, Pm2_ii2_mx_1, Pm1_ii2_mx_1, Pp1_ii2_mx_1, Pp2_ii2_mx_1)
					Flux_extsubmxsupj_1(i, j, k) = Flux_extsubmxsupj_1(i, j, k) - Q_jj_mx_1 * CDj(CCTK_PASS_FTOF, Pm2_jj_mx_1, Pm1_jj_mx_1, Pp1_jj_mx_1, Pp2_jj_mx_1)
					Flux_extsubmxsupk_1(i, j, k) = Flux_extsubmxsupk_1(i, j, k) - Q_kk_mx_1 * CDk(CCTK_PASS_FTOF, Pm2_kk_mx_1, Pm1_kk_mx_1, Pp1_kk_mx_1, Pp2_kk_mx_1)
					Flux_extsubmxsupi_1(i, j, k) = Flux_extsubmxsupi_1(i, j, k) - Q_ii3_mx_1 * CDi(CCTK_PASS_FTOF, Pm2_ii3_mx_1, Pm1_ii3_mx_1, Pp1_ii3_mx_1, Pp2_ii3_mx_1)
					Flux_extsubmxsupj_1(i, j, k) = Flux_extsubmxsupj_1(i, j, k) - Q_ji_mx_1 * CDi(CCTK_PASS_FTOF, Pm2_ji_mx_1, Pm1_ji_mx_1, Pp1_ji_mx_1, Pp2_ji_mx_1)
					Flux_extsubmxsupk_1(i, j, k) = Flux_extsubmxsupk_1(i, j, k) - Q_ki_mx_1 * CDi(CCTK_PASS_FTOF, Pm2_ki_mx_1, Pm1_ki_mx_1, Pp1_ki_mx_1, Pp2_ki_mx_1)
					Flux_extsubmysupj_1(i, j, k) = Flux_extsubmysupj_1(i, j, k) - Q_ji_my_1 * CDi(CCTK_PASS_FTOF, Pm2_ji_my_1, Pm1_ji_my_1, Pp1_ji_my_1, Pp2_ji_my_1)
					Flux_extsubmysupj_1(i, j, k) = Flux_extsubmysupj_1(i, j, k) - Q_jj_my_1 * CDj(CCTK_PASS_FTOF, Pm2_jj_my_1, Pm1_jj_my_1, Pp1_jj_my_1, Pp2_jj_my_1)
					Flux_extsubmysupj_1(i, j, k) = Flux_extsubmysupj_1(i, j, k) - Q_jk_my_1 * CDk(CCTK_PASS_FTOF, Pm2_jk_my_1, Pm1_jk_my_1, Pp1_jk_my_1, Pp2_jk_my_1)
					Flux_extsubmysupi_1(i, j, k) = Flux_extsubmysupi_1(i, j, k) - Q_ii_my_1 * CDi(CCTK_PASS_FTOF, Pm2_ii_my_1, Pm1_ii_my_1, Pp1_ii_my_1, Pp2_ii_my_1)
					Flux_extsubmysupj_1(i, j, k) = Flux_extsubmysupj_1(i, j, k) - Q_jj2_my_1 * CDj(CCTK_PASS_FTOF, Pm2_jj2_my_1, Pm1_jj2_my_1, Pp1_jj2_my_1, Pp2_jj2_my_1)
					Flux_extsubmysupk_1(i, j, k) = Flux_extsubmysupk_1(i, j, k) - Q_kk_my_1 * CDk(CCTK_PASS_FTOF, Pm2_kk_my_1, Pm1_kk_my_1, Pp1_kk_my_1, Pp2_kk_my_1)
					Flux_extsubmysupi_1(i, j, k) = Flux_extsubmysupi_1(i, j, k) - Q_ij_my_1 * CDj(CCTK_PASS_FTOF, Pm2_ij_my_1, Pm1_ij_my_1, Pp1_ij_my_1, Pp2_ij_my_1)
					Flux_extsubmysupj_1(i, j, k) = Flux_extsubmysupj_1(i, j, k) - Q_jj3_my_1 * CDj(CCTK_PASS_FTOF, Pm2_jj3_my_1, Pm1_jj3_my_1, Pp1_jj3_my_1, Pp2_jj3_my_1)
					Flux_extsubmysupk_1(i, j, k) = Flux_extsubmysupk_1(i, j, k) - Q_kj_my_1 * CDj(CCTK_PASS_FTOF, Pm2_kj_my_1, Pm1_kj_my_1, Pp1_kj_my_1, Pp2_kj_my_1)
					Flux_extsubmzsupk_1(i, j, k) = Flux_extsubmzsupk_1(i, j, k) - Q_ki_mz_1 * CDi(CCTK_PASS_FTOF, Pm2_ki_mz_1, Pm1_ki_mz_1, Pp1_ki_mz_1, Pp2_ki_mz_1)
					Flux_extsubmzsupk_1(i, j, k) = Flux_extsubmzsupk_1(i, j, k) - Q_kj_mz_1 * CDj(CCTK_PASS_FTOF, Pm2_kj_mz_1, Pm1_kj_mz_1, Pp1_kj_mz_1, Pp2_kj_mz_1)
					Flux_extsubmzsupk_1(i, j, k) = Flux_extsubmzsupk_1(i, j, k) - Q_kk_mz_1 * CDk(CCTK_PASS_FTOF, Pm2_kk_mz_1, Pm1_kk_mz_1, Pp1_kk_mz_1, Pp2_kk_mz_1)
					Flux_extsubmzsupi_1(i, j, k) = Flux_extsubmzsupi_1(i, j, k) - Q_ii_mz_1 * CDi(CCTK_PASS_FTOF, Pm2_ii_mz_1, Pm1_ii_mz_1, Pp1_ii_mz_1, Pp2_ii_mz_1)
					Flux_extsubmzsupj_1(i, j, k) = Flux_extsubmzsupj_1(i, j, k) - Q_jj_mz_1 * CDj(CCTK_PASS_FTOF, Pm2_jj_mz_1, Pm1_jj_mz_1, Pp1_jj_mz_1, Pp2_jj_mz_1)
					Flux_extsubmzsupk_1(i, j, k) = Flux_extsubmzsupk_1(i, j, k) - Q_kk2_mz_1 * CDk(CCTK_PASS_FTOF, Pm2_kk2_mz_1, Pm1_kk2_mz_1, Pp1_kk2_mz_1, Pp2_kk2_mz_1)
					Flux_extsubmzsupi_1(i, j, k) = Flux_extsubmzsupi_1(i, j, k) - Q_ik_mz_1 * CDk(CCTK_PASS_FTOF, Pm2_ik_mz_1, Pm1_ik_mz_1, Pp1_ik_mz_1, Pp2_ik_mz_1)
					Flux_extsubmzsupj_1(i, j, k) = Flux_extsubmzsupj_1(i, j, k) - Q_jk_mz_1 * CDk(CCTK_PASS_FTOF, Pm2_jk_mz_1, Pm1_jk_mz_1, Pp1_jk_mz_1, Pp2_jk_mz_1)
					Flux_extsubmzsupk_1(i, j, k) = Flux_extsubmzsupk_1(i, j, k) - Q_kk3_mz_1 * CDk(CCTK_PASS_FTOF, Pm2_kk3_mz_1, Pm1_kk3_mz_1, Pp1_kk3_mz_1, Pp2_kk3_mz_1)
					end if
					m_n_1 = extrapolatedsubmxsupi(i, j, k)
					m_tx_1 = 0.0d0
					m_ty_1 = extrapolatedsubmysupi(i, j, k)
					m_tz_1 = extrapolatedsubmzsupi(i, j, k)
					c_x_1 = c0 * ((extrapolatedsubrhosupi(i, j, k) / rho0) ** ((gamma + (-1.0d0)) / 2.0d0))
					cn_x_1 = m_n_1 / extrapolatedsubrhosupi(i, j, k)
					m_n_1 = extrapolatedsubmysupj(i, j, k)
					m_tx_1 = extrapolatedsubmxsupj(i, j, k)
					m_ty_1 = 0.0d0
					m_tz_1 = extrapolatedsubmzsupj(i, j, k)
					c_y_1 = c0 * ((extrapolatedsubrhosupj(i, j, k) / rho0) ** ((gamma + (-1.0d0)) / 2.0d0))
					cn_y_1 = m_n_1 / extrapolatedsubrhosupj(i, j, k)
					m_n_1 = extrapolatedsubmzsupk(i, j, k)
					m_tx_1 = extrapolatedsubmxsupk(i, j, k)
					m_ty_1 = extrapolatedsubmysupk(i, j, k)
					m_tz_1 = 0.0d0
					c_z_1 = c0 * ((extrapolatedsubrhosupk(i, j, k) / rho0) ** ((gamma + (-1.0d0)) / 2.0d0))
					cn_z_1 = m_n_1 / extrapolatedsubrhosupk(i, j, k)
					Speedi_1(i, j, k) = MAX(ABS(c_x_1 + cn_x_1), ABS((-c_x_1) + cn_x_1), ABS(cn_x_1), ABS(cn_x_1), ABS(cn_x_1))
					Speedj_1(i, j, k) = MAX(ABS(c_y_1 + cn_y_1), ABS((-c_y_1) + cn_y_1), ABS(cn_y_1), ABS(cn_y_1), ABS(cn_y_1))
					Speedk_1(i, j, k) = MAX(ABS(c_z_1 + cn_z_1), ABS((-c_z_1) + cn_z_1), ABS(cn_z_1), ABS(cn_z_1), ABS(cn_z_1))
					end if
				end do
			end do
		end do
		do k = kStart, kEnd
			do j = jStart, jEnd
				do i = iStart, iEnd
!					Boundaries
		if ((FOV_xLower(i, j, k) .gt. 0) .and. ((i + 1 .le. cctk_lsh(1) .and. (FOV_3(int(i + 1), j, k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. (FOV_3(int(i + 2), j, k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. (FOV_3(int(i + 3), j, k) .gt. 0)))) then
				Flux_extsubNullsupi_3(4 - i, j, k) = 2.0d0 * Flux_extsubNullsupi_3(4 - i + 1, j, k) - Flux_extsubNullsupi_3(4 - i + 2, j, k)
		end if
		if ((FOV_xUpper(i, j, k) .gt. 0) .and. ((i - 1 .ge. 1 .and. (FOV_3(int(i - 1), j, k) .gt. 0)) .or. (i - 2 .ge. 1 .and. (FOV_3(int(i - 2), j, k) .gt. 0)) .or. (i - 3 .ge. 1 .and. (FOV_3(int(i - 3), j, k) .gt. 0)))) then
			Flux_extsubNullsupi_3(i, j, k) = 2.0d0 * Flux_extsubNullsupi_3(i - 1, j, k) - Flux_extsubNullsupi_3(i - 2, j, k)
		end if
		if ((FOV_yLower(i, j, k) .gt. 0) .and. ((j + 1 .le. cctk_lsh(2) .and. (FOV_3(i, int(j + 1), k) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. (FOV_3(i, int(j + 2), k) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. (FOV_3(i, int(j + 3), k) .gt. 0)))) then
				Flux_extsubNullsupj_3(i, 4 - j, k) = 2.0d0 * Flux_extsubNullsupj_3(i, 4 - j + 1, k) - Flux_extsubNullsupj_3(i, 4 - j + 2, k)
		end if
		if ((FOV_yUpper(i, j, k) .gt. 0) .and. ((j - 1 .ge. 1 .and. (FOV_3(i, int(j - 1), k) .gt. 0)) .or. (j - 2 .ge. 1 .and. (FOV_3(i, int(j - 2), k) .gt. 0)) .or. (j - 3 .ge. 1 .and. (FOV_3(i, int(j - 3), k) .gt. 0)))) then
			Flux_extsubNullsupj_3(i, j, k) = 2.0d0 * Flux_extsubNullsupj_3(i, j - 1, k) - Flux_extsubNullsupj_3(i, j - 2, k)
		end if
		if ((FOV_zLower(i, j, k) .gt. 0) .and. ((k + 1 .le. cctk_lsh(3) .and. (FOV_3(i, j, int(k + 1)) .gt. 0)) .or. (k + 2 .le. cctk_lsh(3) .and. (FOV_3(i, j, int(k + 2)) .gt. 0)) .or. (k + 3 .le. cctk_lsh(3) .and. (FOV_3(i, j, int(k + 3)) .gt. 0)))) then
				Flux_extsubNullsupk_3(i, j, 4 - k) = 2.0d0 * Flux_extsubNullsupk_3(i, j, 4 - k + 1) - Flux_extsubNullsupk_3(i, j, 4 - k + 2)
		end if
		if ((FOV_zUpper(i, j, k) .gt. 0) .and. ((k - 1 .ge. 1 .and. (FOV_3(i, j, int(k - 1)) .gt. 0)) .or. (k - 2 .ge. 1 .and. (FOV_3(i, j, int(k - 2)) .gt. 0)) .or. (k - 3 .ge. 1 .and. (FOV_3(i, j, int(k - 3)) .gt. 0)))) then
			Flux_extsubNullsupk_3(i, j, k) = 2.0d0 * Flux_extsubNullsupk_3(i, j, k - 1) - Flux_extsubNullsupk_3(i, j, k - 2)
		end if
		if (FOV_3(i, j, k) .gt. 0) then
			if ((d_i_rho(i, j, k) .ne. 0 .or. d_j_rho(i, j, k) .ne. 0 .or. d_k_rho(i, j, k) .ne. 0)) then
				if (FOV_1(int(i - d_i_rho(i, j, k)), int(j - d_j_rho(i, j, k)), int(k - d_k_rho(i, j, k))) .gt. 0) then
					call extrapolate_flux(CCTK_PASS_FTOF, d_i_rho, Flux_extsubrhosupi_1, Flux_extsubrhosupi_1, i, d_j_rho, Flux_extsubrhosupj_1, Flux_extsubrhosupj_1, j, d_k_rho, Flux_extsubrhosupk_1, Flux_extsubrhosupk_1, k, FOV_1)
				end if
			end if
			if ((d_i_mx(i, j, k) .ne. 0 .or. d_j_mx(i, j, k) .ne. 0 .or. d_k_mx(i, j, k) .ne. 0)) then
				if (FOV_1(int(i - d_i_mx(i, j, k)), int(j - d_j_mx(i, j, k)), int(k - d_k_mx(i, j, k))) .gt. 0) then
					call extrapolate_flux(CCTK_PASS_FTOF, d_i_mx, Flux_extsubmxsupi_1, Flux_extsubmxsupi_1, i, d_j_mx, Flux_extsubmxsupj_1, Flux_extsubmxsupj_1, j, d_k_mx, Flux_extsubmxsupk_1, Flux_extsubmxsupk_1, k, FOV_1)
				end if
			end if
			if ((d_i_my(i, j, k) .ne. 0 .or. d_j_my(i, j, k) .ne. 0 .or. d_k_my(i, j, k) .ne. 0)) then
				if (FOV_1(int(i - d_i_my(i, j, k)), int(j - d_j_my(i, j, k)), int(k - d_k_my(i, j, k))) .gt. 0) then
					call extrapolate_flux(CCTK_PASS_FTOF, d_i_my, Flux_extsubmysupi_1, Flux_extsubmysupi_1, i, d_j_my, Flux_extsubmysupj_1, Flux_extsubmysupj_1, j, d_k_my, Flux_extsubmysupk_1, Flux_extsubmysupk_1, k, FOV_1)
				end if
			end if
			if ((d_i_mz(i, j, k) .ne. 0 .or. d_j_mz(i, j, k) .ne. 0 .or. d_k_mz(i, j, k) .ne. 0)) then
				if (FOV_1(int(i - d_i_mz(i, j, k)), int(j - d_j_mz(i, j, k)), int(k - d_k_mz(i, j, k))) .gt. 0) then
					call extrapolate_flux(CCTK_PASS_FTOF, d_i_mz, Flux_extsubmzsupi_1, Flux_extsubmzsupi_1, i, d_j_mz, Flux_extsubmzsupj_1, Flux_extsubmzsupj_1, j, d_k_mz, Flux_extsubmzsupk_1, Flux_extsubmzsupk_1, k, FOV_1)
				end if
			end if
		end if
!					Boundaries
		if ((FOV_xLower(i, j, k) .gt. 0) .and. ((i + 1 .le. cctk_lsh(1) .and. (FOV_1(int(i + 1), j, k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. (FOV_1(int(i + 2), j, k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. (FOV_1(int(i + 3), j, k) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 1), k) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 1), k) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 2), k) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 2), k) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 3), k) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 3), k) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(int(i + 1), j, int(k + 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. k - 1 .ge. 1 .and. (FOV_1(int(i + 1), j, int(k - 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(int(i + 1), j, int(k + 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. k - 2 .ge. 1 .and. (FOV_1(int(i + 1), j, int(k - 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(int(i + 1), j, int(k + 3)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. k - 3 .ge. 1 .and. (FOV_1(int(i + 1), j, int(k - 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 1), k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 1), k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 2), k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 2), k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 3), k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 3), k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(int(i + 2), j, int(k + 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. k - 1 .ge. 1 .and. (FOV_1(int(i + 2), j, int(k - 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(int(i + 2), j, int(k + 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. k - 2 .ge. 1 .and. (FOV_1(int(i + 2), j, int(k - 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(int(i + 2), j, int(k + 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. k - 3 .ge. 1 .and. (FOV_1(int(i + 2), j, int(k - 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 1), k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 1), k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 2), k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 2), k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 3), k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 3), k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(int(i + 3), j, int(k + 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. k - 1 .ge. 1 .and. (FOV_1(int(i + 3), j, int(k - 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(int(i + 3), j, int(k + 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. k - 2 .ge. 1 .and. (FOV_1(int(i + 3), j, int(k - 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(int(i + 3), j, int(k + 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. k - 3 .ge. 1 .and. (FOV_1(int(i + 3), j, int(k - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j, k) .ne. 0 .or. d_j_rho(i, j, k) .ne. 0 .or. d_k_rho(i, j, k) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_rho, Flux_extsubrhosupi_1, Flux_extsubrhosupi_1, i, d_j_rho, Flux_extsubrhosupj_1, Flux_extsubrhosupj_1, j, d_k_rho, Flux_extsubrhosupk_1, Flux_extsubrhosupk_1, k, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j, k) .ne. 0 .or. d_j_mx(i, j, k) .ne. 0 .or. d_k_mx(i, j, k) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_mx, Flux_extsubmxsupi_1, Flux_extsubmxsupi_1, i, d_j_mx, Flux_extsubmxsupj_1, Flux_extsubmxsupj_1, j, d_k_mx, Flux_extsubmxsupk_1, Flux_extsubmxsupk_1, k, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j, k) .ne. 0 .or. d_j_my(i, j, k) .ne. 0 .or. d_k_my(i, j, k) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_my, Flux_extsubmysupi_1, Flux_extsubmysupi_1, i, d_j_my, Flux_extsubmysupj_1, Flux_extsubmysupj_1, j, d_k_my, Flux_extsubmysupk_1, Flux_extsubmysupk_1, k, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mz(i, j, k) .ne. 0 .or. d_j_mz(i, j, k) .ne. 0 .or. d_k_mz(i, j, k) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_mz, Flux_extsubmzsupi_1, Flux_extsubmzsupi_1, i, d_j_mz, Flux_extsubmzsupj_1, Flux_extsubmzsupj_1, j, d_k_mz, Flux_extsubmzsupk_1, Flux_extsubmzsupk_1, k, FOV_1)
			end if
		end if
		if ((FOV_xUpper(i, j, k) .gt. 0) .and. ((i - 1 .ge. 1 .and. (FOV_1(int(i - 1), j, k) .gt. 0)) .or. (i - 2 .ge. 1 .and. (FOV_1(int(i - 2), j, k) .gt. 0)) .or. (i - 3 .ge. 1 .and. (FOV_1(int(i - 3), j, k) .gt. 0)) .or. ((i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 1), k) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 1), k) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 2), k) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 2), k) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 3), k) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 3), k) .gt. 0)) .or. (i - 1 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(int(i - 1), j, int(k + 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. k - 1 .ge. 1 .and. (FOV_1(int(i - 1), j, int(k - 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(int(i - 1), j, int(k + 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. k - 2 .ge. 1 .and. (FOV_1(int(i - 1), j, int(k - 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(int(i - 1), j, int(k + 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. k - 3 .ge. 1 .and. (FOV_1(int(i - 1), j, int(k - 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 1), k) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 1), k) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 2), k) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 2), k) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 3), k) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 3), k) .gt. 0)) .or. (i - 2 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(int(i - 2), j, int(k + 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. k - 1 .ge. 1 .and. (FOV_1(int(i - 2), j, int(k - 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(int(i - 2), j, int(k + 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. k - 2 .ge. 1 .and. (FOV_1(int(i - 2), j, int(k - 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(int(i - 2), j, int(k + 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. k - 3 .ge. 1 .and. (FOV_1(int(i - 2), j, int(k - 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 1), k) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 1), k) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 2), k) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 2), k) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 3), k) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 3), k) .gt. 0)) .or. (i - 3 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(int(i - 3), j, int(k + 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. k - 1 .ge. 1 .and. (FOV_1(int(i - 3), j, int(k - 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(int(i - 3), j, int(k + 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. k - 2 .ge. 1 .and. (FOV_1(int(i - 3), j, int(k - 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(int(i - 3), j, int(k + 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. k - 3 .ge. 1 .and. (FOV_1(int(i - 3), j, int(k - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j, k) .ne. 0 .or. d_j_rho(i, j, k) .ne. 0 .or. d_k_rho(i, j, k) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_rho, Flux_extsubrhosupi_1, Flux_extsubrhosupi_1, i, d_j_rho, Flux_extsubrhosupj_1, Flux_extsubrhosupj_1, j, d_k_rho, Flux_extsubrhosupk_1, Flux_extsubrhosupk_1, k, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j, k) .ne. 0 .or. d_j_mx(i, j, k) .ne. 0 .or. d_k_mx(i, j, k) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_mx, Flux_extsubmxsupi_1, Flux_extsubmxsupi_1, i, d_j_mx, Flux_extsubmxsupj_1, Flux_extsubmxsupj_1, j, d_k_mx, Flux_extsubmxsupk_1, Flux_extsubmxsupk_1, k, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j, k) .ne. 0 .or. d_j_my(i, j, k) .ne. 0 .or. d_k_my(i, j, k) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_my, Flux_extsubmysupi_1, Flux_extsubmysupi_1, i, d_j_my, Flux_extsubmysupj_1, Flux_extsubmysupj_1, j, d_k_my, Flux_extsubmysupk_1, Flux_extsubmysupk_1, k, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mz(i, j, k) .ne. 0 .or. d_j_mz(i, j, k) .ne. 0 .or. d_k_mz(i, j, k) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_mz, Flux_extsubmzsupi_1, Flux_extsubmzsupi_1, i, d_j_mz, Flux_extsubmzsupj_1, Flux_extsubmzsupj_1, j, d_k_mz, Flux_extsubmzsupk_1, Flux_extsubmzsupk_1, k, FOV_1)
			end if
		end if
		if ((FOV_yLower(i, j, k) .gt. 0) .and. ((j + 1 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 1), k) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 2), k) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 3), k) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 1), k) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 1), k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 1), k) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 1), k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 1), k) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 1), k) .gt. 0)) .or. (j + 1 .le. cctk_lsh(2) .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 1), int(k + 1)) .gt. 0)) .or. (j + 1 .le. cctk_lsh(2) .and. k - 1 .ge. 1 .and. (FOV_1(i, int(j + 1), int(k - 1)) .gt. 0)) .or. (j + 1 .le. cctk_lsh(2) .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 1), int(k + 2)) .gt. 0)) .or. (j + 1 .le. cctk_lsh(2) .and. k - 2 .ge. 1 .and. (FOV_1(i, int(j + 1), int(k - 2)) .gt. 0)) .or. (j + 1 .le. cctk_lsh(2) .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 1), int(k + 3)) .gt. 0)) .or. (j + 1 .le. cctk_lsh(2) .and. k - 3 .ge. 1 .and. (FOV_1(i, int(j + 1), int(k - 3)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 2), k) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 2), k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 2), k) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 2), k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 2), k) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 2), k) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 2), int(k + 1)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. k - 1 .ge. 1 .and. (FOV_1(i, int(j + 2), int(k - 1)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 2), int(k + 2)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. k - 2 .ge. 1 .and. (FOV_1(i, int(j + 2), int(k - 2)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 2), int(k + 3)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. k - 3 .ge. 1 .and. (FOV_1(i, int(j + 2), int(k - 3)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 3), k) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 3), k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 3), k) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 3), k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 3), k) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 3), k) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 3), int(k + 1)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. k - 1 .ge. 1 .and. (FOV_1(i, int(j + 3), int(k - 1)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 3), int(k + 2)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. k - 2 .ge. 1 .and. (FOV_1(i, int(j + 3), int(k - 2)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 3), int(k + 3)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. k - 3 .ge. 1 .and. (FOV_1(i, int(j + 3), int(k - 3)) .gt. 0))))) then
				Flux_extsubrhosupj_1(i, 4 - j, k) = 2.0d0 * Flux_extsubrhosupj_1(i, 4 - j + 1, k) - Flux_extsubrhosupj_1(i, 4 - j + 2, k)
				Flux_extsubmxsupj_1(i, 4 - j, k) = 2.0d0 * Flux_extsubmxsupj_1(i, 4 - j + 1, k) - Flux_extsubmxsupj_1(i, 4 - j + 2, k)
				Flux_extsubmysupj_1(i, 4 - j, k) = 2.0d0 * Flux_extsubmysupj_1(i, 4 - j + 1, k) - Flux_extsubmysupj_1(i, 4 - j + 2, k)
				Flux_extsubmzsupj_1(i, 4 - j, k) = 2.0d0 * Flux_extsubmzsupj_1(i, 4 - j + 1, k) - Flux_extsubmzsupj_1(i, 4 - j + 2, k)
		end if
		if ((FOV_yUpper(i, j, k) .gt. 0) .and. ((j - 1 .ge. 1 .and. (FOV_1(i, int(j - 1), k) .gt. 0)) .or. (j - 2 .ge. 1 .and. (FOV_1(i, int(j - 2), k) .gt. 0)) .or. (j - 3 .ge. 1 .and. (FOV_1(i, int(j - 3), k) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 1), k) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 1), k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 1), k) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 1), k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 1), k) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 1), k) .gt. 0)) .or. (j - 1 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 1), int(k + 1)) .gt. 0)) .or. (j - 1 .ge. 1 .and. k - 1 .ge. 1 .and. (FOV_1(i, int(j - 1), int(k - 1)) .gt. 0)) .or. (j - 1 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 1), int(k + 2)) .gt. 0)) .or. (j - 1 .ge. 1 .and. k - 2 .ge. 1 .and. (FOV_1(i, int(j - 1), int(k - 2)) .gt. 0)) .or. (j - 1 .ge. 1 .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 1), int(k + 3)) .gt. 0)) .or. (j - 1 .ge. 1 .and. k - 3 .ge. 1 .and. (FOV_1(i, int(j - 1), int(k - 3)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 2), k) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 2), k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 2), k) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 2), k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 2), k) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 2), k) .gt. 0)) .or. (j - 2 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 2), int(k + 1)) .gt. 0)) .or. (j - 2 .ge. 1 .and. k - 1 .ge. 1 .and. (FOV_1(i, int(j - 2), int(k - 1)) .gt. 0)) .or. (j - 2 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 2), int(k + 2)) .gt. 0)) .or. (j - 2 .ge. 1 .and. k - 2 .ge. 1 .and. (FOV_1(i, int(j - 2), int(k - 2)) .gt. 0)) .or. (j - 2 .ge. 1 .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 2), int(k + 3)) .gt. 0)) .or. (j - 2 .ge. 1 .and. k - 3 .ge. 1 .and. (FOV_1(i, int(j - 2), int(k - 3)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 3), k) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 3), k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 3), k) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 3), k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 3), k) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 3), k) .gt. 0)) .or. (j - 3 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 3), int(k + 1)) .gt. 0)) .or. (j - 3 .ge. 1 .and. k - 1 .ge. 1 .and. (FOV_1(i, int(j - 3), int(k - 1)) .gt. 0)) .or. (j - 3 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 3), int(k + 2)) .gt. 0)) .or. (j - 3 .ge. 1 .and. k - 2 .ge. 1 .and. (FOV_1(i, int(j - 3), int(k - 2)) .gt. 0)) .or. (j - 3 .ge. 1 .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 3), int(k + 3)) .gt. 0)) .or. (j - 3 .ge. 1 .and. k - 3 .ge. 1 .and. (FOV_1(i, int(j - 3), int(k - 3)) .gt. 0))))) then
			Flux_extsubrhosupj_1(i, j, k) = 2.0d0 * Flux_extsubrhosupj_1(i, j - 1, k) - Flux_extsubrhosupj_1(i, j - 2, k)
			Flux_extsubmxsupj_1(i, j, k) = 2.0d0 * Flux_extsubmxsupj_1(i, j - 1, k) - Flux_extsubmxsupj_1(i, j - 2, k)
			Flux_extsubmysupj_1(i, j, k) = 2.0d0 * Flux_extsubmysupj_1(i, j - 1, k) - Flux_extsubmysupj_1(i, j - 2, k)
			Flux_extsubmzsupj_1(i, j, k) = 2.0d0 * Flux_extsubmzsupj_1(i, j - 1, k) - Flux_extsubmzsupj_1(i, j - 2, k)
		end if
		if ((FOV_zLower(i, j, k) .gt. 0) .and. ((k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, j, int(k + 1)) .gt. 0)) .or. (k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, j, int(k + 2)) .gt. 0)) .or. (k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, j, int(k + 3)) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(int(i + 1), j, int(k + 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(int(i - 1), j, int(k + 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(int(i + 2), j, int(k + 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(int(i - 2), j, int(k + 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(int(i + 3), j, int(k + 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(int(i - 3), j, int(k + 1)) .gt. 0)) .or. (j + 1 .le. cctk_lsh(2) .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 1), int(k + 1)) .gt. 0)) .or. (j - 1 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 1), int(k + 1)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 2), int(k + 1)) .gt. 0)) .or. (j - 2 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 2), int(k + 1)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 3), int(k + 1)) .gt. 0)) .or. (j - 3 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 3), int(k + 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(int(i + 1), j, int(k + 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(int(i - 1), j, int(k + 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(int(i + 2), j, int(k + 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(int(i - 2), j, int(k + 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(int(i + 3), j, int(k + 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(int(i - 3), j, int(k + 2)) .gt. 0)) .or. (j + 1 .le. cctk_lsh(2) .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 1), int(k + 2)) .gt. 0)) .or. (j - 1 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 1), int(k + 2)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 2), int(k + 2)) .gt. 0)) .or. (j - 2 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 2), int(k + 2)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 3), int(k + 2)) .gt. 0)) .or. (j - 3 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 3), int(k + 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(int(i + 1), j, int(k + 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(int(i - 1), j, int(k + 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(int(i + 2), j, int(k + 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(int(i - 2), j, int(k + 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(int(i + 3), j, int(k + 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(int(i - 3), j, int(k + 3)) .gt. 0)) .or. (j + 1 .le. cctk_lsh(2) .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 1), int(k + 3)) .gt. 0)) .or. (j - 1 .ge. 1 .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 1), int(k + 3)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 2), int(k + 3)) .gt. 0)) .or. (j - 2 .ge. 1 .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 2), int(k + 3)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 3), int(k + 3)) .gt. 0)) .or. (j - 3 .ge. 1 .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 3), int(k + 3)) .gt. 0))))) then
				Flux_extsubrhosupk_1(i, j, 4 - k) = 2.0d0 * Flux_extsubrhosupk_1(i, j, 4 - k + 1) - Flux_extsubrhosupk_1(i, j, 4 - k + 2)
				Flux_extsubmxsupk_1(i, j, 4 - k) = 2.0d0 * Flux_extsubmxsupk_1(i, j, 4 - k + 1) - Flux_extsubmxsupk_1(i, j, 4 - k + 2)
				Flux_extsubmysupk_1(i, j, 4 - k) = 2.0d0 * Flux_extsubmysupk_1(i, j, 4 - k + 1) - Flux_extsubmysupk_1(i, j, 4 - k + 2)
				Flux_extsubmzsupk_1(i, j, 4 - k) = 2.0d0 * Flux_extsubmzsupk_1(i, j, 4 - k + 1) - Flux_extsubmzsupk_1(i, j, 4 - k + 2)
		end if
		if ((FOV_zUpper(i, j, k) .gt. 0) .and. ((k - 1 .ge. 1 .and. (FOV_1(i, j, int(k - 1)) .gt. 0)) .or. (k - 2 .ge. 1 .and. (FOV_1(i, j, int(k - 2)) .gt. 0)) .or. (k - 3 .ge. 1 .and. (FOV_1(i, j, int(k - 3)) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. k - 1 .ge. 1 .and. (FOV_1(int(i + 1), j, int(k - 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. k - 1 .ge. 1 .and. (FOV_1(int(i - 1), j, int(k - 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. k - 1 .ge. 1 .and. (FOV_1(int(i + 2), j, int(k - 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. k - 1 .ge. 1 .and. (FOV_1(int(i - 2), j, int(k - 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. k - 1 .ge. 1 .and. (FOV_1(int(i + 3), j, int(k - 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. k - 1 .ge. 1 .and. (FOV_1(int(i - 3), j, int(k - 1)) .gt. 0)) .or. (j + 1 .le. cctk_lsh(2) .and. k - 1 .ge. 1 .and. (FOV_1(i, int(j + 1), int(k - 1)) .gt. 0)) .or. (j - 1 .ge. 1 .and. k - 1 .ge. 1 .and. (FOV_1(i, int(j - 1), int(k - 1)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. k - 1 .ge. 1 .and. (FOV_1(i, int(j + 2), int(k - 1)) .gt. 0)) .or. (j - 2 .ge. 1 .and. k - 1 .ge. 1 .and. (FOV_1(i, int(j - 2), int(k - 1)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. k - 1 .ge. 1 .and. (FOV_1(i, int(j + 3), int(k - 1)) .gt. 0)) .or. (j - 3 .ge. 1 .and. k - 1 .ge. 1 .and. (FOV_1(i, int(j - 3), int(k - 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. k - 2 .ge. 1 .and. (FOV_1(int(i + 1), j, int(k - 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. k - 2 .ge. 1 .and. (FOV_1(int(i - 1), j, int(k - 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. k - 2 .ge. 1 .and. (FOV_1(int(i + 2), j, int(k - 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. k - 2 .ge. 1 .and. (FOV_1(int(i - 2), j, int(k - 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. k - 2 .ge. 1 .and. (FOV_1(int(i + 3), j, int(k - 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. k - 2 .ge. 1 .and. (FOV_1(int(i - 3), j, int(k - 2)) .gt. 0)) .or. (j + 1 .le. cctk_lsh(2) .and. k - 2 .ge. 1 .and. (FOV_1(i, int(j + 1), int(k - 2)) .gt. 0)) .or. (j - 1 .ge. 1 .and. k - 2 .ge. 1 .and. (FOV_1(i, int(j - 1), int(k - 2)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. k - 2 .ge. 1 .and. (FOV_1(i, int(j + 2), int(k - 2)) .gt. 0)) .or. (j - 2 .ge. 1 .and. k - 2 .ge. 1 .and. (FOV_1(i, int(j - 2), int(k - 2)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. k - 2 .ge. 1 .and. (FOV_1(i, int(j + 3), int(k - 2)) .gt. 0)) .or. (j - 3 .ge. 1 .and. k - 2 .ge. 1 .and. (FOV_1(i, int(j - 3), int(k - 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. k - 3 .ge. 1 .and. (FOV_1(int(i + 1), j, int(k - 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. k - 3 .ge. 1 .and. (FOV_1(int(i - 1), j, int(k - 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. k - 3 .ge. 1 .and. (FOV_1(int(i + 2), j, int(k - 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. k - 3 .ge. 1 .and. (FOV_1(int(i - 2), j, int(k - 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. k - 3 .ge. 1 .and. (FOV_1(int(i + 3), j, int(k - 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. k - 3 .ge. 1 .and. (FOV_1(int(i - 3), j, int(k - 3)) .gt. 0)) .or. (j + 1 .le. cctk_lsh(2) .and. k - 3 .ge. 1 .and. (FOV_1(i, int(j + 1), int(k - 3)) .gt. 0)) .or. (j - 1 .ge. 1 .and. k - 3 .ge. 1 .and. (FOV_1(i, int(j - 1), int(k - 3)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. k - 3 .ge. 1 .and. (FOV_1(i, int(j + 2), int(k - 3)) .gt. 0)) .or. (j - 2 .ge. 1 .and. k - 3 .ge. 1 .and. (FOV_1(i, int(j - 2), int(k - 3)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. k - 3 .ge. 1 .and. (FOV_1(i, int(j + 3), int(k - 3)) .gt. 0)) .or. (j - 3 .ge. 1 .and. k - 3 .ge. 1 .and. (FOV_1(i, int(j - 3), int(k - 3)) .gt. 0))))) then
			Flux_extsubrhosupk_1(i, j, k) = 2.0d0 * Flux_extsubrhosupk_1(i, j, k - 1) - Flux_extsubrhosupk_1(i, j, k - 2)
			Flux_extsubmxsupk_1(i, j, k) = 2.0d0 * Flux_extsubmxsupk_1(i, j, k - 1) - Flux_extsubmxsupk_1(i, j, k - 2)
			Flux_extsubmysupk_1(i, j, k) = 2.0d0 * Flux_extsubmysupk_1(i, j, k - 1) - Flux_extsubmysupk_1(i, j, k - 2)
			Flux_extsubmzsupk_1(i, j, k) = 2.0d0 * Flux_extsubmzsupk_1(i, j, k - 1) - Flux_extsubmzsupk_1(i, j, k - 2)
		end if
		if (FOV_1(i, j, k) .gt. 0) then
			if ((d_i_Null(i, j, k) .ne. 0 .or. d_j_Null(i, j, k) .ne. 0 .or. d_k_Null(i, j, k) .ne. 0)) then
				if (FOV_3(int(i - d_i_Null(i, j, k)), int(j - d_j_Null(i, j, k)), int(k - d_k_Null(i, j, k))) .gt. 0) then
					call extrapolate_flux(CCTK_PASS_FTOF, d_i_Null, Flux_extsubNullsupi_3, Flux_extsubNullsupi_3, i, d_j_Null, Flux_extsubNullsupj_3, Flux_extsubNullsupj_3, j, d_k_Null, Flux_extsubNullsupk_3, Flux_extsubNullsupk_3, k, FOV_3)
				end if
			end if
		end if
				end do
			end do
		end do
		call CCTK_SyncGroup(status, cctkGH, "FDAsNozzle3D::block1")
		do k = kStart, kEnd
			do j = jStart, jEnd
				do i = iStart, iEnd
					if ((FOV_3(i, j, k) .gt. 0 .or. (((i + 1 .le. cctk_lsh(1) .and. FOV_3(i + 1, j, k) .gt. 0) .or. (j + 1 .le. cctk_lsh(2) .and. FOV_3(i, j + 1, k) .gt. 0) .or. (k + 1 .le. cctk_lsh(3) .and. FOV_3(i, j, k + 1) .gt. 0)) .or. ((i - 1 .ge. 1 .and. FOV_3(i - 1, j, k) .gt. 0) .or. (j - 1 .ge. 1 .and. FOV_3(i, j - 1, k) .gt. 0) .or. (k - 1 .ge. 1 .and. FOV_3(i, j, k - 1) .gt. 0)))) .and. (i + 1 .le. cctk_lsh(1) .and. i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. j - 1 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. k - 1 .ge. 1)) then
					Speedi_3(i, j, k) = MAX(Speedi_3(i, j, k), Speedi_3(i + 1, j, k))
					Speedj_3(i, j, k) = MAX(Speedj_3(i, j, k), Speedj_3(i, j + 1, k))
					Speedk_3(i, j, k) = MAX(Speedk_3(i, j, k), Speedk_3(i, j, k + 1))
					end if
					if ((FOV_1(i, j, k) .gt. 0 .or. (((i + 1 .le. cctk_lsh(1) .and. FOV_1(i + 1, j, k) .gt. 0) .or. (j + 1 .le. cctk_lsh(2) .and. FOV_1(i, j + 1, k) .gt. 0) .or. (k + 1 .le. cctk_lsh(3) .and. FOV_1(i, j, k + 1) .gt. 0)) .or. ((i - 1 .ge. 1 .and. FOV_1(i - 1, j, k) .gt. 0) .or. (j - 1 .ge. 1 .and. FOV_1(i, j - 1, k) .gt. 0) .or. (k - 1 .ge. 1 .and. FOV_1(i, j, k - 1) .gt. 0)))) .and. (i + 1 .le. cctk_lsh(1) .and. i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. j - 1 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. k - 1 .ge. 1)) then
					Speedi_1(i, j, k) = MAX(Speedi_1(i, j, k), Speedi_1(i + 1, j, k))
					Speedj_1(i, j, k) = MAX(Speedj_1(i, j, k), Speedj_1(i, j + 1, k))
					Speedk_1(i, j, k) = MAX(Speedk_1(i, j, k), Speedk_1(i, j, k + 1))
					end if
				end do
			end do
		end do
		call CCTK_SyncGroup(status, cctkGH, "FDAsNozzle3D::block2")
		do k = kStart, kEnd
			do j = jStart, jEnd
				do i = iStart, iEnd
					if ((FOV_3(i, j, k) .gt. 0) .and. (i + 2 .le. cctk_lsh(1) .and. i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. j - 2 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. k - 2 .ge. 1)) then
					fluxAccNull_3 = 0.0d0
					fluxAccNull_3 = fluxAccNull_3 - FDOCi(CCTK_PASS_FTOF, extrapolatedsubNullsupi, Flux_extsubNullsupi_3, Speedi_3, i, j, k)
					fluxAccNull_3 = fluxAccNull_3 - FDOCj(CCTK_PASS_FTOF, extrapolatedsubNullsupj, Flux_extsubNullsupj_3, Speedj_3, i, j, k)
					fluxAccNull_3 = fluxAccNull_3 - FDOCk(CCTK_PASS_FTOF, extrapolatedsubNullsupk, Flux_extsubNullsupk_3, Speedk_3, i, j, k)
					Null(i, j, k) = RK3P3(CCTK_PASS_FTOF, fluxAccNull_3, Null_p, k2Null, i, j, k)
					end if
					if ((FOV_1(i, j, k) .gt. 0) .and. (i + 2 .le. cctk_lsh(1) .and. i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. j - 2 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. k - 2 .ge. 1)) then
					fluxAccrho_1 = 0.0d0
					fluxAccmx_1 = k2rho(i, j, k) * k2fx(i, j, k)
					fluxAccmy_1 = k2rho(i, j, k) * k2fy(i, j, k)
					fluxAccmz_1 = k2rho(i, j, k) * k2fz(i, j, k)
					fluxAccrho_1 = fluxAccrho_1 - FDOCi(CCTK_PASS_FTOF, extrapolatedsubrhosupi, Flux_extsubrhosupi_1, Speedi_1, i, j, k)
					fluxAccrho_1 = fluxAccrho_1 - FDOCj(CCTK_PASS_FTOF, extrapolatedsubrhosupj, Flux_extsubrhosupj_1, Speedj_1, i, j, k)
					fluxAccrho_1 = fluxAccrho_1 - FDOCk(CCTK_PASS_FTOF, extrapolatedsubrhosupk, Flux_extsubrhosupk_1, Speedk_1, i, j, k)
					fluxAccmx_1 = fluxAccmx_1 - FDOCi(CCTK_PASS_FTOF, extrapolatedsubmxsupi, Flux_extsubmxsupi_1, Speedi_1, i, j, k)
					fluxAccmx_1 = fluxAccmx_1 - FDOCj(CCTK_PASS_FTOF, extrapolatedsubmxsupj, Flux_extsubmxsupj_1, Speedj_1, i, j, k)
					fluxAccmx_1 = fluxAccmx_1 - FDOCk(CCTK_PASS_FTOF, extrapolatedsubmxsupk, Flux_extsubmxsupk_1, Speedk_1, i, j, k)
					fluxAccmy_1 = fluxAccmy_1 - FDOCi(CCTK_PASS_FTOF, extrapolatedsubmysupi, Flux_extsubmysupi_1, Speedi_1, i, j, k)
					fluxAccmy_1 = fluxAccmy_1 - FDOCj(CCTK_PASS_FTOF, extrapolatedsubmysupj, Flux_extsubmysupj_1, Speedj_1, i, j, k)
					fluxAccmy_1 = fluxAccmy_1 - FDOCk(CCTK_PASS_FTOF, extrapolatedsubmysupk, Flux_extsubmysupk_1, Speedk_1, i, j, k)
					fluxAccmz_1 = fluxAccmz_1 - FDOCi(CCTK_PASS_FTOF, extrapolatedsubmzsupi, Flux_extsubmzsupi_1, Speedi_1, i, j, k)
					fluxAccmz_1 = fluxAccmz_1 - FDOCj(CCTK_PASS_FTOF, extrapolatedsubmzsupj, Flux_extsubmzsupj_1, Speedj_1, i, j, k)
					fluxAccmz_1 = fluxAccmz_1 - FDOCk(CCTK_PASS_FTOF, extrapolatedsubmzsupk, Flux_extsubmzsupk_1, Speedk_1, i, j, k)
					n_x_1 = 0.0d0
					n_y_1 = 0.0d0
					n_z_1 = 0.0d0
					interaction_index_1 = 0.0d0
					if (((n_x_1 .eq. 0.0d0) .and. (n_y_1 .eq. 0.0d0)) .and. (n_z_1 .eq. 0.0d0)) then
					if (FOV_3(int(i + 1.0d0), j, k) .gt. 0.0d0) then
					interaction_index_1 = 1.0d0
					n_x_1 = n_x_1 + deltax
					end if
					if (FOV_3(int(i - 1.0d0), j, k) .gt. 0.0d0) then
					interaction_index_1 = 1.0d0
					n_x_1 = n_x_1 - deltax
					end if
					if (FOV_3(i, int(j + 1.0d0), k) .gt. 0.0d0) then
					interaction_index_1 = 1.0d0
					n_y_1 = n_y_1 + deltay
					end if
					if (FOV_3(i, int(j - 1.0d0), k) .gt. 0.0d0) then
					interaction_index_1 = 1.0d0
					n_y_1 = n_y_1 - deltay
					end if
					if (FOV_3(i, j, int(k + 1.0d0)) .gt. 0.0d0) then
					interaction_index_1 = 1.0d0
					n_z_1 = n_z_1 + deltaz
					end if
					if (FOV_3(i, j, int(k - 1.0d0)) .gt. 0.0d0) then
					interaction_index_1 = 1.0d0
					n_z_1 = n_z_1 - deltaz
					end if
					end if
					if (((n_x_1 .eq. 0.0d0) .and. (n_y_1 .eq. 0.0d0)) .and. (n_z_1 .eq. 0.0d0)) then
					if (((((((((FOV_3(int(i + 1.0d0), int(j + 1.0d0), int(k + 1.0d0)) .gt. 0.0d0) .or. (FOV_3(int(i + 1.0d0), int(j + 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(int(i + 1.0d0), int(j + 1.0d0), k) .gt. 0.0d0)) .or. (FOV_3(int(i + 1.0d0), int(j - 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(int(i + 1.0d0), int(j - 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(int(i + 1.0d0), int(j - 1.0d0), k) .gt. 0.0d0)) .or. (FOV_3(int(i + 1.0d0), j, int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(int(i + 1.0d0), j, int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(int(i + 1.0d0), j, k) .gt. 0.0d0)) then
					interaction_index_1 = 1.0d0
					n_x_1 = n_x_1 + deltax
					end if
					if (((((((((FOV_3(int(i - 1.0d0), int(j + 1.0d0), int(k + 1.0d0)) .gt. 0.0d0) .or. (FOV_3(int(i - 1.0d0), int(j + 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(int(i - 1.0d0), int(j + 1.0d0), k) .gt. 0.0d0)) .or. (FOV_3(int(i - 1.0d0), int(j - 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(int(i - 1.0d0), int(j - 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(int(i - 1.0d0), int(j - 1.0d0), k) .gt. 0.0d0)) .or. (FOV_3(int(i - 1.0d0), j, int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(int(i - 1.0d0), j, int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(int(i - 1.0d0), j, k) .gt. 0.0d0)) then
					interaction_index_1 = 1.0d0
					n_x_1 = n_x_1 - deltax
					end if
					if (((((((((FOV_3(int(i + 1.0d0), int(j + 1.0d0), int(k + 1.0d0)) .gt. 0.0d0) .or. (FOV_3(int(i + 1.0d0), int(j + 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(int(i + 1.0d0), int(j + 1.0d0), k) .gt. 0.0d0)) .or. (FOV_3(int(i - 1.0d0), int(j + 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(int(i - 1.0d0), int(j + 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(int(i - 1.0d0), int(j + 1.0d0), k) .gt. 0.0d0)) .or. (FOV_3(i, int(j + 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(i, int(j + 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(i, int(j + 1.0d0), k) .gt. 0.0d0)) then
					interaction_index_1 = 1.0d0
					n_y_1 = n_y_1 + deltay
					end if
					if (((((((((FOV_3(int(i + 1.0d0), int(j - 1.0d0), int(k + 1.0d0)) .gt. 0.0d0) .or. (FOV_3(int(i + 1.0d0), int(j - 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(int(i + 1.0d0), int(j - 1.0d0), k) .gt. 0.0d0)) .or. (FOV_3(int(i - 1.0d0), int(j - 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(int(i - 1.0d0), int(j - 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(int(i - 1.0d0), int(j - 1.0d0), k) .gt. 0.0d0)) .or. (FOV_3(i, int(j - 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(i, int(j - 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(i, int(j - 1.0d0), k) .gt. 0.0d0)) then
					interaction_index_1 = 1.0d0
					n_y_1 = n_y_1 - deltay
					end if
					if (((((((((FOV_3(int(i + 1.0d0), int(j + 1.0d0), int(k + 1.0d0)) .gt. 0.0d0) .or. (FOV_3(int(i + 1.0d0), int(j - 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(int(i + 1.0d0), j, int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(int(i - 1.0d0), int(j + 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(int(i - 1.0d0), int(j - 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(int(i - 1.0d0), j, int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(i, int(j + 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(i, int(j - 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(i, j, int(k + 1.0d0)) .gt. 0.0d0)) then
					interaction_index_1 = 1.0d0
					n_z_1 = n_z_1 + deltaz
					end if
					if (((((((((FOV_3(int(i + 1.0d0), int(j + 1.0d0), int(k - 1.0d0)) .gt. 0.0d0) .or. (FOV_3(int(i + 1.0d0), int(j - 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(int(i + 1.0d0), j, int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(int(i - 1.0d0), int(j + 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(int(i - 1.0d0), int(j - 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(int(i - 1.0d0), j, int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(i, int(j + 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(i, int(j - 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_3(i, j, int(k - 1.0d0)) .gt. 0.0d0)) then
					interaction_index_1 = 1.0d0
					n_z_1 = n_z_1 - deltaz
					end if
					end if
					if (((n_x_1 .eq. 0.0d0) .and. (n_y_1 .eq. 0.0d0)) .and. (n_z_1 .eq. 0.0d0)) then
					if (FOV_xLower(int(i + 1.0d0), j, k) .gt. 0.0d0) then
					interaction_index_1 = 2.0d0
					n_x_1 = n_x_1 + deltax
					end if
					if (FOV_xLower(int(i - 1.0d0), j, k) .gt. 0.0d0) then
					interaction_index_1 = 2.0d0
					n_x_1 = n_x_1 - deltax
					end if
					if (FOV_xLower(i, int(j + 1.0d0), k) .gt. 0.0d0) then
					interaction_index_1 = 2.0d0
					n_y_1 = n_y_1 + deltay
					end if
					if (FOV_xLower(i, int(j - 1.0d0), k) .gt. 0.0d0) then
					interaction_index_1 = 2.0d0
					n_y_1 = n_y_1 - deltay
					end if
					if (FOV_xLower(i, j, int(k + 1.0d0)) .gt. 0.0d0) then
					interaction_index_1 = 2.0d0
					n_z_1 = n_z_1 + deltaz
					end if
					if (FOV_xLower(i, j, int(k - 1.0d0)) .gt. 0.0d0) then
					interaction_index_1 = 2.0d0
					n_z_1 = n_z_1 - deltaz
					end if
					end if
					if (((n_x_1 .eq. 0.0d0) .and. (n_y_1 .eq. 0.0d0)) .and. (n_z_1 .eq. 0.0d0)) then
					if (((((((((FOV_xLower(int(i + 1.0d0), int(j + 1.0d0), int(k + 1.0d0)) .gt. 0.0d0) .or. (FOV_xLower(int(i + 1.0d0), int(j + 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i + 1.0d0), int(j + 1.0d0), k) .gt. 0.0d0)) .or. (FOV_xLower(int(i + 1.0d0), int(j - 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i + 1.0d0), int(j - 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i + 1.0d0), int(j - 1.0d0), k) .gt. 0.0d0)) .or. (FOV_xLower(int(i + 1.0d0), j, int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i + 1.0d0), j, int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i + 1.0d0), j, k) .gt. 0.0d0)) then
					interaction_index_1 = 2.0d0
					n_x_1 = n_x_1 + deltax
					end if
					if (((((((((FOV_xLower(int(i - 1.0d0), int(j + 1.0d0), int(k + 1.0d0)) .gt. 0.0d0) .or. (FOV_xLower(int(i - 1.0d0), int(j + 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i - 1.0d0), int(j + 1.0d0), k) .gt. 0.0d0)) .or. (FOV_xLower(int(i - 1.0d0), int(j - 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i - 1.0d0), int(j - 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i - 1.0d0), int(j - 1.0d0), k) .gt. 0.0d0)) .or. (FOV_xLower(int(i - 1.0d0), j, int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i - 1.0d0), j, int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i - 1.0d0), j, k) .gt. 0.0d0)) then
					interaction_index_1 = 2.0d0
					n_x_1 = n_x_1 - deltax
					end if
					if (((((((((FOV_xLower(int(i + 1.0d0), int(j + 1.0d0), int(k + 1.0d0)) .gt. 0.0d0) .or. (FOV_xLower(int(i + 1.0d0), int(j + 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i + 1.0d0), int(j + 1.0d0), k) .gt. 0.0d0)) .or. (FOV_xLower(int(i - 1.0d0), int(j + 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i - 1.0d0), int(j + 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i - 1.0d0), int(j + 1.0d0), k) .gt. 0.0d0)) .or. (FOV_xLower(i, int(j + 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(i, int(j + 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(i, int(j + 1.0d0), k) .gt. 0.0d0)) then
					interaction_index_1 = 2.0d0
					n_y_1 = n_y_1 + deltay
					end if
					if (((((((((FOV_xLower(int(i + 1.0d0), int(j - 1.0d0), int(k + 1.0d0)) .gt. 0.0d0) .or. (FOV_xLower(int(i + 1.0d0), int(j - 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i + 1.0d0), int(j - 1.0d0), k) .gt. 0.0d0)) .or. (FOV_xLower(int(i - 1.0d0), int(j - 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i - 1.0d0), int(j - 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i - 1.0d0), int(j - 1.0d0), k) .gt. 0.0d0)) .or. (FOV_xLower(i, int(j - 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(i, int(j - 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(i, int(j - 1.0d0), k) .gt. 0.0d0)) then
					interaction_index_1 = 2.0d0
					n_y_1 = n_y_1 - deltay
					end if
					if (((((((((FOV_xLower(int(i + 1.0d0), int(j + 1.0d0), int(k + 1.0d0)) .gt. 0.0d0) .or. (FOV_xLower(int(i + 1.0d0), int(j - 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i + 1.0d0), j, int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i - 1.0d0), int(j + 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i - 1.0d0), int(j - 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i - 1.0d0), j, int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(i, int(j + 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(i, int(j - 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(i, j, int(k + 1.0d0)) .gt. 0.0d0)) then
					interaction_index_1 = 2.0d0
					n_z_1 = n_z_1 + deltaz
					end if
					if (((((((((FOV_xLower(int(i + 1.0d0), int(j + 1.0d0), int(k - 1.0d0)) .gt. 0.0d0) .or. (FOV_xLower(int(i + 1.0d0), int(j - 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i + 1.0d0), j, int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i - 1.0d0), int(j + 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i - 1.0d0), int(j - 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i - 1.0d0), j, int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(i, int(j + 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(i, int(j - 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(i, j, int(k - 1.0d0)) .gt. 0.0d0)) then
					interaction_index_1 = 2.0d0
					n_z_1 = n_z_1 - deltaz
					end if
					end if
					if (((n_x_1 .eq. 0.0d0) .and. (n_y_1 .eq. 0.0d0)) .and. (n_z_1 .eq. 0.0d0)) then
					if (FOV_xUpper(int(i + 1.0d0), j, k) .gt. 0.0d0) then
					interaction_index_1 = 3.0d0
					n_x_1 = n_x_1 + deltax
					end if
					if (FOV_xUpper(int(i - 1.0d0), j, k) .gt. 0.0d0) then
					interaction_index_1 = 3.0d0
					n_x_1 = n_x_1 - deltax
					end if
					if (FOV_xUpper(i, int(j + 1.0d0), k) .gt. 0.0d0) then
					interaction_index_1 = 3.0d0
					n_y_1 = n_y_1 + deltay
					end if
					if (FOV_xUpper(i, int(j - 1.0d0), k) .gt. 0.0d0) then
					interaction_index_1 = 3.0d0
					n_y_1 = n_y_1 - deltay
					end if
					if (FOV_xUpper(i, j, int(k + 1.0d0)) .gt. 0.0d0) then
					interaction_index_1 = 3.0d0
					n_z_1 = n_z_1 + deltaz
					end if
					if (FOV_xUpper(i, j, int(k - 1.0d0)) .gt. 0.0d0) then
					interaction_index_1 = 3.0d0
					n_z_1 = n_z_1 - deltaz
					end if
					end if
					if (((n_x_1 .eq. 0.0d0) .and. (n_y_1 .eq. 0.0d0)) .and. (n_z_1 .eq. 0.0d0)) then
					if (((((((((FOV_xUpper(int(i + 1.0d0), int(j + 1.0d0), int(k + 1.0d0)) .gt. 0.0d0) .or. (FOV_xUpper(int(i + 1.0d0), int(j + 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i + 1.0d0), int(j + 1.0d0), k) .gt. 0.0d0)) .or. (FOV_xUpper(int(i + 1.0d0), int(j - 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i + 1.0d0), int(j - 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i + 1.0d0), int(j - 1.0d0), k) .gt. 0.0d0)) .or. (FOV_xUpper(int(i + 1.0d0), j, int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i + 1.0d0), j, int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i + 1.0d0), j, k) .gt. 0.0d0)) then
					interaction_index_1 = 3.0d0
					n_x_1 = n_x_1 + deltax
					end if
					if (((((((((FOV_xUpper(int(i - 1.0d0), int(j + 1.0d0), int(k + 1.0d0)) .gt. 0.0d0) .or. (FOV_xUpper(int(i - 1.0d0), int(j + 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i - 1.0d0), int(j + 1.0d0), k) .gt. 0.0d0)) .or. (FOV_xUpper(int(i - 1.0d0), int(j - 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i - 1.0d0), int(j - 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i - 1.0d0), int(j - 1.0d0), k) .gt. 0.0d0)) .or. (FOV_xUpper(int(i - 1.0d0), j, int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i - 1.0d0), j, int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i - 1.0d0), j, k) .gt. 0.0d0)) then
					interaction_index_1 = 3.0d0
					n_x_1 = n_x_1 - deltax
					end if
					if (((((((((FOV_xUpper(int(i + 1.0d0), int(j + 1.0d0), int(k + 1.0d0)) .gt. 0.0d0) .or. (FOV_xUpper(int(i + 1.0d0), int(j + 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i + 1.0d0), int(j + 1.0d0), k) .gt. 0.0d0)) .or. (FOV_xUpper(int(i - 1.0d0), int(j + 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i - 1.0d0), int(j + 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i - 1.0d0), int(j + 1.0d0), k) .gt. 0.0d0)) .or. (FOV_xUpper(i, int(j + 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(i, int(j + 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(i, int(j + 1.0d0), k) .gt. 0.0d0)) then
					interaction_index_1 = 3.0d0
					n_y_1 = n_y_1 + deltay
					end if
					if (((((((((FOV_xUpper(int(i + 1.0d0), int(j - 1.0d0), int(k + 1.0d0)) .gt. 0.0d0) .or. (FOV_xUpper(int(i + 1.0d0), int(j - 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i + 1.0d0), int(j - 1.0d0), k) .gt. 0.0d0)) .or. (FOV_xUpper(int(i - 1.0d0), int(j - 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i - 1.0d0), int(j - 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i - 1.0d0), int(j - 1.0d0), k) .gt. 0.0d0)) .or. (FOV_xUpper(i, int(j - 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(i, int(j - 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(i, int(j - 1.0d0), k) .gt. 0.0d0)) then
					interaction_index_1 = 3.0d0
					n_y_1 = n_y_1 - deltay
					end if
					if (((((((((FOV_xUpper(int(i + 1.0d0), int(j + 1.0d0), int(k + 1.0d0)) .gt. 0.0d0) .or. (FOV_xUpper(int(i + 1.0d0), int(j - 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i + 1.0d0), j, int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i - 1.0d0), int(j + 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i - 1.0d0), int(j - 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i - 1.0d0), j, int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(i, int(j + 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(i, int(j - 1.0d0), int(k + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(i, j, int(k + 1.0d0)) .gt. 0.0d0)) then
					interaction_index_1 = 3.0d0
					n_z_1 = n_z_1 + deltaz
					end if
					if (((((((((FOV_xUpper(int(i + 1.0d0), int(j + 1.0d0), int(k - 1.0d0)) .gt. 0.0d0) .or. (FOV_xUpper(int(i + 1.0d0), int(j - 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i + 1.0d0), j, int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i - 1.0d0), int(j + 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i - 1.0d0), int(j - 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i - 1.0d0), j, int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(i, int(j + 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(i, int(j - 1.0d0), int(k - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(i, j, int(k - 1.0d0)) .gt. 0.0d0)) then
					interaction_index_1 = 3.0d0
					n_z_1 = n_z_1 - deltaz
					end if
					end if
					if (((n_x_1 .ne. 0.0d0) .or. (n_y_1 .ne. 0.0d0)) .or. (n_z_1 .ne. 0.0d0)) then
					mod_normal_1 = SQRT((n_x_1 ** 2.0d0 + n_y_1 ** 2.0d0) + n_z_1 ** 2.0d0)
					n_x_1 = n_x_1 / mod_normal_1
					n_y_1 = n_y_1 / mod_normal_1
					n_z_1 = n_z_1 / mod_normal_1
					end if
					if (interaction_index_1 .eq. 1.0d0) then
					fluxAccm_n_1 = fluxAccmx_1 * n_x_1 + fluxAccmy_1 * n_y_1 + fluxAccmz_1 * n_z_1
					fluxAccm_tx_1 = fluxAccmx_1 - (fluxAccmx_1 * n_x_1 + fluxAccmy_1 * n_y_1 + fluxAccmz_1 * n_z_1) * n_x_1
					fluxAccm_ty_1 = fluxAccmy_1 - (fluxAccmx_1 * n_x_1 + fluxAccmy_1 * n_y_1 + fluxAccmz_1 * n_z_1) * n_y_1
					fluxAccm_tz_1 = fluxAccmz_1 - (fluxAccmx_1 * n_x_1 + fluxAccmy_1 * n_y_1 + fluxAccmz_1 * n_z_1) * n_z_1
					m_n_1 = k2mx(i, j, k) * n_x_1 + k2my(i, j, k) * n_y_1 + k2mz(i, j, k) * n_z_1
					m_tx_1 = k2mx(i, j, k) - (k2mx(i, j, k) * n_x_1 + k2my(i, j, k) * n_y_1 + k2mz(i, j, k) * n_z_1) * n_x_1
					m_ty_1 = k2my(i, j, k) - (k2mx(i, j, k) * n_x_1 + k2my(i, j, k) * n_y_1 + k2mz(i, j, k) * n_z_1) * n_y_1
					m_tz_1 = k2mz(i, j, k) - (k2mx(i, j, k) * n_x_1 + k2my(i, j, k) * n_y_1 + k2mz(i, j, k) * n_z_1) * n_z_1
					c_1 = c0 * ((k2rho(i, j, k) / rho0) ** ((gamma + (-1.0d0)) / 2.0d0))
					cn_1 = m_n_1 / k2rho(i, j, k)
					if ((c_1 + cn_1) .gt. 0.0d0) then
					fluxAccwnp_3_1 = fluxAccrho_1 * (-((-c_1) + cn_1)) + fluxAccm_n_1
					else
					fluxAccwnp_3_1 = (-fluxAccm_n_1) + fluxAccrho_1 * (-((-c_1) + cn_1))
					end if
					if (((-c_1) + cn_1) .gt. 0.0d0) then
					fluxAccwnm_3_1 = fluxAccrho_1 * (-(c_1 + cn_1)) + fluxAccm_n_1
					else
					fluxAccwnm_3_1 = (-fluxAccm_n_1) + fluxAccrho_1 * (-(c_1 + cn_1))
					end if
					if (cn_1 .gt. 0.0d0) then
					fluxAccwtx_3_1 = fluxAccrho_1 * (-m_tx_1) / k2rho(i, j, k) + fluxAccm_tx_1
					else
					fluxAccwtx_3_1 = fluxAccrho_1 * (-m_tx_1) / k2rho(i, j, k)
					end if
					if (cn_1 .gt. 0.0d0) then
					fluxAccwty_3_1 = fluxAccrho_1 * (-m_ty_1) / k2rho(i, j, k) + fluxAccm_ty_1
					else
					fluxAccwty_3_1 = fluxAccrho_1 * (-m_ty_1) / k2rho(i, j, k)
					end if
					if (cn_1 .gt. 0.0d0) then
					fluxAccwtz_3_1 = fluxAccrho_1 * (-m_tz_1) / k2rho(i, j, k) + fluxAccm_tz_1
					else
					fluxAccwtz_3_1 = fluxAccrho_1 * (-m_tz_1) / k2rho(i, j, k)
					end if
					fluxAccrho_1 = fluxAccwnp_3_1 * 1.0d0 / (2.0d0 * c_1) + fluxAccwnm_3_1 * (-1.0d0) / (2.0d0 * c_1)
					fluxAccm_n_1 = fluxAccwnp_3_1 * (cn_1 / (2.0d0 * c_1) + 1.0d0 / 2.0d0) + fluxAccwnm_3_1 * (-(cn_1 / (2.0d0 * c_1) + (-1.0d0 / 2.0d0)))
					fluxAccm_tx_1 = (fluxAccwnp_3_1 * m_tx_1 / (2.0d0 * k2rho(i, j, k) * c_1) + fluxAccwnm_3_1 * (-m_tx_1) / (2.0d0 * k2rho(i, j, k) * c_1)) + fluxAccwtx_3_1
					fluxAccm_ty_1 = (fluxAccwnp_3_1 * m_ty_1 / (2.0d0 * k2rho(i, j, k) * c_1) + fluxAccwnm_3_1 * (-m_ty_1) / (2.0d0 * k2rho(i, j, k) * c_1)) + fluxAccwty_3_1
					fluxAccm_tz_1 = (fluxAccwnp_3_1 * m_tz_1 / (2.0d0 * k2rho(i, j, k) * c_1) + fluxAccwnm_3_1 * (-m_tz_1) / (2.0d0 * k2rho(i, j, k) * c_1)) + fluxAccwtz_3_1
					fluxAccmx_1 = fluxAccm_tx_1 + fluxAccm_n_1 * n_x_1
					fluxAccmy_1 = fluxAccm_ty_1 + fluxAccm_n_1 * n_y_1
					fluxAccmz_1 = fluxAccm_tz_1 + fluxAccm_n_1 * n_z_1
					end if
					if (interaction_index_1 .eq. 2.0d0) then
					fluxAccm_n_1 = fluxAccmx_1 * n_x_1 + fluxAccmy_1 * n_y_1 + fluxAccmz_1 * n_z_1
					fluxAccm_tx_1 = fluxAccmx_1 - (fluxAccmx_1 * n_x_1 + fluxAccmy_1 * n_y_1 + fluxAccmz_1 * n_z_1) * n_x_1
					fluxAccm_ty_1 = fluxAccmy_1 - (fluxAccmx_1 * n_x_1 + fluxAccmy_1 * n_y_1 + fluxAccmz_1 * n_z_1) * n_y_1
					fluxAccm_tz_1 = fluxAccmz_1 - (fluxAccmx_1 * n_x_1 + fluxAccmy_1 * n_y_1 + fluxAccmz_1 * n_z_1) * n_z_1
					m_n_1 = k2mx(i, j, k) * n_x_1 + k2my(i, j, k) * n_y_1 + k2mz(i, j, k) * n_z_1
					m_tx_1 = k2mx(i, j, k) - (k2mx(i, j, k) * n_x_1 + k2my(i, j, k) * n_y_1 + k2mz(i, j, k) * n_z_1) * n_x_1
					m_ty_1 = k2my(i, j, k) - (k2mx(i, j, k) * n_x_1 + k2my(i, j, k) * n_y_1 + k2mz(i, j, k) * n_z_1) * n_y_1
					m_tz_1 = k2mz(i, j, k) - (k2mx(i, j, k) * n_x_1 + k2my(i, j, k) * n_y_1 + k2mz(i, j, k) * n_z_1) * n_z_1
					c_1 = c0 * ((k2rho(i, j, k) / rho0) ** ((gamma + (-1.0d0)) / 2.0d0))
					cn_1 = m_n_1 / k2rho(i, j, k)
					if ((c_1 + cn_1) .gt. 0.0d0) then
					fluxAccwnp_3_1 = fluxAccrho_1 * (-((-c_1) + cn_1)) + fluxAccm_n_1
					else
					fluxAccwnp_3_1 = (-lambdain) * (m_n_1 + k2rho(i, j, k) * v0 * (1.0d0 - (((-0.008d0) + (j - (1 - cctk_lbnd(2))) * deltay) ** 2.0d0 + ((-0.008d0) + (k - (1 - cctk_lbnd(3))) * deltaz) ** 2.0d0) / ((FLOOR((0.006d0 * ABS((-0.008d0) + (j - (1 - cctk_lbnd(2))) * deltay)) / (SQRT(((-0.008d0) + (j - (1 - cctk_lbnd(2))) * deltay) ** 2.0d0 + ((-0.008d0) + (k - (1 - cctk_lbnd(3))) * deltaz) ** 2.0d0) * deltay) + 0.5d0) * deltay) ** 2.0d0 + (FLOOR((0.006d0 * ABS((-0.008d0) + (k - (1 - cctk_lbnd(3))) * deltaz)) / (SQRT(((-0.008d0) + (j - (1 - cctk_lbnd(2))) * deltay) ** 2.0d0 + ((-0.008d0) + (k - (1 - cctk_lbnd(3))) * deltaz) ** 2.0d0) * deltaz) + 0.5d0) * deltaz) ** 2.0d0)))
					end if
					if (((-c_1) + cn_1) .gt. 0.0d0) then
					fluxAccwnm_3_1 = fluxAccrho_1 * (-(c_1 + cn_1)) + fluxAccm_n_1
					else
					fluxAccwnm_3_1 = (-lambdain) * (m_n_1 + k2rho(i, j, k) * v0 * (1.0d0 - (((-0.008d0) + (j - (1 - cctk_lbnd(2))) * deltay) ** 2.0d0 + ((-0.008d0) + (k - (1 - cctk_lbnd(3))) * deltaz) ** 2.0d0) / ((FLOOR((0.006d0 * ABS((-0.008d0) + (j - (1 - cctk_lbnd(2))) * deltay)) / (SQRT(((-0.008d0) + (j - (1 - cctk_lbnd(2))) * deltay) ** 2.0d0 + ((-0.008d0) + (k - (1 - cctk_lbnd(3))) * deltaz) ** 2.0d0) * deltay) + 0.5d0) * deltay) ** 2.0d0 + (FLOOR((0.006d0 * ABS((-0.008d0) + (k - (1 - cctk_lbnd(3))) * deltaz)) / (SQRT(((-0.008d0) + (j - (1 - cctk_lbnd(2))) * deltay) ** 2.0d0 + ((-0.008d0) + (k - (1 - cctk_lbnd(3))) * deltaz) ** 2.0d0) * deltaz) + 0.5d0) * deltaz) ** 2.0d0)))
					end if
					if (cn_1 .gt. 0.0d0) then
					fluxAccwtx_3_1 = fluxAccrho_1 * (-m_tx_1) / k2rho(i, j, k) + fluxAccm_tx_1
					else
					fluxAccwtx_3_1 = (-lambdain) * (m_tx_1 - k2rho(i, j, k) * vt0)
					end if
					if (cn_1 .gt. 0.0d0) then
					fluxAccwty_3_1 = fluxAccrho_1 * (-m_ty_1) / k2rho(i, j, k) + fluxAccm_ty_1
					else
					fluxAccwty_3_1 = (-lambdain) * (m_ty_1 - k2rho(i, j, k) * vt0)
					end if
					if (cn_1 .gt. 0.0d0) then
					fluxAccwtz_3_1 = fluxAccrho_1 * (-m_tz_1) / k2rho(i, j, k) + fluxAccm_tz_1
					else
					fluxAccwtz_3_1 = (-lambdain) * (m_tz_1 - k2rho(i, j, k) * vt0)
					end if
					fluxAccrho_1 = fluxAccwnp_3_1 * 1.0d0 / (2.0d0 * c_1) + fluxAccwnm_3_1 * (-1.0d0) / (2.0d0 * c_1)
					fluxAccm_n_1 = fluxAccwnp_3_1 * (cn_1 / (2.0d0 * c_1) + 1.0d0 / 2.0d0) + fluxAccwnm_3_1 * (-(cn_1 / (2.0d0 * c_1) + (-1.0d0 / 2.0d0)))
					fluxAccm_tx_1 = (fluxAccwnp_3_1 * m_tx_1 / (2.0d0 * k2rho(i, j, k) * c_1) + fluxAccwnm_3_1 * (-m_tx_1) / (2.0d0 * k2rho(i, j, k) * c_1)) + fluxAccwtx_3_1
					fluxAccm_ty_1 = (fluxAccwnp_3_1 * m_ty_1 / (2.0d0 * k2rho(i, j, k) * c_1) + fluxAccwnm_3_1 * (-m_ty_1) / (2.0d0 * k2rho(i, j, k) * c_1)) + fluxAccwty_3_1
					fluxAccm_tz_1 = (fluxAccwnp_3_1 * m_tz_1 / (2.0d0 * k2rho(i, j, k) * c_1) + fluxAccwnm_3_1 * (-m_tz_1) / (2.0d0 * k2rho(i, j, k) * c_1)) + fluxAccwtz_3_1
					fluxAccmx_1 = fluxAccm_tx_1 + fluxAccm_n_1 * n_x_1
					fluxAccmy_1 = fluxAccm_ty_1 + fluxAccm_n_1 * n_y_1
					fluxAccmz_1 = fluxAccm_tz_1 + fluxAccm_n_1 * n_z_1
					end if
					if (interaction_index_1 .eq. 3.0d0) then
					fluxAccm_n_1 = fluxAccmx_1 * n_x_1 + fluxAccmy_1 * n_y_1 + fluxAccmz_1 * n_z_1
					fluxAccm_tx_1 = fluxAccmx_1 - (fluxAccmx_1 * n_x_1 + fluxAccmy_1 * n_y_1 + fluxAccmz_1 * n_z_1) * n_x_1
					fluxAccm_ty_1 = fluxAccmy_1 - (fluxAccmx_1 * n_x_1 + fluxAccmy_1 * n_y_1 + fluxAccmz_1 * n_z_1) * n_y_1
					fluxAccm_tz_1 = fluxAccmz_1 - (fluxAccmx_1 * n_x_1 + fluxAccmy_1 * n_y_1 + fluxAccmz_1 * n_z_1) * n_z_1
					m_n_1 = k2mx(i, j, k) * n_x_1 + k2my(i, j, k) * n_y_1 + k2mz(i, j, k) * n_z_1
					m_tx_1 = k2mx(i, j, k) - (k2mx(i, j, k) * n_x_1 + k2my(i, j, k) * n_y_1 + k2mz(i, j, k) * n_z_1) * n_x_1
					m_ty_1 = k2my(i, j, k) - (k2mx(i, j, k) * n_x_1 + k2my(i, j, k) * n_y_1 + k2mz(i, j, k) * n_z_1) * n_y_1
					m_tz_1 = k2mz(i, j, k) - (k2mx(i, j, k) * n_x_1 + k2my(i, j, k) * n_y_1 + k2mz(i, j, k) * n_z_1) * n_z_1
					c_1 = c0 * ((k2rho(i, j, k) / rho0) ** ((gamma + (-1.0d0)) / 2.0d0))
					cn_1 = m_n_1 / k2rho(i, j, k)
					if ((c_1 + cn_1) .gt. 0.0d0) then
					fluxAccwnp_3_1 = fluxAccrho_1 * (-((-c_1) + cn_1)) + fluxAccm_n_1
					else
					fluxAccwnp_3_1 = ((-c_1) + cn_1) * lambdaout * (k2rho(i, j, k) - rho0)
					end if
					if (((-c_1) + cn_1) .gt. 0.0d0) then
					fluxAccwnm_3_1 = fluxAccrho_1 * (-(c_1 + cn_1)) + fluxAccm_n_1
					else
					fluxAccwnm_3_1 = (c_1 + cn_1) * lambdaout * (k2rho(i, j, k) - rho0)
					end if
					if (cn_1 .gt. 0.0d0) then
					fluxAccwtx_3_1 = fluxAccrho_1 * (-m_tx_1) / k2rho(i, j, k) + fluxAccm_tx_1
					else
					fluxAccwtx_3_1 = m_tx_1 / k2rho(i, j, k) * lambdaout * (k2rho(i, j, k) - rho0)
					end if
					if (cn_1 .gt. 0.0d0) then
					fluxAccwty_3_1 = fluxAccrho_1 * (-m_ty_1) / k2rho(i, j, k) + fluxAccm_ty_1
					else
					fluxAccwty_3_1 = m_ty_1 / k2rho(i, j, k) * lambdaout * (k2rho(i, j, k) - rho0)
					end if
					if (cn_1 .gt. 0.0d0) then
					fluxAccwtz_3_1 = fluxAccrho_1 * (-m_tz_1) / k2rho(i, j, k) + fluxAccm_tz_1
					else
					fluxAccwtz_3_1 = m_tz_1 / k2rho(i, j, k) * lambdaout * (k2rho(i, j, k) - rho0)
					end if
					fluxAccrho_1 = fluxAccwnp_3_1 * 1.0d0 / (2.0d0 * c_1) + fluxAccwnm_3_1 * (-1.0d0) / (2.0d0 * c_1)
					fluxAccm_n_1 = fluxAccwnp_3_1 * (cn_1 / (2.0d0 * c_1) + 1.0d0 / 2.0d0) + fluxAccwnm_3_1 * (-(cn_1 / (2.0d0 * c_1) + (-1.0d0 / 2.0d0)))
					fluxAccm_tx_1 = (fluxAccwnp_3_1 * m_tx_1 / (2.0d0 * k2rho(i, j, k) * c_1) + fluxAccwnm_3_1 * (-m_tx_1) / (2.0d0 * k2rho(i, j, k) * c_1)) + fluxAccwtx_3_1
					fluxAccm_ty_1 = (fluxAccwnp_3_1 * m_ty_1 / (2.0d0 * k2rho(i, j, k) * c_1) + fluxAccwnm_3_1 * (-m_ty_1) / (2.0d0 * k2rho(i, j, k) * c_1)) + fluxAccwty_3_1
					fluxAccm_tz_1 = (fluxAccwnp_3_1 * m_tz_1 / (2.0d0 * k2rho(i, j, k) * c_1) + fluxAccwnm_3_1 * (-m_tz_1) / (2.0d0 * k2rho(i, j, k) * c_1)) + fluxAccwtz_3_1
					fluxAccmx_1 = fluxAccm_tx_1 + fluxAccm_n_1 * n_x_1
					fluxAccmy_1 = fluxAccm_ty_1 + fluxAccm_n_1 * n_y_1
					fluxAccmz_1 = fluxAccm_tz_1 + fluxAccm_n_1 * n_z_1
					end if
					rho(i, j, k) = RK3P3(CCTK_PASS_FTOF, fluxAccrho_1, rho_p, k2rho, i, j, k)
					mx(i, j, k) = RK3P3(CCTK_PASS_FTOF, fluxAccmx_1, mx_p, k2mx, i, j, k)
					my(i, j, k) = RK3P3(CCTK_PASS_FTOF, fluxAccmy_1, my_p, k2my, i, j, k)
					mz(i, j, k) = RK3P3(CCTK_PASS_FTOF, fluxAccmz_1, mz_p, k2mz, i, j, k)
					p(i, j, k) = (Paux + ((rho(i, j, k) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					fx(i, j, k) = pfx
					fy(i, j, k) = pfy
					fz(i, j, k) = pfz
					end if
				end do
			end do
		end do
		call CCTK_SyncGroup(status, cctkGH, "FDAsNozzle3D::fields_tl2")
		call CCTK_SyncGroup(status, cctkGH, "FDAsNozzle3D::fields_tl1")
		do k = kStart, kEnd
			do j = jStart, jEnd
				do i = iStart, iEnd
					extrapolatedsubrhosupi(i, j, k) = rho(i, j, k)
					extrapolatedsubrhosupj(i, j, k) = rho(i, j, k)
					extrapolatedsubrhosupk(i, j, k) = rho(i, j, k)
					extrapolatedsubmxsupi(i, j, k) = mx(i, j, k)
					extrapolatedsubmxsupj(i, j, k) = mx(i, j, k)
					extrapolatedsubmxsupk(i, j, k) = mx(i, j, k)
					extrapolatedsubmysupi(i, j, k) = my(i, j, k)
					extrapolatedsubmysupj(i, j, k) = my(i, j, k)
					extrapolatedsubmysupk(i, j, k) = my(i, j, k)
					extrapolatedsubmzsupi(i, j, k) = mz(i, j, k)
					extrapolatedsubmzsupj(i, j, k) = mz(i, j, k)
					extrapolatedsubmzsupk(i, j, k) = mz(i, j, k)
					extrapolatedsubpsupi(i, j, k) = p(i, j, k)
					extrapolatedsubpsupj(i, j, k) = p(i, j, k)
					extrapolatedsubpsupk(i, j, k) = p(i, j, k)
					extrapolatedsubfxsupi(i, j, k) = fx(i, j, k)
					extrapolatedsubfxsupj(i, j, k) = fx(i, j, k)
					extrapolatedsubfxsupk(i, j, k) = fx(i, j, k)
					extrapolatedsubfysupi(i, j, k) = fy(i, j, k)
					extrapolatedsubfysupj(i, j, k) = fy(i, j, k)
					extrapolatedsubfysupk(i, j, k) = fy(i, j, k)
					extrapolatedsubfzsupi(i, j, k) = fz(i, j, k)
					extrapolatedsubfzsupj(i, j, k) = fz(i, j, k)
					extrapolatedsubfzsupk(i, j, k) = fz(i, j, k)
					extrapolatedsubNullsupi(i, j, k) = Null(i, j, k)
					extrapolatedsubNullsupj(i, j, k) = Null(i, j, k)
					extrapolatedsubNullsupk(i, j, k) = Null(i, j, k)
				end do
			end do
		end do
		do k = kStart, kEnd
			do j = jStart, jEnd
				do i = iStart, iEnd
!					Boundaries
		if ((FOV_xLower(i, j, k) .gt. 0) .and. ((i + 1 .le. cctk_lsh(1) .and. (FOV_3(int(i + 1), j, k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. (FOV_3(int(i + 2), j, k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. (FOV_3(int(i + 3), j, k) .gt. 0)))) then
				Null(4 - i, j, k) = 0.0d0
		end if
		if ((FOV_xUpper(i, j, k) .gt. 0) .and. ((i - 1 .ge. 1 .and. (FOV_3(int(i - 1), j, k) .gt. 0)) .or. (i - 2 .ge. 1 .and. (FOV_3(int(i - 2), j, k) .gt. 0)) .or. (i - 3 .ge. 1 .and. (FOV_3(int(i - 3), j, k) .gt. 0)))) then
			Null(i, j, k) = 0.0d0
		end if
		if ((FOV_yLower(i, j, k) .gt. 0) .and. ((j + 1 .le. cctk_lsh(2) .and. (FOV_3(i, int(j + 1), k) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. (FOV_3(i, int(j + 2), k) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. (FOV_3(i, int(j + 3), k) .gt. 0)))) then
				Null(i, 4 - j, k) = 0.0d0
		end if
		if ((FOV_yUpper(i, j, k) .gt. 0) .and. ((j - 1 .ge. 1 .and. (FOV_3(i, int(j - 1), k) .gt. 0)) .or. (j - 2 .ge. 1 .and. (FOV_3(i, int(j - 2), k) .gt. 0)) .or. (j - 3 .ge. 1 .and. (FOV_3(i, int(j - 3), k) .gt. 0)))) then
			Null(i, j, k) = 0.0d0
		end if
		if ((FOV_zLower(i, j, k) .gt. 0) .and. ((k + 1 .le. cctk_lsh(3) .and. (FOV_3(i, j, int(k + 1)) .gt. 0)) .or. (k + 2 .le. cctk_lsh(3) .and. (FOV_3(i, j, int(k + 2)) .gt. 0)) .or. (k + 3 .le. cctk_lsh(3) .and. (FOV_3(i, j, int(k + 3)) .gt. 0)))) then
				Null(i, j, 4 - k) = 0.0d0
		end if
		if ((FOV_zUpper(i, j, k) .gt. 0) .and. ((k - 1 .ge. 1 .and. (FOV_3(i, j, int(k - 1)) .gt. 0)) .or. (k - 2 .ge. 1 .and. (FOV_3(i, j, int(k - 2)) .gt. 0)) .or. (k - 3 .ge. 1 .and. (FOV_3(i, j, int(k - 3)) .gt. 0)))) then
			Null(i, j, k) = 0.0d0
		end if
		if (FOV_3(i, j, k) .gt. 0) then
			if ((d_i_rho(i, j, k) .ne. 0 .or. d_j_rho(i, j, k) .ne. 0 .or. d_k_rho(i, j, k) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_rho, rho, extrapolatedsubrhosupi, i, d_j_rho, rho, extrapolatedsubrhosupj, j, d_k_rho, rho, extrapolatedsubrhosupk, k, rho, FOV_1)
			end if
			if ((d_i_mx(i, j, k) .ne. 0 .or. d_j_mx(i, j, k) .ne. 0 .or. d_k_mx(i, j, k) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_mx, mx, extrapolatedsubmxsupi, i, d_j_mx, mx, extrapolatedsubmxsupj, j, d_k_mx, mx, extrapolatedsubmxsupk, k, mx, FOV_1)
			end if
			if ((d_i_my(i, j, k) .ne. 0 .or. d_j_my(i, j, k) .ne. 0 .or. d_k_my(i, j, k) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_my, my, extrapolatedsubmysupi, i, d_j_my, my, extrapolatedsubmysupj, j, d_k_my, my, extrapolatedsubmysupk, k, my, FOV_1)
			end if
			if ((d_i_mz(i, j, k) .ne. 0 .or. d_j_mz(i, j, k) .ne. 0 .or. d_k_mz(i, j, k) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_mz, mz, extrapolatedsubmzsupi, i, d_j_mz, mz, extrapolatedsubmzsupj, j, d_k_mz, mz, extrapolatedsubmzsupk, k, mz, FOV_1)
			end if
			if ((d_i_fx(i, j, k) .ne. 0 .or. d_j_fx(i, j, k) .ne. 0 .or. d_k_fx(i, j, k) .ne. 0)) then
				if (FOV_1(int(i - d_i_fx(i, j, k)), int(j - d_j_fx(i, j, k)), int(k - d_k_fx(i, j, k))) .gt. 0) then
					extrapolatedsubfxsupi(i, j, k) = pfx
					extrapolatedsubfxsupj(i, j, k) = pfx
					extrapolatedsubfxsupk(i, j, k) = pfx
					if (((d_i_fx(i, j, k) .ne. 0.0d0) .and. (d_j_fx(i, j, k) .ne. 0.0d0)) .and. (d_k_fx(i, j, k) .ne. 0.0d0)) then
					fx(i, j, k) = ((extrapolatedsubfxsupi(i, j, k) + extrapolatedsubfxsupj(i, j, k)) + extrapolatedsubfxsupk(i, j, k)) / 3.0d0
					else
					if (((d_i_fx(i, j, k) .ne. 0.0d0) .and. (d_j_fx(i, j, k) .ne. 0.0d0)) .and. (d_k_fx(i, j, k) .eq. 0.0d0)) then
					fx(i, j, k) = (extrapolatedsubfxsupi(i, j, k) + extrapolatedsubfxsupj(i, j, k)) / 2.0d0
					extrapolatedsubfxsupk(i, j, k) = fx(i, j, k)
					else
					if (((d_i_fx(i, j, k) .ne. 0.0d0) .and. (d_j_fx(i, j, k) .eq. 0.0d0)) .and. (d_k_fx(i, j, k) .ne. 0.0d0)) then
					fx(i, j, k) = (extrapolatedsubfxsupi(i, j, k) + extrapolatedsubfxsupk(i, j, k)) / 2.0d0
					extrapolatedsubfxsupj(i, j, k) = fx(i, j, k)
					else
					if (((d_i_fx(i, j, k) .ne. 0.0d0) .and. (d_j_fx(i, j, k) .eq. 0.0d0)) .and. (d_k_fx(i, j, k) .eq. 0.0d0)) then
					fx(i, j, k) = extrapolatedsubfxsupi(i, j, k)
					extrapolatedsubfxsupj(i, j, k) = fx(i, j, k)
					extrapolatedsubfxsupk(i, j, k) = fx(i, j, k)
					else
					if (((d_i_fx(i, j, k) .eq. 0.0d0) .and. (d_j_fx(i, j, k) .ne. 0.0d0)) .and. (d_k_fx(i, j, k) .ne. 0.0d0)) then
					fx(i, j, k) = (extrapolatedsubfxsupj(i, j, k) + extrapolatedsubfxsupk(i, j, k)) / 2.0d0
					extrapolatedsubfxsupi(i, j, k) = fx(i, j, k)
					else
					if (((d_i_fx(i, j, k) .eq. 0.0d0) .and. (d_j_fx(i, j, k) .ne. 0.0d0)) .and. (d_k_fx(i, j, k) .eq. 0.0d0)) then
					fx(i, j, k) = extrapolatedsubfxsupj(i, j, k)
					extrapolatedsubfxsupi(i, j, k) = fx(i, j, k)
					extrapolatedsubfxsupk(i, j, k) = fx(i, j, k)
					else
					if (((d_i_fx(i, j, k) .eq. 0.0d0) .and. (d_j_fx(i, j, k) .eq. 0.0d0)) .and. (d_k_fx(i, j, k) .ne. 0.0d0)) then
					fx(i, j, k) = extrapolatedsubfxsupk(i, j, k)
					extrapolatedsubfxsupi(i, j, k) = fx(i, j, k)
					extrapolatedsubfxsupj(i, j, k) = fx(i, j, k)
					end if
					end if
					end if
					end if
					end if
					end if
					end if
				end if
			end if
			if ((d_i_fy(i, j, k) .ne. 0 .or. d_j_fy(i, j, k) .ne. 0 .or. d_k_fy(i, j, k) .ne. 0)) then
				if (FOV_1(int(i - d_i_fy(i, j, k)), int(j - d_j_fy(i, j, k)), int(k - d_k_fy(i, j, k))) .gt. 0) then
					extrapolatedsubfysupi(i, j, k) = pfy
					extrapolatedsubfysupj(i, j, k) = pfy
					extrapolatedsubfysupk(i, j, k) = pfy
					if (((d_i_fy(i, j, k) .ne. 0.0d0) .and. (d_j_fy(i, j, k) .ne. 0.0d0)) .and. (d_k_fy(i, j, k) .ne. 0.0d0)) then
					fy(i, j, k) = ((extrapolatedsubfysupi(i, j, k) + extrapolatedsubfysupj(i, j, k)) + extrapolatedsubfysupk(i, j, k)) / 3.0d0
					else
					if (((d_i_fy(i, j, k) .ne. 0.0d0) .and. (d_j_fy(i, j, k) .ne. 0.0d0)) .and. (d_k_fy(i, j, k) .eq. 0.0d0)) then
					fy(i, j, k) = (extrapolatedsubfysupi(i, j, k) + extrapolatedsubfysupj(i, j, k)) / 2.0d0
					extrapolatedsubfysupk(i, j, k) = fy(i, j, k)
					else
					if (((d_i_fy(i, j, k) .ne. 0.0d0) .and. (d_j_fy(i, j, k) .eq. 0.0d0)) .and. (d_k_fy(i, j, k) .ne. 0.0d0)) then
					fy(i, j, k) = (extrapolatedsubfysupi(i, j, k) + extrapolatedsubfysupk(i, j, k)) / 2.0d0
					extrapolatedsubfysupj(i, j, k) = fy(i, j, k)
					else
					if (((d_i_fy(i, j, k) .ne. 0.0d0) .and. (d_j_fy(i, j, k) .eq. 0.0d0)) .and. (d_k_fy(i, j, k) .eq. 0.0d0)) then
					fy(i, j, k) = extrapolatedsubfysupi(i, j, k)
					extrapolatedsubfysupj(i, j, k) = fy(i, j, k)
					extrapolatedsubfysupk(i, j, k) = fy(i, j, k)
					else
					if (((d_i_fy(i, j, k) .eq. 0.0d0) .and. (d_j_fy(i, j, k) .ne. 0.0d0)) .and. (d_k_fy(i, j, k) .ne. 0.0d0)) then
					fy(i, j, k) = (extrapolatedsubfysupj(i, j, k) + extrapolatedsubfysupk(i, j, k)) / 2.0d0
					extrapolatedsubfysupi(i, j, k) = fy(i, j, k)
					else
					if (((d_i_fy(i, j, k) .eq. 0.0d0) .and. (d_j_fy(i, j, k) .ne. 0.0d0)) .and. (d_k_fy(i, j, k) .eq. 0.0d0)) then
					fy(i, j, k) = extrapolatedsubfysupj(i, j, k)
					extrapolatedsubfysupi(i, j, k) = fy(i, j, k)
					extrapolatedsubfysupk(i, j, k) = fy(i, j, k)
					else
					if (((d_i_fy(i, j, k) .eq. 0.0d0) .and. (d_j_fy(i, j, k) .eq. 0.0d0)) .and. (d_k_fy(i, j, k) .ne. 0.0d0)) then
					fy(i, j, k) = extrapolatedsubfysupk(i, j, k)
					extrapolatedsubfysupi(i, j, k) = fy(i, j, k)
					extrapolatedsubfysupj(i, j, k) = fy(i, j, k)
					end if
					end if
					end if
					end if
					end if
					end if
					end if
				end if
			end if
			if ((d_i_fz(i, j, k) .ne. 0 .or. d_j_fz(i, j, k) .ne. 0 .or. d_k_fz(i, j, k) .ne. 0)) then
				if (FOV_1(int(i - d_i_fz(i, j, k)), int(j - d_j_fz(i, j, k)), int(k - d_k_fz(i, j, k))) .gt. 0) then
					extrapolatedsubfzsupi(i, j, k) = pfz
					extrapolatedsubfzsupj(i, j, k) = pfz
					extrapolatedsubfzsupk(i, j, k) = pfz
					if (((d_i_fz(i, j, k) .ne. 0.0d0) .and. (d_j_fz(i, j, k) .ne. 0.0d0)) .and. (d_k_fz(i, j, k) .ne. 0.0d0)) then
					fz(i, j, k) = ((extrapolatedsubfzsupi(i, j, k) + extrapolatedsubfzsupj(i, j, k)) + extrapolatedsubfzsupk(i, j, k)) / 3.0d0
					else
					if (((d_i_fz(i, j, k) .ne. 0.0d0) .and. (d_j_fz(i, j, k) .ne. 0.0d0)) .and. (d_k_fz(i, j, k) .eq. 0.0d0)) then
					fz(i, j, k) = (extrapolatedsubfzsupi(i, j, k) + extrapolatedsubfzsupj(i, j, k)) / 2.0d0
					extrapolatedsubfzsupk(i, j, k) = fz(i, j, k)
					else
					if (((d_i_fz(i, j, k) .ne. 0.0d0) .and. (d_j_fz(i, j, k) .eq. 0.0d0)) .and. (d_k_fz(i, j, k) .ne. 0.0d0)) then
					fz(i, j, k) = (extrapolatedsubfzsupi(i, j, k) + extrapolatedsubfzsupk(i, j, k)) / 2.0d0
					extrapolatedsubfzsupj(i, j, k) = fz(i, j, k)
					else
					if (((d_i_fz(i, j, k) .ne. 0.0d0) .and. (d_j_fz(i, j, k) .eq. 0.0d0)) .and. (d_k_fz(i, j, k) .eq. 0.0d0)) then
					fz(i, j, k) = extrapolatedsubfzsupi(i, j, k)
					extrapolatedsubfzsupj(i, j, k) = fz(i, j, k)
					extrapolatedsubfzsupk(i, j, k) = fz(i, j, k)
					else
					if (((d_i_fz(i, j, k) .eq. 0.0d0) .and. (d_j_fz(i, j, k) .ne. 0.0d0)) .and. (d_k_fz(i, j, k) .ne. 0.0d0)) then
					fz(i, j, k) = (extrapolatedsubfzsupj(i, j, k) + extrapolatedsubfzsupk(i, j, k)) / 2.0d0
					extrapolatedsubfzsupi(i, j, k) = fz(i, j, k)
					else
					if (((d_i_fz(i, j, k) .eq. 0.0d0) .and. (d_j_fz(i, j, k) .ne. 0.0d0)) .and. (d_k_fz(i, j, k) .eq. 0.0d0)) then
					fz(i, j, k) = extrapolatedsubfzsupj(i, j, k)
					extrapolatedsubfzsupi(i, j, k) = fz(i, j, k)
					extrapolatedsubfzsupk(i, j, k) = fz(i, j, k)
					else
					if (((d_i_fz(i, j, k) .eq. 0.0d0) .and. (d_j_fz(i, j, k) .eq. 0.0d0)) .and. (d_k_fz(i, j, k) .ne. 0.0d0)) then
					fz(i, j, k) = extrapolatedsubfzsupk(i, j, k)
					extrapolatedsubfzsupi(i, j, k) = fz(i, j, k)
					extrapolatedsubfzsupj(i, j, k) = fz(i, j, k)
					end if
					end if
					end if
					end if
					end if
					end if
					end if
				end if
			end if
			if ((d_i_p(i, j, k) .ne. 0 .or. d_j_p(i, j, k) .ne. 0 .or. d_k_p(i, j, k) .ne. 0)) then
				if (FOV_1(int(i - d_i_p(i, j, k)), int(j - d_j_p(i, j, k)), int(k - d_k_p(i, j, k))) .gt. 0) then
					extrapolatedsubpsupi(i, j, k) = (Paux + ((extrapolatedsubrhosupi(i, j, k) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					extrapolatedsubpsupj(i, j, k) = (Paux + ((extrapolatedsubrhosupj(i, j, k) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					extrapolatedsubpsupk(i, j, k) = (Paux + ((extrapolatedsubrhosupk(i, j, k) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					if (((d_i_p(i, j, k) .ne. 0.0d0) .and. (d_j_p(i, j, k) .ne. 0.0d0)) .and. (d_k_p(i, j, k) .ne. 0.0d0)) then
					p(i, j, k) = ((extrapolatedsubpsupi(i, j, k) + extrapolatedsubpsupj(i, j, k)) + extrapolatedsubpsupk(i, j, k)) / 3.0d0
					else
					if (((d_i_p(i, j, k) .ne. 0.0d0) .and. (d_j_p(i, j, k) .ne. 0.0d0)) .and. (d_k_p(i, j, k) .eq. 0.0d0)) then
					p(i, j, k) = (extrapolatedsubpsupi(i, j, k) + extrapolatedsubpsupj(i, j, k)) / 2.0d0
					extrapolatedsubpsupk(i, j, k) = p(i, j, k)
					else
					if (((d_i_p(i, j, k) .ne. 0.0d0) .and. (d_j_p(i, j, k) .eq. 0.0d0)) .and. (d_k_p(i, j, k) .ne. 0.0d0)) then
					p(i, j, k) = (extrapolatedsubpsupi(i, j, k) + extrapolatedsubpsupk(i, j, k)) / 2.0d0
					extrapolatedsubpsupj(i, j, k) = p(i, j, k)
					else
					if (((d_i_p(i, j, k) .ne. 0.0d0) .and. (d_j_p(i, j, k) .eq. 0.0d0)) .and. (d_k_p(i, j, k) .eq. 0.0d0)) then
					p(i, j, k) = extrapolatedsubpsupi(i, j, k)
					extrapolatedsubpsupj(i, j, k) = p(i, j, k)
					extrapolatedsubpsupk(i, j, k) = p(i, j, k)
					else
					if (((d_i_p(i, j, k) .eq. 0.0d0) .and. (d_j_p(i, j, k) .ne. 0.0d0)) .and. (d_k_p(i, j, k) .ne. 0.0d0)) then
					p(i, j, k) = (extrapolatedsubpsupj(i, j, k) + extrapolatedsubpsupk(i, j, k)) / 2.0d0
					extrapolatedsubpsupi(i, j, k) = p(i, j, k)
					else
					if (((d_i_p(i, j, k) .eq. 0.0d0) .and. (d_j_p(i, j, k) .ne. 0.0d0)) .and. (d_k_p(i, j, k) .eq. 0.0d0)) then
					p(i, j, k) = extrapolatedsubpsupj(i, j, k)
					extrapolatedsubpsupi(i, j, k) = p(i, j, k)
					extrapolatedsubpsupk(i, j, k) = p(i, j, k)
					else
					if (((d_i_p(i, j, k) .eq. 0.0d0) .and. (d_j_p(i, j, k) .eq. 0.0d0)) .and. (d_k_p(i, j, k) .ne. 0.0d0)) then
					p(i, j, k) = extrapolatedsubpsupk(i, j, k)
					extrapolatedsubpsupi(i, j, k) = p(i, j, k)
					extrapolatedsubpsupj(i, j, k) = p(i, j, k)
					end if
					end if
					end if
					end if
					end if
					end if
					end if
				end if
			end if
		end if
!					Boundaries
		if ((FOV_xLower(i, j, k) .gt. 0) .and. ((i + 1 .le. cctk_lsh(1) .and. (FOV_1(int(i + 1), j, k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. (FOV_1(int(i + 2), j, k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. (FOV_1(int(i + 3), j, k) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 1), k) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 1), k) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 2), k) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 2), k) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 3), k) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 3), k) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(int(i + 1), j, int(k + 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. k - 1 .ge. 1 .and. (FOV_1(int(i + 1), j, int(k - 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(int(i + 1), j, int(k + 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. k - 2 .ge. 1 .and. (FOV_1(int(i + 1), j, int(k - 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(int(i + 1), j, int(k + 3)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. k - 3 .ge. 1 .and. (FOV_1(int(i + 1), j, int(k - 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 1), k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 1), k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 2), k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 2), k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 3), k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 3), k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(int(i + 2), j, int(k + 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. k - 1 .ge. 1 .and. (FOV_1(int(i + 2), j, int(k - 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(int(i + 2), j, int(k + 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. k - 2 .ge. 1 .and. (FOV_1(int(i + 2), j, int(k - 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(int(i + 2), j, int(k + 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. k - 3 .ge. 1 .and. (FOV_1(int(i + 2), j, int(k - 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 1), k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 1), k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 2), k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 2), k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 3), k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 3), k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(int(i + 3), j, int(k + 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. k - 1 .ge. 1 .and. (FOV_1(int(i + 3), j, int(k - 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(int(i + 3), j, int(k + 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. k - 2 .ge. 1 .and. (FOV_1(int(i + 3), j, int(k - 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(int(i + 3), j, int(k + 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. k - 3 .ge. 1 .and. (FOV_1(int(i + 3), j, int(k - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j, k) .ne. 0 .or. d_j_rho(i, j, k) .ne. 0 .or. d_k_rho(i, j, k) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_rho, rho, extrapolatedsubrhosupi, i, d_j_rho, rho, extrapolatedsubrhosupj, j, d_k_rho, rho, extrapolatedsubrhosupk, k, rho, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j, k) .ne. 0 .or. d_j_mx(i, j, k) .ne. 0 .or. d_k_mx(i, j, k) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_mx, mx, extrapolatedsubmxsupi, i, d_j_mx, mx, extrapolatedsubmxsupj, j, d_k_mx, mx, extrapolatedsubmxsupk, k, mx, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j, k) .ne. 0 .or. d_j_my(i, j, k) .ne. 0 .or. d_k_my(i, j, k) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_my, my, extrapolatedsubmysupi, i, d_j_my, my, extrapolatedsubmysupj, j, d_k_my, my, extrapolatedsubmysupk, k, my, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mz(i, j, k) .ne. 0 .or. d_j_mz(i, j, k) .ne. 0 .or. d_k_mz(i, j, k) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_mz, mz, extrapolatedsubmzsupi, i, d_j_mz, mz, extrapolatedsubmzsupj, j, d_k_mz, mz, extrapolatedsubmzsupk, k, mz, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_p(i, j, k) .ne. 0 .or. d_j_p(i, j, k) .ne. 0 .or. d_k_p(i, j, k) .ne. 0)) then
				if (FOV_1(int(i - d_i_p(i, j, k)), int(j - d_j_p(i, j, k)), int(k - d_k_p(i, j, k))) .gt. 0) then
					extrapolatedsubpsupi(i, j, k) = (Paux + ((extrapolatedsubrhosupi(i, j, k) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					extrapolatedsubpsupj(i, j, k) = (Paux + ((extrapolatedsubrhosupj(i, j, k) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					extrapolatedsubpsupk(i, j, k) = (Paux + ((extrapolatedsubrhosupk(i, j, k) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					if (((d_i_p(i, j, k) .ne. 0.0d0) .and. (d_j_p(i, j, k) .ne. 0.0d0)) .and. (d_k_p(i, j, k) .ne. 0.0d0)) then
					p(i, j, k) = ((extrapolatedsubpsupi(i, j, k) + extrapolatedsubpsupj(i, j, k)) + extrapolatedsubpsupk(i, j, k)) / 3.0d0
					else
					if (((d_i_p(i, j, k) .ne. 0.0d0) .and. (d_j_p(i, j, k) .ne. 0.0d0)) .and. (d_k_p(i, j, k) .eq. 0.0d0)) then
					p(i, j, k) = (extrapolatedsubpsupi(i, j, k) + extrapolatedsubpsupj(i, j, k)) / 2.0d0
					extrapolatedsubpsupk(i, j, k) = p(i, j, k)
					else
					if (((d_i_p(i, j, k) .ne. 0.0d0) .and. (d_j_p(i, j, k) .eq. 0.0d0)) .and. (d_k_p(i, j, k) .ne. 0.0d0)) then
					p(i, j, k) = (extrapolatedsubpsupi(i, j, k) + extrapolatedsubpsupk(i, j, k)) / 2.0d0
					extrapolatedsubpsupj(i, j, k) = p(i, j, k)
					else
					if (((d_i_p(i, j, k) .ne. 0.0d0) .and. (d_j_p(i, j, k) .eq. 0.0d0)) .and. (d_k_p(i, j, k) .eq. 0.0d0)) then
					p(i, j, k) = extrapolatedsubpsupi(i, j, k)
					extrapolatedsubpsupj(i, j, k) = p(i, j, k)
					extrapolatedsubpsupk(i, j, k) = p(i, j, k)
					else
					if (((d_i_p(i, j, k) .eq. 0.0d0) .and. (d_j_p(i, j, k) .ne. 0.0d0)) .and. (d_k_p(i, j, k) .ne. 0.0d0)) then
					p(i, j, k) = (extrapolatedsubpsupj(i, j, k) + extrapolatedsubpsupk(i, j, k)) / 2.0d0
					extrapolatedsubpsupi(i, j, k) = p(i, j, k)
					else
					if (((d_i_p(i, j, k) .eq. 0.0d0) .and. (d_j_p(i, j, k) .ne. 0.0d0)) .and. (d_k_p(i, j, k) .eq. 0.0d0)) then
					p(i, j, k) = extrapolatedsubpsupj(i, j, k)
					extrapolatedsubpsupi(i, j, k) = p(i, j, k)
					extrapolatedsubpsupk(i, j, k) = p(i, j, k)
					else
					if (((d_i_p(i, j, k) .eq. 0.0d0) .and. (d_j_p(i, j, k) .eq. 0.0d0)) .and. (d_k_p(i, j, k) .ne. 0.0d0)) then
					p(i, j, k) = extrapolatedsubpsupk(i, j, k)
					extrapolatedsubpsupi(i, j, k) = p(i, j, k)
					extrapolatedsubpsupj(i, j, k) = p(i, j, k)
					end if
					end if
					end if
					end if
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fx(i, j, k) .ne. 0 .or. d_j_fx(i, j, k) .ne. 0 .or. d_k_fx(i, j, k) .ne. 0)) then
				if (FOV_1(int(i - d_i_fx(i, j, k)), int(j - d_j_fx(i, j, k)), int(k - d_k_fx(i, j, k))) .gt. 0) then
					extrapolatedsubfxsupi(i, j, k) = pfx
					extrapolatedsubfxsupj(i, j, k) = pfx
					extrapolatedsubfxsupk(i, j, k) = pfx
					if (((d_i_fx(i, j, k) .ne. 0.0d0) .and. (d_j_fx(i, j, k) .ne. 0.0d0)) .and. (d_k_fx(i, j, k) .ne. 0.0d0)) then
					fx(i, j, k) = ((extrapolatedsubfxsupi(i, j, k) + extrapolatedsubfxsupj(i, j, k)) + extrapolatedsubfxsupk(i, j, k)) / 3.0d0
					else
					if (((d_i_fx(i, j, k) .ne. 0.0d0) .and. (d_j_fx(i, j, k) .ne. 0.0d0)) .and. (d_k_fx(i, j, k) .eq. 0.0d0)) then
					fx(i, j, k) = (extrapolatedsubfxsupi(i, j, k) + extrapolatedsubfxsupj(i, j, k)) / 2.0d0
					extrapolatedsubfxsupk(i, j, k) = fx(i, j, k)
					else
					if (((d_i_fx(i, j, k) .ne. 0.0d0) .and. (d_j_fx(i, j, k) .eq. 0.0d0)) .and. (d_k_fx(i, j, k) .ne. 0.0d0)) then
					fx(i, j, k) = (extrapolatedsubfxsupi(i, j, k) + extrapolatedsubfxsupk(i, j, k)) / 2.0d0
					extrapolatedsubfxsupj(i, j, k) = fx(i, j, k)
					else
					if (((d_i_fx(i, j, k) .ne. 0.0d0) .and. (d_j_fx(i, j, k) .eq. 0.0d0)) .and. (d_k_fx(i, j, k) .eq. 0.0d0)) then
					fx(i, j, k) = extrapolatedsubfxsupi(i, j, k)
					extrapolatedsubfxsupj(i, j, k) = fx(i, j, k)
					extrapolatedsubfxsupk(i, j, k) = fx(i, j, k)
					else
					if (((d_i_fx(i, j, k) .eq. 0.0d0) .and. (d_j_fx(i, j, k) .ne. 0.0d0)) .and. (d_k_fx(i, j, k) .ne. 0.0d0)) then
					fx(i, j, k) = (extrapolatedsubfxsupj(i, j, k) + extrapolatedsubfxsupk(i, j, k)) / 2.0d0
					extrapolatedsubfxsupi(i, j, k) = fx(i, j, k)
					else
					if (((d_i_fx(i, j, k) .eq. 0.0d0) .and. (d_j_fx(i, j, k) .ne. 0.0d0)) .and. (d_k_fx(i, j, k) .eq. 0.0d0)) then
					fx(i, j, k) = extrapolatedsubfxsupj(i, j, k)
					extrapolatedsubfxsupi(i, j, k) = fx(i, j, k)
					extrapolatedsubfxsupk(i, j, k) = fx(i, j, k)
					else
					if (((d_i_fx(i, j, k) .eq. 0.0d0) .and. (d_j_fx(i, j, k) .eq. 0.0d0)) .and. (d_k_fx(i, j, k) .ne. 0.0d0)) then
					fx(i, j, k) = extrapolatedsubfxsupk(i, j, k)
					extrapolatedsubfxsupi(i, j, k) = fx(i, j, k)
					extrapolatedsubfxsupj(i, j, k) = fx(i, j, k)
					end if
					end if
					end if
					end if
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fy(i, j, k) .ne. 0 .or. d_j_fy(i, j, k) .ne. 0 .or. d_k_fy(i, j, k) .ne. 0)) then
				if (FOV_1(int(i - d_i_fy(i, j, k)), int(j - d_j_fy(i, j, k)), int(k - d_k_fy(i, j, k))) .gt. 0) then
					extrapolatedsubfysupi(i, j, k) = pfy
					extrapolatedsubfysupj(i, j, k) = pfy
					extrapolatedsubfysupk(i, j, k) = pfy
					if (((d_i_fy(i, j, k) .ne. 0.0d0) .and. (d_j_fy(i, j, k) .ne. 0.0d0)) .and. (d_k_fy(i, j, k) .ne. 0.0d0)) then
					fy(i, j, k) = ((extrapolatedsubfysupi(i, j, k) + extrapolatedsubfysupj(i, j, k)) + extrapolatedsubfysupk(i, j, k)) / 3.0d0
					else
					if (((d_i_fy(i, j, k) .ne. 0.0d0) .and. (d_j_fy(i, j, k) .ne. 0.0d0)) .and. (d_k_fy(i, j, k) .eq. 0.0d0)) then
					fy(i, j, k) = (extrapolatedsubfysupi(i, j, k) + extrapolatedsubfysupj(i, j, k)) / 2.0d0
					extrapolatedsubfysupk(i, j, k) = fy(i, j, k)
					else
					if (((d_i_fy(i, j, k) .ne. 0.0d0) .and. (d_j_fy(i, j, k) .eq. 0.0d0)) .and. (d_k_fy(i, j, k) .ne. 0.0d0)) then
					fy(i, j, k) = (extrapolatedsubfysupi(i, j, k) + extrapolatedsubfysupk(i, j, k)) / 2.0d0
					extrapolatedsubfysupj(i, j, k) = fy(i, j, k)
					else
					if (((d_i_fy(i, j, k) .ne. 0.0d0) .and. (d_j_fy(i, j, k) .eq. 0.0d0)) .and. (d_k_fy(i, j, k) .eq. 0.0d0)) then
					fy(i, j, k) = extrapolatedsubfysupi(i, j, k)
					extrapolatedsubfysupj(i, j, k) = fy(i, j, k)
					extrapolatedsubfysupk(i, j, k) = fy(i, j, k)
					else
					if (((d_i_fy(i, j, k) .eq. 0.0d0) .and. (d_j_fy(i, j, k) .ne. 0.0d0)) .and. (d_k_fy(i, j, k) .ne. 0.0d0)) then
					fy(i, j, k) = (extrapolatedsubfysupj(i, j, k) + extrapolatedsubfysupk(i, j, k)) / 2.0d0
					extrapolatedsubfysupi(i, j, k) = fy(i, j, k)
					else
					if (((d_i_fy(i, j, k) .eq. 0.0d0) .and. (d_j_fy(i, j, k) .ne. 0.0d0)) .and. (d_k_fy(i, j, k) .eq. 0.0d0)) then
					fy(i, j, k) = extrapolatedsubfysupj(i, j, k)
					extrapolatedsubfysupi(i, j, k) = fy(i, j, k)
					extrapolatedsubfysupk(i, j, k) = fy(i, j, k)
					else
					if (((d_i_fy(i, j, k) .eq. 0.0d0) .and. (d_j_fy(i, j, k) .eq. 0.0d0)) .and. (d_k_fy(i, j, k) .ne. 0.0d0)) then
					fy(i, j, k) = extrapolatedsubfysupk(i, j, k)
					extrapolatedsubfysupi(i, j, k) = fy(i, j, k)
					extrapolatedsubfysupj(i, j, k) = fy(i, j, k)
					end if
					end if
					end if
					end if
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fz(i, j, k) .ne. 0 .or. d_j_fz(i, j, k) .ne. 0 .or. d_k_fz(i, j, k) .ne. 0)) then
				if (FOV_1(int(i - d_i_fz(i, j, k)), int(j - d_j_fz(i, j, k)), int(k - d_k_fz(i, j, k))) .gt. 0) then
					extrapolatedsubfzsupi(i, j, k) = pfz
					extrapolatedsubfzsupj(i, j, k) = pfz
					extrapolatedsubfzsupk(i, j, k) = pfz
					if (((d_i_fz(i, j, k) .ne. 0.0d0) .and. (d_j_fz(i, j, k) .ne. 0.0d0)) .and. (d_k_fz(i, j, k) .ne. 0.0d0)) then
					fz(i, j, k) = ((extrapolatedsubfzsupi(i, j, k) + extrapolatedsubfzsupj(i, j, k)) + extrapolatedsubfzsupk(i, j, k)) / 3.0d0
					else
					if (((d_i_fz(i, j, k) .ne. 0.0d0) .and. (d_j_fz(i, j, k) .ne. 0.0d0)) .and. (d_k_fz(i, j, k) .eq. 0.0d0)) then
					fz(i, j, k) = (extrapolatedsubfzsupi(i, j, k) + extrapolatedsubfzsupj(i, j, k)) / 2.0d0
					extrapolatedsubfzsupk(i, j, k) = fz(i, j, k)
					else
					if (((d_i_fz(i, j, k) .ne. 0.0d0) .and. (d_j_fz(i, j, k) .eq. 0.0d0)) .and. (d_k_fz(i, j, k) .ne. 0.0d0)) then
					fz(i, j, k) = (extrapolatedsubfzsupi(i, j, k) + extrapolatedsubfzsupk(i, j, k)) / 2.0d0
					extrapolatedsubfzsupj(i, j, k) = fz(i, j, k)
					else
					if (((d_i_fz(i, j, k) .ne. 0.0d0) .and. (d_j_fz(i, j, k) .eq. 0.0d0)) .and. (d_k_fz(i, j, k) .eq. 0.0d0)) then
					fz(i, j, k) = extrapolatedsubfzsupi(i, j, k)
					extrapolatedsubfzsupj(i, j, k) = fz(i, j, k)
					extrapolatedsubfzsupk(i, j, k) = fz(i, j, k)
					else
					if (((d_i_fz(i, j, k) .eq. 0.0d0) .and. (d_j_fz(i, j, k) .ne. 0.0d0)) .and. (d_k_fz(i, j, k) .ne. 0.0d0)) then
					fz(i, j, k) = (extrapolatedsubfzsupj(i, j, k) + extrapolatedsubfzsupk(i, j, k)) / 2.0d0
					extrapolatedsubfzsupi(i, j, k) = fz(i, j, k)
					else
					if (((d_i_fz(i, j, k) .eq. 0.0d0) .and. (d_j_fz(i, j, k) .ne. 0.0d0)) .and. (d_k_fz(i, j, k) .eq. 0.0d0)) then
					fz(i, j, k) = extrapolatedsubfzsupj(i, j, k)
					extrapolatedsubfzsupi(i, j, k) = fz(i, j, k)
					extrapolatedsubfzsupk(i, j, k) = fz(i, j, k)
					else
					if (((d_i_fz(i, j, k) .eq. 0.0d0) .and. (d_j_fz(i, j, k) .eq. 0.0d0)) .and. (d_k_fz(i, j, k) .ne. 0.0d0)) then
					fz(i, j, k) = extrapolatedsubfzsupk(i, j, k)
					extrapolatedsubfzsupi(i, j, k) = fz(i, j, k)
					extrapolatedsubfzsupj(i, j, k) = fz(i, j, k)
					end if
					end if
					end if
					end if
					end if
					end if
					end if
				end if
			end if
		end if
		if ((FOV_xUpper(i, j, k) .gt. 0) .and. ((i - 1 .ge. 1 .and. (FOV_1(int(i - 1), j, k) .gt. 0)) .or. (i - 2 .ge. 1 .and. (FOV_1(int(i - 2), j, k) .gt. 0)) .or. (i - 3 .ge. 1 .and. (FOV_1(int(i - 3), j, k) .gt. 0)) .or. ((i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 1), k) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 1), k) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 2), k) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 2), k) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 3), k) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 3), k) .gt. 0)) .or. (i - 1 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(int(i - 1), j, int(k + 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. k - 1 .ge. 1 .and. (FOV_1(int(i - 1), j, int(k - 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(int(i - 1), j, int(k + 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. k - 2 .ge. 1 .and. (FOV_1(int(i - 1), j, int(k - 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(int(i - 1), j, int(k + 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. k - 3 .ge. 1 .and. (FOV_1(int(i - 1), j, int(k - 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 1), k) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 1), k) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 2), k) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 2), k) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 3), k) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 3), k) .gt. 0)) .or. (i - 2 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(int(i - 2), j, int(k + 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. k - 1 .ge. 1 .and. (FOV_1(int(i - 2), j, int(k - 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(int(i - 2), j, int(k + 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. k - 2 .ge. 1 .and. (FOV_1(int(i - 2), j, int(k - 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(int(i - 2), j, int(k + 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. k - 3 .ge. 1 .and. (FOV_1(int(i - 2), j, int(k - 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 1), k) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 1), k) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 2), k) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 2), k) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 3), k) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 3), k) .gt. 0)) .or. (i - 3 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(int(i - 3), j, int(k + 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. k - 1 .ge. 1 .and. (FOV_1(int(i - 3), j, int(k - 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(int(i - 3), j, int(k + 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. k - 2 .ge. 1 .and. (FOV_1(int(i - 3), j, int(k - 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(int(i - 3), j, int(k + 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. k - 3 .ge. 1 .and. (FOV_1(int(i - 3), j, int(k - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j, k) .ne. 0 .or. d_j_rho(i, j, k) .ne. 0 .or. d_k_rho(i, j, k) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_rho, rho, extrapolatedsubrhosupi, i, d_j_rho, rho, extrapolatedsubrhosupj, j, d_k_rho, rho, extrapolatedsubrhosupk, k, rho, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j, k) .ne. 0 .or. d_j_mx(i, j, k) .ne. 0 .or. d_k_mx(i, j, k) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_mx, mx, extrapolatedsubmxsupi, i, d_j_mx, mx, extrapolatedsubmxsupj, j, d_k_mx, mx, extrapolatedsubmxsupk, k, mx, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j, k) .ne. 0 .or. d_j_my(i, j, k) .ne. 0 .or. d_k_my(i, j, k) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_my, my, extrapolatedsubmysupi, i, d_j_my, my, extrapolatedsubmysupj, j, d_k_my, my, extrapolatedsubmysupk, k, my, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mz(i, j, k) .ne. 0 .or. d_j_mz(i, j, k) .ne. 0 .or. d_k_mz(i, j, k) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_mz, mz, extrapolatedsubmzsupi, i, d_j_mz, mz, extrapolatedsubmzsupj, j, d_k_mz, mz, extrapolatedsubmzsupk, k, mz, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_p(i, j, k) .ne. 0 .or. d_j_p(i, j, k) .ne. 0 .or. d_k_p(i, j, k) .ne. 0)) then
				if (FOV_1(int(i - d_i_p(i, j, k)), int(j - d_j_p(i, j, k)), int(k - d_k_p(i, j, k))) .gt. 0) then
					extrapolatedsubpsupi(i, j, k) = (Paux + ((extrapolatedsubrhosupi(i, j, k) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					extrapolatedsubpsupj(i, j, k) = (Paux + ((extrapolatedsubrhosupj(i, j, k) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					extrapolatedsubpsupk(i, j, k) = (Paux + ((extrapolatedsubrhosupk(i, j, k) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					if (((d_i_p(i, j, k) .ne. 0.0d0) .and. (d_j_p(i, j, k) .ne. 0.0d0)) .and. (d_k_p(i, j, k) .ne. 0.0d0)) then
					p(i, j, k) = ((extrapolatedsubpsupi(i, j, k) + extrapolatedsubpsupj(i, j, k)) + extrapolatedsubpsupk(i, j, k)) / 3.0d0
					else
					if (((d_i_p(i, j, k) .ne. 0.0d0) .and. (d_j_p(i, j, k) .ne. 0.0d0)) .and. (d_k_p(i, j, k) .eq. 0.0d0)) then
					p(i, j, k) = (extrapolatedsubpsupi(i, j, k) + extrapolatedsubpsupj(i, j, k)) / 2.0d0
					extrapolatedsubpsupk(i, j, k) = p(i, j, k)
					else
					if (((d_i_p(i, j, k) .ne. 0.0d0) .and. (d_j_p(i, j, k) .eq. 0.0d0)) .and. (d_k_p(i, j, k) .ne. 0.0d0)) then
					p(i, j, k) = (extrapolatedsubpsupi(i, j, k) + extrapolatedsubpsupk(i, j, k)) / 2.0d0
					extrapolatedsubpsupj(i, j, k) = p(i, j, k)
					else
					if (((d_i_p(i, j, k) .ne. 0.0d0) .and. (d_j_p(i, j, k) .eq. 0.0d0)) .and. (d_k_p(i, j, k) .eq. 0.0d0)) then
					p(i, j, k) = extrapolatedsubpsupi(i, j, k)
					extrapolatedsubpsupj(i, j, k) = p(i, j, k)
					extrapolatedsubpsupk(i, j, k) = p(i, j, k)
					else
					if (((d_i_p(i, j, k) .eq. 0.0d0) .and. (d_j_p(i, j, k) .ne. 0.0d0)) .and. (d_k_p(i, j, k) .ne. 0.0d0)) then
					p(i, j, k) = (extrapolatedsubpsupj(i, j, k) + extrapolatedsubpsupk(i, j, k)) / 2.0d0
					extrapolatedsubpsupi(i, j, k) = p(i, j, k)
					else
					if (((d_i_p(i, j, k) .eq. 0.0d0) .and. (d_j_p(i, j, k) .ne. 0.0d0)) .and. (d_k_p(i, j, k) .eq. 0.0d0)) then
					p(i, j, k) = extrapolatedsubpsupj(i, j, k)
					extrapolatedsubpsupi(i, j, k) = p(i, j, k)
					extrapolatedsubpsupk(i, j, k) = p(i, j, k)
					else
					if (((d_i_p(i, j, k) .eq. 0.0d0) .and. (d_j_p(i, j, k) .eq. 0.0d0)) .and. (d_k_p(i, j, k) .ne. 0.0d0)) then
					p(i, j, k) = extrapolatedsubpsupk(i, j, k)
					extrapolatedsubpsupi(i, j, k) = p(i, j, k)
					extrapolatedsubpsupj(i, j, k) = p(i, j, k)
					end if
					end if
					end if
					end if
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fx(i, j, k) .ne. 0 .or. d_j_fx(i, j, k) .ne. 0 .or. d_k_fx(i, j, k) .ne. 0)) then
				if (FOV_1(int(i - d_i_fx(i, j, k)), int(j - d_j_fx(i, j, k)), int(k - d_k_fx(i, j, k))) .gt. 0) then
					extrapolatedsubfxsupi(i, j, k) = pfx
					extrapolatedsubfxsupj(i, j, k) = pfx
					extrapolatedsubfxsupk(i, j, k) = pfx
					if (((d_i_fx(i, j, k) .ne. 0.0d0) .and. (d_j_fx(i, j, k) .ne. 0.0d0)) .and. (d_k_fx(i, j, k) .ne. 0.0d0)) then
					fx(i, j, k) = ((extrapolatedsubfxsupi(i, j, k) + extrapolatedsubfxsupj(i, j, k)) + extrapolatedsubfxsupk(i, j, k)) / 3.0d0
					else
					if (((d_i_fx(i, j, k) .ne. 0.0d0) .and. (d_j_fx(i, j, k) .ne. 0.0d0)) .and. (d_k_fx(i, j, k) .eq. 0.0d0)) then
					fx(i, j, k) = (extrapolatedsubfxsupi(i, j, k) + extrapolatedsubfxsupj(i, j, k)) / 2.0d0
					extrapolatedsubfxsupk(i, j, k) = fx(i, j, k)
					else
					if (((d_i_fx(i, j, k) .ne. 0.0d0) .and. (d_j_fx(i, j, k) .eq. 0.0d0)) .and. (d_k_fx(i, j, k) .ne. 0.0d0)) then
					fx(i, j, k) = (extrapolatedsubfxsupi(i, j, k) + extrapolatedsubfxsupk(i, j, k)) / 2.0d0
					extrapolatedsubfxsupj(i, j, k) = fx(i, j, k)
					else
					if (((d_i_fx(i, j, k) .ne. 0.0d0) .and. (d_j_fx(i, j, k) .eq. 0.0d0)) .and. (d_k_fx(i, j, k) .eq. 0.0d0)) then
					fx(i, j, k) = extrapolatedsubfxsupi(i, j, k)
					extrapolatedsubfxsupj(i, j, k) = fx(i, j, k)
					extrapolatedsubfxsupk(i, j, k) = fx(i, j, k)
					else
					if (((d_i_fx(i, j, k) .eq. 0.0d0) .and. (d_j_fx(i, j, k) .ne. 0.0d0)) .and. (d_k_fx(i, j, k) .ne. 0.0d0)) then
					fx(i, j, k) = (extrapolatedsubfxsupj(i, j, k) + extrapolatedsubfxsupk(i, j, k)) / 2.0d0
					extrapolatedsubfxsupi(i, j, k) = fx(i, j, k)
					else
					if (((d_i_fx(i, j, k) .eq. 0.0d0) .and. (d_j_fx(i, j, k) .ne. 0.0d0)) .and. (d_k_fx(i, j, k) .eq. 0.0d0)) then
					fx(i, j, k) = extrapolatedsubfxsupj(i, j, k)
					extrapolatedsubfxsupi(i, j, k) = fx(i, j, k)
					extrapolatedsubfxsupk(i, j, k) = fx(i, j, k)
					else
					if (((d_i_fx(i, j, k) .eq. 0.0d0) .and. (d_j_fx(i, j, k) .eq. 0.0d0)) .and. (d_k_fx(i, j, k) .ne. 0.0d0)) then
					fx(i, j, k) = extrapolatedsubfxsupk(i, j, k)
					extrapolatedsubfxsupi(i, j, k) = fx(i, j, k)
					extrapolatedsubfxsupj(i, j, k) = fx(i, j, k)
					end if
					end if
					end if
					end if
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fy(i, j, k) .ne. 0 .or. d_j_fy(i, j, k) .ne. 0 .or. d_k_fy(i, j, k) .ne. 0)) then
				if (FOV_1(int(i - d_i_fy(i, j, k)), int(j - d_j_fy(i, j, k)), int(k - d_k_fy(i, j, k))) .gt. 0) then
					extrapolatedsubfysupi(i, j, k) = pfy
					extrapolatedsubfysupj(i, j, k) = pfy
					extrapolatedsubfysupk(i, j, k) = pfy
					if (((d_i_fy(i, j, k) .ne. 0.0d0) .and. (d_j_fy(i, j, k) .ne. 0.0d0)) .and. (d_k_fy(i, j, k) .ne. 0.0d0)) then
					fy(i, j, k) = ((extrapolatedsubfysupi(i, j, k) + extrapolatedsubfysupj(i, j, k)) + extrapolatedsubfysupk(i, j, k)) / 3.0d0
					else
					if (((d_i_fy(i, j, k) .ne. 0.0d0) .and. (d_j_fy(i, j, k) .ne. 0.0d0)) .and. (d_k_fy(i, j, k) .eq. 0.0d0)) then
					fy(i, j, k) = (extrapolatedsubfysupi(i, j, k) + extrapolatedsubfysupj(i, j, k)) / 2.0d0
					extrapolatedsubfysupk(i, j, k) = fy(i, j, k)
					else
					if (((d_i_fy(i, j, k) .ne. 0.0d0) .and. (d_j_fy(i, j, k) .eq. 0.0d0)) .and. (d_k_fy(i, j, k) .ne. 0.0d0)) then
					fy(i, j, k) = (extrapolatedsubfysupi(i, j, k) + extrapolatedsubfysupk(i, j, k)) / 2.0d0
					extrapolatedsubfysupj(i, j, k) = fy(i, j, k)
					else
					if (((d_i_fy(i, j, k) .ne. 0.0d0) .and. (d_j_fy(i, j, k) .eq. 0.0d0)) .and. (d_k_fy(i, j, k) .eq. 0.0d0)) then
					fy(i, j, k) = extrapolatedsubfysupi(i, j, k)
					extrapolatedsubfysupj(i, j, k) = fy(i, j, k)
					extrapolatedsubfysupk(i, j, k) = fy(i, j, k)
					else
					if (((d_i_fy(i, j, k) .eq. 0.0d0) .and. (d_j_fy(i, j, k) .ne. 0.0d0)) .and. (d_k_fy(i, j, k) .ne. 0.0d0)) then
					fy(i, j, k) = (extrapolatedsubfysupj(i, j, k) + extrapolatedsubfysupk(i, j, k)) / 2.0d0
					extrapolatedsubfysupi(i, j, k) = fy(i, j, k)
					else
					if (((d_i_fy(i, j, k) .eq. 0.0d0) .and. (d_j_fy(i, j, k) .ne. 0.0d0)) .and. (d_k_fy(i, j, k) .eq. 0.0d0)) then
					fy(i, j, k) = extrapolatedsubfysupj(i, j, k)
					extrapolatedsubfysupi(i, j, k) = fy(i, j, k)
					extrapolatedsubfysupk(i, j, k) = fy(i, j, k)
					else
					if (((d_i_fy(i, j, k) .eq. 0.0d0) .and. (d_j_fy(i, j, k) .eq. 0.0d0)) .and. (d_k_fy(i, j, k) .ne. 0.0d0)) then
					fy(i, j, k) = extrapolatedsubfysupk(i, j, k)
					extrapolatedsubfysupi(i, j, k) = fy(i, j, k)
					extrapolatedsubfysupj(i, j, k) = fy(i, j, k)
					end if
					end if
					end if
					end if
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fz(i, j, k) .ne. 0 .or. d_j_fz(i, j, k) .ne. 0 .or. d_k_fz(i, j, k) .ne. 0)) then
				if (FOV_1(int(i - d_i_fz(i, j, k)), int(j - d_j_fz(i, j, k)), int(k - d_k_fz(i, j, k))) .gt. 0) then
					extrapolatedsubfzsupi(i, j, k) = pfz
					extrapolatedsubfzsupj(i, j, k) = pfz
					extrapolatedsubfzsupk(i, j, k) = pfz
					if (((d_i_fz(i, j, k) .ne. 0.0d0) .and. (d_j_fz(i, j, k) .ne. 0.0d0)) .and. (d_k_fz(i, j, k) .ne. 0.0d0)) then
					fz(i, j, k) = ((extrapolatedsubfzsupi(i, j, k) + extrapolatedsubfzsupj(i, j, k)) + extrapolatedsubfzsupk(i, j, k)) / 3.0d0
					else
					if (((d_i_fz(i, j, k) .ne. 0.0d0) .and. (d_j_fz(i, j, k) .ne. 0.0d0)) .and. (d_k_fz(i, j, k) .eq. 0.0d0)) then
					fz(i, j, k) = (extrapolatedsubfzsupi(i, j, k) + extrapolatedsubfzsupj(i, j, k)) / 2.0d0
					extrapolatedsubfzsupk(i, j, k) = fz(i, j, k)
					else
					if (((d_i_fz(i, j, k) .ne. 0.0d0) .and. (d_j_fz(i, j, k) .eq. 0.0d0)) .and. (d_k_fz(i, j, k) .ne. 0.0d0)) then
					fz(i, j, k) = (extrapolatedsubfzsupi(i, j, k) + extrapolatedsubfzsupk(i, j, k)) / 2.0d0
					extrapolatedsubfzsupj(i, j, k) = fz(i, j, k)
					else
					if (((d_i_fz(i, j, k) .ne. 0.0d0) .and. (d_j_fz(i, j, k) .eq. 0.0d0)) .and. (d_k_fz(i, j, k) .eq. 0.0d0)) then
					fz(i, j, k) = extrapolatedsubfzsupi(i, j, k)
					extrapolatedsubfzsupj(i, j, k) = fz(i, j, k)
					extrapolatedsubfzsupk(i, j, k) = fz(i, j, k)
					else
					if (((d_i_fz(i, j, k) .eq. 0.0d0) .and. (d_j_fz(i, j, k) .ne. 0.0d0)) .and. (d_k_fz(i, j, k) .ne. 0.0d0)) then
					fz(i, j, k) = (extrapolatedsubfzsupj(i, j, k) + extrapolatedsubfzsupk(i, j, k)) / 2.0d0
					extrapolatedsubfzsupi(i, j, k) = fz(i, j, k)
					else
					if (((d_i_fz(i, j, k) .eq. 0.0d0) .and. (d_j_fz(i, j, k) .ne. 0.0d0)) .and. (d_k_fz(i, j, k) .eq. 0.0d0)) then
					fz(i, j, k) = extrapolatedsubfzsupj(i, j, k)
					extrapolatedsubfzsupi(i, j, k) = fz(i, j, k)
					extrapolatedsubfzsupk(i, j, k) = fz(i, j, k)
					else
					if (((d_i_fz(i, j, k) .eq. 0.0d0) .and. (d_j_fz(i, j, k) .eq. 0.0d0)) .and. (d_k_fz(i, j, k) .ne. 0.0d0)) then
					fz(i, j, k) = extrapolatedsubfzsupk(i, j, k)
					extrapolatedsubfzsupi(i, j, k) = fz(i, j, k)
					extrapolatedsubfzsupj(i, j, k) = fz(i, j, k)
					end if
					end if
					end if
					end if
					end if
					end if
					end if
				end if
			end if
		end if
		if ((FOV_yLower(i, j, k) .gt. 0) .and. ((j + 1 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 1), k) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 2), k) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 3), k) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 1), k) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 1), k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 1), k) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 1), k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 1), k) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 1), k) .gt. 0)) .or. (j + 1 .le. cctk_lsh(2) .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 1), int(k + 1)) .gt. 0)) .or. (j + 1 .le. cctk_lsh(2) .and. k - 1 .ge. 1 .and. (FOV_1(i, int(j + 1), int(k - 1)) .gt. 0)) .or. (j + 1 .le. cctk_lsh(2) .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 1), int(k + 2)) .gt. 0)) .or. (j + 1 .le. cctk_lsh(2) .and. k - 2 .ge. 1 .and. (FOV_1(i, int(j + 1), int(k - 2)) .gt. 0)) .or. (j + 1 .le. cctk_lsh(2) .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 1), int(k + 3)) .gt. 0)) .or. (j + 1 .le. cctk_lsh(2) .and. k - 3 .ge. 1 .and. (FOV_1(i, int(j + 1), int(k - 3)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 2), k) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 2), k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 2), k) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 2), k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 2), k) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 2), k) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 2), int(k + 1)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. k - 1 .ge. 1 .and. (FOV_1(i, int(j + 2), int(k - 1)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 2), int(k + 2)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. k - 2 .ge. 1 .and. (FOV_1(i, int(j + 2), int(k - 2)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 2), int(k + 3)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. k - 3 .ge. 1 .and. (FOV_1(i, int(j + 2), int(k - 3)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 3), k) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 3), k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 3), k) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 3), k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 3), k) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 3), k) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 3), int(k + 1)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. k - 1 .ge. 1 .and. (FOV_1(i, int(j + 3), int(k - 1)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 3), int(k + 2)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. k - 2 .ge. 1 .and. (FOV_1(i, int(j + 3), int(k - 2)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 3), int(k + 3)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. k - 3 .ge. 1 .and. (FOV_1(i, int(j + 3), int(k - 3)) .gt. 0))))) then
				rho(i, 4 - j, k) = 0.0d0
				mx(i, 4 - j, k) = 0.0d0
				my(i, 4 - j, k) = 0.0d0
				mz(i, 4 - j, k) = 0.0d0
				p(i, 4 - j, k) = 0.0d0
				fx(i, 4 - j, k) = 0.0d0
				fy(i, 4 - j, k) = 0.0d0
				fz(i, 4 - j, k) = 0.0d0
		end if
		if ((FOV_yUpper(i, j, k) .gt. 0) .and. ((j - 1 .ge. 1 .and. (FOV_1(i, int(j - 1), k) .gt. 0)) .or. (j - 2 .ge. 1 .and. (FOV_1(i, int(j - 2), k) .gt. 0)) .or. (j - 3 .ge. 1 .and. (FOV_1(i, int(j - 3), k) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 1), k) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 1), k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 1), k) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 1), k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 1), k) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 1), k) .gt. 0)) .or. (j - 1 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 1), int(k + 1)) .gt. 0)) .or. (j - 1 .ge. 1 .and. k - 1 .ge. 1 .and. (FOV_1(i, int(j - 1), int(k - 1)) .gt. 0)) .or. (j - 1 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 1), int(k + 2)) .gt. 0)) .or. (j - 1 .ge. 1 .and. k - 2 .ge. 1 .and. (FOV_1(i, int(j - 1), int(k - 2)) .gt. 0)) .or. (j - 1 .ge. 1 .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 1), int(k + 3)) .gt. 0)) .or. (j - 1 .ge. 1 .and. k - 3 .ge. 1 .and. (FOV_1(i, int(j - 1), int(k - 3)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 2), k) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 2), k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 2), k) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 2), k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 2), k) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 2), k) .gt. 0)) .or. (j - 2 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 2), int(k + 1)) .gt. 0)) .or. (j - 2 .ge. 1 .and. k - 1 .ge. 1 .and. (FOV_1(i, int(j - 2), int(k - 1)) .gt. 0)) .or. (j - 2 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 2), int(k + 2)) .gt. 0)) .or. (j - 2 .ge. 1 .and. k - 2 .ge. 1 .and. (FOV_1(i, int(j - 2), int(k - 2)) .gt. 0)) .or. (j - 2 .ge. 1 .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 2), int(k + 3)) .gt. 0)) .or. (j - 2 .ge. 1 .and. k - 3 .ge. 1 .and. (FOV_1(i, int(j - 2), int(k - 3)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 3), k) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 3), k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 3), k) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 3), k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 3), k) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 3), k) .gt. 0)) .or. (j - 3 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 3), int(k + 1)) .gt. 0)) .or. (j - 3 .ge. 1 .and. k - 1 .ge. 1 .and. (FOV_1(i, int(j - 3), int(k - 1)) .gt. 0)) .or. (j - 3 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 3), int(k + 2)) .gt. 0)) .or. (j - 3 .ge. 1 .and. k - 2 .ge. 1 .and. (FOV_1(i, int(j - 3), int(k - 2)) .gt. 0)) .or. (j - 3 .ge. 1 .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 3), int(k + 3)) .gt. 0)) .or. (j - 3 .ge. 1 .and. k - 3 .ge. 1 .and. (FOV_1(i, int(j - 3), int(k - 3)) .gt. 0))))) then
			rho(i, j, k) = 0.0d0
			mx(i, j, k) = 0.0d0
			my(i, j, k) = 0.0d0
			mz(i, j, k) = 0.0d0
			p(i, j, k) = 0.0d0
			fx(i, j, k) = 0.0d0
			fy(i, j, k) = 0.0d0
			fz(i, j, k) = 0.0d0
		end if
		if ((FOV_zLower(i, j, k) .gt. 0) .and. ((k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, j, int(k + 1)) .gt. 0)) .or. (k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, j, int(k + 2)) .gt. 0)) .or. (k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, j, int(k + 3)) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(int(i + 1), j, int(k + 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(int(i - 1), j, int(k + 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(int(i + 2), j, int(k + 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(int(i - 2), j, int(k + 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(int(i + 3), j, int(k + 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(int(i - 3), j, int(k + 1)) .gt. 0)) .or. (j + 1 .le. cctk_lsh(2) .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 1), int(k + 1)) .gt. 0)) .or. (j - 1 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 1), int(k + 1)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 2), int(k + 1)) .gt. 0)) .or. (j - 2 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 2), int(k + 1)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 3), int(k + 1)) .gt. 0)) .or. (j - 3 .ge. 1 .and. k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 3), int(k + 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(int(i + 1), j, int(k + 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(int(i - 1), j, int(k + 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(int(i + 2), j, int(k + 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(int(i - 2), j, int(k + 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(int(i + 3), j, int(k + 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(int(i - 3), j, int(k + 2)) .gt. 0)) .or. (j + 1 .le. cctk_lsh(2) .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 1), int(k + 2)) .gt. 0)) .or. (j - 1 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 1), int(k + 2)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 2), int(k + 2)) .gt. 0)) .or. (j - 2 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 2), int(k + 2)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 3), int(k + 2)) .gt. 0)) .or. (j - 3 .ge. 1 .and. k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 3), int(k + 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(int(i + 1), j, int(k + 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(int(i - 1), j, int(k + 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(int(i + 2), j, int(k + 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(int(i - 2), j, int(k + 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(int(i + 3), j, int(k + 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(int(i - 3), j, int(k + 3)) .gt. 0)) .or. (j + 1 .le. cctk_lsh(2) .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 1), int(k + 3)) .gt. 0)) .or. (j - 1 .ge. 1 .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 1), int(k + 3)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 2), int(k + 3)) .gt. 0)) .or. (j - 2 .ge. 1 .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 2), int(k + 3)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, int(j + 3), int(k + 3)) .gt. 0)) .or. (j - 3 .ge. 1 .and. k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, int(j - 3), int(k + 3)) .gt. 0))))) then
				rho(i, j, 4 - k) = 0.0d0
				mx(i, j, 4 - k) = 0.0d0
				my(i, j, 4 - k) = 0.0d0
				mz(i, j, 4 - k) = 0.0d0
				p(i, j, 4 - k) = 0.0d0
				fx(i, j, 4 - k) = 0.0d0
				fy(i, j, 4 - k) = 0.0d0
				fz(i, j, 4 - k) = 0.0d0
		end if
		if ((FOV_zUpper(i, j, k) .gt. 0) .and. ((k - 1 .ge. 1 .and. (FOV_1(i, j, int(k - 1)) .gt. 0)) .or. (k - 2 .ge. 1 .and. (FOV_1(i, j, int(k - 2)) .gt. 0)) .or. (k - 3 .ge. 1 .and. (FOV_1(i, j, int(k - 3)) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. k - 1 .ge. 1 .and. (FOV_1(int(i + 1), j, int(k - 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. k - 1 .ge. 1 .and. (FOV_1(int(i - 1), j, int(k - 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. k - 1 .ge. 1 .and. (FOV_1(int(i + 2), j, int(k - 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. k - 1 .ge. 1 .and. (FOV_1(int(i - 2), j, int(k - 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. k - 1 .ge. 1 .and. (FOV_1(int(i + 3), j, int(k - 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. k - 1 .ge. 1 .and. (FOV_1(int(i - 3), j, int(k - 1)) .gt. 0)) .or. (j + 1 .le. cctk_lsh(2) .and. k - 1 .ge. 1 .and. (FOV_1(i, int(j + 1), int(k - 1)) .gt. 0)) .or. (j - 1 .ge. 1 .and. k - 1 .ge. 1 .and. (FOV_1(i, int(j - 1), int(k - 1)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. k - 1 .ge. 1 .and. (FOV_1(i, int(j + 2), int(k - 1)) .gt. 0)) .or. (j - 2 .ge. 1 .and. k - 1 .ge. 1 .and. (FOV_1(i, int(j - 2), int(k - 1)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. k - 1 .ge. 1 .and. (FOV_1(i, int(j + 3), int(k - 1)) .gt. 0)) .or. (j - 3 .ge. 1 .and. k - 1 .ge. 1 .and. (FOV_1(i, int(j - 3), int(k - 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. k - 2 .ge. 1 .and. (FOV_1(int(i + 1), j, int(k - 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. k - 2 .ge. 1 .and. (FOV_1(int(i - 1), j, int(k - 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. k - 2 .ge. 1 .and. (FOV_1(int(i + 2), j, int(k - 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. k - 2 .ge. 1 .and. (FOV_1(int(i - 2), j, int(k - 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. k - 2 .ge. 1 .and. (FOV_1(int(i + 3), j, int(k - 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. k - 2 .ge. 1 .and. (FOV_1(int(i - 3), j, int(k - 2)) .gt. 0)) .or. (j + 1 .le. cctk_lsh(2) .and. k - 2 .ge. 1 .and. (FOV_1(i, int(j + 1), int(k - 2)) .gt. 0)) .or. (j - 1 .ge. 1 .and. k - 2 .ge. 1 .and. (FOV_1(i, int(j - 1), int(k - 2)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. k - 2 .ge. 1 .and. (FOV_1(i, int(j + 2), int(k - 2)) .gt. 0)) .or. (j - 2 .ge. 1 .and. k - 2 .ge. 1 .and. (FOV_1(i, int(j - 2), int(k - 2)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. k - 2 .ge. 1 .and. (FOV_1(i, int(j + 3), int(k - 2)) .gt. 0)) .or. (j - 3 .ge. 1 .and. k - 2 .ge. 1 .and. (FOV_1(i, int(j - 3), int(k - 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. k - 3 .ge. 1 .and. (FOV_1(int(i + 1), j, int(k - 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. k - 3 .ge. 1 .and. (FOV_1(int(i - 1), j, int(k - 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. k - 3 .ge. 1 .and. (FOV_1(int(i + 2), j, int(k - 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. k - 3 .ge. 1 .and. (FOV_1(int(i - 2), j, int(k - 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. k - 3 .ge. 1 .and. (FOV_1(int(i + 3), j, int(k - 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. k - 3 .ge. 1 .and. (FOV_1(int(i - 3), j, int(k - 3)) .gt. 0)) .or. (j + 1 .le. cctk_lsh(2) .and. k - 3 .ge. 1 .and. (FOV_1(i, int(j + 1), int(k - 3)) .gt. 0)) .or. (j - 1 .ge. 1 .and. k - 3 .ge. 1 .and. (FOV_1(i, int(j - 1), int(k - 3)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. k - 3 .ge. 1 .and. (FOV_1(i, int(j + 2), int(k - 3)) .gt. 0)) .or. (j - 2 .ge. 1 .and. k - 3 .ge. 1 .and. (FOV_1(i, int(j - 2), int(k - 3)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. k - 3 .ge. 1 .and. (FOV_1(i, int(j + 3), int(k - 3)) .gt. 0)) .or. (j - 3 .ge. 1 .and. k - 3 .ge. 1 .and. (FOV_1(i, int(j - 3), int(k - 3)) .gt. 0))))) then
			rho(i, j, k) = 0.0d0
			mx(i, j, k) = 0.0d0
			my(i, j, k) = 0.0d0
			mz(i, j, k) = 0.0d0
			p(i, j, k) = 0.0d0
			fx(i, j, k) = 0.0d0
			fy(i, j, k) = 0.0d0
			fz(i, j, k) = 0.0d0
		end if
		if (FOV_1(i, j, k) .gt. 0) then
			if ((d_i_Null(i, j, k) .ne. 0 .or. d_j_Null(i, j, k) .ne. 0 .or. d_k_Null(i, j, k) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_Null, Null, extrapolatedsubNullsupi, i, d_j_Null, Null, extrapolatedsubNullsupj, j, d_k_Null, Null, extrapolatedsubNullsupk, k, Null, FOV_3)
			end if
		end if
				end do
			end do
		end do
		do k = kStart, kEnd
			do j = jStart, jEnd
				do i = iStart, iEnd
					if ((FOV_xLower(i, j, k) .gt. 0) .and. ((i + 1 .le. cctk_lsh(1) .and. (FOV_3(int(i + 1), j, k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. (FOV_3(int(i + 2), j, k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. (FOV_3(int(i + 3), j, k) .gt. 0)))) then
						extrapolatedsubrhosupi(i, j, k) = rho(i, j, k)
						extrapolatedsubrhosupj(i, j, k) = rho(i, j, k)
						extrapolatedsubrhosupk(i, j, k) = rho(i, j, k)
						extrapolatedsubmxsupi(i, j, k) = mx(i, j, k)
						extrapolatedsubmxsupj(i, j, k) = mx(i, j, k)
						extrapolatedsubmxsupk(i, j, k) = mx(i, j, k)
						extrapolatedsubmysupi(i, j, k) = my(i, j, k)
						extrapolatedsubmysupj(i, j, k) = my(i, j, k)
						extrapolatedsubmysupk(i, j, k) = my(i, j, k)
						extrapolatedsubmzsupi(i, j, k) = mz(i, j, k)
						extrapolatedsubmzsupj(i, j, k) = mz(i, j, k)
						extrapolatedsubmzsupk(i, j, k) = mz(i, j, k)
						extrapolatedsubNullsupi(i, j, k) = Null(i, j, k)
						extrapolatedsubNullsupj(i, j, k) = Null(i, j, k)
						extrapolatedsubNullsupk(i, j, k) = Null(i, j, k)
						extrapolatedsubfxsupi(i, j, k) = fx(i, j, k)
						extrapolatedsubfxsupj(i, j, k) = fx(i, j, k)
						extrapolatedsubfxsupk(i, j, k) = fx(i, j, k)
						extrapolatedsubfysupi(i, j, k) = fy(i, j, k)
						extrapolatedsubfysupj(i, j, k) = fy(i, j, k)
						extrapolatedsubfysupk(i, j, k) = fy(i, j, k)
						extrapolatedsubfzsupi(i, j, k) = fz(i, j, k)
						extrapolatedsubfzsupj(i, j, k) = fz(i, j, k)
						extrapolatedsubfzsupk(i, j, k) = fz(i, j, k)
						extrapolatedsubpsupi(i, j, k) = p(i, j, k)
						extrapolatedsubpsupj(i, j, k) = p(i, j, k)
						extrapolatedsubpsupk(i, j, k) = p(i, j, k)
					end if
					if ((FOV_xUpper(i, j, k) .gt. 0) .and. ((i - 1 .ge. 1 .and. (FOV_3(int(i - 1), j, k) .gt. 0)) .or. (i - 2 .ge. 1 .and. (FOV_3(int(i - 2), j, k) .gt. 0)) .or. (i - 3 .ge. 1 .and. (FOV_3(int(i - 3), j, k) .gt. 0)))) then
						extrapolatedsubrhosupi(i, j, k) = rho(i, j, k)
						extrapolatedsubrhosupj(i, j, k) = rho(i, j, k)
						extrapolatedsubrhosupk(i, j, k) = rho(i, j, k)
						extrapolatedsubmxsupi(i, j, k) = mx(i, j, k)
						extrapolatedsubmxsupj(i, j, k) = mx(i, j, k)
						extrapolatedsubmxsupk(i, j, k) = mx(i, j, k)
						extrapolatedsubmysupi(i, j, k) = my(i, j, k)
						extrapolatedsubmysupj(i, j, k) = my(i, j, k)
						extrapolatedsubmysupk(i, j, k) = my(i, j, k)
						extrapolatedsubmzsupi(i, j, k) = mz(i, j, k)
						extrapolatedsubmzsupj(i, j, k) = mz(i, j, k)
						extrapolatedsubmzsupk(i, j, k) = mz(i, j, k)
						extrapolatedsubNullsupi(i, j, k) = Null(i, j, k)
						extrapolatedsubNullsupj(i, j, k) = Null(i, j, k)
						extrapolatedsubNullsupk(i, j, k) = Null(i, j, k)
						extrapolatedsubfxsupi(i, j, k) = fx(i, j, k)
						extrapolatedsubfxsupj(i, j, k) = fx(i, j, k)
						extrapolatedsubfxsupk(i, j, k) = fx(i, j, k)
						extrapolatedsubfysupi(i, j, k) = fy(i, j, k)
						extrapolatedsubfysupj(i, j, k) = fy(i, j, k)
						extrapolatedsubfysupk(i, j, k) = fy(i, j, k)
						extrapolatedsubfzsupi(i, j, k) = fz(i, j, k)
						extrapolatedsubfzsupj(i, j, k) = fz(i, j, k)
						extrapolatedsubfzsupk(i, j, k) = fz(i, j, k)
						extrapolatedsubpsupi(i, j, k) = p(i, j, k)
						extrapolatedsubpsupj(i, j, k) = p(i, j, k)
						extrapolatedsubpsupk(i, j, k) = p(i, j, k)
					end if
					if ((FOV_yLower(i, j, k) .gt. 0) .and. ((j + 1 .le. cctk_lsh(2) .and. (FOV_3(i, int(j + 1), k) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. (FOV_3(i, int(j + 2), k) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. (FOV_3(i, int(j + 3), k) .gt. 0)))) then
						extrapolatedsubrhosupi(i, j, k) = rho(i, j, k)
						extrapolatedsubrhosupj(i, j, k) = rho(i, j, k)
						extrapolatedsubrhosupk(i, j, k) = rho(i, j, k)
						extrapolatedsubmxsupi(i, j, k) = mx(i, j, k)
						extrapolatedsubmxsupj(i, j, k) = mx(i, j, k)
						extrapolatedsubmxsupk(i, j, k) = mx(i, j, k)
						extrapolatedsubmysupi(i, j, k) = my(i, j, k)
						extrapolatedsubmysupj(i, j, k) = my(i, j, k)
						extrapolatedsubmysupk(i, j, k) = my(i, j, k)
						extrapolatedsubmzsupi(i, j, k) = mz(i, j, k)
						extrapolatedsubmzsupj(i, j, k) = mz(i, j, k)
						extrapolatedsubmzsupk(i, j, k) = mz(i, j, k)
						extrapolatedsubNullsupi(i, j, k) = Null(i, j, k)
						extrapolatedsubNullsupj(i, j, k) = Null(i, j, k)
						extrapolatedsubNullsupk(i, j, k) = Null(i, j, k)
						extrapolatedsubfxsupi(i, j, k) = fx(i, j, k)
						extrapolatedsubfxsupj(i, j, k) = fx(i, j, k)
						extrapolatedsubfxsupk(i, j, k) = fx(i, j, k)
						extrapolatedsubfysupi(i, j, k) = fy(i, j, k)
						extrapolatedsubfysupj(i, j, k) = fy(i, j, k)
						extrapolatedsubfysupk(i, j, k) = fy(i, j, k)
						extrapolatedsubfzsupi(i, j, k) = fz(i, j, k)
						extrapolatedsubfzsupj(i, j, k) = fz(i, j, k)
						extrapolatedsubfzsupk(i, j, k) = fz(i, j, k)
						extrapolatedsubpsupi(i, j, k) = p(i, j, k)
						extrapolatedsubpsupj(i, j, k) = p(i, j, k)
						extrapolatedsubpsupk(i, j, k) = p(i, j, k)
					end if
					if ((FOV_yUpper(i, j, k) .gt. 0) .and. ((j - 1 .ge. 1 .and. (FOV_3(i, int(j - 1), k) .gt. 0)) .or. (j - 2 .ge. 1 .and. (FOV_3(i, int(j - 2), k) .gt. 0)) .or. (j - 3 .ge. 1 .and. (FOV_3(i, int(j - 3), k) .gt. 0)))) then
						extrapolatedsubrhosupi(i, j, k) = rho(i, j, k)
						extrapolatedsubrhosupj(i, j, k) = rho(i, j, k)
						extrapolatedsubrhosupk(i, j, k) = rho(i, j, k)
						extrapolatedsubmxsupi(i, j, k) = mx(i, j, k)
						extrapolatedsubmxsupj(i, j, k) = mx(i, j, k)
						extrapolatedsubmxsupk(i, j, k) = mx(i, j, k)
						extrapolatedsubmysupi(i, j, k) = my(i, j, k)
						extrapolatedsubmysupj(i, j, k) = my(i, j, k)
						extrapolatedsubmysupk(i, j, k) = my(i, j, k)
						extrapolatedsubmzsupi(i, j, k) = mz(i, j, k)
						extrapolatedsubmzsupj(i, j, k) = mz(i, j, k)
						extrapolatedsubmzsupk(i, j, k) = mz(i, j, k)
						extrapolatedsubNullsupi(i, j, k) = Null(i, j, k)
						extrapolatedsubNullsupj(i, j, k) = Null(i, j, k)
						extrapolatedsubNullsupk(i, j, k) = Null(i, j, k)
						extrapolatedsubfxsupi(i, j, k) = fx(i, j, k)
						extrapolatedsubfxsupj(i, j, k) = fx(i, j, k)
						extrapolatedsubfxsupk(i, j, k) = fx(i, j, k)
						extrapolatedsubfysupi(i, j, k) = fy(i, j, k)
						extrapolatedsubfysupj(i, j, k) = fy(i, j, k)
						extrapolatedsubfysupk(i, j, k) = fy(i, j, k)
						extrapolatedsubfzsupi(i, j, k) = fz(i, j, k)
						extrapolatedsubfzsupj(i, j, k) = fz(i, j, k)
						extrapolatedsubfzsupk(i, j, k) = fz(i, j, k)
						extrapolatedsubpsupi(i, j, k) = p(i, j, k)
						extrapolatedsubpsupj(i, j, k) = p(i, j, k)
						extrapolatedsubpsupk(i, j, k) = p(i, j, k)
					end if
					if ((FOV_zLower(i, j, k) .gt. 0) .and. ((k + 1 .le. cctk_lsh(3) .and. (FOV_3(i, j, int(k + 1)) .gt. 0)) .or. (k + 2 .le. cctk_lsh(3) .and. (FOV_3(i, j, int(k + 2)) .gt. 0)) .or. (k + 3 .le. cctk_lsh(3) .and. (FOV_3(i, j, int(k + 3)) .gt. 0)))) then
						extrapolatedsubrhosupi(i, j, k) = rho(i, j, k)
						extrapolatedsubrhosupj(i, j, k) = rho(i, j, k)
						extrapolatedsubrhosupk(i, j, k) = rho(i, j, k)
						extrapolatedsubmxsupi(i, j, k) = mx(i, j, k)
						extrapolatedsubmxsupj(i, j, k) = mx(i, j, k)
						extrapolatedsubmxsupk(i, j, k) = mx(i, j, k)
						extrapolatedsubmysupi(i, j, k) = my(i, j, k)
						extrapolatedsubmysupj(i, j, k) = my(i, j, k)
						extrapolatedsubmysupk(i, j, k) = my(i, j, k)
						extrapolatedsubmzsupi(i, j, k) = mz(i, j, k)
						extrapolatedsubmzsupj(i, j, k) = mz(i, j, k)
						extrapolatedsubmzsupk(i, j, k) = mz(i, j, k)
						extrapolatedsubNullsupi(i, j, k) = Null(i, j, k)
						extrapolatedsubNullsupj(i, j, k) = Null(i, j, k)
						extrapolatedsubNullsupk(i, j, k) = Null(i, j, k)
						extrapolatedsubfxsupi(i, j, k) = fx(i, j, k)
						extrapolatedsubfxsupj(i, j, k) = fx(i, j, k)
						extrapolatedsubfxsupk(i, j, k) = fx(i, j, k)
						extrapolatedsubfysupi(i, j, k) = fy(i, j, k)
						extrapolatedsubfysupj(i, j, k) = fy(i, j, k)
						extrapolatedsubfysupk(i, j, k) = fy(i, j, k)
						extrapolatedsubfzsupi(i, j, k) = fz(i, j, k)
						extrapolatedsubfzsupj(i, j, k) = fz(i, j, k)
						extrapolatedsubfzsupk(i, j, k) = fz(i, j, k)
						extrapolatedsubpsupi(i, j, k) = p(i, j, k)
						extrapolatedsubpsupj(i, j, k) = p(i, j, k)
						extrapolatedsubpsupk(i, j, k) = p(i, j, k)
					end if
					if ((FOV_zUpper(i, j, k) .gt. 0) .and. ((k - 1 .ge. 1 .and. (FOV_3(i, j, int(k - 1)) .gt. 0)) .or. (k - 2 .ge. 1 .and. (FOV_3(i, j, int(k - 2)) .gt. 0)) .or. (k - 3 .ge. 1 .and. (FOV_3(i, j, int(k - 3)) .gt. 0)))) then
						extrapolatedsubrhosupi(i, j, k) = rho(i, j, k)
						extrapolatedsubrhosupj(i, j, k) = rho(i, j, k)
						extrapolatedsubrhosupk(i, j, k) = rho(i, j, k)
						extrapolatedsubmxsupi(i, j, k) = mx(i, j, k)
						extrapolatedsubmxsupj(i, j, k) = mx(i, j, k)
						extrapolatedsubmxsupk(i, j, k) = mx(i, j, k)
						extrapolatedsubmysupi(i, j, k) = my(i, j, k)
						extrapolatedsubmysupj(i, j, k) = my(i, j, k)
						extrapolatedsubmysupk(i, j, k) = my(i, j, k)
						extrapolatedsubmzsupi(i, j, k) = mz(i, j, k)
						extrapolatedsubmzsupj(i, j, k) = mz(i, j, k)
						extrapolatedsubmzsupk(i, j, k) = mz(i, j, k)
						extrapolatedsubNullsupi(i, j, k) = Null(i, j, k)
						extrapolatedsubNullsupj(i, j, k) = Null(i, j, k)
						extrapolatedsubNullsupk(i, j, k) = Null(i, j, k)
						extrapolatedsubfxsupi(i, j, k) = fx(i, j, k)
						extrapolatedsubfxsupj(i, j, k) = fx(i, j, k)
						extrapolatedsubfxsupk(i, j, k) = fx(i, j, k)
						extrapolatedsubfysupi(i, j, k) = fy(i, j, k)
						extrapolatedsubfysupj(i, j, k) = fy(i, j, k)
						extrapolatedsubfysupk(i, j, k) = fy(i, j, k)
						extrapolatedsubfzsupi(i, j, k) = fz(i, j, k)
						extrapolatedsubfzsupj(i, j, k) = fz(i, j, k)
						extrapolatedsubfzsupk(i, j, k) = fz(i, j, k)
						extrapolatedsubpsupi(i, j, k) = p(i, j, k)
						extrapolatedsubpsupj(i, j, k) = p(i, j, k)
						extrapolatedsubpsupk(i, j, k) = p(i, j, k)
					end if
					if ((FOV_xLower(i, j, k) .gt. 0) .and. ((i + 1 .le. cctk_lsh(1) .and. (FOV_1(int(i + 1), j, k) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. (FOV_1(int(i + 2), j, k) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. (FOV_1(int(i + 3), j, k) .gt. 0)))) then
						extrapolatedsubNullsupi(i, j, k) = Null(i, j, k)
						extrapolatedsubNullsupj(i, j, k) = Null(i, j, k)
						extrapolatedsubNullsupk(i, j, k) = Null(i, j, k)
					end if
					if ((FOV_xUpper(i, j, k) .gt. 0) .and. ((i - 1 .ge. 1 .and. (FOV_1(int(i - 1), j, k) .gt. 0)) .or. (i - 2 .ge. 1 .and. (FOV_1(int(i - 2), j, k) .gt. 0)) .or. (i - 3 .ge. 1 .and. (FOV_1(int(i - 3), j, k) .gt. 0)))) then
						extrapolatedsubNullsupi(i, j, k) = Null(i, j, k)
						extrapolatedsubNullsupj(i, j, k) = Null(i, j, k)
						extrapolatedsubNullsupk(i, j, k) = Null(i, j, k)
					end if
					if ((FOV_yLower(i, j, k) .gt. 0) .and. ((j + 1 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 1), k) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 2), k) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 3), k) .gt. 0)))) then
						extrapolatedsubrhosupi(i, j, k) = rho(i, j, k)
						extrapolatedsubrhosupj(i, j, k) = rho(i, j, k)
						extrapolatedsubrhosupk(i, j, k) = rho(i, j, k)
						extrapolatedsubmxsupi(i, j, k) = mx(i, j, k)
						extrapolatedsubmxsupj(i, j, k) = mx(i, j, k)
						extrapolatedsubmxsupk(i, j, k) = mx(i, j, k)
						extrapolatedsubmysupi(i, j, k) = my(i, j, k)
						extrapolatedsubmysupj(i, j, k) = my(i, j, k)
						extrapolatedsubmysupk(i, j, k) = my(i, j, k)
						extrapolatedsubmzsupi(i, j, k) = mz(i, j, k)
						extrapolatedsubmzsupj(i, j, k) = mz(i, j, k)
						extrapolatedsubmzsupk(i, j, k) = mz(i, j, k)
						extrapolatedsubNullsupi(i, j, k) = Null(i, j, k)
						extrapolatedsubNullsupj(i, j, k) = Null(i, j, k)
						extrapolatedsubNullsupk(i, j, k) = Null(i, j, k)
						extrapolatedsubfxsupi(i, j, k) = fx(i, j, k)
						extrapolatedsubfxsupj(i, j, k) = fx(i, j, k)
						extrapolatedsubfxsupk(i, j, k) = fx(i, j, k)
						extrapolatedsubfysupi(i, j, k) = fy(i, j, k)
						extrapolatedsubfysupj(i, j, k) = fy(i, j, k)
						extrapolatedsubfysupk(i, j, k) = fy(i, j, k)
						extrapolatedsubfzsupi(i, j, k) = fz(i, j, k)
						extrapolatedsubfzsupj(i, j, k) = fz(i, j, k)
						extrapolatedsubfzsupk(i, j, k) = fz(i, j, k)
						extrapolatedsubpsupi(i, j, k) = p(i, j, k)
						extrapolatedsubpsupj(i, j, k) = p(i, j, k)
						extrapolatedsubpsupk(i, j, k) = p(i, j, k)
					end if
					if ((FOV_yUpper(i, j, k) .gt. 0) .and. ((j - 1 .ge. 1 .and. (FOV_1(i, int(j - 1), k) .gt. 0)) .or. (j - 2 .ge. 1 .and. (FOV_1(i, int(j - 2), k) .gt. 0)) .or. (j - 3 .ge. 1 .and. (FOV_1(i, int(j - 3), k) .gt. 0)))) then
						extrapolatedsubrhosupi(i, j, k) = rho(i, j, k)
						extrapolatedsubrhosupj(i, j, k) = rho(i, j, k)
						extrapolatedsubrhosupk(i, j, k) = rho(i, j, k)
						extrapolatedsubmxsupi(i, j, k) = mx(i, j, k)
						extrapolatedsubmxsupj(i, j, k) = mx(i, j, k)
						extrapolatedsubmxsupk(i, j, k) = mx(i, j, k)
						extrapolatedsubmysupi(i, j, k) = my(i, j, k)
						extrapolatedsubmysupj(i, j, k) = my(i, j, k)
						extrapolatedsubmysupk(i, j, k) = my(i, j, k)
						extrapolatedsubmzsupi(i, j, k) = mz(i, j, k)
						extrapolatedsubmzsupj(i, j, k) = mz(i, j, k)
						extrapolatedsubmzsupk(i, j, k) = mz(i, j, k)
						extrapolatedsubNullsupi(i, j, k) = Null(i, j, k)
						extrapolatedsubNullsupj(i, j, k) = Null(i, j, k)
						extrapolatedsubNullsupk(i, j, k) = Null(i, j, k)
						extrapolatedsubfxsupi(i, j, k) = fx(i, j, k)
						extrapolatedsubfxsupj(i, j, k) = fx(i, j, k)
						extrapolatedsubfxsupk(i, j, k) = fx(i, j, k)
						extrapolatedsubfysupi(i, j, k) = fy(i, j, k)
						extrapolatedsubfysupj(i, j, k) = fy(i, j, k)
						extrapolatedsubfysupk(i, j, k) = fy(i, j, k)
						extrapolatedsubfzsupi(i, j, k) = fz(i, j, k)
						extrapolatedsubfzsupj(i, j, k) = fz(i, j, k)
						extrapolatedsubfzsupk(i, j, k) = fz(i, j, k)
						extrapolatedsubpsupi(i, j, k) = p(i, j, k)
						extrapolatedsubpsupj(i, j, k) = p(i, j, k)
						extrapolatedsubpsupk(i, j, k) = p(i, j, k)
					end if
					if ((FOV_zLower(i, j, k) .gt. 0) .and. ((k + 1 .le. cctk_lsh(3) .and. (FOV_1(i, j, int(k + 1)) .gt. 0)) .or. (k + 2 .le. cctk_lsh(3) .and. (FOV_1(i, j, int(k + 2)) .gt. 0)) .or. (k + 3 .le. cctk_lsh(3) .and. (FOV_1(i, j, int(k + 3)) .gt. 0)))) then
						extrapolatedsubrhosupi(i, j, k) = rho(i, j, k)
						extrapolatedsubrhosupj(i, j, k) = rho(i, j, k)
						extrapolatedsubrhosupk(i, j, k) = rho(i, j, k)
						extrapolatedsubmxsupi(i, j, k) = mx(i, j, k)
						extrapolatedsubmxsupj(i, j, k) = mx(i, j, k)
						extrapolatedsubmxsupk(i, j, k) = mx(i, j, k)
						extrapolatedsubmysupi(i, j, k) = my(i, j, k)
						extrapolatedsubmysupj(i, j, k) = my(i, j, k)
						extrapolatedsubmysupk(i, j, k) = my(i, j, k)
						extrapolatedsubmzsupi(i, j, k) = mz(i, j, k)
						extrapolatedsubmzsupj(i, j, k) = mz(i, j, k)
						extrapolatedsubmzsupk(i, j, k) = mz(i, j, k)
						extrapolatedsubNullsupi(i, j, k) = Null(i, j, k)
						extrapolatedsubNullsupj(i, j, k) = Null(i, j, k)
						extrapolatedsubNullsupk(i, j, k) = Null(i, j, k)
						extrapolatedsubfxsupi(i, j, k) = fx(i, j, k)
						extrapolatedsubfxsupj(i, j, k) = fx(i, j, k)
						extrapolatedsubfxsupk(i, j, k) = fx(i, j, k)
						extrapolatedsubfysupi(i, j, k) = fy(i, j, k)
						extrapolatedsubfysupj(i, j, k) = fy(i, j, k)
						extrapolatedsubfysupk(i, j, k) = fy(i, j, k)
						extrapolatedsubfzsupi(i, j, k) = fz(i, j, k)
						extrapolatedsubfzsupj(i, j, k) = fz(i, j, k)
						extrapolatedsubfzsupk(i, j, k) = fz(i, j, k)
						extrapolatedsubpsupi(i, j, k) = p(i, j, k)
						extrapolatedsubpsupj(i, j, k) = p(i, j, k)
						extrapolatedsubpsupk(i, j, k) = p(i, j, k)
					end if
					if ((FOV_zUpper(i, j, k) .gt. 0) .and. ((k - 1 .ge. 1 .and. (FOV_1(i, j, int(k - 1)) .gt. 0)) .or. (k - 2 .ge. 1 .and. (FOV_1(i, j, int(k - 2)) .gt. 0)) .or. (k - 3 .ge. 1 .and. (FOV_1(i, j, int(k - 3)) .gt. 0)))) then
						extrapolatedsubrhosupi(i, j, k) = rho(i, j, k)
						extrapolatedsubrhosupj(i, j, k) = rho(i, j, k)
						extrapolatedsubrhosupk(i, j, k) = rho(i, j, k)
						extrapolatedsubmxsupi(i, j, k) = mx(i, j, k)
						extrapolatedsubmxsupj(i, j, k) = mx(i, j, k)
						extrapolatedsubmxsupk(i, j, k) = mx(i, j, k)
						extrapolatedsubmysupi(i, j, k) = my(i, j, k)
						extrapolatedsubmysupj(i, j, k) = my(i, j, k)
						extrapolatedsubmysupk(i, j, k) = my(i, j, k)
						extrapolatedsubmzsupi(i, j, k) = mz(i, j, k)
						extrapolatedsubmzsupj(i, j, k) = mz(i, j, k)
						extrapolatedsubmzsupk(i, j, k) = mz(i, j, k)
						extrapolatedsubNullsupi(i, j, k) = Null(i, j, k)
						extrapolatedsubNullsupj(i, j, k) = Null(i, j, k)
						extrapolatedsubNullsupk(i, j, k) = Null(i, j, k)
						extrapolatedsubfxsupi(i, j, k) = fx(i, j, k)
						extrapolatedsubfxsupj(i, j, k) = fx(i, j, k)
						extrapolatedsubfxsupk(i, j, k) = fx(i, j, k)
						extrapolatedsubfysupi(i, j, k) = fy(i, j, k)
						extrapolatedsubfysupj(i, j, k) = fy(i, j, k)
						extrapolatedsubfysupk(i, j, k) = fy(i, j, k)
						extrapolatedsubfzsupi(i, j, k) = fz(i, j, k)
						extrapolatedsubfzsupj(i, j, k) = fz(i, j, k)
						extrapolatedsubfzsupk(i, j, k) = fz(i, j, k)
						extrapolatedsubpsupi(i, j, k) = p(i, j, k)
						extrapolatedsubpsupj(i, j, k) = p(i, j, k)
						extrapolatedsubpsupk(i, j, k) = p(i, j, k)
					end if
				end do
			end do
		end do
		call CCTK_SyncGroup(status, cctkGH, "FDAsNozzle3D::fields_tl2")
		call CCTK_SyncGroup(status, cctkGH, "FDAsNozzle3D::fields_tl1")
		call CCTK_SyncGroup(status, cctkGH, "FDAsNozzle3D::block4")

	end subroutine FDAsNozzle3D_execution