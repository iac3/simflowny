#include "Problem.h"
#include "Functions.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <stack>
#include "hdf5.h"
#include <gsl/gsl_rng.h>
#include <random>
#include "SAMRAI/pdat/CellVariable.h"
#include "SAMRAI/pdat/NodeData.h"
#include "SAMRAI/pdat/NodeVariable.h"
#include "LagrangianPolynomicRefine.h"


#include "SAMRAI/tbox/Array.h"
#include "SAMRAI/hier/BoundaryBox.h"
#include "SAMRAI/hier/BoxContainer.h"
#include "SAMRAI/geom/CartesianPatchGeometry.h"
#include "SAMRAI/hier/VariableDatabase.h"
#include "SAMRAI/hier/PatchDataRestartManager.h"
#include "SAMRAI/hier/Index.h"
#include "SAMRAI/tbox/PIO.h"
#include "SAMRAI/tbox/Utilities.h"
#include "SAMRAI/tbox/Timer.h"
#include "SAMRAI/tbox/TimerManager.h"

#define MIN(X,Y) ((X) < (Y) ? (X) : (Y))
#define MAX(X,Y) ((X) > (Y) ? (X) : (Y))
#define SIGN(X) (((X) > 0) - ((X) < 0))
#define isEven(a) ((a) % 2 == 0 ? true : false)
#define greaterEq(a,b) ((fabs((a) - (b))/1.0E-15 > 10 ? false : (floor(fabs((a) - (b))/1.0E-15) < 1)) || (b)<(a))
#define lessEq(a,b) ((fabs((a) - (b))/1.0E-15 > 10 ? false: (floor(fabs((a) - (b))/1.0E-15) < 1)) || (a)<(b))
#define equalsEq(a,b) ((fabs((a) - (b))/1.0E-15 > 10 ? false: (floor(fabs((a) - (b))/1.0E-15) < 1)))
#define reducePrecision(x, p) (floor(((x) * pow(10, (p)) + 0.5)) / pow(10, (p)))

using namespace external;

const gsl_rng_type * T;
gsl_rng *r_var;

//Timers
std::shared_ptr<tbox::Timer> t_step;
std::shared_ptr<tbox::Timer> t_moveParticles;
std::shared_ptr<tbox::Timer> t_output;

inline int Problem::GetExpoBase2(double d)
{
	int i = 0;
	((short *)(&i))[0] = (((short *)(&d))[3] & (short)32752); // _123456789ab____ & 0111111111110000
	return (i >> 4) - 1023;
}

bool	Problem::Equals(double d1, double d2)
{
	if (d1 == d2)
		return true;
	int e1 = GetExpoBase2(d1);
	int e2 = GetExpoBase2(d2);
	int e3 = GetExpoBase2(d1 - d2);
	if ((e3 - e2 < -48) && (e3 - e1 < -48))
		return true;
	return false;
}

/*
 * Constructor of the problem.
 */
Problem::Problem(const string& object_name, const tbox::Dimension& dim, std::shared_ptr<tbox::Database>& database, std::shared_ptr<geom::CartesianGridGeometry >& grid_geom, std::shared_ptr<hier::PatchHierarchy >& patch_hierarchy, MainRestartData& mrd, const double dt, const bool init_from_restart, const int console_output, const int timer_output, const int mesh_output_period, const vector<string> full_mesh_writer_variables, std::shared_ptr<appu::VisItDataWriter>& mesh_data_writer, const vector<int> slicer_output_period, const vector<set<string> > sliceVariables, vector<std::shared_ptr<SlicerDataWriter > >sliceWriters, const vector<int> sphere_output_period, const vector<set<string> > sphereVariables, vector<std::shared_ptr<SphereDataWriter > > sphereWriters, const vector<int> integration_output_period, const vector<set<string> > integralVariables, vector<std::shared_ptr<IntegrateDataWriter > > integrateDataWriters, const vector<int> point_output_period, const vector<set<string> > pointVariables, vector<std::shared_ptr<PointDataWriter > > pointDataWriters): 
d_dim(dim), xfer::RefinePatchStrategy(), xfer::CoarsenPatchStrategy(), viz_mesh_dump_interval(mesh_output_period), d_full_mesh_writer_variables(full_mesh_writer_variables.begin(), full_mesh_writer_variables.end()), d_visit_data_writer(mesh_data_writer), d_output_interval(console_output), d_timer_output_interval(timer_output), d_sliceWriters(sliceWriters.begin(), sliceWriters.end()), d_sphereWriters(sphereWriters.begin(), sphereWriters.end()), d_slicer_output_period(slicer_output_period.begin(), slicer_output_period.end()), d_sphere_output_period(sphere_output_period.begin(), sphere_output_period.end()), d_integrateDataWriters(integrateDataWriters.begin(), integrateDataWriters.end()), d_integration_output_period(integration_output_period.begin(), integration_output_period.end()), d_pointDataWriters(pointDataWriters.begin(), pointDataWriters.end()), d_point_output_period(point_output_period.begin(), point_output_period.end())
{
	//Setup the timers
	t_step = tbox::TimerManager::getManager()->getTimer("Step");
	t_moveParticles = tbox::TimerManager::getManager()->getTimer("Move particles");
	t_output = tbox::TimerManager::getManager()->getTimer("OutputGeneration");

	//Output configuration
	next_console_output = d_output_interval;
	next_timer_output = d_timer_output_interval;

	//Get the object name, the grid geometry and the patch hierarchy
  	d_grid_geometry = grid_geom;
	d_patch_hierarchy = patch_hierarchy;
	d_object_name = object_name;
	d_init_from_restart = init_from_restart;
	initial_dt = dt;

	for (vector<set<string> >::const_iterator it = sliceVariables.begin(); it != sliceVariables.end(); ++it) {
		set<string> vars = *it;
		for (set<string>::const_iterator it2 = vars.begin() ; it2 != vars.end(); ++it2) {
			d_sliceVariables.push_back(vars);
		}
	}
	for (vector<set<string> >::const_iterator it = sphereVariables.begin(); it != sphereVariables.end(); ++it) {
		set<string> vars = *it;
		for (set<string>::const_iterator it2 = vars.begin() ; it2 != vars.end(); ++it2) {
			d_sphereVariables.push_back(vars);
		}
	}
	for (vector<set<string> >::const_iterator it = integralVariables.begin(); it != integralVariables.end(); ++it) {
		set<string> vars = *it;
		for (set<string>::const_iterator it2 = vars.begin() ; it2 != vars.end(); ++it2) {
			d_integralVariables.push_back(vars);
		}
	}
	for (vector<set<string> >::const_iterator it = pointVariables.begin(); it != pointVariables.end(); ++it) {
		set<string> vars = *it;
		for (set<string>::const_iterator it2 = vars.begin() ; it2 != vars.end(); ++it2) {
			d_pointVariables.push_back(vars);
		}
	}


	//Get parameters
    cout<<"Reading parameters"<<endl;
	pfy = database->getDouble("pfy");
	rho0 = database->getDouble("rho0");
	pfx = database->getDouble("pfx");
	lambdain = database->getDouble("lambdain");
	pfz = database->getDouble("pfz");
	lambdaout = database->getDouble("lambdaout");
	mu = database->getDouble("mu");
	c0 = database->getDouble("c0");
	Paux = database->getDouble("Paux");
	tend = database->getDouble("tend");
	lambda = database->getDouble("lambda");
	vt0 = database->getDouble("vt0");
	v0 = database->getDouble("v0");
	gamma = database->getDouble("gamma");
	//Random initialization
	gsl_rng_env_setup();
	//Random for simulation
	const tbox::SAMRAI_MPI& mpi(tbox::SAMRAI_MPI::getSAMRAIWorld());
	int random_seed = database->getDouble("random_seed")*(mpi.getRank() + 1);
	r_var = gsl_rng_alloc(gsl_rng_ranlxs0);
	gsl_rng_set(r_var, random_seed);
	for (int il = 0; il < d_patch_hierarchy->getMaxNumberOfLevels(); il++) {
		bo_substep_iteration.push_back(0);
	}
	//Initialization from input file
	if (!d_init_from_restart) {
		next_mesh_dump_iteration = viz_mesh_dump_interval;
		for (std::vector<int>::iterator it = d_slicer_output_period.begin(); it != d_slicer_output_period.end(); ++it) {
			next_slice_dump_iteration.push_back((*it));
		}
		for (std::vector<int>::iterator it = d_sphere_output_period.begin(); it != d_sphere_output_period.end(); ++it) {
			next_sphere_dump_iteration.push_back((*it));
		}
		for (std::vector<int>::iterator it = d_integration_output_period.begin(); it != d_integration_output_period.end(); ++it) {
			next_integration_dump_iteration.push_back((*it));
		}
		for (std::vector<int>::iterator it = d_point_output_period.begin(); it != d_point_output_period.end(); ++it) {
			next_point_dump_iteration.push_back((*it));
		}

		//Iteration counter
		for (int il = 0; il < d_patch_hierarchy->getMaxNumberOfLevels(); il++) {
			current_iteration.push_back(0);
		}
	}
	//Initialization from restart file
	else {
		getFromRestart(mrd);
		if (d_slicer_output_period.size() < next_slice_dump_iteration.size()) {
			TBOX_ERROR("Number of slices cannot be reduced after a checkpoint.");
		}
		for (int il = 0; il < d_slicer_output_period.size(); il++) {
			if (il >= next_slice_dump_iteration.size()) {
				next_slice_dump_iteration.push_back(0);
			}
			if (next_slice_dump_iteration[il] == 0 && d_slicer_output_period[il] > 0) {
				next_slice_dump_iteration[il] = current_iteration[d_patch_hierarchy->getNumberOfLevels() - 1] + d_slicer_output_period[il];
			}
		}
		if (d_sphere_output_period.size() < next_sphere_dump_iteration.size()) {
			TBOX_ERROR("Number of spheres cannot be reduced after a checkpoint.");
		}
		for (int il = 0; il < d_sphere_output_period.size(); il++) {
			if (il >= next_sphere_dump_iteration.size()) {
				next_sphere_dump_iteration.push_back(0);
			}
			if (next_sphere_dump_iteration[il] == 0 && d_sphere_output_period[il] > 0) {
				next_sphere_dump_iteration[il] = current_iteration[d_patch_hierarchy->getNumberOfLevels() - 1] + d_sphere_output_period[il];
			}
		}
		if (d_integration_output_period.size() < next_integration_dump_iteration.size()) {
			TBOX_ERROR("Number of integrations cannot be reduced after a checkpoint.");
		}
		for (int il = 0; il < d_integration_output_period.size(); il++) {
			if (il >= next_integration_dump_iteration.size()) {
				next_integration_dump_iteration.push_back(0);
			}
			if (next_integration_dump_iteration[il] == 0 && d_integration_output_period[il] > 0) {
				next_integration_dump_iteration[il] = current_iteration[d_patch_hierarchy->getNumberOfLevels() - 1] + d_integration_output_period[il];
			}
		}
		if (d_point_output_period.size() < next_point_dump_iteration.size()) {
			TBOX_ERROR("Number of point output cannot be reduced after a checkpoint.");
		}
		for (int il = 0; il < d_point_output_period.size(); il++) {
			if (il >= next_point_dump_iteration.size()) {
				next_point_dump_iteration.push_back(0);
			}
			if (next_point_dump_iteration[il] == 0 && d_point_output_period[il] > 0) {
				next_point_dump_iteration[il] = current_iteration[d_patch_hierarchy->getNumberOfLevels() - 1] + d_point_output_period[il];
			}
		}
	}


	//External eos parameters
#ifdef EXTERNAL_EOS

	std::shared_ptr<tbox::Database> external_eos_db = database->getDatabase("external_EOS");
	Commons::ExternalEos::reprimand_eos_type = external_eos_db->getInteger("eos_type");
	Commons::ExternalEos::reprimand_atmo_Ye = external_eos_db->getDouble("atmo_Ye");
	Commons::ExternalEos::reprimand_max_z = external_eos_db->getDouble("max_z");
	Commons::ExternalEos::reprimand_max_b = external_eos_db->getDouble("max_b");
	Commons::ExternalEos::reprimand_c2p_acc = external_eos_db->getDouble("c2p_acc");
	Commons::ExternalEos::reprimand_atmo_rho = external_eos_db->getDouble("atmo_rho");
	Commons::ExternalEos::reprimand_rho_strict = external_eos_db->getDouble("rho_strict");
	Commons::ExternalEos::reprimand_max_rho = external_eos_db->getDouble("max_rho");
	Commons::ExternalEos::reprimand_max_eps = external_eos_db->getDouble("max_eps");
	Commons::ExternalEos::reprimand_gamma_th = external_eos_db->getDouble("gamma_th");
#endif

    	//Subcycling
	d_refinedTimeStepping = false;
	if (database->isString("subcycling")) {
		if (database->getString("subcycling") == "BERGER-OLIGER") {
			d_refinedTimeStepping = true;
		}
	}


	//Regridding options
	d_regridding = false;
	if (database->isDatabase("regridding")) {
		regridding_db = database->getDatabase("regridding");
		d_regridding_buffer = regridding_db->getDouble("regridding_buffer");
		int smallest_patch_size = d_patch_hierarchy->getSmallestPatchSize(0).min();
		for (int il = 1; il < d_patch_hierarchy->getMaxNumberOfLevels(); il++) {
			smallest_patch_size = MIN(smallest_patch_size, d_patch_hierarchy->getSmallestPatchSize(il).min());
		}
		if (d_regridding_buffer > smallest_patch_size) {
			TBOX_ERROR("Error: Regridding_buffer parameter ("<<d_regridding_buffer<<") cannot be greater than smallest_patch_size minimum value("<<smallest_patch_size<<")");
		}
		if (regridding_db->isString("regridding_type")) {
			d_regridding_type = regridding_db->getString("regridding_type");
			d_regridding_min_level = regridding_db->getInteger("regridding_min_level");
			d_regridding_max_level = regridding_db->getInteger("regridding_max_level");
			if (d_regridding_type == "GRADIENT") {
				d_regridding_field = regridding_db->getString("regridding_field");
				d_regridding_compressionFactor = regridding_db->getDouble("regridding_compressionFactor");
				d_regridding_mOffset = regridding_db->getDouble("regridding_mOffset");
				d_regridding = true;
			} else {
				if (d_regridding_type == "FUNCTION") {
					d_regridding_field = regridding_db->getString("regridding_function_field");
					d_regridding_threshold = regridding_db->getDouble("regridding_threshold");
					d_regridding = true;
				} else {
					if (d_regridding_type == "SHADOW") {
						std::string* fields = new std::string[2];
						regridding_db->getStringArray("regridding_fields", fields, 2);
						d_regridding_field = fields[0];
						d_regridding_field_shadow = fields[1];
						d_regridding_error = regridding_db->getDouble("regridding_error");
						d_regridding = true;
						delete[] fields;
					}
				}
			}
		}
	}

	//Stencil of the discretization method
	int maxratio = 1;
	for (int il = 1; il < d_patch_hierarchy->getMaxNumberOfLevels(); il++) {
		const hier::IntVector ratio = d_patch_hierarchy->getRatioToCoarserLevel(il);
		maxratio = MAX(maxratio, ratio.max());
	}
	//Minimum region thickness
	d_regionMinThickness = 2;
	d_ghost_width = 2;
	//Extrapolation extra stencil
	d_ghost_width = d_ghost_width + 2 - 1;

	
	//Register Fields and temporal fields into the variable database
	hier::VariableDatabase *vdb = hier::VariableDatabase::getDatabase();
  	std::shared_ptr<hier::VariableContext> d_cont_curr(vdb->getContext("Current"));
	std::shared_ptr< pdat::CellVariable<int> > mask(std::shared_ptr< pdat::CellVariable<int> >(new pdat::CellVariable<int>(d_dim, "samrai_mask",1)));
	d_mask_id = vdb->registerVariableAndContext(mask ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	IntegrateDataWriter::setMaskVariable(d_mask_id);
	std::shared_ptr< pdat::NodeVariable<double> > interior(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "regridding_value",1)));
	d_interior_regridding_value_id = vdb->registerVariableAndContext(interior ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<int> > nonSync(std::shared_ptr< pdat::NodeVariable<int> >(new pdat::NodeVariable<int>(d_dim, "regridding_tag",1)));
	d_nonSync_regridding_tag_id = vdb->registerVariableAndContext(nonSync ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<double> > interior_i(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "interior_i",1)));
	d_interior_i_id = vdb->registerVariableAndContext(interior_i ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<double> > interior_j(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "interior_j",1)));
	d_interior_j_id = vdb->registerVariableAndContext(interior_j ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<double> > interior_k(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "interior_k",1)));
	d_interior_k_id = vdb->registerVariableAndContext(interior_k ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<double> > FOV_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FOV_1",1)));
	d_FOV_1_id = vdb->registerVariableAndContext(FOV_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FOV_1_id);
	std::shared_ptr< pdat::NodeVariable<double> > FOV_3(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FOV_3",1)));
	d_FOV_3_id = vdb->registerVariableAndContext(FOV_3 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FOV_3_id);
	std::shared_ptr< pdat::NodeVariable<double> > FOV_xLower(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FOV_xLower",1)));
	d_FOV_xLower_id = vdb->registerVariableAndContext(FOV_xLower ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FOV_xLower_id);
	std::shared_ptr< pdat::NodeVariable<double> > FOV_xUpper(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FOV_xUpper",1)));
	d_FOV_xUpper_id = vdb->registerVariableAndContext(FOV_xUpper ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FOV_xUpper_id);
	std::shared_ptr< pdat::NodeVariable<double> > FOV_yLower(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FOV_yLower",1)));
	d_FOV_yLower_id = vdb->registerVariableAndContext(FOV_yLower ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FOV_yLower_id);
	std::shared_ptr< pdat::NodeVariable<double> > FOV_yUpper(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FOV_yUpper",1)));
	d_FOV_yUpper_id = vdb->registerVariableAndContext(FOV_yUpper ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FOV_yUpper_id);
	std::shared_ptr< pdat::NodeVariable<double> > FOV_zLower(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FOV_zLower",1)));
	d_FOV_zLower_id = vdb->registerVariableAndContext(FOV_zLower ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FOV_zLower_id);
	std::shared_ptr< pdat::NodeVariable<double> > FOV_zUpper(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FOV_zUpper",1)));
	d_FOV_zUpper_id = vdb->registerVariableAndContext(FOV_zUpper ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FOV_zUpper_id);
	std::shared_ptr< pdat::NodeVariable<double> > rho(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "rho",1)));
	d_rho_id = vdb->registerVariableAndContext(rho ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_rho_id);
	std::shared_ptr< pdat::NodeVariable<double> > mx(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "mx",1)));
	d_mx_id = vdb->registerVariableAndContext(mx ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_mx_id);
	std::shared_ptr< pdat::NodeVariable<double> > my(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "my",1)));
	d_my_id = vdb->registerVariableAndContext(my ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_my_id);
	std::shared_ptr< pdat::NodeVariable<double> > mz(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "mz",1)));
	d_mz_id = vdb->registerVariableAndContext(mz ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_mz_id);
	std::shared_ptr< pdat::NodeVariable<double> > Null(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "Null",1)));
	d_Null_id = vdb->registerVariableAndContext(Null ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_Null_id);
	std::shared_ptr< pdat::NodeVariable<double> > p(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "p",1)));
	d_p_id = vdb->registerVariableAndContext(p ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_p_id);
	std::shared_ptr< pdat::NodeVariable<double> > fx(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "fx",1)));
	d_fx_id = vdb->registerVariableAndContext(fx ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_fx_id);
	std::shared_ptr< pdat::NodeVariable<double> > fy(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "fy",1)));
	d_fy_id = vdb->registerVariableAndContext(fy ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_fy_id);
	std::shared_ptr< pdat::NodeVariable<double> > fz(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "fz",1)));
	d_fz_id = vdb->registerVariableAndContext(fz ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_fz_id);
	std::shared_ptr< pdat::NodeVariable<double> > SpeedSBi_3(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "SpeedSBi_3",1)));
	d_SpeedSBi_3_id = vdb->registerVariableAndContext(SpeedSBi_3 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<double> > SpeedSBj_3(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "SpeedSBj_3",1)));
	d_SpeedSBj_3_id = vdb->registerVariableAndContext(SpeedSBj_3 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<double> > SpeedSBk_3(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "SpeedSBk_3",1)));
	d_SpeedSBk_3_id = vdb->registerVariableAndContext(SpeedSBk_3 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<double> > rk1Null(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "rk1Null",1)));
	d_rk1Null_id = vdb->registerVariableAndContext(rk1Null ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<double> > rk2Null(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "rk2Null",1)));
	d_rk2Null_id = vdb->registerVariableAndContext(rk2Null ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<double> > SpeedSBi_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "SpeedSBi_1",1)));
	d_SpeedSBi_1_id = vdb->registerVariableAndContext(SpeedSBi_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<double> > SpeedSBj_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "SpeedSBj_1",1)));
	d_SpeedSBj_1_id = vdb->registerVariableAndContext(SpeedSBj_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<double> > SpeedSBk_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "SpeedSBk_1",1)));
	d_SpeedSBk_1_id = vdb->registerVariableAndContext(SpeedSBk_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<double> > ad_mx_o0_t0_m0_l0_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "ad_mx_o0_t0_m0_l0_1",1)));
	d_ad_mx_o0_t0_m0_l0_1_id = vdb->registerVariableAndContext(ad_mx_o0_t0_m0_l0_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<double> > ad_my_o0_t3_m0_l0_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "ad_my_o0_t3_m0_l0_1",1)));
	d_ad_my_o0_t3_m0_l0_1_id = vdb->registerVariableAndContext(ad_my_o0_t3_m0_l0_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<double> > ad_mz_o0_t3_m0_l0_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "ad_mz_o0_t3_m0_l0_1",1)));
	d_ad_mz_o0_t3_m0_l0_1_id = vdb->registerVariableAndContext(ad_mz_o0_t3_m0_l0_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<double> > FluxSBimx_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FluxSBimx_1",1)));
	d_FluxSBimx_1_id = vdb->registerVariableAndContext(FluxSBimx_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<double> > FluxSBimy_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FluxSBimy_1",1)));
	d_FluxSBimy_1_id = vdb->registerVariableAndContext(FluxSBimy_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<double> > FluxSBimz_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FluxSBimz_1",1)));
	d_FluxSBimz_1_id = vdb->registerVariableAndContext(FluxSBimz_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<double> > FluxSBjmx_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FluxSBjmx_1",1)));
	d_FluxSBjmx_1_id = vdb->registerVariableAndContext(FluxSBjmx_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<double> > FluxSBjmy_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FluxSBjmy_1",1)));
	d_FluxSBjmy_1_id = vdb->registerVariableAndContext(FluxSBjmy_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<double> > FluxSBjmz_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FluxSBjmz_1",1)));
	d_FluxSBjmz_1_id = vdb->registerVariableAndContext(FluxSBjmz_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<double> > FluxSBkmx_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FluxSBkmx_1",1)));
	d_FluxSBkmx_1_id = vdb->registerVariableAndContext(FluxSBkmx_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<double> > FluxSBkmy_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FluxSBkmy_1",1)));
	d_FluxSBkmy_1_id = vdb->registerVariableAndContext(FluxSBkmy_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<double> > FluxSBkmz_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FluxSBkmz_1",1)));
	d_FluxSBkmz_1_id = vdb->registerVariableAndContext(FluxSBkmz_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<double> > rk1rho(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "rk1rho",1)));
	d_rk1rho_id = vdb->registerVariableAndContext(rk1rho ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<double> > rk1mx(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "rk1mx",1)));
	d_rk1mx_id = vdb->registerVariableAndContext(rk1mx ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<double> > rk1my(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "rk1my",1)));
	d_rk1my_id = vdb->registerVariableAndContext(rk1my ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<double> > rk1mz(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "rk1mz",1)));
	d_rk1mz_id = vdb->registerVariableAndContext(rk1mz ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<double> > rk2rho(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "rk2rho",1)));
	d_rk2rho_id = vdb->registerVariableAndContext(rk2rho ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<double> > rk2mx(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "rk2mx",1)));
	d_rk2mx_id = vdb->registerVariableAndContext(rk2mx ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<double> > rk2my(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "rk2my",1)));
	d_rk2my_id = vdb->registerVariableAndContext(rk2my ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<double> > rk2mz(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "rk2mz",1)));
	d_rk2mz_id = vdb->registerVariableAndContext(rk2mz ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<double> > extrapolatedSBrhoSPi(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "extrapolatedSBrhoSPi",1)));
	d_extrapolatedSBrhoSPi_id = vdb->registerVariableAndContext(extrapolatedSBrhoSPi ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_extrapolatedSBrhoSPi_id);
	std::shared_ptr< pdat::NodeVariable<double> > extrapolatedSBrhoSPj(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "extrapolatedSBrhoSPj",1)));
	d_extrapolatedSBrhoSPj_id = vdb->registerVariableAndContext(extrapolatedSBrhoSPj ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_extrapolatedSBrhoSPj_id);
	std::shared_ptr< pdat::NodeVariable<double> > extrapolatedSBrhoSPk(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "extrapolatedSBrhoSPk",1)));
	d_extrapolatedSBrhoSPk_id = vdb->registerVariableAndContext(extrapolatedSBrhoSPk ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_extrapolatedSBrhoSPk_id);
	std::shared_ptr< pdat::NodeVariable<double> > extrapolatedSBmxSPi(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "extrapolatedSBmxSPi",1)));
	d_extrapolatedSBmxSPi_id = vdb->registerVariableAndContext(extrapolatedSBmxSPi ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_extrapolatedSBmxSPi_id);
	std::shared_ptr< pdat::NodeVariable<double> > extrapolatedSBmxSPj(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "extrapolatedSBmxSPj",1)));
	d_extrapolatedSBmxSPj_id = vdb->registerVariableAndContext(extrapolatedSBmxSPj ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_extrapolatedSBmxSPj_id);
	std::shared_ptr< pdat::NodeVariable<double> > extrapolatedSBmxSPk(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "extrapolatedSBmxSPk",1)));
	d_extrapolatedSBmxSPk_id = vdb->registerVariableAndContext(extrapolatedSBmxSPk ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_extrapolatedSBmxSPk_id);
	std::shared_ptr< pdat::NodeVariable<double> > extrapolatedSBmySPi(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "extrapolatedSBmySPi",1)));
	d_extrapolatedSBmySPi_id = vdb->registerVariableAndContext(extrapolatedSBmySPi ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_extrapolatedSBmySPi_id);
	std::shared_ptr< pdat::NodeVariable<double> > extrapolatedSBmySPj(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "extrapolatedSBmySPj",1)));
	d_extrapolatedSBmySPj_id = vdb->registerVariableAndContext(extrapolatedSBmySPj ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_extrapolatedSBmySPj_id);
	std::shared_ptr< pdat::NodeVariable<double> > extrapolatedSBmySPk(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "extrapolatedSBmySPk",1)));
	d_extrapolatedSBmySPk_id = vdb->registerVariableAndContext(extrapolatedSBmySPk ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_extrapolatedSBmySPk_id);
	std::shared_ptr< pdat::NodeVariable<double> > extrapolatedSBmzSPi(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "extrapolatedSBmzSPi",1)));
	d_extrapolatedSBmzSPi_id = vdb->registerVariableAndContext(extrapolatedSBmzSPi ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_extrapolatedSBmzSPi_id);
	std::shared_ptr< pdat::NodeVariable<double> > extrapolatedSBmzSPj(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "extrapolatedSBmzSPj",1)));
	d_extrapolatedSBmzSPj_id = vdb->registerVariableAndContext(extrapolatedSBmzSPj ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_extrapolatedSBmzSPj_id);
	std::shared_ptr< pdat::NodeVariable<double> > extrapolatedSBmzSPk(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "extrapolatedSBmzSPk",1)));
	d_extrapolatedSBmzSPk_id = vdb->registerVariableAndContext(extrapolatedSBmzSPk ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_extrapolatedSBmzSPk_id);
	std::shared_ptr< pdat::NodeVariable<double> > extrapolatedSBpSPi(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "extrapolatedSBpSPi",1)));
	d_extrapolatedSBpSPi_id = vdb->registerVariableAndContext(extrapolatedSBpSPi ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_extrapolatedSBpSPi_id);
	std::shared_ptr< pdat::NodeVariable<double> > extrapolatedSBpSPj(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "extrapolatedSBpSPj",1)));
	d_extrapolatedSBpSPj_id = vdb->registerVariableAndContext(extrapolatedSBpSPj ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_extrapolatedSBpSPj_id);
	std::shared_ptr< pdat::NodeVariable<double> > extrapolatedSBpSPk(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "extrapolatedSBpSPk",1)));
	d_extrapolatedSBpSPk_id = vdb->registerVariableAndContext(extrapolatedSBpSPk ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_extrapolatedSBpSPk_id);
	std::shared_ptr< pdat::NodeVariable<double> > extrapolatedSBfxSPi(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "extrapolatedSBfxSPi",1)));
	d_extrapolatedSBfxSPi_id = vdb->registerVariableAndContext(extrapolatedSBfxSPi ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_extrapolatedSBfxSPi_id);
	std::shared_ptr< pdat::NodeVariable<double> > extrapolatedSBfxSPj(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "extrapolatedSBfxSPj",1)));
	d_extrapolatedSBfxSPj_id = vdb->registerVariableAndContext(extrapolatedSBfxSPj ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_extrapolatedSBfxSPj_id);
	std::shared_ptr< pdat::NodeVariable<double> > extrapolatedSBfxSPk(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "extrapolatedSBfxSPk",1)));
	d_extrapolatedSBfxSPk_id = vdb->registerVariableAndContext(extrapolatedSBfxSPk ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_extrapolatedSBfxSPk_id);
	std::shared_ptr< pdat::NodeVariable<double> > extrapolatedSBfySPi(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "extrapolatedSBfySPi",1)));
	d_extrapolatedSBfySPi_id = vdb->registerVariableAndContext(extrapolatedSBfySPi ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_extrapolatedSBfySPi_id);
	std::shared_ptr< pdat::NodeVariable<double> > extrapolatedSBfySPj(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "extrapolatedSBfySPj",1)));
	d_extrapolatedSBfySPj_id = vdb->registerVariableAndContext(extrapolatedSBfySPj ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_extrapolatedSBfySPj_id);
	std::shared_ptr< pdat::NodeVariable<double> > extrapolatedSBfySPk(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "extrapolatedSBfySPk",1)));
	d_extrapolatedSBfySPk_id = vdb->registerVariableAndContext(extrapolatedSBfySPk ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_extrapolatedSBfySPk_id);
	std::shared_ptr< pdat::NodeVariable<double> > extrapolatedSBfzSPi(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "extrapolatedSBfzSPi",1)));
	d_extrapolatedSBfzSPi_id = vdb->registerVariableAndContext(extrapolatedSBfzSPi ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_extrapolatedSBfzSPi_id);
	std::shared_ptr< pdat::NodeVariable<double> > extrapolatedSBfzSPj(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "extrapolatedSBfzSPj",1)));
	d_extrapolatedSBfzSPj_id = vdb->registerVariableAndContext(extrapolatedSBfzSPj ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_extrapolatedSBfzSPj_id);
	std::shared_ptr< pdat::NodeVariable<double> > extrapolatedSBfzSPk(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "extrapolatedSBfzSPk",1)));
	d_extrapolatedSBfzSPk_id = vdb->registerVariableAndContext(extrapolatedSBfzSPk ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_extrapolatedSBfzSPk_id);
	std::shared_ptr< pdat::NodeVariable<double> > extrapolatedSBNullSPi(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "extrapolatedSBNullSPi",1)));
	d_extrapolatedSBNullSPi_id = vdb->registerVariableAndContext(extrapolatedSBNullSPi ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_extrapolatedSBNullSPi_id);
	std::shared_ptr< pdat::NodeVariable<double> > extrapolatedSBNullSPj(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "extrapolatedSBNullSPj",1)));
	d_extrapolatedSBNullSPj_id = vdb->registerVariableAndContext(extrapolatedSBNullSPj ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_extrapolatedSBNullSPj_id);
	std::shared_ptr< pdat::NodeVariable<double> > extrapolatedSBNullSPk(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "extrapolatedSBNullSPk",1)));
	d_extrapolatedSBNullSPk_id = vdb->registerVariableAndContext(extrapolatedSBNullSPk ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_extrapolatedSBNullSPk_id);
	std::shared_ptr< pdat::NodeVariable<double> > d_i_rho_mx_my_mz_p_fx_fy_fz(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "d_i_rho_mx_my_mz_p_fx_fy_fz",1)));
	d_d_i_rho_mx_my_mz_p_fx_fy_fz_id = vdb->registerVariableAndContext(d_i_rho_mx_my_mz_p_fx_fy_fz ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<double> > d_j_rho_mx_my_mz_p_fx_fy_fz(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "d_j_rho_mx_my_mz_p_fx_fy_fz",1)));
	d_d_j_rho_mx_my_mz_p_fx_fy_fz_id = vdb->registerVariableAndContext(d_j_rho_mx_my_mz_p_fx_fy_fz ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<double> > d_k_rho_mx_my_mz_p_fx_fy_fz(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "d_k_rho_mx_my_mz_p_fx_fy_fz",1)));
	d_d_k_rho_mx_my_mz_p_fx_fy_fz_id = vdb->registerVariableAndContext(d_k_rho_mx_my_mz_p_fx_fy_fz ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<double> > d_i_Null(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "d_i_Null",1)));
	d_d_i_Null_id = vdb->registerVariableAndContext(d_i_Null ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_d_i_Null_id);
	std::shared_ptr< pdat::NodeVariable<double> > d_j_Null(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "d_j_Null",1)));
	d_d_j_Null_id = vdb->registerVariableAndContext(d_j_Null ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_d_j_Null_id);
	std::shared_ptr< pdat::NodeVariable<double> > d_k_Null(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "d_k_Null",1)));
	d_d_k_Null_id = vdb->registerVariableAndContext(d_k_Null ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_d_k_Null_id);
	std::shared_ptr< pdat::NodeVariable<double> > stalled_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "stalled_1",1)));
	d_stalled_1_id = vdb->registerVariableAndContext(stalled_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_stalled_1_id);
	std::shared_ptr< pdat::NodeVariable<double> > stalled_3(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "stalled_3",1)));
	d_stalled_3_id = vdb->registerVariableAndContext(stalled_3 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_stalled_3_id);
	std::shared_ptr< pdat::NodeVariable<double> > rho_p(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "rho_p",1)));
	d_rho_p_id = vdb->registerVariableAndContext(rho_p ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<double> > mx_p(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "mx_p",1)));
	d_mx_p_id = vdb->registerVariableAndContext(mx_p ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<double> > my_p(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "my_p",1)));
	d_my_p_id = vdb->registerVariableAndContext(my_p ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<double> > mz_p(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "mz_p",1)));
	d_mz_p_id = vdb->registerVariableAndContext(mz_p ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<double> > Null_p(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "Null_p",1)));
	d_Null_p_id = vdb->registerVariableAndContext(Null_p ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));


	//Refine and coarse algorithms

	d_bdry_fill_init = std::shared_ptr< xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());
	d_bdry_post_coarsen = std::shared_ptr< xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());
	d_bdry_fill_advance1 = std::shared_ptr< xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());
	d_bdry_fill_advance3 = std::shared_ptr< xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());
	d_bdry_fill_advance9 = std::shared_ptr< xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());
	d_bdry_fill_advance11 = std::shared_ptr< xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());
	d_bdry_fill_advance17 = std::shared_ptr< xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());
	d_bdry_fill_advance19 = std::shared_ptr< xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());
	d_coarsen_algorithm = std::shared_ptr< xfer::CoarsenAlgorithm >(new xfer::CoarsenAlgorithm(d_dim));

	d_mapping_fill = std::shared_ptr< xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());
    d_tagging_fill = std::shared_ptr< xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());
	d_fill_new_level    = std::shared_ptr< xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());
	d_fill_new_level_aux    = std::shared_ptr< xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());


	//mapping communication

	std::shared_ptr< hier::RefineOperator > refine_operator_map = d_grid_geometry->lookupRefineOperator(interior, "LINEAR_REFINE");
	d_mapping_fill->registerRefine(d_interior_regridding_value_id,d_interior_regridding_value_id,d_interior_regridding_value_id, refine_operator_map);
	d_mapping_fill->registerRefine(d_interior_i_id,d_interior_i_id,d_interior_i_id, refine_operator_map);
	d_mapping_fill->registerRefine(d_interior_j_id,d_interior_j_id,d_interior_j_id, refine_operator_map);
	d_mapping_fill->registerRefine(d_interior_k_id,d_interior_k_id,d_interior_k_id, refine_operator_map);
	d_mapping_fill->registerRefine(d_FOV_1_id,d_FOV_1_id,d_FOV_1_id, refine_operator_map);
	d_mapping_fill->registerRefine(d_FOV_3_id,d_FOV_3_id,d_FOV_3_id, refine_operator_map);
	d_mapping_fill->registerRefine(d_FOV_xLower_id,d_FOV_xLower_id,d_FOV_xLower_id, refine_operator_map);
	d_mapping_fill->registerRefine(d_FOV_xUpper_id,d_FOV_xUpper_id,d_FOV_xUpper_id, refine_operator_map);
	d_mapping_fill->registerRefine(d_FOV_yLower_id,d_FOV_yLower_id,d_FOV_yLower_id, refine_operator_map);
	d_mapping_fill->registerRefine(d_FOV_yUpper_id,d_FOV_yUpper_id,d_FOV_yUpper_id, refine_operator_map);
	d_mapping_fill->registerRefine(d_FOV_zLower_id,d_FOV_zLower_id,d_FOV_zLower_id, refine_operator_map);
	d_mapping_fill->registerRefine(d_FOV_zUpper_id,d_FOV_zUpper_id,d_FOV_zUpper_id, refine_operator_map);
	d_mapping_fill->registerRefine(d_stalled_1_id,d_stalled_1_id,d_stalled_1_id, refine_operator_map);
	d_mapping_fill->registerRefine(d_stalled_3_id,d_stalled_3_id,d_stalled_3_id, refine_operator_map);


    d_tagging_fill->registerRefine(d_nonSync_regridding_tag_id,d_nonSync_regridding_tag_id,d_nonSync_regridding_tag_id, d_grid_geometry->lookupRefineOperator(nonSync, "NO_REFINE"));

	//refine and coarsen operators
	string refine_op_name = "LINEAR_REFINE";
	int order = 0;
	if (database->isDatabase("regridding")) {
		regridding_db = database->getDatabase("regridding");
		if (regridding_db->isString("interpolator")) {
			refine_op_name = regridding_db->getString("interpolator");
			if (refine_op_name == "LINEAR_REFINE") {
				order = 1;
			}
			if (refine_op_name == "CUBIC_REFINE") {
				order = 3;
			}
			if (refine_op_name == "QUINTIC_REFINE") {
				order = 5;
			}
		}
	}
	std::shared_ptr< hier::RefineOperator > refine_operator, refine_operator_bound;
	std::shared_ptr< hier::CoarsenOperator > coarsen_operator = d_grid_geometry->lookupCoarsenOperator(FOV_1, "CONSTANT_COARSEN");
	if (order > 0) {
		std::shared_ptr< hier::RefineOperator > tmp_refine_operator(new LagrangianPolynomicRefine(false, order, d_patch_hierarchy, d_dim));
		refine_operator = tmp_refine_operator;
		std::shared_ptr< hier::RefineOperator > tmp_refine_operator_bound(new LagrangianPolynomicRefine(true, order, d_patch_hierarchy, d_dim));
		refine_operator_bound = tmp_refine_operator_bound;
	} else {
		refine_operator = d_grid_geometry->lookupRefineOperator(FOV_1, refine_op_name);
		refine_operator_bound = d_grid_geometry->lookupRefineOperator(FOV_1, refine_op_name);
	}

	std::shared_ptr<SAMRAI::hier::TimeInterpolateOperator> tio_mesh1(new TimeInterpolator(d_grid_geometry, "mesh"));
	time_interpolate_operator_mesh1 = std::dynamic_pointer_cast<TimeInterpolator>(tio_mesh1);


	//Register variables to the refineAlgorithm for boundaries

	d_bdry_fill_advance1->registerRefine(d_SpeedSBk_1_id,d_SpeedSBk_1_id,d_SpeedSBk_1_id,refine_operator);
	d_bdry_fill_advance1->registerRefine(d_SpeedSBj_1_id,d_SpeedSBj_1_id,d_SpeedSBj_1_id,refine_operator);
	d_bdry_fill_advance1->registerRefine(d_SpeedSBi_1_id,d_SpeedSBi_1_id,d_SpeedSBi_1_id,refine_operator);
	if (d_refinedTimeStepping) {
		d_bdry_fill_advance3->registerRefine(d_rk1mz_id,d_rk1mz_id,d_rk1mz_id,refine_operator);
		d_bdry_fill_advance3->registerRefine(d_rk1my_id,d_rk1my_id,d_rk1my_id,refine_operator);
		d_bdry_fill_advance3->registerRefine(d_rk1mx_id,d_rk1mx_id,d_rk1mx_id,refine_operator);
		d_bdry_fill_advance3->registerRefine(d_rk1rho_id,d_rk1rho_id,d_rk1rho_id,refine_operator);
		d_bdry_fill_advance3->registerRefine(d_rk1Null_id,d_rk1Null_id,d_rk1Null_id,refine_operator);
	} else {
		d_bdry_fill_advance3->registerRefine(d_rk1mz_id,d_rk1mz_id,d_rk1mz_id,refine_operator);
		d_bdry_fill_advance3->registerRefine(d_rk1my_id,d_rk1my_id,d_rk1my_id,refine_operator);
		d_bdry_fill_advance3->registerRefine(d_rk1mx_id,d_rk1mx_id,d_rk1mx_id,refine_operator);
		d_bdry_fill_advance3->registerRefine(d_rk1rho_id,d_rk1rho_id,d_rk1rho_id,refine_operator);
		d_bdry_fill_advance3->registerRefine(d_rk1Null_id,d_rk1Null_id,d_rk1Null_id,refine_operator);
	}
	d_bdry_fill_advance9->registerRefine(d_SpeedSBk_1_id,d_SpeedSBk_1_id,d_SpeedSBk_1_id,refine_operator);
	d_bdry_fill_advance9->registerRefine(d_SpeedSBj_1_id,d_SpeedSBj_1_id,d_SpeedSBj_1_id,refine_operator);
	d_bdry_fill_advance9->registerRefine(d_SpeedSBi_1_id,d_SpeedSBi_1_id,d_SpeedSBi_1_id,refine_operator);
	if (d_refinedTimeStepping) {
		d_bdry_fill_advance11->registerRefine(d_rk2mz_id,d_rk2mz_id,d_rk2mz_id,refine_operator);
		d_bdry_fill_advance11->registerRefine(d_rk2my_id,d_rk2my_id,d_rk2my_id,refine_operator);
		d_bdry_fill_advance11->registerRefine(d_rk2mx_id,d_rk2mx_id,d_rk2mx_id,refine_operator);
		d_bdry_fill_advance11->registerRefine(d_rk2rho_id,d_rk2rho_id,d_rk2rho_id,refine_operator);
		d_bdry_fill_advance11->registerRefine(d_rk2Null_id,d_rk2Null_id,d_rk2Null_id,refine_operator);
	} else {
		d_bdry_fill_advance11->registerRefine(d_rk2mz_id,d_rk2mz_id,d_rk2mz_id,refine_operator);
		d_bdry_fill_advance11->registerRefine(d_rk2my_id,d_rk2my_id,d_rk2my_id,refine_operator);
		d_bdry_fill_advance11->registerRefine(d_rk2mx_id,d_rk2mx_id,d_rk2mx_id,refine_operator);
		d_bdry_fill_advance11->registerRefine(d_rk2rho_id,d_rk2rho_id,d_rk2rho_id,refine_operator);
		d_bdry_fill_advance11->registerRefine(d_rk2Null_id,d_rk2Null_id,d_rk2Null_id,refine_operator);
	}
	d_bdry_fill_advance17->registerRefine(d_SpeedSBk_1_id,d_SpeedSBk_1_id,d_SpeedSBk_1_id,refine_operator);
	d_bdry_fill_advance17->registerRefine(d_SpeedSBj_1_id,d_SpeedSBj_1_id,d_SpeedSBj_1_id,refine_operator);
	d_bdry_fill_advance17->registerRefine(d_SpeedSBi_1_id,d_SpeedSBi_1_id,d_SpeedSBi_1_id,refine_operator);
	if (d_refinedTimeStepping) {
		d_bdry_fill_advance19->registerRefine(d_mz_id,d_mz_id,d_mz_p_id,d_mz_id,d_mz_id,refine_operator,tio_mesh1);
		d_bdry_fill_advance19->registerRefine(d_my_id,d_my_id,d_my_p_id,d_my_id,d_my_id,refine_operator,tio_mesh1);
		d_bdry_fill_advance19->registerRefine(d_mx_id,d_mx_id,d_mx_p_id,d_mx_id,d_mx_id,refine_operator,tio_mesh1);
		d_bdry_fill_advance19->registerRefine(d_rho_id,d_rho_id,d_rho_p_id,d_rho_id,d_rho_id,refine_operator,tio_mesh1);
		d_bdry_fill_advance19->registerRefine(d_Null_id,d_Null_id,d_Null_p_id,d_Null_id,d_Null_id,refine_operator,tio_mesh1);
	} else {
		d_bdry_fill_advance19->registerRefine(d_mz_id,d_mz_id,d_mz_id,refine_operator);
		d_bdry_fill_advance19->registerRefine(d_my_id,d_my_id,d_my_id,refine_operator);
		d_bdry_fill_advance19->registerRefine(d_mx_id,d_mx_id,d_mx_id,refine_operator);
		d_bdry_fill_advance19->registerRefine(d_rho_id,d_rho_id,d_rho_id,refine_operator);
		d_bdry_fill_advance19->registerRefine(d_Null_id,d_Null_id,d_Null_id,refine_operator);
	}
	d_bdry_fill_init->registerRefine(d_rho_id,d_rho_id,d_rho_id,refine_operator);
	d_bdry_post_coarsen->registerRefine(d_rho_id,d_rho_id,d_rho_id,refine_operator);
	d_bdry_fill_init->registerRefine(d_mx_id,d_mx_id,d_mx_id,refine_operator);
	d_bdry_post_coarsen->registerRefine(d_mx_id,d_mx_id,d_mx_id,refine_operator);
	d_bdry_fill_init->registerRefine(d_my_id,d_my_id,d_my_id,refine_operator);
	d_bdry_post_coarsen->registerRefine(d_my_id,d_my_id,d_my_id,refine_operator);
	d_bdry_fill_init->registerRefine(d_mz_id,d_mz_id,d_mz_id,refine_operator);
	d_bdry_post_coarsen->registerRefine(d_mz_id,d_mz_id,d_mz_id,refine_operator);
	d_bdry_fill_init->registerRefine(d_Null_id,d_Null_id,d_Null_id,refine_operator);
	d_bdry_post_coarsen->registerRefine(d_Null_id,d_Null_id,d_Null_id,refine_operator);
	d_bdry_fill_init->registerRefine(d_p_id,d_p_id,d_p_id,refine_operator);
	d_bdry_post_coarsen->registerRefine(d_p_id,d_p_id,d_p_id,refine_operator);
	d_bdry_fill_init->registerRefine(d_fx_id,d_fx_id,d_fx_id,refine_operator);
	d_bdry_post_coarsen->registerRefine(d_fx_id,d_fx_id,d_fx_id,refine_operator);
	d_bdry_fill_init->registerRefine(d_fy_id,d_fy_id,d_fy_id,refine_operator);
	d_bdry_post_coarsen->registerRefine(d_fy_id,d_fy_id,d_fy_id,refine_operator);
	d_bdry_fill_init->registerRefine(d_fz_id,d_fz_id,d_fz_id,refine_operator);
	d_bdry_post_coarsen->registerRefine(d_fz_id,d_fz_id,d_fz_id,refine_operator);
	d_bdry_fill_init->registerRefine(d_extrapolatedSBrhoSPi_id , d_extrapolatedSBrhoSPi_id, d_extrapolatedSBrhoSPi_id, refine_operator);
	d_bdry_post_coarsen->registerRefine(d_extrapolatedSBrhoSPi_id , d_extrapolatedSBrhoSPi_id, d_extrapolatedSBrhoSPi_id, refine_operator);
	d_bdry_fill_init->registerRefine(d_extrapolatedSBrhoSPj_id , d_extrapolatedSBrhoSPj_id, d_extrapolatedSBrhoSPj_id, refine_operator);
	d_bdry_post_coarsen->registerRefine(d_extrapolatedSBrhoSPj_id , d_extrapolatedSBrhoSPj_id, d_extrapolatedSBrhoSPj_id, refine_operator);
	d_bdry_fill_init->registerRefine(d_extrapolatedSBrhoSPk_id , d_extrapolatedSBrhoSPk_id, d_extrapolatedSBrhoSPk_id, refine_operator);
	d_bdry_post_coarsen->registerRefine(d_extrapolatedSBrhoSPk_id , d_extrapolatedSBrhoSPk_id, d_extrapolatedSBrhoSPk_id, refine_operator);
	d_bdry_fill_init->registerRefine(d_extrapolatedSBmxSPi_id , d_extrapolatedSBmxSPi_id, d_extrapolatedSBmxSPi_id, refine_operator);
	d_bdry_post_coarsen->registerRefine(d_extrapolatedSBmxSPi_id , d_extrapolatedSBmxSPi_id, d_extrapolatedSBmxSPi_id, refine_operator);
	d_bdry_fill_init->registerRefine(d_extrapolatedSBmxSPj_id , d_extrapolatedSBmxSPj_id, d_extrapolatedSBmxSPj_id, refine_operator);
	d_bdry_post_coarsen->registerRefine(d_extrapolatedSBmxSPj_id , d_extrapolatedSBmxSPj_id, d_extrapolatedSBmxSPj_id, refine_operator);
	d_bdry_fill_init->registerRefine(d_extrapolatedSBmxSPk_id , d_extrapolatedSBmxSPk_id, d_extrapolatedSBmxSPk_id, refine_operator);
	d_bdry_post_coarsen->registerRefine(d_extrapolatedSBmxSPk_id , d_extrapolatedSBmxSPk_id, d_extrapolatedSBmxSPk_id, refine_operator);
	d_bdry_fill_init->registerRefine(d_extrapolatedSBmySPi_id , d_extrapolatedSBmySPi_id, d_extrapolatedSBmySPi_id, refine_operator);
	d_bdry_post_coarsen->registerRefine(d_extrapolatedSBmySPi_id , d_extrapolatedSBmySPi_id, d_extrapolatedSBmySPi_id, refine_operator);
	d_bdry_fill_init->registerRefine(d_extrapolatedSBmySPj_id , d_extrapolatedSBmySPj_id, d_extrapolatedSBmySPj_id, refine_operator);
	d_bdry_post_coarsen->registerRefine(d_extrapolatedSBmySPj_id , d_extrapolatedSBmySPj_id, d_extrapolatedSBmySPj_id, refine_operator);
	d_bdry_fill_init->registerRefine(d_extrapolatedSBmySPk_id , d_extrapolatedSBmySPk_id, d_extrapolatedSBmySPk_id, refine_operator);
	d_bdry_post_coarsen->registerRefine(d_extrapolatedSBmySPk_id , d_extrapolatedSBmySPk_id, d_extrapolatedSBmySPk_id, refine_operator);
	d_bdry_fill_init->registerRefine(d_extrapolatedSBmzSPi_id , d_extrapolatedSBmzSPi_id, d_extrapolatedSBmzSPi_id, refine_operator);
	d_bdry_post_coarsen->registerRefine(d_extrapolatedSBmzSPi_id , d_extrapolatedSBmzSPi_id, d_extrapolatedSBmzSPi_id, refine_operator);
	d_bdry_fill_init->registerRefine(d_extrapolatedSBmzSPj_id , d_extrapolatedSBmzSPj_id, d_extrapolatedSBmzSPj_id, refine_operator);
	d_bdry_post_coarsen->registerRefine(d_extrapolatedSBmzSPj_id , d_extrapolatedSBmzSPj_id, d_extrapolatedSBmzSPj_id, refine_operator);
	d_bdry_fill_init->registerRefine(d_extrapolatedSBmzSPk_id , d_extrapolatedSBmzSPk_id, d_extrapolatedSBmzSPk_id, refine_operator);
	d_bdry_post_coarsen->registerRefine(d_extrapolatedSBmzSPk_id , d_extrapolatedSBmzSPk_id, d_extrapolatedSBmzSPk_id, refine_operator);
	d_bdry_fill_init->registerRefine(d_extrapolatedSBpSPi_id , d_extrapolatedSBpSPi_id, d_extrapolatedSBpSPi_id, refine_operator);
	d_bdry_post_coarsen->registerRefine(d_extrapolatedSBpSPi_id , d_extrapolatedSBpSPi_id, d_extrapolatedSBpSPi_id, refine_operator);
	d_bdry_fill_init->registerRefine(d_extrapolatedSBpSPj_id , d_extrapolatedSBpSPj_id, d_extrapolatedSBpSPj_id, refine_operator);
	d_bdry_post_coarsen->registerRefine(d_extrapolatedSBpSPj_id , d_extrapolatedSBpSPj_id, d_extrapolatedSBpSPj_id, refine_operator);
	d_bdry_fill_init->registerRefine(d_extrapolatedSBpSPk_id , d_extrapolatedSBpSPk_id, d_extrapolatedSBpSPk_id, refine_operator);
	d_bdry_post_coarsen->registerRefine(d_extrapolatedSBpSPk_id , d_extrapolatedSBpSPk_id, d_extrapolatedSBpSPk_id, refine_operator);
	d_bdry_fill_init->registerRefine(d_extrapolatedSBfxSPi_id , d_extrapolatedSBfxSPi_id, d_extrapolatedSBfxSPi_id, refine_operator);
	d_bdry_post_coarsen->registerRefine(d_extrapolatedSBfxSPi_id , d_extrapolatedSBfxSPi_id, d_extrapolatedSBfxSPi_id, refine_operator);
	d_bdry_fill_init->registerRefine(d_extrapolatedSBfxSPj_id , d_extrapolatedSBfxSPj_id, d_extrapolatedSBfxSPj_id, refine_operator);
	d_bdry_post_coarsen->registerRefine(d_extrapolatedSBfxSPj_id , d_extrapolatedSBfxSPj_id, d_extrapolatedSBfxSPj_id, refine_operator);
	d_bdry_fill_init->registerRefine(d_extrapolatedSBfxSPk_id , d_extrapolatedSBfxSPk_id, d_extrapolatedSBfxSPk_id, refine_operator);
	d_bdry_post_coarsen->registerRefine(d_extrapolatedSBfxSPk_id , d_extrapolatedSBfxSPk_id, d_extrapolatedSBfxSPk_id, refine_operator);
	d_bdry_fill_init->registerRefine(d_extrapolatedSBfySPi_id , d_extrapolatedSBfySPi_id, d_extrapolatedSBfySPi_id, refine_operator);
	d_bdry_post_coarsen->registerRefine(d_extrapolatedSBfySPi_id , d_extrapolatedSBfySPi_id, d_extrapolatedSBfySPi_id, refine_operator);
	d_bdry_fill_init->registerRefine(d_extrapolatedSBfySPj_id , d_extrapolatedSBfySPj_id, d_extrapolatedSBfySPj_id, refine_operator);
	d_bdry_post_coarsen->registerRefine(d_extrapolatedSBfySPj_id , d_extrapolatedSBfySPj_id, d_extrapolatedSBfySPj_id, refine_operator);
	d_bdry_fill_init->registerRefine(d_extrapolatedSBfySPk_id , d_extrapolatedSBfySPk_id, d_extrapolatedSBfySPk_id, refine_operator);
	d_bdry_post_coarsen->registerRefine(d_extrapolatedSBfySPk_id , d_extrapolatedSBfySPk_id, d_extrapolatedSBfySPk_id, refine_operator);
	d_bdry_fill_init->registerRefine(d_extrapolatedSBfzSPi_id , d_extrapolatedSBfzSPi_id, d_extrapolatedSBfzSPi_id, refine_operator);
	d_bdry_post_coarsen->registerRefine(d_extrapolatedSBfzSPi_id , d_extrapolatedSBfzSPi_id, d_extrapolatedSBfzSPi_id, refine_operator);
	d_bdry_fill_init->registerRefine(d_extrapolatedSBfzSPj_id , d_extrapolatedSBfzSPj_id, d_extrapolatedSBfzSPj_id, refine_operator);
	d_bdry_post_coarsen->registerRefine(d_extrapolatedSBfzSPj_id , d_extrapolatedSBfzSPj_id, d_extrapolatedSBfzSPj_id, refine_operator);
	d_bdry_fill_init->registerRefine(d_extrapolatedSBfzSPk_id , d_extrapolatedSBfzSPk_id, d_extrapolatedSBfzSPk_id, refine_operator);
	d_bdry_post_coarsen->registerRefine(d_extrapolatedSBfzSPk_id , d_extrapolatedSBfzSPk_id, d_extrapolatedSBfzSPk_id, refine_operator);
	d_bdry_fill_init->registerRefine(d_extrapolatedSBNullSPi_id , d_extrapolatedSBNullSPi_id, d_extrapolatedSBNullSPi_id, refine_operator);
	d_bdry_post_coarsen->registerRefine(d_extrapolatedSBNullSPi_id , d_extrapolatedSBNullSPi_id, d_extrapolatedSBNullSPi_id, refine_operator);
	d_bdry_fill_init->registerRefine(d_extrapolatedSBNullSPj_id , d_extrapolatedSBNullSPj_id, d_extrapolatedSBNullSPj_id, refine_operator);
	d_bdry_post_coarsen->registerRefine(d_extrapolatedSBNullSPj_id , d_extrapolatedSBNullSPj_id, d_extrapolatedSBNullSPj_id, refine_operator);
	d_bdry_fill_init->registerRefine(d_extrapolatedSBNullSPk_id , d_extrapolatedSBNullSPk_id, d_extrapolatedSBNullSPk_id, refine_operator);
	d_bdry_post_coarsen->registerRefine(d_extrapolatedSBNullSPk_id , d_extrapolatedSBNullSPk_id, d_extrapolatedSBNullSPk_id, refine_operator);


	//Register variables to the refineAlgorithm for filling new levels on regridding
	d_fill_new_level->registerRefine(d_rho_id,d_rho_id,d_rho_id,refine_operator_bound);
	d_fill_new_level->registerRefine(d_mx_id,d_mx_id,d_mx_id,refine_operator_bound);
	d_fill_new_level->registerRefine(d_my_id,d_my_id,d_my_id,refine_operator_bound);
	d_fill_new_level->registerRefine(d_mz_id,d_mz_id,d_mz_id,refine_operator_bound);
	d_fill_new_level->registerRefine(d_Null_id,d_Null_id,d_Null_id,refine_operator_bound);
	d_fill_new_level_aux->registerRefine(d_p_id,d_p_id,d_p_id,refine_operator_bound);
	d_fill_new_level->registerRefine(d_p_id,d_p_id,d_p_id,refine_operator_bound);
	d_fill_new_level_aux->registerRefine(d_fx_id,d_fx_id,d_fx_id,refine_operator_bound);
	d_fill_new_level->registerRefine(d_fx_id,d_fx_id,d_fx_id,refine_operator_bound);
	d_fill_new_level_aux->registerRefine(d_fy_id,d_fy_id,d_fy_id,refine_operator_bound);
	d_fill_new_level->registerRefine(d_fy_id,d_fy_id,d_fy_id,refine_operator_bound);
	d_fill_new_level_aux->registerRefine(d_fz_id,d_fz_id,d_fz_id,refine_operator_bound);
	d_fill_new_level->registerRefine(d_fz_id,d_fz_id,d_fz_id,refine_operator_bound);
	d_fill_new_level->registerRefine(d_extrapolatedSBrhoSPi_id,d_extrapolatedSBrhoSPi_id,d_extrapolatedSBrhoSPi_id,refine_operator_bound);
	d_fill_new_level->registerRefine(d_extrapolatedSBrhoSPj_id,d_extrapolatedSBrhoSPj_id,d_extrapolatedSBrhoSPj_id,refine_operator_bound);
	d_fill_new_level->registerRefine(d_extrapolatedSBrhoSPk_id,d_extrapolatedSBrhoSPk_id,d_extrapolatedSBrhoSPk_id,refine_operator_bound);
	d_fill_new_level->registerRefine(d_extrapolatedSBmxSPi_id,d_extrapolatedSBmxSPi_id,d_extrapolatedSBmxSPi_id,refine_operator_bound);
	d_fill_new_level->registerRefine(d_extrapolatedSBmxSPj_id,d_extrapolatedSBmxSPj_id,d_extrapolatedSBmxSPj_id,refine_operator_bound);
	d_fill_new_level->registerRefine(d_extrapolatedSBmxSPk_id,d_extrapolatedSBmxSPk_id,d_extrapolatedSBmxSPk_id,refine_operator_bound);
	d_fill_new_level->registerRefine(d_extrapolatedSBmySPi_id,d_extrapolatedSBmySPi_id,d_extrapolatedSBmySPi_id,refine_operator_bound);
	d_fill_new_level->registerRefine(d_extrapolatedSBmySPj_id,d_extrapolatedSBmySPj_id,d_extrapolatedSBmySPj_id,refine_operator_bound);
	d_fill_new_level->registerRefine(d_extrapolatedSBmySPk_id,d_extrapolatedSBmySPk_id,d_extrapolatedSBmySPk_id,refine_operator_bound);
	d_fill_new_level->registerRefine(d_extrapolatedSBmzSPi_id,d_extrapolatedSBmzSPi_id,d_extrapolatedSBmzSPi_id,refine_operator_bound);
	d_fill_new_level->registerRefine(d_extrapolatedSBmzSPj_id,d_extrapolatedSBmzSPj_id,d_extrapolatedSBmzSPj_id,refine_operator_bound);
	d_fill_new_level->registerRefine(d_extrapolatedSBmzSPk_id,d_extrapolatedSBmzSPk_id,d_extrapolatedSBmzSPk_id,refine_operator_bound);
	d_fill_new_level->registerRefine(d_extrapolatedSBpSPi_id,d_extrapolatedSBpSPi_id,d_extrapolatedSBpSPi_id,refine_operator_bound);
	d_fill_new_level->registerRefine(d_extrapolatedSBpSPj_id,d_extrapolatedSBpSPj_id,d_extrapolatedSBpSPj_id,refine_operator_bound);
	d_fill_new_level->registerRefine(d_extrapolatedSBpSPk_id,d_extrapolatedSBpSPk_id,d_extrapolatedSBpSPk_id,refine_operator_bound);
	d_fill_new_level->registerRefine(d_extrapolatedSBfxSPi_id,d_extrapolatedSBfxSPi_id,d_extrapolatedSBfxSPi_id,refine_operator_bound);
	d_fill_new_level->registerRefine(d_extrapolatedSBfxSPj_id,d_extrapolatedSBfxSPj_id,d_extrapolatedSBfxSPj_id,refine_operator_bound);
	d_fill_new_level->registerRefine(d_extrapolatedSBfxSPk_id,d_extrapolatedSBfxSPk_id,d_extrapolatedSBfxSPk_id,refine_operator_bound);
	d_fill_new_level->registerRefine(d_extrapolatedSBfySPi_id,d_extrapolatedSBfySPi_id,d_extrapolatedSBfySPi_id,refine_operator_bound);
	d_fill_new_level->registerRefine(d_extrapolatedSBfySPj_id,d_extrapolatedSBfySPj_id,d_extrapolatedSBfySPj_id,refine_operator_bound);
	d_fill_new_level->registerRefine(d_extrapolatedSBfySPk_id,d_extrapolatedSBfySPk_id,d_extrapolatedSBfySPk_id,refine_operator_bound);
	d_fill_new_level->registerRefine(d_extrapolatedSBfzSPi_id,d_extrapolatedSBfzSPi_id,d_extrapolatedSBfzSPi_id,refine_operator_bound);
	d_fill_new_level->registerRefine(d_extrapolatedSBfzSPj_id,d_extrapolatedSBfzSPj_id,d_extrapolatedSBfzSPj_id,refine_operator_bound);
	d_fill_new_level->registerRefine(d_extrapolatedSBfzSPk_id,d_extrapolatedSBfzSPk_id,d_extrapolatedSBfzSPk_id,refine_operator_bound);
	d_fill_new_level->registerRefine(d_extrapolatedSBNullSPi_id,d_extrapolatedSBNullSPi_id,d_extrapolatedSBNullSPi_id,refine_operator_bound);
	d_fill_new_level->registerRefine(d_extrapolatedSBNullSPj_id,d_extrapolatedSBNullSPj_id,d_extrapolatedSBNullSPj_id,refine_operator_bound);
	d_fill_new_level->registerRefine(d_extrapolatedSBNullSPk_id,d_extrapolatedSBNullSPk_id,d_extrapolatedSBNullSPk_id,refine_operator_bound);


	//Register variables to the coarsenAlgorithm
	d_coarsen_algorithm->registerCoarsen(d_rho_id,d_rho_id,coarsen_operator);
	d_coarsen_algorithm->registerCoarsen(d_mx_id,d_mx_id,coarsen_operator);
	d_coarsen_algorithm->registerCoarsen(d_my_id,d_my_id,coarsen_operator);
	d_coarsen_algorithm->registerCoarsen(d_mz_id,d_mz_id,coarsen_operator);
	d_coarsen_algorithm->registerCoarsen(d_Null_id,d_Null_id,coarsen_operator);
	d_coarsen_algorithm->registerCoarsen(d_p_id,d_p_id,coarsen_operator);
	d_coarsen_algorithm->registerCoarsen(d_fx_id,d_fx_id,coarsen_operator);
	d_coarsen_algorithm->registerCoarsen(d_fy_id,d_fy_id,coarsen_operator);
	d_coarsen_algorithm->registerCoarsen(d_fz_id,d_fz_id,coarsen_operator);



    Commons::initialization();
}

/*
 * Destructor.
 */
Problem::~Problem() 
{
} 

/*
 * Initialize the data from a given level.
 */
void Problem::initializeLevelData (
   const std::shared_ptr<hier::PatchHierarchy >& hierarchy , 
   const int level_number ,
   const double init_data_time ,
   const bool can_be_refined ,
   const bool initial_time ,
   const std::shared_ptr<hier::PatchLevel >& old_level ,
   const bool allocate_data )
{
    cout<<"Initializing level "<<level_number<<endl;
    tbox::MemoryUtilities::printMemoryInfo(cout);   
	std::shared_ptr< hier::PatchLevel > level(hierarchy->getPatchLevel(level_number));

	// Allocate storage needed to initialize level and fill data from coarser levels in AMR hierarchy.  
	level->allocatePatchData(d_interior_regridding_value_id, init_data_time);
	level->allocatePatchData(d_nonSync_regridding_tag_id, init_data_time);
	level->allocatePatchData(d_interior_i_id, init_data_time);
	level->allocatePatchData(d_interior_j_id, init_data_time);
	level->allocatePatchData(d_interior_k_id, init_data_time);
	level->allocatePatchData(d_FOV_1_id, init_data_time);
	level->allocatePatchData(d_FOV_3_id, init_data_time);
	level->allocatePatchData(d_FOV_xLower_id, init_data_time);
	level->allocatePatchData(d_FOV_xUpper_id, init_data_time);
	level->allocatePatchData(d_FOV_yLower_id, init_data_time);
	level->allocatePatchData(d_FOV_yUpper_id, init_data_time);
	level->allocatePatchData(d_FOV_zLower_id, init_data_time);
	level->allocatePatchData(d_FOV_zUpper_id, init_data_time);
	level->allocatePatchData(d_rho_id, init_data_time);
	level->allocatePatchData(d_mx_id, init_data_time);
	level->allocatePatchData(d_my_id, init_data_time);
	level->allocatePatchData(d_mz_id, init_data_time);
	level->allocatePatchData(d_Null_id, init_data_time);
	level->allocatePatchData(d_p_id, init_data_time);
	level->allocatePatchData(d_fx_id, init_data_time);
	level->allocatePatchData(d_fy_id, init_data_time);
	level->allocatePatchData(d_fz_id, init_data_time);
	level->allocatePatchData(d_SpeedSBi_3_id, init_data_time);
	level->allocatePatchData(d_SpeedSBj_3_id, init_data_time);
	level->allocatePatchData(d_SpeedSBk_3_id, init_data_time);
	level->allocatePatchData(d_rk1Null_id, init_data_time);
	level->allocatePatchData(d_rk2Null_id, init_data_time);
	level->allocatePatchData(d_SpeedSBi_1_id, init_data_time);
	level->allocatePatchData(d_SpeedSBj_1_id, init_data_time);
	level->allocatePatchData(d_SpeedSBk_1_id, init_data_time);
	level->allocatePatchData(d_ad_mx_o0_t0_m0_l0_1_id, init_data_time);
	level->allocatePatchData(d_ad_my_o0_t3_m0_l0_1_id, init_data_time);
	level->allocatePatchData(d_ad_mz_o0_t3_m0_l0_1_id, init_data_time);
	level->allocatePatchData(d_FluxSBimx_1_id, init_data_time);
	level->allocatePatchData(d_FluxSBimy_1_id, init_data_time);
	level->allocatePatchData(d_FluxSBimz_1_id, init_data_time);
	level->allocatePatchData(d_FluxSBjmx_1_id, init_data_time);
	level->allocatePatchData(d_FluxSBjmy_1_id, init_data_time);
	level->allocatePatchData(d_FluxSBjmz_1_id, init_data_time);
	level->allocatePatchData(d_FluxSBkmx_1_id, init_data_time);
	level->allocatePatchData(d_FluxSBkmy_1_id, init_data_time);
	level->allocatePatchData(d_FluxSBkmz_1_id, init_data_time);
	level->allocatePatchData(d_rk1rho_id, init_data_time);
	level->allocatePatchData(d_rk1mx_id, init_data_time);
	level->allocatePatchData(d_rk1my_id, init_data_time);
	level->allocatePatchData(d_rk1mz_id, init_data_time);
	level->allocatePatchData(d_rk2rho_id, init_data_time);
	level->allocatePatchData(d_rk2mx_id, init_data_time);
	level->allocatePatchData(d_rk2my_id, init_data_time);
	level->allocatePatchData(d_rk2mz_id, init_data_time);
	level->allocatePatchData(d_extrapolatedSBrhoSPi_id, init_data_time);
	level->allocatePatchData(d_extrapolatedSBrhoSPj_id, init_data_time);
	level->allocatePatchData(d_extrapolatedSBrhoSPk_id, init_data_time);
	level->allocatePatchData(d_extrapolatedSBmxSPi_id, init_data_time);
	level->allocatePatchData(d_extrapolatedSBmxSPj_id, init_data_time);
	level->allocatePatchData(d_extrapolatedSBmxSPk_id, init_data_time);
	level->allocatePatchData(d_extrapolatedSBmySPi_id, init_data_time);
	level->allocatePatchData(d_extrapolatedSBmySPj_id, init_data_time);
	level->allocatePatchData(d_extrapolatedSBmySPk_id, init_data_time);
	level->allocatePatchData(d_extrapolatedSBmzSPi_id, init_data_time);
	level->allocatePatchData(d_extrapolatedSBmzSPj_id, init_data_time);
	level->allocatePatchData(d_extrapolatedSBmzSPk_id, init_data_time);
	level->allocatePatchData(d_extrapolatedSBpSPi_id, init_data_time);
	level->allocatePatchData(d_extrapolatedSBpSPj_id, init_data_time);
	level->allocatePatchData(d_extrapolatedSBpSPk_id, init_data_time);
	level->allocatePatchData(d_extrapolatedSBfxSPi_id, init_data_time);
	level->allocatePatchData(d_extrapolatedSBfxSPj_id, init_data_time);
	level->allocatePatchData(d_extrapolatedSBfxSPk_id, init_data_time);
	level->allocatePatchData(d_extrapolatedSBfySPi_id, init_data_time);
	level->allocatePatchData(d_extrapolatedSBfySPj_id, init_data_time);
	level->allocatePatchData(d_extrapolatedSBfySPk_id, init_data_time);
	level->allocatePatchData(d_extrapolatedSBfzSPi_id, init_data_time);
	level->allocatePatchData(d_extrapolatedSBfzSPj_id, init_data_time);
	level->allocatePatchData(d_extrapolatedSBfzSPk_id, init_data_time);
	level->allocatePatchData(d_extrapolatedSBNullSPi_id, init_data_time);
	level->allocatePatchData(d_extrapolatedSBNullSPj_id, init_data_time);
	level->allocatePatchData(d_extrapolatedSBNullSPk_id, init_data_time);
	level->allocatePatchData(d_d_i_rho_mx_my_mz_p_fx_fy_fz_id, init_data_time);
	level->allocatePatchData(d_d_j_rho_mx_my_mz_p_fx_fy_fz_id, init_data_time);
	level->allocatePatchData(d_d_k_rho_mx_my_mz_p_fx_fy_fz_id, init_data_time);
	level->allocatePatchData(d_d_i_Null_id, init_data_time);
	level->allocatePatchData(d_d_j_Null_id, init_data_time);
	level->allocatePatchData(d_d_k_Null_id, init_data_time);
	level->allocatePatchData(d_stalled_1_id, init_data_time);
	level->allocatePatchData(d_stalled_3_id, init_data_time);
	level->allocatePatchData(d_rho_p_id, init_data_time);
	level->allocatePatchData(d_mx_p_id, init_data_time);
	level->allocatePatchData(d_my_p_id, init_data_time);
	level->allocatePatchData(d_mz_p_id, init_data_time);
	level->allocatePatchData(d_Null_p_id, init_data_time);
	level->allocatePatchData(d_mask_id, init_data_time);


	//Mapping the current data for new level.
	if (initial_time || level_number == 0) {
		mapDataOnPatch(init_data_time, initial_time, level_number, level);
	}

	//Fill a finer level with the data of the next coarse level.
	if ((level_number > 0) || old_level) {
		d_mapping_fill->createSchedule(level,old_level,level_number-1,hierarchy,this)->fillData(init_data_time, false);
		correctFOVS(level);
	}

	//Fill a finer level with the data of the next coarse level.
	if (!initial_time && ((level_number > 0) || old_level)) {
		d_fill_new_level->createSchedule(level,old_level,level_number-1,hierarchy,this)->fillData(init_data_time, false);
		postNewLevel(level);
		d_fill_new_level_aux->createSchedule(level,this)->fillData(init_data_time, false);
	}

	//Interphase mapping
	if (initial_time || level_number == 0) {
		interphaseMapping(init_data_time, initial_time, level_number, level, 1);
	}


	//Initialize current data for new level.
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr< hier::Patch > patch = *p_it;
		if (initial_time) {
  		    initializeDataOnPatch(*patch, init_data_time, initial_time);
		}

	}
	//Post-initialization Sync.
    	if (initial_time || level_number == 0) {

		//First synchronization from initialization
		d_bdry_fill_init->createSchedule(level,this)->fillData(init_data_time, false);
		double current_time = init_data_time;
		const double level_ratio = level->getRatioToCoarserLevel().max();
		double simPlat_dt = 0;
		for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
			const std::shared_ptr< hier::Patch >& patch = *p_it;
			double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
			double* FOV_3 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_3_id).get())->getPointer();
			double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
			double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
			double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
			double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
			double* FOV_zLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zLower_id).get())->getPointer();
			double* FOV_zUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zUpper_id).get())->getPointer();
			double* fz = ((pdat::NodeData<double> *) patch->getPatchData(d_fz_id).get())->getPointer();
			double* fy = ((pdat::NodeData<double> *) patch->getPatchData(d_fy_id).get())->getPointer();
			double* fx = ((pdat::NodeData<double> *) patch->getPatchData(d_fx_id).get())->getPointer();
			double* p = ((pdat::NodeData<double> *) patch->getPatchData(d_p_id).get())->getPointer();
			double* rho = ((pdat::NodeData<double> *) patch->getPatchData(d_rho_id).get())->getPointer();

			//Get the dimensions of the patch
			const hier::Index boxfirst = patch->getBox().lower();
			const hier::Index boxlast = patch->getBox().upper();

			//Get delta spaces into an array. dx, dy, dz.
			const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
			const double* dx  = patch_geom->getDx();
			int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
			int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
			int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;
			for (int k = 0; k < klast; k++) {
				for (int j = 0; j < jlast; j++) {
					for (int i = 0; i < ilast; i++) {
						if ((vector(FOV_1, i, j, k) > 0) && ((i + 2 < ilast || !patch->getPatchGeometry()->getTouchesRegularBoundary(0, 1)) && (i - 2 >= 0 || !patch->getPatchGeometry()->getTouchesRegularBoundary(0, 0)) && (j + 2 < jlast || !patch->getPatchGeometry()->getTouchesRegularBoundary(1, 1)) && (j - 2 >= 0 || !patch->getPatchGeometry()->getTouchesRegularBoundary(1, 0)) && (k + 2 < klast || !patch->getPatchGeometry()->getTouchesRegularBoundary(2, 1)) && (k - 2 >= 0 || !patch->getPatchGeometry()->getTouchesRegularBoundary(2, 0)))) {
							vector(fz, i, j, k) = pfz;
							vector(fy, i, j, k) = pfy;
							vector(fx, i, j, k) = pfx;
							vector(p, i, j, k) = (Paux + (pow((vector(rho, i, j, k) / rho0), gamma)) * (rho0 * (c0 * c0)) / gamma) - (rho0 * (c0 * c0)) / gamma;
						}
					}
				}
			}
		}
		for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
			const std::shared_ptr< hier::Patch >& patch = *p_it;
			double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
			double* FOV_3 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_3_id).get())->getPointer();
			double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
			double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
			double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
			double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
			double* FOV_zLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zLower_id).get())->getPointer();
			double* FOV_zUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zUpper_id).get())->getPointer();
			double* Null = ((pdat::NodeData<double> *) patch->getPatchData(d_Null_id).get())->getPointer();
			double* extrapolatedSBrhoSPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBrhoSPi_id).get())->getPointer();
			double* extrapolatedSBrhoSPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBrhoSPj_id).get())->getPointer();
			double* extrapolatedSBrhoSPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBrhoSPk_id).get())->getPointer();
			double* rho = ((pdat::NodeData<double> *) patch->getPatchData(d_rho_id).get())->getPointer();
			double* extrapolatedSBmxSPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmxSPi_id).get())->getPointer();
			double* extrapolatedSBmxSPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmxSPj_id).get())->getPointer();
			double* extrapolatedSBmxSPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmxSPk_id).get())->getPointer();
			double* mx = ((pdat::NodeData<double> *) patch->getPatchData(d_mx_id).get())->getPointer();
			double* extrapolatedSBmySPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmySPi_id).get())->getPointer();
			double* extrapolatedSBmySPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmySPj_id).get())->getPointer();
			double* extrapolatedSBmySPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmySPk_id).get())->getPointer();
			double* my = ((pdat::NodeData<double> *) patch->getPatchData(d_my_id).get())->getPointer();
			double* extrapolatedSBmzSPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmzSPi_id).get())->getPointer();
			double* extrapolatedSBmzSPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmzSPj_id).get())->getPointer();
			double* extrapolatedSBmzSPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmzSPk_id).get())->getPointer();
			double* mz = ((pdat::NodeData<double> *) patch->getPatchData(d_mz_id).get())->getPointer();
			double* extrapolatedSBpSPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBpSPi_id).get())->getPointer();
			double* extrapolatedSBpSPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBpSPj_id).get())->getPointer();
			double* extrapolatedSBpSPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBpSPk_id).get())->getPointer();
			double* p = ((pdat::NodeData<double> *) patch->getPatchData(d_p_id).get())->getPointer();
			double* extrapolatedSBfxSPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBfxSPi_id).get())->getPointer();
			double* extrapolatedSBfxSPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBfxSPj_id).get())->getPointer();
			double* extrapolatedSBfxSPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBfxSPk_id).get())->getPointer();
			double* fx = ((pdat::NodeData<double> *) patch->getPatchData(d_fx_id).get())->getPointer();
			double* extrapolatedSBfySPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBfySPi_id).get())->getPointer();
			double* extrapolatedSBfySPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBfySPj_id).get())->getPointer();
			double* extrapolatedSBfySPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBfySPk_id).get())->getPointer();
			double* fy = ((pdat::NodeData<double> *) patch->getPatchData(d_fy_id).get())->getPointer();
			double* extrapolatedSBfzSPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBfzSPi_id).get())->getPointer();
			double* extrapolatedSBfzSPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBfzSPj_id).get())->getPointer();
			double* extrapolatedSBfzSPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBfzSPk_id).get())->getPointer();
			double* fz = ((pdat::NodeData<double> *) patch->getPatchData(d_fz_id).get())->getPointer();
			double* extrapolatedSBNullSPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBNullSPi_id).get())->getPointer();
			double* extrapolatedSBNullSPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBNullSPj_id).get())->getPointer();
			double* extrapolatedSBNullSPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBNullSPk_id).get())->getPointer();

			//Hard region field distance variables
			double* d_i_rho_mx_my_mz_p_fx_fy_fz = ((pdat::NodeData<double> *) patch->getPatchData(d_d_i_rho_mx_my_mz_p_fx_fy_fz_id).get())->getPointer();
			double* d_j_rho_mx_my_mz_p_fx_fy_fz = ((pdat::NodeData<double> *) patch->getPatchData(d_d_j_rho_mx_my_mz_p_fx_fy_fz_id).get())->getPointer();
			double* d_k_rho_mx_my_mz_p_fx_fy_fz = ((pdat::NodeData<double> *) patch->getPatchData(d_d_k_rho_mx_my_mz_p_fx_fy_fz_id).get())->getPointer();
			double* d_i_Null = ((pdat::NodeData<double> *) patch->getPatchData(d_d_i_Null_id).get())->getPointer();
			double* d_j_Null = ((pdat::NodeData<double> *) patch->getPatchData(d_d_j_Null_id).get())->getPointer();
			double* d_k_Null = ((pdat::NodeData<double> *) patch->getPatchData(d_d_k_Null_id).get())->getPointer();

			//Get the dimensions of the patch
			const hier::Index boxfirst = patch->getBox().lower();
			const hier::Index boxlast = patch->getBox().upper();

			//Get delta spaces into an array. dx, dy, dz.
			const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
			const double* dx  = patch_geom->getDx();
			int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
			int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
			int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;
			for (int k = 0; k < klast; k++) {
				for (int j = 0; j < jlast; j++) {
					for (int i = 0; i < ilast; i++) {
						vector(extrapolatedSBrhoSPi, i, j, k) = vector(rho, i, j, k);
						vector(extrapolatedSBrhoSPj, i, j, k) = vector(rho, i, j, k);
						vector(extrapolatedSBrhoSPk, i, j, k) = vector(rho, i, j, k);
						vector(extrapolatedSBmxSPi, i, j, k) = vector(mx, i, j, k);
						vector(extrapolatedSBmxSPj, i, j, k) = vector(mx, i, j, k);
						vector(extrapolatedSBmxSPk, i, j, k) = vector(mx, i, j, k);
						vector(extrapolatedSBmySPi, i, j, k) = vector(my, i, j, k);
						vector(extrapolatedSBmySPj, i, j, k) = vector(my, i, j, k);
						vector(extrapolatedSBmySPk, i, j, k) = vector(my, i, j, k);
						vector(extrapolatedSBmzSPi, i, j, k) = vector(mz, i, j, k);
						vector(extrapolatedSBmzSPj, i, j, k) = vector(mz, i, j, k);
						vector(extrapolatedSBmzSPk, i, j, k) = vector(mz, i, j, k);
						vector(extrapolatedSBpSPi, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBpSPj, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBpSPk, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBfxSPi, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfxSPj, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfxSPk, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfySPi, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfySPj, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfySPk, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfzSPi, i, j, k) = vector(fz, i, j, k);
						vector(extrapolatedSBfzSPj, i, j, k) = vector(fz, i, j, k);
						vector(extrapolatedSBfzSPk, i, j, k) = vector(fz, i, j, k);
						vector(extrapolatedSBNullSPi, i, j, k) = vector(Null, i, j, k);
						vector(extrapolatedSBNullSPj, i, j, k) = vector(Null, i, j, k);
						vector(extrapolatedSBNullSPk, i, j, k) = vector(Null, i, j, k);
					}
				}
			}
			for (int k = 0; k < klast; k++) {
				for (int j = 0; j < jlast; j++) {
					for (int i = 0; i < ilast; i++) {
						if ((vector(FOV_xLower, i, j, k) > 0) && ((i + 1 < ilast && (vector(FOV_3,i + 1, j, k) > 0)) || (i + 2 < ilast && (vector(FOV_3,i + 2, j, k) > 0)))) {
							vector(Null, i, j, k) = 0.0;
						}
						if ((vector(FOV_xUpper, i, j, k) > 0) && ((i - 1 >= 0 && (vector(FOV_3, i - 1, j, k) > 0)) || (i - 2 >= 0 && (vector(FOV_3, i - 2, j, k) > 0)))) {
							vector(Null, i, j, k) = 0.0;
						}
						if ((vector(FOV_yLower, i, j, k) > 0) && ((j + 1 < jlast && (vector(FOV_3,i, j + 1, k) > 0)) || (j + 2 < jlast && (vector(FOV_3,i, j + 2, k) > 0)))) {
							vector(Null, i, j, k) = 0.0;
						}
						if ((vector(FOV_yUpper, i, j, k) > 0) && ((j - 1 >= 0 && (vector(FOV_3, i, j - 1, k) > 0)) || (j - 2 >= 0 && (vector(FOV_3, i, j - 2, k) > 0)))) {
							vector(Null, i, j, k) = 0.0;
						}
						if ((vector(FOV_zLower, i, j, k) > 0) && ((k + 1 < klast && (vector(FOV_3,i, j, k + 1) > 0)) || (k + 2 < klast && (vector(FOV_3,i, j, k + 2) > 0)))) {
							vector(Null, i, j, k) = 0.0;
						}
						if ((vector(FOV_zUpper, i, j, k) > 0) && ((k - 1 >= 0 && (vector(FOV_3, i, j, k - 1) > 0)) || (k - 2 >= 0 && (vector(FOV_3, i, j, k - 2) > 0)))) {
							vector(Null, i, j, k) = 0.0;
						}
						if (vector(FOV_3, i, j, k) > 0) {
							//Region field extrapolations
							if ((vector(d_i_rho_mx_my_mz_p_fx_fy_fz, i, j, k) != 0 || vector(d_j_rho_mx_my_mz_p_fx_fy_fz, i, j, k) != 0 || vector(d_k_rho_mx_my_mz_p_fx_fy_fz, i, j, k) != 0)) {
								extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBrhoSPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBrhoSPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBrhoSPk, k, rho, FOV_1, dx, ilast, jlast);
								extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmxSPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmxSPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmxSPk, k, mx, FOV_1, dx, ilast, jlast);
								extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmySPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmySPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmySPk, k, my, FOV_1, dx, ilast, jlast);
								extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmzSPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmzSPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmzSPk, k, mz, FOV_1, dx, ilast, jlast);
								extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBpSPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBpSPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBpSPk, k, p, FOV_1, dx, ilast, jlast);
								extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfxSPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfxSPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfxSPk, k, fx, FOV_1, dx, ilast, jlast);
								extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfySPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfySPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfySPk, k, fy, FOV_1, dx, ilast, jlast);
								extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfzSPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfzSPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfzSPk, k, fz, FOV_1, dx, ilast, jlast);
							}
						}
						if ((vector(FOV_xLower, i, j, k) > 0) && ((i + 1 < ilast && (vector(FOV_1,i + 1, j, k) > 0)) || (i + 2 < ilast && (vector(FOV_1,i + 2, j, k) > 0)) || ((i + 1 < ilast && k + 1 < klast && (vector(FOV_1, i + 1, j, k + 1) > 0)) || (i + 1 < ilast && k - 1 >= 0 && (vector(FOV_1, i + 1, j, k - 1) > 0)) || (i + 1 < ilast && k + 2 < klast && (vector(FOV_1, i + 1, j, k + 2) > 0)) || (i + 1 < ilast && k - 2 >= 0 && (vector(FOV_1, i + 1, j, k - 2) > 0)) || (i + 1 < ilast && j + 1 < jlast && (vector(FOV_1, i + 1, j + 1, k) > 0)) || (i + 1 < ilast && j + 1 < jlast && k + 1 < klast && (vector(FOV_1, i + 1, j + 1, k + 1) > 0)) || (i + 1 < ilast && j + 1 < jlast && k - 1 >= 0 && (vector(FOV_1, i + 1, j + 1, k - 1) > 0)) || (i + 1 < ilast && j + 1 < jlast && k + 2 < klast && (vector(FOV_1, i + 1, j + 1, k + 2) > 0)) || (i + 1 < ilast && j + 1 < jlast && k - 2 >= 0 && (vector(FOV_1, i + 1, j + 1, k - 2) > 0)) || (i + 1 < ilast && j - 1 >= 0 && (vector(FOV_1, i + 1, j - 1, k) > 0)) || (i + 1 < ilast && j - 1 >= 0 && k + 1 < klast && (vector(FOV_1, i + 1, j - 1, k + 1) > 0)) || (i + 1 < ilast && j - 1 >= 0 && k - 1 >= 0 && (vector(FOV_1, i + 1, j - 1, k - 1) > 0)) || (i + 1 < ilast && j - 1 >= 0 && k + 2 < klast && (vector(FOV_1, i + 1, j - 1, k + 2) > 0)) || (i + 1 < ilast && j - 1 >= 0 && k - 2 >= 0 && (vector(FOV_1, i + 1, j - 1, k - 2) > 0)) || (i + 1 < ilast && j + 2 < jlast && (vector(FOV_1, i + 1, j + 2, k) > 0)) || (i + 1 < ilast && j + 2 < jlast && k + 1 < klast && (vector(FOV_1, i + 1, j + 2, k + 1) > 0)) || (i + 1 < ilast && j + 2 < jlast && k - 1 >= 0 && (vector(FOV_1, i + 1, j + 2, k - 1) > 0)) || (i + 1 < ilast && j + 2 < jlast && k + 2 < klast && (vector(FOV_1, i + 1, j + 2, k + 2) > 0)) || (i + 1 < ilast && j + 2 < jlast && k - 2 >= 0 && (vector(FOV_1, i + 1, j + 2, k - 2) > 0)) || (i + 1 < ilast && j - 2 >= 0 && (vector(FOV_1, i + 1, j - 2, k) > 0)) || (i + 1 < ilast && j - 2 >= 0 && k + 1 < klast && (vector(FOV_1, i + 1, j - 2, k + 1) > 0)) || (i + 1 < ilast && j - 2 >= 0 && k - 1 >= 0 && (vector(FOV_1, i + 1, j - 2, k - 1) > 0)) || (i + 1 < ilast && j - 2 >= 0 && k + 2 < klast && (vector(FOV_1, i + 1, j - 2, k + 2) > 0)) || (i + 1 < ilast && j - 2 >= 0 && k - 2 >= 0 && (vector(FOV_1, i + 1, j - 2, k - 2) > 0)) || (i + 2 < ilast && k + 1 < klast && (vector(FOV_1, i + 2, j, k + 1) > 0)) || (i + 2 < ilast && k - 1 >= 0 && (vector(FOV_1, i + 2, j, k - 1) > 0)) || (i + 2 < ilast && k + 2 < klast && (vector(FOV_1, i + 2, j, k + 2) > 0)) || (i + 2 < ilast && k - 2 >= 0 && (vector(FOV_1, i + 2, j, k - 2) > 0)) || (i + 2 < ilast && j + 1 < jlast && (vector(FOV_1, i + 2, j + 1, k) > 0)) || (i + 2 < ilast && j + 1 < jlast && k + 1 < klast && (vector(FOV_1, i + 2, j + 1, k + 1) > 0)) || (i + 2 < ilast && j + 1 < jlast && k - 1 >= 0 && (vector(FOV_1, i + 2, j + 1, k - 1) > 0)) || (i + 2 < ilast && j + 1 < jlast && k + 2 < klast && (vector(FOV_1, i + 2, j + 1, k + 2) > 0)) || (i + 2 < ilast && j + 1 < jlast && k - 2 >= 0 && (vector(FOV_1, i + 2, j + 1, k - 2) > 0)) || (i + 2 < ilast && j - 1 >= 0 && (vector(FOV_1, i + 2, j - 1, k) > 0)) || (i + 2 < ilast && j - 1 >= 0 && k + 1 < klast && (vector(FOV_1, i + 2, j - 1, k + 1) > 0)) || (i + 2 < ilast && j - 1 >= 0 && k - 1 >= 0 && (vector(FOV_1, i + 2, j - 1, k - 1) > 0)) || (i + 2 < ilast && j - 1 >= 0 && k + 2 < klast && (vector(FOV_1, i + 2, j - 1, k + 2) > 0)) || (i + 2 < ilast && j - 1 >= 0 && k - 2 >= 0 && (vector(FOV_1, i + 2, j - 1, k - 2) > 0)) || (i + 2 < ilast && j + 2 < jlast && (vector(FOV_1, i + 2, j + 2, k) > 0)) || (i + 2 < ilast && j + 2 < jlast && k + 1 < klast && (vector(FOV_1, i + 2, j + 2, k + 1) > 0)) || (i + 2 < ilast && j + 2 < jlast && k - 1 >= 0 && (vector(FOV_1, i + 2, j + 2, k - 1) > 0)) || (i + 2 < ilast && j + 2 < jlast && k + 2 < klast && (vector(FOV_1, i + 2, j + 2, k + 2) > 0)) || (i + 2 < ilast && j + 2 < jlast && k - 2 >= 0 && (vector(FOV_1, i + 2, j + 2, k - 2) > 0)) || (i + 2 < ilast && j - 2 >= 0 && (vector(FOV_1, i + 2, j - 2, k) > 0)) || (i + 2 < ilast && j - 2 >= 0 && k + 1 < klast && (vector(FOV_1, i + 2, j - 2, k + 1) > 0)) || (i + 2 < ilast && j - 2 >= 0 && k - 1 >= 0 && (vector(FOV_1, i + 2, j - 2, k - 1) > 0)) || (i + 2 < ilast && j - 2 >= 0 && k + 2 < klast && (vector(FOV_1, i + 2, j - 2, k + 2) > 0)) || (i + 2 < ilast && j - 2 >= 0 && k - 2 >= 0 && (vector(FOV_1, i + 2, j - 2, k - 2) > 0))))) {
							//Region field extrapolations
							if ((vector(d_i_rho_mx_my_mz_p_fx_fy_fz, i, j, k) != 0 || vector(d_j_rho_mx_my_mz_p_fx_fy_fz, i, j, k) != 0 || vector(d_k_rho_mx_my_mz_p_fx_fy_fz, i, j, k) != 0)) {
								extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBrhoSPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBrhoSPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBrhoSPk, k, rho, FOV_1, dx, ilast, jlast);
								extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmxSPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmxSPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmxSPk, k, mx, FOV_1, dx, ilast, jlast);
								extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmySPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmySPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmySPk, k, my, FOV_1, dx, ilast, jlast);
								extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmzSPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmzSPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmzSPk, k, mz, FOV_1, dx, ilast, jlast);
								extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBpSPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBpSPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBpSPk, k, p, FOV_1, dx, ilast, jlast);
								extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfxSPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfxSPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfxSPk, k, fx, FOV_1, dx, ilast, jlast);
								extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfySPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfySPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfySPk, k, fy, FOV_1, dx, ilast, jlast);
								extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfzSPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfzSPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfzSPk, k, fz, FOV_1, dx, ilast, jlast);
							}
						}
						if ((vector(FOV_xUpper, i, j, k) > 0) && ((i - 1 >= 0 && (vector(FOV_1, i - 1, j, k) > 0)) || (i - 2 >= 0 && (vector(FOV_1, i - 2, j, k) > 0)) || ((i - 1 >= 0 && k + 1 < klast && (vector(FOV_1, i - 1, j, k + 1) > 0)) || (i - 1 >= 0 && k - 1 >= 0 && (vector(FOV_1, i - 1, j, k - 1) > 0)) || (i - 1 >= 0 && k + 2 < klast && (vector(FOV_1, i - 1, j, k + 2) > 0)) || (i - 1 >= 0 && k - 2 >= 0 && (vector(FOV_1, i - 1, j, k - 2) > 0)) || (i - 1 >= 0 && j + 1 < jlast && (vector(FOV_1, i - 1, j + 1, k) > 0)) || (i - 1 >= 0 && j + 1 < jlast && k + 1 < klast && (vector(FOV_1, i - 1, j + 1, k + 1) > 0)) || (i - 1 >= 0 && j + 1 < jlast && k - 1 >= 0 && (vector(FOV_1, i - 1, j + 1, k - 1) > 0)) || (i - 1 >= 0 && j + 1 < jlast && k + 2 < klast && (vector(FOV_1, i - 1, j + 1, k + 2) > 0)) || (i - 1 >= 0 && j + 1 < jlast && k - 2 >= 0 && (vector(FOV_1, i - 1, j + 1, k - 2) > 0)) || (i - 1 >= 0 && j - 1 >= 0 && (vector(FOV_1, i - 1, j - 1, k) > 0)) || (i - 1 >= 0 && j - 1 >= 0 && k + 1 < klast && (vector(FOV_1, i - 1, j - 1, k + 1) > 0)) || (i - 1 >= 0 && j - 1 >= 0 && k - 1 >= 0 && (vector(FOV_1, i - 1, j - 1, k - 1) > 0)) || (i - 1 >= 0 && j - 1 >= 0 && k + 2 < klast && (vector(FOV_1, i - 1, j - 1, k + 2) > 0)) || (i - 1 >= 0 && j - 1 >= 0 && k - 2 >= 0 && (vector(FOV_1, i - 1, j - 1, k - 2) > 0)) || (i - 1 >= 0 && j + 2 < jlast && (vector(FOV_1, i - 1, j + 2, k) > 0)) || (i - 1 >= 0 && j + 2 < jlast && k + 1 < klast && (vector(FOV_1, i - 1, j + 2, k + 1) > 0)) || (i - 1 >= 0 && j + 2 < jlast && k - 1 >= 0 && (vector(FOV_1, i - 1, j + 2, k - 1) > 0)) || (i - 1 >= 0 && j + 2 < jlast && k + 2 < klast && (vector(FOV_1, i - 1, j + 2, k + 2) > 0)) || (i - 1 >= 0 && j + 2 < jlast && k - 2 >= 0 && (vector(FOV_1, i - 1, j + 2, k - 2) > 0)) || (i - 1 >= 0 && j - 2 >= 0 && (vector(FOV_1, i - 1, j - 2, k) > 0)) || (i - 1 >= 0 && j - 2 >= 0 && k + 1 < klast && (vector(FOV_1, i - 1, j - 2, k + 1) > 0)) || (i - 1 >= 0 && j - 2 >= 0 && k - 1 >= 0 && (vector(FOV_1, i - 1, j - 2, k - 1) > 0)) || (i - 1 >= 0 && j - 2 >= 0 && k + 2 < klast && (vector(FOV_1, i - 1, j - 2, k + 2) > 0)) || (i - 1 >= 0 && j - 2 >= 0 && k - 2 >= 0 && (vector(FOV_1, i - 1, j - 2, k - 2) > 0)) || (i - 2 >= 0 && k + 1 < klast && (vector(FOV_1, i - 2, j, k + 1) > 0)) || (i - 2 >= 0 && k - 1 >= 0 && (vector(FOV_1, i - 2, j, k - 1) > 0)) || (i - 2 >= 0 && k + 2 < klast && (vector(FOV_1, i - 2, j, k + 2) > 0)) || (i - 2 >= 0 && k - 2 >= 0 && (vector(FOV_1, i - 2, j, k - 2) > 0)) || (i - 2 >= 0 && j + 1 < jlast && (vector(FOV_1, i - 2, j + 1, k) > 0)) || (i - 2 >= 0 && j + 1 < jlast && k + 1 < klast && (vector(FOV_1, i - 2, j + 1, k + 1) > 0)) || (i - 2 >= 0 && j + 1 < jlast && k - 1 >= 0 && (vector(FOV_1, i - 2, j + 1, k - 1) > 0)) || (i - 2 >= 0 && j + 1 < jlast && k + 2 < klast && (vector(FOV_1, i - 2, j + 1, k + 2) > 0)) || (i - 2 >= 0 && j + 1 < jlast && k - 2 >= 0 && (vector(FOV_1, i - 2, j + 1, k - 2) > 0)) || (i - 2 >= 0 && j - 1 >= 0 && (vector(FOV_1, i - 2, j - 1, k) > 0)) || (i - 2 >= 0 && j - 1 >= 0 && k + 1 < klast && (vector(FOV_1, i - 2, j - 1, k + 1) > 0)) || (i - 2 >= 0 && j - 1 >= 0 && k - 1 >= 0 && (vector(FOV_1, i - 2, j - 1, k - 1) > 0)) || (i - 2 >= 0 && j - 1 >= 0 && k + 2 < klast && (vector(FOV_1, i - 2, j - 1, k + 2) > 0)) || (i - 2 >= 0 && j - 1 >= 0 && k - 2 >= 0 && (vector(FOV_1, i - 2, j - 1, k - 2) > 0)) || (i - 2 >= 0 && j + 2 < jlast && (vector(FOV_1, i - 2, j + 2, k) > 0)) || (i - 2 >= 0 && j + 2 < jlast && k + 1 < klast && (vector(FOV_1, i - 2, j + 2, k + 1) > 0)) || (i - 2 >= 0 && j + 2 < jlast && k - 1 >= 0 && (vector(FOV_1, i - 2, j + 2, k - 1) > 0)) || (i - 2 >= 0 && j + 2 < jlast && k + 2 < klast && (vector(FOV_1, i - 2, j + 2, k + 2) > 0)) || (i - 2 >= 0 && j + 2 < jlast && k - 2 >= 0 && (vector(FOV_1, i - 2, j + 2, k - 2) > 0)) || (i - 2 >= 0 && j - 2 >= 0 && (vector(FOV_1, i - 2, j - 2, k) > 0)) || (i - 2 >= 0 && j - 2 >= 0 && k + 1 < klast && (vector(FOV_1, i - 2, j - 2, k + 1) > 0)) || (i - 2 >= 0 && j - 2 >= 0 && k - 1 >= 0 && (vector(FOV_1, i - 2, j - 2, k - 1) > 0)) || (i - 2 >= 0 && j - 2 >= 0 && k + 2 < klast && (vector(FOV_1, i - 2, j - 2, k + 2) > 0)) || (i - 2 >= 0 && j - 2 >= 0 && k - 2 >= 0 && (vector(FOV_1, i - 2, j - 2, k - 2) > 0))))) {
							//Region field extrapolations
							if ((vector(d_i_rho_mx_my_mz_p_fx_fy_fz, i, j, k) != 0 || vector(d_j_rho_mx_my_mz_p_fx_fy_fz, i, j, k) != 0 || vector(d_k_rho_mx_my_mz_p_fx_fy_fz, i, j, k) != 0)) {
								extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBrhoSPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBrhoSPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBrhoSPk, k, rho, FOV_1, dx, ilast, jlast);
								extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmxSPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmxSPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmxSPk, k, mx, FOV_1, dx, ilast, jlast);
								extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmySPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmySPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmySPk, k, my, FOV_1, dx, ilast, jlast);
								extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmzSPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmzSPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmzSPk, k, mz, FOV_1, dx, ilast, jlast);
								extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBpSPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBpSPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBpSPk, k, p, FOV_1, dx, ilast, jlast);
								extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfxSPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfxSPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfxSPk, k, fx, FOV_1, dx, ilast, jlast);
								extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfySPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfySPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfySPk, k, fy, FOV_1, dx, ilast, jlast);
								extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfzSPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfzSPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfzSPk, k, fz, FOV_1, dx, ilast, jlast);
							}
						}
						if ((vector(FOV_yLower, i, j, k) > 0) && ((j + 1 < jlast && (vector(FOV_1,i, j + 1, k) > 0)) || (j + 2 < jlast && (vector(FOV_1,i, j + 2, k) > 0)) || ((j + 1 < jlast && k + 1 < klast && (vector(FOV_1, i, j + 1, k + 1) > 0)) || (j + 1 < jlast && k - 1 >= 0 && (vector(FOV_1, i, j + 1, k - 1) > 0)) || (j + 1 < jlast && k + 2 < klast && (vector(FOV_1, i, j + 1, k + 2) > 0)) || (j + 1 < jlast && k - 2 >= 0 && (vector(FOV_1, i, j + 1, k - 2) > 0)) || (j + 2 < jlast && k + 1 < klast && (vector(FOV_1, i, j + 2, k + 1) > 0)) || (j + 2 < jlast && k - 1 >= 0 && (vector(FOV_1, i, j + 2, k - 1) > 0)) || (j + 2 < jlast && k + 2 < klast && (vector(FOV_1, i, j + 2, k + 2) > 0)) || (j + 2 < jlast && k - 2 >= 0 && (vector(FOV_1, i, j + 2, k - 2) > 0)) || (i + 1 < ilast && j + 1 < jlast && (vector(FOV_1, i + 1, j + 1, k) > 0)) || (i + 1 < ilast && j + 1 < jlast && k + 1 < klast && (vector(FOV_1, i + 1, j + 1, k + 1) > 0)) || (i + 1 < ilast && j + 1 < jlast && k - 1 >= 0 && (vector(FOV_1, i + 1, j + 1, k - 1) > 0)) || (i + 1 < ilast && j + 1 < jlast && k + 2 < klast && (vector(FOV_1, i + 1, j + 1, k + 2) > 0)) || (i + 1 < ilast && j + 1 < jlast && k - 2 >= 0 && (vector(FOV_1, i + 1, j + 1, k - 2) > 0)) || (i + 1 < ilast && j + 2 < jlast && (vector(FOV_1, i + 1, j + 2, k) > 0)) || (i + 1 < ilast && j + 2 < jlast && k + 1 < klast && (vector(FOV_1, i + 1, j + 2, k + 1) > 0)) || (i + 1 < ilast && j + 2 < jlast && k - 1 >= 0 && (vector(FOV_1, i + 1, j + 2, k - 1) > 0)) || (i + 1 < ilast && j + 2 < jlast && k + 2 < klast && (vector(FOV_1, i + 1, j + 2, k + 2) > 0)) || (i + 1 < ilast && j + 2 < jlast && k - 2 >= 0 && (vector(FOV_1, i + 1, j + 2, k - 2) > 0)) || (i - 1 >= 0 && j + 1 < jlast && (vector(FOV_1, i - 1, j + 1, k) > 0)) || (i - 1 >= 0 && j + 1 < jlast && k + 1 < klast && (vector(FOV_1, i - 1, j + 1, k + 1) > 0)) || (i - 1 >= 0 && j + 1 < jlast && k - 1 >= 0 && (vector(FOV_1, i - 1, j + 1, k - 1) > 0)) || (i - 1 >= 0 && j + 1 < jlast && k + 2 < klast && (vector(FOV_1, i - 1, j + 1, k + 2) > 0)) || (i - 1 >= 0 && j + 1 < jlast && k - 2 >= 0 && (vector(FOV_1, i - 1, j + 1, k - 2) > 0)) || (i - 1 >= 0 && j + 2 < jlast && (vector(FOV_1, i - 1, j + 2, k) > 0)) || (i - 1 >= 0 && j + 2 < jlast && k + 1 < klast && (vector(FOV_1, i - 1, j + 2, k + 1) > 0)) || (i - 1 >= 0 && j + 2 < jlast && k - 1 >= 0 && (vector(FOV_1, i - 1, j + 2, k - 1) > 0)) || (i - 1 >= 0 && j + 2 < jlast && k + 2 < klast && (vector(FOV_1, i - 1, j + 2, k + 2) > 0)) || (i - 1 >= 0 && j + 2 < jlast && k - 2 >= 0 && (vector(FOV_1, i - 1, j + 2, k - 2) > 0)) || (i + 2 < ilast && j + 1 < jlast && (vector(FOV_1, i + 2, j + 1, k) > 0)) || (i + 2 < ilast && j + 1 < jlast && k + 1 < klast && (vector(FOV_1, i + 2, j + 1, k + 1) > 0)) || (i + 2 < ilast && j + 1 < jlast && k - 1 >= 0 && (vector(FOV_1, i + 2, j + 1, k - 1) > 0)) || (i + 2 < ilast && j + 1 < jlast && k + 2 < klast && (vector(FOV_1, i + 2, j + 1, k + 2) > 0)) || (i + 2 < ilast && j + 1 < jlast && k - 2 >= 0 && (vector(FOV_1, i + 2, j + 1, k - 2) > 0)) || (i + 2 < ilast && j + 2 < jlast && (vector(FOV_1, i + 2, j + 2, k) > 0)) || (i + 2 < ilast && j + 2 < jlast && k + 1 < klast && (vector(FOV_1, i + 2, j + 2, k + 1) > 0)) || (i + 2 < ilast && j + 2 < jlast && k - 1 >= 0 && (vector(FOV_1, i + 2, j + 2, k - 1) > 0)) || (i + 2 < ilast && j + 2 < jlast && k + 2 < klast && (vector(FOV_1, i + 2, j + 2, k + 2) > 0)) || (i + 2 < ilast && j + 2 < jlast && k - 2 >= 0 && (vector(FOV_1, i + 2, j + 2, k - 2) > 0)) || (i - 2 >= 0 && j + 1 < jlast && (vector(FOV_1, i - 2, j + 1, k) > 0)) || (i - 2 >= 0 && j + 1 < jlast && k + 1 < klast && (vector(FOV_1, i - 2, j + 1, k + 1) > 0)) || (i - 2 >= 0 && j + 1 < jlast && k - 1 >= 0 && (vector(FOV_1, i - 2, j + 1, k - 1) > 0)) || (i - 2 >= 0 && j + 1 < jlast && k + 2 < klast && (vector(FOV_1, i - 2, j + 1, k + 2) > 0)) || (i - 2 >= 0 && j + 1 < jlast && k - 2 >= 0 && (vector(FOV_1, i - 2, j + 1, k - 2) > 0)) || (i - 2 >= 0 && j + 2 < jlast && (vector(FOV_1, i - 2, j + 2, k) > 0)) || (i - 2 >= 0 && j + 2 < jlast && k + 1 < klast && (vector(FOV_1, i - 2, j + 2, k + 1) > 0)) || (i - 2 >= 0 && j + 2 < jlast && k - 1 >= 0 && (vector(FOV_1, i - 2, j + 2, k - 1) > 0)) || (i - 2 >= 0 && j + 2 < jlast && k + 2 < klast && (vector(FOV_1, i - 2, j + 2, k + 2) > 0)) || (i - 2 >= 0 && j + 2 < jlast && k - 2 >= 0 && (vector(FOV_1, i - 2, j + 2, k - 2) > 0))))) {
							vector(rho, i, j, k) = 0.0;
							vector(mx, i, j, k) = 0.0;
							vector(my, i, j, k) = 0.0;
							vector(mz, i, j, k) = 0.0;
							vector(p, i, j, k) = 0.0;
							vector(fx, i, j, k) = 0.0;
							vector(fy, i, j, k) = 0.0;
							vector(fz, i, j, k) = 0.0;
						}
						if ((vector(FOV_yUpper, i, j, k) > 0) && ((j - 1 >= 0 && (vector(FOV_1, i, j - 1, k) > 0)) || (j - 2 >= 0 && (vector(FOV_1, i, j - 2, k) > 0)) || ((j - 1 >= 0 && k + 1 < klast && (vector(FOV_1, i, j - 1, k + 1) > 0)) || (j - 1 >= 0 && k - 1 >= 0 && (vector(FOV_1, i, j - 1, k - 1) > 0)) || (j - 1 >= 0 && k + 2 < klast && (vector(FOV_1, i, j - 1, k + 2) > 0)) || (j - 1 >= 0 && k - 2 >= 0 && (vector(FOV_1, i, j - 1, k - 2) > 0)) || (j - 2 >= 0 && k + 1 < klast && (vector(FOV_1, i, j - 2, k + 1) > 0)) || (j - 2 >= 0 && k - 1 >= 0 && (vector(FOV_1, i, j - 2, k - 1) > 0)) || (j - 2 >= 0 && k + 2 < klast && (vector(FOV_1, i, j - 2, k + 2) > 0)) || (j - 2 >= 0 && k - 2 >= 0 && (vector(FOV_1, i, j - 2, k - 2) > 0)) || (i + 1 < ilast && j - 1 >= 0 && (vector(FOV_1, i + 1, j - 1, k) > 0)) || (i + 1 < ilast && j - 1 >= 0 && k + 1 < klast && (vector(FOV_1, i + 1, j - 1, k + 1) > 0)) || (i + 1 < ilast && j - 1 >= 0 && k - 1 >= 0 && (vector(FOV_1, i + 1, j - 1, k - 1) > 0)) || (i + 1 < ilast && j - 1 >= 0 && k + 2 < klast && (vector(FOV_1, i + 1, j - 1, k + 2) > 0)) || (i + 1 < ilast && j - 1 >= 0 && k - 2 >= 0 && (vector(FOV_1, i + 1, j - 1, k - 2) > 0)) || (i + 1 < ilast && j - 2 >= 0 && (vector(FOV_1, i + 1, j - 2, k) > 0)) || (i + 1 < ilast && j - 2 >= 0 && k + 1 < klast && (vector(FOV_1, i + 1, j - 2, k + 1) > 0)) || (i + 1 < ilast && j - 2 >= 0 && k - 1 >= 0 && (vector(FOV_1, i + 1, j - 2, k - 1) > 0)) || (i + 1 < ilast && j - 2 >= 0 && k + 2 < klast && (vector(FOV_1, i + 1, j - 2, k + 2) > 0)) || (i + 1 < ilast && j - 2 >= 0 && k - 2 >= 0 && (vector(FOV_1, i + 1, j - 2, k - 2) > 0)) || (i - 1 >= 0 && j - 1 >= 0 && (vector(FOV_1, i - 1, j - 1, k) > 0)) || (i - 1 >= 0 && j - 1 >= 0 && k + 1 < klast && (vector(FOV_1, i - 1, j - 1, k + 1) > 0)) || (i - 1 >= 0 && j - 1 >= 0 && k - 1 >= 0 && (vector(FOV_1, i - 1, j - 1, k - 1) > 0)) || (i - 1 >= 0 && j - 1 >= 0 && k + 2 < klast && (vector(FOV_1, i - 1, j - 1, k + 2) > 0)) || (i - 1 >= 0 && j - 1 >= 0 && k - 2 >= 0 && (vector(FOV_1, i - 1, j - 1, k - 2) > 0)) || (i - 1 >= 0 && j - 2 >= 0 && (vector(FOV_1, i - 1, j - 2, k) > 0)) || (i - 1 >= 0 && j - 2 >= 0 && k + 1 < klast && (vector(FOV_1, i - 1, j - 2, k + 1) > 0)) || (i - 1 >= 0 && j - 2 >= 0 && k - 1 >= 0 && (vector(FOV_1, i - 1, j - 2, k - 1) > 0)) || (i - 1 >= 0 && j - 2 >= 0 && k + 2 < klast && (vector(FOV_1, i - 1, j - 2, k + 2) > 0)) || (i - 1 >= 0 && j - 2 >= 0 && k - 2 >= 0 && (vector(FOV_1, i - 1, j - 2, k - 2) > 0)) || (i + 2 < ilast && j - 1 >= 0 && (vector(FOV_1, i + 2, j - 1, k) > 0)) || (i + 2 < ilast && j - 1 >= 0 && k + 1 < klast && (vector(FOV_1, i + 2, j - 1, k + 1) > 0)) || (i + 2 < ilast && j - 1 >= 0 && k - 1 >= 0 && (vector(FOV_1, i + 2, j - 1, k - 1) > 0)) || (i + 2 < ilast && j - 1 >= 0 && k + 2 < klast && (vector(FOV_1, i + 2, j - 1, k + 2) > 0)) || (i + 2 < ilast && j - 1 >= 0 && k - 2 >= 0 && (vector(FOV_1, i + 2, j - 1, k - 2) > 0)) || (i + 2 < ilast && j - 2 >= 0 && (vector(FOV_1, i + 2, j - 2, k) > 0)) || (i + 2 < ilast && j - 2 >= 0 && k + 1 < klast && (vector(FOV_1, i + 2, j - 2, k + 1) > 0)) || (i + 2 < ilast && j - 2 >= 0 && k - 1 >= 0 && (vector(FOV_1, i + 2, j - 2, k - 1) > 0)) || (i + 2 < ilast && j - 2 >= 0 && k + 2 < klast && (vector(FOV_1, i + 2, j - 2, k + 2) > 0)) || (i + 2 < ilast && j - 2 >= 0 && k - 2 >= 0 && (vector(FOV_1, i + 2, j - 2, k - 2) > 0)) || (i - 2 >= 0 && j - 1 >= 0 && (vector(FOV_1, i - 2, j - 1, k) > 0)) || (i - 2 >= 0 && j - 1 >= 0 && k + 1 < klast && (vector(FOV_1, i - 2, j - 1, k + 1) > 0)) || (i - 2 >= 0 && j - 1 >= 0 && k - 1 >= 0 && (vector(FOV_1, i - 2, j - 1, k - 1) > 0)) || (i - 2 >= 0 && j - 1 >= 0 && k + 2 < klast && (vector(FOV_1, i - 2, j - 1, k + 2) > 0)) || (i - 2 >= 0 && j - 1 >= 0 && k - 2 >= 0 && (vector(FOV_1, i - 2, j - 1, k - 2) > 0)) || (i - 2 >= 0 && j - 2 >= 0 && (vector(FOV_1, i - 2, j - 2, k) > 0)) || (i - 2 >= 0 && j - 2 >= 0 && k + 1 < klast && (vector(FOV_1, i - 2, j - 2, k + 1) > 0)) || (i - 2 >= 0 && j - 2 >= 0 && k - 1 >= 0 && (vector(FOV_1, i - 2, j - 2, k - 1) > 0)) || (i - 2 >= 0 && j - 2 >= 0 && k + 2 < klast && (vector(FOV_1, i - 2, j - 2, k + 2) > 0)) || (i - 2 >= 0 && j - 2 >= 0 && k - 2 >= 0 && (vector(FOV_1, i - 2, j - 2, k - 2) > 0))))) {
							vector(rho, i, j, k) = 0.0;
							vector(mx, i, j, k) = 0.0;
							vector(my, i, j, k) = 0.0;
							vector(mz, i, j, k) = 0.0;
							vector(p, i, j, k) = 0.0;
							vector(fx, i, j, k) = 0.0;
							vector(fy, i, j, k) = 0.0;
							vector(fz, i, j, k) = 0.0;
						}
						if ((vector(FOV_zLower, i, j, k) > 0) && ((k + 1 < klast && (vector(FOV_1,i, j, k + 1) > 0)) || (k + 2 < klast && (vector(FOV_1,i, j, k + 2) > 0)) || ((j + 1 < jlast && k + 1 < klast && (vector(FOV_1, i, j + 1, k + 1) > 0)) || (j + 1 < jlast && k + 2 < klast && (vector(FOV_1, i, j + 1, k + 2) > 0)) || (j - 1 >= 0 && k + 1 < klast && (vector(FOV_1, i, j - 1, k + 1) > 0)) || (j - 1 >= 0 && k + 2 < klast && (vector(FOV_1, i, j - 1, k + 2) > 0)) || (j + 2 < jlast && k + 1 < klast && (vector(FOV_1, i, j + 2, k + 1) > 0)) || (j + 2 < jlast && k + 2 < klast && (vector(FOV_1, i, j + 2, k + 2) > 0)) || (j - 2 >= 0 && k + 1 < klast && (vector(FOV_1, i, j - 2, k + 1) > 0)) || (j - 2 >= 0 && k + 2 < klast && (vector(FOV_1, i, j - 2, k + 2) > 0)) || (i + 1 < ilast && k + 1 < klast && (vector(FOV_1, i + 1, j, k + 1) > 0)) || (i + 1 < ilast && k + 2 < klast && (vector(FOV_1, i + 1, j, k + 2) > 0)) || (i + 1 < ilast && j + 1 < jlast && k + 1 < klast && (vector(FOV_1, i + 1, j + 1, k + 1) > 0)) || (i + 1 < ilast && j + 1 < jlast && k + 2 < klast && (vector(FOV_1, i + 1, j + 1, k + 2) > 0)) || (i + 1 < ilast && j - 1 >= 0 && k + 1 < klast && (vector(FOV_1, i + 1, j - 1, k + 1) > 0)) || (i + 1 < ilast && j - 1 >= 0 && k + 2 < klast && (vector(FOV_1, i + 1, j - 1, k + 2) > 0)) || (i + 1 < ilast && j + 2 < jlast && k + 1 < klast && (vector(FOV_1, i + 1, j + 2, k + 1) > 0)) || (i + 1 < ilast && j + 2 < jlast && k + 2 < klast && (vector(FOV_1, i + 1, j + 2, k + 2) > 0)) || (i + 1 < ilast && j - 2 >= 0 && k + 1 < klast && (vector(FOV_1, i + 1, j - 2, k + 1) > 0)) || (i + 1 < ilast && j - 2 >= 0 && k + 2 < klast && (vector(FOV_1, i + 1, j - 2, k + 2) > 0)) || (i - 1 >= 0 && k + 1 < klast && (vector(FOV_1, i - 1, j, k + 1) > 0)) || (i - 1 >= 0 && k + 2 < klast && (vector(FOV_1, i - 1, j, k + 2) > 0)) || (i - 1 >= 0 && j + 1 < jlast && k + 1 < klast && (vector(FOV_1, i - 1, j + 1, k + 1) > 0)) || (i - 1 >= 0 && j + 1 < jlast && k + 2 < klast && (vector(FOV_1, i - 1, j + 1, k + 2) > 0)) || (i - 1 >= 0 && j - 1 >= 0 && k + 1 < klast && (vector(FOV_1, i - 1, j - 1, k + 1) > 0)) || (i - 1 >= 0 && j - 1 >= 0 && k + 2 < klast && (vector(FOV_1, i - 1, j - 1, k + 2) > 0)) || (i - 1 >= 0 && j + 2 < jlast && k + 1 < klast && (vector(FOV_1, i - 1, j + 2, k + 1) > 0)) || (i - 1 >= 0 && j + 2 < jlast && k + 2 < klast && (vector(FOV_1, i - 1, j + 2, k + 2) > 0)) || (i - 1 >= 0 && j - 2 >= 0 && k + 1 < klast && (vector(FOV_1, i - 1, j - 2, k + 1) > 0)) || (i - 1 >= 0 && j - 2 >= 0 && k + 2 < klast && (vector(FOV_1, i - 1, j - 2, k + 2) > 0)) || (i + 2 < ilast && k + 1 < klast && (vector(FOV_1, i + 2, j, k + 1) > 0)) || (i + 2 < ilast && k + 2 < klast && (vector(FOV_1, i + 2, j, k + 2) > 0)) || (i + 2 < ilast && j + 1 < jlast && k + 1 < klast && (vector(FOV_1, i + 2, j + 1, k + 1) > 0)) || (i + 2 < ilast && j + 1 < jlast && k + 2 < klast && (vector(FOV_1, i + 2, j + 1, k + 2) > 0)) || (i + 2 < ilast && j - 1 >= 0 && k + 1 < klast && (vector(FOV_1, i + 2, j - 1, k + 1) > 0)) || (i + 2 < ilast && j - 1 >= 0 && k + 2 < klast && (vector(FOV_1, i + 2, j - 1, k + 2) > 0)) || (i + 2 < ilast && j + 2 < jlast && k + 1 < klast && (vector(FOV_1, i + 2, j + 2, k + 1) > 0)) || (i + 2 < ilast && j + 2 < jlast && k + 2 < klast && (vector(FOV_1, i + 2, j + 2, k + 2) > 0)) || (i + 2 < ilast && j - 2 >= 0 && k + 1 < klast && (vector(FOV_1, i + 2, j - 2, k + 1) > 0)) || (i + 2 < ilast && j - 2 >= 0 && k + 2 < klast && (vector(FOV_1, i + 2, j - 2, k + 2) > 0)) || (i - 2 >= 0 && k + 1 < klast && (vector(FOV_1, i - 2, j, k + 1) > 0)) || (i - 2 >= 0 && k + 2 < klast && (vector(FOV_1, i - 2, j, k + 2) > 0)) || (i - 2 >= 0 && j + 1 < jlast && k + 1 < klast && (vector(FOV_1, i - 2, j + 1, k + 1) > 0)) || (i - 2 >= 0 && j + 1 < jlast && k + 2 < klast && (vector(FOV_1, i - 2, j + 1, k + 2) > 0)) || (i - 2 >= 0 && j - 1 >= 0 && k + 1 < klast && (vector(FOV_1, i - 2, j - 1, k + 1) > 0)) || (i - 2 >= 0 && j - 1 >= 0 && k + 2 < klast && (vector(FOV_1, i - 2, j - 1, k + 2) > 0)) || (i - 2 >= 0 && j + 2 < jlast && k + 1 < klast && (vector(FOV_1, i - 2, j + 2, k + 1) > 0)) || (i - 2 >= 0 && j + 2 < jlast && k + 2 < klast && (vector(FOV_1, i - 2, j + 2, k + 2) > 0)) || (i - 2 >= 0 && j - 2 >= 0 && k + 1 < klast && (vector(FOV_1, i - 2, j - 2, k + 1) > 0)) || (i - 2 >= 0 && j - 2 >= 0 && k + 2 < klast && (vector(FOV_1, i - 2, j - 2, k + 2) > 0))))) {
							vector(rho, i, j, k) = 0.0;
							vector(mx, i, j, k) = 0.0;
							vector(my, i, j, k) = 0.0;
							vector(mz, i, j, k) = 0.0;
							vector(p, i, j, k) = 0.0;
							vector(fx, i, j, k) = 0.0;
							vector(fy, i, j, k) = 0.0;
							vector(fz, i, j, k) = 0.0;
						}
						if ((vector(FOV_zUpper, i, j, k) > 0) && ((k - 1 >= 0 && (vector(FOV_1, i, j, k - 1) > 0)) || (k - 2 >= 0 && (vector(FOV_1, i, j, k - 2) > 0)) || ((j + 1 < jlast && k - 1 >= 0 && (vector(FOV_1, i, j + 1, k - 1) > 0)) || (j + 1 < jlast && k - 2 >= 0 && (vector(FOV_1, i, j + 1, k - 2) > 0)) || (j - 1 >= 0 && k - 1 >= 0 && (vector(FOV_1, i, j - 1, k - 1) > 0)) || (j - 1 >= 0 && k - 2 >= 0 && (vector(FOV_1, i, j - 1, k - 2) > 0)) || (j + 2 < jlast && k - 1 >= 0 && (vector(FOV_1, i, j + 2, k - 1) > 0)) || (j + 2 < jlast && k - 2 >= 0 && (vector(FOV_1, i, j + 2, k - 2) > 0)) || (j - 2 >= 0 && k - 1 >= 0 && (vector(FOV_1, i, j - 2, k - 1) > 0)) || (j - 2 >= 0 && k - 2 >= 0 && (vector(FOV_1, i, j - 2, k - 2) > 0)) || (i + 1 < ilast && k - 1 >= 0 && (vector(FOV_1, i + 1, j, k - 1) > 0)) || (i + 1 < ilast && k - 2 >= 0 && (vector(FOV_1, i + 1, j, k - 2) > 0)) || (i + 1 < ilast && j + 1 < jlast && k - 1 >= 0 && (vector(FOV_1, i + 1, j + 1, k - 1) > 0)) || (i + 1 < ilast && j + 1 < jlast && k - 2 >= 0 && (vector(FOV_1, i + 1, j + 1, k - 2) > 0)) || (i + 1 < ilast && j - 1 >= 0 && k - 1 >= 0 && (vector(FOV_1, i + 1, j - 1, k - 1) > 0)) || (i + 1 < ilast && j - 1 >= 0 && k - 2 >= 0 && (vector(FOV_1, i + 1, j - 1, k - 2) > 0)) || (i + 1 < ilast && j + 2 < jlast && k - 1 >= 0 && (vector(FOV_1, i + 1, j + 2, k - 1) > 0)) || (i + 1 < ilast && j + 2 < jlast && k - 2 >= 0 && (vector(FOV_1, i + 1, j + 2, k - 2) > 0)) || (i + 1 < ilast && j - 2 >= 0 && k - 1 >= 0 && (vector(FOV_1, i + 1, j - 2, k - 1) > 0)) || (i + 1 < ilast && j - 2 >= 0 && k - 2 >= 0 && (vector(FOV_1, i + 1, j - 2, k - 2) > 0)) || (i - 1 >= 0 && k - 1 >= 0 && (vector(FOV_1, i - 1, j, k - 1) > 0)) || (i - 1 >= 0 && k - 2 >= 0 && (vector(FOV_1, i - 1, j, k - 2) > 0)) || (i - 1 >= 0 && j + 1 < jlast && k - 1 >= 0 && (vector(FOV_1, i - 1, j + 1, k - 1) > 0)) || (i - 1 >= 0 && j + 1 < jlast && k - 2 >= 0 && (vector(FOV_1, i - 1, j + 1, k - 2) > 0)) || (i - 1 >= 0 && j - 1 >= 0 && k - 1 >= 0 && (vector(FOV_1, i - 1, j - 1, k - 1) > 0)) || (i - 1 >= 0 && j - 1 >= 0 && k - 2 >= 0 && (vector(FOV_1, i - 1, j - 1, k - 2) > 0)) || (i - 1 >= 0 && j + 2 < jlast && k - 1 >= 0 && (vector(FOV_1, i - 1, j + 2, k - 1) > 0)) || (i - 1 >= 0 && j + 2 < jlast && k - 2 >= 0 && (vector(FOV_1, i - 1, j + 2, k - 2) > 0)) || (i - 1 >= 0 && j - 2 >= 0 && k - 1 >= 0 && (vector(FOV_1, i - 1, j - 2, k - 1) > 0)) || (i - 1 >= 0 && j - 2 >= 0 && k - 2 >= 0 && (vector(FOV_1, i - 1, j - 2, k - 2) > 0)) || (i + 2 < ilast && k - 1 >= 0 && (vector(FOV_1, i + 2, j, k - 1) > 0)) || (i + 2 < ilast && k - 2 >= 0 && (vector(FOV_1, i + 2, j, k - 2) > 0)) || (i + 2 < ilast && j + 1 < jlast && k - 1 >= 0 && (vector(FOV_1, i + 2, j + 1, k - 1) > 0)) || (i + 2 < ilast && j + 1 < jlast && k - 2 >= 0 && (vector(FOV_1, i + 2, j + 1, k - 2) > 0)) || (i + 2 < ilast && j - 1 >= 0 && k - 1 >= 0 && (vector(FOV_1, i + 2, j - 1, k - 1) > 0)) || (i + 2 < ilast && j - 1 >= 0 && k - 2 >= 0 && (vector(FOV_1, i + 2, j - 1, k - 2) > 0)) || (i + 2 < ilast && j + 2 < jlast && k - 1 >= 0 && (vector(FOV_1, i + 2, j + 2, k - 1) > 0)) || (i + 2 < ilast && j + 2 < jlast && k - 2 >= 0 && (vector(FOV_1, i + 2, j + 2, k - 2) > 0)) || (i + 2 < ilast && j - 2 >= 0 && k - 1 >= 0 && (vector(FOV_1, i + 2, j - 2, k - 1) > 0)) || (i + 2 < ilast && j - 2 >= 0 && k - 2 >= 0 && (vector(FOV_1, i + 2, j - 2, k - 2) > 0)) || (i - 2 >= 0 && k - 1 >= 0 && (vector(FOV_1, i - 2, j, k - 1) > 0)) || (i - 2 >= 0 && k - 2 >= 0 && (vector(FOV_1, i - 2, j, k - 2) > 0)) || (i - 2 >= 0 && j + 1 < jlast && k - 1 >= 0 && (vector(FOV_1, i - 2, j + 1, k - 1) > 0)) || (i - 2 >= 0 && j + 1 < jlast && k - 2 >= 0 && (vector(FOV_1, i - 2, j + 1, k - 2) > 0)) || (i - 2 >= 0 && j - 1 >= 0 && k - 1 >= 0 && (vector(FOV_1, i - 2, j - 1, k - 1) > 0)) || (i - 2 >= 0 && j - 1 >= 0 && k - 2 >= 0 && (vector(FOV_1, i - 2, j - 1, k - 2) > 0)) || (i - 2 >= 0 && j + 2 < jlast && k - 1 >= 0 && (vector(FOV_1, i - 2, j + 2, k - 1) > 0)) || (i - 2 >= 0 && j + 2 < jlast && k - 2 >= 0 && (vector(FOV_1, i - 2, j + 2, k - 2) > 0)) || (i - 2 >= 0 && j - 2 >= 0 && k - 1 >= 0 && (vector(FOV_1, i - 2, j - 2, k - 1) > 0)) || (i - 2 >= 0 && j - 2 >= 0 && k - 2 >= 0 && (vector(FOV_1, i - 2, j - 2, k - 2) > 0))))) {
							vector(rho, i, j, k) = 0.0;
							vector(mx, i, j, k) = 0.0;
							vector(my, i, j, k) = 0.0;
							vector(mz, i, j, k) = 0.0;
							vector(p, i, j, k) = 0.0;
							vector(fx, i, j, k) = 0.0;
							vector(fy, i, j, k) = 0.0;
							vector(fz, i, j, k) = 0.0;
						}
						if (vector(FOV_1, i, j, k) > 0) {
							//Region field extrapolations
							if ((vector(d_i_Null, i, j, k) != 0 || vector(d_j_Null, i, j, k) != 0 || vector(d_k_Null, i, j, k) != 0)) {
								extrapolate_field(d_i_Null, extrapolatedSBNullSPi, i, d_j_Null, extrapolatedSBNullSPj, j, d_k_Null, extrapolatedSBNullSPk, k, Null, FOV_3, dx, ilast, jlast);
							}
						}
					}
				}
			}
			for (int k = 0; k < klast; k++) {
				for (int j = 0; j < jlast; j++) {
					for (int i = 0; i < ilast; i++) {
						if ((vector(FOV_xLower, i, j, k) > 0) && ((i + 1 < ilast && (vector(FOV_3,i + 1, j, k) > 0)) || (i + 2 < ilast && (vector(FOV_3,i + 2, j, k) > 0)))) {
							vector(extrapolatedSBrhoSPi, i, j, k) = vector(rho, i, j, k);
							vector(extrapolatedSBrhoSPj, i, j, k) = vector(rho, i, j, k);
							vector(extrapolatedSBrhoSPk, i, j, k) = vector(rho, i, j, k);
							vector(extrapolatedSBmxSPi, i, j, k) = vector(mx, i, j, k);
							vector(extrapolatedSBmxSPj, i, j, k) = vector(mx, i, j, k);
							vector(extrapolatedSBmxSPk, i, j, k) = vector(mx, i, j, k);
							vector(extrapolatedSBmySPi, i, j, k) = vector(my, i, j, k);
							vector(extrapolatedSBmySPj, i, j, k) = vector(my, i, j, k);
							vector(extrapolatedSBmySPk, i, j, k) = vector(my, i, j, k);
							vector(extrapolatedSBmzSPi, i, j, k) = vector(mz, i, j, k);
							vector(extrapolatedSBmzSPj, i, j, k) = vector(mz, i, j, k);
							vector(extrapolatedSBmzSPk, i, j, k) = vector(mz, i, j, k);
							vector(extrapolatedSBNullSPi, i, j, k) = vector(Null, i, j, k);
							vector(extrapolatedSBNullSPj, i, j, k) = vector(Null, i, j, k);
							vector(extrapolatedSBNullSPk, i, j, k) = vector(Null, i, j, k);
							vector(extrapolatedSBpSPi, i, j, k) = vector(p, i, j, k);
							vector(extrapolatedSBpSPj, i, j, k) = vector(p, i, j, k);
							vector(extrapolatedSBpSPk, i, j, k) = vector(p, i, j, k);
							vector(extrapolatedSBfxSPi, i, j, k) = vector(fx, i, j, k);
							vector(extrapolatedSBfxSPj, i, j, k) = vector(fx, i, j, k);
							vector(extrapolatedSBfxSPk, i, j, k) = vector(fx, i, j, k);
							vector(extrapolatedSBfySPi, i, j, k) = vector(fy, i, j, k);
							vector(extrapolatedSBfySPj, i, j, k) = vector(fy, i, j, k);
							vector(extrapolatedSBfySPk, i, j, k) = vector(fy, i, j, k);
							vector(extrapolatedSBfzSPi, i, j, k) = vector(fz, i, j, k);
							vector(extrapolatedSBfzSPj, i, j, k) = vector(fz, i, j, k);
							vector(extrapolatedSBfzSPk, i, j, k) = vector(fz, i, j, k);
						}
						if ((vector(FOV_xUpper, i, j, k) > 0) && ((i - 1 >= 0 && (vector(FOV_3, i - 1, j, k) > 0)) || (i - 2 >= 0 && (vector(FOV_3, i - 2, j, k) > 0)))) {
							vector(extrapolatedSBrhoSPi, i, j, k) = vector(rho, i, j, k);
							vector(extrapolatedSBrhoSPj, i, j, k) = vector(rho, i, j, k);
							vector(extrapolatedSBrhoSPk, i, j, k) = vector(rho, i, j, k);
							vector(extrapolatedSBmxSPi, i, j, k) = vector(mx, i, j, k);
							vector(extrapolatedSBmxSPj, i, j, k) = vector(mx, i, j, k);
							vector(extrapolatedSBmxSPk, i, j, k) = vector(mx, i, j, k);
							vector(extrapolatedSBmySPi, i, j, k) = vector(my, i, j, k);
							vector(extrapolatedSBmySPj, i, j, k) = vector(my, i, j, k);
							vector(extrapolatedSBmySPk, i, j, k) = vector(my, i, j, k);
							vector(extrapolatedSBmzSPi, i, j, k) = vector(mz, i, j, k);
							vector(extrapolatedSBmzSPj, i, j, k) = vector(mz, i, j, k);
							vector(extrapolatedSBmzSPk, i, j, k) = vector(mz, i, j, k);
							vector(extrapolatedSBNullSPi, i, j, k) = vector(Null, i, j, k);
							vector(extrapolatedSBNullSPj, i, j, k) = vector(Null, i, j, k);
							vector(extrapolatedSBNullSPk, i, j, k) = vector(Null, i, j, k);
							vector(extrapolatedSBpSPi, i, j, k) = vector(p, i, j, k);
							vector(extrapolatedSBpSPj, i, j, k) = vector(p, i, j, k);
							vector(extrapolatedSBpSPk, i, j, k) = vector(p, i, j, k);
							vector(extrapolatedSBfxSPi, i, j, k) = vector(fx, i, j, k);
							vector(extrapolatedSBfxSPj, i, j, k) = vector(fx, i, j, k);
							vector(extrapolatedSBfxSPk, i, j, k) = vector(fx, i, j, k);
							vector(extrapolatedSBfySPi, i, j, k) = vector(fy, i, j, k);
							vector(extrapolatedSBfySPj, i, j, k) = vector(fy, i, j, k);
							vector(extrapolatedSBfySPk, i, j, k) = vector(fy, i, j, k);
							vector(extrapolatedSBfzSPi, i, j, k) = vector(fz, i, j, k);
							vector(extrapolatedSBfzSPj, i, j, k) = vector(fz, i, j, k);
							vector(extrapolatedSBfzSPk, i, j, k) = vector(fz, i, j, k);
						}
						if ((vector(FOV_yLower, i, j, k) > 0) && ((j + 1 < jlast && (vector(FOV_3,i, j + 1, k) > 0)) || (j + 2 < jlast && (vector(FOV_3,i, j + 2, k) > 0)))) {
							vector(extrapolatedSBrhoSPi, i, j, k) = vector(rho, i, j, k);
							vector(extrapolatedSBrhoSPj, i, j, k) = vector(rho, i, j, k);
							vector(extrapolatedSBrhoSPk, i, j, k) = vector(rho, i, j, k);
							vector(extrapolatedSBmxSPi, i, j, k) = vector(mx, i, j, k);
							vector(extrapolatedSBmxSPj, i, j, k) = vector(mx, i, j, k);
							vector(extrapolatedSBmxSPk, i, j, k) = vector(mx, i, j, k);
							vector(extrapolatedSBmySPi, i, j, k) = vector(my, i, j, k);
							vector(extrapolatedSBmySPj, i, j, k) = vector(my, i, j, k);
							vector(extrapolatedSBmySPk, i, j, k) = vector(my, i, j, k);
							vector(extrapolatedSBmzSPi, i, j, k) = vector(mz, i, j, k);
							vector(extrapolatedSBmzSPj, i, j, k) = vector(mz, i, j, k);
							vector(extrapolatedSBmzSPk, i, j, k) = vector(mz, i, j, k);
							vector(extrapolatedSBNullSPi, i, j, k) = vector(Null, i, j, k);
							vector(extrapolatedSBNullSPj, i, j, k) = vector(Null, i, j, k);
							vector(extrapolatedSBNullSPk, i, j, k) = vector(Null, i, j, k);
							vector(extrapolatedSBpSPi, i, j, k) = vector(p, i, j, k);
							vector(extrapolatedSBpSPj, i, j, k) = vector(p, i, j, k);
							vector(extrapolatedSBpSPk, i, j, k) = vector(p, i, j, k);
							vector(extrapolatedSBfxSPi, i, j, k) = vector(fx, i, j, k);
							vector(extrapolatedSBfxSPj, i, j, k) = vector(fx, i, j, k);
							vector(extrapolatedSBfxSPk, i, j, k) = vector(fx, i, j, k);
							vector(extrapolatedSBfySPi, i, j, k) = vector(fy, i, j, k);
							vector(extrapolatedSBfySPj, i, j, k) = vector(fy, i, j, k);
							vector(extrapolatedSBfySPk, i, j, k) = vector(fy, i, j, k);
							vector(extrapolatedSBfzSPi, i, j, k) = vector(fz, i, j, k);
							vector(extrapolatedSBfzSPj, i, j, k) = vector(fz, i, j, k);
							vector(extrapolatedSBfzSPk, i, j, k) = vector(fz, i, j, k);
						}
						if ((vector(FOV_yUpper, i, j, k) > 0) && ((j - 1 >= 0 && (vector(FOV_3, i, j - 1, k) > 0)) || (j - 2 >= 0 && (vector(FOV_3, i, j - 2, k) > 0)))) {
							vector(extrapolatedSBrhoSPi, i, j, k) = vector(rho, i, j, k);
							vector(extrapolatedSBrhoSPj, i, j, k) = vector(rho, i, j, k);
							vector(extrapolatedSBrhoSPk, i, j, k) = vector(rho, i, j, k);
							vector(extrapolatedSBmxSPi, i, j, k) = vector(mx, i, j, k);
							vector(extrapolatedSBmxSPj, i, j, k) = vector(mx, i, j, k);
							vector(extrapolatedSBmxSPk, i, j, k) = vector(mx, i, j, k);
							vector(extrapolatedSBmySPi, i, j, k) = vector(my, i, j, k);
							vector(extrapolatedSBmySPj, i, j, k) = vector(my, i, j, k);
							vector(extrapolatedSBmySPk, i, j, k) = vector(my, i, j, k);
							vector(extrapolatedSBmzSPi, i, j, k) = vector(mz, i, j, k);
							vector(extrapolatedSBmzSPj, i, j, k) = vector(mz, i, j, k);
							vector(extrapolatedSBmzSPk, i, j, k) = vector(mz, i, j, k);
							vector(extrapolatedSBNullSPi, i, j, k) = vector(Null, i, j, k);
							vector(extrapolatedSBNullSPj, i, j, k) = vector(Null, i, j, k);
							vector(extrapolatedSBNullSPk, i, j, k) = vector(Null, i, j, k);
							vector(extrapolatedSBpSPi, i, j, k) = vector(p, i, j, k);
							vector(extrapolatedSBpSPj, i, j, k) = vector(p, i, j, k);
							vector(extrapolatedSBpSPk, i, j, k) = vector(p, i, j, k);
							vector(extrapolatedSBfxSPi, i, j, k) = vector(fx, i, j, k);
							vector(extrapolatedSBfxSPj, i, j, k) = vector(fx, i, j, k);
							vector(extrapolatedSBfxSPk, i, j, k) = vector(fx, i, j, k);
							vector(extrapolatedSBfySPi, i, j, k) = vector(fy, i, j, k);
							vector(extrapolatedSBfySPj, i, j, k) = vector(fy, i, j, k);
							vector(extrapolatedSBfySPk, i, j, k) = vector(fy, i, j, k);
							vector(extrapolatedSBfzSPi, i, j, k) = vector(fz, i, j, k);
							vector(extrapolatedSBfzSPj, i, j, k) = vector(fz, i, j, k);
							vector(extrapolatedSBfzSPk, i, j, k) = vector(fz, i, j, k);
						}
						if ((vector(FOV_zLower, i, j, k) > 0) && ((k + 1 < klast && (vector(FOV_3,i, j, k + 1) > 0)) || (k + 2 < klast && (vector(FOV_3,i, j, k + 2) > 0)))) {
							vector(extrapolatedSBrhoSPi, i, j, k) = vector(rho, i, j, k);
							vector(extrapolatedSBrhoSPj, i, j, k) = vector(rho, i, j, k);
							vector(extrapolatedSBrhoSPk, i, j, k) = vector(rho, i, j, k);
							vector(extrapolatedSBmxSPi, i, j, k) = vector(mx, i, j, k);
							vector(extrapolatedSBmxSPj, i, j, k) = vector(mx, i, j, k);
							vector(extrapolatedSBmxSPk, i, j, k) = vector(mx, i, j, k);
							vector(extrapolatedSBmySPi, i, j, k) = vector(my, i, j, k);
							vector(extrapolatedSBmySPj, i, j, k) = vector(my, i, j, k);
							vector(extrapolatedSBmySPk, i, j, k) = vector(my, i, j, k);
							vector(extrapolatedSBmzSPi, i, j, k) = vector(mz, i, j, k);
							vector(extrapolatedSBmzSPj, i, j, k) = vector(mz, i, j, k);
							vector(extrapolatedSBmzSPk, i, j, k) = vector(mz, i, j, k);
							vector(extrapolatedSBNullSPi, i, j, k) = vector(Null, i, j, k);
							vector(extrapolatedSBNullSPj, i, j, k) = vector(Null, i, j, k);
							vector(extrapolatedSBNullSPk, i, j, k) = vector(Null, i, j, k);
							vector(extrapolatedSBpSPi, i, j, k) = vector(p, i, j, k);
							vector(extrapolatedSBpSPj, i, j, k) = vector(p, i, j, k);
							vector(extrapolatedSBpSPk, i, j, k) = vector(p, i, j, k);
							vector(extrapolatedSBfxSPi, i, j, k) = vector(fx, i, j, k);
							vector(extrapolatedSBfxSPj, i, j, k) = vector(fx, i, j, k);
							vector(extrapolatedSBfxSPk, i, j, k) = vector(fx, i, j, k);
							vector(extrapolatedSBfySPi, i, j, k) = vector(fy, i, j, k);
							vector(extrapolatedSBfySPj, i, j, k) = vector(fy, i, j, k);
							vector(extrapolatedSBfySPk, i, j, k) = vector(fy, i, j, k);
							vector(extrapolatedSBfzSPi, i, j, k) = vector(fz, i, j, k);
							vector(extrapolatedSBfzSPj, i, j, k) = vector(fz, i, j, k);
							vector(extrapolatedSBfzSPk, i, j, k) = vector(fz, i, j, k);
						}
						if ((vector(FOV_zUpper, i, j, k) > 0) && ((k - 1 >= 0 && (vector(FOV_3, i, j, k - 1) > 0)) || (k - 2 >= 0 && (vector(FOV_3, i, j, k - 2) > 0)))) {
							vector(extrapolatedSBrhoSPi, i, j, k) = vector(rho, i, j, k);
							vector(extrapolatedSBrhoSPj, i, j, k) = vector(rho, i, j, k);
							vector(extrapolatedSBrhoSPk, i, j, k) = vector(rho, i, j, k);
							vector(extrapolatedSBmxSPi, i, j, k) = vector(mx, i, j, k);
							vector(extrapolatedSBmxSPj, i, j, k) = vector(mx, i, j, k);
							vector(extrapolatedSBmxSPk, i, j, k) = vector(mx, i, j, k);
							vector(extrapolatedSBmySPi, i, j, k) = vector(my, i, j, k);
							vector(extrapolatedSBmySPj, i, j, k) = vector(my, i, j, k);
							vector(extrapolatedSBmySPk, i, j, k) = vector(my, i, j, k);
							vector(extrapolatedSBmzSPi, i, j, k) = vector(mz, i, j, k);
							vector(extrapolatedSBmzSPj, i, j, k) = vector(mz, i, j, k);
							vector(extrapolatedSBmzSPk, i, j, k) = vector(mz, i, j, k);
							vector(extrapolatedSBNullSPi, i, j, k) = vector(Null, i, j, k);
							vector(extrapolatedSBNullSPj, i, j, k) = vector(Null, i, j, k);
							vector(extrapolatedSBNullSPk, i, j, k) = vector(Null, i, j, k);
							vector(extrapolatedSBpSPi, i, j, k) = vector(p, i, j, k);
							vector(extrapolatedSBpSPj, i, j, k) = vector(p, i, j, k);
							vector(extrapolatedSBpSPk, i, j, k) = vector(p, i, j, k);
							vector(extrapolatedSBfxSPi, i, j, k) = vector(fx, i, j, k);
							vector(extrapolatedSBfxSPj, i, j, k) = vector(fx, i, j, k);
							vector(extrapolatedSBfxSPk, i, j, k) = vector(fx, i, j, k);
							vector(extrapolatedSBfySPi, i, j, k) = vector(fy, i, j, k);
							vector(extrapolatedSBfySPj, i, j, k) = vector(fy, i, j, k);
							vector(extrapolatedSBfySPk, i, j, k) = vector(fy, i, j, k);
							vector(extrapolatedSBfzSPi, i, j, k) = vector(fz, i, j, k);
							vector(extrapolatedSBfzSPj, i, j, k) = vector(fz, i, j, k);
							vector(extrapolatedSBfzSPk, i, j, k) = vector(fz, i, j, k);
						}
						if ((vector(FOV_xLower, i, j, k) > 0) && ((i + 1 < ilast && (vector(FOV_1,i + 1, j, k) > 0)) || (i + 2 < ilast && (vector(FOV_1,i + 2, j, k) > 0)))) {
							vector(extrapolatedSBNullSPi, i, j, k) = vector(Null, i, j, k);
							vector(extrapolatedSBNullSPj, i, j, k) = vector(Null, i, j, k);
							vector(extrapolatedSBNullSPk, i, j, k) = vector(Null, i, j, k);
						}
						if ((vector(FOV_xUpper, i, j, k) > 0) && ((i - 1 >= 0 && (vector(FOV_1, i - 1, j, k) > 0)) || (i - 2 >= 0 && (vector(FOV_1, i - 2, j, k) > 0)))) {
							vector(extrapolatedSBNullSPi, i, j, k) = vector(Null, i, j, k);
							vector(extrapolatedSBNullSPj, i, j, k) = vector(Null, i, j, k);
							vector(extrapolatedSBNullSPk, i, j, k) = vector(Null, i, j, k);
						}
						if ((vector(FOV_yLower, i, j, k) > 0) && ((j + 1 < jlast && (vector(FOV_1,i, j + 1, k) > 0)) || (j + 2 < jlast && (vector(FOV_1,i, j + 2, k) > 0)))) {
							vector(extrapolatedSBrhoSPi, i, j, k) = vector(rho, i, j, k);
							vector(extrapolatedSBrhoSPj, i, j, k) = vector(rho, i, j, k);
							vector(extrapolatedSBrhoSPk, i, j, k) = vector(rho, i, j, k);
							vector(extrapolatedSBmxSPi, i, j, k) = vector(mx, i, j, k);
							vector(extrapolatedSBmxSPj, i, j, k) = vector(mx, i, j, k);
							vector(extrapolatedSBmxSPk, i, j, k) = vector(mx, i, j, k);
							vector(extrapolatedSBmySPi, i, j, k) = vector(my, i, j, k);
							vector(extrapolatedSBmySPj, i, j, k) = vector(my, i, j, k);
							vector(extrapolatedSBmySPk, i, j, k) = vector(my, i, j, k);
							vector(extrapolatedSBmzSPi, i, j, k) = vector(mz, i, j, k);
							vector(extrapolatedSBmzSPj, i, j, k) = vector(mz, i, j, k);
							vector(extrapolatedSBmzSPk, i, j, k) = vector(mz, i, j, k);
							vector(extrapolatedSBNullSPi, i, j, k) = vector(Null, i, j, k);
							vector(extrapolatedSBNullSPj, i, j, k) = vector(Null, i, j, k);
							vector(extrapolatedSBNullSPk, i, j, k) = vector(Null, i, j, k);
							vector(extrapolatedSBpSPi, i, j, k) = vector(p, i, j, k);
							vector(extrapolatedSBpSPj, i, j, k) = vector(p, i, j, k);
							vector(extrapolatedSBpSPk, i, j, k) = vector(p, i, j, k);
							vector(extrapolatedSBfxSPi, i, j, k) = vector(fx, i, j, k);
							vector(extrapolatedSBfxSPj, i, j, k) = vector(fx, i, j, k);
							vector(extrapolatedSBfxSPk, i, j, k) = vector(fx, i, j, k);
							vector(extrapolatedSBfySPi, i, j, k) = vector(fy, i, j, k);
							vector(extrapolatedSBfySPj, i, j, k) = vector(fy, i, j, k);
							vector(extrapolatedSBfySPk, i, j, k) = vector(fy, i, j, k);
							vector(extrapolatedSBfzSPi, i, j, k) = vector(fz, i, j, k);
							vector(extrapolatedSBfzSPj, i, j, k) = vector(fz, i, j, k);
							vector(extrapolatedSBfzSPk, i, j, k) = vector(fz, i, j, k);
						}
						if ((vector(FOV_yUpper, i, j, k) > 0) && ((j - 1 >= 0 && (vector(FOV_1, i, j - 1, k) > 0)) || (j - 2 >= 0 && (vector(FOV_1, i, j - 2, k) > 0)))) {
							vector(extrapolatedSBrhoSPi, i, j, k) = vector(rho, i, j, k);
							vector(extrapolatedSBrhoSPj, i, j, k) = vector(rho, i, j, k);
							vector(extrapolatedSBrhoSPk, i, j, k) = vector(rho, i, j, k);
							vector(extrapolatedSBmxSPi, i, j, k) = vector(mx, i, j, k);
							vector(extrapolatedSBmxSPj, i, j, k) = vector(mx, i, j, k);
							vector(extrapolatedSBmxSPk, i, j, k) = vector(mx, i, j, k);
							vector(extrapolatedSBmySPi, i, j, k) = vector(my, i, j, k);
							vector(extrapolatedSBmySPj, i, j, k) = vector(my, i, j, k);
							vector(extrapolatedSBmySPk, i, j, k) = vector(my, i, j, k);
							vector(extrapolatedSBmzSPi, i, j, k) = vector(mz, i, j, k);
							vector(extrapolatedSBmzSPj, i, j, k) = vector(mz, i, j, k);
							vector(extrapolatedSBmzSPk, i, j, k) = vector(mz, i, j, k);
							vector(extrapolatedSBNullSPi, i, j, k) = vector(Null, i, j, k);
							vector(extrapolatedSBNullSPj, i, j, k) = vector(Null, i, j, k);
							vector(extrapolatedSBNullSPk, i, j, k) = vector(Null, i, j, k);
							vector(extrapolatedSBpSPi, i, j, k) = vector(p, i, j, k);
							vector(extrapolatedSBpSPj, i, j, k) = vector(p, i, j, k);
							vector(extrapolatedSBpSPk, i, j, k) = vector(p, i, j, k);
							vector(extrapolatedSBfxSPi, i, j, k) = vector(fx, i, j, k);
							vector(extrapolatedSBfxSPj, i, j, k) = vector(fx, i, j, k);
							vector(extrapolatedSBfxSPk, i, j, k) = vector(fx, i, j, k);
							vector(extrapolatedSBfySPi, i, j, k) = vector(fy, i, j, k);
							vector(extrapolatedSBfySPj, i, j, k) = vector(fy, i, j, k);
							vector(extrapolatedSBfySPk, i, j, k) = vector(fy, i, j, k);
							vector(extrapolatedSBfzSPi, i, j, k) = vector(fz, i, j, k);
							vector(extrapolatedSBfzSPj, i, j, k) = vector(fz, i, j, k);
							vector(extrapolatedSBfzSPk, i, j, k) = vector(fz, i, j, k);
						}
						if ((vector(FOV_zLower, i, j, k) > 0) && ((k + 1 < klast && (vector(FOV_1,i, j, k + 1) > 0)) || (k + 2 < klast && (vector(FOV_1,i, j, k + 2) > 0)))) {
							vector(extrapolatedSBrhoSPi, i, j, k) = vector(rho, i, j, k);
							vector(extrapolatedSBrhoSPj, i, j, k) = vector(rho, i, j, k);
							vector(extrapolatedSBrhoSPk, i, j, k) = vector(rho, i, j, k);
							vector(extrapolatedSBmxSPi, i, j, k) = vector(mx, i, j, k);
							vector(extrapolatedSBmxSPj, i, j, k) = vector(mx, i, j, k);
							vector(extrapolatedSBmxSPk, i, j, k) = vector(mx, i, j, k);
							vector(extrapolatedSBmySPi, i, j, k) = vector(my, i, j, k);
							vector(extrapolatedSBmySPj, i, j, k) = vector(my, i, j, k);
							vector(extrapolatedSBmySPk, i, j, k) = vector(my, i, j, k);
							vector(extrapolatedSBmzSPi, i, j, k) = vector(mz, i, j, k);
							vector(extrapolatedSBmzSPj, i, j, k) = vector(mz, i, j, k);
							vector(extrapolatedSBmzSPk, i, j, k) = vector(mz, i, j, k);
							vector(extrapolatedSBNullSPi, i, j, k) = vector(Null, i, j, k);
							vector(extrapolatedSBNullSPj, i, j, k) = vector(Null, i, j, k);
							vector(extrapolatedSBNullSPk, i, j, k) = vector(Null, i, j, k);
							vector(extrapolatedSBpSPi, i, j, k) = vector(p, i, j, k);
							vector(extrapolatedSBpSPj, i, j, k) = vector(p, i, j, k);
							vector(extrapolatedSBpSPk, i, j, k) = vector(p, i, j, k);
							vector(extrapolatedSBfxSPi, i, j, k) = vector(fx, i, j, k);
							vector(extrapolatedSBfxSPj, i, j, k) = vector(fx, i, j, k);
							vector(extrapolatedSBfxSPk, i, j, k) = vector(fx, i, j, k);
							vector(extrapolatedSBfySPi, i, j, k) = vector(fy, i, j, k);
							vector(extrapolatedSBfySPj, i, j, k) = vector(fy, i, j, k);
							vector(extrapolatedSBfySPk, i, j, k) = vector(fy, i, j, k);
							vector(extrapolatedSBfzSPi, i, j, k) = vector(fz, i, j, k);
							vector(extrapolatedSBfzSPj, i, j, k) = vector(fz, i, j, k);
							vector(extrapolatedSBfzSPk, i, j, k) = vector(fz, i, j, k);
						}
						if ((vector(FOV_zUpper, i, j, k) > 0) && ((k - 1 >= 0 && (vector(FOV_1, i, j, k - 1) > 0)) || (k - 2 >= 0 && (vector(FOV_1, i, j, k - 2) > 0)))) {
							vector(extrapolatedSBrhoSPi, i, j, k) = vector(rho, i, j, k);
							vector(extrapolatedSBrhoSPj, i, j, k) = vector(rho, i, j, k);
							vector(extrapolatedSBrhoSPk, i, j, k) = vector(rho, i, j, k);
							vector(extrapolatedSBmxSPi, i, j, k) = vector(mx, i, j, k);
							vector(extrapolatedSBmxSPj, i, j, k) = vector(mx, i, j, k);
							vector(extrapolatedSBmxSPk, i, j, k) = vector(mx, i, j, k);
							vector(extrapolatedSBmySPi, i, j, k) = vector(my, i, j, k);
							vector(extrapolatedSBmySPj, i, j, k) = vector(my, i, j, k);
							vector(extrapolatedSBmySPk, i, j, k) = vector(my, i, j, k);
							vector(extrapolatedSBmzSPi, i, j, k) = vector(mz, i, j, k);
							vector(extrapolatedSBmzSPj, i, j, k) = vector(mz, i, j, k);
							vector(extrapolatedSBmzSPk, i, j, k) = vector(mz, i, j, k);
							vector(extrapolatedSBNullSPi, i, j, k) = vector(Null, i, j, k);
							vector(extrapolatedSBNullSPj, i, j, k) = vector(Null, i, j, k);
							vector(extrapolatedSBNullSPk, i, j, k) = vector(Null, i, j, k);
							vector(extrapolatedSBpSPi, i, j, k) = vector(p, i, j, k);
							vector(extrapolatedSBpSPj, i, j, k) = vector(p, i, j, k);
							vector(extrapolatedSBpSPk, i, j, k) = vector(p, i, j, k);
							vector(extrapolatedSBfxSPi, i, j, k) = vector(fx, i, j, k);
							vector(extrapolatedSBfxSPj, i, j, k) = vector(fx, i, j, k);
							vector(extrapolatedSBfxSPk, i, j, k) = vector(fx, i, j, k);
							vector(extrapolatedSBfySPi, i, j, k) = vector(fy, i, j, k);
							vector(extrapolatedSBfySPj, i, j, k) = vector(fy, i, j, k);
							vector(extrapolatedSBfySPk, i, j, k) = vector(fy, i, j, k);
							vector(extrapolatedSBfzSPi, i, j, k) = vector(fz, i, j, k);
							vector(extrapolatedSBfzSPj, i, j, k) = vector(fz, i, j, k);
							vector(extrapolatedSBfzSPk, i, j, k) = vector(fz, i, j, k);
						}
					}
				}
			}
		}
		//Last synchronization from initialization
		d_bdry_fill_init->createSchedule(level,this)->fillData(init_data_time, false);

    	}

    cout<<"Level "<<level_number<<" initialized"<<endl;
    tbox::MemoryUtilities::printMemoryInfo(cout);
}



void Problem::initializeLevelIntegrator(
   const std::shared_ptr<mesh::GriddingAlgorithmStrategy>& gridding_alg)
{
}

double Problem::getLevelDt(
   const std::shared_ptr<hier::PatchLevel>& level,
   const double dt_time,
   const bool initial_time)
{
  
   TBOX_ASSERT(level);

   if (level->getLevelNumber() == 0) return initial_dt;

    double dt = initial_dt;
    const hier::IntVector ratio = level->getRatioToLevelZero();
    double local_dt = dt;
    for (int i = 0; i < 2; i++) {
        if (local_dt > dt / ratio[i]) {
            local_dt = dt / ratio[i];
        }
    }
    return local_dt;
}

double Problem::getMaxFinerLevelDt(
   const int finer_level_number,
   const double coarse_dt,
   const hier::IntVector& ratio)
{
   NULL_USE(finer_level_number);

   TBOX_ASSERT(ratio.min() > 0);

   return coarse_dt / double(ratio.max());
}

void Problem::standardLevelSynchronization(
   const std::shared_ptr<hier::PatchHierarchy>& hierarchy,
   const int coarsest_level,
   const int finest_level,
   const double sync_time,
   const std::vector<double>& old_times)
{

}

void Problem::synchronizeNewLevels(
   const std::shared_ptr<hier::PatchHierarchy>& hierarchy,
   const int coarsest_level,
   const int finest_level,
   const double sync_time,
   const bool initial_time)
{

    //Not needed, but not absolutely sure
    /*for (int fine_ln = finest_level; fine_ln > coarsest_level; --fine_ln) {
        const int coarse_ln = fine_ln - 1;
        std::shared_ptr<hier::PatchLevel> fine_level(hierarchy->getPatchLevel(fine_ln));
        d_bdry_fill_init->createSchedule(fine_level, coarse_ln, hierarchy, this)->fillData(sync_time, true);
    }*/
}

void Problem::resetTimeDependentData(
   const std::shared_ptr<hier::PatchLevel>& level,
   const double new_time,
   const bool can_be_refined)
{
   TBOX_ASSERT(level);
   level->setTime(new_time);
}

void Problem::resetDataToPreadvanceState(
   const std::shared_ptr<hier::PatchLevel>& level)
{
    //cout<<"resetDataToPreadvanceState"<<endl;
}

/*
 * Map data on a patch. This mapping is done only at the begining of the simulation.
 */
void Problem::mapDataOnPatch(const double time, const bool initial_time, const int ln, const std::shared_ptr< hier::PatchLevel >& level)
{
	(void) time;
	const tbox::SAMRAI_MPI& mpi(tbox::SAMRAI_MPI::getSAMRAIWorld());
   	if (initial_time || ln == 0) {

		// Mapping		
		int i, iterm, previousMapi, iWallAcc;
		bool interiorMapi;
		double iMapStart, iMapEnd;
		int j, jterm, previousMapj, jWallAcc;
		bool interiorMapj;
		double jMapStart, jMapEnd;
		int k, kterm, previousMapk, kWallAcc;
		bool interiorMapk;
		double kMapStart, kMapEnd;
		int minBlock[3], maxBlock[3], unionsI, facePointI, ie1, ie2, ie3, proc, pcounter, working, finished, pred;
		double maxDistance, e1, e2, e3;
		bool done, modif, workingArray[mpi.getSize()], finishedArray[mpi.getSize()], workingGlobal, finishedGlobal, workingPatchArray[level->getLocalNumberOfPatches()], finishedPatchArray[level->getLocalNumberOfPatches()], workingPatchGlobal, finishedPatchGlobal;
		int nodes = mpi.getSize();
		int patches = level->getLocalNumberOfPatches();

		double SQRT3INV = 1.0/sqrt(3.0);

		if (ln == 0) {
			//FOV initialization
			for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
				const std::shared_ptr< hier::Patch >& patch = *p_it;

				//Get the dimensions of the patch
				hier::Box pbox = patch->getBox();
				double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
				double* FOV_3 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_3_id).get())->getPointer();
				double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
				double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
				double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
				double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
				double* FOV_zLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zLower_id).get())->getPointer();
				double* FOV_zUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zUpper_id).get())->getPointer();
				const hier::Index boxfirst = patch->getBox().lower();
				const hier::Index boxlast  = patch->getBox().upper();

				//Get delta spaces into an array. dx, dy, dz.
				const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
				const double* dx  = patch_geom->getDx();

				int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
				int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
				int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;

				for (i = 0; i < ilast; i++) {
					for (j = 0; j < jlast; j++) {
						for (k = 0; k < klast; k++) {
							vector(FOV_1, i, j, k) = 0;
							vector(FOV_3, i, j, k) = 0;
							vector(FOV_xLower, i, j, k) = 0;
							vector(FOV_xUpper, i, j, k) = 0;
							vector(FOV_yLower, i, j, k) = 0;
							vector(FOV_yUpper, i, j, k) = 0;
							vector(FOV_zLower, i, j, k) = 0;
							vector(FOV_zUpper, i, j, k) = 0;
						}
					}
				}
			}
			d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);

			//Region: WallI
			for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
				const std::shared_ptr< hier::Patch >& patch = *p_it;

				//Get the dimensions of the patch
				hier::Box pbox = patch->getBox();
				double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
				double* FOV_3 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_3_id).get())->getPointer();
				double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
				double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
				double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
				double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
				double* FOV_zLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zLower_id).get())->getPointer();
				double* FOV_zUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zUpper_id).get())->getPointer();
				double* interior_i = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_i_id).get())->getPointer();
				double* interior_j = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_j_id).get())->getPointer();
				double* interior_k = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_k_id).get())->getPointer();
				double* interior = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_regridding_value_id).get())->getPointer();
				int* nonSync = ((pdat::NodeData<int> *) patch->getPatchData(d_nonSync_regridding_tag_id).get())->getPointer();
				const hier::Index boxfirst = patch->getBox().lower();
				const hier::Index boxlast  = patch->getBox().upper();

				//Get delta spaces into an array. dx, dy, dz.
				const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
				const double* dx  = patch_geom->getDx();

				int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
				int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
				int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;

				iMapStart = 0;
				iMapEnd = d_grid_geometry->getPhysicalDomain().front().numberCells()[0];
				jMapStart = 0;
				jMapEnd = d_grid_geometry->getPhysicalDomain().front().numberCells()[1];
				kMapStart = 0;
				kMapEnd = d_grid_geometry->getPhysicalDomain().front().numberCells()[2];
				for (i = iMapStart; i <= iMapEnd; i++) {
					for (j = jMapStart; j <= jMapEnd; j++) {
						for (k = kMapStart; k <= kMapEnd; k++) {
							if (i >= boxfirst(0) - d_ghost_width && i <= boxlast(0) + d_ghost_width && j >= boxfirst(1) - d_ghost_width && j <= boxlast(1) + d_ghost_width && k >= boxfirst(2) - d_ghost_width && k <= boxlast(2) + d_ghost_width) {
								vector(FOV_3, i - boxfirst(0) + d_ghost_width, j - boxfirst(1) + d_ghost_width, k - boxfirst(2) + d_ghost_width) = 100;
								vector(FOV_1, i - boxfirst(0) + d_ghost_width, j - boxfirst(1) + d_ghost_width, k - boxfirst(2) + d_ghost_width) = 0;
								vector(FOV_xLower, i - boxfirst(0) + d_ghost_width, j - boxfirst(1) + d_ghost_width, k - boxfirst(2) + d_ghost_width) = 0;
								vector(FOV_xUpper, i - boxfirst(0) + d_ghost_width, j - boxfirst(1) + d_ghost_width, k - boxfirst(2) + d_ghost_width) = 0;
								vector(FOV_yLower, i - boxfirst(0) + d_ghost_width, j - boxfirst(1) + d_ghost_width, k - boxfirst(2) + d_ghost_width) = 0;
								vector(FOV_yUpper, i - boxfirst(0) + d_ghost_width, j - boxfirst(1) + d_ghost_width, k - boxfirst(2) + d_ghost_width) = 0;
								vector(FOV_zLower, i - boxfirst(0) + d_ghost_width, j - boxfirst(1) + d_ghost_width, k - boxfirst(2) + d_ghost_width) = 0;
								vector(FOV_zUpper, i - boxfirst(0) + d_ghost_width, j - boxfirst(1) + d_ghost_width, k - boxfirst(2) + d_ghost_width) = 0;
							}
						}
					}
				}
				//Check stencil
				for (k = 0; k < klast; k++) {
					for (j = 0; j < jlast; j++) {
						for (i = 0; i < ilast; i++) {
							vector(interior_i, i, j, k) = 0;
							vector(interior_j, i, j, k) = 0;
							vector(interior_k, i, j, k) = 0;
						}
					}
				}
				for (k = 0; k < klast; k++) {
					for (j = 0; j < jlast; j++) {
						for (i = 0; i < ilast; i++) {
							if (vector(FOV_3, i, j, k) > 0) {
								setStencilLimits(patch, i, j, k, d_FOV_3_id);
							}
						}
					}
				}
			}
			d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);
			for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
				const std::shared_ptr< hier::Patch >& patch = *p_it;

				//Get the dimensions of the patch
				hier::Box pbox = patch->getBox();
				double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
				double* FOV_3 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_3_id).get())->getPointer();
				double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
				double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
				double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
				double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
				double* FOV_zLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zLower_id).get())->getPointer();
				double* FOV_zUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zUpper_id).get())->getPointer();
				double* interior_i = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_i_id).get())->getPointer();
				double* interior_j = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_j_id).get())->getPointer();
				double* interior_k = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_k_id).get())->getPointer();
				double* interior = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_regridding_value_id).get())->getPointer();
				int* nonSync = ((pdat::NodeData<int> *) patch->getPatchData(d_nonSync_regridding_tag_id).get())->getPointer();
				const hier::Index boxfirst = patch->getBox().lower();
				const hier::Index boxlast  = patch->getBox().upper();

				//Get delta spaces into an array. dx, dy, dz.
				const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
				const double* dx  = patch_geom->getDx();

				int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
				int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
				int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;

				for (k = 0; k < klast; k++) {
					for (j = 0; j < jlast; j++) {
						for (i = 0; i < ilast; i++) {
							if ((abs(vector(interior_i, i, j, k)) > 1 || abs(vector(interior_j, i, j, k)) > 1 || abs(vector(interior_k, i, j, k)) > 1) && (i < d_ghost_width || i >= ilast - d_ghost_width || j < d_ghost_width || j >= jlast - d_ghost_width || k < d_ghost_width || k >= klast - d_ghost_width)) {
								checkStencil(patch, i, j, k, d_FOV_3_id);
							}
						}
					}
				}
				for (k = 0; k < klast; k++) {
					for (j = 0; j < jlast; j++) {
						for (i = 0; i < ilast; i++) {
							if (vector(interior_i, i, j, k) != 0 || vector(interior_j, i, j, k) != 0 || vector(interior_k, i, j, k) != 0) {
								vector(FOV_3, i, j, k) = 100;
								vector(FOV_1, i, j, k) = 0;
								vector(FOV_xLower, i, j, k) = 0;
								vector(FOV_xUpper, i, j, k) = 0;
								vector(FOV_yLower, i, j, k) = 0;
								vector(FOV_yUpper, i, j, k) = 0;
								vector(FOV_zLower, i, j, k) = 0;
								vector(FOV_zUpper, i, j, k) = 0;
							}
						}
					}
				}
			}
			d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);

			//Region: PlaneI
			for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
				const std::shared_ptr< hier::Patch >& patch = *p_it;

				//Get the dimensions of the patch
				hier::Box pbox = patch->getBox();
				double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
				double* FOV_3 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_3_id).get())->getPointer();
				double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
				double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
				double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
				double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
				double* FOV_zLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zLower_id).get())->getPointer();
				double* FOV_zUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zUpper_id).get())->getPointer();
				double* interior_i = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_i_id).get())->getPointer();
				double* interior_j = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_j_id).get())->getPointer();
				double* interior_k = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_k_id).get())->getPointer();
				double* interior = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_regridding_value_id).get())->getPointer();
				int* nonSync = ((pdat::NodeData<int> *) patch->getPatchData(d_nonSync_regridding_tag_id).get())->getPointer();
				const hier::Index boxfirst = patch->getBox().lower();
				const hier::Index boxlast  = patch->getBox().upper();

				//Get delta spaces into an array. dx, dy, dz.
				const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
				const double* dx  = patch_geom->getDx();

				int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
				int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
				int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;

				//Fill the faces
				for (unionsI = 0; unionsI < UNIONS_Plane; unionsI++) {
					for (facePointI = 0; facePointI < DIMENSIONS; facePointI++) {
						iterm = round((PlanePoints[PlaneUnions[unionsI][facePointI]][0] - d_grid_geometry->getXLower()[0]) / dx[0]);
						jterm = round((PlanePoints[PlaneUnions[unionsI][facePointI]][1] - d_grid_geometry->getXLower()[1]) / dx[1]);
						kterm = round((PlanePoints[PlaneUnions[unionsI][facePointI]][2] - d_grid_geometry->getXLower()[2]) / dx[2]);
						//Take maximum and minimum coordinates of the face
						if (facePointI == 0) {
							minBlock[0] = iterm;
							maxBlock[0] = iterm;
							minBlock[1] = jterm;
							maxBlock[1] = jterm;
							minBlock[2] = kterm;
							maxBlock[2] = kterm;
						} else {
							if (iterm < minBlock[0]) {
								minBlock[0] = iterm;
							}
							if (iterm > maxBlock[0]) {
								maxBlock[0] = iterm;
							}
							if (jterm < minBlock[1]) {
								minBlock[1] = jterm;
							}
							if (jterm > maxBlock[1]) {
								maxBlock[1] = jterm;
							}
							if (kterm < minBlock[2]) {
								minBlock[2] = kterm;
							}
							if (kterm > maxBlock[2]) {
								maxBlock[2] = kterm;
							}
						}
					}
					if (minBlock[0] <= boxlast(0) + 1 + d_ghost_width && maxBlock[0] >= boxfirst(0) - d_ghost_width && minBlock[1] <= boxlast(1) + 1 + d_ghost_width && maxBlock[1] >= boxfirst(1) - d_ghost_width && minBlock[2] <= boxlast(2) + 1 + d_ghost_width && maxBlock[2] >= boxfirst(2) - d_ghost_width) {
						maxDistance = MAX(abs(minBlock[2] - maxBlock[2]),MAX(abs(minBlock[1] - maxBlock[1]),abs(minBlock[0] - maxBlock[0])));
						for (ie3 = 0; ie3 < 3 * maxDistance; ie3++) {
							e3 = ie3 / (3 * maxDistance);
							for (ie2 = 0; ie2 < 3 * maxDistance; ie2++) {
								e2 = ie2 / (3 * maxDistance);
								for (ie1 = 0; ie1 < 3 * maxDistance; ie1++) {
									e1 = ie1 / (3 * maxDistance);
									if (abs(ie1 + ie2 + ie3 - 3 * maxDistance) <= SQRT3INV) {
										iterm = round(((PlanePoints[PlaneUnions[unionsI][0]][0] - d_grid_geometry->getXLower()[0]) / dx[0]) * e1 + ((PlanePoints[PlaneUnions[unionsI][1]][0] - d_grid_geometry->getXLower()[0]) / dx[0]) * e2 + ((PlanePoints[PlaneUnions[unionsI][2]][0] - d_grid_geometry->getXLower()[0]) / dx[0]) * e3);
										jterm = round(((PlanePoints[PlaneUnions[unionsI][0]][1] - d_grid_geometry->getXLower()[1]) / dx[1]) * e1 + ((PlanePoints[PlaneUnions[unionsI][1]][1] - d_grid_geometry->getXLower()[1]) / dx[1]) * e2 + ((PlanePoints[PlaneUnions[unionsI][2]][1] - d_grid_geometry->getXLower()[1]) / dx[1]) * e3);
										kterm = round(((PlanePoints[PlaneUnions[unionsI][0]][2] - d_grid_geometry->getXLower()[2]) / dx[2]) * e1 + ((PlanePoints[PlaneUnions[unionsI][1]][2] - d_grid_geometry->getXLower()[2]) / dx[2]) * e2 + ((PlanePoints[PlaneUnions[unionsI][2]][2] - d_grid_geometry->getXLower()[2]) / dx[2]) * e3);
										if (iterm >= boxfirst(0) - d_ghost_width && iterm <= boxlast(0) + d_ghost_width && jterm >= boxfirst(1) - d_ghost_width && jterm <= boxlast(1) + d_ghost_width && kterm >= boxfirst(2) - d_ghost_width && kterm <= boxlast(2) + d_ghost_width) {
											int indexi = iterm - boxfirst(0) + d_ghost_width;
											int indexj = jterm - boxfirst(1) + d_ghost_width;
											int indexk = kterm - boxfirst(2) + d_ghost_width;
											if (patch->getPatchGeometry()->getTouchesRegularBoundary (0, 1) && i - boxfirst(0) + 1 + d_ghost_width == ilast - d_ghost_width) {
												indexi--;
											}
											if (patch->getPatchGeometry()->getTouchesRegularBoundary (1, 1) && j - boxfirst(1) + 1 + d_ghost_width == jlast - d_ghost_width) {
												indexj--;
											}
											if (patch->getPatchGeometry()->getTouchesRegularBoundary (2, 1) && k - boxfirst(2) + 1 + d_ghost_width == klast - d_ghost_width) {
												indexk--;
											}
											vector(FOV_1, indexi, indexj, indexk) = 100;
											vector(FOV_3, indexi, indexj, indexk) = 0;
											vector(FOV_xLower, indexi, indexj, indexk) = 0;
											vector(FOV_xUpper, indexi, indexj, indexk) = 0;
											vector(FOV_yLower, indexi, indexj, indexk) = 0;
											vector(FOV_yUpper, indexi, indexj, indexk) = 0;
											vector(FOV_zLower, indexi, indexj, indexk) = 0;
											vector(FOV_zUpper, indexi, indexj, indexk) = 0;
										}
									}
								}
							}
						}
					}
				}
			}
			d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);

			//Region: PlaneI
			//Fill the interior of the region
			//Initialize the variables and set the walls as exterior
			for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
				const std::shared_ptr< hier::Patch >& patch = *p_it;

				//Get the dimensions of the patch
				hier::Box pbox = patch->getBox();
				double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
				double* FOV_3 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_3_id).get())->getPointer();
				double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
				double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
				double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
				double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
				double* FOV_zLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zLower_id).get())->getPointer();
				double* FOV_zUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zUpper_id).get())->getPointer();
				double* interior_i = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_i_id).get())->getPointer();
				double* interior_j = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_j_id).get())->getPointer();
				double* interior_k = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_k_id).get())->getPointer();
				double* interior = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_regridding_value_id).get())->getPointer();
				int* nonSync = ((pdat::NodeData<int> *) patch->getPatchData(d_nonSync_regridding_tag_id).get())->getPointer();
				const hier::Index boxfirst = patch->getBox().lower();
				const hier::Index boxlast  = patch->getBox().upper();

				//Get delta spaces into an array. dx, dy, dz.
				const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
				const double* dx  = patch_geom->getDx();

				int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
				int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
				int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;

				for (k = 0; k < klast; k++) {
					for (j = 0; j < jlast; j++) {
						for (i = 0; i < ilast; i++) {
							//If wall, then it is considered exterior
							if (vector(FOV_1, i, j, k) > 0) {
								vector(interior, i, j, k) = 1;
								vector(nonSync, i, j, k) = 1;
							} else {
								vector(interior, i, j, k) = 0;
								vector(nonSync, i, j, k) = 0;
							}
						}
					}
				}
			}
			//Initial FloodFill call
			finishedGlobal = false;

			finishedPatchGlobal = false;

			pred = 1;
			//First floodfill
			pcounter = 0;
			for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
				const std::shared_ptr< hier::Patch >& patch = *p_it;

				//Get the dimensions of the patch
				hier::Box pbox = patch->getBox();
				double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
				double* FOV_3 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_3_id).get())->getPointer();
				double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
				double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
				double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
				double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
				double* FOV_zLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zLower_id).get())->getPointer();
				double* FOV_zUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zUpper_id).get())->getPointer();
				const hier::Index boxfirst = patch->getBox().lower();
				const hier::Index boxlast  = patch->getBox().upper();

				//Get delta spaces into an array. dx, dy, dz.
				const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
				const double* dx  = patch_geom->getDx();

				if (boxfirst(0) == 0 && boxfirst(1) == 0 && boxfirst(2) == 0) {
					floodfill(patch, 0, 0, 0, 1, 1);
					workingPatchArray[pcounter] = 1;
				}
				pcounter++;
			}
			if (nodes > 1) {
				d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);

				while (!finishedGlobal) {
					workingGlobal = true;
					//Filling started floodfill in all the patches
					while (workingGlobal) {
						working = 0;
						pcounter = 0;
						for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
							const std::shared_ptr< hier::Patch >& patch = *p_it;

							//Get the dimensions of the patch
							hier::Box pbox = patch->getBox();
							double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
							double* FOV_3 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_3_id).get())->getPointer();
							double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
							double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
							double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
							double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
							double* FOV_zLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zLower_id).get())->getPointer();
							double* FOV_zUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zUpper_id).get())->getPointer();
							double* interior_i = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_i_id).get())->getPointer();
							double* interior_j = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_j_id).get())->getPointer();
							double* interior_k = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_k_id).get())->getPointer();
							double* interior = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_regridding_value_id).get())->getPointer();
							int* nonSync = ((pdat::NodeData<int> *) patch->getPatchData(d_nonSync_regridding_tag_id).get())->getPointer();
							const hier::Index boxfirst = patch->getBox().lower();
							const hier::Index boxlast  = patch->getBox().upper();

							//Get delta spaces into an array. dx, dy, dz.
							const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
							const double* dx  = patch_geom->getDx();

							int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
							int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
							int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;
							workingPatchArray[pcounter] = 0;
							for (k = 0; k < klast && !workingPatchArray[pcounter]; k++) {
								for (j = 0; j < jlast && !workingPatchArray[pcounter]; j++) {
									for (i = 0; i < ilast && !workingPatchArray[pcounter]; i++) {
										if (vector(nonSync, i, j, k) != vector(interior, i, j, k) && vector(nonSync, i, j, k) == 0) {
											floodfill(patch, i, j, k, pred, 1);
											workingPatchArray[pcounter] = 1;
											working = 1;
										} else {
											if (vector(nonSync, i, j, k) != vector(interior, i, j, k)) {
												vector(interior, i, j, k) = vector(nonSync, i, j, k);
												if (vector(nonSync, i, j, k) == 2) {
													vector(FOV_1, i, j, k) = 100;
													vector(FOV_3, i, j, k) = 0;
												}
											}
										}
									}
								}
							}
							pcounter++;
						}
						d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);
						//Local working variable communication
						workingGlobal = false;
						int *s = new int[1];
						s[0] = working;
						mpi.AllReduce(s, 1, MPI_MAX);
						if (s[0] == 1) {
							workingGlobal = true;
						}
					}

					//Looking for a new floodfill
					finished = 1;
					pcounter = 0;
					for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
						const std::shared_ptr< hier::Patch >& patch = *p_it;

						//Get the dimensions of the patch
						hier::Box pbox = patch->getBox();
						double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
						double* FOV_3 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_3_id).get())->getPointer();
						double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
						double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
						double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
						double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
						double* FOV_zLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zLower_id).get())->getPointer();
						double* FOV_zUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zUpper_id).get())->getPointer();
						double* interior_i = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_i_id).get())->getPointer();
						double* interior_j = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_j_id).get())->getPointer();
						double* interior_k = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_k_id).get())->getPointer();
						double* interior = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_regridding_value_id).get())->getPointer();
						int* nonSync = ((pdat::NodeData<int> *) patch->getPatchData(d_nonSync_regridding_tag_id).get())->getPointer();
						const hier::Index boxfirst = patch->getBox().lower();
						const hier::Index boxlast  = patch->getBox().upper();

						//Get delta spaces into an array. dx, dy, dz.
						const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
						const double* dx  = patch_geom->getDx();

						int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
						int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
						int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;

						//Limits for the finished checking routine to avoid internal boundary problems
						int il = 0;
						if (!patch->getPatchGeometry()->getTouchesRegularBoundary (0, 0)) {
							il = 2;
						}
						int iu = ilast;
						if (!patch->getPatchGeometry()->getTouchesRegularBoundary (0, 1)) {
							iu = ilast - 4;
						}
						int jl = 0;
						if (!patch->getPatchGeometry()->getTouchesRegularBoundary (1, 0)) {
							jl = 2;
						}
						int ju = jlast;
						if (!patch->getPatchGeometry()->getTouchesRegularBoundary (1, 1)) {
							ju = jlast - 4;
						}
						int kl = 0;
						if (!patch->getPatchGeometry()->getTouchesRegularBoundary (2, 0)) {
							kl = 2;
						}
						int ku = klast;
						if (!patch->getPatchGeometry()->getTouchesRegularBoundary (2, 1)) {
							ku = klast - 4;
						}

						finishedPatchArray[pcounter] = 1;
						for (k = kl; k < ku && finishedPatchArray[pcounter]; k++) {
							for (j = jl; j < ju && finishedPatchArray[pcounter]; j++) {
								for (i = il; i < iu && finishedPatchArray[pcounter]; i++) {
									if (vector(nonSync, i, j, k) == 0) {
										finishedPatchArray[pcounter] = 0;
										finished = 0;
									}
								}
							}
						}
						pcounter++;
					}
					//Finished local variables communication
					for (proc = 0; proc < nodes; proc++) {
						if (proc == mpi.getRank()) {
							for (int dproc = 0; dproc < nodes; dproc++) {
								if (dproc != mpi.getRank()) {
									int *w = new int[1];
									w[0] = finished;
									int size = 1;
									mpi.Send(w, size, MPI_INT, dproc, 0);
								}
							}
							finishedArray[proc] = finished;
						} else {
							int *w = new int[1];
							int size = 1;
							tbox::SAMRAI_MPI::Status status;
							mpi.Recv(w, size, MPI_INT, proc, 0, &status);
							finishedArray[proc] = w[0];
						}
					}
					finishedGlobal = true;
					for (proc = 0; proc < nodes & finishedGlobal; proc++) {
						if (finishedArray[proc] == 0) {
							finishedGlobal = 0;
						}
					}

					//Only one processor could start a floodfill at the same time
					//Take the first who wants to
					int first = -1;
					int firstPatch = -1;
					for (proc = 0; proc < nodes && !finishedGlobal && first < 0; proc++) {
						if (finishedArray[proc] == 0) {
							first = proc;
							for (int pat = 0; pat < patches && !finishedPatchGlobal && firstPatch < 0; pat++) {
								if (finishedPatchArray[pat] == 0) {
									firstPatch = pat;
								}
							}
						}
					}
					if (pred == 1) {
						pred = 2;
					} else {
						pred = 1;
					}
					if (first == mpi.getRank() && !finishedGlobal) {
						pcounter = 0;
						for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
							const std::shared_ptr< hier::Patch >& patch = *p_it;

							//Get the dimensions of the patch
							hier::Box pbox = patch->getBox();
							double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
							double* FOV_3 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_3_id).get())->getPointer();
							double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
							double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
							double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
							double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
							double* FOV_zLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zLower_id).get())->getPointer();
							double* FOV_zUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zUpper_id).get())->getPointer();
							double* interior_i = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_i_id).get())->getPointer();
							double* interior_j = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_j_id).get())->getPointer();
							double* interior_k = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_k_id).get())->getPointer();
							double* interior = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_regridding_value_id).get())->getPointer();
							int* nonSync = ((pdat::NodeData<int> *) patch->getPatchData(d_nonSync_regridding_tag_id).get())->getPointer();
							const hier::Index boxfirst = patch->getBox().lower();
							const hier::Index boxlast  = patch->getBox().upper();

							//Get delta spaces into an array. dx, dy, dz.
							const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
							const double* dx  = patch_geom->getDx();

							int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
							int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
							int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;

							if (firstPatch == pcounter && !finishedPatchGlobal) {
								for (k = 0; k < klast && !workingPatchArray[pcounter]; k++) {
									for (j = 0; j < jlast && !workingPatchArray[pcounter]; j++) {
										for (i = 0; i < ilast && !workingPatchArray[pcounter]; i++) {
											if (vector(nonSync, i, j, k) == 0) {
												floodfill(patch, i, j, k, pred, 1);
												workingPatchArray[pcounter] = 1;
											}
										}
									}
								}
							}
							pcounter++;
						}
					}
					d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);
				}
			}
			//Only one processor
			else {
				while (!finishedPatchGlobal) {
					d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);
					workingPatchGlobal = true;
					//Filling started floodfill in all the patches
					while (workingPatchGlobal) {
						pcounter = 0;
						for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
							const std::shared_ptr< hier::Patch >& patch = *p_it;

							//Get the dimensions of the patch
							hier::Box pbox = patch->getBox();
							double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
							double* FOV_3 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_3_id).get())->getPointer();
							double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
							double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
							double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
							double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
							double* FOV_zLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zLower_id).get())->getPointer();
							double* FOV_zUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zUpper_id).get())->getPointer();
							double* interior_i = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_i_id).get())->getPointer();
							double* interior_j = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_j_id).get())->getPointer();
							double* interior_k = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_k_id).get())->getPointer();
							double* interior = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_regridding_value_id).get())->getPointer();
							int* nonSync = ((pdat::NodeData<int> *) patch->getPatchData(d_nonSync_regridding_tag_id).get())->getPointer();
							const hier::Index boxfirst = patch->getBox().lower();
							const hier::Index boxlast  = patch->getBox().upper();

							//Get delta spaces into an array. dx, dy, dz.
							const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
							const double* dx  = patch_geom->getDx();

							int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
							int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
							int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;

							workingPatchArray[pcounter] = 0;
							for (k = 0; k < klast && !workingPatchArray[pcounter]; k++) {
								for (j = 0; j < jlast && !workingPatchArray[pcounter]; j++) {
									for (i = 0; i < ilast && !workingPatchArray[pcounter]; i++) {
										if (vector(nonSync, i, j, k) != vector(interior, i, j, k) && vector(nonSync, i, j, k) ==0) {
											floodfill(patch, i, j, k, pred, 1);
											workingPatchArray[pcounter] = 1;
										} else {
											if (vector(nonSync, i, j, k) != vector(interior, i, j, k)) {
												vector(interior, i, j, k) = vector(nonSync, i, j, k);
												if (vector(nonSync, i, j, k) == 2) {
													vector(FOV_1, i, j, k) = 100;
													vector(FOV_3, i, j, k) = 0;
												}
											}
										}
									}
								}
							}
							pcounter++;
						}
						workingPatchGlobal = false;
						for (int pat = 0; pat < patches & !workingPatchGlobal; pat++) {
							if (workingPatchArray[pat] == 1) {
								workingPatchGlobal = true;
							}
						}
						d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);
					}
					//Looking for a new floodfill
					finished = 1;
					pcounter = 0;
					for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
						const std::shared_ptr< hier::Patch >& patch = *p_it;

						//Get the dimensions of the patch
						hier::Box pbox = patch->getBox();
						double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
						double* FOV_3 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_3_id).get())->getPointer();
						double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
						double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
						double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
						double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
						double* FOV_zLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zLower_id).get())->getPointer();
						double* FOV_zUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zUpper_id).get())->getPointer();
						double* interior_i = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_i_id).get())->getPointer();
						double* interior_j = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_j_id).get())->getPointer();
						double* interior_k = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_k_id).get())->getPointer();
						double* interior = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_regridding_value_id).get())->getPointer();
						int* nonSync = ((pdat::NodeData<int> *) patch->getPatchData(d_nonSync_regridding_tag_id).get())->getPointer();
						const hier::Index boxfirst = patch->getBox().lower();
						const hier::Index boxlast  = patch->getBox().upper();

						//Get delta spaces into an array. dx, dy, dz.
						const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
						const double* dx  = patch_geom->getDx();

						int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
						int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
						int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;

						//Limits for the finished checking routine to avoid internal boundary problems
						int il = 0;
						if (!patch->getPatchGeometry()->getTouchesRegularBoundary (0, 0)) {
							il = 2;
						}
						int iu = ilast;
						if (!patch->getPatchGeometry()->getTouchesRegularBoundary (0, 1)) {
							iu = ilast - 4;
						}
						int jl = 0;
						if (!patch->getPatchGeometry()->getTouchesRegularBoundary (1, 0)) {
							jl = 2;
						}
						int ju = jlast;
						if (!patch->getPatchGeometry()->getTouchesRegularBoundary (1, 1)) {
							ju = jlast - 4;
						}
						int kl = 0;
						if (!patch->getPatchGeometry()->getTouchesRegularBoundary (2, 0)) {
							kl = 2;
						}
						int ku = klast;
						if (!patch->getPatchGeometry()->getTouchesRegularBoundary (2, 1)) {
							ku = klast - 4;
						}

						finishedPatchArray[pcounter] = 1;
						for (k = kl; k < ku && finishedPatchArray[pcounter]; k++) {
							for (j = jl; j < ju && finishedPatchArray[pcounter]; j++) {
								for (i = il; i < iu && finishedPatchArray[pcounter]; i++) {
									if (vector(nonSync, i, j, k) == 0) {
										finishedPatchArray[pcounter] = 0;
										finished = 0;
									}
								}
							}
						}
						pcounter++;
					}
					//Finished local variables communication
					finishedPatchGlobal = true;
					for (int pat = 0; pat < patches & finishedPatchGlobal; pat++) {
						if (finishedPatchArray[pat] == 0) {
							finishedPatchGlobal = 0;
						}
					}
					//Only one patch could start a floodfill at the same time
					//Take the first who wants to
					int firstPatch = -1;
					for (int pat = 0; pat < patches && !finishedPatchGlobal && firstPatch < 0; pat++) {
						if (finishedPatchArray[pat] == 0) {
							firstPatch = pat;
						}
					}
					if (pred == 1) {
						pred = 2;
					} else {
						pred = 1;
					}
					//New floodfill
					pcounter = 0;
					for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
						const std::shared_ptr< hier::Patch >& patch = *p_it;

						//Get the dimensions of the patch
						hier::Box pbox = patch->getBox();
						double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
						double* FOV_3 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_3_id).get())->getPointer();
						double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
						double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
						double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
						double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
						double* FOV_zLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zLower_id).get())->getPointer();
						double* FOV_zUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zUpper_id).get())->getPointer();
						double* interior_i = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_i_id).get())->getPointer();
						double* interior_j = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_j_id).get())->getPointer();
						double* interior_k = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_k_id).get())->getPointer();
						double* interior = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_regridding_value_id).get())->getPointer();
						int* nonSync = ((pdat::NodeData<int> *) patch->getPatchData(d_nonSync_regridding_tag_id).get())->getPointer();
						const hier::Index boxfirst = patch->getBox().lower();
						const hier::Index boxlast  = patch->getBox().upper();

						//Get delta spaces into an array. dx, dy, dz.
						const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
						const double* dx  = patch_geom->getDx();

						int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
						int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
						int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;

						if (firstPatch == pcounter && !finishedPatchGlobal) {
							for (k = 0; k < klast && !workingPatchArray[pcounter]; k++) {
								for (j = 0; j < jlast && !workingPatchArray[pcounter]; j++) {
									for (i = 0; i < ilast && !workingPatchArray[pcounter]; i++) {
										if (vector(nonSync, i, j, k) == 0) {
											floodfill(patch, i, j, k, pred, 1);
											workingPatchArray[pcounter] = 1;
										}
									}
								}
							}
						}
						pcounter++;
					}
					d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);
				}
			}

			for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
				const std::shared_ptr< hier::Patch >& patch = *p_it;

				//Get the dimensions of the patch
				hier::Box pbox = patch->getBox();
				double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
				double* FOV_3 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_3_id).get())->getPointer();
				double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
				double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
				double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
				double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
				double* FOV_zLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zLower_id).get())->getPointer();
				double* FOV_zUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zUpper_id).get())->getPointer();
				double* interior_i = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_i_id).get())->getPointer();
				double* interior_j = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_j_id).get())->getPointer();
				double* interior_k = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_k_id).get())->getPointer();
				double* interior = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_regridding_value_id).get())->getPointer();
				int* nonSync = ((pdat::NodeData<int> *) patch->getPatchData(d_nonSync_regridding_tag_id).get())->getPointer();
				const hier::Index boxfirst = patch->getBox().lower();
				const hier::Index boxlast  = patch->getBox().upper();

				//Get delta spaces into an array. dx, dy, dz.
				const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
				const double* dx  = patch_geom->getDx();

				int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
				int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
				int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;

				//Check stencil
				for (k = 0; k < klast; k++) {
					for (j = 0; j < jlast; j++) {
						for (i = 0; i < ilast; i++) {
							vector(interior_i, i, j, k) = 0;
							vector(interior_j, i, j, k) = 0;
							vector(interior_k, i, j, k) = 0;
						}
					}
				}
				for (k = 0; k < klast; k++) {
					for (j = 0; j < jlast; j++) {
						for (i = 0; i < ilast; i++) {
							if (vector(FOV_1, i, j, k) > 0) {
								setStencilLimits(patch, i, j, k, d_FOV_1_id);
							}
						}
					}
				}
			}
			d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);
			for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
				const std::shared_ptr< hier::Patch >& patch = *p_it;

				//Get the dimensions of the patch
				hier::Box pbox = patch->getBox();
				double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
				double* FOV_3 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_3_id).get())->getPointer();
				double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
				double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
				double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
				double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
				double* FOV_zLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zLower_id).get())->getPointer();
				double* FOV_zUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zUpper_id).get())->getPointer();
				double* interior_i = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_i_id).get())->getPointer();
				double* interior_j = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_j_id).get())->getPointer();
				double* interior_k = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_k_id).get())->getPointer();
				double* interior = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_regridding_value_id).get())->getPointer();
				int* nonSync = ((pdat::NodeData<int> *) patch->getPatchData(d_nonSync_regridding_tag_id).get())->getPointer();
				const hier::Index boxfirst = patch->getBox().lower();
				const hier::Index boxlast  = patch->getBox().upper();

				//Get delta spaces into an array. dx, dy, dz.
				const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
				const double* dx  = patch_geom->getDx();

				int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
				int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
				int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;

				for (k = 0; k < klast; k++) {
					for (j = 0; j < jlast; j++) {
						for (i = 0; i < ilast; i++) {
							if ((abs(vector(interior_i, i, j, k)) > 1 || abs(vector(interior_j, i, j, k)) > 1 || abs(vector(interior_k, i, j, k)) > 1) && (i < d_ghost_width || i >= ilast - d_ghost_width || j < d_ghost_width || j >= jlast - d_ghost_width || k < d_ghost_width || k >= klast - d_ghost_width)) {
								checkStencil(patch, i, j, k, d_FOV_1_id);
							}
						}
					}
				}
				for (k = 0; k < klast; k++) {
					for (j = 0; j < jlast; j++) {
						for (i = 0; i < ilast; i++) {
							if (vector(interior_i, i, j, k) != 0 || vector(interior_j, i, j, k) != 0 || vector(interior_k, i, j, k) != 0) {
								vector(FOV_1, i, j, k) = 100;
								vector(FOV_3, i, j, k) = 0;
								vector(FOV_xLower, i, j, k) = 0;
								vector(FOV_xUpper, i, j, k) = 0;
								vector(FOV_yLower, i, j, k) = 0;
								vector(FOV_yUpper, i, j, k) = 0;
								vector(FOV_zLower, i, j, k) = 0;
								vector(FOV_zUpper, i, j, k) = 0;
							}
						}
					}
				}
			}
			d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);

		}
		//Boundaries Mapping
		for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
			const std::shared_ptr< hier::Patch >& patch = *p_it;

			//Get the dimensions of the patch
			hier::Box pbox = patch->getBox();
			double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
			double* FOV_3 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_3_id).get())->getPointer();
			double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
			double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
			double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
			double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
			double* FOV_zLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zLower_id).get())->getPointer();
			double* FOV_zUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zUpper_id).get())->getPointer();
			const hier::Index boxfirst = patch->getBox().lower();
			const hier::Index boxlast  = patch->getBox().upper();

			//Get delta spaces into an array. dx, dy, dz.
			const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
			const double* dx  = patch_geom->getDx();

			int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
			int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
			int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;

			//z-Upper
			if (patch->getPatchGeometry()->getTouchesRegularBoundary(2, 1)) {
				for (k = klast - d_ghost_width; k < klast; k++) {
					for (j = 0; j < jlast; j++) {
						for (i = 0; i < ilast; i++) {
							vector(FOV_zUpper, i, j, k) = 100;
							vector(FOV_1, i, j, k) = 0;
							vector(FOV_3, i, j, k) = 0;
							vector(FOV_xLower, i, j, k) = 0;
							vector(FOV_xUpper, i, j, k) = 0;
							vector(FOV_yLower, i, j, k) = 0;
							vector(FOV_yUpper, i, j, k) = 0;
							vector(FOV_zLower, i, j, k) = 0;
						}
					}
				}
			}
			//z-Lower
			if (patch->getPatchGeometry()->getTouchesRegularBoundary(2, 0)) {
				for (k = 0; k < d_ghost_width; k++) {
					for (j = 0; j < jlast; j++) {
						for (i = 0; i < ilast; i++) {
							vector(FOV_zLower, i, j, k) = 100;
							vector(FOV_1, i, j, k) = 0;
							vector(FOV_3, i, j, k) = 0;
							vector(FOV_xLower, i, j, k) = 0;
							vector(FOV_xUpper, i, j, k) = 0;
							vector(FOV_yLower, i, j, k) = 0;
							vector(FOV_yUpper, i, j, k) = 0;
							vector(FOV_zUpper, i, j, k) = 0;
						}
					}
				}
			}
			//y-Upper
			if (patch->getPatchGeometry()->getTouchesRegularBoundary(1, 1)) {
				for (k = 0; k < klast; k++) {
					for (j = jlast - d_ghost_width; j < jlast; j++) {
						for (i = 0; i < ilast; i++) {
							vector(FOV_yUpper, i, j, k) = 100;
							vector(FOV_1, i, j, k) = 0;
							vector(FOV_3, i, j, k) = 0;
							vector(FOV_xLower, i, j, k) = 0;
							vector(FOV_xUpper, i, j, k) = 0;
							vector(FOV_yLower, i, j, k) = 0;
							vector(FOV_zLower, i, j, k) = 0;
							vector(FOV_zUpper, i, j, k) = 0;
						}
					}
				}
			}
			//y-Lower
			if (patch->getPatchGeometry()->getTouchesRegularBoundary(1, 0)) {
				for (k = 0; k < klast; k++) {
					for (j = 0; j < d_ghost_width; j++) {
						for (i = 0; i < ilast; i++) {
							vector(FOV_yLower, i, j, k) = 100;
							vector(FOV_1, i, j, k) = 0;
							vector(FOV_3, i, j, k) = 0;
							vector(FOV_xLower, i, j, k) = 0;
							vector(FOV_xUpper, i, j, k) = 0;
							vector(FOV_yUpper, i, j, k) = 0;
							vector(FOV_zLower, i, j, k) = 0;
							vector(FOV_zUpper, i, j, k) = 0;
						}
					}
				}
			}
			//x-Upper
			if (patch->getPatchGeometry()->getTouchesRegularBoundary(0, 1)) {
				for (k = 0; k < klast; k++) {
					for (j = 0; j < jlast; j++) {
						for (i = ilast - d_ghost_width; i < ilast; i++) {
							vector(FOV_xUpper, i, j, k) = 100;
							vector(FOV_1, i, j, k) = 0;
							vector(FOV_3, i, j, k) = 0;
							vector(FOV_xLower, i, j, k) = 0;
							vector(FOV_yLower, i, j, k) = 0;
							vector(FOV_yUpper, i, j, k) = 0;
							vector(FOV_zLower, i, j, k) = 0;
							vector(FOV_zUpper, i, j, k) = 0;
						}
					}
				}
			}
			//x-Lower
			if (patch->getPatchGeometry()->getTouchesRegularBoundary(0, 0)) {
				for (k = 0; k < klast; k++) {
					for (j = 0; j < jlast; j++) {
						for (i = 0; i < d_ghost_width; i++) {
							vector(FOV_xLower, i, j, k) = 100;
							vector(FOV_1, i, j, k) = 0;
							vector(FOV_3, i, j, k) = 0;
							vector(FOV_xUpper, i, j, k) = 0;
							vector(FOV_yLower, i, j, k) = 0;
							vector(FOV_yUpper, i, j, k) = 0;
							vector(FOV_zLower, i, j, k) = 0;
							vector(FOV_zUpper, i, j, k) = 0;
						}
					}
				}
			}
		}
		d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);




   	}
}


/*
 * Sets the limit for the checkstencil routine
 */
void Problem::setStencilLimits(std::shared_ptr< hier::Patch > patch, int i, int j, int k, int v) const {
	double* FOV = ((pdat::NodeData<double> *) patch->getPatchData(v).get())->getPointer();
	double* interior_i = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_i_id).get())->getPointer();
	double* interior_j = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_j_id).get())->getPointer();
	double* interior_k = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_k_id).get())->getPointer();
	//Get the dimensions of the patch
	const hier::Index boxfirst = patch->getBox().lower();
	const hier::Index boxlast  = patch->getBox().upper();
	//Auxiliary definitions
	int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
	int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
	int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;

	int iStart, iEnd, currentGhosti, otherSideShifti, jStart, jEnd, currentGhostj, otherSideShiftj, kStart, kEnd, currentGhostk, otherSideShiftk, shift;

	currentGhosti = d_ghost_width - 1;
	otherSideShifti = 0;
	currentGhostj = d_ghost_width - 1;
	otherSideShiftj = 0;
	currentGhostk = d_ghost_width - 1;
	otherSideShiftk = 0;
	//Checking width
	if ((i + 1 < ilast && vector(FOV, i + 1, j, k) == 0) ||  (i - 1 >= 0 && vector(FOV, i - 1, j, k) == 0)) {
		if (i + 1 < ilast && vector(FOV, i + 1, j, k) > 0) {
			bool stop_counting = false;
			for(int iti = i + 1; iti <= i + d_ghost_width - 1 && currentGhosti > 0; iti++) {
				if (iti < ilast  && vector(FOV, iti, j, k) > 0 && stop_counting == false) {
					currentGhosti--;
				} else {
					//First not interior point found
					if (iti < ilast  && vector(FOV, iti, j, k) == 0) {
						stop_counting = true;
					}
					//Physical boundary reach
					if (iti >= ilast - 2 && patch->getPatchGeometry()->getTouchesRegularBoundary (0, 1)) {
						stop_counting = true;
						//Calculate the number of cells the limit cannot grow
						if (iti + currentGhosti/2 >= ilast - 2) {
							otherSideShifti = (iti  + currentGhosti/2) - (ilast - 2);
						}
					}
				}
			}
		}
		if (i - 1 >= 0 && vector(FOV, i - 1, j, k) > 0) {
			bool stop_counting = false;
			for(int iti = i - 1; iti >= i - d_ghost_width + 1 && currentGhosti > 0; iti--) {
				if (iti >= 0  && vector(FOV, iti, j, k) > 0) {
					currentGhosti--;
				} else {
					//First not interior point found
					if (iti >= 0 && vector(FOV, iti, j, k) == 0) {
						stop_counting = true;
					}
					//Physical boundary reach
					if (iti < 2 && patch->getPatchGeometry()->getTouchesRegularBoundary (0, 0)) {
						stop_counting = true;
						//calculate the number of cells the limit cannot grow
						if (iti  -  ((currentGhosti) - currentGhosti/2) < 2) {
							otherSideShifti = 2 - (iti  - ((currentGhosti) - currentGhosti/2));
						}
					}
				}
			}
		}
		if (currentGhosti > 0) {
			if (i + 1 < ilast && vector(FOV, i + 1, j, k) > 0) {
				shift = 0;
				if(patch->getPatchGeometry()->getTouchesRegularBoundary (0, 0)) {
					while(i - ((currentGhosti) - currentGhosti/2) + shift < 2) {
						shift++;
					}
				}
				iStart = (currentGhosti - currentGhosti/2) - shift - otherSideShifti;
				iEnd = 0;
			} else {
				if (i - 1 >= 0 && vector(FOV, i - 1, j, k) > 0) {
					shift = 0;
					if(patch->getPatchGeometry()->getTouchesRegularBoundary (0, 1)) {
						while(i + currentGhosti/2 + shift >= ilast - 2) {
							shift--;
						}
					}
					iStart = 0;
					iEnd = currentGhosti/2 + shift + otherSideShifti;
				} else {
					shift = 0;
					if(patch->getPatchGeometry()->getTouchesRegularBoundary (0, 0)) {
						while(i - ((currentGhosti) - currentGhosti/2) + shift < 2) {
							shift++;
						}
					}
					if(patch->getPatchGeometry()->getTouchesRegularBoundary (0, 1)) {
						while(i + currentGhosti/2 + shift >= ilast - 2) {
							shift--;
						}
					}
					iStart = (currentGhosti - currentGhosti/2) - shift;
					iEnd = currentGhosti/2 + shift;
				}
			}
		} else {
			iStart = 0;
			iEnd = 0;
		}
	} else {
		iStart = 0;
		iEnd = 0;
	}
	if ((j + 1 < jlast && vector(FOV, i, j + 1, k) == 0) ||  (j - 1 >= 0 && vector(FOV, i, j - 1, k) == 0)) {
		if (j + 1 < jlast && vector(FOV, i, j + 1, k) > 0) {
			bool stop_counting = false;
			for(int itj = j + 1; itj <= j + d_ghost_width - 1 && currentGhostj > 0; itj++) {
				if (itj < jlast  && vector(FOV, i, itj, k) > 0 && stop_counting == false) {
					currentGhostj--;
				} else {
					//First not interior point found
					if (itj < jlast  && vector(FOV, i, itj, k) == 0) {
						stop_counting = true;
					}
					//Physical boundary reach
					if (itj >= jlast - 2 && patch->getPatchGeometry()->getTouchesRegularBoundary (1, 1)) {
						stop_counting = true;
						//Calculate the number of cells the limit cannot grow
						if (itj + currentGhostj/2 >= jlast - 2) {
							otherSideShiftj = (itj  + currentGhostj/2) - (jlast - 2);
						}
					}
				}
			}
		}
		if (j - 1 >= 0 && vector(FOV, i, j - 1, k) > 0) {
			bool stop_counting = false;
			for(int itj = j - 1; itj >= j - d_ghost_width + 1 && currentGhostj > 0; itj--) {
				if (itj >= 0  && vector(FOV, i, itj, k) > 0) {
					currentGhostj--;
				} else {
					//First not interior point found
					if (itj >= 0 && vector(FOV, i, itj, k) == 0) {
						stop_counting = true;
					}
					//Physical boundary reach
					if (itj < 2 && patch->getPatchGeometry()->getTouchesRegularBoundary (1, 0)) {
						stop_counting = true;
						//calculate the number of cells the limit cannot grow
						if (itj  -  ((currentGhostj) - currentGhostj/2) < 2) {
							otherSideShiftj = 2 - (itj  - ((currentGhostj) - currentGhostj/2));
						}
					}
				}
			}
		}
		if (currentGhostj > 0) {
			if (j + 1 < jlast && vector(FOV, i, j + 1, k) > 0) {
				shift = 0;
				if(patch->getPatchGeometry()->getTouchesRegularBoundary (1, 0)) {
					while(j - ((currentGhostj) - currentGhostj/2) + shift < 2) {
						shift++;
					}
				}
				jStart = (currentGhostj - currentGhostj/2) - shift - otherSideShiftj;
				jEnd = 0;
			} else {
				if (j - 1 >= 0 && vector(FOV, i, j - 1, k) > 0) {
					shift = 0;
					if(patch->getPatchGeometry()->getTouchesRegularBoundary (1, 1)) {
						while(j + currentGhostj/2 + shift >= jlast - 2) {
							shift--;
						}
					}
					jStart = 0;
					jEnd = currentGhostj/2 + shift + otherSideShiftj;
				} else {
					shift = 0;
					if(patch->getPatchGeometry()->getTouchesRegularBoundary (1, 0)) {
						while(j - ((currentGhostj) - currentGhostj/2) + shift < 2) {
							shift++;
						}
					}
					if(patch->getPatchGeometry()->getTouchesRegularBoundary (1, 1)) {
						while(j + currentGhostj/2 + shift >= jlast - 2) {
							shift--;
						}
					}
					jStart = (currentGhostj - currentGhostj/2) - shift;
					jEnd = currentGhostj/2 + shift;
				}
			}
		} else {
			jStart = 0;
			jEnd = 0;
		}
	} else {
		jStart = 0;
		jEnd = 0;
	}
	if ((k + 1 < klast && vector(FOV, i, j, k + 1) == 0) ||  (k - 1 >= 0 && vector(FOV, i, j, k - 1) == 0)) {
		if (k + 1 < klast && vector(FOV, i, j, k + 1) > 0) {
			bool stop_counting = false;
			for(int itk = k + 1; itk <= k + d_ghost_width - 1 && currentGhostk > 0; itk++) {
				if (itk < klast  && vector(FOV, i, j, itk) > 0 && stop_counting == false) {
					currentGhostk--;
				} else {
					//First not interior point found
					if (itk < klast  && vector(FOV, i, j, itk) == 0) {
						stop_counting = true;
					}
					//Physical boundary reach
					if (itk >= klast - 2 && patch->getPatchGeometry()->getTouchesRegularBoundary (2, 1)) {
						stop_counting = true;
						//Calculate the number of cells the limit cannot grow
						if (itk + currentGhostk/2 >= klast - 2) {
							otherSideShiftk = (itk  + currentGhostk/2) - (klast - 2);
						}
					}
				}
			}
		}
		if (k - 1 >= 0 && vector(FOV, i, j, k - 1) > 0) {
			bool stop_counting = false;
			for(int itk = k - 1; itk >= k - d_ghost_width + 1 && currentGhostk > 0; itk--) {
				if (itk >= 0  && vector(FOV, i, j, itk) > 0) {
					currentGhostk--;
				} else {
					//First not interior point found
					if (itk >= 0 && vector(FOV, i, j, itk) == 0) {
						stop_counting = true;
					}
					//Physical boundary reach
					if (itk < 2 && patch->getPatchGeometry()->getTouchesRegularBoundary (2, 0)) {
						stop_counting = true;
						//calculate the number of cells the limit cannot grow
						if (itk  -  ((currentGhostk) - currentGhostk/2) < 2) {
							otherSideShiftk = 2 - (itk  - ((currentGhostk) - currentGhostk/2));
						}
					}
				}
			}
		}
		if (currentGhostk > 0) {
			if (k + 1 < klast && vector(FOV, i, j, k + 1) > 0) {
				shift = 0;
				if(patch->getPatchGeometry()->getTouchesRegularBoundary (2, 0)) {
					while(k - ((currentGhostk) - currentGhostk/2) + shift < 2) {
						shift++;
					}
				}
				kStart = (currentGhostk - currentGhostk/2) - shift - otherSideShiftk;
				kEnd = 0;
			} else {
				if (k - 1 >= 0 && vector(FOV, i, j, k - 1) > 0) {
					shift = 0;
					if(patch->getPatchGeometry()->getTouchesRegularBoundary (2, 1)) {
						while(k + currentGhostk/2 + shift >= klast - 2) {
							shift--;
						}
					}
					kStart = 0;
					kEnd = currentGhostk/2 + shift + otherSideShiftk;
				} else {
					shift = 0;
					if(patch->getPatchGeometry()->getTouchesRegularBoundary (2, 0)) {
						while(k - ((currentGhostk) - currentGhostk/2) + shift < 2) {
							shift++;
						}
					}
					if(patch->getPatchGeometry()->getTouchesRegularBoundary (2, 1)) {
						while(k + currentGhostk/2 + shift >= klast - 2) {
							shift--;
						}
					}
					kStart = (currentGhostk - currentGhostk/2) - shift;
					kEnd = currentGhostk/2 + shift;
				}
			}
		} else {
			kStart = 0;
			kEnd = 0;
		}
	} else {
		kStart = 0;
		kEnd = 0;
	}
	//Assigning stencil limits
	for(int iti = i - iStart; iti <= i + iEnd; iti++) {
		for(int itj = j - jStart; itj <= j + jEnd; itj++) {
			for(int itk = k - kStart; itk <= k + kEnd; itk++) {
				if(iti >= 0 && iti < ilast && itj >= 0 && itj < jlast && itk >= 0 && itk < klast && vector(FOV, iti, itj, itk) == 0) {
					if (i - iti < 0) {
						vector(interior_i, iti, itj, itk) = - (iStart + 1) - (i - iti);
					} else {
						vector(interior_i, iti, itj, itk) = (iStart + 1) - (i - iti);
					}
					if (j - itj < 0) {
						vector(interior_j, iti, itj, itk) = - (jStart + 1) - (j - itj);
					} else {
						vector(interior_j, iti, itj, itk) = (jStart + 1) - (j - itj);
					}
					if (k - itk < 0) {
						vector(interior_k, iti, itj, itk) = - (kStart + 1) - (k - itk);
					} else {
						vector(interior_k, iti, itj, itk) = (kStart + 1) - (k - itk);
					}
				}
			}
		}
	}
}

/*
 * Checks if the point has a stencil width
 */
void Problem::checkStencil(std::shared_ptr< hier::Patch > patch, int i, int j, int k, int v) const {
	double* FOV = ((pdat::NodeData<double> *) patch->getPatchData(v).get())->getPointer();
	double* interior_i = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_i_id).get())->getPointer();
	double* interior_j = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_j_id).get())->getPointer();
	double* interior_k = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_k_id).get())->getPointer();
	//Get the dimensions of the patch
	const hier::Index boxfirst = patch->getBox().lower();
	const hier::Index boxlast  = patch->getBox().upper();
	//Auxiliary definitions
	int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
	int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
	int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;

	int i_i = vector(interior_i, i, j, k);
	int iStart = MAX(0, i_i) - 1;
	int iEnd = MAX(0, -i_i) - 1;
	int i_j = vector(interior_j, i, j, k);
	int jStart = MAX(0, i_j) - 1;
	int jEnd = MAX(0, -i_j) - 1;
	int i_k = vector(interior_k, i, j, k);
	int kStart = MAX(0, i_k) - 1;
	int kEnd = MAX(0, -i_k) - 1;

	for(int iti = i - iStart; iti <= i + iEnd; iti++) {
		for(int itj = j - jStart; itj <= j + jEnd; itj++) {
			for(int itk = k - kStart; itk <= k + kEnd; itk++) {
				if(iti >= 0 && iti < ilast && itj >= 0 && itj < jlast && itk >= 0 && itk < klast && vector(FOV, iti, itj, itk) == 0) {
					vector(interior_i, iti, itj, itk) = i_i  - (i - iti);
					vector(interior_j, iti, itj, itk) = i_j  - (j - itj);
					vector(interior_k, iti, itj, itk) = i_k  - (k - itk);
				}
			}
		}
	}
}


// Point class for the floodfill algorithm
class Point {
private:
public:
	int i, j, k;
};

void Problem::floodfill(std::shared_ptr< hier::Patch > patch, int i, int j, int k, int pred, int seg) const {

	double* FOV;
	double* FOV_1;
	switch(seg) {
		case 1:
			FOV = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
			FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_3_id).get())->getPointer();
		break;
		case 3:
			FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
			FOV = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_3_id).get())->getPointer();
		break;
	}
	int* nonSync = ((pdat::NodeData<int> *) patch->getPatchData(d_nonSync_regridding_tag_id).get())->getPointer();
	double* interior = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_regridding_value_id).get())->getPointer();
	//Get the dimensions of the patch
	const hier::Index boxfirst = patch->getBox().lower();
	const hier::Index boxlast  = patch->getBox().upper();
	//Auxiliary definitions
	int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
	int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
	int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;

	stack<Point> mystack;

	Point p;
	p.i = i;
	p.j = j;
	p.k = k;

	mystack.push(p);
	while(mystack.size() > 0) {
		p = mystack.top();
		mystack.pop();
		if (vector(nonSync, p.i, p.j, p.k) == 0) {
			vector(nonSync, p.i, p.j, p.k) = pred;
			vector(interior, p.i, p.j, p.k) = pred;
			if (pred == 2) {
				vector(FOV, p.i, p.j, p.k) = 100;
				vector(FOV_1, p.i, p.j, p.k) = 0;
			}
			if (p.i - 1 >= 0 && vector(nonSync, p.i - 1, p.j, p.k) == 0) {
				Point np;
				np.i = p.i-1;
				np.j = p.j;
				np.k = p.k;
				mystack.push(np);
			}
			if (p.i + 1 < ilast && vector(nonSync, p.i + 1, p.j, p.k) == 0) {
				Point np;
				np.i = p.i+1;
				np.j = p.j;
				np.k = p.k;
				mystack.push(np);
			}
			if (p.j - 1 >= 0 && vector(nonSync, p.i, p.j - 1, p.k) == 0) {
				Point np;
				np.i = p.i;
				np.j = p.j-1;
				np.k = p.k;
				mystack.push(np);
			}
			if (p.j + 1 < jlast && vector(nonSync, p.i, p.j + 1, p.k) == 0) {
				Point np;
				np.i = p.i;
				np.j = p.j+1;
				np.k = p.k;
				mystack.push(np);
			}
			if (p.k - 1 >= 0 && vector(nonSync, p.i, p.j, p.k - 1) == 0) {
				Point np;
				np.i = p.i;
				np.j = p.j;
				np.k = p.k-1;
				mystack.push(np);
			}
			if (p.k + 1 < klast && vector(nonSync, p.i, p.j, p.k + 1) == 0) {
				Point np;
				np.i = p.i;
				np.j = p.j;
				np.k = p.k+1;
				mystack.push(np);
			}
		}
	}
}


/*
 * FOV correction for AMR
 */
void Problem::correctFOVS(const std::shared_ptr< hier::PatchLevel >& level) {
	int i, j, k;
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr< hier::Patch >& patch = *p_it;

		//Get the dimensions of the patch
		hier::Box pbox = patch->getBox();
		double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_3 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_3_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
		double* FOV_zLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zLower_id).get())->getPointer();
		double* FOV_zUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zUpper_id).get())->getPointer();
		const hier::Index boxfirst = patch->getBox().lower();
		const hier::Index boxlast  = patch->getBox().upper();

		//Get delta spaces into an array. dx, dy, dz.
		const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
		const double* dx  = patch_geom->getDx();

		int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
		int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
		int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;

		for (i = 0; i < ilast; i++) {
			for (j = 0; j < jlast; j++) {
				for (k = 0; k < klast; k++) {
					if (vector(FOV_xLower, i, j, k) > 0) {
						vector(FOV_xLower, i, j, k) = 100;
						vector(FOV_1, i, j, k) = 0;
						vector(FOV_3, i, j, k) = 0;
						vector(FOV_xUpper, i, j, k) = 0;
						vector(FOV_yLower, i, j, k) = 0;
						vector(FOV_yUpper, i, j, k) = 0;
						vector(FOV_zLower, i, j, k) = 0;
						vector(FOV_zUpper, i, j, k) = 0;
					}
					if (vector(FOV_xUpper, i, j, k) > 0) {
						vector(FOV_xUpper, i, j, k) = 100;
						vector(FOV_1, i, j, k) = 0;
						vector(FOV_3, i, j, k) = 0;
						vector(FOV_xLower, i, j, k) = 0;
						vector(FOV_yLower, i, j, k) = 0;
						vector(FOV_yUpper, i, j, k) = 0;
						vector(FOV_zLower, i, j, k) = 0;
						vector(FOV_zUpper, i, j, k) = 0;
					}
					if (vector(FOV_yLower, i, j, k) > 0) {
						vector(FOV_yLower, i, j, k) = 100;
						vector(FOV_1, i, j, k) = 0;
						vector(FOV_3, i, j, k) = 0;
						vector(FOV_xLower, i, j, k) = 0;
						vector(FOV_xUpper, i, j, k) = 0;
						vector(FOV_yUpper, i, j, k) = 0;
						vector(FOV_zLower, i, j, k) = 0;
						vector(FOV_zUpper, i, j, k) = 0;
					}
					if (vector(FOV_yUpper, i, j, k) > 0) {
						vector(FOV_yUpper, i, j, k) = 100;
						vector(FOV_1, i, j, k) = 0;
						vector(FOV_3, i, j, k) = 0;
						vector(FOV_xLower, i, j, k) = 0;
						vector(FOV_xUpper, i, j, k) = 0;
						vector(FOV_yLower, i, j, k) = 0;
						vector(FOV_zLower, i, j, k) = 0;
						vector(FOV_zUpper, i, j, k) = 0;
					}
					if (vector(FOV_zLower, i, j, k) > 0) {
						vector(FOV_zLower, i, j, k) = 100;
						vector(FOV_1, i, j, k) = 0;
						vector(FOV_3, i, j, k) = 0;
						vector(FOV_xLower, i, j, k) = 0;
						vector(FOV_xUpper, i, j, k) = 0;
						vector(FOV_yLower, i, j, k) = 0;
						vector(FOV_yUpper, i, j, k) = 0;
						vector(FOV_zUpper, i, j, k) = 0;
					}
					if (vector(FOV_zUpper, i, j, k) > 0) {
						vector(FOV_zUpper, i, j, k) = 100;
						vector(FOV_1, i, j, k) = 0;
						vector(FOV_3, i, j, k) = 0;
						vector(FOV_xLower, i, j, k) = 0;
						vector(FOV_xUpper, i, j, k) = 0;
						vector(FOV_yLower, i, j, k) = 0;
						vector(FOV_yUpper, i, j, k) = 0;
						vector(FOV_zLower, i, j, k) = 0;
					}
					if (vector(FOV_1, i, j, k) > 0) {
						vector(FOV_1, i, j, k) = 100;
						vector(FOV_3, i, j, k) = 0;
						vector(FOV_xLower, i, j, k) = 0;
						vector(FOV_xUpper, i, j, k) = 0;
						vector(FOV_yLower, i, j, k) = 0;
						vector(FOV_yUpper, i, j, k) = 0;
						vector(FOV_zLower, i, j, k) = 0;
						vector(FOV_zUpper, i, j, k) = 0;
					}
					if (vector(FOV_3, i, j, k) > 0) {
						vector(FOV_3, i, j, k) = 100;
						vector(FOV_1, i, j, k) = 0;
						vector(FOV_xLower, i, j, k) = 0;
						vector(FOV_xUpper, i, j, k) = 0;
						vector(FOV_yLower, i, j, k) = 0;
						vector(FOV_yUpper, i, j, k) = 0;
						vector(FOV_zLower, i, j, k) = 0;
						vector(FOV_zUpper, i, j, k) = 0;
					}
				}
			}
		}
	}
}




void Problem::interphaseMapping(const double time ,const bool initial_time,const int ln, const std::shared_ptr< hier::PatchLevel >& level, const int remesh) {
	const tbox::SAMRAI_MPI& mpi(tbox::SAMRAI_MPI::getSAMRAIWorld());
	if (remesh == 1) {
		//Update extrapolation variables
		//Calculation of hard region distance variables
		for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
			const std::shared_ptr< hier::Patch >& patch = *p_it;

			//Get the dimensions of the patch
			const hier::Index boxfirst = patch->getBox().lower();
			const hier::Index boxlast  = patch->getBox().upper();
			const hier::IntVector ratio = level->getRatioToCoarserLevel();
			double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
			double* FOV_3 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_3_id).get())->getPointer();
			double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
			double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
			double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
			double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
			double* FOV_zLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zLower_id).get())->getPointer();
			double* FOV_zUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zUpper_id).get())->getPointer();
			double* stalled_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_stalled_1_id).get())->getPointer();
			double* stalled_3 = ((pdat::NodeData<double> *) patch->getPatchData(d_stalled_3_id).get())->getPointer();
			//Hard region field distance variables
			double* d_i_rho_mx_my_mz_p_fx_fy_fz = ((pdat::NodeData<double> *) patch->getPatchData(d_d_i_rho_mx_my_mz_p_fx_fy_fz_id).get())->getPointer();
			double* d_j_rho_mx_my_mz_p_fx_fy_fz = ((pdat::NodeData<double> *) patch->getPatchData(d_d_j_rho_mx_my_mz_p_fx_fy_fz_id).get())->getPointer();
			double* d_k_rho_mx_my_mz_p_fx_fy_fz = ((pdat::NodeData<double> *) patch->getPatchData(d_d_k_rho_mx_my_mz_p_fx_fy_fz_id).get())->getPointer();
			double* d_i_Null = ((pdat::NodeData<double> *) patch->getPatchData(d_d_i_Null_id).get())->getPointer();
			double* d_j_Null = ((pdat::NodeData<double> *) patch->getPatchData(d_d_j_Null_id).get())->getPointer();
			double* d_k_Null = ((pdat::NodeData<double> *) patch->getPatchData(d_d_k_Null_id).get())->getPointer();

			int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
			int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
			int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;
			for (int i = 0; i < ilast; i++) {
				for (int j = 0; j < jlast; j++) {
					for (int k = 0; k < klast; k++) {
						vector(stalled_1, i, j, k) = checkStalled(patch, i, j, k, d_FOV_1_id);
						vector(stalled_3, i, j, k) = checkStalled(patch, i, j, k, d_FOV_3_id);
						vector(d_i_rho_mx_my_mz_p_fx_fy_fz, i, j, k) = 0;
						vector(d_j_rho_mx_my_mz_p_fx_fy_fz, i, j, k) = 0;
						vector(d_k_rho_mx_my_mz_p_fx_fy_fz, i, j, k) = 0;
						vector(d_i_Null, i, j, k) = 0;
						vector(d_j_Null, i, j, k) = 0;
						vector(d_k_Null, i, j, k) = 0;
					}
				}
			}
		}
		d_mapping_fill->createSchedule(level, level)->fillData(time, true);
		for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
			const std::shared_ptr< hier::Patch >& patch = *p_it;

			//Get the dimensions of the patch
			const hier::Index boxfirst = patch->getBox().lower();
			const hier::Index boxlast  = patch->getBox().upper();
			const hier::IntVector ratio = level->getRatioToCoarserLevel();
			double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
			double* FOV_3 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_3_id).get())->getPointer();
			double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
			double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
			double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
			double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
			double* FOV_zLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zLower_id).get())->getPointer();
			double* FOV_zUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zUpper_id).get())->getPointer();
			double* stalled_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_stalled_1_id).get())->getPointer();
			double* stalled_3 = ((pdat::NodeData<double> *) patch->getPatchData(d_stalled_3_id).get())->getPointer();
			//Hard region field distance variables
			double* d_i_rho_mx_my_mz_p_fx_fy_fz = ((pdat::NodeData<double> *) patch->getPatchData(d_d_i_rho_mx_my_mz_p_fx_fy_fz_id).get())->getPointer();
			double* d_j_rho_mx_my_mz_p_fx_fy_fz = ((pdat::NodeData<double> *) patch->getPatchData(d_d_j_rho_mx_my_mz_p_fx_fy_fz_id).get())->getPointer();
			double* d_k_rho_mx_my_mz_p_fx_fy_fz = ((pdat::NodeData<double> *) patch->getPatchData(d_d_k_rho_mx_my_mz_p_fx_fy_fz_id).get())->getPointer();
			double* d_i_Null = ((pdat::NodeData<double> *) patch->getPatchData(d_d_i_Null_id).get())->getPointer();
			double* d_j_Null = ((pdat::NodeData<double> *) patch->getPatchData(d_d_j_Null_id).get())->getPointer();
			double* d_k_Null = ((pdat::NodeData<double> *) patch->getPatchData(d_d_k_Null_id).get())->getPointer();

			int reGrid_i;
			if (ratio(0) == 0) reGrid_i = 1;
			else           reGrid_i = ratio(0);
			int reGrid_j;
			if (ratio(1) == 0) reGrid_j = 1;
			else           reGrid_j = ratio(1);
			int reGrid_k;
			if (ratio(2) == 0) reGrid_k = 1;
			else           reGrid_k = ratio(2);

			int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
			int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
			int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;
			int dist_i_tmp, dist_i_tmp_p, dist_i_tmp_m, dist_i_tmp_r;
			int dist_j_tmp, dist_j_tmp_p, dist_j_tmp_m, dist_j_tmp_r;
			int dist_k_tmp, dist_k_tmp_p, dist_k_tmp_m, dist_k_tmp_r;
			int dist_r, dist_r_tmp;

			for (int i = 0; i < ilast; i++) {
				for (int j = 0; j < jlast; j++) {
					for (int k = 0; k < klast; k++) {
						if (vector(FOV_3, i, j, k) > 0) {
							dist_i_tmp = 999;
							dist_i_tmp_p = 999;
							dist_i_tmp_m = 999;
							dist_i_tmp_r = 999;
							dist_j_tmp = 999;
							dist_j_tmp_p = 999;
							dist_j_tmp_m = 999;
							dist_j_tmp_r = 999;
							dist_k_tmp = 999;
							dist_k_tmp_p = 999;
							dist_k_tmp_m = 999;
							dist_k_tmp_r = 999;
							dist_r = 999;
							for (int Dist_i = -2 * reGrid_i; Dist_i <= 2 * reGrid_i; Dist_i++) {
								for (int Dist_j = -2 * reGrid_j; Dist_j <= 2 * reGrid_j; Dist_j++) {
									for (int Dist_k = -2 * reGrid_k; Dist_k <= 2 * reGrid_k; Dist_k++) {
										if (i + Dist_i >= 0 && i + Dist_i < ilast && j + Dist_j >= 0 && j + Dist_j < jlast && k + Dist_k >= 0 && k + Dist_k < klast && (!vector(stalled_1, i + Dist_i, j + Dist_j, k + Dist_k))) {
											if (Dist_i < 0 && Dist_j == 0 && Dist_k == 0 && fabs(Dist_i) < fabs(dist_i_tmp_m)) {
												dist_i_tmp_m = -Dist_i;
											}
											if (Dist_i > 0 && Dist_j == 0 && Dist_k == 0 && fabs(Dist_i) < fabs(dist_i_tmp_p)) {
												dist_i_tmp_p = -Dist_i;
											}
											if (Dist_j < 0 && Dist_i == 0 && Dist_k == 0 && fabs(Dist_j) < fabs(dist_j_tmp_m)) {
												dist_j_tmp_m = -Dist_j;
											}
											if (Dist_j > 0 && Dist_i == 0 && Dist_k == 0 && fabs(Dist_j) < fabs(dist_j_tmp_p)) {
												dist_j_tmp_p = -Dist_j;
											}
											if (Dist_k < 0 && Dist_i == 0 && Dist_j == 0 && fabs(Dist_k) < fabs(dist_k_tmp_m)) {
												dist_k_tmp_m = -Dist_k;
											}
											if (Dist_k > 0 && Dist_i == 0 && Dist_j == 0 && fabs(Dist_k) < fabs(dist_k_tmp_p)) {
												dist_k_tmp_p = -Dist_k;
											}
											dist_r_tmp = Dist_i * Dist_i + Dist_j * Dist_j + Dist_k * Dist_k;
											if (dist_r_tmp < dist_r) {
												dist_r = dist_r_tmp;
												dist_i_tmp_r = -Dist_i;
												dist_j_tmp_r = -Dist_j;
												dist_k_tmp_r = -Dist_k;
											}
										}
									}
								}
							}
							if (fabs(dist_i_tmp_m) == fabs(dist_i_tmp_p) && dist_i_tmp_m < 999) {
								dist_i_tmp = 0;
								bool enough = true;
								for (int Dist_i = 0; Dist_i <= 1 && enough; Dist_i++) {
									if (!(i - dist_i_tmp_p + Dist_i < ilast && (!vector(stalled_1, i - dist_i_tmp_p + Dist_i, j, k)))) {
										enough = false;
									}
								}
								if (enough) {
									dist_i_tmp = dist_i_tmp_p;
								}
								enough = true;
								for (int Dist_i = -1; Dist_i <= 0 && enough; Dist_i++) {
									if (!(i - dist_i_tmp_m + Dist_i >= 0 && (!vector(stalled_1, i - dist_i_tmp_m + Dist_i, j, k)))) {
										enough = false;
									}
								}
								if (enough) {
									dist_i_tmp = dist_i_tmp_m;
								}
							} else if (fabs(dist_i_tmp_m) < fabs(dist_i_tmp_p)) {
								dist_i_tmp = dist_i_tmp_m;
							} else {
								dist_i_tmp = dist_i_tmp_p;
							}
							if (fabs(dist_j_tmp_m) == fabs(dist_j_tmp_p) && dist_j_tmp_m < 999) {
								dist_j_tmp = 0;
								bool enough = true;
								for (int Dist_j = 0; Dist_j <= 1 && enough; Dist_j++) {
									if (!(j - dist_j_tmp_p + Dist_j < jlast && (!vector(stalled_1, i, j - dist_j_tmp_p + Dist_j, k)))) {
										enough = false;
									}
								}
								if (enough) {
									dist_j_tmp = dist_j_tmp_p;
								}
								enough = true;
								for (int Dist_j = -1; Dist_j <= 0 && enough; Dist_j++) {
									if (!(j - dist_j_tmp_m + Dist_j >= 0 && (!vector(stalled_1, i, j - dist_j_tmp_m + Dist_j, k)))) {
										enough = false;
									}
								}
								if (enough) {
									dist_j_tmp = dist_j_tmp_m;
								}
							} else if (fabs(dist_j_tmp_m) < fabs(dist_j_tmp_p)) {
								dist_j_tmp = dist_j_tmp_m;
							} else {
								dist_j_tmp = dist_j_tmp_p;
							}
							if (fabs(dist_k_tmp_m) == fabs(dist_k_tmp_p) && dist_k_tmp_m < 999) {
								dist_k_tmp = 0;
								bool enough = true;
								for (int Dist_k = 0; Dist_k <= 1 && enough; Dist_k++) {
									if (!(k - dist_k_tmp_p + Dist_k < klast && (!vector(stalled_1, i, j, k - dist_k_tmp_p + Dist_k)))) {
										enough = false;
									}
								}
								if (enough) {
									dist_k_tmp = dist_k_tmp_p;
								}
								enough = true;
								for (int Dist_k = -1; Dist_k <= 0 && enough; Dist_k++) {
									if (!(k - dist_k_tmp_m + Dist_k >= 0 && (!vector(stalled_1, i, j, k - dist_k_tmp_m + Dist_k)))) {
										enough = false;
									}
								}
								if (enough) {
									dist_k_tmp = dist_k_tmp_m;
								}
							} else if (fabs(dist_k_tmp_m) < fabs(dist_k_tmp_p)) {
								dist_k_tmp = dist_k_tmp_m;
							} else {
								dist_k_tmp = dist_k_tmp_p;
							}
							if (dist_i_tmp == 999 && dist_j_tmp == 999 && dist_k_tmp == 999) {
								dist_i_tmp = dist_i_tmp_r;
								dist_j_tmp = dist_j_tmp_r;
								dist_k_tmp = dist_k_tmp_r;
							}
							if (dist_i_tmp != 999 && ((vector(d_i_rho_mx_my_mz_p_fx_fy_fz, i, j, k) == 0) || (fabs(dist_i_tmp) < fabs(vector(d_i_rho_mx_my_mz_p_fx_fy_fz, i, j, k))))) {
								vector(d_i_rho_mx_my_mz_p_fx_fy_fz, i, j, k) = dist_i_tmp;
							}
							if (dist_j_tmp != 999 && ((vector(d_j_rho_mx_my_mz_p_fx_fy_fz, i, j, k) == 0) || (fabs(dist_j_tmp) < fabs(vector(d_j_rho_mx_my_mz_p_fx_fy_fz, i, j, k))))) {
								vector(d_j_rho_mx_my_mz_p_fx_fy_fz, i, j, k) = dist_j_tmp;
							}
							if (dist_k_tmp != 999 && ((vector(d_k_rho_mx_my_mz_p_fx_fy_fz, i, j, k) == 0) || (fabs(dist_k_tmp) < fabs(vector(d_k_rho_mx_my_mz_p_fx_fy_fz, i, j, k))))) {
								vector(d_k_rho_mx_my_mz_p_fx_fy_fz, i, j, k) = dist_k_tmp;
							}
						}
						if (vector(FOV_1, i, j, k) > 0) {
							dist_i_tmp = 999;
							dist_i_tmp_p = 999;
							dist_i_tmp_m = 999;
							dist_i_tmp_r = 999;
							dist_j_tmp = 999;
							dist_j_tmp_p = 999;
							dist_j_tmp_m = 999;
							dist_j_tmp_r = 999;
							dist_k_tmp = 999;
							dist_k_tmp_p = 999;
							dist_k_tmp_m = 999;
							dist_k_tmp_r = 999;
							dist_r = 999;
							for (int Dist_i = -2 * reGrid_i; Dist_i <= 2 * reGrid_i; Dist_i++) {
								for (int Dist_j = -2 * reGrid_j; Dist_j <= 2 * reGrid_j; Dist_j++) {
									for (int Dist_k = -2 * reGrid_k; Dist_k <= 2 * reGrid_k; Dist_k++) {
										if (i + Dist_i >= 0 && i + Dist_i < ilast && j + Dist_j >= 0 && j + Dist_j < jlast && k + Dist_k >= 0 && k + Dist_k < klast && (!vector(stalled_3, i + Dist_i, j + Dist_j, k + Dist_k))) {
											if (Dist_i < 0 && Dist_j == 0 && Dist_k == 0 && fabs(Dist_i) < fabs(dist_i_tmp_m)) {
												dist_i_tmp_m = -Dist_i;
											}
											if (Dist_i > 0 && Dist_j == 0 && Dist_k == 0 && fabs(Dist_i) < fabs(dist_i_tmp_p)) {
												dist_i_tmp_p = -Dist_i;
											}
											if (Dist_j < 0 && Dist_i == 0 && Dist_k == 0 && fabs(Dist_j) < fabs(dist_j_tmp_m)) {
												dist_j_tmp_m = -Dist_j;
											}
											if (Dist_j > 0 && Dist_i == 0 && Dist_k == 0 && fabs(Dist_j) < fabs(dist_j_tmp_p)) {
												dist_j_tmp_p = -Dist_j;
											}
											if (Dist_k < 0 && Dist_i == 0 && Dist_j == 0 && fabs(Dist_k) < fabs(dist_k_tmp_m)) {
												dist_k_tmp_m = -Dist_k;
											}
											if (Dist_k > 0 && Dist_i == 0 && Dist_j == 0 && fabs(Dist_k) < fabs(dist_k_tmp_p)) {
												dist_k_tmp_p = -Dist_k;
											}
											dist_r_tmp = Dist_i * Dist_i + Dist_j * Dist_j + Dist_k * Dist_k;
											if (dist_r_tmp < dist_r) {
												dist_r = dist_r_tmp;
												dist_i_tmp_r = -Dist_i;
												dist_j_tmp_r = -Dist_j;
												dist_k_tmp_r = -Dist_k;
											}
										}
									}
								}
							}
							if (fabs(dist_i_tmp_m) == fabs(dist_i_tmp_p) && dist_i_tmp_m < 999) {
								dist_i_tmp = 0;
								bool enough = true;
								for (int Dist_i = 0; Dist_i <= 1 && enough; Dist_i++) {
									if (!(i - dist_i_tmp_p + Dist_i < ilast && (!vector(stalled_3, i - dist_i_tmp_p + Dist_i, j, k)))) {
										enough = false;
									}
								}
								if (enough) {
									dist_i_tmp = dist_i_tmp_p;
								}
								enough = true;
								for (int Dist_i = -1; Dist_i <= 0 && enough; Dist_i++) {
									if (!(i - dist_i_tmp_m + Dist_i >= 0 && (!vector(stalled_3, i - dist_i_tmp_m + Dist_i, j, k)))) {
										enough = false;
									}
								}
								if (enough) {
									dist_i_tmp = dist_i_tmp_m;
								}
							} else if (fabs(dist_i_tmp_m) < fabs(dist_i_tmp_p)) {
								dist_i_tmp = dist_i_tmp_m;
							} else {
								dist_i_tmp = dist_i_tmp_p;
							}
							if (fabs(dist_j_tmp_m) == fabs(dist_j_tmp_p) && dist_j_tmp_m < 999) {
								dist_j_tmp = 0;
								bool enough = true;
								for (int Dist_j = 0; Dist_j <= 1 && enough; Dist_j++) {
									if (!(j - dist_j_tmp_p + Dist_j < jlast && (!vector(stalled_3, i, j - dist_j_tmp_p + Dist_j, k)))) {
										enough = false;
									}
								}
								if (enough) {
									dist_j_tmp = dist_j_tmp_p;
								}
								enough = true;
								for (int Dist_j = -1; Dist_j <= 0 && enough; Dist_j++) {
									if (!(j - dist_j_tmp_m + Dist_j >= 0 && (!vector(stalled_3, i, j - dist_j_tmp_m + Dist_j, k)))) {
										enough = false;
									}
								}
								if (enough) {
									dist_j_tmp = dist_j_tmp_m;
								}
							} else if (fabs(dist_j_tmp_m) < fabs(dist_j_tmp_p)) {
								dist_j_tmp = dist_j_tmp_m;
							} else {
								dist_j_tmp = dist_j_tmp_p;
							}
							if (fabs(dist_k_tmp_m) == fabs(dist_k_tmp_p) && dist_k_tmp_m < 999) {
								dist_k_tmp = 0;
								bool enough = true;
								for (int Dist_k = 0; Dist_k <= 1 && enough; Dist_k++) {
									if (!(k - dist_k_tmp_p + Dist_k < klast && (!vector(stalled_3, i, j, k - dist_k_tmp_p + Dist_k)))) {
										enough = false;
									}
								}
								if (enough) {
									dist_k_tmp = dist_k_tmp_p;
								}
								enough = true;
								for (int Dist_k = -1; Dist_k <= 0 && enough; Dist_k++) {
									if (!(k - dist_k_tmp_m + Dist_k >= 0 && (!vector(stalled_3, i, j, k - dist_k_tmp_m + Dist_k)))) {
										enough = false;
									}
								}
								if (enough) {
									dist_k_tmp = dist_k_tmp_m;
								}
							} else if (fabs(dist_k_tmp_m) < fabs(dist_k_tmp_p)) {
								dist_k_tmp = dist_k_tmp_m;
							} else {
								dist_k_tmp = dist_k_tmp_p;
							}
							if (dist_i_tmp == 999 && dist_j_tmp == 999 && dist_k_tmp == 999) {
								dist_i_tmp = dist_i_tmp_r;
								dist_j_tmp = dist_j_tmp_r;
								dist_k_tmp = dist_k_tmp_r;
							}
							if (dist_i_tmp != 999 && ((vector(d_i_Null, i, j, k) == 0) || (fabs(dist_i_tmp) < fabs(vector(d_i_Null, i, j, k))))) {
								vector(d_i_Null, i, j, k) = dist_i_tmp;
							}
							if (dist_j_tmp != 999 && ((vector(d_j_Null, i, j, k) == 0) || (fabs(dist_j_tmp) < fabs(vector(d_j_Null, i, j, k))))) {
								vector(d_j_Null, i, j, k) = dist_j_tmp;
							}
							if (dist_k_tmp != 999 && ((vector(d_k_Null, i, j, k) == 0) || (fabs(dist_k_tmp) < fabs(vector(d_k_Null, i, j, k))))) {
								vector(d_k_Null, i, j, k) = dist_k_tmp;
							}
						}
						if (vector(FOV_xUpper, i, j, k) > 0) {
							dist_i_tmp = 999;
							dist_i_tmp_p = 999;
							dist_i_tmp_m = 999;
							dist_i_tmp_r = 999;
							dist_j_tmp = 999;
							dist_j_tmp_p = 999;
							dist_j_tmp_m = 999;
							dist_j_tmp_r = 999;
							dist_k_tmp = 999;
							dist_k_tmp_p = 999;
							dist_k_tmp_m = 999;
							dist_k_tmp_r = 999;
							dist_r = 999;
							for (int Dist_i = -2 * reGrid_i; Dist_i <= 2 * reGrid_i; Dist_i++) {
								for (int Dist_j = -2 * reGrid_j; Dist_j <= 2 * reGrid_j; Dist_j++) {
									for (int Dist_k = -2 * reGrid_k; Dist_k <= 2 * reGrid_k; Dist_k++) {
										if (i + Dist_i >= 0 && i + Dist_i < ilast && j + Dist_j >= 0 && j + Dist_j < jlast && k + Dist_k >= 0 && k + Dist_k < klast && (!vector(stalled_1, i + Dist_i, j + Dist_j, k + Dist_k))) {
											if (Dist_i < 0 && Dist_j == 0 && Dist_k == 0 && fabs(Dist_i) < fabs(dist_i_tmp_m)) {
												dist_i_tmp_m = -Dist_i;
											}
											if (Dist_i > 0 && Dist_j == 0 && Dist_k == 0 && fabs(Dist_i) < fabs(dist_i_tmp_p)) {
												dist_i_tmp_p = -Dist_i;
											}
											if (Dist_j < 0 && Dist_i == 0 && Dist_k == 0 && fabs(Dist_j) < fabs(dist_j_tmp_m)) {
												dist_j_tmp_m = -Dist_j;
											}
											if (Dist_j > 0 && Dist_i == 0 && Dist_k == 0 && fabs(Dist_j) < fabs(dist_j_tmp_p)) {
												dist_j_tmp_p = -Dist_j;
											}
											if (Dist_k < 0 && Dist_i == 0 && Dist_j == 0 && fabs(Dist_k) < fabs(dist_k_tmp_m)) {
												dist_k_tmp_m = -Dist_k;
											}
											if (Dist_k > 0 && Dist_i == 0 && Dist_j == 0 && fabs(Dist_k) < fabs(dist_k_tmp_p)) {
												dist_k_tmp_p = -Dist_k;
											}
											dist_r_tmp = Dist_i * Dist_i + Dist_j * Dist_j + Dist_k * Dist_k;
											if (dist_r_tmp < dist_r) {
												dist_r = dist_r_tmp;
												dist_i_tmp_r = -Dist_i;
												dist_j_tmp_r = -Dist_j;
												dist_k_tmp_r = -Dist_k;
											}
										}
									}
								}
							}
							if (fabs(dist_i_tmp_m) == fabs(dist_i_tmp_p) && dist_i_tmp_m < 999) {
								dist_i_tmp = 0;
								bool enough = true;
								for (int Dist_i = 0; Dist_i <= 1 && enough; Dist_i++) {
									if (!(i - dist_i_tmp_p + Dist_i < ilast && (!vector(stalled_1, i - dist_i_tmp_p + Dist_i, j, k)))) {
										enough = false;
									}
								}
								if (enough) {
									dist_i_tmp = dist_i_tmp_p;
								}
								enough = true;
								for (int Dist_i = -1; Dist_i <= 0 && enough; Dist_i++) {
									if (!(i - dist_i_tmp_m + Dist_i >= 0 && (!vector(stalled_1, i - dist_i_tmp_m + Dist_i, j, k)))) {
										enough = false;
									}
								}
								if (enough) {
									dist_i_tmp = dist_i_tmp_m;
								}
							} else if (fabs(dist_i_tmp_m) < fabs(dist_i_tmp_p)) {
								dist_i_tmp = dist_i_tmp_m;
							} else {
								dist_i_tmp = dist_i_tmp_p;
							}
							if (fabs(dist_j_tmp_m) == fabs(dist_j_tmp_p) && dist_j_tmp_m < 999) {
								dist_j_tmp = 0;
								bool enough = true;
								for (int Dist_j = 0; Dist_j <= 1 && enough; Dist_j++) {
									if (!(j - dist_j_tmp_p + Dist_j < jlast && (!vector(stalled_1, i, j - dist_j_tmp_p + Dist_j, k)))) {
										enough = false;
									}
								}
								if (enough) {
									dist_j_tmp = dist_j_tmp_p;
								}
								enough = true;
								for (int Dist_j = -1; Dist_j <= 0 && enough; Dist_j++) {
									if (!(j - dist_j_tmp_m + Dist_j >= 0 && (!vector(stalled_1, i, j - dist_j_tmp_m + Dist_j, k)))) {
										enough = false;
									}
								}
								if (enough) {
									dist_j_tmp = dist_j_tmp_m;
								}
							} else if (fabs(dist_j_tmp_m) < fabs(dist_j_tmp_p)) {
								dist_j_tmp = dist_j_tmp_m;
							} else {
								dist_j_tmp = dist_j_tmp_p;
							}
							if (fabs(dist_k_tmp_m) == fabs(dist_k_tmp_p) && dist_k_tmp_m < 999) {
								dist_k_tmp = 0;
								bool enough = true;
								for (int Dist_k = 0; Dist_k <= 1 && enough; Dist_k++) {
									if (!(k - dist_k_tmp_p + Dist_k < klast && (!vector(stalled_1, i, j, k - dist_k_tmp_p + Dist_k)))) {
										enough = false;
									}
								}
								if (enough) {
									dist_k_tmp = dist_k_tmp_p;
								}
								enough = true;
								for (int Dist_k = -1; Dist_k <= 0 && enough; Dist_k++) {
									if (!(k - dist_k_tmp_m + Dist_k >= 0 && (!vector(stalled_1, i, j, k - dist_k_tmp_m + Dist_k)))) {
										enough = false;
									}
								}
								if (enough) {
									dist_k_tmp = dist_k_tmp_m;
								}
							} else if (fabs(dist_k_tmp_m) < fabs(dist_k_tmp_p)) {
								dist_k_tmp = dist_k_tmp_m;
							} else {
								dist_k_tmp = dist_k_tmp_p;
							}
							if (dist_i_tmp == 999 && dist_j_tmp == 999 && dist_k_tmp == 999) {
								dist_i_tmp = dist_i_tmp_r;
								dist_j_tmp = dist_j_tmp_r;
								dist_k_tmp = dist_k_tmp_r;
							}
							if (dist_i_tmp != 999 && ((vector(d_i_rho_mx_my_mz_p_fx_fy_fz, i, j, k) == 0) || (fabs(dist_i_tmp) < fabs(vector(d_i_rho_mx_my_mz_p_fx_fy_fz, i, j, k))))) {
								vector(d_i_rho_mx_my_mz_p_fx_fy_fz, i, j, k) = dist_i_tmp;
							}
							if (dist_j_tmp != 999 && ((vector(d_j_rho_mx_my_mz_p_fx_fy_fz, i, j, k) == 0) || (fabs(dist_j_tmp) < fabs(vector(d_j_rho_mx_my_mz_p_fx_fy_fz, i, j, k))))) {
								vector(d_j_rho_mx_my_mz_p_fx_fy_fz, i, j, k) = dist_j_tmp;
							}
							if (dist_k_tmp != 999 && ((vector(d_k_rho_mx_my_mz_p_fx_fy_fz, i, j, k) == 0) || (fabs(dist_k_tmp) < fabs(vector(d_k_rho_mx_my_mz_p_fx_fy_fz, i, j, k))))) {
								vector(d_k_rho_mx_my_mz_p_fx_fy_fz, i, j, k) = dist_k_tmp;
							}
						}
						if (vector(FOV_xLower, i, j, k) > 0) {
							dist_i_tmp = 999;
							dist_i_tmp_p = 999;
							dist_i_tmp_m = 999;
							dist_i_tmp_r = 999;
							dist_j_tmp = 999;
							dist_j_tmp_p = 999;
							dist_j_tmp_m = 999;
							dist_j_tmp_r = 999;
							dist_k_tmp = 999;
							dist_k_tmp_p = 999;
							dist_k_tmp_m = 999;
							dist_k_tmp_r = 999;
							dist_r = 999;
							for (int Dist_i = -2 * reGrid_i; Dist_i <= 2 * reGrid_i; Dist_i++) {
								for (int Dist_j = -2 * reGrid_j; Dist_j <= 2 * reGrid_j; Dist_j++) {
									for (int Dist_k = -2 * reGrid_k; Dist_k <= 2 * reGrid_k; Dist_k++) {
										if (i + Dist_i >= 0 && i + Dist_i < ilast && j + Dist_j >= 0 && j + Dist_j < jlast && k + Dist_k >= 0 && k + Dist_k < klast && (!vector(stalled_1, i + Dist_i, j + Dist_j, k + Dist_k))) {
											if (Dist_i < 0 && Dist_j == 0 && Dist_k == 0 && fabs(Dist_i) < fabs(dist_i_tmp_m)) {
												dist_i_tmp_m = -Dist_i;
											}
											if (Dist_i > 0 && Dist_j == 0 && Dist_k == 0 && fabs(Dist_i) < fabs(dist_i_tmp_p)) {
												dist_i_tmp_p = -Dist_i;
											}
											if (Dist_j < 0 && Dist_i == 0 && Dist_k == 0 && fabs(Dist_j) < fabs(dist_j_tmp_m)) {
												dist_j_tmp_m = -Dist_j;
											}
											if (Dist_j > 0 && Dist_i == 0 && Dist_k == 0 && fabs(Dist_j) < fabs(dist_j_tmp_p)) {
												dist_j_tmp_p = -Dist_j;
											}
											if (Dist_k < 0 && Dist_i == 0 && Dist_j == 0 && fabs(Dist_k) < fabs(dist_k_tmp_m)) {
												dist_k_tmp_m = -Dist_k;
											}
											if (Dist_k > 0 && Dist_i == 0 && Dist_j == 0 && fabs(Dist_k) < fabs(dist_k_tmp_p)) {
												dist_k_tmp_p = -Dist_k;
											}
											dist_r_tmp = Dist_i * Dist_i + Dist_j * Dist_j + Dist_k * Dist_k;
											if (dist_r_tmp < dist_r) {
												dist_r = dist_r_tmp;
												dist_i_tmp_r = -Dist_i;
												dist_j_tmp_r = -Dist_j;
												dist_k_tmp_r = -Dist_k;
											}
										}
									}
								}
							}
							if (fabs(dist_i_tmp_m) == fabs(dist_i_tmp_p) && dist_i_tmp_m < 999) {
								dist_i_tmp = 0;
								bool enough = true;
								for (int Dist_i = 0; Dist_i <= 1 && enough; Dist_i++) {
									if (!(i - dist_i_tmp_p + Dist_i < ilast && (!vector(stalled_1, i - dist_i_tmp_p + Dist_i, j, k)))) {
										enough = false;
									}
								}
								if (enough) {
									dist_i_tmp = dist_i_tmp_p;
								}
								enough = true;
								for (int Dist_i = -1; Dist_i <= 0 && enough; Dist_i++) {
									if (!(i - dist_i_tmp_m + Dist_i >= 0 && (!vector(stalled_1, i - dist_i_tmp_m + Dist_i, j, k)))) {
										enough = false;
									}
								}
								if (enough) {
									dist_i_tmp = dist_i_tmp_m;
								}
							} else if (fabs(dist_i_tmp_m) < fabs(dist_i_tmp_p)) {
								dist_i_tmp = dist_i_tmp_m;
							} else {
								dist_i_tmp = dist_i_tmp_p;
							}
							if (fabs(dist_j_tmp_m) == fabs(dist_j_tmp_p) && dist_j_tmp_m < 999) {
								dist_j_tmp = 0;
								bool enough = true;
								for (int Dist_j = 0; Dist_j <= 1 && enough; Dist_j++) {
									if (!(j - dist_j_tmp_p + Dist_j < jlast && (!vector(stalled_1, i, j - dist_j_tmp_p + Dist_j, k)))) {
										enough = false;
									}
								}
								if (enough) {
									dist_j_tmp = dist_j_tmp_p;
								}
								enough = true;
								for (int Dist_j = -1; Dist_j <= 0 && enough; Dist_j++) {
									if (!(j - dist_j_tmp_m + Dist_j >= 0 && (!vector(stalled_1, i, j - dist_j_tmp_m + Dist_j, k)))) {
										enough = false;
									}
								}
								if (enough) {
									dist_j_tmp = dist_j_tmp_m;
								}
							} else if (fabs(dist_j_tmp_m) < fabs(dist_j_tmp_p)) {
								dist_j_tmp = dist_j_tmp_m;
							} else {
								dist_j_tmp = dist_j_tmp_p;
							}
							if (fabs(dist_k_tmp_m) == fabs(dist_k_tmp_p) && dist_k_tmp_m < 999) {
								dist_k_tmp = 0;
								bool enough = true;
								for (int Dist_k = 0; Dist_k <= 1 && enough; Dist_k++) {
									if (!(k - dist_k_tmp_p + Dist_k < klast && (!vector(stalled_1, i, j, k - dist_k_tmp_p + Dist_k)))) {
										enough = false;
									}
								}
								if (enough) {
									dist_k_tmp = dist_k_tmp_p;
								}
								enough = true;
								for (int Dist_k = -1; Dist_k <= 0 && enough; Dist_k++) {
									if (!(k - dist_k_tmp_m + Dist_k >= 0 && (!vector(stalled_1, i, j, k - dist_k_tmp_m + Dist_k)))) {
										enough = false;
									}
								}
								if (enough) {
									dist_k_tmp = dist_k_tmp_m;
								}
							} else if (fabs(dist_k_tmp_m) < fabs(dist_k_tmp_p)) {
								dist_k_tmp = dist_k_tmp_m;
							} else {
								dist_k_tmp = dist_k_tmp_p;
							}
							if (dist_i_tmp == 999 && dist_j_tmp == 999 && dist_k_tmp == 999) {
								dist_i_tmp = dist_i_tmp_r;
								dist_j_tmp = dist_j_tmp_r;
								dist_k_tmp = dist_k_tmp_r;
							}
							if (dist_i_tmp != 999 && ((vector(d_i_rho_mx_my_mz_p_fx_fy_fz, i, j, k) == 0) || (fabs(dist_i_tmp) < fabs(vector(d_i_rho_mx_my_mz_p_fx_fy_fz, i, j, k))))) {
								vector(d_i_rho_mx_my_mz_p_fx_fy_fz, i, j, k) = dist_i_tmp;
							}
							if (dist_j_tmp != 999 && ((vector(d_j_rho_mx_my_mz_p_fx_fy_fz, i, j, k) == 0) || (fabs(dist_j_tmp) < fabs(vector(d_j_rho_mx_my_mz_p_fx_fy_fz, i, j, k))))) {
								vector(d_j_rho_mx_my_mz_p_fx_fy_fz, i, j, k) = dist_j_tmp;
							}
							if (dist_k_tmp != 999 && ((vector(d_k_rho_mx_my_mz_p_fx_fy_fz, i, j, k) == 0) || (fabs(dist_k_tmp) < fabs(vector(d_k_rho_mx_my_mz_p_fx_fy_fz, i, j, k))))) {
								vector(d_k_rho_mx_my_mz_p_fx_fy_fz, i, j, k) = dist_k_tmp;
							}
						}
					}
				}
			}
		}
		d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);
	}
}


/*
 * Checks if the point has to be stalled
 */
bool Problem::checkStalled(std::shared_ptr< hier::Patch > patch, int i, int j, int k, int v) const {
	double* FOV = ((pdat::NodeData<double> *) patch->getPatchData(v).get())->getPointer();
	double* interior = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_regridding_value_id).get())->getPointer();
	//Get the dimensions of the patch
	const hier::Index boxfirst = patch->getBox().lower();
	const hier::Index boxlast  = patch->getBox().upper();
	//Auxiliary definitions
	int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
	int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
	int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;

	int stencilAcc, stencilAccMax_i, stencilAccMax_j, stencilAccMax_k;
	bool notEnoughStencil = false;
	int FOV_threshold = 0;
	if (vector(FOV, i, j, k) <= FOV_threshold) {
		notEnoughStencil = true;
	} else {
		stencilAcc = 0;
		stencilAccMax_i = 0;
		for (int it1 = MAX(i-d_regionMinThickness, 0); it1 <= MIN(i+d_regionMinThickness, ilast - 1); it1++) {
			if (vector(FOV, it1, j, k) > FOV_threshold) {
				stencilAcc++;
			} else {
				stencilAccMax_i = MAX(stencilAccMax_i, stencilAcc);
				stencilAcc = 0;
			}
		}
		stencilAccMax_i = MAX(stencilAccMax_i, stencilAcc);
		stencilAcc = 0;
		stencilAccMax_j = 0;
		for (int jt1 = MAX(j-d_regionMinThickness, 0); jt1 <= MIN(j+d_regionMinThickness, jlast - 1); jt1++) {
			if (vector(FOV, i, jt1, k) > FOV_threshold) {
				stencilAcc++;
			} else {
				stencilAccMax_j = MAX(stencilAccMax_j, stencilAcc);
				stencilAcc = 0;
			}
		}
		stencilAccMax_j = MAX(stencilAccMax_j, stencilAcc);
		stencilAcc = 0;
		stencilAccMax_k = 0;
		for (int kt1 = MAX(k-d_regionMinThickness, 0); kt1 <= MIN(k+d_regionMinThickness, klast - 1); kt1++) {
			if (vector(FOV, i, j, kt1) > FOV_threshold) {
				stencilAcc++;
			} else {
				stencilAccMax_k = MAX(stencilAccMax_k, stencilAcc);
				stencilAcc = 0;
			}
		}
		stencilAccMax_k = MAX(stencilAccMax_k, stencilAcc);
		if ((stencilAccMax_i < d_regionMinThickness) || (stencilAccMax_j < d_regionMinThickness) || (stencilAccMax_k < d_regionMinThickness)) {
			notEnoughStencil = true;
		}
	}
	return notEnoughStencil;
}






/*
 * Initialize data on a patch. This initialization is done only at the begining of the simulation.
 */
void Problem::initializeDataOnPatch(hier::Patch& patch, 
                                    const double time,
                                    const bool initial_time)
{
	(void) time;
   	if (initial_time) {
		// Initial conditions		
		//Get fields, auxiliary fields and local variables that are going to be used.
		double* rho = ((pdat::NodeData<double> *) patch.getPatchData(d_rho_id).get())->getPointer();
		((pdat::NodeData<double> *) patch.getPatchData(d_rho_id).get())->fillAll(0);
		double* mx = ((pdat::NodeData<double> *) patch.getPatchData(d_mx_id).get())->getPointer();
		((pdat::NodeData<double> *) patch.getPatchData(d_mx_id).get())->fillAll(0);
		double* my = ((pdat::NodeData<double> *) patch.getPatchData(d_my_id).get())->getPointer();
		((pdat::NodeData<double> *) patch.getPatchData(d_my_id).get())->fillAll(0);
		double* mz = ((pdat::NodeData<double> *) patch.getPatchData(d_mz_id).get())->getPointer();
		((pdat::NodeData<double> *) patch.getPatchData(d_mz_id).get())->fillAll(0);
		double* Null = ((pdat::NodeData<double> *) patch.getPatchData(d_Null_id).get())->getPointer();
		((pdat::NodeData<double> *) patch.getPatchData(d_Null_id).get())->fillAll(0);
		double* p = ((pdat::NodeData<double> *) patch.getPatchData(d_p_id).get())->getPointer();
		((pdat::NodeData<double> *) patch.getPatchData(d_p_id).get())->fillAll(0);
		double* fx = ((pdat::NodeData<double> *) patch.getPatchData(d_fx_id).get())->getPointer();
		((pdat::NodeData<double> *) patch.getPatchData(d_fx_id).get())->fillAll(0);
		double* fy = ((pdat::NodeData<double> *) patch.getPatchData(d_fy_id).get())->getPointer();
		((pdat::NodeData<double> *) patch.getPatchData(d_fy_id).get())->fillAll(0);
		double* fz = ((pdat::NodeData<double> *) patch.getPatchData(d_fz_id).get())->getPointer();
		((pdat::NodeData<double> *) patch.getPatchData(d_fz_id).get())->fillAll(0);
		
		//Get the dimensions of the patch
		hier::Box pbox = patch.getBox();
		double* FOV_1 = ((pdat::NodeData<double> *) patch.getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_3 = ((pdat::NodeData<double> *) patch.getPatchData(d_FOV_3_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch.getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch.getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch.getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch.getPatchData(d_FOV_yUpper_id).get())->getPointer();
		double* FOV_zLower = ((pdat::NodeData<double> *) patch.getPatchData(d_FOV_zLower_id).get())->getPointer();
		double* FOV_zUpper = ((pdat::NodeData<double> *) patch.getPatchData(d_FOV_zUpper_id).get())->getPointer();
		const hier::Index boxfirst = patch.getBox().lower();
		const hier::Index boxlast  = patch.getBox().upper();
		
		//Get delta spaces into an array. dx, dy, dz.
		const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch.getPatchGeometry()));
		const double* dx  = patch_geom->getDx();
		
		int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
		int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
		int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;
		for(int k = 0; k < klast; k++) {
			for(int j = 0; j < jlast; j++) {
				for(int i = 0; i < ilast; i++) {
					if (vector(FOV_3, i, j, k) > 0) {
						vector(Null, i, j, k) = 0.0;
					}
		
					if (vector(FOV_1, i, j, k) > 0) {
						vector(rho, i, j, k) = rho0;
						vector(mx, i, j, k) = 0.0;
						vector(my, i, j, k) = 0.0;
						vector(mz, i, j, k) = 0.0;
						vector(fx, i, j, k) = pfx;
						vector(fy, i, j, k) = pfy;
						vector(fz, i, j, k) = pfz;
						vector(p, i, j, k) = Paux;
					}
		
				}
			}
		}
		

   	}
}



/*
 * Gets the coarser patch that contains the box
 */
const std::shared_ptr<hier::Patch >& Problem::getCoarserPatch(
	const std::shared_ptr< hier::PatchLevel >& level,
	const hier::Box interior, 
	const hier::IntVector ratio)
{
	const hier::Box& coarsenBox = hier::Box::coarsen(interior, ratio);
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr< hier::Patch >& patch = *p_it;
		const hier::Box& interior_C = patch->getBox();
		if (interior_C.intersects(coarsenBox)) {
			return patch;		
		}
	}
    return NULL;
}

/*
 * Reset the hierarchy-dependent internal information.
 */
void Problem::resetHierarchyConfiguration (
   const std::shared_ptr<hier::PatchHierarchy >& new_hierarchy ,
   int coarsest_level ,
   int finest_level )
{
	int finest_hiera_level = new_hierarchy->getFinestLevelNumber();

   	//  If we have added or removed a level, resize the schedule arrays

	d_bdry_sched_advance1.resize(finest_hiera_level+1);
	d_bdry_sched_advance3.resize(finest_hiera_level+1);
	d_bdry_sched_advance9.resize(finest_hiera_level+1);
	d_bdry_sched_advance11.resize(finest_hiera_level+1);
	d_bdry_sched_advance17.resize(finest_hiera_level+1);
	d_bdry_sched_advance19.resize(finest_hiera_level+1);
	d_coarsen_schedule.resize(finest_hiera_level+1);
	d_bdry_sched_postCoarsen.resize(finest_hiera_level+1);
	//  Build coarsen and refine communication schedules.
	for (int ln = coarsest_level; ln <= finest_hiera_level; ln++) {
		std::shared_ptr< hier::PatchLevel > level(new_hierarchy->getPatchLevel(ln));
		d_bdry_sched_advance1[ln] = d_bdry_fill_advance1->createSchedule(level,ln-1,new_hierarchy,this);
		d_bdry_sched_advance3[ln] = d_bdry_fill_advance3->createSchedule(level,ln-1,new_hierarchy,this);
		d_bdry_sched_advance9[ln] = d_bdry_fill_advance9->createSchedule(level,ln-1,new_hierarchy,this);
		d_bdry_sched_advance11[ln] = d_bdry_fill_advance11->createSchedule(level,ln-1,new_hierarchy,this);
		d_bdry_sched_advance17[ln] = d_bdry_fill_advance17->createSchedule(level,ln-1,new_hierarchy,this);
		d_bdry_sched_advance19[ln] = d_bdry_fill_advance19->createSchedule(level,ln-1,new_hierarchy,this);
		d_bdry_sched_postCoarsen[ln] = d_bdry_post_coarsen->createSchedule(level);
		// coarsen schedule only for levels > 0
		if (ln > 0) {
			std::shared_ptr< hier::PatchLevel > coarser_level(new_hierarchy->getPatchLevel(ln-1));
			d_coarsen_schedule[ln] = d_coarsen_algorithm->createSchedule(coarser_level, level, NULL);
		}
	}

}

/* 
 * Calculation of auxiliary terms after a new level is regridded.
 */
void Problem::postNewLevel(const std::shared_ptr< hier::PatchLevel >& level) {
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr< hier::Patch >& patch = *p_it;
		double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_3 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_3_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
		double* FOV_zLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zLower_id).get())->getPointer();
		double* FOV_zUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zUpper_id).get())->getPointer();
		double* fz = ((pdat::NodeData<double> *) patch->getPatchData(d_fz_id).get())->getPointer();
		double* fy = ((pdat::NodeData<double> *) patch->getPatchData(d_fy_id).get())->getPointer();
		double* fx = ((pdat::NodeData<double> *) patch->getPatchData(d_fx_id).get())->getPointer();
		double* p = ((pdat::NodeData<double> *) patch->getPatchData(d_p_id).get())->getPointer();
		double* rho = ((pdat::NodeData<double> *) patch->getPatchData(d_rho_id).get())->getPointer();

		//Get the dimensions of the patch
		const hier::Index boxfirst = patch->getBox().lower();
		const hier::Index boxlast = patch->getBox().upper();
		//Get delta spaces into an array. dx, dy, dz.
		std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
		const double* dx  = patch_geom->getDx();
		const double simPlat_dt = patch->getPatchData(d_FOV_1_id)->getTime();

		//Auxiliary definitions
		int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
		int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
		int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;
		for(int k = 0; k < klast; k++) {
			for(int j = 0; j < jlast; j++) {
				for(int i = 0; i < ilast; i++) {
					if ((vector(FOV_1, i, j, k) > 0)) {
						vector(fz, i, j, k) = pfz;
						vector(fy, i, j, k) = pfy;
						vector(fx, i, j, k) = pfx;
						vector(p, i, j, k) = (Paux + (pow((vector(rho, i, j, k) / rho0), gamma)) * (rho0 * (c0 * c0)) / gamma) - (rho0 * (c0 * c0)) / gamma;
					}
				}
			}
		}
	}
}

/*
 * This method sets the physical boundary conditions.
 */
void Problem::setPhysicalBoundaryConditions(
   hier::Patch& patch,
   const double fill_time,
   const hier::IntVector& ghost_width_to_fill)
{
	//Boundary must not be implemented in this method
}

/*
 * Set up external plotter to plot internal data from this class.
 * Register variables appropriate for plotting.
 */
int Problem::setupPlotterMesh(appu::VisItDataWriter &plotter) const {
	if (!d_patch_hierarchy) {
		TBOX_ERROR(d_object_name << ": No hierarchy inn"
			<< " Problem::setupPlottern"
			<< "The hierarchy must be set before callingn"
			<< "this function.n");
	}
	plotter.registerPlotQuantity("FOV_1","SCALAR",d_FOV_1_id);
	plotter.registerPlotQuantity("FOV_3","SCALAR",d_FOV_3_id);
	hier::VariableDatabase *vdb = hier::VariableDatabase::getDatabase();
	for (set<string>::const_iterator it = d_full_mesh_writer_variables.begin() ; it != d_full_mesh_writer_variables.end(); ++it) {
		string var_to_register = *it;
		if (!(vdb->checkVariableExists(var_to_register))) {
			TBOX_ERROR(d_object_name << ": Variable selected for 3D write not found:" <<  var_to_register);
		}
		int var_id = vdb->getVariable(var_to_register)->getInstanceIdentifier();
		plotter.registerPlotQuantity(var_to_register,"SCALAR",var_id);
	}
	return 0;
}
/*
 * Set up external plotter to plot sliced data from this class.
 * Register variables appropriate for plotting.
 */
int Problem::setupSlicePlotter(vector<std::shared_ptr<SlicerDataWriter> > &plotters) const {
	if (!d_patch_hierarchy) {
	TBOX_ERROR(d_object_name << ": No hierarchy in\n"
		<< " Problem::setupSlicePlotter\n"
		<< "The hierarchy must be set before calling\n"
		<< "this function.\n");
	}
	int i = 0;
	for (vector<std::shared_ptr<SlicerDataWriter> >::const_iterator it = plotters.begin(); it != plotters.end(); ++it) {
		std::shared_ptr<SlicerDataWriter> plotter = *it;
		plotter->registerPlotQuantity("FOV_1","SCALAR",d_FOV_1_id);
		plotter->registerPlotQuantity("FOV_3","SCALAR",d_FOV_3_id);
		hier::VariableDatabase *vdb = hier::VariableDatabase::getDatabase();
		set<string> variables = d_sliceVariables[i];
		for (set<string>::const_iterator it2 = variables.begin() ; it2 != variables.end(); ++it2) {
			string var_to_register = *it2;
			if (!(vdb->checkVariableExists(var_to_register))) {
				TBOX_ERROR(d_object_name << ": Variable selected for Slice not found:" <<  var_to_register);
			}
			int var_id = vdb->getVariable(var_to_register)->getInstanceIdentifier();
			plotter->registerPlotQuantity(var_to_register,"SCALAR",var_id);
		}
		i++;
	}

	return 0;
}

/*
 * Set up external plotter to plot spherical data from this class.
 * Register variables appropriate for plotting.
 */
int Problem::setupSpherePlotter(vector<std::shared_ptr<SphereDataWriter> > &plotters) const {
	if (!d_patch_hierarchy) {
	TBOX_ERROR(d_object_name << ": No hierarchy in\n"
		<< " Problem::setupSpherePlotter\n"
		<< "The hierarchy must be set before calling\n"
		<< "this function.\n");
	}
	int i = 0;
	for (vector<std::shared_ptr<SphereDataWriter> >::const_iterator it = plotters.begin(); it != plotters.end(); ++it) {
		std::shared_ptr<SphereDataWriter> plotter = *it;
		plotter->registerPlotQuantity("FOV_1","SCALAR",d_FOV_1_id);
		plotter->registerPlotQuantity("FOV_3","SCALAR",d_FOV_3_id);
		hier::VariableDatabase *vdb = hier::VariableDatabase::getDatabase();
		set<string> variables = d_sphereVariables[i];
		for (set<string>::const_iterator it2 = variables.begin() ; it2 != variables.end(); ++it2) {
			string var_to_register = *it2;
			if (!(vdb->checkVariableExists(var_to_register))) {
				TBOX_ERROR(d_object_name << ": Variable selected for Sphere not found:" <<  var_to_register);
			}
			int var_id = vdb->getVariable(var_to_register)->getInstanceIdentifier();
			plotter->registerPlotQuantity(var_to_register,"SCALAR",var_id);
		}
		i++;
	}

	return 0;
}
/*
 * Set up external plotter to plot integration data from this class.
 * Register variables appropriate for plotting.
 */
int Problem::setupIntegralPlotter(vector<std::shared_ptr<IntegrateDataWriter> > &plotters) const {
	if (!d_patch_hierarchy) {
		TBOX_ERROR(d_object_name << ": No hierarchy inn"
		<< " Problem::setupIntegralPlottern"
		<< "The hierarchy must be set before callingn"
		<< "this function.n");
	}
	int i = 0;
	for (vector<std::shared_ptr<IntegrateDataWriter> >::const_iterator it = plotters.begin(); it != plotters.end(); ++it) {
		std::shared_ptr<IntegrateDataWriter> plotter = *it;
		hier::VariableDatabase *vdb = hier::VariableDatabase::getDatabase();
		set<string> variables = d_integralVariables[i];
		for (set<string>::const_iterator it2 = variables.begin() ; it2 != variables.end(); ++it2) {
			string var_to_register = *it2;
			if (!(vdb->checkVariableExists(var_to_register))) {
				TBOX_ERROR(d_object_name << ": Variable selected for Integration not found:" <<  var_to_register);
			}
			int var_id = vdb->getVariable(var_to_register)->getInstanceIdentifier();
			plotter->registerPlotQuantity(var_to_register,"SCALAR",var_id);
		}
		i++;
	}
	return 0;
}
/*
 * Set up external plotter to plot point data from this class.
 * Register variables appropriate for plotting.
 */
int Problem::setupPointPlotter(vector<std::shared_ptr<PointDataWriter> > &plotters) const {
	int i = 0;
	for (vector<std::shared_ptr<PointDataWriter> >::const_iterator it = plotters.begin(); it != plotters.end(); ++it) {
		std::shared_ptr<PointDataWriter> plotter = *it;
		hier::VariableDatabase *vdb = hier::VariableDatabase::getDatabase();
		set<string> variables = d_pointVariables[i];
		for (set<string>::const_iterator it2 = variables.begin() ; it2 != variables.end(); ++it2) {
			string var_to_register = *it2;
			if (!(vdb->checkVariableExists(var_to_register))) {
				TBOX_ERROR(d_object_name << ": Variable selected for Point not found:" <<  var_to_register);
			}
			int var_id = vdb->getVariable(var_to_register)->getInstanceIdentifier();
			plotter->registerPlotQuantity(var_to_register,"SCALAR",var_id);
		}
		i++;
	}
	return 0;
}


/*
 * Perform a single step from the discretization schema algorithm   
 */
double Problem::advanceLevel(
   const std::shared_ptr<hier::PatchLevel>& level,
   const std::shared_ptr<hier::PatchHierarchy>& hierarchy,
   const double current_time,
   const double new_time,
   const bool first_step,
   const bool last_step,
   const bool regrid_advance)
{
	const tbox::SAMRAI_MPI& mpi(tbox::SAMRAI_MPI::getSAMRAIWorld());

	const int ln = level->getLevelNumber();
	const double simPlat_dt = new_time - current_time;
	const double level_ratio = level->getRatioToCoarserLevel().max();
	if (first_step) {
		bo_substep_iteration[ln] = 0;
	}
	else {
		bo_substep_iteration[ln] = bo_substep_iteration[ln] + 1;
	}
	time_interpolate_operator_mesh1->setRatio(level_ratio);
	time_interpolate_operator_mesh1->setStep(0);
	time_interpolate_operator_mesh1->setTimeSubstepNumber(bo_substep_iteration[ln]);

	if (d_refinedTimeStepping && first_step && ln > 0) {
		current_iteration[ln] = (current_iteration[ln - 1] - 1) * hierarchy->getRatioToCoarserLevel(ln).max() + 1;
	} else {
		current_iteration[ln] = current_iteration[ln] + 1;
	}
	int previous_iteration = current_iteration[ln] - 1;
	int outputCycle = current_iteration[ln];
	int maxLevels = hierarchy->getMaxNumberOfLevels();
	if (maxLevels > ln + 1) {
		int currentLevelNumber = ln;
		while (currentLevelNumber < maxLevels - 1) {
			int ratio = hierarchy->getRatioToCoarserLevel(currentLevelNumber + 1).max();
			outputCycle = outputCycle * ratio;
			previous_iteration = previous_iteration * ratio;
			currentLevelNumber++;
		}
	}

	t_step->start();
  	// Shifting time
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch > patch = *p_it;
		std::shared_ptr< pdat::NodeData<double> > Null(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_Null_id)));
		std::shared_ptr< pdat::NodeData<double> > Null_p(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_Null_p_id)));
		std::shared_ptr< pdat::NodeData<double> > rho(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_rho_id)));
		std::shared_ptr< pdat::NodeData<double> > rho_p(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_rho_p_id)));
		std::shared_ptr< pdat::NodeData<double> > mx(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_mx_id)));
		std::shared_ptr< pdat::NodeData<double> > mx_p(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_mx_p_id)));
		std::shared_ptr< pdat::NodeData<double> > my(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_my_id)));
		std::shared_ptr< pdat::NodeData<double> > my_p(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_my_p_id)));
		std::shared_ptr< pdat::NodeData<double> > mz(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_mz_id)));
		std::shared_ptr< pdat::NodeData<double> > mz_p(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_mz_p_id)));
		std::shared_ptr< pdat::NodeData<double> > rk1rho(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_rk1rho_id)));
		std::shared_ptr< pdat::NodeData<double> > rk1Null(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_rk1Null_id)));
		std::shared_ptr< pdat::NodeData<double> > rk1mx(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_rk1mx_id)));
		std::shared_ptr< pdat::NodeData<double> > rk1my(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_rk1my_id)));
		std::shared_ptr< pdat::NodeData<double> > rk1mz(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_rk1mz_id)));
		std::shared_ptr< pdat::NodeData<double> > rk2rho(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_rk2rho_id)));
		std::shared_ptr< pdat::NodeData<double> > rk2Null(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_rk2Null_id)));
		std::shared_ptr< pdat::NodeData<double> > rk2mx(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_rk2mx_id)));
		std::shared_ptr< pdat::NodeData<double> > rk2my(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_rk2my_id)));
		std::shared_ptr< pdat::NodeData<double> > rk2mz(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_rk2mz_id)));
		mz_p->copy(*mz);
		my_p->copy(*my);
		mx_p->copy(*mx);
		rho_p->copy(*rho);
		Null_p->copy(*Null);
		Null_p->setTime(current_time);
		rho_p->setTime(current_time);
		mx_p->setTime(current_time);
		my_p->setTime(current_time);
		mz_p->setTime(current_time);
		rk1rho->setTime(current_time + simPlat_dt);
		rk1Null->setTime(current_time + simPlat_dt);
		rk1mx->setTime(current_time + simPlat_dt);
		rk1my->setTime(current_time + simPlat_dt);
		rk1mz->setTime(current_time + simPlat_dt);
		rk2rho->setTime(current_time + simPlat_dt * 0.5);
		rk2Null->setTime(current_time + simPlat_dt * 0.5);
		rk2mx->setTime(current_time + simPlat_dt * 0.5);
		rk2my->setTime(current_time + simPlat_dt * 0.5);
		rk2mz->setTime(current_time + simPlat_dt * 0.5);
		rho->setTime(current_time + simPlat_dt);
		Null->setTime(current_time + simPlat_dt);
		mx->setTime(current_time + simPlat_dt);
		my->setTime(current_time + simPlat_dt);
		mz->setTime(current_time + simPlat_dt);
	}
  	// Evolution
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch >& patch = *p_it;
		double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_3 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_3_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
		double* FOV_zLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zLower_id).get())->getPointer();
		double* FOV_zUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zUpper_id).get())->getPointer();
	
		double* SpeedSBi_3 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBi_3_id).get())->getPointer();
		double* SpeedSBj_3 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBj_3_id).get())->getPointer();
		double* SpeedSBk_3 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBk_3_id).get())->getPointer();
		double* extrapolatedSBmxSPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmxSPi_id).get())->getPointer();
		double* extrapolatedSBmySPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmySPi_id).get())->getPointer();
		double* extrapolatedSBmzSPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmzSPi_id).get())->getPointer();
		double* extrapolatedSBrhoSPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBrhoSPi_id).get())->getPointer();
		double* SpeedSBi_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBi_1_id).get())->getPointer();
		double* extrapolatedSBmySPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmySPj_id).get())->getPointer();
		double* extrapolatedSBmxSPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmxSPj_id).get())->getPointer();
		double* extrapolatedSBmzSPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmzSPj_id).get())->getPointer();
		double* extrapolatedSBrhoSPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBrhoSPj_id).get())->getPointer();
		double* SpeedSBj_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBj_1_id).get())->getPointer();
		double* extrapolatedSBmzSPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmzSPk_id).get())->getPointer();
		double* extrapolatedSBmxSPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmxSPk_id).get())->getPointer();
		double* extrapolatedSBmySPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmySPk_id).get())->getPointer();
		double* extrapolatedSBrhoSPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBrhoSPk_id).get())->getPointer();
		double* SpeedSBk_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBk_1_id).get())->getPointer();
		double m_n, m_tx, m_ty, m_tz, extrapolatedSBcnSPi, extrapolatedSBcSPi, extrapolatedSBcnSPj, extrapolatedSBcSPj, extrapolatedSBcnSPk, extrapolatedSBcSPk;
	
		//Get the dimensions of the patch
		const hier::Index boxfirst = patch->getBox().lower();
		const hier::Index boxlast = patch->getBox().upper();
	
		//Get delta spaces into an array. dx, dy, dz.
		std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
		const double* dx  = patch_geom->getDx();
	
		//Auxiliary definitions
		int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
		int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
		int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;
		for(int k = 0; k < klast; k++) {
			for(int j = 0; j < jlast; j++) {
				for(int i = 0; i < ilast; i++) {
					if ((vector(FOV_3, i, j, k) > 0 || (((i + 1 < ilast && vector(FOV_3, i + 1, j, k) > 0) || (i + 2 < ilast && vector(FOV_3, i + 2, j, k) > 0) || (j + 1 < jlast && vector(FOV_3, i, j + 1, k) > 0) || (j + 2 < jlast && vector(FOV_3, i, j + 2, k) > 0) || (k + 1 < klast && vector(FOV_3, i, j, k + 1) > 0) || (k + 2 < klast && vector(FOV_3, i, j, k + 2) > 0)) || ((i - 1 >= 0 && vector(FOV_3, i - 1, j, k) > 0) || (i - 2 >= 0 && vector(FOV_3, i - 2, j, k) > 0) || (j - 1 >= 0 && vector(FOV_3, i, j - 1, k) > 0) || (j - 2 >= 0 && vector(FOV_3, i, j - 2, k) > 0) || (k - 1 >= 0 && vector(FOV_3, i, j, k - 1) > 0) || (k - 2 >= 0 && vector(FOV_3, i, j, k - 2) > 0))) )) {
						vector(SpeedSBi_3, i, j, k) = fabs(1.0);
						vector(SpeedSBj_3, i, j, k) = fabs(1.0);
						vector(SpeedSBk_3, i, j, k) = fabs(1.0);
					}
					if ((vector(FOV_1, i, j, k) > 0 || (((i + 1 < ilast && vector(FOV_1, i + 1, j, k) > 0) || (i + 2 < ilast && vector(FOV_1, i + 2, j, k) > 0) || (j + 1 < jlast && vector(FOV_1, i, j + 1, k) > 0) || (j + 2 < jlast && vector(FOV_1, i, j + 2, k) > 0) || (k + 1 < klast && vector(FOV_1, i, j, k + 1) > 0) || (k + 2 < klast && vector(FOV_1, i, j, k + 2) > 0)) || ((i - 1 >= 0 && vector(FOV_1, i - 1, j, k) > 0) || (i - 2 >= 0 && vector(FOV_1, i - 2, j, k) > 0) || (j - 1 >= 0 && vector(FOV_1, i, j - 1, k) > 0) || (j - 2 >= 0 && vector(FOV_1, i, j - 2, k) > 0) || (k - 1 >= 0 && vector(FOV_1, i, j, k - 1) > 0) || (k - 2 >= 0 && vector(FOV_1, i, j, k - 2) > 0))) )) {
						m_n = vector(extrapolatedSBmxSPi, i, j, k);
						m_tx = 0.0;
						m_ty = vector(extrapolatedSBmySPi, i, j, k);
						m_tz = vector(extrapolatedSBmzSPi, i, j, k);
						extrapolatedSBcnSPi = m_n / vector(extrapolatedSBrhoSPi, i, j, k);
						extrapolatedSBcSPi = c0 * (pow((vector(extrapolatedSBrhoSPi, i, j, k) / rho0), ((gamma + (-1.0)) / 2.0)));
						vector(SpeedSBi_1, i, j, k) = MAX(fabs(extrapolatedSBcSPi + extrapolatedSBcnSPi), MAX(fabs((-extrapolatedSBcSPi) + extrapolatedSBcnSPi), MAX(fabs(extrapolatedSBcnSPi), MAX(fabs(extrapolatedSBcnSPi), fabs(extrapolatedSBcnSPi)))));
						m_n = vector(extrapolatedSBmySPj, i, j, k);
						m_tx = vector(extrapolatedSBmxSPj, i, j, k);
						m_ty = 0.0;
						m_tz = vector(extrapolatedSBmzSPj, i, j, k);
						extrapolatedSBcnSPj = m_n / vector(extrapolatedSBrhoSPj, i, j, k);
						extrapolatedSBcSPj = c0 * (pow((vector(extrapolatedSBrhoSPj, i, j, k) / rho0), ((gamma + (-1.0)) / 2.0)));
						vector(SpeedSBj_1, i, j, k) = MAX(fabs(extrapolatedSBcSPj + extrapolatedSBcnSPj), MAX(fabs((-extrapolatedSBcSPj) + extrapolatedSBcnSPj), MAX(fabs(extrapolatedSBcnSPj), MAX(fabs(extrapolatedSBcnSPj), fabs(extrapolatedSBcnSPj)))));
						m_n = vector(extrapolatedSBmzSPk, i, j, k);
						m_tx = vector(extrapolatedSBmxSPk, i, j, k);
						m_ty = vector(extrapolatedSBmySPk, i, j, k);
						m_tz = 0.0;
						extrapolatedSBcnSPk = m_n / vector(extrapolatedSBrhoSPk, i, j, k);
						extrapolatedSBcSPk = c0 * (pow((vector(extrapolatedSBrhoSPk, i, j, k) / rho0), ((gamma + (-1.0)) / 2.0)));
						vector(SpeedSBk_1, i, j, k) = MAX(fabs(extrapolatedSBcSPk + extrapolatedSBcnSPk), MAX(fabs((-extrapolatedSBcSPk) + extrapolatedSBcnSPk), MAX(fabs(extrapolatedSBcnSPk), MAX(fabs(extrapolatedSBcnSPk), fabs(extrapolatedSBcnSPk)))));
					}
				}
			}
		}
	}
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch >& patch = *p_it;
		double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_3 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_3_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
		double* FOV_zLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zLower_id).get())->getPointer();
		double* FOV_zUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zUpper_id).get())->getPointer();
	
		double* SpeedSBi_3 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBi_3_id).get())->getPointer();
		double* SpeedSBj_3 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBj_3_id).get())->getPointer();
		double* SpeedSBk_3 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBk_3_id).get())->getPointer();
		double* SpeedSBi_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBi_1_id).get())->getPointer();
		double* SpeedSBj_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBj_1_id).get())->getPointer();
		double* SpeedSBk_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBk_1_id).get())->getPointer();
	
		//Get the dimensions of the patch
		const hier::Index boxfirst = patch->getBox().lower();
		const hier::Index boxlast = patch->getBox().upper();
	
		//Get delta spaces into an array. dx, dy, dz.
		std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
		const double* dx  = patch_geom->getDx();
	
		//Auxiliary definitions
		int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
		int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
		int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;
		for(int k = 0; k < klast; k++) {
			for(int j = 0; j < jlast; j++) {
				for(int i = 0; i < ilast; i++) {
					if ((vector(FOV_3, i, j, k) > 0 || (((i + 1 < ilast && vector(FOV_3, i + 1, j, k) > 0) || (j + 1 < jlast && vector(FOV_3, i, j + 1, k) > 0) || (k + 1 < klast && vector(FOV_3, i, j, k + 1) > 0)) || ((i - 1 >= 0 && vector(FOV_3, i - 1, j, k) > 0) || (j - 1 >= 0 && vector(FOV_3, i, j - 1, k) > 0) || (k - 1 >= 0 && vector(FOV_3, i, j, k - 1) > 0))) ) && (i + 1 < ilast && i - 1 >= 0 && j + 1 < jlast && j - 1 >= 0 && k + 1 < klast && k - 1 >= 0)) {
						vector(SpeedSBi_3, i, j, k) = MAX(vector(SpeedSBi_3, i, j, k), vector(SpeedSBi_3, i + 1, j, k));
						vector(SpeedSBj_3, i, j, k) = MAX(vector(SpeedSBj_3, i, j, k), vector(SpeedSBj_3, i, j + 1, k));
						vector(SpeedSBk_3, i, j, k) = MAX(vector(SpeedSBk_3, i, j, k), vector(SpeedSBk_3, i, j, k + 1));
					}
					if ((vector(FOV_1, i, j, k) > 0 || (((i + 1 < ilast && vector(FOV_1, i + 1, j, k) > 0) || (j + 1 < jlast && vector(FOV_1, i, j + 1, k) > 0) || (k + 1 < klast && vector(FOV_1, i, j, k + 1) > 0)) || ((i - 1 >= 0 && vector(FOV_1, i - 1, j, k) > 0) || (j - 1 >= 0 && vector(FOV_1, i, j - 1, k) > 0) || (k - 1 >= 0 && vector(FOV_1, i, j, k - 1) > 0))) ) && (i + 1 < ilast && i - 1 >= 0 && j + 1 < jlast && j - 1 >= 0 && k + 1 < klast && k - 1 >= 0)) {
						vector(SpeedSBi_1, i, j, k) = MAX(vector(SpeedSBi_1, i, j, k), vector(SpeedSBi_1, i + 1, j, k));
						vector(SpeedSBj_1, i, j, k) = MAX(vector(SpeedSBj_1, i, j, k), vector(SpeedSBj_1, i, j + 1, k));
						vector(SpeedSBk_1, i, j, k) = MAX(vector(SpeedSBk_1, i, j, k), vector(SpeedSBk_1, i, j, k + 1));
					}
				}
			}
		}
	}
	//Fill ghosts and periodical boundaries
	d_bdry_sched_advance1[ln]->fillData(current_time, false);
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch >& patch = *p_it;
		double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_3 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_3_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
		double* FOV_zLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zLower_id).get())->getPointer();
		double* FOV_zUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zUpper_id).get())->getPointer();
	
		double* ad_mx_o0_t0_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_mx_o0_t0_m0_l0_1_id).get())->getPointer();
		double* mx_p = ((pdat::NodeData<double> *) patch->getPatchData(d_mx_p_id).get())->getPointer();
		double* rho_p = ((pdat::NodeData<double> *) patch->getPatchData(d_rho_p_id).get())->getPointer();
		double* ad_my_o0_t3_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_my_o0_t3_m0_l0_1_id).get())->getPointer();
		double* my_p = ((pdat::NodeData<double> *) patch->getPatchData(d_my_p_id).get())->getPointer();
		double* ad_mz_o0_t3_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_mz_o0_t3_m0_l0_1_id).get())->getPointer();
		double* mz_p = ((pdat::NodeData<double> *) patch->getPatchData(d_mz_p_id).get())->getPointer();
		double* FluxSBimx_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBimx_1_id).get())->getPointer();
		double* extrapolatedSBrhoSPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBrhoSPi_id).get())->getPointer();
		double* extrapolatedSBmxSPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmxSPi_id).get())->getPointer();
		double* extrapolatedSBpSPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBpSPi_id).get())->getPointer();
		double* FluxSBimy_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBimy_1_id).get())->getPointer();
		double* extrapolatedSBmySPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmySPi_id).get())->getPointer();
		double* FluxSBimz_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBimz_1_id).get())->getPointer();
		double* extrapolatedSBmzSPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmzSPi_id).get())->getPointer();
		double* FluxSBjmx_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjmx_1_id).get())->getPointer();
		double* extrapolatedSBrhoSPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBrhoSPj_id).get())->getPointer();
		double* extrapolatedSBmxSPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmxSPj_id).get())->getPointer();
		double* extrapolatedSBmySPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmySPj_id).get())->getPointer();
		double* FluxSBjmy_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjmy_1_id).get())->getPointer();
		double* extrapolatedSBpSPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBpSPj_id).get())->getPointer();
		double* FluxSBjmz_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjmz_1_id).get())->getPointer();
		double* extrapolatedSBmzSPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmzSPj_id).get())->getPointer();
		double* FluxSBkmx_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkmx_1_id).get())->getPointer();
		double* extrapolatedSBrhoSPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBrhoSPk_id).get())->getPointer();
		double* extrapolatedSBmxSPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmxSPk_id).get())->getPointer();
		double* extrapolatedSBmzSPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmzSPk_id).get())->getPointer();
		double* FluxSBkmy_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkmy_1_id).get())->getPointer();
		double* extrapolatedSBmySPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmySPk_id).get())->getPointer();
		double* FluxSBkmz_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkmz_1_id).get())->getPointer();
		double* extrapolatedSBpSPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBpSPk_id).get())->getPointer();
	
		//Get the dimensions of the patch
		const hier::Index boxfirst = patch->getBox().lower();
		const hier::Index boxlast = patch->getBox().upper();
	
		//Get delta spaces into an array. dx, dy, dz.
		std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
		const double* dx  = patch_geom->getDx();
	
		//Auxiliary definitions
		int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
		int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
		int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;
		for(int k = 0; k < klast; k++) {
			for(int j = 0; j < jlast; j++) {
				for(int i = 0; i < ilast; i++) {
					if ((vector(FOV_1, i, j, k) > 0 || (((i + 1 < ilast && vector(FOV_1, i + 1, j, k) > 0) || (i + 2 < ilast && vector(FOV_1, i + 2, j, k) > 0) || (j + 1 < jlast && vector(FOV_1, i, j + 1, k) > 0) || (j + 2 < jlast && vector(FOV_1, i, j + 2, k) > 0) || (k + 1 < klast && vector(FOV_1, i, j, k + 1) > 0) || (k + 2 < klast && vector(FOV_1, i, j, k + 2) > 0)) || ((i - 1 >= 0 && vector(FOV_1, i - 1, j, k) > 0) || (i - 2 >= 0 && vector(FOV_1, i - 2, j, k) > 0) || (j - 1 >= 0 && vector(FOV_1, i, j - 1, k) > 0) || (j - 2 >= 0 && vector(FOV_1, i, j - 2, k) > 0) || (k - 1 >= 0 && vector(FOV_1, i, j, k - 1) > 0) || (k - 2 >= 0 && vector(FOV_1, i, j, k - 2) > 0)) || (((j + 1 < jlast && k + 1 < klast) && vector(FOV_1, i, j + 1, k + 1) > 0) || ((j + 1 < jlast && k - 1 >= 0) && vector(FOV_1, i, j + 1, k - 1) > 0) || ((j + 1 < jlast && k + 2 < klast) && vector(FOV_1, i, j + 1, k + 2) > 0) || ((j + 1 < jlast && k - 2 >= 0) && vector(FOV_1, i, j + 1, k - 2) > 0) || ((j - 1 >= 0 && k + 1 < klast) && vector(FOV_1, i, j - 1, k + 1) > 0) || ((j - 1 >= 0 && k - 1 >= 0) && vector(FOV_1, i, j - 1, k - 1) > 0) || ((j - 1 >= 0 && k + 2 < klast) && vector(FOV_1, i, j - 1, k + 2) > 0) || ((j - 1 >= 0 && k - 2 >= 0) && vector(FOV_1, i, j - 1, k - 2) > 0) || ((j + 2 < jlast && k + 1 < klast) && vector(FOV_1, i, j + 2, k + 1) > 0) || ((j + 2 < jlast && k - 1 >= 0) && vector(FOV_1, i, j + 2, k - 1) > 0) || ((j + 2 < jlast && k + 2 < klast) && vector(FOV_1, i, j + 2, k + 2) > 0) || ((j + 2 < jlast && k - 2 >= 0) && vector(FOV_1, i, j + 2, k - 2) > 0) || ((j - 2 >= 0 && k + 1 < klast) && vector(FOV_1, i, j - 2, k + 1) > 0) || ((j - 2 >= 0 && k - 1 >= 0) && vector(FOV_1, i, j - 2, k - 1) > 0) || ((j - 2 >= 0 && k + 2 < klast) && vector(FOV_1, i, j - 2, k + 2) > 0) || ((j - 2 >= 0 && k - 2 >= 0) && vector(FOV_1, i, j - 2, k - 2) > 0) || ((i + 1 < ilast && k + 1 < klast) && vector(FOV_1, i + 1, j, k + 1) > 0) || ((i + 1 < ilast && k - 1 >= 0) && vector(FOV_1, i + 1, j, k - 1) > 0) || ((i + 1 < ilast && k + 2 < klast) && vector(FOV_1, i + 1, j, k + 2) > 0) || ((i + 1 < ilast && k - 2 >= 0) && vector(FOV_1, i + 1, j, k - 2) > 0) || ((i + 1 < ilast && j + 1 < jlast) && vector(FOV_1, i + 1, j + 1, k) > 0) || ((i + 1 < ilast && j + 1 < jlast && k + 1 < klast) && vector(FOV_1, i + 1, j + 1, k + 1) > 0) || ((i + 1 < ilast && j + 1 < jlast && k - 1 >= 0) && vector(FOV_1, i + 1, j + 1, k - 1) > 0) || ((i + 1 < ilast && j + 1 < jlast && k + 2 < klast) && vector(FOV_1, i + 1, j + 1, k + 2) > 0) || ((i + 1 < ilast && j + 1 < jlast && k - 2 >= 0) && vector(FOV_1, i + 1, j + 1, k - 2) > 0) || ((i + 1 < ilast && j - 1 >= 0) && vector(FOV_1, i + 1, j - 1, k) > 0) || ((i + 1 < ilast && j - 1 >= 0 && k + 1 < klast) && vector(FOV_1, i + 1, j - 1, k + 1) > 0) || ((i + 1 < ilast && j - 1 >= 0 && k - 1 >= 0) && vector(FOV_1, i + 1, j - 1, k - 1) > 0) || ((i + 1 < ilast && j - 1 >= 0 && k + 2 < klast) && vector(FOV_1, i + 1, j - 1, k + 2) > 0) || ((i + 1 < ilast && j - 1 >= 0 && k - 2 >= 0) && vector(FOV_1, i + 1, j - 1, k - 2) > 0) || ((i + 1 < ilast && j + 2 < jlast) && vector(FOV_1, i + 1, j + 2, k) > 0) || ((i + 1 < ilast && j + 2 < jlast && k + 1 < klast) && vector(FOV_1, i + 1, j + 2, k + 1) > 0) || ((i + 1 < ilast && j + 2 < jlast && k - 1 >= 0) && vector(FOV_1, i + 1, j + 2, k - 1) > 0) || ((i + 1 < ilast && j + 2 < jlast && k + 2 < klast) && vector(FOV_1, i + 1, j + 2, k + 2) > 0) || ((i + 1 < ilast && j + 2 < jlast && k - 2 >= 0) && vector(FOV_1, i + 1, j + 2, k - 2) > 0) || ((i + 1 < ilast && j - 2 >= 0) && vector(FOV_1, i + 1, j - 2, k) > 0) || ((i + 1 < ilast && j - 2 >= 0 && k + 1 < klast) && vector(FOV_1, i + 1, j - 2, k + 1) > 0) || ((i + 1 < ilast && j - 2 >= 0 && k - 1 >= 0) && vector(FOV_1, i + 1, j - 2, k - 1) > 0) || ((i + 1 < ilast && j - 2 >= 0 && k + 2 < klast) && vector(FOV_1, i + 1, j - 2, k + 2) > 0) || ((i + 1 < ilast && j - 2 >= 0 && k - 2 >= 0) && vector(FOV_1, i + 1, j - 2, k - 2) > 0) || ((i - 1 >= 0 && k + 1 < klast) && vector(FOV_1, i - 1, j, k + 1) > 0) || ((i - 1 >= 0 && k - 1 >= 0) && vector(FOV_1, i - 1, j, k - 1) > 0) || ((i - 1 >= 0 && k + 2 < klast) && vector(FOV_1, i - 1, j, k + 2) > 0) || ((i - 1 >= 0 && k - 2 >= 0) && vector(FOV_1, i - 1, j, k - 2) > 0) || ((i - 1 >= 0 && j + 1 < jlast) && vector(FOV_1, i - 1, j + 1, k) > 0) || ((i - 1 >= 0 && j + 1 < jlast && k + 1 < klast) && vector(FOV_1, i - 1, j + 1, k + 1) > 0) || ((i - 1 >= 0 && j + 1 < jlast && k - 1 >= 0) && vector(FOV_1, i - 1, j + 1, k - 1) > 0) || ((i - 1 >= 0 && j + 1 < jlast && k + 2 < klast) && vector(FOV_1, i - 1, j + 1, k + 2) > 0) || ((i - 1 >= 0 && j + 1 < jlast && k - 2 >= 0) && vector(FOV_1, i - 1, j + 1, k - 2) > 0) || ((i - 1 >= 0 && j - 1 >= 0) && vector(FOV_1, i - 1, j - 1, k) > 0) || ((i - 1 >= 0 && j - 1 >= 0 && k + 1 < klast) && vector(FOV_1, i - 1, j - 1, k + 1) > 0) || ((i - 1 >= 0 && j - 1 >= 0 && k - 1 >= 0) && vector(FOV_1, i - 1, j - 1, k - 1) > 0) || ((i - 1 >= 0 && j - 1 >= 0 && k + 2 < klast) && vector(FOV_1, i - 1, j - 1, k + 2) > 0) || ((i - 1 >= 0 && j - 1 >= 0 && k - 2 >= 0) && vector(FOV_1, i - 1, j - 1, k - 2) > 0) || ((i - 1 >= 0 && j + 2 < jlast) && vector(FOV_1, i - 1, j + 2, k) > 0) || ((i - 1 >= 0 && j + 2 < jlast && k + 1 < klast) && vector(FOV_1, i - 1, j + 2, k + 1) > 0) || ((i - 1 >= 0 && j + 2 < jlast && k - 1 >= 0) && vector(FOV_1, i - 1, j + 2, k - 1) > 0) || ((i - 1 >= 0 && j + 2 < jlast && k + 2 < klast) && vector(FOV_1, i - 1, j + 2, k + 2) > 0) || ((i - 1 >= 0 && j + 2 < jlast && k - 2 >= 0) && vector(FOV_1, i - 1, j + 2, k - 2) > 0) || ((i - 1 >= 0 && j - 2 >= 0) && vector(FOV_1, i - 1, j - 2, k) > 0) || ((i - 1 >= 0 && j - 2 >= 0 && k + 1 < klast) && vector(FOV_1, i - 1, j - 2, k + 1) > 0) || ((i - 1 >= 0 && j - 2 >= 0 && k - 1 >= 0) && vector(FOV_1, i - 1, j - 2, k - 1) > 0) || ((i - 1 >= 0 && j - 2 >= 0 && k + 2 < klast) && vector(FOV_1, i - 1, j - 2, k + 2) > 0) || ((i - 1 >= 0 && j - 2 >= 0 && k - 2 >= 0) && vector(FOV_1, i - 1, j - 2, k - 2) > 0) || ((i + 2 < ilast && k + 1 < klast) && vector(FOV_1, i + 2, j, k + 1) > 0) || ((i + 2 < ilast && k - 1 >= 0) && vector(FOV_1, i + 2, j, k - 1) > 0) || ((i + 2 < ilast && k + 2 < klast) && vector(FOV_1, i + 2, j, k + 2) > 0) || ((i + 2 < ilast && k - 2 >= 0) && vector(FOV_1, i + 2, j, k - 2) > 0) || ((i + 2 < ilast && j + 1 < jlast) && vector(FOV_1, i + 2, j + 1, k) > 0) || ((i + 2 < ilast && j + 1 < jlast && k + 1 < klast) && vector(FOV_1, i + 2, j + 1, k + 1) > 0) || ((i + 2 < ilast && j + 1 < jlast && k - 1 >= 0) && vector(FOV_1, i + 2, j + 1, k - 1) > 0) || ((i + 2 < ilast && j + 1 < jlast && k + 2 < klast) && vector(FOV_1, i + 2, j + 1, k + 2) > 0) || ((i + 2 < ilast && j + 1 < jlast && k - 2 >= 0) && vector(FOV_1, i + 2, j + 1, k - 2) > 0) || ((i + 2 < ilast && j - 1 >= 0) && vector(FOV_1, i + 2, j - 1, k) > 0) || ((i + 2 < ilast && j - 1 >= 0 && k + 1 < klast) && vector(FOV_1, i + 2, j - 1, k + 1) > 0) || ((i + 2 < ilast && j - 1 >= 0 && k - 1 >= 0) && vector(FOV_1, i + 2, j - 1, k - 1) > 0) || ((i + 2 < ilast && j - 1 >= 0 && k + 2 < klast) && vector(FOV_1, i + 2, j - 1, k + 2) > 0) || ((i + 2 < ilast && j - 1 >= 0 && k - 2 >= 0) && vector(FOV_1, i + 2, j - 1, k - 2) > 0) || ((i + 2 < ilast && j + 2 < jlast) && vector(FOV_1, i + 2, j + 2, k) > 0) || ((i + 2 < ilast && j + 2 < jlast && k + 1 < klast) && vector(FOV_1, i + 2, j + 2, k + 1) > 0) || ((i + 2 < ilast && j + 2 < jlast && k - 1 >= 0) && vector(FOV_1, i + 2, j + 2, k - 1) > 0) || ((i + 2 < ilast && j + 2 < jlast && k + 2 < klast) && vector(FOV_1, i + 2, j + 2, k + 2) > 0) || ((i + 2 < ilast && j + 2 < jlast && k - 2 >= 0) && vector(FOV_1, i + 2, j + 2, k - 2) > 0) || ((i + 2 < ilast && j - 2 >= 0) && vector(FOV_1, i + 2, j - 2, k) > 0) || ((i + 2 < ilast && j - 2 >= 0 && k + 1 < klast) && vector(FOV_1, i + 2, j - 2, k + 1) > 0) || ((i + 2 < ilast && j - 2 >= 0 && k - 1 >= 0) && vector(FOV_1, i + 2, j - 2, k - 1) > 0) || ((i + 2 < ilast && j - 2 >= 0 && k + 2 < klast) && vector(FOV_1, i + 2, j - 2, k + 2) > 0) || ((i + 2 < ilast && j - 2 >= 0 && k - 2 >= 0) && vector(FOV_1, i + 2, j - 2, k - 2) > 0) || ((i - 2 >= 0 && k + 1 < klast) && vector(FOV_1, i - 2, j, k + 1) > 0) || ((i - 2 >= 0 && k - 1 >= 0) && vector(FOV_1, i - 2, j, k - 1) > 0) || ((i - 2 >= 0 && k + 2 < klast) && vector(FOV_1, i - 2, j, k + 2) > 0) || ((i - 2 >= 0 && k - 2 >= 0) && vector(FOV_1, i - 2, j, k - 2) > 0) || ((i - 2 >= 0 && j + 1 < jlast) && vector(FOV_1, i - 2, j + 1, k) > 0) || ((i - 2 >= 0 && j + 1 < jlast && k + 1 < klast) && vector(FOV_1, i - 2, j + 1, k + 1) > 0) || ((i - 2 >= 0 && j + 1 < jlast && k - 1 >= 0) && vector(FOV_1, i - 2, j + 1, k - 1) > 0) || ((i - 2 >= 0 && j + 1 < jlast && k + 2 < klast) && vector(FOV_1, i - 2, j + 1, k + 2) > 0) || ((i - 2 >= 0 && j + 1 < jlast && k - 2 >= 0) && vector(FOV_1, i - 2, j + 1, k - 2) > 0) || ((i - 2 >= 0 && j - 1 >= 0) && vector(FOV_1, i - 2, j - 1, k) > 0) || ((i - 2 >= 0 && j - 1 >= 0 && k + 1 < klast) && vector(FOV_1, i - 2, j - 1, k + 1) > 0) || ((i - 2 >= 0 && j - 1 >= 0 && k - 1 >= 0) && vector(FOV_1, i - 2, j - 1, k - 1) > 0) || ((i - 2 >= 0 && j - 1 >= 0 && k + 2 < klast) && vector(FOV_1, i - 2, j - 1, k + 2) > 0) || ((i - 2 >= 0 && j - 1 >= 0 && k - 2 >= 0) && vector(FOV_1, i - 2, j - 1, k - 2) > 0) || ((i - 2 >= 0 && j + 2 < jlast) && vector(FOV_1, i - 2, j + 2, k) > 0) || ((i - 2 >= 0 && j + 2 < jlast && k + 1 < klast) && vector(FOV_1, i - 2, j + 2, k + 1) > 0) || ((i - 2 >= 0 && j + 2 < jlast && k - 1 >= 0) && vector(FOV_1, i - 2, j + 2, k - 1) > 0) || ((i - 2 >= 0 && j + 2 < jlast && k + 2 < klast) && vector(FOV_1, i - 2, j + 2, k + 2) > 0) || ((i - 2 >= 0 && j + 2 < jlast && k - 2 >= 0) && vector(FOV_1, i - 2, j + 2, k - 2) > 0) || ((i - 2 >= 0 && j - 2 >= 0) && vector(FOV_1, i - 2, j - 2, k) > 0) || ((i - 2 >= 0 && j - 2 >= 0 && k + 1 < klast) && vector(FOV_1, i - 2, j - 2, k + 1) > 0) || ((i - 2 >= 0 && j - 2 >= 0 && k - 1 >= 0) && vector(FOV_1, i - 2, j - 2, k - 1) > 0) || ((i - 2 >= 0 && j - 2 >= 0 && k + 2 < klast) && vector(FOV_1, i - 2, j - 2, k + 2) > 0) || ((i - 2 >= 0 && j - 2 >= 0 && k - 2 >= 0) && vector(FOV_1, i - 2, j - 2, k - 2) > 0))) )) {
						vector(ad_mx_o0_t0_m0_l0_1, i, j, k) = vector(mx_p, i, j, k) / vector(rho_p, i, j, k);
						vector(ad_my_o0_t3_m0_l0_1, i, j, k) = vector(my_p, i, j, k) / vector(rho_p, i, j, k);
						vector(ad_mz_o0_t3_m0_l0_1, i, j, k) = vector(mz_p, i, j, k) / vector(rho_p, i, j, k);
						vector(FluxSBimx_1, i, j, k) = Fimx_PlaneI(vector(extrapolatedSBrhoSPi, i, j, k), vector(extrapolatedSBmxSPi, i, j, k), vector(extrapolatedSBpSPi, i, j, k), dx, simPlat_dt, ilast, jlast);
						vector(FluxSBimy_1, i, j, k) = Fimy_PlaneI(vector(extrapolatedSBrhoSPi, i, j, k), vector(extrapolatedSBmxSPi, i, j, k), vector(extrapolatedSBmySPi, i, j, k), dx, simPlat_dt, ilast, jlast);
						vector(FluxSBimz_1, i, j, k) = Fimz_PlaneI(vector(extrapolatedSBrhoSPi, i, j, k), vector(extrapolatedSBmxSPi, i, j, k), vector(extrapolatedSBmzSPi, i, j, k), dx, simPlat_dt, ilast, jlast);
						vector(FluxSBjmx_1, i, j, k) = Fjmx_PlaneI(vector(extrapolatedSBrhoSPj, i, j, k), vector(extrapolatedSBmxSPj, i, j, k), vector(extrapolatedSBmySPj, i, j, k), dx, simPlat_dt, ilast, jlast);
						vector(FluxSBjmy_1, i, j, k) = Fjmy_PlaneI(vector(extrapolatedSBrhoSPj, i, j, k), vector(extrapolatedSBmySPj, i, j, k), vector(extrapolatedSBpSPj, i, j, k), dx, simPlat_dt, ilast, jlast);
						vector(FluxSBjmz_1, i, j, k) = Fjmz_PlaneI(vector(extrapolatedSBrhoSPj, i, j, k), vector(extrapolatedSBmySPj, i, j, k), vector(extrapolatedSBmzSPj, i, j, k), dx, simPlat_dt, ilast, jlast);
						vector(FluxSBkmx_1, i, j, k) = Fkmx_PlaneI(vector(extrapolatedSBrhoSPk, i, j, k), vector(extrapolatedSBmxSPk, i, j, k), vector(extrapolatedSBmzSPk, i, j, k), dx, simPlat_dt, ilast, jlast);
						vector(FluxSBkmy_1, i, j, k) = Fkmy_PlaneI(vector(extrapolatedSBrhoSPk, i, j, k), vector(extrapolatedSBmySPk, i, j, k), vector(extrapolatedSBmzSPk, i, j, k), dx, simPlat_dt, ilast, jlast);
						vector(FluxSBkmz_1, i, j, k) = Fkmz_PlaneI(vector(extrapolatedSBrhoSPk, i, j, k), vector(extrapolatedSBmzSPk, i, j, k), vector(extrapolatedSBpSPk, i, j, k), dx, simPlat_dt, ilast, jlast);
					}
				}
			}
		}
	}
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch >& patch = *p_it;
		double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_3 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_3_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
		double* FOV_zLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zLower_id).get())->getPointer();
		double* FOV_zUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zUpper_id).get())->getPointer();
	
		double* rk1Null = ((pdat::NodeData<double> *) patch->getPatchData(d_rk1Null_id).get())->getPointer();
		double* Null_p = ((pdat::NodeData<double> *) patch->getPatchData(d_Null_p_id).get())->getPointer();
		double* ad_mx_o0_t0_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_mx_o0_t0_m0_l0_1_id).get())->getPointer();
		double* ad_my_o0_t3_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_my_o0_t3_m0_l0_1_id).get())->getPointer();
		double* ad_mz_o0_t3_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_mz_o0_t3_m0_l0_1_id).get())->getPointer();
		double* rho_p = ((pdat::NodeData<double> *) patch->getPatchData(d_rho_p_id).get())->getPointer();
		double* fx = ((pdat::NodeData<double> *) patch->getPatchData(d_fx_id).get())->getPointer();
		double* fy = ((pdat::NodeData<double> *) patch->getPatchData(d_fy_id).get())->getPointer();
		double* fz = ((pdat::NodeData<double> *) patch->getPatchData(d_fz_id).get())->getPointer();
		double* extrapolatedSBrhoSPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBrhoSPi_id).get())->getPointer();
		double* extrapolatedSBmxSPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmxSPi_id).get())->getPointer();
		double* SpeedSBi_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBi_1_id).get())->getPointer();
		double* extrapolatedSBrhoSPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBrhoSPj_id).get())->getPointer();
		double* extrapolatedSBmySPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmySPj_id).get())->getPointer();
		double* SpeedSBj_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBj_1_id).get())->getPointer();
		double* extrapolatedSBrhoSPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBrhoSPk_id).get())->getPointer();
		double* extrapolatedSBmzSPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmzSPk_id).get())->getPointer();
		double* SpeedSBk_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBk_1_id).get())->getPointer();
		double* FluxSBimx_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBimx_1_id).get())->getPointer();
		double* extrapolatedSBmxSPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmxSPj_id).get())->getPointer();
		double* FluxSBjmx_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjmx_1_id).get())->getPointer();
		double* extrapolatedSBmxSPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmxSPk_id).get())->getPointer();
		double* FluxSBkmx_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkmx_1_id).get())->getPointer();
		double* extrapolatedSBmySPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmySPi_id).get())->getPointer();
		double* FluxSBimy_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBimy_1_id).get())->getPointer();
		double* FluxSBjmy_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjmy_1_id).get())->getPointer();
		double* extrapolatedSBmySPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmySPk_id).get())->getPointer();
		double* FluxSBkmy_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkmy_1_id).get())->getPointer();
		double* extrapolatedSBmzSPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmzSPi_id).get())->getPointer();
		double* FluxSBimz_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBimz_1_id).get())->getPointer();
		double* extrapolatedSBmzSPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmzSPj_id).get())->getPointer();
		double* FluxSBjmz_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjmz_1_id).get())->getPointer();
		double* FluxSBkmz_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkmz_1_id).get())->getPointer();
		double* mx_p = ((pdat::NodeData<double> *) patch->getPatchData(d_mx_p_id).get())->getPointer();
		double* my_p = ((pdat::NodeData<double> *) patch->getPatchData(d_my_p_id).get())->getPointer();
		double* mz_p = ((pdat::NodeData<double> *) patch->getPatchData(d_mz_p_id).get())->getPointer();
		double* rk1rho = ((pdat::NodeData<double> *) patch->getPatchData(d_rk1rho_id).get())->getPointer();
		double* rk1mx = ((pdat::NodeData<double> *) patch->getPatchData(d_rk1mx_id).get())->getPointer();
		double* rk1my = ((pdat::NodeData<double> *) patch->getPatchData(d_rk1my_id).get())->getPointer();
		double* rk1mz = ((pdat::NodeData<double> *) patch->getPatchData(d_rk1mz_id).get())->getPointer();
		double RHS_Null, d_mx_o0_t0_m0_l0, d_my_o0_t0_m0_l0, d_mz_o0_t0_m0_l0, d_my_o0_t6_m0_l0, d_mx_o0_t4_m0_l0, d_mz_o0_t6_m0_l0, d_mx_o0_t5_m0_l0, d_my_o0_t3_m0_l0, d_mx_o0_t7_m0_l0, d_mx_o0_t1_m0_l0, d_my_o0_t1_m0_l0, d_mz_o0_t1_m0_l0, d_my_o0_t5_m0_l0, d_mz_o0_t3_m0_l0, d_mx_o0_t8_m0_l0, d_mz_o0_t4_m0_l0, d_my_o0_t8_m0_l0, d_mx_o0_t2_m0_l0, d_my_o0_t2_m0_l0, d_mz_o0_t2_m0_l0, RHS_rho, RHS_mx, RHS_my, RHS_mz, m_mz_o0_t6_l0, m_mz_o0_t5_l0, m_mz_o0_t4_l0, m_mz_o0_t3_l0, m_mz_o0_t2_l0, m_mz_o0_t1_l0, m_mz_o0_t0_l0, m_my_o0_t8_l0, m_my_o0_t6_l0, m_my_o0_t5_l0, m_my_o0_t4_l0, m_my_o0_t3_l0, m_my_o0_t2_l0, m_my_o0_t1_l0, m_my_o0_t0_l0, m_mx_o0_t8_l0, m_mx_o0_t7_l0, m_mx_o0_t5_l0, m_mx_o0_t4_l0, m_mx_o0_t3_l0, m_mx_o0_t2_l0, m_mx_o0_t1_l0, m_mx_o0_t0_l0, n_x, n_y, n_z, interaction_index, mod_normal, m_n_var, m_tx_var, m_ty_var, m_tz_var, m_n, m_tx, m_ty, m_tz, cn_p, c_p, fluxAccwnp_3, fluxAccwnm_3, fluxAccwtx_3, fluxAccwty_3, fluxAccwtz_3;
	
		//Get the dimensions of the patch
		const hier::Index boxfirst = patch->getBox().lower();
		const hier::Index boxlast = patch->getBox().upper();
	
		//Get delta spaces into an array. dx, dy, dz.
		std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
		const double* dx  = patch_geom->getDx();
	
		//Auxiliary definitions
		int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
		int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
		int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;
		for(int k = 0; k < klast; k++) {
			for(int j = 0; j < jlast; j++) {
				for(int i = 0; i < ilast; i++) {
					if ((vector(FOV_3, i, j, k) > 0)) {
						RHS_Null = 0.0;
						vector(rk1Null, i, j, k) = RK3P1_(RHS_Null, Null_p, 0.0, i, j, k, dx, simPlat_dt, ilast, jlast);
					}
					if ((vector(FOV_1, i, j, k) > 0) && (i + 2 < ilast && i - 2 >= 0 && j + 2 < jlast && j - 2 >= 0 && k + 2 < klast && k - 2 >= 0)) {
						d_mx_o0_t0_m0_l0 = centereddifferences4thorder_i(ad_mx_o0_t0_m0_l0_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						d_my_o0_t0_m0_l0 = centereddifferences4thordercrossed_ij(ad_mx_o0_t0_m0_l0_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						d_mz_o0_t0_m0_l0 = centereddifferences4thordercrossed_ik(ad_mx_o0_t0_m0_l0_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						d_my_o0_t6_m0_l0 = centereddifferences4thordercrossed_ji(ad_mx_o0_t0_m0_l0_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						d_mx_o0_t4_m0_l0 = centereddifferences4thorder_j(ad_mx_o0_t0_m0_l0_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						d_mz_o0_t6_m0_l0 = centereddifferences4thordercrossed_ki(ad_mx_o0_t0_m0_l0_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						d_mx_o0_t5_m0_l0 = centereddifferences4thorder_k(ad_mx_o0_t0_m0_l0_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						d_my_o0_t3_m0_l0 = centereddifferences4thorder_i(ad_my_o0_t3_m0_l0_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						d_mx_o0_t7_m0_l0 = centereddifferences4thordercrossed_ij(ad_my_o0_t3_m0_l0_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						d_mx_o0_t1_m0_l0 = centereddifferences4thordercrossed_ji(ad_my_o0_t3_m0_l0_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						d_my_o0_t1_m0_l0 = centereddifferences4thorder_j(ad_my_o0_t3_m0_l0_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						d_mz_o0_t1_m0_l0 = centereddifferences4thordercrossed_jk(ad_my_o0_t3_m0_l0_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						d_my_o0_t5_m0_l0 = centereddifferences4thorder_k(ad_my_o0_t3_m0_l0_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						d_mz_o0_t3_m0_l0 = centereddifferences4thorder_i(ad_mz_o0_t3_m0_l0_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						d_mx_o0_t8_m0_l0 = centereddifferences4thordercrossed_ik(ad_mz_o0_t3_m0_l0_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						d_mz_o0_t4_m0_l0 = centereddifferences4thorder_j(ad_mz_o0_t3_m0_l0_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						d_my_o0_t8_m0_l0 = centereddifferences4thordercrossed_jk(ad_mz_o0_t3_m0_l0_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						d_mx_o0_t2_m0_l0 = centereddifferences4thordercrossed_ki(ad_mz_o0_t3_m0_l0_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						d_my_o0_t2_m0_l0 = centereddifferences4thordercrossed_kj(ad_mz_o0_t3_m0_l0_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						d_mz_o0_t2_m0_l0 = centereddifferences4thorder_k(ad_mz_o0_t3_m0_l0_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						RHS_rho = 0.0;
						RHS_mx = vector(rho_p, i, j, k) * vector(fx, i, j, k);
						RHS_my = vector(rho_p, i, j, k) * vector(fy, i, j, k);
						RHS_mz = vector(rho_p, i, j, k) * vector(fz, i, j, k);
						RHS_rho = RHS_rho - FDOC_i(extrapolatedSBrhoSPi, extrapolatedSBmxSPi, SpeedSBi_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						RHS_rho = RHS_rho - FDOC_j(extrapolatedSBrhoSPj, extrapolatedSBmySPj, SpeedSBj_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						RHS_rho = RHS_rho - FDOC_k(extrapolatedSBrhoSPk, extrapolatedSBmzSPk, SpeedSBk_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						RHS_mx = RHS_mx - FDOC_i(extrapolatedSBmxSPi, FluxSBimx_1, SpeedSBi_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						RHS_mx = RHS_mx - FDOC_j(extrapolatedSBmxSPj, FluxSBjmx_1, SpeedSBj_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						RHS_mx = RHS_mx - FDOC_k(extrapolatedSBmxSPk, FluxSBkmx_1, SpeedSBk_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						RHS_my = RHS_my - FDOC_i(extrapolatedSBmySPi, FluxSBimy_1, SpeedSBi_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						RHS_my = RHS_my - FDOC_j(extrapolatedSBmySPj, FluxSBjmy_1, SpeedSBj_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						RHS_my = RHS_my - FDOC_k(extrapolatedSBmySPk, FluxSBkmy_1, SpeedSBk_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						RHS_mz = RHS_mz - FDOC_i(extrapolatedSBmzSPi, FluxSBimz_1, SpeedSBi_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						RHS_mz = RHS_mz - FDOC_j(extrapolatedSBmzSPj, FluxSBjmz_1, SpeedSBj_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						RHS_mz = RHS_mz - FDOC_k(extrapolatedSBmzSPk, FluxSBkmz_1, SpeedSBk_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						m_mz_o0_t6_l0 = mu * d_mz_o0_t6_m0_l0;
						m_mz_o0_t5_l0 = mu * d_mz_o0_t2_m0_l0;
						m_mz_o0_t4_l0 = mu * d_mz_o0_t4_m0_l0;
						m_mz_o0_t3_l0 = mu * d_mz_o0_t3_m0_l0;
						m_mz_o0_t2_l0 = lambda * d_mz_o0_t2_m0_l0;
						m_mz_o0_t1_l0 = lambda * d_mz_o0_t1_m0_l0;
						m_mz_o0_t0_l0 = lambda * d_mz_o0_t0_m0_l0;
						m_my_o0_t8_l0 = mu * d_my_o0_t8_m0_l0;
						m_my_o0_t6_l0 = mu * d_my_o0_t6_m0_l0;
						m_my_o0_t5_l0 = mu * d_my_o0_t5_m0_l0;
						m_my_o0_t4_l0 = mu * d_my_o0_t1_m0_l0;
						m_my_o0_t3_l0 = mu * d_my_o0_t3_m0_l0;
						m_my_o0_t2_l0 = lambda * d_my_o0_t2_m0_l0;
						m_my_o0_t1_l0 = lambda * d_my_o0_t1_m0_l0;
						m_my_o0_t0_l0 = lambda * d_my_o0_t0_m0_l0;
						m_mx_o0_t8_l0 = mu * d_mx_o0_t8_m0_l0;
						m_mx_o0_t7_l0 = mu * d_mx_o0_t7_m0_l0;
						m_mx_o0_t5_l0 = mu * d_mx_o0_t5_m0_l0;
						m_mx_o0_t4_l0 = mu * d_mx_o0_t4_m0_l0;
						m_mx_o0_t3_l0 = mu * d_mx_o0_t0_m0_l0;
						m_mx_o0_t2_l0 = lambda * d_mx_o0_t2_m0_l0;
						m_mx_o0_t1_l0 = lambda * d_mx_o0_t1_m0_l0;
						m_mx_o0_t0_l0 = lambda * d_mx_o0_t0_m0_l0;
						RHS_mx = RHS_mx + ((((((((m_mx_o0_t0_l0 + m_mx_o0_t1_l0) + m_mx_o0_t2_l0) + m_mx_o0_t3_l0) + m_mx_o0_t4_l0) + m_mx_o0_t5_l0) + m_mx_o0_t3_l0) + m_mx_o0_t7_l0) + m_mx_o0_t8_l0);
						RHS_my = RHS_my + ((((((((m_my_o0_t0_l0 + m_my_o0_t1_l0) + m_my_o0_t2_l0) + m_my_o0_t3_l0) + m_my_o0_t4_l0) + m_my_o0_t5_l0) + m_my_o0_t6_l0) + m_my_o0_t4_l0) + m_my_o0_t8_l0);
						RHS_mz = RHS_mz + ((((((((m_mz_o0_t0_l0 + m_mz_o0_t1_l0) + m_mz_o0_t2_l0) + m_mz_o0_t3_l0) + m_mz_o0_t4_l0) + m_mz_o0_t5_l0) + m_mz_o0_t6_l0) + m_mx_o0_t7_l0) + m_mx_o0_t8_l0);
						n_x = 0.0;
						n_y = 0.0;
						n_z = 0.0;
						interaction_index = 0.0;
						if ((equalsEq(n_x, 0.0) && equalsEq(n_y, 0.0)) && equalsEq(n_z, 0.0)) {
							if (vector(FOV_3, i + 1, j, k) > 0.0) {
								interaction_index = 1.0;
								n_x = n_x + 1.0 / dx[0];
							}
							if (vector(FOV_3, i - 1, j, k) > 0.0) {
								interaction_index = 1.0;
								n_x = n_x - 1.0 / dx[0];
							}
							if (vector(FOV_3, i, j + 1, k) > 0.0) {
								interaction_index = 1.0;
								n_y = n_y + 1.0 / dx[1];
							}
							if (vector(FOV_3, i, j - 1, k) > 0.0) {
								interaction_index = 1.0;
								n_y = n_y - 1.0 / dx[1];
							}
							if (vector(FOV_3, i, j, k + 1) > 0.0) {
								interaction_index = 1.0;
								n_z = n_z + 1.0 / dx[2];
							}
							if (vector(FOV_3, i, j, k - 1) > 0.0) {
								interaction_index = 1.0;
								n_z = n_z - 1.0 / dx[2];
							}
						}
						if ((equalsEq(n_x, 0.0) && equalsEq(n_y, 0.0)) && equalsEq(n_z, 0.0)) {
							if (((((((((vector(FOV_3, i + 1, j + 1, k + 1) > 0.0) || (vector(FOV_3, i + 1, j + 1, k - 1) > 0.0)) || (vector(FOV_3, i + 1, j + 1, k) > 0.0)) || (vector(FOV_3, i + 1, j - 1, k + 1) > 0.0)) || (vector(FOV_3, i + 1, j - 1, k - 1) > 0.0)) || (vector(FOV_3, i + 1, j - 1, k) > 0.0)) || (vector(FOV_3, i + 1, j, k + 1) > 0.0)) || (vector(FOV_3, i + 1, j, k - 1) > 0.0)) || (vector(FOV_3, i + 1, j, k) > 0.0)) {
								interaction_index = 1.0;
								n_x = n_x + 1.0 / dx[0];
							}
							if (((((((((vector(FOV_3, i - 1, j + 1, k + 1) > 0.0) || (vector(FOV_3, i - 1, j + 1, k - 1) > 0.0)) || (vector(FOV_3, i - 1, j + 1, k) > 0.0)) || (vector(FOV_3, i - 1, j - 1, k + 1) > 0.0)) || (vector(FOV_3, i - 1, j - 1, k - 1) > 0.0)) || (vector(FOV_3, i - 1, j - 1, k) > 0.0)) || (vector(FOV_3, i - 1, j, k + 1) > 0.0)) || (vector(FOV_3, i - 1, j, k - 1) > 0.0)) || (vector(FOV_3, i - 1, j, k) > 0.0)) {
								interaction_index = 1.0;
								n_x = n_x - 1.0 / dx[0];
							}
							if (((((((((vector(FOV_3, i + 1, j + 1, k + 1) > 0.0) || (vector(FOV_3, i + 1, j + 1, k - 1) > 0.0)) || (vector(FOV_3, i + 1, j + 1, k) > 0.0)) || (vector(FOV_3, i - 1, j + 1, k + 1) > 0.0)) || (vector(FOV_3, i - 1, j + 1, k - 1) > 0.0)) || (vector(FOV_3, i - 1, j + 1, k) > 0.0)) || (vector(FOV_3, i, j + 1, k + 1) > 0.0)) || (vector(FOV_3, i, j + 1, k - 1) > 0.0)) || (vector(FOV_3, i, j + 1, k) > 0.0)) {
								interaction_index = 1.0;
								n_y = n_y + 1.0 / dx[1];
							}
							if (((((((((vector(FOV_3, i + 1, j - 1, k + 1) > 0.0) || (vector(FOV_3, i + 1, j - 1, k - 1) > 0.0)) || (vector(FOV_3, i + 1, j - 1, k) > 0.0)) || (vector(FOV_3, i - 1, j - 1, k + 1) > 0.0)) || (vector(FOV_3, i - 1, j - 1, k - 1) > 0.0)) || (vector(FOV_3, i - 1, j - 1, k) > 0.0)) || (vector(FOV_3, i, j - 1, k + 1) > 0.0)) || (vector(FOV_3, i, j - 1, k - 1) > 0.0)) || (vector(FOV_3, i, j - 1, k) > 0.0)) {
								interaction_index = 1.0;
								n_y = n_y - 1.0 / dx[1];
							}
							if (((((((((vector(FOV_3, i + 1, j + 1, k + 1) > 0.0) || (vector(FOV_3, i + 1, j - 1, k + 1) > 0.0)) || (vector(FOV_3, i + 1, j, k + 1) > 0.0)) || (vector(FOV_3, i - 1, j + 1, k + 1) > 0.0)) || (vector(FOV_3, i - 1, j - 1, k + 1) > 0.0)) || (vector(FOV_3, i - 1, j, k + 1) > 0.0)) || (vector(FOV_3, i, j + 1, k + 1) > 0.0)) || (vector(FOV_3, i, j - 1, k + 1) > 0.0)) || (vector(FOV_3, i, j, k + 1) > 0.0)) {
								interaction_index = 1.0;
								n_z = n_z + 1.0 / dx[2];
							}
							if (((((((((vector(FOV_3, i + 1, j + 1, k - 1) > 0.0) || (vector(FOV_3, i + 1, j - 1, k - 1) > 0.0)) || (vector(FOV_3, i + 1, j, k - 1) > 0.0)) || (vector(FOV_3, i - 1, j + 1, k - 1) > 0.0)) || (vector(FOV_3, i - 1, j - 1, k - 1) > 0.0)) || (vector(FOV_3, i - 1, j, k - 1) > 0.0)) || (vector(FOV_3, i, j + 1, k - 1) > 0.0)) || (vector(FOV_3, i, j - 1, k - 1) > 0.0)) || (vector(FOV_3, i, j, k - 1) > 0.0)) {
								interaction_index = 1.0;
								n_z = n_z - 1.0 / dx[2];
							}
						}
						if ((equalsEq(n_x, 0.0) && equalsEq(n_y, 0.0)) && equalsEq(n_z, 0.0)) {
							if (vector(FOV_xLower, i + 1, j, k) > 0.0) {
								interaction_index = 2.0;
								n_x = n_x + 1.0 / dx[0];
							}
							if (vector(FOV_xLower, i - 1, j, k) > 0.0) {
								interaction_index = 2.0;
								n_x = n_x - 1.0 / dx[0];
							}
							if (vector(FOV_xLower, i, j + 1, k) > 0.0) {
								interaction_index = 2.0;
								n_y = n_y + 1.0 / dx[1];
							}
							if (vector(FOV_xLower, i, j - 1, k) > 0.0) {
								interaction_index = 2.0;
								n_y = n_y - 1.0 / dx[1];
							}
							if (vector(FOV_xLower, i, j, k + 1) > 0.0) {
								interaction_index = 2.0;
								n_z = n_z + 1.0 / dx[2];
							}
							if (vector(FOV_xLower, i, j, k - 1) > 0.0) {
								interaction_index = 2.0;
								n_z = n_z - 1.0 / dx[2];
							}
						}
						if ((equalsEq(n_x, 0.0) && equalsEq(n_y, 0.0)) && equalsEq(n_z, 0.0)) {
							if (((((((((vector(FOV_xLower, i + 1, j + 1, k + 1) > 0.0) || (vector(FOV_xLower, i + 1, j + 1, k - 1) > 0.0)) || (vector(FOV_xLower, i + 1, j + 1, k) > 0.0)) || (vector(FOV_xLower, i + 1, j - 1, k + 1) > 0.0)) || (vector(FOV_xLower, i + 1, j - 1, k - 1) > 0.0)) || (vector(FOV_xLower, i + 1, j - 1, k) > 0.0)) || (vector(FOV_xLower, i + 1, j, k + 1) > 0.0)) || (vector(FOV_xLower, i + 1, j, k - 1) > 0.0)) || (vector(FOV_xLower, i + 1, j, k) > 0.0)) {
								interaction_index = 2.0;
								n_x = n_x + 1.0 / dx[0];
							}
							if (((((((((vector(FOV_xLower, i - 1, j + 1, k + 1) > 0.0) || (vector(FOV_xLower, i - 1, j + 1, k - 1) > 0.0)) || (vector(FOV_xLower, i - 1, j + 1, k) > 0.0)) || (vector(FOV_xLower, i - 1, j - 1, k + 1) > 0.0)) || (vector(FOV_xLower, i - 1, j - 1, k - 1) > 0.0)) || (vector(FOV_xLower, i - 1, j - 1, k) > 0.0)) || (vector(FOV_xLower, i - 1, j, k + 1) > 0.0)) || (vector(FOV_xLower, i - 1, j, k - 1) > 0.0)) || (vector(FOV_xLower, i - 1, j, k) > 0.0)) {
								interaction_index = 2.0;
								n_x = n_x - 1.0 / dx[0];
							}
							if (((((((((vector(FOV_xLower, i + 1, j + 1, k + 1) > 0.0) || (vector(FOV_xLower, i + 1, j + 1, k - 1) > 0.0)) || (vector(FOV_xLower, i + 1, j + 1, k) > 0.0)) || (vector(FOV_xLower, i - 1, j + 1, k + 1) > 0.0)) || (vector(FOV_xLower, i - 1, j + 1, k - 1) > 0.0)) || (vector(FOV_xLower, i - 1, j + 1, k) > 0.0)) || (vector(FOV_xLower, i, j + 1, k + 1) > 0.0)) || (vector(FOV_xLower, i, j + 1, k - 1) > 0.0)) || (vector(FOV_xLower, i, j + 1, k) > 0.0)) {
								interaction_index = 2.0;
								n_y = n_y + 1.0 / dx[1];
							}
							if (((((((((vector(FOV_xLower, i + 1, j - 1, k + 1) > 0.0) || (vector(FOV_xLower, i + 1, j - 1, k - 1) > 0.0)) || (vector(FOV_xLower, i + 1, j - 1, k) > 0.0)) || (vector(FOV_xLower, i - 1, j - 1, k + 1) > 0.0)) || (vector(FOV_xLower, i - 1, j - 1, k - 1) > 0.0)) || (vector(FOV_xLower, i - 1, j - 1, k) > 0.0)) || (vector(FOV_xLower, i, j - 1, k + 1) > 0.0)) || (vector(FOV_xLower, i, j - 1, k - 1) > 0.0)) || (vector(FOV_xLower, i, j - 1, k) > 0.0)) {
								interaction_index = 2.0;
								n_y = n_y - 1.0 / dx[1];
							}
							if (((((((((vector(FOV_xLower, i + 1, j + 1, k + 1) > 0.0) || (vector(FOV_xLower, i + 1, j - 1, k + 1) > 0.0)) || (vector(FOV_xLower, i + 1, j, k + 1) > 0.0)) || (vector(FOV_xLower, i - 1, j + 1, k + 1) > 0.0)) || (vector(FOV_xLower, i - 1, j - 1, k + 1) > 0.0)) || (vector(FOV_xLower, i - 1, j, k + 1) > 0.0)) || (vector(FOV_xLower, i, j + 1, k + 1) > 0.0)) || (vector(FOV_xLower, i, j - 1, k + 1) > 0.0)) || (vector(FOV_xLower, i, j, k + 1) > 0.0)) {
								interaction_index = 2.0;
								n_z = n_z + 1.0 / dx[2];
							}
							if (((((((((vector(FOV_xLower, i + 1, j + 1, k - 1) > 0.0) || (vector(FOV_xLower, i + 1, j - 1, k - 1) > 0.0)) || (vector(FOV_xLower, i + 1, j, k - 1) > 0.0)) || (vector(FOV_xLower, i - 1, j + 1, k - 1) > 0.0)) || (vector(FOV_xLower, i - 1, j - 1, k - 1) > 0.0)) || (vector(FOV_xLower, i - 1, j, k - 1) > 0.0)) || (vector(FOV_xLower, i, j + 1, k - 1) > 0.0)) || (vector(FOV_xLower, i, j - 1, k - 1) > 0.0)) || (vector(FOV_xLower, i, j, k - 1) > 0.0)) {
								interaction_index = 2.0;
								n_z = n_z - 1.0 / dx[2];
							}
						}
						if ((equalsEq(n_x, 0.0) && equalsEq(n_y, 0.0)) && equalsEq(n_z, 0.0)) {
							if (vector(FOV_xUpper, i + 1, j, k) > 0.0) {
								interaction_index = 3.0;
								n_x = n_x + 1.0 / dx[0];
							}
							if (vector(FOV_xUpper, i - 1, j, k) > 0.0) {
								interaction_index = 3.0;
								n_x = n_x - 1.0 / dx[0];
							}
							if (vector(FOV_xUpper, i, j + 1, k) > 0.0) {
								interaction_index = 3.0;
								n_y = n_y + 1.0 / dx[1];
							}
							if (vector(FOV_xUpper, i, j - 1, k) > 0.0) {
								interaction_index = 3.0;
								n_y = n_y - 1.0 / dx[1];
							}
							if (vector(FOV_xUpper, i, j, k + 1) > 0.0) {
								interaction_index = 3.0;
								n_z = n_z + 1.0 / dx[2];
							}
							if (vector(FOV_xUpper, i, j, k - 1) > 0.0) {
								interaction_index = 3.0;
								n_z = n_z - 1.0 / dx[2];
							}
						}
						if ((equalsEq(n_x, 0.0) && equalsEq(n_y, 0.0)) && equalsEq(n_z, 0.0)) {
							if (((((((((vector(FOV_xUpper, i + 1, j + 1, k + 1) > 0.0) || (vector(FOV_xUpper, i + 1, j + 1, k - 1) > 0.0)) || (vector(FOV_xUpper, i + 1, j + 1, k) > 0.0)) || (vector(FOV_xUpper, i + 1, j - 1, k + 1) > 0.0)) || (vector(FOV_xUpper, i + 1, j - 1, k - 1) > 0.0)) || (vector(FOV_xUpper, i + 1, j - 1, k) > 0.0)) || (vector(FOV_xUpper, i + 1, j, k + 1) > 0.0)) || (vector(FOV_xUpper, i + 1, j, k - 1) > 0.0)) || (vector(FOV_xUpper, i + 1, j, k) > 0.0)) {
								interaction_index = 3.0;
								n_x = n_x + 1.0 / dx[0];
							}
							if (((((((((vector(FOV_xUpper, i - 1, j + 1, k + 1) > 0.0) || (vector(FOV_xUpper, i - 1, j + 1, k - 1) > 0.0)) || (vector(FOV_xUpper, i - 1, j + 1, k) > 0.0)) || (vector(FOV_xUpper, i - 1, j - 1, k + 1) > 0.0)) || (vector(FOV_xUpper, i - 1, j - 1, k - 1) > 0.0)) || (vector(FOV_xUpper, i - 1, j - 1, k) > 0.0)) || (vector(FOV_xUpper, i - 1, j, k + 1) > 0.0)) || (vector(FOV_xUpper, i - 1, j, k - 1) > 0.0)) || (vector(FOV_xUpper, i - 1, j, k) > 0.0)) {
								interaction_index = 3.0;
								n_x = n_x - 1.0 / dx[0];
							}
							if (((((((((vector(FOV_xUpper, i + 1, j + 1, k + 1) > 0.0) || (vector(FOV_xUpper, i + 1, j + 1, k - 1) > 0.0)) || (vector(FOV_xUpper, i + 1, j + 1, k) > 0.0)) || (vector(FOV_xUpper, i - 1, j + 1, k + 1) > 0.0)) || (vector(FOV_xUpper, i - 1, j + 1, k - 1) > 0.0)) || (vector(FOV_xUpper, i - 1, j + 1, k) > 0.0)) || (vector(FOV_xUpper, i, j + 1, k + 1) > 0.0)) || (vector(FOV_xUpper, i, j + 1, k - 1) > 0.0)) || (vector(FOV_xUpper, i, j + 1, k) > 0.0)) {
								interaction_index = 3.0;
								n_y = n_y + 1.0 / dx[1];
							}
							if (((((((((vector(FOV_xUpper, i + 1, j - 1, k + 1) > 0.0) || (vector(FOV_xUpper, i + 1, j - 1, k - 1) > 0.0)) || (vector(FOV_xUpper, i + 1, j - 1, k) > 0.0)) || (vector(FOV_xUpper, i - 1, j - 1, k + 1) > 0.0)) || (vector(FOV_xUpper, i - 1, j - 1, k - 1) > 0.0)) || (vector(FOV_xUpper, i - 1, j - 1, k) > 0.0)) || (vector(FOV_xUpper, i, j - 1, k + 1) > 0.0)) || (vector(FOV_xUpper, i, j - 1, k - 1) > 0.0)) || (vector(FOV_xUpper, i, j - 1, k) > 0.0)) {
								interaction_index = 3.0;
								n_y = n_y - 1.0 / dx[1];
							}
							if (((((((((vector(FOV_xUpper, i + 1, j + 1, k + 1) > 0.0) || (vector(FOV_xUpper, i + 1, j - 1, k + 1) > 0.0)) || (vector(FOV_xUpper, i + 1, j, k + 1) > 0.0)) || (vector(FOV_xUpper, i - 1, j + 1, k + 1) > 0.0)) || (vector(FOV_xUpper, i - 1, j - 1, k + 1) > 0.0)) || (vector(FOV_xUpper, i - 1, j, k + 1) > 0.0)) || (vector(FOV_xUpper, i, j + 1, k + 1) > 0.0)) || (vector(FOV_xUpper, i, j - 1, k + 1) > 0.0)) || (vector(FOV_xUpper, i, j, k + 1) > 0.0)) {
								interaction_index = 3.0;
								n_z = n_z + 1.0 / dx[2];
							}
							if (((((((((vector(FOV_xUpper, i + 1, j + 1, k - 1) > 0.0) || (vector(FOV_xUpper, i + 1, j - 1, k - 1) > 0.0)) || (vector(FOV_xUpper, i + 1, j, k - 1) > 0.0)) || (vector(FOV_xUpper, i - 1, j + 1, k - 1) > 0.0)) || (vector(FOV_xUpper, i - 1, j - 1, k - 1) > 0.0)) || (vector(FOV_xUpper, i - 1, j, k - 1) > 0.0)) || (vector(FOV_xUpper, i, j + 1, k - 1) > 0.0)) || (vector(FOV_xUpper, i, j - 1, k - 1) > 0.0)) || (vector(FOV_xUpper, i, j, k - 1) > 0.0)) {
								interaction_index = 3.0;
								n_z = n_z - 1.0 / dx[2];
							}
						}
						if ((!equalsEq(n_x, 0.0) || !equalsEq(n_y, 0.0)) || !equalsEq(n_z, 0.0)) {
							mod_normal = sqrt((n_x * n_x + n_y * n_y) + n_z * n_z);
							n_x = n_x / mod_normal;
							n_y = n_y / mod_normal;
							n_z = n_z / mod_normal;
						}
						if (equalsEq(interaction_index, 1.0)) {
							m_n_var = RHS_mx * n_x + RHS_my * n_y + RHS_mz * n_z;
							m_tx_var = RHS_mx - (RHS_mx * n_x + RHS_my * n_y + RHS_mz * n_z) * n_x;
							m_ty_var = RHS_my - (RHS_mx * n_x + RHS_my * n_y + RHS_mz * n_z) * n_y;
							m_tz_var = RHS_mz - (RHS_mx * n_x + RHS_my * n_y + RHS_mz * n_z) * n_z;
							m_n = vector(mx_p, i, j, k) * n_x + vector(my_p, i, j, k) * n_y + vector(mz_p, i, j, k) * n_z;
							m_tx = vector(mx_p, i, j, k) - (vector(mx_p, i, j, k) * n_x + vector(my_p, i, j, k) * n_y + vector(mz_p, i, j, k) * n_z) * n_x;
							m_ty = vector(my_p, i, j, k) - (vector(mx_p, i, j, k) * n_x + vector(my_p, i, j, k) * n_y + vector(mz_p, i, j, k) * n_z) * n_y;
							m_tz = vector(mz_p, i, j, k) - (vector(mx_p, i, j, k) * n_x + vector(my_p, i, j, k) * n_y + vector(mz_p, i, j, k) * n_z) * n_z;
							cn_p = m_n / vector(rho_p, i, j, k);
							c_p = c0 * (pow((vector(rho_p, i, j, k) / rho0), ((gamma + (-1.0)) / 2.0)));
							if ((c_p + cn_p) > 0.0) {
								fluxAccwnp_3 = RHS_rho * (-((-c_p) + cn_p)) + m_n_var;
							} else {
								fluxAccwnp_3 = (-m_n_var) + RHS_rho * (-((-c_p) + cn_p));
							}
							if (((-c_p) + cn_p) > 0.0) {
								fluxAccwnm_3 = RHS_rho * (-(c_p + cn_p)) + m_n_var;
							} else {
								fluxAccwnm_3 = (-m_n_var) + RHS_rho * (-(c_p + cn_p));
							}
							if (cn_p > 0.0) {
								fluxAccwtx_3 = RHS_rho * (-m_tx) / vector(rho_p, i, j, k) + m_tx_var;
							} else {
								fluxAccwtx_3 = RHS_rho * (-m_tx) / vector(rho_p, i, j, k);
							}
							if (cn_p > 0.0) {
								fluxAccwty_3 = RHS_rho * (-m_ty) / vector(rho_p, i, j, k) + m_ty_var;
							} else {
								fluxAccwty_3 = RHS_rho * (-m_ty) / vector(rho_p, i, j, k);
							}
							if (cn_p > 0.0) {
								fluxAccwtz_3 = RHS_rho * (-m_tz) / vector(rho_p, i, j, k) + m_tz_var;
							} else {
								fluxAccwtz_3 = RHS_rho * (-m_tz) / vector(rho_p, i, j, k);
							}
							RHS_rho = fluxAccwnp_3 * 1.0 / (2.0 * c_p) + fluxAccwnm_3 * (-1.0) / (2.0 * c_p);
							m_n_var = fluxAccwnp_3 * (cn_p / (2.0 * c_p) + 1.0 / 2.0) + fluxAccwnm_3 * (-(cn_p / (2.0 * c_p) + (-1.0 / 2.0)));
							m_tx_var = (fluxAccwnp_3 * m_tx / (2.0 * vector(rho_p, i, j, k) * c_p) + fluxAccwnm_3 * (-m_tx) / (2.0 * vector(rho_p, i, j, k) * c_p)) + fluxAccwtx_3;
							m_ty_var = (fluxAccwnp_3 * m_ty / (2.0 * vector(rho_p, i, j, k) * c_p) + fluxAccwnm_3 * (-m_ty) / (2.0 * vector(rho_p, i, j, k) * c_p)) + fluxAccwty_3;
							m_tz_var = (fluxAccwnp_3 * m_tz / (2.0 * vector(rho_p, i, j, k) * c_p) + fluxAccwnm_3 * (-m_tz) / (2.0 * vector(rho_p, i, j, k) * c_p)) + fluxAccwtz_3;
							RHS_mx = m_tx_var + m_n_var * n_x;
							RHS_my = m_ty_var + m_n_var * n_y;
							RHS_mz = m_tz_var + m_n_var * n_z;
						}
						if (equalsEq(interaction_index, 2.0)) {
							m_n_var = RHS_mx * n_x + RHS_my * n_y + RHS_mz * n_z;
							m_tx_var = RHS_mx - (RHS_mx * n_x + RHS_my * n_y + RHS_mz * n_z) * n_x;
							m_ty_var = RHS_my - (RHS_mx * n_x + RHS_my * n_y + RHS_mz * n_z) * n_y;
							m_tz_var = RHS_mz - (RHS_mx * n_x + RHS_my * n_y + RHS_mz * n_z) * n_z;
							m_n = vector(mx_p, i, j, k) * n_x + vector(my_p, i, j, k) * n_y + vector(mz_p, i, j, k) * n_z;
							m_tx = vector(mx_p, i, j, k) - (vector(mx_p, i, j, k) * n_x + vector(my_p, i, j, k) * n_y + vector(mz_p, i, j, k) * n_z) * n_x;
							m_ty = vector(my_p, i, j, k) - (vector(mx_p, i, j, k) * n_x + vector(my_p, i, j, k) * n_y + vector(mz_p, i, j, k) * n_z) * n_y;
							m_tz = vector(mz_p, i, j, k) - (vector(mx_p, i, j, k) * n_x + vector(my_p, i, j, k) * n_y + vector(mz_p, i, j, k) * n_z) * n_z;
							cn_p = m_n / vector(rho_p, i, j, k);
							c_p = c0 * (pow((vector(rho_p, i, j, k) / rho0), ((gamma + (-1.0)) / 2.0)));
							if ((c_p + cn_p) > 0.0) {
								fluxAccwnp_3 = RHS_rho * (-((-c_p) + cn_p)) + m_n_var;
							} else {
								fluxAccwnp_3 = (-lambdain) * (m_n + vector(rho_p, i, j, k) * v0 * (1.0 - (ycoord(j) * ycoord(j) + zcoord(k) * zcoord(k)) / ((floor((0.006 * fabs(ycoord(j))) / (sqrt(ycoord(j) * ycoord(j) + zcoord(k) * zcoord(k)) * dx[1]) + 0.5) * dx[1]) * (floor((0.006 * fabs(ycoord(j))) / (sqrt(ycoord(j) * ycoord(j) + zcoord(k) * zcoord(k)) * dx[1]) + 0.5) * dx[1]) + (floor((0.006 * fabs(zcoord(k))) / (sqrt(ycoord(j) * ycoord(j) + zcoord(k) * zcoord(k)) * dx[2]) + 0.5) * dx[2]) * (floor((0.006 * fabs(zcoord(k))) / (sqrt(ycoord(j) * ycoord(j) + zcoord(k) * zcoord(k)) * dx[2]) + 0.5) * dx[2]))));
							}
							if (((-c_p) + cn_p) > 0.0) {
								fluxAccwnm_3 = RHS_rho * (-(c_p + cn_p)) + m_n_var;
							} else {
								fluxAccwnm_3 = (-lambdain) * (m_n + vector(rho_p, i, j, k) * v0 * (1.0 - (ycoord(j) * ycoord(j) + zcoord(k) * zcoord(k)) / ((floor((0.006 * fabs(ycoord(j))) / (sqrt(ycoord(j) * ycoord(j) + zcoord(k) * zcoord(k)) * dx[1]) + 0.5) * dx[1]) * (floor((0.006 * fabs(ycoord(j))) / (sqrt(ycoord(j) * ycoord(j) + zcoord(k) * zcoord(k)) * dx[1]) + 0.5) * dx[1]) + (floor((0.006 * fabs(zcoord(k))) / (sqrt(ycoord(j) * ycoord(j) + zcoord(k) * zcoord(k)) * dx[2]) + 0.5) * dx[2]) * (floor((0.006 * fabs(zcoord(k))) / (sqrt(ycoord(j) * ycoord(j) + zcoord(k) * zcoord(k)) * dx[2]) + 0.5) * dx[2]))));
							}
							if (cn_p > 0.0) {
								fluxAccwtx_3 = RHS_rho * (-m_tx) / vector(rho_p, i, j, k) + m_tx_var;
							} else {
								fluxAccwtx_3 = (-lambdain) * (m_tx - vector(rho_p, i, j, k) * vt0);
							}
							if (cn_p > 0.0) {
								fluxAccwty_3 = RHS_rho * (-m_ty) / vector(rho_p, i, j, k) + m_ty_var;
							} else {
								fluxAccwty_3 = (-lambdain) * (m_ty - vector(rho_p, i, j, k) * vt0);
							}
							if (cn_p > 0.0) {
								fluxAccwtz_3 = RHS_rho * (-m_tz) / vector(rho_p, i, j, k) + m_tz_var;
							} else {
								fluxAccwtz_3 = (-lambdain) * (m_tz - vector(rho_p, i, j, k) * vt0);
							}
							RHS_rho = fluxAccwnp_3 * 1.0 / (2.0 * c_p) + fluxAccwnm_3 * (-1.0) / (2.0 * c_p);
							m_n_var = fluxAccwnp_3 * (cn_p / (2.0 * c_p) + 1.0 / 2.0) + fluxAccwnm_3 * (-(cn_p / (2.0 * c_p) + (-1.0 / 2.0)));
							m_tx_var = (fluxAccwnp_3 * m_tx / (2.0 * vector(rho_p, i, j, k) * c_p) + fluxAccwnm_3 * (-m_tx) / (2.0 * vector(rho_p, i, j, k) * c_p)) + fluxAccwtx_3;
							m_ty_var = (fluxAccwnp_3 * m_ty / (2.0 * vector(rho_p, i, j, k) * c_p) + fluxAccwnm_3 * (-m_ty) / (2.0 * vector(rho_p, i, j, k) * c_p)) + fluxAccwty_3;
							m_tz_var = (fluxAccwnp_3 * m_tz / (2.0 * vector(rho_p, i, j, k) * c_p) + fluxAccwnm_3 * (-m_tz) / (2.0 * vector(rho_p, i, j, k) * c_p)) + fluxAccwtz_3;
							RHS_mx = m_tx_var + m_n_var * n_x;
							RHS_my = m_ty_var + m_n_var * n_y;
							RHS_mz = m_tz_var + m_n_var * n_z;
						}
						if (equalsEq(interaction_index, 3.0)) {
							m_n_var = RHS_mx * n_x + RHS_my * n_y + RHS_mz * n_z;
							m_tx_var = RHS_mx - (RHS_mx * n_x + RHS_my * n_y + RHS_mz * n_z) * n_x;
							m_ty_var = RHS_my - (RHS_mx * n_x + RHS_my * n_y + RHS_mz * n_z) * n_y;
							m_tz_var = RHS_mz - (RHS_mx * n_x + RHS_my * n_y + RHS_mz * n_z) * n_z;
							m_n = vector(mx_p, i, j, k) * n_x + vector(my_p, i, j, k) * n_y + vector(mz_p, i, j, k) * n_z;
							m_tx = vector(mx_p, i, j, k) - (vector(mx_p, i, j, k) * n_x + vector(my_p, i, j, k) * n_y + vector(mz_p, i, j, k) * n_z) * n_x;
							m_ty = vector(my_p, i, j, k) - (vector(mx_p, i, j, k) * n_x + vector(my_p, i, j, k) * n_y + vector(mz_p, i, j, k) * n_z) * n_y;
							m_tz = vector(mz_p, i, j, k) - (vector(mx_p, i, j, k) * n_x + vector(my_p, i, j, k) * n_y + vector(mz_p, i, j, k) * n_z) * n_z;
							cn_p = m_n / vector(rho_p, i, j, k);
							c_p = c0 * (pow((vector(rho_p, i, j, k) / rho0), ((gamma + (-1.0)) / 2.0)));
							if ((c_p + cn_p) > 0.0) {
								fluxAccwnp_3 = RHS_rho * (-((-c_p) + cn_p)) + m_n_var;
							} else {
								fluxAccwnp_3 = ((-c_p) + cn_p) * lambdaout * (vector(rho_p, i, j, k) - rho0);
							}
							if (((-c_p) + cn_p) > 0.0) {
								fluxAccwnm_3 = RHS_rho * (-(c_p + cn_p)) + m_n_var;
							} else {
								fluxAccwnm_3 = (c_p + cn_p) * lambdaout * (vector(rho_p, i, j, k) - rho0);
							}
							if (cn_p > 0.0) {
								fluxAccwtx_3 = RHS_rho * (-m_tx) / vector(rho_p, i, j, k) + m_tx_var;
							} else {
								fluxAccwtx_3 = m_tx / vector(rho_p, i, j, k) * lambdaout * (vector(rho_p, i, j, k) - rho0);
							}
							if (cn_p > 0.0) {
								fluxAccwty_3 = RHS_rho * (-m_ty) / vector(rho_p, i, j, k) + m_ty_var;
							} else {
								fluxAccwty_3 = m_ty / vector(rho_p, i, j, k) * lambdaout * (vector(rho_p, i, j, k) - rho0);
							}
							if (cn_p > 0.0) {
								fluxAccwtz_3 = RHS_rho * (-m_tz) / vector(rho_p, i, j, k) + m_tz_var;
							} else {
								fluxAccwtz_3 = m_tz / vector(rho_p, i, j, k) * lambdaout * (vector(rho_p, i, j, k) - rho0);
							}
							RHS_rho = fluxAccwnp_3 * 1.0 / (2.0 * c_p) + fluxAccwnm_3 * (-1.0) / (2.0 * c_p);
							m_n_var = fluxAccwnp_3 * (cn_p / (2.0 * c_p) + 1.0 / 2.0) + fluxAccwnm_3 * (-(cn_p / (2.0 * c_p) + (-1.0 / 2.0)));
							m_tx_var = (fluxAccwnp_3 * m_tx / (2.0 * vector(rho_p, i, j, k) * c_p) + fluxAccwnm_3 * (-m_tx) / (2.0 * vector(rho_p, i, j, k) * c_p)) + fluxAccwtx_3;
							m_ty_var = (fluxAccwnp_3 * m_ty / (2.0 * vector(rho_p, i, j, k) * c_p) + fluxAccwnm_3 * (-m_ty) / (2.0 * vector(rho_p, i, j, k) * c_p)) + fluxAccwty_3;
							m_tz_var = (fluxAccwnp_3 * m_tz / (2.0 * vector(rho_p, i, j, k) * c_p) + fluxAccwnm_3 * (-m_tz) / (2.0 * vector(rho_p, i, j, k) * c_p)) + fluxAccwtz_3;
							RHS_mx = m_tx_var + m_n_var * n_x;
							RHS_my = m_ty_var + m_n_var * n_y;
							RHS_mz = m_tz_var + m_n_var * n_z;
						}
						vector(rk1rho, i, j, k) = RK3P1_(RHS_rho, rho_p, 0.0, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(rk1mx, i, j, k) = RK3P1_(RHS_mx, mx_p, 0.0, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(rk1my, i, j, k) = RK3P1_(RHS_my, my_p, 0.0, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(rk1mz, i, j, k) = RK3P1_(RHS_mz, mz_p, 0.0, i, j, k, dx, simPlat_dt, ilast, jlast);
					}
				}
			}
		}
	}
	//Fill ghosts and periodical boundaries
	time_interpolate_operator_mesh1->setStep(1);
	d_bdry_sched_advance3[ln]->fillData(current_time + simPlat_dt, false);
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch >& patch = *p_it;
		double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_3 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_3_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
		double* FOV_zLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zLower_id).get())->getPointer();
		double* FOV_zUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zUpper_id).get())->getPointer();
	
		double* fz = ((pdat::NodeData<double> *) patch->getPatchData(d_fz_id).get())->getPointer();
		double* fy = ((pdat::NodeData<double> *) patch->getPatchData(d_fy_id).get())->getPointer();
		double* fx = ((pdat::NodeData<double> *) patch->getPatchData(d_fx_id).get())->getPointer();
		double* p = ((pdat::NodeData<double> *) patch->getPatchData(d_p_id).get())->getPointer();
		double* rk1rho = ((pdat::NodeData<double> *) patch->getPatchData(d_rk1rho_id).get())->getPointer();
	
		//Get the dimensions of the patch
		const hier::Index boxfirst = patch->getBox().lower();
		const hier::Index boxlast = patch->getBox().upper();
	
		//Get delta spaces into an array. dx, dy, dz.
		std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
		const double* dx  = patch_geom->getDx();
	
		//Auxiliary definitions
		int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
		int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
		int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;
		for(int k = 0; k < klast; k++) {
			for(int j = 0; j < jlast; j++) {
				for(int i = 0; i < ilast; i++) {
					if ((vector(FOV_1, i, j, k) > 0) && ((i + 2 < ilast || !patch->getPatchGeometry()->getTouchesRegularBoundary(0, 1)) && (i - 2 >= 0 || !patch->getPatchGeometry()->getTouchesRegularBoundary(0, 0)) && (j + 2 < jlast || !patch->getPatchGeometry()->getTouchesRegularBoundary(1, 1)) && (j - 2 >= 0 || !patch->getPatchGeometry()->getTouchesRegularBoundary(1, 0)) && (k + 2 < klast || !patch->getPatchGeometry()->getTouchesRegularBoundary(2, 1)) && (k - 2 >= 0 || !patch->getPatchGeometry()->getTouchesRegularBoundary(2, 0)))) {
						vector(fz, i, j, k) = pfz;
						vector(fy, i, j, k) = pfy;
						vector(fx, i, j, k) = pfx;
						vector(p, i, j, k) = (Paux + (pow((vector(rk1rho, i, j, k) / rho0), gamma)) * (rho0 * (c0 * c0)) / gamma) - (rho0 * (c0 * c0)) / gamma;
					}
				}
			}
		}
	}
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch >& patch = *p_it;
		double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_3 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_3_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
		double* FOV_zLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zLower_id).get())->getPointer();
		double* FOV_zUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zUpper_id).get())->getPointer();
	
		//Hard region field distance variables
		double* d_i_rho_mx_my_mz_p_fx_fy_fz = ((pdat::NodeData<double> *) patch->getPatchData(d_d_i_rho_mx_my_mz_p_fx_fy_fz_id).get())->getPointer();
		double* d_j_rho_mx_my_mz_p_fx_fy_fz = ((pdat::NodeData<double> *) patch->getPatchData(d_d_j_rho_mx_my_mz_p_fx_fy_fz_id).get())->getPointer();
		double* d_k_rho_mx_my_mz_p_fx_fy_fz = ((pdat::NodeData<double> *) patch->getPatchData(d_d_k_rho_mx_my_mz_p_fx_fy_fz_id).get())->getPointer();
		double* d_i_Null = ((pdat::NodeData<double> *) patch->getPatchData(d_d_i_Null_id).get())->getPointer();
		double* d_j_Null = ((pdat::NodeData<double> *) patch->getPatchData(d_d_j_Null_id).get())->getPointer();
		double* d_k_Null = ((pdat::NodeData<double> *) patch->getPatchData(d_d_k_Null_id).get())->getPointer();
	
		double* rk1Null = ((pdat::NodeData<double> *) patch->getPatchData(d_rk1Null_id).get())->getPointer();
		double* extrapolatedSBrhoSPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBrhoSPi_id).get())->getPointer();
		double* extrapolatedSBrhoSPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBrhoSPj_id).get())->getPointer();
		double* extrapolatedSBrhoSPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBrhoSPk_id).get())->getPointer();
		double* rk1rho = ((pdat::NodeData<double> *) patch->getPatchData(d_rk1rho_id).get())->getPointer();
		double* extrapolatedSBmxSPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmxSPi_id).get())->getPointer();
		double* extrapolatedSBmxSPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmxSPj_id).get())->getPointer();
		double* extrapolatedSBmxSPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmxSPk_id).get())->getPointer();
		double* rk1mx = ((pdat::NodeData<double> *) patch->getPatchData(d_rk1mx_id).get())->getPointer();
		double* extrapolatedSBmySPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmySPi_id).get())->getPointer();
		double* extrapolatedSBmySPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmySPj_id).get())->getPointer();
		double* extrapolatedSBmySPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmySPk_id).get())->getPointer();
		double* rk1my = ((pdat::NodeData<double> *) patch->getPatchData(d_rk1my_id).get())->getPointer();
		double* extrapolatedSBmzSPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmzSPi_id).get())->getPointer();
		double* extrapolatedSBmzSPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmzSPj_id).get())->getPointer();
		double* extrapolatedSBmzSPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmzSPk_id).get())->getPointer();
		double* rk1mz = ((pdat::NodeData<double> *) patch->getPatchData(d_rk1mz_id).get())->getPointer();
		double* extrapolatedSBpSPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBpSPi_id).get())->getPointer();
		double* extrapolatedSBpSPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBpSPj_id).get())->getPointer();
		double* extrapolatedSBpSPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBpSPk_id).get())->getPointer();
		double* p = ((pdat::NodeData<double> *) patch->getPatchData(d_p_id).get())->getPointer();
		double* extrapolatedSBfxSPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBfxSPi_id).get())->getPointer();
		double* extrapolatedSBfxSPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBfxSPj_id).get())->getPointer();
		double* extrapolatedSBfxSPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBfxSPk_id).get())->getPointer();
		double* fx = ((pdat::NodeData<double> *) patch->getPatchData(d_fx_id).get())->getPointer();
		double* extrapolatedSBfySPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBfySPi_id).get())->getPointer();
		double* extrapolatedSBfySPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBfySPj_id).get())->getPointer();
		double* extrapolatedSBfySPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBfySPk_id).get())->getPointer();
		double* fy = ((pdat::NodeData<double> *) patch->getPatchData(d_fy_id).get())->getPointer();
		double* extrapolatedSBfzSPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBfzSPi_id).get())->getPointer();
		double* extrapolatedSBfzSPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBfzSPj_id).get())->getPointer();
		double* extrapolatedSBfzSPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBfzSPk_id).get())->getPointer();
		double* fz = ((pdat::NodeData<double> *) patch->getPatchData(d_fz_id).get())->getPointer();
		double* extrapolatedSBNullSPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBNullSPi_id).get())->getPointer();
		double* extrapolatedSBNullSPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBNullSPj_id).get())->getPointer();
		double* extrapolatedSBNullSPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBNullSPk_id).get())->getPointer();
	
		//Get the dimensions of the patch
		const hier::Index boxfirst = patch->getBox().lower();
		const hier::Index boxlast = patch->getBox().upper();
	
		//Get delta spaces into an array. dx, dy, dz.
		std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
		const double* dx  = patch_geom->getDx();
	
		//Auxiliary definitions
		int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
		int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
		int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;
		for(int k = 0; k < klast; k++) {
			for(int j = 0; j < jlast; j++) {
				for(int i = 0; i < ilast; i++) {
						vector(extrapolatedSBrhoSPi, i, j, k) = vector(rk1rho, i, j, k);
						vector(extrapolatedSBrhoSPj, i, j, k) = vector(rk1rho, i, j, k);
						vector(extrapolatedSBrhoSPk, i, j, k) = vector(rk1rho, i, j, k);
						vector(extrapolatedSBmxSPi, i, j, k) = vector(rk1mx, i, j, k);
						vector(extrapolatedSBmxSPj, i, j, k) = vector(rk1mx, i, j, k);
						vector(extrapolatedSBmxSPk, i, j, k) = vector(rk1mx, i, j, k);
						vector(extrapolatedSBmySPi, i, j, k) = vector(rk1my, i, j, k);
						vector(extrapolatedSBmySPj, i, j, k) = vector(rk1my, i, j, k);
						vector(extrapolatedSBmySPk, i, j, k) = vector(rk1my, i, j, k);
						vector(extrapolatedSBmzSPi, i, j, k) = vector(rk1mz, i, j, k);
						vector(extrapolatedSBmzSPj, i, j, k) = vector(rk1mz, i, j, k);
						vector(extrapolatedSBmzSPk, i, j, k) = vector(rk1mz, i, j, k);
						vector(extrapolatedSBNullSPi, i, j, k) = vector(rk1Null, i, j, k);
						vector(extrapolatedSBNullSPj, i, j, k) = vector(rk1Null, i, j, k);
						vector(extrapolatedSBNullSPk, i, j, k) = vector(rk1Null, i, j, k);
						vector(extrapolatedSBpSPi, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBpSPj, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBpSPk, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBfxSPi, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfxSPj, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfxSPk, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfySPi, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfySPj, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfySPk, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfzSPi, i, j, k) = vector(fz, i, j, k);
						vector(extrapolatedSBfzSPj, i, j, k) = vector(fz, i, j, k);
						vector(extrapolatedSBfzSPk, i, j, k) = vector(fz, i, j, k);
				}
			}
		}
		for(int k = 0; k < klast; k++) {
			for(int j = 0; j < jlast; j++) {
				for(int i = 0; i < ilast; i++) {
					if ((vector(FOV_xLower, i, j, k) > 0) && ((i + 1 < ilast && (vector(FOV_3,i + 1, j, k) > 0)) || (i + 2 < ilast && (vector(FOV_3,i + 2, j, k) > 0)))) {
						vector(rk1Null, i, j, k) = 0.0;
					}
					if ((vector(FOV_xUpper, i, j, k) > 0) && ((i - 1 >= 0 && (vector(FOV_3, i - 1, j, k) > 0)) || (i - 2 >= 0 && (vector(FOV_3, i - 2, j, k) > 0)))) {
						vector(rk1Null, i, j, k) = 0.0;
					}
					if ((vector(FOV_yLower, i, j, k) > 0) && ((j + 1 < jlast && (vector(FOV_3,i, j + 1, k) > 0)) || (j + 2 < jlast && (vector(FOV_3,i, j + 2, k) > 0)))) {
						vector(rk1Null, i, j, k) = 0.0;
					}
					if ((vector(FOV_yUpper, i, j, k) > 0) && ((j - 1 >= 0 && (vector(FOV_3, i, j - 1, k) > 0)) || (j - 2 >= 0 && (vector(FOV_3, i, j - 2, k) > 0)))) {
						vector(rk1Null, i, j, k) = 0.0;
					}
					if ((vector(FOV_zLower, i, j, k) > 0) && ((k + 1 < klast && (vector(FOV_3,i, j, k + 1) > 0)) || (k + 2 < klast && (vector(FOV_3,i, j, k + 2) > 0)))) {
						vector(rk1Null, i, j, k) = 0.0;
					}
					if ((vector(FOV_zUpper, i, j, k) > 0) && ((k - 1 >= 0 && (vector(FOV_3, i, j, k - 1) > 0)) || (k - 2 >= 0 && (vector(FOV_3, i, j, k - 2) > 0)))) {
						vector(rk1Null, i, j, k) = 0.0;
					}
					if (vector(FOV_3, i, j, k) > 0) {
						//Region field extrapolations
						if ((vector(d_i_rho_mx_my_mz_p_fx_fy_fz, i, j, k) != 0 || vector(d_j_rho_mx_my_mz_p_fx_fy_fz, i, j, k) != 0 || vector(d_k_rho_mx_my_mz_p_fx_fy_fz, i, j, k) != 0)) {
							extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBrhoSPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBrhoSPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBrhoSPk, k, rk1rho, FOV_1, dx, ilast, jlast);
							extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmxSPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmxSPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmxSPk, k, rk1mx, FOV_1, dx, ilast, jlast);
							extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmySPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmySPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmySPk, k, rk1my, FOV_1, dx, ilast, jlast);
							extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmzSPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmzSPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmzSPk, k, rk1mz, FOV_1, dx, ilast, jlast);
							extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBpSPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBpSPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBpSPk, k, p, FOV_1, dx, ilast, jlast);
							extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfxSPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfxSPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfxSPk, k, fx, FOV_1, dx, ilast, jlast);
							extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfySPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfySPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfySPk, k, fy, FOV_1, dx, ilast, jlast);
							extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfzSPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfzSPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfzSPk, k, fz, FOV_1, dx, ilast, jlast);
						}
					}
					if ((vector(FOV_xLower, i, j, k) > 0) && ((i + 1 < ilast && (vector(FOV_1,i + 1, j, k) > 0)) || (i + 2 < ilast && (vector(FOV_1,i + 2, j, k) > 0)) || ((i + 1 < ilast && k + 1 < klast && (vector(FOV_1, i + 1, j, k + 1) > 0)) || (i + 1 < ilast && k - 1 >= 0 && (vector(FOV_1, i + 1, j, k - 1) > 0)) || (i + 1 < ilast && k + 2 < klast && (vector(FOV_1, i + 1, j, k + 2) > 0)) || (i + 1 < ilast && k - 2 >= 0 && (vector(FOV_1, i + 1, j, k - 2) > 0)) || (i + 1 < ilast && j + 1 < jlast && (vector(FOV_1, i + 1, j + 1, k) > 0)) || (i + 1 < ilast && j + 1 < jlast && k + 1 < klast && (vector(FOV_1, i + 1, j + 1, k + 1) > 0)) || (i + 1 < ilast && j + 1 < jlast && k - 1 >= 0 && (vector(FOV_1, i + 1, j + 1, k - 1) > 0)) || (i + 1 < ilast && j + 1 < jlast && k + 2 < klast && (vector(FOV_1, i + 1, j + 1, k + 2) > 0)) || (i + 1 < ilast && j + 1 < jlast && k - 2 >= 0 && (vector(FOV_1, i + 1, j + 1, k - 2) > 0)) || (i + 1 < ilast && j - 1 >= 0 && (vector(FOV_1, i + 1, j - 1, k) > 0)) || (i + 1 < ilast && j - 1 >= 0 && k + 1 < klast && (vector(FOV_1, i + 1, j - 1, k + 1) > 0)) || (i + 1 < ilast && j - 1 >= 0 && k - 1 >= 0 && (vector(FOV_1, i + 1, j - 1, k - 1) > 0)) || (i + 1 < ilast && j - 1 >= 0 && k + 2 < klast && (vector(FOV_1, i + 1, j - 1, k + 2) > 0)) || (i + 1 < ilast && j - 1 >= 0 && k - 2 >= 0 && (vector(FOV_1, i + 1, j - 1, k - 2) > 0)) || (i + 1 < ilast && j + 2 < jlast && (vector(FOV_1, i + 1, j + 2, k) > 0)) || (i + 1 < ilast && j + 2 < jlast && k + 1 < klast && (vector(FOV_1, i + 1, j + 2, k + 1) > 0)) || (i + 1 < ilast && j + 2 < jlast && k - 1 >= 0 && (vector(FOV_1, i + 1, j + 2, k - 1) > 0)) || (i + 1 < ilast && j + 2 < jlast && k + 2 < klast && (vector(FOV_1, i + 1, j + 2, k + 2) > 0)) || (i + 1 < ilast && j + 2 < jlast && k - 2 >= 0 && (vector(FOV_1, i + 1, j + 2, k - 2) > 0)) || (i + 1 < ilast && j - 2 >= 0 && (vector(FOV_1, i + 1, j - 2, k) > 0)) || (i + 1 < ilast && j - 2 >= 0 && k + 1 < klast && (vector(FOV_1, i + 1, j - 2, k + 1) > 0)) || (i + 1 < ilast && j - 2 >= 0 && k - 1 >= 0 && (vector(FOV_1, i + 1, j - 2, k - 1) > 0)) || (i + 1 < ilast && j - 2 >= 0 && k + 2 < klast && (vector(FOV_1, i + 1, j - 2, k + 2) > 0)) || (i + 1 < ilast && j - 2 >= 0 && k - 2 >= 0 && (vector(FOV_1, i + 1, j - 2, k - 2) > 0)) || (i + 2 < ilast && k + 1 < klast && (vector(FOV_1, i + 2, j, k + 1) > 0)) || (i + 2 < ilast && k - 1 >= 0 && (vector(FOV_1, i + 2, j, k - 1) > 0)) || (i + 2 < ilast && k + 2 < klast && (vector(FOV_1, i + 2, j, k + 2) > 0)) || (i + 2 < ilast && k - 2 >= 0 && (vector(FOV_1, i + 2, j, k - 2) > 0)) || (i + 2 < ilast && j + 1 < jlast && (vector(FOV_1, i + 2, j + 1, k) > 0)) || (i + 2 < ilast && j + 1 < jlast && k + 1 < klast && (vector(FOV_1, i + 2, j + 1, k + 1) > 0)) || (i + 2 < ilast && j + 1 < jlast && k - 1 >= 0 && (vector(FOV_1, i + 2, j + 1, k - 1) > 0)) || (i + 2 < ilast && j + 1 < jlast && k + 2 < klast && (vector(FOV_1, i + 2, j + 1, k + 2) > 0)) || (i + 2 < ilast && j + 1 < jlast && k - 2 >= 0 && (vector(FOV_1, i + 2, j + 1, k - 2) > 0)) || (i + 2 < ilast && j - 1 >= 0 && (vector(FOV_1, i + 2, j - 1, k) > 0)) || (i + 2 < ilast && j - 1 >= 0 && k + 1 < klast && (vector(FOV_1, i + 2, j - 1, k + 1) > 0)) || (i + 2 < ilast && j - 1 >= 0 && k - 1 >= 0 && (vector(FOV_1, i + 2, j - 1, k - 1) > 0)) || (i + 2 < ilast && j - 1 >= 0 && k + 2 < klast && (vector(FOV_1, i + 2, j - 1, k + 2) > 0)) || (i + 2 < ilast && j - 1 >= 0 && k - 2 >= 0 && (vector(FOV_1, i + 2, j - 1, k - 2) > 0)) || (i + 2 < ilast && j + 2 < jlast && (vector(FOV_1, i + 2, j + 2, k) > 0)) || (i + 2 < ilast && j + 2 < jlast && k + 1 < klast && (vector(FOV_1, i + 2, j + 2, k + 1) > 0)) || (i + 2 < ilast && j + 2 < jlast && k - 1 >= 0 && (vector(FOV_1, i + 2, j + 2, k - 1) > 0)) || (i + 2 < ilast && j + 2 < jlast && k + 2 < klast && (vector(FOV_1, i + 2, j + 2, k + 2) > 0)) || (i + 2 < ilast && j + 2 < jlast && k - 2 >= 0 && (vector(FOV_1, i + 2, j + 2, k - 2) > 0)) || (i + 2 < ilast && j - 2 >= 0 && (vector(FOV_1, i + 2, j - 2, k) > 0)) || (i + 2 < ilast && j - 2 >= 0 && k + 1 < klast && (vector(FOV_1, i + 2, j - 2, k + 1) > 0)) || (i + 2 < ilast && j - 2 >= 0 && k - 1 >= 0 && (vector(FOV_1, i + 2, j - 2, k - 1) > 0)) || (i + 2 < ilast && j - 2 >= 0 && k + 2 < klast && (vector(FOV_1, i + 2, j - 2, k + 2) > 0)) || (i + 2 < ilast && j - 2 >= 0 && k - 2 >= 0 && (vector(FOV_1, i + 2, j - 2, k - 2) > 0))))) {
						//Region field extrapolations
						if ((vector(d_i_rho_mx_my_mz_p_fx_fy_fz, i, j, k) != 0 || vector(d_j_rho_mx_my_mz_p_fx_fy_fz, i, j, k) != 0 || vector(d_k_rho_mx_my_mz_p_fx_fy_fz, i, j, k) != 0)) {
							extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBrhoSPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBrhoSPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBrhoSPk, k, rk1rho, FOV_1, dx, ilast, jlast);
							extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmxSPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmxSPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmxSPk, k, rk1mx, FOV_1, dx, ilast, jlast);
							extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmySPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmySPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmySPk, k, rk1my, FOV_1, dx, ilast, jlast);
							extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmzSPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmzSPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmzSPk, k, rk1mz, FOV_1, dx, ilast, jlast);
							extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBpSPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBpSPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBpSPk, k, p, FOV_1, dx, ilast, jlast);
							extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfxSPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfxSPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfxSPk, k, fx, FOV_1, dx, ilast, jlast);
							extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfySPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfySPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfySPk, k, fy, FOV_1, dx, ilast, jlast);
							extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfzSPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfzSPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfzSPk, k, fz, FOV_1, dx, ilast, jlast);
						}
					}
					if ((vector(FOV_xUpper, i, j, k) > 0) && ((i - 1 >= 0 && (vector(FOV_1, i - 1, j, k) > 0)) || (i - 2 >= 0 && (vector(FOV_1, i - 2, j, k) > 0)) || ((i - 1 >= 0 && k + 1 < klast && (vector(FOV_1, i - 1, j, k + 1) > 0)) || (i - 1 >= 0 && k - 1 >= 0 && (vector(FOV_1, i - 1, j, k - 1) > 0)) || (i - 1 >= 0 && k + 2 < klast && (vector(FOV_1, i - 1, j, k + 2) > 0)) || (i - 1 >= 0 && k - 2 >= 0 && (vector(FOV_1, i - 1, j, k - 2) > 0)) || (i - 1 >= 0 && j + 1 < jlast && (vector(FOV_1, i - 1, j + 1, k) > 0)) || (i - 1 >= 0 && j + 1 < jlast && k + 1 < klast && (vector(FOV_1, i - 1, j + 1, k + 1) > 0)) || (i - 1 >= 0 && j + 1 < jlast && k - 1 >= 0 && (vector(FOV_1, i - 1, j + 1, k - 1) > 0)) || (i - 1 >= 0 && j + 1 < jlast && k + 2 < klast && (vector(FOV_1, i - 1, j + 1, k + 2) > 0)) || (i - 1 >= 0 && j + 1 < jlast && k - 2 >= 0 && (vector(FOV_1, i - 1, j + 1, k - 2) > 0)) || (i - 1 >= 0 && j - 1 >= 0 && (vector(FOV_1, i - 1, j - 1, k) > 0)) || (i - 1 >= 0 && j - 1 >= 0 && k + 1 < klast && (vector(FOV_1, i - 1, j - 1, k + 1) > 0)) || (i - 1 >= 0 && j - 1 >= 0 && k - 1 >= 0 && (vector(FOV_1, i - 1, j - 1, k - 1) > 0)) || (i - 1 >= 0 && j - 1 >= 0 && k + 2 < klast && (vector(FOV_1, i - 1, j - 1, k + 2) > 0)) || (i - 1 >= 0 && j - 1 >= 0 && k - 2 >= 0 && (vector(FOV_1, i - 1, j - 1, k - 2) > 0)) || (i - 1 >= 0 && j + 2 < jlast && (vector(FOV_1, i - 1, j + 2, k) > 0)) || (i - 1 >= 0 && j + 2 < jlast && k + 1 < klast && (vector(FOV_1, i - 1, j + 2, k + 1) > 0)) || (i - 1 >= 0 && j + 2 < jlast && k - 1 >= 0 && (vector(FOV_1, i - 1, j + 2, k - 1) > 0)) || (i - 1 >= 0 && j + 2 < jlast && k + 2 < klast && (vector(FOV_1, i - 1, j + 2, k + 2) > 0)) || (i - 1 >= 0 && j + 2 < jlast && k - 2 >= 0 && (vector(FOV_1, i - 1, j + 2, k - 2) > 0)) || (i - 1 >= 0 && j - 2 >= 0 && (vector(FOV_1, i - 1, j - 2, k) > 0)) || (i - 1 >= 0 && j - 2 >= 0 && k + 1 < klast && (vector(FOV_1, i - 1, j - 2, k + 1) > 0)) || (i - 1 >= 0 && j - 2 >= 0 && k - 1 >= 0 && (vector(FOV_1, i - 1, j - 2, k - 1) > 0)) || (i - 1 >= 0 && j - 2 >= 0 && k + 2 < klast && (vector(FOV_1, i - 1, j - 2, k + 2) > 0)) || (i - 1 >= 0 && j - 2 >= 0 && k - 2 >= 0 && (vector(FOV_1, i - 1, j - 2, k - 2) > 0)) || (i - 2 >= 0 && k + 1 < klast && (vector(FOV_1, i - 2, j, k + 1) > 0)) || (i - 2 >= 0 && k - 1 >= 0 && (vector(FOV_1, i - 2, j, k - 1) > 0)) || (i - 2 >= 0 && k + 2 < klast && (vector(FOV_1, i - 2, j, k + 2) > 0)) || (i - 2 >= 0 && k - 2 >= 0 && (vector(FOV_1, i - 2, j, k - 2) > 0)) || (i - 2 >= 0 && j + 1 < jlast && (vector(FOV_1, i - 2, j + 1, k) > 0)) || (i - 2 >= 0 && j + 1 < jlast && k + 1 < klast && (vector(FOV_1, i - 2, j + 1, k + 1) > 0)) || (i - 2 >= 0 && j + 1 < jlast && k - 1 >= 0 && (vector(FOV_1, i - 2, j + 1, k - 1) > 0)) || (i - 2 >= 0 && j + 1 < jlast && k + 2 < klast && (vector(FOV_1, i - 2, j + 1, k + 2) > 0)) || (i - 2 >= 0 && j + 1 < jlast && k - 2 >= 0 && (vector(FOV_1, i - 2, j + 1, k - 2) > 0)) || (i - 2 >= 0 && j - 1 >= 0 && (vector(FOV_1, i - 2, j - 1, k) > 0)) || (i - 2 >= 0 && j - 1 >= 0 && k + 1 < klast && (vector(FOV_1, i - 2, j - 1, k + 1) > 0)) || (i - 2 >= 0 && j - 1 >= 0 && k - 1 >= 0 && (vector(FOV_1, i - 2, j - 1, k - 1) > 0)) || (i - 2 >= 0 && j - 1 >= 0 && k + 2 < klast && (vector(FOV_1, i - 2, j - 1, k + 2) > 0)) || (i - 2 >= 0 && j - 1 >= 0 && k - 2 >= 0 && (vector(FOV_1, i - 2, j - 1, k - 2) > 0)) || (i - 2 >= 0 && j + 2 < jlast && (vector(FOV_1, i - 2, j + 2, k) > 0)) || (i - 2 >= 0 && j + 2 < jlast && k + 1 < klast && (vector(FOV_1, i - 2, j + 2, k + 1) > 0)) || (i - 2 >= 0 && j + 2 < jlast && k - 1 >= 0 && (vector(FOV_1, i - 2, j + 2, k - 1) > 0)) || (i - 2 >= 0 && j + 2 < jlast && k + 2 < klast && (vector(FOV_1, i - 2, j + 2, k + 2) > 0)) || (i - 2 >= 0 && j + 2 < jlast && k - 2 >= 0 && (vector(FOV_1, i - 2, j + 2, k - 2) > 0)) || (i - 2 >= 0 && j - 2 >= 0 && (vector(FOV_1, i - 2, j - 2, k) > 0)) || (i - 2 >= 0 && j - 2 >= 0 && k + 1 < klast && (vector(FOV_1, i - 2, j - 2, k + 1) > 0)) || (i - 2 >= 0 && j - 2 >= 0 && k - 1 >= 0 && (vector(FOV_1, i - 2, j - 2, k - 1) > 0)) || (i - 2 >= 0 && j - 2 >= 0 && k + 2 < klast && (vector(FOV_1, i - 2, j - 2, k + 2) > 0)) || (i - 2 >= 0 && j - 2 >= 0 && k - 2 >= 0 && (vector(FOV_1, i - 2, j - 2, k - 2) > 0))))) {
						//Region field extrapolations
						if ((vector(d_i_rho_mx_my_mz_p_fx_fy_fz, i, j, k) != 0 || vector(d_j_rho_mx_my_mz_p_fx_fy_fz, i, j, k) != 0 || vector(d_k_rho_mx_my_mz_p_fx_fy_fz, i, j, k) != 0)) {
							extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBrhoSPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBrhoSPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBrhoSPk, k, rk1rho, FOV_1, dx, ilast, jlast);
							extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmxSPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmxSPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmxSPk, k, rk1mx, FOV_1, dx, ilast, jlast);
							extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmySPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmySPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmySPk, k, rk1my, FOV_1, dx, ilast, jlast);
							extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmzSPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmzSPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmzSPk, k, rk1mz, FOV_1, dx, ilast, jlast);
							extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBpSPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBpSPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBpSPk, k, p, FOV_1, dx, ilast, jlast);
							extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfxSPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfxSPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfxSPk, k, fx, FOV_1, dx, ilast, jlast);
							extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfySPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfySPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfySPk, k, fy, FOV_1, dx, ilast, jlast);
							extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfzSPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfzSPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfzSPk, k, fz, FOV_1, dx, ilast, jlast);
						}
					}
					if ((vector(FOV_yLower, i, j, k) > 0) && ((j + 1 < jlast && (vector(FOV_1,i, j + 1, k) > 0)) || (j + 2 < jlast && (vector(FOV_1,i, j + 2, k) > 0)) || ((j + 1 < jlast && k + 1 < klast && (vector(FOV_1, i, j + 1, k + 1) > 0)) || (j + 1 < jlast && k - 1 >= 0 && (vector(FOV_1, i, j + 1, k - 1) > 0)) || (j + 1 < jlast && k + 2 < klast && (vector(FOV_1, i, j + 1, k + 2) > 0)) || (j + 1 < jlast && k - 2 >= 0 && (vector(FOV_1, i, j + 1, k - 2) > 0)) || (j + 2 < jlast && k + 1 < klast && (vector(FOV_1, i, j + 2, k + 1) > 0)) || (j + 2 < jlast && k - 1 >= 0 && (vector(FOV_1, i, j + 2, k - 1) > 0)) || (j + 2 < jlast && k + 2 < klast && (vector(FOV_1, i, j + 2, k + 2) > 0)) || (j + 2 < jlast && k - 2 >= 0 && (vector(FOV_1, i, j + 2, k - 2) > 0)) || (i + 1 < ilast && j + 1 < jlast && (vector(FOV_1, i + 1, j + 1, k) > 0)) || (i + 1 < ilast && j + 1 < jlast && k + 1 < klast && (vector(FOV_1, i + 1, j + 1, k + 1) > 0)) || (i + 1 < ilast && j + 1 < jlast && k - 1 >= 0 && (vector(FOV_1, i + 1, j + 1, k - 1) > 0)) || (i + 1 < ilast && j + 1 < jlast && k + 2 < klast && (vector(FOV_1, i + 1, j + 1, k + 2) > 0)) || (i + 1 < ilast && j + 1 < jlast && k - 2 >= 0 && (vector(FOV_1, i + 1, j + 1, k - 2) > 0)) || (i + 1 < ilast && j + 2 < jlast && (vector(FOV_1, i + 1, j + 2, k) > 0)) || (i + 1 < ilast && j + 2 < jlast && k + 1 < klast && (vector(FOV_1, i + 1, j + 2, k + 1) > 0)) || (i + 1 < ilast && j + 2 < jlast && k - 1 >= 0 && (vector(FOV_1, i + 1, j + 2, k - 1) > 0)) || (i + 1 < ilast && j + 2 < jlast && k + 2 < klast && (vector(FOV_1, i + 1, j + 2, k + 2) > 0)) || (i + 1 < ilast && j + 2 < jlast && k - 2 >= 0 && (vector(FOV_1, i + 1, j + 2, k - 2) > 0)) || (i - 1 >= 0 && j + 1 < jlast && (vector(FOV_1, i - 1, j + 1, k) > 0)) || (i - 1 >= 0 && j + 1 < jlast && k + 1 < klast && (vector(FOV_1, i - 1, j + 1, k + 1) > 0)) || (i - 1 >= 0 && j + 1 < jlast && k - 1 >= 0 && (vector(FOV_1, i - 1, j + 1, k - 1) > 0)) || (i - 1 >= 0 && j + 1 < jlast && k + 2 < klast && (vector(FOV_1, i - 1, j + 1, k + 2) > 0)) || (i - 1 >= 0 && j + 1 < jlast && k - 2 >= 0 && (vector(FOV_1, i - 1, j + 1, k - 2) > 0)) || (i - 1 >= 0 && j + 2 < jlast && (vector(FOV_1, i - 1, j + 2, k) > 0)) || (i - 1 >= 0 && j + 2 < jlast && k + 1 < klast && (vector(FOV_1, i - 1, j + 2, k + 1) > 0)) || (i - 1 >= 0 && j + 2 < jlast && k - 1 >= 0 && (vector(FOV_1, i - 1, j + 2, k - 1) > 0)) || (i - 1 >= 0 && j + 2 < jlast && k + 2 < klast && (vector(FOV_1, i - 1, j + 2, k + 2) > 0)) || (i - 1 >= 0 && j + 2 < jlast && k - 2 >= 0 && (vector(FOV_1, i - 1, j + 2, k - 2) > 0)) || (i + 2 < ilast && j + 1 < jlast && (vector(FOV_1, i + 2, j + 1, k) > 0)) || (i + 2 < ilast && j + 1 < jlast && k + 1 < klast && (vector(FOV_1, i + 2, j + 1, k + 1) > 0)) || (i + 2 < ilast && j + 1 < jlast && k - 1 >= 0 && (vector(FOV_1, i + 2, j + 1, k - 1) > 0)) || (i + 2 < ilast && j + 1 < jlast && k + 2 < klast && (vector(FOV_1, i + 2, j + 1, k + 2) > 0)) || (i + 2 < ilast && j + 1 < jlast && k - 2 >= 0 && (vector(FOV_1, i + 2, j + 1, k - 2) > 0)) || (i + 2 < ilast && j + 2 < jlast && (vector(FOV_1, i + 2, j + 2, k) > 0)) || (i + 2 < ilast && j + 2 < jlast && k + 1 < klast && (vector(FOV_1, i + 2, j + 2, k + 1) > 0)) || (i + 2 < ilast && j + 2 < jlast && k - 1 >= 0 && (vector(FOV_1, i + 2, j + 2, k - 1) > 0)) || (i + 2 < ilast && j + 2 < jlast && k + 2 < klast && (vector(FOV_1, i + 2, j + 2, k + 2) > 0)) || (i + 2 < ilast && j + 2 < jlast && k - 2 >= 0 && (vector(FOV_1, i + 2, j + 2, k - 2) > 0)) || (i - 2 >= 0 && j + 1 < jlast && (vector(FOV_1, i - 2, j + 1, k) > 0)) || (i - 2 >= 0 && j + 1 < jlast && k + 1 < klast && (vector(FOV_1, i - 2, j + 1, k + 1) > 0)) || (i - 2 >= 0 && j + 1 < jlast && k - 1 >= 0 && (vector(FOV_1, i - 2, j + 1, k - 1) > 0)) || (i - 2 >= 0 && j + 1 < jlast && k + 2 < klast && (vector(FOV_1, i - 2, j + 1, k + 2) > 0)) || (i - 2 >= 0 && j + 1 < jlast && k - 2 >= 0 && (vector(FOV_1, i - 2, j + 1, k - 2) > 0)) || (i - 2 >= 0 && j + 2 < jlast && (vector(FOV_1, i - 2, j + 2, k) > 0)) || (i - 2 >= 0 && j + 2 < jlast && k + 1 < klast && (vector(FOV_1, i - 2, j + 2, k + 1) > 0)) || (i - 2 >= 0 && j + 2 < jlast && k - 1 >= 0 && (vector(FOV_1, i - 2, j + 2, k - 1) > 0)) || (i - 2 >= 0 && j + 2 < jlast && k + 2 < klast && (vector(FOV_1, i - 2, j + 2, k + 2) > 0)) || (i - 2 >= 0 && j + 2 < jlast && k - 2 >= 0 && (vector(FOV_1, i - 2, j + 2, k - 2) > 0))))) {
						vector(rk1rho, i, j, k) = 0.0;
						vector(rk1mx, i, j, k) = 0.0;
						vector(rk1my, i, j, k) = 0.0;
						vector(rk1mz, i, j, k) = 0.0;
						vector(p, i, j, k) = 0.0;
						vector(fx, i, j, k) = 0.0;
						vector(fy, i, j, k) = 0.0;
						vector(fz, i, j, k) = 0.0;
					}
					if ((vector(FOV_yUpper, i, j, k) > 0) && ((j - 1 >= 0 && (vector(FOV_1, i, j - 1, k) > 0)) || (j - 2 >= 0 && (vector(FOV_1, i, j - 2, k) > 0)) || ((j - 1 >= 0 && k + 1 < klast && (vector(FOV_1, i, j - 1, k + 1) > 0)) || (j - 1 >= 0 && k - 1 >= 0 && (vector(FOV_1, i, j - 1, k - 1) > 0)) || (j - 1 >= 0 && k + 2 < klast && (vector(FOV_1, i, j - 1, k + 2) > 0)) || (j - 1 >= 0 && k - 2 >= 0 && (vector(FOV_1, i, j - 1, k - 2) > 0)) || (j - 2 >= 0 && k + 1 < klast && (vector(FOV_1, i, j - 2, k + 1) > 0)) || (j - 2 >= 0 && k - 1 >= 0 && (vector(FOV_1, i, j - 2, k - 1) > 0)) || (j - 2 >= 0 && k + 2 < klast && (vector(FOV_1, i, j - 2, k + 2) > 0)) || (j - 2 >= 0 && k - 2 >= 0 && (vector(FOV_1, i, j - 2, k - 2) > 0)) || (i + 1 < ilast && j - 1 >= 0 && (vector(FOV_1, i + 1, j - 1, k) > 0)) || (i + 1 < ilast && j - 1 >= 0 && k + 1 < klast && (vector(FOV_1, i + 1, j - 1, k + 1) > 0)) || (i + 1 < ilast && j - 1 >= 0 && k - 1 >= 0 && (vector(FOV_1, i + 1, j - 1, k - 1) > 0)) || (i + 1 < ilast && j - 1 >= 0 && k + 2 < klast && (vector(FOV_1, i + 1, j - 1, k + 2) > 0)) || (i + 1 < ilast && j - 1 >= 0 && k - 2 >= 0 && (vector(FOV_1, i + 1, j - 1, k - 2) > 0)) || (i + 1 < ilast && j - 2 >= 0 && (vector(FOV_1, i + 1, j - 2, k) > 0)) || (i + 1 < ilast && j - 2 >= 0 && k + 1 < klast && (vector(FOV_1, i + 1, j - 2, k + 1) > 0)) || (i + 1 < ilast && j - 2 >= 0 && k - 1 >= 0 && (vector(FOV_1, i + 1, j - 2, k - 1) > 0)) || (i + 1 < ilast && j - 2 >= 0 && k + 2 < klast && (vector(FOV_1, i + 1, j - 2, k + 2) > 0)) || (i + 1 < ilast && j - 2 >= 0 && k - 2 >= 0 && (vector(FOV_1, i + 1, j - 2, k - 2) > 0)) || (i - 1 >= 0 && j - 1 >= 0 && (vector(FOV_1, i - 1, j - 1, k) > 0)) || (i - 1 >= 0 && j - 1 >= 0 && k + 1 < klast && (vector(FOV_1, i - 1, j - 1, k + 1) > 0)) || (i - 1 >= 0 && j - 1 >= 0 && k - 1 >= 0 && (vector(FOV_1, i - 1, j - 1, k - 1) > 0)) || (i - 1 >= 0 && j - 1 >= 0 && k + 2 < klast && (vector(FOV_1, i - 1, j - 1, k + 2) > 0)) || (i - 1 >= 0 && j - 1 >= 0 && k - 2 >= 0 && (vector(FOV_1, i - 1, j - 1, k - 2) > 0)) || (i - 1 >= 0 && j - 2 >= 0 && (vector(FOV_1, i - 1, j - 2, k) > 0)) || (i - 1 >= 0 && j - 2 >= 0 && k + 1 < klast && (vector(FOV_1, i - 1, j - 2, k + 1) > 0)) || (i - 1 >= 0 && j - 2 >= 0 && k - 1 >= 0 && (vector(FOV_1, i - 1, j - 2, k - 1) > 0)) || (i - 1 >= 0 && j - 2 >= 0 && k + 2 < klast && (vector(FOV_1, i - 1, j - 2, k + 2) > 0)) || (i - 1 >= 0 && j - 2 >= 0 && k - 2 >= 0 && (vector(FOV_1, i - 1, j - 2, k - 2) > 0)) || (i + 2 < ilast && j - 1 >= 0 && (vector(FOV_1, i + 2, j - 1, k) > 0)) || (i + 2 < ilast && j - 1 >= 0 && k + 1 < klast && (vector(FOV_1, i + 2, j - 1, k + 1) > 0)) || (i + 2 < ilast && j - 1 >= 0 && k - 1 >= 0 && (vector(FOV_1, i + 2, j - 1, k - 1) > 0)) || (i + 2 < ilast && j - 1 >= 0 && k + 2 < klast && (vector(FOV_1, i + 2, j - 1, k + 2) > 0)) || (i + 2 < ilast && j - 1 >= 0 && k - 2 >= 0 && (vector(FOV_1, i + 2, j - 1, k - 2) > 0)) || (i + 2 < ilast && j - 2 >= 0 && (vector(FOV_1, i + 2, j - 2, k) > 0)) || (i + 2 < ilast && j - 2 >= 0 && k + 1 < klast && (vector(FOV_1, i + 2, j - 2, k + 1) > 0)) || (i + 2 < ilast && j - 2 >= 0 && k - 1 >= 0 && (vector(FOV_1, i + 2, j - 2, k - 1) > 0)) || (i + 2 < ilast && j - 2 >= 0 && k + 2 < klast && (vector(FOV_1, i + 2, j - 2, k + 2) > 0)) || (i + 2 < ilast && j - 2 >= 0 && k - 2 >= 0 && (vector(FOV_1, i + 2, j - 2, k - 2) > 0)) || (i - 2 >= 0 && j - 1 >= 0 && (vector(FOV_1, i - 2, j - 1, k) > 0)) || (i - 2 >= 0 && j - 1 >= 0 && k + 1 < klast && (vector(FOV_1, i - 2, j - 1, k + 1) > 0)) || (i - 2 >= 0 && j - 1 >= 0 && k - 1 >= 0 && (vector(FOV_1, i - 2, j - 1, k - 1) > 0)) || (i - 2 >= 0 && j - 1 >= 0 && k + 2 < klast && (vector(FOV_1, i - 2, j - 1, k + 2) > 0)) || (i - 2 >= 0 && j - 1 >= 0 && k - 2 >= 0 && (vector(FOV_1, i - 2, j - 1, k - 2) > 0)) || (i - 2 >= 0 && j - 2 >= 0 && (vector(FOV_1, i - 2, j - 2, k) > 0)) || (i - 2 >= 0 && j - 2 >= 0 && k + 1 < klast && (vector(FOV_1, i - 2, j - 2, k + 1) > 0)) || (i - 2 >= 0 && j - 2 >= 0 && k - 1 >= 0 && (vector(FOV_1, i - 2, j - 2, k - 1) > 0)) || (i - 2 >= 0 && j - 2 >= 0 && k + 2 < klast && (vector(FOV_1, i - 2, j - 2, k + 2) > 0)) || (i - 2 >= 0 && j - 2 >= 0 && k - 2 >= 0 && (vector(FOV_1, i - 2, j - 2, k - 2) > 0))))) {
						vector(rk1rho, i, j, k) = 0.0;
						vector(rk1mx, i, j, k) = 0.0;
						vector(rk1my, i, j, k) = 0.0;
						vector(rk1mz, i, j, k) = 0.0;
						vector(p, i, j, k) = 0.0;
						vector(fx, i, j, k) = 0.0;
						vector(fy, i, j, k) = 0.0;
						vector(fz, i, j, k) = 0.0;
					}
					if ((vector(FOV_zLower, i, j, k) > 0) && ((k + 1 < klast && (vector(FOV_1,i, j, k + 1) > 0)) || (k + 2 < klast && (vector(FOV_1,i, j, k + 2) > 0)) || ((j + 1 < jlast && k + 1 < klast && (vector(FOV_1, i, j + 1, k + 1) > 0)) || (j + 1 < jlast && k + 2 < klast && (vector(FOV_1, i, j + 1, k + 2) > 0)) || (j - 1 >= 0 && k + 1 < klast && (vector(FOV_1, i, j - 1, k + 1) > 0)) || (j - 1 >= 0 && k + 2 < klast && (vector(FOV_1, i, j - 1, k + 2) > 0)) || (j + 2 < jlast && k + 1 < klast && (vector(FOV_1, i, j + 2, k + 1) > 0)) || (j + 2 < jlast && k + 2 < klast && (vector(FOV_1, i, j + 2, k + 2) > 0)) || (j - 2 >= 0 && k + 1 < klast && (vector(FOV_1, i, j - 2, k + 1) > 0)) || (j - 2 >= 0 && k + 2 < klast && (vector(FOV_1, i, j - 2, k + 2) > 0)) || (i + 1 < ilast && k + 1 < klast && (vector(FOV_1, i + 1, j, k + 1) > 0)) || (i + 1 < ilast && k + 2 < klast && (vector(FOV_1, i + 1, j, k + 2) > 0)) || (i + 1 < ilast && j + 1 < jlast && k + 1 < klast && (vector(FOV_1, i + 1, j + 1, k + 1) > 0)) || (i + 1 < ilast && j + 1 < jlast && k + 2 < klast && (vector(FOV_1, i + 1, j + 1, k + 2) > 0)) || (i + 1 < ilast && j - 1 >= 0 && k + 1 < klast && (vector(FOV_1, i + 1, j - 1, k + 1) > 0)) || (i + 1 < ilast && j - 1 >= 0 && k + 2 < klast && (vector(FOV_1, i + 1, j - 1, k + 2) > 0)) || (i + 1 < ilast && j + 2 < jlast && k + 1 < klast && (vector(FOV_1, i + 1, j + 2, k + 1) > 0)) || (i + 1 < ilast && j + 2 < jlast && k + 2 < klast && (vector(FOV_1, i + 1, j + 2, k + 2) > 0)) || (i + 1 < ilast && j - 2 >= 0 && k + 1 < klast && (vector(FOV_1, i + 1, j - 2, k + 1) > 0)) || (i + 1 < ilast && j - 2 >= 0 && k + 2 < klast && (vector(FOV_1, i + 1, j - 2, k + 2) > 0)) || (i - 1 >= 0 && k + 1 < klast && (vector(FOV_1, i - 1, j, k + 1) > 0)) || (i - 1 >= 0 && k + 2 < klast && (vector(FOV_1, i - 1, j, k + 2) > 0)) || (i - 1 >= 0 && j + 1 < jlast && k + 1 < klast && (vector(FOV_1, i - 1, j + 1, k + 1) > 0)) || (i - 1 >= 0 && j + 1 < jlast && k + 2 < klast && (vector(FOV_1, i - 1, j + 1, k + 2) > 0)) || (i - 1 >= 0 && j - 1 >= 0 && k + 1 < klast && (vector(FOV_1, i - 1, j - 1, k + 1) > 0)) || (i - 1 >= 0 && j - 1 >= 0 && k + 2 < klast && (vector(FOV_1, i - 1, j - 1, k + 2) > 0)) || (i - 1 >= 0 && j + 2 < jlast && k + 1 < klast && (vector(FOV_1, i - 1, j + 2, k + 1) > 0)) || (i - 1 >= 0 && j + 2 < jlast && k + 2 < klast && (vector(FOV_1, i - 1, j + 2, k + 2) > 0)) || (i - 1 >= 0 && j - 2 >= 0 && k + 1 < klast && (vector(FOV_1, i - 1, j - 2, k + 1) > 0)) || (i - 1 >= 0 && j - 2 >= 0 && k + 2 < klast && (vector(FOV_1, i - 1, j - 2, k + 2) > 0)) || (i + 2 < ilast && k + 1 < klast && (vector(FOV_1, i + 2, j, k + 1) > 0)) || (i + 2 < ilast && k + 2 < klast && (vector(FOV_1, i + 2, j, k + 2) > 0)) || (i + 2 < ilast && j + 1 < jlast && k + 1 < klast && (vector(FOV_1, i + 2, j + 1, k + 1) > 0)) || (i + 2 < ilast && j + 1 < jlast && k + 2 < klast && (vector(FOV_1, i + 2, j + 1, k + 2) > 0)) || (i + 2 < ilast && j - 1 >= 0 && k + 1 < klast && (vector(FOV_1, i + 2, j - 1, k + 1) > 0)) || (i + 2 < ilast && j - 1 >= 0 && k + 2 < klast && (vector(FOV_1, i + 2, j - 1, k + 2) > 0)) || (i + 2 < ilast && j + 2 < jlast && k + 1 < klast && (vector(FOV_1, i + 2, j + 2, k + 1) > 0)) || (i + 2 < ilast && j + 2 < jlast && k + 2 < klast && (vector(FOV_1, i + 2, j + 2, k + 2) > 0)) || (i + 2 < ilast && j - 2 >= 0 && k + 1 < klast && (vector(FOV_1, i + 2, j - 2, k + 1) > 0)) || (i + 2 < ilast && j - 2 >= 0 && k + 2 < klast && (vector(FOV_1, i + 2, j - 2, k + 2) > 0)) || (i - 2 >= 0 && k + 1 < klast && (vector(FOV_1, i - 2, j, k + 1) > 0)) || (i - 2 >= 0 && k + 2 < klast && (vector(FOV_1, i - 2, j, k + 2) > 0)) || (i - 2 >= 0 && j + 1 < jlast && k + 1 < klast && (vector(FOV_1, i - 2, j + 1, k + 1) > 0)) || (i - 2 >= 0 && j + 1 < jlast && k + 2 < klast && (vector(FOV_1, i - 2, j + 1, k + 2) > 0)) || (i - 2 >= 0 && j - 1 >= 0 && k + 1 < klast && (vector(FOV_1, i - 2, j - 1, k + 1) > 0)) || (i - 2 >= 0 && j - 1 >= 0 && k + 2 < klast && (vector(FOV_1, i - 2, j - 1, k + 2) > 0)) || (i - 2 >= 0 && j + 2 < jlast && k + 1 < klast && (vector(FOV_1, i - 2, j + 2, k + 1) > 0)) || (i - 2 >= 0 && j + 2 < jlast && k + 2 < klast && (vector(FOV_1, i - 2, j + 2, k + 2) > 0)) || (i - 2 >= 0 && j - 2 >= 0 && k + 1 < klast && (vector(FOV_1, i - 2, j - 2, k + 1) > 0)) || (i - 2 >= 0 && j - 2 >= 0 && k + 2 < klast && (vector(FOV_1, i - 2, j - 2, k + 2) > 0))))) {
						vector(rk1rho, i, j, k) = 0.0;
						vector(rk1mx, i, j, k) = 0.0;
						vector(rk1my, i, j, k) = 0.0;
						vector(rk1mz, i, j, k) = 0.0;
						vector(p, i, j, k) = 0.0;
						vector(fx, i, j, k) = 0.0;
						vector(fy, i, j, k) = 0.0;
						vector(fz, i, j, k) = 0.0;
					}
					if ((vector(FOV_zUpper, i, j, k) > 0) && ((k - 1 >= 0 && (vector(FOV_1, i, j, k - 1) > 0)) || (k - 2 >= 0 && (vector(FOV_1, i, j, k - 2) > 0)) || ((j + 1 < jlast && k - 1 >= 0 && (vector(FOV_1, i, j + 1, k - 1) > 0)) || (j + 1 < jlast && k - 2 >= 0 && (vector(FOV_1, i, j + 1, k - 2) > 0)) || (j - 1 >= 0 && k - 1 >= 0 && (vector(FOV_1, i, j - 1, k - 1) > 0)) || (j - 1 >= 0 && k - 2 >= 0 && (vector(FOV_1, i, j - 1, k - 2) > 0)) || (j + 2 < jlast && k - 1 >= 0 && (vector(FOV_1, i, j + 2, k - 1) > 0)) || (j + 2 < jlast && k - 2 >= 0 && (vector(FOV_1, i, j + 2, k - 2) > 0)) || (j - 2 >= 0 && k - 1 >= 0 && (vector(FOV_1, i, j - 2, k - 1) > 0)) || (j - 2 >= 0 && k - 2 >= 0 && (vector(FOV_1, i, j - 2, k - 2) > 0)) || (i + 1 < ilast && k - 1 >= 0 && (vector(FOV_1, i + 1, j, k - 1) > 0)) || (i + 1 < ilast && k - 2 >= 0 && (vector(FOV_1, i + 1, j, k - 2) > 0)) || (i + 1 < ilast && j + 1 < jlast && k - 1 >= 0 && (vector(FOV_1, i + 1, j + 1, k - 1) > 0)) || (i + 1 < ilast && j + 1 < jlast && k - 2 >= 0 && (vector(FOV_1, i + 1, j + 1, k - 2) > 0)) || (i + 1 < ilast && j - 1 >= 0 && k - 1 >= 0 && (vector(FOV_1, i + 1, j - 1, k - 1) > 0)) || (i + 1 < ilast && j - 1 >= 0 && k - 2 >= 0 && (vector(FOV_1, i + 1, j - 1, k - 2) > 0)) || (i + 1 < ilast && j + 2 < jlast && k - 1 >= 0 && (vector(FOV_1, i + 1, j + 2, k - 1) > 0)) || (i + 1 < ilast && j + 2 < jlast && k - 2 >= 0 && (vector(FOV_1, i + 1, j + 2, k - 2) > 0)) || (i + 1 < ilast && j - 2 >= 0 && k - 1 >= 0 && (vector(FOV_1, i + 1, j - 2, k - 1) > 0)) || (i + 1 < ilast && j - 2 >= 0 && k - 2 >= 0 && (vector(FOV_1, i + 1, j - 2, k - 2) > 0)) || (i - 1 >= 0 && k - 1 >= 0 && (vector(FOV_1, i - 1, j, k - 1) > 0)) || (i - 1 >= 0 && k - 2 >= 0 && (vector(FOV_1, i - 1, j, k - 2) > 0)) || (i - 1 >= 0 && j + 1 < jlast && k - 1 >= 0 && (vector(FOV_1, i - 1, j + 1, k - 1) > 0)) || (i - 1 >= 0 && j + 1 < jlast && k - 2 >= 0 && (vector(FOV_1, i - 1, j + 1, k - 2) > 0)) || (i - 1 >= 0 && j - 1 >= 0 && k - 1 >= 0 && (vector(FOV_1, i - 1, j - 1, k - 1) > 0)) || (i - 1 >= 0 && j - 1 >= 0 && k - 2 >= 0 && (vector(FOV_1, i - 1, j - 1, k - 2) > 0)) || (i - 1 >= 0 && j + 2 < jlast && k - 1 >= 0 && (vector(FOV_1, i - 1, j + 2, k - 1) > 0)) || (i - 1 >= 0 && j + 2 < jlast && k - 2 >= 0 && (vector(FOV_1, i - 1, j + 2, k - 2) > 0)) || (i - 1 >= 0 && j - 2 >= 0 && k - 1 >= 0 && (vector(FOV_1, i - 1, j - 2, k - 1) > 0)) || (i - 1 >= 0 && j - 2 >= 0 && k - 2 >= 0 && (vector(FOV_1, i - 1, j - 2, k - 2) > 0)) || (i + 2 < ilast && k - 1 >= 0 && (vector(FOV_1, i + 2, j, k - 1) > 0)) || (i + 2 < ilast && k - 2 >= 0 && (vector(FOV_1, i + 2, j, k - 2) > 0)) || (i + 2 < ilast && j + 1 < jlast && k - 1 >= 0 && (vector(FOV_1, i + 2, j + 1, k - 1) > 0)) || (i + 2 < ilast && j + 1 < jlast && k - 2 >= 0 && (vector(FOV_1, i + 2, j + 1, k - 2) > 0)) || (i + 2 < ilast && j - 1 >= 0 && k - 1 >= 0 && (vector(FOV_1, i + 2, j - 1, k - 1) > 0)) || (i + 2 < ilast && j - 1 >= 0 && k - 2 >= 0 && (vector(FOV_1, i + 2, j - 1, k - 2) > 0)) || (i + 2 < ilast && j + 2 < jlast && k - 1 >= 0 && (vector(FOV_1, i + 2, j + 2, k - 1) > 0)) || (i + 2 < ilast && j + 2 < jlast && k - 2 >= 0 && (vector(FOV_1, i + 2, j + 2, k - 2) > 0)) || (i + 2 < ilast && j - 2 >= 0 && k - 1 >= 0 && (vector(FOV_1, i + 2, j - 2, k - 1) > 0)) || (i + 2 < ilast && j - 2 >= 0 && k - 2 >= 0 && (vector(FOV_1, i + 2, j - 2, k - 2) > 0)) || (i - 2 >= 0 && k - 1 >= 0 && (vector(FOV_1, i - 2, j, k - 1) > 0)) || (i - 2 >= 0 && k - 2 >= 0 && (vector(FOV_1, i - 2, j, k - 2) > 0)) || (i - 2 >= 0 && j + 1 < jlast && k - 1 >= 0 && (vector(FOV_1, i - 2, j + 1, k - 1) > 0)) || (i - 2 >= 0 && j + 1 < jlast && k - 2 >= 0 && (vector(FOV_1, i - 2, j + 1, k - 2) > 0)) || (i - 2 >= 0 && j - 1 >= 0 && k - 1 >= 0 && (vector(FOV_1, i - 2, j - 1, k - 1) > 0)) || (i - 2 >= 0 && j - 1 >= 0 && k - 2 >= 0 && (vector(FOV_1, i - 2, j - 1, k - 2) > 0)) || (i - 2 >= 0 && j + 2 < jlast && k - 1 >= 0 && (vector(FOV_1, i - 2, j + 2, k - 1) > 0)) || (i - 2 >= 0 && j + 2 < jlast && k - 2 >= 0 && (vector(FOV_1, i - 2, j + 2, k - 2) > 0)) || (i - 2 >= 0 && j - 2 >= 0 && k - 1 >= 0 && (vector(FOV_1, i - 2, j - 2, k - 1) > 0)) || (i - 2 >= 0 && j - 2 >= 0 && k - 2 >= 0 && (vector(FOV_1, i - 2, j - 2, k - 2) > 0))))) {
						vector(rk1rho, i, j, k) = 0.0;
						vector(rk1mx, i, j, k) = 0.0;
						vector(rk1my, i, j, k) = 0.0;
						vector(rk1mz, i, j, k) = 0.0;
						vector(p, i, j, k) = 0.0;
						vector(fx, i, j, k) = 0.0;
						vector(fy, i, j, k) = 0.0;
						vector(fz, i, j, k) = 0.0;
					}
					if (vector(FOV_1, i, j, k) > 0) {
						//Region field extrapolations
						if ((vector(d_i_Null, i, j, k) != 0 || vector(d_j_Null, i, j, k) != 0 || vector(d_k_Null, i, j, k) != 0)) {
							extrapolate_field(d_i_Null, extrapolatedSBNullSPi, i, d_j_Null, extrapolatedSBNullSPj, j, d_k_Null, extrapolatedSBNullSPk, k, rk1Null, FOV_3, dx, ilast, jlast);
						}
					}
				}
			}
		}
		for(int k = 0; k < klast; k++) {
			for(int j = 0; j < jlast; j++) {
				for(int i = 0; i < ilast; i++) {
					if ((vector(FOV_xLower, i, j, k) > 0) && ((i + 1 < ilast && (vector(FOV_3,i + 1, j, k) > 0)) || (i + 2 < ilast && (vector(FOV_3,i + 2, j, k) > 0)))) {
						vector(extrapolatedSBrhoSPi, i, j, k) = vector(rk1rho, i, j, k);
						vector(extrapolatedSBrhoSPj, i, j, k) = vector(rk1rho, i, j, k);
						vector(extrapolatedSBrhoSPk, i, j, k) = vector(rk1rho, i, j, k);
						vector(extrapolatedSBmxSPi, i, j, k) = vector(rk1mx, i, j, k);
						vector(extrapolatedSBmxSPj, i, j, k) = vector(rk1mx, i, j, k);
						vector(extrapolatedSBmxSPk, i, j, k) = vector(rk1mx, i, j, k);
						vector(extrapolatedSBmySPi, i, j, k) = vector(rk1my, i, j, k);
						vector(extrapolatedSBmySPj, i, j, k) = vector(rk1my, i, j, k);
						vector(extrapolatedSBmySPk, i, j, k) = vector(rk1my, i, j, k);
						vector(extrapolatedSBmzSPi, i, j, k) = vector(rk1mz, i, j, k);
						vector(extrapolatedSBmzSPj, i, j, k) = vector(rk1mz, i, j, k);
						vector(extrapolatedSBmzSPk, i, j, k) = vector(rk1mz, i, j, k);
						vector(extrapolatedSBNullSPi, i, j, k) = vector(rk1Null, i, j, k);
						vector(extrapolatedSBNullSPj, i, j, k) = vector(rk1Null, i, j, k);
						vector(extrapolatedSBNullSPk, i, j, k) = vector(rk1Null, i, j, k);
						vector(extrapolatedSBpSPi, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBpSPj, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBpSPk, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBfxSPi, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfxSPj, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfxSPk, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfySPi, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfySPj, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfySPk, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfzSPi, i, j, k) = vector(fz, i, j, k);
						vector(extrapolatedSBfzSPj, i, j, k) = vector(fz, i, j, k);
						vector(extrapolatedSBfzSPk, i, j, k) = vector(fz, i, j, k);
					}
					if ((vector(FOV_xUpper, i, j, k) > 0) && ((i - 1 >= 0 && (vector(FOV_3, i - 1, j, k) > 0)) || (i - 2 >= 0 && (vector(FOV_3, i - 2, j, k) > 0)))) {
						vector(extrapolatedSBrhoSPi, i, j, k) = vector(rk1rho, i, j, k);
						vector(extrapolatedSBrhoSPj, i, j, k) = vector(rk1rho, i, j, k);
						vector(extrapolatedSBrhoSPk, i, j, k) = vector(rk1rho, i, j, k);
						vector(extrapolatedSBmxSPi, i, j, k) = vector(rk1mx, i, j, k);
						vector(extrapolatedSBmxSPj, i, j, k) = vector(rk1mx, i, j, k);
						vector(extrapolatedSBmxSPk, i, j, k) = vector(rk1mx, i, j, k);
						vector(extrapolatedSBmySPi, i, j, k) = vector(rk1my, i, j, k);
						vector(extrapolatedSBmySPj, i, j, k) = vector(rk1my, i, j, k);
						vector(extrapolatedSBmySPk, i, j, k) = vector(rk1my, i, j, k);
						vector(extrapolatedSBmzSPi, i, j, k) = vector(rk1mz, i, j, k);
						vector(extrapolatedSBmzSPj, i, j, k) = vector(rk1mz, i, j, k);
						vector(extrapolatedSBmzSPk, i, j, k) = vector(rk1mz, i, j, k);
						vector(extrapolatedSBNullSPi, i, j, k) = vector(rk1Null, i, j, k);
						vector(extrapolatedSBNullSPj, i, j, k) = vector(rk1Null, i, j, k);
						vector(extrapolatedSBNullSPk, i, j, k) = vector(rk1Null, i, j, k);
						vector(extrapolatedSBpSPi, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBpSPj, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBpSPk, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBfxSPi, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfxSPj, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfxSPk, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfySPi, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfySPj, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfySPk, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfzSPi, i, j, k) = vector(fz, i, j, k);
						vector(extrapolatedSBfzSPj, i, j, k) = vector(fz, i, j, k);
						vector(extrapolatedSBfzSPk, i, j, k) = vector(fz, i, j, k);
					}
					if ((vector(FOV_yLower, i, j, k) > 0) && ((j + 1 < jlast && (vector(FOV_3,i, j + 1, k) > 0)) || (j + 2 < jlast && (vector(FOV_3,i, j + 2, k) > 0)))) {
						vector(extrapolatedSBrhoSPi, i, j, k) = vector(rk1rho, i, j, k);
						vector(extrapolatedSBrhoSPj, i, j, k) = vector(rk1rho, i, j, k);
						vector(extrapolatedSBrhoSPk, i, j, k) = vector(rk1rho, i, j, k);
						vector(extrapolatedSBmxSPi, i, j, k) = vector(rk1mx, i, j, k);
						vector(extrapolatedSBmxSPj, i, j, k) = vector(rk1mx, i, j, k);
						vector(extrapolatedSBmxSPk, i, j, k) = vector(rk1mx, i, j, k);
						vector(extrapolatedSBmySPi, i, j, k) = vector(rk1my, i, j, k);
						vector(extrapolatedSBmySPj, i, j, k) = vector(rk1my, i, j, k);
						vector(extrapolatedSBmySPk, i, j, k) = vector(rk1my, i, j, k);
						vector(extrapolatedSBmzSPi, i, j, k) = vector(rk1mz, i, j, k);
						vector(extrapolatedSBmzSPj, i, j, k) = vector(rk1mz, i, j, k);
						vector(extrapolatedSBmzSPk, i, j, k) = vector(rk1mz, i, j, k);
						vector(extrapolatedSBNullSPi, i, j, k) = vector(rk1Null, i, j, k);
						vector(extrapolatedSBNullSPj, i, j, k) = vector(rk1Null, i, j, k);
						vector(extrapolatedSBNullSPk, i, j, k) = vector(rk1Null, i, j, k);
						vector(extrapolatedSBpSPi, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBpSPj, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBpSPk, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBfxSPi, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfxSPj, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfxSPk, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfySPi, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfySPj, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfySPk, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfzSPi, i, j, k) = vector(fz, i, j, k);
						vector(extrapolatedSBfzSPj, i, j, k) = vector(fz, i, j, k);
						vector(extrapolatedSBfzSPk, i, j, k) = vector(fz, i, j, k);
					}
					if ((vector(FOV_yUpper, i, j, k) > 0) && ((j - 1 >= 0 && (vector(FOV_3, i, j - 1, k) > 0)) || (j - 2 >= 0 && (vector(FOV_3, i, j - 2, k) > 0)))) {
						vector(extrapolatedSBrhoSPi, i, j, k) = vector(rk1rho, i, j, k);
						vector(extrapolatedSBrhoSPj, i, j, k) = vector(rk1rho, i, j, k);
						vector(extrapolatedSBrhoSPk, i, j, k) = vector(rk1rho, i, j, k);
						vector(extrapolatedSBmxSPi, i, j, k) = vector(rk1mx, i, j, k);
						vector(extrapolatedSBmxSPj, i, j, k) = vector(rk1mx, i, j, k);
						vector(extrapolatedSBmxSPk, i, j, k) = vector(rk1mx, i, j, k);
						vector(extrapolatedSBmySPi, i, j, k) = vector(rk1my, i, j, k);
						vector(extrapolatedSBmySPj, i, j, k) = vector(rk1my, i, j, k);
						vector(extrapolatedSBmySPk, i, j, k) = vector(rk1my, i, j, k);
						vector(extrapolatedSBmzSPi, i, j, k) = vector(rk1mz, i, j, k);
						vector(extrapolatedSBmzSPj, i, j, k) = vector(rk1mz, i, j, k);
						vector(extrapolatedSBmzSPk, i, j, k) = vector(rk1mz, i, j, k);
						vector(extrapolatedSBNullSPi, i, j, k) = vector(rk1Null, i, j, k);
						vector(extrapolatedSBNullSPj, i, j, k) = vector(rk1Null, i, j, k);
						vector(extrapolatedSBNullSPk, i, j, k) = vector(rk1Null, i, j, k);
						vector(extrapolatedSBpSPi, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBpSPj, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBpSPk, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBfxSPi, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfxSPj, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfxSPk, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfySPi, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfySPj, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfySPk, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfzSPi, i, j, k) = vector(fz, i, j, k);
						vector(extrapolatedSBfzSPj, i, j, k) = vector(fz, i, j, k);
						vector(extrapolatedSBfzSPk, i, j, k) = vector(fz, i, j, k);
					}
					if ((vector(FOV_zLower, i, j, k) > 0) && ((k + 1 < klast && (vector(FOV_3,i, j, k + 1) > 0)) || (k + 2 < klast && (vector(FOV_3,i, j, k + 2) > 0)))) {
						vector(extrapolatedSBrhoSPi, i, j, k) = vector(rk1rho, i, j, k);
						vector(extrapolatedSBrhoSPj, i, j, k) = vector(rk1rho, i, j, k);
						vector(extrapolatedSBrhoSPk, i, j, k) = vector(rk1rho, i, j, k);
						vector(extrapolatedSBmxSPi, i, j, k) = vector(rk1mx, i, j, k);
						vector(extrapolatedSBmxSPj, i, j, k) = vector(rk1mx, i, j, k);
						vector(extrapolatedSBmxSPk, i, j, k) = vector(rk1mx, i, j, k);
						vector(extrapolatedSBmySPi, i, j, k) = vector(rk1my, i, j, k);
						vector(extrapolatedSBmySPj, i, j, k) = vector(rk1my, i, j, k);
						vector(extrapolatedSBmySPk, i, j, k) = vector(rk1my, i, j, k);
						vector(extrapolatedSBmzSPi, i, j, k) = vector(rk1mz, i, j, k);
						vector(extrapolatedSBmzSPj, i, j, k) = vector(rk1mz, i, j, k);
						vector(extrapolatedSBmzSPk, i, j, k) = vector(rk1mz, i, j, k);
						vector(extrapolatedSBNullSPi, i, j, k) = vector(rk1Null, i, j, k);
						vector(extrapolatedSBNullSPj, i, j, k) = vector(rk1Null, i, j, k);
						vector(extrapolatedSBNullSPk, i, j, k) = vector(rk1Null, i, j, k);
						vector(extrapolatedSBpSPi, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBpSPj, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBpSPk, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBfxSPi, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfxSPj, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfxSPk, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfySPi, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfySPj, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfySPk, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfzSPi, i, j, k) = vector(fz, i, j, k);
						vector(extrapolatedSBfzSPj, i, j, k) = vector(fz, i, j, k);
						vector(extrapolatedSBfzSPk, i, j, k) = vector(fz, i, j, k);
					}
					if ((vector(FOV_zUpper, i, j, k) > 0) && ((k - 1 >= 0 && (vector(FOV_3, i, j, k - 1) > 0)) || (k - 2 >= 0 && (vector(FOV_3, i, j, k - 2) > 0)))) {
						vector(extrapolatedSBrhoSPi, i, j, k) = vector(rk1rho, i, j, k);
						vector(extrapolatedSBrhoSPj, i, j, k) = vector(rk1rho, i, j, k);
						vector(extrapolatedSBrhoSPk, i, j, k) = vector(rk1rho, i, j, k);
						vector(extrapolatedSBmxSPi, i, j, k) = vector(rk1mx, i, j, k);
						vector(extrapolatedSBmxSPj, i, j, k) = vector(rk1mx, i, j, k);
						vector(extrapolatedSBmxSPk, i, j, k) = vector(rk1mx, i, j, k);
						vector(extrapolatedSBmySPi, i, j, k) = vector(rk1my, i, j, k);
						vector(extrapolatedSBmySPj, i, j, k) = vector(rk1my, i, j, k);
						vector(extrapolatedSBmySPk, i, j, k) = vector(rk1my, i, j, k);
						vector(extrapolatedSBmzSPi, i, j, k) = vector(rk1mz, i, j, k);
						vector(extrapolatedSBmzSPj, i, j, k) = vector(rk1mz, i, j, k);
						vector(extrapolatedSBmzSPk, i, j, k) = vector(rk1mz, i, j, k);
						vector(extrapolatedSBNullSPi, i, j, k) = vector(rk1Null, i, j, k);
						vector(extrapolatedSBNullSPj, i, j, k) = vector(rk1Null, i, j, k);
						vector(extrapolatedSBNullSPk, i, j, k) = vector(rk1Null, i, j, k);
						vector(extrapolatedSBpSPi, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBpSPj, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBpSPk, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBfxSPi, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfxSPj, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfxSPk, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfySPi, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfySPj, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfySPk, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfzSPi, i, j, k) = vector(fz, i, j, k);
						vector(extrapolatedSBfzSPj, i, j, k) = vector(fz, i, j, k);
						vector(extrapolatedSBfzSPk, i, j, k) = vector(fz, i, j, k);
					}
					if ((vector(FOV_xLower, i, j, k) > 0) && ((i + 1 < ilast && (vector(FOV_1,i + 1, j, k) > 0)) || (i + 2 < ilast && (vector(FOV_1,i + 2, j, k) > 0)))) {
						vector(extrapolatedSBNullSPi, i, j, k) = vector(rk1Null, i, j, k);
						vector(extrapolatedSBNullSPj, i, j, k) = vector(rk1Null, i, j, k);
						vector(extrapolatedSBNullSPk, i, j, k) = vector(rk1Null, i, j, k);
					}
					if ((vector(FOV_xUpper, i, j, k) > 0) && ((i - 1 >= 0 && (vector(FOV_1, i - 1, j, k) > 0)) || (i - 2 >= 0 && (vector(FOV_1, i - 2, j, k) > 0)))) {
						vector(extrapolatedSBNullSPi, i, j, k) = vector(rk1Null, i, j, k);
						vector(extrapolatedSBNullSPj, i, j, k) = vector(rk1Null, i, j, k);
						vector(extrapolatedSBNullSPk, i, j, k) = vector(rk1Null, i, j, k);
					}
					if ((vector(FOV_yLower, i, j, k) > 0) && ((j + 1 < jlast && (vector(FOV_1,i, j + 1, k) > 0)) || (j + 2 < jlast && (vector(FOV_1,i, j + 2, k) > 0)))) {
						vector(extrapolatedSBrhoSPi, i, j, k) = vector(rk1rho, i, j, k);
						vector(extrapolatedSBrhoSPj, i, j, k) = vector(rk1rho, i, j, k);
						vector(extrapolatedSBrhoSPk, i, j, k) = vector(rk1rho, i, j, k);
						vector(extrapolatedSBmxSPi, i, j, k) = vector(rk1mx, i, j, k);
						vector(extrapolatedSBmxSPj, i, j, k) = vector(rk1mx, i, j, k);
						vector(extrapolatedSBmxSPk, i, j, k) = vector(rk1mx, i, j, k);
						vector(extrapolatedSBmySPi, i, j, k) = vector(rk1my, i, j, k);
						vector(extrapolatedSBmySPj, i, j, k) = vector(rk1my, i, j, k);
						vector(extrapolatedSBmySPk, i, j, k) = vector(rk1my, i, j, k);
						vector(extrapolatedSBmzSPi, i, j, k) = vector(rk1mz, i, j, k);
						vector(extrapolatedSBmzSPj, i, j, k) = vector(rk1mz, i, j, k);
						vector(extrapolatedSBmzSPk, i, j, k) = vector(rk1mz, i, j, k);
						vector(extrapolatedSBNullSPi, i, j, k) = vector(rk1Null, i, j, k);
						vector(extrapolatedSBNullSPj, i, j, k) = vector(rk1Null, i, j, k);
						vector(extrapolatedSBNullSPk, i, j, k) = vector(rk1Null, i, j, k);
						vector(extrapolatedSBpSPi, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBpSPj, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBpSPk, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBfxSPi, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfxSPj, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfxSPk, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfySPi, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfySPj, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfySPk, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfzSPi, i, j, k) = vector(fz, i, j, k);
						vector(extrapolatedSBfzSPj, i, j, k) = vector(fz, i, j, k);
						vector(extrapolatedSBfzSPk, i, j, k) = vector(fz, i, j, k);
					}
					if ((vector(FOV_yUpper, i, j, k) > 0) && ((j - 1 >= 0 && (vector(FOV_1, i, j - 1, k) > 0)) || (j - 2 >= 0 && (vector(FOV_1, i, j - 2, k) > 0)))) {
						vector(extrapolatedSBrhoSPi, i, j, k) = vector(rk1rho, i, j, k);
						vector(extrapolatedSBrhoSPj, i, j, k) = vector(rk1rho, i, j, k);
						vector(extrapolatedSBrhoSPk, i, j, k) = vector(rk1rho, i, j, k);
						vector(extrapolatedSBmxSPi, i, j, k) = vector(rk1mx, i, j, k);
						vector(extrapolatedSBmxSPj, i, j, k) = vector(rk1mx, i, j, k);
						vector(extrapolatedSBmxSPk, i, j, k) = vector(rk1mx, i, j, k);
						vector(extrapolatedSBmySPi, i, j, k) = vector(rk1my, i, j, k);
						vector(extrapolatedSBmySPj, i, j, k) = vector(rk1my, i, j, k);
						vector(extrapolatedSBmySPk, i, j, k) = vector(rk1my, i, j, k);
						vector(extrapolatedSBmzSPi, i, j, k) = vector(rk1mz, i, j, k);
						vector(extrapolatedSBmzSPj, i, j, k) = vector(rk1mz, i, j, k);
						vector(extrapolatedSBmzSPk, i, j, k) = vector(rk1mz, i, j, k);
						vector(extrapolatedSBNullSPi, i, j, k) = vector(rk1Null, i, j, k);
						vector(extrapolatedSBNullSPj, i, j, k) = vector(rk1Null, i, j, k);
						vector(extrapolatedSBNullSPk, i, j, k) = vector(rk1Null, i, j, k);
						vector(extrapolatedSBpSPi, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBpSPj, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBpSPk, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBfxSPi, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfxSPj, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfxSPk, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfySPi, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfySPj, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfySPk, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfzSPi, i, j, k) = vector(fz, i, j, k);
						vector(extrapolatedSBfzSPj, i, j, k) = vector(fz, i, j, k);
						vector(extrapolatedSBfzSPk, i, j, k) = vector(fz, i, j, k);
					}
					if ((vector(FOV_zLower, i, j, k) > 0) && ((k + 1 < klast && (vector(FOV_1,i, j, k + 1) > 0)) || (k + 2 < klast && (vector(FOV_1,i, j, k + 2) > 0)))) {
						vector(extrapolatedSBrhoSPi, i, j, k) = vector(rk1rho, i, j, k);
						vector(extrapolatedSBrhoSPj, i, j, k) = vector(rk1rho, i, j, k);
						vector(extrapolatedSBrhoSPk, i, j, k) = vector(rk1rho, i, j, k);
						vector(extrapolatedSBmxSPi, i, j, k) = vector(rk1mx, i, j, k);
						vector(extrapolatedSBmxSPj, i, j, k) = vector(rk1mx, i, j, k);
						vector(extrapolatedSBmxSPk, i, j, k) = vector(rk1mx, i, j, k);
						vector(extrapolatedSBmySPi, i, j, k) = vector(rk1my, i, j, k);
						vector(extrapolatedSBmySPj, i, j, k) = vector(rk1my, i, j, k);
						vector(extrapolatedSBmySPk, i, j, k) = vector(rk1my, i, j, k);
						vector(extrapolatedSBmzSPi, i, j, k) = vector(rk1mz, i, j, k);
						vector(extrapolatedSBmzSPj, i, j, k) = vector(rk1mz, i, j, k);
						vector(extrapolatedSBmzSPk, i, j, k) = vector(rk1mz, i, j, k);
						vector(extrapolatedSBNullSPi, i, j, k) = vector(rk1Null, i, j, k);
						vector(extrapolatedSBNullSPj, i, j, k) = vector(rk1Null, i, j, k);
						vector(extrapolatedSBNullSPk, i, j, k) = vector(rk1Null, i, j, k);
						vector(extrapolatedSBpSPi, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBpSPj, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBpSPk, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBfxSPi, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfxSPj, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfxSPk, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfySPi, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfySPj, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfySPk, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfzSPi, i, j, k) = vector(fz, i, j, k);
						vector(extrapolatedSBfzSPj, i, j, k) = vector(fz, i, j, k);
						vector(extrapolatedSBfzSPk, i, j, k) = vector(fz, i, j, k);
					}
					if ((vector(FOV_zUpper, i, j, k) > 0) && ((k - 1 >= 0 && (vector(FOV_1, i, j, k - 1) > 0)) || (k - 2 >= 0 && (vector(FOV_1, i, j, k - 2) > 0)))) {
						vector(extrapolatedSBrhoSPi, i, j, k) = vector(rk1rho, i, j, k);
						vector(extrapolatedSBrhoSPj, i, j, k) = vector(rk1rho, i, j, k);
						vector(extrapolatedSBrhoSPk, i, j, k) = vector(rk1rho, i, j, k);
						vector(extrapolatedSBmxSPi, i, j, k) = vector(rk1mx, i, j, k);
						vector(extrapolatedSBmxSPj, i, j, k) = vector(rk1mx, i, j, k);
						vector(extrapolatedSBmxSPk, i, j, k) = vector(rk1mx, i, j, k);
						vector(extrapolatedSBmySPi, i, j, k) = vector(rk1my, i, j, k);
						vector(extrapolatedSBmySPj, i, j, k) = vector(rk1my, i, j, k);
						vector(extrapolatedSBmySPk, i, j, k) = vector(rk1my, i, j, k);
						vector(extrapolatedSBmzSPi, i, j, k) = vector(rk1mz, i, j, k);
						vector(extrapolatedSBmzSPj, i, j, k) = vector(rk1mz, i, j, k);
						vector(extrapolatedSBmzSPk, i, j, k) = vector(rk1mz, i, j, k);
						vector(extrapolatedSBNullSPi, i, j, k) = vector(rk1Null, i, j, k);
						vector(extrapolatedSBNullSPj, i, j, k) = vector(rk1Null, i, j, k);
						vector(extrapolatedSBNullSPk, i, j, k) = vector(rk1Null, i, j, k);
						vector(extrapolatedSBpSPi, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBpSPj, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBpSPk, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBfxSPi, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfxSPj, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfxSPk, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfySPi, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfySPj, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfySPk, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfzSPi, i, j, k) = vector(fz, i, j, k);
						vector(extrapolatedSBfzSPj, i, j, k) = vector(fz, i, j, k);
						vector(extrapolatedSBfzSPk, i, j, k) = vector(fz, i, j, k);
					}
				}
			}
		}
	}
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch >& patch = *p_it;
		double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_3 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_3_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
		double* FOV_zLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zLower_id).get())->getPointer();
		double* FOV_zUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zUpper_id).get())->getPointer();
	
		double* SpeedSBi_3 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBi_3_id).get())->getPointer();
		double* SpeedSBj_3 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBj_3_id).get())->getPointer();
		double* SpeedSBk_3 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBk_3_id).get())->getPointer();
		double* extrapolatedSBmxSPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmxSPi_id).get())->getPointer();
		double* extrapolatedSBmySPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmySPi_id).get())->getPointer();
		double* extrapolatedSBmzSPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmzSPi_id).get())->getPointer();
		double* extrapolatedSBrhoSPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBrhoSPi_id).get())->getPointer();
		double* SpeedSBi_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBi_1_id).get())->getPointer();
		double* extrapolatedSBmySPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmySPj_id).get())->getPointer();
		double* extrapolatedSBmxSPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmxSPj_id).get())->getPointer();
		double* extrapolatedSBmzSPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmzSPj_id).get())->getPointer();
		double* extrapolatedSBrhoSPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBrhoSPj_id).get())->getPointer();
		double* SpeedSBj_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBj_1_id).get())->getPointer();
		double* extrapolatedSBmzSPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmzSPk_id).get())->getPointer();
		double* extrapolatedSBmxSPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmxSPk_id).get())->getPointer();
		double* extrapolatedSBmySPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmySPk_id).get())->getPointer();
		double* extrapolatedSBrhoSPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBrhoSPk_id).get())->getPointer();
		double* SpeedSBk_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBk_1_id).get())->getPointer();
		double m_n, m_tx, m_ty, m_tz, extrapolatedSBcnSPi, extrapolatedSBcSPi, extrapolatedSBcnSPj, extrapolatedSBcSPj, extrapolatedSBcnSPk, extrapolatedSBcSPk;
	
		//Get the dimensions of the patch
		const hier::Index boxfirst = patch->getBox().lower();
		const hier::Index boxlast = patch->getBox().upper();
	
		//Get delta spaces into an array. dx, dy, dz.
		std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
		const double* dx  = patch_geom->getDx();
	
		//Auxiliary definitions
		int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
		int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
		int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;
		for(int k = 0; k < klast; k++) {
			for(int j = 0; j < jlast; j++) {
				for(int i = 0; i < ilast; i++) {
					if ((vector(FOV_3, i, j, k) > 0 || (((i + 1 < ilast && vector(FOV_3, i + 1, j, k) > 0) || (i + 2 < ilast && vector(FOV_3, i + 2, j, k) > 0) || (j + 1 < jlast && vector(FOV_3, i, j + 1, k) > 0) || (j + 2 < jlast && vector(FOV_3, i, j + 2, k) > 0) || (k + 1 < klast && vector(FOV_3, i, j, k + 1) > 0) || (k + 2 < klast && vector(FOV_3, i, j, k + 2) > 0)) || ((i - 1 >= 0 && vector(FOV_3, i - 1, j, k) > 0) || (i - 2 >= 0 && vector(FOV_3, i - 2, j, k) > 0) || (j - 1 >= 0 && vector(FOV_3, i, j - 1, k) > 0) || (j - 2 >= 0 && vector(FOV_3, i, j - 2, k) > 0) || (k - 1 >= 0 && vector(FOV_3, i, j, k - 1) > 0) || (k - 2 >= 0 && vector(FOV_3, i, j, k - 2) > 0))) )) {
						vector(SpeedSBi_3, i, j, k) = fabs(1.0);
						vector(SpeedSBj_3, i, j, k) = fabs(1.0);
						vector(SpeedSBk_3, i, j, k) = fabs(1.0);
					}
					if ((vector(FOV_1, i, j, k) > 0 || (((i + 1 < ilast && vector(FOV_1, i + 1, j, k) > 0) || (i + 2 < ilast && vector(FOV_1, i + 2, j, k) > 0) || (j + 1 < jlast && vector(FOV_1, i, j + 1, k) > 0) || (j + 2 < jlast && vector(FOV_1, i, j + 2, k) > 0) || (k + 1 < klast && vector(FOV_1, i, j, k + 1) > 0) || (k + 2 < klast && vector(FOV_1, i, j, k + 2) > 0)) || ((i - 1 >= 0 && vector(FOV_1, i - 1, j, k) > 0) || (i - 2 >= 0 && vector(FOV_1, i - 2, j, k) > 0) || (j - 1 >= 0 && vector(FOV_1, i, j - 1, k) > 0) || (j - 2 >= 0 && vector(FOV_1, i, j - 2, k) > 0) || (k - 1 >= 0 && vector(FOV_1, i, j, k - 1) > 0) || (k - 2 >= 0 && vector(FOV_1, i, j, k - 2) > 0))) )) {
						m_n = vector(extrapolatedSBmxSPi, i, j, k);
						m_tx = 0.0;
						m_ty = vector(extrapolatedSBmySPi, i, j, k);
						m_tz = vector(extrapolatedSBmzSPi, i, j, k);
						extrapolatedSBcnSPi = m_n / vector(extrapolatedSBrhoSPi, i, j, k);
						extrapolatedSBcSPi = c0 * (pow((vector(extrapolatedSBrhoSPi, i, j, k) / rho0), ((gamma + (-1.0)) / 2.0)));
						vector(SpeedSBi_1, i, j, k) = MAX(fabs(extrapolatedSBcSPi + extrapolatedSBcnSPi), MAX(fabs((-extrapolatedSBcSPi) + extrapolatedSBcnSPi), MAX(fabs(extrapolatedSBcnSPi), MAX(fabs(extrapolatedSBcnSPi), fabs(extrapolatedSBcnSPi)))));
						m_n = vector(extrapolatedSBmySPj, i, j, k);
						m_tx = vector(extrapolatedSBmxSPj, i, j, k);
						m_ty = 0.0;
						m_tz = vector(extrapolatedSBmzSPj, i, j, k);
						extrapolatedSBcnSPj = m_n / vector(extrapolatedSBrhoSPj, i, j, k);
						extrapolatedSBcSPj = c0 * (pow((vector(extrapolatedSBrhoSPj, i, j, k) / rho0), ((gamma + (-1.0)) / 2.0)));
						vector(SpeedSBj_1, i, j, k) = MAX(fabs(extrapolatedSBcSPj + extrapolatedSBcnSPj), MAX(fabs((-extrapolatedSBcSPj) + extrapolatedSBcnSPj), MAX(fabs(extrapolatedSBcnSPj), MAX(fabs(extrapolatedSBcnSPj), fabs(extrapolatedSBcnSPj)))));
						m_n = vector(extrapolatedSBmzSPk, i, j, k);
						m_tx = vector(extrapolatedSBmxSPk, i, j, k);
						m_ty = vector(extrapolatedSBmySPk, i, j, k);
						m_tz = 0.0;
						extrapolatedSBcnSPk = m_n / vector(extrapolatedSBrhoSPk, i, j, k);
						extrapolatedSBcSPk = c0 * (pow((vector(extrapolatedSBrhoSPk, i, j, k) / rho0), ((gamma + (-1.0)) / 2.0)));
						vector(SpeedSBk_1, i, j, k) = MAX(fabs(extrapolatedSBcSPk + extrapolatedSBcnSPk), MAX(fabs((-extrapolatedSBcSPk) + extrapolatedSBcnSPk), MAX(fabs(extrapolatedSBcnSPk), MAX(fabs(extrapolatedSBcnSPk), fabs(extrapolatedSBcnSPk)))));
					}
				}
			}
		}
	}
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch >& patch = *p_it;
		double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_3 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_3_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
		double* FOV_zLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zLower_id).get())->getPointer();
		double* FOV_zUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zUpper_id).get())->getPointer();
	
		double* SpeedSBi_3 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBi_3_id).get())->getPointer();
		double* SpeedSBj_3 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBj_3_id).get())->getPointer();
		double* SpeedSBk_3 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBk_3_id).get())->getPointer();
		double* SpeedSBi_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBi_1_id).get())->getPointer();
		double* SpeedSBj_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBj_1_id).get())->getPointer();
		double* SpeedSBk_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBk_1_id).get())->getPointer();
	
		//Get the dimensions of the patch
		const hier::Index boxfirst = patch->getBox().lower();
		const hier::Index boxlast = patch->getBox().upper();
	
		//Get delta spaces into an array. dx, dy, dz.
		std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
		const double* dx  = patch_geom->getDx();
	
		//Auxiliary definitions
		int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
		int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
		int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;
		for(int k = 0; k < klast; k++) {
			for(int j = 0; j < jlast; j++) {
				for(int i = 0; i < ilast; i++) {
					if ((vector(FOV_3, i, j, k) > 0 || (((i + 1 < ilast && vector(FOV_3, i + 1, j, k) > 0) || (j + 1 < jlast && vector(FOV_3, i, j + 1, k) > 0) || (k + 1 < klast && vector(FOV_3, i, j, k + 1) > 0)) || ((i - 1 >= 0 && vector(FOV_3, i - 1, j, k) > 0) || (j - 1 >= 0 && vector(FOV_3, i, j - 1, k) > 0) || (k - 1 >= 0 && vector(FOV_3, i, j, k - 1) > 0))) ) && (i + 1 < ilast && i - 1 >= 0 && j + 1 < jlast && j - 1 >= 0 && k + 1 < klast && k - 1 >= 0)) {
						vector(SpeedSBi_3, i, j, k) = MAX(vector(SpeedSBi_3, i, j, k), vector(SpeedSBi_3, i + 1, j, k));
						vector(SpeedSBj_3, i, j, k) = MAX(vector(SpeedSBj_3, i, j, k), vector(SpeedSBj_3, i, j + 1, k));
						vector(SpeedSBk_3, i, j, k) = MAX(vector(SpeedSBk_3, i, j, k), vector(SpeedSBk_3, i, j, k + 1));
					}
					if ((vector(FOV_1, i, j, k) > 0 || (((i + 1 < ilast && vector(FOV_1, i + 1, j, k) > 0) || (j + 1 < jlast && vector(FOV_1, i, j + 1, k) > 0) || (k + 1 < klast && vector(FOV_1, i, j, k + 1) > 0)) || ((i - 1 >= 0 && vector(FOV_1, i - 1, j, k) > 0) || (j - 1 >= 0 && vector(FOV_1, i, j - 1, k) > 0) || (k - 1 >= 0 && vector(FOV_1, i, j, k - 1) > 0))) ) && (i + 1 < ilast && i - 1 >= 0 && j + 1 < jlast && j - 1 >= 0 && k + 1 < klast && k - 1 >= 0)) {
						vector(SpeedSBi_1, i, j, k) = MAX(vector(SpeedSBi_1, i, j, k), vector(SpeedSBi_1, i + 1, j, k));
						vector(SpeedSBj_1, i, j, k) = MAX(vector(SpeedSBj_1, i, j, k), vector(SpeedSBj_1, i, j + 1, k));
						vector(SpeedSBk_1, i, j, k) = MAX(vector(SpeedSBk_1, i, j, k), vector(SpeedSBk_1, i, j, k + 1));
					}
				}
			}
		}
	}
	//Fill ghosts and periodical boundaries
	d_bdry_sched_advance9[ln]->fillData(current_time, false);
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch >& patch = *p_it;
		double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_3 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_3_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
		double* FOV_zLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zLower_id).get())->getPointer();
		double* FOV_zUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zUpper_id).get())->getPointer();
	
		double* ad_mx_o0_t0_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_mx_o0_t0_m0_l0_1_id).get())->getPointer();
		double* rk1mx = ((pdat::NodeData<double> *) patch->getPatchData(d_rk1mx_id).get())->getPointer();
		double* rk1rho = ((pdat::NodeData<double> *) patch->getPatchData(d_rk1rho_id).get())->getPointer();
		double* ad_my_o0_t3_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_my_o0_t3_m0_l0_1_id).get())->getPointer();
		double* rk1my = ((pdat::NodeData<double> *) patch->getPatchData(d_rk1my_id).get())->getPointer();
		double* ad_mz_o0_t3_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_mz_o0_t3_m0_l0_1_id).get())->getPointer();
		double* rk1mz = ((pdat::NodeData<double> *) patch->getPatchData(d_rk1mz_id).get())->getPointer();
		double* FluxSBimx_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBimx_1_id).get())->getPointer();
		double* extrapolatedSBrhoSPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBrhoSPi_id).get())->getPointer();
		double* extrapolatedSBmxSPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmxSPi_id).get())->getPointer();
		double* extrapolatedSBpSPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBpSPi_id).get())->getPointer();
		double* FluxSBimy_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBimy_1_id).get())->getPointer();
		double* extrapolatedSBmySPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmySPi_id).get())->getPointer();
		double* FluxSBimz_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBimz_1_id).get())->getPointer();
		double* extrapolatedSBmzSPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmzSPi_id).get())->getPointer();
		double* FluxSBjmx_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjmx_1_id).get())->getPointer();
		double* extrapolatedSBrhoSPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBrhoSPj_id).get())->getPointer();
		double* extrapolatedSBmxSPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmxSPj_id).get())->getPointer();
		double* extrapolatedSBmySPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmySPj_id).get())->getPointer();
		double* FluxSBjmy_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjmy_1_id).get())->getPointer();
		double* extrapolatedSBpSPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBpSPj_id).get())->getPointer();
		double* FluxSBjmz_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjmz_1_id).get())->getPointer();
		double* extrapolatedSBmzSPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmzSPj_id).get())->getPointer();
		double* FluxSBkmx_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkmx_1_id).get())->getPointer();
		double* extrapolatedSBrhoSPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBrhoSPk_id).get())->getPointer();
		double* extrapolatedSBmxSPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmxSPk_id).get())->getPointer();
		double* extrapolatedSBmzSPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmzSPk_id).get())->getPointer();
		double* FluxSBkmy_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkmy_1_id).get())->getPointer();
		double* extrapolatedSBmySPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmySPk_id).get())->getPointer();
		double* FluxSBkmz_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkmz_1_id).get())->getPointer();
		double* extrapolatedSBpSPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBpSPk_id).get())->getPointer();
	
		//Get the dimensions of the patch
		const hier::Index boxfirst = patch->getBox().lower();
		const hier::Index boxlast = patch->getBox().upper();
	
		//Get delta spaces into an array. dx, dy, dz.
		std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
		const double* dx  = patch_geom->getDx();
	
		//Auxiliary definitions
		int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
		int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
		int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;
		for(int k = 0; k < klast; k++) {
			for(int j = 0; j < jlast; j++) {
				for(int i = 0; i < ilast; i++) {
					if ((vector(FOV_1, i, j, k) > 0 || (((i + 1 < ilast && vector(FOV_1, i + 1, j, k) > 0) || (i + 2 < ilast && vector(FOV_1, i + 2, j, k) > 0) || (j + 1 < jlast && vector(FOV_1, i, j + 1, k) > 0) || (j + 2 < jlast && vector(FOV_1, i, j + 2, k) > 0) || (k + 1 < klast && vector(FOV_1, i, j, k + 1) > 0) || (k + 2 < klast && vector(FOV_1, i, j, k + 2) > 0)) || ((i - 1 >= 0 && vector(FOV_1, i - 1, j, k) > 0) || (i - 2 >= 0 && vector(FOV_1, i - 2, j, k) > 0) || (j - 1 >= 0 && vector(FOV_1, i, j - 1, k) > 0) || (j - 2 >= 0 && vector(FOV_1, i, j - 2, k) > 0) || (k - 1 >= 0 && vector(FOV_1, i, j, k - 1) > 0) || (k - 2 >= 0 && vector(FOV_1, i, j, k - 2) > 0)) || (((j + 1 < jlast && k + 1 < klast) && vector(FOV_1, i, j + 1, k + 1) > 0) || ((j + 1 < jlast && k - 1 >= 0) && vector(FOV_1, i, j + 1, k - 1) > 0) || ((j + 1 < jlast && k + 2 < klast) && vector(FOV_1, i, j + 1, k + 2) > 0) || ((j + 1 < jlast && k - 2 >= 0) && vector(FOV_1, i, j + 1, k - 2) > 0) || ((j - 1 >= 0 && k + 1 < klast) && vector(FOV_1, i, j - 1, k + 1) > 0) || ((j - 1 >= 0 && k - 1 >= 0) && vector(FOV_1, i, j - 1, k - 1) > 0) || ((j - 1 >= 0 && k + 2 < klast) && vector(FOV_1, i, j - 1, k + 2) > 0) || ((j - 1 >= 0 && k - 2 >= 0) && vector(FOV_1, i, j - 1, k - 2) > 0) || ((j + 2 < jlast && k + 1 < klast) && vector(FOV_1, i, j + 2, k + 1) > 0) || ((j + 2 < jlast && k - 1 >= 0) && vector(FOV_1, i, j + 2, k - 1) > 0) || ((j + 2 < jlast && k + 2 < klast) && vector(FOV_1, i, j + 2, k + 2) > 0) || ((j + 2 < jlast && k - 2 >= 0) && vector(FOV_1, i, j + 2, k - 2) > 0) || ((j - 2 >= 0 && k + 1 < klast) && vector(FOV_1, i, j - 2, k + 1) > 0) || ((j - 2 >= 0 && k - 1 >= 0) && vector(FOV_1, i, j - 2, k - 1) > 0) || ((j - 2 >= 0 && k + 2 < klast) && vector(FOV_1, i, j - 2, k + 2) > 0) || ((j - 2 >= 0 && k - 2 >= 0) && vector(FOV_1, i, j - 2, k - 2) > 0) || ((i + 1 < ilast && k + 1 < klast) && vector(FOV_1, i + 1, j, k + 1) > 0) || ((i + 1 < ilast && k - 1 >= 0) && vector(FOV_1, i + 1, j, k - 1) > 0) || ((i + 1 < ilast && k + 2 < klast) && vector(FOV_1, i + 1, j, k + 2) > 0) || ((i + 1 < ilast && k - 2 >= 0) && vector(FOV_1, i + 1, j, k - 2) > 0) || ((i + 1 < ilast && j + 1 < jlast) && vector(FOV_1, i + 1, j + 1, k) > 0) || ((i + 1 < ilast && j + 1 < jlast && k + 1 < klast) && vector(FOV_1, i + 1, j + 1, k + 1) > 0) || ((i + 1 < ilast && j + 1 < jlast && k - 1 >= 0) && vector(FOV_1, i + 1, j + 1, k - 1) > 0) || ((i + 1 < ilast && j + 1 < jlast && k + 2 < klast) && vector(FOV_1, i + 1, j + 1, k + 2) > 0) || ((i + 1 < ilast && j + 1 < jlast && k - 2 >= 0) && vector(FOV_1, i + 1, j + 1, k - 2) > 0) || ((i + 1 < ilast && j - 1 >= 0) && vector(FOV_1, i + 1, j - 1, k) > 0) || ((i + 1 < ilast && j - 1 >= 0 && k + 1 < klast) && vector(FOV_1, i + 1, j - 1, k + 1) > 0) || ((i + 1 < ilast && j - 1 >= 0 && k - 1 >= 0) && vector(FOV_1, i + 1, j - 1, k - 1) > 0) || ((i + 1 < ilast && j - 1 >= 0 && k + 2 < klast) && vector(FOV_1, i + 1, j - 1, k + 2) > 0) || ((i + 1 < ilast && j - 1 >= 0 && k - 2 >= 0) && vector(FOV_1, i + 1, j - 1, k - 2) > 0) || ((i + 1 < ilast && j + 2 < jlast) && vector(FOV_1, i + 1, j + 2, k) > 0) || ((i + 1 < ilast && j + 2 < jlast && k + 1 < klast) && vector(FOV_1, i + 1, j + 2, k + 1) > 0) || ((i + 1 < ilast && j + 2 < jlast && k - 1 >= 0) && vector(FOV_1, i + 1, j + 2, k - 1) > 0) || ((i + 1 < ilast && j + 2 < jlast && k + 2 < klast) && vector(FOV_1, i + 1, j + 2, k + 2) > 0) || ((i + 1 < ilast && j + 2 < jlast && k - 2 >= 0) && vector(FOV_1, i + 1, j + 2, k - 2) > 0) || ((i + 1 < ilast && j - 2 >= 0) && vector(FOV_1, i + 1, j - 2, k) > 0) || ((i + 1 < ilast && j - 2 >= 0 && k + 1 < klast) && vector(FOV_1, i + 1, j - 2, k + 1) > 0) || ((i + 1 < ilast && j - 2 >= 0 && k - 1 >= 0) && vector(FOV_1, i + 1, j - 2, k - 1) > 0) || ((i + 1 < ilast && j - 2 >= 0 && k + 2 < klast) && vector(FOV_1, i + 1, j - 2, k + 2) > 0) || ((i + 1 < ilast && j - 2 >= 0 && k - 2 >= 0) && vector(FOV_1, i + 1, j - 2, k - 2) > 0) || ((i - 1 >= 0 && k + 1 < klast) && vector(FOV_1, i - 1, j, k + 1) > 0) || ((i - 1 >= 0 && k - 1 >= 0) && vector(FOV_1, i - 1, j, k - 1) > 0) || ((i - 1 >= 0 && k + 2 < klast) && vector(FOV_1, i - 1, j, k + 2) > 0) || ((i - 1 >= 0 && k - 2 >= 0) && vector(FOV_1, i - 1, j, k - 2) > 0) || ((i - 1 >= 0 && j + 1 < jlast) && vector(FOV_1, i - 1, j + 1, k) > 0) || ((i - 1 >= 0 && j + 1 < jlast && k + 1 < klast) && vector(FOV_1, i - 1, j + 1, k + 1) > 0) || ((i - 1 >= 0 && j + 1 < jlast && k - 1 >= 0) && vector(FOV_1, i - 1, j + 1, k - 1) > 0) || ((i - 1 >= 0 && j + 1 < jlast && k + 2 < klast) && vector(FOV_1, i - 1, j + 1, k + 2) > 0) || ((i - 1 >= 0 && j + 1 < jlast && k - 2 >= 0) && vector(FOV_1, i - 1, j + 1, k - 2) > 0) || ((i - 1 >= 0 && j - 1 >= 0) && vector(FOV_1, i - 1, j - 1, k) > 0) || ((i - 1 >= 0 && j - 1 >= 0 && k + 1 < klast) && vector(FOV_1, i - 1, j - 1, k + 1) > 0) || ((i - 1 >= 0 && j - 1 >= 0 && k - 1 >= 0) && vector(FOV_1, i - 1, j - 1, k - 1) > 0) || ((i - 1 >= 0 && j - 1 >= 0 && k + 2 < klast) && vector(FOV_1, i - 1, j - 1, k + 2) > 0) || ((i - 1 >= 0 && j - 1 >= 0 && k - 2 >= 0) && vector(FOV_1, i - 1, j - 1, k - 2) > 0) || ((i - 1 >= 0 && j + 2 < jlast) && vector(FOV_1, i - 1, j + 2, k) > 0) || ((i - 1 >= 0 && j + 2 < jlast && k + 1 < klast) && vector(FOV_1, i - 1, j + 2, k + 1) > 0) || ((i - 1 >= 0 && j + 2 < jlast && k - 1 >= 0) && vector(FOV_1, i - 1, j + 2, k - 1) > 0) || ((i - 1 >= 0 && j + 2 < jlast && k + 2 < klast) && vector(FOV_1, i - 1, j + 2, k + 2) > 0) || ((i - 1 >= 0 && j + 2 < jlast && k - 2 >= 0) && vector(FOV_1, i - 1, j + 2, k - 2) > 0) || ((i - 1 >= 0 && j - 2 >= 0) && vector(FOV_1, i - 1, j - 2, k) > 0) || ((i - 1 >= 0 && j - 2 >= 0 && k + 1 < klast) && vector(FOV_1, i - 1, j - 2, k + 1) > 0) || ((i - 1 >= 0 && j - 2 >= 0 && k - 1 >= 0) && vector(FOV_1, i - 1, j - 2, k - 1) > 0) || ((i - 1 >= 0 && j - 2 >= 0 && k + 2 < klast) && vector(FOV_1, i - 1, j - 2, k + 2) > 0) || ((i - 1 >= 0 && j - 2 >= 0 && k - 2 >= 0) && vector(FOV_1, i - 1, j - 2, k - 2) > 0) || ((i + 2 < ilast && k + 1 < klast) && vector(FOV_1, i + 2, j, k + 1) > 0) || ((i + 2 < ilast && k - 1 >= 0) && vector(FOV_1, i + 2, j, k - 1) > 0) || ((i + 2 < ilast && k + 2 < klast) && vector(FOV_1, i + 2, j, k + 2) > 0) || ((i + 2 < ilast && k - 2 >= 0) && vector(FOV_1, i + 2, j, k - 2) > 0) || ((i + 2 < ilast && j + 1 < jlast) && vector(FOV_1, i + 2, j + 1, k) > 0) || ((i + 2 < ilast && j + 1 < jlast && k + 1 < klast) && vector(FOV_1, i + 2, j + 1, k + 1) > 0) || ((i + 2 < ilast && j + 1 < jlast && k - 1 >= 0) && vector(FOV_1, i + 2, j + 1, k - 1) > 0) || ((i + 2 < ilast && j + 1 < jlast && k + 2 < klast) && vector(FOV_1, i + 2, j + 1, k + 2) > 0) || ((i + 2 < ilast && j + 1 < jlast && k - 2 >= 0) && vector(FOV_1, i + 2, j + 1, k - 2) > 0) || ((i + 2 < ilast && j - 1 >= 0) && vector(FOV_1, i + 2, j - 1, k) > 0) || ((i + 2 < ilast && j - 1 >= 0 && k + 1 < klast) && vector(FOV_1, i + 2, j - 1, k + 1) > 0) || ((i + 2 < ilast && j - 1 >= 0 && k - 1 >= 0) && vector(FOV_1, i + 2, j - 1, k - 1) > 0) || ((i + 2 < ilast && j - 1 >= 0 && k + 2 < klast) && vector(FOV_1, i + 2, j - 1, k + 2) > 0) || ((i + 2 < ilast && j - 1 >= 0 && k - 2 >= 0) && vector(FOV_1, i + 2, j - 1, k - 2) > 0) || ((i + 2 < ilast && j + 2 < jlast) && vector(FOV_1, i + 2, j + 2, k) > 0) || ((i + 2 < ilast && j + 2 < jlast && k + 1 < klast) && vector(FOV_1, i + 2, j + 2, k + 1) > 0) || ((i + 2 < ilast && j + 2 < jlast && k - 1 >= 0) && vector(FOV_1, i + 2, j + 2, k - 1) > 0) || ((i + 2 < ilast && j + 2 < jlast && k + 2 < klast) && vector(FOV_1, i + 2, j + 2, k + 2) > 0) || ((i + 2 < ilast && j + 2 < jlast && k - 2 >= 0) && vector(FOV_1, i + 2, j + 2, k - 2) > 0) || ((i + 2 < ilast && j - 2 >= 0) && vector(FOV_1, i + 2, j - 2, k) > 0) || ((i + 2 < ilast && j - 2 >= 0 && k + 1 < klast) && vector(FOV_1, i + 2, j - 2, k + 1) > 0) || ((i + 2 < ilast && j - 2 >= 0 && k - 1 >= 0) && vector(FOV_1, i + 2, j - 2, k - 1) > 0) || ((i + 2 < ilast && j - 2 >= 0 && k + 2 < klast) && vector(FOV_1, i + 2, j - 2, k + 2) > 0) || ((i + 2 < ilast && j - 2 >= 0 && k - 2 >= 0) && vector(FOV_1, i + 2, j - 2, k - 2) > 0) || ((i - 2 >= 0 && k + 1 < klast) && vector(FOV_1, i - 2, j, k + 1) > 0) || ((i - 2 >= 0 && k - 1 >= 0) && vector(FOV_1, i - 2, j, k - 1) > 0) || ((i - 2 >= 0 && k + 2 < klast) && vector(FOV_1, i - 2, j, k + 2) > 0) || ((i - 2 >= 0 && k - 2 >= 0) && vector(FOV_1, i - 2, j, k - 2) > 0) || ((i - 2 >= 0 && j + 1 < jlast) && vector(FOV_1, i - 2, j + 1, k) > 0) || ((i - 2 >= 0 && j + 1 < jlast && k + 1 < klast) && vector(FOV_1, i - 2, j + 1, k + 1) > 0) || ((i - 2 >= 0 && j + 1 < jlast && k - 1 >= 0) && vector(FOV_1, i - 2, j + 1, k - 1) > 0) || ((i - 2 >= 0 && j + 1 < jlast && k + 2 < klast) && vector(FOV_1, i - 2, j + 1, k + 2) > 0) || ((i - 2 >= 0 && j + 1 < jlast && k - 2 >= 0) && vector(FOV_1, i - 2, j + 1, k - 2) > 0) || ((i - 2 >= 0 && j - 1 >= 0) && vector(FOV_1, i - 2, j - 1, k) > 0) || ((i - 2 >= 0 && j - 1 >= 0 && k + 1 < klast) && vector(FOV_1, i - 2, j - 1, k + 1) > 0) || ((i - 2 >= 0 && j - 1 >= 0 && k - 1 >= 0) && vector(FOV_1, i - 2, j - 1, k - 1) > 0) || ((i - 2 >= 0 && j - 1 >= 0 && k + 2 < klast) && vector(FOV_1, i - 2, j - 1, k + 2) > 0) || ((i - 2 >= 0 && j - 1 >= 0 && k - 2 >= 0) && vector(FOV_1, i - 2, j - 1, k - 2) > 0) || ((i - 2 >= 0 && j + 2 < jlast) && vector(FOV_1, i - 2, j + 2, k) > 0) || ((i - 2 >= 0 && j + 2 < jlast && k + 1 < klast) && vector(FOV_1, i - 2, j + 2, k + 1) > 0) || ((i - 2 >= 0 && j + 2 < jlast && k - 1 >= 0) && vector(FOV_1, i - 2, j + 2, k - 1) > 0) || ((i - 2 >= 0 && j + 2 < jlast && k + 2 < klast) && vector(FOV_1, i - 2, j + 2, k + 2) > 0) || ((i - 2 >= 0 && j + 2 < jlast && k - 2 >= 0) && vector(FOV_1, i - 2, j + 2, k - 2) > 0) || ((i - 2 >= 0 && j - 2 >= 0) && vector(FOV_1, i - 2, j - 2, k) > 0) || ((i - 2 >= 0 && j - 2 >= 0 && k + 1 < klast) && vector(FOV_1, i - 2, j - 2, k + 1) > 0) || ((i - 2 >= 0 && j - 2 >= 0 && k - 1 >= 0) && vector(FOV_1, i - 2, j - 2, k - 1) > 0) || ((i - 2 >= 0 && j - 2 >= 0 && k + 2 < klast) && vector(FOV_1, i - 2, j - 2, k + 2) > 0) || ((i - 2 >= 0 && j - 2 >= 0 && k - 2 >= 0) && vector(FOV_1, i - 2, j - 2, k - 2) > 0))) )) {
						vector(ad_mx_o0_t0_m0_l0_1, i, j, k) = vector(rk1mx, i, j, k) / vector(rk1rho, i, j, k);
						vector(ad_my_o0_t3_m0_l0_1, i, j, k) = vector(rk1my, i, j, k) / vector(rk1rho, i, j, k);
						vector(ad_mz_o0_t3_m0_l0_1, i, j, k) = vector(rk1mz, i, j, k) / vector(rk1rho, i, j, k);
						vector(FluxSBimx_1, i, j, k) = Fimx_PlaneI(vector(extrapolatedSBrhoSPi, i, j, k), vector(extrapolatedSBmxSPi, i, j, k), vector(extrapolatedSBpSPi, i, j, k), dx, simPlat_dt, ilast, jlast);
						vector(FluxSBimy_1, i, j, k) = Fimy_PlaneI(vector(extrapolatedSBrhoSPi, i, j, k), vector(extrapolatedSBmxSPi, i, j, k), vector(extrapolatedSBmySPi, i, j, k), dx, simPlat_dt, ilast, jlast);
						vector(FluxSBimz_1, i, j, k) = Fimz_PlaneI(vector(extrapolatedSBrhoSPi, i, j, k), vector(extrapolatedSBmxSPi, i, j, k), vector(extrapolatedSBmzSPi, i, j, k), dx, simPlat_dt, ilast, jlast);
						vector(FluxSBjmx_1, i, j, k) = Fjmx_PlaneI(vector(extrapolatedSBrhoSPj, i, j, k), vector(extrapolatedSBmxSPj, i, j, k), vector(extrapolatedSBmySPj, i, j, k), dx, simPlat_dt, ilast, jlast);
						vector(FluxSBjmy_1, i, j, k) = Fjmy_PlaneI(vector(extrapolatedSBrhoSPj, i, j, k), vector(extrapolatedSBmySPj, i, j, k), vector(extrapolatedSBpSPj, i, j, k), dx, simPlat_dt, ilast, jlast);
						vector(FluxSBjmz_1, i, j, k) = Fjmz_PlaneI(vector(extrapolatedSBrhoSPj, i, j, k), vector(extrapolatedSBmySPj, i, j, k), vector(extrapolatedSBmzSPj, i, j, k), dx, simPlat_dt, ilast, jlast);
						vector(FluxSBkmx_1, i, j, k) = Fkmx_PlaneI(vector(extrapolatedSBrhoSPk, i, j, k), vector(extrapolatedSBmxSPk, i, j, k), vector(extrapolatedSBmzSPk, i, j, k), dx, simPlat_dt, ilast, jlast);
						vector(FluxSBkmy_1, i, j, k) = Fkmy_PlaneI(vector(extrapolatedSBrhoSPk, i, j, k), vector(extrapolatedSBmySPk, i, j, k), vector(extrapolatedSBmzSPk, i, j, k), dx, simPlat_dt, ilast, jlast);
						vector(FluxSBkmz_1, i, j, k) = Fkmz_PlaneI(vector(extrapolatedSBrhoSPk, i, j, k), vector(extrapolatedSBmzSPk, i, j, k), vector(extrapolatedSBpSPk, i, j, k), dx, simPlat_dt, ilast, jlast);
					}
				}
			}
		}
	}
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch >& patch = *p_it;
		double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_3 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_3_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
		double* FOV_zLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zLower_id).get())->getPointer();
		double* FOV_zUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zUpper_id).get())->getPointer();
	
		double* rk2Null = ((pdat::NodeData<double> *) patch->getPatchData(d_rk2Null_id).get())->getPointer();
		double* Null_p = ((pdat::NodeData<double> *) patch->getPatchData(d_Null_p_id).get())->getPointer();
		double* rk1Null = ((pdat::NodeData<double> *) patch->getPatchData(d_rk1Null_id).get())->getPointer();
		double* ad_mx_o0_t0_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_mx_o0_t0_m0_l0_1_id).get())->getPointer();
		double* ad_my_o0_t3_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_my_o0_t3_m0_l0_1_id).get())->getPointer();
		double* ad_mz_o0_t3_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_mz_o0_t3_m0_l0_1_id).get())->getPointer();
		double* rk1rho = ((pdat::NodeData<double> *) patch->getPatchData(d_rk1rho_id).get())->getPointer();
		double* fx = ((pdat::NodeData<double> *) patch->getPatchData(d_fx_id).get())->getPointer();
		double* fy = ((pdat::NodeData<double> *) patch->getPatchData(d_fy_id).get())->getPointer();
		double* fz = ((pdat::NodeData<double> *) patch->getPatchData(d_fz_id).get())->getPointer();
		double* extrapolatedSBrhoSPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBrhoSPi_id).get())->getPointer();
		double* extrapolatedSBmxSPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmxSPi_id).get())->getPointer();
		double* SpeedSBi_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBi_1_id).get())->getPointer();
		double* extrapolatedSBrhoSPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBrhoSPj_id).get())->getPointer();
		double* extrapolatedSBmySPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmySPj_id).get())->getPointer();
		double* SpeedSBj_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBj_1_id).get())->getPointer();
		double* extrapolatedSBrhoSPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBrhoSPk_id).get())->getPointer();
		double* extrapolatedSBmzSPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmzSPk_id).get())->getPointer();
		double* SpeedSBk_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBk_1_id).get())->getPointer();
		double* FluxSBimx_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBimx_1_id).get())->getPointer();
		double* extrapolatedSBmxSPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmxSPj_id).get())->getPointer();
		double* FluxSBjmx_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjmx_1_id).get())->getPointer();
		double* extrapolatedSBmxSPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmxSPk_id).get())->getPointer();
		double* FluxSBkmx_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkmx_1_id).get())->getPointer();
		double* extrapolatedSBmySPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmySPi_id).get())->getPointer();
		double* FluxSBimy_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBimy_1_id).get())->getPointer();
		double* FluxSBjmy_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjmy_1_id).get())->getPointer();
		double* extrapolatedSBmySPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmySPk_id).get())->getPointer();
		double* FluxSBkmy_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkmy_1_id).get())->getPointer();
		double* extrapolatedSBmzSPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmzSPi_id).get())->getPointer();
		double* FluxSBimz_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBimz_1_id).get())->getPointer();
		double* extrapolatedSBmzSPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmzSPj_id).get())->getPointer();
		double* FluxSBjmz_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjmz_1_id).get())->getPointer();
		double* FluxSBkmz_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkmz_1_id).get())->getPointer();
		double* rk1mx = ((pdat::NodeData<double> *) patch->getPatchData(d_rk1mx_id).get())->getPointer();
		double* rk1my = ((pdat::NodeData<double> *) patch->getPatchData(d_rk1my_id).get())->getPointer();
		double* rk1mz = ((pdat::NodeData<double> *) patch->getPatchData(d_rk1mz_id).get())->getPointer();
		double* rk2rho = ((pdat::NodeData<double> *) patch->getPatchData(d_rk2rho_id).get())->getPointer();
		double* rho_p = ((pdat::NodeData<double> *) patch->getPatchData(d_rho_p_id).get())->getPointer();
		double* rk2mx = ((pdat::NodeData<double> *) patch->getPatchData(d_rk2mx_id).get())->getPointer();
		double* mx_p = ((pdat::NodeData<double> *) patch->getPatchData(d_mx_p_id).get())->getPointer();
		double* rk2my = ((pdat::NodeData<double> *) patch->getPatchData(d_rk2my_id).get())->getPointer();
		double* my_p = ((pdat::NodeData<double> *) patch->getPatchData(d_my_p_id).get())->getPointer();
		double* rk2mz = ((pdat::NodeData<double> *) patch->getPatchData(d_rk2mz_id).get())->getPointer();
		double* mz_p = ((pdat::NodeData<double> *) patch->getPatchData(d_mz_p_id).get())->getPointer();
		double RHS_Null, d_mx_o0_t0_m0_l0, d_my_o0_t0_m0_l0, d_mz_o0_t0_m0_l0, d_my_o0_t6_m0_l0, d_mx_o0_t4_m0_l0, d_mz_o0_t6_m0_l0, d_mx_o0_t5_m0_l0, d_my_o0_t3_m0_l0, d_mx_o0_t7_m0_l0, d_mx_o0_t1_m0_l0, d_my_o0_t1_m0_l0, d_mz_o0_t1_m0_l0, d_my_o0_t5_m0_l0, d_mz_o0_t3_m0_l0, d_mx_o0_t8_m0_l0, d_mz_o0_t4_m0_l0, d_my_o0_t8_m0_l0, d_mx_o0_t2_m0_l0, d_my_o0_t2_m0_l0, d_mz_o0_t2_m0_l0, RHS_rho, RHS_mx, RHS_my, RHS_mz, m_mz_o0_t6_l0, m_mz_o0_t5_l0, m_mz_o0_t4_l0, m_mz_o0_t3_l0, m_mz_o0_t2_l0, m_mz_o0_t1_l0, m_mz_o0_t0_l0, m_my_o0_t8_l0, m_my_o0_t6_l0, m_my_o0_t5_l0, m_my_o0_t4_l0, m_my_o0_t3_l0, m_my_o0_t2_l0, m_my_o0_t1_l0, m_my_o0_t0_l0, m_mx_o0_t8_l0, m_mx_o0_t7_l0, m_mx_o0_t5_l0, m_mx_o0_t4_l0, m_mx_o0_t3_l0, m_mx_o0_t2_l0, m_mx_o0_t1_l0, m_mx_o0_t0_l0, n_x, n_y, n_z, interaction_index, mod_normal, m_n_var, m_tx_var, m_ty_var, m_tz_var, m_n, m_tx, m_ty, m_tz, rk1cn, rk1c, fluxAccwnp_3, fluxAccwnm_3, fluxAccwtx_3, fluxAccwty_3, fluxAccwtz_3;
	
		//Get the dimensions of the patch
		const hier::Index boxfirst = patch->getBox().lower();
		const hier::Index boxlast = patch->getBox().upper();
	
		//Get delta spaces into an array. dx, dy, dz.
		std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
		const double* dx  = patch_geom->getDx();
	
		//Auxiliary definitions
		int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
		int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
		int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;
		for(int k = 0; k < klast; k++) {
			for(int j = 0; j < jlast; j++) {
				for(int i = 0; i < ilast; i++) {
					if ((vector(FOV_3, i, j, k) > 0)) {
						RHS_Null = 0.0;
						vector(rk2Null, i, j, k) = RK3P2_(RHS_Null, Null_p, rk1Null, i, j, k, dx, simPlat_dt, ilast, jlast);
					}
					if ((vector(FOV_1, i, j, k) > 0) && (i + 2 < ilast && i - 2 >= 0 && j + 2 < jlast && j - 2 >= 0 && k + 2 < klast && k - 2 >= 0)) {
						d_mx_o0_t0_m0_l0 = centereddifferences4thorder_i(ad_mx_o0_t0_m0_l0_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						d_my_o0_t0_m0_l0 = centereddifferences4thordercrossed_ij(ad_mx_o0_t0_m0_l0_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						d_mz_o0_t0_m0_l0 = centereddifferences4thordercrossed_ik(ad_mx_o0_t0_m0_l0_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						d_my_o0_t6_m0_l0 = centereddifferences4thordercrossed_ji(ad_mx_o0_t0_m0_l0_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						d_mx_o0_t4_m0_l0 = centereddifferences4thorder_j(ad_mx_o0_t0_m0_l0_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						d_mz_o0_t6_m0_l0 = centereddifferences4thordercrossed_ki(ad_mx_o0_t0_m0_l0_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						d_mx_o0_t5_m0_l0 = centereddifferences4thorder_k(ad_mx_o0_t0_m0_l0_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						d_my_o0_t3_m0_l0 = centereddifferences4thorder_i(ad_my_o0_t3_m0_l0_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						d_mx_o0_t7_m0_l0 = centereddifferences4thordercrossed_ij(ad_my_o0_t3_m0_l0_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						d_mx_o0_t1_m0_l0 = centereddifferences4thordercrossed_ji(ad_my_o0_t3_m0_l0_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						d_my_o0_t1_m0_l0 = centereddifferences4thorder_j(ad_my_o0_t3_m0_l0_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						d_mz_o0_t1_m0_l0 = centereddifferences4thordercrossed_jk(ad_my_o0_t3_m0_l0_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						d_my_o0_t5_m0_l0 = centereddifferences4thorder_k(ad_my_o0_t3_m0_l0_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						d_mz_o0_t3_m0_l0 = centereddifferences4thorder_i(ad_mz_o0_t3_m0_l0_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						d_mx_o0_t8_m0_l0 = centereddifferences4thordercrossed_ik(ad_mz_o0_t3_m0_l0_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						d_mz_o0_t4_m0_l0 = centereddifferences4thorder_j(ad_mz_o0_t3_m0_l0_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						d_my_o0_t8_m0_l0 = centereddifferences4thordercrossed_jk(ad_mz_o0_t3_m0_l0_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						d_mx_o0_t2_m0_l0 = centereddifferences4thordercrossed_ki(ad_mz_o0_t3_m0_l0_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						d_my_o0_t2_m0_l0 = centereddifferences4thordercrossed_kj(ad_mz_o0_t3_m0_l0_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						d_mz_o0_t2_m0_l0 = centereddifferences4thorder_k(ad_mz_o0_t3_m0_l0_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						RHS_rho = 0.0;
						RHS_mx = vector(rk1rho, i, j, k) * vector(fx, i, j, k);
						RHS_my = vector(rk1rho, i, j, k) * vector(fy, i, j, k);
						RHS_mz = vector(rk1rho, i, j, k) * vector(fz, i, j, k);
						RHS_rho = RHS_rho - FDOC_i(extrapolatedSBrhoSPi, extrapolatedSBmxSPi, SpeedSBi_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						RHS_rho = RHS_rho - FDOC_j(extrapolatedSBrhoSPj, extrapolatedSBmySPj, SpeedSBj_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						RHS_rho = RHS_rho - FDOC_k(extrapolatedSBrhoSPk, extrapolatedSBmzSPk, SpeedSBk_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						RHS_mx = RHS_mx - FDOC_i(extrapolatedSBmxSPi, FluxSBimx_1, SpeedSBi_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						RHS_mx = RHS_mx - FDOC_j(extrapolatedSBmxSPj, FluxSBjmx_1, SpeedSBj_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						RHS_mx = RHS_mx - FDOC_k(extrapolatedSBmxSPk, FluxSBkmx_1, SpeedSBk_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						RHS_my = RHS_my - FDOC_i(extrapolatedSBmySPi, FluxSBimy_1, SpeedSBi_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						RHS_my = RHS_my - FDOC_j(extrapolatedSBmySPj, FluxSBjmy_1, SpeedSBj_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						RHS_my = RHS_my - FDOC_k(extrapolatedSBmySPk, FluxSBkmy_1, SpeedSBk_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						RHS_mz = RHS_mz - FDOC_i(extrapolatedSBmzSPi, FluxSBimz_1, SpeedSBi_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						RHS_mz = RHS_mz - FDOC_j(extrapolatedSBmzSPj, FluxSBjmz_1, SpeedSBj_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						RHS_mz = RHS_mz - FDOC_k(extrapolatedSBmzSPk, FluxSBkmz_1, SpeedSBk_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						m_mz_o0_t6_l0 = mu * d_mz_o0_t6_m0_l0;
						m_mz_o0_t5_l0 = mu * d_mz_o0_t2_m0_l0;
						m_mz_o0_t4_l0 = mu * d_mz_o0_t4_m0_l0;
						m_mz_o0_t3_l0 = mu * d_mz_o0_t3_m0_l0;
						m_mz_o0_t2_l0 = lambda * d_mz_o0_t2_m0_l0;
						m_mz_o0_t1_l0 = lambda * d_mz_o0_t1_m0_l0;
						m_mz_o0_t0_l0 = lambda * d_mz_o0_t0_m0_l0;
						m_my_o0_t8_l0 = mu * d_my_o0_t8_m0_l0;
						m_my_o0_t6_l0 = mu * d_my_o0_t6_m0_l0;
						m_my_o0_t5_l0 = mu * d_my_o0_t5_m0_l0;
						m_my_o0_t4_l0 = mu * d_my_o0_t1_m0_l0;
						m_my_o0_t3_l0 = mu * d_my_o0_t3_m0_l0;
						m_my_o0_t2_l0 = lambda * d_my_o0_t2_m0_l0;
						m_my_o0_t1_l0 = lambda * d_my_o0_t1_m0_l0;
						m_my_o0_t0_l0 = lambda * d_my_o0_t0_m0_l0;
						m_mx_o0_t8_l0 = mu * d_mx_o0_t8_m0_l0;
						m_mx_o0_t7_l0 = mu * d_mx_o0_t7_m0_l0;
						m_mx_o0_t5_l0 = mu * d_mx_o0_t5_m0_l0;
						m_mx_o0_t4_l0 = mu * d_mx_o0_t4_m0_l0;
						m_mx_o0_t3_l0 = mu * d_mx_o0_t0_m0_l0;
						m_mx_o0_t2_l0 = lambda * d_mx_o0_t2_m0_l0;
						m_mx_o0_t1_l0 = lambda * d_mx_o0_t1_m0_l0;
						m_mx_o0_t0_l0 = lambda * d_mx_o0_t0_m0_l0;
						RHS_mx = RHS_mx + ((((((((m_mx_o0_t0_l0 + m_mx_o0_t1_l0) + m_mx_o0_t2_l0) + m_mx_o0_t3_l0) + m_mx_o0_t4_l0) + m_mx_o0_t5_l0) + m_mx_o0_t3_l0) + m_mx_o0_t7_l0) + m_mx_o0_t8_l0);
						RHS_my = RHS_my + ((((((((m_my_o0_t0_l0 + m_my_o0_t1_l0) + m_my_o0_t2_l0) + m_my_o0_t3_l0) + m_my_o0_t4_l0) + m_my_o0_t5_l0) + m_my_o0_t6_l0) + m_my_o0_t4_l0) + m_my_o0_t8_l0);
						RHS_mz = RHS_mz + ((((((((m_mz_o0_t0_l0 + m_mz_o0_t1_l0) + m_mz_o0_t2_l0) + m_mz_o0_t3_l0) + m_mz_o0_t4_l0) + m_mz_o0_t5_l0) + m_mz_o0_t6_l0) + m_mx_o0_t7_l0) + m_mx_o0_t8_l0);
						n_x = 0.0;
						n_y = 0.0;
						n_z = 0.0;
						interaction_index = 0.0;
						if ((equalsEq(n_x, 0.0) && equalsEq(n_y, 0.0)) && equalsEq(n_z, 0.0)) {
							if (vector(FOV_3, i + 1, j, k) > 0.0) {
								interaction_index = 1.0;
								n_x = n_x + 1.0 / dx[0];
							}
							if (vector(FOV_3, i - 1, j, k) > 0.0) {
								interaction_index = 1.0;
								n_x = n_x - 1.0 / dx[0];
							}
							if (vector(FOV_3, i, j + 1, k) > 0.0) {
								interaction_index = 1.0;
								n_y = n_y + 1.0 / dx[1];
							}
							if (vector(FOV_3, i, j - 1, k) > 0.0) {
								interaction_index = 1.0;
								n_y = n_y - 1.0 / dx[1];
							}
							if (vector(FOV_3, i, j, k + 1) > 0.0) {
								interaction_index = 1.0;
								n_z = n_z + 1.0 / dx[2];
							}
							if (vector(FOV_3, i, j, k - 1) > 0.0) {
								interaction_index = 1.0;
								n_z = n_z - 1.0 / dx[2];
							}
						}
						if ((equalsEq(n_x, 0.0) && equalsEq(n_y, 0.0)) && equalsEq(n_z, 0.0)) {
							if (((((((((vector(FOV_3, i + 1, j + 1, k + 1) > 0.0) || (vector(FOV_3, i + 1, j + 1, k - 1) > 0.0)) || (vector(FOV_3, i + 1, j + 1, k) > 0.0)) || (vector(FOV_3, i + 1, j - 1, k + 1) > 0.0)) || (vector(FOV_3, i + 1, j - 1, k - 1) > 0.0)) || (vector(FOV_3, i + 1, j - 1, k) > 0.0)) || (vector(FOV_3, i + 1, j, k + 1) > 0.0)) || (vector(FOV_3, i + 1, j, k - 1) > 0.0)) || (vector(FOV_3, i + 1, j, k) > 0.0)) {
								interaction_index = 1.0;
								n_x = n_x + 1.0 / dx[0];
							}
							if (((((((((vector(FOV_3, i - 1, j + 1, k + 1) > 0.0) || (vector(FOV_3, i - 1, j + 1, k - 1) > 0.0)) || (vector(FOV_3, i - 1, j + 1, k) > 0.0)) || (vector(FOV_3, i - 1, j - 1, k + 1) > 0.0)) || (vector(FOV_3, i - 1, j - 1, k - 1) > 0.0)) || (vector(FOV_3, i - 1, j - 1, k) > 0.0)) || (vector(FOV_3, i - 1, j, k + 1) > 0.0)) || (vector(FOV_3, i - 1, j, k - 1) > 0.0)) || (vector(FOV_3, i - 1, j, k) > 0.0)) {
								interaction_index = 1.0;
								n_x = n_x - 1.0 / dx[0];
							}
							if (((((((((vector(FOV_3, i + 1, j + 1, k + 1) > 0.0) || (vector(FOV_3, i + 1, j + 1, k - 1) > 0.0)) || (vector(FOV_3, i + 1, j + 1, k) > 0.0)) || (vector(FOV_3, i - 1, j + 1, k + 1) > 0.0)) || (vector(FOV_3, i - 1, j + 1, k - 1) > 0.0)) || (vector(FOV_3, i - 1, j + 1, k) > 0.0)) || (vector(FOV_3, i, j + 1, k + 1) > 0.0)) || (vector(FOV_3, i, j + 1, k - 1) > 0.0)) || (vector(FOV_3, i, j + 1, k) > 0.0)) {
								interaction_index = 1.0;
								n_y = n_y + 1.0 / dx[1];
							}
							if (((((((((vector(FOV_3, i + 1, j - 1, k + 1) > 0.0) || (vector(FOV_3, i + 1, j - 1, k - 1) > 0.0)) || (vector(FOV_3, i + 1, j - 1, k) > 0.0)) || (vector(FOV_3, i - 1, j - 1, k + 1) > 0.0)) || (vector(FOV_3, i - 1, j - 1, k - 1) > 0.0)) || (vector(FOV_3, i - 1, j - 1, k) > 0.0)) || (vector(FOV_3, i, j - 1, k + 1) > 0.0)) || (vector(FOV_3, i, j - 1, k - 1) > 0.0)) || (vector(FOV_3, i, j - 1, k) > 0.0)) {
								interaction_index = 1.0;
								n_y = n_y - 1.0 / dx[1];
							}
							if (((((((((vector(FOV_3, i + 1, j + 1, k + 1) > 0.0) || (vector(FOV_3, i + 1, j - 1, k + 1) > 0.0)) || (vector(FOV_3, i + 1, j, k + 1) > 0.0)) || (vector(FOV_3, i - 1, j + 1, k + 1) > 0.0)) || (vector(FOV_3, i - 1, j - 1, k + 1) > 0.0)) || (vector(FOV_3, i - 1, j, k + 1) > 0.0)) || (vector(FOV_3, i, j + 1, k + 1) > 0.0)) || (vector(FOV_3, i, j - 1, k + 1) > 0.0)) || (vector(FOV_3, i, j, k + 1) > 0.0)) {
								interaction_index = 1.0;
								n_z = n_z + 1.0 / dx[2];
							}
							if (((((((((vector(FOV_3, i + 1, j + 1, k - 1) > 0.0) || (vector(FOV_3, i + 1, j - 1, k - 1) > 0.0)) || (vector(FOV_3, i + 1, j, k - 1) > 0.0)) || (vector(FOV_3, i - 1, j + 1, k - 1) > 0.0)) || (vector(FOV_3, i - 1, j - 1, k - 1) > 0.0)) || (vector(FOV_3, i - 1, j, k - 1) > 0.0)) || (vector(FOV_3, i, j + 1, k - 1) > 0.0)) || (vector(FOV_3, i, j - 1, k - 1) > 0.0)) || (vector(FOV_3, i, j, k - 1) > 0.0)) {
								interaction_index = 1.0;
								n_z = n_z - 1.0 / dx[2];
							}
						}
						if ((equalsEq(n_x, 0.0) && equalsEq(n_y, 0.0)) && equalsEq(n_z, 0.0)) {
							if (vector(FOV_xLower, i + 1, j, k) > 0.0) {
								interaction_index = 2.0;
								n_x = n_x + 1.0 / dx[0];
							}
							if (vector(FOV_xLower, i - 1, j, k) > 0.0) {
								interaction_index = 2.0;
								n_x = n_x - 1.0 / dx[0];
							}
							if (vector(FOV_xLower, i, j + 1, k) > 0.0) {
								interaction_index = 2.0;
								n_y = n_y + 1.0 / dx[1];
							}
							if (vector(FOV_xLower, i, j - 1, k) > 0.0) {
								interaction_index = 2.0;
								n_y = n_y - 1.0 / dx[1];
							}
							if (vector(FOV_xLower, i, j, k + 1) > 0.0) {
								interaction_index = 2.0;
								n_z = n_z + 1.0 / dx[2];
							}
							if (vector(FOV_xLower, i, j, k - 1) > 0.0) {
								interaction_index = 2.0;
								n_z = n_z - 1.0 / dx[2];
							}
						}
						if ((equalsEq(n_x, 0.0) && equalsEq(n_y, 0.0)) && equalsEq(n_z, 0.0)) {
							if (((((((((vector(FOV_xLower, i + 1, j + 1, k + 1) > 0.0) || (vector(FOV_xLower, i + 1, j + 1, k - 1) > 0.0)) || (vector(FOV_xLower, i + 1, j + 1, k) > 0.0)) || (vector(FOV_xLower, i + 1, j - 1, k + 1) > 0.0)) || (vector(FOV_xLower, i + 1, j - 1, k - 1) > 0.0)) || (vector(FOV_xLower, i + 1, j - 1, k) > 0.0)) || (vector(FOV_xLower, i + 1, j, k + 1) > 0.0)) || (vector(FOV_xLower, i + 1, j, k - 1) > 0.0)) || (vector(FOV_xLower, i + 1, j, k) > 0.0)) {
								interaction_index = 2.0;
								n_x = n_x + 1.0 / dx[0];
							}
							if (((((((((vector(FOV_xLower, i - 1, j + 1, k + 1) > 0.0) || (vector(FOV_xLower, i - 1, j + 1, k - 1) > 0.0)) || (vector(FOV_xLower, i - 1, j + 1, k) > 0.0)) || (vector(FOV_xLower, i - 1, j - 1, k + 1) > 0.0)) || (vector(FOV_xLower, i - 1, j - 1, k - 1) > 0.0)) || (vector(FOV_xLower, i - 1, j - 1, k) > 0.0)) || (vector(FOV_xLower, i - 1, j, k + 1) > 0.0)) || (vector(FOV_xLower, i - 1, j, k - 1) > 0.0)) || (vector(FOV_xLower, i - 1, j, k) > 0.0)) {
								interaction_index = 2.0;
								n_x = n_x - 1.0 / dx[0];
							}
							if (((((((((vector(FOV_xLower, i + 1, j + 1, k + 1) > 0.0) || (vector(FOV_xLower, i + 1, j + 1, k - 1) > 0.0)) || (vector(FOV_xLower, i + 1, j + 1, k) > 0.0)) || (vector(FOV_xLower, i - 1, j + 1, k + 1) > 0.0)) || (vector(FOV_xLower, i - 1, j + 1, k - 1) > 0.0)) || (vector(FOV_xLower, i - 1, j + 1, k) > 0.0)) || (vector(FOV_xLower, i, j + 1, k + 1) > 0.0)) || (vector(FOV_xLower, i, j + 1, k - 1) > 0.0)) || (vector(FOV_xLower, i, j + 1, k) > 0.0)) {
								interaction_index = 2.0;
								n_y = n_y + 1.0 / dx[1];
							}
							if (((((((((vector(FOV_xLower, i + 1, j - 1, k + 1) > 0.0) || (vector(FOV_xLower, i + 1, j - 1, k - 1) > 0.0)) || (vector(FOV_xLower, i + 1, j - 1, k) > 0.0)) || (vector(FOV_xLower, i - 1, j - 1, k + 1) > 0.0)) || (vector(FOV_xLower, i - 1, j - 1, k - 1) > 0.0)) || (vector(FOV_xLower, i - 1, j - 1, k) > 0.0)) || (vector(FOV_xLower, i, j - 1, k + 1) > 0.0)) || (vector(FOV_xLower, i, j - 1, k - 1) > 0.0)) || (vector(FOV_xLower, i, j - 1, k) > 0.0)) {
								interaction_index = 2.0;
								n_y = n_y - 1.0 / dx[1];
							}
							if (((((((((vector(FOV_xLower, i + 1, j + 1, k + 1) > 0.0) || (vector(FOV_xLower, i + 1, j - 1, k + 1) > 0.0)) || (vector(FOV_xLower, i + 1, j, k + 1) > 0.0)) || (vector(FOV_xLower, i - 1, j + 1, k + 1) > 0.0)) || (vector(FOV_xLower, i - 1, j - 1, k + 1) > 0.0)) || (vector(FOV_xLower, i - 1, j, k + 1) > 0.0)) || (vector(FOV_xLower, i, j + 1, k + 1) > 0.0)) || (vector(FOV_xLower, i, j - 1, k + 1) > 0.0)) || (vector(FOV_xLower, i, j, k + 1) > 0.0)) {
								interaction_index = 2.0;
								n_z = n_z + 1.0 / dx[2];
							}
							if (((((((((vector(FOV_xLower, i + 1, j + 1, k - 1) > 0.0) || (vector(FOV_xLower, i + 1, j - 1, k - 1) > 0.0)) || (vector(FOV_xLower, i + 1, j, k - 1) > 0.0)) || (vector(FOV_xLower, i - 1, j + 1, k - 1) > 0.0)) || (vector(FOV_xLower, i - 1, j - 1, k - 1) > 0.0)) || (vector(FOV_xLower, i - 1, j, k - 1) > 0.0)) || (vector(FOV_xLower, i, j + 1, k - 1) > 0.0)) || (vector(FOV_xLower, i, j - 1, k - 1) > 0.0)) || (vector(FOV_xLower, i, j, k - 1) > 0.0)) {
								interaction_index = 2.0;
								n_z = n_z - 1.0 / dx[2];
							}
						}
						if ((equalsEq(n_x, 0.0) && equalsEq(n_y, 0.0)) && equalsEq(n_z, 0.0)) {
							if (vector(FOV_xUpper, i + 1, j, k) > 0.0) {
								interaction_index = 3.0;
								n_x = n_x + 1.0 / dx[0];
							}
							if (vector(FOV_xUpper, i - 1, j, k) > 0.0) {
								interaction_index = 3.0;
								n_x = n_x - 1.0 / dx[0];
							}
							if (vector(FOV_xUpper, i, j + 1, k) > 0.0) {
								interaction_index = 3.0;
								n_y = n_y + 1.0 / dx[1];
							}
							if (vector(FOV_xUpper, i, j - 1, k) > 0.0) {
								interaction_index = 3.0;
								n_y = n_y - 1.0 / dx[1];
							}
							if (vector(FOV_xUpper, i, j, k + 1) > 0.0) {
								interaction_index = 3.0;
								n_z = n_z + 1.0 / dx[2];
							}
							if (vector(FOV_xUpper, i, j, k - 1) > 0.0) {
								interaction_index = 3.0;
								n_z = n_z - 1.0 / dx[2];
							}
						}
						if ((equalsEq(n_x, 0.0) && equalsEq(n_y, 0.0)) && equalsEq(n_z, 0.0)) {
							if (((((((((vector(FOV_xUpper, i + 1, j + 1, k + 1) > 0.0) || (vector(FOV_xUpper, i + 1, j + 1, k - 1) > 0.0)) || (vector(FOV_xUpper, i + 1, j + 1, k) > 0.0)) || (vector(FOV_xUpper, i + 1, j - 1, k + 1) > 0.0)) || (vector(FOV_xUpper, i + 1, j - 1, k - 1) > 0.0)) || (vector(FOV_xUpper, i + 1, j - 1, k) > 0.0)) || (vector(FOV_xUpper, i + 1, j, k + 1) > 0.0)) || (vector(FOV_xUpper, i + 1, j, k - 1) > 0.0)) || (vector(FOV_xUpper, i + 1, j, k) > 0.0)) {
								interaction_index = 3.0;
								n_x = n_x + 1.0 / dx[0];
							}
							if (((((((((vector(FOV_xUpper, i - 1, j + 1, k + 1) > 0.0) || (vector(FOV_xUpper, i - 1, j + 1, k - 1) > 0.0)) || (vector(FOV_xUpper, i - 1, j + 1, k) > 0.0)) || (vector(FOV_xUpper, i - 1, j - 1, k + 1) > 0.0)) || (vector(FOV_xUpper, i - 1, j - 1, k - 1) > 0.0)) || (vector(FOV_xUpper, i - 1, j - 1, k) > 0.0)) || (vector(FOV_xUpper, i - 1, j, k + 1) > 0.0)) || (vector(FOV_xUpper, i - 1, j, k - 1) > 0.0)) || (vector(FOV_xUpper, i - 1, j, k) > 0.0)) {
								interaction_index = 3.0;
								n_x = n_x - 1.0 / dx[0];
							}
							if (((((((((vector(FOV_xUpper, i + 1, j + 1, k + 1) > 0.0) || (vector(FOV_xUpper, i + 1, j + 1, k - 1) > 0.0)) || (vector(FOV_xUpper, i + 1, j + 1, k) > 0.0)) || (vector(FOV_xUpper, i - 1, j + 1, k + 1) > 0.0)) || (vector(FOV_xUpper, i - 1, j + 1, k - 1) > 0.0)) || (vector(FOV_xUpper, i - 1, j + 1, k) > 0.0)) || (vector(FOV_xUpper, i, j + 1, k + 1) > 0.0)) || (vector(FOV_xUpper, i, j + 1, k - 1) > 0.0)) || (vector(FOV_xUpper, i, j + 1, k) > 0.0)) {
								interaction_index = 3.0;
								n_y = n_y + 1.0 / dx[1];
							}
							if (((((((((vector(FOV_xUpper, i + 1, j - 1, k + 1) > 0.0) || (vector(FOV_xUpper, i + 1, j - 1, k - 1) > 0.0)) || (vector(FOV_xUpper, i + 1, j - 1, k) > 0.0)) || (vector(FOV_xUpper, i - 1, j - 1, k + 1) > 0.0)) || (vector(FOV_xUpper, i - 1, j - 1, k - 1) > 0.0)) || (vector(FOV_xUpper, i - 1, j - 1, k) > 0.0)) || (vector(FOV_xUpper, i, j - 1, k + 1) > 0.0)) || (vector(FOV_xUpper, i, j - 1, k - 1) > 0.0)) || (vector(FOV_xUpper, i, j - 1, k) > 0.0)) {
								interaction_index = 3.0;
								n_y = n_y - 1.0 / dx[1];
							}
							if (((((((((vector(FOV_xUpper, i + 1, j + 1, k + 1) > 0.0) || (vector(FOV_xUpper, i + 1, j - 1, k + 1) > 0.0)) || (vector(FOV_xUpper, i + 1, j, k + 1) > 0.0)) || (vector(FOV_xUpper, i - 1, j + 1, k + 1) > 0.0)) || (vector(FOV_xUpper, i - 1, j - 1, k + 1) > 0.0)) || (vector(FOV_xUpper, i - 1, j, k + 1) > 0.0)) || (vector(FOV_xUpper, i, j + 1, k + 1) > 0.0)) || (vector(FOV_xUpper, i, j - 1, k + 1) > 0.0)) || (vector(FOV_xUpper, i, j, k + 1) > 0.0)) {
								interaction_index = 3.0;
								n_z = n_z + 1.0 / dx[2];
							}
							if (((((((((vector(FOV_xUpper, i + 1, j + 1, k - 1) > 0.0) || (vector(FOV_xUpper, i + 1, j - 1, k - 1) > 0.0)) || (vector(FOV_xUpper, i + 1, j, k - 1) > 0.0)) || (vector(FOV_xUpper, i - 1, j + 1, k - 1) > 0.0)) || (vector(FOV_xUpper, i - 1, j - 1, k - 1) > 0.0)) || (vector(FOV_xUpper, i - 1, j, k - 1) > 0.0)) || (vector(FOV_xUpper, i, j + 1, k - 1) > 0.0)) || (vector(FOV_xUpper, i, j - 1, k - 1) > 0.0)) || (vector(FOV_xUpper, i, j, k - 1) > 0.0)) {
								interaction_index = 3.0;
								n_z = n_z - 1.0 / dx[2];
							}
						}
						if ((!equalsEq(n_x, 0.0) || !equalsEq(n_y, 0.0)) || !equalsEq(n_z, 0.0)) {
							mod_normal = sqrt((n_x * n_x + n_y * n_y) + n_z * n_z);
							n_x = n_x / mod_normal;
							n_y = n_y / mod_normal;
							n_z = n_z / mod_normal;
						}
						if (equalsEq(interaction_index, 1.0)) {
							m_n_var = RHS_mx * n_x + RHS_my * n_y + RHS_mz * n_z;
							m_tx_var = RHS_mx - (RHS_mx * n_x + RHS_my * n_y + RHS_mz * n_z) * n_x;
							m_ty_var = RHS_my - (RHS_mx * n_x + RHS_my * n_y + RHS_mz * n_z) * n_y;
							m_tz_var = RHS_mz - (RHS_mx * n_x + RHS_my * n_y + RHS_mz * n_z) * n_z;
							m_n = vector(rk1mx, i, j, k) * n_x + vector(rk1my, i, j, k) * n_y + vector(rk1mz, i, j, k) * n_z;
							m_tx = vector(rk1mx, i, j, k) - (vector(rk1mx, i, j, k) * n_x + vector(rk1my, i, j, k) * n_y + vector(rk1mz, i, j, k) * n_z) * n_x;
							m_ty = vector(rk1my, i, j, k) - (vector(rk1mx, i, j, k) * n_x + vector(rk1my, i, j, k) * n_y + vector(rk1mz, i, j, k) * n_z) * n_y;
							m_tz = vector(rk1mz, i, j, k) - (vector(rk1mx, i, j, k) * n_x + vector(rk1my, i, j, k) * n_y + vector(rk1mz, i, j, k) * n_z) * n_z;
							rk1cn = m_n / vector(rk1rho, i, j, k);
							rk1c = c0 * (pow((vector(rk1rho, i, j, k) / rho0), ((gamma + (-1.0)) / 2.0)));
							if ((rk1c + rk1cn) > 0.0) {
								fluxAccwnp_3 = RHS_rho * (-((-rk1c) + rk1cn)) + m_n_var;
							} else {
								fluxAccwnp_3 = (-m_n_var) + RHS_rho * (-((-rk1c) + rk1cn));
							}
							if (((-rk1c) + rk1cn) > 0.0) {
								fluxAccwnm_3 = RHS_rho * (-(rk1c + rk1cn)) + m_n_var;
							} else {
								fluxAccwnm_3 = (-m_n_var) + RHS_rho * (-(rk1c + rk1cn));
							}
							if (rk1cn > 0.0) {
								fluxAccwtx_3 = RHS_rho * (-m_tx) / vector(rk1rho, i, j, k) + m_tx_var;
							} else {
								fluxAccwtx_3 = RHS_rho * (-m_tx) / vector(rk1rho, i, j, k);
							}
							if (rk1cn > 0.0) {
								fluxAccwty_3 = RHS_rho * (-m_ty) / vector(rk1rho, i, j, k) + m_ty_var;
							} else {
								fluxAccwty_3 = RHS_rho * (-m_ty) / vector(rk1rho, i, j, k);
							}
							if (rk1cn > 0.0) {
								fluxAccwtz_3 = RHS_rho * (-m_tz) / vector(rk1rho, i, j, k) + m_tz_var;
							} else {
								fluxAccwtz_3 = RHS_rho * (-m_tz) / vector(rk1rho, i, j, k);
							}
							RHS_rho = fluxAccwnp_3 * 1.0 / (2.0 * rk1c) + fluxAccwnm_3 * (-1.0) / (2.0 * rk1c);
							m_n_var = fluxAccwnp_3 * (rk1cn / (2.0 * rk1c) + 1.0 / 2.0) + fluxAccwnm_3 * (-(rk1cn / (2.0 * rk1c) + (-1.0 / 2.0)));
							m_tx_var = (fluxAccwnp_3 * m_tx / (2.0 * vector(rk1rho, i, j, k) * rk1c) + fluxAccwnm_3 * (-m_tx) / (2.0 * vector(rk1rho, i, j, k) * rk1c)) + fluxAccwtx_3;
							m_ty_var = (fluxAccwnp_3 * m_ty / (2.0 * vector(rk1rho, i, j, k) * rk1c) + fluxAccwnm_3 * (-m_ty) / (2.0 * vector(rk1rho, i, j, k) * rk1c)) + fluxAccwty_3;
							m_tz_var = (fluxAccwnp_3 * m_tz / (2.0 * vector(rk1rho, i, j, k) * rk1c) + fluxAccwnm_3 * (-m_tz) / (2.0 * vector(rk1rho, i, j, k) * rk1c)) + fluxAccwtz_3;
							RHS_mx = m_tx_var + m_n_var * n_x;
							RHS_my = m_ty_var + m_n_var * n_y;
							RHS_mz = m_tz_var + m_n_var * n_z;
						}
						if (equalsEq(interaction_index, 2.0)) {
							m_n_var = RHS_mx * n_x + RHS_my * n_y + RHS_mz * n_z;
							m_tx_var = RHS_mx - (RHS_mx * n_x + RHS_my * n_y + RHS_mz * n_z) * n_x;
							m_ty_var = RHS_my - (RHS_mx * n_x + RHS_my * n_y + RHS_mz * n_z) * n_y;
							m_tz_var = RHS_mz - (RHS_mx * n_x + RHS_my * n_y + RHS_mz * n_z) * n_z;
							m_n = vector(rk1mx, i, j, k) * n_x + vector(rk1my, i, j, k) * n_y + vector(rk1mz, i, j, k) * n_z;
							m_tx = vector(rk1mx, i, j, k) - (vector(rk1mx, i, j, k) * n_x + vector(rk1my, i, j, k) * n_y + vector(rk1mz, i, j, k) * n_z) * n_x;
							m_ty = vector(rk1my, i, j, k) - (vector(rk1mx, i, j, k) * n_x + vector(rk1my, i, j, k) * n_y + vector(rk1mz, i, j, k) * n_z) * n_y;
							m_tz = vector(rk1mz, i, j, k) - (vector(rk1mx, i, j, k) * n_x + vector(rk1my, i, j, k) * n_y + vector(rk1mz, i, j, k) * n_z) * n_z;
							rk1cn = m_n / vector(rk1rho, i, j, k);
							rk1c = c0 * (pow((vector(rk1rho, i, j, k) / rho0), ((gamma + (-1.0)) / 2.0)));
							if ((rk1c + rk1cn) > 0.0) {
								fluxAccwnp_3 = RHS_rho * (-((-rk1c) + rk1cn)) + m_n_var;
							} else {
								fluxAccwnp_3 = (-lambdain) * (m_n + vector(rk1rho, i, j, k) * v0 * (1.0 - (ycoord(j) * ycoord(j) + zcoord(k) * zcoord(k)) / ((floor((0.006 * fabs(ycoord(j))) / (sqrt(ycoord(j) * ycoord(j) + zcoord(k) * zcoord(k)) * dx[1]) + 0.5) * dx[1]) * (floor((0.006 * fabs(ycoord(j))) / (sqrt(ycoord(j) * ycoord(j) + zcoord(k) * zcoord(k)) * dx[1]) + 0.5) * dx[1]) + (floor((0.006 * fabs(zcoord(k))) / (sqrt(ycoord(j) * ycoord(j) + zcoord(k) * zcoord(k)) * dx[2]) + 0.5) * dx[2]) * (floor((0.006 * fabs(zcoord(k))) / (sqrt(ycoord(j) * ycoord(j) + zcoord(k) * zcoord(k)) * dx[2]) + 0.5) * dx[2]))));
							}
							if (((-rk1c) + rk1cn) > 0.0) {
								fluxAccwnm_3 = RHS_rho * (-(rk1c + rk1cn)) + m_n_var;
							} else {
								fluxAccwnm_3 = (-lambdain) * (m_n + vector(rk1rho, i, j, k) * v0 * (1.0 - (ycoord(j) * ycoord(j) + zcoord(k) * zcoord(k)) / ((floor((0.006 * fabs(ycoord(j))) / (sqrt(ycoord(j) * ycoord(j) + zcoord(k) * zcoord(k)) * dx[1]) + 0.5) * dx[1]) * (floor((0.006 * fabs(ycoord(j))) / (sqrt(ycoord(j) * ycoord(j) + zcoord(k) * zcoord(k)) * dx[1]) + 0.5) * dx[1]) + (floor((0.006 * fabs(zcoord(k))) / (sqrt(ycoord(j) * ycoord(j) + zcoord(k) * zcoord(k)) * dx[2]) + 0.5) * dx[2]) * (floor((0.006 * fabs(zcoord(k))) / (sqrt(ycoord(j) * ycoord(j) + zcoord(k) * zcoord(k)) * dx[2]) + 0.5) * dx[2]))));
							}
							if (rk1cn > 0.0) {
								fluxAccwtx_3 = RHS_rho * (-m_tx) / vector(rk1rho, i, j, k) + m_tx_var;
							} else {
								fluxAccwtx_3 = (-lambdain) * (m_tx - vector(rk1rho, i, j, k) * vt0);
							}
							if (rk1cn > 0.0) {
								fluxAccwty_3 = RHS_rho * (-m_ty) / vector(rk1rho, i, j, k) + m_ty_var;
							} else {
								fluxAccwty_3 = (-lambdain) * (m_ty - vector(rk1rho, i, j, k) * vt0);
							}
							if (rk1cn > 0.0) {
								fluxAccwtz_3 = RHS_rho * (-m_tz) / vector(rk1rho, i, j, k) + m_tz_var;
							} else {
								fluxAccwtz_3 = (-lambdain) * (m_tz - vector(rk1rho, i, j, k) * vt0);
							}
							RHS_rho = fluxAccwnp_3 * 1.0 / (2.0 * rk1c) + fluxAccwnm_3 * (-1.0) / (2.0 * rk1c);
							m_n_var = fluxAccwnp_3 * (rk1cn / (2.0 * rk1c) + 1.0 / 2.0) + fluxAccwnm_3 * (-(rk1cn / (2.0 * rk1c) + (-1.0 / 2.0)));
							m_tx_var = (fluxAccwnp_3 * m_tx / (2.0 * vector(rk1rho, i, j, k) * rk1c) + fluxAccwnm_3 * (-m_tx) / (2.0 * vector(rk1rho, i, j, k) * rk1c)) + fluxAccwtx_3;
							m_ty_var = (fluxAccwnp_3 * m_ty / (2.0 * vector(rk1rho, i, j, k) * rk1c) + fluxAccwnm_3 * (-m_ty) / (2.0 * vector(rk1rho, i, j, k) * rk1c)) + fluxAccwty_3;
							m_tz_var = (fluxAccwnp_3 * m_tz / (2.0 * vector(rk1rho, i, j, k) * rk1c) + fluxAccwnm_3 * (-m_tz) / (2.0 * vector(rk1rho, i, j, k) * rk1c)) + fluxAccwtz_3;
							RHS_mx = m_tx_var + m_n_var * n_x;
							RHS_my = m_ty_var + m_n_var * n_y;
							RHS_mz = m_tz_var + m_n_var * n_z;
						}
						if (equalsEq(interaction_index, 3.0)) {
							m_n_var = RHS_mx * n_x + RHS_my * n_y + RHS_mz * n_z;
							m_tx_var = RHS_mx - (RHS_mx * n_x + RHS_my * n_y + RHS_mz * n_z) * n_x;
							m_ty_var = RHS_my - (RHS_mx * n_x + RHS_my * n_y + RHS_mz * n_z) * n_y;
							m_tz_var = RHS_mz - (RHS_mx * n_x + RHS_my * n_y + RHS_mz * n_z) * n_z;
							m_n = vector(rk1mx, i, j, k) * n_x + vector(rk1my, i, j, k) * n_y + vector(rk1mz, i, j, k) * n_z;
							m_tx = vector(rk1mx, i, j, k) - (vector(rk1mx, i, j, k) * n_x + vector(rk1my, i, j, k) * n_y + vector(rk1mz, i, j, k) * n_z) * n_x;
							m_ty = vector(rk1my, i, j, k) - (vector(rk1mx, i, j, k) * n_x + vector(rk1my, i, j, k) * n_y + vector(rk1mz, i, j, k) * n_z) * n_y;
							m_tz = vector(rk1mz, i, j, k) - (vector(rk1mx, i, j, k) * n_x + vector(rk1my, i, j, k) * n_y + vector(rk1mz, i, j, k) * n_z) * n_z;
							rk1cn = m_n / vector(rk1rho, i, j, k);
							rk1c = c0 * (pow((vector(rk1rho, i, j, k) / rho0), ((gamma + (-1.0)) / 2.0)));
							if ((rk1c + rk1cn) > 0.0) {
								fluxAccwnp_3 = RHS_rho * (-((-rk1c) + rk1cn)) + m_n_var;
							} else {
								fluxAccwnp_3 = ((-rk1c) + rk1cn) * lambdaout * (vector(rk1rho, i, j, k) - rho0);
							}
							if (((-rk1c) + rk1cn) > 0.0) {
								fluxAccwnm_3 = RHS_rho * (-(rk1c + rk1cn)) + m_n_var;
							} else {
								fluxAccwnm_3 = (rk1c + rk1cn) * lambdaout * (vector(rk1rho, i, j, k) - rho0);
							}
							if (rk1cn > 0.0) {
								fluxAccwtx_3 = RHS_rho * (-m_tx) / vector(rk1rho, i, j, k) + m_tx_var;
							} else {
								fluxAccwtx_3 = m_tx / vector(rk1rho, i, j, k) * lambdaout * (vector(rk1rho, i, j, k) - rho0);
							}
							if (rk1cn > 0.0) {
								fluxAccwty_3 = RHS_rho * (-m_ty) / vector(rk1rho, i, j, k) + m_ty_var;
							} else {
								fluxAccwty_3 = m_ty / vector(rk1rho, i, j, k) * lambdaout * (vector(rk1rho, i, j, k) - rho0);
							}
							if (rk1cn > 0.0) {
								fluxAccwtz_3 = RHS_rho * (-m_tz) / vector(rk1rho, i, j, k) + m_tz_var;
							} else {
								fluxAccwtz_3 = m_tz / vector(rk1rho, i, j, k) * lambdaout * (vector(rk1rho, i, j, k) - rho0);
							}
							RHS_rho = fluxAccwnp_3 * 1.0 / (2.0 * rk1c) + fluxAccwnm_3 * (-1.0) / (2.0 * rk1c);
							m_n_var = fluxAccwnp_3 * (rk1cn / (2.0 * rk1c) + 1.0 / 2.0) + fluxAccwnm_3 * (-(rk1cn / (2.0 * rk1c) + (-1.0 / 2.0)));
							m_tx_var = (fluxAccwnp_3 * m_tx / (2.0 * vector(rk1rho, i, j, k) * rk1c) + fluxAccwnm_3 * (-m_tx) / (2.0 * vector(rk1rho, i, j, k) * rk1c)) + fluxAccwtx_3;
							m_ty_var = (fluxAccwnp_3 * m_ty / (2.0 * vector(rk1rho, i, j, k) * rk1c) + fluxAccwnm_3 * (-m_ty) / (2.0 * vector(rk1rho, i, j, k) * rk1c)) + fluxAccwty_3;
							m_tz_var = (fluxAccwnp_3 * m_tz / (2.0 * vector(rk1rho, i, j, k) * rk1c) + fluxAccwnm_3 * (-m_tz) / (2.0 * vector(rk1rho, i, j, k) * rk1c)) + fluxAccwtz_3;
							RHS_mx = m_tx_var + m_n_var * n_x;
							RHS_my = m_ty_var + m_n_var * n_y;
							RHS_mz = m_tz_var + m_n_var * n_z;
						}
						vector(rk2rho, i, j, k) = RK3P2_(RHS_rho, rho_p, rk1rho, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(rk2mx, i, j, k) = RK3P2_(RHS_mx, mx_p, rk1mx, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(rk2my, i, j, k) = RK3P2_(RHS_my, my_p, rk1my, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(rk2mz, i, j, k) = RK3P2_(RHS_mz, mz_p, rk1mz, i, j, k, dx, simPlat_dt, ilast, jlast);
					}
				}
			}
		}
	}
	//Fill ghosts and periodical boundaries
	time_interpolate_operator_mesh1->setStep(2);
	d_bdry_sched_advance11[ln]->fillData(current_time + simPlat_dt * 0.5, false);
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch >& patch = *p_it;
		double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_3 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_3_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
		double* FOV_zLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zLower_id).get())->getPointer();
		double* FOV_zUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zUpper_id).get())->getPointer();
	
		double* fz = ((pdat::NodeData<double> *) patch->getPatchData(d_fz_id).get())->getPointer();
		double* fy = ((pdat::NodeData<double> *) patch->getPatchData(d_fy_id).get())->getPointer();
		double* fx = ((pdat::NodeData<double> *) patch->getPatchData(d_fx_id).get())->getPointer();
		double* p = ((pdat::NodeData<double> *) patch->getPatchData(d_p_id).get())->getPointer();
		double* rk2rho = ((pdat::NodeData<double> *) patch->getPatchData(d_rk2rho_id).get())->getPointer();
	
		//Get the dimensions of the patch
		const hier::Index boxfirst = patch->getBox().lower();
		const hier::Index boxlast = patch->getBox().upper();
	
		//Get delta spaces into an array. dx, dy, dz.
		std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
		const double* dx  = patch_geom->getDx();
	
		//Auxiliary definitions
		int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
		int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
		int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;
		for(int k = 0; k < klast; k++) {
			for(int j = 0; j < jlast; j++) {
				for(int i = 0; i < ilast; i++) {
					if ((vector(FOV_1, i, j, k) > 0) && ((i + 2 < ilast || !patch->getPatchGeometry()->getTouchesRegularBoundary(0, 1)) && (i - 2 >= 0 || !patch->getPatchGeometry()->getTouchesRegularBoundary(0, 0)) && (j + 2 < jlast || !patch->getPatchGeometry()->getTouchesRegularBoundary(1, 1)) && (j - 2 >= 0 || !patch->getPatchGeometry()->getTouchesRegularBoundary(1, 0)) && (k + 2 < klast || !patch->getPatchGeometry()->getTouchesRegularBoundary(2, 1)) && (k - 2 >= 0 || !patch->getPatchGeometry()->getTouchesRegularBoundary(2, 0)))) {
						vector(fz, i, j, k) = pfz;
						vector(fy, i, j, k) = pfy;
						vector(fx, i, j, k) = pfx;
						vector(p, i, j, k) = (Paux + (pow((vector(rk2rho, i, j, k) / rho0), gamma)) * (rho0 * (c0 * c0)) / gamma) - (rho0 * (c0 * c0)) / gamma;
					}
				}
			}
		}
	}
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch >& patch = *p_it;
		double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_3 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_3_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
		double* FOV_zLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zLower_id).get())->getPointer();
		double* FOV_zUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zUpper_id).get())->getPointer();
	
		//Hard region field distance variables
		double* d_i_rho_mx_my_mz_p_fx_fy_fz = ((pdat::NodeData<double> *) patch->getPatchData(d_d_i_rho_mx_my_mz_p_fx_fy_fz_id).get())->getPointer();
		double* d_j_rho_mx_my_mz_p_fx_fy_fz = ((pdat::NodeData<double> *) patch->getPatchData(d_d_j_rho_mx_my_mz_p_fx_fy_fz_id).get())->getPointer();
		double* d_k_rho_mx_my_mz_p_fx_fy_fz = ((pdat::NodeData<double> *) patch->getPatchData(d_d_k_rho_mx_my_mz_p_fx_fy_fz_id).get())->getPointer();
		double* d_i_Null = ((pdat::NodeData<double> *) patch->getPatchData(d_d_i_Null_id).get())->getPointer();
		double* d_j_Null = ((pdat::NodeData<double> *) patch->getPatchData(d_d_j_Null_id).get())->getPointer();
		double* d_k_Null = ((pdat::NodeData<double> *) patch->getPatchData(d_d_k_Null_id).get())->getPointer();
	
		double* rk2Null = ((pdat::NodeData<double> *) patch->getPatchData(d_rk2Null_id).get())->getPointer();
		double* extrapolatedSBrhoSPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBrhoSPi_id).get())->getPointer();
		double* extrapolatedSBrhoSPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBrhoSPj_id).get())->getPointer();
		double* extrapolatedSBrhoSPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBrhoSPk_id).get())->getPointer();
		double* rk2rho = ((pdat::NodeData<double> *) patch->getPatchData(d_rk2rho_id).get())->getPointer();
		double* extrapolatedSBmxSPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmxSPi_id).get())->getPointer();
		double* extrapolatedSBmxSPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmxSPj_id).get())->getPointer();
		double* extrapolatedSBmxSPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmxSPk_id).get())->getPointer();
		double* rk2mx = ((pdat::NodeData<double> *) patch->getPatchData(d_rk2mx_id).get())->getPointer();
		double* extrapolatedSBmySPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmySPi_id).get())->getPointer();
		double* extrapolatedSBmySPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmySPj_id).get())->getPointer();
		double* extrapolatedSBmySPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmySPk_id).get())->getPointer();
		double* rk2my = ((pdat::NodeData<double> *) patch->getPatchData(d_rk2my_id).get())->getPointer();
		double* extrapolatedSBmzSPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmzSPi_id).get())->getPointer();
		double* extrapolatedSBmzSPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmzSPj_id).get())->getPointer();
		double* extrapolatedSBmzSPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmzSPk_id).get())->getPointer();
		double* rk2mz = ((pdat::NodeData<double> *) patch->getPatchData(d_rk2mz_id).get())->getPointer();
		double* extrapolatedSBpSPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBpSPi_id).get())->getPointer();
		double* extrapolatedSBpSPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBpSPj_id).get())->getPointer();
		double* extrapolatedSBpSPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBpSPk_id).get())->getPointer();
		double* p = ((pdat::NodeData<double> *) patch->getPatchData(d_p_id).get())->getPointer();
		double* extrapolatedSBfxSPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBfxSPi_id).get())->getPointer();
		double* extrapolatedSBfxSPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBfxSPj_id).get())->getPointer();
		double* extrapolatedSBfxSPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBfxSPk_id).get())->getPointer();
		double* fx = ((pdat::NodeData<double> *) patch->getPatchData(d_fx_id).get())->getPointer();
		double* extrapolatedSBfySPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBfySPi_id).get())->getPointer();
		double* extrapolatedSBfySPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBfySPj_id).get())->getPointer();
		double* extrapolatedSBfySPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBfySPk_id).get())->getPointer();
		double* fy = ((pdat::NodeData<double> *) patch->getPatchData(d_fy_id).get())->getPointer();
		double* extrapolatedSBfzSPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBfzSPi_id).get())->getPointer();
		double* extrapolatedSBfzSPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBfzSPj_id).get())->getPointer();
		double* extrapolatedSBfzSPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBfzSPk_id).get())->getPointer();
		double* fz = ((pdat::NodeData<double> *) patch->getPatchData(d_fz_id).get())->getPointer();
		double* extrapolatedSBNullSPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBNullSPi_id).get())->getPointer();
		double* extrapolatedSBNullSPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBNullSPj_id).get())->getPointer();
		double* extrapolatedSBNullSPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBNullSPk_id).get())->getPointer();
	
		//Get the dimensions of the patch
		const hier::Index boxfirst = patch->getBox().lower();
		const hier::Index boxlast = patch->getBox().upper();
	
		//Get delta spaces into an array. dx, dy, dz.
		std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
		const double* dx  = patch_geom->getDx();
	
		//Auxiliary definitions
		int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
		int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
		int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;
		for(int k = 0; k < klast; k++) {
			for(int j = 0; j < jlast; j++) {
				for(int i = 0; i < ilast; i++) {
						vector(extrapolatedSBrhoSPi, i, j, k) = vector(rk2rho, i, j, k);
						vector(extrapolatedSBrhoSPj, i, j, k) = vector(rk2rho, i, j, k);
						vector(extrapolatedSBrhoSPk, i, j, k) = vector(rk2rho, i, j, k);
						vector(extrapolatedSBmxSPi, i, j, k) = vector(rk2mx, i, j, k);
						vector(extrapolatedSBmxSPj, i, j, k) = vector(rk2mx, i, j, k);
						vector(extrapolatedSBmxSPk, i, j, k) = vector(rk2mx, i, j, k);
						vector(extrapolatedSBmySPi, i, j, k) = vector(rk2my, i, j, k);
						vector(extrapolatedSBmySPj, i, j, k) = vector(rk2my, i, j, k);
						vector(extrapolatedSBmySPk, i, j, k) = vector(rk2my, i, j, k);
						vector(extrapolatedSBmzSPi, i, j, k) = vector(rk2mz, i, j, k);
						vector(extrapolatedSBmzSPj, i, j, k) = vector(rk2mz, i, j, k);
						vector(extrapolatedSBmzSPk, i, j, k) = vector(rk2mz, i, j, k);
						vector(extrapolatedSBNullSPi, i, j, k) = vector(rk2Null, i, j, k);
						vector(extrapolatedSBNullSPj, i, j, k) = vector(rk2Null, i, j, k);
						vector(extrapolatedSBNullSPk, i, j, k) = vector(rk2Null, i, j, k);
						vector(extrapolatedSBpSPi, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBpSPj, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBpSPk, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBfxSPi, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfxSPj, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfxSPk, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfySPi, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfySPj, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfySPk, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfzSPi, i, j, k) = vector(fz, i, j, k);
						vector(extrapolatedSBfzSPj, i, j, k) = vector(fz, i, j, k);
						vector(extrapolatedSBfzSPk, i, j, k) = vector(fz, i, j, k);
				}
			}
		}
		for(int k = 0; k < klast; k++) {
			for(int j = 0; j < jlast; j++) {
				for(int i = 0; i < ilast; i++) {
					if ((vector(FOV_xLower, i, j, k) > 0) && ((i + 1 < ilast && (vector(FOV_3,i + 1, j, k) > 0)) || (i + 2 < ilast && (vector(FOV_3,i + 2, j, k) > 0)))) {
						vector(rk2Null, i, j, k) = 0.0;
					}
					if ((vector(FOV_xUpper, i, j, k) > 0) && ((i - 1 >= 0 && (vector(FOV_3, i - 1, j, k) > 0)) || (i - 2 >= 0 && (vector(FOV_3, i - 2, j, k) > 0)))) {
						vector(rk2Null, i, j, k) = 0.0;
					}
					if ((vector(FOV_yLower, i, j, k) > 0) && ((j + 1 < jlast && (vector(FOV_3,i, j + 1, k) > 0)) || (j + 2 < jlast && (vector(FOV_3,i, j + 2, k) > 0)))) {
						vector(rk2Null, i, j, k) = 0.0;
					}
					if ((vector(FOV_yUpper, i, j, k) > 0) && ((j - 1 >= 0 && (vector(FOV_3, i, j - 1, k) > 0)) || (j - 2 >= 0 && (vector(FOV_3, i, j - 2, k) > 0)))) {
						vector(rk2Null, i, j, k) = 0.0;
					}
					if ((vector(FOV_zLower, i, j, k) > 0) && ((k + 1 < klast && (vector(FOV_3,i, j, k + 1) > 0)) || (k + 2 < klast && (vector(FOV_3,i, j, k + 2) > 0)))) {
						vector(rk2Null, i, j, k) = 0.0;
					}
					if ((vector(FOV_zUpper, i, j, k) > 0) && ((k - 1 >= 0 && (vector(FOV_3, i, j, k - 1) > 0)) || (k - 2 >= 0 && (vector(FOV_3, i, j, k - 2) > 0)))) {
						vector(rk2Null, i, j, k) = 0.0;
					}
					if (vector(FOV_3, i, j, k) > 0) {
						//Region field extrapolations
						if ((vector(d_i_rho_mx_my_mz_p_fx_fy_fz, i, j, k) != 0 || vector(d_j_rho_mx_my_mz_p_fx_fy_fz, i, j, k) != 0 || vector(d_k_rho_mx_my_mz_p_fx_fy_fz, i, j, k) != 0)) {
							extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBrhoSPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBrhoSPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBrhoSPk, k, rk2rho, FOV_1, dx, ilast, jlast);
							extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmxSPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmxSPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmxSPk, k, rk2mx, FOV_1, dx, ilast, jlast);
							extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmySPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmySPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmySPk, k, rk2my, FOV_1, dx, ilast, jlast);
							extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmzSPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmzSPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmzSPk, k, rk2mz, FOV_1, dx, ilast, jlast);
							extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBpSPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBpSPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBpSPk, k, p, FOV_1, dx, ilast, jlast);
							extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfxSPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfxSPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfxSPk, k, fx, FOV_1, dx, ilast, jlast);
							extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfySPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfySPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfySPk, k, fy, FOV_1, dx, ilast, jlast);
							extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfzSPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfzSPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfzSPk, k, fz, FOV_1, dx, ilast, jlast);
						}
					}
					if ((vector(FOV_xLower, i, j, k) > 0) && ((i + 1 < ilast && (vector(FOV_1,i + 1, j, k) > 0)) || (i + 2 < ilast && (vector(FOV_1,i + 2, j, k) > 0)) || ((i + 1 < ilast && k + 1 < klast && (vector(FOV_1, i + 1, j, k + 1) > 0)) || (i + 1 < ilast && k - 1 >= 0 && (vector(FOV_1, i + 1, j, k - 1) > 0)) || (i + 1 < ilast && k + 2 < klast && (vector(FOV_1, i + 1, j, k + 2) > 0)) || (i + 1 < ilast && k - 2 >= 0 && (vector(FOV_1, i + 1, j, k - 2) > 0)) || (i + 1 < ilast && j + 1 < jlast && (vector(FOV_1, i + 1, j + 1, k) > 0)) || (i + 1 < ilast && j + 1 < jlast && k + 1 < klast && (vector(FOV_1, i + 1, j + 1, k + 1) > 0)) || (i + 1 < ilast && j + 1 < jlast && k - 1 >= 0 && (vector(FOV_1, i + 1, j + 1, k - 1) > 0)) || (i + 1 < ilast && j + 1 < jlast && k + 2 < klast && (vector(FOV_1, i + 1, j + 1, k + 2) > 0)) || (i + 1 < ilast && j + 1 < jlast && k - 2 >= 0 && (vector(FOV_1, i + 1, j + 1, k - 2) > 0)) || (i + 1 < ilast && j - 1 >= 0 && (vector(FOV_1, i + 1, j - 1, k) > 0)) || (i + 1 < ilast && j - 1 >= 0 && k + 1 < klast && (vector(FOV_1, i + 1, j - 1, k + 1) > 0)) || (i + 1 < ilast && j - 1 >= 0 && k - 1 >= 0 && (vector(FOV_1, i + 1, j - 1, k - 1) > 0)) || (i + 1 < ilast && j - 1 >= 0 && k + 2 < klast && (vector(FOV_1, i + 1, j - 1, k + 2) > 0)) || (i + 1 < ilast && j - 1 >= 0 && k - 2 >= 0 && (vector(FOV_1, i + 1, j - 1, k - 2) > 0)) || (i + 1 < ilast && j + 2 < jlast && (vector(FOV_1, i + 1, j + 2, k) > 0)) || (i + 1 < ilast && j + 2 < jlast && k + 1 < klast && (vector(FOV_1, i + 1, j + 2, k + 1) > 0)) || (i + 1 < ilast && j + 2 < jlast && k - 1 >= 0 && (vector(FOV_1, i + 1, j + 2, k - 1) > 0)) || (i + 1 < ilast && j + 2 < jlast && k + 2 < klast && (vector(FOV_1, i + 1, j + 2, k + 2) > 0)) || (i + 1 < ilast && j + 2 < jlast && k - 2 >= 0 && (vector(FOV_1, i + 1, j + 2, k - 2) > 0)) || (i + 1 < ilast && j - 2 >= 0 && (vector(FOV_1, i + 1, j - 2, k) > 0)) || (i + 1 < ilast && j - 2 >= 0 && k + 1 < klast && (vector(FOV_1, i + 1, j - 2, k + 1) > 0)) || (i + 1 < ilast && j - 2 >= 0 && k - 1 >= 0 && (vector(FOV_1, i + 1, j - 2, k - 1) > 0)) || (i + 1 < ilast && j - 2 >= 0 && k + 2 < klast && (vector(FOV_1, i + 1, j - 2, k + 2) > 0)) || (i + 1 < ilast && j - 2 >= 0 && k - 2 >= 0 && (vector(FOV_1, i + 1, j - 2, k - 2) > 0)) || (i + 2 < ilast && k + 1 < klast && (vector(FOV_1, i + 2, j, k + 1) > 0)) || (i + 2 < ilast && k - 1 >= 0 && (vector(FOV_1, i + 2, j, k - 1) > 0)) || (i + 2 < ilast && k + 2 < klast && (vector(FOV_1, i + 2, j, k + 2) > 0)) || (i + 2 < ilast && k - 2 >= 0 && (vector(FOV_1, i + 2, j, k - 2) > 0)) || (i + 2 < ilast && j + 1 < jlast && (vector(FOV_1, i + 2, j + 1, k) > 0)) || (i + 2 < ilast && j + 1 < jlast && k + 1 < klast && (vector(FOV_1, i + 2, j + 1, k + 1) > 0)) || (i + 2 < ilast && j + 1 < jlast && k - 1 >= 0 && (vector(FOV_1, i + 2, j + 1, k - 1) > 0)) || (i + 2 < ilast && j + 1 < jlast && k + 2 < klast && (vector(FOV_1, i + 2, j + 1, k + 2) > 0)) || (i + 2 < ilast && j + 1 < jlast && k - 2 >= 0 && (vector(FOV_1, i + 2, j + 1, k - 2) > 0)) || (i + 2 < ilast && j - 1 >= 0 && (vector(FOV_1, i + 2, j - 1, k) > 0)) || (i + 2 < ilast && j - 1 >= 0 && k + 1 < klast && (vector(FOV_1, i + 2, j - 1, k + 1) > 0)) || (i + 2 < ilast && j - 1 >= 0 && k - 1 >= 0 && (vector(FOV_1, i + 2, j - 1, k - 1) > 0)) || (i + 2 < ilast && j - 1 >= 0 && k + 2 < klast && (vector(FOV_1, i + 2, j - 1, k + 2) > 0)) || (i + 2 < ilast && j - 1 >= 0 && k - 2 >= 0 && (vector(FOV_1, i + 2, j - 1, k - 2) > 0)) || (i + 2 < ilast && j + 2 < jlast && (vector(FOV_1, i + 2, j + 2, k) > 0)) || (i + 2 < ilast && j + 2 < jlast && k + 1 < klast && (vector(FOV_1, i + 2, j + 2, k + 1) > 0)) || (i + 2 < ilast && j + 2 < jlast && k - 1 >= 0 && (vector(FOV_1, i + 2, j + 2, k - 1) > 0)) || (i + 2 < ilast && j + 2 < jlast && k + 2 < klast && (vector(FOV_1, i + 2, j + 2, k + 2) > 0)) || (i + 2 < ilast && j + 2 < jlast && k - 2 >= 0 && (vector(FOV_1, i + 2, j + 2, k - 2) > 0)) || (i + 2 < ilast && j - 2 >= 0 && (vector(FOV_1, i + 2, j - 2, k) > 0)) || (i + 2 < ilast && j - 2 >= 0 && k + 1 < klast && (vector(FOV_1, i + 2, j - 2, k + 1) > 0)) || (i + 2 < ilast && j - 2 >= 0 && k - 1 >= 0 && (vector(FOV_1, i + 2, j - 2, k - 1) > 0)) || (i + 2 < ilast && j - 2 >= 0 && k + 2 < klast && (vector(FOV_1, i + 2, j - 2, k + 2) > 0)) || (i + 2 < ilast && j - 2 >= 0 && k - 2 >= 0 && (vector(FOV_1, i + 2, j - 2, k - 2) > 0))))) {
						//Region field extrapolations
						if ((vector(d_i_rho_mx_my_mz_p_fx_fy_fz, i, j, k) != 0 || vector(d_j_rho_mx_my_mz_p_fx_fy_fz, i, j, k) != 0 || vector(d_k_rho_mx_my_mz_p_fx_fy_fz, i, j, k) != 0)) {
							extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBrhoSPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBrhoSPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBrhoSPk, k, rk2rho, FOV_1, dx, ilast, jlast);
							extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmxSPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmxSPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmxSPk, k, rk2mx, FOV_1, dx, ilast, jlast);
							extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmySPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmySPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmySPk, k, rk2my, FOV_1, dx, ilast, jlast);
							extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmzSPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmzSPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmzSPk, k, rk2mz, FOV_1, dx, ilast, jlast);
							extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBpSPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBpSPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBpSPk, k, p, FOV_1, dx, ilast, jlast);
							extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfxSPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfxSPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfxSPk, k, fx, FOV_1, dx, ilast, jlast);
							extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfySPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfySPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfySPk, k, fy, FOV_1, dx, ilast, jlast);
							extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfzSPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfzSPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfzSPk, k, fz, FOV_1, dx, ilast, jlast);
						}
					}
					if ((vector(FOV_xUpper, i, j, k) > 0) && ((i - 1 >= 0 && (vector(FOV_1, i - 1, j, k) > 0)) || (i - 2 >= 0 && (vector(FOV_1, i - 2, j, k) > 0)) || ((i - 1 >= 0 && k + 1 < klast && (vector(FOV_1, i - 1, j, k + 1) > 0)) || (i - 1 >= 0 && k - 1 >= 0 && (vector(FOV_1, i - 1, j, k - 1) > 0)) || (i - 1 >= 0 && k + 2 < klast && (vector(FOV_1, i - 1, j, k + 2) > 0)) || (i - 1 >= 0 && k - 2 >= 0 && (vector(FOV_1, i - 1, j, k - 2) > 0)) || (i - 1 >= 0 && j + 1 < jlast && (vector(FOV_1, i - 1, j + 1, k) > 0)) || (i - 1 >= 0 && j + 1 < jlast && k + 1 < klast && (vector(FOV_1, i - 1, j + 1, k + 1) > 0)) || (i - 1 >= 0 && j + 1 < jlast && k - 1 >= 0 && (vector(FOV_1, i - 1, j + 1, k - 1) > 0)) || (i - 1 >= 0 && j + 1 < jlast && k + 2 < klast && (vector(FOV_1, i - 1, j + 1, k + 2) > 0)) || (i - 1 >= 0 && j + 1 < jlast && k - 2 >= 0 && (vector(FOV_1, i - 1, j + 1, k - 2) > 0)) || (i - 1 >= 0 && j - 1 >= 0 && (vector(FOV_1, i - 1, j - 1, k) > 0)) || (i - 1 >= 0 && j - 1 >= 0 && k + 1 < klast && (vector(FOV_1, i - 1, j - 1, k + 1) > 0)) || (i - 1 >= 0 && j - 1 >= 0 && k - 1 >= 0 && (vector(FOV_1, i - 1, j - 1, k - 1) > 0)) || (i - 1 >= 0 && j - 1 >= 0 && k + 2 < klast && (vector(FOV_1, i - 1, j - 1, k + 2) > 0)) || (i - 1 >= 0 && j - 1 >= 0 && k - 2 >= 0 && (vector(FOV_1, i - 1, j - 1, k - 2) > 0)) || (i - 1 >= 0 && j + 2 < jlast && (vector(FOV_1, i - 1, j + 2, k) > 0)) || (i - 1 >= 0 && j + 2 < jlast && k + 1 < klast && (vector(FOV_1, i - 1, j + 2, k + 1) > 0)) || (i - 1 >= 0 && j + 2 < jlast && k - 1 >= 0 && (vector(FOV_1, i - 1, j + 2, k - 1) > 0)) || (i - 1 >= 0 && j + 2 < jlast && k + 2 < klast && (vector(FOV_1, i - 1, j + 2, k + 2) > 0)) || (i - 1 >= 0 && j + 2 < jlast && k - 2 >= 0 && (vector(FOV_1, i - 1, j + 2, k - 2) > 0)) || (i - 1 >= 0 && j - 2 >= 0 && (vector(FOV_1, i - 1, j - 2, k) > 0)) || (i - 1 >= 0 && j - 2 >= 0 && k + 1 < klast && (vector(FOV_1, i - 1, j - 2, k + 1) > 0)) || (i - 1 >= 0 && j - 2 >= 0 && k - 1 >= 0 && (vector(FOV_1, i - 1, j - 2, k - 1) > 0)) || (i - 1 >= 0 && j - 2 >= 0 && k + 2 < klast && (vector(FOV_1, i - 1, j - 2, k + 2) > 0)) || (i - 1 >= 0 && j - 2 >= 0 && k - 2 >= 0 && (vector(FOV_1, i - 1, j - 2, k - 2) > 0)) || (i - 2 >= 0 && k + 1 < klast && (vector(FOV_1, i - 2, j, k + 1) > 0)) || (i - 2 >= 0 && k - 1 >= 0 && (vector(FOV_1, i - 2, j, k - 1) > 0)) || (i - 2 >= 0 && k + 2 < klast && (vector(FOV_1, i - 2, j, k + 2) > 0)) || (i - 2 >= 0 && k - 2 >= 0 && (vector(FOV_1, i - 2, j, k - 2) > 0)) || (i - 2 >= 0 && j + 1 < jlast && (vector(FOV_1, i - 2, j + 1, k) > 0)) || (i - 2 >= 0 && j + 1 < jlast && k + 1 < klast && (vector(FOV_1, i - 2, j + 1, k + 1) > 0)) || (i - 2 >= 0 && j + 1 < jlast && k - 1 >= 0 && (vector(FOV_1, i - 2, j + 1, k - 1) > 0)) || (i - 2 >= 0 && j + 1 < jlast && k + 2 < klast && (vector(FOV_1, i - 2, j + 1, k + 2) > 0)) || (i - 2 >= 0 && j + 1 < jlast && k - 2 >= 0 && (vector(FOV_1, i - 2, j + 1, k - 2) > 0)) || (i - 2 >= 0 && j - 1 >= 0 && (vector(FOV_1, i - 2, j - 1, k) > 0)) || (i - 2 >= 0 && j - 1 >= 0 && k + 1 < klast && (vector(FOV_1, i - 2, j - 1, k + 1) > 0)) || (i - 2 >= 0 && j - 1 >= 0 && k - 1 >= 0 && (vector(FOV_1, i - 2, j - 1, k - 1) > 0)) || (i - 2 >= 0 && j - 1 >= 0 && k + 2 < klast && (vector(FOV_1, i - 2, j - 1, k + 2) > 0)) || (i - 2 >= 0 && j - 1 >= 0 && k - 2 >= 0 && (vector(FOV_1, i - 2, j - 1, k - 2) > 0)) || (i - 2 >= 0 && j + 2 < jlast && (vector(FOV_1, i - 2, j + 2, k) > 0)) || (i - 2 >= 0 && j + 2 < jlast && k + 1 < klast && (vector(FOV_1, i - 2, j + 2, k + 1) > 0)) || (i - 2 >= 0 && j + 2 < jlast && k - 1 >= 0 && (vector(FOV_1, i - 2, j + 2, k - 1) > 0)) || (i - 2 >= 0 && j + 2 < jlast && k + 2 < klast && (vector(FOV_1, i - 2, j + 2, k + 2) > 0)) || (i - 2 >= 0 && j + 2 < jlast && k - 2 >= 0 && (vector(FOV_1, i - 2, j + 2, k - 2) > 0)) || (i - 2 >= 0 && j - 2 >= 0 && (vector(FOV_1, i - 2, j - 2, k) > 0)) || (i - 2 >= 0 && j - 2 >= 0 && k + 1 < klast && (vector(FOV_1, i - 2, j - 2, k + 1) > 0)) || (i - 2 >= 0 && j - 2 >= 0 && k - 1 >= 0 && (vector(FOV_1, i - 2, j - 2, k - 1) > 0)) || (i - 2 >= 0 && j - 2 >= 0 && k + 2 < klast && (vector(FOV_1, i - 2, j - 2, k + 2) > 0)) || (i - 2 >= 0 && j - 2 >= 0 && k - 2 >= 0 && (vector(FOV_1, i - 2, j - 2, k - 2) > 0))))) {
						//Region field extrapolations
						if ((vector(d_i_rho_mx_my_mz_p_fx_fy_fz, i, j, k) != 0 || vector(d_j_rho_mx_my_mz_p_fx_fy_fz, i, j, k) != 0 || vector(d_k_rho_mx_my_mz_p_fx_fy_fz, i, j, k) != 0)) {
							extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBrhoSPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBrhoSPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBrhoSPk, k, rk2rho, FOV_1, dx, ilast, jlast);
							extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmxSPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmxSPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmxSPk, k, rk2mx, FOV_1, dx, ilast, jlast);
							extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmySPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmySPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmySPk, k, rk2my, FOV_1, dx, ilast, jlast);
							extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmzSPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmzSPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmzSPk, k, rk2mz, FOV_1, dx, ilast, jlast);
							extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBpSPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBpSPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBpSPk, k, p, FOV_1, dx, ilast, jlast);
							extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfxSPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfxSPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfxSPk, k, fx, FOV_1, dx, ilast, jlast);
							extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfySPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfySPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfySPk, k, fy, FOV_1, dx, ilast, jlast);
							extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfzSPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfzSPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfzSPk, k, fz, FOV_1, dx, ilast, jlast);
						}
					}
					if ((vector(FOV_yLower, i, j, k) > 0) && ((j + 1 < jlast && (vector(FOV_1,i, j + 1, k) > 0)) || (j + 2 < jlast && (vector(FOV_1,i, j + 2, k) > 0)) || ((j + 1 < jlast && k + 1 < klast && (vector(FOV_1, i, j + 1, k + 1) > 0)) || (j + 1 < jlast && k - 1 >= 0 && (vector(FOV_1, i, j + 1, k - 1) > 0)) || (j + 1 < jlast && k + 2 < klast && (vector(FOV_1, i, j + 1, k + 2) > 0)) || (j + 1 < jlast && k - 2 >= 0 && (vector(FOV_1, i, j + 1, k - 2) > 0)) || (j + 2 < jlast && k + 1 < klast && (vector(FOV_1, i, j + 2, k + 1) > 0)) || (j + 2 < jlast && k - 1 >= 0 && (vector(FOV_1, i, j + 2, k - 1) > 0)) || (j + 2 < jlast && k + 2 < klast && (vector(FOV_1, i, j + 2, k + 2) > 0)) || (j + 2 < jlast && k - 2 >= 0 && (vector(FOV_1, i, j + 2, k - 2) > 0)) || (i + 1 < ilast && j + 1 < jlast && (vector(FOV_1, i + 1, j + 1, k) > 0)) || (i + 1 < ilast && j + 1 < jlast && k + 1 < klast && (vector(FOV_1, i + 1, j + 1, k + 1) > 0)) || (i + 1 < ilast && j + 1 < jlast && k - 1 >= 0 && (vector(FOV_1, i + 1, j + 1, k - 1) > 0)) || (i + 1 < ilast && j + 1 < jlast && k + 2 < klast && (vector(FOV_1, i + 1, j + 1, k + 2) > 0)) || (i + 1 < ilast && j + 1 < jlast && k - 2 >= 0 && (vector(FOV_1, i + 1, j + 1, k - 2) > 0)) || (i + 1 < ilast && j + 2 < jlast && (vector(FOV_1, i + 1, j + 2, k) > 0)) || (i + 1 < ilast && j + 2 < jlast && k + 1 < klast && (vector(FOV_1, i + 1, j + 2, k + 1) > 0)) || (i + 1 < ilast && j + 2 < jlast && k - 1 >= 0 && (vector(FOV_1, i + 1, j + 2, k - 1) > 0)) || (i + 1 < ilast && j + 2 < jlast && k + 2 < klast && (vector(FOV_1, i + 1, j + 2, k + 2) > 0)) || (i + 1 < ilast && j + 2 < jlast && k - 2 >= 0 && (vector(FOV_1, i + 1, j + 2, k - 2) > 0)) || (i - 1 >= 0 && j + 1 < jlast && (vector(FOV_1, i - 1, j + 1, k) > 0)) || (i - 1 >= 0 && j + 1 < jlast && k + 1 < klast && (vector(FOV_1, i - 1, j + 1, k + 1) > 0)) || (i - 1 >= 0 && j + 1 < jlast && k - 1 >= 0 && (vector(FOV_1, i - 1, j + 1, k - 1) > 0)) || (i - 1 >= 0 && j + 1 < jlast && k + 2 < klast && (vector(FOV_1, i - 1, j + 1, k + 2) > 0)) || (i - 1 >= 0 && j + 1 < jlast && k - 2 >= 0 && (vector(FOV_1, i - 1, j + 1, k - 2) > 0)) || (i - 1 >= 0 && j + 2 < jlast && (vector(FOV_1, i - 1, j + 2, k) > 0)) || (i - 1 >= 0 && j + 2 < jlast && k + 1 < klast && (vector(FOV_1, i - 1, j + 2, k + 1) > 0)) || (i - 1 >= 0 && j + 2 < jlast && k - 1 >= 0 && (vector(FOV_1, i - 1, j + 2, k - 1) > 0)) || (i - 1 >= 0 && j + 2 < jlast && k + 2 < klast && (vector(FOV_1, i - 1, j + 2, k + 2) > 0)) || (i - 1 >= 0 && j + 2 < jlast && k - 2 >= 0 && (vector(FOV_1, i - 1, j + 2, k - 2) > 0)) || (i + 2 < ilast && j + 1 < jlast && (vector(FOV_1, i + 2, j + 1, k) > 0)) || (i + 2 < ilast && j + 1 < jlast && k + 1 < klast && (vector(FOV_1, i + 2, j + 1, k + 1) > 0)) || (i + 2 < ilast && j + 1 < jlast && k - 1 >= 0 && (vector(FOV_1, i + 2, j + 1, k - 1) > 0)) || (i + 2 < ilast && j + 1 < jlast && k + 2 < klast && (vector(FOV_1, i + 2, j + 1, k + 2) > 0)) || (i + 2 < ilast && j + 1 < jlast && k - 2 >= 0 && (vector(FOV_1, i + 2, j + 1, k - 2) > 0)) || (i + 2 < ilast && j + 2 < jlast && (vector(FOV_1, i + 2, j + 2, k) > 0)) || (i + 2 < ilast && j + 2 < jlast && k + 1 < klast && (vector(FOV_1, i + 2, j + 2, k + 1) > 0)) || (i + 2 < ilast && j + 2 < jlast && k - 1 >= 0 && (vector(FOV_1, i + 2, j + 2, k - 1) > 0)) || (i + 2 < ilast && j + 2 < jlast && k + 2 < klast && (vector(FOV_1, i + 2, j + 2, k + 2) > 0)) || (i + 2 < ilast && j + 2 < jlast && k - 2 >= 0 && (vector(FOV_1, i + 2, j + 2, k - 2) > 0)) || (i - 2 >= 0 && j + 1 < jlast && (vector(FOV_1, i - 2, j + 1, k) > 0)) || (i - 2 >= 0 && j + 1 < jlast && k + 1 < klast && (vector(FOV_1, i - 2, j + 1, k + 1) > 0)) || (i - 2 >= 0 && j + 1 < jlast && k - 1 >= 0 && (vector(FOV_1, i - 2, j + 1, k - 1) > 0)) || (i - 2 >= 0 && j + 1 < jlast && k + 2 < klast && (vector(FOV_1, i - 2, j + 1, k + 2) > 0)) || (i - 2 >= 0 && j + 1 < jlast && k - 2 >= 0 && (vector(FOV_1, i - 2, j + 1, k - 2) > 0)) || (i - 2 >= 0 && j + 2 < jlast && (vector(FOV_1, i - 2, j + 2, k) > 0)) || (i - 2 >= 0 && j + 2 < jlast && k + 1 < klast && (vector(FOV_1, i - 2, j + 2, k + 1) > 0)) || (i - 2 >= 0 && j + 2 < jlast && k - 1 >= 0 && (vector(FOV_1, i - 2, j + 2, k - 1) > 0)) || (i - 2 >= 0 && j + 2 < jlast && k + 2 < klast && (vector(FOV_1, i - 2, j + 2, k + 2) > 0)) || (i - 2 >= 0 && j + 2 < jlast && k - 2 >= 0 && (vector(FOV_1, i - 2, j + 2, k - 2) > 0))))) {
						vector(rk2rho, i, j, k) = 0.0;
						vector(rk2mx, i, j, k) = 0.0;
						vector(rk2my, i, j, k) = 0.0;
						vector(rk2mz, i, j, k) = 0.0;
						vector(p, i, j, k) = 0.0;
						vector(fx, i, j, k) = 0.0;
						vector(fy, i, j, k) = 0.0;
						vector(fz, i, j, k) = 0.0;
					}
					if ((vector(FOV_yUpper, i, j, k) > 0) && ((j - 1 >= 0 && (vector(FOV_1, i, j - 1, k) > 0)) || (j - 2 >= 0 && (vector(FOV_1, i, j - 2, k) > 0)) || ((j - 1 >= 0 && k + 1 < klast && (vector(FOV_1, i, j - 1, k + 1) > 0)) || (j - 1 >= 0 && k - 1 >= 0 && (vector(FOV_1, i, j - 1, k - 1) > 0)) || (j - 1 >= 0 && k + 2 < klast && (vector(FOV_1, i, j - 1, k + 2) > 0)) || (j - 1 >= 0 && k - 2 >= 0 && (vector(FOV_1, i, j - 1, k - 2) > 0)) || (j - 2 >= 0 && k + 1 < klast && (vector(FOV_1, i, j - 2, k + 1) > 0)) || (j - 2 >= 0 && k - 1 >= 0 && (vector(FOV_1, i, j - 2, k - 1) > 0)) || (j - 2 >= 0 && k + 2 < klast && (vector(FOV_1, i, j - 2, k + 2) > 0)) || (j - 2 >= 0 && k - 2 >= 0 && (vector(FOV_1, i, j - 2, k - 2) > 0)) || (i + 1 < ilast && j - 1 >= 0 && (vector(FOV_1, i + 1, j - 1, k) > 0)) || (i + 1 < ilast && j - 1 >= 0 && k + 1 < klast && (vector(FOV_1, i + 1, j - 1, k + 1) > 0)) || (i + 1 < ilast && j - 1 >= 0 && k - 1 >= 0 && (vector(FOV_1, i + 1, j - 1, k - 1) > 0)) || (i + 1 < ilast && j - 1 >= 0 && k + 2 < klast && (vector(FOV_1, i + 1, j - 1, k + 2) > 0)) || (i + 1 < ilast && j - 1 >= 0 && k - 2 >= 0 && (vector(FOV_1, i + 1, j - 1, k - 2) > 0)) || (i + 1 < ilast && j - 2 >= 0 && (vector(FOV_1, i + 1, j - 2, k) > 0)) || (i + 1 < ilast && j - 2 >= 0 && k + 1 < klast && (vector(FOV_1, i + 1, j - 2, k + 1) > 0)) || (i + 1 < ilast && j - 2 >= 0 && k - 1 >= 0 && (vector(FOV_1, i + 1, j - 2, k - 1) > 0)) || (i + 1 < ilast && j - 2 >= 0 && k + 2 < klast && (vector(FOV_1, i + 1, j - 2, k + 2) > 0)) || (i + 1 < ilast && j - 2 >= 0 && k - 2 >= 0 && (vector(FOV_1, i + 1, j - 2, k - 2) > 0)) || (i - 1 >= 0 && j - 1 >= 0 && (vector(FOV_1, i - 1, j - 1, k) > 0)) || (i - 1 >= 0 && j - 1 >= 0 && k + 1 < klast && (vector(FOV_1, i - 1, j - 1, k + 1) > 0)) || (i - 1 >= 0 && j - 1 >= 0 && k - 1 >= 0 && (vector(FOV_1, i - 1, j - 1, k - 1) > 0)) || (i - 1 >= 0 && j - 1 >= 0 && k + 2 < klast && (vector(FOV_1, i - 1, j - 1, k + 2) > 0)) || (i - 1 >= 0 && j - 1 >= 0 && k - 2 >= 0 && (vector(FOV_1, i - 1, j - 1, k - 2) > 0)) || (i - 1 >= 0 && j - 2 >= 0 && (vector(FOV_1, i - 1, j - 2, k) > 0)) || (i - 1 >= 0 && j - 2 >= 0 && k + 1 < klast && (vector(FOV_1, i - 1, j - 2, k + 1) > 0)) || (i - 1 >= 0 && j - 2 >= 0 && k - 1 >= 0 && (vector(FOV_1, i - 1, j - 2, k - 1) > 0)) || (i - 1 >= 0 && j - 2 >= 0 && k + 2 < klast && (vector(FOV_1, i - 1, j - 2, k + 2) > 0)) || (i - 1 >= 0 && j - 2 >= 0 && k - 2 >= 0 && (vector(FOV_1, i - 1, j - 2, k - 2) > 0)) || (i + 2 < ilast && j - 1 >= 0 && (vector(FOV_1, i + 2, j - 1, k) > 0)) || (i + 2 < ilast && j - 1 >= 0 && k + 1 < klast && (vector(FOV_1, i + 2, j - 1, k + 1) > 0)) || (i + 2 < ilast && j - 1 >= 0 && k - 1 >= 0 && (vector(FOV_1, i + 2, j - 1, k - 1) > 0)) || (i + 2 < ilast && j - 1 >= 0 && k + 2 < klast && (vector(FOV_1, i + 2, j - 1, k + 2) > 0)) || (i + 2 < ilast && j - 1 >= 0 && k - 2 >= 0 && (vector(FOV_1, i + 2, j - 1, k - 2) > 0)) || (i + 2 < ilast && j - 2 >= 0 && (vector(FOV_1, i + 2, j - 2, k) > 0)) || (i + 2 < ilast && j - 2 >= 0 && k + 1 < klast && (vector(FOV_1, i + 2, j - 2, k + 1) > 0)) || (i + 2 < ilast && j - 2 >= 0 && k - 1 >= 0 && (vector(FOV_1, i + 2, j - 2, k - 1) > 0)) || (i + 2 < ilast && j - 2 >= 0 && k + 2 < klast && (vector(FOV_1, i + 2, j - 2, k + 2) > 0)) || (i + 2 < ilast && j - 2 >= 0 && k - 2 >= 0 && (vector(FOV_1, i + 2, j - 2, k - 2) > 0)) || (i - 2 >= 0 && j - 1 >= 0 && (vector(FOV_1, i - 2, j - 1, k) > 0)) || (i - 2 >= 0 && j - 1 >= 0 && k + 1 < klast && (vector(FOV_1, i - 2, j - 1, k + 1) > 0)) || (i - 2 >= 0 && j - 1 >= 0 && k - 1 >= 0 && (vector(FOV_1, i - 2, j - 1, k - 1) > 0)) || (i - 2 >= 0 && j - 1 >= 0 && k + 2 < klast && (vector(FOV_1, i - 2, j - 1, k + 2) > 0)) || (i - 2 >= 0 && j - 1 >= 0 && k - 2 >= 0 && (vector(FOV_1, i - 2, j - 1, k - 2) > 0)) || (i - 2 >= 0 && j - 2 >= 0 && (vector(FOV_1, i - 2, j - 2, k) > 0)) || (i - 2 >= 0 && j - 2 >= 0 && k + 1 < klast && (vector(FOV_1, i - 2, j - 2, k + 1) > 0)) || (i - 2 >= 0 && j - 2 >= 0 && k - 1 >= 0 && (vector(FOV_1, i - 2, j - 2, k - 1) > 0)) || (i - 2 >= 0 && j - 2 >= 0 && k + 2 < klast && (vector(FOV_1, i - 2, j - 2, k + 2) > 0)) || (i - 2 >= 0 && j - 2 >= 0 && k - 2 >= 0 && (vector(FOV_1, i - 2, j - 2, k - 2) > 0))))) {
						vector(rk2rho, i, j, k) = 0.0;
						vector(rk2mx, i, j, k) = 0.0;
						vector(rk2my, i, j, k) = 0.0;
						vector(rk2mz, i, j, k) = 0.0;
						vector(p, i, j, k) = 0.0;
						vector(fx, i, j, k) = 0.0;
						vector(fy, i, j, k) = 0.0;
						vector(fz, i, j, k) = 0.0;
					}
					if ((vector(FOV_zLower, i, j, k) > 0) && ((k + 1 < klast && (vector(FOV_1,i, j, k + 1) > 0)) || (k + 2 < klast && (vector(FOV_1,i, j, k + 2) > 0)) || ((j + 1 < jlast && k + 1 < klast && (vector(FOV_1, i, j + 1, k + 1) > 0)) || (j + 1 < jlast && k + 2 < klast && (vector(FOV_1, i, j + 1, k + 2) > 0)) || (j - 1 >= 0 && k + 1 < klast && (vector(FOV_1, i, j - 1, k + 1) > 0)) || (j - 1 >= 0 && k + 2 < klast && (vector(FOV_1, i, j - 1, k + 2) > 0)) || (j + 2 < jlast && k + 1 < klast && (vector(FOV_1, i, j + 2, k + 1) > 0)) || (j + 2 < jlast && k + 2 < klast && (vector(FOV_1, i, j + 2, k + 2) > 0)) || (j - 2 >= 0 && k + 1 < klast && (vector(FOV_1, i, j - 2, k + 1) > 0)) || (j - 2 >= 0 && k + 2 < klast && (vector(FOV_1, i, j - 2, k + 2) > 0)) || (i + 1 < ilast && k + 1 < klast && (vector(FOV_1, i + 1, j, k + 1) > 0)) || (i + 1 < ilast && k + 2 < klast && (vector(FOV_1, i + 1, j, k + 2) > 0)) || (i + 1 < ilast && j + 1 < jlast && k + 1 < klast && (vector(FOV_1, i + 1, j + 1, k + 1) > 0)) || (i + 1 < ilast && j + 1 < jlast && k + 2 < klast && (vector(FOV_1, i + 1, j + 1, k + 2) > 0)) || (i + 1 < ilast && j - 1 >= 0 && k + 1 < klast && (vector(FOV_1, i + 1, j - 1, k + 1) > 0)) || (i + 1 < ilast && j - 1 >= 0 && k + 2 < klast && (vector(FOV_1, i + 1, j - 1, k + 2) > 0)) || (i + 1 < ilast && j + 2 < jlast && k + 1 < klast && (vector(FOV_1, i + 1, j + 2, k + 1) > 0)) || (i + 1 < ilast && j + 2 < jlast && k + 2 < klast && (vector(FOV_1, i + 1, j + 2, k + 2) > 0)) || (i + 1 < ilast && j - 2 >= 0 && k + 1 < klast && (vector(FOV_1, i + 1, j - 2, k + 1) > 0)) || (i + 1 < ilast && j - 2 >= 0 && k + 2 < klast && (vector(FOV_1, i + 1, j - 2, k + 2) > 0)) || (i - 1 >= 0 && k + 1 < klast && (vector(FOV_1, i - 1, j, k + 1) > 0)) || (i - 1 >= 0 && k + 2 < klast && (vector(FOV_1, i - 1, j, k + 2) > 0)) || (i - 1 >= 0 && j + 1 < jlast && k + 1 < klast && (vector(FOV_1, i - 1, j + 1, k + 1) > 0)) || (i - 1 >= 0 && j + 1 < jlast && k + 2 < klast && (vector(FOV_1, i - 1, j + 1, k + 2) > 0)) || (i - 1 >= 0 && j - 1 >= 0 && k + 1 < klast && (vector(FOV_1, i - 1, j - 1, k + 1) > 0)) || (i - 1 >= 0 && j - 1 >= 0 && k + 2 < klast && (vector(FOV_1, i - 1, j - 1, k + 2) > 0)) || (i - 1 >= 0 && j + 2 < jlast && k + 1 < klast && (vector(FOV_1, i - 1, j + 2, k + 1) > 0)) || (i - 1 >= 0 && j + 2 < jlast && k + 2 < klast && (vector(FOV_1, i - 1, j + 2, k + 2) > 0)) || (i - 1 >= 0 && j - 2 >= 0 && k + 1 < klast && (vector(FOV_1, i - 1, j - 2, k + 1) > 0)) || (i - 1 >= 0 && j - 2 >= 0 && k + 2 < klast && (vector(FOV_1, i - 1, j - 2, k + 2) > 0)) || (i + 2 < ilast && k + 1 < klast && (vector(FOV_1, i + 2, j, k + 1) > 0)) || (i + 2 < ilast && k + 2 < klast && (vector(FOV_1, i + 2, j, k + 2) > 0)) || (i + 2 < ilast && j + 1 < jlast && k + 1 < klast && (vector(FOV_1, i + 2, j + 1, k + 1) > 0)) || (i + 2 < ilast && j + 1 < jlast && k + 2 < klast && (vector(FOV_1, i + 2, j + 1, k + 2) > 0)) || (i + 2 < ilast && j - 1 >= 0 && k + 1 < klast && (vector(FOV_1, i + 2, j - 1, k + 1) > 0)) || (i + 2 < ilast && j - 1 >= 0 && k + 2 < klast && (vector(FOV_1, i + 2, j - 1, k + 2) > 0)) || (i + 2 < ilast && j + 2 < jlast && k + 1 < klast && (vector(FOV_1, i + 2, j + 2, k + 1) > 0)) || (i + 2 < ilast && j + 2 < jlast && k + 2 < klast && (vector(FOV_1, i + 2, j + 2, k + 2) > 0)) || (i + 2 < ilast && j - 2 >= 0 && k + 1 < klast && (vector(FOV_1, i + 2, j - 2, k + 1) > 0)) || (i + 2 < ilast && j - 2 >= 0 && k + 2 < klast && (vector(FOV_1, i + 2, j - 2, k + 2) > 0)) || (i - 2 >= 0 && k + 1 < klast && (vector(FOV_1, i - 2, j, k + 1) > 0)) || (i - 2 >= 0 && k + 2 < klast && (vector(FOV_1, i - 2, j, k + 2) > 0)) || (i - 2 >= 0 && j + 1 < jlast && k + 1 < klast && (vector(FOV_1, i - 2, j + 1, k + 1) > 0)) || (i - 2 >= 0 && j + 1 < jlast && k + 2 < klast && (vector(FOV_1, i - 2, j + 1, k + 2) > 0)) || (i - 2 >= 0 && j - 1 >= 0 && k + 1 < klast && (vector(FOV_1, i - 2, j - 1, k + 1) > 0)) || (i - 2 >= 0 && j - 1 >= 0 && k + 2 < klast && (vector(FOV_1, i - 2, j - 1, k + 2) > 0)) || (i - 2 >= 0 && j + 2 < jlast && k + 1 < klast && (vector(FOV_1, i - 2, j + 2, k + 1) > 0)) || (i - 2 >= 0 && j + 2 < jlast && k + 2 < klast && (vector(FOV_1, i - 2, j + 2, k + 2) > 0)) || (i - 2 >= 0 && j - 2 >= 0 && k + 1 < klast && (vector(FOV_1, i - 2, j - 2, k + 1) > 0)) || (i - 2 >= 0 && j - 2 >= 0 && k + 2 < klast && (vector(FOV_1, i - 2, j - 2, k + 2) > 0))))) {
						vector(rk2rho, i, j, k) = 0.0;
						vector(rk2mx, i, j, k) = 0.0;
						vector(rk2my, i, j, k) = 0.0;
						vector(rk2mz, i, j, k) = 0.0;
						vector(p, i, j, k) = 0.0;
						vector(fx, i, j, k) = 0.0;
						vector(fy, i, j, k) = 0.0;
						vector(fz, i, j, k) = 0.0;
					}
					if ((vector(FOV_zUpper, i, j, k) > 0) && ((k - 1 >= 0 && (vector(FOV_1, i, j, k - 1) > 0)) || (k - 2 >= 0 && (vector(FOV_1, i, j, k - 2) > 0)) || ((j + 1 < jlast && k - 1 >= 0 && (vector(FOV_1, i, j + 1, k - 1) > 0)) || (j + 1 < jlast && k - 2 >= 0 && (vector(FOV_1, i, j + 1, k - 2) > 0)) || (j - 1 >= 0 && k - 1 >= 0 && (vector(FOV_1, i, j - 1, k - 1) > 0)) || (j - 1 >= 0 && k - 2 >= 0 && (vector(FOV_1, i, j - 1, k - 2) > 0)) || (j + 2 < jlast && k - 1 >= 0 && (vector(FOV_1, i, j + 2, k - 1) > 0)) || (j + 2 < jlast && k - 2 >= 0 && (vector(FOV_1, i, j + 2, k - 2) > 0)) || (j - 2 >= 0 && k - 1 >= 0 && (vector(FOV_1, i, j - 2, k - 1) > 0)) || (j - 2 >= 0 && k - 2 >= 0 && (vector(FOV_1, i, j - 2, k - 2) > 0)) || (i + 1 < ilast && k - 1 >= 0 && (vector(FOV_1, i + 1, j, k - 1) > 0)) || (i + 1 < ilast && k - 2 >= 0 && (vector(FOV_1, i + 1, j, k - 2) > 0)) || (i + 1 < ilast && j + 1 < jlast && k - 1 >= 0 && (vector(FOV_1, i + 1, j + 1, k - 1) > 0)) || (i + 1 < ilast && j + 1 < jlast && k - 2 >= 0 && (vector(FOV_1, i + 1, j + 1, k - 2) > 0)) || (i + 1 < ilast && j - 1 >= 0 && k - 1 >= 0 && (vector(FOV_1, i + 1, j - 1, k - 1) > 0)) || (i + 1 < ilast && j - 1 >= 0 && k - 2 >= 0 && (vector(FOV_1, i + 1, j - 1, k - 2) > 0)) || (i + 1 < ilast && j + 2 < jlast && k - 1 >= 0 && (vector(FOV_1, i + 1, j + 2, k - 1) > 0)) || (i + 1 < ilast && j + 2 < jlast && k - 2 >= 0 && (vector(FOV_1, i + 1, j + 2, k - 2) > 0)) || (i + 1 < ilast && j - 2 >= 0 && k - 1 >= 0 && (vector(FOV_1, i + 1, j - 2, k - 1) > 0)) || (i + 1 < ilast && j - 2 >= 0 && k - 2 >= 0 && (vector(FOV_1, i + 1, j - 2, k - 2) > 0)) || (i - 1 >= 0 && k - 1 >= 0 && (vector(FOV_1, i - 1, j, k - 1) > 0)) || (i - 1 >= 0 && k - 2 >= 0 && (vector(FOV_1, i - 1, j, k - 2) > 0)) || (i - 1 >= 0 && j + 1 < jlast && k - 1 >= 0 && (vector(FOV_1, i - 1, j + 1, k - 1) > 0)) || (i - 1 >= 0 && j + 1 < jlast && k - 2 >= 0 && (vector(FOV_1, i - 1, j + 1, k - 2) > 0)) || (i - 1 >= 0 && j - 1 >= 0 && k - 1 >= 0 && (vector(FOV_1, i - 1, j - 1, k - 1) > 0)) || (i - 1 >= 0 && j - 1 >= 0 && k - 2 >= 0 && (vector(FOV_1, i - 1, j - 1, k - 2) > 0)) || (i - 1 >= 0 && j + 2 < jlast && k - 1 >= 0 && (vector(FOV_1, i - 1, j + 2, k - 1) > 0)) || (i - 1 >= 0 && j + 2 < jlast && k - 2 >= 0 && (vector(FOV_1, i - 1, j + 2, k - 2) > 0)) || (i - 1 >= 0 && j - 2 >= 0 && k - 1 >= 0 && (vector(FOV_1, i - 1, j - 2, k - 1) > 0)) || (i - 1 >= 0 && j - 2 >= 0 && k - 2 >= 0 && (vector(FOV_1, i - 1, j - 2, k - 2) > 0)) || (i + 2 < ilast && k - 1 >= 0 && (vector(FOV_1, i + 2, j, k - 1) > 0)) || (i + 2 < ilast && k - 2 >= 0 && (vector(FOV_1, i + 2, j, k - 2) > 0)) || (i + 2 < ilast && j + 1 < jlast && k - 1 >= 0 && (vector(FOV_1, i + 2, j + 1, k - 1) > 0)) || (i + 2 < ilast && j + 1 < jlast && k - 2 >= 0 && (vector(FOV_1, i + 2, j + 1, k - 2) > 0)) || (i + 2 < ilast && j - 1 >= 0 && k - 1 >= 0 && (vector(FOV_1, i + 2, j - 1, k - 1) > 0)) || (i + 2 < ilast && j - 1 >= 0 && k - 2 >= 0 && (vector(FOV_1, i + 2, j - 1, k - 2) > 0)) || (i + 2 < ilast && j + 2 < jlast && k - 1 >= 0 && (vector(FOV_1, i + 2, j + 2, k - 1) > 0)) || (i + 2 < ilast && j + 2 < jlast && k - 2 >= 0 && (vector(FOV_1, i + 2, j + 2, k - 2) > 0)) || (i + 2 < ilast && j - 2 >= 0 && k - 1 >= 0 && (vector(FOV_1, i + 2, j - 2, k - 1) > 0)) || (i + 2 < ilast && j - 2 >= 0 && k - 2 >= 0 && (vector(FOV_1, i + 2, j - 2, k - 2) > 0)) || (i - 2 >= 0 && k - 1 >= 0 && (vector(FOV_1, i - 2, j, k - 1) > 0)) || (i - 2 >= 0 && k - 2 >= 0 && (vector(FOV_1, i - 2, j, k - 2) > 0)) || (i - 2 >= 0 && j + 1 < jlast && k - 1 >= 0 && (vector(FOV_1, i - 2, j + 1, k - 1) > 0)) || (i - 2 >= 0 && j + 1 < jlast && k - 2 >= 0 && (vector(FOV_1, i - 2, j + 1, k - 2) > 0)) || (i - 2 >= 0 && j - 1 >= 0 && k - 1 >= 0 && (vector(FOV_1, i - 2, j - 1, k - 1) > 0)) || (i - 2 >= 0 && j - 1 >= 0 && k - 2 >= 0 && (vector(FOV_1, i - 2, j - 1, k - 2) > 0)) || (i - 2 >= 0 && j + 2 < jlast && k - 1 >= 0 && (vector(FOV_1, i - 2, j + 2, k - 1) > 0)) || (i - 2 >= 0 && j + 2 < jlast && k - 2 >= 0 && (vector(FOV_1, i - 2, j + 2, k - 2) > 0)) || (i - 2 >= 0 && j - 2 >= 0 && k - 1 >= 0 && (vector(FOV_1, i - 2, j - 2, k - 1) > 0)) || (i - 2 >= 0 && j - 2 >= 0 && k - 2 >= 0 && (vector(FOV_1, i - 2, j - 2, k - 2) > 0))))) {
						vector(rk2rho, i, j, k) = 0.0;
						vector(rk2mx, i, j, k) = 0.0;
						vector(rk2my, i, j, k) = 0.0;
						vector(rk2mz, i, j, k) = 0.0;
						vector(p, i, j, k) = 0.0;
						vector(fx, i, j, k) = 0.0;
						vector(fy, i, j, k) = 0.0;
						vector(fz, i, j, k) = 0.0;
					}
					if (vector(FOV_1, i, j, k) > 0) {
						//Region field extrapolations
						if ((vector(d_i_Null, i, j, k) != 0 || vector(d_j_Null, i, j, k) != 0 || vector(d_k_Null, i, j, k) != 0)) {
							extrapolate_field(d_i_Null, extrapolatedSBNullSPi, i, d_j_Null, extrapolatedSBNullSPj, j, d_k_Null, extrapolatedSBNullSPk, k, rk2Null, FOV_3, dx, ilast, jlast);
						}
					}
				}
			}
		}
		for(int k = 0; k < klast; k++) {
			for(int j = 0; j < jlast; j++) {
				for(int i = 0; i < ilast; i++) {
					if ((vector(FOV_xLower, i, j, k) > 0) && ((i + 1 < ilast && (vector(FOV_3,i + 1, j, k) > 0)) || (i + 2 < ilast && (vector(FOV_3,i + 2, j, k) > 0)))) {
						vector(extrapolatedSBrhoSPi, i, j, k) = vector(rk2rho, i, j, k);
						vector(extrapolatedSBrhoSPj, i, j, k) = vector(rk2rho, i, j, k);
						vector(extrapolatedSBrhoSPk, i, j, k) = vector(rk2rho, i, j, k);
						vector(extrapolatedSBmxSPi, i, j, k) = vector(rk2mx, i, j, k);
						vector(extrapolatedSBmxSPj, i, j, k) = vector(rk2mx, i, j, k);
						vector(extrapolatedSBmxSPk, i, j, k) = vector(rk2mx, i, j, k);
						vector(extrapolatedSBmySPi, i, j, k) = vector(rk2my, i, j, k);
						vector(extrapolatedSBmySPj, i, j, k) = vector(rk2my, i, j, k);
						vector(extrapolatedSBmySPk, i, j, k) = vector(rk2my, i, j, k);
						vector(extrapolatedSBmzSPi, i, j, k) = vector(rk2mz, i, j, k);
						vector(extrapolatedSBmzSPj, i, j, k) = vector(rk2mz, i, j, k);
						vector(extrapolatedSBmzSPk, i, j, k) = vector(rk2mz, i, j, k);
						vector(extrapolatedSBNullSPi, i, j, k) = vector(rk2Null, i, j, k);
						vector(extrapolatedSBNullSPj, i, j, k) = vector(rk2Null, i, j, k);
						vector(extrapolatedSBNullSPk, i, j, k) = vector(rk2Null, i, j, k);
						vector(extrapolatedSBpSPi, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBpSPj, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBpSPk, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBfxSPi, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfxSPj, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfxSPk, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfySPi, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfySPj, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfySPk, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfzSPi, i, j, k) = vector(fz, i, j, k);
						vector(extrapolatedSBfzSPj, i, j, k) = vector(fz, i, j, k);
						vector(extrapolatedSBfzSPk, i, j, k) = vector(fz, i, j, k);
					}
					if ((vector(FOV_xUpper, i, j, k) > 0) && ((i - 1 >= 0 && (vector(FOV_3, i - 1, j, k) > 0)) || (i - 2 >= 0 && (vector(FOV_3, i - 2, j, k) > 0)))) {
						vector(extrapolatedSBrhoSPi, i, j, k) = vector(rk2rho, i, j, k);
						vector(extrapolatedSBrhoSPj, i, j, k) = vector(rk2rho, i, j, k);
						vector(extrapolatedSBrhoSPk, i, j, k) = vector(rk2rho, i, j, k);
						vector(extrapolatedSBmxSPi, i, j, k) = vector(rk2mx, i, j, k);
						vector(extrapolatedSBmxSPj, i, j, k) = vector(rk2mx, i, j, k);
						vector(extrapolatedSBmxSPk, i, j, k) = vector(rk2mx, i, j, k);
						vector(extrapolatedSBmySPi, i, j, k) = vector(rk2my, i, j, k);
						vector(extrapolatedSBmySPj, i, j, k) = vector(rk2my, i, j, k);
						vector(extrapolatedSBmySPk, i, j, k) = vector(rk2my, i, j, k);
						vector(extrapolatedSBmzSPi, i, j, k) = vector(rk2mz, i, j, k);
						vector(extrapolatedSBmzSPj, i, j, k) = vector(rk2mz, i, j, k);
						vector(extrapolatedSBmzSPk, i, j, k) = vector(rk2mz, i, j, k);
						vector(extrapolatedSBNullSPi, i, j, k) = vector(rk2Null, i, j, k);
						vector(extrapolatedSBNullSPj, i, j, k) = vector(rk2Null, i, j, k);
						vector(extrapolatedSBNullSPk, i, j, k) = vector(rk2Null, i, j, k);
						vector(extrapolatedSBpSPi, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBpSPj, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBpSPk, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBfxSPi, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfxSPj, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfxSPk, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfySPi, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfySPj, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfySPk, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfzSPi, i, j, k) = vector(fz, i, j, k);
						vector(extrapolatedSBfzSPj, i, j, k) = vector(fz, i, j, k);
						vector(extrapolatedSBfzSPk, i, j, k) = vector(fz, i, j, k);
					}
					if ((vector(FOV_yLower, i, j, k) > 0) && ((j + 1 < jlast && (vector(FOV_3,i, j + 1, k) > 0)) || (j + 2 < jlast && (vector(FOV_3,i, j + 2, k) > 0)))) {
						vector(extrapolatedSBrhoSPi, i, j, k) = vector(rk2rho, i, j, k);
						vector(extrapolatedSBrhoSPj, i, j, k) = vector(rk2rho, i, j, k);
						vector(extrapolatedSBrhoSPk, i, j, k) = vector(rk2rho, i, j, k);
						vector(extrapolatedSBmxSPi, i, j, k) = vector(rk2mx, i, j, k);
						vector(extrapolatedSBmxSPj, i, j, k) = vector(rk2mx, i, j, k);
						vector(extrapolatedSBmxSPk, i, j, k) = vector(rk2mx, i, j, k);
						vector(extrapolatedSBmySPi, i, j, k) = vector(rk2my, i, j, k);
						vector(extrapolatedSBmySPj, i, j, k) = vector(rk2my, i, j, k);
						vector(extrapolatedSBmySPk, i, j, k) = vector(rk2my, i, j, k);
						vector(extrapolatedSBmzSPi, i, j, k) = vector(rk2mz, i, j, k);
						vector(extrapolatedSBmzSPj, i, j, k) = vector(rk2mz, i, j, k);
						vector(extrapolatedSBmzSPk, i, j, k) = vector(rk2mz, i, j, k);
						vector(extrapolatedSBNullSPi, i, j, k) = vector(rk2Null, i, j, k);
						vector(extrapolatedSBNullSPj, i, j, k) = vector(rk2Null, i, j, k);
						vector(extrapolatedSBNullSPk, i, j, k) = vector(rk2Null, i, j, k);
						vector(extrapolatedSBpSPi, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBpSPj, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBpSPk, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBfxSPi, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfxSPj, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfxSPk, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfySPi, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfySPj, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfySPk, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfzSPi, i, j, k) = vector(fz, i, j, k);
						vector(extrapolatedSBfzSPj, i, j, k) = vector(fz, i, j, k);
						vector(extrapolatedSBfzSPk, i, j, k) = vector(fz, i, j, k);
					}
					if ((vector(FOV_yUpper, i, j, k) > 0) && ((j - 1 >= 0 && (vector(FOV_3, i, j - 1, k) > 0)) || (j - 2 >= 0 && (vector(FOV_3, i, j - 2, k) > 0)))) {
						vector(extrapolatedSBrhoSPi, i, j, k) = vector(rk2rho, i, j, k);
						vector(extrapolatedSBrhoSPj, i, j, k) = vector(rk2rho, i, j, k);
						vector(extrapolatedSBrhoSPk, i, j, k) = vector(rk2rho, i, j, k);
						vector(extrapolatedSBmxSPi, i, j, k) = vector(rk2mx, i, j, k);
						vector(extrapolatedSBmxSPj, i, j, k) = vector(rk2mx, i, j, k);
						vector(extrapolatedSBmxSPk, i, j, k) = vector(rk2mx, i, j, k);
						vector(extrapolatedSBmySPi, i, j, k) = vector(rk2my, i, j, k);
						vector(extrapolatedSBmySPj, i, j, k) = vector(rk2my, i, j, k);
						vector(extrapolatedSBmySPk, i, j, k) = vector(rk2my, i, j, k);
						vector(extrapolatedSBmzSPi, i, j, k) = vector(rk2mz, i, j, k);
						vector(extrapolatedSBmzSPj, i, j, k) = vector(rk2mz, i, j, k);
						vector(extrapolatedSBmzSPk, i, j, k) = vector(rk2mz, i, j, k);
						vector(extrapolatedSBNullSPi, i, j, k) = vector(rk2Null, i, j, k);
						vector(extrapolatedSBNullSPj, i, j, k) = vector(rk2Null, i, j, k);
						vector(extrapolatedSBNullSPk, i, j, k) = vector(rk2Null, i, j, k);
						vector(extrapolatedSBpSPi, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBpSPj, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBpSPk, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBfxSPi, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfxSPj, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfxSPk, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfySPi, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfySPj, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfySPk, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfzSPi, i, j, k) = vector(fz, i, j, k);
						vector(extrapolatedSBfzSPj, i, j, k) = vector(fz, i, j, k);
						vector(extrapolatedSBfzSPk, i, j, k) = vector(fz, i, j, k);
					}
					if ((vector(FOV_zLower, i, j, k) > 0) && ((k + 1 < klast && (vector(FOV_3,i, j, k + 1) > 0)) || (k + 2 < klast && (vector(FOV_3,i, j, k + 2) > 0)))) {
						vector(extrapolatedSBrhoSPi, i, j, k) = vector(rk2rho, i, j, k);
						vector(extrapolatedSBrhoSPj, i, j, k) = vector(rk2rho, i, j, k);
						vector(extrapolatedSBrhoSPk, i, j, k) = vector(rk2rho, i, j, k);
						vector(extrapolatedSBmxSPi, i, j, k) = vector(rk2mx, i, j, k);
						vector(extrapolatedSBmxSPj, i, j, k) = vector(rk2mx, i, j, k);
						vector(extrapolatedSBmxSPk, i, j, k) = vector(rk2mx, i, j, k);
						vector(extrapolatedSBmySPi, i, j, k) = vector(rk2my, i, j, k);
						vector(extrapolatedSBmySPj, i, j, k) = vector(rk2my, i, j, k);
						vector(extrapolatedSBmySPk, i, j, k) = vector(rk2my, i, j, k);
						vector(extrapolatedSBmzSPi, i, j, k) = vector(rk2mz, i, j, k);
						vector(extrapolatedSBmzSPj, i, j, k) = vector(rk2mz, i, j, k);
						vector(extrapolatedSBmzSPk, i, j, k) = vector(rk2mz, i, j, k);
						vector(extrapolatedSBNullSPi, i, j, k) = vector(rk2Null, i, j, k);
						vector(extrapolatedSBNullSPj, i, j, k) = vector(rk2Null, i, j, k);
						vector(extrapolatedSBNullSPk, i, j, k) = vector(rk2Null, i, j, k);
						vector(extrapolatedSBpSPi, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBpSPj, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBpSPk, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBfxSPi, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfxSPj, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfxSPk, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfySPi, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfySPj, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfySPk, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfzSPi, i, j, k) = vector(fz, i, j, k);
						vector(extrapolatedSBfzSPj, i, j, k) = vector(fz, i, j, k);
						vector(extrapolatedSBfzSPk, i, j, k) = vector(fz, i, j, k);
					}
					if ((vector(FOV_zUpper, i, j, k) > 0) && ((k - 1 >= 0 && (vector(FOV_3, i, j, k - 1) > 0)) || (k - 2 >= 0 && (vector(FOV_3, i, j, k - 2) > 0)))) {
						vector(extrapolatedSBrhoSPi, i, j, k) = vector(rk2rho, i, j, k);
						vector(extrapolatedSBrhoSPj, i, j, k) = vector(rk2rho, i, j, k);
						vector(extrapolatedSBrhoSPk, i, j, k) = vector(rk2rho, i, j, k);
						vector(extrapolatedSBmxSPi, i, j, k) = vector(rk2mx, i, j, k);
						vector(extrapolatedSBmxSPj, i, j, k) = vector(rk2mx, i, j, k);
						vector(extrapolatedSBmxSPk, i, j, k) = vector(rk2mx, i, j, k);
						vector(extrapolatedSBmySPi, i, j, k) = vector(rk2my, i, j, k);
						vector(extrapolatedSBmySPj, i, j, k) = vector(rk2my, i, j, k);
						vector(extrapolatedSBmySPk, i, j, k) = vector(rk2my, i, j, k);
						vector(extrapolatedSBmzSPi, i, j, k) = vector(rk2mz, i, j, k);
						vector(extrapolatedSBmzSPj, i, j, k) = vector(rk2mz, i, j, k);
						vector(extrapolatedSBmzSPk, i, j, k) = vector(rk2mz, i, j, k);
						vector(extrapolatedSBNullSPi, i, j, k) = vector(rk2Null, i, j, k);
						vector(extrapolatedSBNullSPj, i, j, k) = vector(rk2Null, i, j, k);
						vector(extrapolatedSBNullSPk, i, j, k) = vector(rk2Null, i, j, k);
						vector(extrapolatedSBpSPi, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBpSPj, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBpSPk, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBfxSPi, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfxSPj, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfxSPk, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfySPi, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfySPj, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfySPk, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfzSPi, i, j, k) = vector(fz, i, j, k);
						vector(extrapolatedSBfzSPj, i, j, k) = vector(fz, i, j, k);
						vector(extrapolatedSBfzSPk, i, j, k) = vector(fz, i, j, k);
					}
					if ((vector(FOV_xLower, i, j, k) > 0) && ((i + 1 < ilast && (vector(FOV_1,i + 1, j, k) > 0)) || (i + 2 < ilast && (vector(FOV_1,i + 2, j, k) > 0)))) {
						vector(extrapolatedSBNullSPi, i, j, k) = vector(rk2Null, i, j, k);
						vector(extrapolatedSBNullSPj, i, j, k) = vector(rk2Null, i, j, k);
						vector(extrapolatedSBNullSPk, i, j, k) = vector(rk2Null, i, j, k);
					}
					if ((vector(FOV_xUpper, i, j, k) > 0) && ((i - 1 >= 0 && (vector(FOV_1, i - 1, j, k) > 0)) || (i - 2 >= 0 && (vector(FOV_1, i - 2, j, k) > 0)))) {
						vector(extrapolatedSBNullSPi, i, j, k) = vector(rk2Null, i, j, k);
						vector(extrapolatedSBNullSPj, i, j, k) = vector(rk2Null, i, j, k);
						vector(extrapolatedSBNullSPk, i, j, k) = vector(rk2Null, i, j, k);
					}
					if ((vector(FOV_yLower, i, j, k) > 0) && ((j + 1 < jlast && (vector(FOV_1,i, j + 1, k) > 0)) || (j + 2 < jlast && (vector(FOV_1,i, j + 2, k) > 0)))) {
						vector(extrapolatedSBrhoSPi, i, j, k) = vector(rk2rho, i, j, k);
						vector(extrapolatedSBrhoSPj, i, j, k) = vector(rk2rho, i, j, k);
						vector(extrapolatedSBrhoSPk, i, j, k) = vector(rk2rho, i, j, k);
						vector(extrapolatedSBmxSPi, i, j, k) = vector(rk2mx, i, j, k);
						vector(extrapolatedSBmxSPj, i, j, k) = vector(rk2mx, i, j, k);
						vector(extrapolatedSBmxSPk, i, j, k) = vector(rk2mx, i, j, k);
						vector(extrapolatedSBmySPi, i, j, k) = vector(rk2my, i, j, k);
						vector(extrapolatedSBmySPj, i, j, k) = vector(rk2my, i, j, k);
						vector(extrapolatedSBmySPk, i, j, k) = vector(rk2my, i, j, k);
						vector(extrapolatedSBmzSPi, i, j, k) = vector(rk2mz, i, j, k);
						vector(extrapolatedSBmzSPj, i, j, k) = vector(rk2mz, i, j, k);
						vector(extrapolatedSBmzSPk, i, j, k) = vector(rk2mz, i, j, k);
						vector(extrapolatedSBNullSPi, i, j, k) = vector(rk2Null, i, j, k);
						vector(extrapolatedSBNullSPj, i, j, k) = vector(rk2Null, i, j, k);
						vector(extrapolatedSBNullSPk, i, j, k) = vector(rk2Null, i, j, k);
						vector(extrapolatedSBpSPi, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBpSPj, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBpSPk, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBfxSPi, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfxSPj, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfxSPk, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfySPi, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfySPj, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfySPk, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfzSPi, i, j, k) = vector(fz, i, j, k);
						vector(extrapolatedSBfzSPj, i, j, k) = vector(fz, i, j, k);
						vector(extrapolatedSBfzSPk, i, j, k) = vector(fz, i, j, k);
					}
					if ((vector(FOV_yUpper, i, j, k) > 0) && ((j - 1 >= 0 && (vector(FOV_1, i, j - 1, k) > 0)) || (j - 2 >= 0 && (vector(FOV_1, i, j - 2, k) > 0)))) {
						vector(extrapolatedSBrhoSPi, i, j, k) = vector(rk2rho, i, j, k);
						vector(extrapolatedSBrhoSPj, i, j, k) = vector(rk2rho, i, j, k);
						vector(extrapolatedSBrhoSPk, i, j, k) = vector(rk2rho, i, j, k);
						vector(extrapolatedSBmxSPi, i, j, k) = vector(rk2mx, i, j, k);
						vector(extrapolatedSBmxSPj, i, j, k) = vector(rk2mx, i, j, k);
						vector(extrapolatedSBmxSPk, i, j, k) = vector(rk2mx, i, j, k);
						vector(extrapolatedSBmySPi, i, j, k) = vector(rk2my, i, j, k);
						vector(extrapolatedSBmySPj, i, j, k) = vector(rk2my, i, j, k);
						vector(extrapolatedSBmySPk, i, j, k) = vector(rk2my, i, j, k);
						vector(extrapolatedSBmzSPi, i, j, k) = vector(rk2mz, i, j, k);
						vector(extrapolatedSBmzSPj, i, j, k) = vector(rk2mz, i, j, k);
						vector(extrapolatedSBmzSPk, i, j, k) = vector(rk2mz, i, j, k);
						vector(extrapolatedSBNullSPi, i, j, k) = vector(rk2Null, i, j, k);
						vector(extrapolatedSBNullSPj, i, j, k) = vector(rk2Null, i, j, k);
						vector(extrapolatedSBNullSPk, i, j, k) = vector(rk2Null, i, j, k);
						vector(extrapolatedSBpSPi, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBpSPj, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBpSPk, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBfxSPi, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfxSPj, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfxSPk, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfySPi, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfySPj, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfySPk, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfzSPi, i, j, k) = vector(fz, i, j, k);
						vector(extrapolatedSBfzSPj, i, j, k) = vector(fz, i, j, k);
						vector(extrapolatedSBfzSPk, i, j, k) = vector(fz, i, j, k);
					}
					if ((vector(FOV_zLower, i, j, k) > 0) && ((k + 1 < klast && (vector(FOV_1,i, j, k + 1) > 0)) || (k + 2 < klast && (vector(FOV_1,i, j, k + 2) > 0)))) {
						vector(extrapolatedSBrhoSPi, i, j, k) = vector(rk2rho, i, j, k);
						vector(extrapolatedSBrhoSPj, i, j, k) = vector(rk2rho, i, j, k);
						vector(extrapolatedSBrhoSPk, i, j, k) = vector(rk2rho, i, j, k);
						vector(extrapolatedSBmxSPi, i, j, k) = vector(rk2mx, i, j, k);
						vector(extrapolatedSBmxSPj, i, j, k) = vector(rk2mx, i, j, k);
						vector(extrapolatedSBmxSPk, i, j, k) = vector(rk2mx, i, j, k);
						vector(extrapolatedSBmySPi, i, j, k) = vector(rk2my, i, j, k);
						vector(extrapolatedSBmySPj, i, j, k) = vector(rk2my, i, j, k);
						vector(extrapolatedSBmySPk, i, j, k) = vector(rk2my, i, j, k);
						vector(extrapolatedSBmzSPi, i, j, k) = vector(rk2mz, i, j, k);
						vector(extrapolatedSBmzSPj, i, j, k) = vector(rk2mz, i, j, k);
						vector(extrapolatedSBmzSPk, i, j, k) = vector(rk2mz, i, j, k);
						vector(extrapolatedSBNullSPi, i, j, k) = vector(rk2Null, i, j, k);
						vector(extrapolatedSBNullSPj, i, j, k) = vector(rk2Null, i, j, k);
						vector(extrapolatedSBNullSPk, i, j, k) = vector(rk2Null, i, j, k);
						vector(extrapolatedSBpSPi, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBpSPj, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBpSPk, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBfxSPi, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfxSPj, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfxSPk, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfySPi, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfySPj, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfySPk, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfzSPi, i, j, k) = vector(fz, i, j, k);
						vector(extrapolatedSBfzSPj, i, j, k) = vector(fz, i, j, k);
						vector(extrapolatedSBfzSPk, i, j, k) = vector(fz, i, j, k);
					}
					if ((vector(FOV_zUpper, i, j, k) > 0) && ((k - 1 >= 0 && (vector(FOV_1, i, j, k - 1) > 0)) || (k - 2 >= 0 && (vector(FOV_1, i, j, k - 2) > 0)))) {
						vector(extrapolatedSBrhoSPi, i, j, k) = vector(rk2rho, i, j, k);
						vector(extrapolatedSBrhoSPj, i, j, k) = vector(rk2rho, i, j, k);
						vector(extrapolatedSBrhoSPk, i, j, k) = vector(rk2rho, i, j, k);
						vector(extrapolatedSBmxSPi, i, j, k) = vector(rk2mx, i, j, k);
						vector(extrapolatedSBmxSPj, i, j, k) = vector(rk2mx, i, j, k);
						vector(extrapolatedSBmxSPk, i, j, k) = vector(rk2mx, i, j, k);
						vector(extrapolatedSBmySPi, i, j, k) = vector(rk2my, i, j, k);
						vector(extrapolatedSBmySPj, i, j, k) = vector(rk2my, i, j, k);
						vector(extrapolatedSBmySPk, i, j, k) = vector(rk2my, i, j, k);
						vector(extrapolatedSBmzSPi, i, j, k) = vector(rk2mz, i, j, k);
						vector(extrapolatedSBmzSPj, i, j, k) = vector(rk2mz, i, j, k);
						vector(extrapolatedSBmzSPk, i, j, k) = vector(rk2mz, i, j, k);
						vector(extrapolatedSBNullSPi, i, j, k) = vector(rk2Null, i, j, k);
						vector(extrapolatedSBNullSPj, i, j, k) = vector(rk2Null, i, j, k);
						vector(extrapolatedSBNullSPk, i, j, k) = vector(rk2Null, i, j, k);
						vector(extrapolatedSBpSPi, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBpSPj, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBpSPk, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBfxSPi, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfxSPj, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfxSPk, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfySPi, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfySPj, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfySPk, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfzSPi, i, j, k) = vector(fz, i, j, k);
						vector(extrapolatedSBfzSPj, i, j, k) = vector(fz, i, j, k);
						vector(extrapolatedSBfzSPk, i, j, k) = vector(fz, i, j, k);
					}
				}
			}
		}
	}
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch >& patch = *p_it;
		double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_3 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_3_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
		double* FOV_zLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zLower_id).get())->getPointer();
		double* FOV_zUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zUpper_id).get())->getPointer();
	
		double* SpeedSBi_3 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBi_3_id).get())->getPointer();
		double* SpeedSBj_3 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBj_3_id).get())->getPointer();
		double* SpeedSBk_3 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBk_3_id).get())->getPointer();
		double* extrapolatedSBmxSPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmxSPi_id).get())->getPointer();
		double* extrapolatedSBmySPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmySPi_id).get())->getPointer();
		double* extrapolatedSBmzSPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmzSPi_id).get())->getPointer();
		double* extrapolatedSBrhoSPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBrhoSPi_id).get())->getPointer();
		double* SpeedSBi_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBi_1_id).get())->getPointer();
		double* extrapolatedSBmySPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmySPj_id).get())->getPointer();
		double* extrapolatedSBmxSPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmxSPj_id).get())->getPointer();
		double* extrapolatedSBmzSPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmzSPj_id).get())->getPointer();
		double* extrapolatedSBrhoSPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBrhoSPj_id).get())->getPointer();
		double* SpeedSBj_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBj_1_id).get())->getPointer();
		double* extrapolatedSBmzSPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmzSPk_id).get())->getPointer();
		double* extrapolatedSBmxSPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmxSPk_id).get())->getPointer();
		double* extrapolatedSBmySPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmySPk_id).get())->getPointer();
		double* extrapolatedSBrhoSPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBrhoSPk_id).get())->getPointer();
		double* SpeedSBk_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBk_1_id).get())->getPointer();
		double m_n, m_tx, m_ty, m_tz, extrapolatedSBcnSPi, extrapolatedSBcSPi, extrapolatedSBcnSPj, extrapolatedSBcSPj, extrapolatedSBcnSPk, extrapolatedSBcSPk;
	
		//Get the dimensions of the patch
		const hier::Index boxfirst = patch->getBox().lower();
		const hier::Index boxlast = patch->getBox().upper();
	
		//Get delta spaces into an array. dx, dy, dz.
		std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
		const double* dx  = patch_geom->getDx();
	
		//Auxiliary definitions
		int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
		int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
		int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;
		for(int k = 0; k < klast; k++) {
			for(int j = 0; j < jlast; j++) {
				for(int i = 0; i < ilast; i++) {
					if ((vector(FOV_3, i, j, k) > 0 || (((i + 1 < ilast && vector(FOV_3, i + 1, j, k) > 0) || (i + 2 < ilast && vector(FOV_3, i + 2, j, k) > 0) || (j + 1 < jlast && vector(FOV_3, i, j + 1, k) > 0) || (j + 2 < jlast && vector(FOV_3, i, j + 2, k) > 0) || (k + 1 < klast && vector(FOV_3, i, j, k + 1) > 0) || (k + 2 < klast && vector(FOV_3, i, j, k + 2) > 0)) || ((i - 1 >= 0 && vector(FOV_3, i - 1, j, k) > 0) || (i - 2 >= 0 && vector(FOV_3, i - 2, j, k) > 0) || (j - 1 >= 0 && vector(FOV_3, i, j - 1, k) > 0) || (j - 2 >= 0 && vector(FOV_3, i, j - 2, k) > 0) || (k - 1 >= 0 && vector(FOV_3, i, j, k - 1) > 0) || (k - 2 >= 0 && vector(FOV_3, i, j, k - 2) > 0))) )) {
						vector(SpeedSBi_3, i, j, k) = fabs(1.0);
						vector(SpeedSBj_3, i, j, k) = fabs(1.0);
						vector(SpeedSBk_3, i, j, k) = fabs(1.0);
					}
					if ((vector(FOV_1, i, j, k) > 0 || (((i + 1 < ilast && vector(FOV_1, i + 1, j, k) > 0) || (i + 2 < ilast && vector(FOV_1, i + 2, j, k) > 0) || (j + 1 < jlast && vector(FOV_1, i, j + 1, k) > 0) || (j + 2 < jlast && vector(FOV_1, i, j + 2, k) > 0) || (k + 1 < klast && vector(FOV_1, i, j, k + 1) > 0) || (k + 2 < klast && vector(FOV_1, i, j, k + 2) > 0)) || ((i - 1 >= 0 && vector(FOV_1, i - 1, j, k) > 0) || (i - 2 >= 0 && vector(FOV_1, i - 2, j, k) > 0) || (j - 1 >= 0 && vector(FOV_1, i, j - 1, k) > 0) || (j - 2 >= 0 && vector(FOV_1, i, j - 2, k) > 0) || (k - 1 >= 0 && vector(FOV_1, i, j, k - 1) > 0) || (k - 2 >= 0 && vector(FOV_1, i, j, k - 2) > 0))) )) {
						m_n = vector(extrapolatedSBmxSPi, i, j, k);
						m_tx = 0.0;
						m_ty = vector(extrapolatedSBmySPi, i, j, k);
						m_tz = vector(extrapolatedSBmzSPi, i, j, k);
						extrapolatedSBcnSPi = m_n / vector(extrapolatedSBrhoSPi, i, j, k);
						extrapolatedSBcSPi = c0 * (pow((vector(extrapolatedSBrhoSPi, i, j, k) / rho0), ((gamma + (-1.0)) / 2.0)));
						vector(SpeedSBi_1, i, j, k) = MAX(fabs(extrapolatedSBcSPi + extrapolatedSBcnSPi), MAX(fabs((-extrapolatedSBcSPi) + extrapolatedSBcnSPi), MAX(fabs(extrapolatedSBcnSPi), MAX(fabs(extrapolatedSBcnSPi), fabs(extrapolatedSBcnSPi)))));
						m_n = vector(extrapolatedSBmySPj, i, j, k);
						m_tx = vector(extrapolatedSBmxSPj, i, j, k);
						m_ty = 0.0;
						m_tz = vector(extrapolatedSBmzSPj, i, j, k);
						extrapolatedSBcnSPj = m_n / vector(extrapolatedSBrhoSPj, i, j, k);
						extrapolatedSBcSPj = c0 * (pow((vector(extrapolatedSBrhoSPj, i, j, k) / rho0), ((gamma + (-1.0)) / 2.0)));
						vector(SpeedSBj_1, i, j, k) = MAX(fabs(extrapolatedSBcSPj + extrapolatedSBcnSPj), MAX(fabs((-extrapolatedSBcSPj) + extrapolatedSBcnSPj), MAX(fabs(extrapolatedSBcnSPj), MAX(fabs(extrapolatedSBcnSPj), fabs(extrapolatedSBcnSPj)))));
						m_n = vector(extrapolatedSBmzSPk, i, j, k);
						m_tx = vector(extrapolatedSBmxSPk, i, j, k);
						m_ty = vector(extrapolatedSBmySPk, i, j, k);
						m_tz = 0.0;
						extrapolatedSBcnSPk = m_n / vector(extrapolatedSBrhoSPk, i, j, k);
						extrapolatedSBcSPk = c0 * (pow((vector(extrapolatedSBrhoSPk, i, j, k) / rho0), ((gamma + (-1.0)) / 2.0)));
						vector(SpeedSBk_1, i, j, k) = MAX(fabs(extrapolatedSBcSPk + extrapolatedSBcnSPk), MAX(fabs((-extrapolatedSBcSPk) + extrapolatedSBcnSPk), MAX(fabs(extrapolatedSBcnSPk), MAX(fabs(extrapolatedSBcnSPk), fabs(extrapolatedSBcnSPk)))));
					}
				}
			}
		}
	}
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch >& patch = *p_it;
		double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_3 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_3_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
		double* FOV_zLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zLower_id).get())->getPointer();
		double* FOV_zUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zUpper_id).get())->getPointer();
	
		double* SpeedSBi_3 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBi_3_id).get())->getPointer();
		double* SpeedSBj_3 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBj_3_id).get())->getPointer();
		double* SpeedSBk_3 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBk_3_id).get())->getPointer();
		double* SpeedSBi_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBi_1_id).get())->getPointer();
		double* SpeedSBj_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBj_1_id).get())->getPointer();
		double* SpeedSBk_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBk_1_id).get())->getPointer();
	
		//Get the dimensions of the patch
		const hier::Index boxfirst = patch->getBox().lower();
		const hier::Index boxlast = patch->getBox().upper();
	
		//Get delta spaces into an array. dx, dy, dz.
		std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
		const double* dx  = patch_geom->getDx();
	
		//Auxiliary definitions
		int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
		int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
		int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;
		for(int k = 0; k < klast; k++) {
			for(int j = 0; j < jlast; j++) {
				for(int i = 0; i < ilast; i++) {
					if ((vector(FOV_3, i, j, k) > 0 || (((i + 1 < ilast && vector(FOV_3, i + 1, j, k) > 0) || (j + 1 < jlast && vector(FOV_3, i, j + 1, k) > 0) || (k + 1 < klast && vector(FOV_3, i, j, k + 1) > 0)) || ((i - 1 >= 0 && vector(FOV_3, i - 1, j, k) > 0) || (j - 1 >= 0 && vector(FOV_3, i, j - 1, k) > 0) || (k - 1 >= 0 && vector(FOV_3, i, j, k - 1) > 0))) ) && (i + 1 < ilast && i - 1 >= 0 && j + 1 < jlast && j - 1 >= 0 && k + 1 < klast && k - 1 >= 0)) {
						vector(SpeedSBi_3, i, j, k) = MAX(vector(SpeedSBi_3, i, j, k), vector(SpeedSBi_3, i + 1, j, k));
						vector(SpeedSBj_3, i, j, k) = MAX(vector(SpeedSBj_3, i, j, k), vector(SpeedSBj_3, i, j + 1, k));
						vector(SpeedSBk_3, i, j, k) = MAX(vector(SpeedSBk_3, i, j, k), vector(SpeedSBk_3, i, j, k + 1));
					}
					if ((vector(FOV_1, i, j, k) > 0 || (((i + 1 < ilast && vector(FOV_1, i + 1, j, k) > 0) || (j + 1 < jlast && vector(FOV_1, i, j + 1, k) > 0) || (k + 1 < klast && vector(FOV_1, i, j, k + 1) > 0)) || ((i - 1 >= 0 && vector(FOV_1, i - 1, j, k) > 0) || (j - 1 >= 0 && vector(FOV_1, i, j - 1, k) > 0) || (k - 1 >= 0 && vector(FOV_1, i, j, k - 1) > 0))) ) && (i + 1 < ilast && i - 1 >= 0 && j + 1 < jlast && j - 1 >= 0 && k + 1 < klast && k - 1 >= 0)) {
						vector(SpeedSBi_1, i, j, k) = MAX(vector(SpeedSBi_1, i, j, k), vector(SpeedSBi_1, i + 1, j, k));
						vector(SpeedSBj_1, i, j, k) = MAX(vector(SpeedSBj_1, i, j, k), vector(SpeedSBj_1, i, j + 1, k));
						vector(SpeedSBk_1, i, j, k) = MAX(vector(SpeedSBk_1, i, j, k), vector(SpeedSBk_1, i, j, k + 1));
					}
				}
			}
		}
	}
	//Fill ghosts and periodical boundaries
	d_bdry_sched_advance17[ln]->fillData(current_time, false);
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch >& patch = *p_it;
		double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_3 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_3_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
		double* FOV_zLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zLower_id).get())->getPointer();
		double* FOV_zUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zUpper_id).get())->getPointer();
	
		double* ad_mx_o0_t0_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_mx_o0_t0_m0_l0_1_id).get())->getPointer();
		double* rk2mx = ((pdat::NodeData<double> *) patch->getPatchData(d_rk2mx_id).get())->getPointer();
		double* rk2rho = ((pdat::NodeData<double> *) patch->getPatchData(d_rk2rho_id).get())->getPointer();
		double* ad_my_o0_t3_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_my_o0_t3_m0_l0_1_id).get())->getPointer();
		double* rk2my = ((pdat::NodeData<double> *) patch->getPatchData(d_rk2my_id).get())->getPointer();
		double* ad_mz_o0_t3_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_mz_o0_t3_m0_l0_1_id).get())->getPointer();
		double* rk2mz = ((pdat::NodeData<double> *) patch->getPatchData(d_rk2mz_id).get())->getPointer();
		double* FluxSBimx_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBimx_1_id).get())->getPointer();
		double* extrapolatedSBrhoSPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBrhoSPi_id).get())->getPointer();
		double* extrapolatedSBmxSPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmxSPi_id).get())->getPointer();
		double* extrapolatedSBpSPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBpSPi_id).get())->getPointer();
		double* FluxSBimy_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBimy_1_id).get())->getPointer();
		double* extrapolatedSBmySPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmySPi_id).get())->getPointer();
		double* FluxSBimz_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBimz_1_id).get())->getPointer();
		double* extrapolatedSBmzSPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmzSPi_id).get())->getPointer();
		double* FluxSBjmx_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjmx_1_id).get())->getPointer();
		double* extrapolatedSBrhoSPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBrhoSPj_id).get())->getPointer();
		double* extrapolatedSBmxSPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmxSPj_id).get())->getPointer();
		double* extrapolatedSBmySPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmySPj_id).get())->getPointer();
		double* FluxSBjmy_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjmy_1_id).get())->getPointer();
		double* extrapolatedSBpSPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBpSPj_id).get())->getPointer();
		double* FluxSBjmz_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjmz_1_id).get())->getPointer();
		double* extrapolatedSBmzSPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmzSPj_id).get())->getPointer();
		double* FluxSBkmx_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkmx_1_id).get())->getPointer();
		double* extrapolatedSBrhoSPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBrhoSPk_id).get())->getPointer();
		double* extrapolatedSBmxSPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmxSPk_id).get())->getPointer();
		double* extrapolatedSBmzSPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmzSPk_id).get())->getPointer();
		double* FluxSBkmy_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkmy_1_id).get())->getPointer();
		double* extrapolatedSBmySPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmySPk_id).get())->getPointer();
		double* FluxSBkmz_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkmz_1_id).get())->getPointer();
		double* extrapolatedSBpSPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBpSPk_id).get())->getPointer();
	
		//Get the dimensions of the patch
		const hier::Index boxfirst = patch->getBox().lower();
		const hier::Index boxlast = patch->getBox().upper();
	
		//Get delta spaces into an array. dx, dy, dz.
		std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
		const double* dx  = patch_geom->getDx();
	
		//Auxiliary definitions
		int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
		int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
		int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;
		for(int k = 0; k < klast; k++) {
			for(int j = 0; j < jlast; j++) {
				for(int i = 0; i < ilast; i++) {
					if ((vector(FOV_1, i, j, k) > 0 || (((i + 1 < ilast && vector(FOV_1, i + 1, j, k) > 0) || (i + 2 < ilast && vector(FOV_1, i + 2, j, k) > 0) || (j + 1 < jlast && vector(FOV_1, i, j + 1, k) > 0) || (j + 2 < jlast && vector(FOV_1, i, j + 2, k) > 0) || (k + 1 < klast && vector(FOV_1, i, j, k + 1) > 0) || (k + 2 < klast && vector(FOV_1, i, j, k + 2) > 0)) || ((i - 1 >= 0 && vector(FOV_1, i - 1, j, k) > 0) || (i - 2 >= 0 && vector(FOV_1, i - 2, j, k) > 0) || (j - 1 >= 0 && vector(FOV_1, i, j - 1, k) > 0) || (j - 2 >= 0 && vector(FOV_1, i, j - 2, k) > 0) || (k - 1 >= 0 && vector(FOV_1, i, j, k - 1) > 0) || (k - 2 >= 0 && vector(FOV_1, i, j, k - 2) > 0)) || (((j + 1 < jlast && k + 1 < klast) && vector(FOV_1, i, j + 1, k + 1) > 0) || ((j + 1 < jlast && k - 1 >= 0) && vector(FOV_1, i, j + 1, k - 1) > 0) || ((j + 1 < jlast && k + 2 < klast) && vector(FOV_1, i, j + 1, k + 2) > 0) || ((j + 1 < jlast && k - 2 >= 0) && vector(FOV_1, i, j + 1, k - 2) > 0) || ((j - 1 >= 0 && k + 1 < klast) && vector(FOV_1, i, j - 1, k + 1) > 0) || ((j - 1 >= 0 && k - 1 >= 0) && vector(FOV_1, i, j - 1, k - 1) > 0) || ((j - 1 >= 0 && k + 2 < klast) && vector(FOV_1, i, j - 1, k + 2) > 0) || ((j - 1 >= 0 && k - 2 >= 0) && vector(FOV_1, i, j - 1, k - 2) > 0) || ((j + 2 < jlast && k + 1 < klast) && vector(FOV_1, i, j + 2, k + 1) > 0) || ((j + 2 < jlast && k - 1 >= 0) && vector(FOV_1, i, j + 2, k - 1) > 0) || ((j + 2 < jlast && k + 2 < klast) && vector(FOV_1, i, j + 2, k + 2) > 0) || ((j + 2 < jlast && k - 2 >= 0) && vector(FOV_1, i, j + 2, k - 2) > 0) || ((j - 2 >= 0 && k + 1 < klast) && vector(FOV_1, i, j - 2, k + 1) > 0) || ((j - 2 >= 0 && k - 1 >= 0) && vector(FOV_1, i, j - 2, k - 1) > 0) || ((j - 2 >= 0 && k + 2 < klast) && vector(FOV_1, i, j - 2, k + 2) > 0) || ((j - 2 >= 0 && k - 2 >= 0) && vector(FOV_1, i, j - 2, k - 2) > 0) || ((i + 1 < ilast && k + 1 < klast) && vector(FOV_1, i + 1, j, k + 1) > 0) || ((i + 1 < ilast && k - 1 >= 0) && vector(FOV_1, i + 1, j, k - 1) > 0) || ((i + 1 < ilast && k + 2 < klast) && vector(FOV_1, i + 1, j, k + 2) > 0) || ((i + 1 < ilast && k - 2 >= 0) && vector(FOV_1, i + 1, j, k - 2) > 0) || ((i + 1 < ilast && j + 1 < jlast) && vector(FOV_1, i + 1, j + 1, k) > 0) || ((i + 1 < ilast && j + 1 < jlast && k + 1 < klast) && vector(FOV_1, i + 1, j + 1, k + 1) > 0) || ((i + 1 < ilast && j + 1 < jlast && k - 1 >= 0) && vector(FOV_1, i + 1, j + 1, k - 1) > 0) || ((i + 1 < ilast && j + 1 < jlast && k + 2 < klast) && vector(FOV_1, i + 1, j + 1, k + 2) > 0) || ((i + 1 < ilast && j + 1 < jlast && k - 2 >= 0) && vector(FOV_1, i + 1, j + 1, k - 2) > 0) || ((i + 1 < ilast && j - 1 >= 0) && vector(FOV_1, i + 1, j - 1, k) > 0) || ((i + 1 < ilast && j - 1 >= 0 && k + 1 < klast) && vector(FOV_1, i + 1, j - 1, k + 1) > 0) || ((i + 1 < ilast && j - 1 >= 0 && k - 1 >= 0) && vector(FOV_1, i + 1, j - 1, k - 1) > 0) || ((i + 1 < ilast && j - 1 >= 0 && k + 2 < klast) && vector(FOV_1, i + 1, j - 1, k + 2) > 0) || ((i + 1 < ilast && j - 1 >= 0 && k - 2 >= 0) && vector(FOV_1, i + 1, j - 1, k - 2) > 0) || ((i + 1 < ilast && j + 2 < jlast) && vector(FOV_1, i + 1, j + 2, k) > 0) || ((i + 1 < ilast && j + 2 < jlast && k + 1 < klast) && vector(FOV_1, i + 1, j + 2, k + 1) > 0) || ((i + 1 < ilast && j + 2 < jlast && k - 1 >= 0) && vector(FOV_1, i + 1, j + 2, k - 1) > 0) || ((i + 1 < ilast && j + 2 < jlast && k + 2 < klast) && vector(FOV_1, i + 1, j + 2, k + 2) > 0) || ((i + 1 < ilast && j + 2 < jlast && k - 2 >= 0) && vector(FOV_1, i + 1, j + 2, k - 2) > 0) || ((i + 1 < ilast && j - 2 >= 0) && vector(FOV_1, i + 1, j - 2, k) > 0) || ((i + 1 < ilast && j - 2 >= 0 && k + 1 < klast) && vector(FOV_1, i + 1, j - 2, k + 1) > 0) || ((i + 1 < ilast && j - 2 >= 0 && k - 1 >= 0) && vector(FOV_1, i + 1, j - 2, k - 1) > 0) || ((i + 1 < ilast && j - 2 >= 0 && k + 2 < klast) && vector(FOV_1, i + 1, j - 2, k + 2) > 0) || ((i + 1 < ilast && j - 2 >= 0 && k - 2 >= 0) && vector(FOV_1, i + 1, j - 2, k - 2) > 0) || ((i - 1 >= 0 && k + 1 < klast) && vector(FOV_1, i - 1, j, k + 1) > 0) || ((i - 1 >= 0 && k - 1 >= 0) && vector(FOV_1, i - 1, j, k - 1) > 0) || ((i - 1 >= 0 && k + 2 < klast) && vector(FOV_1, i - 1, j, k + 2) > 0) || ((i - 1 >= 0 && k - 2 >= 0) && vector(FOV_1, i - 1, j, k - 2) > 0) || ((i - 1 >= 0 && j + 1 < jlast) && vector(FOV_1, i - 1, j + 1, k) > 0) || ((i - 1 >= 0 && j + 1 < jlast && k + 1 < klast) && vector(FOV_1, i - 1, j + 1, k + 1) > 0) || ((i - 1 >= 0 && j + 1 < jlast && k - 1 >= 0) && vector(FOV_1, i - 1, j + 1, k - 1) > 0) || ((i - 1 >= 0 && j + 1 < jlast && k + 2 < klast) && vector(FOV_1, i - 1, j + 1, k + 2) > 0) || ((i - 1 >= 0 && j + 1 < jlast && k - 2 >= 0) && vector(FOV_1, i - 1, j + 1, k - 2) > 0) || ((i - 1 >= 0 && j - 1 >= 0) && vector(FOV_1, i - 1, j - 1, k) > 0) || ((i - 1 >= 0 && j - 1 >= 0 && k + 1 < klast) && vector(FOV_1, i - 1, j - 1, k + 1) > 0) || ((i - 1 >= 0 && j - 1 >= 0 && k - 1 >= 0) && vector(FOV_1, i - 1, j - 1, k - 1) > 0) || ((i - 1 >= 0 && j - 1 >= 0 && k + 2 < klast) && vector(FOV_1, i - 1, j - 1, k + 2) > 0) || ((i - 1 >= 0 && j - 1 >= 0 && k - 2 >= 0) && vector(FOV_1, i - 1, j - 1, k - 2) > 0) || ((i - 1 >= 0 && j + 2 < jlast) && vector(FOV_1, i - 1, j + 2, k) > 0) || ((i - 1 >= 0 && j + 2 < jlast && k + 1 < klast) && vector(FOV_1, i - 1, j + 2, k + 1) > 0) || ((i - 1 >= 0 && j + 2 < jlast && k - 1 >= 0) && vector(FOV_1, i - 1, j + 2, k - 1) > 0) || ((i - 1 >= 0 && j + 2 < jlast && k + 2 < klast) && vector(FOV_1, i - 1, j + 2, k + 2) > 0) || ((i - 1 >= 0 && j + 2 < jlast && k - 2 >= 0) && vector(FOV_1, i - 1, j + 2, k - 2) > 0) || ((i - 1 >= 0 && j - 2 >= 0) && vector(FOV_1, i - 1, j - 2, k) > 0) || ((i - 1 >= 0 && j - 2 >= 0 && k + 1 < klast) && vector(FOV_1, i - 1, j - 2, k + 1) > 0) || ((i - 1 >= 0 && j - 2 >= 0 && k - 1 >= 0) && vector(FOV_1, i - 1, j - 2, k - 1) > 0) || ((i - 1 >= 0 && j - 2 >= 0 && k + 2 < klast) && vector(FOV_1, i - 1, j - 2, k + 2) > 0) || ((i - 1 >= 0 && j - 2 >= 0 && k - 2 >= 0) && vector(FOV_1, i - 1, j - 2, k - 2) > 0) || ((i + 2 < ilast && k + 1 < klast) && vector(FOV_1, i + 2, j, k + 1) > 0) || ((i + 2 < ilast && k - 1 >= 0) && vector(FOV_1, i + 2, j, k - 1) > 0) || ((i + 2 < ilast && k + 2 < klast) && vector(FOV_1, i + 2, j, k + 2) > 0) || ((i + 2 < ilast && k - 2 >= 0) && vector(FOV_1, i + 2, j, k - 2) > 0) || ((i + 2 < ilast && j + 1 < jlast) && vector(FOV_1, i + 2, j + 1, k) > 0) || ((i + 2 < ilast && j + 1 < jlast && k + 1 < klast) && vector(FOV_1, i + 2, j + 1, k + 1) > 0) || ((i + 2 < ilast && j + 1 < jlast && k - 1 >= 0) && vector(FOV_1, i + 2, j + 1, k - 1) > 0) || ((i + 2 < ilast && j + 1 < jlast && k + 2 < klast) && vector(FOV_1, i + 2, j + 1, k + 2) > 0) || ((i + 2 < ilast && j + 1 < jlast && k - 2 >= 0) && vector(FOV_1, i + 2, j + 1, k - 2) > 0) || ((i + 2 < ilast && j - 1 >= 0) && vector(FOV_1, i + 2, j - 1, k) > 0) || ((i + 2 < ilast && j - 1 >= 0 && k + 1 < klast) && vector(FOV_1, i + 2, j - 1, k + 1) > 0) || ((i + 2 < ilast && j - 1 >= 0 && k - 1 >= 0) && vector(FOV_1, i + 2, j - 1, k - 1) > 0) || ((i + 2 < ilast && j - 1 >= 0 && k + 2 < klast) && vector(FOV_1, i + 2, j - 1, k + 2) > 0) || ((i + 2 < ilast && j - 1 >= 0 && k - 2 >= 0) && vector(FOV_1, i + 2, j - 1, k - 2) > 0) || ((i + 2 < ilast && j + 2 < jlast) && vector(FOV_1, i + 2, j + 2, k) > 0) || ((i + 2 < ilast && j + 2 < jlast && k + 1 < klast) && vector(FOV_1, i + 2, j + 2, k + 1) > 0) || ((i + 2 < ilast && j + 2 < jlast && k - 1 >= 0) && vector(FOV_1, i + 2, j + 2, k - 1) > 0) || ((i + 2 < ilast && j + 2 < jlast && k + 2 < klast) && vector(FOV_1, i + 2, j + 2, k + 2) > 0) || ((i + 2 < ilast && j + 2 < jlast && k - 2 >= 0) && vector(FOV_1, i + 2, j + 2, k - 2) > 0) || ((i + 2 < ilast && j - 2 >= 0) && vector(FOV_1, i + 2, j - 2, k) > 0) || ((i + 2 < ilast && j - 2 >= 0 && k + 1 < klast) && vector(FOV_1, i + 2, j - 2, k + 1) > 0) || ((i + 2 < ilast && j - 2 >= 0 && k - 1 >= 0) && vector(FOV_1, i + 2, j - 2, k - 1) > 0) || ((i + 2 < ilast && j - 2 >= 0 && k + 2 < klast) && vector(FOV_1, i + 2, j - 2, k + 2) > 0) || ((i + 2 < ilast && j - 2 >= 0 && k - 2 >= 0) && vector(FOV_1, i + 2, j - 2, k - 2) > 0) || ((i - 2 >= 0 && k + 1 < klast) && vector(FOV_1, i - 2, j, k + 1) > 0) || ((i - 2 >= 0 && k - 1 >= 0) && vector(FOV_1, i - 2, j, k - 1) > 0) || ((i - 2 >= 0 && k + 2 < klast) && vector(FOV_1, i - 2, j, k + 2) > 0) || ((i - 2 >= 0 && k - 2 >= 0) && vector(FOV_1, i - 2, j, k - 2) > 0) || ((i - 2 >= 0 && j + 1 < jlast) && vector(FOV_1, i - 2, j + 1, k) > 0) || ((i - 2 >= 0 && j + 1 < jlast && k + 1 < klast) && vector(FOV_1, i - 2, j + 1, k + 1) > 0) || ((i - 2 >= 0 && j + 1 < jlast && k - 1 >= 0) && vector(FOV_1, i - 2, j + 1, k - 1) > 0) || ((i - 2 >= 0 && j + 1 < jlast && k + 2 < klast) && vector(FOV_1, i - 2, j + 1, k + 2) > 0) || ((i - 2 >= 0 && j + 1 < jlast && k - 2 >= 0) && vector(FOV_1, i - 2, j + 1, k - 2) > 0) || ((i - 2 >= 0 && j - 1 >= 0) && vector(FOV_1, i - 2, j - 1, k) > 0) || ((i - 2 >= 0 && j - 1 >= 0 && k + 1 < klast) && vector(FOV_1, i - 2, j - 1, k + 1) > 0) || ((i - 2 >= 0 && j - 1 >= 0 && k - 1 >= 0) && vector(FOV_1, i - 2, j - 1, k - 1) > 0) || ((i - 2 >= 0 && j - 1 >= 0 && k + 2 < klast) && vector(FOV_1, i - 2, j - 1, k + 2) > 0) || ((i - 2 >= 0 && j - 1 >= 0 && k - 2 >= 0) && vector(FOV_1, i - 2, j - 1, k - 2) > 0) || ((i - 2 >= 0 && j + 2 < jlast) && vector(FOV_1, i - 2, j + 2, k) > 0) || ((i - 2 >= 0 && j + 2 < jlast && k + 1 < klast) && vector(FOV_1, i - 2, j + 2, k + 1) > 0) || ((i - 2 >= 0 && j + 2 < jlast && k - 1 >= 0) && vector(FOV_1, i - 2, j + 2, k - 1) > 0) || ((i - 2 >= 0 && j + 2 < jlast && k + 2 < klast) && vector(FOV_1, i - 2, j + 2, k + 2) > 0) || ((i - 2 >= 0 && j + 2 < jlast && k - 2 >= 0) && vector(FOV_1, i - 2, j + 2, k - 2) > 0) || ((i - 2 >= 0 && j - 2 >= 0) && vector(FOV_1, i - 2, j - 2, k) > 0) || ((i - 2 >= 0 && j - 2 >= 0 && k + 1 < klast) && vector(FOV_1, i - 2, j - 2, k + 1) > 0) || ((i - 2 >= 0 && j - 2 >= 0 && k - 1 >= 0) && vector(FOV_1, i - 2, j - 2, k - 1) > 0) || ((i - 2 >= 0 && j - 2 >= 0 && k + 2 < klast) && vector(FOV_1, i - 2, j - 2, k + 2) > 0) || ((i - 2 >= 0 && j - 2 >= 0 && k - 2 >= 0) && vector(FOV_1, i - 2, j - 2, k - 2) > 0))) )) {
						vector(ad_mx_o0_t0_m0_l0_1, i, j, k) = vector(rk2mx, i, j, k) / vector(rk2rho, i, j, k);
						vector(ad_my_o0_t3_m0_l0_1, i, j, k) = vector(rk2my, i, j, k) / vector(rk2rho, i, j, k);
						vector(ad_mz_o0_t3_m0_l0_1, i, j, k) = vector(rk2mz, i, j, k) / vector(rk2rho, i, j, k);
						vector(FluxSBimx_1, i, j, k) = Fimx_PlaneI(vector(extrapolatedSBrhoSPi, i, j, k), vector(extrapolatedSBmxSPi, i, j, k), vector(extrapolatedSBpSPi, i, j, k), dx, simPlat_dt, ilast, jlast);
						vector(FluxSBimy_1, i, j, k) = Fimy_PlaneI(vector(extrapolatedSBrhoSPi, i, j, k), vector(extrapolatedSBmxSPi, i, j, k), vector(extrapolatedSBmySPi, i, j, k), dx, simPlat_dt, ilast, jlast);
						vector(FluxSBimz_1, i, j, k) = Fimz_PlaneI(vector(extrapolatedSBrhoSPi, i, j, k), vector(extrapolatedSBmxSPi, i, j, k), vector(extrapolatedSBmzSPi, i, j, k), dx, simPlat_dt, ilast, jlast);
						vector(FluxSBjmx_1, i, j, k) = Fjmx_PlaneI(vector(extrapolatedSBrhoSPj, i, j, k), vector(extrapolatedSBmxSPj, i, j, k), vector(extrapolatedSBmySPj, i, j, k), dx, simPlat_dt, ilast, jlast);
						vector(FluxSBjmy_1, i, j, k) = Fjmy_PlaneI(vector(extrapolatedSBrhoSPj, i, j, k), vector(extrapolatedSBmySPj, i, j, k), vector(extrapolatedSBpSPj, i, j, k), dx, simPlat_dt, ilast, jlast);
						vector(FluxSBjmz_1, i, j, k) = Fjmz_PlaneI(vector(extrapolatedSBrhoSPj, i, j, k), vector(extrapolatedSBmySPj, i, j, k), vector(extrapolatedSBmzSPj, i, j, k), dx, simPlat_dt, ilast, jlast);
						vector(FluxSBkmx_1, i, j, k) = Fkmx_PlaneI(vector(extrapolatedSBrhoSPk, i, j, k), vector(extrapolatedSBmxSPk, i, j, k), vector(extrapolatedSBmzSPk, i, j, k), dx, simPlat_dt, ilast, jlast);
						vector(FluxSBkmy_1, i, j, k) = Fkmy_PlaneI(vector(extrapolatedSBrhoSPk, i, j, k), vector(extrapolatedSBmySPk, i, j, k), vector(extrapolatedSBmzSPk, i, j, k), dx, simPlat_dt, ilast, jlast);
						vector(FluxSBkmz_1, i, j, k) = Fkmz_PlaneI(vector(extrapolatedSBrhoSPk, i, j, k), vector(extrapolatedSBmzSPk, i, j, k), vector(extrapolatedSBpSPk, i, j, k), dx, simPlat_dt, ilast, jlast);
					}
				}
			}
		}
	}
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch >& patch = *p_it;
		double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_3 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_3_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
		double* FOV_zLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zLower_id).get())->getPointer();
		double* FOV_zUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zUpper_id).get())->getPointer();
	
		double* Null = ((pdat::NodeData<double> *) patch->getPatchData(d_Null_id).get())->getPointer();
		double* Null_p = ((pdat::NodeData<double> *) patch->getPatchData(d_Null_p_id).get())->getPointer();
		double* rk2Null = ((pdat::NodeData<double> *) patch->getPatchData(d_rk2Null_id).get())->getPointer();
		double* ad_mx_o0_t0_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_mx_o0_t0_m0_l0_1_id).get())->getPointer();
		double* ad_my_o0_t3_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_my_o0_t3_m0_l0_1_id).get())->getPointer();
		double* ad_mz_o0_t3_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_mz_o0_t3_m0_l0_1_id).get())->getPointer();
		double* rk2rho = ((pdat::NodeData<double> *) patch->getPatchData(d_rk2rho_id).get())->getPointer();
		double* fx = ((pdat::NodeData<double> *) patch->getPatchData(d_fx_id).get())->getPointer();
		double* fy = ((pdat::NodeData<double> *) patch->getPatchData(d_fy_id).get())->getPointer();
		double* fz = ((pdat::NodeData<double> *) patch->getPatchData(d_fz_id).get())->getPointer();
		double* extrapolatedSBrhoSPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBrhoSPi_id).get())->getPointer();
		double* extrapolatedSBmxSPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmxSPi_id).get())->getPointer();
		double* SpeedSBi_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBi_1_id).get())->getPointer();
		double* extrapolatedSBrhoSPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBrhoSPj_id).get())->getPointer();
		double* extrapolatedSBmySPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmySPj_id).get())->getPointer();
		double* SpeedSBj_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBj_1_id).get())->getPointer();
		double* extrapolatedSBrhoSPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBrhoSPk_id).get())->getPointer();
		double* extrapolatedSBmzSPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmzSPk_id).get())->getPointer();
		double* SpeedSBk_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBk_1_id).get())->getPointer();
		double* FluxSBimx_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBimx_1_id).get())->getPointer();
		double* extrapolatedSBmxSPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmxSPj_id).get())->getPointer();
		double* FluxSBjmx_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjmx_1_id).get())->getPointer();
		double* extrapolatedSBmxSPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmxSPk_id).get())->getPointer();
		double* FluxSBkmx_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkmx_1_id).get())->getPointer();
		double* extrapolatedSBmySPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmySPi_id).get())->getPointer();
		double* FluxSBimy_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBimy_1_id).get())->getPointer();
		double* FluxSBjmy_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjmy_1_id).get())->getPointer();
		double* extrapolatedSBmySPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmySPk_id).get())->getPointer();
		double* FluxSBkmy_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkmy_1_id).get())->getPointer();
		double* extrapolatedSBmzSPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmzSPi_id).get())->getPointer();
		double* FluxSBimz_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBimz_1_id).get())->getPointer();
		double* extrapolatedSBmzSPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmzSPj_id).get())->getPointer();
		double* FluxSBjmz_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjmz_1_id).get())->getPointer();
		double* FluxSBkmz_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkmz_1_id).get())->getPointer();
		double* rk2mx = ((pdat::NodeData<double> *) patch->getPatchData(d_rk2mx_id).get())->getPointer();
		double* rk2my = ((pdat::NodeData<double> *) patch->getPatchData(d_rk2my_id).get())->getPointer();
		double* rk2mz = ((pdat::NodeData<double> *) patch->getPatchData(d_rk2mz_id).get())->getPointer();
		double* rho = ((pdat::NodeData<double> *) patch->getPatchData(d_rho_id).get())->getPointer();
		double* rho_p = ((pdat::NodeData<double> *) patch->getPatchData(d_rho_p_id).get())->getPointer();
		double* mx = ((pdat::NodeData<double> *) patch->getPatchData(d_mx_id).get())->getPointer();
		double* mx_p = ((pdat::NodeData<double> *) patch->getPatchData(d_mx_p_id).get())->getPointer();
		double* my = ((pdat::NodeData<double> *) patch->getPatchData(d_my_id).get())->getPointer();
		double* my_p = ((pdat::NodeData<double> *) patch->getPatchData(d_my_p_id).get())->getPointer();
		double* mz = ((pdat::NodeData<double> *) patch->getPatchData(d_mz_id).get())->getPointer();
		double* mz_p = ((pdat::NodeData<double> *) patch->getPatchData(d_mz_p_id).get())->getPointer();
		double RHS_Null, d_mx_o0_t0_m0_l0, d_my_o0_t0_m0_l0, d_mz_o0_t0_m0_l0, d_my_o0_t6_m0_l0, d_mx_o0_t4_m0_l0, d_mz_o0_t6_m0_l0, d_mx_o0_t5_m0_l0, d_my_o0_t3_m0_l0, d_mx_o0_t7_m0_l0, d_mx_o0_t1_m0_l0, d_my_o0_t1_m0_l0, d_mz_o0_t1_m0_l0, d_my_o0_t5_m0_l0, d_mz_o0_t3_m0_l0, d_mx_o0_t8_m0_l0, d_mz_o0_t4_m0_l0, d_my_o0_t8_m0_l0, d_mx_o0_t2_m0_l0, d_my_o0_t2_m0_l0, d_mz_o0_t2_m0_l0, RHS_rho, RHS_mx, RHS_my, RHS_mz, m_mz_o0_t6_l0, m_mz_o0_t5_l0, m_mz_o0_t4_l0, m_mz_o0_t3_l0, m_mz_o0_t2_l0, m_mz_o0_t1_l0, m_mz_o0_t0_l0, m_my_o0_t8_l0, m_my_o0_t6_l0, m_my_o0_t5_l0, m_my_o0_t4_l0, m_my_o0_t3_l0, m_my_o0_t2_l0, m_my_o0_t1_l0, m_my_o0_t0_l0, m_mx_o0_t8_l0, m_mx_o0_t7_l0, m_mx_o0_t5_l0, m_mx_o0_t4_l0, m_mx_o0_t3_l0, m_mx_o0_t2_l0, m_mx_o0_t1_l0, m_mx_o0_t0_l0, n_x, n_y, n_z, interaction_index, mod_normal, m_n_var, m_tx_var, m_ty_var, m_tz_var, m_n, m_tx, m_ty, m_tz, rk2cn, rk2c, fluxAccwnp_3, fluxAccwnm_3, fluxAccwtx_3, fluxAccwty_3, fluxAccwtz_3;
	
		//Get the dimensions of the patch
		const hier::Index boxfirst = patch->getBox().lower();
		const hier::Index boxlast = patch->getBox().upper();
	
		//Get delta spaces into an array. dx, dy, dz.
		std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
		const double* dx  = patch_geom->getDx();
	
		//Auxiliary definitions
		int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
		int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
		int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;
		for(int k = 0; k < klast; k++) {
			for(int j = 0; j < jlast; j++) {
				for(int i = 0; i < ilast; i++) {
					if ((vector(FOV_3, i, j, k) > 0)) {
						RHS_Null = 0.0;
						vector(Null, i, j, k) = RK3P3_(RHS_Null, Null_p, rk2Null, i, j, k, dx, simPlat_dt, ilast, jlast);
					}
					if ((vector(FOV_1, i, j, k) > 0) && (i + 2 < ilast && i - 2 >= 0 && j + 2 < jlast && j - 2 >= 0 && k + 2 < klast && k - 2 >= 0)) {
						d_mx_o0_t0_m0_l0 = centereddifferences4thorder_i(ad_mx_o0_t0_m0_l0_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						d_my_o0_t0_m0_l0 = centereddifferences4thordercrossed_ij(ad_mx_o0_t0_m0_l0_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						d_mz_o0_t0_m0_l0 = centereddifferences4thordercrossed_ik(ad_mx_o0_t0_m0_l0_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						d_my_o0_t6_m0_l0 = centereddifferences4thordercrossed_ji(ad_mx_o0_t0_m0_l0_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						d_mx_o0_t4_m0_l0 = centereddifferences4thorder_j(ad_mx_o0_t0_m0_l0_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						d_mz_o0_t6_m0_l0 = centereddifferences4thordercrossed_ki(ad_mx_o0_t0_m0_l0_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						d_mx_o0_t5_m0_l0 = centereddifferences4thorder_k(ad_mx_o0_t0_m0_l0_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						d_my_o0_t3_m0_l0 = centereddifferences4thorder_i(ad_my_o0_t3_m0_l0_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						d_mx_o0_t7_m0_l0 = centereddifferences4thordercrossed_ij(ad_my_o0_t3_m0_l0_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						d_mx_o0_t1_m0_l0 = centereddifferences4thordercrossed_ji(ad_my_o0_t3_m0_l0_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						d_my_o0_t1_m0_l0 = centereddifferences4thorder_j(ad_my_o0_t3_m0_l0_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						d_mz_o0_t1_m0_l0 = centereddifferences4thordercrossed_jk(ad_my_o0_t3_m0_l0_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						d_my_o0_t5_m0_l0 = centereddifferences4thorder_k(ad_my_o0_t3_m0_l0_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						d_mz_o0_t3_m0_l0 = centereddifferences4thorder_i(ad_mz_o0_t3_m0_l0_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						d_mx_o0_t8_m0_l0 = centereddifferences4thordercrossed_ik(ad_mz_o0_t3_m0_l0_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						d_mz_o0_t4_m0_l0 = centereddifferences4thorder_j(ad_mz_o0_t3_m0_l0_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						d_my_o0_t8_m0_l0 = centereddifferences4thordercrossed_jk(ad_mz_o0_t3_m0_l0_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						d_mx_o0_t2_m0_l0 = centereddifferences4thordercrossed_ki(ad_mz_o0_t3_m0_l0_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						d_my_o0_t2_m0_l0 = centereddifferences4thordercrossed_kj(ad_mz_o0_t3_m0_l0_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						d_mz_o0_t2_m0_l0 = centereddifferences4thorder_k(ad_mz_o0_t3_m0_l0_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						RHS_rho = 0.0;
						RHS_mx = vector(rk2rho, i, j, k) * vector(fx, i, j, k);
						RHS_my = vector(rk2rho, i, j, k) * vector(fy, i, j, k);
						RHS_mz = vector(rk2rho, i, j, k) * vector(fz, i, j, k);
						RHS_rho = RHS_rho - FDOC_i(extrapolatedSBrhoSPi, extrapolatedSBmxSPi, SpeedSBi_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						RHS_rho = RHS_rho - FDOC_j(extrapolatedSBrhoSPj, extrapolatedSBmySPj, SpeedSBj_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						RHS_rho = RHS_rho - FDOC_k(extrapolatedSBrhoSPk, extrapolatedSBmzSPk, SpeedSBk_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						RHS_mx = RHS_mx - FDOC_i(extrapolatedSBmxSPi, FluxSBimx_1, SpeedSBi_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						RHS_mx = RHS_mx - FDOC_j(extrapolatedSBmxSPj, FluxSBjmx_1, SpeedSBj_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						RHS_mx = RHS_mx - FDOC_k(extrapolatedSBmxSPk, FluxSBkmx_1, SpeedSBk_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						RHS_my = RHS_my - FDOC_i(extrapolatedSBmySPi, FluxSBimy_1, SpeedSBi_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						RHS_my = RHS_my - FDOC_j(extrapolatedSBmySPj, FluxSBjmy_1, SpeedSBj_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						RHS_my = RHS_my - FDOC_k(extrapolatedSBmySPk, FluxSBkmy_1, SpeedSBk_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						RHS_mz = RHS_mz - FDOC_i(extrapolatedSBmzSPi, FluxSBimz_1, SpeedSBi_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						RHS_mz = RHS_mz - FDOC_j(extrapolatedSBmzSPj, FluxSBjmz_1, SpeedSBj_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						RHS_mz = RHS_mz - FDOC_k(extrapolatedSBmzSPk, FluxSBkmz_1, SpeedSBk_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						m_mz_o0_t6_l0 = mu * d_mz_o0_t6_m0_l0;
						m_mz_o0_t5_l0 = mu * d_mz_o0_t2_m0_l0;
						m_mz_o0_t4_l0 = mu * d_mz_o0_t4_m0_l0;
						m_mz_o0_t3_l0 = mu * d_mz_o0_t3_m0_l0;
						m_mz_o0_t2_l0 = lambda * d_mz_o0_t2_m0_l0;
						m_mz_o0_t1_l0 = lambda * d_mz_o0_t1_m0_l0;
						m_mz_o0_t0_l0 = lambda * d_mz_o0_t0_m0_l0;
						m_my_o0_t8_l0 = mu * d_my_o0_t8_m0_l0;
						m_my_o0_t6_l0 = mu * d_my_o0_t6_m0_l0;
						m_my_o0_t5_l0 = mu * d_my_o0_t5_m0_l0;
						m_my_o0_t4_l0 = mu * d_my_o0_t1_m0_l0;
						m_my_o0_t3_l0 = mu * d_my_o0_t3_m0_l0;
						m_my_o0_t2_l0 = lambda * d_my_o0_t2_m0_l0;
						m_my_o0_t1_l0 = lambda * d_my_o0_t1_m0_l0;
						m_my_o0_t0_l0 = lambda * d_my_o0_t0_m0_l0;
						m_mx_o0_t8_l0 = mu * d_mx_o0_t8_m0_l0;
						m_mx_o0_t7_l0 = mu * d_mx_o0_t7_m0_l0;
						m_mx_o0_t5_l0 = mu * d_mx_o0_t5_m0_l0;
						m_mx_o0_t4_l0 = mu * d_mx_o0_t4_m0_l0;
						m_mx_o0_t3_l0 = mu * d_mx_o0_t0_m0_l0;
						m_mx_o0_t2_l0 = lambda * d_mx_o0_t2_m0_l0;
						m_mx_o0_t1_l0 = lambda * d_mx_o0_t1_m0_l0;
						m_mx_o0_t0_l0 = lambda * d_mx_o0_t0_m0_l0;
						RHS_mx = RHS_mx + ((((((((m_mx_o0_t0_l0 + m_mx_o0_t1_l0) + m_mx_o0_t2_l0) + m_mx_o0_t3_l0) + m_mx_o0_t4_l0) + m_mx_o0_t5_l0) + m_mx_o0_t3_l0) + m_mx_o0_t7_l0) + m_mx_o0_t8_l0);
						RHS_my = RHS_my + ((((((((m_my_o0_t0_l0 + m_my_o0_t1_l0) + m_my_o0_t2_l0) + m_my_o0_t3_l0) + m_my_o0_t4_l0) + m_my_o0_t5_l0) + m_my_o0_t6_l0) + m_my_o0_t4_l0) + m_my_o0_t8_l0);
						RHS_mz = RHS_mz + ((((((((m_mz_o0_t0_l0 + m_mz_o0_t1_l0) + m_mz_o0_t2_l0) + m_mz_o0_t3_l0) + m_mz_o0_t4_l0) + m_mz_o0_t5_l0) + m_mz_o0_t6_l0) + m_mx_o0_t7_l0) + m_mx_o0_t8_l0);
						n_x = 0.0;
						n_y = 0.0;
						n_z = 0.0;
						interaction_index = 0.0;
						if ((equalsEq(n_x, 0.0) && equalsEq(n_y, 0.0)) && equalsEq(n_z, 0.0)) {
							if (vector(FOV_3, i + 1, j, k) > 0.0) {
								interaction_index = 1.0;
								n_x = n_x + 1.0 / dx[0];
							}
							if (vector(FOV_3, i - 1, j, k) > 0.0) {
								interaction_index = 1.0;
								n_x = n_x - 1.0 / dx[0];
							}
							if (vector(FOV_3, i, j + 1, k) > 0.0) {
								interaction_index = 1.0;
								n_y = n_y + 1.0 / dx[1];
							}
							if (vector(FOV_3, i, j - 1, k) > 0.0) {
								interaction_index = 1.0;
								n_y = n_y - 1.0 / dx[1];
							}
							if (vector(FOV_3, i, j, k + 1) > 0.0) {
								interaction_index = 1.0;
								n_z = n_z + 1.0 / dx[2];
							}
							if (vector(FOV_3, i, j, k - 1) > 0.0) {
								interaction_index = 1.0;
								n_z = n_z - 1.0 / dx[2];
							}
						}
						if ((equalsEq(n_x, 0.0) && equalsEq(n_y, 0.0)) && equalsEq(n_z, 0.0)) {
							if (((((((((vector(FOV_3, i + 1, j + 1, k + 1) > 0.0) || (vector(FOV_3, i + 1, j + 1, k - 1) > 0.0)) || (vector(FOV_3, i + 1, j + 1, k) > 0.0)) || (vector(FOV_3, i + 1, j - 1, k + 1) > 0.0)) || (vector(FOV_3, i + 1, j - 1, k - 1) > 0.0)) || (vector(FOV_3, i + 1, j - 1, k) > 0.0)) || (vector(FOV_3, i + 1, j, k + 1) > 0.0)) || (vector(FOV_3, i + 1, j, k - 1) > 0.0)) || (vector(FOV_3, i + 1, j, k) > 0.0)) {
								interaction_index = 1.0;
								n_x = n_x + 1.0 / dx[0];
							}
							if (((((((((vector(FOV_3, i - 1, j + 1, k + 1) > 0.0) || (vector(FOV_3, i - 1, j + 1, k - 1) > 0.0)) || (vector(FOV_3, i - 1, j + 1, k) > 0.0)) || (vector(FOV_3, i - 1, j - 1, k + 1) > 0.0)) || (vector(FOV_3, i - 1, j - 1, k - 1) > 0.0)) || (vector(FOV_3, i - 1, j - 1, k) > 0.0)) || (vector(FOV_3, i - 1, j, k + 1) > 0.0)) || (vector(FOV_3, i - 1, j, k - 1) > 0.0)) || (vector(FOV_3, i - 1, j, k) > 0.0)) {
								interaction_index = 1.0;
								n_x = n_x - 1.0 / dx[0];
							}
							if (((((((((vector(FOV_3, i + 1, j + 1, k + 1) > 0.0) || (vector(FOV_3, i + 1, j + 1, k - 1) > 0.0)) || (vector(FOV_3, i + 1, j + 1, k) > 0.0)) || (vector(FOV_3, i - 1, j + 1, k + 1) > 0.0)) || (vector(FOV_3, i - 1, j + 1, k - 1) > 0.0)) || (vector(FOV_3, i - 1, j + 1, k) > 0.0)) || (vector(FOV_3, i, j + 1, k + 1) > 0.0)) || (vector(FOV_3, i, j + 1, k - 1) > 0.0)) || (vector(FOV_3, i, j + 1, k) > 0.0)) {
								interaction_index = 1.0;
								n_y = n_y + 1.0 / dx[1];
							}
							if (((((((((vector(FOV_3, i + 1, j - 1, k + 1) > 0.0) || (vector(FOV_3, i + 1, j - 1, k - 1) > 0.0)) || (vector(FOV_3, i + 1, j - 1, k) > 0.0)) || (vector(FOV_3, i - 1, j - 1, k + 1) > 0.0)) || (vector(FOV_3, i - 1, j - 1, k - 1) > 0.0)) || (vector(FOV_3, i - 1, j - 1, k) > 0.0)) || (vector(FOV_3, i, j - 1, k + 1) > 0.0)) || (vector(FOV_3, i, j - 1, k - 1) > 0.0)) || (vector(FOV_3, i, j - 1, k) > 0.0)) {
								interaction_index = 1.0;
								n_y = n_y - 1.0 / dx[1];
							}
							if (((((((((vector(FOV_3, i + 1, j + 1, k + 1) > 0.0) || (vector(FOV_3, i + 1, j - 1, k + 1) > 0.0)) || (vector(FOV_3, i + 1, j, k + 1) > 0.0)) || (vector(FOV_3, i - 1, j + 1, k + 1) > 0.0)) || (vector(FOV_3, i - 1, j - 1, k + 1) > 0.0)) || (vector(FOV_3, i - 1, j, k + 1) > 0.0)) || (vector(FOV_3, i, j + 1, k + 1) > 0.0)) || (vector(FOV_3, i, j - 1, k + 1) > 0.0)) || (vector(FOV_3, i, j, k + 1) > 0.0)) {
								interaction_index = 1.0;
								n_z = n_z + 1.0 / dx[2];
							}
							if (((((((((vector(FOV_3, i + 1, j + 1, k - 1) > 0.0) || (vector(FOV_3, i + 1, j - 1, k - 1) > 0.0)) || (vector(FOV_3, i + 1, j, k - 1) > 0.0)) || (vector(FOV_3, i - 1, j + 1, k - 1) > 0.0)) || (vector(FOV_3, i - 1, j - 1, k - 1) > 0.0)) || (vector(FOV_3, i - 1, j, k - 1) > 0.0)) || (vector(FOV_3, i, j + 1, k - 1) > 0.0)) || (vector(FOV_3, i, j - 1, k - 1) > 0.0)) || (vector(FOV_3, i, j, k - 1) > 0.0)) {
								interaction_index = 1.0;
								n_z = n_z - 1.0 / dx[2];
							}
						}
						if ((equalsEq(n_x, 0.0) && equalsEq(n_y, 0.0)) && equalsEq(n_z, 0.0)) {
							if (vector(FOV_xLower, i + 1, j, k) > 0.0) {
								interaction_index = 2.0;
								n_x = n_x + 1.0 / dx[0];
							}
							if (vector(FOV_xLower, i - 1, j, k) > 0.0) {
								interaction_index = 2.0;
								n_x = n_x - 1.0 / dx[0];
							}
							if (vector(FOV_xLower, i, j + 1, k) > 0.0) {
								interaction_index = 2.0;
								n_y = n_y + 1.0 / dx[1];
							}
							if (vector(FOV_xLower, i, j - 1, k) > 0.0) {
								interaction_index = 2.0;
								n_y = n_y - 1.0 / dx[1];
							}
							if (vector(FOV_xLower, i, j, k + 1) > 0.0) {
								interaction_index = 2.0;
								n_z = n_z + 1.0 / dx[2];
							}
							if (vector(FOV_xLower, i, j, k - 1) > 0.0) {
								interaction_index = 2.0;
								n_z = n_z - 1.0 / dx[2];
							}
						}
						if ((equalsEq(n_x, 0.0) && equalsEq(n_y, 0.0)) && equalsEq(n_z, 0.0)) {
							if (((((((((vector(FOV_xLower, i + 1, j + 1, k + 1) > 0.0) || (vector(FOV_xLower, i + 1, j + 1, k - 1) > 0.0)) || (vector(FOV_xLower, i + 1, j + 1, k) > 0.0)) || (vector(FOV_xLower, i + 1, j - 1, k + 1) > 0.0)) || (vector(FOV_xLower, i + 1, j - 1, k - 1) > 0.0)) || (vector(FOV_xLower, i + 1, j - 1, k) > 0.0)) || (vector(FOV_xLower, i + 1, j, k + 1) > 0.0)) || (vector(FOV_xLower, i + 1, j, k - 1) > 0.0)) || (vector(FOV_xLower, i + 1, j, k) > 0.0)) {
								interaction_index = 2.0;
								n_x = n_x + 1.0 / dx[0];
							}
							if (((((((((vector(FOV_xLower, i - 1, j + 1, k + 1) > 0.0) || (vector(FOV_xLower, i - 1, j + 1, k - 1) > 0.0)) || (vector(FOV_xLower, i - 1, j + 1, k) > 0.0)) || (vector(FOV_xLower, i - 1, j - 1, k + 1) > 0.0)) || (vector(FOV_xLower, i - 1, j - 1, k - 1) > 0.0)) || (vector(FOV_xLower, i - 1, j - 1, k) > 0.0)) || (vector(FOV_xLower, i - 1, j, k + 1) > 0.0)) || (vector(FOV_xLower, i - 1, j, k - 1) > 0.0)) || (vector(FOV_xLower, i - 1, j, k) > 0.0)) {
								interaction_index = 2.0;
								n_x = n_x - 1.0 / dx[0];
							}
							if (((((((((vector(FOV_xLower, i + 1, j + 1, k + 1) > 0.0) || (vector(FOV_xLower, i + 1, j + 1, k - 1) > 0.0)) || (vector(FOV_xLower, i + 1, j + 1, k) > 0.0)) || (vector(FOV_xLower, i - 1, j + 1, k + 1) > 0.0)) || (vector(FOV_xLower, i - 1, j + 1, k - 1) > 0.0)) || (vector(FOV_xLower, i - 1, j + 1, k) > 0.0)) || (vector(FOV_xLower, i, j + 1, k + 1) > 0.0)) || (vector(FOV_xLower, i, j + 1, k - 1) > 0.0)) || (vector(FOV_xLower, i, j + 1, k) > 0.0)) {
								interaction_index = 2.0;
								n_y = n_y + 1.0 / dx[1];
							}
							if (((((((((vector(FOV_xLower, i + 1, j - 1, k + 1) > 0.0) || (vector(FOV_xLower, i + 1, j - 1, k - 1) > 0.0)) || (vector(FOV_xLower, i + 1, j - 1, k) > 0.0)) || (vector(FOV_xLower, i - 1, j - 1, k + 1) > 0.0)) || (vector(FOV_xLower, i - 1, j - 1, k - 1) > 0.0)) || (vector(FOV_xLower, i - 1, j - 1, k) > 0.0)) || (vector(FOV_xLower, i, j - 1, k + 1) > 0.0)) || (vector(FOV_xLower, i, j - 1, k - 1) > 0.0)) || (vector(FOV_xLower, i, j - 1, k) > 0.0)) {
								interaction_index = 2.0;
								n_y = n_y - 1.0 / dx[1];
							}
							if (((((((((vector(FOV_xLower, i + 1, j + 1, k + 1) > 0.0) || (vector(FOV_xLower, i + 1, j - 1, k + 1) > 0.0)) || (vector(FOV_xLower, i + 1, j, k + 1) > 0.0)) || (vector(FOV_xLower, i - 1, j + 1, k + 1) > 0.0)) || (vector(FOV_xLower, i - 1, j - 1, k + 1) > 0.0)) || (vector(FOV_xLower, i - 1, j, k + 1) > 0.0)) || (vector(FOV_xLower, i, j + 1, k + 1) > 0.0)) || (vector(FOV_xLower, i, j - 1, k + 1) > 0.0)) || (vector(FOV_xLower, i, j, k + 1) > 0.0)) {
								interaction_index = 2.0;
								n_z = n_z + 1.0 / dx[2];
							}
							if (((((((((vector(FOV_xLower, i + 1, j + 1, k - 1) > 0.0) || (vector(FOV_xLower, i + 1, j - 1, k - 1) > 0.0)) || (vector(FOV_xLower, i + 1, j, k - 1) > 0.0)) || (vector(FOV_xLower, i - 1, j + 1, k - 1) > 0.0)) || (vector(FOV_xLower, i - 1, j - 1, k - 1) > 0.0)) || (vector(FOV_xLower, i - 1, j, k - 1) > 0.0)) || (vector(FOV_xLower, i, j + 1, k - 1) > 0.0)) || (vector(FOV_xLower, i, j - 1, k - 1) > 0.0)) || (vector(FOV_xLower, i, j, k - 1) > 0.0)) {
								interaction_index = 2.0;
								n_z = n_z - 1.0 / dx[2];
							}
						}
						if ((equalsEq(n_x, 0.0) && equalsEq(n_y, 0.0)) && equalsEq(n_z, 0.0)) {
							if (vector(FOV_xUpper, i + 1, j, k) > 0.0) {
								interaction_index = 3.0;
								n_x = n_x + 1.0 / dx[0];
							}
							if (vector(FOV_xUpper, i - 1, j, k) > 0.0) {
								interaction_index = 3.0;
								n_x = n_x - 1.0 / dx[0];
							}
							if (vector(FOV_xUpper, i, j + 1, k) > 0.0) {
								interaction_index = 3.0;
								n_y = n_y + 1.0 / dx[1];
							}
							if (vector(FOV_xUpper, i, j - 1, k) > 0.0) {
								interaction_index = 3.0;
								n_y = n_y - 1.0 / dx[1];
							}
							if (vector(FOV_xUpper, i, j, k + 1) > 0.0) {
								interaction_index = 3.0;
								n_z = n_z + 1.0 / dx[2];
							}
							if (vector(FOV_xUpper, i, j, k - 1) > 0.0) {
								interaction_index = 3.0;
								n_z = n_z - 1.0 / dx[2];
							}
						}
						if ((equalsEq(n_x, 0.0) && equalsEq(n_y, 0.0)) && equalsEq(n_z, 0.0)) {
							if (((((((((vector(FOV_xUpper, i + 1, j + 1, k + 1) > 0.0) || (vector(FOV_xUpper, i + 1, j + 1, k - 1) > 0.0)) || (vector(FOV_xUpper, i + 1, j + 1, k) > 0.0)) || (vector(FOV_xUpper, i + 1, j - 1, k + 1) > 0.0)) || (vector(FOV_xUpper, i + 1, j - 1, k - 1) > 0.0)) || (vector(FOV_xUpper, i + 1, j - 1, k) > 0.0)) || (vector(FOV_xUpper, i + 1, j, k + 1) > 0.0)) || (vector(FOV_xUpper, i + 1, j, k - 1) > 0.0)) || (vector(FOV_xUpper, i + 1, j, k) > 0.0)) {
								interaction_index = 3.0;
								n_x = n_x + 1.0 / dx[0];
							}
							if (((((((((vector(FOV_xUpper, i - 1, j + 1, k + 1) > 0.0) || (vector(FOV_xUpper, i - 1, j + 1, k - 1) > 0.0)) || (vector(FOV_xUpper, i - 1, j + 1, k) > 0.0)) || (vector(FOV_xUpper, i - 1, j - 1, k + 1) > 0.0)) || (vector(FOV_xUpper, i - 1, j - 1, k - 1) > 0.0)) || (vector(FOV_xUpper, i - 1, j - 1, k) > 0.0)) || (vector(FOV_xUpper, i - 1, j, k + 1) > 0.0)) || (vector(FOV_xUpper, i - 1, j, k - 1) > 0.0)) || (vector(FOV_xUpper, i - 1, j, k) > 0.0)) {
								interaction_index = 3.0;
								n_x = n_x - 1.0 / dx[0];
							}
							if (((((((((vector(FOV_xUpper, i + 1, j + 1, k + 1) > 0.0) || (vector(FOV_xUpper, i + 1, j + 1, k - 1) > 0.0)) || (vector(FOV_xUpper, i + 1, j + 1, k) > 0.0)) || (vector(FOV_xUpper, i - 1, j + 1, k + 1) > 0.0)) || (vector(FOV_xUpper, i - 1, j + 1, k - 1) > 0.0)) || (vector(FOV_xUpper, i - 1, j + 1, k) > 0.0)) || (vector(FOV_xUpper, i, j + 1, k + 1) > 0.0)) || (vector(FOV_xUpper, i, j + 1, k - 1) > 0.0)) || (vector(FOV_xUpper, i, j + 1, k) > 0.0)) {
								interaction_index = 3.0;
								n_y = n_y + 1.0 / dx[1];
							}
							if (((((((((vector(FOV_xUpper, i + 1, j - 1, k + 1) > 0.0) || (vector(FOV_xUpper, i + 1, j - 1, k - 1) > 0.0)) || (vector(FOV_xUpper, i + 1, j - 1, k) > 0.0)) || (vector(FOV_xUpper, i - 1, j - 1, k + 1) > 0.0)) || (vector(FOV_xUpper, i - 1, j - 1, k - 1) > 0.0)) || (vector(FOV_xUpper, i - 1, j - 1, k) > 0.0)) || (vector(FOV_xUpper, i, j - 1, k + 1) > 0.0)) || (vector(FOV_xUpper, i, j - 1, k - 1) > 0.0)) || (vector(FOV_xUpper, i, j - 1, k) > 0.0)) {
								interaction_index = 3.0;
								n_y = n_y - 1.0 / dx[1];
							}
							if (((((((((vector(FOV_xUpper, i + 1, j + 1, k + 1) > 0.0) || (vector(FOV_xUpper, i + 1, j - 1, k + 1) > 0.0)) || (vector(FOV_xUpper, i + 1, j, k + 1) > 0.0)) || (vector(FOV_xUpper, i - 1, j + 1, k + 1) > 0.0)) || (vector(FOV_xUpper, i - 1, j - 1, k + 1) > 0.0)) || (vector(FOV_xUpper, i - 1, j, k + 1) > 0.0)) || (vector(FOV_xUpper, i, j + 1, k + 1) > 0.0)) || (vector(FOV_xUpper, i, j - 1, k + 1) > 0.0)) || (vector(FOV_xUpper, i, j, k + 1) > 0.0)) {
								interaction_index = 3.0;
								n_z = n_z + 1.0 / dx[2];
							}
							if (((((((((vector(FOV_xUpper, i + 1, j + 1, k - 1) > 0.0) || (vector(FOV_xUpper, i + 1, j - 1, k - 1) > 0.0)) || (vector(FOV_xUpper, i + 1, j, k - 1) > 0.0)) || (vector(FOV_xUpper, i - 1, j + 1, k - 1) > 0.0)) || (vector(FOV_xUpper, i - 1, j - 1, k - 1) > 0.0)) || (vector(FOV_xUpper, i - 1, j, k - 1) > 0.0)) || (vector(FOV_xUpper, i, j + 1, k - 1) > 0.0)) || (vector(FOV_xUpper, i, j - 1, k - 1) > 0.0)) || (vector(FOV_xUpper, i, j, k - 1) > 0.0)) {
								interaction_index = 3.0;
								n_z = n_z - 1.0 / dx[2];
							}
						}
						if ((!equalsEq(n_x, 0.0) || !equalsEq(n_y, 0.0)) || !equalsEq(n_z, 0.0)) {
							mod_normal = sqrt((n_x * n_x + n_y * n_y) + n_z * n_z);
							n_x = n_x / mod_normal;
							n_y = n_y / mod_normal;
							n_z = n_z / mod_normal;
						}
						if (equalsEq(interaction_index, 1.0)) {
							m_n_var = RHS_mx * n_x + RHS_my * n_y + RHS_mz * n_z;
							m_tx_var = RHS_mx - (RHS_mx * n_x + RHS_my * n_y + RHS_mz * n_z) * n_x;
							m_ty_var = RHS_my - (RHS_mx * n_x + RHS_my * n_y + RHS_mz * n_z) * n_y;
							m_tz_var = RHS_mz - (RHS_mx * n_x + RHS_my * n_y + RHS_mz * n_z) * n_z;
							m_n = vector(rk2mx, i, j, k) * n_x + vector(rk2my, i, j, k) * n_y + vector(rk2mz, i, j, k) * n_z;
							m_tx = vector(rk2mx, i, j, k) - (vector(rk2mx, i, j, k) * n_x + vector(rk2my, i, j, k) * n_y + vector(rk2mz, i, j, k) * n_z) * n_x;
							m_ty = vector(rk2my, i, j, k) - (vector(rk2mx, i, j, k) * n_x + vector(rk2my, i, j, k) * n_y + vector(rk2mz, i, j, k) * n_z) * n_y;
							m_tz = vector(rk2mz, i, j, k) - (vector(rk2mx, i, j, k) * n_x + vector(rk2my, i, j, k) * n_y + vector(rk2mz, i, j, k) * n_z) * n_z;
							rk2cn = m_n / vector(rk2rho, i, j, k);
							rk2c = c0 * (pow((vector(rk2rho, i, j, k) / rho0), ((gamma + (-1.0)) / 2.0)));
							if ((rk2c + rk2cn) > 0.0) {
								fluxAccwnp_3 = RHS_rho * (-((-rk2c) + rk2cn)) + m_n_var;
							} else {
								fluxAccwnp_3 = (-m_n_var) + RHS_rho * (-((-rk2c) + rk2cn));
							}
							if (((-rk2c) + rk2cn) > 0.0) {
								fluxAccwnm_3 = RHS_rho * (-(rk2c + rk2cn)) + m_n_var;
							} else {
								fluxAccwnm_3 = (-m_n_var) + RHS_rho * (-(rk2c + rk2cn));
							}
							if (rk2cn > 0.0) {
								fluxAccwtx_3 = RHS_rho * (-m_tx) / vector(rk2rho, i, j, k) + m_tx_var;
							} else {
								fluxAccwtx_3 = RHS_rho * (-m_tx) / vector(rk2rho, i, j, k);
							}
							if (rk2cn > 0.0) {
								fluxAccwty_3 = RHS_rho * (-m_ty) / vector(rk2rho, i, j, k) + m_ty_var;
							} else {
								fluxAccwty_3 = RHS_rho * (-m_ty) / vector(rk2rho, i, j, k);
							}
							if (rk2cn > 0.0) {
								fluxAccwtz_3 = RHS_rho * (-m_tz) / vector(rk2rho, i, j, k) + m_tz_var;
							} else {
								fluxAccwtz_3 = RHS_rho * (-m_tz) / vector(rk2rho, i, j, k);
							}
							RHS_rho = fluxAccwnp_3 * 1.0 / (2.0 * rk2c) + fluxAccwnm_3 * (-1.0) / (2.0 * rk2c);
							m_n_var = fluxAccwnp_3 * (rk2cn / (2.0 * rk2c) + 1.0 / 2.0) + fluxAccwnm_3 * (-(rk2cn / (2.0 * rk2c) + (-1.0 / 2.0)));
							m_tx_var = (fluxAccwnp_3 * m_tx / (2.0 * vector(rk2rho, i, j, k) * rk2c) + fluxAccwnm_3 * (-m_tx) / (2.0 * vector(rk2rho, i, j, k) * rk2c)) + fluxAccwtx_3;
							m_ty_var = (fluxAccwnp_3 * m_ty / (2.0 * vector(rk2rho, i, j, k) * rk2c) + fluxAccwnm_3 * (-m_ty) / (2.0 * vector(rk2rho, i, j, k) * rk2c)) + fluxAccwty_3;
							m_tz_var = (fluxAccwnp_3 * m_tz / (2.0 * vector(rk2rho, i, j, k) * rk2c) + fluxAccwnm_3 * (-m_tz) / (2.0 * vector(rk2rho, i, j, k) * rk2c)) + fluxAccwtz_3;
							RHS_mx = m_tx_var + m_n_var * n_x;
							RHS_my = m_ty_var + m_n_var * n_y;
							RHS_mz = m_tz_var + m_n_var * n_z;
						}
						if (equalsEq(interaction_index, 2.0)) {
							m_n_var = RHS_mx * n_x + RHS_my * n_y + RHS_mz * n_z;
							m_tx_var = RHS_mx - (RHS_mx * n_x + RHS_my * n_y + RHS_mz * n_z) * n_x;
							m_ty_var = RHS_my - (RHS_mx * n_x + RHS_my * n_y + RHS_mz * n_z) * n_y;
							m_tz_var = RHS_mz - (RHS_mx * n_x + RHS_my * n_y + RHS_mz * n_z) * n_z;
							m_n = vector(rk2mx, i, j, k) * n_x + vector(rk2my, i, j, k) * n_y + vector(rk2mz, i, j, k) * n_z;
							m_tx = vector(rk2mx, i, j, k) - (vector(rk2mx, i, j, k) * n_x + vector(rk2my, i, j, k) * n_y + vector(rk2mz, i, j, k) * n_z) * n_x;
							m_ty = vector(rk2my, i, j, k) - (vector(rk2mx, i, j, k) * n_x + vector(rk2my, i, j, k) * n_y + vector(rk2mz, i, j, k) * n_z) * n_y;
							m_tz = vector(rk2mz, i, j, k) - (vector(rk2mx, i, j, k) * n_x + vector(rk2my, i, j, k) * n_y + vector(rk2mz, i, j, k) * n_z) * n_z;
							rk2cn = m_n / vector(rk2rho, i, j, k);
							rk2c = c0 * (pow((vector(rk2rho, i, j, k) / rho0), ((gamma + (-1.0)) / 2.0)));
							if ((rk2c + rk2cn) > 0.0) {
								fluxAccwnp_3 = RHS_rho * (-((-rk2c) + rk2cn)) + m_n_var;
							} else {
								fluxAccwnp_3 = (-lambdain) * (m_n + vector(rk2rho, i, j, k) * v0 * (1.0 - (ycoord(j) * ycoord(j) + zcoord(k) * zcoord(k)) / ((floor((0.006 * fabs(ycoord(j))) / (sqrt(ycoord(j) * ycoord(j) + zcoord(k) * zcoord(k)) * dx[1]) + 0.5) * dx[1]) * (floor((0.006 * fabs(ycoord(j))) / (sqrt(ycoord(j) * ycoord(j) + zcoord(k) * zcoord(k)) * dx[1]) + 0.5) * dx[1]) + (floor((0.006 * fabs(zcoord(k))) / (sqrt(ycoord(j) * ycoord(j) + zcoord(k) * zcoord(k)) * dx[2]) + 0.5) * dx[2]) * (floor((0.006 * fabs(zcoord(k))) / (sqrt(ycoord(j) * ycoord(j) + zcoord(k) * zcoord(k)) * dx[2]) + 0.5) * dx[2]))));
							}
							if (((-rk2c) + rk2cn) > 0.0) {
								fluxAccwnm_3 = RHS_rho * (-(rk2c + rk2cn)) + m_n_var;
							} else {
								fluxAccwnm_3 = (-lambdain) * (m_n + vector(rk2rho, i, j, k) * v0 * (1.0 - (ycoord(j) * ycoord(j) + zcoord(k) * zcoord(k)) / ((floor((0.006 * fabs(ycoord(j))) / (sqrt(ycoord(j) * ycoord(j) + zcoord(k) * zcoord(k)) * dx[1]) + 0.5) * dx[1]) * (floor((0.006 * fabs(ycoord(j))) / (sqrt(ycoord(j) * ycoord(j) + zcoord(k) * zcoord(k)) * dx[1]) + 0.5) * dx[1]) + (floor((0.006 * fabs(zcoord(k))) / (sqrt(ycoord(j) * ycoord(j) + zcoord(k) * zcoord(k)) * dx[2]) + 0.5) * dx[2]) * (floor((0.006 * fabs(zcoord(k))) / (sqrt(ycoord(j) * ycoord(j) + zcoord(k) * zcoord(k)) * dx[2]) + 0.5) * dx[2]))));
							}
							if (rk2cn > 0.0) {
								fluxAccwtx_3 = RHS_rho * (-m_tx) / vector(rk2rho, i, j, k) + m_tx_var;
							} else {
								fluxAccwtx_3 = (-lambdain) * (m_tx - vector(rk2rho, i, j, k) * vt0);
							}
							if (rk2cn > 0.0) {
								fluxAccwty_3 = RHS_rho * (-m_ty) / vector(rk2rho, i, j, k) + m_ty_var;
							} else {
								fluxAccwty_3 = (-lambdain) * (m_ty - vector(rk2rho, i, j, k) * vt0);
							}
							if (rk2cn > 0.0) {
								fluxAccwtz_3 = RHS_rho * (-m_tz) / vector(rk2rho, i, j, k) + m_tz_var;
							} else {
								fluxAccwtz_3 = (-lambdain) * (m_tz - vector(rk2rho, i, j, k) * vt0);
							}
							RHS_rho = fluxAccwnp_3 * 1.0 / (2.0 * rk2c) + fluxAccwnm_3 * (-1.0) / (2.0 * rk2c);
							m_n_var = fluxAccwnp_3 * (rk2cn / (2.0 * rk2c) + 1.0 / 2.0) + fluxAccwnm_3 * (-(rk2cn / (2.0 * rk2c) + (-1.0 / 2.0)));
							m_tx_var = (fluxAccwnp_3 * m_tx / (2.0 * vector(rk2rho, i, j, k) * rk2c) + fluxAccwnm_3 * (-m_tx) / (2.0 * vector(rk2rho, i, j, k) * rk2c)) + fluxAccwtx_3;
							m_ty_var = (fluxAccwnp_3 * m_ty / (2.0 * vector(rk2rho, i, j, k) * rk2c) + fluxAccwnm_3 * (-m_ty) / (2.0 * vector(rk2rho, i, j, k) * rk2c)) + fluxAccwty_3;
							m_tz_var = (fluxAccwnp_3 * m_tz / (2.0 * vector(rk2rho, i, j, k) * rk2c) + fluxAccwnm_3 * (-m_tz) / (2.0 * vector(rk2rho, i, j, k) * rk2c)) + fluxAccwtz_3;
							RHS_mx = m_tx_var + m_n_var * n_x;
							RHS_my = m_ty_var + m_n_var * n_y;
							RHS_mz = m_tz_var + m_n_var * n_z;
						}
						if (equalsEq(interaction_index, 3.0)) {
							m_n_var = RHS_mx * n_x + RHS_my * n_y + RHS_mz * n_z;
							m_tx_var = RHS_mx - (RHS_mx * n_x + RHS_my * n_y + RHS_mz * n_z) * n_x;
							m_ty_var = RHS_my - (RHS_mx * n_x + RHS_my * n_y + RHS_mz * n_z) * n_y;
							m_tz_var = RHS_mz - (RHS_mx * n_x + RHS_my * n_y + RHS_mz * n_z) * n_z;
							m_n = vector(rk2mx, i, j, k) * n_x + vector(rk2my, i, j, k) * n_y + vector(rk2mz, i, j, k) * n_z;
							m_tx = vector(rk2mx, i, j, k) - (vector(rk2mx, i, j, k) * n_x + vector(rk2my, i, j, k) * n_y + vector(rk2mz, i, j, k) * n_z) * n_x;
							m_ty = vector(rk2my, i, j, k) - (vector(rk2mx, i, j, k) * n_x + vector(rk2my, i, j, k) * n_y + vector(rk2mz, i, j, k) * n_z) * n_y;
							m_tz = vector(rk2mz, i, j, k) - (vector(rk2mx, i, j, k) * n_x + vector(rk2my, i, j, k) * n_y + vector(rk2mz, i, j, k) * n_z) * n_z;
							rk2cn = m_n / vector(rk2rho, i, j, k);
							rk2c = c0 * (pow((vector(rk2rho, i, j, k) / rho0), ((gamma + (-1.0)) / 2.0)));
							if ((rk2c + rk2cn) > 0.0) {
								fluxAccwnp_3 = RHS_rho * (-((-rk2c) + rk2cn)) + m_n_var;
							} else {
								fluxAccwnp_3 = ((-rk2c) + rk2cn) * lambdaout * (vector(rk2rho, i, j, k) - rho0);
							}
							if (((-rk2c) + rk2cn) > 0.0) {
								fluxAccwnm_3 = RHS_rho * (-(rk2c + rk2cn)) + m_n_var;
							} else {
								fluxAccwnm_3 = (rk2c + rk2cn) * lambdaout * (vector(rk2rho, i, j, k) - rho0);
							}
							if (rk2cn > 0.0) {
								fluxAccwtx_3 = RHS_rho * (-m_tx) / vector(rk2rho, i, j, k) + m_tx_var;
							} else {
								fluxAccwtx_3 = m_tx / vector(rk2rho, i, j, k) * lambdaout * (vector(rk2rho, i, j, k) - rho0);
							}
							if (rk2cn > 0.0) {
								fluxAccwty_3 = RHS_rho * (-m_ty) / vector(rk2rho, i, j, k) + m_ty_var;
							} else {
								fluxAccwty_3 = m_ty / vector(rk2rho, i, j, k) * lambdaout * (vector(rk2rho, i, j, k) - rho0);
							}
							if (rk2cn > 0.0) {
								fluxAccwtz_3 = RHS_rho * (-m_tz) / vector(rk2rho, i, j, k) + m_tz_var;
							} else {
								fluxAccwtz_3 = m_tz / vector(rk2rho, i, j, k) * lambdaout * (vector(rk2rho, i, j, k) - rho0);
							}
							RHS_rho = fluxAccwnp_3 * 1.0 / (2.0 * rk2c) + fluxAccwnm_3 * (-1.0) / (2.0 * rk2c);
							m_n_var = fluxAccwnp_3 * (rk2cn / (2.0 * rk2c) + 1.0 / 2.0) + fluxAccwnm_3 * (-(rk2cn / (2.0 * rk2c) + (-1.0 / 2.0)));
							m_tx_var = (fluxAccwnp_3 * m_tx / (2.0 * vector(rk2rho, i, j, k) * rk2c) + fluxAccwnm_3 * (-m_tx) / (2.0 * vector(rk2rho, i, j, k) * rk2c)) + fluxAccwtx_3;
							m_ty_var = (fluxAccwnp_3 * m_ty / (2.0 * vector(rk2rho, i, j, k) * rk2c) + fluxAccwnm_3 * (-m_ty) / (2.0 * vector(rk2rho, i, j, k) * rk2c)) + fluxAccwty_3;
							m_tz_var = (fluxAccwnp_3 * m_tz / (2.0 * vector(rk2rho, i, j, k) * rk2c) + fluxAccwnm_3 * (-m_tz) / (2.0 * vector(rk2rho, i, j, k) * rk2c)) + fluxAccwtz_3;
							RHS_mx = m_tx_var + m_n_var * n_x;
							RHS_my = m_ty_var + m_n_var * n_y;
							RHS_mz = m_tz_var + m_n_var * n_z;
						}
						vector(rho, i, j, k) = RK3P3_(RHS_rho, rho_p, rk2rho, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(mx, i, j, k) = RK3P3_(RHS_mx, mx_p, rk2mx, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(my, i, j, k) = RK3P3_(RHS_my, my_p, rk2my, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(mz, i, j, k) = RK3P3_(RHS_mz, mz_p, rk2mz, i, j, k, dx, simPlat_dt, ilast, jlast);
					}
				}
			}
		}
	}
	//Fill ghosts and periodical boundaries
	time_interpolate_operator_mesh1->setStep(3);
	d_bdry_sched_advance19[ln]->fillData(current_time + simPlat_dt, false);
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch >& patch = *p_it;
		double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_3 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_3_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
		double* FOV_zLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zLower_id).get())->getPointer();
		double* FOV_zUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zUpper_id).get())->getPointer();
	
		double* fz = ((pdat::NodeData<double> *) patch->getPatchData(d_fz_id).get())->getPointer();
		double* fy = ((pdat::NodeData<double> *) patch->getPatchData(d_fy_id).get())->getPointer();
		double* fx = ((pdat::NodeData<double> *) patch->getPatchData(d_fx_id).get())->getPointer();
		double* p = ((pdat::NodeData<double> *) patch->getPatchData(d_p_id).get())->getPointer();
		double* rho = ((pdat::NodeData<double> *) patch->getPatchData(d_rho_id).get())->getPointer();
	
		//Get the dimensions of the patch
		const hier::Index boxfirst = patch->getBox().lower();
		const hier::Index boxlast = patch->getBox().upper();
	
		//Get delta spaces into an array. dx, dy, dz.
		std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
		const double* dx  = patch_geom->getDx();
	
		//Auxiliary definitions
		int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
		int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
		int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;
		for(int k = 0; k < klast; k++) {
			for(int j = 0; j < jlast; j++) {
				for(int i = 0; i < ilast; i++) {
					if ((vector(FOV_1, i, j, k) > 0) && ((i + 2 < ilast || !patch->getPatchGeometry()->getTouchesRegularBoundary(0, 1)) && (i - 2 >= 0 || !patch->getPatchGeometry()->getTouchesRegularBoundary(0, 0)) && (j + 2 < jlast || !patch->getPatchGeometry()->getTouchesRegularBoundary(1, 1)) && (j - 2 >= 0 || !patch->getPatchGeometry()->getTouchesRegularBoundary(1, 0)) && (k + 2 < klast || !patch->getPatchGeometry()->getTouchesRegularBoundary(2, 1)) && (k - 2 >= 0 || !patch->getPatchGeometry()->getTouchesRegularBoundary(2, 0)))) {
						vector(fz, i, j, k) = pfz;
						vector(fy, i, j, k) = pfy;
						vector(fx, i, j, k) = pfx;
						vector(p, i, j, k) = (Paux + (pow((vector(rho, i, j, k) / rho0), gamma)) * (rho0 * (c0 * c0)) / gamma) - (rho0 * (c0 * c0)) / gamma;
					}
				}
			}
		}
	}
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch >& patch = *p_it;
		double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_3 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_3_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
		double* FOV_zLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zLower_id).get())->getPointer();
		double* FOV_zUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zUpper_id).get())->getPointer();
	
		//Hard region field distance variables
		double* d_i_rho_mx_my_mz_p_fx_fy_fz = ((pdat::NodeData<double> *) patch->getPatchData(d_d_i_rho_mx_my_mz_p_fx_fy_fz_id).get())->getPointer();
		double* d_j_rho_mx_my_mz_p_fx_fy_fz = ((pdat::NodeData<double> *) patch->getPatchData(d_d_j_rho_mx_my_mz_p_fx_fy_fz_id).get())->getPointer();
		double* d_k_rho_mx_my_mz_p_fx_fy_fz = ((pdat::NodeData<double> *) patch->getPatchData(d_d_k_rho_mx_my_mz_p_fx_fy_fz_id).get())->getPointer();
		double* d_i_Null = ((pdat::NodeData<double> *) patch->getPatchData(d_d_i_Null_id).get())->getPointer();
		double* d_j_Null = ((pdat::NodeData<double> *) patch->getPatchData(d_d_j_Null_id).get())->getPointer();
		double* d_k_Null = ((pdat::NodeData<double> *) patch->getPatchData(d_d_k_Null_id).get())->getPointer();
	
		double* Null = ((pdat::NodeData<double> *) patch->getPatchData(d_Null_id).get())->getPointer();
		double* extrapolatedSBrhoSPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBrhoSPi_id).get())->getPointer();
		double* extrapolatedSBrhoSPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBrhoSPj_id).get())->getPointer();
		double* extrapolatedSBrhoSPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBrhoSPk_id).get())->getPointer();
		double* rho = ((pdat::NodeData<double> *) patch->getPatchData(d_rho_id).get())->getPointer();
		double* extrapolatedSBmxSPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmxSPi_id).get())->getPointer();
		double* extrapolatedSBmxSPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmxSPj_id).get())->getPointer();
		double* extrapolatedSBmxSPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmxSPk_id).get())->getPointer();
		double* mx = ((pdat::NodeData<double> *) patch->getPatchData(d_mx_id).get())->getPointer();
		double* extrapolatedSBmySPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmySPi_id).get())->getPointer();
		double* extrapolatedSBmySPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmySPj_id).get())->getPointer();
		double* extrapolatedSBmySPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmySPk_id).get())->getPointer();
		double* my = ((pdat::NodeData<double> *) patch->getPatchData(d_my_id).get())->getPointer();
		double* extrapolatedSBmzSPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmzSPi_id).get())->getPointer();
		double* extrapolatedSBmzSPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmzSPj_id).get())->getPointer();
		double* extrapolatedSBmzSPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBmzSPk_id).get())->getPointer();
		double* mz = ((pdat::NodeData<double> *) patch->getPatchData(d_mz_id).get())->getPointer();
		double* extrapolatedSBpSPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBpSPi_id).get())->getPointer();
		double* extrapolatedSBpSPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBpSPj_id).get())->getPointer();
		double* extrapolatedSBpSPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBpSPk_id).get())->getPointer();
		double* p = ((pdat::NodeData<double> *) patch->getPatchData(d_p_id).get())->getPointer();
		double* extrapolatedSBfxSPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBfxSPi_id).get())->getPointer();
		double* extrapolatedSBfxSPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBfxSPj_id).get())->getPointer();
		double* extrapolatedSBfxSPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBfxSPk_id).get())->getPointer();
		double* fx = ((pdat::NodeData<double> *) patch->getPatchData(d_fx_id).get())->getPointer();
		double* extrapolatedSBfySPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBfySPi_id).get())->getPointer();
		double* extrapolatedSBfySPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBfySPj_id).get())->getPointer();
		double* extrapolatedSBfySPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBfySPk_id).get())->getPointer();
		double* fy = ((pdat::NodeData<double> *) patch->getPatchData(d_fy_id).get())->getPointer();
		double* extrapolatedSBfzSPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBfzSPi_id).get())->getPointer();
		double* extrapolatedSBfzSPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBfzSPj_id).get())->getPointer();
		double* extrapolatedSBfzSPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBfzSPk_id).get())->getPointer();
		double* fz = ((pdat::NodeData<double> *) patch->getPatchData(d_fz_id).get())->getPointer();
		double* extrapolatedSBNullSPi = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBNullSPi_id).get())->getPointer();
		double* extrapolatedSBNullSPj = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBNullSPj_id).get())->getPointer();
		double* extrapolatedSBNullSPk = ((pdat::NodeData<double> *) patch->getPatchData(d_extrapolatedSBNullSPk_id).get())->getPointer();
	
		//Get the dimensions of the patch
		const hier::Index boxfirst = patch->getBox().lower();
		const hier::Index boxlast = patch->getBox().upper();
	
		//Get delta spaces into an array. dx, dy, dz.
		std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
		const double* dx  = patch_geom->getDx();
	
		//Auxiliary definitions
		int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
		int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
		int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;
		for(int k = 0; k < klast; k++) {
			for(int j = 0; j < jlast; j++) {
				for(int i = 0; i < ilast; i++) {
						vector(extrapolatedSBrhoSPi, i, j, k) = vector(rho, i, j, k);
						vector(extrapolatedSBrhoSPj, i, j, k) = vector(rho, i, j, k);
						vector(extrapolatedSBrhoSPk, i, j, k) = vector(rho, i, j, k);
						vector(extrapolatedSBmxSPi, i, j, k) = vector(mx, i, j, k);
						vector(extrapolatedSBmxSPj, i, j, k) = vector(mx, i, j, k);
						vector(extrapolatedSBmxSPk, i, j, k) = vector(mx, i, j, k);
						vector(extrapolatedSBmySPi, i, j, k) = vector(my, i, j, k);
						vector(extrapolatedSBmySPj, i, j, k) = vector(my, i, j, k);
						vector(extrapolatedSBmySPk, i, j, k) = vector(my, i, j, k);
						vector(extrapolatedSBmzSPi, i, j, k) = vector(mz, i, j, k);
						vector(extrapolatedSBmzSPj, i, j, k) = vector(mz, i, j, k);
						vector(extrapolatedSBmzSPk, i, j, k) = vector(mz, i, j, k);
						vector(extrapolatedSBNullSPi, i, j, k) = vector(Null, i, j, k);
						vector(extrapolatedSBNullSPj, i, j, k) = vector(Null, i, j, k);
						vector(extrapolatedSBNullSPk, i, j, k) = vector(Null, i, j, k);
						vector(extrapolatedSBpSPi, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBpSPj, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBpSPk, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBfxSPi, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfxSPj, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfxSPk, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfySPi, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfySPj, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfySPk, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfzSPi, i, j, k) = vector(fz, i, j, k);
						vector(extrapolatedSBfzSPj, i, j, k) = vector(fz, i, j, k);
						vector(extrapolatedSBfzSPk, i, j, k) = vector(fz, i, j, k);
				}
			}
		}
		for(int k = 0; k < klast; k++) {
			for(int j = 0; j < jlast; j++) {
				for(int i = 0; i < ilast; i++) {
					if ((vector(FOV_xLower, i, j, k) > 0) && ((i + 1 < ilast && (vector(FOV_3,i + 1, j, k) > 0)) || (i + 2 < ilast && (vector(FOV_3,i + 2, j, k) > 0)))) {
						vector(Null, i, j, k) = 0.0;
					}
					if ((vector(FOV_xUpper, i, j, k) > 0) && ((i - 1 >= 0 && (vector(FOV_3, i - 1, j, k) > 0)) || (i - 2 >= 0 && (vector(FOV_3, i - 2, j, k) > 0)))) {
						vector(Null, i, j, k) = 0.0;
					}
					if ((vector(FOV_yLower, i, j, k) > 0) && ((j + 1 < jlast && (vector(FOV_3,i, j + 1, k) > 0)) || (j + 2 < jlast && (vector(FOV_3,i, j + 2, k) > 0)))) {
						vector(Null, i, j, k) = 0.0;
					}
					if ((vector(FOV_yUpper, i, j, k) > 0) && ((j - 1 >= 0 && (vector(FOV_3, i, j - 1, k) > 0)) || (j - 2 >= 0 && (vector(FOV_3, i, j - 2, k) > 0)))) {
						vector(Null, i, j, k) = 0.0;
					}
					if ((vector(FOV_zLower, i, j, k) > 0) && ((k + 1 < klast && (vector(FOV_3,i, j, k + 1) > 0)) || (k + 2 < klast && (vector(FOV_3,i, j, k + 2) > 0)))) {
						vector(Null, i, j, k) = 0.0;
					}
					if ((vector(FOV_zUpper, i, j, k) > 0) && ((k - 1 >= 0 && (vector(FOV_3, i, j, k - 1) > 0)) || (k - 2 >= 0 && (vector(FOV_3, i, j, k - 2) > 0)))) {
						vector(Null, i, j, k) = 0.0;
					}
					if (vector(FOV_3, i, j, k) > 0) {
						//Region field extrapolations
						if ((vector(d_i_rho_mx_my_mz_p_fx_fy_fz, i, j, k) != 0 || vector(d_j_rho_mx_my_mz_p_fx_fy_fz, i, j, k) != 0 || vector(d_k_rho_mx_my_mz_p_fx_fy_fz, i, j, k) != 0)) {
							extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBrhoSPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBrhoSPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBrhoSPk, k, rho, FOV_1, dx, ilast, jlast);
							extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmxSPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmxSPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmxSPk, k, mx, FOV_1, dx, ilast, jlast);
							extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmySPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmySPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmySPk, k, my, FOV_1, dx, ilast, jlast);
							extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmzSPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmzSPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmzSPk, k, mz, FOV_1, dx, ilast, jlast);
							extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBpSPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBpSPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBpSPk, k, p, FOV_1, dx, ilast, jlast);
							extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfxSPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfxSPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfxSPk, k, fx, FOV_1, dx, ilast, jlast);
							extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfySPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfySPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfySPk, k, fy, FOV_1, dx, ilast, jlast);
							extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfzSPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfzSPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfzSPk, k, fz, FOV_1, dx, ilast, jlast);
						}
					}
					if ((vector(FOV_xLower, i, j, k) > 0) && ((i + 1 < ilast && (vector(FOV_1,i + 1, j, k) > 0)) || (i + 2 < ilast && (vector(FOV_1,i + 2, j, k) > 0)) || ((i + 1 < ilast && k + 1 < klast && (vector(FOV_1, i + 1, j, k + 1) > 0)) || (i + 1 < ilast && k - 1 >= 0 && (vector(FOV_1, i + 1, j, k - 1) > 0)) || (i + 1 < ilast && k + 2 < klast && (vector(FOV_1, i + 1, j, k + 2) > 0)) || (i + 1 < ilast && k - 2 >= 0 && (vector(FOV_1, i + 1, j, k - 2) > 0)) || (i + 1 < ilast && j + 1 < jlast && (vector(FOV_1, i + 1, j + 1, k) > 0)) || (i + 1 < ilast && j + 1 < jlast && k + 1 < klast && (vector(FOV_1, i + 1, j + 1, k + 1) > 0)) || (i + 1 < ilast && j + 1 < jlast && k - 1 >= 0 && (vector(FOV_1, i + 1, j + 1, k - 1) > 0)) || (i + 1 < ilast && j + 1 < jlast && k + 2 < klast && (vector(FOV_1, i + 1, j + 1, k + 2) > 0)) || (i + 1 < ilast && j + 1 < jlast && k - 2 >= 0 && (vector(FOV_1, i + 1, j + 1, k - 2) > 0)) || (i + 1 < ilast && j - 1 >= 0 && (vector(FOV_1, i + 1, j - 1, k) > 0)) || (i + 1 < ilast && j - 1 >= 0 && k + 1 < klast && (vector(FOV_1, i + 1, j - 1, k + 1) > 0)) || (i + 1 < ilast && j - 1 >= 0 && k - 1 >= 0 && (vector(FOV_1, i + 1, j - 1, k - 1) > 0)) || (i + 1 < ilast && j - 1 >= 0 && k + 2 < klast && (vector(FOV_1, i + 1, j - 1, k + 2) > 0)) || (i + 1 < ilast && j - 1 >= 0 && k - 2 >= 0 && (vector(FOV_1, i + 1, j - 1, k - 2) > 0)) || (i + 1 < ilast && j + 2 < jlast && (vector(FOV_1, i + 1, j + 2, k) > 0)) || (i + 1 < ilast && j + 2 < jlast && k + 1 < klast && (vector(FOV_1, i + 1, j + 2, k + 1) > 0)) || (i + 1 < ilast && j + 2 < jlast && k - 1 >= 0 && (vector(FOV_1, i + 1, j + 2, k - 1) > 0)) || (i + 1 < ilast && j + 2 < jlast && k + 2 < klast && (vector(FOV_1, i + 1, j + 2, k + 2) > 0)) || (i + 1 < ilast && j + 2 < jlast && k - 2 >= 0 && (vector(FOV_1, i + 1, j + 2, k - 2) > 0)) || (i + 1 < ilast && j - 2 >= 0 && (vector(FOV_1, i + 1, j - 2, k) > 0)) || (i + 1 < ilast && j - 2 >= 0 && k + 1 < klast && (vector(FOV_1, i + 1, j - 2, k + 1) > 0)) || (i + 1 < ilast && j - 2 >= 0 && k - 1 >= 0 && (vector(FOV_1, i + 1, j - 2, k - 1) > 0)) || (i + 1 < ilast && j - 2 >= 0 && k + 2 < klast && (vector(FOV_1, i + 1, j - 2, k + 2) > 0)) || (i + 1 < ilast && j - 2 >= 0 && k - 2 >= 0 && (vector(FOV_1, i + 1, j - 2, k - 2) > 0)) || (i + 2 < ilast && k + 1 < klast && (vector(FOV_1, i + 2, j, k + 1) > 0)) || (i + 2 < ilast && k - 1 >= 0 && (vector(FOV_1, i + 2, j, k - 1) > 0)) || (i + 2 < ilast && k + 2 < klast && (vector(FOV_1, i + 2, j, k + 2) > 0)) || (i + 2 < ilast && k - 2 >= 0 && (vector(FOV_1, i + 2, j, k - 2) > 0)) || (i + 2 < ilast && j + 1 < jlast && (vector(FOV_1, i + 2, j + 1, k) > 0)) || (i + 2 < ilast && j + 1 < jlast && k + 1 < klast && (vector(FOV_1, i + 2, j + 1, k + 1) > 0)) || (i + 2 < ilast && j + 1 < jlast && k - 1 >= 0 && (vector(FOV_1, i + 2, j + 1, k - 1) > 0)) || (i + 2 < ilast && j + 1 < jlast && k + 2 < klast && (vector(FOV_1, i + 2, j + 1, k + 2) > 0)) || (i + 2 < ilast && j + 1 < jlast && k - 2 >= 0 && (vector(FOV_1, i + 2, j + 1, k - 2) > 0)) || (i + 2 < ilast && j - 1 >= 0 && (vector(FOV_1, i + 2, j - 1, k) > 0)) || (i + 2 < ilast && j - 1 >= 0 && k + 1 < klast && (vector(FOV_1, i + 2, j - 1, k + 1) > 0)) || (i + 2 < ilast && j - 1 >= 0 && k - 1 >= 0 && (vector(FOV_1, i + 2, j - 1, k - 1) > 0)) || (i + 2 < ilast && j - 1 >= 0 && k + 2 < klast && (vector(FOV_1, i + 2, j - 1, k + 2) > 0)) || (i + 2 < ilast && j - 1 >= 0 && k - 2 >= 0 && (vector(FOV_1, i + 2, j - 1, k - 2) > 0)) || (i + 2 < ilast && j + 2 < jlast && (vector(FOV_1, i + 2, j + 2, k) > 0)) || (i + 2 < ilast && j + 2 < jlast && k + 1 < klast && (vector(FOV_1, i + 2, j + 2, k + 1) > 0)) || (i + 2 < ilast && j + 2 < jlast && k - 1 >= 0 && (vector(FOV_1, i + 2, j + 2, k - 1) > 0)) || (i + 2 < ilast && j + 2 < jlast && k + 2 < klast && (vector(FOV_1, i + 2, j + 2, k + 2) > 0)) || (i + 2 < ilast && j + 2 < jlast && k - 2 >= 0 && (vector(FOV_1, i + 2, j + 2, k - 2) > 0)) || (i + 2 < ilast && j - 2 >= 0 && (vector(FOV_1, i + 2, j - 2, k) > 0)) || (i + 2 < ilast && j - 2 >= 0 && k + 1 < klast && (vector(FOV_1, i + 2, j - 2, k + 1) > 0)) || (i + 2 < ilast && j - 2 >= 0 && k - 1 >= 0 && (vector(FOV_1, i + 2, j - 2, k - 1) > 0)) || (i + 2 < ilast && j - 2 >= 0 && k + 2 < klast && (vector(FOV_1, i + 2, j - 2, k + 2) > 0)) || (i + 2 < ilast && j - 2 >= 0 && k - 2 >= 0 && (vector(FOV_1, i + 2, j - 2, k - 2) > 0))))) {
						//Region field extrapolations
						if ((vector(d_i_rho_mx_my_mz_p_fx_fy_fz, i, j, k) != 0 || vector(d_j_rho_mx_my_mz_p_fx_fy_fz, i, j, k) != 0 || vector(d_k_rho_mx_my_mz_p_fx_fy_fz, i, j, k) != 0)) {
							extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBrhoSPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBrhoSPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBrhoSPk, k, rho, FOV_1, dx, ilast, jlast);
							extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmxSPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmxSPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmxSPk, k, mx, FOV_1, dx, ilast, jlast);
							extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmySPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmySPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmySPk, k, my, FOV_1, dx, ilast, jlast);
							extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmzSPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmzSPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmzSPk, k, mz, FOV_1, dx, ilast, jlast);
							extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBpSPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBpSPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBpSPk, k, p, FOV_1, dx, ilast, jlast);
							extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfxSPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfxSPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfxSPk, k, fx, FOV_1, dx, ilast, jlast);
							extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfySPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfySPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfySPk, k, fy, FOV_1, dx, ilast, jlast);
							extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfzSPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfzSPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfzSPk, k, fz, FOV_1, dx, ilast, jlast);
						}
					}
					if ((vector(FOV_xUpper, i, j, k) > 0) && ((i - 1 >= 0 && (vector(FOV_1, i - 1, j, k) > 0)) || (i - 2 >= 0 && (vector(FOV_1, i - 2, j, k) > 0)) || ((i - 1 >= 0 && k + 1 < klast && (vector(FOV_1, i - 1, j, k + 1) > 0)) || (i - 1 >= 0 && k - 1 >= 0 && (vector(FOV_1, i - 1, j, k - 1) > 0)) || (i - 1 >= 0 && k + 2 < klast && (vector(FOV_1, i - 1, j, k + 2) > 0)) || (i - 1 >= 0 && k - 2 >= 0 && (vector(FOV_1, i - 1, j, k - 2) > 0)) || (i - 1 >= 0 && j + 1 < jlast && (vector(FOV_1, i - 1, j + 1, k) > 0)) || (i - 1 >= 0 && j + 1 < jlast && k + 1 < klast && (vector(FOV_1, i - 1, j + 1, k + 1) > 0)) || (i - 1 >= 0 && j + 1 < jlast && k - 1 >= 0 && (vector(FOV_1, i - 1, j + 1, k - 1) > 0)) || (i - 1 >= 0 && j + 1 < jlast && k + 2 < klast && (vector(FOV_1, i - 1, j + 1, k + 2) > 0)) || (i - 1 >= 0 && j + 1 < jlast && k - 2 >= 0 && (vector(FOV_1, i - 1, j + 1, k - 2) > 0)) || (i - 1 >= 0 && j - 1 >= 0 && (vector(FOV_1, i - 1, j - 1, k) > 0)) || (i - 1 >= 0 && j - 1 >= 0 && k + 1 < klast && (vector(FOV_1, i - 1, j - 1, k + 1) > 0)) || (i - 1 >= 0 && j - 1 >= 0 && k - 1 >= 0 && (vector(FOV_1, i - 1, j - 1, k - 1) > 0)) || (i - 1 >= 0 && j - 1 >= 0 && k + 2 < klast && (vector(FOV_1, i - 1, j - 1, k + 2) > 0)) || (i - 1 >= 0 && j - 1 >= 0 && k - 2 >= 0 && (vector(FOV_1, i - 1, j - 1, k - 2) > 0)) || (i - 1 >= 0 && j + 2 < jlast && (vector(FOV_1, i - 1, j + 2, k) > 0)) || (i - 1 >= 0 && j + 2 < jlast && k + 1 < klast && (vector(FOV_1, i - 1, j + 2, k + 1) > 0)) || (i - 1 >= 0 && j + 2 < jlast && k - 1 >= 0 && (vector(FOV_1, i - 1, j + 2, k - 1) > 0)) || (i - 1 >= 0 && j + 2 < jlast && k + 2 < klast && (vector(FOV_1, i - 1, j + 2, k + 2) > 0)) || (i - 1 >= 0 && j + 2 < jlast && k - 2 >= 0 && (vector(FOV_1, i - 1, j + 2, k - 2) > 0)) || (i - 1 >= 0 && j - 2 >= 0 && (vector(FOV_1, i - 1, j - 2, k) > 0)) || (i - 1 >= 0 && j - 2 >= 0 && k + 1 < klast && (vector(FOV_1, i - 1, j - 2, k + 1) > 0)) || (i - 1 >= 0 && j - 2 >= 0 && k - 1 >= 0 && (vector(FOV_1, i - 1, j - 2, k - 1) > 0)) || (i - 1 >= 0 && j - 2 >= 0 && k + 2 < klast && (vector(FOV_1, i - 1, j - 2, k + 2) > 0)) || (i - 1 >= 0 && j - 2 >= 0 && k - 2 >= 0 && (vector(FOV_1, i - 1, j - 2, k - 2) > 0)) || (i - 2 >= 0 && k + 1 < klast && (vector(FOV_1, i - 2, j, k + 1) > 0)) || (i - 2 >= 0 && k - 1 >= 0 && (vector(FOV_1, i - 2, j, k - 1) > 0)) || (i - 2 >= 0 && k + 2 < klast && (vector(FOV_1, i - 2, j, k + 2) > 0)) || (i - 2 >= 0 && k - 2 >= 0 && (vector(FOV_1, i - 2, j, k - 2) > 0)) || (i - 2 >= 0 && j + 1 < jlast && (vector(FOV_1, i - 2, j + 1, k) > 0)) || (i - 2 >= 0 && j + 1 < jlast && k + 1 < klast && (vector(FOV_1, i - 2, j + 1, k + 1) > 0)) || (i - 2 >= 0 && j + 1 < jlast && k - 1 >= 0 && (vector(FOV_1, i - 2, j + 1, k - 1) > 0)) || (i - 2 >= 0 && j + 1 < jlast && k + 2 < klast && (vector(FOV_1, i - 2, j + 1, k + 2) > 0)) || (i - 2 >= 0 && j + 1 < jlast && k - 2 >= 0 && (vector(FOV_1, i - 2, j + 1, k - 2) > 0)) || (i - 2 >= 0 && j - 1 >= 0 && (vector(FOV_1, i - 2, j - 1, k) > 0)) || (i - 2 >= 0 && j - 1 >= 0 && k + 1 < klast && (vector(FOV_1, i - 2, j - 1, k + 1) > 0)) || (i - 2 >= 0 && j - 1 >= 0 && k - 1 >= 0 && (vector(FOV_1, i - 2, j - 1, k - 1) > 0)) || (i - 2 >= 0 && j - 1 >= 0 && k + 2 < klast && (vector(FOV_1, i - 2, j - 1, k + 2) > 0)) || (i - 2 >= 0 && j - 1 >= 0 && k - 2 >= 0 && (vector(FOV_1, i - 2, j - 1, k - 2) > 0)) || (i - 2 >= 0 && j + 2 < jlast && (vector(FOV_1, i - 2, j + 2, k) > 0)) || (i - 2 >= 0 && j + 2 < jlast && k + 1 < klast && (vector(FOV_1, i - 2, j + 2, k + 1) > 0)) || (i - 2 >= 0 && j + 2 < jlast && k - 1 >= 0 && (vector(FOV_1, i - 2, j + 2, k - 1) > 0)) || (i - 2 >= 0 && j + 2 < jlast && k + 2 < klast && (vector(FOV_1, i - 2, j + 2, k + 2) > 0)) || (i - 2 >= 0 && j + 2 < jlast && k - 2 >= 0 && (vector(FOV_1, i - 2, j + 2, k - 2) > 0)) || (i - 2 >= 0 && j - 2 >= 0 && (vector(FOV_1, i - 2, j - 2, k) > 0)) || (i - 2 >= 0 && j - 2 >= 0 && k + 1 < klast && (vector(FOV_1, i - 2, j - 2, k + 1) > 0)) || (i - 2 >= 0 && j - 2 >= 0 && k - 1 >= 0 && (vector(FOV_1, i - 2, j - 2, k - 1) > 0)) || (i - 2 >= 0 && j - 2 >= 0 && k + 2 < klast && (vector(FOV_1, i - 2, j - 2, k + 2) > 0)) || (i - 2 >= 0 && j - 2 >= 0 && k - 2 >= 0 && (vector(FOV_1, i - 2, j - 2, k - 2) > 0))))) {
						//Region field extrapolations
						if ((vector(d_i_rho_mx_my_mz_p_fx_fy_fz, i, j, k) != 0 || vector(d_j_rho_mx_my_mz_p_fx_fy_fz, i, j, k) != 0 || vector(d_k_rho_mx_my_mz_p_fx_fy_fz, i, j, k) != 0)) {
							extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBrhoSPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBrhoSPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBrhoSPk, k, rho, FOV_1, dx, ilast, jlast);
							extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmxSPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmxSPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmxSPk, k, mx, FOV_1, dx, ilast, jlast);
							extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmySPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmySPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmySPk, k, my, FOV_1, dx, ilast, jlast);
							extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmzSPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmzSPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBmzSPk, k, mz, FOV_1, dx, ilast, jlast);
							extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBpSPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBpSPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBpSPk, k, p, FOV_1, dx, ilast, jlast);
							extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfxSPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfxSPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfxSPk, k, fx, FOV_1, dx, ilast, jlast);
							extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfySPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfySPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfySPk, k, fy, FOV_1, dx, ilast, jlast);
							extrapolate_field(d_i_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfzSPi, i, d_j_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfzSPj, j, d_k_rho_mx_my_mz_p_fx_fy_fz, extrapolatedSBfzSPk, k, fz, FOV_1, dx, ilast, jlast);
						}
					}
					if ((vector(FOV_yLower, i, j, k) > 0) && ((j + 1 < jlast && (vector(FOV_1,i, j + 1, k) > 0)) || (j + 2 < jlast && (vector(FOV_1,i, j + 2, k) > 0)) || ((j + 1 < jlast && k + 1 < klast && (vector(FOV_1, i, j + 1, k + 1) > 0)) || (j + 1 < jlast && k - 1 >= 0 && (vector(FOV_1, i, j + 1, k - 1) > 0)) || (j + 1 < jlast && k + 2 < klast && (vector(FOV_1, i, j + 1, k + 2) > 0)) || (j + 1 < jlast && k - 2 >= 0 && (vector(FOV_1, i, j + 1, k - 2) > 0)) || (j + 2 < jlast && k + 1 < klast && (vector(FOV_1, i, j + 2, k + 1) > 0)) || (j + 2 < jlast && k - 1 >= 0 && (vector(FOV_1, i, j + 2, k - 1) > 0)) || (j + 2 < jlast && k + 2 < klast && (vector(FOV_1, i, j + 2, k + 2) > 0)) || (j + 2 < jlast && k - 2 >= 0 && (vector(FOV_1, i, j + 2, k - 2) > 0)) || (i + 1 < ilast && j + 1 < jlast && (vector(FOV_1, i + 1, j + 1, k) > 0)) || (i + 1 < ilast && j + 1 < jlast && k + 1 < klast && (vector(FOV_1, i + 1, j + 1, k + 1) > 0)) || (i + 1 < ilast && j + 1 < jlast && k - 1 >= 0 && (vector(FOV_1, i + 1, j + 1, k - 1) > 0)) || (i + 1 < ilast && j + 1 < jlast && k + 2 < klast && (vector(FOV_1, i + 1, j + 1, k + 2) > 0)) || (i + 1 < ilast && j + 1 < jlast && k - 2 >= 0 && (vector(FOV_1, i + 1, j + 1, k - 2) > 0)) || (i + 1 < ilast && j + 2 < jlast && (vector(FOV_1, i + 1, j + 2, k) > 0)) || (i + 1 < ilast && j + 2 < jlast && k + 1 < klast && (vector(FOV_1, i + 1, j + 2, k + 1) > 0)) || (i + 1 < ilast && j + 2 < jlast && k - 1 >= 0 && (vector(FOV_1, i + 1, j + 2, k - 1) > 0)) || (i + 1 < ilast && j + 2 < jlast && k + 2 < klast && (vector(FOV_1, i + 1, j + 2, k + 2) > 0)) || (i + 1 < ilast && j + 2 < jlast && k - 2 >= 0 && (vector(FOV_1, i + 1, j + 2, k - 2) > 0)) || (i - 1 >= 0 && j + 1 < jlast && (vector(FOV_1, i - 1, j + 1, k) > 0)) || (i - 1 >= 0 && j + 1 < jlast && k + 1 < klast && (vector(FOV_1, i - 1, j + 1, k + 1) > 0)) || (i - 1 >= 0 && j + 1 < jlast && k - 1 >= 0 && (vector(FOV_1, i - 1, j + 1, k - 1) > 0)) || (i - 1 >= 0 && j + 1 < jlast && k + 2 < klast && (vector(FOV_1, i - 1, j + 1, k + 2) > 0)) || (i - 1 >= 0 && j + 1 < jlast && k - 2 >= 0 && (vector(FOV_1, i - 1, j + 1, k - 2) > 0)) || (i - 1 >= 0 && j + 2 < jlast && (vector(FOV_1, i - 1, j + 2, k) > 0)) || (i - 1 >= 0 && j + 2 < jlast && k + 1 < klast && (vector(FOV_1, i - 1, j + 2, k + 1) > 0)) || (i - 1 >= 0 && j + 2 < jlast && k - 1 >= 0 && (vector(FOV_1, i - 1, j + 2, k - 1) > 0)) || (i - 1 >= 0 && j + 2 < jlast && k + 2 < klast && (vector(FOV_1, i - 1, j + 2, k + 2) > 0)) || (i - 1 >= 0 && j + 2 < jlast && k - 2 >= 0 && (vector(FOV_1, i - 1, j + 2, k - 2) > 0)) || (i + 2 < ilast && j + 1 < jlast && (vector(FOV_1, i + 2, j + 1, k) > 0)) || (i + 2 < ilast && j + 1 < jlast && k + 1 < klast && (vector(FOV_1, i + 2, j + 1, k + 1) > 0)) || (i + 2 < ilast && j + 1 < jlast && k - 1 >= 0 && (vector(FOV_1, i + 2, j + 1, k - 1) > 0)) || (i + 2 < ilast && j + 1 < jlast && k + 2 < klast && (vector(FOV_1, i + 2, j + 1, k + 2) > 0)) || (i + 2 < ilast && j + 1 < jlast && k - 2 >= 0 && (vector(FOV_1, i + 2, j + 1, k - 2) > 0)) || (i + 2 < ilast && j + 2 < jlast && (vector(FOV_1, i + 2, j + 2, k) > 0)) || (i + 2 < ilast && j + 2 < jlast && k + 1 < klast && (vector(FOV_1, i + 2, j + 2, k + 1) > 0)) || (i + 2 < ilast && j + 2 < jlast && k - 1 >= 0 && (vector(FOV_1, i + 2, j + 2, k - 1) > 0)) || (i + 2 < ilast && j + 2 < jlast && k + 2 < klast && (vector(FOV_1, i + 2, j + 2, k + 2) > 0)) || (i + 2 < ilast && j + 2 < jlast && k - 2 >= 0 && (vector(FOV_1, i + 2, j + 2, k - 2) > 0)) || (i - 2 >= 0 && j + 1 < jlast && (vector(FOV_1, i - 2, j + 1, k) > 0)) || (i - 2 >= 0 && j + 1 < jlast && k + 1 < klast && (vector(FOV_1, i - 2, j + 1, k + 1) > 0)) || (i - 2 >= 0 && j + 1 < jlast && k - 1 >= 0 && (vector(FOV_1, i - 2, j + 1, k - 1) > 0)) || (i - 2 >= 0 && j + 1 < jlast && k + 2 < klast && (vector(FOV_1, i - 2, j + 1, k + 2) > 0)) || (i - 2 >= 0 && j + 1 < jlast && k - 2 >= 0 && (vector(FOV_1, i - 2, j + 1, k - 2) > 0)) || (i - 2 >= 0 && j + 2 < jlast && (vector(FOV_1, i - 2, j + 2, k) > 0)) || (i - 2 >= 0 && j + 2 < jlast && k + 1 < klast && (vector(FOV_1, i - 2, j + 2, k + 1) > 0)) || (i - 2 >= 0 && j + 2 < jlast && k - 1 >= 0 && (vector(FOV_1, i - 2, j + 2, k - 1) > 0)) || (i - 2 >= 0 && j + 2 < jlast && k + 2 < klast && (vector(FOV_1, i - 2, j + 2, k + 2) > 0)) || (i - 2 >= 0 && j + 2 < jlast && k - 2 >= 0 && (vector(FOV_1, i - 2, j + 2, k - 2) > 0))))) {
						vector(rho, i, j, k) = 0.0;
						vector(mx, i, j, k) = 0.0;
						vector(my, i, j, k) = 0.0;
						vector(mz, i, j, k) = 0.0;
						vector(p, i, j, k) = 0.0;
						vector(fx, i, j, k) = 0.0;
						vector(fy, i, j, k) = 0.0;
						vector(fz, i, j, k) = 0.0;
					}
					if ((vector(FOV_yUpper, i, j, k) > 0) && ((j - 1 >= 0 && (vector(FOV_1, i, j - 1, k) > 0)) || (j - 2 >= 0 && (vector(FOV_1, i, j - 2, k) > 0)) || ((j - 1 >= 0 && k + 1 < klast && (vector(FOV_1, i, j - 1, k + 1) > 0)) || (j - 1 >= 0 && k - 1 >= 0 && (vector(FOV_1, i, j - 1, k - 1) > 0)) || (j - 1 >= 0 && k + 2 < klast && (vector(FOV_1, i, j - 1, k + 2) > 0)) || (j - 1 >= 0 && k - 2 >= 0 && (vector(FOV_1, i, j - 1, k - 2) > 0)) || (j - 2 >= 0 && k + 1 < klast && (vector(FOV_1, i, j - 2, k + 1) > 0)) || (j - 2 >= 0 && k - 1 >= 0 && (vector(FOV_1, i, j - 2, k - 1) > 0)) || (j - 2 >= 0 && k + 2 < klast && (vector(FOV_1, i, j - 2, k + 2) > 0)) || (j - 2 >= 0 && k - 2 >= 0 && (vector(FOV_1, i, j - 2, k - 2) > 0)) || (i + 1 < ilast && j - 1 >= 0 && (vector(FOV_1, i + 1, j - 1, k) > 0)) || (i + 1 < ilast && j - 1 >= 0 && k + 1 < klast && (vector(FOV_1, i + 1, j - 1, k + 1) > 0)) || (i + 1 < ilast && j - 1 >= 0 && k - 1 >= 0 && (vector(FOV_1, i + 1, j - 1, k - 1) > 0)) || (i + 1 < ilast && j - 1 >= 0 && k + 2 < klast && (vector(FOV_1, i + 1, j - 1, k + 2) > 0)) || (i + 1 < ilast && j - 1 >= 0 && k - 2 >= 0 && (vector(FOV_1, i + 1, j - 1, k - 2) > 0)) || (i + 1 < ilast && j - 2 >= 0 && (vector(FOV_1, i + 1, j - 2, k) > 0)) || (i + 1 < ilast && j - 2 >= 0 && k + 1 < klast && (vector(FOV_1, i + 1, j - 2, k + 1) > 0)) || (i + 1 < ilast && j - 2 >= 0 && k - 1 >= 0 && (vector(FOV_1, i + 1, j - 2, k - 1) > 0)) || (i + 1 < ilast && j - 2 >= 0 && k + 2 < klast && (vector(FOV_1, i + 1, j - 2, k + 2) > 0)) || (i + 1 < ilast && j - 2 >= 0 && k - 2 >= 0 && (vector(FOV_1, i + 1, j - 2, k - 2) > 0)) || (i - 1 >= 0 && j - 1 >= 0 && (vector(FOV_1, i - 1, j - 1, k) > 0)) || (i - 1 >= 0 && j - 1 >= 0 && k + 1 < klast && (vector(FOV_1, i - 1, j - 1, k + 1) > 0)) || (i - 1 >= 0 && j - 1 >= 0 && k - 1 >= 0 && (vector(FOV_1, i - 1, j - 1, k - 1) > 0)) || (i - 1 >= 0 && j - 1 >= 0 && k + 2 < klast && (vector(FOV_1, i - 1, j - 1, k + 2) > 0)) || (i - 1 >= 0 && j - 1 >= 0 && k - 2 >= 0 && (vector(FOV_1, i - 1, j - 1, k - 2) > 0)) || (i - 1 >= 0 && j - 2 >= 0 && (vector(FOV_1, i - 1, j - 2, k) > 0)) || (i - 1 >= 0 && j - 2 >= 0 && k + 1 < klast && (vector(FOV_1, i - 1, j - 2, k + 1) > 0)) || (i - 1 >= 0 && j - 2 >= 0 && k - 1 >= 0 && (vector(FOV_1, i - 1, j - 2, k - 1) > 0)) || (i - 1 >= 0 && j - 2 >= 0 && k + 2 < klast && (vector(FOV_1, i - 1, j - 2, k + 2) > 0)) || (i - 1 >= 0 && j - 2 >= 0 && k - 2 >= 0 && (vector(FOV_1, i - 1, j - 2, k - 2) > 0)) || (i + 2 < ilast && j - 1 >= 0 && (vector(FOV_1, i + 2, j - 1, k) > 0)) || (i + 2 < ilast && j - 1 >= 0 && k + 1 < klast && (vector(FOV_1, i + 2, j - 1, k + 1) > 0)) || (i + 2 < ilast && j - 1 >= 0 && k - 1 >= 0 && (vector(FOV_1, i + 2, j - 1, k - 1) > 0)) || (i + 2 < ilast && j - 1 >= 0 && k + 2 < klast && (vector(FOV_1, i + 2, j - 1, k + 2) > 0)) || (i + 2 < ilast && j - 1 >= 0 && k - 2 >= 0 && (vector(FOV_1, i + 2, j - 1, k - 2) > 0)) || (i + 2 < ilast && j - 2 >= 0 && (vector(FOV_1, i + 2, j - 2, k) > 0)) || (i + 2 < ilast && j - 2 >= 0 && k + 1 < klast && (vector(FOV_1, i + 2, j - 2, k + 1) > 0)) || (i + 2 < ilast && j - 2 >= 0 && k - 1 >= 0 && (vector(FOV_1, i + 2, j - 2, k - 1) > 0)) || (i + 2 < ilast && j - 2 >= 0 && k + 2 < klast && (vector(FOV_1, i + 2, j - 2, k + 2) > 0)) || (i + 2 < ilast && j - 2 >= 0 && k - 2 >= 0 && (vector(FOV_1, i + 2, j - 2, k - 2) > 0)) || (i - 2 >= 0 && j - 1 >= 0 && (vector(FOV_1, i - 2, j - 1, k) > 0)) || (i - 2 >= 0 && j - 1 >= 0 && k + 1 < klast && (vector(FOV_1, i - 2, j - 1, k + 1) > 0)) || (i - 2 >= 0 && j - 1 >= 0 && k - 1 >= 0 && (vector(FOV_1, i - 2, j - 1, k - 1) > 0)) || (i - 2 >= 0 && j - 1 >= 0 && k + 2 < klast && (vector(FOV_1, i - 2, j - 1, k + 2) > 0)) || (i - 2 >= 0 && j - 1 >= 0 && k - 2 >= 0 && (vector(FOV_1, i - 2, j - 1, k - 2) > 0)) || (i - 2 >= 0 && j - 2 >= 0 && (vector(FOV_1, i - 2, j - 2, k) > 0)) || (i - 2 >= 0 && j - 2 >= 0 && k + 1 < klast && (vector(FOV_1, i - 2, j - 2, k + 1) > 0)) || (i - 2 >= 0 && j - 2 >= 0 && k - 1 >= 0 && (vector(FOV_1, i - 2, j - 2, k - 1) > 0)) || (i - 2 >= 0 && j - 2 >= 0 && k + 2 < klast && (vector(FOV_1, i - 2, j - 2, k + 2) > 0)) || (i - 2 >= 0 && j - 2 >= 0 && k - 2 >= 0 && (vector(FOV_1, i - 2, j - 2, k - 2) > 0))))) {
						vector(rho, i, j, k) = 0.0;
						vector(mx, i, j, k) = 0.0;
						vector(my, i, j, k) = 0.0;
						vector(mz, i, j, k) = 0.0;
						vector(p, i, j, k) = 0.0;
						vector(fx, i, j, k) = 0.0;
						vector(fy, i, j, k) = 0.0;
						vector(fz, i, j, k) = 0.0;
					}
					if ((vector(FOV_zLower, i, j, k) > 0) && ((k + 1 < klast && (vector(FOV_1,i, j, k + 1) > 0)) || (k + 2 < klast && (vector(FOV_1,i, j, k + 2) > 0)) || ((j + 1 < jlast && k + 1 < klast && (vector(FOV_1, i, j + 1, k + 1) > 0)) || (j + 1 < jlast && k + 2 < klast && (vector(FOV_1, i, j + 1, k + 2) > 0)) || (j - 1 >= 0 && k + 1 < klast && (vector(FOV_1, i, j - 1, k + 1) > 0)) || (j - 1 >= 0 && k + 2 < klast && (vector(FOV_1, i, j - 1, k + 2) > 0)) || (j + 2 < jlast && k + 1 < klast && (vector(FOV_1, i, j + 2, k + 1) > 0)) || (j + 2 < jlast && k + 2 < klast && (vector(FOV_1, i, j + 2, k + 2) > 0)) || (j - 2 >= 0 && k + 1 < klast && (vector(FOV_1, i, j - 2, k + 1) > 0)) || (j - 2 >= 0 && k + 2 < klast && (vector(FOV_1, i, j - 2, k + 2) > 0)) || (i + 1 < ilast && k + 1 < klast && (vector(FOV_1, i + 1, j, k + 1) > 0)) || (i + 1 < ilast && k + 2 < klast && (vector(FOV_1, i + 1, j, k + 2) > 0)) || (i + 1 < ilast && j + 1 < jlast && k + 1 < klast && (vector(FOV_1, i + 1, j + 1, k + 1) > 0)) || (i + 1 < ilast && j + 1 < jlast && k + 2 < klast && (vector(FOV_1, i + 1, j + 1, k + 2) > 0)) || (i + 1 < ilast && j - 1 >= 0 && k + 1 < klast && (vector(FOV_1, i + 1, j - 1, k + 1) > 0)) || (i + 1 < ilast && j - 1 >= 0 && k + 2 < klast && (vector(FOV_1, i + 1, j - 1, k + 2) > 0)) || (i + 1 < ilast && j + 2 < jlast && k + 1 < klast && (vector(FOV_1, i + 1, j + 2, k + 1) > 0)) || (i + 1 < ilast && j + 2 < jlast && k + 2 < klast && (vector(FOV_1, i + 1, j + 2, k + 2) > 0)) || (i + 1 < ilast && j - 2 >= 0 && k + 1 < klast && (vector(FOV_1, i + 1, j - 2, k + 1) > 0)) || (i + 1 < ilast && j - 2 >= 0 && k + 2 < klast && (vector(FOV_1, i + 1, j - 2, k + 2) > 0)) || (i - 1 >= 0 && k + 1 < klast && (vector(FOV_1, i - 1, j, k + 1) > 0)) || (i - 1 >= 0 && k + 2 < klast && (vector(FOV_1, i - 1, j, k + 2) > 0)) || (i - 1 >= 0 && j + 1 < jlast && k + 1 < klast && (vector(FOV_1, i - 1, j + 1, k + 1) > 0)) || (i - 1 >= 0 && j + 1 < jlast && k + 2 < klast && (vector(FOV_1, i - 1, j + 1, k + 2) > 0)) || (i - 1 >= 0 && j - 1 >= 0 && k + 1 < klast && (vector(FOV_1, i - 1, j - 1, k + 1) > 0)) || (i - 1 >= 0 && j - 1 >= 0 && k + 2 < klast && (vector(FOV_1, i - 1, j - 1, k + 2) > 0)) || (i - 1 >= 0 && j + 2 < jlast && k + 1 < klast && (vector(FOV_1, i - 1, j + 2, k + 1) > 0)) || (i - 1 >= 0 && j + 2 < jlast && k + 2 < klast && (vector(FOV_1, i - 1, j + 2, k + 2) > 0)) || (i - 1 >= 0 && j - 2 >= 0 && k + 1 < klast && (vector(FOV_1, i - 1, j - 2, k + 1) > 0)) || (i - 1 >= 0 && j - 2 >= 0 && k + 2 < klast && (vector(FOV_1, i - 1, j - 2, k + 2) > 0)) || (i + 2 < ilast && k + 1 < klast && (vector(FOV_1, i + 2, j, k + 1) > 0)) || (i + 2 < ilast && k + 2 < klast && (vector(FOV_1, i + 2, j, k + 2) > 0)) || (i + 2 < ilast && j + 1 < jlast && k + 1 < klast && (vector(FOV_1, i + 2, j + 1, k + 1) > 0)) || (i + 2 < ilast && j + 1 < jlast && k + 2 < klast && (vector(FOV_1, i + 2, j + 1, k + 2) > 0)) || (i + 2 < ilast && j - 1 >= 0 && k + 1 < klast && (vector(FOV_1, i + 2, j - 1, k + 1) > 0)) || (i + 2 < ilast && j - 1 >= 0 && k + 2 < klast && (vector(FOV_1, i + 2, j - 1, k + 2) > 0)) || (i + 2 < ilast && j + 2 < jlast && k + 1 < klast && (vector(FOV_1, i + 2, j + 2, k + 1) > 0)) || (i + 2 < ilast && j + 2 < jlast && k + 2 < klast && (vector(FOV_1, i + 2, j + 2, k + 2) > 0)) || (i + 2 < ilast && j - 2 >= 0 && k + 1 < klast && (vector(FOV_1, i + 2, j - 2, k + 1) > 0)) || (i + 2 < ilast && j - 2 >= 0 && k + 2 < klast && (vector(FOV_1, i + 2, j - 2, k + 2) > 0)) || (i - 2 >= 0 && k + 1 < klast && (vector(FOV_1, i - 2, j, k + 1) > 0)) || (i - 2 >= 0 && k + 2 < klast && (vector(FOV_1, i - 2, j, k + 2) > 0)) || (i - 2 >= 0 && j + 1 < jlast && k + 1 < klast && (vector(FOV_1, i - 2, j + 1, k + 1) > 0)) || (i - 2 >= 0 && j + 1 < jlast && k + 2 < klast && (vector(FOV_1, i - 2, j + 1, k + 2) > 0)) || (i - 2 >= 0 && j - 1 >= 0 && k + 1 < klast && (vector(FOV_1, i - 2, j - 1, k + 1) > 0)) || (i - 2 >= 0 && j - 1 >= 0 && k + 2 < klast && (vector(FOV_1, i - 2, j - 1, k + 2) > 0)) || (i - 2 >= 0 && j + 2 < jlast && k + 1 < klast && (vector(FOV_1, i - 2, j + 2, k + 1) > 0)) || (i - 2 >= 0 && j + 2 < jlast && k + 2 < klast && (vector(FOV_1, i - 2, j + 2, k + 2) > 0)) || (i - 2 >= 0 && j - 2 >= 0 && k + 1 < klast && (vector(FOV_1, i - 2, j - 2, k + 1) > 0)) || (i - 2 >= 0 && j - 2 >= 0 && k + 2 < klast && (vector(FOV_1, i - 2, j - 2, k + 2) > 0))))) {
						vector(rho, i, j, k) = 0.0;
						vector(mx, i, j, k) = 0.0;
						vector(my, i, j, k) = 0.0;
						vector(mz, i, j, k) = 0.0;
						vector(p, i, j, k) = 0.0;
						vector(fx, i, j, k) = 0.0;
						vector(fy, i, j, k) = 0.0;
						vector(fz, i, j, k) = 0.0;
					}
					if ((vector(FOV_zUpper, i, j, k) > 0) && ((k - 1 >= 0 && (vector(FOV_1, i, j, k - 1) > 0)) || (k - 2 >= 0 && (vector(FOV_1, i, j, k - 2) > 0)) || ((j + 1 < jlast && k - 1 >= 0 && (vector(FOV_1, i, j + 1, k - 1) > 0)) || (j + 1 < jlast && k - 2 >= 0 && (vector(FOV_1, i, j + 1, k - 2) > 0)) || (j - 1 >= 0 && k - 1 >= 0 && (vector(FOV_1, i, j - 1, k - 1) > 0)) || (j - 1 >= 0 && k - 2 >= 0 && (vector(FOV_1, i, j - 1, k - 2) > 0)) || (j + 2 < jlast && k - 1 >= 0 && (vector(FOV_1, i, j + 2, k - 1) > 0)) || (j + 2 < jlast && k - 2 >= 0 && (vector(FOV_1, i, j + 2, k - 2) > 0)) || (j - 2 >= 0 && k - 1 >= 0 && (vector(FOV_1, i, j - 2, k - 1) > 0)) || (j - 2 >= 0 && k - 2 >= 0 && (vector(FOV_1, i, j - 2, k - 2) > 0)) || (i + 1 < ilast && k - 1 >= 0 && (vector(FOV_1, i + 1, j, k - 1) > 0)) || (i + 1 < ilast && k - 2 >= 0 && (vector(FOV_1, i + 1, j, k - 2) > 0)) || (i + 1 < ilast && j + 1 < jlast && k - 1 >= 0 && (vector(FOV_1, i + 1, j + 1, k - 1) > 0)) || (i + 1 < ilast && j + 1 < jlast && k - 2 >= 0 && (vector(FOV_1, i + 1, j + 1, k - 2) > 0)) || (i + 1 < ilast && j - 1 >= 0 && k - 1 >= 0 && (vector(FOV_1, i + 1, j - 1, k - 1) > 0)) || (i + 1 < ilast && j - 1 >= 0 && k - 2 >= 0 && (vector(FOV_1, i + 1, j - 1, k - 2) > 0)) || (i + 1 < ilast && j + 2 < jlast && k - 1 >= 0 && (vector(FOV_1, i + 1, j + 2, k - 1) > 0)) || (i + 1 < ilast && j + 2 < jlast && k - 2 >= 0 && (vector(FOV_1, i + 1, j + 2, k - 2) > 0)) || (i + 1 < ilast && j - 2 >= 0 && k - 1 >= 0 && (vector(FOV_1, i + 1, j - 2, k - 1) > 0)) || (i + 1 < ilast && j - 2 >= 0 && k - 2 >= 0 && (vector(FOV_1, i + 1, j - 2, k - 2) > 0)) || (i - 1 >= 0 && k - 1 >= 0 && (vector(FOV_1, i - 1, j, k - 1) > 0)) || (i - 1 >= 0 && k - 2 >= 0 && (vector(FOV_1, i - 1, j, k - 2) > 0)) || (i - 1 >= 0 && j + 1 < jlast && k - 1 >= 0 && (vector(FOV_1, i - 1, j + 1, k - 1) > 0)) || (i - 1 >= 0 && j + 1 < jlast && k - 2 >= 0 && (vector(FOV_1, i - 1, j + 1, k - 2) > 0)) || (i - 1 >= 0 && j - 1 >= 0 && k - 1 >= 0 && (vector(FOV_1, i - 1, j - 1, k - 1) > 0)) || (i - 1 >= 0 && j - 1 >= 0 && k - 2 >= 0 && (vector(FOV_1, i - 1, j - 1, k - 2) > 0)) || (i - 1 >= 0 && j + 2 < jlast && k - 1 >= 0 && (vector(FOV_1, i - 1, j + 2, k - 1) > 0)) || (i - 1 >= 0 && j + 2 < jlast && k - 2 >= 0 && (vector(FOV_1, i - 1, j + 2, k - 2) > 0)) || (i - 1 >= 0 && j - 2 >= 0 && k - 1 >= 0 && (vector(FOV_1, i - 1, j - 2, k - 1) > 0)) || (i - 1 >= 0 && j - 2 >= 0 && k - 2 >= 0 && (vector(FOV_1, i - 1, j - 2, k - 2) > 0)) || (i + 2 < ilast && k - 1 >= 0 && (vector(FOV_1, i + 2, j, k - 1) > 0)) || (i + 2 < ilast && k - 2 >= 0 && (vector(FOV_1, i + 2, j, k - 2) > 0)) || (i + 2 < ilast && j + 1 < jlast && k - 1 >= 0 && (vector(FOV_1, i + 2, j + 1, k - 1) > 0)) || (i + 2 < ilast && j + 1 < jlast && k - 2 >= 0 && (vector(FOV_1, i + 2, j + 1, k - 2) > 0)) || (i + 2 < ilast && j - 1 >= 0 && k - 1 >= 0 && (vector(FOV_1, i + 2, j - 1, k - 1) > 0)) || (i + 2 < ilast && j - 1 >= 0 && k - 2 >= 0 && (vector(FOV_1, i + 2, j - 1, k - 2) > 0)) || (i + 2 < ilast && j + 2 < jlast && k - 1 >= 0 && (vector(FOV_1, i + 2, j + 2, k - 1) > 0)) || (i + 2 < ilast && j + 2 < jlast && k - 2 >= 0 && (vector(FOV_1, i + 2, j + 2, k - 2) > 0)) || (i + 2 < ilast && j - 2 >= 0 && k - 1 >= 0 && (vector(FOV_1, i + 2, j - 2, k - 1) > 0)) || (i + 2 < ilast && j - 2 >= 0 && k - 2 >= 0 && (vector(FOV_1, i + 2, j - 2, k - 2) > 0)) || (i - 2 >= 0 && k - 1 >= 0 && (vector(FOV_1, i - 2, j, k - 1) > 0)) || (i - 2 >= 0 && k - 2 >= 0 && (vector(FOV_1, i - 2, j, k - 2) > 0)) || (i - 2 >= 0 && j + 1 < jlast && k - 1 >= 0 && (vector(FOV_1, i - 2, j + 1, k - 1) > 0)) || (i - 2 >= 0 && j + 1 < jlast && k - 2 >= 0 && (vector(FOV_1, i - 2, j + 1, k - 2) > 0)) || (i - 2 >= 0 && j - 1 >= 0 && k - 1 >= 0 && (vector(FOV_1, i - 2, j - 1, k - 1) > 0)) || (i - 2 >= 0 && j - 1 >= 0 && k - 2 >= 0 && (vector(FOV_1, i - 2, j - 1, k - 2) > 0)) || (i - 2 >= 0 && j + 2 < jlast && k - 1 >= 0 && (vector(FOV_1, i - 2, j + 2, k - 1) > 0)) || (i - 2 >= 0 && j + 2 < jlast && k - 2 >= 0 && (vector(FOV_1, i - 2, j + 2, k - 2) > 0)) || (i - 2 >= 0 && j - 2 >= 0 && k - 1 >= 0 && (vector(FOV_1, i - 2, j - 2, k - 1) > 0)) || (i - 2 >= 0 && j - 2 >= 0 && k - 2 >= 0 && (vector(FOV_1, i - 2, j - 2, k - 2) > 0))))) {
						vector(rho, i, j, k) = 0.0;
						vector(mx, i, j, k) = 0.0;
						vector(my, i, j, k) = 0.0;
						vector(mz, i, j, k) = 0.0;
						vector(p, i, j, k) = 0.0;
						vector(fx, i, j, k) = 0.0;
						vector(fy, i, j, k) = 0.0;
						vector(fz, i, j, k) = 0.0;
					}
					if (vector(FOV_1, i, j, k) > 0) {
						//Region field extrapolations
						if ((vector(d_i_Null, i, j, k) != 0 || vector(d_j_Null, i, j, k) != 0 || vector(d_k_Null, i, j, k) != 0)) {
							extrapolate_field(d_i_Null, extrapolatedSBNullSPi, i, d_j_Null, extrapolatedSBNullSPj, j, d_k_Null, extrapolatedSBNullSPk, k, Null, FOV_3, dx, ilast, jlast);
						}
					}
				}
			}
		}
		for(int k = 0; k < klast; k++) {
			for(int j = 0; j < jlast; j++) {
				for(int i = 0; i < ilast; i++) {
					if ((vector(FOV_xLower, i, j, k) > 0) && ((i + 1 < ilast && (vector(FOV_3,i + 1, j, k) > 0)) || (i + 2 < ilast && (vector(FOV_3,i + 2, j, k) > 0)))) {
						vector(extrapolatedSBrhoSPi, i, j, k) = vector(rho, i, j, k);
						vector(extrapolatedSBrhoSPj, i, j, k) = vector(rho, i, j, k);
						vector(extrapolatedSBrhoSPk, i, j, k) = vector(rho, i, j, k);
						vector(extrapolatedSBmxSPi, i, j, k) = vector(mx, i, j, k);
						vector(extrapolatedSBmxSPj, i, j, k) = vector(mx, i, j, k);
						vector(extrapolatedSBmxSPk, i, j, k) = vector(mx, i, j, k);
						vector(extrapolatedSBmySPi, i, j, k) = vector(my, i, j, k);
						vector(extrapolatedSBmySPj, i, j, k) = vector(my, i, j, k);
						vector(extrapolatedSBmySPk, i, j, k) = vector(my, i, j, k);
						vector(extrapolatedSBmzSPi, i, j, k) = vector(mz, i, j, k);
						vector(extrapolatedSBmzSPj, i, j, k) = vector(mz, i, j, k);
						vector(extrapolatedSBmzSPk, i, j, k) = vector(mz, i, j, k);
						vector(extrapolatedSBNullSPi, i, j, k) = vector(Null, i, j, k);
						vector(extrapolatedSBNullSPj, i, j, k) = vector(Null, i, j, k);
						vector(extrapolatedSBNullSPk, i, j, k) = vector(Null, i, j, k);
						vector(extrapolatedSBpSPi, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBpSPj, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBpSPk, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBfxSPi, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfxSPj, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfxSPk, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfySPi, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfySPj, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfySPk, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfzSPi, i, j, k) = vector(fz, i, j, k);
						vector(extrapolatedSBfzSPj, i, j, k) = vector(fz, i, j, k);
						vector(extrapolatedSBfzSPk, i, j, k) = vector(fz, i, j, k);
					}
					if ((vector(FOV_xUpper, i, j, k) > 0) && ((i - 1 >= 0 && (vector(FOV_3, i - 1, j, k) > 0)) || (i - 2 >= 0 && (vector(FOV_3, i - 2, j, k) > 0)))) {
						vector(extrapolatedSBrhoSPi, i, j, k) = vector(rho, i, j, k);
						vector(extrapolatedSBrhoSPj, i, j, k) = vector(rho, i, j, k);
						vector(extrapolatedSBrhoSPk, i, j, k) = vector(rho, i, j, k);
						vector(extrapolatedSBmxSPi, i, j, k) = vector(mx, i, j, k);
						vector(extrapolatedSBmxSPj, i, j, k) = vector(mx, i, j, k);
						vector(extrapolatedSBmxSPk, i, j, k) = vector(mx, i, j, k);
						vector(extrapolatedSBmySPi, i, j, k) = vector(my, i, j, k);
						vector(extrapolatedSBmySPj, i, j, k) = vector(my, i, j, k);
						vector(extrapolatedSBmySPk, i, j, k) = vector(my, i, j, k);
						vector(extrapolatedSBmzSPi, i, j, k) = vector(mz, i, j, k);
						vector(extrapolatedSBmzSPj, i, j, k) = vector(mz, i, j, k);
						vector(extrapolatedSBmzSPk, i, j, k) = vector(mz, i, j, k);
						vector(extrapolatedSBNullSPi, i, j, k) = vector(Null, i, j, k);
						vector(extrapolatedSBNullSPj, i, j, k) = vector(Null, i, j, k);
						vector(extrapolatedSBNullSPk, i, j, k) = vector(Null, i, j, k);
						vector(extrapolatedSBpSPi, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBpSPj, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBpSPk, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBfxSPi, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfxSPj, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfxSPk, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfySPi, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfySPj, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfySPk, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfzSPi, i, j, k) = vector(fz, i, j, k);
						vector(extrapolatedSBfzSPj, i, j, k) = vector(fz, i, j, k);
						vector(extrapolatedSBfzSPk, i, j, k) = vector(fz, i, j, k);
					}
					if ((vector(FOV_yLower, i, j, k) > 0) && ((j + 1 < jlast && (vector(FOV_3,i, j + 1, k) > 0)) || (j + 2 < jlast && (vector(FOV_3,i, j + 2, k) > 0)))) {
						vector(extrapolatedSBrhoSPi, i, j, k) = vector(rho, i, j, k);
						vector(extrapolatedSBrhoSPj, i, j, k) = vector(rho, i, j, k);
						vector(extrapolatedSBrhoSPk, i, j, k) = vector(rho, i, j, k);
						vector(extrapolatedSBmxSPi, i, j, k) = vector(mx, i, j, k);
						vector(extrapolatedSBmxSPj, i, j, k) = vector(mx, i, j, k);
						vector(extrapolatedSBmxSPk, i, j, k) = vector(mx, i, j, k);
						vector(extrapolatedSBmySPi, i, j, k) = vector(my, i, j, k);
						vector(extrapolatedSBmySPj, i, j, k) = vector(my, i, j, k);
						vector(extrapolatedSBmySPk, i, j, k) = vector(my, i, j, k);
						vector(extrapolatedSBmzSPi, i, j, k) = vector(mz, i, j, k);
						vector(extrapolatedSBmzSPj, i, j, k) = vector(mz, i, j, k);
						vector(extrapolatedSBmzSPk, i, j, k) = vector(mz, i, j, k);
						vector(extrapolatedSBNullSPi, i, j, k) = vector(Null, i, j, k);
						vector(extrapolatedSBNullSPj, i, j, k) = vector(Null, i, j, k);
						vector(extrapolatedSBNullSPk, i, j, k) = vector(Null, i, j, k);
						vector(extrapolatedSBpSPi, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBpSPj, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBpSPk, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBfxSPi, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfxSPj, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfxSPk, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfySPi, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfySPj, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfySPk, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfzSPi, i, j, k) = vector(fz, i, j, k);
						vector(extrapolatedSBfzSPj, i, j, k) = vector(fz, i, j, k);
						vector(extrapolatedSBfzSPk, i, j, k) = vector(fz, i, j, k);
					}
					if ((vector(FOV_yUpper, i, j, k) > 0) && ((j - 1 >= 0 && (vector(FOV_3, i, j - 1, k) > 0)) || (j - 2 >= 0 && (vector(FOV_3, i, j - 2, k) > 0)))) {
						vector(extrapolatedSBrhoSPi, i, j, k) = vector(rho, i, j, k);
						vector(extrapolatedSBrhoSPj, i, j, k) = vector(rho, i, j, k);
						vector(extrapolatedSBrhoSPk, i, j, k) = vector(rho, i, j, k);
						vector(extrapolatedSBmxSPi, i, j, k) = vector(mx, i, j, k);
						vector(extrapolatedSBmxSPj, i, j, k) = vector(mx, i, j, k);
						vector(extrapolatedSBmxSPk, i, j, k) = vector(mx, i, j, k);
						vector(extrapolatedSBmySPi, i, j, k) = vector(my, i, j, k);
						vector(extrapolatedSBmySPj, i, j, k) = vector(my, i, j, k);
						vector(extrapolatedSBmySPk, i, j, k) = vector(my, i, j, k);
						vector(extrapolatedSBmzSPi, i, j, k) = vector(mz, i, j, k);
						vector(extrapolatedSBmzSPj, i, j, k) = vector(mz, i, j, k);
						vector(extrapolatedSBmzSPk, i, j, k) = vector(mz, i, j, k);
						vector(extrapolatedSBNullSPi, i, j, k) = vector(Null, i, j, k);
						vector(extrapolatedSBNullSPj, i, j, k) = vector(Null, i, j, k);
						vector(extrapolatedSBNullSPk, i, j, k) = vector(Null, i, j, k);
						vector(extrapolatedSBpSPi, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBpSPj, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBpSPk, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBfxSPi, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfxSPj, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfxSPk, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfySPi, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfySPj, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfySPk, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfzSPi, i, j, k) = vector(fz, i, j, k);
						vector(extrapolatedSBfzSPj, i, j, k) = vector(fz, i, j, k);
						vector(extrapolatedSBfzSPk, i, j, k) = vector(fz, i, j, k);
					}
					if ((vector(FOV_zLower, i, j, k) > 0) && ((k + 1 < klast && (vector(FOV_3,i, j, k + 1) > 0)) || (k + 2 < klast && (vector(FOV_3,i, j, k + 2) > 0)))) {
						vector(extrapolatedSBrhoSPi, i, j, k) = vector(rho, i, j, k);
						vector(extrapolatedSBrhoSPj, i, j, k) = vector(rho, i, j, k);
						vector(extrapolatedSBrhoSPk, i, j, k) = vector(rho, i, j, k);
						vector(extrapolatedSBmxSPi, i, j, k) = vector(mx, i, j, k);
						vector(extrapolatedSBmxSPj, i, j, k) = vector(mx, i, j, k);
						vector(extrapolatedSBmxSPk, i, j, k) = vector(mx, i, j, k);
						vector(extrapolatedSBmySPi, i, j, k) = vector(my, i, j, k);
						vector(extrapolatedSBmySPj, i, j, k) = vector(my, i, j, k);
						vector(extrapolatedSBmySPk, i, j, k) = vector(my, i, j, k);
						vector(extrapolatedSBmzSPi, i, j, k) = vector(mz, i, j, k);
						vector(extrapolatedSBmzSPj, i, j, k) = vector(mz, i, j, k);
						vector(extrapolatedSBmzSPk, i, j, k) = vector(mz, i, j, k);
						vector(extrapolatedSBNullSPi, i, j, k) = vector(Null, i, j, k);
						vector(extrapolatedSBNullSPj, i, j, k) = vector(Null, i, j, k);
						vector(extrapolatedSBNullSPk, i, j, k) = vector(Null, i, j, k);
						vector(extrapolatedSBpSPi, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBpSPj, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBpSPk, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBfxSPi, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfxSPj, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfxSPk, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfySPi, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfySPj, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfySPk, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfzSPi, i, j, k) = vector(fz, i, j, k);
						vector(extrapolatedSBfzSPj, i, j, k) = vector(fz, i, j, k);
						vector(extrapolatedSBfzSPk, i, j, k) = vector(fz, i, j, k);
					}
					if ((vector(FOV_zUpper, i, j, k) > 0) && ((k - 1 >= 0 && (vector(FOV_3, i, j, k - 1) > 0)) || (k - 2 >= 0 && (vector(FOV_3, i, j, k - 2) > 0)))) {
						vector(extrapolatedSBrhoSPi, i, j, k) = vector(rho, i, j, k);
						vector(extrapolatedSBrhoSPj, i, j, k) = vector(rho, i, j, k);
						vector(extrapolatedSBrhoSPk, i, j, k) = vector(rho, i, j, k);
						vector(extrapolatedSBmxSPi, i, j, k) = vector(mx, i, j, k);
						vector(extrapolatedSBmxSPj, i, j, k) = vector(mx, i, j, k);
						vector(extrapolatedSBmxSPk, i, j, k) = vector(mx, i, j, k);
						vector(extrapolatedSBmySPi, i, j, k) = vector(my, i, j, k);
						vector(extrapolatedSBmySPj, i, j, k) = vector(my, i, j, k);
						vector(extrapolatedSBmySPk, i, j, k) = vector(my, i, j, k);
						vector(extrapolatedSBmzSPi, i, j, k) = vector(mz, i, j, k);
						vector(extrapolatedSBmzSPj, i, j, k) = vector(mz, i, j, k);
						vector(extrapolatedSBmzSPk, i, j, k) = vector(mz, i, j, k);
						vector(extrapolatedSBNullSPi, i, j, k) = vector(Null, i, j, k);
						vector(extrapolatedSBNullSPj, i, j, k) = vector(Null, i, j, k);
						vector(extrapolatedSBNullSPk, i, j, k) = vector(Null, i, j, k);
						vector(extrapolatedSBpSPi, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBpSPj, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBpSPk, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBfxSPi, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfxSPj, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfxSPk, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfySPi, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfySPj, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfySPk, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfzSPi, i, j, k) = vector(fz, i, j, k);
						vector(extrapolatedSBfzSPj, i, j, k) = vector(fz, i, j, k);
						vector(extrapolatedSBfzSPk, i, j, k) = vector(fz, i, j, k);
					}
					if ((vector(FOV_xLower, i, j, k) > 0) && ((i + 1 < ilast && (vector(FOV_1,i + 1, j, k) > 0)) || (i + 2 < ilast && (vector(FOV_1,i + 2, j, k) > 0)))) {
						vector(extrapolatedSBNullSPi, i, j, k) = vector(Null, i, j, k);
						vector(extrapolatedSBNullSPj, i, j, k) = vector(Null, i, j, k);
						vector(extrapolatedSBNullSPk, i, j, k) = vector(Null, i, j, k);
					}
					if ((vector(FOV_xUpper, i, j, k) > 0) && ((i - 1 >= 0 && (vector(FOV_1, i - 1, j, k) > 0)) || (i - 2 >= 0 && (vector(FOV_1, i - 2, j, k) > 0)))) {
						vector(extrapolatedSBNullSPi, i, j, k) = vector(Null, i, j, k);
						vector(extrapolatedSBNullSPj, i, j, k) = vector(Null, i, j, k);
						vector(extrapolatedSBNullSPk, i, j, k) = vector(Null, i, j, k);
					}
					if ((vector(FOV_yLower, i, j, k) > 0) && ((j + 1 < jlast && (vector(FOV_1,i, j + 1, k) > 0)) || (j + 2 < jlast && (vector(FOV_1,i, j + 2, k) > 0)))) {
						vector(extrapolatedSBrhoSPi, i, j, k) = vector(rho, i, j, k);
						vector(extrapolatedSBrhoSPj, i, j, k) = vector(rho, i, j, k);
						vector(extrapolatedSBrhoSPk, i, j, k) = vector(rho, i, j, k);
						vector(extrapolatedSBmxSPi, i, j, k) = vector(mx, i, j, k);
						vector(extrapolatedSBmxSPj, i, j, k) = vector(mx, i, j, k);
						vector(extrapolatedSBmxSPk, i, j, k) = vector(mx, i, j, k);
						vector(extrapolatedSBmySPi, i, j, k) = vector(my, i, j, k);
						vector(extrapolatedSBmySPj, i, j, k) = vector(my, i, j, k);
						vector(extrapolatedSBmySPk, i, j, k) = vector(my, i, j, k);
						vector(extrapolatedSBmzSPi, i, j, k) = vector(mz, i, j, k);
						vector(extrapolatedSBmzSPj, i, j, k) = vector(mz, i, j, k);
						vector(extrapolatedSBmzSPk, i, j, k) = vector(mz, i, j, k);
						vector(extrapolatedSBNullSPi, i, j, k) = vector(Null, i, j, k);
						vector(extrapolatedSBNullSPj, i, j, k) = vector(Null, i, j, k);
						vector(extrapolatedSBNullSPk, i, j, k) = vector(Null, i, j, k);
						vector(extrapolatedSBpSPi, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBpSPj, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBpSPk, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBfxSPi, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfxSPj, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfxSPk, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfySPi, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfySPj, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfySPk, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfzSPi, i, j, k) = vector(fz, i, j, k);
						vector(extrapolatedSBfzSPj, i, j, k) = vector(fz, i, j, k);
						vector(extrapolatedSBfzSPk, i, j, k) = vector(fz, i, j, k);
					}
					if ((vector(FOV_yUpper, i, j, k) > 0) && ((j - 1 >= 0 && (vector(FOV_1, i, j - 1, k) > 0)) || (j - 2 >= 0 && (vector(FOV_1, i, j - 2, k) > 0)))) {
						vector(extrapolatedSBrhoSPi, i, j, k) = vector(rho, i, j, k);
						vector(extrapolatedSBrhoSPj, i, j, k) = vector(rho, i, j, k);
						vector(extrapolatedSBrhoSPk, i, j, k) = vector(rho, i, j, k);
						vector(extrapolatedSBmxSPi, i, j, k) = vector(mx, i, j, k);
						vector(extrapolatedSBmxSPj, i, j, k) = vector(mx, i, j, k);
						vector(extrapolatedSBmxSPk, i, j, k) = vector(mx, i, j, k);
						vector(extrapolatedSBmySPi, i, j, k) = vector(my, i, j, k);
						vector(extrapolatedSBmySPj, i, j, k) = vector(my, i, j, k);
						vector(extrapolatedSBmySPk, i, j, k) = vector(my, i, j, k);
						vector(extrapolatedSBmzSPi, i, j, k) = vector(mz, i, j, k);
						vector(extrapolatedSBmzSPj, i, j, k) = vector(mz, i, j, k);
						vector(extrapolatedSBmzSPk, i, j, k) = vector(mz, i, j, k);
						vector(extrapolatedSBNullSPi, i, j, k) = vector(Null, i, j, k);
						vector(extrapolatedSBNullSPj, i, j, k) = vector(Null, i, j, k);
						vector(extrapolatedSBNullSPk, i, j, k) = vector(Null, i, j, k);
						vector(extrapolatedSBpSPi, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBpSPj, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBpSPk, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBfxSPi, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfxSPj, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfxSPk, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfySPi, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfySPj, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfySPk, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfzSPi, i, j, k) = vector(fz, i, j, k);
						vector(extrapolatedSBfzSPj, i, j, k) = vector(fz, i, j, k);
						vector(extrapolatedSBfzSPk, i, j, k) = vector(fz, i, j, k);
					}
					if ((vector(FOV_zLower, i, j, k) > 0) && ((k + 1 < klast && (vector(FOV_1,i, j, k + 1) > 0)) || (k + 2 < klast && (vector(FOV_1,i, j, k + 2) > 0)))) {
						vector(extrapolatedSBrhoSPi, i, j, k) = vector(rho, i, j, k);
						vector(extrapolatedSBrhoSPj, i, j, k) = vector(rho, i, j, k);
						vector(extrapolatedSBrhoSPk, i, j, k) = vector(rho, i, j, k);
						vector(extrapolatedSBmxSPi, i, j, k) = vector(mx, i, j, k);
						vector(extrapolatedSBmxSPj, i, j, k) = vector(mx, i, j, k);
						vector(extrapolatedSBmxSPk, i, j, k) = vector(mx, i, j, k);
						vector(extrapolatedSBmySPi, i, j, k) = vector(my, i, j, k);
						vector(extrapolatedSBmySPj, i, j, k) = vector(my, i, j, k);
						vector(extrapolatedSBmySPk, i, j, k) = vector(my, i, j, k);
						vector(extrapolatedSBmzSPi, i, j, k) = vector(mz, i, j, k);
						vector(extrapolatedSBmzSPj, i, j, k) = vector(mz, i, j, k);
						vector(extrapolatedSBmzSPk, i, j, k) = vector(mz, i, j, k);
						vector(extrapolatedSBNullSPi, i, j, k) = vector(Null, i, j, k);
						vector(extrapolatedSBNullSPj, i, j, k) = vector(Null, i, j, k);
						vector(extrapolatedSBNullSPk, i, j, k) = vector(Null, i, j, k);
						vector(extrapolatedSBpSPi, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBpSPj, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBpSPk, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBfxSPi, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfxSPj, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfxSPk, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfySPi, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfySPj, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfySPk, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfzSPi, i, j, k) = vector(fz, i, j, k);
						vector(extrapolatedSBfzSPj, i, j, k) = vector(fz, i, j, k);
						vector(extrapolatedSBfzSPk, i, j, k) = vector(fz, i, j, k);
					}
					if ((vector(FOV_zUpper, i, j, k) > 0) && ((k - 1 >= 0 && (vector(FOV_1, i, j, k - 1) > 0)) || (k - 2 >= 0 && (vector(FOV_1, i, j, k - 2) > 0)))) {
						vector(extrapolatedSBrhoSPi, i, j, k) = vector(rho, i, j, k);
						vector(extrapolatedSBrhoSPj, i, j, k) = vector(rho, i, j, k);
						vector(extrapolatedSBrhoSPk, i, j, k) = vector(rho, i, j, k);
						vector(extrapolatedSBmxSPi, i, j, k) = vector(mx, i, j, k);
						vector(extrapolatedSBmxSPj, i, j, k) = vector(mx, i, j, k);
						vector(extrapolatedSBmxSPk, i, j, k) = vector(mx, i, j, k);
						vector(extrapolatedSBmySPi, i, j, k) = vector(my, i, j, k);
						vector(extrapolatedSBmySPj, i, j, k) = vector(my, i, j, k);
						vector(extrapolatedSBmySPk, i, j, k) = vector(my, i, j, k);
						vector(extrapolatedSBmzSPi, i, j, k) = vector(mz, i, j, k);
						vector(extrapolatedSBmzSPj, i, j, k) = vector(mz, i, j, k);
						vector(extrapolatedSBmzSPk, i, j, k) = vector(mz, i, j, k);
						vector(extrapolatedSBNullSPi, i, j, k) = vector(Null, i, j, k);
						vector(extrapolatedSBNullSPj, i, j, k) = vector(Null, i, j, k);
						vector(extrapolatedSBNullSPk, i, j, k) = vector(Null, i, j, k);
						vector(extrapolatedSBpSPi, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBpSPj, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBpSPk, i, j, k) = vector(p, i, j, k);
						vector(extrapolatedSBfxSPi, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfxSPj, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfxSPk, i, j, k) = vector(fx, i, j, k);
						vector(extrapolatedSBfySPi, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfySPj, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfySPk, i, j, k) = vector(fy, i, j, k);
						vector(extrapolatedSBfzSPi, i, j, k) = vector(fz, i, j, k);
						vector(extrapolatedSBfzSPj, i, j, k) = vector(fz, i, j, k);
						vector(extrapolatedSBfzSPk, i, j, k) = vector(fz, i, j, k);
					}
				}
			}
		}
	}
	if (d_refinedTimeStepping) {
		if (!hierarchy->finerLevelExists(ln) && last_step) {
			int currentLevelNumber = ln;
			while (currentLevelNumber > 0 && current_iteration[currentLevelNumber] % hierarchy->getRatioToCoarserLevel(currentLevelNumber).max() == 0) {
				d_coarsen_schedule[currentLevelNumber]->coarsenData();
				d_bdry_sched_postCoarsen[currentLevelNumber - 1]->fillData(current_time, false);
				currentLevelNumber--;
			}
		}
	} else {
		if (ln > 0) {
			d_coarsen_schedule[ln]->coarsenData();
			d_bdry_sched_postCoarsen[ln - 1]->fillData(current_time, false);
		}
	}
	

	t_step->stop();


	//Output
	t_output->start();
	if (ln == hierarchy->getFinestLevelNumber() && viz_mesh_dump_interval > 0) {
		if (previous_iteration < next_mesh_dump_iteration && outputCycle >= next_mesh_dump_iteration) {
			d_visit_data_writer->writePlotData(hierarchy, outputCycle, new_time);
			next_mesh_dump_iteration = next_mesh_dump_iteration + viz_mesh_dump_interval;
			while (outputCycle >= next_mesh_dump_iteration) {
				next_mesh_dump_iteration = next_mesh_dump_iteration + viz_mesh_dump_interval;
			}
		}
	}
	//Slicer output
	if (ln == hierarchy->getFinestLevelNumber() && d_slicer_output_period.size() > 0) {
		int i = 0;
		for (std::vector<std::shared_ptr<SlicerDataWriter> >::iterator it = d_sliceWriters.begin(); it != d_sliceWriters.end(); ++it) {
			if (d_slicer_output_period[i] > 0 && previous_iteration < next_slice_dump_iteration[i] && outputCycle >= next_slice_dump_iteration[i]) {
				(*it)->writePlotData(hierarchy, outputCycle, new_time);
				next_slice_dump_iteration[i] = next_slice_dump_iteration[i] + d_slicer_output_period[i];
				while (outputCycle >= next_slice_dump_iteration[i]) {
					next_slice_dump_iteration[i] = next_slice_dump_iteration[i] + d_slicer_output_period[i];
				}
			}
			i++;
		}
	}

	//Spherical output
	if (ln == hierarchy->getFinestLevelNumber() && d_sphere_output_period.size() > 0) {
		int i = 0;
		for (std::vector<std::shared_ptr<SphereDataWriter> >::iterator it = d_sphereWriters.begin(); it != d_sphereWriters.end(); ++it) {
			if (d_sphere_output_period[i] > 0 && previous_iteration < next_sphere_dump_iteration[i] && outputCycle >= next_sphere_dump_iteration[i]) {
				(*it)->writePlotData(hierarchy, outputCycle, new_time);
				next_sphere_dump_iteration[i] = next_sphere_dump_iteration[i] + d_sphere_output_period[i];
				while (outputCycle >= next_sphere_dump_iteration[i]) {
					next_sphere_dump_iteration[i] = next_sphere_dump_iteration[i] + d_sphere_output_period[i];
				}
			}
			i++;
		}
	}

	//Integration output
	if (ln == hierarchy->getFinestLevelNumber() && d_integration_output_period.size() > 0) {
		int i = 0;
		for (std::vector<std::shared_ptr<IntegrateDataWriter> >::iterator it = d_integrateDataWriters.begin(); it != d_integrateDataWriters.end(); ++it) {
			if (d_integration_output_period[i] > 0 && previous_iteration < next_integration_dump_iteration[i] && outputCycle >= next_integration_dump_iteration[i]) {
				(*it)->writePlotData(hierarchy, outputCycle, new_time);
				next_integration_dump_iteration[i] = next_integration_dump_iteration[i] + d_integration_output_period[i];
				while (outputCycle >= next_integration_dump_iteration[i]) {
					next_integration_dump_iteration[i] = next_integration_dump_iteration[i] + d_integration_output_period[i];
				}
			}
			i++;
		}
	}

	//Point output
	if (ln == hierarchy->getFinestLevelNumber() && d_point_output_period.size() > 0) {
		int i = 0;
		for (std::vector<std::shared_ptr<PointDataWriter> >::iterator it = d_pointDataWriters.begin(); it != d_pointDataWriters.end(); ++it) {
			if (d_point_output_period[i] > 0 && previous_iteration < next_point_dump_iteration[i] && outputCycle >= next_point_dump_iteration[i]) {
				(*it)->writePlotData(hierarchy, outputCycle, new_time);
				next_point_dump_iteration[i] = next_point_dump_iteration[i] + d_point_output_period[i];
				while (outputCycle >= next_point_dump_iteration[i]) {
					next_point_dump_iteration[i] = next_point_dump_iteration[i] + d_point_output_period[i];
				}
			}
			i++;
		}
	}


	t_output->stop();

	if (mpi.getRank() == 0 && d_output_interval > 0 ) {
		if (previous_iteration < next_console_output && outputCycle >= next_console_output) {
			int currentLevelNumber = ln;
			while (currentLevelNumber > 0) {
				currentLevelNumber--;
				cout <<"  ";
			}

			cout << "Level "<<ln<<". Iteration " << current_iteration[ln]<<". Time "<<current_time<<"."<< endl;
			if (ln == hierarchy->getFinestLevelNumber()) {
				next_console_output = next_console_output + d_output_interval;
				while (outputCycle >= next_console_output) {
					next_console_output = next_console_output + d_output_interval;
				}
			
			}
		}
	}

	if (d_timer_output_interval > 0 ) {
		if (previous_iteration < next_timer_output && outputCycle >= next_timer_output) {
			if (ln == hierarchy->getFinestLevelNumber()) {
				//Print timers
				if (mpi.getRank() == 0) {
					tbox::TimerManager::getManager()->print(cout);
				}
				else {
					if (ln == hierarchy->getFinestLevelNumber()) {
						//Dispose other processor timers
						//SAMRAI needs all processors run tbox::TimerManager::getManager()->print, otherwise it hungs
						std::ofstream ofs;
						ofs.setstate(std::ios_base::badbit);
						tbox::TimerManager::getManager()->print(ofs);
					}
				}
				next_timer_output = next_timer_output + d_timer_output_interval;
				while (outputCycle >= next_timer_output) {
					next_timer_output = next_timer_output + d_timer_output_interval;
				}
			
			}
		}
	}

	return simPlat_dt;
}

/*
 * Checks the finalization conditions              
 */
bool Problem::checkFinalization(const double current_time, const double simPlat_dt)
{
	if (greaterEq(current_time, tend)) { 
		return true;
	}
	return false;
	
	

}

void Problem::putToRestart(MainRestartData& mrd) {
	mrd.setNextSliceDumpIteration(next_slice_dump_iteration);
	mrd.setNextSphereDumpIteration(next_sphere_dump_iteration);
	mrd.setNextMeshDumpIteration(next_mesh_dump_iteration);
	mrd.setNextIntegrationDumpIteration(next_integration_dump_iteration);
	mrd.setNextPointDumpIteration(next_point_dump_iteration);

	mrd.setCurrentIteration(current_iteration);
	mrd.setNextConsoleOutputIteration(next_console_output);
	mrd.setNextTimerOutputIteration(next_timer_output);
}

void Problem::getFromRestart(MainRestartData& mrd) {
	next_slice_dump_iteration = mrd.getNextSliceDumpIteration();
	next_sphere_dump_iteration = mrd.getNextSphereDumpIteration();
	next_mesh_dump_iteration = mrd.getNextMeshDumpIteration();
	next_integration_dump_iteration = mrd.getNextIntegrationDumpIteration();
	next_point_dump_iteration = mrd.getNextPointDumpIteration();

	current_iteration = mrd.getCurrentIteration();
	next_console_output = mrd.getNextConsoleOutputIteration();
	next_timer_output = mrd.getNextTimerOutputIteration();
}

void Problem::allocateAfterRestart() {
	for (int il = 0; il < d_patch_hierarchy->getNumberOfLevels(); il++) {
		std::shared_ptr< hier::PatchLevel > level(d_patch_hierarchy->getPatchLevel(il));
		level->allocatePatchData(d_mask_id);
		level->allocatePatchData(d_interior_regridding_value_id);
		level->allocatePatchData(d_nonSync_regridding_tag_id);
		level->allocatePatchData(d_interior_i_id);
		level->allocatePatchData(d_interior_j_id);
		level->allocatePatchData(d_interior_k_id);
		level->allocatePatchData(d_SpeedSBi_3_id);
		level->allocatePatchData(d_SpeedSBj_3_id);
		level->allocatePatchData(d_SpeedSBk_3_id);
		level->allocatePatchData(d_rk1Null_id);
		level->allocatePatchData(d_rk2Null_id);
		level->allocatePatchData(d_SpeedSBi_1_id);
		level->allocatePatchData(d_SpeedSBj_1_id);
		level->allocatePatchData(d_SpeedSBk_1_id);
		level->allocatePatchData(d_ad_mx_o0_t0_m0_l0_1_id);
		level->allocatePatchData(d_ad_my_o0_t3_m0_l0_1_id);
		level->allocatePatchData(d_ad_mz_o0_t3_m0_l0_1_id);
		level->allocatePatchData(d_FluxSBimx_1_id);
		level->allocatePatchData(d_FluxSBimy_1_id);
		level->allocatePatchData(d_FluxSBimz_1_id);
		level->allocatePatchData(d_FluxSBjmx_1_id);
		level->allocatePatchData(d_FluxSBjmy_1_id);
		level->allocatePatchData(d_FluxSBjmz_1_id);
		level->allocatePatchData(d_FluxSBkmx_1_id);
		level->allocatePatchData(d_FluxSBkmy_1_id);
		level->allocatePatchData(d_FluxSBkmz_1_id);
		level->allocatePatchData(d_rk1rho_id);
		level->allocatePatchData(d_rk1mx_id);
		level->allocatePatchData(d_rk1my_id);
		level->allocatePatchData(d_rk1mz_id);
		level->allocatePatchData(d_rk2rho_id);
		level->allocatePatchData(d_rk2mx_id);
		level->allocatePatchData(d_rk2my_id);
		level->allocatePatchData(d_rk2mz_id);
		level->allocatePatchData(d_d_i_rho_mx_my_mz_p_fx_fy_fz_id);
		level->allocatePatchData(d_d_j_rho_mx_my_mz_p_fx_fy_fz_id);
		level->allocatePatchData(d_d_k_rho_mx_my_mz_p_fx_fy_fz_id);
		level->allocatePatchData(d_rho_p_id);
		level->allocatePatchData(d_mx_p_id);
		level->allocatePatchData(d_my_p_id);
		level->allocatePatchData(d_mz_p_id);
		level->allocatePatchData(d_Null_p_id);
	}

}

/*
 *  Cell tagging routine - tag cells that require refinement based on a provided condition. 
 */
void Problem::applyGradientDetector(
   const std::shared_ptr< hier::PatchHierarchy >& hierarchy, 
   const int level_number,
   const double time, 
   const int tag_index,
   const bool initial_time,
   const bool uses_richardson_extrapolation_too) 
{
	if (d_regridding && level_number + 1 >= d_regridding_min_level && level_number + 1 <= d_regridding_max_level) {
		hier::VariableDatabase *vdb = hier::VariableDatabase::getDatabase();
		if (!(vdb->checkVariableExists(d_regridding_field))) {
			TBOX_ERROR(d_object_name << ": Regridding field selected not found:n"					<<  d_regridding_field<<  "n");
		}

		std::shared_ptr< hier::PatchLevel > level(hierarchy->getPatchLevel(level_number));

		for (hier::PatchLevel::iterator ip(level->begin()); ip != level->end(); ++ip) {
			const std::shared_ptr< hier::Patch >& patch = *ip;
			int* tags = ((pdat::CellData<int> *) patch->getPatchData(tag_index).get())->getPointer();
			int* regridding_tag = ((pdat::NodeData<int> *) patch->getPatchData(d_nonSync_regridding_tag_id).get())->getPointer();
			const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
			const double* dx  = patch_geom->getDx();
			const hier::Index tfirst = patch->getPatchData(tag_index)->getGhostBox().lower();
			const hier::Index tlast  = patch->getPatchData(tag_index)->getGhostBox().upper();
			const hier::Index boxfirst = patch->getBox().lower();
			const hier::Index boxlast  = patch->getBox().upper();
			int ilast = boxlast(0)-boxfirst(0)+2+2*d_ghost_width;
			int itlast = tlast(0)-tfirst(0)+1;
			int jlast = boxlast(1)-boxfirst(1)+2+2*d_ghost_width;
			int jtlast = tlast(1)-tfirst(1)+1;
			int klast = boxlast(2)-boxfirst(2)+2+2*d_ghost_width;
			int ktlast = tlast(2)-tfirst(2)+1;
			for(int index2 = 0; index2 < (tlast(2)-tfirst(2))+1; index2++) {
				for(int index1 = 0; index1 < (tlast(1)-tfirst(1))+1; index1++) {
					for(int index0 = 0; index0 < (tlast(0)-tfirst(0))+1; index0++) {
						vectorT(tags,index0, index1, index2) = 0;
					}
				}
			}
			for(int index2 = 0; index2 < klast; index2++) {
				for(int index1 = 0; index1 < jlast; index1++) {
					for(int index0 = 0; index0 < ilast; index0++) {
						vector(regridding_tag,index0, index1, index2) = 0;
					}
				}
			}
			if (vdb->checkVariableExists(d_regridding_field)) {
				//Mesh
				if (d_regridding_type == "GRADIENT") {
					int regrid_field_id = vdb->getVariable(d_regridding_field)->getInstanceIdentifier();
					double* regrid_field = ((pdat::NodeData<double> *) patch->getPatchData(regrid_field_id).get())->getPointer();
					double* regridding_value = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_regridding_value_id).get())->getPointer();
					for(int index2 = 0; index2 < klast - 1; index2++) {
						for(int index1 = 0; index1 < jlast - 1; index1++) {
							for(int index0 = 0; index0 < ilast - 1; index0++) {
								if (vector(regrid_field, index0, index1, index2)!=0) {
									if ((fabs(vector(regrid_field, index0+1+d_ghost_width, index1+d_ghost_width, index2+d_ghost_width)-vector(regrid_field, index0+d_ghost_width, index1+d_ghost_width, index2+d_ghost_width)) > d_regridding_compressionFactor * MIN(fabs(vector(regrid_field, index0+2+d_ghost_width, index1+d_ghost_width, index2+d_ghost_width)-vector(regrid_field, index0+1+d_ghost_width, index1+d_ghost_width, index2+d_ghost_width)) + d_regridding_mOffset * pow(dx[0], 2), fabs(vector(regrid_field, index0+d_ghost_width, index1+d_ghost_width, index2+d_ghost_width)-vector(regrid_field, index0-1+d_ghost_width, index1+d_ghost_width, index2+d_ghost_width)) + d_regridding_mOffset * pow(dx[0], 2)) ) ||(fabs(vector(regrid_field, index0+d_ghost_width, index1+1+d_ghost_width, index2+d_ghost_width)-vector(regrid_field, index0+d_ghost_width, index1+d_ghost_width, index2+d_ghost_width)) > d_regridding_compressionFactor * MIN(fabs(vector(regrid_field, index0+d_ghost_width, index1+2+d_ghost_width, index2+d_ghost_width)-vector(regrid_field, index0+d_ghost_width, index1+1+d_ghost_width, index2+d_ghost_width)) + d_regridding_mOffset * pow(dx[1], 2), fabs(vector(regrid_field, index0+d_ghost_width, index1+d_ghost_width, index2+d_ghost_width)-vector(regrid_field, index0+d_ghost_width, index1-1+d_ghost_width, index2+d_ghost_width)) + d_regridding_mOffset * pow(dx[1], 2)) ) ||(fabs(vector(regrid_field, index0+d_ghost_width, index1+d_ghost_width, index2+1+d_ghost_width)-vector(regrid_field, index0+d_ghost_width, index1+d_ghost_width, index2+d_ghost_width)) > d_regridding_compressionFactor * MIN(fabs(vector(regrid_field, index0+d_ghost_width, index1+d_ghost_width, index2+2+d_ghost_width)-vector(regrid_field, index0+d_ghost_width, index1+d_ghost_width, index2+1+d_ghost_width)) + d_regridding_mOffset * pow(dx[2], 2), fabs(vector(regrid_field, index0+d_ghost_width, index1+d_ghost_width, index2+d_ghost_width)-vector(regrid_field, index0+d_ghost_width, index1+d_ghost_width, index2-1+d_ghost_width)) + d_regridding_mOffset * pow(dx[2], 2)) ) ) {
										if (index0 >= d_ghost_width && index0 < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1 >= d_ghost_width && index1 < (tlast(1)-tfirst(1))+1 + d_ghost_width && index2 >= d_ghost_width && index2 < (tlast(2)-tfirst(2))+1 + d_ghost_width) {
											vectorT(tags,index0 - d_ghost_width, index1 - d_ghost_width, index2 - d_ghost_width) = 1;
										}
										vector(regridding_tag,index0, index1, index2) = 1;
										//SAMRAI tagging
										if (index0 > d_ghost_width && index0 < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1 > d_ghost_width && index1 < (tlast(1)-tfirst(1))+1 + d_ghost_width && index2 > d_ghost_width && index2 < (tlast(2)-tfirst(2))+1 + d_ghost_width) {
											vectorT(tags, index0-d_ghost_width - 1, index1-d_ghost_width - 1, index2-d_ghost_width - 1) = 1;
										}
										if (index0 >= d_ghost_width && index0 < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1 > d_ghost_width && index1 < (tlast(1)-tfirst(1))+1 + d_ghost_width && index2 > d_ghost_width && index2 < (tlast(2)-tfirst(2))+1 + d_ghost_width) {
											vectorT(tags, index0-d_ghost_width, index1-d_ghost_width - 1, index2-d_ghost_width - 1) = 1;
										}
										if (index0 > d_ghost_width && index0 < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1 >= d_ghost_width && index1 < (tlast(1)-tfirst(1))+1 + d_ghost_width && index2 > d_ghost_width && index2 < (tlast(2)-tfirst(2))+1 + d_ghost_width) {
											vectorT(tags, index0-d_ghost_width - 1, index1-d_ghost_width, index2-d_ghost_width - 1) = 1;
										}
										if (index0 >= d_ghost_width && index0 < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1 >= d_ghost_width && index1 < (tlast(1)-tfirst(1))+1 + d_ghost_width && index2 > d_ghost_width && index2 < (tlast(2)-tfirst(2))+1 + d_ghost_width) {
											vectorT(tags, index0-d_ghost_width, index1-d_ghost_width, index2-d_ghost_width - 1) = 1;
										}
										if (index0 > d_ghost_width && index0 < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1 > d_ghost_width && index1 < (tlast(1)-tfirst(1))+1 + d_ghost_width && index2 >= d_ghost_width && index2 < (tlast(2)-tfirst(2))+1 + d_ghost_width) {
											vectorT(tags, index0-d_ghost_width - 1, index1-d_ghost_width - 1, index2-d_ghost_width) = 1;
										}
										if (index0 > d_ghost_width && index0 < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1 >= d_ghost_width && index1 < (tlast(1)-tfirst(1))+1 + d_ghost_width && index2 >= d_ghost_width && index2 < (tlast(2)-tfirst(2))+1 + d_ghost_width) {
											vectorT(tags, index0-d_ghost_width - 1, index1-d_ghost_width, index2-d_ghost_width) = 1;
										}
										if (index0 >= d_ghost_width && index0 < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1 > d_ghost_width && index1 < (tlast(1)-tfirst(1))+1 + d_ghost_width && index2 >= d_ghost_width && index2 < (tlast(2)-tfirst(2))+1 + d_ghost_width) {
											vectorT(tags, index0-d_ghost_width, index1-d_ghost_width - 1, index2-d_ghost_width) = 1;
										}
										//Informative tagging
										if (index0 > 0 && index1 > 0 && index2 > 0) {
											if (vector(regridding_tag,index0-1,index1-1,index2-1) != 1)
												vector(regridding_tag,index0-1,index1-1,index2-1) = 1;
										}
										if (index1 > 0 && index2 > 0) {
											if (vector(regridding_tag,index0,index1-1,index2-1) != 1)
												vector(regridding_tag,index0,index1-1,index2-1) = 1;
										}
										if (index0 > 0 && index2 > 0) {
											if (vector(regridding_tag,index0-1,index1,index2-1) != 1)
												vector(regridding_tag,index0-1,index1,index2-1) = 1;
										}
										if (index2 > 0) {
											if (vector(regridding_tag,index0,index1,index2-1) != 1)
												vector(regridding_tag,index0,index1,index2-1) = 1;
										}
										if (index0 > 0 && index1 > 0) {
											if (vector(regridding_tag,index0-1,index1-1,index2) != 1)
												vector(regridding_tag,index0-1,index1-1,index2) = 1;
										}
										if (index0 > 0) {
											if (vector(regridding_tag,index0-1,index1,index2) != 1)
												vector(regridding_tag,index0-1,index1,index2) = 1;
										}
										if (index1 > 0) {
											if (vector(regridding_tag,index0,index1-1,index2) != 1)
												vector(regridding_tag,index0,index1-1,index2) = 1;
										}
										if (d_regridding_buffer > 0) {
											int distance;
											for(int index2b = MAX(0, index2 - d_regridding_buffer - 1); index2b < MIN(index2 + d_regridding_buffer + 1, klast); index2b++) {
												for(int index1b = MAX(0, index1 - d_regridding_buffer - 1); index1b < MIN(index1 + d_regridding_buffer + 1, jlast); index1b++) {
													for(int index0b = MAX(0, index0 - d_regridding_buffer - 1); index0b < MIN(index0 + d_regridding_buffer + 1, ilast); index0b++) {
														int distx = (index0b - index0);
														if (distx < 0) {
															distx++;
														}
														int disty = (index1b - index1);
														if (disty < 0) {
															disty++;
														}
														int distz = (index2b - index2);
														if (distz < 0) {
															distz++;
														}
														distance = 1 + MAX(MAX(abs(distx), abs(disty)), abs(distz));
														if (index0b >= d_ghost_width && index0b < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1b >= d_ghost_width && index1b < (tlast(1)-tfirst(1))+1 + d_ghost_width && index2b >= d_ghost_width && index2b < (tlast(2)-tfirst(2))+1 + d_ghost_width) {
															vectorT(tags,index0b-d_ghost_width,index1b-d_ghost_width,index2b-d_ghost_width) = 1;
														}
														if (vector(regridding_tag,index0b,index1b,index2b) == 0 || vector(regridding_tag,index0b,index1b,index2b) > distance) {
															vector(regridding_tag,index0b,index1b,index2b) = distance;
														}
													}
												}
											}
										}
			
									}
								}
							}
						}
					}
		
				} else {
					if (d_regridding_type == "FUNCTION") {
						int regrid_field_id = vdb->getVariable(d_regridding_field)->getInstanceIdentifier();
						double* regrid_field = ((pdat::NodeData<double> *) patch->getPatchData(regrid_field_id).get())->getPointer();
						double* regridding_value = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_regridding_value_id).get())->getPointer();
						for(int index2 = 0; index2 < klast; index2++) {
							for(int index1 = 0; index1 < jlast; index1++) {
								for(int index0 = 0; index0 < ilast; index0++) {
									vector(regridding_value, index0, index1, index2) = vector(regrid_field, index0, index1, index2);
									if (vector(regrid_field, index0, index1, index2) > d_regridding_threshold) {
										if (index0 >= d_ghost_width && index0 < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1 >= d_ghost_width && index1 < (tlast(1)-tfirst(1))+1 + d_ghost_width && index2 >= d_ghost_width && index2 < (tlast(2)-tfirst(2))+1 + d_ghost_width) {
											vectorT(tags,index0 - d_ghost_width, index1 - d_ghost_width, index2 - d_ghost_width) = 1;
										}
										vector(regridding_tag,index0, index1, index2) = 1;
										//SAMRAI tagging
										if (index0 > d_ghost_width && index0 < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1 > d_ghost_width && index1 < (tlast(1)-tfirst(1))+1 + d_ghost_width && index2 > d_ghost_width && index2 < (tlast(2)-tfirst(2))+1 + d_ghost_width) {
											vectorT(tags, index0-d_ghost_width - 1, index1-d_ghost_width - 1, index2-d_ghost_width - 1) = 1;
										}
										if (index0 >= d_ghost_width && index0 < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1 > d_ghost_width && index1 < (tlast(1)-tfirst(1))+1 + d_ghost_width && index2 > d_ghost_width && index2 < (tlast(2)-tfirst(2))+1 + d_ghost_width) {
											vectorT(tags, index0-d_ghost_width, index1-d_ghost_width - 1, index2-d_ghost_width - 1) = 1;
										}
										if (index0 > d_ghost_width && index0 < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1 >= d_ghost_width && index1 < (tlast(1)-tfirst(1))+1 + d_ghost_width && index2 > d_ghost_width && index2 < (tlast(2)-tfirst(2))+1 + d_ghost_width) {
											vectorT(tags, index0-d_ghost_width - 1, index1-d_ghost_width, index2-d_ghost_width - 1) = 1;
										}
										if (index0 >= d_ghost_width && index0 < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1 >= d_ghost_width && index1 < (tlast(1)-tfirst(1))+1 + d_ghost_width && index2 > d_ghost_width && index2 < (tlast(2)-tfirst(2))+1 + d_ghost_width) {
											vectorT(tags, index0-d_ghost_width, index1-d_ghost_width, index2-d_ghost_width - 1) = 1;
										}
										if (index0 > d_ghost_width && index0 < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1 > d_ghost_width && index1 < (tlast(1)-tfirst(1))+1 + d_ghost_width && index2 >= d_ghost_width && index2 < (tlast(2)-tfirst(2))+1 + d_ghost_width) {
											vectorT(tags, index0-d_ghost_width - 1, index1-d_ghost_width - 1, index2-d_ghost_width) = 1;
										}
										if (index0 > d_ghost_width && index0 < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1 >= d_ghost_width && index1 < (tlast(1)-tfirst(1))+1 + d_ghost_width && index2 >= d_ghost_width && index2 < (tlast(2)-tfirst(2))+1 + d_ghost_width) {
											vectorT(tags, index0-d_ghost_width - 1, index1-d_ghost_width, index2-d_ghost_width) = 1;
										}
										if (index0 >= d_ghost_width && index0 < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1 > d_ghost_width && index1 < (tlast(1)-tfirst(1))+1 + d_ghost_width && index2 >= d_ghost_width && index2 < (tlast(2)-tfirst(2))+1 + d_ghost_width) {
											vectorT(tags, index0-d_ghost_width, index1-d_ghost_width - 1, index2-d_ghost_width) = 1;
										}
										//Informative tagging
										if (index0 > 0 && index1 > 0 && index2 > 0) {
											if (vector(regridding_tag,index0-1,index1-1,index2-1) != 1)
												vector(regridding_tag,index0-1,index1-1,index2-1) = 1;
										}
										if (index1 > 0 && index2 > 0) {
											if (vector(regridding_tag,index0,index1-1,index2-1) != 1)
												vector(regridding_tag,index0,index1-1,index2-1) = 1;
										}
										if (index0 > 0 && index2 > 0) {
											if (vector(regridding_tag,index0-1,index1,index2-1) != 1)
												vector(regridding_tag,index0-1,index1,index2-1) = 1;
										}
										if (index2 > 0) {
											if (vector(regridding_tag,index0,index1,index2-1) != 1)
												vector(regridding_tag,index0,index1,index2-1) = 1;
										}
										if (index0 > 0 && index1 > 0) {
											if (vector(regridding_tag,index0-1,index1-1,index2) != 1)
												vector(regridding_tag,index0-1,index1-1,index2) = 1;
										}
										if (index0 > 0) {
											if (vector(regridding_tag,index0-1,index1,index2) != 1)
												vector(regridding_tag,index0-1,index1,index2) = 1;
										}
										if (index1 > 0) {
											if (vector(regridding_tag,index0,index1-1,index2) != 1)
												vector(regridding_tag,index0,index1-1,index2) = 1;
										}
										if (d_regridding_buffer > 0) {
											int distance;
											for(int index2b = MAX(0, index2 - d_regridding_buffer - 1); index2b < MIN(index2 + d_regridding_buffer + 1, klast); index2b++) {
												for(int index1b = MAX(0, index1 - d_regridding_buffer - 1); index1b < MIN(index1 + d_regridding_buffer + 1, jlast); index1b++) {
													for(int index0b = MAX(0, index0 - d_regridding_buffer - 1); index0b < MIN(index0 + d_regridding_buffer + 1, ilast); index0b++) {
														int distx = (index0b - index0);
														if (distx < 0) {
															distx++;
														}
														int disty = (index1b - index1);
														if (disty < 0) {
															disty++;
														}
														int distz = (index2b - index2);
														if (distz < 0) {
															distz++;
														}
														distance = 1 + MAX(MAX(abs(distx), abs(disty)), abs(distz));
														if (index0b >= d_ghost_width && index0b < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1b >= d_ghost_width && index1b < (tlast(1)-tfirst(1))+1 + d_ghost_width && index2b >= d_ghost_width && index2b < (tlast(2)-tfirst(2))+1 + d_ghost_width) {
															vectorT(tags,index0b-d_ghost_width,index1b-d_ghost_width,index2b-d_ghost_width) = 1;
														}
														if (vector(regridding_tag,index0b,index1b,index2b) == 0 || vector(regridding_tag,index0b,index1b,index2b) > distance) {
															vector(regridding_tag,index0b,index1b,index2b) = distance;
														}
													}
												}
											}
										}
									}
								}
							}
						}
			
					} else {
						if (d_regridding_type == "SHADOW") {
							if (!initial_time) {
								if (!(vdb->checkVariableExists(d_regridding_field_shadow))) {
									TBOX_ERROR(d_object_name << ": Regridding field selected not found:" <<  d_regridding_field_shadow<<  "");
								}
								int regrid_field_id = vdb->getVariable(d_regridding_field)->getInstanceIdentifier();
								double* regrid_field1 = ((pdat::NodeData<double> *) patch->getPatchData(regrid_field_id).get())->getPointer();
								int regrid_field_shadow_id = vdb->getVariable(d_regridding_field_shadow)->getInstanceIdentifier();
								double* regrid_field2 = ((pdat::NodeData<double> *) patch->getPatchData(regrid_field_shadow_id).get())->getPointer();
								double* regridding_value = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_regridding_value_id).get())->getPointer();
								for(int index2 = 0; index2 < klast; index2++) {
									for(int index1 = 0; index1 < jlast; index1++) {
										for(int index0 = 0; index0 < ilast; index0++) {
					
											double error = 2 * fabs(vector(regrid_field1, index0, index1, index2) - vector(regrid_field2, index0, index1, index2))/fabs(vector(regrid_field1, index0, index1, index2) + vector(regrid_field2, index0, index1, index2));
											vector(regridding_value, index0, index1, index2) = error;
											if (error > d_regridding_error) {
												if (index0 >= d_ghost_width && index0 < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1 >= d_ghost_width && index1 < (tlast(1)-tfirst(1))+1 + d_ghost_width && index2 >= d_ghost_width && index2 < (tlast(2)-tfirst(2))+1 + d_ghost_width) {
													vectorT(tags,index0 - d_ghost_width, index1 - d_ghost_width, index2 - d_ghost_width) = 1;
												}
												vector(regridding_tag,index0, index1, index2) = 1;
												//SAMRAI tagging
												if (index0 > d_ghost_width && index0 < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1 > d_ghost_width && index1 < (tlast(1)-tfirst(1))+1 + d_ghost_width && index2 > d_ghost_width && index2 < (tlast(2)-tfirst(2))+1 + d_ghost_width) {
													vectorT(tags, index0-d_ghost_width - 1, index1-d_ghost_width - 1, index2-d_ghost_width - 1) = 1;
												}
												if (index0 >= d_ghost_width && index0 < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1 > d_ghost_width && index1 < (tlast(1)-tfirst(1))+1 + d_ghost_width && index2 > d_ghost_width && index2 < (tlast(2)-tfirst(2))+1 + d_ghost_width) {
													vectorT(tags, index0-d_ghost_width, index1-d_ghost_width - 1, index2-d_ghost_width - 1) = 1;
												}
												if (index0 > d_ghost_width && index0 < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1 >= d_ghost_width && index1 < (tlast(1)-tfirst(1))+1 + d_ghost_width && index2 > d_ghost_width && index2 < (tlast(2)-tfirst(2))+1 + d_ghost_width) {
													vectorT(tags, index0-d_ghost_width - 1, index1-d_ghost_width, index2-d_ghost_width - 1) = 1;
												}
												if (index0 >= d_ghost_width && index0 < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1 >= d_ghost_width && index1 < (tlast(1)-tfirst(1))+1 + d_ghost_width && index2 > d_ghost_width && index2 < (tlast(2)-tfirst(2))+1 + d_ghost_width) {
													vectorT(tags, index0-d_ghost_width, index1-d_ghost_width, index2-d_ghost_width - 1) = 1;
												}
												if (index0 > d_ghost_width && index0 < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1 > d_ghost_width && index1 < (tlast(1)-tfirst(1))+1 + d_ghost_width && index2 >= d_ghost_width && index2 < (tlast(2)-tfirst(2))+1 + d_ghost_width) {
													vectorT(tags, index0-d_ghost_width - 1, index1-d_ghost_width - 1, index2-d_ghost_width) = 1;
												}
												if (index0 > d_ghost_width && index0 < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1 >= d_ghost_width && index1 < (tlast(1)-tfirst(1))+1 + d_ghost_width && index2 >= d_ghost_width && index2 < (tlast(2)-tfirst(2))+1 + d_ghost_width) {
													vectorT(tags, index0-d_ghost_width - 1, index1-d_ghost_width, index2-d_ghost_width) = 1;
												}
												if (index0 >= d_ghost_width && index0 < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1 > d_ghost_width && index1 < (tlast(1)-tfirst(1))+1 + d_ghost_width && index2 >= d_ghost_width && index2 < (tlast(2)-tfirst(2))+1 + d_ghost_width) {
													vectorT(tags, index0-d_ghost_width, index1-d_ghost_width - 1, index2-d_ghost_width) = 1;
												}
												//Informative tagging
												if (index0 > 0 && index1 > 0 && index2 > 0) {
													if (vector(regridding_tag,index0-1,index1-1,index2-1) != 1)
														vector(regridding_tag,index0-1,index1-1,index2-1) = 1;
												}
												if (index1 > 0 && index2 > 0) {
													if (vector(regridding_tag,index0,index1-1,index2-1) != 1)
														vector(regridding_tag,index0,index1-1,index2-1) = 1;
												}
												if (index0 > 0 && index2 > 0) {
													if (vector(regridding_tag,index0-1,index1,index2-1) != 1)
														vector(regridding_tag,index0-1,index1,index2-1) = 1;
												}
												if (index2 > 0) {
													if (vector(regridding_tag,index0,index1,index2-1) != 1)
														vector(regridding_tag,index0,index1,index2-1) = 1;
												}
												if (index0 > 0 && index1 > 0) {
													if (vector(regridding_tag,index0-1,index1-1,index2) != 1)
														vector(regridding_tag,index0-1,index1-1,index2) = 1;
												}
												if (index0 > 0) {
													if (vector(regridding_tag,index0-1,index1,index2) != 1)
														vector(regridding_tag,index0-1,index1,index2) = 1;
												}
												if (index1 > 0) {
													if (vector(regridding_tag,index0,index1-1,index2) != 1)
														vector(regridding_tag,index0,index1-1,index2) = 1;
												}
												if (d_regridding_buffer > 0) {
													int distance;
													for(int index2b = MAX(0, index2 - d_regridding_buffer - 1); index2b < MIN(index2 + d_regridding_buffer + 1, klast); index2b++) {
														for(int index1b = MAX(0, index1 - d_regridding_buffer - 1); index1b < MIN(index1 + d_regridding_buffer + 1, jlast); index1b++) {
															for(int index0b = MAX(0, index0 - d_regridding_buffer - 1); index0b < MIN(index0 + d_regridding_buffer + 1, ilast); index0b++) {
																int distx = (index0b - index0);
																if (distx < 0) {
																	distx++;
																}
																int disty = (index1b - index1);
																if (disty < 0) {
																	disty++;
																}
																int distz = (index2b - index2);
																if (distz < 0) {
																	distz++;
																}
																distance = 1 + MAX(MAX(abs(distx), abs(disty)), abs(distz));
																if (index0b >= d_ghost_width && index0b < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1b >= d_ghost_width && index1b < (tlast(1)-tfirst(1))+1 + d_ghost_width && index2b >= d_ghost_width && index2b < (tlast(2)-tfirst(2))+1 + d_ghost_width) {
																	vectorT(tags,index0b-d_ghost_width,index1b-d_ghost_width,index2b-d_ghost_width) = 1;
																}
																if (vector(regridding_tag,index0b,index1b,index2b) == 0 || vector(regridding_tag,index0b,index1b,index2b) > distance) {
																	vector(regridding_tag,index0b,index1b,index2b) = distance;
																}
															}
														}
													}
												}
					
											}
										}
									}
								}
					
							}
				
						}
					}
				}
			}
		}
		//Buffer synchronization if needed
		if (d_regridding_buffer > d_ghost_width) {
			d_tagging_fill->createSchedule(level)->fillData(0, false);
		}
		if (d_regridding_buffer > 0) {
			for (hier::PatchLevel::iterator ip(level->begin()); ip != level->end(); ++ip) {
				const std::shared_ptr< hier::Patch >& patch = *ip;
				int* tags = ((pdat::CellData<int> *) patch->getPatchData(tag_index).get())->getPointer();
				int* regridding_tag = ((pdat::NodeData<int> *) patch->getPatchData(d_nonSync_regridding_tag_id).get())->getPointer();
				const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
				const double* dx  = patch_geom->getDx();
				const hier::Index tfirst = patch->getPatchData(tag_index)->getGhostBox().lower();
				const hier::Index tlast  = patch->getPatchData(tag_index)->getGhostBox().upper();
				const hier::Index boxfirst = patch->getBox().lower();
				const hier::Index boxlast  = patch->getBox().upper();
				int ilast = boxlast(0)-boxfirst(0)+2+2*d_ghost_width;
				int itlast = tlast(0)-tfirst(0)+1;
				int jlast = boxlast(1)-boxfirst(1)+2+2*d_ghost_width;
				int jtlast = tlast(1)-tfirst(1)+1;
				int klast = boxlast(2)-boxfirst(2)+2+2*d_ghost_width;
				int ktlast = tlast(2)-tfirst(2)+1;
	
				for(int index2 = 0; index2 < klast; index2++) {
					for(int index1 = 0; index1 < jlast; index1++) {
						for(int index0 = 0; index0 < ilast; index0++) {
	
							int value = vector(regridding_tag, index0, index1, index2);
							if (value > 0 && value < 1 + d_regridding_buffer) {
								int buffer_left = 1 + d_regridding_buffer - value;
								int distance;
								for(int index2b = MAX(0, index2 - buffer_left); index2b < MIN(index2 + buffer_left + 1, klast); index2b++) {
									for(int index1b = MAX(0, index1 - buffer_left); index1b < MIN(index1 + buffer_left + 1, jlast); index1b++) {
										for(int index0b = MAX(0, index0 - buffer_left); index0b < MIN(index0 + buffer_left + 1, ilast); index0b++) {
								
											distance = MAX(MAX(abs(index0b - index0), abs(index1b - index1)), abs(index2b - index2));
											if (distance > 0 && index0b >= d_ghost_width && index0b < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1b >= d_ghost_width && index1b < (tlast(1)-tfirst(1))+1 + d_ghost_width && index2b >= d_ghost_width && index2b < (tlast(2)-tfirst(2))+1 + d_ghost_width) {
												vectorT(tags, index0b - d_ghost_width, index1b - d_ghost_width, index2b - d_ghost_width) = 1;
											}
											if (distance > 0 && (vector(regridding_tag,index0b, index1b, index2b) == 0 || vector(regridding_tag, index0b, index1b, index2b) > value + distance)) {
												vector(regridding_tag, index0b, index1b, index2b) = value + distance;
											}
										}
									}
								}
								
							}
						}
					}
				}
	
			}
		}
	}
}



