/*@@
  @file      Functions.F
  @date      Fri May 31 13:10:17 CEST 2013
  @author    2
  @desc
             Functions for AdvectionwithSinus1D
             Include flux functions and discretization functions
  @enddesc
  @version 2
@@*/

#include "cctk.h"
#include "cctk_Parameters.h"
#include "cctk_Arguments.h"
#include "cctk_Functions.h"

	subroutine extrapolate_field(CCTK_ARGUMENTS, paramd_i_f, paramfrom_i_f, paramto_i_f, parami, paramto_f, paramFOV)
		implicit none

		DECLARE_CCTK_ARGUMENTS
		DECLARE_CCTK_PARAMETERS
		DECLARE_CCTK_FUNCTIONS

		CCTK_INT, INTENT(INOUT) :: paramd_i_f(1:cctk_lsh(1)), parami, paramFOV(1:cctk_lsh(1))
		CCTK_REAL, INTENT(INOUT) :: paramfrom_i_f(1:cctk_lsh(1)), paramto_i_f(1:cctk_lsh(1)), paramto_f(1:cctk_lsh(1))

		CCTK_REAL sign_i_ext, Ex_ext, Ey_ext, Exy_ext, Ex2_ext, Ew_ext, a1_ext, a0_ext


		CCTK_REAL deltax
		CCTK_REAL deltat

		deltax = (2.0d0 * 3.1415926535897932384626433832795028841971693993751d+0 - (0.0d0))/(cctk_gsh(1) - 4)
		deltat = CCTK_DELTA_TIME

		sign_i_ext = SIGN(1, paramd_i_f(parami))
		if (paramd_i_f(parami) .ne. 0.0d0) then
			Ex_ext = paramFOV(int(parami - (paramd_i_f(parami) + 1 * sign_i_ext))) * (parami - (paramd_i_f(parami) + 1.0d0 * sign_i_ext)) + paramFOV(int(parami - paramd_i_f(parami))) * (parami - (paramd_i_f(parami) + 0.0d0 * sign_i_ext))
			Ey_ext = paramFOV(int(parami - (paramd_i_f(parami) + 1 * sign_i_ext))) * paramfrom_i_f(int(parami - (paramd_i_f(parami) + 1 * sign_i_ext))) + paramFOV(int(parami - paramd_i_f(parami))) * paramfrom_i_f(int(parami - paramd_i_f(parami)))
			Exy_ext = paramFOV(int(parami - (paramd_i_f(parami) + 1 * sign_i_ext))) * (parami - (paramd_i_f(parami) + 1.0d0 * sign_i_ext)) * paramfrom_i_f(int(parami - (paramd_i_f(parami) + 1 * sign_i_ext))) + paramFOV(int(parami - paramd_i_f(parami))) * (parami - (paramd_i_f(parami) + 0.0d0 * sign_i_ext)) * paramfrom_i_f(int(parami - paramd_i_f(parami)))
			Ex2_ext = paramFOV(int(parami - (paramd_i_f(parami) + 1 * sign_i_ext))) * ((parami - (paramd_i_f(parami) + 1.0d0 * sign_i_ext)) ** 2.0d0) + paramFOV(int(parami - paramd_i_f(parami))) * ((parami - (paramd_i_f(parami) + 0.0d0 * sign_i_ext)) ** 2.0d0)
			Ew_ext = paramFOV(int(parami - (paramd_i_f(parami) + 1 * sign_i_ext))) + paramFOV(int(parami - paramd_i_f(parami)))
			a1_ext = (Exy_ext * Ew_ext - Ex_ext * Ey_ext) / (Ex2_ext * Ew_ext - Ex_ext ** 2.0d0)
			a0_ext = (Ey_ext - a1_ext * Ex_ext) / Ew_ext
			paramto_i_f(parami) = a0_ext + a1_ext * parami
		end if
		if (paramd_i_f(parami) .ne. 0.0d0) then
			paramto_f(parami) = paramto_i_f(parami)
		end if

	end subroutine extrapolate_field

	subroutine extrapolate_flux(CCTK_ARGUMENTS, paramd_i_f, paramfrom_i_f, paramto_i_f, parami, paramFOV)
		implicit none

		DECLARE_CCTK_ARGUMENTS
		DECLARE_CCTK_PARAMETERS
		DECLARE_CCTK_FUNCTIONS

		CCTK_INT, INTENT(INOUT) :: paramd_i_f(1:cctk_lsh(1)), parami, paramFOV(1:cctk_lsh(1))
		CCTK_REAL, INTENT(INOUT) :: paramfrom_i_f(1:cctk_lsh(1)), paramto_i_f(1:cctk_lsh(1))

		CCTK_REAL sign_i_ext, Ex_ext, Ey_ext, Exy_ext, Ex2_ext, Ew_ext, a1_ext, a0_ext


		CCTK_REAL deltax
		CCTK_REAL deltat

		deltax = (2.0d0 * 3.1415926535897932384626433832795028841971693993751d+0 - (0.0d0))/(cctk_gsh(1) - 4)
		deltat = CCTK_DELTA_TIME

		sign_i_ext = SIGN(1, paramd_i_f(parami))
		if (paramd_i_f(parami) .ne. 0.0d0) then
			Ex_ext = paramFOV(int(parami - (paramd_i_f(parami) + 1 * sign_i_ext))) * (parami - (paramd_i_f(parami) + 1.0d0 * sign_i_ext)) + paramFOV(int(parami - paramd_i_f(parami))) * (parami - (paramd_i_f(parami) + 0.0d0 * sign_i_ext))
			Ey_ext = paramFOV(int(parami - (paramd_i_f(parami) + 1 * sign_i_ext))) * paramfrom_i_f(int(parami - (paramd_i_f(parami) + 1 * sign_i_ext))) + paramFOV(int(parami - paramd_i_f(parami))) * paramfrom_i_f(int(parami - paramd_i_f(parami)))
			Exy_ext = paramFOV(int(parami - (paramd_i_f(parami) + 1 * sign_i_ext))) * (parami - (paramd_i_f(parami) + 1.0d0 * sign_i_ext)) * paramfrom_i_f(int(parami - (paramd_i_f(parami) + 1 * sign_i_ext))) + paramFOV(int(parami - paramd_i_f(parami))) * (parami - (paramd_i_f(parami) + 0.0d0 * sign_i_ext)) * paramfrom_i_f(int(parami - paramd_i_f(parami)))
			Ex2_ext = paramFOV(int(parami - (paramd_i_f(parami) + 1 * sign_i_ext))) * ((parami - (paramd_i_f(parami) + 1.0d0 * sign_i_ext)) ** 2.0d0) + paramFOV(int(parami - paramd_i_f(parami))) * ((parami - (paramd_i_f(parami) + 0.0d0 * sign_i_ext)) ** 2.0d0)
			Ew_ext = paramFOV(int(parami - (paramd_i_f(parami) + 1 * sign_i_ext))) + paramFOV(int(parami - paramd_i_f(parami)))
			a1_ext = (Exy_ext * Ew_ext - Ex_ext * Ey_ext) / (Ex2_ext * Ew_ext - Ex_ext ** 2.0d0)
			a0_ext = (Ey_ext - a1_ext * Ex_ext) / Ew_ext
			paramto_i_f(parami) = a0_ext + a1_ext * parami
		end if

	end subroutine extrapolate_flux

