/*@@
  @file      MappingSegments.F
  @date      Fri May 31 13:10:17 CEST 2013
  @author    2
  @desc
             Mapping segments for AdvectionwithSinus1D
  @enddesc
  @version 2
@@*/

define(leF,((abs(($1) - ($2))/FPC_epsilon_Cactus .lt. 10 .and. FLOOR(abs(($1) - ($2))/FPC_epsilon_Cactus) .lt. 1) .or. ($1) < ($2)))dnl
define(geF,((abs(($1) - ($2))/FPC_epsilon_Cactus .lt. 10 .and. FLOOR(abs(($1) - ($2))/FPC_epsilon_Cactus) .lt. 1) .or. ($2) < ($1)))dnl

#include "cctk.h"
#include "cctk_Parameters.h"
#include "cctk_Arguments.h"
#include "cctk_Functions.h"

	subroutine AdvectionwithSinus1D_mapping(CCTK_ARGUMENTS)

		implicit none

		DECLARE_CCTK_ARGUMENTS
		DECLARE_CCTK_PARAMETERS
		DECLARE_CCTK_FUNCTIONS

		CCTK_REAL FPC_epsilon_Cactus

!		Declare local variables
!		-----------------

		CCTK_INT i, iMapStart, iMapEnd, iterm, previousMapi, iWallAcc, iu, il, indexi
		logical interiorMapi
		CCTK_INT minBlock(1), maxBlock(1), unionsI, facePointI, status, ie1, ie2, ie3, proc, reduction_handle
		CCTK_REAL maxDistance, e1, e2, e3, SQRT3INV
		logical done, modif, checkStencil, s1, s2, s3, s4, checkStalled
!		Mapping variables
		logical workingGlobal, finishedGlobal, cleanStencil
		CCTK_INT working, finished, nodes, tmp1, tmp2, pred, first
		CCTK_INT, allocatable :: workingArray(:), finishedArray(:)
!		Deltas
		CCTK_REAL deltax
		CCTK_REAL deltat

		deltax = (2.0d0 * 3.1415926535897932384626433832795028841971693993751d+0 - (0.0d0))/(cctk_gsh(1) - 4)
		deltat = CCTK_DELTA_TIME

		allocate(workingArray(cctk_nProcs(cctkGH)))
		allocate(finishedArray(cctk_nProcs(cctkGH)))

		FPC_epsilon_Cactus = max(0.0d0, 2.0d0 * 3.1415926535897932384626433832795028841971693993751d+0) * 1.0E-10

		SQRT3INV = 1.0/SQRT(3.0)
!		Segment mapping
!		----------------
!		Segment: segmentI
		if (1 .le. cctk_ubnd(1) - cctk_lbnd(1) + 1) then
			iMapStart = 1
			iMapEnd = cctk_lsh(1)
			do i = iMapStart, iMapEnd
					FOV_1(i) = 100
					FOV_xLower(i) = 0
					FOV_xUpper(i) = 0
			end do
!			Check stencil
			do i = 1, cctk_lsh(1)
!				If wall, then it is considered exterior
				if (FOV_1(i) .gt. 0) then
					interior(i) = 1
				else
					interior(i) = -1
				end if
			end do
			do i = 1, cctk_lsh(1)
				if (FOV_1(i) .gt. 0) then
					call setStencilLimits(CCTK_PASS_FTOF, i, 1)
				end if
			end do
			modif = .true.
			do while (modif)
				modif = .false.
				do i = 1, cctk_lsh(1)
					if (interior(i) .eq. 1 .and. FOV_1(i) .eq. 0) then
					if (checkStencil(CCTK_PASS_FTOF, i, 1)) then
					modif = .true.
					end if
					end if
				end do
				do i = 1, cctk_lsh(1)
				if (interior(i) .eq. 2) then
				FOV_1(i) = 100
				FOV_xLower(i) = 0
				FOV_xUpper(i) = 0
				end if
				end do
			end do
		end if
!		Boundaries Mapping
!		x-Upper
		if (cctk_bbox(2) .eq. 1) then
			do i = cctk_lsh(1) - 1, cctk_lsh(1)
				FOV_xUpper(i) = 100
				FOV_1(i) = 0
				FOV_xLower(i) = 0
			end do
		end if
!		x-Lower
		if (cctk_bbox(1) .eq. 1) then
			do i = 1, 2
				FOV_xLower(i) = 100
				FOV_1(i) = 0
				FOV_xUpper(i) = 0
			end do
		end if


	end subroutine AdvectionwithSinus1D_mapping
	subroutine floodfill(CCTK_ARGUMENTS, i, pred, seg)
		implicit none

		DECLARE_CCTK_ARGUMENTS
		DECLARE_CCTK_PARAMETERS
		DECLARE_CCTK_FUNCTIONS
		CCTK_INT, INTENT(IN) :: i, pred, seg
		CCTK_INT hi(cctk_lsh(1))
		CCTK_INT ti, index, loop
		logical control(cctk_lsh(1))

		CCTK_INT FOV(X0AuxiliaryGroup)
		select case(seg)
			case (1)
				FOV = FOV_1
		end select
		do ti = 1 , cctk_lsh(1)
			control(ti) = .false.
		end do

		hi(1) = i
		index = 1
		do while (index .ne. 0)
			ti = hi(index)
			index = index - 1
			if (nonSync(ti) .eq. 0) then
				nonSync(ti) = pred
				interior(ti) = pred
				if (pred .eq. 2) then
				FOV(ti) = 100
				end if
				if (ti - 1 .ge. 1 .and. nonSync(ti - 1) .eq. 0) then
					if (.not. control(ti - 1)) then
						index = index + 1
						hi(index) = ti - 1
						control(ti - 1) = .true.
					end if
				end if
				if (ti + 1 .le. cctk_lsh(1) .and. nonSync(ti + 1) .eq. 0) then
					if (.not. control(ti + 1)) then
						index = index + 1
						hi(index) = ti + 1
						control(ti + 1) = .true.
					end if
				end if
			end if
		end do
		select case(seg)
			case (1)
				FOV_1 = FOV
		end select
	end subroutine floodfill
	logical function checkStalled(CCTK_ARGUMENTS, i, seg)
		implicit none

		DECLARE_CCTK_ARGUMENTS
		DECLARE_CCTK_PARAMETERS
		DECLARE_CCTK_FUNCTIONS

		CCTK_INT, INTENT(IN) :: i, seg
		logical notEnoughStencil
		CCTK_INT FOV_threshold
		CCTK_INT stencilAcc, stencilAccMax_i, it1;

		CCTK_INT FOV(X0AuxiliaryGroup)
		select case(seg)
			case (1)
				FOV = FOV_1
		end select
		notEnoughStencil = .false.
		FOV_threshold = 0
		if (FOV(i) .le. FOV_threshold) then
			notEnoughStencil = .true.
		else
			stencilAcc = 0
			stencilAccMax_i = 0
			do it1 = MAX(i - 2, 0), MIN(i + 2, cctk_lsh(1))
				if (FOV(it1) .gt. FOV_threshold) then
					stencilAcc = stencilAcc + 1
				else
					stencilAccMax_i = MAX(stencilAccMax_i, stencilAcc)
					stencilAcc = 0
				end if
			end do
			stencilAccMax_i = MAX(stencilAccMax_i, stencilAcc)
			if ((stencilAccMax_i .lt. 2)) then
				notEnoughStencil = .true.
			end if
		end if
		checkStalled = notEnoughStencil
	end function checkStalled
	subroutine setStencilLimits(CCTK_ARGUMENTS, i, v)
		implicit none

		DECLARE_CCTK_ARGUMENTS
		DECLARE_CCTK_PARAMETERS
		DECLARE_CCTK_FUNCTIONS

		CCTK_INT, INTENT(IN) :: i, v
		CCTK_INT iStart, iEnd, iti
		CCTK_INT shift
		CCTK_INT FOV(X0AuxiliaryGroup)

		select case(v)
			case (1)
				FOV = FOV_1
		end select
		shift = 0
		if(cctk_bbox(1) .eq. 1) then
			do while(i - ((2 - 1) - (2 - 1)/2) + shift .lt. 3)
				shift = shift + 1
			end do
		end if
		if(cctk_bbox(2) .eq. 1) then
			do while(i + (2 - 1)/2 + shift .gt. cctk_lsh(1) - 2)
				shift = shift - 1
			end do
		end if
		iStart = ((2 - 1) - (2 - 1)/2) - shift
		iEnd = (2 - 1)/2 + shift

		do iti = i - iStart, i + iEnd
			if(iti .ge. 1 .and. iti .le. cctk_lsh(1) .and. FOV(iti) .eq. 0) then
				interior(iti) = 1
			end if
		end do
		select case(v)
			case (1)
				FOV_1 = FOV
		end select
	end subroutine setStencilLimits

	logical function checkStencil(CCTK_ARGUMENTS,  i, v)
		implicit none

		DECLARE_CCTK_ARGUMENTS
		DECLARE_CCTK_PARAMETERS
		DECLARE_CCTK_FUNCTIONS

		CCTK_INT, INTENT(IN) :: i, v
		logical toGrow
		CCTK_INT iti
		CCTK_INT FOV(X0AuxiliaryGroup)

		select case(v)
			case (1)
				FOV = FOV_1
		end select
		toGrow = .false.
	if (i + 1 .le. cctk_lsh(1) .and. FOV(i + 1) .gt. 0 .and.  .not. toGrow) then
		do iti = i + 1, i + 2
			if ((iti .le. cctk_lsh(1)  .and. interior(iti) .lt. 1) .or. (iti .gt. cctk_lsh(1) .and. cctk_bbox(2) .eq. 1)) then
				toGrow = .true.
			end if
		end do
	end if
	if (i - 1 .ge. 1 .and. FOV(i - 1) .gt. 0 .and.  .not. toGrow) then
		do iti = i - 2, i - 1
			if ((iti .ge. 1  .and. interior(iti) .lt. 1) .or. (iti .lt. 1 .and. cctk_bbox(1) .eq. 1)) then
				toGrow = .true.
			end if
		end do
	end if
		if (toGrow) then
			interior(i) = 2
		end if
		checkStencil = toGrow
		select case(v)
			case (1)
				FOV_1 = FOV
		end select
	end function checkStencil
