/*@@
  @file      InitialConditions.F
  @date      Fri May 31 13:10:17 CEST 2013
  @author    2
  @desc
             Initial conditions for AdvectionwithSinus1D
  @enddesc
  @version 2
@@*/

#include "cctk.h"
#include "cctk_Parameters.h"
#include "cctk_Arguments.h"
#include "cctk_Functions.h"

	subroutine AdvectionwithSinus1D_initial(CCTK_ARGUMENTS)

		implicit none

		DECLARE_CCTK_ARGUMENTS
		DECLARE_CCTK_PARAMETERS
		DECLARE_CCTK_FUNCTIONS

!		Declare local variables
!		-----------------

		CCTK_INT dist_to_segment, status, ReflectionCounter
		CCTK_INT i, iStart, iEnd, iMapStart, iMapEnd

		CCTK_REAL deltax
		CCTK_REAL deltat

		deltax = (2.0d0 * 3.1415926535897932384626433832795028841971693993751d+0 - (0.0d0))/(cctk_gsh(1) - 4)
		deltat = CCTK_DELTA_TIME

		iStart = 1
		iEnd = cctk_lsh(1)


!		Initial conditions
!		----------------
		do i = iStart, iEnd
			if (FOV_1(i) .gt. 0) then
				phiCap(i) = SIN(0.0d0 + (i - (1 - cctk_lbnd(1))) * deltax)
			end if

!			Boundaries Initialization
		end do

!       Post-initialization synchronization
!       ----------------
		call CCTK_SyncGroup(status, cctkGH, "AdvectionwithSinus1D::fields_tl2")

	end subroutine AdvectionwithSinus1D_initial