/*************************************************************************
 *
 * This file is part of the SAMRAI distribution.  For full copyright
 * information, see COPYRIGHT and COPYING.LESSER.
 *
 * Copyright:     (c) 1997-2016 Lawrence Livermore National Security, LLC
 * Description:   Conservative linear refine operator for cell-centered
 *                double data on a Skeleton mesh.
 *
 ************************************************************************/

#ifndef included_ParticleRefineXD
#define included_ParticleRefineXD

#include "SAMRAI/SAMRAI_config.h"
#include "SAMRAI/hier/Box.h"
#include "SAMRAI/hier/IntVector.h"
#include "SAMRAI/hier/Patch.h"
#include "SAMRAI/hier/PatchHierarchy.h"
#include "SAMRAI/geom/CartesianGridGeometry.h"
#include "SAMRAI/geom/CartesianPatchGeometry.h"
#include "SAMRAI/pdat/IndexData.h"
#include "SAMRAI/pdat/CellVariable.h"
#include "Particle_particles.h"

#include "Particles.h"
#include "SAMRAI/tbox/Utilities.h"
#ifndef included_String
#include <string>
#define included_String
#endif
#include "SAMRAI/hier/RefineOperator.h"
#include "Functions.h"

using namespace std;
using namespace SAMRAI;

template<class T>
class ParticleRefine:
   public hier::RefineOperator
{
public:
   /**
    * Uninteresting default constructor.
    */
   ParticleRefine(
      std::shared_ptr<hier::PatchHierarchy >& patch_hierarchy,
      const std::shared_ptr<geom::CartesianGridGeometry >& grid_geom,
	const double initial_particle_separation_x, 
	const double initial_particle_separation_y, 

      const int ghost,
      const double influenceRadius);

   /**
    * Uninteresting virtual destructor.
    */
   virtual ~ParticleRefine();

   /**
    * The priority of cell-centered double conservative linear is 0.
    * It will be performed before any user-defined interpolation operations.
    */
   int
   getOperatorPriority() const;

   /**
    * The stencil width of the conservative linear interpolation operator is
    * the vector of ones.
    */
   hier::IntVector
   getStencilWidth(
      const tbox::Dimension& dim) const;

   /**
    * Refine the source component on the coarse patch to the destination
    * component on the fine patch using the cell-centered double conservative
    * linear interpolation operator.  Interpolation is performed on the
    * intersection of the destination patch and the fine box.   It is assumed
    * that the coarse patch contains sufficient data for the stencil width of
    * the refinement operator.
    */
   void
   refine(
      hier::Patch& fine,
      const hier::Patch& coarse,
      const int dst_component,
      const int src_component,
      const hier::BoxOverlap& fine_overlap,
      const hier::IntVector& ratio) const;

   void
   refine_particles(
      hier::Patch& fine,
      const hier::Patch& coarse,
      const int dst_component,
      const int src_component,
      const hier::Box& fine_box,
      const hier::IntVector& ratio) const;

void addParticleToFine(std::shared_ptr<pdat::IndexData<Particles<T>, pdat::CellGeometry > > cdata, std::shared_ptr<pdat::IndexData<Particles<T>, pdat::CellGeometry > > fdata, T& particle, int minx, int maxx, const double init_pos_x, const double particle_separation_f_x, int ii, int miny, int maxy, const double init_pos_y, const double particle_separation_f_y, int jj, const double* dx) const;

inline void
setStep(int step) {
  d_step = step;
}


void checkPosition(const hier::Patch& patch, const hier::Box& box, int index0, int index1, Particles<T>& particles) const;


private:

  static std::shared_ptr<tbox::Timer> t_interpolate;

	double xGlower, xGupper, d_initial_particle_separation_x, yGlower, yGupper, d_initial_particle_separation_y;

   std::shared_ptr<hier::PatchHierarchy > d_patch_hierarchy;

   const int d_ghost_width;

   const double d_influenceRadius;

   int d_step;
};

#endif
