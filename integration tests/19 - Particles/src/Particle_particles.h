#ifndef included_Particle_particles
#define included_Particle_particles

#include "SAMRAI/tbox/MessageStream.h"
#include "SAMRAI/hier/IntVector.h"
#include "SAMRAI/tbox/Database.h"
#include <vector>

/**
 * The SampleClass struct holds some dummy data and methods.  It's intent
 * is to indicate how a user could construct their own index data double.
 */
using namespace std;
using namespace SAMRAI;

class Particle_particles
{
public:

	inline int GetExpoBase2(double d);

	bool	Equal(double d1, double d2);

	/**
	* The default constructor.
	*/
	Particle_particles();

	/**
	* Constructor.
	*/
	Particle_particles(const int region);

	/**
	* The complete constructor.
	*/
	Particle_particles(const double fieldValue, const double* pos, const int region = 0);

	/**
	* Copy constructor.
	*/
	Particle_particles(const Particle_particles& data);
	Particle_particles(const Particle_particles& data, bool newId);

	~Particle_particles();

	//Setters 
	void setId(const int id);
	void setProcId(const int procId);
	void setFieldValue(string varName, double value);

	bool hasField(string varName);

	//Getters
	double getFieldValue(string varName);
	int getId();
	int getProcId();

	//Distance between particles
	inline double distance(Particle_particles* data)
	{
		double xd = (data->positionx - positionx);
		double yd = (data->positiony - positiony);
		return sqrt(xd * xd + yd * yd);
	}

	inline double distance(double* point)
	{
		double xd = (point[0] - positionx);
		double yd = (point[1] - positiony);
		return sqrt(xd * xd + yd * yd);
	}
	inline double distance_p(Particle_particles* data)
	{
		double xd = (data->positionx_p - positionx_p);
		double yd = (data->positiony_p - positiony_p);
		return sqrt(xd * xd + yd * yd);
	}

	inline double distance_p(double* point)
	{
		double xd = (point[0] - positionx_p);
		double yd = (point[1] - positiony_p);
		return sqrt(xd * xd + yd * yd);
	}
	inline double distancepred(Particle_particles* data)
	{
		double xd = (data->predpositionx - predpositionx);
		double yd = (data->predpositiony - predpositiony);
		return sqrt(xd * xd + yd * yd);
	}

	inline double distancepred(double* point)
	{
		double xd = (point[0] - predpositionx);
		double yd = (point[1] - predpositiony);
		return sqrt(xd * xd + yd * yd);
	}


	inline bool same(const Particle_particles& data)
	{
		return !(!Equal(positionx, data.positionx) || !Equal(positiony, data.positiony) || region != data.region);
	}
	Particle_particles* overlaps(const Particle_particles& data);

	Particle_particles& operator=(const Particle_particles& data);

	inline bool operator==(const Particle_particles& data)
	{
		return !((this != &data) || (id != data.id) || (procId != data.procId));
	}

	void packStream(tbox::MessageStream& stream);
	void unpackStream(tbox::MessageStream& stream, const hier::IntVector& offset);
	void getFromDatabase(std::shared_ptr<tbox::Database>& database, int particle);
	void putToDatabase(std::shared_ptr<tbox::Database>& database, int particle);

	static size_t size() {
		size_t bytes = tbox::MessageStream::getSizeof<double>(6 + 6) + tbox::MessageStream::getSizeof<int>(5);
		return(bytes);
	};

   	//Field value of the particle
	double field;
	double field_p;
	double auxiliaryField;
	double derAuxiliaryField;
	double m_derAuxiliaryField_o0_t0_l0_1;
	double predfield;


   	//Positions of the particle (Global index position)
	double positionx;
	double positionx_p;
	double positiony;
	double positiony_p;
	double predpositionx;
	double predpositiony;


	//Region information
	int region;
	int interior;
	int newRegion; //The region to become when entering the domain (boundaries)

	//Particle identifier
	int id;
	//Creator processor identifier
	int procId;

private:

	//Particle counter
	static int counter;


};

#endif
