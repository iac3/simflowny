#ifndef included_FunctionsXD
#define included_FunctionsXD

#include "SAMRAI/tbox/Utilities.h"
#include <string>
#include <math.h>
#include <vector>
#include "hdf5.h"
#include "Commons.h"
#include "SAMRAI/pdat/CellData.h"
#include "SAMRAI/pdat/CellGeometry.h"
#include "Particles.h"
#include "SAMRAI/hier/Index.h"
#include "SAMRAI/pdat/IndexData.h"


#define MIN(X,Y) ((X) < (Y) ? (X) : (Y))
#define MAX(X,Y) ((X) > (Y) ? (X) : (Y))
#define SIGN(X) (((X) > 0) - ((X) < 0))
#define greaterEq(a,b) ((fabs((a) - (b))/1.0E-15 > 10 ? false : (floor(fabs((a) - (b))/1.0E-15) < 1)) || (b)<(a))
#define lessEq(a,b) ((fabs((a) - (b))/1.0E-15 > 10 ? false: (floor(fabs((a) - (b))/1.0E-15) < 1)) || (a)<(b))
#define equalsEq(a,b) ((fabs((a) - (b))/1.0E-15 > 10 ? false: (floor(fabs((a) - (b))/1.0E-15) < 1)))
#define reducePrecision(x, p) (floor(((x) * pow(10, (p)) + 0.5)) / pow(10, (p)))

using namespace external;

/*
 * Calculates coefficients for quintic lagrangian interpolation.
 *    coefs;      coefficients to obtain
 *    coord:      point in which the interpolation must be calculated
 *    position:   surrounding points for interpolation
 */
inline void calculateCoefficientsMapFile(double* coefs, double coord, double* position) {

    for (int i = 0; i < 6; i++) {
        coefs[i] = 0;
    }
    //Search for a perfect fit

    for (int i = 0; i < 6; i++) {
        if (position[i] == coord) {
            coefs[i] = 1;
            return;
        }
    }

    double x1 = position[0];
    double x2 = position[1];
    double x3 = position[2];
    double x4 = position[3];
    double x5 = position[4];
    double x6 = position[5];

    coefs[0] = (coord-x2)*(coord-x3)*(coord-x4)*(coord-x5)*(coord-x6) / ((x1-x2)*(x1-x3)*(x1-x4)*(x1-x5)*(x1-x6));
    coefs[1] = (coord-x1)*(coord-x3)*(coord-x4)*(coord-x5)*(coord-x6) / ((x2-x1)*(x2-x3)*(x2-x4)*(x2-x5)*(x2-x6));
    coefs[2] = (coord-x1)*(coord-x2)*(coord-x4)*(coord-x5)*(coord-x6) / ((x3-x1)*(x3-x2)*(x3-x4)*(x3-x5)*(x3-x6));
    coefs[3] = (coord-x1)*(coord-x2)*(coord-x3)*(coord-x5)*(coord-x6) / ((x4-x1)*(x4-x2)*(x4-x3)*(x4-x5)*(x4-x6));
    coefs[4] = (coord-x1)*(coord-x2)*(coord-x3)*(coord-x4)*(coord-x6) / ((x5-x1)*(x5-x2)*(x5-x3)*(x5-x4)*(x5-x6));
    coefs[5] = (coord-x1)*(coord-x2)*(coord-x3)*(coord-x4)*(coord-x5) / ((x6-x1)*(x6-x2)*(x6-x3)*(x6-x4)*(x6-x5));
}



#define vector(v, i, j) (v)[i+ilast*(j)]

#define W_(parq, dx, simPlat_dt, ilast, jlast) (((1.0 - 0.5 * (parq)) * (1.0 - 0.5 * (parq)) * (1.0 - 0.5 * (parq)) * (1.0 - 0.5 * (parq))) * (2.0 * (parq) + 1.0))

#define gW_(parq, dx, simPlat_dt, ilast, jlast) ((-5.0) * (parq) * ((1.0 - 0.5 * (parq)) * (1.0 - 0.5 * (parq)) * (1.0 - 0.5 * (parq))))

#define SPHAsym_(paru_a, paru_b, parp_a, parp_b, parvol_b, pardis, pardim, parkernGrad, dx, simPlat_dt, ilast, jlast) (((parvol_b) * ((paru_b) - (paru_a)) * ((parp_b) - (parp_a)) * (parkernGrad)) / MAX((pardis), 0.0000000001))

#define SPHSym_(paru_a, paru_b, parp_a, parp_b, parvol_b, pardis, pardim, parkernGrad, dx, simPlat_dt, ilast, jlast) (((parvol_b) * ((paru_b) + (paru_a)) * ((parp_b) - (parp_a)) * (parkernGrad)) / MAX((pardis), 0.0000000001))

#define Interpolation_(parvol, parub, parkern, dx, simPlat_dt, ilast, jlast) ((parvol) * (parub) * (parkern))

#define particleDissipation_(paru_a, paru_b, parp_a, parp_b, parvol_b, pardis, pardim, parkernGrad, dx, simPlat_dt, ilast, jlast) (((parvol_b) * ((paru_b) - (paru_a)) * (parkernGrad)) / MAX((pardis), 0.0000000001) * ((((pardim) + 2.0) * ((parp_b) - (parp_a)) * ((parp_b) - (parp_a))) / MAX((pardis) * (pardis), 0.0000000001) - 1.0))

#define PRED_(paru, parfluxes, dx, simPlat_dt, ilast, jlast) ((paru) + 0.5 * simPlat_dt * (parfluxes))

#define SPHfirstderivativenormalization_(pardim, parvol_b, pardis, parkernGrad, dx, simPlat_dt, ilast, jlast) (((parvol_b) * (pardis) * (parkernGrad)) / (pardim))

#define Normalization_(parvol, parkern, dx, simPlat_dt, ilast, jlast) ((parvol) * (parkern))

#define CORR_(paru, parfluxes, dx, simPlat_dt, ilast, jlast) ((paru) + simPlat_dt * (parfluxes))

#define Fifield_mainI(parauxiliaryField, dx, simPlat_dt, ilast, jlast) ((parauxiliaryField))





#endif
