#include "Problem.h"
#include "Functions.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <stack>
#include "hdf5.h"
#include <gsl/gsl_rng.h>
#include <random>
#include "SAMRAI/pdat/CellVariable.h"
#include <time.h>
#include <silo.h>
#include <algorithm>
#include "float.h"
#include "NonSync.h"
#include "NonSyncs.h"
#include "SAMRAI/pdat/IndexVariable.h"
#include "SAMRAI/pdat/CellData.h"
#include "SAMRAI/pdat/NodeData.h"
#include "SAMRAI/pdat/NodeVariable.h"
#include "LagrangianPolynomicRefine.h"


#include "SAMRAI/tbox/Array.h"
#include "SAMRAI/hier/BoundaryBox.h"
#include "SAMRAI/hier/BoxContainer.h"
#include "SAMRAI/geom/CartesianPatchGeometry.h"
#include "SAMRAI/hier/VariableDatabase.h"
#include "SAMRAI/hier/PatchDataRestartManager.h"
#include "SAMRAI/hier/Index.h"
#include "SAMRAI/tbox/PIO.h"
#include "SAMRAI/tbox/Utilities.h"
#include "SAMRAI/tbox/Timer.h"
#include "SAMRAI/tbox/TimerManager.h"

#define MIN(X,Y) ((X) < (Y) ? (X) : (Y))
#define MAX(X,Y) ((X) > (Y) ? (X) : (Y))
#define SIGN(X) (((X) > 0) - ((X) < 0))
#define isEven(a) ((a) % 2 == 0 ? true : false)
#define greaterEq(a,b) ((fabs((a) - (b))/1.0E-15 > 10 ? false : (floor(fabs((a) - (b))/1.0E-15) < 1)) || (b)<(a))
#define lessEq(a,b) ((fabs((a) - (b))/1.0E-15 > 10 ? false: (floor(fabs((a) - (b))/1.0E-15) < 1)) || (a)<(b))
#define equalsEq(a,b) ((fabs((a) - (b))/1.0E-15 > 10 ? false: (floor(fabs((a) - (b))/1.0E-15) < 1)))
#define reducePrecision(x, p) (floor(((x) * pow(10, (p)) + 0.5)) / pow(10, (p)))

using namespace external;

const gsl_rng_type * T;
gsl_rng *r_var;
gsl_rng *r_map;
std::default_random_engine generator;

//Timers
std::shared_ptr<tbox::Timer> t_step;
std::shared_ptr<tbox::Timer> t_moveParticles;
std::shared_ptr<tbox::Timer> t_output;

inline int Problem::GetExpoBase2(double d)
{
	int i = 0;
	((short *)(&i))[0] = (((short *)(&d))[3] & (short)32752); // _123456789ab____ & 0111111111110000
	return (i >> 4) - 1023;
}

bool	Problem::Equals(double d1, double d2)
{
	if (d1 == d2)
		return true;
	int e1 = GetExpoBase2(d1);
	int e2 = GetExpoBase2(d2);
	int e3 = GetExpoBase2(d1 - d2);
	if ((e3 - e2 < -48) && (e3 - e1 < -48))
		return true;
	return false;
}

/*
 * Constructor of the problem.
 */
Problem::Problem(const string& object_name, const tbox::Dimension& dim, std::shared_ptr<tbox::Database>& database, std::shared_ptr<geom::CartesianGridGeometry >& grid_geom, std::shared_ptr<hier::PatchHierarchy >& patch_hierarchy, MainRestartData& mrd, const double dt, const bool init_from_restart, const int console_output, const int timer_output, const int particle_output_period, const vector<string> full_particle_writer_variables, std::shared_ptr<ParticleDataWriter>& particle_data_writer): 
d_dim(dim), xfer::RefinePatchStrategy(), xfer::CoarsenPatchStrategy(), viz_particle_dump_interval(particle_output_period), d_full_particle_writer_variables(full_particle_writer_variables.begin(), full_particle_writer_variables.end()), d_particle_data_writer(particle_data_writer), d_output_interval(console_output), d_timer_output_interval(timer_output)
{
	//Setup the timers
	t_step = tbox::TimerManager::getManager()->getTimer("Step");
	t_moveParticles = tbox::TimerManager::getManager()->getTimer("Move particles");
	t_output = tbox::TimerManager::getManager()->getTimer("OutputGeneration");

	//Output configuration
	next_console_output = d_output_interval;
	next_timer_output = d_timer_output_interval;

	//Get the object name, the grid geometry and the patch hierarchy
  	d_grid_geometry = grid_geom;
	d_patch_hierarchy = patch_hierarchy;
	d_object_name = object_name;
	d_init_from_restart = init_from_restart;
	initial_dt = dt;




	//Get parameters
    cout<<"Reading parameters"<<endl;
	const double* dx  = grid_geom->getDx();
	dimensions_main = database->getInteger("dimensions_main");
	dissipation_factor_field = database->getDouble("dissipation_factor_field");
	std::shared_ptr<tbox::Database> particles_common_db(database->getDatabase("particles"));
	std::shared_ptr<tbox::Database> particles_db(particles_common_db->getDatabase("particles"));
	particleDistribution_particles = particles_db->getString("particle_distribution");
	domain_offset_factor_particles = particles_db->getDoubleVector("domain_offset_factor");
	normal_mean_particles = particles_db->getDoubleVector("normal_mean");
	normal_stddev_particles = particles_db->getDoubleVector("normal_stddev");
	box_min_particles = particles_db->getDoubleVector("box_min");
	box_max_particles = particles_db->getDoubleVector("box_max");
	number_of_particles_particles = particles_db->getDoubleVector("number_of_particles");
	particleSeparation_particles.push_back((grid_geom->getXUpper()[0] - grid_geom->getXLower()[0])/number_of_particles_particles[0]);
	particleSeparation_particles.push_back((grid_geom->getXUpper()[1] - grid_geom->getXLower()[1])/number_of_particles_particles[1]);
	if (domain_offset_factor_particles[0] < 0 || domain_offset_factor_particles[0] > 1 || domain_offset_factor_particles[1] < 0 || domain_offset_factor_particles[1] > 1 ) {
		TBOX_ERROR("Problem::Problem"
			<< "n      Error in parameter input:"
			<< "n      domain_offset_factor_particles must be between [0, 1]"<< std::endl);
	}
	int number_of_particles_x = number_of_particles_particles[0];
	particleSeparation_x = (grid_geom->getXUpper()[0] - grid_geom->getXLower()[0])/number_of_particles_x;
	if (particleSeparation_x <= 0) {
		TBOX_ERROR("Problem::Problem"
			<< "n      Error in parameter input:"
			<< "n      number_of_particles_x must be greater than 0"<< std::endl);
	}
	int number_of_particles_y = number_of_particles_particles[1];
	particleSeparation_y = (grid_geom->getXUpper()[1] - grid_geom->getXLower()[1])/number_of_particles_y;
	if (particleSeparation_y <= 0) {
		TBOX_ERROR("Problem::Problem"
			<< "n      Error in parameter input:"
			<< "n      number_of_particles_y must be greater than 0"<< std::endl);
	}
	influenceRadius = particles_common_db->getDouble("influenceRadius");
	//Random initialization
	gsl_rng_env_setup();
	//Random for simulation
	const tbox::SAMRAI_MPI& mpi(tbox::SAMRAI_MPI::getSAMRAIWorld());
	int random_seed = database->getDouble("random_seed")*(mpi.getRank() + 1);
	r_var = gsl_rng_alloc(gsl_rng_ranlxs0);
	gsl_rng_set(r_var, random_seed);
	//Random for mapping pursposes
	r_map = gsl_rng_alloc(gsl_rng_ranlxs0);
	gsl_rng_set(r_map, database->getDouble("random_seed"));
	for (int il = 0; il < d_patch_hierarchy->getMaxNumberOfLevels(); il++) {
		bo_substep_iteration.push_back(0);
	}
	//Initialization from input file
	if (!d_init_from_restart) {
		next_particle_dump_iteration = viz_particle_dump_interval;

		//Iteration counter
		for (int il = 0; il < d_patch_hierarchy->getMaxNumberOfLevels(); il++) {
			current_iteration.push_back(0);
		}
	}
	//Initialization from restart file
	else {
		getFromRestart(mrd);
	}


	//External eos parameters
#ifdef EXTERNAL_EOS

	std::shared_ptr<tbox::Database> external_eos_db = database->getDatabase("external_EOS");
	Commons::ExternalEos::reprimand_eos_type = external_eos_db->getInteger("eos_type");
	Commons::ExternalEos::reprimand_atmo_Ye = external_eos_db->getDouble("atmo_Ye");
	Commons::ExternalEos::reprimand_max_z = external_eos_db->getDouble("max_z");
	Commons::ExternalEos::reprimand_max_b = external_eos_db->getDouble("max_b");
	Commons::ExternalEos::reprimand_c2p_acc = external_eos_db->getDouble("c2p_acc");
	Commons::ExternalEos::reprimand_atmo_rho = external_eos_db->getDouble("atmo_rho");
	Commons::ExternalEos::reprimand_rho_strict = external_eos_db->getDouble("rho_strict");
	Commons::ExternalEos::reprimand_max_rho = external_eos_db->getDouble("max_rho");
	Commons::ExternalEos::reprimand_max_eps = external_eos_db->getDouble("max_eps");
	Commons::ExternalEos::reprimand_gamma_th = external_eos_db->getDouble("gamma_th");
#endif

    	//Subcycling
	d_refinedTimeStepping = false;
	if (database->isString("subcycling")) {
		if (database->getString("subcycling") == "BERGER-OLIGER") {
			d_refinedTimeStepping = true;
		}
	}


	//Regridding options
	d_regridding = false;
	if (database->isDatabase("regridding")) {
		regridding_db = database->getDatabase("regridding");
		d_regridding_buffer = regridding_db->getDouble("regridding_buffer");
		int smallest_patch_size = d_patch_hierarchy->getSmallestPatchSize(0).min();
		for (int il = 1; il < d_patch_hierarchy->getMaxNumberOfLevels(); il++) {
			smallest_patch_size = MIN(smallest_patch_size, d_patch_hierarchy->getSmallestPatchSize(il).min());
		}
		if (d_regridding_buffer > smallest_patch_size) {
			TBOX_ERROR("Error: Regridding_buffer parameter ("<<d_regridding_buffer<<") cannot be greater than smallest_patch_size minimum value("<<smallest_patch_size<<")");
		}
		if (regridding_db->isString("regridding_type")) {
			d_regridding_type = regridding_db->getString("regridding_type");
			d_regridding_min_level = regridding_db->getInteger("regridding_min_level");
			d_regridding_max_level = regridding_db->getInteger("regridding_max_level");
			if (d_regridding_type == "GRADIENT") {
				d_regridding_field = regridding_db->getString("regridding_field");
				d_regridding_compressionFactor = regridding_db->getDouble("regridding_compressionFactor");
				d_regridding_mOffset = regridding_db->getDouble("regridding_mOffset");
				d_regridding = true;
			} else {
				if (d_regridding_type == "FUNCTION") {
					d_regridding_field = regridding_db->getString("regridding_function_field");
					d_regridding_threshold = regridding_db->getDouble("regridding_threshold");
					d_regridding = true;
				} else {
					if (d_regridding_type == "SHADOW") {
						std::string* fields = new std::string[2];
						regridding_db->getStringArray("regridding_fields", fields, 2);
						d_regridding_field = fields[0];
						d_regridding_field_shadow = fields[1];
						d_regridding_error = regridding_db->getDouble("regridding_error");
						d_regridding = true;
						delete[] fields;
					}
				}
			}
		}
	}

	//Stencil of the discretization method
	d_ghost_width = MAX(ceil(2 * influenceRadius/dx[0]), ceil(2 * influenceRadius/dx[1]));

	
	//Register Fields and temporal fields into the variable database
	hier::VariableDatabase *vdb = hier::VariableDatabase::getDatabase();
  	std::shared_ptr<hier::VariableContext> d_cont_curr(vdb->getContext("Current"));
	std::shared_ptr< pdat::NodeVariable<double> > interior(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "regridding_value",1)));
	d_interior_regridding_value_id = vdb->registerVariableAndContext(interior ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<int> > nonSync(std::shared_ptr< pdat::NodeVariable<int> >(new pdat::NodeVariable<int>(d_dim, "regridding_tag",1)));
	d_nonSync_regridding_tag_id = vdb->registerVariableAndContext(nonSync ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<double> > interior_i(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "interior_i",1)));
	d_interior_i_id = vdb->registerVariableAndContext(interior_i ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<double> > interior_j(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "interior_j",1)));
	d_interior_j_id = vdb->registerVariableAndContext(interior_j ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<double> > FOV_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FOV_1",1)));
	d_FOV_1_id = vdb->registerVariableAndContext(FOV_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FOV_1_id);
	std::shared_ptr< pdat::NodeVariable<double> > FOV_xLower(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FOV_xLower",1)));
	d_FOV_xLower_id = vdb->registerVariableAndContext(FOV_xLower ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FOV_xLower_id);
	std::shared_ptr< pdat::NodeVariable<double> > FOV_xUpper(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FOV_xUpper",1)));
	d_FOV_xUpper_id = vdb->registerVariableAndContext(FOV_xUpper ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FOV_xUpper_id);
	std::shared_ptr< pdat::NodeVariable<double> > FOV_yLower(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FOV_yLower",1)));
	d_FOV_yLower_id = vdb->registerVariableAndContext(FOV_yLower ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FOV_yLower_id);
	std::shared_ptr< pdat::NodeVariable<double> > FOV_yUpper(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FOV_yUpper",1)));
	d_FOV_yUpper_id = vdb->registerVariableAndContext(FOV_yUpper ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FOV_yUpper_id);
	std::shared_ptr< pdat::IndexVariable<NonSyncs, pdat::CellGeometry > > nonSyncP(std::shared_ptr< pdat::IndexVariable<NonSyncs, pdat::CellGeometry > >(new pdat::IndexVariable<NonSyncs, pdat::CellGeometry >(d_dim, "nonSyncP")));
	d_nonSyncP_id = vdb->registerVariableAndContext(nonSyncP ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::IndexVariable<Particles<Particle_particles>, pdat::CellGeometry > > particleVariables_particles(std::shared_ptr< pdat::IndexVariable<Particles<Particle_particles>, pdat::CellGeometry > >(new pdat::IndexVariable<Particles<Particle_particles>, pdat::CellGeometry >(d_dim, "particleVariables_particles")));
	d_particleVariables_particles_id = vdb->registerVariableAndContext(particleVariables_particles, d_cont_curr, hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_particleVariables_particles_id);
	particleVariableList_particles.push_back("field");
	particleVariableList_particles.push_back("field_p");
	particleVariableList_particles.push_back("auxiliaryField");
	particleVariableList_particles.push_back("derAuxiliaryField");
	particleVariableList_particles.push_back("m_derAuxiliaryField_o0_t0_l0_1");
	particleVariableList_particles.push_back("predfield");
	particleVariableList_particles.push_back("id_particles");
	particleVariableList_particles.push_back("procId_particles");


	//Refine and coarse algorithms

	d_bdry_fill_init = std::shared_ptr< xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());
	d_bdry_post_coarsen = std::shared_ptr< xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());
	d_bdry_fill_advance = std::shared_ptr< xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());
	d_coarsen_algorithm = std::shared_ptr< xfer::CoarsenAlgorithm >(new xfer::CoarsenAlgorithm(d_dim));

	d_mapping_fill = std::shared_ptr< xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());
    d_tagging_fill = std::shared_ptr< xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());
	d_fill_new_level    = std::shared_ptr< xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());
	d_fill_new_level_der_aux    = std::shared_ptr< xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());


	//mapping communication

	std::shared_ptr< hier::RefineOperator > refine_operator_map = d_grid_geometry->lookupRefineOperator(interior, "LINEAR_REFINE");
	d_mapping_fill->registerRefine(d_interior_regridding_value_id,d_interior_regridding_value_id,d_interior_regridding_value_id, refine_operator_map);
	d_mapping_fill->registerRefine(d_interior_i_id,d_interior_i_id,d_interior_i_id, refine_operator_map);
	d_mapping_fill->registerRefine(d_interior_j_id,d_interior_j_id,d_interior_j_id, refine_operator_map);
	refine_particle_operator_particles = new ParticleRefine<Particle_particles>(d_patch_hierarchy, d_grid_geometry, particleSeparation_x, particleSeparation_y, d_ghost_width, influenceRadius);
	std::shared_ptr< hier::RefineOperator > refine_particles_particles(refine_particle_operator_particles);
	d_mapping_fill->registerRefine(d_particleVariables_particles_id,d_particleVariables_particles_id,d_particleVariables_particles_id, refine_particles_particles);
	d_mapping_fill->registerRefine(d_FOV_1_id,d_FOV_1_id,d_FOV_1_id, refine_operator_map);
	d_mapping_fill->registerRefine(d_FOV_xLower_id,d_FOV_xLower_id,d_FOV_xLower_id, refine_operator_map);
	d_mapping_fill->registerRefine(d_FOV_xUpper_id,d_FOV_xUpper_id,d_FOV_xUpper_id, refine_operator_map);
	d_mapping_fill->registerRefine(d_FOV_yLower_id,d_FOV_yLower_id,d_FOV_yLower_id, refine_operator_map);
	d_mapping_fill->registerRefine(d_FOV_yUpper_id,d_FOV_yUpper_id,d_FOV_yUpper_id, refine_operator_map);


    d_tagging_fill->registerRefine(d_nonSync_regridding_tag_id,d_nonSync_regridding_tag_id,d_nonSync_regridding_tag_id, d_grid_geometry->lookupRefineOperator(nonSync, "NO_REFINE"));

	//refine and coarsen operators
	string refine_op_name = "LINEAR_REFINE";
	int order = 0;
	if (database->isDatabase("regridding")) {
		regridding_db = database->getDatabase("regridding");
		if (regridding_db->isString("interpolator")) {
			refine_op_name = regridding_db->getString("interpolator");
			if (refine_op_name == "LINEAR_REFINE") {
				order = 1;
			}
			if (refine_op_name == "CUBIC_REFINE") {
				order = 3;
			}
			if (refine_op_name == "QUINTIC_REFINE") {
				order = 5;
			}
		}
	}
	std::shared_ptr< hier::RefineOperator > refine_operator, refine_operator_bound;
	std::shared_ptr< hier::CoarsenOperator > coarsen_operator = d_grid_geometry->lookupCoarsenOperator(FOV_1, "CONSTANT_COARSEN");
	if (order > 0) {
		std::shared_ptr< hier::RefineOperator > tmp_refine_operator(new LagrangianPolynomicRefine(false, order, d_patch_hierarchy, d_dim));
		refine_operator = tmp_refine_operator;
		std::shared_ptr< hier::RefineOperator > tmp_refine_operator_bound(new LagrangianPolynomicRefine(true, order, d_patch_hierarchy, d_dim));
		refine_operator_bound = tmp_refine_operator_bound;
	} else {
		refine_operator = d_grid_geometry->lookupRefineOperator(FOV_1, refine_op_name);
		refine_operator_bound = d_grid_geometry->lookupRefineOperator(FOV_1, refine_op_name);
	}

	std::shared_ptr<SAMRAI::hier::TimeInterpolateOperator> tio_mesh1(new TimeInterpolator(d_grid_geometry, "mesh"));
	time_interpolate_operator_mesh1 = std::dynamic_pointer_cast<TimeInterpolator>(tio_mesh1);
	std::shared_ptr<SAMRAI::hier::TimeInterpolateOperator> tio_particles_particles(new TimeInterpolator(d_grid_geometry, "particle_particles"));
	time_interpolate_operator_particles_particles = std::dynamic_pointer_cast<TimeInterpolator>(tio_particles_particles);


	//Register variables to the refineAlgorithm for boundaries

	d_bdry_fill_init->registerRefine(d_particleVariables_particles_id,d_particleVariables_particles_id,d_particleVariables_particles_id,refine_particles_particles);
	if (d_refinedTimeStepping) {
		d_bdry_fill_advance->registerRefine(d_particleVariables_particles_id,d_particleVariables_particles_id,d_particleVariables_particles_id,d_particleVariables_particles_id,d_particleVariables_particles_id,refine_particles_particles, tio_particles_particles);
	} else {
		d_bdry_fill_advance->registerRefine(d_particleVariables_particles_id,d_particleVariables_particles_id,d_particleVariables_particles_id,refine_particles_particles);
	}


	//Register variables to the refineAlgorithm for filling new levels on regridding


	//Register variables to the coarsenAlgorithm



    Commons::initialization();
}

/*
 * Destructor.
 */
Problem::~Problem() 
{
} 

/*
 * Initialize the data from a given level.
 */
void Problem::initializeLevelData (
   const std::shared_ptr<hier::PatchHierarchy >& hierarchy , 
   const int level_number ,
   const double init_data_time ,
   const bool can_be_refined ,
   const bool initial_time ,
   const std::shared_ptr<hier::PatchLevel >& old_level ,
   const bool allocate_data )
{
    cout<<"Initializing level "<<level_number<<endl;
    tbox::MemoryUtilities::printMemoryInfo(cout);   
	std::shared_ptr< hier::PatchLevel > level(hierarchy->getPatchLevel(level_number));

	// Allocate storage needed to initialize level and fill data from coarser levels in AMR hierarchy.  
	level->allocatePatchData(d_interior_regridding_value_id, init_data_time);
	level->allocatePatchData(d_nonSync_regridding_tag_id, init_data_time);
	level->allocatePatchData(d_interior_i_id, init_data_time);
	level->allocatePatchData(d_interior_j_id, init_data_time);
	level->allocatePatchData(d_FOV_1_id, init_data_time);
	level->allocatePatchData(d_FOV_xLower_id, init_data_time);
	level->allocatePatchData(d_FOV_xUpper_id, init_data_time);
	level->allocatePatchData(d_FOV_yLower_id, init_data_time);
	level->allocatePatchData(d_FOV_yUpper_id, init_data_time);
	level->allocatePatchData(d_particleVariables_particles_id, init_data_time);
	level->allocatePatchData(d_nonSyncP_id, init_data_time);
	level->allocatePatchData(d_interior_i_id, init_data_time);
	level->allocatePatchData(d_interior_j_id, init_data_time);


	//Mapping the current data for new level.
	if (initial_time || level_number == 0) {
		mapDataOnPatch(init_data_time, initial_time, level_number, level);
	}

	//Fill a finer level with the data of the next coarse level.
	if ((level_number > 0) || old_level) {
		d_mapping_fill->createSchedule(level,old_level,level_number-1,hierarchy,this)->fillData(init_data_time, false);
		correctFOVS(level);
	}



	//Interphase mapping
	if (initial_time || level_number == 0) {
		interphaseMapping(init_data_time, initial_time, level_number, level, 1);
	}


	//Initialize current data for new level.
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr< hier::Patch > patch = *p_it;
		if (initial_time) {
  		    initializeDataOnPatch(*patch, init_data_time, initial_time);
		}

	}
	//Post-initialization Sync.
    	if (initial_time || level_number == 0) {

		//First synchronization from initialization
		d_bdry_fill_init->createSchedule(level,this)->fillData(init_data_time, false);
		double current_time = init_data_time;
		const double level_ratio = level->getRatioToCoarserLevel().max();
		double level_influenceRadius = influenceRadius/MAX(1,level_ratio);
		double volume_level_factor = 1.0 / MAX(1, level->getRatioToLevelZero().getProduct());
		double simPlat_dt = 0;
		for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
			const std::shared_ptr< hier::Patch >& patch = *p_it;
			double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
			double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
			double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
			double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
			double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
			double auxVarField2;

			std::shared_ptr< pdat::IndexData<Particles<Particle_particles>, pdat::CellGeometry > > particleVariables_particles(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles<Particle_particles>, pdat::CellGeometry >, hier::PatchData>(patch->getPatchData(d_particleVariables_particles_id)));
			//Get the dimensions of the patch
			const hier::Index boxfirst = patch->getBox().lower();
			const hier::Index boxlast = patch->getBox().upper();

			const hier::Index boxfirstP = particleVariables_particles->getGhostBox().lower();
			const hier::Index boxlastP = particleVariables_particles->getGhostBox().upper();

			//Get delta spaces into an array. dx, dy, dz.
			const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
			const double* dx  = patch_geom->getDx();
			int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
			int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
			for (int j = 0; j < jlast; j++) {
				for (int i = 0; i < ilast; i++) {
					if (i + 1 < ilast && j + 1 < jlast) {
						hier::Index idx(i + boxfirstP(0), j + boxfirstP(1));
						Particles<Particle_particles>* part_particles = particleVariables_particles->getItem(idx);
						for (int pit = part_particles->getNumberOfParticles() - 1; pit >= 0; pit--) {
							Particle_particles* particle = part_particles->getParticle(pit);
							auxVarField2 = particle->field * particle->auxiliaryField;
							if (particle->region == 1 || particle->newRegion == 1) {
								particle->auxiliaryField = auxVarField2;
							}
						}
					}
				}
			}
		}
		for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
			const std::shared_ptr< hier::Patch >& patch = *p_it;
			double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
			double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
			double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
			double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
			double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
			double derivative_normalization, interpolation_normalization, d_derAuxiliaryField_o0_t0_m0_l0;

			std::shared_ptr< pdat::IndexData<Particles<Particle_particles>, pdat::CellGeometry > > particleVariables_particles(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles<Particle_particles>, pdat::CellGeometry >, hier::PatchData>(patch->getPatchData(d_particleVariables_particles_id)));
			//Get the dimensions of the patch
			const hier::Index boxfirst = patch->getBox().lower();
			const hier::Index boxlast = patch->getBox().upper();

			const hier::Index boxfirstP = particleVariables_particles->getGhostBox().lower();
			const hier::Index boxlastP = particleVariables_particles->getGhostBox().upper();

			//Get delta spaces into an array. dx, dy, dz.
			const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
			const double* dx  = patch_geom->getDx();
			int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
			int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
			for (int j = 0; j < jlast; j++) {
				for (int i = 0; i < ilast; i++) {
					if (i + 1 < ilast && j + 1 < jlast) {
						hier::Index idx(i + boxfirstP(0), j + boxfirstP(1));
						Particles<Particle_particles>* part_particles = particleVariables_particles->getItem(idx);
						for (int pit = part_particles->getNumberOfParticles() - 1; pit >= 0; pit--) {
							Particle_particles* particle = part_particles->getParticle(pit);
							derivative_normalization = 0.0;
							interpolation_normalization = 0.0;
							d_derAuxiliaryField_o0_t0_m0_l0 = 0.0;
							miniIndex = MAX(0, MAX((int) floor((particle->positionx - 2 * level_influenceRadius - d_grid_geometry->getXLower()[0])/dx[0]) - boxfirst(0) + d_ghost_width, (int) i - ceil((2 * level_influenceRadius)/dx[0])));
							maxiIndex = MIN(boxlast(0) - boxfirst(0) + 2 * d_ghost_width, MIN((int) floor((particle->positionx + 2 * level_influenceRadius - d_grid_geometry->getXLower()[0])/dx[0]) - boxfirst(0) + d_ghost_width, (int) i + ceil((2 * level_influenceRadius)/dx[0])));
							minjIndex = MAX(0, MAX((int) floor((particle->positiony - 2 * level_influenceRadius - d_grid_geometry->getXLower()[1])/dx[1]) - boxfirst(1) + d_ghost_width, (int) j - ceil((2 * level_influenceRadius)/dx[1])));
							maxjIndex = MIN(boxlast(1) - boxfirst(1) + 2 * d_ghost_width, MIN((int) floor((particle->positiony + 2 * level_influenceRadius - d_grid_geometry->getXLower()[1])/dx[1]) - boxfirst(1) + d_ghost_width, (int) j + ceil((2 * level_influenceRadius)/dx[1])));
							for(int jb = minjIndex; jb <= maxjIndex; jb++) {
								for(int ib = miniIndex; ib <= maxiIndex; ib++) {
									hier::Index idxb(ib + boxfirstP(0), jb + boxfirstP(1));
									Particles<Particle_particles>* partb_particles = particleVariables_particles->getItem(idxb);
									for (int pitb = 0; pitb < partb_particles->getNumberOfParticles(); pitb++) {
										Particle_particles* particleb = partb_particles->getParticle(pitb);
										double particle_distance = particle->distance(particleb);
										if (particle_distance < 2 * level_influenceRadius) {
											derivative_normalization = derivative_normalization + SPHfirstderivativenormalization_(dimensions_main, 1.0 * volume_level_factor, particle_distance, gW_(particle_distance / level_influenceRadius, dx, simPlat_dt, ilast, jlast), dx, simPlat_dt, ilast, jlast);
											interpolation_normalization = interpolation_normalization + Normalization_(1.0 * volume_level_factor, W_(particle_distance / level_influenceRadius, dx, simPlat_dt, ilast, jlast), dx, simPlat_dt, ilast, jlast);
											d_derAuxiliaryField_o0_t0_m0_l0 = d_derAuxiliaryField_o0_t0_m0_l0 + SPHAsym_(particle->field, particleb->field, particle->positiony_p, particleb->positiony_p, 1.0 * volume_level_factor, particle_distance, dimensions_main, gW_(particle_distance / level_influenceRadius, dx, simPlat_dt, ilast, jlast), dx, simPlat_dt, ilast, jlast);
										}
									}				
								}
							}
							if (fabs(derivative_normalization) < 0.000000000000001) {
								derivative_normalization = 0.000000000000001;
							}
							if (fabs(interpolation_normalization) < 0.000000000000001) {
								interpolation_normalization = 0.000000000000001;
							}
							d_derAuxiliaryField_o0_t0_m0_l0 = d_derAuxiliaryField_o0_t0_m0_l0 / derivative_normalization;
							particle->m_derAuxiliaryField_o0_t0_l0_1 = particle->field * d_derAuxiliaryField_o0_t0_m0_l0;
						}
					}
				}
			}
		}
		for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
			const std::shared_ptr< hier::Patch >& patch = *p_it;
			double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
			double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
			double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
			double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
			double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
			double derivative_normalization, interpolation_normalization, d_derAuxiliaryField_o0_t0_m0_l1, auxVarField2, d_derAuxiliaryField_o0_t1_m0_l0;

			std::shared_ptr< pdat::IndexData<Particles<Particle_particles>, pdat::CellGeometry > > particleVariables_particles(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles<Particle_particles>, pdat::CellGeometry >, hier::PatchData>(patch->getPatchData(d_particleVariables_particles_id)));
			//Get the dimensions of the patch
			const hier::Index boxfirst = patch->getBox().lower();
			const hier::Index boxlast = patch->getBox().upper();

			const hier::Index boxfirstP = particleVariables_particles->getGhostBox().lower();
			const hier::Index boxlastP = particleVariables_particles->getGhostBox().upper();

			//Get delta spaces into an array. dx, dy, dz.
			const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
			const double* dx  = patch_geom->getDx();
			int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
			int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
			for (int j = 0; j < jlast; j++) {
				for (int i = 0; i < ilast; i++) {
					if (i + 1 < ilast && j + 1 < jlast) {
						hier::Index idx(i + boxfirstP(0), j + boxfirstP(1));
						Particles<Particle_particles>* part_particles = particleVariables_particles->getItem(idx);
						for (int pit = part_particles->getNumberOfParticles() - 1; pit >= 0; pit--) {
							Particle_particles* particle = part_particles->getParticle(pit);
							derivative_normalization = 0.0;
							interpolation_normalization = 0.0;
							miniIndex = MAX(0, MAX((int) floor((particle->positionx - 2 * level_influenceRadius - d_grid_geometry->getXLower()[0])/dx[0]) - boxfirst(0) + d_ghost_width, (int) i - ceil((2 * level_influenceRadius)/dx[0])));
							maxiIndex = MIN(boxlast(0) - boxfirst(0) + 2 * d_ghost_width, MIN((int) floor((particle->positionx + 2 * level_influenceRadius - d_grid_geometry->getXLower()[0])/dx[0]) - boxfirst(0) + d_ghost_width, (int) i + ceil((2 * level_influenceRadius)/dx[0])));
							minjIndex = MAX(0, MAX((int) floor((particle->positiony - 2 * level_influenceRadius - d_grid_geometry->getXLower()[1])/dx[1]) - boxfirst(1) + d_ghost_width, (int) j - ceil((2 * level_influenceRadius)/dx[1])));
							maxjIndex = MIN(boxlast(1) - boxfirst(1) + 2 * d_ghost_width, MIN((int) floor((particle->positiony + 2 * level_influenceRadius - d_grid_geometry->getXLower()[1])/dx[1]) - boxfirst(1) + d_ghost_width, (int) j + ceil((2 * level_influenceRadius)/dx[1])));
							for(int jb = minjIndex; jb <= maxjIndex; jb++) {
								for(int ib = miniIndex; ib <= maxiIndex; ib++) {
									hier::Index idxb(ib + boxfirstP(0), jb + boxfirstP(1));
									Particles<Particle_particles>* partb_particles = particleVariables_particles->getItem(idxb);
									for (int pitb = 0; pitb < partb_particles->getNumberOfParticles(); pitb++) {
										Particle_particles* particleb = partb_particles->getParticle(pitb);
										double particle_distance = particle->distance(particleb);
										if (particle_distance < 2 * level_influenceRadius) {
											derivative_normalization = derivative_normalization + SPHfirstderivativenormalization_(dimensions_main, 1.0 * volume_level_factor, particle_distance, gW_(particle_distance / level_influenceRadius, dx, simPlat_dt, ilast, jlast), dx, simPlat_dt, ilast, jlast);
											interpolation_normalization = interpolation_normalization + Normalization_(1.0 * volume_level_factor, W_(particle_distance / level_influenceRadius, dx, simPlat_dt, ilast, jlast), dx, simPlat_dt, ilast, jlast);
											d_derAuxiliaryField_o0_t0_m0_l1 = d_derAuxiliaryField_o0_t0_m0_l1 + SPHAsym_(particle->m_derAuxiliaryField_o0_t0_l0_1, particleb->m_derAuxiliaryField_o0_t0_l0_1, particle->positionx_p, particleb->positionx_p, 1.0 * volume_level_factor, particle_distance, dimensions_main, gW_(particle_distance / level_influenceRadius, dx, simPlat_dt, ilast, jlast), dx, simPlat_dt, ilast, jlast);
										}
									}				
								}
							}
							auxVarField2 = particle->field * particle->auxiliaryField;
							d_derAuxiliaryField_o0_t1_m0_l0 = auxVarField2;
							if (fabs(derivative_normalization) < 0.000000000000001) {
								derivative_normalization = 0.000000000000001;
							}
							if (fabs(interpolation_normalization) < 0.000000000000001) {
								interpolation_normalization = 0.000000000000001;
							}
							d_derAuxiliaryField_o0_t0_m0_l1 = d_derAuxiliaryField_o0_t0_m0_l1 / derivative_normalization;
							if (particle->region == 1 || particle->newRegion == 1) {
								particle->derAuxiliaryField = d_derAuxiliaryField_o0_t0_m0_l1 + d_derAuxiliaryField_o0_t1_m0_l0;
							}
						}
					}
				}
			}
		}
		//Last synchronization from initialization
		d_bdry_fill_init->createSchedule(level,this)->fillData(init_data_time, false);
		for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
			const std::shared_ptr< hier::Patch > patch = *p_it;
			std::shared_ptr< pdat::IndexData<Particles<Particle_particles>, pdat::CellGeometry > > particleVariables_particles(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles<Particle_particles>, pdat::CellGeometry >, hier::PatchData>(patch->getPatchData(d_particleVariables_particles_id)));
			const hier::Index boxfirst = particleVariables_particles->getGhostBox().lower();
			const hier::Index boxlast = particleVariables_particles->getGhostBox().upper();
			for (int index1 = boxfirst(1); index1 <= boxlast(1); index1++) {
				for (int index0 = boxfirst(0); index0 <= boxlast(0); index0++) {
					hier::Index idx(index0, index1);
					//Correct the position if there is any periodical boundary
					Particles<Particle_particles>* part_particles = particleVariables_particles->getItem(idx);
					checkPosition(patch, index0, index1, part_particles);
				}
			}
		}

    	}
	level->deallocatePatchData(d_nonSyncP_id);

    cout<<"Level "<<level_number<<" initialized"<<endl;
    tbox::MemoryUtilities::printMemoryInfo(cout);
}



void Problem::initializeLevelIntegrator(
   const std::shared_ptr<mesh::GriddingAlgorithmStrategy>& gridding_alg)
{
}

double Problem::getLevelDt(
   const std::shared_ptr<hier::PatchLevel>& level,
   const double dt_time,
   const bool initial_time)
{
  
   TBOX_ASSERT(level);

   if (level->getLevelNumber() == 0) return initial_dt;

    double dt = initial_dt;
    const hier::IntVector ratio = level->getRatioToLevelZero();
    double local_dt = dt;
    for (int i = 0; i < 2; i++) {
        if (local_dt > dt / ratio[i]) {
            local_dt = dt / ratio[i];
        }
    }
    return local_dt;
}

double Problem::getMaxFinerLevelDt(
   const int finer_level_number,
   const double coarse_dt,
   const hier::IntVector& ratio)
{
   NULL_USE(finer_level_number);

   TBOX_ASSERT(ratio.min() > 0);

   return coarse_dt / double(ratio.max());
}

void Problem::standardLevelSynchronization(
   const std::shared_ptr<hier::PatchHierarchy>& hierarchy,
   const int coarsest_level,
   const int finest_level,
   const double sync_time,
   const std::vector<double>& old_times)
{

}

void Problem::synchronizeNewLevels(
   const std::shared_ptr<hier::PatchHierarchy>& hierarchy,
   const int coarsest_level,
   const int finest_level,
   const double sync_time,
   const bool initial_time)
{

    //Not needed, but not absolutely sure
    /*for (int fine_ln = finest_level; fine_ln > coarsest_level; --fine_ln) {
        const int coarse_ln = fine_ln - 1;
        std::shared_ptr<hier::PatchLevel> fine_level(hierarchy->getPatchLevel(fine_ln));
        d_bdry_fill_init->createSchedule(fine_level, coarse_ln, hierarchy, this)->fillData(sync_time, true);
    }*/
}

void Problem::resetTimeDependentData(
   const std::shared_ptr<hier::PatchLevel>& level,
   const double new_time,
   const bool can_be_refined)
{
   TBOX_ASSERT(level);
   level->setTime(new_time);
}

void Problem::resetDataToPreadvanceState(
   const std::shared_ptr<hier::PatchLevel>& level)
{
    //cout<<"resetDataToPreadvanceState"<<endl;
}

/*
 * Map data on a patch. This mapping is done only at the begining of the simulation.
 */
void Problem::mapDataOnPatch(const double time, const bool initial_time, const int ln, const std::shared_ptr< hier::PatchLevel >& level)
{
	(void) time;
	const tbox::SAMRAI_MPI& mpi(tbox::SAMRAI_MPI::getSAMRAIWorld());
   	if (initial_time || ln == 0) {

		// Mapping		
		int i, iterm, previousMapi, iWallAcc;
		bool interiorMapi;
		double iMapStart, iMapEnd;
		int j, jterm, previousMapj, jWallAcc;
		bool interiorMapj;
		double jMapStart, jMapEnd;
		int minBlock[2], maxBlock[2], unionsI, facePointI, ie1, ie2, ie3, proc, pcounter, working, finished, pred;
		double maxDistance, e1, e2, e3;
		bool done, modif, workingArray[mpi.getSize()], finishedArray[mpi.getSize()], workingGlobal, finishedGlobal, workingPatchArray[level->getLocalNumberOfPatches()], finishedPatchArray[level->getLocalNumberOfPatches()], workingPatchGlobal, finishedPatchGlobal;
		int nodes = mpi.getSize();
		int patches = level->getLocalNumberOfPatches();

		double SQRT3INV = 1.0/sqrt(3.0);

		if (ln == 0) {
			//FOV initialization
			for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
				const std::shared_ptr< hier::Patch >& patch = *p_it;

				//Get the dimensions of the patch
				hier::Box pbox = patch->getBox();
				double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
				double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
				double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
				double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
				double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
				const hier::Index boxfirst = patch->getBox().lower();
				const hier::Index boxlast  = patch->getBox().upper();

				//Get delta spaces into an array. dx, dy, dz.
				const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
				const double* dx  = patch_geom->getDx();

				int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
				int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;

				for (i = 0; i < ilast; i++) {
					for (j = 0; j < jlast; j++) {
						vector(FOV_1, i, j) = 0;
						vector(FOV_xLower, i, j) = 0;
						vector(FOV_xUpper, i, j) = 0;
						vector(FOV_yLower, i, j) = 0;
						vector(FOV_yUpper, i, j) = 0;
					}
				}
			}
			d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);

			//Region: mainI
			for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
				const std::shared_ptr< hier::Patch >& patch = *p_it;

				//Get the dimensions of the patch
				hier::Box pbox = patch->getBox();
				double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
				double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
				double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
				double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
				double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
				double* interior_i = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_i_id).get())->getPointer();
				double* interior_j = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_j_id).get())->getPointer();
				double* interior = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_regridding_value_id).get())->getPointer();
				int* nonSync = ((pdat::NodeData<int> *) patch->getPatchData(d_nonSync_regridding_tag_id).get())->getPointer();
				const hier::Index boxfirst = patch->getBox().lower();
				const hier::Index boxlast  = patch->getBox().upper();

				//Get delta spaces into an array. dx, dy, dz.
				const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
				const double* dx  = patch_geom->getDx();

				int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
				int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;

				iMapStart = 0;
				iMapEnd = d_grid_geometry->getPhysicalDomain().front().numberCells()[0];
				jMapStart = 0;
				jMapEnd = d_grid_geometry->getPhysicalDomain().front().numberCells()[1];
				for (i = iMapStart; i <= iMapEnd; i++) {
					for (j = jMapStart; j <= jMapEnd; j++) {
						if (i >= boxfirst(0) - d_ghost_width && i <= boxlast(0) + d_ghost_width && j >= boxfirst(1) - d_ghost_width && j <= boxlast(1) + d_ghost_width) {
							vector(FOV_1, i - boxfirst(0) + d_ghost_width, j - boxfirst(1) + d_ghost_width) = 100;
							vector(FOV_xLower, i - boxfirst(0) + d_ghost_width, j - boxfirst(1) + d_ghost_width) = 0;
							vector(FOV_xUpper, i - boxfirst(0) + d_ghost_width, j - boxfirst(1) + d_ghost_width) = 0;
							vector(FOV_yLower, i - boxfirst(0) + d_ghost_width, j - boxfirst(1) + d_ghost_width) = 0;
							vector(FOV_yUpper, i - boxfirst(0) + d_ghost_width, j - boxfirst(1) + d_ghost_width) = 0;
						}
					}
				}
			}
			d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);

		}
		//Boundaries Mapping
		for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
			const std::shared_ptr< hier::Patch >& patch = *p_it;

			//Get the dimensions of the patch
			hier::Box pbox = patch->getBox();
			double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
			double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
			double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
			double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
			double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
			const hier::Index boxfirst = patch->getBox().lower();
			const hier::Index boxlast  = patch->getBox().upper();

			//Get delta spaces into an array. dx, dy, dz.
			const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
			const double* dx  = patch_geom->getDx();

			int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
			int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;

			//y-Upper
			if (patch->getPatchGeometry()->getTouchesRegularBoundary(1, 1)) {
				for (j = jlast - d_ghost_width; j < jlast; j++) {
					for (i = 0; i < ilast; i++) {
						vector(FOV_yUpper, i, j) = 100;
						vector(FOV_1, i, j) = 0;
						vector(FOV_xLower, i, j) = 0;
						vector(FOV_xUpper, i, j) = 0;
						vector(FOV_yLower, i, j) = 0;
					}
				}
			}
			//y-Lower
			if (patch->getPatchGeometry()->getTouchesRegularBoundary(1, 0)) {
				for (j = 0; j < d_ghost_width; j++) {
					for (i = 0; i < ilast; i++) {
						vector(FOV_yLower, i, j) = 100;
						vector(FOV_1, i, j) = 0;
						vector(FOV_xLower, i, j) = 0;
						vector(FOV_xUpper, i, j) = 0;
						vector(FOV_yUpper, i, j) = 0;
					}
				}
			}
			//x-Upper
			if (patch->getPatchGeometry()->getTouchesRegularBoundary(0, 1)) {
				for (j = 0; j < jlast; j++) {
					for (i = ilast - d_ghost_width; i < ilast; i++) {
						vector(FOV_xUpper, i, j) = 100;
						vector(FOV_1, i, j) = 0;
						vector(FOV_xLower, i, j) = 0;
						vector(FOV_yLower, i, j) = 0;
						vector(FOV_yUpper, i, j) = 0;
					}
				}
			}
			//x-Lower
			if (patch->getPatchGeometry()->getTouchesRegularBoundary(0, 0)) {
				for (j = 0; j < jlast; j++) {
					for (i = 0; i < d_ghost_width; i++) {
						vector(FOV_xLower, i, j) = 100;
						vector(FOV_1, i, j) = 0;
						vector(FOV_xUpper, i, j) = 0;
						vector(FOV_yLower, i, j) = 0;
						vector(FOV_yUpper, i, j) = 0;
					}
				}
			}
		}
		d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);

		mapDataOnParticles<Particle_particles>(level, particleDistribution_particles, number_of_particles_particles, particleSeparation_particles, domain_offset_factor_particles, normal_mean_particles, normal_stddev_particles, box_min_particles, box_max_particles);



   	}
}


// Point class for the floodfill algorithm
class Point {
private:
public:
	int i, j;
};

void Problem::floodfill(std::shared_ptr< hier::Patch > patch, int i, int j, int pred, int seg) const {

	double* FOV;
	switch(seg) {
		case 1:
			FOV = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
		break;
	}
	int* nonSync = ((pdat::NodeData<int> *) patch->getPatchData(d_nonSync_regridding_tag_id).get())->getPointer();
	double* interior = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_regridding_value_id).get())->getPointer();
	//Get the dimensions of the patch
	const hier::Index boxfirst = patch->getBox().lower();
	const hier::Index boxlast  = patch->getBox().upper();
	//Auxiliary definitions
	int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
	int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;

	stack<Point> mystack;

	Point p;
	p.i = i;
	p.j = j;

	mystack.push(p);
	while(mystack.size() > 0) {
		p = mystack.top();
		mystack.pop();
		if (vector(nonSync, p.i, p.j) == 0) {
			vector(nonSync, p.i, p.j) = pred;
			vector(interior, p.i, p.j) = pred;
			if (pred == 2) {
				vector(FOV, p.i, p.j) = 100;
			}
			if (p.i - 1 >= 0 && vector(nonSync, p.i - 1, p.j) == 0) {
				Point np;
				np.i = p.i-1;
				np.j = p.j;
				mystack.push(np);
			}
			if (p.i + 1 < ilast && vector(nonSync, p.i + 1, p.j) == 0) {
				Point np;
				np.i = p.i+1;
				np.j = p.j;
				mystack.push(np);
			}
			if (p.j - 1 >= 0 && vector(nonSync, p.i, p.j - 1) == 0) {
				Point np;
				np.i = p.i;
				np.j = p.j-1;
				mystack.push(np);
			}
			if (p.j + 1 < jlast && vector(nonSync, p.i, p.j + 1) == 0) {
				Point np;
				np.i = p.i;
				np.j = p.j+1;
				mystack.push(np);
			}
		}
	}
}


/*
 * FOV correction for AMR
 */
void Problem::correctFOVS(const std::shared_ptr< hier::PatchLevel >& level) {
	int i, j, k;
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr< hier::Patch >& patch = *p_it;

		//Get the dimensions of the patch
		hier::Box pbox = patch->getBox();
		double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
		const hier::Index boxfirst = patch->getBox().lower();
		const hier::Index boxlast  = patch->getBox().upper();

		//Get delta spaces into an array. dx, dy, dz.
		const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
		const double* dx  = patch_geom->getDx();

		int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
		int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;

		for (i = 0; i < ilast; i++) {
			for (j = 0; j < jlast; j++) {
				if (vector(FOV_xLower, i, j) > 0) {
					vector(FOV_xLower, i, j) = 100;
					vector(FOV_1, i, j) = 0;
					vector(FOV_xUpper, i, j) = 0;
					vector(FOV_yLower, i, j) = 0;
					vector(FOV_yUpper, i, j) = 0;
				}
				if (vector(FOV_xUpper, i, j) > 0) {
					vector(FOV_xUpper, i, j) = 100;
					vector(FOV_1, i, j) = 0;
					vector(FOV_xLower, i, j) = 0;
					vector(FOV_yLower, i, j) = 0;
					vector(FOV_yUpper, i, j) = 0;
				}
				if (vector(FOV_yLower, i, j) > 0) {
					vector(FOV_yLower, i, j) = 100;
					vector(FOV_1, i, j) = 0;
					vector(FOV_xLower, i, j) = 0;
					vector(FOV_xUpper, i, j) = 0;
					vector(FOV_yUpper, i, j) = 0;
				}
				if (vector(FOV_yUpper, i, j) > 0) {
					vector(FOV_yUpper, i, j) = 100;
					vector(FOV_1, i, j) = 0;
					vector(FOV_xLower, i, j) = 0;
					vector(FOV_xUpper, i, j) = 0;
					vector(FOV_yLower, i, j) = 0;
				}
				if (vector(FOV_1, i, j) > 0) {
					vector(FOV_1, i, j) = 100;
					vector(FOV_xLower, i, j) = 0;
					vector(FOV_xUpper, i, j) = 0;
					vector(FOV_yLower, i, j) = 0;
					vector(FOV_yUpper, i, j) = 0;
				}
			}
		}
	}
}

/*
 * Particle mapping
 */
template<class T>
void Problem::mapDataOnParticles(const std::shared_ptr< hier::PatchLevel >& level, std::string particleDistribution, std::vector<double> number_of_particles, std::vector<double> particleSeparation, std::vector<double> domain_offset_factor, std::vector<double> normal_mean, std::vector<double> normal_stddev, std::vector<double> box_min, std::vector<double> box_max)
{
	const tbox::SAMRAI_MPI& mpi(tbox::SAMRAI_MPI::getSAMRAIWorld());
	int i;
	double iMapStart, iMapEnd;
	xGlower = d_grid_geometry->getXLower()[0];
	xGupper = d_grid_geometry->getXUpper()[0];
	int j;
	double jMapStart, jMapEnd;
	yGlower = d_grid_geometry->getXLower()[1];
	yGupper = d_grid_geometry->getXUpper()[1];
	double position[2], maxPosition[2], minPosition[2];
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr< hier::Patch > patch = *p_it;
		int tmpVar_id;
		int* interior_i = ((pdat::CellData<int> *) patch->getPatchData(d_interior_i_id).get())->getPointer();
		int* interior_j = ((pdat::CellData<int> *) patch->getPatchData(d_interior_j_id).get())->getPointer();
		int* interior = ((pdat::CellData<int> *) patch->getPatchData(d_interior_regridding_value_id).get())->getPointer();
		int* nonSync = ((pdat::CellData<int> *) patch->getPatchData(d_nonSync_regridding_tag_id).get())->getPointer();
		if (std::is_same<T, Particle_particles>::value) {
			tmpVar_id = d_particleVariables_particles_id;
		}
		std::shared_ptr< pdat::IndexData<Particles<T>, pdat::CellGeometry > > particleVariables(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles<T>, pdat::CellGeometry >, hier::PatchData>(patch->getPatchData(tmpVar_id)));
		std::shared_ptr< pdat::IndexData<NonSyncs, pdat::CellGeometry > > nonSyncVariable(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< NonSyncs, pdat::CellGeometry >, hier::PatchData>(patch->getPatchData(d_nonSyncP_id)));
		const hier::Index boxfirst = particleVariables->getGhostBox().lower();
		const hier::Index boxlast  = particleVariables->getGhostBox().upper();
		const hier::Index boxfirst1 = patch->getBox().lower();
		const hier::Index boxlast1  = patch->getBox().upper();

		//Get delta spaces into an array. dx, dy, dz.
		const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
		const double* dx  = patch_geom->getDx();

		int ilast = boxlast1(0)-boxfirst1(0) + 2 + 2 * d_ghost_width;
		int jlast = boxlast1(1)-boxfirst1(1) + 2 + 2 * d_ghost_width;
		//Give particles support
		for (j = boxfirst(1); j <= boxlast(1); j++) {
			for (i = boxfirst(0); i <= boxlast(0); i++) {
				hier::Index idx(i, j);
				if (!particleVariables->isElement(idx)) {
					Particles<T>* part = new Particles<T>();
					particleVariables->addItemPointer(idx, part);
				}
				if (!nonSyncVariable->isElement(idx)) {
					NonSyncs* nonSyncPart = new NonSyncs();
					nonSyncVariable->addItemPointer(idx, nonSyncPart);
				}
			}
		}

		if (particleDistribution.compare("RANDOM") == 0) {
			//All domain mapping
			int numberOfParticles = (number_of_particles[0] + 2 * d_ghost_width * (dx[0]/particleSeparation[0])) * (number_of_particles[1] + 2 * d_ghost_width * (dx[1]/particleSeparation[1]));
			for (int n = 0; n < numberOfParticles; n++) {
				position[0] = gsl_rng_uniform(r_map) * (d_grid_geometry->getXUpper()[0] - d_grid_geometry->getXLower()[0] + 2 * d_ghost_width * dx[0]) + d_grid_geometry->getXLower()[0] - d_ghost_width * dx[0];
				i = floor((position[0] - d_grid_geometry->getXLower()[0])/dx[0] + 1E-10);
				position[1] = gsl_rng_uniform(r_map) * (d_grid_geometry->getXUpper()[1] - d_grid_geometry->getXLower()[1] + 2 * d_ghost_width * dx[1]) + d_grid_geometry->getXLower()[1] - d_ghost_width * dx[1];
				j = floor((position[1] - d_grid_geometry->getXLower()[1])/dx[1] + 1E-10);
				if (i >= boxfirst(0) && i <= boxlast(0) && j >= boxfirst(1) && j <= boxlast(1)) {
					//Region: mainI
					if (i >= boxfirst1(0) && i <= boxlast1(0) && j >= boxfirst1(1) && j <= boxlast1(1)) {
						hier::Index idx(i, j);
						Particles<T>* part = particleVariables->getItem(idx);
						T* particle = new T(0, position, 1);
						T* oldParticle = part->overlaps(*particle);
						if (oldParticle != NULL) {
							part->deleteParticle(*oldParticle);
						}
						part->addParticle(*particle);
						delete particle;
					}
				}
			}
		} else if (particleDistribution.compare("NORMAL") == 0) {
			//All domain mapping
			std::normal_distribution<double> distribution_i(normal_mean[0], normal_stddev[0]);
			std::normal_distribution<double> distribution_j(normal_mean[1], normal_stddev[1]);
			int numberOfParticles = (number_of_particles[0] + 2 * d_ghost_width * (dx[0]/particleSeparation[0])) * (number_of_particles[1] + 2 * d_ghost_width * (dx[1]/particleSeparation[1]));
			int innerParticles = 0;
			while (innerParticles < numberOfParticles) {
				position[0] = distribution_i(generator);
				i = floor((position[0] - d_grid_geometry->getXLower()[0])/dx[0] + 1E-10);
				position[1] = distribution_j(generator);
				j = floor((position[1] - d_grid_geometry->getXLower()[1])/dx[1] + 1E-10);
				if (i >= boxfirst(0) && i <= boxlast(0) && j >= boxfirst(1) && j <= boxlast(1)) {
					innerParticles++;
					//Region: mainI
					if (i >= boxfirst1(0) && i <= boxlast1(0) && j >= boxfirst1(1) && j <= boxlast1(1)) {
						hier::Index idx(i, j);
						Particles<T>* part = particleVariables->getItem(idx);
						T* particle = new T(0, position, 1);
						T* oldParticle = part->overlaps(*particle);
						if (oldParticle != NULL) {
							part->deleteParticle(*oldParticle);
						}
						part->addParticle(*particle);
						delete particle;
					}
				}
			}
		} else if (particleDistribution.compare("BOX") == 0) {
			//Region: mainI
			iMapStart = d_grid_geometry->getXLower()[0] + domain_offset_factor[0] * particleSeparation[0] - (floor(0.5 + d_ghost_width * (dx[0]/particleSeparation[0])))*particleSeparation[0];
			iMapEnd = d_grid_geometry->getXUpper()[0] + d_ghost_width * dx[0];
			jMapStart = d_grid_geometry->getXLower()[1] + domain_offset_factor[1] * particleSeparation[1] - (floor(0.5 + d_ghost_width * (dx[1]/particleSeparation[1])))*particleSeparation[1];
			jMapEnd = d_grid_geometry->getXUpper()[1] + d_ghost_width * dx[1];
			for (int count_j = 0; jMapStart + count_j * particleSeparation[1] < jMapEnd; count_j++) {
				for (int count_i = 0; iMapStart + count_i * particleSeparation[0] < iMapEnd; count_i++) {
					position[0] = iMapStart + count_i * particleSeparation[0];
					i = floor((position[0] - d_grid_geometry->getXLower()[0])/dx[0] + 1E-10);
					position[1] = jMapStart + count_j * particleSeparation[1];
					j = floor((position[1] - d_grid_geometry->getXLower()[1])/dx[1] + 1E-10);
					if (i >= boxfirst1(0) && i <= boxlast1(0) && j >= boxfirst1(1) && j <= boxlast1(1) && greaterEq(position[0], box_min[0]) && lessEq(position[0], box_max[0]) && greaterEq(position[1], box_min[1]) && lessEq(position[1], box_max[1])) {
						hier::Index idx(i, j);
						Particles<T>* part = particleVariables->getItem(idx);
						T* particle = new T(0, position, 1);
						T* oldParticle = part->overlaps(*particle);
						if (oldParticle != NULL) {
							part->deleteParticle(*oldParticle);
						}
						part->addParticle(*particle);
						delete particle;
					}
				}
			}
		} else {
			//Region: mainI
			iMapEnd = d_grid_geometry->getXUpper()[0] + d_ghost_width * dx[0];
			jMapStart = d_grid_geometry->getXLower()[1] + domain_offset_factor[1] * particleSeparation[1] - (floor(0.5 + d_ghost_width * (dx[1]/particleSeparation[1])))*particleSeparation[1];
			jMapEnd = d_grid_geometry->getXUpper()[1] + d_ghost_width * dx[1];
			for (int count_j = 0; jMapStart + count_j * particleSeparation[1] < jMapEnd; count_j++) {
				iMapStart = d_grid_geometry->getXLower()[0] + domain_offset_factor[0] * particleSeparation[0] - (floor(0.5 + d_ghost_width * (dx[0]/particleSeparation[0])))*particleSeparation[0];
				if (particleDistribution.compare("STAGGERED") == 0 && isEven(count_j)) {
					iMapStart = iMapStart + particleSeparation[0]/2;
				}
				for (int count_i = 0; iMapStart + count_i * particleSeparation[0] < iMapEnd; count_i++) {
					position[0] = iMapStart + count_i * particleSeparation[0];
					i = floor((position[0] - d_grid_geometry->getXLower()[0])/dx[0] + 1E-10);
					position[1] = jMapStart + count_j * particleSeparation[1];
					j = floor((position[1] - d_grid_geometry->getXLower()[1])/dx[1] + 1E-10);
					if (i >= boxfirst1(0) && i <= boxlast1(0) && j >= boxfirst1(1) && j <= boxlast1(1)) {
						hier::Index idx(i, j);
						Particles<T>* part = particleVariables->getItem(idx);
						T* particle = new T(0, position, 1);
						T* oldParticle = part->overlaps(*particle);
						if (oldParticle != NULL) {
							part->deleteParticle(*oldParticle);
						}
						part->addParticle(*particle);
						delete particle;
					}
				}
			}
		}
	}
}
template<class T>
void Problem::floodfillParticles(const hier::Patch& patch, std::vector<double> particleSeparation, std::vector<double> domain_offset_factor, int i, int j, int pred, int seg) const {

	double position[2], newPosition[2];
	int tmpVar_id;
	if (std::is_same<T, Particle_particles>::value) {
		tmpVar_id = d_particleVariables_particles_id;
	}
	std::shared_ptr< pdat::IndexData<Particles<T>, pdat::CellGeometry > > particleVariables(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles<T>, pdat::CellGeometry >, hier::PatchData>(patch.getPatchData(tmpVar_id)));
	std::shared_ptr< pdat::IndexData<NonSyncs, pdat::CellGeometry > > nonSyncVariable(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< NonSyncs, pdat::CellGeometry >, hier::PatchData>(patch.getPatchData(d_nonSyncP_id)));

	const hier::Index boxfirst = particleVariables->getGhostBox().lower();
	const hier::Index boxlast  = particleVariables->getGhostBox().upper();

	const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch.getPatchGeometry()));
	const double* dx  = patch_geom->getDx();
	int indexi;
	int indexj;

	stack<Point> mystack;

	Point p;
	p.i = i;
	p.j = j;

	mystack.push(p);
	while(mystack.size() > 0) {
		p = mystack.top();
		mystack.pop();
		position[0] = d_grid_geometry->getXLower()[0] + domain_offset_factor[0] * particleSeparation[0] - (floor(0.5 + d_ghost_width * (dx[0]/particleSeparation[0])))*particleSeparation[0] + (p.i)*particleSeparation[0];
		position[1] = d_grid_geometry->getXLower()[1] + domain_offset_factor[1] * particleSeparation[1] - (floor(0.5 + d_ghost_width * (dx[1]/particleSeparation[1])))*particleSeparation[1] + (p.j)*particleSeparation[1];
		indexi = floor((position[0] - d_grid_geometry->getXLower()[0])/dx[0] + 1E-10);
		indexj = floor((position[1] - d_grid_geometry->getXLower()[1])/dx[1] + 1E-10);
		hier::Index idx(indexi, indexj);
		if (indexi >= boxfirst(0) && indexi <= boxlast(0) && indexj >= boxfirst(1) && indexj <= boxlast(1)) {
			T* particle = new T(0, position, 0);
			Particles<T>* part = particleVariables->getItem(idx);
			T* oldParticle = part->overlaps(*particle);
			NonSyncs* nonSyncpart = nonSyncVariable->getItem(idx);
			NonSync* nonSyncparticle = new NonSync(0, position);
			NonSync* nonSyncoldParticle = nonSyncpart->overlaps(*nonSyncparticle);
			if (nonSyncoldParticle->getnonSync() == 0) {
				nonSyncoldParticle->setnonSync(pred);
				oldParticle->interior = pred;
				if (pred == 2) {
					oldParticle->region = seg;
				}
				if (greaterEq(position[0] - particleSeparation[0], patch_geom->getXLower()[0] - dx[0]*d_ghost_width)) {
					newPosition[0] = d_grid_geometry->getXLower()[0] + domain_offset_factor[0] * particleSeparation[0] - (floor(0.5 + d_ghost_width * (dx[0]/particleSeparation[0])))*particleSeparation[0] + (p.i - 1)*particleSeparation[0];
					newPosition[1] = d_grid_geometry->getXLower()[1] + domain_offset_factor[1] * particleSeparation[1] - (floor(0.5 + d_ghost_width * (dx[1]/particleSeparation[1])))*particleSeparation[1] + (p.j)*particleSeparation[1];
					indexi = floor((newPosition[0] - d_grid_geometry->getXLower()[0])/dx[0] + 1E-10);
					indexj = floor((newPosition[1] - d_grid_geometry->getXLower()[1])/dx[1] + 1E-10);
					hier::Index idx(indexi, indexj);
					if (indexi >= boxfirst(0) && indexi <= boxlast(0) && indexj >= boxfirst(1) && indexj <= boxlast(1)) {
						NonSyncs* newnonSyncpart = nonSyncVariable->getItem(idx);
						NonSync* newnonSyncparticle = new NonSync(0, newPosition);
						NonSync* newnonSyncoldParticle = newnonSyncpart->overlaps(*newnonSyncparticle);
						if (newnonSyncoldParticle->getnonSync() == 0) {
							Point np;
							np.i = p.i-1;
							np.j = p.j;
							mystack.push(np);
						}
						delete newnonSyncparticle;
					}
				}
				if (position[0] + particleSeparation[0] < patch_geom->getXUpper()[0] + dx[0]*d_ghost_width) {
					newPosition[0] = d_grid_geometry->getXLower()[0] + domain_offset_factor[0] * particleSeparation[0] - (floor(0.5 + d_ghost_width * (dx[0]/particleSeparation[0])))*particleSeparation[0] + (p.i + 1)*particleSeparation[0];
					newPosition[1] = d_grid_geometry->getXLower()[1] + domain_offset_factor[1] * particleSeparation[1] - (floor(0.5 + d_ghost_width * (dx[1]/particleSeparation[1])))*particleSeparation[1] + (p.j)*particleSeparation[1];
					indexi = floor((newPosition[0] - d_grid_geometry->getXLower()[0])/dx[0] + 1E-10);
					indexj = floor((newPosition[1] - d_grid_geometry->getXLower()[1])/dx[1] + 1E-10);
					hier::Index idx(indexi, indexj);
					if (indexi >= boxfirst(0) && indexi <= boxlast(0) && indexj >= boxfirst(1) && indexj <= boxlast(1)) {
						NonSyncs* newnonSyncpart = nonSyncVariable->getItem(idx);
						NonSync* newnonSyncparticle = new NonSync(0, newPosition);
						NonSync* newnonSyncoldParticle = newnonSyncpart->overlaps(*newnonSyncparticle);
						if (newnonSyncoldParticle->getnonSync() == 0) {
							Point np;
							np.i = p.i+1;
							np.j = p.j;
							mystack.push(np);
						}
						delete newnonSyncparticle;
					}
				}
				if (greaterEq(position[1] - particleSeparation[1], patch_geom->getXLower()[1] - dx[1]*d_ghost_width)) {
					newPosition[0] = d_grid_geometry->getXLower()[0] + domain_offset_factor[0] * particleSeparation[0] - (floor(0.5 + d_ghost_width * (dx[0]/particleSeparation[0])))*particleSeparation[0] + (p.i)*particleSeparation[0];
					newPosition[1] = d_grid_geometry->getXLower()[1] + domain_offset_factor[1] * particleSeparation[1] - (floor(0.5 + d_ghost_width * (dx[1]/particleSeparation[1])))*particleSeparation[1] + (p.j - 1)*particleSeparation[1];
					indexi = floor((newPosition[0] - d_grid_geometry->getXLower()[0])/dx[0] + 1E-10);
					indexj = floor((newPosition[1] - d_grid_geometry->getXLower()[1])/dx[1] + 1E-10);
					hier::Index idx(indexi, indexj);
					if (indexi >= boxfirst(0) && indexi <= boxlast(0) && indexj >= boxfirst(1) && indexj <= boxlast(1)) {
						NonSyncs* newnonSyncpart = nonSyncVariable->getItem(idx);
						NonSync* newnonSyncparticle = new NonSync(0, newPosition);
						NonSync* newnonSyncoldParticle = newnonSyncpart->overlaps(*newnonSyncparticle);
						if (newnonSyncoldParticle->getnonSync() == 0) {
							Point np;
							np.i = p.i;
							np.j = p.j-1;
							mystack.push(np);
						}
						delete newnonSyncparticle;
					}
				}
				if (position[1] + particleSeparation[1] < patch_geom->getXUpper()[1] + dx[1]*d_ghost_width) {
					newPosition[0] = d_grid_geometry->getXLower()[0] + domain_offset_factor[0] * particleSeparation[0] - (floor(0.5 + d_ghost_width * (dx[0]/particleSeparation[0])))*particleSeparation[0] + (p.i)*particleSeparation[0];
					newPosition[1] = d_grid_geometry->getXLower()[1] + domain_offset_factor[1] * particleSeparation[1] - (floor(0.5 + d_ghost_width * (dx[1]/particleSeparation[1])))*particleSeparation[1] + (p.j + 1)*particleSeparation[1];
					indexi = floor((newPosition[0] - d_grid_geometry->getXLower()[0])/dx[0] + 1E-10);
					indexj = floor((newPosition[1] - d_grid_geometry->getXLower()[1])/dx[1] + 1E-10);
					hier::Index idx(indexi, indexj);
					if (indexi >= boxfirst(0) && indexi <= boxlast(0) && indexj >= boxfirst(1) && indexj <= boxlast(1)) {
						NonSyncs* newnonSyncpart = nonSyncVariable->getItem(idx);
						NonSync* newnonSyncparticle = new NonSync(0, newPosition);
						NonSync* newnonSyncoldParticle = newnonSyncpart->overlaps(*newnonSyncparticle);
						if (newnonSyncoldParticle->getnonSync() == 0) {
							Point np;
							np.i = p.i;
							np.j = p.j+1;
							mystack.push(np);
						}
						delete newnonSyncparticle;
					}
				}
			}
			delete nonSyncparticle;
			delete particle;
		}
	}
}





void Problem::interphaseMapping(const double time ,const bool initial_time,const int ln, const std::shared_ptr< hier::PatchLevel >& level, const int remesh) {
	const tbox::SAMRAI_MPI& mpi(tbox::SAMRAI_MPI::getSAMRAIWorld());
	if (remesh == 1) {
		//Update extrapolation variables
	}
}


/*
 * Checks if the point has to be stalled
 */
bool Problem::checkStalled(std::shared_ptr< hier::Patch > patch, int i, int j, int v) const {
	double* FOV = ((pdat::NodeData<double> *) patch->getPatchData(v).get())->getPointer();
	double* interior = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_regridding_value_id).get())->getPointer();
	//Get the dimensions of the patch
	const hier::Index boxfirst = patch->getBox().lower();
	const hier::Index boxlast  = patch->getBox().upper();
	//Auxiliary definitions
	int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
	int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;

	int stencilAcc, stencilAccMax_i, stencilAccMax_j;
	bool notEnoughStencil = false;
	int FOV_threshold = 0;
	if (vector(FOV, i, j) <= FOV_threshold) {
		notEnoughStencil = true;
	} else {
		stencilAcc = 0;
		stencilAccMax_i = 0;
		for (int it1 = MAX(i-d_regionMinThickness, 0); it1 <= MIN(i+d_regionMinThickness, ilast - 1); it1++) {
			if (vector(FOV, it1, j) > FOV_threshold) {
				stencilAcc++;
			} else {
				stencilAccMax_i = MAX(stencilAccMax_i, stencilAcc);
				stencilAcc = 0;
			}
		}
		stencilAccMax_i = MAX(stencilAccMax_i, stencilAcc);
		stencilAcc = 0;
		stencilAccMax_j = 0;
		for (int jt1 = MAX(j-d_regionMinThickness, 0); jt1 <= MIN(j+d_regionMinThickness, jlast - 1); jt1++) {
			if (vector(FOV, i, jt1) > FOV_threshold) {
				stencilAcc++;
			} else {
				stencilAccMax_j = MAX(stencilAccMax_j, stencilAcc);
				stencilAcc = 0;
			}
		}
		stencilAccMax_j = MAX(stencilAccMax_j, stencilAcc);
		if ((stencilAccMax_i < d_regionMinThickness) || (stencilAccMax_j < d_regionMinThickness)) {
			notEnoughStencil = true;
		}
	}
	return notEnoughStencil;
}






/*
 * Initialize data on a patch. This initialization is done only at the begining of the simulation.
 */
void Problem::initializeDataOnPatch(hier::Patch& patch, 
                                    const double time,
                                    const bool initial_time)
{
	(void) time;
   	if (initial_time) {
		// Initial conditions		
		//Get fields, auxiliary fields and local variables that are going to be used.
			std::shared_ptr< pdat::IndexData<Particles<Particle_particles>, pdat::CellGeometry > > particleVariables_particles(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles<Particle_particles>, pdat::CellGeometry >, hier::PatchData>(patch.getPatchData(d_particleVariables_particles_id)));
		
		//Get the dimensions of the patch
		hier::Box pbox = patch.getBox();
		double* FOV_1 = ((pdat::NodeData<double> *) patch.getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch.getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch.getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch.getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch.getPatchData(d_FOV_yUpper_id).get())->getPointer();
		const hier::Index boxfirst = patch.getBox().lower();
		const hier::Index boxlast  = patch.getBox().upper();
		
		const hier::Index boxfirstP = particleVariables_particles->getGhostBox().lower();
		const hier::Index boxlastP = particleVariables_particles->getGhostBox().upper();
		
		//Get delta spaces into an array. dx, dy, dz.
		const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch.getPatchGeometry()));
		const double* dx  = patch_geom->getDx();
		
		int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
		int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
		for(int j = 0; j < jlast; j++) {
			for(int i = 0; i < ilast; i++) {
				if (patch.getPatchLevelNumber() == 0 && (i + 1 < ilast && j + 1 < jlast)) {
					hier::Index idx(i + boxfirstP(0), j + boxfirstP(1));
					Particles<Particle_particles>* part_particles = particleVariables_particles->getItem(idx);
					for (int pit = part_particles->getNumberOfParticles() - 1; pit >= 0; pit--) {
						Particle_particles* particle = part_particles->getParticle(pit);
						if (particle->region == 1 || particle->region == 2) {
							particle->field = 0.0;
							particle->auxiliaryField = 0.0;
							particle->derAuxiliaryField = 0.0;
						}
		
					}
				}
			}
		}
		

   	}
}



/*
 * Gets the coarser patch that contains the box
 */
const std::shared_ptr<hier::Patch >& Problem::getCoarserPatch(
	const std::shared_ptr< hier::PatchLevel >& level,
	const hier::Box interior, 
	const hier::IntVector ratio)
{
	const hier::Box& coarsenBox = hier::Box::coarsen(interior, ratio);
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr< hier::Patch >& patch = *p_it;
		const hier::Box& interior_C = patch->getBox();
		if (interior_C.intersects(coarsenBox)) {
			return patch;		
		}
	}
    return NULL;
}

/*
 * Reset the hierarchy-dependent internal information.
 */
void Problem::resetHierarchyConfiguration (
   const std::shared_ptr<hier::PatchHierarchy >& new_hierarchy ,
   int coarsest_level ,
   int finest_level )
{
	int finest_hiera_level = new_hierarchy->getFinestLevelNumber();

   	//  If we have added or removed a level, resize the schedule arrays

	d_bdry_sched_advance.resize(finest_hiera_level+1);
	d_coarsen_schedule.resize(finest_hiera_level+1);
	d_bdry_sched_postCoarsen.resize(finest_hiera_level+1);
	//  Build coarsen and refine communication schedules.
	for (int ln = coarsest_level; ln <= finest_hiera_level; ln++) {
		std::shared_ptr< hier::PatchLevel > level(new_hierarchy->getPatchLevel(ln));
		d_bdry_sched_advance[ln] = d_bdry_fill_advance->createSchedule(level,ln-1,new_hierarchy,this);
		d_bdry_sched_postCoarsen[ln] = d_bdry_post_coarsen->createSchedule(level);
		// coarsen schedule only for levels > 0
		if (ln > 0) {
			std::shared_ptr< hier::PatchLevel > coarser_level(new_hierarchy->getPatchLevel(ln-1));
			d_coarsen_schedule[ln] = d_coarsen_algorithm->createSchedule(coarser_level, level, NULL);
		}
	}

}



/*
 * This method sets the physical boundary conditions.
 */
void Problem::setPhysicalBoundaryConditions(
   hier::Patch& patch,
   const double fill_time,
   const hier::IntVector& ghost_width_to_fill)
{
	//Boundary must not be implemented in this method
}

/*
 * Set up external plotter to plot internal data from this class.
 * Register variables appropriate for plotting.
 */
int Problem::setupPlotterParticles(ParticleDataWriter &plotter) const {
	if (!d_patch_hierarchy) {
		TBOX_ERROR(d_object_name << ": No hierarchy inn"
		<< " Problem::setupPlottern"
			<< "The hierarchy must be set before callingn"
			<< "this function.n");
	}
	hier::VariableDatabase *vdb = hier::VariableDatabase::getDatabase();

	for (set<string>::const_iterator it = d_full_particle_writer_variables.begin() ; it != d_full_particle_writer_variables.end(); ++it) {

		string var_to_register = *it;
		if (!(particleVariableList_particles.end() != std::find(particleVariableList_particles.begin(), particleVariableList_particles.end(), var_to_register))) {
			TBOX_ERROR(d_object_name << ": Variable selected for Particle write not found:" <<  var_to_register);
		}
		if (particleVariableList_particles.end() != std::find(particleVariableList_particles.begin(), particleVariableList_particles.end(), var_to_register)) {
			plotter.registerPlotVariable<Particle_particles>(var_to_register,d_particleVariables_particles_id);
		}
	}
	return 0;
}


/*
 * Perform a single step from the discretization schema algorithm   
 */
double Problem::advanceLevel(
   const std::shared_ptr<hier::PatchLevel>& level,
   const std::shared_ptr<hier::PatchHierarchy>& hierarchy,
   const double current_time,
   const double new_time,
   const bool first_step,
   const bool last_step,
   const bool regrid_advance)
{
	const tbox::SAMRAI_MPI& mpi(tbox::SAMRAI_MPI::getSAMRAIWorld());

	const int ln = level->getLevelNumber();
	const double simPlat_dt = new_time - current_time;
	const double level_ratio = level->getRatioToCoarserLevel().max();
	if (first_step) {
		bo_substep_iteration[ln] = 0;
	}
	else {
		bo_substep_iteration[ln] = bo_substep_iteration[ln] + 1;
	}
	double level_influenceRadius = influenceRadius/MAX(1,level_ratio);
	double volume_level_factor = 1.0 / MAX(1, level->getRatioToLevelZero().getProduct());
	time_interpolate_operator_particles_particles->setRatio(level_ratio);
	refine_particle_operator_particles->setStep(0);
	time_interpolate_operator_particles_particles->setStep(0);
	time_interpolate_operator_particles_particles->setTimeSubstepNumber(bo_substep_iteration[ln]);

	if (d_refinedTimeStepping && first_step && ln > 0) {
		current_iteration[ln] = (current_iteration[ln - 1] - 1) * hierarchy->getRatioToCoarserLevel(ln).max() + 1;
	} else {
		current_iteration[ln] = current_iteration[ln] + 1;
	}
	int previous_iteration = current_iteration[ln] - 1;
	int outputCycle = current_iteration[ln];
	int maxLevels = hierarchy->getMaxNumberOfLevels();
	if (maxLevels > ln + 1) {
		int currentLevelNumber = ln;
		while (currentLevelNumber < maxLevels - 1) {
			int ratio = hierarchy->getRatioToCoarserLevel(currentLevelNumber + 1).max();
			outputCycle = outputCycle * ratio;
			previous_iteration = previous_iteration * ratio;
			currentLevelNumber++;
		}
	}

	t_step->start();
  	// Shifting time
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch > patch = *p_it;
		std::shared_ptr< pdat::IndexData<Particles<Particle_particles>, pdat::CellGeometry > > particleVariables_particles(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles<Particle_particles>, pdat::CellGeometry >, hier::PatchData>(patch->getPatchData(d_particleVariables_particles_id)));
		const hier::Index boxfirst = particleVariables_particles->getGhostBox().lower();
		const hier::Index boxlast  = particleVariables_particles->getGhostBox().upper();
		//Get delta spaces into an array. dx, dy, dz.
		std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
		const double* dx  = patch_geom->getDx();
		double* tmpdx = new double[2];
		tmpdx[0] = dx[0] * hierarchy->getRatioToCoarserLevel(ln)[0];
		tmpdx[1] = dx[1] * hierarchy->getRatioToCoarserLevel(ln)[1];
		time_interpolate_operator_particles_particles->setDx(tmpdx);
		delete[] tmpdx;
		for (int index1 = boxfirst(1); index1 <= boxlast(1); index1++) {
			for (int index0 = boxfirst(0); index0 <= boxlast(0); index0++) {
				hier::Index idx(index0, index1);
				Particles<Particle_particles>* part_particles = particleVariables_particles->getItem(idx);
				part_particles->setTime_p(current_time);
				part_particles->setTimepred(current_time + simPlat_dt * 0.5);
				part_particles->setTime(current_time + simPlat_dt);
				for (int pit = part_particles->getNumberOfParticles() - 1; pit >= 0; pit--) {
					Particle_particles* particle = part_particles->getParticle(pit);
					particle->field_p = particle->field;
					particle->positionx_p = particle->positionx;
					particle->positiony_p = particle->positiony;
				}
			}
		}
	}
  	// Evolution
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch >& patch = *p_it;
		double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
	
		std::shared_ptr< pdat::IndexData<Particles<Particle_particles>, pdat::CellGeometry > > particleVariables_particles(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles<Particle_particles>, pdat::CellGeometry >, hier::PatchData>(patch->getPatchData(d_particleVariables_particles_id)));
		double derivative_normalization, interpolation_normalization, d_auxVarField1_o0_t0_m0_l0, d_field_o0_t0_m0_l0, RHS_dissipative_term_field_x, RHS_dissipative_term_field_y, RHS_field_Cons_x, d_field_o0_t1_m0_l0, auxVarField2_neighbour_p, RHS_field_sources, d_auxVarField1_o0_t1_m0_l0_p, m_auxVarField1_o0_t0_l0_p, auxVarField1_p, m_field_o0_t0_l0, RHS_field;
	
		//Get the dimensions of the patch
		const hier::Index boxfirst = patch->getBox().lower();
		const hier::Index boxlast = patch->getBox().upper();
	
		const hier::Index boxfirstP = particleVariables_particles->getGhostBox().lower();
		const hier::Index boxlastP = particleVariables_particles->getGhostBox().upper();
	
		//Get delta spaces into an array. dx, dy, dz.
		std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
		const double* dx  = patch_geom->getDx();
	
		//Auxiliary definitions
		int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
		int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
		for(int j = 0; j < jlast; j++) {
			for(int i = 0; i < ilast; i++) {
				if (i + 1 < ilast && j + 1 < jlast) {
					hier::Index idx(i + boxfirstP(0), j + boxfirstP(1));
					Particles<Particle_particles>* part_particles = particleVariables_particles->getItem(idx);
					for (int pit = part_particles->getNumberOfParticles() - 1; pit >= 0; pit--) {
						Particle_particles* particle = part_particles->getParticle(pit);
						derivative_normalization = 0.0;
						interpolation_normalization = 0.0;
						d_auxVarField1_o0_t0_m0_l0 = 0.0;
						d_field_o0_t0_m0_l0 = 0.0;
						RHS_dissipative_term_field_x = 0.0;
						RHS_dissipative_term_field_y = 0.0;
						RHS_field_Cons_x = 0.0;
						d_field_o0_t1_m0_l0 = 0.0;
						miniIndex = MAX(0, MAX((int) floor((particle->positionx_p - 2 * level_influenceRadius - d_grid_geometry->getXLower()[0])/dx[0]) - boxfirst(0) + d_ghost_width, (int) i - ceil((2 * level_influenceRadius)/dx[0])));
						maxiIndex = MIN(boxlast(0) - boxfirst(0) + 2 * d_ghost_width, MIN((int) floor((particle->positionx_p + 2 * level_influenceRadius - d_grid_geometry->getXLower()[0])/dx[0]) - boxfirst(0) + d_ghost_width, (int) i + ceil((2 * level_influenceRadius)/dx[0])));
						minjIndex = MAX(0, MAX((int) floor((particle->positiony_p - 2 * level_influenceRadius - d_grid_geometry->getXLower()[1])/dx[1]) - boxfirst(1) + d_ghost_width, (int) j - ceil((2 * level_influenceRadius)/dx[1])));
						maxjIndex = MIN(boxlast(1) - boxfirst(1) + 2 * d_ghost_width, MIN((int) floor((particle->positiony_p + 2 * level_influenceRadius - d_grid_geometry->getXLower()[1])/dx[1]) - boxfirst(1) + d_ghost_width, (int) j + ceil((2 * level_influenceRadius)/dx[1])));
						for(int jb = minjIndex; jb <= maxjIndex; jb++) {
							for(int ib = miniIndex; ib <= maxiIndex; ib++) {
								hier::Index idxb(ib + boxfirstP(0), jb + boxfirstP(1));
								Particles<Particle_particles>* partb_particles = particleVariables_particles->getItem(idxb);
								for (int pitb = 0; pitb < partb_particles->getNumberOfParticles(); pitb++) {
									Particle_particles* particleb = partb_particles->getParticle(pitb);
									double particle_distance = particle->distance_p(particleb);
									if (particle_distance < 2 * level_influenceRadius) {
										d_auxVarField1_o0_t0_m0_l0 = d_auxVarField1_o0_t0_m0_l0 + SPHAsym_(particle->field_p, particleb->field_p, particle->positionx_p, particleb->positionx_p, 1.0 * volume_level_factor, particle_distance, dimensions_main, gW_(particle_distance / level_influenceRadius, dx, simPlat_dt, ilast, jlast), dx, simPlat_dt, ilast, jlast);
										d_field_o0_t0_m0_l0 = d_field_o0_t0_m0_l0 + SPHSym_(particle->auxiliaryField, particleb->auxiliaryField, particle->positiony_p, particleb->positiony_p, 1.0 * volume_level_factor, particle_distance, dimensions_main, gW_(particle_distance / level_influenceRadius, dx, simPlat_dt, ilast, jlast), dx, simPlat_dt, ilast, jlast);
										auxVarField2_neighbour_p = particleb->field_p * particleb->auxiliaryField;
										RHS_field_Cons_x = RHS_field_Cons_x - SPHSym_(particle->auxiliaryField, particleb->auxiliaryField, particle->positionx_p, particleb->positionx_p, 1.0 * volume_level_factor, particle_distance, dimensions_main, gW_(particle_distance / level_influenceRadius, dx, simPlat_dt, ilast, jlast), dx, simPlat_dt, ilast, jlast);
										RHS_field_sources = RHS_field_sources + Interpolation_(1.0 * volume_level_factor, auxVarField2_neighbour_p, W_(particle_distance / level_influenceRadius, dx, simPlat_dt, ilast, jlast), dx, simPlat_dt, ilast, jlast);
										if (dissipation_factor_field > 0.0) {
											RHS_dissipative_term_field_x = RHS_dissipative_term_field_x + particleDissipation_(particle->field_p, particleb->field_p, particle->positionx_p, particleb->positionx_p, 1.0 * volume_level_factor, particle_distance, dimensions_main, gW_(particle_distance / level_influenceRadius, dx, simPlat_dt, ilast, jlast), dx, simPlat_dt, ilast, jlast);
											RHS_dissipative_term_field_y = RHS_dissipative_term_field_y + particleDissipation_(particle->field_p, particleb->field_p, particle->positiony_p, particleb->positiony_p, 1.0 * volume_level_factor, particle_distance, dimensions_main, gW_(particle_distance / level_influenceRadius, dx, simPlat_dt, ilast, jlast), dx, simPlat_dt, ilast, jlast);
										}
									}
								}				
							}
						}
						if (fabs(derivative_normalization) < 0.000000000000001) {
							derivative_normalization = 0.000000000000001;
						}
						if (fabs(interpolation_normalization) < 0.000000000000001) {
							interpolation_normalization = 0.000000000000001;
						}
						RHS_field_Cons_x = RHS_field_Cons_x / derivative_normalization;
						RHS_dissipative_term_field_x = RHS_dissipative_term_field_x / derivative_normalization;
						RHS_dissipative_term_field_y = RHS_dissipative_term_field_y / derivative_normalization;
						RHS_field_sources = RHS_field_sources / interpolation_normalization;
						d_field_o0_t0_m0_l0 = d_field_o0_t0_m0_l0 / derivative_normalization;
						d_auxVarField1_o0_t0_m0_l0 = d_auxVarField1_o0_t0_m0_l0 / derivative_normalization;
						d_auxVarField1_o0_t1_m0_l0_p = -2.0 * particle->derAuxiliaryField;
						m_auxVarField1_o0_t0_l0_p = particle->field_p * d_auxVarField1_o0_t0_m0_l0;
						auxVarField1_p = m_auxVarField1_o0_t0_l0_p + d_auxVarField1_o0_t1_m0_l0_p;
						d_field_o0_t1_m0_l0 = auxVarField1_p;
						m_field_o0_t0_l0 = 1.3 * d_field_o0_t0_m0_l0;
						RHS_field = (((m_field_o0_t0_l0 + d_field_o0_t1_m0_l0) + RHS_field_sources) + RHS_field_Cons_x) + dissipation_factor_field * level_influenceRadius * (RHS_dissipative_term_field_x + RHS_dissipative_term_field_y);
						if (particle->region == 1 || particle->newRegion == 1) {
							particle->predfield = PRED_(particle->field_p, RHS_field, dx, simPlat_dt, ilast, jlast);
							particle->predpositionx = particle->positionx_p;
							particle->predpositiony = particle->positiony_p;
						}
					}
				}
			}
		}
	}
	//Fill ghosts and periodical boundaries
	time_interpolate_operator_particles_particles->setStep(1);
	refine_particle_operator_particles->setStep(1);
	d_bdry_sched_advance[ln]->fillData(current_time + simPlat_dt * 0.5, false);
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch >& patch = *p_it;
		double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
	
		std::shared_ptr< pdat::IndexData<Particles<Particle_particles>, pdat::CellGeometry > > particleVariables_particles(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles<Particle_particles>, pdat::CellGeometry >, hier::PatchData>(patch->getPatchData(d_particleVariables_particles_id)));
		const hier::Index boxfirst = particleVariables_particles->getGhostBox().lower();
		const hier::Index boxlast = particleVariables_particles->getGhostBox().upper();
		for (int index1 = boxfirst(1); index1 <= boxlast(1); index1++) {
			for (int index0 = boxfirst(0); index0 <= boxlast(0); index0++) {
				hier::Index idx(index0, index1);
				//Correct the position if there is any periodical boundary
				Particles<Particle_particles>* part_particles = particleVariables_particles->getItem(idx);
				checkPosition(patch, index0, index1, part_particles);
			}
		}
	}
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch >& patch = *p_it;
		double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
	
		std::shared_ptr< pdat::IndexData<Particles<Particle_particles>, pdat::CellGeometry > > particleVariables_particles(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles<Particle_particles>, pdat::CellGeometry >, hier::PatchData>(patch->getPatchData(d_particleVariables_particles_id)));
		double predauxVarField2;
	
		//Get the dimensions of the patch
		const hier::Index boxfirst = patch->getBox().lower();
		const hier::Index boxlast = patch->getBox().upper();
	
		const hier::Index boxfirstP = particleVariables_particles->getGhostBox().lower();
		const hier::Index boxlastP = particleVariables_particles->getGhostBox().upper();
	
		//Get delta spaces into an array. dx, dy, dz.
		std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
		const double* dx  = patch_geom->getDx();
	
		//Auxiliary definitions
		int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
		int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
		for(int j = 0; j < jlast; j++) {
			for(int i = 0; i < ilast; i++) {
				if (i + 1 < ilast && j + 1 < jlast) {
					hier::Index idx(i + boxfirstP(0), j + boxfirstP(1));
					Particles<Particle_particles>* part_particles = particleVariables_particles->getItem(idx);
					for (int pit = part_particles->getNumberOfParticles() - 1; pit >= 0; pit--) {
						Particle_particles* particle = part_particles->getParticle(pit);
						predauxVarField2 = particle->predfield * particle->auxiliaryField;
						if (particle->region == 1 || particle->newRegion == 1) {
							particle->auxiliaryField = predauxVarField2;
						}
					}
				}
			}
		}
	}
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch >& patch = *p_it;
		double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
	
		std::shared_ptr< pdat::IndexData<Particles<Particle_particles>, pdat::CellGeometry > > particleVariables_particles(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles<Particle_particles>, pdat::CellGeometry >, hier::PatchData>(patch->getPatchData(d_particleVariables_particles_id)));
		double derivative_normalization, interpolation_normalization, d_derAuxiliaryField_o0_t0_m0_l0;
	
		//Get the dimensions of the patch
		const hier::Index boxfirst = patch->getBox().lower();
		const hier::Index boxlast = patch->getBox().upper();
	
		const hier::Index boxfirstP = particleVariables_particles->getGhostBox().lower();
		const hier::Index boxlastP = particleVariables_particles->getGhostBox().upper();
	
		//Get delta spaces into an array. dx, dy, dz.
		std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
		const double* dx  = patch_geom->getDx();
	
		//Auxiliary definitions
		int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
		int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
		for(int j = 0; j < jlast; j++) {
			for(int i = 0; i < ilast; i++) {
				if (i + 1 < ilast && j + 1 < jlast) {
					hier::Index idx(i + boxfirstP(0), j + boxfirstP(1));
					Particles<Particle_particles>* part_particles = particleVariables_particles->getItem(idx);
					for (int pit = part_particles->getNumberOfParticles() - 1; pit >= 0; pit--) {
						Particle_particles* particle = part_particles->getParticle(pit);
						derivative_normalization = 0.0;
						interpolation_normalization = 0.0;
						d_derAuxiliaryField_o0_t0_m0_l0 = 0.0;
						miniIndex = MAX(0, MAX((int) floor((particle->positionx_p - 2 * level_influenceRadius - d_grid_geometry->getXLower()[0])/dx[0]) - boxfirst(0) + d_ghost_width, (int) i - ceil((2 * level_influenceRadius)/dx[0])));
						maxiIndex = MIN(boxlast(0) - boxfirst(0) + 2 * d_ghost_width, MIN((int) floor((particle->positionx_p + 2 * level_influenceRadius - d_grid_geometry->getXLower()[0])/dx[0]) - boxfirst(0) + d_ghost_width, (int) i + ceil((2 * level_influenceRadius)/dx[0])));
						minjIndex = MAX(0, MAX((int) floor((particle->positiony_p - 2 * level_influenceRadius - d_grid_geometry->getXLower()[1])/dx[1]) - boxfirst(1) + d_ghost_width, (int) j - ceil((2 * level_influenceRadius)/dx[1])));
						maxjIndex = MIN(boxlast(1) - boxfirst(1) + 2 * d_ghost_width, MIN((int) floor((particle->positiony_p + 2 * level_influenceRadius - d_grid_geometry->getXLower()[1])/dx[1]) - boxfirst(1) + d_ghost_width, (int) j + ceil((2 * level_influenceRadius)/dx[1])));
						for(int jb = minjIndex; jb <= maxjIndex; jb++) {
							for(int ib = miniIndex; ib <= maxiIndex; ib++) {
								hier::Index idxb(ib + boxfirstP(0), jb + boxfirstP(1));
								Particles<Particle_particles>* partb_particles = particleVariables_particles->getItem(idxb);
								for (int pitb = 0; pitb < partb_particles->getNumberOfParticles(); pitb++) {
									Particle_particles* particleb = partb_particles->getParticle(pitb);
									double particle_distance = particle->distance_p(particleb);
									if (particle_distance < 2 * level_influenceRadius) {
										derivative_normalization = derivative_normalization + SPHfirstderivativenormalization_(dimensions_main, 1.0 * volume_level_factor, particle_distance, gW_(particle_distance / level_influenceRadius, dx, simPlat_dt, ilast, jlast), dx, simPlat_dt, ilast, jlast);
										interpolation_normalization = interpolation_normalization + Normalization_(1.0 * volume_level_factor, W_(particle_distance / level_influenceRadius, dx, simPlat_dt, ilast, jlast), dx, simPlat_dt, ilast, jlast);
										d_derAuxiliaryField_o0_t0_m0_l0 = d_derAuxiliaryField_o0_t0_m0_l0 + SPHAsym_(particle->predfield, particleb->predfield, particle->positiony_p, particleb->positiony_p, 1.0 * volume_level_factor, particle_distance, dimensions_main, gW_(particle_distance / level_influenceRadius, dx, simPlat_dt, ilast, jlast), dx, simPlat_dt, ilast, jlast);
									}
								}				
							}
						}
						if (fabs(derivative_normalization) < 0.000000000000001) {
							derivative_normalization = 0.000000000000001;
						}
						if (fabs(interpolation_normalization) < 0.000000000000001) {
							interpolation_normalization = 0.000000000000001;
						}
						d_derAuxiliaryField_o0_t0_m0_l0 = d_derAuxiliaryField_o0_t0_m0_l0 / derivative_normalization;
						particle->m_derAuxiliaryField_o0_t0_l0_1 = particle->predfield * d_derAuxiliaryField_o0_t0_m0_l0;
					}
				}
			}
		}
	}
	//Fill ghosts and periodical boundaries
	d_bdry_sched_advance[ln]->fillData(current_time, false);
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch >& patch = *p_it;
		double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
	
		std::shared_ptr< pdat::IndexData<Particles<Particle_particles>, pdat::CellGeometry > > particleVariables_particles(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles<Particle_particles>, pdat::CellGeometry >, hier::PatchData>(patch->getPatchData(d_particleVariables_particles_id)));
		const hier::Index boxfirst = particleVariables_particles->getGhostBox().lower();
		const hier::Index boxlast = particleVariables_particles->getGhostBox().upper();
		for (int index1 = boxfirst(1); index1 <= boxlast(1); index1++) {
			for (int index0 = boxfirst(0); index0 <= boxlast(0); index0++) {
				hier::Index idx(index0, index1);
				//Correct the position if there is any periodical boundary
				Particles<Particle_particles>* part_particles = particleVariables_particles->getItem(idx);
				checkPosition(patch, index0, index1, part_particles);
			}
		}
	}
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch >& patch = *p_it;
		double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
	
		std::shared_ptr< pdat::IndexData<Particles<Particle_particles>, pdat::CellGeometry > > particleVariables_particles(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles<Particle_particles>, pdat::CellGeometry >, hier::PatchData>(patch->getPatchData(d_particleVariables_particles_id)));
		double derivative_normalization, interpolation_normalization, d_derAuxiliaryField_o0_t0_m0_l1, predauxVarField2, d_derAuxiliaryField_o0_t1_m0_l0;
	
		//Get the dimensions of the patch
		const hier::Index boxfirst = patch->getBox().lower();
		const hier::Index boxlast = patch->getBox().upper();
	
		const hier::Index boxfirstP = particleVariables_particles->getGhostBox().lower();
		const hier::Index boxlastP = particleVariables_particles->getGhostBox().upper();
	
		//Get delta spaces into an array. dx, dy, dz.
		std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
		const double* dx  = patch_geom->getDx();
	
		//Auxiliary definitions
		int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
		int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
		for(int j = 0; j < jlast; j++) {
			for(int i = 0; i < ilast; i++) {
				if (i + 1 < ilast && j + 1 < jlast) {
					hier::Index idx(i + boxfirstP(0), j + boxfirstP(1));
					Particles<Particle_particles>* part_particles = particleVariables_particles->getItem(idx);
					for (int pit = part_particles->getNumberOfParticles() - 1; pit >= 0; pit--) {
						Particle_particles* particle = part_particles->getParticle(pit);
						derivative_normalization = 0.0;
						interpolation_normalization = 0.0;
						miniIndex = MAX(0, MAX((int) floor((particle->positionx_p - 2 * level_influenceRadius - d_grid_geometry->getXLower()[0])/dx[0]) - boxfirst(0) + d_ghost_width, (int) i - ceil((2 * level_influenceRadius)/dx[0])));
						maxiIndex = MIN(boxlast(0) - boxfirst(0) + 2 * d_ghost_width, MIN((int) floor((particle->positionx_p + 2 * level_influenceRadius - d_grid_geometry->getXLower()[0])/dx[0]) - boxfirst(0) + d_ghost_width, (int) i + ceil((2 * level_influenceRadius)/dx[0])));
						minjIndex = MAX(0, MAX((int) floor((particle->positiony_p - 2 * level_influenceRadius - d_grid_geometry->getXLower()[1])/dx[1]) - boxfirst(1) + d_ghost_width, (int) j - ceil((2 * level_influenceRadius)/dx[1])));
						maxjIndex = MIN(boxlast(1) - boxfirst(1) + 2 * d_ghost_width, MIN((int) floor((particle->positiony_p + 2 * level_influenceRadius - d_grid_geometry->getXLower()[1])/dx[1]) - boxfirst(1) + d_ghost_width, (int) j + ceil((2 * level_influenceRadius)/dx[1])));
						for(int jb = minjIndex; jb <= maxjIndex; jb++) {
							for(int ib = miniIndex; ib <= maxiIndex; ib++) {
								hier::Index idxb(ib + boxfirstP(0), jb + boxfirstP(1));
								Particles<Particle_particles>* partb_particles = particleVariables_particles->getItem(idxb);
								for (int pitb = 0; pitb < partb_particles->getNumberOfParticles(); pitb++) {
									Particle_particles* particleb = partb_particles->getParticle(pitb);
									double particle_distance = particle->distance_p(particleb);
									if (particle_distance < 2 * level_influenceRadius) {
										derivative_normalization = derivative_normalization + SPHfirstderivativenormalization_(dimensions_main, 1.0 * volume_level_factor, particle_distance, gW_(particle_distance / level_influenceRadius, dx, simPlat_dt, ilast, jlast), dx, simPlat_dt, ilast, jlast);
										interpolation_normalization = interpolation_normalization + Normalization_(1.0 * volume_level_factor, W_(particle_distance / level_influenceRadius, dx, simPlat_dt, ilast, jlast), dx, simPlat_dt, ilast, jlast);
										d_derAuxiliaryField_o0_t0_m0_l1 = d_derAuxiliaryField_o0_t0_m0_l1 + SPHAsym_(particle->m_derAuxiliaryField_o0_t0_l0_1, particleb->m_derAuxiliaryField_o0_t0_l0_1, particle->positionx_p, particleb->positionx_p, 1.0 * volume_level_factor, particle_distance, dimensions_main, gW_(particle_distance / level_influenceRadius, dx, simPlat_dt, ilast, jlast), dx, simPlat_dt, ilast, jlast);
									}
								}				
							}
						}
						predauxVarField2 = particle->predfield * particle->auxiliaryField;
						d_derAuxiliaryField_o0_t1_m0_l0 = predauxVarField2;
						if (fabs(derivative_normalization) < 0.000000000000001) {
							derivative_normalization = 0.000000000000001;
						}
						if (fabs(interpolation_normalization) < 0.000000000000001) {
							interpolation_normalization = 0.000000000000001;
						}
						d_derAuxiliaryField_o0_t0_m0_l1 = d_derAuxiliaryField_o0_t0_m0_l1 / derivative_normalization;
						if (particle->region == 1 || particle->newRegion == 1) {
							particle->derAuxiliaryField = d_derAuxiliaryField_o0_t0_m0_l1 + d_derAuxiliaryField_o0_t1_m0_l0;
						}
					}
				}
			}
		}
	}
	//Fill ghosts and periodical boundaries
	d_bdry_sched_advance[ln]->fillData(current_time, false);
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch >& patch = *p_it;
		double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
	
		std::shared_ptr< pdat::IndexData<Particles<Particle_particles>, pdat::CellGeometry > > particleVariables_particles(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles<Particle_particles>, pdat::CellGeometry >, hier::PatchData>(patch->getPatchData(d_particleVariables_particles_id)));
		const hier::Index boxfirst = particleVariables_particles->getGhostBox().lower();
		const hier::Index boxlast = particleVariables_particles->getGhostBox().upper();
		for (int index1 = boxfirst(1); index1 <= boxlast(1); index1++) {
			for (int index0 = boxfirst(0); index0 <= boxlast(0); index0++) {
				hier::Index idx(index0, index1);
				//Correct the position if there is any periodical boundary
				Particles<Particle_particles>* part_particles = particleVariables_particles->getItem(idx);
				checkPosition(patch, index0, index1, part_particles);
			}
		}
	}
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch >& patch = *p_it;
		double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
	
		std::shared_ptr< pdat::IndexData<Particles<Particle_particles>, pdat::CellGeometry > > particleVariables_particles(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles<Particle_particles>, pdat::CellGeometry >, hier::PatchData>(patch->getPatchData(d_particleVariables_particles_id)));
		double derivative_normalization, interpolation_normalization, d_auxVarField1_o0_t0_m0_l0, d_field_o0_t0_m0_l0, RHS_dissipative_term_field_x, RHS_dissipative_term_field_y, RHS_field_Cons_x, d_field_o0_t1_m0_l0, predauxVarField2_neighbour, RHS_field_sources, predd_auxVarField1_o0_t1_m0_l0, predm_auxVarField1_o0_t0_l0, predauxVarField1, m_field_o0_t0_l0, RHS_field;
	
		//Get the dimensions of the patch
		const hier::Index boxfirst = patch->getBox().lower();
		const hier::Index boxlast = patch->getBox().upper();
	
		const hier::Index boxfirstP = particleVariables_particles->getGhostBox().lower();
		const hier::Index boxlastP = particleVariables_particles->getGhostBox().upper();
	
		//Get delta spaces into an array. dx, dy, dz.
		std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
		const double* dx  = patch_geom->getDx();
	
		//Auxiliary definitions
		int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
		int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
		for(int j = 0; j < jlast; j++) {
			for(int i = 0; i < ilast; i++) {
				if (i + 1 < ilast && j + 1 < jlast) {
					hier::Index idx(i + boxfirstP(0), j + boxfirstP(1));
					Particles<Particle_particles>* part_particles = particleVariables_particles->getItem(idx);
					for (int pit = part_particles->getNumberOfParticles() - 1; pit >= 0; pit--) {
						Particle_particles* particle = part_particles->getParticle(pit);
						derivative_normalization = 0.0;
						interpolation_normalization = 0.0;
						d_auxVarField1_o0_t0_m0_l0 = 0.0;
						d_field_o0_t0_m0_l0 = 0.0;
						RHS_dissipative_term_field_x = 0.0;
						RHS_dissipative_term_field_y = 0.0;
						RHS_field_Cons_x = 0.0;
						d_field_o0_t1_m0_l0 = 0.0;
						miniIndex = MAX(0, MAX((int) floor((particle->predpositionx - 2 * level_influenceRadius - d_grid_geometry->getXLower()[0])/dx[0]) - boxfirst(0) + d_ghost_width, (int) i - ceil((2 * level_influenceRadius)/dx[0])));
						maxiIndex = MIN(boxlast(0) - boxfirst(0) + 2 * d_ghost_width, MIN((int) floor((particle->predpositionx + 2 * level_influenceRadius - d_grid_geometry->getXLower()[0])/dx[0]) - boxfirst(0) + d_ghost_width, (int) i + ceil((2 * level_influenceRadius)/dx[0])));
						minjIndex = MAX(0, MAX((int) floor((particle->predpositiony - 2 * level_influenceRadius - d_grid_geometry->getXLower()[1])/dx[1]) - boxfirst(1) + d_ghost_width, (int) j - ceil((2 * level_influenceRadius)/dx[1])));
						maxjIndex = MIN(boxlast(1) - boxfirst(1) + 2 * d_ghost_width, MIN((int) floor((particle->predpositiony + 2 * level_influenceRadius - d_grid_geometry->getXLower()[1])/dx[1]) - boxfirst(1) + d_ghost_width, (int) j + ceil((2 * level_influenceRadius)/dx[1])));
						for(int jb = minjIndex; jb <= maxjIndex; jb++) {
							for(int ib = miniIndex; ib <= maxiIndex; ib++) {
								hier::Index idxb(ib + boxfirstP(0), jb + boxfirstP(1));
								Particles<Particle_particles>* partb_particles = particleVariables_particles->getItem(idxb);
								for (int pitb = 0; pitb < partb_particles->getNumberOfParticles(); pitb++) {
									Particle_particles* particleb = partb_particles->getParticle(pitb);
									double particle_distance = particle->distancepred(particleb);
									if (particle_distance < 2 * level_influenceRadius) {
										d_auxVarField1_o0_t0_m0_l0 = d_auxVarField1_o0_t0_m0_l0 + SPHAsym_(particle->predfield, particleb->predfield, particle->predpositionx, particleb->predpositionx, 1.0 * volume_level_factor, particle_distance, dimensions_main, gW_(particle_distance / level_influenceRadius, dx, simPlat_dt, ilast, jlast), dx, simPlat_dt, ilast, jlast);
										d_field_o0_t0_m0_l0 = d_field_o0_t0_m0_l0 + SPHSym_(particle->auxiliaryField, particleb->auxiliaryField, particle->predpositiony, particleb->predpositiony, 1.0 * volume_level_factor, particle_distance, dimensions_main, gW_(particle_distance / level_influenceRadius, dx, simPlat_dt, ilast, jlast), dx, simPlat_dt, ilast, jlast);
										predauxVarField2_neighbour = particleb->predfield * particleb->auxiliaryField;
										RHS_field_Cons_x = RHS_field_Cons_x - SPHSym_(particle->auxiliaryField, particleb->auxiliaryField, particle->predpositionx, particleb->predpositionx, 1.0 * volume_level_factor, particle_distance, dimensions_main, gW_(particle_distance / level_influenceRadius, dx, simPlat_dt, ilast, jlast), dx, simPlat_dt, ilast, jlast);
										RHS_field_sources = RHS_field_sources + Interpolation_(1.0 * volume_level_factor, predauxVarField2_neighbour, W_(particle_distance / level_influenceRadius, dx, simPlat_dt, ilast, jlast), dx, simPlat_dt, ilast, jlast);
										if (dissipation_factor_field > 0.0) {
											RHS_dissipative_term_field_x = RHS_dissipative_term_field_x + particleDissipation_(particle->predfield, particleb->predfield, particle->predpositionx, particleb->predpositionx, 1.0 * volume_level_factor, particle_distance, dimensions_main, gW_(particle_distance / level_influenceRadius, dx, simPlat_dt, ilast, jlast), dx, simPlat_dt, ilast, jlast);
											RHS_dissipative_term_field_y = RHS_dissipative_term_field_y + particleDissipation_(particle->predfield, particleb->predfield, particle->predpositiony, particleb->predpositiony, 1.0 * volume_level_factor, particle_distance, dimensions_main, gW_(particle_distance / level_influenceRadius, dx, simPlat_dt, ilast, jlast), dx, simPlat_dt, ilast, jlast);
										}
									}
								}				
							}
						}
						if (fabs(derivative_normalization) < 0.000000000000001) {
							derivative_normalization = 0.000000000000001;
						}
						if (fabs(interpolation_normalization) < 0.000000000000001) {
							interpolation_normalization = 0.000000000000001;
						}
						RHS_field_Cons_x = RHS_field_Cons_x / derivative_normalization;
						RHS_dissipative_term_field_x = RHS_dissipative_term_field_x / derivative_normalization;
						RHS_dissipative_term_field_y = RHS_dissipative_term_field_y / derivative_normalization;
						RHS_field_sources = RHS_field_sources / interpolation_normalization;
						d_field_o0_t0_m0_l0 = d_field_o0_t0_m0_l0 / derivative_normalization;
						d_auxVarField1_o0_t0_m0_l0 = d_auxVarField1_o0_t0_m0_l0 / derivative_normalization;
						predd_auxVarField1_o0_t1_m0_l0 = -2.0 * particle->derAuxiliaryField;
						predm_auxVarField1_o0_t0_l0 = particle->predfield * d_auxVarField1_o0_t0_m0_l0;
						predauxVarField1 = predm_auxVarField1_o0_t0_l0 + predd_auxVarField1_o0_t1_m0_l0;
						d_field_o0_t1_m0_l0 = predauxVarField1;
						m_field_o0_t0_l0 = 1.3 * d_field_o0_t0_m0_l0;
						RHS_field = (((m_field_o0_t0_l0 + d_field_o0_t1_m0_l0) + RHS_field_sources) + RHS_field_Cons_x) + dissipation_factor_field * level_influenceRadius * (RHS_dissipative_term_field_x + RHS_dissipative_term_field_y);
						if (particle->region == 1 || particle->newRegion == 1) {
							particle->field = CORR_(particle->field_p, RHS_field, dx, simPlat_dt, ilast, jlast);
							particle->positionx = particle->predpositionx;
							particle->positiony = particle->predpositiony;
						}
					}
				}
			}
		}
	}
	//Fill ghosts and periodical boundaries
	time_interpolate_operator_particles_particles->setStep(2);
	refine_particle_operator_particles->setStep(2);
	d_bdry_sched_advance[ln]->fillData(current_time + simPlat_dt, false);
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch >& patch = *p_it;
		double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
	
		std::shared_ptr< pdat::IndexData<Particles<Particle_particles>, pdat::CellGeometry > > particleVariables_particles(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles<Particle_particles>, pdat::CellGeometry >, hier::PatchData>(patch->getPatchData(d_particleVariables_particles_id)));
		const hier::Index boxfirst = particleVariables_particles->getGhostBox().lower();
		const hier::Index boxlast = particleVariables_particles->getGhostBox().upper();
		for (int index1 = boxfirst(1); index1 <= boxlast(1); index1++) {
			for (int index0 = boxfirst(0); index0 <= boxlast(0); index0++) {
				hier::Index idx(index0, index1);
				//Correct the position if there is any periodical boundary
				Particles<Particle_particles>* part_particles = particleVariables_particles->getItem(idx);
				checkPosition(patch, index0, index1, part_particles);
			}
		}
	}
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch >& patch = *p_it;
		double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
	
		std::shared_ptr< pdat::IndexData<Particles<Particle_particles>, pdat::CellGeometry > > particleVariables_particles(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles<Particle_particles>, pdat::CellGeometry >, hier::PatchData>(patch->getPatchData(d_particleVariables_particles_id)));
		double auxVarField2;
	
		//Get the dimensions of the patch
		const hier::Index boxfirst = patch->getBox().lower();
		const hier::Index boxlast = patch->getBox().upper();
	
		const hier::Index boxfirstP = particleVariables_particles->getGhostBox().lower();
		const hier::Index boxlastP = particleVariables_particles->getGhostBox().upper();
	
		//Get delta spaces into an array. dx, dy, dz.
		std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
		const double* dx  = patch_geom->getDx();
	
		//Auxiliary definitions
		int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
		int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
		for(int j = 0; j < jlast; j++) {
			for(int i = 0; i < ilast; i++) {
				if (i + 1 < ilast && j + 1 < jlast) {
					hier::Index idx(i + boxfirstP(0), j + boxfirstP(1));
					Particles<Particle_particles>* part_particles = particleVariables_particles->getItem(idx);
					for (int pit = part_particles->getNumberOfParticles() - 1; pit >= 0; pit--) {
						Particle_particles* particle = part_particles->getParticle(pit);
						auxVarField2 = particle->field * particle->auxiliaryField;
						if (particle->region == 1 || particle->newRegion == 1) {
							particle->auxiliaryField = auxVarField2;
						}
					}
				}
			}
		}
	}
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch >& patch = *p_it;
		double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
	
		std::shared_ptr< pdat::IndexData<Particles<Particle_particles>, pdat::CellGeometry > > particleVariables_particles(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles<Particle_particles>, pdat::CellGeometry >, hier::PatchData>(patch->getPatchData(d_particleVariables_particles_id)));
		double derivative_normalization, interpolation_normalization, d_derAuxiliaryField_o0_t0_m0_l0;
	
		//Get the dimensions of the patch
		const hier::Index boxfirst = patch->getBox().lower();
		const hier::Index boxlast = patch->getBox().upper();
	
		const hier::Index boxfirstP = particleVariables_particles->getGhostBox().lower();
		const hier::Index boxlastP = particleVariables_particles->getGhostBox().upper();
	
		//Get delta spaces into an array. dx, dy, dz.
		std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
		const double* dx  = patch_geom->getDx();
	
		//Auxiliary definitions
		int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
		int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
		for(int j = 0; j < jlast; j++) {
			for(int i = 0; i < ilast; i++) {
				if (i + 1 < ilast && j + 1 < jlast) {
					hier::Index idx(i + boxfirstP(0), j + boxfirstP(1));
					Particles<Particle_particles>* part_particles = particleVariables_particles->getItem(idx);
					for (int pit = part_particles->getNumberOfParticles() - 1; pit >= 0; pit--) {
						Particle_particles* particle = part_particles->getParticle(pit);
						derivative_normalization = 0.0;
						interpolation_normalization = 0.0;
						d_derAuxiliaryField_o0_t0_m0_l0 = 0.0;
						miniIndex = MAX(0, MAX((int) floor((particle->predpositionx - 2 * level_influenceRadius - d_grid_geometry->getXLower()[0])/dx[0]) - boxfirst(0) + d_ghost_width, (int) i - ceil((2 * level_influenceRadius)/dx[0])));
						maxiIndex = MIN(boxlast(0) - boxfirst(0) + 2 * d_ghost_width, MIN((int) floor((particle->predpositionx + 2 * level_influenceRadius - d_grid_geometry->getXLower()[0])/dx[0]) - boxfirst(0) + d_ghost_width, (int) i + ceil((2 * level_influenceRadius)/dx[0])));
						minjIndex = MAX(0, MAX((int) floor((particle->predpositiony - 2 * level_influenceRadius - d_grid_geometry->getXLower()[1])/dx[1]) - boxfirst(1) + d_ghost_width, (int) j - ceil((2 * level_influenceRadius)/dx[1])));
						maxjIndex = MIN(boxlast(1) - boxfirst(1) + 2 * d_ghost_width, MIN((int) floor((particle->predpositiony + 2 * level_influenceRadius - d_grid_geometry->getXLower()[1])/dx[1]) - boxfirst(1) + d_ghost_width, (int) j + ceil((2 * level_influenceRadius)/dx[1])));
						for(int jb = minjIndex; jb <= maxjIndex; jb++) {
							for(int ib = miniIndex; ib <= maxiIndex; ib++) {
								hier::Index idxb(ib + boxfirstP(0), jb + boxfirstP(1));
								Particles<Particle_particles>* partb_particles = particleVariables_particles->getItem(idxb);
								for (int pitb = 0; pitb < partb_particles->getNumberOfParticles(); pitb++) {
									Particle_particles* particleb = partb_particles->getParticle(pitb);
									double particle_distance = particle->distancepred(particleb);
									if (particle_distance < 2 * level_influenceRadius) {
										derivative_normalization = derivative_normalization + SPHfirstderivativenormalization_(dimensions_main, 1.0 * volume_level_factor, particle_distance, gW_(particle_distance / level_influenceRadius, dx, simPlat_dt, ilast, jlast), dx, simPlat_dt, ilast, jlast);
										interpolation_normalization = interpolation_normalization + Normalization_(1.0 * volume_level_factor, W_(particle_distance / level_influenceRadius, dx, simPlat_dt, ilast, jlast), dx, simPlat_dt, ilast, jlast);
										d_derAuxiliaryField_o0_t0_m0_l0 = d_derAuxiliaryField_o0_t0_m0_l0 + SPHAsym_(particle->field, particleb->field, particle->predpositiony, particleb->predpositiony, 1.0 * volume_level_factor, particle_distance, dimensions_main, gW_(particle_distance / level_influenceRadius, dx, simPlat_dt, ilast, jlast), dx, simPlat_dt, ilast, jlast);
									}
								}				
							}
						}
						if (fabs(derivative_normalization) < 0.000000000000001) {
							derivative_normalization = 0.000000000000001;
						}
						if (fabs(interpolation_normalization) < 0.000000000000001) {
							interpolation_normalization = 0.000000000000001;
						}
						d_derAuxiliaryField_o0_t0_m0_l0 = d_derAuxiliaryField_o0_t0_m0_l0 / derivative_normalization;
						particle->m_derAuxiliaryField_o0_t0_l0_1 = particle->field * d_derAuxiliaryField_o0_t0_m0_l0;
					}
				}
			}
		}
	}
	//Fill ghosts and periodical boundaries
	d_bdry_sched_advance[ln]->fillData(current_time, false);
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch >& patch = *p_it;
		double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
	
		std::shared_ptr< pdat::IndexData<Particles<Particle_particles>, pdat::CellGeometry > > particleVariables_particles(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles<Particle_particles>, pdat::CellGeometry >, hier::PatchData>(patch->getPatchData(d_particleVariables_particles_id)));
		const hier::Index boxfirst = particleVariables_particles->getGhostBox().lower();
		const hier::Index boxlast = particleVariables_particles->getGhostBox().upper();
		for (int index1 = boxfirst(1); index1 <= boxlast(1); index1++) {
			for (int index0 = boxfirst(0); index0 <= boxlast(0); index0++) {
				hier::Index idx(index0, index1);
				//Correct the position if there is any periodical boundary
				Particles<Particle_particles>* part_particles = particleVariables_particles->getItem(idx);
				checkPosition(patch, index0, index1, part_particles);
			}
		}
	}
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch >& patch = *p_it;
		double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
	
		std::shared_ptr< pdat::IndexData<Particles<Particle_particles>, pdat::CellGeometry > > particleVariables_particles(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles<Particle_particles>, pdat::CellGeometry >, hier::PatchData>(patch->getPatchData(d_particleVariables_particles_id)));
		double derivative_normalization, interpolation_normalization, d_derAuxiliaryField_o0_t0_m0_l1, auxVarField2, d_derAuxiliaryField_o0_t1_m0_l0;
	
		//Get the dimensions of the patch
		const hier::Index boxfirst = patch->getBox().lower();
		const hier::Index boxlast = patch->getBox().upper();
	
		const hier::Index boxfirstP = particleVariables_particles->getGhostBox().lower();
		const hier::Index boxlastP = particleVariables_particles->getGhostBox().upper();
	
		//Get delta spaces into an array. dx, dy, dz.
		std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
		const double* dx  = patch_geom->getDx();
	
		//Auxiliary definitions
		int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
		int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
		for(int j = 0; j < jlast; j++) {
			for(int i = 0; i < ilast; i++) {
				if (i + 1 < ilast && j + 1 < jlast) {
					hier::Index idx(i + boxfirstP(0), j + boxfirstP(1));
					Particles<Particle_particles>* part_particles = particleVariables_particles->getItem(idx);
					for (int pit = part_particles->getNumberOfParticles() - 1; pit >= 0; pit--) {
						Particle_particles* particle = part_particles->getParticle(pit);
						derivative_normalization = 0.0;
						interpolation_normalization = 0.0;
						miniIndex = MAX(0, MAX((int) floor((particle->predpositionx - 2 * level_influenceRadius - d_grid_geometry->getXLower()[0])/dx[0]) - boxfirst(0) + d_ghost_width, (int) i - ceil((2 * level_influenceRadius)/dx[0])));
						maxiIndex = MIN(boxlast(0) - boxfirst(0) + 2 * d_ghost_width, MIN((int) floor((particle->predpositionx + 2 * level_influenceRadius - d_grid_geometry->getXLower()[0])/dx[0]) - boxfirst(0) + d_ghost_width, (int) i + ceil((2 * level_influenceRadius)/dx[0])));
						minjIndex = MAX(0, MAX((int) floor((particle->predpositiony - 2 * level_influenceRadius - d_grid_geometry->getXLower()[1])/dx[1]) - boxfirst(1) + d_ghost_width, (int) j - ceil((2 * level_influenceRadius)/dx[1])));
						maxjIndex = MIN(boxlast(1) - boxfirst(1) + 2 * d_ghost_width, MIN((int) floor((particle->predpositiony + 2 * level_influenceRadius - d_grid_geometry->getXLower()[1])/dx[1]) - boxfirst(1) + d_ghost_width, (int) j + ceil((2 * level_influenceRadius)/dx[1])));
						for(int jb = minjIndex; jb <= maxjIndex; jb++) {
							for(int ib = miniIndex; ib <= maxiIndex; ib++) {
								hier::Index idxb(ib + boxfirstP(0), jb + boxfirstP(1));
								Particles<Particle_particles>* partb_particles = particleVariables_particles->getItem(idxb);
								for (int pitb = 0; pitb < partb_particles->getNumberOfParticles(); pitb++) {
									Particle_particles* particleb = partb_particles->getParticle(pitb);
									double particle_distance = particle->distancepred(particleb);
									if (particle_distance < 2 * level_influenceRadius) {
										derivative_normalization = derivative_normalization + SPHfirstderivativenormalization_(dimensions_main, 1.0 * volume_level_factor, particle_distance, gW_(particle_distance / level_influenceRadius, dx, simPlat_dt, ilast, jlast), dx, simPlat_dt, ilast, jlast);
										interpolation_normalization = interpolation_normalization + Normalization_(1.0 * volume_level_factor, W_(particle_distance / level_influenceRadius, dx, simPlat_dt, ilast, jlast), dx, simPlat_dt, ilast, jlast);
										d_derAuxiliaryField_o0_t0_m0_l1 = d_derAuxiliaryField_o0_t0_m0_l1 + SPHAsym_(particle->m_derAuxiliaryField_o0_t0_l0_1, particleb->m_derAuxiliaryField_o0_t0_l0_1, particle->predpositionx, particleb->predpositionx, 1.0 * volume_level_factor, particle_distance, dimensions_main, gW_(particle_distance / level_influenceRadius, dx, simPlat_dt, ilast, jlast), dx, simPlat_dt, ilast, jlast);
									}
								}				
							}
						}
						auxVarField2 = particle->field * particle->auxiliaryField;
						d_derAuxiliaryField_o0_t1_m0_l0 = auxVarField2;
						if (fabs(derivative_normalization) < 0.000000000000001) {
							derivative_normalization = 0.000000000000001;
						}
						if (fabs(interpolation_normalization) < 0.000000000000001) {
							interpolation_normalization = 0.000000000000001;
						}
						d_derAuxiliaryField_o0_t0_m0_l1 = d_derAuxiliaryField_o0_t0_m0_l1 / derivative_normalization;
						if (particle->region == 1 || particle->newRegion == 1) {
							particle->derAuxiliaryField = d_derAuxiliaryField_o0_t0_m0_l1 + d_derAuxiliaryField_o0_t1_m0_l0;
						}
					}
				}
			}
		}
	}
	//Fill ghosts and periodical boundaries
	d_bdry_sched_advance[ln]->fillData(current_time, false);
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch >& patch = *p_it;
		double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
	
		std::shared_ptr< pdat::IndexData<Particles<Particle_particles>, pdat::CellGeometry > > particleVariables_particles(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles<Particle_particles>, pdat::CellGeometry >, hier::PatchData>(patch->getPatchData(d_particleVariables_particles_id)));
		const hier::Index boxfirst = particleVariables_particles->getGhostBox().lower();
		const hier::Index boxlast = particleVariables_particles->getGhostBox().upper();
		for (int index1 = boxfirst(1); index1 <= boxlast(1); index1++) {
			for (int index0 = boxfirst(0); index0 <= boxlast(0); index0++) {
				hier::Index idx(index0, index1);
				//Correct the position if there is any periodical boundary
				Particles<Particle_particles>* part_particles = particleVariables_particles->getItem(idx);
				checkPosition(patch, index0, index1, part_particles);
			}
		}
	}
	if (d_refinedTimeStepping) {
		if (!hierarchy->finerLevelExists(ln) && last_step) {
			int currentLevelNumber = ln;
			while (currentLevelNumber > 0 && current_iteration[currentLevelNumber] % hierarchy->getRatioToCoarserLevel(currentLevelNumber).max() == 0) {
				d_coarsen_schedule[currentLevelNumber]->coarsenData();
				d_bdry_sched_postCoarsen[currentLevelNumber - 1]->fillData(current_time, false);
				currentLevelNumber--;
			}
		}
	} else {
		if (ln > 0) {
			d_coarsen_schedule[ln]->coarsenData();
			d_bdry_sched_postCoarsen[ln - 1]->fillData(current_time, false);
		}
	}
	

	t_step->stop();


	//Output
	t_output->start();
	if (ln == hierarchy->getFinestLevelNumber() && viz_particle_dump_interval > 0) {
		if (previous_iteration < next_particle_dump_iteration && outputCycle >= next_particle_dump_iteration) {
			d_particle_data_writer->writePlotData(d_grid_geometry, hierarchy, outputCycle, new_time);
			next_particle_dump_iteration = next_particle_dump_iteration + viz_particle_dump_interval;
			while (outputCycle >= next_particle_dump_iteration) {
				next_particle_dump_iteration = next_particle_dump_iteration + viz_particle_dump_interval;
			}
		}
	}

	t_output->stop();

	if (mpi.getRank() == 0 && d_output_interval > 0 ) {
		if (previous_iteration < next_console_output && outputCycle >= next_console_output) {
			int currentLevelNumber = ln;
			while (currentLevelNumber > 0) {
				currentLevelNumber--;
				cout <<"  ";
			}

			cout << "Level "<<ln<<". Iteration " << current_iteration[ln]<<". Time "<<current_time<<"."<< endl;
			if (ln == hierarchy->getFinestLevelNumber()) {
				next_console_output = next_console_output + d_output_interval;
				while (outputCycle >= next_console_output) {
					next_console_output = next_console_output + d_output_interval;
				}
			
			}
		}
	}

	if (d_timer_output_interval > 0 ) {
		if (previous_iteration < next_timer_output && outputCycle >= next_timer_output) {
			if (ln == hierarchy->getFinestLevelNumber()) {
				//Print timers
				if (mpi.getRank() == 0) {
					tbox::TimerManager::getManager()->print(cout);
				}
				else {
					if (ln == hierarchy->getFinestLevelNumber()) {
						//Dispose other processor timers
						//SAMRAI needs all processors run tbox::TimerManager::getManager()->print, otherwise it hungs
						std::ofstream ofs;
						ofs.setstate(std::ios_base::badbit);
						tbox::TimerManager::getManager()->print(ofs);
					}
				}
				next_timer_output = next_timer_output + d_timer_output_interval;
				while (outputCycle >= next_timer_output) {
					next_timer_output = next_timer_output + d_timer_output_interval;
				}
			
			}
		}
	}

	return simPlat_dt;
}

/*
 * Checks the finalization conditions              
 */
bool Problem::checkFinalization(const double current_time, const double simPlat_dt)
{
	if (greaterEq(current_time, 1.0)) { 
		return true;
	}
	return false;
	
	

}

void Problem::putToRestart(MainRestartData& mrd) {
	mrd.setNextParticleDumpIteration(next_particle_dump_iteration);

	mrd.setCurrentIteration(current_iteration);
	mrd.setNextConsoleOutputIteration(next_console_output);
	mrd.setNextTimerOutputIteration(next_timer_output);
}

void Problem::getFromRestart(MainRestartData& mrd) {
	next_particle_dump_iteration = mrd.getNextParticleDumpIteration();

	current_iteration = mrd.getCurrentIteration();
	next_console_output = mrd.getNextConsoleOutputIteration();
	next_timer_output = mrd.getNextTimerOutputIteration();
}

void Problem::allocateAfterRestart() {
	for (int il = 0; il < d_patch_hierarchy->getNumberOfLevels(); il++) {
		std::shared_ptr< hier::PatchLevel > level(d_patch_hierarchy->getPatchLevel(il));
		level->allocatePatchData(d_interior_regridding_value_id);
		level->allocatePatchData(d_nonSync_regridding_tag_id);
		level->allocatePatchData(d_interior_i_id);
		level->allocatePatchData(d_interior_j_id);
		level->allocatePatchData(d_interior_regridding_value_id);
		level->allocatePatchData(d_nonSync_regridding_tag_id);
		level->allocatePatchData(d_nonSyncP_id);
		level->allocatePatchData(d_interior_i_id);
		level->allocatePatchData(d_interior_j_id);
	}

}

/*
 *  Cell tagging routine - tag cells that require refinement based on a provided condition. 
 */
void Problem::applyGradientDetector(
   const std::shared_ptr< hier::PatchHierarchy >& hierarchy, 
   const int level_number,
   const double time, 
   const int tag_index,
   const bool initial_time,
   const bool uses_richardson_extrapolation_too) 
{
	if (d_regridding && level_number + 1 >= d_regridding_min_level && level_number + 1 <= d_regridding_max_level) {
		hier::VariableDatabase *vdb = hier::VariableDatabase::getDatabase();
		if (!(vdb->checkVariableExists(d_regridding_field)) && std::find(particleVariableList_particles.begin(), particleVariableList_particles.end(), d_regridding_field) == particleVariableList_particles.end()) {
			TBOX_ERROR(d_object_name << ": Regridding field selected not found:n"					<<  d_regridding_field<<  "n");
		}

		std::shared_ptr< hier::PatchLevel > level(hierarchy->getPatchLevel(level_number));

		for (hier::PatchLevel::iterator ip(level->begin()); ip != level->end(); ++ip) {
			const std::shared_ptr< hier::Patch >& patch = *ip;
			int* tags = ((pdat::CellData<int> *) patch->getPatchData(tag_index).get())->getPointer();
			int* regridding_tag = ((pdat::NodeData<int> *) patch->getPatchData(d_nonSync_regridding_tag_id).get())->getPointer();
			const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
			const double* dx  = patch_geom->getDx();
			const hier::Index tfirst = patch->getPatchData(tag_index)->getGhostBox().lower();
			const hier::Index tlast  = patch->getPatchData(tag_index)->getGhostBox().upper();
			const hier::Index boxfirst = patch->getBox().lower();
			const hier::Index boxlast  = patch->getBox().upper();
			int ilast = boxlast(0)-boxfirst(0)+2+2*d_ghost_width;
			int itlast = tlast(0)-tfirst(0)+1;
			int jlast = boxlast(1)-boxfirst(1)+2+2*d_ghost_width;
			int jtlast = tlast(1)-tfirst(1)+1;
			for(int index1 = 0; index1 < (tlast(1)-tfirst(1))+1; index1++) {
				for(int index0 = 0; index0 < (tlast(0)-tfirst(0))+1; index0++) {
					vectorT(tags,index0, index1) = 0;
				}
			}
			for(int index1 = 0; index1 < jlast; index1++) {
				for(int index0 = 0; index0 < ilast; index0++) {
					vector(regridding_tag,index0, index1) = 0;
				}
			}
			if (vdb->checkVariableExists(d_regridding_field)) {
				//Mesh
				if (d_regridding_type == "GRADIENT") {
					int regrid_field_id = vdb->getVariable(d_regridding_field)->getInstanceIdentifier();
					double* regrid_field = ((pdat::NodeData<double> *) patch->getPatchData(regrid_field_id).get())->getPointer();
					double* regridding_value = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_regridding_value_id).get())->getPointer();
					for(int index1 = 0; index1 < jlast - 1; index1++) {
						for(int index0 = 0; index0 < ilast - 1; index0++) {
							if (vector(regrid_field, index0, index1)!=0) {
								if ((fabs(vector(regrid_field, index0+1+d_ghost_width, index1+d_ghost_width)-vector(regrid_field, index0+d_ghost_width, index1+d_ghost_width)) > d_regridding_compressionFactor * MIN(fabs(vector(regrid_field, index0+2+d_ghost_width, index1+d_ghost_width)-vector(regrid_field, index0+1+d_ghost_width, index1+d_ghost_width)) + d_regridding_mOffset * pow(dx[0], 2), fabs(vector(regrid_field, index0+d_ghost_width, index1+d_ghost_width)-vector(regrid_field, index0-1+d_ghost_width, index1+d_ghost_width)) + d_regridding_mOffset * pow(dx[0], 2)) ) ||(fabs(vector(regrid_field, index0+d_ghost_width, index1+1+d_ghost_width)-vector(regrid_field, index0+d_ghost_width, index1+d_ghost_width)) > d_regridding_compressionFactor * MIN(fabs(vector(regrid_field, index0+d_ghost_width, index1+2+d_ghost_width)-vector(regrid_field, index0+d_ghost_width, index1+1+d_ghost_width)) + d_regridding_mOffset * pow(dx[1], 2), fabs(vector(regrid_field, index0+d_ghost_width, index1+d_ghost_width)-vector(regrid_field, index0+d_ghost_width, index1-1+d_ghost_width)) + d_regridding_mOffset * pow(dx[1], 2)) ) ) {
									if (index0 >= d_ghost_width && index0 < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1 >= d_ghost_width && index1 < (tlast(1)-tfirst(1))+1 + d_ghost_width) {
										vectorT(tags,index0 - d_ghost_width, index1 - d_ghost_width) = 1;
									}
									vector(regridding_tag,index0, index1) = 1;
									//SAMRAI tagging
									if (index0 > d_ghost_width && index0 < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1 > d_ghost_width && index1 < (tlast(1)-tfirst(1))+1 + d_ghost_width) {
										vectorT(tags, index0-d_ghost_width - 1, index1-d_ghost_width - 1) = 1;
									}
									if (index0 > d_ghost_width && index0 < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1 >= d_ghost_width && index1 < (tlast(1)-tfirst(1))+1 + d_ghost_width) {
										vectorT(tags, index0-d_ghost_width - 1, index1-d_ghost_width) = 1;
									}
									if (index0 >= d_ghost_width && index0 < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1 > d_ghost_width && index1 < (tlast(1)-tfirst(1))+1 + d_ghost_width) {
										vectorT(tags, index0-d_ghost_width, index1-d_ghost_width - 1) = 1;
									}
									//Informative tagging
									if (index0 > 0 && index1 > 0) {
										if (vector(regridding_tag,index0-1,index1-1) != 1)
											vector(regridding_tag,index0-1,index1-1) = 1;
									}
									if (index0 > 0) {
										if (vector(regridding_tag,index0-1,index1) != 1)
											vector(regridding_tag,index0-1,index1) = 1;
									}
									if (index1 > 0) {
										if (vector(regridding_tag,index0,index1-1) != 1)
											vector(regridding_tag,index0,index1-1) = 1;
									}
									if (d_regridding_buffer > 0) {
										int distance;
										for(int index1b = MAX(0, index1 - d_regridding_buffer - 1); index1b < MIN(index1 + d_regridding_buffer + 1, jlast); index1b++) {
											for(int index0b = MAX(0, index0 - d_regridding_buffer - 1); index0b < MIN(index0 + d_regridding_buffer + 1, ilast); index0b++) {
												int distx = (index0b - index0);
												if (distx < 0) {
													distx++;
												}
												int disty = (index1b - index1);
												if (disty < 0) {
													disty++;
												}
												distance = 1 + MAX(abs(distx), abs(disty));
												if (index0b >= d_ghost_width && index0b < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1b >= d_ghost_width && index1b < (tlast(1)-tfirst(1))+1 + d_ghost_width) {
													vectorT(tags,index0b-d_ghost_width,index1b-d_ghost_width) = 1;
												}
												if (vector(regridding_tag,index0b,index1b) == 0 || vector(regridding_tag,index0b,index1b) > distance) {
													vector(regridding_tag,index0b,index1b) = distance;
												}
											}
										}
									}
			
								}
							}
						}
					}
		
				} else {
					if (d_regridding_type == "FUNCTION") {
						int regrid_field_id = vdb->getVariable(d_regridding_field)->getInstanceIdentifier();
						double* regrid_field = ((pdat::NodeData<double> *) patch->getPatchData(regrid_field_id).get())->getPointer();
						double* regridding_value = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_regridding_value_id).get())->getPointer();
						for(int index1 = 0; index1 < jlast; index1++) {
							for(int index0 = 0; index0 < ilast; index0++) {
								vector(regridding_value, index0, index1) = vector(regrid_field, index0, index1);
								if (vector(regrid_field, index0, index1) > d_regridding_threshold) {
									if (index0 >= d_ghost_width && index0 < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1 >= d_ghost_width && index1 < (tlast(1)-tfirst(1))+1 + d_ghost_width) {
										vectorT(tags,index0 - d_ghost_width, index1 - d_ghost_width) = 1;
									}
									vector(regridding_tag,index0, index1) = 1;
									//SAMRAI tagging
									if (index0 > d_ghost_width && index0 < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1 > d_ghost_width && index1 < (tlast(1)-tfirst(1))+1 + d_ghost_width) {
										vectorT(tags, index0-d_ghost_width - 1, index1-d_ghost_width - 1) = 1;
									}
									if (index0 > d_ghost_width && index0 < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1 >= d_ghost_width && index1 < (tlast(1)-tfirst(1))+1 + d_ghost_width) {
										vectorT(tags, index0-d_ghost_width - 1, index1-d_ghost_width) = 1;
									}
									if (index0 >= d_ghost_width && index0 < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1 > d_ghost_width && index1 < (tlast(1)-tfirst(1))+1 + d_ghost_width) {
										vectorT(tags, index0-d_ghost_width, index1-d_ghost_width - 1) = 1;
									}
									//Informative tagging
									if (index0 > 0 && index1 > 0) {
										if (vector(regridding_tag,index0-1,index1-1) != 1)
											vector(regridding_tag,index0-1,index1-1) = 1;
									}
									if (index0 > 0) {
										if (vector(regridding_tag,index0-1,index1) != 1)
											vector(regridding_tag,index0-1,index1) = 1;
									}
									if (index1 > 0) {
										if (vector(regridding_tag,index0,index1-1) != 1)
											vector(regridding_tag,index0,index1-1) = 1;
									}
									if (d_regridding_buffer > 0) {
										int distance;
										for(int index1b = MAX(0, index1 - d_regridding_buffer - 1); index1b < MIN(index1 + d_regridding_buffer + 1, jlast); index1b++) {
											for(int index0b = MAX(0, index0 - d_regridding_buffer - 1); index0b < MIN(index0 + d_regridding_buffer + 1, ilast); index0b++) {
												int distx = (index0b - index0);
												if (distx < 0) {
													distx++;
												}
												int disty = (index1b - index1);
												if (disty < 0) {
													disty++;
												}
												distance = 1 + MAX(abs(distx), abs(disty));
												if (index0b >= d_ghost_width && index0b < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1b >= d_ghost_width && index1b < (tlast(1)-tfirst(1))+1 + d_ghost_width) {
													vectorT(tags,index0b-d_ghost_width,index1b-d_ghost_width) = 1;
												}
												if (vector(regridding_tag,index0b,index1b) == 0 || vector(regridding_tag,index0b,index1b) > distance) {
													vector(regridding_tag,index0b,index1b) = distance;
												}
											}
										}
									}
								}
							}
						}
			
					} else {
						if (d_regridding_type == "SHADOW") {
							if (!initial_time) {
								if (!(vdb->checkVariableExists(d_regridding_field_shadow))) {
									TBOX_ERROR(d_object_name << ": Regridding field selected not found:" <<  d_regridding_field_shadow<<  "");
								}
								int regrid_field_id = vdb->getVariable(d_regridding_field)->getInstanceIdentifier();
								double* regrid_field1 = ((pdat::NodeData<double> *) patch->getPatchData(regrid_field_id).get())->getPointer();
								int regrid_field_shadow_id = vdb->getVariable(d_regridding_field_shadow)->getInstanceIdentifier();
								double* regrid_field2 = ((pdat::NodeData<double> *) patch->getPatchData(regrid_field_shadow_id).get())->getPointer();
								double* regridding_value = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_regridding_value_id).get())->getPointer();
								for(int index1 = 0; index1 < jlast; index1++) {
									for(int index0 = 0; index0 < ilast; index0++) {
					
										double error = 2 * fabs(vector(regrid_field1, index0, index1) - vector(regrid_field2, index0, index1))/fabs(vector(regrid_field1, index0, index1) + vector(regrid_field2, index0, index1));
										vector(regridding_value, index0, index1) = error;
										if (error > d_regridding_error) {
											if (index0 >= d_ghost_width && index0 < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1 >= d_ghost_width && index1 < (tlast(1)-tfirst(1))+1 + d_ghost_width) {
												vectorT(tags,index0 - d_ghost_width, index1 - d_ghost_width) = 1;
											}
											vector(regridding_tag,index0, index1) = 1;
											//SAMRAI tagging
											if (index0 > d_ghost_width && index0 < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1 > d_ghost_width && index1 < (tlast(1)-tfirst(1))+1 + d_ghost_width) {
												vectorT(tags, index0-d_ghost_width - 1, index1-d_ghost_width - 1) = 1;
											}
											if (index0 > d_ghost_width && index0 < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1 >= d_ghost_width && index1 < (tlast(1)-tfirst(1))+1 + d_ghost_width) {
												vectorT(tags, index0-d_ghost_width - 1, index1-d_ghost_width) = 1;
											}
											if (index0 >= d_ghost_width && index0 < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1 > d_ghost_width && index1 < (tlast(1)-tfirst(1))+1 + d_ghost_width) {
												vectorT(tags, index0-d_ghost_width, index1-d_ghost_width - 1) = 1;
											}
											//Informative tagging
											if (index0 > 0 && index1 > 0) {
												if (vector(regridding_tag,index0-1,index1-1) != 1)
													vector(regridding_tag,index0-1,index1-1) = 1;
											}
											if (index0 > 0) {
												if (vector(regridding_tag,index0-1,index1) != 1)
													vector(regridding_tag,index0-1,index1) = 1;
											}
											if (index1 > 0) {
												if (vector(regridding_tag,index0,index1-1) != 1)
													vector(regridding_tag,index0,index1-1) = 1;
											}
											if (d_regridding_buffer > 0) {
												int distance;
												for(int index1b = MAX(0, index1 - d_regridding_buffer - 1); index1b < MIN(index1 + d_regridding_buffer + 1, jlast); index1b++) {
													for(int index0b = MAX(0, index0 - d_regridding_buffer - 1); index0b < MIN(index0 + d_regridding_buffer + 1, ilast); index0b++) {
														int distx = (index0b - index0);
														if (distx < 0) {
															distx++;
														}
														int disty = (index1b - index1);
														if (disty < 0) {
															disty++;
														}
														distance = 1 + MAX(abs(distx), abs(disty));
														if (index0b >= d_ghost_width && index0b < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1b >= d_ghost_width && index1b < (tlast(1)-tfirst(1))+1 + d_ghost_width) {
															vectorT(tags,index0b-d_ghost_width,index1b-d_ghost_width) = 1;
														}
														if (vector(regridding_tag,index0b,index1b) == 0 || vector(regridding_tag,index0b,index1b) > distance) {
															vector(regridding_tag,index0b,index1b) = distance;
														}
													}
												}
											}
					
										}
									}
								}
					
							}
				
						}
					}
				}
			}
			if (std::find(particleVariableList_particles.begin(), particleVariableList_particles.end(), d_regridding_field) != particleVariableList_particles.end()) {
				//Particle particles
				if (d_regridding_type == "GRADIENT") {
						TBOX_ERROR(d_object_name << ": GRADIENT tagging not valid for particles, use FUNCTION or SHADOW instead:");
		
				} else {
					if (d_regridding_type == "FUNCTION") {
						std::shared_ptr< pdat::IndexData<Particles<Particle_particles>, pdat::CellGeometry > > particleVariables_particles(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles<Particle_particles>, pdat::CellGeometry >, hier::PatchData>(patch->getPatchData(d_particleVariables_particles_id)));
						double* regridding_value = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_regridding_value_id).get())->getPointer();
			
						const hier::Index boxfirstP = particleVariables_particles->getGhostBox().lower();
						const hier::Index boxlastP = particleVariables_particles->getGhostBox().upper();
			
						for(int index1 = 0; index1 < jlast - 1; index1++) {
							for(int index0 = 0; index0 < ilast - 1; index0++) {
								hier::Index idx(index0 + boxfirstP(0), index1 + boxfirstP(1));
								Particles<Particle_particles>* part_particles = particleVariables_particles->getItem(idx);
								for (int pit = part_particles->getNumberOfParticles() - 1; pit >= 0; pit--) {
									Particle_particles* particle = part_particles->getParticle(pit);
									vector(regridding_value, index0, index1) = particle->getFieldValue(d_regridding_field);
									if (vector(regridding_value, index0, index1) > d_regridding_threshold) {
										if (index0 >= d_ghost_width && index0 < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1 >= d_ghost_width && index1 < (tlast(1)-tfirst(1))+1 + d_ghost_width) {
											vectorT(tags,index0 - d_ghost_width, index1 - d_ghost_width) = 1;
										}
										vector(regridding_tag,index0, index1) = 1;
										if (d_regridding_buffer > 0) {
											int distance;
											for(int index1b = MAX(0, index1 - d_regridding_buffer); index1b < MIN(index1 + d_regridding_buffer + 1, jlast); index1b++) {
												for(int index0b = MAX(0, index0 - d_regridding_buffer); index0b < MIN(index0 + d_regridding_buffer + 1, ilast); index0b++) {
													int distx = (index0b - index0);
													int disty = (index1b - index1);
													distance = 1 + MAX(abs(distx), abs(disty));
													if (index0b >= d_ghost_width && index0b < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1b >= d_ghost_width && index1b < (tlast(1)-tfirst(1))+1 + d_ghost_width) {
														vectorT(tags,index0b-d_ghost_width,index1b-d_ghost_width) = 1;
													}
													if (vector(regridding_tag,index0b,index1b) == 0 || vector(regridding_tag,index0b,index1b) > distance) {
														vector(regridding_tag,index0b,index1b) = distance;
													}
												}
											}
										}
									}
								}
							}
						}
			
					} else {
						if (d_regridding_type == "SHADOW") {
							if (std::find(particleVariableList_particles.begin(), particleVariableList_particles.end(), d_regridding_field_shadow) == particleVariableList_particles.end()) {
								TBOX_ERROR(d_object_name << ": Regridding field selected not found:" <<  d_regridding_field_shadow<<  "");
							}
							std::shared_ptr< pdat::IndexData<Particles<Particle_particles>, pdat::CellGeometry > > particleVariables_particles(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles<Particle_particles>, pdat::CellGeometry >, hier::PatchData>(patch->getPatchData(d_particleVariables_particles_id)));
							double* regridding_value = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_regridding_value_id).get())->getPointer();
							const hier::Index boxfirstP = particleVariables_particles->getGhostBox().lower();
							const hier::Index boxlastP = particleVariables_particles->getGhostBox().upper();
				
							for(int index1 = 0; index1 < jlast - 1; index1++) {
								for(int index0 = 0; index0 < ilast - 1; index0++) {
									hier::Index idx(index0 + boxfirstP(0), index1 + boxfirstP(1));
									Particles<Particle_particles>* part_particles = particleVariables_particles->getItem(idx);
									for (int pit = part_particles->getNumberOfParticles() - 1; pit >= 0; pit--) {
										Particle_particles* particle = part_particles->getParticle(pit);
										double error = fabs(particle->getFieldValue(d_regridding_field) - particle->getFieldValue(d_regridding_field_shadow));
										vector(regridding_value, index0, index1) = error;
										if (error > d_regridding_error) {
											if (index0 >= d_ghost_width && index0 < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1 >= d_ghost_width && index1 < (tlast(1)-tfirst(1))+1 + d_ghost_width) {
												vectorT(tags,index0 - d_ghost_width, index1 - d_ghost_width) = 1;
											}
											vector(regridding_tag,index0, index1) = 1;
											if (d_regridding_buffer > 0) {
												int distance;
												for(int index1b = MAX(0, index1 - d_regridding_buffer); index1b < MIN(index1 + d_regridding_buffer + 1, jlast); index1b++) {
													for(int index0b = MAX(0, index0 - d_regridding_buffer); index0b < MIN(index0 + d_regridding_buffer + 1, ilast); index0b++) {
														int distx = (index0b - index0);
														int disty = (index1b - index1);
														distance = 1 + MAX(abs(distx), abs(disty));
														if (index0b >= d_ghost_width && index0b < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1b >= d_ghost_width && index1b < (tlast(1)-tfirst(1))+1 + d_ghost_width) {
															vectorT(tags,index0b-d_ghost_width,index1b-d_ghost_width) = 1;
														}
														if (vector(regridding_tag,index0b,index1b) == 0 || vector(regridding_tag,index0b,index1b) > distance) {
															vector(regridding_tag,index0b,index1b) = distance;
														}
													}
												}
											}
										}
									}
								}
							}
				
						}
					}
				}
			}
		}
		//Buffer synchronization if needed
		if (d_regridding_buffer > d_ghost_width) {
			d_tagging_fill->createSchedule(level)->fillData(0, false);
		}
		if (d_regridding_buffer > 0) {
			for (hier::PatchLevel::iterator ip(level->begin()); ip != level->end(); ++ip) {
				const std::shared_ptr< hier::Patch >& patch = *ip;
				int* tags = ((pdat::CellData<int> *) patch->getPatchData(tag_index).get())->getPointer();
				int* regridding_tag = ((pdat::NodeData<int> *) patch->getPatchData(d_nonSync_regridding_tag_id).get())->getPointer();
				const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
				const double* dx  = patch_geom->getDx();
				const hier::Index tfirst = patch->getPatchData(tag_index)->getGhostBox().lower();
				const hier::Index tlast  = patch->getPatchData(tag_index)->getGhostBox().upper();
				const hier::Index boxfirst = patch->getBox().lower();
				const hier::Index boxlast  = patch->getBox().upper();
				int ilast = boxlast(0)-boxfirst(0)+2+2*d_ghost_width;
				int itlast = tlast(0)-tfirst(0)+1;
				int jlast = boxlast(1)-boxfirst(1)+2+2*d_ghost_width;
				int jtlast = tlast(1)-tfirst(1)+1;
	
				for(int index1 = 0; index1 < jlast; index1++) {
					for(int index0 = 0; index0 < ilast; index0++) {
	
						int value = vector(regridding_tag, index0, index1);
						if (value > 0 && value < 1 + d_regridding_buffer) {
							int buffer_left = 1 + d_regridding_buffer - value;
							int distance;
							for(int index1b = MAX(0, index1 - buffer_left); index1b < MIN(index1 + buffer_left + 1, jlast); index1b++) {
								for(int index0b = MAX(0, index0 - buffer_left); index0b < MIN(index0 + buffer_left + 1, ilast); index0b++) {
							
									distance = MAX(abs(index0b - index0), abs(index1b - index1));
									if (distance > 0 && index0b >= d_ghost_width && index0b < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1b >= d_ghost_width && index1b < (tlast(1)-tfirst(1))+1 + d_ghost_width) {
										vectorT(tags, index0b - d_ghost_width, index1b - d_ghost_width) = 1;
									}
									if (distance > 0 && (vector(regridding_tag,index0b, index1b) == 0 || vector(regridding_tag, index0b, index1b) > value + distance)) {
										vector(regridding_tag, index0b, index1b) = value + distance;
									}
								}
							}
							
						}
					}
				}
	
			}
		}
	}
}

/*
 * Move the particles if the problem have any
 */
template<class T>
void Problem::moveParticles(const std::shared_ptr<hier::Patch > patch, std::shared_ptr< pdat::IndexData<Particles<T>, pdat::CellGeometry > > particleVariables, Particles<T>* part, T* particle, const int index0, const int index1, const double newPosi, const double newPosj, const double simPlat_dt, const double current_time, int pit, int step, double level_influenceRadius)
{
	double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
	double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
	double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
	double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
	double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
	const hier::Index boxfirst1 = patch->getBox().lower();
	const hier::Index boxlast1  = patch->getBox().upper();
	const hier::Index boxfirst = particleVariables->getGhostBox().lower();
	const hier::Index boxlast  = particleVariables->getGhostBox().upper();

	//Get delta spaces into an array. dx, dy, dz.
	std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
	const double* dx  = patch_geom->getDx();
	int xlower = patch_geom->getXLower()[0];
	int xupper = patch_geom->getXUpper()[0];
	int ylower = patch_geom->getXLower()[1];
	int yupper = patch_geom->getXUpper()[1];
	int ilast = boxlast1(0)-boxfirst1(0) + 2 + 2 * d_ghost_width;
	int jlast = boxlast1(1)-boxfirst1(1) + 2 + 2 * d_ghost_width;
	double newPosition[2];

	hier::Index idx(index0, index1);
	newPosition[0] = (newPosi - xGlower)/dx[0];
	int newIndex0 = floor(newPosition[0] + 1E-10);
	newPosition[1] = (newPosj - yGlower)/dx[1];
	int newIndex1 = floor(newPosition[1] + 1E-10);

	bool out = false;
	if (newIndex0 > boxlast(0) || newIndex0 < boxfirst(0)) {
		out = true;
	}
	if (newIndex1 > boxlast(1) || newIndex1 < boxfirst(1)) {
		out = true;
	}
	if (!out) {
		hier::Index newIdx(newIndex0, newIndex1);
		if (!(newIdx == idx)) {
			bool deleted = false;
			if (!deleted) {
				//Need to create copy of the particle. Erase an item from a vector calls the destructor of the object
				T* newParticle = new T(*particle);
				part->deleteParticle(*particle);
				Particles<T>* destPart = particleVariables->getItem(newIdx);
				destPart->addParticle(*newParticle);
				delete newParticle;
			}
		}
	} else {
		part->deleteParticle(*particle);
	}
}
template<class T>
void Problem::checkPosition(const std::shared_ptr<hier::Patch >& patch, int index0, int index1, Particles<T>* particles)
{
	const hier::Index boxfirst1 = patch->getBox().lower();
	const hier::Index boxlast1  = patch->getBox().upper();

	std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));

	double xlower = patch_geom->getXLower()[0];
	double xupper = patch_geom->getXUpper()[0];
	bool periodicL0 = greaterEq(xGlower, xlower) && !patch_geom->getTouchesRegularBoundary(0, 0);
	bool periodicU0 = lessEq(xGupper, xupper) && !patch_geom->getTouchesRegularBoundary(0, 1);
	double ylower = patch_geom->getXLower()[1];
	double yupper = patch_geom->getXUpper()[1];
	bool periodicL1 = greaterEq(yGlower, ylower) && !patch_geom->getTouchesRegularBoundary(1, 0);
	bool periodicU1 = lessEq(yGupper, yupper) && !patch_geom->getTouchesRegularBoundary(1, 1);
	if (index0 < boxfirst1[0] || index0 > boxlast1[0] || index1 < boxfirst1[1] || index1 > boxlast1[1] ) {
		for(int pit = 0; pit < particles->getNumberOfParticles(); pit++) {
			T* particle = particles->getParticle(pit);
			double positionx = particle->positionx;
			double positionx_p = particle->positionx_p;
			double positiony = particle->positiony;
			double positiony_p = particle->positiony_p;
			double predpositionx = particle->predpositionx;
			double predpositiony = particle->predpositiony;
			if (periodicL0 && index0 < boxfirst1[0] && positionx > xGlower) {
				positionx = positionx - (xGupper - xGlower);
			}
			if (periodicU0 && index0 > boxlast1[0] && positionx < xGupper) {
				positionx = positionx + (xGupper - xGlower);
			}
			if (periodicL0 && index0 < boxfirst1[0] && positionx_p > xGlower) {
				positionx_p = positionx_p - (xGupper - xGlower);
			}
			if (periodicU0 && index0 > boxlast1[0] && positionx_p < xGupper) {
				positionx_p = positionx_p + (xGupper - xGlower);
			}
			if (periodicL1 && index1 < boxfirst1[1] && positiony > yGlower) {
				positiony = positiony - (yGupper - yGlower);
			}
			if (periodicU1 && index1 > boxlast1[1] && positiony < yGupper) {
				positiony = positiony + (yGupper - yGlower);
			}
			if (periodicL1 && index1 < boxfirst1[1] && positiony_p > yGlower) {
				positiony_p = positiony_p - (yGupper - yGlower);
			}
			if (periodicU1 && index1 > boxlast1[1] && positiony_p < yGupper) {
				positiony_p = positiony_p + (yGupper - yGlower);
			}
			if (periodicL0 && index0 < boxfirst1[0] && predpositionx > xGlower) {
				predpositionx = predpositionx - (xGupper - xGlower);
			}
			if (periodicU0 && index0 > boxlast1[0] && predpositionx < xGupper) {
				predpositionx = predpositionx + (xGupper - xGlower);
			}
			if (periodicL1 && index1 < boxfirst1[1] && predpositiony > yGlower) {
				predpositiony = predpositiony - (yGupper - yGlower);
			}
			if (periodicU1 && index1 > boxlast1[1] && predpositiony < yGupper) {
				predpositiony = predpositiony + (yGupper - yGlower);
			}
			particle->positionx = positionx;
			particle->positionx_p = positionx_p;
			particle->positiony = positiony;
			particle->positiony_p = positiony_p;
			particle->predpositionx = predpositionx;
			particle->predpositiony = predpositiony;
		}
	}
}


/*
 * Initialization of the common variables in case of restarting.
 */
void Problem::initCommonVars(const std::shared_ptr<hier::PatchHierarchy >& hierarchy)
{
	xGlower = d_grid_geometry->getXLower()[0];
	xGupper = d_grid_geometry->getXUpper()[0];
	ilastG = d_grid_geometry->getPhysicalDomain().front().numberCells()[0] + 2 * d_ghost_width;
	yGlower = d_grid_geometry->getXLower()[1];
	yGupper = d_grid_geometry->getXUpper()[1];
	jlastG = d_grid_geometry->getPhysicalDomain().front().numberCells()[1] + 2 * d_ghost_width;
}


