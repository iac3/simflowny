#ifndef BOOST_GRAPH_PLOD_GENERATOR_HPP
#define BOOST_GRAPH_PLOD_GENERATOR_HPP

#include <iterator>
#include <utility>
#include <gsl/gsl_rng.h>
#include <boost/shared_ptr.hpp>
#include <boost/graph/graph_traits.hpp>
#include <vector>
#include <map>
#include <boost/config/no_tr1/cmath.hpp>
#include <boost/mpl/if.hpp>


double harmonic_number(double alpha, int n) {
	double harmonic = 0;
	for (int i = 1; i <= n; i++) {
		harmonic += pow(i, -alpha);
	}
	return harmonic;
}

namespace boost {
  class out_directed_plod_iterator 
  {
  public:
    typedef std::forward_iterator_tag            iterator_category;
    typedef std::pair<std::size_t, std::size_t>  value_type;
    typedef const value_type&                    reference;
    typedef const value_type*                    pointer;
    typedef std::ptrdiff_t                       difference_type;

    out_directed_plod_iterator() : at_end(true) { }

    out_directed_plod_iterator(std::size_t n,  
                               double alpha, int seed, 
                               bool allow_self_loops)
      : n(n), alpha(alpha), 
        allow_self_loops(allow_self_loops), at_end(false), degree(0),
        current(0, 0)
    {
	const gsl_rng_type * T;
      	gsl_rng_env_setup();
      	r = gsl_rng_alloc(gsl_rng_mt19937); 
      	gsl_rng_set(r, seed);

      	harmonic = harmonic_number(alpha, n);
		double limit = 0;
		double rand_num = harmonic * gsl_rng_uniform(r);
		for (degree = 1; degree < n; degree++) {
			    limit = limit + pow(degree, -alpha);
			    if ( rand_num < limit ) {
			            break;
			    }
		}
    }

    reference operator*() const { return current; }
    pointer operator->() const { return &current; }

    out_directed_plod_iterator& operator++()
    { 
      using std::pow;
	bool saltaSource = true;

      // Continue stepping through source nodes until the
      // (out)degree is > 0
      while (saltaSource) {
		saltaSource = false;
	      while (degree == 0) {
		added.clear();
		// Step to the next source node. If we've gone past the
		// number of nodes we're responsible for, we're done.
		if (++current.first >= n) {
		  at_end = true;
		  return *this;
		}
	
		double limit = 0;
		double rand_num = harmonic * gsl_rng_uniform(r);
		for (degree = 1; degree < n; degree++) {
			    limit = limit + pow(degree, -alpha);
			    if ( rand_num < limit ) {
				    break;
			    }
		} 
	      }
	      do {
	
		current.second = gsl_rng_uniform_int(r, n);
		while (added.find(current.second) != added.end()) {
		      --degree;		
			current.second = gsl_rng_uniform_int(r, n);
			if (degree == 0) {
		    		saltaSource = true;
				break;
			}
		}
		if (saltaSource) break;
	      } while (current.first == current.second && !allow_self_loops);
	}
	added.insert(current.second);
      --degree;


      return *this;
    }

    out_directed_plod_iterator operator++(int)
    {
      out_directed_plod_iterator temp(*this);
      ++(*this);
      return temp;
    }

    bool operator==(const out_directed_plod_iterator& other) const
    { 
      return at_end == other.at_end; 
    }

    bool operator!=(const out_directed_plod_iterator& other) const
    { 
      return !(*this == other); 
    }

  private:
    std::size_t n;
    double alpha;
    double beta;
    bool allow_self_loops;
    bool at_end;
    std::size_t degree;
    double harmonic;
    gsl_rng *r;
    value_type current;
    std::set<int> added;
  };

  class undirected_plod_iterator 
  {
    typedef std::vector<std::pair<std::size_t, std::size_t> > out_degrees_t;

  public:
    typedef std::input_iterator_tag              iterator_category;
    typedef std::pair<std::size_t, std::size_t>  value_type;
    typedef const value_type&                    reference;
    typedef const value_type*                    pointer;
    typedef std::ptrdiff_t                       difference_type;

	struct edge_mine
	{
	  int source;
	  int target;

	 edge_mine(int a, int b) {
		source = a;
		target = b;
	}

	  bool operator<(const edge_mine& rhs) const
	  {
	    return source < rhs.source || (source == rhs.source && target < rhs.target);
	  }

	  bool operator==(const edge_mine& rhs) const
	  {
	    return (source == rhs.source && target == rhs.target) || (source == rhs.target && target == rhs.source);
	  }
	};

    undirected_plod_iterator() 
      : out_degrees(), degrees_left(0), allow_self_loops(false) { }

    undirected_plod_iterator( std::size_t n,  
                             double alpha, int seed, 
                             bool allow_self_loops = false)
      : n(n), out_degrees(new out_degrees_t),
        degrees_left(0), allow_self_loops(allow_self_loops)
    {
      using std::pow;

	const gsl_rng_type * T;
      	gsl_rng_env_setup();
      	r = gsl_rng_alloc(gsl_rng_mt19937); 
      	gsl_rng_set(r, seed);

      	double harmonic = harmonic_number(alpha, n);
      	for (std::size_t i = 0; i != n; ++i) {
		double limit = 0;
		double rand_num = harmonic * gsl_rng_uniform(r);
		std::size_t degree;
		for (degree = 1; degree < n; degree++) {
		        limit = limit + pow(degree, -alpha);
		        if ( rand_num < limit ) {
		                break;
		        }
		}

        	out_degrees->push_back(std::make_pair(i, degree));
        	degrees_left += degree;
      	}

      	next();
    }

    reference operator*() const { return current; }
    pointer operator->() const { return &current; }

    undirected_plod_iterator& operator++()
    { 
      next();
      return *this;
    }

    undirected_plod_iterator operator++(int)
    {
      undirected_plod_iterator temp(*this);
      ++(*this);
      return temp;
    }

    bool operator==(const undirected_plod_iterator& other) const
    { 
      return degrees_left == other.degrees_left; 
    }

    bool operator!=(const undirected_plod_iterator& other) const
    { return !(*this == other); }

  private:
    void next()
    {
      std::size_t source, target;
      while (true) {
        /* We may get to the point where we can't actually find any
           new edges, so we just add some random edge and set the
           degrees left = 0 to signal termination. */
        if (out_degrees->size() < 2) {
          current.first  = gsl_rng_uniform_int(r, n);
          do {
            current.second = gsl_rng_uniform_int(r, n);
          } while (current.first == current.second && !allow_self_loops);
          degrees_left = 0;
          out_degrees->clear();
          return;
        }

        // Select source vertex
        source = gsl_rng_uniform_int(r, out_degrees->size());
        if ((*out_degrees)[source].second == 0) {
          (*out_degrees)[source] = out_degrees->back();
          out_degrees->pop_back();
          continue;
        } 

        // Select target vertex
        target = gsl_rng_uniform_int(r, out_degrees->size());
        if ((*out_degrees)[target].second == 0) {
          (*out_degrees)[target] = out_degrees->back();
          out_degrees->pop_back();
          continue;
        } else if (source != target 
                   || (allow_self_loops && (*out_degrees)[source].second > 2)) {

		bool b1 = added.find(edge_mine((*out_degrees)[source].first, (*out_degrees)[target].first)) != added.end();
		bool b2 = added.find(edge_mine((*out_degrees)[target].first, (*out_degrees)[source].first)) != added.end();
		if (b1 || b2) {
		      --(*out_degrees)[source].second;
		      --degrees_left;
		      --(*out_degrees)[target].second;
		      --degrees_left;
		}
		else {
		          break;
		}
        }
      }
	added.insert(edge_mine((*out_degrees)[source].first, (*out_degrees)[target].first));
      // Update degree counts
      --(*out_degrees)[source].second;
      --degrees_left;
      --(*out_degrees)[target].second;
      --degrees_left;
      current.first  = (*out_degrees)[source].first;
      current.second = (*out_degrees)[target].first;
    }

    std::size_t n;
    shared_ptr<out_degrees_t> out_degrees;
    std::size_t degrees_left;
    bool allow_self_loops;
    value_type current;
    std::set<edge_mine> added;
    gsl_rng *r;
  
  };


  template<typename Graph>
  class plod_iterator
    : public mpl::if_<is_convertible<
                        typename graph_traits<Graph>::directed_category,
                        directed_tag>,
                      out_directed_plod_iterator,
                      undirected_plod_iterator >::type
  {
    typedef typename mpl::if_<
                       is_convertible<
                         typename graph_traits<Graph>::directed_category,
                         directed_tag>,
                        out_directed_plod_iterator,
                        undirected_plod_iterator >::type
      inherited;

  public:
    plod_iterator() : inherited() { }

    plod_iterator(std::size_t n,  
                  double alpha, int seed, bool allow_self_loops = false)
      : inherited(n, alpha, seed, allow_self_loops) { }
  };

} // end namespace boost



#endif
