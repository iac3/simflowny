/*@@
  @file      MappingSegments.F
  @date      Fri May 31 13:23:17 CEST 2013
  @author    4
  @desc
             Mapping segments for HeatProblem2D
  @enddesc
  @version 4
@@*/

define(leF,((abs(($1) - ($2))/FPC_epsilon_Cactus .lt. 10 .and. FLOOR(abs(($1) - ($2))/FPC_epsilon_Cactus) .lt. 1) .or. ($1) < ($2)))dnl
define(geF,((abs(($1) - ($2))/FPC_epsilon_Cactus .lt. 10 .and. FLOOR(abs(($1) - ($2))/FPC_epsilon_Cactus) .lt. 1) .or. ($2) < ($1)))dnl

#include "cctk.h"
#include "cctk_Parameters.h"
#include "cctk_Arguments.h"
#include "cctk_Functions.h"

	subroutine HeatProblem2D_mapping(CCTK_ARGUMENTS)

		implicit none

		DECLARE_CCTK_ARGUMENTS
		DECLARE_CCTK_PARAMETERS
		DECLARE_CCTK_FUNCTIONS

		CCTK_REAL FPC_epsilon_Cactus

!		Declare local variables
!		-----------------

		CCTK_INT i, iMapStart, iMapEnd, iterm, previousMapi, iWallAcc, iu, il, indexi
		logical interiorMapi
		CCTK_INT j, jMapStart, jMapEnd, jterm, previousMapj, jWallAcc, ju, jl, indexj
		logical interiorMapj
		CCTK_INT minBlock(2), maxBlock(2), unionsI, facePointI, status, ie1, ie2, ie3, proc, reduction_handle
		CCTK_REAL maxDistance, e1, e2, e3, SQRT3INV
		logical done, modif, checkStencil, s1, s2, s3, s4, checkStalled
!		Mapping variables
		logical workingGlobal, finishedGlobal, cleanStencil
		CCTK_INT working, finished, nodes, tmp1, tmp2, pred, first
		CCTK_INT, allocatable :: workingArray(:), finishedArray(:)
!		Deltas
		CCTK_REAL deltax
		CCTK_REAL deltay
		CCTK_REAL deltan

		deltax = (3.1415926535897932384626433832795028841971693993751d+0 - (0.0d0))/(cctk_gsh(1) - 1)
		deltay = (3.1415926535897932384626433832795028841971693993751d+0 - (0.0d0))/(cctk_gsh(2) - 1)
		deltan = CCTK_DELTA_TIME

		allocate(workingArray(cctk_nProcs(cctkGH)))
		allocate(finishedArray(cctk_nProcs(cctkGH)))

		FPC_epsilon_Cactus = max(0.0d0, max(3.1415926535897932384626433832795028841971693993751d+0, max(0.0d0, 3.1415926535897932384626433832795028841971693993751d+0))) * 1.0E-10

		SQRT3INV = 1.0/SQRT(3.0)
!		Segment mapping
!		----------------
!		Segment: interiorI
		if (1 .le. cctk_ubnd(1) - cctk_lbnd(1) + 1 .and. 1 .le. cctk_ubnd(2) - cctk_lbnd(2) + 1) then
			iMapStart = 1
			iMapEnd = cctk_lsh(1)
			jMapStart = 1
			jMapEnd = cctk_lsh(2)
			do i = iMapStart, iMapEnd
				do j = jMapStart, jMapEnd
						FOV_1(i, j) = 100
						FOV_yUpper(i, j) = 0
						FOV_xLower(i, j) = 0
						FOV_xUpper(i, j) = 0
						FOV_yLower(i, j) = 0
				end do
			end do
!			Check stencil
			do j = 1, cctk_lsh(2)
				do i = 1, cctk_lsh(1)
!					If wall, then it is considered exterior
					if (FOV_1(i, j) .gt. 0) then
						interior(i, j) = 1
					else
						interior(i, j) = -1
					end if
				end do
			end do
			do j = 1, cctk_lsh(2)
				do i = 1, cctk_lsh(1)
					if (FOV_1(i, j) .gt. 0) then
						call setStencilLimits(CCTK_PASS_FTOF, i, j, 1)
					end if
				end do
			end do
			modif = .true.
			do while (modif)
				modif = .false.
				do j = 1, cctk_lsh(2)
					do i = 1, cctk_lsh(1)
						if (interior(i, j) .eq. 1 .and. FOV_1(i, j) .eq. 0) then
						if (checkStencil(CCTK_PASS_FTOF, i, j, 1)) then
						modif = .true.
						end if
						end if
					end do
				end do
				do j = 1, cctk_lsh(2)
					do i = 1, cctk_lsh(1)
					if (interior(i, j) .eq. 2) then
				FOV_1(i, j) = 100
				FOV_yUpper(i, j) = 0
				FOV_xLower(i, j) = 0
				FOV_xUpper(i, j) = 0
				FOV_yLower(i, j) = 0
					end if
					end do
				end do
			end do
		end if
!		Boundaries Mapping
!		y-Lower
		if (cctk_bbox(3) .eq. 1) then
			do j = 1, 2
				do i = 1, cctk_lsh(1)
					FOV_yLower(i, j) = 100
					FOV_1(i, j) = 0
					FOV_yUpper(i, j) = 0
					FOV_xLower(i, j) = 0
					FOV_xUpper(i, j) = 0
				end do
			end do
		end if
!		x-Upper
		if (cctk_bbox(2) .eq. 1) then
			do j = 1, cctk_lsh(2)
				do i = cctk_lsh(1) - 1, cctk_lsh(1)
					FOV_xUpper(i, j) = 100
					FOV_1(i, j) = 0
					FOV_yUpper(i, j) = 0
					FOV_xLower(i, j) = 0
					FOV_yLower(i, j) = 0
				end do
			end do
		end if
!		x-Lower
		if (cctk_bbox(1) .eq. 1) then
			do j = 1, cctk_lsh(2)
				do i = 1, 2
					FOV_xLower(i, j) = 100
					FOV_1(i, j) = 0
					FOV_yUpper(i, j) = 0
					FOV_xUpper(i, j) = 0
					FOV_yLower(i, j) = 0
				end do
			end do
		end if
!		y-Upper
		if (cctk_bbox(4) .eq. 1) then
			do j = cctk_lsh(2) - 1, cctk_lsh(2)
				do i = 1, cctk_lsh(1)
					FOV_yUpper(i, j) = 100
					FOV_1(i, j) = 0
					FOV_xLower(i, j) = 0
					FOV_xUpper(i, j) = 0
					FOV_yLower(i, j) = 0
				end do
			end do
		end if


	end subroutine HeatProblem2D_mapping
	subroutine floodfill(CCTK_ARGUMENTS, i, j, pred, seg)
		implicit none

		DECLARE_CCTK_ARGUMENTS
		DECLARE_CCTK_PARAMETERS
		DECLARE_CCTK_FUNCTIONS
		CCTK_INT, INTENT(IN) :: i, j, pred, seg
		CCTK_INT hi(cctk_lsh(1)*cctk_lsh(2)), hj(cctk_lsh(1)*cctk_lsh(2))
		CCTK_INT ti, tj, index, loop
		logical control(cctk_lsh(1), cctk_lsh(2))

		CCTK_INT FOV(X0AuxiliaryGroup,X1AuxiliaryGroup)
		select case(seg)
			case (1)
				FOV = FOV_1
		end select
		do tj = 1 , cctk_lsh(2)
			do ti = 1 , cctk_lsh(1)
				control(ti, tj) = .false.
			end do
		end do

		hi(1) = i
		hj(1) = j
		index = 1
		do while (index .ne. 0)
			ti = hi(index)
			tj = hj(index)
			index = index - 1
			if (nonSync(ti, tj) .eq. 0) then
				nonSync(ti, tj) = pred
				interior(ti, tj) = pred
				if (pred .eq. 2) then
				FOV(ti, tj) = 100
				end if
				if (tj - 1 .ge. 1 .and. nonSync(ti, tj - 1) .eq. 0) then
					if (.not. control(ti, tj - 1)) then
						index = index + 1
						hi(index) = ti
						hj(index) = tj - 1
						control(ti, tj - 1) = .true.
					end if
				end if
				if (tj + 1 .le. cctk_lsh(2) .and. nonSync(ti, tj + 1) .eq. 0) then
					if (.not. control(ti, tj + 1)) then
						index = index + 1
						hi(index) = ti
						hj(index) = tj + 1
						control(ti, tj + 1) = .true.
					end if
				end if
				if (ti - 1 .ge. 1 .and. nonSync(ti - 1, tj) .eq. 0) then
					if (.not. control(ti - 1, tj)) then
						index = index + 1
						hi(index) = ti - 1
						hj(index) = tj
						control(ti - 1, tj) = .true.
					end if
				end if
				if (ti + 1 .le. cctk_lsh(1) .and. nonSync(ti + 1, tj) .eq. 0) then
					if (.not. control(ti + 1, tj)) then
						index = index + 1
						hi(index) = ti + 1
						hj(index) = tj
						control(ti + 1, tj) = .true.
					end if
				end if
			end if
		end do
		select case(seg)
			case (1)
				FOV_1 = FOV
		end select
	end subroutine floodfill
	logical function checkStalled(CCTK_ARGUMENTS, i, j, seg)
		implicit none

		DECLARE_CCTK_ARGUMENTS
		DECLARE_CCTK_PARAMETERS
		DECLARE_CCTK_FUNCTIONS

		CCTK_INT, INTENT(IN) :: i, j, seg
		logical notEnoughStencil
		CCTK_INT FOV_threshold
		CCTK_INT stencilAcc, stencilAccMax_i, it1, stencilAccMax_j, jt1;

		CCTK_INT FOV(X0AuxiliaryGroup,X1AuxiliaryGroup)
		select case(seg)
			case (1)
				FOV = FOV_1
		end select
		notEnoughStencil = .false.
		FOV_threshold = 0
		if (FOV(i, j) .le. FOV_threshold) then
			notEnoughStencil = .true.
		else
			stencilAcc = 0
			stencilAccMax_i = 0
			do it1 = MAX(i - 2, 0), MIN(i + 2, cctk_lsh(1))
				if (FOV(it1, j) .gt. FOV_threshold) then
					stencilAcc = stencilAcc + 1
				else
					stencilAccMax_i = MAX(stencilAccMax_i, stencilAcc)
					stencilAcc = 0
				end if
			end do
			stencilAccMax_i = MAX(stencilAccMax_i, stencilAcc)
			stencilAcc = 0
			stencilAccMax_j = 0
			do jt1 = MAX(j - 2, 0), MIN(j + 2, cctk_lsh(2))
				if (FOV(i, jt1) .gt. FOV_threshold) then
					stencilAcc = stencilAcc + 1
				else
					stencilAccMax_j = MAX(stencilAccMax_j, stencilAcc)
					stencilAcc = 0
				end if
			end do
			stencilAccMax_j = MAX(stencilAccMax_j, stencilAcc)
			if ((stencilAccMax_i .lt. 2) .or. (stencilAccMax_j .lt. 2)) then
				notEnoughStencil = .true.
			end if
		end if
		checkStalled = notEnoughStencil
	end function checkStalled
	subroutine setStencilLimits(CCTK_ARGUMENTS, i, j, v)
		implicit none

		DECLARE_CCTK_ARGUMENTS
		DECLARE_CCTK_PARAMETERS
		DECLARE_CCTK_FUNCTIONS

		CCTK_INT, INTENT(IN) :: i, j, v
		CCTK_INT iStart, iEnd, iti, jStart, jEnd, itj
		CCTK_INT shift
		CCTK_INT FOV(X0AuxiliaryGroup,X1AuxiliaryGroup)

		select case(v)
			case (1)
				FOV = FOV_1
		end select
		shift = 0
		if(cctk_bbox(1) .eq. 1) then
			do while(i - ((2 - 1) - (2 - 1)/2) + shift .lt. 3)
				shift = shift + 1
			end do
		end if
		if(cctk_bbox(2) .eq. 1) then
			do while(i + (2 - 1)/2 + shift .gt. cctk_lsh(1) - 2)
				shift = shift - 1
			end do
		end if
		iStart = ((2 - 1) - (2 - 1)/2) - shift
		iEnd = (2 - 1)/2 + shift

		shift = 0
		if(cctk_bbox(3) .eq. 1) then
			do while(j - ((2 - 1) - (2 - 1)/2) + shift .lt. 3)
				shift = shift + 1
			end do
		end if
		if(cctk_bbox(4) .eq. 1) then
			do while(j + (2 - 1)/2 + shift .gt. cctk_lsh(2) - 2)
				shift = shift - 1
			end do
		end if
		jStart = ((2 - 1) - (2 - 1)/2) - shift
		jEnd = (2 - 1)/2 + shift

		do iti = i - iStart, i + iEnd
			do itj = j - jStart, j + jEnd
				if(iti .ge. 1 .and. iti .le. cctk_lsh(1) .and. itj .ge. 1 .and. itj .le. cctk_lsh(2) .and. FOV(iti, itj) .eq. 0) then
					interior(iti, itj) = 1
				end if
			end do
		end do
		select case(v)
			case (1)
				FOV_1 = FOV
		end select
	end subroutine setStencilLimits

	logical function checkStencil(CCTK_ARGUMENTS,  i, j, v)
		implicit none

		DECLARE_CCTK_ARGUMENTS
		DECLARE_CCTK_PARAMETERS
		DECLARE_CCTK_FUNCTIONS

		CCTK_INT, INTENT(IN) :: i, j, v
		logical toGrow
		CCTK_INT iti, itj
		CCTK_INT FOV(X0AuxiliaryGroup,X1AuxiliaryGroup)

		select case(v)
			case (1)
				FOV = FOV_1
		end select
		toGrow = .false.
	if (i + 1 .le. cctk_lsh(1) .and. FOV(i + 1, j) .gt. 0 .and.  .not. toGrow) then
		do iti = i + 1, i + 2
			if ((iti .le. cctk_lsh(1)  .and. interior(iti, j) .lt. 1) .or. (iti .gt. cctk_lsh(1) .and. cctk_bbox(2) .eq. 1)) then
				toGrow = .true.
			end if
		end do
	end if
	if (i - 1 .ge. 1 .and. FOV(i - 1, j) .gt. 0 .and.  .not. toGrow) then
		do iti = i - 2, i - 1
			if ((iti .ge. 1  .and. interior(iti, j) .lt. 1) .or. (iti .lt. 1 .and. cctk_bbox(1) .eq. 1)) then
				toGrow = .true.
			end if
		end do
	end if
	if (j + 1 .le. cctk_lsh(2) .and. FOV(i, j + 1) .gt. 0 .and.  .not. toGrow) then
		do itj = j + 1, j + 2
			if ((itj .le. cctk_lsh(2)  .and. interior(i, itj) .lt. 1) .or. (itj .gt. cctk_lsh(2) .and. cctk_bbox(4) .eq. 1)) then
				toGrow = .true.
			end if
		end do
	end if
	if (i + 1 .le. cctk_lsh(1) .and. FOV(i + 1, j) .gt. 0 .and. j + 1 .le. cctk_lsh(2) .and. FOV(i, j + 1) .gt. 0 .and.  .not. toGrow) then
		do iti = i + 1, i + 2
			do itj = j + 1, j + 2
				if ((iti .le. cctk_lsh(1) .and. itj .le. cctk_lsh(2)  .and. interior(iti, itj) .lt. 1) .or. (iti .gt. cctk_lsh(1) .and. cctk_bbox(2) .eq. 1) .or. (itj .gt. cctk_lsh(2) .and. cctk_bbox(4) .eq. 1)) then
					toGrow = .true.
				end if
			end do
		end do
	end if
	if (i - 1 .ge. 1 .and. FOV(i - 1, j) .gt. 0 .and. j + 1 .le. cctk_lsh(2) .and. FOV(i, j + 1) .gt. 0 .and.  .not. toGrow) then
		do iti = i - 2, i - 1
			do itj = j + 1, j + 2
				if ((iti .ge. 1 .and. itj .le. cctk_lsh(2)  .and. interior(iti, itj) .lt. 1) .or. (iti .lt. 1 .and. cctk_bbox(1) .eq. 1) .or. (itj .gt. cctk_lsh(2) .and. cctk_bbox(4) .eq. 1)) then
					toGrow = .true.
				end if
			end do
		end do
	end if
	if (j - 1 .ge. 1 .and. FOV(i, j - 1) .gt. 0 .and.  .not. toGrow) then
		do itj = j - 2, j - 1
			if ((itj .ge. 1  .and. interior(i, itj) .lt. 1) .or. (itj .lt. 1 .and. cctk_bbox(3) .eq. 1)) then
				toGrow = .true.
			end if
		end do
	end if
	if (i + 1 .le. cctk_lsh(1) .and. FOV(i + 1, j) .gt. 0 .and. j - 1 .ge. 1 .and. FOV(i, j - 1) .gt. 0 .and.  .not. toGrow) then
		do iti = i + 1, i + 2
			do itj = j - 2, j - 1
				if ((iti .le. cctk_lsh(1) .and. itj .ge. 1  .and. interior(iti, itj) .lt. 1) .or. (iti .gt. cctk_lsh(1) .and. cctk_bbox(2) .eq. 1) .or. (itj .lt. 1 .and. cctk_bbox(3) .eq. 1)) then
					toGrow = .true.
				end if
			end do
		end do
	end if
	if (i - 1 .ge. 1 .and. FOV(i - 1, j) .gt. 0 .and. j - 1 .ge. 1 .and. FOV(i, j - 1) .gt. 0 .and.  .not. toGrow) then
		do iti = i - 2, i - 1
			do itj = j - 2, j - 1
				if ((iti .ge. 1 .and. itj .ge. 1  .and. interior(iti, itj) .lt. 1) .or. (iti .lt. 1 .and. cctk_bbox(1) .eq. 1) .or. (itj .lt. 1 .and. cctk_bbox(3) .eq. 1)) then
					toGrow = .true.
				end if
			end do
		end do
	end if
		if (toGrow) then
			interior(i, j) = 2
		end if
		checkStencil = toGrow
		select case(v)
			case (1)
				FOV_1 = FOV
		end select
	end function checkStencil
