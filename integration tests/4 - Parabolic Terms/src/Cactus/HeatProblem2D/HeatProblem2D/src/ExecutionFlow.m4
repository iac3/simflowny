/*@@
  @file      ExecutionFlow.F
  @date      Fri May 31 13:23:17 CEST 2013
  @author    4
  @desc
             Execution flow for HeatProblem2D
  @enddesc
  @version 4
@@*/

define(cdptii,1.0d0 / (144.0d0 * (deltax ** 2.0d0)) * ((($2) * ((((-25.0d0) * ($7) + 48.0d0 * ($8)) - 36.0d0 * ($6) + 16.0d0 * ($9)) - 3.0d0 * ($10)) - 8.0d0 * ($3) * (((-3.0d0) * ($7) - 10.0d0 * ($8) + 18.0d0 * ($6)) - 6.0d0 * ($9) + ($10)) + 8.0d0 * ($4) * (((-($7)) + 6.0d0 * ($8)) - 18.0d0 * ($6) + 10.0d0 * ($9) + 3.0d0 * ($10))) - ($5) * ((3.0d0 * ($7) - 16.0d0 * ($8) + 36.0d0 * ($6)) - 48.0d0 * ($9) + 25.0d0 * ($10))))dnl
define(cdptij,1.0d0 / (144.0d0 * deltax * deltay) * ((($2) * ((($11) - 8.0d0 * ($12) + 8.0d0 * ($13)) - ($14)) - 8.0d0 * ($3) * ((($15) - 8.0d0 * ($16) + 8.0d0 * ($17)) - ($18)) + 8.0d0 * ($4) * ((($19) - 8.0d0 * ($20) + 8.0d0 * ($21)) - ($22))) - ($5) * ((($23) - 8.0d0 * ($24) + 8.0d0 * ($25)) - ($26))))dnl
define(cdptji,1.0d0 / (144.0d0 * deltay * deltax) * ((($2) * ((($11) - 8.0d0 * ($12) + 8.0d0 * ($13)) - ($14)) - 8.0d0 * ($3) * ((($15) - 8.0d0 * ($16) + 8.0d0 * ($17)) - ($18)) + 8.0d0 * ($4) * ((($19) - 8.0d0 * ($20) + 8.0d0 * ($21)) - ($22))) - ($5) * ((($23) - 8.0d0 * ($24) + 8.0d0 * ($25)) - ($26))))dnl
define(cdptjj,1.0d0 / (144.0d0 * (deltay ** 2.0d0)) * ((($2) * ((((-25.0d0) * ($7) + 48.0d0 * ($8)) - 36.0d0 * ($6) + 16.0d0 * ($9)) - 3.0d0 * ($10)) - 8.0d0 * ($3) * (((-3.0d0) * ($7) - 10.0d0 * ($8) + 18.0d0 * ($6)) - 6.0d0 * ($9) + ($10)) + 8.0d0 * ($4) * (((-($7)) + 6.0d0 * ($8)) - 18.0d0 * ($6) + 10.0d0 * ($9) + 3.0d0 * ($10))) - ($5) * ((3.0d0 * ($7) - 16.0d0 * ($8) + 36.0d0 * ($6)) - 48.0d0 * ($9) + 25.0d0 * ($10))))dnl
define(FDOCi,1.0d0 / (12.0d0 * deltax) * (($3(($5) - 2, ($6)) - 8.0d0 * $3(($5) - 1, ($6)) + 8.0d0 * $3(($5) + 1, ($6))) - $3(($5) + 2, ($6)) + 1.0d0 * (((-$4(($5) - 2, ($6))) * ($2(($5) - 1, ($6)) - $2(($5) - 2, ($6))) + 3.0d0 * $4(($5) - 1, ($6)) * ($2(($5), ($6)) - $2(($5) - 1, ($6)))) - 3.0d0 * $4(($5), ($6)) * ($2(($5) + 1, ($6)) - $2(($5), ($6))) + $4(($5) + 1, ($6)) * ($2(($5) + 2, ($6)) - $2(($5) + 1, ($6))))))dnl
define(FDOCj,1.0d0 / (12.0d0 * deltay) * (($3(($5), ($6) - 2) - 8.0d0 * $3(($5), ($6) - 1) + 8.0d0 * $3(($5), ($6) + 1)) - $3(($5), ($6) + 2) + 1.0d0 * (((-$4(($5), ($6) - 2)) * ($2(($5), ($6) - 1) - $2(($5), ($6) - 2)) + 3.0d0 * $4(($5), ($6) - 1) * ($2(($5), ($6)) - $2(($5), ($6) - 1))) - 3.0d0 * $4(($5), ($6)) * ($2(($5), ($6) + 1) - $2(($5), ($6))) + $4(($5), ($6) + 1) * ($2(($5), ($6) + 2) - $2(($5), ($6) + 1)))))dnl
define(RK3P1,$3(($5), ($6)) + deltan * ($2))dnl
define(RK3P2,1.0d0 / 4.0d0 * (3.0d0 * $3(($5), ($6)) + $4(($5), ($6)) + deltan * ($2)))dnl
define(RK3P3,1.0d0 / 3.0d0 * ($3(($5), ($6)) + 2.0d0 * $4(($5), ($6)) + 2.0d0 * deltan * ($2)))dnl

#include "cctk.h"
#include "cctk_Parameters.h"
#include "cctk_Arguments.h"
#include "cctk_Functions.h"

	subroutine HeatProblem2D_execution(CCTK_ARGUMENTS)

		implicit none

		DECLARE_CCTK_ARGUMENTS
		DECLARE_CCTK_PARAMETERS
		DECLARE_CCTK_FUNCTIONS

!		Declare local variables
!		-----------------

		INTEGER i, iStart, iEnd
		INTEGER j, jStart, jEnd
		INTEGER istat, status, ReflectionCounter
		CCTK_REAL PT_ii_T_1, PT_jj_T_1, fluxAccT_1


		CCTK_INT exitCond
		CCTK_REAL deltax
		CCTK_REAL deltay
		CCTK_REAL deltan

		deltax = (3.1415926535897932384626433832795028841971693993751d+0 - (0.0d0))/(cctk_gsh(1) - 1)
		deltay = (3.1415926535897932384626433832795028841971693993751d+0 - (0.0d0))/(cctk_gsh(2) - 1)
		deltan = CCTK_DELTA_TIME

!		Finalization condition
		if (cctk_time .ge. tend) then 
			call CCTK_Exit(exitCond , cctkGH, 0)
		end if

!		Evolution
		iStart = 1
		iEnd = cctk_lsh(1)
		jStart = 1
		jEnd = cctk_lsh(2)
		do j = jStart, jEnd
			do i = iStart, iEnd
				if ((FOV_1(i, j) .gt. 0 .or. (((i + 1 .le. cctk_lsh(1) .and. FOV_1(i + 1, j) .gt. 0) .or. (i + 2 .le. cctk_lsh(1) .and. FOV_1(i + 2, j) .gt. 0) .or. (j + 1 .le. cctk_lsh(2) .and. FOV_1(i, j + 1) .gt. 0) .or. (j + 2 .le. cctk_lsh(2) .and. FOV_1(i, j + 2) .gt. 0)) .or. ((i - 1 .ge. 1 .and. FOV_1(i - 1, j) .gt. 0) .or. (i - 2 .ge. 1 .and. FOV_1(i - 2, j) .gt. 0) .or. (j - 1 .ge. 1 .and. FOV_1(i, j - 1) .gt. 0) .or. (j - 2 .ge. 1 .and. FOV_1(i, j - 2) .gt. 0)) .or. (((i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1) .and. FOV_1(int(i + 1), int(j - 1)) .gt. 0) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2)) .and. FOV_1(int(i + 1), int(j + 1)) .gt. 0) .or. ((i - 1 .ge. 1 .and. j - 1 .ge. 1) .and. FOV_1(int(i - 1), int(j - 1)) .gt. 0) .or. ((i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2)) .and. FOV_1(int(i - 1), int(j + 1)) .gt. 0) .or. ((i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1) .and. FOV_1(int(i + 1), int(j - 2)) .gt. 0) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2)) .and. FOV_1(int(i + 1), int(j + 2)) .gt. 0) .or. ((i - 1 .ge. 1 .and. j - 2 .ge. 1) .and. FOV_1(int(i - 1), int(j - 2)) .gt. 0) .or. ((i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2)) .and. FOV_1(int(i - 1), int(j + 2)) .gt. 0) .or. ((i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1) .and. FOV_1(int(i + 2), int(j - 1)) .gt. 0) .or. ((i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2)) .and. FOV_1(int(i + 2), int(j + 1)) .gt. 0) .or. ((i - 2 .ge. 1 .and. j - 1 .ge. 1) .and. FOV_1(int(i - 2), int(j - 1)) .gt. 0) .or. ((i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2)) .and. FOV_1(int(i - 2), int(j + 1)) .gt. 0) .or. ((i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1) .and. FOV_1(int(i + 2), int(j - 2)) .gt. 0) .or. ((i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2)) .and. FOV_1(int(i + 2), int(j + 2)) .gt. 0) .or. ((i - 2 .ge. 1 .and. j - 2 .ge. 1) .and. FOV_1(int(i - 2), int(j - 2)) .gt. 0) .or. ((i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2)) .and. FOV_1(int(i - 2), int(j + 2)) .gt. 0))))) then
					Speedi_1(i, j) = ABS(0)
					Speedj_1(i, j) = ABS(0)
					Q_ii_T_1(i, j) = 1.0d0
					Q_jj_T_1(i, j) = 1.0d0
					P_ii_T_1(i, j) = T_p(i, j)
					P_jj_T_1(i, j) = T_p(i, j)
				end if
			end do
		end do
		do j = jStart, jEnd
			do i = iStart, iEnd
				if ((FOV_1(i, j) .gt. 0 .or. (((i + 1 .le. cctk_lsh(1) .and. FOV_1(i + 1, j) .gt. 0) .or. (j + 1 .le. cctk_lsh(2) .and. FOV_1(i, j + 1) .gt. 0)) .or. ((i - 1 .ge. 1 .and. FOV_1(i - 1, j) .gt. 0) .or. (j - 1 .ge. 1 .and. FOV_1(i, j - 1) .gt. 0)))) .and. (i + 1 .le. cctk_lsh(1) .and. i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. j - 1 .ge. 1)) then
					Speedi_1(i, j) = MAX(Speedi_1(i, j), Speedi_1(i + 1, j))
					Speedj_1(i, j) = MAX(Speedj_1(i, j), Speedj_1(i, j + 1))
				end if
			end do
		end do
		call CCTK_SyncGroup(status, cctkGH, "HeatProblem2D::block1")
		do j = jStart, jEnd
			do i = iStart, iEnd
				if ((FOV_1(i, j) .gt. 0) .and. (i + 2 .le. cctk_lsh(1) .and. i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. j - 2 .ge. 1)) then
					PT_ii_T_1 = cdptii(CCTK_PASS_FTOF, Q_ii_T_1(i - 2, j), Q_ii_T_1(i - 1, j), Q_ii_T_1(i + 1, j), Q_ii_T_1(i + 2, j), P_ii_T_1(i, j), P_ii_T_1(i - 2, j), P_ii_T_1(i - 1, j), P_ii_T_1(i + 1, j), P_ii_T_1(i + 2, j), P_ii_T_1(i - 2, j), P_ii_T_1(i - 2, j), P_ii_T_1(i - 2, j), P_ii_T_1(i - 2, j), P_ii_T_1(i - 1, j), P_ii_T_1(i - 1, j), P_ii_T_1(i - 1, j), P_ii_T_1(i - 1, j), P_ii_T_1(i + 1, j), P_ii_T_1(i + 1, j), P_ii_T_1(i + 1, j), P_ii_T_1(i + 1, j), P_ii_T_1(i + 2, j), P_ii_T_1(i + 2, j), P_ii_T_1(i + 2, j), P_ii_T_1(i + 2, j))
					PT_jj_T_1 = cdptjj(CCTK_PASS_FTOF, Q_jj_T_1(i, j - 2), Q_jj_T_1(i, j - 1), Q_jj_T_1(i, j + 1), Q_jj_T_1(i, j + 2), P_jj_T_1(i, j), P_jj_T_1(i, j - 2), P_jj_T_1(i, j - 1), P_jj_T_1(i, j + 1), P_jj_T_1(i, j + 2), P_jj_T_1(i, j - 2), P_jj_T_1(i, j - 2), P_jj_T_1(i, j - 2), P_jj_T_1(i, j - 2), P_jj_T_1(i, j - 1), P_jj_T_1(i, j - 1), P_jj_T_1(i, j - 1), P_jj_T_1(i, j - 1), P_jj_T_1(i, j + 1), P_jj_T_1(i, j + 1), P_jj_T_1(i, j + 1), P_jj_T_1(i, j + 1), P_jj_T_1(i, j + 2), P_jj_T_1(i, j + 2), P_jj_T_1(i, j + 2), P_jj_T_1(i, j + 2))
					fluxAccT_1 = 0.0d0 + (PT_ii_T_1 + PT_jj_T_1)
					k1T(i, j) = RK3P1(CCTK_PASS_FTOF, fluxAccT_1, T_p, 0, i, j)
				end if
			end do
		end do
		call CCTK_SyncGroup(status, cctkGH, "HeatProblem2D::block2")
		do j = jStart, jEnd
			do i = iStart, iEnd
!				Boundaries
		if ((FOV_xLower(i, j) .gt. 0) .and. ((i + 1 .le. cctk_lsh(1) .and. (FOV_1(int(i + 1), j) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. (FOV_1(int(i + 2), j) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 2)) .gt. 0))))) then
				k1T(3 - i, j) = 0.0d0
		end if
		if ((FOV_xUpper(i, j) .gt. 0) .and. ((i - 1 .ge. 1 .and. (FOV_1(int(i - 1), j) .gt. 0)) .or. (i - 2 .ge. 1 .and. (FOV_1(int(i - 2), j) .gt. 0)) .or. ((i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 2)) .gt. 0))))) then
			k1T(i, j) = 0.0d0
		end if
		if ((FOV_yLower(i, j) .gt. 0) .and. ((j + 1 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 1)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 2)) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 2)) .gt. 0))))) then
				k1T(i, 3 - j) = 0.0d0
		end if
		if ((FOV_yUpper(i, j) .gt. 0) .and. ((j - 1 .ge. 1 .and. (FOV_1(i, int(j - 1)) .gt. 0)) .or. (j - 2 .ge. 1 .and. (FOV_1(i, int(j - 2)) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 2)) .gt. 0))))) then
			k1T(i, j) = 0.0d0 + (i - (1 - cctk_lbnd(1))) * deltax
		end if
			end do
		end do
		call CCTK_SyncGroup(status, cctkGH, "HeatProblem2D::block2")
		do j = jStart, jEnd
			do i = iStart, iEnd
				if ((FOV_1(i, j) .gt. 0 .or. (((i + 1 .le. cctk_lsh(1) .and. FOV_1(i + 1, j) .gt. 0) .or. (i + 2 .le. cctk_lsh(1) .and. FOV_1(i + 2, j) .gt. 0) .or. (j + 1 .le. cctk_lsh(2) .and. FOV_1(i, j + 1) .gt. 0) .or. (j + 2 .le. cctk_lsh(2) .and. FOV_1(i, j + 2) .gt. 0)) .or. ((i - 1 .ge. 1 .and. FOV_1(i - 1, j) .gt. 0) .or. (i - 2 .ge. 1 .and. FOV_1(i - 2, j) .gt. 0) .or. (j - 1 .ge. 1 .and. FOV_1(i, j - 1) .gt. 0) .or. (j - 2 .ge. 1 .and. FOV_1(i, j - 2) .gt. 0)) .or. (((i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1) .and. FOV_1(int(i + 1), int(j - 1)) .gt. 0) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2)) .and. FOV_1(int(i + 1), int(j + 1)) .gt. 0) .or. ((i - 1 .ge. 1 .and. j - 1 .ge. 1) .and. FOV_1(int(i - 1), int(j - 1)) .gt. 0) .or. ((i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2)) .and. FOV_1(int(i - 1), int(j + 1)) .gt. 0) .or. ((i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1) .and. FOV_1(int(i + 1), int(j - 2)) .gt. 0) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2)) .and. FOV_1(int(i + 1), int(j + 2)) .gt. 0) .or. ((i - 1 .ge. 1 .and. j - 2 .ge. 1) .and. FOV_1(int(i - 1), int(j - 2)) .gt. 0) .or. ((i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2)) .and. FOV_1(int(i - 1), int(j + 2)) .gt. 0) .or. ((i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1) .and. FOV_1(int(i + 2), int(j - 1)) .gt. 0) .or. ((i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2)) .and. FOV_1(int(i + 2), int(j + 1)) .gt. 0) .or. ((i - 2 .ge. 1 .and. j - 1 .ge. 1) .and. FOV_1(int(i - 2), int(j - 1)) .gt. 0) .or. ((i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2)) .and. FOV_1(int(i - 2), int(j + 1)) .gt. 0) .or. ((i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1) .and. FOV_1(int(i + 2), int(j - 2)) .gt. 0) .or. ((i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2)) .and. FOV_1(int(i + 2), int(j + 2)) .gt. 0) .or. ((i - 2 .ge. 1 .and. j - 2 .ge. 1) .and. FOV_1(int(i - 2), int(j - 2)) .gt. 0) .or. ((i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2)) .and. FOV_1(int(i - 2), int(j + 2)) .gt. 0))))) then
					Speedi_1(i, j) = ABS(0)
					Speedj_1(i, j) = ABS(0)
					Q_ii_T_1(i, j) = 1.0d0
					Q_jj_T_1(i, j) = 1.0d0
					P_ii_T_1(i, j) = k1T(i, j)
					P_jj_T_1(i, j) = k1T(i, j)
				end if
			end do
		end do
		do j = jStart, jEnd
			do i = iStart, iEnd
				if ((FOV_1(i, j) .gt. 0 .or. (((i + 1 .le. cctk_lsh(1) .and. FOV_1(i + 1, j) .gt. 0) .or. (j + 1 .le. cctk_lsh(2) .and. FOV_1(i, j + 1) .gt. 0)) .or. ((i - 1 .ge. 1 .and. FOV_1(i - 1, j) .gt. 0) .or. (j - 1 .ge. 1 .and. FOV_1(i, j - 1) .gt. 0)))) .and. (i + 1 .le. cctk_lsh(1) .and. i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. j - 1 .ge. 1)) then
					Speedi_1(i, j) = MAX(Speedi_1(i, j), Speedi_1(i + 1, j))
					Speedj_1(i, j) = MAX(Speedj_1(i, j), Speedj_1(i, j + 1))
				end if
			end do
		end do
		call CCTK_SyncGroup(status, cctkGH, "HeatProblem2D::block1")
		do j = jStart, jEnd
			do i = iStart, iEnd
				if ((FOV_1(i, j) .gt. 0) .and. (i + 2 .le. cctk_lsh(1) .and. i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. j - 2 .ge. 1)) then
					PT_ii_T_1 = cdptii(CCTK_PASS_FTOF, Q_ii_T_1(i - 2, j), Q_ii_T_1(i - 1, j), Q_ii_T_1(i + 1, j), Q_ii_T_1(i + 2, j), P_ii_T_1(i, j), P_ii_T_1(i - 2, j), P_ii_T_1(i - 1, j), P_ii_T_1(i + 1, j), P_ii_T_1(i + 2, j), P_ii_T_1(i - 2, j), P_ii_T_1(i - 2, j), P_ii_T_1(i - 2, j), P_ii_T_1(i - 2, j), P_ii_T_1(i - 1, j), P_ii_T_1(i - 1, j), P_ii_T_1(i - 1, j), P_ii_T_1(i - 1, j), P_ii_T_1(i + 1, j), P_ii_T_1(i + 1, j), P_ii_T_1(i + 1, j), P_ii_T_1(i + 1, j), P_ii_T_1(i + 2, j), P_ii_T_1(i + 2, j), P_ii_T_1(i + 2, j), P_ii_T_1(i + 2, j))
					PT_jj_T_1 = cdptjj(CCTK_PASS_FTOF, Q_jj_T_1(i, j - 2), Q_jj_T_1(i, j - 1), Q_jj_T_1(i, j + 1), Q_jj_T_1(i, j + 2), P_jj_T_1(i, j), P_jj_T_1(i, j - 2), P_jj_T_1(i, j - 1), P_jj_T_1(i, j + 1), P_jj_T_1(i, j + 2), P_jj_T_1(i, j - 2), P_jj_T_1(i, j - 2), P_jj_T_1(i, j - 2), P_jj_T_1(i, j - 2), P_jj_T_1(i, j - 1), P_jj_T_1(i, j - 1), P_jj_T_1(i, j - 1), P_jj_T_1(i, j - 1), P_jj_T_1(i, j + 1), P_jj_T_1(i, j + 1), P_jj_T_1(i, j + 1), P_jj_T_1(i, j + 1), P_jj_T_1(i, j + 2), P_jj_T_1(i, j + 2), P_jj_T_1(i, j + 2), P_jj_T_1(i, j + 2))
					fluxAccT_1 = 0.0d0 + (PT_ii_T_1 + PT_jj_T_1)
					k2T(i, j) = RK3P2(CCTK_PASS_FTOF, fluxAccT_1, T_p, k1T, i, j)
				end if
			end do
		end do
		call CCTK_SyncGroup(status, cctkGH, "HeatProblem2D::block6")
		do j = jStart, jEnd
			do i = iStart, iEnd
!				Boundaries
		if ((FOV_xLower(i, j) .gt. 0) .and. ((i + 1 .le. cctk_lsh(1) .and. (FOV_1(int(i + 1), j) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. (FOV_1(int(i + 2), j) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 2)) .gt. 0))))) then
				k2T(3 - i, j) = 0.0d0
		end if
		if ((FOV_xUpper(i, j) .gt. 0) .and. ((i - 1 .ge. 1 .and. (FOV_1(int(i - 1), j) .gt. 0)) .or. (i - 2 .ge. 1 .and. (FOV_1(int(i - 2), j) .gt. 0)) .or. ((i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 2)) .gt. 0))))) then
			k2T(i, j) = 0.0d0
		end if
		if ((FOV_yLower(i, j) .gt. 0) .and. ((j + 1 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 1)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 2)) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 2)) .gt. 0))))) then
				k2T(i, 3 - j) = 0.0d0
		end if
		if ((FOV_yUpper(i, j) .gt. 0) .and. ((j - 1 .ge. 1 .and. (FOV_1(i, int(j - 1)) .gt. 0)) .or. (j - 2 .ge. 1 .and. (FOV_1(i, int(j - 2)) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 2)) .gt. 0))))) then
			k2T(i, j) = 0.0d0 + (i - (1 - cctk_lbnd(1))) * deltax
		end if
			end do
		end do
		call CCTK_SyncGroup(status, cctkGH, "HeatProblem2D::block6")
		do j = jStart, jEnd
			do i = iStart, iEnd
				if ((FOV_1(i, j) .gt. 0 .or. (((i + 1 .le. cctk_lsh(1) .and. FOV_1(i + 1, j) .gt. 0) .or. (i + 2 .le. cctk_lsh(1) .and. FOV_1(i + 2, j) .gt. 0) .or. (j + 1 .le. cctk_lsh(2) .and. FOV_1(i, j + 1) .gt. 0) .or. (j + 2 .le. cctk_lsh(2) .and. FOV_1(i, j + 2) .gt. 0)) .or. ((i - 1 .ge. 1 .and. FOV_1(i - 1, j) .gt. 0) .or. (i - 2 .ge. 1 .and. FOV_1(i - 2, j) .gt. 0) .or. (j - 1 .ge. 1 .and. FOV_1(i, j - 1) .gt. 0) .or. (j - 2 .ge. 1 .and. FOV_1(i, j - 2) .gt. 0)) .or. (((i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1) .and. FOV_1(int(i + 1), int(j - 1)) .gt. 0) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2)) .and. FOV_1(int(i + 1), int(j + 1)) .gt. 0) .or. ((i - 1 .ge. 1 .and. j - 1 .ge. 1) .and. FOV_1(int(i - 1), int(j - 1)) .gt. 0) .or. ((i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2)) .and. FOV_1(int(i - 1), int(j + 1)) .gt. 0) .or. ((i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1) .and. FOV_1(int(i + 1), int(j - 2)) .gt. 0) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2)) .and. FOV_1(int(i + 1), int(j + 2)) .gt. 0) .or. ((i - 1 .ge. 1 .and. j - 2 .ge. 1) .and. FOV_1(int(i - 1), int(j - 2)) .gt. 0) .or. ((i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2)) .and. FOV_1(int(i - 1), int(j + 2)) .gt. 0) .or. ((i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1) .and. FOV_1(int(i + 2), int(j - 1)) .gt. 0) .or. ((i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2)) .and. FOV_1(int(i + 2), int(j + 1)) .gt. 0) .or. ((i - 2 .ge. 1 .and. j - 1 .ge. 1) .and. FOV_1(int(i - 2), int(j - 1)) .gt. 0) .or. ((i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2)) .and. FOV_1(int(i - 2), int(j + 1)) .gt. 0) .or. ((i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1) .and. FOV_1(int(i + 2), int(j - 2)) .gt. 0) .or. ((i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2)) .and. FOV_1(int(i + 2), int(j + 2)) .gt. 0) .or. ((i - 2 .ge. 1 .and. j - 2 .ge. 1) .and. FOV_1(int(i - 2), int(j - 2)) .gt. 0) .or. ((i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2)) .and. FOV_1(int(i - 2), int(j + 2)) .gt. 0))))) then
					Speedi_1(i, j) = ABS(0)
					Speedj_1(i, j) = ABS(0)
					Q_ii_T_1(i, j) = 1.0d0
					Q_jj_T_1(i, j) = 1.0d0
					P_ii_T_1(i, j) = k2T(i, j)
					P_jj_T_1(i, j) = k2T(i, j)
				end if
			end do
		end do
		do j = jStart, jEnd
			do i = iStart, iEnd
				if ((FOV_1(i, j) .gt. 0 .or. (((i + 1 .le. cctk_lsh(1) .and. FOV_1(i + 1, j) .gt. 0) .or. (j + 1 .le. cctk_lsh(2) .and. FOV_1(i, j + 1) .gt. 0)) .or. ((i - 1 .ge. 1 .and. FOV_1(i - 1, j) .gt. 0) .or. (j - 1 .ge. 1 .and. FOV_1(i, j - 1) .gt. 0)))) .and. (i + 1 .le. cctk_lsh(1) .and. i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. j - 1 .ge. 1)) then
					Speedi_1(i, j) = MAX(Speedi_1(i, j), Speedi_1(i + 1, j))
					Speedj_1(i, j) = MAX(Speedj_1(i, j), Speedj_1(i, j + 1))
				end if
			end do
		end do
		call CCTK_SyncGroup(status, cctkGH, "HeatProblem2D::block1")
		do j = jStart, jEnd
			do i = iStart, iEnd
				if ((FOV_1(i, j) .gt. 0) .and. (i + 2 .le. cctk_lsh(1) .and. i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. j - 2 .ge. 1)) then
					PT_ii_T_1 = cdptii(CCTK_PASS_FTOF, Q_ii_T_1(i - 2, j), Q_ii_T_1(i - 1, j), Q_ii_T_1(i + 1, j), Q_ii_T_1(i + 2, j), P_ii_T_1(i, j), P_ii_T_1(i - 2, j), P_ii_T_1(i - 1, j), P_ii_T_1(i + 1, j), P_ii_T_1(i + 2, j), P_ii_T_1(i - 2, j), P_ii_T_1(i - 2, j), P_ii_T_1(i - 2, j), P_ii_T_1(i - 2, j), P_ii_T_1(i - 1, j), P_ii_T_1(i - 1, j), P_ii_T_1(i - 1, j), P_ii_T_1(i - 1, j), P_ii_T_1(i + 1, j), P_ii_T_1(i + 1, j), P_ii_T_1(i + 1, j), P_ii_T_1(i + 1, j), P_ii_T_1(i + 2, j), P_ii_T_1(i + 2, j), P_ii_T_1(i + 2, j), P_ii_T_1(i + 2, j))
					PT_jj_T_1 = cdptjj(CCTK_PASS_FTOF, Q_jj_T_1(i, j - 2), Q_jj_T_1(i, j - 1), Q_jj_T_1(i, j + 1), Q_jj_T_1(i, j + 2), P_jj_T_1(i, j), P_jj_T_1(i, j - 2), P_jj_T_1(i, j - 1), P_jj_T_1(i, j + 1), P_jj_T_1(i, j + 2), P_jj_T_1(i, j - 2), P_jj_T_1(i, j - 2), P_jj_T_1(i, j - 2), P_jj_T_1(i, j - 2), P_jj_T_1(i, j - 1), P_jj_T_1(i, j - 1), P_jj_T_1(i, j - 1), P_jj_T_1(i, j - 1), P_jj_T_1(i, j + 1), P_jj_T_1(i, j + 1), P_jj_T_1(i, j + 1), P_jj_T_1(i, j + 1), P_jj_T_1(i, j + 2), P_jj_T_1(i, j + 2), P_jj_T_1(i, j + 2), P_jj_T_1(i, j + 2))
					fluxAccT_1 = 0.0d0 + (PT_ii_T_1 + PT_jj_T_1)
					T(i, j) = RK3P3(CCTK_PASS_FTOF, fluxAccT_1, T_p, k2T, i, j)
				end if
			end do
		end do
		call CCTK_SyncGroup(status, cctkGH, "HeatProblem2D::fields_tl2")
		do j = jStart, jEnd
			do i = iStart, iEnd
!				Boundaries
		if ((FOV_xLower(i, j) .gt. 0) .and. ((i + 1 .le. cctk_lsh(1) .and. (FOV_1(int(i + 1), j) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. (FOV_1(int(i + 2), j) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 2)) .gt. 0))))) then
				T(3 - i, j) = 0.0d0
		end if
		if ((FOV_xUpper(i, j) .gt. 0) .and. ((i - 1 .ge. 1 .and. (FOV_1(int(i - 1), j) .gt. 0)) .or. (i - 2 .ge. 1 .and. (FOV_1(int(i - 2), j) .gt. 0)) .or. ((i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 2)) .gt. 0))))) then
			T(i, j) = 0.0d0
		end if
		if ((FOV_yLower(i, j) .gt. 0) .and. ((j + 1 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 1)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 2)) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 2)) .gt. 0))))) then
				T(i, 3 - j) = 0.0d0
		end if
		if ((FOV_yUpper(i, j) .gt. 0) .and. ((j - 1 .ge. 1 .and. (FOV_1(i, int(j - 1)) .gt. 0)) .or. (j - 2 .ge. 1 .and. (FOV_1(i, int(j - 2)) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 2)) .gt. 0))))) then
			T(i, j) = 0.0d0 + (i - (1 - cctk_lbnd(1))) * deltax
		end if
			end do
		end do
		call CCTK_SyncGroup(status, cctkGH, "HeatProblem2D::fields_tl2")

	end subroutine HeatProblem2D_execution