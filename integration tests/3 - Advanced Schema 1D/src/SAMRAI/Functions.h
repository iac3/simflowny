#ifndef included_FunctionsXD
#define included_FunctionsXD

#include "SAMRAI/tbox/Utilities.h"
#include <string>
#include <math.h>
#include <vector>
#include "hdf5.h"
#include "Commons.h"


#define MIN(X,Y) ((X) < (Y) ? (X) : (Y))
#define MAX(X,Y) ((X) > (Y) ? (X) : (Y))
#define SIGN(X) (((X) > 0) - ((X) < 0))
#define greaterEq(a,b) ((fabs((a) - (b))/1.0E-15 > 10 ? false : (floor(fabs((a) - (b))/1.0E-15) < 1)) || (b)<(a))
#define lessEq(a,b) ((fabs((a) - (b))/1.0E-15 > 10 ? false: (floor(fabs((a) - (b))/1.0E-15) < 1)) || (a)<(b))
#define equalsEq(a,b) ((fabs((a) - (b))/1.0E-15 > 10 ? false: (floor(fabs((a) - (b))/1.0E-15) < 1)))
#define reducePrecision(x, p) (floor(((x) * pow(10, (p)) + 0.5)) / pow(10, (p)))

using namespace external;

/*
 * Calculates coefficients for quintic lagrangian interpolation.
 *    coefs;      coefficients to obtain
 *    coord:      point in which the interpolation must be calculated
 *    position:   surrounding points for interpolation
 */
inline void calculateCoefficientsMapFile(double* coefs, double coord, double* position) {

    for (int i = 0; i < 6; i++) {
        coefs[i] = 0;
    }
    //Search for a perfect fit

    for (int i = 0; i < 6; i++) {
        if (position[i] == coord) {
            coefs[i] = 1;
            return;
        }
    }

    double x1 = position[0];
    double x2 = position[1];
    double x3 = position[2];
    double x4 = position[3];
    double x5 = position[4];
    double x6 = position[5];

    coefs[0] = (coord-x2)*(coord-x3)*(coord-x4)*(coord-x5)*(coord-x6) / ((x1-x2)*(x1-x3)*(x1-x4)*(x1-x5)*(x1-x6));
    coefs[1] = (coord-x1)*(coord-x3)*(coord-x4)*(coord-x5)*(coord-x6) / ((x2-x1)*(x2-x3)*(x2-x4)*(x2-x5)*(x2-x6));
    coefs[2] = (coord-x1)*(coord-x2)*(coord-x4)*(coord-x5)*(coord-x6) / ((x3-x1)*(x3-x2)*(x3-x4)*(x3-x5)*(x3-x6));
    coefs[3] = (coord-x1)*(coord-x2)*(coord-x3)*(coord-x5)*(coord-x6) / ((x4-x1)*(x4-x2)*(x4-x3)*(x4-x5)*(x4-x6));
    coefs[4] = (coord-x1)*(coord-x2)*(coord-x3)*(coord-x4)*(coord-x6) / ((x5-x1)*(x5-x2)*(x5-x3)*(x5-x4)*(x5-x6));
    coefs[5] = (coord-x1)*(coord-x2)*(coord-x3)*(coord-x4)*(coord-x5) / ((x6-x1)*(x6-x2)*(x6-x3)*(x6-x4)*(x6-x5));
}



#define vector(v, i, j) (v)[i+ilast*(j)]

#define weno(parw0, parw1, parw2, parw3, parp0, parp1, parp2, parp3, dx, simPlat_dt, ilast, jlast) ((parw0) * (parp0) + (parw1) * (parp1) + (parw2) * (parp2) + (parw3) * (parp3))

#define lor1(paru1, paru2, paru3, paru4, dx, simPlat_dt, ilast, jlast) (((-1.0 / 4.0) * (paru1) + 13.0 / 12.0 * (paru2)) - 23.0 / 12.0 * (paru3) + 25.0 / 12.0 * (paru4))

#define lor2(paru1, paru2, paru3, paru4, dx, simPlat_dt, ilast, jlast) (1.0 / 12.0 * (paru1) - 5.0 / 12.0 * (paru2) + 13.0 / 12.0 * (paru3) + 1.0 / 4.0 * (paru4))

#define lor3(paru1, paru2, paru3, paru4, dx, simPlat_dt, ilast, jlast) (((-1.0 / 12.0) * (paru1) + 7.0 / 12.0 * (paru2) + 7.0 / 12.0 * (paru3)) - 1.0 / 12.0 * (paru4))

#define lor4(paru1, paru2, paru3, paru4, dx, simPlat_dt, ilast, jlast) ((1.0 / 4.0 * (paru1) + 13.0 / 12.0 * (paru2)) - 5.0 / 12.0 * (paru3) + 1.0 / 12.0 * (paru4))

#define smooth1(paru1, paru2, paru3, paru4, dx, simPlat_dt, ilast, jlast) ((paru1) * ((547.0 * (paru1) - 3882.0 * (paru2) + 4642.0 * (paru3)) - 1854.0 * (paru4)) + (paru2) * (7043.0 * (paru2) - 17246.0 * (paru3) + 7042.0 * (paru4)) + (paru3) * (11003.0 * (paru3) - 9402.0 * (paru4)) + 2107.0 * (paru4) * (paru4))

#define smooth2(paru1, paru2, paru3, paru4, dx, simPlat_dt, ilast, jlast) ((paru1) * ((267.0 * (paru1) - 1642.0 * (paru2) + 1602.0 * (paru3)) - 494.0 * (paru4)) + (paru2) * (2843.0 * (paru2) - 5966.0 * (paru3) + 1922.0 * (paru4)) + (paru3) * (3443.0 * (paru3) - 2522.0 * (paru4)) + 547.0 * (paru4) * (paru4))

#define smooth3(paru1, paru2, paru3, paru4, dx, simPlat_dt, ilast, jlast) ((paru1) * ((547.0 * (paru1) - 2522.0 * (paru2) + 1922.0 * (paru3)) - 494.0 * (paru4)) + (paru2) * (3443.0 * (paru2) - 5966.0 * (paru3) + 1602.0 * (paru4)) + (paru3) * (2843.0 * (paru3) - 1642.0 * (paru4)) + 267.0 * (paru4) * (paru4))

#define smooth4(paru1, paru2, paru3, paru4, dx, simPlat_dt, ilast, jlast) ((paru1) * ((2107.0 * (paru1) - 9402.0 * (paru2) + 7042.0 * (paru3)) - 1854.0 * (paru4)) + (paru2) * (11003.0 * (paru2) - 17246.0 * (paru3) + 4642.0 * (paru4)) + (paru3) * (7043.0 * (paru3) - 3882.0 * (paru4)) + 547.0 * (paru4) * (paru4))

#define nlw1(parbeta, parepsilon, dx, simPlat_dt, ilast, jlast) (1.0 / (35.0 * (((parepsilon) + (parbeta)) * ((parepsilon) + (parbeta)))))

#define nlw2(parbeta, parepsilon, dx, simPlat_dt, ilast, jlast) (12.0 / (35.0 * (((parepsilon) + (parbeta)) * ((parepsilon) + (parbeta)))))

#define nlw3(parbeta, parepsilon, dx, simPlat_dt, ilast, jlast) (18.0 / (35.0 * (((parepsilon) + (parbeta)) * ((parepsilon) + (parbeta)))))

#define nlw4(parbeta, parepsilon, dx, simPlat_dt, ilast, jlast) (4.0 / (35.0 * (((parepsilon) + (parbeta)) * ((parepsilon) + (parbeta)))))

#define LLF_i(parFuln, parFurn, parFulp, parFurp, paruln, parurn, parulp, parurp, parspeed, parspeedn, parspeedp, dx, simPlat_dt, ilast, jlast) (((0.5 * ((parFulp) + (parFurp)) - 0.5 * MAX((parspeed), (parspeedp)) * ((parurp) - (parulp))) - (0.5 * ((parFuln) + (parFurn)) - 0.5 * MAX((parspeed), (parspeedn)) * ((parurn) - (paruln)))) / dx[0])

#define LLF_j(parFuln, parFurn, parFulp, parFurp, paruln, parurn, parulp, parurp, parspeed, parspeedn, parspeedp, dx, simPlat_dt, ilast, jlast) (((0.5 * ((parFulp) + (parFurp)) - 0.5 * MAX((parspeed), (parspeedp)) * ((parurp) - (parulp))) - (0.5 * ((parFuln) + (parFurn)) - 0.5 * MAX((parspeed), (parspeedn)) * ((parurn) - (paruln)))) / dx[1])

#define RK3P1_(parsources, parQ1, parQ2, pari, parj, dx, simPlat_dt, ilast, jlast) (vector(parQ1, (pari), (parj)) + simPlat_dt * (parsources))

#define RK3P2_(parsources, parQ1, parQ2, pari, parj, dx, simPlat_dt, ilast, jlast) (1.0 / 4.0 * (3.0 * vector(parQ1, (pari), (parj)) + vector(parQ2, (pari), (parj)) + simPlat_dt * (parsources)))

#define RK3P3_(parsources, parQ1, parQ2, pari, parj, dx, simPlat_dt, ilast, jlast) (1.0 / 3.0 * (vector(parQ1, (pari), (parj)) + 2.0 * vector(parQ2, (pari), (parj)) + 2.0 * simPlat_dt * (parsources)))

#define FiPhi_segmentI(parPhi, dx, simPlat_dt, ilast, jlast) ((parPhi))





#endif
