 /*@@
   @file      Startup.F90
   @date
   @author    3
   @desc
              Register banner
   @enddesc
   @version 3

 @@*/

#include "cctk.h"

	integer function AdvectionwithBalsaranegShu_startup ()

	implicit none

	integer ierr
	call CCTK_RegisterBanner(ierr, "AdvectionwithBalsaranegShu")
	AdvectionwithBalsaranegShu_startup = 0

	end function AdvectionwithBalsaranegShu_startup