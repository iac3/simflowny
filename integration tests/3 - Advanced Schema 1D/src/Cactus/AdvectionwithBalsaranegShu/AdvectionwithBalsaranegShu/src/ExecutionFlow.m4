/*@@
  @file      ExecutionFlow.F
  @date      Fri May 31 13:15:47 CEST 2013
  @author    3
  @desc
             Execution flow for AdvectionwithBalsaranegShu
  @enddesc
  @version 3
@@*/

define(cdptii,1.0d0 / (705600.0d0 * (deltax ** 2.0d0)) * ((((3.0d0 * ($2) * ((((((-2283.0d0) * ($11) + 6720.0d0 * ($12)) - 11760.0d0 * ($13) + 15680.0d0 * ($14)) - 14700.0d0 * ($10) + 9408.0d0 * ($15)) - 3920.0d0 * ($16) + 960.0d0 * ($17)) - 105.0d0 * ($18)) - 32.0d0 * ($3) * (((((-105.0d0) * ($11) - 1338.0d0 * ($12) + 2940.0d0 * ($13)) - 2940.0d0 * ($14) + 2450.0d0 * ($10)) - 1470.0d0 * ($15) + 588.0d0 * ($16)) - 140.0d0 * ($17) + 15.0d0 * ($18)) + 168.0d0 * ($4) * (((((15.0d0 * ($11) - 240.0d0 * ($12)) - 798.0d0 * ($13) + 1680.0d0 * ($14)) - 1050.0d0 * ($10) + 560.0d0 * ($15)) - 210.0d0 * ($16) + 48.0d0 * ($17)) - 5.0d0 * ($18))) - 672.0d0 * ($5) * ((((((-5.0d0) * ($11) + 60.0d0 * ($12)) - 420.0d0 * ($13)) - 378.0d0 * ($14) + 1050.0d0 * ($10)) - 420.0d0 * ($15) + 140.0d0 * ($16)) - 30.0d0 * ($17) + 3.0d0 * ($18)) + 672.0d0 * ($6) * (((((-3.0d0) * ($11) + 30.0d0 * ($12)) - 140.0d0 * ($13) + 420.0d0 * ($14)) - 1050.0d0 * ($10) + 378.0d0 * ($15) + 420.0d0 * ($16)) - 60.0d0 * ($17) + 5.0d0 * ($18))) - 168.0d0 * ($7) * ((((5.0d0 * ($11) - 48.0d0 * ($12) + 210.0d0 * ($13)) - 560.0d0 * ($14) + 1050.0d0 * ($10)) - 1680.0d0 * ($15) + 798.0d0 * ($16) + 240.0d0 * ($17)) - 15.0d0 * ($18)) + 32.0d0 * ($8) * (((((-15.0d0) * ($11) + 140.0d0 * ($12)) - 588.0d0 * ($13) + 1470.0d0 * ($14)) - 2450.0d0 * ($10) + 2940.0d0 * ($15)) - 2940.0d0 * ($16) + 1338.0d0 * ($17) + 105.0d0 * ($18))) - 3.0d0 * ($9) * ((((105.0d0 * ($11) - 960.0d0 * ($12) + 3920.0d0 * ($13)) - 9408.0d0 * ($14) + 14700.0d0 * ($10)) - 15680.0d0 * ($15) + 11760.0d0 * ($16)) - 6720.0d0 * ($17) + 2283.0d0 * ($18))))dnl
define(weno,($2) * ($6) + ($3) * ($7) + ($4) * ($8) + ($5) * ($9))dnl
define(Fii,0.5d0 * (($2) + ($3)) - 0.5d0 * MAX(($6), ($7)) * (($5) - ($4)))dnl
define(RK3P1,$3(($5)) + deltat * ($2))dnl
define(RK3P2,1.0d0 / 4.0d0 * (3.0d0 * $3(($5)) + $4(($5)) + deltat * ($2)))dnl
define(RK3P3,1.0d0 / 3.0d0 * ($3(($5)) + 2.0d0 * $4(($5)) + 2.0d0 * deltat * ($2)))dnl
define(FVMi,(($2) - ($3)) / deltax)dnl
define(lor1,((-1.0d0 / 4.0d0) * ($2) + 13.0d0 / 12.0d0 * ($3)) - 23.0d0 / 12.0d0 * ($4) + 25.0d0 / 12.0d0 * ($5))dnl
define(lor2,1.0d0 / 12.0d0 * ($2) - 5.0d0 / 12.0d0 * ($3) + 13.0d0 / 12.0d0 * ($4) + 1.0d0 / 4.0d0 * ($5))dnl
define(lor3,((-1.0d0 / 12.0d0) * ($2) + 7.0d0 / 12.0d0 * ($3) + 7.0d0 / 12.0d0 * ($4)) - 1.0d0 / 12.0d0 * ($5))dnl
define(lor4,(1.0d0 / 4.0d0 * ($2) + 13.0d0 / 12.0d0 * ($3)) - 5.0d0 / 12.0d0 * ($4) + 1.0d0 / 12.0d0 * ($5))dnl
define(smooth1,($2) * ((547.0d0 * ($2) - 3882.0d0 * ($3) + 4642.0d0 * ($4)) - 1854.0d0 * ($5)) + ($3) * (7043.0d0 * ($3) - 17246.0d0 * ($4) + 7042.0d0 * ($5)) + ($4) * (11003.0d0 * ($4) - 9402.0d0 * ($5)) + 2107.0d0 * ($5) * ($5))dnl
define(smooth2,($2) * ((267.0d0 * ($2) - 1642.0d0 * ($3) + 1602.0d0 * ($4)) - 494.0d0 * ($5)) + ($3) * (2843.0d0 * ($3) - 5966.0d0 * ($4) + 1922.0d0 * ($5)) + ($4) * (3443.0d0 * ($4) - 2522.0d0 * ($5)) + 547.0d0 * ($5) * ($5))dnl
define(smooth3,($2) * ((547.0d0 * ($2) - 2522.0d0 * ($3) + 1922.0d0 * ($4)) - 494.0d0 * ($5)) + ($3) * (3443.0d0 * ($3) - 5966.0d0 * ($4) + 1602.0d0 * ($5)) + ($4) * (2843.0d0 * ($4) - 1642.0d0 * ($5)) + 267.0d0 * ($5) * ($5))dnl
define(smooth4,($2) * ((2107.0d0 * ($2) - 9402.0d0 * ($3) + 7042.0d0 * ($4)) - 1854.0d0 * ($5)) + ($3) * (11003.0d0 * ($3) - 17246.0d0 * ($4) + 4642.0d0 * ($5)) + ($4) * (7043.0d0 * ($4) - 3882.0d0 * ($5)) + 547.0d0 * ($5) * ($5))dnl
define(nlw1,1.0d0 / (35.0d0 * ((0.000001 + ($2)) ** 2.0d0)))dnl
define(nlw2,12.0d0 / (35.0d0 * ((0.000001 + ($2)) ** 2.0d0)))dnl
define(nlw3,18.0d0 / (35.0d0 * ((0.000001 + ($2)) ** 2.0d0)))dnl
define(nlw4,4.0d0 / (35.0d0 * ((0.000001 + ($2)) ** 2.0d0)))dnl
define(FiphiCap_segmentI,($2))dnl

#include "cctk.h"
#include "cctk_Parameters.h"
#include "cctk_Arguments.h"
#include "cctk_Functions.h"

	subroutine AdvectionwithBalsaranegShu_execution(CCTK_ARGUMENTS)

		implicit none

		DECLARE_CCTK_ARGUMENTS
		DECLARE_CCTK_PARAMETERS
		DECLARE_CCTK_FUNCTIONS

!		Declare local variables
!		-----------------

		INTEGER i, iStart, iEnd
		INTEGER istat, status, ReflectionCounter
		CCTK_REAL p1lniphiCap_1, p2lniphiCap_1, p3lniphiCap_1, p4lniphiCap_1, p1rniphiCap_1, p2rniphiCap_1, p3rniphiCap_1, p4rniphiCap_1, beta1lniphiCap_1, beta2lniphiCap_1, beta3lniphiCap_1, beta4lniphiCap_1, beta1rniphiCap_1, beta2rniphiCap_1, beta3rniphiCap_1, beta4rniphiCap_1, w1lniphiCap_1, w2lniphiCap_1, w3lniphiCap_1, w4lniphiCap_1, w1rniphiCap_1, w2rniphiCap_1, w3rniphiCap_1, w4rniphiCap_1, wlniphiCap_1, wrniphiCap_1, p1lpiphiCap_1, p2lpiphiCap_1, p3lpiphiCap_1, p4lpiphiCap_1, p1rpiphiCap_1, p2rpiphiCap_1, p3rpiphiCap_1, p4rpiphiCap_1, beta1lpiphiCap_1, beta2lpiphiCap_1, beta3lpiphiCap_1, beta4lpiphiCap_1, beta1rpiphiCap_1, beta2rpiphiCap_1, beta3rpiphiCap_1, beta4rpiphiCap_1, w1lpiphiCap_1, w2lpiphiCap_1, w3lpiphiCap_1, w4lpiphiCap_1, w1rpiphiCap_1, w2rpiphiCap_1, w3rpiphiCap_1, w4rpiphiCap_1, wlpiphiCap_1, wrpiphiCap_1, SpeediphiCap_1, SpeedpiphiCap_1, SpeedniphiCap_1, FluxlpiphiCap_1, FluxlniphiCap_1, FluxrpiphiCap_1, FluxrniphiCap_1, FpiphiCap_1, FniphiCap_1, fluxAccphiCap_1


		CCTK_INT exitCond
		CCTK_REAL deltax
		CCTK_REAL deltat

		deltax = 2.0d0/(cctk_gsh(1) - 8)
		deltat = CCTK_DELTA_TIME

!		Finalization condition
		if (cctk_time .ge. tend) then 
			call CCTK_Exit(exitCond , cctkGH, 0)
		end if

!		Evolution
		iStart = 1
		iEnd = cctk_lsh(1)
		do i = iStart, iEnd
			if ((FOV_1(i) .gt. 0 .or. (((i + 1 .le. cctk_lsh(1) .and. FOV_1(i + 1) .gt. 0) .or. (i + 2 .le. cctk_lsh(1) .and. FOV_1(i + 2) .gt. 0) .or. (i + 3 .le. cctk_lsh(1) .and. FOV_1(i + 3) .gt. 0) .or. (i + 4 .le. cctk_lsh(1) .and. FOV_1(i + 4) .gt. 0)) .or. ((i - 1 .ge. 1 .and. FOV_1(i - 1) .gt. 0) .or. (i - 2 .ge. 1 .and. FOV_1(i - 2) .gt. 0) .or. (i - 3 .ge. 1 .and. FOV_1(i - 3) .gt. 0) .or. (i - 4 .ge. 1 .and. FOV_1(i - 4) .gt. 0))))) then
				used_1(i) = 0.0d0
			end if
		end do
		do i = iStart, iEnd
			if ((FOV_1(i) .gt. 0) .and. (i + 4 .le. cctk_lsh(1) .and. i - 4 .ge. 1)) then
				used_1(i) = 1.0d0
				if (used_1(i - 1) .eq. 0.0d0) then
					p1lniphiCap_1 = lor1(CCTK_PASS_FTOF, phiCap_p(i - 4), phiCap_p(i - 3), phiCap_p(i - 2), phiCap_p(i - 1))
					p2lniphiCap_1 = lor2(CCTK_PASS_FTOF, phiCap_p(i - 3), phiCap_p(i - 2), phiCap_p(i - 1), phiCap_p(i))
					p3lniphiCap_1 = lor3(CCTK_PASS_FTOF, phiCap_p(i - 2), phiCap_p(i - 1), phiCap_p(i), phiCap_p(i + 1))
					p4lniphiCap_1 = lor4(CCTK_PASS_FTOF, phiCap_p(i - 1), phiCap_p(i), phiCap_p(i + 1), phiCap_p(i + 2))
					p1rniphiCap_1 = lor1(CCTK_PASS_FTOF, phiCap_p(i + 3), phiCap_p(i + 2), phiCap_p(i + 1), phiCap_p(i))
					p2rniphiCap_1 = lor2(CCTK_PASS_FTOF, phiCap_p(i + 2), phiCap_p(i + 1), phiCap_p(i), phiCap_p(i - 1))
					p3rniphiCap_1 = lor3(CCTK_PASS_FTOF, phiCap_p(i + 1), phiCap_p(i), phiCap_p(i - 1), phiCap_p(i - 2))
					p4rniphiCap_1 = lor4(CCTK_PASS_FTOF, phiCap_p(i), phiCap_p(i - 1), phiCap_p(i - 2), phiCap_p(i - 3))
					beta1lniphiCap_1 = smooth1(CCTK_PASS_FTOF, phiCap_p(i - 4), phiCap_p(i - 3), phiCap_p(i - 2), phiCap_p(i - 1))
					beta2lniphiCap_1 = smooth2(CCTK_PASS_FTOF, phiCap_p(i - 3), phiCap_p(i - 2), phiCap_p(i - 1), phiCap_p(i))
					beta3lniphiCap_1 = smooth3(CCTK_PASS_FTOF, phiCap_p(i - 2), phiCap_p(i - 1), phiCap_p(i), phiCap_p(i + 1))
					beta4lniphiCap_1 = smooth4(CCTK_PASS_FTOF, phiCap_p(i - 1), phiCap_p(i), phiCap_p(i + 1), phiCap_p(i + 2))
					beta1rniphiCap_1 = smooth1(CCTK_PASS_FTOF, phiCap_p(i + 3), phiCap_p(i + 2), phiCap_p(i + 1), phiCap_p(i))
					beta2rniphiCap_1 = smooth2(CCTK_PASS_FTOF, phiCap_p(i + 2), phiCap_p(i + 1), phiCap_p(i), phiCap_p(i - 1))
					beta3rniphiCap_1 = smooth3(CCTK_PASS_FTOF, phiCap_p(i + 1), phiCap_p(i), phiCap_p(i - 1), phiCap_p(i - 2))
					beta4rniphiCap_1 = smooth4(CCTK_PASS_FTOF, phiCap_p(i), phiCap_p(i - 1), phiCap_p(i - 2), phiCap_p(i - 3))
					w1lniphiCap_1 = nlw1(CCTK_PASS_FTOF, beta1lniphiCap_1)
					w2lniphiCap_1 = nlw2(CCTK_PASS_FTOF, beta2lniphiCap_1)
					w3lniphiCap_1 = nlw3(CCTK_PASS_FTOF, beta3lniphiCap_1)
					w4lniphiCap_1 = nlw4(CCTK_PASS_FTOF, beta4lniphiCap_1)
					w1rniphiCap_1 = nlw1(CCTK_PASS_FTOF, beta1rniphiCap_1)
					w2rniphiCap_1 = nlw2(CCTK_PASS_FTOF, beta2rniphiCap_1)
					w3rniphiCap_1 = nlw3(CCTK_PASS_FTOF, beta3rniphiCap_1)
					w4rniphiCap_1 = nlw4(CCTK_PASS_FTOF, beta4rniphiCap_1)
					wlniphiCap_1 = w1lniphiCap_1 + w2lniphiCap_1 + w3lniphiCap_1 + w4lniphiCap_1
					wrniphiCap_1 = w1rniphiCap_1 + w2rniphiCap_1 + w3rniphiCap_1 + w4rniphiCap_1
					w1lniphiCap_1 = w1lniphiCap_1 / wlniphiCap_1
					w2lniphiCap_1 = w2lniphiCap_1 / wlniphiCap_1
					w3lniphiCap_1 = w3lniphiCap_1 / wlniphiCap_1
					w4lniphiCap_1 = w4lniphiCap_1 / wlniphiCap_1
					w1rniphiCap_1 = w1rniphiCap_1 / wrniphiCap_1
					w2rniphiCap_1 = w2rniphiCap_1 / wrniphiCap_1
					w3rniphiCap_1 = w3rniphiCap_1 / wrniphiCap_1
					w4rniphiCap_1 = w4rniphiCap_1 / wrniphiCap_1
					phiCap_lni_1(i) = weno(CCTK_PASS_FTOF, w1lniphiCap_1, w2lniphiCap_1, w3lniphiCap_1, w4lniphiCap_1, p1lniphiCap_1, p2lniphiCap_1, p3lniphiCap_1, p4lniphiCap_1)
					phiCap_rni_1(i) = weno(CCTK_PASS_FTOF, w1rniphiCap_1, w2rniphiCap_1, w3rniphiCap_1, w4rniphiCap_1, p1rniphiCap_1, p2rniphiCap_1, p3rniphiCap_1, p4rniphiCap_1)
				else
					phiCap_lni_1(i) = phiCap_lpi_1(i - 1)
					phiCap_rni_1(i) = phiCap_rpi_1(i - 1)
				end if
				p1lpiphiCap_1 = lor1(CCTK_PASS_FTOF, phiCap_p(i - 3), phiCap_p(i - 2), phiCap_p(i - 1), phiCap_p(i))
				p2lpiphiCap_1 = lor2(CCTK_PASS_FTOF, phiCap_p(i - 2), phiCap_p(i - 1), phiCap_p(i), phiCap_p(i + 1))
				p3lpiphiCap_1 = lor3(CCTK_PASS_FTOF, phiCap_p(i - 1), phiCap_p(i), phiCap_p(i + 1), phiCap_p(i + 2))
				p4lpiphiCap_1 = lor4(CCTK_PASS_FTOF, phiCap_p(i), phiCap_p(i + 1), phiCap_p(i + 2), phiCap_p(i + 3))
				p1rpiphiCap_1 = lor1(CCTK_PASS_FTOF, phiCap_p(i + 4), phiCap_p(i + 3), phiCap_p(i + 2), phiCap_p(i + 1))
				p2rpiphiCap_1 = lor2(CCTK_PASS_FTOF, phiCap_p(i + 3), phiCap_p(i + 2), phiCap_p(i + 1), phiCap_p(i))
				p3rpiphiCap_1 = lor3(CCTK_PASS_FTOF, phiCap_p(i + 2), phiCap_p(i + 1), phiCap_p(i), phiCap_p(i - 1))
				p4rpiphiCap_1 = lor4(CCTK_PASS_FTOF, phiCap_p(i + 1), phiCap_p(i), phiCap_p(i - 1), phiCap_p(i - 2))
				beta1lpiphiCap_1 = smooth1(CCTK_PASS_FTOF, phiCap_p(i - 3), phiCap_p(i - 2), phiCap_p(i - 1), phiCap_p(i))
				beta2lpiphiCap_1 = smooth2(CCTK_PASS_FTOF, phiCap_p(i - 2), phiCap_p(i - 1), phiCap_p(i), phiCap_p(i + 1))
				beta3lpiphiCap_1 = smooth3(CCTK_PASS_FTOF, phiCap_p(i - 1), phiCap_p(i), phiCap_p(i + 1), phiCap_p(i + 2))
				beta4lpiphiCap_1 = smooth4(CCTK_PASS_FTOF, phiCap_p(i), phiCap_p(i + 1), phiCap_p(i + 2), phiCap_p(i + 3))
				beta1rpiphiCap_1 = smooth1(CCTK_PASS_FTOF, phiCap_p(i + 4), phiCap_p(i + 3), phiCap_p(i + 2), phiCap_p(i + 1))
				beta2rpiphiCap_1 = smooth2(CCTK_PASS_FTOF, phiCap_p(i + 3), phiCap_p(i + 2), phiCap_p(i + 1), phiCap_p(i))
				beta3rpiphiCap_1 = smooth3(CCTK_PASS_FTOF, phiCap_p(i + 2), phiCap_p(i + 1), phiCap_p(i), phiCap_p(i - 1))
				beta4rpiphiCap_1 = smooth4(CCTK_PASS_FTOF, phiCap_p(i + 1), phiCap_p(i), phiCap_p(i - 1), phiCap_p(i - 2))
				w1lpiphiCap_1 = nlw1(CCTK_PASS_FTOF, beta1lpiphiCap_1)
				w2lpiphiCap_1 = nlw2(CCTK_PASS_FTOF, beta2lpiphiCap_1)
				w3lpiphiCap_1 = nlw3(CCTK_PASS_FTOF, beta3lpiphiCap_1)
				w4lpiphiCap_1 = nlw4(CCTK_PASS_FTOF, beta4lpiphiCap_1)
				w1rpiphiCap_1 = nlw1(CCTK_PASS_FTOF, beta1rpiphiCap_1)
				w2rpiphiCap_1 = nlw2(CCTK_PASS_FTOF, beta2rpiphiCap_1)
				w3rpiphiCap_1 = nlw3(CCTK_PASS_FTOF, beta3rpiphiCap_1)
				w4rpiphiCap_1 = nlw4(CCTK_PASS_FTOF, beta4rpiphiCap_1)
				wlpiphiCap_1 = w1lpiphiCap_1 + w2lpiphiCap_1 + w3lpiphiCap_1 + w4lpiphiCap_1
				wrpiphiCap_1 = w1rpiphiCap_1 + w2rpiphiCap_1 + w3rpiphiCap_1 + w4rpiphiCap_1
				w1lpiphiCap_1 = w1lpiphiCap_1 / wlpiphiCap_1
				w2lpiphiCap_1 = w2lpiphiCap_1 / wlpiphiCap_1
				w3lpiphiCap_1 = w3lpiphiCap_1 / wlpiphiCap_1
				w4lpiphiCap_1 = w4lpiphiCap_1 / wlpiphiCap_1
				w1rpiphiCap_1 = w1rpiphiCap_1 / wrpiphiCap_1
				w2rpiphiCap_1 = w2rpiphiCap_1 / wrpiphiCap_1
				w3rpiphiCap_1 = w3rpiphiCap_1 / wrpiphiCap_1
				w4rpiphiCap_1 = w4rpiphiCap_1 / wrpiphiCap_1
				phiCap_lpi_1(i) = weno(CCTK_PASS_FTOF, w1lpiphiCap_1, w2lpiphiCap_1, w3lpiphiCap_1, w4lpiphiCap_1, p1lpiphiCap_1, p2lpiphiCap_1, p3lpiphiCap_1, p4lpiphiCap_1)
				phiCap_rpi_1(i) = weno(CCTK_PASS_FTOF, w1rpiphiCap_1, w2rpiphiCap_1, w3rpiphiCap_1, w4rpiphiCap_1, p1rpiphiCap_1, p2rpiphiCap_1, p3rpiphiCap_1, p4rpiphiCap_1)
				SpeediphiCap_1 = ABS(1.0d0)
				SpeedpiphiCap_1 = ABS(1.0d0)
				SpeedniphiCap_1 = ABS(1.0d0)
				FluxlpiphiCap_1 = FiphiCap_segmentI(CCTK_PASS_FTOF, phiCap_lpi_1(i))
				FluxlniphiCap_1 = FiphiCap_segmentI(CCTK_PASS_FTOF, phiCap_lni_1(i))
				FluxrpiphiCap_1 = FiphiCap_segmentI(CCTK_PASS_FTOF, phiCap_rpi_1(i))
				FluxrniphiCap_1 = FiphiCap_segmentI(CCTK_PASS_FTOF, phiCap_rni_1(i))
				FpiphiCap_1 = Fii(CCTK_PASS_FTOF, FluxlpiphiCap_1, FluxrpiphiCap_1, phiCap_lpi_1(i), phiCap_rpi_1(i), SpeediphiCap_1, SpeedpiphiCap_1)
				FniphiCap_1 = Fii(CCTK_PASS_FTOF, FluxlniphiCap_1, FluxrniphiCap_1, phiCap_lni_1(i), phiCap_rni_1(i), SpeediphiCap_1, SpeedniphiCap_1)
				fluxAccphiCap_1 = 0.0d0 + 0.0d0
				fluxAccphiCap_1 = fluxAccphiCap_1 - FVMi(CCTK_PASS_FTOF, FpiphiCap_1, FniphiCap_1)
				k1phiCap(i) = RK3P1(CCTK_PASS_FTOF, fluxAccphiCap_1, phiCap_p, 0, i)
			end if
		end do
		call CCTK_SyncGroup(status, cctkGH, "AdvectionwithBalsaranegShu::block1")
		call CCTK_SyncGroup(status, cctkGH, "AdvectionwithBalsaranegShu::k1phiCapGroup")
		do i = iStart, iEnd
			if ((FOV_1(i) .gt. 0 .or. (((i + 1 .le. cctk_lsh(1) .and. FOV_1(i + 1) .gt. 0) .or. (i + 2 .le. cctk_lsh(1) .and. FOV_1(i + 2) .gt. 0) .or. (i + 3 .le. cctk_lsh(1) .and. FOV_1(i + 3) .gt. 0) .or. (i + 4 .le. cctk_lsh(1) .and. FOV_1(i + 4) .gt. 0)) .or. ((i - 1 .ge. 1 .and. FOV_1(i - 1) .gt. 0) .or. (i - 2 .ge. 1 .and. FOV_1(i - 2) .gt. 0) .or. (i - 3 .ge. 1 .and. FOV_1(i - 3) .gt. 0) .or. (i - 4 .ge. 1 .and. FOV_1(i - 4) .gt. 0))))) then
				used_1(i) = 0.0d0
			end if
		end do
		do i = iStart, iEnd
			if ((FOV_1(i) .gt. 0) .and. (i + 4 .le. cctk_lsh(1) .and. i - 4 .ge. 1)) then
				used_1(i) = 1.0d0
				if (used_1(i - 1) .eq. 0.0d0) then
					p1lniphiCap_1 = lor1(CCTK_PASS_FTOF, k1phiCap(i - 4), k1phiCap(i - 3), k1phiCap(i - 2), k1phiCap(i - 1))
					p2lniphiCap_1 = lor2(CCTK_PASS_FTOF, k1phiCap(i - 3), k1phiCap(i - 2), k1phiCap(i - 1), k1phiCap(i))
					p3lniphiCap_1 = lor3(CCTK_PASS_FTOF, k1phiCap(i - 2), k1phiCap(i - 1), k1phiCap(i), k1phiCap(i + 1))
					p4lniphiCap_1 = lor4(CCTK_PASS_FTOF, k1phiCap(i - 1), k1phiCap(i), k1phiCap(i + 1), k1phiCap(i + 2))
					p1rniphiCap_1 = lor1(CCTK_PASS_FTOF, k1phiCap(i + 3), k1phiCap(i + 2), k1phiCap(i + 1), k1phiCap(i))
					p2rniphiCap_1 = lor2(CCTK_PASS_FTOF, k1phiCap(i + 2), k1phiCap(i + 1), k1phiCap(i), k1phiCap(i - 1))
					p3rniphiCap_1 = lor3(CCTK_PASS_FTOF, k1phiCap(i + 1), k1phiCap(i), k1phiCap(i - 1), k1phiCap(i - 2))
					p4rniphiCap_1 = lor4(CCTK_PASS_FTOF, k1phiCap(i), k1phiCap(i - 1), k1phiCap(i - 2), k1phiCap(i - 3))
					beta1lniphiCap_1 = smooth1(CCTK_PASS_FTOF, k1phiCap(i - 4), k1phiCap(i - 3), k1phiCap(i - 2), k1phiCap(i - 1))
					beta2lniphiCap_1 = smooth2(CCTK_PASS_FTOF, k1phiCap(i - 3), k1phiCap(i - 2), k1phiCap(i - 1), k1phiCap(i))
					beta3lniphiCap_1 = smooth3(CCTK_PASS_FTOF, k1phiCap(i - 2), k1phiCap(i - 1), k1phiCap(i), k1phiCap(i + 1))
					beta4lniphiCap_1 = smooth4(CCTK_PASS_FTOF, k1phiCap(i - 1), k1phiCap(i), k1phiCap(i + 1), k1phiCap(i + 2))
					beta1rniphiCap_1 = smooth1(CCTK_PASS_FTOF, k1phiCap(i + 3), k1phiCap(i + 2), k1phiCap(i + 1), k1phiCap(i))
					beta2rniphiCap_1 = smooth2(CCTK_PASS_FTOF, k1phiCap(i + 2), k1phiCap(i + 1), k1phiCap(i), k1phiCap(i - 1))
					beta3rniphiCap_1 = smooth3(CCTK_PASS_FTOF, k1phiCap(i + 1), k1phiCap(i), k1phiCap(i - 1), k1phiCap(i - 2))
					beta4rniphiCap_1 = smooth4(CCTK_PASS_FTOF, k1phiCap(i), k1phiCap(i - 1), k1phiCap(i - 2), k1phiCap(i - 3))
					w1lniphiCap_1 = nlw1(CCTK_PASS_FTOF, beta1lniphiCap_1)
					w2lniphiCap_1 = nlw2(CCTK_PASS_FTOF, beta2lniphiCap_1)
					w3lniphiCap_1 = nlw3(CCTK_PASS_FTOF, beta3lniphiCap_1)
					w4lniphiCap_1 = nlw4(CCTK_PASS_FTOF, beta4lniphiCap_1)
					w1rniphiCap_1 = nlw1(CCTK_PASS_FTOF, beta1rniphiCap_1)
					w2rniphiCap_1 = nlw2(CCTK_PASS_FTOF, beta2rniphiCap_1)
					w3rniphiCap_1 = nlw3(CCTK_PASS_FTOF, beta3rniphiCap_1)
					w4rniphiCap_1 = nlw4(CCTK_PASS_FTOF, beta4rniphiCap_1)
					wlniphiCap_1 = w1lniphiCap_1 + w2lniphiCap_1 + w3lniphiCap_1 + w4lniphiCap_1
					wrniphiCap_1 = w1rniphiCap_1 + w2rniphiCap_1 + w3rniphiCap_1 + w4rniphiCap_1
					w1lniphiCap_1 = w1lniphiCap_1 / wlniphiCap_1
					w2lniphiCap_1 = w2lniphiCap_1 / wlniphiCap_1
					w3lniphiCap_1 = w3lniphiCap_1 / wlniphiCap_1
					w4lniphiCap_1 = w4lniphiCap_1 / wlniphiCap_1
					w1rniphiCap_1 = w1rniphiCap_1 / wrniphiCap_1
					w2rniphiCap_1 = w2rniphiCap_1 / wrniphiCap_1
					w3rniphiCap_1 = w3rniphiCap_1 / wrniphiCap_1
					w4rniphiCap_1 = w4rniphiCap_1 / wrniphiCap_1
					phiCap_lni_1(i) = weno(CCTK_PASS_FTOF, w1lniphiCap_1, w2lniphiCap_1, w3lniphiCap_1, w4lniphiCap_1, p1lniphiCap_1, p2lniphiCap_1, p3lniphiCap_1, p4lniphiCap_1)
					phiCap_rni_1(i) = weno(CCTK_PASS_FTOF, w1rniphiCap_1, w2rniphiCap_1, w3rniphiCap_1, w4rniphiCap_1, p1rniphiCap_1, p2rniphiCap_1, p3rniphiCap_1, p4rniphiCap_1)
				else
					phiCap_lni_1(i) = phiCap_lpi_1(i - 1)
					phiCap_rni_1(i) = phiCap_rpi_1(i - 1)
				end if
				p1lpiphiCap_1 = lor1(CCTK_PASS_FTOF, k1phiCap(i - 3), k1phiCap(i - 2), k1phiCap(i - 1), k1phiCap(i))
				p2lpiphiCap_1 = lor2(CCTK_PASS_FTOF, k1phiCap(i - 2), k1phiCap(i - 1), k1phiCap(i), k1phiCap(i + 1))
				p3lpiphiCap_1 = lor3(CCTK_PASS_FTOF, k1phiCap(i - 1), k1phiCap(i), k1phiCap(i + 1), k1phiCap(i + 2))
				p4lpiphiCap_1 = lor4(CCTK_PASS_FTOF, k1phiCap(i), k1phiCap(i + 1), k1phiCap(i + 2), k1phiCap(i + 3))
				p1rpiphiCap_1 = lor1(CCTK_PASS_FTOF, k1phiCap(i + 4), k1phiCap(i + 3), k1phiCap(i + 2), k1phiCap(i + 1))
				p2rpiphiCap_1 = lor2(CCTK_PASS_FTOF, k1phiCap(i + 3), k1phiCap(i + 2), k1phiCap(i + 1), k1phiCap(i))
				p3rpiphiCap_1 = lor3(CCTK_PASS_FTOF, k1phiCap(i + 2), k1phiCap(i + 1), k1phiCap(i), k1phiCap(i - 1))
				p4rpiphiCap_1 = lor4(CCTK_PASS_FTOF, k1phiCap(i + 1), k1phiCap(i), k1phiCap(i - 1), k1phiCap(i - 2))
				beta1lpiphiCap_1 = smooth1(CCTK_PASS_FTOF, k1phiCap(i - 3), k1phiCap(i - 2), k1phiCap(i - 1), k1phiCap(i))
				beta2lpiphiCap_1 = smooth2(CCTK_PASS_FTOF, k1phiCap(i - 2), k1phiCap(i - 1), k1phiCap(i), k1phiCap(i + 1))
				beta3lpiphiCap_1 = smooth3(CCTK_PASS_FTOF, k1phiCap(i - 1), k1phiCap(i), k1phiCap(i + 1), k1phiCap(i + 2))
				beta4lpiphiCap_1 = smooth4(CCTK_PASS_FTOF, k1phiCap(i), k1phiCap(i + 1), k1phiCap(i + 2), k1phiCap(i + 3))
				beta1rpiphiCap_1 = smooth1(CCTK_PASS_FTOF, k1phiCap(i + 4), k1phiCap(i + 3), k1phiCap(i + 2), k1phiCap(i + 1))
				beta2rpiphiCap_1 = smooth2(CCTK_PASS_FTOF, k1phiCap(i + 3), k1phiCap(i + 2), k1phiCap(i + 1), k1phiCap(i))
				beta3rpiphiCap_1 = smooth3(CCTK_PASS_FTOF, k1phiCap(i + 2), k1phiCap(i + 1), k1phiCap(i), k1phiCap(i - 1))
				beta4rpiphiCap_1 = smooth4(CCTK_PASS_FTOF, k1phiCap(i + 1), k1phiCap(i), k1phiCap(i - 1), k1phiCap(i - 2))
				w1lpiphiCap_1 = nlw1(CCTK_PASS_FTOF, beta1lpiphiCap_1)
				w2lpiphiCap_1 = nlw2(CCTK_PASS_FTOF, beta2lpiphiCap_1)
				w3lpiphiCap_1 = nlw3(CCTK_PASS_FTOF, beta3lpiphiCap_1)
				w4lpiphiCap_1 = nlw4(CCTK_PASS_FTOF, beta4lpiphiCap_1)
				w1rpiphiCap_1 = nlw1(CCTK_PASS_FTOF, beta1rpiphiCap_1)
				w2rpiphiCap_1 = nlw2(CCTK_PASS_FTOF, beta2rpiphiCap_1)
				w3rpiphiCap_1 = nlw3(CCTK_PASS_FTOF, beta3rpiphiCap_1)
				w4rpiphiCap_1 = nlw4(CCTK_PASS_FTOF, beta4rpiphiCap_1)
				wlpiphiCap_1 = w1lpiphiCap_1 + w2lpiphiCap_1 + w3lpiphiCap_1 + w4lpiphiCap_1
				wrpiphiCap_1 = w1rpiphiCap_1 + w2rpiphiCap_1 + w3rpiphiCap_1 + w4rpiphiCap_1
				w1lpiphiCap_1 = w1lpiphiCap_1 / wlpiphiCap_1
				w2lpiphiCap_1 = w2lpiphiCap_1 / wlpiphiCap_1
				w3lpiphiCap_1 = w3lpiphiCap_1 / wlpiphiCap_1
				w4lpiphiCap_1 = w4lpiphiCap_1 / wlpiphiCap_1
				w1rpiphiCap_1 = w1rpiphiCap_1 / wrpiphiCap_1
				w2rpiphiCap_1 = w2rpiphiCap_1 / wrpiphiCap_1
				w3rpiphiCap_1 = w3rpiphiCap_1 / wrpiphiCap_1
				w4rpiphiCap_1 = w4rpiphiCap_1 / wrpiphiCap_1
				phiCap_lpi_1(i) = weno(CCTK_PASS_FTOF, w1lpiphiCap_1, w2lpiphiCap_1, w3lpiphiCap_1, w4lpiphiCap_1, p1lpiphiCap_1, p2lpiphiCap_1, p3lpiphiCap_1, p4lpiphiCap_1)
				phiCap_rpi_1(i) = weno(CCTK_PASS_FTOF, w1rpiphiCap_1, w2rpiphiCap_1, w3rpiphiCap_1, w4rpiphiCap_1, p1rpiphiCap_1, p2rpiphiCap_1, p3rpiphiCap_1, p4rpiphiCap_1)
				SpeediphiCap_1 = ABS(1.0d0)
				SpeedpiphiCap_1 = ABS(1.0d0)
				SpeedniphiCap_1 = ABS(1.0d0)
				FluxlpiphiCap_1 = FiphiCap_segmentI(CCTK_PASS_FTOF, phiCap_lpi_1(i))
				FluxlniphiCap_1 = FiphiCap_segmentI(CCTK_PASS_FTOF, phiCap_lni_1(i))
				FluxrpiphiCap_1 = FiphiCap_segmentI(CCTK_PASS_FTOF, phiCap_rpi_1(i))
				FluxrniphiCap_1 = FiphiCap_segmentI(CCTK_PASS_FTOF, phiCap_rni_1(i))
				FpiphiCap_1 = Fii(CCTK_PASS_FTOF, FluxlpiphiCap_1, FluxrpiphiCap_1, phiCap_lpi_1(i), phiCap_rpi_1(i), SpeediphiCap_1, SpeedpiphiCap_1)
				FniphiCap_1 = Fii(CCTK_PASS_FTOF, FluxlniphiCap_1, FluxrniphiCap_1, phiCap_lni_1(i), phiCap_rni_1(i), SpeediphiCap_1, SpeedniphiCap_1)
				fluxAccphiCap_1 = 0.0d0 + 0.0d0
				fluxAccphiCap_1 = fluxAccphiCap_1 - FVMi(CCTK_PASS_FTOF, FpiphiCap_1, FniphiCap_1)
				k2phiCap(i) = RK3P2(CCTK_PASS_FTOF, fluxAccphiCap_1, phiCap_p, k1phiCap, i)
			end if
		end do
		call CCTK_SyncGroup(status, cctkGH, "AdvectionwithBalsaranegShu::k2phiCapGroup")
		call CCTK_SyncGroup(status, cctkGH, "AdvectionwithBalsaranegShu::block1")
		do i = iStart, iEnd
			if ((FOV_1(i) .gt. 0 .or. (((i + 1 .le. cctk_lsh(1) .and. FOV_1(i + 1) .gt. 0) .or. (i + 2 .le. cctk_lsh(1) .and. FOV_1(i + 2) .gt. 0) .or. (i + 3 .le. cctk_lsh(1) .and. FOV_1(i + 3) .gt. 0) .or. (i + 4 .le. cctk_lsh(1) .and. FOV_1(i + 4) .gt. 0)) .or. ((i - 1 .ge. 1 .and. FOV_1(i - 1) .gt. 0) .or. (i - 2 .ge. 1 .and. FOV_1(i - 2) .gt. 0) .or. (i - 3 .ge. 1 .and. FOV_1(i - 3) .gt. 0) .or. (i - 4 .ge. 1 .and. FOV_1(i - 4) .gt. 0))))) then
				used_1(i) = 0.0d0
			end if
		end do
		do i = iStart, iEnd
			if ((FOV_1(i) .gt. 0) .and. (i + 4 .le. cctk_lsh(1) .and. i - 4 .ge. 1)) then
				used_1(i) = 1.0d0
				if (used_1(i - 1) .eq. 0.0d0) then
					p1lniphiCap_1 = lor1(CCTK_PASS_FTOF, k2phiCap(i - 4), k2phiCap(i - 3), k2phiCap(i - 2), k2phiCap(i - 1))
					p2lniphiCap_1 = lor2(CCTK_PASS_FTOF, k2phiCap(i - 3), k2phiCap(i - 2), k2phiCap(i - 1), k2phiCap(i))
					p3lniphiCap_1 = lor3(CCTK_PASS_FTOF, k2phiCap(i - 2), k2phiCap(i - 1), k2phiCap(i), k2phiCap(i + 1))
					p4lniphiCap_1 = lor4(CCTK_PASS_FTOF, k2phiCap(i - 1), k2phiCap(i), k2phiCap(i + 1), k2phiCap(i + 2))
					p1rniphiCap_1 = lor1(CCTK_PASS_FTOF, k2phiCap(i + 3), k2phiCap(i + 2), k2phiCap(i + 1), k2phiCap(i))
					p2rniphiCap_1 = lor2(CCTK_PASS_FTOF, k2phiCap(i + 2), k2phiCap(i + 1), k2phiCap(i), k2phiCap(i - 1))
					p3rniphiCap_1 = lor3(CCTK_PASS_FTOF, k2phiCap(i + 1), k2phiCap(i), k2phiCap(i - 1), k2phiCap(i - 2))
					p4rniphiCap_1 = lor4(CCTK_PASS_FTOF, k2phiCap(i), k2phiCap(i - 1), k2phiCap(i - 2), k2phiCap(i - 3))
					beta1lniphiCap_1 = smooth1(CCTK_PASS_FTOF, k2phiCap(i - 4), k2phiCap(i - 3), k2phiCap(i - 2), k2phiCap(i - 1))
					beta2lniphiCap_1 = smooth2(CCTK_PASS_FTOF, k2phiCap(i - 3), k2phiCap(i - 2), k2phiCap(i - 1), k2phiCap(i))
					beta3lniphiCap_1 = smooth3(CCTK_PASS_FTOF, k2phiCap(i - 2), k2phiCap(i - 1), k2phiCap(i), k2phiCap(i + 1))
					beta4lniphiCap_1 = smooth4(CCTK_PASS_FTOF, k2phiCap(i - 1), k2phiCap(i), k2phiCap(i + 1), k2phiCap(i + 2))
					beta1rniphiCap_1 = smooth1(CCTK_PASS_FTOF, k2phiCap(i + 3), k2phiCap(i + 2), k2phiCap(i + 1), k2phiCap(i))
					beta2rniphiCap_1 = smooth2(CCTK_PASS_FTOF, k2phiCap(i + 2), k2phiCap(i + 1), k2phiCap(i), k2phiCap(i - 1))
					beta3rniphiCap_1 = smooth3(CCTK_PASS_FTOF, k2phiCap(i + 1), k2phiCap(i), k2phiCap(i - 1), k2phiCap(i - 2))
					beta4rniphiCap_1 = smooth4(CCTK_PASS_FTOF, k2phiCap(i), k2phiCap(i - 1), k2phiCap(i - 2), k2phiCap(i - 3))
					w1lniphiCap_1 = nlw1(CCTK_PASS_FTOF, beta1lniphiCap_1)
					w2lniphiCap_1 = nlw2(CCTK_PASS_FTOF, beta2lniphiCap_1)
					w3lniphiCap_1 = nlw3(CCTK_PASS_FTOF, beta3lniphiCap_1)
					w4lniphiCap_1 = nlw4(CCTK_PASS_FTOF, beta4lniphiCap_1)
					w1rniphiCap_1 = nlw1(CCTK_PASS_FTOF, beta1rniphiCap_1)
					w2rniphiCap_1 = nlw2(CCTK_PASS_FTOF, beta2rniphiCap_1)
					w3rniphiCap_1 = nlw3(CCTK_PASS_FTOF, beta3rniphiCap_1)
					w4rniphiCap_1 = nlw4(CCTK_PASS_FTOF, beta4rniphiCap_1)
					wlniphiCap_1 = w1lniphiCap_1 + w2lniphiCap_1 + w3lniphiCap_1 + w4lniphiCap_1
					wrniphiCap_1 = w1rniphiCap_1 + w2rniphiCap_1 + w3rniphiCap_1 + w4rniphiCap_1
					w1lniphiCap_1 = w1lniphiCap_1 / wlniphiCap_1
					w2lniphiCap_1 = w2lniphiCap_1 / wlniphiCap_1
					w3lniphiCap_1 = w3lniphiCap_1 / wlniphiCap_1
					w4lniphiCap_1 = w4lniphiCap_1 / wlniphiCap_1
					w1rniphiCap_1 = w1rniphiCap_1 / wrniphiCap_1
					w2rniphiCap_1 = w2rniphiCap_1 / wrniphiCap_1
					w3rniphiCap_1 = w3rniphiCap_1 / wrniphiCap_1
					w4rniphiCap_1 = w4rniphiCap_1 / wrniphiCap_1
					phiCap_lni_1(i) = weno(CCTK_PASS_FTOF, w1lniphiCap_1, w2lniphiCap_1, w3lniphiCap_1, w4lniphiCap_1, p1lniphiCap_1, p2lniphiCap_1, p3lniphiCap_1, p4lniphiCap_1)
					phiCap_rni_1(i) = weno(CCTK_PASS_FTOF, w1rniphiCap_1, w2rniphiCap_1, w3rniphiCap_1, w4rniphiCap_1, p1rniphiCap_1, p2rniphiCap_1, p3rniphiCap_1, p4rniphiCap_1)
				else
					phiCap_lni_1(i) = phiCap_lpi_1(i - 1)
					phiCap_rni_1(i) = phiCap_rpi_1(i - 1)
				end if
				p1lpiphiCap_1 = lor1(CCTK_PASS_FTOF, k2phiCap(i - 3), k2phiCap(i - 2), k2phiCap(i - 1), k2phiCap(i))
				p2lpiphiCap_1 = lor2(CCTK_PASS_FTOF, k2phiCap(i - 2), k2phiCap(i - 1), k2phiCap(i), k2phiCap(i + 1))
				p3lpiphiCap_1 = lor3(CCTK_PASS_FTOF, k2phiCap(i - 1), k2phiCap(i), k2phiCap(i + 1), k2phiCap(i + 2))
				p4lpiphiCap_1 = lor4(CCTK_PASS_FTOF, k2phiCap(i), k2phiCap(i + 1), k2phiCap(i + 2), k2phiCap(i + 3))
				p1rpiphiCap_1 = lor1(CCTK_PASS_FTOF, k2phiCap(i + 4), k2phiCap(i + 3), k2phiCap(i + 2), k2phiCap(i + 1))
				p2rpiphiCap_1 = lor2(CCTK_PASS_FTOF, k2phiCap(i + 3), k2phiCap(i + 2), k2phiCap(i + 1), k2phiCap(i))
				p3rpiphiCap_1 = lor3(CCTK_PASS_FTOF, k2phiCap(i + 2), k2phiCap(i + 1), k2phiCap(i), k2phiCap(i - 1))
				p4rpiphiCap_1 = lor4(CCTK_PASS_FTOF, k2phiCap(i + 1), k2phiCap(i), k2phiCap(i - 1), k2phiCap(i - 2))
				beta1lpiphiCap_1 = smooth1(CCTK_PASS_FTOF, k2phiCap(i - 3), k2phiCap(i - 2), k2phiCap(i - 1), k2phiCap(i))
				beta2lpiphiCap_1 = smooth2(CCTK_PASS_FTOF, k2phiCap(i - 2), k2phiCap(i - 1), k2phiCap(i), k2phiCap(i + 1))
				beta3lpiphiCap_1 = smooth3(CCTK_PASS_FTOF, k2phiCap(i - 1), k2phiCap(i), k2phiCap(i + 1), k2phiCap(i + 2))
				beta4lpiphiCap_1 = smooth4(CCTK_PASS_FTOF, k2phiCap(i), k2phiCap(i + 1), k2phiCap(i + 2), k2phiCap(i + 3))
				beta1rpiphiCap_1 = smooth1(CCTK_PASS_FTOF, k2phiCap(i + 4), k2phiCap(i + 3), k2phiCap(i + 2), k2phiCap(i + 1))
				beta2rpiphiCap_1 = smooth2(CCTK_PASS_FTOF, k2phiCap(i + 3), k2phiCap(i + 2), k2phiCap(i + 1), k2phiCap(i))
				beta3rpiphiCap_1 = smooth3(CCTK_PASS_FTOF, k2phiCap(i + 2), k2phiCap(i + 1), k2phiCap(i), k2phiCap(i - 1))
				beta4rpiphiCap_1 = smooth4(CCTK_PASS_FTOF, k2phiCap(i + 1), k2phiCap(i), k2phiCap(i - 1), k2phiCap(i - 2))
				w1lpiphiCap_1 = nlw1(CCTK_PASS_FTOF, beta1lpiphiCap_1)
				w2lpiphiCap_1 = nlw2(CCTK_PASS_FTOF, beta2lpiphiCap_1)
				w3lpiphiCap_1 = nlw3(CCTK_PASS_FTOF, beta3lpiphiCap_1)
				w4lpiphiCap_1 = nlw4(CCTK_PASS_FTOF, beta4lpiphiCap_1)
				w1rpiphiCap_1 = nlw1(CCTK_PASS_FTOF, beta1rpiphiCap_1)
				w2rpiphiCap_1 = nlw2(CCTK_PASS_FTOF, beta2rpiphiCap_1)
				w3rpiphiCap_1 = nlw3(CCTK_PASS_FTOF, beta3rpiphiCap_1)
				w4rpiphiCap_1 = nlw4(CCTK_PASS_FTOF, beta4rpiphiCap_1)
				wlpiphiCap_1 = w1lpiphiCap_1 + w2lpiphiCap_1 + w3lpiphiCap_1 + w4lpiphiCap_1
				wrpiphiCap_1 = w1rpiphiCap_1 + w2rpiphiCap_1 + w3rpiphiCap_1 + w4rpiphiCap_1
				w1lpiphiCap_1 = w1lpiphiCap_1 / wlpiphiCap_1
				w2lpiphiCap_1 = w2lpiphiCap_1 / wlpiphiCap_1
				w3lpiphiCap_1 = w3lpiphiCap_1 / wlpiphiCap_1
				w4lpiphiCap_1 = w4lpiphiCap_1 / wlpiphiCap_1
				w1rpiphiCap_1 = w1rpiphiCap_1 / wrpiphiCap_1
				w2rpiphiCap_1 = w2rpiphiCap_1 / wrpiphiCap_1
				w3rpiphiCap_1 = w3rpiphiCap_1 / wrpiphiCap_1
				w4rpiphiCap_1 = w4rpiphiCap_1 / wrpiphiCap_1
				phiCap_lpi_1(i) = weno(CCTK_PASS_FTOF, w1lpiphiCap_1, w2lpiphiCap_1, w3lpiphiCap_1, w4lpiphiCap_1, p1lpiphiCap_1, p2lpiphiCap_1, p3lpiphiCap_1, p4lpiphiCap_1)
				phiCap_rpi_1(i) = weno(CCTK_PASS_FTOF, w1rpiphiCap_1, w2rpiphiCap_1, w3rpiphiCap_1, w4rpiphiCap_1, p1rpiphiCap_1, p2rpiphiCap_1, p3rpiphiCap_1, p4rpiphiCap_1)
				SpeediphiCap_1 = ABS(1.0d0)
				SpeedpiphiCap_1 = ABS(1.0d0)
				SpeedniphiCap_1 = ABS(1.0d0)
				FluxlpiphiCap_1 = FiphiCap_segmentI(CCTK_PASS_FTOF, phiCap_lpi_1(i))
				FluxlniphiCap_1 = FiphiCap_segmentI(CCTK_PASS_FTOF, phiCap_lni_1(i))
				FluxrpiphiCap_1 = FiphiCap_segmentI(CCTK_PASS_FTOF, phiCap_rpi_1(i))
				FluxrniphiCap_1 = FiphiCap_segmentI(CCTK_PASS_FTOF, phiCap_rni_1(i))
				FpiphiCap_1 = Fii(CCTK_PASS_FTOF, FluxlpiphiCap_1, FluxrpiphiCap_1, phiCap_lpi_1(i), phiCap_rpi_1(i), SpeediphiCap_1, SpeedpiphiCap_1)
				FniphiCap_1 = Fii(CCTK_PASS_FTOF, FluxlniphiCap_1, FluxrniphiCap_1, phiCap_lni_1(i), phiCap_rni_1(i), SpeediphiCap_1, SpeedniphiCap_1)
				fluxAccphiCap_1 = 0.0d0 + 0.0d0
				fluxAccphiCap_1 = fluxAccphiCap_1 - FVMi(CCTK_PASS_FTOF, FpiphiCap_1, FniphiCap_1)
				phiCap(i) = RK3P3(CCTK_PASS_FTOF, fluxAccphiCap_1, phiCap_p, k2phiCap, i)
			end if
		end do
		call CCTK_SyncGroup(status, cctkGH, "AdvectionwithBalsaranegShu::fields_tl2")
		call CCTK_SyncGroup(status, cctkGH, "AdvectionwithBalsaranegShu::block1")

	end subroutine AdvectionwithBalsaranegShu_execution