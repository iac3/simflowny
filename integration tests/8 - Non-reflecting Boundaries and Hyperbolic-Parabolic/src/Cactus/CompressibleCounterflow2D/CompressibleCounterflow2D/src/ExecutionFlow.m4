/*@@
  @file      ExecutionFlow.F
  @date      Thu Jun 06 13:26:38 CEST 2013
  @author    8
  @desc
             Execution flow for CompressibleCounterflow2D
  @enddesc
  @version 8
@@*/

define(CDi,(1.333333333d0 * ($4) - 1.333333333d0 * ($3) + ($2) * 0.166666667d0 - ($5) * 0.166666667d0) * 0.5d0 / deltax)dnl
define(CDj,(1.333333333d0 * ($4) - 1.333333333d0 * ($3) + ($2) * 0.166666667d0 - ($5) * 0.166666667d0) * 0.5d0 / deltay)dnl
define(FDOCi,1.0d0 / (12.0d0 * deltax) * (($3(($5) - 2, ($6)) - 8.0d0 * $3(($5) - 1, ($6)) + 8.0d0 * $3(($5) + 1, ($6))) - $3(($5) + 2, ($6)) + 1.0d0 * (((-$4(($5) - 2, ($6))) * ($2(($5) - 1, ($6)) - $2(($5) - 2, ($6))) + 3.0d0 * $4(($5) - 1, ($6)) * ($2(($5), ($6)) - $2(($5) - 1, ($6)))) - 3.0d0 * $4(($5), ($6)) * ($2(($5) + 1, ($6)) - $2(($5), ($6))) + $4(($5) + 1, ($6)) * ($2(($5) + 2, ($6)) - $2(($5) + 1, ($6))))))dnl
define(FDOCj,1.0d0 / (12.0d0 * deltay) * (($3(($5), ($6) - 2) - 8.0d0 * $3(($5), ($6) - 1) + 8.0d0 * $3(($5), ($6) + 1)) - $3(($5), ($6) + 2) + 1.0d0 * (((-$4(($5), ($6) - 2)) * ($2(($5), ($6) - 1) - $2(($5), ($6) - 2)) + 3.0d0 * $4(($5), ($6) - 1) * ($2(($5), ($6)) - $2(($5), ($6) - 1))) - 3.0d0 * $4(($5), ($6)) * ($2(($5), ($6) + 1) - $2(($5), ($6))) + $4(($5), ($6) + 1) * ($2(($5), ($6) + 2) - $2(($5), ($6) + 1)))))dnl
define(RK3P1,$3(($5), ($6)) + deltat * ($2))dnl
define(RK3P2,1.0d0 / 4.0d0 * (3.0d0 * $3(($5), ($6)) + $4(($5), ($6)) + deltat * ($2)))dnl
define(RK3P3,1.0d0 / 3.0d0 * ($3(($5), ($6)) + 2.0d0 * $4(($5), ($6)) + 2.0d0 * deltat * ($2)))dnl
define(Firho_FluidI,($2))dnl
define(Fjrho_FluidI,($2))dnl
define(Fimx_FluidI,(($3) * ($3)) / ($2) + ($4))dnl
define(Fjmx_FluidI,(($3) * ($4)) / ($2))dnl
define(Fimy_FluidI,(($4) * ($3)) / ($2))dnl
define(Fjmy_FluidI,(($3) * ($3)) / ($2) + ($4))dnl

#include "cctk.h"
#include "cctk_Parameters.h"
#include "cctk_Arguments.h"
#include "cctk_Functions.h"

	subroutine CompressibleCounterflow2D_execution(CCTK_ARGUMENTS)

		implicit none

		DECLARE_CCTK_ARGUMENTS
		DECLARE_CCTK_PARAMETERS
		DECLARE_CCTK_FUNCTIONS

!		Declare local variables
!		-----------------

		INTEGER i, iStart, iEnd
		INTEGER j, jStart, jEnd
		INTEGER istat, status, ReflectionCounter
		CCTK_REAL Q_ii_mx_1, Q_ij_mx_1, Q_ii2_mx_1, Q_jj_mx_1, Q_ii3_mx_1, Q_ji_mx_1, Q_ji_my_1, Q_jj_my_1, Q_ii_my_1, Q_jj2_my_1, Q_ij_my_1, Q_jj3_my_1, Pm2_ii_mx_1, Pm2_ij_mx_1, Pm2_ii2_mx_1, Pm2_jj_mx_1, Pm2_ii3_mx_1, Pm2_ji_mx_1, Pm2_ji_my_1, Pm2_jj_my_1, Pm2_ii_my_1, Pm2_jj2_my_1, Pm2_ij_my_1, Pm2_jj3_my_1, Pm1_ii_mx_1, Pm1_ij_mx_1, Pm1_ii2_mx_1, Pm1_jj_mx_1, Pm1_ii3_mx_1, Pm1_ji_mx_1, Pm1_ji_my_1, Pm1_jj_my_1, Pm1_ii_my_1, Pm1_jj2_my_1, Pm1_ij_my_1, Pm1_jj3_my_1, Pp1_ii_mx_1, Pp1_ij_mx_1, Pp1_ii2_mx_1, Pp1_jj_mx_1, Pp1_ii3_mx_1, Pp1_ji_mx_1, Pp1_ji_my_1, Pp1_jj_my_1, Pp1_ii_my_1, Pp1_jj2_my_1, Pp1_ij_my_1, Pp1_jj3_my_1, Pp2_ii_mx_1, Pp2_ij_mx_1, Pp2_ii2_mx_1, Pp2_jj_mx_1, Pp2_ii3_mx_1, Pp2_ji_mx_1, Pp2_ji_my_1, Pp2_jj_my_1, Pp2_ii_my_1, Pp2_jj2_my_1, Pp2_ij_my_1, Pp2_jj3_my_1, m_n_1, m_tx_1, m_ty_1, c_x_1, cn_x_1, c_y_1, cn_y_1, fluxAccrho_1, fluxAccmx_1, fluxAccmy_1, n_x_1, n_y_1, interaction_index_1, mod_normal_1, fluxAccm_n_1, fluxAccm_tx_1, fluxAccm_ty_1, c_1, cn_1, fluxAccwnp_0_1, fluxAccwnm_0_1, fluxAccwtx_0_1, fluxAccwty_0_1


		CCTK_INT exitCond
		CCTK_REAL deltax
		CCTK_REAL deltay
		CCTK_REAL deltat

		deltax = 0.009999999776482582d0/(cctk_gsh(1) - 1)
		deltay = 0.009999999776482582d0/(cctk_gsh(2) - 1)
		deltat = CCTK_DELTA_TIME

!		Finalization condition
		if (cctk_time .ge. tend) then 
			call CCTK_Exit(exitCond , cctkGH, 0)
		end if

!		Evolution
		iStart = 1
		iEnd = cctk_lsh(1)
		jStart = 1
		jEnd = cctk_lsh(2)
		do j = jStart, jEnd
			do i = iStart, iEnd
				if ((FOV_1(i, j) .gt. 0 .or. (((i + 1 .le. cctk_lsh(1) .and. FOV_1(i + 1, j) .gt. 0) .or. (i + 2 .le. cctk_lsh(1) .and. FOV_1(i + 2, j) .gt. 0) .or. (j + 1 .le. cctk_lsh(2) .and. FOV_1(i, j + 1) .gt. 0) .or. (j + 2 .le. cctk_lsh(2) .and. FOV_1(i, j + 2) .gt. 0)) .or. ((i - 1 .ge. 1 .and. FOV_1(i - 1, j) .gt. 0) .or. (i - 2 .ge. 1 .and. FOV_1(i - 2, j) .gt. 0) .or. (j - 1 .ge. 1 .and. FOV_1(i, j - 1) .gt. 0) .or. (j - 2 .ge. 1 .and. FOV_1(i, j - 2) .gt. 0)) .or. (((i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1) .and. FOV_1(int(i + 1), int(j - 1)) .gt. 0) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2)) .and. FOV_1(int(i + 1), int(j + 1)) .gt. 0) .or. ((i - 1 .ge. 1 .and. j - 1 .ge. 1) .and. FOV_1(int(i - 1), int(j - 1)) .gt. 0) .or. ((i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2)) .and. FOV_1(int(i - 1), int(j + 1)) .gt. 0) .or. ((i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1) .and. FOV_1(int(i + 1), int(j - 2)) .gt. 0) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2)) .and. FOV_1(int(i + 1), int(j + 2)) .gt. 0) .or. ((i - 1 .ge. 1 .and. j - 2 .ge. 1) .and. FOV_1(int(i - 1), int(j - 2)) .gt. 0) .or. ((i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2)) .and. FOV_1(int(i - 1), int(j + 2)) .gt. 0) .or. ((i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1) .and. FOV_1(int(i + 2), int(j - 1)) .gt. 0) .or. ((i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2)) .and. FOV_1(int(i + 2), int(j + 1)) .gt. 0) .or. ((i - 2 .ge. 1 .and. j - 1 .ge. 1) .and. FOV_1(int(i - 2), int(j - 1)) .gt. 0) .or. ((i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2)) .and. FOV_1(int(i - 2), int(j + 1)) .gt. 0) .or. ((i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1) .and. FOV_1(int(i + 2), int(j - 2)) .gt. 0) .or. ((i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2)) .and. FOV_1(int(i + 2), int(j + 2)) .gt. 0) .or. ((i - 2 .ge. 1 .and. j - 2 .ge. 1) .and. FOV_1(int(i - 2), int(j - 2)) .gt. 0) .or. ((i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2)) .and. FOV_1(int(i - 2), int(j + 2)) .gt. 0))))) then
					if (((i .ge. 3) .and. (i .le. (cctk_lsh(1) - 3))) .and. ((j .ge. 3) .and. (j .le. (cctk_lsh(2) - 3)))) then
					Flux_extsubrhosupi_1(i, j) = Firho_FluidI(CCTK_PASS_FTOF, extrapolatedsubmxsupi(i, j))
					Flux_extsubrhosupj_1(i, j) = Fjrho_FluidI(CCTK_PASS_FTOF, extrapolatedsubmysupj(i, j))
					Flux_extsubmxsupi_1(i, j) = Fimx_FluidI(CCTK_PASS_FTOF, extrapolatedsubrhosupi(i, j), extrapolatedsubmxsupi(i, j), extrapolatedsubpsupi(i, j))
					Flux_extsubmxsupj_1(i, j) = Fjmx_FluidI(CCTK_PASS_FTOF, extrapolatedsubrhosupj(i, j), extrapolatedsubmxsupj(i, j), extrapolatedsubmysupj(i, j))
					Flux_extsubmysupi_1(i, j) = Fimy_FluidI(CCTK_PASS_FTOF, extrapolatedsubrhosupi(i, j), extrapolatedsubmxsupi(i, j), extrapolatedsubmysupi(i, j))
					Flux_extsubmysupj_1(i, j) = Fjmy_FluidI(CCTK_PASS_FTOF, extrapolatedsubrhosupj(i, j), extrapolatedsubmysupj(i, j), extrapolatedsubpsupj(i, j))
					Q_ii_mx_1 = lambda
					Q_ij_mx_1 = lambda
					Q_ii2_mx_1 = mu
					Q_jj_mx_1 = mu
					Q_ii3_mx_1 = mu
					Q_ji_mx_1 = mu
					Q_ji_my_1 = lambda
					Q_jj_my_1 = lambda
					Q_ii_my_1 = mu
					Q_jj2_my_1 = mu
					Q_ij_my_1 = mu
					Q_jj3_my_1 = mu
					Pm2_ii_mx_1 = mx_p(i - 2, j) / rho_p(i - 2, j)
					Pm2_ij_mx_1 = my_p(i, j - 2) / rho_p(i, j - 2)
					Pm2_ii2_mx_1 = mx_p(i - 2, j) / rho_p(i - 2, j)
					Pm2_jj_mx_1 = mx_p(i, j - 2) / rho_p(i, j - 2)
					Pm2_ii3_mx_1 = mx_p(i - 2, j) / rho_p(i - 2, j)
					Pm2_ji_mx_1 = my_p(i - 2, j) / rho_p(i - 2, j)
					Pm2_ji_my_1 = mx_p(i - 2, j) / rho_p(i - 2, j)
					Pm2_jj_my_1 = my_p(i, j - 2) / rho_p(i, j - 2)
					Pm2_ii_my_1 = my_p(i - 2, j) / rho_p(i - 2, j)
					Pm2_jj2_my_1 = my_p(i, j - 2) / rho_p(i, j - 2)
					Pm2_ij_my_1 = mx_p(i, j - 2) / rho_p(i, j - 2)
					Pm2_jj3_my_1 = my_p(i, j - 2) / rho_p(i, j - 2)
					Pm1_ii_mx_1 = mx_p(i - 1, j) / rho_p(i - 1, j)
					Pm1_ij_mx_1 = my_p(i, j - 1) / rho_p(i, j - 1)
					Pm1_ii2_mx_1 = mx_p(i - 1, j) / rho_p(i - 1, j)
					Pm1_jj_mx_1 = mx_p(i, j - 1) / rho_p(i, j - 1)
					Pm1_ii3_mx_1 = mx_p(i - 1, j) / rho_p(i - 1, j)
					Pm1_ji_mx_1 = my_p(i - 1, j) / rho_p(i - 1, j)
					Pm1_ji_my_1 = mx_p(i - 1, j) / rho_p(i - 1, j)
					Pm1_jj_my_1 = my_p(i, j - 1) / rho_p(i, j - 1)
					Pm1_ii_my_1 = my_p(i - 1, j) / rho_p(i - 1, j)
					Pm1_jj2_my_1 = my_p(i, j - 1) / rho_p(i, j - 1)
					Pm1_ij_my_1 = mx_p(i, j - 1) / rho_p(i, j - 1)
					Pm1_jj3_my_1 = my_p(i, j - 1) / rho_p(i, j - 1)
					Pp1_ii_mx_1 = mx_p(i + 1, j) / rho_p(i + 1, j)
					Pp1_ij_mx_1 = my_p(i, j + 1) / rho_p(i, j + 1)
					Pp1_ii2_mx_1 = mx_p(i + 1, j) / rho_p(i + 1, j)
					Pp1_jj_mx_1 = mx_p(i, j + 1) / rho_p(i, j + 1)
					Pp1_ii3_mx_1 = mx_p(i + 1, j) / rho_p(i + 1, j)
					Pp1_ji_mx_1 = my_p(i + 1, j) / rho_p(i + 1, j)
					Pp1_ji_my_1 = mx_p(i + 1, j) / rho_p(i + 1, j)
					Pp1_jj_my_1 = my_p(i, j + 1) / rho_p(i, j + 1)
					Pp1_ii_my_1 = my_p(i + 1, j) / rho_p(i + 1, j)
					Pp1_jj2_my_1 = my_p(i, j + 1) / rho_p(i, j + 1)
					Pp1_ij_my_1 = mx_p(i, j + 1) / rho_p(i, j + 1)
					Pp1_jj3_my_1 = my_p(i, j + 1) / rho_p(i, j + 1)
					Pp2_ii_mx_1 = mx_p(i + 2, j) / rho_p(i + 2, j)
					Pp2_ij_mx_1 = my_p(i, j + 2) / rho_p(i, j + 2)
					Pp2_ii2_mx_1 = mx_p(i + 2, j) / rho_p(i + 2, j)
					Pp2_jj_mx_1 = mx_p(i, j + 2) / rho_p(i, j + 2)
					Pp2_ii3_mx_1 = mx_p(i + 2, j) / rho_p(i + 2, j)
					Pp2_ji_mx_1 = my_p(i + 2, j) / rho_p(i + 2, j)
					Pp2_ji_my_1 = mx_p(i + 2, j) / rho_p(i + 2, j)
					Pp2_jj_my_1 = my_p(i, j + 2) / rho_p(i, j + 2)
					Pp2_ii_my_1 = my_p(i + 2, j) / rho_p(i + 2, j)
					Pp2_jj2_my_1 = my_p(i, j + 2) / rho_p(i, j + 2)
					Pp2_ij_my_1 = mx_p(i, j + 2) / rho_p(i, j + 2)
					Pp2_jj3_my_1 = my_p(i, j + 2) / rho_p(i, j + 2)
					Flux_extsubmxsupi_1(i, j) = Flux_extsubmxsupi_1(i, j) - Q_ii_mx_1 * CDi(CCTK_PASS_FTOF, Pm2_ii_mx_1, Pm1_ii_mx_1, Pp1_ii_mx_1, Pp2_ii_mx_1)
					Flux_extsubmxsupi_1(i, j) = Flux_extsubmxsupi_1(i, j) - Q_ij_mx_1 * CDj(CCTK_PASS_FTOF, Pm2_ij_mx_1, Pm1_ij_mx_1, Pp1_ij_mx_1, Pp2_ij_mx_1)
					Flux_extsubmxsupi_1(i, j) = Flux_extsubmxsupi_1(i, j) - Q_ii2_mx_1 * CDi(CCTK_PASS_FTOF, Pm2_ii2_mx_1, Pm1_ii2_mx_1, Pp1_ii2_mx_1, Pp2_ii2_mx_1)
					Flux_extsubmxsupj_1(i, j) = Flux_extsubmxsupj_1(i, j) - Q_jj_mx_1 * CDj(CCTK_PASS_FTOF, Pm2_jj_mx_1, Pm1_jj_mx_1, Pp1_jj_mx_1, Pp2_jj_mx_1)
					Flux_extsubmxsupi_1(i, j) = Flux_extsubmxsupi_1(i, j) - Q_ii3_mx_1 * CDi(CCTK_PASS_FTOF, Pm2_ii3_mx_1, Pm1_ii3_mx_1, Pp1_ii3_mx_1, Pp2_ii3_mx_1)
					Flux_extsubmxsupj_1(i, j) = Flux_extsubmxsupj_1(i, j) - Q_ji_mx_1 * CDi(CCTK_PASS_FTOF, Pm2_ji_mx_1, Pm1_ji_mx_1, Pp1_ji_mx_1, Pp2_ji_mx_1)
					Flux_extsubmysupj_1(i, j) = Flux_extsubmysupj_1(i, j) - Q_ji_my_1 * CDi(CCTK_PASS_FTOF, Pm2_ji_my_1, Pm1_ji_my_1, Pp1_ji_my_1, Pp2_ji_my_1)
					Flux_extsubmysupj_1(i, j) = Flux_extsubmysupj_1(i, j) - Q_jj_my_1 * CDj(CCTK_PASS_FTOF, Pm2_jj_my_1, Pm1_jj_my_1, Pp1_jj_my_1, Pp2_jj_my_1)
					Flux_extsubmysupi_1(i, j) = Flux_extsubmysupi_1(i, j) - Q_ii_my_1 * CDi(CCTK_PASS_FTOF, Pm2_ii_my_1, Pm1_ii_my_1, Pp1_ii_my_1, Pp2_ii_my_1)
					Flux_extsubmysupj_1(i, j) = Flux_extsubmysupj_1(i, j) - Q_jj2_my_1 * CDj(CCTK_PASS_FTOF, Pm2_jj2_my_1, Pm1_jj2_my_1, Pp1_jj2_my_1, Pp2_jj2_my_1)
					Flux_extsubmysupi_1(i, j) = Flux_extsubmysupi_1(i, j) - Q_ij_my_1 * CDj(CCTK_PASS_FTOF, Pm2_ij_my_1, Pm1_ij_my_1, Pp1_ij_my_1, Pp2_ij_my_1)
					Flux_extsubmysupj_1(i, j) = Flux_extsubmysupj_1(i, j) - Q_jj3_my_1 * CDj(CCTK_PASS_FTOF, Pm2_jj3_my_1, Pm1_jj3_my_1, Pp1_jj3_my_1, Pp2_jj3_my_1)
					end if
					m_n_1 = extrapolatedsubmxsupi(i, j)
					m_tx_1 = 0.0d0
					m_ty_1 = extrapolatedsubmysupi(i, j)
					c_x_1 = c0 * ((extrapolatedsubrhosupi(i, j) / rho0) ** ((gamma + (-1.0d0)) / 2.0d0))
					cn_x_1 = m_n_1 / extrapolatedsubrhosupi(i, j)
					m_n_1 = extrapolatedsubmysupj(i, j)
					m_tx_1 = extrapolatedsubmxsupj(i, j)
					m_ty_1 = 0.0d0
					c_y_1 = c0 * ((extrapolatedsubrhosupj(i, j) / rho0) ** ((gamma + (-1.0d0)) / 2.0d0))
					cn_y_1 = m_n_1 / extrapolatedsubrhosupj(i, j)
					Speedi_1(i, j) = MAX(ABS(c_x_1 + cn_x_1), ABS((-c_x_1) + cn_x_1), ABS(cn_x_1), ABS(cn_x_1))
					Speedj_1(i, j) = MAX(ABS(c_y_1 + cn_y_1), ABS((-c_y_1) + cn_y_1), ABS(cn_y_1), ABS(cn_y_1))
				end if
			end do
		end do
		do j = jEnd/2, jEnd
			do i = iEnd/2, iEnd
!				Boundaries
		if ((FOV_xLower(i, j) .gt. 0) .and. ((i + 1 .le. cctk_lsh(1) .and. (FOV_1(int(i + 1), j) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. (FOV_1(int(i + 2), j) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. (FOV_1(int(i + 3), j) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 3)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_rho, Flux_extsubrhosupi_1, Flux_extsubrhosupi_1, i, d_j_rho, Flux_extsubrhosupj_1, Flux_extsubrhosupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_mx, Flux_extsubmxsupi_1, Flux_extsubmxsupi_1, i, d_j_mx, Flux_extsubmxsupj_1, Flux_extsubmxsupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_my, Flux_extsubmysupi_1, Flux_extsubmysupi_1, i, d_j_my, Flux_extsubmysupj_1, Flux_extsubmysupj_1, j, FOV_1)
			end if
		end if
		if ((FOV_xUpper(i, j) .gt. 0) .and. ((i - 1 .ge. 1 .and. (FOV_1(int(i - 1), j) .gt. 0)) .or. (i - 2 .ge. 1 .and. (FOV_1(int(i - 2), j) .gt. 0)) .or. (i - 3 .ge. 1 .and. (FOV_1(int(i - 3), j) .gt. 0)) .or. ((i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_rho, Flux_extsubrhosupi_1, Flux_extsubrhosupi_1, i, d_j_rho, Flux_extsubrhosupj_1, Flux_extsubrhosupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_mx, Flux_extsubmxsupi_1, Flux_extsubmxsupi_1, i, d_j_mx, Flux_extsubmxsupj_1, Flux_extsubmxsupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_my, Flux_extsubmysupi_1, Flux_extsubmysupi_1, i, d_j_my, Flux_extsubmysupj_1, Flux_extsubmysupj_1, j, FOV_1)
			end if
		end if
		if ((FOV_yLower(i, j) .gt. 0) .and. ((j + 1 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 1)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 2)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 3)) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_rho, Flux_extsubrhosupi_1, Flux_extsubrhosupi_1, i, d_j_rho, Flux_extsubrhosupj_1, Flux_extsubrhosupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_mx, Flux_extsubmxsupi_1, Flux_extsubmxsupi_1, i, d_j_mx, Flux_extsubmxsupj_1, Flux_extsubmxsupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_my, Flux_extsubmysupi_1, Flux_extsubmysupi_1, i, d_j_my, Flux_extsubmysupj_1, Flux_extsubmysupj_1, j, FOV_1)
			end if
		end if
		if ((FOV_yUpper(i, j) .gt. 0) .and. ((j - 1 .ge. 1 .and. (FOV_1(i, int(j - 1)) .gt. 0)) .or. (j - 2 .ge. 1 .and. (FOV_1(i, int(j - 2)) .gt. 0)) .or. (j - 3 .ge. 1 .and. (FOV_1(i, int(j - 3)) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_rho, Flux_extsubrhosupi_1, Flux_extsubrhosupi_1, i, d_j_rho, Flux_extsubrhosupj_1, Flux_extsubrhosupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_mx, Flux_extsubmxsupi_1, Flux_extsubmxsupi_1, i, d_j_mx, Flux_extsubmxsupj_1, Flux_extsubmxsupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_my, Flux_extsubmysupi_1, Flux_extsubmysupi_1, i, d_j_my, Flux_extsubmysupj_1, Flux_extsubmysupj_1, j, FOV_1)
			end if
		end if
			end do
		end do
		do j = jEnd/2 - 1, jStart, -1
			do i = iEnd/2, iEnd
!				Boundaries
		if ((FOV_xLower(i, j) .gt. 0) .and. ((i + 1 .le. cctk_lsh(1) .and. (FOV_1(int(i + 1), j) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. (FOV_1(int(i + 2), j) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. (FOV_1(int(i + 3), j) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 3)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_rho, Flux_extsubrhosupi_1, Flux_extsubrhosupi_1, i, d_j_rho, Flux_extsubrhosupj_1, Flux_extsubrhosupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_mx, Flux_extsubmxsupi_1, Flux_extsubmxsupi_1, i, d_j_mx, Flux_extsubmxsupj_1, Flux_extsubmxsupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_my, Flux_extsubmysupi_1, Flux_extsubmysupi_1, i, d_j_my, Flux_extsubmysupj_1, Flux_extsubmysupj_1, j, FOV_1)
			end if
		end if
		if ((FOV_xUpper(i, j) .gt. 0) .and. ((i - 1 .ge. 1 .and. (FOV_1(int(i - 1), j) .gt. 0)) .or. (i - 2 .ge. 1 .and. (FOV_1(int(i - 2), j) .gt. 0)) .or. (i - 3 .ge. 1 .and. (FOV_1(int(i - 3), j) .gt. 0)) .or. ((i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_rho, Flux_extsubrhosupi_1, Flux_extsubrhosupi_1, i, d_j_rho, Flux_extsubrhosupj_1, Flux_extsubrhosupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_mx, Flux_extsubmxsupi_1, Flux_extsubmxsupi_1, i, d_j_mx, Flux_extsubmxsupj_1, Flux_extsubmxsupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_my, Flux_extsubmysupi_1, Flux_extsubmysupi_1, i, d_j_my, Flux_extsubmysupj_1, Flux_extsubmysupj_1, j, FOV_1)
			end if
		end if
		if ((FOV_yLower(i, j) .gt. 0) .and. ((j + 1 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 1)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 2)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 3)) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_rho, Flux_extsubrhosupi_1, Flux_extsubrhosupi_1, i, d_j_rho, Flux_extsubrhosupj_1, Flux_extsubrhosupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_mx, Flux_extsubmxsupi_1, Flux_extsubmxsupi_1, i, d_j_mx, Flux_extsubmxsupj_1, Flux_extsubmxsupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_my, Flux_extsubmysupi_1, Flux_extsubmysupi_1, i, d_j_my, Flux_extsubmysupj_1, Flux_extsubmysupj_1, j, FOV_1)
			end if
		end if
		if ((FOV_yUpper(i, j) .gt. 0) .and. ((j - 1 .ge. 1 .and. (FOV_1(i, int(j - 1)) .gt. 0)) .or. (j - 2 .ge. 1 .and. (FOV_1(i, int(j - 2)) .gt. 0)) .or. (j - 3 .ge. 1 .and. (FOV_1(i, int(j - 3)) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_rho, Flux_extsubrhosupi_1, Flux_extsubrhosupi_1, i, d_j_rho, Flux_extsubrhosupj_1, Flux_extsubrhosupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_mx, Flux_extsubmxsupi_1, Flux_extsubmxsupi_1, i, d_j_mx, Flux_extsubmxsupj_1, Flux_extsubmxsupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_my, Flux_extsubmysupi_1, Flux_extsubmysupi_1, i, d_j_my, Flux_extsubmysupj_1, Flux_extsubmysupj_1, j, FOV_1)
			end if
		end if
			end do
		end do
		do j = jEnd/2, jEnd
			do i = iEnd/2 - 1, iStart, -1
!				Boundaries
		if ((FOV_xLower(i, j) .gt. 0) .and. ((i + 1 .le. cctk_lsh(1) .and. (FOV_1(int(i + 1), j) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. (FOV_1(int(i + 2), j) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. (FOV_1(int(i + 3), j) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 3)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_rho, Flux_extsubrhosupi_1, Flux_extsubrhosupi_1, i, d_j_rho, Flux_extsubrhosupj_1, Flux_extsubrhosupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_mx, Flux_extsubmxsupi_1, Flux_extsubmxsupi_1, i, d_j_mx, Flux_extsubmxsupj_1, Flux_extsubmxsupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_my, Flux_extsubmysupi_1, Flux_extsubmysupi_1, i, d_j_my, Flux_extsubmysupj_1, Flux_extsubmysupj_1, j, FOV_1)
			end if
		end if
		if ((FOV_xUpper(i, j) .gt. 0) .and. ((i - 1 .ge. 1 .and. (FOV_1(int(i - 1), j) .gt. 0)) .or. (i - 2 .ge. 1 .and. (FOV_1(int(i - 2), j) .gt. 0)) .or. (i - 3 .ge. 1 .and. (FOV_1(int(i - 3), j) .gt. 0)) .or. ((i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_rho, Flux_extsubrhosupi_1, Flux_extsubrhosupi_1, i, d_j_rho, Flux_extsubrhosupj_1, Flux_extsubrhosupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_mx, Flux_extsubmxsupi_1, Flux_extsubmxsupi_1, i, d_j_mx, Flux_extsubmxsupj_1, Flux_extsubmxsupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_my, Flux_extsubmysupi_1, Flux_extsubmysupi_1, i, d_j_my, Flux_extsubmysupj_1, Flux_extsubmysupj_1, j, FOV_1)
			end if
		end if
		if ((FOV_yLower(i, j) .gt. 0) .and. ((j + 1 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 1)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 2)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 3)) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_rho, Flux_extsubrhosupi_1, Flux_extsubrhosupi_1, i, d_j_rho, Flux_extsubrhosupj_1, Flux_extsubrhosupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_mx, Flux_extsubmxsupi_1, Flux_extsubmxsupi_1, i, d_j_mx, Flux_extsubmxsupj_1, Flux_extsubmxsupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_my, Flux_extsubmysupi_1, Flux_extsubmysupi_1, i, d_j_my, Flux_extsubmysupj_1, Flux_extsubmysupj_1, j, FOV_1)
			end if
		end if
		if ((FOV_yUpper(i, j) .gt. 0) .and. ((j - 1 .ge. 1 .and. (FOV_1(i, int(j - 1)) .gt. 0)) .or. (j - 2 .ge. 1 .and. (FOV_1(i, int(j - 2)) .gt. 0)) .or. (j - 3 .ge. 1 .and. (FOV_1(i, int(j - 3)) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_rho, Flux_extsubrhosupi_1, Flux_extsubrhosupi_1, i, d_j_rho, Flux_extsubrhosupj_1, Flux_extsubrhosupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_mx, Flux_extsubmxsupi_1, Flux_extsubmxsupi_1, i, d_j_mx, Flux_extsubmxsupj_1, Flux_extsubmxsupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_my, Flux_extsubmysupi_1, Flux_extsubmysupi_1, i, d_j_my, Flux_extsubmysupj_1, Flux_extsubmysupj_1, j, FOV_1)
			end if
		end if
			end do
		end do
		do j = jEnd/2 - 1, jStart, -1
			do i = iEnd/2 - 1, iStart, -1
!				Boundaries
		if ((FOV_xLower(i, j) .gt. 0) .and. ((i + 1 .le. cctk_lsh(1) .and. (FOV_1(int(i + 1), j) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. (FOV_1(int(i + 2), j) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. (FOV_1(int(i + 3), j) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 3)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_rho, Flux_extsubrhosupi_1, Flux_extsubrhosupi_1, i, d_j_rho, Flux_extsubrhosupj_1, Flux_extsubrhosupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_mx, Flux_extsubmxsupi_1, Flux_extsubmxsupi_1, i, d_j_mx, Flux_extsubmxsupj_1, Flux_extsubmxsupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_my, Flux_extsubmysupi_1, Flux_extsubmysupi_1, i, d_j_my, Flux_extsubmysupj_1, Flux_extsubmysupj_1, j, FOV_1)
			end if
		end if
		if ((FOV_xUpper(i, j) .gt. 0) .and. ((i - 1 .ge. 1 .and. (FOV_1(int(i - 1), j) .gt. 0)) .or. (i - 2 .ge. 1 .and. (FOV_1(int(i - 2), j) .gt. 0)) .or. (i - 3 .ge. 1 .and. (FOV_1(int(i - 3), j) .gt. 0)) .or. ((i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_rho, Flux_extsubrhosupi_1, Flux_extsubrhosupi_1, i, d_j_rho, Flux_extsubrhosupj_1, Flux_extsubrhosupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_mx, Flux_extsubmxsupi_1, Flux_extsubmxsupi_1, i, d_j_mx, Flux_extsubmxsupj_1, Flux_extsubmxsupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_my, Flux_extsubmysupi_1, Flux_extsubmysupi_1, i, d_j_my, Flux_extsubmysupj_1, Flux_extsubmysupj_1, j, FOV_1)
			end if
		end if
		if ((FOV_yLower(i, j) .gt. 0) .and. ((j + 1 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 1)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 2)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 3)) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_rho, Flux_extsubrhosupi_1, Flux_extsubrhosupi_1, i, d_j_rho, Flux_extsubrhosupj_1, Flux_extsubrhosupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_mx, Flux_extsubmxsupi_1, Flux_extsubmxsupi_1, i, d_j_mx, Flux_extsubmxsupj_1, Flux_extsubmxsupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_my, Flux_extsubmysupi_1, Flux_extsubmysupi_1, i, d_j_my, Flux_extsubmysupj_1, Flux_extsubmysupj_1, j, FOV_1)
			end if
		end if
		if ((FOV_yUpper(i, j) .gt. 0) .and. ((j - 1 .ge. 1 .and. (FOV_1(i, int(j - 1)) .gt. 0)) .or. (j - 2 .ge. 1 .and. (FOV_1(i, int(j - 2)) .gt. 0)) .or. (j - 3 .ge. 1 .and. (FOV_1(i, int(j - 3)) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_rho, Flux_extsubrhosupi_1, Flux_extsubrhosupi_1, i, d_j_rho, Flux_extsubrhosupj_1, Flux_extsubrhosupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_mx, Flux_extsubmxsupi_1, Flux_extsubmxsupi_1, i, d_j_mx, Flux_extsubmxsupj_1, Flux_extsubmxsupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_my, Flux_extsubmysupi_1, Flux_extsubmysupi_1, i, d_j_my, Flux_extsubmysupj_1, Flux_extsubmysupj_1, j, FOV_1)
			end if
		end if
			end do
		end do
		call CCTK_SyncGroup(status, cctkGH, "CompressibleCounterflow2D::block1")
		do j = jStart, jEnd
			do i = iStart, iEnd
				if ((FOV_1(i, j) .gt. 0 .or. (((i + 1 .le. cctk_lsh(1) .and. FOV_1(i + 1, j) .gt. 0) .or. (j + 1 .le. cctk_lsh(2) .and. FOV_1(i, j + 1) .gt. 0)) .or. ((i - 1 .ge. 1 .and. FOV_1(i - 1, j) .gt. 0) .or. (j - 1 .ge. 1 .and. FOV_1(i, j - 1) .gt. 0)))) .and. (i + 1 .le. cctk_lsh(1) .and. i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. j - 1 .ge. 1)) then
					Speedi_1(i, j) = MAX(Speedi_1(i, j), Speedi_1(i + 1, j))
					Speedj_1(i, j) = MAX(Speedj_1(i, j), Speedj_1(i, j + 1))
				end if
			end do
		end do
		call CCTK_SyncGroup(status, cctkGH, "CompressibleCounterflow2D::block2")
		do j = jStart, jEnd
			do i = iStart, iEnd
				if ((FOV_1(i, j) .gt. 0) .and. (i + 2 .le. cctk_lsh(1) .and. i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. j - 2 .ge. 1)) then
					fluxAccrho_1 = 0.0d0
					fluxAccmx_1 = rho_p(i, j) * fx_p(i, j)
					fluxAccmy_1 = rho_p(i, j) * fy_p(i, j)
					fluxAccrho_1 = fluxAccrho_1 - FDOCi(CCTK_PASS_FTOF, extrapolatedsubrhosupi, Flux_extsubrhosupi_1, Speedi_1, i, j)
					fluxAccrho_1 = fluxAccrho_1 - FDOCj(CCTK_PASS_FTOF, extrapolatedsubrhosupj, Flux_extsubrhosupj_1, Speedj_1, i, j)
					fluxAccmx_1 = fluxAccmx_1 - FDOCi(CCTK_PASS_FTOF, extrapolatedsubmxsupi, Flux_extsubmxsupi_1, Speedi_1, i, j)
					fluxAccmx_1 = fluxAccmx_1 - FDOCj(CCTK_PASS_FTOF, extrapolatedsubmxsupj, Flux_extsubmxsupj_1, Speedj_1, i, j)
					fluxAccmy_1 = fluxAccmy_1 - FDOCi(CCTK_PASS_FTOF, extrapolatedsubmysupi, Flux_extsubmysupi_1, Speedi_1, i, j)
					fluxAccmy_1 = fluxAccmy_1 - FDOCj(CCTK_PASS_FTOF, extrapolatedsubmysupj, Flux_extsubmysupj_1, Speedj_1, i, j)
					n_x_1 = 0.0d0
					n_y_1 = 0.0d0
					interaction_index_1 = 0.0d0
					if ((n_x_1 .eq. 0.0d0) .and. (n_y_1 .eq. 0.0d0)) then
					if ((FOV_xLower(int(i + 1.0d0), j) .gt. 0.0d0) .or. (FOV_xUpper(int(i + 1.0d0), j) .gt. 0.0d0)) then
					interaction_index_1 = 1.0d0
					n_x_1 = n_x_1 + deltax
					end if
					if ((FOV_xLower(int(i - 1.0d0), j) .gt. 0.0d0) .or. (FOV_xUpper(int(i - 1.0d0), j) .gt. 0.0d0)) then
					interaction_index_1 = 1.0d0
					n_x_1 = n_x_1 - deltax
					end if
					if ((FOV_xLower(i, int(j + 1.0d0)) .gt. 0.0d0) .or. (FOV_xUpper(i, int(j + 1.0d0)) .gt. 0.0d0)) then
					interaction_index_1 = 1.0d0
					n_y_1 = n_y_1 + deltay
					end if
					if ((FOV_xLower(i, int(j - 1.0d0)) .gt. 0.0d0) .or. (FOV_xUpper(i, int(j - 1.0d0)) .gt. 0.0d0)) then
					interaction_index_1 = 1.0d0
					n_y_1 = n_y_1 - deltay
					end if
					end if
					if ((n_x_1 .eq. 0.0d0) .and. (n_y_1 .eq. 0.0d0)) then
					if ((((((FOV_xLower(int(i + 1.0d0), int(j + 1.0d0)) .gt. 0.0d0) .or. (FOV_xUpper(int(i + 1.0d0), int(j + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i + 1.0d0), int(j - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i + 1.0d0), int(j - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i + 1.0d0), j) .gt. 0.0d0)) .or. (FOV_xUpper(int(i + 1.0d0), j) .gt. 0.0d0)) then
					interaction_index_1 = 1.0d0
					n_x_1 = n_x_1 + deltax
					end if
					if ((((((FOV_xLower(int(i - 1.0d0), int(j + 1.0d0)) .gt. 0.0d0) .or. (FOV_xUpper(int(i - 1.0d0), int(j + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i - 1.0d0), int(j - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i - 1.0d0), int(j - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i - 1.0d0), j) .gt. 0.0d0)) .or. (FOV_xUpper(int(i - 1.0d0), j) .gt. 0.0d0)) then
					interaction_index_1 = 1.0d0
					n_x_1 = n_x_1 - deltax
					end if
					if ((((((FOV_xLower(int(i + 1.0d0), int(j + 1.0d0)) .gt. 0.0d0) .or. (FOV_xUpper(int(i + 1.0d0), int(j + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i - 1.0d0), int(j + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i - 1.0d0), int(j + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(i, int(j + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(i, int(j + 1.0d0)) .gt. 0.0d0)) then
					interaction_index_1 = 1.0d0
					n_y_1 = n_y_1 + deltay
					end if
					if ((((((FOV_xLower(int(i + 1.0d0), int(j - 1.0d0)) .gt. 0.0d0) .or. (FOV_xUpper(int(i + 1.0d0), int(j - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i - 1.0d0), int(j - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i - 1.0d0), int(j - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(i, int(j - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(i, int(j - 1.0d0)) .gt. 0.0d0)) then
					interaction_index_1 = 1.0d0
					n_y_1 = n_y_1 - deltay
					end if
					end if
					if ((n_x_1 .eq. 0.0d0) .and. (n_y_1 .eq. 0.0d0)) then
					if ((FOV_yLower(int(i + 1.0d0), j) .gt. 0.0d0) .or. (FOV_yUpper(int(i + 1.0d0), j) .gt. 0.0d0)) then
					interaction_index_1 = 2.0d0
					n_x_1 = n_x_1 + deltax
					end if
					if ((FOV_yLower(int(i - 1.0d0), j) .gt. 0.0d0) .or. (FOV_yUpper(int(i - 1.0d0), j) .gt. 0.0d0)) then
					interaction_index_1 = 2.0d0
					n_x_1 = n_x_1 - deltax
					end if
					if ((FOV_yLower(i, int(j + 1.0d0)) .gt. 0.0d0) .or. (FOV_yUpper(i, int(j + 1.0d0)) .gt. 0.0d0)) then
					interaction_index_1 = 2.0d0
					n_y_1 = n_y_1 + deltay
					end if
					if ((FOV_yLower(i, int(j - 1.0d0)) .gt. 0.0d0) .or. (FOV_yUpper(i, int(j - 1.0d0)) .gt. 0.0d0)) then
					interaction_index_1 = 2.0d0
					n_y_1 = n_y_1 - deltay
					end if
					end if
					if ((n_x_1 .eq. 0.0d0) .and. (n_y_1 .eq. 0.0d0)) then
					if ((((((FOV_yLower(int(i + 1.0d0), int(j + 1.0d0)) .gt. 0.0d0) .or. (FOV_yUpper(int(i + 1.0d0), int(j + 1.0d0)) .gt. 0.0d0)) .or. (FOV_yLower(int(i + 1.0d0), int(j - 1.0d0)) .gt. 0.0d0)) .or. (FOV_yUpper(int(i + 1.0d0), int(j - 1.0d0)) .gt. 0.0d0)) .or. (FOV_yLower(int(i + 1.0d0), j) .gt. 0.0d0)) .or. (FOV_yUpper(int(i + 1.0d0), j) .gt. 0.0d0)) then
					interaction_index_1 = 2.0d0
					n_x_1 = n_x_1 + deltax
					end if
					if ((((((FOV_yLower(int(i - 1.0d0), int(j + 1.0d0)) .gt. 0.0d0) .or. (FOV_yUpper(int(i - 1.0d0), int(j + 1.0d0)) .gt. 0.0d0)) .or. (FOV_yLower(int(i - 1.0d0), int(j - 1.0d0)) .gt. 0.0d0)) .or. (FOV_yUpper(int(i - 1.0d0), int(j - 1.0d0)) .gt. 0.0d0)) .or. (FOV_yLower(int(i - 1.0d0), j) .gt. 0.0d0)) .or. (FOV_yUpper(int(i - 1.0d0), j) .gt. 0.0d0)) then
					interaction_index_1 = 2.0d0
					n_x_1 = n_x_1 - deltax
					end if
					if ((((((FOV_yLower(int(i + 1.0d0), int(j + 1.0d0)) .gt. 0.0d0) .or. (FOV_yUpper(int(i + 1.0d0), int(j + 1.0d0)) .gt. 0.0d0)) .or. (FOV_yLower(int(i - 1.0d0), int(j + 1.0d0)) .gt. 0.0d0)) .or. (FOV_yUpper(int(i - 1.0d0), int(j + 1.0d0)) .gt. 0.0d0)) .or. (FOV_yLower(i, int(j + 1.0d0)) .gt. 0.0d0)) .or. (FOV_yUpper(i, int(j + 1.0d0)) .gt. 0.0d0)) then
					interaction_index_1 = 2.0d0
					n_y_1 = n_y_1 + deltay
					end if
					if ((((((FOV_yLower(int(i + 1.0d0), int(j - 1.0d0)) .gt. 0.0d0) .or. (FOV_yUpper(int(i + 1.0d0), int(j - 1.0d0)) .gt. 0.0d0)) .or. (FOV_yLower(int(i - 1.0d0), int(j - 1.0d0)) .gt. 0.0d0)) .or. (FOV_yUpper(int(i - 1.0d0), int(j - 1.0d0)) .gt. 0.0d0)) .or. (FOV_yLower(i, int(j - 1.0d0)) .gt. 0.0d0)) .or. (FOV_yUpper(i, int(j - 1.0d0)) .gt. 0.0d0)) then
					interaction_index_1 = 2.0d0
					n_y_1 = n_y_1 - deltay
					end if
					end if
					if ((n_x_1 .ne. 0.0d0) .or. (n_y_1 .ne. 0.0d0)) then
					mod_normal_1 = SQRT(n_x_1 ** 2.0d0 + n_y_1 ** 2.0d0)
					n_x_1 = n_x_1 / mod_normal_1
					n_y_1 = n_y_1 / mod_normal_1
					end if
					if (interaction_index_1 .eq. 1.0d0) then
					fluxAccm_n_1 = fluxAccmx_1 * n_x_1 + fluxAccmy_1 * n_y_1
					fluxAccm_tx_1 = fluxAccmx_1 - (fluxAccmx_1 * n_x_1 + fluxAccmy_1 * n_y_1) * n_x_1
					fluxAccm_ty_1 = fluxAccmy_1 - (fluxAccmx_1 * n_x_1 + fluxAccmy_1 * n_y_1) * n_y_1
					m_n_1 = mx_p(i, j) * n_x_1 + my_p(i, j) * n_y_1
					m_tx_1 = mx_p(i, j) - (mx_p(i, j) * n_x_1 + my_p(i, j) * n_y_1) * n_x_1
					m_ty_1 = my_p(i, j) - (mx_p(i, j) * n_x_1 + my_p(i, j) * n_y_1) * n_y_1
					c_1 = c0 * ((rho_p(i, j) / rho0) ** ((gamma + (-1.0d0)) / 2.0d0))
					cn_1 = m_n_1 / rho_p(i, j)
					if ((c_1 + cn_1) .gt. 0.0d0) then
					fluxAccwnp_0_1 = fluxAccrho_1 * (-((-c_1) + cn_1)) + fluxAccm_n_1
					else
					fluxAccwnp_0_1 = (-lambdain) * (m_n_1 + u0 * n_x_1 * rho_p(i, j) * (200.0d0 * (0.0d0 + (i - (1 - cctk_lbnd(1))) * deltax) - 1.0d0))
					end if
					if (((-c_1) + cn_1) .gt. 0.0d0) then
					fluxAccwnm_0_1 = fluxAccrho_1 * (-(c_1 + cn_1)) + fluxAccm_n_1
					else
					fluxAccwnm_0_1 = (-lambdain) * (m_n_1 + u0 * n_x_1 * rho_p(i, j) * (200.0d0 * (0.0d0 + (i - (1 - cctk_lbnd(1))) * deltax) - 1.0d0))
					end if
					if (cn_1 .gt. 0.0d0) then
					fluxAccwtx_0_1 = fluxAccrho_1 * (-m_tx_1) / rho_p(i, j) + fluxAccm_tx_1
					else
					fluxAccwtx_0_1 = 0.0d0
					end if
					if (cn_1 .gt. 0.0d0) then
					fluxAccwty_0_1 = fluxAccrho_1 * (-m_ty_1) / rho_p(i, j) + fluxAccm_ty_1
					else
					fluxAccwty_0_1 = (-lambdain) * (my_p(i, j) - u0 * rho_p(i, j) * (200.0d0 * (0.0d0 + (j - (1 - cctk_lbnd(2))) * deltay) - 1.0d0))
					end if
					fluxAccrho_1 = fluxAccwnp_0_1 * 1.0d0 / (2.0d0 * c_1) + fluxAccwnm_0_1 * (-1.0d0) / (2.0d0 * c_1)
					fluxAccm_n_1 = fluxAccwnp_0_1 * (cn_1 / (2.0d0 * c_1) + 1.0d0 / 2.0d0) + fluxAccwnm_0_1 * (-(cn_1 / (2.0d0 * c_1) + (-1.0d0 / 2.0d0)))
					fluxAccm_tx_1 = (fluxAccwnp_0_1 * m_tx_1 / (2.0d0 * rho_p(i, j) * c_1) + fluxAccwnm_0_1 * (-m_tx_1) / (2.0d0 * rho_p(i, j) * c_1)) + fluxAccwtx_0_1
					fluxAccm_ty_1 = (fluxAccwnp_0_1 * m_ty_1 / (2.0d0 * rho_p(i, j) * c_1) + fluxAccwnm_0_1 * (-m_ty_1) / (2.0d0 * rho_p(i, j) * c_1)) + fluxAccwty_0_1
					fluxAccmx_1 = fluxAccm_tx_1 + fluxAccm_n_1 * n_x_1
					fluxAccmy_1 = fluxAccm_ty_1 + fluxAccm_n_1 * n_y_1
					end if
					if (interaction_index_1 .eq. 2.0d0) then
					fluxAccm_n_1 = fluxAccmx_1 * n_x_1 + fluxAccmy_1 * n_y_1
					fluxAccm_tx_1 = fluxAccmx_1 - (fluxAccmx_1 * n_x_1 + fluxAccmy_1 * n_y_1) * n_x_1
					fluxAccm_ty_1 = fluxAccmy_1 - (fluxAccmx_1 * n_x_1 + fluxAccmy_1 * n_y_1) * n_y_1
					m_n_1 = mx_p(i, j) * n_x_1 + my_p(i, j) * n_y_1
					m_tx_1 = mx_p(i, j) - (mx_p(i, j) * n_x_1 + my_p(i, j) * n_y_1) * n_x_1
					m_ty_1 = my_p(i, j) - (mx_p(i, j) * n_x_1 + my_p(i, j) * n_y_1) * n_y_1
					c_1 = c0 * ((rho_p(i, j) / rho0) ** ((gamma + (-1.0d0)) / 2.0d0))
					cn_1 = m_n_1 / rho_p(i, j)
					if ((c_1 + cn_1) .gt. 0.0d0) then
					fluxAccwnp_0_1 = fluxAccrho_1 * (-((-c_1) + cn_1)) + fluxAccm_n_1
					else
					fluxAccwnp_0_1 = ((-c_1) + cn_1) * lambdain * (rho_p(i, j) - rho0 * ((1.0d0 + (gamma * (u0 ** 2.0d0)) / (c0 ** 2.0d0) * (1.0d0 - 1.0d0 / 2.0d0 * ((200.0d0 * (0.0d0 + (i - (1 - cctk_lbnd(1))) * deltax) - 1.0d0) ** 2.0d0 + (200.0d0 * (0.0d0 + (j - (1 - cctk_lbnd(2))) * deltay) - 1.0d0) ** 2.0d0))) ** (1.0d0 / gamma)))
					end if
					if (((-c_1) + cn_1) .gt. 0.0d0) then
					fluxAccwnm_0_1 = fluxAccrho_1 * (-(c_1 + cn_1)) + fluxAccm_n_1
					else
					fluxAccwnm_0_1 = (c_1 + cn_1) * lambdain * (rho_p(i, j) - rho0 * ((1.0d0 + (gamma * (u0 ** 2.0d0)) / (c0 ** 2.0d0) * (1.0d0 - 1.0d0 / 2.0d0 * ((200.0d0 * (0.0d0 + (i - (1 - cctk_lbnd(1))) * deltax) - 1.0d0) ** 2.0d0 + (200.0d0 * (0.0d0 + (j - (1 - cctk_lbnd(2))) * deltay) - 1.0d0) ** 2.0d0))) ** (1.0d0 / gamma)))
					end if
					if (cn_1 .gt. 0.0d0) then
					fluxAccwtx_0_1 = fluxAccrho_1 * (-m_tx_1) / rho_p(i, j) + fluxAccm_tx_1
					else
					fluxAccwtx_0_1 = mx_p(i, j) / rho_p(i, j) * lambdain * (rho_p(i, j) - rho0 * ((1.0d0 + (gamma * (u0 ** 2.0d0)) / (c0 ** 2.0d0) * (1.0d0 - 1.0d0 / 2.0d0 * ((200.0d0 * (0.0d0 + (i - (1 - cctk_lbnd(1))) * deltax) - 1.0d0) ** 2.0d0 + (200.0d0 * (0.0d0 + (j - (1 - cctk_lbnd(2))) * deltay) - 1.0d0) ** 2.0d0))) ** (1.0d0 / gamma)))
					end if
					if (cn_1 .gt. 0.0d0) then
					fluxAccwty_0_1 = fluxAccrho_1 * (-m_ty_1) / rho_p(i, j) + fluxAccm_ty_1
					else
					fluxAccwty_0_1 = 0.0d0
					end if
					fluxAccrho_1 = fluxAccwnp_0_1 * 1.0d0 / (2.0d0 * c_1) + fluxAccwnm_0_1 * (-1.0d0) / (2.0d0 * c_1)
					fluxAccm_n_1 = fluxAccwnp_0_1 * (cn_1 / (2.0d0 * c_1) + 1.0d0 / 2.0d0) + fluxAccwnm_0_1 * (-(cn_1 / (2.0d0 * c_1) + (-1.0d0 / 2.0d0)))
					fluxAccm_tx_1 = (fluxAccwnp_0_1 * m_tx_1 / (2.0d0 * rho_p(i, j) * c_1) + fluxAccwnm_0_1 * (-m_tx_1) / (2.0d0 * rho_p(i, j) * c_1)) + fluxAccwtx_0_1
					fluxAccm_ty_1 = (fluxAccwnp_0_1 * m_ty_1 / (2.0d0 * rho_p(i, j) * c_1) + fluxAccwnm_0_1 * (-m_ty_1) / (2.0d0 * rho_p(i, j) * c_1)) + fluxAccwty_0_1
					fluxAccmx_1 = fluxAccm_tx_1 + fluxAccm_n_1 * n_x_1
					fluxAccmy_1 = fluxAccm_ty_1 + fluxAccm_n_1 * n_y_1
					end if
					k1rho(i, j) = RK3P1(CCTK_PASS_FTOF, fluxAccrho_1, rho_p, 0, i, j)
					k1mx(i, j) = RK3P1(CCTK_PASS_FTOF, fluxAccmx_1, mx_p, 0, i, j)
					k1my(i, j) = RK3P1(CCTK_PASS_FTOF, fluxAccmy_1, my_p, 0, i, j)
					k1p(i, j) = (Paux + ((k1rho(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					k1fx(i, j) = pfx
					k1fy(i, j) = pfy
				end if
			end do
		end do
		call CCTK_SyncGroup(status, cctkGH, "CompressibleCounterflow2D::block3")
		do j = jStart, jEnd
			do i = iStart, iEnd
				extrapolatedsubrhosupi(i, j) = k1rho(i, j)
				extrapolatedsubrhosupj(i, j) = k1rho(i, j)
				extrapolatedsubmxsupi(i, j) = k1mx(i, j)
				extrapolatedsubmxsupj(i, j) = k1mx(i, j)
				extrapolatedsubmysupi(i, j) = k1my(i, j)
				extrapolatedsubmysupj(i, j) = k1my(i, j)
				extrapolatedsubpsupi(i, j) = k1p(i, j)
				extrapolatedsubpsupj(i, j) = k1p(i, j)
				extrapolatedsubfxsupi(i, j) = k1fx(i, j)
				extrapolatedsubfxsupj(i, j) = k1fx(i, j)
				extrapolatedsubfysupi(i, j) = k1fy(i, j)
				extrapolatedsubfysupj(i, j) = k1fy(i, j)
			end do
		end do
		do j = jEnd/2, jEnd
			do i = iEnd/2, iEnd
!				Boundaries
		if ((FOV_xLower(i, j) .gt. 0) .and. ((i + 1 .le. cctk_lsh(1) .and. (FOV_1(int(i + 1), j) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. (FOV_1(int(i + 2), j) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. (FOV_1(int(i + 3), j) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 3)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_rho, k1rho, extrapolatedsubrhosupi, i, d_j_rho, k1rho, extrapolatedsubrhosupj, j, k1rho, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_mx, k1mx, extrapolatedsubmxsupi, i, d_j_mx, k1mx, extrapolatedsubmxsupj, j, k1mx, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_my, k1my, extrapolatedsubmysupi, i, d_j_my, k1my, extrapolatedsubmysupj, j, k1my, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_p(i, j) .ne. 0 .or. d_j_p(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_p(i, j)), int(j - d_j_p(i, j))) .gt. 0) then
					extrapolatedsubpsupi(i, j) = (Paux + ((extrapolatedsubrhosupi(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					extrapolatedsubpsupj(i, j) = (Paux + ((extrapolatedsubrhosupj(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					k1p(i, j) = (extrapolatedsubpsupi(i, j) + extrapolatedsubpsupj(i, j)) / 2.0d0
					else
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .eq. 0.0d0)) then
					k1p(i, j) = extrapolatedsubpsupi(i, j)
					extrapolatedsubpsupj(i, j) = k1p(i, j)
					else
					if ((d_i_p(i, j) .eq. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					k1p(i, j) = extrapolatedsubpsupj(i, j)
					extrapolatedsubpsupi(i, j) = k1p(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fx(i, j) .ne. 0 .or. d_j_fx(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fx(i, j)), int(j - d_j_fx(i, j))) .gt. 0) then
					extrapolatedsubfxsupi(i, j) = pfx
					extrapolatedsubfxsupj(i, j) = pfx
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					k1fx(i, j) = (extrapolatedsubfxsupi(i, j) + extrapolatedsubfxsupj(i, j)) / 2.0d0
					else
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .eq. 0.0d0)) then
					k1fx(i, j) = extrapolatedsubfxsupi(i, j)
					extrapolatedsubfxsupj(i, j) = k1fx(i, j)
					else
					if ((d_i_fx(i, j) .eq. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					k1fx(i, j) = extrapolatedsubfxsupj(i, j)
					extrapolatedsubfxsupi(i, j) = k1fx(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fy(i, j) .ne. 0 .or. d_j_fy(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fy(i, j)), int(j - d_j_fy(i, j))) .gt. 0) then
					extrapolatedsubfysupi(i, j) = pfy
					extrapolatedsubfysupj(i, j) = pfy
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					k1fy(i, j) = (extrapolatedsubfysupi(i, j) + extrapolatedsubfysupj(i, j)) / 2.0d0
					else
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .eq. 0.0d0)) then
					k1fy(i, j) = extrapolatedsubfysupi(i, j)
					extrapolatedsubfysupj(i, j) = k1fy(i, j)
					else
					if ((d_i_fy(i, j) .eq. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					k1fy(i, j) = extrapolatedsubfysupj(i, j)
					extrapolatedsubfysupi(i, j) = k1fy(i, j)
					end if
					end if
					end if
				end if
			end if
		end if
		if ((FOV_xUpper(i, j) .gt. 0) .and. ((i - 1 .ge. 1 .and. (FOV_1(int(i - 1), j) .gt. 0)) .or. (i - 2 .ge. 1 .and. (FOV_1(int(i - 2), j) .gt. 0)) .or. (i - 3 .ge. 1 .and. (FOV_1(int(i - 3), j) .gt. 0)) .or. ((i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_rho, k1rho, extrapolatedsubrhosupi, i, d_j_rho, k1rho, extrapolatedsubrhosupj, j, k1rho, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_mx, k1mx, extrapolatedsubmxsupi, i, d_j_mx, k1mx, extrapolatedsubmxsupj, j, k1mx, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_my, k1my, extrapolatedsubmysupi, i, d_j_my, k1my, extrapolatedsubmysupj, j, k1my, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_p(i, j) .ne. 0 .or. d_j_p(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_p(i, j)), int(j - d_j_p(i, j))) .gt. 0) then
					extrapolatedsubpsupi(i, j) = (Paux + ((extrapolatedsubrhosupi(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					extrapolatedsubpsupj(i, j) = (Paux + ((extrapolatedsubrhosupj(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					k1p(i, j) = (extrapolatedsubpsupi(i, j) + extrapolatedsubpsupj(i, j)) / 2.0d0
					else
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .eq. 0.0d0)) then
					k1p(i, j) = extrapolatedsubpsupi(i, j)
					extrapolatedsubpsupj(i, j) = k1p(i, j)
					else
					if ((d_i_p(i, j) .eq. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					k1p(i, j) = extrapolatedsubpsupj(i, j)
					extrapolatedsubpsupi(i, j) = k1p(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fx(i, j) .ne. 0 .or. d_j_fx(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fx(i, j)), int(j - d_j_fx(i, j))) .gt. 0) then
					extrapolatedsubfxsupi(i, j) = pfx
					extrapolatedsubfxsupj(i, j) = pfx
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					k1fx(i, j) = (extrapolatedsubfxsupi(i, j) + extrapolatedsubfxsupj(i, j)) / 2.0d0
					else
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .eq. 0.0d0)) then
					k1fx(i, j) = extrapolatedsubfxsupi(i, j)
					extrapolatedsubfxsupj(i, j) = k1fx(i, j)
					else
					if ((d_i_fx(i, j) .eq. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					k1fx(i, j) = extrapolatedsubfxsupj(i, j)
					extrapolatedsubfxsupi(i, j) = k1fx(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fy(i, j) .ne. 0 .or. d_j_fy(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fy(i, j)), int(j - d_j_fy(i, j))) .gt. 0) then
					extrapolatedsubfysupi(i, j) = pfy
					extrapolatedsubfysupj(i, j) = pfy
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					k1fy(i, j) = (extrapolatedsubfysupi(i, j) + extrapolatedsubfysupj(i, j)) / 2.0d0
					else
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .eq. 0.0d0)) then
					k1fy(i, j) = extrapolatedsubfysupi(i, j)
					extrapolatedsubfysupj(i, j) = k1fy(i, j)
					else
					if ((d_i_fy(i, j) .eq. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					k1fy(i, j) = extrapolatedsubfysupj(i, j)
					extrapolatedsubfysupi(i, j) = k1fy(i, j)
					end if
					end if
					end if
				end if
			end if
		end if
		if ((FOV_yLower(i, j) .gt. 0) .and. ((j + 1 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 1)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 2)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 3)) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_rho, k1rho, extrapolatedsubrhosupi, i, d_j_rho, k1rho, extrapolatedsubrhosupj, j, k1rho, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_mx, k1mx, extrapolatedsubmxsupi, i, d_j_mx, k1mx, extrapolatedsubmxsupj, j, k1mx, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_my, k1my, extrapolatedsubmysupi, i, d_j_my, k1my, extrapolatedsubmysupj, j, k1my, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_p(i, j) .ne. 0 .or. d_j_p(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_p(i, j)), int(j - d_j_p(i, j))) .gt. 0) then
					extrapolatedsubpsupi(i, j) = (Paux + ((extrapolatedsubrhosupi(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					extrapolatedsubpsupj(i, j) = (Paux + ((extrapolatedsubrhosupj(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					k1p(i, j) = (extrapolatedsubpsupi(i, j) + extrapolatedsubpsupj(i, j)) / 2.0d0
					else
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .eq. 0.0d0)) then
					k1p(i, j) = extrapolatedsubpsupi(i, j)
					extrapolatedsubpsupj(i, j) = k1p(i, j)
					else
					if ((d_i_p(i, j) .eq. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					k1p(i, j) = extrapolatedsubpsupj(i, j)
					extrapolatedsubpsupi(i, j) = k1p(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fx(i, j) .ne. 0 .or. d_j_fx(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fx(i, j)), int(j - d_j_fx(i, j))) .gt. 0) then
					extrapolatedsubfxsupi(i, j) = pfx
					extrapolatedsubfxsupj(i, j) = pfx
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					k1fx(i, j) = (extrapolatedsubfxsupi(i, j) + extrapolatedsubfxsupj(i, j)) / 2.0d0
					else
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .eq. 0.0d0)) then
					k1fx(i, j) = extrapolatedsubfxsupi(i, j)
					extrapolatedsubfxsupj(i, j) = k1fx(i, j)
					else
					if ((d_i_fx(i, j) .eq. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					k1fx(i, j) = extrapolatedsubfxsupj(i, j)
					extrapolatedsubfxsupi(i, j) = k1fx(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fy(i, j) .ne. 0 .or. d_j_fy(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fy(i, j)), int(j - d_j_fy(i, j))) .gt. 0) then
					extrapolatedsubfysupi(i, j) = pfy
					extrapolatedsubfysupj(i, j) = pfy
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					k1fy(i, j) = (extrapolatedsubfysupi(i, j) + extrapolatedsubfysupj(i, j)) / 2.0d0
					else
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .eq. 0.0d0)) then
					k1fy(i, j) = extrapolatedsubfysupi(i, j)
					extrapolatedsubfysupj(i, j) = k1fy(i, j)
					else
					if ((d_i_fy(i, j) .eq. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					k1fy(i, j) = extrapolatedsubfysupj(i, j)
					extrapolatedsubfysupi(i, j) = k1fy(i, j)
					end if
					end if
					end if
				end if
			end if
		end if
		if ((FOV_yUpper(i, j) .gt. 0) .and. ((j - 1 .ge. 1 .and. (FOV_1(i, int(j - 1)) .gt. 0)) .or. (j - 2 .ge. 1 .and. (FOV_1(i, int(j - 2)) .gt. 0)) .or. (j - 3 .ge. 1 .and. (FOV_1(i, int(j - 3)) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_rho, k1rho, extrapolatedsubrhosupi, i, d_j_rho, k1rho, extrapolatedsubrhosupj, j, k1rho, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_mx, k1mx, extrapolatedsubmxsupi, i, d_j_mx, k1mx, extrapolatedsubmxsupj, j, k1mx, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_my, k1my, extrapolatedsubmysupi, i, d_j_my, k1my, extrapolatedsubmysupj, j, k1my, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_p(i, j) .ne. 0 .or. d_j_p(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_p(i, j)), int(j - d_j_p(i, j))) .gt. 0) then
					extrapolatedsubpsupi(i, j) = (Paux + ((extrapolatedsubrhosupi(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					extrapolatedsubpsupj(i, j) = (Paux + ((extrapolatedsubrhosupj(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					k1p(i, j) = (extrapolatedsubpsupi(i, j) + extrapolatedsubpsupj(i, j)) / 2.0d0
					else
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .eq. 0.0d0)) then
					k1p(i, j) = extrapolatedsubpsupi(i, j)
					extrapolatedsubpsupj(i, j) = k1p(i, j)
					else
					if ((d_i_p(i, j) .eq. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					k1p(i, j) = extrapolatedsubpsupj(i, j)
					extrapolatedsubpsupi(i, j) = k1p(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fx(i, j) .ne. 0 .or. d_j_fx(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fx(i, j)), int(j - d_j_fx(i, j))) .gt. 0) then
					extrapolatedsubfxsupi(i, j) = pfx
					extrapolatedsubfxsupj(i, j) = pfx
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					k1fx(i, j) = (extrapolatedsubfxsupi(i, j) + extrapolatedsubfxsupj(i, j)) / 2.0d0
					else
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .eq. 0.0d0)) then
					k1fx(i, j) = extrapolatedsubfxsupi(i, j)
					extrapolatedsubfxsupj(i, j) = k1fx(i, j)
					else
					if ((d_i_fx(i, j) .eq. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					k1fx(i, j) = extrapolatedsubfxsupj(i, j)
					extrapolatedsubfxsupi(i, j) = k1fx(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fy(i, j) .ne. 0 .or. d_j_fy(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fy(i, j)), int(j - d_j_fy(i, j))) .gt. 0) then
					extrapolatedsubfysupi(i, j) = pfy
					extrapolatedsubfysupj(i, j) = pfy
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					k1fy(i, j) = (extrapolatedsubfysupi(i, j) + extrapolatedsubfysupj(i, j)) / 2.0d0
					else
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .eq. 0.0d0)) then
					k1fy(i, j) = extrapolatedsubfysupi(i, j)
					extrapolatedsubfysupj(i, j) = k1fy(i, j)
					else
					if ((d_i_fy(i, j) .eq. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					k1fy(i, j) = extrapolatedsubfysupj(i, j)
					extrapolatedsubfysupi(i, j) = k1fy(i, j)
					end if
					end if
					end if
				end if
			end if
		end if
			end do
		end do
		do j = jEnd/2 - 1, jStart, -1
			do i = iEnd/2, iEnd
!				Boundaries
		if ((FOV_xLower(i, j) .gt. 0) .and. ((i + 1 .le. cctk_lsh(1) .and. (FOV_1(int(i + 1), j) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. (FOV_1(int(i + 2), j) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. (FOV_1(int(i + 3), j) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 3)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_rho, k1rho, extrapolatedsubrhosupi, i, d_j_rho, k1rho, extrapolatedsubrhosupj, j, k1rho, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_mx, k1mx, extrapolatedsubmxsupi, i, d_j_mx, k1mx, extrapolatedsubmxsupj, j, k1mx, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_my, k1my, extrapolatedsubmysupi, i, d_j_my, k1my, extrapolatedsubmysupj, j, k1my, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_p(i, j) .ne. 0 .or. d_j_p(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_p(i, j)), int(j - d_j_p(i, j))) .gt. 0) then
					extrapolatedsubpsupi(i, j) = (Paux + ((extrapolatedsubrhosupi(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					extrapolatedsubpsupj(i, j) = (Paux + ((extrapolatedsubrhosupj(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					k1p(i, j) = (extrapolatedsubpsupi(i, j) + extrapolatedsubpsupj(i, j)) / 2.0d0
					else
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .eq. 0.0d0)) then
					k1p(i, j) = extrapolatedsubpsupi(i, j)
					extrapolatedsubpsupj(i, j) = k1p(i, j)
					else
					if ((d_i_p(i, j) .eq. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					k1p(i, j) = extrapolatedsubpsupj(i, j)
					extrapolatedsubpsupi(i, j) = k1p(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fx(i, j) .ne. 0 .or. d_j_fx(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fx(i, j)), int(j - d_j_fx(i, j))) .gt. 0) then
					extrapolatedsubfxsupi(i, j) = pfx
					extrapolatedsubfxsupj(i, j) = pfx
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					k1fx(i, j) = (extrapolatedsubfxsupi(i, j) + extrapolatedsubfxsupj(i, j)) / 2.0d0
					else
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .eq. 0.0d0)) then
					k1fx(i, j) = extrapolatedsubfxsupi(i, j)
					extrapolatedsubfxsupj(i, j) = k1fx(i, j)
					else
					if ((d_i_fx(i, j) .eq. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					k1fx(i, j) = extrapolatedsubfxsupj(i, j)
					extrapolatedsubfxsupi(i, j) = k1fx(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fy(i, j) .ne. 0 .or. d_j_fy(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fy(i, j)), int(j - d_j_fy(i, j))) .gt. 0) then
					extrapolatedsubfysupi(i, j) = pfy
					extrapolatedsubfysupj(i, j) = pfy
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					k1fy(i, j) = (extrapolatedsubfysupi(i, j) + extrapolatedsubfysupj(i, j)) / 2.0d0
					else
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .eq. 0.0d0)) then
					k1fy(i, j) = extrapolatedsubfysupi(i, j)
					extrapolatedsubfysupj(i, j) = k1fy(i, j)
					else
					if ((d_i_fy(i, j) .eq. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					k1fy(i, j) = extrapolatedsubfysupj(i, j)
					extrapolatedsubfysupi(i, j) = k1fy(i, j)
					end if
					end if
					end if
				end if
			end if
		end if
		if ((FOV_xUpper(i, j) .gt. 0) .and. ((i - 1 .ge. 1 .and. (FOV_1(int(i - 1), j) .gt. 0)) .or. (i - 2 .ge. 1 .and. (FOV_1(int(i - 2), j) .gt. 0)) .or. (i - 3 .ge. 1 .and. (FOV_1(int(i - 3), j) .gt. 0)) .or. ((i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_rho, k1rho, extrapolatedsubrhosupi, i, d_j_rho, k1rho, extrapolatedsubrhosupj, j, k1rho, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_mx, k1mx, extrapolatedsubmxsupi, i, d_j_mx, k1mx, extrapolatedsubmxsupj, j, k1mx, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_my, k1my, extrapolatedsubmysupi, i, d_j_my, k1my, extrapolatedsubmysupj, j, k1my, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_p(i, j) .ne. 0 .or. d_j_p(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_p(i, j)), int(j - d_j_p(i, j))) .gt. 0) then
					extrapolatedsubpsupi(i, j) = (Paux + ((extrapolatedsubrhosupi(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					extrapolatedsubpsupj(i, j) = (Paux + ((extrapolatedsubrhosupj(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					k1p(i, j) = (extrapolatedsubpsupi(i, j) + extrapolatedsubpsupj(i, j)) / 2.0d0
					else
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .eq. 0.0d0)) then
					k1p(i, j) = extrapolatedsubpsupi(i, j)
					extrapolatedsubpsupj(i, j) = k1p(i, j)
					else
					if ((d_i_p(i, j) .eq. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					k1p(i, j) = extrapolatedsubpsupj(i, j)
					extrapolatedsubpsupi(i, j) = k1p(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fx(i, j) .ne. 0 .or. d_j_fx(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fx(i, j)), int(j - d_j_fx(i, j))) .gt. 0) then
					extrapolatedsubfxsupi(i, j) = pfx
					extrapolatedsubfxsupj(i, j) = pfx
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					k1fx(i, j) = (extrapolatedsubfxsupi(i, j) + extrapolatedsubfxsupj(i, j)) / 2.0d0
					else
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .eq. 0.0d0)) then
					k1fx(i, j) = extrapolatedsubfxsupi(i, j)
					extrapolatedsubfxsupj(i, j) = k1fx(i, j)
					else
					if ((d_i_fx(i, j) .eq. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					k1fx(i, j) = extrapolatedsubfxsupj(i, j)
					extrapolatedsubfxsupi(i, j) = k1fx(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fy(i, j) .ne. 0 .or. d_j_fy(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fy(i, j)), int(j - d_j_fy(i, j))) .gt. 0) then
					extrapolatedsubfysupi(i, j) = pfy
					extrapolatedsubfysupj(i, j) = pfy
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					k1fy(i, j) = (extrapolatedsubfysupi(i, j) + extrapolatedsubfysupj(i, j)) / 2.0d0
					else
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .eq. 0.0d0)) then
					k1fy(i, j) = extrapolatedsubfysupi(i, j)
					extrapolatedsubfysupj(i, j) = k1fy(i, j)
					else
					if ((d_i_fy(i, j) .eq. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					k1fy(i, j) = extrapolatedsubfysupj(i, j)
					extrapolatedsubfysupi(i, j) = k1fy(i, j)
					end if
					end if
					end if
				end if
			end if
		end if
		if ((FOV_yLower(i, j) .gt. 0) .and. ((j + 1 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 1)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 2)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 3)) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_rho, k1rho, extrapolatedsubrhosupi, i, d_j_rho, k1rho, extrapolatedsubrhosupj, j, k1rho, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_mx, k1mx, extrapolatedsubmxsupi, i, d_j_mx, k1mx, extrapolatedsubmxsupj, j, k1mx, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_my, k1my, extrapolatedsubmysupi, i, d_j_my, k1my, extrapolatedsubmysupj, j, k1my, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_p(i, j) .ne. 0 .or. d_j_p(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_p(i, j)), int(j - d_j_p(i, j))) .gt. 0) then
					extrapolatedsubpsupi(i, j) = (Paux + ((extrapolatedsubrhosupi(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					extrapolatedsubpsupj(i, j) = (Paux + ((extrapolatedsubrhosupj(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					k1p(i, j) = (extrapolatedsubpsupi(i, j) + extrapolatedsubpsupj(i, j)) / 2.0d0
					else
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .eq. 0.0d0)) then
					k1p(i, j) = extrapolatedsubpsupi(i, j)
					extrapolatedsubpsupj(i, j) = k1p(i, j)
					else
					if ((d_i_p(i, j) .eq. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					k1p(i, j) = extrapolatedsubpsupj(i, j)
					extrapolatedsubpsupi(i, j) = k1p(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fx(i, j) .ne. 0 .or. d_j_fx(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fx(i, j)), int(j - d_j_fx(i, j))) .gt. 0) then
					extrapolatedsubfxsupi(i, j) = pfx
					extrapolatedsubfxsupj(i, j) = pfx
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					k1fx(i, j) = (extrapolatedsubfxsupi(i, j) + extrapolatedsubfxsupj(i, j)) / 2.0d0
					else
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .eq. 0.0d0)) then
					k1fx(i, j) = extrapolatedsubfxsupi(i, j)
					extrapolatedsubfxsupj(i, j) = k1fx(i, j)
					else
					if ((d_i_fx(i, j) .eq. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					k1fx(i, j) = extrapolatedsubfxsupj(i, j)
					extrapolatedsubfxsupi(i, j) = k1fx(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fy(i, j) .ne. 0 .or. d_j_fy(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fy(i, j)), int(j - d_j_fy(i, j))) .gt. 0) then
					extrapolatedsubfysupi(i, j) = pfy
					extrapolatedsubfysupj(i, j) = pfy
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					k1fy(i, j) = (extrapolatedsubfysupi(i, j) + extrapolatedsubfysupj(i, j)) / 2.0d0
					else
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .eq. 0.0d0)) then
					k1fy(i, j) = extrapolatedsubfysupi(i, j)
					extrapolatedsubfysupj(i, j) = k1fy(i, j)
					else
					if ((d_i_fy(i, j) .eq. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					k1fy(i, j) = extrapolatedsubfysupj(i, j)
					extrapolatedsubfysupi(i, j) = k1fy(i, j)
					end if
					end if
					end if
				end if
			end if
		end if
		if ((FOV_yUpper(i, j) .gt. 0) .and. ((j - 1 .ge. 1 .and. (FOV_1(i, int(j - 1)) .gt. 0)) .or. (j - 2 .ge. 1 .and. (FOV_1(i, int(j - 2)) .gt. 0)) .or. (j - 3 .ge. 1 .and. (FOV_1(i, int(j - 3)) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_rho, k1rho, extrapolatedsubrhosupi, i, d_j_rho, k1rho, extrapolatedsubrhosupj, j, k1rho, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_mx, k1mx, extrapolatedsubmxsupi, i, d_j_mx, k1mx, extrapolatedsubmxsupj, j, k1mx, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_my, k1my, extrapolatedsubmysupi, i, d_j_my, k1my, extrapolatedsubmysupj, j, k1my, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_p(i, j) .ne. 0 .or. d_j_p(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_p(i, j)), int(j - d_j_p(i, j))) .gt. 0) then
					extrapolatedsubpsupi(i, j) = (Paux + ((extrapolatedsubrhosupi(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					extrapolatedsubpsupj(i, j) = (Paux + ((extrapolatedsubrhosupj(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					k1p(i, j) = (extrapolatedsubpsupi(i, j) + extrapolatedsubpsupj(i, j)) / 2.0d0
					else
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .eq. 0.0d0)) then
					k1p(i, j) = extrapolatedsubpsupi(i, j)
					extrapolatedsubpsupj(i, j) = k1p(i, j)
					else
					if ((d_i_p(i, j) .eq. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					k1p(i, j) = extrapolatedsubpsupj(i, j)
					extrapolatedsubpsupi(i, j) = k1p(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fx(i, j) .ne. 0 .or. d_j_fx(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fx(i, j)), int(j - d_j_fx(i, j))) .gt. 0) then
					extrapolatedsubfxsupi(i, j) = pfx
					extrapolatedsubfxsupj(i, j) = pfx
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					k1fx(i, j) = (extrapolatedsubfxsupi(i, j) + extrapolatedsubfxsupj(i, j)) / 2.0d0
					else
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .eq. 0.0d0)) then
					k1fx(i, j) = extrapolatedsubfxsupi(i, j)
					extrapolatedsubfxsupj(i, j) = k1fx(i, j)
					else
					if ((d_i_fx(i, j) .eq. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					k1fx(i, j) = extrapolatedsubfxsupj(i, j)
					extrapolatedsubfxsupi(i, j) = k1fx(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fy(i, j) .ne. 0 .or. d_j_fy(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fy(i, j)), int(j - d_j_fy(i, j))) .gt. 0) then
					extrapolatedsubfysupi(i, j) = pfy
					extrapolatedsubfysupj(i, j) = pfy
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					k1fy(i, j) = (extrapolatedsubfysupi(i, j) + extrapolatedsubfysupj(i, j)) / 2.0d0
					else
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .eq. 0.0d0)) then
					k1fy(i, j) = extrapolatedsubfysupi(i, j)
					extrapolatedsubfysupj(i, j) = k1fy(i, j)
					else
					if ((d_i_fy(i, j) .eq. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					k1fy(i, j) = extrapolatedsubfysupj(i, j)
					extrapolatedsubfysupi(i, j) = k1fy(i, j)
					end if
					end if
					end if
				end if
			end if
		end if
			end do
		end do
		do j = jEnd/2, jEnd
			do i = iEnd/2 - 1, iStart, -1
!				Boundaries
		if ((FOV_xLower(i, j) .gt. 0) .and. ((i + 1 .le. cctk_lsh(1) .and. (FOV_1(int(i + 1), j) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. (FOV_1(int(i + 2), j) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. (FOV_1(int(i + 3), j) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 3)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_rho, k1rho, extrapolatedsubrhosupi, i, d_j_rho, k1rho, extrapolatedsubrhosupj, j, k1rho, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_mx, k1mx, extrapolatedsubmxsupi, i, d_j_mx, k1mx, extrapolatedsubmxsupj, j, k1mx, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_my, k1my, extrapolatedsubmysupi, i, d_j_my, k1my, extrapolatedsubmysupj, j, k1my, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_p(i, j) .ne. 0 .or. d_j_p(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_p(i, j)), int(j - d_j_p(i, j))) .gt. 0) then
					extrapolatedsubpsupi(i, j) = (Paux + ((extrapolatedsubrhosupi(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					extrapolatedsubpsupj(i, j) = (Paux + ((extrapolatedsubrhosupj(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					k1p(i, j) = (extrapolatedsubpsupi(i, j) + extrapolatedsubpsupj(i, j)) / 2.0d0
					else
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .eq. 0.0d0)) then
					k1p(i, j) = extrapolatedsubpsupi(i, j)
					extrapolatedsubpsupj(i, j) = k1p(i, j)
					else
					if ((d_i_p(i, j) .eq. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					k1p(i, j) = extrapolatedsubpsupj(i, j)
					extrapolatedsubpsupi(i, j) = k1p(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fx(i, j) .ne. 0 .or. d_j_fx(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fx(i, j)), int(j - d_j_fx(i, j))) .gt. 0) then
					extrapolatedsubfxsupi(i, j) = pfx
					extrapolatedsubfxsupj(i, j) = pfx
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					k1fx(i, j) = (extrapolatedsubfxsupi(i, j) + extrapolatedsubfxsupj(i, j)) / 2.0d0
					else
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .eq. 0.0d0)) then
					k1fx(i, j) = extrapolatedsubfxsupi(i, j)
					extrapolatedsubfxsupj(i, j) = k1fx(i, j)
					else
					if ((d_i_fx(i, j) .eq. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					k1fx(i, j) = extrapolatedsubfxsupj(i, j)
					extrapolatedsubfxsupi(i, j) = k1fx(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fy(i, j) .ne. 0 .or. d_j_fy(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fy(i, j)), int(j - d_j_fy(i, j))) .gt. 0) then
					extrapolatedsubfysupi(i, j) = pfy
					extrapolatedsubfysupj(i, j) = pfy
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					k1fy(i, j) = (extrapolatedsubfysupi(i, j) + extrapolatedsubfysupj(i, j)) / 2.0d0
					else
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .eq. 0.0d0)) then
					k1fy(i, j) = extrapolatedsubfysupi(i, j)
					extrapolatedsubfysupj(i, j) = k1fy(i, j)
					else
					if ((d_i_fy(i, j) .eq. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					k1fy(i, j) = extrapolatedsubfysupj(i, j)
					extrapolatedsubfysupi(i, j) = k1fy(i, j)
					end if
					end if
					end if
				end if
			end if
		end if
		if ((FOV_xUpper(i, j) .gt. 0) .and. ((i - 1 .ge. 1 .and. (FOV_1(int(i - 1), j) .gt. 0)) .or. (i - 2 .ge. 1 .and. (FOV_1(int(i - 2), j) .gt. 0)) .or. (i - 3 .ge. 1 .and. (FOV_1(int(i - 3), j) .gt. 0)) .or. ((i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_rho, k1rho, extrapolatedsubrhosupi, i, d_j_rho, k1rho, extrapolatedsubrhosupj, j, k1rho, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_mx, k1mx, extrapolatedsubmxsupi, i, d_j_mx, k1mx, extrapolatedsubmxsupj, j, k1mx, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_my, k1my, extrapolatedsubmysupi, i, d_j_my, k1my, extrapolatedsubmysupj, j, k1my, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_p(i, j) .ne. 0 .or. d_j_p(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_p(i, j)), int(j - d_j_p(i, j))) .gt. 0) then
					extrapolatedsubpsupi(i, j) = (Paux + ((extrapolatedsubrhosupi(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					extrapolatedsubpsupj(i, j) = (Paux + ((extrapolatedsubrhosupj(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					k1p(i, j) = (extrapolatedsubpsupi(i, j) + extrapolatedsubpsupj(i, j)) / 2.0d0
					else
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .eq. 0.0d0)) then
					k1p(i, j) = extrapolatedsubpsupi(i, j)
					extrapolatedsubpsupj(i, j) = k1p(i, j)
					else
					if ((d_i_p(i, j) .eq. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					k1p(i, j) = extrapolatedsubpsupj(i, j)
					extrapolatedsubpsupi(i, j) = k1p(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fx(i, j) .ne. 0 .or. d_j_fx(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fx(i, j)), int(j - d_j_fx(i, j))) .gt. 0) then
					extrapolatedsubfxsupi(i, j) = pfx
					extrapolatedsubfxsupj(i, j) = pfx
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					k1fx(i, j) = (extrapolatedsubfxsupi(i, j) + extrapolatedsubfxsupj(i, j)) / 2.0d0
					else
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .eq. 0.0d0)) then
					k1fx(i, j) = extrapolatedsubfxsupi(i, j)
					extrapolatedsubfxsupj(i, j) = k1fx(i, j)
					else
					if ((d_i_fx(i, j) .eq. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					k1fx(i, j) = extrapolatedsubfxsupj(i, j)
					extrapolatedsubfxsupi(i, j) = k1fx(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fy(i, j) .ne. 0 .or. d_j_fy(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fy(i, j)), int(j - d_j_fy(i, j))) .gt. 0) then
					extrapolatedsubfysupi(i, j) = pfy
					extrapolatedsubfysupj(i, j) = pfy
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					k1fy(i, j) = (extrapolatedsubfysupi(i, j) + extrapolatedsubfysupj(i, j)) / 2.0d0
					else
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .eq. 0.0d0)) then
					k1fy(i, j) = extrapolatedsubfysupi(i, j)
					extrapolatedsubfysupj(i, j) = k1fy(i, j)
					else
					if ((d_i_fy(i, j) .eq. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					k1fy(i, j) = extrapolatedsubfysupj(i, j)
					extrapolatedsubfysupi(i, j) = k1fy(i, j)
					end if
					end if
					end if
				end if
			end if
		end if
		if ((FOV_yLower(i, j) .gt. 0) .and. ((j + 1 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 1)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 2)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 3)) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_rho, k1rho, extrapolatedsubrhosupi, i, d_j_rho, k1rho, extrapolatedsubrhosupj, j, k1rho, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_mx, k1mx, extrapolatedsubmxsupi, i, d_j_mx, k1mx, extrapolatedsubmxsupj, j, k1mx, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_my, k1my, extrapolatedsubmysupi, i, d_j_my, k1my, extrapolatedsubmysupj, j, k1my, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_p(i, j) .ne. 0 .or. d_j_p(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_p(i, j)), int(j - d_j_p(i, j))) .gt. 0) then
					extrapolatedsubpsupi(i, j) = (Paux + ((extrapolatedsubrhosupi(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					extrapolatedsubpsupj(i, j) = (Paux + ((extrapolatedsubrhosupj(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					k1p(i, j) = (extrapolatedsubpsupi(i, j) + extrapolatedsubpsupj(i, j)) / 2.0d0
					else
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .eq. 0.0d0)) then
					k1p(i, j) = extrapolatedsubpsupi(i, j)
					extrapolatedsubpsupj(i, j) = k1p(i, j)
					else
					if ((d_i_p(i, j) .eq. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					k1p(i, j) = extrapolatedsubpsupj(i, j)
					extrapolatedsubpsupi(i, j) = k1p(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fx(i, j) .ne. 0 .or. d_j_fx(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fx(i, j)), int(j - d_j_fx(i, j))) .gt. 0) then
					extrapolatedsubfxsupi(i, j) = pfx
					extrapolatedsubfxsupj(i, j) = pfx
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					k1fx(i, j) = (extrapolatedsubfxsupi(i, j) + extrapolatedsubfxsupj(i, j)) / 2.0d0
					else
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .eq. 0.0d0)) then
					k1fx(i, j) = extrapolatedsubfxsupi(i, j)
					extrapolatedsubfxsupj(i, j) = k1fx(i, j)
					else
					if ((d_i_fx(i, j) .eq. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					k1fx(i, j) = extrapolatedsubfxsupj(i, j)
					extrapolatedsubfxsupi(i, j) = k1fx(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fy(i, j) .ne. 0 .or. d_j_fy(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fy(i, j)), int(j - d_j_fy(i, j))) .gt. 0) then
					extrapolatedsubfysupi(i, j) = pfy
					extrapolatedsubfysupj(i, j) = pfy
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					k1fy(i, j) = (extrapolatedsubfysupi(i, j) + extrapolatedsubfysupj(i, j)) / 2.0d0
					else
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .eq. 0.0d0)) then
					k1fy(i, j) = extrapolatedsubfysupi(i, j)
					extrapolatedsubfysupj(i, j) = k1fy(i, j)
					else
					if ((d_i_fy(i, j) .eq. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					k1fy(i, j) = extrapolatedsubfysupj(i, j)
					extrapolatedsubfysupi(i, j) = k1fy(i, j)
					end if
					end if
					end if
				end if
			end if
		end if
		if ((FOV_yUpper(i, j) .gt. 0) .and. ((j - 1 .ge. 1 .and. (FOV_1(i, int(j - 1)) .gt. 0)) .or. (j - 2 .ge. 1 .and. (FOV_1(i, int(j - 2)) .gt. 0)) .or. (j - 3 .ge. 1 .and. (FOV_1(i, int(j - 3)) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_rho, k1rho, extrapolatedsubrhosupi, i, d_j_rho, k1rho, extrapolatedsubrhosupj, j, k1rho, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_mx, k1mx, extrapolatedsubmxsupi, i, d_j_mx, k1mx, extrapolatedsubmxsupj, j, k1mx, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_my, k1my, extrapolatedsubmysupi, i, d_j_my, k1my, extrapolatedsubmysupj, j, k1my, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_p(i, j) .ne. 0 .or. d_j_p(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_p(i, j)), int(j - d_j_p(i, j))) .gt. 0) then
					extrapolatedsubpsupi(i, j) = (Paux + ((extrapolatedsubrhosupi(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					extrapolatedsubpsupj(i, j) = (Paux + ((extrapolatedsubrhosupj(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					k1p(i, j) = (extrapolatedsubpsupi(i, j) + extrapolatedsubpsupj(i, j)) / 2.0d0
					else
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .eq. 0.0d0)) then
					k1p(i, j) = extrapolatedsubpsupi(i, j)
					extrapolatedsubpsupj(i, j) = k1p(i, j)
					else
					if ((d_i_p(i, j) .eq. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					k1p(i, j) = extrapolatedsubpsupj(i, j)
					extrapolatedsubpsupi(i, j) = k1p(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fx(i, j) .ne. 0 .or. d_j_fx(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fx(i, j)), int(j - d_j_fx(i, j))) .gt. 0) then
					extrapolatedsubfxsupi(i, j) = pfx
					extrapolatedsubfxsupj(i, j) = pfx
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					k1fx(i, j) = (extrapolatedsubfxsupi(i, j) + extrapolatedsubfxsupj(i, j)) / 2.0d0
					else
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .eq. 0.0d0)) then
					k1fx(i, j) = extrapolatedsubfxsupi(i, j)
					extrapolatedsubfxsupj(i, j) = k1fx(i, j)
					else
					if ((d_i_fx(i, j) .eq. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					k1fx(i, j) = extrapolatedsubfxsupj(i, j)
					extrapolatedsubfxsupi(i, j) = k1fx(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fy(i, j) .ne. 0 .or. d_j_fy(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fy(i, j)), int(j - d_j_fy(i, j))) .gt. 0) then
					extrapolatedsubfysupi(i, j) = pfy
					extrapolatedsubfysupj(i, j) = pfy
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					k1fy(i, j) = (extrapolatedsubfysupi(i, j) + extrapolatedsubfysupj(i, j)) / 2.0d0
					else
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .eq. 0.0d0)) then
					k1fy(i, j) = extrapolatedsubfysupi(i, j)
					extrapolatedsubfysupj(i, j) = k1fy(i, j)
					else
					if ((d_i_fy(i, j) .eq. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					k1fy(i, j) = extrapolatedsubfysupj(i, j)
					extrapolatedsubfysupi(i, j) = k1fy(i, j)
					end if
					end if
					end if
				end if
			end if
		end if
			end do
		end do
		do j = jEnd/2 - 1, jStart, -1
			do i = iEnd/2 - 1, iStart, -1
!				Boundaries
		if ((FOV_xLower(i, j) .gt. 0) .and. ((i + 1 .le. cctk_lsh(1) .and. (FOV_1(int(i + 1), j) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. (FOV_1(int(i + 2), j) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. (FOV_1(int(i + 3), j) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 3)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_rho, k1rho, extrapolatedsubrhosupi, i, d_j_rho, k1rho, extrapolatedsubrhosupj, j, k1rho, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_mx, k1mx, extrapolatedsubmxsupi, i, d_j_mx, k1mx, extrapolatedsubmxsupj, j, k1mx, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_my, k1my, extrapolatedsubmysupi, i, d_j_my, k1my, extrapolatedsubmysupj, j, k1my, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_p(i, j) .ne. 0 .or. d_j_p(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_p(i, j)), int(j - d_j_p(i, j))) .gt. 0) then
					extrapolatedsubpsupi(i, j) = (Paux + ((extrapolatedsubrhosupi(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					extrapolatedsubpsupj(i, j) = (Paux + ((extrapolatedsubrhosupj(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					k1p(i, j) = (extrapolatedsubpsupi(i, j) + extrapolatedsubpsupj(i, j)) / 2.0d0
					else
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .eq. 0.0d0)) then
					k1p(i, j) = extrapolatedsubpsupi(i, j)
					extrapolatedsubpsupj(i, j) = k1p(i, j)
					else
					if ((d_i_p(i, j) .eq. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					k1p(i, j) = extrapolatedsubpsupj(i, j)
					extrapolatedsubpsupi(i, j) = k1p(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fx(i, j) .ne. 0 .or. d_j_fx(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fx(i, j)), int(j - d_j_fx(i, j))) .gt. 0) then
					extrapolatedsubfxsupi(i, j) = pfx
					extrapolatedsubfxsupj(i, j) = pfx
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					k1fx(i, j) = (extrapolatedsubfxsupi(i, j) + extrapolatedsubfxsupj(i, j)) / 2.0d0
					else
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .eq. 0.0d0)) then
					k1fx(i, j) = extrapolatedsubfxsupi(i, j)
					extrapolatedsubfxsupj(i, j) = k1fx(i, j)
					else
					if ((d_i_fx(i, j) .eq. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					k1fx(i, j) = extrapolatedsubfxsupj(i, j)
					extrapolatedsubfxsupi(i, j) = k1fx(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fy(i, j) .ne. 0 .or. d_j_fy(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fy(i, j)), int(j - d_j_fy(i, j))) .gt. 0) then
					extrapolatedsubfysupi(i, j) = pfy
					extrapolatedsubfysupj(i, j) = pfy
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					k1fy(i, j) = (extrapolatedsubfysupi(i, j) + extrapolatedsubfysupj(i, j)) / 2.0d0
					else
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .eq. 0.0d0)) then
					k1fy(i, j) = extrapolatedsubfysupi(i, j)
					extrapolatedsubfysupj(i, j) = k1fy(i, j)
					else
					if ((d_i_fy(i, j) .eq. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					k1fy(i, j) = extrapolatedsubfysupj(i, j)
					extrapolatedsubfysupi(i, j) = k1fy(i, j)
					end if
					end if
					end if
				end if
			end if
		end if
		if ((FOV_xUpper(i, j) .gt. 0) .and. ((i - 1 .ge. 1 .and. (FOV_1(int(i - 1), j) .gt. 0)) .or. (i - 2 .ge. 1 .and. (FOV_1(int(i - 2), j) .gt. 0)) .or. (i - 3 .ge. 1 .and. (FOV_1(int(i - 3), j) .gt. 0)) .or. ((i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_rho, k1rho, extrapolatedsubrhosupi, i, d_j_rho, k1rho, extrapolatedsubrhosupj, j, k1rho, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_mx, k1mx, extrapolatedsubmxsupi, i, d_j_mx, k1mx, extrapolatedsubmxsupj, j, k1mx, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_my, k1my, extrapolatedsubmysupi, i, d_j_my, k1my, extrapolatedsubmysupj, j, k1my, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_p(i, j) .ne. 0 .or. d_j_p(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_p(i, j)), int(j - d_j_p(i, j))) .gt. 0) then
					extrapolatedsubpsupi(i, j) = (Paux + ((extrapolatedsubrhosupi(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					extrapolatedsubpsupj(i, j) = (Paux + ((extrapolatedsubrhosupj(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					k1p(i, j) = (extrapolatedsubpsupi(i, j) + extrapolatedsubpsupj(i, j)) / 2.0d0
					else
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .eq. 0.0d0)) then
					k1p(i, j) = extrapolatedsubpsupi(i, j)
					extrapolatedsubpsupj(i, j) = k1p(i, j)
					else
					if ((d_i_p(i, j) .eq. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					k1p(i, j) = extrapolatedsubpsupj(i, j)
					extrapolatedsubpsupi(i, j) = k1p(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fx(i, j) .ne. 0 .or. d_j_fx(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fx(i, j)), int(j - d_j_fx(i, j))) .gt. 0) then
					extrapolatedsubfxsupi(i, j) = pfx
					extrapolatedsubfxsupj(i, j) = pfx
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					k1fx(i, j) = (extrapolatedsubfxsupi(i, j) + extrapolatedsubfxsupj(i, j)) / 2.0d0
					else
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .eq. 0.0d0)) then
					k1fx(i, j) = extrapolatedsubfxsupi(i, j)
					extrapolatedsubfxsupj(i, j) = k1fx(i, j)
					else
					if ((d_i_fx(i, j) .eq. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					k1fx(i, j) = extrapolatedsubfxsupj(i, j)
					extrapolatedsubfxsupi(i, j) = k1fx(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fy(i, j) .ne. 0 .or. d_j_fy(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fy(i, j)), int(j - d_j_fy(i, j))) .gt. 0) then
					extrapolatedsubfysupi(i, j) = pfy
					extrapolatedsubfysupj(i, j) = pfy
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					k1fy(i, j) = (extrapolatedsubfysupi(i, j) + extrapolatedsubfysupj(i, j)) / 2.0d0
					else
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .eq. 0.0d0)) then
					k1fy(i, j) = extrapolatedsubfysupi(i, j)
					extrapolatedsubfysupj(i, j) = k1fy(i, j)
					else
					if ((d_i_fy(i, j) .eq. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					k1fy(i, j) = extrapolatedsubfysupj(i, j)
					extrapolatedsubfysupi(i, j) = k1fy(i, j)
					end if
					end if
					end if
				end if
			end if
		end if
		if ((FOV_yLower(i, j) .gt. 0) .and. ((j + 1 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 1)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 2)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 3)) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_rho, k1rho, extrapolatedsubrhosupi, i, d_j_rho, k1rho, extrapolatedsubrhosupj, j, k1rho, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_mx, k1mx, extrapolatedsubmxsupi, i, d_j_mx, k1mx, extrapolatedsubmxsupj, j, k1mx, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_my, k1my, extrapolatedsubmysupi, i, d_j_my, k1my, extrapolatedsubmysupj, j, k1my, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_p(i, j) .ne. 0 .or. d_j_p(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_p(i, j)), int(j - d_j_p(i, j))) .gt. 0) then
					extrapolatedsubpsupi(i, j) = (Paux + ((extrapolatedsubrhosupi(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					extrapolatedsubpsupj(i, j) = (Paux + ((extrapolatedsubrhosupj(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					k1p(i, j) = (extrapolatedsubpsupi(i, j) + extrapolatedsubpsupj(i, j)) / 2.0d0
					else
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .eq. 0.0d0)) then
					k1p(i, j) = extrapolatedsubpsupi(i, j)
					extrapolatedsubpsupj(i, j) = k1p(i, j)
					else
					if ((d_i_p(i, j) .eq. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					k1p(i, j) = extrapolatedsubpsupj(i, j)
					extrapolatedsubpsupi(i, j) = k1p(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fx(i, j) .ne. 0 .or. d_j_fx(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fx(i, j)), int(j - d_j_fx(i, j))) .gt. 0) then
					extrapolatedsubfxsupi(i, j) = pfx
					extrapolatedsubfxsupj(i, j) = pfx
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					k1fx(i, j) = (extrapolatedsubfxsupi(i, j) + extrapolatedsubfxsupj(i, j)) / 2.0d0
					else
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .eq. 0.0d0)) then
					k1fx(i, j) = extrapolatedsubfxsupi(i, j)
					extrapolatedsubfxsupj(i, j) = k1fx(i, j)
					else
					if ((d_i_fx(i, j) .eq. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					k1fx(i, j) = extrapolatedsubfxsupj(i, j)
					extrapolatedsubfxsupi(i, j) = k1fx(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fy(i, j) .ne. 0 .or. d_j_fy(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fy(i, j)), int(j - d_j_fy(i, j))) .gt. 0) then
					extrapolatedsubfysupi(i, j) = pfy
					extrapolatedsubfysupj(i, j) = pfy
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					k1fy(i, j) = (extrapolatedsubfysupi(i, j) + extrapolatedsubfysupj(i, j)) / 2.0d0
					else
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .eq. 0.0d0)) then
					k1fy(i, j) = extrapolatedsubfysupi(i, j)
					extrapolatedsubfysupj(i, j) = k1fy(i, j)
					else
					if ((d_i_fy(i, j) .eq. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					k1fy(i, j) = extrapolatedsubfysupj(i, j)
					extrapolatedsubfysupi(i, j) = k1fy(i, j)
					end if
					end if
					end if
				end if
			end if
		end if
		if ((FOV_yUpper(i, j) .gt. 0) .and. ((j - 1 .ge. 1 .and. (FOV_1(i, int(j - 1)) .gt. 0)) .or. (j - 2 .ge. 1 .and. (FOV_1(i, int(j - 2)) .gt. 0)) .or. (j - 3 .ge. 1 .and. (FOV_1(i, int(j - 3)) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_rho, k1rho, extrapolatedsubrhosupi, i, d_j_rho, k1rho, extrapolatedsubrhosupj, j, k1rho, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_mx, k1mx, extrapolatedsubmxsupi, i, d_j_mx, k1mx, extrapolatedsubmxsupj, j, k1mx, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_my, k1my, extrapolatedsubmysupi, i, d_j_my, k1my, extrapolatedsubmysupj, j, k1my, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_p(i, j) .ne. 0 .or. d_j_p(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_p(i, j)), int(j - d_j_p(i, j))) .gt. 0) then
					extrapolatedsubpsupi(i, j) = (Paux + ((extrapolatedsubrhosupi(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					extrapolatedsubpsupj(i, j) = (Paux + ((extrapolatedsubrhosupj(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					k1p(i, j) = (extrapolatedsubpsupi(i, j) + extrapolatedsubpsupj(i, j)) / 2.0d0
					else
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .eq. 0.0d0)) then
					k1p(i, j) = extrapolatedsubpsupi(i, j)
					extrapolatedsubpsupj(i, j) = k1p(i, j)
					else
					if ((d_i_p(i, j) .eq. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					k1p(i, j) = extrapolatedsubpsupj(i, j)
					extrapolatedsubpsupi(i, j) = k1p(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fx(i, j) .ne. 0 .or. d_j_fx(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fx(i, j)), int(j - d_j_fx(i, j))) .gt. 0) then
					extrapolatedsubfxsupi(i, j) = pfx
					extrapolatedsubfxsupj(i, j) = pfx
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					k1fx(i, j) = (extrapolatedsubfxsupi(i, j) + extrapolatedsubfxsupj(i, j)) / 2.0d0
					else
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .eq. 0.0d0)) then
					k1fx(i, j) = extrapolatedsubfxsupi(i, j)
					extrapolatedsubfxsupj(i, j) = k1fx(i, j)
					else
					if ((d_i_fx(i, j) .eq. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					k1fx(i, j) = extrapolatedsubfxsupj(i, j)
					extrapolatedsubfxsupi(i, j) = k1fx(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fy(i, j) .ne. 0 .or. d_j_fy(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fy(i, j)), int(j - d_j_fy(i, j))) .gt. 0) then
					extrapolatedsubfysupi(i, j) = pfy
					extrapolatedsubfysupj(i, j) = pfy
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					k1fy(i, j) = (extrapolatedsubfysupi(i, j) + extrapolatedsubfysupj(i, j)) / 2.0d0
					else
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .eq. 0.0d0)) then
					k1fy(i, j) = extrapolatedsubfysupi(i, j)
					extrapolatedsubfysupj(i, j) = k1fy(i, j)
					else
					if ((d_i_fy(i, j) .eq. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					k1fy(i, j) = extrapolatedsubfysupj(i, j)
					extrapolatedsubfysupi(i, j) = k1fy(i, j)
					end if
					end if
					end if
				end if
			end if
		end if
			end do
		end do
		call CCTK_SyncGroup(status, cctkGH, "CompressibleCounterflow2D::block4")
		call CCTK_SyncGroup(status, cctkGH, "CompressibleCounterflow2D::block3")
		do j = jStart, jEnd
			do i = iStart, iEnd
				if ((FOV_1(i, j) .gt. 0 .or. (((i + 1 .le. cctk_lsh(1) .and. FOV_1(i + 1, j) .gt. 0) .or. (i + 2 .le. cctk_lsh(1) .and. FOV_1(i + 2, j) .gt. 0) .or. (j + 1 .le. cctk_lsh(2) .and. FOV_1(i, j + 1) .gt. 0) .or. (j + 2 .le. cctk_lsh(2) .and. FOV_1(i, j + 2) .gt. 0)) .or. ((i - 1 .ge. 1 .and. FOV_1(i - 1, j) .gt. 0) .or. (i - 2 .ge. 1 .and. FOV_1(i - 2, j) .gt. 0) .or. (j - 1 .ge. 1 .and. FOV_1(i, j - 1) .gt. 0) .or. (j - 2 .ge. 1 .and. FOV_1(i, j - 2) .gt. 0)) .or. (((i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1) .and. FOV_1(int(i + 1), int(j - 1)) .gt. 0) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2)) .and. FOV_1(int(i + 1), int(j + 1)) .gt. 0) .or. ((i - 1 .ge. 1 .and. j - 1 .ge. 1) .and. FOV_1(int(i - 1), int(j - 1)) .gt. 0) .or. ((i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2)) .and. FOV_1(int(i - 1), int(j + 1)) .gt. 0) .or. ((i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1) .and. FOV_1(int(i + 1), int(j - 2)) .gt. 0) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2)) .and. FOV_1(int(i + 1), int(j + 2)) .gt. 0) .or. ((i - 1 .ge. 1 .and. j - 2 .ge. 1) .and. FOV_1(int(i - 1), int(j - 2)) .gt. 0) .or. ((i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2)) .and. FOV_1(int(i - 1), int(j + 2)) .gt. 0) .or. ((i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1) .and. FOV_1(int(i + 2), int(j - 1)) .gt. 0) .or. ((i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2)) .and. FOV_1(int(i + 2), int(j + 1)) .gt. 0) .or. ((i - 2 .ge. 1 .and. j - 1 .ge. 1) .and. FOV_1(int(i - 2), int(j - 1)) .gt. 0) .or. ((i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2)) .and. FOV_1(int(i - 2), int(j + 1)) .gt. 0) .or. ((i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1) .and. FOV_1(int(i + 2), int(j - 2)) .gt. 0) .or. ((i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2)) .and. FOV_1(int(i + 2), int(j + 2)) .gt. 0) .or. ((i - 2 .ge. 1 .and. j - 2 .ge. 1) .and. FOV_1(int(i - 2), int(j - 2)) .gt. 0) .or. ((i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2)) .and. FOV_1(int(i - 2), int(j + 2)) .gt. 0))))) then
					if (((i .ge. 3) .and. (i .le. (cctk_lsh(1) - 3))) .and. ((j .ge. 3) .and. (j .le. (cctk_lsh(2) - 3)))) then
					Flux_extsubrhosupi_1(i, j) = Firho_FluidI(CCTK_PASS_FTOF, extrapolatedsubmxsupi(i, j))
					Flux_extsubrhosupj_1(i, j) = Fjrho_FluidI(CCTK_PASS_FTOF, extrapolatedsubmysupj(i, j))
					Flux_extsubmxsupi_1(i, j) = Fimx_FluidI(CCTK_PASS_FTOF, extrapolatedsubrhosupi(i, j), extrapolatedsubmxsupi(i, j), extrapolatedsubpsupi(i, j))
					Flux_extsubmxsupj_1(i, j) = Fjmx_FluidI(CCTK_PASS_FTOF, extrapolatedsubrhosupj(i, j), extrapolatedsubmxsupj(i, j), extrapolatedsubmysupj(i, j))
					Flux_extsubmysupi_1(i, j) = Fimy_FluidI(CCTK_PASS_FTOF, extrapolatedsubrhosupi(i, j), extrapolatedsubmxsupi(i, j), extrapolatedsubmysupi(i, j))
					Flux_extsubmysupj_1(i, j) = Fjmy_FluidI(CCTK_PASS_FTOF, extrapolatedsubrhosupj(i, j), extrapolatedsubmysupj(i, j), extrapolatedsubpsupj(i, j))
					Q_ii_mx_1 = lambda
					Q_ij_mx_1 = lambda
					Q_ii2_mx_1 = mu
					Q_jj_mx_1 = mu
					Q_ii3_mx_1 = mu
					Q_ji_mx_1 = mu
					Q_ji_my_1 = lambda
					Q_jj_my_1 = lambda
					Q_ii_my_1 = mu
					Q_jj2_my_1 = mu
					Q_ij_my_1 = mu
					Q_jj3_my_1 = mu
					Pm2_ii_mx_1 = k1mx(i - 2, j) / k1rho(i - 2, j)
					Pm2_ij_mx_1 = k1my(i, j - 2) / k1rho(i, j - 2)
					Pm2_ii2_mx_1 = k1mx(i - 2, j) / k1rho(i - 2, j)
					Pm2_jj_mx_1 = k1mx(i, j - 2) / k1rho(i, j - 2)
					Pm2_ii3_mx_1 = k1mx(i - 2, j) / k1rho(i - 2, j)
					Pm2_ji_mx_1 = k1my(i - 2, j) / k1rho(i - 2, j)
					Pm2_ji_my_1 = k1mx(i - 2, j) / k1rho(i - 2, j)
					Pm2_jj_my_1 = k1my(i, j - 2) / k1rho(i, j - 2)
					Pm2_ii_my_1 = k1my(i - 2, j) / k1rho(i - 2, j)
					Pm2_jj2_my_1 = k1my(i, j - 2) / k1rho(i, j - 2)
					Pm2_ij_my_1 = k1mx(i, j - 2) / k1rho(i, j - 2)
					Pm2_jj3_my_1 = k1my(i, j - 2) / k1rho(i, j - 2)
					Pm1_ii_mx_1 = k1mx(i - 1, j) / k1rho(i - 1, j)
					Pm1_ij_mx_1 = k1my(i, j - 1) / k1rho(i, j - 1)
					Pm1_ii2_mx_1 = k1mx(i - 1, j) / k1rho(i - 1, j)
					Pm1_jj_mx_1 = k1mx(i, j - 1) / k1rho(i, j - 1)
					Pm1_ii3_mx_1 = k1mx(i - 1, j) / k1rho(i - 1, j)
					Pm1_ji_mx_1 = k1my(i - 1, j) / k1rho(i - 1, j)
					Pm1_ji_my_1 = k1mx(i - 1, j) / k1rho(i - 1, j)
					Pm1_jj_my_1 = k1my(i, j - 1) / k1rho(i, j - 1)
					Pm1_ii_my_1 = k1my(i - 1, j) / k1rho(i - 1, j)
					Pm1_jj2_my_1 = k1my(i, j - 1) / k1rho(i, j - 1)
					Pm1_ij_my_1 = k1mx(i, j - 1) / k1rho(i, j - 1)
					Pm1_jj3_my_1 = k1my(i, j - 1) / k1rho(i, j - 1)
					Pp1_ii_mx_1 = k1mx(i + 1, j) / k1rho(i + 1, j)
					Pp1_ij_mx_1 = k1my(i, j + 1) / k1rho(i, j + 1)
					Pp1_ii2_mx_1 = k1mx(i + 1, j) / k1rho(i + 1, j)
					Pp1_jj_mx_1 = k1mx(i, j + 1) / k1rho(i, j + 1)
					Pp1_ii3_mx_1 = k1mx(i + 1, j) / k1rho(i + 1, j)
					Pp1_ji_mx_1 = k1my(i + 1, j) / k1rho(i + 1, j)
					Pp1_ji_my_1 = k1mx(i + 1, j) / k1rho(i + 1, j)
					Pp1_jj_my_1 = k1my(i, j + 1) / k1rho(i, j + 1)
					Pp1_ii_my_1 = k1my(i + 1, j) / k1rho(i + 1, j)
					Pp1_jj2_my_1 = k1my(i, j + 1) / k1rho(i, j + 1)
					Pp1_ij_my_1 = k1mx(i, j + 1) / k1rho(i, j + 1)
					Pp1_jj3_my_1 = k1my(i, j + 1) / k1rho(i, j + 1)
					Pp2_ii_mx_1 = k1mx(i + 2, j) / k1rho(i + 2, j)
					Pp2_ij_mx_1 = k1my(i, j + 2) / k1rho(i, j + 2)
					Pp2_ii2_mx_1 = k1mx(i + 2, j) / k1rho(i + 2, j)
					Pp2_jj_mx_1 = k1mx(i, j + 2) / k1rho(i, j + 2)
					Pp2_ii3_mx_1 = k1mx(i + 2, j) / k1rho(i + 2, j)
					Pp2_ji_mx_1 = k1my(i + 2, j) / k1rho(i + 2, j)
					Pp2_ji_my_1 = k1mx(i + 2, j) / k1rho(i + 2, j)
					Pp2_jj_my_1 = k1my(i, j + 2) / k1rho(i, j + 2)
					Pp2_ii_my_1 = k1my(i + 2, j) / k1rho(i + 2, j)
					Pp2_jj2_my_1 = k1my(i, j + 2) / k1rho(i, j + 2)
					Pp2_ij_my_1 = k1mx(i, j + 2) / k1rho(i, j + 2)
					Pp2_jj3_my_1 = k1my(i, j + 2) / k1rho(i, j + 2)
					Flux_extsubmxsupi_1(i, j) = Flux_extsubmxsupi_1(i, j) - Q_ii_mx_1 * CDi(CCTK_PASS_FTOF, Pm2_ii_mx_1, Pm1_ii_mx_1, Pp1_ii_mx_1, Pp2_ii_mx_1)
					Flux_extsubmxsupi_1(i, j) = Flux_extsubmxsupi_1(i, j) - Q_ij_mx_1 * CDj(CCTK_PASS_FTOF, Pm2_ij_mx_1, Pm1_ij_mx_1, Pp1_ij_mx_1, Pp2_ij_mx_1)
					Flux_extsubmxsupi_1(i, j) = Flux_extsubmxsupi_1(i, j) - Q_ii2_mx_1 * CDi(CCTK_PASS_FTOF, Pm2_ii2_mx_1, Pm1_ii2_mx_1, Pp1_ii2_mx_1, Pp2_ii2_mx_1)
					Flux_extsubmxsupj_1(i, j) = Flux_extsubmxsupj_1(i, j) - Q_jj_mx_1 * CDj(CCTK_PASS_FTOF, Pm2_jj_mx_1, Pm1_jj_mx_1, Pp1_jj_mx_1, Pp2_jj_mx_1)
					Flux_extsubmxsupi_1(i, j) = Flux_extsubmxsupi_1(i, j) - Q_ii3_mx_1 * CDi(CCTK_PASS_FTOF, Pm2_ii3_mx_1, Pm1_ii3_mx_1, Pp1_ii3_mx_1, Pp2_ii3_mx_1)
					Flux_extsubmxsupj_1(i, j) = Flux_extsubmxsupj_1(i, j) - Q_ji_mx_1 * CDi(CCTK_PASS_FTOF, Pm2_ji_mx_1, Pm1_ji_mx_1, Pp1_ji_mx_1, Pp2_ji_mx_1)
					Flux_extsubmysupj_1(i, j) = Flux_extsubmysupj_1(i, j) - Q_ji_my_1 * CDi(CCTK_PASS_FTOF, Pm2_ji_my_1, Pm1_ji_my_1, Pp1_ji_my_1, Pp2_ji_my_1)
					Flux_extsubmysupj_1(i, j) = Flux_extsubmysupj_1(i, j) - Q_jj_my_1 * CDj(CCTK_PASS_FTOF, Pm2_jj_my_1, Pm1_jj_my_1, Pp1_jj_my_1, Pp2_jj_my_1)
					Flux_extsubmysupi_1(i, j) = Flux_extsubmysupi_1(i, j) - Q_ii_my_1 * CDi(CCTK_PASS_FTOF, Pm2_ii_my_1, Pm1_ii_my_1, Pp1_ii_my_1, Pp2_ii_my_1)
					Flux_extsubmysupj_1(i, j) = Flux_extsubmysupj_1(i, j) - Q_jj2_my_1 * CDj(CCTK_PASS_FTOF, Pm2_jj2_my_1, Pm1_jj2_my_1, Pp1_jj2_my_1, Pp2_jj2_my_1)
					Flux_extsubmysupi_1(i, j) = Flux_extsubmysupi_1(i, j) - Q_ij_my_1 * CDj(CCTK_PASS_FTOF, Pm2_ij_my_1, Pm1_ij_my_1, Pp1_ij_my_1, Pp2_ij_my_1)
					Flux_extsubmysupj_1(i, j) = Flux_extsubmysupj_1(i, j) - Q_jj3_my_1 * CDj(CCTK_PASS_FTOF, Pm2_jj3_my_1, Pm1_jj3_my_1, Pp1_jj3_my_1, Pp2_jj3_my_1)
					end if
					m_n_1 = extrapolatedsubmxsupi(i, j)
					m_tx_1 = 0.0d0
					m_ty_1 = extrapolatedsubmysupi(i, j)
					c_x_1 = c0 * ((extrapolatedsubrhosupi(i, j) / rho0) ** ((gamma + (-1.0d0)) / 2.0d0))
					cn_x_1 = m_n_1 / extrapolatedsubrhosupi(i, j)
					m_n_1 = extrapolatedsubmysupj(i, j)
					m_tx_1 = extrapolatedsubmxsupj(i, j)
					m_ty_1 = 0.0d0
					c_y_1 = c0 * ((extrapolatedsubrhosupj(i, j) / rho0) ** ((gamma + (-1.0d0)) / 2.0d0))
					cn_y_1 = m_n_1 / extrapolatedsubrhosupj(i, j)
					Speedi_1(i, j) = MAX(ABS(c_x_1 + cn_x_1), ABS((-c_x_1) + cn_x_1), ABS(cn_x_1), ABS(cn_x_1))
					Speedj_1(i, j) = MAX(ABS(c_y_1 + cn_y_1), ABS((-c_y_1) + cn_y_1), ABS(cn_y_1), ABS(cn_y_1))
				end if
			end do
		end do
		do j = jEnd/2, jEnd
			do i = iEnd/2, iEnd
!				Boundaries
		if ((FOV_xLower(i, j) .gt. 0) .and. ((i + 1 .le. cctk_lsh(1) .and. (FOV_1(int(i + 1), j) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. (FOV_1(int(i + 2), j) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. (FOV_1(int(i + 3), j) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 3)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_rho, Flux_extsubrhosupi_1, Flux_extsubrhosupi_1, i, d_j_rho, Flux_extsubrhosupj_1, Flux_extsubrhosupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_mx, Flux_extsubmxsupi_1, Flux_extsubmxsupi_1, i, d_j_mx, Flux_extsubmxsupj_1, Flux_extsubmxsupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_my, Flux_extsubmysupi_1, Flux_extsubmysupi_1, i, d_j_my, Flux_extsubmysupj_1, Flux_extsubmysupj_1, j, FOV_1)
			end if
		end if
		if ((FOV_xUpper(i, j) .gt. 0) .and. ((i - 1 .ge. 1 .and. (FOV_1(int(i - 1), j) .gt. 0)) .or. (i - 2 .ge. 1 .and. (FOV_1(int(i - 2), j) .gt. 0)) .or. (i - 3 .ge. 1 .and. (FOV_1(int(i - 3), j) .gt. 0)) .or. ((i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_rho, Flux_extsubrhosupi_1, Flux_extsubrhosupi_1, i, d_j_rho, Flux_extsubrhosupj_1, Flux_extsubrhosupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_mx, Flux_extsubmxsupi_1, Flux_extsubmxsupi_1, i, d_j_mx, Flux_extsubmxsupj_1, Flux_extsubmxsupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_my, Flux_extsubmysupi_1, Flux_extsubmysupi_1, i, d_j_my, Flux_extsubmysupj_1, Flux_extsubmysupj_1, j, FOV_1)
			end if
		end if
		if ((FOV_yLower(i, j) .gt. 0) .and. ((j + 1 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 1)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 2)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 3)) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_rho, Flux_extsubrhosupi_1, Flux_extsubrhosupi_1, i, d_j_rho, Flux_extsubrhosupj_1, Flux_extsubrhosupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_mx, Flux_extsubmxsupi_1, Flux_extsubmxsupi_1, i, d_j_mx, Flux_extsubmxsupj_1, Flux_extsubmxsupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_my, Flux_extsubmysupi_1, Flux_extsubmysupi_1, i, d_j_my, Flux_extsubmysupj_1, Flux_extsubmysupj_1, j, FOV_1)
			end if
		end if
		if ((FOV_yUpper(i, j) .gt. 0) .and. ((j - 1 .ge. 1 .and. (FOV_1(i, int(j - 1)) .gt. 0)) .or. (j - 2 .ge. 1 .and. (FOV_1(i, int(j - 2)) .gt. 0)) .or. (j - 3 .ge. 1 .and. (FOV_1(i, int(j - 3)) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_rho, Flux_extsubrhosupi_1, Flux_extsubrhosupi_1, i, d_j_rho, Flux_extsubrhosupj_1, Flux_extsubrhosupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_mx, Flux_extsubmxsupi_1, Flux_extsubmxsupi_1, i, d_j_mx, Flux_extsubmxsupj_1, Flux_extsubmxsupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_my, Flux_extsubmysupi_1, Flux_extsubmysupi_1, i, d_j_my, Flux_extsubmysupj_1, Flux_extsubmysupj_1, j, FOV_1)
			end if
		end if
			end do
		end do
		do j = jEnd/2 - 1, jStart, -1
			do i = iEnd/2, iEnd
!				Boundaries
		if ((FOV_xLower(i, j) .gt. 0) .and. ((i + 1 .le. cctk_lsh(1) .and. (FOV_1(int(i + 1), j) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. (FOV_1(int(i + 2), j) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. (FOV_1(int(i + 3), j) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 3)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_rho, Flux_extsubrhosupi_1, Flux_extsubrhosupi_1, i, d_j_rho, Flux_extsubrhosupj_1, Flux_extsubrhosupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_mx, Flux_extsubmxsupi_1, Flux_extsubmxsupi_1, i, d_j_mx, Flux_extsubmxsupj_1, Flux_extsubmxsupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_my, Flux_extsubmysupi_1, Flux_extsubmysupi_1, i, d_j_my, Flux_extsubmysupj_1, Flux_extsubmysupj_1, j, FOV_1)
			end if
		end if
		if ((FOV_xUpper(i, j) .gt. 0) .and. ((i - 1 .ge. 1 .and. (FOV_1(int(i - 1), j) .gt. 0)) .or. (i - 2 .ge. 1 .and. (FOV_1(int(i - 2), j) .gt. 0)) .or. (i - 3 .ge. 1 .and. (FOV_1(int(i - 3), j) .gt. 0)) .or. ((i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_rho, Flux_extsubrhosupi_1, Flux_extsubrhosupi_1, i, d_j_rho, Flux_extsubrhosupj_1, Flux_extsubrhosupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_mx, Flux_extsubmxsupi_1, Flux_extsubmxsupi_1, i, d_j_mx, Flux_extsubmxsupj_1, Flux_extsubmxsupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_my, Flux_extsubmysupi_1, Flux_extsubmysupi_1, i, d_j_my, Flux_extsubmysupj_1, Flux_extsubmysupj_1, j, FOV_1)
			end if
		end if
		if ((FOV_yLower(i, j) .gt. 0) .and. ((j + 1 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 1)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 2)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 3)) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_rho, Flux_extsubrhosupi_1, Flux_extsubrhosupi_1, i, d_j_rho, Flux_extsubrhosupj_1, Flux_extsubrhosupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_mx, Flux_extsubmxsupi_1, Flux_extsubmxsupi_1, i, d_j_mx, Flux_extsubmxsupj_1, Flux_extsubmxsupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_my, Flux_extsubmysupi_1, Flux_extsubmysupi_1, i, d_j_my, Flux_extsubmysupj_1, Flux_extsubmysupj_1, j, FOV_1)
			end if
		end if
		if ((FOV_yUpper(i, j) .gt. 0) .and. ((j - 1 .ge. 1 .and. (FOV_1(i, int(j - 1)) .gt. 0)) .or. (j - 2 .ge. 1 .and. (FOV_1(i, int(j - 2)) .gt. 0)) .or. (j - 3 .ge. 1 .and. (FOV_1(i, int(j - 3)) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_rho, Flux_extsubrhosupi_1, Flux_extsubrhosupi_1, i, d_j_rho, Flux_extsubrhosupj_1, Flux_extsubrhosupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_mx, Flux_extsubmxsupi_1, Flux_extsubmxsupi_1, i, d_j_mx, Flux_extsubmxsupj_1, Flux_extsubmxsupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_my, Flux_extsubmysupi_1, Flux_extsubmysupi_1, i, d_j_my, Flux_extsubmysupj_1, Flux_extsubmysupj_1, j, FOV_1)
			end if
		end if
			end do
		end do
		do j = jEnd/2, jEnd
			do i = iEnd/2 - 1, iStart, -1
!				Boundaries
		if ((FOV_xLower(i, j) .gt. 0) .and. ((i + 1 .le. cctk_lsh(1) .and. (FOV_1(int(i + 1), j) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. (FOV_1(int(i + 2), j) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. (FOV_1(int(i + 3), j) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 3)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_rho, Flux_extsubrhosupi_1, Flux_extsubrhosupi_1, i, d_j_rho, Flux_extsubrhosupj_1, Flux_extsubrhosupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_mx, Flux_extsubmxsupi_1, Flux_extsubmxsupi_1, i, d_j_mx, Flux_extsubmxsupj_1, Flux_extsubmxsupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_my, Flux_extsubmysupi_1, Flux_extsubmysupi_1, i, d_j_my, Flux_extsubmysupj_1, Flux_extsubmysupj_1, j, FOV_1)
			end if
		end if
		if ((FOV_xUpper(i, j) .gt. 0) .and. ((i - 1 .ge. 1 .and. (FOV_1(int(i - 1), j) .gt. 0)) .or. (i - 2 .ge. 1 .and. (FOV_1(int(i - 2), j) .gt. 0)) .or. (i - 3 .ge. 1 .and. (FOV_1(int(i - 3), j) .gt. 0)) .or. ((i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_rho, Flux_extsubrhosupi_1, Flux_extsubrhosupi_1, i, d_j_rho, Flux_extsubrhosupj_1, Flux_extsubrhosupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_mx, Flux_extsubmxsupi_1, Flux_extsubmxsupi_1, i, d_j_mx, Flux_extsubmxsupj_1, Flux_extsubmxsupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_my, Flux_extsubmysupi_1, Flux_extsubmysupi_1, i, d_j_my, Flux_extsubmysupj_1, Flux_extsubmysupj_1, j, FOV_1)
			end if
		end if
		if ((FOV_yLower(i, j) .gt. 0) .and. ((j + 1 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 1)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 2)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 3)) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_rho, Flux_extsubrhosupi_1, Flux_extsubrhosupi_1, i, d_j_rho, Flux_extsubrhosupj_1, Flux_extsubrhosupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_mx, Flux_extsubmxsupi_1, Flux_extsubmxsupi_1, i, d_j_mx, Flux_extsubmxsupj_1, Flux_extsubmxsupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_my, Flux_extsubmysupi_1, Flux_extsubmysupi_1, i, d_j_my, Flux_extsubmysupj_1, Flux_extsubmysupj_1, j, FOV_1)
			end if
		end if
		if ((FOV_yUpper(i, j) .gt. 0) .and. ((j - 1 .ge. 1 .and. (FOV_1(i, int(j - 1)) .gt. 0)) .or. (j - 2 .ge. 1 .and. (FOV_1(i, int(j - 2)) .gt. 0)) .or. (j - 3 .ge. 1 .and. (FOV_1(i, int(j - 3)) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_rho, Flux_extsubrhosupi_1, Flux_extsubrhosupi_1, i, d_j_rho, Flux_extsubrhosupj_1, Flux_extsubrhosupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_mx, Flux_extsubmxsupi_1, Flux_extsubmxsupi_1, i, d_j_mx, Flux_extsubmxsupj_1, Flux_extsubmxsupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_my, Flux_extsubmysupi_1, Flux_extsubmysupi_1, i, d_j_my, Flux_extsubmysupj_1, Flux_extsubmysupj_1, j, FOV_1)
			end if
		end if
			end do
		end do
		do j = jEnd/2 - 1, jStart, -1
			do i = iEnd/2 - 1, iStart, -1
!				Boundaries
		if ((FOV_xLower(i, j) .gt. 0) .and. ((i + 1 .le. cctk_lsh(1) .and. (FOV_1(int(i + 1), j) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. (FOV_1(int(i + 2), j) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. (FOV_1(int(i + 3), j) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 3)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_rho, Flux_extsubrhosupi_1, Flux_extsubrhosupi_1, i, d_j_rho, Flux_extsubrhosupj_1, Flux_extsubrhosupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_mx, Flux_extsubmxsupi_1, Flux_extsubmxsupi_1, i, d_j_mx, Flux_extsubmxsupj_1, Flux_extsubmxsupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_my, Flux_extsubmysupi_1, Flux_extsubmysupi_1, i, d_j_my, Flux_extsubmysupj_1, Flux_extsubmysupj_1, j, FOV_1)
			end if
		end if
		if ((FOV_xUpper(i, j) .gt. 0) .and. ((i - 1 .ge. 1 .and. (FOV_1(int(i - 1), j) .gt. 0)) .or. (i - 2 .ge. 1 .and. (FOV_1(int(i - 2), j) .gt. 0)) .or. (i - 3 .ge. 1 .and. (FOV_1(int(i - 3), j) .gt. 0)) .or. ((i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_rho, Flux_extsubrhosupi_1, Flux_extsubrhosupi_1, i, d_j_rho, Flux_extsubrhosupj_1, Flux_extsubrhosupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_mx, Flux_extsubmxsupi_1, Flux_extsubmxsupi_1, i, d_j_mx, Flux_extsubmxsupj_1, Flux_extsubmxsupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_my, Flux_extsubmysupi_1, Flux_extsubmysupi_1, i, d_j_my, Flux_extsubmysupj_1, Flux_extsubmysupj_1, j, FOV_1)
			end if
		end if
		if ((FOV_yLower(i, j) .gt. 0) .and. ((j + 1 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 1)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 2)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 3)) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_rho, Flux_extsubrhosupi_1, Flux_extsubrhosupi_1, i, d_j_rho, Flux_extsubrhosupj_1, Flux_extsubrhosupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_mx, Flux_extsubmxsupi_1, Flux_extsubmxsupi_1, i, d_j_mx, Flux_extsubmxsupj_1, Flux_extsubmxsupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_my, Flux_extsubmysupi_1, Flux_extsubmysupi_1, i, d_j_my, Flux_extsubmysupj_1, Flux_extsubmysupj_1, j, FOV_1)
			end if
		end if
		if ((FOV_yUpper(i, j) .gt. 0) .and. ((j - 1 .ge. 1 .and. (FOV_1(i, int(j - 1)) .gt. 0)) .or. (j - 2 .ge. 1 .and. (FOV_1(i, int(j - 2)) .gt. 0)) .or. (j - 3 .ge. 1 .and. (FOV_1(i, int(j - 3)) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_rho, Flux_extsubrhosupi_1, Flux_extsubrhosupi_1, i, d_j_rho, Flux_extsubrhosupj_1, Flux_extsubrhosupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_mx, Flux_extsubmxsupi_1, Flux_extsubmxsupi_1, i, d_j_mx, Flux_extsubmxsupj_1, Flux_extsubmxsupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_my, Flux_extsubmysupi_1, Flux_extsubmysupi_1, i, d_j_my, Flux_extsubmysupj_1, Flux_extsubmysupj_1, j, FOV_1)
			end if
		end if
			end do
		end do
		call CCTK_SyncGroup(status, cctkGH, "CompressibleCounterflow2D::block1")
		do j = jStart, jEnd
			do i = iStart, iEnd
				if ((FOV_1(i, j) .gt. 0 .or. (((i + 1 .le. cctk_lsh(1) .and. FOV_1(i + 1, j) .gt. 0) .or. (j + 1 .le. cctk_lsh(2) .and. FOV_1(i, j + 1) .gt. 0)) .or. ((i - 1 .ge. 1 .and. FOV_1(i - 1, j) .gt. 0) .or. (j - 1 .ge. 1 .and. FOV_1(i, j - 1) .gt. 0)))) .and. (i + 1 .le. cctk_lsh(1) .and. i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. j - 1 .ge. 1)) then
					Speedi_1(i, j) = MAX(Speedi_1(i, j), Speedi_1(i + 1, j))
					Speedj_1(i, j) = MAX(Speedj_1(i, j), Speedj_1(i, j + 1))
				end if
			end do
		end do
		call CCTK_SyncGroup(status, cctkGH, "CompressibleCounterflow2D::block2")
		do j = jStart, jEnd
			do i = iStart, iEnd
				if ((FOV_1(i, j) .gt. 0) .and. (i + 2 .le. cctk_lsh(1) .and. i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. j - 2 .ge. 1)) then
					fluxAccrho_1 = 0.0d0
					fluxAccmx_1 = k1rho(i, j) * k1fx(i, j)
					fluxAccmy_1 = k1rho(i, j) * k1fy(i, j)
					fluxAccrho_1 = fluxAccrho_1 - FDOCi(CCTK_PASS_FTOF, extrapolatedsubrhosupi, Flux_extsubrhosupi_1, Speedi_1, i, j)
					fluxAccrho_1 = fluxAccrho_1 - FDOCj(CCTK_PASS_FTOF, extrapolatedsubrhosupj, Flux_extsubrhosupj_1, Speedj_1, i, j)
					fluxAccmx_1 = fluxAccmx_1 - FDOCi(CCTK_PASS_FTOF, extrapolatedsubmxsupi, Flux_extsubmxsupi_1, Speedi_1, i, j)
					fluxAccmx_1 = fluxAccmx_1 - FDOCj(CCTK_PASS_FTOF, extrapolatedsubmxsupj, Flux_extsubmxsupj_1, Speedj_1, i, j)
					fluxAccmy_1 = fluxAccmy_1 - FDOCi(CCTK_PASS_FTOF, extrapolatedsubmysupi, Flux_extsubmysupi_1, Speedi_1, i, j)
					fluxAccmy_1 = fluxAccmy_1 - FDOCj(CCTK_PASS_FTOF, extrapolatedsubmysupj, Flux_extsubmysupj_1, Speedj_1, i, j)
					n_x_1 = 0.0d0
					n_y_1 = 0.0d0
					interaction_index_1 = 0.0d0
					if ((n_x_1 .eq. 0.0d0) .and. (n_y_1 .eq. 0.0d0)) then
					if ((FOV_xLower(int(i + 1.0d0), j) .gt. 0.0d0) .or. (FOV_xUpper(int(i + 1.0d0), j) .gt. 0.0d0)) then
					interaction_index_1 = 1.0d0
					n_x_1 = n_x_1 + deltax
					end if
					if ((FOV_xLower(int(i - 1.0d0), j) .gt. 0.0d0) .or. (FOV_xUpper(int(i - 1.0d0), j) .gt. 0.0d0)) then
					interaction_index_1 = 1.0d0
					n_x_1 = n_x_1 - deltax
					end if
					if ((FOV_xLower(i, int(j + 1.0d0)) .gt. 0.0d0) .or. (FOV_xUpper(i, int(j + 1.0d0)) .gt. 0.0d0)) then
					interaction_index_1 = 1.0d0
					n_y_1 = n_y_1 + deltay
					end if
					if ((FOV_xLower(i, int(j - 1.0d0)) .gt. 0.0d0) .or. (FOV_xUpper(i, int(j - 1.0d0)) .gt. 0.0d0)) then
					interaction_index_1 = 1.0d0
					n_y_1 = n_y_1 - deltay
					end if
					end if
					if ((n_x_1 .eq. 0.0d0) .and. (n_y_1 .eq. 0.0d0)) then
					if ((((((FOV_xLower(int(i + 1.0d0), int(j + 1.0d0)) .gt. 0.0d0) .or. (FOV_xUpper(int(i + 1.0d0), int(j + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i + 1.0d0), int(j - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i + 1.0d0), int(j - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i + 1.0d0), j) .gt. 0.0d0)) .or. (FOV_xUpper(int(i + 1.0d0), j) .gt. 0.0d0)) then
					interaction_index_1 = 1.0d0
					n_x_1 = n_x_1 + deltax
					end if
					if ((((((FOV_xLower(int(i - 1.0d0), int(j + 1.0d0)) .gt. 0.0d0) .or. (FOV_xUpper(int(i - 1.0d0), int(j + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i - 1.0d0), int(j - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i - 1.0d0), int(j - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i - 1.0d0), j) .gt. 0.0d0)) .or. (FOV_xUpper(int(i - 1.0d0), j) .gt. 0.0d0)) then
					interaction_index_1 = 1.0d0
					n_x_1 = n_x_1 - deltax
					end if
					if ((((((FOV_xLower(int(i + 1.0d0), int(j + 1.0d0)) .gt. 0.0d0) .or. (FOV_xUpper(int(i + 1.0d0), int(j + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i - 1.0d0), int(j + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i - 1.0d0), int(j + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(i, int(j + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(i, int(j + 1.0d0)) .gt. 0.0d0)) then
					interaction_index_1 = 1.0d0
					n_y_1 = n_y_1 + deltay
					end if
					if ((((((FOV_xLower(int(i + 1.0d0), int(j - 1.0d0)) .gt. 0.0d0) .or. (FOV_xUpper(int(i + 1.0d0), int(j - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i - 1.0d0), int(j - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i - 1.0d0), int(j - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(i, int(j - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(i, int(j - 1.0d0)) .gt. 0.0d0)) then
					interaction_index_1 = 1.0d0
					n_y_1 = n_y_1 - deltay
					end if
					end if
					if ((n_x_1 .eq. 0.0d0) .and. (n_y_1 .eq. 0.0d0)) then
					if ((FOV_yLower(int(i + 1.0d0), j) .gt. 0.0d0) .or. (FOV_yUpper(int(i + 1.0d0), j) .gt. 0.0d0)) then
					interaction_index_1 = 2.0d0
					n_x_1 = n_x_1 + deltax
					end if
					if ((FOV_yLower(int(i - 1.0d0), j) .gt. 0.0d0) .or. (FOV_yUpper(int(i - 1.0d0), j) .gt. 0.0d0)) then
					interaction_index_1 = 2.0d0
					n_x_1 = n_x_1 - deltax
					end if
					if ((FOV_yLower(i, int(j + 1.0d0)) .gt. 0.0d0) .or. (FOV_yUpper(i, int(j + 1.0d0)) .gt. 0.0d0)) then
					interaction_index_1 = 2.0d0
					n_y_1 = n_y_1 + deltay
					end if
					if ((FOV_yLower(i, int(j - 1.0d0)) .gt. 0.0d0) .or. (FOV_yUpper(i, int(j - 1.0d0)) .gt. 0.0d0)) then
					interaction_index_1 = 2.0d0
					n_y_1 = n_y_1 - deltay
					end if
					end if
					if ((n_x_1 .eq. 0.0d0) .and. (n_y_1 .eq. 0.0d0)) then
					if ((((((FOV_yLower(int(i + 1.0d0), int(j + 1.0d0)) .gt. 0.0d0) .or. (FOV_yUpper(int(i + 1.0d0), int(j + 1.0d0)) .gt. 0.0d0)) .or. (FOV_yLower(int(i + 1.0d0), int(j - 1.0d0)) .gt. 0.0d0)) .or. (FOV_yUpper(int(i + 1.0d0), int(j - 1.0d0)) .gt. 0.0d0)) .or. (FOV_yLower(int(i + 1.0d0), j) .gt. 0.0d0)) .or. (FOV_yUpper(int(i + 1.0d0), j) .gt. 0.0d0)) then
					interaction_index_1 = 2.0d0
					n_x_1 = n_x_1 + deltax
					end if
					if ((((((FOV_yLower(int(i - 1.0d0), int(j + 1.0d0)) .gt. 0.0d0) .or. (FOV_yUpper(int(i - 1.0d0), int(j + 1.0d0)) .gt. 0.0d0)) .or. (FOV_yLower(int(i - 1.0d0), int(j - 1.0d0)) .gt. 0.0d0)) .or. (FOV_yUpper(int(i - 1.0d0), int(j - 1.0d0)) .gt. 0.0d0)) .or. (FOV_yLower(int(i - 1.0d0), j) .gt. 0.0d0)) .or. (FOV_yUpper(int(i - 1.0d0), j) .gt. 0.0d0)) then
					interaction_index_1 = 2.0d0
					n_x_1 = n_x_1 - deltax
					end if
					if ((((((FOV_yLower(int(i + 1.0d0), int(j + 1.0d0)) .gt. 0.0d0) .or. (FOV_yUpper(int(i + 1.0d0), int(j + 1.0d0)) .gt. 0.0d0)) .or. (FOV_yLower(int(i - 1.0d0), int(j + 1.0d0)) .gt. 0.0d0)) .or. (FOV_yUpper(int(i - 1.0d0), int(j + 1.0d0)) .gt. 0.0d0)) .or. (FOV_yLower(i, int(j + 1.0d0)) .gt. 0.0d0)) .or. (FOV_yUpper(i, int(j + 1.0d0)) .gt. 0.0d0)) then
					interaction_index_1 = 2.0d0
					n_y_1 = n_y_1 + deltay
					end if
					if ((((((FOV_yLower(int(i + 1.0d0), int(j - 1.0d0)) .gt. 0.0d0) .or. (FOV_yUpper(int(i + 1.0d0), int(j - 1.0d0)) .gt. 0.0d0)) .or. (FOV_yLower(int(i - 1.0d0), int(j - 1.0d0)) .gt. 0.0d0)) .or. (FOV_yUpper(int(i - 1.0d0), int(j - 1.0d0)) .gt. 0.0d0)) .or. (FOV_yLower(i, int(j - 1.0d0)) .gt. 0.0d0)) .or. (FOV_yUpper(i, int(j - 1.0d0)) .gt. 0.0d0)) then
					interaction_index_1 = 2.0d0
					n_y_1 = n_y_1 - deltay
					end if
					end if
					if ((n_x_1 .ne. 0.0d0) .or. (n_y_1 .ne. 0.0d0)) then
					mod_normal_1 = SQRT(n_x_1 ** 2.0d0 + n_y_1 ** 2.0d0)
					n_x_1 = n_x_1 / mod_normal_1
					n_y_1 = n_y_1 / mod_normal_1
					end if
					if (interaction_index_1 .eq. 1.0d0) then
					fluxAccm_n_1 = fluxAccmx_1 * n_x_1 + fluxAccmy_1 * n_y_1
					fluxAccm_tx_1 = fluxAccmx_1 - (fluxAccmx_1 * n_x_1 + fluxAccmy_1 * n_y_1) * n_x_1
					fluxAccm_ty_1 = fluxAccmy_1 - (fluxAccmx_1 * n_x_1 + fluxAccmy_1 * n_y_1) * n_y_1
					m_n_1 = k1mx(i, j) * n_x_1 + k1my(i, j) * n_y_1
					m_tx_1 = k1mx(i, j) - (k1mx(i, j) * n_x_1 + k1my(i, j) * n_y_1) * n_x_1
					m_ty_1 = k1my(i, j) - (k1mx(i, j) * n_x_1 + k1my(i, j) * n_y_1) * n_y_1
					c_1 = c0 * ((k1rho(i, j) / rho0) ** ((gamma + (-1.0d0)) / 2.0d0))
					cn_1 = m_n_1 / k1rho(i, j)
					if ((c_1 + cn_1) .gt. 0.0d0) then
					fluxAccwnp_0_1 = fluxAccrho_1 * (-((-c_1) + cn_1)) + fluxAccm_n_1
					else
					fluxAccwnp_0_1 = (-lambdain) * (m_n_1 + u0 * n_x_1 * k1rho(i, j) * (200.0d0 * (0.0d0 + (i - (1 - cctk_lbnd(1))) * deltax) - 1.0d0))
					end if
					if (((-c_1) + cn_1) .gt. 0.0d0) then
					fluxAccwnm_0_1 = fluxAccrho_1 * (-(c_1 + cn_1)) + fluxAccm_n_1
					else
					fluxAccwnm_0_1 = (-lambdain) * (m_n_1 + u0 * n_x_1 * k1rho(i, j) * (200.0d0 * (0.0d0 + (i - (1 - cctk_lbnd(1))) * deltax) - 1.0d0))
					end if
					if (cn_1 .gt. 0.0d0) then
					fluxAccwtx_0_1 = fluxAccrho_1 * (-m_tx_1) / k1rho(i, j) + fluxAccm_tx_1
					else
					fluxAccwtx_0_1 = 0.0d0
					end if
					if (cn_1 .gt. 0.0d0) then
					fluxAccwty_0_1 = fluxAccrho_1 * (-m_ty_1) / k1rho(i, j) + fluxAccm_ty_1
					else
					fluxAccwty_0_1 = (-lambdain) * (k1my(i, j) - u0 * k1rho(i, j) * (200.0d0 * (0.0d0 + (j - (1 - cctk_lbnd(2))) * deltay) - 1.0d0))
					end if
					fluxAccrho_1 = fluxAccwnp_0_1 * 1.0d0 / (2.0d0 * c_1) + fluxAccwnm_0_1 * (-1.0d0) / (2.0d0 * c_1)
					fluxAccm_n_1 = fluxAccwnp_0_1 * (cn_1 / (2.0d0 * c_1) + 1.0d0 / 2.0d0) + fluxAccwnm_0_1 * (-(cn_1 / (2.0d0 * c_1) + (-1.0d0 / 2.0d0)))
					fluxAccm_tx_1 = (fluxAccwnp_0_1 * m_tx_1 / (2.0d0 * k1rho(i, j) * c_1) + fluxAccwnm_0_1 * (-m_tx_1) / (2.0d0 * k1rho(i, j) * c_1)) + fluxAccwtx_0_1
					fluxAccm_ty_1 = (fluxAccwnp_0_1 * m_ty_1 / (2.0d0 * k1rho(i, j) * c_1) + fluxAccwnm_0_1 * (-m_ty_1) / (2.0d0 * k1rho(i, j) * c_1)) + fluxAccwty_0_1
					fluxAccmx_1 = fluxAccm_tx_1 + fluxAccm_n_1 * n_x_1
					fluxAccmy_1 = fluxAccm_ty_1 + fluxAccm_n_1 * n_y_1
					end if
					if (interaction_index_1 .eq. 2.0d0) then
					fluxAccm_n_1 = fluxAccmx_1 * n_x_1 + fluxAccmy_1 * n_y_1
					fluxAccm_tx_1 = fluxAccmx_1 - (fluxAccmx_1 * n_x_1 + fluxAccmy_1 * n_y_1) * n_x_1
					fluxAccm_ty_1 = fluxAccmy_1 - (fluxAccmx_1 * n_x_1 + fluxAccmy_1 * n_y_1) * n_y_1
					m_n_1 = k1mx(i, j) * n_x_1 + k1my(i, j) * n_y_1
					m_tx_1 = k1mx(i, j) - (k1mx(i, j) * n_x_1 + k1my(i, j) * n_y_1) * n_x_1
					m_ty_1 = k1my(i, j) - (k1mx(i, j) * n_x_1 + k1my(i, j) * n_y_1) * n_y_1
					c_1 = c0 * ((k1rho(i, j) / rho0) ** ((gamma + (-1.0d0)) / 2.0d0))
					cn_1 = m_n_1 / k1rho(i, j)
					if ((c_1 + cn_1) .gt. 0.0d0) then
					fluxAccwnp_0_1 = fluxAccrho_1 * (-((-c_1) + cn_1)) + fluxAccm_n_1
					else
					fluxAccwnp_0_1 = ((-c_1) + cn_1) * lambdain * (k1rho(i, j) - rho0 * ((1.0d0 + (gamma * (u0 ** 2.0d0)) / (c0 ** 2.0d0) * (1.0d0 - 1.0d0 / 2.0d0 * ((200.0d0 * (0.0d0 + (i - (1 - cctk_lbnd(1))) * deltax) - 1.0d0) ** 2.0d0 + (200.0d0 * (0.0d0 + (j - (1 - cctk_lbnd(2))) * deltay) - 1.0d0) ** 2.0d0))) ** (1.0d0 / gamma)))
					end if
					if (((-c_1) + cn_1) .gt. 0.0d0) then
					fluxAccwnm_0_1 = fluxAccrho_1 * (-(c_1 + cn_1)) + fluxAccm_n_1
					else
					fluxAccwnm_0_1 = (c_1 + cn_1) * lambdain * (k1rho(i, j) - rho0 * ((1.0d0 + (gamma * (u0 ** 2.0d0)) / (c0 ** 2.0d0) * (1.0d0 - 1.0d0 / 2.0d0 * ((200.0d0 * (0.0d0 + (i - (1 - cctk_lbnd(1))) * deltax) - 1.0d0) ** 2.0d0 + (200.0d0 * (0.0d0 + (j - (1 - cctk_lbnd(2))) * deltay) - 1.0d0) ** 2.0d0))) ** (1.0d0 / gamma)))
					end if
					if (cn_1 .gt. 0.0d0) then
					fluxAccwtx_0_1 = fluxAccrho_1 * (-m_tx_1) / k1rho(i, j) + fluxAccm_tx_1
					else
					fluxAccwtx_0_1 = k1mx(i, j) / k1rho(i, j) * lambdain * (k1rho(i, j) - rho0 * ((1.0d0 + (gamma * (u0 ** 2.0d0)) / (c0 ** 2.0d0) * (1.0d0 - 1.0d0 / 2.0d0 * ((200.0d0 * (0.0d0 + (i - (1 - cctk_lbnd(1))) * deltax) - 1.0d0) ** 2.0d0 + (200.0d0 * (0.0d0 + (j - (1 - cctk_lbnd(2))) * deltay) - 1.0d0) ** 2.0d0))) ** (1.0d0 / gamma)))
					end if
					if (cn_1 .gt. 0.0d0) then
					fluxAccwty_0_1 = fluxAccrho_1 * (-m_ty_1) / k1rho(i, j) + fluxAccm_ty_1
					else
					fluxAccwty_0_1 = 0.0d0
					end if
					fluxAccrho_1 = fluxAccwnp_0_1 * 1.0d0 / (2.0d0 * c_1) + fluxAccwnm_0_1 * (-1.0d0) / (2.0d0 * c_1)
					fluxAccm_n_1 = fluxAccwnp_0_1 * (cn_1 / (2.0d0 * c_1) + 1.0d0 / 2.0d0) + fluxAccwnm_0_1 * (-(cn_1 / (2.0d0 * c_1) + (-1.0d0 / 2.0d0)))
					fluxAccm_tx_1 = (fluxAccwnp_0_1 * m_tx_1 / (2.0d0 * k1rho(i, j) * c_1) + fluxAccwnm_0_1 * (-m_tx_1) / (2.0d0 * k1rho(i, j) * c_1)) + fluxAccwtx_0_1
					fluxAccm_ty_1 = (fluxAccwnp_0_1 * m_ty_1 / (2.0d0 * k1rho(i, j) * c_1) + fluxAccwnm_0_1 * (-m_ty_1) / (2.0d0 * k1rho(i, j) * c_1)) + fluxAccwty_0_1
					fluxAccmx_1 = fluxAccm_tx_1 + fluxAccm_n_1 * n_x_1
					fluxAccmy_1 = fluxAccm_ty_1 + fluxAccm_n_1 * n_y_1
					end if
					k2rho(i, j) = RK3P2(CCTK_PASS_FTOF, fluxAccrho_1, rho_p, k1rho, i, j)
					k2mx(i, j) = RK3P2(CCTK_PASS_FTOF, fluxAccmx_1, mx_p, k1mx, i, j)
					k2my(i, j) = RK3P2(CCTK_PASS_FTOF, fluxAccmy_1, my_p, k1my, i, j)
					k2p(i, j) = (Paux + ((k2rho(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					k2fx(i, j) = pfx
					k2fy(i, j) = pfy
				end if
			end do
		end do
		call CCTK_SyncGroup(status, cctkGH, "CompressibleCounterflow2D::block8")
		do j = jStart, jEnd
			do i = iStart, iEnd
				extrapolatedsubrhosupi(i, j) = k2rho(i, j)
				extrapolatedsubrhosupj(i, j) = k2rho(i, j)
				extrapolatedsubmxsupi(i, j) = k2mx(i, j)
				extrapolatedsubmxsupj(i, j) = k2mx(i, j)
				extrapolatedsubmysupi(i, j) = k2my(i, j)
				extrapolatedsubmysupj(i, j) = k2my(i, j)
				extrapolatedsubpsupi(i, j) = k2p(i, j)
				extrapolatedsubpsupj(i, j) = k2p(i, j)
				extrapolatedsubfxsupi(i, j) = k2fx(i, j)
				extrapolatedsubfxsupj(i, j) = k2fx(i, j)
				extrapolatedsubfysupi(i, j) = k2fy(i, j)
				extrapolatedsubfysupj(i, j) = k2fy(i, j)
			end do
		end do
		do j = jEnd/2, jEnd
			do i = iEnd/2, iEnd
!				Boundaries
		if ((FOV_xLower(i, j) .gt. 0) .and. ((i + 1 .le. cctk_lsh(1) .and. (FOV_1(int(i + 1), j) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. (FOV_1(int(i + 2), j) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. (FOV_1(int(i + 3), j) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 3)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_rho, k2rho, extrapolatedsubrhosupi, i, d_j_rho, k2rho, extrapolatedsubrhosupj, j, k2rho, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_mx, k2mx, extrapolatedsubmxsupi, i, d_j_mx, k2mx, extrapolatedsubmxsupj, j, k2mx, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_my, k2my, extrapolatedsubmysupi, i, d_j_my, k2my, extrapolatedsubmysupj, j, k2my, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_p(i, j) .ne. 0 .or. d_j_p(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_p(i, j)), int(j - d_j_p(i, j))) .gt. 0) then
					extrapolatedsubpsupi(i, j) = (Paux + ((extrapolatedsubrhosupi(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					extrapolatedsubpsupj(i, j) = (Paux + ((extrapolatedsubrhosupj(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					k2p(i, j) = (extrapolatedsubpsupi(i, j) + extrapolatedsubpsupj(i, j)) / 2.0d0
					else
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .eq. 0.0d0)) then
					k2p(i, j) = extrapolatedsubpsupi(i, j)
					extrapolatedsubpsupj(i, j) = k2p(i, j)
					else
					if ((d_i_p(i, j) .eq. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					k2p(i, j) = extrapolatedsubpsupj(i, j)
					extrapolatedsubpsupi(i, j) = k2p(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fx(i, j) .ne. 0 .or. d_j_fx(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fx(i, j)), int(j - d_j_fx(i, j))) .gt. 0) then
					extrapolatedsubfxsupi(i, j) = pfx
					extrapolatedsubfxsupj(i, j) = pfx
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					k2fx(i, j) = (extrapolatedsubfxsupi(i, j) + extrapolatedsubfxsupj(i, j)) / 2.0d0
					else
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .eq. 0.0d0)) then
					k2fx(i, j) = extrapolatedsubfxsupi(i, j)
					extrapolatedsubfxsupj(i, j) = k2fx(i, j)
					else
					if ((d_i_fx(i, j) .eq. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					k2fx(i, j) = extrapolatedsubfxsupj(i, j)
					extrapolatedsubfxsupi(i, j) = k2fx(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fy(i, j) .ne. 0 .or. d_j_fy(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fy(i, j)), int(j - d_j_fy(i, j))) .gt. 0) then
					extrapolatedsubfysupi(i, j) = pfy
					extrapolatedsubfysupj(i, j) = pfy
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					k2fy(i, j) = (extrapolatedsubfysupi(i, j) + extrapolatedsubfysupj(i, j)) / 2.0d0
					else
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .eq. 0.0d0)) then
					k2fy(i, j) = extrapolatedsubfysupi(i, j)
					extrapolatedsubfysupj(i, j) = k2fy(i, j)
					else
					if ((d_i_fy(i, j) .eq. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					k2fy(i, j) = extrapolatedsubfysupj(i, j)
					extrapolatedsubfysupi(i, j) = k2fy(i, j)
					end if
					end if
					end if
				end if
			end if
		end if
		if ((FOV_xUpper(i, j) .gt. 0) .and. ((i - 1 .ge. 1 .and. (FOV_1(int(i - 1), j) .gt. 0)) .or. (i - 2 .ge. 1 .and. (FOV_1(int(i - 2), j) .gt. 0)) .or. (i - 3 .ge. 1 .and. (FOV_1(int(i - 3), j) .gt. 0)) .or. ((i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_rho, k2rho, extrapolatedsubrhosupi, i, d_j_rho, k2rho, extrapolatedsubrhosupj, j, k2rho, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_mx, k2mx, extrapolatedsubmxsupi, i, d_j_mx, k2mx, extrapolatedsubmxsupj, j, k2mx, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_my, k2my, extrapolatedsubmysupi, i, d_j_my, k2my, extrapolatedsubmysupj, j, k2my, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_p(i, j) .ne. 0 .or. d_j_p(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_p(i, j)), int(j - d_j_p(i, j))) .gt. 0) then
					extrapolatedsubpsupi(i, j) = (Paux + ((extrapolatedsubrhosupi(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					extrapolatedsubpsupj(i, j) = (Paux + ((extrapolatedsubrhosupj(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					k2p(i, j) = (extrapolatedsubpsupi(i, j) + extrapolatedsubpsupj(i, j)) / 2.0d0
					else
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .eq. 0.0d0)) then
					k2p(i, j) = extrapolatedsubpsupi(i, j)
					extrapolatedsubpsupj(i, j) = k2p(i, j)
					else
					if ((d_i_p(i, j) .eq. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					k2p(i, j) = extrapolatedsubpsupj(i, j)
					extrapolatedsubpsupi(i, j) = k2p(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fx(i, j) .ne. 0 .or. d_j_fx(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fx(i, j)), int(j - d_j_fx(i, j))) .gt. 0) then
					extrapolatedsubfxsupi(i, j) = pfx
					extrapolatedsubfxsupj(i, j) = pfx
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					k2fx(i, j) = (extrapolatedsubfxsupi(i, j) + extrapolatedsubfxsupj(i, j)) / 2.0d0
					else
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .eq. 0.0d0)) then
					k2fx(i, j) = extrapolatedsubfxsupi(i, j)
					extrapolatedsubfxsupj(i, j) = k2fx(i, j)
					else
					if ((d_i_fx(i, j) .eq. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					k2fx(i, j) = extrapolatedsubfxsupj(i, j)
					extrapolatedsubfxsupi(i, j) = k2fx(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fy(i, j) .ne. 0 .or. d_j_fy(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fy(i, j)), int(j - d_j_fy(i, j))) .gt. 0) then
					extrapolatedsubfysupi(i, j) = pfy
					extrapolatedsubfysupj(i, j) = pfy
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					k2fy(i, j) = (extrapolatedsubfysupi(i, j) + extrapolatedsubfysupj(i, j)) / 2.0d0
					else
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .eq. 0.0d0)) then
					k2fy(i, j) = extrapolatedsubfysupi(i, j)
					extrapolatedsubfysupj(i, j) = k2fy(i, j)
					else
					if ((d_i_fy(i, j) .eq. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					k2fy(i, j) = extrapolatedsubfysupj(i, j)
					extrapolatedsubfysupi(i, j) = k2fy(i, j)
					end if
					end if
					end if
				end if
			end if
		end if
		if ((FOV_yLower(i, j) .gt. 0) .and. ((j + 1 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 1)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 2)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 3)) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_rho, k2rho, extrapolatedsubrhosupi, i, d_j_rho, k2rho, extrapolatedsubrhosupj, j, k2rho, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_mx, k2mx, extrapolatedsubmxsupi, i, d_j_mx, k2mx, extrapolatedsubmxsupj, j, k2mx, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_my, k2my, extrapolatedsubmysupi, i, d_j_my, k2my, extrapolatedsubmysupj, j, k2my, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_p(i, j) .ne. 0 .or. d_j_p(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_p(i, j)), int(j - d_j_p(i, j))) .gt. 0) then
					extrapolatedsubpsupi(i, j) = (Paux + ((extrapolatedsubrhosupi(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					extrapolatedsubpsupj(i, j) = (Paux + ((extrapolatedsubrhosupj(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					k2p(i, j) = (extrapolatedsubpsupi(i, j) + extrapolatedsubpsupj(i, j)) / 2.0d0
					else
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .eq. 0.0d0)) then
					k2p(i, j) = extrapolatedsubpsupi(i, j)
					extrapolatedsubpsupj(i, j) = k2p(i, j)
					else
					if ((d_i_p(i, j) .eq. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					k2p(i, j) = extrapolatedsubpsupj(i, j)
					extrapolatedsubpsupi(i, j) = k2p(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fx(i, j) .ne. 0 .or. d_j_fx(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fx(i, j)), int(j - d_j_fx(i, j))) .gt. 0) then
					extrapolatedsubfxsupi(i, j) = pfx
					extrapolatedsubfxsupj(i, j) = pfx
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					k2fx(i, j) = (extrapolatedsubfxsupi(i, j) + extrapolatedsubfxsupj(i, j)) / 2.0d0
					else
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .eq. 0.0d0)) then
					k2fx(i, j) = extrapolatedsubfxsupi(i, j)
					extrapolatedsubfxsupj(i, j) = k2fx(i, j)
					else
					if ((d_i_fx(i, j) .eq. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					k2fx(i, j) = extrapolatedsubfxsupj(i, j)
					extrapolatedsubfxsupi(i, j) = k2fx(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fy(i, j) .ne. 0 .or. d_j_fy(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fy(i, j)), int(j - d_j_fy(i, j))) .gt. 0) then
					extrapolatedsubfysupi(i, j) = pfy
					extrapolatedsubfysupj(i, j) = pfy
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					k2fy(i, j) = (extrapolatedsubfysupi(i, j) + extrapolatedsubfysupj(i, j)) / 2.0d0
					else
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .eq. 0.0d0)) then
					k2fy(i, j) = extrapolatedsubfysupi(i, j)
					extrapolatedsubfysupj(i, j) = k2fy(i, j)
					else
					if ((d_i_fy(i, j) .eq. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					k2fy(i, j) = extrapolatedsubfysupj(i, j)
					extrapolatedsubfysupi(i, j) = k2fy(i, j)
					end if
					end if
					end if
				end if
			end if
		end if
		if ((FOV_yUpper(i, j) .gt. 0) .and. ((j - 1 .ge. 1 .and. (FOV_1(i, int(j - 1)) .gt. 0)) .or. (j - 2 .ge. 1 .and. (FOV_1(i, int(j - 2)) .gt. 0)) .or. (j - 3 .ge. 1 .and. (FOV_1(i, int(j - 3)) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_rho, k2rho, extrapolatedsubrhosupi, i, d_j_rho, k2rho, extrapolatedsubrhosupj, j, k2rho, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_mx, k2mx, extrapolatedsubmxsupi, i, d_j_mx, k2mx, extrapolatedsubmxsupj, j, k2mx, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_my, k2my, extrapolatedsubmysupi, i, d_j_my, k2my, extrapolatedsubmysupj, j, k2my, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_p(i, j) .ne. 0 .or. d_j_p(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_p(i, j)), int(j - d_j_p(i, j))) .gt. 0) then
					extrapolatedsubpsupi(i, j) = (Paux + ((extrapolatedsubrhosupi(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					extrapolatedsubpsupj(i, j) = (Paux + ((extrapolatedsubrhosupj(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					k2p(i, j) = (extrapolatedsubpsupi(i, j) + extrapolatedsubpsupj(i, j)) / 2.0d0
					else
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .eq. 0.0d0)) then
					k2p(i, j) = extrapolatedsubpsupi(i, j)
					extrapolatedsubpsupj(i, j) = k2p(i, j)
					else
					if ((d_i_p(i, j) .eq. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					k2p(i, j) = extrapolatedsubpsupj(i, j)
					extrapolatedsubpsupi(i, j) = k2p(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fx(i, j) .ne. 0 .or. d_j_fx(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fx(i, j)), int(j - d_j_fx(i, j))) .gt. 0) then
					extrapolatedsubfxsupi(i, j) = pfx
					extrapolatedsubfxsupj(i, j) = pfx
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					k2fx(i, j) = (extrapolatedsubfxsupi(i, j) + extrapolatedsubfxsupj(i, j)) / 2.0d0
					else
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .eq. 0.0d0)) then
					k2fx(i, j) = extrapolatedsubfxsupi(i, j)
					extrapolatedsubfxsupj(i, j) = k2fx(i, j)
					else
					if ((d_i_fx(i, j) .eq. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					k2fx(i, j) = extrapolatedsubfxsupj(i, j)
					extrapolatedsubfxsupi(i, j) = k2fx(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fy(i, j) .ne. 0 .or. d_j_fy(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fy(i, j)), int(j - d_j_fy(i, j))) .gt. 0) then
					extrapolatedsubfysupi(i, j) = pfy
					extrapolatedsubfysupj(i, j) = pfy
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					k2fy(i, j) = (extrapolatedsubfysupi(i, j) + extrapolatedsubfysupj(i, j)) / 2.0d0
					else
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .eq. 0.0d0)) then
					k2fy(i, j) = extrapolatedsubfysupi(i, j)
					extrapolatedsubfysupj(i, j) = k2fy(i, j)
					else
					if ((d_i_fy(i, j) .eq. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					k2fy(i, j) = extrapolatedsubfysupj(i, j)
					extrapolatedsubfysupi(i, j) = k2fy(i, j)
					end if
					end if
					end if
				end if
			end if
		end if
			end do
		end do
		do j = jEnd/2 - 1, jStart, -1
			do i = iEnd/2, iEnd
!				Boundaries
		if ((FOV_xLower(i, j) .gt. 0) .and. ((i + 1 .le. cctk_lsh(1) .and. (FOV_1(int(i + 1), j) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. (FOV_1(int(i + 2), j) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. (FOV_1(int(i + 3), j) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 3)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_rho, k2rho, extrapolatedsubrhosupi, i, d_j_rho, k2rho, extrapolatedsubrhosupj, j, k2rho, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_mx, k2mx, extrapolatedsubmxsupi, i, d_j_mx, k2mx, extrapolatedsubmxsupj, j, k2mx, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_my, k2my, extrapolatedsubmysupi, i, d_j_my, k2my, extrapolatedsubmysupj, j, k2my, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_p(i, j) .ne. 0 .or. d_j_p(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_p(i, j)), int(j - d_j_p(i, j))) .gt. 0) then
					extrapolatedsubpsupi(i, j) = (Paux + ((extrapolatedsubrhosupi(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					extrapolatedsubpsupj(i, j) = (Paux + ((extrapolatedsubrhosupj(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					k2p(i, j) = (extrapolatedsubpsupi(i, j) + extrapolatedsubpsupj(i, j)) / 2.0d0
					else
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .eq. 0.0d0)) then
					k2p(i, j) = extrapolatedsubpsupi(i, j)
					extrapolatedsubpsupj(i, j) = k2p(i, j)
					else
					if ((d_i_p(i, j) .eq. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					k2p(i, j) = extrapolatedsubpsupj(i, j)
					extrapolatedsubpsupi(i, j) = k2p(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fx(i, j) .ne. 0 .or. d_j_fx(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fx(i, j)), int(j - d_j_fx(i, j))) .gt. 0) then
					extrapolatedsubfxsupi(i, j) = pfx
					extrapolatedsubfxsupj(i, j) = pfx
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					k2fx(i, j) = (extrapolatedsubfxsupi(i, j) + extrapolatedsubfxsupj(i, j)) / 2.0d0
					else
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .eq. 0.0d0)) then
					k2fx(i, j) = extrapolatedsubfxsupi(i, j)
					extrapolatedsubfxsupj(i, j) = k2fx(i, j)
					else
					if ((d_i_fx(i, j) .eq. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					k2fx(i, j) = extrapolatedsubfxsupj(i, j)
					extrapolatedsubfxsupi(i, j) = k2fx(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fy(i, j) .ne. 0 .or. d_j_fy(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fy(i, j)), int(j - d_j_fy(i, j))) .gt. 0) then
					extrapolatedsubfysupi(i, j) = pfy
					extrapolatedsubfysupj(i, j) = pfy
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					k2fy(i, j) = (extrapolatedsubfysupi(i, j) + extrapolatedsubfysupj(i, j)) / 2.0d0
					else
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .eq. 0.0d0)) then
					k2fy(i, j) = extrapolatedsubfysupi(i, j)
					extrapolatedsubfysupj(i, j) = k2fy(i, j)
					else
					if ((d_i_fy(i, j) .eq. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					k2fy(i, j) = extrapolatedsubfysupj(i, j)
					extrapolatedsubfysupi(i, j) = k2fy(i, j)
					end if
					end if
					end if
				end if
			end if
		end if
		if ((FOV_xUpper(i, j) .gt. 0) .and. ((i - 1 .ge. 1 .and. (FOV_1(int(i - 1), j) .gt. 0)) .or. (i - 2 .ge. 1 .and. (FOV_1(int(i - 2), j) .gt. 0)) .or. (i - 3 .ge. 1 .and. (FOV_1(int(i - 3), j) .gt. 0)) .or. ((i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_rho, k2rho, extrapolatedsubrhosupi, i, d_j_rho, k2rho, extrapolatedsubrhosupj, j, k2rho, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_mx, k2mx, extrapolatedsubmxsupi, i, d_j_mx, k2mx, extrapolatedsubmxsupj, j, k2mx, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_my, k2my, extrapolatedsubmysupi, i, d_j_my, k2my, extrapolatedsubmysupj, j, k2my, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_p(i, j) .ne. 0 .or. d_j_p(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_p(i, j)), int(j - d_j_p(i, j))) .gt. 0) then
					extrapolatedsubpsupi(i, j) = (Paux + ((extrapolatedsubrhosupi(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					extrapolatedsubpsupj(i, j) = (Paux + ((extrapolatedsubrhosupj(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					k2p(i, j) = (extrapolatedsubpsupi(i, j) + extrapolatedsubpsupj(i, j)) / 2.0d0
					else
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .eq. 0.0d0)) then
					k2p(i, j) = extrapolatedsubpsupi(i, j)
					extrapolatedsubpsupj(i, j) = k2p(i, j)
					else
					if ((d_i_p(i, j) .eq. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					k2p(i, j) = extrapolatedsubpsupj(i, j)
					extrapolatedsubpsupi(i, j) = k2p(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fx(i, j) .ne. 0 .or. d_j_fx(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fx(i, j)), int(j - d_j_fx(i, j))) .gt. 0) then
					extrapolatedsubfxsupi(i, j) = pfx
					extrapolatedsubfxsupj(i, j) = pfx
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					k2fx(i, j) = (extrapolatedsubfxsupi(i, j) + extrapolatedsubfxsupj(i, j)) / 2.0d0
					else
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .eq. 0.0d0)) then
					k2fx(i, j) = extrapolatedsubfxsupi(i, j)
					extrapolatedsubfxsupj(i, j) = k2fx(i, j)
					else
					if ((d_i_fx(i, j) .eq. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					k2fx(i, j) = extrapolatedsubfxsupj(i, j)
					extrapolatedsubfxsupi(i, j) = k2fx(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fy(i, j) .ne. 0 .or. d_j_fy(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fy(i, j)), int(j - d_j_fy(i, j))) .gt. 0) then
					extrapolatedsubfysupi(i, j) = pfy
					extrapolatedsubfysupj(i, j) = pfy
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					k2fy(i, j) = (extrapolatedsubfysupi(i, j) + extrapolatedsubfysupj(i, j)) / 2.0d0
					else
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .eq. 0.0d0)) then
					k2fy(i, j) = extrapolatedsubfysupi(i, j)
					extrapolatedsubfysupj(i, j) = k2fy(i, j)
					else
					if ((d_i_fy(i, j) .eq. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					k2fy(i, j) = extrapolatedsubfysupj(i, j)
					extrapolatedsubfysupi(i, j) = k2fy(i, j)
					end if
					end if
					end if
				end if
			end if
		end if
		if ((FOV_yLower(i, j) .gt. 0) .and. ((j + 1 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 1)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 2)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 3)) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_rho, k2rho, extrapolatedsubrhosupi, i, d_j_rho, k2rho, extrapolatedsubrhosupj, j, k2rho, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_mx, k2mx, extrapolatedsubmxsupi, i, d_j_mx, k2mx, extrapolatedsubmxsupj, j, k2mx, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_my, k2my, extrapolatedsubmysupi, i, d_j_my, k2my, extrapolatedsubmysupj, j, k2my, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_p(i, j) .ne. 0 .or. d_j_p(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_p(i, j)), int(j - d_j_p(i, j))) .gt. 0) then
					extrapolatedsubpsupi(i, j) = (Paux + ((extrapolatedsubrhosupi(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					extrapolatedsubpsupj(i, j) = (Paux + ((extrapolatedsubrhosupj(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					k2p(i, j) = (extrapolatedsubpsupi(i, j) + extrapolatedsubpsupj(i, j)) / 2.0d0
					else
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .eq. 0.0d0)) then
					k2p(i, j) = extrapolatedsubpsupi(i, j)
					extrapolatedsubpsupj(i, j) = k2p(i, j)
					else
					if ((d_i_p(i, j) .eq. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					k2p(i, j) = extrapolatedsubpsupj(i, j)
					extrapolatedsubpsupi(i, j) = k2p(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fx(i, j) .ne. 0 .or. d_j_fx(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fx(i, j)), int(j - d_j_fx(i, j))) .gt. 0) then
					extrapolatedsubfxsupi(i, j) = pfx
					extrapolatedsubfxsupj(i, j) = pfx
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					k2fx(i, j) = (extrapolatedsubfxsupi(i, j) + extrapolatedsubfxsupj(i, j)) / 2.0d0
					else
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .eq. 0.0d0)) then
					k2fx(i, j) = extrapolatedsubfxsupi(i, j)
					extrapolatedsubfxsupj(i, j) = k2fx(i, j)
					else
					if ((d_i_fx(i, j) .eq. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					k2fx(i, j) = extrapolatedsubfxsupj(i, j)
					extrapolatedsubfxsupi(i, j) = k2fx(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fy(i, j) .ne. 0 .or. d_j_fy(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fy(i, j)), int(j - d_j_fy(i, j))) .gt. 0) then
					extrapolatedsubfysupi(i, j) = pfy
					extrapolatedsubfysupj(i, j) = pfy
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					k2fy(i, j) = (extrapolatedsubfysupi(i, j) + extrapolatedsubfysupj(i, j)) / 2.0d0
					else
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .eq. 0.0d0)) then
					k2fy(i, j) = extrapolatedsubfysupi(i, j)
					extrapolatedsubfysupj(i, j) = k2fy(i, j)
					else
					if ((d_i_fy(i, j) .eq. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					k2fy(i, j) = extrapolatedsubfysupj(i, j)
					extrapolatedsubfysupi(i, j) = k2fy(i, j)
					end if
					end if
					end if
				end if
			end if
		end if
		if ((FOV_yUpper(i, j) .gt. 0) .and. ((j - 1 .ge. 1 .and. (FOV_1(i, int(j - 1)) .gt. 0)) .or. (j - 2 .ge. 1 .and. (FOV_1(i, int(j - 2)) .gt. 0)) .or. (j - 3 .ge. 1 .and. (FOV_1(i, int(j - 3)) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_rho, k2rho, extrapolatedsubrhosupi, i, d_j_rho, k2rho, extrapolatedsubrhosupj, j, k2rho, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_mx, k2mx, extrapolatedsubmxsupi, i, d_j_mx, k2mx, extrapolatedsubmxsupj, j, k2mx, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_my, k2my, extrapolatedsubmysupi, i, d_j_my, k2my, extrapolatedsubmysupj, j, k2my, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_p(i, j) .ne. 0 .or. d_j_p(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_p(i, j)), int(j - d_j_p(i, j))) .gt. 0) then
					extrapolatedsubpsupi(i, j) = (Paux + ((extrapolatedsubrhosupi(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					extrapolatedsubpsupj(i, j) = (Paux + ((extrapolatedsubrhosupj(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					k2p(i, j) = (extrapolatedsubpsupi(i, j) + extrapolatedsubpsupj(i, j)) / 2.0d0
					else
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .eq. 0.0d0)) then
					k2p(i, j) = extrapolatedsubpsupi(i, j)
					extrapolatedsubpsupj(i, j) = k2p(i, j)
					else
					if ((d_i_p(i, j) .eq. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					k2p(i, j) = extrapolatedsubpsupj(i, j)
					extrapolatedsubpsupi(i, j) = k2p(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fx(i, j) .ne. 0 .or. d_j_fx(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fx(i, j)), int(j - d_j_fx(i, j))) .gt. 0) then
					extrapolatedsubfxsupi(i, j) = pfx
					extrapolatedsubfxsupj(i, j) = pfx
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					k2fx(i, j) = (extrapolatedsubfxsupi(i, j) + extrapolatedsubfxsupj(i, j)) / 2.0d0
					else
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .eq. 0.0d0)) then
					k2fx(i, j) = extrapolatedsubfxsupi(i, j)
					extrapolatedsubfxsupj(i, j) = k2fx(i, j)
					else
					if ((d_i_fx(i, j) .eq. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					k2fx(i, j) = extrapolatedsubfxsupj(i, j)
					extrapolatedsubfxsupi(i, j) = k2fx(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fy(i, j) .ne. 0 .or. d_j_fy(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fy(i, j)), int(j - d_j_fy(i, j))) .gt. 0) then
					extrapolatedsubfysupi(i, j) = pfy
					extrapolatedsubfysupj(i, j) = pfy
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					k2fy(i, j) = (extrapolatedsubfysupi(i, j) + extrapolatedsubfysupj(i, j)) / 2.0d0
					else
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .eq. 0.0d0)) then
					k2fy(i, j) = extrapolatedsubfysupi(i, j)
					extrapolatedsubfysupj(i, j) = k2fy(i, j)
					else
					if ((d_i_fy(i, j) .eq. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					k2fy(i, j) = extrapolatedsubfysupj(i, j)
					extrapolatedsubfysupi(i, j) = k2fy(i, j)
					end if
					end if
					end if
				end if
			end if
		end if
			end do
		end do
		do j = jEnd/2, jEnd
			do i = iEnd/2 - 1, iStart, -1
!				Boundaries
		if ((FOV_xLower(i, j) .gt. 0) .and. ((i + 1 .le. cctk_lsh(1) .and. (FOV_1(int(i + 1), j) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. (FOV_1(int(i + 2), j) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. (FOV_1(int(i + 3), j) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 3)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_rho, k2rho, extrapolatedsubrhosupi, i, d_j_rho, k2rho, extrapolatedsubrhosupj, j, k2rho, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_mx, k2mx, extrapolatedsubmxsupi, i, d_j_mx, k2mx, extrapolatedsubmxsupj, j, k2mx, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_my, k2my, extrapolatedsubmysupi, i, d_j_my, k2my, extrapolatedsubmysupj, j, k2my, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_p(i, j) .ne. 0 .or. d_j_p(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_p(i, j)), int(j - d_j_p(i, j))) .gt. 0) then
					extrapolatedsubpsupi(i, j) = (Paux + ((extrapolatedsubrhosupi(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					extrapolatedsubpsupj(i, j) = (Paux + ((extrapolatedsubrhosupj(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					k2p(i, j) = (extrapolatedsubpsupi(i, j) + extrapolatedsubpsupj(i, j)) / 2.0d0
					else
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .eq. 0.0d0)) then
					k2p(i, j) = extrapolatedsubpsupi(i, j)
					extrapolatedsubpsupj(i, j) = k2p(i, j)
					else
					if ((d_i_p(i, j) .eq. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					k2p(i, j) = extrapolatedsubpsupj(i, j)
					extrapolatedsubpsupi(i, j) = k2p(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fx(i, j) .ne. 0 .or. d_j_fx(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fx(i, j)), int(j - d_j_fx(i, j))) .gt. 0) then
					extrapolatedsubfxsupi(i, j) = pfx
					extrapolatedsubfxsupj(i, j) = pfx
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					k2fx(i, j) = (extrapolatedsubfxsupi(i, j) + extrapolatedsubfxsupj(i, j)) / 2.0d0
					else
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .eq. 0.0d0)) then
					k2fx(i, j) = extrapolatedsubfxsupi(i, j)
					extrapolatedsubfxsupj(i, j) = k2fx(i, j)
					else
					if ((d_i_fx(i, j) .eq. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					k2fx(i, j) = extrapolatedsubfxsupj(i, j)
					extrapolatedsubfxsupi(i, j) = k2fx(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fy(i, j) .ne. 0 .or. d_j_fy(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fy(i, j)), int(j - d_j_fy(i, j))) .gt. 0) then
					extrapolatedsubfysupi(i, j) = pfy
					extrapolatedsubfysupj(i, j) = pfy
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					k2fy(i, j) = (extrapolatedsubfysupi(i, j) + extrapolatedsubfysupj(i, j)) / 2.0d0
					else
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .eq. 0.0d0)) then
					k2fy(i, j) = extrapolatedsubfysupi(i, j)
					extrapolatedsubfysupj(i, j) = k2fy(i, j)
					else
					if ((d_i_fy(i, j) .eq. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					k2fy(i, j) = extrapolatedsubfysupj(i, j)
					extrapolatedsubfysupi(i, j) = k2fy(i, j)
					end if
					end if
					end if
				end if
			end if
		end if
		if ((FOV_xUpper(i, j) .gt. 0) .and. ((i - 1 .ge. 1 .and. (FOV_1(int(i - 1), j) .gt. 0)) .or. (i - 2 .ge. 1 .and. (FOV_1(int(i - 2), j) .gt. 0)) .or. (i - 3 .ge. 1 .and. (FOV_1(int(i - 3), j) .gt. 0)) .or. ((i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_rho, k2rho, extrapolatedsubrhosupi, i, d_j_rho, k2rho, extrapolatedsubrhosupj, j, k2rho, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_mx, k2mx, extrapolatedsubmxsupi, i, d_j_mx, k2mx, extrapolatedsubmxsupj, j, k2mx, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_my, k2my, extrapolatedsubmysupi, i, d_j_my, k2my, extrapolatedsubmysupj, j, k2my, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_p(i, j) .ne. 0 .or. d_j_p(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_p(i, j)), int(j - d_j_p(i, j))) .gt. 0) then
					extrapolatedsubpsupi(i, j) = (Paux + ((extrapolatedsubrhosupi(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					extrapolatedsubpsupj(i, j) = (Paux + ((extrapolatedsubrhosupj(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					k2p(i, j) = (extrapolatedsubpsupi(i, j) + extrapolatedsubpsupj(i, j)) / 2.0d0
					else
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .eq. 0.0d0)) then
					k2p(i, j) = extrapolatedsubpsupi(i, j)
					extrapolatedsubpsupj(i, j) = k2p(i, j)
					else
					if ((d_i_p(i, j) .eq. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					k2p(i, j) = extrapolatedsubpsupj(i, j)
					extrapolatedsubpsupi(i, j) = k2p(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fx(i, j) .ne. 0 .or. d_j_fx(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fx(i, j)), int(j - d_j_fx(i, j))) .gt. 0) then
					extrapolatedsubfxsupi(i, j) = pfx
					extrapolatedsubfxsupj(i, j) = pfx
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					k2fx(i, j) = (extrapolatedsubfxsupi(i, j) + extrapolatedsubfxsupj(i, j)) / 2.0d0
					else
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .eq. 0.0d0)) then
					k2fx(i, j) = extrapolatedsubfxsupi(i, j)
					extrapolatedsubfxsupj(i, j) = k2fx(i, j)
					else
					if ((d_i_fx(i, j) .eq. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					k2fx(i, j) = extrapolatedsubfxsupj(i, j)
					extrapolatedsubfxsupi(i, j) = k2fx(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fy(i, j) .ne. 0 .or. d_j_fy(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fy(i, j)), int(j - d_j_fy(i, j))) .gt. 0) then
					extrapolatedsubfysupi(i, j) = pfy
					extrapolatedsubfysupj(i, j) = pfy
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					k2fy(i, j) = (extrapolatedsubfysupi(i, j) + extrapolatedsubfysupj(i, j)) / 2.0d0
					else
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .eq. 0.0d0)) then
					k2fy(i, j) = extrapolatedsubfysupi(i, j)
					extrapolatedsubfysupj(i, j) = k2fy(i, j)
					else
					if ((d_i_fy(i, j) .eq. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					k2fy(i, j) = extrapolatedsubfysupj(i, j)
					extrapolatedsubfysupi(i, j) = k2fy(i, j)
					end if
					end if
					end if
				end if
			end if
		end if
		if ((FOV_yLower(i, j) .gt. 0) .and. ((j + 1 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 1)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 2)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 3)) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_rho, k2rho, extrapolatedsubrhosupi, i, d_j_rho, k2rho, extrapolatedsubrhosupj, j, k2rho, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_mx, k2mx, extrapolatedsubmxsupi, i, d_j_mx, k2mx, extrapolatedsubmxsupj, j, k2mx, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_my, k2my, extrapolatedsubmysupi, i, d_j_my, k2my, extrapolatedsubmysupj, j, k2my, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_p(i, j) .ne. 0 .or. d_j_p(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_p(i, j)), int(j - d_j_p(i, j))) .gt. 0) then
					extrapolatedsubpsupi(i, j) = (Paux + ((extrapolatedsubrhosupi(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					extrapolatedsubpsupj(i, j) = (Paux + ((extrapolatedsubrhosupj(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					k2p(i, j) = (extrapolatedsubpsupi(i, j) + extrapolatedsubpsupj(i, j)) / 2.0d0
					else
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .eq. 0.0d0)) then
					k2p(i, j) = extrapolatedsubpsupi(i, j)
					extrapolatedsubpsupj(i, j) = k2p(i, j)
					else
					if ((d_i_p(i, j) .eq. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					k2p(i, j) = extrapolatedsubpsupj(i, j)
					extrapolatedsubpsupi(i, j) = k2p(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fx(i, j) .ne. 0 .or. d_j_fx(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fx(i, j)), int(j - d_j_fx(i, j))) .gt. 0) then
					extrapolatedsubfxsupi(i, j) = pfx
					extrapolatedsubfxsupj(i, j) = pfx
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					k2fx(i, j) = (extrapolatedsubfxsupi(i, j) + extrapolatedsubfxsupj(i, j)) / 2.0d0
					else
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .eq. 0.0d0)) then
					k2fx(i, j) = extrapolatedsubfxsupi(i, j)
					extrapolatedsubfxsupj(i, j) = k2fx(i, j)
					else
					if ((d_i_fx(i, j) .eq. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					k2fx(i, j) = extrapolatedsubfxsupj(i, j)
					extrapolatedsubfxsupi(i, j) = k2fx(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fy(i, j) .ne. 0 .or. d_j_fy(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fy(i, j)), int(j - d_j_fy(i, j))) .gt. 0) then
					extrapolatedsubfysupi(i, j) = pfy
					extrapolatedsubfysupj(i, j) = pfy
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					k2fy(i, j) = (extrapolatedsubfysupi(i, j) + extrapolatedsubfysupj(i, j)) / 2.0d0
					else
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .eq. 0.0d0)) then
					k2fy(i, j) = extrapolatedsubfysupi(i, j)
					extrapolatedsubfysupj(i, j) = k2fy(i, j)
					else
					if ((d_i_fy(i, j) .eq. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					k2fy(i, j) = extrapolatedsubfysupj(i, j)
					extrapolatedsubfysupi(i, j) = k2fy(i, j)
					end if
					end if
					end if
				end if
			end if
		end if
		if ((FOV_yUpper(i, j) .gt. 0) .and. ((j - 1 .ge. 1 .and. (FOV_1(i, int(j - 1)) .gt. 0)) .or. (j - 2 .ge. 1 .and. (FOV_1(i, int(j - 2)) .gt. 0)) .or. (j - 3 .ge. 1 .and. (FOV_1(i, int(j - 3)) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_rho, k2rho, extrapolatedsubrhosupi, i, d_j_rho, k2rho, extrapolatedsubrhosupj, j, k2rho, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_mx, k2mx, extrapolatedsubmxsupi, i, d_j_mx, k2mx, extrapolatedsubmxsupj, j, k2mx, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_my, k2my, extrapolatedsubmysupi, i, d_j_my, k2my, extrapolatedsubmysupj, j, k2my, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_p(i, j) .ne. 0 .or. d_j_p(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_p(i, j)), int(j - d_j_p(i, j))) .gt. 0) then
					extrapolatedsubpsupi(i, j) = (Paux + ((extrapolatedsubrhosupi(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					extrapolatedsubpsupj(i, j) = (Paux + ((extrapolatedsubrhosupj(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					k2p(i, j) = (extrapolatedsubpsupi(i, j) + extrapolatedsubpsupj(i, j)) / 2.0d0
					else
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .eq. 0.0d0)) then
					k2p(i, j) = extrapolatedsubpsupi(i, j)
					extrapolatedsubpsupj(i, j) = k2p(i, j)
					else
					if ((d_i_p(i, j) .eq. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					k2p(i, j) = extrapolatedsubpsupj(i, j)
					extrapolatedsubpsupi(i, j) = k2p(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fx(i, j) .ne. 0 .or. d_j_fx(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fx(i, j)), int(j - d_j_fx(i, j))) .gt. 0) then
					extrapolatedsubfxsupi(i, j) = pfx
					extrapolatedsubfxsupj(i, j) = pfx
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					k2fx(i, j) = (extrapolatedsubfxsupi(i, j) + extrapolatedsubfxsupj(i, j)) / 2.0d0
					else
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .eq. 0.0d0)) then
					k2fx(i, j) = extrapolatedsubfxsupi(i, j)
					extrapolatedsubfxsupj(i, j) = k2fx(i, j)
					else
					if ((d_i_fx(i, j) .eq. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					k2fx(i, j) = extrapolatedsubfxsupj(i, j)
					extrapolatedsubfxsupi(i, j) = k2fx(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fy(i, j) .ne. 0 .or. d_j_fy(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fy(i, j)), int(j - d_j_fy(i, j))) .gt. 0) then
					extrapolatedsubfysupi(i, j) = pfy
					extrapolatedsubfysupj(i, j) = pfy
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					k2fy(i, j) = (extrapolatedsubfysupi(i, j) + extrapolatedsubfysupj(i, j)) / 2.0d0
					else
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .eq. 0.0d0)) then
					k2fy(i, j) = extrapolatedsubfysupi(i, j)
					extrapolatedsubfysupj(i, j) = k2fy(i, j)
					else
					if ((d_i_fy(i, j) .eq. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					k2fy(i, j) = extrapolatedsubfysupj(i, j)
					extrapolatedsubfysupi(i, j) = k2fy(i, j)
					end if
					end if
					end if
				end if
			end if
		end if
			end do
		end do
		do j = jEnd/2 - 1, jStart, -1
			do i = iEnd/2 - 1, iStart, -1
!				Boundaries
		if ((FOV_xLower(i, j) .gt. 0) .and. ((i + 1 .le. cctk_lsh(1) .and. (FOV_1(int(i + 1), j) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. (FOV_1(int(i + 2), j) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. (FOV_1(int(i + 3), j) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 3)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_rho, k2rho, extrapolatedsubrhosupi, i, d_j_rho, k2rho, extrapolatedsubrhosupj, j, k2rho, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_mx, k2mx, extrapolatedsubmxsupi, i, d_j_mx, k2mx, extrapolatedsubmxsupj, j, k2mx, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_my, k2my, extrapolatedsubmysupi, i, d_j_my, k2my, extrapolatedsubmysupj, j, k2my, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_p(i, j) .ne. 0 .or. d_j_p(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_p(i, j)), int(j - d_j_p(i, j))) .gt. 0) then
					extrapolatedsubpsupi(i, j) = (Paux + ((extrapolatedsubrhosupi(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					extrapolatedsubpsupj(i, j) = (Paux + ((extrapolatedsubrhosupj(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					k2p(i, j) = (extrapolatedsubpsupi(i, j) + extrapolatedsubpsupj(i, j)) / 2.0d0
					else
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .eq. 0.0d0)) then
					k2p(i, j) = extrapolatedsubpsupi(i, j)
					extrapolatedsubpsupj(i, j) = k2p(i, j)
					else
					if ((d_i_p(i, j) .eq. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					k2p(i, j) = extrapolatedsubpsupj(i, j)
					extrapolatedsubpsupi(i, j) = k2p(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fx(i, j) .ne. 0 .or. d_j_fx(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fx(i, j)), int(j - d_j_fx(i, j))) .gt. 0) then
					extrapolatedsubfxsupi(i, j) = pfx
					extrapolatedsubfxsupj(i, j) = pfx
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					k2fx(i, j) = (extrapolatedsubfxsupi(i, j) + extrapolatedsubfxsupj(i, j)) / 2.0d0
					else
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .eq. 0.0d0)) then
					k2fx(i, j) = extrapolatedsubfxsupi(i, j)
					extrapolatedsubfxsupj(i, j) = k2fx(i, j)
					else
					if ((d_i_fx(i, j) .eq. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					k2fx(i, j) = extrapolatedsubfxsupj(i, j)
					extrapolatedsubfxsupi(i, j) = k2fx(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fy(i, j) .ne. 0 .or. d_j_fy(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fy(i, j)), int(j - d_j_fy(i, j))) .gt. 0) then
					extrapolatedsubfysupi(i, j) = pfy
					extrapolatedsubfysupj(i, j) = pfy
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					k2fy(i, j) = (extrapolatedsubfysupi(i, j) + extrapolatedsubfysupj(i, j)) / 2.0d0
					else
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .eq. 0.0d0)) then
					k2fy(i, j) = extrapolatedsubfysupi(i, j)
					extrapolatedsubfysupj(i, j) = k2fy(i, j)
					else
					if ((d_i_fy(i, j) .eq. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					k2fy(i, j) = extrapolatedsubfysupj(i, j)
					extrapolatedsubfysupi(i, j) = k2fy(i, j)
					end if
					end if
					end if
				end if
			end if
		end if
		if ((FOV_xUpper(i, j) .gt. 0) .and. ((i - 1 .ge. 1 .and. (FOV_1(int(i - 1), j) .gt. 0)) .or. (i - 2 .ge. 1 .and. (FOV_1(int(i - 2), j) .gt. 0)) .or. (i - 3 .ge. 1 .and. (FOV_1(int(i - 3), j) .gt. 0)) .or. ((i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_rho, k2rho, extrapolatedsubrhosupi, i, d_j_rho, k2rho, extrapolatedsubrhosupj, j, k2rho, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_mx, k2mx, extrapolatedsubmxsupi, i, d_j_mx, k2mx, extrapolatedsubmxsupj, j, k2mx, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_my, k2my, extrapolatedsubmysupi, i, d_j_my, k2my, extrapolatedsubmysupj, j, k2my, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_p(i, j) .ne. 0 .or. d_j_p(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_p(i, j)), int(j - d_j_p(i, j))) .gt. 0) then
					extrapolatedsubpsupi(i, j) = (Paux + ((extrapolatedsubrhosupi(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					extrapolatedsubpsupj(i, j) = (Paux + ((extrapolatedsubrhosupj(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					k2p(i, j) = (extrapolatedsubpsupi(i, j) + extrapolatedsubpsupj(i, j)) / 2.0d0
					else
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .eq. 0.0d0)) then
					k2p(i, j) = extrapolatedsubpsupi(i, j)
					extrapolatedsubpsupj(i, j) = k2p(i, j)
					else
					if ((d_i_p(i, j) .eq. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					k2p(i, j) = extrapolatedsubpsupj(i, j)
					extrapolatedsubpsupi(i, j) = k2p(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fx(i, j) .ne. 0 .or. d_j_fx(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fx(i, j)), int(j - d_j_fx(i, j))) .gt. 0) then
					extrapolatedsubfxsupi(i, j) = pfx
					extrapolatedsubfxsupj(i, j) = pfx
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					k2fx(i, j) = (extrapolatedsubfxsupi(i, j) + extrapolatedsubfxsupj(i, j)) / 2.0d0
					else
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .eq. 0.0d0)) then
					k2fx(i, j) = extrapolatedsubfxsupi(i, j)
					extrapolatedsubfxsupj(i, j) = k2fx(i, j)
					else
					if ((d_i_fx(i, j) .eq. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					k2fx(i, j) = extrapolatedsubfxsupj(i, j)
					extrapolatedsubfxsupi(i, j) = k2fx(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fy(i, j) .ne. 0 .or. d_j_fy(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fy(i, j)), int(j - d_j_fy(i, j))) .gt. 0) then
					extrapolatedsubfysupi(i, j) = pfy
					extrapolatedsubfysupj(i, j) = pfy
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					k2fy(i, j) = (extrapolatedsubfysupi(i, j) + extrapolatedsubfysupj(i, j)) / 2.0d0
					else
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .eq. 0.0d0)) then
					k2fy(i, j) = extrapolatedsubfysupi(i, j)
					extrapolatedsubfysupj(i, j) = k2fy(i, j)
					else
					if ((d_i_fy(i, j) .eq. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					k2fy(i, j) = extrapolatedsubfysupj(i, j)
					extrapolatedsubfysupi(i, j) = k2fy(i, j)
					end if
					end if
					end if
				end if
			end if
		end if
		if ((FOV_yLower(i, j) .gt. 0) .and. ((j + 1 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 1)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 2)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 3)) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_rho, k2rho, extrapolatedsubrhosupi, i, d_j_rho, k2rho, extrapolatedsubrhosupj, j, k2rho, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_mx, k2mx, extrapolatedsubmxsupi, i, d_j_mx, k2mx, extrapolatedsubmxsupj, j, k2mx, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_my, k2my, extrapolatedsubmysupi, i, d_j_my, k2my, extrapolatedsubmysupj, j, k2my, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_p(i, j) .ne. 0 .or. d_j_p(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_p(i, j)), int(j - d_j_p(i, j))) .gt. 0) then
					extrapolatedsubpsupi(i, j) = (Paux + ((extrapolatedsubrhosupi(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					extrapolatedsubpsupj(i, j) = (Paux + ((extrapolatedsubrhosupj(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					k2p(i, j) = (extrapolatedsubpsupi(i, j) + extrapolatedsubpsupj(i, j)) / 2.0d0
					else
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .eq. 0.0d0)) then
					k2p(i, j) = extrapolatedsubpsupi(i, j)
					extrapolatedsubpsupj(i, j) = k2p(i, j)
					else
					if ((d_i_p(i, j) .eq. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					k2p(i, j) = extrapolatedsubpsupj(i, j)
					extrapolatedsubpsupi(i, j) = k2p(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fx(i, j) .ne. 0 .or. d_j_fx(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fx(i, j)), int(j - d_j_fx(i, j))) .gt. 0) then
					extrapolatedsubfxsupi(i, j) = pfx
					extrapolatedsubfxsupj(i, j) = pfx
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					k2fx(i, j) = (extrapolatedsubfxsupi(i, j) + extrapolatedsubfxsupj(i, j)) / 2.0d0
					else
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .eq. 0.0d0)) then
					k2fx(i, j) = extrapolatedsubfxsupi(i, j)
					extrapolatedsubfxsupj(i, j) = k2fx(i, j)
					else
					if ((d_i_fx(i, j) .eq. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					k2fx(i, j) = extrapolatedsubfxsupj(i, j)
					extrapolatedsubfxsupi(i, j) = k2fx(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fy(i, j) .ne. 0 .or. d_j_fy(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fy(i, j)), int(j - d_j_fy(i, j))) .gt. 0) then
					extrapolatedsubfysupi(i, j) = pfy
					extrapolatedsubfysupj(i, j) = pfy
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					k2fy(i, j) = (extrapolatedsubfysupi(i, j) + extrapolatedsubfysupj(i, j)) / 2.0d0
					else
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .eq. 0.0d0)) then
					k2fy(i, j) = extrapolatedsubfysupi(i, j)
					extrapolatedsubfysupj(i, j) = k2fy(i, j)
					else
					if ((d_i_fy(i, j) .eq. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					k2fy(i, j) = extrapolatedsubfysupj(i, j)
					extrapolatedsubfysupi(i, j) = k2fy(i, j)
					end if
					end if
					end if
				end if
			end if
		end if
		if ((FOV_yUpper(i, j) .gt. 0) .and. ((j - 1 .ge. 1 .and. (FOV_1(i, int(j - 1)) .gt. 0)) .or. (j - 2 .ge. 1 .and. (FOV_1(i, int(j - 2)) .gt. 0)) .or. (j - 3 .ge. 1 .and. (FOV_1(i, int(j - 3)) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_rho, k2rho, extrapolatedsubrhosupi, i, d_j_rho, k2rho, extrapolatedsubrhosupj, j, k2rho, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_mx, k2mx, extrapolatedsubmxsupi, i, d_j_mx, k2mx, extrapolatedsubmxsupj, j, k2mx, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_my, k2my, extrapolatedsubmysupi, i, d_j_my, k2my, extrapolatedsubmysupj, j, k2my, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_p(i, j) .ne. 0 .or. d_j_p(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_p(i, j)), int(j - d_j_p(i, j))) .gt. 0) then
					extrapolatedsubpsupi(i, j) = (Paux + ((extrapolatedsubrhosupi(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					extrapolatedsubpsupj(i, j) = (Paux + ((extrapolatedsubrhosupj(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					k2p(i, j) = (extrapolatedsubpsupi(i, j) + extrapolatedsubpsupj(i, j)) / 2.0d0
					else
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .eq. 0.0d0)) then
					k2p(i, j) = extrapolatedsubpsupi(i, j)
					extrapolatedsubpsupj(i, j) = k2p(i, j)
					else
					if ((d_i_p(i, j) .eq. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					k2p(i, j) = extrapolatedsubpsupj(i, j)
					extrapolatedsubpsupi(i, j) = k2p(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fx(i, j) .ne. 0 .or. d_j_fx(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fx(i, j)), int(j - d_j_fx(i, j))) .gt. 0) then
					extrapolatedsubfxsupi(i, j) = pfx
					extrapolatedsubfxsupj(i, j) = pfx
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					k2fx(i, j) = (extrapolatedsubfxsupi(i, j) + extrapolatedsubfxsupj(i, j)) / 2.0d0
					else
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .eq. 0.0d0)) then
					k2fx(i, j) = extrapolatedsubfxsupi(i, j)
					extrapolatedsubfxsupj(i, j) = k2fx(i, j)
					else
					if ((d_i_fx(i, j) .eq. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					k2fx(i, j) = extrapolatedsubfxsupj(i, j)
					extrapolatedsubfxsupi(i, j) = k2fx(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fy(i, j) .ne. 0 .or. d_j_fy(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fy(i, j)), int(j - d_j_fy(i, j))) .gt. 0) then
					extrapolatedsubfysupi(i, j) = pfy
					extrapolatedsubfysupj(i, j) = pfy
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					k2fy(i, j) = (extrapolatedsubfysupi(i, j) + extrapolatedsubfysupj(i, j)) / 2.0d0
					else
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .eq. 0.0d0)) then
					k2fy(i, j) = extrapolatedsubfysupi(i, j)
					extrapolatedsubfysupj(i, j) = k2fy(i, j)
					else
					if ((d_i_fy(i, j) .eq. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					k2fy(i, j) = extrapolatedsubfysupj(i, j)
					extrapolatedsubfysupi(i, j) = k2fy(i, j)
					end if
					end if
					end if
				end if
			end if
		end if
			end do
		end do
		call CCTK_SyncGroup(status, cctkGH, "CompressibleCounterflow2D::block8")
		call CCTK_SyncGroup(status, cctkGH, "CompressibleCounterflow2D::block4")
		do j = jStart, jEnd
			do i = iStart, iEnd
				if ((FOV_1(i, j) .gt. 0 .or. (((i + 1 .le. cctk_lsh(1) .and. FOV_1(i + 1, j) .gt. 0) .or. (i + 2 .le. cctk_lsh(1) .and. FOV_1(i + 2, j) .gt. 0) .or. (j + 1 .le. cctk_lsh(2) .and. FOV_1(i, j + 1) .gt. 0) .or. (j + 2 .le. cctk_lsh(2) .and. FOV_1(i, j + 2) .gt. 0)) .or. ((i - 1 .ge. 1 .and. FOV_1(i - 1, j) .gt. 0) .or. (i - 2 .ge. 1 .and. FOV_1(i - 2, j) .gt. 0) .or. (j - 1 .ge. 1 .and. FOV_1(i, j - 1) .gt. 0) .or. (j - 2 .ge. 1 .and. FOV_1(i, j - 2) .gt. 0)) .or. (((i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1) .and. FOV_1(int(i + 1), int(j - 1)) .gt. 0) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2)) .and. FOV_1(int(i + 1), int(j + 1)) .gt. 0) .or. ((i - 1 .ge. 1 .and. j - 1 .ge. 1) .and. FOV_1(int(i - 1), int(j - 1)) .gt. 0) .or. ((i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2)) .and. FOV_1(int(i - 1), int(j + 1)) .gt. 0) .or. ((i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1) .and. FOV_1(int(i + 1), int(j - 2)) .gt. 0) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2)) .and. FOV_1(int(i + 1), int(j + 2)) .gt. 0) .or. ((i - 1 .ge. 1 .and. j - 2 .ge. 1) .and. FOV_1(int(i - 1), int(j - 2)) .gt. 0) .or. ((i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2)) .and. FOV_1(int(i - 1), int(j + 2)) .gt. 0) .or. ((i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1) .and. FOV_1(int(i + 2), int(j - 1)) .gt. 0) .or. ((i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2)) .and. FOV_1(int(i + 2), int(j + 1)) .gt. 0) .or. ((i - 2 .ge. 1 .and. j - 1 .ge. 1) .and. FOV_1(int(i - 2), int(j - 1)) .gt. 0) .or. ((i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2)) .and. FOV_1(int(i - 2), int(j + 1)) .gt. 0) .or. ((i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1) .and. FOV_1(int(i + 2), int(j - 2)) .gt. 0) .or. ((i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2)) .and. FOV_1(int(i + 2), int(j + 2)) .gt. 0) .or. ((i - 2 .ge. 1 .and. j - 2 .ge. 1) .and. FOV_1(int(i - 2), int(j - 2)) .gt. 0) .or. ((i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2)) .and. FOV_1(int(i - 2), int(j + 2)) .gt. 0))))) then
					if (((i .ge. 3) .and. (i .le. (cctk_lsh(1) - 3))) .and. ((j .ge. 3) .and. (j .le. (cctk_lsh(2) - 3)))) then
					Flux_extsubrhosupi_1(i, j) = Firho_FluidI(CCTK_PASS_FTOF, extrapolatedsubmxsupi(i, j))
					Flux_extsubrhosupj_1(i, j) = Fjrho_FluidI(CCTK_PASS_FTOF, extrapolatedsubmysupj(i, j))
					Flux_extsubmxsupi_1(i, j) = Fimx_FluidI(CCTK_PASS_FTOF, extrapolatedsubrhosupi(i, j), extrapolatedsubmxsupi(i, j), extrapolatedsubpsupi(i, j))
					Flux_extsubmxsupj_1(i, j) = Fjmx_FluidI(CCTK_PASS_FTOF, extrapolatedsubrhosupj(i, j), extrapolatedsubmxsupj(i, j), extrapolatedsubmysupj(i, j))
					Flux_extsubmysupi_1(i, j) = Fimy_FluidI(CCTK_PASS_FTOF, extrapolatedsubrhosupi(i, j), extrapolatedsubmxsupi(i, j), extrapolatedsubmysupi(i, j))
					Flux_extsubmysupj_1(i, j) = Fjmy_FluidI(CCTK_PASS_FTOF, extrapolatedsubrhosupj(i, j), extrapolatedsubmysupj(i, j), extrapolatedsubpsupj(i, j))
					Q_ii_mx_1 = lambda
					Q_ij_mx_1 = lambda
					Q_ii2_mx_1 = mu
					Q_jj_mx_1 = mu
					Q_ii3_mx_1 = mu
					Q_ji_mx_1 = mu
					Q_ji_my_1 = lambda
					Q_jj_my_1 = lambda
					Q_ii_my_1 = mu
					Q_jj2_my_1 = mu
					Q_ij_my_1 = mu
					Q_jj3_my_1 = mu
					Pm2_ii_mx_1 = k2mx(i - 2, j) / k2rho(i - 2, j)
					Pm2_ij_mx_1 = k2my(i, j - 2) / k2rho(i, j - 2)
					Pm2_ii2_mx_1 = k2mx(i - 2, j) / k2rho(i - 2, j)
					Pm2_jj_mx_1 = k2mx(i, j - 2) / k2rho(i, j - 2)
					Pm2_ii3_mx_1 = k2mx(i - 2, j) / k2rho(i - 2, j)
					Pm2_ji_mx_1 = k2my(i - 2, j) / k2rho(i - 2, j)
					Pm2_ji_my_1 = k2mx(i - 2, j) / k2rho(i - 2, j)
					Pm2_jj_my_1 = k2my(i, j - 2) / k2rho(i, j - 2)
					Pm2_ii_my_1 = k2my(i - 2, j) / k2rho(i - 2, j)
					Pm2_jj2_my_1 = k2my(i, j - 2) / k2rho(i, j - 2)
					Pm2_ij_my_1 = k2mx(i, j - 2) / k2rho(i, j - 2)
					Pm2_jj3_my_1 = k2my(i, j - 2) / k2rho(i, j - 2)
					Pm1_ii_mx_1 = k2mx(i - 1, j) / k2rho(i - 1, j)
					Pm1_ij_mx_1 = k2my(i, j - 1) / k2rho(i, j - 1)
					Pm1_ii2_mx_1 = k2mx(i - 1, j) / k2rho(i - 1, j)
					Pm1_jj_mx_1 = k2mx(i, j - 1) / k2rho(i, j - 1)
					Pm1_ii3_mx_1 = k2mx(i - 1, j) / k2rho(i - 1, j)
					Pm1_ji_mx_1 = k2my(i - 1, j) / k2rho(i - 1, j)
					Pm1_ji_my_1 = k2mx(i - 1, j) / k2rho(i - 1, j)
					Pm1_jj_my_1 = k2my(i, j - 1) / k2rho(i, j - 1)
					Pm1_ii_my_1 = k2my(i - 1, j) / k2rho(i - 1, j)
					Pm1_jj2_my_1 = k2my(i, j - 1) / k2rho(i, j - 1)
					Pm1_ij_my_1 = k2mx(i, j - 1) / k2rho(i, j - 1)
					Pm1_jj3_my_1 = k2my(i, j - 1) / k2rho(i, j - 1)
					Pp1_ii_mx_1 = k2mx(i + 1, j) / k2rho(i + 1, j)
					Pp1_ij_mx_1 = k2my(i, j + 1) / k2rho(i, j + 1)
					Pp1_ii2_mx_1 = k2mx(i + 1, j) / k2rho(i + 1, j)
					Pp1_jj_mx_1 = k2mx(i, j + 1) / k2rho(i, j + 1)
					Pp1_ii3_mx_1 = k2mx(i + 1, j) / k2rho(i + 1, j)
					Pp1_ji_mx_1 = k2my(i + 1, j) / k2rho(i + 1, j)
					Pp1_ji_my_1 = k2mx(i + 1, j) / k2rho(i + 1, j)
					Pp1_jj_my_1 = k2my(i, j + 1) / k2rho(i, j + 1)
					Pp1_ii_my_1 = k2my(i + 1, j) / k2rho(i + 1, j)
					Pp1_jj2_my_1 = k2my(i, j + 1) / k2rho(i, j + 1)
					Pp1_ij_my_1 = k2mx(i, j + 1) / k2rho(i, j + 1)
					Pp1_jj3_my_1 = k2my(i, j + 1) / k2rho(i, j + 1)
					Pp2_ii_mx_1 = k2mx(i + 2, j) / k2rho(i + 2, j)
					Pp2_ij_mx_1 = k2my(i, j + 2) / k2rho(i, j + 2)
					Pp2_ii2_mx_1 = k2mx(i + 2, j) / k2rho(i + 2, j)
					Pp2_jj_mx_1 = k2mx(i, j + 2) / k2rho(i, j + 2)
					Pp2_ii3_mx_1 = k2mx(i + 2, j) / k2rho(i + 2, j)
					Pp2_ji_mx_1 = k2my(i + 2, j) / k2rho(i + 2, j)
					Pp2_ji_my_1 = k2mx(i + 2, j) / k2rho(i + 2, j)
					Pp2_jj_my_1 = k2my(i, j + 2) / k2rho(i, j + 2)
					Pp2_ii_my_1 = k2my(i + 2, j) / k2rho(i + 2, j)
					Pp2_jj2_my_1 = k2my(i, j + 2) / k2rho(i, j + 2)
					Pp2_ij_my_1 = k2mx(i, j + 2) / k2rho(i, j + 2)
					Pp2_jj3_my_1 = k2my(i, j + 2) / k2rho(i, j + 2)
					Flux_extsubmxsupi_1(i, j) = Flux_extsubmxsupi_1(i, j) - Q_ii_mx_1 * CDi(CCTK_PASS_FTOF, Pm2_ii_mx_1, Pm1_ii_mx_1, Pp1_ii_mx_1, Pp2_ii_mx_1)
					Flux_extsubmxsupi_1(i, j) = Flux_extsubmxsupi_1(i, j) - Q_ij_mx_1 * CDj(CCTK_PASS_FTOF, Pm2_ij_mx_1, Pm1_ij_mx_1, Pp1_ij_mx_1, Pp2_ij_mx_1)
					Flux_extsubmxsupi_1(i, j) = Flux_extsubmxsupi_1(i, j) - Q_ii2_mx_1 * CDi(CCTK_PASS_FTOF, Pm2_ii2_mx_1, Pm1_ii2_mx_1, Pp1_ii2_mx_1, Pp2_ii2_mx_1)
					Flux_extsubmxsupj_1(i, j) = Flux_extsubmxsupj_1(i, j) - Q_jj_mx_1 * CDj(CCTK_PASS_FTOF, Pm2_jj_mx_1, Pm1_jj_mx_1, Pp1_jj_mx_1, Pp2_jj_mx_1)
					Flux_extsubmxsupi_1(i, j) = Flux_extsubmxsupi_1(i, j) - Q_ii3_mx_1 * CDi(CCTK_PASS_FTOF, Pm2_ii3_mx_1, Pm1_ii3_mx_1, Pp1_ii3_mx_1, Pp2_ii3_mx_1)
					Flux_extsubmxsupj_1(i, j) = Flux_extsubmxsupj_1(i, j) - Q_ji_mx_1 * CDi(CCTK_PASS_FTOF, Pm2_ji_mx_1, Pm1_ji_mx_1, Pp1_ji_mx_1, Pp2_ji_mx_1)
					Flux_extsubmysupj_1(i, j) = Flux_extsubmysupj_1(i, j) - Q_ji_my_1 * CDi(CCTK_PASS_FTOF, Pm2_ji_my_1, Pm1_ji_my_1, Pp1_ji_my_1, Pp2_ji_my_1)
					Flux_extsubmysupj_1(i, j) = Flux_extsubmysupj_1(i, j) - Q_jj_my_1 * CDj(CCTK_PASS_FTOF, Pm2_jj_my_1, Pm1_jj_my_1, Pp1_jj_my_1, Pp2_jj_my_1)
					Flux_extsubmysupi_1(i, j) = Flux_extsubmysupi_1(i, j) - Q_ii_my_1 * CDi(CCTK_PASS_FTOF, Pm2_ii_my_1, Pm1_ii_my_1, Pp1_ii_my_1, Pp2_ii_my_1)
					Flux_extsubmysupj_1(i, j) = Flux_extsubmysupj_1(i, j) - Q_jj2_my_1 * CDj(CCTK_PASS_FTOF, Pm2_jj2_my_1, Pm1_jj2_my_1, Pp1_jj2_my_1, Pp2_jj2_my_1)
					Flux_extsubmysupi_1(i, j) = Flux_extsubmysupi_1(i, j) - Q_ij_my_1 * CDj(CCTK_PASS_FTOF, Pm2_ij_my_1, Pm1_ij_my_1, Pp1_ij_my_1, Pp2_ij_my_1)
					Flux_extsubmysupj_1(i, j) = Flux_extsubmysupj_1(i, j) - Q_jj3_my_1 * CDj(CCTK_PASS_FTOF, Pm2_jj3_my_1, Pm1_jj3_my_1, Pp1_jj3_my_1, Pp2_jj3_my_1)
					end if
					m_n_1 = extrapolatedsubmxsupi(i, j)
					m_tx_1 = 0.0d0
					m_ty_1 = extrapolatedsubmysupi(i, j)
					c_x_1 = c0 * ((extrapolatedsubrhosupi(i, j) / rho0) ** ((gamma + (-1.0d0)) / 2.0d0))
					cn_x_1 = m_n_1 / extrapolatedsubrhosupi(i, j)
					m_n_1 = extrapolatedsubmysupj(i, j)
					m_tx_1 = extrapolatedsubmxsupj(i, j)
					m_ty_1 = 0.0d0
					c_y_1 = c0 * ((extrapolatedsubrhosupj(i, j) / rho0) ** ((gamma + (-1.0d0)) / 2.0d0))
					cn_y_1 = m_n_1 / extrapolatedsubrhosupj(i, j)
					Speedi_1(i, j) = MAX(ABS(c_x_1 + cn_x_1), ABS((-c_x_1) + cn_x_1), ABS(cn_x_1), ABS(cn_x_1))
					Speedj_1(i, j) = MAX(ABS(c_y_1 + cn_y_1), ABS((-c_y_1) + cn_y_1), ABS(cn_y_1), ABS(cn_y_1))
				end if
			end do
		end do
		do j = jEnd/2, jEnd
			do i = iEnd/2, iEnd
!				Boundaries
		if ((FOV_xLower(i, j) .gt. 0) .and. ((i + 1 .le. cctk_lsh(1) .and. (FOV_1(int(i + 1), j) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. (FOV_1(int(i + 2), j) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. (FOV_1(int(i + 3), j) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 3)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_rho, Flux_extsubrhosupi_1, Flux_extsubrhosupi_1, i, d_j_rho, Flux_extsubrhosupj_1, Flux_extsubrhosupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_mx, Flux_extsubmxsupi_1, Flux_extsubmxsupi_1, i, d_j_mx, Flux_extsubmxsupj_1, Flux_extsubmxsupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_my, Flux_extsubmysupi_1, Flux_extsubmysupi_1, i, d_j_my, Flux_extsubmysupj_1, Flux_extsubmysupj_1, j, FOV_1)
			end if
		end if
		if ((FOV_xUpper(i, j) .gt. 0) .and. ((i - 1 .ge. 1 .and. (FOV_1(int(i - 1), j) .gt. 0)) .or. (i - 2 .ge. 1 .and. (FOV_1(int(i - 2), j) .gt. 0)) .or. (i - 3 .ge. 1 .and. (FOV_1(int(i - 3), j) .gt. 0)) .or. ((i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_rho, Flux_extsubrhosupi_1, Flux_extsubrhosupi_1, i, d_j_rho, Flux_extsubrhosupj_1, Flux_extsubrhosupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_mx, Flux_extsubmxsupi_1, Flux_extsubmxsupi_1, i, d_j_mx, Flux_extsubmxsupj_1, Flux_extsubmxsupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_my, Flux_extsubmysupi_1, Flux_extsubmysupi_1, i, d_j_my, Flux_extsubmysupj_1, Flux_extsubmysupj_1, j, FOV_1)
			end if
		end if
		if ((FOV_yLower(i, j) .gt. 0) .and. ((j + 1 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 1)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 2)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 3)) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_rho, Flux_extsubrhosupi_1, Flux_extsubrhosupi_1, i, d_j_rho, Flux_extsubrhosupj_1, Flux_extsubrhosupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_mx, Flux_extsubmxsupi_1, Flux_extsubmxsupi_1, i, d_j_mx, Flux_extsubmxsupj_1, Flux_extsubmxsupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_my, Flux_extsubmysupi_1, Flux_extsubmysupi_1, i, d_j_my, Flux_extsubmysupj_1, Flux_extsubmysupj_1, j, FOV_1)
			end if
		end if
		if ((FOV_yUpper(i, j) .gt. 0) .and. ((j - 1 .ge. 1 .and. (FOV_1(i, int(j - 1)) .gt. 0)) .or. (j - 2 .ge. 1 .and. (FOV_1(i, int(j - 2)) .gt. 0)) .or. (j - 3 .ge. 1 .and. (FOV_1(i, int(j - 3)) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_rho, Flux_extsubrhosupi_1, Flux_extsubrhosupi_1, i, d_j_rho, Flux_extsubrhosupj_1, Flux_extsubrhosupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_mx, Flux_extsubmxsupi_1, Flux_extsubmxsupi_1, i, d_j_mx, Flux_extsubmxsupj_1, Flux_extsubmxsupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_my, Flux_extsubmysupi_1, Flux_extsubmysupi_1, i, d_j_my, Flux_extsubmysupj_1, Flux_extsubmysupj_1, j, FOV_1)
			end if
		end if
			end do
		end do
		do j = jEnd/2 - 1, jStart, -1
			do i = iEnd/2, iEnd
!				Boundaries
		if ((FOV_xLower(i, j) .gt. 0) .and. ((i + 1 .le. cctk_lsh(1) .and. (FOV_1(int(i + 1), j) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. (FOV_1(int(i + 2), j) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. (FOV_1(int(i + 3), j) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 3)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_rho, Flux_extsubrhosupi_1, Flux_extsubrhosupi_1, i, d_j_rho, Flux_extsubrhosupj_1, Flux_extsubrhosupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_mx, Flux_extsubmxsupi_1, Flux_extsubmxsupi_1, i, d_j_mx, Flux_extsubmxsupj_1, Flux_extsubmxsupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_my, Flux_extsubmysupi_1, Flux_extsubmysupi_1, i, d_j_my, Flux_extsubmysupj_1, Flux_extsubmysupj_1, j, FOV_1)
			end if
		end if
		if ((FOV_xUpper(i, j) .gt. 0) .and. ((i - 1 .ge. 1 .and. (FOV_1(int(i - 1), j) .gt. 0)) .or. (i - 2 .ge. 1 .and. (FOV_1(int(i - 2), j) .gt. 0)) .or. (i - 3 .ge. 1 .and. (FOV_1(int(i - 3), j) .gt. 0)) .or. ((i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_rho, Flux_extsubrhosupi_1, Flux_extsubrhosupi_1, i, d_j_rho, Flux_extsubrhosupj_1, Flux_extsubrhosupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_mx, Flux_extsubmxsupi_1, Flux_extsubmxsupi_1, i, d_j_mx, Flux_extsubmxsupj_1, Flux_extsubmxsupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_my, Flux_extsubmysupi_1, Flux_extsubmysupi_1, i, d_j_my, Flux_extsubmysupj_1, Flux_extsubmysupj_1, j, FOV_1)
			end if
		end if
		if ((FOV_yLower(i, j) .gt. 0) .and. ((j + 1 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 1)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 2)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 3)) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_rho, Flux_extsubrhosupi_1, Flux_extsubrhosupi_1, i, d_j_rho, Flux_extsubrhosupj_1, Flux_extsubrhosupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_mx, Flux_extsubmxsupi_1, Flux_extsubmxsupi_1, i, d_j_mx, Flux_extsubmxsupj_1, Flux_extsubmxsupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_my, Flux_extsubmysupi_1, Flux_extsubmysupi_1, i, d_j_my, Flux_extsubmysupj_1, Flux_extsubmysupj_1, j, FOV_1)
			end if
		end if
		if ((FOV_yUpper(i, j) .gt. 0) .and. ((j - 1 .ge. 1 .and. (FOV_1(i, int(j - 1)) .gt. 0)) .or. (j - 2 .ge. 1 .and. (FOV_1(i, int(j - 2)) .gt. 0)) .or. (j - 3 .ge. 1 .and. (FOV_1(i, int(j - 3)) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_rho, Flux_extsubrhosupi_1, Flux_extsubrhosupi_1, i, d_j_rho, Flux_extsubrhosupj_1, Flux_extsubrhosupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_mx, Flux_extsubmxsupi_1, Flux_extsubmxsupi_1, i, d_j_mx, Flux_extsubmxsupj_1, Flux_extsubmxsupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_my, Flux_extsubmysupi_1, Flux_extsubmysupi_1, i, d_j_my, Flux_extsubmysupj_1, Flux_extsubmysupj_1, j, FOV_1)
			end if
		end if
			end do
		end do
		do j = jEnd/2, jEnd
			do i = iEnd/2 - 1, iStart, -1
!				Boundaries
		if ((FOV_xLower(i, j) .gt. 0) .and. ((i + 1 .le. cctk_lsh(1) .and. (FOV_1(int(i + 1), j) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. (FOV_1(int(i + 2), j) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. (FOV_1(int(i + 3), j) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 3)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_rho, Flux_extsubrhosupi_1, Flux_extsubrhosupi_1, i, d_j_rho, Flux_extsubrhosupj_1, Flux_extsubrhosupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_mx, Flux_extsubmxsupi_1, Flux_extsubmxsupi_1, i, d_j_mx, Flux_extsubmxsupj_1, Flux_extsubmxsupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_my, Flux_extsubmysupi_1, Flux_extsubmysupi_1, i, d_j_my, Flux_extsubmysupj_1, Flux_extsubmysupj_1, j, FOV_1)
			end if
		end if
		if ((FOV_xUpper(i, j) .gt. 0) .and. ((i - 1 .ge. 1 .and. (FOV_1(int(i - 1), j) .gt. 0)) .or. (i - 2 .ge. 1 .and. (FOV_1(int(i - 2), j) .gt. 0)) .or. (i - 3 .ge. 1 .and. (FOV_1(int(i - 3), j) .gt. 0)) .or. ((i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_rho, Flux_extsubrhosupi_1, Flux_extsubrhosupi_1, i, d_j_rho, Flux_extsubrhosupj_1, Flux_extsubrhosupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_mx, Flux_extsubmxsupi_1, Flux_extsubmxsupi_1, i, d_j_mx, Flux_extsubmxsupj_1, Flux_extsubmxsupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_my, Flux_extsubmysupi_1, Flux_extsubmysupi_1, i, d_j_my, Flux_extsubmysupj_1, Flux_extsubmysupj_1, j, FOV_1)
			end if
		end if
		if ((FOV_yLower(i, j) .gt. 0) .and. ((j + 1 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 1)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 2)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 3)) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_rho, Flux_extsubrhosupi_1, Flux_extsubrhosupi_1, i, d_j_rho, Flux_extsubrhosupj_1, Flux_extsubrhosupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_mx, Flux_extsubmxsupi_1, Flux_extsubmxsupi_1, i, d_j_mx, Flux_extsubmxsupj_1, Flux_extsubmxsupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_my, Flux_extsubmysupi_1, Flux_extsubmysupi_1, i, d_j_my, Flux_extsubmysupj_1, Flux_extsubmysupj_1, j, FOV_1)
			end if
		end if
		if ((FOV_yUpper(i, j) .gt. 0) .and. ((j - 1 .ge. 1 .and. (FOV_1(i, int(j - 1)) .gt. 0)) .or. (j - 2 .ge. 1 .and. (FOV_1(i, int(j - 2)) .gt. 0)) .or. (j - 3 .ge. 1 .and. (FOV_1(i, int(j - 3)) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_rho, Flux_extsubrhosupi_1, Flux_extsubrhosupi_1, i, d_j_rho, Flux_extsubrhosupj_1, Flux_extsubrhosupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_mx, Flux_extsubmxsupi_1, Flux_extsubmxsupi_1, i, d_j_mx, Flux_extsubmxsupj_1, Flux_extsubmxsupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_my, Flux_extsubmysupi_1, Flux_extsubmysupi_1, i, d_j_my, Flux_extsubmysupj_1, Flux_extsubmysupj_1, j, FOV_1)
			end if
		end if
			end do
		end do
		do j = jEnd/2 - 1, jStart, -1
			do i = iEnd/2 - 1, iStart, -1
!				Boundaries
		if ((FOV_xLower(i, j) .gt. 0) .and. ((i + 1 .le. cctk_lsh(1) .and. (FOV_1(int(i + 1), j) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. (FOV_1(int(i + 2), j) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. (FOV_1(int(i + 3), j) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 3)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_rho, Flux_extsubrhosupi_1, Flux_extsubrhosupi_1, i, d_j_rho, Flux_extsubrhosupj_1, Flux_extsubrhosupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_mx, Flux_extsubmxsupi_1, Flux_extsubmxsupi_1, i, d_j_mx, Flux_extsubmxsupj_1, Flux_extsubmxsupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_my, Flux_extsubmysupi_1, Flux_extsubmysupi_1, i, d_j_my, Flux_extsubmysupj_1, Flux_extsubmysupj_1, j, FOV_1)
			end if
		end if
		if ((FOV_xUpper(i, j) .gt. 0) .and. ((i - 1 .ge. 1 .and. (FOV_1(int(i - 1), j) .gt. 0)) .or. (i - 2 .ge. 1 .and. (FOV_1(int(i - 2), j) .gt. 0)) .or. (i - 3 .ge. 1 .and. (FOV_1(int(i - 3), j) .gt. 0)) .or. ((i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_rho, Flux_extsubrhosupi_1, Flux_extsubrhosupi_1, i, d_j_rho, Flux_extsubrhosupj_1, Flux_extsubrhosupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_mx, Flux_extsubmxsupi_1, Flux_extsubmxsupi_1, i, d_j_mx, Flux_extsubmxsupj_1, Flux_extsubmxsupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_my, Flux_extsubmysupi_1, Flux_extsubmysupi_1, i, d_j_my, Flux_extsubmysupj_1, Flux_extsubmysupj_1, j, FOV_1)
			end if
		end if
		if ((FOV_yLower(i, j) .gt. 0) .and. ((j + 1 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 1)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 2)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 3)) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_rho, Flux_extsubrhosupi_1, Flux_extsubrhosupi_1, i, d_j_rho, Flux_extsubrhosupj_1, Flux_extsubrhosupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_mx, Flux_extsubmxsupi_1, Flux_extsubmxsupi_1, i, d_j_mx, Flux_extsubmxsupj_1, Flux_extsubmxsupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_my, Flux_extsubmysupi_1, Flux_extsubmysupi_1, i, d_j_my, Flux_extsubmysupj_1, Flux_extsubmysupj_1, j, FOV_1)
			end if
		end if
		if ((FOV_yUpper(i, j) .gt. 0) .and. ((j - 1 .ge. 1 .and. (FOV_1(i, int(j - 1)) .gt. 0)) .or. (j - 2 .ge. 1 .and. (FOV_1(i, int(j - 2)) .gt. 0)) .or. (j - 3 .ge. 1 .and. (FOV_1(i, int(j - 3)) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_rho, Flux_extsubrhosupi_1, Flux_extsubrhosupi_1, i, d_j_rho, Flux_extsubrhosupj_1, Flux_extsubrhosupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_mx, Flux_extsubmxsupi_1, Flux_extsubmxsupi_1, i, d_j_mx, Flux_extsubmxsupj_1, Flux_extsubmxsupj_1, j, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_flux(CCTK_PASS_FTOF, d_i_my, Flux_extsubmysupi_1, Flux_extsubmysupi_1, i, d_j_my, Flux_extsubmysupj_1, Flux_extsubmysupj_1, j, FOV_1)
			end if
		end if
			end do
		end do
		call CCTK_SyncGroup(status, cctkGH, "CompressibleCounterflow2D::block1")
		do j = jStart, jEnd
			do i = iStart, iEnd
				if ((FOV_1(i, j) .gt. 0 .or. (((i + 1 .le. cctk_lsh(1) .and. FOV_1(i + 1, j) .gt. 0) .or. (j + 1 .le. cctk_lsh(2) .and. FOV_1(i, j + 1) .gt. 0)) .or. ((i - 1 .ge. 1 .and. FOV_1(i - 1, j) .gt. 0) .or. (j - 1 .ge. 1 .and. FOV_1(i, j - 1) .gt. 0)))) .and. (i + 1 .le. cctk_lsh(1) .and. i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. j - 1 .ge. 1)) then
					Speedi_1(i, j) = MAX(Speedi_1(i, j), Speedi_1(i + 1, j))
					Speedj_1(i, j) = MAX(Speedj_1(i, j), Speedj_1(i, j + 1))
				end if
			end do
		end do
		call CCTK_SyncGroup(status, cctkGH, "CompressibleCounterflow2D::block2")
		do j = jStart, jEnd
			do i = iStart, iEnd
				if ((FOV_1(i, j) .gt. 0) .and. (i + 2 .le. cctk_lsh(1) .and. i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. j - 2 .ge. 1)) then
					fluxAccrho_1 = 0.0d0
					fluxAccmx_1 = k2rho(i, j) * k2fx(i, j)
					fluxAccmy_1 = k2rho(i, j) * k2fy(i, j)
					fluxAccrho_1 = fluxAccrho_1 - FDOCi(CCTK_PASS_FTOF, extrapolatedsubrhosupi, Flux_extsubrhosupi_1, Speedi_1, i, j)
					fluxAccrho_1 = fluxAccrho_1 - FDOCj(CCTK_PASS_FTOF, extrapolatedsubrhosupj, Flux_extsubrhosupj_1, Speedj_1, i, j)
					fluxAccmx_1 = fluxAccmx_1 - FDOCi(CCTK_PASS_FTOF, extrapolatedsubmxsupi, Flux_extsubmxsupi_1, Speedi_1, i, j)
					fluxAccmx_1 = fluxAccmx_1 - FDOCj(CCTK_PASS_FTOF, extrapolatedsubmxsupj, Flux_extsubmxsupj_1, Speedj_1, i, j)
					fluxAccmy_1 = fluxAccmy_1 - FDOCi(CCTK_PASS_FTOF, extrapolatedsubmysupi, Flux_extsubmysupi_1, Speedi_1, i, j)
					fluxAccmy_1 = fluxAccmy_1 - FDOCj(CCTK_PASS_FTOF, extrapolatedsubmysupj, Flux_extsubmysupj_1, Speedj_1, i, j)
					n_x_1 = 0.0d0
					n_y_1 = 0.0d0
					interaction_index_1 = 0.0d0
					if ((n_x_1 .eq. 0.0d0) .and. (n_y_1 .eq. 0.0d0)) then
					if ((FOV_xLower(int(i + 1.0d0), j) .gt. 0.0d0) .or. (FOV_xUpper(int(i + 1.0d0), j) .gt. 0.0d0)) then
					interaction_index_1 = 1.0d0
					n_x_1 = n_x_1 + deltax
					end if
					if ((FOV_xLower(int(i - 1.0d0), j) .gt. 0.0d0) .or. (FOV_xUpper(int(i - 1.0d0), j) .gt. 0.0d0)) then
					interaction_index_1 = 1.0d0
					n_x_1 = n_x_1 - deltax
					end if
					if ((FOV_xLower(i, int(j + 1.0d0)) .gt. 0.0d0) .or. (FOV_xUpper(i, int(j + 1.0d0)) .gt. 0.0d0)) then
					interaction_index_1 = 1.0d0
					n_y_1 = n_y_1 + deltay
					end if
					if ((FOV_xLower(i, int(j - 1.0d0)) .gt. 0.0d0) .or. (FOV_xUpper(i, int(j - 1.0d0)) .gt. 0.0d0)) then
					interaction_index_1 = 1.0d0
					n_y_1 = n_y_1 - deltay
					end if
					end if
					if ((n_x_1 .eq. 0.0d0) .and. (n_y_1 .eq. 0.0d0)) then
					if ((((((FOV_xLower(int(i + 1.0d0), int(j + 1.0d0)) .gt. 0.0d0) .or. (FOV_xUpper(int(i + 1.0d0), int(j + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i + 1.0d0), int(j - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i + 1.0d0), int(j - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i + 1.0d0), j) .gt. 0.0d0)) .or. (FOV_xUpper(int(i + 1.0d0), j) .gt. 0.0d0)) then
					interaction_index_1 = 1.0d0
					n_x_1 = n_x_1 + deltax
					end if
					if ((((((FOV_xLower(int(i - 1.0d0), int(j + 1.0d0)) .gt. 0.0d0) .or. (FOV_xUpper(int(i - 1.0d0), int(j + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i - 1.0d0), int(j - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i - 1.0d0), int(j - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i - 1.0d0), j) .gt. 0.0d0)) .or. (FOV_xUpper(int(i - 1.0d0), j) .gt. 0.0d0)) then
					interaction_index_1 = 1.0d0
					n_x_1 = n_x_1 - deltax
					end if
					if ((((((FOV_xLower(int(i + 1.0d0), int(j + 1.0d0)) .gt. 0.0d0) .or. (FOV_xUpper(int(i + 1.0d0), int(j + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i - 1.0d0), int(j + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i - 1.0d0), int(j + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(i, int(j + 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(i, int(j + 1.0d0)) .gt. 0.0d0)) then
					interaction_index_1 = 1.0d0
					n_y_1 = n_y_1 + deltay
					end if
					if ((((((FOV_xLower(int(i + 1.0d0), int(j - 1.0d0)) .gt. 0.0d0) .or. (FOV_xUpper(int(i + 1.0d0), int(j - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(int(i - 1.0d0), int(j - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(int(i - 1.0d0), int(j - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xLower(i, int(j - 1.0d0)) .gt. 0.0d0)) .or. (FOV_xUpper(i, int(j - 1.0d0)) .gt. 0.0d0)) then
					interaction_index_1 = 1.0d0
					n_y_1 = n_y_1 - deltay
					end if
					end if
					if ((n_x_1 .eq. 0.0d0) .and. (n_y_1 .eq. 0.0d0)) then
					if ((FOV_yLower(int(i + 1.0d0), j) .gt. 0.0d0) .or. (FOV_yUpper(int(i + 1.0d0), j) .gt. 0.0d0)) then
					interaction_index_1 = 2.0d0
					n_x_1 = n_x_1 + deltax
					end if
					if ((FOV_yLower(int(i - 1.0d0), j) .gt. 0.0d0) .or. (FOV_yUpper(int(i - 1.0d0), j) .gt. 0.0d0)) then
					interaction_index_1 = 2.0d0
					n_x_1 = n_x_1 - deltax
					end if
					if ((FOV_yLower(i, int(j + 1.0d0)) .gt. 0.0d0) .or. (FOV_yUpper(i, int(j + 1.0d0)) .gt. 0.0d0)) then
					interaction_index_1 = 2.0d0
					n_y_1 = n_y_1 + deltay
					end if
					if ((FOV_yLower(i, int(j - 1.0d0)) .gt. 0.0d0) .or. (FOV_yUpper(i, int(j - 1.0d0)) .gt. 0.0d0)) then
					interaction_index_1 = 2.0d0
					n_y_1 = n_y_1 - deltay
					end if
					end if
					if ((n_x_1 .eq. 0.0d0) .and. (n_y_1 .eq. 0.0d0)) then
					if ((((((FOV_yLower(int(i + 1.0d0), int(j + 1.0d0)) .gt. 0.0d0) .or. (FOV_yUpper(int(i + 1.0d0), int(j + 1.0d0)) .gt. 0.0d0)) .or. (FOV_yLower(int(i + 1.0d0), int(j - 1.0d0)) .gt. 0.0d0)) .or. (FOV_yUpper(int(i + 1.0d0), int(j - 1.0d0)) .gt. 0.0d0)) .or. (FOV_yLower(int(i + 1.0d0), j) .gt. 0.0d0)) .or. (FOV_yUpper(int(i + 1.0d0), j) .gt. 0.0d0)) then
					interaction_index_1 = 2.0d0
					n_x_1 = n_x_1 + deltax
					end if
					if ((((((FOV_yLower(int(i - 1.0d0), int(j + 1.0d0)) .gt. 0.0d0) .or. (FOV_yUpper(int(i - 1.0d0), int(j + 1.0d0)) .gt. 0.0d0)) .or. (FOV_yLower(int(i - 1.0d0), int(j - 1.0d0)) .gt. 0.0d0)) .or. (FOV_yUpper(int(i - 1.0d0), int(j - 1.0d0)) .gt. 0.0d0)) .or. (FOV_yLower(int(i - 1.0d0), j) .gt. 0.0d0)) .or. (FOV_yUpper(int(i - 1.0d0), j) .gt. 0.0d0)) then
					interaction_index_1 = 2.0d0
					n_x_1 = n_x_1 - deltax
					end if
					if ((((((FOV_yLower(int(i + 1.0d0), int(j + 1.0d0)) .gt. 0.0d0) .or. (FOV_yUpper(int(i + 1.0d0), int(j + 1.0d0)) .gt. 0.0d0)) .or. (FOV_yLower(int(i - 1.0d0), int(j + 1.0d0)) .gt. 0.0d0)) .or. (FOV_yUpper(int(i - 1.0d0), int(j + 1.0d0)) .gt. 0.0d0)) .or. (FOV_yLower(i, int(j + 1.0d0)) .gt. 0.0d0)) .or. (FOV_yUpper(i, int(j + 1.0d0)) .gt. 0.0d0)) then
					interaction_index_1 = 2.0d0
					n_y_1 = n_y_1 + deltay
					end if
					if ((((((FOV_yLower(int(i + 1.0d0), int(j - 1.0d0)) .gt. 0.0d0) .or. (FOV_yUpper(int(i + 1.0d0), int(j - 1.0d0)) .gt. 0.0d0)) .or. (FOV_yLower(int(i - 1.0d0), int(j - 1.0d0)) .gt. 0.0d0)) .or. (FOV_yUpper(int(i - 1.0d0), int(j - 1.0d0)) .gt. 0.0d0)) .or. (FOV_yLower(i, int(j - 1.0d0)) .gt. 0.0d0)) .or. (FOV_yUpper(i, int(j - 1.0d0)) .gt. 0.0d0)) then
					interaction_index_1 = 2.0d0
					n_y_1 = n_y_1 - deltay
					end if
					end if
					if ((n_x_1 .ne. 0.0d0) .or. (n_y_1 .ne. 0.0d0)) then
					mod_normal_1 = SQRT(n_x_1 ** 2.0d0 + n_y_1 ** 2.0d0)
					n_x_1 = n_x_1 / mod_normal_1
					n_y_1 = n_y_1 / mod_normal_1
					end if
					if (interaction_index_1 .eq. 1.0d0) then
					fluxAccm_n_1 = fluxAccmx_1 * n_x_1 + fluxAccmy_1 * n_y_1
					fluxAccm_tx_1 = fluxAccmx_1 - (fluxAccmx_1 * n_x_1 + fluxAccmy_1 * n_y_1) * n_x_1
					fluxAccm_ty_1 = fluxAccmy_1 - (fluxAccmx_1 * n_x_1 + fluxAccmy_1 * n_y_1) * n_y_1
					m_n_1 = k2mx(i, j) * n_x_1 + k2my(i, j) * n_y_1
					m_tx_1 = k2mx(i, j) - (k2mx(i, j) * n_x_1 + k2my(i, j) * n_y_1) * n_x_1
					m_ty_1 = k2my(i, j) - (k2mx(i, j) * n_x_1 + k2my(i, j) * n_y_1) * n_y_1
					c_1 = c0 * ((k2rho(i, j) / rho0) ** ((gamma + (-1.0d0)) / 2.0d0))
					cn_1 = m_n_1 / k2rho(i, j)
					if ((c_1 + cn_1) .gt. 0.0d0) then
					fluxAccwnp_0_1 = fluxAccrho_1 * (-((-c_1) + cn_1)) + fluxAccm_n_1
					else
					fluxAccwnp_0_1 = (-lambdain) * (m_n_1 + u0 * n_x_1 * k2rho(i, j) * (200.0d0 * (0.0d0 + (i - (1 - cctk_lbnd(1))) * deltax) - 1.0d0))
					end if
					if (((-c_1) + cn_1) .gt. 0.0d0) then
					fluxAccwnm_0_1 = fluxAccrho_1 * (-(c_1 + cn_1)) + fluxAccm_n_1
					else
					fluxAccwnm_0_1 = (-lambdain) * (m_n_1 + u0 * n_x_1 * k2rho(i, j) * (200.0d0 * (0.0d0 + (i - (1 - cctk_lbnd(1))) * deltax) - 1.0d0))
					end if
					if (cn_1 .gt. 0.0d0) then
					fluxAccwtx_0_1 = fluxAccrho_1 * (-m_tx_1) / k2rho(i, j) + fluxAccm_tx_1
					else
					fluxAccwtx_0_1 = 0.0d0
					end if
					if (cn_1 .gt. 0.0d0) then
					fluxAccwty_0_1 = fluxAccrho_1 * (-m_ty_1) / k2rho(i, j) + fluxAccm_ty_1
					else
					fluxAccwty_0_1 = (-lambdain) * (k2my(i, j) - u0 * k2rho(i, j) * (200.0d0 * (0.0d0 + (j - (1 - cctk_lbnd(2))) * deltay) - 1.0d0))
					end if
					fluxAccrho_1 = fluxAccwnp_0_1 * 1.0d0 / (2.0d0 * c_1) + fluxAccwnm_0_1 * (-1.0d0) / (2.0d0 * c_1)
					fluxAccm_n_1 = fluxAccwnp_0_1 * (cn_1 / (2.0d0 * c_1) + 1.0d0 / 2.0d0) + fluxAccwnm_0_1 * (-(cn_1 / (2.0d0 * c_1) + (-1.0d0 / 2.0d0)))
					fluxAccm_tx_1 = (fluxAccwnp_0_1 * m_tx_1 / (2.0d0 * k2rho(i, j) * c_1) + fluxAccwnm_0_1 * (-m_tx_1) / (2.0d0 * k2rho(i, j) * c_1)) + fluxAccwtx_0_1
					fluxAccm_ty_1 = (fluxAccwnp_0_1 * m_ty_1 / (2.0d0 * k2rho(i, j) * c_1) + fluxAccwnm_0_1 * (-m_ty_1) / (2.0d0 * k2rho(i, j) * c_1)) + fluxAccwty_0_1
					fluxAccmx_1 = fluxAccm_tx_1 + fluxAccm_n_1 * n_x_1
					fluxAccmy_1 = fluxAccm_ty_1 + fluxAccm_n_1 * n_y_1
					end if
					if (interaction_index_1 .eq. 2.0d0) then
					fluxAccm_n_1 = fluxAccmx_1 * n_x_1 + fluxAccmy_1 * n_y_1
					fluxAccm_tx_1 = fluxAccmx_1 - (fluxAccmx_1 * n_x_1 + fluxAccmy_1 * n_y_1) * n_x_1
					fluxAccm_ty_1 = fluxAccmy_1 - (fluxAccmx_1 * n_x_1 + fluxAccmy_1 * n_y_1) * n_y_1
					m_n_1 = k2mx(i, j) * n_x_1 + k2my(i, j) * n_y_1
					m_tx_1 = k2mx(i, j) - (k2mx(i, j) * n_x_1 + k2my(i, j) * n_y_1) * n_x_1
					m_ty_1 = k2my(i, j) - (k2mx(i, j) * n_x_1 + k2my(i, j) * n_y_1) * n_y_1
					c_1 = c0 * ((k2rho(i, j) / rho0) ** ((gamma + (-1.0d0)) / 2.0d0))
					cn_1 = m_n_1 / k2rho(i, j)
					if ((c_1 + cn_1) .gt. 0.0d0) then
					fluxAccwnp_0_1 = fluxAccrho_1 * (-((-c_1) + cn_1)) + fluxAccm_n_1
					else
					fluxAccwnp_0_1 = ((-c_1) + cn_1) * lambdain * (k2rho(i, j) - rho0 * ((1.0d0 + (gamma * (u0 ** 2.0d0)) / (c0 ** 2.0d0) * (1.0d0 - 1.0d0 / 2.0d0 * ((200.0d0 * (0.0d0 + (i - (1 - cctk_lbnd(1))) * deltax) - 1.0d0) ** 2.0d0 + (200.0d0 * (0.0d0 + (j - (1 - cctk_lbnd(2))) * deltay) - 1.0d0) ** 2.0d0))) ** (1.0d0 / gamma)))
					end if
					if (((-c_1) + cn_1) .gt. 0.0d0) then
					fluxAccwnm_0_1 = fluxAccrho_1 * (-(c_1 + cn_1)) + fluxAccm_n_1
					else
					fluxAccwnm_0_1 = (c_1 + cn_1) * lambdain * (k2rho(i, j) - rho0 * ((1.0d0 + (gamma * (u0 ** 2.0d0)) / (c0 ** 2.0d0) * (1.0d0 - 1.0d0 / 2.0d0 * ((200.0d0 * (0.0d0 + (i - (1 - cctk_lbnd(1))) * deltax) - 1.0d0) ** 2.0d0 + (200.0d0 * (0.0d0 + (j - (1 - cctk_lbnd(2))) * deltay) - 1.0d0) ** 2.0d0))) ** (1.0d0 / gamma)))
					end if
					if (cn_1 .gt. 0.0d0) then
					fluxAccwtx_0_1 = fluxAccrho_1 * (-m_tx_1) / k2rho(i, j) + fluxAccm_tx_1
					else
					fluxAccwtx_0_1 = k2mx(i, j) / k2rho(i, j) * lambdain * (k2rho(i, j) - rho0 * ((1.0d0 + (gamma * (u0 ** 2.0d0)) / (c0 ** 2.0d0) * (1.0d0 - 1.0d0 / 2.0d0 * ((200.0d0 * (0.0d0 + (i - (1 - cctk_lbnd(1))) * deltax) - 1.0d0) ** 2.0d0 + (200.0d0 * (0.0d0 + (j - (1 - cctk_lbnd(2))) * deltay) - 1.0d0) ** 2.0d0))) ** (1.0d0 / gamma)))
					end if
					if (cn_1 .gt. 0.0d0) then
					fluxAccwty_0_1 = fluxAccrho_1 * (-m_ty_1) / k2rho(i, j) + fluxAccm_ty_1
					else
					fluxAccwty_0_1 = 0.0d0
					end if
					fluxAccrho_1 = fluxAccwnp_0_1 * 1.0d0 / (2.0d0 * c_1) + fluxAccwnm_0_1 * (-1.0d0) / (2.0d0 * c_1)
					fluxAccm_n_1 = fluxAccwnp_0_1 * (cn_1 / (2.0d0 * c_1) + 1.0d0 / 2.0d0) + fluxAccwnm_0_1 * (-(cn_1 / (2.0d0 * c_1) + (-1.0d0 / 2.0d0)))
					fluxAccm_tx_1 = (fluxAccwnp_0_1 * m_tx_1 / (2.0d0 * k2rho(i, j) * c_1) + fluxAccwnm_0_1 * (-m_tx_1) / (2.0d0 * k2rho(i, j) * c_1)) + fluxAccwtx_0_1
					fluxAccm_ty_1 = (fluxAccwnp_0_1 * m_ty_1 / (2.0d0 * k2rho(i, j) * c_1) + fluxAccwnm_0_1 * (-m_ty_1) / (2.0d0 * k2rho(i, j) * c_1)) + fluxAccwty_0_1
					fluxAccmx_1 = fluxAccm_tx_1 + fluxAccm_n_1 * n_x_1
					fluxAccmy_1 = fluxAccm_ty_1 + fluxAccm_n_1 * n_y_1
					end if
					rho(i, j) = RK3P3(CCTK_PASS_FTOF, fluxAccrho_1, rho_p, k2rho, i, j)
					mx(i, j) = RK3P3(CCTK_PASS_FTOF, fluxAccmx_1, mx_p, k2mx, i, j)
					my(i, j) = RK3P3(CCTK_PASS_FTOF, fluxAccmy_1, my_p, k2my, i, j)
					p(i, j) = (Paux + ((rho(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					fx(i, j) = pfx
					fy(i, j) = pfy
				end if
			end do
		end do
		call CCTK_SyncGroup(status, cctkGH, "CompressibleCounterflow2D::fields_tl2")
		call CCTK_SyncGroup(status, cctkGH, "CompressibleCounterflow2D::fields_tl1")
		do j = jStart, jEnd
			do i = iStart, iEnd
				extrapolatedsubrhosupi(i, j) = rho(i, j)
				extrapolatedsubrhosupj(i, j) = rho(i, j)
				extrapolatedsubmxsupi(i, j) = mx(i, j)
				extrapolatedsubmxsupj(i, j) = mx(i, j)
				extrapolatedsubmysupi(i, j) = my(i, j)
				extrapolatedsubmysupj(i, j) = my(i, j)
				extrapolatedsubpsupi(i, j) = p(i, j)
				extrapolatedsubpsupj(i, j) = p(i, j)
				extrapolatedsubfxsupi(i, j) = fx(i, j)
				extrapolatedsubfxsupj(i, j) = fx(i, j)
				extrapolatedsubfysupi(i, j) = fy(i, j)
				extrapolatedsubfysupj(i, j) = fy(i, j)
			end do
		end do
		do j = jEnd/2, jEnd
			do i = iEnd/2, iEnd
!				Boundaries
		if ((FOV_xLower(i, j) .gt. 0) .and. ((i + 1 .le. cctk_lsh(1) .and. (FOV_1(int(i + 1), j) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. (FOV_1(int(i + 2), j) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. (FOV_1(int(i + 3), j) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 3)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_rho, rho, extrapolatedsubrhosupi, i, d_j_rho, rho, extrapolatedsubrhosupj, j, rho, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_mx, mx, extrapolatedsubmxsupi, i, d_j_mx, mx, extrapolatedsubmxsupj, j, mx, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_my, my, extrapolatedsubmysupi, i, d_j_my, my, extrapolatedsubmysupj, j, my, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_p(i, j) .ne. 0 .or. d_j_p(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_p(i, j)), int(j - d_j_p(i, j))) .gt. 0) then
					extrapolatedsubpsupi(i, j) = (Paux + ((extrapolatedsubrhosupi(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					extrapolatedsubpsupj(i, j) = (Paux + ((extrapolatedsubrhosupj(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					p(i, j) = (extrapolatedsubpsupi(i, j) + extrapolatedsubpsupj(i, j)) / 2.0d0
					else
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .eq. 0.0d0)) then
					p(i, j) = extrapolatedsubpsupi(i, j)
					extrapolatedsubpsupj(i, j) = p(i, j)
					else
					if ((d_i_p(i, j) .eq. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					p(i, j) = extrapolatedsubpsupj(i, j)
					extrapolatedsubpsupi(i, j) = p(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fx(i, j) .ne. 0 .or. d_j_fx(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fx(i, j)), int(j - d_j_fx(i, j))) .gt. 0) then
					extrapolatedsubfxsupi(i, j) = pfx
					extrapolatedsubfxsupj(i, j) = pfx
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					fx(i, j) = (extrapolatedsubfxsupi(i, j) + extrapolatedsubfxsupj(i, j)) / 2.0d0
					else
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .eq. 0.0d0)) then
					fx(i, j) = extrapolatedsubfxsupi(i, j)
					extrapolatedsubfxsupj(i, j) = fx(i, j)
					else
					if ((d_i_fx(i, j) .eq. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					fx(i, j) = extrapolatedsubfxsupj(i, j)
					extrapolatedsubfxsupi(i, j) = fx(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fy(i, j) .ne. 0 .or. d_j_fy(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fy(i, j)), int(j - d_j_fy(i, j))) .gt. 0) then
					extrapolatedsubfysupi(i, j) = pfy
					extrapolatedsubfysupj(i, j) = pfy
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					fy(i, j) = (extrapolatedsubfysupi(i, j) + extrapolatedsubfysupj(i, j)) / 2.0d0
					else
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .eq. 0.0d0)) then
					fy(i, j) = extrapolatedsubfysupi(i, j)
					extrapolatedsubfysupj(i, j) = fy(i, j)
					else
					if ((d_i_fy(i, j) .eq. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					fy(i, j) = extrapolatedsubfysupj(i, j)
					extrapolatedsubfysupi(i, j) = fy(i, j)
					end if
					end if
					end if
				end if
			end if
		end if
		if ((FOV_xUpper(i, j) .gt. 0) .and. ((i - 1 .ge. 1 .and. (FOV_1(int(i - 1), j) .gt. 0)) .or. (i - 2 .ge. 1 .and. (FOV_1(int(i - 2), j) .gt. 0)) .or. (i - 3 .ge. 1 .and. (FOV_1(int(i - 3), j) .gt. 0)) .or. ((i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_rho, rho, extrapolatedsubrhosupi, i, d_j_rho, rho, extrapolatedsubrhosupj, j, rho, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_mx, mx, extrapolatedsubmxsupi, i, d_j_mx, mx, extrapolatedsubmxsupj, j, mx, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_my, my, extrapolatedsubmysupi, i, d_j_my, my, extrapolatedsubmysupj, j, my, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_p(i, j) .ne. 0 .or. d_j_p(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_p(i, j)), int(j - d_j_p(i, j))) .gt. 0) then
					extrapolatedsubpsupi(i, j) = (Paux + ((extrapolatedsubrhosupi(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					extrapolatedsubpsupj(i, j) = (Paux + ((extrapolatedsubrhosupj(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					p(i, j) = (extrapolatedsubpsupi(i, j) + extrapolatedsubpsupj(i, j)) / 2.0d0
					else
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .eq. 0.0d0)) then
					p(i, j) = extrapolatedsubpsupi(i, j)
					extrapolatedsubpsupj(i, j) = p(i, j)
					else
					if ((d_i_p(i, j) .eq. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					p(i, j) = extrapolatedsubpsupj(i, j)
					extrapolatedsubpsupi(i, j) = p(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fx(i, j) .ne. 0 .or. d_j_fx(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fx(i, j)), int(j - d_j_fx(i, j))) .gt. 0) then
					extrapolatedsubfxsupi(i, j) = pfx
					extrapolatedsubfxsupj(i, j) = pfx
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					fx(i, j) = (extrapolatedsubfxsupi(i, j) + extrapolatedsubfxsupj(i, j)) / 2.0d0
					else
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .eq. 0.0d0)) then
					fx(i, j) = extrapolatedsubfxsupi(i, j)
					extrapolatedsubfxsupj(i, j) = fx(i, j)
					else
					if ((d_i_fx(i, j) .eq. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					fx(i, j) = extrapolatedsubfxsupj(i, j)
					extrapolatedsubfxsupi(i, j) = fx(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fy(i, j) .ne. 0 .or. d_j_fy(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fy(i, j)), int(j - d_j_fy(i, j))) .gt. 0) then
					extrapolatedsubfysupi(i, j) = pfy
					extrapolatedsubfysupj(i, j) = pfy
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					fy(i, j) = (extrapolatedsubfysupi(i, j) + extrapolatedsubfysupj(i, j)) / 2.0d0
					else
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .eq. 0.0d0)) then
					fy(i, j) = extrapolatedsubfysupi(i, j)
					extrapolatedsubfysupj(i, j) = fy(i, j)
					else
					if ((d_i_fy(i, j) .eq. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					fy(i, j) = extrapolatedsubfysupj(i, j)
					extrapolatedsubfysupi(i, j) = fy(i, j)
					end if
					end if
					end if
				end if
			end if
		end if
		if ((FOV_yLower(i, j) .gt. 0) .and. ((j + 1 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 1)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 2)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 3)) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_rho, rho, extrapolatedsubrhosupi, i, d_j_rho, rho, extrapolatedsubrhosupj, j, rho, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_mx, mx, extrapolatedsubmxsupi, i, d_j_mx, mx, extrapolatedsubmxsupj, j, mx, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_my, my, extrapolatedsubmysupi, i, d_j_my, my, extrapolatedsubmysupj, j, my, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_p(i, j) .ne. 0 .or. d_j_p(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_p(i, j)), int(j - d_j_p(i, j))) .gt. 0) then
					extrapolatedsubpsupi(i, j) = (Paux + ((extrapolatedsubrhosupi(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					extrapolatedsubpsupj(i, j) = (Paux + ((extrapolatedsubrhosupj(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					p(i, j) = (extrapolatedsubpsupi(i, j) + extrapolatedsubpsupj(i, j)) / 2.0d0
					else
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .eq. 0.0d0)) then
					p(i, j) = extrapolatedsubpsupi(i, j)
					extrapolatedsubpsupj(i, j) = p(i, j)
					else
					if ((d_i_p(i, j) .eq. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					p(i, j) = extrapolatedsubpsupj(i, j)
					extrapolatedsubpsupi(i, j) = p(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fx(i, j) .ne. 0 .or. d_j_fx(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fx(i, j)), int(j - d_j_fx(i, j))) .gt. 0) then
					extrapolatedsubfxsupi(i, j) = pfx
					extrapolatedsubfxsupj(i, j) = pfx
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					fx(i, j) = (extrapolatedsubfxsupi(i, j) + extrapolatedsubfxsupj(i, j)) / 2.0d0
					else
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .eq. 0.0d0)) then
					fx(i, j) = extrapolatedsubfxsupi(i, j)
					extrapolatedsubfxsupj(i, j) = fx(i, j)
					else
					if ((d_i_fx(i, j) .eq. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					fx(i, j) = extrapolatedsubfxsupj(i, j)
					extrapolatedsubfxsupi(i, j) = fx(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fy(i, j) .ne. 0 .or. d_j_fy(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fy(i, j)), int(j - d_j_fy(i, j))) .gt. 0) then
					extrapolatedsubfysupi(i, j) = pfy
					extrapolatedsubfysupj(i, j) = pfy
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					fy(i, j) = (extrapolatedsubfysupi(i, j) + extrapolatedsubfysupj(i, j)) / 2.0d0
					else
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .eq. 0.0d0)) then
					fy(i, j) = extrapolatedsubfysupi(i, j)
					extrapolatedsubfysupj(i, j) = fy(i, j)
					else
					if ((d_i_fy(i, j) .eq. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					fy(i, j) = extrapolatedsubfysupj(i, j)
					extrapolatedsubfysupi(i, j) = fy(i, j)
					end if
					end if
					end if
				end if
			end if
		end if
		if ((FOV_yUpper(i, j) .gt. 0) .and. ((j - 1 .ge. 1 .and. (FOV_1(i, int(j - 1)) .gt. 0)) .or. (j - 2 .ge. 1 .and. (FOV_1(i, int(j - 2)) .gt. 0)) .or. (j - 3 .ge. 1 .and. (FOV_1(i, int(j - 3)) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_rho, rho, extrapolatedsubrhosupi, i, d_j_rho, rho, extrapolatedsubrhosupj, j, rho, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_mx, mx, extrapolatedsubmxsupi, i, d_j_mx, mx, extrapolatedsubmxsupj, j, mx, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_my, my, extrapolatedsubmysupi, i, d_j_my, my, extrapolatedsubmysupj, j, my, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_p(i, j) .ne. 0 .or. d_j_p(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_p(i, j)), int(j - d_j_p(i, j))) .gt. 0) then
					extrapolatedsubpsupi(i, j) = (Paux + ((extrapolatedsubrhosupi(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					extrapolatedsubpsupj(i, j) = (Paux + ((extrapolatedsubrhosupj(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					p(i, j) = (extrapolatedsubpsupi(i, j) + extrapolatedsubpsupj(i, j)) / 2.0d0
					else
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .eq. 0.0d0)) then
					p(i, j) = extrapolatedsubpsupi(i, j)
					extrapolatedsubpsupj(i, j) = p(i, j)
					else
					if ((d_i_p(i, j) .eq. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					p(i, j) = extrapolatedsubpsupj(i, j)
					extrapolatedsubpsupi(i, j) = p(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fx(i, j) .ne. 0 .or. d_j_fx(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fx(i, j)), int(j - d_j_fx(i, j))) .gt. 0) then
					extrapolatedsubfxsupi(i, j) = pfx
					extrapolatedsubfxsupj(i, j) = pfx
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					fx(i, j) = (extrapolatedsubfxsupi(i, j) + extrapolatedsubfxsupj(i, j)) / 2.0d0
					else
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .eq. 0.0d0)) then
					fx(i, j) = extrapolatedsubfxsupi(i, j)
					extrapolatedsubfxsupj(i, j) = fx(i, j)
					else
					if ((d_i_fx(i, j) .eq. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					fx(i, j) = extrapolatedsubfxsupj(i, j)
					extrapolatedsubfxsupi(i, j) = fx(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fy(i, j) .ne. 0 .or. d_j_fy(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fy(i, j)), int(j - d_j_fy(i, j))) .gt. 0) then
					extrapolatedsubfysupi(i, j) = pfy
					extrapolatedsubfysupj(i, j) = pfy
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					fy(i, j) = (extrapolatedsubfysupi(i, j) + extrapolatedsubfysupj(i, j)) / 2.0d0
					else
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .eq. 0.0d0)) then
					fy(i, j) = extrapolatedsubfysupi(i, j)
					extrapolatedsubfysupj(i, j) = fy(i, j)
					else
					if ((d_i_fy(i, j) .eq. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					fy(i, j) = extrapolatedsubfysupj(i, j)
					extrapolatedsubfysupi(i, j) = fy(i, j)
					end if
					end if
					end if
				end if
			end if
		end if
			end do
		end do
		do j = jEnd/2 - 1, jStart, -1
			do i = iEnd/2, iEnd
!				Boundaries
		if ((FOV_xLower(i, j) .gt. 0) .and. ((i + 1 .le. cctk_lsh(1) .and. (FOV_1(int(i + 1), j) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. (FOV_1(int(i + 2), j) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. (FOV_1(int(i + 3), j) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 3)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_rho, rho, extrapolatedsubrhosupi, i, d_j_rho, rho, extrapolatedsubrhosupj, j, rho, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_mx, mx, extrapolatedsubmxsupi, i, d_j_mx, mx, extrapolatedsubmxsupj, j, mx, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_my, my, extrapolatedsubmysupi, i, d_j_my, my, extrapolatedsubmysupj, j, my, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_p(i, j) .ne. 0 .or. d_j_p(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_p(i, j)), int(j - d_j_p(i, j))) .gt. 0) then
					extrapolatedsubpsupi(i, j) = (Paux + ((extrapolatedsubrhosupi(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					extrapolatedsubpsupj(i, j) = (Paux + ((extrapolatedsubrhosupj(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					p(i, j) = (extrapolatedsubpsupi(i, j) + extrapolatedsubpsupj(i, j)) / 2.0d0
					else
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .eq. 0.0d0)) then
					p(i, j) = extrapolatedsubpsupi(i, j)
					extrapolatedsubpsupj(i, j) = p(i, j)
					else
					if ((d_i_p(i, j) .eq. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					p(i, j) = extrapolatedsubpsupj(i, j)
					extrapolatedsubpsupi(i, j) = p(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fx(i, j) .ne. 0 .or. d_j_fx(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fx(i, j)), int(j - d_j_fx(i, j))) .gt. 0) then
					extrapolatedsubfxsupi(i, j) = pfx
					extrapolatedsubfxsupj(i, j) = pfx
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					fx(i, j) = (extrapolatedsubfxsupi(i, j) + extrapolatedsubfxsupj(i, j)) / 2.0d0
					else
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .eq. 0.0d0)) then
					fx(i, j) = extrapolatedsubfxsupi(i, j)
					extrapolatedsubfxsupj(i, j) = fx(i, j)
					else
					if ((d_i_fx(i, j) .eq. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					fx(i, j) = extrapolatedsubfxsupj(i, j)
					extrapolatedsubfxsupi(i, j) = fx(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fy(i, j) .ne. 0 .or. d_j_fy(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fy(i, j)), int(j - d_j_fy(i, j))) .gt. 0) then
					extrapolatedsubfysupi(i, j) = pfy
					extrapolatedsubfysupj(i, j) = pfy
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					fy(i, j) = (extrapolatedsubfysupi(i, j) + extrapolatedsubfysupj(i, j)) / 2.0d0
					else
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .eq. 0.0d0)) then
					fy(i, j) = extrapolatedsubfysupi(i, j)
					extrapolatedsubfysupj(i, j) = fy(i, j)
					else
					if ((d_i_fy(i, j) .eq. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					fy(i, j) = extrapolatedsubfysupj(i, j)
					extrapolatedsubfysupi(i, j) = fy(i, j)
					end if
					end if
					end if
				end if
			end if
		end if
		if ((FOV_xUpper(i, j) .gt. 0) .and. ((i - 1 .ge. 1 .and. (FOV_1(int(i - 1), j) .gt. 0)) .or. (i - 2 .ge. 1 .and. (FOV_1(int(i - 2), j) .gt. 0)) .or. (i - 3 .ge. 1 .and. (FOV_1(int(i - 3), j) .gt. 0)) .or. ((i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_rho, rho, extrapolatedsubrhosupi, i, d_j_rho, rho, extrapolatedsubrhosupj, j, rho, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_mx, mx, extrapolatedsubmxsupi, i, d_j_mx, mx, extrapolatedsubmxsupj, j, mx, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_my, my, extrapolatedsubmysupi, i, d_j_my, my, extrapolatedsubmysupj, j, my, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_p(i, j) .ne. 0 .or. d_j_p(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_p(i, j)), int(j - d_j_p(i, j))) .gt. 0) then
					extrapolatedsubpsupi(i, j) = (Paux + ((extrapolatedsubrhosupi(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					extrapolatedsubpsupj(i, j) = (Paux + ((extrapolatedsubrhosupj(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					p(i, j) = (extrapolatedsubpsupi(i, j) + extrapolatedsubpsupj(i, j)) / 2.0d0
					else
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .eq. 0.0d0)) then
					p(i, j) = extrapolatedsubpsupi(i, j)
					extrapolatedsubpsupj(i, j) = p(i, j)
					else
					if ((d_i_p(i, j) .eq. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					p(i, j) = extrapolatedsubpsupj(i, j)
					extrapolatedsubpsupi(i, j) = p(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fx(i, j) .ne. 0 .or. d_j_fx(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fx(i, j)), int(j - d_j_fx(i, j))) .gt. 0) then
					extrapolatedsubfxsupi(i, j) = pfx
					extrapolatedsubfxsupj(i, j) = pfx
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					fx(i, j) = (extrapolatedsubfxsupi(i, j) + extrapolatedsubfxsupj(i, j)) / 2.0d0
					else
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .eq. 0.0d0)) then
					fx(i, j) = extrapolatedsubfxsupi(i, j)
					extrapolatedsubfxsupj(i, j) = fx(i, j)
					else
					if ((d_i_fx(i, j) .eq. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					fx(i, j) = extrapolatedsubfxsupj(i, j)
					extrapolatedsubfxsupi(i, j) = fx(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fy(i, j) .ne. 0 .or. d_j_fy(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fy(i, j)), int(j - d_j_fy(i, j))) .gt. 0) then
					extrapolatedsubfysupi(i, j) = pfy
					extrapolatedsubfysupj(i, j) = pfy
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					fy(i, j) = (extrapolatedsubfysupi(i, j) + extrapolatedsubfysupj(i, j)) / 2.0d0
					else
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .eq. 0.0d0)) then
					fy(i, j) = extrapolatedsubfysupi(i, j)
					extrapolatedsubfysupj(i, j) = fy(i, j)
					else
					if ((d_i_fy(i, j) .eq. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					fy(i, j) = extrapolatedsubfysupj(i, j)
					extrapolatedsubfysupi(i, j) = fy(i, j)
					end if
					end if
					end if
				end if
			end if
		end if
		if ((FOV_yLower(i, j) .gt. 0) .and. ((j + 1 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 1)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 2)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 3)) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_rho, rho, extrapolatedsubrhosupi, i, d_j_rho, rho, extrapolatedsubrhosupj, j, rho, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_mx, mx, extrapolatedsubmxsupi, i, d_j_mx, mx, extrapolatedsubmxsupj, j, mx, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_my, my, extrapolatedsubmysupi, i, d_j_my, my, extrapolatedsubmysupj, j, my, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_p(i, j) .ne. 0 .or. d_j_p(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_p(i, j)), int(j - d_j_p(i, j))) .gt. 0) then
					extrapolatedsubpsupi(i, j) = (Paux + ((extrapolatedsubrhosupi(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					extrapolatedsubpsupj(i, j) = (Paux + ((extrapolatedsubrhosupj(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					p(i, j) = (extrapolatedsubpsupi(i, j) + extrapolatedsubpsupj(i, j)) / 2.0d0
					else
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .eq. 0.0d0)) then
					p(i, j) = extrapolatedsubpsupi(i, j)
					extrapolatedsubpsupj(i, j) = p(i, j)
					else
					if ((d_i_p(i, j) .eq. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					p(i, j) = extrapolatedsubpsupj(i, j)
					extrapolatedsubpsupi(i, j) = p(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fx(i, j) .ne. 0 .or. d_j_fx(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fx(i, j)), int(j - d_j_fx(i, j))) .gt. 0) then
					extrapolatedsubfxsupi(i, j) = pfx
					extrapolatedsubfxsupj(i, j) = pfx
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					fx(i, j) = (extrapolatedsubfxsupi(i, j) + extrapolatedsubfxsupj(i, j)) / 2.0d0
					else
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .eq. 0.0d0)) then
					fx(i, j) = extrapolatedsubfxsupi(i, j)
					extrapolatedsubfxsupj(i, j) = fx(i, j)
					else
					if ((d_i_fx(i, j) .eq. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					fx(i, j) = extrapolatedsubfxsupj(i, j)
					extrapolatedsubfxsupi(i, j) = fx(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fy(i, j) .ne. 0 .or. d_j_fy(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fy(i, j)), int(j - d_j_fy(i, j))) .gt. 0) then
					extrapolatedsubfysupi(i, j) = pfy
					extrapolatedsubfysupj(i, j) = pfy
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					fy(i, j) = (extrapolatedsubfysupi(i, j) + extrapolatedsubfysupj(i, j)) / 2.0d0
					else
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .eq. 0.0d0)) then
					fy(i, j) = extrapolatedsubfysupi(i, j)
					extrapolatedsubfysupj(i, j) = fy(i, j)
					else
					if ((d_i_fy(i, j) .eq. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					fy(i, j) = extrapolatedsubfysupj(i, j)
					extrapolatedsubfysupi(i, j) = fy(i, j)
					end if
					end if
					end if
				end if
			end if
		end if
		if ((FOV_yUpper(i, j) .gt. 0) .and. ((j - 1 .ge. 1 .and. (FOV_1(i, int(j - 1)) .gt. 0)) .or. (j - 2 .ge. 1 .and. (FOV_1(i, int(j - 2)) .gt. 0)) .or. (j - 3 .ge. 1 .and. (FOV_1(i, int(j - 3)) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_rho, rho, extrapolatedsubrhosupi, i, d_j_rho, rho, extrapolatedsubrhosupj, j, rho, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_mx, mx, extrapolatedsubmxsupi, i, d_j_mx, mx, extrapolatedsubmxsupj, j, mx, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_my, my, extrapolatedsubmysupi, i, d_j_my, my, extrapolatedsubmysupj, j, my, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_p(i, j) .ne. 0 .or. d_j_p(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_p(i, j)), int(j - d_j_p(i, j))) .gt. 0) then
					extrapolatedsubpsupi(i, j) = (Paux + ((extrapolatedsubrhosupi(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					extrapolatedsubpsupj(i, j) = (Paux + ((extrapolatedsubrhosupj(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					p(i, j) = (extrapolatedsubpsupi(i, j) + extrapolatedsubpsupj(i, j)) / 2.0d0
					else
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .eq. 0.0d0)) then
					p(i, j) = extrapolatedsubpsupi(i, j)
					extrapolatedsubpsupj(i, j) = p(i, j)
					else
					if ((d_i_p(i, j) .eq. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					p(i, j) = extrapolatedsubpsupj(i, j)
					extrapolatedsubpsupi(i, j) = p(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fx(i, j) .ne. 0 .or. d_j_fx(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fx(i, j)), int(j - d_j_fx(i, j))) .gt. 0) then
					extrapolatedsubfxsupi(i, j) = pfx
					extrapolatedsubfxsupj(i, j) = pfx
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					fx(i, j) = (extrapolatedsubfxsupi(i, j) + extrapolatedsubfxsupj(i, j)) / 2.0d0
					else
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .eq. 0.0d0)) then
					fx(i, j) = extrapolatedsubfxsupi(i, j)
					extrapolatedsubfxsupj(i, j) = fx(i, j)
					else
					if ((d_i_fx(i, j) .eq. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					fx(i, j) = extrapolatedsubfxsupj(i, j)
					extrapolatedsubfxsupi(i, j) = fx(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fy(i, j) .ne. 0 .or. d_j_fy(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fy(i, j)), int(j - d_j_fy(i, j))) .gt. 0) then
					extrapolatedsubfysupi(i, j) = pfy
					extrapolatedsubfysupj(i, j) = pfy
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					fy(i, j) = (extrapolatedsubfysupi(i, j) + extrapolatedsubfysupj(i, j)) / 2.0d0
					else
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .eq. 0.0d0)) then
					fy(i, j) = extrapolatedsubfysupi(i, j)
					extrapolatedsubfysupj(i, j) = fy(i, j)
					else
					if ((d_i_fy(i, j) .eq. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					fy(i, j) = extrapolatedsubfysupj(i, j)
					extrapolatedsubfysupi(i, j) = fy(i, j)
					end if
					end if
					end if
				end if
			end if
		end if
			end do
		end do
		do j = jEnd/2, jEnd
			do i = iEnd/2 - 1, iStart, -1
!				Boundaries
		if ((FOV_xLower(i, j) .gt. 0) .and. ((i + 1 .le. cctk_lsh(1) .and. (FOV_1(int(i + 1), j) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. (FOV_1(int(i + 2), j) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. (FOV_1(int(i + 3), j) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 3)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_rho, rho, extrapolatedsubrhosupi, i, d_j_rho, rho, extrapolatedsubrhosupj, j, rho, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_mx, mx, extrapolatedsubmxsupi, i, d_j_mx, mx, extrapolatedsubmxsupj, j, mx, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_my, my, extrapolatedsubmysupi, i, d_j_my, my, extrapolatedsubmysupj, j, my, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_p(i, j) .ne. 0 .or. d_j_p(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_p(i, j)), int(j - d_j_p(i, j))) .gt. 0) then
					extrapolatedsubpsupi(i, j) = (Paux + ((extrapolatedsubrhosupi(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					extrapolatedsubpsupj(i, j) = (Paux + ((extrapolatedsubrhosupj(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					p(i, j) = (extrapolatedsubpsupi(i, j) + extrapolatedsubpsupj(i, j)) / 2.0d0
					else
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .eq. 0.0d0)) then
					p(i, j) = extrapolatedsubpsupi(i, j)
					extrapolatedsubpsupj(i, j) = p(i, j)
					else
					if ((d_i_p(i, j) .eq. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					p(i, j) = extrapolatedsubpsupj(i, j)
					extrapolatedsubpsupi(i, j) = p(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fx(i, j) .ne. 0 .or. d_j_fx(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fx(i, j)), int(j - d_j_fx(i, j))) .gt. 0) then
					extrapolatedsubfxsupi(i, j) = pfx
					extrapolatedsubfxsupj(i, j) = pfx
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					fx(i, j) = (extrapolatedsubfxsupi(i, j) + extrapolatedsubfxsupj(i, j)) / 2.0d0
					else
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .eq. 0.0d0)) then
					fx(i, j) = extrapolatedsubfxsupi(i, j)
					extrapolatedsubfxsupj(i, j) = fx(i, j)
					else
					if ((d_i_fx(i, j) .eq. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					fx(i, j) = extrapolatedsubfxsupj(i, j)
					extrapolatedsubfxsupi(i, j) = fx(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fy(i, j) .ne. 0 .or. d_j_fy(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fy(i, j)), int(j - d_j_fy(i, j))) .gt. 0) then
					extrapolatedsubfysupi(i, j) = pfy
					extrapolatedsubfysupj(i, j) = pfy
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					fy(i, j) = (extrapolatedsubfysupi(i, j) + extrapolatedsubfysupj(i, j)) / 2.0d0
					else
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .eq. 0.0d0)) then
					fy(i, j) = extrapolatedsubfysupi(i, j)
					extrapolatedsubfysupj(i, j) = fy(i, j)
					else
					if ((d_i_fy(i, j) .eq. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					fy(i, j) = extrapolatedsubfysupj(i, j)
					extrapolatedsubfysupi(i, j) = fy(i, j)
					end if
					end if
					end if
				end if
			end if
		end if
		if ((FOV_xUpper(i, j) .gt. 0) .and. ((i - 1 .ge. 1 .and. (FOV_1(int(i - 1), j) .gt. 0)) .or. (i - 2 .ge. 1 .and. (FOV_1(int(i - 2), j) .gt. 0)) .or. (i - 3 .ge. 1 .and. (FOV_1(int(i - 3), j) .gt. 0)) .or. ((i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_rho, rho, extrapolatedsubrhosupi, i, d_j_rho, rho, extrapolatedsubrhosupj, j, rho, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_mx, mx, extrapolatedsubmxsupi, i, d_j_mx, mx, extrapolatedsubmxsupj, j, mx, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_my, my, extrapolatedsubmysupi, i, d_j_my, my, extrapolatedsubmysupj, j, my, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_p(i, j) .ne. 0 .or. d_j_p(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_p(i, j)), int(j - d_j_p(i, j))) .gt. 0) then
					extrapolatedsubpsupi(i, j) = (Paux + ((extrapolatedsubrhosupi(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					extrapolatedsubpsupj(i, j) = (Paux + ((extrapolatedsubrhosupj(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					p(i, j) = (extrapolatedsubpsupi(i, j) + extrapolatedsubpsupj(i, j)) / 2.0d0
					else
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .eq. 0.0d0)) then
					p(i, j) = extrapolatedsubpsupi(i, j)
					extrapolatedsubpsupj(i, j) = p(i, j)
					else
					if ((d_i_p(i, j) .eq. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					p(i, j) = extrapolatedsubpsupj(i, j)
					extrapolatedsubpsupi(i, j) = p(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fx(i, j) .ne. 0 .or. d_j_fx(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fx(i, j)), int(j - d_j_fx(i, j))) .gt. 0) then
					extrapolatedsubfxsupi(i, j) = pfx
					extrapolatedsubfxsupj(i, j) = pfx
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					fx(i, j) = (extrapolatedsubfxsupi(i, j) + extrapolatedsubfxsupj(i, j)) / 2.0d0
					else
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .eq. 0.0d0)) then
					fx(i, j) = extrapolatedsubfxsupi(i, j)
					extrapolatedsubfxsupj(i, j) = fx(i, j)
					else
					if ((d_i_fx(i, j) .eq. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					fx(i, j) = extrapolatedsubfxsupj(i, j)
					extrapolatedsubfxsupi(i, j) = fx(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fy(i, j) .ne. 0 .or. d_j_fy(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fy(i, j)), int(j - d_j_fy(i, j))) .gt. 0) then
					extrapolatedsubfysupi(i, j) = pfy
					extrapolatedsubfysupj(i, j) = pfy
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					fy(i, j) = (extrapolatedsubfysupi(i, j) + extrapolatedsubfysupj(i, j)) / 2.0d0
					else
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .eq. 0.0d0)) then
					fy(i, j) = extrapolatedsubfysupi(i, j)
					extrapolatedsubfysupj(i, j) = fy(i, j)
					else
					if ((d_i_fy(i, j) .eq. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					fy(i, j) = extrapolatedsubfysupj(i, j)
					extrapolatedsubfysupi(i, j) = fy(i, j)
					end if
					end if
					end if
				end if
			end if
		end if
		if ((FOV_yLower(i, j) .gt. 0) .and. ((j + 1 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 1)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 2)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 3)) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_rho, rho, extrapolatedsubrhosupi, i, d_j_rho, rho, extrapolatedsubrhosupj, j, rho, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_mx, mx, extrapolatedsubmxsupi, i, d_j_mx, mx, extrapolatedsubmxsupj, j, mx, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_my, my, extrapolatedsubmysupi, i, d_j_my, my, extrapolatedsubmysupj, j, my, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_p(i, j) .ne. 0 .or. d_j_p(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_p(i, j)), int(j - d_j_p(i, j))) .gt. 0) then
					extrapolatedsubpsupi(i, j) = (Paux + ((extrapolatedsubrhosupi(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					extrapolatedsubpsupj(i, j) = (Paux + ((extrapolatedsubrhosupj(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					p(i, j) = (extrapolatedsubpsupi(i, j) + extrapolatedsubpsupj(i, j)) / 2.0d0
					else
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .eq. 0.0d0)) then
					p(i, j) = extrapolatedsubpsupi(i, j)
					extrapolatedsubpsupj(i, j) = p(i, j)
					else
					if ((d_i_p(i, j) .eq. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					p(i, j) = extrapolatedsubpsupj(i, j)
					extrapolatedsubpsupi(i, j) = p(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fx(i, j) .ne. 0 .or. d_j_fx(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fx(i, j)), int(j - d_j_fx(i, j))) .gt. 0) then
					extrapolatedsubfxsupi(i, j) = pfx
					extrapolatedsubfxsupj(i, j) = pfx
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					fx(i, j) = (extrapolatedsubfxsupi(i, j) + extrapolatedsubfxsupj(i, j)) / 2.0d0
					else
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .eq. 0.0d0)) then
					fx(i, j) = extrapolatedsubfxsupi(i, j)
					extrapolatedsubfxsupj(i, j) = fx(i, j)
					else
					if ((d_i_fx(i, j) .eq. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					fx(i, j) = extrapolatedsubfxsupj(i, j)
					extrapolatedsubfxsupi(i, j) = fx(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fy(i, j) .ne. 0 .or. d_j_fy(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fy(i, j)), int(j - d_j_fy(i, j))) .gt. 0) then
					extrapolatedsubfysupi(i, j) = pfy
					extrapolatedsubfysupj(i, j) = pfy
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					fy(i, j) = (extrapolatedsubfysupi(i, j) + extrapolatedsubfysupj(i, j)) / 2.0d0
					else
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .eq. 0.0d0)) then
					fy(i, j) = extrapolatedsubfysupi(i, j)
					extrapolatedsubfysupj(i, j) = fy(i, j)
					else
					if ((d_i_fy(i, j) .eq. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					fy(i, j) = extrapolatedsubfysupj(i, j)
					extrapolatedsubfysupi(i, j) = fy(i, j)
					end if
					end if
					end if
				end if
			end if
		end if
		if ((FOV_yUpper(i, j) .gt. 0) .and. ((j - 1 .ge. 1 .and. (FOV_1(i, int(j - 1)) .gt. 0)) .or. (j - 2 .ge. 1 .and. (FOV_1(i, int(j - 2)) .gt. 0)) .or. (j - 3 .ge. 1 .and. (FOV_1(i, int(j - 3)) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_rho, rho, extrapolatedsubrhosupi, i, d_j_rho, rho, extrapolatedsubrhosupj, j, rho, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_mx, mx, extrapolatedsubmxsupi, i, d_j_mx, mx, extrapolatedsubmxsupj, j, mx, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_my, my, extrapolatedsubmysupi, i, d_j_my, my, extrapolatedsubmysupj, j, my, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_p(i, j) .ne. 0 .or. d_j_p(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_p(i, j)), int(j - d_j_p(i, j))) .gt. 0) then
					extrapolatedsubpsupi(i, j) = (Paux + ((extrapolatedsubrhosupi(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					extrapolatedsubpsupj(i, j) = (Paux + ((extrapolatedsubrhosupj(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					p(i, j) = (extrapolatedsubpsupi(i, j) + extrapolatedsubpsupj(i, j)) / 2.0d0
					else
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .eq. 0.0d0)) then
					p(i, j) = extrapolatedsubpsupi(i, j)
					extrapolatedsubpsupj(i, j) = p(i, j)
					else
					if ((d_i_p(i, j) .eq. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					p(i, j) = extrapolatedsubpsupj(i, j)
					extrapolatedsubpsupi(i, j) = p(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fx(i, j) .ne. 0 .or. d_j_fx(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fx(i, j)), int(j - d_j_fx(i, j))) .gt. 0) then
					extrapolatedsubfxsupi(i, j) = pfx
					extrapolatedsubfxsupj(i, j) = pfx
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					fx(i, j) = (extrapolatedsubfxsupi(i, j) + extrapolatedsubfxsupj(i, j)) / 2.0d0
					else
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .eq. 0.0d0)) then
					fx(i, j) = extrapolatedsubfxsupi(i, j)
					extrapolatedsubfxsupj(i, j) = fx(i, j)
					else
					if ((d_i_fx(i, j) .eq. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					fx(i, j) = extrapolatedsubfxsupj(i, j)
					extrapolatedsubfxsupi(i, j) = fx(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fy(i, j) .ne. 0 .or. d_j_fy(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fy(i, j)), int(j - d_j_fy(i, j))) .gt. 0) then
					extrapolatedsubfysupi(i, j) = pfy
					extrapolatedsubfysupj(i, j) = pfy
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					fy(i, j) = (extrapolatedsubfysupi(i, j) + extrapolatedsubfysupj(i, j)) / 2.0d0
					else
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .eq. 0.0d0)) then
					fy(i, j) = extrapolatedsubfysupi(i, j)
					extrapolatedsubfysupj(i, j) = fy(i, j)
					else
					if ((d_i_fy(i, j) .eq. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					fy(i, j) = extrapolatedsubfysupj(i, j)
					extrapolatedsubfysupi(i, j) = fy(i, j)
					end if
					end if
					end if
				end if
			end if
		end if
			end do
		end do
		do j = jEnd/2 - 1, jStart, -1
			do i = iEnd/2 - 1, iStart, -1
!				Boundaries
		if ((FOV_xLower(i, j) .gt. 0) .and. ((i + 1 .le. cctk_lsh(1) .and. (FOV_1(int(i + 1), j) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. (FOV_1(int(i + 2), j) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. (FOV_1(int(i + 3), j) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 3)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_rho, rho, extrapolatedsubrhosupi, i, d_j_rho, rho, extrapolatedsubrhosupj, j, rho, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_mx, mx, extrapolatedsubmxsupi, i, d_j_mx, mx, extrapolatedsubmxsupj, j, mx, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_my, my, extrapolatedsubmysupi, i, d_j_my, my, extrapolatedsubmysupj, j, my, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_p(i, j) .ne. 0 .or. d_j_p(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_p(i, j)), int(j - d_j_p(i, j))) .gt. 0) then
					extrapolatedsubpsupi(i, j) = (Paux + ((extrapolatedsubrhosupi(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					extrapolatedsubpsupj(i, j) = (Paux + ((extrapolatedsubrhosupj(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					p(i, j) = (extrapolatedsubpsupi(i, j) + extrapolatedsubpsupj(i, j)) / 2.0d0
					else
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .eq. 0.0d0)) then
					p(i, j) = extrapolatedsubpsupi(i, j)
					extrapolatedsubpsupj(i, j) = p(i, j)
					else
					if ((d_i_p(i, j) .eq. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					p(i, j) = extrapolatedsubpsupj(i, j)
					extrapolatedsubpsupi(i, j) = p(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fx(i, j) .ne. 0 .or. d_j_fx(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fx(i, j)), int(j - d_j_fx(i, j))) .gt. 0) then
					extrapolatedsubfxsupi(i, j) = pfx
					extrapolatedsubfxsupj(i, j) = pfx
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					fx(i, j) = (extrapolatedsubfxsupi(i, j) + extrapolatedsubfxsupj(i, j)) / 2.0d0
					else
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .eq. 0.0d0)) then
					fx(i, j) = extrapolatedsubfxsupi(i, j)
					extrapolatedsubfxsupj(i, j) = fx(i, j)
					else
					if ((d_i_fx(i, j) .eq. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					fx(i, j) = extrapolatedsubfxsupj(i, j)
					extrapolatedsubfxsupi(i, j) = fx(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fy(i, j) .ne. 0 .or. d_j_fy(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fy(i, j)), int(j - d_j_fy(i, j))) .gt. 0) then
					extrapolatedsubfysupi(i, j) = pfy
					extrapolatedsubfysupj(i, j) = pfy
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					fy(i, j) = (extrapolatedsubfysupi(i, j) + extrapolatedsubfysupj(i, j)) / 2.0d0
					else
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .eq. 0.0d0)) then
					fy(i, j) = extrapolatedsubfysupi(i, j)
					extrapolatedsubfysupj(i, j) = fy(i, j)
					else
					if ((d_i_fy(i, j) .eq. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					fy(i, j) = extrapolatedsubfysupj(i, j)
					extrapolatedsubfysupi(i, j) = fy(i, j)
					end if
					end if
					end if
				end if
			end if
		end if
		if ((FOV_xUpper(i, j) .gt. 0) .and. ((i - 1 .ge. 1 .and. (FOV_1(int(i - 1), j) .gt. 0)) .or. (i - 2 .ge. 1 .and. (FOV_1(int(i - 2), j) .gt. 0)) .or. (i - 3 .ge. 1 .and. (FOV_1(int(i - 3), j) .gt. 0)) .or. ((i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_rho, rho, extrapolatedsubrhosupi, i, d_j_rho, rho, extrapolatedsubrhosupj, j, rho, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_mx, mx, extrapolatedsubmxsupi, i, d_j_mx, mx, extrapolatedsubmxsupj, j, mx, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_my, my, extrapolatedsubmysupi, i, d_j_my, my, extrapolatedsubmysupj, j, my, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_p(i, j) .ne. 0 .or. d_j_p(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_p(i, j)), int(j - d_j_p(i, j))) .gt. 0) then
					extrapolatedsubpsupi(i, j) = (Paux + ((extrapolatedsubrhosupi(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					extrapolatedsubpsupj(i, j) = (Paux + ((extrapolatedsubrhosupj(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					p(i, j) = (extrapolatedsubpsupi(i, j) + extrapolatedsubpsupj(i, j)) / 2.0d0
					else
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .eq. 0.0d0)) then
					p(i, j) = extrapolatedsubpsupi(i, j)
					extrapolatedsubpsupj(i, j) = p(i, j)
					else
					if ((d_i_p(i, j) .eq. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					p(i, j) = extrapolatedsubpsupj(i, j)
					extrapolatedsubpsupi(i, j) = p(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fx(i, j) .ne. 0 .or. d_j_fx(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fx(i, j)), int(j - d_j_fx(i, j))) .gt. 0) then
					extrapolatedsubfxsupi(i, j) = pfx
					extrapolatedsubfxsupj(i, j) = pfx
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					fx(i, j) = (extrapolatedsubfxsupi(i, j) + extrapolatedsubfxsupj(i, j)) / 2.0d0
					else
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .eq. 0.0d0)) then
					fx(i, j) = extrapolatedsubfxsupi(i, j)
					extrapolatedsubfxsupj(i, j) = fx(i, j)
					else
					if ((d_i_fx(i, j) .eq. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					fx(i, j) = extrapolatedsubfxsupj(i, j)
					extrapolatedsubfxsupi(i, j) = fx(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fy(i, j) .ne. 0 .or. d_j_fy(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fy(i, j)), int(j - d_j_fy(i, j))) .gt. 0) then
					extrapolatedsubfysupi(i, j) = pfy
					extrapolatedsubfysupj(i, j) = pfy
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					fy(i, j) = (extrapolatedsubfysupi(i, j) + extrapolatedsubfysupj(i, j)) / 2.0d0
					else
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .eq. 0.0d0)) then
					fy(i, j) = extrapolatedsubfysupi(i, j)
					extrapolatedsubfysupj(i, j) = fy(i, j)
					else
					if ((d_i_fy(i, j) .eq. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					fy(i, j) = extrapolatedsubfysupj(i, j)
					extrapolatedsubfysupi(i, j) = fy(i, j)
					end if
					end if
					end if
				end if
			end if
		end if
		if ((FOV_yLower(i, j) .gt. 0) .and. ((j + 1 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 1)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 2)) .gt. 0)) .or. (j + 3 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 3)) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 1), int(j + 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 1), int(j + 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 2), int(j + 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 2), int(j + 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i + 3), int(j + 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j + 3 .le. cctk_lsh(2) .and. (FOV_1(int(i - 3), int(j + 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_rho, rho, extrapolatedsubrhosupi, i, d_j_rho, rho, extrapolatedsubrhosupj, j, rho, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_mx, mx, extrapolatedsubmxsupi, i, d_j_mx, mx, extrapolatedsubmxsupj, j, mx, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_my, my, extrapolatedsubmysupi, i, d_j_my, my, extrapolatedsubmysupj, j, my, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_p(i, j) .ne. 0 .or. d_j_p(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_p(i, j)), int(j - d_j_p(i, j))) .gt. 0) then
					extrapolatedsubpsupi(i, j) = (Paux + ((extrapolatedsubrhosupi(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					extrapolatedsubpsupj(i, j) = (Paux + ((extrapolatedsubrhosupj(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					p(i, j) = (extrapolatedsubpsupi(i, j) + extrapolatedsubpsupj(i, j)) / 2.0d0
					else
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .eq. 0.0d0)) then
					p(i, j) = extrapolatedsubpsupi(i, j)
					extrapolatedsubpsupj(i, j) = p(i, j)
					else
					if ((d_i_p(i, j) .eq. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					p(i, j) = extrapolatedsubpsupj(i, j)
					extrapolatedsubpsupi(i, j) = p(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fx(i, j) .ne. 0 .or. d_j_fx(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fx(i, j)), int(j - d_j_fx(i, j))) .gt. 0) then
					extrapolatedsubfxsupi(i, j) = pfx
					extrapolatedsubfxsupj(i, j) = pfx
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					fx(i, j) = (extrapolatedsubfxsupi(i, j) + extrapolatedsubfxsupj(i, j)) / 2.0d0
					else
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .eq. 0.0d0)) then
					fx(i, j) = extrapolatedsubfxsupi(i, j)
					extrapolatedsubfxsupj(i, j) = fx(i, j)
					else
					if ((d_i_fx(i, j) .eq. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					fx(i, j) = extrapolatedsubfxsupj(i, j)
					extrapolatedsubfxsupi(i, j) = fx(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fy(i, j) .ne. 0 .or. d_j_fy(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fy(i, j)), int(j - d_j_fy(i, j))) .gt. 0) then
					extrapolatedsubfysupi(i, j) = pfy
					extrapolatedsubfysupj(i, j) = pfy
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					fy(i, j) = (extrapolatedsubfysupi(i, j) + extrapolatedsubfysupj(i, j)) / 2.0d0
					else
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .eq. 0.0d0)) then
					fy(i, j) = extrapolatedsubfysupi(i, j)
					extrapolatedsubfysupj(i, j) = fy(i, j)
					else
					if ((d_i_fy(i, j) .eq. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					fy(i, j) = extrapolatedsubfysupj(i, j)
					extrapolatedsubfysupi(i, j) = fy(i, j)
					end if
					end if
					end if
				end if
			end if
		end if
		if ((FOV_yUpper(i, j) .gt. 0) .and. ((j - 1 .ge. 1 .and. (FOV_1(i, int(j - 1)) .gt. 0)) .or. (j - 2 .ge. 1 .and. (FOV_1(i, int(j - 2)) .gt. 0)) .or. (j - 3 .ge. 1 .and. (FOV_1(i, int(j - 3)) .gt. 0)) .or. ((i + 1 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 1)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 1)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 1)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 1)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 1 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 1)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 1 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 1)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 2)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 2)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 2)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 2)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 2 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 2)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 2 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 2)) .gt. 0)) .or. (i + 1 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 1), int(j - 3)) .gt. 0)) .or. (i - 1 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 1), int(j - 3)) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 2), int(j - 3)) .gt. 0)) .or. (i - 2 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 2), int(j - 3)) .gt. 0)) .or. (i + 3 .le. cctk_lsh(1) .and. j - 3 .ge. 1 .and. (FOV_1(int(i + 3), int(j - 3)) .gt. 0)) .or. (i - 3 .ge. 1 .and. j - 3 .ge. 1 .and. (FOV_1(int(i - 3), int(j - 3)) .gt. 0))))) then
!			Segment field extrapolations
			if ((d_i_rho(i, j) .ne. 0 .or. d_j_rho(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_rho, rho, extrapolatedsubrhosupi, i, d_j_rho, rho, extrapolatedsubrhosupj, j, rho, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_mx(i, j) .ne. 0 .or. d_j_mx(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_mx, mx, extrapolatedsubmxsupi, i, d_j_mx, mx, extrapolatedsubmxsupj, j, mx, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_my(i, j) .ne. 0 .or. d_j_my(i, j) .ne. 0)) then
				call extrapolate_field(CCTK_PASS_FTOF, d_i_my, my, extrapolatedsubmysupi, i, d_j_my, my, extrapolatedsubmysupj, j, my, FOV_1)
			end if
!			Segment field extrapolations
			if ((d_i_p(i, j) .ne. 0 .or. d_j_p(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_p(i, j)), int(j - d_j_p(i, j))) .gt. 0) then
					extrapolatedsubpsupi(i, j) = (Paux + ((extrapolatedsubrhosupi(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					extrapolatedsubpsupj(i, j) = (Paux + ((extrapolatedsubrhosupj(i, j) / rho0) ** gamma) * (rho0 * (c0 ** 2.0d0)) / gamma) - (rho0 * (c0 ** 2.0d0)) / gamma
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					p(i, j) = (extrapolatedsubpsupi(i, j) + extrapolatedsubpsupj(i, j)) / 2.0d0
					else
					if ((d_i_p(i, j) .ne. 0.0d0) .and. (d_j_p(i, j) .eq. 0.0d0)) then
					p(i, j) = extrapolatedsubpsupi(i, j)
					extrapolatedsubpsupj(i, j) = p(i, j)
					else
					if ((d_i_p(i, j) .eq. 0.0d0) .and. (d_j_p(i, j) .ne. 0.0d0)) then
					p(i, j) = extrapolatedsubpsupj(i, j)
					extrapolatedsubpsupi(i, j) = p(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fx(i, j) .ne. 0 .or. d_j_fx(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fx(i, j)), int(j - d_j_fx(i, j))) .gt. 0) then
					extrapolatedsubfxsupi(i, j) = pfx
					extrapolatedsubfxsupj(i, j) = pfx
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					fx(i, j) = (extrapolatedsubfxsupi(i, j) + extrapolatedsubfxsupj(i, j)) / 2.0d0
					else
					if ((d_i_fx(i, j) .ne. 0.0d0) .and. (d_j_fx(i, j) .eq. 0.0d0)) then
					fx(i, j) = extrapolatedsubfxsupi(i, j)
					extrapolatedsubfxsupj(i, j) = fx(i, j)
					else
					if ((d_i_fx(i, j) .eq. 0.0d0) .and. (d_j_fx(i, j) .ne. 0.0d0)) then
					fx(i, j) = extrapolatedsubfxsupj(i, j)
					extrapolatedsubfxsupi(i, j) = fx(i, j)
					end if
					end if
					end if
				end if
			end if
!			Segment field extrapolations
			if ((d_i_fy(i, j) .ne. 0 .or. d_j_fy(i, j) .ne. 0)) then
				if (FOV_1(int(i - d_i_fy(i, j)), int(j - d_j_fy(i, j))) .gt. 0) then
					extrapolatedsubfysupi(i, j) = pfy
					extrapolatedsubfysupj(i, j) = pfy
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					fy(i, j) = (extrapolatedsubfysupi(i, j) + extrapolatedsubfysupj(i, j)) / 2.0d0
					else
					if ((d_i_fy(i, j) .ne. 0.0d0) .and. (d_j_fy(i, j) .eq. 0.0d0)) then
					fy(i, j) = extrapolatedsubfysupi(i, j)
					extrapolatedsubfysupj(i, j) = fy(i, j)
					else
					if ((d_i_fy(i, j) .eq. 0.0d0) .and. (d_j_fy(i, j) .ne. 0.0d0)) then
					fy(i, j) = extrapolatedsubfysupj(i, j)
					extrapolatedsubfysupi(i, j) = fy(i, j)
					end if
					end if
					end if
				end if
			end if
		end if
			end do
		end do
		call CCTK_SyncGroup(status, cctkGH, "CompressibleCounterflow2D::fields_tl1")
		call CCTK_SyncGroup(status, cctkGH, "CompressibleCounterflow2D::fields_tl2")
		call CCTK_SyncGroup(status, cctkGH, "CompressibleCounterflow2D::block4")

	end subroutine CompressibleCounterflow2D_execution