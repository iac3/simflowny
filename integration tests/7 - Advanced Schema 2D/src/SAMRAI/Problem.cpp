#include "Problem.h"
#include "Functions.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <stack>
#include "hdf5.h"
#include <gsl/gsl_rng.h>
#include <random>
#include "SAMRAI/pdat/CellVariable.h"
#include "SAMRAI/pdat/NodeData.h"
#include "SAMRAI/pdat/NodeVariable.h"
#include "LagrangianPolynomicRefine.h"


#include "SAMRAI/tbox/Array.h"
#include "SAMRAI/hier/BoundaryBox.h"
#include "SAMRAI/hier/BoxContainer.h"
#include "SAMRAI/geom/CartesianPatchGeometry.h"
#include "SAMRAI/hier/VariableDatabase.h"
#include "SAMRAI/hier/PatchDataRestartManager.h"
#include "SAMRAI/hier/Index.h"
#include "SAMRAI/tbox/PIO.h"
#include "SAMRAI/tbox/Utilities.h"
#include "SAMRAI/tbox/Timer.h"
#include "SAMRAI/tbox/TimerManager.h"

#define MIN(X,Y) ((X) < (Y) ? (X) : (Y))
#define MAX(X,Y) ((X) > (Y) ? (X) : (Y))
#define SIGN(X) (((X) > 0) - ((X) < 0))
#define isEven(a) ((a) % 2 == 0 ? true : false)
#define greaterEq(a,b) ((fabs((a) - (b))/1.0E-15 > 10 ? false : (floor(fabs((a) - (b))/1.0E-15) < 1)) || (b)<(a))
#define lessEq(a,b) ((fabs((a) - (b))/1.0E-15 > 10 ? false: (floor(fabs((a) - (b))/1.0E-15) < 1)) || (a)<(b))
#define equalsEq(a,b) ((fabs((a) - (b))/1.0E-15 > 10 ? false: (floor(fabs((a) - (b))/1.0E-15) < 1)))
#define reducePrecision(x, p) (floor(((x) * pow(10, (p)) + 0.5)) / pow(10, (p)))

using namespace external;

const gsl_rng_type * T;
gsl_rng *r_var;

//Timers
std::shared_ptr<tbox::Timer> t_step;
std::shared_ptr<tbox::Timer> t_moveParticles;
std::shared_ptr<tbox::Timer> t_output;

inline int Problem::GetExpoBase2(double d)
{
	int i = 0;
	((short *)(&i))[0] = (((short *)(&d))[3] & (short)32752); // _123456789ab____ & 0111111111110000
	return (i >> 4) - 1023;
}

bool	Problem::Equals(double d1, double d2)
{
	if (d1 == d2)
		return true;
	int e1 = GetExpoBase2(d1);
	int e2 = GetExpoBase2(d2);
	int e3 = GetExpoBase2(d1 - d2);
	if ((e3 - e2 < -48) && (e3 - e1 < -48))
		return true;
	return false;
}

/*
 * Constructor of the problem.
 */
Problem::Problem(const string& object_name, const tbox::Dimension& dim, std::shared_ptr<tbox::Database>& database, std::shared_ptr<geom::CartesianGridGeometry >& grid_geom, std::shared_ptr<hier::PatchHierarchy >& patch_hierarchy, MainRestartData& mrd, const double dt, const bool init_from_restart, const int console_output, const int timer_output, const int mesh_output_period, const vector<string> full_mesh_writer_variables, std::shared_ptr<appu::VisItDataWriter>& mesh_data_writer, const vector<int> integration_output_period, const vector<set<string> > integralVariables, vector<std::shared_ptr<IntegrateDataWriter > > integrateDataWriters, const vector<int> point_output_period, const vector<set<string> > pointVariables, vector<std::shared_ptr<PointDataWriter > > pointDataWriters): 
d_dim(dim), xfer::RefinePatchStrategy(), xfer::CoarsenPatchStrategy(), viz_mesh_dump_interval(mesh_output_period), d_full_mesh_writer_variables(full_mesh_writer_variables.begin(), full_mesh_writer_variables.end()), d_visit_data_writer(mesh_data_writer), d_output_interval(console_output), d_timer_output_interval(timer_output), d_integrateDataWriters(integrateDataWriters.begin(), integrateDataWriters.end()), d_integration_output_period(integration_output_period.begin(), integration_output_period.end()), d_pointDataWriters(pointDataWriters.begin(), pointDataWriters.end()), d_point_output_period(point_output_period.begin(), point_output_period.end())
{
	//Setup the timers
	t_step = tbox::TimerManager::getManager()->getTimer("Step");
	t_moveParticles = tbox::TimerManager::getManager()->getTimer("Move particles");
	t_output = tbox::TimerManager::getManager()->getTimer("OutputGeneration");

	//Output configuration
	next_console_output = d_output_interval;
	next_timer_output = d_timer_output_interval;

	//Get the object name, the grid geometry and the patch hierarchy
  	d_grid_geometry = grid_geom;
	d_patch_hierarchy = patch_hierarchy;
	d_object_name = object_name;
	d_init_from_restart = init_from_restart;
	initial_dt = dt;


	for (vector<set<string> >::const_iterator it = integralVariables.begin(); it != integralVariables.end(); ++it) {
		set<string> vars = *it;
		for (set<string>::const_iterator it2 = vars.begin() ; it2 != vars.end(); ++it2) {
			d_integralVariables.push_back(vars);
		}
	}
	for (vector<set<string> >::const_iterator it = pointVariables.begin(); it != pointVariables.end(); ++it) {
		set<string> vars = *it;
		for (set<string>::const_iterator it2 = vars.begin() ; it2 != vars.end(); ++it2) {
			d_pointVariables.push_back(vars);
		}
	}


	//Get parameters
    cout<<"Reading parameters"<<endl;
	epsilon = database->getDouble("epsilon");
	tend = database->getDouble("tend");
	epsilon_domain = database->getDouble("epsilon_domain");
	gamma = database->getDouble("gamma");
	//Random initialization
	gsl_rng_env_setup();
	//Random for simulation
	const tbox::SAMRAI_MPI& mpi(tbox::SAMRAI_MPI::getSAMRAIWorld());
	int random_seed = database->getDouble("random_seed")*(mpi.getRank() + 1);
	r_var = gsl_rng_alloc(gsl_rng_ranlxs0);
	gsl_rng_set(r_var, random_seed);
	for (int il = 0; il < d_patch_hierarchy->getMaxNumberOfLevels(); il++) {
		bo_substep_iteration.push_back(0);
	}
	//Initialization from input file
	if (!d_init_from_restart) {
		next_mesh_dump_iteration = viz_mesh_dump_interval;
		for (std::vector<int>::iterator it = d_integration_output_period.begin(); it != d_integration_output_period.end(); ++it) {
			next_integration_dump_iteration.push_back((*it));
		}
		for (std::vector<int>::iterator it = d_point_output_period.begin(); it != d_point_output_period.end(); ++it) {
			next_point_dump_iteration.push_back((*it));
		}

		//Iteration counter
		for (int il = 0; il < d_patch_hierarchy->getMaxNumberOfLevels(); il++) {
			current_iteration.push_back(0);
		}
	}
	//Initialization from restart file
	else {
		getFromRestart(mrd);
		if (d_integration_output_period.size() < next_integration_dump_iteration.size()) {
			TBOX_ERROR("Number of integrations cannot be reduced after a checkpoint.");
		}
		for (int il = 0; il < d_integration_output_period.size(); il++) {
			if (il >= next_integration_dump_iteration.size()) {
				next_integration_dump_iteration.push_back(0);
			}
			if (next_integration_dump_iteration[il] == 0 && d_integration_output_period[il] > 0) {
				next_integration_dump_iteration[il] = current_iteration[d_patch_hierarchy->getNumberOfLevels() - 1] + d_integration_output_period[il];
			}
		}
		if (d_point_output_period.size() < next_point_dump_iteration.size()) {
			TBOX_ERROR("Number of point output cannot be reduced after a checkpoint.");
		}
		for (int il = 0; il < d_point_output_period.size(); il++) {
			if (il >= next_point_dump_iteration.size()) {
				next_point_dump_iteration.push_back(0);
			}
			if (next_point_dump_iteration[il] == 0 && d_point_output_period[il] > 0) {
				next_point_dump_iteration[il] = current_iteration[d_patch_hierarchy->getNumberOfLevels() - 1] + d_point_output_period[il];
			}
		}
	}


	//External eos parameters
#ifdef EXTERNAL_EOS

	std::shared_ptr<tbox::Database> external_eos_db = database->getDatabase("external_EOS");
	Commons::ExternalEos::reprimand_eos_type = external_eos_db->getInteger("eos_type");
	Commons::ExternalEos::reprimand_atmo_Ye = external_eos_db->getDouble("atmo_Ye");
	Commons::ExternalEos::reprimand_max_z = external_eos_db->getDouble("max_z");
	Commons::ExternalEos::reprimand_max_b = external_eos_db->getDouble("max_b");
	Commons::ExternalEos::reprimand_c2p_acc = external_eos_db->getDouble("c2p_acc");
	Commons::ExternalEos::reprimand_atmo_rho = external_eos_db->getDouble("atmo_rho");
	Commons::ExternalEos::reprimand_rho_strict = external_eos_db->getDouble("rho_strict");
	Commons::ExternalEos::reprimand_max_rho = external_eos_db->getDouble("max_rho");
	Commons::ExternalEos::reprimand_max_eps = external_eos_db->getDouble("max_eps");
	Commons::ExternalEos::reprimand_gamma_th = external_eos_db->getDouble("gamma_th");
#endif

    	//Subcycling
	d_refinedTimeStepping = false;
	if (database->isString("subcycling")) {
		if (database->getString("subcycling") == "BERGER-OLIGER") {
			d_refinedTimeStepping = true;
		}
	}


	//Regridding options
	d_regridding = false;
	if (database->isDatabase("regridding")) {
		regridding_db = database->getDatabase("regridding");
		d_regridding_buffer = regridding_db->getDouble("regridding_buffer");
		int smallest_patch_size = d_patch_hierarchy->getSmallestPatchSize(0).min();
		for (int il = 1; il < d_patch_hierarchy->getMaxNumberOfLevels(); il++) {
			smallest_patch_size = MIN(smallest_patch_size, d_patch_hierarchy->getSmallestPatchSize(il).min());
		}
		if (d_regridding_buffer > smallest_patch_size) {
			TBOX_ERROR("Error: Regridding_buffer parameter ("<<d_regridding_buffer<<") cannot be greater than smallest_patch_size minimum value("<<smallest_patch_size<<")");
		}
		if (regridding_db->isString("regridding_type")) {
			d_regridding_type = regridding_db->getString("regridding_type");
			d_regridding_min_level = regridding_db->getInteger("regridding_min_level");
			d_regridding_max_level = regridding_db->getInteger("regridding_max_level");
			if (d_regridding_type == "GRADIENT") {
				d_regridding_field = regridding_db->getString("regridding_field");
				d_regridding_compressionFactor = regridding_db->getDouble("regridding_compressionFactor");
				d_regridding_mOffset = regridding_db->getDouble("regridding_mOffset");
				d_regridding = true;
			} else {
				if (d_regridding_type == "FUNCTION") {
					d_regridding_field = regridding_db->getString("regridding_function_field");
					d_regridding_threshold = regridding_db->getDouble("regridding_threshold");
					d_regridding = true;
				} else {
					if (d_regridding_type == "SHADOW") {
						std::string* fields = new std::string[2];
						regridding_db->getStringArray("regridding_fields", fields, 2);
						d_regridding_field = fields[0];
						d_regridding_field_shadow = fields[1];
						d_regridding_error = regridding_db->getDouble("regridding_error");
						d_regridding = true;
						delete[] fields;
					}
				}
			}
		}
	}

	//Stencil of the discretization method
	int maxratio = 1;
	for (int il = 1; il < d_patch_hierarchy->getMaxNumberOfLevels(); il++) {
		const hier::IntVector ratio = d_patch_hierarchy->getRatioToCoarserLevel(il);
		maxratio = MAX(maxratio, ratio.max());
	}
	//Minimum region thickness
	d_regionMinThickness = 4;
	d_ghost_width = 4;

	
	//Register Fields and temporal fields into the variable database
	hier::VariableDatabase *vdb = hier::VariableDatabase::getDatabase();
  	std::shared_ptr<hier::VariableContext> d_cont_curr(vdb->getContext("Current"));
	std::shared_ptr< pdat::CellVariable<int> > mask(std::shared_ptr< pdat::CellVariable<int> >(new pdat::CellVariable<int>(d_dim, "samrai_mask",1)));
	d_mask_id = vdb->registerVariableAndContext(mask ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	IntegrateDataWriter::setMaskVariable(d_mask_id);
	std::shared_ptr< pdat::NodeVariable<double> > interior(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "regridding_value",1)));
	d_interior_regridding_value_id = vdb->registerVariableAndContext(interior ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<int> > nonSync(std::shared_ptr< pdat::NodeVariable<int> >(new pdat::NodeVariable<int>(d_dim, "regridding_tag",1)));
	d_nonSync_regridding_tag_id = vdb->registerVariableAndContext(nonSync ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<double> > interior_i(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "interior_i",1)));
	d_interior_i_id = vdb->registerVariableAndContext(interior_i ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<double> > interior_j(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "interior_j",1)));
	d_interior_j_id = vdb->registerVariableAndContext(interior_j ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<double> > FOV_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FOV_1",1)));
	d_FOV_1_id = vdb->registerVariableAndContext(FOV_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FOV_1_id);
	std::shared_ptr< pdat::NodeVariable<double> > FOV_xLower(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FOV_xLower",1)));
	d_FOV_xLower_id = vdb->registerVariableAndContext(FOV_xLower ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FOV_xLower_id);
	std::shared_ptr< pdat::NodeVariable<double> > FOV_xUpper(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FOV_xUpper",1)));
	d_FOV_xUpper_id = vdb->registerVariableAndContext(FOV_xUpper ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FOV_xUpper_id);
	std::shared_ptr< pdat::NodeVariable<double> > FOV_yLower(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FOV_yLower",1)));
	d_FOV_yLower_id = vdb->registerVariableAndContext(FOV_yLower ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FOV_yLower_id);
	std::shared_ptr< pdat::NodeVariable<double> > FOV_yUpper(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FOV_yUpper",1)));
	d_FOV_yUpper_id = vdb->registerVariableAndContext(FOV_yUpper ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FOV_yUpper_id);
	std::shared_ptr< pdat::NodeVariable<double> > rho(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "rho",1)));
	d_rho_id = vdb->registerVariableAndContext(rho ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_rho_id);
	std::shared_ptr< pdat::NodeVariable<double> > mx(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "mx",1)));
	d_mx_id = vdb->registerVariableAndContext(mx ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_mx_id);
	std::shared_ptr< pdat::NodeVariable<double> > my(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "my",1)));
	d_my_id = vdb->registerVariableAndContext(my ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_my_id);
	std::shared_ptr< pdat::NodeVariable<double> > e(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "e",1)));
	d_e_id = vdb->registerVariableAndContext(e ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_e_id);
	std::shared_ptr< pdat::NodeVariable<double> > P(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "P",1)));
	d_P_id = vdb->registerVariableAndContext(P ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_P_id);
	std::shared_ptr< pdat::NodeVariable<double> > rk1rho(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "rk1rho",1)));
	d_rk1rho_id = vdb->registerVariableAndContext(rk1rho ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<double> > rk1mx(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "rk1mx",1)));
	d_rk1mx_id = vdb->registerVariableAndContext(rk1mx ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<double> > rk1my(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "rk1my",1)));
	d_rk1my_id = vdb->registerVariableAndContext(rk1my ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<double> > rk1e(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "rk1e",1)));
	d_rk1e_id = vdb->registerVariableAndContext(rk1e ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<double> > rk2rho(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "rk2rho",1)));
	d_rk2rho_id = vdb->registerVariableAndContext(rk2rho ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<double> > rk2mx(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "rk2mx",1)));
	d_rk2mx_id = vdb->registerVariableAndContext(rk2mx ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<double> > rk2my(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "rk2my",1)));
	d_rk2my_id = vdb->registerVariableAndContext(rk2my ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<double> > rk2e(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "rk2e",1)));
	d_rk2e_id = vdb->registerVariableAndContext(rk2e ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<double> > rho_p(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "rho_p",1)));
	d_rho_p_id = vdb->registerVariableAndContext(rho_p ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<double> > mx_p(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "mx_p",1)));
	d_mx_p_id = vdb->registerVariableAndContext(mx_p ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<double> > my_p(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "my_p",1)));
	d_my_p_id = vdb->registerVariableAndContext(my_p ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<double> > e_p(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "e_p",1)));
	d_e_p_id = vdb->registerVariableAndContext(e_p ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));


	//Refine and coarse algorithms

	d_bdry_fill_init = std::shared_ptr< xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());
	d_bdry_post_coarsen = std::shared_ptr< xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());
	d_bdry_fill_advance2 = std::shared_ptr< xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());
	d_bdry_fill_advance8 = std::shared_ptr< xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());
	d_bdry_fill_advance14 = std::shared_ptr< xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());
	d_coarsen_algorithm = std::shared_ptr< xfer::CoarsenAlgorithm >(new xfer::CoarsenAlgorithm(d_dim));

	d_mapping_fill = std::shared_ptr< xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());
    d_tagging_fill = std::shared_ptr< xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());
	d_fill_new_level    = std::shared_ptr< xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());
	d_fill_new_level_aux    = std::shared_ptr< xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());


	//mapping communication

	std::shared_ptr< hier::RefineOperator > refine_operator_map = d_grid_geometry->lookupRefineOperator(interior, "LINEAR_REFINE");
	d_mapping_fill->registerRefine(d_interior_regridding_value_id,d_interior_regridding_value_id,d_interior_regridding_value_id, refine_operator_map);
	d_mapping_fill->registerRefine(d_interior_i_id,d_interior_i_id,d_interior_i_id, refine_operator_map);
	d_mapping_fill->registerRefine(d_interior_j_id,d_interior_j_id,d_interior_j_id, refine_operator_map);
	d_mapping_fill->registerRefine(d_FOV_1_id,d_FOV_1_id,d_FOV_1_id, refine_operator_map);
	d_mapping_fill->registerRefine(d_FOV_xLower_id,d_FOV_xLower_id,d_FOV_xLower_id, refine_operator_map);
	d_mapping_fill->registerRefine(d_FOV_xUpper_id,d_FOV_xUpper_id,d_FOV_xUpper_id, refine_operator_map);
	d_mapping_fill->registerRefine(d_FOV_yLower_id,d_FOV_yLower_id,d_FOV_yLower_id, refine_operator_map);
	d_mapping_fill->registerRefine(d_FOV_yUpper_id,d_FOV_yUpper_id,d_FOV_yUpper_id, refine_operator_map);


    d_tagging_fill->registerRefine(d_nonSync_regridding_tag_id,d_nonSync_regridding_tag_id,d_nonSync_regridding_tag_id, d_grid_geometry->lookupRefineOperator(nonSync, "NO_REFINE"));

	//refine and coarsen operators
	string refine_op_name = "LINEAR_REFINE";
	int order = 0;
	if (database->isDatabase("regridding")) {
		regridding_db = database->getDatabase("regridding");
		if (regridding_db->isString("interpolator")) {
			refine_op_name = regridding_db->getString("interpolator");
			if (refine_op_name == "LINEAR_REFINE") {
				order = 1;
			}
			if (refine_op_name == "CUBIC_REFINE") {
				order = 3;
			}
			if (refine_op_name == "QUINTIC_REFINE") {
				order = 5;
			}
		}
	}
	std::shared_ptr< hier::RefineOperator > refine_operator, refine_operator_bound;
	std::shared_ptr< hier::CoarsenOperator > coarsen_operator = d_grid_geometry->lookupCoarsenOperator(FOV_1, "CONSTANT_COARSEN");
	if (order > 0) {
		std::shared_ptr< hier::RefineOperator > tmp_refine_operator(new LagrangianPolynomicRefine(false, order, d_patch_hierarchy, d_dim));
		refine_operator = tmp_refine_operator;
		std::shared_ptr< hier::RefineOperator > tmp_refine_operator_bound(new LagrangianPolynomicRefine(true, order, d_patch_hierarchy, d_dim));
		refine_operator_bound = tmp_refine_operator_bound;
	} else {
		refine_operator = d_grid_geometry->lookupRefineOperator(FOV_1, refine_op_name);
		refine_operator_bound = d_grid_geometry->lookupRefineOperator(FOV_1, refine_op_name);
	}

	std::shared_ptr<SAMRAI::hier::TimeInterpolateOperator> tio_mesh1(new TimeInterpolator(d_grid_geometry, "mesh"));
	time_interpolate_operator_mesh1 = std::dynamic_pointer_cast<TimeInterpolator>(tio_mesh1);


	//Register variables to the refineAlgorithm for boundaries

	if (d_refinedTimeStepping) {
		d_bdry_fill_advance2->registerRefine(d_rk1e_id,d_rk1e_id,d_rk1e_id,refine_operator);
		d_bdry_fill_advance2->registerRefine(d_rk1my_id,d_rk1my_id,d_rk1my_id,refine_operator);
		d_bdry_fill_advance2->registerRefine(d_rk1mx_id,d_rk1mx_id,d_rk1mx_id,refine_operator);
		d_bdry_fill_advance2->registerRefine(d_rk1rho_id,d_rk1rho_id,d_rk1rho_id,refine_operator);
	} else {
		d_bdry_fill_advance2->registerRefine(d_rk1e_id,d_rk1e_id,d_rk1e_id,refine_operator);
		d_bdry_fill_advance2->registerRefine(d_rk1my_id,d_rk1my_id,d_rk1my_id,refine_operator);
		d_bdry_fill_advance2->registerRefine(d_rk1mx_id,d_rk1mx_id,d_rk1mx_id,refine_operator);
		d_bdry_fill_advance2->registerRefine(d_rk1rho_id,d_rk1rho_id,d_rk1rho_id,refine_operator);
	}
	if (d_refinedTimeStepping) {
		d_bdry_fill_advance8->registerRefine(d_rk2e_id,d_rk2e_id,d_rk2e_id,refine_operator);
		d_bdry_fill_advance8->registerRefine(d_rk2my_id,d_rk2my_id,d_rk2my_id,refine_operator);
		d_bdry_fill_advance8->registerRefine(d_rk2mx_id,d_rk2mx_id,d_rk2mx_id,refine_operator);
		d_bdry_fill_advance8->registerRefine(d_rk2rho_id,d_rk2rho_id,d_rk2rho_id,refine_operator);
	} else {
		d_bdry_fill_advance8->registerRefine(d_rk2e_id,d_rk2e_id,d_rk2e_id,refine_operator);
		d_bdry_fill_advance8->registerRefine(d_rk2my_id,d_rk2my_id,d_rk2my_id,refine_operator);
		d_bdry_fill_advance8->registerRefine(d_rk2mx_id,d_rk2mx_id,d_rk2mx_id,refine_operator);
		d_bdry_fill_advance8->registerRefine(d_rk2rho_id,d_rk2rho_id,d_rk2rho_id,refine_operator);
	}
	if (d_refinedTimeStepping) {
		d_bdry_fill_advance14->registerRefine(d_e_id,d_e_id,d_e_p_id,d_e_id,d_e_id,refine_operator,tio_mesh1);
		d_bdry_fill_advance14->registerRefine(d_my_id,d_my_id,d_my_p_id,d_my_id,d_my_id,refine_operator,tio_mesh1);
		d_bdry_fill_advance14->registerRefine(d_mx_id,d_mx_id,d_mx_p_id,d_mx_id,d_mx_id,refine_operator,tio_mesh1);
		d_bdry_fill_advance14->registerRefine(d_rho_id,d_rho_id,d_rho_p_id,d_rho_id,d_rho_id,refine_operator,tio_mesh1);
	} else {
		d_bdry_fill_advance14->registerRefine(d_e_id,d_e_id,d_e_id,refine_operator);
		d_bdry_fill_advance14->registerRefine(d_my_id,d_my_id,d_my_id,refine_operator);
		d_bdry_fill_advance14->registerRefine(d_mx_id,d_mx_id,d_mx_id,refine_operator);
		d_bdry_fill_advance14->registerRefine(d_rho_id,d_rho_id,d_rho_id,refine_operator);
	}
	d_bdry_fill_init->registerRefine(d_rho_id,d_rho_id,d_rho_id,refine_operator);
	d_bdry_post_coarsen->registerRefine(d_rho_id,d_rho_id,d_rho_id,refine_operator);
	d_bdry_fill_init->registerRefine(d_mx_id,d_mx_id,d_mx_id,refine_operator);
	d_bdry_post_coarsen->registerRefine(d_mx_id,d_mx_id,d_mx_id,refine_operator);
	d_bdry_fill_init->registerRefine(d_my_id,d_my_id,d_my_id,refine_operator);
	d_bdry_post_coarsen->registerRefine(d_my_id,d_my_id,d_my_id,refine_operator);
	d_bdry_fill_init->registerRefine(d_e_id,d_e_id,d_e_id,refine_operator);
	d_bdry_post_coarsen->registerRefine(d_e_id,d_e_id,d_e_id,refine_operator);
	d_bdry_fill_init->registerRefine(d_P_id,d_P_id,d_P_id,refine_operator);
	d_bdry_post_coarsen->registerRefine(d_P_id,d_P_id,d_P_id,refine_operator);


	//Register variables to the refineAlgorithm for filling new levels on regridding
	d_fill_new_level->registerRefine(d_rho_id,d_rho_id,d_rho_id,refine_operator_bound);
	d_fill_new_level->registerRefine(d_mx_id,d_mx_id,d_mx_id,refine_operator_bound);
	d_fill_new_level->registerRefine(d_my_id,d_my_id,d_my_id,refine_operator_bound);
	d_fill_new_level->registerRefine(d_e_id,d_e_id,d_e_id,refine_operator_bound);
	d_fill_new_level_aux->registerRefine(d_P_id,d_P_id,d_P_id,refine_operator_bound);
	d_fill_new_level->registerRefine(d_P_id,d_P_id,d_P_id,refine_operator_bound);


	//Register variables to the coarsenAlgorithm
	d_coarsen_algorithm->registerCoarsen(d_rho_id,d_rho_id,coarsen_operator);
	d_coarsen_algorithm->registerCoarsen(d_mx_id,d_mx_id,coarsen_operator);
	d_coarsen_algorithm->registerCoarsen(d_my_id,d_my_id,coarsen_operator);
	d_coarsen_algorithm->registerCoarsen(d_e_id,d_e_id,coarsen_operator);
	d_coarsen_algorithm->registerCoarsen(d_P_id,d_P_id,coarsen_operator);



    Commons::initialization();
}

/*
 * Destructor.
 */
Problem::~Problem() 
{
} 

/*
 * Initialize the data from a given level.
 */
void Problem::initializeLevelData (
   const std::shared_ptr<hier::PatchHierarchy >& hierarchy , 
   const int level_number ,
   const double init_data_time ,
   const bool can_be_refined ,
   const bool initial_time ,
   const std::shared_ptr<hier::PatchLevel >& old_level ,
   const bool allocate_data )
{
    cout<<"Initializing level "<<level_number<<endl;
    tbox::MemoryUtilities::printMemoryInfo(cout);   
	std::shared_ptr< hier::PatchLevel > level(hierarchy->getPatchLevel(level_number));

	// Allocate storage needed to initialize level and fill data from coarser levels in AMR hierarchy.  
	level->allocatePatchData(d_interior_regridding_value_id, init_data_time);
	level->allocatePatchData(d_nonSync_regridding_tag_id, init_data_time);
	level->allocatePatchData(d_interior_i_id, init_data_time);
	level->allocatePatchData(d_interior_j_id, init_data_time);
	level->allocatePatchData(d_FOV_1_id, init_data_time);
	level->allocatePatchData(d_FOV_xLower_id, init_data_time);
	level->allocatePatchData(d_FOV_xUpper_id, init_data_time);
	level->allocatePatchData(d_FOV_yLower_id, init_data_time);
	level->allocatePatchData(d_FOV_yUpper_id, init_data_time);
	level->allocatePatchData(d_rho_id, init_data_time);
	level->allocatePatchData(d_mx_id, init_data_time);
	level->allocatePatchData(d_my_id, init_data_time);
	level->allocatePatchData(d_e_id, init_data_time);
	level->allocatePatchData(d_P_id, init_data_time);
	level->allocatePatchData(d_rk1rho_id, init_data_time);
	level->allocatePatchData(d_rk1mx_id, init_data_time);
	level->allocatePatchData(d_rk1my_id, init_data_time);
	level->allocatePatchData(d_rk1e_id, init_data_time);
	level->allocatePatchData(d_rk2rho_id, init_data_time);
	level->allocatePatchData(d_rk2mx_id, init_data_time);
	level->allocatePatchData(d_rk2my_id, init_data_time);
	level->allocatePatchData(d_rk2e_id, init_data_time);
	level->allocatePatchData(d_rho_p_id, init_data_time);
	level->allocatePatchData(d_mx_p_id, init_data_time);
	level->allocatePatchData(d_my_p_id, init_data_time);
	level->allocatePatchData(d_e_p_id, init_data_time);
	level->allocatePatchData(d_mask_id, init_data_time);


	//Mapping the current data for new level.
	if (initial_time || level_number == 0) {
		mapDataOnPatch(init_data_time, initial_time, level_number, level);
	}

	//Fill a finer level with the data of the next coarse level.
	if ((level_number > 0) || old_level) {
		d_mapping_fill->createSchedule(level,old_level,level_number-1,hierarchy,this)->fillData(init_data_time, false);
		correctFOVS(level);
	}

	//Fill a finer level with the data of the next coarse level.
	if (!initial_time && ((level_number > 0) || old_level)) {
		d_fill_new_level->createSchedule(level,old_level,level_number-1,hierarchy,this)->fillData(init_data_time, false);
		postNewLevel(level);
		d_fill_new_level_aux->createSchedule(level,this)->fillData(init_data_time, false);
	}

	//Interphase mapping
	if (initial_time || level_number == 0) {
		interphaseMapping(init_data_time, initial_time, level_number, level, 1);
	}


	//Initialize current data for new level.
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr< hier::Patch > patch = *p_it;
		if (initial_time) {
  		    initializeDataOnPatch(*patch, init_data_time, initial_time);
		}

	}
	//Post-initialization Sync.
    	if (initial_time || level_number == 0) {

		//First synchronization from initialization
		d_bdry_fill_init->createSchedule(level,this)->fillData(init_data_time, false);
		double current_time = init_data_time;
		const double level_ratio = level->getRatioToCoarserLevel().max();
		double simPlat_dt = 0;
		for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
			const std::shared_ptr< hier::Patch >& patch = *p_it;
			double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
			double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
			double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
			double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
			double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
			double* P = ((pdat::NodeData<double> *) patch->getPatchData(d_P_id).get())->getPointer();
			double* e = ((pdat::NodeData<double> *) patch->getPatchData(d_e_id).get())->getPointer();
			double* rho = ((pdat::NodeData<double> *) patch->getPatchData(d_rho_id).get())->getPointer();
			double* mx = ((pdat::NodeData<double> *) patch->getPatchData(d_mx_id).get())->getPointer();
			double* my = ((pdat::NodeData<double> *) patch->getPatchData(d_my_id).get())->getPointer();

			//Get the dimensions of the patch
			const hier::Index boxfirst = patch->getBox().lower();
			const hier::Index boxlast = patch->getBox().upper();

			//Get delta spaces into an array. dx, dy, dz.
			const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
			const double* dx  = patch_geom->getDx();
			int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
			int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
			for (int j = 0; j < jlast; j++) {
				for (int i = 0; i < ilast; i++) {
					if (((i + 4 < ilast || !patch->getPatchGeometry()->getTouchesRegularBoundary(0, 1)) && (i - 4 >= 0 || !patch->getPatchGeometry()->getTouchesRegularBoundary(0, 0)) && (j + 4 < jlast || !patch->getPatchGeometry()->getTouchesRegularBoundary(1, 1)) && (j - 4 >= 0 || !patch->getPatchGeometry()->getTouchesRegularBoundary(1, 0)))) {
						vector(P, i, j) = (gamma - 1.0) * (vector(e, i, j) - vector(rho, i, j) * (((vector(mx, i, j) / vector(rho, i, j)) * (vector(mx, i, j) / vector(rho, i, j))) / 2.0 + ((vector(my, i, j) / vector(rho, i, j)) * (vector(my, i, j) / vector(rho, i, j))) / 2.0));
					}
				}
			}
		}
		//Last synchronization from initialization
		d_bdry_fill_init->createSchedule(level,this)->fillData(init_data_time, false);

    	}

    cout<<"Level "<<level_number<<" initialized"<<endl;
    tbox::MemoryUtilities::printMemoryInfo(cout);
}



void Problem::initializeLevelIntegrator(
   const std::shared_ptr<mesh::GriddingAlgorithmStrategy>& gridding_alg)
{
}

double Problem::getLevelDt(
   const std::shared_ptr<hier::PatchLevel>& level,
   const double dt_time,
   const bool initial_time)
{
  
   TBOX_ASSERT(level);

   if (level->getLevelNumber() == 0) return initial_dt;

    double dt = initial_dt;
    const hier::IntVector ratio = level->getRatioToLevelZero();
    double local_dt = dt;
    for (int i = 0; i < 2; i++) {
        if (local_dt > dt / ratio[i]) {
            local_dt = dt / ratio[i];
        }
    }
    return local_dt;
}

double Problem::getMaxFinerLevelDt(
   const int finer_level_number,
   const double coarse_dt,
   const hier::IntVector& ratio)
{
   NULL_USE(finer_level_number);

   TBOX_ASSERT(ratio.min() > 0);

   return coarse_dt / double(ratio.max());
}

void Problem::standardLevelSynchronization(
   const std::shared_ptr<hier::PatchHierarchy>& hierarchy,
   const int coarsest_level,
   const int finest_level,
   const double sync_time,
   const std::vector<double>& old_times)
{

}

void Problem::synchronizeNewLevels(
   const std::shared_ptr<hier::PatchHierarchy>& hierarchy,
   const int coarsest_level,
   const int finest_level,
   const double sync_time,
   const bool initial_time)
{

    //Not needed, but not absolutely sure
    /*for (int fine_ln = finest_level; fine_ln > coarsest_level; --fine_ln) {
        const int coarse_ln = fine_ln - 1;
        std::shared_ptr<hier::PatchLevel> fine_level(hierarchy->getPatchLevel(fine_ln));
        d_bdry_fill_init->createSchedule(fine_level, coarse_ln, hierarchy, this)->fillData(sync_time, true);
    }*/
}

void Problem::resetTimeDependentData(
   const std::shared_ptr<hier::PatchLevel>& level,
   const double new_time,
   const bool can_be_refined)
{
   TBOX_ASSERT(level);
   level->setTime(new_time);
}

void Problem::resetDataToPreadvanceState(
   const std::shared_ptr<hier::PatchLevel>& level)
{
    //cout<<"resetDataToPreadvanceState"<<endl;
}

/*
 * Map data on a patch. This mapping is done only at the begining of the simulation.
 */
void Problem::mapDataOnPatch(const double time, const bool initial_time, const int ln, const std::shared_ptr< hier::PatchLevel >& level)
{
	(void) time;
	const tbox::SAMRAI_MPI& mpi(tbox::SAMRAI_MPI::getSAMRAIWorld());
   	if (initial_time || ln == 0) {

		// Mapping		
		int i, iterm, previousMapi, iWallAcc;
		bool interiorMapi;
		double iMapStart, iMapEnd;
		int j, jterm, previousMapj, jWallAcc;
		bool interiorMapj;
		double jMapStart, jMapEnd;
		int minBlock[2], maxBlock[2], unionsI, facePointI, ie1, ie2, ie3, proc, pcounter, working, finished, pred;
		double maxDistance, e1, e2, e3;
		bool done, modif, workingArray[mpi.getSize()], finishedArray[mpi.getSize()], workingGlobal, finishedGlobal, workingPatchArray[level->getLocalNumberOfPatches()], finishedPatchArray[level->getLocalNumberOfPatches()], workingPatchGlobal, finishedPatchGlobal;
		int nodes = mpi.getSize();
		int patches = level->getLocalNumberOfPatches();

		double SQRT3INV = 1.0/sqrt(3.0);

		if (ln == 0) {
			//FOV initialization
			for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
				const std::shared_ptr< hier::Patch >& patch = *p_it;

				//Get the dimensions of the patch
				hier::Box pbox = patch->getBox();
				double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
				double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
				double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
				double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
				double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
				const hier::Index boxfirst = patch->getBox().lower();
				const hier::Index boxlast  = patch->getBox().upper();

				//Get delta spaces into an array. dx, dy, dz.
				const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
				const double* dx  = patch_geom->getDx();

				int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
				int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;

				for (i = 0; i < ilast; i++) {
					for (j = 0; j < jlast; j++) {
						vector(FOV_1, i, j) = 0;
						vector(FOV_xLower, i, j) = 0;
						vector(FOV_xUpper, i, j) = 0;
						vector(FOV_yLower, i, j) = 0;
						vector(FOV_yUpper, i, j) = 0;
					}
				}
			}
			d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);

			//Region: domainI
			for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
				const std::shared_ptr< hier::Patch >& patch = *p_it;

				//Get the dimensions of the patch
				hier::Box pbox = patch->getBox();
				double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
				double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
				double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
				double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
				double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
				double* interior_i = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_i_id).get())->getPointer();
				double* interior_j = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_j_id).get())->getPointer();
				double* interior = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_regridding_value_id).get())->getPointer();
				int* nonSync = ((pdat::NodeData<int> *) patch->getPatchData(d_nonSync_regridding_tag_id).get())->getPointer();
				const hier::Index boxfirst = patch->getBox().lower();
				const hier::Index boxlast  = patch->getBox().upper();

				//Get delta spaces into an array. dx, dy, dz.
				const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
				const double* dx  = patch_geom->getDx();

				int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
				int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;

				iMapStart = 0;
				iMapEnd = d_grid_geometry->getPhysicalDomain().front().numberCells()[0];
				jMapStart = 0;
				jMapEnd = d_grid_geometry->getPhysicalDomain().front().numberCells()[1];
				for (i = iMapStart; i <= iMapEnd; i++) {
					for (j = jMapStart; j <= jMapEnd; j++) {
						if (i >= boxfirst(0) - d_ghost_width && i <= boxlast(0) + d_ghost_width && j >= boxfirst(1) - d_ghost_width && j <= boxlast(1) + d_ghost_width) {
							vector(FOV_1, i - boxfirst(0) + d_ghost_width, j - boxfirst(1) + d_ghost_width) = 100;
							vector(FOV_xLower, i - boxfirst(0) + d_ghost_width, j - boxfirst(1) + d_ghost_width) = 0;
							vector(FOV_xUpper, i - boxfirst(0) + d_ghost_width, j - boxfirst(1) + d_ghost_width) = 0;
							vector(FOV_yLower, i - boxfirst(0) + d_ghost_width, j - boxfirst(1) + d_ghost_width) = 0;
							vector(FOV_yUpper, i - boxfirst(0) + d_ghost_width, j - boxfirst(1) + d_ghost_width) = 0;
						}
					}
				}
				//Check stencil
				for (j = 0; j < jlast; j++) {
					for (i = 0; i < ilast; i++) {
						vector(interior_i, i, j) = 0;
						vector(interior_j, i, j) = 0;
					}
				}
				for (j = 0; j < jlast; j++) {
					for (i = 0; i < ilast; i++) {
						if (vector(FOV_1, i, j) > 0) {
							setStencilLimits(patch, i, j, d_FOV_1_id);
						}
					}
				}
			}
			d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);
			for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
				const std::shared_ptr< hier::Patch >& patch = *p_it;

				//Get the dimensions of the patch
				hier::Box pbox = patch->getBox();
				double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
				double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
				double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
				double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
				double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
				double* interior_i = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_i_id).get())->getPointer();
				double* interior_j = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_j_id).get())->getPointer();
				double* interior = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_regridding_value_id).get())->getPointer();
				int* nonSync = ((pdat::NodeData<int> *) patch->getPatchData(d_nonSync_regridding_tag_id).get())->getPointer();
				const hier::Index boxfirst = patch->getBox().lower();
				const hier::Index boxlast  = patch->getBox().upper();

				//Get delta spaces into an array. dx, dy, dz.
				const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
				const double* dx  = patch_geom->getDx();

				int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
				int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;

				for (j = 0; j < jlast; j++) {
					for (i = 0; i < ilast; i++) {
						if ((abs(vector(interior_i, i, j)) > 1 || abs(vector(interior_j, i, j)) > 1) && (i < d_ghost_width || i >= ilast - d_ghost_width || j < d_ghost_width || j >= jlast - d_ghost_width)) {
							checkStencil(patch, i, j, d_FOV_1_id);
						}
					}
				}
				for (j = 0; j < jlast; j++) {
					for (i = 0; i < ilast; i++) {
						if (vector(interior_i, i, j) != 0 || vector(interior_j, i, j) != 0) {
							vector(FOV_1, i, j) = 100;
							vector(FOV_xLower, i, j) = 0;
							vector(FOV_xUpper, i, j) = 0;
							vector(FOV_yLower, i, j) = 0;
							vector(FOV_yUpper, i, j) = 0;
						}
					}
				}
			}
			d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);

		}
		//Boundaries Mapping
		for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
			const std::shared_ptr< hier::Patch >& patch = *p_it;

			//Get the dimensions of the patch
			hier::Box pbox = patch->getBox();
			double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
			double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
			double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
			double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
			double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
			const hier::Index boxfirst = patch->getBox().lower();
			const hier::Index boxlast  = patch->getBox().upper();

			//Get delta spaces into an array. dx, dy, dz.
			const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
			const double* dx  = patch_geom->getDx();

			int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
			int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;

			//y-Upper
			if (patch->getPatchGeometry()->getTouchesRegularBoundary(1, 1)) {
				for (j = jlast - d_ghost_width; j < jlast; j++) {
					for (i = 0; i < ilast; i++) {
						vector(FOV_yUpper, i, j) = 100;
						vector(FOV_1, i, j) = 0;
						vector(FOV_xLower, i, j) = 0;
						vector(FOV_xUpper, i, j) = 0;
						vector(FOV_yLower, i, j) = 0;
					}
				}
			}
			//y-Lower
			if (patch->getPatchGeometry()->getTouchesRegularBoundary(1, 0)) {
				for (j = 0; j < d_ghost_width; j++) {
					for (i = 0; i < ilast; i++) {
						vector(FOV_yLower, i, j) = 100;
						vector(FOV_1, i, j) = 0;
						vector(FOV_xLower, i, j) = 0;
						vector(FOV_xUpper, i, j) = 0;
						vector(FOV_yUpper, i, j) = 0;
					}
				}
			}
			//x-Upper
			if (patch->getPatchGeometry()->getTouchesRegularBoundary(0, 1)) {
				for (j = 0; j < jlast; j++) {
					for (i = ilast - d_ghost_width; i < ilast; i++) {
						vector(FOV_xUpper, i, j) = 100;
						vector(FOV_1, i, j) = 0;
						vector(FOV_xLower, i, j) = 0;
						vector(FOV_yLower, i, j) = 0;
						vector(FOV_yUpper, i, j) = 0;
					}
				}
			}
			//x-Lower
			if (patch->getPatchGeometry()->getTouchesRegularBoundary(0, 0)) {
				for (j = 0; j < jlast; j++) {
					for (i = 0; i < d_ghost_width; i++) {
						vector(FOV_xLower, i, j) = 100;
						vector(FOV_1, i, j) = 0;
						vector(FOV_xUpper, i, j) = 0;
						vector(FOV_yLower, i, j) = 0;
						vector(FOV_yUpper, i, j) = 0;
					}
				}
			}
		}
		d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);




   	}
}


/*
 * Sets the limit for the checkstencil routine
 */
void Problem::setStencilLimits(std::shared_ptr< hier::Patch > patch, int i, int j, int v) const {
	double* FOV = ((pdat::NodeData<double> *) patch->getPatchData(v).get())->getPointer();
	double* interior_i = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_i_id).get())->getPointer();
	double* interior_j = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_j_id).get())->getPointer();
	//Get the dimensions of the patch
	const hier::Index boxfirst = patch->getBox().lower();
	const hier::Index boxlast  = patch->getBox().upper();
	//Auxiliary definitions
	int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
	int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;

	int iStart, iEnd, currentGhosti, otherSideShifti, jStart, jEnd, currentGhostj, otherSideShiftj, shift;

	currentGhosti = d_ghost_width - 1;
	otherSideShifti = 0;
	currentGhostj = d_ghost_width - 1;
	otherSideShiftj = 0;
	//Checking width
	if ((i + 1 < ilast && vector(FOV, i + 1, j) == 0) ||  (i - 1 >= 0 && vector(FOV, i - 1, j) == 0)) {
		if (i + 1 < ilast && vector(FOV, i + 1, j) > 0) {
			bool stop_counting = false;
			for(int iti = i + 1; iti <= i + d_ghost_width - 1 && currentGhosti > 0; iti++) {
				if (iti < ilast  && vector(FOV, iti, j) > 0 && stop_counting == false) {
					currentGhosti--;
				} else {
					//First not interior point found
					if (iti < ilast  && vector(FOV, iti, j) == 0) {
						stop_counting = true;
					}
					//Physical boundary reach
					if (iti >= ilast - 4 && patch->getPatchGeometry()->getTouchesRegularBoundary (0, 1)) {
						stop_counting = true;
						//Calculate the number of cells the limit cannot grow
						if (iti + currentGhosti/2 >= ilast - 4) {
							otherSideShifti = (iti  + currentGhosti/2) - (ilast - 4);
						}
					}
				}
			}
		}
		if (i - 1 >= 0 && vector(FOV, i - 1, j) > 0) {
			bool stop_counting = false;
			for(int iti = i - 1; iti >= i - d_ghost_width + 1 && currentGhosti > 0; iti--) {
				if (iti >= 0  && vector(FOV, iti, j) > 0) {
					currentGhosti--;
				} else {
					//First not interior point found
					if (iti >= 0 && vector(FOV, iti, j) == 0) {
						stop_counting = true;
					}
					//Physical boundary reach
					if (iti < 4 && patch->getPatchGeometry()->getTouchesRegularBoundary (0, 0)) {
						stop_counting = true;
						//calculate the number of cells the limit cannot grow
						if (iti  -  ((currentGhosti) - currentGhosti/2) < 4) {
							otherSideShifti = 4 - (iti  - ((currentGhosti) - currentGhosti/2));
						}
					}
				}
			}
		}
		if (currentGhosti > 0) {
			if (i + 1 < ilast && vector(FOV, i + 1, j) > 0) {
				shift = 0;
				if(patch->getPatchGeometry()->getTouchesRegularBoundary (0, 0)) {
					while(i - ((currentGhosti) - currentGhosti/2) + shift < 4) {
						shift++;
					}
				}
				iStart = (currentGhosti - currentGhosti/2) - shift - otherSideShifti;
				iEnd = 0;
			} else {
				if (i - 1 >= 0 && vector(FOV, i - 1, j) > 0) {
					shift = 0;
					if(patch->getPatchGeometry()->getTouchesRegularBoundary (0, 1)) {
						while(i + currentGhosti/2 + shift >= ilast - 4) {
							shift--;
						}
					}
					iStart = 0;
					iEnd = currentGhosti/2 + shift + otherSideShifti;
				} else {
					shift = 0;
					if(patch->getPatchGeometry()->getTouchesRegularBoundary (0, 0)) {
						while(i - ((currentGhosti) - currentGhosti/2) + shift < 4) {
							shift++;
						}
					}
					if(patch->getPatchGeometry()->getTouchesRegularBoundary (0, 1)) {
						while(i + currentGhosti/2 + shift >= ilast - 4) {
							shift--;
						}
					}
					iStart = (currentGhosti - currentGhosti/2) - shift;
					iEnd = currentGhosti/2 + shift;
				}
			}
		} else {
			iStart = 0;
			iEnd = 0;
		}
	} else {
		iStart = 0;
		iEnd = 0;
	}
	if ((j + 1 < jlast && vector(FOV, i, j + 1) == 0) ||  (j - 1 >= 0 && vector(FOV, i, j - 1) == 0)) {
		if (j + 1 < jlast && vector(FOV, i, j + 1) > 0) {
			bool stop_counting = false;
			for(int itj = j + 1; itj <= j + d_ghost_width - 1 && currentGhostj > 0; itj++) {
				if (itj < jlast  && vector(FOV, i, itj) > 0 && stop_counting == false) {
					currentGhostj--;
				} else {
					//First not interior point found
					if (itj < jlast  && vector(FOV, i, itj) == 0) {
						stop_counting = true;
					}
					//Physical boundary reach
					if (itj >= jlast - 4 && patch->getPatchGeometry()->getTouchesRegularBoundary (1, 1)) {
						stop_counting = true;
						//Calculate the number of cells the limit cannot grow
						if (itj + currentGhostj/2 >= jlast - 4) {
							otherSideShiftj = (itj  + currentGhostj/2) - (jlast - 4);
						}
					}
				}
			}
		}
		if (j - 1 >= 0 && vector(FOV, i, j - 1) > 0) {
			bool stop_counting = false;
			for(int itj = j - 1; itj >= j - d_ghost_width + 1 && currentGhostj > 0; itj--) {
				if (itj >= 0  && vector(FOV, i, itj) > 0) {
					currentGhostj--;
				} else {
					//First not interior point found
					if (itj >= 0 && vector(FOV, i, itj) == 0) {
						stop_counting = true;
					}
					//Physical boundary reach
					if (itj < 4 && patch->getPatchGeometry()->getTouchesRegularBoundary (1, 0)) {
						stop_counting = true;
						//calculate the number of cells the limit cannot grow
						if (itj  -  ((currentGhostj) - currentGhostj/2) < 4) {
							otherSideShiftj = 4 - (itj  - ((currentGhostj) - currentGhostj/2));
						}
					}
				}
			}
		}
		if (currentGhostj > 0) {
			if (j + 1 < jlast && vector(FOV, i, j + 1) > 0) {
				shift = 0;
				if(patch->getPatchGeometry()->getTouchesRegularBoundary (1, 0)) {
					while(j - ((currentGhostj) - currentGhostj/2) + shift < 4) {
						shift++;
					}
				}
				jStart = (currentGhostj - currentGhostj/2) - shift - otherSideShiftj;
				jEnd = 0;
			} else {
				if (j - 1 >= 0 && vector(FOV, i, j - 1) > 0) {
					shift = 0;
					if(patch->getPatchGeometry()->getTouchesRegularBoundary (1, 1)) {
						while(j + currentGhostj/2 + shift >= jlast - 4) {
							shift--;
						}
					}
					jStart = 0;
					jEnd = currentGhostj/2 + shift + otherSideShiftj;
				} else {
					shift = 0;
					if(patch->getPatchGeometry()->getTouchesRegularBoundary (1, 0)) {
						while(j - ((currentGhostj) - currentGhostj/2) + shift < 4) {
							shift++;
						}
					}
					if(patch->getPatchGeometry()->getTouchesRegularBoundary (1, 1)) {
						while(j + currentGhostj/2 + shift >= jlast - 4) {
							shift--;
						}
					}
					jStart = (currentGhostj - currentGhostj/2) - shift;
					jEnd = currentGhostj/2 + shift;
				}
			}
		} else {
			jStart = 0;
			jEnd = 0;
		}
	} else {
		jStart = 0;
		jEnd = 0;
	}
	//Assigning stencil limits
	for(int iti = i - iStart; iti <= i + iEnd; iti++) {
		for(int itj = j - jStart; itj <= j + jEnd; itj++) {
			if(iti >= 0 && iti < ilast && itj >= 0 && itj < jlast && vector(FOV, iti, itj) == 0) {
				if (i - iti < 0) {
					vector(interior_i, iti, itj) = - (iStart + 1) - (i - iti);
				} else {
					vector(interior_i, iti, itj) = (iStart + 1) - (i - iti);
				}
				if (j - itj < 0) {
					vector(interior_j, iti, itj) = - (jStart + 1) - (j - itj);
				} else {
					vector(interior_j, iti, itj) = (jStart + 1) - (j - itj);
				}
			}
		}
	}
}

/*
 * Checks if the point has a stencil width
 */
void Problem::checkStencil(std::shared_ptr< hier::Patch > patch, int i, int j, int v) const {
	double* FOV = ((pdat::NodeData<double> *) patch->getPatchData(v).get())->getPointer();
	double* interior_i = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_i_id).get())->getPointer();
	double* interior_j = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_j_id).get())->getPointer();
	//Get the dimensions of the patch
	const hier::Index boxfirst = patch->getBox().lower();
	const hier::Index boxlast  = patch->getBox().upper();
	//Auxiliary definitions
	int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
	int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;

	int i_i = vector(interior_i, i, j);
	int iStart = MAX(0, i_i) - 1;
	int iEnd = MAX(0, -i_i) - 1;
	int i_j = vector(interior_j, i, j);
	int jStart = MAX(0, i_j) - 1;
	int jEnd = MAX(0, -i_j) - 1;

	for(int iti = i - iStart; iti <= i + iEnd; iti++) {
		for(int itj = j - jStart; itj <= j + jEnd; itj++) {
			if(iti >= 0 && iti < ilast && itj >= 0 && itj < jlast && vector(FOV, iti, itj) == 0) {
				vector(interior_i, iti, itj) = i_i  - (i - iti);
				vector(interior_j, iti, itj) = i_j  - (j - itj);
			}
		}
	}
}


// Point class for the floodfill algorithm
class Point {
private:
public:
	int i, j;
};

void Problem::floodfill(std::shared_ptr< hier::Patch > patch, int i, int j, int pred, int seg) const {

	double* FOV;
	switch(seg) {
		case 1:
			FOV = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
		break;
	}
	int* nonSync = ((pdat::NodeData<int> *) patch->getPatchData(d_nonSync_regridding_tag_id).get())->getPointer();
	double* interior = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_regridding_value_id).get())->getPointer();
	//Get the dimensions of the patch
	const hier::Index boxfirst = patch->getBox().lower();
	const hier::Index boxlast  = patch->getBox().upper();
	//Auxiliary definitions
	int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
	int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;

	stack<Point> mystack;

	Point p;
	p.i = i;
	p.j = j;

	mystack.push(p);
	while(mystack.size() > 0) {
		p = mystack.top();
		mystack.pop();
		if (vector(nonSync, p.i, p.j) == 0) {
			vector(nonSync, p.i, p.j) = pred;
			vector(interior, p.i, p.j) = pred;
			if (pred == 2) {
				vector(FOV, p.i, p.j) = 100;
			}
			if (p.i - 1 >= 0 && vector(nonSync, p.i - 1, p.j) == 0) {
				Point np;
				np.i = p.i-1;
				np.j = p.j;
				mystack.push(np);
			}
			if (p.i + 1 < ilast && vector(nonSync, p.i + 1, p.j) == 0) {
				Point np;
				np.i = p.i+1;
				np.j = p.j;
				mystack.push(np);
			}
			if (p.j - 1 >= 0 && vector(nonSync, p.i, p.j - 1) == 0) {
				Point np;
				np.i = p.i;
				np.j = p.j-1;
				mystack.push(np);
			}
			if (p.j + 1 < jlast && vector(nonSync, p.i, p.j + 1) == 0) {
				Point np;
				np.i = p.i;
				np.j = p.j+1;
				mystack.push(np);
			}
		}
	}
}


/*
 * FOV correction for AMR
 */
void Problem::correctFOVS(const std::shared_ptr< hier::PatchLevel >& level) {
	int i, j, k;
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr< hier::Patch >& patch = *p_it;

		//Get the dimensions of the patch
		hier::Box pbox = patch->getBox();
		double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
		const hier::Index boxfirst = patch->getBox().lower();
		const hier::Index boxlast  = patch->getBox().upper();

		//Get delta spaces into an array. dx, dy, dz.
		const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
		const double* dx  = patch_geom->getDx();

		int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
		int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;

		for (i = 0; i < ilast; i++) {
			for (j = 0; j < jlast; j++) {
				if (vector(FOV_xLower, i, j) > 0) {
					vector(FOV_xLower, i, j) = 100;
					vector(FOV_1, i, j) = 0;
					vector(FOV_xUpper, i, j) = 0;
					vector(FOV_yLower, i, j) = 0;
					vector(FOV_yUpper, i, j) = 0;
				}
				if (vector(FOV_xUpper, i, j) > 0) {
					vector(FOV_xUpper, i, j) = 100;
					vector(FOV_1, i, j) = 0;
					vector(FOV_xLower, i, j) = 0;
					vector(FOV_yLower, i, j) = 0;
					vector(FOV_yUpper, i, j) = 0;
				}
				if (vector(FOV_yLower, i, j) > 0) {
					vector(FOV_yLower, i, j) = 100;
					vector(FOV_1, i, j) = 0;
					vector(FOV_xLower, i, j) = 0;
					vector(FOV_xUpper, i, j) = 0;
					vector(FOV_yUpper, i, j) = 0;
				}
				if (vector(FOV_yUpper, i, j) > 0) {
					vector(FOV_yUpper, i, j) = 100;
					vector(FOV_1, i, j) = 0;
					vector(FOV_xLower, i, j) = 0;
					vector(FOV_xUpper, i, j) = 0;
					vector(FOV_yLower, i, j) = 0;
				}
				if (vector(FOV_1, i, j) > 0) {
					vector(FOV_1, i, j) = 100;
					vector(FOV_xLower, i, j) = 0;
					vector(FOV_xUpper, i, j) = 0;
					vector(FOV_yLower, i, j) = 0;
					vector(FOV_yUpper, i, j) = 0;
				}
			}
		}
	}
}




void Problem::interphaseMapping(const double time ,const bool initial_time,const int ln, const std::shared_ptr< hier::PatchLevel >& level, const int remesh) {
	const tbox::SAMRAI_MPI& mpi(tbox::SAMRAI_MPI::getSAMRAIWorld());
	if (remesh == 1) {
		//Update extrapolation variables
	}
}


/*
 * Checks if the point has to be stalled
 */
bool Problem::checkStalled(std::shared_ptr< hier::Patch > patch, int i, int j, int v) const {
	double* FOV = ((pdat::NodeData<double> *) patch->getPatchData(v).get())->getPointer();
	double* interior = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_regridding_value_id).get())->getPointer();
	//Get the dimensions of the patch
	const hier::Index boxfirst = patch->getBox().lower();
	const hier::Index boxlast  = patch->getBox().upper();
	//Auxiliary definitions
	int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
	int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;

	int stencilAcc, stencilAccMax_i, stencilAccMax_j;
	bool notEnoughStencil = false;
	int FOV_threshold = 0;
	if (vector(FOV, i, j) <= FOV_threshold) {
		notEnoughStencil = true;
	} else {
		stencilAcc = 0;
		stencilAccMax_i = 0;
		for (int it1 = MAX(i-d_regionMinThickness, 0); it1 <= MIN(i+d_regionMinThickness, ilast - 1); it1++) {
			if (vector(FOV, it1, j) > FOV_threshold) {
				stencilAcc++;
			} else {
				stencilAccMax_i = MAX(stencilAccMax_i, stencilAcc);
				stencilAcc = 0;
			}
		}
		stencilAccMax_i = MAX(stencilAccMax_i, stencilAcc);
		stencilAcc = 0;
		stencilAccMax_j = 0;
		for (int jt1 = MAX(j-d_regionMinThickness, 0); jt1 <= MIN(j+d_regionMinThickness, jlast - 1); jt1++) {
			if (vector(FOV, i, jt1) > FOV_threshold) {
				stencilAcc++;
			} else {
				stencilAccMax_j = MAX(stencilAccMax_j, stencilAcc);
				stencilAcc = 0;
			}
		}
		stencilAccMax_j = MAX(stencilAccMax_j, stencilAcc);
		if ((stencilAccMax_i < d_regionMinThickness) || (stencilAccMax_j < d_regionMinThickness)) {
			notEnoughStencil = true;
		}
	}
	return notEnoughStencil;
}






/*
 * Initialize data on a patch. This initialization is done only at the begining of the simulation.
 */
void Problem::initializeDataOnPatch(hier::Patch& patch, 
                                    const double time,
                                    const bool initial_time)
{
	(void) time;
   	if (initial_time) {
		// Initial conditions		
		//Get fields, auxiliary fields and local variables that are going to be used.
		double* rho = ((pdat::NodeData<double> *) patch.getPatchData(d_rho_id).get())->getPointer();
		((pdat::NodeData<double> *) patch.getPatchData(d_rho_id).get())->fillAll(0);
		double* mx = ((pdat::NodeData<double> *) patch.getPatchData(d_mx_id).get())->getPointer();
		((pdat::NodeData<double> *) patch.getPatchData(d_mx_id).get())->fillAll(0);
		double* my = ((pdat::NodeData<double> *) patch.getPatchData(d_my_id).get())->getPointer();
		((pdat::NodeData<double> *) patch.getPatchData(d_my_id).get())->fillAll(0);
		double* e = ((pdat::NodeData<double> *) patch.getPatchData(d_e_id).get())->getPointer();
		((pdat::NodeData<double> *) patch.getPatchData(d_e_id).get())->fillAll(0);
		double* P = ((pdat::NodeData<double> *) patch.getPatchData(d_P_id).get())->getPointer();
		((pdat::NodeData<double> *) patch.getPatchData(d_P_id).get())->fillAll(0);
		
		//Get the dimensions of the patch
		hier::Box pbox = patch.getBox();
		double* FOV_1 = ((pdat::NodeData<double> *) patch.getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch.getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch.getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch.getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch.getPatchData(d_FOV_yUpper_id).get())->getPointer();
		const hier::Index boxfirst = patch.getBox().lower();
		const hier::Index boxlast  = patch.getBox().upper();
		
		//Get delta spaces into an array. dx, dy, dz.
		const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch.getPatchGeometry()));
		const double* dx  = patch_geom->getDx();
		
		int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
		int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
		for(int j = 0; j < jlast; j++) {
			for(int i = 0; i < ilast; i++) {
				if (vector(FOV_1, i, j) > 0) {
					vector(rho, i, j) = pow((1.0 - ((gamma - 1.0) * (epsilon * epsilon)) / (8.0 * gamma * (M_PI * M_PI)) * exp((1.0 - (xcoord(i) - 5.0) * (xcoord(i) - 5.0)) - (ycoord(j) - 5.0) * (ycoord(j) - 5.0))), (1.0 / (gamma - 1.0)));
					vector(mx, i, j) = vector(rho, i, j) - (vector(rho, i, j) * epsilon) / (2.0 * M_PI) * (ycoord(j) - 5.0) * exp(0.5 * ((1.0 - (xcoord(i) - 5.0) * (xcoord(i) - 5.0)) - (ycoord(j) - 5.0) * (ycoord(j) - 5.0)));
					vector(my, i, j) = vector(rho, i, j) + (vector(rho, i, j) * epsilon) / (2.0 * M_PI) * (xcoord(i) - 5.0) * exp(0.5 * ((1.0 - (xcoord(i) - 5.0) * (xcoord(i) - 5.0)) - (ycoord(j) - 5.0) * (ycoord(j) - 5.0)));
					vector(e, i, j) = (pow(vector(rho, i, j), gamma)) / (gamma - 1.0) + (vector(mx, i, j) * vector(mx, i, j) + vector(my, i, j) * vector(my, i, j)) / (2.0 * vector(rho, i, j));
					vector(P, i, j) = pow(vector(rho, i, j), gamma);
				}
		
			}
		}
		

   	}
}



/*
 * Gets the coarser patch that contains the box
 */
const std::shared_ptr<hier::Patch >& Problem::getCoarserPatch(
	const std::shared_ptr< hier::PatchLevel >& level,
	const hier::Box interior, 
	const hier::IntVector ratio)
{
	const hier::Box& coarsenBox = hier::Box::coarsen(interior, ratio);
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr< hier::Patch >& patch = *p_it;
		const hier::Box& interior_C = patch->getBox();
		if (interior_C.intersects(coarsenBox)) {
			return patch;		
		}
	}
    return NULL;
}

/*
 * Reset the hierarchy-dependent internal information.
 */
void Problem::resetHierarchyConfiguration (
   const std::shared_ptr<hier::PatchHierarchy >& new_hierarchy ,
   int coarsest_level ,
   int finest_level )
{
	int finest_hiera_level = new_hierarchy->getFinestLevelNumber();

   	//  If we have added or removed a level, resize the schedule arrays

	d_bdry_sched_advance2.resize(finest_hiera_level+1);
	d_bdry_sched_advance8.resize(finest_hiera_level+1);
	d_bdry_sched_advance14.resize(finest_hiera_level+1);
	d_coarsen_schedule.resize(finest_hiera_level+1);
	d_bdry_sched_postCoarsen.resize(finest_hiera_level+1);
	//  Build coarsen and refine communication schedules.
	for (int ln = coarsest_level; ln <= finest_hiera_level; ln++) {
		std::shared_ptr< hier::PatchLevel > level(new_hierarchy->getPatchLevel(ln));
		d_bdry_sched_advance2[ln] = d_bdry_fill_advance2->createSchedule(level,ln-1,new_hierarchy,this);
		d_bdry_sched_advance8[ln] = d_bdry_fill_advance8->createSchedule(level,ln-1,new_hierarchy,this);
		d_bdry_sched_advance14[ln] = d_bdry_fill_advance14->createSchedule(level,ln-1,new_hierarchy,this);
		d_bdry_sched_postCoarsen[ln] = d_bdry_post_coarsen->createSchedule(level);
		// coarsen schedule only for levels > 0
		if (ln > 0) {
			std::shared_ptr< hier::PatchLevel > coarser_level(new_hierarchy->getPatchLevel(ln-1));
			d_coarsen_schedule[ln] = d_coarsen_algorithm->createSchedule(coarser_level, level, NULL);
		}
	}

}

/* 
 * Calculation of auxiliary terms after a new level is regridded.
 */
void Problem::postNewLevel(const std::shared_ptr< hier::PatchLevel >& level) {
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr< hier::Patch >& patch = *p_it;
		double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
		double* P = ((pdat::NodeData<double> *) patch->getPatchData(d_P_id).get())->getPointer();
		double* e = ((pdat::NodeData<double> *) patch->getPatchData(d_e_id).get())->getPointer();
		double* rho = ((pdat::NodeData<double> *) patch->getPatchData(d_rho_id).get())->getPointer();
		double* mx = ((pdat::NodeData<double> *) patch->getPatchData(d_mx_id).get())->getPointer();
		double* my = ((pdat::NodeData<double> *) patch->getPatchData(d_my_id).get())->getPointer();

		//Get the dimensions of the patch
		const hier::Index boxfirst = patch->getBox().lower();
		const hier::Index boxlast = patch->getBox().upper();
		//Get delta spaces into an array. dx, dy, dz.
		std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
		const double* dx  = patch_geom->getDx();
		const double simPlat_dt = patch->getPatchData(d_FOV_1_id)->getTime();

		//Auxiliary definitions
		int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
		int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
		for(int j = 0; j < jlast; j++) {
			for(int i = 0; i < ilast; i++) {
				if ((vector(FOV_1, i, j) > 0)) {
					vector(P, i, j) = (gamma - 1.0) * (vector(e, i, j) - vector(rho, i, j) * (((vector(mx, i, j) / vector(rho, i, j)) * (vector(mx, i, j) / vector(rho, i, j))) / 2.0 + ((vector(my, i, j) / vector(rho, i, j)) * (vector(my, i, j) / vector(rho, i, j))) / 2.0));
				}
			}
		}
	}
}

/*
 * This method sets the physical boundary conditions.
 */
void Problem::setPhysicalBoundaryConditions(
   hier::Patch& patch,
   const double fill_time,
   const hier::IntVector& ghost_width_to_fill)
{
	//Boundary must not be implemented in this method
}

/*
 * Set up external plotter to plot internal data from this class.
 * Register variables appropriate for plotting.
 */
int Problem::setupPlotterMesh(appu::VisItDataWriter &plotter) const {
	if (!d_patch_hierarchy) {
		TBOX_ERROR(d_object_name << ": No hierarchy inn"
			<< " Problem::setupPlottern"
			<< "The hierarchy must be set before callingn"
			<< "this function.n");
	}
	plotter.registerPlotQuantity("FOV_1","SCALAR",d_FOV_1_id);
	hier::VariableDatabase *vdb = hier::VariableDatabase::getDatabase();
	for (set<string>::const_iterator it = d_full_mesh_writer_variables.begin() ; it != d_full_mesh_writer_variables.end(); ++it) {
		string var_to_register = *it;
		if (!(vdb->checkVariableExists(var_to_register))) {
			TBOX_ERROR(d_object_name << ": Variable selected for 3D write not found:" <<  var_to_register);
		}
		int var_id = vdb->getVariable(var_to_register)->getInstanceIdentifier();
		plotter.registerPlotQuantity(var_to_register,"SCALAR",var_id);
	}
	return 0;
}
/*
 * Set up external plotter to plot integration data from this class.
 * Register variables appropriate for plotting.
 */
int Problem::setupIntegralPlotter(vector<std::shared_ptr<IntegrateDataWriter> > &plotters) const {
	if (!d_patch_hierarchy) {
		TBOX_ERROR(d_object_name << ": No hierarchy inn"
		<< " Problem::setupIntegralPlottern"
		<< "The hierarchy must be set before callingn"
		<< "this function.n");
	}
	int i = 0;
	for (vector<std::shared_ptr<IntegrateDataWriter> >::const_iterator it = plotters.begin(); it != plotters.end(); ++it) {
		std::shared_ptr<IntegrateDataWriter> plotter = *it;
		hier::VariableDatabase *vdb = hier::VariableDatabase::getDatabase();
		set<string> variables = d_integralVariables[i];
		for (set<string>::const_iterator it2 = variables.begin() ; it2 != variables.end(); ++it2) {
			string var_to_register = *it2;
			if (!(vdb->checkVariableExists(var_to_register))) {
				TBOX_ERROR(d_object_name << ": Variable selected for Integration not found:" <<  var_to_register);
			}
			int var_id = vdb->getVariable(var_to_register)->getInstanceIdentifier();
			plotter->registerPlotQuantity(var_to_register,"SCALAR",var_id);
		}
		i++;
	}
	return 0;
}
/*
 * Set up external plotter to plot point data from this class.
 * Register variables appropriate for plotting.
 */
int Problem::setupPointPlotter(vector<std::shared_ptr<PointDataWriter> > &plotters) const {
	int i = 0;
	for (vector<std::shared_ptr<PointDataWriter> >::const_iterator it = plotters.begin(); it != plotters.end(); ++it) {
		std::shared_ptr<PointDataWriter> plotter = *it;
		hier::VariableDatabase *vdb = hier::VariableDatabase::getDatabase();
		set<string> variables = d_pointVariables[i];
		for (set<string>::const_iterator it2 = variables.begin() ; it2 != variables.end(); ++it2) {
			string var_to_register = *it2;
			if (!(vdb->checkVariableExists(var_to_register))) {
				TBOX_ERROR(d_object_name << ": Variable selected for Point not found:" <<  var_to_register);
			}
			int var_id = vdb->getVariable(var_to_register)->getInstanceIdentifier();
			plotter->registerPlotQuantity(var_to_register,"SCALAR",var_id);
		}
		i++;
	}
	return 0;
}


/*
 * Perform a single step from the discretization schema algorithm   
 */
double Problem::advanceLevel(
   const std::shared_ptr<hier::PatchLevel>& level,
   const std::shared_ptr<hier::PatchHierarchy>& hierarchy,
   const double current_time,
   const double new_time,
   const bool first_step,
   const bool last_step,
   const bool regrid_advance)
{
	const tbox::SAMRAI_MPI& mpi(tbox::SAMRAI_MPI::getSAMRAIWorld());

	const int ln = level->getLevelNumber();
	const double simPlat_dt = new_time - current_time;
	const double level_ratio = level->getRatioToCoarserLevel().max();
	if (first_step) {
		bo_substep_iteration[ln] = 0;
	}
	else {
		bo_substep_iteration[ln] = bo_substep_iteration[ln] + 1;
	}
	time_interpolate_operator_mesh1->setRatio(level_ratio);
	time_interpolate_operator_mesh1->setStep(0);
	time_interpolate_operator_mesh1->setTimeSubstepNumber(bo_substep_iteration[ln]);

	if (d_refinedTimeStepping && first_step && ln > 0) {
		current_iteration[ln] = (current_iteration[ln - 1] - 1) * hierarchy->getRatioToCoarserLevel(ln).max() + 1;
	} else {
		current_iteration[ln] = current_iteration[ln] + 1;
	}
	int previous_iteration = current_iteration[ln] - 1;
	int outputCycle = current_iteration[ln];
	int maxLevels = hierarchy->getMaxNumberOfLevels();
	if (maxLevels > ln + 1) {
		int currentLevelNumber = ln;
		while (currentLevelNumber < maxLevels - 1) {
			int ratio = hierarchy->getRatioToCoarserLevel(currentLevelNumber + 1).max();
			outputCycle = outputCycle * ratio;
			previous_iteration = previous_iteration * ratio;
			currentLevelNumber++;
		}
	}

	t_step->start();
  	// Shifting time
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch > patch = *p_it;
		std::shared_ptr< pdat::NodeData<double> > e(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_e_id)));
		std::shared_ptr< pdat::NodeData<double> > e_p(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_e_p_id)));
		std::shared_ptr< pdat::NodeData<double> > rho(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_rho_id)));
		std::shared_ptr< pdat::NodeData<double> > rho_p(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_rho_p_id)));
		std::shared_ptr< pdat::NodeData<double> > mx(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_mx_id)));
		std::shared_ptr< pdat::NodeData<double> > mx_p(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_mx_p_id)));
		std::shared_ptr< pdat::NodeData<double> > my(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_my_id)));
		std::shared_ptr< pdat::NodeData<double> > my_p(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_my_p_id)));
		std::shared_ptr< pdat::NodeData<double> > rk1rho(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_rk1rho_id)));
		std::shared_ptr< pdat::NodeData<double> > rk1e(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_rk1e_id)));
		std::shared_ptr< pdat::NodeData<double> > rk1mx(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_rk1mx_id)));
		std::shared_ptr< pdat::NodeData<double> > rk1my(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_rk1my_id)));
		std::shared_ptr< pdat::NodeData<double> > rk2rho(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_rk2rho_id)));
		std::shared_ptr< pdat::NodeData<double> > rk2e(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_rk2e_id)));
		std::shared_ptr< pdat::NodeData<double> > rk2mx(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_rk2mx_id)));
		std::shared_ptr< pdat::NodeData<double> > rk2my(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_rk2my_id)));
		my_p->copy(*my);
		mx_p->copy(*mx);
		rho_p->copy(*rho);
		e_p->copy(*e);
		e_p->setTime(current_time);
		rho_p->setTime(current_time);
		mx_p->setTime(current_time);
		my_p->setTime(current_time);
		rk1rho->setTime(current_time + simPlat_dt);
		rk1e->setTime(current_time + simPlat_dt);
		rk1mx->setTime(current_time + simPlat_dt);
		rk1my->setTime(current_time + simPlat_dt);
		rk2rho->setTime(current_time + simPlat_dt * 0.5);
		rk2e->setTime(current_time + simPlat_dt * 0.5);
		rk2mx->setTime(current_time + simPlat_dt * 0.5);
		rk2my->setTime(current_time + simPlat_dt * 0.5);
		rho->setTime(current_time + simPlat_dt);
		e->setTime(current_time + simPlat_dt);
		mx->setTime(current_time + simPlat_dt);
		my->setTime(current_time + simPlat_dt);
	}
  	// Evolution
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch >& patch = *p_it;
		double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
	
		double* rho_p = ((pdat::NodeData<double> *) patch->getPatchData(d_rho_p_id).get())->getPointer();
		double* mx_p = ((pdat::NodeData<double> *) patch->getPatchData(d_mx_p_id).get())->getPointer();
		double* my_p = ((pdat::NodeData<double> *) patch->getPatchData(d_my_p_id).get())->getPointer();
		double* e_p = ((pdat::NodeData<double> *) patch->getPatchData(d_e_p_id).get())->getPointer();
		double* P = ((pdat::NodeData<double> *) patch->getPatchData(d_P_id).get())->getPointer();
		double* rk1rho = ((pdat::NodeData<double> *) patch->getPatchData(d_rk1rho_id).get())->getPointer();
		double* rk1mx = ((pdat::NodeData<double> *) patch->getPatchData(d_rk1mx_id).get())->getPointer();
		double* rk1my = ((pdat::NodeData<double> *) patch->getPatchData(d_rk1my_id).get())->getPointer();
		double* rk1e = ((pdat::NodeData<double> *) patch->getPatchData(d_rk1e_id).get())->getPointer();
		double p1lnirho, p1lnimx, p1lnimy, p1lnie, p1lnjrho, p1lnjmx, p1lnjmy, p1lnje, p2lnirho, p2lnimx, p2lnimy, p2lnie, p2lnjrho, p2lnjmx, p2lnjmy, p2lnje, p3lnirho, p3lnimx, p3lnimy, p3lnie, p3lnjrho, p3lnjmx, p3lnjmy, p3lnje, p4lnirho, p4lnimx, p4lnimy, p4lnie, p4lnjrho, p4lnjmx, p4lnjmy, p4lnje, p1rnirho, p1rnimx, p1rnimy, p1rnie, p1rnjrho, p1rnjmx, p1rnjmy, p1rnje, p2rnirho, p2rnimx, p2rnimy, p2rnie, p2rnjrho, p2rnjmx, p2rnjmy, p2rnje, p3rnirho, p3rnimx, p3rnimy, p3rnie, p3rnjrho, p3rnjmx, p3rnjmy, p3rnje, p4rnirho, p4rnimx, p4rnimy, p4rnie, p4rnjrho, p4rnjmx, p4rnjmy, p4rnje, beta1lnirho, beta1lnimx, beta1lnimy, beta1lnie, beta1lnjrho, beta1lnjmx, beta1lnjmy, beta1lnje, beta2lnirho, beta2lnimx, beta2lnimy, beta2lnie, beta2lnjrho, beta2lnjmx, beta2lnjmy, beta2lnje, beta3lnirho, beta3lnimx, beta3lnimy, beta3lnie, beta3lnjrho, beta3lnjmx, beta3lnjmy, beta3lnje, beta4lnirho, beta4lnimx, beta4lnimy, beta4lnie, beta4lnjrho, beta4lnjmx, beta4lnjmy, beta4lnje, beta1rnirho, beta1rnimx, beta1rnimy, beta1rnie, beta1rnjrho, beta1rnjmx, beta1rnjmy, beta1rnje, beta2rnirho, beta2rnimx, beta2rnimy, beta2rnie, beta2rnjrho, beta2rnjmx, beta2rnjmy, beta2rnje, beta3rnirho, beta3rnimx, beta3rnimy, beta3rnie, beta3rnjrho, beta3rnjmx, beta3rnjmy, beta3rnje, beta4rnirho, beta4rnimx, beta4rnimy, beta4rnie, beta4rnjrho, beta4rnjmx, beta4rnjmy, beta4rnje, w1lnirho, w1lnimx, w1lnimy, w1lnie, w1lnjrho, w1lnjmx, w1lnjmy, w1lnje, w2lnirho, w2lnimx, w2lnimy, w2lnie, w2lnjrho, w2lnjmx, w2lnjmy, w2lnje, w3lnirho, w3lnimx, w3lnimy, w3lnie, w3lnjrho, w3lnjmx, w3lnjmy, w3lnje, w4lnirho, w4lnimx, w4lnimy, w4lnie, w4lnjrho, w4lnjmx, w4lnjmy, w4lnje, w1rnirho, w1rnimx, w1rnimy, w1rnie, w1rnjrho, w1rnjmx, w1rnjmy, w1rnje, w2rnirho, w2rnimx, w2rnimy, w2rnie, w2rnjrho, w2rnjmx, w2rnjmy, w2rnje, w3rnirho, w3rnimx, w3rnimy, w3rnie, w3rnjrho, w3rnjmx, w3rnjmy, w3rnje, w4rnirho, w4rnimx, w4rnimy, w4rnie, w4rnjrho, w4rnjmx, w4rnjmy, w4rnje, wlnirho, wlnimx, wlnimy, wlnie, wlnjrho, wlnjmx, wlnjmy, wlnje, wrnirho, wrnimx, wrnimy, wrnie, wrnjrho, wrnjmx, wrnjmy, wrnje, rho_lni, mx_lni, my_lni, e_lni, rho_lnj, mx_lnj, my_lnj, e_lnj, rho_rni, mx_rni, my_rni, e_rni, rho_rnj, mx_rnj, my_rnj, e_rnj, p1lpirho, p1lpimx, p1lpimy, p1lpie, p1lpjrho, p1lpjmx, p1lpjmy, p1lpje, p2lpirho, p2lpimx, p2lpimy, p2lpie, p2lpjrho, p2lpjmx, p2lpjmy, p2lpje, p3lpirho, p3lpimx, p3lpimy, p3lpie, p3lpjrho, p3lpjmx, p3lpjmy, p3lpje, p4lpirho, p4lpimx, p4lpimy, p4lpie, p4lpjrho, p4lpjmx, p4lpjmy, p4lpje, p1rpirho, p1rpimx, p1rpimy, p1rpie, p1rpjrho, p1rpjmx, p1rpjmy, p1rpje, p2rpirho, p2rpimx, p2rpimy, p2rpie, p2rpjrho, p2rpjmx, p2rpjmy, p2rpje, p3rpirho, p3rpimx, p3rpimy, p3rpie, p3rpjrho, p3rpjmx, p3rpjmy, p3rpje, p4rpirho, p4rpimx, p4rpimy, p4rpie, p4rpjrho, p4rpjmx, p4rpjmy, p4rpje, beta1lpirho, beta1lpimx, beta1lpimy, beta1lpie, beta1lpjrho, beta1lpjmx, beta1lpjmy, beta1lpje, beta2lpirho, beta2lpimx, beta2lpimy, beta2lpie, beta2lpjrho, beta2lpjmx, beta2lpjmy, beta2lpje, beta3lpirho, beta3lpimx, beta3lpimy, beta3lpie, beta3lpjrho, beta3lpjmx, beta3lpjmy, beta3lpje, beta4lpirho, beta4lpimx, beta4lpimy, beta4lpie, beta4lpjrho, beta4lpjmx, beta4lpjmy, beta4lpje, beta1rpirho, beta1rpimx, beta1rpimy, beta1rpie, beta1rpjrho, beta1rpjmx, beta1rpjmy, beta1rpje, beta2rpirho, beta2rpimx, beta2rpimy, beta2rpie, beta2rpjrho, beta2rpjmx, beta2rpjmy, beta2rpje, beta3rpirho, beta3rpimx, beta3rpimy, beta3rpie, beta3rpjrho, beta3rpjmx, beta3rpjmy, beta3rpje, beta4rpirho, beta4rpimx, beta4rpimy, beta4rpie, beta4rpjrho, beta4rpjmx, beta4rpjmy, beta4rpje, w1lpirho, w1lpimx, w1lpimy, w1lpie, w1lpjrho, w1lpjmx, w1lpjmy, w1lpje, w2lpirho, w2lpimx, w2lpimy, w2lpie, w2lpjrho, w2lpjmx, w2lpjmy, w2lpje, w3lpirho, w3lpimx, w3lpimy, w3lpie, w3lpjrho, w3lpjmx, w3lpjmy, w3lpje, w4lpirho, w4lpimx, w4lpimy, w4lpie, w4lpjrho, w4lpjmx, w4lpjmy, w4lpje, w1rpirho, w1rpimx, w1rpimy, w1rpie, w1rpjrho, w1rpjmx, w1rpjmy, w1rpje, w2rpirho, w2rpimx, w2rpimy, w2rpie, w2rpjrho, w2rpjmx, w2rpjmy, w2rpje, w3rpirho, w3rpimx, w3rpimy, w3rpie, w3rpjrho, w3rpjmx, w3rpjmy, w3rpje, w4rpirho, w4rpimx, w4rpimy, w4rpie, w4rpjrho, w4rpjmx, w4rpjmy, w4rpje, wlpirho, wlpimx, wlpimy, wlpie, wlpjrho, wlpjmx, wlpjmy, wlpje, wrpirho, wrpimx, wrpimy, wrpie, wrpjrho, wrpjmx, wrpjmy, wrpje, rho_lpi, mx_lpi, my_lpi, e_lpi, rho_lpj, mx_lpj, my_lpj, e_lpj, rho_rpi, mx_rpi, my_rpi, e_rpi, rho_rpj, mx_rpj, my_rpj, e_rpj, P_lpi, P_lpj, P_lni, P_lnj, P_rpi, P_rpj, P_rni, P_rnj, cy_p, cx_p, c_p, Speedirho, Speedimx, Speedimy, Speedie, Speedjrho, Speedjmx, Speedjmy, Speedje, Speedpirho, Speedpimx, Speedpimy, Speedpie, Speedpjrho, Speedpjmx, Speedpjmy, Speedpje, Speednirho, Speednimx, Speednimy, Speednie, Speednjrho, Speednjmx, Speednjmy, Speednje, Fluxlpimx, Fluxlpimy, Fluxlpie, Fluxlpjmx, Fluxlpjmy, Fluxlpje, Fluxlnimx, Fluxlnimy, Fluxlnie, Fluxlnjmx, Fluxlnjmy, Fluxlnje, Fluxrpimx, Fluxrpimy, Fluxrpie, Fluxrpjmx, Fluxrpjmy, Fluxrpje, Fluxrnimx, Fluxrnimy, Fluxrnie, Fluxrnjmx, Fluxrnjmy, Fluxrnje, RHS_rho, RHS_mx, RHS_my, RHS_e;
	
		//Get the dimensions of the patch
		const hier::Index boxfirst = patch->getBox().lower();
		const hier::Index boxlast = patch->getBox().upper();
	
		//Get delta spaces into an array. dx, dy, dz.
		std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
		const double* dx  = patch_geom->getDx();
	
		//Auxiliary definitions
		int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
		int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
		for(int j = 0; j < jlast; j++) {
			for(int i = 0; i < ilast; i++) {
				if ((i + 4 < ilast && i - 4 >= 0 && j + 4 < jlast && j - 4 >= 0)) {
					p1lnirho = lor1(vector(rho_p, i - 4, j), vector(rho_p, i - 3, j), vector(rho_p, i - 2, j), vector(rho_p, i - 1, j), dx, simPlat_dt, ilast, jlast);
					p1lnimx = lor1(vector(mx_p, i - 4, j), vector(mx_p, i - 3, j), vector(mx_p, i - 2, j), vector(mx_p, i - 1, j), dx, simPlat_dt, ilast, jlast);
					p1lnimy = lor1(vector(my_p, i - 4, j), vector(my_p, i - 3, j), vector(my_p, i - 2, j), vector(my_p, i - 1, j), dx, simPlat_dt, ilast, jlast);
					p1lnie = lor1(vector(e_p, i - 4, j), vector(e_p, i - 3, j), vector(e_p, i - 2, j), vector(e_p, i - 1, j), dx, simPlat_dt, ilast, jlast);
					p1lnjrho = lor1(vector(rho_p, i, j - 4), vector(rho_p, i, j - 3), vector(rho_p, i, j - 2), vector(rho_p, i, j - 1), dx, simPlat_dt, ilast, jlast);
					p1lnjmx = lor1(vector(mx_p, i, j - 4), vector(mx_p, i, j - 3), vector(mx_p, i, j - 2), vector(mx_p, i, j - 1), dx, simPlat_dt, ilast, jlast);
					p1lnjmy = lor1(vector(my_p, i, j - 4), vector(my_p, i, j - 3), vector(my_p, i, j - 2), vector(my_p, i, j - 1), dx, simPlat_dt, ilast, jlast);
					p1lnje = lor1(vector(e_p, i, j - 4), vector(e_p, i, j - 3), vector(e_p, i, j - 2), vector(e_p, i, j - 1), dx, simPlat_dt, ilast, jlast);
					p2lnirho = lor2(vector(rho_p, i - 3, j), vector(rho_p, i - 2, j), vector(rho_p, i - 1, j), vector(rho_p, i, j), dx, simPlat_dt, ilast, jlast);
					p2lnimx = lor2(vector(mx_p, i - 3, j), vector(mx_p, i - 2, j), vector(mx_p, i - 1, j), vector(mx_p, i, j), dx, simPlat_dt, ilast, jlast);
					p2lnimy = lor2(vector(my_p, i - 3, j), vector(my_p, i - 2, j), vector(my_p, i - 1, j), vector(my_p, i, j), dx, simPlat_dt, ilast, jlast);
					p2lnie = lor2(vector(e_p, i - 3, j), vector(e_p, i - 2, j), vector(e_p, i - 1, j), vector(e_p, i, j), dx, simPlat_dt, ilast, jlast);
					p2lnjrho = lor2(vector(rho_p, i, j - 3), vector(rho_p, i, j - 2), vector(rho_p, i, j - 1), vector(rho_p, i, j), dx, simPlat_dt, ilast, jlast);
					p2lnjmx = lor2(vector(mx_p, i, j - 3), vector(mx_p, i, j - 2), vector(mx_p, i, j - 1), vector(mx_p, i, j), dx, simPlat_dt, ilast, jlast);
					p2lnjmy = lor2(vector(my_p, i, j - 3), vector(my_p, i, j - 2), vector(my_p, i, j - 1), vector(my_p, i, j), dx, simPlat_dt, ilast, jlast);
					p2lnje = lor2(vector(e_p, i, j - 3), vector(e_p, i, j - 2), vector(e_p, i, j - 1), vector(e_p, i, j), dx, simPlat_dt, ilast, jlast);
					p3lnirho = lor3(vector(rho_p, i - 2, j), vector(rho_p, i - 1, j), vector(rho_p, i, j), vector(rho_p, i + 1, j), dx, simPlat_dt, ilast, jlast);
					p3lnimx = lor3(vector(mx_p, i - 2, j), vector(mx_p, i - 1, j), vector(mx_p, i, j), vector(mx_p, i + 1, j), dx, simPlat_dt, ilast, jlast);
					p3lnimy = lor3(vector(my_p, i - 2, j), vector(my_p, i - 1, j), vector(my_p, i, j), vector(my_p, i + 1, j), dx, simPlat_dt, ilast, jlast);
					p3lnie = lor3(vector(e_p, i - 2, j), vector(e_p, i - 1, j), vector(e_p, i, j), vector(e_p, i + 1, j), dx, simPlat_dt, ilast, jlast);
					p3lnjrho = lor3(vector(rho_p, i, j - 2), vector(rho_p, i, j - 1), vector(rho_p, i, j), vector(rho_p, i, j + 1), dx, simPlat_dt, ilast, jlast);
					p3lnjmx = lor3(vector(mx_p, i, j - 2), vector(mx_p, i, j - 1), vector(mx_p, i, j), vector(mx_p, i, j + 1), dx, simPlat_dt, ilast, jlast);
					p3lnjmy = lor3(vector(my_p, i, j - 2), vector(my_p, i, j - 1), vector(my_p, i, j), vector(my_p, i, j + 1), dx, simPlat_dt, ilast, jlast);
					p3lnje = lor3(vector(e_p, i, j - 2), vector(e_p, i, j - 1), vector(e_p, i, j), vector(e_p, i, j + 1), dx, simPlat_dt, ilast, jlast);
					p4lnirho = lor4(vector(rho_p, i - 1, j), vector(rho_p, i, j), vector(rho_p, i + 1, j), vector(rho_p, i + 2, j), dx, simPlat_dt, ilast, jlast);
					p4lnimx = lor4(vector(mx_p, i - 1, j), vector(mx_p, i, j), vector(mx_p, i + 1, j), vector(mx_p, i + 2, j), dx, simPlat_dt, ilast, jlast);
					p4lnimy = lor4(vector(my_p, i - 1, j), vector(my_p, i, j), vector(my_p, i + 1, j), vector(my_p, i + 2, j), dx, simPlat_dt, ilast, jlast);
					p4lnie = lor4(vector(e_p, i - 1, j), vector(e_p, i, j), vector(e_p, i + 1, j), vector(e_p, i + 2, j), dx, simPlat_dt, ilast, jlast);
					p4lnjrho = lor4(vector(rho_p, i, j - 1), vector(rho_p, i, j), vector(rho_p, i, j + 1), vector(rho_p, i, j + 2), dx, simPlat_dt, ilast, jlast);
					p4lnjmx = lor4(vector(mx_p, i, j - 1), vector(mx_p, i, j), vector(mx_p, i, j + 1), vector(mx_p, i, j + 2), dx, simPlat_dt, ilast, jlast);
					p4lnjmy = lor4(vector(my_p, i, j - 1), vector(my_p, i, j), vector(my_p, i, j + 1), vector(my_p, i, j + 2), dx, simPlat_dt, ilast, jlast);
					p4lnje = lor4(vector(e_p, i, j - 1), vector(e_p, i, j), vector(e_p, i, j + 1), vector(e_p, i, j + 2), dx, simPlat_dt, ilast, jlast);
					p1rnirho = lor1(vector(rho_p, i + 3, j), vector(rho_p, i + 2, j), vector(rho_p, i + 1, j), vector(rho_p, i, j), dx, simPlat_dt, ilast, jlast);
					p1rnimx = lor1(vector(mx_p, i + 3, j), vector(mx_p, i + 2, j), vector(mx_p, i + 1, j), vector(mx_p, i, j), dx, simPlat_dt, ilast, jlast);
					p1rnimy = lor1(vector(my_p, i + 3, j), vector(my_p, i + 2, j), vector(my_p, i + 1, j), vector(my_p, i, j), dx, simPlat_dt, ilast, jlast);
					p1rnie = lor1(vector(e_p, i + 3, j), vector(e_p, i + 2, j), vector(e_p, i + 1, j), vector(e_p, i, j), dx, simPlat_dt, ilast, jlast);
					p1rnjrho = lor1(vector(rho_p, i, j + 3), vector(rho_p, i, j + 2), vector(rho_p, i, j + 1), vector(rho_p, i, j), dx, simPlat_dt, ilast, jlast);
					p1rnjmx = lor1(vector(mx_p, i, j + 3), vector(mx_p, i, j + 2), vector(mx_p, i, j + 1), vector(mx_p, i, j), dx, simPlat_dt, ilast, jlast);
					p1rnjmy = lor1(vector(my_p, i, j + 3), vector(my_p, i, j + 2), vector(my_p, i, j + 1), vector(my_p, i, j), dx, simPlat_dt, ilast, jlast);
					p1rnje = lor1(vector(e_p, i, j + 3), vector(e_p, i, j + 2), vector(e_p, i, j + 1), vector(e_p, i, j), dx, simPlat_dt, ilast, jlast);
					p2rnirho = lor2(vector(rho_p, i + 2, j), vector(rho_p, i + 1, j), vector(rho_p, i, j), vector(rho_p, i - 1, j), dx, simPlat_dt, ilast, jlast);
					p2rnimx = lor2(vector(mx_p, i + 2, j), vector(mx_p, i + 1, j), vector(mx_p, i, j), vector(mx_p, i - 1, j), dx, simPlat_dt, ilast, jlast);
					p2rnimy = lor2(vector(my_p, i + 2, j), vector(my_p, i + 1, j), vector(my_p, i, j), vector(my_p, i - 1, j), dx, simPlat_dt, ilast, jlast);
					p2rnie = lor2(vector(e_p, i + 2, j), vector(e_p, i + 1, j), vector(e_p, i, j), vector(e_p, i - 1, j), dx, simPlat_dt, ilast, jlast);
					p2rnjrho = lor2(vector(rho_p, i, j + 2), vector(rho_p, i, j + 1), vector(rho_p, i, j), vector(rho_p, i, j - 1), dx, simPlat_dt, ilast, jlast);
					p2rnjmx = lor2(vector(mx_p, i, j + 2), vector(mx_p, i, j + 1), vector(mx_p, i, j), vector(mx_p, i, j - 1), dx, simPlat_dt, ilast, jlast);
					p2rnjmy = lor2(vector(my_p, i, j + 2), vector(my_p, i, j + 1), vector(my_p, i, j), vector(my_p, i, j - 1), dx, simPlat_dt, ilast, jlast);
					p2rnje = lor2(vector(e_p, i, j + 2), vector(e_p, i, j + 1), vector(e_p, i, j), vector(e_p, i, j - 1), dx, simPlat_dt, ilast, jlast);
					p3rnirho = lor3(vector(rho_p, i + 1, j), vector(rho_p, i, j), vector(rho_p, i - 1, j), vector(rho_p, i - 2, j), dx, simPlat_dt, ilast, jlast);
					p3rnimx = lor3(vector(mx_p, i + 1, j), vector(mx_p, i, j), vector(mx_p, i - 1, j), vector(mx_p, i - 2, j), dx, simPlat_dt, ilast, jlast);
					p3rnimy = lor3(vector(my_p, i + 1, j), vector(my_p, i, j), vector(my_p, i - 1, j), vector(my_p, i - 2, j), dx, simPlat_dt, ilast, jlast);
					p3rnie = lor3(vector(e_p, i + 1, j), vector(e_p, i, j), vector(e_p, i - 1, j), vector(e_p, i - 2, j), dx, simPlat_dt, ilast, jlast);
					p3rnjrho = lor3(vector(rho_p, i, j + 1), vector(rho_p, i, j), vector(rho_p, i, j - 1), vector(rho_p, i, j - 2), dx, simPlat_dt, ilast, jlast);
					p3rnjmx = lor3(vector(mx_p, i, j + 1), vector(mx_p, i, j), vector(mx_p, i, j - 1), vector(mx_p, i, j - 2), dx, simPlat_dt, ilast, jlast);
					p3rnjmy = lor3(vector(my_p, i, j + 1), vector(my_p, i, j), vector(my_p, i, j - 1), vector(my_p, i, j - 2), dx, simPlat_dt, ilast, jlast);
					p3rnje = lor3(vector(e_p, i, j + 1), vector(e_p, i, j), vector(e_p, i, j - 1), vector(e_p, i, j - 2), dx, simPlat_dt, ilast, jlast);
					p4rnirho = lor4(vector(rho_p, i, j), vector(rho_p, i - 1, j), vector(rho_p, i - 2, j), vector(rho_p, i - 3, j), dx, simPlat_dt, ilast, jlast);
					p4rnimx = lor4(vector(mx_p, i, j), vector(mx_p, i - 1, j), vector(mx_p, i - 2, j), vector(mx_p, i - 3, j), dx, simPlat_dt, ilast, jlast);
					p4rnimy = lor4(vector(my_p, i, j), vector(my_p, i - 1, j), vector(my_p, i - 2, j), vector(my_p, i - 3, j), dx, simPlat_dt, ilast, jlast);
					p4rnie = lor4(vector(e_p, i, j), vector(e_p, i - 1, j), vector(e_p, i - 2, j), vector(e_p, i - 3, j), dx, simPlat_dt, ilast, jlast);
					p4rnjrho = lor4(vector(rho_p, i, j), vector(rho_p, i, j - 1), vector(rho_p, i, j - 2), vector(rho_p, i, j - 3), dx, simPlat_dt, ilast, jlast);
					p4rnjmx = lor4(vector(mx_p, i, j), vector(mx_p, i, j - 1), vector(mx_p, i, j - 2), vector(mx_p, i, j - 3), dx, simPlat_dt, ilast, jlast);
					p4rnjmy = lor4(vector(my_p, i, j), vector(my_p, i, j - 1), vector(my_p, i, j - 2), vector(my_p, i, j - 3), dx, simPlat_dt, ilast, jlast);
					p4rnje = lor4(vector(e_p, i, j), vector(e_p, i, j - 1), vector(e_p, i, j - 2), vector(e_p, i, j - 3), dx, simPlat_dt, ilast, jlast);
					beta1lnirho = smooth1(vector(rho_p, i - 4, j), vector(rho_p, i - 3, j), vector(rho_p, i - 2, j), vector(rho_p, i - 1, j), dx, simPlat_dt, ilast, jlast);
					beta1lnimx = smooth1(vector(mx_p, i - 4, j), vector(mx_p, i - 3, j), vector(mx_p, i - 2, j), vector(mx_p, i - 1, j), dx, simPlat_dt, ilast, jlast);
					beta1lnimy = smooth1(vector(my_p, i - 4, j), vector(my_p, i - 3, j), vector(my_p, i - 2, j), vector(my_p, i - 1, j), dx, simPlat_dt, ilast, jlast);
					beta1lnie = smooth1(vector(e_p, i - 4, j), vector(e_p, i - 3, j), vector(e_p, i - 2, j), vector(e_p, i - 1, j), dx, simPlat_dt, ilast, jlast);
					beta1lnjrho = smooth1(vector(rho_p, i, j - 4), vector(rho_p, i, j - 3), vector(rho_p, i, j - 2), vector(rho_p, i, j - 1), dx, simPlat_dt, ilast, jlast);
					beta1lnjmx = smooth1(vector(mx_p, i, j - 4), vector(mx_p, i, j - 3), vector(mx_p, i, j - 2), vector(mx_p, i, j - 1), dx, simPlat_dt, ilast, jlast);
					beta1lnjmy = smooth1(vector(my_p, i, j - 4), vector(my_p, i, j - 3), vector(my_p, i, j - 2), vector(my_p, i, j - 1), dx, simPlat_dt, ilast, jlast);
					beta1lnje = smooth1(vector(e_p, i, j - 4), vector(e_p, i, j - 3), vector(e_p, i, j - 2), vector(e_p, i, j - 1), dx, simPlat_dt, ilast, jlast);
					beta2lnirho = smooth2(vector(rho_p, i - 3, j), vector(rho_p, i - 2, j), vector(rho_p, i - 1, j), vector(rho_p, i, j), dx, simPlat_dt, ilast, jlast);
					beta2lnimx = smooth2(vector(mx_p, i - 3, j), vector(mx_p, i - 2, j), vector(mx_p, i - 1, j), vector(mx_p, i, j), dx, simPlat_dt, ilast, jlast);
					beta2lnimy = smooth2(vector(my_p, i - 3, j), vector(my_p, i - 2, j), vector(my_p, i - 1, j), vector(my_p, i, j), dx, simPlat_dt, ilast, jlast);
					beta2lnie = smooth2(vector(e_p, i - 3, j), vector(e_p, i - 2, j), vector(e_p, i - 1, j), vector(e_p, i, j), dx, simPlat_dt, ilast, jlast);
					beta2lnjrho = smooth2(vector(rho_p, i, j - 3), vector(rho_p, i, j - 2), vector(rho_p, i, j - 1), vector(rho_p, i, j), dx, simPlat_dt, ilast, jlast);
					beta2lnjmx = smooth2(vector(mx_p, i, j - 3), vector(mx_p, i, j - 2), vector(mx_p, i, j - 1), vector(mx_p, i, j), dx, simPlat_dt, ilast, jlast);
					beta2lnjmy = smooth2(vector(my_p, i, j - 3), vector(my_p, i, j - 2), vector(my_p, i, j - 1), vector(my_p, i, j), dx, simPlat_dt, ilast, jlast);
					beta2lnje = smooth2(vector(e_p, i, j - 3), vector(e_p, i, j - 2), vector(e_p, i, j - 1), vector(e_p, i, j), dx, simPlat_dt, ilast, jlast);
					beta3lnirho = smooth3(vector(rho_p, i - 2, j), vector(rho_p, i - 1, j), vector(rho_p, i, j), vector(rho_p, i + 1, j), dx, simPlat_dt, ilast, jlast);
					beta3lnimx = smooth3(vector(mx_p, i - 2, j), vector(mx_p, i - 1, j), vector(mx_p, i, j), vector(mx_p, i + 1, j), dx, simPlat_dt, ilast, jlast);
					beta3lnimy = smooth3(vector(my_p, i - 2, j), vector(my_p, i - 1, j), vector(my_p, i, j), vector(my_p, i + 1, j), dx, simPlat_dt, ilast, jlast);
					beta3lnie = smooth3(vector(e_p, i - 2, j), vector(e_p, i - 1, j), vector(e_p, i, j), vector(e_p, i + 1, j), dx, simPlat_dt, ilast, jlast);
					beta3lnjrho = smooth3(vector(rho_p, i, j - 2), vector(rho_p, i, j - 1), vector(rho_p, i, j), vector(rho_p, i, j + 1), dx, simPlat_dt, ilast, jlast);
					beta3lnjmx = smooth3(vector(mx_p, i, j - 2), vector(mx_p, i, j - 1), vector(mx_p, i, j), vector(mx_p, i, j + 1), dx, simPlat_dt, ilast, jlast);
					beta3lnjmy = smooth3(vector(my_p, i, j - 2), vector(my_p, i, j - 1), vector(my_p, i, j), vector(my_p, i, j + 1), dx, simPlat_dt, ilast, jlast);
					beta3lnje = smooth3(vector(e_p, i, j - 2), vector(e_p, i, j - 1), vector(e_p, i, j), vector(e_p, i, j + 1), dx, simPlat_dt, ilast, jlast);
					beta4lnirho = smooth4(vector(rho_p, i - 1, j), vector(rho_p, i, j), vector(rho_p, i + 1, j), vector(rho_p, i + 2, j), dx, simPlat_dt, ilast, jlast);
					beta4lnimx = smooth4(vector(mx_p, i - 1, j), vector(mx_p, i, j), vector(mx_p, i + 1, j), vector(mx_p, i + 2, j), dx, simPlat_dt, ilast, jlast);
					beta4lnimy = smooth4(vector(my_p, i - 1, j), vector(my_p, i, j), vector(my_p, i + 1, j), vector(my_p, i + 2, j), dx, simPlat_dt, ilast, jlast);
					beta4lnie = smooth4(vector(e_p, i - 1, j), vector(e_p, i, j), vector(e_p, i + 1, j), vector(e_p, i + 2, j), dx, simPlat_dt, ilast, jlast);
					beta4lnjrho = smooth4(vector(rho_p, i, j - 1), vector(rho_p, i, j), vector(rho_p, i, j + 1), vector(rho_p, i, j + 2), dx, simPlat_dt, ilast, jlast);
					beta4lnjmx = smooth4(vector(mx_p, i, j - 1), vector(mx_p, i, j), vector(mx_p, i, j + 1), vector(mx_p, i, j + 2), dx, simPlat_dt, ilast, jlast);
					beta4lnjmy = smooth4(vector(my_p, i, j - 1), vector(my_p, i, j), vector(my_p, i, j + 1), vector(my_p, i, j + 2), dx, simPlat_dt, ilast, jlast);
					beta4lnje = smooth4(vector(e_p, i, j - 1), vector(e_p, i, j), vector(e_p, i, j + 1), vector(e_p, i, j + 2), dx, simPlat_dt, ilast, jlast);
					beta1rnirho = smooth1(vector(rho_p, i + 3, j), vector(rho_p, i + 2, j), vector(rho_p, i + 1, j), vector(rho_p, i, j), dx, simPlat_dt, ilast, jlast);
					beta1rnimx = smooth1(vector(mx_p, i + 3, j), vector(mx_p, i + 2, j), vector(mx_p, i + 1, j), vector(mx_p, i, j), dx, simPlat_dt, ilast, jlast);
					beta1rnimy = smooth1(vector(my_p, i + 3, j), vector(my_p, i + 2, j), vector(my_p, i + 1, j), vector(my_p, i, j), dx, simPlat_dt, ilast, jlast);
					beta1rnie = smooth1(vector(e_p, i + 3, j), vector(e_p, i + 2, j), vector(e_p, i + 1, j), vector(e_p, i, j), dx, simPlat_dt, ilast, jlast);
					beta1rnjrho = smooth1(vector(rho_p, i, j + 3), vector(rho_p, i, j + 2), vector(rho_p, i, j + 1), vector(rho_p, i, j), dx, simPlat_dt, ilast, jlast);
					beta1rnjmx = smooth1(vector(mx_p, i, j + 3), vector(mx_p, i, j + 2), vector(mx_p, i, j + 1), vector(mx_p, i, j), dx, simPlat_dt, ilast, jlast);
					beta1rnjmy = smooth1(vector(my_p, i, j + 3), vector(my_p, i, j + 2), vector(my_p, i, j + 1), vector(my_p, i, j), dx, simPlat_dt, ilast, jlast);
					beta1rnje = smooth1(vector(e_p, i, j + 3), vector(e_p, i, j + 2), vector(e_p, i, j + 1), vector(e_p, i, j), dx, simPlat_dt, ilast, jlast);
					beta2rnirho = smooth2(vector(rho_p, i + 2, j), vector(rho_p, i + 1, j), vector(rho_p, i, j), vector(rho_p, i - 1, j), dx, simPlat_dt, ilast, jlast);
					beta2rnimx = smooth2(vector(mx_p, i + 2, j), vector(mx_p, i + 1, j), vector(mx_p, i, j), vector(mx_p, i - 1, j), dx, simPlat_dt, ilast, jlast);
					beta2rnimy = smooth2(vector(my_p, i + 2, j), vector(my_p, i + 1, j), vector(my_p, i, j), vector(my_p, i - 1, j), dx, simPlat_dt, ilast, jlast);
					beta2rnie = smooth2(vector(e_p, i + 2, j), vector(e_p, i + 1, j), vector(e_p, i, j), vector(e_p, i - 1, j), dx, simPlat_dt, ilast, jlast);
					beta2rnjrho = smooth2(vector(rho_p, i, j + 2), vector(rho_p, i, j + 1), vector(rho_p, i, j), vector(rho_p, i, j - 1), dx, simPlat_dt, ilast, jlast);
					beta2rnjmx = smooth2(vector(mx_p, i, j + 2), vector(mx_p, i, j + 1), vector(mx_p, i, j), vector(mx_p, i, j - 1), dx, simPlat_dt, ilast, jlast);
					beta2rnjmy = smooth2(vector(my_p, i, j + 2), vector(my_p, i, j + 1), vector(my_p, i, j), vector(my_p, i, j - 1), dx, simPlat_dt, ilast, jlast);
					beta2rnje = smooth2(vector(e_p, i, j + 2), vector(e_p, i, j + 1), vector(e_p, i, j), vector(e_p, i, j - 1), dx, simPlat_dt, ilast, jlast);
					beta3rnirho = smooth3(vector(rho_p, i + 1, j), vector(rho_p, i, j), vector(rho_p, i - 1, j), vector(rho_p, i - 2, j), dx, simPlat_dt, ilast, jlast);
					beta3rnimx = smooth3(vector(mx_p, i + 1, j), vector(mx_p, i, j), vector(mx_p, i - 1, j), vector(mx_p, i - 2, j), dx, simPlat_dt, ilast, jlast);
					beta3rnimy = smooth3(vector(my_p, i + 1, j), vector(my_p, i, j), vector(my_p, i - 1, j), vector(my_p, i - 2, j), dx, simPlat_dt, ilast, jlast);
					beta3rnie = smooth3(vector(e_p, i + 1, j), vector(e_p, i, j), vector(e_p, i - 1, j), vector(e_p, i - 2, j), dx, simPlat_dt, ilast, jlast);
					beta3rnjrho = smooth3(vector(rho_p, i, j + 1), vector(rho_p, i, j), vector(rho_p, i, j - 1), vector(rho_p, i, j - 2), dx, simPlat_dt, ilast, jlast);
					beta3rnjmx = smooth3(vector(mx_p, i, j + 1), vector(mx_p, i, j), vector(mx_p, i, j - 1), vector(mx_p, i, j - 2), dx, simPlat_dt, ilast, jlast);
					beta3rnjmy = smooth3(vector(my_p, i, j + 1), vector(my_p, i, j), vector(my_p, i, j - 1), vector(my_p, i, j - 2), dx, simPlat_dt, ilast, jlast);
					beta3rnje = smooth3(vector(e_p, i, j + 1), vector(e_p, i, j), vector(e_p, i, j - 1), vector(e_p, i, j - 2), dx, simPlat_dt, ilast, jlast);
					beta4rnirho = smooth4(vector(rho_p, i, j), vector(rho_p, i - 1, j), vector(rho_p, i - 2, j), vector(rho_p, i - 3, j), dx, simPlat_dt, ilast, jlast);
					beta4rnimx = smooth4(vector(mx_p, i, j), vector(mx_p, i - 1, j), vector(mx_p, i - 2, j), vector(mx_p, i - 3, j), dx, simPlat_dt, ilast, jlast);
					beta4rnimy = smooth4(vector(my_p, i, j), vector(my_p, i - 1, j), vector(my_p, i - 2, j), vector(my_p, i - 3, j), dx, simPlat_dt, ilast, jlast);
					beta4rnie = smooth4(vector(e_p, i, j), vector(e_p, i - 1, j), vector(e_p, i - 2, j), vector(e_p, i - 3, j), dx, simPlat_dt, ilast, jlast);
					beta4rnjrho = smooth4(vector(rho_p, i, j), vector(rho_p, i, j - 1), vector(rho_p, i, j - 2), vector(rho_p, i, j - 3), dx, simPlat_dt, ilast, jlast);
					beta4rnjmx = smooth4(vector(mx_p, i, j), vector(mx_p, i, j - 1), vector(mx_p, i, j - 2), vector(mx_p, i, j - 3), dx, simPlat_dt, ilast, jlast);
					beta4rnjmy = smooth4(vector(my_p, i, j), vector(my_p, i, j - 1), vector(my_p, i, j - 2), vector(my_p, i, j - 3), dx, simPlat_dt, ilast, jlast);
					beta4rnje = smooth4(vector(e_p, i, j), vector(e_p, i, j - 1), vector(e_p, i, j - 2), vector(e_p, i, j - 3), dx, simPlat_dt, ilast, jlast);
					w1lnirho = nlw1(beta1lnirho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1lnimx = nlw1(beta1lnimx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1lnimy = nlw1(beta1lnimy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1lnie = nlw1(beta1lnie, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1lnjrho = nlw1(beta1lnjrho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1lnjmx = nlw1(beta1lnjmx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1lnjmy = nlw1(beta1lnjmy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1lnje = nlw1(beta1lnje, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2lnirho = nlw2(beta2lnirho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2lnimx = nlw2(beta2lnimx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2lnimy = nlw2(beta2lnimy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2lnie = nlw2(beta2lnie, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2lnjrho = nlw2(beta2lnjrho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2lnjmx = nlw2(beta2lnjmx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2lnjmy = nlw2(beta2lnjmy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2lnje = nlw2(beta2lnje, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3lnirho = nlw3(beta3lnirho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3lnimx = nlw3(beta3lnimx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3lnimy = nlw3(beta3lnimy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3lnie = nlw3(beta3lnie, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3lnjrho = nlw3(beta3lnjrho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3lnjmx = nlw3(beta3lnjmx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3lnjmy = nlw3(beta3lnjmy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3lnje = nlw3(beta3lnje, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4lnirho = nlw4(beta4lnirho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4lnimx = nlw4(beta4lnimx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4lnimy = nlw4(beta4lnimy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4lnie = nlw4(beta4lnie, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4lnjrho = nlw4(beta4lnjrho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4lnjmx = nlw4(beta4lnjmx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4lnjmy = nlw4(beta4lnjmy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4lnje = nlw4(beta4lnje, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1rnirho = nlw1(beta1rnirho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1rnimx = nlw1(beta1rnimx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1rnimy = nlw1(beta1rnimy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1rnie = nlw1(beta1rnie, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1rnjrho = nlw1(beta1rnjrho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1rnjmx = nlw1(beta1rnjmx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1rnjmy = nlw1(beta1rnjmy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1rnje = nlw1(beta1rnje, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2rnirho = nlw2(beta2rnirho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2rnimx = nlw2(beta2rnimx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2rnimy = nlw2(beta2rnimy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2rnie = nlw2(beta2rnie, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2rnjrho = nlw2(beta2rnjrho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2rnjmx = nlw2(beta2rnjmx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2rnjmy = nlw2(beta2rnjmy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2rnje = nlw2(beta2rnje, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3rnirho = nlw3(beta3rnirho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3rnimx = nlw3(beta3rnimx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3rnimy = nlw3(beta3rnimy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3rnie = nlw3(beta3rnie, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3rnjrho = nlw3(beta3rnjrho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3rnjmx = nlw3(beta3rnjmx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3rnjmy = nlw3(beta3rnjmy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3rnje = nlw3(beta3rnje, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4rnirho = nlw4(beta4rnirho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4rnimx = nlw4(beta4rnimx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4rnimy = nlw4(beta4rnimy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4rnie = nlw4(beta4rnie, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4rnjrho = nlw4(beta4rnjrho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4rnjmx = nlw4(beta4rnjmx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4rnjmy = nlw4(beta4rnjmy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4rnje = nlw4(beta4rnje, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					wlnirho = w1lnirho + w2lnirho + w3lnirho + w4lnirho;
					wlnimx = w1lnimx + w2lnimx + w3lnimx + w4lnimx;
					wlnimy = w1lnimy + w2lnimy + w3lnimy + w4lnimy;
					wlnie = w1lnie + w2lnie + w3lnie + w4lnie;
					wlnjrho = w1lnjrho + w2lnjrho + w3lnjrho + w4lnjrho;
					wlnjmx = w1lnjmx + w2lnjmx + w3lnjmx + w4lnjmx;
					wlnjmy = w1lnjmy + w2lnjmy + w3lnjmy + w4lnjmy;
					wlnje = w1lnje + w2lnje + w3lnje + w4lnje;
					wrnirho = w1rnirho + w2rnirho + w3rnirho + w4rnirho;
					wrnimx = w1rnimx + w2rnimx + w3rnimx + w4rnimx;
					wrnimy = w1rnimy + w2rnimy + w3rnimy + w4rnimy;
					wrnie = w1rnie + w2rnie + w3rnie + w4rnie;
					wrnjrho = w1rnjrho + w2rnjrho + w3rnjrho + w4rnjrho;
					wrnjmx = w1rnjmx + w2rnjmx + w3rnjmx + w4rnjmx;
					wrnjmy = w1rnjmy + w2rnjmy + w3rnjmy + w4rnjmy;
					wrnje = w1rnje + w2rnje + w3rnje + w4rnje;
					w1lnirho = w1lnirho / wlnirho;
					w1lnimx = w1lnimx / wlnimx;
					w1lnimy = w1lnimy / wlnimy;
					w1lnie = w1lnie / wlnie;
					w1lnjrho = w1lnjrho / wlnjrho;
					w1lnjmx = w1lnjmx / wlnjmx;
					w1lnjmy = w1lnjmy / wlnjmy;
					w1lnje = w1lnje / wlnje;
					w2lnirho = w2lnirho / wlnirho;
					w2lnimx = w2lnimx / wlnimx;
					w2lnimy = w2lnimy / wlnimy;
					w2lnie = w2lnie / wlnie;
					w2lnjrho = w2lnjrho / wlnjrho;
					w2lnjmx = w2lnjmx / wlnjmx;
					w2lnjmy = w2lnjmy / wlnjmy;
					w2lnje = w2lnje / wlnje;
					w3lnirho = w3lnirho / wlnirho;
					w3lnimx = w3lnimx / wlnimx;
					w3lnimy = w3lnimy / wlnimy;
					w3lnie = w3lnie / wlnie;
					w3lnjrho = w3lnjrho / wlnjrho;
					w3lnjmx = w3lnjmx / wlnjmx;
					w3lnjmy = w3lnjmy / wlnjmy;
					w3lnje = w3lnje / wlnje;
					w4lnirho = w4lnirho / wlnirho;
					w4lnimx = w4lnimx / wlnimx;
					w4lnimy = w4lnimy / wlnimy;
					w4lnie = w4lnie / wlnie;
					w4lnjrho = w4lnjrho / wlnjrho;
					w4lnjmx = w4lnjmx / wlnjmx;
					w4lnjmy = w4lnjmy / wlnjmy;
					w4lnje = w4lnje / wlnje;
					w1rnirho = w1rnirho / wrnirho;
					w1rnimx = w1rnimx / wrnimx;
					w1rnimy = w1rnimy / wrnimy;
					w1rnie = w1rnie / wrnie;
					w1rnjrho = w1rnjrho / wrnjrho;
					w1rnjmx = w1rnjmx / wrnjmx;
					w1rnjmy = w1rnjmy / wrnjmy;
					w1rnje = w1rnje / wrnje;
					w2rnirho = w2rnirho / wrnirho;
					w2rnimx = w2rnimx / wrnimx;
					w2rnimy = w2rnimy / wrnimy;
					w2rnie = w2rnie / wrnie;
					w2rnjrho = w2rnjrho / wrnjrho;
					w2rnjmx = w2rnjmx / wrnjmx;
					w2rnjmy = w2rnjmy / wrnjmy;
					w2rnje = w2rnje / wrnje;
					w3rnirho = w3rnirho / wrnirho;
					w3rnimx = w3rnimx / wrnimx;
					w3rnimy = w3rnimy / wrnimy;
					w3rnie = w3rnie / wrnie;
					w3rnjrho = w3rnjrho / wrnjrho;
					w3rnjmx = w3rnjmx / wrnjmx;
					w3rnjmy = w3rnjmy / wrnjmy;
					w3rnje = w3rnje / wrnje;
					w4rnirho = w4rnirho / wrnirho;
					w4rnimx = w4rnimx / wrnimx;
					w4rnimy = w4rnimy / wrnimy;
					w4rnie = w4rnie / wrnie;
					w4rnjrho = w4rnjrho / wrnjrho;
					w4rnjmx = w4rnjmx / wrnjmx;
					w4rnjmy = w4rnjmy / wrnjmy;
					w4rnje = w4rnje / wrnje;
					rho_lni = weno(w1lnirho, w2lnirho, w3lnirho, w4lnirho, p1lnirho, p2lnirho, p3lnirho, p4lnirho, dx, simPlat_dt, ilast, jlast);
					mx_lni = weno(w1lnimx, w2lnimx, w3lnimx, w4lnimx, p1lnimx, p2lnimx, p3lnimx, p4lnimx, dx, simPlat_dt, ilast, jlast);
					my_lni = weno(w1lnimy, w2lnimy, w3lnimy, w4lnimy, p1lnimy, p2lnimy, p3lnimy, p4lnimy, dx, simPlat_dt, ilast, jlast);
					e_lni = weno(w1lnie, w2lnie, w3lnie, w4lnie, p1lnie, p2lnie, p3lnie, p4lnie, dx, simPlat_dt, ilast, jlast);
					rho_lnj = weno(w1lnjrho, w2lnjrho, w3lnjrho, w4lnjrho, p1lnjrho, p2lnjrho, p3lnjrho, p4lnjrho, dx, simPlat_dt, ilast, jlast);
					mx_lnj = weno(w1lnjmx, w2lnjmx, w3lnjmx, w4lnjmx, p1lnjmx, p2lnjmx, p3lnjmx, p4lnjmx, dx, simPlat_dt, ilast, jlast);
					my_lnj = weno(w1lnjmy, w2lnjmy, w3lnjmy, w4lnjmy, p1lnjmy, p2lnjmy, p3lnjmy, p4lnjmy, dx, simPlat_dt, ilast, jlast);
					e_lnj = weno(w1lnje, w2lnje, w3lnje, w4lnje, p1lnje, p2lnje, p3lnje, p4lnje, dx, simPlat_dt, ilast, jlast);
					rho_rni = weno(w1rnirho, w2rnirho, w3rnirho, w4rnirho, p1rnirho, p2rnirho, p3rnirho, p4rnirho, dx, simPlat_dt, ilast, jlast);
					mx_rni = weno(w1rnimx, w2rnimx, w3rnimx, w4rnimx, p1rnimx, p2rnimx, p3rnimx, p4rnimx, dx, simPlat_dt, ilast, jlast);
					my_rni = weno(w1rnimy, w2rnimy, w3rnimy, w4rnimy, p1rnimy, p2rnimy, p3rnimy, p4rnimy, dx, simPlat_dt, ilast, jlast);
					e_rni = weno(w1rnie, w2rnie, w3rnie, w4rnie, p1rnie, p2rnie, p3rnie, p4rnie, dx, simPlat_dt, ilast, jlast);
					rho_rnj = weno(w1rnjrho, w2rnjrho, w3rnjrho, w4rnjrho, p1rnjrho, p2rnjrho, p3rnjrho, p4rnjrho, dx, simPlat_dt, ilast, jlast);
					mx_rnj = weno(w1rnjmx, w2rnjmx, w3rnjmx, w4rnjmx, p1rnjmx, p2rnjmx, p3rnjmx, p4rnjmx, dx, simPlat_dt, ilast, jlast);
					my_rnj = weno(w1rnjmy, w2rnjmy, w3rnjmy, w4rnjmy, p1rnjmy, p2rnjmy, p3rnjmy, p4rnjmy, dx, simPlat_dt, ilast, jlast);
					e_rnj = weno(w1rnje, w2rnje, w3rnje, w4rnje, p1rnje, p2rnje, p3rnje, p4rnje, dx, simPlat_dt, ilast, jlast);
					p1lpirho = lor1(vector(rho_p, i - 3, j), vector(rho_p, i - 2, j), vector(rho_p, i - 1, j), vector(rho_p, i, j), dx, simPlat_dt, ilast, jlast);
					p1lpimx = lor1(vector(mx_p, i - 3, j), vector(mx_p, i - 2, j), vector(mx_p, i - 1, j), vector(mx_p, i, j), dx, simPlat_dt, ilast, jlast);
					p1lpimy = lor1(vector(my_p, i - 3, j), vector(my_p, i - 2, j), vector(my_p, i - 1, j), vector(my_p, i, j), dx, simPlat_dt, ilast, jlast);
					p1lpie = lor1(vector(e_p, i - 3, j), vector(e_p, i - 2, j), vector(e_p, i - 1, j), vector(e_p, i, j), dx, simPlat_dt, ilast, jlast);
					p1lpjrho = lor1(vector(rho_p, i, j - 3), vector(rho_p, i, j - 2), vector(rho_p, i, j - 1), vector(rho_p, i, j), dx, simPlat_dt, ilast, jlast);
					p1lpjmx = lor1(vector(mx_p, i, j - 3), vector(mx_p, i, j - 2), vector(mx_p, i, j - 1), vector(mx_p, i, j), dx, simPlat_dt, ilast, jlast);
					p1lpjmy = lor1(vector(my_p, i, j - 3), vector(my_p, i, j - 2), vector(my_p, i, j - 1), vector(my_p, i, j), dx, simPlat_dt, ilast, jlast);
					p1lpje = lor1(vector(e_p, i, j - 3), vector(e_p, i, j - 2), vector(e_p, i, j - 1), vector(e_p, i, j), dx, simPlat_dt, ilast, jlast);
					p2lpirho = lor2(vector(rho_p, i - 2, j), vector(rho_p, i - 1, j), vector(rho_p, i, j), vector(rho_p, i + 1, j), dx, simPlat_dt, ilast, jlast);
					p2lpimx = lor2(vector(mx_p, i - 2, j), vector(mx_p, i - 1, j), vector(mx_p, i, j), vector(mx_p, i + 1, j), dx, simPlat_dt, ilast, jlast);
					p2lpimy = lor2(vector(my_p, i - 2, j), vector(my_p, i - 1, j), vector(my_p, i, j), vector(my_p, i + 1, j), dx, simPlat_dt, ilast, jlast);
					p2lpie = lor2(vector(e_p, i - 2, j), vector(e_p, i - 1, j), vector(e_p, i, j), vector(e_p, i + 1, j), dx, simPlat_dt, ilast, jlast);
					p2lpjrho = lor2(vector(rho_p, i, j - 2), vector(rho_p, i, j - 1), vector(rho_p, i, j), vector(rho_p, i, j + 1), dx, simPlat_dt, ilast, jlast);
					p2lpjmx = lor2(vector(mx_p, i, j - 2), vector(mx_p, i, j - 1), vector(mx_p, i, j), vector(mx_p, i, j + 1), dx, simPlat_dt, ilast, jlast);
					p2lpjmy = lor2(vector(my_p, i, j - 2), vector(my_p, i, j - 1), vector(my_p, i, j), vector(my_p, i, j + 1), dx, simPlat_dt, ilast, jlast);
					p2lpje = lor2(vector(e_p, i, j - 2), vector(e_p, i, j - 1), vector(e_p, i, j), vector(e_p, i, j + 1), dx, simPlat_dt, ilast, jlast);
					p3lpirho = lor3(vector(rho_p, i - 1, j), vector(rho_p, i, j), vector(rho_p, i + 1, j), vector(rho_p, i + 2, j), dx, simPlat_dt, ilast, jlast);
					p3lpimx = lor3(vector(mx_p, i - 1, j), vector(mx_p, i, j), vector(mx_p, i + 1, j), vector(mx_p, i + 2, j), dx, simPlat_dt, ilast, jlast);
					p3lpimy = lor3(vector(my_p, i - 1, j), vector(my_p, i, j), vector(my_p, i + 1, j), vector(my_p, i + 2, j), dx, simPlat_dt, ilast, jlast);
					p3lpie = lor3(vector(e_p, i - 1, j), vector(e_p, i, j), vector(e_p, i + 1, j), vector(e_p, i + 2, j), dx, simPlat_dt, ilast, jlast);
					p3lpjrho = lor3(vector(rho_p, i, j - 1), vector(rho_p, i, j), vector(rho_p, i, j + 1), vector(rho_p, i, j + 2), dx, simPlat_dt, ilast, jlast);
					p3lpjmx = lor3(vector(mx_p, i, j - 1), vector(mx_p, i, j), vector(mx_p, i, j + 1), vector(mx_p, i, j + 2), dx, simPlat_dt, ilast, jlast);
					p3lpjmy = lor3(vector(my_p, i, j - 1), vector(my_p, i, j), vector(my_p, i, j + 1), vector(my_p, i, j + 2), dx, simPlat_dt, ilast, jlast);
					p3lpje = lor3(vector(e_p, i, j - 1), vector(e_p, i, j), vector(e_p, i, j + 1), vector(e_p, i, j + 2), dx, simPlat_dt, ilast, jlast);
					p4lpirho = lor4(vector(rho_p, i, j), vector(rho_p, i + 1, j), vector(rho_p, i + 2, j), vector(rho_p, i + 3, j), dx, simPlat_dt, ilast, jlast);
					p4lpimx = lor4(vector(mx_p, i, j), vector(mx_p, i + 1, j), vector(mx_p, i + 2, j), vector(mx_p, i + 3, j), dx, simPlat_dt, ilast, jlast);
					p4lpimy = lor4(vector(my_p, i, j), vector(my_p, i + 1, j), vector(my_p, i + 2, j), vector(my_p, i + 3, j), dx, simPlat_dt, ilast, jlast);
					p4lpie = lor4(vector(e_p, i, j), vector(e_p, i + 1, j), vector(e_p, i + 2, j), vector(e_p, i + 3, j), dx, simPlat_dt, ilast, jlast);
					p4lpjrho = lor4(vector(rho_p, i, j), vector(rho_p, i, j + 1), vector(rho_p, i, j + 2), vector(rho_p, i, j + 3), dx, simPlat_dt, ilast, jlast);
					p4lpjmx = lor4(vector(mx_p, i, j), vector(mx_p, i, j + 1), vector(mx_p, i, j + 2), vector(mx_p, i, j + 3), dx, simPlat_dt, ilast, jlast);
					p4lpjmy = lor4(vector(my_p, i, j), vector(my_p, i, j + 1), vector(my_p, i, j + 2), vector(my_p, i, j + 3), dx, simPlat_dt, ilast, jlast);
					p4lpje = lor4(vector(e_p, i, j), vector(e_p, i, j + 1), vector(e_p, i, j + 2), vector(e_p, i, j + 3), dx, simPlat_dt, ilast, jlast);
					p1rpirho = lor1(vector(rho_p, i + 4, j), vector(rho_p, i + 3, j), vector(rho_p, i + 2, j), vector(rho_p, i + 1, j), dx, simPlat_dt, ilast, jlast);
					p1rpimx = lor1(vector(mx_p, i + 4, j), vector(mx_p, i + 3, j), vector(mx_p, i + 2, j), vector(mx_p, i + 1, j), dx, simPlat_dt, ilast, jlast);
					p1rpimy = lor1(vector(my_p, i + 4, j), vector(my_p, i + 3, j), vector(my_p, i + 2, j), vector(my_p, i + 1, j), dx, simPlat_dt, ilast, jlast);
					p1rpie = lor1(vector(e_p, i + 4, j), vector(e_p, i + 3, j), vector(e_p, i + 2, j), vector(e_p, i + 1, j), dx, simPlat_dt, ilast, jlast);
					p1rpjrho = lor1(vector(rho_p, i, j + 4), vector(rho_p, i, j + 3), vector(rho_p, i, j + 2), vector(rho_p, i, j + 1), dx, simPlat_dt, ilast, jlast);
					p1rpjmx = lor1(vector(mx_p, i, j + 4), vector(mx_p, i, j + 3), vector(mx_p, i, j + 2), vector(mx_p, i, j + 1), dx, simPlat_dt, ilast, jlast);
					p1rpjmy = lor1(vector(my_p, i, j + 4), vector(my_p, i, j + 3), vector(my_p, i, j + 2), vector(my_p, i, j + 1), dx, simPlat_dt, ilast, jlast);
					p1rpje = lor1(vector(e_p, i, j + 4), vector(e_p, i, j + 3), vector(e_p, i, j + 2), vector(e_p, i, j + 1), dx, simPlat_dt, ilast, jlast);
					p2rpirho = lor2(vector(rho_p, i + 3, j), vector(rho_p, i + 2, j), vector(rho_p, i + 1, j), vector(rho_p, i, j), dx, simPlat_dt, ilast, jlast);
					p2rpimx = lor2(vector(mx_p, i + 3, j), vector(mx_p, i + 2, j), vector(mx_p, i + 1, j), vector(mx_p, i, j), dx, simPlat_dt, ilast, jlast);
					p2rpimy = lor2(vector(my_p, i + 3, j), vector(my_p, i + 2, j), vector(my_p, i + 1, j), vector(my_p, i, j), dx, simPlat_dt, ilast, jlast);
					p2rpie = lor2(vector(e_p, i + 3, j), vector(e_p, i + 2, j), vector(e_p, i + 1, j), vector(e_p, i, j), dx, simPlat_dt, ilast, jlast);
					p2rpjrho = lor2(vector(rho_p, i, j + 3), vector(rho_p, i, j + 2), vector(rho_p, i, j + 1), vector(rho_p, i, j), dx, simPlat_dt, ilast, jlast);
					p2rpjmx = lor2(vector(mx_p, i, j + 3), vector(mx_p, i, j + 2), vector(mx_p, i, j + 1), vector(mx_p, i, j), dx, simPlat_dt, ilast, jlast);
					p2rpjmy = lor2(vector(my_p, i, j + 3), vector(my_p, i, j + 2), vector(my_p, i, j + 1), vector(my_p, i, j), dx, simPlat_dt, ilast, jlast);
					p2rpje = lor2(vector(e_p, i, j + 3), vector(e_p, i, j + 2), vector(e_p, i, j + 1), vector(e_p, i, j), dx, simPlat_dt, ilast, jlast);
					p3rpirho = lor3(vector(rho_p, i + 2, j), vector(rho_p, i + 1, j), vector(rho_p, i, j), vector(rho_p, i - 1, j), dx, simPlat_dt, ilast, jlast);
					p3rpimx = lor3(vector(mx_p, i + 2, j), vector(mx_p, i + 1, j), vector(mx_p, i, j), vector(mx_p, i - 1, j), dx, simPlat_dt, ilast, jlast);
					p3rpimy = lor3(vector(my_p, i + 2, j), vector(my_p, i + 1, j), vector(my_p, i, j), vector(my_p, i - 1, j), dx, simPlat_dt, ilast, jlast);
					p3rpie = lor3(vector(e_p, i + 2, j), vector(e_p, i + 1, j), vector(e_p, i, j), vector(e_p, i - 1, j), dx, simPlat_dt, ilast, jlast);
					p3rpjrho = lor3(vector(rho_p, i, j + 2), vector(rho_p, i, j + 1), vector(rho_p, i, j), vector(rho_p, i, j - 1), dx, simPlat_dt, ilast, jlast);
					p3rpjmx = lor3(vector(mx_p, i, j + 2), vector(mx_p, i, j + 1), vector(mx_p, i, j), vector(mx_p, i, j - 1), dx, simPlat_dt, ilast, jlast);
					p3rpjmy = lor3(vector(my_p, i, j + 2), vector(my_p, i, j + 1), vector(my_p, i, j), vector(my_p, i, j - 1), dx, simPlat_dt, ilast, jlast);
					p3rpje = lor3(vector(e_p, i, j + 2), vector(e_p, i, j + 1), vector(e_p, i, j), vector(e_p, i, j - 1), dx, simPlat_dt, ilast, jlast);
					p4rpirho = lor4(vector(rho_p, i + 1, j), vector(rho_p, i, j), vector(rho_p, i - 1, j), vector(rho_p, i - 2, j), dx, simPlat_dt, ilast, jlast);
					p4rpimx = lor4(vector(mx_p, i + 1, j), vector(mx_p, i, j), vector(mx_p, i - 1, j), vector(mx_p, i - 2, j), dx, simPlat_dt, ilast, jlast);
					p4rpimy = lor4(vector(my_p, i + 1, j), vector(my_p, i, j), vector(my_p, i - 1, j), vector(my_p, i - 2, j), dx, simPlat_dt, ilast, jlast);
					p4rpie = lor4(vector(e_p, i + 1, j), vector(e_p, i, j), vector(e_p, i - 1, j), vector(e_p, i - 2, j), dx, simPlat_dt, ilast, jlast);
					p4rpjrho = lor4(vector(rho_p, i, j + 1), vector(rho_p, i, j), vector(rho_p, i, j - 1), vector(rho_p, i, j - 2), dx, simPlat_dt, ilast, jlast);
					p4rpjmx = lor4(vector(mx_p, i, j + 1), vector(mx_p, i, j), vector(mx_p, i, j - 1), vector(mx_p, i, j - 2), dx, simPlat_dt, ilast, jlast);
					p4rpjmy = lor4(vector(my_p, i, j + 1), vector(my_p, i, j), vector(my_p, i, j - 1), vector(my_p, i, j - 2), dx, simPlat_dt, ilast, jlast);
					p4rpje = lor4(vector(e_p, i, j + 1), vector(e_p, i, j), vector(e_p, i, j - 1), vector(e_p, i, j - 2), dx, simPlat_dt, ilast, jlast);
					beta1lpirho = smooth1(vector(rho_p, i - 3, j), vector(rho_p, i - 2, j), vector(rho_p, i - 1, j), vector(rho_p, i, j), dx, simPlat_dt, ilast, jlast);
					beta1lpimx = smooth1(vector(mx_p, i - 3, j), vector(mx_p, i - 2, j), vector(mx_p, i - 1, j), vector(mx_p, i, j), dx, simPlat_dt, ilast, jlast);
					beta1lpimy = smooth1(vector(my_p, i - 3, j), vector(my_p, i - 2, j), vector(my_p, i - 1, j), vector(my_p, i, j), dx, simPlat_dt, ilast, jlast);
					beta1lpie = smooth1(vector(e_p, i - 3, j), vector(e_p, i - 2, j), vector(e_p, i - 1, j), vector(e_p, i, j), dx, simPlat_dt, ilast, jlast);
					beta1lpjrho = smooth1(vector(rho_p, i, j - 3), vector(rho_p, i, j - 2), vector(rho_p, i, j - 1), vector(rho_p, i, j), dx, simPlat_dt, ilast, jlast);
					beta1lpjmx = smooth1(vector(mx_p, i, j - 3), vector(mx_p, i, j - 2), vector(mx_p, i, j - 1), vector(mx_p, i, j), dx, simPlat_dt, ilast, jlast);
					beta1lpjmy = smooth1(vector(my_p, i, j - 3), vector(my_p, i, j - 2), vector(my_p, i, j - 1), vector(my_p, i, j), dx, simPlat_dt, ilast, jlast);
					beta1lpje = smooth1(vector(e_p, i, j - 3), vector(e_p, i, j - 2), vector(e_p, i, j - 1), vector(e_p, i, j), dx, simPlat_dt, ilast, jlast);
					beta2lpirho = smooth2(vector(rho_p, i - 2, j), vector(rho_p, i - 1, j), vector(rho_p, i, j), vector(rho_p, i + 1, j), dx, simPlat_dt, ilast, jlast);
					beta2lpimx = smooth2(vector(mx_p, i - 2, j), vector(mx_p, i - 1, j), vector(mx_p, i, j), vector(mx_p, i + 1, j), dx, simPlat_dt, ilast, jlast);
					beta2lpimy = smooth2(vector(my_p, i - 2, j), vector(my_p, i - 1, j), vector(my_p, i, j), vector(my_p, i + 1, j), dx, simPlat_dt, ilast, jlast);
					beta2lpie = smooth2(vector(e_p, i - 2, j), vector(e_p, i - 1, j), vector(e_p, i, j), vector(e_p, i + 1, j), dx, simPlat_dt, ilast, jlast);
					beta2lpjrho = smooth2(vector(rho_p, i, j - 2), vector(rho_p, i, j - 1), vector(rho_p, i, j), vector(rho_p, i, j + 1), dx, simPlat_dt, ilast, jlast);
					beta2lpjmx = smooth2(vector(mx_p, i, j - 2), vector(mx_p, i, j - 1), vector(mx_p, i, j), vector(mx_p, i, j + 1), dx, simPlat_dt, ilast, jlast);
					beta2lpjmy = smooth2(vector(my_p, i, j - 2), vector(my_p, i, j - 1), vector(my_p, i, j), vector(my_p, i, j + 1), dx, simPlat_dt, ilast, jlast);
					beta2lpje = smooth2(vector(e_p, i, j - 2), vector(e_p, i, j - 1), vector(e_p, i, j), vector(e_p, i, j + 1), dx, simPlat_dt, ilast, jlast);
					beta3lpirho = smooth3(vector(rho_p, i - 1, j), vector(rho_p, i, j), vector(rho_p, i + 1, j), vector(rho_p, i + 2, j), dx, simPlat_dt, ilast, jlast);
					beta3lpimx = smooth3(vector(mx_p, i - 1, j), vector(mx_p, i, j), vector(mx_p, i + 1, j), vector(mx_p, i + 2, j), dx, simPlat_dt, ilast, jlast);
					beta3lpimy = smooth3(vector(my_p, i - 1, j), vector(my_p, i, j), vector(my_p, i + 1, j), vector(my_p, i + 2, j), dx, simPlat_dt, ilast, jlast);
					beta3lpie = smooth3(vector(e_p, i - 1, j), vector(e_p, i, j), vector(e_p, i + 1, j), vector(e_p, i + 2, j), dx, simPlat_dt, ilast, jlast);
					beta3lpjrho = smooth3(vector(rho_p, i, j - 1), vector(rho_p, i, j), vector(rho_p, i, j + 1), vector(rho_p, i, j + 2), dx, simPlat_dt, ilast, jlast);
					beta3lpjmx = smooth3(vector(mx_p, i, j - 1), vector(mx_p, i, j), vector(mx_p, i, j + 1), vector(mx_p, i, j + 2), dx, simPlat_dt, ilast, jlast);
					beta3lpjmy = smooth3(vector(my_p, i, j - 1), vector(my_p, i, j), vector(my_p, i, j + 1), vector(my_p, i, j + 2), dx, simPlat_dt, ilast, jlast);
					beta3lpje = smooth3(vector(e_p, i, j - 1), vector(e_p, i, j), vector(e_p, i, j + 1), vector(e_p, i, j + 2), dx, simPlat_dt, ilast, jlast);
					beta4lpirho = smooth4(vector(rho_p, i, j), vector(rho_p, i + 1, j), vector(rho_p, i + 2, j), vector(rho_p, i + 3, j), dx, simPlat_dt, ilast, jlast);
					beta4lpimx = smooth4(vector(mx_p, i, j), vector(mx_p, i + 1, j), vector(mx_p, i + 2, j), vector(mx_p, i + 3, j), dx, simPlat_dt, ilast, jlast);
					beta4lpimy = smooth4(vector(my_p, i, j), vector(my_p, i + 1, j), vector(my_p, i + 2, j), vector(my_p, i + 3, j), dx, simPlat_dt, ilast, jlast);
					beta4lpie = smooth4(vector(e_p, i, j), vector(e_p, i + 1, j), vector(e_p, i + 2, j), vector(e_p, i + 3, j), dx, simPlat_dt, ilast, jlast);
					beta4lpjrho = smooth4(vector(rho_p, i, j), vector(rho_p, i, j + 1), vector(rho_p, i, j + 2), vector(rho_p, i, j + 3), dx, simPlat_dt, ilast, jlast);
					beta4lpjmx = smooth4(vector(mx_p, i, j), vector(mx_p, i, j + 1), vector(mx_p, i, j + 2), vector(mx_p, i, j + 3), dx, simPlat_dt, ilast, jlast);
					beta4lpjmy = smooth4(vector(my_p, i, j), vector(my_p, i, j + 1), vector(my_p, i, j + 2), vector(my_p, i, j + 3), dx, simPlat_dt, ilast, jlast);
					beta4lpje = smooth4(vector(e_p, i, j), vector(e_p, i, j + 1), vector(e_p, i, j + 2), vector(e_p, i, j + 3), dx, simPlat_dt, ilast, jlast);
					beta1rpirho = smooth1(vector(rho_p, i + 4, j), vector(rho_p, i + 3, j), vector(rho_p, i + 2, j), vector(rho_p, i + 1, j), dx, simPlat_dt, ilast, jlast);
					beta1rpimx = smooth1(vector(mx_p, i + 4, j), vector(mx_p, i + 3, j), vector(mx_p, i + 2, j), vector(mx_p, i + 1, j), dx, simPlat_dt, ilast, jlast);
					beta1rpimy = smooth1(vector(my_p, i + 4, j), vector(my_p, i + 3, j), vector(my_p, i + 2, j), vector(my_p, i + 1, j), dx, simPlat_dt, ilast, jlast);
					beta1rpie = smooth1(vector(e_p, i + 4, j), vector(e_p, i + 3, j), vector(e_p, i + 2, j), vector(e_p, i + 1, j), dx, simPlat_dt, ilast, jlast);
					beta1rpjrho = smooth1(vector(rho_p, i, j + 4), vector(rho_p, i, j + 3), vector(rho_p, i, j + 2), vector(rho_p, i, j + 1), dx, simPlat_dt, ilast, jlast);
					beta1rpjmx = smooth1(vector(mx_p, i, j + 4), vector(mx_p, i, j + 3), vector(mx_p, i, j + 2), vector(mx_p, i, j + 1), dx, simPlat_dt, ilast, jlast);
					beta1rpjmy = smooth1(vector(my_p, i, j + 4), vector(my_p, i, j + 3), vector(my_p, i, j + 2), vector(my_p, i, j + 1), dx, simPlat_dt, ilast, jlast);
					beta1rpje = smooth1(vector(e_p, i, j + 4), vector(e_p, i, j + 3), vector(e_p, i, j + 2), vector(e_p, i, j + 1), dx, simPlat_dt, ilast, jlast);
					beta2rpirho = smooth2(vector(rho_p, i + 3, j), vector(rho_p, i + 2, j), vector(rho_p, i + 1, j), vector(rho_p, i, j), dx, simPlat_dt, ilast, jlast);
					beta2rpimx = smooth2(vector(mx_p, i + 3, j), vector(mx_p, i + 2, j), vector(mx_p, i + 1, j), vector(mx_p, i, j), dx, simPlat_dt, ilast, jlast);
					beta2rpimy = smooth2(vector(my_p, i + 3, j), vector(my_p, i + 2, j), vector(my_p, i + 1, j), vector(my_p, i, j), dx, simPlat_dt, ilast, jlast);
					beta2rpie = smooth2(vector(e_p, i + 3, j), vector(e_p, i + 2, j), vector(e_p, i + 1, j), vector(e_p, i, j), dx, simPlat_dt, ilast, jlast);
					beta2rpjrho = smooth2(vector(rho_p, i, j + 3), vector(rho_p, i, j + 2), vector(rho_p, i, j + 1), vector(rho_p, i, j), dx, simPlat_dt, ilast, jlast);
					beta2rpjmx = smooth2(vector(mx_p, i, j + 3), vector(mx_p, i, j + 2), vector(mx_p, i, j + 1), vector(mx_p, i, j), dx, simPlat_dt, ilast, jlast);
					beta2rpjmy = smooth2(vector(my_p, i, j + 3), vector(my_p, i, j + 2), vector(my_p, i, j + 1), vector(my_p, i, j), dx, simPlat_dt, ilast, jlast);
					beta2rpje = smooth2(vector(e_p, i, j + 3), vector(e_p, i, j + 2), vector(e_p, i, j + 1), vector(e_p, i, j), dx, simPlat_dt, ilast, jlast);
					beta3rpirho = smooth3(vector(rho_p, i + 2, j), vector(rho_p, i + 1, j), vector(rho_p, i, j), vector(rho_p, i - 1, j), dx, simPlat_dt, ilast, jlast);
					beta3rpimx = smooth3(vector(mx_p, i + 2, j), vector(mx_p, i + 1, j), vector(mx_p, i, j), vector(mx_p, i - 1, j), dx, simPlat_dt, ilast, jlast);
					beta3rpimy = smooth3(vector(my_p, i + 2, j), vector(my_p, i + 1, j), vector(my_p, i, j), vector(my_p, i - 1, j), dx, simPlat_dt, ilast, jlast);
					beta3rpie = smooth3(vector(e_p, i + 2, j), vector(e_p, i + 1, j), vector(e_p, i, j), vector(e_p, i - 1, j), dx, simPlat_dt, ilast, jlast);
					beta3rpjrho = smooth3(vector(rho_p, i, j + 2), vector(rho_p, i, j + 1), vector(rho_p, i, j), vector(rho_p, i, j - 1), dx, simPlat_dt, ilast, jlast);
					beta3rpjmx = smooth3(vector(mx_p, i, j + 2), vector(mx_p, i, j + 1), vector(mx_p, i, j), vector(mx_p, i, j - 1), dx, simPlat_dt, ilast, jlast);
					beta3rpjmy = smooth3(vector(my_p, i, j + 2), vector(my_p, i, j + 1), vector(my_p, i, j), vector(my_p, i, j - 1), dx, simPlat_dt, ilast, jlast);
					beta3rpje = smooth3(vector(e_p, i, j + 2), vector(e_p, i, j + 1), vector(e_p, i, j), vector(e_p, i, j - 1), dx, simPlat_dt, ilast, jlast);
					beta4rpirho = smooth4(vector(rho_p, i + 1, j), vector(rho_p, i, j), vector(rho_p, i - 1, j), vector(rho_p, i - 2, j), dx, simPlat_dt, ilast, jlast);
					beta4rpimx = smooth4(vector(mx_p, i + 1, j), vector(mx_p, i, j), vector(mx_p, i - 1, j), vector(mx_p, i - 2, j), dx, simPlat_dt, ilast, jlast);
					beta4rpimy = smooth4(vector(my_p, i + 1, j), vector(my_p, i, j), vector(my_p, i - 1, j), vector(my_p, i - 2, j), dx, simPlat_dt, ilast, jlast);
					beta4rpie = smooth4(vector(e_p, i + 1, j), vector(e_p, i, j), vector(e_p, i - 1, j), vector(e_p, i - 2, j), dx, simPlat_dt, ilast, jlast);
					beta4rpjrho = smooth4(vector(rho_p, i, j + 1), vector(rho_p, i, j), vector(rho_p, i, j - 1), vector(rho_p, i, j - 2), dx, simPlat_dt, ilast, jlast);
					beta4rpjmx = smooth4(vector(mx_p, i, j + 1), vector(mx_p, i, j), vector(mx_p, i, j - 1), vector(mx_p, i, j - 2), dx, simPlat_dt, ilast, jlast);
					beta4rpjmy = smooth4(vector(my_p, i, j + 1), vector(my_p, i, j), vector(my_p, i, j - 1), vector(my_p, i, j - 2), dx, simPlat_dt, ilast, jlast);
					beta4rpje = smooth4(vector(e_p, i, j + 1), vector(e_p, i, j), vector(e_p, i, j - 1), vector(e_p, i, j - 2), dx, simPlat_dt, ilast, jlast);
					w1lpirho = nlw1(beta1lpirho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1lpimx = nlw1(beta1lpimx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1lpimy = nlw1(beta1lpimy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1lpie = nlw1(beta1lpie, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1lpjrho = nlw1(beta1lpjrho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1lpjmx = nlw1(beta1lpjmx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1lpjmy = nlw1(beta1lpjmy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1lpje = nlw1(beta1lpje, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2lpirho = nlw2(beta2lpirho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2lpimx = nlw2(beta2lpimx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2lpimy = nlw2(beta2lpimy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2lpie = nlw2(beta2lpie, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2lpjrho = nlw2(beta2lpjrho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2lpjmx = nlw2(beta2lpjmx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2lpjmy = nlw2(beta2lpjmy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2lpje = nlw2(beta2lpje, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3lpirho = nlw3(beta3lpirho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3lpimx = nlw3(beta3lpimx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3lpimy = nlw3(beta3lpimy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3lpie = nlw3(beta3lpie, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3lpjrho = nlw3(beta3lpjrho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3lpjmx = nlw3(beta3lpjmx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3lpjmy = nlw3(beta3lpjmy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3lpje = nlw3(beta3lpje, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4lpirho = nlw4(beta4lpirho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4lpimx = nlw4(beta4lpimx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4lpimy = nlw4(beta4lpimy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4lpie = nlw4(beta4lpie, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4lpjrho = nlw4(beta4lpjrho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4lpjmx = nlw4(beta4lpjmx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4lpjmy = nlw4(beta4lpjmy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4lpje = nlw4(beta4lpje, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1rpirho = nlw1(beta1rpirho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1rpimx = nlw1(beta1rpimx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1rpimy = nlw1(beta1rpimy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1rpie = nlw1(beta1rpie, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1rpjrho = nlw1(beta1rpjrho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1rpjmx = nlw1(beta1rpjmx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1rpjmy = nlw1(beta1rpjmy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1rpje = nlw1(beta1rpje, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2rpirho = nlw2(beta2rpirho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2rpimx = nlw2(beta2rpimx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2rpimy = nlw2(beta2rpimy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2rpie = nlw2(beta2rpie, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2rpjrho = nlw2(beta2rpjrho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2rpjmx = nlw2(beta2rpjmx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2rpjmy = nlw2(beta2rpjmy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2rpje = nlw2(beta2rpje, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3rpirho = nlw3(beta3rpirho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3rpimx = nlw3(beta3rpimx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3rpimy = nlw3(beta3rpimy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3rpie = nlw3(beta3rpie, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3rpjrho = nlw3(beta3rpjrho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3rpjmx = nlw3(beta3rpjmx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3rpjmy = nlw3(beta3rpjmy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3rpje = nlw3(beta3rpje, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4rpirho = nlw4(beta4rpirho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4rpimx = nlw4(beta4rpimx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4rpimy = nlw4(beta4rpimy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4rpie = nlw4(beta4rpie, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4rpjrho = nlw4(beta4rpjrho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4rpjmx = nlw4(beta4rpjmx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4rpjmy = nlw4(beta4rpjmy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4rpje = nlw4(beta4rpje, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					wlpirho = w1lpirho + w2lpirho + w3lpirho + w4lpirho;
					wlpimx = w1lpimx + w2lpimx + w3lpimx + w4lpimx;
					wlpimy = w1lpimy + w2lpimy + w3lpimy + w4lpimy;
					wlpie = w1lpie + w2lpie + w3lpie + w4lpie;
					wlpjrho = w1lpjrho + w2lpjrho + w3lpjrho + w4lpjrho;
					wlpjmx = w1lpjmx + w2lpjmx + w3lpjmx + w4lpjmx;
					wlpjmy = w1lpjmy + w2lpjmy + w3lpjmy + w4lpjmy;
					wlpje = w1lpje + w2lpje + w3lpje + w4lpje;
					wrpirho = w1rpirho + w2rpirho + w3rpirho + w4rpirho;
					wrpimx = w1rpimx + w2rpimx + w3rpimx + w4rpimx;
					wrpimy = w1rpimy + w2rpimy + w3rpimy + w4rpimy;
					wrpie = w1rpie + w2rpie + w3rpie + w4rpie;
					wrpjrho = w1rpjrho + w2rpjrho + w3rpjrho + w4rpjrho;
					wrpjmx = w1rpjmx + w2rpjmx + w3rpjmx + w4rpjmx;
					wrpjmy = w1rpjmy + w2rpjmy + w3rpjmy + w4rpjmy;
					wrpje = w1rpje + w2rpje + w3rpje + w4rpje;
					w1lpirho = w1lpirho / wlpirho;
					w1lpimx = w1lpimx / wlpimx;
					w1lpimy = w1lpimy / wlpimy;
					w1lpie = w1lpie / wlpie;
					w1lpjrho = w1lpjrho / wlpjrho;
					w1lpjmx = w1lpjmx / wlpjmx;
					w1lpjmy = w1lpjmy / wlpjmy;
					w1lpje = w1lpje / wlpje;
					w2lpirho = w2lpirho / wlpirho;
					w2lpimx = w2lpimx / wlpimx;
					w2lpimy = w2lpimy / wlpimy;
					w2lpie = w2lpie / wlpie;
					w2lpjrho = w2lpjrho / wlpjrho;
					w2lpjmx = w2lpjmx / wlpjmx;
					w2lpjmy = w2lpjmy / wlpjmy;
					w2lpje = w2lpje / wlpje;
					w3lpirho = w3lpirho / wlpirho;
					w3lpimx = w3lpimx / wlpimx;
					w3lpimy = w3lpimy / wlpimy;
					w3lpie = w3lpie / wlpie;
					w3lpjrho = w3lpjrho / wlpjrho;
					w3lpjmx = w3lpjmx / wlpjmx;
					w3lpjmy = w3lpjmy / wlpjmy;
					w3lpje = w3lpje / wlpje;
					w4lpirho = w4lpirho / wlpirho;
					w4lpimx = w4lpimx / wlpimx;
					w4lpimy = w4lpimy / wlpimy;
					w4lpie = w4lpie / wlpie;
					w4lpjrho = w4lpjrho / wlpjrho;
					w4lpjmx = w4lpjmx / wlpjmx;
					w4lpjmy = w4lpjmy / wlpjmy;
					w4lpje = w4lpje / wlpje;
					w1rpirho = w1rpirho / wrpirho;
					w1rpimx = w1rpimx / wrpimx;
					w1rpimy = w1rpimy / wrpimy;
					w1rpie = w1rpie / wrpie;
					w1rpjrho = w1rpjrho / wrpjrho;
					w1rpjmx = w1rpjmx / wrpjmx;
					w1rpjmy = w1rpjmy / wrpjmy;
					w1rpje = w1rpje / wrpje;
					w2rpirho = w2rpirho / wrpirho;
					w2rpimx = w2rpimx / wrpimx;
					w2rpimy = w2rpimy / wrpimy;
					w2rpie = w2rpie / wrpie;
					w2rpjrho = w2rpjrho / wrpjrho;
					w2rpjmx = w2rpjmx / wrpjmx;
					w2rpjmy = w2rpjmy / wrpjmy;
					w2rpje = w2rpje / wrpje;
					w3rpirho = w3rpirho / wrpirho;
					w3rpimx = w3rpimx / wrpimx;
					w3rpimy = w3rpimy / wrpimy;
					w3rpie = w3rpie / wrpie;
					w3rpjrho = w3rpjrho / wrpjrho;
					w3rpjmx = w3rpjmx / wrpjmx;
					w3rpjmy = w3rpjmy / wrpjmy;
					w3rpje = w3rpje / wrpje;
					w4rpirho = w4rpirho / wrpirho;
					w4rpimx = w4rpimx / wrpimx;
					w4rpimy = w4rpimy / wrpimy;
					w4rpie = w4rpie / wrpie;
					w4rpjrho = w4rpjrho / wrpjrho;
					w4rpjmx = w4rpjmx / wrpjmx;
					w4rpjmy = w4rpjmy / wrpjmy;
					w4rpje = w4rpje / wrpje;
					rho_lpi = weno(w1lpirho, w2lpirho, w3lpirho, w4lpirho, p1lpirho, p2lpirho, p3lpirho, p4lpirho, dx, simPlat_dt, ilast, jlast);
					mx_lpi = weno(w1lpimx, w2lpimx, w3lpimx, w4lpimx, p1lpimx, p2lpimx, p3lpimx, p4lpimx, dx, simPlat_dt, ilast, jlast);
					my_lpi = weno(w1lpimy, w2lpimy, w3lpimy, w4lpimy, p1lpimy, p2lpimy, p3lpimy, p4lpimy, dx, simPlat_dt, ilast, jlast);
					e_lpi = weno(w1lpie, w2lpie, w3lpie, w4lpie, p1lpie, p2lpie, p3lpie, p4lpie, dx, simPlat_dt, ilast, jlast);
					rho_lpj = weno(w1lpjrho, w2lpjrho, w3lpjrho, w4lpjrho, p1lpjrho, p2lpjrho, p3lpjrho, p4lpjrho, dx, simPlat_dt, ilast, jlast);
					mx_lpj = weno(w1lpjmx, w2lpjmx, w3lpjmx, w4lpjmx, p1lpjmx, p2lpjmx, p3lpjmx, p4lpjmx, dx, simPlat_dt, ilast, jlast);
					my_lpj = weno(w1lpjmy, w2lpjmy, w3lpjmy, w4lpjmy, p1lpjmy, p2lpjmy, p3lpjmy, p4lpjmy, dx, simPlat_dt, ilast, jlast);
					e_lpj = weno(w1lpje, w2lpje, w3lpje, w4lpje, p1lpje, p2lpje, p3lpje, p4lpje, dx, simPlat_dt, ilast, jlast);
					rho_rpi = weno(w1rpirho, w2rpirho, w3rpirho, w4rpirho, p1rpirho, p2rpirho, p3rpirho, p4rpirho, dx, simPlat_dt, ilast, jlast);
					mx_rpi = weno(w1rpimx, w2rpimx, w3rpimx, w4rpimx, p1rpimx, p2rpimx, p3rpimx, p4rpimx, dx, simPlat_dt, ilast, jlast);
					my_rpi = weno(w1rpimy, w2rpimy, w3rpimy, w4rpimy, p1rpimy, p2rpimy, p3rpimy, p4rpimy, dx, simPlat_dt, ilast, jlast);
					e_rpi = weno(w1rpie, w2rpie, w3rpie, w4rpie, p1rpie, p2rpie, p3rpie, p4rpie, dx, simPlat_dt, ilast, jlast);
					rho_rpj = weno(w1rpjrho, w2rpjrho, w3rpjrho, w4rpjrho, p1rpjrho, p2rpjrho, p3rpjrho, p4rpjrho, dx, simPlat_dt, ilast, jlast);
					mx_rpj = weno(w1rpjmx, w2rpjmx, w3rpjmx, w4rpjmx, p1rpjmx, p2rpjmx, p3rpjmx, p4rpjmx, dx, simPlat_dt, ilast, jlast);
					my_rpj = weno(w1rpjmy, w2rpjmy, w3rpjmy, w4rpjmy, p1rpjmy, p2rpjmy, p3rpjmy, p4rpjmy, dx, simPlat_dt, ilast, jlast);
					e_rpj = weno(w1rpje, w2rpje, w3rpje, w4rpje, p1rpje, p2rpje, p3rpje, p4rpje, dx, simPlat_dt, ilast, jlast);
					P_lpi = (gamma - 1.0) * (e_lpi - rho_lpi * (((mx_lpi / rho_lpi) * (mx_lpi / rho_lpi)) / 2.0 + ((my_lpi / rho_lpi) * (my_lpi / rho_lpi)) / 2.0));
					P_lpj = (gamma - 1.0) * (e_lpj - rho_lpj * (((mx_lpj / rho_lpj) * (mx_lpj / rho_lpj)) / 2.0 + ((my_lpj / rho_lpj) * (my_lpj / rho_lpj)) / 2.0));
					P_lni = (gamma - 1.0) * (e_lni - rho_lni * (((mx_lni / rho_lni) * (mx_lni / rho_lni)) / 2.0 + ((my_lni / rho_lni) * (my_lni / rho_lni)) / 2.0));
					P_lnj = (gamma - 1.0) * (e_lnj - rho_lnj * (((mx_lnj / rho_lnj) * (mx_lnj / rho_lnj)) / 2.0 + ((my_lnj / rho_lnj) * (my_lnj / rho_lnj)) / 2.0));
					P_rpi = (gamma - 1.0) * (e_rpi - rho_rpi * (((mx_rpi / rho_rpi) * (mx_rpi / rho_rpi)) / 2.0 + ((my_rpi / rho_rpi) * (my_rpi / rho_rpi)) / 2.0));
					P_rpj = (gamma - 1.0) * (e_rpj - rho_rpj * (((mx_rpj / rho_rpj) * (mx_rpj / rho_rpj)) / 2.0 + ((my_rpj / rho_rpj) * (my_rpj / rho_rpj)) / 2.0));
					P_rni = (gamma - 1.0) * (e_rni - rho_rni * (((mx_rni / rho_rni) * (mx_rni / rho_rni)) / 2.0 + ((my_rni / rho_rni) * (my_rni / rho_rni)) / 2.0));
					P_rnj = (gamma - 1.0) * (e_rnj - rho_rnj * (((mx_rnj / rho_rnj) * (mx_rnj / rho_rnj)) / 2.0 + ((my_rnj / rho_rnj) * (my_rnj / rho_rnj)) / 2.0));
					cy_p = vector(my_p, i, j) / vector(rho_p, i, j);
					cx_p = vector(mx_p, i, j) / vector(rho_p, i, j);
					c_p = sqrt(fabs((gamma * vector(P, i, j)) / vector(rho_p, i, j)));
					Speedirho = MAX(fabs(cx_p), MAX(fabs(cx_p - c_p), fabs(cx_p + c_p)));
					Speedimx = MAX(fabs(cx_p), MAX(fabs(cx_p - c_p), fabs(cx_p + c_p)));
					Speedimy = MAX(fabs(cx_p), MAX(fabs(cx_p - c_p), fabs(cx_p + c_p)));
					Speedie = MAX(fabs(cx_p), MAX(fabs(cx_p - c_p), fabs(cx_p + c_p)));
					Speedjrho = MAX(fabs(cy_p), MAX(fabs(cy_p - c_p), fabs(cy_p + c_p)));
					Speedjmx = MAX(fabs(cy_p), MAX(fabs(cy_p - c_p), fabs(cy_p + c_p)));
					Speedjmy = MAX(fabs(cy_p), MAX(fabs(cy_p - c_p), fabs(cy_p + c_p)));
					Speedje = MAX(fabs(cy_p), MAX(fabs(cy_p - c_p), fabs(cy_p + c_p)));
					cy_p = vector(my_p, i + 1, j) / vector(rho_p, i + 1, j);
					cx_p = vector(mx_p, i + 1, j) / vector(rho_p, i + 1, j);
					c_p = sqrt(fabs((gamma * vector(P, i + 1, j)) / vector(rho_p, i + 1, j)));
					Speedpirho = MAX(fabs(cx_p), MAX(fabs(cx_p - c_p), fabs(cx_p + c_p)));
					Speedpimx = MAX(fabs(cx_p), MAX(fabs(cx_p - c_p), fabs(cx_p + c_p)));
					Speedpimy = MAX(fabs(cx_p), MAX(fabs(cx_p - c_p), fabs(cx_p + c_p)));
					Speedpie = MAX(fabs(cx_p), MAX(fabs(cx_p - c_p), fabs(cx_p + c_p)));
					cy_p = vector(my_p, i, j + 1) / vector(rho_p, i, j + 1);
					cx_p = vector(mx_p, i, j + 1) / vector(rho_p, i, j + 1);
					c_p = sqrt(fabs((gamma * vector(P, i, j + 1)) / vector(rho_p, i, j + 1)));
					Speedpjrho = MAX(fabs(cy_p), MAX(fabs(cy_p - c_p), fabs(cy_p + c_p)));
					Speedpjmx = MAX(fabs(cy_p), MAX(fabs(cy_p - c_p), fabs(cy_p + c_p)));
					Speedpjmy = MAX(fabs(cy_p), MAX(fabs(cy_p - c_p), fabs(cy_p + c_p)));
					Speedpje = MAX(fabs(cy_p), MAX(fabs(cy_p - c_p), fabs(cy_p + c_p)));
					cy_p = vector(my_p, i - 1, j) / vector(rho_p, i - 1, j);
					cx_p = vector(mx_p, i - 1, j) / vector(rho_p, i - 1, j);
					c_p = sqrt(fabs((gamma * vector(P, i - 1, j)) / vector(rho_p, i - 1, j)));
					Speednirho = MAX(fabs(cx_p), MAX(fabs(cx_p - c_p), fabs(cx_p + c_p)));
					Speednimx = MAX(fabs(cx_p), MAX(fabs(cx_p - c_p), fabs(cx_p + c_p)));
					Speednimy = MAX(fabs(cx_p), MAX(fabs(cx_p - c_p), fabs(cx_p + c_p)));
					Speednie = MAX(fabs(cx_p), MAX(fabs(cx_p - c_p), fabs(cx_p + c_p)));
					cy_p = vector(my_p, i, j - 1) / vector(rho_p, i, j - 1);
					cx_p = vector(mx_p, i, j - 1) / vector(rho_p, i, j - 1);
					c_p = sqrt(fabs((gamma * vector(P, i, j - 1)) / vector(rho_p, i, j - 1)));
					Speednjrho = MAX(fabs(cy_p), MAX(fabs(cy_p - c_p), fabs(cy_p + c_p)));
					Speednjmx = MAX(fabs(cy_p), MAX(fabs(cy_p - c_p), fabs(cy_p + c_p)));
					Speednjmy = MAX(fabs(cy_p), MAX(fabs(cy_p - c_p), fabs(cy_p + c_p)));
					Speednje = MAX(fabs(cy_p), MAX(fabs(cy_p - c_p), fabs(cy_p + c_p)));
					Fluxlpimx = Fimx_domainI(rho_lpi, mx_lpi, P_lpi, dx, simPlat_dt, ilast, jlast);
					Fluxlpimy = Fimy_domainI(rho_lpi, mx_lpi, my_lpi, dx, simPlat_dt, ilast, jlast);
					Fluxlpie = Fie_domainI(rho_lpi, mx_lpi, e_lpi, P_lpi, dx, simPlat_dt, ilast, jlast);
					Fluxlpjmx = Fjmx_domainI(rho_lpj, mx_lpj, my_lpj, dx, simPlat_dt, ilast, jlast);
					Fluxlpjmy = Fjmy_domainI(rho_lpj, my_lpj, P_lpj, dx, simPlat_dt, ilast, jlast);
					Fluxlpje = Fje_domainI(rho_lpj, my_lpj, e_lpj, P_lpj, dx, simPlat_dt, ilast, jlast);
					Fluxlnimx = Fimx_domainI(rho_lni, mx_lni, P_lni, dx, simPlat_dt, ilast, jlast);
					Fluxlnimy = Fimy_domainI(rho_lni, mx_lni, my_lni, dx, simPlat_dt, ilast, jlast);
					Fluxlnie = Fie_domainI(rho_lni, mx_lni, e_lni, P_lni, dx, simPlat_dt, ilast, jlast);
					Fluxlnjmx = Fjmx_domainI(rho_lnj, mx_lnj, my_lnj, dx, simPlat_dt, ilast, jlast);
					Fluxlnjmy = Fjmy_domainI(rho_lnj, my_lnj, P_lnj, dx, simPlat_dt, ilast, jlast);
					Fluxlnje = Fje_domainI(rho_lnj, my_lnj, e_lnj, P_lnj, dx, simPlat_dt, ilast, jlast);
					Fluxrpimx = Fimx_domainI(rho_rpi, mx_rpi, P_rpi, dx, simPlat_dt, ilast, jlast);
					Fluxrpimy = Fimy_domainI(rho_rpi, mx_rpi, my_rpi, dx, simPlat_dt, ilast, jlast);
					Fluxrpie = Fie_domainI(rho_rpi, mx_rpi, e_rpi, P_rpi, dx, simPlat_dt, ilast, jlast);
					Fluxrpjmx = Fjmx_domainI(rho_rpj, mx_rpj, my_rpj, dx, simPlat_dt, ilast, jlast);
					Fluxrpjmy = Fjmy_domainI(rho_rpj, my_rpj, P_rpj, dx, simPlat_dt, ilast, jlast);
					Fluxrpje = Fje_domainI(rho_rpj, my_rpj, e_rpj, P_rpj, dx, simPlat_dt, ilast, jlast);
					Fluxrnimx = Fimx_domainI(rho_rni, mx_rni, P_rni, dx, simPlat_dt, ilast, jlast);
					Fluxrnimy = Fimy_domainI(rho_rni, mx_rni, my_rni, dx, simPlat_dt, ilast, jlast);
					Fluxrnie = Fie_domainI(rho_rni, mx_rni, e_rni, P_rni, dx, simPlat_dt, ilast, jlast);
					Fluxrnjmx = Fjmx_domainI(rho_rnj, mx_rnj, my_rnj, dx, simPlat_dt, ilast, jlast);
					Fluxrnjmy = Fjmy_domainI(rho_rnj, my_rnj, P_rnj, dx, simPlat_dt, ilast, jlast);
					Fluxrnje = Fje_domainI(rho_rnj, my_rnj, e_rnj, P_rnj, dx, simPlat_dt, ilast, jlast);
					RHS_rho = 0.0;
					RHS_mx = 0.0;
					RHS_my = 0.0;
					RHS_e = 0.0;
					RHS_rho = RHS_rho - LLF_i(mx_lni, mx_rni, mx_lpi, mx_rpi, rho_lni, rho_rni, rho_lpi, rho_rpi, Speedirho, Speednirho, Speedpirho, dx, simPlat_dt, ilast, jlast);
					RHS_rho = RHS_rho - LLF_j(my_lnj, my_rnj, my_lpj, my_rpj, rho_lnj, rho_rnj, rho_lpj, rho_rpj, Speedjrho, Speednjrho, Speedpjrho, dx, simPlat_dt, ilast, jlast);
					RHS_mx = RHS_mx - LLF_i(Fluxlnimx, Fluxrnimx, Fluxlpimx, Fluxrpimx, mx_lni, mx_rni, mx_lpi, mx_rpi, Speedimx, Speednimx, Speedpimx, dx, simPlat_dt, ilast, jlast);
					RHS_mx = RHS_mx - LLF_j(Fluxlnjmx, Fluxrnjmx, Fluxlpjmx, Fluxrpjmx, mx_lnj, mx_rnj, mx_lpj, mx_rpj, Speedjmx, Speednjmx, Speedpjmx, dx, simPlat_dt, ilast, jlast);
					RHS_my = RHS_my - LLF_i(Fluxlnimy, Fluxrnimy, Fluxlpimy, Fluxrpimy, my_lni, my_rni, my_lpi, my_rpi, Speedimy, Speednimy, Speedpimy, dx, simPlat_dt, ilast, jlast);
					RHS_my = RHS_my - LLF_j(Fluxlnjmy, Fluxrnjmy, Fluxlpjmy, Fluxrpjmy, my_lnj, my_rnj, my_lpj, my_rpj, Speedjmy, Speednjmy, Speedpjmy, dx, simPlat_dt, ilast, jlast);
					RHS_e = RHS_e - LLF_i(Fluxlnie, Fluxrnie, Fluxlpie, Fluxrpie, e_lni, e_rni, e_lpi, e_rpi, Speedie, Speednie, Speedpie, dx, simPlat_dt, ilast, jlast);
					RHS_e = RHS_e - LLF_j(Fluxlnje, Fluxrnje, Fluxlpje, Fluxrpje, e_lnj, e_rnj, e_lpj, e_rpj, Speedje, Speednje, Speedpje, dx, simPlat_dt, ilast, jlast);
					vector(rk1rho, i, j) = RK3P1_(RHS_rho, rho_p, 0.0, i, j, dx, simPlat_dt, ilast, jlast);
					vector(rk1mx, i, j) = RK3P1_(RHS_mx, mx_p, 0.0, i, j, dx, simPlat_dt, ilast, jlast);
					vector(rk1my, i, j) = RK3P1_(RHS_my, my_p, 0.0, i, j, dx, simPlat_dt, ilast, jlast);
					vector(rk1e, i, j) = RK3P1_(RHS_e, e_p, 0.0, i, j, dx, simPlat_dt, ilast, jlast);
				}
			}
		}
	}
	//Fill ghosts and periodical boundaries
	time_interpolate_operator_mesh1->setStep(1);
	d_bdry_sched_advance2[ln]->fillData(current_time + simPlat_dt, false);
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch >& patch = *p_it;
		double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
	
		double* P = ((pdat::NodeData<double> *) patch->getPatchData(d_P_id).get())->getPointer();
		double* rk1e = ((pdat::NodeData<double> *) patch->getPatchData(d_rk1e_id).get())->getPointer();
		double* rk1rho = ((pdat::NodeData<double> *) patch->getPatchData(d_rk1rho_id).get())->getPointer();
		double* rk1mx = ((pdat::NodeData<double> *) patch->getPatchData(d_rk1mx_id).get())->getPointer();
		double* rk1my = ((pdat::NodeData<double> *) patch->getPatchData(d_rk1my_id).get())->getPointer();
	
		//Get the dimensions of the patch
		const hier::Index boxfirst = patch->getBox().lower();
		const hier::Index boxlast = patch->getBox().upper();
	
		//Get delta spaces into an array. dx, dy, dz.
		std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
		const double* dx  = patch_geom->getDx();
	
		//Auxiliary definitions
		int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
		int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
		for(int j = 0; j < jlast; j++) {
			for(int i = 0; i < ilast; i++) {
				if (((i + 4 < ilast || !patch->getPatchGeometry()->getTouchesRegularBoundary(0, 1)) && (i - 4 >= 0 || !patch->getPatchGeometry()->getTouchesRegularBoundary(0, 0)) && (j + 4 < jlast || !patch->getPatchGeometry()->getTouchesRegularBoundary(1, 1)) && (j - 4 >= 0 || !patch->getPatchGeometry()->getTouchesRegularBoundary(1, 0)))) {
					vector(P, i, j) = (gamma - 1.0) * (vector(rk1e, i, j) - vector(rk1rho, i, j) * (((vector(rk1mx, i, j) / vector(rk1rho, i, j)) * (vector(rk1mx, i, j) / vector(rk1rho, i, j))) / 2.0 + ((vector(rk1my, i, j) / vector(rk1rho, i, j)) * (vector(rk1my, i, j) / vector(rk1rho, i, j))) / 2.0));
				}
			}
		}
	}
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch >& patch = *p_it;
		double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
	
		double* rk1rho = ((pdat::NodeData<double> *) patch->getPatchData(d_rk1rho_id).get())->getPointer();
		double* rk1mx = ((pdat::NodeData<double> *) patch->getPatchData(d_rk1mx_id).get())->getPointer();
		double* rk1my = ((pdat::NodeData<double> *) patch->getPatchData(d_rk1my_id).get())->getPointer();
		double* rk1e = ((pdat::NodeData<double> *) patch->getPatchData(d_rk1e_id).get())->getPointer();
		double* P = ((pdat::NodeData<double> *) patch->getPatchData(d_P_id).get())->getPointer();
		double* rk2rho = ((pdat::NodeData<double> *) patch->getPatchData(d_rk2rho_id).get())->getPointer();
		double* rho_p = ((pdat::NodeData<double> *) patch->getPatchData(d_rho_p_id).get())->getPointer();
		double* rk2mx = ((pdat::NodeData<double> *) patch->getPatchData(d_rk2mx_id).get())->getPointer();
		double* mx_p = ((pdat::NodeData<double> *) patch->getPatchData(d_mx_p_id).get())->getPointer();
		double* rk2my = ((pdat::NodeData<double> *) patch->getPatchData(d_rk2my_id).get())->getPointer();
		double* my_p = ((pdat::NodeData<double> *) patch->getPatchData(d_my_p_id).get())->getPointer();
		double* rk2e = ((pdat::NodeData<double> *) patch->getPatchData(d_rk2e_id).get())->getPointer();
		double* e_p = ((pdat::NodeData<double> *) patch->getPatchData(d_e_p_id).get())->getPointer();
		double p1lnirho, p1lnimx, p1lnimy, p1lnie, p1lnjrho, p1lnjmx, p1lnjmy, p1lnje, p2lnirho, p2lnimx, p2lnimy, p2lnie, p2lnjrho, p2lnjmx, p2lnjmy, p2lnje, p3lnirho, p3lnimx, p3lnimy, p3lnie, p3lnjrho, p3lnjmx, p3lnjmy, p3lnje, p4lnirho, p4lnimx, p4lnimy, p4lnie, p4lnjrho, p4lnjmx, p4lnjmy, p4lnje, p1rnirho, p1rnimx, p1rnimy, p1rnie, p1rnjrho, p1rnjmx, p1rnjmy, p1rnje, p2rnirho, p2rnimx, p2rnimy, p2rnie, p2rnjrho, p2rnjmx, p2rnjmy, p2rnje, p3rnirho, p3rnimx, p3rnimy, p3rnie, p3rnjrho, p3rnjmx, p3rnjmy, p3rnje, p4rnirho, p4rnimx, p4rnimy, p4rnie, p4rnjrho, p4rnjmx, p4rnjmy, p4rnje, beta1lnirho, beta1lnimx, beta1lnimy, beta1lnie, beta1lnjrho, beta1lnjmx, beta1lnjmy, beta1lnje, beta2lnirho, beta2lnimx, beta2lnimy, beta2lnie, beta2lnjrho, beta2lnjmx, beta2lnjmy, beta2lnje, beta3lnirho, beta3lnimx, beta3lnimy, beta3lnie, beta3lnjrho, beta3lnjmx, beta3lnjmy, beta3lnje, beta4lnirho, beta4lnimx, beta4lnimy, beta4lnie, beta4lnjrho, beta4lnjmx, beta4lnjmy, beta4lnje, beta1rnirho, beta1rnimx, beta1rnimy, beta1rnie, beta1rnjrho, beta1rnjmx, beta1rnjmy, beta1rnje, beta2rnirho, beta2rnimx, beta2rnimy, beta2rnie, beta2rnjrho, beta2rnjmx, beta2rnjmy, beta2rnje, beta3rnirho, beta3rnimx, beta3rnimy, beta3rnie, beta3rnjrho, beta3rnjmx, beta3rnjmy, beta3rnje, beta4rnirho, beta4rnimx, beta4rnimy, beta4rnie, beta4rnjrho, beta4rnjmx, beta4rnjmy, beta4rnje, w1lnirho, w1lnimx, w1lnimy, w1lnie, w1lnjrho, w1lnjmx, w1lnjmy, w1lnje, w2lnirho, w2lnimx, w2lnimy, w2lnie, w2lnjrho, w2lnjmx, w2lnjmy, w2lnje, w3lnirho, w3lnimx, w3lnimy, w3lnie, w3lnjrho, w3lnjmx, w3lnjmy, w3lnje, w4lnirho, w4lnimx, w4lnimy, w4lnie, w4lnjrho, w4lnjmx, w4lnjmy, w4lnje, w1rnirho, w1rnimx, w1rnimy, w1rnie, w1rnjrho, w1rnjmx, w1rnjmy, w1rnje, w2rnirho, w2rnimx, w2rnimy, w2rnie, w2rnjrho, w2rnjmx, w2rnjmy, w2rnje, w3rnirho, w3rnimx, w3rnimy, w3rnie, w3rnjrho, w3rnjmx, w3rnjmy, w3rnje, w4rnirho, w4rnimx, w4rnimy, w4rnie, w4rnjrho, w4rnjmx, w4rnjmy, w4rnje, wlnirho, wlnimx, wlnimy, wlnie, wlnjrho, wlnjmx, wlnjmy, wlnje, wrnirho, wrnimx, wrnimy, wrnie, wrnjrho, wrnjmx, wrnjmy, wrnje, rho_lni, mx_lni, my_lni, e_lni, rho_lnj, mx_lnj, my_lnj, e_lnj, rho_rni, mx_rni, my_rni, e_rni, rho_rnj, mx_rnj, my_rnj, e_rnj, p1lpirho, p1lpimx, p1lpimy, p1lpie, p1lpjrho, p1lpjmx, p1lpjmy, p1lpje, p2lpirho, p2lpimx, p2lpimy, p2lpie, p2lpjrho, p2lpjmx, p2lpjmy, p2lpje, p3lpirho, p3lpimx, p3lpimy, p3lpie, p3lpjrho, p3lpjmx, p3lpjmy, p3lpje, p4lpirho, p4lpimx, p4lpimy, p4lpie, p4lpjrho, p4lpjmx, p4lpjmy, p4lpje, p1rpirho, p1rpimx, p1rpimy, p1rpie, p1rpjrho, p1rpjmx, p1rpjmy, p1rpje, p2rpirho, p2rpimx, p2rpimy, p2rpie, p2rpjrho, p2rpjmx, p2rpjmy, p2rpje, p3rpirho, p3rpimx, p3rpimy, p3rpie, p3rpjrho, p3rpjmx, p3rpjmy, p3rpje, p4rpirho, p4rpimx, p4rpimy, p4rpie, p4rpjrho, p4rpjmx, p4rpjmy, p4rpje, beta1lpirho, beta1lpimx, beta1lpimy, beta1lpie, beta1lpjrho, beta1lpjmx, beta1lpjmy, beta1lpje, beta2lpirho, beta2lpimx, beta2lpimy, beta2lpie, beta2lpjrho, beta2lpjmx, beta2lpjmy, beta2lpje, beta3lpirho, beta3lpimx, beta3lpimy, beta3lpie, beta3lpjrho, beta3lpjmx, beta3lpjmy, beta3lpje, beta4lpirho, beta4lpimx, beta4lpimy, beta4lpie, beta4lpjrho, beta4lpjmx, beta4lpjmy, beta4lpje, beta1rpirho, beta1rpimx, beta1rpimy, beta1rpie, beta1rpjrho, beta1rpjmx, beta1rpjmy, beta1rpje, beta2rpirho, beta2rpimx, beta2rpimy, beta2rpie, beta2rpjrho, beta2rpjmx, beta2rpjmy, beta2rpje, beta3rpirho, beta3rpimx, beta3rpimy, beta3rpie, beta3rpjrho, beta3rpjmx, beta3rpjmy, beta3rpje, beta4rpirho, beta4rpimx, beta4rpimy, beta4rpie, beta4rpjrho, beta4rpjmx, beta4rpjmy, beta4rpje, w1lpirho, w1lpimx, w1lpimy, w1lpie, w1lpjrho, w1lpjmx, w1lpjmy, w1lpje, w2lpirho, w2lpimx, w2lpimy, w2lpie, w2lpjrho, w2lpjmx, w2lpjmy, w2lpje, w3lpirho, w3lpimx, w3lpimy, w3lpie, w3lpjrho, w3lpjmx, w3lpjmy, w3lpje, w4lpirho, w4lpimx, w4lpimy, w4lpie, w4lpjrho, w4lpjmx, w4lpjmy, w4lpje, w1rpirho, w1rpimx, w1rpimy, w1rpie, w1rpjrho, w1rpjmx, w1rpjmy, w1rpje, w2rpirho, w2rpimx, w2rpimy, w2rpie, w2rpjrho, w2rpjmx, w2rpjmy, w2rpje, w3rpirho, w3rpimx, w3rpimy, w3rpie, w3rpjrho, w3rpjmx, w3rpjmy, w3rpje, w4rpirho, w4rpimx, w4rpimy, w4rpie, w4rpjrho, w4rpjmx, w4rpjmy, w4rpje, wlpirho, wlpimx, wlpimy, wlpie, wlpjrho, wlpjmx, wlpjmy, wlpje, wrpirho, wrpimx, wrpimy, wrpie, wrpjrho, wrpjmx, wrpjmy, wrpje, rho_lpi, mx_lpi, my_lpi, e_lpi, rho_lpj, mx_lpj, my_lpj, e_lpj, rho_rpi, mx_rpi, my_rpi, e_rpi, rho_rpj, mx_rpj, my_rpj, e_rpj, P_lpi, P_lpj, P_lni, P_lnj, P_rpi, P_rpj, P_rni, P_rnj, rk1cy, rk1cx, rk1c, Speedirho, Speedimx, Speedimy, Speedie, Speedjrho, Speedjmx, Speedjmy, Speedje, Speedpirho, Speedpimx, Speedpimy, Speedpie, Speedpjrho, Speedpjmx, Speedpjmy, Speedpje, Speednirho, Speednimx, Speednimy, Speednie, Speednjrho, Speednjmx, Speednjmy, Speednje, Fluxlpimx, Fluxlpimy, Fluxlpie, Fluxlpjmx, Fluxlpjmy, Fluxlpje, Fluxlnimx, Fluxlnimy, Fluxlnie, Fluxlnjmx, Fluxlnjmy, Fluxlnje, Fluxrpimx, Fluxrpimy, Fluxrpie, Fluxrpjmx, Fluxrpjmy, Fluxrpje, Fluxrnimx, Fluxrnimy, Fluxrnie, Fluxrnjmx, Fluxrnjmy, Fluxrnje, RHS_rho, RHS_mx, RHS_my, RHS_e;
	
		//Get the dimensions of the patch
		const hier::Index boxfirst = patch->getBox().lower();
		const hier::Index boxlast = patch->getBox().upper();
	
		//Get delta spaces into an array. dx, dy, dz.
		std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
		const double* dx  = patch_geom->getDx();
	
		//Auxiliary definitions
		int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
		int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
		for(int j = 0; j < jlast; j++) {
			for(int i = 0; i < ilast; i++) {
				if ((i + 4 < ilast && i - 4 >= 0 && j + 4 < jlast && j - 4 >= 0)) {
					p1lnirho = lor1(vector(rk1rho, i - 4, j), vector(rk1rho, i - 3, j), vector(rk1rho, i - 2, j), vector(rk1rho, i - 1, j), dx, simPlat_dt, ilast, jlast);
					p1lnimx = lor1(vector(rk1mx, i - 4, j), vector(rk1mx, i - 3, j), vector(rk1mx, i - 2, j), vector(rk1mx, i - 1, j), dx, simPlat_dt, ilast, jlast);
					p1lnimy = lor1(vector(rk1my, i - 4, j), vector(rk1my, i - 3, j), vector(rk1my, i - 2, j), vector(rk1my, i - 1, j), dx, simPlat_dt, ilast, jlast);
					p1lnie = lor1(vector(rk1e, i - 4, j), vector(rk1e, i - 3, j), vector(rk1e, i - 2, j), vector(rk1e, i - 1, j), dx, simPlat_dt, ilast, jlast);
					p1lnjrho = lor1(vector(rk1rho, i, j - 4), vector(rk1rho, i, j - 3), vector(rk1rho, i, j - 2), vector(rk1rho, i, j - 1), dx, simPlat_dt, ilast, jlast);
					p1lnjmx = lor1(vector(rk1mx, i, j - 4), vector(rk1mx, i, j - 3), vector(rk1mx, i, j - 2), vector(rk1mx, i, j - 1), dx, simPlat_dt, ilast, jlast);
					p1lnjmy = lor1(vector(rk1my, i, j - 4), vector(rk1my, i, j - 3), vector(rk1my, i, j - 2), vector(rk1my, i, j - 1), dx, simPlat_dt, ilast, jlast);
					p1lnje = lor1(vector(rk1e, i, j - 4), vector(rk1e, i, j - 3), vector(rk1e, i, j - 2), vector(rk1e, i, j - 1), dx, simPlat_dt, ilast, jlast);
					p2lnirho = lor2(vector(rk1rho, i - 3, j), vector(rk1rho, i - 2, j), vector(rk1rho, i - 1, j), vector(rk1rho, i, j), dx, simPlat_dt, ilast, jlast);
					p2lnimx = lor2(vector(rk1mx, i - 3, j), vector(rk1mx, i - 2, j), vector(rk1mx, i - 1, j), vector(rk1mx, i, j), dx, simPlat_dt, ilast, jlast);
					p2lnimy = lor2(vector(rk1my, i - 3, j), vector(rk1my, i - 2, j), vector(rk1my, i - 1, j), vector(rk1my, i, j), dx, simPlat_dt, ilast, jlast);
					p2lnie = lor2(vector(rk1e, i - 3, j), vector(rk1e, i - 2, j), vector(rk1e, i - 1, j), vector(rk1e, i, j), dx, simPlat_dt, ilast, jlast);
					p2lnjrho = lor2(vector(rk1rho, i, j - 3), vector(rk1rho, i, j - 2), vector(rk1rho, i, j - 1), vector(rk1rho, i, j), dx, simPlat_dt, ilast, jlast);
					p2lnjmx = lor2(vector(rk1mx, i, j - 3), vector(rk1mx, i, j - 2), vector(rk1mx, i, j - 1), vector(rk1mx, i, j), dx, simPlat_dt, ilast, jlast);
					p2lnjmy = lor2(vector(rk1my, i, j - 3), vector(rk1my, i, j - 2), vector(rk1my, i, j - 1), vector(rk1my, i, j), dx, simPlat_dt, ilast, jlast);
					p2lnje = lor2(vector(rk1e, i, j - 3), vector(rk1e, i, j - 2), vector(rk1e, i, j - 1), vector(rk1e, i, j), dx, simPlat_dt, ilast, jlast);
					p3lnirho = lor3(vector(rk1rho, i - 2, j), vector(rk1rho, i - 1, j), vector(rk1rho, i, j), vector(rk1rho, i + 1, j), dx, simPlat_dt, ilast, jlast);
					p3lnimx = lor3(vector(rk1mx, i - 2, j), vector(rk1mx, i - 1, j), vector(rk1mx, i, j), vector(rk1mx, i + 1, j), dx, simPlat_dt, ilast, jlast);
					p3lnimy = lor3(vector(rk1my, i - 2, j), vector(rk1my, i - 1, j), vector(rk1my, i, j), vector(rk1my, i + 1, j), dx, simPlat_dt, ilast, jlast);
					p3lnie = lor3(vector(rk1e, i - 2, j), vector(rk1e, i - 1, j), vector(rk1e, i, j), vector(rk1e, i + 1, j), dx, simPlat_dt, ilast, jlast);
					p3lnjrho = lor3(vector(rk1rho, i, j - 2), vector(rk1rho, i, j - 1), vector(rk1rho, i, j), vector(rk1rho, i, j + 1), dx, simPlat_dt, ilast, jlast);
					p3lnjmx = lor3(vector(rk1mx, i, j - 2), vector(rk1mx, i, j - 1), vector(rk1mx, i, j), vector(rk1mx, i, j + 1), dx, simPlat_dt, ilast, jlast);
					p3lnjmy = lor3(vector(rk1my, i, j - 2), vector(rk1my, i, j - 1), vector(rk1my, i, j), vector(rk1my, i, j + 1), dx, simPlat_dt, ilast, jlast);
					p3lnje = lor3(vector(rk1e, i, j - 2), vector(rk1e, i, j - 1), vector(rk1e, i, j), vector(rk1e, i, j + 1), dx, simPlat_dt, ilast, jlast);
					p4lnirho = lor4(vector(rk1rho, i - 1, j), vector(rk1rho, i, j), vector(rk1rho, i + 1, j), vector(rk1rho, i + 2, j), dx, simPlat_dt, ilast, jlast);
					p4lnimx = lor4(vector(rk1mx, i - 1, j), vector(rk1mx, i, j), vector(rk1mx, i + 1, j), vector(rk1mx, i + 2, j), dx, simPlat_dt, ilast, jlast);
					p4lnimy = lor4(vector(rk1my, i - 1, j), vector(rk1my, i, j), vector(rk1my, i + 1, j), vector(rk1my, i + 2, j), dx, simPlat_dt, ilast, jlast);
					p4lnie = lor4(vector(rk1e, i - 1, j), vector(rk1e, i, j), vector(rk1e, i + 1, j), vector(rk1e, i + 2, j), dx, simPlat_dt, ilast, jlast);
					p4lnjrho = lor4(vector(rk1rho, i, j - 1), vector(rk1rho, i, j), vector(rk1rho, i, j + 1), vector(rk1rho, i, j + 2), dx, simPlat_dt, ilast, jlast);
					p4lnjmx = lor4(vector(rk1mx, i, j - 1), vector(rk1mx, i, j), vector(rk1mx, i, j + 1), vector(rk1mx, i, j + 2), dx, simPlat_dt, ilast, jlast);
					p4lnjmy = lor4(vector(rk1my, i, j - 1), vector(rk1my, i, j), vector(rk1my, i, j + 1), vector(rk1my, i, j + 2), dx, simPlat_dt, ilast, jlast);
					p4lnje = lor4(vector(rk1e, i, j - 1), vector(rk1e, i, j), vector(rk1e, i, j + 1), vector(rk1e, i, j + 2), dx, simPlat_dt, ilast, jlast);
					p1rnirho = lor1(vector(rk1rho, i + 3, j), vector(rk1rho, i + 2, j), vector(rk1rho, i + 1, j), vector(rk1rho, i, j), dx, simPlat_dt, ilast, jlast);
					p1rnimx = lor1(vector(rk1mx, i + 3, j), vector(rk1mx, i + 2, j), vector(rk1mx, i + 1, j), vector(rk1mx, i, j), dx, simPlat_dt, ilast, jlast);
					p1rnimy = lor1(vector(rk1my, i + 3, j), vector(rk1my, i + 2, j), vector(rk1my, i + 1, j), vector(rk1my, i, j), dx, simPlat_dt, ilast, jlast);
					p1rnie = lor1(vector(rk1e, i + 3, j), vector(rk1e, i + 2, j), vector(rk1e, i + 1, j), vector(rk1e, i, j), dx, simPlat_dt, ilast, jlast);
					p1rnjrho = lor1(vector(rk1rho, i, j + 3), vector(rk1rho, i, j + 2), vector(rk1rho, i, j + 1), vector(rk1rho, i, j), dx, simPlat_dt, ilast, jlast);
					p1rnjmx = lor1(vector(rk1mx, i, j + 3), vector(rk1mx, i, j + 2), vector(rk1mx, i, j + 1), vector(rk1mx, i, j), dx, simPlat_dt, ilast, jlast);
					p1rnjmy = lor1(vector(rk1my, i, j + 3), vector(rk1my, i, j + 2), vector(rk1my, i, j + 1), vector(rk1my, i, j), dx, simPlat_dt, ilast, jlast);
					p1rnje = lor1(vector(rk1e, i, j + 3), vector(rk1e, i, j + 2), vector(rk1e, i, j + 1), vector(rk1e, i, j), dx, simPlat_dt, ilast, jlast);
					p2rnirho = lor2(vector(rk1rho, i + 2, j), vector(rk1rho, i + 1, j), vector(rk1rho, i, j), vector(rk1rho, i - 1, j), dx, simPlat_dt, ilast, jlast);
					p2rnimx = lor2(vector(rk1mx, i + 2, j), vector(rk1mx, i + 1, j), vector(rk1mx, i, j), vector(rk1mx, i - 1, j), dx, simPlat_dt, ilast, jlast);
					p2rnimy = lor2(vector(rk1my, i + 2, j), vector(rk1my, i + 1, j), vector(rk1my, i, j), vector(rk1my, i - 1, j), dx, simPlat_dt, ilast, jlast);
					p2rnie = lor2(vector(rk1e, i + 2, j), vector(rk1e, i + 1, j), vector(rk1e, i, j), vector(rk1e, i - 1, j), dx, simPlat_dt, ilast, jlast);
					p2rnjrho = lor2(vector(rk1rho, i, j + 2), vector(rk1rho, i, j + 1), vector(rk1rho, i, j), vector(rk1rho, i, j - 1), dx, simPlat_dt, ilast, jlast);
					p2rnjmx = lor2(vector(rk1mx, i, j + 2), vector(rk1mx, i, j + 1), vector(rk1mx, i, j), vector(rk1mx, i, j - 1), dx, simPlat_dt, ilast, jlast);
					p2rnjmy = lor2(vector(rk1my, i, j + 2), vector(rk1my, i, j + 1), vector(rk1my, i, j), vector(rk1my, i, j - 1), dx, simPlat_dt, ilast, jlast);
					p2rnje = lor2(vector(rk1e, i, j + 2), vector(rk1e, i, j + 1), vector(rk1e, i, j), vector(rk1e, i, j - 1), dx, simPlat_dt, ilast, jlast);
					p3rnirho = lor3(vector(rk1rho, i + 1, j), vector(rk1rho, i, j), vector(rk1rho, i - 1, j), vector(rk1rho, i - 2, j), dx, simPlat_dt, ilast, jlast);
					p3rnimx = lor3(vector(rk1mx, i + 1, j), vector(rk1mx, i, j), vector(rk1mx, i - 1, j), vector(rk1mx, i - 2, j), dx, simPlat_dt, ilast, jlast);
					p3rnimy = lor3(vector(rk1my, i + 1, j), vector(rk1my, i, j), vector(rk1my, i - 1, j), vector(rk1my, i - 2, j), dx, simPlat_dt, ilast, jlast);
					p3rnie = lor3(vector(rk1e, i + 1, j), vector(rk1e, i, j), vector(rk1e, i - 1, j), vector(rk1e, i - 2, j), dx, simPlat_dt, ilast, jlast);
					p3rnjrho = lor3(vector(rk1rho, i, j + 1), vector(rk1rho, i, j), vector(rk1rho, i, j - 1), vector(rk1rho, i, j - 2), dx, simPlat_dt, ilast, jlast);
					p3rnjmx = lor3(vector(rk1mx, i, j + 1), vector(rk1mx, i, j), vector(rk1mx, i, j - 1), vector(rk1mx, i, j - 2), dx, simPlat_dt, ilast, jlast);
					p3rnjmy = lor3(vector(rk1my, i, j + 1), vector(rk1my, i, j), vector(rk1my, i, j - 1), vector(rk1my, i, j - 2), dx, simPlat_dt, ilast, jlast);
					p3rnje = lor3(vector(rk1e, i, j + 1), vector(rk1e, i, j), vector(rk1e, i, j - 1), vector(rk1e, i, j - 2), dx, simPlat_dt, ilast, jlast);
					p4rnirho = lor4(vector(rk1rho, i, j), vector(rk1rho, i - 1, j), vector(rk1rho, i - 2, j), vector(rk1rho, i - 3, j), dx, simPlat_dt, ilast, jlast);
					p4rnimx = lor4(vector(rk1mx, i, j), vector(rk1mx, i - 1, j), vector(rk1mx, i - 2, j), vector(rk1mx, i - 3, j), dx, simPlat_dt, ilast, jlast);
					p4rnimy = lor4(vector(rk1my, i, j), vector(rk1my, i - 1, j), vector(rk1my, i - 2, j), vector(rk1my, i - 3, j), dx, simPlat_dt, ilast, jlast);
					p4rnie = lor4(vector(rk1e, i, j), vector(rk1e, i - 1, j), vector(rk1e, i - 2, j), vector(rk1e, i - 3, j), dx, simPlat_dt, ilast, jlast);
					p4rnjrho = lor4(vector(rk1rho, i, j), vector(rk1rho, i, j - 1), vector(rk1rho, i, j - 2), vector(rk1rho, i, j - 3), dx, simPlat_dt, ilast, jlast);
					p4rnjmx = lor4(vector(rk1mx, i, j), vector(rk1mx, i, j - 1), vector(rk1mx, i, j - 2), vector(rk1mx, i, j - 3), dx, simPlat_dt, ilast, jlast);
					p4rnjmy = lor4(vector(rk1my, i, j), vector(rk1my, i, j - 1), vector(rk1my, i, j - 2), vector(rk1my, i, j - 3), dx, simPlat_dt, ilast, jlast);
					p4rnje = lor4(vector(rk1e, i, j), vector(rk1e, i, j - 1), vector(rk1e, i, j - 2), vector(rk1e, i, j - 3), dx, simPlat_dt, ilast, jlast);
					beta1lnirho = smooth1(vector(rk1rho, i - 4, j), vector(rk1rho, i - 3, j), vector(rk1rho, i - 2, j), vector(rk1rho, i - 1, j), dx, simPlat_dt, ilast, jlast);
					beta1lnimx = smooth1(vector(rk1mx, i - 4, j), vector(rk1mx, i - 3, j), vector(rk1mx, i - 2, j), vector(rk1mx, i - 1, j), dx, simPlat_dt, ilast, jlast);
					beta1lnimy = smooth1(vector(rk1my, i - 4, j), vector(rk1my, i - 3, j), vector(rk1my, i - 2, j), vector(rk1my, i - 1, j), dx, simPlat_dt, ilast, jlast);
					beta1lnie = smooth1(vector(rk1e, i - 4, j), vector(rk1e, i - 3, j), vector(rk1e, i - 2, j), vector(rk1e, i - 1, j), dx, simPlat_dt, ilast, jlast);
					beta1lnjrho = smooth1(vector(rk1rho, i, j - 4), vector(rk1rho, i, j - 3), vector(rk1rho, i, j - 2), vector(rk1rho, i, j - 1), dx, simPlat_dt, ilast, jlast);
					beta1lnjmx = smooth1(vector(rk1mx, i, j - 4), vector(rk1mx, i, j - 3), vector(rk1mx, i, j - 2), vector(rk1mx, i, j - 1), dx, simPlat_dt, ilast, jlast);
					beta1lnjmy = smooth1(vector(rk1my, i, j - 4), vector(rk1my, i, j - 3), vector(rk1my, i, j - 2), vector(rk1my, i, j - 1), dx, simPlat_dt, ilast, jlast);
					beta1lnje = smooth1(vector(rk1e, i, j - 4), vector(rk1e, i, j - 3), vector(rk1e, i, j - 2), vector(rk1e, i, j - 1), dx, simPlat_dt, ilast, jlast);
					beta2lnirho = smooth2(vector(rk1rho, i - 3, j), vector(rk1rho, i - 2, j), vector(rk1rho, i - 1, j), vector(rk1rho, i, j), dx, simPlat_dt, ilast, jlast);
					beta2lnimx = smooth2(vector(rk1mx, i - 3, j), vector(rk1mx, i - 2, j), vector(rk1mx, i - 1, j), vector(rk1mx, i, j), dx, simPlat_dt, ilast, jlast);
					beta2lnimy = smooth2(vector(rk1my, i - 3, j), vector(rk1my, i - 2, j), vector(rk1my, i - 1, j), vector(rk1my, i, j), dx, simPlat_dt, ilast, jlast);
					beta2lnie = smooth2(vector(rk1e, i - 3, j), vector(rk1e, i - 2, j), vector(rk1e, i - 1, j), vector(rk1e, i, j), dx, simPlat_dt, ilast, jlast);
					beta2lnjrho = smooth2(vector(rk1rho, i, j - 3), vector(rk1rho, i, j - 2), vector(rk1rho, i, j - 1), vector(rk1rho, i, j), dx, simPlat_dt, ilast, jlast);
					beta2lnjmx = smooth2(vector(rk1mx, i, j - 3), vector(rk1mx, i, j - 2), vector(rk1mx, i, j - 1), vector(rk1mx, i, j), dx, simPlat_dt, ilast, jlast);
					beta2lnjmy = smooth2(vector(rk1my, i, j - 3), vector(rk1my, i, j - 2), vector(rk1my, i, j - 1), vector(rk1my, i, j), dx, simPlat_dt, ilast, jlast);
					beta2lnje = smooth2(vector(rk1e, i, j - 3), vector(rk1e, i, j - 2), vector(rk1e, i, j - 1), vector(rk1e, i, j), dx, simPlat_dt, ilast, jlast);
					beta3lnirho = smooth3(vector(rk1rho, i - 2, j), vector(rk1rho, i - 1, j), vector(rk1rho, i, j), vector(rk1rho, i + 1, j), dx, simPlat_dt, ilast, jlast);
					beta3lnimx = smooth3(vector(rk1mx, i - 2, j), vector(rk1mx, i - 1, j), vector(rk1mx, i, j), vector(rk1mx, i + 1, j), dx, simPlat_dt, ilast, jlast);
					beta3lnimy = smooth3(vector(rk1my, i - 2, j), vector(rk1my, i - 1, j), vector(rk1my, i, j), vector(rk1my, i + 1, j), dx, simPlat_dt, ilast, jlast);
					beta3lnie = smooth3(vector(rk1e, i - 2, j), vector(rk1e, i - 1, j), vector(rk1e, i, j), vector(rk1e, i + 1, j), dx, simPlat_dt, ilast, jlast);
					beta3lnjrho = smooth3(vector(rk1rho, i, j - 2), vector(rk1rho, i, j - 1), vector(rk1rho, i, j), vector(rk1rho, i, j + 1), dx, simPlat_dt, ilast, jlast);
					beta3lnjmx = smooth3(vector(rk1mx, i, j - 2), vector(rk1mx, i, j - 1), vector(rk1mx, i, j), vector(rk1mx, i, j + 1), dx, simPlat_dt, ilast, jlast);
					beta3lnjmy = smooth3(vector(rk1my, i, j - 2), vector(rk1my, i, j - 1), vector(rk1my, i, j), vector(rk1my, i, j + 1), dx, simPlat_dt, ilast, jlast);
					beta3lnje = smooth3(vector(rk1e, i, j - 2), vector(rk1e, i, j - 1), vector(rk1e, i, j), vector(rk1e, i, j + 1), dx, simPlat_dt, ilast, jlast);
					beta4lnirho = smooth4(vector(rk1rho, i - 1, j), vector(rk1rho, i, j), vector(rk1rho, i + 1, j), vector(rk1rho, i + 2, j), dx, simPlat_dt, ilast, jlast);
					beta4lnimx = smooth4(vector(rk1mx, i - 1, j), vector(rk1mx, i, j), vector(rk1mx, i + 1, j), vector(rk1mx, i + 2, j), dx, simPlat_dt, ilast, jlast);
					beta4lnimy = smooth4(vector(rk1my, i - 1, j), vector(rk1my, i, j), vector(rk1my, i + 1, j), vector(rk1my, i + 2, j), dx, simPlat_dt, ilast, jlast);
					beta4lnie = smooth4(vector(rk1e, i - 1, j), vector(rk1e, i, j), vector(rk1e, i + 1, j), vector(rk1e, i + 2, j), dx, simPlat_dt, ilast, jlast);
					beta4lnjrho = smooth4(vector(rk1rho, i, j - 1), vector(rk1rho, i, j), vector(rk1rho, i, j + 1), vector(rk1rho, i, j + 2), dx, simPlat_dt, ilast, jlast);
					beta4lnjmx = smooth4(vector(rk1mx, i, j - 1), vector(rk1mx, i, j), vector(rk1mx, i, j + 1), vector(rk1mx, i, j + 2), dx, simPlat_dt, ilast, jlast);
					beta4lnjmy = smooth4(vector(rk1my, i, j - 1), vector(rk1my, i, j), vector(rk1my, i, j + 1), vector(rk1my, i, j + 2), dx, simPlat_dt, ilast, jlast);
					beta4lnje = smooth4(vector(rk1e, i, j - 1), vector(rk1e, i, j), vector(rk1e, i, j + 1), vector(rk1e, i, j + 2), dx, simPlat_dt, ilast, jlast);
					beta1rnirho = smooth1(vector(rk1rho, i + 3, j), vector(rk1rho, i + 2, j), vector(rk1rho, i + 1, j), vector(rk1rho, i, j), dx, simPlat_dt, ilast, jlast);
					beta1rnimx = smooth1(vector(rk1mx, i + 3, j), vector(rk1mx, i + 2, j), vector(rk1mx, i + 1, j), vector(rk1mx, i, j), dx, simPlat_dt, ilast, jlast);
					beta1rnimy = smooth1(vector(rk1my, i + 3, j), vector(rk1my, i + 2, j), vector(rk1my, i + 1, j), vector(rk1my, i, j), dx, simPlat_dt, ilast, jlast);
					beta1rnie = smooth1(vector(rk1e, i + 3, j), vector(rk1e, i + 2, j), vector(rk1e, i + 1, j), vector(rk1e, i, j), dx, simPlat_dt, ilast, jlast);
					beta1rnjrho = smooth1(vector(rk1rho, i, j + 3), vector(rk1rho, i, j + 2), vector(rk1rho, i, j + 1), vector(rk1rho, i, j), dx, simPlat_dt, ilast, jlast);
					beta1rnjmx = smooth1(vector(rk1mx, i, j + 3), vector(rk1mx, i, j + 2), vector(rk1mx, i, j + 1), vector(rk1mx, i, j), dx, simPlat_dt, ilast, jlast);
					beta1rnjmy = smooth1(vector(rk1my, i, j + 3), vector(rk1my, i, j + 2), vector(rk1my, i, j + 1), vector(rk1my, i, j), dx, simPlat_dt, ilast, jlast);
					beta1rnje = smooth1(vector(rk1e, i, j + 3), vector(rk1e, i, j + 2), vector(rk1e, i, j + 1), vector(rk1e, i, j), dx, simPlat_dt, ilast, jlast);
					beta2rnirho = smooth2(vector(rk1rho, i + 2, j), vector(rk1rho, i + 1, j), vector(rk1rho, i, j), vector(rk1rho, i - 1, j), dx, simPlat_dt, ilast, jlast);
					beta2rnimx = smooth2(vector(rk1mx, i + 2, j), vector(rk1mx, i + 1, j), vector(rk1mx, i, j), vector(rk1mx, i - 1, j), dx, simPlat_dt, ilast, jlast);
					beta2rnimy = smooth2(vector(rk1my, i + 2, j), vector(rk1my, i + 1, j), vector(rk1my, i, j), vector(rk1my, i - 1, j), dx, simPlat_dt, ilast, jlast);
					beta2rnie = smooth2(vector(rk1e, i + 2, j), vector(rk1e, i + 1, j), vector(rk1e, i, j), vector(rk1e, i - 1, j), dx, simPlat_dt, ilast, jlast);
					beta2rnjrho = smooth2(vector(rk1rho, i, j + 2), vector(rk1rho, i, j + 1), vector(rk1rho, i, j), vector(rk1rho, i, j - 1), dx, simPlat_dt, ilast, jlast);
					beta2rnjmx = smooth2(vector(rk1mx, i, j + 2), vector(rk1mx, i, j + 1), vector(rk1mx, i, j), vector(rk1mx, i, j - 1), dx, simPlat_dt, ilast, jlast);
					beta2rnjmy = smooth2(vector(rk1my, i, j + 2), vector(rk1my, i, j + 1), vector(rk1my, i, j), vector(rk1my, i, j - 1), dx, simPlat_dt, ilast, jlast);
					beta2rnje = smooth2(vector(rk1e, i, j + 2), vector(rk1e, i, j + 1), vector(rk1e, i, j), vector(rk1e, i, j - 1), dx, simPlat_dt, ilast, jlast);
					beta3rnirho = smooth3(vector(rk1rho, i + 1, j), vector(rk1rho, i, j), vector(rk1rho, i - 1, j), vector(rk1rho, i - 2, j), dx, simPlat_dt, ilast, jlast);
					beta3rnimx = smooth3(vector(rk1mx, i + 1, j), vector(rk1mx, i, j), vector(rk1mx, i - 1, j), vector(rk1mx, i - 2, j), dx, simPlat_dt, ilast, jlast);
					beta3rnimy = smooth3(vector(rk1my, i + 1, j), vector(rk1my, i, j), vector(rk1my, i - 1, j), vector(rk1my, i - 2, j), dx, simPlat_dt, ilast, jlast);
					beta3rnie = smooth3(vector(rk1e, i + 1, j), vector(rk1e, i, j), vector(rk1e, i - 1, j), vector(rk1e, i - 2, j), dx, simPlat_dt, ilast, jlast);
					beta3rnjrho = smooth3(vector(rk1rho, i, j + 1), vector(rk1rho, i, j), vector(rk1rho, i, j - 1), vector(rk1rho, i, j - 2), dx, simPlat_dt, ilast, jlast);
					beta3rnjmx = smooth3(vector(rk1mx, i, j + 1), vector(rk1mx, i, j), vector(rk1mx, i, j - 1), vector(rk1mx, i, j - 2), dx, simPlat_dt, ilast, jlast);
					beta3rnjmy = smooth3(vector(rk1my, i, j + 1), vector(rk1my, i, j), vector(rk1my, i, j - 1), vector(rk1my, i, j - 2), dx, simPlat_dt, ilast, jlast);
					beta3rnje = smooth3(vector(rk1e, i, j + 1), vector(rk1e, i, j), vector(rk1e, i, j - 1), vector(rk1e, i, j - 2), dx, simPlat_dt, ilast, jlast);
					beta4rnirho = smooth4(vector(rk1rho, i, j), vector(rk1rho, i - 1, j), vector(rk1rho, i - 2, j), vector(rk1rho, i - 3, j), dx, simPlat_dt, ilast, jlast);
					beta4rnimx = smooth4(vector(rk1mx, i, j), vector(rk1mx, i - 1, j), vector(rk1mx, i - 2, j), vector(rk1mx, i - 3, j), dx, simPlat_dt, ilast, jlast);
					beta4rnimy = smooth4(vector(rk1my, i, j), vector(rk1my, i - 1, j), vector(rk1my, i - 2, j), vector(rk1my, i - 3, j), dx, simPlat_dt, ilast, jlast);
					beta4rnie = smooth4(vector(rk1e, i, j), vector(rk1e, i - 1, j), vector(rk1e, i - 2, j), vector(rk1e, i - 3, j), dx, simPlat_dt, ilast, jlast);
					beta4rnjrho = smooth4(vector(rk1rho, i, j), vector(rk1rho, i, j - 1), vector(rk1rho, i, j - 2), vector(rk1rho, i, j - 3), dx, simPlat_dt, ilast, jlast);
					beta4rnjmx = smooth4(vector(rk1mx, i, j), vector(rk1mx, i, j - 1), vector(rk1mx, i, j - 2), vector(rk1mx, i, j - 3), dx, simPlat_dt, ilast, jlast);
					beta4rnjmy = smooth4(vector(rk1my, i, j), vector(rk1my, i, j - 1), vector(rk1my, i, j - 2), vector(rk1my, i, j - 3), dx, simPlat_dt, ilast, jlast);
					beta4rnje = smooth4(vector(rk1e, i, j), vector(rk1e, i, j - 1), vector(rk1e, i, j - 2), vector(rk1e, i, j - 3), dx, simPlat_dt, ilast, jlast);
					w1lnirho = nlw1(beta1lnirho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1lnimx = nlw1(beta1lnimx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1lnimy = nlw1(beta1lnimy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1lnie = nlw1(beta1lnie, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1lnjrho = nlw1(beta1lnjrho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1lnjmx = nlw1(beta1lnjmx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1lnjmy = nlw1(beta1lnjmy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1lnje = nlw1(beta1lnje, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2lnirho = nlw2(beta2lnirho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2lnimx = nlw2(beta2lnimx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2lnimy = nlw2(beta2lnimy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2lnie = nlw2(beta2lnie, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2lnjrho = nlw2(beta2lnjrho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2lnjmx = nlw2(beta2lnjmx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2lnjmy = nlw2(beta2lnjmy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2lnje = nlw2(beta2lnje, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3lnirho = nlw3(beta3lnirho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3lnimx = nlw3(beta3lnimx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3lnimy = nlw3(beta3lnimy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3lnie = nlw3(beta3lnie, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3lnjrho = nlw3(beta3lnjrho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3lnjmx = nlw3(beta3lnjmx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3lnjmy = nlw3(beta3lnjmy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3lnje = nlw3(beta3lnje, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4lnirho = nlw4(beta4lnirho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4lnimx = nlw4(beta4lnimx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4lnimy = nlw4(beta4lnimy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4lnie = nlw4(beta4lnie, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4lnjrho = nlw4(beta4lnjrho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4lnjmx = nlw4(beta4lnjmx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4lnjmy = nlw4(beta4lnjmy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4lnje = nlw4(beta4lnje, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1rnirho = nlw1(beta1rnirho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1rnimx = nlw1(beta1rnimx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1rnimy = nlw1(beta1rnimy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1rnie = nlw1(beta1rnie, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1rnjrho = nlw1(beta1rnjrho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1rnjmx = nlw1(beta1rnjmx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1rnjmy = nlw1(beta1rnjmy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1rnje = nlw1(beta1rnje, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2rnirho = nlw2(beta2rnirho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2rnimx = nlw2(beta2rnimx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2rnimy = nlw2(beta2rnimy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2rnie = nlw2(beta2rnie, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2rnjrho = nlw2(beta2rnjrho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2rnjmx = nlw2(beta2rnjmx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2rnjmy = nlw2(beta2rnjmy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2rnje = nlw2(beta2rnje, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3rnirho = nlw3(beta3rnirho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3rnimx = nlw3(beta3rnimx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3rnimy = nlw3(beta3rnimy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3rnie = nlw3(beta3rnie, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3rnjrho = nlw3(beta3rnjrho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3rnjmx = nlw3(beta3rnjmx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3rnjmy = nlw3(beta3rnjmy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3rnje = nlw3(beta3rnje, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4rnirho = nlw4(beta4rnirho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4rnimx = nlw4(beta4rnimx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4rnimy = nlw4(beta4rnimy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4rnie = nlw4(beta4rnie, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4rnjrho = nlw4(beta4rnjrho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4rnjmx = nlw4(beta4rnjmx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4rnjmy = nlw4(beta4rnjmy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4rnje = nlw4(beta4rnje, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					wlnirho = w1lnirho + w2lnirho + w3lnirho + w4lnirho;
					wlnimx = w1lnimx + w2lnimx + w3lnimx + w4lnimx;
					wlnimy = w1lnimy + w2lnimy + w3lnimy + w4lnimy;
					wlnie = w1lnie + w2lnie + w3lnie + w4lnie;
					wlnjrho = w1lnjrho + w2lnjrho + w3lnjrho + w4lnjrho;
					wlnjmx = w1lnjmx + w2lnjmx + w3lnjmx + w4lnjmx;
					wlnjmy = w1lnjmy + w2lnjmy + w3lnjmy + w4lnjmy;
					wlnje = w1lnje + w2lnje + w3lnje + w4lnje;
					wrnirho = w1rnirho + w2rnirho + w3rnirho + w4rnirho;
					wrnimx = w1rnimx + w2rnimx + w3rnimx + w4rnimx;
					wrnimy = w1rnimy + w2rnimy + w3rnimy + w4rnimy;
					wrnie = w1rnie + w2rnie + w3rnie + w4rnie;
					wrnjrho = w1rnjrho + w2rnjrho + w3rnjrho + w4rnjrho;
					wrnjmx = w1rnjmx + w2rnjmx + w3rnjmx + w4rnjmx;
					wrnjmy = w1rnjmy + w2rnjmy + w3rnjmy + w4rnjmy;
					wrnje = w1rnje + w2rnje + w3rnje + w4rnje;
					w1lnirho = w1lnirho / wlnirho;
					w1lnimx = w1lnimx / wlnimx;
					w1lnimy = w1lnimy / wlnimy;
					w1lnie = w1lnie / wlnie;
					w1lnjrho = w1lnjrho / wlnjrho;
					w1lnjmx = w1lnjmx / wlnjmx;
					w1lnjmy = w1lnjmy / wlnjmy;
					w1lnje = w1lnje / wlnje;
					w2lnirho = w2lnirho / wlnirho;
					w2lnimx = w2lnimx / wlnimx;
					w2lnimy = w2lnimy / wlnimy;
					w2lnie = w2lnie / wlnie;
					w2lnjrho = w2lnjrho / wlnjrho;
					w2lnjmx = w2lnjmx / wlnjmx;
					w2lnjmy = w2lnjmy / wlnjmy;
					w2lnje = w2lnje / wlnje;
					w3lnirho = w3lnirho / wlnirho;
					w3lnimx = w3lnimx / wlnimx;
					w3lnimy = w3lnimy / wlnimy;
					w3lnie = w3lnie / wlnie;
					w3lnjrho = w3lnjrho / wlnjrho;
					w3lnjmx = w3lnjmx / wlnjmx;
					w3lnjmy = w3lnjmy / wlnjmy;
					w3lnje = w3lnje / wlnje;
					w4lnirho = w4lnirho / wlnirho;
					w4lnimx = w4lnimx / wlnimx;
					w4lnimy = w4lnimy / wlnimy;
					w4lnie = w4lnie / wlnie;
					w4lnjrho = w4lnjrho / wlnjrho;
					w4lnjmx = w4lnjmx / wlnjmx;
					w4lnjmy = w4lnjmy / wlnjmy;
					w4lnje = w4lnje / wlnje;
					w1rnirho = w1rnirho / wrnirho;
					w1rnimx = w1rnimx / wrnimx;
					w1rnimy = w1rnimy / wrnimy;
					w1rnie = w1rnie / wrnie;
					w1rnjrho = w1rnjrho / wrnjrho;
					w1rnjmx = w1rnjmx / wrnjmx;
					w1rnjmy = w1rnjmy / wrnjmy;
					w1rnje = w1rnje / wrnje;
					w2rnirho = w2rnirho / wrnirho;
					w2rnimx = w2rnimx / wrnimx;
					w2rnimy = w2rnimy / wrnimy;
					w2rnie = w2rnie / wrnie;
					w2rnjrho = w2rnjrho / wrnjrho;
					w2rnjmx = w2rnjmx / wrnjmx;
					w2rnjmy = w2rnjmy / wrnjmy;
					w2rnje = w2rnje / wrnje;
					w3rnirho = w3rnirho / wrnirho;
					w3rnimx = w3rnimx / wrnimx;
					w3rnimy = w3rnimy / wrnimy;
					w3rnie = w3rnie / wrnie;
					w3rnjrho = w3rnjrho / wrnjrho;
					w3rnjmx = w3rnjmx / wrnjmx;
					w3rnjmy = w3rnjmy / wrnjmy;
					w3rnje = w3rnje / wrnje;
					w4rnirho = w4rnirho / wrnirho;
					w4rnimx = w4rnimx / wrnimx;
					w4rnimy = w4rnimy / wrnimy;
					w4rnie = w4rnie / wrnie;
					w4rnjrho = w4rnjrho / wrnjrho;
					w4rnjmx = w4rnjmx / wrnjmx;
					w4rnjmy = w4rnjmy / wrnjmy;
					w4rnje = w4rnje / wrnje;
					rho_lni = weno(w1lnirho, w2lnirho, w3lnirho, w4lnirho, p1lnirho, p2lnirho, p3lnirho, p4lnirho, dx, simPlat_dt, ilast, jlast);
					mx_lni = weno(w1lnimx, w2lnimx, w3lnimx, w4lnimx, p1lnimx, p2lnimx, p3lnimx, p4lnimx, dx, simPlat_dt, ilast, jlast);
					my_lni = weno(w1lnimy, w2lnimy, w3lnimy, w4lnimy, p1lnimy, p2lnimy, p3lnimy, p4lnimy, dx, simPlat_dt, ilast, jlast);
					e_lni = weno(w1lnie, w2lnie, w3lnie, w4lnie, p1lnie, p2lnie, p3lnie, p4lnie, dx, simPlat_dt, ilast, jlast);
					rho_lnj = weno(w1lnjrho, w2lnjrho, w3lnjrho, w4lnjrho, p1lnjrho, p2lnjrho, p3lnjrho, p4lnjrho, dx, simPlat_dt, ilast, jlast);
					mx_lnj = weno(w1lnjmx, w2lnjmx, w3lnjmx, w4lnjmx, p1lnjmx, p2lnjmx, p3lnjmx, p4lnjmx, dx, simPlat_dt, ilast, jlast);
					my_lnj = weno(w1lnjmy, w2lnjmy, w3lnjmy, w4lnjmy, p1lnjmy, p2lnjmy, p3lnjmy, p4lnjmy, dx, simPlat_dt, ilast, jlast);
					e_lnj = weno(w1lnje, w2lnje, w3lnje, w4lnje, p1lnje, p2lnje, p3lnje, p4lnje, dx, simPlat_dt, ilast, jlast);
					rho_rni = weno(w1rnirho, w2rnirho, w3rnirho, w4rnirho, p1rnirho, p2rnirho, p3rnirho, p4rnirho, dx, simPlat_dt, ilast, jlast);
					mx_rni = weno(w1rnimx, w2rnimx, w3rnimx, w4rnimx, p1rnimx, p2rnimx, p3rnimx, p4rnimx, dx, simPlat_dt, ilast, jlast);
					my_rni = weno(w1rnimy, w2rnimy, w3rnimy, w4rnimy, p1rnimy, p2rnimy, p3rnimy, p4rnimy, dx, simPlat_dt, ilast, jlast);
					e_rni = weno(w1rnie, w2rnie, w3rnie, w4rnie, p1rnie, p2rnie, p3rnie, p4rnie, dx, simPlat_dt, ilast, jlast);
					rho_rnj = weno(w1rnjrho, w2rnjrho, w3rnjrho, w4rnjrho, p1rnjrho, p2rnjrho, p3rnjrho, p4rnjrho, dx, simPlat_dt, ilast, jlast);
					mx_rnj = weno(w1rnjmx, w2rnjmx, w3rnjmx, w4rnjmx, p1rnjmx, p2rnjmx, p3rnjmx, p4rnjmx, dx, simPlat_dt, ilast, jlast);
					my_rnj = weno(w1rnjmy, w2rnjmy, w3rnjmy, w4rnjmy, p1rnjmy, p2rnjmy, p3rnjmy, p4rnjmy, dx, simPlat_dt, ilast, jlast);
					e_rnj = weno(w1rnje, w2rnje, w3rnje, w4rnje, p1rnje, p2rnje, p3rnje, p4rnje, dx, simPlat_dt, ilast, jlast);
					p1lpirho = lor1(vector(rk1rho, i - 3, j), vector(rk1rho, i - 2, j), vector(rk1rho, i - 1, j), vector(rk1rho, i, j), dx, simPlat_dt, ilast, jlast);
					p1lpimx = lor1(vector(rk1mx, i - 3, j), vector(rk1mx, i - 2, j), vector(rk1mx, i - 1, j), vector(rk1mx, i, j), dx, simPlat_dt, ilast, jlast);
					p1lpimy = lor1(vector(rk1my, i - 3, j), vector(rk1my, i - 2, j), vector(rk1my, i - 1, j), vector(rk1my, i, j), dx, simPlat_dt, ilast, jlast);
					p1lpie = lor1(vector(rk1e, i - 3, j), vector(rk1e, i - 2, j), vector(rk1e, i - 1, j), vector(rk1e, i, j), dx, simPlat_dt, ilast, jlast);
					p1lpjrho = lor1(vector(rk1rho, i, j - 3), vector(rk1rho, i, j - 2), vector(rk1rho, i, j - 1), vector(rk1rho, i, j), dx, simPlat_dt, ilast, jlast);
					p1lpjmx = lor1(vector(rk1mx, i, j - 3), vector(rk1mx, i, j - 2), vector(rk1mx, i, j - 1), vector(rk1mx, i, j), dx, simPlat_dt, ilast, jlast);
					p1lpjmy = lor1(vector(rk1my, i, j - 3), vector(rk1my, i, j - 2), vector(rk1my, i, j - 1), vector(rk1my, i, j), dx, simPlat_dt, ilast, jlast);
					p1lpje = lor1(vector(rk1e, i, j - 3), vector(rk1e, i, j - 2), vector(rk1e, i, j - 1), vector(rk1e, i, j), dx, simPlat_dt, ilast, jlast);
					p2lpirho = lor2(vector(rk1rho, i - 2, j), vector(rk1rho, i - 1, j), vector(rk1rho, i, j), vector(rk1rho, i + 1, j), dx, simPlat_dt, ilast, jlast);
					p2lpimx = lor2(vector(rk1mx, i - 2, j), vector(rk1mx, i - 1, j), vector(rk1mx, i, j), vector(rk1mx, i + 1, j), dx, simPlat_dt, ilast, jlast);
					p2lpimy = lor2(vector(rk1my, i - 2, j), vector(rk1my, i - 1, j), vector(rk1my, i, j), vector(rk1my, i + 1, j), dx, simPlat_dt, ilast, jlast);
					p2lpie = lor2(vector(rk1e, i - 2, j), vector(rk1e, i - 1, j), vector(rk1e, i, j), vector(rk1e, i + 1, j), dx, simPlat_dt, ilast, jlast);
					p2lpjrho = lor2(vector(rk1rho, i, j - 2), vector(rk1rho, i, j - 1), vector(rk1rho, i, j), vector(rk1rho, i, j + 1), dx, simPlat_dt, ilast, jlast);
					p2lpjmx = lor2(vector(rk1mx, i, j - 2), vector(rk1mx, i, j - 1), vector(rk1mx, i, j), vector(rk1mx, i, j + 1), dx, simPlat_dt, ilast, jlast);
					p2lpjmy = lor2(vector(rk1my, i, j - 2), vector(rk1my, i, j - 1), vector(rk1my, i, j), vector(rk1my, i, j + 1), dx, simPlat_dt, ilast, jlast);
					p2lpje = lor2(vector(rk1e, i, j - 2), vector(rk1e, i, j - 1), vector(rk1e, i, j), vector(rk1e, i, j + 1), dx, simPlat_dt, ilast, jlast);
					p3lpirho = lor3(vector(rk1rho, i - 1, j), vector(rk1rho, i, j), vector(rk1rho, i + 1, j), vector(rk1rho, i + 2, j), dx, simPlat_dt, ilast, jlast);
					p3lpimx = lor3(vector(rk1mx, i - 1, j), vector(rk1mx, i, j), vector(rk1mx, i + 1, j), vector(rk1mx, i + 2, j), dx, simPlat_dt, ilast, jlast);
					p3lpimy = lor3(vector(rk1my, i - 1, j), vector(rk1my, i, j), vector(rk1my, i + 1, j), vector(rk1my, i + 2, j), dx, simPlat_dt, ilast, jlast);
					p3lpie = lor3(vector(rk1e, i - 1, j), vector(rk1e, i, j), vector(rk1e, i + 1, j), vector(rk1e, i + 2, j), dx, simPlat_dt, ilast, jlast);
					p3lpjrho = lor3(vector(rk1rho, i, j - 1), vector(rk1rho, i, j), vector(rk1rho, i, j + 1), vector(rk1rho, i, j + 2), dx, simPlat_dt, ilast, jlast);
					p3lpjmx = lor3(vector(rk1mx, i, j - 1), vector(rk1mx, i, j), vector(rk1mx, i, j + 1), vector(rk1mx, i, j + 2), dx, simPlat_dt, ilast, jlast);
					p3lpjmy = lor3(vector(rk1my, i, j - 1), vector(rk1my, i, j), vector(rk1my, i, j + 1), vector(rk1my, i, j + 2), dx, simPlat_dt, ilast, jlast);
					p3lpje = lor3(vector(rk1e, i, j - 1), vector(rk1e, i, j), vector(rk1e, i, j + 1), vector(rk1e, i, j + 2), dx, simPlat_dt, ilast, jlast);
					p4lpirho = lor4(vector(rk1rho, i, j), vector(rk1rho, i + 1, j), vector(rk1rho, i + 2, j), vector(rk1rho, i + 3, j), dx, simPlat_dt, ilast, jlast);
					p4lpimx = lor4(vector(rk1mx, i, j), vector(rk1mx, i + 1, j), vector(rk1mx, i + 2, j), vector(rk1mx, i + 3, j), dx, simPlat_dt, ilast, jlast);
					p4lpimy = lor4(vector(rk1my, i, j), vector(rk1my, i + 1, j), vector(rk1my, i + 2, j), vector(rk1my, i + 3, j), dx, simPlat_dt, ilast, jlast);
					p4lpie = lor4(vector(rk1e, i, j), vector(rk1e, i + 1, j), vector(rk1e, i + 2, j), vector(rk1e, i + 3, j), dx, simPlat_dt, ilast, jlast);
					p4lpjrho = lor4(vector(rk1rho, i, j), vector(rk1rho, i, j + 1), vector(rk1rho, i, j + 2), vector(rk1rho, i, j + 3), dx, simPlat_dt, ilast, jlast);
					p4lpjmx = lor4(vector(rk1mx, i, j), vector(rk1mx, i, j + 1), vector(rk1mx, i, j + 2), vector(rk1mx, i, j + 3), dx, simPlat_dt, ilast, jlast);
					p4lpjmy = lor4(vector(rk1my, i, j), vector(rk1my, i, j + 1), vector(rk1my, i, j + 2), vector(rk1my, i, j + 3), dx, simPlat_dt, ilast, jlast);
					p4lpje = lor4(vector(rk1e, i, j), vector(rk1e, i, j + 1), vector(rk1e, i, j + 2), vector(rk1e, i, j + 3), dx, simPlat_dt, ilast, jlast);
					p1rpirho = lor1(vector(rk1rho, i + 4, j), vector(rk1rho, i + 3, j), vector(rk1rho, i + 2, j), vector(rk1rho, i + 1, j), dx, simPlat_dt, ilast, jlast);
					p1rpimx = lor1(vector(rk1mx, i + 4, j), vector(rk1mx, i + 3, j), vector(rk1mx, i + 2, j), vector(rk1mx, i + 1, j), dx, simPlat_dt, ilast, jlast);
					p1rpimy = lor1(vector(rk1my, i + 4, j), vector(rk1my, i + 3, j), vector(rk1my, i + 2, j), vector(rk1my, i + 1, j), dx, simPlat_dt, ilast, jlast);
					p1rpie = lor1(vector(rk1e, i + 4, j), vector(rk1e, i + 3, j), vector(rk1e, i + 2, j), vector(rk1e, i + 1, j), dx, simPlat_dt, ilast, jlast);
					p1rpjrho = lor1(vector(rk1rho, i, j + 4), vector(rk1rho, i, j + 3), vector(rk1rho, i, j + 2), vector(rk1rho, i, j + 1), dx, simPlat_dt, ilast, jlast);
					p1rpjmx = lor1(vector(rk1mx, i, j + 4), vector(rk1mx, i, j + 3), vector(rk1mx, i, j + 2), vector(rk1mx, i, j + 1), dx, simPlat_dt, ilast, jlast);
					p1rpjmy = lor1(vector(rk1my, i, j + 4), vector(rk1my, i, j + 3), vector(rk1my, i, j + 2), vector(rk1my, i, j + 1), dx, simPlat_dt, ilast, jlast);
					p1rpje = lor1(vector(rk1e, i, j + 4), vector(rk1e, i, j + 3), vector(rk1e, i, j + 2), vector(rk1e, i, j + 1), dx, simPlat_dt, ilast, jlast);
					p2rpirho = lor2(vector(rk1rho, i + 3, j), vector(rk1rho, i + 2, j), vector(rk1rho, i + 1, j), vector(rk1rho, i, j), dx, simPlat_dt, ilast, jlast);
					p2rpimx = lor2(vector(rk1mx, i + 3, j), vector(rk1mx, i + 2, j), vector(rk1mx, i + 1, j), vector(rk1mx, i, j), dx, simPlat_dt, ilast, jlast);
					p2rpimy = lor2(vector(rk1my, i + 3, j), vector(rk1my, i + 2, j), vector(rk1my, i + 1, j), vector(rk1my, i, j), dx, simPlat_dt, ilast, jlast);
					p2rpie = lor2(vector(rk1e, i + 3, j), vector(rk1e, i + 2, j), vector(rk1e, i + 1, j), vector(rk1e, i, j), dx, simPlat_dt, ilast, jlast);
					p2rpjrho = lor2(vector(rk1rho, i, j + 3), vector(rk1rho, i, j + 2), vector(rk1rho, i, j + 1), vector(rk1rho, i, j), dx, simPlat_dt, ilast, jlast);
					p2rpjmx = lor2(vector(rk1mx, i, j + 3), vector(rk1mx, i, j + 2), vector(rk1mx, i, j + 1), vector(rk1mx, i, j), dx, simPlat_dt, ilast, jlast);
					p2rpjmy = lor2(vector(rk1my, i, j + 3), vector(rk1my, i, j + 2), vector(rk1my, i, j + 1), vector(rk1my, i, j), dx, simPlat_dt, ilast, jlast);
					p2rpje = lor2(vector(rk1e, i, j + 3), vector(rk1e, i, j + 2), vector(rk1e, i, j + 1), vector(rk1e, i, j), dx, simPlat_dt, ilast, jlast);
					p3rpirho = lor3(vector(rk1rho, i + 2, j), vector(rk1rho, i + 1, j), vector(rk1rho, i, j), vector(rk1rho, i - 1, j), dx, simPlat_dt, ilast, jlast);
					p3rpimx = lor3(vector(rk1mx, i + 2, j), vector(rk1mx, i + 1, j), vector(rk1mx, i, j), vector(rk1mx, i - 1, j), dx, simPlat_dt, ilast, jlast);
					p3rpimy = lor3(vector(rk1my, i + 2, j), vector(rk1my, i + 1, j), vector(rk1my, i, j), vector(rk1my, i - 1, j), dx, simPlat_dt, ilast, jlast);
					p3rpie = lor3(vector(rk1e, i + 2, j), vector(rk1e, i + 1, j), vector(rk1e, i, j), vector(rk1e, i - 1, j), dx, simPlat_dt, ilast, jlast);
					p3rpjrho = lor3(vector(rk1rho, i, j + 2), vector(rk1rho, i, j + 1), vector(rk1rho, i, j), vector(rk1rho, i, j - 1), dx, simPlat_dt, ilast, jlast);
					p3rpjmx = lor3(vector(rk1mx, i, j + 2), vector(rk1mx, i, j + 1), vector(rk1mx, i, j), vector(rk1mx, i, j - 1), dx, simPlat_dt, ilast, jlast);
					p3rpjmy = lor3(vector(rk1my, i, j + 2), vector(rk1my, i, j + 1), vector(rk1my, i, j), vector(rk1my, i, j - 1), dx, simPlat_dt, ilast, jlast);
					p3rpje = lor3(vector(rk1e, i, j + 2), vector(rk1e, i, j + 1), vector(rk1e, i, j), vector(rk1e, i, j - 1), dx, simPlat_dt, ilast, jlast);
					p4rpirho = lor4(vector(rk1rho, i + 1, j), vector(rk1rho, i, j), vector(rk1rho, i - 1, j), vector(rk1rho, i - 2, j), dx, simPlat_dt, ilast, jlast);
					p4rpimx = lor4(vector(rk1mx, i + 1, j), vector(rk1mx, i, j), vector(rk1mx, i - 1, j), vector(rk1mx, i - 2, j), dx, simPlat_dt, ilast, jlast);
					p4rpimy = lor4(vector(rk1my, i + 1, j), vector(rk1my, i, j), vector(rk1my, i - 1, j), vector(rk1my, i - 2, j), dx, simPlat_dt, ilast, jlast);
					p4rpie = lor4(vector(rk1e, i + 1, j), vector(rk1e, i, j), vector(rk1e, i - 1, j), vector(rk1e, i - 2, j), dx, simPlat_dt, ilast, jlast);
					p4rpjrho = lor4(vector(rk1rho, i, j + 1), vector(rk1rho, i, j), vector(rk1rho, i, j - 1), vector(rk1rho, i, j - 2), dx, simPlat_dt, ilast, jlast);
					p4rpjmx = lor4(vector(rk1mx, i, j + 1), vector(rk1mx, i, j), vector(rk1mx, i, j - 1), vector(rk1mx, i, j - 2), dx, simPlat_dt, ilast, jlast);
					p4rpjmy = lor4(vector(rk1my, i, j + 1), vector(rk1my, i, j), vector(rk1my, i, j - 1), vector(rk1my, i, j - 2), dx, simPlat_dt, ilast, jlast);
					p4rpje = lor4(vector(rk1e, i, j + 1), vector(rk1e, i, j), vector(rk1e, i, j - 1), vector(rk1e, i, j - 2), dx, simPlat_dt, ilast, jlast);
					beta1lpirho = smooth1(vector(rk1rho, i - 3, j), vector(rk1rho, i - 2, j), vector(rk1rho, i - 1, j), vector(rk1rho, i, j), dx, simPlat_dt, ilast, jlast);
					beta1lpimx = smooth1(vector(rk1mx, i - 3, j), vector(rk1mx, i - 2, j), vector(rk1mx, i - 1, j), vector(rk1mx, i, j), dx, simPlat_dt, ilast, jlast);
					beta1lpimy = smooth1(vector(rk1my, i - 3, j), vector(rk1my, i - 2, j), vector(rk1my, i - 1, j), vector(rk1my, i, j), dx, simPlat_dt, ilast, jlast);
					beta1lpie = smooth1(vector(rk1e, i - 3, j), vector(rk1e, i - 2, j), vector(rk1e, i - 1, j), vector(rk1e, i, j), dx, simPlat_dt, ilast, jlast);
					beta1lpjrho = smooth1(vector(rk1rho, i, j - 3), vector(rk1rho, i, j - 2), vector(rk1rho, i, j - 1), vector(rk1rho, i, j), dx, simPlat_dt, ilast, jlast);
					beta1lpjmx = smooth1(vector(rk1mx, i, j - 3), vector(rk1mx, i, j - 2), vector(rk1mx, i, j - 1), vector(rk1mx, i, j), dx, simPlat_dt, ilast, jlast);
					beta1lpjmy = smooth1(vector(rk1my, i, j - 3), vector(rk1my, i, j - 2), vector(rk1my, i, j - 1), vector(rk1my, i, j), dx, simPlat_dt, ilast, jlast);
					beta1lpje = smooth1(vector(rk1e, i, j - 3), vector(rk1e, i, j - 2), vector(rk1e, i, j - 1), vector(rk1e, i, j), dx, simPlat_dt, ilast, jlast);
					beta2lpirho = smooth2(vector(rk1rho, i - 2, j), vector(rk1rho, i - 1, j), vector(rk1rho, i, j), vector(rk1rho, i + 1, j), dx, simPlat_dt, ilast, jlast);
					beta2lpimx = smooth2(vector(rk1mx, i - 2, j), vector(rk1mx, i - 1, j), vector(rk1mx, i, j), vector(rk1mx, i + 1, j), dx, simPlat_dt, ilast, jlast);
					beta2lpimy = smooth2(vector(rk1my, i - 2, j), vector(rk1my, i - 1, j), vector(rk1my, i, j), vector(rk1my, i + 1, j), dx, simPlat_dt, ilast, jlast);
					beta2lpie = smooth2(vector(rk1e, i - 2, j), vector(rk1e, i - 1, j), vector(rk1e, i, j), vector(rk1e, i + 1, j), dx, simPlat_dt, ilast, jlast);
					beta2lpjrho = smooth2(vector(rk1rho, i, j - 2), vector(rk1rho, i, j - 1), vector(rk1rho, i, j), vector(rk1rho, i, j + 1), dx, simPlat_dt, ilast, jlast);
					beta2lpjmx = smooth2(vector(rk1mx, i, j - 2), vector(rk1mx, i, j - 1), vector(rk1mx, i, j), vector(rk1mx, i, j + 1), dx, simPlat_dt, ilast, jlast);
					beta2lpjmy = smooth2(vector(rk1my, i, j - 2), vector(rk1my, i, j - 1), vector(rk1my, i, j), vector(rk1my, i, j + 1), dx, simPlat_dt, ilast, jlast);
					beta2lpje = smooth2(vector(rk1e, i, j - 2), vector(rk1e, i, j - 1), vector(rk1e, i, j), vector(rk1e, i, j + 1), dx, simPlat_dt, ilast, jlast);
					beta3lpirho = smooth3(vector(rk1rho, i - 1, j), vector(rk1rho, i, j), vector(rk1rho, i + 1, j), vector(rk1rho, i + 2, j), dx, simPlat_dt, ilast, jlast);
					beta3lpimx = smooth3(vector(rk1mx, i - 1, j), vector(rk1mx, i, j), vector(rk1mx, i + 1, j), vector(rk1mx, i + 2, j), dx, simPlat_dt, ilast, jlast);
					beta3lpimy = smooth3(vector(rk1my, i - 1, j), vector(rk1my, i, j), vector(rk1my, i + 1, j), vector(rk1my, i + 2, j), dx, simPlat_dt, ilast, jlast);
					beta3lpie = smooth3(vector(rk1e, i - 1, j), vector(rk1e, i, j), vector(rk1e, i + 1, j), vector(rk1e, i + 2, j), dx, simPlat_dt, ilast, jlast);
					beta3lpjrho = smooth3(vector(rk1rho, i, j - 1), vector(rk1rho, i, j), vector(rk1rho, i, j + 1), vector(rk1rho, i, j + 2), dx, simPlat_dt, ilast, jlast);
					beta3lpjmx = smooth3(vector(rk1mx, i, j - 1), vector(rk1mx, i, j), vector(rk1mx, i, j + 1), vector(rk1mx, i, j + 2), dx, simPlat_dt, ilast, jlast);
					beta3lpjmy = smooth3(vector(rk1my, i, j - 1), vector(rk1my, i, j), vector(rk1my, i, j + 1), vector(rk1my, i, j + 2), dx, simPlat_dt, ilast, jlast);
					beta3lpje = smooth3(vector(rk1e, i, j - 1), vector(rk1e, i, j), vector(rk1e, i, j + 1), vector(rk1e, i, j + 2), dx, simPlat_dt, ilast, jlast);
					beta4lpirho = smooth4(vector(rk1rho, i, j), vector(rk1rho, i + 1, j), vector(rk1rho, i + 2, j), vector(rk1rho, i + 3, j), dx, simPlat_dt, ilast, jlast);
					beta4lpimx = smooth4(vector(rk1mx, i, j), vector(rk1mx, i + 1, j), vector(rk1mx, i + 2, j), vector(rk1mx, i + 3, j), dx, simPlat_dt, ilast, jlast);
					beta4lpimy = smooth4(vector(rk1my, i, j), vector(rk1my, i + 1, j), vector(rk1my, i + 2, j), vector(rk1my, i + 3, j), dx, simPlat_dt, ilast, jlast);
					beta4lpie = smooth4(vector(rk1e, i, j), vector(rk1e, i + 1, j), vector(rk1e, i + 2, j), vector(rk1e, i + 3, j), dx, simPlat_dt, ilast, jlast);
					beta4lpjrho = smooth4(vector(rk1rho, i, j), vector(rk1rho, i, j + 1), vector(rk1rho, i, j + 2), vector(rk1rho, i, j + 3), dx, simPlat_dt, ilast, jlast);
					beta4lpjmx = smooth4(vector(rk1mx, i, j), vector(rk1mx, i, j + 1), vector(rk1mx, i, j + 2), vector(rk1mx, i, j + 3), dx, simPlat_dt, ilast, jlast);
					beta4lpjmy = smooth4(vector(rk1my, i, j), vector(rk1my, i, j + 1), vector(rk1my, i, j + 2), vector(rk1my, i, j + 3), dx, simPlat_dt, ilast, jlast);
					beta4lpje = smooth4(vector(rk1e, i, j), vector(rk1e, i, j + 1), vector(rk1e, i, j + 2), vector(rk1e, i, j + 3), dx, simPlat_dt, ilast, jlast);
					beta1rpirho = smooth1(vector(rk1rho, i + 4, j), vector(rk1rho, i + 3, j), vector(rk1rho, i + 2, j), vector(rk1rho, i + 1, j), dx, simPlat_dt, ilast, jlast);
					beta1rpimx = smooth1(vector(rk1mx, i + 4, j), vector(rk1mx, i + 3, j), vector(rk1mx, i + 2, j), vector(rk1mx, i + 1, j), dx, simPlat_dt, ilast, jlast);
					beta1rpimy = smooth1(vector(rk1my, i + 4, j), vector(rk1my, i + 3, j), vector(rk1my, i + 2, j), vector(rk1my, i + 1, j), dx, simPlat_dt, ilast, jlast);
					beta1rpie = smooth1(vector(rk1e, i + 4, j), vector(rk1e, i + 3, j), vector(rk1e, i + 2, j), vector(rk1e, i + 1, j), dx, simPlat_dt, ilast, jlast);
					beta1rpjrho = smooth1(vector(rk1rho, i, j + 4), vector(rk1rho, i, j + 3), vector(rk1rho, i, j + 2), vector(rk1rho, i, j + 1), dx, simPlat_dt, ilast, jlast);
					beta1rpjmx = smooth1(vector(rk1mx, i, j + 4), vector(rk1mx, i, j + 3), vector(rk1mx, i, j + 2), vector(rk1mx, i, j + 1), dx, simPlat_dt, ilast, jlast);
					beta1rpjmy = smooth1(vector(rk1my, i, j + 4), vector(rk1my, i, j + 3), vector(rk1my, i, j + 2), vector(rk1my, i, j + 1), dx, simPlat_dt, ilast, jlast);
					beta1rpje = smooth1(vector(rk1e, i, j + 4), vector(rk1e, i, j + 3), vector(rk1e, i, j + 2), vector(rk1e, i, j + 1), dx, simPlat_dt, ilast, jlast);
					beta2rpirho = smooth2(vector(rk1rho, i + 3, j), vector(rk1rho, i + 2, j), vector(rk1rho, i + 1, j), vector(rk1rho, i, j), dx, simPlat_dt, ilast, jlast);
					beta2rpimx = smooth2(vector(rk1mx, i + 3, j), vector(rk1mx, i + 2, j), vector(rk1mx, i + 1, j), vector(rk1mx, i, j), dx, simPlat_dt, ilast, jlast);
					beta2rpimy = smooth2(vector(rk1my, i + 3, j), vector(rk1my, i + 2, j), vector(rk1my, i + 1, j), vector(rk1my, i, j), dx, simPlat_dt, ilast, jlast);
					beta2rpie = smooth2(vector(rk1e, i + 3, j), vector(rk1e, i + 2, j), vector(rk1e, i + 1, j), vector(rk1e, i, j), dx, simPlat_dt, ilast, jlast);
					beta2rpjrho = smooth2(vector(rk1rho, i, j + 3), vector(rk1rho, i, j + 2), vector(rk1rho, i, j + 1), vector(rk1rho, i, j), dx, simPlat_dt, ilast, jlast);
					beta2rpjmx = smooth2(vector(rk1mx, i, j + 3), vector(rk1mx, i, j + 2), vector(rk1mx, i, j + 1), vector(rk1mx, i, j), dx, simPlat_dt, ilast, jlast);
					beta2rpjmy = smooth2(vector(rk1my, i, j + 3), vector(rk1my, i, j + 2), vector(rk1my, i, j + 1), vector(rk1my, i, j), dx, simPlat_dt, ilast, jlast);
					beta2rpje = smooth2(vector(rk1e, i, j + 3), vector(rk1e, i, j + 2), vector(rk1e, i, j + 1), vector(rk1e, i, j), dx, simPlat_dt, ilast, jlast);
					beta3rpirho = smooth3(vector(rk1rho, i + 2, j), vector(rk1rho, i + 1, j), vector(rk1rho, i, j), vector(rk1rho, i - 1, j), dx, simPlat_dt, ilast, jlast);
					beta3rpimx = smooth3(vector(rk1mx, i + 2, j), vector(rk1mx, i + 1, j), vector(rk1mx, i, j), vector(rk1mx, i - 1, j), dx, simPlat_dt, ilast, jlast);
					beta3rpimy = smooth3(vector(rk1my, i + 2, j), vector(rk1my, i + 1, j), vector(rk1my, i, j), vector(rk1my, i - 1, j), dx, simPlat_dt, ilast, jlast);
					beta3rpie = smooth3(vector(rk1e, i + 2, j), vector(rk1e, i + 1, j), vector(rk1e, i, j), vector(rk1e, i - 1, j), dx, simPlat_dt, ilast, jlast);
					beta3rpjrho = smooth3(vector(rk1rho, i, j + 2), vector(rk1rho, i, j + 1), vector(rk1rho, i, j), vector(rk1rho, i, j - 1), dx, simPlat_dt, ilast, jlast);
					beta3rpjmx = smooth3(vector(rk1mx, i, j + 2), vector(rk1mx, i, j + 1), vector(rk1mx, i, j), vector(rk1mx, i, j - 1), dx, simPlat_dt, ilast, jlast);
					beta3rpjmy = smooth3(vector(rk1my, i, j + 2), vector(rk1my, i, j + 1), vector(rk1my, i, j), vector(rk1my, i, j - 1), dx, simPlat_dt, ilast, jlast);
					beta3rpje = smooth3(vector(rk1e, i, j + 2), vector(rk1e, i, j + 1), vector(rk1e, i, j), vector(rk1e, i, j - 1), dx, simPlat_dt, ilast, jlast);
					beta4rpirho = smooth4(vector(rk1rho, i + 1, j), vector(rk1rho, i, j), vector(rk1rho, i - 1, j), vector(rk1rho, i - 2, j), dx, simPlat_dt, ilast, jlast);
					beta4rpimx = smooth4(vector(rk1mx, i + 1, j), vector(rk1mx, i, j), vector(rk1mx, i - 1, j), vector(rk1mx, i - 2, j), dx, simPlat_dt, ilast, jlast);
					beta4rpimy = smooth4(vector(rk1my, i + 1, j), vector(rk1my, i, j), vector(rk1my, i - 1, j), vector(rk1my, i - 2, j), dx, simPlat_dt, ilast, jlast);
					beta4rpie = smooth4(vector(rk1e, i + 1, j), vector(rk1e, i, j), vector(rk1e, i - 1, j), vector(rk1e, i - 2, j), dx, simPlat_dt, ilast, jlast);
					beta4rpjrho = smooth4(vector(rk1rho, i, j + 1), vector(rk1rho, i, j), vector(rk1rho, i, j - 1), vector(rk1rho, i, j - 2), dx, simPlat_dt, ilast, jlast);
					beta4rpjmx = smooth4(vector(rk1mx, i, j + 1), vector(rk1mx, i, j), vector(rk1mx, i, j - 1), vector(rk1mx, i, j - 2), dx, simPlat_dt, ilast, jlast);
					beta4rpjmy = smooth4(vector(rk1my, i, j + 1), vector(rk1my, i, j), vector(rk1my, i, j - 1), vector(rk1my, i, j - 2), dx, simPlat_dt, ilast, jlast);
					beta4rpje = smooth4(vector(rk1e, i, j + 1), vector(rk1e, i, j), vector(rk1e, i, j - 1), vector(rk1e, i, j - 2), dx, simPlat_dt, ilast, jlast);
					w1lpirho = nlw1(beta1lpirho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1lpimx = nlw1(beta1lpimx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1lpimy = nlw1(beta1lpimy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1lpie = nlw1(beta1lpie, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1lpjrho = nlw1(beta1lpjrho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1lpjmx = nlw1(beta1lpjmx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1lpjmy = nlw1(beta1lpjmy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1lpje = nlw1(beta1lpje, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2lpirho = nlw2(beta2lpirho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2lpimx = nlw2(beta2lpimx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2lpimy = nlw2(beta2lpimy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2lpie = nlw2(beta2lpie, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2lpjrho = nlw2(beta2lpjrho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2lpjmx = nlw2(beta2lpjmx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2lpjmy = nlw2(beta2lpjmy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2lpje = nlw2(beta2lpje, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3lpirho = nlw3(beta3lpirho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3lpimx = nlw3(beta3lpimx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3lpimy = nlw3(beta3lpimy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3lpie = nlw3(beta3lpie, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3lpjrho = nlw3(beta3lpjrho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3lpjmx = nlw3(beta3lpjmx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3lpjmy = nlw3(beta3lpjmy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3lpje = nlw3(beta3lpje, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4lpirho = nlw4(beta4lpirho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4lpimx = nlw4(beta4lpimx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4lpimy = nlw4(beta4lpimy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4lpie = nlw4(beta4lpie, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4lpjrho = nlw4(beta4lpjrho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4lpjmx = nlw4(beta4lpjmx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4lpjmy = nlw4(beta4lpjmy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4lpje = nlw4(beta4lpje, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1rpirho = nlw1(beta1rpirho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1rpimx = nlw1(beta1rpimx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1rpimy = nlw1(beta1rpimy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1rpie = nlw1(beta1rpie, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1rpjrho = nlw1(beta1rpjrho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1rpjmx = nlw1(beta1rpjmx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1rpjmy = nlw1(beta1rpjmy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1rpje = nlw1(beta1rpje, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2rpirho = nlw2(beta2rpirho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2rpimx = nlw2(beta2rpimx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2rpimy = nlw2(beta2rpimy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2rpie = nlw2(beta2rpie, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2rpjrho = nlw2(beta2rpjrho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2rpjmx = nlw2(beta2rpjmx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2rpjmy = nlw2(beta2rpjmy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2rpje = nlw2(beta2rpje, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3rpirho = nlw3(beta3rpirho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3rpimx = nlw3(beta3rpimx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3rpimy = nlw3(beta3rpimy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3rpie = nlw3(beta3rpie, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3rpjrho = nlw3(beta3rpjrho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3rpjmx = nlw3(beta3rpjmx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3rpjmy = nlw3(beta3rpjmy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3rpje = nlw3(beta3rpje, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4rpirho = nlw4(beta4rpirho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4rpimx = nlw4(beta4rpimx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4rpimy = nlw4(beta4rpimy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4rpie = nlw4(beta4rpie, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4rpjrho = nlw4(beta4rpjrho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4rpjmx = nlw4(beta4rpjmx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4rpjmy = nlw4(beta4rpjmy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4rpje = nlw4(beta4rpje, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					wlpirho = w1lpirho + w2lpirho + w3lpirho + w4lpirho;
					wlpimx = w1lpimx + w2lpimx + w3lpimx + w4lpimx;
					wlpimy = w1lpimy + w2lpimy + w3lpimy + w4lpimy;
					wlpie = w1lpie + w2lpie + w3lpie + w4lpie;
					wlpjrho = w1lpjrho + w2lpjrho + w3lpjrho + w4lpjrho;
					wlpjmx = w1lpjmx + w2lpjmx + w3lpjmx + w4lpjmx;
					wlpjmy = w1lpjmy + w2lpjmy + w3lpjmy + w4lpjmy;
					wlpje = w1lpje + w2lpje + w3lpje + w4lpje;
					wrpirho = w1rpirho + w2rpirho + w3rpirho + w4rpirho;
					wrpimx = w1rpimx + w2rpimx + w3rpimx + w4rpimx;
					wrpimy = w1rpimy + w2rpimy + w3rpimy + w4rpimy;
					wrpie = w1rpie + w2rpie + w3rpie + w4rpie;
					wrpjrho = w1rpjrho + w2rpjrho + w3rpjrho + w4rpjrho;
					wrpjmx = w1rpjmx + w2rpjmx + w3rpjmx + w4rpjmx;
					wrpjmy = w1rpjmy + w2rpjmy + w3rpjmy + w4rpjmy;
					wrpje = w1rpje + w2rpje + w3rpje + w4rpje;
					w1lpirho = w1lpirho / wlpirho;
					w1lpimx = w1lpimx / wlpimx;
					w1lpimy = w1lpimy / wlpimy;
					w1lpie = w1lpie / wlpie;
					w1lpjrho = w1lpjrho / wlpjrho;
					w1lpjmx = w1lpjmx / wlpjmx;
					w1lpjmy = w1lpjmy / wlpjmy;
					w1lpje = w1lpje / wlpje;
					w2lpirho = w2lpirho / wlpirho;
					w2lpimx = w2lpimx / wlpimx;
					w2lpimy = w2lpimy / wlpimy;
					w2lpie = w2lpie / wlpie;
					w2lpjrho = w2lpjrho / wlpjrho;
					w2lpjmx = w2lpjmx / wlpjmx;
					w2lpjmy = w2lpjmy / wlpjmy;
					w2lpje = w2lpje / wlpje;
					w3lpirho = w3lpirho / wlpirho;
					w3lpimx = w3lpimx / wlpimx;
					w3lpimy = w3lpimy / wlpimy;
					w3lpie = w3lpie / wlpie;
					w3lpjrho = w3lpjrho / wlpjrho;
					w3lpjmx = w3lpjmx / wlpjmx;
					w3lpjmy = w3lpjmy / wlpjmy;
					w3lpje = w3lpje / wlpje;
					w4lpirho = w4lpirho / wlpirho;
					w4lpimx = w4lpimx / wlpimx;
					w4lpimy = w4lpimy / wlpimy;
					w4lpie = w4lpie / wlpie;
					w4lpjrho = w4lpjrho / wlpjrho;
					w4lpjmx = w4lpjmx / wlpjmx;
					w4lpjmy = w4lpjmy / wlpjmy;
					w4lpje = w4lpje / wlpje;
					w1rpirho = w1rpirho / wrpirho;
					w1rpimx = w1rpimx / wrpimx;
					w1rpimy = w1rpimy / wrpimy;
					w1rpie = w1rpie / wrpie;
					w1rpjrho = w1rpjrho / wrpjrho;
					w1rpjmx = w1rpjmx / wrpjmx;
					w1rpjmy = w1rpjmy / wrpjmy;
					w1rpje = w1rpje / wrpje;
					w2rpirho = w2rpirho / wrpirho;
					w2rpimx = w2rpimx / wrpimx;
					w2rpimy = w2rpimy / wrpimy;
					w2rpie = w2rpie / wrpie;
					w2rpjrho = w2rpjrho / wrpjrho;
					w2rpjmx = w2rpjmx / wrpjmx;
					w2rpjmy = w2rpjmy / wrpjmy;
					w2rpje = w2rpje / wrpje;
					w3rpirho = w3rpirho / wrpirho;
					w3rpimx = w3rpimx / wrpimx;
					w3rpimy = w3rpimy / wrpimy;
					w3rpie = w3rpie / wrpie;
					w3rpjrho = w3rpjrho / wrpjrho;
					w3rpjmx = w3rpjmx / wrpjmx;
					w3rpjmy = w3rpjmy / wrpjmy;
					w3rpje = w3rpje / wrpje;
					w4rpirho = w4rpirho / wrpirho;
					w4rpimx = w4rpimx / wrpimx;
					w4rpimy = w4rpimy / wrpimy;
					w4rpie = w4rpie / wrpie;
					w4rpjrho = w4rpjrho / wrpjrho;
					w4rpjmx = w4rpjmx / wrpjmx;
					w4rpjmy = w4rpjmy / wrpjmy;
					w4rpje = w4rpje / wrpje;
					rho_lpi = weno(w1lpirho, w2lpirho, w3lpirho, w4lpirho, p1lpirho, p2lpirho, p3lpirho, p4lpirho, dx, simPlat_dt, ilast, jlast);
					mx_lpi = weno(w1lpimx, w2lpimx, w3lpimx, w4lpimx, p1lpimx, p2lpimx, p3lpimx, p4lpimx, dx, simPlat_dt, ilast, jlast);
					my_lpi = weno(w1lpimy, w2lpimy, w3lpimy, w4lpimy, p1lpimy, p2lpimy, p3lpimy, p4lpimy, dx, simPlat_dt, ilast, jlast);
					e_lpi = weno(w1lpie, w2lpie, w3lpie, w4lpie, p1lpie, p2lpie, p3lpie, p4lpie, dx, simPlat_dt, ilast, jlast);
					rho_lpj = weno(w1lpjrho, w2lpjrho, w3lpjrho, w4lpjrho, p1lpjrho, p2lpjrho, p3lpjrho, p4lpjrho, dx, simPlat_dt, ilast, jlast);
					mx_lpj = weno(w1lpjmx, w2lpjmx, w3lpjmx, w4lpjmx, p1lpjmx, p2lpjmx, p3lpjmx, p4lpjmx, dx, simPlat_dt, ilast, jlast);
					my_lpj = weno(w1lpjmy, w2lpjmy, w3lpjmy, w4lpjmy, p1lpjmy, p2lpjmy, p3lpjmy, p4lpjmy, dx, simPlat_dt, ilast, jlast);
					e_lpj = weno(w1lpje, w2lpje, w3lpje, w4lpje, p1lpje, p2lpje, p3lpje, p4lpje, dx, simPlat_dt, ilast, jlast);
					rho_rpi = weno(w1rpirho, w2rpirho, w3rpirho, w4rpirho, p1rpirho, p2rpirho, p3rpirho, p4rpirho, dx, simPlat_dt, ilast, jlast);
					mx_rpi = weno(w1rpimx, w2rpimx, w3rpimx, w4rpimx, p1rpimx, p2rpimx, p3rpimx, p4rpimx, dx, simPlat_dt, ilast, jlast);
					my_rpi = weno(w1rpimy, w2rpimy, w3rpimy, w4rpimy, p1rpimy, p2rpimy, p3rpimy, p4rpimy, dx, simPlat_dt, ilast, jlast);
					e_rpi = weno(w1rpie, w2rpie, w3rpie, w4rpie, p1rpie, p2rpie, p3rpie, p4rpie, dx, simPlat_dt, ilast, jlast);
					rho_rpj = weno(w1rpjrho, w2rpjrho, w3rpjrho, w4rpjrho, p1rpjrho, p2rpjrho, p3rpjrho, p4rpjrho, dx, simPlat_dt, ilast, jlast);
					mx_rpj = weno(w1rpjmx, w2rpjmx, w3rpjmx, w4rpjmx, p1rpjmx, p2rpjmx, p3rpjmx, p4rpjmx, dx, simPlat_dt, ilast, jlast);
					my_rpj = weno(w1rpjmy, w2rpjmy, w3rpjmy, w4rpjmy, p1rpjmy, p2rpjmy, p3rpjmy, p4rpjmy, dx, simPlat_dt, ilast, jlast);
					e_rpj = weno(w1rpje, w2rpje, w3rpje, w4rpje, p1rpje, p2rpje, p3rpje, p4rpje, dx, simPlat_dt, ilast, jlast);
					P_lpi = (gamma - 1.0) * (e_lpi - rho_lpi * (((mx_lpi / rho_lpi) * (mx_lpi / rho_lpi)) / 2.0 + ((my_lpi / rho_lpi) * (my_lpi / rho_lpi)) / 2.0));
					P_lpj = (gamma - 1.0) * (e_lpj - rho_lpj * (((mx_lpj / rho_lpj) * (mx_lpj / rho_lpj)) / 2.0 + ((my_lpj / rho_lpj) * (my_lpj / rho_lpj)) / 2.0));
					P_lni = (gamma - 1.0) * (e_lni - rho_lni * (((mx_lni / rho_lni) * (mx_lni / rho_lni)) / 2.0 + ((my_lni / rho_lni) * (my_lni / rho_lni)) / 2.0));
					P_lnj = (gamma - 1.0) * (e_lnj - rho_lnj * (((mx_lnj / rho_lnj) * (mx_lnj / rho_lnj)) / 2.0 + ((my_lnj / rho_lnj) * (my_lnj / rho_lnj)) / 2.0));
					P_rpi = (gamma - 1.0) * (e_rpi - rho_rpi * (((mx_rpi / rho_rpi) * (mx_rpi / rho_rpi)) / 2.0 + ((my_rpi / rho_rpi) * (my_rpi / rho_rpi)) / 2.0));
					P_rpj = (gamma - 1.0) * (e_rpj - rho_rpj * (((mx_rpj / rho_rpj) * (mx_rpj / rho_rpj)) / 2.0 + ((my_rpj / rho_rpj) * (my_rpj / rho_rpj)) / 2.0));
					P_rni = (gamma - 1.0) * (e_rni - rho_rni * (((mx_rni / rho_rni) * (mx_rni / rho_rni)) / 2.0 + ((my_rni / rho_rni) * (my_rni / rho_rni)) / 2.0));
					P_rnj = (gamma - 1.0) * (e_rnj - rho_rnj * (((mx_rnj / rho_rnj) * (mx_rnj / rho_rnj)) / 2.0 + ((my_rnj / rho_rnj) * (my_rnj / rho_rnj)) / 2.0));
					rk1cy = vector(rk1my, i, j) / vector(rk1rho, i, j);
					rk1cx = vector(rk1mx, i, j) / vector(rk1rho, i, j);
					rk1c = sqrt(fabs((gamma * vector(P, i, j)) / vector(rk1rho, i, j)));
					Speedirho = MAX(fabs(rk1cx), MAX(fabs(rk1cx - rk1c), fabs(rk1cx + rk1c)));
					Speedimx = MAX(fabs(rk1cx), MAX(fabs(rk1cx - rk1c), fabs(rk1cx + rk1c)));
					Speedimy = MAX(fabs(rk1cx), MAX(fabs(rk1cx - rk1c), fabs(rk1cx + rk1c)));
					Speedie = MAX(fabs(rk1cx), MAX(fabs(rk1cx - rk1c), fabs(rk1cx + rk1c)));
					Speedjrho = MAX(fabs(rk1cy), MAX(fabs(rk1cy - rk1c), fabs(rk1cy + rk1c)));
					Speedjmx = MAX(fabs(rk1cy), MAX(fabs(rk1cy - rk1c), fabs(rk1cy + rk1c)));
					Speedjmy = MAX(fabs(rk1cy), MAX(fabs(rk1cy - rk1c), fabs(rk1cy + rk1c)));
					Speedje = MAX(fabs(rk1cy), MAX(fabs(rk1cy - rk1c), fabs(rk1cy + rk1c)));
					rk1cy = vector(rk1my, i + 1, j) / vector(rk1rho, i + 1, j);
					rk1cx = vector(rk1mx, i + 1, j) / vector(rk1rho, i + 1, j);
					rk1c = sqrt(fabs((gamma * vector(P, i + 1, j)) / vector(rk1rho, i + 1, j)));
					Speedpirho = MAX(fabs(rk1cx), MAX(fabs(rk1cx - rk1c), fabs(rk1cx + rk1c)));
					Speedpimx = MAX(fabs(rk1cx), MAX(fabs(rk1cx - rk1c), fabs(rk1cx + rk1c)));
					Speedpimy = MAX(fabs(rk1cx), MAX(fabs(rk1cx - rk1c), fabs(rk1cx + rk1c)));
					Speedpie = MAX(fabs(rk1cx), MAX(fabs(rk1cx - rk1c), fabs(rk1cx + rk1c)));
					rk1cy = vector(rk1my, i, j + 1) / vector(rk1rho, i, j + 1);
					rk1cx = vector(rk1mx, i, j + 1) / vector(rk1rho, i, j + 1);
					rk1c = sqrt(fabs((gamma * vector(P, i, j + 1)) / vector(rk1rho, i, j + 1)));
					Speedpjrho = MAX(fabs(rk1cy), MAX(fabs(rk1cy - rk1c), fabs(rk1cy + rk1c)));
					Speedpjmx = MAX(fabs(rk1cy), MAX(fabs(rk1cy - rk1c), fabs(rk1cy + rk1c)));
					Speedpjmy = MAX(fabs(rk1cy), MAX(fabs(rk1cy - rk1c), fabs(rk1cy + rk1c)));
					Speedpje = MAX(fabs(rk1cy), MAX(fabs(rk1cy - rk1c), fabs(rk1cy + rk1c)));
					rk1cy = vector(rk1my, i - 1, j) / vector(rk1rho, i - 1, j);
					rk1cx = vector(rk1mx, i - 1, j) / vector(rk1rho, i - 1, j);
					rk1c = sqrt(fabs((gamma * vector(P, i - 1, j)) / vector(rk1rho, i - 1, j)));
					Speednirho = MAX(fabs(rk1cx), MAX(fabs(rk1cx - rk1c), fabs(rk1cx + rk1c)));
					Speednimx = MAX(fabs(rk1cx), MAX(fabs(rk1cx - rk1c), fabs(rk1cx + rk1c)));
					Speednimy = MAX(fabs(rk1cx), MAX(fabs(rk1cx - rk1c), fabs(rk1cx + rk1c)));
					Speednie = MAX(fabs(rk1cx), MAX(fabs(rk1cx - rk1c), fabs(rk1cx + rk1c)));
					rk1cy = vector(rk1my, i, j - 1) / vector(rk1rho, i, j - 1);
					rk1cx = vector(rk1mx, i, j - 1) / vector(rk1rho, i, j - 1);
					rk1c = sqrt(fabs((gamma * vector(P, i, j - 1)) / vector(rk1rho, i, j - 1)));
					Speednjrho = MAX(fabs(rk1cy), MAX(fabs(rk1cy - rk1c), fabs(rk1cy + rk1c)));
					Speednjmx = MAX(fabs(rk1cy), MAX(fabs(rk1cy - rk1c), fabs(rk1cy + rk1c)));
					Speednjmy = MAX(fabs(rk1cy), MAX(fabs(rk1cy - rk1c), fabs(rk1cy + rk1c)));
					Speednje = MAX(fabs(rk1cy), MAX(fabs(rk1cy - rk1c), fabs(rk1cy + rk1c)));
					Fluxlpimx = Fimx_domainI(rho_lpi, mx_lpi, P_lpi, dx, simPlat_dt, ilast, jlast);
					Fluxlpimy = Fimy_domainI(rho_lpi, mx_lpi, my_lpi, dx, simPlat_dt, ilast, jlast);
					Fluxlpie = Fie_domainI(rho_lpi, mx_lpi, e_lpi, P_lpi, dx, simPlat_dt, ilast, jlast);
					Fluxlpjmx = Fjmx_domainI(rho_lpj, mx_lpj, my_lpj, dx, simPlat_dt, ilast, jlast);
					Fluxlpjmy = Fjmy_domainI(rho_lpj, my_lpj, P_lpj, dx, simPlat_dt, ilast, jlast);
					Fluxlpje = Fje_domainI(rho_lpj, my_lpj, e_lpj, P_lpj, dx, simPlat_dt, ilast, jlast);
					Fluxlnimx = Fimx_domainI(rho_lni, mx_lni, P_lni, dx, simPlat_dt, ilast, jlast);
					Fluxlnimy = Fimy_domainI(rho_lni, mx_lni, my_lni, dx, simPlat_dt, ilast, jlast);
					Fluxlnie = Fie_domainI(rho_lni, mx_lni, e_lni, P_lni, dx, simPlat_dt, ilast, jlast);
					Fluxlnjmx = Fjmx_domainI(rho_lnj, mx_lnj, my_lnj, dx, simPlat_dt, ilast, jlast);
					Fluxlnjmy = Fjmy_domainI(rho_lnj, my_lnj, P_lnj, dx, simPlat_dt, ilast, jlast);
					Fluxlnje = Fje_domainI(rho_lnj, my_lnj, e_lnj, P_lnj, dx, simPlat_dt, ilast, jlast);
					Fluxrpimx = Fimx_domainI(rho_rpi, mx_rpi, P_rpi, dx, simPlat_dt, ilast, jlast);
					Fluxrpimy = Fimy_domainI(rho_rpi, mx_rpi, my_rpi, dx, simPlat_dt, ilast, jlast);
					Fluxrpie = Fie_domainI(rho_rpi, mx_rpi, e_rpi, P_rpi, dx, simPlat_dt, ilast, jlast);
					Fluxrpjmx = Fjmx_domainI(rho_rpj, mx_rpj, my_rpj, dx, simPlat_dt, ilast, jlast);
					Fluxrpjmy = Fjmy_domainI(rho_rpj, my_rpj, P_rpj, dx, simPlat_dt, ilast, jlast);
					Fluxrpje = Fje_domainI(rho_rpj, my_rpj, e_rpj, P_rpj, dx, simPlat_dt, ilast, jlast);
					Fluxrnimx = Fimx_domainI(rho_rni, mx_rni, P_rni, dx, simPlat_dt, ilast, jlast);
					Fluxrnimy = Fimy_domainI(rho_rni, mx_rni, my_rni, dx, simPlat_dt, ilast, jlast);
					Fluxrnie = Fie_domainI(rho_rni, mx_rni, e_rni, P_rni, dx, simPlat_dt, ilast, jlast);
					Fluxrnjmx = Fjmx_domainI(rho_rnj, mx_rnj, my_rnj, dx, simPlat_dt, ilast, jlast);
					Fluxrnjmy = Fjmy_domainI(rho_rnj, my_rnj, P_rnj, dx, simPlat_dt, ilast, jlast);
					Fluxrnje = Fje_domainI(rho_rnj, my_rnj, e_rnj, P_rnj, dx, simPlat_dt, ilast, jlast);
					RHS_rho = 0.0;
					RHS_mx = 0.0;
					RHS_my = 0.0;
					RHS_e = 0.0;
					RHS_rho = RHS_rho - LLF_i(mx_lni, mx_rni, mx_lpi, mx_rpi, rho_lni, rho_rni, rho_lpi, rho_rpi, Speedirho, Speednirho, Speedpirho, dx, simPlat_dt, ilast, jlast);
					RHS_rho = RHS_rho - LLF_j(my_lnj, my_rnj, my_lpj, my_rpj, rho_lnj, rho_rnj, rho_lpj, rho_rpj, Speedjrho, Speednjrho, Speedpjrho, dx, simPlat_dt, ilast, jlast);
					RHS_mx = RHS_mx - LLF_i(Fluxlnimx, Fluxrnimx, Fluxlpimx, Fluxrpimx, mx_lni, mx_rni, mx_lpi, mx_rpi, Speedimx, Speednimx, Speedpimx, dx, simPlat_dt, ilast, jlast);
					RHS_mx = RHS_mx - LLF_j(Fluxlnjmx, Fluxrnjmx, Fluxlpjmx, Fluxrpjmx, mx_lnj, mx_rnj, mx_lpj, mx_rpj, Speedjmx, Speednjmx, Speedpjmx, dx, simPlat_dt, ilast, jlast);
					RHS_my = RHS_my - LLF_i(Fluxlnimy, Fluxrnimy, Fluxlpimy, Fluxrpimy, my_lni, my_rni, my_lpi, my_rpi, Speedimy, Speednimy, Speedpimy, dx, simPlat_dt, ilast, jlast);
					RHS_my = RHS_my - LLF_j(Fluxlnjmy, Fluxrnjmy, Fluxlpjmy, Fluxrpjmy, my_lnj, my_rnj, my_lpj, my_rpj, Speedjmy, Speednjmy, Speedpjmy, dx, simPlat_dt, ilast, jlast);
					RHS_e = RHS_e - LLF_i(Fluxlnie, Fluxrnie, Fluxlpie, Fluxrpie, e_lni, e_rni, e_lpi, e_rpi, Speedie, Speednie, Speedpie, dx, simPlat_dt, ilast, jlast);
					RHS_e = RHS_e - LLF_j(Fluxlnje, Fluxrnje, Fluxlpje, Fluxrpje, e_lnj, e_rnj, e_lpj, e_rpj, Speedje, Speednje, Speedpje, dx, simPlat_dt, ilast, jlast);
					vector(rk2rho, i, j) = RK3P2_(RHS_rho, rho_p, rk1rho, i, j, dx, simPlat_dt, ilast, jlast);
					vector(rk2mx, i, j) = RK3P2_(RHS_mx, mx_p, rk1mx, i, j, dx, simPlat_dt, ilast, jlast);
					vector(rk2my, i, j) = RK3P2_(RHS_my, my_p, rk1my, i, j, dx, simPlat_dt, ilast, jlast);
					vector(rk2e, i, j) = RK3P2_(RHS_e, e_p, rk1e, i, j, dx, simPlat_dt, ilast, jlast);
				}
			}
		}
	}
	//Fill ghosts and periodical boundaries
	time_interpolate_operator_mesh1->setStep(2);
	d_bdry_sched_advance8[ln]->fillData(current_time + simPlat_dt * 0.5, false);
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch >& patch = *p_it;
		double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
	
		double* P = ((pdat::NodeData<double> *) patch->getPatchData(d_P_id).get())->getPointer();
		double* rk2e = ((pdat::NodeData<double> *) patch->getPatchData(d_rk2e_id).get())->getPointer();
		double* rk2rho = ((pdat::NodeData<double> *) patch->getPatchData(d_rk2rho_id).get())->getPointer();
		double* rk2mx = ((pdat::NodeData<double> *) patch->getPatchData(d_rk2mx_id).get())->getPointer();
		double* rk2my = ((pdat::NodeData<double> *) patch->getPatchData(d_rk2my_id).get())->getPointer();
	
		//Get the dimensions of the patch
		const hier::Index boxfirst = patch->getBox().lower();
		const hier::Index boxlast = patch->getBox().upper();
	
		//Get delta spaces into an array. dx, dy, dz.
		std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
		const double* dx  = patch_geom->getDx();
	
		//Auxiliary definitions
		int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
		int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
		for(int j = 0; j < jlast; j++) {
			for(int i = 0; i < ilast; i++) {
				if (((i + 4 < ilast || !patch->getPatchGeometry()->getTouchesRegularBoundary(0, 1)) && (i - 4 >= 0 || !patch->getPatchGeometry()->getTouchesRegularBoundary(0, 0)) && (j + 4 < jlast || !patch->getPatchGeometry()->getTouchesRegularBoundary(1, 1)) && (j - 4 >= 0 || !patch->getPatchGeometry()->getTouchesRegularBoundary(1, 0)))) {
					vector(P, i, j) = (gamma - 1.0) * (vector(rk2e, i, j) - vector(rk2rho, i, j) * (((vector(rk2mx, i, j) / vector(rk2rho, i, j)) * (vector(rk2mx, i, j) / vector(rk2rho, i, j))) / 2.0 + ((vector(rk2my, i, j) / vector(rk2rho, i, j)) * (vector(rk2my, i, j) / vector(rk2rho, i, j))) / 2.0));
				}
			}
		}
	}
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch >& patch = *p_it;
		double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
	
		double* rk2rho = ((pdat::NodeData<double> *) patch->getPatchData(d_rk2rho_id).get())->getPointer();
		double* rk2mx = ((pdat::NodeData<double> *) patch->getPatchData(d_rk2mx_id).get())->getPointer();
		double* rk2my = ((pdat::NodeData<double> *) patch->getPatchData(d_rk2my_id).get())->getPointer();
		double* rk2e = ((pdat::NodeData<double> *) patch->getPatchData(d_rk2e_id).get())->getPointer();
		double* P = ((pdat::NodeData<double> *) patch->getPatchData(d_P_id).get())->getPointer();
		double* rho = ((pdat::NodeData<double> *) patch->getPatchData(d_rho_id).get())->getPointer();
		double* rho_p = ((pdat::NodeData<double> *) patch->getPatchData(d_rho_p_id).get())->getPointer();
		double* mx = ((pdat::NodeData<double> *) patch->getPatchData(d_mx_id).get())->getPointer();
		double* mx_p = ((pdat::NodeData<double> *) patch->getPatchData(d_mx_p_id).get())->getPointer();
		double* my = ((pdat::NodeData<double> *) patch->getPatchData(d_my_id).get())->getPointer();
		double* my_p = ((pdat::NodeData<double> *) patch->getPatchData(d_my_p_id).get())->getPointer();
		double* e = ((pdat::NodeData<double> *) patch->getPatchData(d_e_id).get())->getPointer();
		double* e_p = ((pdat::NodeData<double> *) patch->getPatchData(d_e_p_id).get())->getPointer();
		double p1lnirho, p1lnimx, p1lnimy, p1lnie, p1lnjrho, p1lnjmx, p1lnjmy, p1lnje, p2lnirho, p2lnimx, p2lnimy, p2lnie, p2lnjrho, p2lnjmx, p2lnjmy, p2lnje, p3lnirho, p3lnimx, p3lnimy, p3lnie, p3lnjrho, p3lnjmx, p3lnjmy, p3lnje, p4lnirho, p4lnimx, p4lnimy, p4lnie, p4lnjrho, p4lnjmx, p4lnjmy, p4lnje, p1rnirho, p1rnimx, p1rnimy, p1rnie, p1rnjrho, p1rnjmx, p1rnjmy, p1rnje, p2rnirho, p2rnimx, p2rnimy, p2rnie, p2rnjrho, p2rnjmx, p2rnjmy, p2rnje, p3rnirho, p3rnimx, p3rnimy, p3rnie, p3rnjrho, p3rnjmx, p3rnjmy, p3rnje, p4rnirho, p4rnimx, p4rnimy, p4rnie, p4rnjrho, p4rnjmx, p4rnjmy, p4rnje, beta1lnirho, beta1lnimx, beta1lnimy, beta1lnie, beta1lnjrho, beta1lnjmx, beta1lnjmy, beta1lnje, beta2lnirho, beta2lnimx, beta2lnimy, beta2lnie, beta2lnjrho, beta2lnjmx, beta2lnjmy, beta2lnje, beta3lnirho, beta3lnimx, beta3lnimy, beta3lnie, beta3lnjrho, beta3lnjmx, beta3lnjmy, beta3lnje, beta4lnirho, beta4lnimx, beta4lnimy, beta4lnie, beta4lnjrho, beta4lnjmx, beta4lnjmy, beta4lnje, beta1rnirho, beta1rnimx, beta1rnimy, beta1rnie, beta1rnjrho, beta1rnjmx, beta1rnjmy, beta1rnje, beta2rnirho, beta2rnimx, beta2rnimy, beta2rnie, beta2rnjrho, beta2rnjmx, beta2rnjmy, beta2rnje, beta3rnirho, beta3rnimx, beta3rnimy, beta3rnie, beta3rnjrho, beta3rnjmx, beta3rnjmy, beta3rnje, beta4rnirho, beta4rnimx, beta4rnimy, beta4rnie, beta4rnjrho, beta4rnjmx, beta4rnjmy, beta4rnje, w1lnirho, w1lnimx, w1lnimy, w1lnie, w1lnjrho, w1lnjmx, w1lnjmy, w1lnje, w2lnirho, w2lnimx, w2lnimy, w2lnie, w2lnjrho, w2lnjmx, w2lnjmy, w2lnje, w3lnirho, w3lnimx, w3lnimy, w3lnie, w3lnjrho, w3lnjmx, w3lnjmy, w3lnje, w4lnirho, w4lnimx, w4lnimy, w4lnie, w4lnjrho, w4lnjmx, w4lnjmy, w4lnje, w1rnirho, w1rnimx, w1rnimy, w1rnie, w1rnjrho, w1rnjmx, w1rnjmy, w1rnje, w2rnirho, w2rnimx, w2rnimy, w2rnie, w2rnjrho, w2rnjmx, w2rnjmy, w2rnje, w3rnirho, w3rnimx, w3rnimy, w3rnie, w3rnjrho, w3rnjmx, w3rnjmy, w3rnje, w4rnirho, w4rnimx, w4rnimy, w4rnie, w4rnjrho, w4rnjmx, w4rnjmy, w4rnje, wlnirho, wlnimx, wlnimy, wlnie, wlnjrho, wlnjmx, wlnjmy, wlnje, wrnirho, wrnimx, wrnimy, wrnie, wrnjrho, wrnjmx, wrnjmy, wrnje, rho_lni, mx_lni, my_lni, e_lni, rho_lnj, mx_lnj, my_lnj, e_lnj, rho_rni, mx_rni, my_rni, e_rni, rho_rnj, mx_rnj, my_rnj, e_rnj, p1lpirho, p1lpimx, p1lpimy, p1lpie, p1lpjrho, p1lpjmx, p1lpjmy, p1lpje, p2lpirho, p2lpimx, p2lpimy, p2lpie, p2lpjrho, p2lpjmx, p2lpjmy, p2lpje, p3lpirho, p3lpimx, p3lpimy, p3lpie, p3lpjrho, p3lpjmx, p3lpjmy, p3lpje, p4lpirho, p4lpimx, p4lpimy, p4lpie, p4lpjrho, p4lpjmx, p4lpjmy, p4lpje, p1rpirho, p1rpimx, p1rpimy, p1rpie, p1rpjrho, p1rpjmx, p1rpjmy, p1rpje, p2rpirho, p2rpimx, p2rpimy, p2rpie, p2rpjrho, p2rpjmx, p2rpjmy, p2rpje, p3rpirho, p3rpimx, p3rpimy, p3rpie, p3rpjrho, p3rpjmx, p3rpjmy, p3rpje, p4rpirho, p4rpimx, p4rpimy, p4rpie, p4rpjrho, p4rpjmx, p4rpjmy, p4rpje, beta1lpirho, beta1lpimx, beta1lpimy, beta1lpie, beta1lpjrho, beta1lpjmx, beta1lpjmy, beta1lpje, beta2lpirho, beta2lpimx, beta2lpimy, beta2lpie, beta2lpjrho, beta2lpjmx, beta2lpjmy, beta2lpje, beta3lpirho, beta3lpimx, beta3lpimy, beta3lpie, beta3lpjrho, beta3lpjmx, beta3lpjmy, beta3lpje, beta4lpirho, beta4lpimx, beta4lpimy, beta4lpie, beta4lpjrho, beta4lpjmx, beta4lpjmy, beta4lpje, beta1rpirho, beta1rpimx, beta1rpimy, beta1rpie, beta1rpjrho, beta1rpjmx, beta1rpjmy, beta1rpje, beta2rpirho, beta2rpimx, beta2rpimy, beta2rpie, beta2rpjrho, beta2rpjmx, beta2rpjmy, beta2rpje, beta3rpirho, beta3rpimx, beta3rpimy, beta3rpie, beta3rpjrho, beta3rpjmx, beta3rpjmy, beta3rpje, beta4rpirho, beta4rpimx, beta4rpimy, beta4rpie, beta4rpjrho, beta4rpjmx, beta4rpjmy, beta4rpje, w1lpirho, w1lpimx, w1lpimy, w1lpie, w1lpjrho, w1lpjmx, w1lpjmy, w1lpje, w2lpirho, w2lpimx, w2lpimy, w2lpie, w2lpjrho, w2lpjmx, w2lpjmy, w2lpje, w3lpirho, w3lpimx, w3lpimy, w3lpie, w3lpjrho, w3lpjmx, w3lpjmy, w3lpje, w4lpirho, w4lpimx, w4lpimy, w4lpie, w4lpjrho, w4lpjmx, w4lpjmy, w4lpje, w1rpirho, w1rpimx, w1rpimy, w1rpie, w1rpjrho, w1rpjmx, w1rpjmy, w1rpje, w2rpirho, w2rpimx, w2rpimy, w2rpie, w2rpjrho, w2rpjmx, w2rpjmy, w2rpje, w3rpirho, w3rpimx, w3rpimy, w3rpie, w3rpjrho, w3rpjmx, w3rpjmy, w3rpje, w4rpirho, w4rpimx, w4rpimy, w4rpie, w4rpjrho, w4rpjmx, w4rpjmy, w4rpje, wlpirho, wlpimx, wlpimy, wlpie, wlpjrho, wlpjmx, wlpjmy, wlpje, wrpirho, wrpimx, wrpimy, wrpie, wrpjrho, wrpjmx, wrpjmy, wrpje, rho_lpi, mx_lpi, my_lpi, e_lpi, rho_lpj, mx_lpj, my_lpj, e_lpj, rho_rpi, mx_rpi, my_rpi, e_rpi, rho_rpj, mx_rpj, my_rpj, e_rpj, P_lpi, P_lpj, P_lni, P_lnj, P_rpi, P_rpj, P_rni, P_rnj, rk2cy, rk2cx, rk2c, Speedirho, Speedimx, Speedimy, Speedie, Speedjrho, Speedjmx, Speedjmy, Speedje, Speedpirho, Speedpimx, Speedpimy, Speedpie, Speedpjrho, Speedpjmx, Speedpjmy, Speedpje, Speednirho, Speednimx, Speednimy, Speednie, Speednjrho, Speednjmx, Speednjmy, Speednje, Fluxlpimx, Fluxlpimy, Fluxlpie, Fluxlpjmx, Fluxlpjmy, Fluxlpje, Fluxlnimx, Fluxlnimy, Fluxlnie, Fluxlnjmx, Fluxlnjmy, Fluxlnje, Fluxrpimx, Fluxrpimy, Fluxrpie, Fluxrpjmx, Fluxrpjmy, Fluxrpje, Fluxrnimx, Fluxrnimy, Fluxrnie, Fluxrnjmx, Fluxrnjmy, Fluxrnje, RHS_rho, RHS_mx, RHS_my, RHS_e;
	
		//Get the dimensions of the patch
		const hier::Index boxfirst = patch->getBox().lower();
		const hier::Index boxlast = patch->getBox().upper();
	
		//Get delta spaces into an array. dx, dy, dz.
		std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
		const double* dx  = patch_geom->getDx();
	
		//Auxiliary definitions
		int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
		int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
		for(int j = 0; j < jlast; j++) {
			for(int i = 0; i < ilast; i++) {
				if ((i + 4 < ilast && i - 4 >= 0 && j + 4 < jlast && j - 4 >= 0)) {
					p1lnirho = lor1(vector(rk2rho, i - 4, j), vector(rk2rho, i - 3, j), vector(rk2rho, i - 2, j), vector(rk2rho, i - 1, j), dx, simPlat_dt, ilast, jlast);
					p1lnimx = lor1(vector(rk2mx, i - 4, j), vector(rk2mx, i - 3, j), vector(rk2mx, i - 2, j), vector(rk2mx, i - 1, j), dx, simPlat_dt, ilast, jlast);
					p1lnimy = lor1(vector(rk2my, i - 4, j), vector(rk2my, i - 3, j), vector(rk2my, i - 2, j), vector(rk2my, i - 1, j), dx, simPlat_dt, ilast, jlast);
					p1lnie = lor1(vector(rk2e, i - 4, j), vector(rk2e, i - 3, j), vector(rk2e, i - 2, j), vector(rk2e, i - 1, j), dx, simPlat_dt, ilast, jlast);
					p1lnjrho = lor1(vector(rk2rho, i, j - 4), vector(rk2rho, i, j - 3), vector(rk2rho, i, j - 2), vector(rk2rho, i, j - 1), dx, simPlat_dt, ilast, jlast);
					p1lnjmx = lor1(vector(rk2mx, i, j - 4), vector(rk2mx, i, j - 3), vector(rk2mx, i, j - 2), vector(rk2mx, i, j - 1), dx, simPlat_dt, ilast, jlast);
					p1lnjmy = lor1(vector(rk2my, i, j - 4), vector(rk2my, i, j - 3), vector(rk2my, i, j - 2), vector(rk2my, i, j - 1), dx, simPlat_dt, ilast, jlast);
					p1lnje = lor1(vector(rk2e, i, j - 4), vector(rk2e, i, j - 3), vector(rk2e, i, j - 2), vector(rk2e, i, j - 1), dx, simPlat_dt, ilast, jlast);
					p2lnirho = lor2(vector(rk2rho, i - 3, j), vector(rk2rho, i - 2, j), vector(rk2rho, i - 1, j), vector(rk2rho, i, j), dx, simPlat_dt, ilast, jlast);
					p2lnimx = lor2(vector(rk2mx, i - 3, j), vector(rk2mx, i - 2, j), vector(rk2mx, i - 1, j), vector(rk2mx, i, j), dx, simPlat_dt, ilast, jlast);
					p2lnimy = lor2(vector(rk2my, i - 3, j), vector(rk2my, i - 2, j), vector(rk2my, i - 1, j), vector(rk2my, i, j), dx, simPlat_dt, ilast, jlast);
					p2lnie = lor2(vector(rk2e, i - 3, j), vector(rk2e, i - 2, j), vector(rk2e, i - 1, j), vector(rk2e, i, j), dx, simPlat_dt, ilast, jlast);
					p2lnjrho = lor2(vector(rk2rho, i, j - 3), vector(rk2rho, i, j - 2), vector(rk2rho, i, j - 1), vector(rk2rho, i, j), dx, simPlat_dt, ilast, jlast);
					p2lnjmx = lor2(vector(rk2mx, i, j - 3), vector(rk2mx, i, j - 2), vector(rk2mx, i, j - 1), vector(rk2mx, i, j), dx, simPlat_dt, ilast, jlast);
					p2lnjmy = lor2(vector(rk2my, i, j - 3), vector(rk2my, i, j - 2), vector(rk2my, i, j - 1), vector(rk2my, i, j), dx, simPlat_dt, ilast, jlast);
					p2lnje = lor2(vector(rk2e, i, j - 3), vector(rk2e, i, j - 2), vector(rk2e, i, j - 1), vector(rk2e, i, j), dx, simPlat_dt, ilast, jlast);
					p3lnirho = lor3(vector(rk2rho, i - 2, j), vector(rk2rho, i - 1, j), vector(rk2rho, i, j), vector(rk2rho, i + 1, j), dx, simPlat_dt, ilast, jlast);
					p3lnimx = lor3(vector(rk2mx, i - 2, j), vector(rk2mx, i - 1, j), vector(rk2mx, i, j), vector(rk2mx, i + 1, j), dx, simPlat_dt, ilast, jlast);
					p3lnimy = lor3(vector(rk2my, i - 2, j), vector(rk2my, i - 1, j), vector(rk2my, i, j), vector(rk2my, i + 1, j), dx, simPlat_dt, ilast, jlast);
					p3lnie = lor3(vector(rk2e, i - 2, j), vector(rk2e, i - 1, j), vector(rk2e, i, j), vector(rk2e, i + 1, j), dx, simPlat_dt, ilast, jlast);
					p3lnjrho = lor3(vector(rk2rho, i, j - 2), vector(rk2rho, i, j - 1), vector(rk2rho, i, j), vector(rk2rho, i, j + 1), dx, simPlat_dt, ilast, jlast);
					p3lnjmx = lor3(vector(rk2mx, i, j - 2), vector(rk2mx, i, j - 1), vector(rk2mx, i, j), vector(rk2mx, i, j + 1), dx, simPlat_dt, ilast, jlast);
					p3lnjmy = lor3(vector(rk2my, i, j - 2), vector(rk2my, i, j - 1), vector(rk2my, i, j), vector(rk2my, i, j + 1), dx, simPlat_dt, ilast, jlast);
					p3lnje = lor3(vector(rk2e, i, j - 2), vector(rk2e, i, j - 1), vector(rk2e, i, j), vector(rk2e, i, j + 1), dx, simPlat_dt, ilast, jlast);
					p4lnirho = lor4(vector(rk2rho, i - 1, j), vector(rk2rho, i, j), vector(rk2rho, i + 1, j), vector(rk2rho, i + 2, j), dx, simPlat_dt, ilast, jlast);
					p4lnimx = lor4(vector(rk2mx, i - 1, j), vector(rk2mx, i, j), vector(rk2mx, i + 1, j), vector(rk2mx, i + 2, j), dx, simPlat_dt, ilast, jlast);
					p4lnimy = lor4(vector(rk2my, i - 1, j), vector(rk2my, i, j), vector(rk2my, i + 1, j), vector(rk2my, i + 2, j), dx, simPlat_dt, ilast, jlast);
					p4lnie = lor4(vector(rk2e, i - 1, j), vector(rk2e, i, j), vector(rk2e, i + 1, j), vector(rk2e, i + 2, j), dx, simPlat_dt, ilast, jlast);
					p4lnjrho = lor4(vector(rk2rho, i, j - 1), vector(rk2rho, i, j), vector(rk2rho, i, j + 1), vector(rk2rho, i, j + 2), dx, simPlat_dt, ilast, jlast);
					p4lnjmx = lor4(vector(rk2mx, i, j - 1), vector(rk2mx, i, j), vector(rk2mx, i, j + 1), vector(rk2mx, i, j + 2), dx, simPlat_dt, ilast, jlast);
					p4lnjmy = lor4(vector(rk2my, i, j - 1), vector(rk2my, i, j), vector(rk2my, i, j + 1), vector(rk2my, i, j + 2), dx, simPlat_dt, ilast, jlast);
					p4lnje = lor4(vector(rk2e, i, j - 1), vector(rk2e, i, j), vector(rk2e, i, j + 1), vector(rk2e, i, j + 2), dx, simPlat_dt, ilast, jlast);
					p1rnirho = lor1(vector(rk2rho, i + 3, j), vector(rk2rho, i + 2, j), vector(rk2rho, i + 1, j), vector(rk2rho, i, j), dx, simPlat_dt, ilast, jlast);
					p1rnimx = lor1(vector(rk2mx, i + 3, j), vector(rk2mx, i + 2, j), vector(rk2mx, i + 1, j), vector(rk2mx, i, j), dx, simPlat_dt, ilast, jlast);
					p1rnimy = lor1(vector(rk2my, i + 3, j), vector(rk2my, i + 2, j), vector(rk2my, i + 1, j), vector(rk2my, i, j), dx, simPlat_dt, ilast, jlast);
					p1rnie = lor1(vector(rk2e, i + 3, j), vector(rk2e, i + 2, j), vector(rk2e, i + 1, j), vector(rk2e, i, j), dx, simPlat_dt, ilast, jlast);
					p1rnjrho = lor1(vector(rk2rho, i, j + 3), vector(rk2rho, i, j + 2), vector(rk2rho, i, j + 1), vector(rk2rho, i, j), dx, simPlat_dt, ilast, jlast);
					p1rnjmx = lor1(vector(rk2mx, i, j + 3), vector(rk2mx, i, j + 2), vector(rk2mx, i, j + 1), vector(rk2mx, i, j), dx, simPlat_dt, ilast, jlast);
					p1rnjmy = lor1(vector(rk2my, i, j + 3), vector(rk2my, i, j + 2), vector(rk2my, i, j + 1), vector(rk2my, i, j), dx, simPlat_dt, ilast, jlast);
					p1rnje = lor1(vector(rk2e, i, j + 3), vector(rk2e, i, j + 2), vector(rk2e, i, j + 1), vector(rk2e, i, j), dx, simPlat_dt, ilast, jlast);
					p2rnirho = lor2(vector(rk2rho, i + 2, j), vector(rk2rho, i + 1, j), vector(rk2rho, i, j), vector(rk2rho, i - 1, j), dx, simPlat_dt, ilast, jlast);
					p2rnimx = lor2(vector(rk2mx, i + 2, j), vector(rk2mx, i + 1, j), vector(rk2mx, i, j), vector(rk2mx, i - 1, j), dx, simPlat_dt, ilast, jlast);
					p2rnimy = lor2(vector(rk2my, i + 2, j), vector(rk2my, i + 1, j), vector(rk2my, i, j), vector(rk2my, i - 1, j), dx, simPlat_dt, ilast, jlast);
					p2rnie = lor2(vector(rk2e, i + 2, j), vector(rk2e, i + 1, j), vector(rk2e, i, j), vector(rk2e, i - 1, j), dx, simPlat_dt, ilast, jlast);
					p2rnjrho = lor2(vector(rk2rho, i, j + 2), vector(rk2rho, i, j + 1), vector(rk2rho, i, j), vector(rk2rho, i, j - 1), dx, simPlat_dt, ilast, jlast);
					p2rnjmx = lor2(vector(rk2mx, i, j + 2), vector(rk2mx, i, j + 1), vector(rk2mx, i, j), vector(rk2mx, i, j - 1), dx, simPlat_dt, ilast, jlast);
					p2rnjmy = lor2(vector(rk2my, i, j + 2), vector(rk2my, i, j + 1), vector(rk2my, i, j), vector(rk2my, i, j - 1), dx, simPlat_dt, ilast, jlast);
					p2rnje = lor2(vector(rk2e, i, j + 2), vector(rk2e, i, j + 1), vector(rk2e, i, j), vector(rk2e, i, j - 1), dx, simPlat_dt, ilast, jlast);
					p3rnirho = lor3(vector(rk2rho, i + 1, j), vector(rk2rho, i, j), vector(rk2rho, i - 1, j), vector(rk2rho, i - 2, j), dx, simPlat_dt, ilast, jlast);
					p3rnimx = lor3(vector(rk2mx, i + 1, j), vector(rk2mx, i, j), vector(rk2mx, i - 1, j), vector(rk2mx, i - 2, j), dx, simPlat_dt, ilast, jlast);
					p3rnimy = lor3(vector(rk2my, i + 1, j), vector(rk2my, i, j), vector(rk2my, i - 1, j), vector(rk2my, i - 2, j), dx, simPlat_dt, ilast, jlast);
					p3rnie = lor3(vector(rk2e, i + 1, j), vector(rk2e, i, j), vector(rk2e, i - 1, j), vector(rk2e, i - 2, j), dx, simPlat_dt, ilast, jlast);
					p3rnjrho = lor3(vector(rk2rho, i, j + 1), vector(rk2rho, i, j), vector(rk2rho, i, j - 1), vector(rk2rho, i, j - 2), dx, simPlat_dt, ilast, jlast);
					p3rnjmx = lor3(vector(rk2mx, i, j + 1), vector(rk2mx, i, j), vector(rk2mx, i, j - 1), vector(rk2mx, i, j - 2), dx, simPlat_dt, ilast, jlast);
					p3rnjmy = lor3(vector(rk2my, i, j + 1), vector(rk2my, i, j), vector(rk2my, i, j - 1), vector(rk2my, i, j - 2), dx, simPlat_dt, ilast, jlast);
					p3rnje = lor3(vector(rk2e, i, j + 1), vector(rk2e, i, j), vector(rk2e, i, j - 1), vector(rk2e, i, j - 2), dx, simPlat_dt, ilast, jlast);
					p4rnirho = lor4(vector(rk2rho, i, j), vector(rk2rho, i - 1, j), vector(rk2rho, i - 2, j), vector(rk2rho, i - 3, j), dx, simPlat_dt, ilast, jlast);
					p4rnimx = lor4(vector(rk2mx, i, j), vector(rk2mx, i - 1, j), vector(rk2mx, i - 2, j), vector(rk2mx, i - 3, j), dx, simPlat_dt, ilast, jlast);
					p4rnimy = lor4(vector(rk2my, i, j), vector(rk2my, i - 1, j), vector(rk2my, i - 2, j), vector(rk2my, i - 3, j), dx, simPlat_dt, ilast, jlast);
					p4rnie = lor4(vector(rk2e, i, j), vector(rk2e, i - 1, j), vector(rk2e, i - 2, j), vector(rk2e, i - 3, j), dx, simPlat_dt, ilast, jlast);
					p4rnjrho = lor4(vector(rk2rho, i, j), vector(rk2rho, i, j - 1), vector(rk2rho, i, j - 2), vector(rk2rho, i, j - 3), dx, simPlat_dt, ilast, jlast);
					p4rnjmx = lor4(vector(rk2mx, i, j), vector(rk2mx, i, j - 1), vector(rk2mx, i, j - 2), vector(rk2mx, i, j - 3), dx, simPlat_dt, ilast, jlast);
					p4rnjmy = lor4(vector(rk2my, i, j), vector(rk2my, i, j - 1), vector(rk2my, i, j - 2), vector(rk2my, i, j - 3), dx, simPlat_dt, ilast, jlast);
					p4rnje = lor4(vector(rk2e, i, j), vector(rk2e, i, j - 1), vector(rk2e, i, j - 2), vector(rk2e, i, j - 3), dx, simPlat_dt, ilast, jlast);
					beta1lnirho = smooth1(vector(rk2rho, i - 4, j), vector(rk2rho, i - 3, j), vector(rk2rho, i - 2, j), vector(rk2rho, i - 1, j), dx, simPlat_dt, ilast, jlast);
					beta1lnimx = smooth1(vector(rk2mx, i - 4, j), vector(rk2mx, i - 3, j), vector(rk2mx, i - 2, j), vector(rk2mx, i - 1, j), dx, simPlat_dt, ilast, jlast);
					beta1lnimy = smooth1(vector(rk2my, i - 4, j), vector(rk2my, i - 3, j), vector(rk2my, i - 2, j), vector(rk2my, i - 1, j), dx, simPlat_dt, ilast, jlast);
					beta1lnie = smooth1(vector(rk2e, i - 4, j), vector(rk2e, i - 3, j), vector(rk2e, i - 2, j), vector(rk2e, i - 1, j), dx, simPlat_dt, ilast, jlast);
					beta1lnjrho = smooth1(vector(rk2rho, i, j - 4), vector(rk2rho, i, j - 3), vector(rk2rho, i, j - 2), vector(rk2rho, i, j - 1), dx, simPlat_dt, ilast, jlast);
					beta1lnjmx = smooth1(vector(rk2mx, i, j - 4), vector(rk2mx, i, j - 3), vector(rk2mx, i, j - 2), vector(rk2mx, i, j - 1), dx, simPlat_dt, ilast, jlast);
					beta1lnjmy = smooth1(vector(rk2my, i, j - 4), vector(rk2my, i, j - 3), vector(rk2my, i, j - 2), vector(rk2my, i, j - 1), dx, simPlat_dt, ilast, jlast);
					beta1lnje = smooth1(vector(rk2e, i, j - 4), vector(rk2e, i, j - 3), vector(rk2e, i, j - 2), vector(rk2e, i, j - 1), dx, simPlat_dt, ilast, jlast);
					beta2lnirho = smooth2(vector(rk2rho, i - 3, j), vector(rk2rho, i - 2, j), vector(rk2rho, i - 1, j), vector(rk2rho, i, j), dx, simPlat_dt, ilast, jlast);
					beta2lnimx = smooth2(vector(rk2mx, i - 3, j), vector(rk2mx, i - 2, j), vector(rk2mx, i - 1, j), vector(rk2mx, i, j), dx, simPlat_dt, ilast, jlast);
					beta2lnimy = smooth2(vector(rk2my, i - 3, j), vector(rk2my, i - 2, j), vector(rk2my, i - 1, j), vector(rk2my, i, j), dx, simPlat_dt, ilast, jlast);
					beta2lnie = smooth2(vector(rk2e, i - 3, j), vector(rk2e, i - 2, j), vector(rk2e, i - 1, j), vector(rk2e, i, j), dx, simPlat_dt, ilast, jlast);
					beta2lnjrho = smooth2(vector(rk2rho, i, j - 3), vector(rk2rho, i, j - 2), vector(rk2rho, i, j - 1), vector(rk2rho, i, j), dx, simPlat_dt, ilast, jlast);
					beta2lnjmx = smooth2(vector(rk2mx, i, j - 3), vector(rk2mx, i, j - 2), vector(rk2mx, i, j - 1), vector(rk2mx, i, j), dx, simPlat_dt, ilast, jlast);
					beta2lnjmy = smooth2(vector(rk2my, i, j - 3), vector(rk2my, i, j - 2), vector(rk2my, i, j - 1), vector(rk2my, i, j), dx, simPlat_dt, ilast, jlast);
					beta2lnje = smooth2(vector(rk2e, i, j - 3), vector(rk2e, i, j - 2), vector(rk2e, i, j - 1), vector(rk2e, i, j), dx, simPlat_dt, ilast, jlast);
					beta3lnirho = smooth3(vector(rk2rho, i - 2, j), vector(rk2rho, i - 1, j), vector(rk2rho, i, j), vector(rk2rho, i + 1, j), dx, simPlat_dt, ilast, jlast);
					beta3lnimx = smooth3(vector(rk2mx, i - 2, j), vector(rk2mx, i - 1, j), vector(rk2mx, i, j), vector(rk2mx, i + 1, j), dx, simPlat_dt, ilast, jlast);
					beta3lnimy = smooth3(vector(rk2my, i - 2, j), vector(rk2my, i - 1, j), vector(rk2my, i, j), vector(rk2my, i + 1, j), dx, simPlat_dt, ilast, jlast);
					beta3lnie = smooth3(vector(rk2e, i - 2, j), vector(rk2e, i - 1, j), vector(rk2e, i, j), vector(rk2e, i + 1, j), dx, simPlat_dt, ilast, jlast);
					beta3lnjrho = smooth3(vector(rk2rho, i, j - 2), vector(rk2rho, i, j - 1), vector(rk2rho, i, j), vector(rk2rho, i, j + 1), dx, simPlat_dt, ilast, jlast);
					beta3lnjmx = smooth3(vector(rk2mx, i, j - 2), vector(rk2mx, i, j - 1), vector(rk2mx, i, j), vector(rk2mx, i, j + 1), dx, simPlat_dt, ilast, jlast);
					beta3lnjmy = smooth3(vector(rk2my, i, j - 2), vector(rk2my, i, j - 1), vector(rk2my, i, j), vector(rk2my, i, j + 1), dx, simPlat_dt, ilast, jlast);
					beta3lnje = smooth3(vector(rk2e, i, j - 2), vector(rk2e, i, j - 1), vector(rk2e, i, j), vector(rk2e, i, j + 1), dx, simPlat_dt, ilast, jlast);
					beta4lnirho = smooth4(vector(rk2rho, i - 1, j), vector(rk2rho, i, j), vector(rk2rho, i + 1, j), vector(rk2rho, i + 2, j), dx, simPlat_dt, ilast, jlast);
					beta4lnimx = smooth4(vector(rk2mx, i - 1, j), vector(rk2mx, i, j), vector(rk2mx, i + 1, j), vector(rk2mx, i + 2, j), dx, simPlat_dt, ilast, jlast);
					beta4lnimy = smooth4(vector(rk2my, i - 1, j), vector(rk2my, i, j), vector(rk2my, i + 1, j), vector(rk2my, i + 2, j), dx, simPlat_dt, ilast, jlast);
					beta4lnie = smooth4(vector(rk2e, i - 1, j), vector(rk2e, i, j), vector(rk2e, i + 1, j), vector(rk2e, i + 2, j), dx, simPlat_dt, ilast, jlast);
					beta4lnjrho = smooth4(vector(rk2rho, i, j - 1), vector(rk2rho, i, j), vector(rk2rho, i, j + 1), vector(rk2rho, i, j + 2), dx, simPlat_dt, ilast, jlast);
					beta4lnjmx = smooth4(vector(rk2mx, i, j - 1), vector(rk2mx, i, j), vector(rk2mx, i, j + 1), vector(rk2mx, i, j + 2), dx, simPlat_dt, ilast, jlast);
					beta4lnjmy = smooth4(vector(rk2my, i, j - 1), vector(rk2my, i, j), vector(rk2my, i, j + 1), vector(rk2my, i, j + 2), dx, simPlat_dt, ilast, jlast);
					beta4lnje = smooth4(vector(rk2e, i, j - 1), vector(rk2e, i, j), vector(rk2e, i, j + 1), vector(rk2e, i, j + 2), dx, simPlat_dt, ilast, jlast);
					beta1rnirho = smooth1(vector(rk2rho, i + 3, j), vector(rk2rho, i + 2, j), vector(rk2rho, i + 1, j), vector(rk2rho, i, j), dx, simPlat_dt, ilast, jlast);
					beta1rnimx = smooth1(vector(rk2mx, i + 3, j), vector(rk2mx, i + 2, j), vector(rk2mx, i + 1, j), vector(rk2mx, i, j), dx, simPlat_dt, ilast, jlast);
					beta1rnimy = smooth1(vector(rk2my, i + 3, j), vector(rk2my, i + 2, j), vector(rk2my, i + 1, j), vector(rk2my, i, j), dx, simPlat_dt, ilast, jlast);
					beta1rnie = smooth1(vector(rk2e, i + 3, j), vector(rk2e, i + 2, j), vector(rk2e, i + 1, j), vector(rk2e, i, j), dx, simPlat_dt, ilast, jlast);
					beta1rnjrho = smooth1(vector(rk2rho, i, j + 3), vector(rk2rho, i, j + 2), vector(rk2rho, i, j + 1), vector(rk2rho, i, j), dx, simPlat_dt, ilast, jlast);
					beta1rnjmx = smooth1(vector(rk2mx, i, j + 3), vector(rk2mx, i, j + 2), vector(rk2mx, i, j + 1), vector(rk2mx, i, j), dx, simPlat_dt, ilast, jlast);
					beta1rnjmy = smooth1(vector(rk2my, i, j + 3), vector(rk2my, i, j + 2), vector(rk2my, i, j + 1), vector(rk2my, i, j), dx, simPlat_dt, ilast, jlast);
					beta1rnje = smooth1(vector(rk2e, i, j + 3), vector(rk2e, i, j + 2), vector(rk2e, i, j + 1), vector(rk2e, i, j), dx, simPlat_dt, ilast, jlast);
					beta2rnirho = smooth2(vector(rk2rho, i + 2, j), vector(rk2rho, i + 1, j), vector(rk2rho, i, j), vector(rk2rho, i - 1, j), dx, simPlat_dt, ilast, jlast);
					beta2rnimx = smooth2(vector(rk2mx, i + 2, j), vector(rk2mx, i + 1, j), vector(rk2mx, i, j), vector(rk2mx, i - 1, j), dx, simPlat_dt, ilast, jlast);
					beta2rnimy = smooth2(vector(rk2my, i + 2, j), vector(rk2my, i + 1, j), vector(rk2my, i, j), vector(rk2my, i - 1, j), dx, simPlat_dt, ilast, jlast);
					beta2rnie = smooth2(vector(rk2e, i + 2, j), vector(rk2e, i + 1, j), vector(rk2e, i, j), vector(rk2e, i - 1, j), dx, simPlat_dt, ilast, jlast);
					beta2rnjrho = smooth2(vector(rk2rho, i, j + 2), vector(rk2rho, i, j + 1), vector(rk2rho, i, j), vector(rk2rho, i, j - 1), dx, simPlat_dt, ilast, jlast);
					beta2rnjmx = smooth2(vector(rk2mx, i, j + 2), vector(rk2mx, i, j + 1), vector(rk2mx, i, j), vector(rk2mx, i, j - 1), dx, simPlat_dt, ilast, jlast);
					beta2rnjmy = smooth2(vector(rk2my, i, j + 2), vector(rk2my, i, j + 1), vector(rk2my, i, j), vector(rk2my, i, j - 1), dx, simPlat_dt, ilast, jlast);
					beta2rnje = smooth2(vector(rk2e, i, j + 2), vector(rk2e, i, j + 1), vector(rk2e, i, j), vector(rk2e, i, j - 1), dx, simPlat_dt, ilast, jlast);
					beta3rnirho = smooth3(vector(rk2rho, i + 1, j), vector(rk2rho, i, j), vector(rk2rho, i - 1, j), vector(rk2rho, i - 2, j), dx, simPlat_dt, ilast, jlast);
					beta3rnimx = smooth3(vector(rk2mx, i + 1, j), vector(rk2mx, i, j), vector(rk2mx, i - 1, j), vector(rk2mx, i - 2, j), dx, simPlat_dt, ilast, jlast);
					beta3rnimy = smooth3(vector(rk2my, i + 1, j), vector(rk2my, i, j), vector(rk2my, i - 1, j), vector(rk2my, i - 2, j), dx, simPlat_dt, ilast, jlast);
					beta3rnie = smooth3(vector(rk2e, i + 1, j), vector(rk2e, i, j), vector(rk2e, i - 1, j), vector(rk2e, i - 2, j), dx, simPlat_dt, ilast, jlast);
					beta3rnjrho = smooth3(vector(rk2rho, i, j + 1), vector(rk2rho, i, j), vector(rk2rho, i, j - 1), vector(rk2rho, i, j - 2), dx, simPlat_dt, ilast, jlast);
					beta3rnjmx = smooth3(vector(rk2mx, i, j + 1), vector(rk2mx, i, j), vector(rk2mx, i, j - 1), vector(rk2mx, i, j - 2), dx, simPlat_dt, ilast, jlast);
					beta3rnjmy = smooth3(vector(rk2my, i, j + 1), vector(rk2my, i, j), vector(rk2my, i, j - 1), vector(rk2my, i, j - 2), dx, simPlat_dt, ilast, jlast);
					beta3rnje = smooth3(vector(rk2e, i, j + 1), vector(rk2e, i, j), vector(rk2e, i, j - 1), vector(rk2e, i, j - 2), dx, simPlat_dt, ilast, jlast);
					beta4rnirho = smooth4(vector(rk2rho, i, j), vector(rk2rho, i - 1, j), vector(rk2rho, i - 2, j), vector(rk2rho, i - 3, j), dx, simPlat_dt, ilast, jlast);
					beta4rnimx = smooth4(vector(rk2mx, i, j), vector(rk2mx, i - 1, j), vector(rk2mx, i - 2, j), vector(rk2mx, i - 3, j), dx, simPlat_dt, ilast, jlast);
					beta4rnimy = smooth4(vector(rk2my, i, j), vector(rk2my, i - 1, j), vector(rk2my, i - 2, j), vector(rk2my, i - 3, j), dx, simPlat_dt, ilast, jlast);
					beta4rnie = smooth4(vector(rk2e, i, j), vector(rk2e, i - 1, j), vector(rk2e, i - 2, j), vector(rk2e, i - 3, j), dx, simPlat_dt, ilast, jlast);
					beta4rnjrho = smooth4(vector(rk2rho, i, j), vector(rk2rho, i, j - 1), vector(rk2rho, i, j - 2), vector(rk2rho, i, j - 3), dx, simPlat_dt, ilast, jlast);
					beta4rnjmx = smooth4(vector(rk2mx, i, j), vector(rk2mx, i, j - 1), vector(rk2mx, i, j - 2), vector(rk2mx, i, j - 3), dx, simPlat_dt, ilast, jlast);
					beta4rnjmy = smooth4(vector(rk2my, i, j), vector(rk2my, i, j - 1), vector(rk2my, i, j - 2), vector(rk2my, i, j - 3), dx, simPlat_dt, ilast, jlast);
					beta4rnje = smooth4(vector(rk2e, i, j), vector(rk2e, i, j - 1), vector(rk2e, i, j - 2), vector(rk2e, i, j - 3), dx, simPlat_dt, ilast, jlast);
					w1lnirho = nlw1(beta1lnirho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1lnimx = nlw1(beta1lnimx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1lnimy = nlw1(beta1lnimy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1lnie = nlw1(beta1lnie, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1lnjrho = nlw1(beta1lnjrho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1lnjmx = nlw1(beta1lnjmx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1lnjmy = nlw1(beta1lnjmy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1lnje = nlw1(beta1lnje, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2lnirho = nlw2(beta2lnirho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2lnimx = nlw2(beta2lnimx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2lnimy = nlw2(beta2lnimy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2lnie = nlw2(beta2lnie, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2lnjrho = nlw2(beta2lnjrho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2lnjmx = nlw2(beta2lnjmx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2lnjmy = nlw2(beta2lnjmy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2lnje = nlw2(beta2lnje, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3lnirho = nlw3(beta3lnirho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3lnimx = nlw3(beta3lnimx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3lnimy = nlw3(beta3lnimy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3lnie = nlw3(beta3lnie, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3lnjrho = nlw3(beta3lnjrho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3lnjmx = nlw3(beta3lnjmx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3lnjmy = nlw3(beta3lnjmy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3lnje = nlw3(beta3lnje, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4lnirho = nlw4(beta4lnirho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4lnimx = nlw4(beta4lnimx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4lnimy = nlw4(beta4lnimy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4lnie = nlw4(beta4lnie, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4lnjrho = nlw4(beta4lnjrho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4lnjmx = nlw4(beta4lnjmx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4lnjmy = nlw4(beta4lnjmy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4lnje = nlw4(beta4lnje, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1rnirho = nlw1(beta1rnirho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1rnimx = nlw1(beta1rnimx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1rnimy = nlw1(beta1rnimy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1rnie = nlw1(beta1rnie, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1rnjrho = nlw1(beta1rnjrho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1rnjmx = nlw1(beta1rnjmx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1rnjmy = nlw1(beta1rnjmy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1rnje = nlw1(beta1rnje, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2rnirho = nlw2(beta2rnirho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2rnimx = nlw2(beta2rnimx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2rnimy = nlw2(beta2rnimy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2rnie = nlw2(beta2rnie, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2rnjrho = nlw2(beta2rnjrho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2rnjmx = nlw2(beta2rnjmx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2rnjmy = nlw2(beta2rnjmy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2rnje = nlw2(beta2rnje, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3rnirho = nlw3(beta3rnirho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3rnimx = nlw3(beta3rnimx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3rnimy = nlw3(beta3rnimy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3rnie = nlw3(beta3rnie, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3rnjrho = nlw3(beta3rnjrho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3rnjmx = nlw3(beta3rnjmx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3rnjmy = nlw3(beta3rnjmy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3rnje = nlw3(beta3rnje, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4rnirho = nlw4(beta4rnirho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4rnimx = nlw4(beta4rnimx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4rnimy = nlw4(beta4rnimy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4rnie = nlw4(beta4rnie, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4rnjrho = nlw4(beta4rnjrho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4rnjmx = nlw4(beta4rnjmx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4rnjmy = nlw4(beta4rnjmy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4rnje = nlw4(beta4rnje, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					wlnirho = w1lnirho + w2lnirho + w3lnirho + w4lnirho;
					wlnimx = w1lnimx + w2lnimx + w3lnimx + w4lnimx;
					wlnimy = w1lnimy + w2lnimy + w3lnimy + w4lnimy;
					wlnie = w1lnie + w2lnie + w3lnie + w4lnie;
					wlnjrho = w1lnjrho + w2lnjrho + w3lnjrho + w4lnjrho;
					wlnjmx = w1lnjmx + w2lnjmx + w3lnjmx + w4lnjmx;
					wlnjmy = w1lnjmy + w2lnjmy + w3lnjmy + w4lnjmy;
					wlnje = w1lnje + w2lnje + w3lnje + w4lnje;
					wrnirho = w1rnirho + w2rnirho + w3rnirho + w4rnirho;
					wrnimx = w1rnimx + w2rnimx + w3rnimx + w4rnimx;
					wrnimy = w1rnimy + w2rnimy + w3rnimy + w4rnimy;
					wrnie = w1rnie + w2rnie + w3rnie + w4rnie;
					wrnjrho = w1rnjrho + w2rnjrho + w3rnjrho + w4rnjrho;
					wrnjmx = w1rnjmx + w2rnjmx + w3rnjmx + w4rnjmx;
					wrnjmy = w1rnjmy + w2rnjmy + w3rnjmy + w4rnjmy;
					wrnje = w1rnje + w2rnje + w3rnje + w4rnje;
					w1lnirho = w1lnirho / wlnirho;
					w1lnimx = w1lnimx / wlnimx;
					w1lnimy = w1lnimy / wlnimy;
					w1lnie = w1lnie / wlnie;
					w1lnjrho = w1lnjrho / wlnjrho;
					w1lnjmx = w1lnjmx / wlnjmx;
					w1lnjmy = w1lnjmy / wlnjmy;
					w1lnje = w1lnje / wlnje;
					w2lnirho = w2lnirho / wlnirho;
					w2lnimx = w2lnimx / wlnimx;
					w2lnimy = w2lnimy / wlnimy;
					w2lnie = w2lnie / wlnie;
					w2lnjrho = w2lnjrho / wlnjrho;
					w2lnjmx = w2lnjmx / wlnjmx;
					w2lnjmy = w2lnjmy / wlnjmy;
					w2lnje = w2lnje / wlnje;
					w3lnirho = w3lnirho / wlnirho;
					w3lnimx = w3lnimx / wlnimx;
					w3lnimy = w3lnimy / wlnimy;
					w3lnie = w3lnie / wlnie;
					w3lnjrho = w3lnjrho / wlnjrho;
					w3lnjmx = w3lnjmx / wlnjmx;
					w3lnjmy = w3lnjmy / wlnjmy;
					w3lnje = w3lnje / wlnje;
					w4lnirho = w4lnirho / wlnirho;
					w4lnimx = w4lnimx / wlnimx;
					w4lnimy = w4lnimy / wlnimy;
					w4lnie = w4lnie / wlnie;
					w4lnjrho = w4lnjrho / wlnjrho;
					w4lnjmx = w4lnjmx / wlnjmx;
					w4lnjmy = w4lnjmy / wlnjmy;
					w4lnje = w4lnje / wlnje;
					w1rnirho = w1rnirho / wrnirho;
					w1rnimx = w1rnimx / wrnimx;
					w1rnimy = w1rnimy / wrnimy;
					w1rnie = w1rnie / wrnie;
					w1rnjrho = w1rnjrho / wrnjrho;
					w1rnjmx = w1rnjmx / wrnjmx;
					w1rnjmy = w1rnjmy / wrnjmy;
					w1rnje = w1rnje / wrnje;
					w2rnirho = w2rnirho / wrnirho;
					w2rnimx = w2rnimx / wrnimx;
					w2rnimy = w2rnimy / wrnimy;
					w2rnie = w2rnie / wrnie;
					w2rnjrho = w2rnjrho / wrnjrho;
					w2rnjmx = w2rnjmx / wrnjmx;
					w2rnjmy = w2rnjmy / wrnjmy;
					w2rnje = w2rnje / wrnje;
					w3rnirho = w3rnirho / wrnirho;
					w3rnimx = w3rnimx / wrnimx;
					w3rnimy = w3rnimy / wrnimy;
					w3rnie = w3rnie / wrnie;
					w3rnjrho = w3rnjrho / wrnjrho;
					w3rnjmx = w3rnjmx / wrnjmx;
					w3rnjmy = w3rnjmy / wrnjmy;
					w3rnje = w3rnje / wrnje;
					w4rnirho = w4rnirho / wrnirho;
					w4rnimx = w4rnimx / wrnimx;
					w4rnimy = w4rnimy / wrnimy;
					w4rnie = w4rnie / wrnie;
					w4rnjrho = w4rnjrho / wrnjrho;
					w4rnjmx = w4rnjmx / wrnjmx;
					w4rnjmy = w4rnjmy / wrnjmy;
					w4rnje = w4rnje / wrnje;
					rho_lni = weno(w1lnirho, w2lnirho, w3lnirho, w4lnirho, p1lnirho, p2lnirho, p3lnirho, p4lnirho, dx, simPlat_dt, ilast, jlast);
					mx_lni = weno(w1lnimx, w2lnimx, w3lnimx, w4lnimx, p1lnimx, p2lnimx, p3lnimx, p4lnimx, dx, simPlat_dt, ilast, jlast);
					my_lni = weno(w1lnimy, w2lnimy, w3lnimy, w4lnimy, p1lnimy, p2lnimy, p3lnimy, p4lnimy, dx, simPlat_dt, ilast, jlast);
					e_lni = weno(w1lnie, w2lnie, w3lnie, w4lnie, p1lnie, p2lnie, p3lnie, p4lnie, dx, simPlat_dt, ilast, jlast);
					rho_lnj = weno(w1lnjrho, w2lnjrho, w3lnjrho, w4lnjrho, p1lnjrho, p2lnjrho, p3lnjrho, p4lnjrho, dx, simPlat_dt, ilast, jlast);
					mx_lnj = weno(w1lnjmx, w2lnjmx, w3lnjmx, w4lnjmx, p1lnjmx, p2lnjmx, p3lnjmx, p4lnjmx, dx, simPlat_dt, ilast, jlast);
					my_lnj = weno(w1lnjmy, w2lnjmy, w3lnjmy, w4lnjmy, p1lnjmy, p2lnjmy, p3lnjmy, p4lnjmy, dx, simPlat_dt, ilast, jlast);
					e_lnj = weno(w1lnje, w2lnje, w3lnje, w4lnje, p1lnje, p2lnje, p3lnje, p4lnje, dx, simPlat_dt, ilast, jlast);
					rho_rni = weno(w1rnirho, w2rnirho, w3rnirho, w4rnirho, p1rnirho, p2rnirho, p3rnirho, p4rnirho, dx, simPlat_dt, ilast, jlast);
					mx_rni = weno(w1rnimx, w2rnimx, w3rnimx, w4rnimx, p1rnimx, p2rnimx, p3rnimx, p4rnimx, dx, simPlat_dt, ilast, jlast);
					my_rni = weno(w1rnimy, w2rnimy, w3rnimy, w4rnimy, p1rnimy, p2rnimy, p3rnimy, p4rnimy, dx, simPlat_dt, ilast, jlast);
					e_rni = weno(w1rnie, w2rnie, w3rnie, w4rnie, p1rnie, p2rnie, p3rnie, p4rnie, dx, simPlat_dt, ilast, jlast);
					rho_rnj = weno(w1rnjrho, w2rnjrho, w3rnjrho, w4rnjrho, p1rnjrho, p2rnjrho, p3rnjrho, p4rnjrho, dx, simPlat_dt, ilast, jlast);
					mx_rnj = weno(w1rnjmx, w2rnjmx, w3rnjmx, w4rnjmx, p1rnjmx, p2rnjmx, p3rnjmx, p4rnjmx, dx, simPlat_dt, ilast, jlast);
					my_rnj = weno(w1rnjmy, w2rnjmy, w3rnjmy, w4rnjmy, p1rnjmy, p2rnjmy, p3rnjmy, p4rnjmy, dx, simPlat_dt, ilast, jlast);
					e_rnj = weno(w1rnje, w2rnje, w3rnje, w4rnje, p1rnje, p2rnje, p3rnje, p4rnje, dx, simPlat_dt, ilast, jlast);
					p1lpirho = lor1(vector(rk2rho, i - 3, j), vector(rk2rho, i - 2, j), vector(rk2rho, i - 1, j), vector(rk2rho, i, j), dx, simPlat_dt, ilast, jlast);
					p1lpimx = lor1(vector(rk2mx, i - 3, j), vector(rk2mx, i - 2, j), vector(rk2mx, i - 1, j), vector(rk2mx, i, j), dx, simPlat_dt, ilast, jlast);
					p1lpimy = lor1(vector(rk2my, i - 3, j), vector(rk2my, i - 2, j), vector(rk2my, i - 1, j), vector(rk2my, i, j), dx, simPlat_dt, ilast, jlast);
					p1lpie = lor1(vector(rk2e, i - 3, j), vector(rk2e, i - 2, j), vector(rk2e, i - 1, j), vector(rk2e, i, j), dx, simPlat_dt, ilast, jlast);
					p1lpjrho = lor1(vector(rk2rho, i, j - 3), vector(rk2rho, i, j - 2), vector(rk2rho, i, j - 1), vector(rk2rho, i, j), dx, simPlat_dt, ilast, jlast);
					p1lpjmx = lor1(vector(rk2mx, i, j - 3), vector(rk2mx, i, j - 2), vector(rk2mx, i, j - 1), vector(rk2mx, i, j), dx, simPlat_dt, ilast, jlast);
					p1lpjmy = lor1(vector(rk2my, i, j - 3), vector(rk2my, i, j - 2), vector(rk2my, i, j - 1), vector(rk2my, i, j), dx, simPlat_dt, ilast, jlast);
					p1lpje = lor1(vector(rk2e, i, j - 3), vector(rk2e, i, j - 2), vector(rk2e, i, j - 1), vector(rk2e, i, j), dx, simPlat_dt, ilast, jlast);
					p2lpirho = lor2(vector(rk2rho, i - 2, j), vector(rk2rho, i - 1, j), vector(rk2rho, i, j), vector(rk2rho, i + 1, j), dx, simPlat_dt, ilast, jlast);
					p2lpimx = lor2(vector(rk2mx, i - 2, j), vector(rk2mx, i - 1, j), vector(rk2mx, i, j), vector(rk2mx, i + 1, j), dx, simPlat_dt, ilast, jlast);
					p2lpimy = lor2(vector(rk2my, i - 2, j), vector(rk2my, i - 1, j), vector(rk2my, i, j), vector(rk2my, i + 1, j), dx, simPlat_dt, ilast, jlast);
					p2lpie = lor2(vector(rk2e, i - 2, j), vector(rk2e, i - 1, j), vector(rk2e, i, j), vector(rk2e, i + 1, j), dx, simPlat_dt, ilast, jlast);
					p2lpjrho = lor2(vector(rk2rho, i, j - 2), vector(rk2rho, i, j - 1), vector(rk2rho, i, j), vector(rk2rho, i, j + 1), dx, simPlat_dt, ilast, jlast);
					p2lpjmx = lor2(vector(rk2mx, i, j - 2), vector(rk2mx, i, j - 1), vector(rk2mx, i, j), vector(rk2mx, i, j + 1), dx, simPlat_dt, ilast, jlast);
					p2lpjmy = lor2(vector(rk2my, i, j - 2), vector(rk2my, i, j - 1), vector(rk2my, i, j), vector(rk2my, i, j + 1), dx, simPlat_dt, ilast, jlast);
					p2lpje = lor2(vector(rk2e, i, j - 2), vector(rk2e, i, j - 1), vector(rk2e, i, j), vector(rk2e, i, j + 1), dx, simPlat_dt, ilast, jlast);
					p3lpirho = lor3(vector(rk2rho, i - 1, j), vector(rk2rho, i, j), vector(rk2rho, i + 1, j), vector(rk2rho, i + 2, j), dx, simPlat_dt, ilast, jlast);
					p3lpimx = lor3(vector(rk2mx, i - 1, j), vector(rk2mx, i, j), vector(rk2mx, i + 1, j), vector(rk2mx, i + 2, j), dx, simPlat_dt, ilast, jlast);
					p3lpimy = lor3(vector(rk2my, i - 1, j), vector(rk2my, i, j), vector(rk2my, i + 1, j), vector(rk2my, i + 2, j), dx, simPlat_dt, ilast, jlast);
					p3lpie = lor3(vector(rk2e, i - 1, j), vector(rk2e, i, j), vector(rk2e, i + 1, j), vector(rk2e, i + 2, j), dx, simPlat_dt, ilast, jlast);
					p3lpjrho = lor3(vector(rk2rho, i, j - 1), vector(rk2rho, i, j), vector(rk2rho, i, j + 1), vector(rk2rho, i, j + 2), dx, simPlat_dt, ilast, jlast);
					p3lpjmx = lor3(vector(rk2mx, i, j - 1), vector(rk2mx, i, j), vector(rk2mx, i, j + 1), vector(rk2mx, i, j + 2), dx, simPlat_dt, ilast, jlast);
					p3lpjmy = lor3(vector(rk2my, i, j - 1), vector(rk2my, i, j), vector(rk2my, i, j + 1), vector(rk2my, i, j + 2), dx, simPlat_dt, ilast, jlast);
					p3lpje = lor3(vector(rk2e, i, j - 1), vector(rk2e, i, j), vector(rk2e, i, j + 1), vector(rk2e, i, j + 2), dx, simPlat_dt, ilast, jlast);
					p4lpirho = lor4(vector(rk2rho, i, j), vector(rk2rho, i + 1, j), vector(rk2rho, i + 2, j), vector(rk2rho, i + 3, j), dx, simPlat_dt, ilast, jlast);
					p4lpimx = lor4(vector(rk2mx, i, j), vector(rk2mx, i + 1, j), vector(rk2mx, i + 2, j), vector(rk2mx, i + 3, j), dx, simPlat_dt, ilast, jlast);
					p4lpimy = lor4(vector(rk2my, i, j), vector(rk2my, i + 1, j), vector(rk2my, i + 2, j), vector(rk2my, i + 3, j), dx, simPlat_dt, ilast, jlast);
					p4lpie = lor4(vector(rk2e, i, j), vector(rk2e, i + 1, j), vector(rk2e, i + 2, j), vector(rk2e, i + 3, j), dx, simPlat_dt, ilast, jlast);
					p4lpjrho = lor4(vector(rk2rho, i, j), vector(rk2rho, i, j + 1), vector(rk2rho, i, j + 2), vector(rk2rho, i, j + 3), dx, simPlat_dt, ilast, jlast);
					p4lpjmx = lor4(vector(rk2mx, i, j), vector(rk2mx, i, j + 1), vector(rk2mx, i, j + 2), vector(rk2mx, i, j + 3), dx, simPlat_dt, ilast, jlast);
					p4lpjmy = lor4(vector(rk2my, i, j), vector(rk2my, i, j + 1), vector(rk2my, i, j + 2), vector(rk2my, i, j + 3), dx, simPlat_dt, ilast, jlast);
					p4lpje = lor4(vector(rk2e, i, j), vector(rk2e, i, j + 1), vector(rk2e, i, j + 2), vector(rk2e, i, j + 3), dx, simPlat_dt, ilast, jlast);
					p1rpirho = lor1(vector(rk2rho, i + 4, j), vector(rk2rho, i + 3, j), vector(rk2rho, i + 2, j), vector(rk2rho, i + 1, j), dx, simPlat_dt, ilast, jlast);
					p1rpimx = lor1(vector(rk2mx, i + 4, j), vector(rk2mx, i + 3, j), vector(rk2mx, i + 2, j), vector(rk2mx, i + 1, j), dx, simPlat_dt, ilast, jlast);
					p1rpimy = lor1(vector(rk2my, i + 4, j), vector(rk2my, i + 3, j), vector(rk2my, i + 2, j), vector(rk2my, i + 1, j), dx, simPlat_dt, ilast, jlast);
					p1rpie = lor1(vector(rk2e, i + 4, j), vector(rk2e, i + 3, j), vector(rk2e, i + 2, j), vector(rk2e, i + 1, j), dx, simPlat_dt, ilast, jlast);
					p1rpjrho = lor1(vector(rk2rho, i, j + 4), vector(rk2rho, i, j + 3), vector(rk2rho, i, j + 2), vector(rk2rho, i, j + 1), dx, simPlat_dt, ilast, jlast);
					p1rpjmx = lor1(vector(rk2mx, i, j + 4), vector(rk2mx, i, j + 3), vector(rk2mx, i, j + 2), vector(rk2mx, i, j + 1), dx, simPlat_dt, ilast, jlast);
					p1rpjmy = lor1(vector(rk2my, i, j + 4), vector(rk2my, i, j + 3), vector(rk2my, i, j + 2), vector(rk2my, i, j + 1), dx, simPlat_dt, ilast, jlast);
					p1rpje = lor1(vector(rk2e, i, j + 4), vector(rk2e, i, j + 3), vector(rk2e, i, j + 2), vector(rk2e, i, j + 1), dx, simPlat_dt, ilast, jlast);
					p2rpirho = lor2(vector(rk2rho, i + 3, j), vector(rk2rho, i + 2, j), vector(rk2rho, i + 1, j), vector(rk2rho, i, j), dx, simPlat_dt, ilast, jlast);
					p2rpimx = lor2(vector(rk2mx, i + 3, j), vector(rk2mx, i + 2, j), vector(rk2mx, i + 1, j), vector(rk2mx, i, j), dx, simPlat_dt, ilast, jlast);
					p2rpimy = lor2(vector(rk2my, i + 3, j), vector(rk2my, i + 2, j), vector(rk2my, i + 1, j), vector(rk2my, i, j), dx, simPlat_dt, ilast, jlast);
					p2rpie = lor2(vector(rk2e, i + 3, j), vector(rk2e, i + 2, j), vector(rk2e, i + 1, j), vector(rk2e, i, j), dx, simPlat_dt, ilast, jlast);
					p2rpjrho = lor2(vector(rk2rho, i, j + 3), vector(rk2rho, i, j + 2), vector(rk2rho, i, j + 1), vector(rk2rho, i, j), dx, simPlat_dt, ilast, jlast);
					p2rpjmx = lor2(vector(rk2mx, i, j + 3), vector(rk2mx, i, j + 2), vector(rk2mx, i, j + 1), vector(rk2mx, i, j), dx, simPlat_dt, ilast, jlast);
					p2rpjmy = lor2(vector(rk2my, i, j + 3), vector(rk2my, i, j + 2), vector(rk2my, i, j + 1), vector(rk2my, i, j), dx, simPlat_dt, ilast, jlast);
					p2rpje = lor2(vector(rk2e, i, j + 3), vector(rk2e, i, j + 2), vector(rk2e, i, j + 1), vector(rk2e, i, j), dx, simPlat_dt, ilast, jlast);
					p3rpirho = lor3(vector(rk2rho, i + 2, j), vector(rk2rho, i + 1, j), vector(rk2rho, i, j), vector(rk2rho, i - 1, j), dx, simPlat_dt, ilast, jlast);
					p3rpimx = lor3(vector(rk2mx, i + 2, j), vector(rk2mx, i + 1, j), vector(rk2mx, i, j), vector(rk2mx, i - 1, j), dx, simPlat_dt, ilast, jlast);
					p3rpimy = lor3(vector(rk2my, i + 2, j), vector(rk2my, i + 1, j), vector(rk2my, i, j), vector(rk2my, i - 1, j), dx, simPlat_dt, ilast, jlast);
					p3rpie = lor3(vector(rk2e, i + 2, j), vector(rk2e, i + 1, j), vector(rk2e, i, j), vector(rk2e, i - 1, j), dx, simPlat_dt, ilast, jlast);
					p3rpjrho = lor3(vector(rk2rho, i, j + 2), vector(rk2rho, i, j + 1), vector(rk2rho, i, j), vector(rk2rho, i, j - 1), dx, simPlat_dt, ilast, jlast);
					p3rpjmx = lor3(vector(rk2mx, i, j + 2), vector(rk2mx, i, j + 1), vector(rk2mx, i, j), vector(rk2mx, i, j - 1), dx, simPlat_dt, ilast, jlast);
					p3rpjmy = lor3(vector(rk2my, i, j + 2), vector(rk2my, i, j + 1), vector(rk2my, i, j), vector(rk2my, i, j - 1), dx, simPlat_dt, ilast, jlast);
					p3rpje = lor3(vector(rk2e, i, j + 2), vector(rk2e, i, j + 1), vector(rk2e, i, j), vector(rk2e, i, j - 1), dx, simPlat_dt, ilast, jlast);
					p4rpirho = lor4(vector(rk2rho, i + 1, j), vector(rk2rho, i, j), vector(rk2rho, i - 1, j), vector(rk2rho, i - 2, j), dx, simPlat_dt, ilast, jlast);
					p4rpimx = lor4(vector(rk2mx, i + 1, j), vector(rk2mx, i, j), vector(rk2mx, i - 1, j), vector(rk2mx, i - 2, j), dx, simPlat_dt, ilast, jlast);
					p4rpimy = lor4(vector(rk2my, i + 1, j), vector(rk2my, i, j), vector(rk2my, i - 1, j), vector(rk2my, i - 2, j), dx, simPlat_dt, ilast, jlast);
					p4rpie = lor4(vector(rk2e, i + 1, j), vector(rk2e, i, j), vector(rk2e, i - 1, j), vector(rk2e, i - 2, j), dx, simPlat_dt, ilast, jlast);
					p4rpjrho = lor4(vector(rk2rho, i, j + 1), vector(rk2rho, i, j), vector(rk2rho, i, j - 1), vector(rk2rho, i, j - 2), dx, simPlat_dt, ilast, jlast);
					p4rpjmx = lor4(vector(rk2mx, i, j + 1), vector(rk2mx, i, j), vector(rk2mx, i, j - 1), vector(rk2mx, i, j - 2), dx, simPlat_dt, ilast, jlast);
					p4rpjmy = lor4(vector(rk2my, i, j + 1), vector(rk2my, i, j), vector(rk2my, i, j - 1), vector(rk2my, i, j - 2), dx, simPlat_dt, ilast, jlast);
					p4rpje = lor4(vector(rk2e, i, j + 1), vector(rk2e, i, j), vector(rk2e, i, j - 1), vector(rk2e, i, j - 2), dx, simPlat_dt, ilast, jlast);
					beta1lpirho = smooth1(vector(rk2rho, i - 3, j), vector(rk2rho, i - 2, j), vector(rk2rho, i - 1, j), vector(rk2rho, i, j), dx, simPlat_dt, ilast, jlast);
					beta1lpimx = smooth1(vector(rk2mx, i - 3, j), vector(rk2mx, i - 2, j), vector(rk2mx, i - 1, j), vector(rk2mx, i, j), dx, simPlat_dt, ilast, jlast);
					beta1lpimy = smooth1(vector(rk2my, i - 3, j), vector(rk2my, i - 2, j), vector(rk2my, i - 1, j), vector(rk2my, i, j), dx, simPlat_dt, ilast, jlast);
					beta1lpie = smooth1(vector(rk2e, i - 3, j), vector(rk2e, i - 2, j), vector(rk2e, i - 1, j), vector(rk2e, i, j), dx, simPlat_dt, ilast, jlast);
					beta1lpjrho = smooth1(vector(rk2rho, i, j - 3), vector(rk2rho, i, j - 2), vector(rk2rho, i, j - 1), vector(rk2rho, i, j), dx, simPlat_dt, ilast, jlast);
					beta1lpjmx = smooth1(vector(rk2mx, i, j - 3), vector(rk2mx, i, j - 2), vector(rk2mx, i, j - 1), vector(rk2mx, i, j), dx, simPlat_dt, ilast, jlast);
					beta1lpjmy = smooth1(vector(rk2my, i, j - 3), vector(rk2my, i, j - 2), vector(rk2my, i, j - 1), vector(rk2my, i, j), dx, simPlat_dt, ilast, jlast);
					beta1lpje = smooth1(vector(rk2e, i, j - 3), vector(rk2e, i, j - 2), vector(rk2e, i, j - 1), vector(rk2e, i, j), dx, simPlat_dt, ilast, jlast);
					beta2lpirho = smooth2(vector(rk2rho, i - 2, j), vector(rk2rho, i - 1, j), vector(rk2rho, i, j), vector(rk2rho, i + 1, j), dx, simPlat_dt, ilast, jlast);
					beta2lpimx = smooth2(vector(rk2mx, i - 2, j), vector(rk2mx, i - 1, j), vector(rk2mx, i, j), vector(rk2mx, i + 1, j), dx, simPlat_dt, ilast, jlast);
					beta2lpimy = smooth2(vector(rk2my, i - 2, j), vector(rk2my, i - 1, j), vector(rk2my, i, j), vector(rk2my, i + 1, j), dx, simPlat_dt, ilast, jlast);
					beta2lpie = smooth2(vector(rk2e, i - 2, j), vector(rk2e, i - 1, j), vector(rk2e, i, j), vector(rk2e, i + 1, j), dx, simPlat_dt, ilast, jlast);
					beta2lpjrho = smooth2(vector(rk2rho, i, j - 2), vector(rk2rho, i, j - 1), vector(rk2rho, i, j), vector(rk2rho, i, j + 1), dx, simPlat_dt, ilast, jlast);
					beta2lpjmx = smooth2(vector(rk2mx, i, j - 2), vector(rk2mx, i, j - 1), vector(rk2mx, i, j), vector(rk2mx, i, j + 1), dx, simPlat_dt, ilast, jlast);
					beta2lpjmy = smooth2(vector(rk2my, i, j - 2), vector(rk2my, i, j - 1), vector(rk2my, i, j), vector(rk2my, i, j + 1), dx, simPlat_dt, ilast, jlast);
					beta2lpje = smooth2(vector(rk2e, i, j - 2), vector(rk2e, i, j - 1), vector(rk2e, i, j), vector(rk2e, i, j + 1), dx, simPlat_dt, ilast, jlast);
					beta3lpirho = smooth3(vector(rk2rho, i - 1, j), vector(rk2rho, i, j), vector(rk2rho, i + 1, j), vector(rk2rho, i + 2, j), dx, simPlat_dt, ilast, jlast);
					beta3lpimx = smooth3(vector(rk2mx, i - 1, j), vector(rk2mx, i, j), vector(rk2mx, i + 1, j), vector(rk2mx, i + 2, j), dx, simPlat_dt, ilast, jlast);
					beta3lpimy = smooth3(vector(rk2my, i - 1, j), vector(rk2my, i, j), vector(rk2my, i + 1, j), vector(rk2my, i + 2, j), dx, simPlat_dt, ilast, jlast);
					beta3lpie = smooth3(vector(rk2e, i - 1, j), vector(rk2e, i, j), vector(rk2e, i + 1, j), vector(rk2e, i + 2, j), dx, simPlat_dt, ilast, jlast);
					beta3lpjrho = smooth3(vector(rk2rho, i, j - 1), vector(rk2rho, i, j), vector(rk2rho, i, j + 1), vector(rk2rho, i, j + 2), dx, simPlat_dt, ilast, jlast);
					beta3lpjmx = smooth3(vector(rk2mx, i, j - 1), vector(rk2mx, i, j), vector(rk2mx, i, j + 1), vector(rk2mx, i, j + 2), dx, simPlat_dt, ilast, jlast);
					beta3lpjmy = smooth3(vector(rk2my, i, j - 1), vector(rk2my, i, j), vector(rk2my, i, j + 1), vector(rk2my, i, j + 2), dx, simPlat_dt, ilast, jlast);
					beta3lpje = smooth3(vector(rk2e, i, j - 1), vector(rk2e, i, j), vector(rk2e, i, j + 1), vector(rk2e, i, j + 2), dx, simPlat_dt, ilast, jlast);
					beta4lpirho = smooth4(vector(rk2rho, i, j), vector(rk2rho, i + 1, j), vector(rk2rho, i + 2, j), vector(rk2rho, i + 3, j), dx, simPlat_dt, ilast, jlast);
					beta4lpimx = smooth4(vector(rk2mx, i, j), vector(rk2mx, i + 1, j), vector(rk2mx, i + 2, j), vector(rk2mx, i + 3, j), dx, simPlat_dt, ilast, jlast);
					beta4lpimy = smooth4(vector(rk2my, i, j), vector(rk2my, i + 1, j), vector(rk2my, i + 2, j), vector(rk2my, i + 3, j), dx, simPlat_dt, ilast, jlast);
					beta4lpie = smooth4(vector(rk2e, i, j), vector(rk2e, i + 1, j), vector(rk2e, i + 2, j), vector(rk2e, i + 3, j), dx, simPlat_dt, ilast, jlast);
					beta4lpjrho = smooth4(vector(rk2rho, i, j), vector(rk2rho, i, j + 1), vector(rk2rho, i, j + 2), vector(rk2rho, i, j + 3), dx, simPlat_dt, ilast, jlast);
					beta4lpjmx = smooth4(vector(rk2mx, i, j), vector(rk2mx, i, j + 1), vector(rk2mx, i, j + 2), vector(rk2mx, i, j + 3), dx, simPlat_dt, ilast, jlast);
					beta4lpjmy = smooth4(vector(rk2my, i, j), vector(rk2my, i, j + 1), vector(rk2my, i, j + 2), vector(rk2my, i, j + 3), dx, simPlat_dt, ilast, jlast);
					beta4lpje = smooth4(vector(rk2e, i, j), vector(rk2e, i, j + 1), vector(rk2e, i, j + 2), vector(rk2e, i, j + 3), dx, simPlat_dt, ilast, jlast);
					beta1rpirho = smooth1(vector(rk2rho, i + 4, j), vector(rk2rho, i + 3, j), vector(rk2rho, i + 2, j), vector(rk2rho, i + 1, j), dx, simPlat_dt, ilast, jlast);
					beta1rpimx = smooth1(vector(rk2mx, i + 4, j), vector(rk2mx, i + 3, j), vector(rk2mx, i + 2, j), vector(rk2mx, i + 1, j), dx, simPlat_dt, ilast, jlast);
					beta1rpimy = smooth1(vector(rk2my, i + 4, j), vector(rk2my, i + 3, j), vector(rk2my, i + 2, j), vector(rk2my, i + 1, j), dx, simPlat_dt, ilast, jlast);
					beta1rpie = smooth1(vector(rk2e, i + 4, j), vector(rk2e, i + 3, j), vector(rk2e, i + 2, j), vector(rk2e, i + 1, j), dx, simPlat_dt, ilast, jlast);
					beta1rpjrho = smooth1(vector(rk2rho, i, j + 4), vector(rk2rho, i, j + 3), vector(rk2rho, i, j + 2), vector(rk2rho, i, j + 1), dx, simPlat_dt, ilast, jlast);
					beta1rpjmx = smooth1(vector(rk2mx, i, j + 4), vector(rk2mx, i, j + 3), vector(rk2mx, i, j + 2), vector(rk2mx, i, j + 1), dx, simPlat_dt, ilast, jlast);
					beta1rpjmy = smooth1(vector(rk2my, i, j + 4), vector(rk2my, i, j + 3), vector(rk2my, i, j + 2), vector(rk2my, i, j + 1), dx, simPlat_dt, ilast, jlast);
					beta1rpje = smooth1(vector(rk2e, i, j + 4), vector(rk2e, i, j + 3), vector(rk2e, i, j + 2), vector(rk2e, i, j + 1), dx, simPlat_dt, ilast, jlast);
					beta2rpirho = smooth2(vector(rk2rho, i + 3, j), vector(rk2rho, i + 2, j), vector(rk2rho, i + 1, j), vector(rk2rho, i, j), dx, simPlat_dt, ilast, jlast);
					beta2rpimx = smooth2(vector(rk2mx, i + 3, j), vector(rk2mx, i + 2, j), vector(rk2mx, i + 1, j), vector(rk2mx, i, j), dx, simPlat_dt, ilast, jlast);
					beta2rpimy = smooth2(vector(rk2my, i + 3, j), vector(rk2my, i + 2, j), vector(rk2my, i + 1, j), vector(rk2my, i, j), dx, simPlat_dt, ilast, jlast);
					beta2rpie = smooth2(vector(rk2e, i + 3, j), vector(rk2e, i + 2, j), vector(rk2e, i + 1, j), vector(rk2e, i, j), dx, simPlat_dt, ilast, jlast);
					beta2rpjrho = smooth2(vector(rk2rho, i, j + 3), vector(rk2rho, i, j + 2), vector(rk2rho, i, j + 1), vector(rk2rho, i, j), dx, simPlat_dt, ilast, jlast);
					beta2rpjmx = smooth2(vector(rk2mx, i, j + 3), vector(rk2mx, i, j + 2), vector(rk2mx, i, j + 1), vector(rk2mx, i, j), dx, simPlat_dt, ilast, jlast);
					beta2rpjmy = smooth2(vector(rk2my, i, j + 3), vector(rk2my, i, j + 2), vector(rk2my, i, j + 1), vector(rk2my, i, j), dx, simPlat_dt, ilast, jlast);
					beta2rpje = smooth2(vector(rk2e, i, j + 3), vector(rk2e, i, j + 2), vector(rk2e, i, j + 1), vector(rk2e, i, j), dx, simPlat_dt, ilast, jlast);
					beta3rpirho = smooth3(vector(rk2rho, i + 2, j), vector(rk2rho, i + 1, j), vector(rk2rho, i, j), vector(rk2rho, i - 1, j), dx, simPlat_dt, ilast, jlast);
					beta3rpimx = smooth3(vector(rk2mx, i + 2, j), vector(rk2mx, i + 1, j), vector(rk2mx, i, j), vector(rk2mx, i - 1, j), dx, simPlat_dt, ilast, jlast);
					beta3rpimy = smooth3(vector(rk2my, i + 2, j), vector(rk2my, i + 1, j), vector(rk2my, i, j), vector(rk2my, i - 1, j), dx, simPlat_dt, ilast, jlast);
					beta3rpie = smooth3(vector(rk2e, i + 2, j), vector(rk2e, i + 1, j), vector(rk2e, i, j), vector(rk2e, i - 1, j), dx, simPlat_dt, ilast, jlast);
					beta3rpjrho = smooth3(vector(rk2rho, i, j + 2), vector(rk2rho, i, j + 1), vector(rk2rho, i, j), vector(rk2rho, i, j - 1), dx, simPlat_dt, ilast, jlast);
					beta3rpjmx = smooth3(vector(rk2mx, i, j + 2), vector(rk2mx, i, j + 1), vector(rk2mx, i, j), vector(rk2mx, i, j - 1), dx, simPlat_dt, ilast, jlast);
					beta3rpjmy = smooth3(vector(rk2my, i, j + 2), vector(rk2my, i, j + 1), vector(rk2my, i, j), vector(rk2my, i, j - 1), dx, simPlat_dt, ilast, jlast);
					beta3rpje = smooth3(vector(rk2e, i, j + 2), vector(rk2e, i, j + 1), vector(rk2e, i, j), vector(rk2e, i, j - 1), dx, simPlat_dt, ilast, jlast);
					beta4rpirho = smooth4(vector(rk2rho, i + 1, j), vector(rk2rho, i, j), vector(rk2rho, i - 1, j), vector(rk2rho, i - 2, j), dx, simPlat_dt, ilast, jlast);
					beta4rpimx = smooth4(vector(rk2mx, i + 1, j), vector(rk2mx, i, j), vector(rk2mx, i - 1, j), vector(rk2mx, i - 2, j), dx, simPlat_dt, ilast, jlast);
					beta4rpimy = smooth4(vector(rk2my, i + 1, j), vector(rk2my, i, j), vector(rk2my, i - 1, j), vector(rk2my, i - 2, j), dx, simPlat_dt, ilast, jlast);
					beta4rpie = smooth4(vector(rk2e, i + 1, j), vector(rk2e, i, j), vector(rk2e, i - 1, j), vector(rk2e, i - 2, j), dx, simPlat_dt, ilast, jlast);
					beta4rpjrho = smooth4(vector(rk2rho, i, j + 1), vector(rk2rho, i, j), vector(rk2rho, i, j - 1), vector(rk2rho, i, j - 2), dx, simPlat_dt, ilast, jlast);
					beta4rpjmx = smooth4(vector(rk2mx, i, j + 1), vector(rk2mx, i, j), vector(rk2mx, i, j - 1), vector(rk2mx, i, j - 2), dx, simPlat_dt, ilast, jlast);
					beta4rpjmy = smooth4(vector(rk2my, i, j + 1), vector(rk2my, i, j), vector(rk2my, i, j - 1), vector(rk2my, i, j - 2), dx, simPlat_dt, ilast, jlast);
					beta4rpje = smooth4(vector(rk2e, i, j + 1), vector(rk2e, i, j), vector(rk2e, i, j - 1), vector(rk2e, i, j - 2), dx, simPlat_dt, ilast, jlast);
					w1lpirho = nlw1(beta1lpirho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1lpimx = nlw1(beta1lpimx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1lpimy = nlw1(beta1lpimy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1lpie = nlw1(beta1lpie, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1lpjrho = nlw1(beta1lpjrho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1lpjmx = nlw1(beta1lpjmx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1lpjmy = nlw1(beta1lpjmy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1lpje = nlw1(beta1lpje, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2lpirho = nlw2(beta2lpirho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2lpimx = nlw2(beta2lpimx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2lpimy = nlw2(beta2lpimy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2lpie = nlw2(beta2lpie, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2lpjrho = nlw2(beta2lpjrho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2lpjmx = nlw2(beta2lpjmx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2lpjmy = nlw2(beta2lpjmy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2lpje = nlw2(beta2lpje, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3lpirho = nlw3(beta3lpirho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3lpimx = nlw3(beta3lpimx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3lpimy = nlw3(beta3lpimy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3lpie = nlw3(beta3lpie, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3lpjrho = nlw3(beta3lpjrho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3lpjmx = nlw3(beta3lpjmx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3lpjmy = nlw3(beta3lpjmy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3lpje = nlw3(beta3lpje, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4lpirho = nlw4(beta4lpirho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4lpimx = nlw4(beta4lpimx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4lpimy = nlw4(beta4lpimy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4lpie = nlw4(beta4lpie, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4lpjrho = nlw4(beta4lpjrho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4lpjmx = nlw4(beta4lpjmx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4lpjmy = nlw4(beta4lpjmy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4lpje = nlw4(beta4lpje, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1rpirho = nlw1(beta1rpirho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1rpimx = nlw1(beta1rpimx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1rpimy = nlw1(beta1rpimy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1rpie = nlw1(beta1rpie, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1rpjrho = nlw1(beta1rpjrho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1rpjmx = nlw1(beta1rpjmx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1rpjmy = nlw1(beta1rpjmy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w1rpje = nlw1(beta1rpje, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2rpirho = nlw2(beta2rpirho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2rpimx = nlw2(beta2rpimx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2rpimy = nlw2(beta2rpimy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2rpie = nlw2(beta2rpie, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2rpjrho = nlw2(beta2rpjrho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2rpjmx = nlw2(beta2rpjmx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2rpjmy = nlw2(beta2rpjmy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w2rpje = nlw2(beta2rpje, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3rpirho = nlw3(beta3rpirho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3rpimx = nlw3(beta3rpimx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3rpimy = nlw3(beta3rpimy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3rpie = nlw3(beta3rpie, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3rpjrho = nlw3(beta3rpjrho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3rpjmx = nlw3(beta3rpjmx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3rpjmy = nlw3(beta3rpjmy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w3rpje = nlw3(beta3rpje, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4rpirho = nlw4(beta4rpirho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4rpimx = nlw4(beta4rpimx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4rpimy = nlw4(beta4rpimy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4rpie = nlw4(beta4rpie, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4rpjrho = nlw4(beta4rpjrho, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4rpjmx = nlw4(beta4rpjmx, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4rpjmy = nlw4(beta4rpjmy, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					w4rpje = nlw4(beta4rpje, epsilon_domain, dx, simPlat_dt, ilast, jlast);
					wlpirho = w1lpirho + w2lpirho + w3lpirho + w4lpirho;
					wlpimx = w1lpimx + w2lpimx + w3lpimx + w4lpimx;
					wlpimy = w1lpimy + w2lpimy + w3lpimy + w4lpimy;
					wlpie = w1lpie + w2lpie + w3lpie + w4lpie;
					wlpjrho = w1lpjrho + w2lpjrho + w3lpjrho + w4lpjrho;
					wlpjmx = w1lpjmx + w2lpjmx + w3lpjmx + w4lpjmx;
					wlpjmy = w1lpjmy + w2lpjmy + w3lpjmy + w4lpjmy;
					wlpje = w1lpje + w2lpje + w3lpje + w4lpje;
					wrpirho = w1rpirho + w2rpirho + w3rpirho + w4rpirho;
					wrpimx = w1rpimx + w2rpimx + w3rpimx + w4rpimx;
					wrpimy = w1rpimy + w2rpimy + w3rpimy + w4rpimy;
					wrpie = w1rpie + w2rpie + w3rpie + w4rpie;
					wrpjrho = w1rpjrho + w2rpjrho + w3rpjrho + w4rpjrho;
					wrpjmx = w1rpjmx + w2rpjmx + w3rpjmx + w4rpjmx;
					wrpjmy = w1rpjmy + w2rpjmy + w3rpjmy + w4rpjmy;
					wrpje = w1rpje + w2rpje + w3rpje + w4rpje;
					w1lpirho = w1lpirho / wlpirho;
					w1lpimx = w1lpimx / wlpimx;
					w1lpimy = w1lpimy / wlpimy;
					w1lpie = w1lpie / wlpie;
					w1lpjrho = w1lpjrho / wlpjrho;
					w1lpjmx = w1lpjmx / wlpjmx;
					w1lpjmy = w1lpjmy / wlpjmy;
					w1lpje = w1lpje / wlpje;
					w2lpirho = w2lpirho / wlpirho;
					w2lpimx = w2lpimx / wlpimx;
					w2lpimy = w2lpimy / wlpimy;
					w2lpie = w2lpie / wlpie;
					w2lpjrho = w2lpjrho / wlpjrho;
					w2lpjmx = w2lpjmx / wlpjmx;
					w2lpjmy = w2lpjmy / wlpjmy;
					w2lpje = w2lpje / wlpje;
					w3lpirho = w3lpirho / wlpirho;
					w3lpimx = w3lpimx / wlpimx;
					w3lpimy = w3lpimy / wlpimy;
					w3lpie = w3lpie / wlpie;
					w3lpjrho = w3lpjrho / wlpjrho;
					w3lpjmx = w3lpjmx / wlpjmx;
					w3lpjmy = w3lpjmy / wlpjmy;
					w3lpje = w3lpje / wlpje;
					w4lpirho = w4lpirho / wlpirho;
					w4lpimx = w4lpimx / wlpimx;
					w4lpimy = w4lpimy / wlpimy;
					w4lpie = w4lpie / wlpie;
					w4lpjrho = w4lpjrho / wlpjrho;
					w4lpjmx = w4lpjmx / wlpjmx;
					w4lpjmy = w4lpjmy / wlpjmy;
					w4lpje = w4lpje / wlpje;
					w1rpirho = w1rpirho / wrpirho;
					w1rpimx = w1rpimx / wrpimx;
					w1rpimy = w1rpimy / wrpimy;
					w1rpie = w1rpie / wrpie;
					w1rpjrho = w1rpjrho / wrpjrho;
					w1rpjmx = w1rpjmx / wrpjmx;
					w1rpjmy = w1rpjmy / wrpjmy;
					w1rpje = w1rpje / wrpje;
					w2rpirho = w2rpirho / wrpirho;
					w2rpimx = w2rpimx / wrpimx;
					w2rpimy = w2rpimy / wrpimy;
					w2rpie = w2rpie / wrpie;
					w2rpjrho = w2rpjrho / wrpjrho;
					w2rpjmx = w2rpjmx / wrpjmx;
					w2rpjmy = w2rpjmy / wrpjmy;
					w2rpje = w2rpje / wrpje;
					w3rpirho = w3rpirho / wrpirho;
					w3rpimx = w3rpimx / wrpimx;
					w3rpimy = w3rpimy / wrpimy;
					w3rpie = w3rpie / wrpie;
					w3rpjrho = w3rpjrho / wrpjrho;
					w3rpjmx = w3rpjmx / wrpjmx;
					w3rpjmy = w3rpjmy / wrpjmy;
					w3rpje = w3rpje / wrpje;
					w4rpirho = w4rpirho / wrpirho;
					w4rpimx = w4rpimx / wrpimx;
					w4rpimy = w4rpimy / wrpimy;
					w4rpie = w4rpie / wrpie;
					w4rpjrho = w4rpjrho / wrpjrho;
					w4rpjmx = w4rpjmx / wrpjmx;
					w4rpjmy = w4rpjmy / wrpjmy;
					w4rpje = w4rpje / wrpje;
					rho_lpi = weno(w1lpirho, w2lpirho, w3lpirho, w4lpirho, p1lpirho, p2lpirho, p3lpirho, p4lpirho, dx, simPlat_dt, ilast, jlast);
					mx_lpi = weno(w1lpimx, w2lpimx, w3lpimx, w4lpimx, p1lpimx, p2lpimx, p3lpimx, p4lpimx, dx, simPlat_dt, ilast, jlast);
					my_lpi = weno(w1lpimy, w2lpimy, w3lpimy, w4lpimy, p1lpimy, p2lpimy, p3lpimy, p4lpimy, dx, simPlat_dt, ilast, jlast);
					e_lpi = weno(w1lpie, w2lpie, w3lpie, w4lpie, p1lpie, p2lpie, p3lpie, p4lpie, dx, simPlat_dt, ilast, jlast);
					rho_lpj = weno(w1lpjrho, w2lpjrho, w3lpjrho, w4lpjrho, p1lpjrho, p2lpjrho, p3lpjrho, p4lpjrho, dx, simPlat_dt, ilast, jlast);
					mx_lpj = weno(w1lpjmx, w2lpjmx, w3lpjmx, w4lpjmx, p1lpjmx, p2lpjmx, p3lpjmx, p4lpjmx, dx, simPlat_dt, ilast, jlast);
					my_lpj = weno(w1lpjmy, w2lpjmy, w3lpjmy, w4lpjmy, p1lpjmy, p2lpjmy, p3lpjmy, p4lpjmy, dx, simPlat_dt, ilast, jlast);
					e_lpj = weno(w1lpje, w2lpje, w3lpje, w4lpje, p1lpje, p2lpje, p3lpje, p4lpje, dx, simPlat_dt, ilast, jlast);
					rho_rpi = weno(w1rpirho, w2rpirho, w3rpirho, w4rpirho, p1rpirho, p2rpirho, p3rpirho, p4rpirho, dx, simPlat_dt, ilast, jlast);
					mx_rpi = weno(w1rpimx, w2rpimx, w3rpimx, w4rpimx, p1rpimx, p2rpimx, p3rpimx, p4rpimx, dx, simPlat_dt, ilast, jlast);
					my_rpi = weno(w1rpimy, w2rpimy, w3rpimy, w4rpimy, p1rpimy, p2rpimy, p3rpimy, p4rpimy, dx, simPlat_dt, ilast, jlast);
					e_rpi = weno(w1rpie, w2rpie, w3rpie, w4rpie, p1rpie, p2rpie, p3rpie, p4rpie, dx, simPlat_dt, ilast, jlast);
					rho_rpj = weno(w1rpjrho, w2rpjrho, w3rpjrho, w4rpjrho, p1rpjrho, p2rpjrho, p3rpjrho, p4rpjrho, dx, simPlat_dt, ilast, jlast);
					mx_rpj = weno(w1rpjmx, w2rpjmx, w3rpjmx, w4rpjmx, p1rpjmx, p2rpjmx, p3rpjmx, p4rpjmx, dx, simPlat_dt, ilast, jlast);
					my_rpj = weno(w1rpjmy, w2rpjmy, w3rpjmy, w4rpjmy, p1rpjmy, p2rpjmy, p3rpjmy, p4rpjmy, dx, simPlat_dt, ilast, jlast);
					e_rpj = weno(w1rpje, w2rpje, w3rpje, w4rpje, p1rpje, p2rpje, p3rpje, p4rpje, dx, simPlat_dt, ilast, jlast);
					P_lpi = (gamma - 1.0) * (e_lpi - rho_lpi * (((mx_lpi / rho_lpi) * (mx_lpi / rho_lpi)) / 2.0 + ((my_lpi / rho_lpi) * (my_lpi / rho_lpi)) / 2.0));
					P_lpj = (gamma - 1.0) * (e_lpj - rho_lpj * (((mx_lpj / rho_lpj) * (mx_lpj / rho_lpj)) / 2.0 + ((my_lpj / rho_lpj) * (my_lpj / rho_lpj)) / 2.0));
					P_lni = (gamma - 1.0) * (e_lni - rho_lni * (((mx_lni / rho_lni) * (mx_lni / rho_lni)) / 2.0 + ((my_lni / rho_lni) * (my_lni / rho_lni)) / 2.0));
					P_lnj = (gamma - 1.0) * (e_lnj - rho_lnj * (((mx_lnj / rho_lnj) * (mx_lnj / rho_lnj)) / 2.0 + ((my_lnj / rho_lnj) * (my_lnj / rho_lnj)) / 2.0));
					P_rpi = (gamma - 1.0) * (e_rpi - rho_rpi * (((mx_rpi / rho_rpi) * (mx_rpi / rho_rpi)) / 2.0 + ((my_rpi / rho_rpi) * (my_rpi / rho_rpi)) / 2.0));
					P_rpj = (gamma - 1.0) * (e_rpj - rho_rpj * (((mx_rpj / rho_rpj) * (mx_rpj / rho_rpj)) / 2.0 + ((my_rpj / rho_rpj) * (my_rpj / rho_rpj)) / 2.0));
					P_rni = (gamma - 1.0) * (e_rni - rho_rni * (((mx_rni / rho_rni) * (mx_rni / rho_rni)) / 2.0 + ((my_rni / rho_rni) * (my_rni / rho_rni)) / 2.0));
					P_rnj = (gamma - 1.0) * (e_rnj - rho_rnj * (((mx_rnj / rho_rnj) * (mx_rnj / rho_rnj)) / 2.0 + ((my_rnj / rho_rnj) * (my_rnj / rho_rnj)) / 2.0));
					rk2cy = vector(rk2my, i, j) / vector(rk2rho, i, j);
					rk2cx = vector(rk2mx, i, j) / vector(rk2rho, i, j);
					rk2c = sqrt(fabs((gamma * vector(P, i, j)) / vector(rk2rho, i, j)));
					Speedirho = MAX(fabs(rk2cx), MAX(fabs(rk2cx - rk2c), fabs(rk2cx + rk2c)));
					Speedimx = MAX(fabs(rk2cx), MAX(fabs(rk2cx - rk2c), fabs(rk2cx + rk2c)));
					Speedimy = MAX(fabs(rk2cx), MAX(fabs(rk2cx - rk2c), fabs(rk2cx + rk2c)));
					Speedie = MAX(fabs(rk2cx), MAX(fabs(rk2cx - rk2c), fabs(rk2cx + rk2c)));
					Speedjrho = MAX(fabs(rk2cy), MAX(fabs(rk2cy - rk2c), fabs(rk2cy + rk2c)));
					Speedjmx = MAX(fabs(rk2cy), MAX(fabs(rk2cy - rk2c), fabs(rk2cy + rk2c)));
					Speedjmy = MAX(fabs(rk2cy), MAX(fabs(rk2cy - rk2c), fabs(rk2cy + rk2c)));
					Speedje = MAX(fabs(rk2cy), MAX(fabs(rk2cy - rk2c), fabs(rk2cy + rk2c)));
					rk2cy = vector(rk2my, i + 1, j) / vector(rk2rho, i + 1, j);
					rk2cx = vector(rk2mx, i + 1, j) / vector(rk2rho, i + 1, j);
					rk2c = sqrt(fabs((gamma * vector(P, i + 1, j)) / vector(rk2rho, i + 1, j)));
					Speedpirho = MAX(fabs(rk2cx), MAX(fabs(rk2cx - rk2c), fabs(rk2cx + rk2c)));
					Speedpimx = MAX(fabs(rk2cx), MAX(fabs(rk2cx - rk2c), fabs(rk2cx + rk2c)));
					Speedpimy = MAX(fabs(rk2cx), MAX(fabs(rk2cx - rk2c), fabs(rk2cx + rk2c)));
					Speedpie = MAX(fabs(rk2cx), MAX(fabs(rk2cx - rk2c), fabs(rk2cx + rk2c)));
					rk2cy = vector(rk2my, i, j + 1) / vector(rk2rho, i, j + 1);
					rk2cx = vector(rk2mx, i, j + 1) / vector(rk2rho, i, j + 1);
					rk2c = sqrt(fabs((gamma * vector(P, i, j + 1)) / vector(rk2rho, i, j + 1)));
					Speedpjrho = MAX(fabs(rk2cy), MAX(fabs(rk2cy - rk2c), fabs(rk2cy + rk2c)));
					Speedpjmx = MAX(fabs(rk2cy), MAX(fabs(rk2cy - rk2c), fabs(rk2cy + rk2c)));
					Speedpjmy = MAX(fabs(rk2cy), MAX(fabs(rk2cy - rk2c), fabs(rk2cy + rk2c)));
					Speedpje = MAX(fabs(rk2cy), MAX(fabs(rk2cy - rk2c), fabs(rk2cy + rk2c)));
					rk2cy = vector(rk2my, i - 1, j) / vector(rk2rho, i - 1, j);
					rk2cx = vector(rk2mx, i - 1, j) / vector(rk2rho, i - 1, j);
					rk2c = sqrt(fabs((gamma * vector(P, i - 1, j)) / vector(rk2rho, i - 1, j)));
					Speednirho = MAX(fabs(rk2cx), MAX(fabs(rk2cx - rk2c), fabs(rk2cx + rk2c)));
					Speednimx = MAX(fabs(rk2cx), MAX(fabs(rk2cx - rk2c), fabs(rk2cx + rk2c)));
					Speednimy = MAX(fabs(rk2cx), MAX(fabs(rk2cx - rk2c), fabs(rk2cx + rk2c)));
					Speednie = MAX(fabs(rk2cx), MAX(fabs(rk2cx - rk2c), fabs(rk2cx + rk2c)));
					rk2cy = vector(rk2my, i, j - 1) / vector(rk2rho, i, j - 1);
					rk2cx = vector(rk2mx, i, j - 1) / vector(rk2rho, i, j - 1);
					rk2c = sqrt(fabs((gamma * vector(P, i, j - 1)) / vector(rk2rho, i, j - 1)));
					Speednjrho = MAX(fabs(rk2cy), MAX(fabs(rk2cy - rk2c), fabs(rk2cy + rk2c)));
					Speednjmx = MAX(fabs(rk2cy), MAX(fabs(rk2cy - rk2c), fabs(rk2cy + rk2c)));
					Speednjmy = MAX(fabs(rk2cy), MAX(fabs(rk2cy - rk2c), fabs(rk2cy + rk2c)));
					Speednje = MAX(fabs(rk2cy), MAX(fabs(rk2cy - rk2c), fabs(rk2cy + rk2c)));
					Fluxlpimx = Fimx_domainI(rho_lpi, mx_lpi, P_lpi, dx, simPlat_dt, ilast, jlast);
					Fluxlpimy = Fimy_domainI(rho_lpi, mx_lpi, my_lpi, dx, simPlat_dt, ilast, jlast);
					Fluxlpie = Fie_domainI(rho_lpi, mx_lpi, e_lpi, P_lpi, dx, simPlat_dt, ilast, jlast);
					Fluxlpjmx = Fjmx_domainI(rho_lpj, mx_lpj, my_lpj, dx, simPlat_dt, ilast, jlast);
					Fluxlpjmy = Fjmy_domainI(rho_lpj, my_lpj, P_lpj, dx, simPlat_dt, ilast, jlast);
					Fluxlpje = Fje_domainI(rho_lpj, my_lpj, e_lpj, P_lpj, dx, simPlat_dt, ilast, jlast);
					Fluxlnimx = Fimx_domainI(rho_lni, mx_lni, P_lni, dx, simPlat_dt, ilast, jlast);
					Fluxlnimy = Fimy_domainI(rho_lni, mx_lni, my_lni, dx, simPlat_dt, ilast, jlast);
					Fluxlnie = Fie_domainI(rho_lni, mx_lni, e_lni, P_lni, dx, simPlat_dt, ilast, jlast);
					Fluxlnjmx = Fjmx_domainI(rho_lnj, mx_lnj, my_lnj, dx, simPlat_dt, ilast, jlast);
					Fluxlnjmy = Fjmy_domainI(rho_lnj, my_lnj, P_lnj, dx, simPlat_dt, ilast, jlast);
					Fluxlnje = Fje_domainI(rho_lnj, my_lnj, e_lnj, P_lnj, dx, simPlat_dt, ilast, jlast);
					Fluxrpimx = Fimx_domainI(rho_rpi, mx_rpi, P_rpi, dx, simPlat_dt, ilast, jlast);
					Fluxrpimy = Fimy_domainI(rho_rpi, mx_rpi, my_rpi, dx, simPlat_dt, ilast, jlast);
					Fluxrpie = Fie_domainI(rho_rpi, mx_rpi, e_rpi, P_rpi, dx, simPlat_dt, ilast, jlast);
					Fluxrpjmx = Fjmx_domainI(rho_rpj, mx_rpj, my_rpj, dx, simPlat_dt, ilast, jlast);
					Fluxrpjmy = Fjmy_domainI(rho_rpj, my_rpj, P_rpj, dx, simPlat_dt, ilast, jlast);
					Fluxrpje = Fje_domainI(rho_rpj, my_rpj, e_rpj, P_rpj, dx, simPlat_dt, ilast, jlast);
					Fluxrnimx = Fimx_domainI(rho_rni, mx_rni, P_rni, dx, simPlat_dt, ilast, jlast);
					Fluxrnimy = Fimy_domainI(rho_rni, mx_rni, my_rni, dx, simPlat_dt, ilast, jlast);
					Fluxrnie = Fie_domainI(rho_rni, mx_rni, e_rni, P_rni, dx, simPlat_dt, ilast, jlast);
					Fluxrnjmx = Fjmx_domainI(rho_rnj, mx_rnj, my_rnj, dx, simPlat_dt, ilast, jlast);
					Fluxrnjmy = Fjmy_domainI(rho_rnj, my_rnj, P_rnj, dx, simPlat_dt, ilast, jlast);
					Fluxrnje = Fje_domainI(rho_rnj, my_rnj, e_rnj, P_rnj, dx, simPlat_dt, ilast, jlast);
					RHS_rho = 0.0;
					RHS_mx = 0.0;
					RHS_my = 0.0;
					RHS_e = 0.0;
					RHS_rho = RHS_rho - LLF_i(mx_lni, mx_rni, mx_lpi, mx_rpi, rho_lni, rho_rni, rho_lpi, rho_rpi, Speedirho, Speednirho, Speedpirho, dx, simPlat_dt, ilast, jlast);
					RHS_rho = RHS_rho - LLF_j(my_lnj, my_rnj, my_lpj, my_rpj, rho_lnj, rho_rnj, rho_lpj, rho_rpj, Speedjrho, Speednjrho, Speedpjrho, dx, simPlat_dt, ilast, jlast);
					RHS_mx = RHS_mx - LLF_i(Fluxlnimx, Fluxrnimx, Fluxlpimx, Fluxrpimx, mx_lni, mx_rni, mx_lpi, mx_rpi, Speedimx, Speednimx, Speedpimx, dx, simPlat_dt, ilast, jlast);
					RHS_mx = RHS_mx - LLF_j(Fluxlnjmx, Fluxrnjmx, Fluxlpjmx, Fluxrpjmx, mx_lnj, mx_rnj, mx_lpj, mx_rpj, Speedjmx, Speednjmx, Speedpjmx, dx, simPlat_dt, ilast, jlast);
					RHS_my = RHS_my - LLF_i(Fluxlnimy, Fluxrnimy, Fluxlpimy, Fluxrpimy, my_lni, my_rni, my_lpi, my_rpi, Speedimy, Speednimy, Speedpimy, dx, simPlat_dt, ilast, jlast);
					RHS_my = RHS_my - LLF_j(Fluxlnjmy, Fluxrnjmy, Fluxlpjmy, Fluxrpjmy, my_lnj, my_rnj, my_lpj, my_rpj, Speedjmy, Speednjmy, Speedpjmy, dx, simPlat_dt, ilast, jlast);
					RHS_e = RHS_e - LLF_i(Fluxlnie, Fluxrnie, Fluxlpie, Fluxrpie, e_lni, e_rni, e_lpi, e_rpi, Speedie, Speednie, Speedpie, dx, simPlat_dt, ilast, jlast);
					RHS_e = RHS_e - LLF_j(Fluxlnje, Fluxrnje, Fluxlpje, Fluxrpje, e_lnj, e_rnj, e_lpj, e_rpj, Speedje, Speednje, Speedpje, dx, simPlat_dt, ilast, jlast);
					vector(rho, i, j) = RK3P3_(RHS_rho, rho_p, rk2rho, i, j, dx, simPlat_dt, ilast, jlast);
					vector(mx, i, j) = RK3P3_(RHS_mx, mx_p, rk2mx, i, j, dx, simPlat_dt, ilast, jlast);
					vector(my, i, j) = RK3P3_(RHS_my, my_p, rk2my, i, j, dx, simPlat_dt, ilast, jlast);
					vector(e, i, j) = RK3P3_(RHS_e, e_p, rk2e, i, j, dx, simPlat_dt, ilast, jlast);
				}
			}
		}
	}
	//Fill ghosts and periodical boundaries
	time_interpolate_operator_mesh1->setStep(3);
	d_bdry_sched_advance14[ln]->fillData(current_time + simPlat_dt, false);
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch >& patch = *p_it;
		double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
	
		double* P = ((pdat::NodeData<double> *) patch->getPatchData(d_P_id).get())->getPointer();
		double* e = ((pdat::NodeData<double> *) patch->getPatchData(d_e_id).get())->getPointer();
		double* rho = ((pdat::NodeData<double> *) patch->getPatchData(d_rho_id).get())->getPointer();
		double* mx = ((pdat::NodeData<double> *) patch->getPatchData(d_mx_id).get())->getPointer();
		double* my = ((pdat::NodeData<double> *) patch->getPatchData(d_my_id).get())->getPointer();
	
		//Get the dimensions of the patch
		const hier::Index boxfirst = patch->getBox().lower();
		const hier::Index boxlast = patch->getBox().upper();
	
		//Get delta spaces into an array. dx, dy, dz.
		std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
		const double* dx  = patch_geom->getDx();
	
		//Auxiliary definitions
		int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
		int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
		for(int j = 0; j < jlast; j++) {
			for(int i = 0; i < ilast; i++) {
				if (((i + 4 < ilast || !patch->getPatchGeometry()->getTouchesRegularBoundary(0, 1)) && (i - 4 >= 0 || !patch->getPatchGeometry()->getTouchesRegularBoundary(0, 0)) && (j + 4 < jlast || !patch->getPatchGeometry()->getTouchesRegularBoundary(1, 1)) && (j - 4 >= 0 || !patch->getPatchGeometry()->getTouchesRegularBoundary(1, 0)))) {
					vector(P, i, j) = (gamma - 1.0) * (vector(e, i, j) - vector(rho, i, j) * (((vector(mx, i, j) / vector(rho, i, j)) * (vector(mx, i, j) / vector(rho, i, j))) / 2.0 + ((vector(my, i, j) / vector(rho, i, j)) * (vector(my, i, j) / vector(rho, i, j))) / 2.0));
				}
			}
		}
	}
	if (d_refinedTimeStepping) {
		if (!hierarchy->finerLevelExists(ln) && last_step) {
			int currentLevelNumber = ln;
			while (currentLevelNumber > 0 && current_iteration[currentLevelNumber] % hierarchy->getRatioToCoarserLevel(currentLevelNumber).max() == 0) {
				d_coarsen_schedule[currentLevelNumber]->coarsenData();
				d_bdry_sched_postCoarsen[currentLevelNumber - 1]->fillData(current_time, false);
				currentLevelNumber--;
			}
		}
	} else {
		if (ln > 0) {
			d_coarsen_schedule[ln]->coarsenData();
			d_bdry_sched_postCoarsen[ln - 1]->fillData(current_time, false);
		}
	}
	

	t_step->stop();


	//Output
	t_output->start();
	if (ln == hierarchy->getFinestLevelNumber() && viz_mesh_dump_interval > 0) {
		if (previous_iteration < next_mesh_dump_iteration && outputCycle >= next_mesh_dump_iteration) {
			d_visit_data_writer->writePlotData(hierarchy, outputCycle, new_time);
			next_mesh_dump_iteration = next_mesh_dump_iteration + viz_mesh_dump_interval;
			while (outputCycle >= next_mesh_dump_iteration) {
				next_mesh_dump_iteration = next_mesh_dump_iteration + viz_mesh_dump_interval;
			}
		}
	}
	//Integration output
	if (ln == hierarchy->getFinestLevelNumber() && d_integration_output_period.size() > 0) {
		int i = 0;
		for (std::vector<std::shared_ptr<IntegrateDataWriter> >::iterator it = d_integrateDataWriters.begin(); it != d_integrateDataWriters.end(); ++it) {
			if (d_integration_output_period[i] > 0 && previous_iteration < next_integration_dump_iteration[i] && outputCycle >= next_integration_dump_iteration[i]) {
				(*it)->writePlotData(hierarchy, outputCycle, new_time);
				next_integration_dump_iteration[i] = next_integration_dump_iteration[i] + d_integration_output_period[i];
				while (outputCycle >= next_integration_dump_iteration[i]) {
					next_integration_dump_iteration[i] = next_integration_dump_iteration[i] + d_integration_output_period[i];
				}
			}
			i++;
		}
	}

	//Point output
	if (ln == hierarchy->getFinestLevelNumber() && d_point_output_period.size() > 0) {
		int i = 0;
		for (std::vector<std::shared_ptr<PointDataWriter> >::iterator it = d_pointDataWriters.begin(); it != d_pointDataWriters.end(); ++it) {
			if (d_point_output_period[i] > 0 && previous_iteration < next_point_dump_iteration[i] && outputCycle >= next_point_dump_iteration[i]) {
				(*it)->writePlotData(hierarchy, outputCycle, new_time);
				next_point_dump_iteration[i] = next_point_dump_iteration[i] + d_point_output_period[i];
				while (outputCycle >= next_point_dump_iteration[i]) {
					next_point_dump_iteration[i] = next_point_dump_iteration[i] + d_point_output_period[i];
				}
			}
			i++;
		}
	}


	t_output->stop();

	if (mpi.getRank() == 0 && d_output_interval > 0 ) {
		if (previous_iteration < next_console_output && outputCycle >= next_console_output) {
			int currentLevelNumber = ln;
			while (currentLevelNumber > 0) {
				currentLevelNumber--;
				cout <<"  ";
			}

			cout << "Level "<<ln<<". Iteration " << current_iteration[ln]<<". Time "<<current_time<<"."<< endl;
			if (ln == hierarchy->getFinestLevelNumber()) {
				next_console_output = next_console_output + d_output_interval;
				while (outputCycle >= next_console_output) {
					next_console_output = next_console_output + d_output_interval;
				}
			
			}
		}
	}

	if (d_timer_output_interval > 0 ) {
		if (previous_iteration < next_timer_output && outputCycle >= next_timer_output) {
			if (ln == hierarchy->getFinestLevelNumber()) {
				//Print timers
				if (mpi.getRank() == 0) {
					tbox::TimerManager::getManager()->print(cout);
				}
				else {
					if (ln == hierarchy->getFinestLevelNumber()) {
						//Dispose other processor timers
						//SAMRAI needs all processors run tbox::TimerManager::getManager()->print, otherwise it hungs
						std::ofstream ofs;
						ofs.setstate(std::ios_base::badbit);
						tbox::TimerManager::getManager()->print(ofs);
					}
				}
				next_timer_output = next_timer_output + d_timer_output_interval;
				while (outputCycle >= next_timer_output) {
					next_timer_output = next_timer_output + d_timer_output_interval;
				}
			
			}
		}
	}

	return simPlat_dt;
}

/*
 * Checks the finalization conditions              
 */
bool Problem::checkFinalization(const double current_time, const double simPlat_dt)
{
	if (greaterEq(current_time, tend)) { 
		return true;
	}
	return false;
	
	

}

void Problem::putToRestart(MainRestartData& mrd) {
	mrd.setNextMeshDumpIteration(next_mesh_dump_iteration);
	mrd.setNextIntegrationDumpIteration(next_integration_dump_iteration);
	mrd.setNextPointDumpIteration(next_point_dump_iteration);

	mrd.setCurrentIteration(current_iteration);
	mrd.setNextConsoleOutputIteration(next_console_output);
	mrd.setNextTimerOutputIteration(next_timer_output);
}

void Problem::getFromRestart(MainRestartData& mrd) {
	next_mesh_dump_iteration = mrd.getNextMeshDumpIteration();
	next_integration_dump_iteration = mrd.getNextIntegrationDumpIteration();
	next_point_dump_iteration = mrd.getNextPointDumpIteration();

	current_iteration = mrd.getCurrentIteration();
	next_console_output = mrd.getNextConsoleOutputIteration();
	next_timer_output = mrd.getNextTimerOutputIteration();
}

void Problem::allocateAfterRestart() {
	for (int il = 0; il < d_patch_hierarchy->getNumberOfLevels(); il++) {
		std::shared_ptr< hier::PatchLevel > level(d_patch_hierarchy->getPatchLevel(il));
		level->allocatePatchData(d_mask_id);
		level->allocatePatchData(d_interior_regridding_value_id);
		level->allocatePatchData(d_nonSync_regridding_tag_id);
		level->allocatePatchData(d_interior_i_id);
		level->allocatePatchData(d_interior_j_id);
		level->allocatePatchData(d_rk1rho_id);
		level->allocatePatchData(d_rk1mx_id);
		level->allocatePatchData(d_rk1my_id);
		level->allocatePatchData(d_rk1e_id);
		level->allocatePatchData(d_rk2rho_id);
		level->allocatePatchData(d_rk2mx_id);
		level->allocatePatchData(d_rk2my_id);
		level->allocatePatchData(d_rk2e_id);
		level->allocatePatchData(d_rho_p_id);
		level->allocatePatchData(d_mx_p_id);
		level->allocatePatchData(d_my_p_id);
		level->allocatePatchData(d_e_p_id);
	}

}

/*
 *  Cell tagging routine - tag cells that require refinement based on a provided condition. 
 */
void Problem::applyGradientDetector(
   const std::shared_ptr< hier::PatchHierarchy >& hierarchy, 
   const int level_number,
   const double time, 
   const int tag_index,
   const bool initial_time,
   const bool uses_richardson_extrapolation_too) 
{
	if (d_regridding && level_number + 1 >= d_regridding_min_level && level_number + 1 <= d_regridding_max_level) {
		hier::VariableDatabase *vdb = hier::VariableDatabase::getDatabase();
		if (!(vdb->checkVariableExists(d_regridding_field))) {
			TBOX_ERROR(d_object_name << ": Regridding field selected not found:n"					<<  d_regridding_field<<  "n");
		}

		std::shared_ptr< hier::PatchLevel > level(hierarchy->getPatchLevel(level_number));

		for (hier::PatchLevel::iterator ip(level->begin()); ip != level->end(); ++ip) {
			const std::shared_ptr< hier::Patch >& patch = *ip;
			int* tags = ((pdat::CellData<int> *) patch->getPatchData(tag_index).get())->getPointer();
			int* regridding_tag = ((pdat::NodeData<int> *) patch->getPatchData(d_nonSync_regridding_tag_id).get())->getPointer();
			const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
			const double* dx  = patch_geom->getDx();
			const hier::Index tfirst = patch->getPatchData(tag_index)->getGhostBox().lower();
			const hier::Index tlast  = patch->getPatchData(tag_index)->getGhostBox().upper();
			const hier::Index boxfirst = patch->getBox().lower();
			const hier::Index boxlast  = patch->getBox().upper();
			int ilast = boxlast(0)-boxfirst(0)+2+2*d_ghost_width;
			int itlast = tlast(0)-tfirst(0)+1;
			int jlast = boxlast(1)-boxfirst(1)+2+2*d_ghost_width;
			int jtlast = tlast(1)-tfirst(1)+1;
			for(int index1 = 0; index1 < (tlast(1)-tfirst(1))+1; index1++) {
				for(int index0 = 0; index0 < (tlast(0)-tfirst(0))+1; index0++) {
					vectorT(tags,index0, index1) = 0;
				}
			}
			for(int index1 = 0; index1 < jlast; index1++) {
				for(int index0 = 0; index0 < ilast; index0++) {
					vector(regridding_tag,index0, index1) = 0;
				}
			}
			if (vdb->checkVariableExists(d_regridding_field)) {
				//Mesh
				if (d_regridding_type == "GRADIENT") {
					int regrid_field_id = vdb->getVariable(d_regridding_field)->getInstanceIdentifier();
					double* regrid_field = ((pdat::NodeData<double> *) patch->getPatchData(regrid_field_id).get())->getPointer();
					double* regridding_value = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_regridding_value_id).get())->getPointer();
					for(int index1 = 0; index1 < jlast - 1; index1++) {
						for(int index0 = 0; index0 < ilast - 1; index0++) {
							if (vector(regrid_field, index0, index1)!=0) {
								if ((fabs(vector(regrid_field, index0+1+d_ghost_width, index1+d_ghost_width)-vector(regrid_field, index0+d_ghost_width, index1+d_ghost_width)) > d_regridding_compressionFactor * MIN(fabs(vector(regrid_field, index0+2+d_ghost_width, index1+d_ghost_width)-vector(regrid_field, index0+1+d_ghost_width, index1+d_ghost_width)) + d_regridding_mOffset * pow(dx[0], 2), fabs(vector(regrid_field, index0+d_ghost_width, index1+d_ghost_width)-vector(regrid_field, index0-1+d_ghost_width, index1+d_ghost_width)) + d_regridding_mOffset * pow(dx[0], 2)) ) ||(fabs(vector(regrid_field, index0+d_ghost_width, index1+1+d_ghost_width)-vector(regrid_field, index0+d_ghost_width, index1+d_ghost_width)) > d_regridding_compressionFactor * MIN(fabs(vector(regrid_field, index0+d_ghost_width, index1+2+d_ghost_width)-vector(regrid_field, index0+d_ghost_width, index1+1+d_ghost_width)) + d_regridding_mOffset * pow(dx[1], 2), fabs(vector(regrid_field, index0+d_ghost_width, index1+d_ghost_width)-vector(regrid_field, index0+d_ghost_width, index1-1+d_ghost_width)) + d_regridding_mOffset * pow(dx[1], 2)) ) ) {
									if (index0 >= d_ghost_width && index0 < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1 >= d_ghost_width && index1 < (tlast(1)-tfirst(1))+1 + d_ghost_width) {
										vectorT(tags,index0 - d_ghost_width, index1 - d_ghost_width) = 1;
									}
									vector(regridding_tag,index0, index1) = 1;
									//SAMRAI tagging
									if (index0 > d_ghost_width && index0 < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1 > d_ghost_width && index1 < (tlast(1)-tfirst(1))+1 + d_ghost_width) {
										vectorT(tags, index0-d_ghost_width - 1, index1-d_ghost_width - 1) = 1;
									}
									if (index0 > d_ghost_width && index0 < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1 >= d_ghost_width && index1 < (tlast(1)-tfirst(1))+1 + d_ghost_width) {
										vectorT(tags, index0-d_ghost_width - 1, index1-d_ghost_width) = 1;
									}
									if (index0 >= d_ghost_width && index0 < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1 > d_ghost_width && index1 < (tlast(1)-tfirst(1))+1 + d_ghost_width) {
										vectorT(tags, index0-d_ghost_width, index1-d_ghost_width - 1) = 1;
									}
									//Informative tagging
									if (index0 > 0 && index1 > 0) {
										if (vector(regridding_tag,index0-1,index1-1) != 1)
											vector(regridding_tag,index0-1,index1-1) = 1;
									}
									if (index0 > 0) {
										if (vector(regridding_tag,index0-1,index1) != 1)
											vector(regridding_tag,index0-1,index1) = 1;
									}
									if (index1 > 0) {
										if (vector(regridding_tag,index0,index1-1) != 1)
											vector(regridding_tag,index0,index1-1) = 1;
									}
									if (d_regridding_buffer > 0) {
										int distance;
										for(int index1b = MAX(0, index1 - d_regridding_buffer - 1); index1b < MIN(index1 + d_regridding_buffer + 1, jlast); index1b++) {
											for(int index0b = MAX(0, index0 - d_regridding_buffer - 1); index0b < MIN(index0 + d_regridding_buffer + 1, ilast); index0b++) {
												int distx = (index0b - index0);
												if (distx < 0) {
													distx++;
												}
												int disty = (index1b - index1);
												if (disty < 0) {
													disty++;
												}
												distance = 1 + MAX(abs(distx), abs(disty));
												if (index0b >= d_ghost_width && index0b < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1b >= d_ghost_width && index1b < (tlast(1)-tfirst(1))+1 + d_ghost_width) {
													vectorT(tags,index0b-d_ghost_width,index1b-d_ghost_width) = 1;
												}
												if (vector(regridding_tag,index0b,index1b) == 0 || vector(regridding_tag,index0b,index1b) > distance) {
													vector(regridding_tag,index0b,index1b) = distance;
												}
											}
										}
									}
			
								}
							}
						}
					}
		
				} else {
					if (d_regridding_type == "FUNCTION") {
						int regrid_field_id = vdb->getVariable(d_regridding_field)->getInstanceIdentifier();
						double* regrid_field = ((pdat::NodeData<double> *) patch->getPatchData(regrid_field_id).get())->getPointer();
						double* regridding_value = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_regridding_value_id).get())->getPointer();
						for(int index1 = 0; index1 < jlast; index1++) {
							for(int index0 = 0; index0 < ilast; index0++) {
								vector(regridding_value, index0, index1) = vector(regrid_field, index0, index1);
								if (vector(regrid_field, index0, index1) > d_regridding_threshold) {
									if (index0 >= d_ghost_width && index0 < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1 >= d_ghost_width && index1 < (tlast(1)-tfirst(1))+1 + d_ghost_width) {
										vectorT(tags,index0 - d_ghost_width, index1 - d_ghost_width) = 1;
									}
									vector(regridding_tag,index0, index1) = 1;
									//SAMRAI tagging
									if (index0 > d_ghost_width && index0 < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1 > d_ghost_width && index1 < (tlast(1)-tfirst(1))+1 + d_ghost_width) {
										vectorT(tags, index0-d_ghost_width - 1, index1-d_ghost_width - 1) = 1;
									}
									if (index0 > d_ghost_width && index0 < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1 >= d_ghost_width && index1 < (tlast(1)-tfirst(1))+1 + d_ghost_width) {
										vectorT(tags, index0-d_ghost_width - 1, index1-d_ghost_width) = 1;
									}
									if (index0 >= d_ghost_width && index0 < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1 > d_ghost_width && index1 < (tlast(1)-tfirst(1))+1 + d_ghost_width) {
										vectorT(tags, index0-d_ghost_width, index1-d_ghost_width - 1) = 1;
									}
									//Informative tagging
									if (index0 > 0 && index1 > 0) {
										if (vector(regridding_tag,index0-1,index1-1) != 1)
											vector(regridding_tag,index0-1,index1-1) = 1;
									}
									if (index0 > 0) {
										if (vector(regridding_tag,index0-1,index1) != 1)
											vector(regridding_tag,index0-1,index1) = 1;
									}
									if (index1 > 0) {
										if (vector(regridding_tag,index0,index1-1) != 1)
											vector(regridding_tag,index0,index1-1) = 1;
									}
									if (d_regridding_buffer > 0) {
										int distance;
										for(int index1b = MAX(0, index1 - d_regridding_buffer - 1); index1b < MIN(index1 + d_regridding_buffer + 1, jlast); index1b++) {
											for(int index0b = MAX(0, index0 - d_regridding_buffer - 1); index0b < MIN(index0 + d_regridding_buffer + 1, ilast); index0b++) {
												int distx = (index0b - index0);
												if (distx < 0) {
													distx++;
												}
												int disty = (index1b - index1);
												if (disty < 0) {
													disty++;
												}
												distance = 1 + MAX(abs(distx), abs(disty));
												if (index0b >= d_ghost_width && index0b < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1b >= d_ghost_width && index1b < (tlast(1)-tfirst(1))+1 + d_ghost_width) {
													vectorT(tags,index0b-d_ghost_width,index1b-d_ghost_width) = 1;
												}
												if (vector(regridding_tag,index0b,index1b) == 0 || vector(regridding_tag,index0b,index1b) > distance) {
													vector(regridding_tag,index0b,index1b) = distance;
												}
											}
										}
									}
								}
							}
						}
			
					} else {
						if (d_regridding_type == "SHADOW") {
							if (!initial_time) {
								if (!(vdb->checkVariableExists(d_regridding_field_shadow))) {
									TBOX_ERROR(d_object_name << ": Regridding field selected not found:" <<  d_regridding_field_shadow<<  "");
								}
								int regrid_field_id = vdb->getVariable(d_regridding_field)->getInstanceIdentifier();
								double* regrid_field1 = ((pdat::NodeData<double> *) patch->getPatchData(regrid_field_id).get())->getPointer();
								int regrid_field_shadow_id = vdb->getVariable(d_regridding_field_shadow)->getInstanceIdentifier();
								double* regrid_field2 = ((pdat::NodeData<double> *) patch->getPatchData(regrid_field_shadow_id).get())->getPointer();
								double* regridding_value = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_regridding_value_id).get())->getPointer();
								for(int index1 = 0; index1 < jlast; index1++) {
									for(int index0 = 0; index0 < ilast; index0++) {
					
										double error = 2 * fabs(vector(regrid_field1, index0, index1) - vector(regrid_field2, index0, index1))/fabs(vector(regrid_field1, index0, index1) + vector(regrid_field2, index0, index1));
										vector(regridding_value, index0, index1) = error;
										if (error > d_regridding_error) {
											if (index0 >= d_ghost_width && index0 < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1 >= d_ghost_width && index1 < (tlast(1)-tfirst(1))+1 + d_ghost_width) {
												vectorT(tags,index0 - d_ghost_width, index1 - d_ghost_width) = 1;
											}
											vector(regridding_tag,index0, index1) = 1;
											//SAMRAI tagging
											if (index0 > d_ghost_width && index0 < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1 > d_ghost_width && index1 < (tlast(1)-tfirst(1))+1 + d_ghost_width) {
												vectorT(tags, index0-d_ghost_width - 1, index1-d_ghost_width - 1) = 1;
											}
											if (index0 > d_ghost_width && index0 < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1 >= d_ghost_width && index1 < (tlast(1)-tfirst(1))+1 + d_ghost_width) {
												vectorT(tags, index0-d_ghost_width - 1, index1-d_ghost_width) = 1;
											}
											if (index0 >= d_ghost_width && index0 < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1 > d_ghost_width && index1 < (tlast(1)-tfirst(1))+1 + d_ghost_width) {
												vectorT(tags, index0-d_ghost_width, index1-d_ghost_width - 1) = 1;
											}
											//Informative tagging
											if (index0 > 0 && index1 > 0) {
												if (vector(regridding_tag,index0-1,index1-1) != 1)
													vector(regridding_tag,index0-1,index1-1) = 1;
											}
											if (index0 > 0) {
												if (vector(regridding_tag,index0-1,index1) != 1)
													vector(regridding_tag,index0-1,index1) = 1;
											}
											if (index1 > 0) {
												if (vector(regridding_tag,index0,index1-1) != 1)
													vector(regridding_tag,index0,index1-1) = 1;
											}
											if (d_regridding_buffer > 0) {
												int distance;
												for(int index1b = MAX(0, index1 - d_regridding_buffer - 1); index1b < MIN(index1 + d_regridding_buffer + 1, jlast); index1b++) {
													for(int index0b = MAX(0, index0 - d_regridding_buffer - 1); index0b < MIN(index0 + d_regridding_buffer + 1, ilast); index0b++) {
														int distx = (index0b - index0);
														if (distx < 0) {
															distx++;
														}
														int disty = (index1b - index1);
														if (disty < 0) {
															disty++;
														}
														distance = 1 + MAX(abs(distx), abs(disty));
														if (index0b >= d_ghost_width && index0b < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1b >= d_ghost_width && index1b < (tlast(1)-tfirst(1))+1 + d_ghost_width) {
															vectorT(tags,index0b-d_ghost_width,index1b-d_ghost_width) = 1;
														}
														if (vector(regridding_tag,index0b,index1b) == 0 || vector(regridding_tag,index0b,index1b) > distance) {
															vector(regridding_tag,index0b,index1b) = distance;
														}
													}
												}
											}
					
										}
									}
								}
					
							}
				
						}
					}
				}
			}
		}
		//Buffer synchronization if needed
		if (d_regridding_buffer > d_ghost_width) {
			d_tagging_fill->createSchedule(level)->fillData(0, false);
		}
		if (d_regridding_buffer > 0) {
			for (hier::PatchLevel::iterator ip(level->begin()); ip != level->end(); ++ip) {
				const std::shared_ptr< hier::Patch >& patch = *ip;
				int* tags = ((pdat::CellData<int> *) patch->getPatchData(tag_index).get())->getPointer();
				int* regridding_tag = ((pdat::NodeData<int> *) patch->getPatchData(d_nonSync_regridding_tag_id).get())->getPointer();
				const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
				const double* dx  = patch_geom->getDx();
				const hier::Index tfirst = patch->getPatchData(tag_index)->getGhostBox().lower();
				const hier::Index tlast  = patch->getPatchData(tag_index)->getGhostBox().upper();
				const hier::Index boxfirst = patch->getBox().lower();
				const hier::Index boxlast  = patch->getBox().upper();
				int ilast = boxlast(0)-boxfirst(0)+2+2*d_ghost_width;
				int itlast = tlast(0)-tfirst(0)+1;
				int jlast = boxlast(1)-boxfirst(1)+2+2*d_ghost_width;
				int jtlast = tlast(1)-tfirst(1)+1;
	
				for(int index1 = 0; index1 < jlast; index1++) {
					for(int index0 = 0; index0 < ilast; index0++) {
	
						int value = vector(regridding_tag, index0, index1);
						if (value > 0 && value < 1 + d_regridding_buffer) {
							int buffer_left = 1 + d_regridding_buffer - value;
							int distance;
							for(int index1b = MAX(0, index1 - buffer_left); index1b < MIN(index1 + buffer_left + 1, jlast); index1b++) {
								for(int index0b = MAX(0, index0 - buffer_left); index0b < MIN(index0 + buffer_left + 1, ilast); index0b++) {
							
									distance = MAX(abs(index0b - index0), abs(index1b - index1));
									if (distance > 0 && index0b >= d_ghost_width && index0b < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1b >= d_ghost_width && index1b < (tlast(1)-tfirst(1))+1 + d_ghost_width) {
										vectorT(tags, index0b - d_ghost_width, index1b - d_ghost_width) = 1;
									}
									if (distance > 0 && (vector(regridding_tag,index0b, index1b) == 0 || vector(regridding_tag, index0b, index1b) > value + distance)) {
										vector(regridding_tag, index0b, index1b) = value + distance;
									}
								}
							}
							
						}
					}
				}
	
			}
		}
	}
}



