#ifndef included_FunctionsXD
#define included_FunctionsXD

#include "SAMRAI/tbox/Utilities.h"
#include <string>
#include <math.h>
#include <vector>
#include "hdf5.h"
#include "Commons.h"


#define MIN(X,Y) ((X) < (Y) ? (X) : (Y))
#define MAX(X,Y) ((X) > (Y) ? (X) : (Y))
#define SIGN(X) (((X) > 0) - ((X) < 0))
#define greaterEq(a,b) ((fabs((a) - (b))/1.0E-15 > 10 ? false : (floor(fabs((a) - (b))/1.0E-15) < 1)) || (b)<(a))
#define lessEq(a,b) ((fabs((a) - (b))/1.0E-15 > 10 ? false: (floor(fabs((a) - (b))/1.0E-15) < 1)) || (a)<(b))
#define equalsEq(a,b) ((fabs((a) - (b))/1.0E-15 > 10 ? false: (floor(fabs((a) - (b))/1.0E-15) < 1)))
#define reducePrecision(x, p) (floor(((x) * pow(10, (p)) + 0.5)) / pow(10, (p)))

using namespace external;

/*
 * Calculates coefficients for quintic lagrangian interpolation.
 *    coefs;      coefficients to obtain
 *    coord:      point in which the interpolation must be calculated
 *    position:   surrounding points for interpolation
 */
inline void calculateCoefficientsMapFile(double* coefs, double coord, double* position) {

    for (int i = 0; i < 6; i++) {
        coefs[i] = 0;
    }
    //Search for a perfect fit

    for (int i = 0; i < 6; i++) {
        if (position[i] == coord) {
            coefs[i] = 1;
            return;
        }
    }

    double x1 = position[0];
    double x2 = position[1];
    double x3 = position[2];
    double x4 = position[3];
    double x5 = position[4];
    double x6 = position[5];

    coefs[0] = (coord-x2)*(coord-x3)*(coord-x4)*(coord-x5)*(coord-x6) / ((x1-x2)*(x1-x3)*(x1-x4)*(x1-x5)*(x1-x6));
    coefs[1] = (coord-x1)*(coord-x3)*(coord-x4)*(coord-x5)*(coord-x6) / ((x2-x1)*(x2-x3)*(x2-x4)*(x2-x5)*(x2-x6));
    coefs[2] = (coord-x1)*(coord-x2)*(coord-x4)*(coord-x5)*(coord-x6) / ((x3-x1)*(x3-x2)*(x3-x4)*(x3-x5)*(x3-x6));
    coefs[3] = (coord-x1)*(coord-x2)*(coord-x3)*(coord-x5)*(coord-x6) / ((x4-x1)*(x4-x2)*(x4-x3)*(x4-x5)*(x4-x6));
    coefs[4] = (coord-x1)*(coord-x2)*(coord-x3)*(coord-x4)*(coord-x6) / ((x5-x1)*(x5-x2)*(x5-x3)*(x5-x4)*(x5-x6));
    coefs[5] = (coord-x1)*(coord-x2)*(coord-x3)*(coord-x4)*(coord-x5) / ((x6-x1)*(x6-x2)*(x6-x3)*(x6-x4)*(x6-x5));
}

/*
 * Reads fields from a 1D table interpolating in the give coordinate.
 * Uses Trilinear interpolation.
 * Table must be equidistant.
 *    nCoords:            number of coordinates of table
 *    nOutFields:         number of fields to read
 *    coord0:             coordinate value to search in the table
 *    data0, ...:         output variables to store interpolated fields
 *    data0Index, ...:    index of fields inside the table
 *    coord0Data:         variable storing all values of table coordinate
 *    coord0Dx:           spacing of table coordinate
 *    coord0Size:         size of table coordinate
 *    varData, ...:       table data
 *    nVars, ...:         number of variables in the table
 */
inline void readFromFileLinear1D(int nCoords, int nOutFields, double coord0, double &data0, int data0Index, double *coord0Data, double coord0Dx, int coord0Size, double *varData, int nVars) {
	// determine spacing parameters of table
	double dx = coord0Dx;
	double dxi = 1. / dx;
	double fh[2];
	double a1, a2;
	// determine location in table
	int idx[2];
	int ix  = 1 + int((coord0 - coord0Data[0] - 1.e-10)/coord0Dx);
	ix = MAX( 1, MIN( ix, coord0Size - 1 ) );
	// set up aux vars for interpolation
	double delx = coord0Data[ix] - coord0;
	idx[0] = data0Index + nVars * (ix);
	idx[1] = data0Index + nVars * (ix - 1);
	// set up aux vars for interpolation
	fh[0] = varData[idx[0]];
	fh[1] = varData[idx[1]];
	// set up coeffs of interpolation polynomical and evaluate function values
	a1 = fh[0];
	a2 = dxi   * ( fh[1] - fh[0] );
	data0 = a1 +  a2 * delx;
}


#define vector(v, i, j, k) (v)[i+ilast*(j+jlast*(k))]

#define POLINT_MACRO_LINEAR_1(y1, y2, i_ext, s_ext, dx, simPlat_dt, ilast, jlast) (-i_ext*y1 + i_ext*y2 + s_ext*y2)
#define POLINT_MACRO_LINEAR_2(y1, y2, i_ext, s_ext, dx, simPlat_dt, ilast, jlast) (-i_ext*y1 + i_ext*y2 - s_ext*y1)
#define POLINT_MACRO_QUADRATIC_1(y1,y2,y3, i_ext, s_ext, dx, simPlat_dt, ilast, jlast) (0.5*(i_ext*i_ext*y1-2*i_ext*i_ext*y2+i_ext*i_ext*y3+i_ext*s_ext*y1-4*i_ext*s_ext*y2+3*i_ext*s_ext*y3+2*s_ext*s_ext*y3)/(s_ext*s_ext))
#define POLINT_MACRO_QUADRATIC_2(y1,y2,y3, i_ext, s_ext, dx, simPlat_dt, ilast, jlast) (0.5*(i_ext*i_ext*y1-2*i_ext*i_ext*y2+i_ext*i_ext*y3+3*i_ext*s_ext*y1-4*i_ext*s_ext*y2+i_ext*s_ext*y3+2*s_ext*s_ext*y1)/(s_ext*s_ext))
#define POLINT_MACRO_CUBIC_1(y1,y2,y3, y4, i_ext, s_ext, dx, simPlat_dt, ilast, jlast) (-(1.0/6.0)*(i_ext*i_ext*i_ext*y1-3*i_ext*i_ext*i_ext*y2+3*i_ext*i_ext*i_ext*y3-i_ext*i_ext*i_ext*y4+3*i_ext*i_ext*s_ext*y1-12*i_ext*i_ext*s_ext*y2+15*i_ext*i_ext*s_ext*y3-6*i_ext*i_ext*s_ext*y4+2*i_ext*s_ext*s_ext*y1-9*i_ext*s_ext*s_ext*y2+18*i_ext*s_ext*s_ext*y3-11*i_ext*s_ext*s_ext*y4-6*s_ext*s_ext*s_ext*y4)/(s_ext*s_ext*s_ext))
#define POLINT_MACRO_CUBIC_2(y1,y2,y3, y4, i_ext, s_ext, dx, simPlat_dt, ilast, jlast) ((1.0/6.0)*(i_ext*i_ext*i_ext*y1-3*i_ext*i_ext*i_ext*y2+3*i_ext*i_ext*i_ext*y3-i_ext*i_ext*i_ext*y4+6*i_ext*i_ext*s_ext*y1-15*i_ext*i_ext*s_ext*y2+12*i_ext*i_ext*s_ext*y3-3*i_ext*i_ext*s_ext*y4+11*i_ext*s_ext*s_ext*y1-18*i_ext*s_ext*s_ext*y2+9*i_ext*s_ext*s_ext*y3-2*i_ext*s_ext*s_ext*y4+6*s_ext*s_ext*s_ext*y1)/(s_ext*s_ext*s_ext))

#define weno(parw0, parw1, parw2, parp0, parp1, parp2, dx, simPlat_dt, ilast, jlast) ((parw0) * (parp0) + (parw1) * (parp1) + (parw2) * (parp2))

#define lor0(paru1, paru2, paru3, dx, simPlat_dt, ilast, jlast) (((2.0 * (paru1) + 5.0 * (paru2)) - 1.0 * (paru3)) / 6.0)

#define lor1(paru1, paru2, paru3, dx, simPlat_dt, ilast, jlast) (((-1.0) * (paru1) + 5.0 * (paru2) + 2.0 * (paru3)) / 6.0)

#define lor2(paru1, paru2, paru3, dx, simPlat_dt, ilast, jlast) ((2.0 * (paru1) - 7.0 * (paru2) + 11.0 * (paru3)) / 6.0)

#define smooth0(paru1, paru2, paru3, dx, simPlat_dt, ilast, jlast) (13.0 / 12.0 * (((paru1) - 2.0 * (paru2) + (paru3)) * ((paru1) - 2.0 * (paru2) + (paru3))) + 1.0 / 4.0 * ((3.0 * (paru1) - 4.0 * (paru2) + (paru3)) * (3.0 * (paru1) - 4.0 * (paru2) + (paru3))))

#define smooth1(paru1, paru2, paru3, dx, simPlat_dt, ilast, jlast) (13.0 / 12.0 * (((paru1) - 2.0 * (paru2) + (paru3)) * ((paru1) - 2.0 * (paru2) + (paru3))) + 1.0 / 4.0 * (((paru1) - (paru3)) * ((paru1) - (paru3))))

#define smooth2(paru1, paru2, paru3, dx, simPlat_dt, ilast, jlast) (13.0 / 12.0 * (((paru1) - 2.0 * (paru2) + (paru3)) * ((paru1) - 2.0 * (paru2) + (paru3))) + 1.0 / 4.0 * (((paru1) - 4.0 * (paru2) + 3.0 * (paru3)) * ((paru1) - 4.0 * (paru2) + 3.0 * (paru3))))

#define nlw0(parbeta, partau, parepsilon, dx, simPlat_dt, ilast, jlast) (3.0 / 10.0 * (1.0 + (partau) / ((parepsilon) + (parbeta))))

#define nlw1(parbeta, partau, parepsilon, dx, simPlat_dt, ilast, jlast) (3.0 / 5.0 * (1.0 + (partau) / ((parepsilon) + (parbeta))))

#define nlw2(parbeta, partau, parepsilon, dx, simPlat_dt, ilast, jlast) (1.0 / 10.0 * (1.0 + (partau) / ((parepsilon) + (parbeta))))

#define quinticInterpolation(parum3, parum2, parum1, parup1, parup2, parup3, dx, simPlat_dt, ilast, jlast) (0.5859375 * ((parum1) + (parup1)) + (-0.09765625 * ((parum2) + (parup2))) + 0.01171875 * ((parum3) + (parup3)))

#define LLFi(parFul, parFur, parul, parur, parspeed, parspeedAlt, dx, simPlat_dt, ilast, jlast) (0.5 * ((parFul) + (parFur)) - 0.5 * MAX((parspeed), (parspeedAlt)) * ((parur) - (parul)))

#define LLFj(parFul, parFur, parul, parur, parspeed, parspeedAlt, dx, simPlat_dt, ilast, jlast) (0.5 * ((parFul) + (parFur)) - 0.5 * MAX((parspeed), (parspeedAlt)) * ((parur) - (parul)))

#define LLFk(parFul, parFur, parul, parur, parspeed, parspeedAlt, dx, simPlat_dt, ilast, jlast) (0.5 * ((parFul) + (parFur)) - 0.5 * MAX((parspeed), (parspeedAlt)) * ((parur) - (parul)))

#define centereddifferences_i(paru, pari, parj, park, dx, simPlat_dt, ilast, jlast) ((((-vector(paru, (pari) + 2, (parj), (park))) + 8.0 * vector(paru, (pari) + 1, (parj), (park))) + ((-8.0 * vector(paru, (pari) - 1, (parj), (park))) + vector(paru, (pari) - 2, (parj), (park)))) / (12.0 * dx[0]))

#define centereddifferences_j(paru, pari, parj, park, dx, simPlat_dt, ilast, jlast) ((((-vector(paru, (pari), (parj) + 2, (park))) + 8.0 * vector(paru, (pari), (parj) + 1, (park))) + ((-8.0 * vector(paru, (pari), (parj) - 1, (park))) + vector(paru, (pari), (parj) - 2, (park)))) / (12.0 * dx[1]))

#define centereddifferences_k(paru, pari, parj, park, dx, simPlat_dt, ilast, jlast) ((((-vector(paru, (pari), (parj), (park) + 2)) + 8.0 * vector(paru, (pari), (parj), (park) + 1)) + ((-8.0 * vector(paru, (pari), (parj), (park) - 1)) + vector(paru, (pari), (parj), (park) - 2))) / (12.0 * dx[2]))

#define lieforward_i(paru, pari, parj, park, dx, simPlat_dt, ilast, jlast) (((-3.0 * vector(paru, (pari) - 1, (parj), (park))) + (-10.0 * vector(paru, (pari), (parj), (park))) + 18.0 * vector(paru, (pari) + 1, (parj), (park)) + (-6.0 * vector(paru, (pari) + 2, (parj), (park))) + vector(paru, (pari) + 3, (parj), (park))) / (dx[0] * 12.0))

#define lieforward_j(paru, pari, parj, park, dx, simPlat_dt, ilast, jlast) (((-3.0 * vector(paru, (pari), (parj) - 1, (park))) + (-10.0 * vector(paru, (pari), (parj), (park))) + 18.0 * vector(paru, (pari), (parj) + 1, (park)) + (-6.0 * vector(paru, (pari), (parj) + 2, (park))) + vector(paru, (pari), (parj) + 3, (park))) / (dx[1] * 12.0))

#define lieforward_k(paru, pari, parj, park, dx, simPlat_dt, ilast, jlast) (((-3.0 * vector(paru, (pari), (parj), (park) - 1)) + (-10.0 * vector(paru, (pari), (parj), (park))) + 18.0 * vector(paru, (pari), (parj), (park) + 1) + (-6.0 * vector(paru, (pari), (parj), (park) + 2)) + vector(paru, (pari), (parj), (park) + 3)) / (dx[2] * 12.0))

#define liebackward_i(paru, pari, parj, park, dx, simPlat_dt, ilast, jlast) ((((-vector(paru, (pari) - 3, (parj), (park))) + 6.0 * vector(paru, (pari) - 2, (parj), (park))) - 18.0 * vector(paru, (pari) - 1, (parj), (park)) + (10.0 * vector(paru, (pari), (parj), (park)) + 3.0 * vector(paru, (pari) + 1, (parj), (park)))) / (dx[0] * 12.0))

#define liebackward_j(paru, pari, parj, park, dx, simPlat_dt, ilast, jlast) ((((-vector(paru, (pari), (parj) - 3, (park))) + 6.0 * vector(paru, (pari), (parj) - 2, (park))) - 18.0 * vector(paru, (pari), (parj) - 1, (park)) + (10.0 * vector(paru, (pari), (parj), (park)) + 3.0 * vector(paru, (pari), (parj) + 1, (park)))) / (dx[1] * 12.0))

#define liebackward_k(paru, pari, parj, park, dx, simPlat_dt, ilast, jlast) ((((-vector(paru, (pari), (parj), (park) - 3)) + 6.0 * vector(paru, (pari), (parj), (park) - 2)) - 18.0 * vector(paru, (pari), (parj), (park) - 1) + (10.0 * vector(paru, (pari), (parj), (park)) + 3.0 * vector(paru, (pari), (parj), (park) + 1))) / (dx[2] * 12.0))

#define centereddifferences4thorder_i(paru, pari, parj, park, dx, simPlat_dt, ilast, jlast) ((((-vector(paru, (pari) + 2, (parj), (park))) + 16.0 * vector(paru, (pari) + 1, (parj), (park))) - 30.0 * vector(paru, (pari), (parj), (park)) + 16.0 * vector(paru, (pari) - 1, (parj), (park)) - vector(paru, (pari) - 2, (parj), (park))) / (12.0 * (dx[0] * dx[0])))

#define centereddifferences4thorder_j(paru, pari, parj, park, dx, simPlat_dt, ilast, jlast) ((((-vector(paru, (pari), (parj) + 2, (park))) + 16.0 * vector(paru, (pari), (parj) + 1, (park))) - 30.0 * vector(paru, (pari), (parj), (park)) + 16.0 * vector(paru, (pari), (parj) - 1, (park)) - vector(paru, (pari), (parj) - 2, (park))) / (12.0 * (dx[1] * dx[1])))

#define centereddifferences4thorder_k(paru, pari, parj, park, dx, simPlat_dt, ilast, jlast) ((((-vector(paru, (pari), (parj), (park) + 2)) + 16.0 * vector(paru, (pari), (parj), (park) + 1)) - 30.0 * vector(paru, (pari), (parj), (park)) + 16.0 * vector(paru, (pari), (parj), (park) - 1) - vector(paru, (pari), (parj), (park) - 2)) / (12.0 * (dx[2] * dx[2])))

#define centereddifferences4thordercrossed_ii(paru, pari, parj, park, dx, simPlat_dt, ilast, jlast) (((vector(paru, (pari) - 2, (parj), (park)) + 8.0 * vector(paru, (pari) + 1, (parj), (park)) + 64.0 * vector(paru, (pari) - 1, (parj), (park)) + 8.0 * vector(paru, (pari) + 2, (parj), (park)) + 8.0 * vector(paru, (pari) - 2, (parj), (park)) + 64.0 * vector(paru, (pari) + 1, (parj), (park)) + 8.0 * vector(paru, (pari) - 1, (parj), (park)) + vector(paru, (pari) + 2, (parj), (park))) - (8.0 * vector(paru, (pari) - 1, (parj), (park)) + vector(paru, (pari) + 2, (parj), (park)) + 8.0 * vector(paru, (pari) - 2, (parj), (park)) + 64.0 * vector(paru, (pari) + 1, (parj), (park)) + 64.0 * vector(paru, (pari) - 1, (parj), (park)) + 8.0 * vector(paru, (pari) + 2, (parj), (park)) + vector(paru, (pari) - 2, (parj), (park)) + 8.0 * vector(paru, (pari) + 1, (parj), (park)))) / (144.0 * dx[0] * dx[0]))

#define centereddifferences4thordercrossed_ij(paru, pari, parj, park, dx, simPlat_dt, ilast, jlast) (((vector(paru, (pari) - 2, (parj) - 2, (park)) + 8.0 * vector(paru, (pari) + 1, (parj) - 2, (park)) + 64.0 * vector(paru, (pari) - 1, (parj) - 1, (park)) + 8.0 * vector(paru, (pari) + 2, (parj) - 1, (park)) + 8.0 * vector(paru, (pari) - 2, (parj) + 1, (park)) + 64.0 * vector(paru, (pari) + 1, (parj) + 1, (park)) + 8.0 * vector(paru, (pari) - 1, (parj) + 2, (park)) + vector(paru, (pari) + 2, (parj) + 2, (park))) - (8.0 * vector(paru, (pari) - 1, (parj) - 2, (park)) + vector(paru, (pari) + 2, (parj) - 2, (park)) + 8.0 * vector(paru, (pari) - 2, (parj) - 1, (park)) + 64.0 * vector(paru, (pari) + 1, (parj) - 1, (park)) + 64.0 * vector(paru, (pari) - 1, (parj) + 1, (park)) + 8.0 * vector(paru, (pari) + 2, (parj) + 1, (park)) + vector(paru, (pari) - 2, (parj) + 2, (park)) + 8.0 * vector(paru, (pari) + 1, (parj) + 2, (park)))) / (144.0 * dx[0] * dx[1]))

#define centereddifferences4thordercrossed_ik(paru, pari, parj, park, dx, simPlat_dt, ilast, jlast) (((vector(paru, (pari) - 2, (parj), (park) - 2) + 8.0 * vector(paru, (pari) + 1, (parj), (park) - 2) + 64.0 * vector(paru, (pari) - 1, (parj), (park) - 1) + 8.0 * vector(paru, (pari) + 2, (parj), (park) - 1) + 8.0 * vector(paru, (pari) - 2, (parj), (park) + 1) + 64.0 * vector(paru, (pari) + 1, (parj), (park) + 1) + 8.0 * vector(paru, (pari) - 1, (parj), (park) + 2) + vector(paru, (pari) + 2, (parj), (park) + 2)) - (8.0 * vector(paru, (pari) - 1, (parj), (park) - 2) + vector(paru, (pari) + 2, (parj), (park) - 2) + 8.0 * vector(paru, (pari) - 2, (parj), (park) - 1) + 64.0 * vector(paru, (pari) + 1, (parj), (park) - 1) + 64.0 * vector(paru, (pari) - 1, (parj), (park) + 1) + 8.0 * vector(paru, (pari) + 2, (parj), (park) + 1) + vector(paru, (pari) - 2, (parj), (park) + 2) + 8.0 * vector(paru, (pari) + 1, (parj), (park) + 2))) / (144.0 * dx[0] * dx[2]))

#define centereddifferences4thordercrossed_ji(paru, pari, parj, park, dx, simPlat_dt, ilast, jlast) (((vector(paru, (pari) - 2, (parj) - 2, (park)) + 8.0 * vector(paru, (pari) - 2, (parj) + 1, (park)) + 64.0 * vector(paru, (pari) - 1, (parj) - 1, (park)) + 8.0 * vector(paru, (pari) - 1, (parj) + 2, (park)) + 8.0 * vector(paru, (pari) + 1, (parj) - 2, (park)) + 64.0 * vector(paru, (pari) + 1, (parj) + 1, (park)) + 8.0 * vector(paru, (pari) + 2, (parj) - 1, (park)) + vector(paru, (pari) + 2, (parj) + 2, (park))) - (8.0 * vector(paru, (pari) - 2, (parj) - 1, (park)) + vector(paru, (pari) - 2, (parj) + 2, (park)) + 8.0 * vector(paru, (pari) - 1, (parj) - 2, (park)) + 64.0 * vector(paru, (pari) - 1, (parj) + 1, (park)) + 64.0 * vector(paru, (pari) + 1, (parj) - 1, (park)) + 8.0 * vector(paru, (pari) + 1, (parj) + 2, (park)) + vector(paru, (pari) + 2, (parj) - 2, (park)) + 8.0 * vector(paru, (pari) + 2, (parj) + 1, (park)))) / (144.0 * dx[1] * dx[0]))

#define centereddifferences4thordercrossed_jj(paru, pari, parj, park, dx, simPlat_dt, ilast, jlast) (((vector(paru, (pari), (parj) - 2, (park)) + 8.0 * vector(paru, (pari), (parj) + 1, (park)) + 64.0 * vector(paru, (pari), (parj) - 1, (park)) + 8.0 * vector(paru, (pari), (parj) + 2, (park)) + 8.0 * vector(paru, (pari), (parj) - 2, (park)) + 64.0 * vector(paru, (pari), (parj) + 1, (park)) + 8.0 * vector(paru, (pari), (parj) - 1, (park)) + vector(paru, (pari), (parj) + 2, (park))) - (8.0 * vector(paru, (pari), (parj) - 1, (park)) + vector(paru, (pari), (parj) + 2, (park)) + 8.0 * vector(paru, (pari), (parj) - 2, (park)) + 64.0 * vector(paru, (pari), (parj) + 1, (park)) + 64.0 * vector(paru, (pari), (parj) - 1, (park)) + 8.0 * vector(paru, (pari), (parj) + 2, (park)) + vector(paru, (pari), (parj) - 2, (park)) + 8.0 * vector(paru, (pari), (parj) + 1, (park)))) / (144.0 * dx[1] * dx[1]))

#define centereddifferences4thordercrossed_jk(paru, pari, parj, park, dx, simPlat_dt, ilast, jlast) (((vector(paru, (pari), (parj) - 2, (park) - 2) + 8.0 * vector(paru, (pari), (parj) + 1, (park) - 2) + 64.0 * vector(paru, (pari), (parj) - 1, (park) - 1) + 8.0 * vector(paru, (pari), (parj) + 2, (park) - 1) + 8.0 * vector(paru, (pari), (parj) - 2, (park) + 1) + 64.0 * vector(paru, (pari), (parj) + 1, (park) + 1) + 8.0 * vector(paru, (pari), (parj) - 1, (park) + 2) + vector(paru, (pari), (parj) + 2, (park) + 2)) - (8.0 * vector(paru, (pari), (parj) - 1, (park) - 2) + vector(paru, (pari), (parj) + 2, (park) - 2) + 8.0 * vector(paru, (pari), (parj) - 2, (park) - 1) + 64.0 * vector(paru, (pari), (parj) + 1, (park) - 1) + 64.0 * vector(paru, (pari), (parj) - 1, (park) + 1) + 8.0 * vector(paru, (pari), (parj) + 2, (park) + 1) + vector(paru, (pari), (parj) - 2, (park) + 2) + 8.0 * vector(paru, (pari), (parj) + 1, (park) + 2))) / (144.0 * dx[1] * dx[2]))

#define centereddifferences4thordercrossed_ki(paru, pari, parj, park, dx, simPlat_dt, ilast, jlast) (((vector(paru, (pari) - 2, (parj), (park) - 2) + 8.0 * vector(paru, (pari) - 2, (parj), (park) + 1) + 64.0 * vector(paru, (pari) - 1, (parj), (park) - 1) + 8.0 * vector(paru, (pari) - 1, (parj), (park) + 2) + 8.0 * vector(paru, (pari) + 1, (parj), (park) - 2) + 64.0 * vector(paru, (pari) + 1, (parj), (park) + 1) + 8.0 * vector(paru, (pari) + 2, (parj), (park) - 1) + vector(paru, (pari) + 2, (parj), (park) + 2)) - (8.0 * vector(paru, (pari) - 2, (parj), (park) - 1) + vector(paru, (pari) - 2, (parj), (park) + 2) + 8.0 * vector(paru, (pari) - 1, (parj), (park) - 2) + 64.0 * vector(paru, (pari) - 1, (parj), (park) + 1) + 64.0 * vector(paru, (pari) + 1, (parj), (park) - 1) + 8.0 * vector(paru, (pari) + 1, (parj), (park) + 2) + vector(paru, (pari) + 2, (parj), (park) - 2) + 8.0 * vector(paru, (pari) + 2, (parj), (park) + 1))) / (144.0 * dx[2] * dx[0]))

#define centereddifferences4thordercrossed_kj(paru, pari, parj, park, dx, simPlat_dt, ilast, jlast) (((vector(paru, (pari), (parj) - 2, (park) - 2) + 8.0 * vector(paru, (pari), (parj) - 2, (park) + 1) + 64.0 * vector(paru, (pari), (parj) - 1, (park) - 1) + 8.0 * vector(paru, (pari), (parj) - 1, (park) + 2) + 8.0 * vector(paru, (pari), (parj) + 1, (park) - 2) + 64.0 * vector(paru, (pari), (parj) + 1, (park) + 1) + 8.0 * vector(paru, (pari), (parj) + 2, (park) - 1) + vector(paru, (pari), (parj) + 2, (park) + 2)) - (8.0 * vector(paru, (pari), (parj) - 2, (park) - 1) + vector(paru, (pari), (parj) - 2, (park) + 2) + 8.0 * vector(paru, (pari), (parj) - 1, (park) - 2) + 64.0 * vector(paru, (pari), (parj) - 1, (park) + 1) + 64.0 * vector(paru, (pari), (parj) + 1, (park) - 1) + 8.0 * vector(paru, (pari), (parj) + 1, (park) + 2) + vector(paru, (pari), (parj) + 2, (park) - 2) + 8.0 * vector(paru, (pari), (parj) + 2, (park) + 1))) / (144.0 * dx[2] * dx[1]))

#define centereddifferences4thordercrossed_kk(paru, pari, parj, park, dx, simPlat_dt, ilast, jlast) (((vector(paru, (pari), (parj), (park) - 2) + 8.0 * vector(paru, (pari), (parj), (park) + 1) + 64.0 * vector(paru, (pari), (parj), (park) - 1) + 8.0 * vector(paru, (pari), (parj), (park) + 2) + 8.0 * vector(paru, (pari), (parj), (park) - 2) + 64.0 * vector(paru, (pari), (parj), (park) + 1) + 8.0 * vector(paru, (pari), (parj), (park) - 1) + vector(paru, (pari), (parj), (park) + 2)) - (8.0 * vector(paru, (pari), (parj), (park) - 1) + vector(paru, (pari), (parj), (park) + 2) + 8.0 * vector(paru, (pari), (parj), (park) - 2) + 64.0 * vector(paru, (pari), (parj), (park) + 1) + 64.0 * vector(paru, (pari), (parj), (park) - 1) + 8.0 * vector(paru, (pari), (parj), (park) + 2) + vector(paru, (pari), (parj), (park) - 2) + 8.0 * vector(paru, (pari), (parj), (park) + 1))) / (144.0 * dx[2] * dx[2]))

#define FVM_i(parfIntPos, parfIntNeg, dx, simPlat_dt, ilast, jlast) (((parfIntPos) - (parfIntNeg)) / dx[0])

#define FVM_j(parfIntPos, parfIntNeg, dx, simPlat_dt, ilast, jlast) (((parfIntPos) - (parfIntNeg)) / dx[1])

#define FVM_k(parfIntPos, parfIntNeg, dx, simPlat_dt, ilast, jlast) (((parfIntPos) - (parfIntNeg)) / dx[2])

#define centereddifferences_1_i(parflux, pari, parj, park, dx, simPlat_dt, ilast, jlast) ((vector(parflux, (pari) + 1, (parj), (park)) - vector(parflux, (pari) - 1, (parj), (park))) / (2.0 * dx[0]))

#define centereddifferences_1_j(parflux, pari, parj, park, dx, simPlat_dt, ilast, jlast) ((vector(parflux, (pari), (parj) + 1, (park)) - vector(parflux, (pari), (parj) - 1, (park))) / (2.0 * dx[1]))

#define centereddifferences_1_k(parflux, pari, parj, park, dx, simPlat_dt, ilast, jlast) ((vector(parflux, (pari), (parj), (park) + 1) - vector(parflux, (pari), (parj), (park) - 1)) / (2.0 * dx[2]))

#define meshDissipation_i(paru, pari, parj, park, dx, simPlat_dt, ilast, jlast) ((vector(paru, (pari) + 3, (parj), (park)) + (-6.0 * vector(paru, (pari) + 2, (parj), (park))) + 15.0 * vector(paru, (pari) + 1, (parj), (park)) + (-20.0 * vector(paru, (pari), (parj), (park))) + 15.0 * vector(paru, (pari) - 1, (parj), (park)) + (-6.0 * vector(paru, (pari) - 2, (parj), (park))) + vector(paru, (pari) - 3, (parj), (park))) / (64.0 * dx[0]))

#define meshDissipation_j(paru, pari, parj, park, dx, simPlat_dt, ilast, jlast) ((vector(paru, (pari), (parj) + 3, (park)) + (-6.0 * vector(paru, (pari), (parj) + 2, (park))) + 15.0 * vector(paru, (pari), (parj) + 1, (park)) + (-20.0 * vector(paru, (pari), (parj), (park))) + 15.0 * vector(paru, (pari), (parj) - 1, (park)) + (-6.0 * vector(paru, (pari), (parj) - 2, (park))) + vector(paru, (pari), (parj) - 3, (park))) / (64.0 * dx[1]))

#define meshDissipation_k(paru, pari, parj, park, dx, simPlat_dt, ilast, jlast) ((vector(paru, (pari), (parj), (park) + 3) + (-6.0 * vector(paru, (pari), (parj), (park) + 2)) + 15.0 * vector(paru, (pari), (parj), (park) + 1) + (-20.0 * vector(paru, (pari), (parj), (park))) + 15.0 * vector(paru, (pari), (parj), (park) - 1) + (-6.0 * vector(paru, (pari), (parj), (park) - 2)) + vector(paru, (pari), (parj), (park) - 3)) / (64.0 * dx[2]))

#define RK4P1_(parRHS, parQn, pari, parj, park, dx, simPlat_dt, ilast, jlast) (vector(parQn, (pari), (parj), (park)) + (simPlat_dt * (parRHS)) / 2.0)

#define RK4P2_(parRHS, parQn, pari, parj, park, dx, simPlat_dt, ilast, jlast) (vector(parQn, (pari), (parj), (park)) + (simPlat_dt * (parRHS)) / 2.0)

#define RK4P3_(parRHS, parQn, pari, parj, park, dx, simPlat_dt, ilast, jlast) (vector(parQn, (pari), (parj), (park)) + simPlat_dt * (parRHS))

#define RK4P4_(parRHS, parQn, parQK1, parQK2, parQK3, pari, parj, park, dx, simPlat_dt, ilast, jlast) ((-vector(parQn, (pari), (parj), (park)) / 3.0) + vector(parQK1, (pari), (parj), (park)) / 3.0 + 2.0 * vector(parQK2, (pari), (parj), (park)) / 3.0 + vector(parQK3, (pari), (parj), (park)) / 3.0 + (simPlat_dt * (parRHS)) / 6.0)

#define FiSfd_x_mainI(parSfd_x, parBetau_x, parAlpha, parSfud_xx, dx, simPlat_dt, ilast, jlast) ((parAlpha) * (parSfud_xx) + (-(parBetau_x) * (parSfd_x)))

#define FjSfd_x_mainI(parSfd_x, parBetau_y, parAlpha, parSfud_yx, dx, simPlat_dt, ilast, jlast) ((parAlpha) * (parSfud_yx) + (-(parBetau_y) * (parSfd_x)))

#define FkSfd_x_mainI(parSfd_x, parBetau_z, parAlpha, parSfud_zx, dx, simPlat_dt, ilast, jlast) ((parAlpha) * (parSfud_zx) + (-(parBetau_z) * (parSfd_x)))

#define FiSfd_y_mainI(parSfd_y, parBetau_x, parAlpha, parSfud_xy, dx, simPlat_dt, ilast, jlast) ((parAlpha) * (parSfud_xy) + (-(parBetau_x) * (parSfd_y)))

#define FjSfd_y_mainI(parSfd_y, parBetau_y, parAlpha, parSfud_yy, dx, simPlat_dt, ilast, jlast) ((parAlpha) * (parSfud_yy) + (-(parBetau_y) * (parSfd_y)))

#define FkSfd_y_mainI(parSfd_y, parBetau_z, parAlpha, parSfud_zy, dx, simPlat_dt, ilast, jlast) ((parAlpha) * (parSfud_zy) + (-(parBetau_z) * (parSfd_y)))

#define FiSfd_z_mainI(parSfd_z, parBetau_x, parAlpha, parSfud_xz, dx, simPlat_dt, ilast, jlast) ((parAlpha) * (parSfud_xz) + (-(parBetau_x) * (parSfd_z)))

#define FjSfd_z_mainI(parSfd_z, parBetau_y, parAlpha, parSfud_yz, dx, simPlat_dt, ilast, jlast) ((parAlpha) * (parSfud_yz) + (-(parBetau_y) * (parSfd_z)))

#define FkSfd_z_mainI(parSfd_z, parBetau_z, parAlpha, parSfud_zz, dx, simPlat_dt, ilast, jlast) ((parAlpha) * (parSfud_zz) + (-(parBetau_z) * (parSfd_z)))

#define FiBfu_x_mainI(parphif, parAlpha, parchi, pargtu_xx, dx, simPlat_dt, ilast, jlast) ((parAlpha) * (parchi) * (pargtu_xx) * (parphif))

#define FjBfu_x_mainI(parBfu_x, parBfu_y, parphif, parBetau_x, parBetau_y, parAlpha, parchi, pargtu_xy, parvfu_x, parvfu_y, dx, simPlat_dt, ilast, jlast) ((parBfu_x) * ((parAlpha) * (parvfu_y) - (parBetau_y)) + (-(parBfu_y) * ((parAlpha) * (parvfu_x) - (parBetau_x))) + (parAlpha) * (parchi) * (pargtu_xy) * (parphif))

#define FkBfu_x_mainI(parBfu_x, parBfu_z, parphif, parBetau_x, parBetau_z, parAlpha, parchi, pargtu_xz, parvfu_x, parvfu_z, dx, simPlat_dt, ilast, jlast) ((parBfu_x) * ((parAlpha) * (parvfu_z) - (parBetau_z)) + (-(parBfu_z) * ((parAlpha) * (parvfu_x) - (parBetau_x))) + (parAlpha) * (parchi) * (pargtu_xz) * (parphif))

#define FiBfu_y_mainI(parBfu_x, parBfu_y, parphif, parBetau_x, parBetau_y, parAlpha, parchi, pargtu_xy, parvfu_x, parvfu_y, dx, simPlat_dt, ilast, jlast) ((parBfu_y) * ((parAlpha) * (parvfu_x) - (parBetau_x)) + (-(parBfu_x) * ((parAlpha) * (parvfu_y) - (parBetau_y))) + (parAlpha) * (parchi) * (pargtu_xy) * (parphif))

#define FjBfu_y_mainI(parphif, parAlpha, parchi, pargtu_yy, dx, simPlat_dt, ilast, jlast) ((parAlpha) * (parchi) * (pargtu_yy) * (parphif))

#define FkBfu_y_mainI(parBfu_y, parBfu_z, parphif, parBetau_y, parBetau_z, parAlpha, parchi, pargtu_yz, parvfu_y, parvfu_z, dx, simPlat_dt, ilast, jlast) ((parBfu_y) * ((parAlpha) * (parvfu_z) - (parBetau_z)) + (-(parBfu_z) * ((parAlpha) * (parvfu_y) - (parBetau_y))) + (parAlpha) * (parchi) * (pargtu_yz) * (parphif))

#define FiBfu_z_mainI(parBfu_x, parBfu_z, parphif, parBetau_x, parBetau_z, parAlpha, parchi, pargtu_xz, parvfu_x, parvfu_z, dx, simPlat_dt, ilast, jlast) ((parBfu_z) * ((parAlpha) * (parvfu_x) - (parBetau_x)) + (-(parBfu_x) * ((parAlpha) * (parvfu_z) - (parBetau_z))) + (parAlpha) * (parchi) * (pargtu_xz) * (parphif))

#define FjBfu_z_mainI(parBfu_y, parBfu_z, parphif, parBetau_y, parBetau_z, parAlpha, parchi, pargtu_yz, parvfu_y, parvfu_z, dx, simPlat_dt, ilast, jlast) ((parBfu_z) * ((parAlpha) * (parvfu_y) - (parBetau_y)) + (-(parBfu_y) * ((parAlpha) * (parvfu_z) - (parBetau_z))) + (parAlpha) * (parchi) * (pargtu_yz) * (parphif))

#define FkBfu_z_mainI(parphif, parAlpha, parchi, pargtu_zz, dx, simPlat_dt, ilast, jlast) ((parAlpha) * (parchi) * (pargtu_zz) * (parphif))

#define FiDf_mainI(parDf, parBetau_x, parAlpha, parvfu_x, dx, simPlat_dt, ilast, jlast) (((parAlpha) * (parvfu_x) - (parBetau_x)) * (parDf))

#define FjDf_mainI(parDf, parBetau_y, parAlpha, parvfu_y, dx, simPlat_dt, ilast, jlast) (((parAlpha) * (parvfu_y) - (parBetau_y)) * (parDf))

#define FkDf_mainI(parDf, parBetau_z, parAlpha, parvfu_z, dx, simPlat_dt, ilast, jlast) (((parAlpha) * (parvfu_z) - (parBetau_z)) * (parDf))

#define Fitauf_mainI(parDf, partauf, parBetau_x, parAlpha, parvfu_x, parSfu_x, dx, simPlat_dt, ilast, jlast) ((-(parBetau_x) * (partauf)) + (parAlpha) * ((-(parDf) * (parvfu_x)) + (parSfu_x)))

#define Fjtauf_mainI(parDf, partauf, parBetau_y, parAlpha, parvfu_y, parSfu_y, dx, simPlat_dt, ilast, jlast) ((-(parBetau_y) * (partauf)) + (parAlpha) * ((-(parDf) * (parvfu_y)) + (parSfu_y)))

#define Fktauf_mainI(parDf, partauf, parBetau_z, parAlpha, parvfu_z, parSfu_z, dx, simPlat_dt, ilast, jlast) ((-(parBetau_z) * (partauf)) + (parAlpha) * ((-(parDf) * (parvfu_z)) + (parSfu_z)))

#define Fiphif_mainI(parBfu_x, parphif, parBetau_x, parAlpha, parch, dx, simPlat_dt, ilast, jlast) ((parAlpha) * ((parch) * (parch)) * (parBfu_x) + (-(parphif) * (parBetau_x)))

#define Fjphif_mainI(parBfu_y, parphif, parBetau_y, parAlpha, parch, dx, simPlat_dt, ilast, jlast) ((parAlpha) * ((parch) * (parch)) * (parBfu_y) + (-(parphif) * (parBetau_y)))

#define Fkphif_mainI(parBfu_z, parphif, parBetau_z, parAlpha, parch, dx, simPlat_dt, ilast, jlast) ((parAlpha) * ((parch) * (parch)) * (parBfu_z) + (-(parphif) * (parBetau_z)))

#define Unit_MCD(i, j, k) ((((int)i) % 10 == 0 && ((int)j) % 10 == 0 && ((int)k) % 10 == 0) ? 10 : (((int)i) % 9 == 0 && ((int)j) % 9 == 0 && ((int)k) % 9 == 0) ? 9 : (((int)i) % 8 == 0 && ((int)j) % 8 == 0 && ((int)k) % 8 == 0) ? 8 : (((int)i) % 7 == 0 && ((int)j) % 7 == 0 && ((int)k) % 7 == 0) ? 7 : (((int)i) % 6 == 0 && ((int)j) % 6 == 0 && ((int)k) % 6 == 0) ? 6 : (((int)i) % 5 == 0 && ((int)j) % 5 == 0 && ((int)k) % 5 == 0) ? 5 : (((int)i) % 4 == 0 && ((int)j) % 4 == 0 && ((int)k) % 4 == 0) ? 4 : (((int)i) % 3 == 0 && ((int)j) % 3 == 0 && ((int)k) % 3 == 0) ? 3 : (((int)i) % 2 == 0 && ((int)j) % 2 == 0 && ((int)k) % 2 == 0) ? 2 : 1)

inline void extrapolate_field(double* pard_i_f, int pari, double* pard_j_f, int parj, double* pard_k_f, int park, double* parto_f, double* parFOV, const double* dx, const int ilast, const int jlast) {
	double sign_i_ext, sign_j_ext, sign_k_ext, disp_i_j, disp_i_k, disp_j_i, disp_j_k, disp_k_i, disp_k_j, y1, y2, y3, to_i_f, to_j_f, to_k_f, mod_d, term_i, term_j, term_k;

	sign_i_ext = SIGN(vector(pard_i_f, pari, parj, park));
	sign_j_ext = SIGN(vector(pard_j_f, pari, parj, park));
	sign_k_ext = SIGN(vector(pard_k_f, pari, parj, park));
	disp_i_j = 0.0;
	disp_i_k = 0.0;
	disp_j_i = 0.0;
	disp_j_k = 0.0;
	disp_k_i = 0.0;
	disp_k_j = 0.0;
	if (!equalsEq(vector(pard_i_f, pari, parj, park), 0.0) && equalsEq(vector(parFOV, int(pari - vector(pard_i_f, pari, parj, park)), parj, park), 0.0)) {
		disp_j_i = vector(pard_j_f, pari, parj, park);
		disp_k_i = vector(pard_k_f, pari, parj, park);
	}
	if (!equalsEq(vector(pard_j_f, pari, parj, park), 0.0) && equalsEq(vector(parFOV, pari, int(parj - vector(pard_j_f, pari, parj, park)), park), 0.0)) {
		disp_i_j = vector(pard_i_f, pari, parj, park);
		disp_k_j = vector(pard_k_f, pari, parj, park);
	}
	if (!equalsEq(vector(pard_k_f, pari, parj, park), 0.0) && equalsEq(vector(parFOV, pari, parj, int(park - vector(pard_k_f, pari, parj, park))), 0.0)) {
		disp_i_k = vector(pard_i_f, pari, parj, park);
		disp_j_k = vector(pard_j_f, pari, parj, park);
	}
	if (vector(pard_i_f, pari, parj, park) > 0.0) {
		y1 = vector(parto_f, int(pari - (vector(pard_i_f, pari, parj, park) + 2 * sign_i_ext)), int(parj - disp_j_i), int(park - disp_k_i));
		y2 = vector(parto_f, int(pari - (vector(pard_i_f, pari, parj, park) + sign_i_ext)), int(parj - disp_j_i), int(park - disp_k_i));
		y3 = vector(parto_f, int(pari - vector(pard_i_f, pari, parj, park)), int(parj - disp_j_i), int(park - disp_k_i));
		to_i_f = POLINT_MACRO_QUADRATIC_1(y1, y2, y3, vector(pard_i_f, pari, parj, park), sign_i_ext, dx, simPlat_dt, ilast, jlast);
	}
	if (vector(pard_i_f, pari, parj, park) < 0.0) {
		y3 = vector(parto_f, int(pari - (vector(pard_i_f, pari, parj, park) + 2 * sign_i_ext)), int(parj - disp_j_i), int(park - disp_k_i));
		y2 = vector(parto_f, int(pari - (vector(pard_i_f, pari, parj, park) + sign_i_ext)), int(parj - disp_j_i), int(park - disp_k_i));
		y1 = vector(parto_f, int(pari - vector(pard_i_f, pari, parj, park)), int(parj - disp_j_i), int(park - disp_k_i));
		to_i_f = POLINT_MACRO_QUADRATIC_2(y1, y2, y3, vector(pard_i_f, pari, parj, park), sign_i_ext, dx, simPlat_dt, ilast, jlast);
	}
	if (vector(pard_j_f, pari, parj, park) > 0.0) {
		y1 = vector(parto_f, int(pari - disp_i_j), int(parj - (vector(pard_j_f, pari, parj, park) + 2 * sign_j_ext)), int(park - disp_k_j));
		y2 = vector(parto_f, int(pari - disp_i_j), int(parj - (vector(pard_j_f, pari, parj, park) + sign_j_ext)), int(park - disp_k_j));
		y3 = vector(parto_f, int(pari - disp_i_j), int(parj - vector(pard_j_f, pari, parj, park)), int(park - disp_k_j));
		to_j_f = POLINT_MACRO_QUADRATIC_1(y1, y2, y3, vector(pard_j_f, pari, parj, park), sign_j_ext, dx, simPlat_dt, ilast, jlast);
	}
	if (vector(pard_j_f, pari, parj, park) < 0.0) {
		y3 = vector(parto_f, int(pari - disp_i_j), int(parj - (vector(pard_j_f, pari, parj, park) + 2 * sign_j_ext)), int(park - disp_k_j));
		y2 = vector(parto_f, int(pari - disp_i_j), int(parj - (vector(pard_j_f, pari, parj, park) + sign_j_ext)), int(park - disp_k_j));
		y1 = vector(parto_f, int(pari - disp_i_j), int(parj - vector(pard_j_f, pari, parj, park)), int(park - disp_k_j));
		to_j_f = POLINT_MACRO_QUADRATIC_2(y1, y2, y3, vector(pard_j_f, pari, parj, park), sign_j_ext, dx, simPlat_dt, ilast, jlast);
	}
	if (vector(pard_k_f, pari, parj, park) > 0.0) {
		y1 = vector(parto_f, int(pari - disp_i_k), int(parj - disp_j_k), int(park - (vector(pard_k_f, pari, parj, park) + 2 * sign_k_ext)));
		y2 = vector(parto_f, int(pari - disp_i_k), int(parj - disp_j_k), int(park - (vector(pard_k_f, pari, parj, park) + sign_k_ext)));
		y3 = vector(parto_f, int(pari - disp_i_k), int(parj - disp_j_k), int(park - vector(pard_k_f, pari, parj, park)));
		to_k_f = POLINT_MACRO_QUADRATIC_1(y1, y2, y3, vector(pard_k_f, pari, parj, park), sign_k_ext, dx, simPlat_dt, ilast, jlast);
	}
	if (vector(pard_k_f, pari, parj, park) < 0.0) {
		y3 = vector(parto_f, int(pari - disp_i_k), int(parj - disp_j_k), int(park - (vector(pard_k_f, pari, parj, park) + 2 * sign_k_ext)));
		y2 = vector(parto_f, int(pari - disp_i_k), int(parj - disp_j_k), int(park - (vector(pard_k_f, pari, parj, park) + sign_k_ext)));
		y1 = vector(parto_f, int(pari - disp_i_k), int(parj - disp_j_k), int(park - vector(pard_k_f, pari, parj, park)));
		to_k_f = POLINT_MACRO_QUADRATIC_2(y1, y2, y3, vector(pard_k_f, pari, parj, park), sign_k_ext, dx, simPlat_dt, ilast, jlast);
	}
	mod_d = sqrt(vector(pard_i_f, pari, parj, park) * vector(pard_i_f, pari, parj, park) + vector(pard_j_f, pari, parj, park) * vector(pard_j_f, pari, parj, park) + vector(pard_k_f, pari, parj, park) * vector(pard_k_f, pari, parj, park));
	term_i = 0.0;
	if (!equalsEq(vector(pard_i_f, pari, parj, park), 0.0)) {
		term_i = (to_i_f - vector(parto_f, int(pari - vector(pard_i_f, pari, parj, park)), int(parj - disp_j_i), int(park - disp_k_i))) / dx[0];
	}
	term_j = 0.0;
	if (!equalsEq(vector(pard_j_f, pari, parj, park), 0.0)) {
		term_j = (to_j_f - vector(parto_f, int(pari - disp_i_j), int(parj - vector(pard_j_f, pari, parj, park)), int(park - disp_k_j))) / dx[1];
	}
	term_k = 0.0;
	if (!equalsEq(vector(pard_k_f, pari, parj, park), 0.0)) {
		term_k = (to_k_f - vector(parto_f, int(pari - disp_i_k), int(parj - disp_j_k), int(park - vector(pard_k_f, pari, parj, park)))) / dx[2];
	}
	vector(parto_f, pari, parj, park) = vector(parto_f, int(pari - vector(pard_i_f, pari, parj, park)), int(parj - vector(pard_j_f, pari, parj, park)), int(park - vector(pard_k_f, pari, parj, park))) + (term_i + term_j + term_k) * sqrt((vector(pard_i_f, pari, parj, park) * dx[0]) * (vector(pard_i_f, pari, parj, park) * dx[0]) + (vector(pard_j_f, pari, parj, park) * dx[1]) * (vector(pard_j_f, pari, parj, park) * dx[1]) + (vector(pard_k_f, pari, parj, park) * dx[2]) * (vector(pard_k_f, pari, parj, park) * dx[2])) / mod_d;

};

void readTable(std::string inputFile, double **coord0, int &coord0Size, double &coord0Dx, double** data, int& nVars);
void readTable(std::string inputFile, double **coord0, int &coord0Size, double &coord0Dx, double** data, int& nVars);
void readTable(std::string inputFile, double **coord0, int &coord0Size, double &coord0Dx, double** data, int& nVars);
void readTable(std::string inputFile, double **coord0, int &coord0Size, double &coord0Dx, double** data, int& nVars);
void readTable(std::string inputFile, double **coord0, int &coord0Size, double &coord0Dx, double** data, int& nVars);
void readTable(std::string inputFile, double **coord0, int &coord0Size, double &coord0Dx, double** data, int& nVars);
void readTable(std::string inputFile, double **coord0, int &coord0Size, double &coord0Dx, double** data, int& nVars);
void readTable(std::string inputFile, double **coord0, int &coord0Size, double &coord0Dx, double** data, int& nVars);




#endif
