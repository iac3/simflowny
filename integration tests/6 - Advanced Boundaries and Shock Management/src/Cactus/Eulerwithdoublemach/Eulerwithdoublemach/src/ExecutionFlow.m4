/*@@
  @file      ExecutionFlow.F
  @date      Fri May 31 14:54:32 CEST 2013
  @author    6
  @desc
             Execution flow for Eulerwithdoublemach
  @enddesc
  @version 6
@@*/

define(cdptii,1.0d0 / (144.0d0 * (deltax ** 2.0d0)) * ((($2) * ((((-25.0d0) * ($7) + 48.0d0 * ($8)) - 36.0d0 * ($6) + 16.0d0 * ($9)) - 3.0d0 * ($10)) - 8.0d0 * ($3) * (((-3.0d0) * ($7) - 10.0d0 * ($8) + 18.0d0 * ($6)) - 6.0d0 * ($9) + ($10)) + 8.0d0 * ($4) * (((-($7)) + 6.0d0 * ($8)) - 18.0d0 * ($6) + 10.0d0 * ($9) + 3.0d0 * ($10))) - ($5) * ((3.0d0 * ($7) - 16.0d0 * ($8) + 36.0d0 * ($6)) - 48.0d0 * ($9) + 25.0d0 * ($10))))dnl
define(cdptij,1.0d0 / (144.0d0 * deltax * deltay) * ((($2) * ((($11) - 8.0d0 * ($12) + 8.0d0 * ($13)) - ($14)) - 8.0d0 * ($3) * ((($15) - 8.0d0 * ($16) + 8.0d0 * ($17)) - ($18)) + 8.0d0 * ($4) * ((($19) - 8.0d0 * ($20) + 8.0d0 * ($21)) - ($22))) - ($5) * ((($23) - 8.0d0 * ($24) + 8.0d0 * ($25)) - ($26))))dnl
define(cdptji,1.0d0 / (144.0d0 * deltay * deltax) * ((($2) * ((($11) - 8.0d0 * ($12) + 8.0d0 * ($13)) - ($14)) - 8.0d0 * ($3) * ((($15) - 8.0d0 * ($16) + 8.0d0 * ($17)) - ($18)) + 8.0d0 * ($4) * ((($19) - 8.0d0 * ($20) + 8.0d0 * ($21)) - ($22))) - ($5) * ((($23) - 8.0d0 * ($24) + 8.0d0 * ($25)) - ($26))))dnl
define(cdptjj,1.0d0 / (144.0d0 * (deltay ** 2.0d0)) * ((($2) * ((((-25.0d0) * ($7) + 48.0d0 * ($8)) - 36.0d0 * ($6) + 16.0d0 * ($9)) - 3.0d0 * ($10)) - 8.0d0 * ($3) * (((-3.0d0) * ($7) - 10.0d0 * ($8) + 18.0d0 * ($6)) - 6.0d0 * ($9) + ($10)) + 8.0d0 * ($4) * (((-($7)) + 6.0d0 * ($8)) - 18.0d0 * ($6) + 10.0d0 * ($9) + 3.0d0 * ($10))) - ($5) * ((3.0d0 * ($7) - 16.0d0 * ($8) + 36.0d0 * ($6)) - 48.0d0 * ($9) + 25.0d0 * ($10))))dnl
define(FDOCi,1.0d0 / (12.0d0 * deltax) * (($3(($5) - 2, ($6)) - 8.0d0 * $3(($5) - 1, ($6)) + 8.0d0 * $3(($5) + 1, ($6))) - $3(($5) + 2, ($6)) + 1.0d0 * (((-$4(($5) - 2, ($6))) * ($2(($5) - 1, ($6)) - $2(($5) - 2, ($6))) + 3.0d0 * $4(($5) - 1, ($6)) * ($2(($5), ($6)) - $2(($5) - 1, ($6)))) - 3.0d0 * $4(($5), ($6)) * ($2(($5) + 1, ($6)) - $2(($5), ($6))) + $4(($5) + 1, ($6)) * ($2(($5) + 2, ($6)) - $2(($5) + 1, ($6))))))dnl
define(FDOCj,1.0d0 / (12.0d0 * deltay) * (($3(($5), ($6) - 2) - 8.0d0 * $3(($5), ($6) - 1) + 8.0d0 * $3(($5), ($6) + 1)) - $3(($5), ($6) + 2) + 1.0d0 * (((-$4(($5), ($6) - 2)) * ($2(($5), ($6) - 1) - $2(($5), ($6) - 2)) + 3.0d0 * $4(($5), ($6) - 1) * ($2(($5), ($6)) - $2(($5), ($6) - 1))) - 3.0d0 * $4(($5), ($6)) * ($2(($5), ($6) + 1) - $2(($5), ($6))) + $4(($5), ($6) + 1) * ($2(($5), ($6) + 2) - $2(($5), ($6) + 1)))))dnl
define(RK3P1,$3(($5), ($6)) + deltat * ($2))dnl
define(RK3P2,1.0d0 / 4.0d0 * (3.0d0 * $3(($5), ($6)) + $4(($5), ($6)) + deltat * ($2)))dnl
define(RK3P3,1.0d0 / 3.0d0 * ($3(($5), ($6)) + 2.0d0 * $4(($5), ($6)) + 2.0d0 * deltat * ($2)))dnl
define(Firho_segmentI,($2))dnl
define(Fjrho_segmentI,($2))dnl
define(Fimx_segmentI,($4) + ($3) * ($3) / ($2))dnl
define(Fjmx_segmentI,($4) * ($3) / ($2))dnl
define(Fimy_segmentI,($3) * ($4) / ($2))dnl
define(Fjmy_segmentI,($4) + ($3) * ($3) / ($2))dnl
define(Fie_segmentI,($3) / ($2) * (($4) + ($5)))dnl
define(Fje_segmentI,($3) / ($2) * (($4) + ($5)))dnl

#include "cctk.h"
#include "cctk_Parameters.h"
#include "cctk_Arguments.h"
#include "cctk_Functions.h"

	subroutine Eulerwithdoublemach_execution(CCTK_ARGUMENTS)

		implicit none

		DECLARE_CCTK_ARGUMENTS
		DECLARE_CCTK_PARAMETERS
		DECLARE_CCTK_FUNCTIONS

!		Declare local variables
!		-----------------

		INTEGER i, iStart, iEnd
		INTEGER j, jStart, jEnd
		INTEGER istat, status, ReflectionCounter
		CCTK_REAL cx_1, cy_1, c_1, fluxAccrho_1, fluxAccmx_1, fluxAccmy_1, fluxAcce_1


		CCTK_INT exitCond
		CCTK_REAL deltax
		CCTK_REAL deltay
		CCTK_REAL deltat

		deltax = 4.0d0/(cctk_gsh(1) - 1)
		deltay = 1.0d0/(cctk_gsh(2) - 1)
		deltat = CCTK_DELTA_TIME

!		Finalization condition
		if (cctk_time .ge. 0.2d0) then 
			call CCTK_Exit(exitCond , cctkGH, 0)
		end if

!		Evolution
		iStart = 1
		iEnd = cctk_lsh(1)
		jStart = 1
		jEnd = cctk_lsh(2)
		do j = jStart, jEnd
			do i = iStart, iEnd
				if ((FOV_1(i, j) .gt. 0 .or. (((i + 1 .le. cctk_lsh(1) .and. FOV_1(i + 1, j) .gt. 0) .or. (i + 2 .le. cctk_lsh(1) .and. FOV_1(i + 2, j) .gt. 0) .or. (j + 1 .le. cctk_lsh(2) .and. FOV_1(i, j + 1) .gt. 0) .or. (j + 2 .le. cctk_lsh(2) .and. FOV_1(i, j + 2) .gt. 0)) .or. ((i - 1 .ge. 1 .and. FOV_1(i - 1, j) .gt. 0) .or. (i - 2 .ge. 1 .and. FOV_1(i - 2, j) .gt. 0) .or. (j - 1 .ge. 1 .and. FOV_1(i, j - 1) .gt. 0) .or. (j - 2 .ge. 1 .and. FOV_1(i, j - 2) .gt. 0))))) then
					Flux_irho_1(i, j) = Firho_segmentI(CCTK_PASS_FTOF, mx_p(i, j))
					Flux_jrho_1(i, j) = Fjrho_segmentI(CCTK_PASS_FTOF, my_p(i, j))
					Flux_imx_1(i, j) = Fimx_segmentI(CCTK_PASS_FTOF, rho_p(i, j), mx_p(i, j), P_p(i, j))
					Flux_jmx_1(i, j) = Fjmx_segmentI(CCTK_PASS_FTOF, rho_p(i, j), mx_p(i, j), my_p(i, j))
					Flux_imy_1(i, j) = Fimy_segmentI(CCTK_PASS_FTOF, rho_p(i, j), mx_p(i, j), my_p(i, j))
					Flux_jmy_1(i, j) = Fjmy_segmentI(CCTK_PASS_FTOF, rho_p(i, j), my_p(i, j), P_p(i, j))
					Flux_ie_1(i, j) = Fie_segmentI(CCTK_PASS_FTOF, rho_p(i, j), mx_p(i, j), e_p(i, j), P_p(i, j))
					Flux_je_1(i, j) = Fje_segmentI(CCTK_PASS_FTOF, rho_p(i, j), my_p(i, j), e_p(i, j), P_p(i, j))
					cx_1 = mx_p(i, j) / rho_p(i, j)
					cy_1 = my_p(i, j) / rho_p(i, j)
					c_1 = SQRT(ABS((gamma * P_p(i, j)) / rho_p(i, j)))
					Speedi_1(i, j) = MAX(ABS(cx_1), ABS(cx_1 - c_1), ABS(cx_1 + c_1))
					Speedj_1(i, j) = MAX(ABS(cy_1), ABS(cy_1 - c_1), ABS(cy_1 + c_1))
				end if
			end do
		end do
		do j = jStart, jEnd
			do i = iStart, iEnd
				if ((FOV_1(i, j) .gt. 0 .or. (((i + 1 .le. cctk_lsh(1) .and. FOV_1(i + 1, j) .gt. 0) .or. (j + 1 .le. cctk_lsh(2) .and. FOV_1(i, j + 1) .gt. 0)) .or. ((i - 1 .ge. 1 .and. FOV_1(i - 1, j) .gt. 0) .or. (j - 1 .ge. 1 .and. FOV_1(i, j - 1) .gt. 0)))) .and. (i + 1 .le. cctk_lsh(1) .and. i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. j - 1 .ge. 1)) then
					Speedi_1(i, j) = MAX(Speedi_1(i, j), Speedi_1(i + 1, j))
					Speedj_1(i, j) = MAX(Speedj_1(i, j), Speedj_1(i, j + 1))
				end if
			end do
		end do
		call CCTK_SyncGroup(status, cctkGH, "Eulerwithdoublemach::block1")
		do j = jStart, jEnd
			do i = iStart, iEnd
				if ((FOV_1(i, j) .gt. 0) .and. (i + 2 .le. cctk_lsh(1) .and. i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. j - 2 .ge. 1)) then
					fluxAccrho_1 = 0.0d0 + 0.0d0
					fluxAccmx_1 = 0.0d0 + 0.0d0
					fluxAccmy_1 = 0.0d0 + 0.0d0
					fluxAcce_1 = 0.0d0 + 0.0d0
					fluxAccrho_1 = fluxAccrho_1 - FDOCi(CCTK_PASS_FTOF, rho_p, Flux_irho_1, Speedi_1, i, j)
					fluxAccrho_1 = fluxAccrho_1 - FDOCj(CCTK_PASS_FTOF, rho_p, Flux_jrho_1, Speedj_1, i, j)
					fluxAccmx_1 = fluxAccmx_1 - FDOCi(CCTK_PASS_FTOF, mx_p, Flux_imx_1, Speedi_1, i, j)
					fluxAccmx_1 = fluxAccmx_1 - FDOCj(CCTK_PASS_FTOF, mx_p, Flux_jmx_1, Speedj_1, i, j)
					fluxAccmy_1 = fluxAccmy_1 - FDOCi(CCTK_PASS_FTOF, my_p, Flux_imy_1, Speedi_1, i, j)
					fluxAccmy_1 = fluxAccmy_1 - FDOCj(CCTK_PASS_FTOF, my_p, Flux_jmy_1, Speedj_1, i, j)
					fluxAcce_1 = fluxAcce_1 - FDOCi(CCTK_PASS_FTOF, e_p, Flux_ie_1, Speedi_1, i, j)
					fluxAcce_1 = fluxAcce_1 - FDOCj(CCTK_PASS_FTOF, e_p, Flux_je_1, Speedj_1, i, j)
					k1rho(i, j) = RK3P1(CCTK_PASS_FTOF, fluxAccrho_1, rho_p, 0, i, j)
					k1mx(i, j) = RK3P1(CCTK_PASS_FTOF, fluxAccmx_1, mx_p, 0, i, j)
					k1my(i, j) = RK3P1(CCTK_PASS_FTOF, fluxAccmy_1, my_p, 0, i, j)
					k1e(i, j) = RK3P1(CCTK_PASS_FTOF, fluxAcce_1, e_p, 0, i, j)
					k1P(i, j) = (gamma - 1.0d0) * (k1e(i, j) - k1rho(i, j) * (((k1mx(i, j) / k1rho(i, j)) ** 2.0d0) / 2.0d0 + ((k1my(i, j) / k1rho(i, j)) ** 2.0d0) / 2.0d0))
				end if
			end do
		end do
		call CCTK_SyncGroup(status, cctkGH, "Eulerwithdoublemach::block2")
		do j = jStart, jEnd
			do i = iStart, iEnd
!				Boundaries
		if ((FOV_xLower(i, j) .gt. 0) .and. ((i + 1 .le. cctk_lsh(1) .and. (FOV_1(int(i + 1), j) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. (FOV_1(int(i + 2), j) .gt. 0)))) then
				k1rho(3 - i, j) = rho_p(3 - i, j)
				k1mx(3 - i, j) = mx_p(3 - i, j)
				k1my(3 - i, j) = my_p(3 - i, j)
				k1e(3 - i, j) = e_p(3 - i, j)
				k1P(3 - i, j) = P_p(3 - i, j)
		end if
		if ((FOV_xUpper(i, j) .gt. 0) .and. ((i - 1 .ge. 1 .and. (FOV_1(int(i - 1), j) .gt. 0)) .or. (i - 2 .ge. 1 .and. (FOV_1(int(i - 2), j) .gt. 0)))) then
			k1rho(i, j) = k1rho(i - 1, j)
			k1mx(i, j) = k1mx(i - 1, j)
			k1my(i, j) = k1my(i - 1, j)
			k1e(i, j) = k1e(i - 1, j)
			k1P(i, j) = k1P(i - 1, j)
		end if
		if ((FOV_yLower(i, j) .gt. 0) .and. ((j + 1 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 1)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 2)) .gt. 0)))) then
				if ((0.0d0 + (i - (1 - cctk_lbnd(1))) * deltax) .lt. (1.0d0 / 6.0d0)) then
					k1rho(i, 3 - j) = rho_p(i, 3 - j)
					k1mx(i, 3 - j) = mx_p(i, 3 - j)
					k1my(i, 3 - j) = my_p(i, 3 - j)
					k1e(i, 3 - j) = e_p(i, 3 - j)
					k1P(i, 3 - j) = P_p(i, 3 - j)
				end if
				ReflectionCounter = 0
				do while (FOV_yLower(i, int(3 - j + int(ReflectionCounter))) .gt. 0.0d0)
					ReflectionCounter = ReflectionCounter + 1
				end do
				if ((0.0d0 + (i - (1 - cctk_lbnd(1))) * deltax) .ge. (1.0d0 / 6.0d0)) then
					k1rho(i, 3 - j) = k1rho(i, 3 - j + 2 * int(ReflectionCounter) - 1)
					k1mx(i, 3 - j) = k1mx(i, 3 - j + 2 * int(ReflectionCounter) - 1)
					k1my(i, 3 - j) = -k1my(i, 3 - j + 2 * int(ReflectionCounter) - 1)
					k1e(i, 3 - j) = k1e(i, 3 - j + 2 * int(ReflectionCounter) - 1)
					k1P(i, 3 - j) = k1P(i, 3 - j + 2 * int(ReflectionCounter) - 1)
				end if
		end if
		if ((FOV_yUpper(i, j) .gt. 0) .and. ((j - 1 .ge. 1 .and. (FOV_1(i, int(j - 1)) .gt. 0)) .or. (j - 2 .ge. 1 .and. (FOV_1(i, int(j - 2)) .gt. 0)))) then
			if ((0.0d0 + (i - (1 - cctk_lbnd(1))) * deltax) .lt. (1.0d0 / 6.0d0 + (1.0d0 + 20.0d0 * cctk_time) / SQRT(3.0d0))) then
				k1rho(i, j) = 8.0d0
				k1mx(i, j) = 33.0d0 * SQRT(3.0d0)
				k1my(i, j) = -33.0d0
				k1e(i, j) = 563.5d0
				k1P(i, j) = (gamma - 1.0d0) * (k1e(i, j) - k1rho(i, j) * (((k1mx(i, j) / k1rho(i, j)) ** 2.0d0) / 2.0d0 + ((k1my(i, j) / k1rho(i, j)) ** 2.0d0) / 2.0d0))
			end if
			if ((0.0d0 + (i - (1 - cctk_lbnd(1))) * deltax) .ge. (1.0d0 / 6.0d0 + (1.0d0 + 20.0d0 * cctk_time) / SQRT(3.0d0))) then
				k1rho(i, j) = 1.4d0
				k1mx(i, j) = 0.0d0
				k1my(i, j) = 0.0d0
				k1e(i, j) = 2.5d0
				k1P(i, j) = (gamma - 1.0d0) * (k1e(i, j) - k1rho(i, j) * (((k1mx(i, j) / k1rho(i, j)) ** 2.0d0) / 2.0d0 + ((k1my(i, j) / k1rho(i, j)) ** 2.0d0) / 2.0d0))
			end if
		end if
			end do
		end do
		call CCTK_SyncGroup(status, cctkGH, "Eulerwithdoublemach::block2")
		do j = jStart, jEnd
			do i = iStart, iEnd
				if ((FOV_1(i, j) .gt. 0 .or. (((i + 1 .le. cctk_lsh(1) .and. FOV_1(i + 1, j) .gt. 0) .or. (i + 2 .le. cctk_lsh(1) .and. FOV_1(i + 2, j) .gt. 0) .or. (j + 1 .le. cctk_lsh(2) .and. FOV_1(i, j + 1) .gt. 0) .or. (j + 2 .le. cctk_lsh(2) .and. FOV_1(i, j + 2) .gt. 0)) .or. ((i - 1 .ge. 1 .and. FOV_1(i - 1, j) .gt. 0) .or. (i - 2 .ge. 1 .and. FOV_1(i - 2, j) .gt. 0) .or. (j - 1 .ge. 1 .and. FOV_1(i, j - 1) .gt. 0) .or. (j - 2 .ge. 1 .and. FOV_1(i, j - 2) .gt. 0))))) then
					Flux_irho_1(i, j) = Firho_segmentI(CCTK_PASS_FTOF, k1mx(i, j))
					Flux_jrho_1(i, j) = Fjrho_segmentI(CCTK_PASS_FTOF, k1my(i, j))
					Flux_imx_1(i, j) = Fimx_segmentI(CCTK_PASS_FTOF, k1rho(i, j), k1mx(i, j), k1P(i, j))
					Flux_jmx_1(i, j) = Fjmx_segmentI(CCTK_PASS_FTOF, k1rho(i, j), k1mx(i, j), k1my(i, j))
					Flux_imy_1(i, j) = Fimy_segmentI(CCTK_PASS_FTOF, k1rho(i, j), k1mx(i, j), k1my(i, j))
					Flux_jmy_1(i, j) = Fjmy_segmentI(CCTK_PASS_FTOF, k1rho(i, j), k1my(i, j), k1P(i, j))
					Flux_ie_1(i, j) = Fie_segmentI(CCTK_PASS_FTOF, k1rho(i, j), k1mx(i, j), k1e(i, j), k1P(i, j))
					Flux_je_1(i, j) = Fje_segmentI(CCTK_PASS_FTOF, k1rho(i, j), k1my(i, j), k1e(i, j), k1P(i, j))
					cx_1 = k1mx(i, j) / k1rho(i, j)
					cy_1 = k1my(i, j) / k1rho(i, j)
					c_1 = SQRT(ABS((gamma * k1P(i, j)) / k1rho(i, j)))
					Speedi_1(i, j) = MAX(ABS(cx_1), ABS(cx_1 - c_1), ABS(cx_1 + c_1))
					Speedj_1(i, j) = MAX(ABS(cy_1), ABS(cy_1 - c_1), ABS(cy_1 + c_1))
				end if
			end do
		end do
		do j = jStart, jEnd
			do i = iStart, iEnd
				if ((FOV_1(i, j) .gt. 0 .or. (((i + 1 .le. cctk_lsh(1) .and. FOV_1(i + 1, j) .gt. 0) .or. (j + 1 .le. cctk_lsh(2) .and. FOV_1(i, j + 1) .gt. 0)) .or. ((i - 1 .ge. 1 .and. FOV_1(i - 1, j) .gt. 0) .or. (j - 1 .ge. 1 .and. FOV_1(i, j - 1) .gt. 0)))) .and. (i + 1 .le. cctk_lsh(1) .and. i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. j - 1 .ge. 1)) then
					Speedi_1(i, j) = MAX(Speedi_1(i, j), Speedi_1(i + 1, j))
					Speedj_1(i, j) = MAX(Speedj_1(i, j), Speedj_1(i, j + 1))
				end if
			end do
		end do
		call CCTK_SyncGroup(status, cctkGH, "Eulerwithdoublemach::block1")
		do j = jStart, jEnd
			do i = iStart, iEnd
				if ((FOV_1(i, j) .gt. 0) .and. (i + 2 .le. cctk_lsh(1) .and. i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. j - 2 .ge. 1)) then
					fluxAccrho_1 = 0.0d0 + 0.0d0
					fluxAccmx_1 = 0.0d0 + 0.0d0
					fluxAccmy_1 = 0.0d0 + 0.0d0
					fluxAcce_1 = 0.0d0 + 0.0d0
					fluxAccrho_1 = fluxAccrho_1 - FDOCi(CCTK_PASS_FTOF, k1rho, Flux_irho_1, Speedi_1, i, j)
					fluxAccrho_1 = fluxAccrho_1 - FDOCj(CCTK_PASS_FTOF, k1rho, Flux_jrho_1, Speedj_1, i, j)
					fluxAccmx_1 = fluxAccmx_1 - FDOCi(CCTK_PASS_FTOF, k1mx, Flux_imx_1, Speedi_1, i, j)
					fluxAccmx_1 = fluxAccmx_1 - FDOCj(CCTK_PASS_FTOF, k1mx, Flux_jmx_1, Speedj_1, i, j)
					fluxAccmy_1 = fluxAccmy_1 - FDOCi(CCTK_PASS_FTOF, k1my, Flux_imy_1, Speedi_1, i, j)
					fluxAccmy_1 = fluxAccmy_1 - FDOCj(CCTK_PASS_FTOF, k1my, Flux_jmy_1, Speedj_1, i, j)
					fluxAcce_1 = fluxAcce_1 - FDOCi(CCTK_PASS_FTOF, k1e, Flux_ie_1, Speedi_1, i, j)
					fluxAcce_1 = fluxAcce_1 - FDOCj(CCTK_PASS_FTOF, k1e, Flux_je_1, Speedj_1, i, j)
					k2rho(i, j) = RK3P2(CCTK_PASS_FTOF, fluxAccrho_1, rho_p, k1rho, i, j)
					k2mx(i, j) = RK3P2(CCTK_PASS_FTOF, fluxAccmx_1, mx_p, k1mx, i, j)
					k2my(i, j) = RK3P2(CCTK_PASS_FTOF, fluxAccmy_1, my_p, k1my, i, j)
					k2e(i, j) = RK3P2(CCTK_PASS_FTOF, fluxAcce_1, e_p, k1e, i, j)
					k2P(i, j) = (gamma - 1.0d0) * (k2e(i, j) - k2rho(i, j) * (((k2mx(i, j) / k2rho(i, j)) ** 2.0d0) / 2.0d0 + ((k2my(i, j) / k2rho(i, j)) ** 2.0d0) / 2.0d0))
				end if
			end do
		end do
		call CCTK_SyncGroup(status, cctkGH, "Eulerwithdoublemach::block6")
		do j = jStart, jEnd
			do i = iStart, iEnd
!				Boundaries
		if ((FOV_xLower(i, j) .gt. 0) .and. ((i + 1 .le. cctk_lsh(1) .and. (FOV_1(int(i + 1), j) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. (FOV_1(int(i + 2), j) .gt. 0)))) then
				k2rho(3 - i, j) = k1rho(3 - i, j)
				k2mx(3 - i, j) = k1mx(3 - i, j)
				k2my(3 - i, j) = k1my(3 - i, j)
				k2e(3 - i, j) = k1e(3 - i, j)
				k2P(3 - i, j) = k1P(3 - i, j)
		end if
		if ((FOV_xUpper(i, j) .gt. 0) .and. ((i - 1 .ge. 1 .and. (FOV_1(int(i - 1), j) .gt. 0)) .or. (i - 2 .ge. 1 .and. (FOV_1(int(i - 2), j) .gt. 0)))) then
			k2rho(i, j) = k2rho(i - 1, j)
			k2mx(i, j) = k2mx(i - 1, j)
			k2my(i, j) = k2my(i - 1, j)
			k2e(i, j) = k2e(i - 1, j)
			k2P(i, j) = k2P(i - 1, j)
		end if
		if ((FOV_yLower(i, j) .gt. 0) .and. ((j + 1 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 1)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 2)) .gt. 0)))) then
				if ((0.0d0 + (i - (1 - cctk_lbnd(1))) * deltax) .lt. (1.0d0 / 6.0d0)) then
					k2rho(i, 3 - j) = k1rho(i, 3 - j)
					k2mx(i, 3 - j) = k1mx(i, 3 - j)
					k2my(i, 3 - j) = k1my(i, 3 - j)
					k2e(i, 3 - j) = k1e(i, 3 - j)
					k2P(i, 3 - j) = k1P(i, 3 - j)
				end if
				ReflectionCounter = 0
				do while (FOV_yLower(i, int(3 - j + int(ReflectionCounter))) .gt. 0.0d0)
					ReflectionCounter = ReflectionCounter + 1
				end do
				if ((0.0d0 + (i - (1 - cctk_lbnd(1))) * deltax) .ge. (1.0d0 / 6.0d0)) then
					k2rho(i, 3 - j) = k2rho(i, 3 - j + 2 * int(ReflectionCounter) - 1)
					k2mx(i, 3 - j) = k2mx(i, 3 - j + 2 * int(ReflectionCounter) - 1)
					k2my(i, 3 - j) = -k2my(i, 3 - j + 2 * int(ReflectionCounter) - 1)
					k2e(i, 3 - j) = k2e(i, 3 - j + 2 * int(ReflectionCounter) - 1)
					k2P(i, 3 - j) = k2P(i, 3 - j + 2 * int(ReflectionCounter) - 1)
				end if
		end if
		if ((FOV_yUpper(i, j) .gt. 0) .and. ((j - 1 .ge. 1 .and. (FOV_1(i, int(j - 1)) .gt. 0)) .or. (j - 2 .ge. 1 .and. (FOV_1(i, int(j - 2)) .gt. 0)))) then
			if ((0.0d0 + (i - (1 - cctk_lbnd(1))) * deltax) .lt. (1.0d0 / 6.0d0 + (1.0d0 + 20.0d0 * cctk_time) / SQRT(3.0d0))) then
				k2rho(i, j) = 8.0d0
				k2mx(i, j) = 33.0d0 * SQRT(3.0d0)
				k2my(i, j) = -33.0d0
				k2e(i, j) = 563.5d0
				k2P(i, j) = (gamma - 1.0d0) * (k2e(i, j) - k2rho(i, j) * (((k2mx(i, j) / k2rho(i, j)) ** 2.0d0) / 2.0d0 + ((k2my(i, j) / k2rho(i, j)) ** 2.0d0) / 2.0d0))
			end if
			if ((0.0d0 + (i - (1 - cctk_lbnd(1))) * deltax) .ge. (1.0d0 / 6.0d0 + (1.0d0 + 20.0d0 * cctk_time) / SQRT(3.0d0))) then
				k2rho(i, j) = 1.4d0
				k2mx(i, j) = 0.0d0
				k2my(i, j) = 0.0d0
				k2e(i, j) = 2.5d0
				k2P(i, j) = (gamma - 1.0d0) * (k2e(i, j) - k2rho(i, j) * (((k2mx(i, j) / k2rho(i, j)) ** 2.0d0) / 2.0d0 + ((k2my(i, j) / k2rho(i, j)) ** 2.0d0) / 2.0d0))
			end if
		end if
			end do
		end do
		call CCTK_SyncGroup(status, cctkGH, "Eulerwithdoublemach::block6")
		do j = jStart, jEnd
			do i = iStart, iEnd
				if ((FOV_1(i, j) .gt. 0 .or. (((i + 1 .le. cctk_lsh(1) .and. FOV_1(i + 1, j) .gt. 0) .or. (i + 2 .le. cctk_lsh(1) .and. FOV_1(i + 2, j) .gt. 0) .or. (j + 1 .le. cctk_lsh(2) .and. FOV_1(i, j + 1) .gt. 0) .or. (j + 2 .le. cctk_lsh(2) .and. FOV_1(i, j + 2) .gt. 0)) .or. ((i - 1 .ge. 1 .and. FOV_1(i - 1, j) .gt. 0) .or. (i - 2 .ge. 1 .and. FOV_1(i - 2, j) .gt. 0) .or. (j - 1 .ge. 1 .and. FOV_1(i, j - 1) .gt. 0) .or. (j - 2 .ge. 1 .and. FOV_1(i, j - 2) .gt. 0))))) then
					Flux_irho_1(i, j) = Firho_segmentI(CCTK_PASS_FTOF, k2mx(i, j))
					Flux_jrho_1(i, j) = Fjrho_segmentI(CCTK_PASS_FTOF, k2my(i, j))
					Flux_imx_1(i, j) = Fimx_segmentI(CCTK_PASS_FTOF, k2rho(i, j), k2mx(i, j), k2P(i, j))
					Flux_jmx_1(i, j) = Fjmx_segmentI(CCTK_PASS_FTOF, k2rho(i, j), k2mx(i, j), k2my(i, j))
					Flux_imy_1(i, j) = Fimy_segmentI(CCTK_PASS_FTOF, k2rho(i, j), k2mx(i, j), k2my(i, j))
					Flux_jmy_1(i, j) = Fjmy_segmentI(CCTK_PASS_FTOF, k2rho(i, j), k2my(i, j), k2P(i, j))
					Flux_ie_1(i, j) = Fie_segmentI(CCTK_PASS_FTOF, k2rho(i, j), k2mx(i, j), k2e(i, j), k2P(i, j))
					Flux_je_1(i, j) = Fje_segmentI(CCTK_PASS_FTOF, k2rho(i, j), k2my(i, j), k2e(i, j), k2P(i, j))
					cx_1 = k2mx(i, j) / k2rho(i, j)
					cy_1 = k2my(i, j) / k2rho(i, j)
					c_1 = SQRT(ABS((gamma * k2P(i, j)) / k2rho(i, j)))
					Speedi_1(i, j) = MAX(ABS(cx_1), ABS(cx_1 - c_1), ABS(cx_1 + c_1))
					Speedj_1(i, j) = MAX(ABS(cy_1), ABS(cy_1 - c_1), ABS(cy_1 + c_1))
				end if
			end do
		end do
		do j = jStart, jEnd
			do i = iStart, iEnd
				if ((FOV_1(i, j) .gt. 0 .or. (((i + 1 .le. cctk_lsh(1) .and. FOV_1(i + 1, j) .gt. 0) .or. (j + 1 .le. cctk_lsh(2) .and. FOV_1(i, j + 1) .gt. 0)) .or. ((i - 1 .ge. 1 .and. FOV_1(i - 1, j) .gt. 0) .or. (j - 1 .ge. 1 .and. FOV_1(i, j - 1) .gt. 0)))) .and. (i + 1 .le. cctk_lsh(1) .and. i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. j - 1 .ge. 1)) then
					Speedi_1(i, j) = MAX(Speedi_1(i, j), Speedi_1(i + 1, j))
					Speedj_1(i, j) = MAX(Speedj_1(i, j), Speedj_1(i, j + 1))
				end if
			end do
		end do
		call CCTK_SyncGroup(status, cctkGH, "Eulerwithdoublemach::block1")
		do j = jStart, jEnd
			do i = iStart, iEnd
				if ((FOV_1(i, j) .gt. 0) .and. (i + 2 .le. cctk_lsh(1) .and. i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. j - 2 .ge. 1)) then
					fluxAccrho_1 = 0.0d0 + 0.0d0
					fluxAccmx_1 = 0.0d0 + 0.0d0
					fluxAccmy_1 = 0.0d0 + 0.0d0
					fluxAcce_1 = 0.0d0 + 0.0d0
					fluxAccrho_1 = fluxAccrho_1 - FDOCi(CCTK_PASS_FTOF, k2rho, Flux_irho_1, Speedi_1, i, j)
					fluxAccrho_1 = fluxAccrho_1 - FDOCj(CCTK_PASS_FTOF, k2rho, Flux_jrho_1, Speedj_1, i, j)
					fluxAccmx_1 = fluxAccmx_1 - FDOCi(CCTK_PASS_FTOF, k2mx, Flux_imx_1, Speedi_1, i, j)
					fluxAccmx_1 = fluxAccmx_1 - FDOCj(CCTK_PASS_FTOF, k2mx, Flux_jmx_1, Speedj_1, i, j)
					fluxAccmy_1 = fluxAccmy_1 - FDOCi(CCTK_PASS_FTOF, k2my, Flux_imy_1, Speedi_1, i, j)
					fluxAccmy_1 = fluxAccmy_1 - FDOCj(CCTK_PASS_FTOF, k2my, Flux_jmy_1, Speedj_1, i, j)
					fluxAcce_1 = fluxAcce_1 - FDOCi(CCTK_PASS_FTOF, k2e, Flux_ie_1, Speedi_1, i, j)
					fluxAcce_1 = fluxAcce_1 - FDOCj(CCTK_PASS_FTOF, k2e, Flux_je_1, Speedj_1, i, j)
					rho(i, j) = RK3P3(CCTK_PASS_FTOF, fluxAccrho_1, rho_p, k2rho, i, j)
					mx(i, j) = RK3P3(CCTK_PASS_FTOF, fluxAccmx_1, mx_p, k2mx, i, j)
					my(i, j) = RK3P3(CCTK_PASS_FTOF, fluxAccmy_1, my_p, k2my, i, j)
					e(i, j) = RK3P3(CCTK_PASS_FTOF, fluxAcce_1, e_p, k2e, i, j)
					P(i, j) = (gamma - 1.0d0) * (e(i, j) - rho(i, j) * (((mx(i, j) / rho(i, j)) ** 2.0d0) / 2.0d0 + ((my(i, j) / rho(i, j)) ** 2.0d0) / 2.0d0))
				end if
			end do
		end do
		call CCTK_SyncGroup(status, cctkGH, "Eulerwithdoublemach::fields_tl2")
		do j = jStart, jEnd
			do i = iStart, iEnd
!				Boundaries
		if ((FOV_xLower(i, j) .gt. 0) .and. ((i + 1 .le. cctk_lsh(1) .and. (FOV_1(int(i + 1), j) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. (FOV_1(int(i + 2), j) .gt. 0)))) then
				rho(3 - i, j) = k2rho(3 - i, j)
				mx(3 - i, j) = k2mx(3 - i, j)
				my(3 - i, j) = k2my(3 - i, j)
				e(3 - i, j) = k2e(3 - i, j)
				P(3 - i, j) = k2P(3 - i, j)
		end if
		if ((FOV_xUpper(i, j) .gt. 0) .and. ((i - 1 .ge. 1 .and. (FOV_1(int(i - 1), j) .gt. 0)) .or. (i - 2 .ge. 1 .and. (FOV_1(int(i - 2), j) .gt. 0)))) then
			rho(i, j) = rho(i - 1, j)
			mx(i, j) = mx(i - 1, j)
			my(i, j) = my(i - 1, j)
			e(i, j) = e(i - 1, j)
			P(i, j) = P(i - 1, j)
		end if
		if ((FOV_yLower(i, j) .gt. 0) .and. ((j + 1 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 1)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 2)) .gt. 0)))) then
				if ((0.0d0 + (i - (1 - cctk_lbnd(1))) * deltax) .lt. (1.0d0 / 6.0d0)) then
					rho(i, 3 - j) = k2rho(i, 3 - j)
					mx(i, 3 - j) = k2mx(i, 3 - j)
					my(i, 3 - j) = k2my(i, 3 - j)
					e(i, 3 - j) = k2e(i, 3 - j)
					P(i, 3 - j) = k2P(i, 3 - j)
				end if
				ReflectionCounter = 0
				do while (FOV_yLower(i, int(3 - j + int(ReflectionCounter))) .gt. 0.0d0)
					ReflectionCounter = ReflectionCounter + 1
				end do
				if ((0.0d0 + (i - (1 - cctk_lbnd(1))) * deltax) .ge. (1.0d0 / 6.0d0)) then
					rho(i, 3 - j) = rho(i, 3 - j + 2 * int(ReflectionCounter) - 1)
					mx(i, 3 - j) = mx(i, 3 - j + 2 * int(ReflectionCounter) - 1)
					my(i, 3 - j) = -my(i, 3 - j + 2 * int(ReflectionCounter) - 1)
					e(i, 3 - j) = e(i, 3 - j + 2 * int(ReflectionCounter) - 1)
					P(i, 3 - j) = P(i, 3 - j + 2 * int(ReflectionCounter) - 1)
				end if
		end if
		if ((FOV_yUpper(i, j) .gt. 0) .and. ((j - 1 .ge. 1 .and. (FOV_1(i, int(j - 1)) .gt. 0)) .or. (j - 2 .ge. 1 .and. (FOV_1(i, int(j - 2)) .gt. 0)))) then
			if ((0.0d0 + (i - (1 - cctk_lbnd(1))) * deltax) .lt. (1.0d0 / 6.0d0 + (1.0d0 + 20.0d0 * cctk_time) / SQRT(3.0d0))) then
				rho(i, j) = 8.0d0
				mx(i, j) = 33.0d0 * SQRT(3.0d0)
				my(i, j) = -33.0d0
				e(i, j) = 563.5d0
				P(i, j) = (gamma - 1.0d0) * (e(i, j) - rho(i, j) * (((mx(i, j) / rho(i, j)) ** 2.0d0) / 2.0d0 + ((my(i, j) / rho(i, j)) ** 2.0d0) / 2.0d0))
			end if
			if ((0.0d0 + (i - (1 - cctk_lbnd(1))) * deltax) .ge. (1.0d0 / 6.0d0 + (1.0d0 + 20.0d0 * cctk_time) / SQRT(3.0d0))) then
				rho(i, j) = 1.4d0
				mx(i, j) = 0.0d0
				my(i, j) = 0.0d0
				e(i, j) = 2.5d0
				P(i, j) = (gamma - 1.0d0) * (e(i, j) - rho(i, j) * (((mx(i, j) / rho(i, j)) ** 2.0d0) / 2.0d0 + ((my(i, j) / rho(i, j)) ** 2.0d0) / 2.0d0))
			end if
		end if
			end do
		end do
		call CCTK_SyncGroup(status, cctkGH, "Eulerwithdoublemach::fields_tl2")

	end subroutine Eulerwithdoublemach_execution