/*@@
  @file      ExecutionFlow.F
  @date      Fri May 31 12:51:43 CEST 2013
  @author    1
  @desc
             Execution flow for AdvectionwithSinus1D
  @enddesc
  @version 1
@@*/

define(CD4i,(1.333333333d0 * $2(($3) + 1) - 1.333333333d0 * $2(($3) - 1) + $2(($3) - 2) * 0.166666667d0 - $2(($3) + 2) * 0.166666667d0) * 0.5d0 / deltax)dnl
define(Euler,($2) + deltat * ($3))dnl
define(cdptii,1.0d0 / (144.0d0 * (deltax ** 2.0d0)) * ((($2) * ((((-25.0d0) * ($7) + 48.0d0 * ($8)) - 36.0d0 * ($6) + 16.0d0 * ($9)) - 3.0d0 * ($10)) - 8.0d0 * ($3) * (((-3.0d0) * ($7) - 10.0d0 * ($8) + 18.0d0 * ($6)) - 6.0d0 * ($9) + ($10)) + 8.0d0 * ($4) * (((-($7)) + 6.0d0 * ($8)) - 18.0d0 * ($6) + 10.0d0 * ($9) + 3.0d0 * ($10))) - ($5) * ((3.0d0 * ($7) - 16.0d0 * ($8) + 36.0d0 * ($6)) - 48.0d0 * ($9) + 25.0d0 * ($10))))dnl
define(FiphiCap_segmentI,($2))dnl

#include "cctk.h"
#include "cctk_Parameters.h"
#include "cctk_Arguments.h"
#include "cctk_Functions.h"

	subroutine AdvectionwithSinus1D_execution(CCTK_ARGUMENTS)

		implicit none

		DECLARE_CCTK_ARGUMENTS
		DECLARE_CCTK_PARAMETERS
		DECLARE_CCTK_FUNCTIONS

!		Declare local variables
!		-----------------

		INTEGER i, iStart, iEnd
		INTEGER istat, status, ReflectionCounter
		CCTK_REAL fluxAccphiCap_1


		CCTK_INT exitCond
		CCTK_REAL deltax
		CCTK_REAL deltat

		deltax = (2.0d0 * 3.1415926535897932384626433832795028841971693993751d+0 - (0.0d0))/(cctk_gsh(1) - 4)
		deltat = CCTK_DELTA_TIME

!		Finalization condition
		if (cctk_time .ge. tend) then 
			call CCTK_Exit(exitCond , cctkGH, 0)
		end if

!		Evolution
		iStart = 1
		iEnd = cctk_lsh(1)
		do i = iStart, iEnd
			if ((FOV_1(i) .gt. 0 .or. (((i + 1 .le. cctk_lsh(1) .and. FOV_1(i + 1) .gt. 0) .or. (i + 2 .le. cctk_lsh(1) .and. FOV_1(i + 2) .gt. 0)) .or. ((i - 1 .ge. 1 .and. FOV_1(i - 1) .gt. 0) .or. (i - 2 .ge. 1 .and. FOV_1(i - 2) .gt. 0))))) then
				FluxiphiCap_1(i) = FiphiCap_segmentI(CCTK_PASS_FTOF, phiCap_p(i))
			end if
		end do
		do i = iStart, iEnd
			if ((FOV_1(i) .gt. 0) .and. (i + 2 .le. cctk_lsh(1) .and. i - 2 .ge. 1)) then
				fluxAccphiCap_1 = 0.0d0 + 0.0d0
				fluxAccphiCap_1 = fluxAccphiCap_1 - CD4i(CCTK_PASS_FTOF, FluxiphiCap_1, i)
				phiCap(i) = Euler(CCTK_PASS_FTOF, phiCap_p(i), fluxAccphiCap_1)
			end if
		end do
		call CCTK_SyncGroup(status, cctkGH, "AdvectionwithSinus1D::fields_tl2")

	end subroutine AdvectionwithSinus1D_execution