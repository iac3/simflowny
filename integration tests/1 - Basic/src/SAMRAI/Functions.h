#ifndef included_FunctionsXD
#define included_FunctionsXD

#include "SAMRAI/tbox/Utilities.h"
#include <string>
#include <math.h>
#include <vector>
#include "hdf5.h"
#include "Commons.h"


#define MIN(X,Y) ((X) < (Y) ? (X) : (Y))
#define MAX(X,Y) ((X) > (Y) ? (X) : (Y))
#define SIGN(X) (((X) > 0) - ((X) < 0))
#define greaterEq(a,b) ((fabs((a) - (b))/1.0E-15 > 10 ? false : (floor(fabs((a) - (b))/1.0E-15) < 1)) || (b)<(a))
#define lessEq(a,b) ((fabs((a) - (b))/1.0E-15 > 10 ? false: (floor(fabs((a) - (b))/1.0E-15) < 1)) || (a)<(b))
#define equalsEq(a,b) ((fabs((a) - (b))/1.0E-15 > 10 ? false: (floor(fabs((a) - (b))/1.0E-15) < 1)))
#define reducePrecision(x, p) (floor(((x) * pow(10, (p)) + 0.5)) / pow(10, (p)))

using namespace external;

/*
 * Calculates coefficients for quintic lagrangian interpolation.
 *    coefs;      coefficients to obtain
 *    coord:      point in which the interpolation must be calculated
 *    position:   surrounding points for interpolation
 */
inline void calculateCoefficientsMapFile(double* coefs, double coord, double* position) {

    for (int i = 0; i < 6; i++) {
        coefs[i] = 0;
    }
    //Search for a perfect fit

    for (int i = 0; i < 6; i++) {
        if (position[i] == coord) {
            coefs[i] = 1;
            return;
        }
    }

    double x1 = position[0];
    double x2 = position[1];
    double x3 = position[2];
    double x4 = position[3];
    double x5 = position[4];
    double x6 = position[5];

    coefs[0] = (coord-x2)*(coord-x3)*(coord-x4)*(coord-x5)*(coord-x6) / ((x1-x2)*(x1-x3)*(x1-x4)*(x1-x5)*(x1-x6));
    coefs[1] = (coord-x1)*(coord-x3)*(coord-x4)*(coord-x5)*(coord-x6) / ((x2-x1)*(x2-x3)*(x2-x4)*(x2-x5)*(x2-x6));
    coefs[2] = (coord-x1)*(coord-x2)*(coord-x4)*(coord-x5)*(coord-x6) / ((x3-x1)*(x3-x2)*(x3-x4)*(x3-x5)*(x3-x6));
    coefs[3] = (coord-x1)*(coord-x2)*(coord-x3)*(coord-x5)*(coord-x6) / ((x4-x1)*(x4-x2)*(x4-x3)*(x4-x5)*(x4-x6));
    coefs[4] = (coord-x1)*(coord-x2)*(coord-x3)*(coord-x4)*(coord-x6) / ((x5-x1)*(x5-x2)*(x5-x3)*(x5-x4)*(x5-x6));
    coefs[5] = (coord-x1)*(coord-x2)*(coord-x3)*(coord-x4)*(coord-x5) / ((x6-x1)*(x6-x2)*(x6-x3)*(x6-x4)*(x6-x5));
}



#define vector(v, i, j) (v)[i+ilast*(j)]

#define CD4_i(parflux, pari, parj, dx, simPlat_dt, ilast, jlast) ((1.333333333 * vector(parflux, (pari) + 1, (parj)) - 1.333333333 * vector(parflux, (pari) - 1, (parj)) + vector(parflux, (pari) - 2, (parj)) * 0.166666667 - vector(parflux, (pari) + 2, (parj)) * 0.166666667) * 0.5 / dx[0])

#define CD4_j(parflux, pari, parj, dx, simPlat_dt, ilast, jlast) ((1.333333333 * vector(parflux, (pari), (parj) + 1) - 1.333333333 * vector(parflux, (pari), (parj) - 1) + vector(parflux, (pari), (parj) - 2) * 0.166666667 - vector(parflux, (pari), (parj) + 2) * 0.166666667) * 0.5 / dx[1])

#define Euler_(paru, parsources, dx, simPlat_dt, ilast, jlast) ((paru) + simPlat_dt * (parsources))

#define FiPhi_segmentI(parPhi, dx, simPlat_dt, ilast, jlast) ((parPhi))





#endif
