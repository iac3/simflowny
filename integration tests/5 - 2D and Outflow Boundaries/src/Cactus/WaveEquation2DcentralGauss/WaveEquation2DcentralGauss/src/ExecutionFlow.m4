/*@@
  @file      ExecutionFlow.F
  @date      Fri May 31 14:22:25 CEST 2013
  @author    5
  @desc
             Execution flow for WaveEquation2DcentralGauss
  @enddesc
  @version 5
@@*/

define(cdptii,1.0d0 / (144.0d0 * (deltax ** 2.0d0)) * ((($2) * ((((-25.0d0) * ($7) + 48.0d0 * ($8)) - 36.0d0 * ($6) + 16.0d0 * ($9)) - 3.0d0 * ($10)) - 8.0d0 * ($3) * (((-3.0d0) * ($7) - 10.0d0 * ($8) + 18.0d0 * ($6)) - 6.0d0 * ($9) + ($10)) + 8.0d0 * ($4) * (((-($7)) + 6.0d0 * ($8)) - 18.0d0 * ($6) + 10.0d0 * ($9) + 3.0d0 * ($10))) - ($5) * ((3.0d0 * ($7) - 16.0d0 * ($8) + 36.0d0 * ($6)) - 48.0d0 * ($9) + 25.0d0 * ($10))))dnl
define(cdptij,1.0d0 / (144.0d0 * deltax * deltay) * ((($2) * ((($11) - 8.0d0 * ($12) + 8.0d0 * ($13)) - ($14)) - 8.0d0 * ($3) * ((($15) - 8.0d0 * ($16) + 8.0d0 * ($17)) - ($18)) + 8.0d0 * ($4) * ((($19) - 8.0d0 * ($20) + 8.0d0 * ($21)) - ($22))) - ($5) * ((($23) - 8.0d0 * ($24) + 8.0d0 * ($25)) - ($26))))dnl
define(cdptji,1.0d0 / (144.0d0 * deltay * deltax) * ((($2) * ((($11) - 8.0d0 * ($12) + 8.0d0 * ($13)) - ($14)) - 8.0d0 * ($3) * ((($15) - 8.0d0 * ($16) + 8.0d0 * ($17)) - ($18)) + 8.0d0 * ($4) * ((($19) - 8.0d0 * ($20) + 8.0d0 * ($21)) - ($22))) - ($5) * ((($23) - 8.0d0 * ($24) + 8.0d0 * ($25)) - ($26))))dnl
define(cdptjj,1.0d0 / (144.0d0 * (deltay ** 2.0d0)) * ((($2) * ((((-25.0d0) * ($7) + 48.0d0 * ($8)) - 36.0d0 * ($6) + 16.0d0 * ($9)) - 3.0d0 * ($10)) - 8.0d0 * ($3) * (((-3.0d0) * ($7) - 10.0d0 * ($8) + 18.0d0 * ($6)) - 6.0d0 * ($9) + ($10)) + 8.0d0 * ($4) * (((-($7)) + 6.0d0 * ($8)) - 18.0d0 * ($6) + 10.0d0 * ($9) + 3.0d0 * ($10))) - ($5) * ((3.0d0 * ($7) - 16.0d0 * ($8) + 36.0d0 * ($6)) - 48.0d0 * ($9) + 25.0d0 * ($10))))dnl
define(FDOCi,1.0d0 / (12.0d0 * deltax) * (($3(($5) - 2, ($6)) - 8.0d0 * $3(($5) - 1, ($6)) + 8.0d0 * $3(($5) + 1, ($6))) - $3(($5) + 2, ($6)) + 1.0d0 * (((-$4(($5) - 2, ($6))) * ($2(($5) - 1, ($6)) - $2(($5) - 2, ($6))) + 3.0d0 * $4(($5) - 1, ($6)) * ($2(($5), ($6)) - $2(($5) - 1, ($6)))) - 3.0d0 * $4(($5), ($6)) * ($2(($5) + 1, ($6)) - $2(($5), ($6))) + $4(($5) + 1, ($6)) * ($2(($5) + 2, ($6)) - $2(($5) + 1, ($6))))))dnl
define(FDOCj,1.0d0 / (12.0d0 * deltay) * (($3(($5), ($6) - 2) - 8.0d0 * $3(($5), ($6) - 1) + 8.0d0 * $3(($5), ($6) + 1)) - $3(($5), ($6) + 2) + 1.0d0 * (((-$4(($5), ($6) - 2)) * ($2(($5), ($6) - 1) - $2(($5), ($6) - 2)) + 3.0d0 * $4(($5), ($6) - 1) * ($2(($5), ($6)) - $2(($5), ($6) - 1))) - 3.0d0 * $4(($5), ($6)) * ($2(($5), ($6) + 1) - $2(($5), ($6))) + $4(($5), ($6) + 1) * ($2(($5), ($6) + 2) - $2(($5), ($6) + 1)))))dnl
define(RK3P1,$3(($5), ($6)) + deltat * ($2))dnl
define(RK3P2,1.0d0 / 4.0d0 * (3.0d0 * $3(($5), ($6)) + $4(($5), ($6)) + deltat * ($2)))dnl
define(RK3P3,1.0d0 / 3.0d0 * ($3(($5), ($6)) + 2.0d0 * $4(($5), ($6)) + 2.0d0 * deltat * ($2)))dnl
define(FiomegaCap_unoI,-($2))dnl
define(FjomegaCap_unoI,-($2))dnl
define(Fiphix_unoI,-($2))dnl
define(Fjphiy_unoI,-($2))dnl

#include "cctk.h"
#include "cctk_Parameters.h"
#include "cctk_Arguments.h"
#include "cctk_Functions.h"

	subroutine WaveEquation2DcentralGauss_execution(CCTK_ARGUMENTS)

		implicit none

		DECLARE_CCTK_ARGUMENTS
		DECLARE_CCTK_PARAMETERS
		DECLARE_CCTK_FUNCTIONS

!		Declare local variables
!		-----------------

		INTEGER i, iStart, iEnd
		INTEGER j, jStart, jEnd
		INTEGER istat, status, ReflectionCounter
		CCTK_REAL fluxAccomegaCap_1, fluxAccphix_1, fluxAccphiy_1


		CCTK_INT exitCond
		CCTK_REAL deltax
		CCTK_REAL deltay
		CCTK_REAL deltat

		deltax = 2.0d0/(cctk_gsh(1) - 1)
		deltay = 2.0d0/(cctk_gsh(2) - 1)
		deltat = CCTK_DELTA_TIME

!		Finalization condition
		if (cctk_time .ge. 2.0d0) then 
			call CCTK_Exit(exitCond , cctkGH, 0)
		end if

!		Evolution
		iStart = 1
		iEnd = cctk_lsh(1)
		jStart = 1
		jEnd = cctk_lsh(2)
		do j = jStart, jEnd
			do i = iStart, iEnd
				if ((FOV_1(i, j) .gt. 0 .or. (((i + 1 .le. cctk_lsh(1) .and. FOV_1(i + 1, j) .gt. 0) .or. (i + 2 .le. cctk_lsh(1) .and. FOV_1(i + 2, j) .gt. 0) .or. (j + 1 .le. cctk_lsh(2) .and. FOV_1(i, j + 1) .gt. 0) .or. (j + 2 .le. cctk_lsh(2) .and. FOV_1(i, j + 2) .gt. 0)) .or. ((i - 1 .ge. 1 .and. FOV_1(i - 1, j) .gt. 0) .or. (i - 2 .ge. 1 .and. FOV_1(i - 2, j) .gt. 0) .or. (j - 1 .ge. 1 .and. FOV_1(i, j - 1) .gt. 0) .or. (j - 2 .ge. 1 .and. FOV_1(i, j - 2) .gt. 0))))) then
					Flux_iomegaCap_1(i, j) = FiomegaCap_unoI(CCTK_PASS_FTOF, phix_p(i, j))
					Flux_jomegaCap_1(i, j) = FjomegaCap_unoI(CCTK_PASS_FTOF, phiy_p(i, j))
					Flux_iphix_1(i, j) = Fiphix_unoI(CCTK_PASS_FTOF, omegaCap_p(i, j))
					Flux_jphiy_1(i, j) = Fjphiy_unoI(CCTK_PASS_FTOF, omegaCap_p(i, j))
					Speedi_1(i, j) = MAX(ABS(1.0d0), ABS(-1.0d0), ABS(0.0d0))
					Speedj_1(i, j) = MAX(ABS(1.0d0), ABS(-1.0d0), ABS(0.0d0))
				end if
			end do
		end do
		do j = jStart, jEnd
			do i = iStart, iEnd
				if ((FOV_1(i, j) .gt. 0 .or. (((i + 1 .le. cctk_lsh(1) .and. FOV_1(i + 1, j) .gt. 0) .or. (j + 1 .le. cctk_lsh(2) .and. FOV_1(i, j + 1) .gt. 0)) .or. ((i - 1 .ge. 1 .and. FOV_1(i - 1, j) .gt. 0) .or. (j - 1 .ge. 1 .and. FOV_1(i, j - 1) .gt. 0)))) .and. (i + 1 .le. cctk_lsh(1) .and. i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. j - 1 .ge. 1)) then
					Speedi_1(i, j) = MAX(Speedi_1(i, j), Speedi_1(i + 1, j))
					Speedj_1(i, j) = MAX(Speedj_1(i, j), Speedj_1(i, j + 1))
				end if
			end do
		end do
		call CCTK_SyncGroup(status, cctkGH, "WaveEquation2DcentralGauss::block1")
		do j = jStart, jEnd
			do i = iStart, iEnd
				if ((FOV_1(i, j) .gt. 0) .and. (i + 2 .le. cctk_lsh(1) .and. i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. j - 2 .ge. 1)) then
					fluxAccomegaCap_1 = 0.0d0 + 0.0d0
					fluxAccphix_1 = 0.0d0 + 0.0d0
					fluxAccphiy_1 = 0.0d0 + 0.0d0
					fluxAccomegaCap_1 = fluxAccomegaCap_1 - FDOCi(CCTK_PASS_FTOF, omegaCap_p, Flux_iomegaCap_1, Speedi_1, i, j)
					fluxAccomegaCap_1 = fluxAccomegaCap_1 - FDOCj(CCTK_PASS_FTOF, omegaCap_p, Flux_jomegaCap_1, Speedj_1, i, j)
					fluxAccphix_1 = fluxAccphix_1 - FDOCi(CCTK_PASS_FTOF, phix_p, Flux_iphix_1, Speedi_1, i, j)
					fluxAccphiy_1 = fluxAccphiy_1 - FDOCj(CCTK_PASS_FTOF, phiy_p, Flux_jphiy_1, Speedj_1, i, j)
					k1omegaCap(i, j) = RK3P1(CCTK_PASS_FTOF, fluxAccomegaCap_1, omegaCap_p, 0, i, j)
					k1phix(i, j) = RK3P1(CCTK_PASS_FTOF, fluxAccphix_1, phix_p, 0, i, j)
					k1phiy(i, j) = RK3P1(CCTK_PASS_FTOF, fluxAccphiy_1, phiy_p, 0, i, j)
				end if
			end do
		end do
		call CCTK_SyncGroup(status, cctkGH, "WaveEquation2DcentralGauss::block2")
		do j = jStart, jEnd
			do i = iStart, iEnd
!				Boundaries
		if ((FOV_xLower(i, j) .gt. 0) .and. ((i + 1 .le. cctk_lsh(1) .and. (FOV_1(int(i + 1), j) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. (FOV_1(int(i + 2), j) .gt. 0)))) then
				k1omegaCap(3 - i, j) = k1omegaCap(3 - i + 1, j)
				k1phix(3 - i, j) = k1phix(3 - i + 1, j)
				k1phiy(3 - i, j) = k1phiy(3 - i + 1, j)
		end if
		if ((FOV_xUpper(i, j) .gt. 0) .and. ((i - 1 .ge. 1 .and. (FOV_1(int(i - 1), j) .gt. 0)) .or. (i - 2 .ge. 1 .and. (FOV_1(int(i - 2), j) .gt. 0)))) then
			k1omegaCap(i, j) = k1omegaCap(i - 1, j)
			k1phix(i, j) = k1phix(i - 1, j)
			k1phiy(i, j) = k1phiy(i - 1, j)
		end if
		if ((FOV_yLower(i, j) .gt. 0) .and. ((j + 1 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 1)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 2)) .gt. 0)))) then
				k1omegaCap(i, 3 - j) = k1omegaCap(i, 3 - j + 1)
				k1phix(i, 3 - j) = k1phix(i, 3 - j + 1)
				k1phiy(i, 3 - j) = k1phiy(i, 3 - j + 1)
		end if
		if ((FOV_yUpper(i, j) .gt. 0) .and. ((j - 1 .ge. 1 .and. (FOV_1(i, int(j - 1)) .gt. 0)) .or. (j - 2 .ge. 1 .and. (FOV_1(i, int(j - 2)) .gt. 0)))) then
			k1omegaCap(i, j) = k1omegaCap(i, j - 1)
			k1phix(i, j) = k1phix(i, j - 1)
			k1phiy(i, j) = k1phiy(i, j - 1)
		end if
			end do
		end do
		call CCTK_SyncGroup(status, cctkGH, "WaveEquation2DcentralGauss::block2")
		do j = jStart, jEnd
			do i = iStart, iEnd
				if ((FOV_1(i, j) .gt. 0 .or. (((i + 1 .le. cctk_lsh(1) .and. FOV_1(i + 1, j) .gt. 0) .or. (i + 2 .le. cctk_lsh(1) .and. FOV_1(i + 2, j) .gt. 0) .or. (j + 1 .le. cctk_lsh(2) .and. FOV_1(i, j + 1) .gt. 0) .or. (j + 2 .le. cctk_lsh(2) .and. FOV_1(i, j + 2) .gt. 0)) .or. ((i - 1 .ge. 1 .and. FOV_1(i - 1, j) .gt. 0) .or. (i - 2 .ge. 1 .and. FOV_1(i - 2, j) .gt. 0) .or. (j - 1 .ge. 1 .and. FOV_1(i, j - 1) .gt. 0) .or. (j - 2 .ge. 1 .and. FOV_1(i, j - 2) .gt. 0))))) then
					Flux_iomegaCap_1(i, j) = FiomegaCap_unoI(CCTK_PASS_FTOF, k1phix(i, j))
					Flux_jomegaCap_1(i, j) = FjomegaCap_unoI(CCTK_PASS_FTOF, k1phiy(i, j))
					Flux_iphix_1(i, j) = Fiphix_unoI(CCTK_PASS_FTOF, k1omegaCap(i, j))
					Flux_jphiy_1(i, j) = Fjphiy_unoI(CCTK_PASS_FTOF, k1omegaCap(i, j))
					Speedi_1(i, j) = MAX(ABS(1.0d0), ABS(-1.0d0), ABS(0.0d0))
					Speedj_1(i, j) = MAX(ABS(1.0d0), ABS(-1.0d0), ABS(0.0d0))
				end if
			end do
		end do
		do j = jStart, jEnd
			do i = iStart, iEnd
				if ((FOV_1(i, j) .gt. 0 .or. (((i + 1 .le. cctk_lsh(1) .and. FOV_1(i + 1, j) .gt. 0) .or. (j + 1 .le. cctk_lsh(2) .and. FOV_1(i, j + 1) .gt. 0)) .or. ((i - 1 .ge. 1 .and. FOV_1(i - 1, j) .gt. 0) .or. (j - 1 .ge. 1 .and. FOV_1(i, j - 1) .gt. 0)))) .and. (i + 1 .le. cctk_lsh(1) .and. i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. j - 1 .ge. 1)) then
					Speedi_1(i, j) = MAX(Speedi_1(i, j), Speedi_1(i + 1, j))
					Speedj_1(i, j) = MAX(Speedj_1(i, j), Speedj_1(i, j + 1))
				end if
			end do
		end do
		call CCTK_SyncGroup(status, cctkGH, "WaveEquation2DcentralGauss::block1")
		do j = jStart, jEnd
			do i = iStart, iEnd
				if ((FOV_1(i, j) .gt. 0) .and. (i + 2 .le. cctk_lsh(1) .and. i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. j - 2 .ge. 1)) then
					fluxAccomegaCap_1 = 0.0d0 + 0.0d0
					fluxAccphix_1 = 0.0d0 + 0.0d0
					fluxAccphiy_1 = 0.0d0 + 0.0d0
					fluxAccomegaCap_1 = fluxAccomegaCap_1 - FDOCi(CCTK_PASS_FTOF, k1omegaCap, Flux_iomegaCap_1, Speedi_1, i, j)
					fluxAccomegaCap_1 = fluxAccomegaCap_1 - FDOCj(CCTK_PASS_FTOF, k1omegaCap, Flux_jomegaCap_1, Speedj_1, i, j)
					fluxAccphix_1 = fluxAccphix_1 - FDOCi(CCTK_PASS_FTOF, k1phix, Flux_iphix_1, Speedi_1, i, j)
					fluxAccphiy_1 = fluxAccphiy_1 - FDOCj(CCTK_PASS_FTOF, k1phiy, Flux_jphiy_1, Speedj_1, i, j)
					k2omegaCap(i, j) = RK3P2(CCTK_PASS_FTOF, fluxAccomegaCap_1, omegaCap_p, k1omegaCap, i, j)
					k2phix(i, j) = RK3P2(CCTK_PASS_FTOF, fluxAccphix_1, phix_p, k1phix, i, j)
					k2phiy(i, j) = RK3P2(CCTK_PASS_FTOF, fluxAccphiy_1, phiy_p, k1phiy, i, j)
				end if
			end do
		end do
		call CCTK_SyncGroup(status, cctkGH, "WaveEquation2DcentralGauss::block6")
		do j = jStart, jEnd
			do i = iStart, iEnd
!				Boundaries
		if ((FOV_xLower(i, j) .gt. 0) .and. ((i + 1 .le. cctk_lsh(1) .and. (FOV_1(int(i + 1), j) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. (FOV_1(int(i + 2), j) .gt. 0)))) then
				k2omegaCap(3 - i, j) = k2omegaCap(3 - i + 1, j)
				k2phix(3 - i, j) = k2phix(3 - i + 1, j)
				k2phiy(3 - i, j) = k2phiy(3 - i + 1, j)
		end if
		if ((FOV_xUpper(i, j) .gt. 0) .and. ((i - 1 .ge. 1 .and. (FOV_1(int(i - 1), j) .gt. 0)) .or. (i - 2 .ge. 1 .and. (FOV_1(int(i - 2), j) .gt. 0)))) then
			k2omegaCap(i, j) = k2omegaCap(i - 1, j)
			k2phix(i, j) = k2phix(i - 1, j)
			k2phiy(i, j) = k2phiy(i - 1, j)
		end if
		if ((FOV_yLower(i, j) .gt. 0) .and. ((j + 1 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 1)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 2)) .gt. 0)))) then
				k2omegaCap(i, 3 - j) = k2omegaCap(i, 3 - j + 1)
				k2phix(i, 3 - j) = k2phix(i, 3 - j + 1)
				k2phiy(i, 3 - j) = k2phiy(i, 3 - j + 1)
		end if
		if ((FOV_yUpper(i, j) .gt. 0) .and. ((j - 1 .ge. 1 .and. (FOV_1(i, int(j - 1)) .gt. 0)) .or. (j - 2 .ge. 1 .and. (FOV_1(i, int(j - 2)) .gt. 0)))) then
			k2omegaCap(i, j) = k2omegaCap(i, j - 1)
			k2phix(i, j) = k2phix(i, j - 1)
			k2phiy(i, j) = k2phiy(i, j - 1)
		end if
			end do
		end do
		call CCTK_SyncGroup(status, cctkGH, "WaveEquation2DcentralGauss::block6")
		do j = jStart, jEnd
			do i = iStart, iEnd
				if ((FOV_1(i, j) .gt. 0 .or. (((i + 1 .le. cctk_lsh(1) .and. FOV_1(i + 1, j) .gt. 0) .or. (i + 2 .le. cctk_lsh(1) .and. FOV_1(i + 2, j) .gt. 0) .or. (j + 1 .le. cctk_lsh(2) .and. FOV_1(i, j + 1) .gt. 0) .or. (j + 2 .le. cctk_lsh(2) .and. FOV_1(i, j + 2) .gt. 0)) .or. ((i - 1 .ge. 1 .and. FOV_1(i - 1, j) .gt. 0) .or. (i - 2 .ge. 1 .and. FOV_1(i - 2, j) .gt. 0) .or. (j - 1 .ge. 1 .and. FOV_1(i, j - 1) .gt. 0) .or. (j - 2 .ge. 1 .and. FOV_1(i, j - 2) .gt. 0))))) then
					Flux_iomegaCap_1(i, j) = FiomegaCap_unoI(CCTK_PASS_FTOF, k2phix(i, j))
					Flux_jomegaCap_1(i, j) = FjomegaCap_unoI(CCTK_PASS_FTOF, k2phiy(i, j))
					Flux_iphix_1(i, j) = Fiphix_unoI(CCTK_PASS_FTOF, k2omegaCap(i, j))
					Flux_jphiy_1(i, j) = Fjphiy_unoI(CCTK_PASS_FTOF, k2omegaCap(i, j))
					Speedi_1(i, j) = MAX(ABS(1.0d0), ABS(-1.0d0), ABS(0.0d0))
					Speedj_1(i, j) = MAX(ABS(1.0d0), ABS(-1.0d0), ABS(0.0d0))
				end if
			end do
		end do
		do j = jStart, jEnd
			do i = iStart, iEnd
				if ((FOV_1(i, j) .gt. 0 .or. (((i + 1 .le. cctk_lsh(1) .and. FOV_1(i + 1, j) .gt. 0) .or. (j + 1 .le. cctk_lsh(2) .and. FOV_1(i, j + 1) .gt. 0)) .or. ((i - 1 .ge. 1 .and. FOV_1(i - 1, j) .gt. 0) .or. (j - 1 .ge. 1 .and. FOV_1(i, j - 1) .gt. 0)))) .and. (i + 1 .le. cctk_lsh(1) .and. i - 1 .ge. 1 .and. j + 1 .le. cctk_lsh(2) .and. j - 1 .ge. 1)) then
					Speedi_1(i, j) = MAX(Speedi_1(i, j), Speedi_1(i + 1, j))
					Speedj_1(i, j) = MAX(Speedj_1(i, j), Speedj_1(i, j + 1))
				end if
			end do
		end do
		call CCTK_SyncGroup(status, cctkGH, "WaveEquation2DcentralGauss::block1")
		do j = jStart, jEnd
			do i = iStart, iEnd
				if ((FOV_1(i, j) .gt. 0) .and. (i + 2 .le. cctk_lsh(1) .and. i - 2 .ge. 1 .and. j + 2 .le. cctk_lsh(2) .and. j - 2 .ge. 1)) then
					fluxAccomegaCap_1 = 0.0d0 + 0.0d0
					fluxAccphix_1 = 0.0d0 + 0.0d0
					fluxAccphiy_1 = 0.0d0 + 0.0d0
					fluxAccomegaCap_1 = fluxAccomegaCap_1 - FDOCi(CCTK_PASS_FTOF, k2omegaCap, Flux_iomegaCap_1, Speedi_1, i, j)
					fluxAccomegaCap_1 = fluxAccomegaCap_1 - FDOCj(CCTK_PASS_FTOF, k2omegaCap, Flux_jomegaCap_1, Speedj_1, i, j)
					fluxAccphix_1 = fluxAccphix_1 - FDOCi(CCTK_PASS_FTOF, k2phix, Flux_iphix_1, Speedi_1, i, j)
					fluxAccphiy_1 = fluxAccphiy_1 - FDOCj(CCTK_PASS_FTOF, k2phiy, Flux_jphiy_1, Speedj_1, i, j)
					omegaCap(i, j) = RK3P3(CCTK_PASS_FTOF, fluxAccomegaCap_1, omegaCap_p, k2omegaCap, i, j)
					phix(i, j) = RK3P3(CCTK_PASS_FTOF, fluxAccphix_1, phix_p, k2phix, i, j)
					phiy(i, j) = RK3P3(CCTK_PASS_FTOF, fluxAccphiy_1, phiy_p, k2phiy, i, j)
				end if
			end do
		end do
		call CCTK_SyncGroup(status, cctkGH, "WaveEquation2DcentralGauss::fields_tl2")
		do j = jStart, jEnd
			do i = iStart, iEnd
!				Boundaries
		if ((FOV_xLower(i, j) .gt. 0) .and. ((i + 1 .le. cctk_lsh(1) .and. (FOV_1(int(i + 1), j) .gt. 0)) .or. (i + 2 .le. cctk_lsh(1) .and. (FOV_1(int(i + 2), j) .gt. 0)))) then
				omegaCap(3 - i, j) = omegaCap(3 - i + 1, j)
				phix(3 - i, j) = phix(3 - i + 1, j)
				phiy(3 - i, j) = phiy(3 - i + 1, j)
		end if
		if ((FOV_xUpper(i, j) .gt. 0) .and. ((i - 1 .ge. 1 .and. (FOV_1(int(i - 1), j) .gt. 0)) .or. (i - 2 .ge. 1 .and. (FOV_1(int(i - 2), j) .gt. 0)))) then
			omegaCap(i, j) = omegaCap(i - 1, j)
			phix(i, j) = phix(i - 1, j)
			phiy(i, j) = phiy(i - 1, j)
		end if
		if ((FOV_yLower(i, j) .gt. 0) .and. ((j + 1 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 1)) .gt. 0)) .or. (j + 2 .le. cctk_lsh(2) .and. (FOV_1(i, int(j + 2)) .gt. 0)))) then
				omegaCap(i, 3 - j) = omegaCap(i, 3 - j + 1)
				phix(i, 3 - j) = phix(i, 3 - j + 1)
				phiy(i, 3 - j) = phiy(i, 3 - j + 1)
		end if
		if ((FOV_yUpper(i, j) .gt. 0) .and. ((j - 1 .ge. 1 .and. (FOV_1(i, int(j - 1)) .gt. 0)) .or. (j - 2 .ge. 1 .and. (FOV_1(i, int(j - 2)) .gt. 0)))) then
			omegaCap(i, j) = omegaCap(i, j - 1)
			phix(i, j) = phix(i, j - 1)
			phiy(i, j) = phiy(i, j - 1)
		end if
			end do
		end do
		call CCTK_SyncGroup(status, cctkGH, "WaveEquation2DcentralGauss::fields_tl2")

	end subroutine WaveEquation2DcentralGauss_execution