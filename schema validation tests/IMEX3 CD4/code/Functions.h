#ifndef included_FunctionsXD
#define included_FunctionsXD

#include "SAMRAI/tbox/Utilities.h"
#include <string>
#include <math.h>
#include <vector>
#include "hdf5.h"
#include "Commons.h"


#define MIN(X,Y) ((X) < (Y) ? (X) : (Y))
#define MAX(X,Y) ((X) > (Y) ? (X) : (Y))
#define SIGN(X) (((X) > 0) - ((X) < 0))
#define greaterEq(a,b) ((fabs((a) - (b))/1.0E-15 > 10 ? false : (floor(fabs((a) - (b))/1.0E-15) < 1)) || (b)<(a))
#define lessEq(a,b) ((fabs((a) - (b))/1.0E-15 > 10 ? false: (floor(fabs((a) - (b))/1.0E-15) < 1)) || (a)<(b))
#define equalsEq(a,b) ((fabs((a) - (b))/1.0E-15 > 10 ? false: (floor(fabs((a) - (b))/1.0E-15) < 1)))
#define reducePrecision(x, p) (floor(((x) * pow(10, (p)) + 0.5)) / pow(10, (p)))

using namespace external;

/*
 * Calculates coefficients for quintic lagrangian interpolation.
 *    coefs;      coefficients to obtain
 *    coord:      point in which the interpolation must be calculated
 *    position:   surrounding points for interpolation
 */
inline void calculateCoefficientsMapFile(double* coefs, double coord, double* position) {

    for (int i = 0; i < 6; i++) {
        coefs[i] = 0;
    }
    //Search for a perfect fit

    for (int i = 0; i < 6; i++) {
        if (position[i] == coord) {
            coefs[i] = 1;
            return;
        }
    }

    double x1 = position[0];
    double x2 = position[1];
    double x3 = position[2];
    double x4 = position[3];
    double x5 = position[4];
    double x6 = position[5];

    coefs[0] = (coord-x2)*(coord-x3)*(coord-x4)*(coord-x5)*(coord-x6) / ((x1-x2)*(x1-x3)*(x1-x4)*(x1-x5)*(x1-x6));
    coefs[1] = (coord-x1)*(coord-x3)*(coord-x4)*(coord-x5)*(coord-x6) / ((x2-x1)*(x2-x3)*(x2-x4)*(x2-x5)*(x2-x6));
    coefs[2] = (coord-x1)*(coord-x2)*(coord-x4)*(coord-x5)*(coord-x6) / ((x3-x1)*(x3-x2)*(x3-x4)*(x3-x5)*(x3-x6));
    coefs[3] = (coord-x1)*(coord-x2)*(coord-x3)*(coord-x5)*(coord-x6) / ((x4-x1)*(x4-x2)*(x4-x3)*(x4-x5)*(x4-x6));
    coefs[4] = (coord-x1)*(coord-x2)*(coord-x3)*(coord-x4)*(coord-x6) / ((x5-x1)*(x5-x2)*(x5-x3)*(x5-x4)*(x5-x6));
    coefs[5] = (coord-x1)*(coord-x2)*(coord-x3)*(coord-x4)*(coord-x5) / ((x6-x1)*(x6-x2)*(x6-x3)*(x6-x4)*(x6-x5));
}



#define vector(v, i, j) (v)[i+ilast*(j)]

#define IMEX3P1_(parun, dx, simPlat_dt, ilast, jlast) ((parun))

#define IMEX3P2_(parun, dx, simPlat_dt, ilast, jlast) ((parun))

#define IMEX3P2S_(parrk1rhs, dx, simPlat_dt, ilast, jlast) (simPlat_dt * (-ALPHA) * (parrk1rhs))

#define D1CDO4_i(paru, pari, parj, dx, simPlat_dt, ilast, jlast) ((((-vector(paru, (pari) + 2, (parj))) + 8.0 * vector(paru, (pari) + 1, (parj))) + ((-8.0 * vector(paru, (pari) - 1, (parj))) + vector(paru, (pari) - 2, (parj)))) / (12.0 * dx[0]))

#define D1CDO4_j(paru, pari, parj, dx, simPlat_dt, ilast, jlast) ((((-vector(paru, (pari), (parj) + 2)) + 8.0 * vector(paru, (pari), (parj) + 1)) + ((-8.0 * vector(paru, (pari), (parj) - 1)) + vector(paru, (pari), (parj) - 2))) / (12.0 * dx[1]))

#define meshDissipation_i(paru, pari, parj, dx, simPlat_dt, ilast, jlast) ((vector(paru, (pari) + 3, (parj)) + (-6.0 * vector(paru, (pari) + 2, (parj))) + 15.0 * vector(paru, (pari) + 1, (parj)) + (-20.0 * vector(paru, (pari), (parj))) + 15.0 * vector(paru, (pari) - 1, (parj)) + (-6.0 * vector(paru, (pari) - 2, (parj))) + vector(paru, (pari) - 3, (parj))) / (64.0 * dx[0]))

#define meshDissipation_j(paru, pari, parj, dx, simPlat_dt, ilast, jlast) ((vector(paru, (pari), (parj) + 3) + (-6.0 * vector(paru, (pari), (parj) + 2)) + 15.0 * vector(paru, (pari), (parj) + 1) + (-20.0 * vector(paru, (pari), (parj))) + 15.0 * vector(paru, (pari), (parj) - 1) + (-6.0 * vector(paru, (pari), (parj) - 2)) + vector(paru, (pari), (parj) - 3)) / (64.0 * dx[1]))

#define IMEX3P3_(parrhs, parun, dx, simPlat_dt, ilast, jlast) ((parun) + simPlat_dt * (parrhs))

#define IMEX3P3S_(parrk2rhs, dx, simPlat_dt, ilast, jlast) (simPlat_dt * (1.0 - ALPHA) * (parrk2rhs))

#define IMEX3P4_(parrhs, parun, parrk3, dx, simPlat_dt, ilast, jlast) (1.0 / 4.0 * (3.0 * (parun) + (parrk3) + simPlat_dt * (parrhs)))

#define IMEX3P4S_(parrk1rhs, parrk2rhs, parrk3rhs, dx, simPlat_dt, ilast, jlast) (simPlat_dt * ((2.0 * ALPHA * ALPHA + 2.0 * ALPHA + (-1.0)) / (8.0 * ALPHA) * (parrk1rhs) + (((-4.0 * ALPHA * ALPHA) + 1.0) / (8.0 * ALPHA) - (1.0 - ALPHA) / 4.0) * (parrk2rhs) + (0.25 * ((-3.0 * ALPHA) + 1.0) - ALPHA / 4.0) * (parrk3rhs)))

#define IMEX3P5_(parrhs, parun, parrk4, dx, simPlat_dt, ilast, jlast) (1.0 / 3.0 * ((parun) + 2.0 * (parrk4) + 2.0 * simPlat_dt * (parrhs)))

#define IMEX3P5S_(parrk1rhs, parrk2rhs, parrk3rhs, parrk4rhs, dx, simPlat_dt, ilast, jlast) (simPlat_dt * ((-(2.0 * (2.0 * ALPHA * ALPHA + 2.0 * ALPHA + (-1.0)) / (8.0 * ALPHA)) / 3.0) * (parrk1rhs) + (0.16666666666666666666 - (2.0 * ((-4.0 * ALPHA * ALPHA) + 1.0) / (8.0 * ALPHA)) / 3.0) * (parrk2rhs) + (-(2.0 * 0.25 * ((-3.0 * ALPHA) + 1.0)) / 3.0) * (parrk3rhs) + (0.66666666666666666666 - (2.0 * ALPHA) / 3.0) * (parrk4rhs)))





#endif
