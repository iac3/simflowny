import sys, getopt
import os
import numpy as np
import h5py
from scipy import interpolate

def writefields2D(folder, field, level):
    #Read mesh information
    f_sum = h5py.File(folder + '/summary.samrai', "r")
    nProcessors  = f_sum['/BASIC_INFO/number_processors']
    patchExtents = f_sum['/extents/patch_extents']
    patchMap     = f_sum['/extents/patch_map']
    varNames = f_sum['/BASIC_INFO/var_names']

    maximum = [0, 0]
    minimum = [999999, 999999]
    for i in range(len(patchExtents)):
        if (patchMap[i][2] == level):
            maximum[0] = max(maximum[0], patchExtents[i][1][0])
            maximum[1] = max(maximum[1], patchExtents[i][1][1])
            minimum[0] = min(minimum[0], patchExtents[i][0][0])
            minimum[1] = min(minimum[1], patchExtents[i][0][1])
    size = [(maximum[0] - minimum[0]) + 2 ,(maximum[1] - minimum[1]) + 2]
    data = np.zeros(shape=(size[0], size[1]))

    for iPatch in range(len(patchExtents)):
        if (patchMap[iPatch][2] == level):
            iProc = patchMap[iPatch][0]
            iProcStr = str(iProc).zfill(5)
            iPatchStr = str(patchMap[iPatch][3]).zfill(5)
            rangeX = (patchExtents[iPatch][0][0] - minimum[0], patchExtents[iPatch][1][0]+2 - minimum[0])
            rangeY = (patchExtents[iPatch][0][1] - minimum[1], patchExtents[iPatch][1][1]+2 - minimum[1])
            sizeX = patchExtents[iPatch][1][0] - patchExtents[iPatch][0][0] + 2;
            sizeY = patchExtents[iPatch][1][1] - patchExtents[iPatch][0][1] + 2;
            f_data = h5py.File(folder + '/processor_cluster.' + iProcStr + '.samrai', "r")
            tmp = f_data['/processor.' + iProcStr + '/level.' + str(level).zfill(5) + '/patch.' + iPatchStr + '/' + field]
            tmp = np.reshape(tmp, (sizeX,sizeY), order="F")
            data[rangeX[0]:rangeX[1],rangeY[0]:rangeY[1]] = tmp[:,:]
            f_data.close()

    data2 = data
    #Writing the variable to disk
    f = h5py.File(folder + '/' + field + '_' + str(level) + '.hdf5', "w")
    dset = f.create_dataset(field, data2.shape, dtype='float64')
    dset[:,:] = data2[:,:]
    f.close()
    f_sum.close()


def main():
        
    for t in xrange(0, 501, 10):
        writefields2D("./outputDir_mesh_50_mono/visit_dump." + str(t).zfill(5), "K", 0)

    for t in xrange(0, 1001, 20):
        writefields2D("./outputDir_mesh_100_mono/visit_dump." + str(t).zfill(5), "K", 0)

    for t in xrange(0, 2001, 40):
        writefields2D("./outputDir_mesh_200_mono/visit_dump." + str(t).zfill(5), "K", 0)

    for t in xrange(0, 4001, 80):
        writefields2D("./outputDir_mesh_400_mono/visit_dump." + str(t).zfill(5), "K", 0)

    for t in xrange(0, 1993, 24):
        writefields2D("./outputDir_mesh_50_fmr/visit_dump." + str(t).zfill(5), "K", 0)

    for t in xrange(0, 3985, 48):
        writefields2D("./outputDir_mesh_100_fmr/visit_dump." + str(t).zfill(5), "K", 0)

    for t in xrange(0, 7969, 96):
        writefields2D("./outputDir_mesh_200_fmr/visit_dump." + str(t).zfill(5), "K", 0)
    
    for t in xrange(0, 15937, 192):
        writefields2D("./outputDir_mesh_400_fmr/visit_dump." + str(t).zfill(5), "K", 0)
    


if __name__ == "__main__":
    main()
