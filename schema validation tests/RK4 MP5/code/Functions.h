#ifndef included_FunctionsXD
#define included_FunctionsXD

#include "SAMRAI/tbox/Utilities.h"
#include <string>
#include <math.h>
#include <vector>
#include "hdf5.h"
#include "Commons.h"


#define MIN(X,Y) ((X) < (Y) ? (X) : (Y))
#define MAX(X,Y) ((X) > (Y) ? (X) : (Y))
#define SIGN(X) (((X) > 0) - ((X) < 0))
#define greaterEq(a,b) ((fabs((a) - (b))/1.0E-15 > 10 ? false : (floor(fabs((a) - (b))/1.0E-15) < 1)) || (b)<(a))
#define lessEq(a,b) ((fabs((a) - (b))/1.0E-15 > 10 ? false: (floor(fabs((a) - (b))/1.0E-15) < 1)) || (a)<(b))
#define equalsEq(a,b) ((fabs((a) - (b))/1.0E-15 > 10 ? false: (floor(fabs((a) - (b))/1.0E-15) < 1)))
#define reducePrecision(x, p) (floor(((x) * pow(10, (p)) + 0.5)) / pow(10, (p)))

using namespace external;

/*
 * Calculates coefficients for quintic lagrangian interpolation.
 *    coefs;      coefficients to obtain
 *    coord:      point in which the interpolation must be calculated
 *    position:   surrounding points for interpolation
 */
inline void calculateCoefficientsMapFile(double* coefs, double coord, double* position) {

    for (int i = 0; i < 6; i++) {
        coefs[i] = 0;
    }
    //Search for a perfect fit

    for (int i = 0; i < 6; i++) {
        if (position[i] == coord) {
            coefs[i] = 1;
            return;
        }
    }

    double x1 = position[0];
    double x2 = position[1];
    double x3 = position[2];
    double x4 = position[3];
    double x5 = position[4];
    double x6 = position[5];

    coefs[0] = (coord-x2)*(coord-x3)*(coord-x4)*(coord-x5)*(coord-x6) / ((x1-x2)*(x1-x3)*(x1-x4)*(x1-x5)*(x1-x6));
    coefs[1] = (coord-x1)*(coord-x3)*(coord-x4)*(coord-x5)*(coord-x6) / ((x2-x1)*(x2-x3)*(x2-x4)*(x2-x5)*(x2-x6));
    coefs[2] = (coord-x1)*(coord-x2)*(coord-x4)*(coord-x5)*(coord-x6) / ((x3-x1)*(x3-x2)*(x3-x4)*(x3-x5)*(x3-x6));
    coefs[3] = (coord-x1)*(coord-x2)*(coord-x3)*(coord-x5)*(coord-x6) / ((x4-x1)*(x4-x2)*(x4-x3)*(x4-x5)*(x4-x6));
    coefs[4] = (coord-x1)*(coord-x2)*(coord-x3)*(coord-x4)*(coord-x6) / ((x5-x1)*(x5-x2)*(x5-x3)*(x5-x4)*(x5-x6));
    coefs[5] = (coord-x1)*(coord-x2)*(coord-x3)*(coord-x4)*(coord-x5) / ((x6-x1)*(x6-x2)*(x6-x3)*(x6-x4)*(x6-x5));
}



#define vector(v, i, j) (v)[i+ilast*(j)]

#define FVM_i(parfIntPos, parfIntNeg, dx, simPlat_dt, ilast, jlast) (((parfIntPos) - (parfIntNeg)) / dx[0])

#define FVM_j(parfIntPos, parfIntNeg, dx, simPlat_dt, ilast, jlast) (((parfIntPos) - (parfIntNeg)) / dx[1])

#define meshDissipation_i(paru, pari, parj, dx, simPlat_dt, ilast, jlast) ((vector(paru, (pari) + 3, (parj)) + (-6.0 * vector(paru, (pari) + 2, (parj))) + 15.0 * vector(paru, (pari) + 1, (parj)) + (-20.0 * vector(paru, (pari), (parj))) + 15.0 * vector(paru, (pari) - 1, (parj)) + (-6.0 * vector(paru, (pari) - 2, (parj))) + vector(paru, (pari) - 3, (parj))) / (64.0 * dx[0]))

#define meshDissipation_j(paru, pari, parj, dx, simPlat_dt, ilast, jlast) ((vector(paru, (pari), (parj) + 3) + (-6.0 * vector(paru, (pari), (parj) + 2)) + 15.0 * vector(paru, (pari), (parj) + 1) + (-20.0 * vector(paru, (pari), (parj))) + 15.0 * vector(paru, (pari), (parj) - 1) + (-6.0 * vector(paru, (pari), (parj) - 2)) + vector(paru, (pari), (parj) - 3)) / (64.0 * dx[1]))

#define RK4P1_(parRHS, parQn, dx, simPlat_dt, ilast, jlast) ((parQn) + (simPlat_dt * (parRHS)) / 2.0)

#define RK4P2_(parRHS, parQn, dx, simPlat_dt, ilast, jlast) ((parQn) + (simPlat_dt * (parRHS)) / 2.0)

#define RK4P3_(parRHS, parQn, dx, simPlat_dt, ilast, jlast) ((parQn) + simPlat_dt * (parRHS))

#define RK4P4_(parRHS, parQn, parQK1, parQK2, parQK3, dx, simPlat_dt, ilast, jlast) ((-(parQn) / 3.0) + (parQK1) / 3.0 + 2.0 * (parQK2) / 3.0 + (parQK3) / 3.0 + (simPlat_dt * (parRHS)) / 6.0)

#define Fiphix_mainI(parAlpha, parK, dx, simPlat_dt, ilast, jlast) ((parAlpha) * (parK))

#define Fjphiy_mainI(parAlpha, parK, dx, simPlat_dt, ilast, jlast) ((parAlpha) * (parK))

#define FiK_mainI(parAlpha, parphix, dx, simPlat_dt, ilast, jlast) ((parAlpha) * (parphix))

#define FjK_mainI(parAlpha, parphiy, dx, simPlat_dt, ilast, jlast) ((parAlpha) * (parphiy))

inline double Minmod4(double parw, double para, double parb, double parc, const double* dx, const double simPlat_dt, const int ilast, const int jlast) {

	if (equalsEq((parw * para * parb * parc), 0.0)) {
		return 0.0;
	} else {
		return 0.125 * (parw / fabs(parw) + para / fabs(para)) * fabs((parw / fabs(parw) + parb / fabs(parb)) * (parw / fabs(parw) + parc / fabs(parc))) * MIN(fabs(parw), MIN(fabs(para), MIN(fabs(parb), fabs(parc))));
	}

};

inline double Minmod(double para, double parb, const double* dx, const double simPlat_dt, const int ilast, const int jlast) {

	if (equalsEq((para * parb), 0.0)) {
		return 0.0;
	} else {
		return 0.5 * (para / fabs(para) + parb / fabs(parb)) * MIN(fabs(para), fabs(parb));
	}

};

inline double MP5(double param2, double param1, double para, double parap1, double parap2, double paralpha, double parepsilon, const double* dx, const double simPlat_dt, const int ilast, const int jlast) {
	double vnorm, vl, vmp, djm1, dj, djp1, dm4jph, dm4jmh, vul, vav, vmd, vlc, vmin, vmax;

	vnorm = sqrt(param2 * param2 + param1 * param1 + para * para + parap1 * parap1 + parap2 * parap2);
	vl = (2.0 * param2 + (-13.0 * param1) + 47.0 * para + 27.0 * parap1 + (-3.0 * parap2)) / 60.0;
	vmp = para + Minmod(parap1 - para, paralpha * (para - param1), dx, simPlat_dt, ilast, jlast);
	if (lessEq(((vl - para) * (vl - vmp)), (parepsilon * vnorm))) {
		return vl;
	} else {
		djm1 = param2 + (-2.0 * param1) + para;
		dj = param1 + (-2.0 * para) + parap1;
		djp1 = para + (-2.0 * parap1) + parap2;
		dm4jph = Minmod4(4.0 * dj - djp1, 4.0 * djp1 - dj, dj, djp1, dx, simPlat_dt, ilast, jlast);
		dm4jmh = Minmod4(4.0 * dj - djm1, 4.0 * djm1 - dj, dj, djm1, dx, simPlat_dt, ilast, jlast);
		vul = para + paralpha * (para - param1);
		vav = 0.5 * (para + parap1);
		vmd = vav - 0.5 * dm4jph;
		vlc = para + 0.5 * (para - param1) + 4.0 / 3.0 * dm4jmh;
		vmin = MAX(MIN(para, MIN(parap1, vmd)), MIN(para, MIN(vul, vlc)));
		vmax = MIN(MAX(para, MAX(parap1, vmd)), MAX(para, MAX(vul, vlc)));
		return vl + Minmod(vmin - vl, vmax - vl, dx, simPlat_dt, ilast, jlast);
	}

};





#endif
