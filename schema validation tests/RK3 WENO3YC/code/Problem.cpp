#include "Problem.h"
#include "Functions.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <stack>
#include "hdf5.h"
#include <gsl/gsl_rng.h>
#include <random>
#include "SAMRAI/pdat/CellVariable.h"
#include "SAMRAI/pdat/NodeData.h"
#include "SAMRAI/pdat/NodeVariable.h"
#include "LagrangianPolynomicRefine.h"


#include "SAMRAI/tbox/Array.h"
#include "SAMRAI/hier/BoundaryBox.h"
#include "SAMRAI/hier/BoxContainer.h"
#include "SAMRAI/geom/CartesianPatchGeometry.h"
#include "SAMRAI/hier/VariableDatabase.h"
#include "SAMRAI/hier/PatchDataRestartManager.h"
#include "SAMRAI/hier/Index.h"
#include "SAMRAI/tbox/PIO.h"
#include "SAMRAI/tbox/Utilities.h"
#include "SAMRAI/tbox/Timer.h"
#include "SAMRAI/tbox/TimerManager.h"

#define MIN(X,Y) ((X) < (Y) ? (X) : (Y))
#define MAX(X,Y) ((X) > (Y) ? (X) : (Y))
#define SIGN(X) (((X) > 0) - ((X) < 0))
#define isEven(a) ((a) % 2 == 0 ? true : false)
#define greaterEq(a,b) ((fabs((a) - (b))/1.0E-15 > 10 ? false : (floor(fabs((a) - (b))/1.0E-15) < 1)) || (b)<(a))
#define lessEq(a,b) ((fabs((a) - (b))/1.0E-15 > 10 ? false: (floor(fabs((a) - (b))/1.0E-15) < 1)) || (a)<(b))
#define equalsEq(a,b) ((fabs((a) - (b))/1.0E-15 > 10 ? false: (floor(fabs((a) - (b))/1.0E-15) < 1)))
#define reducePrecision(x, p) (floor(((x) * pow(10, (p)) + 0.5)) / pow(10, (p)))

using namespace external;

const gsl_rng_type * T;
gsl_rng *r_var;

//Timers
std::shared_ptr<tbox::Timer> t_step;
std::shared_ptr<tbox::Timer> t_moveParticles;
std::shared_ptr<tbox::Timer> t_output;

inline int Problem::GetExpoBase2(double d)
{
	int i = 0;
	((short *)(&i))[0] = (((short *)(&d))[3] & (short)32752); // _123456789ab____ & 0111111111110000
	return (i >> 4) - 1023;
}

bool	Problem::Equals(double d1, double d2)
{
	if (d1 == d2)
		return true;
	int e1 = GetExpoBase2(d1);
	int e2 = GetExpoBase2(d2);
	int e3 = GetExpoBase2(d1 - d2);
	if ((e3 - e2 < -48) && (e3 - e1 < -48))
		return true;
	return false;
}

/*
 * Constructor of the problem.
 */
Problem::Problem(const string& object_name, const tbox::Dimension& dim, std::shared_ptr<tbox::Database>& database, std::shared_ptr<geom::CartesianGridGeometry >& grid_geom, std::shared_ptr<hier::PatchHierarchy >& patch_hierarchy, MainRestartData& mrd, const double dt, const bool init_from_restart, const int console_output, const int timer_output, const int mesh_output_period, const vector<string> full_mesh_writer_variables, std::shared_ptr<appu::VisItDataWriter>& mesh_data_writer, const vector<int> integration_output_period, const vector<set<string> > integralVariables, vector<std::shared_ptr<IntegrateDataWriter > > integrateDataWriters, const vector<int> point_output_period, const vector<set<string> > pointVariables, vector<std::shared_ptr<PointDataWriter > > pointDataWriters): 
d_dim(dim), xfer::RefinePatchStrategy(), xfer::CoarsenPatchStrategy(), viz_mesh_dump_interval(mesh_output_period), d_full_mesh_writer_variables(full_mesh_writer_variables.begin(), full_mesh_writer_variables.end()), d_visit_data_writer(mesh_data_writer), d_output_interval(console_output), d_timer_output_interval(timer_output), d_integrateDataWriters(integrateDataWriters.begin(), integrateDataWriters.end()), d_integration_output_period(integration_output_period.begin(), integration_output_period.end()), d_pointDataWriters(pointDataWriters.begin(), pointDataWriters.end()), d_point_output_period(point_output_period.begin(), point_output_period.end())
{
	//Setup the timers
	t_step = tbox::TimerManager::getManager()->getTimer("Step");
	t_moveParticles = tbox::TimerManager::getManager()->getTimer("Move particles");
	t_output = tbox::TimerManager::getManager()->getTimer("OutputGeneration");

	//Output configuration
	next_console_output = d_output_interval;
	next_timer_output = d_timer_output_interval;

	//Get the object name, the grid geometry and the patch hierarchy
  	d_grid_geometry = grid_geom;
	d_patch_hierarchy = patch_hierarchy;
	d_object_name = object_name;
	d_init_from_restart = init_from_restart;
	initial_dt = dt;


	for (vector<set<string> >::const_iterator it = integralVariables.begin(); it != integralVariables.end(); ++it) {
		set<string> vars = *it;
		for (set<string>::const_iterator it2 = vars.begin() ; it2 != vars.end(); ++it2) {
			d_integralVariables.push_back(vars);
		}
	}
	for (vector<set<string> >::const_iterator it = pointVariables.begin(); it != pointVariables.end(); ++it) {
		set<string> vars = *it;
		for (set<string>::const_iterator it2 = vars.begin() ; it2 != vars.end(); ++it2) {
			d_pointVariables.push_back(vars);
		}
	}


	//Get parameters
    cout<<"Reading parameters"<<endl;
	dissipation_factor_K = database->getDouble("dissipation_factor_K");
	tend = database->getDouble("tend");
	agauss = database->getDouble("agauss");
	epsilon_main = database->getDouble("epsilon_main");
	dissipation_factor_Phi = database->getDouble("dissipation_factor_Phi");
	dissipation_factor_Alpha = database->getDouble("dissipation_factor_Alpha");
	dissipation_factor_phix = database->getDouble("dissipation_factor_phix");
	bgauss = database->getDouble("bgauss");
	m = database->getDouble("m");
	dissipation_factor_phiy = database->getDouble("dissipation_factor_phiy");
	//Random initialization
	gsl_rng_env_setup();
	//Random for simulation
	const tbox::SAMRAI_MPI& mpi(tbox::SAMRAI_MPI::getSAMRAIWorld());
	int random_seed = database->getDouble("random_seed")*(mpi.getRank() + 1);
	r_var = gsl_rng_alloc(gsl_rng_ranlxs0);
	gsl_rng_set(r_var, random_seed);
	for (int il = 0; il < d_patch_hierarchy->getMaxNumberOfLevels(); il++) {
		bo_substep_iteration.push_back(0);
	}
	//Initialization from input file
	if (!d_init_from_restart) {
		next_mesh_dump_iteration = viz_mesh_dump_interval;
		for (std::vector<int>::iterator it = d_integration_output_period.begin(); it != d_integration_output_period.end(); ++it) {
			next_integration_dump_iteration.push_back((*it));
		}
		for (std::vector<int>::iterator it = d_point_output_period.begin(); it != d_point_output_period.end(); ++it) {
			next_point_dump_iteration.push_back((*it));
		}

		//Iteration counter
		for (int il = 0; il < d_patch_hierarchy->getMaxNumberOfLevels(); il++) {
			current_iteration.push_back(0);
		}
	}
	//Initialization from restart file
	else {
		getFromRestart(mrd);
		if (d_integration_output_period.size() < next_integration_dump_iteration.size()) {
			TBOX_ERROR("Number of integrations cannot be reduced after a checkpoint.");
		}
		for (int il = 0; il < d_integration_output_period.size(); il++) {
			if (il >= next_integration_dump_iteration.size()) {
				next_integration_dump_iteration.push_back(0);
			}
			if (next_integration_dump_iteration[il] == 0 && d_integration_output_period[il] > 0) {
				next_integration_dump_iteration[il] = current_iteration[d_patch_hierarchy->getNumberOfLevels() - 1] + d_integration_output_period[il];
			}
		}
		if (d_point_output_period.size() < next_point_dump_iteration.size()) {
			TBOX_ERROR("Number of point output cannot be reduced after a checkpoint.");
		}
		for (int il = 0; il < d_point_output_period.size(); il++) {
			if (il >= next_point_dump_iteration.size()) {
				next_point_dump_iteration.push_back(0);
			}
			if (next_point_dump_iteration[il] == 0 && d_point_output_period[il] > 0) {
				next_point_dump_iteration[il] = current_iteration[d_patch_hierarchy->getNumberOfLevels() - 1] + d_point_output_period[il];
			}
		}
	}


	//External eos parameters
#ifdef EXTERNAL_EOS

	std::shared_ptr<tbox::Database> external_eos_db = database->getDatabase("external_EOS");
	Commons::ExternalEos::reprimand_eos_type = external_eos_db->getInteger("eos_type");
	Commons::ExternalEos::reprimand_atmo_Ye = external_eos_db->getDouble("atmo_Ye");
	Commons::ExternalEos::reprimand_max_z = external_eos_db->getDouble("max_z");
	Commons::ExternalEos::reprimand_max_b = external_eos_db->getDouble("max_b");
	Commons::ExternalEos::reprimand_c2p_acc = external_eos_db->getDouble("c2p_acc");
	Commons::ExternalEos::reprimand_atmo_rho = external_eos_db->getDouble("atmo_rho");
	Commons::ExternalEos::reprimand_rho_strict = external_eos_db->getDouble("rho_strict");
	Commons::ExternalEos::reprimand_max_rho = external_eos_db->getDouble("max_rho");
	Commons::ExternalEos::reprimand_max_eps = external_eos_db->getDouble("max_eps");
	Commons::ExternalEos::reprimand_gamma_th = external_eos_db->getDouble("gamma_th");
#endif

    	//Subcycling
	d_refinedTimeStepping = false;
	if (database->isString("subcycling")) {
		if (database->getString("subcycling") == "BERGER-OLIGER") {
			d_refinedTimeStepping = true;
		}
	}


	//Regridding options
	d_regridding = false;
	if (database->isDatabase("regridding")) {
		regridding_db = database->getDatabase("regridding");
		d_regridding_buffer = regridding_db->getDouble("regridding_buffer");
		int smallest_patch_size = d_patch_hierarchy->getSmallestPatchSize(0).min();
		for (int il = 1; il < d_patch_hierarchy->getMaxNumberOfLevels(); il++) {
			smallest_patch_size = MIN(smallest_patch_size, d_patch_hierarchy->getSmallestPatchSize(il).min());
		}
		if (d_regridding_buffer > smallest_patch_size) {
			TBOX_ERROR("Error: Regridding_buffer parameter ("<<d_regridding_buffer<<") cannot be greater than smallest_patch_size minimum value("<<smallest_patch_size<<")");
		}
		if (regridding_db->isString("regridding_type")) {
			d_regridding_type = regridding_db->getString("regridding_type");
			d_regridding_min_level = regridding_db->getInteger("regridding_min_level");
			d_regridding_max_level = regridding_db->getInteger("regridding_max_level");
			if (d_regridding_type == "GRADIENT") {
				d_regridding_field = regridding_db->getString("regridding_field");
				d_regridding_compressionFactor = regridding_db->getDouble("regridding_compressionFactor");
				d_regridding_mOffset = regridding_db->getDouble("regridding_mOffset");
				d_regridding = true;
			} else {
				if (d_regridding_type == "FUNCTION") {
					d_regridding_field = regridding_db->getString("regridding_function_field");
					d_regridding_threshold = regridding_db->getDouble("regridding_threshold");
					d_regridding = true;
				} else {
					if (d_regridding_type == "SHADOW") {
						std::string* fields = new std::string[2];
						regridding_db->getStringArray("regridding_fields", fields, 2);
						d_regridding_field = fields[0];
						d_regridding_field_shadow = fields[1];
						d_regridding_error = regridding_db->getDouble("regridding_error");
						d_regridding = true;
						delete[] fields;
					}
				}
			}
		}
	}

	//Stencil of the discretization method
	int maxratio = 1;
	for (int il = 1; il < d_patch_hierarchy->getMaxNumberOfLevels(); il++) {
		const hier::IntVector ratio = d_patch_hierarchy->getRatioToCoarserLevel(il);
		maxratio = MAX(maxratio, ratio.max());
	}
	//Minimum region thickness
	d_regionMinThickness = 3;
	d_ghost_width = 3;

	
	//Register Fields and temporal fields into the variable database
	hier::VariableDatabase *vdb = hier::VariableDatabase::getDatabase();
  	std::shared_ptr<hier::VariableContext> d_cont_curr(vdb->getContext("Current"));
	std::shared_ptr< pdat::CellVariable<int> > mask(std::shared_ptr< pdat::CellVariable<int> >(new pdat::CellVariable<int>(d_dim, "samrai_mask",1)));
	d_mask_id = vdb->registerVariableAndContext(mask ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	IntegrateDataWriter::setMaskVariable(d_mask_id);
	std::shared_ptr< pdat::NodeVariable<double> > interior(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "regridding_value",1)));
	d_interior_regridding_value_id = vdb->registerVariableAndContext(interior ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<int> > nonSync(std::shared_ptr< pdat::NodeVariable<int> >(new pdat::NodeVariable<int>(d_dim, "regridding_tag",1)));
	d_nonSync_regridding_tag_id = vdb->registerVariableAndContext(nonSync ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<double> > interior_i(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "interior_i",1)));
	d_interior_i_id = vdb->registerVariableAndContext(interior_i ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<double> > interior_j(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "interior_j",1)));
	d_interior_j_id = vdb->registerVariableAndContext(interior_j ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<double> > FOV_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FOV_1",1)));
	d_FOV_1_id = vdb->registerVariableAndContext(FOV_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FOV_1_id);
	std::shared_ptr< pdat::NodeVariable<double> > FOV_xLower(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FOV_xLower",1)));
	d_FOV_xLower_id = vdb->registerVariableAndContext(FOV_xLower ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FOV_xLower_id);
	std::shared_ptr< pdat::NodeVariable<double> > FOV_xUpper(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FOV_xUpper",1)));
	d_FOV_xUpper_id = vdb->registerVariableAndContext(FOV_xUpper ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FOV_xUpper_id);
	std::shared_ptr< pdat::NodeVariable<double> > FOV_yLower(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FOV_yLower",1)));
	d_FOV_yLower_id = vdb->registerVariableAndContext(FOV_yLower ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FOV_yLower_id);
	std::shared_ptr< pdat::NodeVariable<double> > FOV_yUpper(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FOV_yUpper",1)));
	d_FOV_yUpper_id = vdb->registerVariableAndContext(FOV_yUpper ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FOV_yUpper_id);
	std::shared_ptr< pdat::NodeVariable<double> > Alpha(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "Alpha",1)));
	d_Alpha_id = vdb->registerVariableAndContext(Alpha ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_Alpha_id);
	std::shared_ptr< pdat::NodeVariable<double> > Phi(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "Phi",1)));
	d_Phi_id = vdb->registerVariableAndContext(Phi ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_Phi_id);
	std::shared_ptr< pdat::NodeVariable<double> > phix(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "phix",1)));
	d_phix_id = vdb->registerVariableAndContext(phix ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_phix_id);
	std::shared_ptr< pdat::NodeVariable<double> > phiy(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "phiy",1)));
	d_phiy_id = vdb->registerVariableAndContext(phiy ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_phiy_id);
	std::shared_ptr< pdat::NodeVariable<double> > K(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "K",1)));
	d_K_id = vdb->registerVariableAndContext(K ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_K_id);
	std::shared_ptr< pdat::NodeVariable<double> > rk1Alpha(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "rk1Alpha",1)));
	d_rk1Alpha_id = vdb->registerVariableAndContext(rk1Alpha ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<double> > rk1Phi(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "rk1Phi",1)));
	d_rk1Phi_id = vdb->registerVariableAndContext(rk1Phi ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<double> > rk1phix(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "rk1phix",1)));
	d_rk1phix_id = vdb->registerVariableAndContext(rk1phix ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<double> > rk1phiy(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "rk1phiy",1)));
	d_rk1phiy_id = vdb->registerVariableAndContext(rk1phiy ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<double> > rk1K(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "rk1K",1)));
	d_rk1K_id = vdb->registerVariableAndContext(rk1K ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<double> > rk2Alpha(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "rk2Alpha",1)));
	d_rk2Alpha_id = vdb->registerVariableAndContext(rk2Alpha ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<double> > rk2Phi(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "rk2Phi",1)));
	d_rk2Phi_id = vdb->registerVariableAndContext(rk2Phi ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<double> > rk2phix(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "rk2phix",1)));
	d_rk2phix_id = vdb->registerVariableAndContext(rk2phix ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<double> > rk2phiy(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "rk2phiy",1)));
	d_rk2phiy_id = vdb->registerVariableAndContext(rk2phiy ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<double> > rk2K(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "rk2K",1)));
	d_rk2K_id = vdb->registerVariableAndContext(rk2K ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<double> > Alpha_p(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "Alpha_p",1)));
	d_Alpha_p_id = vdb->registerVariableAndContext(Alpha_p ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<double> > Phi_p(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "Phi_p",1)));
	d_Phi_p_id = vdb->registerVariableAndContext(Phi_p ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<double> > phix_p(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "phix_p",1)));
	d_phix_p_id = vdb->registerVariableAndContext(phix_p ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<double> > phiy_p(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "phiy_p",1)));
	d_phiy_p_id = vdb->registerVariableAndContext(phiy_p ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	std::shared_ptr< pdat::NodeVariable<double> > K_p(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "K_p",1)));
	d_K_p_id = vdb->registerVariableAndContext(K_p ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));


	//Refine and coarse algorithms

	d_bdry_fill_init = std::shared_ptr< xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());
	d_bdry_post_coarsen = std::shared_ptr< xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());
	d_bdry_fill_advance2 = std::shared_ptr< xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());
	d_bdry_fill_advance8 = std::shared_ptr< xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());
	d_bdry_fill_advance14 = std::shared_ptr< xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());
	d_coarsen_algorithm = std::shared_ptr< xfer::CoarsenAlgorithm >(new xfer::CoarsenAlgorithm(d_dim));

	d_mapping_fill = std::shared_ptr< xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());
    d_tagging_fill = std::shared_ptr< xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());
	d_fill_new_level    = std::shared_ptr< xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());


	//mapping communication

	std::shared_ptr< hier::RefineOperator > refine_operator_map = d_grid_geometry->lookupRefineOperator(interior, "LINEAR_REFINE");
	d_mapping_fill->registerRefine(d_interior_regridding_value_id,d_interior_regridding_value_id,d_interior_regridding_value_id, refine_operator_map);
	d_mapping_fill->registerRefine(d_interior_i_id,d_interior_i_id,d_interior_i_id, refine_operator_map);
	d_mapping_fill->registerRefine(d_interior_j_id,d_interior_j_id,d_interior_j_id, refine_operator_map);
	d_mapping_fill->registerRefine(d_FOV_1_id,d_FOV_1_id,d_FOV_1_id, refine_operator_map);
	d_mapping_fill->registerRefine(d_FOV_xLower_id,d_FOV_xLower_id,d_FOV_xLower_id, refine_operator_map);
	d_mapping_fill->registerRefine(d_FOV_xUpper_id,d_FOV_xUpper_id,d_FOV_xUpper_id, refine_operator_map);
	d_mapping_fill->registerRefine(d_FOV_yLower_id,d_FOV_yLower_id,d_FOV_yLower_id, refine_operator_map);
	d_mapping_fill->registerRefine(d_FOV_yUpper_id,d_FOV_yUpper_id,d_FOV_yUpper_id, refine_operator_map);


    d_tagging_fill->registerRefine(d_nonSync_regridding_tag_id,d_nonSync_regridding_tag_id,d_nonSync_regridding_tag_id, d_grid_geometry->lookupRefineOperator(nonSync, "NO_REFINE"));

	//refine and coarsen operators
	string refine_op_name = "LINEAR_REFINE";
	int order = 0;
	if (database->isDatabase("regridding")) {
		regridding_db = database->getDatabase("regridding");
		if (regridding_db->isString("interpolator")) {
			refine_op_name = regridding_db->getString("interpolator");
			if (refine_op_name == "LINEAR_REFINE") {
				order = 1;
			}
			if (refine_op_name == "CUBIC_REFINE") {
				order = 3;
			}
			if (refine_op_name == "QUINTIC_REFINE") {
				order = 5;
			}
		}
	}
	std::shared_ptr< hier::RefineOperator > refine_operator, refine_operator_bound;
	std::shared_ptr< hier::CoarsenOperator > coarsen_operator = d_grid_geometry->lookupCoarsenOperator(FOV_1, "CONSTANT_COARSEN");
	if (order > 0) {
		std::shared_ptr< hier::RefineOperator > tmp_refine_operator(new LagrangianPolynomicRefine(false, order, d_patch_hierarchy, d_dim));
		refine_operator = tmp_refine_operator;
		std::shared_ptr< hier::RefineOperator > tmp_refine_operator_bound(new LagrangianPolynomicRefine(true, order, d_patch_hierarchy, d_dim));
		refine_operator_bound = tmp_refine_operator_bound;
	} else {
		refine_operator = d_grid_geometry->lookupRefineOperator(FOV_1, refine_op_name);
		refine_operator_bound = d_grid_geometry->lookupRefineOperator(FOV_1, refine_op_name);
	}

	std::shared_ptr<SAMRAI::hier::TimeInterpolateOperator> tio_mesh1(new TimeInterpolator(d_grid_geometry, "mesh"));
	time_interpolate_operator_mesh1 = std::dynamic_pointer_cast<TimeInterpolator>(tio_mesh1);


	//Register variables to the refineAlgorithm for boundaries

	if (d_refinedTimeStepping) {
		d_bdry_fill_advance2->registerRefine(d_rk1K_id,d_rk1K_id,d_K_p_id,d_rk1K_id,d_rk2K_id,d_K_id,d_rk1K_id,refine_operator,tio_mesh1);
		d_bdry_fill_advance2->registerRefine(d_rk1phiy_id,d_rk1phiy_id,d_phiy_p_id,d_rk1phiy_id,d_rk2phiy_id,d_phiy_id,d_rk1phiy_id,refine_operator,tio_mesh1);
		d_bdry_fill_advance2->registerRefine(d_rk1phix_id,d_rk1phix_id,d_phix_p_id,d_rk1phix_id,d_rk2phix_id,d_phix_id,d_rk1phix_id,refine_operator,tio_mesh1);
		d_bdry_fill_advance2->registerRefine(d_rk1Phi_id,d_rk1Phi_id,d_Phi_p_id,d_rk1Phi_id,d_rk2Phi_id,d_Phi_id,d_rk1Phi_id,refine_operator,tio_mesh1);
		d_bdry_fill_advance2->registerRefine(d_rk1Alpha_id,d_rk1Alpha_id,d_Alpha_p_id,d_rk1Alpha_id,d_rk2Alpha_id,d_Alpha_id,d_rk1Alpha_id,refine_operator,tio_mesh1);
	} else {
		d_bdry_fill_advance2->registerRefine(d_rk1K_id,d_rk1K_id,d_rk1K_id,refine_operator);
		d_bdry_fill_advance2->registerRefine(d_rk1phiy_id,d_rk1phiy_id,d_rk1phiy_id,refine_operator);
		d_bdry_fill_advance2->registerRefine(d_rk1phix_id,d_rk1phix_id,d_rk1phix_id,refine_operator);
		d_bdry_fill_advance2->registerRefine(d_rk1Phi_id,d_rk1Phi_id,d_rk1Phi_id,refine_operator);
		d_bdry_fill_advance2->registerRefine(d_rk1Alpha_id,d_rk1Alpha_id,d_rk1Alpha_id,refine_operator);
	}
	if (d_refinedTimeStepping) {
		d_bdry_fill_advance8->registerRefine(d_rk2K_id,d_rk2K_id,d_K_p_id,d_rk1K_id,d_rk2K_id,d_K_id,d_rk2K_id,refine_operator,tio_mesh1);
		d_bdry_fill_advance8->registerRefine(d_rk2phiy_id,d_rk2phiy_id,d_phiy_p_id,d_rk1phiy_id,d_rk2phiy_id,d_phiy_id,d_rk2phiy_id,refine_operator,tio_mesh1);
		d_bdry_fill_advance8->registerRefine(d_rk2phix_id,d_rk2phix_id,d_phix_p_id,d_rk1phix_id,d_rk2phix_id,d_phix_id,d_rk2phix_id,refine_operator,tio_mesh1);
		d_bdry_fill_advance8->registerRefine(d_rk2Phi_id,d_rk2Phi_id,d_Phi_p_id,d_rk1Phi_id,d_rk2Phi_id,d_Phi_id,d_rk2Phi_id,refine_operator,tio_mesh1);
		d_bdry_fill_advance8->registerRefine(d_rk2Alpha_id,d_rk2Alpha_id,d_Alpha_p_id,d_rk1Alpha_id,d_rk2Alpha_id,d_Alpha_id,d_rk2Alpha_id,refine_operator,tio_mesh1);
	} else {
		d_bdry_fill_advance8->registerRefine(d_rk2K_id,d_rk2K_id,d_rk2K_id,refine_operator);
		d_bdry_fill_advance8->registerRefine(d_rk2phiy_id,d_rk2phiy_id,d_rk2phiy_id,refine_operator);
		d_bdry_fill_advance8->registerRefine(d_rk2phix_id,d_rk2phix_id,d_rk2phix_id,refine_operator);
		d_bdry_fill_advance8->registerRefine(d_rk2Phi_id,d_rk2Phi_id,d_rk2Phi_id,refine_operator);
		d_bdry_fill_advance8->registerRefine(d_rk2Alpha_id,d_rk2Alpha_id,d_rk2Alpha_id,refine_operator);
	}
	if (d_refinedTimeStepping) {
		d_bdry_fill_advance14->registerRefine(d_K_id,d_K_id,d_K_p_id,d_rk1K_id,d_rk2K_id,d_K_id,d_K_id,refine_operator,tio_mesh1);
		d_bdry_fill_advance14->registerRefine(d_phiy_id,d_phiy_id,d_phiy_p_id,d_rk1phiy_id,d_rk2phiy_id,d_phiy_id,d_phiy_id,refine_operator,tio_mesh1);
		d_bdry_fill_advance14->registerRefine(d_phix_id,d_phix_id,d_phix_p_id,d_rk1phix_id,d_rk2phix_id,d_phix_id,d_phix_id,refine_operator,tio_mesh1);
		d_bdry_fill_advance14->registerRefine(d_Phi_id,d_Phi_id,d_Phi_p_id,d_rk1Phi_id,d_rk2Phi_id,d_Phi_id,d_Phi_id,refine_operator,tio_mesh1);
		d_bdry_fill_advance14->registerRefine(d_Alpha_id,d_Alpha_id,d_Alpha_p_id,d_rk1Alpha_id,d_rk2Alpha_id,d_Alpha_id,d_Alpha_id,refine_operator,tio_mesh1);
	} else {
		d_bdry_fill_advance14->registerRefine(d_K_id,d_K_id,d_K_id,refine_operator);
		d_bdry_fill_advance14->registerRefine(d_phiy_id,d_phiy_id,d_phiy_id,refine_operator);
		d_bdry_fill_advance14->registerRefine(d_phix_id,d_phix_id,d_phix_id,refine_operator);
		d_bdry_fill_advance14->registerRefine(d_Phi_id,d_Phi_id,d_Phi_id,refine_operator);
		d_bdry_fill_advance14->registerRefine(d_Alpha_id,d_Alpha_id,d_Alpha_id,refine_operator);
	}
	d_bdry_fill_init->registerRefine(d_Alpha_id,d_Alpha_id,d_Alpha_id,refine_operator);
	d_bdry_post_coarsen->registerRefine(d_Alpha_id,d_Alpha_id,d_Alpha_id,refine_operator);
	d_bdry_fill_init->registerRefine(d_Phi_id,d_Phi_id,d_Phi_id,refine_operator);
	d_bdry_post_coarsen->registerRefine(d_Phi_id,d_Phi_id,d_Phi_id,refine_operator);
	d_bdry_fill_init->registerRefine(d_phix_id,d_phix_id,d_phix_id,refine_operator);
	d_bdry_post_coarsen->registerRefine(d_phix_id,d_phix_id,d_phix_id,refine_operator);
	d_bdry_fill_init->registerRefine(d_phiy_id,d_phiy_id,d_phiy_id,refine_operator);
	d_bdry_post_coarsen->registerRefine(d_phiy_id,d_phiy_id,d_phiy_id,refine_operator);
	d_bdry_fill_init->registerRefine(d_K_id,d_K_id,d_K_id,refine_operator);
	d_bdry_post_coarsen->registerRefine(d_K_id,d_K_id,d_K_id,refine_operator);


	//Register variables to the refineAlgorithm for filling new levels on regridding
	d_fill_new_level->registerRefine(d_Alpha_id,d_Alpha_id,d_Alpha_id,refine_operator_bound);
	d_fill_new_level->registerRefine(d_Phi_id,d_Phi_id,d_Phi_id,refine_operator_bound);
	d_fill_new_level->registerRefine(d_phix_id,d_phix_id,d_phix_id,refine_operator_bound);
	d_fill_new_level->registerRefine(d_phiy_id,d_phiy_id,d_phiy_id,refine_operator_bound);
	d_fill_new_level->registerRefine(d_K_id,d_K_id,d_K_id,refine_operator_bound);


	//Register variables to the coarsenAlgorithm
	d_coarsen_algorithm->registerCoarsen(d_Alpha_id,d_Alpha_id,coarsen_operator);
	d_coarsen_algorithm->registerCoarsen(d_Phi_id,d_Phi_id,coarsen_operator);
	d_coarsen_algorithm->registerCoarsen(d_phix_id,d_phix_id,coarsen_operator);
	d_coarsen_algorithm->registerCoarsen(d_phiy_id,d_phiy_id,coarsen_operator);
	d_coarsen_algorithm->registerCoarsen(d_K_id,d_K_id,coarsen_operator);



    Commons::initialization();
}

/*
 * Destructor.
 */
Problem::~Problem() 
{
} 

/*
 * Initialize the data from a given level.
 */
void Problem::initializeLevelData (
   const std::shared_ptr<hier::PatchHierarchy >& hierarchy , 
   const int level_number ,
   const double init_data_time ,
   const bool can_be_refined ,
   const bool initial_time ,
   const std::shared_ptr<hier::PatchLevel >& old_level ,
   const bool allocate_data )
{
    cout<<"Initializing level "<<level_number<<endl;
    tbox::MemoryUtilities::printMemoryInfo(cout);   
	std::shared_ptr< hier::PatchLevel > level(hierarchy->getPatchLevel(level_number));

	// Allocate storage needed to initialize level and fill data from coarser levels in AMR hierarchy.  
	level->allocatePatchData(d_interior_regridding_value_id, init_data_time);
	level->allocatePatchData(d_nonSync_regridding_tag_id, init_data_time);
	level->allocatePatchData(d_interior_i_id, init_data_time);
	level->allocatePatchData(d_interior_j_id, init_data_time);
	level->allocatePatchData(d_FOV_1_id, init_data_time);
	level->allocatePatchData(d_FOV_xLower_id, init_data_time);
	level->allocatePatchData(d_FOV_xUpper_id, init_data_time);
	level->allocatePatchData(d_FOV_yLower_id, init_data_time);
	level->allocatePatchData(d_FOV_yUpper_id, init_data_time);
	level->allocatePatchData(d_Alpha_id, init_data_time);
	level->allocatePatchData(d_Phi_id, init_data_time);
	level->allocatePatchData(d_phix_id, init_data_time);
	level->allocatePatchData(d_phiy_id, init_data_time);
	level->allocatePatchData(d_K_id, init_data_time);
	level->allocatePatchData(d_rk1Alpha_id, init_data_time);
	level->allocatePatchData(d_rk1Phi_id, init_data_time);
	level->allocatePatchData(d_rk1phix_id, init_data_time);
	level->allocatePatchData(d_rk1phiy_id, init_data_time);
	level->allocatePatchData(d_rk1K_id, init_data_time);
	level->allocatePatchData(d_rk2Alpha_id, init_data_time);
	level->allocatePatchData(d_rk2Phi_id, init_data_time);
	level->allocatePatchData(d_rk2phix_id, init_data_time);
	level->allocatePatchData(d_rk2phiy_id, init_data_time);
	level->allocatePatchData(d_rk2K_id, init_data_time);
	level->allocatePatchData(d_Alpha_p_id, init_data_time);
	level->allocatePatchData(d_Phi_p_id, init_data_time);
	level->allocatePatchData(d_phix_p_id, init_data_time);
	level->allocatePatchData(d_phiy_p_id, init_data_time);
	level->allocatePatchData(d_K_p_id, init_data_time);
	level->allocatePatchData(d_mask_id, init_data_time);


	//Mapping the current data for new level.
	if (initial_time || level_number == 0) {
		mapDataOnPatch(init_data_time, initial_time, level_number, level);
	}

	//Fill a finer level with the data of the next coarse level.
	if ((level_number > 0) || old_level) {
		d_mapping_fill->createSchedule(level,old_level,level_number-1,hierarchy,this)->fillData(init_data_time, false);
		correctFOVS(level);
	}

	//Fill a finer level with the data of the next coarse level.
	if (!initial_time && ((level_number > 0) || old_level)) {
		d_fill_new_level->createSchedule(level,old_level,level_number-1,hierarchy,this)->fillData(init_data_time, false);
	}

	//Interphase mapping
	if (initial_time || level_number == 0) {
		interphaseMapping(init_data_time, initial_time, level_number, level, 1);
	}


	//Initialize current data for new level.
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr< hier::Patch > patch = *p_it;
		if (initial_time) {
  		    initializeDataOnPatch(*patch, init_data_time, initial_time);
		}

	}
	//Post-initialization Sync.
    	if (initial_time || level_number == 0) {

		//First synchronization from initialization
		d_bdry_fill_init->createSchedule(level,this)->fillData(init_data_time, false);
		double current_time = init_data_time;
		const double level_ratio = level->getRatioToCoarserLevel().max();
		double simPlat_dt = 0;
		//Last synchronization from initialization
		d_bdry_fill_init->createSchedule(level,this)->fillData(init_data_time, false);

    	}

    cout<<"Level "<<level_number<<" initialized"<<endl;
    tbox::MemoryUtilities::printMemoryInfo(cout);
}



void Problem::initializeLevelIntegrator(
   const std::shared_ptr<mesh::GriddingAlgorithmStrategy>& gridding_alg)
{
}

double Problem::getLevelDt(
   const std::shared_ptr<hier::PatchLevel>& level,
   const double dt_time,
   const bool initial_time)
{
  
   TBOX_ASSERT(level);

   if (level->getLevelNumber() == 0) return initial_dt;

    double dt = initial_dt;
    const hier::IntVector ratio = level->getRatioToLevelZero();
    double local_dt = dt;
    for (int i = 0; i < 2; i++) {
        if (local_dt > dt / ratio[i]) {
            local_dt = dt / ratio[i];
        }
    }
    return local_dt;
}

double Problem::getMaxFinerLevelDt(
   const int finer_level_number,
   const double coarse_dt,
   const hier::IntVector& ratio)
{
   NULL_USE(finer_level_number);

   TBOX_ASSERT(ratio.min() > 0);

   return coarse_dt / double(ratio.max());
}

void Problem::standardLevelSynchronization(
   const std::shared_ptr<hier::PatchHierarchy>& hierarchy,
   const int coarsest_level,
   const int finest_level,
   const double sync_time,
   const std::vector<double>& old_times)
{

}

void Problem::synchronizeNewLevels(
   const std::shared_ptr<hier::PatchHierarchy>& hierarchy,
   const int coarsest_level,
   const int finest_level,
   const double sync_time,
   const bool initial_time)
{

    //Not needed, but not absolutely sure
    /*for (int fine_ln = finest_level; fine_ln > coarsest_level; --fine_ln) {
        const int coarse_ln = fine_ln - 1;
        std::shared_ptr<hier::PatchLevel> fine_level(hierarchy->getPatchLevel(fine_ln));
        d_bdry_fill_init->createSchedule(fine_level, coarse_ln, hierarchy, this)->fillData(sync_time, true);
    }*/
}

void Problem::resetTimeDependentData(
   const std::shared_ptr<hier::PatchLevel>& level,
   const double new_time,
   const bool can_be_refined)
{
   TBOX_ASSERT(level);
   level->setTime(new_time);
}

void Problem::resetDataToPreadvanceState(
   const std::shared_ptr<hier::PatchLevel>& level)
{
    //cout<<"resetDataToPreadvanceState"<<endl;
}

/*
 * Map data on a patch. This mapping is done only at the begining of the simulation.
 */
void Problem::mapDataOnPatch(const double time, const bool initial_time, const int ln, const std::shared_ptr< hier::PatchLevel >& level)
{
	(void) time;
	const tbox::SAMRAI_MPI& mpi(tbox::SAMRAI_MPI::getSAMRAIWorld());
   	if (initial_time || ln == 0) {

		// Mapping		
		int i, iterm, previousMapi, iWallAcc;
		bool interiorMapi;
		double iMapStart, iMapEnd;
		int j, jterm, previousMapj, jWallAcc;
		bool interiorMapj;
		double jMapStart, jMapEnd;
		int minBlock[2], maxBlock[2], unionsI, facePointI, ie1, ie2, ie3, proc, pcounter, working, finished, pred;
		double maxDistance, e1, e2, e3;
		bool done, modif, workingArray[mpi.getSize()], finishedArray[mpi.getSize()], workingGlobal, finishedGlobal, workingPatchArray[level->getLocalNumberOfPatches()], finishedPatchArray[level->getLocalNumberOfPatches()], workingPatchGlobal, finishedPatchGlobal;
		int nodes = mpi.getSize();
		int patches = level->getLocalNumberOfPatches();

		double SQRT3INV = 1.0/sqrt(3.0);

		if (ln == 0) {
			//FOV initialization
			for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
				const std::shared_ptr< hier::Patch >& patch = *p_it;

				//Get the dimensions of the patch
				hier::Box pbox = patch->getBox();
				double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
				double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
				double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
				double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
				double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
				const hier::Index boxfirst = patch->getBox().lower();
				const hier::Index boxlast  = patch->getBox().upper();

				//Get delta spaces into an array. dx, dy, dz.
				const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
				const double* dx  = patch_geom->getDx();

				int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
				int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;

				for (i = 0; i < ilast; i++) {
					for (j = 0; j < jlast; j++) {
						vector(FOV_1, i, j) = 0;
						vector(FOV_xLower, i, j) = 0;
						vector(FOV_xUpper, i, j) = 0;
						vector(FOV_yLower, i, j) = 0;
						vector(FOV_yUpper, i, j) = 0;
					}
				}
			}
			d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);

			//Region: mainI
			for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
				const std::shared_ptr< hier::Patch >& patch = *p_it;

				//Get the dimensions of the patch
				hier::Box pbox = patch->getBox();
				double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
				double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
				double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
				double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
				double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
				double* interior_i = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_i_id).get())->getPointer();
				double* interior_j = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_j_id).get())->getPointer();
				double* interior = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_regridding_value_id).get())->getPointer();
				int* nonSync = ((pdat::NodeData<int> *) patch->getPatchData(d_nonSync_regridding_tag_id).get())->getPointer();
				const hier::Index boxfirst = patch->getBox().lower();
				const hier::Index boxlast  = patch->getBox().upper();

				//Get delta spaces into an array. dx, dy, dz.
				const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
				const double* dx  = patch_geom->getDx();

				int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
				int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;

				iMapStart = 0;
				iMapEnd = d_grid_geometry->getPhysicalDomain().front().numberCells()[0];
				jMapStart = 0;
				jMapEnd = d_grid_geometry->getPhysicalDomain().front().numberCells()[1];
				for (i = iMapStart; i <= iMapEnd; i++) {
					for (j = jMapStart; j <= jMapEnd; j++) {
						if (i >= boxfirst(0) - d_ghost_width && i <= boxlast(0) + d_ghost_width && j >= boxfirst(1) - d_ghost_width && j <= boxlast(1) + d_ghost_width) {
							vector(FOV_1, i - boxfirst(0) + d_ghost_width, j - boxfirst(1) + d_ghost_width) = 100;
							vector(FOV_xLower, i - boxfirst(0) + d_ghost_width, j - boxfirst(1) + d_ghost_width) = 0;
							vector(FOV_xUpper, i - boxfirst(0) + d_ghost_width, j - boxfirst(1) + d_ghost_width) = 0;
							vector(FOV_yLower, i - boxfirst(0) + d_ghost_width, j - boxfirst(1) + d_ghost_width) = 0;
							vector(FOV_yUpper, i - boxfirst(0) + d_ghost_width, j - boxfirst(1) + d_ghost_width) = 0;
						}
					}
				}
				//Check stencil
				for (j = 0; j < jlast; j++) {
					for (i = 0; i < ilast; i++) {
						vector(interior_i, i, j) = 0;
						vector(interior_j, i, j) = 0;
					}
				}
				for (j = 0; j < jlast; j++) {
					for (i = 0; i < ilast; i++) {
						if (vector(FOV_1, i, j) > 0) {
							setStencilLimits(patch, i, j, d_FOV_1_id);
						}
					}
				}
			}
			d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);
			for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
				const std::shared_ptr< hier::Patch >& patch = *p_it;

				//Get the dimensions of the patch
				hier::Box pbox = patch->getBox();
				double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
				double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
				double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
				double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
				double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
				double* interior_i = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_i_id).get())->getPointer();
				double* interior_j = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_j_id).get())->getPointer();
				double* interior = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_regridding_value_id).get())->getPointer();
				int* nonSync = ((pdat::NodeData<int> *) patch->getPatchData(d_nonSync_regridding_tag_id).get())->getPointer();
				const hier::Index boxfirst = patch->getBox().lower();
				const hier::Index boxlast  = patch->getBox().upper();

				//Get delta spaces into an array. dx, dy, dz.
				const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
				const double* dx  = patch_geom->getDx();

				int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
				int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;

				for (j = 0; j < jlast; j++) {
					for (i = 0; i < ilast; i++) {
						if ((abs(vector(interior_i, i, j)) > 1 || abs(vector(interior_j, i, j)) > 1) && (i < d_ghost_width || i >= ilast - d_ghost_width || j < d_ghost_width || j >= jlast - d_ghost_width)) {
							checkStencil(patch, i, j, d_FOV_1_id);
						}
					}
				}
				for (j = 0; j < jlast; j++) {
					for (i = 0; i < ilast; i++) {
						if (vector(interior_i, i, j) != 0 || vector(interior_j, i, j) != 0) {
							vector(FOV_1, i, j) = 100;
							vector(FOV_xLower, i, j) = 0;
							vector(FOV_xUpper, i, j) = 0;
							vector(FOV_yLower, i, j) = 0;
							vector(FOV_yUpper, i, j) = 0;
						}
					}
				}
			}
			d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);

		}
		//Boundaries Mapping
		for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
			const std::shared_ptr< hier::Patch >& patch = *p_it;

			//Get the dimensions of the patch
			hier::Box pbox = patch->getBox();
			double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
			double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
			double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
			double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
			double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
			const hier::Index boxfirst = patch->getBox().lower();
			const hier::Index boxlast  = patch->getBox().upper();

			//Get delta spaces into an array. dx, dy, dz.
			const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
			const double* dx  = patch_geom->getDx();

			int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
			int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;

			//y-Upper
			if (patch->getPatchGeometry()->getTouchesRegularBoundary(1, 1)) {
				for (j = jlast - d_ghost_width; j < jlast; j++) {
					for (i = 0; i < ilast; i++) {
						vector(FOV_yUpper, i, j) = 100;
						vector(FOV_1, i, j) = 0;
						vector(FOV_xLower, i, j) = 0;
						vector(FOV_xUpper, i, j) = 0;
						vector(FOV_yLower, i, j) = 0;
					}
				}
			}
			//y-Lower
			if (patch->getPatchGeometry()->getTouchesRegularBoundary(1, 0)) {
				for (j = 0; j < d_ghost_width; j++) {
					for (i = 0; i < ilast; i++) {
						vector(FOV_yLower, i, j) = 100;
						vector(FOV_1, i, j) = 0;
						vector(FOV_xLower, i, j) = 0;
						vector(FOV_xUpper, i, j) = 0;
						vector(FOV_yUpper, i, j) = 0;
					}
				}
			}
			//x-Upper
			if (patch->getPatchGeometry()->getTouchesRegularBoundary(0, 1)) {
				for (j = 0; j < jlast; j++) {
					for (i = ilast - d_ghost_width; i < ilast; i++) {
						vector(FOV_xUpper, i, j) = 100;
						vector(FOV_1, i, j) = 0;
						vector(FOV_xLower, i, j) = 0;
						vector(FOV_yLower, i, j) = 0;
						vector(FOV_yUpper, i, j) = 0;
					}
				}
			}
			//x-Lower
			if (patch->getPatchGeometry()->getTouchesRegularBoundary(0, 0)) {
				for (j = 0; j < jlast; j++) {
					for (i = 0; i < d_ghost_width; i++) {
						vector(FOV_xLower, i, j) = 100;
						vector(FOV_1, i, j) = 0;
						vector(FOV_xUpper, i, j) = 0;
						vector(FOV_yLower, i, j) = 0;
						vector(FOV_yUpper, i, j) = 0;
					}
				}
			}
		}
		d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);




   	}
}


/*
 * Sets the limit for the checkstencil routine
 */
void Problem::setStencilLimits(std::shared_ptr< hier::Patch > patch, int i, int j, int v) const {
	double* FOV = ((pdat::NodeData<double> *) patch->getPatchData(v).get())->getPointer();
	double* interior_i = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_i_id).get())->getPointer();
	double* interior_j = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_j_id).get())->getPointer();
	//Get the dimensions of the patch
	const hier::Index boxfirst = patch->getBox().lower();
	const hier::Index boxlast  = patch->getBox().upper();
	//Auxiliary definitions
	int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
	int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;

	int iStart, iEnd, currentGhosti, otherSideShifti, jStart, jEnd, currentGhostj, otherSideShiftj, shift;

	currentGhosti = d_ghost_width - 1;
	otherSideShifti = 0;
	currentGhostj = d_ghost_width - 1;
	otherSideShiftj = 0;
	//Checking width
	if ((i + 1 < ilast && vector(FOV, i + 1, j) == 0) ||  (i - 1 >= 0 && vector(FOV, i - 1, j) == 0)) {
		if (i + 1 < ilast && vector(FOV, i + 1, j) > 0) {
			bool stop_counting = false;
			for(int iti = i + 1; iti <= i + d_ghost_width - 1 && currentGhosti > 0; iti++) {
				if (iti < ilast  && vector(FOV, iti, j) > 0 && stop_counting == false) {
					currentGhosti--;
				} else {
					//First not interior point found
					if (iti < ilast  && vector(FOV, iti, j) == 0) {
						stop_counting = true;
					}
					//Physical boundary reach
					if (iti >= ilast - 3 && patch->getPatchGeometry()->getTouchesRegularBoundary (0, 1)) {
						stop_counting = true;
						//Calculate the number of cells the limit cannot grow
						if (iti + currentGhosti/2 >= ilast - 3) {
							otherSideShifti = (iti  + currentGhosti/2) - (ilast - 3);
						}
					}
				}
			}
		}
		if (i - 1 >= 0 && vector(FOV, i - 1, j) > 0) {
			bool stop_counting = false;
			for(int iti = i - 1; iti >= i - d_ghost_width + 1 && currentGhosti > 0; iti--) {
				if (iti >= 0  && vector(FOV, iti, j) > 0) {
					currentGhosti--;
				} else {
					//First not interior point found
					if (iti >= 0 && vector(FOV, iti, j) == 0) {
						stop_counting = true;
					}
					//Physical boundary reach
					if (iti < 3 && patch->getPatchGeometry()->getTouchesRegularBoundary (0, 0)) {
						stop_counting = true;
						//calculate the number of cells the limit cannot grow
						if (iti  -  ((currentGhosti) - currentGhosti/2) < 3) {
							otherSideShifti = 3 - (iti  - ((currentGhosti) - currentGhosti/2));
						}
					}
				}
			}
		}
		if (currentGhosti > 0) {
			if (i + 1 < ilast && vector(FOV, i + 1, j) > 0) {
				shift = 0;
				if(patch->getPatchGeometry()->getTouchesRegularBoundary (0, 0)) {
					while(i - ((currentGhosti) - currentGhosti/2) + shift < 3) {
						shift++;
					}
				}
				iStart = (currentGhosti - currentGhosti/2) - shift - otherSideShifti;
				iEnd = 0;
			} else {
				if (i - 1 >= 0 && vector(FOV, i - 1, j) > 0) {
					shift = 0;
					if(patch->getPatchGeometry()->getTouchesRegularBoundary (0, 1)) {
						while(i + currentGhosti/2 + shift >= ilast - 3) {
							shift--;
						}
					}
					iStart = 0;
					iEnd = currentGhosti/2 + shift + otherSideShifti;
				} else {
					shift = 0;
					if(patch->getPatchGeometry()->getTouchesRegularBoundary (0, 0)) {
						while(i - ((currentGhosti) - currentGhosti/2) + shift < 3) {
							shift++;
						}
					}
					if(patch->getPatchGeometry()->getTouchesRegularBoundary (0, 1)) {
						while(i + currentGhosti/2 + shift >= ilast - 3) {
							shift--;
						}
					}
					iStart = (currentGhosti - currentGhosti/2) - shift;
					iEnd = currentGhosti/2 + shift;
				}
			}
		} else {
			iStart = 0;
			iEnd = 0;
		}
	} else {
		iStart = 0;
		iEnd = 0;
	}
	if ((j + 1 < jlast && vector(FOV, i, j + 1) == 0) ||  (j - 1 >= 0 && vector(FOV, i, j - 1) == 0)) {
		if (j + 1 < jlast && vector(FOV, i, j + 1) > 0) {
			bool stop_counting = false;
			for(int itj = j + 1; itj <= j + d_ghost_width - 1 && currentGhostj > 0; itj++) {
				if (itj < jlast  && vector(FOV, i, itj) > 0 && stop_counting == false) {
					currentGhostj--;
				} else {
					//First not interior point found
					if (itj < jlast  && vector(FOV, i, itj) == 0) {
						stop_counting = true;
					}
					//Physical boundary reach
					if (itj >= jlast - 3 && patch->getPatchGeometry()->getTouchesRegularBoundary (1, 1)) {
						stop_counting = true;
						//Calculate the number of cells the limit cannot grow
						if (itj + currentGhostj/2 >= jlast - 3) {
							otherSideShiftj = (itj  + currentGhostj/2) - (jlast - 3);
						}
					}
				}
			}
		}
		if (j - 1 >= 0 && vector(FOV, i, j - 1) > 0) {
			bool stop_counting = false;
			for(int itj = j - 1; itj >= j - d_ghost_width + 1 && currentGhostj > 0; itj--) {
				if (itj >= 0  && vector(FOV, i, itj) > 0) {
					currentGhostj--;
				} else {
					//First not interior point found
					if (itj >= 0 && vector(FOV, i, itj) == 0) {
						stop_counting = true;
					}
					//Physical boundary reach
					if (itj < 3 && patch->getPatchGeometry()->getTouchesRegularBoundary (1, 0)) {
						stop_counting = true;
						//calculate the number of cells the limit cannot grow
						if (itj  -  ((currentGhostj) - currentGhostj/2) < 3) {
							otherSideShiftj = 3 - (itj  - ((currentGhostj) - currentGhostj/2));
						}
					}
				}
			}
		}
		if (currentGhostj > 0) {
			if (j + 1 < jlast && vector(FOV, i, j + 1) > 0) {
				shift = 0;
				if(patch->getPatchGeometry()->getTouchesRegularBoundary (1, 0)) {
					while(j - ((currentGhostj) - currentGhostj/2) + shift < 3) {
						shift++;
					}
				}
				jStart = (currentGhostj - currentGhostj/2) - shift - otherSideShiftj;
				jEnd = 0;
			} else {
				if (j - 1 >= 0 && vector(FOV, i, j - 1) > 0) {
					shift = 0;
					if(patch->getPatchGeometry()->getTouchesRegularBoundary (1, 1)) {
						while(j + currentGhostj/2 + shift >= jlast - 3) {
							shift--;
						}
					}
					jStart = 0;
					jEnd = currentGhostj/2 + shift + otherSideShiftj;
				} else {
					shift = 0;
					if(patch->getPatchGeometry()->getTouchesRegularBoundary (1, 0)) {
						while(j - ((currentGhostj) - currentGhostj/2) + shift < 3) {
							shift++;
						}
					}
					if(patch->getPatchGeometry()->getTouchesRegularBoundary (1, 1)) {
						while(j + currentGhostj/2 + shift >= jlast - 3) {
							shift--;
						}
					}
					jStart = (currentGhostj - currentGhostj/2) - shift;
					jEnd = currentGhostj/2 + shift;
				}
			}
		} else {
			jStart = 0;
			jEnd = 0;
		}
	} else {
		jStart = 0;
		jEnd = 0;
	}
	//Assigning stencil limits
	for(int iti = i - iStart; iti <= i + iEnd; iti++) {
		for(int itj = j - jStart; itj <= j + jEnd; itj++) {
			if(iti >= 0 && iti < ilast && itj >= 0 && itj < jlast && vector(FOV, iti, itj) == 0) {
				if (i - iti < 0) {
					vector(interior_i, iti, itj) = - (iStart + 1) - (i - iti);
				} else {
					vector(interior_i, iti, itj) = (iStart + 1) - (i - iti);
				}
				if (j - itj < 0) {
					vector(interior_j, iti, itj) = - (jStart + 1) - (j - itj);
				} else {
					vector(interior_j, iti, itj) = (jStart + 1) - (j - itj);
				}
			}
		}
	}
}

/*
 * Checks if the point has a stencil width
 */
void Problem::checkStencil(std::shared_ptr< hier::Patch > patch, int i, int j, int v) const {
	double* FOV = ((pdat::NodeData<double> *) patch->getPatchData(v).get())->getPointer();
	double* interior_i = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_i_id).get())->getPointer();
	double* interior_j = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_j_id).get())->getPointer();
	//Get the dimensions of the patch
	const hier::Index boxfirst = patch->getBox().lower();
	const hier::Index boxlast  = patch->getBox().upper();
	//Auxiliary definitions
	int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
	int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;

	int i_i = vector(interior_i, i, j);
	int iStart = MAX(0, i_i) - 1;
	int iEnd = MAX(0, -i_i) - 1;
	int i_j = vector(interior_j, i, j);
	int jStart = MAX(0, i_j) - 1;
	int jEnd = MAX(0, -i_j) - 1;

	for(int iti = i - iStart; iti <= i + iEnd; iti++) {
		for(int itj = j - jStart; itj <= j + jEnd; itj++) {
			if(iti >= 0 && iti < ilast && itj >= 0 && itj < jlast && vector(FOV, iti, itj) == 0) {
				vector(interior_i, iti, itj) = i_i  - (i - iti);
				vector(interior_j, iti, itj) = i_j  - (j - itj);
			}
		}
	}
}


// Point class for the floodfill algorithm
class Point {
private:
public:
	int i, j;
};

void Problem::floodfill(std::shared_ptr< hier::Patch > patch, int i, int j, int pred, int seg) const {

	double* FOV;
	switch(seg) {
		case 1:
			FOV = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
		break;
	}
	int* nonSync = ((pdat::NodeData<int> *) patch->getPatchData(d_nonSync_regridding_tag_id).get())->getPointer();
	double* interior = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_regridding_value_id).get())->getPointer();
	//Get the dimensions of the patch
	const hier::Index boxfirst = patch->getBox().lower();
	const hier::Index boxlast  = patch->getBox().upper();
	//Auxiliary definitions
	int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
	int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;

	stack<Point> mystack;

	Point p;
	p.i = i;
	p.j = j;

	mystack.push(p);
	while(mystack.size() > 0) {
		p = mystack.top();
		mystack.pop();
		if (vector(nonSync, p.i, p.j) == 0) {
			vector(nonSync, p.i, p.j) = pred;
			vector(interior, p.i, p.j) = pred;
			if (pred == 2) {
				vector(FOV, p.i, p.j) = 100;
			}
			if (p.i - 1 >= 0 && vector(nonSync, p.i - 1, p.j) == 0) {
				Point np;
				np.i = p.i-1;
				np.j = p.j;
				mystack.push(np);
			}
			if (p.i + 1 < ilast && vector(nonSync, p.i + 1, p.j) == 0) {
				Point np;
				np.i = p.i+1;
				np.j = p.j;
				mystack.push(np);
			}
			if (p.j - 1 >= 0 && vector(nonSync, p.i, p.j - 1) == 0) {
				Point np;
				np.i = p.i;
				np.j = p.j-1;
				mystack.push(np);
			}
			if (p.j + 1 < jlast && vector(nonSync, p.i, p.j + 1) == 0) {
				Point np;
				np.i = p.i;
				np.j = p.j+1;
				mystack.push(np);
			}
		}
	}
}


/*
 * FOV correction for AMR
 */
void Problem::correctFOVS(const std::shared_ptr< hier::PatchLevel >& level) {
	int i, j, k;
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr< hier::Patch >& patch = *p_it;

		//Get the dimensions of the patch
		hier::Box pbox = patch->getBox();
		double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
		const hier::Index boxfirst = patch->getBox().lower();
		const hier::Index boxlast  = patch->getBox().upper();

		//Get delta spaces into an array. dx, dy, dz.
		const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
		const double* dx  = patch_geom->getDx();

		int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
		int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;

		for (i = 0; i < ilast; i++) {
			for (j = 0; j < jlast; j++) {
				if (vector(FOV_xLower, i, j) > 0) {
					vector(FOV_xLower, i, j) = 100;
					vector(FOV_1, i, j) = 0;
					vector(FOV_xUpper, i, j) = 0;
					vector(FOV_yLower, i, j) = 0;
					vector(FOV_yUpper, i, j) = 0;
				}
				if (vector(FOV_xUpper, i, j) > 0) {
					vector(FOV_xUpper, i, j) = 100;
					vector(FOV_1, i, j) = 0;
					vector(FOV_xLower, i, j) = 0;
					vector(FOV_yLower, i, j) = 0;
					vector(FOV_yUpper, i, j) = 0;
				}
				if (vector(FOV_yLower, i, j) > 0) {
					vector(FOV_yLower, i, j) = 100;
					vector(FOV_1, i, j) = 0;
					vector(FOV_xLower, i, j) = 0;
					vector(FOV_xUpper, i, j) = 0;
					vector(FOV_yUpper, i, j) = 0;
				}
				if (vector(FOV_yUpper, i, j) > 0) {
					vector(FOV_yUpper, i, j) = 100;
					vector(FOV_1, i, j) = 0;
					vector(FOV_xLower, i, j) = 0;
					vector(FOV_xUpper, i, j) = 0;
					vector(FOV_yLower, i, j) = 0;
				}
				if (vector(FOV_1, i, j) > 0) {
					vector(FOV_1, i, j) = 100;
					vector(FOV_xLower, i, j) = 0;
					vector(FOV_xUpper, i, j) = 0;
					vector(FOV_yLower, i, j) = 0;
					vector(FOV_yUpper, i, j) = 0;
				}
			}
		}
	}
}




void Problem::interphaseMapping(const double time ,const bool initial_time,const int ln, const std::shared_ptr< hier::PatchLevel >& level, const int remesh) {
	const tbox::SAMRAI_MPI& mpi(tbox::SAMRAI_MPI::getSAMRAIWorld());
	if (remesh == 1) {
		//Update extrapolation variables
	}
}


/*
 * Checks if the point has to be stalled
 */
bool Problem::checkStalled(std::shared_ptr< hier::Patch > patch, int i, int j, int v) const {
	double* FOV = ((pdat::NodeData<double> *) patch->getPatchData(v).get())->getPointer();
	double* interior = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_regridding_value_id).get())->getPointer();
	//Get the dimensions of the patch
	const hier::Index boxfirst = patch->getBox().lower();
	const hier::Index boxlast  = patch->getBox().upper();
	//Auxiliary definitions
	int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
	int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;

	int stencilAcc, stencilAccMax_i, stencilAccMax_j;
	bool notEnoughStencil = false;
	int FOV_threshold = 0;
	if (vector(FOV, i, j) <= FOV_threshold) {
		notEnoughStencil = true;
	} else {
		stencilAcc = 0;
		stencilAccMax_i = 0;
		for (int it1 = MAX(i-d_regionMinThickness, 0); it1 <= MIN(i+d_regionMinThickness, ilast - 1); it1++) {
			if (vector(FOV, it1, j) > FOV_threshold) {
				stencilAcc++;
			} else {
				stencilAccMax_i = MAX(stencilAccMax_i, stencilAcc);
				stencilAcc = 0;
			}
		}
		stencilAccMax_i = MAX(stencilAccMax_i, stencilAcc);
		stencilAcc = 0;
		stencilAccMax_j = 0;
		for (int jt1 = MAX(j-d_regionMinThickness, 0); jt1 <= MIN(j+d_regionMinThickness, jlast - 1); jt1++) {
			if (vector(FOV, i, jt1) > FOV_threshold) {
				stencilAcc++;
			} else {
				stencilAccMax_j = MAX(stencilAccMax_j, stencilAcc);
				stencilAcc = 0;
			}
		}
		stencilAccMax_j = MAX(stencilAccMax_j, stencilAcc);
		if ((stencilAccMax_i < d_regionMinThickness) || (stencilAccMax_j < d_regionMinThickness)) {
			notEnoughStencil = true;
		}
	}
	return notEnoughStencil;
}






/*
 * Initialize data on a patch. This initialization is done only at the begining of the simulation.
 */
void Problem::initializeDataOnPatch(hier::Patch& patch, 
                                    const double time,
                                    const bool initial_time)
{
	(void) time;
   	if (initial_time) {
		// Initial conditions		
		//Get fields, auxiliary fields and local variables that are going to be used.
		double* Alpha = ((pdat::NodeData<double> *) patch.getPatchData(d_Alpha_id).get())->getPointer();
		((pdat::NodeData<double> *) patch.getPatchData(d_Alpha_id).get())->fillAll(0);
		double* Phi = ((pdat::NodeData<double> *) patch.getPatchData(d_Phi_id).get())->getPointer();
		((pdat::NodeData<double> *) patch.getPatchData(d_Phi_id).get())->fillAll(0);
		double* phix = ((pdat::NodeData<double> *) patch.getPatchData(d_phix_id).get())->getPointer();
		((pdat::NodeData<double> *) patch.getPatchData(d_phix_id).get())->fillAll(0);
		double* phiy = ((pdat::NodeData<double> *) patch.getPatchData(d_phiy_id).get())->getPointer();
		((pdat::NodeData<double> *) patch.getPatchData(d_phiy_id).get())->fillAll(0);
		double* K = ((pdat::NodeData<double> *) patch.getPatchData(d_K_id).get())->getPointer();
		((pdat::NodeData<double> *) patch.getPatchData(d_K_id).get())->fillAll(0);
		
		//Get the dimensions of the patch
		hier::Box pbox = patch.getBox();
		double* FOV_1 = ((pdat::NodeData<double> *) patch.getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch.getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch.getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch.getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch.getPatchData(d_FOV_yUpper_id).get())->getPointer();
		const hier::Index boxfirst = patch.getBox().lower();
		const hier::Index boxlast  = patch.getBox().upper();
		
		//Get delta spaces into an array. dx, dy, dz.
		const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch.getPatchGeometry()));
		const double* dx  = patch_geom->getDx();
		
		int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
		int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
		for(int j = 0; j < jlast; j++) {
			for(int i = 0; i < ilast; i++) {
				if (vector(FOV_1, i, j) > 0) {
					vector(Alpha, i, j) = 0.75 + 0.25 * agauss * exp((-(xcoord(i) * xcoord(i) + ycoord(j) * ycoord(j))) / bgauss);
					vector(Phi, i, j) = agauss * exp((-(xcoord(i) * xcoord(i) + ycoord(j) * ycoord(j))) / bgauss);
					vector(phix, i, j) = -2.0 * agauss / bgauss * xcoord(i) * exp((-(xcoord(i) * xcoord(i) + ycoord(j) * ycoord(j))) / bgauss);
					vector(phiy, i, j) = -2.0 * agauss / bgauss * ycoord(j) * exp((-(xcoord(i) * xcoord(i) + ycoord(j) * ycoord(j))) / bgauss);
					vector(K, i, j) = 0.0;
				}
		
			}
		}
		

   	}
}



/*
 * Gets the coarser patch that contains the box
 */
const std::shared_ptr<hier::Patch >& Problem::getCoarserPatch(
	const std::shared_ptr< hier::PatchLevel >& level,
	const hier::Box interior, 
	const hier::IntVector ratio)
{
	const hier::Box& coarsenBox = hier::Box::coarsen(interior, ratio);
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr< hier::Patch >& patch = *p_it;
		const hier::Box& interior_C = patch->getBox();
		if (interior_C.intersects(coarsenBox)) {
			return patch;		
		}
	}
    return NULL;
}

/*
 * Reset the hierarchy-dependent internal information.
 */
void Problem::resetHierarchyConfiguration (
   const std::shared_ptr<hier::PatchHierarchy >& new_hierarchy ,
   int coarsest_level ,
   int finest_level )
{
	int finest_hiera_level = new_hierarchy->getFinestLevelNumber();

   	//  If we have added or removed a level, resize the schedule arrays

	d_bdry_sched_advance2.resize(finest_hiera_level+1);
	d_bdry_sched_advance8.resize(finest_hiera_level+1);
	d_bdry_sched_advance14.resize(finest_hiera_level+1);
	d_coarsen_schedule.resize(finest_hiera_level+1);
	d_bdry_sched_postCoarsen.resize(finest_hiera_level+1);
	//  Build coarsen and refine communication schedules.
	for (int ln = coarsest_level; ln <= finest_hiera_level; ln++) {
		std::shared_ptr< hier::PatchLevel > level(new_hierarchy->getPatchLevel(ln));
		d_bdry_sched_advance2[ln] = d_bdry_fill_advance2->createSchedule(level,ln-1,new_hierarchy,this);
		d_bdry_sched_advance8[ln] = d_bdry_fill_advance8->createSchedule(level,ln-1,new_hierarchy,this);
		d_bdry_sched_advance14[ln] = d_bdry_fill_advance14->createSchedule(level,ln-1,new_hierarchy,this);
		d_bdry_sched_postCoarsen[ln] = d_bdry_post_coarsen->createSchedule(level);
		// coarsen schedule only for levels > 0
		if (ln > 0) {
			std::shared_ptr< hier::PatchLevel > coarser_level(new_hierarchy->getPatchLevel(ln-1));
			d_coarsen_schedule[ln] = d_coarsen_algorithm->createSchedule(coarser_level, level, NULL);
		}
	}

}



/*
 * This method sets the physical boundary conditions.
 */
void Problem::setPhysicalBoundaryConditions(
   hier::Patch& patch,
   const double fill_time,
   const hier::IntVector& ghost_width_to_fill)
{
	//Boundary must not be implemented in this method
}

/*
 * Set up external plotter to plot internal data from this class.
 * Register variables appropriate for plotting.
 */
int Problem::setupPlotterMesh(appu::VisItDataWriter &plotter) const {
	if (!d_patch_hierarchy) {
		TBOX_ERROR(d_object_name << ": No hierarchy inn"
			<< " Problem::setupPlottern"
			<< "The hierarchy must be set before callingn"
			<< "this function.n");
	}
	plotter.registerPlotQuantity("FOV_1","SCALAR",d_FOV_1_id);
	hier::VariableDatabase *vdb = hier::VariableDatabase::getDatabase();
	for (set<string>::const_iterator it = d_full_mesh_writer_variables.begin() ; it != d_full_mesh_writer_variables.end(); ++it) {
		string var_to_register = *it;
		if (!(vdb->checkVariableExists(var_to_register))) {
			TBOX_ERROR(d_object_name << ": Variable selected for 3D write not found:" <<  var_to_register);
		}
		int var_id = vdb->getVariable(var_to_register)->getInstanceIdentifier();
		plotter.registerPlotQuantity(var_to_register,"SCALAR",var_id);
	}
	return 0;
}
/*
 * Set up external plotter to plot integration data from this class.
 * Register variables appropriate for plotting.
 */
int Problem::setupIntegralPlotter(vector<std::shared_ptr<IntegrateDataWriter> > &plotters) const {
	if (!d_patch_hierarchy) {
		TBOX_ERROR(d_object_name << ": No hierarchy inn"
		<< " Problem::setupIntegralPlottern"
		<< "The hierarchy must be set before callingn"
		<< "this function.n");
	}
	int i = 0;
	for (vector<std::shared_ptr<IntegrateDataWriter> >::const_iterator it = plotters.begin(); it != plotters.end(); ++it) {
		std::shared_ptr<IntegrateDataWriter> plotter = *it;
		hier::VariableDatabase *vdb = hier::VariableDatabase::getDatabase();
		set<string> variables = d_integralVariables[i];
		for (set<string>::const_iterator it2 = variables.begin() ; it2 != variables.end(); ++it2) {
			string var_to_register = *it2;
			if (!(vdb->checkVariableExists(var_to_register))) {
				TBOX_ERROR(d_object_name << ": Variable selected for Integration not found:" <<  var_to_register);
			}
			int var_id = vdb->getVariable(var_to_register)->getInstanceIdentifier();
			plotter->registerPlotQuantity(var_to_register,"SCALAR",var_id);
		}
		i++;
	}
	return 0;
}
/*
 * Set up external plotter to plot point data from this class.
 * Register variables appropriate for plotting.
 */
int Problem::setupPointPlotter(vector<std::shared_ptr<PointDataWriter> > &plotters) const {
	int i = 0;
	for (vector<std::shared_ptr<PointDataWriter> >::const_iterator it = plotters.begin(); it != plotters.end(); ++it) {
		std::shared_ptr<PointDataWriter> plotter = *it;
		hier::VariableDatabase *vdb = hier::VariableDatabase::getDatabase();
		set<string> variables = d_pointVariables[i];
		for (set<string>::const_iterator it2 = variables.begin() ; it2 != variables.end(); ++it2) {
			string var_to_register = *it2;
			if (!(vdb->checkVariableExists(var_to_register))) {
				TBOX_ERROR(d_object_name << ": Variable selected for Point not found:" <<  var_to_register);
			}
			int var_id = vdb->getVariable(var_to_register)->getInstanceIdentifier();
			plotter->registerPlotQuantity(var_to_register,"SCALAR",var_id);
		}
		i++;
	}
	return 0;
}


/*
 * Perform a single step from the discretization schema algorithm   
 */
double Problem::advanceLevel(
   const std::shared_ptr<hier::PatchLevel>& level,
   const std::shared_ptr<hier::PatchHierarchy>& hierarchy,
   const double current_time,
   const double new_time,
   const bool first_step,
   const bool last_step,
   const bool regrid_advance)
{
	const tbox::SAMRAI_MPI& mpi(tbox::SAMRAI_MPI::getSAMRAIWorld());

	const int ln = level->getLevelNumber();
	const double simPlat_dt = new_time - current_time;
	const double level_ratio = level->getRatioToCoarserLevel().max();
	if (first_step) {
		bo_substep_iteration[ln] = 0;
	}
	else {
		bo_substep_iteration[ln] = bo_substep_iteration[ln] + 1;
	}
	time_interpolate_operator_mesh1->setRatio(level_ratio);
	time_interpolate_operator_mesh1->setStep(0);
	time_interpolate_operator_mesh1->setTimeSubstepNumber(bo_substep_iteration[ln]);

	if (d_refinedTimeStepping && first_step && ln > 0) {
		current_iteration[ln] = (current_iteration[ln - 1] - 1) * hierarchy->getRatioToCoarserLevel(ln).max() + 1;
	} else {
		current_iteration[ln] = current_iteration[ln] + 1;
	}
	int previous_iteration = current_iteration[ln] - 1;
	int outputCycle = current_iteration[ln];
	int maxLevels = hierarchy->getMaxNumberOfLevels();
	if (maxLevels > ln + 1) {
		int currentLevelNumber = ln;
		while (currentLevelNumber < maxLevels - 1) {
			int ratio = hierarchy->getRatioToCoarserLevel(currentLevelNumber + 1).max();
			outputCycle = outputCycle * ratio;
			previous_iteration = previous_iteration * ratio;
			currentLevelNumber++;
		}
	}

	t_step->start();
  	// Shifting time
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch > patch = *p_it;
		std::shared_ptr< pdat::NodeData<double> > Phi(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_Phi_id)));
		std::shared_ptr< pdat::NodeData<double> > Phi_p(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_Phi_p_id)));
		std::shared_ptr< pdat::NodeData<double> > phix(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_phix_id)));
		std::shared_ptr< pdat::NodeData<double> > phix_p(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_phix_p_id)));
		std::shared_ptr< pdat::NodeData<double> > Alpha(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_Alpha_id)));
		std::shared_ptr< pdat::NodeData<double> > Alpha_p(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_Alpha_p_id)));
		std::shared_ptr< pdat::NodeData<double> > K(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_K_id)));
		std::shared_ptr< pdat::NodeData<double> > K_p(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_K_p_id)));
		std::shared_ptr< pdat::NodeData<double> > phiy(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_phiy_id)));
		std::shared_ptr< pdat::NodeData<double> > phiy_p(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_phiy_p_id)));
		std::shared_ptr< pdat::NodeData<double> > rk1Phi(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_rk1Phi_id)));
		std::shared_ptr< pdat::NodeData<double> > rk1phix(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_rk1phix_id)));
		std::shared_ptr< pdat::NodeData<double> > rk1Alpha(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_rk1Alpha_id)));
		std::shared_ptr< pdat::NodeData<double> > rk1K(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_rk1K_id)));
		std::shared_ptr< pdat::NodeData<double> > rk1phiy(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_rk1phiy_id)));
		std::shared_ptr< pdat::NodeData<double> > rk2Phi(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_rk2Phi_id)));
		std::shared_ptr< pdat::NodeData<double> > rk2phix(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_rk2phix_id)));
		std::shared_ptr< pdat::NodeData<double> > rk2Alpha(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_rk2Alpha_id)));
		std::shared_ptr< pdat::NodeData<double> > rk2K(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_rk2K_id)));
		std::shared_ptr< pdat::NodeData<double> > rk2phiy(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_rk2phiy_id)));
		phiy_p->copy(*phiy);
		K_p->copy(*K);
		Alpha_p->copy(*Alpha);
		phix_p->copy(*phix);
		Phi_p->copy(*Phi);
		Phi_p->setTime(current_time);
		phix_p->setTime(current_time);
		Alpha_p->setTime(current_time);
		K_p->setTime(current_time);
		phiy_p->setTime(current_time);
		rk1Phi->setTime(current_time + simPlat_dt);
		rk1phix->setTime(current_time + simPlat_dt);
		rk1Alpha->setTime(current_time + simPlat_dt);
		rk1K->setTime(current_time + simPlat_dt);
		rk1phiy->setTime(current_time + simPlat_dt);
		rk2Phi->setTime(current_time + simPlat_dt * 0.5);
		rk2phix->setTime(current_time + simPlat_dt * 0.5);
		rk2Alpha->setTime(current_time + simPlat_dt * 0.5);
		rk2K->setTime(current_time + simPlat_dt * 0.5);
		rk2phiy->setTime(current_time + simPlat_dt * 0.5);
		Phi->setTime(current_time + simPlat_dt);
		phix->setTime(current_time + simPlat_dt);
		Alpha->setTime(current_time + simPlat_dt);
		K->setTime(current_time + simPlat_dt);
		phiy->setTime(current_time + simPlat_dt);
	}
  	// Evolution
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch >& patch = *p_it;
		double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
	
		double* Alpha_p = ((pdat::NodeData<double> *) patch->getPatchData(d_Alpha_p_id).get())->getPointer();
		double* K_p = ((pdat::NodeData<double> *) patch->getPatchData(d_K_p_id).get())->getPointer();
		double* phix_p = ((pdat::NodeData<double> *) patch->getPatchData(d_phix_p_id).get())->getPointer();
		double* phiy_p = ((pdat::NodeData<double> *) patch->getPatchData(d_phiy_p_id).get())->getPointer();
		double* Phi_p = ((pdat::NodeData<double> *) patch->getPatchData(d_Phi_p_id).get())->getPointer();
		double* rk1Alpha = ((pdat::NodeData<double> *) patch->getPatchData(d_rk1Alpha_id).get())->getPointer();
		double* rk1Phi = ((pdat::NodeData<double> *) patch->getPatchData(d_rk1Phi_id).get())->getPointer();
		double* rk1phix = ((pdat::NodeData<double> *) patch->getPatchData(d_rk1phix_id).get())->getPointer();
		double* rk1phiy = ((pdat::NodeData<double> *) patch->getPatchData(d_rk1phiy_id).get())->getPointer();
		double* rk1K = ((pdat::NodeData<double> *) patch->getPatchData(d_rk1K_id).get())->getPointer();
		double Fluxiphix, FluxiK, Speedi, Fluxm2iphix, Fluxm2iK, Fluxm1iphix, Fluxm1iK, Fluxp1iphix, Fluxp1iK, Fluxp2iphix, Fluxp2iK, LLFpiphix, LLFpiK, LLFniphix, LLFniK, LLFp_m2iphix, LLFp_m2iK, LLFn_m2iphix, LLFn_m2iK, LLFp_m1iphix, LLFp_m1iK, LLFn_m1iphix, LLFn_m1iK, LLFp_p1iphix, LLFp_p1iK, LLFn_p1iphix, LLFn_p1iK, LLFp_p2iphix, LLFp_p2iK, LLFn_p2iphix, LLFn_p2iK, Fluxjphiy, FluxjK, Speedj, Fluxm2jphiy, Fluxm2jK, Fluxm1jphiy, Fluxm1jK, Fluxp1jphiy, Fluxp1jK, Fluxp2jphiy, Fluxp2jK, LLFpjphiy, LLFpjK, LLFnjphiy, LLFnjK, LLFp_m2jphiy, LLFp_m2jK, LLFn_m2jphiy, LLFn_m2jK, LLFp_m1jphiy, LLFp_m1jK, LLFn_m1jphiy, LLFn_m1jK, LLFp_p1jphiy, LLFp_p1jK, LLFn_p1jphiy, LLFn_p1jK, LLFp_p2jphiy, LLFp_p2jK, LLFn_p2jphiy, LLFn_p2jK, taulniphix, taulniK, taulnjphiy, taulnjK, taurniphix, taurniK, taurnjphiy, taurnjK, p0lniphix, p0lniK, p0lnjphiy, p0lnjK, p1lniphix, p1lniK, p1lnjphiy, p1lnjK, p0rniphix, p0rniK, p0rnjphiy, p0rnjK, p1rniphix, p1rniK, p1rnjphiy, p1rnjK, beta0lniphix, beta0lniK, beta0lnjphiy, beta0lnjK, beta1lniphix, beta1lniK, beta1lnjphiy, beta1lnjK, beta0rniphix, beta0rniK, beta0rnjphiy, beta0rnjK, beta1rniphix, beta1rniK, beta1rnjphiy, beta1rnjK, w0lniphix, w0lniK, w0lnjphiy, w0lnjK, w1lniphix, w1lniK, w1lnjphiy, w1lnjK, w0rniphix, w0rniK, w0rnjphiy, w0rnjK, w1rniphix, w1rniK, w1rnjphiy, w1rnjK, wlniphix, wlniK, wlnjphiy, wlnjK, wrniphix, wrniK, wrnjphiy, wrnjK, phix_lni, K_lni, phiy_lnj, K_lnj, phix_rni, K_rni, phiy_rnj, K_rnj, p0lpiphix, p0lpiK, p0lpjphiy, p0lpjK, p1lpiphix, p1lpiK, p1lpjphiy, p1lpjK, p0rpiphix, p0rpiK, p0rpjphiy, p0rpjK, p1rpiphix, p1rpiK, p1rpjphiy, p1rpjK, beta0lpiphix, beta0lpiK, beta0lpjphiy, beta0lpjK, beta1lpiphix, beta1lpiK, beta1lpjphiy, beta1lpjK, beta0rpiphix, beta0rpiK, beta0rpjphiy, beta0rpjK, beta1rpiphix, beta1rpiK, beta1rpjphiy, beta1rpjK, taulpiphix, taulpiK, taulpjphiy, taulpjK, taurpiphix, taurpiK, taurpjphiy, taurpjK, w0lpiphix, w0lpiK, w0lpjphiy, w0lpjK, w1lpiphix, w1lpiK, w1lpjphiy, w1lpjK, w0rpiphix, w0rpiK, w0rpjphiy, w0rpjK, w1rpiphix, w1rpiK, w1rpjphiy, w1rpjK, wlpiphix, wlpiK, wlpjphiy, wlpjK, wrpiphix, wrpiK, wrpjphiy, wrpjK, phix_lpi, K_lpi, phiy_lpj, K_lpj, phix_rpi, K_rpi, phiy_rpj, K_rpj, Fluxniphix, FluxniK, Fluxnjphiy, FluxnjK, Fluxpiphix, FluxpiK, Fluxpjphiy, FluxpjK, RHS_Alpha, RHS_Phi, RHS_K, RHS_phix, RHS_phiy;
	
		//Get the dimensions of the patch
		const hier::Index boxfirst = patch->getBox().lower();
		const hier::Index boxlast = patch->getBox().upper();
	
		//Get delta spaces into an array. dx, dy, dz.
		std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
		const double* dx  = patch_geom->getDx();
	
		//Auxiliary definitions
		int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
		int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
		for(int j = 0; j < jlast; j++) {
			for(int i = 0; i < ilast; i++) {
				if ((i + 3 < ilast && i - 3 >= 0 && j + 3 < jlast && j - 3 >= 0)) {
					Fluxiphix = Fiphix_mainI(vector(Alpha_p, i, j), vector(K_p, i, j), dx, simPlat_dt, ilast, jlast);
					FluxiK = FiK_mainI(vector(Alpha_p, i, j), vector(phix_p, i, j), dx, simPlat_dt, ilast, jlast);
					Speedi = fabs(fabs(vector(Alpha_p, i, j)));
					Fluxm2iphix = Fiphix_mainI(vector(Alpha_p, i - 2, j), vector(K_p, i - 2, j), dx, simPlat_dt, ilast, jlast);
					Fluxm2iK = FiK_mainI(vector(Alpha_p, i - 2, j), vector(phix_p, i - 2, j), dx, simPlat_dt, ilast, jlast);
					Speedi = MAX(Speedi, fabs(fabs(vector(Alpha_p, i - 2, j))));
					Fluxm1iphix = Fiphix_mainI(vector(Alpha_p, i - 1, j), vector(K_p, i - 1, j), dx, simPlat_dt, ilast, jlast);
					Fluxm1iK = FiK_mainI(vector(Alpha_p, i - 1, j), vector(phix_p, i - 1, j), dx, simPlat_dt, ilast, jlast);
					Speedi = MAX(Speedi, fabs(fabs(vector(Alpha_p, i - 1, j))));
					Fluxp1iphix = Fiphix_mainI(vector(Alpha_p, i + 1, j), vector(K_p, i + 1, j), dx, simPlat_dt, ilast, jlast);
					Fluxp1iK = FiK_mainI(vector(Alpha_p, i + 1, j), vector(phix_p, i + 1, j), dx, simPlat_dt, ilast, jlast);
					Speedi = MAX(Speedi, fabs(fabs(vector(Alpha_p, i + 1, j))));
					Fluxp2iphix = Fiphix_mainI(vector(Alpha_p, i + 2, j), vector(K_p, i + 2, j), dx, simPlat_dt, ilast, jlast);
					Fluxp2iK = FiK_mainI(vector(Alpha_p, i + 2, j), vector(phix_p, i + 2, j), dx, simPlat_dt, ilast, jlast);
					Speedi = MAX(Speedi, fabs(fabs(vector(Alpha_p, i + 2, j))));
					LLFpiphix = 0.5 * (Fluxiphix + fabs(Speedi) * vector(phix_p, i, j));
					LLFpiK = 0.5 * (FluxiK + fabs(Speedi) * vector(K_p, i, j));
					LLFniphix = 0.5 * (Fluxiphix - fabs(Speedi) * vector(phix_p, i, j));
					LLFniK = 0.5 * (FluxiK - fabs(Speedi) * vector(K_p, i, j));
					LLFp_m2iphix = 0.5 * (Fluxm2iphix + fabs(Speedi) * vector(phix_p, i - 2, j));
					LLFp_m2iK = 0.5 * (Fluxm2iK + fabs(Speedi) * vector(K_p, i - 2, j));
					LLFn_m2iphix = 0.5 * (Fluxm2iphix - fabs(Speedi) * vector(phix_p, i - 2, j));
					LLFn_m2iK = 0.5 * (Fluxm2iK - fabs(Speedi) * vector(K_p, i - 2, j));
					LLFp_m1iphix = 0.5 * (Fluxm1iphix + fabs(Speedi) * vector(phix_p, i - 1, j));
					LLFp_m1iK = 0.5 * (Fluxm1iK + fabs(Speedi) * vector(K_p, i - 1, j));
					LLFn_m1iphix = 0.5 * (Fluxm1iphix - fabs(Speedi) * vector(phix_p, i - 1, j));
					LLFn_m1iK = 0.5 * (Fluxm1iK - fabs(Speedi) * vector(K_p, i - 1, j));
					LLFp_p1iphix = 0.5 * (Fluxp1iphix + fabs(Speedi) * vector(phix_p, i + 1, j));
					LLFp_p1iK = 0.5 * (Fluxp1iK + fabs(Speedi) * vector(K_p, i + 1, j));
					LLFn_p1iphix = 0.5 * (Fluxp1iphix - fabs(Speedi) * vector(phix_p, i + 1, j));
					LLFn_p1iK = 0.5 * (Fluxp1iK - fabs(Speedi) * vector(K_p, i + 1, j));
					LLFp_p2iphix = 0.5 * (Fluxp2iphix + fabs(Speedi) * vector(phix_p, i + 2, j));
					LLFp_p2iK = 0.5 * (Fluxp2iK + fabs(Speedi) * vector(K_p, i + 2, j));
					LLFn_p2iphix = 0.5 * (Fluxp2iphix - fabs(Speedi) * vector(phix_p, i + 2, j));
					LLFn_p2iK = 0.5 * (Fluxp2iK - fabs(Speedi) * vector(K_p, i + 2, j));
					Fluxjphiy = Fjphiy_mainI(vector(Alpha_p, i, j), vector(K_p, i, j), dx, simPlat_dt, ilast, jlast);
					FluxjK = FjK_mainI(vector(Alpha_p, i, j), vector(phiy_p, i, j), dx, simPlat_dt, ilast, jlast);
					Speedj = fabs(fabs(vector(Alpha_p, i, j)));
					Fluxm2jphiy = Fjphiy_mainI(vector(Alpha_p, i, j - 2), vector(K_p, i, j - 2), dx, simPlat_dt, ilast, jlast);
					Fluxm2jK = FjK_mainI(vector(Alpha_p, i, j - 2), vector(phiy_p, i, j - 2), dx, simPlat_dt, ilast, jlast);
					Speedj = MAX(Speedj, fabs(fabs(vector(Alpha_p, i, j - 2))));
					Fluxm1jphiy = Fjphiy_mainI(vector(Alpha_p, i, j - 1), vector(K_p, i, j - 1), dx, simPlat_dt, ilast, jlast);
					Fluxm1jK = FjK_mainI(vector(Alpha_p, i, j - 1), vector(phiy_p, i, j - 1), dx, simPlat_dt, ilast, jlast);
					Speedj = MAX(Speedj, fabs(fabs(vector(Alpha_p, i, j - 1))));
					Fluxp1jphiy = Fjphiy_mainI(vector(Alpha_p, i, j + 1), vector(K_p, i, j + 1), dx, simPlat_dt, ilast, jlast);
					Fluxp1jK = FjK_mainI(vector(Alpha_p, i, j + 1), vector(phiy_p, i, j + 1), dx, simPlat_dt, ilast, jlast);
					Speedj = MAX(Speedj, fabs(fabs(vector(Alpha_p, i, j + 1))));
					Fluxp2jphiy = Fjphiy_mainI(vector(Alpha_p, i, j + 2), vector(K_p, i, j + 2), dx, simPlat_dt, ilast, jlast);
					Fluxp2jK = FjK_mainI(vector(Alpha_p, i, j + 2), vector(phiy_p, i, j + 2), dx, simPlat_dt, ilast, jlast);
					Speedj = MAX(Speedj, fabs(fabs(vector(Alpha_p, i, j + 2))));
					LLFpjphiy = 0.5 * (Fluxjphiy + fabs(Speedj) * vector(phiy_p, i, j));
					LLFpjK = 0.5 * (FluxjK + fabs(Speedj) * vector(K_p, i, j));
					LLFnjphiy = 0.5 * (Fluxjphiy - fabs(Speedj) * vector(phiy_p, i, j));
					LLFnjK = 0.5 * (FluxjK - fabs(Speedj) * vector(K_p, i, j));
					LLFp_m2jphiy = 0.5 * (Fluxm2jphiy + fabs(Speedj) * vector(phiy_p, i, j - 2));
					LLFp_m2jK = 0.5 * (Fluxm2jK + fabs(Speedj) * vector(K_p, i, j - 2));
					LLFn_m2jphiy = 0.5 * (Fluxm2jphiy - fabs(Speedj) * vector(phiy_p, i, j - 2));
					LLFn_m2jK = 0.5 * (Fluxm2jK - fabs(Speedj) * vector(K_p, i, j - 2));
					LLFp_m1jphiy = 0.5 * (Fluxm1jphiy + fabs(Speedj) * vector(phiy_p, i, j - 1));
					LLFp_m1jK = 0.5 * (Fluxm1jK + fabs(Speedj) * vector(K_p, i, j - 1));
					LLFn_m1jphiy = 0.5 * (Fluxm1jphiy - fabs(Speedj) * vector(phiy_p, i, j - 1));
					LLFn_m1jK = 0.5 * (Fluxm1jK - fabs(Speedj) * vector(K_p, i, j - 1));
					LLFp_p1jphiy = 0.5 * (Fluxp1jphiy + fabs(Speedj) * vector(phiy_p, i, j + 1));
					LLFp_p1jK = 0.5 * (Fluxp1jK + fabs(Speedj) * vector(K_p, i, j + 1));
					LLFn_p1jphiy = 0.5 * (Fluxp1jphiy - fabs(Speedj) * vector(phiy_p, i, j + 1));
					LLFn_p1jK = 0.5 * (Fluxp1jK - fabs(Speedj) * vector(K_p, i, j + 1));
					LLFp_p2jphiy = 0.5 * (Fluxp2jphiy + fabs(Speedj) * vector(phiy_p, i, j + 2));
					LLFp_p2jK = 0.5 * (Fluxp2jK + fabs(Speedj) * vector(K_p, i, j + 2));
					LLFn_p2jphiy = 0.5 * (Fluxp2jphiy - fabs(Speedj) * vector(phiy_p, i, j + 2));
					LLFn_p2jK = 0.5 * (Fluxp2jK - fabs(Speedj) * vector(K_p, i, j + 2));
					taulniphix = fabs(LLFpiphix + (-2.0 * LLFp_m1iphix) + LLFp_m2iphix) * fabs(LLFpiphix + (-2.0 * LLFp_m1iphix) + LLFp_m2iphix);
					taulniK = fabs(LLFpiK + (-2.0 * LLFp_m1iK) + LLFp_m2iK) * fabs(LLFpiK + (-2.0 * LLFp_m1iK) + LLFp_m2iK);
					taulnjphiy = fabs(LLFpjphiy + (-2.0 * LLFp_m1jphiy) + LLFp_m2jphiy) * fabs(LLFpjphiy + (-2.0 * LLFp_m1jphiy) + LLFp_m2jphiy);
					taulnjK = fabs(LLFpjK + (-2.0 * LLFp_m1jK) + LLFp_m2jK) * fabs(LLFpjK + (-2.0 * LLFp_m1jK) + LLFp_m2jK);
					taurniphix = fabs(LLFn_p1iphix + (-2.0 * LLFniphix) + LLFn_m1iphix) * fabs(LLFn_p1iphix + (-2.0 * LLFniphix) + LLFn_m1iphix);
					taurniK = fabs(LLFn_p1iK + (-2.0 * LLFniK) + LLFn_m1iK) * fabs(LLFn_p1iK + (-2.0 * LLFniK) + LLFn_m1iK);
					taurnjphiy = fabs(LLFn_p1jphiy + (-2.0 * LLFnjphiy) + LLFn_m1jphiy) * fabs(LLFn_p1jphiy + (-2.0 * LLFnjphiy) + LLFn_m1jphiy);
					taurnjK = fabs(LLFn_p1jK + (-2.0 * LLFnjK) + LLFn_m1jK) * fabs(LLFn_p1jK + (-2.0 * LLFnjK) + LLFn_m1jK);
					p0lniphix = lor1(LLFp_m1iphix, LLFpiphix, dx, simPlat_dt, ilast, jlast);
					p0lniK = lor1(LLFp_m1iK, LLFpiK, dx, simPlat_dt, ilast, jlast);
					p0lnjphiy = lor1(LLFp_m1jphiy, LLFpjphiy, dx, simPlat_dt, ilast, jlast);
					p0lnjK = lor1(LLFp_m1jK, LLFpjK, dx, simPlat_dt, ilast, jlast);
					p1lniphix = lor0(LLFp_m2iphix, LLFp_m1iphix, dx, simPlat_dt, ilast, jlast);
					p1lniK = lor0(LLFp_m2iK, LLFp_m1iK, dx, simPlat_dt, ilast, jlast);
					p1lnjphiy = lor0(LLFp_m2jphiy, LLFp_m1jphiy, dx, simPlat_dt, ilast, jlast);
					p1lnjK = lor0(LLFp_m2jK, LLFp_m1jK, dx, simPlat_dt, ilast, jlast);
					p0rniphix = lor0(LLFn_p1iphix, LLFniphix, dx, simPlat_dt, ilast, jlast);
					p0rniK = lor0(LLFn_p1iK, LLFniK, dx, simPlat_dt, ilast, jlast);
					p0rnjphiy = lor0(LLFn_p1jphiy, LLFnjphiy, dx, simPlat_dt, ilast, jlast);
					p0rnjK = lor0(LLFn_p1jK, LLFnjK, dx, simPlat_dt, ilast, jlast);
					p1rniphix = lor1(LLFniphix, LLFn_m1iphix, dx, simPlat_dt, ilast, jlast);
					p1rniK = lor1(LLFniK, LLFn_m1iK, dx, simPlat_dt, ilast, jlast);
					p1rnjphiy = lor1(LLFnjphiy, LLFn_m1jphiy, dx, simPlat_dt, ilast, jlast);
					p1rnjK = lor1(LLFnjK, LLFn_m1jK, dx, simPlat_dt, ilast, jlast);
					beta0lniphix = smooth1(LLFp_m1iphix, LLFpiphix, dx, simPlat_dt, ilast, jlast);
					beta0lniK = smooth1(LLFp_m1iK, LLFpiK, dx, simPlat_dt, ilast, jlast);
					beta0lnjphiy = smooth1(LLFp_m1jphiy, LLFpjphiy, dx, simPlat_dt, ilast, jlast);
					beta0lnjK = smooth1(LLFp_m1jK, LLFpjK, dx, simPlat_dt, ilast, jlast);
					beta1lniphix = smooth0(LLFp_m2iphix, LLFp_m1iphix, dx, simPlat_dt, ilast, jlast);
					beta1lniK = smooth0(LLFp_m2iK, LLFp_m1iK, dx, simPlat_dt, ilast, jlast);
					beta1lnjphiy = smooth0(LLFp_m2jphiy, LLFp_m1jphiy, dx, simPlat_dt, ilast, jlast);
					beta1lnjK = smooth0(LLFp_m2jK, LLFp_m1jK, dx, simPlat_dt, ilast, jlast);
					beta0rniphix = smooth0(LLFn_p1iphix, LLFniphix, dx, simPlat_dt, ilast, jlast);
					beta0rniK = smooth0(LLFn_p1iK, LLFniK, dx, simPlat_dt, ilast, jlast);
					beta0rnjphiy = smooth0(LLFn_p1jphiy, LLFnjphiy, dx, simPlat_dt, ilast, jlast);
					beta0rnjK = smooth0(LLFn_p1jK, LLFnjK, dx, simPlat_dt, ilast, jlast);
					beta1rniphix = smooth1(LLFniphix, LLFn_m1iphix, dx, simPlat_dt, ilast, jlast);
					beta1rniK = smooth1(LLFniK, LLFn_m1iK, dx, simPlat_dt, ilast, jlast);
					beta1rnjphiy = smooth1(LLFnjphiy, LLFn_m1jphiy, dx, simPlat_dt, ilast, jlast);
					beta1rnjK = smooth1(LLFnjK, LLFn_m1jK, dx, simPlat_dt, ilast, jlast);
					w0lniphix = nlw1i(beta0lniphix, epsilon_main, taulniphix, dx, simPlat_dt, ilast, jlast);
					w0lniK = nlw1i(beta0lniK, epsilon_main, taulniK, dx, simPlat_dt, ilast, jlast);
					w0lnjphiy = nlw1j(beta0lnjphiy, epsilon_main, taulnjphiy, dx, simPlat_dt, ilast, jlast);
					w0lnjK = nlw1j(beta0lnjK, epsilon_main, taulnjK, dx, simPlat_dt, ilast, jlast);
					w1lniphix = nlw0i(beta1lniphix, epsilon_main, taulniphix, dx, simPlat_dt, ilast, jlast);
					w1lniK = nlw0i(beta1lniK, epsilon_main, taulniK, dx, simPlat_dt, ilast, jlast);
					w1lnjphiy = nlw0j(beta1lnjphiy, epsilon_main, taulnjphiy, dx, simPlat_dt, ilast, jlast);
					w1lnjK = nlw0j(beta1lnjK, epsilon_main, taulnjK, dx, simPlat_dt, ilast, jlast);
					w0rniphix = nlw0i(beta0rniphix, epsilon_main, taurniphix, dx, simPlat_dt, ilast, jlast);
					w0rniK = nlw0i(beta0rniK, epsilon_main, taurniK, dx, simPlat_dt, ilast, jlast);
					w0rnjphiy = nlw0j(beta0rnjphiy, epsilon_main, taurnjphiy, dx, simPlat_dt, ilast, jlast);
					w0rnjK = nlw0j(beta0rnjK, epsilon_main, taurnjK, dx, simPlat_dt, ilast, jlast);
					w1rniphix = nlw1i(beta1rniphix, epsilon_main, taurniphix, dx, simPlat_dt, ilast, jlast);
					w1rniK = nlw1i(beta1rniK, epsilon_main, taurniK, dx, simPlat_dt, ilast, jlast);
					w1rnjphiy = nlw1j(beta1rnjphiy, epsilon_main, taurnjphiy, dx, simPlat_dt, ilast, jlast);
					w1rnjK = nlw1j(beta1rnjK, epsilon_main, taurnjK, dx, simPlat_dt, ilast, jlast);
					wlniphix = w0lniphix + w1lniphix;
					wlniK = w0lniK + w1lniK;
					wlnjphiy = w0lnjphiy + w1lnjphiy;
					wlnjK = w0lnjK + w1lnjK;
					wrniphix = w0rniphix + w1rniphix;
					wrniK = w0rniK + w1rniK;
					wrnjphiy = w0rnjphiy + w1rnjphiy;
					wrnjK = w0rnjK + w1rnjK;
					w0lniphix = w0lniphix / wlniphix;
					w0lniK = w0lniK / wlniK;
					w0lnjphiy = w0lnjphiy / wlnjphiy;
					w0lnjK = w0lnjK / wlnjK;
					w1lniphix = w1lniphix / wlniphix;
					w1lniK = w1lniK / wlniK;
					w1lnjphiy = w1lnjphiy / wlnjphiy;
					w1lnjK = w1lnjK / wlnjK;
					w0rniphix = w0rniphix / wrniphix;
					w0rniK = w0rniK / wrniK;
					w0rnjphiy = w0rnjphiy / wrnjphiy;
					w0rnjK = w0rnjK / wrnjK;
					w1rniphix = w1rniphix / wrniphix;
					w1rniK = w1rniK / wrniK;
					w1rnjphiy = w1rnjphiy / wrnjphiy;
					w1rnjK = w1rnjK / wrnjK;
					phix_lni = weno(w0lniphix, w1lniphix, p0lniphix, p1lniphix, dx, simPlat_dt, ilast, jlast);
					K_lni = weno(w0lniK, w1lniK, p0lniK, p1lniK, dx, simPlat_dt, ilast, jlast);
					phiy_lnj = weno(w0lnjphiy, w1lnjphiy, p0lnjphiy, p1lnjphiy, dx, simPlat_dt, ilast, jlast);
					K_lnj = weno(w0lnjK, w1lnjK, p0lnjK, p1lnjK, dx, simPlat_dt, ilast, jlast);
					phix_rni = weno(w0rniphix, w1rniphix, p0rniphix, p1rniphix, dx, simPlat_dt, ilast, jlast);
					K_rni = weno(w0rniK, w1rniK, p0rniK, p1rniK, dx, simPlat_dt, ilast, jlast);
					phiy_rnj = weno(w0rnjphiy, w1rnjphiy, p0rnjphiy, p1rnjphiy, dx, simPlat_dt, ilast, jlast);
					K_rnj = weno(w0rnjK, w1rnjK, p0rnjK, p1rnjK, dx, simPlat_dt, ilast, jlast);
					p0lpiphix = lor1(LLFpiphix, LLFp_p1iphix, dx, simPlat_dt, ilast, jlast);
					p0lpiK = lor1(LLFpiK, LLFp_p1iK, dx, simPlat_dt, ilast, jlast);
					p0lpjphiy = lor1(LLFpjphiy, LLFp_p1jphiy, dx, simPlat_dt, ilast, jlast);
					p0lpjK = lor1(LLFpjK, LLFp_p1jK, dx, simPlat_dt, ilast, jlast);
					p1lpiphix = lor0(LLFp_m1iphix, LLFpiphix, dx, simPlat_dt, ilast, jlast);
					p1lpiK = lor0(LLFp_m1iK, LLFpiK, dx, simPlat_dt, ilast, jlast);
					p1lpjphiy = lor0(LLFp_m1jphiy, LLFpjphiy, dx, simPlat_dt, ilast, jlast);
					p1lpjK = lor0(LLFp_m1jK, LLFpjK, dx, simPlat_dt, ilast, jlast);
					p0rpiphix = lor0(LLFn_p2iphix, LLFn_p1iphix, dx, simPlat_dt, ilast, jlast);
					p0rpiK = lor0(LLFn_p2iK, LLFn_p1iK, dx, simPlat_dt, ilast, jlast);
					p0rpjphiy = lor0(LLFn_p2jphiy, LLFn_p1jphiy, dx, simPlat_dt, ilast, jlast);
					p0rpjK = lor0(LLFn_p2jK, LLFn_p1jK, dx, simPlat_dt, ilast, jlast);
					p1rpiphix = lor1(LLFn_p1iphix, LLFniphix, dx, simPlat_dt, ilast, jlast);
					p1rpiK = lor1(LLFn_p1iK, LLFniK, dx, simPlat_dt, ilast, jlast);
					p1rpjphiy = lor1(LLFn_p1jphiy, LLFnjphiy, dx, simPlat_dt, ilast, jlast);
					p1rpjK = lor1(LLFn_p1jK, LLFnjK, dx, simPlat_dt, ilast, jlast);
					beta0lpiphix = smooth1(LLFpiphix, LLFp_p1iphix, dx, simPlat_dt, ilast, jlast);
					beta0lpiK = smooth1(LLFpiK, LLFp_p1iK, dx, simPlat_dt, ilast, jlast);
					beta0lpjphiy = smooth1(LLFpjphiy, LLFp_p1jphiy, dx, simPlat_dt, ilast, jlast);
					beta0lpjK = smooth1(LLFpjK, LLFp_p1jK, dx, simPlat_dt, ilast, jlast);
					beta1lpiphix = smooth0(LLFp_m1iphix, LLFpiphix, dx, simPlat_dt, ilast, jlast);
					beta1lpiK = smooth0(LLFp_m1iK, LLFpiK, dx, simPlat_dt, ilast, jlast);
					beta1lpjphiy = smooth0(LLFp_m1jphiy, LLFpjphiy, dx, simPlat_dt, ilast, jlast);
					beta1lpjK = smooth0(LLFp_m1jK, LLFpjK, dx, simPlat_dt, ilast, jlast);
					beta0rpiphix = smooth0(LLFn_p2iphix, LLFn_p1iphix, dx, simPlat_dt, ilast, jlast);
					beta0rpiK = smooth0(LLFn_p2iK, LLFn_p1iK, dx, simPlat_dt, ilast, jlast);
					beta0rpjphiy = smooth0(LLFn_p2jphiy, LLFn_p1jphiy, dx, simPlat_dt, ilast, jlast);
					beta0rpjK = smooth0(LLFn_p2jK, LLFn_p1jK, dx, simPlat_dt, ilast, jlast);
					beta1rpiphix = smooth1(LLFn_p1iphix, LLFniphix, dx, simPlat_dt, ilast, jlast);
					beta1rpiK = smooth1(LLFn_p1iK, LLFniK, dx, simPlat_dt, ilast, jlast);
					beta1rpjphiy = smooth1(LLFn_p1jphiy, LLFnjphiy, dx, simPlat_dt, ilast, jlast);
					beta1rpjK = smooth1(LLFn_p1jK, LLFnjK, dx, simPlat_dt, ilast, jlast);
					taulpiphix = fabs(LLFp_p1iphix + (-2.0 * LLFpiphix) + LLFp_m1iphix) * fabs(LLFp_p1iphix + (-2.0 * LLFpiphix) + LLFp_m1iphix);
					taulpiK = fabs(LLFp_p1iK + (-2.0 * LLFpiK) + LLFp_m1iK) * fabs(LLFp_p1iK + (-2.0 * LLFpiK) + LLFp_m1iK);
					taulpjphiy = fabs(LLFp_p1jphiy + (-2.0 * LLFpjphiy) + LLFp_m1jphiy) * fabs(LLFp_p1jphiy + (-2.0 * LLFpjphiy) + LLFp_m1jphiy);
					taulpjK = fabs(LLFp_p1jK + (-2.0 * LLFpjK) + LLFp_m1jK) * fabs(LLFp_p1jK + (-2.0 * LLFpjK) + LLFp_m1jK);
					taurpiphix = fabs(LLFn_p2iphix + (-2.0 * LLFn_p1iphix) + LLFniphix) * fabs(LLFn_p2iphix + (-2.0 * LLFn_p1iphix) + LLFniphix);
					taurpiK = fabs(LLFn_p2iK + (-2.0 * LLFn_p1iK) + LLFniK) * fabs(LLFn_p2iK + (-2.0 * LLFn_p1iK) + LLFniK);
					taurpjphiy = fabs(LLFn_p2jphiy + (-2.0 * LLFn_p1jphiy) + LLFnjphiy) * fabs(LLFn_p2jphiy + (-2.0 * LLFn_p1jphiy) + LLFnjphiy);
					taurpjK = fabs(LLFn_p2jK + (-2.0 * LLFn_p1jK) + LLFnjK) * fabs(LLFn_p2jK + (-2.0 * LLFn_p1jK) + LLFnjK);
					w0lpiphix = nlw1i(beta0lpiphix, epsilon_main, taulpiphix, dx, simPlat_dt, ilast, jlast);
					w0lpiK = nlw1i(beta0lpiK, epsilon_main, taulpiK, dx, simPlat_dt, ilast, jlast);
					w0lpjphiy = nlw1j(beta0lpjphiy, epsilon_main, taulpjphiy, dx, simPlat_dt, ilast, jlast);
					w0lpjK = nlw1j(beta0lpjK, epsilon_main, taulpjK, dx, simPlat_dt, ilast, jlast);
					w1lpiphix = nlw0i(beta1lpiphix, epsilon_main, taulpiphix, dx, simPlat_dt, ilast, jlast);
					w1lpiK = nlw0i(beta1lpiK, epsilon_main, taulpiK, dx, simPlat_dt, ilast, jlast);
					w1lpjphiy = nlw0j(beta1lpjphiy, epsilon_main, taulpjphiy, dx, simPlat_dt, ilast, jlast);
					w1lpjK = nlw0j(beta1lpjK, epsilon_main, taulpjK, dx, simPlat_dt, ilast, jlast);
					w0rpiphix = nlw0i(beta0rpiphix, epsilon_main, taurpiphix, dx, simPlat_dt, ilast, jlast);
					w0rpiK = nlw0i(beta0rpiK, epsilon_main, taurpiK, dx, simPlat_dt, ilast, jlast);
					w0rpjphiy = nlw0j(beta0rpjphiy, epsilon_main, taurpjphiy, dx, simPlat_dt, ilast, jlast);
					w0rpjK = nlw0j(beta0rpjK, epsilon_main, taurpjK, dx, simPlat_dt, ilast, jlast);
					w1rpiphix = nlw1i(beta1rpiphix, epsilon_main, taurpiphix, dx, simPlat_dt, ilast, jlast);
					w1rpiK = nlw1i(beta1rpiK, epsilon_main, taurpiK, dx, simPlat_dt, ilast, jlast);
					w1rpjphiy = nlw1j(beta1rpjphiy, epsilon_main, taurpjphiy, dx, simPlat_dt, ilast, jlast);
					w1rpjK = nlw1j(beta1rpjK, epsilon_main, taurpjK, dx, simPlat_dt, ilast, jlast);
					wlpiphix = w0lpiphix + w1lpiphix;
					wlpiK = w0lpiK + w1lpiK;
					wlpjphiy = w0lpjphiy + w1lpjphiy;
					wlpjK = w0lpjK + w1lpjK;
					wrpiphix = w0rpiphix + w1rpiphix;
					wrpiK = w0rpiK + w1rpiK;
					wrpjphiy = w0rpjphiy + w1rpjphiy;
					wrpjK = w0rpjK + w1rpjK;
					w0lpiphix = w0lpiphix / wlpiphix;
					w0lpiK = w0lpiK / wlpiK;
					w0lpjphiy = w0lpjphiy / wlpjphiy;
					w0lpjK = w0lpjK / wlpjK;
					w1lpiphix = w1lpiphix / wlpiphix;
					w1lpiK = w1lpiK / wlpiK;
					w1lpjphiy = w1lpjphiy / wlpjphiy;
					w1lpjK = w1lpjK / wlpjK;
					w0rpiphix = w0rpiphix / wrpiphix;
					w0rpiK = w0rpiK / wrpiK;
					w0rpjphiy = w0rpjphiy / wrpjphiy;
					w0rpjK = w0rpjK / wrpjK;
					w1rpiphix = w1rpiphix / wrpiphix;
					w1rpiK = w1rpiK / wrpiK;
					w1rpjphiy = w1rpjphiy / wrpjphiy;
					w1rpjK = w1rpjK / wrpjK;
					phix_lpi = weno(w0lpiphix, w1lpiphix, p0lpiphix, p1lpiphix, dx, simPlat_dt, ilast, jlast);
					K_lpi = weno(w0lpiK, w1lpiK, p0lpiK, p1lpiK, dx, simPlat_dt, ilast, jlast);
					phiy_lpj = weno(w0lpjphiy, w1lpjphiy, p0lpjphiy, p1lpjphiy, dx, simPlat_dt, ilast, jlast);
					K_lpj = weno(w0lpjK, w1lpjK, p0lpjK, p1lpjK, dx, simPlat_dt, ilast, jlast);
					phix_rpi = weno(w0rpiphix, w1rpiphix, p0rpiphix, p1rpiphix, dx, simPlat_dt, ilast, jlast);
					K_rpi = weno(w0rpiK, w1rpiK, p0rpiK, p1rpiK, dx, simPlat_dt, ilast, jlast);
					phiy_rpj = weno(w0rpjphiy, w1rpjphiy, p0rpjphiy, p1rpjphiy, dx, simPlat_dt, ilast, jlast);
					K_rpj = weno(w0rpjK, w1rpjK, p0rpjK, p1rpjK, dx, simPlat_dt, ilast, jlast);
					Fluxniphix = phix_lni + phix_rni;
					FluxniK = K_lni + K_rni;
					Fluxnjphiy = phiy_lnj + phiy_rnj;
					FluxnjK = K_lnj + K_rnj;
					Fluxpiphix = phix_lpi + phix_rpi;
					FluxpiK = K_lpi + K_rpi;
					Fluxpjphiy = phiy_lpj + phiy_rpj;
					FluxpjK = K_lpj + K_rpj;
					RHS_Alpha = 0.0;
					RHS_Phi = -vector(Alpha_p, i, j) * vector(K_p, i, j);
					RHS_K = (m * m) * vector(Phi_p, i, j);
					RHS_phix = -FVM_i(Fluxpiphix, Fluxniphix, dx, simPlat_dt, ilast, jlast);
					RHS_phiy = -FVM_j(Fluxpjphiy, Fluxnjphiy, dx, simPlat_dt, ilast, jlast);
					RHS_K = RHS_K - FVM_i(FluxpiK, FluxniK, dx, simPlat_dt, ilast, jlast);
					RHS_K = RHS_K - FVM_j(FluxpjK, FluxnjK, dx, simPlat_dt, ilast, jlast);
					if (dissipation_factor_Alpha > 0.0) {
						RHS_Alpha = RHS_Alpha + dissipation_factor_Alpha * (meshDissipation_i(Alpha_p, i, j, dx, simPlat_dt, ilast, jlast) + meshDissipation_j(Alpha_p, i, j, dx, simPlat_dt, ilast, jlast));
					}
					if (dissipation_factor_Phi > 0.0) {
						RHS_Phi = RHS_Phi + dissipation_factor_Phi * (meshDissipation_i(Phi_p, i, j, dx, simPlat_dt, ilast, jlast) + meshDissipation_j(Phi_p, i, j, dx, simPlat_dt, ilast, jlast));
					}
					if (dissipation_factor_phix > 0.0) {
						RHS_phix = RHS_phix + dissipation_factor_phix * (meshDissipation_i(phix_p, i, j, dx, simPlat_dt, ilast, jlast) + meshDissipation_j(phix_p, i, j, dx, simPlat_dt, ilast, jlast));
					}
					if (dissipation_factor_phiy > 0.0) {
						RHS_phiy = RHS_phiy + dissipation_factor_phiy * (meshDissipation_i(phiy_p, i, j, dx, simPlat_dt, ilast, jlast) + meshDissipation_j(phiy_p, i, j, dx, simPlat_dt, ilast, jlast));
					}
					if (dissipation_factor_K > 0.0) {
						RHS_K = RHS_K + dissipation_factor_K * (meshDissipation_i(K_p, i, j, dx, simPlat_dt, ilast, jlast) + meshDissipation_j(K_p, i, j, dx, simPlat_dt, ilast, jlast));
					}
					vector(rk1Alpha, i, j) = RK3P1_(RHS_Alpha, vector(Alpha_p, i, j), 0.0, dx, simPlat_dt, ilast, jlast);
					vector(rk1Phi, i, j) = RK3P1_(RHS_Phi, vector(Phi_p, i, j), 0.0, dx, simPlat_dt, ilast, jlast);
					vector(rk1phix, i, j) = RK3P1_(RHS_phix, vector(phix_p, i, j), 0.0, dx, simPlat_dt, ilast, jlast);
					vector(rk1phiy, i, j) = RK3P1_(RHS_phiy, vector(phiy_p, i, j), 0.0, dx, simPlat_dt, ilast, jlast);
					vector(rk1K, i, j) = RK3P1_(RHS_K, vector(K_p, i, j), 0.0, dx, simPlat_dt, ilast, jlast);
				}
			}
		}
	}
	//Fill ghosts and periodical boundaries
	time_interpolate_operator_mesh1->setStep(1);
	d_bdry_sched_advance2[ln]->fillData(current_time + simPlat_dt, false);
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch >& patch = *p_it;
		double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
	
		double* rk1Alpha = ((pdat::NodeData<double> *) patch->getPatchData(d_rk1Alpha_id).get())->getPointer();
		double* rk1K = ((pdat::NodeData<double> *) patch->getPatchData(d_rk1K_id).get())->getPointer();
		double* rk1phix = ((pdat::NodeData<double> *) patch->getPatchData(d_rk1phix_id).get())->getPointer();
		double* rk1phiy = ((pdat::NodeData<double> *) patch->getPatchData(d_rk1phiy_id).get())->getPointer();
		double* rk1Phi = ((pdat::NodeData<double> *) patch->getPatchData(d_rk1Phi_id).get())->getPointer();
		double* rk2Alpha = ((pdat::NodeData<double> *) patch->getPatchData(d_rk2Alpha_id).get())->getPointer();
		double* Alpha_p = ((pdat::NodeData<double> *) patch->getPatchData(d_Alpha_p_id).get())->getPointer();
		double* rk2Phi = ((pdat::NodeData<double> *) patch->getPatchData(d_rk2Phi_id).get())->getPointer();
		double* Phi_p = ((pdat::NodeData<double> *) patch->getPatchData(d_Phi_p_id).get())->getPointer();
		double* rk2phix = ((pdat::NodeData<double> *) patch->getPatchData(d_rk2phix_id).get())->getPointer();
		double* phix_p = ((pdat::NodeData<double> *) patch->getPatchData(d_phix_p_id).get())->getPointer();
		double* rk2phiy = ((pdat::NodeData<double> *) patch->getPatchData(d_rk2phiy_id).get())->getPointer();
		double* phiy_p = ((pdat::NodeData<double> *) patch->getPatchData(d_phiy_p_id).get())->getPointer();
		double* rk2K = ((pdat::NodeData<double> *) patch->getPatchData(d_rk2K_id).get())->getPointer();
		double* K_p = ((pdat::NodeData<double> *) patch->getPatchData(d_K_p_id).get())->getPointer();
		double Fluxiphix, FluxiK, Speedi, Fluxm2iphix, Fluxm2iK, Fluxm1iphix, Fluxm1iK, Fluxp1iphix, Fluxp1iK, Fluxp2iphix, Fluxp2iK, LLFpiphix, LLFpiK, LLFniphix, LLFniK, LLFp_m2iphix, LLFp_m2iK, LLFn_m2iphix, LLFn_m2iK, LLFp_m1iphix, LLFp_m1iK, LLFn_m1iphix, LLFn_m1iK, LLFp_p1iphix, LLFp_p1iK, LLFn_p1iphix, LLFn_p1iK, LLFp_p2iphix, LLFp_p2iK, LLFn_p2iphix, LLFn_p2iK, Fluxjphiy, FluxjK, Speedj, Fluxm2jphiy, Fluxm2jK, Fluxm1jphiy, Fluxm1jK, Fluxp1jphiy, Fluxp1jK, Fluxp2jphiy, Fluxp2jK, LLFpjphiy, LLFpjK, LLFnjphiy, LLFnjK, LLFp_m2jphiy, LLFp_m2jK, LLFn_m2jphiy, LLFn_m2jK, LLFp_m1jphiy, LLFp_m1jK, LLFn_m1jphiy, LLFn_m1jK, LLFp_p1jphiy, LLFp_p1jK, LLFn_p1jphiy, LLFn_p1jK, LLFp_p2jphiy, LLFp_p2jK, LLFn_p2jphiy, LLFn_p2jK, taulniphix, taulniK, taulnjphiy, taulnjK, taurniphix, taurniK, taurnjphiy, taurnjK, p0lniphix, p0lniK, p0lnjphiy, p0lnjK, p1lniphix, p1lniK, p1lnjphiy, p1lnjK, p0rniphix, p0rniK, p0rnjphiy, p0rnjK, p1rniphix, p1rniK, p1rnjphiy, p1rnjK, beta0lniphix, beta0lniK, beta0lnjphiy, beta0lnjK, beta1lniphix, beta1lniK, beta1lnjphiy, beta1lnjK, beta0rniphix, beta0rniK, beta0rnjphiy, beta0rnjK, beta1rniphix, beta1rniK, beta1rnjphiy, beta1rnjK, w0lniphix, w0lniK, w0lnjphiy, w0lnjK, w1lniphix, w1lniK, w1lnjphiy, w1lnjK, w0rniphix, w0rniK, w0rnjphiy, w0rnjK, w1rniphix, w1rniK, w1rnjphiy, w1rnjK, wlniphix, wlniK, wlnjphiy, wlnjK, wrniphix, wrniK, wrnjphiy, wrnjK, phix_lni, K_lni, phiy_lnj, K_lnj, phix_rni, K_rni, phiy_rnj, K_rnj, p0lpiphix, p0lpiK, p0lpjphiy, p0lpjK, p1lpiphix, p1lpiK, p1lpjphiy, p1lpjK, p0rpiphix, p0rpiK, p0rpjphiy, p0rpjK, p1rpiphix, p1rpiK, p1rpjphiy, p1rpjK, beta0lpiphix, beta0lpiK, beta0lpjphiy, beta0lpjK, beta1lpiphix, beta1lpiK, beta1lpjphiy, beta1lpjK, beta0rpiphix, beta0rpiK, beta0rpjphiy, beta0rpjK, beta1rpiphix, beta1rpiK, beta1rpjphiy, beta1rpjK, taulpiphix, taulpiK, taulpjphiy, taulpjK, taurpiphix, taurpiK, taurpjphiy, taurpjK, w0lpiphix, w0lpiK, w0lpjphiy, w0lpjK, w1lpiphix, w1lpiK, w1lpjphiy, w1lpjK, w0rpiphix, w0rpiK, w0rpjphiy, w0rpjK, w1rpiphix, w1rpiK, w1rpjphiy, w1rpjK, wlpiphix, wlpiK, wlpjphiy, wlpjK, wrpiphix, wrpiK, wrpjphiy, wrpjK, phix_lpi, K_lpi, phiy_lpj, K_lpj, phix_rpi, K_rpi, phiy_rpj, K_rpj, Fluxniphix, FluxniK, Fluxnjphiy, FluxnjK, Fluxpiphix, FluxpiK, Fluxpjphiy, FluxpjK, RHS_Alpha, RHS_Phi, RHS_K, RHS_phix, RHS_phiy;
	
		//Get the dimensions of the patch
		const hier::Index boxfirst = patch->getBox().lower();
		const hier::Index boxlast = patch->getBox().upper();
	
		//Get delta spaces into an array. dx, dy, dz.
		std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
		const double* dx  = patch_geom->getDx();
	
		//Auxiliary definitions
		int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
		int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
		for(int j = 0; j < jlast; j++) {
			for(int i = 0; i < ilast; i++) {
				if ((i + 3 < ilast && i - 3 >= 0 && j + 3 < jlast && j - 3 >= 0)) {
					Fluxiphix = Fiphix_mainI(vector(rk1Alpha, i, j), vector(rk1K, i, j), dx, simPlat_dt, ilast, jlast);
					FluxiK = FiK_mainI(vector(rk1Alpha, i, j), vector(rk1phix, i, j), dx, simPlat_dt, ilast, jlast);
					Speedi = fabs(fabs(vector(rk1Alpha, i, j)));
					Fluxm2iphix = Fiphix_mainI(vector(rk1Alpha, i - 2, j), vector(rk1K, i - 2, j), dx, simPlat_dt, ilast, jlast);
					Fluxm2iK = FiK_mainI(vector(rk1Alpha, i - 2, j), vector(rk1phix, i - 2, j), dx, simPlat_dt, ilast, jlast);
					Speedi = MAX(Speedi, fabs(fabs(vector(rk1Alpha, i - 2, j))));
					Fluxm1iphix = Fiphix_mainI(vector(rk1Alpha, i - 1, j), vector(rk1K, i - 1, j), dx, simPlat_dt, ilast, jlast);
					Fluxm1iK = FiK_mainI(vector(rk1Alpha, i - 1, j), vector(rk1phix, i - 1, j), dx, simPlat_dt, ilast, jlast);
					Speedi = MAX(Speedi, fabs(fabs(vector(rk1Alpha, i - 1, j))));
					Fluxp1iphix = Fiphix_mainI(vector(rk1Alpha, i + 1, j), vector(rk1K, i + 1, j), dx, simPlat_dt, ilast, jlast);
					Fluxp1iK = FiK_mainI(vector(rk1Alpha, i + 1, j), vector(rk1phix, i + 1, j), dx, simPlat_dt, ilast, jlast);
					Speedi = MAX(Speedi, fabs(fabs(vector(rk1Alpha, i + 1, j))));
					Fluxp2iphix = Fiphix_mainI(vector(rk1Alpha, i + 2, j), vector(rk1K, i + 2, j), dx, simPlat_dt, ilast, jlast);
					Fluxp2iK = FiK_mainI(vector(rk1Alpha, i + 2, j), vector(rk1phix, i + 2, j), dx, simPlat_dt, ilast, jlast);
					Speedi = MAX(Speedi, fabs(fabs(vector(rk1Alpha, i + 2, j))));
					LLFpiphix = 0.5 * (Fluxiphix + fabs(Speedi) * vector(rk1phix, i, j));
					LLFpiK = 0.5 * (FluxiK + fabs(Speedi) * vector(rk1K, i, j));
					LLFniphix = 0.5 * (Fluxiphix - fabs(Speedi) * vector(rk1phix, i, j));
					LLFniK = 0.5 * (FluxiK - fabs(Speedi) * vector(rk1K, i, j));
					LLFp_m2iphix = 0.5 * (Fluxm2iphix + fabs(Speedi) * vector(rk1phix, i - 2, j));
					LLFp_m2iK = 0.5 * (Fluxm2iK + fabs(Speedi) * vector(rk1K, i - 2, j));
					LLFn_m2iphix = 0.5 * (Fluxm2iphix - fabs(Speedi) * vector(rk1phix, i - 2, j));
					LLFn_m2iK = 0.5 * (Fluxm2iK - fabs(Speedi) * vector(rk1K, i - 2, j));
					LLFp_m1iphix = 0.5 * (Fluxm1iphix + fabs(Speedi) * vector(rk1phix, i - 1, j));
					LLFp_m1iK = 0.5 * (Fluxm1iK + fabs(Speedi) * vector(rk1K, i - 1, j));
					LLFn_m1iphix = 0.5 * (Fluxm1iphix - fabs(Speedi) * vector(rk1phix, i - 1, j));
					LLFn_m1iK = 0.5 * (Fluxm1iK - fabs(Speedi) * vector(rk1K, i - 1, j));
					LLFp_p1iphix = 0.5 * (Fluxp1iphix + fabs(Speedi) * vector(rk1phix, i + 1, j));
					LLFp_p1iK = 0.5 * (Fluxp1iK + fabs(Speedi) * vector(rk1K, i + 1, j));
					LLFn_p1iphix = 0.5 * (Fluxp1iphix - fabs(Speedi) * vector(rk1phix, i + 1, j));
					LLFn_p1iK = 0.5 * (Fluxp1iK - fabs(Speedi) * vector(rk1K, i + 1, j));
					LLFp_p2iphix = 0.5 * (Fluxp2iphix + fabs(Speedi) * vector(rk1phix, i + 2, j));
					LLFp_p2iK = 0.5 * (Fluxp2iK + fabs(Speedi) * vector(rk1K, i + 2, j));
					LLFn_p2iphix = 0.5 * (Fluxp2iphix - fabs(Speedi) * vector(rk1phix, i + 2, j));
					LLFn_p2iK = 0.5 * (Fluxp2iK - fabs(Speedi) * vector(rk1K, i + 2, j));
					Fluxjphiy = Fjphiy_mainI(vector(rk1Alpha, i, j), vector(rk1K, i, j), dx, simPlat_dt, ilast, jlast);
					FluxjK = FjK_mainI(vector(rk1Alpha, i, j), vector(rk1phiy, i, j), dx, simPlat_dt, ilast, jlast);
					Speedj = fabs(fabs(vector(rk1Alpha, i, j)));
					Fluxm2jphiy = Fjphiy_mainI(vector(rk1Alpha, i, j - 2), vector(rk1K, i, j - 2), dx, simPlat_dt, ilast, jlast);
					Fluxm2jK = FjK_mainI(vector(rk1Alpha, i, j - 2), vector(rk1phiy, i, j - 2), dx, simPlat_dt, ilast, jlast);
					Speedj = MAX(Speedj, fabs(fabs(vector(rk1Alpha, i, j - 2))));
					Fluxm1jphiy = Fjphiy_mainI(vector(rk1Alpha, i, j - 1), vector(rk1K, i, j - 1), dx, simPlat_dt, ilast, jlast);
					Fluxm1jK = FjK_mainI(vector(rk1Alpha, i, j - 1), vector(rk1phiy, i, j - 1), dx, simPlat_dt, ilast, jlast);
					Speedj = MAX(Speedj, fabs(fabs(vector(rk1Alpha, i, j - 1))));
					Fluxp1jphiy = Fjphiy_mainI(vector(rk1Alpha, i, j + 1), vector(rk1K, i, j + 1), dx, simPlat_dt, ilast, jlast);
					Fluxp1jK = FjK_mainI(vector(rk1Alpha, i, j + 1), vector(rk1phiy, i, j + 1), dx, simPlat_dt, ilast, jlast);
					Speedj = MAX(Speedj, fabs(fabs(vector(rk1Alpha, i, j + 1))));
					Fluxp2jphiy = Fjphiy_mainI(vector(rk1Alpha, i, j + 2), vector(rk1K, i, j + 2), dx, simPlat_dt, ilast, jlast);
					Fluxp2jK = FjK_mainI(vector(rk1Alpha, i, j + 2), vector(rk1phiy, i, j + 2), dx, simPlat_dt, ilast, jlast);
					Speedj = MAX(Speedj, fabs(fabs(vector(rk1Alpha, i, j + 2))));
					LLFpjphiy = 0.5 * (Fluxjphiy + fabs(Speedj) * vector(rk1phiy, i, j));
					LLFpjK = 0.5 * (FluxjK + fabs(Speedj) * vector(rk1K, i, j));
					LLFnjphiy = 0.5 * (Fluxjphiy - fabs(Speedj) * vector(rk1phiy, i, j));
					LLFnjK = 0.5 * (FluxjK - fabs(Speedj) * vector(rk1K, i, j));
					LLFp_m2jphiy = 0.5 * (Fluxm2jphiy + fabs(Speedj) * vector(rk1phiy, i, j - 2));
					LLFp_m2jK = 0.5 * (Fluxm2jK + fabs(Speedj) * vector(rk1K, i, j - 2));
					LLFn_m2jphiy = 0.5 * (Fluxm2jphiy - fabs(Speedj) * vector(rk1phiy, i, j - 2));
					LLFn_m2jK = 0.5 * (Fluxm2jK - fabs(Speedj) * vector(rk1K, i, j - 2));
					LLFp_m1jphiy = 0.5 * (Fluxm1jphiy + fabs(Speedj) * vector(rk1phiy, i, j - 1));
					LLFp_m1jK = 0.5 * (Fluxm1jK + fabs(Speedj) * vector(rk1K, i, j - 1));
					LLFn_m1jphiy = 0.5 * (Fluxm1jphiy - fabs(Speedj) * vector(rk1phiy, i, j - 1));
					LLFn_m1jK = 0.5 * (Fluxm1jK - fabs(Speedj) * vector(rk1K, i, j - 1));
					LLFp_p1jphiy = 0.5 * (Fluxp1jphiy + fabs(Speedj) * vector(rk1phiy, i, j + 1));
					LLFp_p1jK = 0.5 * (Fluxp1jK + fabs(Speedj) * vector(rk1K, i, j + 1));
					LLFn_p1jphiy = 0.5 * (Fluxp1jphiy - fabs(Speedj) * vector(rk1phiy, i, j + 1));
					LLFn_p1jK = 0.5 * (Fluxp1jK - fabs(Speedj) * vector(rk1K, i, j + 1));
					LLFp_p2jphiy = 0.5 * (Fluxp2jphiy + fabs(Speedj) * vector(rk1phiy, i, j + 2));
					LLFp_p2jK = 0.5 * (Fluxp2jK + fabs(Speedj) * vector(rk1K, i, j + 2));
					LLFn_p2jphiy = 0.5 * (Fluxp2jphiy - fabs(Speedj) * vector(rk1phiy, i, j + 2));
					LLFn_p2jK = 0.5 * (Fluxp2jK - fabs(Speedj) * vector(rk1K, i, j + 2));
					taulniphix = fabs(LLFpiphix + (-2.0 * LLFp_m1iphix) + LLFp_m2iphix) * fabs(LLFpiphix + (-2.0 * LLFp_m1iphix) + LLFp_m2iphix);
					taulniK = fabs(LLFpiK + (-2.0 * LLFp_m1iK) + LLFp_m2iK) * fabs(LLFpiK + (-2.0 * LLFp_m1iK) + LLFp_m2iK);
					taulnjphiy = fabs(LLFpjphiy + (-2.0 * LLFp_m1jphiy) + LLFp_m2jphiy) * fabs(LLFpjphiy + (-2.0 * LLFp_m1jphiy) + LLFp_m2jphiy);
					taulnjK = fabs(LLFpjK + (-2.0 * LLFp_m1jK) + LLFp_m2jK) * fabs(LLFpjK + (-2.0 * LLFp_m1jK) + LLFp_m2jK);
					taurniphix = fabs(LLFn_p1iphix + (-2.0 * LLFniphix) + LLFn_m1iphix) * fabs(LLFn_p1iphix + (-2.0 * LLFniphix) + LLFn_m1iphix);
					taurniK = fabs(LLFn_p1iK + (-2.0 * LLFniK) + LLFn_m1iK) * fabs(LLFn_p1iK + (-2.0 * LLFniK) + LLFn_m1iK);
					taurnjphiy = fabs(LLFn_p1jphiy + (-2.0 * LLFnjphiy) + LLFn_m1jphiy) * fabs(LLFn_p1jphiy + (-2.0 * LLFnjphiy) + LLFn_m1jphiy);
					taurnjK = fabs(LLFn_p1jK + (-2.0 * LLFnjK) + LLFn_m1jK) * fabs(LLFn_p1jK + (-2.0 * LLFnjK) + LLFn_m1jK);
					p0lniphix = lor1(LLFp_m1iphix, LLFpiphix, dx, simPlat_dt, ilast, jlast);
					p0lniK = lor1(LLFp_m1iK, LLFpiK, dx, simPlat_dt, ilast, jlast);
					p0lnjphiy = lor1(LLFp_m1jphiy, LLFpjphiy, dx, simPlat_dt, ilast, jlast);
					p0lnjK = lor1(LLFp_m1jK, LLFpjK, dx, simPlat_dt, ilast, jlast);
					p1lniphix = lor0(LLFp_m2iphix, LLFp_m1iphix, dx, simPlat_dt, ilast, jlast);
					p1lniK = lor0(LLFp_m2iK, LLFp_m1iK, dx, simPlat_dt, ilast, jlast);
					p1lnjphiy = lor0(LLFp_m2jphiy, LLFp_m1jphiy, dx, simPlat_dt, ilast, jlast);
					p1lnjK = lor0(LLFp_m2jK, LLFp_m1jK, dx, simPlat_dt, ilast, jlast);
					p0rniphix = lor0(LLFn_p1iphix, LLFniphix, dx, simPlat_dt, ilast, jlast);
					p0rniK = lor0(LLFn_p1iK, LLFniK, dx, simPlat_dt, ilast, jlast);
					p0rnjphiy = lor0(LLFn_p1jphiy, LLFnjphiy, dx, simPlat_dt, ilast, jlast);
					p0rnjK = lor0(LLFn_p1jK, LLFnjK, dx, simPlat_dt, ilast, jlast);
					p1rniphix = lor1(LLFniphix, LLFn_m1iphix, dx, simPlat_dt, ilast, jlast);
					p1rniK = lor1(LLFniK, LLFn_m1iK, dx, simPlat_dt, ilast, jlast);
					p1rnjphiy = lor1(LLFnjphiy, LLFn_m1jphiy, dx, simPlat_dt, ilast, jlast);
					p1rnjK = lor1(LLFnjK, LLFn_m1jK, dx, simPlat_dt, ilast, jlast);
					beta0lniphix = smooth1(LLFp_m1iphix, LLFpiphix, dx, simPlat_dt, ilast, jlast);
					beta0lniK = smooth1(LLFp_m1iK, LLFpiK, dx, simPlat_dt, ilast, jlast);
					beta0lnjphiy = smooth1(LLFp_m1jphiy, LLFpjphiy, dx, simPlat_dt, ilast, jlast);
					beta0lnjK = smooth1(LLFp_m1jK, LLFpjK, dx, simPlat_dt, ilast, jlast);
					beta1lniphix = smooth0(LLFp_m2iphix, LLFp_m1iphix, dx, simPlat_dt, ilast, jlast);
					beta1lniK = smooth0(LLFp_m2iK, LLFp_m1iK, dx, simPlat_dt, ilast, jlast);
					beta1lnjphiy = smooth0(LLFp_m2jphiy, LLFp_m1jphiy, dx, simPlat_dt, ilast, jlast);
					beta1lnjK = smooth0(LLFp_m2jK, LLFp_m1jK, dx, simPlat_dt, ilast, jlast);
					beta0rniphix = smooth0(LLFn_p1iphix, LLFniphix, dx, simPlat_dt, ilast, jlast);
					beta0rniK = smooth0(LLFn_p1iK, LLFniK, dx, simPlat_dt, ilast, jlast);
					beta0rnjphiy = smooth0(LLFn_p1jphiy, LLFnjphiy, dx, simPlat_dt, ilast, jlast);
					beta0rnjK = smooth0(LLFn_p1jK, LLFnjK, dx, simPlat_dt, ilast, jlast);
					beta1rniphix = smooth1(LLFniphix, LLFn_m1iphix, dx, simPlat_dt, ilast, jlast);
					beta1rniK = smooth1(LLFniK, LLFn_m1iK, dx, simPlat_dt, ilast, jlast);
					beta1rnjphiy = smooth1(LLFnjphiy, LLFn_m1jphiy, dx, simPlat_dt, ilast, jlast);
					beta1rnjK = smooth1(LLFnjK, LLFn_m1jK, dx, simPlat_dt, ilast, jlast);
					w0lniphix = nlw1i(beta0lniphix, epsilon_main, taulniphix, dx, simPlat_dt, ilast, jlast);
					w0lniK = nlw1i(beta0lniK, epsilon_main, taulniK, dx, simPlat_dt, ilast, jlast);
					w0lnjphiy = nlw1j(beta0lnjphiy, epsilon_main, taulnjphiy, dx, simPlat_dt, ilast, jlast);
					w0lnjK = nlw1j(beta0lnjK, epsilon_main, taulnjK, dx, simPlat_dt, ilast, jlast);
					w1lniphix = nlw0i(beta1lniphix, epsilon_main, taulniphix, dx, simPlat_dt, ilast, jlast);
					w1lniK = nlw0i(beta1lniK, epsilon_main, taulniK, dx, simPlat_dt, ilast, jlast);
					w1lnjphiy = nlw0j(beta1lnjphiy, epsilon_main, taulnjphiy, dx, simPlat_dt, ilast, jlast);
					w1lnjK = nlw0j(beta1lnjK, epsilon_main, taulnjK, dx, simPlat_dt, ilast, jlast);
					w0rniphix = nlw0i(beta0rniphix, epsilon_main, taurniphix, dx, simPlat_dt, ilast, jlast);
					w0rniK = nlw0i(beta0rniK, epsilon_main, taurniK, dx, simPlat_dt, ilast, jlast);
					w0rnjphiy = nlw0j(beta0rnjphiy, epsilon_main, taurnjphiy, dx, simPlat_dt, ilast, jlast);
					w0rnjK = nlw0j(beta0rnjK, epsilon_main, taurnjK, dx, simPlat_dt, ilast, jlast);
					w1rniphix = nlw1i(beta1rniphix, epsilon_main, taurniphix, dx, simPlat_dt, ilast, jlast);
					w1rniK = nlw1i(beta1rniK, epsilon_main, taurniK, dx, simPlat_dt, ilast, jlast);
					w1rnjphiy = nlw1j(beta1rnjphiy, epsilon_main, taurnjphiy, dx, simPlat_dt, ilast, jlast);
					w1rnjK = nlw1j(beta1rnjK, epsilon_main, taurnjK, dx, simPlat_dt, ilast, jlast);
					wlniphix = w0lniphix + w1lniphix;
					wlniK = w0lniK + w1lniK;
					wlnjphiy = w0lnjphiy + w1lnjphiy;
					wlnjK = w0lnjK + w1lnjK;
					wrniphix = w0rniphix + w1rniphix;
					wrniK = w0rniK + w1rniK;
					wrnjphiy = w0rnjphiy + w1rnjphiy;
					wrnjK = w0rnjK + w1rnjK;
					w0lniphix = w0lniphix / wlniphix;
					w0lniK = w0lniK / wlniK;
					w0lnjphiy = w0lnjphiy / wlnjphiy;
					w0lnjK = w0lnjK / wlnjK;
					w1lniphix = w1lniphix / wlniphix;
					w1lniK = w1lniK / wlniK;
					w1lnjphiy = w1lnjphiy / wlnjphiy;
					w1lnjK = w1lnjK / wlnjK;
					w0rniphix = w0rniphix / wrniphix;
					w0rniK = w0rniK / wrniK;
					w0rnjphiy = w0rnjphiy / wrnjphiy;
					w0rnjK = w0rnjK / wrnjK;
					w1rniphix = w1rniphix / wrniphix;
					w1rniK = w1rniK / wrniK;
					w1rnjphiy = w1rnjphiy / wrnjphiy;
					w1rnjK = w1rnjK / wrnjK;
					phix_lni = weno(w0lniphix, w1lniphix, p0lniphix, p1lniphix, dx, simPlat_dt, ilast, jlast);
					K_lni = weno(w0lniK, w1lniK, p0lniK, p1lniK, dx, simPlat_dt, ilast, jlast);
					phiy_lnj = weno(w0lnjphiy, w1lnjphiy, p0lnjphiy, p1lnjphiy, dx, simPlat_dt, ilast, jlast);
					K_lnj = weno(w0lnjK, w1lnjK, p0lnjK, p1lnjK, dx, simPlat_dt, ilast, jlast);
					phix_rni = weno(w0rniphix, w1rniphix, p0rniphix, p1rniphix, dx, simPlat_dt, ilast, jlast);
					K_rni = weno(w0rniK, w1rniK, p0rniK, p1rniK, dx, simPlat_dt, ilast, jlast);
					phiy_rnj = weno(w0rnjphiy, w1rnjphiy, p0rnjphiy, p1rnjphiy, dx, simPlat_dt, ilast, jlast);
					K_rnj = weno(w0rnjK, w1rnjK, p0rnjK, p1rnjK, dx, simPlat_dt, ilast, jlast);
					p0lpiphix = lor1(LLFpiphix, LLFp_p1iphix, dx, simPlat_dt, ilast, jlast);
					p0lpiK = lor1(LLFpiK, LLFp_p1iK, dx, simPlat_dt, ilast, jlast);
					p0lpjphiy = lor1(LLFpjphiy, LLFp_p1jphiy, dx, simPlat_dt, ilast, jlast);
					p0lpjK = lor1(LLFpjK, LLFp_p1jK, dx, simPlat_dt, ilast, jlast);
					p1lpiphix = lor0(LLFp_m1iphix, LLFpiphix, dx, simPlat_dt, ilast, jlast);
					p1lpiK = lor0(LLFp_m1iK, LLFpiK, dx, simPlat_dt, ilast, jlast);
					p1lpjphiy = lor0(LLFp_m1jphiy, LLFpjphiy, dx, simPlat_dt, ilast, jlast);
					p1lpjK = lor0(LLFp_m1jK, LLFpjK, dx, simPlat_dt, ilast, jlast);
					p0rpiphix = lor0(LLFn_p2iphix, LLFn_p1iphix, dx, simPlat_dt, ilast, jlast);
					p0rpiK = lor0(LLFn_p2iK, LLFn_p1iK, dx, simPlat_dt, ilast, jlast);
					p0rpjphiy = lor0(LLFn_p2jphiy, LLFn_p1jphiy, dx, simPlat_dt, ilast, jlast);
					p0rpjK = lor0(LLFn_p2jK, LLFn_p1jK, dx, simPlat_dt, ilast, jlast);
					p1rpiphix = lor1(LLFn_p1iphix, LLFniphix, dx, simPlat_dt, ilast, jlast);
					p1rpiK = lor1(LLFn_p1iK, LLFniK, dx, simPlat_dt, ilast, jlast);
					p1rpjphiy = lor1(LLFn_p1jphiy, LLFnjphiy, dx, simPlat_dt, ilast, jlast);
					p1rpjK = lor1(LLFn_p1jK, LLFnjK, dx, simPlat_dt, ilast, jlast);
					beta0lpiphix = smooth1(LLFpiphix, LLFp_p1iphix, dx, simPlat_dt, ilast, jlast);
					beta0lpiK = smooth1(LLFpiK, LLFp_p1iK, dx, simPlat_dt, ilast, jlast);
					beta0lpjphiy = smooth1(LLFpjphiy, LLFp_p1jphiy, dx, simPlat_dt, ilast, jlast);
					beta0lpjK = smooth1(LLFpjK, LLFp_p1jK, dx, simPlat_dt, ilast, jlast);
					beta1lpiphix = smooth0(LLFp_m1iphix, LLFpiphix, dx, simPlat_dt, ilast, jlast);
					beta1lpiK = smooth0(LLFp_m1iK, LLFpiK, dx, simPlat_dt, ilast, jlast);
					beta1lpjphiy = smooth0(LLFp_m1jphiy, LLFpjphiy, dx, simPlat_dt, ilast, jlast);
					beta1lpjK = smooth0(LLFp_m1jK, LLFpjK, dx, simPlat_dt, ilast, jlast);
					beta0rpiphix = smooth0(LLFn_p2iphix, LLFn_p1iphix, dx, simPlat_dt, ilast, jlast);
					beta0rpiK = smooth0(LLFn_p2iK, LLFn_p1iK, dx, simPlat_dt, ilast, jlast);
					beta0rpjphiy = smooth0(LLFn_p2jphiy, LLFn_p1jphiy, dx, simPlat_dt, ilast, jlast);
					beta0rpjK = smooth0(LLFn_p2jK, LLFn_p1jK, dx, simPlat_dt, ilast, jlast);
					beta1rpiphix = smooth1(LLFn_p1iphix, LLFniphix, dx, simPlat_dt, ilast, jlast);
					beta1rpiK = smooth1(LLFn_p1iK, LLFniK, dx, simPlat_dt, ilast, jlast);
					beta1rpjphiy = smooth1(LLFn_p1jphiy, LLFnjphiy, dx, simPlat_dt, ilast, jlast);
					beta1rpjK = smooth1(LLFn_p1jK, LLFnjK, dx, simPlat_dt, ilast, jlast);
					taulpiphix = fabs(LLFp_p1iphix + (-2.0 * LLFpiphix) + LLFp_m1iphix) * fabs(LLFp_p1iphix + (-2.0 * LLFpiphix) + LLFp_m1iphix);
					taulpiK = fabs(LLFp_p1iK + (-2.0 * LLFpiK) + LLFp_m1iK) * fabs(LLFp_p1iK + (-2.0 * LLFpiK) + LLFp_m1iK);
					taulpjphiy = fabs(LLFp_p1jphiy + (-2.0 * LLFpjphiy) + LLFp_m1jphiy) * fabs(LLFp_p1jphiy + (-2.0 * LLFpjphiy) + LLFp_m1jphiy);
					taulpjK = fabs(LLFp_p1jK + (-2.0 * LLFpjK) + LLFp_m1jK) * fabs(LLFp_p1jK + (-2.0 * LLFpjK) + LLFp_m1jK);
					taurpiphix = fabs(LLFn_p2iphix + (-2.0 * LLFn_p1iphix) + LLFniphix) * fabs(LLFn_p2iphix + (-2.0 * LLFn_p1iphix) + LLFniphix);
					taurpiK = fabs(LLFn_p2iK + (-2.0 * LLFn_p1iK) + LLFniK) * fabs(LLFn_p2iK + (-2.0 * LLFn_p1iK) + LLFniK);
					taurpjphiy = fabs(LLFn_p2jphiy + (-2.0 * LLFn_p1jphiy) + LLFnjphiy) * fabs(LLFn_p2jphiy + (-2.0 * LLFn_p1jphiy) + LLFnjphiy);
					taurpjK = fabs(LLFn_p2jK + (-2.0 * LLFn_p1jK) + LLFnjK) * fabs(LLFn_p2jK + (-2.0 * LLFn_p1jK) + LLFnjK);
					w0lpiphix = nlw1i(beta0lpiphix, epsilon_main, taulpiphix, dx, simPlat_dt, ilast, jlast);
					w0lpiK = nlw1i(beta0lpiK, epsilon_main, taulpiK, dx, simPlat_dt, ilast, jlast);
					w0lpjphiy = nlw1j(beta0lpjphiy, epsilon_main, taulpjphiy, dx, simPlat_dt, ilast, jlast);
					w0lpjK = nlw1j(beta0lpjK, epsilon_main, taulpjK, dx, simPlat_dt, ilast, jlast);
					w1lpiphix = nlw0i(beta1lpiphix, epsilon_main, taulpiphix, dx, simPlat_dt, ilast, jlast);
					w1lpiK = nlw0i(beta1lpiK, epsilon_main, taulpiK, dx, simPlat_dt, ilast, jlast);
					w1lpjphiy = nlw0j(beta1lpjphiy, epsilon_main, taulpjphiy, dx, simPlat_dt, ilast, jlast);
					w1lpjK = nlw0j(beta1lpjK, epsilon_main, taulpjK, dx, simPlat_dt, ilast, jlast);
					w0rpiphix = nlw0i(beta0rpiphix, epsilon_main, taurpiphix, dx, simPlat_dt, ilast, jlast);
					w0rpiK = nlw0i(beta0rpiK, epsilon_main, taurpiK, dx, simPlat_dt, ilast, jlast);
					w0rpjphiy = nlw0j(beta0rpjphiy, epsilon_main, taurpjphiy, dx, simPlat_dt, ilast, jlast);
					w0rpjK = nlw0j(beta0rpjK, epsilon_main, taurpjK, dx, simPlat_dt, ilast, jlast);
					w1rpiphix = nlw1i(beta1rpiphix, epsilon_main, taurpiphix, dx, simPlat_dt, ilast, jlast);
					w1rpiK = nlw1i(beta1rpiK, epsilon_main, taurpiK, dx, simPlat_dt, ilast, jlast);
					w1rpjphiy = nlw1j(beta1rpjphiy, epsilon_main, taurpjphiy, dx, simPlat_dt, ilast, jlast);
					w1rpjK = nlw1j(beta1rpjK, epsilon_main, taurpjK, dx, simPlat_dt, ilast, jlast);
					wlpiphix = w0lpiphix + w1lpiphix;
					wlpiK = w0lpiK + w1lpiK;
					wlpjphiy = w0lpjphiy + w1lpjphiy;
					wlpjK = w0lpjK + w1lpjK;
					wrpiphix = w0rpiphix + w1rpiphix;
					wrpiK = w0rpiK + w1rpiK;
					wrpjphiy = w0rpjphiy + w1rpjphiy;
					wrpjK = w0rpjK + w1rpjK;
					w0lpiphix = w0lpiphix / wlpiphix;
					w0lpiK = w0lpiK / wlpiK;
					w0lpjphiy = w0lpjphiy / wlpjphiy;
					w0lpjK = w0lpjK / wlpjK;
					w1lpiphix = w1lpiphix / wlpiphix;
					w1lpiK = w1lpiK / wlpiK;
					w1lpjphiy = w1lpjphiy / wlpjphiy;
					w1lpjK = w1lpjK / wlpjK;
					w0rpiphix = w0rpiphix / wrpiphix;
					w0rpiK = w0rpiK / wrpiK;
					w0rpjphiy = w0rpjphiy / wrpjphiy;
					w0rpjK = w0rpjK / wrpjK;
					w1rpiphix = w1rpiphix / wrpiphix;
					w1rpiK = w1rpiK / wrpiK;
					w1rpjphiy = w1rpjphiy / wrpjphiy;
					w1rpjK = w1rpjK / wrpjK;
					phix_lpi = weno(w0lpiphix, w1lpiphix, p0lpiphix, p1lpiphix, dx, simPlat_dt, ilast, jlast);
					K_lpi = weno(w0lpiK, w1lpiK, p0lpiK, p1lpiK, dx, simPlat_dt, ilast, jlast);
					phiy_lpj = weno(w0lpjphiy, w1lpjphiy, p0lpjphiy, p1lpjphiy, dx, simPlat_dt, ilast, jlast);
					K_lpj = weno(w0lpjK, w1lpjK, p0lpjK, p1lpjK, dx, simPlat_dt, ilast, jlast);
					phix_rpi = weno(w0rpiphix, w1rpiphix, p0rpiphix, p1rpiphix, dx, simPlat_dt, ilast, jlast);
					K_rpi = weno(w0rpiK, w1rpiK, p0rpiK, p1rpiK, dx, simPlat_dt, ilast, jlast);
					phiy_rpj = weno(w0rpjphiy, w1rpjphiy, p0rpjphiy, p1rpjphiy, dx, simPlat_dt, ilast, jlast);
					K_rpj = weno(w0rpjK, w1rpjK, p0rpjK, p1rpjK, dx, simPlat_dt, ilast, jlast);
					Fluxniphix = phix_lni + phix_rni;
					FluxniK = K_lni + K_rni;
					Fluxnjphiy = phiy_lnj + phiy_rnj;
					FluxnjK = K_lnj + K_rnj;
					Fluxpiphix = phix_lpi + phix_rpi;
					FluxpiK = K_lpi + K_rpi;
					Fluxpjphiy = phiy_lpj + phiy_rpj;
					FluxpjK = K_lpj + K_rpj;
					RHS_Alpha = 0.0;
					RHS_Phi = -vector(rk1Alpha, i, j) * vector(rk1K, i, j);
					RHS_K = (m * m) * vector(rk1Phi, i, j);
					RHS_phix = -FVM_i(Fluxpiphix, Fluxniphix, dx, simPlat_dt, ilast, jlast);
					RHS_phiy = -FVM_j(Fluxpjphiy, Fluxnjphiy, dx, simPlat_dt, ilast, jlast);
					RHS_K = RHS_K - FVM_i(FluxpiK, FluxniK, dx, simPlat_dt, ilast, jlast);
					RHS_K = RHS_K - FVM_j(FluxpjK, FluxnjK, dx, simPlat_dt, ilast, jlast);
					if (dissipation_factor_Alpha > 0.0) {
						RHS_Alpha = RHS_Alpha + dissipation_factor_Alpha * (meshDissipation_i(rk1Alpha, i, j, dx, simPlat_dt, ilast, jlast) + meshDissipation_j(rk1Alpha, i, j, dx, simPlat_dt, ilast, jlast));
					}
					if (dissipation_factor_Phi > 0.0) {
						RHS_Phi = RHS_Phi + dissipation_factor_Phi * (meshDissipation_i(rk1Phi, i, j, dx, simPlat_dt, ilast, jlast) + meshDissipation_j(rk1Phi, i, j, dx, simPlat_dt, ilast, jlast));
					}
					if (dissipation_factor_phix > 0.0) {
						RHS_phix = RHS_phix + dissipation_factor_phix * (meshDissipation_i(rk1phix, i, j, dx, simPlat_dt, ilast, jlast) + meshDissipation_j(rk1phix, i, j, dx, simPlat_dt, ilast, jlast));
					}
					if (dissipation_factor_phiy > 0.0) {
						RHS_phiy = RHS_phiy + dissipation_factor_phiy * (meshDissipation_i(rk1phiy, i, j, dx, simPlat_dt, ilast, jlast) + meshDissipation_j(rk1phiy, i, j, dx, simPlat_dt, ilast, jlast));
					}
					if (dissipation_factor_K > 0.0) {
						RHS_K = RHS_K + dissipation_factor_K * (meshDissipation_i(rk1K, i, j, dx, simPlat_dt, ilast, jlast) + meshDissipation_j(rk1K, i, j, dx, simPlat_dt, ilast, jlast));
					}
					vector(rk2Alpha, i, j) = RK3P2_(RHS_Alpha, vector(Alpha_p, i, j), vector(rk1Alpha, i, j), dx, simPlat_dt, ilast, jlast);
					vector(rk2Phi, i, j) = RK3P2_(RHS_Phi, vector(Phi_p, i, j), vector(rk1Phi, i, j), dx, simPlat_dt, ilast, jlast);
					vector(rk2phix, i, j) = RK3P2_(RHS_phix, vector(phix_p, i, j), vector(rk1phix, i, j), dx, simPlat_dt, ilast, jlast);
					vector(rk2phiy, i, j) = RK3P2_(RHS_phiy, vector(phiy_p, i, j), vector(rk1phiy, i, j), dx, simPlat_dt, ilast, jlast);
					vector(rk2K, i, j) = RK3P2_(RHS_K, vector(K_p, i, j), vector(rk1K, i, j), dx, simPlat_dt, ilast, jlast);
				}
			}
		}
	}
	//Fill ghosts and periodical boundaries
	time_interpolate_operator_mesh1->setStep(2);
	d_bdry_sched_advance8[ln]->fillData(current_time + simPlat_dt * 0.5, false);
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch >& patch = *p_it;
		double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
	
		double* rk2Alpha = ((pdat::NodeData<double> *) patch->getPatchData(d_rk2Alpha_id).get())->getPointer();
		double* rk2K = ((pdat::NodeData<double> *) patch->getPatchData(d_rk2K_id).get())->getPointer();
		double* rk2phix = ((pdat::NodeData<double> *) patch->getPatchData(d_rk2phix_id).get())->getPointer();
		double* rk2phiy = ((pdat::NodeData<double> *) patch->getPatchData(d_rk2phiy_id).get())->getPointer();
		double* rk2Phi = ((pdat::NodeData<double> *) patch->getPatchData(d_rk2Phi_id).get())->getPointer();
		double* Alpha = ((pdat::NodeData<double> *) patch->getPatchData(d_Alpha_id).get())->getPointer();
		double* Alpha_p = ((pdat::NodeData<double> *) patch->getPatchData(d_Alpha_p_id).get())->getPointer();
		double* Phi = ((pdat::NodeData<double> *) patch->getPatchData(d_Phi_id).get())->getPointer();
		double* Phi_p = ((pdat::NodeData<double> *) patch->getPatchData(d_Phi_p_id).get())->getPointer();
		double* phix = ((pdat::NodeData<double> *) patch->getPatchData(d_phix_id).get())->getPointer();
		double* phix_p = ((pdat::NodeData<double> *) patch->getPatchData(d_phix_p_id).get())->getPointer();
		double* phiy = ((pdat::NodeData<double> *) patch->getPatchData(d_phiy_id).get())->getPointer();
		double* phiy_p = ((pdat::NodeData<double> *) patch->getPatchData(d_phiy_p_id).get())->getPointer();
		double* K = ((pdat::NodeData<double> *) patch->getPatchData(d_K_id).get())->getPointer();
		double* K_p = ((pdat::NodeData<double> *) patch->getPatchData(d_K_p_id).get())->getPointer();
		double Fluxiphix, FluxiK, Speedi, Fluxm2iphix, Fluxm2iK, Fluxm1iphix, Fluxm1iK, Fluxp1iphix, Fluxp1iK, Fluxp2iphix, Fluxp2iK, LLFpiphix, LLFpiK, LLFniphix, LLFniK, LLFp_m2iphix, LLFp_m2iK, LLFn_m2iphix, LLFn_m2iK, LLFp_m1iphix, LLFp_m1iK, LLFn_m1iphix, LLFn_m1iK, LLFp_p1iphix, LLFp_p1iK, LLFn_p1iphix, LLFn_p1iK, LLFp_p2iphix, LLFp_p2iK, LLFn_p2iphix, LLFn_p2iK, Fluxjphiy, FluxjK, Speedj, Fluxm2jphiy, Fluxm2jK, Fluxm1jphiy, Fluxm1jK, Fluxp1jphiy, Fluxp1jK, Fluxp2jphiy, Fluxp2jK, LLFpjphiy, LLFpjK, LLFnjphiy, LLFnjK, LLFp_m2jphiy, LLFp_m2jK, LLFn_m2jphiy, LLFn_m2jK, LLFp_m1jphiy, LLFp_m1jK, LLFn_m1jphiy, LLFn_m1jK, LLFp_p1jphiy, LLFp_p1jK, LLFn_p1jphiy, LLFn_p1jK, LLFp_p2jphiy, LLFp_p2jK, LLFn_p2jphiy, LLFn_p2jK, taulniphix, taulniK, taulnjphiy, taulnjK, taurniphix, taurniK, taurnjphiy, taurnjK, p0lniphix, p0lniK, p0lnjphiy, p0lnjK, p1lniphix, p1lniK, p1lnjphiy, p1lnjK, p0rniphix, p0rniK, p0rnjphiy, p0rnjK, p1rniphix, p1rniK, p1rnjphiy, p1rnjK, beta0lniphix, beta0lniK, beta0lnjphiy, beta0lnjK, beta1lniphix, beta1lniK, beta1lnjphiy, beta1lnjK, beta0rniphix, beta0rniK, beta0rnjphiy, beta0rnjK, beta1rniphix, beta1rniK, beta1rnjphiy, beta1rnjK, w0lniphix, w0lniK, w0lnjphiy, w0lnjK, w1lniphix, w1lniK, w1lnjphiy, w1lnjK, w0rniphix, w0rniK, w0rnjphiy, w0rnjK, w1rniphix, w1rniK, w1rnjphiy, w1rnjK, wlniphix, wlniK, wlnjphiy, wlnjK, wrniphix, wrniK, wrnjphiy, wrnjK, phix_lni, K_lni, phiy_lnj, K_lnj, phix_rni, K_rni, phiy_rnj, K_rnj, p0lpiphix, p0lpiK, p0lpjphiy, p0lpjK, p1lpiphix, p1lpiK, p1lpjphiy, p1lpjK, p0rpiphix, p0rpiK, p0rpjphiy, p0rpjK, p1rpiphix, p1rpiK, p1rpjphiy, p1rpjK, beta0lpiphix, beta0lpiK, beta0lpjphiy, beta0lpjK, beta1lpiphix, beta1lpiK, beta1lpjphiy, beta1lpjK, beta0rpiphix, beta0rpiK, beta0rpjphiy, beta0rpjK, beta1rpiphix, beta1rpiK, beta1rpjphiy, beta1rpjK, taulpiphix, taulpiK, taulpjphiy, taulpjK, taurpiphix, taurpiK, taurpjphiy, taurpjK, w0lpiphix, w0lpiK, w0lpjphiy, w0lpjK, w1lpiphix, w1lpiK, w1lpjphiy, w1lpjK, w0rpiphix, w0rpiK, w0rpjphiy, w0rpjK, w1rpiphix, w1rpiK, w1rpjphiy, w1rpjK, wlpiphix, wlpiK, wlpjphiy, wlpjK, wrpiphix, wrpiK, wrpjphiy, wrpjK, phix_lpi, K_lpi, phiy_lpj, K_lpj, phix_rpi, K_rpi, phiy_rpj, K_rpj, Fluxniphix, FluxniK, Fluxnjphiy, FluxnjK, Fluxpiphix, FluxpiK, Fluxpjphiy, FluxpjK, RHS_Alpha, RHS_Phi, RHS_K, RHS_phix, RHS_phiy;
	
		//Get the dimensions of the patch
		const hier::Index boxfirst = patch->getBox().lower();
		const hier::Index boxlast = patch->getBox().upper();
	
		//Get delta spaces into an array. dx, dy, dz.
		std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
		const double* dx  = patch_geom->getDx();
	
		//Auxiliary definitions
		int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
		int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
		for(int j = 0; j < jlast; j++) {
			for(int i = 0; i < ilast; i++) {
				if ((i + 3 < ilast && i - 3 >= 0 && j + 3 < jlast && j - 3 >= 0)) {
					Fluxiphix = Fiphix_mainI(vector(rk2Alpha, i, j), vector(rk2K, i, j), dx, simPlat_dt, ilast, jlast);
					FluxiK = FiK_mainI(vector(rk2Alpha, i, j), vector(rk2phix, i, j), dx, simPlat_dt, ilast, jlast);
					Speedi = fabs(fabs(vector(rk2Alpha, i, j)));
					Fluxm2iphix = Fiphix_mainI(vector(rk2Alpha, i - 2, j), vector(rk2K, i - 2, j), dx, simPlat_dt, ilast, jlast);
					Fluxm2iK = FiK_mainI(vector(rk2Alpha, i - 2, j), vector(rk2phix, i - 2, j), dx, simPlat_dt, ilast, jlast);
					Speedi = MAX(Speedi, fabs(fabs(vector(rk2Alpha, i - 2, j))));
					Fluxm1iphix = Fiphix_mainI(vector(rk2Alpha, i - 1, j), vector(rk2K, i - 1, j), dx, simPlat_dt, ilast, jlast);
					Fluxm1iK = FiK_mainI(vector(rk2Alpha, i - 1, j), vector(rk2phix, i - 1, j), dx, simPlat_dt, ilast, jlast);
					Speedi = MAX(Speedi, fabs(fabs(vector(rk2Alpha, i - 1, j))));
					Fluxp1iphix = Fiphix_mainI(vector(rk2Alpha, i + 1, j), vector(rk2K, i + 1, j), dx, simPlat_dt, ilast, jlast);
					Fluxp1iK = FiK_mainI(vector(rk2Alpha, i + 1, j), vector(rk2phix, i + 1, j), dx, simPlat_dt, ilast, jlast);
					Speedi = MAX(Speedi, fabs(fabs(vector(rk2Alpha, i + 1, j))));
					Fluxp2iphix = Fiphix_mainI(vector(rk2Alpha, i + 2, j), vector(rk2K, i + 2, j), dx, simPlat_dt, ilast, jlast);
					Fluxp2iK = FiK_mainI(vector(rk2Alpha, i + 2, j), vector(rk2phix, i + 2, j), dx, simPlat_dt, ilast, jlast);
					Speedi = MAX(Speedi, fabs(fabs(vector(rk2Alpha, i + 2, j))));
					LLFpiphix = 0.5 * (Fluxiphix + fabs(Speedi) * vector(rk2phix, i, j));
					LLFpiK = 0.5 * (FluxiK + fabs(Speedi) * vector(rk2K, i, j));
					LLFniphix = 0.5 * (Fluxiphix - fabs(Speedi) * vector(rk2phix, i, j));
					LLFniK = 0.5 * (FluxiK - fabs(Speedi) * vector(rk2K, i, j));
					LLFp_m2iphix = 0.5 * (Fluxm2iphix + fabs(Speedi) * vector(rk2phix, i - 2, j));
					LLFp_m2iK = 0.5 * (Fluxm2iK + fabs(Speedi) * vector(rk2K, i - 2, j));
					LLFn_m2iphix = 0.5 * (Fluxm2iphix - fabs(Speedi) * vector(rk2phix, i - 2, j));
					LLFn_m2iK = 0.5 * (Fluxm2iK - fabs(Speedi) * vector(rk2K, i - 2, j));
					LLFp_m1iphix = 0.5 * (Fluxm1iphix + fabs(Speedi) * vector(rk2phix, i - 1, j));
					LLFp_m1iK = 0.5 * (Fluxm1iK + fabs(Speedi) * vector(rk2K, i - 1, j));
					LLFn_m1iphix = 0.5 * (Fluxm1iphix - fabs(Speedi) * vector(rk2phix, i - 1, j));
					LLFn_m1iK = 0.5 * (Fluxm1iK - fabs(Speedi) * vector(rk2K, i - 1, j));
					LLFp_p1iphix = 0.5 * (Fluxp1iphix + fabs(Speedi) * vector(rk2phix, i + 1, j));
					LLFp_p1iK = 0.5 * (Fluxp1iK + fabs(Speedi) * vector(rk2K, i + 1, j));
					LLFn_p1iphix = 0.5 * (Fluxp1iphix - fabs(Speedi) * vector(rk2phix, i + 1, j));
					LLFn_p1iK = 0.5 * (Fluxp1iK - fabs(Speedi) * vector(rk2K, i + 1, j));
					LLFp_p2iphix = 0.5 * (Fluxp2iphix + fabs(Speedi) * vector(rk2phix, i + 2, j));
					LLFp_p2iK = 0.5 * (Fluxp2iK + fabs(Speedi) * vector(rk2K, i + 2, j));
					LLFn_p2iphix = 0.5 * (Fluxp2iphix - fabs(Speedi) * vector(rk2phix, i + 2, j));
					LLFn_p2iK = 0.5 * (Fluxp2iK - fabs(Speedi) * vector(rk2K, i + 2, j));
					Fluxjphiy = Fjphiy_mainI(vector(rk2Alpha, i, j), vector(rk2K, i, j), dx, simPlat_dt, ilast, jlast);
					FluxjK = FjK_mainI(vector(rk2Alpha, i, j), vector(rk2phiy, i, j), dx, simPlat_dt, ilast, jlast);
					Speedj = fabs(fabs(vector(rk2Alpha, i, j)));
					Fluxm2jphiy = Fjphiy_mainI(vector(rk2Alpha, i, j - 2), vector(rk2K, i, j - 2), dx, simPlat_dt, ilast, jlast);
					Fluxm2jK = FjK_mainI(vector(rk2Alpha, i, j - 2), vector(rk2phiy, i, j - 2), dx, simPlat_dt, ilast, jlast);
					Speedj = MAX(Speedj, fabs(fabs(vector(rk2Alpha, i, j - 2))));
					Fluxm1jphiy = Fjphiy_mainI(vector(rk2Alpha, i, j - 1), vector(rk2K, i, j - 1), dx, simPlat_dt, ilast, jlast);
					Fluxm1jK = FjK_mainI(vector(rk2Alpha, i, j - 1), vector(rk2phiy, i, j - 1), dx, simPlat_dt, ilast, jlast);
					Speedj = MAX(Speedj, fabs(fabs(vector(rk2Alpha, i, j - 1))));
					Fluxp1jphiy = Fjphiy_mainI(vector(rk2Alpha, i, j + 1), vector(rk2K, i, j + 1), dx, simPlat_dt, ilast, jlast);
					Fluxp1jK = FjK_mainI(vector(rk2Alpha, i, j + 1), vector(rk2phiy, i, j + 1), dx, simPlat_dt, ilast, jlast);
					Speedj = MAX(Speedj, fabs(fabs(vector(rk2Alpha, i, j + 1))));
					Fluxp2jphiy = Fjphiy_mainI(vector(rk2Alpha, i, j + 2), vector(rk2K, i, j + 2), dx, simPlat_dt, ilast, jlast);
					Fluxp2jK = FjK_mainI(vector(rk2Alpha, i, j + 2), vector(rk2phiy, i, j + 2), dx, simPlat_dt, ilast, jlast);
					Speedj = MAX(Speedj, fabs(fabs(vector(rk2Alpha, i, j + 2))));
					LLFpjphiy = 0.5 * (Fluxjphiy + fabs(Speedj) * vector(rk2phiy, i, j));
					LLFpjK = 0.5 * (FluxjK + fabs(Speedj) * vector(rk2K, i, j));
					LLFnjphiy = 0.5 * (Fluxjphiy - fabs(Speedj) * vector(rk2phiy, i, j));
					LLFnjK = 0.5 * (FluxjK - fabs(Speedj) * vector(rk2K, i, j));
					LLFp_m2jphiy = 0.5 * (Fluxm2jphiy + fabs(Speedj) * vector(rk2phiy, i, j - 2));
					LLFp_m2jK = 0.5 * (Fluxm2jK + fabs(Speedj) * vector(rk2K, i, j - 2));
					LLFn_m2jphiy = 0.5 * (Fluxm2jphiy - fabs(Speedj) * vector(rk2phiy, i, j - 2));
					LLFn_m2jK = 0.5 * (Fluxm2jK - fabs(Speedj) * vector(rk2K, i, j - 2));
					LLFp_m1jphiy = 0.5 * (Fluxm1jphiy + fabs(Speedj) * vector(rk2phiy, i, j - 1));
					LLFp_m1jK = 0.5 * (Fluxm1jK + fabs(Speedj) * vector(rk2K, i, j - 1));
					LLFn_m1jphiy = 0.5 * (Fluxm1jphiy - fabs(Speedj) * vector(rk2phiy, i, j - 1));
					LLFn_m1jK = 0.5 * (Fluxm1jK - fabs(Speedj) * vector(rk2K, i, j - 1));
					LLFp_p1jphiy = 0.5 * (Fluxp1jphiy + fabs(Speedj) * vector(rk2phiy, i, j + 1));
					LLFp_p1jK = 0.5 * (Fluxp1jK + fabs(Speedj) * vector(rk2K, i, j + 1));
					LLFn_p1jphiy = 0.5 * (Fluxp1jphiy - fabs(Speedj) * vector(rk2phiy, i, j + 1));
					LLFn_p1jK = 0.5 * (Fluxp1jK - fabs(Speedj) * vector(rk2K, i, j + 1));
					LLFp_p2jphiy = 0.5 * (Fluxp2jphiy + fabs(Speedj) * vector(rk2phiy, i, j + 2));
					LLFp_p2jK = 0.5 * (Fluxp2jK + fabs(Speedj) * vector(rk2K, i, j + 2));
					LLFn_p2jphiy = 0.5 * (Fluxp2jphiy - fabs(Speedj) * vector(rk2phiy, i, j + 2));
					LLFn_p2jK = 0.5 * (Fluxp2jK - fabs(Speedj) * vector(rk2K, i, j + 2));
					taulniphix = fabs(LLFpiphix + (-2.0 * LLFp_m1iphix) + LLFp_m2iphix) * fabs(LLFpiphix + (-2.0 * LLFp_m1iphix) + LLFp_m2iphix);
					taulniK = fabs(LLFpiK + (-2.0 * LLFp_m1iK) + LLFp_m2iK) * fabs(LLFpiK + (-2.0 * LLFp_m1iK) + LLFp_m2iK);
					taulnjphiy = fabs(LLFpjphiy + (-2.0 * LLFp_m1jphiy) + LLFp_m2jphiy) * fabs(LLFpjphiy + (-2.0 * LLFp_m1jphiy) + LLFp_m2jphiy);
					taulnjK = fabs(LLFpjK + (-2.0 * LLFp_m1jK) + LLFp_m2jK) * fabs(LLFpjK + (-2.0 * LLFp_m1jK) + LLFp_m2jK);
					taurniphix = fabs(LLFn_p1iphix + (-2.0 * LLFniphix) + LLFn_m1iphix) * fabs(LLFn_p1iphix + (-2.0 * LLFniphix) + LLFn_m1iphix);
					taurniK = fabs(LLFn_p1iK + (-2.0 * LLFniK) + LLFn_m1iK) * fabs(LLFn_p1iK + (-2.0 * LLFniK) + LLFn_m1iK);
					taurnjphiy = fabs(LLFn_p1jphiy + (-2.0 * LLFnjphiy) + LLFn_m1jphiy) * fabs(LLFn_p1jphiy + (-2.0 * LLFnjphiy) + LLFn_m1jphiy);
					taurnjK = fabs(LLFn_p1jK + (-2.0 * LLFnjK) + LLFn_m1jK) * fabs(LLFn_p1jK + (-2.0 * LLFnjK) + LLFn_m1jK);
					p0lniphix = lor1(LLFp_m1iphix, LLFpiphix, dx, simPlat_dt, ilast, jlast);
					p0lniK = lor1(LLFp_m1iK, LLFpiK, dx, simPlat_dt, ilast, jlast);
					p0lnjphiy = lor1(LLFp_m1jphiy, LLFpjphiy, dx, simPlat_dt, ilast, jlast);
					p0lnjK = lor1(LLFp_m1jK, LLFpjK, dx, simPlat_dt, ilast, jlast);
					p1lniphix = lor0(LLFp_m2iphix, LLFp_m1iphix, dx, simPlat_dt, ilast, jlast);
					p1lniK = lor0(LLFp_m2iK, LLFp_m1iK, dx, simPlat_dt, ilast, jlast);
					p1lnjphiy = lor0(LLFp_m2jphiy, LLFp_m1jphiy, dx, simPlat_dt, ilast, jlast);
					p1lnjK = lor0(LLFp_m2jK, LLFp_m1jK, dx, simPlat_dt, ilast, jlast);
					p0rniphix = lor0(LLFn_p1iphix, LLFniphix, dx, simPlat_dt, ilast, jlast);
					p0rniK = lor0(LLFn_p1iK, LLFniK, dx, simPlat_dt, ilast, jlast);
					p0rnjphiy = lor0(LLFn_p1jphiy, LLFnjphiy, dx, simPlat_dt, ilast, jlast);
					p0rnjK = lor0(LLFn_p1jK, LLFnjK, dx, simPlat_dt, ilast, jlast);
					p1rniphix = lor1(LLFniphix, LLFn_m1iphix, dx, simPlat_dt, ilast, jlast);
					p1rniK = lor1(LLFniK, LLFn_m1iK, dx, simPlat_dt, ilast, jlast);
					p1rnjphiy = lor1(LLFnjphiy, LLFn_m1jphiy, dx, simPlat_dt, ilast, jlast);
					p1rnjK = lor1(LLFnjK, LLFn_m1jK, dx, simPlat_dt, ilast, jlast);
					beta0lniphix = smooth1(LLFp_m1iphix, LLFpiphix, dx, simPlat_dt, ilast, jlast);
					beta0lniK = smooth1(LLFp_m1iK, LLFpiK, dx, simPlat_dt, ilast, jlast);
					beta0lnjphiy = smooth1(LLFp_m1jphiy, LLFpjphiy, dx, simPlat_dt, ilast, jlast);
					beta0lnjK = smooth1(LLFp_m1jK, LLFpjK, dx, simPlat_dt, ilast, jlast);
					beta1lniphix = smooth0(LLFp_m2iphix, LLFp_m1iphix, dx, simPlat_dt, ilast, jlast);
					beta1lniK = smooth0(LLFp_m2iK, LLFp_m1iK, dx, simPlat_dt, ilast, jlast);
					beta1lnjphiy = smooth0(LLFp_m2jphiy, LLFp_m1jphiy, dx, simPlat_dt, ilast, jlast);
					beta1lnjK = smooth0(LLFp_m2jK, LLFp_m1jK, dx, simPlat_dt, ilast, jlast);
					beta0rniphix = smooth0(LLFn_p1iphix, LLFniphix, dx, simPlat_dt, ilast, jlast);
					beta0rniK = smooth0(LLFn_p1iK, LLFniK, dx, simPlat_dt, ilast, jlast);
					beta0rnjphiy = smooth0(LLFn_p1jphiy, LLFnjphiy, dx, simPlat_dt, ilast, jlast);
					beta0rnjK = smooth0(LLFn_p1jK, LLFnjK, dx, simPlat_dt, ilast, jlast);
					beta1rniphix = smooth1(LLFniphix, LLFn_m1iphix, dx, simPlat_dt, ilast, jlast);
					beta1rniK = smooth1(LLFniK, LLFn_m1iK, dx, simPlat_dt, ilast, jlast);
					beta1rnjphiy = smooth1(LLFnjphiy, LLFn_m1jphiy, dx, simPlat_dt, ilast, jlast);
					beta1rnjK = smooth1(LLFnjK, LLFn_m1jK, dx, simPlat_dt, ilast, jlast);
					w0lniphix = nlw1i(beta0lniphix, epsilon_main, taulniphix, dx, simPlat_dt, ilast, jlast);
					w0lniK = nlw1i(beta0lniK, epsilon_main, taulniK, dx, simPlat_dt, ilast, jlast);
					w0lnjphiy = nlw1j(beta0lnjphiy, epsilon_main, taulnjphiy, dx, simPlat_dt, ilast, jlast);
					w0lnjK = nlw1j(beta0lnjK, epsilon_main, taulnjK, dx, simPlat_dt, ilast, jlast);
					w1lniphix = nlw0i(beta1lniphix, epsilon_main, taulniphix, dx, simPlat_dt, ilast, jlast);
					w1lniK = nlw0i(beta1lniK, epsilon_main, taulniK, dx, simPlat_dt, ilast, jlast);
					w1lnjphiy = nlw0j(beta1lnjphiy, epsilon_main, taulnjphiy, dx, simPlat_dt, ilast, jlast);
					w1lnjK = nlw0j(beta1lnjK, epsilon_main, taulnjK, dx, simPlat_dt, ilast, jlast);
					w0rniphix = nlw0i(beta0rniphix, epsilon_main, taurniphix, dx, simPlat_dt, ilast, jlast);
					w0rniK = nlw0i(beta0rniK, epsilon_main, taurniK, dx, simPlat_dt, ilast, jlast);
					w0rnjphiy = nlw0j(beta0rnjphiy, epsilon_main, taurnjphiy, dx, simPlat_dt, ilast, jlast);
					w0rnjK = nlw0j(beta0rnjK, epsilon_main, taurnjK, dx, simPlat_dt, ilast, jlast);
					w1rniphix = nlw1i(beta1rniphix, epsilon_main, taurniphix, dx, simPlat_dt, ilast, jlast);
					w1rniK = nlw1i(beta1rniK, epsilon_main, taurniK, dx, simPlat_dt, ilast, jlast);
					w1rnjphiy = nlw1j(beta1rnjphiy, epsilon_main, taurnjphiy, dx, simPlat_dt, ilast, jlast);
					w1rnjK = nlw1j(beta1rnjK, epsilon_main, taurnjK, dx, simPlat_dt, ilast, jlast);
					wlniphix = w0lniphix + w1lniphix;
					wlniK = w0lniK + w1lniK;
					wlnjphiy = w0lnjphiy + w1lnjphiy;
					wlnjK = w0lnjK + w1lnjK;
					wrniphix = w0rniphix + w1rniphix;
					wrniK = w0rniK + w1rniK;
					wrnjphiy = w0rnjphiy + w1rnjphiy;
					wrnjK = w0rnjK + w1rnjK;
					w0lniphix = w0lniphix / wlniphix;
					w0lniK = w0lniK / wlniK;
					w0lnjphiy = w0lnjphiy / wlnjphiy;
					w0lnjK = w0lnjK / wlnjK;
					w1lniphix = w1lniphix / wlniphix;
					w1lniK = w1lniK / wlniK;
					w1lnjphiy = w1lnjphiy / wlnjphiy;
					w1lnjK = w1lnjK / wlnjK;
					w0rniphix = w0rniphix / wrniphix;
					w0rniK = w0rniK / wrniK;
					w0rnjphiy = w0rnjphiy / wrnjphiy;
					w0rnjK = w0rnjK / wrnjK;
					w1rniphix = w1rniphix / wrniphix;
					w1rniK = w1rniK / wrniK;
					w1rnjphiy = w1rnjphiy / wrnjphiy;
					w1rnjK = w1rnjK / wrnjK;
					phix_lni = weno(w0lniphix, w1lniphix, p0lniphix, p1lniphix, dx, simPlat_dt, ilast, jlast);
					K_lni = weno(w0lniK, w1lniK, p0lniK, p1lniK, dx, simPlat_dt, ilast, jlast);
					phiy_lnj = weno(w0lnjphiy, w1lnjphiy, p0lnjphiy, p1lnjphiy, dx, simPlat_dt, ilast, jlast);
					K_lnj = weno(w0lnjK, w1lnjK, p0lnjK, p1lnjK, dx, simPlat_dt, ilast, jlast);
					phix_rni = weno(w0rniphix, w1rniphix, p0rniphix, p1rniphix, dx, simPlat_dt, ilast, jlast);
					K_rni = weno(w0rniK, w1rniK, p0rniK, p1rniK, dx, simPlat_dt, ilast, jlast);
					phiy_rnj = weno(w0rnjphiy, w1rnjphiy, p0rnjphiy, p1rnjphiy, dx, simPlat_dt, ilast, jlast);
					K_rnj = weno(w0rnjK, w1rnjK, p0rnjK, p1rnjK, dx, simPlat_dt, ilast, jlast);
					p0lpiphix = lor1(LLFpiphix, LLFp_p1iphix, dx, simPlat_dt, ilast, jlast);
					p0lpiK = lor1(LLFpiK, LLFp_p1iK, dx, simPlat_dt, ilast, jlast);
					p0lpjphiy = lor1(LLFpjphiy, LLFp_p1jphiy, dx, simPlat_dt, ilast, jlast);
					p0lpjK = lor1(LLFpjK, LLFp_p1jK, dx, simPlat_dt, ilast, jlast);
					p1lpiphix = lor0(LLFp_m1iphix, LLFpiphix, dx, simPlat_dt, ilast, jlast);
					p1lpiK = lor0(LLFp_m1iK, LLFpiK, dx, simPlat_dt, ilast, jlast);
					p1lpjphiy = lor0(LLFp_m1jphiy, LLFpjphiy, dx, simPlat_dt, ilast, jlast);
					p1lpjK = lor0(LLFp_m1jK, LLFpjK, dx, simPlat_dt, ilast, jlast);
					p0rpiphix = lor0(LLFn_p2iphix, LLFn_p1iphix, dx, simPlat_dt, ilast, jlast);
					p0rpiK = lor0(LLFn_p2iK, LLFn_p1iK, dx, simPlat_dt, ilast, jlast);
					p0rpjphiy = lor0(LLFn_p2jphiy, LLFn_p1jphiy, dx, simPlat_dt, ilast, jlast);
					p0rpjK = lor0(LLFn_p2jK, LLFn_p1jK, dx, simPlat_dt, ilast, jlast);
					p1rpiphix = lor1(LLFn_p1iphix, LLFniphix, dx, simPlat_dt, ilast, jlast);
					p1rpiK = lor1(LLFn_p1iK, LLFniK, dx, simPlat_dt, ilast, jlast);
					p1rpjphiy = lor1(LLFn_p1jphiy, LLFnjphiy, dx, simPlat_dt, ilast, jlast);
					p1rpjK = lor1(LLFn_p1jK, LLFnjK, dx, simPlat_dt, ilast, jlast);
					beta0lpiphix = smooth1(LLFpiphix, LLFp_p1iphix, dx, simPlat_dt, ilast, jlast);
					beta0lpiK = smooth1(LLFpiK, LLFp_p1iK, dx, simPlat_dt, ilast, jlast);
					beta0lpjphiy = smooth1(LLFpjphiy, LLFp_p1jphiy, dx, simPlat_dt, ilast, jlast);
					beta0lpjK = smooth1(LLFpjK, LLFp_p1jK, dx, simPlat_dt, ilast, jlast);
					beta1lpiphix = smooth0(LLFp_m1iphix, LLFpiphix, dx, simPlat_dt, ilast, jlast);
					beta1lpiK = smooth0(LLFp_m1iK, LLFpiK, dx, simPlat_dt, ilast, jlast);
					beta1lpjphiy = smooth0(LLFp_m1jphiy, LLFpjphiy, dx, simPlat_dt, ilast, jlast);
					beta1lpjK = smooth0(LLFp_m1jK, LLFpjK, dx, simPlat_dt, ilast, jlast);
					beta0rpiphix = smooth0(LLFn_p2iphix, LLFn_p1iphix, dx, simPlat_dt, ilast, jlast);
					beta0rpiK = smooth0(LLFn_p2iK, LLFn_p1iK, dx, simPlat_dt, ilast, jlast);
					beta0rpjphiy = smooth0(LLFn_p2jphiy, LLFn_p1jphiy, dx, simPlat_dt, ilast, jlast);
					beta0rpjK = smooth0(LLFn_p2jK, LLFn_p1jK, dx, simPlat_dt, ilast, jlast);
					beta1rpiphix = smooth1(LLFn_p1iphix, LLFniphix, dx, simPlat_dt, ilast, jlast);
					beta1rpiK = smooth1(LLFn_p1iK, LLFniK, dx, simPlat_dt, ilast, jlast);
					beta1rpjphiy = smooth1(LLFn_p1jphiy, LLFnjphiy, dx, simPlat_dt, ilast, jlast);
					beta1rpjK = smooth1(LLFn_p1jK, LLFnjK, dx, simPlat_dt, ilast, jlast);
					taulpiphix = fabs(LLFp_p1iphix + (-2.0 * LLFpiphix) + LLFp_m1iphix) * fabs(LLFp_p1iphix + (-2.0 * LLFpiphix) + LLFp_m1iphix);
					taulpiK = fabs(LLFp_p1iK + (-2.0 * LLFpiK) + LLFp_m1iK) * fabs(LLFp_p1iK + (-2.0 * LLFpiK) + LLFp_m1iK);
					taulpjphiy = fabs(LLFp_p1jphiy + (-2.0 * LLFpjphiy) + LLFp_m1jphiy) * fabs(LLFp_p1jphiy + (-2.0 * LLFpjphiy) + LLFp_m1jphiy);
					taulpjK = fabs(LLFp_p1jK + (-2.0 * LLFpjK) + LLFp_m1jK) * fabs(LLFp_p1jK + (-2.0 * LLFpjK) + LLFp_m1jK);
					taurpiphix = fabs(LLFn_p2iphix + (-2.0 * LLFn_p1iphix) + LLFniphix) * fabs(LLFn_p2iphix + (-2.0 * LLFn_p1iphix) + LLFniphix);
					taurpiK = fabs(LLFn_p2iK + (-2.0 * LLFn_p1iK) + LLFniK) * fabs(LLFn_p2iK + (-2.0 * LLFn_p1iK) + LLFniK);
					taurpjphiy = fabs(LLFn_p2jphiy + (-2.0 * LLFn_p1jphiy) + LLFnjphiy) * fabs(LLFn_p2jphiy + (-2.0 * LLFn_p1jphiy) + LLFnjphiy);
					taurpjK = fabs(LLFn_p2jK + (-2.0 * LLFn_p1jK) + LLFnjK) * fabs(LLFn_p2jK + (-2.0 * LLFn_p1jK) + LLFnjK);
					w0lpiphix = nlw1i(beta0lpiphix, epsilon_main, taulpiphix, dx, simPlat_dt, ilast, jlast);
					w0lpiK = nlw1i(beta0lpiK, epsilon_main, taulpiK, dx, simPlat_dt, ilast, jlast);
					w0lpjphiy = nlw1j(beta0lpjphiy, epsilon_main, taulpjphiy, dx, simPlat_dt, ilast, jlast);
					w0lpjK = nlw1j(beta0lpjK, epsilon_main, taulpjK, dx, simPlat_dt, ilast, jlast);
					w1lpiphix = nlw0i(beta1lpiphix, epsilon_main, taulpiphix, dx, simPlat_dt, ilast, jlast);
					w1lpiK = nlw0i(beta1lpiK, epsilon_main, taulpiK, dx, simPlat_dt, ilast, jlast);
					w1lpjphiy = nlw0j(beta1lpjphiy, epsilon_main, taulpjphiy, dx, simPlat_dt, ilast, jlast);
					w1lpjK = nlw0j(beta1lpjK, epsilon_main, taulpjK, dx, simPlat_dt, ilast, jlast);
					w0rpiphix = nlw0i(beta0rpiphix, epsilon_main, taurpiphix, dx, simPlat_dt, ilast, jlast);
					w0rpiK = nlw0i(beta0rpiK, epsilon_main, taurpiK, dx, simPlat_dt, ilast, jlast);
					w0rpjphiy = nlw0j(beta0rpjphiy, epsilon_main, taurpjphiy, dx, simPlat_dt, ilast, jlast);
					w0rpjK = nlw0j(beta0rpjK, epsilon_main, taurpjK, dx, simPlat_dt, ilast, jlast);
					w1rpiphix = nlw1i(beta1rpiphix, epsilon_main, taurpiphix, dx, simPlat_dt, ilast, jlast);
					w1rpiK = nlw1i(beta1rpiK, epsilon_main, taurpiK, dx, simPlat_dt, ilast, jlast);
					w1rpjphiy = nlw1j(beta1rpjphiy, epsilon_main, taurpjphiy, dx, simPlat_dt, ilast, jlast);
					w1rpjK = nlw1j(beta1rpjK, epsilon_main, taurpjK, dx, simPlat_dt, ilast, jlast);
					wlpiphix = w0lpiphix + w1lpiphix;
					wlpiK = w0lpiK + w1lpiK;
					wlpjphiy = w0lpjphiy + w1lpjphiy;
					wlpjK = w0lpjK + w1lpjK;
					wrpiphix = w0rpiphix + w1rpiphix;
					wrpiK = w0rpiK + w1rpiK;
					wrpjphiy = w0rpjphiy + w1rpjphiy;
					wrpjK = w0rpjK + w1rpjK;
					w0lpiphix = w0lpiphix / wlpiphix;
					w0lpiK = w0lpiK / wlpiK;
					w0lpjphiy = w0lpjphiy / wlpjphiy;
					w0lpjK = w0lpjK / wlpjK;
					w1lpiphix = w1lpiphix / wlpiphix;
					w1lpiK = w1lpiK / wlpiK;
					w1lpjphiy = w1lpjphiy / wlpjphiy;
					w1lpjK = w1lpjK / wlpjK;
					w0rpiphix = w0rpiphix / wrpiphix;
					w0rpiK = w0rpiK / wrpiK;
					w0rpjphiy = w0rpjphiy / wrpjphiy;
					w0rpjK = w0rpjK / wrpjK;
					w1rpiphix = w1rpiphix / wrpiphix;
					w1rpiK = w1rpiK / wrpiK;
					w1rpjphiy = w1rpjphiy / wrpjphiy;
					w1rpjK = w1rpjK / wrpjK;
					phix_lpi = weno(w0lpiphix, w1lpiphix, p0lpiphix, p1lpiphix, dx, simPlat_dt, ilast, jlast);
					K_lpi = weno(w0lpiK, w1lpiK, p0lpiK, p1lpiK, dx, simPlat_dt, ilast, jlast);
					phiy_lpj = weno(w0lpjphiy, w1lpjphiy, p0lpjphiy, p1lpjphiy, dx, simPlat_dt, ilast, jlast);
					K_lpj = weno(w0lpjK, w1lpjK, p0lpjK, p1lpjK, dx, simPlat_dt, ilast, jlast);
					phix_rpi = weno(w0rpiphix, w1rpiphix, p0rpiphix, p1rpiphix, dx, simPlat_dt, ilast, jlast);
					K_rpi = weno(w0rpiK, w1rpiK, p0rpiK, p1rpiK, dx, simPlat_dt, ilast, jlast);
					phiy_rpj = weno(w0rpjphiy, w1rpjphiy, p0rpjphiy, p1rpjphiy, dx, simPlat_dt, ilast, jlast);
					K_rpj = weno(w0rpjK, w1rpjK, p0rpjK, p1rpjK, dx, simPlat_dt, ilast, jlast);
					Fluxniphix = phix_lni + phix_rni;
					FluxniK = K_lni + K_rni;
					Fluxnjphiy = phiy_lnj + phiy_rnj;
					FluxnjK = K_lnj + K_rnj;
					Fluxpiphix = phix_lpi + phix_rpi;
					FluxpiK = K_lpi + K_rpi;
					Fluxpjphiy = phiy_lpj + phiy_rpj;
					FluxpjK = K_lpj + K_rpj;
					RHS_Alpha = 0.0;
					RHS_Phi = -vector(rk2Alpha, i, j) * vector(rk2K, i, j);
					RHS_K = (m * m) * vector(rk2Phi, i, j);
					RHS_phix = -FVM_i(Fluxpiphix, Fluxniphix, dx, simPlat_dt, ilast, jlast);
					RHS_phiy = -FVM_j(Fluxpjphiy, Fluxnjphiy, dx, simPlat_dt, ilast, jlast);
					RHS_K = RHS_K - FVM_i(FluxpiK, FluxniK, dx, simPlat_dt, ilast, jlast);
					RHS_K = RHS_K - FVM_j(FluxpjK, FluxnjK, dx, simPlat_dt, ilast, jlast);
					if (dissipation_factor_Alpha > 0.0) {
						RHS_Alpha = RHS_Alpha + dissipation_factor_Alpha * (meshDissipation_i(rk2Alpha, i, j, dx, simPlat_dt, ilast, jlast) + meshDissipation_j(rk2Alpha, i, j, dx, simPlat_dt, ilast, jlast));
					}
					if (dissipation_factor_Phi > 0.0) {
						RHS_Phi = RHS_Phi + dissipation_factor_Phi * (meshDissipation_i(rk2Phi, i, j, dx, simPlat_dt, ilast, jlast) + meshDissipation_j(rk2Phi, i, j, dx, simPlat_dt, ilast, jlast));
					}
					if (dissipation_factor_phix > 0.0) {
						RHS_phix = RHS_phix + dissipation_factor_phix * (meshDissipation_i(rk2phix, i, j, dx, simPlat_dt, ilast, jlast) + meshDissipation_j(rk2phix, i, j, dx, simPlat_dt, ilast, jlast));
					}
					if (dissipation_factor_phiy > 0.0) {
						RHS_phiy = RHS_phiy + dissipation_factor_phiy * (meshDissipation_i(rk2phiy, i, j, dx, simPlat_dt, ilast, jlast) + meshDissipation_j(rk2phiy, i, j, dx, simPlat_dt, ilast, jlast));
					}
					if (dissipation_factor_K > 0.0) {
						RHS_K = RHS_K + dissipation_factor_K * (meshDissipation_i(rk2K, i, j, dx, simPlat_dt, ilast, jlast) + meshDissipation_j(rk2K, i, j, dx, simPlat_dt, ilast, jlast));
					}
					vector(Alpha, i, j) = RK3P3_(RHS_Alpha, vector(Alpha_p, i, j), vector(rk2Alpha, i, j), dx, simPlat_dt, ilast, jlast);
					vector(Phi, i, j) = RK3P3_(RHS_Phi, vector(Phi_p, i, j), vector(rk2Phi, i, j), dx, simPlat_dt, ilast, jlast);
					vector(phix, i, j) = RK3P3_(RHS_phix, vector(phix_p, i, j), vector(rk2phix, i, j), dx, simPlat_dt, ilast, jlast);
					vector(phiy, i, j) = RK3P3_(RHS_phiy, vector(phiy_p, i, j), vector(rk2phiy, i, j), dx, simPlat_dt, ilast, jlast);
					vector(K, i, j) = RK3P3_(RHS_K, vector(K_p, i, j), vector(rk2K, i, j), dx, simPlat_dt, ilast, jlast);
				}
			}
		}
	}
	//Fill ghosts and periodical boundaries
	time_interpolate_operator_mesh1->setStep(3);
	d_bdry_sched_advance14[ln]->fillData(current_time + simPlat_dt, false);
	if (d_refinedTimeStepping) {
		if (!hierarchy->finerLevelExists(ln) && last_step) {
			int currentLevelNumber = ln;
			while (currentLevelNumber > 0 && current_iteration[currentLevelNumber] % hierarchy->getRatioToCoarserLevel(currentLevelNumber).max() == 0) {
				d_coarsen_schedule[currentLevelNumber]->coarsenData();
				d_bdry_sched_postCoarsen[currentLevelNumber - 1]->fillData(current_time, false);
				currentLevelNumber--;
			}
		}
	} else {
		if (ln > 0) {
			d_coarsen_schedule[ln]->coarsenData();
			d_bdry_sched_postCoarsen[ln - 1]->fillData(current_time, false);
		}
	}
	

	t_step->stop();


	//Output
	t_output->start();
	if (ln == hierarchy->getFinestLevelNumber() && viz_mesh_dump_interval > 0) {
		if (previous_iteration < next_mesh_dump_iteration && outputCycle >= next_mesh_dump_iteration) {
			d_visit_data_writer->writePlotData(hierarchy, outputCycle, new_time);
			next_mesh_dump_iteration = next_mesh_dump_iteration + viz_mesh_dump_interval;
			while (outputCycle >= next_mesh_dump_iteration) {
				next_mesh_dump_iteration = next_mesh_dump_iteration + viz_mesh_dump_interval;
			}
		}
	}
	//Integration output
	if (ln == hierarchy->getFinestLevelNumber() && d_integration_output_period.size() > 0) {
		int i = 0;
		for (std::vector<std::shared_ptr<IntegrateDataWriter> >::iterator it = d_integrateDataWriters.begin(); it != d_integrateDataWriters.end(); ++it) {
			if (d_integration_output_period[i] > 0 && previous_iteration < next_integration_dump_iteration[i] && outputCycle >= next_integration_dump_iteration[i]) {
				(*it)->writePlotData(hierarchy, outputCycle, new_time);
				next_integration_dump_iteration[i] = next_integration_dump_iteration[i] + d_integration_output_period[i];
				while (outputCycle >= next_integration_dump_iteration[i]) {
					next_integration_dump_iteration[i] = next_integration_dump_iteration[i] + d_integration_output_period[i];
				}
			}
			i++;
		}
	}

	//Point output
	if (ln == hierarchy->getFinestLevelNumber() && d_point_output_period.size() > 0) {
		int i = 0;
		for (std::vector<std::shared_ptr<PointDataWriter> >::iterator it = d_pointDataWriters.begin(); it != d_pointDataWriters.end(); ++it) {
			if (d_point_output_period[i] > 0 && previous_iteration < next_point_dump_iteration[i] && outputCycle >= next_point_dump_iteration[i]) {
				(*it)->writePlotData(hierarchy, outputCycle, new_time);
				next_point_dump_iteration[i] = next_point_dump_iteration[i] + d_point_output_period[i];
				while (outputCycle >= next_point_dump_iteration[i]) {
					next_point_dump_iteration[i] = next_point_dump_iteration[i] + d_point_output_period[i];
				}
			}
			i++;
		}
	}


	t_output->stop();

	if (mpi.getRank() == 0 && d_output_interval > 0 ) {
		if (previous_iteration < next_console_output && outputCycle >= next_console_output) {
			int currentLevelNumber = ln;
			while (currentLevelNumber > 0) {
				currentLevelNumber--;
				cout <<"  ";
			}

			cout << "Level "<<ln<<". Iteration " << current_iteration[ln]<<". Time "<<current_time<<"."<< endl;
			if (ln == hierarchy->getFinestLevelNumber()) {
				next_console_output = next_console_output + d_output_interval;
				while (outputCycle >= next_console_output) {
					next_console_output = next_console_output + d_output_interval;
				}
			
			}
		}
	}

	if (d_timer_output_interval > 0 ) {
		if (previous_iteration < next_timer_output && outputCycle >= next_timer_output) {
			if (ln == hierarchy->getFinestLevelNumber()) {
				//Print timers
				if (mpi.getRank() == 0) {
					tbox::TimerManager::getManager()->print(cout);
				}
				else {
					if (ln == hierarchy->getFinestLevelNumber()) {
						//Dispose other processor timers
						//SAMRAI needs all processors run tbox::TimerManager::getManager()->print, otherwise it hungs
						std::ofstream ofs;
						ofs.setstate(std::ios_base::badbit);
						tbox::TimerManager::getManager()->print(ofs);
					}
				}
				next_timer_output = next_timer_output + d_timer_output_interval;
				while (outputCycle >= next_timer_output) {
					next_timer_output = next_timer_output + d_timer_output_interval;
				}
			
			}
		}
	}

	return simPlat_dt;
}

/*
 * Checks the finalization conditions              
 */
bool Problem::checkFinalization(const double current_time, const double simPlat_dt)
{
	if (greaterEq(current_time, tend)) { 
		return true;
	}
	return false;
	
	

}

void Problem::putToRestart(MainRestartData& mrd) {
	mrd.setNextMeshDumpIteration(next_mesh_dump_iteration);
	mrd.setNextIntegrationDumpIteration(next_integration_dump_iteration);
	mrd.setNextPointDumpIteration(next_point_dump_iteration);

	mrd.setCurrentIteration(current_iteration);
	mrd.setNextConsoleOutputIteration(next_console_output);
	mrd.setNextTimerOutputIteration(next_timer_output);
}

void Problem::getFromRestart(MainRestartData& mrd) {
	next_mesh_dump_iteration = mrd.getNextMeshDumpIteration();
	next_integration_dump_iteration = mrd.getNextIntegrationDumpIteration();
	next_point_dump_iteration = mrd.getNextPointDumpIteration();

	current_iteration = mrd.getCurrentIteration();
	next_console_output = mrd.getNextConsoleOutputIteration();
	next_timer_output = mrd.getNextTimerOutputIteration();
}

void Problem::allocateAfterRestart() {
	for (int il = 0; il < d_patch_hierarchy->getNumberOfLevels(); il++) {
		std::shared_ptr< hier::PatchLevel > level(d_patch_hierarchy->getPatchLevel(il));
		level->allocatePatchData(d_mask_id);
		level->allocatePatchData(d_interior_regridding_value_id);
		level->allocatePatchData(d_nonSync_regridding_tag_id);
		level->allocatePatchData(d_interior_i_id);
		level->allocatePatchData(d_interior_j_id);
		level->allocatePatchData(d_rk1Alpha_id);
		level->allocatePatchData(d_rk1Phi_id);
		level->allocatePatchData(d_rk1phix_id);
		level->allocatePatchData(d_rk1phiy_id);
		level->allocatePatchData(d_rk1K_id);
		level->allocatePatchData(d_rk2Alpha_id);
		level->allocatePatchData(d_rk2Phi_id);
		level->allocatePatchData(d_rk2phix_id);
		level->allocatePatchData(d_rk2phiy_id);
		level->allocatePatchData(d_rk2K_id);
		level->allocatePatchData(d_Alpha_p_id);
		level->allocatePatchData(d_Phi_p_id);
		level->allocatePatchData(d_phix_p_id);
		level->allocatePatchData(d_phiy_p_id);
		level->allocatePatchData(d_K_p_id);
	}

}

/*
 *  Cell tagging routine - tag cells that require refinement based on a provided condition. 
 */
void Problem::applyGradientDetector(
   const std::shared_ptr< hier::PatchHierarchy >& hierarchy, 
   const int level_number,
   const double time, 
   const int tag_index,
   const bool initial_time,
   const bool uses_richardson_extrapolation_too) 
{
	if (d_regridding && level_number + 1 >= d_regridding_min_level && level_number + 1 <= d_regridding_max_level) {
		hier::VariableDatabase *vdb = hier::VariableDatabase::getDatabase();
		if (!(vdb->checkVariableExists(d_regridding_field))) {
			TBOX_ERROR(d_object_name << ": Regridding field selected not found:n"					<<  d_regridding_field<<  "n");
		}

		std::shared_ptr< hier::PatchLevel > level(hierarchy->getPatchLevel(level_number));

		for (hier::PatchLevel::iterator ip(level->begin()); ip != level->end(); ++ip) {
			const std::shared_ptr< hier::Patch >& patch = *ip;
			int* tags = ((pdat::CellData<int> *) patch->getPatchData(tag_index).get())->getPointer();
			int* regridding_tag = ((pdat::NodeData<int> *) patch->getPatchData(d_nonSync_regridding_tag_id).get())->getPointer();
			const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
			const double* dx  = patch_geom->getDx();
			const hier::Index tfirst = patch->getPatchData(tag_index)->getGhostBox().lower();
			const hier::Index tlast  = patch->getPatchData(tag_index)->getGhostBox().upper();
			const hier::Index boxfirst = patch->getBox().lower();
			const hier::Index boxlast  = patch->getBox().upper();
			int ilast = boxlast(0)-boxfirst(0)+2+2*d_ghost_width;
			int itlast = tlast(0)-tfirst(0)+1;
			int jlast = boxlast(1)-boxfirst(1)+2+2*d_ghost_width;
			int jtlast = tlast(1)-tfirst(1)+1;
			for(int index1 = 0; index1 < (tlast(1)-tfirst(1))+1; index1++) {
				for(int index0 = 0; index0 < (tlast(0)-tfirst(0))+1; index0++) {
					vectorT(tags,index0, index1) = 0;
				}
			}
			for(int index1 = 0; index1 < jlast; index1++) {
				for(int index0 = 0; index0 < ilast; index0++) {
					vector(regridding_tag,index0, index1) = 0;
				}
			}
			if (vdb->checkVariableExists(d_regridding_field)) {
				//Mesh
				if (d_regridding_type == "GRADIENT") {
					int regrid_field_id = vdb->getVariable(d_regridding_field)->getInstanceIdentifier();
					double* regrid_field = ((pdat::NodeData<double> *) patch->getPatchData(regrid_field_id).get())->getPointer();
					double* regridding_value = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_regridding_value_id).get())->getPointer();
					for(int index1 = 0; index1 < jlast - 1; index1++) {
						for(int index0 = 0; index0 < ilast - 1; index0++) {
							if (vector(regrid_field, index0, index1)!=0) {
								if ((fabs(vector(regrid_field, index0+1+d_ghost_width, index1+d_ghost_width)-vector(regrid_field, index0+d_ghost_width, index1+d_ghost_width)) > d_regridding_compressionFactor * MIN(fabs(vector(regrid_field, index0+2+d_ghost_width, index1+d_ghost_width)-vector(regrid_field, index0+1+d_ghost_width, index1+d_ghost_width)) + d_regridding_mOffset * pow(dx[0], 2), fabs(vector(regrid_field, index0+d_ghost_width, index1+d_ghost_width)-vector(regrid_field, index0-1+d_ghost_width, index1+d_ghost_width)) + d_regridding_mOffset * pow(dx[0], 2)) ) ||(fabs(vector(regrid_field, index0+d_ghost_width, index1+1+d_ghost_width)-vector(regrid_field, index0+d_ghost_width, index1+d_ghost_width)) > d_regridding_compressionFactor * MIN(fabs(vector(regrid_field, index0+d_ghost_width, index1+2+d_ghost_width)-vector(regrid_field, index0+d_ghost_width, index1+1+d_ghost_width)) + d_regridding_mOffset * pow(dx[1], 2), fabs(vector(regrid_field, index0+d_ghost_width, index1+d_ghost_width)-vector(regrid_field, index0+d_ghost_width, index1-1+d_ghost_width)) + d_regridding_mOffset * pow(dx[1], 2)) ) ) {
									if (index0 >= d_ghost_width && index0 < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1 >= d_ghost_width && index1 < (tlast(1)-tfirst(1))+1 + d_ghost_width) {
										vectorT(tags,index0 - d_ghost_width, index1 - d_ghost_width) = 1;
									}
									vector(regridding_tag,index0, index1) = 1;
									//SAMRAI tagging
									if (index0 > d_ghost_width && index0 < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1 > d_ghost_width && index1 < (tlast(1)-tfirst(1))+1 + d_ghost_width) {
										vectorT(tags, index0-d_ghost_width - 1, index1-d_ghost_width - 1) = 1;
									}
									if (index0 > d_ghost_width && index0 < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1 >= d_ghost_width && index1 < (tlast(1)-tfirst(1))+1 + d_ghost_width) {
										vectorT(tags, index0-d_ghost_width - 1, index1-d_ghost_width) = 1;
									}
									if (index0 >= d_ghost_width && index0 < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1 > d_ghost_width && index1 < (tlast(1)-tfirst(1))+1 + d_ghost_width) {
										vectorT(tags, index0-d_ghost_width, index1-d_ghost_width - 1) = 1;
									}
									//Informative tagging
									if (index0 > 0 && index1 > 0) {
										if (vector(regridding_tag,index0-1,index1-1) != 1)
											vector(regridding_tag,index0-1,index1-1) = 1;
									}
									if (index0 > 0) {
										if (vector(regridding_tag,index0-1,index1) != 1)
											vector(regridding_tag,index0-1,index1) = 1;
									}
									if (index1 > 0) {
										if (vector(regridding_tag,index0,index1-1) != 1)
											vector(regridding_tag,index0,index1-1) = 1;
									}
									if (d_regridding_buffer > 0) {
										int distance;
										for(int index1b = MAX(0, index1 - d_regridding_buffer - 1); index1b < MIN(index1 + d_regridding_buffer + 1, jlast); index1b++) {
											for(int index0b = MAX(0, index0 - d_regridding_buffer - 1); index0b < MIN(index0 + d_regridding_buffer + 1, ilast); index0b++) {
												int distx = (index0b - index0);
												if (distx < 0) {
													distx++;
												}
												int disty = (index1b - index1);
												if (disty < 0) {
													disty++;
												}
												distance = 1 + MAX(abs(distx), abs(disty));
												if (index0b >= d_ghost_width && index0b < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1b >= d_ghost_width && index1b < (tlast(1)-tfirst(1))+1 + d_ghost_width) {
													vectorT(tags,index0b-d_ghost_width,index1b-d_ghost_width) = 1;
												}
												if (vector(regridding_tag,index0b,index1b) == 0 || vector(regridding_tag,index0b,index1b) > distance) {
													vector(regridding_tag,index0b,index1b) = distance;
												}
											}
										}
									}
			
								}
							}
						}
					}
		
				} else {
					if (d_regridding_type == "FUNCTION") {
						int regrid_field_id = vdb->getVariable(d_regridding_field)->getInstanceIdentifier();
						double* regrid_field = ((pdat::NodeData<double> *) patch->getPatchData(regrid_field_id).get())->getPointer();
						double* regridding_value = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_regridding_value_id).get())->getPointer();
						for(int index1 = 0; index1 < jlast; index1++) {
							for(int index0 = 0; index0 < ilast; index0++) {
								vector(regridding_value, index0, index1) = vector(regrid_field, index0, index1);
								if (vector(regrid_field, index0, index1) > d_regridding_threshold) {
									if (index0 >= d_ghost_width && index0 < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1 >= d_ghost_width && index1 < (tlast(1)-tfirst(1))+1 + d_ghost_width) {
										vectorT(tags,index0 - d_ghost_width, index1 - d_ghost_width) = 1;
									}
									vector(regridding_tag,index0, index1) = 1;
									//SAMRAI tagging
									if (index0 > d_ghost_width && index0 < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1 > d_ghost_width && index1 < (tlast(1)-tfirst(1))+1 + d_ghost_width) {
										vectorT(tags, index0-d_ghost_width - 1, index1-d_ghost_width - 1) = 1;
									}
									if (index0 > d_ghost_width && index0 < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1 >= d_ghost_width && index1 < (tlast(1)-tfirst(1))+1 + d_ghost_width) {
										vectorT(tags, index0-d_ghost_width - 1, index1-d_ghost_width) = 1;
									}
									if (index0 >= d_ghost_width && index0 < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1 > d_ghost_width && index1 < (tlast(1)-tfirst(1))+1 + d_ghost_width) {
										vectorT(tags, index0-d_ghost_width, index1-d_ghost_width - 1) = 1;
									}
									//Informative tagging
									if (index0 > 0 && index1 > 0) {
										if (vector(regridding_tag,index0-1,index1-1) != 1)
											vector(regridding_tag,index0-1,index1-1) = 1;
									}
									if (index0 > 0) {
										if (vector(regridding_tag,index0-1,index1) != 1)
											vector(regridding_tag,index0-1,index1) = 1;
									}
									if (index1 > 0) {
										if (vector(regridding_tag,index0,index1-1) != 1)
											vector(regridding_tag,index0,index1-1) = 1;
									}
									if (d_regridding_buffer > 0) {
										int distance;
										for(int index1b = MAX(0, index1 - d_regridding_buffer - 1); index1b < MIN(index1 + d_regridding_buffer + 1, jlast); index1b++) {
											for(int index0b = MAX(0, index0 - d_regridding_buffer - 1); index0b < MIN(index0 + d_regridding_buffer + 1, ilast); index0b++) {
												int distx = (index0b - index0);
												if (distx < 0) {
													distx++;
												}
												int disty = (index1b - index1);
												if (disty < 0) {
													disty++;
												}
												distance = 1 + MAX(abs(distx), abs(disty));
												if (index0b >= d_ghost_width && index0b < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1b >= d_ghost_width && index1b < (tlast(1)-tfirst(1))+1 + d_ghost_width) {
													vectorT(tags,index0b-d_ghost_width,index1b-d_ghost_width) = 1;
												}
												if (vector(regridding_tag,index0b,index1b) == 0 || vector(regridding_tag,index0b,index1b) > distance) {
													vector(regridding_tag,index0b,index1b) = distance;
												}
											}
										}
									}
								}
							}
						}
			
					} else {
						if (d_regridding_type == "SHADOW") {
							if (!initial_time) {
								if (!(vdb->checkVariableExists(d_regridding_field_shadow))) {
									TBOX_ERROR(d_object_name << ": Regridding field selected not found:" <<  d_regridding_field_shadow<<  "");
								}
								int regrid_field_id = vdb->getVariable(d_regridding_field)->getInstanceIdentifier();
								double* regrid_field1 = ((pdat::NodeData<double> *) patch->getPatchData(regrid_field_id).get())->getPointer();
								int regrid_field_shadow_id = vdb->getVariable(d_regridding_field_shadow)->getInstanceIdentifier();
								double* regrid_field2 = ((pdat::NodeData<double> *) patch->getPatchData(regrid_field_shadow_id).get())->getPointer();
								double* regridding_value = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_regridding_value_id).get())->getPointer();
								for(int index1 = 0; index1 < jlast; index1++) {
									for(int index0 = 0; index0 < ilast; index0++) {
					
										double error = 2 * fabs(vector(regrid_field1, index0, index1) - vector(regrid_field2, index0, index1))/fabs(vector(regrid_field1, index0, index1) + vector(regrid_field2, index0, index1));
										vector(regridding_value, index0, index1) = error;
										if (error > d_regridding_error) {
											if (index0 >= d_ghost_width && index0 < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1 >= d_ghost_width && index1 < (tlast(1)-tfirst(1))+1 + d_ghost_width) {
												vectorT(tags,index0 - d_ghost_width, index1 - d_ghost_width) = 1;
											}
											vector(regridding_tag,index0, index1) = 1;
											//SAMRAI tagging
											if (index0 > d_ghost_width && index0 < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1 > d_ghost_width && index1 < (tlast(1)-tfirst(1))+1 + d_ghost_width) {
												vectorT(tags, index0-d_ghost_width - 1, index1-d_ghost_width - 1) = 1;
											}
											if (index0 > d_ghost_width && index0 < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1 >= d_ghost_width && index1 < (tlast(1)-tfirst(1))+1 + d_ghost_width) {
												vectorT(tags, index0-d_ghost_width - 1, index1-d_ghost_width) = 1;
											}
											if (index0 >= d_ghost_width && index0 < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1 > d_ghost_width && index1 < (tlast(1)-tfirst(1))+1 + d_ghost_width) {
												vectorT(tags, index0-d_ghost_width, index1-d_ghost_width - 1) = 1;
											}
											//Informative tagging
											if (index0 > 0 && index1 > 0) {
												if (vector(regridding_tag,index0-1,index1-1) != 1)
													vector(regridding_tag,index0-1,index1-1) = 1;
											}
											if (index0 > 0) {
												if (vector(regridding_tag,index0-1,index1) != 1)
													vector(regridding_tag,index0-1,index1) = 1;
											}
											if (index1 > 0) {
												if (vector(regridding_tag,index0,index1-1) != 1)
													vector(regridding_tag,index0,index1-1) = 1;
											}
											if (d_regridding_buffer > 0) {
												int distance;
												for(int index1b = MAX(0, index1 - d_regridding_buffer - 1); index1b < MIN(index1 + d_regridding_buffer + 1, jlast); index1b++) {
													for(int index0b = MAX(0, index0 - d_regridding_buffer - 1); index0b < MIN(index0 + d_regridding_buffer + 1, ilast); index0b++) {
														int distx = (index0b - index0);
														if (distx < 0) {
															distx++;
														}
														int disty = (index1b - index1);
														if (disty < 0) {
															disty++;
														}
														distance = 1 + MAX(abs(distx), abs(disty));
														if (index0b >= d_ghost_width && index0b < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1b >= d_ghost_width && index1b < (tlast(1)-tfirst(1))+1 + d_ghost_width) {
															vectorT(tags,index0b-d_ghost_width,index1b-d_ghost_width) = 1;
														}
														if (vector(regridding_tag,index0b,index1b) == 0 || vector(regridding_tag,index0b,index1b) > distance) {
															vector(regridding_tag,index0b,index1b) = distance;
														}
													}
												}
											}
					
										}
									}
								}
					
							}
				
						}
					}
				}
			}
		}
		//Buffer synchronization if needed
		if (d_regridding_buffer > d_ghost_width) {
			d_tagging_fill->createSchedule(level)->fillData(0, false);
		}
		if (d_regridding_buffer > 0) {
			for (hier::PatchLevel::iterator ip(level->begin()); ip != level->end(); ++ip) {
				const std::shared_ptr< hier::Patch >& patch = *ip;
				int* tags = ((pdat::CellData<int> *) patch->getPatchData(tag_index).get())->getPointer();
				int* regridding_tag = ((pdat::NodeData<int> *) patch->getPatchData(d_nonSync_regridding_tag_id).get())->getPointer();
				const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
				const double* dx  = patch_geom->getDx();
				const hier::Index tfirst = patch->getPatchData(tag_index)->getGhostBox().lower();
				const hier::Index tlast  = patch->getPatchData(tag_index)->getGhostBox().upper();
				const hier::Index boxfirst = patch->getBox().lower();
				const hier::Index boxlast  = patch->getBox().upper();
				int ilast = boxlast(0)-boxfirst(0)+2+2*d_ghost_width;
				int itlast = tlast(0)-tfirst(0)+1;
				int jlast = boxlast(1)-boxfirst(1)+2+2*d_ghost_width;
				int jtlast = tlast(1)-tfirst(1)+1;
	
				for(int index1 = 0; index1 < jlast; index1++) {
					for(int index0 = 0; index0 < ilast; index0++) {
	
						int value = vector(regridding_tag, index0, index1);
						if (value > 0 && value < 1 + d_regridding_buffer) {
							int buffer_left = 1 + d_regridding_buffer - value;
							int distance;
							for(int index1b = MAX(0, index1 - buffer_left); index1b < MIN(index1 + buffer_left + 1, jlast); index1b++) {
								for(int index0b = MAX(0, index0 - buffer_left); index0b < MIN(index0 + buffer_left + 1, ilast); index0b++) {
							
									distance = MAX(abs(index0b - index0), abs(index1b - index1));
									if (distance > 0 && index0b >= d_ghost_width && index0b < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1b >= d_ghost_width && index1b < (tlast(1)-tfirst(1))+1 + d_ghost_width) {
										vectorT(tags, index0b - d_ghost_width, index1b - d_ghost_width) = 1;
									}
									if (distance > 0 && (vector(regridding_tag,index0b, index1b) == 0 || vector(regridding_tag, index0b, index1b) > value + distance)) {
										vector(regridding_tag, index0b, index1b) = value + distance;
									}
								}
							}
							
						}
					}
				}
	
			}
		}
	}
}



