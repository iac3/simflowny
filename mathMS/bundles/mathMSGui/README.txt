----------------------------------------------------------------------------------
------------------SIMFLOWNY GUI README--------------------------------------------
----------------------------------------------------------------------------------

La GUI de simflowny está implementada enteramente en lenguaje javascript, las principales librerías utilizadas para su desarrollo son:

1.EXT JS version 4.2.1 (http://docs.sencha.com/extjs/4.2.1/)
2.JQUERY 1.10.2 /code.jquery.com/jquery-1.10.2.js
3.PURE.js
4.AsciiMathParser.js
5.MathJax.js
6.jsonpath-0.8.0.js

------------------------------------------------------------
--ESTRUCTURA DE DIRECTORIOS GUI-----------------------------
------------------------------------------------------------

La GUI sigue la estructura estándar de un proyecto de EXT JS, lo más importante a tener en cuenta es que hay 2 proyectos EXT JS completos:

1.El gestor de documentos --> Carpeta princial, es la pantalla inicial de la Gui, su fichero de entrada es "index.html". Es un Proyecto EXT JS completo, las carpetas que conforman este proyecto son:
	--->"app": código fuente en javascript del Gui
	--->"data": Datos de entrada de ejemplo del Gui
	--->"extjs": librería usada por el Gui
	--->"META-INF": definicion de metadatos
	--->"resources": Imágenes y librerías javascript usadas en el Gui.
	--->"WEB-INF": Definición de la aplicación web.
	--->El resto de carpetas son definidas más abajo, pero no forman propiamente parte del Gui como proyecto EXT JS.

2.Editor de documentos --> Carpeta "xmlEditor", dentro el fichero de entrada es "index.html", esta carpeta es en sí un proyecto EXT JS independiente del gestor de documentos, las subcarpetas que forma parte de él son:
	--->"app": código fuente en javascript del Gui
	--->"data":Datos de entrada de ejemplo del Gui
	--->"resources":Imágenes, xsl y css usados por el Gui.
	--->Todas las librerías javascript usadas se cogen de la carpeta principal de "resources" o "extjs" del apartado anterior, así no hace falta copiaras de nuevo dentro de este otro proyecto.


------------------------------------------------------------
--ESTRUCTURA DE DIRECTORIOS GESTOR DE DOCUMENTOS(GUI)-------
------------------------------------------------------------

Estos directorios y ficheros son los principales del proyecto ext js:
-->"app":Código principal del gestor documental, escrito en js
------"controller": Contiene funciones EXT JS que controlan eventos en los datos o el Gui
------"global": Variables globales de EXT JS en todo el Gui
------"model": Modelo de datos en memoria de la lista de documentos que se muestran en la Gui, viene a ser cómo se guarda el json de documentos en memoria.
------"store": Variables que guardan los datos que vienen del servidor en la Gui, los datos son el arbol de directorios y el listado de documentos
------"app.js": Programa principal del GUI, este es el que llama a todos los demás que están en la siguiente carpeta
------"view": Tiene todos los progrmas en javascript que pintan la Gui y gestionan todos los eventos de usuario
------------"Viewport.js": Funcion principal de pintado del GUI
------------"MainTree.js": Pinta y gestiona el arbol de directorios.
------------"XMLDocumentsList.js": Pinta y gestiona la lista de documentos en el Gui
-->"data": Contiene ficheros .json de ejemplo sobre el gestor de documentos, tiene propositos de desarrollo y debug, son los usados por el código del Gui internamente.
-->"extjs": Toda la librería de EXT JS 4.2.1 utilizada en la Gui en la carpeta "app".
-->"resources": imágenes que se utilizan en el Gui, los que se visualizan y que se crean a partir de los que hay en "image-edition".
-->"WEB-INF": Metadatos asociados a la Gui para poder ser subida a un servidor


------------------------------------------------------------
--RESTO DE DIRECTORIOS--------------------------------------
------------------------------------------------------------

Estos directorios están en la carpeta del Gui pero no forman parte del proyecto EXT JS en sí:
-->"image-edition": Todos los iconos e imagenes usados en el gestor de documentos, pero sólo templates, no se visualizan en el Gui, son sólo para hacer el diseño.
-->"test": Tests unitarios de la GUI utilizando Sikuli
-->"xmlEditor": Proyecto EXT JS entero que se encarga de editar un documento xml en el Gui, visto en el primer apartado.





