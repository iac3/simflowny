<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- Edited by XMLSpy� -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:mt="http://www.w3.org/1998/Math/MathML" xmlns:s="urn:simml">

  <xsl:param name="indent" select="0"/>

  
  <xsl:template match="mt:math">
    <xsl:param name="indent" select="$indent"/>
    <xsl:if test="$indent > 0">
<div style='padding-left:{$indent}px'>
    <xsl:copy>
      <xsl:for-each select=".">
        <xsl:apply-templates/>
      </xsl:for-each>
    </xsl:copy>
</div>
    </xsl:if>
    <xsl:if test="$indent = 0">
        <xsl:copy>
          <xsl:for-each select=".">
            <xsl:apply-templates/>
          </xsl:for-each>
        </xsl:copy>
    </xsl:if>
  </xsl:template>
  
  <xsl:template match="mt:*" priority='-1'>
      <xsl:copy><xsl:for-each select="."><xsl:apply-templates/></xsl:for-each></xsl:copy>
  </xsl:template>
  
  <xsl:template match="mt:ci/mt:msup[*[position() = 1 and name() = 'mt:ci']/s:field and *[position() = 2 and name() = 'mt:apply'][mt:plus and mt:ci/s:timeCoordinate and mt:cn[text() = '1']]]" priority="9">
    <xsl:text>$f_n</xsl:text>
  </xsl:template>
  
  <xsl:template match="mt:ci/mt:msup[*[position() = 1 and name() = 'mt:ci']/s:field and *[position() = 2 and name() = 'mt:ci']/s:timeCoordinate]" priority="9">
    <xsl:text>$f_p</xsl:text>
  </xsl:template>
  
  <xsl:template match="mt:ci/mt:msup[*[position() = 1 and name() = 'mt:ci']/s:field and *[position() = 2 and name() = 'mt:apply'][mt:minus and mt:ci/s:timeCoordinate and mt:cn]]" priority="9">
    <xsl:text>$f</xsl:text><xsl:call-template name="recPrint"><xsl:with-param name="counter" select=".//mt:cn/text()+1"/><xsl:with-param name="value" select='"_p"'/></xsl:call-template>
  </xsl:template>
  
   <xsl:template match="mt:ci/mt:msup[*[position() = 1 and name() = 'mt:ci']/s:particlePosition and *[position() = 2 and name() = 'mt:apply'][mt:plus and mt:ci/s:timeCoordinate and mt:cn[text() = '1']]]" priority="9">
      <xsl:text>$pp_n</xsl:text>
   </xsl:template>
   
   <xsl:template match="mt:ci/mt:msup[*[position() = 1 and name() = 'mt:ci']/s:particlePosition and *[position() = 2 and name() = 'mt:ci']/s:timeCoordinate]" priority="9">
      <xsl:text>$pp_p</xsl:text>
   </xsl:template>
   
   <xsl:template match="mt:ci/mt:msup[*[position() = 1 and name() = 'mt:ci']/s:particlePosition and *[position() = 2 and name() = 'mt:apply'][mt:minus and mt:ci/s:timeCoordinate and mt:cn]]" priority="9">
      <xsl:text>$pp</xsl:text><xsl:call-template name="recPrint"><xsl:with-param name="counter" select=".//mt:cn/text()+1"/><xsl:with-param name="value" select='"_p"'/></xsl:call-template>
   </xsl:template>
            
	<xsl:template match="s:randomNumber">
	  <mt:ci>$rnd_<xsl:value-of select="@typeAtt"/>
	  <xsl:if test="./@rangeMinAtt">
	    <xsl:text>_</xsl:text><xsl:value-of select="@rangeMinAtt"/>
	  </xsl:if>
	    <xsl:if test="./@rangeMaxAtt">
	      <xsl:text>_</xsl:text><xsl:value-of select="@rangeMaxAtt"/>
	    </xsl:if></mt:ci>
	</xsl:template>
 
   <xsl:template match="s:particleDistance">
      <mt:ci>$pd</mt:ci>
   </xsl:template>
 
   <xsl:template match="s:dimensions">
      <mt:ci>$dim</mt:ci>
   </xsl:template>
 
  <xsl:template match="s:incrementCoordinate">
    <mt:ci>$ic_<xsl:value-of select="text()"/></mt:ci>
  </xsl:template>
  
  <xsl:template match="s:decrementCoordinate">
    <mt:ci>$dc_<xsl:value-of select="text()"/></mt:ci>
  </xsl:template>
  
  <xsl:template match="s:incrementCoordinate2">
    <mt:ci>$ic2_<xsl:value-of select="text()"/></mt:ci>
  </xsl:template>
  
  <xsl:template match="s:decrementCoordinate2">
    <mt:ci>$dc2_<xsl:value-of select="text()"/></mt:ci>
  </xsl:template>
  
  <xsl:template match="s:incrementCoordinate1IncrementCoordinate2">
    <mt:ci>$icic_<xsl:value-of select="substring-before(text(),',')"/><xsl:text>_</xsl:text><xsl:value-of select="substring-after(text(),',')"/></mt:ci>
  </xsl:template>
  
  <xsl:template match="s:incrementCoordinate1DecrementCoordinate2">
    <mt:ci>$icdc_<xsl:value-of select="substring-before(text(),',')"/><xsl:text>_</xsl:text><xsl:value-of select="substring-after(text(),',')"/></mt:ci>
  </xsl:template>
  
  <xsl:template match="s:decrementCoordinate1IncrementCoordinate2">
    <mt:ci>$dcic_<xsl:value-of select="substring-before(text(),',')"/><xsl:text>_</xsl:text><xsl:value-of select="substring-after(text(),',')"/></mt:ci>
  </xsl:template>
  
  <xsl:template match="s:decrementCoordinate1DecrementCoordinate2">
    <mt:ci>$dcdc_<xsl:value-of select="substring-before(text(),',')"/><xsl:text>_</xsl:text><xsl:value-of select="substring-after(text(),',')"/></mt:ci>
  </xsl:template>

  <xsl:template match="s:iterationNumber">
    <mt:ci>$in</mt:ci>
  </xsl:template>
  
  <xsl:template match="s:currentTime">
    <mt:ci>$ct</mt:ci>
  </xsl:template>

  <xsl:template match="s:newTime">
    <mt:ci>$nt</mt:ci>
  </xsl:template>
  
  <xsl:template match="s:refinementRatio">
    <mt:ci>$rr</mt:ci>
  </xsl:template>
  
  <xsl:template match="s:substepNumber">
    <mt:ci>$sn</mt:ci>
  </xsl:template>

   <xsl:template match="s:timeSubstepNumber">
      <mt:ci>$tsn</mt:ci>
   </xsl:template>

  <xsl:template match="s:timeIncrement">
    <mt:ci>$ti</mt:ci>
  </xsl:template>
  
  <xsl:template match="s:influenceRadius">
    <mt:ci>$ir</mt:ci>
  </xsl:template>
  
  <xsl:template match="s:smoothingLength">
    <mt:ci>$sl</mt:ci>
  </xsl:template>
    
  <xsl:template match="s:currentCell">
    <mt:ci>$cc</mt:ci>
  </xsl:template>

  <xsl:template match="s:currentAgent">
    <mt:ci>$ca</mt:ci>
  </xsl:template>
  <xsl:template match="s:currentVertex">
    <mt:ci>$cv</mt:ci>
  </xsl:template>
		
  <xsl:template match="s:currentEdge">
    <mt:ci>$ce</mt:ci>
  </xsl:template>
   
   <xsl:template match="s:particleSeparationProduct">
      <mt:ci>$psp</mt:ci>
   </xsl:template>
   
   <xsl:template match="s:kernel">
      <mt:ci>$k</mt:ci>
   </xsl:template>
   
   <xsl:template match="s:kernelGradient">
      <mt:ci>$kg</mt:ci>
   </xsl:template>
   
  <xsl:template match="s:contTimeCoordinate">
    <xsl:text>$ctc</xsl:text>
  </xsl:template>
  
  <xsl:template match="s:interactionRegionId">
    <mt:ci>$irid</mt:ci>
  </xsl:template>
  
  <xsl:template match="s:regionInteriorId">
    <mt:ci>$riid</mt:ci>
  </xsl:template>
  
  <xsl:template match="s:regionSurfaceId">
    <mt:ci>$rsid</mt:ci>
  </xsl:template>
  
   <xsl:template match="s:particlePosition">
      <mt:ci>$pp</mt:ci>
   </xsl:template>
   
   <xsl:template match="s:cellPosition">
      <mt:ci>$cp</mt:ci>
   </xsl:template>
  
   <xsl:template match="s:particleVelocity">
      <mt:ci>$pvel</mt:ci>
   </xsl:template>
   
  <xsl:template match="s:timeCoordinate">
    <xsl:text>$t</xsl:text>
  </xsl:template>
    
  <xsl:template match="s:contSpatialCoordinate">
    <xsl:text>$x</xsl:text>
  </xsl:template>
  
  <xsl:template match="s:secondContSpatialCoordinate">
    <xsl:text>$x2</xsl:text>
  </xsl:template>
  
  <xsl:template match="s:spatialCoordinate">
    <xsl:text>$i</xsl:text>
  </xsl:template>
  
  <xsl:template match="s:secondSpatialCoordinate">
    <xsl:text>$i2</xsl:text>
  </xsl:template>
  
  <xsl:template match="s:parabolicTermsVariable">
    <xsl:text>$ptv</xsl:text>
  </xsl:template>
  
  <xsl:template match="s:neighbourAgent">
    <mt:ci>$na</mt:ci>
  </xsl:template>
  
  <xsl:template match="s:eigenVector">
    <xsl:text>$v</xsl:text>
  </xsl:template>
  
  <xsl:template match="s:edgeTarget">
    <mt:apply>
      <mt:ci>$et</mt:ci>
		<xsl:apply-templates select="./*">
		</xsl:apply-templates>
    </mt:apply>
	</xsl:template>
	
  <xsl:template match="s:edgeSource">
    <mt:apply>
      <mt:ci>es</mt:ci>
      <xsl:apply-templates select="./*">
      </xsl:apply-templates>
    </mt:apply>
	</xsl:template>

  <xsl:template match="s:flux">
    <mt:apply>
      <mt:ci>$flx</mt:ci>
      <xsl:apply-templates select="./*">
      </xsl:apply-templates>
    </mt:apply>
  </xsl:template>

    <xsl:template match="s:readFromFileQuintic">
        <mt:apply>
            <mt:ci>$rffq</mt:ci>
            <xsl:apply-templates select="./*">
            </xsl:apply-templates>
        </mt:apply>
    </xsl:template>
   
   <xsl:template match="s:readFromFileLinear1D">
      <mt:apply>
         <mt:ci>$rffl1d</mt:ci>
         <xsl:apply-templates select="./*">
         </xsl:apply-templates>
      </mt:apply>
   </xsl:template>
   
   <xsl:template match="s:readFromFileLinear2D">
      <mt:apply>
         <mt:ci>$rffl2d</mt:ci>
         <xsl:apply-templates select="./*">
         </xsl:apply-templates>
      </mt:apply>
   </xsl:template>
   
   <xsl:template match="s:readFromFileLinear3D">
      <mt:apply>
         <mt:ci>$rffl3d</mt:ci>
         <xsl:apply-templates select="./*">
         </xsl:apply-templates>
      </mt:apply>
   </xsl:template>
   
    <xsl:template match="s:readFromFileLinearWithDerivatives1D">
        <mt:apply>
            <mt:ci>$rfflwd1d</mt:ci>
            <xsl:apply-templates select="./*">
            </xsl:apply-templates>
        </mt:apply>
    </xsl:template>
   
   <xsl:template match="s:readFromFileLinearWithDerivatives2D">
      <mt:apply>
         <mt:ci>$rfflwd2d</mt:ci>
         <xsl:apply-templates select="./*">
         </xsl:apply-templates>
      </mt:apply>
   </xsl:template>
   
   <xsl:template match="s:readFromFileLinearWithDerivatives3D">
      <mt:apply>
         <mt:ci>$rfflwd3d</mt:ci>
         <xsl:apply-templates select="./*">
         </xsl:apply-templates>
      </mt:apply>
   </xsl:template>
   
   <xsl:template match="s:findCoordFromFile1D">
      <mt:apply>
         <mt:ci>$fcff1d</mt:ci>
         <xsl:apply-templates select="./*">
         </xsl:apply-templates>
      </mt:apply>
   </xsl:template>
   
   <xsl:template match="s:findCoordFromFile2D">
      <mt:apply>
         <mt:ci>$fcff2d</mt:ci>
         <xsl:apply-templates select="./*">
         </xsl:apply-templates>
      </mt:apply>
   </xsl:template>
   
   <xsl:template match="s:findCoordFromFile3D">
      <mt:apply>
         <mt:ci>$fcff3d</mt:ci>
         <xsl:apply-templates select="./*">
         </xsl:apply-templates>
      </mt:apply>
   </xsl:template>
   
  <xsl:template match="s:functionCall">
    <mt:apply>
      <mt:ci>$fc</mt:ci>
      <xsl:apply-templates select="./*">
      </xsl:apply-templates>
    </mt:apply>
  </xsl:template>
  
   <xsl:template match="s:particleVolume">
      <mt:apply>
         <mt:ci>$pv</mt:ci>
         <xsl:apply-templates select="./*">
         </xsl:apply-templates>
      </mt:apply>
   </xsl:template>
  
  <xsl:template match="s:sharedVariable">
    <mt:apply>
      <mt:ci>$sv</mt:ci>
      <xsl:apply-templates select="./*">
      </xsl:apply-templates>
    </mt:apply>
  </xsl:template>

  <xsl:template match="s:neighbourParticle">
    <mt:apply>
      <mt:ci>$np</mt:ci>
      <xsl:apply-templates select="./*">
      </xsl:apply-templates>
    </mt:apply>
  </xsl:template>
  
   <xsl:template match="s:sign">
      <mt:apply>
         <mt:ci>$sign</mt:ci>
         <xsl:apply-templates select="./*">
         </xsl:apply-templates>
      </mt:apply>
   </xsl:template>

  <xsl:template match="s:sources">
    <mt:apply>
      <mt:ci>$src</mt:ci>
      <xsl:apply-templates select="./*">
      </xsl:apply-templates>
    </mt:apply>
  </xsl:template>
  
  <xsl:template match="s:parabolicFirstDerivative">
    <mt:apply>
      <mt:ci>$pfd</mt:ci>
      <xsl:apply-templates select="./*">
      </xsl:apply-templates>
    </mt:apply>
  </xsl:template>
  
  <xsl:template match="s:parabolicSecondDerivative">
    <mt:apply>
      <mt:ci>$psd</mt:ci>
      <xsl:apply-templates select="./*">
      </xsl:apply-templates>
    </mt:apply>
  </xsl:template>
  
  <xsl:template match="s:parabolicTermsSumVariables">
    <mt:apply>
      <mt:ci>$pts</mt:ci>
      <xsl:apply-templates select="./*">
      </xsl:apply-templates>
    </mt:apply>
  </xsl:template>
  
  <xsl:template match="s:maxCharSpeed">
    <mt:apply>
      <mt:ci>$mcs</mt:ci>
      <xsl:apply-templates select="./*">
      </xsl:apply-templates>
    </mt:apply>
  </xsl:template>
  
   <xsl:template match="s:positiveCharSpeed">
      <mt:apply>
         <mt:ci>$pcs</mt:ci>
         <xsl:apply-templates select="./*">
         </xsl:apply-templates>
      </mt:apply>
   </xsl:template>
   
   <xsl:template match="s:negativeCharSpeed">
      <mt:apply>
         <mt:ci>$ncs</mt:ci>
         <xsl:apply-templates select="./*">
         </xsl:apply-templates>
      </mt:apply>
   </xsl:template>
  
  <xsl:template match="s:maxCharSpeedCoordinate">
    <mt:apply>
      <mt:ci>$mcsc</mt:ci>
      <xsl:apply-templates select="./*">
      </xsl:apply-templates>
    </mt:apply>
  </xsl:template>

   <xsl:template match="s:positiveCharSpeedCoordinate">
      <mt:apply>
         <mt:ci>$pcsc</mt:ci>
         <xsl:apply-templates select="./*">
         </xsl:apply-templates>
      </mt:apply>
   </xsl:template>
   
   <xsl:template match="s:negativeCharSpeedCoordinate">
      <mt:apply>
         <mt:ci>$ncsc</mt:ci>
         <xsl:apply-templates select="./*">
         </xsl:apply-templates>
      </mt:apply>
   </xsl:template>

  <xsl:template match="s:globalNumberOfAgents">
    <mt:ci>$gnoa</mt:ci>
  </xsl:template>
  <xsl:template match="s:globalNumberOfVertices">
    <mt:ci>$gnov</mt:ci>
  </xsl:template>

  <xsl:template match="s:globalNumberOfEdges">
    <mt:ci>$gnoe</mt:ci>
  </xsl:template>
  	
  <xsl:template match="s:field">
    <xsl:text>$f</xsl:text>
  </xsl:template>
   		
  <xsl:template match="s:localNumberOfEdges">
    <mt:apply>
		<xsl:if test="./@directionAtt = 'in'">
		  <mt:ci>$lnoe_in</mt:ci>
			<xsl:apply-templates select="./*">
			</xsl:apply-templates>
		</xsl:if>
      <xsl:if test="./@directionAtt = 'out'">
		  <mt:ci>$lnoe_out</mt:ci>
			<xsl:apply-templates select="./*">
			</xsl:apply-templates>
		</xsl:if>
    </mt:apply>
	</xsl:template>

  <xsl:template match="s:minRegionValue">
    <mt:ci>$minrv</mt:ci>
  </xsl:template>
  
  <xsl:template match="s:maxRegionValue">
    <mt:ci>$maxrv</mt:ci>
  </xsl:template>
  
  <xsl:template match="s:minDomainValue">
    <xsl:text>$mindv_</xsl:text><xsl:value-of select="text()"/>
  </xsl:template>
  
  <xsl:template match="s:maxDomainValue">
    <xsl:text>$maxdv_</xsl:text><xsl:value-of select="text()"/>
  </xsl:template>
  
  <xsl:template name="recPrint">
    <xsl:param name="counter"/>
    <xsl:param name="value"/>
    <xsl:if test="$counter &gt; 0">
      <xsl:value-of select="$value"/>
      <xsl:call-template name="recPrint">
        <xsl:with-param name="counter" select="$counter - 1"/>
        <xsl:with-param name="value" select="$value"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>
  
</xsl:stylesheet>
