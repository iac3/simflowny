Ext.Loader.setPath('Ext.ux', '../extjs/examples/ux');

Ext.require([
    'Ext.selection.CellModel',
    'Ext.grid.*',
    'Ext.data.*',
    'Ext.util.*',
    'Ext.state.*',
    'Ext.form.*',
    'Ext.ux.CheckColumn',
    'Ext.ux.form.field.DateTime',
    'Ext.dd.*'
]);

Ext.application({
    name: 'sf',
    autoCreateViewport: true,   
    stores: ['TreeStore'],
    paths: {
        Manager : '../app'
    },
    launch: function(){
        this.viewport = Ext.ComponentQuery.query('viewport')[0]; 
        this.centerRegion = this.viewport.down('[region=center]');
        initApp();    
        //Focus manager, useful for debugging
   //     Ext.FocusManager.enable(true);
        //Event observer for debugging, it should be declared within the context of the observed object
       // Ext.util.Observable.capture(Ext.getCmp('maintree'), function(evname, f) {console.log(evname);console.log(Ext.get(Ext.Element.getActiveElement()));})
/*Ext.util.Observable.prototype.fireEvent =Ext.Function.createInterceptor(Ext.util.Observable.prototype.fireEvent, function() {        console.log(this.name);
        console.log(arguments);
        return true;});
*/
    }

});

/**
 * Task to be repeated every 1 minute to save the document
**/
function automaticSave() {
    if (modifiedDocument && (sf.globals.vars.docType.toLowerCase() != 'x3d' || sf.globals.vars.imported)) {
        modifiedDocument = false;
        saveDocument();
    }
    else {
        if (modifiedDocument && sf.globals.vars.docType.toLowerCase() == 'x3d' && !sf.globals.vars.imported) {
            importX3D();
            sf.globals.vars.imported = true;
            modifiedDocument = false;
        }
    }
      //console.log(Ext.get(Ext.Element.getActiveElement()));
}

//Map documentId - JSON document
var auxDocStruct;
//funcion que inicializa todo una vez cargado
function initApp() {
    //Capture close window and save
    Ext.EventManager.on(window, 'beforeunload', function() {
        automaticSave();
    });
    //Loads the xsl transformation document.
    xsl_simML = loadXMLDoc2("resources/xsl/simML2HTML.xsl");
    xsl_asciiMath = loadXMLDoc2("resources/xsl/XSLTmathmlToasciiML/mmlascii.xsl");
    //Check cookies
    checkCookies()
    //Look for the sessionStorage, variables will be there in case of page refresh (To allow refreshing)
    sf.globals.vars.docId = window.sessionStorage.getItem("xmlEditor.documentID");
    console.log("session "+window.sessionStorage.getItem("xmlEditor.documentID"));
    sf.globals.vars.docType = window.sessionStorage.getItem("xmlEditor.documentType");
    sf.globals.vars.docPath = window.sessionStorage.getItem("xmlEditor.documentPath");
    //If sessionStorage does not contain the variables, then load from cookies
    if (sf.globals.vars.docId == null || sf.globals.vars.docId == 'null' || sf.globals.vars.docId == "") {
        //Load variables from cookies
        sf.globals.vars.docId = Ext.util.Cookies.get("xmlEditor.documentID");
        console.log("cookie "+Ext.util.Cookies.get("xmlEditor.documentID"));
        sf.globals.vars.docType = Ext.util.Cookies.get("xmlEditor.documentType");
        sf.globals.vars.docPath = Ext.util.Cookies.get("xmlEditor.documentPath");
        //Create the sessionStorage variables to allow refreshing without losing the current document
        window.sessionStorage.setItem('xmlEditor.documentID', sf.globals.vars.docId);
        window.sessionStorage.setItem('xmlEditor.documentType', sf.globals.vars.docType);
        window.sessionStorage.setItem('xmlEditor.documentPath', sf.globals.vars.docPath);
    }
    //Setting the path label
    Ext.getCmp("documentPathLabel").setText("/" + sf.globals.vars.docPath);
    //Align toolbars
    Ext.getCmp('treetoolbar').setHeight(Ext.getCmp('pathToolbar').getHeight());

    console.log(sf.globals.vars.docId + " " +  sf.globals.vars.docPath + " " + sf.globals.vars.docType + " " + sf.globals.vars.docPath);
    //Load existing document
    if (sf.globals.vars.docId != null && sf.globals.vars.docId != 'null' && sf.globals.vars.docId != '') {
		loadDocument(sf.globals.vars.docId);
    }
    else {
        document.title = "New Document - Simflowny - Editor";
        //Special case X3D
        if (sf.globals.vars.docType.toLowerCase() == 'x3d') {
            newDocument("x3dRegion");
            sf.globals.vars.x3d = localStorage.getItem("xmlEditor.x3d");
            sf.globals.vars.imported = false;
        }
        //New empty document
        else {
            newDocument(sf.globals.vars.docType);
        }
    }

    //Init map auxiliary documents
    auxDocStruct = new Array();

    //Add on click event to the document path label
    Ext.getCmp('documentPathLabel').getEl().on('click', function()
    {
       openDocumentManager();
    }
    );
}

function openDocumentManager() {
    Ext.util.Cookies.set("docManager.documentID", sf.globals.vars.docId);
    Ext.util.Cookies.set("docManager.documentPath", sf.globals.vars.docPath);
    var win = window.open("../index.html", '_blank');
    win.focus();
}

function checkCookies() {
    document.cookie = "__verify=1";
    var supportsCookies = document.cookie.length >= 1 && 
                        document.cookie.indexOf("__verify=1") !== -1;
    var thePast = new Date(1976, 8, 16);
    document.cookie = "__verify=1;expires=" + thePast.toUTCString();
    if (!supportsCookies) {
        showMessage("Cookies", message.COOKIES, Ext.MessageBox.ERROR);
    }
}

function loadXMLDoc(dname)
{
	if (window.ActiveXObject)
	  {
	  xhttp=new ActiveXObject("Msxml2.XMLHTTP.3.0");
	  }
	else 
	  {
	  xhttp=new XMLHttpRequest();
	  }
	xhttp.open("GET",dname,false);
	xhttp.send("");
	return xhttp.responseXML;
}

function stringToXML(XMLString)
{
	if (window.DOMParser)
	{
	  parser=new DOMParser();
	  xmlDoc=parser.parseFromString(XMLString,"text/xml");
	}
	else // Internet Explorer
	{
	  xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
	  xmlDoc.async=false;
	  xmlDoc.loadXML(XMLString); 
	}
	
	return xmlDoc;
}

function loadDocument(documentId) {
    showProgressBar(message.LOADING);
	$.soap({
		url: '/cxf/DocumentManager/',
		method: 'getDocument',
		namespaceQualifier: 'myns',
		namespaceURL: 'http://documentManagerPlugin.mathMS.iac3.eu/',
		data: {
			arg0: documentId
			},
		success: function (soapResponse) {
			try {
				json = soapResponse.toXML().getElementsByTagNameNS('http://documentManagerPlugin.mathMS.iac3.eu/',"getDocumentResponse")[0].childNodes[0].innerHTML;
                //Convert scaped special characters &lt; to <
                json = $("<span />", { html: json }).text();
				var dataJSON = jQuery.parseJSON(json);
                var tree = Ext.getCmp('maintreeEditor');
				tree.getStore().setRootNode(dataJSON);
                //Set page title
                document.title = tree.getRootNode().findChild("tag", "name", true).get("input") + " - Simflowny - Editor";
				//Refresh the viewer
				updateView();
                //Load the struct
                docType = dataJSON.children[0].tag;
                sf.globals.vars.docType = docType;
                getStruct(docType);

                //Focus root element
                tree.getSelectionModel().select(tree.getRootNode().getChildAt(0));
                Ext.MessageBox.hide();

                //Loading external documents asynchronously
                var jsonData = {"children": []};
                getJSON(tree.getRootNode().childNodes[0], jsonData);
                var opValues = JSPath.apply('..children{.type == "UUID"}.input', jsonData);
                for (var i in opValues) {
                    if (opValues[i] != "") {
                        loadAuxiliaryDocuments(opValues[i]);
                    }
                }
			} catch (e) {
				showMessage("Document load", message.ERROR_DOCUMENT.replace("{0}", e), Ext.MessageBox.ERROR);
				return null;
			}
		},
		error: function (SOAPResponse) {
			var msg = SOAPResponse.toXML().getElementsByTagName("faultstring")[0].childNodes[0].nodeValue;
			showMessage("Document load", message.ERROR_DOCUMENT.replace("{0}", msg), Ext.MessageBox.ERROR);
		}
	});
}

/*
 * Loads the imported documents into an array to enable external value references
 */
function loadAuxiliaryDocuments(documentId) {
	$.soap({
		url: '/cxf/DocumentManager/',
		method: 'getDocument',
		namespaceQualifier: 'myns',
		namespaceURL: 'http://documentManagerPlugin.mathMS.iac3.eu/',
		data: {
			arg0: documentId
			},
		success: function (soapResponse) {
			try {
				json = soapResponse.toXML().getElementsByTagNameNS('http://documentManagerPlugin.mathMS.iac3.eu/',"getDocumentResponse")[0].childNodes[0].innerHTML;
                //Convert scaped special characters &lt; to <
                json = $("<span />", { html: json }).text();
				var dataJSON = jQuery.parseJSON(json);
                auxDocStruct[documentId] = dataJSON;
                //Load problem models
                if (dataJSON.children[0].tag.endsWith("Problem")) {
                    var opValues = JSPath.apply('..children{.type == "UUID"}.input', dataJSON);
                    for (var i in opValues) {
                        if (opValues[i] != "") {
                            loadAuxiliaryDocuments(opValues[i]);
                        }
                    }
                }
			} catch (e) {
				return null;
			}
		},
		error: function (SOAPResponse) {
            return null;
		}
	});
}


function showProgressBar(t) {
    Ext.MessageBox.show({
        msg : t,
        progressText : 'Please wait...',
        width : 300,
        wait:true,
        waitConfig: {interval:200}
    });
}
