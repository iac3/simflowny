Ext.define('sf.view.Viewport', {
    extend: 'Ext.container.Viewport',
    layout: 'fit',
    
    requires: [
        'sf.view.XMLDocumentViewer',        
        'sf.view.MainTree',
        'sf.view.MathMLEditor',
        'sf.view.Symbols',
        'sf.globals.vars',
        'Ext.form.Text'
    ],
    
    initComponent: function() {
        var editorWidthProp = localStorage["editorWidthProp"];
        if (editorWidthProp == undefined) {
            editorWidthProp = 2;
        }
        this.items = {
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [{
                flex: 1,
                xtype: 'container',
                id: 'west-region',
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                items: [
                    {
                        xtype: 'maintreeEditor',
                        flex: 1,
                        id: 'maintreeEditor'
                    }]
            }, 
            {
                width: window.innerWidth / parseFloat(editorWidthProp),
                xtype: 'container',
                resizable:true,
                resizeHandles: 'w',
                listeners: {
                    resize: resize
                },
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                items: [{
                    xtype: 'xmldocumentViewer',
                    flex: 1
                }]
            }]
        };
        this.callParent();
    }
});

function resize(obj, width, height, oldWidth, oldHeight, eOpts) {
    localStorage["editorWidthProp"] = window.innerWidth/width;
}
