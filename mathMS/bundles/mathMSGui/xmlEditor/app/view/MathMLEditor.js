Ext.define('sf.view.MathMLEditor', {
    extend: 'Ext.Window',
    alias: 'widget.mathmleditor',
    id: 'mathmleditor',
    floating: true,
    centered: true,
    title: 'MathML edition',
    modal: true,
    width: 1030, //Cannot be minWidth, otherwise it grows when the formula is too large; 1000 + padding
    bodyPadding: 10,
    layout: {
            type: 'vbox',
            align: 'stretch'
    },
    //Ok and cancel buttons
    buttonAlign: 'center',
    buttons: [
        { 
            text: 'Ok',
            handler: saveAndClose
        },
        { 
            text: 'Cancel',
            handler: function() {
                Ext.getCmp('mathmleditor').close();
            } 
        }
    ],
    closable: true,
    resizable: true,
    closeAction: 'hide',
    items: [{
        id : 'mathmleditorView',
        xtype: 'component',
        padding: "10 0 10 5",
        html: '<div id="mathmleditorView" tabindex="1" style="text-align: left"/>'
    },{
        id: 'textMathML',
        xtype: 'textfield',
        name: 'mathml',
        padding: "10 0 10 0",
        emptyText: 'mathml expression',
        listeners: {
            keydown: function(obj, e, eOpts) {
                if (e.getKey() === e.ENTER) {
                    e.stopEvent();
                    saveAndClose();
                }
            }
        }
    }],
    listeners: {
        //Adding Simml menus and buttons 
        beforerender: function() {
            $.getJSON("../data/mathMLEditor/" + sf.globals.vars.docType + ".json", function(json) {
                    var helpers = Ext.getCmp('mathmleditor');
                    helpers.add(json);
                    var layout = helpers.items.getAt(2);
                    for (var i = 0; i < layout.items.length; i++) {
                        var submenu = layout.items.getAt(i);
                        if (submenu.items != undefined) {
                            for (var j = 0; j < submenu.items.length; j++) {
                                //Setting click listener
                                Ext.get(submenu.items.getAt(j).id).on('click', handleSimMLButton);
                                //Setting tooltips
                                setTooltip(Ext.getCmp(submenu.items.getAt(j).id));
                            }
                        }
                    }
            });
        }
    }
});

var editor = Ext.create('sf.view.MathMLEditor', {});

function saveAndClose() {
var renderValue = Ext.getCmp('textMathML').getValue();
    //Ascii to presentation
    var presentation = asciiToPresentation(renderValue);
    console.log("presentation " + presentation);
    //mathML presentation to mathML content
    $.soap({
        url: '/cxf/SimflownyUtils/',
        method: 'infixToMathML',
            namespaceQualifier: 'myns',
            namespaceURL: 'http://simflownyUtilsPlugin.mathMS.iac3.eu/',
        data: {
            arg0: presentation
        },
        success: function (soapResponse) {
            try {
                var xmldoc = soapResponse.toXML()
                xmldoc.normalize();
                //Gets math content and save it
                var math = xmldoc.getElementsByTagNameNS('http://simflownyUtilsPlugin.mathMS.iac3.eu/',"infixToMathMLResponse")[0].childNodes[0].childNodes[0].nodeValue;
                if (currentRecord.get('input') != math) {
                    currentRecord.set('input', math);
                    updateView();
                    modifiedDocument = true;
                }
                Ext.getCmp('mathmleditor').close();
            } catch (ex) {
                showMessage("Internal error", message.ERROR_INFIX.replace("{0}", "Wrong mathematical expression"), Ext.MessageBox.ERROR);
                return null;
            }
        },
        error: function (SOAPResponse) {
            var msg = SOAPResponse.toXML().getElementsByTagName("faultstring")[0].childNodes[0].nodeValue;
            showMessage("Internal error", message.ERROR_INFIX.replace("{0}", msg), Ext.MessageBox.ERROR);
        }
    });
}

/*
 * Opens the editor
 */
var currentRecord;
function openMathMLEditor(record) {
    currentRecord = record;
    var e = Ext.getCmp('mathmleditor');
    var value = mathToAscii(record.get('input'));
    console.log("openMathMLEditor " + record.get('input'));
    console.log("mathToAscii " + value);
    Ext.getCmp('textMathML').setValue(value);
    editor.show();
    updateEditor();
    Ext.getCmp('textMathML').focus();

    //Listener to update the editor after input edition
    $('#textMathML-inputEl').donetyping(function() {
        updateEditor();
    });
}

/*
 * Simml button click handler
 */
function handleSimMLButton(obj, evt) {
    //Get field text area and caret position
    var value = Ext.getCmp(this.id).value;
    var oldValue = Ext.getCmp('textMathML').getValue();
    var caretPosition = Ext.getDom('textMathML-inputEl').selectionStart;
    //insert button text
    Ext.getCmp('textMathML').setValue(oldValue.substring(0, caretPosition) + value + oldValue.substring(caretPosition));
    Ext.getCmp('textMathML').focus();
    updateEditor();
}

/*
 * Updates the MathML viewer
 */
function updateEditor() {
    var renderValue = Ext.getCmp('textMathML').getValue();
    if (renderValue != "") {
        console.log("updateEditor renderValue "+renderValue);
        var presentation = asciiToPresentation(renderValue);
console.log("updateEditor presentation "+presentation);
        //mathML presentation to mathML content
        $.soap({
            url: '/cxf/SimflownyUtils/',
            method: 'infixToMathML',
                namespaceQualifier: 'myns',
                namespaceURL: 'http://simflownyUtilsPlugin.mathMS.iac3.eu/',
            data: {
                arg0: presentation
            },
            success: function (soapResponse) {
                try {
                    var xmldoc = soapResponse.toXML()
                    xmldoc.normalize();
                    var math = xmldoc.getElementsByTagNameNS('http://simflownyUtilsPlugin.mathMS.iac3.eu/',"infixToMathMLResponse")[0].childNodes[0].childNodes[0].nodeValue;
                    Ext.getCmp('mathmleditorView').update('<div id="mathmleditorView" tabindex="1" style="text-align: left">' + displayResult2(math) + '</div>');

console.log("updateEditor infixToMath "+math);
                    MathJax.Hub.Queue(["Typeset", MathJax.Hub, 'mathmleditorView'], reloadLayout);
                } catch (ex) {
                    //Print mathML presentation instead
                    console.log("Wrong mathematical content");
                    Ext.getCmp('mathmleditorView').update('<div id="mathmleditorView" tabindex="1" style="text-align: left">' + displayResult2(presentation).replace(/\*/g, "\u22c5") + '</div>');
                    MathJax.Hub.Queue(["Typeset", MathJax.Hub, 'mathmleditorView'], reloadLayout);
                    return null;
                }
            },
            error: function (SOAPResponse) {
                //Print mathML presentation instead
                var msg = SOAPResponse.toXML().getElementsByTagName("faultstring")[0].childNodes[0].nodeValue;
                console.log("Error getting mathML content " + msg);
                Ext.getCmp('mathmleditorView').update('<div id="mathmleditorView" tabindex="1" style="text-align: left">' + displayResult2(presentation).replace(/\*/g, "\u22c5") + '</div>');
                MathJax.Hub.Queue(["Typeset", MathJax.Hub, 'mathmleditorView'], reloadLayout);
            }
        });
    }
    else {
        Ext.getCmp('mathmleditorView').update('<div id="mathmleditorView" tabindex="1" style="text-align: left"></div>');
        MathJax.Hub.Queue(["Typeset", MathJax.Hub, 'mathmleditorView'], reloadLayout);
    }
}

/*
 * Reloads the layout after the Mathjax render. Otherwise, the elements could overlap.
 */
function reloadLayout() {
    Ext.getCmp('mathmleditor').doLayout();
}

/*
 * Sets the tooltip for the SimML button. It reads the content from external data files.
 */
function setTooltip(button) {
    //Get correct name for the help file
    var helpfile = button.value.substring(1);
    if (helpfile.indexOf('(') > 0) {
        helpfile = helpfile.substring(0, helpfile.indexOf('('));
    }
    Ext.Ajax.request({
        url: "../data/mathMLEditor/help/" + helpfile + ".html",
        success : function(response) {
            var toolTip = Ext.create('Ext.tip.ToolTip', {
                target: button.getId(),
                autoHide: false,
                html: response.responseText,
                autoScroll: true
            });
            //Keep tooltip while over target button or tooltip
            toolTip.on('show', function(){
                var timeout;
                toolTip.getEl().on('mouseout', function() {
                    timeout = window.setTimeout(function() {
                        toolTip.hide();
                    }, 300);
                });

                toolTip.getEl().on('mouseover', function() {
                    window.clearTimeout(timeout);
                });

                Ext.get(toolTip.target).on('mouseover', function() {
                    window.clearTimeout(timeout);
                });

                Ext.get(toolTip.target).on('mouseout', function() {
                    timeout = window.setTimeout(function() {
                        toolTip.hide();
                    }, 300);
                });
            });
        },
        failure : function(response) {
            Ext.create('Ext.tip.ToolTip', {
                target: button.getId(),
                html: "Help file not found"
            });
        }
    });
}

/*
 * Extension to jquery to fire an event after editing the text input but not while writing.
 */
// http://stackoverflow.com/questions/14042193/how-to-trigger-an-event-in-input-text-after-i-stop-typing-writing
// $('#element').donetyping(callback[, timeout=1000])
// Fires callback when a user has finished typing. This is determined by the time elapsed
// since the last keystroke and timeout parameter or the blur event--whichever comes first.
//   @callback: function to be called when even triggers
//   @timeout:  (default=1000) timeout, in ms, to to wait before triggering event if not
//              caused by blur.
// Requires jQuery 1.7+
//
;(function($){
    $.fn.extend({
        donetyping: function(callback,timeout){

            timeout = timeout || 1e3; // 1 second default timeout
            var timeoutReference,
                doneTyping = function(el){
                    if (!timeoutReference) return;
                    timeoutReference = null;
                    callback.call(el);
                };
            return this.each(function(i,el){
                var $el = $(el);
                // Chrome Fix (Use keyup over keypress to detect backspace)
                // thank you @palerdot
                $el.is(':input') && $el.on('keyup keypress paste',function(e){
                    // This catches the backspace button in chrome, but also prevents
                    // the event from triggering too preemptively. Without this line,
                    // using tab/shift+tab will make the focused element fire the callback.

                    if (e.type=='keyup' && e.keyCode==13) {
                        saveAndClose();
                    }
                    if (e.type=='keyup' && e.keyCode!=8) return;
                    
                    // Check if timeout has been set. If it has, "reset" the clock and
                    // start over again.
                    if (timeoutReference) clearTimeout(timeoutReference);
                    timeoutReference = setTimeout(function(){
                        // if we made it here, our timeout has elapsed. Fire the
                        // callback
                        doneTyping(el);
                    }, timeout);
                }).on('blur',function(){
                    // If we can, fire the event since we're leaving the field
                    doneTyping(el);
                });
            });
        }
    });
})(jQuery);

