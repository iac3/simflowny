//The Model properties that are present on each node of the tree
Ext.define('TreeModel', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'tag', type: 'string'},
        {name: 'input', type: 'string'},
        {name: 'type', type: 'string'},
        {name: 'jsonPath', type: 'string'},
        {name: 'pureType', type: 'string'},
        {name: 'attribute', type: 'boolean'},
        {name: 'actionCol', type: 'string', defaultValue: true},
        {name: 'minCard', type: 'string'},
        {name: 'maxCard', type: 'string'},
        {name: 'order', type: 'string'},
        {name: 'recursive', type: 'string', defaultValue: null},
    ]
});


//Transforms a treeNode data into a standard json data into jsonNode variable
function getJSON(treeNode, jsonNode) {

    var localData = {};
    localData['tag'] = treeNode.get('tag');
    localData['input'] = treeNode.get('input');
    localData['type'] = treeNode.get('type');
    localData['jsonPath'] = treeNode.get('jsonPath');
    localData['attribute'] = treeNode.get('attribute');
    localData['pureType'] = treeNode.get('pureType');
    localData['leaf'] = treeNode.get('leaf');
    localData['actionCol'] = treeNode.get('actionCol');
    localData['minCard'] = treeNode.get('minCard');
    localData['maxCard'] = treeNode.get('maxCard');
    localData['order'] = treeNode.get('order');
    localData['recursive'] = treeNode.get('recursive');

    if (treeNode.get('type') == 'date' && treeNode.get('input') != '') treeNode.set('input', Ext.util.Format.date(treeNode.get('input'), 'Y-m-d'));

        if(treeNode.childNodes.length > 0){
        localData['children'] = [];
            for (var i=0; i < treeNode.childNodes.length; i++) {
                    getJSON(treeNode.childNodes[i], localData);
            }
        }   
    jsonNode.children.push(localData);
}

//It updates the right side view: html representation of the xml being edited
var updateView = function(){
        var jsonData = {"children": []};
        var comp = Ext.getCmp('maintreeEditor');
        if (comp !== undefined) {
        stringHTML = '<div class="treeItem">'
                    + '<div class="treeDetail"><div class="content"></div><div class="children"></div></div>'
                +'</div>';
        var xmlViewerDiv = Ext.getCmp('xmlViewerDiv');
        Ext.DomHelper.overwrite('xmlViewerDiv',stringHTML);
        //Get JSON data from TreeStore (Not possible to do it directly through raw data in 4.2: http://www.sencha.com/forum/showthread.php?294971-Raw-data-from-reader-on-nested-json-data&p=1077665#post1077665)
        getJSON(comp.getRootNode().childNodes[0], jsonData);
        //Render Pure directive
        render(jsonData);
        //Query a MathJax update
        MathJax.Hub.Queue(["Typeset",MathJax.Hub]);                    
    }    
}

//XSL to represent simML + mathML code in HTML
var xsl_simML;
//XSL to convert mathML code to asciiMath
var xsl_asciiMath;
//Structure of the JSON loaded
var struct;

//Store of the tree data, the nodes are stored in memory and comes from the server
var storeEditor = Ext.create('Ext.data.TreeStore', {
    model: 'TreeModel',
    proxy: {
        type: 'memory'
    },
    folderSort: false,
    listeners: {
        add: updateView,
        remove: updateView,
        load: function( store ) {
            updateView();
        }
    }
});

//To implement copy/paste
var lastCopied = [];
//when a user left clic on a node runs this function
//This functions creates the context menu associated to a given selected node on the tree
function contextualMenus( view, record, item, index, event ) {
    //Check lock
    var id = "";
    $.each(storeEditor.getRootNode().childNodes[0].childNodes[0].childNodes, function(i, v) {
        if (v.data.tag == "id") {
            id = v.data.input;
        }
    });
    var editable = true;
    if (localStorage.getItem(id) !== null) {
        editable = false;
    }


    //Check clipboard
    var clipboard_tmp = JSON.parse(window.localStorage.getItem('clip'));
    lastCopied = [];
    for (var i in clipboard_tmp) {
        lastCopied[i] = Ext.create('TreeModel', Ext.decode(clipboard_tmp[i]));
    }


    //Disable browser contextual menu
    event.stopEvent();
    //get properties of the current selected node
    var tag = record.get('tag');
    var jsonPath = record.get('jsonPath');
    var minCard = record.get('minCard');
    var maxCard = record.get('maxCard');
    var input = record.get('input');
    var type = record.get('type');
    var leaf = record.get('leaf');
    var isMovableUp = isMovable(record, "up");
    var isMovableDown = isMovable(record, "down");
    console.log("jsonPath del seleccionado " + jsonPath);
    //get the metadata of the selected node from the struct.json
    var structTag = getStructTag(record);
    var menu1 = new Ext.menu.Menu();
    menu1.on({
        hide: function() {
            Ext.getCmp('maintreeEditor').getView().focus();
        },
        scope: this // Important. Ensure "this" is correct during handler execution
    });
    
    /*
     ADD: Add to the context menu the sons of the current node that can be added to it
        Si maxCard es 1:
            Si el hijo ya se encuentra añadido en el arbol no se puede añadir este hijo
            Si el hijo no está, se muestra como posible hijo a añadir.

        Si maxCard es > 1:
        Se debería contar el número de apariciones del hijo en el arbol:
            Si es igual al maxCard no se enseña
            Si es menor se puede añadir
            Si maxCard es -1 se enseña siempre, puesto que es infinito
    */
    
    //vars to store the items to be added at the add menu
    var addArray = [];
    var addTagArray = [];
    
    //Go throut the children of the current node, that comes from the struct.
    if (structTag) {
        //Only allow add when is not a choice or the parent element has no children
        if (structTag.children.length > 0 && (!structTag.children[0].choice || record.childNodes.length == 0 || type == 'instructionSet')) {
            for (var i in structTag.children) {
                //if the child of the node can be repeated undefinetly add to the menu
                if (structTag.children[i].maxCard == -1) {

                    addArray[addArray.length] = getStructTagIndex(structTag.children[i].tag, structTag.children[i].jsonPath, structTag.children[i].type, structTag.children[i].recursive);
                    addTagArray[addTagArray.length] = structTag.children[i].tag;
                }
                //if the child of the node is repeated less than his maximum repetition value add to the menu
                else if (structTag.children[i].maxCard > 0) {
                    //Vemos cuantas veces aparece en los hijos del record actual.
                    var recordCount = getStructCountInTree(structTag.children[i]);
                    if (recordCount < structTag.children[i].maxCard) {
                        addArray[addArray.length] = getStructTagIndex(structTag.children[i].tag, structTag.children[i].jsonPath, structTag.children[i].type, structTag.children[i].recursive);
                            addTagArray[addTagArray.length] = structTag.children[i].tag;
                    }                
                }
            }
        }
    }
          
    //sort the add elements by alphabetical order
    addArray.sort();
    addTagArray.sort();
    
    //ADD add items to the menu
    if (editable) {
        for (index = 0; index < addArray.length; index++) {
            menu1.add(
                      {
                        text: 'Add ' + formatString(addTagArray[index]),
                        iconAlign: '',
                        icon: '../resources/images/plus_16x16.png',                                
                        handler: Ext.bind(addElement, this, [addTagArray[index]], true)
                      }
                    );
        }
    }

    var pastable = isPastable(addTagArray);
    var replaceable = isReplaceable(tag);

    //Separator for copy paste
    if (editable && addArray.length > 0 && (!record.parentNode.isRoot() || (lastCopied.length > 0 && pastable) || isMovableUp || isMovableDown)) {
        menu1.add('-');
    }

    //Move node upwards
    if (editable && isMovableUp) {
        menu1.add(
                  {
                    text: 'Move upwards',
                    iconAlign: '',
                    icon: '../resources/images/arrow_up_16x16.png',   
                    handler: moveElement
                  }
                );
    }
    //Move node downwards
    if (editable && isMovableDown) {
        menu1.add(
                  {
                    text: 'Move downwards',
                    iconAlign: '',
                    icon: '../resources/images/arrow_down_16x16.png',   
                    handler: moveElement
                  }
                );
    }

    //Copy selected tree
    if (!record.parentNode.isRoot()) {
        menu1.add(
                  {
                    text: 'Copy ' + formatString(tag),
                    iconAlign: '',
                    icon: '../resources/images/copy_16x16.png',                                 
                    handler: function() {
                        var selected = Ext.getCmp('maintreeEditor').getSelectionModel().getSelection();
                        var parent = selected[0].parentNode;
                        var tag = selected[0].get('tag');
                        var toCopy = [];
                        for (var i in selected) {
                            if (parent != selected[i].parentNode || tag != selected[i].get('tag')) {
                                showMessage("Copy", message.ERROR_COPY, Ext.MessageBox.ERROR);
                                toCopy = [];
                                break;
                            }
                            tmp = selected[i].copy(undefined, true);
                            tmpjsonPath = selected[i].get('jsonPath');
                            removeJsonPathPrefix(tmp, tmpjsonPath);
                            toCopy[i] = Ext.encode(tmp.serialize());
                        }
                        Ext.getCmp('maintreeEditor').getView().focus();
                        if (toCopy.length > 0) {
                            window.localStorage.setItem('clip', JSON.stringify(toCopy));
                        }
                        
                    }
                  }
                );
    }
    //Paste Element
    if (editable && lastCopied.length > 0 && pastable) {
        menu1.add(
                  {
                    text: 'Paste ' + formatString(lastCopied[0].get('tag')),
                    iconAlign: '',
                    icon: '../resources/images/paste_16x16.png',   
                    handler: pasteNode
                  }
                );
    }
    //Replace Element
    if (editable && !record.parentNode.isRoot() && lastCopied.length > 0 && replaceable) {
        menu1.add(
                  {
                    text: 'Replace ' + formatString(lastCopied[0].get('tag')),
                    iconAlign: '',
                    icon: '../resources/images/paste_16x16.png',   
                    handler: replaceNode
                  }
                );
    }    
    //Insert before Element
    if (editable && !record.parentNode.isRoot() && lastCopied.length > 0 && replaceable) {
        menu1.add(
                  {
                    text: 'Insert Before ' + formatString(lastCopied[0].get('tag')),
                    iconAlign: '',
                    icon: '../resources/images/paste_16x16.png',   
                    handler: insertBeforeNode
                  }
                );
    }    
    /*
     Del
    */
    //ADD Del option to the menu, if the user clicks this option the current node will be deleted
    if (editable && type != 'uneditable') {
        if (addArray.length > 0 || (!record.parentNode.isRoot() || (lastCopied.length > 0 && pastable) || isMovableUp || isMovableDown)) {
            menu1.add('-');  
        }
        var menuText = tag;
        if (leaf) {
            if (minCard == 1 && !isRepeated(record)) {
                menuText = "Clear " + formatString(menuText);
            }
            else {
                menuText = "Delete " + formatString(menuText);
            }
        }
        else {
            menuText = "Clear " + formatString(menuText);
        }
        menu1.add(
                    {
                        text: menuText,
                        iconAlign: '',
                        icon: '../resources/images/x_alt_16x16.png', 
                        handler: delElement
                    }
                );
    }

    //Edition of imported document
    if (editable && type == 'UUID' && input != '') {
        if (addArray.length > 0 || (!record.parentNode.isRoot() || (lastCopied.length > 0 && pastable) || isMovableUp || isMovableDown) || (type != 'uneditable')) {
            menu1.add('-');  
        }
        menu1.add(
                    {
                        text: 'Edit document',
                        iconAlign: '',
                        icon: '../resources/images/pen_alt2_16x16.png', 
                        handler: editDocument
                    }
                );
    }

  
    //render the menu if items exists.    
    if (menu1.items.length > 0) {
        menu1.showAt(event.getXY());
    }                    
}

//Checks if a node is repeated
function isRepeated(node, direction) {
    if (direction == undefined || direction == "up") {
        var previous = node.previousSibling;
        if (previous != null && (previous.get('tag') == node.get('tag'))) {
            return true;
        }
    }
    if (direction == undefined || direction == "down") {
        var next = node.nextSibling;
        if (next != null && (next.get('tag') == node.get('tag'))) {
            return true;
        }
    }
    return false;
}

//Checks if the node can be moved in the given direction
function isMovable(node, direction) {
    //If the node is repeated then it can be moved
    if(isRepeated(node, direction)) {
        return true;
    }
    //Otherwise, the element can be moved only if it has a brother and its parent does not require ordered childern
    var parentStruct = getStructTag(node.parentNode);
    var brother;
    if (direction == "up") {
        brother = node.previousSibling;
    }
    else {
        brother = node.nextSibling;
    }
    return brother != null && !parentStruct.ordered;
}

//Moves an element upwards or downwards
function moveElement(obj) {
    var tree = Ext.getCmp('maintreeEditor');
    var node = tree.getSelectionModel().getSelection()[0];
    var nodePath = node.get('jsonPath');
    if (obj.text.indexOf("down") > -1) {
        var next = node.nextSibling;
        var nextPath = next.get('jsonPath');
        //If the tag is the same for both elements, interchange them
        if (next.get('tag') == node.get('tag')) {
            replaceJsonPathPrefix(node, nodePath, nextPath);
            replaceJsonPathPrefix(next, nextPath, nodePath);
        }
        node.parentNode.insertBefore(next, node);
    }
    else {
        var previous = node.previousSibling;
        var previousPath = previous.get('jsonPath');
        if (previous.get('tag') == node.get('tag')) {
            replaceJsonPathPrefix(node, nodePath, previousPath);
            replaceJsonPathPrefix(previous, previousPath, nodePath);
        }
        node = node.parentNode.insertBefore(node, previous);
    }
    //Update right view
    updateView();
    modifiedDocument = true;
    //Set focus
    modifiedElementFocus = true;
    tree.getView().focusRow(node);
}

//Adds a jsonPath prefix from the node and its descendants
function replaceJsonPathPrefix(node, oldPath, newPath) {
    var oldjsonPath = node.get('jsonPath');
    var newjsonPath = newPath + oldjsonPath.substring(oldPath.length);
    node.set('jsonPath', newjsonPath);
    if(node.childNodes.length > 0){
        for (var i=0; i < node.childNodes.length; i++) {
                replaceJsonPathPrefix(node.childNodes[i], oldPath, newPath);
        }
    } 
}

//Checks if the clipboard node can be pasted to the current node
function isPastable(addTagArray) {
    if (lastCopied != null) {
        if (lastCopied.length > 0) {
            for (var index = 0; index < addTagArray.length; index++) {
                if (addTagArray[index] == lastCopied[0].get('tag')) {
                    return true;
                }
            }
        }
    }
    return false;
}

//Checks if the clipboard node can be pasted to the current node
function isReplaceable(currentTag) {
    if (lastCopied != null) {
        if (lastCopied.length > 0) {
            if (currentTag == lastCopied[0].get('tag')) {
                return true;
            }
        }
    }
    return false;
}

//Adds a jsonPath prefix from the node and its descendants
function addJsonPathPrefix(node, prefix) {
    var oldjsonPath = node.get('jsonPath');
    var newjsonPath = prefix + oldjsonPath;
    node.set('jsonPath', newjsonPath);

    var children = node.childNodes
    if(node.childNodes.length > 0){
        for (var i=0; i < node.childNodes.length; i++) {
            addJsonPathPrefix(node.childNodes[i], prefix);
        }
    }
}

//Increments a jsonPath prefix from the node and its descendants
function incrementJsonPathPrefix(node, prefix) {
    var jsonPath = node.get('jsonPath');
    var re = new RegExp(escapeRegExp(prefix) + "\\[([0-9]+)\\]", "g")
    jsonPath = jsonPath.replace(re, function(a, b){
                                        return prefix + '[' + (parseInt(b) + 1) + ']';
                                    })
    node.set('jsonPath', jsonPath);
    if(node.childNodes.length > 0){
        for (var i=0; i < node.childNodes.length; i++) {
                incrementJsonPathPrefix(node.childNodes[i], prefix);
        }
    } 
}


//Removes a jsonPath prefix from the node and its descendants
function removeJsonPathPrefix(node, prefix) {
    var oldjsonPath = node.get('jsonPath');
    var newjsonPath = oldjsonPath.substring(prefix.length);
    node.set('jsonPath', newjsonPath);
    if(node.childNodes.length > 0){
        for (var i=0; i < node.childNodes.length; i++) {
                removeJsonPathPrefix(node.childNodes[i], prefix);
        }
    } 
}

//Appends the clipboard node into the selected node.
function pasteNode() {
    var tree = Ext.getCmp('maintreeEditor');
    var node = tree.getSelectionModel().getSelection()[0];
    var jsonPath = node.get('jsonPath');

    for (var i = lastCopied.length - 1; i >= 0; i=i-1) {
        var element = lastCopied[i];
        var newNode = element.copy(undefined, true);
        var newTag = element.get('tag');
        var pureType = element.get('pureType');
        //Calculate and set the new jsonPath
        var prefix = "mms";
        if (newTag == 'math') {
            prefix = "mt";
            newTag = newTag + "ML";
        }
        if (pureType == 'simml') {
            prefix = "sml";
        }
        var auxPath = "/" + prefix + ":" + newTag;
        var newPath = calculateJsonPath(node, auxPath);
        addJsonPathPrefix(newNode, newPath);

        //If ordered nodes, paste it where it corresponds
        var position = -1;
        var nodeOrder = parseInt(newNode.get('order'));
        if (nodeOrder > -1) {
            for (var j = 0; j < node.childNodes.length; j++) {
                var nodeTreeOrder = parseInt(node.childNodes[j].get('order'));
                if (nodeOrder < nodeTreeOrder) {
                    position = j;
                    node.insertChild(position, newNode); 
                    break;
                }
            }     
        }
        if (position == -1) {
            node.appendChild(newNode);     
        }
    }

    //Set focus
    modifiedElementFocus = true;
    focusAppend(node, newNode);
    //Update right view
    updateView();
    modifiedDocument = true;
}

//Replaces the selected node by the clipboard node.
function replaceNode() {
    var tree = Ext.getCmp('maintreeEditor');
    var node = tree.getSelectionModel().getSelection()[0];
    var parentNode = node.parentNode;
    var sibling; 
    if (parentNode != null) {
        var nodeJsonPath = node.get('jsonPath');
        for (i in lastCopied) {
            var element = lastCopied[i];
            var newNode = element.copy(undefined, true);
            var newTag = element.get('tag');
            if (i == 0) {
                sibling = node.nextSibling;
                //Append the new node
                parentNode.replaceChild(newNode, node);
                addJsonPathPrefix(newNode, nodeJsonPath);
            }
            else {
                if (sibling == null) {
                    parentNode.appendChild(newNode);
                    addJsonPathPrefix(newNode, nodeJsonPath);
                    tmp_path = nodeJsonPath.substring(0, nodeJsonPath.lastIndexOf("["));
                    incrementJsonPathPrefix(newNode, tmp_path);
                }
                else {
                    parentNode.insertBefore(newNode, sibling);
                    addJsonPathPrefix(newNode, nodeJsonPath);
                    tmp_path = nodeJsonPath.substring(0, nodeJsonPath.lastIndexOf("["));
                    incrementJsonPathPrefix(newNode, tmp_path);
                    var siblingtmp = sibling;
                    while (siblingtmp !== null) {
                        incrementJsonPathPrefix(siblingtmp, tmp_path);
                        siblingtmp = siblingtmp.nextSibling;
                    }
                }
            }
        }

        //Set focus
        modifiedElementFocus = true;
        focusAppend(parentNode, newNode);
        //Update right view
        updateView();
        modifiedDocument = true;
    }
}

//Inserts the clipboard node before the selected node.
function insertBeforeNode() {
    var tree = Ext.getCmp('maintreeEditor');
    var node = tree.getSelectionModel().getSelection()[0];
    var parentNode = node.parentNode;
    if (parentNode != null) {
        for (i in lastCopied) {
            var element = lastCopied[i];
            var newNode = element.copy(undefined, true);
            var newTag = element.get('tag');
            var nodeJsonPath = node.get('jsonPath');
            //Append the new node
            newNode = parentNode.insertBefore(newNode, node);
            //Add prefix
            addJsonPathPrefix(newNode, nodeJsonPath);
            //Increment jsonPath for next nodes
            nodeJsonPath = nodeJsonPath.substring(0, nodeJsonPath.lastIndexOf("["));
            var sibling = node;
            while (sibling !== null) {
                incrementJsonPathPrefix(sibling, nodeJsonPath);
                sibling = sibling.nextSibling;
            }
        }

        //Set focus
        modifiedElementFocus = true;
        focusAppend(parentNode, newNode);
        //Update right view
        updateView();
        modifiedDocument = true;
    }
}


//Open a new editor with the document id selected
function editDocument() {
    var tree = Ext.getCmp('maintreeEditor');
    var node = tree.getSelectionModel().getSelection()[0];
    var docId = node.get('input');
	Ext.util.Cookies.set("xmlEditor.documentID", docId);
    //The document type only matters when it is an new document or a X3D import, which is not the case. Anyway, it must have a value
    Ext.util.Cookies.set('xmlEditor.documentType', "notX3d");
    loadDocumentPathAndOpen(docId);
}


var modifiedDocument = false;
function changed(obj, value, startValue, eOpts) {
    if (value != startValue) {
        modifiedDocument = true;
    }
}

//This event run each time a node input is right clicked by the user (right side of the tree)
//It creates the corresponding input edit widget depending on the node type, for example a calendar, a combo
//It overrides the default behaviour
function editorInput(record, defaultField){
    //No edition if locked
    var id = "";
    $.each(storeEditor.getRootNode().childNodes[0].childNodes[0].childNodes, function(i, v) {
        if (v.data.tag == "id") {
            id = v.data.input;
        }
    });
    if (localStorage.getItem(id)!== null) {
        return false;
    }

    //get current rendered node type
    var type = record.get('type');

    //render default inputs for each type
    this.editors = {
        'dateTime'    : Ext.create('Ext.grid.CellEditor',  { listeners: { complete:changed}, field: Ext.create('Ext.ux.form.field.DateTime',   {selectOnFocus: true})}),

        'string'  : Ext.create('Ext.grid.CellEditor', {listeners: { complete:changed }, field: Ext.create('Ext.form.field.Text',   {selectOnFocus: true})}),
        'number'  : Ext.create('Ext.grid.CellEditor', {listeners: { complete:changed }, field: Ext.create('Ext.form.field.Number', {selectOnFocus: true})}),
        'int'  : Ext.create('Ext.grid.CellEditor', {listeners: { complete:changed }, field: Ext.create('Ext.form.field.Number', {selectOnFocus: true})}),
        'integer'  : Ext.create('Ext.grid.CellEditor', {listeners: { complete:changed }, field: Ext.create('Ext.form.field.Number', {selectOnFocus: true})}),
        'boolean' : Ext.create('Ext.grid.CellEditor', {listeners: { complete:changed }, field: Ext.create('Ext.form.field.ComboBox', {
          editable: false,
          store: [[ true, 'True' ], [false, 'False' ]]
          })})
    };
      
    //para chequear si es un string con lista incorporada:string[Values = REAL, INT, STRING, BOOLEAN]
    var isStringWithValues = /^string\[Values = ([\w-](\s*,?\s*[\w-])*)\]/g
    //para chequear si es un string con referencia incorporada:string[Content = 
    //pueden contener también valores literales
    var isStringWithReference = /^string\[Content = '(.+)'\]$/g
    //if is a string with options it creates a combobox with the options rendered in the input
    if (isStringWithValues.test(type) ) {
        var output = type.replace(/string\[Values = /, "").replace("]","").split(",");
        
        var opList = new Array(output.length);
        for (var i = 0; i < output.length; i++) {
            opList[i] = new Array(2);
            opList[i][0] = output[i].trim();
            opList[i][1] = output[i].trim();
        }
        
        return Ext.create('Ext.grid.CellEditor', {listeners: { complete:changed }, field: Ext.create('Ext.form.field.ComboBox', {
          editable: true,
          store: opList
          })})        
    }
    //If is a type that is linked to another part of the tree, for example a list of fileds linked to a canonicalPDE
    else if (isStringWithReference.test(type)) {
        var opList = new Array(0);
        var reference = type.replace(/string\[Content = '/, "").replace(/'\]$/,"");
        //Split if there is multiple jsonpath or literals expressions (^jsonPath1^jsonPath2...)
        var references = reference.match(/\^[^\^]*/g);
        var jsonData = {"children": []};
        var comp = Ext.getCmp('maintreeEditor');
        getJSON(comp.getRootNode().childNodes[0], jsonData);
        //iterate over each external reference
        for (var j = 0; j < references.length; j++) {
            //Concatenation operation
            var isConcat = /^\^concat\([^\)]*\)$/;
            if (isConcat.test(references[j])) {
                //Split operands
                var operands = references[j].substring("^concat(".length, references[j].length - 1).split(",");
                var opValues = [""];
                //For each operand get its values
                for (var i = 0; i < operands.length; i++) {
                    var tmp = getContentValues("^" + operands[i].trim(), record.get('jsonPath'), jsonData);
                    var newOpValues = [];
                    //Concat the values with the former ones
                    for (var k = 0; k < tmp.length; k++) {
                        for (var l = 0; l < opValues.length; l++) {
                            newOpValues.push(opValues[l] + tmp[k]);
                        }
                    }
                    opValues = newOpValues;
                }
            }
            else {
                //Simple reference values (literal, internal or external reference)
                var opValues = getContentValues(references[j], record.get('jsonPath'), jsonData);
            }
            for (var i = 0; i < opValues.length; i++) {
                opList[opList.length] = new Array(2);
                opList[opList.length - 1][0] = opValues[i].trim();
                opList[opList.length - 1][1] = opValues[i].trim();
            }
        }
        //return the list of options to render
        return Ext.create('Ext.grid.CellEditor', {listeners: { complete:changed },  field: Ext.create('Ext.form.field.ComboBox', {
              editable: true,
              store: opList
          })})
    }
    //Document ID
    else if (type == 'UUID') {
        return openDocManager(record);
    }
    //Document ID
    else if (type == 'mathML') {
        return openMathMLEditor(record);
    }
    
   return this.editors[record.get('type')];
}

/*
 * Gets the values from a single reference
 */
function getContentValues(reference, jsonPath, jsonData) {
    //literal value
    var isLiteral = /^\^'[A-Za-z0-9_-]*'$/;
    if (isLiteral.test(reference)) {
        var opValues = [reference.substring(2, reference.length - 1)];
    }
    //JSPath https://github.com/dfilatov/jspath
    else {
        //Replaces the ./ancestor-or-self paths with the proper value, using the current context
        reference = replaceAncestorPaths(reference, jsonPath, jsonData);

        //External reference
        var opValues;
        if (reference.indexOf("..") >= 0) {
            opValues = getExternalValues(reference);
        }
        //Local reference
        else {
            opValues = JSPath.apply(reference, jsonData);
        }
    }
    return opValues;
}

/*
 *  Gets the element values from an external referenced document.
 */
function getExternalValues(inputPath) {
    //Get document references
    var references = inputPath.substring(1, inputPath.indexOf("..")).split(/[",\[\]]+/);
    //Get the values from the external document, stored locally
    var jsonPathSearch = "^" + inputPath.substring(inputPath.indexOf("..") + 1);

    //Chack if there are any other concatenated external searchs
    var count = (jsonPathSearch.match(/\.\./g) || []).length;
    if (count > 0) {
        //Take next path values and ignore the others
        nextPath = jsonPathSearch.substring(0, jsonPathSearch.indexOf("..")) + ".input";
        var values = getValuesFromAuxiliaryDocuments(nextPath, references);

        //Concatenate all references obtained
        var references = "";
        $.each(values, function(i, el){
            references = references + "\"" + el + "\"" + ",";
        });

        //Perform next search
        return getExternalValues("^" + references + jsonPathSearch.substring(jsonPathSearch.indexOf("..")));
    }
    else {
        //Get values from the documents
        return getValuesFromAuxiliaryDocuments(jsonPathSearch, references);
    }
}

/*
 * Gets the values from the auxiliary documents using jsonPath.
 */
function getValuesFromAuxiliaryDocuments(jsonPathSearch, references) {
    //Search in the documents referenced
    var values = [];
    for (var i = 0; i < references.length; i++) {
        var id = references[i];
        if (id != "") {
            var localValues = JSPath.apply(jsonPathSearch, auxDocStruct[id]);
            if (localValues) {
                values = values.concat(localValues);
            }
        }
    }

    //Removing duplicate values
    var uniqueValues = [];
    $.each(values, function(i, el){
        if($.inArray(el, uniqueValues) === -1) uniqueValues.push(el);
    });

    return uniqueValues;
}

/*
 * Replaces the ancestor path references with the values
 */
function replaceAncestorPaths(path, currentPath, data) {
    var originalPath = path;
    var ancestorPaths = path.match(/\.\/ancestor-or-self::([^\/]*)[^.}]*/g);
    if (ancestorPaths != null) {
        for (var i = 0; i < ancestorPaths.length; i++) {
            var re = new RegExp(escapeRegExp(ancestorPaths[i]), "g")
            var values = getAncestorValues(ancestorPaths[i], currentPath, data);
            if (values.length > 1) {
                var newValues = "";
                for (var v in values) {
                    newValues = newValues + ',"' + values[v] + '"';
                }
                originalPath = originalPath.replace(re, "[" + newValues.substring(1) + "]");
            }
            else {
                originalPath = originalPath.replace(re, '"' + values + '"');
            }
        }
    }
    return originalPath;
}

/*
 *  Get the values from an ancestor xpath.
 */
function getAncestorValues(path, currentPath, data) {
    var ancestor = path.match(/\/ancestor-or-self::([^\/]*)/);
    ancestor = ancestor[1];

    //Get current path up to the ancestor node to keep the indices of the elements.
    var newPath = currentPath.substring(0, currentPath.indexOf(ancestor + "[") + ancestor.length);
    var tmp2 = currentPath.substring(currentPath.indexOf(ancestor + "[") + ancestor.length);
    newPath = newPath + tmp2.substring(0, tmp2.indexOf(']') + 1);
    //Join the ancestor's path with remaining path.
    //The new appended nodes have a 0 index, meaning any node repetition will match the path.
    var appendPath = path.match(/(\/[\w]*:[^\/]*)/g);
    for (var i = 0; i < appendPath.length; i++) {
        newPath = newPath + appendPath[i];
    }
    //Transform xpath to JSPath
    newPath = newPath.replace(/\[([0-9]+)\]/g, function(match, group1) { //Decrement indexes number
            return "[" + (parseInt(group1,10) - 1) + "]";
        })
        .replace(/\//g, ".")    //Replace navigation
        .replace(/mms:([a-zA-Z]*)/g, "children{.tag == \"$1\"}") //Replace tag names with propper JSON structure
         + ".input"; //Access to the values

    return JSPath.apply(newPath, data);
}


//Imports Document manager objects
Ext.require([
        'Manager.store.XMLDocuments',
        'Manager.store.TreeStore',
        'Manager.view.MainTree',
        'Manager.globals.vars',
        'Manager.view.XMLDocumentsList'
]);

//Manager Documents List Extension
Ext.define('sf.view.PopUpXMLDocumentsList', {
    extend: 'Manager.view.XMLDocumentsList',
    alias: 'widget.popupxmldocumentslist',
    //Disable selection model
    selModel: undefined,
    //Disable drag and drop
    viewConfig: undefined
});

//Manager Documents List Extension
Ext.define('sf.view.PopUpMainTree', {
    extend: 'Manager.view.MainTree',
    alias: 'widget.popupmaintree',
    //Disable drag and drop
    viewConfig: undefined
});

function openDocManager(record) {
    var popup = new Ext.Window({
                floating: true,
                centered: true,
                title: 'Select a document',
                modal: true,
                width: 1000,
                height: 400,
                id: 'docManager',
                layout: 'fit',
                closable: true,
                resizable: true,
                items: [{
                    xtype: 'panel',
                    id: 'popup',
                    flex: 1,
                    layout: {
                        type: 'hbox',
                        align: 'stretch'
                    },
                    items: [{
                        xtype: 'panel',
                        width: 250,
                        id: 'tree',
                        layout: {
                            type: 'vbox',
                            align: 'stretch'
                        },
                        items: [{
                            xtype: 'popupmaintree',
                            flex: 1,
                            id: 'maintree'
                        }]
                    },
                    {
					    xtype: 'popupxmldocumentslist',
					    id: 'xmldocumentslist',
					    flex: 1
				    }]
                }],    
                listeners: 
                {
		            close: function(panel, e) 
		            {
                        popupResponse = Ext.util.Cookies.get("xmlEditor.popup");
                        //If a model has been selected save the data in the Record
                        if (popupResponse != undefined && popupResponse != "") {
                            record.set('input', popupResponse);
                            updateView();
                            modifiedDocument = true;
                            Ext.util.Cookies.set("xmlEditor.popup", "");

                            //Load external documents
                            loadAuxiliaryDocuments(popupResponse);
                        }
                        Ext.TaskManager.stop(task);
                        var xmlList = Ext.getCmp('maintreeEditor');
                        xmlList.getView().focus();
		            }
	            }
                });
    var documentList = Ext.getCmp('xmldocumentslist');
    getCollectionTree();
    popup.show();
}

//This event fires each time a input node is rendered in the browser (right side of the tree)
//it overrides the default render widget
function renderInput(value, metaData, record, row, col, store, gridView){         
    var result = value;
    var type = record.get('type');

    //it type is mathML we have to convert mathML into a asciimath string and render it
    if (type == 'mathML') {
        //MathML content to asciiMath
        //Replace to avoid html encoding interference
        return mathToAscii(value).replace(/</g,'&lt;');
    }

    //if it's a date type we have to return a date rendered value
    if (type == 'dateTime') {
        return Ext.util.Format.date(value, 'Y-m-d H:i:s');
    }
    
    //if value is none no input has to be rendered
    if (value == null) {
      return '';
    }

    //if actual value is a date render in y-m-d format
    else if (Ext.isDate(value)) {
        return Ext.util.Format.date(value, 'Y-m-d');
    } 
    
    //if value is boolean render true or false
    else if (Ext.isBoolean(value)) {
        result = me.renderBool(value);
    }
    //return default rendering
    return Ext.util.Format.htmlEncode(result);
}

//Fires each time a node on the tree is rendered
//Changes de default render behaviour
//It paints the node background-color to red if the node has a mandatory value
function renderTag(value, metaData, record, row, col, store, gridView){               

    var result = value;
    var input = record.get('input');
    var type = record.get('type');
    var leaf = record.get('leaf');
    if ((input == null || input == '')  && leaf && type != 'string[Values = ]') {
        metaData.style = "color:white;background-color:#9C0901;";
        metaData.tdCls = 'emptyCell';
    }

    return Ext.util.Format.htmlEncode(formatString(result));
}

//Adds a new tagChild node to the current selected node
//input are tagChild, and node
function addElement(obj, evt) {
    var tree = Ext.getCmp('maintreeEditor');
    //get the current selected node, in this node we will add the new child
    var node = tree.getSelectionModel().getSelection()[0];    
    //añadimos el hijo al árbol, buscamos el nodo a añadir en el segundo nivel
    //obtenemos el padre del hijo desde struct, nivel 1
    var structTagRoot = getStructTag(node);
    
    //get the child tag name that we have to add, it comes frome the fired menu selection event
    var tagChild = arguments[2];
    var structTag;
    //buscamos el hijo a añadir en structs, nivel 2
    for (var i in structTagRoot.children) {
        if (structTagRoot.children[i].tag == tagChild) {
            structTag = structTagRoot.children[i];
        }            
    }
    //añadimos el nodo.
    modifiedDocument = true;
    modifiedElementFocus = true;
    addNodeFromStruct(node, struct, structTag, true, structTag.recursive);
    updateView();
}

//Insert structTag as a child of node, where node is on the treePanel
//node: the node in wich we will insert the child
//struct: metadata tree structure
//structTag: the child to insert in Node, in struct format
//order:The position in with structTag have to be inserted.
function addNodeFromStruct(node, struct, structTag, order, recursive){
    //Recursive elements must inherit the property
    if (recursive == null && structTag.recursive) {
        recursive = structTag.tag;
    }
    var position = -1;
    //buscamos en que lugar meter el hijo en el padre
    if (order) {
        var nodeOrder = structTag.order;
        for (var i = 0; i < node.childNodes.length; i++) {
                var nodeTreeOrder = node.childNodes[i].get('order');
                if (nodeOrder < nodeTreeOrder) {
                    position = i;
                    break;
                }
        }
        
    }
    if (position == -1) {
        position = node.childNodes.length;        
    }

    //If parent was a leaf node then it is no more
    if (node.get('leaf')) {
        node.set('leaf', false);
    }

    //calculamos el jsonpath del nuevo hijo
    var jsonPath = calculateJsonPath(node, structTag.jsonPath);
    //insertamos el hijo
    node.insertChild(position,{
        tag: structTag.tag,
        jsonPath: jsonPath,
        pureType: structTag.pureType,
        type: structTag.type.replace(/\\\"/g, "\""), //Removing jsonpath's scape quotation character
        input: null,
        actionCol: structTag.actionCol,
        expanded: structTag.expanded,
        minCard: structTag.minCard,
        maxCard: structTag.maxCard,
        leaf: structTag.leaf,
        attribute: structTag.attribute,
        order: structTag.order,
        recursive: recursive
    });

    node = node.getChildAt(position);

    //recursivamente obtener los hijos de structTag e ir añadiendolos,
    //Ya que hay que meter toda la subestructura también
    //vamos a buscar a sus hijos al primer nivel.

    structTag = struct[getStructTagIndex(structTag.tag, jsonPath, structTag.type, recursive)];
    for (var i in structTag.children) {
        //sólo si el tag es obligatorio lo pintamos
        if (structTag.children[i].minCard > 0) {
            var structTag2 = structTag.children[i];
            addNodeFromStruct(node, struct, structTag2, false, recursive);
        }
    }

    node.expand(true);
}

//For a given node and a given child (structTag) calculates the jsonPath of the child
//node: The current node
//structTag: a new Child of node
//position: The position of the child into the node
//return: the jsonPath of the child in the node
function calculateJsonPath(node, childPath) {
    var rootJsonPath = node.get('jsonPath');
    var lastPath = childPath.substring(childPath.lastIndexOf("/")).replace(/\[[0-9]*\]/g,"");
    var position = 1;
    //Find all exiting tags with similar path
    var re = new RegExp("^" + escapeRegExp(rootJsonPath + lastPath) + "\[[0-9]*\]$");
    node.cascadeBy(function(nodeIt) {
      if (re.test(nodeIt.get('jsonPath'))) {
        position = position + 1
      }
    });

    var jsonPath = rootJsonPath + lastPath + "[" + position + "]";
    return jsonPath;
}

//Escape regExp special characters from the expression
function escapeRegExp(expression) {
    return expression.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
}

//For a given node returns it's corresponding tag on the struct.json metadata
function getStructTag(node) {
    var tag = node.get('tag');
    var jsonPath = node.get('jsonPath');
    var type = node.get('type');
    var recursive = node.get('recursive');
    return struct[getStructTagIndex(tag, jsonPath, type, recursive)];
}

//Calculate the key (index) of a tag into the struct.json, taking into account his jsonPath and type
//Remember that struct.json in his first level is a hashtable, and the index is the key
function getStructTagIndex(tag, jsonPath, type, recursive) {
     var jsonPathIdx = jsonPath.replace(/\[[0-9]*\]/g,"");

     //if sufix is sml only sml + tag is the key of the element
     var suffix = "/sml:" + tag;
     if (jsonPathIdx.indexOf(suffix, this.length - suffix.length) !== -1) {
         jsonPathIdx = "/sml:" + tag;
     }
     //if sufix is mt only mt + tag is the key of the element
     suffix = "/mt:" + tag + "ML";
     if (jsonPathIdx.indexOf(suffix, this.length - suffix.length) !== -1) {
         jsonPathIdx = "/mt:" + tag + "ML";
     }

     if (recursive != null) {
        var pattern = new RegExp(":" + recursive + "/[A-Za-z:\/]*:" + recursive);

        jsonPathIdx = jsonPathIdx.replace(pattern, ":" + recursive);
     }

     //The key is the tag plus the jsonPathIdx
     var index = tag + "_" + jsonPathIdx;

     return index;
}

//Count how many times appears struct as a son of the current selected node
//struct: node that we want to count as a child of node.
function getStructCountInTree(struct) {
    var tree = Ext.getCmp('maintreeEditor');
    var node = tree.getSelectionModel().getSelection()[0];
    var count = 0;
    var indexStruct = getStructTagIndex(struct.tag, struct.jsonPath, struct.type, struct.recursive);
    for (var i = 0; i < node.childNodes.length; i++) {
        var tag = node.childNodes[i].get('tag');
        var jsonPath = node.childNodes[i].get('jsonPath');
        var type = node.childNodes[i].get('type');
        var recursive = node.childNodes[i].get('recursive');  
        var index = getStructTagIndex(tag,jsonPath,type, recursive);
        if (index == indexStruct) {
                count = count + 1;
        }        
    }
    return count;
}

//Get the conversion between a node in the tree and in the struct.json
function getStructInternalTag(node) {
    
     var tag = node.tag;
     var jsonPath = node.jsonPath;
     var index = tag + "_" + jsonPath;
     if (struct[index] == undefined) {
         alert ("is undefined" + index);
     }
     return struct[index];
}

//Counts how many brothers have of the same type a node from the tree
function getBrothersCount(node) {
    
    var count = 0;
    var indexNode = node.get('tag') + "_" + node.get('jsonPath').replace(/\[[0-9]*\]/g,"");
    
    for (var i = 0; i < node.parentNode.childNodes.length; i++) {
        var indexNode2 = node.parentNode.childNodes[i].get('tag') + "_" + node.parentNode.childNodes[i].get('jsonPath').replace(/\[[0-9]*\]/g,"");
        if (indexNode == indexNode2) {
                count = count + 1;
        }
    }
    
    return count;
}

//Update jsonPath of the brothers begin node,
//It updates it's last path index of the json, incrementing his value in 1
function updateJsonPathBrothers(node) {
    
    var count = -1;
    var found = false;
    var indexNode = node.get('tag') + "_" + node.get('jsonPath');
    var rootJsonPath = node.parentNode.get('jsonPath');
    for (var i = 0; i < node.parentNode.childNodes.length; i++) {
        var indexNode2 = node.parentNode.childNodes[i].get('tag') + "_" + node.parentNode.childNodes[i].get('jsonPath');
        if (indexNode == indexNode2 && !found) {
                count = i-1;
                found = true;
        }
        if (found && node.parentNode.childNodes[i].get('tag') == node.get('tag')) {
            var lastPath = node.parentNode.childNodes[i].get('jsonPath').substring(node.parentNode.childNodes[i].get('jsonPath').lastIndexOf("/")).replace(/\[[0-9]*\]/g,"");
            var jsonPath = rootJsonPath + lastPath + "[" + count + "]";
            node.parentNode.childNodes[i].set('jsonPath', jsonPath);
            //updated recursively the childs jsonPath of the curren node being modified
            updateJsonPathSons(node.parentNode.childNodes[i]);
            count = count + 1;
        }
    }
    
    return count;
}

//Updateh jsonPath of the childs of node recursively.
function updateJsonPathSons(node) {    
    var fatherJsonPath = node.get('jsonPath');
    for (var i = 0; i < node.childNodes.length; i++) {
        var lastPath = node.childNodes[i].get('jsonPath').substring(node.childNodes[i].get('jsonPath').lastIndexOf("/"));
        var jsonPath = fatherJsonPath + lastPath;
        node.childNodes[i].set('jsonPath', jsonPath);
        updateJsonPathSons(node.childNodes[i]);
    }
}


//Deletes the current selected node from the tree
function delElement(obj, evt) {
    var tree = Ext.getCmp('maintreeEditor');
    if (tree.getSelectionModel().hasSelection()) {
        var nodes = tree.getSelectionModel().getSelection();
        for (var i = nodes.length - 1; i >= 0; i--) {
            deleteRecursive(nodes[i]);
        }
        modifiedDocument = true;
        modifiedElementFocus = true;
        updateView();
    }
}

function deleteRecursive(node) {
    var minCard = node.get('minCard');
    var brotherCount = getBrothersCount(node);
    if (brotherCount > minCard) {
        //update jsonPath
        if (node.get("maxCard") == -1 || node.get("maxCard") > 1) {
            //updata el jsonPath de los hermanos con el mismo id debajo de él
            updateJsonPathBrothers(node);
        }
        node.remove(false);        
    }
    else {
        if (node.get('type') == 'date') {
            node.set('input', new Date());
        }
        else  {
            node.set('input', null);
        }
        //Delete children recursively
        for (var i = node.childNodes.length - 1; i >= 0; i--) {
            deleteRecursive(node.childNodes[i]);
        }
    }
}

//function that clones a node.
function clone(node) {
    var atts = node.attributes;
    //atts.id = Ext.id();
    var clonedNode = new Ext.tree.TreeNode(Ext.apply({}, atts));
    clonedNode.text = node.text;
    for (var i = 0; i < node.childNodes.length; i++) {
        clonedNode.appendChild(clone(node.childNodes[i]));
    }
    return clonedNode;
}

/**
    Moves the focus of the right panel to the content related to the tree selected element.
**/
function focusOnRight(view, record, item, index, event) {
    //debugger
   // console.log(record.get('jsonPath'));
    //fin debbug
    var path = record.get('jsonPath');

    var container = $('div[id="xmlViewerDiv"]'),
        scrollTo = $('a[name="' + path + '"]');

    var windowHeight = $(window).height();
    container.scrollTop(
        scrollTo.offset().top - ((windowHeight/2) - (container.scrollTop()))
    );

    var divID = $('a[name="' + path + '"]').children().first().attr("id");

    switch (divID) {
        case "row":
            $('a[name="' + path + '"]').parent().parent().toggleClass('clicked');
            setTimeout(function(){
                $('a[name="' + path + '"]').parent().parent().toggleClass('clicked');
                $('a[name="' + path + '"]').parent().parent().toggleClass('unclicked');
                setTimeout(function(){
                        $('a[name="' + path + '"]').parent().parent().toggleClass('unclicked');
                }, 1000);
            }, 1000);
            break;
        case "clear":
             $('a[name="' + path + '"]').parent().toggleClass('clicked');
            setTimeout(function(){
                $('a[name="' + path + '"]').parent().toggleClass('clicked');
                $('a[name="' + path + '"]').parent().toggleClass('unclicked');
                setTimeout(function(){
                        $('a[name="' + path + '"]').parent().toggleClass('unclicked');
                }, 1000);
            }, 1000);
            break;
        default:
            $('a[name="' + path + '"]').toggleClass('clicked');
            setTimeout(function(){
                $('a[name="' + path + '"]').toggleClass('clicked');
                $('a[name="' + path + '"]').toggleClass('unclicked');
                setTimeout(function(){
                        $('a[name="' + path + '"]').toggleClass('unclicked');
                }, 1000);
            }, 1000);
            break;
    }
}

/**
    Moves the focus of the tree to the node related to the content of the right panel.
**/
function focusOnLeft(element) {
    if (element.parentNode.getAttribute("name")) {
        var jsonPath = element.parentNode.getAttribute("name");
        expandTree(jsonPath);
        Ext.getCmp('maintreeEditor').getView().focusRow(Ext.getCmp('maintreeEditor').getSelectionModel().getLastSelected());
        var container = $('div[id="xmlViewerDiv"]');
        container.focus();
    }
}

/**
    Focus on the last appended node
**/
var modifiedElementFocus = false;
function focusAppend(parent, node, index, eOpts) {
    if (modifiedElementFocus) {
        var jsonPath = node.get('jsonPath');
        expandTree(jsonPath);
        modifiedElementFocus = false;
    }
}

/**
    Focus on the next/previous sibling or parent of last removed node
**/
function focusRemove(obj, node, isMove, eOpts) {
    if (modifiedElementFocus) {
        var tree = Ext.getCmp('maintreeEditor');
        var nextFocus = obj;
        if (node.nextSibling != undefined) {
            nextFocus = node.nextSibling;
        }
        if (node.previousSibling != undefined) {
            nextFocus = node.previousSibling;
        }
        expandTree(nextFocus.get('jsonPath'));
        tree.getSelectionModel().select(nextFocus);
        modifiedElementFocus = false;
    }
}

/**
    Expands the tree to the json Path
**/
var expandedNode;
function expandTree(jsonPath) {
    console.log("expandTree " + jsonPath);
    var tree = Ext.getCmp('maintreeEditor');
    //The json path cannot be used directly. The indexed elements (.../parameter[2]) are not available in TreePanel selectPath methods. The navigation has to be made manually
    var nodes = jsonPath.split("/");
    var node = tree.getRootNode();
    var someExpansion = false;
    //Expanding nodes one by one
    for (var i = 1; i < nodes.length; i++) {
        //Getting tag name
        var tag = nodes[i].substring(nodes[i].indexOf(":") + 1, nodes[i].indexOf("["));
        //Special case mathML: rename tag to math
        if (nodes[i].substring(0, nodes[i].indexOf(":")) == "mt") tag = "math";
        //Getting index of tag
        var index = nodes[i].substring(nodes[i].indexOf("[") + 1, nodes[i].indexOf("]"));
        //Search for the first occurrence
        node = node.findChild("tag", tag);
        //If the index is not one go to the next siblings
        if (index > 1) {
            counter = 1;
            while ((node = node.nextSibling) != null) {
                //Next sibling with the same tag
                if (node.get("tag") == tag) {
                    counter = counter + 1;
                    //The indexed tag is found
                    if (counter == index) {
                        break;
                    }
                }
            }
        }
        //Expanding
        if (node.isExpandable() && !node.isExpanded() && i < nodes.length - 1) {
            node.expand();
            someExpansion = true;
        }
    }
    //When a node has been expanded, the visual expansion event is launched and the node selection must be done in that event, otherwise the focus is lost
    if (!someExpansion) {
        tree.getSelectionModel().select(node);
    } 
    else {
        expandedNode = node;
    }
}

//Fires after a node has visually expanded and allows the correct focus of the children
function focusExpansion() {
    if (expandedNode && expandedNode != "") {
        var tree = Ext.getCmp('maintreeEditor');
        tree.getSelectionModel().select(expandedNode);  
        expandedNode = "";
    }
}


//MathML content to asciiMath
function mathToAscii( xml ) {
    if (xml == null || xml == '') {
        return "";
    }
    xml = xml.replace("<mt:math>", "<mt:math xmlns:mt='http://www.w3.org/1998/Math/MathML' xmlns:sml='urn:simml'>");
    return transformXSLT(xml, xsl_asciiMath, true);
}

function editAscii( editor, e, eOpts ) {
    if (e.record.get('type') == 'mathML') {
        e.value = mathToAscii(e.value);
    }
}


function asciiToPresentation(ascii) {
    try {
        var impl    = document.implementation,
            xmlDoc  = impl.createDocument('http://www.w3.org/1998/Math/MathML', 'math', null);
        AsciiMathParser(xmlDoc);
        var mathmlpresentation = xmlDoc.createElement("div");
        mathmlpresentation.appendChild(parseAsciiMathInput(ascii));
        return mathmlpresentation.innerHTML;
    } catch (ex) {
        showMessage("Internal error", message.ERROR_INFIX.replace("{0}", ex), Ext.MessageBox.ERROR);
        return null;
    }
}

function celledition( editor, e, eOpts ) {
    if (e.record.get('type') != 'mathML') {
        updateView();
    }
    var tree = Ext.getCmp('maintreeEditor');
    tree.getSelectionModel().select(tree.getSelectionModel().getLastSelected());
    tree.getView().focus();
}

function saveDocument() {
    var jsonData = {"children": []};
    var comp = Ext.getCmp('maintreeEditor');
    getJSON(comp.getRootNode().childNodes[0], jsonData);
    jsonData = JSON.stringify(jsonData);
    $.soap({
        url: '/cxf/DocumentManager/',
        method: 'saveDocument',
           namespaceQualifier: 'myns',
        namespaceURL: 'http://documentManagerPlugin.mathMS.iac3.eu/',
        data: {
            arg0: jsonData,
            arg1: sf.globals.vars.docPath
        },
        success: function (soapResponse) {
            try {
                //Get response, normalization needed in Firefox
                var xmldoc = soapResponse.toXML()
                xmldoc.normalize();
                json = xmldoc.getElementsByTagNameNS('http://documentManagerPlugin.mathMS.iac3.eu/',"saveDocumentResponse")[0].childNodes[0].childNodes[0].nodeValue;
                //Set the document id if not already present
                var dataJSON = jQuery.parseJSON(json);
                //Get id and position in the header
                var id = '';
                var counter = 0;
                $.each(storeEditor.getRootNode().childNodes[0].childNodes[0].childNodes, function(i, v) {
                    if (v.data.tag == "id") {
                        id = v.data.input;
                        counter = i;
                    }
                });
                //Set the id if there was no previous one
                $.each(dataJSON.children[0].children[0].children, function(i, v) {
                    if (v.tag == "id" && id != v.input) {
                        storeEditor.getRootNode().childNodes[0].childNodes[0].childNodes[counter].set("input",v.input);
                        //Refresh the viewer
                        updateView();
                    }
                });
                storeEditor.sync();

                //Refresh documentPath, the document may have been moved in the manager
                $.soap({
                    url: '/cxf/DocumentManager/',
                    method: 'getDocumentCollection',
                       namespaceQualifier: 'myns',
                        namespaceURL: 'http://documentManagerPlugin.mathMS.iac3.eu/',

                    data: {
                        arg0: id
                    },
                    success: function (soapResponse) {
                        try {
                            data = soapResponse.toXML().getElementsByTagNameNS('http://documentManagerPlugin.mathMS.iac3.eu/',"getDocumentCollectionResponse")[0].childNodes[0].innerHTML;
                            //Setting the path label
                            Ext.util.Cookies.set("xmlEditor.documentPath", data);
                            sf.globals.vars.docPath = data;
                            Ext.getCmp("documentPathLabel").setText("/" + sf.globals.vars.docPath);
                        } catch (e) {
                            return null;
                        }
                    }
                });

            } catch (e) {
                showMessage("Save document", message.ERROR_SAVE.replace("{0}", e), Ext.MessageBox.ERROR);
                return null;
            }
        },
        error: function (SOAPResponse) {
            var msg = SOAPResponse.toXML().getElementsByTagName("faultstring")[0].childNodes[0].nodeValue;
            showMessage("Save document", message.ERROR_SAVE.replace("{0}", msg), Ext.MessageBox.ERROR);
        }
    });
}

function importX3D() {
    var jsonData = {"children": []};
    var comp = Ext.getCmp('maintreeEditor');
    getJSON(comp.getRootNode().childNodes[0], jsonData);
    jsonData = JSON.stringify(jsonData);

    $.soap({
        url: '/cxf/DocumentManager/',
        method: 'importX3D',
        namespaceQualifier: 'myns',
        namespaceURL: 'http://documentManagerPlugin.mathMS.iac3.eu/',
        data: {
            arg0: jsonData,
            arg1: sf.globals.vars.x3d,
            arg2: sf.globals.vars.docPath
        },
        success: function (soapResponse) {
            try {
                //Get response, normalization needed in Firefox
                var xmldoc = soapResponse.toXML()
                xmldoc.normalize();
                json = xmldoc.getElementsByTagNameNS('http://documentManagerPlugin.mathMS.iac3.eu/',"importX3DResponse")[0].childNodes[0].childNodes[0].nodeValue;
                //Set the document id if not already present
                var dataJSON = jQuery.parseJSON(json);
                //Get id and position in the header
                var id = '';
                var counter = 0;
                $.each(storeEditor.getRootNode().childNodes[0].childNodes[0].childNodes, function(i, v) {
                    if (v.data.tag == "id") {
                        id = v.data.input;
                        counter = i;
                    }
                });
                //Set the id if there was no previous one
                $.each(dataJSON.children[0].children[0].children, function(i, v) {
                    if (v.tag == "id" && id != v.input) {
                        storeEditor.getRootNode().childNodes[0].childNodes[0].childNodes[counter].set("input",v.input);
                    }
                });
                //Set the x3d segmleft->rightent id
                var segmentId = dataJSON.children[0].children[1].input;
                storeEditor.getRootNode().childNodes[0].childNodes[1].set("input",segmentId);
                //Refresh the viewer
                updateView();
                storeEditor.sync();
                //Removing x3d from local storage
                localStorage.removeItem("xmlEditor.x3d");
            } catch (e) {
                showMessage("New document", message.ERROR_SAVE.replace("{0}", e), Ext.MessageBox.ERROR);
                return null;
            }
        },
        error: function (SOAPResponse) {
            var msg = SOAPResponse.toXML().getElementsByTagName("faultstring")[0].childNodes[0].nodeValue;
            showMessage("New document", message.ERROR_SAVE.replace("{0}", msg), Ext.MessageBox.ERROR);
        }
    });
}

function newDocument(docType) {
    //Get the struct for this model/problem
    $.soap({
        url: '/cxf/SimflownyUtils/',
        method: 'xmlToJSON',
           namespaceQualifier: 'myns',
        namespaceURL: 'http://simflownyUtilsPlugin.mathMS.iac3.eu/',
        data: {
            arg0: docType,
            arg1: true
        },

        success: function (soapResponse) {
        try {
            //Get response, normalization needed in Firefox
            var xmldoc = soapResponse.toXML()
            xmldoc.normalize();
            json = xmldoc.getElementsByTagNameNS('http://simflownyUtilsPlugin.mathMS.iac3.eu/',"xmlToJSONResponse")[0].childNodes[0].childNodes[0].nodeValue;
            //Parse result as json and store it
            var dataJSON = jQuery.parseJSON(json);
            var tree = Ext.getCmp('maintreeEditor');
            tree.getStore().setRootNode(dataJSON);
            getStruct(docType);
            //Refresh the viewer
            updateView();

            //Focus root element
            tree.getSelectionModel().select(tree.getRootNode().getChildAt(0));
        } catch (e) {
            showMessage("New document", message.ERROR_NEW_DOCUMENT.replace("{0}", e), Ext.MessageBox.ERROR);
            return null;
        }
        },
        error: function (SOAPResponse) {
            var msg = SOAPResponse.toXML().getElementsByTagName("faultstring")[0].childNodes[0].nodeValue;
            showMessage("New document", message.ERROR_NEW_DOCUMENT.replace("{0}", msg), Ext.MessageBox.ERROR);
        }
    });
}

function loadDocumentPathAndOpen(id) {
    //jquery soap client
    $.soap({
        url: '/cxf/DocumentManager/',
        method: 'getDocumentCollection',
           namespaceQualifier: 'myns',
            namespaceURL: 'http://documentManagerPlugin.mathMS.iac3.eu/',

        data: {
            arg0: id
        },

        success: function (soapResponse) {
            try {
                data = soapResponse.toXML().getElementsByTagNameNS('http://documentManagerPlugin.mathMS.iac3.eu/',"getDocumentCollectionResponse")[0].childNodes[0].innerHTML;
                //Clear sessionStorage or the new window will inherit the session
                window.sessionStorage.clear();
                Ext.util.Cookies.set("xmlEditor.documentPath", data);
                var win = window.open("index.html", '_blank');
                win.focus();
                //Restore the session to allow current page refresh
                window.sessionStorage.setItem('xmlEditor.documentID', sf.globals.vars.docId);
                window.sessionStorage.setItem('xmlEditor.documentType', sf.globals.vars.docType);
                window.sessionStorage.setItem('xmlEditor.documentPath', sf.globals.vars.docPath);
            } catch (e) {
                showMessage("Document path", message.ERROR_DOCUMENT_PATH.replace("{0}", e), Ext.MessageBox.ERROR);
                return null;
            }
        },
        error: function (SOAPResponse) {
            var msg = SOAPResponse.toXML().getElementsByTagName("faultstring")[0].childNodes[0].nodeValue;
            showMessage("Document path", message.ERROR_DOCUMENT_PATH.replace("{0}", msg), Ext.MessageBox.ERROR);
        }
    });
}

function validate() {
    //Save before validating
    if (sf.globals.vars.docType.toLowerCase() != 'x3d' || sf.globals.vars.imported) {
        saveDocument();
    }
    else {
        importX3D();
        sf.globals.vars.imported = true;
    }
    modifiedDocument = false;

    var jsonData = {"children": []};
    var comp = Ext.getCmp('maintreeEditor');
    getJSON(comp.getRootNode().childNodes[0], jsonData);
    jsonData = JSON.stringify(jsonData);
    //jquery soap client
    $.soap({
        url: '/cxf/DocumentManager/',
        method: 'validateDocument',
           namespaceQualifier: 'myns',
            namespaceURL: 'http://documentManagerPlugin.mathMS.iac3.eu/',

        data: {
            arg0: jsonData,
            arg1: true
        },

        success: function (soapResponse) {
            showMessage("Validation", message.VALIDATION_OK, Ext.MessageBox.INFO);
        },
        error: function (SOAPResponse) {
            var msg = SOAPResponse.toXML().getElementsByTagName("faultstring")[0].childNodes[0].nodeValue;
            msg = msg.substring(msg.indexOf("(") + 1, msg.lastIndexOf(")")).replace("\n", "<br>");
            msg = msg.replace(/'([^']*)'/g, "'<strong>$1<\/strong>'")
            showMessage("Validation", message.ERROR_VALIDATION.replace("{0}", msg), Ext.MessageBox.ERROR);
        }
    });
}

//get struct.json from the server
function getStruct(modelName) {
    //jquery soap client
    $.soap({
        url: '/cxf/SimflownyUtils/',
        method: 'getStruct',
           namespaceQualifier: 'myns',
            namespaceURL: 'http://simflownyUtilsPlugin.mathMS.iac3.eu/',

        data: {
            arg0: modelName
        },

        success: function (soapResponse) {
            try {
                json = soapResponse.toXML().getElementsByTagNameNS('http://simflownyUtilsPlugin.mathMS.iac3.eu/',"getStructResponse")[0].childNodes[0].innerHTML;

                var structAux = jQuery.parseJSON(json);
                //Creation of associative array for struct
                struct = new Array();
                var idx;
                for(idx in structAux)
                {
                    var tag = structAux[idx].tag;
                    var jsonPath = structAux[idx].jsonPath;
                    var index = tag + "_" + jsonPath;
                    struct[index] = structAux[idx];
                }
            } catch (e) {
                showMessage("Document load", message.ERROR_STRUCT.replace("{0}", e), Ext.MessageBox.ERROR);
                return null;
            }
        },
        error: function (SOAPResponse) {
            var msg = SOAPResponse.toXML().getElementsByTagName("faultstring")[0].childNodes[0].nodeValue;
            showMessage("Document load", message.ERROR_STRUCT.replace("{0}", msg), Ext.MessageBox.ERROR);
        }
    });
}

    
//Define the entire tree
Ext.define('sf.view.MainTree', {
    extend: 'Ext.tree.Panel',
    alias: 'widget.maintreeEditor',
    store: storeEditor,
    autoScroll: false,
    title: 'Editor',
    header: {
        style: 'padding-right:12px',
        bodyStyle: 'margin: 0px;'
    },
    titleAlign: 'right',
    icon: '../resources/images/logo-white.png',
    iconCls: 'logo-icon',
    id: 'documentTree',
    rootVisible: false,
    selModel : {
        mode: 'MULTI'
    },
    listeners: {
        itemcontextmenu: contextualMenus,
        itemclick: focusOnRight,
        itemappend: focusAppend,
        iteminsert: focusAppend,
        afteritemexpand: focusExpansion,
        beforeitemremove: focusRemove,
        cellkeydown: function(obj, td, cellIndex, record, tr, rowIndex, e, eOpts) {
            //Open context menu as an alternative to menu key, that does not work in sikuli
            if (e.getKey() === e.F7) {
                e.stopEvent();
                obj.fireEvent('itemcontextmenu', obj, record, tr, cellIndex, e);
            }
        }
    },
    //expand all and collapse all buttons
    dockedItems: [{
        id: 'treetoolbar',
        xtype: 'toolbar',
        dock: 'bottom',
        flex: 1,
        items: [
                {
                    xtype : 'button',
                    text: 'Expand all',
                    handler: function() {
                        var tree = Ext.getCmp('maintreeEditor');
                        tree.expandAll();
                        tree.getView().focusNode(tree.getStore().getRootNode());
                        tree.getView().focus();
                    }
                },
                { 
                    xtype: 'tbseparator'
                },
                {
                    xtype : 'button',
                    text: 'Collapse all',
                    handler: function() {
                        var tree = Ext.getCmp('maintreeEditor');
                        tree.collapseAll();
                        tree.getView().focusNode(tree.getStore().getRootNode());
                        tree.getView().focus();
                    }
                },
                { 
                    xtype: 'tbseparator'
                },
                {
                    xtype : 'button',
                    text: '\u03B1 \u03A9',
                    handler: function() {
                        if (greekPopup.isVisible()) {
                            closeGreekPopup();
                        }
                        else {
                            openGreekPopup();
                        }
                    }
                },
                '->',
                {
                    xtype : 'button',
                    text: 'Save & Validate',
                    cls: 'button-validate',
                    handler: validate
                }
        ]
    }],
    //Cell Editing event
    plugins: [
        Ext.create('Ext.grid.plugin.CellEditing', {
            pluginId: 'CellEditingPlugin',
            clicksToEdit: 1,
            listeners: {
                edit: celledition,
                beforeedit: editAscii
            }
        }),
        //Plugin for performance purposes when expanding all nodes
        {
           ptype: "bufferedrenderer",
           synchronousRender: true
        }
    ],
    //the firt column renders the tree, the second column renders the input widget of each node
    columns: [{
            xtype: 'treecolumn', //this is so we know which column will show the tree
            text: 'Document Tree',
            flex: 1,
            sortable: false,
            dataIndex: 'tag',
            renderer: renderTag
        },{
            text: 'Input',
            flex: 1,
            dataIndex: 'input',
            sortable: false,
            renderer: renderInput,
            getEditor: editorInput               
        }]          
});

// map multiple keys to multiple actions by strings and array of codes
Ext.onReady(function(){

    //Modify value
    Ext.create('Ext.util.KeyMap', Ext.getBody(), {
        key: 45, //Insert key
        fn: function(a, b, c){
                var tree = Ext.getCmp('maintreeEditor');
                var node = tree.getSelectionModel().getSelection()[0];
                if (node.get('leaf')) {
                    var rowSelected = tree.getSelectionModel().getCurrentPosition().row;
                    tree.editingPlugin.startEditByPosition({row:rowSelected,column:1});
                }
            
        }
    });

    //Delete
    Ext.create('Ext.util.KeyMap', Ext.getBody(), {
        key: 46, //Delete key
        fn: function(a, b, c){
                var tree = Ext.getCmp('maintreeEditor');
                //Enable deleting node only if not editing text
                if (!tree.editingPlugin.editing) {
                    var node = tree.getSelectionModel().getSelection()[0];

                    //get properties of the current selected node
                    var minCard = node.get('minCard');
                    var input = node.get('input');
                    var type = node.get('type');

                    if (!(minCard == 1 && getBrothersCount(node) == 1 && input == null) && type != 'uneditable') {
                        delElement();
                    }
                }
        }
    });

});


