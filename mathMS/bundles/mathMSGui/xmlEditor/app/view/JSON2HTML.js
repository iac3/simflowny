
	/*
		Loads the xsl document
	 */
	function loadXMLDoc2(filename) {
		if (window.ActiveXObject) {
 			xhttp = new ActiveXObject("Msxml2.XMLHTTP");
  		} else {
  			xhttp = new XMLHttpRequest();
  		}
		xhttp.open("GET", filename, false);
		try {xhttp.responseType = "msxml-document"} catch(err) {} // Helping IE11
		xhttp.send("");
		return xhttp.responseXML;
	}


	function transformXSLT(xml, xsl, text) {
		var parseXml;
		if (window.DOMParser) {
		    parseXml = function(xmlStr) {
			return ( new window.DOMParser() ).parseFromString(xmlStr, "text/xml");
		    };
		} else if (typeof window.ActiveXObject != "undefined" && new window.ActiveXObject("Microsoft.XMLDOM")) {
		    parseXml = function(xmlStr) {
			var xmlDoc = new window.ActiveXObject("Microsoft.XMLDOM");
			xmlDoc.async = "false";
			xmlDoc.loadXML(xmlStr);
			return xmlDoc;
		    };
		} else {
		    parseXml = function() { return null; }
		}

		var xmlDoc = parseXml(xml);

		// code for IE
		if (window.ActiveXObject || xhttp.responseType == "msxml-document") {
			ex = xml.transformNode(xsl);
			return ex.innerHTML;
  		}
		// code for Chrome, Firefox, Opera, etc.
		else if (document.implementation && document.implementation.createDocument) {
	  		xsltProcessor = new XSLTProcessor();
  			xsltProcessor.importStylesheet(xsl);
  			var resultDocument = xsltProcessor.transformToDocument(xmlDoc);
			var div = document.createElement('div');
			div.appendChild( resultDocument.documentElement.cloneNode(true) );
			if (text) {
				return div.childNodes[0].textContent;
			}
			else {
				return div.innerHTML;
			}
  		}
	}

    /*
     *  Open a new editor with the document passed by parameter
     */
    function editFromViewer(docId) {
	    Ext.util.Cookies.set("xmlEditor.documentID", docId);
        //The document type only matters when it is an new document or a X3D import, which is not the case. Anyway, it must have a value
        Ext.util.Cookies.set('xmlEditor.documentType', "notX3d");
        loadDocumentPathAndOpen(docId);
    }

	/*
		Transforms a simML + mathML instruction set into an HTML + mathML representation
	 */
	function displayResult2(xml) {
		xml = xml.replace("<sml:simml>", "<sml:simml xmlns:sml='urn:simml' xmlns:mt='http://www.w3.org/1998/Math/MathML'>");
		xml = xml.replace("<mt:math>", "<mt:math xmlns:mt='http://www.w3.org/1998/Math/MathML' xmlns:sml='urn:simml'>");
		return transformXSLT(xml, xsl_simML, false);
	}

	/*
		Renders the data with Pure
	 */
	function render(data) {
        var uuidExpr = new RegExp("^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$");
		var columns = [];
        var headersAcc = [];
        var oldPad = [];
		var padh = 15;
		var pad = 5;
        var smallHeader = 0;
		directive = {
		  'div.treeDetail': {
			'child <- children': {
			  // Input data
			  'div.content': function(a){
				var header = formatString(a.item.tag);
				var headerStyle = "h1";
				if (pad > 5 + padh) {
					headerStyle = "h2";
				}
				if (pad > 5 + 2 * padh || a.item.type == 'mathML') {
					headerStyle = "h3";
				}
				if (smallHeader > 0) {
					headerStyle = "h3";
				}
				var content = a.item.input != null ? a.item.input : '';
				var jsonPath = a.item.jsonPath;
				//SimML, MathML
				if ((a.item.type == 'mathML' || a.item.type == 'simml') && content != '') {
					content = displayResult2(content);
				}
                //Date
				if (a.item.type == 'dateTime' && content != '') {
					content = Ext.util.Format.date(content, 'Y-m-d H:i:s');
				}
                //Document id navigation
                var dblClick = "";
				if (uuidExpr.test(a.item.input)) {
					dblClick = " ondblclick='editFromViewer(\"" + content + "\")'";
				}
				var contentStyle = "simpleContent";
				//Empty
				if (a.item.pureType == 'hidden') return "<a name='" + jsonPath + "'><div></div></a>";

				//Comma List
				if (a.item.pureType == 'commaList') {
					pad = pad + padh;
                    content = "<div id='" + contentStyle + "' style='padding-left:" + pad + "px;'>";
		            for (var i = 0; i < a.item.children.length; i++) {
                        item = a.item.children[i];
                    	var childJsonPath = item.jsonPath;
			            content = content + "<a name='" + childJsonPath + "'><span onclick='focusOnLeft(this)'>" + item.input + "</span></a>";
                        if (i + 1 < a.item.children.length) {
			                content = content + "<span>, </span>";
                        }
		            }
                    content = content + "</div>";
					pad = pad - padh;
					return "<a name='" + jsonPath + "'><div id='" + headerStyle + "' style='padding-left:" + pad + "px;' onclick='focusOnLeft(this)'>" + header + "</div></a><div" + dblClick + ">" + content + "</div>";
				}
				//List
				if (a.item.pureType == 'list') {
					pad = pad + padh;
					content = rfn(a.child.item);
					pad = pad - padh;
					return "<a name='" + jsonPath + "'><div id='" + headerStyle + "' style='padding-left:" + pad + "px;' onclick='focusOnLeft(this)'>" + header + "</div></a><div" + dblClick + ">" + content + "</div>";
				}
				//List Item
				if (a.item.pureType == 'listItem') {
					if (content == '') {
						content = rfn(a.child.item);
					}
					else {
						content = "<span class='" + contentStyle + "' style='padding-left:" + pad + "px;' onclick='focusOnLeft(this)'" + dblClick + ">" + content + "</span>";
					}
					return "<a name='" + jsonPath + "'>" + content + "</a>";
				}

				//Pair List
				if (a.item.pureType == 'pairList') {
					pad = pad + padh;
					content = rfn(a.child.item);
					pad = pad - padh;
					return "<a name='" + jsonPath + "'><div id='" + headerStyle + "' style='padding-left:" + pad + "px;' onclick='focusOnLeft(this)'>" + header + "</div></a><div class='" + contentStyle + "' style='padding-left:" + (pad + padh) + "px;'><table class='tabla'>" + content + "</table></div>";
				}
				//Pair List Item
				if (a.item.pureType == 'pairListItem') {
					if (content == '') {
                        oldPad.push(pad);
                        smallHeader = smallHeader + 1;
					    pad = 0;
					    content = rfn(a.child.item);
					    pad = oldPad.pop();
                        smallHeader = smallHeader - 1;
					}
					return "<tr><td><a name='" + jsonPath + "'><div onclick='focusOnLeft(this)'>" + header + "</div></a></td><td><a name='" + jsonPath + "'><div onclick='focusOnLeft(this)'" + dblClick + ">" + content + "</div></a></td></tr>";
				}

				//Table
				if (a.item.pureType == 'table') {
					//Get all column names
					var columnsLevel = [];
					maxLen = 0;
					for (var i = 0; i < a.item.children.length; i++) {
						for (var j = 0; j < a.item.children[i].children.length; j++) {
							var value = a.item.children[i].children[j].tag;
							if (!contains(columnsLevel, value)) {
								columnsLevel.push(value);
							}
						}
					}
                    columns.push(columnsLevel);
					pad = pad + padh;
					content = rfn(a.child.item);
					pad = pad - padh;
                    columns.pop();
					return "<a name='" + jsonPath + "'><div id='" + headerStyle + "' style='padding-left:" + pad + "px;' onclick='focusOnLeft(this)'>" + header + "</div></a><div class='" + contentStyle + "' style='padding-left:" + (pad + padh) + "px;'><table class='tabla'>" + content + "</table></div>";
				}
				//Table Row
				if (a.item.pureType == 'tableRow') {
					var headers = '';
					content = '';
                    var columnsLocal = columns[columns.length - 1];
					for (var i = 0; i < columnsLocal.length; i++) {
						if (a.pos == 0) {
							headers = headers + "<th>" + formatString(columnsLocal[i]) + "</th>";
						}
                    }
					for (var i = 0; i < columnsLocal.length; i++) {
						var found = false;
                        var parentPath = a.item.jsonPath;
                        var first = true;
						//Print column
						for (var j = 0; j < a.item.children.length && !found; j++){
							item = a.item.children[j];
						  	if (item.tag == columnsLocal[i]){
								found = true;
								var contentC = item.input != null ? item.input : '';
								var jsonPath = item.jsonPath;
								//SimML, MathML
								if ((item.type == 'mathML' || item.type == 'simml') && content != '') {
									contentC = displayResult2(content);
								}
								if (item.type == 'date' && content != '') {
									contentC = Ext.util.Format.date(content, 'Y-m-d');
								}
                                //Document id navigation
				                if (uuidExpr.test(item.input)) {
					                dblClick = " ondblclick='editFromViewer(\"" + item.input + "\")'";
				                }
								if (contentC == '' && item.children != null) {
									oldPad.push(pad);
                                    smallHeader = smallHeader + 1;
									pad = 0;
									contentC = rfn(item);
									pad = oldPad.pop();
                                    smallHeader = smallHeader - 1;
								}
                                var rowLink = "";
                                if (first) {
                                    rowLink = "<a name='" + parentPath + "'><div id=\"row\"></div></a>";
                                    first = false;
                                }
								content = content + "<td>" + rowLink + "<a name='" + jsonPath + "'><div onclick='focusOnLeft(this)'" + dblClick + ">" + contentC + "</div></a></td>";
						  	}
						}
						if (!found) {
							//Empty column
							content = content + "<td><a><div></div></a></td>";
						}
					}
					if (a.pos == 0) {
						headers = "<tr>" + headers + "</tr>";
					}
					return headers + "<tr><a name='" + jsonPath + "'><div onclick='focusOnLeft(this)'>" + content + "</div></a></tr>";
				}
				//Table Column
				if (a.item.pureType == 'tableColumn') {
					//Already printed in table row directive for visualization issues when differente column number
					return '';
				}
				//simml
				if (a.item.pureType == 'simml') {
					if (content != '') {
						content = "<div class='" + contentStyle + "' style='padding-left:" + (pad + padh) + "px;' onclick='focusOnLeft(this)'" + dblClick + ">" + content + "</div>"
					}
					else {
						pad = pad + padh;
						content = rfn(a.child.item);
						pad = pad - padh;
					}
					return "<a name='" + jsonPath + "'><div class='simml' style='padding-left:" + pad + "px;' onclick='focusOnLeft(this)'>" + header + "</div>" + content + "</a>";
				}
				//clear
				if (a.item.pureType == 'clear') {
					return "<a name='" + jsonPath + "'><div id=\"clear\"></div></a>" + rfn(a.child.item);
				}
				//mathML
				if (a.item.pureType == 'mathML') {
					return "<a name='" + jsonPath + "'><div class='" + contentStyle + "' style='padding-left:" + pad + "px;' onclick='focusOnLeft(this)'" + dblClick + ">" + content + "</div></a>";
				}
				//attribute
				if (a.item.pureType == 'attribute') {
					return "<a name='" + jsonPath + "'><div class='attribute' style='padding-left:" + pad + "px;' onclick='focusOnLeft(this)'" + dblClick + ">Attribute " + a.item.tag + " : " + content + "</div></a>";
				}

				//Non-formated element
				if (content != '') {
					content = "<div class='" + contentStyle + "' style='padding-left:" + (pad + padh) + "px;' onclick='focusOnLeft(this)'" + dblClick + ">" + content + "</div>"
				}
				else {
					pad = pad + padh;
					content = rfn(a.child.item);
					pad = pad - padh;
				}
				return "<a name='" + jsonPath + "'><div id='" + headerStyle + "' style='padding-left:" + pad + "px;' onclick='focusOnLeft(this)'>" + header + "</div>" + content + "</a>";
			  }
			}
		      }
		    };
        	var rfn = $p('div.treeItem').compile(directive);
    		$p('div.treeItem').render( data, rfn );
	}

	function formatString(string) {
	    return (string.charAt(0).toUpperCase() + string.slice(1)).split(/(?=[A-Z][a-z])/).join(' ');
	}

	function contains(list, value) {
    		return ($.inArray(value, list) > -1);
	}
