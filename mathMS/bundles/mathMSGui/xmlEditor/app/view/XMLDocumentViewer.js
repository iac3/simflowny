var autosave_task = {
    run: automaticSave,
    interval: 15000
}

Ext.define('sf.view.XMLDocumentViewer', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.xmldocumentViewer',
    title: 'Viewer',
    hideHeaders: false,
    layout: 'fit',
    id: 'xmlViewer',
    iconCls: 'no-icon',
    items: [{
	    id : 'xmlViewerDiv',
            xtype: 'component',
            html: '<div id="xmlViewerDiv" tabindex="1" />',
	        autoScroll: true					
        }],
    dockedItems: [{
        xtype: 'toolbar',
        id: 'pathToolbar',
        dock: 'bottom',
        minHeight: 37,
        flex: 1,
        items: [
                {
                    xtype: 'checkboxfield',
                    boxLabel: 'Autosave:',
                    boxLabelAlign: 'before',
                    defaultType: 'checkboxfield',
                    id: 'check_autosave',
                    checked: readAutosave(),
                    listeners: {
                        change: changeAutoSave                    }
                },
                {
                    xtype : 'label',
                    text: 'File Path:'
                },
                {
                    xtype : 'label',
                    text: '',
                    flex: 1,
                    cls:'bold',
                    id: 'documentPathLabel'
                }
        ]
    }],
});



function changeAutoSave(button, newValue, oldValue, eOpts) {
    localStorage["autosave"] = newValue;
    if (newValue) {
        Ext.TaskManager.start(autosave_task);
    } 
    else {
        Ext.TaskManager.stop(autosave_task) 
    }
}


function readAutosave() {
    var autosave_value = localStorage["autosave"] === 'true';
    if (autosave_value == undefined) {
        Ext.TaskManager.start(autosave_task);
        return true;
    }
    else {
        if (autosave_value == true) {
           Ext.TaskManager.start(autosave_task); 
        }
        return autosave_value;
    }
}