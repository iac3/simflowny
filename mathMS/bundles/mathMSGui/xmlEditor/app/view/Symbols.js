Ext.define('sf.view.Symbols', {
    extend: 'Ext.Window',
    alias: 'widget.symbols',
    id: 'symbols',
    floating: true,
    centered: true,
    title: 'Greek characters',
    bodyPadding: 10,
    tools:[{
        type:'help',
        tooltip: 'Copy the symbol and paste it as needed'
    }],
    layout: {
            type: 'table',
            columns: 6
    },
    defaults:{
        xtype: 'label'

    },
    closable: true,
    resizable: false,
    closeAction: 'hide',
    items: [{
        text: "\u0391",
    },{
        text: "\u03B1"
    },{
        text: "\u039A"
    },{
        text: "\u03BA"
    },{
        text: "\u03A4"
    },{
        text: "\u03C4"
    },{
        text: "\u0392"
    },{
        text: "\u03B2"
    },{
        text: "\u039B"
    },{
        text: "\u03BB"
    },{
        text: "\u03A5"
    },{
        text: "\u03C5"
    },{
        text: "\u0393"
    },{
        text: "\u03B3"
    },{
        text: "\u039C"
    },{
        text: "\u03BC"
    },{
        text: "\u03A6"
    },{
        text: "\u03C6"
    },{
        text: "\u0394"
    },{
        text: "\u03B4"
    },{
        text: "\u039D"
    },{
        text: "\u03BD"
    },{
        text: "\u03A7"
    },{
        text: "\u03C7"
    },{
        text: "\u0395"
    },{
        text: "\u03B5"
    },{
        text: "\u039E"
    },{
        text: "\u03BE"
    },{
        text: "\u03A8"
    },{
        text: "\u03C8"
    },{
        text: "\u0396"
    },{
        text: "\u03B6"
    },{
        text: "\u039F"
    },{
        text: "\u03BF"
    },{
        text: "\u03A9"
    },{
        text: "\u03C9"
    },{
        text: "\u0397"
    },{
        text: "\u03B7"
    },{
        text: "\u03A0"
    },{
        text: "\u03C0"
    },{
        text: "\u03D2"
    },{
        text: "\u03D1"
    },{
        text: "\u0398"
    },{
        text: "\u03B8"
    },{
        text: "\u03A1"
    },{
        text: "\u03C1"
    },{
        text: "\u03D6"
    },{
        text: "\u03C2"
    },{
        text: "\u0399"
    },{
        text: "\u03B9"
    },{
        text: "\u03A3"
    },{
        text: "\u03C3"
    }]
});

var greekPopup = Ext.create('sf.view.Symbols', {});

/*
 * Opens the popup
 */
function openGreekPopup() {
    greekPopup.show();
}

function closeGreekPopup() {
    greekPopup.hide();
}

