Ext.define('Manager.model.XMLDocuments', {
    extend: 'Ext.data.Model',
    fields: [{name: 'dataindex', type: 'string'},
    {name: 'id', type: 'string'},
    {name: 'type', type: 'string'},
    {name: 'name', type: 'string'},
    {name: 'author', type: 'string'},
    {name: 'date', type: 'date'},
    {name: 'version', type: 'string'},
    {name: 'Description', type: 'string'}]
});
