Ext.define('Manager.controller.XMLDocuments', {
    extend: 'Ext.app.Controller',

    refs: [{
        ref: 'stationsList',
        selector: 'stationslist'
    }],

    stores: ['XMLDocuments'],
    
   
    
    onLaunch: function() {
        var XMLDocumentsStore = this.getXMLDocumentsStore();        
        XMLDocumentsStore.load({
            callback: this.onXMLDocumentsLoad,
            scope: this
        });
    }
});
