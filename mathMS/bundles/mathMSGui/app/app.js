Ext.Loader.setPath('Ext.ux', 'extjs/examples/ux');

Ext.require([
    'Ext.selection.CellModel',
    'Ext.grid.*',
    'Ext.data.*',
    'Ext.util.*',
    'Ext.state.*',
    'Ext.form.*',
    'Ext.ux.CheckColumn',
    'Ext.dd.*'
]);

Ext.application({
    name: 'Manager',
    autoCreateViewport: true,
    models: ['XMLDocuments'],    
    stores: ['XMLDocuments', 'TreeStore'],
    launch: function(){
        this.viewport = Ext.ComponentQuery.query('viewport')[0]; 
        this.centerRegion = this.viewport.down('[region=center]');
        initApp();

       // Ext.FocusManager.enable(true);
    }
});


//funcion que inicializa todo una vez cargado
function initApp() {
    //Capture close window and save collection in sessionStorage
    Ext.EventManager.on(window, 'beforeunload', function() {
        setSessionStorage();
    });
    //Check cookies
    checkCookies()
    //Loads the special actions
    getSpecialActions();
    //Hides the buttons
    hideOptions();
    //Select collection and document if comes from the editor
    selectPathDocument();
    //Loads the collection tree
    getCollectionTree();
} 

function selectPathDocument() {
    //Look for the sessionStorage, variables will be there in case of page refresh (To allow refreshing)
    Manager.globals.vars.docId = window.sessionStorage.getItem("docManager.documentID");
    Manager.globals.vars.docPath = window.sessionStorage.getItem("docManager.documentPath");
    if (Manager.globals.vars.docPath == undefined || Manager.globals.vars.docPath == null || Manager.globals.vars.docPath == 'null') {
        //Load variables from cookies
        Manager.globals.vars.docId = Ext.util.Cookies.get("docManager.documentID");
        Manager.globals.vars.docPath = Ext.util.Cookies.get("docManager.documentPath");
        //Nullify cookies after usage
        Ext.util.Cookies.set("docManager.documentID", null);
        Ext.util.Cookies.set("docManager.documentPath", null);
    }
}

function setSessionStorage() {
    //Create the sessionStorage variables to allow refreshing without losing the current document
    window.sessionStorage.setItem('docManager.documentID', Manager.globals.vars.docId);
    window.sessionStorage.setItem('docManager.documentPath', Manager.globals.vars.docPath);
}

function checkCookies() {
    document.cookie = "__verify=1";
    var supportsCookies = document.cookie.length >= 1 && 
    document.cookie.indexOf("__verify=1") !== -1;
    var thePast = new Date(1976, 8, 16);
    document.cookie = "__verify=1;expires=" + thePast.toUTCString();
    if (!supportsCookies) {
        showMessage("Cookies", message.COOKIES, Ext.MessageBox.ERROR);
    }
}

/**
 * Hides the buttons in the Main Toolbar
**/
function hideOptions() {
    var menuOption = Ext.getCmp('editMenu');
    menuOption.hide();
    menuOption = Ext.getCmp('duplicateMenu');
    menuOption.hide();
    menuOption = Ext.getCmp('deleteMenu');
    menuOption.hide();
    menuOption = Ext.getCmp('downloadMenu');
    menuOption.hide();  
    for (var i in Manager.globals.vars.specialActions) {
        var methods = Manager.globals.vars.specialActions[i];
        for (var j in methods) {
            var method = methods[j];
            menuOption = Ext.getCmp(method.method + "SpecialAction");
            menuOption.hide();
        }
    }

} 

/**
 * Read the document special actions
**/
function getSpecialActions() {
    $.getJSON("data/actions.json", function(json) {
        var structAux = json;   
        struct = new Array();
        var counter = 8;
        for(var idx in structAux) {
            //Creates the special action structure
            var specialAction = structAux[idx];
            var documents = specialAction.targetDocuments;
            var newAction = jQuery.extend(true, {}, specialAction);
            delete newAction.targetDocuments;
            for (var i in specialAction.targetDocuments) {
                var docType = specialAction.targetDocuments[i].name;
                if (struct[docType] == undefined) {
                    struct[docType] = new Array();
                }
                struct[docType].push(newAction);
            }

            //Create specialAction buttons and add it to the menu
            var newButton = Ext.create('Ext.Button', {
                id: specialAction.method + "SpecialAction",
                tooltip: 'Special action: ' + formatString(specialAction.method),
                arrowCls: '',
                icon: specialAction.icon,
                scale: 'large',
                width: 40,
                height: 40, 
                hidden: true,
                tabIndex: counter,              
                enableToggle: false,
                listeners: {
                    click: performSpecialAction,
                    render: function (obj) {
                                var c = Ext.create('Ext.util.KeyMap', this.id, {
                                    key: Ext.EventObject.ENTER,
                                    fn: function () {
                                        performSpecialAction(obj);
                                    }
                                });
                             }
                }
            });
            Ext.getCmp('maintoolbar').add(newButton);

            //Create specialAction item for the contextual menu
            var newItem = Ext.create('Ext.menu.Item', {
                    id: specialAction.method + "SpecialActionContextual",
                    text: formatString(specialAction.method),
                    iconAlign: '',
                    icon: reduceIconSize(specialAction.icon), 
                    handler: performSpecialAction
            });

            counter = counter + 1;
        }
        Manager.globals.vars.specialActions = struct;
    });
}

function reduceIconSize(iconPath) {
    return iconPath.substring(0, iconPath.lastIndexOf('.')) + "_16x16" + iconPath.substring(iconPath.lastIndexOf('.'));
}

function downloadFile(data, name) {
    var link = document.createElement("a");
    link.download = name;
    link.href = data;
    var clickEvent = new MouseEvent("click", {
        "view": window,
        "bubbles": true,
        "cancelable": false
    });

    link.dispatchEvent(clickEvent);
}

function formatString(string) {
    return (string.charAt(0).toUpperCase() + string.slice(1)).split(/(?=[A-Z][a-z])/).join(' ');
}

