Ext.define('Manager.store.XMLDocuments', {
    extend: 'Ext.data.Store',
    requires: 'Manager.model.XMLDocuments',    
    model: 'Manager.model.XMLDocuments',
    autoLoad: false,
       proxy: {
		type: 'memory',
        reader: {
            type: 'json',
            root: 'results'
        }
    }
});
