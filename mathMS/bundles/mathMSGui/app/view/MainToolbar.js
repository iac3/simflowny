/**
 * @author Antoni Artigues
 */
Ext.define('Manager.view.MainToolbar', {
    extend: 'Ext.toolbar.Toolbar',
    alias: 'widget.maintoolbar',
    id: 'maintoolbar',
    layout: {
                type: 'hbox',
                align: 'center',
                pack: 'center'
            },
    baseCls: '',
    items: [{
            xtype: 'button',
            cls: 'x-btn-icon',
            arrowCls: '',
            icon: 'resources/images/plus.png',
            id:'plusMenu',
            tooltip:'Create new document',
            scale: 'large',
            minWidth: 40,
            minHeight: 40,
            tabIndex: '1',
            action: 'addFolder',
            enableToggle: false,
            menu : {
                    xtype: 'menu',
                    showSeparator : false,
                    minWidth: 20,
                    id:'plusSubMenu',
                    minHeight: 20,
                    padding : 0,
                    plain: true,
                    
                    items: [{
                        id:'PDEModel',
                        xtype: 'button',
                        iconCls: 'button-menu',
                        cls: 'botonSimml',
                        icon: 'resources/images/document_PDEModel.png',
                        text: 'PDE Model',
                        textAlign: 'left',
                        handler: handleNewDocument,
                        listeners: {
                            render: function (obj) {
                                Ext.create('Ext.util.KeyMap','PDEModel', {
                                    key: 13, //Enter key
                                    fn: function () {
                                        handleNewDocument(obj);
                                    }
                                });
                             }
                        },
                    },                
                    {
                        id:'PDEProblem',
                        xtype: 'button',
                        cls: 'botonSimml',
                        iconCls: 'button-menu',
                        icon: 'resources/images/document_PDEProblem.png',
                        text: 'PDE Problem',
                        textAlign: 'left',
                        handler: handleNewDocument,
                        listeners: {
                            render: function (obj) {
                                Ext.create('Ext.util.KeyMap','PDEProblem', {
                                    key: 13, //Enter key
                                    fn: function () {
                                        handleNewDocument(obj);
                                    }
                                });
                             }
                        },
                    },
                    {
                        id:'PDEDiscretizationSchema',
                        xtype: 'button',
                        cls: 'botonSimml',
                        iconCls: 'button-menu',
                        icon: 'resources/images/document_PDEDiscretizationSchema.png',
                        text: 'PDE Discretizacion Schema',
                        textAlign: 'left',
                        handler: handleNewDocument,
                        listeners: {
                            render: function (obj) {
                                Ext.create('Ext.util.KeyMap','PDEDiscretizationSchema', {
                                    key: 13, //Enter key
                                    fn: function () {
                                        handleNewDocument(obj);
                                    }
                                });
                             }
                        },
                    },
                    {
                        id:'transformationRule',
                        xtype: 'button',
                        cls: 'botonSimml',
                        iconCls: 'button-menu',
                        icon: 'resources/images/document_transformationRule.png',
                        text: 'Transformation rule',
                        textAlign: 'left',
                        handler: handleNewDocument,
                        listeners: {
                            render: function (obj) {
                                Ext.create('Ext.util.KeyMap','transformationRule', {
                                    key: 13, //Enter key
                                    fn: function () {
                                        handleNewDocument(obj);
                                    }
                                });
                             }
                        },
                    },
                    {
                        id:'spatialOperatorDiscretization',
                        xtype: 'button',
                        cls: 'botonSimml',
                        iconCls: 'button-menu',
                        icon: 'resources/images/document_spatialOperatorDiscretization.png',
                        text: 'Spatial Operator Discretization',
                        textAlign: 'left',
                        handler: handleNewDocument,
                        listeners: {
                            render: function (obj) {
                                Ext.create('Ext.util.KeyMap','spatialOperatorDiscretization', {
                                    key: 13, //Enter key
                                    fn: function () {
                                        handleNewDocument(obj);
                                    }
                                });
                             }
                        },
                    },
                    {
                        id:'PDEDiscretizationPolicy',
                        xtype: 'button',
                        cls: 'botonSimml',
                        iconCls: 'button-menu',
                        icon: 'resources/images/document_PDEDiscretizationPolicy.png',
                        text: 'PDE Discretization Policy',
                        textAlign: 'left',
                        handler: handleNewDocument,
                        listeners: {
                            render: function (obj) {
                                Ext.create('Ext.util.KeyMap','PDEDiscretizationPolicy', {
                                    key: 13, //Enter key
                                    fn: function () {
                                        handleNewDocument(obj);
                                    }
                                });
                             }
                        },
                    }/*,             //Out in current version pending on merge with PDE 
                    '-',
                    {
                        id:'agentBasedModelGraph',
                        xtype: 'button',
                        cls: 'botonSimml',
                        iconCls: 'button-menu',
                        icon: 'resources/images/document_agentBasedModelGraph.png',
                        text: 'Agent Based Model on graph',
                        textAlign: 'left',
                        handler: handleNewDocument,
                        listeners: {
                            render: function (obj) {
                                Ext.create('Ext.util.KeyMap','agentBasedModelGraph', {
                                    key: 13, //Enter key
                                    fn: function () {
                                        handleNewDocument(obj);
                                    }
                                });
                             }
                        },
                    },     
                    {
                        id:'agentBasedModelSpatialDomain',
                        xtype: 'button',
                        cls: 'botonSimml',
                        iconCls: 'button-menu',
                        icon: 'resources/images/document_agentBasedModelSpatialDomain.png',
                        text: 'Agent Based Model on spatial domain',
                        textAlign: 'left',
                        handler: handleNewDocument,
                        listeners: {
                            render: function (obj) {
                                Ext.create('Ext.util.KeyMap','agentBasedModelSpatialDomain', {
                                    key: 13, //Enter key
                                    fn: function () {
                                        handleNewDocument(obj);
                                    }
                                });
                             }
                        },
                    },
                    {
                        id:'agentBasedProblemGraph',
                        xtype: 'button',
                        cls: 'botonSimml',
                        iconCls: 'button-menu',
                        icon: 'resources/images/document_agentBasedProblemGraph.png',
                        text: 'Agent Based Problem on graph',
                        textAlign: 'left',
                        handler: handleNewDocument,
                        listeners: {
                            render: function (obj) {
                                Ext.create('Ext.util.KeyMap','agentBasedProblemGraph', {
                                    key: 13, //Enter key
                                    fn: function () {
                                        handleNewDocument(obj);
                                    }
                                });
                             }
                        },
                    },     
                    {
                        id:'agentBasedProblemSpatialDomain',
                        xtype: 'button',
                        cls: 'botonSimml',
                        iconCls: 'button-menu',
                        icon: 'resources/images/document_agentBasedProblemSpatialDomain.png',
                        text: 'Agent Based Problem on spatial domain',
                        textAlign: 'left',
                        handler: handleNewDocument,
                        listeners: {
                            render: function (obj) {
                                Ext.create('Ext.util.KeyMap','agentBasedProblemSpatialDomain', {
                                    key: 13, //Enter key
                                    fn: function () {
                                        handleNewDocument(obj);
                                    }
                                });
                             }
                        },
                    }*/]
                }
        },
        {           
                xtype: 'button',
                id:'importButton',
                tooltip:'Import document',
                cls: 'x-btn-icon',
                arrowCls: '',
                icon: 'resources/images/upload.png',
                scale: 'large',
                width: 40,
                height: 40,
                tabIndex: '3',
                handler: function() {
                    document.getElementById('upload-field').click();
                }
        },
        //Input for files hidden to avoid browser tooltip http://code.tonytuan.org/2013/08/extjs-file-field-wrapper-show-custom.html
        {
                hidden : true,
                xtype : 'component',
                html : '<input type="file" onchange="javascript:handleImportDocument();" id="upload-field"/>'
        }, 
        {           
                xtype: 'button',
                id:'editMenu',
                tooltip:'Edit document',
                cls: 'x-btn-icon',
                arrowCls: '',
                icon: 'resources/images/edit.png',
                scale: 'large',
                width: 40,
                height: 40,
                tabIndex: '5',
                handler: edit
         },     
        {           
                xtype: 'button',
                id:'duplicateMenu',
                tooltip:'Duplicate document',
                cls: 'x-btn-icon',
                arrowCls: '',
                icon: 'resources/images/duplicate.png',
                scale: 'large',
                width: 40,
                height: 40,
                tabIndex: '6',
                handler: duplicate
         },         
         {
                xtype: 'button',
                id:'downloadMenu',
                tooltip:'Download document',
                cls: 'x-btn-icon',
                arrowCls: '',
                icon: 'resources/images/download.png',
                scale: 'large',
                width: 40,
                height: 40,
                tabIndex: '7',
                action: 'zip',
                handler: hadleDownloadDocument
        },
        {
                xtype: 'button',
                id:'deleteMenu',
                tooltip:'Delete document',
                cls: 'x-btn-icon',
                arrowCls: '',
                icon: 'resources/images/delete.png',
                scale: 'large',
                width: 40,
                height: 40,
                tabIndex: '8',
                action: 'delete',
                handler: deleteDocuments
        }]  
});

/**
 * Download the selected documents.
**/
function hadleDownloadDocument(obj, evt) {
    var comp = Ext.getCmp('xmldocumentslist');

    var selected = comp.getSelectionModel().getSelection();
    for (var i = 0; i < selected.length; i++) {
        downloadDocument(selected[i].get('id'), selected[i].get('name'));
    }
}

/**
 * Call to the download SOAP web service.
**/
function downloadDocument(documentId, documentName) {
    $.soap({
        url: '/cxf/DocumentManager/',
        method: 'downloadDocument',
        namespaceQualifier: 'myns',
        namespaceURL: 'http://documentManagerPlugin.mathMS.iac3.eu/',
        data: {
            arg0: documentId
        },
        success: function (soapResponse) {
            try {
                var response = soapResponse.toXML().getElementsByTagNameNS('http://documentManagerPlugin.mathMS.iac3.eu/',"downloadDocumentResponse")[0].childNodes[0].innerHTML;
                //Download the file 
                var dataUri="data:application/zip;base64," + response;
                downloadFile(dataUri, "download_" + documentName + ".zip");
            } catch (e) {
                showMessage("Document load", message.ERROR_DOWNLOAD.replace("{0}", e), Ext.MessageBox.ERROR);
                return null;
            }
        },
        error: function (SOAPResponse) {
            var msg = SOAPResponse.toXML().getElementsByTagName("faultstring")[0].childNodes[0].nodeValue;
            showMessage("Document load", message.ERROR_DOWNLOAD.replace("{0}", msg), Ext.MessageBox.ERROR);
            //Refresh the document list
            getDocumentList();
        }
    });
}

/**
 *  Opens the editor with the selected document.
**/
function edit() {
    //Getting the current document Id
    var grid = Ext.getCmp('xmldocumentslist');
    if (grid.getSelectionModel().hasSelection()) {
       var row = grid.getSelectionModel().getSelection()[0];
       var id = row.get('id');
       var type = row.get('type');
    }

    //Store the cookie with the selected document id
    Ext.util.Cookies.set('xmlEditor.documentID', id);
    Ext.util.Cookies.set('xmlEditor.documentType', type);
    Ext.util.Cookies.set('xmlEditor.documentPath', Manager.globals.vars.docPath);
    window.open('xmlEditor/index.html','_newtab');        
}

function duplicate() {
    //Getting the current document Id
    var grid = Ext.getCmp('xmldocumentslist');
    if (grid.getSelectionModel().hasSelection()) {
       var row = grid.getSelectionModel().getSelection()[0];
       var id = row.get('id');
       showProgressBar(message.DUPLICATING);
       //Getting the document
        $.soap({
        url: '/cxf/DocumentManager/',
        method: 'duplicateDocument',
        namespaceQualifier: 'myns',
        namespaceURL: 'http://documentManagerPlugin.mathMS.iac3.eu/',
        data: {
            arg0: id
        },
        success: function (soapResponse) {
            Ext.MessageBox.hide();
            try {
                //Refresh the document list
                getDocumentList(true);
            } catch (e) {
                showMessage("Duplicate document ", message.ERROR_DELETE.replace("{0}", e), Ext.MessageBox.ERROR);
                return null;
            }
        },
        error: function (SOAPResponse) {
            Ext.MessageBox.hide();
            var msg = SOAPResponse.toXML().getElementsByTagName("faultstring")[0].childNodes[0].nodeValue;
            showMessage("Duplicate document ", message.ERROR_DELETE.replace("{0}", msg), Ext.MessageBox.ERROR);
            //Refresh the document list
            getDocumentList();
        }
    }); 
    }
}

/**
 *  Deletes the selected documents.
**/
var docsToDelete = 0;
var dependency = false;
var checkOk = true;
function deleteDocuments() {

    var grid = Ext.getCmp('xmldocumentslist');
    if (grid.getSelectionModel().hasSelection()) {
        var rows = grid.getSelectionModel().getSelection();
        //Cannot delete locked documents
        var noLocked = true;
        for (var i = 0; i < rows.length; i++) {
            var id = rows[i].get('id');
            if (localStorage.getItem(id) !== null) {
                showMessage("Document locked", message.ERROR_DELETE_LOCKED.replace("{0}", id), Ext.MessageBox.ERROR);
                noLocked = false;
            }
        }
        if (noLocked) {
            Ext.MessageBox.confirm('Confirm', message.DELETE_CONFIRM, checkAndDeleteDocumentBoxHandler);
        }
    }
}
function checkAndDeleteDocumentBoxHandler(btn) {
    if (btn == 'yes') {
        dependency = false;
        console.log("Chequeando");
        showProgressBar(message.CHECKING_DEPENDENCIES);
        //MessageBox as ProgressBar
        var grid = Ext.getCmp('xmldocumentslist');
        if (grid.getSelectionModel().hasSelection()) {
            var rows = grid.getSelectionModel().getSelection();
            docsToDelete = rows.length
            for (var i = 0; i < rows.length; i++) {
                var id = rows[i].get('id');
                checkDocument(id);
            }
        }
    }
}

function deleteDocumentsHandler(btn) {
    if (btn == 'yes') {
        showProgressBar(message.DELETING);
        //MessageBox as ProgressBar
        var grid = Ext.getCmp('xmldocumentslist');
        if (grid.getSelectionModel().hasSelection()) {
            var rows = grid.getSelectionModel().getSelection();
            docsToDelete = rows.length
            for (var i = 0; i < rows.length; i++) {
                var id = rows[i].get('id');
                deleteDocument(id);
            }
        }
    }
}

function checkDocument(id) {
    var idToDelete = id
    $.soap({
        url: '/cxf/DocumentManager/',
        method: 'getParentDependencies',
        namespaceQualifier: 'myns',
        namespaceURL: 'http://documentManagerPlugin.mathMS.iac3.eu/',
        data: {
            arg0: id
        },
        success: function (soapResponse) {
            try {
                var xmldoc = soapResponse.toXML();
                xmldoc.normalize();
                var json = xmldoc.getElementsByTagNameNS('http://documentManagerPlugin.mathMS.iac3.eu/',"getParentDependenciesResponse")[0].childNodes[0].childNodes[0].nodeValue;
                var dataJSON = jQuery.parseJSON(json);

     console.log("Dentro de check");
     console.log(dataJSON);

                if(dataJSON.length > 0) {
                    console.log("encuentro dependencia");
                    dependency = true;
                }
                docsToDelete = docsToDelete - 1;
                if (docsToDelete == 0) {
                    if (checkOk) {
                        if (dependency) {
                            Ext.MessageBox.confirm('Confirm', message.DELETE_DEPENDENCY_CONFIRM, deleteDocumentsHandler);
                        }
                        else {
                            deleteDocumentsHandler('yes');
                        }
                    }
                }

            } catch (e) {
                checkOk = false;
                showMessage("Document dependencies", message.ERROR_DEPENDENCIES.replace("{0}", e), Ext.MessageBox.ERROR);
                return null;
            }
        },
        error: function (SOAPResponse) {
            checkOk = false;
            docsToDelete = docsToDelete - 1;
            var msg = SOAPResponse.toXML().getElementsByTagName("faultstring")[0].childNodes[0].nodeValue;
            showMessage("Document dependencies", message.ERROR_DEPENDENCIES.replace("{0}", msg), Ext.MessageBox.ERROR);
        }
    });
}

/**
 *  Deletes the selected document.
**/
function deleteDocument(documentId) {
    $.soap({
        url: '/cxf/DocumentManager/',
        method: 'deleteDocument',
        namespaceQualifier: 'myns',
        namespaceURL: 'http://documentManagerPlugin.mathMS.iac3.eu/',
        data: {
            arg0: documentId
        },
        success: function (soapResponse) {
            docsToDelete = docsToDelete - 1;
            if (docsToDelete == 0) {
                Ext.MessageBox.hide();
                try {
                    showProgressBar(message.LOADING);
                    getDocumentsLaunch = true;
                    //Refresh the document list
                    getDocumentList(true);
                } catch (e) {
                    showMessage("Document delete", message.ERROR_DELETE.replace("{0}", e), Ext.MessageBox.ERROR);
                    return null;
                }
            }

        },
        error: function (SOAPResponse) {
            docsToDelete = docsToDelete - 1;
            if (docsToDelete == 0) {
                Ext.MessageBox.hide();
            }
            var msg = SOAPResponse.toXML().getElementsByTagName("faultstring")[0].childNodes[0].nodeValue;
            showMessage("Document delete", message.ERROR_DELETE.replace("{0}", msg), Ext.MessageBox.ERROR);
        }
    }); 
}

/**
 * Triggers the process of creating a new document.
**/
function handleNewDocument(obj, evt) {

    if (Manager.globals.vars.docPath != undefined) {
        Ext.util.Cookies.set('xmlEditor.documentID', '');
        Ext.util.Cookies.set('xmlEditor.documentType', obj.id);
        Ext.util.Cookies.set('xmlEditor.documentPath', Manager.globals.vars.docPath);
        window.open('xmlEditor/index.html','_newtab');
        Ext.getCmp('plusSubMenu').hide();
    }
    else {
        showMessage("New Document", message.SELECT_FOLDER, Ext.MessageBox.ERROR);
    }
}

/*
 * 1 - Transform XML to JSON
 * 2 - Validates document
 * 3 - Saves document independently of its validation
 */
function validateAndSave(data) {
//Reseting the id in the header (it is the first occurrence)
    var document = data.replace(/<mms:id>[0-9A-Za-z\-]*<\/mms:id>|<mms:id\/>/, "<mms:id/>");
    Ext.MessageBox.updateProgress(0.3, message.PARSING_JSON);
    //XML to JSON
    //Convert xml document to json
    $.soap({
        url: '/cxf/SimflownyUtils/',
        method: 'xmlToJSON',
        namespaceQualifier: 'myns',
            namespaceURL: 'http://simflownyUtilsPlugin.mathMS.iac3.eu/',

        data: {
        arg0: document,
        arg1: false
        },

        success: function (soapResponse) {
            try {
                //Get response, normalization needed in Firefox
                var xmldoc = soapResponse.toXML();
                xmldoc.normalize();
                json = xmldoc.getElementsByTagNameNS('http://simflownyUtilsPlugin.mathMS.iac3.eu/',"xmlToJSONResponse")[0].childNodes[0].childNodes[0].nodeValue;
                Ext.MessageBox.updateProgress(0.6, message.VALIDATING);
                //Validate previous to importing
                $.soap({
                    url: '/cxf/DocumentManager/',
                    method: 'validateDocument',
                        namespaceQualifier: 'myns',
                        namespaceURL: 'http://documentManagerPlugin.mathMS.iac3.eu/',

                    data: {
                        arg0: json,
                        arg1: false
                    },

                    success: function (soapResponse) {
                        Ext.MessageBox.updateProgress(0.9, message.SAVING);
                        //Saving document
                        $.soap({
                                url: '/cxf/DocumentManager/',
                                method: 'saveDocument',
                                namespaceQualifier: 'myns',
                                namespaceURL: 'http://documentManagerPlugin.mathMS.iac3.eu/',
                                data: {
                                    arg0: json,
                                    arg1: Manager.globals.vars.docPath
                                },
                                success: function (soapResponse) {
                                    try {
                                        console.log("File imported correctly");
                                        //Refresh document list
                                        getDocumentList(true);
                                        Ext.MessageBox.hide();
                                    } catch (e) {
                                        showMessage("Import document", message.ERROR_SAVE.replace("{0}", e), Ext.MessageBox.ERROR);
                                        Ext.MessageBox.hide();
                                        return null;
                                    }
                                },
                                error: function (SOAPResponse) {
                                    Ext.MessageBox.hide();
                                    var msg = SOAPResponse.toXML().getElementsByTagName("faultstring")[0].childNodes[0].nodeValue;
                                    showMessage("Import document", message.ERROR_SAVE.replace("{0}", msg), Ext.MessageBox.ERROR);
                                }
                            });
                    },
                    error: function (SOAPResponse) {
                        Ext.MessageBox.hide();
                        var msg = SOAPResponse.toXML().getElementsByTagName("faultstring")[0].childNodes[0].nodeValue;
                        showMessage("Import document", message.ERROR_VALIDATION.replace("{0}", msg), Ext.MessageBox.ERROR);
                        //Saving document anyway
                        $.soap({
                                url: '/cxf/DocumentManager/',
                                method: 'saveDocument',
                                namespaceQualifier: 'myns',
                                namespaceURL: 'http://documentManagerPlugin.mathMS.iac3.eu/',
                                data: {
                                    arg0: json,
                                    arg1: Manager.globals.vars.docPath
                                },
                                success: function (soapResponse) {
                                    try {
                                        console.log("File imported correctly");
                                        //Refresh document list
                                        getDocumentList(true);
                                        Ext.MessageBox.hide();
                                    } catch (e) {
                                        showMessage("Import document", message.ERROR_SAVE.replace("{0}", e), Ext.MessageBox.ERROR);
                                        Ext.MessageBox.hide();
                                        return null;
                                    }
                                },
                                error: function (SOAPResponse) {
                                    Ext.MessageBox.hide();
                                    var msg = SOAPResponse.toXML().getElementsByTagName("faultstring")[0].childNodes[0].nodeValue;
                                    showMessage("Import document", message.ERROR_SAVE.replace("{0}", msg), Ext.MessageBox.ERROR);
                                }
                            });                                        
                    }
                });
            } catch (e) {
                Ext.MessageBox.hide();
                showMessage("Import document", message.ERROR_IMPORT.replace("{0}", e), Ext.MessageBox.ERROR);
                return null;
            }
        },
        error: function (SOAPResponse) {
            Ext.MessageBox.hide();
            var msg = SOAPResponse.toXML().getElementsByTagName("faultstring")[0].childNodes[0].nodeValue;
            showMessage("Import document", message.ERROR_IMPORT.replace("{0}", msg), Ext.MessageBox.ERROR);
        }
    });
}

/*
 * Checks if the dom parsing is an error
 */
function isParseError(parsedDocument) {
    // parser and parsererrorNS could be cached on startup for efficiency
    var parser = new DOMParser(),
        errorneousParse = parser.parseFromString('<', 'text/xml'),
        parsererrorNS = errorneousParse.getElementsByTagName("parsererror")[0].namespaceURI;

    if (parsererrorNS === 'http://www.w3.org/1999/xhtml') {
        // In PhantomJS the parseerror element doesn't seem to have a special namespace, so we are just guessing here :(
        return parsedDocument.getElementsByTagName("parsererror").length > 0;
    }

    return parsedDocument.getElementsByTagNameNS(parsererrorNS, 'parsererror').length > 0;
};

/**
 * Imports the document from external file.
 * Handle regular documents and X3D.
**/
function handleImportDocument() {
    if (Manager.globals.vars.docPath || Manager.globals.vars.docPath == "") {
        Ext.MessageBox.show({
           title: 'Please wait',
           msg: 'Importing document...',
           progressText: message.READING,
           width:300,
           wait:true,
           waitConfig: {interval:200},
           closable:false,
           animateTarget: 'gcbutton'
        });
        //Reader
        var reader = new FileReader();
        reader.onload = function(evt) {
            var doc;
            //Parse XML
            var parser = new DOMParser();
            doc = parser.parseFromString(evt.target.result, "text/xml");
            if(isParseError(doc)) {
                //Parse the file as YAML document
                try{
                    console.log(evt.target.result);
                    var yaml = YAML.eval(evt.target.result);
                    $.soap({
                            url: '/cxf/SimflownyUtils/',
                            method: 'yamlToXML',
                            namespaceQualifier: 'myns',
                            namespaceURL: 'http://simflownyUtilsPlugin.mathMS.iac3.eu/',
                            data: {
                                arg0: evt.target.result
                            },
                            success: function (soapResponse) {
                                try {
                                    console.log("Bien");
                                    //Get response, normalization needed in Firefox
                                    var xmldoc = soapResponse.toXML();
                                    xmldoc.normalize();
                                    xml = xmldoc.getElementsByTagNameNS('http://simflownyUtilsPlugin.mathMS.iac3.eu/',"yamlToXMLResponse")[0].childNodes[0].childNodes[0].nodeValue;
                                    console.log("Vale "+xmldoc);
                                    validateAndSave(xml);
                                } catch (e) {
                                    showMessage("Import document", message.ERROR_SAVE.replace("{0}", e), Ext.MessageBox.ERROR);
                                    Ext.MessageBox.hide();
                                    return null;
                                }
                            },
                            error: function (SOAPResponse) {
                                console.log("Mal");
                                Ext.MessageBox.hide();
                                var msg = SOAPResponse.toXML().getElementsByTagName("faultstring")[0].childNodes[0].nodeValue;
                                showMessage("Import document", message.ERROR_SAVE.replace("{0}", msg), Ext.MessageBox.ERROR);
                            }
                        });   
                }
                catch (e) {
                    console.log("Its not a YAML file");
                    console.log(e);
                    showMessage("Load document", message.ERROR_IMPORT.replace("{0}", message.ERROR_NOT_XML), Ext.MessageBox.ERROR); 
                }
            } else {
                console.log("Opcion XML");
                var docType = doc.documentElement.localName;
                //Import the document resetting the document ID.
                if (docType.toLowerCase() != 'x3d') {
                    validateAndSave(evt.target.result);
                }
                //Import X3D and create the meta information file.
                else if (docType.toLowerCase() == 'x3d') {
                    Ext.util.Cookies.set('xmlEditor.documentID', '');
                    Ext.util.Cookies.set('xmlEditor.documentType', docType);
                    Ext.util.Cookies.set('xmlEditor.documentPath', Manager.globals.vars.docPath);
                    if(typeof(Storage) !== "undefined") {
                        // Code for localStorage/sessionStorage.
                        localStorage.setItem("xmlEditor.x3d", evt.target.result);
                    } else {
                        Ext.MessageBox.hide();
                        showMessage("Import document", message.ERROR_STORAGE, Ext.MessageBox.ERROR);
                    }
                    Ext.MessageBox.hide();
                    window.open('xmlEditor/index.html','_newtab');
                }
                else {
                    Ext.MessageBox.hide();
                    showMessage("Import document", message.ERROR_IMPORT.replace("{0}", message.ERROR_NOT_XML), Ext.MessageBox.ERROR);
                }
            }
        };
        reader.onerror = function(evt) {
            Ext.MessageBox.hide();
            showMessage("Load document", message.ERROR_IMPORT.replace("{0}", evt.getMessage()), Ext.MessageBox.ERROR);
        };
        //Read as text
        filecontent = reader.readAsText(document.getElementById('upload-field').files[0]);
                //Reset file field, otherwise it does not allow reload
                document.getElementById('upload-field').value = '';
    }
    else {
        //Reset file field, otherwise it does not allow reload
        document.getElementById('upload-field').value = '';
        showMessage("Import Document", message.SELECT_FOLDER, Ext.MessageBox.ERROR);
    }
}
