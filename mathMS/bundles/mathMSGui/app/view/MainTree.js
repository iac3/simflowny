var documentMovementRemain = 0;
Ext.define('Manager.view.MainTree', {
    extend: 'Ext.tree.Panel',
    alias: 'widget.maintree',
    store: 'TreeStore',
    title: 'Documents tree',
    rootVisible: true,
    viewConfig: {
    		markDirty:false,
	        autoScroll: true,
            preserveScrollOnRefresh: true,
            plugins: {
                ptype: 'treeviewdragdrop',
                appendOnly: false,
                enableDrag: true,
                enableDrop: true,
                ddGroup: 'docManagerDD'
            },
            listeners: {
                beforedrop: function ( node, data, overModel, dropPosition, dropFunction, eOpts ) {
                    //Drag from List
                    if (data.view.xtype == 'gridview') {
                        showProgressBar(message.MOVING);
                        documentMovementRemain = data.records.length;
                        console.log("Antes de lanzar " + documentMovementRemain)
                        for (var i = 0; i < data.records.length; i++) {
                            moveDocument(data.records[i].get("id"), overModel.getPath('text','/'));
                        }
                        reloadDocuments();
                        //The documents have already moved, cancel Drop to avoid empty nodes appear in the tree and avoid need to reload
                        //the tree that does not keep the collapsing state
                        dropFunction.cancelDrop();
                    }
                    //Drag from Tree
                    else if (data.view.xtype == 'treeview') {
                        var origin = data.records[0];
                        var destination = overModel;

                        var colName = origin.get('text');
                        var sourcePath = origin.getPath('text','/');

                        sourcePath = sourcePath.substr(0, sourcePath.lastIndexOf("/"));
                        var destinationPath = destination.getPath('text','/');
                        moveCollection(colName, sourcePath, destinationPath);
                    }
                },
                drop: function ( node, data, overModel, dropPosition, eOpts ) {
                    //Focus after drop
                    if (data.view.xtype == 'treeview') {
                        var tree = Ext.getCmp('maintree');
                        tree.getSelectionModel().select(data.records[0]);
                        tree.getView().focusNode(data.records[0]);
                    }
                }
            }
   },
   dockedItems: [{
        xtype: 'toolbar',
        dock: 'bottom',
        flex: 1,
        items: [
                {
                    xtype: 'label',
                    html: '&copy; IAC&sup3;',
                },
                '->',
                {
                    xtype : 'button',
                    text: 'Disclaimer',
                    handler: openDisclaimer
                }
        ]
   }],
   listeners: {
        itemcontextmenu: contextualMenus,
        itemclick: getDocuments,
        cellkeydown: function(obj, td, cellIndex, record, tr, rowIndex, e, eOpts){
            if (e.getKey() === e.ENTER) {
                var tree = Ext.getCmp('maintree');
                var node = tree.getSelectionModel().getSelection()[0];
                var path = node.getPath('text','/').substring(2);
                //Remove first slash if not root element
                if (path != "") {
                    path = path.substring(1);
                }
                Manager.globals.vars.docPath = path;
                getDocumentList(true);
            }
            if (e.getKey() === e.TAB) {
                e.stopEvent();
                var xmlList = Ext.getCmp('xmldocumentslist');
                xmlList.getSelectionModel().select(0);
                xmlList.getView().focusRow(0);
            }
            //Open context menu as an alternative to menu key, that does not work in sikuli
            if (e.getKey() === e.F7) {
                e.stopEvent();
                obj.fireEvent('itemcontextmenu', obj, record, tr, cellIndex, e);
            }
            if (e.getKey() === e.DELETE) {
                e.stopEvent();
                var tree = Ext.getCmp('maintree');
                var node = tree.getSelectionModel().getSelection()[0];
                var path = node.getPath('text','/').substring(2);
                if (path != "") {
                    folderPath = path;
                    Ext.MessageBox.confirm('Confirm', 'Are you sure you want to delete the selected folder and all its documents?', deleteFolderBoxHandler);
                }
            }
        },
        //Needed due to a focus lost in the editor popup when show. If any record has been selected, the focus is lost.
        afteritemexpand: function() {
            Ext.getCmp('maintree').getView().focus();
        }
   }   
});

var debug = false;
function getCollectionTree() {
    try {
        //DEBUG MODE, opens directly a json file instead of an xml file
        if (debug) {        
            $.getJSON("data/tree.json", function(json) {
                Ext.getCmp('maintree').getStore().setRootNode(json);
            });        
        }
        else {
            $.soap({
                url: '/cxf/DocumentManager/',
                method: 'getCollectionTree',
                namespaceQualifier: 'myns',
                namespaceURL: 'http://documentManagerPlugin.mathMS.iac3.eu/',
                data: {},
                success: function (soapResponse) {
                    try {
                        json = soapResponse.toXML().getElementsByTagNameNS('http://documentManagerPlugin.mathMS.iac3.eu/',"getCollectionTreeResponse")[0].childNodes[0].innerHTML;                    
                    } catch (e) {
                        showMessage("Documents tree load", message.ERROR_COLLECTION_TREE.replace("{0}", e), Ext.MessageBox.ERROR);
                        return null;
                    }
                    var dataJSON = jQuery.parseJSON(json);
                    var tree = Ext.getCmp('maintree');

                    tree.getStore().setRootNode(dataJSON);
                    tree.getStore().getRootNode().expand();
                    tree.getStore().sync();
                    //Focus root element
                    if (Manager.globals.vars.docPath == undefined || Manager.globals.vars.docPath == "") {
                        tree.getSelectionModel().select(tree.getRootNode());
                    }
                    else {
                        //Select the path
                        var node = tree.getStore().getRootNode();
                        var collections = Manager.globals.vars.docPath.split("/");
                        for (var i = 0; i < collections.length; i++) {
                            node = node.findChild("text", collections[i]);
                        }
                        tree.getSelectionModel().select(node);
                        tree.getView().focusRow(node);

                        //Load the document list and select it if a document is to be selected.
                        if (Manager.globals.vars.docId != '') {
                            var id = Manager.globals.vars.docId;
                            Manager.globals.vars.docId = '';
                            getDocumentList(false, id);
                        }
                        //If the id does not exist, just load the document list
                        else {
                            getDocumentList(false);
                        }
                    }
                },
                error: function (SOAPResponse) {
                    var msg = SOAPResponse.toXML().getElementsByTagName("faultstring")[0].childNodes[0].nodeValue;
                    showMessage("Documents tree load", message.ERROR_COLLECTION_TREE.replace("{0}", msg), Ext.MessageBox.ERROR);
                }
            });
        }

    } catch (e) {
        showMessage("Documents tree load", message.ERROR_COLLECTION_TREE.replace("{0}", e), Ext.MessageBox.ERROR);
    }
}

/**
 * Pop up to add a new folder
**/
Ext.define('Manager.view.NewFolder', {
        title: 'Adding new folder',
        extend: 'Ext.window.Window',
        layout: 'fit',
        modal: true,
        buttonAlign: 'center',
        closable: true,
        items: [
            {
                xtype: 'form',
                frame: false,
                border: 0,
                layout: {
                    type: 'hbox',
                    align: 'middle'
                },
                fieldDefaults: {
                    msgTarget: 'side'
                },
                items: [
                    {
                        xtype: 'container',
                        flex: 1,
                        padding: 10,
                        layout: {
                            type: 'vbox',
                            align: 'stretch'
                        },
                        items: [
                            {
                                id: 'newFolderName',
                                xtype: 'textfield',
                                name: 'newfolder',
                                fieldLabel: 'New folder name',
                                allowBlank: false,
                                enableKeyEvents: true,
                                flex: 1,
                                listeners: {
                                    keydown: function (obj, e, eOpts) {
                                        if (e.getKey() === e.ENTER) {
                                            e.stopEvent();
                                            addFolder(this, folderPath);
                                        }
                                    }
                                }
                            }
                        ]
                    }
                ]
            }
        ],
        buttons: [
            {
                text: 'Create',
                handler: function () {
                    addFolder(this, folderPath);
                }
            },
            {
                text: 'Cancel',
                handler: function () {
                    this.up('window').close();
                    //Return focus to the view
                    var tree = Ext.getCmp('maintree');
                    tree.getView().focus();
                }
            }
        ]
});

function addFolder(obj, folderPath) {
    var newFolderName = Ext.getCmp('newFolderName').getValue();
    addCollection(newFolderName, folderPath);
    obj.up('window').close();
}

/**
 * Popup to rename the folder
**/
Ext.define('Manager.view.RenameFolder', {
        title: 'Renaming the folder',
        extend: 'Ext.window.Window',
        layout: 'fit',
        modal: true,
        buttonAlign: 'center',
        closable: true,
        items: [
            {
                xtype: 'form',
                frame: false,
                border: 0,
                layout: {
                    type: 'hbox',
                    align: 'middle'
                },
                fieldDefaults: {
                    msgTarget: 'side'
                },
                items: [
                    {
                        xtype: 'container',
                        flex: 1,
                        padding: 10,
                        layout: {
                            type: 'vbox',
                            align: 'stretch'
                        },
                        items: [
                            {
                                id: 'newFolderRename',
                                xtype: 'textfield',
                                name: 'newfolder',
                                fieldLabel: 'New folder name',
                                allowBlank: false,
                                enableKeyEvents: true,
                                flex: 1,
                                listeners: {
                                    keydown: function (obj, e, eOpts) {
                                        if (e.getKey() === e.ENTER) {
                                            e.stopEvent();
                                            renameFolder(this, folderPath);
                                        }
                                    }
                                }
                            }
                        ]
                    }
                ]
            }
        ],
        buttons: [
            {
                text: 'Rename',
                handler: function () {
                    renameFolder(this, folderPath);
                }
            },
            {
                text: 'Cancel',
                handler: function () {
                    this.up('window').close();
                    //Return focus to the view
                    var tree = Ext.getCmp('maintree');
                    tree.getView().focus();
                }
            }
        ]
    });

function renameFolder(obj, folderPath) {
    var newFolderName = Ext.getCmp('newFolderRename').getValue();
    var oldFolderName = folderPath.substring(folderPath.lastIndexOf('/') + 1);
    folderPath = folderPath.substring(0, folderPath.lastIndexOf('/'));
    if (folderPath.substring(0, folderPath.length) === '/') folderPath = folderPath.substring(1);
    obj.up('window').close();
    renameCollection(oldFolderName, newFolderName, folderPath);
}

/**
 * Handler to delete the folder and documents
**/
function deleteFolderBoxHandler(btn) {
    if (btn == 'yes') {
        deleteCollection(folderPath);
    }
    else {
        //Return focus to the view
        var tree = Ext.getCmp('maintree');
        tree.getView().focus();
    }
}

var folderPath;
//Contextual menu
function contextualMenus( view, record, item, index, event ) {
    //Disable browser contextual menu
    event.stopEvent();

    folderPath = record.getPath('text','/').substring(2);
    if (folderPath != "") {
        folderPath = folderPath.substring(1);
    }
    Manager.globals.vars.docPath = folderPath;

    var contxtMenu = Ext.create('Ext.menu.Menu', {
                            floating: true,
                            renderTo: Ext.getBody(),  // usually rendered by it's containing component
                            listeners: {
                                destroy: function() {
                                    Ext.getCmp('maintree').getView().focus();
                                }
                            },
                            items: [{
                                text: 'Add new folder',
                                iconAlign: '',
                                icon: 'resources/images/plus_16x16.png', 
                                handler: function() {
                                    Ext.create('Manager.view.NewFolder').show();
                                    Ext.getCmp('newFolderName').focus();
                                }
                            }]
                        });

    //Root node cannot be deleted nor renamed
    if (record.get("text") != "/") {
        contxtMenu.add(
                  {
                        text: 'Rename folder',
                        iconAlign: '',
                        icon: 'resources/images/pen_alt2_16x16.png', 
                        handler: function() {
                            Ext.create('Manager.view.RenameFolder').show();
                            Ext.getCmp('newFolderRename').focus();
                            
                        }
                  },
                  {
                        text: 'Delete folder',
                        iconAlign: '',
                        icon: 'resources/images/x_alt_16x16.png', 
                        handler: function() {
                            Ext.MessageBox.confirm('Confirm', message.DELETE_FOLDER_CONFIRM, deleteFolderBoxHandler);
                        }
                  }
                );
    }
    
    contxtMenu.showAt(event.getXY());
}

/**
 * Get the document list from the collection selected
**/
var getDocumentsLaunch = false;;
function getDocuments( view, record, item, index, event) {
    //Store the path in a global variable, the selection in the tree panel gets lost when selecting the document in the grid panel 
    var path = record.getPath('text','/').substring(2);
    //Remove first slash if not root element
    if (path != "") {
        path = path.substring(1);
    }
    Manager.globals.vars.docPath = path;
    showProgressBar(message.LOADING);
    getDocumentsLaunch = true;
    getDocumentList(true);
}

/**
 * Task to be repeated every 1 minute to save the document
**/
var task = {
            run: reloadDocuments,
            interval: 6000
        }

Ext.TaskManager.start(task);

function reloadDocuments() {
    //Refresh only if the path is defined
    if (!Manager.globals.vars.serverRequestOn && !Manager.globals.vars.docListLoading && Manager.globals.vars.docPath != undefined && Manager.globals.vars.docPath != null && Manager.globals.vars.docPath != "null" && Manager.globals.vars.docPath != '') {
        getDocumentList();
    }
        /*  var tree = Ext.getCmp('maintree');
console.log(tree.getSelectionModel().getSelection()[0].get("text"));
    console.log(document.activeElement);*/
}


/**
 * Call to the SOAP web service to get the document list
**/
var currentRequest = null;  
function getDocumentList(reloadFilter, id) {
    Manager.globals.vars.docListLoading = true; 
    currentRequest = $.soap({
        url: '/cxf/DocumentManager/',
        method: 'getDocumentList',
        namespaceQualifier: 'myns',
        namespaceURL: 'http://documentManagerPlugin.mathMS.iac3.eu/',
        data: {
            arg0: Manager.globals.vars.docPath
        },
        beforeSend : function()    {
            console.log("call to  getDocumentList " + currentRequest);
            if(currentRequest != null) {
                currentRequest.abort();
            }
        },
        success: function (soapResponse) {
            currentRequest = null;
            try {
                if (soapResponse.toXML() != null) {
                    responseDocumentList = soapResponse.toXML().getElementsByTagNameNS('http://documentManagerPlugin.mathMS.iac3.eu/',"getDocumentListResponse")[0].childNodes[0].innerHTML;
                }
            } catch (e) {
                showMessage("Documents load", message.ERROR_DOCUMENTS.replace("{0}", e), Ext.MessageBox.ERROR);
                Manager.globals.vars.docListLoading = false;
                if (getDocumentsLaunch) {
                    Ext.MessageBox.hide();
                    getDocumentsLaunch = false;
                }
                return null;
            }
            var dataJSON = jQuery.parseJSON(responseDocumentList);

            //Proxy data refresh
            store = Ext.getCmp('xmldocumentslist').getStore();
            prox = store.proxy;
            //When the results only contains 1 element, it cannot be set directly, the proxy needs an array.
            if (dataJSON.results == undefined || dataJSON.results.length > 1) {
                prox.data = dataJSON.results;
            }
            else {
                array = [dataJSON.results];
                prox.data = array;
            }
            store.setProxy(prox);
            Ext.getCmp('xmldocumentslist').getStore().reload();
            if (reloadFilter!=undefined && reloadFilter) {
                reloadTypeFilter();
            }

            //If a document id is providen, it must be selected
            if (id != undefined) {
                var row = Ext.getCmp('xmldocumentslist').getStore().find("id", id);
                Ext.getCmp('xmldocumentslist').getSelectionModel().select(row);
            }
            Manager.globals.vars.docListLoading = false;
            if (getDocumentsLaunch) {
                Ext.MessageBox.hide();
                getDocumentsLaunch = false;
            }
            console.log("end  getDocumentList ");
        },
        error: function (SOAPResponse) {
            currentRequest = null;
            if (SOAPResponse.toXML() != null) {
                if (getDocumentsLaunch) {
                    Ext.MessageBox.hide();
                    getDocumentsLaunch = false;
                }
                Manager.globals.vars.docListLoading = false;
                var msg = SOAPResponse.toXML().getElementsByTagName("faultstring")[0].childNodes[0].nodeValue;
                showMessage("Document load", message.ERROR_DOCUMENTS.replace("{0}", msg), Ext.MessageBox.ERROR);
            }
        }
    });
}

function reloadTypeFilter() {
    //Reload the type filter
    var grid = Ext.getCmp('xmldocumentslist'),
        filterFeature = grid.filters,
        colFilter = filterFeature.getFilter('type');

    // If the filter has never been used, it won't be available            
    if (!colFilter) {
        filterFeature.view.headerCt.getMenu();
        colFilter = filterFeature.getFilter('type');
    }
    //Pretifying the values
    var options = grid.getStore().collect('type').sort();
    options.forEach(function(part, index, theArray) {
        theArray[index] = [theArray[index], capitaliseFirstLetter(theArray[index]).split(/(?=[A-Z][a-z])/).join(' ')];
    });

    // ok, we've got the ref, now let's try to recreate the menu
    colFilter.menu = colFilter.createMenu({
        options: options
    });
}

/**
 * Call to the SOAP web service to add a new collection
**/
function addCollection(colName, colPath) {
    Manager.globals.vars.serverRequestOn = true;
    console.log(colName + " " + colPath + ";");
    $.soap({
        url: '/cxf/DocumentManager/',
        method: 'addCollection',
        namespaceQualifier: 'myns',
        namespaceURL: 'http://documentManagerPlugin.mathMS.iac3.eu/',
        data: {
            arg0: colName,
            arg1: colPath
            },
        success: function (soapResponse) {
            Manager.globals.vars.serverRequestOn = false;
            var newCollection = {
                    text: colName
                };

            var tree = Ext.getCmp('maintree');
            var node = tree.getStore().getRootNode();
            if (Manager.globals.vars.docPath != "") {
                var collections = Manager.globals.vars.docPath.split("/");
                for (var i = 0; i < collections.length; i++) {
                    node = node.findChild("text", collections[i]);
                }
            }
            node.appendChild(newCollection);
            tree.store.sync();
            node.expand();
            newCollection = node.findChild("text", colName);
            tree.getSelectionModel().select(newCollection);
            tree.getView().focusRow(newCollection);



            Manager.globals.vars.docPath = colPath + "/" + colName;
            if (colPath == "") {
                Manager.globals.vars.docPath = Manager.globals.vars.docPath.substring(1);
            }

        },
        error: function (SOAPResponse) {
            Manager.globals.vars.serverRequestOn = false;
            var msg = SOAPResponse.toXML().getElementsByTagName("faultstring")[0].childNodes[0].nodeValue;
            showMessage("Document load", message.ERROR_ADD_COLLECTION.replace("{0}", msg), Ext.MessageBox.ERROR);
        }
    });
}

/**
 * Call to the SOAP web service to rename a collection
**/
function renameCollection(oldColName, newColName, colPath) {
    Manager.globals.vars.serverRequestOn = true;
    showProgressBar(message.RENAMING_COLLECTION);
    $.soap({
        url: '/cxf/DocumentManager/',
        method: 'renameCollection',
        namespaceQualifier: 'myns',
        namespaceURL: 'http://documentManagerPlugin.mathMS.iac3.eu/',
        data: {
            arg0: oldColName,
            arg1: newColName,
            arg2: colPath
            },
        success: function (soapResponse) {
            Manager.globals.vars.serverRequestOn = false;
            var tree = Ext.getCmp('maintree');
            var node = tree.getStore().getRootNode();
            var collections = Manager.globals.vars.docPath.split("/");
            for (var i = 0; i < collections.length; i++) {
                node = node.findChild("text", collections[i]);
            }
            node.set("text", newColName);
            tree.getView().focusRow(node);
            tree.store.sync();

            Manager.globals.vars.docPath = colPath + "/" + newColName;
            if (colPath == "") {
                Manager.globals.vars.docPath = Manager.globals.vars.docPath.substring(1);
            }
            Ext.MessageBox.hide();
        },
        error: function (SOAPResponse) {
            Manager.globals.vars.serverRequestOn = false;
            Ext.MessageBox.hide();
            var msg = SOAPResponse.toXML().getElementsByTagName("faultstring")[0].childNodes[0].nodeValue;
            showMessage("Document load", message.ERROR_RENAME_COLLECTION.replace("{0}", msg), Ext.MessageBox.ERROR);
        }
    });
}

/**
 * Call to the SOAP web service to delete a collection
**/
function deleteCollection(colPath) {
    Manager.globals.vars.serverRequestOn = true;
    $.soap({
        url: '/cxf/DocumentManager/',
        method: 'deleteCollection',
        namespaceQualifier: 'myns',
        namespaceURL: 'http://documentManagerPlugin.mathMS.iac3.eu/',
        data: {
            arg0: colPath
            },
        success: function (soapResponse) {
            Manager.globals.vars.serverRequestOn = false;
            var tree = Ext.getCmp('maintree');
            var node = tree.getStore().getRootNode();
            var collections = Manager.globals.vars.docPath.split("/");
            for (var i = 0; i < collections.length; i++) {
                node = node.findChild("text", collections[i]);
            }
            var parent = node.parentNode;
            tree.getView().focusRow(parent);
            node.remove();
            tree.store.sync();

            Manager.globals.vars.docPath = colPath.substring(0, colPath.lastIndexOf("/"));
        },
        error: function (SOAPResponse) {
            Manager.globals.vars.serverRequestOn = false;
            var msg = SOAPResponse.toXML().getElementsByTagName("faultstring")[0].childNodes[0].nodeValue;
            showMessage("Document load", message.ERROR_DELETE_COLLECTION.replace("{0}", msg), Ext.MessageBox.ERROR);
        }
    });
}

/**
 * Call to the SOAP web service to move a collection
**/
function moveCollection(colName, sourcePath, destinationPath) {
    Manager.globals.vars.serverRequestOn = true;
     $.soap({
        url: '/cxf/DocumentManager/',
        method: 'moveCollection',
        namespaceQualifier: 'myns',
        namespaceURL: 'http://documentManagerPlugin.mathMS.iac3.eu/',
        data: {
            arg0: colName,
            arg1: sourcePath,
            arg2: destinationPath
        },
        success: function (soapResponse) {
            Manager.globals.vars.serverRequestOn = false;
            var tree = Ext.getCmp('maintree');
            var path = destinationPath.substring(2) + "/" + colName;
            //Remove first slash if not root element
            if (path != "") {
                path = path.substring(1);
            }
            Manager.globals.vars.docPath = path;
            console.log("Moved successfully");
        },
        error: function (SOAPResponse) {
            Manager.globals.vars.serverRequestOn = false;
            var msg = SOAPResponse.toXML().getElementsByTagName("faultstring")[0].childNodes[0].nodeValue;
            showMessage("Folder movement", message.ERROR_MOVE_COLLECTION.replace("{0}", msg), Ext.MessageBox.ERROR);
            node = false;
        }
    });    
}

/**
 * Call to the SOAP web service to move a collection
**/
function moveDocument(docId, destinationPath) {
    Manager.globals.vars.serverRequestOn = true;
     $.soap({
        url: '/cxf/DocumentManager/',
        method: 'moveDocument',
        namespaceQualifier: 'myns',
        namespaceURL: 'http://documentManagerPlugin.mathMS.iac3.eu/',
        data: {
            arg0: docId,
            arg1: destinationPath
        },
        success: function (soapResponse) {
            documentMovementRemain = documentMovementRemain - 1;
            console.log("documentMovementRemain " + documentMovementRemain);
            if (documentMovementRemain == 0) {
                Manager.globals.vars.serverRequestOn = false;
                Ext.MessageBox.hide();
                showProgressBar(message.LOADING);
                getDocumentsLaunch = true;
                getDocumentList(true);
                //Recover the focus on the tree
                var tree = Ext.getCmp('maintree');
                var selected = tree.getSelectionModel().getSelection()[0];
                tree.getView().focusRow(selected);
            }
        },
        error: function (SOAPResponse) {
            documentMovementRemain = documentMovementRemain - 1;
            console.log("documentMovementRemain en error " + documentMovementRemain);
            if (documentMovementRemain == 0) {
                Ext.MessageBox.hide();
                Manager.globals.vars.serverRequestOn = false;
            }
            var msg = SOAPResponse.toXML().getElementsByTagName("faultstring")[0].childNodes[0].nodeValue;
            showMessage("Document movement", message.ERROR_MOVE_DOCUMENT.replace("{0}", msg), Ext.MessageBox.ERROR);
            node = false;
        }
    });    
}

function openDisclaimer() {
   var msgBox = new Ext.window.MessageBox();
    msgBox.autoShow=true;
    msgBox.autoScroll=true;
    msgBox.overflowY='auto';
    msgBox.resizable = true;
    msgBox.show({
       title: "Disclaimer",
       msg: message.DISCLAIMER,
       buttons: Ext.MessageBox.OK,
       icon: Ext.MessageBox.INFO
    });
}

//auxiliary function to show messages to the user.
//Used in the document manager and also the editor
function showMessage(t, m, i) {
    var msgBox = new Ext.window.MessageBox();
    msgBox.autoShow=true;
    msgBox.autoScroll=true;
    msgBox.overflowY='auto';
    msgBox.resizable = true;
    msgBox.show({
       title: t,
       msg: m,
       buttons: Ext.MessageBox.OK,
       icon: i,
       fn: function(buttonId) {
                var tree = Ext.getCmp('maintreeEditor');
                if (tree) {
                    tree.getView().focus();
                }
            }
    });
}

function showProgressBar(t) {
    Ext.MessageBox.show({
        msg : t,
        progressText : 'Please wait...',
        width : 300,
        wait:true,
        waitConfig: {interval:200}
    });
}


