Ext.define('Manager.view.DependenciesPopup', {
    extend: 'Ext.Window',
    alias: 'widget.dependenciespopup',
    id: 'dependenciespopup',
    floating: true,
    centered: true,
    title: 'Document dependencies',
    modal: true,
    bodyPadding: 10,
    layout: {
            type: 'vbox',
            align: 'stretch'
    },
    buttonAlign: 'center',
    buttons: [
        { 
            text: 'Ok',
            handler: function() {
                Ext.getCmp('dependenciespopup').close();
            } 
        }
    ],
    closable: true,
    resizable: true,
    closeAction: 'hide',
    items: [{
        id : 'panelList',
        xtype: 'panel',
        border: false,
        layout: {
                type: 'hbox',
                align: 'stretch'
        },
        items: [{
            id : 'docNames',
            xtype: 'panel',
            border: false,
            header: false,
            layout: {
                    type: 'vbox',
                    align: 'stretch'
            }
        }, {
            id : 'docCols',
            xtype: 'panel',
            border: false,
            header: false,
            layout: {
                    type: 'vbox',
                    align: 'stretch'
            }
        }]
    }]
});

var dependenciesPopup = Ext.create('Manager.view.DependenciesPopup', {});

/*
 * Opens the editor
 */
var currentRecord;
function openDependenciesPopup(dependencies) {
    //If no dependency
    if (dependencies.length == 0) {
        showMessage("Document dependencies", message.NO_DOCUMENT_DEPENDENCIES, Ext.MessageBox.INFO); 
    }
    else {
        var panelNames = Ext.getCmp("docNames");
        var panelCols = Ext.getCmp("docCols");
        //Remove previous items
        panelNames.removeAll();
        panelCols.removeAll();
        //Add the dependecy buttons
        for(var i = 0; i < dependencies.length; i++) {
            var dependency = dependencies[i];
            console.log(dependency);
            var nameButton = Ext.create('Ext.Button', {
                xtype: 'button',
                text: dependency.name,
                value: dependency.id,
                cls: 'botonSimml',
                textAlign: 'left',
                handler: editDependency
            });
            var colButton = Ext.create('Ext.Button', {
                xtype: 'button',
                text: dependency.collection,
                value: dependency.id,
                cls: 'botonSimml',
                textAlign: 'left',
                handler: goToDependency
            })

            panelNames.add(nameButton);
            panelCols.add(colButton);
        }
        dependenciesPopup.show();
    }
}

function editDependency(obj) {
    //Store the cookie with the selected document id
    Ext.util.Cookies.set('xmlEditor.documentID', obj.value);
    Ext.util.Cookies.set('xmlEditor.documentType', "notX3d");
    $.soap({
        url: '/cxf/DocumentManager/',
        method: 'getDocumentCollection',
           namespaceQualifier: 'myns',
            namespaceURL: 'http://documentManagerPlugin.mathMS.iac3.eu/',

        data: {
            arg0: obj.value
        },

        success: function (soapResponse) {
            try {
                data = soapResponse.toXML().getElementsByTagNameNS('http://documentManagerPlugin.mathMS.iac3.eu/',"getDocumentCollectionResponse")[0].childNodes[0].innerHTML;
                Ext.util.Cookies.set("xmlEditor.documentPath", data);
                var win = window.open("xmlEditor/index.html", '_blank');
                win.focus();
            } catch (e) {
                showMessage("Document path", message.ERROR_DOCUMENT_PATH.replace("{0}", e), Ext.MessageBox.ERROR);
                return null;
            }
        },
        error: function (SOAPResponse) {
            var msg = SOAPResponse.toXML().getElementsByTagName("faultstring")[0].childNodes[0].nodeValue;
            showMessage("Document path", message.ERROR_DOCUMENT_PATH.replace("{0}", msg), Ext.MessageBox.ERROR);
        }
    });
}



function goToDependency(obj) {
    //Clear sessionStorage or the new window will inherit the session
    window.sessionStorage.clear();
    Ext.util.Cookies.set("docManager.documentID", obj.value);
    Ext.util.Cookies.set("docManager.documentPath", obj.text.substring(1));
console.log("Set in cookies ", obj.value, obj.text.substring(1));
    var win = window.open("index.html", '_blank');
    win.focus();
    //Restore the session to allow current page refresh
    window.sessionStorage.setItem('docManager.documentID', Manager.globals.vars.docId);
    window.sessionStorage.setItem('docManager.documentPath', Manager.globals.vars.docPath);
}
