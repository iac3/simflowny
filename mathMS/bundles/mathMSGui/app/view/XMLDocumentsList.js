//Template renderer
function templateRenderer(template) {
    return function(value, meta, record, rowIndex, colIndex, store) {
        return template.applyTemplate(record.data);
    };
} 

Ext.define('Manager.view.XMLDocumentsList', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.xmldocumentslist',
    title: 'Documents',
    store: {
        extend: 'Ext.data.Store',
        requires: 'Manager.model.XMLDocuments',    
        model: 'Manager.model.XMLDocuments',
        sorters: ['name', 'version'],
        autoLoad: false,
           proxy: {
            type: 'memory',
            reader: {
                type: 'json',
                root: 'results'
            }
        }
    },
    hideHeaders: false,
    selModel: {
            selType: 'rowmodel',
            mode: 'MULTI',
            listeners : {           
                    selectionchange: function( ) {
                        var xmlList = Ext.getCmp('xmldocumentslist');
                        var xmlModel = xmlList.getSelectionModel();
                        var lastActive = Ext.Element.getActiveElement();
                        //ocultamos todas las opciones de menu
                        hideOptions();
                        //un documento seleccionado
                        if (xmlModel.getCount() == 1) {
                            var selected = xmlModel.getSelection();
                            var docType = selected[0].get('type');
                            //visualizamos los menus comunes a todos los tipos
                            var menuOption = Ext.getCmp('deleteMenu');
                            menuOption.show();
                            menuOption = Ext.getCmp('editMenu');
                            menuOption.show();
                            menuOption = Ext.getCmp('duplicateMenu');
                            menuOption.show();
                            menuOption = Ext.getCmp('downloadMenu');
                            menuOption.show();
                            //Special action menus
                            for (var i in Manager.globals.vars.specialActions[docType]) {
                                var method = Manager.globals.vars.specialActions[docType][i];
                                var specialActionOption = Ext.getCmp(method.method + "SpecialAction");
                                specialActionOption.show();
                            }
                        }
                        else if (xmlModel.getCount() > 1){
                            var menuOption = Ext.getCmp('deleteMenu');
                            menuOption.show();
                            menuOption = Ext.getCmp('downloadMenu');
                            menuOption.show();
                        }
                        lastActive.focus(); 
                    }   
                }
    },
    viewConfig:{
        markDirty:false,
        autoScroll: true,
        preserveScrollOnRefresh: true,
        plugins: {
                ddGroup: 'docManagerDD',
                ptype: 'gridviewdragdrop',
                enableDrop: false,
                enableDrag: true
            }
    },
    requires:['Ext.ux.grid.FiltersFeature'],
    features: [{
        ftype: 'filters',
        encode: false,
        local: true,
        filters: [{
            type: 'string',
            dataIndex:'name'
        }, 
        {
            type: 'string',
            dataIndex:'author'
        }, 
        {
            type: 'string',
            dataIndex:'version'
        }, 
        {
            type: 'string',
            dataIndex:'Description'
        }, 
        {
            type: 'date',
            dataIndex:'date'
        },
        {
            type: 'list',
            dataIndex:'type'
        }]
    }],
    enableDragDrop   : true,
    id: 'documentsList',
    initComponent: function() {
        //Images path is different if the Panel comes from the Editor Pop-up
        var imgPath = "";
        if (this.xtype == "popupxmldocumentslist") {
            Ext.TaskManager.stop(task);
            imgPath = "../";
        }
        this.columns = [
            {header: '', dataIndex: 'id', width: 40, 
                renderer: function(value) {
                    if (localStorage.getItem(value) !== null) {
                        return '<img src="resources/images/lock.png" height="20" width="20" />';
                    }
                    return '';
                }
            },
            {header: 'Name', dataIndex: 'name', flex: 1, field: 'textfield', hideable: false,
                renderer: templateRenderer(new Ext.Template('<img src="' + imgPath + 'resources/images/document_{type}.png" height="20" width="20" /> <span>{name}</span>'))
            },
            {header: 'Author', dataIndex: 'author', flex: 1, field: 'textfield', 
                renderer: templateRenderer(new Ext.Template('<span>{author}</span>'))
            },
            {header: 'Date', dataIndex: 'date', flex: 1, 
                renderer: Ext.util.Format.dateRenderer('Y-m-d\\TH:i:s')
            },
            {header: 'Version', dataIndex: 'version', flex: 1, 
                renderer: templateRenderer(new Ext.Template('<span>{version}</span>'))
            },
            {header: 'Type', dataIndex: 'type', flex: 1,
                renderer: function(value) {
                    var name = capitaliseFirstLetter(value).replace(/([A-Z]*)([A-Z])(?![A-Z])/g, " $1 $2");
                    return Ext.String.format('<span>{0}</span>', name);
                }
            },
            {header: 'Description', dataIndex: 'Description', flex: 1, field: 'textfield', 
                renderer: templateRenderer(new Ext.Template('<span>{Description}</span>'))
            }
        ];
        this.callParent();
    },
    listeners: 
    {
        itemcontextmenu: contextualMenus,
        itemdblclick: function(obj, record) {
            selectDocument(this, obj, record);
        },
        cellkeydown: function(obj, td, cellIndex, record, tr, rowIndex, e, eOpts) {
            if (e.getKey() === e.ENTER) {
                selectDocument(this, obj, record);
            }
            if (e.getKey() === e.TAB) {
                //Navigate forwards
                if (!e.shiftKey) {
                    e.stopEvent();
                    if (this.xtype == "xmldocumentslist") {
                        var plus = Ext.getCmp('plusMenu');
                        plus.focus();
                    }
                }
                //Navigate backwards
                else{
                    e.stopEvent();
                    var tree = Ext.getCmp('maintree');
                    tree.getView().focus();
                }
            }
            //Open context menu as an alternative to menu key, that does not work in sikuli
            if (e.getKey() === e.F7) {
                e.stopEvent();
                obj.fireEvent('itemcontextmenu', obj, record, tr, cellIndex, e);
            }
        }
    }
});

function selectDocument(view, dv, record) {
        //From Editor's popup
        if (view.xtype == "popupxmldocumentslist") {
            Ext.util.Cookies.set("xmlEditor.popup", record.get("id"));
            //Close pop-up window
            dv.ownerCt.ownerCt.ownerCt.close();
            var xmlList = Ext.getCmp('maintreeEditor');
            xmlList.getView().focus();
        }
        // Normal behaviour from Manager
        else {
            console.log(record.get("id"));
            Ext.util.Cookies.set("xmlEditor.documentID", record.get("id"));
            Ext.util.Cookies.set('xmlEditor.documentType', record.get("type"));
            Ext.util.Cookies.set("xmlEditor.documentPath", Manager.globals.vars.docPath);
            var win = window.open("xmlEditor/index.html", '_blank');
            win.focus();
        }
}

function performSpecialAction(b, e, eOpts) {
    //Get special action parameters
    var action = b.id.substring(0, b.id.indexOf("SpecialAction"));
    var docType = Ext.getCmp('xmldocumentslist').getSelectionModel().getSelection()[0].get('type');
    var specialActions = Manager.globals.vars.specialActions[docType];
    var callParameters;
    $.each(specialActions, function(i, v) {
        if (v.method == action) {
            callParameters = v;
        }
    });
    Manager.globals.vars.serverRequestOn = true;
    //Load document to be used as the argument for the web service
    $.soap({
        url: '/cxf/DocumentManager/',
        method: 'getDocument',
        namespaceQualifier: 'myns',
        namespaceURL: 'http://documentManagerPlugin.mathMS.iac3.eu/',
        data: {
            arg0: Ext.getCmp('xmldocumentslist').getSelectionModel().getSelection()[0].get('id')
        },
        success: function (soapResponse) {
            try {
                //MessageBox as ProgressBar
                showProgressBar(message.SPECIAL_ACTION.replace("{0}", callParameters.method));

                json = soapResponse.toXML().getElementsByTagNameNS('http://documentManagerPlugin.mathMS.iac3.eu/',"getDocumentResponse")[0].childNodes[0].innerHTML;
                //Convert scaped special characters &lt; to <
                json = $("<span />", { html: json }).text();
                //Call web service using the parameters
                $.soap({
                    url: '/cxf/' + callParameters.url,
                    method: callParameters.method,
                    namespaceQualifier: callParameters.namespaceQualifier,
                    namespaceURL: callParameters.namespaceURL,
                    data: {
                        arg0: json
                    },
                    success: function (soapResponse) {
                        try {
                            var returnMethod = callParameters.returns[0];
                            if (returnMethod.type == 'document') {
                                var response = soapResponse.toXML().getElementsByTagNameNS(callParameters.namespaceURL,callParameters.method + "Response")[0].childNodes[0].innerHTML;
                                //Save document
                                $.soap({
                                    url: '/cxf/DocumentManager/',
                                    method: 'saveDocument',
                                       namespaceQualifier: 'myns',
                                    namespaceURL: 'http://documentManagerPlugin.mathMS.iac3.eu/',
                                    data: {
                                        arg0: response,
                                        arg1: Manager.globals.vars.docPath
                                    },
                                    success: function (soapResponse) {
                                        Manager.globals.vars.serverRequestOn = false;
                                        //Refresh the document list
                                        getDocumentList(true);
                                        Ext.MessageBox.hide();
                                    },
                                    error: function (SOAPResponse) {
                                        Manager.globals.vars.serverRequestOn = false;
                                        Ext.MessageBox.hide();
                                        var msg = SOAPResponse.toXML().getElementsByTagName("faultstring")[0].childNodes[0].nodeValue;
                                        showMessage("Save document", message.ERROR_SAVE.replace("{0}", msg), Ext.MessageBox.ERROR);
                                    }
                                });
                            }
                            if (returnMethod.type == 'file') {
                                var response = soapResponse.toXML().getElementsByTagNameNS(callParameters.namespaceURL,callParameters.method + "Response")[0].childNodes[0].innerHTML;
                                Manager.globals.vars.serverRequestOn = false;
                                Ext.MessageBox.hide();
                                //Determine the mime type
                                var extension = returnMethod.extension;
                                if (extension == "tex") {
                                    var mime = "application/x-tex";
                                }
                                if (extension == "yaml") {
                                    var mime = "application/x-yaml";
                                }
                                //Download the file
                                var dataUri="data:" + mime + ";base64," + response;
                                downloadFile(dataUri, "download." + extension);
                            }
                            if (returnMethod.type == 'none') {
                                Manager.globals.vars.serverRequestOn = false;
                                Ext.MessageBox.hide();
                            }
                        } catch (e) {
                            Manager.globals.vars.serverRequestOn = false;
                            Ext.MessageBox.hide();
                            showMessage("Special action " + callParameters.method, message.ERROR_SPECIAL_ACTION.replace("{0}", e).replace("{1}", callParameters.method), Ext.MessageBox.ERROR);
                            return null;
                        }
                    },
                    error: function (SOAPResponse) {
                        Manager.globals.vars.serverRequestOn = false;
                        Ext.MessageBox.hide();
                        var msg = SOAPResponse.toXML().getElementsByTagName("faultstring")[0].childNodes[0].nodeValue;
                        showMessage("Special action " + callParameters.method, message.ERROR_SPECIAL_ACTION.replace("{0}", msg).replace("{1}", callParameters.method), Ext.MessageBox.ERROR);
                    }
                });

            } catch (e) {
                Manager.globals.vars.serverRequestOn = false;
                showMessage("Special action " + callParameters.method, message.ERROR_SPECIAL_ACTION.replace("{0}", e).replace("{1}", callParameters.method), Ext.MessageBox.ERROR);
                return null;
            }
        },
        error: function (SOAPResponse) {
            Manager.globals.vars.serverRequestOn = false;
            var msg = SOAPResponse.toXML().getElementsByTagName("faultstring")[0].childNodes[0].nodeValue;
            showMessage("Special action " + callParameters.method, message.ERROR_SPECIAL_ACTION.replace("{0}", msg).replace("{1}", callParameters.method), Ext.MessageBox.ERROR);
        }
    });
}

/*
 * Creates and shows the contextual menu for document list
 */
function contextualMenus(view, record, item, index, event) {
    //Disable browser contextual menu
    event.stopEvent();

    var menu = new Ext.menu.Menu();
    menu.on({
        hide: function() {
            Ext.getCmp('xmldocumentslist').getView().focus();
        },
        scope: this // Important. Ensure "this" is correct during handler execution
    });

    //Add common actions
    menu.add(
                    {
                        text: 'Edit document',
                        iconAlign: '',
                        icon: 'resources/images/pen_alt2_16x16.png', 
                        handler: edit
                    },
                    {
                        text: 'Duplicate document',
                        iconAlign: '',
                        icon: 'resources/images/duplicate_16x16.png', 
                        handler: duplicate
                    },
                    {
                        text: 'Download document',
                        iconAlign: '',
                        icon: 'resources/images/download_16x16.png', 
                        handler: hadleDownloadDocument
                    },
                    {
                        text: 'Delete document',
                        iconAlign: '',
                        icon: 'resources/images/x_alt_16x16.png', 
                        handler: deleteDocuments
                    },
                    {
                        text: 'List of usage',
                        iconAlign: '',
                        icon: 'resources/images/info_16x16.png', 
                        handler: dependencyDocuments
                    }
              );

    //Lock-unlock document
    var docId = record.get('id');
    console.log(localStorage.getItem(docId));
    if (localStorage.getItem(docId) === null) {
        menu.add(
                    {
                        text: 'Lock document',
                        iconAlign: '',
                        icon: 'resources/images/lock_16x16.png', 
                        handler: lockDocument
                    });
    }
    else {
        menu.add(
                    {
                        text: 'Unlock document',
                        iconAlign: '',
                        icon: 'resources/images/unlock_16x16.png', 
                        handler: unlockDocument
                    });
    }

    //Special action menus
    var docType = record.get('type');
    for (var i in Manager.globals.vars.specialActions[docType]) {
        var specialAction = Manager.globals.vars.specialActions[docType][i];
        //Menu items already created at loading time in app.js
        menu.add(Ext.getCmp(specialAction.method + "SpecialActionContextual"));
    }

    //Show contextual menu
    menu.showAt(event.getXY());
}

/**
 * Locks a document by adding its id to the local storage
 */
function lockDocument() {
    var id = Ext.getCmp('xmldocumentslist').getSelectionModel().getSelection()[0].get('id');
    localStorage.setItem(id, "1");
}

/**
 * Unlocks a document by removing its id to the local storage
 */
function unlockDocument() {
    var id = Ext.getCmp('xmldocumentslist').getSelectionModel().getSelection()[0].get('id');
    localStorage.removeItem(id);
}

function dependencyDocuments() {
    var id = Ext.getCmp('xmldocumentslist').getSelectionModel().getSelection()[0].get('id');
    Manager.globals.vars.serverRequestOn = true;
    $.soap({
        url: '/cxf/DocumentManager/',
        method: 'getParentDependencies',
        namespaceQualifier: 'myns',
        namespaceURL: 'http://documentManagerPlugin.mathMS.iac3.eu/',
        data: {
            arg0: id
        },
        success: function (soapResponse) {
            Manager.globals.vars.serverRequestOn = false;
            try {
                var xmldoc = soapResponse.toXML();
                xmldoc.normalize();
                var json = xmldoc.getElementsByTagNameNS('http://documentManagerPlugin.mathMS.iac3.eu/',"getParentDependenciesResponse")[0].childNodes[0].childNodes[0].nodeValue;
                var dataJSON = jQuery.parseJSON(json);
                openDependenciesPopup(dataJSON);

            } catch (e) {
                showMessage("Document dependencies", message.ERROR_DEPENDENCIES.replace("{0}", e), Ext.MessageBox.ERROR);
                return null;
            }
        },
        error: function (SOAPResponse) {
            Manager.globals.vars.serverRequestOn = false;
            var msg = SOAPResponse.toXML().getElementsByTagName("faultstring")[0].childNodes[0].nodeValue;
            showMessage("Document dependencies", message.ERROR_DEPENDENCIES.replace("{0}", msg), Ext.MessageBox.ERROR);
        }
    });
}


function capitaliseFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}


