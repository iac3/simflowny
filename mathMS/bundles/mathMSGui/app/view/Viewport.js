Ext.define('Manager.view.Viewport', {
    extend: 'Ext.container.Viewport',
    layout: 'fit',
    
    requires: [
        'Manager.view.XMLDocumentsList',
        'Manager.view.MainTree',
        'Manager.view.DependenciesPopup',
        'Manager.view.MainToolbar',
        'Manager.globals.vars'
    ],
    
    initComponent: function() {	
        this.items = {
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            items: [{
                xtype: 'panel',
                id: 'north',
                layout: {
                    type: 'hbox',
                    pack: 'center',
                    align: 'stretch'
                },
                items: [{
                    xtype: 'component',
                    width: 250,
                    id: 'logo',
                    html: '<img src="resources/images/logo.png" height="50" width="188" />'
                },
                {
			        xtype: 'container',
                    flex: 1,
                    layout: {
				        type: 'hbox',
				        align: 'stretch'
			        },
			        items: [{
				        xtype: 'container',
				        padding: 4,
				        height: 50,
				        flex: 1,
				        layout: {
					        type: 'hbox',
					        align: 'left'
				        },
				        items: [{
					        xtype: 'maintoolbar'
				        }]
			        }]
                }]
            },
            {
                xtype: 'container',
                id: 'south',
                flex: 1,
                layout: {
                    type: 'hbox',
                    align: 'stretch'
                },
                items: [{
                    xtype: 'container',
                    id: 'tree',
					flex: 1,
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    items: [{
                        xtype: 'maintree',
                        flex: 1,
                        id: 'maintree'
                    }]
                },
                {
					xtype: 'xmldocumentslist',
					id: 'xmldocumentslist',
                    width: 4 * window.innerWidth/5,
                    resizable:true,
                    resizeHandles: 'w'
				}]
            }]
        };
        this.callParent();
    }
});
