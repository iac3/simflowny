#!/bin/bash
# Create an empty file tree in file manager


# Uncompress the folder structure
tar -xvzf ./data/db.tar.gz

# Delete previous file tree
rm -rf ../simflowny_abm/db/*

# move new file tree to final destination
mv db/* ../simflowny_abm/db/

# Cleaning up
rm -Rf db

