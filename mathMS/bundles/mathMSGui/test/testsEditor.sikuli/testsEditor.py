import SimflownyLib
reload(SimflownyLib)
from SimflownyLib import *

def editorTests():
    click("1452595169364.png")
    sleep(0.5) 
    type(Key.TAB)
    sleep(0.5) 
    type(Key.DOWN+Key.ENTER)
    waitVanish("1452756706858.png")
    
    wait("1452595645846.png")
    sleep(1)
    
    expandAllTest()
    collapseAllTest()
    navigationTest()
    textEditionTest()
    numericEditionTest()
    dateEditionTest()
    referenceDataEditionTest()
    listEditionTest()
    documentIdEditionTest()
    mathMLEditionTest()
    addNodeTest()
    deleteNodeTest()
    copyPasteTest()
    reorderNodeTest()
    validateTest()
    leftRightNavigationTest()
    rightLeftNavigationTest()

def expandAllTest():
    wait("1452595682661.png", 120)
    
    click("1452595682661.png")
    sleep(5)
    if exists("1452599138783.png"):
        print "expand all Test OK"
    else:
        print "expand all Test FAILURE"
        
def collapseAllTest():
    click("1452599307300.png")
    sleep(1)
    if exists("1452599138783.png"):
        print "expand all Test FAILURE"
    else:
        print "expand all Test OK"

def navigationTest():
    testOk = True
    expandNode()
    sleep(0.5)
    if not exists("1452600676590.png"):
        print "navigation Test FAILURE: cannot expand node"
        testOk = False
    moveTreeDown(3)
    expandNode()
    sleep(0.5)
    if not exists("1452601449769.png"):
        print "navigation Test FAILURE: wrong descent on the tree"
        testOk = False
    collapseNode()
    if exists("1452601449769.png"):
        print "navigation Test FAILURE: cannot collapse node"
        testOk = False
    moveTreeUp(2)
    expandNode()
    sleep(0.5)
    if not exists("1452601435346.png"):
        print "navigation Test FAILURE: wrong ascension on the tree"
        testOk = False
    if testOk:
        print "navigation Test OK"
        
def textEditionTest():
    moveTreeDown(1)
    modifyValue("Nuevo nombre")
    sleep(0.5)
    if exists(Pattern("1452601471649.png").similar(0.90)):
        print "text edition Test OK"
    else:
        print "text edition Test FAILURE"

def numericEditionTest():
    collapseNode()
    collapseNode()
    moveTreeDown(16)
    expandNode()
    moveTreeDown(2)
    type(Key.INSERT)
    click(Pattern("1452606851416.png").targetOffset(2,-3))
    type(Key.ENTER)
    sleep(0.5)
    if exists(Pattern("1452606923247.png").similar(0.85)):
        print "numeric edition Test OK"
    else:
        print "numeric edition Test FAILURE"

def dateEditionTest():
    collapseNode()
    collapseNode()
    moveTreeUp(16)
    expandNode()
    moveTreeDown(5)
    type(Key.INSERT)
    sleep(0.2)
    click("1452607622798.png")
    sleep(0.8)
    wait("1452761758259.png")
    type(Key.DOWN)
    sleep(0.5)
    type(Key.DOWN)
    sleep(1)
    type(Key.ENTER)
    sleep(0.5)
    type(Key.ENTER)
    sleep(0.5)
    if exists(Pattern("1452608020207.png").similar(0.90)):
        print "date edition Test OK"
    else:
        print "date edition Test FAILURE"

def referenceDataEditionTest():
    collapseNode()
    collapseNode()
    moveTreeDown(4)
    expandNode()
    moveTreeDown(1)
    expandNode()
    moveTreeDown(3)
    expandNode()
    moveTreeDown(1)
    type(Key.INSERT)
    click("1452610727054.png")
    sleep(1)
    if not exists("1452610769127.png"):
        print "reference data edition Test FAILURE: empty list"
    else:
        selectOption(1)
        sleep(0.5)
        if exists(Pattern("1452610974090.png").similar(0.95)):
            print "reference data edition Test OK"
        else:
            print "reference data edition Test FAILURE"

def listEditionTest():
    collapseNode()
    collapseNode()
    collapseNode()
    collapseNode()
    collapseNode()
    collapseNode()
    moveTreeDown(1)
    expandNode()
    moveTreeDown(1)
    expandNode()
    moveTreeDown(2)
    type(Key.INSERT)
    sleep(0.2)
    click("1452610727054.png")
    sleep(1)
    selectOption(2)
    sleep(1)
    if exists("1452684414451.png"): 
        print "list edition Test OK"
    else:
        print "list edition Test FAILURE"

def documentIdEditionTest():
    collapseNode()
    collapseNode()
    collapseNode()
    collapseNode()
    moveTreeDown(5)
    expandNode()
    moveTreeDown(2)
    expandNode()
    moveTreeDown(3)
    expandNode()
    moveTreeDown(1)
    expandNode()
    moveTreeDown(2)
    modifyUUID(24, 0)
    sleep(1)
    if exists("1452684888676.png"):
        print "documentID edition Test OK"
    else:
        print "documentID edition Test FAILURE"

def mathMLEditionTest():
    #Set up
    collapseNode()
    collapseNode()
    collapseNode()
    collapseNode()
    moveTreeDown(1)
    expandNode()
    moveTreeDown(1)
    expandNode()
    moveTreeDown(1)
    expandNode()
    moveTreeDown(1)
    expandNode()
    moveTreeDown(1)
    expandNode()
    moveTreeDown(1)
    expandNode()
    moveTreeDown(1)
    #Wrong expression
    modifyValue("a>>")
    sleep(1)
    if not exists("1452685453115.png"):
        print "mathML edition Test FAILURE: an incorrect expression has been accepted"
    else:
        click("1452685509635.png")
        sleep(0.5)
        
    #Correct expression
    modifyValue("a+b=c")
    sleep(1)
    if exists("1452686022889.png"):
        print "mathML edition Test OK"
    else:
        print "mathML edition Test FAILURE: a correct expression has not been accepted"

def addNodeTest():
    moveTreeUp(1)
    openContextualMenu()
    sleep(0.5)
    selectOption(9)
    sleep(0.5)
    if exists("1452686555153.png"):
        print "add node Test OK"
    else:
        print "add node Test FAILURE"

def deleteNodeTest():
    passed = True
    #Delete leaf node
    openContextualMenu()
    sleep(0.5)
    selectOption(3)
    waitVanish("1452762550078.png")
    
    sleep(2)
    if exists("1452686555153.png"):
        passed = False
        print "delete node Test FAILURE: cannot delete a leaf node"
    #Delete parent node
    moveTreeUp(1)
    openContextualMenu()
    sleep(0.5)
    selectOption(1)
    sleep(1)
    openContextualMenu()
    sleep(0.5)
    selectOption(3)
    waitVanish("1452762591733.png")
    
    sleep(2)
    if exists(Pattern("1452686885772.png").similar(0.80)):
        passed = False
        print "delete node Test FAILURE: cannot delete a parent node"
    #Delete mandatory node
    moveTreeUp(2)
    openContextualMenu()
    sleep(0.5)
    selectOption(1)
    waitVanish("1452762626677.png")
    
    sleep(2)
    if exists("1452687038625.png"):
        passed = False
        print "delete node Test FAILURE: cannot delete a mandatory node"
    if passed:
        passed = False
        print "delete node Test OK"

def copyPasteTest():
    passed=True
    openContextualMenu()
    wait("1452764367506.png")
    sleep(1)
    selectOption(19)
    sleep(1)
    modifyValue("a+b=c")
    sleep(1)
    openContextualMenu()
    sleep(1)
    wait("1452764339643.png")
    
    sleep(1)
    selectOption(1)
    sleep(1)
    openContextualMenu()
    if exists("1452687959164.png"):
        passed = False
        print "copy paste Test FAILURE: it is allowed to paste on a node that does not permit doing so"
    type(Key.ESC)
    sleep(0.5)
    moveTreeUp(1)
    openContextualMenu()
    wait("1452764367506.png")
    
    sleep(1)
    selectOption(31)
    sleep(3)
    if not exists("1452693467682.png"):
        passed = False
        print "copy paste Test FAILURE: cannot paste node"
    if passed:
        print "copy paste Test OK"

def reorderNodeTest():
    modifyValue("k/2*sin(3)")
    sleep(1)
    openContextualMenu()
    wait("1452762756650.png")
    sleep(1)
    selectOption(1)
    waitVanish("1452762756650.png")
    
    sleep(0.5)
    if not exists(Pattern("1452695519002-1.png").similar(0.80)):
        print "reorder Test FAILURE: not possible to move upwards"
    else:
        openContextualMenu()
        wait("1452762789346.png")
        sleep(1)
        selectOption(1)
        waitVanish("1452762789346.png")
        
        sleep(0.5)
        if exists(Pattern("1452695519002-1.png").similar(0.80)):
            print "reorder Test FAILURE: not possible to move downwards"
        else:
            print "reorder Test OK"

def validateTest():
    click(Pattern("1452698191384.png").similar(0.80))
    sleep(3)
    if exists(Pattern("1452698222728.png").similar(0.95)):
        print "validate Test OK"
    else:
        print "validate Test FAILURE"
    click("1452698376196.png")

def leftRightNavigationTest():
    click("1452698439828.png")
    sleep(1)
    if exists("1452698489923.png") or exists("nueva.png"): 
        print "left->right navigation Test OK"
    else:
        print "left->right navigation Test FAILURE"

def rightLeftNavigationTest():
    if not exists("1452698489923.png"):
        sleep(30)
        click("1452698439828.png")
    click("1452698574754.png")
    sleep(1)
    if exists("1452698619705.png"):
        print "right->left navigation Test OK"
    else:
        print "right->left navigation Test FAILURE"
