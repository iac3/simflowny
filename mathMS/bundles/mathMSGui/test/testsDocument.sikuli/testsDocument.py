import os.path
import SimflownyLib
reload(SimflownyLib)
from SimflownyLib import *

def moveDocument():
    click(Pattern("1452250534491.png").similar(0.80))
    type(Key.DOWN)
    type(Key.DOWN)
    type(Key.ENTER)
    sleep(0.5)
    dragDrop("1452161159921.png", "1452250449434.png")
    sleep(0.5)
    click("1452250449434.png")
    sleep(0.5)
    type(Key.TAB)
    click("1452161292469.png")
    sleep(1)
    wait(Pattern("1452162478282.png").similar(0.98))
    
    click(Pattern("1452162478282.png").similar(0.98))
    type("testMoveDocument" + Key.ENTER);
    sleep(1)
    if (os.path.isfile(os.path.expanduser("~") + "/testMoveDocument.zip")):
        os.remove(os.path.expanduser("~") + "/testMoveDocument.zip")
        print "moveDocumentTest OK"
    else:
        print "moveDocumentTest FAILURE"

def deleteDocumentTest(basePath):
    click("1452265949283.png")
    sleep(0.5) 
    type(Key.TAB)
    sleep(0.5) 
    click("1452265999463.png")
    sleep(0.5)
    click("1452266029272.png")
    sleep(1)
    if exists("1452266075134.png"):
        print "delete document Test FAILURE"
    else:
        print "delete document Test OK"

def editDocumentTest():
    click("1452266511608.png")
    sleep(0.5) 
    type(Key.TAB + Key.ENTER)
    wait("1452266567030.png")
    moveTreeDown(1)
    expandNode()
    moveTreeDown(1)
    modifyValue("Modified problem")
    sleep(15) #Worst case 15 seconds to save
    type("w", Key.CTRL)
    if exists("1452267086398.png"):
        print "modify document Test OK"
    else:
        print "modify document Test FAILURE"

def importDocumentTest(basePath):
    click("1452501400506.png")
    type(Key.DOWN + Key.DOWN + Key.ENTER)
    sleep(0.3)
    click("1452501463481.png")
    sleep(0.5)
    type("l",Key.CTRL)
    safeType(basePath + "/testImportDocument.xml" + Key.ENTER)
    sleep(1.5)
    if exists("1452502084639.png"):
        click("1452502084639.png")
        click("1452161292469.png")
        sleep(1)
        wait(Pattern("1452162478282.png").similar(0.98))
    
        click(Pattern("1452162478282.png").similar(0.98))
        type("testImportDocument" + Key.ENTER);
        sleep(1)
        if (os.path.isfile(os.path.expanduser("~") + "/testImportDocument.zip")):
           extractZip(os.path.expanduser("~") + '/testImportDocument.zip', os.path.expanduser("~") + "/testsSikuliGUI")
           removeID(os.path.expanduser("~") + "/testsSikuliGUI/Elementary Cellular Automata.xml")
           if compareFiles(os.path.expanduser("~") + "/testsSikuliGUI/Elementary Cellular Automata.xml", basePath + "/testImportDocument.xml"):
               print "import document Test OK"
           else:
               print "import document Test FAILURE: file imported different from expected"
           removeFolder(os.path.expanduser("~") + "/testsSikuliGUI")
        os.remove(os.path.expanduser("~") + "/testImportDocument.zip")
    else:
        print "import document Test FAILURE: any file imported"

def importX3D(basePath):
    click("1452508825370.png")
    type(Key.DOWN + Key.ENTER)
    sleep(0.3)
    click("1452508853826.png")
    sleep(0.5)
    type("l",Key.CTRL)
    safeType(basePath + "/testImportX3D.x3d" + Key.ENTER)
    wait("1452508966144.png")
    moveTreeDown(1)
    expandNode()
    moveTreeDown(1)
    modifyValue("Imported x3d")
    moveTreeDown(2)
    modifyValue("Test")
    moveTreeDown(1)
    modifyValue("Test")
    sleep(15) #Worst case 15 seconds to save
    type("w", Key.CTRL)
    click("1452509273157.png")
    sleep(0.3)
    click("1452509288907.png")
    sleep(1)
    wait(Pattern("1452509334739.png").similar(0.98))
    click(Pattern("1452509334739.png").similar(0.98))
    type("testImportX3D" + Key.ENTER);
    sleep(1)
    if (os.path.isfile(os.path.expanduser("~") + "/testImportX3D.zip")):
        extractZip(os.path.expanduser("~") + '/testImportX3D.zip', os.path.expanduser("~") + "/testsSikuliGUI")
        if compareFiles(os.path.expanduser("~") + "/testsSikuliGUI/Imported x3d.x3d", basePath + "/testImportX3D.x3d"):
            print "import x3d Test OK"
        else:
            print "import x3d Test FAILURE: file imported different from expected"
        removeFolder(os.path.expanduser("~") + "/testsSikuliGUI")
        os.remove(os.path.expanduser("~") + "/testImportX3D.zip")
    else:
        print "import x3d Test FAILURE: any file imported"

def createDocumentTest():
    click("1452583235067.png")
    click("1452583256178.png")
    selectOption(5)
    wait("1452508966144.png")
    moveTreeDown(1)
    expandNode()
    moveTreeDown(1)
    modifyValue("New document")
    moveTreeDown(2)
    modifyValue("Test")
    moveTreeDown(1)
    modifyValue("Test")
    sleep(15) #Worst case 15 seconds to save
    type("w", Key.CTRL)
    if exists("1452584384424.png"):
        print "create document Test OK"
    else:
        print "create document Test FAILURE"