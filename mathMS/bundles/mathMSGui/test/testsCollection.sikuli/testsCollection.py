import os.path
import SimflownyLib
reload(SimflownyLib)
from SimflownyLib import *

def createCollectionTest(basePath):
    click("1452249704080.png")
    openContextualMenu()
    selectOption(1)
    type("newCollection" + Key.ENTER)
    sleep(1)
    if (os.path.isdir(basePath + "/db/xindiceDB/db/docManager/testCreateCollection/newCollection")):
        print "createCollectionTest OK"
    else:
        print "createCollectionTest FAILURE"

def renameCollectionTest(basePath):
    click("1452249914717.png")
    type(Key.DOWN)
    openContextualMenu()
    selectOption(2)
    type("renamedCollection" + Key.ENTER)
    sleep(1.5)
    if (os.path.isdir(basePath + "/db/xindiceDB/db/docManager/testRenameCollection/renamedCollection") and not os.path.isdir(basePath + "/db/xindiceDB/db/docManager/testRenameCollection/collectionToRename")):
        print "renameCollectionTest OK"
    else:
        print "renameCollectionTest FAILURE"

def deleteCollectionTest(basePath):
    click("1452250177826.png")       
    type(Key.DOWN)
    openContextualMenu()
    selectOption(3)
    type(Key.ENTER)
    sleep(1.5)
    if (not os.path.isdir(basePath + "/db/xindiceDB/db/docManager/testDeleteCollection/folderToDelete")):
        print "deleteCollectionTest OK"
    else:
        print "deleteCollectionTest FAILURE"


def moveCollectionTest(basePath):
    dragDrop("1452250296239.png", "1452250309424.png")
    sleep(1.5)
    if (os.path.isdir(basePath + "/db/xindiceDB/db/docManager/testMoveCollection/collectionTarget/collectionSource")):
        print "moveCollectionTest OK"
    else:
        print "moveCollectionTest FAILURE"


