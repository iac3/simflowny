import SimflownyLib
reload(SimflownyLib)
from SimflownyLib import *
import time

#Select root folder
safeType(Key.HOME)

getDocumentList()
click("Add.png")
time.sleep(1)
selectOption(1)
time.sleep(3)
moveTreeDown(1)
expandNode()
moveTreeDown(1)
modifyValue("Null 2D")
moveTreeDown(1)
moveTreeDown(1)
modifyValue("Miquel Trias")
moveTreeDown(1)
modifyValue("1")
moveTreeDown(1)
collapseNode()
openContextualMenu()
selectOption(1)
modifyValue("Null 2D model. It does nothing.")
collapseNode()
collapseNode()
moveTreeDown(1)
expandNode()
moveTreeDown(1)
expandNode()
moveTreeDown(1)
modifyValue("x")
collapseNode()
openContextualMenu()
selectOption(1)
modifyValue("y")
collapseNode()
collapseNode()
moveTreeDown(1)
modifyValue("t")
collapseNode()
collapseNode()
moveTreeDown(1)
expandNode()
moveTreeDown(1)
modifyValue("Null")
collapseNode()
collapseNode()
moveTreeDown(1)
expandNode()
openContextualMenu()
selectOption(4)
expandNode()
moveTreeDown(1)
expandNode()
moveTreeDown(1)
modifyValue("Null equation")
moveTreeDown(1)
expandNode()
moveTreeDown(1)
modifyValue("Null")
moveTreeDown(1)
expandNode()
moveTreeDown(1)
expandNode()
moveTreeDown(1)
modifyValue("0")
collapseNode()
collapseNode()
collapseNode()
collapseNode()
collapseNode()
collapseNode()
collapseNode()
collapseNode()
collapseNode()
collapseNode()
collapseNode()
openContextualMenu()
selectOption(2)
expandNode()
openContextualMenu()
selectOption(3)
expandNode()
moveTreeDown(1)
expandNode()
moveTreeDown(1)
expandNode()
moveTreeDown(1)
modifyValue("NullEigen")
moveTreeDown(1)
expandNode()
moveTreeDown(1)
expandNode()
moveTreeDown(1)
modifyValue("1")
collapseNode()
collapseNode()
collapseNode()
collapseNode()
collapseNode()
openContextualMenu()
selectOption(1)
expandNode()
moveTreeDown(1)
expandNode()
moveTreeDown(1)
modifyValue("NullEigen")
moveTreeDown(1)
expandNode()
moveTreeDown(1)
expandNode()
collapseNode()
openContextualMenu()
selectOption(1)
modifyValue("Null")
moveTreeDown(1)
expandNode()
moveTreeDown(1)
modifyValue("1")
collapseNode()
collapseNode()
collapseNode()
collapseNode()
collapseNode()
collapseNode()
collapseNode()
collapseNode()
collapseNode()
collapseNode()
collapseNode()
collapseNode()
collapseNode()
collapseNode()
collapseNode()
openContextualMenu()
selectOption(1)
expandNode()
moveTreeDown(1)
expandNode()
moveTreeDown(1)
modifyValue("Null")
moveTreeDown(1)
expandNode()
moveTreeDown(1)
expandNode()
moveTreeDown(1)
modifyValue("NullEigen")
moveTreeDown(1)
expandNode()
moveTreeDown(1)
modifyValue("1")
collapseNode()
collapseNode()
collapseNode()
collapseNode()
collapseNode()
collapseNode()
collapseNode()
collapseNode()
collapseNode()
collapseNode()
collapseNode()
collapseNode()
collapseNode()
collapseNode()
collapseNode()
collapseNode()
collapseNode()
collapseNode()

#Download code
click("document.png")
type("w", KeyModifier.CTRL)
click("Download.png")
time.sleep(3)
type("Null 2D_1_Miquel Trias"+Key.ENTER)
