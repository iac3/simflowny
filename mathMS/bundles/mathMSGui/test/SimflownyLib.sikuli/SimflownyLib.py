from sikuli import *
import filecmp
import zipfile
import re
from os import rename
import shutil

#Tree navigation
def moveTreeDown(reps):
    for x in range(reps):
        slowType(Key.DOWN)

def moveTreeUp(reps):
    for x in range(reps):
        slowType(Key.UP)

def collapseNode():
    time.sleep(0.2)
    slowType(Key.LEFT)
    time.sleep(0.2)

def expandNode():
    time.sleep(0.2)
    slowType(Key.RIGHT)
    time.sleep(0.7)

def modifyValue(value):
    slowType(Key.INSERT)
    sleep(0.5)
    safeType(value)
    sleep(0.5)
    slowType(Key.ENTER)
    sleep(0.5)

def modifyValueSelect(reps):
    slowType(Key.INSERT)
    for x in range(reps):
        slowType(Key.DOWN)
    slowType(Key.ENTER)

def modifyUUID(tree, document):
    slowType(Key.INSERT)
    wait("1452762032398.png")
    sleep(1)
    for x in range(tree):
        slowType(Key.DOWN)
    time.sleep(0.5)
    slowType(Key.ENTER)    
    slowType(Key.TAB)
    sleep(0.5)
    for x in range(document):
        slowType(Key.DOWN)
    slowType(Key.ENTER)
    time.sleep(0.5)

#Set any document id from the root folder, which is the place for the models in the integration test
#When comparing, the id will be removed, so it doesn't matter if the selected model is correct or not
def modifyUUIDIntegration():
    slowType(Key.INSERT)
    wait("1452762032398.png")
    sleep(1)
    slowType(Key.ENTER)    
    sleep(0.5)
    slowType(Key.TAB)
    sleep(0.5)
    doubleClick("1452844237210.png")
    sleep(0.5)

def getDocumentList():
    slowType(Key.ENTER)

#Menu navigation
def selectOption(num):
    time.sleep(0.2)
    for x in range(num):
        slowType(Key.DOWN)
    slowType(Key.ENTER)
    time.sleep(0.5)

def openContextualMenu():
    type(Key.F10, KeyModifier.SHIFT)

# General functions
def safeType(string):
    for char in string:
        if (char=='='):
            type("=", KeyModifier.SHIFT) # =
        elif (char=='+'):
            type(Key.ADD)                # +
        elif (char=='('):
            type("8", KeyModifier.SHIFT) # (
        elif (char==')'):
            type("(", KeyModifier.SHIFT) # )
        elif (char=='$'):
            type("$", KeyModifier.SHIFT) # $   
        elif (char=='%'):
            type("5", KeyModifier.SHIFT) # ^
        elif (char=='^'):
            paste("^")  # ^
        elif (char=='/'):
            type("7", KeyModifier.SHIFT) # /
        elif (char=='>'):
            paste('>') # > 
        elif (char=='*'):
            type(Key.MULTIPLY)           # *                   
        else:
            paste(char)
        sleep(0.05)
    return

def slowType(val):
    Settings.TypeDelay = 0.5
    type(val)

def compareFiles(fileA, fileB):
    return filecmp.cmp(os.path.abspath(fileA), os.path.abspath(fileB), False)

def compareFileSize(fileA, fileB):
    sizeinfoA = os.stat(fileA).st_size
    sizeinfoB = os.stat(fileB).st_size
    return sizeinfoA == sizeinfoB

def extractZip(file, folder):
    if not os.path.isdir(folder):
        os.makedirs(folder)
    z = zipfile.ZipFile(file)
    for n in z.namelist():
        dest = os.path.join(folder, n)
        destdir = os.path.dirname(dest)
        if not os.path.isdir(destdir):
            os.makedirs(destdir)
        data = z.read(n)
        f = open(dest, 'w')
        f.write(data)
        f.close()
    z.close()

def removeID(file):
    fo = open(file + ".tmp", 'w')
    fi = open(file, 'r')
    for line in fi:
        fo.write(re.sub("<mms:id>[0-9A-Za-z\-]*<\/mms:id>","<mms:id/>",line))
    fi.close()
    fo.close()
    rename(file + ".tmp", file)

def removeFolder(folder):
    shutil.rmtree(folder)