import os.path
import SimflownyLib
reload(SimflownyLib)
from SimflownyLib import *

def xmlLatex(basePath):
    click("1452250754575.png")
    sleep(0.5)
    type(Key.TAB)
    sleep(0.5)
    click("1452163760652.png")
    sleep(0.5)
    type(Key.DOWN)
    type(Key.ENTER)
    sleep(1)
    wait(Pattern("1452162478282.png").similar(0.98))
    
    click(Pattern("1452162478282.png").similar(0.98))
    type("testXmlLatex" + Key.ENTER);
    sleep(1)
    if (os.path.isfile(os.path.expanduser("~") + "/testXmlLatex.tex")):
        if (compareFiles(os.path.expanduser("~") + "/testXmlLatex.tex", basePath + "/testXmlLatex.tex")):
            print "xmlLatexTest OK"  
        else:
            print "xmlLatexTest FAILURE"
        os.remove(os.path.expanduser("~") + "/testXmlLatex.tex")
    else:
        print "xmlLatexTest FAILURE"

def xmlPdf(basePath):
    click("1452250959461.png")
    sleep(0.5)
    type(Key.TAB)
    sleep(0.5)
    click("1452163760652.png")
    sleep(0.5)
    type(Key.DOWN)
    type(Key.DOWN)
    type(Key.ENTER)
    sleep(1)
    wait(Pattern("1452162478282.png").similar(0.98))
    
    click(Pattern("1452162478282.png").similar(0.98))
    type("testXmlPdf" + Key.ENTER);
    sleep(1)
    if (os.path.isfile(os.path.expanduser("~") + "/testXmlPdf.pdf")):
        #All generated pdfs have different bytes, so compare sizes instead of content
        if (compareFileSize(os.path.expanduser("~") + "/testXmlPdf.pdf", basePath + "/testXmlPdf.pdf")):
            print "xmlPdfTest OK"  
        else:
            print "xmlPdfTest FAILURE comparacion"
        os.remove(os.path.expanduser("~") + "/testXmlPdf.pdf")
    else:
        print "xmlPdfTest FAILURE"

def discretizeTest():
    click("1452251047859.png")
    sleep(0.5)
    type(Key.TAB)
    type(Key.DOWN+Key.DOWN+Key.DOWN+Key.DOWN)
    sleep(0.5)
    click("1452163760652.png")
    sleep(0.5)
    type(Key.DOWN)
    type(Key.ENTER)
    sleep(0.5)
    waitVanish("1452251267568.png")
    click(Pattern("1452251722472.png").targetOffset(-37,-1))
    sleep(0.5)
    type(Key.DOWN+Key.DOWN+Key.DOWN+Key.RIGHT)
    sleep(0.5)
    type(Key.DOWN+Key.DOWN+Key.DOWN+Key.ENTER)
    sleep(1)
    click(Pattern("1452251868669.png").targetOffset(-49,-1))
    sleep(0.5)
    type(Key.DOWN+Key.DOWN+Key.DOWN+Key.DOWN+Key.RIGHT)
    sleep(0.5)
    type(Key.DOWN+Key.DOWN+Key.ENTER)
    sleep(1)
    if (exists("1452262869672.png")):
        print "discretize test OK"  
    else:
        print "discretize test FAILURE"
    click(Pattern("1452251868669.png").targetOffset(-49,-1))
    sleep(0.5)
    type(Key.DOWN+Key.DOWN+Key.DOWN+Key.DOWN+Key.ENTER)

def discretizationPolicyTest():
    click("1452263459766.png")
    sleep(0.5)
    type(Key.TAB)
    type(Key.DOWN)
    sleep(0.5)
    click("1452163760652.png")
    sleep(0.5)
    type(Key.DOWN + Key.ENTER)
    sleep(1.5)
    click(Pattern("1452251868669.png").targetOffset(-49,-1))
    sleep(0.5)
    type(Key.DOWN+Key.DOWN+Key.DOWN+Key.DOWN+Key.RIGHT)
    sleep(0.5)
    type(Key.ENTER)
    sleep(1)
    if (exists("1452263795010.png")):
        print "discretization policy test OK"  
    else:
        print "discretization policy test FAILURE"
    click(Pattern("1452251868669.png").targetOffset(-49,-1))
    sleep(0.5)
    type(Key.DOWN+Key.DOWN+Key.DOWN+Key.DOWN+Key.ENTER)

def generateSAMRAITest(basePath):
    click("1452264162548.png")
    sleep(0.5)
    type(Key.TAB)
    sleep(0.5)
    click("1452264237379.png")
    sleep(0.5)
    type(Key.DOWN + Key.ENTER)
    waitVanish("1452264510422.png")
    sleep(0.5)
    if (exists(Pattern("1452264550663.png").exact())):
        files = [f for f in os.listdir(basePath + "/codeFiles/Advection with Sinus 2D/Carles Bona Jr./1/simPlatSAMRAI/AdvectionwithSinus2D/src")]
        expected = ['Makefile', 'Problem.h', 'MainRestartData.h', 'SAMRAIConnector.cpp', 'Problem.cpp', 'problem.input', 'Functions.cpp', 'MainRestartData.cpp', 'Functions.h']
        if files.sort() == expected.sort():
            print "generate SAMRAI code test OK"
        else:
            print "generate SAMRAI code test FAILURE"
    else:
        print "generate SAMRAI code test FAILURE"

def generateBOOSTTest(basePath):
    click("1452265564534.png")
    sleep(0.5)
    type(Key.TAB)
    type(Key.DOWN)
    sleep(0.5)
    click("1452265591406.png")
    sleep(0.5)
    type(Key.DOWN + Key.ENTER)
    waitVanish("Captura de pantalla de 2016-01-08 16:07:36.png")
    sleep(0.5)
    if (exists(Pattern("1452265778539.png").exact())):
        files = [f for f in os.listdir(basePath + "/codeFiles/Cash and goods/Miquel Trias/1/simPlatSAMRAI/Cashandgoods/src")]
        expected = ['Cashandgoods.hpp', 'iac3_plod_generator.hpp', 'Makefile', 'problem.input']
        if files.sort() == expected.sort():
            print "generate BOOST code test OK"
        else:
            print "generate BOOST code test FAILURE"
    else:
        print "generate BOOST code test FAILURE"