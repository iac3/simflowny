/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.xQueryPlugin;
/**
 * Data returned by the getXquery method.
 */
public class XQueryData {
	
    private String xquery;
    private String discSchema;
	/**
	 * Constructor.
	 * @param xqueryData xquery data
	 * @param discSchemaData discretization schema data
	 */
    public XQueryData(String xqueryData, String discSchemaData) {
        this.xquery = xqueryData;
        this.discSchema = discSchemaData;
    }
	/**
	 * setter.
	 * @param xquery new xquery data.
	 */
    public void setXquery(String xquery) {
        this.xquery = xquery;
    }
    /**
     * getter.
     * @return the xquery data.
     */
    public String getXquery() {
        return xquery;
    }
    /**
     * setter.
     * @param discSchema the discSchema data.
     */
    public void setDiscSchema(String discSchema) {
    	this.discSchema = discSchema;
    }
    /**
     * getter.
     * @return the discSchema data
     */
    public String getDiscSchema() {
        return discSchema;
    }
}
