/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/

package eu.iac3.mathMS.xQueryPlugin;

/** 
 * PhysicalModels is an interface that provides the functionality 
 * To permit querying and updating of the database of physical models and
 * physical problems.
 * 
 * @author      ----
 * @version     ----
 */
public interface XQueryPlugin {
	
	/*
	 * String mathMLToDSML(String mathml);
	    String infixToDSML(String infixFormula);
		DiscretizatinSchema xQueryToDSML(String xQuery);
		String saveXQuery(String xQuery, Head head);
		La función hará:Convertir xquery a esquema, concatenarle el head, guardarlo en la bbdd de esquemas, después guardar el 
		xquey en la bbdd de Derby con el id del esquema como primarykey
		String modifyXQuery(String oldId, String xQuery, Head head);
		La función hará:Convertir xquery a esquema, concatenarle el head, llamar a la función de modificación de DiscSchemasPlugin, 
		borrar el antiguo xquery, meter en nuevo xquery con el nuevo id
		XQueryData getXQuery(String id);
		Obtener el xquery, Obtener el esquema, devolver al GUI tanto el xquery como el esquema:
		donde XqueryData contiene:
		DiscretizationShema schema;
		String xQuery;
		Void delXQuery(String[] id);
		Tendrá que borrar tanto el esquema como el xquery.

	 */
	/**
	 * Transforms mathml with notations to dsml.
	 * @param mathml the mathml content xml with notations
	 * @return the dsml xml
	 * @throws XQException   xqueryPlugin exceptions.
	 */
    String mathMLToDSML(String mathml) throws XQException;
    
    /**
     * Transform an infix formula to DSML xml.
     * @param infixFormula the infix formula
     * @return the DSML xml.
     * @throws XQException   xqueryPlugin exceptions.
     */
    String infixToDSML(String infixFormula) throws XQException;
    
    /**
     * Transforms an xQuery with infix formulas to a discretization schema xml.
     * @param xQuery xQuery program with infix formulas
     * @return the corresponding discretizationSchema xml.
     * @throws XQException   xqueryPlugin exceptions.
     */
    String xQueryToDSML(String xQuery) throws XQException;
    
    /**
     * Saves the xquery and its associated schema program to the bbdd.
     * @param xQuery The xquery program to save.
     * @param head The corresponding head schema.
     * @return It returns the document ID 
     * @throws XQException   xqueryPlugin exceptions.
     */
    String saveXQuery(String xQuery, String head) throws XQException;
    
    /**
     * Retrives the xquery data.
     * @param id The xquery id
     * @return the XQueryData type containing the xquery and the schema 
     * @throws XQException xqueryPlugin exceptions.
     */
    XQueryData getXQuery(String id) throws XQException;
    /**
     * Updates the xquery and its associated schema program to the bbdd.
     * @param oldId The old xquery id
     * @param xQuery The modified xquery source code
     * @param head The modified xquery header
     * @return the xquery new id
     * @throws XQException xqueryPlugin exceptions.
     */
    String modifyXQuery(String oldId, String xQuery, String head) throws XQException;
    /**
     * deletes the xquery and its associated schema to the bbdd.
     * @param id                The id's list to delete
     * @throws XQException      XML document not found
     */
    void delXQuery(String[] id)throws XQException;

	
}
