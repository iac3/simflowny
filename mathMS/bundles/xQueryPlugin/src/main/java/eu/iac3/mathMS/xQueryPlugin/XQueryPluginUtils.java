/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.xQueryPlugin;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.xml.serialize.OutputFormat;
import org.apache.xml.serialize.XMLSerializer;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;

/**
 * XqueryPlugin utilities.
 */
public class XQueryPluginUtils {
    final int indentation = 4;
    private int start = 0;
    private int end = 0;
    private int state = 0;
    private int errorCode = 0;
    private int  loc = 0;
    private List<TokenRecord> tList;
    private int isTimes = 0;
    private String word;
    private int temp1 = 0;
    private DocumentBuilder builder;
    private Document xmlDoc;
    
    //constant token types
    private final int chToken = 1;
    private final int nuToken = 2;
    private final int ptToken = 3;
    private final int opToken = 4;
    private final int spToken = 5;
    
    static final int THREE = 3;
    static final int FOUR = 4;
    static final int FIVE = 5;
    static final int SIX = 6;
    static final int SEVEN = 7;
    static final int CHAR_TYPE_1 = 9633;
    static final int CHAR_TYPE_2 = 8734;
    static final int CHAR_TYPE_3 = 945;
    static final int CHAR_TYPE_4 = 970;
    static final int CHAR_TYPE_5 = 913;
    static final int CHAR_TYPE_6 = 937;
	
    /**
     * Constructor.
     * @throws ParserConfigurationException Parser exception.
     */
    public XQueryPluginUtils() throws ParserConfigurationException {
        builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        xmlDoc = builder.newDocument();
    }
	
	/**
	 * Private class used to store a token.
	 */
    private class TokenRecord {
        private String word;
        private int state;
		/**
		 * Constructor.
		 * @param word  The token word.
		 * @param state The state of the token:
		 *              Error = -1   Character = 1  Number = 2  '.' = 3   Operator = 4 Space = 5		 
		 */
        public TokenRecord(String word, int state) {
            this.word = word;
            this.setState(state);
        }
        /**
         * word getter.
         * @return the word.
         */
        public String getWord() {
            return word;
        }
        /**
         * state setter.
         * @param state The new state.
         */
        public void setState(int state) {
            this.state = state;
        }
        /**
         * state getter.
         * @return the actual state.
         */
        public int getState() {
            return state;
        }			
    }
	/**
	 * Class to store an operation data.
	 */
    private class OperRecord {
    	private String op;
    	private int ary;
		/**
		 * Constructor.
		 * @param op  The operation identifier.
		 * @param ary The number of arguments of the operation.
		 */
        public OperRecord(String op, int ary) {
            this.setOp(op);
            this.setAry(ary);
        }
        /**
         * Setter.
         * @param op The operation identifier
         */
        public void setOp(String op) {
            this.op = op;
        }
        /**
         * Getter.
         * @return The operation.
         */
        public String getOp() {
            return op;
        }
        /**
         * Setter.
         * @param ary The new ary.
         */
        public void setAry(int ary) {
            this.ary = ary;
        }
        /**
         * Getter.
         * @return the ary value.
         */
        public int getAry() {
            return ary;
        }
    }
    
	/**
	 * Ressets all the main data.
	 */
    private void reset() {
        start = 0;
        end = 0;
        state = 0;
        errorCode = 0;
        loc = 0;
    }
    
    /**
     * Creates a DOM element.
     * @param name The Dom element name
     * @return the Dom element.
     */
    private Node createElement(String name) {
    	return xmlDoc.createElement("mt:" + name);
    }
	/**
	 * Transforms an infix formula into a mathml content DOM Node.
	 * @param str The infix formula
	 * @return the mathml content DOM.
	 * @throws Exception exceptions.
	 */
    private Node convertToContentDOM(String str) throws Exception {
    	final int operator = FOUR;
        String expression = new String(str);
        reset();
        //fill the tList with the expression tokens.
        tList = new ArrayList<TokenRecord>();
        while ((end < expression.length()) && (errorCode >= 0)) {
            word = getToken(expression);
            if (errorCode < 0) {
                throw new Exception();
            }
            else {
                tList.add(new TokenRecord(word, state));
                if (isTimes == 1) { 	 
                    tList.add(new TokenRecord("*", operator));
                    isTimes = 0;
                }
            }
        }
        tList.add(new TokenRecord(";", operator));
        Node expr = mathmlContent();
        return expr;
    }
    
    /**
     * Converts a Dom node to string.
     * 
     * @param node              The dom node
     * @return                  The String
     * @throws TransformerException exception.
     * @throws UnsupportedEncodingException exception.
     */
    public String domToString(Node node) throws TransformerException, UnsupportedEncodingException {
    	String res = null;
        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        StreamResult result = new StreamResult(new ByteArrayOutputStream());
        DOMSource source = new DOMSource(node);
        transformer.transform(source, result);
        res = new String(((ByteArrayOutputStream) result.getOutputStream()).toByteArray(), "UTF-8");
        return res;
    }
    
/**
 * Transforms an infix formula into a mathml content xml.
 * @param str The infix formula
 * @return the mathml content as string.
 * @throws Exception external exception.
 */
    public String convertToMathMLSTR(String str) throws Exception {
        xmlDoc = builder.newDocument();
        Node node = convertToContentDOM(str);
        Node root = createElement("math");
        ((Element)root).setAttribute("xmlns:mt", "http://www.w3.org/1998/Math/MathML");
        root.appendChild(node);
        String result = domToString(root);
        return result;
    }
	/**
	 * Returns the next token of the given infix formula.
	 * @param str The infix formula
	 * @return the next token of the infix formula.
	 * @throws XQException 
	 */
    private String getToken(String str) throws XQException {
        state = 0;
        errorCode = 0;
        String token;
        int cType;
        while (end < str.length()) {		  	
            cType = charType(str.charAt(end));
            if (cType < 1) {
                throw new XQException(XQException.XQ004, "XQuery unknown token '" + str.charAt(end) + "'");
            }
            switch (state) {
                case 0:	// Initial State		  				  	
                    switch (cType) {
                        case chToken:	// Character
                            state = chToken;
                            end++;
                            break;
                        case nuToken:
                            state = nuToken;
                            end++;
                            break;
                        case ptToken:
                            state = ptToken;
                            end++;
                            break;
                        case opToken:
                            state = opToken;
                            end++;
                            break;
                        case spToken:
                            end++;
                            start = end;
                    }
                    break;
                case chToken:	// Previous Character		 
                    switch (cType)  {
                        case 1:
                        case 2: 
                            end++; 
                            break;  		  			
                        case THREE: 
                            if (charType(str.charAt(end + 1)) == THREE) {
                                token = str.substring(start, end);
                                start = end;    		  			   
                                return token;
                            }
                            else {	 
                                state = THREE;   		  			
                                end++; 
                            } 
                            break;
                        default:
                            token = str.substring(start, end);
                            start = end;  		  		
                            return token;
                    } 		  		  		  	 		  
                    break;
                case 2:	// Previous Number
                    switch (cType) {
                        case 1:
                            isTimes = 1;
                            token = str.substring(start, end);
                            start = end;
                            return token;
                        case 2: 
                            end++; 
                            break;  		  			
                        case THREE:
                            if (charType(str.charAt(end + 1)) == THREE) {
                                token = str.substring(start, end);
                                start = end;    		  			   
                                return token;
                            }
                            else {	 
                                state = THREE;   		  			
                                end++; 
                            }
                        
                            break;
                        default:
                            token = str.substring(start, end);
                            start = end;
                            String tokenNext = str.substring(end, end + 1);
                            if ("(".equals(tokenNext) || "[".equals(tokenNext)) {
                                isTimes = 1;
                            }
                            return token;
                    }
                    break;
                case THREE:	// Previous '.'
                    switch (cType) {
                        case 1:		  		  	 
                            token = str.substring(start, end);
                            start = end;
                            return token;
                        case 2:
                            if (charType(str.charAt(end - 2)) == THREE) {
                                token = str.substring(start, end);
                                start = end;   		  		  
                                return token;
                            }
                            else {	
                                state = 2;
                                end++;
                            }
                            break;
                        case THREE:  		  	
                            token = str.substring(start, end);
                            start = end;
                            return token;
                        case FOUR:		  	
                            token = str.substring(start, end);
                            start = end;
                            return token;
                        default:
                            return "";		  		  	
                    }
                    break;
                case FOUR:	// Previous Operator
                    if (cType == FOUR) {
                        String tmpOper = str.substring(start, end + 1);		  		 
                        if (jointOPER(tmpOper)) {		  		  		 
                            end++;
                            token = str.substring(start, end);
                            start = end;
                            return token;
                        }
                    }
                    token = str.substring(start, end);
                    start = end;
                    return token;
                default:
                    return "";
            }
        }  // End of While
        token = str.substring(start, end);
        start = end;
        return token;
    }

    /**
     * Determines if its a two letters operator.
     * @param str The string to check
     * @return true if str its a tow letters operator.
     */
    private boolean jointOPER(String str) {
    	if ("==".equals(str) || ">=".equals(str) || "<=".equals(str) || "!=".equals(str)) {
            return true;
    	}
    	else {
            return false;
    	}
    }
    /**
     * Determines the character type.
     * Error = -1   Character = 1  Number = 2  '.' = 3   Operator = 4 Space = 5
     * @param ch The character to analize.
     * @return the character type.
     */
    private int charType(char ch) {
        if (((ch >= 'A') && (ch <= 'Z')) || ((ch >= 'a') && (ch <= 'z'))) {
            return 1;
        }
        if ((ch >= '0') && (ch <= '9')) {
            return 2;
        }
        if (ch == '.') {
            return THREE;
        }
        if (ch == '&') {
            return 1;
        }
        int ch1 = (int)ch; 
        if ((ch1 == CHAR_TYPE_1) || (ch1 == CHAR_TYPE_2) || (ch1 >= CHAR_TYPE_3 && ch1 < CHAR_TYPE_4) || (ch1 >= CHAR_TYPE_5 && ch1 <= CHAR_TYPE_6)) {
            return 1;
        }
        switch (ch) {   
	    case '+' :
	    case '-' :
	    case '*' :
	    case '/' :
	    case '=' :
	    case '>' :
            case '~' :
	    case '<' :
	    case ',' :	// Seperator
	    case '|' :	// Absolute Value
	    case '!' :
	    case ';' :
	    case '(' :
	    case ')' :
	    case '[' :	// Array Index Start
	    case ']' :	// Array Index End    
	    case '{' :
	    case '}' :
	    case '^' :
                return FOUR;
	    case ' ' :
	    case '\t':
	    case '\r':
	    case '\n':
            case '\f': 
                return FIVE;	    
	    case '@' :
	    case '_' :
	    case '#' :
	    case '$' :
	    case '%' :
	    case '&' :
                return 1;
            default: 
	    	return -1;
        }
    }
	/**
	 * Determines if a string is a set function.
	 * @param str The string to check
	 * @return true if the string ia a set function
	 */
    private boolean isSetFunc(String str) {
        String tmp = str.toLowerCase();
        if ("and".equals(tmp) 
			|| "or".equals(tmp)
			|| "xor".equals(tmp)
			|| "implies".equals(tmp)
			|| "union".equals(tmp)
			|| "intersect".equals(tmp)
			|| "notsubset".equals(tmp)
			|| "notprsubset".equals(tmp)
			|| "subset".equals(tmp)
			|| "prsubset".equals(tmp)
			|| "in".equals(tmp)
			|| "notin".equals(tmp)
			|| "setdiff".equals(tmp)
			|| "cartesianproduct".equals(tmp)) {
            return true;
        }
        else {
            return false;
        }
		/*switch (tmp) {
			case "and"  :
			case "or"  :
			case "xor"  :
			case "implies"   :
			case "union"   :
			case "intersect"   :
			case "notsubset"   :
			case "notprsubset"   :
			case "subset"   :
			case "prsubset"   :
			case "in"   :
			case "notin"   :
		  case "setdiff"   :
		  case "cartesianproduct":
			 return true;

		}
		return false;*/
    }
	/**
	 * checks if a string is a reserved function.
	 * @param str The string to check
	 * @return 1 if its a reserved function. 0 if not
	 */
    private int isReservedFunc(String str) {
        String tmp = str.toLowerCase();
        if ("sin".equals(tmp)
			|| "cos".equals(tmp)
			|| "tan".equals(tmp)
			|| "sec".equals(tmp)   
			|| "csc".equals(tmp)   
			|| "cot".equals(tmp)   
			|| "sinh".equals(tmp)   
			|| "cosh".equals(tmp)   
			|| "tanh".equals(tmp)   
			|| "sech".equals(tmp)   
			|| "csch".equals(tmp)   
			|| "coth".equals(tmp)   
			|| "arcsin".equals(tmp) 
			|| "arccos".equals(tmp) 
			|| "arctan".equals(tmp) 
			|| "arccosh".equals(tmp) 
			|| "arccot".equals(tmp)   
			|| "arccoth".equals(tmp)   
			|| "arccsc".equals(tmp)   
			|| "arccsch".equals(tmp)   
			|| "arcsec".equals(tmp)
			|| "arcsech".equals(tmp)   
			|| "arcsinh".equals(tmp)
			|| "arctanh".equals(tmp)
			|| "exp".equals(tmp)
			|| "log".equals(tmp)
			|| "ln".equals(tmp)   
			|| "root".equals(tmp) 
			|| "abs".equals(tmp) 
			|| "not".equals(tmp) 
			|| "sum".equals(tmp) 
			|| "card".equals(tmp) 
			|| "exists".equals(tmp) 
			|| "forall".equals(tmp) 
			|| "sqrt".equals(tmp) 
			|| "limit".equals(tmp) 
			|| "product".equals(tmp) 
			|| "diff".equals(tmp) 
			|| "partialdiff".equals(tmp) 
			|| "int".equals(tmp) 
			|| "vector".equals(tmp)   
	        || "max".equals(tmp)
	        || "min".equals(tmp) 
			|| "matrix".equals(tmp)  			  
			|| "interval".equals(tmp)) {
            return 1;
        }
        else {
            return 0;
        }
		/*
		 * function isReservedFunc(str)
{
	var tmp = str.toLowerCase();
	switch (str) {
		case "sin"  :
		case "cos"  :
		case "tan"  :
		case "sec"   :
		case "csc"   :
		case "cot"   :
		case "sinh"   :
		case "cosh"   :
		case "tanh"   :
		case "sech"   :
		case "csch"   :
		case "coth"   :
		case "arcsin" :
		case "arccos" :
		case "arctan" :
		case "arccosh" :
		case "arccot"   :
		case "arccoth"   :
		case "arccsc"   :
		case "arccsch"   :
		case "arcsec"  :
		case "arcsech"   :
		case "arcsinh"  :
		case "arctanh"  :
		case "exp"  :
		case "log"  :
		case "ln"   :
		case "root" :
		case "abs" :
		case "not" :
		case "sum" :
		case "card" :
		case "exists" :
		case "forall" :
		case "sqrt" :
		case "limit" :
		case "product" :
		case "diff" :
		case "partialdiff" :
		case "int" :
		case "vector"   :
                case "max"   :
		case "matrix"  :
		case "interval" : return 1;
	}
	return 0;
}
		 */
    }
	/**
	 * check if a string is a reserved constant.
	 * @param str The string to check.
	 * @return true if its a reserved constant.
	 */
    private boolean isReservedConst(String str) {
        String tmp = str.toUpperCase();
        if ("PI".equals(tmp) 
			|| "E".equals(tmp)  
			|| "I".equals(tmp)   
			|| "GAMMA".equals(tmp) 
			|| "INF".equals(tmp) 
			|| "AL".equals(tmp) 
			|| "BE".equals(tmp) 
			|| "GA".equals(tmp) 
			|| "DE".equals(tmp) 
			|| "ES".equals(tmp)) {
            return true;
        }
        else {
            return false;
        }
		/*
		 * function isReservedConst(str)
          {
	          var tmp = str.toUpperCase();
	          switch (str) {
				case "PI" :
				case "E"  :
				case "I"  : 
				case "GAMMA" :
				case "INF" :
				case "AL" :
				case "BE" :
				case "GA" :
				case "DE" :
				case "ES" :
					return true;
	     }
	   return false;
       }
		 * */
    }
	/**
	 * Returns the are for a two given operators.
	 * @param oper1 operator 1
	 * @param oper2 operator 2
	 * @return return 1 if the two operanda are equals and are '+' or '*'. Els return 0
	 */
    private int nary(String oper1, String oper2) {
        if (oper1.equals(oper2)) {
            if ("+".equals(oper1) || "*".equals(oper1)) {
                return 1;
            }
            else {
            	return 0;
            }
        }
        else {
            return 0;
        }
    }
	/**
	 * Return the priority for a given operator.
	 * @param opt the operator
	 * @return the operator priority
	 */
    private int getPriority(String opt) {   
        int response  = -1;
        if ("~".equals(opt)) {
            return SEVEN;
        }
        else if ("+".equals(opt) || "-".equals(opt)) {
            return 2;
        }
        else if ("*".equals(opt) || "/".equals(opt)) {
            return THREE;
        }
        else if ("^".equals(opt)) {
            return FOUR;
        }
        else if ("(".equals(opt) || "[".equals(opt)) {
            return FIVE;
        }
        else if (")".equals(opt) || "]".equals(opt)) {
            return SIX;
        }
        else if ("=".equals(opt)
        		|| ">".equals(opt)
        		|| "<".equals(opt)
        		|| ">=".equals(opt)
        		|| "<=".equals(opt)
        		|| "!=".equals(opt)) {
            return 1;
        }
        else if (",".equals(opt) || "|".equals(opt) || ";".equals(opt)) {
            return 0;
        }
        return response;
	 /* switch (OPT) {
		case "~" :	// Special Negative Operator '-'
				return 7;
		case "+" :
		case "-" :
		    	return 2;
	  case "*" :
	  case "/" :
	    	return 3;
	  case "^" :	
	   	  return 4;
	  case "(" :
	  case "[" :	
	    	return 5;
	  case ")" :
	  case "]" :	
	    	return 6;
	  case "=" :
	  case ">" :
	  case "<" :
		case ">=" :
		case "<=" :
		case "!=" :
				return 1;
	  case "," :	
	  case "|" :	
	  case ";" :	return 0;
	 }
	  */
    }
	/**
	 * Compares the priority for a given two operators.
	 * @param left  the left operand
	 * @param right the right operand
	 * @return the comparation result.
	 */
    private int compare(String left, String right) { 
        int l = getPriority(left);
        int r = getPriority(right);
		
        if (r == 0) {
            return 2;	// ENd of expression
        }
        if (r == FIVE)  {
            return 1;	// OPT, '('
        }
        if (r == SIX) {
            return 2;	// OPT, ')'
        }
        if (l == FIVE) {
            return 1;	// '(', OPT
        }
        if (l == SIX) {
            return 2;	// ')', OPT
        }
        if (r > l)  {
            return 1;	// OPT, OPT
        }
        else {
            return 2;		
        }
    }
	
    /**
     * Creates the vector mathml element.
     * @return the vector element
     * @throws ParserConfigurationException exception.
     */
    private Node doVector() throws ParserConfigurationException {
        temp1 = 1;
        Node current = createElement("vector");
        Node subNode = null;
        loc = loc + THREE;
        while (loc < tList.size() - 1) {
            subNode = mathmlContent();
            current.appendChild(subNode);
            loc++;
        }
        return current;
    }
    /**
     * Creates the max mathml elment.
     * @return the max element
     * @throws ParserConfigurationException exception.
     */
    private Node doMax() throws ParserConfigurationException {
        temp1 = 1;
        Node current = createElement("apply");
        Node node = createElement("max");
        current.appendChild(node);
        loc = loc + THREE;
        Node subNode = null;
        while (loc < tList.size() - 1) {
            subNode = mathmlContent();
            if (subNode != null) {
                current.appendChild(subNode);
            }
            if (")".equals(tList.get(loc).getWord())) {
                loc++;
                break;
            }                     
            loc++;
        }        
        return current;       
    }
    /**
     * Creates the min mathml elment.
     * @return the min element
     * @throws ParserConfigurationException exception.
     */
    private Node doMin() throws ParserConfigurationException {
        temp1 = 1;
        Node current = createElement("apply");
        Node node = createElement("min");
        current.appendChild(node);
        loc = loc + THREE;
        Node subNode = null;
        while (loc < tList.size() - 1) {
            subNode = mathmlContent();
            if (subNode != null) {
                current.appendChild(subNode);
            }
            if (")".equals(tList.get(loc).getWord())) {
                loc++;
                break;
            }                     
            loc++;
        }        
        return current;       
    }
    /**
     * Creates the interval mathml element.
     * @return the interval element
     * @throws ParserConfigurationException exception.
     */    
    private Node doInterval() throws ParserConfigurationException {
        temp1 = 1;
        Node current = createElement("interval");
        if (("(".equals(tList.get(loc + 1).getWord())) && (")".equals(tList.get(tList.size() - 2).getWord()))) {
            ((Element)current).setAttribute("closure", "open");
        }					 				
        if ("(".equals(tList.get(loc + 1).getWord()) && "]".equals(tList.get(tList.size() - 2).getWord())) {
            ((Element)current).setAttribute("closure", "open-closed");							
        }
        if ("[".equals(tList.get(loc + 1).getWord()) && ")".equals(tList.get(tList.size() - 2).getWord())) {
            ((Element)current).setAttribute("closure", "closed-open");			
        }					
        if ("[".equals(tList.get(loc + 1).getWord()) && "]".equals(tList.get(tList.size() - 2).getWord())) {
            ((Element)current).setAttribute("closure", "closed");
        }
        loc = loc + 2;
        Node subNode = null;
        while ((loc + 1) < tList.size() - 1) {
            subNode = mathmlContent();
            current.appendChild(subNode);
            loc++;
        }			 
        return current;
    }
    
    /**
     * Creates the matrix mathml element.
     * @return the matrix element
     * @throws ParserConfigurationException exception.
     */    
    private Node doMatrix() throws ParserConfigurationException {
        temp1 = 1;
        Node current = createElement("matrix");
        Node subNode = null;
        loc = loc + THREE;						
        while (loc < tList.size() - 2) {			 
            if ("[".equals(tList.get(loc).getWord())) {							  			
                Node sNode = createElement("matrixrow");
                loc++; 
                do {
                    subNode = mathmlContent();
                    sNode.appendChild(subNode);
                    loc++; 
                } while(!"]".equals(tList.get(loc - 1).getWord()));								 
                current.appendChild(sNode);
            }
        }	
        return current;
    }
    /**
     * Creates the log mathml element.
     * @return the log element
     * @throws ParserConfigurationException exception.
     */
    private Node doLog() throws ParserConfigurationException {
        Node current = createElement("apply");
        Node node = createElement("log");
        Node subNode = null; 
        current.appendChild(node);
        loc = loc + 2;
        if ("[".equals(tList.get(loc - 1).getWord())) {
            Node  sNode = createElement("logbase");
            do {
                subNode = mathmlContent();
                sNode.appendChild(subNode);
                loc++; 
            }	while(!"]".equals(tList.get(loc - 1).getWord()));
            current.appendChild(sNode);	
        }					 												
        subNode = mathmlContent();
        current.appendChild(subNode);	    
        return current;
    }
    /**
     * Creates the sqrl mathml element.
     * @return the sqrl element
     * @throws ParserConfigurationException exception.
     */
    private Node doSqrt() throws ParserConfigurationException {
        Node current = createElement("apply");
        Node node = createElement("root");
        Node subNode = null;
        current.appendChild(node);
        loc = loc + 2;
        subNode = mathmlContent();
        current.appendChild(subNode);
		
        return current;
    }
    /**
     * Creates the abs mathml element.
     * @return the abs element
     * @throws ParserConfigurationException exception.
     */
    private Node doAbs() throws ParserConfigurationException {
        Node current = createElement("apply");
        Node node = createElement("abs");
        Node subNode = null;
        current.appendChild(node);
        loc = loc + 2;
        subNode = mathmlContent();
        current.appendChild(subNode);
        return current;
    }
    /**
     * Creates the not mathml element.
     * @return the not element
     * @throws ParserConfigurationException exception.
     */
    private Node doNot() throws ParserConfigurationException {
        Node current = createElement("apply");
        Node node = createElement("not");
        Node subNode = null;
        current.appendChild(node);
        loc = loc + 2;
        subNode = mathmlContent();
        current.appendChild(subNode);
        return current;
    }
    /**
     * Creates the sum or product mathml element.
     * @param word The actual operator name
     * @return the sum or product element
     * @throws ParserConfigurationException exception.
     */
    private Node doSumProduct(String word) throws ParserConfigurationException {
        Node current = createElement("apply");
        Node node = createElement(word.toLowerCase());
        current.appendChild(node);
        loc = loc + 2;
        Node subNode = mathmlContent();							
        while  (",".equals(tList.get(loc).getWord())) {  
            if (")".equals(tList.get(loc - 1).getWord()) && temp1 == 1) {
            	break;
            }
            loc++;
            int i = loc;
            int esp = 0;
            while (i<tList.size()) {  
                if (".".equals(tList.get(i).getWord())) {
                    esp = 1;
                } 	
                if ("sum".equals(tList.get(i).getWord()) || "product".equals(tList.get(i).getWord()) || "int".equals(tList.get(i).getWord())) {
                    break;	
                }
                i++;			 
            }
            if (esp == 1) {
                Node sNode = createElement("lowlimit");
                Node  vNode = mathmlContent();				
                sNode.appendChild(vNode);	
                current.appendChild(sNode);
                loc += 2;
                Node  eNode = createElement("uplimit");
                vNode = mathmlContent();
                eNode.appendChild(vNode);	
                current.appendChild(eNode);						
            }
            else {
                Node  sNode = createElement("domainofapplication");
                Node vNode = mathmlContent();
                sNode.appendChild(vNode);	
                current.appendChild(sNode);
            }	
        }				 
        current.appendChild(subNode);
        return current;
    }
    /**
     * Creates the forAll or exists mathml element.
     * @param word The actual operator name
     * @return the sum or product element
     * @throws ParserConfigurationException exception.
     */
    private Node doForAllExists(String word) throws ParserConfigurationException {
        Node current = createElement("apply");
        Node node = createElement(word.toLowerCase());
        current.appendChild(node);
        loc++;	
        Node sNode = createElement("bvar");
        Node eNode = createElement("ci");
        ((Element)eNode).setAttribute("type", "bvar");								
        Node subNode = mathmlContent();
        eNode.appendChild(subNode);
        sNode.appendChild(eNode);	
        current.appendChild(sNode);
        loc += 2;																
        Node vNode = mathmlContent();
        current.appendChild(vNode);		
        return current;
    }
    /**
     * Creates the diff mathml element.
     * @param word The actual operator name
     * @return the diff element
     * @throws ParserConfigurationException exception.
     */
    private Node doDiff(String word) throws ParserConfigurationException {
        Node current = createElement("apply");
        Node node = createElement(word.toLowerCase());
        current.appendChild(node);
        loc = loc + 2;
        Node subNode = mathmlContent();
        loc++;							
        Node sNode = createElement("bvar");
        Node eNode = createElement("ci");
        ((Element)eNode).setAttribute("type", "bvar");								
        Node vNode = mathmlContent();
        eNode.appendChild(vNode);
        sNode.appendChild(eNode);						
        current.appendChild(sNode);
        current.appendChild(subNode);
        return current;
    }
    /**
     * Creates the limit mathml element.
     * @param word The actual operator name
     * @return the limit element
     * @throws ParserConfigurationException exception.
     */
    private Node doLimit(String word) throws ParserConfigurationException {
        Node current = createElement("apply");
        Node node = createElement(word.toLowerCase());
        current.appendChild(node);
        loc = loc + 2;
        Node subNode = mathmlContent();
        loc++;
        if (",".equals(tList.get(loc - 1).getWord())) {
            Node  sNode = createElement("bvar");
            Node  eNode = createElement("ci");
            ((Element)eNode).setAttribute("type", "bvar");								
            Node  vNode = mathmlContent();
            eNode.appendChild(vNode);
            sNode.appendChild(eNode);						
            current.appendChild(sNode);
            loc++;
        }				   
        if ("=".equals(tList.get(loc - 1).getWord())) {
            int esp = 0;
            for (int i = loc; i < tList.size(); i++) {
                if (".".equals(tList.get(i).getWord())) { 
                    esp = 1;						
                }
            }
            if (esp == 1) {
                Node sNode = createElement("lowlimit");
                Node vNode = mathmlContent();						
                sNode.appendChild(vNode);	
                current.appendChild(sNode);
                loc += 2;
                Node eNode = createElement("uplimit");
                vNode = mathmlContent();
                eNode.appendChild(vNode);	
                current.appendChild(eNode);
            }
            else {
                Node sNode = createElement("lowlimit");
                Node vNode = mathmlContent();
                sNode.appendChild(vNode);	
                current.appendChild(sNode);
            }						
        }
        current.appendChild(subNode);
        return current;
    }
    /**
     * Creates a function mathml element.
     * @param word The actual operator name
     * @return the function element
     * @throws ParserConfigurationException exception.
     */
    private Node doDefautReservedFunc(String word) throws ParserConfigurationException {
        Node current = createElement("apply");
        Node node = createElement(word.toLowerCase());
        current.appendChild(node);
		//OPER.push(TList[Loc+1]);	// Push '(' to OPER Stack
        loc = loc + 2;				// Bypass '('
        Node subNode = mathmlContent();
        current.appendChild(subNode);		
        return current;
    }
    /**
     * Creates a function mathml element.
     * @param word The actual operator name
     * @return the function element
     * @throws ParserConfigurationException exception.
     */
    private Node doDefaultFunc(String word) throws ParserConfigurationException {
        Node current = createElement("apply");
        Node node = createElement("ci");
        Text text = xmlDoc.createTextNode(word);
        node.appendChild(text);
        current.appendChild(node);
        Node subNode = null;
		//-------------nuevo
        if ("(".equals(tList.get(loc + 1).getWord())) {
            loc = loc + THREE;
            while (loc < tList.size() - 1) {
                subNode = mathmlContent();
                if (subNode != null) {
                    current.appendChild(subNode);
                }
                if (")".equals(tList.get(loc).getWord())) {
                    loc++;
                    break;
                }                    
                loc++;
            }
        }
		//------------antiguo
        else {
            loc = loc + 2;
            subNode = mathmlContent();
            current.appendChild(subNode);
        }
        return current;
    }
    /**
     * Creates a constant mathml element.
     * @param word The actual operator name
     * @return the constant element
     */
    private Node doReservedConst(String word) {
    	String wordTmp = word.toUpperCase();
    	Node node = null;
        if ("PI".equals(wordTmp)) {
            node = createElement("pi");
        }
        else if ("E".equals(wordTmp)) {
            node = createElement("exponentiale");
        }
        else if ("I".equals(wordTmp)) {
            node = createElement("imaginaryi");
        }
        else if ("GAMMA".equals(wordTmp)) {
            node = createElement("eulergamma");
        }
        else if ("INF".equals(wordTmp)) {
            node = createElement("infinity");
        }
        return node;
    }
    /**
     * Creates a set function mathml element.
     * @param word The actual operator name
     * @param oprd the operators stack
     * @return the set function element
     * @throws ParserConfigurationException exception.
     */
    private Node doSetFunc(String word, Node oprd) throws ParserConfigurationException {
        Node node = createElement(word.toLowerCase());	
        Node current = createElement("apply");								
        current.appendChild(node);
        Node subNode = oprd;			
        current.appendChild(subNode);
        loc = loc + 1;
        Node eNode = mathmlContent();
        current.appendChild(eNode);
        return current;
    }
    /**
     * Creates a parentesses mathml element.
     * @param oprd the operators stack
     * @throws ParserConfigurationException exception.
     */
    private void doParentesses(Stack<Node> oprd) throws ParserConfigurationException {
        loc++;
        Node node = mathmlContent();
        if (node.getNodeName().contains("apply") && node.hasChildNodes() && node.getFirstChild().getNodeType() == 1 
        		&& (node.getFirstChild().getNodeName().contains("minus") || node.getFirstChild().getNodeName().contains("plus"))) {
            ((Element)node).setAttribute("parentheses", "1");
        }
        oprd.push(node);
    }
    
    /**
     * Processes the operators when the oper stack is empty.
     * @param word The actual operator name
     * @param oprd The operators stack
     * @return the mathml node
     * @throws ParserConfigurationException exception.
     */
    private Node doLZero(String word, Stack<Node> oprd) throws ParserConfigurationException {
        Node node = null;
        Node subNode = null;
    	 
        if (";".equals(word)) {
            node = oprd.pop();
            return node;
        }
        if (("{".equals(word))) { 								 
            Text text = null;
            if ("}".equals(tList.get(loc + 1).getWord())) {
                node = createElement("mi");
                text = xmlDoc.createTextNode("\u2205");
                node.appendChild(text);
            }
            else {
                temp1 = 1;
                node = createElement("set");
                loc++;
                while (loc < tList.size() - 1) { 
                    subNode = mathmlContent();
                    node.appendChild(subNode);
                    loc++;					      
                }
            }
            return node;
        }			  
        if (("[".equals(word))) { 
            temp1 = 1;
            node = createElement("list");
            loc++;
            while (loc < tList.size() - 1) { 
            	subNode = mathmlContent();
            	node.appendChild(subNode);
            	loc++;					      
            }
            return node;
        }
        if (")".equals(word)) {
            if (loc == tList.size() - 2) {
                node = oprd.pop();
                return node;
            }
            else {
            	node = oprd.pop();
                loc++;
                return node;
            }
        }
        if (",".equals(word)) {	
            node = oprd.pop();								  					
            return node;
        }
//	  if (WORD == "=")
//	   {
//			node = OPRD.pop();							
//		  return node;
//						}
        if ("]".equals(word)) {
            if (loc == tList.size() - 2) {
                node = oprd.pop();
                return node;
            }
            else {
                node = oprd.pop();
                loc++;
                return node;                   
            }
        }
        return node;
    }
    /**
     * Creates a minus mathml element.
     * @param oprd the operators stack
     * @param current The actual mathml node
     */
    private void doOperMinus(Stack<Node> oprd, Node current) {
        Node operand1 = oprd.pop();
        Node node = createElement("minus");
        current.appendChild(node);
        current.appendChild(operand1);
        oprd.push(current);
    }
    /**
     * Creates a plus mathml element.
     * @param oprd the operators stack
     * @param current The actual mathml node
     * @param act The actual operator
     */
    private void doOperPlus(Stack<Node> oprd, Node current, OperRecord act) {
        Node node = createElement("plus");
        current.appendChild(node);
        for (int a = act.ary; a >= 1;a--) {
            Node operand1 = oprd.get(oprd.size() - a);
            if (operand1.hasChildNodes() && ((Element)operand1).getAttribute("parentheses") != "1" && operand1.getFirstChild().getNodeType() == 1 
            		&& operand1.getFirstChild().getNodeName().contains("minus")) {
                Node sem = createElement("semantics");
                sem.appendChild(operand1);
                current.appendChild(sem);
            }
            else {
                current.appendChild(operand1);
            } 
        }
        for (int a = act.ary; a >= 1; a--) {
            oprd.pop();
        }
        oprd.push(current);
    }
    /**
     * Creates a minus mathml element.
     * @param oprd the operators stack
     * @param current the actual mathml node
     */
    private void doOperMinus2(Stack<Node> oprd, Node current) {
        Node operand2 = oprd.pop();
        Node operand1 = oprd.pop();
        Node node = createElement("minus");
        current.appendChild(node);
        if (operand1.hasChildNodes() && ((Element)operand1).getAttribute("parentheses") != "1" && operand1.getFirstChild().getNodeType() == 1 
        		&& (operand1.getFirstChild().getNodeName().contains("plus") || operand1.getFirstChild().getNodeName().contains("minus"))) {
            Node sem = createElement("semantics");
            sem.appendChild(operand1);
            current.appendChild(sem);
        }
        else {
            current.appendChild(operand1);
        }
        current.appendChild(operand2);
        oprd.push(current);
    }
    /**
     * Creates a plus mathml element.
     * @param oprd the operators stack
     * @param current The actual mathml node
     * @param act The actual operator
     */
    private void doOperTimes(Stack<Node> oprd, Node current, OperRecord act) {
    	Node node = createElement("times");
    	current.appendChild(node);
    	for (int a = act.ary; a >= 1; a--) {
            current.appendChild(oprd.get(oprd.size() - a));
        }
        for (int a = act.ary; a >= 1; a--) {
            oprd.pop();
        }
        oprd.push(current);
    }
    /**
     * Creates a divide mathml element.
     * @param oprd the operators stack
     * @param current The actual mathml node
     */
    private void doOperDivide(Stack<Node> oprd, Node current) {
        Node node = createElement("divide");
        Node operand2 = oprd.pop();
        Node operand1 = oprd.pop();
        current.appendChild(node);
        current.appendChild(operand1);
        current.appendChild(operand2);
        oprd.push(current);
    }
    /**
     * Creates a power mathml element.
     * @param oprd the operators stack
     * @param current The actual mathml node
     */
    private void doOperPower(Stack<Node> oprd, Node current) {
    	Node node = createElement("power");
        current.appendChild(node);
        Node operand2 = oprd.pop();
        Node operand1 = oprd.pop();
        current.appendChild(operand1);
        current.appendChild(operand2);
        oprd.push(current);
    }
    /**
     * Creates a eq mathml element.
     * @param oprd the operators stack
     * @param current The actual mathml node
     */
    private void doOperEQ(Stack<Node> oprd, Node current) {
        Node operand2 = oprd.pop();
        Node operand1 = oprd.pop();
        Node node = createElement("eq");
        current.appendChild(node);
        current.appendChild(operand1);
        current.appendChild(operand2);
        oprd.push(current);
    }
    /**
     * Creates a gt mathml element.
     * @param oprd the operators stack
     * @param current The actual mathml node
     */
    private void doOperGT(Stack<Node> oprd, Node current) {
        Node operand2 = oprd.pop();
        Node operand1 = oprd.pop();
        Node node = createElement("gt");
        current.appendChild(node);
        current.appendChild(operand1);
        current.appendChild(operand2);
        oprd.push(current);
    }
    /**
     * Creates a lt mathml element.
     * @param oprd the operators stack
     * @param current The actual mathml node
     */
    private void doOperLT(Stack<Node> oprd, Node current) {
        Node operand2 = oprd.pop();
        Node operand1 = oprd.pop();
        Node node = createElement("lt");
        current.appendChild(node);
        current.appendChild(operand1);
        current.appendChild(operand2);
        oprd.push(current);
    }
    /**
     * Creates a geq mathml element.
     * @param oprd the operators stack
     * @param current The actual mathml node
     */
    private void doOperGEQ(Stack<Node> oprd, Node current) {
        Node operand2 = oprd.pop();
        Node operand1 = oprd.pop();
        Node node = createElement("geq");
        current.appendChild(node);
        current.appendChild(operand1);
        current.appendChild(operand2);
        oprd.push(current);
    }
    /**
     * Creates a leq mathml element.
     * @param oprd the operators stack
     * @param current The actual mathml node
     */
    private void doOperLEQ(Stack<Node> oprd, Node current) {
        Node operand2 = oprd.pop();
        Node operand1 = oprd.pop();
        Node node = createElement("leq");
        current.appendChild(node);
        current.appendChild(operand1);
        current.appendChild(operand2);
        oprd.push(current);
    }
    /**
     * Creates a neq mathml element.
     * @param oprd the operators stack
     * @param current The actual mathml node
     */
    private void doOperNEQ(Stack<Node> oprd, Node current) {
        Node operand2 = oprd.pop();
        Node operand1 = oprd.pop();
        Node node = createElement("neq");
        current.appendChild(node);
        current.appendChild(operand1);
        current.appendChild(operand2);
        oprd.push(current);
    }
    /**
     * Creates a equivalent mathml element.
     * @param oprd the operators stack
     * @param current The actual mathml node
     */
    private void doOperEquivalent(Stack<Node> oprd, Node current) {
        Node operand2 = oprd.pop();
        Node operand1 = oprd.pop();
        Node node = createElement("equivalent");
        current.appendChild(node);
        current.appendChild(operand1);
        current.appendChild(operand2);
        oprd.push(current);
    }
     
    /**
     * Transform an infix expression to mathml.
     * The infix expression is in the tList variable.
     * @return The mathml expression
     * @throws ParserConfigurationException Exceptionx
     */
    private Node mathmlContent() throws ParserConfigurationException {
        Stack<Node> oprd = new Stack<Node>();
        Stack<OperRecord> oper = new Stack<OperRecord>();
        Node node = null;
        Node text;
        Node current;
        String wordTk;
		//var xmlDoc=createMSXML();
        while ((loc < tList.size()) && (errorCode >= 0)) {			
            wordTk = tList.get(loc).getWord();
            state = tList.get(loc).getState();            			
            switch (state) {
                case 1:	// Token is string
                    if (!isSetFunc(wordTk) && ((loc + 1) < tList.size()) 
						&& (("(".equals(tList.get(loc + 1).getWord())) || ("[".equals(tList.get(loc + 1).getWord())))
						|| "exists".equals(tList.get(loc).getWord()) || "forall".equals(tList.get(loc).getWord())) {	
                    	//Function Name?
                        if (isReservedFunc(wordTk) != 0) {
                            String wordTmp = wordTk.toLowerCase();
                            if ("vector".equals(wordTmp)) { current = doVector(); }
                            else if ("max".equals(wordTmp)) { current = doMax(); }
                            else if ("min".equals(wordTmp)) { current = doMin(); }
                            else if ("interval".equals(wordTmp)) { current = doInterval(); }
                            else if ("matrix".equals(wordTmp)) { current = doMatrix(); }
                            else if ("log".equals(wordTmp)) { current = doLog(); }
                            else if ("sqrt".equals(wordTmp)) { current = doSqrt(); }
                            else if ("abs".equals(wordTmp)) { current = doAbs(); }
                            else if ("not".equals(wordTmp)) { current = doNot(); }
                            else if ("sum".equals(wordTmp) || "product".equals(wordTmp)) { current = doSumProduct(wordTk); }
                            else if ("forall".equals(wordTmp) || "exists".equals(wordTmp)) { current = doForAllExists(wordTk); }
                            else if ("diff".equals(wordTmp) || "partialdiff".equals(wordTmp)) { current = doDiff(wordTk); }
                            else if ("limit".equals(wordTmp) || "int".equals(wordTmp)) { current = doLimit(wordTk); }
                            else { current = doDefautReservedFunc(wordTk); }					
                        }
                        //is function name, but not a reserved word
                        else { current = doDefaultFunc(wordTk); }
			            //function node to the stack
                        oprd.push(current);			// Push MathNode to Statck
                    }
                    else {	// TOken is not Function Name
                        if (isReservedConst(wordTk)) { oprd.push(doReservedConst(wordTk)); }
                        else {
                            if (isSetFunc(wordTk)) { oprd.push(doSetFunc(wordTk, oprd.pop())); } 
                            else {
                                if ("for".equals(tList.get(loc).getWord())) {
                                    node = oprd.pop();
                                    return node;
                                }
                                else {
                                    node = createElement("ci");
                                    text = xmlDoc.createTextNode(wordTk);
                                    node.appendChild(text);
                                    //Curent.appendChild(node);
                                    oprd.push(node);
                                    loc++;
                                }
                            }
                        }	
                    }			
                    break;
                case 2://Token is a number
                    node = createElement("cn");
                    text = xmlDoc.createTextNode(wordTk);
                    node.appendChild(text);
					//Current.appendChild(node);
                    oprd.push(node);					
                    loc++;
                   // if (charType(tList.get(loc).getWord().charAt(0)) == 1) { temp2 = 1; }
                    break;
                case THREE://Token is a point '.'
                    if (".".equals(tList.get(loc + 1).getWord())) { oprd.push(node); return node; }
                    else { loc++; }
                    break;
                case FOUR://Token is an operator.
                    if ("(".equals(wordTk)) { doParentesses(oprd); }
                    else {
                        int opSize = oper.size();
                        if (opSize == 0) {
                            if (";".equals(wordTk) || ("{".equals(wordTk)) || ("[".equals(wordTk)) || ")".equals(wordTk) 
									|| ",".equals(wordTk) || "]".equals(wordTk)) {
                                return doLZero(wordTk, oprd);
                            }
                            else {
                                if ((oprd.size() == 0) && ("-".equals(wordTk))) {	// Should be modified
                                    //node = createElement("mrow");
                                    oper.push(new OperRecord("~", 1));
                                    loc++;
                                    //return node; 
                                    break;
                                }
                                else {
                                    oper.push(new OperRecord(wordTk, 2));
                                    loc++;
                                    break;
                                }	
                            }							 							 
                        }
                        if (nary(oper.get(opSize - 1).getOp(), wordTk) == 1) {
                            oper.get(opSize - 1).setAry(oper.get(opSize - 1).getAry() + 1); 
						    //OPER[L-1].ary=OPER[L-1].ary+1;
                            loc++;
                            break;
                        }
                        else {
                            int priority = compare(oper.get(opSize - 1).getOp(), wordTk);
		  				    // High Priority
                            if (priority == 1) { oper.push(new OperRecord(wordTk, 2));loc++; }
                            if (priority == 2) {	// Low or equal priority
                                current = createElement("apply");
                                OperRecord act = oper.pop();
                                if ("~".equals(act.op)) { doOperMinus(oprd, current); }
                                else if ("+".equals(act.op)) { doOperPlus(oprd, current, act); }
                                else if ("-".equals(act.op)) { doOperMinus2(oprd, current); }
                                else if ("*".equals(act.op)) { doOperTimes(oprd, current, act); }
                                else if ("/".equals(act.op)) { doOperDivide(oprd, current); }
                                else if ("^".equals(act.op)) { doOperPower(oprd, current); }
		  						/* case "(" else {
								       OPER.push(new OperRecord(WORD,2));
								       loc++;
								       break;
								   }: */	// Never handle this
                                else if ("=".equals(act.op)) { doOperEQ(oprd, current); }
                                else if (">".equals(act.op)) { doOperGT(oprd, current); }
                                else if ("<".equals(act.op)) { doOperLT(oprd, current); }
                                else if ("[".equals(act.op)) { System.out.println(""); }
                                else if ("]".equals(act.op)) { System.out.println(""); }
                                else if (",".equals(act.op)) { System.out.println(""); }
                                else if ("|".equals(act.op)) { System.out.println(""); }
                                else if (">=".equals(act.op)) { doOperGEQ(oprd, current); }
                                else if ("<=".equals(act.op)) { doOperLEQ(oprd, current); }
                                else if ("!=".equals(act.op)) { doOperNEQ(oprd, current); }
                                else if ("==".equals(act.op)) { doOperEquivalent(oprd, current); }
                                else if (";".equals(act.op)) { System.out.println(""); }
                            }
                        }
                    } //End of Else '('
                    break;
                default :
                    Node empty = xmlDoc.createTextNode(" ");
                    return empty;
            }
        }		
        node = createElement("mrow");
        return node;
    }

//----------------------------------------------------------------------------------
//--------------------methods to transform xquery with infix to xquery with mathml--
//----------------------------------------------------------------------------------
	
    /**
     * Converts a string to Dom.
     * 
     * @param str               The string
     * @return                  The Dom document
     * @throws ParserConfigurationException exception 
     * @throws IOException exception
     * @throws SAXException exception
     */
    public Document stringToDom(String str) throws ParserConfigurationException, SAXException, IOException {
    	Document response = null;
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        docFactory.setNamespaceAware(true);
        docFactory.setIgnoringElementContentWhitespace(true);
        docFactory.setIgnoringComments(true);
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
        InputStream is = new ByteArrayInputStream(str.getBytes("UTF-8"));
        Document result = docBuilder.parse(is);
        removeWhitespaceNodes(result.getDocumentElement());
        response = result;
        return response;
    }
    
    /**
     * Remove the whitespaced nodes from a Dom element.
     * 
     * @param e The element to delete the whitespaces from
     */
    private void removeWhitespaceNodes(Element e) {
        NodeList children = e.getChildNodes();
        for (int i = children.getLength() - 1; i >= 0; i--) {
            Node child = children.item(i);
            if (child instanceof Text && ((Text) child).getData().trim().length() == 0) {
                e.removeChild(child);
            }
            else if (child instanceof Element) {
                removeWhitespaceNodes((Element) child);
            }
        }
    }
  
  /**
   * Transforms a mathml with notations to dsml.
   * @param mathML The mathml to transform
   * @return The dsml
   * @throws Exception Exceptions.
   */
    public String mathMLToDSML(String mathML) throws Exception {
    	String file;
    	String result = null;
    	try {
    	    file = mathML;
    	    //reemplazamos los que se pueda mediante expresiones, antes de pasar al dom
            file = replaceXqueryTags(file);
    	   
    	    //lo pasamos a DOM    	    
    	    Document doc = stringToDom(file);
    	    String prefix = doc.getFirstChild().getPrefix();
    	    if (prefix == null) {
    	    	prefix = "";
    	    }
    	    if (!"".equals(prefix)) {
    	    	prefix = prefix + ":";
    	    }
    	    //tratamos el caso 3
    	    doc = processCase3(doc);
    	    //tratamos el caso 4
    	    doc = processCase4(doc);
    	    
    	    result = domToString(doc);
    	    //comprovamos que no hay ningún $pos en el texto
    	    String pattern = "\\$pos";
    	    Pattern p = Pattern.compile(pattern);
    	    Matcher m = p.matcher(result);
    	    if (m.find()) {
    	    	throw new Exception("infix syntax error: $pos is not a valid name");
    	    }
    	  //comprovamos que no hay ningún $pd en el texto
    	    pattern = "\\$pd";
    	    p = Pattern.compile(pattern);
    	    m = p.matcher(result);
    	    if (m.find()) {
    	    	throw new Exception("infix syntax error: $pd is not a valid name");
    	    }
    	} 
    	catch (TransformerException e) {
            e.printStackTrace();
            throw new Exception(e.getMessageAndLocation());
    	}
        catch (Exception e) {
            throw new Exception(e.toString());
        } 
    	return result;
    }
    
    /**
     * Process the case 3.
     * @param doc   The document.
     * @return      The document processed
     */
    private Document processCase3(Document doc) {
        String prefix = doc.getFirstChild().getPrefix();
        if (prefix == null) {
            prefix = "";
        }
        if (!"".equals(prefix)) {
            prefix = prefix + ":";
        }
        NodeList transformList = doc.getElementsByTagName(prefix + "apply");
        for (int i = transformList.getLength() - 1; i >= 0; i--) {
            Node node = transformList.item(i);
            //cogemos el primer hijo
            Node fChild = node.getFirstChild();
            Node text = fChild.getFirstChild();
                            
            if (text != null && "mcs".equals(text.getNodeValue())) {
                //si el primer hijo es una función, tenemos que reemplazar todo el apply       
                Element root = doc.createElement("maxCharSpeed");
                root.appendChild(fChild.getNextSibling());
                node.getParentNode().replaceChild(root, node);
            }
            if (text != null && "mcsc".equals(text.getNodeValue())) {
                //si el primer hijo es una función, tenemos que reemplazar todo el apply       
                Element root = doc.createElement("maxCharSpeedCoordinate");
                root.appendChild(fChild.getNextSibling());
                node.getParentNode().replaceChild(root, node);
            }
            if (text != null && "src".equals(text.getNodeValue())) {
                //si el primer hijo es una función, tenemos que reemplazar todo el apply       
                Element root = doc.createElement("sources");
                root.appendChild(fChild.getNextSibling());
                node.getParentNode().replaceChild(root, node);
            }
            if (text != null && "dc".equals(text.getNodeValue())) {
                //si el primer hijo es una función, tenemos que reemplazar todo el apply       
                Element root = doc.createElement("decrementCoordinate");
                root.appendChild(fChild.getNextSibling().getFirstChild());
                node.getParentNode().replaceChild(root, node);
            }
            if (text != null && "ic".equals(text.getNodeValue())) {
                //si el primer hijo es una función, tenemos que reemplazar todo el apply       
                Element root = doc.createElement("incrementCoordinate");
                root.appendChild(fChild.getNextSibling().getFirstChild());
                node.getParentNode().replaceChild(root, node);
            }
            if (text != null && "ic2".equals(text.getNodeValue())) {
                //si el primer hijo es una función, tenemos que reemplazar todo el apply       
                Element root = doc.createElement("incrementCoordinate2");
                root.appendChild(fChild.getNextSibling().getFirstChild());
                node.getParentNode().replaceChild(root, node);
            }
            if (text != null && "dc2".equals(text.getNodeValue())) {
                //si el primer hijo es una función, tenemos que reemplazar todo el apply       
                Element root = doc.createElement("decrementCoordinate2");
                root.appendChild(fChild.getNextSibling().getFirstChild());
                node.getParentNode().replaceChild(root, node);
            }
            if (text != null && "ic1".equals(text.getNodeValue())) {
                //si el primer hijo es una función, tenemos que reemplazar todo el apply       
                Element root = doc.createElement("incrementCoordinate1");
                root.appendChild(fChild.getNextSibling().getFirstChild());
                node.getParentNode().replaceChild(root, node);
            }
            if (text != null && "ic1ic2".equals(text.getNodeValue())) {
                //si el primer hijo es una función, tenemos que reemplazar todo el apply       
                Element root = doc.createElement("incrementCoordinate1IncrementCoordinate2");
                String rootValue = fChild.getNextSibling().getFirstChild().getNodeValue() + ",";
                rootValue = rootValue + fChild.getNextSibling().getNextSibling().getFirstChild().getNodeValue();                    
                root.appendChild(doc.createTextNode(rootValue));
                node.getParentNode().replaceChild(root, node);
            }
            if (text != null && "ic1dc2".equals(text.getNodeValue())) {
                //si el primer hijo es una función, tenemos que reemplazar todo el apply       
                Element root = doc.createElement("incrementCoordinate1DecrementCoordinate2");
                String rootValue = fChild.getNextSibling().getFirstChild().getNodeValue() + ",";
                rootValue = rootValue + fChild.getNextSibling().getNextSibling().getFirstChild().getNodeValue();                    
                root.appendChild(doc.createTextNode(rootValue));
                node.getParentNode().replaceChild(root, node);
            }
            if (text != null && "dc1".equals(text.getNodeValue())) {
                //si el primer hijo es una función, tenemos que reemplazar todo el apply       
                Element root = doc.createElement("decrementCoordinate1");
                root.appendChild(fChild.getNextSibling().getFirstChild());
                node.getParentNode().replaceChild(root, node);
            }
            if (text != null && "dc1ic2".equals(text.getNodeValue())) {
                //si el primer hijo es una función, tenemos que reemplazar todo el apply       
                Element root = doc.createElement("decrementCoordinate1IncrementCoordinate2");
                String rootValue = fChild.getNextSibling().getFirstChild().getNodeValue() + ",";
                rootValue = rootValue + fChild.getNextSibling().getNextSibling().getFirstChild().getNodeValue();                    
                root.appendChild(doc.createTextNode(rootValue));
                node.getParentNode().replaceChild(root, node);
            }
            if (text != null && "dc1dc2".equals(text.getNodeValue())) {
                //si el primer hijo es una función, tenemos que reemplazar todo el apply       
                Element root = doc.createElement("decrementCoordinate1DecrementCoordinate2");
                String rootValue = fChild.getNextSibling().getFirstChild().getNodeValue() + ",";
                rootValue = rootValue + fChild.getNextSibling().getNextSibling().getFirstChild().getNodeValue();                    
                root.appendChild(doc.createTextNode(rootValue));
                node.getParentNode().replaceChild(root, node);
            }
            //nuevos tags particulas
            if (text != null && "np".equals(text.getNodeValue())) {
                //si el primer hijo es una función, tenemos que reemplazar todo el apply       
                Element root = doc.createElement("neighbourParticle");
                root.appendChild(fChild.getNextSibling());
                node.getParentNode().replaceChild(root, node);
            }
            if (text != null && "ptq".equals(text.getNodeValue())) {
                //si el primer hijo es una función, tenemos que reemplazar todo el apply
                /*parabolicTermsQ
                <parabolicTermsQ>
                <variable/>
            </parabolicTermsQ>*/
                Element root = doc.createElement("parabolicTermsQ");
                root.appendChild(fChild.getNextSibling());
                node.getParentNode().replaceChild(root, node);
            }
            if (text != null && "ptp".equals(text.getNodeValue())) {
                //si el primer hijo es una función, tenemos que reemplazar todo el apply
                /*parabolicTermsQ
                <parabolicTermsQ>
                <variable/>
            </parabolicTermsQ>*/
                Element root = doc.createElement("parabolicTermsP");
                root.appendChild(fChild.getNextSibling());
                node.getParentNode().replaceChild(root, node);
            }
            if (text != null && "ptsv".equals(text.getNodeValue())) {
                //si el primer hijo es una función, tenemos que reemplazar todo el apply
                /*parabolicTermsQ
                <parabolicTermsQ>
                <variable/>
            </parabolicTermsQ>*/
                Element root = doc.createElement("parabolicTermsSumVariables");
                root.appendChild(fChild.getNextSibling());
                node.getParentNode().replaceChild(root, node);
            }
        }
        
        return doc;
    }
  
    /**
     * Process the case 4.
     * @param doc   The document.
     * @return      The document processed
     */
    private Document processCase4(Document doc) {
        String prefix = doc.getFirstChild().getPrefix();
        if (prefix == null) {
            prefix = "";
        }
        if (!prefix.equals("")) {
            prefix = prefix + ":";
        }
        NodeList transformList = doc.getElementsByTagName(prefix + "ci");
        for (int i = transformList.getLength() - 1; i >= 0; i--) {
            Node node = transformList.item(i);
            //cogemos el primer hijo
            Node child = node.getFirstChild();
            if (child != null && child.getNodeValue() != null && child.getNodeValue().startsWith("$flux_")) {
                child.setNodeValue(child.getNodeValue().replaceFirst("\\$flux_", ""));
                ((Element)node).setAttribute("type", "flux");
            }
            if (child != null && child.getNodeValue() != null && child.getNodeValue().startsWith("$func_")) {
                child.setNodeValue(child.getNodeValue().replaceFirst("\\$func_", ""));
                ((Element)node.getParentNode()).setAttribute("type", "function");
            }
            if (child != null && child.getNodeValue() != null && child.getNodeValue().startsWith("$sh_")) {
                child.setNodeValue(child.getNodeValue().replaceFirst("\\$sh_", ""));
                ((Element)node).setAttribute("type", "shared");
            }
            //caso $u$n_p --> u^n
            if (child != null && child.getNodeValue() != null && (child.getNodeValue().startsWith("¿u¿n")
                 || child.getNodeValue().startsWith("¿sf¿n") || child.getNodeValue().startsWith("¿pd¿n"))) {
                String val = child.getNodeValue();
                //determine de time:
                String pattern = "\\_p";
                Pattern p = Pattern.compile(pattern);
                Matcher m = p.matcher(val);
                int time = 0;
                while (m.find()) {
                    time++;
                }
                Element root = doc.createElement(prefix + "msup");
                Element ciField = doc.createElement(prefix + "ci");
                String name = "";
                if (val.startsWith("¿u¿n")) {
                    name = "field";
                }
                else if (val.startsWith("¿ev¿n")) {
                    name = "eigenVector";
                }
                else if (val.startsWith("¿sf¿n")) {
                    name = "specialField";
                }
                else if (val.startsWith("¿pd¿n")) {
                    name = "particleDistance";
                }
                Element field = doc.createElement(name);
                ciField.appendChild(field);
                root.appendChild(ciField);
                if (time == 0) {
                    /*<mt:apply>
                    <mt:plus/>
                    <mt:ci>
                        <timeCoordinate/>
                    </mt:ci>
                    <mt:cn>1</mt:cn>
                    </mt:apply>*/
                    root.appendChild(createTimeSlice(doc, prefix, "1", "plus"));
                }
                else if (time == 1) {
                    /* <mt:ci>
                        <timeCoordinate/>
                       </mt:ci>*/
                    Element ci = doc.createElement(prefix + "ci");
                    Element tc = doc.createElement("timeCoordinate");
                    ci.appendChild(tc);
                    root.appendChild(ci);
                }
                else {
                    /*<mt:apply>
                     <mt:minus/>
                     <mt:ci>
                         <timeCoordinate/>
                     </mt:ci>
                     <mt:cn>n</mt:cn>
                 </mt:apply>*/
                    root.appendChild(createTimeSlice(doc, prefix, String.valueOf(time - 1), "minus"));
                }
                node.replaceChild(root, child);
            }
            //caso $pos_p --> x^n
            if (child != null && child.getNodeValue() != null && child.getNodeValue().startsWith("¿pos¿n")) {
                String val = child.getNodeValue();
                //determine de time:
                String pattern = "\\_p";
                Pattern p = Pattern.compile(pattern);
                Matcher m = p.matcher(val);
                int time = 0;
                while (m.find()) {
                    time++;
                }
                Element root = doc.createElement(prefix + "msup");
                Element ciSpat = doc.createElement(prefix + "ci");
                Element spat = doc.createElement("spatialCoordinate");
                ciSpat.appendChild(spat);
                root.appendChild(ciSpat);
                if (time == 0) {
                    /*<mt:apply>
                    <mt:plus/>
                    <mt:ci>
                        <timeCoordinate/>
                    </mt:ci>
                    <mt:cn>1</mt:cn>
                </mt:apply>*/
                    root.appendChild(createTimeSlice(doc, prefix, "1", "plus"));
                }
                else if (time == 1) {
                    /*
                <mt:ci>
                    <timeCoordinate/>
                </mt:ci>*/
                    Element ci = doc.createElement(prefix + "ci");
                    Element tc = doc.createElement("timeCoordinate");
                    ci.appendChild(tc);
                    root.appendChild(ci);
                }
                else {
                    /*<mt:apply>
                     <mt:minus/>
                     <mt:ci>
                         <timeCoordinate/>
                     </mt:ci>
                     <mt:cn>n</mt:cn>
                 </mt:apply>*/
                    root.appendChild(createTimeSlice(doc, prefix, String.valueOf(time - 1), "minus"));
                }
                node.replaceChild(root, child);
            }
        }
        return doc;
    }
    
    /**
     * Create the timeSlice.
     * @param doc           The document
     * @param prefix        The document prefix
     * @param time          The index for the time
     * @param operation     The operation for the time
     * @return              The time slice XML.
     */
    private Element createTimeSlice(Document doc, String prefix, String time, String operation) {
        Element apply = doc.createElement(prefix + "apply");
        Element plus = doc.createElement(prefix + operation);
        apply.appendChild(plus);
        Element ci = doc.createElement(prefix + "ci");
        Element tc = doc.createElement("timeCoordinate");
        ci.appendChild(tc);
        apply.appendChild(ci);
        Element cn = doc.createElement(prefix + "cn");
        cn.appendChild(doc.createTextNode(time));
        apply.appendChild(cn);
        
        return apply;
    }
    
    /**
     * Obtains a correct xQuery from a xQuery containing infix formulas.
     * @param xQuery the xquery with infix formulas
     * @return The xQuery with the infix formulas transfomed to mathml
     * @throws Exception exception
     */
    public String getXQuery(String xQuery) throws Exception {
        
        String eol = System.getProperty("line.separator");
        BufferedReader reader = new BufferedReader(new StringReader(xQuery));
        String str;
        StringBuffer resp =  new StringBuffer();
        int mainIndex = 0;
        //read each line of the file
        while ((str = reader.readLine()) != null) {
            if (str.trim().startsWith("let $form")) {        	
                int fStart = str.indexOf(":=") + 2;
                int fEnd = -1;
                String infix = "";
                String beginning =  str.substring(0, fStart);
                //hay que permitir multilineas
                while (fEnd == -1 && str != null) {
                    fEnd = str.indexOf(";");        		
                    if (fEnd == -1) {
                        infix = infix + str.substring(fStart);
                        str = reader.readLine();
                        fStart = 0;        			
                    }
                    else {
                    	infix = infix + str.substring(fStart, fEnd);
                    }
                }
                String mathml = convertToMathMLSTR(infix);
                String dsml = mathMLToDSML(mathml);
                //dsml = dsml.replaceAll("<\\?xml version=\"1.0\" encoding=\"UTF-8\"\\?>", "");
                dsml = dsml.replaceAll("<\\?xml.+\\?>", "");
                //<mt:math xmlns:mt=\"http://www.w3.org/1998/Math/MathML\">
                String newForm = beginning + dsml;
                resp.append(newForm + eol);
            }
            else {
            	resp.append(str + eol);
            }
            mainIndex = mainIndex + str.length();
        }
        return resp.toString();
    }
    
    /**
     * Replaces the Xquery tags in the string.
     * @param file      The file as string
     * @return          The file processed
     */
    private String replaceXqueryTags(String file) {
      //caso 2
        file = file.replaceAll("<[\\w|:]+>\\$ijk</[\\w|:]+>", "<deployCoordinates/>");
        //mismo caso para parabolicos
        file = file.replaceAll("<[\\w|:]+>\\$p_ijk</[\\w|:]+>", "<deployCoordinates12/>");
        //caso1
        file = file.replaceAll("\\$u\\$n", "¿u¿n");
        file = file.replaceAll("\\$pos\\$n", "¿pos¿n");
        file = file.replaceAll("\\$sf\\$n", "¿sf¿n");
        file = file.replaceAll("\\$pd\\$n", "¿pd¿n");
        file = file.replaceAll("\\$u", "<field/>");
        file = file.replaceAll("\\$ev", "<eigenVector/>");
        file = file.replaceAll("\\$in", "<iterationNumber/>");
        file = file.replaceAll("\\$ct", "<currentTime/>");
        file = file.replaceAll("\\$i", "<spatialCoordinate/>");
        file = file.replaceAll("\\$j", "<secondSpatialCoordinate/>");
        file = file.replaceAll("\\$n", "<timeCoordinate/>");
        file = file.replaceAll("\\$x", "<contSpatialCoordinate/>");
        file = file.replaceAll("\\$t", "<contTimeCoordinate/>");
        file = file.replaceAll("\\$c", "<coordinate/>");
        file = file.replaceAll("\\$o", "<contCoordinate/>");
        //mismo caso para parabolicos
        file = file.replaceAll("\\$y", "<secondContSpatialCoordinate/>");
        //mismo caso para particulas            
        file = file.replaceAll("\\$sf", "<specialField/>");
        file = file.replaceAll("\\$sl", "<smoothingLength/>");
        file = file.replaceAll("\\$pv", "<particleVolume/>");
        file = file.replaceAll("\\$ptv", "<parabolicTermsVariable/>");
        
        //caso5  $p_valor_
        //param también se puede hacer aqui
        String pattern = "\\$p\\_(\\w+)\\_";
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(file);
        List<String> reemplazos =  new ArrayList<String>();         
        while (m.find()) {
            String valor = m.group(1);              
            reemplazos.add(valor);
        }
        for (int i = 0; i < reemplazos.size(); i++) {
            file = file.replaceFirst(pattern, "<param>" + reemplazos.get(i) + "</param>");
        }
        //param también se puede hacer aqui
        //tag <parabolicTerms> $pt_valor_
        pattern = "\\$pt\\_(\\w+)\\_";
        p = Pattern.compile(pattern);
        m = p.matcher(file);
        reemplazos =  new ArrayList<String>();          
        while (m.find()) {
            String valor = m.group(1);              
            reemplazos.add(valor);
        }
        for (int i = 0; i < reemplazos.size(); i++) {
            file = file.replaceFirst(pattern, "<parabolicTerms>" + reemplazos.get(i) + "</parabolicTerms>");
        }
        
        //caso $pos[letras]
        pattern = "\\$pos(\\w+)";
        p = Pattern.compile(pattern);
        m = p.matcher(file);
        reemplazos =  new ArrayList<String>();          
        while (m.find()) {
            String valor = m.group(1);
            reemplazos.add(valor);
        }
        String reemplazo1 = "   <mt:msup>"
            + "      <mt:ci>"
            + "         <spatialCoordinate/>"
            + "      </mt:ci>"
            + "      <mt:ci>";
        String reemplazo2 =   "</mt:ci>"
            + "   </mt:msup>";
        for (int i = 0; i < reemplazos.size(); i++) {
            String value = reemplazos.get(i);
            file = file.replaceAll("\\$pos" + value, reemplazo1 + value + reemplazo2);
        }
        
        //caso $pd[letras]
        pattern = "\\$pd(\\w+)";
        p = Pattern.compile(pattern);
        m = p.matcher(file);
        reemplazos =  new ArrayList<String>();          
        while (m.find()) {
            String valor = m.group(1);
            reemplazos.add(valor);
        }
        reemplazo1 = "   <mt:msup>"
            + "      <mt:ci>"
            + "         <particleDistance/>"
            + "      </mt:ci>"
            + "      <mt:ci>";
        reemplazo2 =   "</mt:ci>"
            + "   </mt:msup>";
        for (int i = 0; i < reemplazos.size(); i++) {
            String value = reemplazos.get(i);
            file = file.replaceAll("\\$pd" + value, reemplazo1 + value + reemplazo2);
        }
        
        return file;
    }
    
    /**
     * A method for the format of the output.
     * 
     * @param document          The document to be formatted
     * @return                  The document formatted
     * @throws XQException      XQ005 - External error
     */
    public String indent(String document) throws XQException {
        try {
            DocumentBuilder docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document doc = docBuilder.parse(new ByteArrayInputStream(document.getBytes("UTF-8")));

            OutputFormat format = new OutputFormat(doc);
            format.setIndenting(true);
            format.setIndent(indentation);
            Writer output = new StringWriter();
            XMLSerializer serializer = new XMLSerializer(output, format);
            serializer.serialize(doc);

            return output.toString();
        }
        catch (Throwable e) {
            throw new XQException(XQException.XQ005, e.toString());
        }
    }
}
