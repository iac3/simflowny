/*
-Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.xQueryPlugin;

import eu.iac3.mathMS.discretizationSchemasPlugin.DDSException;
import eu.iac3.mathMS.discretizationSchemasPlugin.DiscretizationSchemas;
import eu.iac3.mathMS.discretizationSchemasPlugin.DiscretizationSchemasImpl;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import net.sf.saxon.s9api.Processor;
import net.sf.saxon.s9api.SaxonApiException;
import net.sf.saxon.s9api.XQueryCompiler;
import net.sf.saxon.s9api.XQueryEvaluator;
import net.sf.saxon.s9api.XQueryExecutable;
import net.sf.saxon.s9api.XdmValue;

import org.osgi.framework.BundleContext;
import org.osgi.service.log.LogService;
import org.osgi.util.tracker.ServiceTracker;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import org.xml.sax.SAXException;

/** 
 * PhysicalModelsImpl implements the 
 * {@link XQueryPlugin PhysicalModels} interface.
 * It provides the functionality To permit querying and updating 
 * of the database of physical models and physical problems. 
 * @author      ----
 * @version     ----
 */
public class XQueryPluginImpl implements XQueryPlugin {
    static final String DB_NAME = "CodesDB";
    static final String DRIVER = "org.apache.derby.jdbc.EmbeddedDriver";
    static final int XPATH_NOT_WELL_FORMED = 640;
    static String port;
    static LogService logservice = null;
    private XQueryPluginUtils utils;
    private DiscretizationSchemas discSchemas;
    /**
     * constructor.
     */
    public XQueryPluginImpl() {
    	try {
            utils = new XQueryPluginUtils();
        } 
    	catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
    }
    /**
     * Constructor that initialice the logger.
     * 
     * @param context  The bundle context
     */
    public XQueryPluginImpl(BundleContext context) {
    	Connection conn = null;
        Statement s = null;
        if (context.getProperty("org.osgi.service.http.port") == null) {
        	//Default port
            port = "8080";
    	} 
        else {
            port = context.getProperty("org.osgi.service.http.port");
    	}
        ServiceTracker logServiceTracker = new ServiceTracker(context, org.osgi.service.log.LogService.class.getName(), null);
        logServiceTracker.open();
        logservice = (LogService) logServiceTracker.getService();
    	discSchemas = new DiscretizationSchemasImpl(context);
        try {
            utils = new XQueryPluginUtils();
        } 
        catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        try {
	        //Get the connection
            conn = getConnection();	            
            s = conn.createStatement();
	        //Check if the table already exists
            ResultSet rs = s.executeQuery("SELECT TABLENAME FROM sys.systables where TABLENAME='XQUERYCODE'");
	        //If it does not exist then create it 
            if (!rs.next()) {
                s.executeUpdate("CREATE TABLE xqueryCode(Code_id VARCHAR(50) NOT NULL, Code CLOB, PRIMARY KEY (Code_id))");
            }	          
            s.close();
            conn.close();
            
            try {
                shutdownConnection();
            }
            catch (Exception e) {
                //Nothing to do, the method throws an exception when the database is successfully closed.
            }
        }  
        catch (Exception e)  {
            e.printStackTrace();
        }
    }
    
	/**
	 * Transforms mathml with notations to dsml.
	 * @param mathml the mathml content xml with notations
	 * @return the dsml xml
	 * @throws XQException   xqueryPlugin exceptions.
	 */
    public String mathMLToDSML(String mathml) throws XQException {
    	String resp = null;
        try {
            resp = utils.mathMLToDSML(mathml);
        } 
        catch (Exception e) {
            e.printStackTrace();
            throw new XQException(XQException.XQ003, e.getMessage());
        }
        return resp;
    }
    /**
     * Transform an infix formula to DSML xml.
     * @param infixFormula the infix formula
     * @return the DSML xml.
     * @throws XQException   xqueryPlugin exceptions.
     */
    public String infixToDSML(String infixFormula) throws XQException {
        String resp = null;
        try {
            resp = utils.convertToMathMLSTR(infixFormula);
            resp = utils.mathMLToDSML(resp);
        } 
        catch (Exception e) {
            e.printStackTrace();
            throw new XQException(XQException.XQ003, e.getMessage());
        }
        return resp;
    }

    /**
     * Transforms an xQuery with infix formulas to a discretization schema xml.
     * @param query xQuery program with infix formulas
     * @return the corresponding discretizationSchema xml.
     * @throws XQException   xqueryPlugin exceptions.
     */
    public String xQueryToDSML(String query) throws XQException {
        String resp = null;
        try {
            String validXquery = utils.getXQuery(query);
			//executes this valid xquery
            Processor p = new Processor(false);
            XQueryCompiler comp = p.newXQueryCompiler();
			//at "file:///home/tartigues/xquery/libreria.xquery"
            URIModuleResolver res = new URIModuleResolver();
            comp.setModuleURIResolver(res);
            XQueryExecutable exe = comp.compile(validXquery);
            XQueryEvaluator eval = exe.load();
            XdmValue valu = eval.evaluate();
            resp = utils.indent(valu.toString());
        } 
        catch (SaxonApiException e) {
            e.printStackTrace();
            throw new XQException(XQException.XQ003, e.getMessage());
        } 
        catch (IOException e) {
            e.printStackTrace();
            throw new XQException(XQException.XQ003, e.getMessage());
        } 
        catch (Exception e) {
            e.printStackTrace();
            throw new XQException(XQException.XQ003, e.getMessage());
        }
		
        return resp;
    }
    
    /**
     * Saves the xquery program to the bbdd.
     * @param xQuery The xquery program to save.
     * @param head The corresponding head schema.
     * @return It returns the document ID 
     * @throws XQException   xqueryPlugin exceptions.
     */
    public String saveXQuery(String xQuery, String head) throws XQException {
    	String id = null;
    	try {
        	//Transforms the xQuery into a DiscretizationSchema
            String schema = xQueryToDSML(xQuery);
        	//adds the head information to the schema.
            Document schemaDom = utils.stringToDom(schema);
            Document headDom = utils.stringToDom(head);
            Node headClon = schemaDom.importNode(headDom.getFirstChild().cloneNode(true), true);			
            Node firstChild = schemaDom.getFirstChild().getFirstChild();
            schemaDom.getFirstChild().insertBefore(headClon, firstChild);
            schema = utils.domToString(schemaDom);
			//saves the schema with the discretization schemas plugin
            id = discSchemas.addDiscretizationSchema(schema);
			//saves the xquery
            saveBBDDXquery(id, xQuery);			
        } 
    	catch (ParserConfigurationException e) {
            e.printStackTrace();
            throw new XQException(XQException.XQ003, e.getMessage());
        } 
    	catch (SAXException e) {
            e.printStackTrace();
            throw new XQException(XQException.XQ003, e.getMessage());
        } 
    	catch (IOException e) {
            e.printStackTrace();
            throw new XQException(XQException.XQ003, e.getMessage());
        } 
    	catch (TransformerException e) {
            e.printStackTrace();
            throw new XQException(XQException.XQ003, e.getMessage());
        } 
    	catch (DDSException e) {
            e.printStackTrace();
            throw new XQException(XQException.XQ004, e.getMessage());
        } 
    	return id;
    }
    
    /**
     * Retrives the xquery data.
     * @param id The xquery id
     * @return the XQueryData type containing the xquery and the schema 
     * @throws XQException xqueryPlugin exceptions.
     */
    public XQueryData getXQuery(String id) throws XQException {
    	
    	XQueryData data = null;
    	//saves the schema with the discretization schemas plugin
        try {
            String schema = discSchemas.getDiscretizationSchema(id);
            String xquery = getBBDDXquery(id);
            if (schema != null && xquery != null) {
            	data = new XQueryData(xquery, schema);
            }
        } 
        catch (DDSException e) {
            e.printStackTrace();
            throw new XQException(XQException.XQ004, e.getMessage());
        } 
        
    	return data;
    }
    
   /**
     * Updates the xquery program to the bbdd.
     * @param oldId The old xquery id
     * @param xQuery The modified xquery source code
     * @param head The modified xquery header
     * @return the xquery new id
     * @throws XQException xqueryPlugin exceptions.
     */
    public String modifyXQuery(String oldId, String xQuery, String head) throws XQException {
    	String id = null;
    	try {
        	//Transforms the xQuery into a DiscretizationSchema
            String schema = xQueryToDSML(xQuery);
        	//adds the head information to the schema.
            Document schemaDom = utils.stringToDom(schema);
            Document headDom = utils.stringToDom(head);
            Node headClon = schemaDom.importNode(headDom.getFirstChild().cloneNode(true), true);			
            Node firstChild = schemaDom.getFirstChild().getFirstChild();
            schemaDom.getFirstChild().insertBefore(headClon, firstChild);
            schema = utils.domToString(schemaDom);
			//modifies the schema with the discretization schemas plugin
            discSchemas.modifyDiscretizationSchema(oldId, schema);
            //get the new id from the header
            NodeList list = headDom.getElementsByTagName("mms:id");
            String newId = "-1";
            if (list != null && list.getLength() > 0) {
            	newId = list.item(0).getFirstChild().getNodeValue();
            }
			//modifies the xquery
            modifyBBDDXquery(oldId, newId, xQuery);		
        } 
    	catch (ParserConfigurationException e) {
    	    e.printStackTrace();
            throw new XQException(XQException.XQ003, e.getMessage());
        } 
    	catch (SAXException e) {
    	    e.printStackTrace();
            throw new XQException(XQException.XQ003, e.getMessage());
        } 
    	catch (IOException e) {
    	    e.printStackTrace();
            throw new XQException(XQException.XQ003, e.getMessage());
        } 
    	catch (TransformerException e) {
    	    e.printStackTrace();
            throw new XQException(XQException.XQ003, e.getMessage());
        } 
    	catch (DDSException e) {
    	    e.printStackTrace();
            throw new XQException(XQException.XQ004, e.getMessage());
        }
    	return id;
    }
    
    /**
     * deletes the xquery and its associated schema to the bbdd.
     * @param id The id's list to delete
     * @throws XQException exception
     */
    public void delXQuery(String[] id) throws XQException {
        try {
            for (int i = 0; id != null && i < id.length; i++) {
            	String idAux = id[i];
            	discSchemas.deleteDiscretizationSchema(idAux);
            	delBBDDXquery(idAux);				
            }
        } 
        catch (DDSException e) {
            e.printStackTrace();
            throw new XQException(XQException.XQ004, e.getMessage());
        } 
    }
    
    /**
     * Retrieves the xquery code for a given id.
     * @param id the xquery id.
     * @return the xquery source code
     * @throws XQException exception
     */
    private String getBBDDXquery(String id) throws XQException {
    	Connection conn = null;
        Statement s = null;
    	String result = null;   
    	ResultSet rs = null;
        try {
            conn = getConnection();
            s = conn.createStatement();
	    	//obtains the clob
            rs = s.executeQuery("SELECT Code FROM xqueryCode WHERE Code_id='" + id + "'");
            boolean isNext = rs.next();
            if (isNext) {
            	Clob clob = rs.getClob(1);
            	result = convertStreamToString(clob.getCharacterStream());
            }            
        } 
        catch (ClassNotFoundException e) {
            e.printStackTrace();
            throw new XQException(XQException.XQ002, e.getMessage());
        } 
        catch (SQLException e) {
            e.printStackTrace();
            throw new XQException(XQException.XQ001, e.getMessage());
        }  
        finally {
            try {
                rs.close();
                s.close();
                conn.close();
                try {
                    shutdownConnection();
                }
                catch (Exception e) {
                    //Nothing to do, the method throws an exception when the database is successfully closed. 
                }
            }
            catch (Exception e) {
                throw new XQException(XQException.XQ003, e.getMessage());
            }
        }
        return result;
    }
    /**
     * Saves an xquery source code into bbdd.
     * @param id the xquery id
     * @param xquery The xquery source code
     * @throws XQException exception
     */
    private void saveBBDDXquery(String id, String xquery) throws XQException {
    	Connection conn = null;
        Statement s = null;
        try {
            conn = getConnection();
            s = conn.createStatement();
            String xqueryAux = xquery.replaceAll("'", "''");
	        //insert the file with an enmpty clob
            
            StringReader myStringReader = new StringReader(xqueryAux);
            PreparedStatement ps = conn.prepareStatement("INSERT INTO xqueryCode(Code_id, Code) values(?, ?)");
            ps.setString(1, id);
            ps.setCharacterStream(2, myStringReader, xqueryAux.length());
            ps.execute();
            conn.commit();
        } 
        catch (ClassNotFoundException e) {
            throw new XQException(XQException.XQ002, e.getMessage());
        } 
        catch (SQLException e) {
            e.printStackTrace();
            throw new XQException(XQException.XQ001, e.getMessage());
        }  		
        catch (Exception e) {
            throw new XQException(XQException.XQ003, e.getMessage());
        }
        finally {
            try {
                s.close();
                conn.close();
                try {
                    shutdownConnection();
                }
                catch (Exception e) {
                    //Nothing to do, the method throws an exception when the database is successfully closed.   
                }
            }
            catch (Exception e) {
                throw new XQException(XQException.XQ003, e.getMessage());
            }
        }
    }
    
    /**
     * Saves an xquery source code into bbdd.
     * @param oldId the xquery old id
     * @param newId the xquery new id
     * @param xquery The xquery source code
     * @throws XQException exception
     */
    private void modifyBBDDXquery(String oldId, String newId, String xquery) throws XQException {
    	Connection conn = null;
        Statement s = null;
        try {
            conn = getConnection();
            s = conn.createStatement();
		    //deletes the old id xquery
            s.executeUpdate("DELETE FROM xqueryCode WHERE Code_id='" + oldId + "'");
		    //insert the new xquery
            saveBBDDXquery(newId, xquery);
        } 
        catch (ClassNotFoundException e) {
            e.printStackTrace();
            throw new XQException(XQException.XQ002, e.getMessage());
        } 
        catch (SQLException e) {
            e.printStackTrace();
            throw new XQException(XQException.XQ001, e.getMessage());
        }     
        finally {
            try {
            	s.close();
                conn.close();
                try {
                    shutdownConnection();
                }
                catch (Exception e) {
                    //Nothing to do, the method throws an exception when the database is successfully closed.  
                }
            }
            catch (Exception e) {
                throw new XQException(XQException.XQ003, e.getMessage());
            }
        }            
    }
    
    /**
     * Deletes an xquery source code into bbdd.
     * @param id the xquery id
     * @throws XQException exception
     */
    private void delBBDDXquery(String id) throws XQException {
    	Connection conn = null;
        Statement s = null;
        try {
            conn = getConnection();
            s = conn.createStatement();
	        //deletes the old id xquery
            s.executeUpdate("DELETE FROM xqueryCode WHERE Code_id='" + id + "'");
        } 
        catch (ClassNotFoundException e) {
            e.printStackTrace();
            throw new XQException(XQException.XQ002, e.getMessage());
        } 
        catch (SQLException e) {
            e.printStackTrace();
            throw new XQException(XQException.XQ001, e.getMessage());
        }
        finally {
            try {
            	s.close();
            	conn.close();
                try {
                    shutdownConnection();
                }
                catch (Exception e) {
                    //Nothing to do, the method throws an exception when the database is successfully closed.
                }
            }
            catch (Exception e) {
                throw new XQException(XQException.XQ003, e.getMessage());
            }
        }       
    }
    
    /**
     * Establish the connection with the DB.
     * @return                  The connection
     * @throws ClassNotFoundException 
     * @throws SQLException 
     */
    private Connection getConnection() throws ClassNotFoundException, SQLException {
        String connectionURL = "jdbc:derby:" + DB_NAME + ";create=true;"; 
        Class.forName(DRIVER); 
        return DriverManager.getConnection(connectionURL); 
    }
    
    /**
     * Shutdowns the connection with the DB.
     * @throws ClassNotFoundException 
     * @throws SQLException 
     */
    public void shutdownConnection() throws ClassNotFoundException, SQLException {
        String connectionURL = "jdbc:derby:" + DB_NAME + ";shutdown=true;";
        Class.forName(DRIVER);
        DriverManager.getConnection(connectionURL);
    }
    
    /**
     * An auxiliary function to read a file as String.
     * 
     * @param reader    The input stream of the file
     * @return          The resource as string
     */
    private static String convertStreamToString(Reader reader) {
    	
    	int c;
    	StringWriter s = new StringWriter();
        try {          
            while ((c = reader.read()) > -1) {
                s.write(c);
            }
        } 
        catch (Exception e) {
            System.err.println("Exception: " + e.getMessage());
            e.printStackTrace();
        }
        return s.toString();
    }
	
}
