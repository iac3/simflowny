/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.xQueryPlugin;

/** 
 * DPMException is the class Exception launched by the 
 * {@link XQueryPlugin PhysicalModels} interface.
 * The exceptions that this class return are:
 * XQ001	XpathQuery not well formed
 * XQ002	Not a valid XML Document
 * XQ003	XML document does not match XML Schema
 * XQ004	XML document not found
 * XQ005   External error
 * @author      ----
 * @version     ----
 */
@SuppressWarnings("serial")
public class XQException extends Exception {

    public static final String XQ001 = "SQL exception";
    public static final String XQ002 = "Class not found";
    public static final String XQ003 = "External error";
    public static final String XQ004 = "DiscSchemasPlugin error";
    public static final String XQ005 = "External error";
	
    //External error message
    private String extError;
	  /** 
	     * Constructor for known messages.
	     *
	     * @param msg  the message	     
	     */
    XQException(String msg) {
    	super(msg);
    }
	  
	  /** 
	     * Constructor.
	     *
	     * @param intMsg  the internal error message	    
	     * @param extMsg  the external error message
	     */
    XQException(String intMsg, String extMsg) {
		super(intMsg);
        extError = extMsg;
    }
	  /** 
     * Returns the error code.
     *
     * @return the error code	     
     */
    public String getErrorCode() {
    	return super.getMessage();
    }
	  /** 
	     * Returns the error message as a string.
	     *
	     * @return the error message	     
	     */
    public String getMessage() {
    	if (extError == null) {
            return "XQException: " + super.getMessage();
    	}
    	else {
            return "XQException: " + super.getMessage() + " (" + extError + ")";
    	}
    }  
}
