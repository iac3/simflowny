xquery version "1.0";
import module namespace ds = "http://ds.iac3.eu/" at "file:///libreria.xquery";
declare namespace mt = "http://www.w3.org/1998/Math/MathML";
declare namespace mms = "urn:mathms";
     
(:------------------------------------------------------------------------------------------------------------------------------:)
(:------------------------------------------------Schema Parameters-------------------------------------------------------------:)
(:------------------------------------------------------------------------------------------------------------------------------:)
declare variable $schemaParams :=
let $params := ()
return $params;

declare variable $schema :=
   
(: ------------------------------------------------STEP1--------------------------------------------- :)
let $form1 :=Flux$i$u([$ijk]) = $flux_F$i$u([$u$n([$ijk])]);
let $impFdoc := ds:import("centeredDifference4","CD4", element spatialCoordinate{},())
let $timestep1 := ds:timestep(($impFdoc, $form1), 0)

(: --------------------------------------------------STEP2--------------------------------------------------- :)
let $form3 :=fluxAcc$u([$ijk]) = src([$u$n]);
let $form4 :=fluxAcc$u([$ijk]) = fluxAcc$u([$ijk]) + 
             $func_CD4$i([Flux$i$u,$ijk]);
let $imp2 := ds:import("RK3","RK3",(),())
let $form5 := k1$u([$ijk]) = RK3([1,$u$n,fluxAcc$u([$ijk]),0,0,$ijk]);
let $form6 := $u_p([$ijk])=$u$n([$ijk])+((0.333333333)*(K1$u([$ijk])));
let $timestep2 := ds:timestep(($form3, $form4, $imp2, $form5, $form6), 2)

(:-----------------------------------------------------------BOUNDARY1-----------------------------------------------------------:)
let $form8 := $u_p;
let $form9 := $u$n;
let $boundary1 := ds:boundary($form8, $form9, 2)

(:------------------------------------------------------TimeStep3----------------------------------------------------------------:)
let $form10 := $u_p;
let $auxEquations1 := ds:auxiliaryEquations($form10)
let $form11 := Flux$i$u([$ijk]) = $flux_F$i$u([$u_p([$ijk])]);
let $timestep3 := ds:timestep(($auxEquations1, $form11), 0)

(:---------------------------------------------------------timeStep4-------------------------------------------------------------:)
let $form13 := fluxAcc$u([$ijk])=src([$u_p]);
let $form14 := fluxAcc$u([$ijk]) = fluxAcc$u([$ijk]) 
                                     + $func_CD4$i([Flux$i$u,$ijk]);
let $form15 := k2$u([$ijk]) = RK3([2,$u$n,fluxAcc$u([$ijk]),0,0,$ijk]);
let $form16 :=   $u_pp([$ijk])=$u$n([$ijk])+((0.666666667)*(K2$u([$ijk])));  
let $timestep4 := ds:timestep(($form13, $form14, $form15, $form16), 2)

(:-----------------------------------------------------------boundary2----------------------------------------------------------:)
let $form18 := $u_pp;
let $form19 := $u_p;
let $boundary2 := ds:boundary($form18, $form19, 2)   

(:----------------------------------------------------------timeStep5-----------------------------------------------------------:)
let $form20 := $u_pp;
let $auxEquations2 := ds:auxiliaryEquations($form20)
let $form21 := Flux$i$u([$ijk]) = $flux_F$i$u([$u_pp([$ijk])]);
let $timestep5 := ds:timestep(($auxEquations2, $form21), 0)

 (:------------------------------------------------------timestep6---------------------------------------------------------:)
let $form23 :=fluxAcc$u([$ijk])=src([$u_pp]);
let $form24 := fluxAcc$u([$ijk]) = fluxAcc$u([$ijk]) + $func_CD4$i([Flux$i$u,$ijk]);
let $form25 :=  k3$u([$ijk]) = $func_RK3([3,$u$n,fluxAcc$u([$ijk]),0,0,$ijk]);
let $form26 := $u$n_1([$ijk])=$func_RK3([4,$u$n,0,k1$u,k3$u,$ijk]);       
let $timestep6 := ds:timestep(($form23, $form24, $form25, $form26), 2)
 
 (:------------------------------------------------------boundary3------------------------------------------------------------:)
 let $form28 := $u$n_1;
 let $form29 :=  $u$n;
 let $boundary3 := ds:boundary($form28, $form29, 2)
 
(:----------------------------------------------------timestep7-------------------------------------------------------------:)
let $form30 := $u$n_1;
let $auxEquations3 := ds:auxiliaryEquations($form30)
let $form31 := $u$n_1;
let $constraints1 := ds:constraints($form31)
let $timestep7 := ds:timestep(($auxEquations3, $constraints1), 0)

(:---------------------------------------------------main--------------------------------------------------------------------:)
let $instructionSet := element mms:instructionSet {
    $timestep1, 
    $timestep2, 
    $boundary1, 
    $timestep3, 
    $timestep4, 
    $boundary2, 
    $timestep5, 
    $timestep6, 
    $boundary3, 
    $timestep7    
}
let $schemaAux := ds:schema($instructionSet)
return
     $schemaAux;

document {
   <mms:discretizationSchema xmlns:mms="urn:mathms" xmlns:mt="http://www.w3.org/1998/Math/MathML">
   {$schemaParams, $schema}
   </mms:discretizationSchema>
}
