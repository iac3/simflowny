xquery version "1.0";
import module namespace ds = "http://ds.iac3.eu/" at "file:///libreria.xquery";
declare namespace mt = "http://www.w3.org/1998/Math/MathML";
declare namespace mms = "urn:mathms";
     
(:------------------------------------------------------------------------------------------------------------------------------:)
(:------------------------------------------------Schema Parameters-------------------------------------------------------------:)
(:------------------------------------------------------------------------------------------------------------------------------:)
declare variable $schemaParams :=
let $params := ()
return $params;

declare variable $schema :=
   
(: ------------------------------------------------STEP1--------------------------------------------- :)
let $form1 :=Flux$i$u([$ijk]) = $flux_F$i$u([$u$n_p([$ijk])]);
let $form2 :=speed$i$u([$ijk])=mcsc([$u$n_p]);
let $impFdoc := ds:import("FDOC","FDOC", element spatialCoordinate{},())
let $timestep1 := ds:timestep(($impFdoc, $form1, $form2), 0, 2)

(: --------------------------------------------------STEP2--------------------------------------------------- :)
let $form3 :=fluxAcc$u = src([$u$n_p([$ijk])]);
let $form4 :=fluxAcc$u = fluxAcc$u + 
             $func_FDOC$i([$u$n_p,Flux$i$u,Speed$i$u,$ijk]);
let $imp2 := ds:import("RK3","RK3",(),())
let $form5 := k1$u([$ijk]) = RK3([1,$u$n_p,fluxAcc$u,0,0,$ijk]);
let $form6 := $u_p([$ijk])=$u$n_p([$ijk])+((0.333333333)*(K1$u([$ijk])));
let $timestep2 := ds:timestep(($form3, $form4, $imp2, $form5, $form6), 2, 0)

(:-----------------------------------------------------------BOUNDARY1-----------------------------------------------------------:)
let $form8 := $u_p([$ijk]);
let $form9 := $u$n_p([$ijk]);
let $boundary1 := ds:boundary($form8, $form9, 2, "")

(:------------------------------------------------------TimeStep3----------------------------------------------------------------:)
let $form10 := $u_p([$ijk]);
let $auxEquations1 := ds:auxiliaryEquations($form10)
let $form11 := Flux$i$u([$ijk]) = $flux_F$i$u([$u_p([$ijk])]);
 let $form12 := Speed$i$u([$ijk]) = mcsc([$u_p([$ijk])]);
let $timestep3 := ds:timestep(($auxEquations1, $form11, $form12), 0, 2)

(:---------------------------------------------------------timeStep4-------------------------------------------------------------:)
let $form13 := fluxAcc$u=src([$u_p([$ijk])]);
let $form14 := fluxAcc$u = fluxAcc$u 
                                     + $func_FDOC$i([$u_p,Flux$i$u,Speed$i$u,$ijk]);   
let $form15 := k2$u([$ijk]) = RK3([2,$u$n_p,fluxAcc$u,0,0,$ijk]);
let $form16 :=   $u_pp([$ijk])=$u$n_p([$ijk])+((0.666666667)*(K2$u([$ijk])));  
let $timestep4 := ds:timestep(($form13, $form14, $form15, $form16), 2, 0)

(:-----------------------------------------------------------boundary2----------------------------------------------------------:)
let $form18 := $u_pp([$ijk]);
let $form19 := $u_p([$ijk]);
let $boundary2 := ds:boundary($form18, $form19, 2, "")   

(:----------------------------------------------------------timeStep5-----------------------------------------------------------:)
let $form20 := $u_pp([$ijk]);
let $auxEquations2 := ds:auxiliaryEquations($form20)
let $form21 := Flux$i$u([$ijk]) = $flux_F$i$u([$u_pp([$ijk])]);
let $form22 := Speed$i$u([$ijk]) = mcsc([$u_pp]);
let $timestep5 := ds:timestep(($auxEquations2, $form21, $form22), 0, 2)

 (:------------------------------------------------------timestep6---------------------------------------------------------:)
let $form23 :=fluxAcc$u=src([$u_pp([$ijk])]);
let $form24 := fluxAcc$u = fluxAcc$u + $func_FDOC$i([$u_pp,Flux$i$u,Speed$i$u,$ijk]);
let $form25 :=  k3$u([$ijk]) = $func_RK3([3,$u$n_p,fluxAcc$u,0,0,$ijk]);
let $form26 := $u$n([$ijk])=$func_RK3([4,$u$n_p,0,k1$u,k3$u,$ijk]);       
let $timestep6 := ds:timestep(($form23, $form24, $form25, $form26), 2, 0)
 
 (:------------------------------------------------------boundary3------------------------------------------------------------:)
 let $form28 := $u$n([$ijk]);
 let $form29 :=  $u_pp([$ijk]);
 let $boundary3 := ds:boundary($form28, $form29, 2, "")
 
(:----------------------------------------------------timestep7-------------------------------------------------------------:)
let $form30 := $u$n([$ijk]);
let $auxEquations3 := ds:auxiliaryEquations($form30)
let $form31 := $u$n([$ijk]);
let $constraints1 := ds:constraints($form31)
let $timestep7 := ds:timestep(($auxEquations3, $constraints1), 0, 0)

(:---------------------------------------------------main--------------------------------------------------------------------:)
let $instructionSet := element mms:instructionSet {
    $timestep1, 
    $timestep2, 
    $boundary1, 
    $timestep3, 
    $timestep4, 
    $boundary2, 
    $timestep5, 
    $timestep6, 
    $boundary3, 
    $timestep7    
}
let $schemaAux := ds:schema($instructionSet)
return
     $schemaAux;

document {
   <mms:discretizationSchema xmlns:mms="urn:mathms" xmlns:mt="http://www.w3.org/1998/Math/MathML">
   {$schemaParams, $schema}
   </mms:discretizationSchema>
}
