xquery version "1.0";
import module namespace ds = "http://ds.iac3.eu/" at "file:///libreria.xquery";
declare namespace mt = "http://www.w3.org/1998/Math/MathML";
declare namespace mms = "urn:mathms";

(:-------------------------------------------------------------------------------------------------:)
(:------------------------------------------------Schema Parameters--------------------------------:)
(:-------------------------------------------------------------------------------------------------:)
declare variable $schemaParams :=
let $param1 := ds:schemaParameter ("sigma", "sigma", (), ())
let $params := element mms:schemaParameters {$param1}
return $params;
(:-------------------------------------------------------------------------------------------------:)
(:------------------------------------------------Function Parameters------------------------------:)
(:-------------------------------------------------------------------------------------------------:)
declare variable $funcParams :=
let $func1 :=  ds:functionParameter ( ("u","flux",""), ("field","field","coordinates")) 
let $params := element mms:functionParameters {$func1}
return $params;


(:-------------------------------------------------------------------------------------------------:)
(:------------------------------------------------Transformation Rule------------------------------:)
(:-------------------------------------------------------------------------------------------------:)
declare variable $transformationRule := 

(:-------------------------------------------------------------------------------------------------:)
(:------------------------------------------------Main Program-------------------------------------:)
(:-------------------------------------------------------------------------------------------------:)
let $form1 :=(($p_sigma_*((u([ic([2])]) - u([ic([1])]))/6)) -
 ($p_sigma_*((u([ic([1])]) - u([$ijk]))/2))+ 
 ($p_sigma_*((u([$ijk]) - u([dc([1])]))/2)) - 
 ($p_sigma_*((u([dc([1])]) - u([dc([2])]))/6)) + 
 (4*((flux([ic([1])]))/3)) - 
 (4*((flux([dc([1])]))/3)) + 
 ((flux([dc([2])]))/6) - 
 ((flux([ic([2])]))/6)
)*(0.5/INC$i);
let $return := ds:return($form1)
let $instructionSet := element mms:instructionSet {
$return
}
(:----------------------------------------------transformation rule tag-----------------------------:)
let $trRuleAux := element mms:transformationRule {$instructionSet}
return
$trRuleAux;

document {
<mms:discretizationSchema xmlns:mms="urn:mathms" xmlns:mt="http://www.w3.org/1998/Math/MathML">
{$schemaParams, $funcParams, $transformationRule}
</mms:discretizationSchema>
}