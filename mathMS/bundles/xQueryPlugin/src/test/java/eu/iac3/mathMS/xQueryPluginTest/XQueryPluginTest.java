/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.xQueryPluginTest;

import eu.iac3.mathMS.xQueryPlugin.XQException;
import eu.iac3.mathMS.xQueryPlugin.XQueryData;
import eu.iac3.mathMS.xQueryPlugin.XQueryPluginImpl;
import eu.iac3.mathMS.xQueryPlugin.XQueryPluginUtils;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.jar.Manifest;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.springframework.osgi.test.AbstractConfigurableBundleCreatorTests;
import org.springframework.osgi.test.platform.OsgiPlatform;
import org.springframework.osgi.test.platform.Platforms;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

/** 
 * PhysicalModelsTest class is a junit testcase to
 * test the {@link eu.iac3.mathMS.xQueryPlugin.XQueryPlugin}.
 * The class has two single test with multiple assertions:
 * The first test checks the PhysicalModels methods with the
 * phisModel collection. It uses the modelWaves2ndOrder.xml file
 * The second test checks the PhysicalModels methods with the
 * phisCase collection. It uses the problemWaves.xml file
 
 * @author      ----
 * @version     ----
 */
public class XQueryPluginTest extends AbstractConfigurableBundleCreatorTests {
	/**
	 * removesWhitespace elements.
	 * @param e Element to remove the whitespaces
	 */
    public static void removeWhitespaceNodes(Element e) {
    	NodeList children = e.getChildNodes();
    	for (int i = children.getLength() - 1; i >= 0; i--) {
            Node child = children.item(i);
            if (child instanceof Text && ((Text) child).getData().trim().length() == 0) {
                e.removeChild(child);
            }
            else if (child instanceof Element) {
                removeWhitespaceNodes((Element) child);
            }
    	}
    }
	

    /**
     * An auxiliar function to read xml resource documents removing whitespaces.
     * 
     * @param bc    The bundle context
     * @param path  The file name path
     * @return      The resource as string
     */
    String readAsString(BundleContext bc, String path) {
        try {
            DocumentBuilderFactory dFactory = DocumentBuilderFactory.newInstance();
            dFactory.setNamespaceAware(true);
            dFactory.setIgnoringElementContentWhitespace(true);
            dFactory.setIgnoringComments(true);
            DocumentBuilder dBuilder = dFactory.newDocumentBuilder();
            Document document = dBuilder.parse(getClass().getResourceAsStream(path)); 
            //Getting the root element
            Element root = document.getDocumentElement();
            //Remove the whitespaces
            removeWhitespaceNodes(root);
            //Returning as a String
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            //StreamResult result = new StreamResult(new StringWriter());
            StreamResult result = new StreamResult(new ByteArrayOutputStream());
            DOMSource source = new DOMSource(document);
            transformer.transform(source, result);
            return new String(((ByteArrayOutputStream) result.getOutputStream()).toByteArray(), "UTF-8");
        }
        catch (Exception e) {
            fail("Error reading resource document");
            return null;
        }
    }
    
    /**
     * An auxiliar function to read a file as String.
     * 
     * @param is  The input stream of the file
     * @return      The resource as string
     * @throws UnsupportedEncodingException Error reading UTF-8
     */
    public static String convertStreamToString(InputStream is) throws UnsupportedEncodingException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
        String eol = System.getProperty("line.separator");
        StringBuilder sb = new StringBuilder();
        String line = null;
        List<String> lines =  new ArrayList<String>();
        try {
            int numLines = 0;
            while ((line = reader.readLine()) != null) {
                numLines++;
                lines.add(line);
            }
            int index = 0;
            while (index < lines.size()) {
            	if (index < (numLines - 1)) {
                    sb.append(lines.get(index) + eol);
            	}
            	else {
                    sb.append(lines.get(index));
            	}
            	index++;
            }
        } 
        catch (IOException e) {
            e.printStackTrace();
        } 
        finally {
            try {
                is.close();
            } 
            catch (IOException e) { 
                e.printStackTrace();
            }
        }
        return sb.toString();
    }
    
    /**
     * This method is used to set Felix as the 
     * testing platform.
     * Spring-osgi-test
     * 
     * @return OsgiPlatform
     */
    protected String getPlatformName() {
        return Platforms.FELIX;
    }
    
    /**
     * This method is used to set the system parameters
     * to the testing platform.
     * Spring-osgi-test
     * 
     * @return OsgiPlatform
     */
    protected OsgiPlatform createPlatform() {
        System.setProperty("file.encoding", "UTF-8");
        System.setProperty("xindice.db.home", "target/xindiceDB");
        System.setProperty("org.osgi.service.http.port", "8080");
    	System.setProperty("config", "net.sf.saxon.Configuration");
    	System.setProperty("platform", "net.sf.saxon.java.JavaPlatform");
    	System.setProperty("derby.system.home", "target/Derby");
        return super.createPlatform();
    } 
    
    /**
     * This method is used to set the boot 
     * delegation packages.
     * Spring-osgi-test
     * 
     * @return The boot delegation Packages
     */
    protected List<String> getBootDelegationPackages() { 
    	List<String> defaults = new ArrayList<String>(); 
    	defaults.add("java.*");
    	return defaults; 
    } 
    
    /**
     * This method is used to set the manifest.
     * Spring-osgi-test
     * 
     * @return The manifest for the test bundle
     */
    protected Manifest getManifest() {
    	
        // let the testing framework create/load the manifest
        Manifest mf = super.getManifest();
        mf.getMainAttributes().putValue(Constants.BUNDLE_REQUIREDEXECUTIONENVIRONMENT, "J2SE-1.5");
        mf.getMainAttributes().putValue(Constants.IMPORT_PACKAGE, ""        		
        		 + "javax.xml, javax.xml.transform, javax.xml.parsers, javax.xml.validation, " 
        		 + "javax.xml.transform.dom, javax.xml.transform.sax,"
        		 + " javax.xml.transform.stream, javax.xml.datatype, javax.xml.namespace, " 
        		 //+ "javax.xml.stream.events, javax.xml.stream, "
        		 + "javax.xml.xpath,org.apache.derby.jdbc,org.apache.xindice.client.xmldb,"
        		 + "org.apache.xindice.client.xmldb.services,org.apache.xindice.xml.dom,"
        		 + "org.osgi.framework;version=\"1.4.0\",org.osgi.service.log;version=\"1.3\","
        		 + "org.osgi.util.tracker;version=\"1.3\",org.w3c.dom,org.xml.sax,"
        		 + "org.xml.sax.ext,org.xml.sax.helpers,org.xmldb.api,org.xmldb.api.base,org.apache.xml.serialize,"
                         + "junit.framework, org.apache.commons.logging, org.springframework.util, org.springframework.osgi.service, "
                         + "org.springframework.osgi.util, org.springframework.osgi.test, org.springframework.context, "
        		 + "org.springframework.osgi.test.platform, "
        		 + "org.xmldb.api.modules,"
                         + "eu.iac3.mathMS.discretizationSchemasPlugin");
        		//+ "net.sf.saxon.s9api");
        //+ "net.sf.saxon.s9api");
        mf.getMainAttributes().putValue(Constants.BUNDLE_CLASSPATH, " .,lib/saxon9he.jar");
        mf.getMainAttributes().putValue("Private-Package", "" 
        		 + "javax.xml.xquery,lib,net.sf.saxon,net.sf.saxon.charcode,"
				 + "net.sf.saxon.codenorm,net.sf.saxon.dom,net.sf.saxon.event,"
				 + "net.sf.saxon.evpull,net.sf.saxon.expr,net.sf.saxon.functions,"
				 + "net.sf.saxon.instruct,net.sf.saxon.java,net.sf.saxon.number,net.sf.saxon.om,"
				 + "net.sf.saxon.pattern,net.sf.saxon.pull,net.sf.saxon.query,net.sf.saxon.regex,"
				 + "net.sf.saxon.s9api,net.sf.saxon.sort,net.sf.saxon.style,"
				 + "net.sf.saxon.sxpath,net.sf.saxon.tinytree,net.sf.saxon.trace,net.sf.saxon.trans,"
				 + "net.sf.saxon.tree,net.sf.saxon.type,net.sf.saxon.value,"
				 + "net.sf.saxon.xpath,net.sf.saxon.xqj");
        return mf;
    }
    
    /**
     * This method is used to set bundles
     * to be loaded in the test.
     * Spring-osgi-test
     * 
     * @return The bundles for the test
     */
    protected String[] getTestBundlesNames() {  
        return new String[] {  
            "org.apache.felix,org.osgi.compendium,1.2.0",
            "eu.iac3.thirdParty.bundles,xml-apis,1.0",
            "eu.iac3.thirdParty.bundles,xerces,2.6.0",
            "eu.iac3.thirdParty.bundles,xmlrpc,1.1",
            "eu.iac3.thirdParty.bundles.xmldb,xmldb-api,20030701",
            "eu.iac3.thirdParty.bundles.xmldb,xmldb-api-sdk,20030701",
            "eu.iac3.thirdParty.bundles.xmldb,xmldb-common,20030701",
            "eu.iac3.thirdParty.bundles.xmldb,xmldb-xupdate,20040205",
            "eu.iac3.thirdParty.bundles.xindice,xindice-lib,1.1", //Xindice lib
            //"org.apache.commons,com.springsourcet.sf.saxon.javae.org.apache.commons.logging,1.1.1",
            "org.ops4j.pax.web,pax-web-service,0.5.2",
            "org.ops4j.pax.web-extender,pax-web-ex-war,0.5.0",
            "org.ops4j.pax.web,pax-web-jsp,0.5.2",
            "eu.iac3.thirdParty.bundles.xindice,xindice,1.1", //Xindice bundle            
            //saxon
            "eu.iac3.thirdParty.bundles.saxon,saxon9he,9",
            "eu.iac3.mathMS,discretizationSchemasPlugin, 2.4",
            "org.apache.derby,com.springsource.org.apache.derby,10.4.1000003.648739"
        };  
    } 
    
    /**
     * Test the mathmlToDSML function.
     */
   /* public void testMathMLToDSML() {
    	
        try {            
            XQueryPluginImpl xp = new XQueryPluginImpl(bundleContext);
            String xmlDoc = readAsString(bundleContext, "/content_with_notations.xml");
            String xmlDocResp = convertStreamToString(getClass().getResourceAsStream("/content_dsml.xml"));
            String resp = xp.mathMLToDSML(xmlDoc);
            assertEquals(resp, xmlDocResp);
        } 
        catch (XQException e) {
            fail("error testing mathMLToDSML:" + e.toString());
        } 
        catch (UnsupportedEncodingException e) {
            fail("error testing mathMLToDSML:" + e.toString());
        }
        catch (Exception e) {
            e.printStackTrace();
            fail("error testing mathMLToDSML:" + e.toString());
        }
    }*/
    
    /**
     * Test the infixToDSML function.
     */
    
    /*public void testInfixToDSML() {         
        XQueryPluginImpl xp = new XQueryPluginImpl(bundleContext);
    	try {
            String infixFormula = convertStreamToString(getClass().getResourceAsStream("/formula_infix.txt"));			
            String resp = xp.infixToDSML(infixFormula);
            String formulaDSML = convertStreamToString(getClass().getResourceAsStream("/formula_dsml.xml"));
            assertEquals(resp, formulaDSML);
        } 
    	catch (UnsupportedEncodingException e) {
            fail("Error transforming infixFormula: " + e.toString());
        } 
    	catch (XQException e) {
            fail("Error transforming infixFormula: " + e.toString());
        }    	
    }*/
    
/**
 * Test xQueryToDSML function.
 */
    
    public void testXQueryToDSML() {
        XQueryPluginImpl xp = new XQueryPluginImpl(bundleContext);
    	try {
    	    XQueryPluginUtils utils = new XQueryPluginUtils();
    		//-------------------Reproducción del error que no deja pasar el test-----------------
    		/*
    		try {
    			
    			ClassLoader loader = Thread.currentThread().getContextClassLoader();
    			Class clase = loader.loadClass("net.sf.saxon.java.JavaPlatform");
    			System.out.println("to string" + clase.getCanonicalName() + clase.getName() + clase.getSimpleName());
				Class clase2 = Class.forName("net.sf.saxon.java.JavaPlatform");
				System.out.println("to string" + clase2.getCanonicalName() + clase2.getName()+ clase2.getSimpleName());
				net.sf.saxon.java.JavaPlatform plat = (net.sf.saxon.java.JavaPlatform)clase.newInstance();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
			*/
    		//-----------------fin reproduccion error---------------------------------------------
    	    if (bundleContext == null) {
                System.out.println("soy nulo");
            }
    		
            String xQueryInfix = convertStreamToString(getClass().getResourceAsStream("/cdpt7.xquery"));
            String dsml = xp.xQueryToDSML(xQueryInfix);
            FileWriter fichero = new FileWriter("MyFile.xml");
            PrintWriter pw = new PrintWriter(fichero);
            pw.write(dsml);
            fichero.close();
        } 
    	catch (Exception e) {
            fail("Error transforming infixFormula: " + e.toString());
        }  
    }
    
    /**
     * Test all the database functions:delXQuery, saveXQuery, modifyXQuery, getXQuery.
     */
    
    /*public void testDerby() {         
        XQueryPluginImpl xp = new XQueryPluginImpl(bundleContext);
        String head = "<mms:head xmlns:mms=\"urn:mathms\">"
            + "<mms:name>test xquery</mms:name>"
            + "<mms:id>testxquery</mms:id>"
            + "<mms:author>toni</mms:author>"
            + "<mms:version>1</mms:version>"
            + "<mms:date>28/09/2009</mms:date>"
            + "<mms:collection></mms:collection>"
            + "<mms:description></mms:description>"
            + "<mms:withXQuery>true</mms:withXQuery>"
            + "</mms:head>";
        
        //initializes the bbdd
		//deleteXquery
        try {
        	
            XQueryData data = xp.getXQuery("testxquery2");
            if (data != null) {
            	String[] id = new String[]{"testxquery2"};
                xp.delXQuery(id);
            }
            XQueryData data2 = xp.getXQuery("testxquery");
            if (data2 != null) {
            	String[] id = new String[]{"testxquery"};
                xp.delXQuery(id);
            }
        } 
        catch (XQException e) {
            System.out.println("");
        }
        String xQueryInfix = null;
        try {
            xQueryInfix = convertStreamToString(getClass().getResourceAsStream("/fdoc_mas_rk3_infix.xquery"));
        } 
        catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
        }
 		
        //saveXquery
    	try {
            xp.saveXQuery(xQueryInfix, head);
        } 
        catch (XQException e) {
            e.printStackTrace();
            fail("Error saveXQuery: " + e.toString());
        }
		
		//modifyXquery
        try {
            String head2 = "<mms:head xmlns:mms=\"urn:mathms\">"
		           + "<mms:name>test xquery</mms:name>"
		           + "<mms:id>testxquery2</mms:id>"
		           + "<mms:author>toni</mms:author>"
		           + "<mms:version>1</mms:version>"
		           + "<mms:date>28/09/2009</mms:date>"
		           + "<mms:collection></mms:collection>"
		           + "<mms:description></mms:description>"
		           + "<mms:withXQuery>true</mms:withXQuery>"
		          +  "</mms:head>";
            xp.modifyXQuery("testxquery", xQueryInfix, head2);
        } 
        catch (XQException e) {
            e.printStackTrace();
            fail("Error modifyXQuery: " + e.toString());
        }
    	 
 		//getXquery
        try {
            XQueryData data = xp.getXQuery("testxquery2");
            String xquery = data.getXquery();
            assertEquals(xQueryInfix, xquery);
        } 
     	catch (XQException e) {
            e.printStackTrace();
            fail("Error getXQuery: " + e.toString());
        }
		
		
		//deleteXquery
        try {
            String[] id = new String[]{"testxquery2"};
            xp.delXQuery(id);
        } 
        catch (XQException e) {
            e.printStackTrace();
            fail("Error delXQuery: " + e.toString());
        }
    }*/
    
    /**
     * Test of {@link eu.iac3.mathMS.xQueryPlugin.XQueryPlugin #getXPathModel(String, CollectionType)}.
     * Includes 3 tests:
     * 1 - Query that must return a known value
     * 2 - Query that must not return a value
     * 3 - An invalid query format. Must throw a MM001 exception
     * 
     */
    /*public void testGetXPathModel() {
        try {
            //Test configuration
            XQueryPluginImpl mm = new XQueryPluginImpl(bundleContext);
            mm.createCollection(CollectionType.physModel);
            String xmlDoc = readAsString(bundleContext, "/maxwell.xml");
            mm.addModel(xmlDoc, CollectionType.physModel);
            
            //Test 1: a query that must return a value
            String xpath = "//mms:version";
            String[] prb = mm.getXPathModel(xpath, CollectionType.physModel);
            assertEquals("<mms:version>1</mms:version>", prb[0]);
            
            //Test 2: a query that must not return a value
            xpath = "//noExistingResult";
            prb = mm.getXPathModel(xpath, CollectionType.physModel);
            assertNull(prb);
        }
        catch (XQException e) {
            fail("Error querying the database: " + e.toString());
        }
        //Test 3: XpathQuery not well formed
        try {
            XQueryPluginImpl mm = new XQueryPluginImpl(bundleContext);
            String xpath = "/*P$·:·";
            mm.getXPathModel(xpath, CollectionType.physModel);
            fail("MM001 should have been thrown");
        }
        catch (XQException e) {
            assertEquals(XQException.MM001, e.getErrorCode());
        }
    } */
    
   
}
