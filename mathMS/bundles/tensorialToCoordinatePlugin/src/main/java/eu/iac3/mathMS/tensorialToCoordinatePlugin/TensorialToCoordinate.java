package eu.iac3.mathMS.tensorialToCoordinatePlugin;
/** 
 * TensorialToCoordinate is an interface that provides the functionality 
 * to transform xml TensorialPhysicalModel to xml PhysicalModel
 * assuming the mathematical expressions are in MathML.
 * <p>
 * The interface has functions which accept XML as input and 
 * return xml as output.
 * 
 * @author      T.Artigues
 * @version     1.0
 */
public interface TensorialToCoordinate {
    /** 
     * It returns the xml physicalModel as a result to 
     * transform the tensorialPhysicalModel.
     *
     * @param tensorialPhysicalModel    The TensorialPhysicalModel xml to be transformed
     * @param spatialCoordinates        The coordinates list
     * @param timeCoordinate            The time coordinate
     * @return                          the PhysicalModel as a result to the transformation
     * @throws TCException              TC001 Not a valid XML Document
     *                                  TC002 XML document does not match XML Schema
     *                                  TC003 XML transform error
     *                                  TC004 Incorrect mathematical content in tensorial model
     *                                  TC00X External
     */
    String tensorialToCanonical(String tensorialPhysicalModel, String[] spatialCoordinates, String timeCoordinate) throws TCException;
    
    /**
     * Checks the formula for tensorial correctness.
     * @param formula           The formula
     * @param formula2          A second formula (only for parabolic terms)
     * @param freeIndexes       The number of free indexes of the formula (order of the field
     *                                                                            + 2 if parabolic term
     *                                                                            + 1 if flux
     *                                                                            + 0 otherwise)
     * @param coefOrder         The order of the formula coefficient (0 in case the formula is not a coefficient one) 
     * @param pt                if the formula is a parabolic term formula      
     * @return                  True if the formula is correct.
     * @throws TCException      TC004 Incorrect mathematical content in tensorial model
     *                          TC00X External
     */
    boolean checkTensorialFormula(String formula, String formula2, int freeIndexes, int coefOrder, boolean pt) throws TCException;
}
