package eu.iac3.mathMS.tensorialToCoordinatePlugin;

import java.util.Dictionary;
import java.util.Hashtable;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

/** 
 * Activator implements the BundleActivator interface 
 * This class is used to register the
 * {@link tensorialToCoordinatePlugin} as an OSGI Bundle webservice.
 * 
 * @author      ----
 * @version     ----
 */
public class Activator implements BundleActivator {
    private ServiceRegistration registration;

	/**
    * Method called by the OSGI manager when the 
    * PhysicalModels Bundle are started.
    * @param context        Bundle context
    * @throws Exception     Bundle exception
    */
    @SuppressWarnings("unchecked")
    public void start(BundleContext  context) throws Exception {
    	
    	String port;
    	
    	//Getting the actual http port
        if (context.getProperty("org.osgi.service.http.port").equals("9090")) {
        	//Default port
            port = "9091";
    	} 
        else {
            port = "9090";
    	}	
    	//Registration as a web service.
        @SuppressWarnings("rawtypes")
        Dictionary props = new Hashtable();
        props.put("osgi.remote.interfaces", "*");
        props.put("osgi.remote.configuration.type", "pojo");
        props.put("osgi.remote.configuration.pojo.address", "http://localhost:" + port + "/TensorialToCoordinate");
        registration = context.registerService(TensorialToCoordinate.class.getName(), new TensorialToCoordinateImpl(context, false), props);
        
    }
    /**
     * Method called by the OSGI manager when the 
     * DiscModelsPlugin Bundle are stopped.
     * @param context       Bundle context
     * @throws Exception    Bundle exception
     */
    public void stop(BundleContext context) throws Exception {
    	registration.unregister();
    }
}
