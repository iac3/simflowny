package eu.iac3.mathMS.tensorialToCoordinatePlugin;

import eu.iac3.mathMS.tensorialToCoordinatePlugin.utils.SymmetryAction;
import eu.iac3.mathMS.tensorialToCoordinatePlugin.utils.TCUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;

import org.osgi.framework.BundleContext;
import org.osgi.service.log.LogService;
import org.osgi.util.tracker.ServiceTracker;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


/** 
 * TensorialToCoordinateImpl implements the TensorialToCoordinate interface.
 * @author      T.Artigues
 * @version     1.0
 */
public class TensorialToCoordinateImpl implements TensorialToCoordinate {
	
    static LogService logservice = null;
    static BundleContext context;
    static TCUtils util;
    static String mmsUri = "urn:mathms";
    static String mtUri = "http://www.w3.org/1998/Math/MathML";   
    boolean test;
    static final int THREE = 3;
	/**
     * Constructor.
     * 
     * @param bc  The bundle context
     * @param test  if the execution is a test (problem OSGI-system properties-Saxon with DOM)
     */
    public TensorialToCoordinateImpl(BundleContext bc, boolean test) {
    	super();
    	
    	//Getting the bundle context
    	context = bc;
    	
    	if (context != null) {
    		//Getting the log service
    	    ServiceTracker logServiceTracker = new ServiceTracker(context, org.osgi.service.log.LogService.class.getName(), null);
    	    logServiceTracker.open();
    	    logservice = (LogService) logServiceTracker.getService();
    	}
        
        this.test = test;
        util = new TCUtils(test);
    }
    
    /** 
     * It returns the xml physicalModel as a result to 
     * transform the tensorialPhysicalModel.
     *
     * @param tensPhysModel             The TensorialPhysicalModel xml to be transformed
     * @param spatialCoordinates        The coordinates list
     * @param timeCoordinate            The time coordinate
     * @return                          the PhysicalModel as a result to the transformation
     * @throws TCException              TC001 Not a valid XML Document
     *                                  TC002 XML document does not match XML Schema
     *                                  TC003 XML transform error
     *                                  TC004 Incorrect mathematical content in tensorial model
     *                                  TC00X External error
     */
    public String tensorialToCanonical(String tensPhysModel, String[] spatialCoordinates, String timeCoordinate) throws TCException {
        Document result = null;
        if (logservice != null) {
            logservice.log(LogService.LOG_INFO, "Discretizing problem");
        }
        //Validation of the problem
        TCUtils.schemaValidation(tensPhysModel, "tensPhysModel.xsd");
        
        //Checks the formulas consistency
        TCUtils.checkConsistency(TCUtils.stringToDom(tensPhysModel));
        
        Document tensModel = TCUtils.preprocess(TCUtils.stringToDom(tensPhysModel), spatialCoordinates);
        
        //Creation of the model result skeleton
        result = TCUtils.builder.newDocument();
        
        //---------------------Root--------------------
        String rootName = "physicalModel";
        Element root = TCUtils.createElement(result, mmsUri, rootName);
        result.appendChild(root);
        
        //----------------------Header-----------------
        makeHeader(result, tensModel);
        
        //----------------------Coordinates-------------
        makeCoordinates(result, spatialCoordinates, timeCoordinate);
        
        //Calculate Symmetries
        HashMap<String, SymmetryAction> symmetricVars = TCUtils.calculateSymmetries(tensModel, spatialCoordinates);
        
        //---------------------TensorialFields--------------------------
        //the list of fields and tensors obtained in the transformation
        List<Node> fields = new ArrayList<Node>();
        List<Node> tensors = new ArrayList<Node>();
        //the evolutionEquations obtained in the transformation
        List<Node> evolutionEquations = makeTensorialFields(result, tensModel, spatialCoordinates, timeCoordinate, fields, tensors, symmetricVars);
        
        //--------------------TensorialAuxiliarFields---------------------
        //the list of auxiliar fields obtained in the transformation
        List<Node> auxFields = new ArrayList<Node>();
        //the auxiliarEquations obtained in the transformation
        List<Node> auxiliarEquations = new ArrayList<Node>();
        makeTensorialAuxiliarFields(result, tensModel, spatialCoordinates,
        		            auxFields, auxiliarEquations, symmetricVars);
                    
        //-------------------Parameters-----------------------------------
        List<Node> parameters = makeParameters(result, tensModel, spatialCoordinates);
        
        //add the fields to the response
        addElements(root, fields);
        //add the auxFields to the response
        addElements(root, auxFields);
        //Create the tensor information
        addElements(root, tensors);
        //add the parameters to the response
        addElements(root, parameters);
        
        //-------------------Constraints-------------------------------
        //the evolutionEquations obtained in the transformation
        List<Node> constraintEquations = new ArrayList<Node>();
        makeConstraints(result, tensModel, spatialCoordinates, constraintEquations, symmetricVars);
 
        //---------------------equationSet------------------------------
        Element equationSet = TCUtils.createElement(result, mmsUri, "equationSet");
        root.appendChild(equationSet);
        //Add the evolution equations to the equationSet
        addElements(equationSet, evolutionEquations);
        //Add the constraint equations to the equationSet
        Element constr = TCUtils.createElement(result, mmsUri, "constraint");
        equationSet.appendChild(constr);
        addElements(constr, constraintEquations);
        //Add the auxiliar equations to the equationSet
        addElements(equationSet, auxiliarEquations);
        //Add the characteristic decomposition to the equationSet
        makeCharDecomposition(equationSet, tensModel, spatialCoordinates, symmetricVars);
        
        String response = TCUtils.indent(TCUtils.domToString(TCUtils.postProcess(result)));
      //  System.out.println("RESPONSE:" + response);
        return response;
    }
	
	
	/**
     * Create the header tag for the model document.
     * 
     * @param result        The document to construct the header
     * @param problem       The problem that the header must be constructed from
     */
    private void makeHeader(Document result, Document problem) {
        //copy the entire header
        Element root = result.getDocumentElement();
        Node headerOry = problem.getDocumentElement().getFirstChild();
        Node header = result.importNode(headerOry.cloneNode(true), true);        
        root.appendChild(header);
    }
    
	/**
     * Create the coordinates tag for the model document.
     * 
     * @param result        The document to construct the header
     * @param spaceCoord    The spatial coordinates
     * @param timeCoord     The time coordinate
     */
    private void makeCoordinates(Document result, String[] spaceCoord, String timeCoord) {    	
    	Element root = result.getDocumentElement();
        Element coordinates = TCUtils.createElement(result, mmsUri, "coordinates");
        root.appendChild(coordinates);
        for (int i = 0; spaceCoord != null && i < spaceCoord.length; i++) {
            Element coordinate = TCUtils.createElement(result, mmsUri, "spatialCoordinate");
            coordinate.setTextContent(spaceCoord[i]);
            coordinates.appendChild(coordinate);
        }
        Element coordinate = TCUtils.createElement(result, mmsUri, "timeCoordinate");
        coordinate.setTextContent(timeCoord);
        coordinates.appendChild(coordinate);                
    }
    
    /**
     * Transforms the tensorial fields and equations to canonical ones.
     * @param result                The canonical model
     * @param tensModel             The tensorial model
     * @param spaceCoordinates      The spatial coordinates
     * @param timeCoordinate        The time coordinate
     * @param fields                The fields
     * @param tensors               The tensors
     * @param symmetricVars         Information about the symmetries in variables
     * @return                      The list of evolution equations
     * @throws TCException          TC001   Not a valid XML Document
     *                              TC00X External error
     */
    private List<Node> makeTensorialFields(Document result, Document tensModel, String[] spaceCoordinates, String timeCoordinate, 
            List<Node> fields, List<Node> tensors, HashMap<String, SymmetryAction> symmetricVars) throws TCException {
        List<Node> evolutionEquations = new ArrayList<Node>();
    	//Prefixes of the root element
        String prefix = tensModel.getDocumentElement().getPrefix();
        NodeList tensFieldsList =  TCUtils.find(tensModel.getDocumentElement(), 
                "/" + prefix + ":tensorialPhysicalModel" + "/" + prefix + ":tensorialField");
        for (int i = 0; i < tensFieldsList.getLength(); i++) {
            Node tensField  = tensFieldsList.item(i);
            //The order of the tensorial field
            String order = tensField.getChildNodes().item(1).getTextContent();            	
            int ord = Integer.parseInt(order);
            Element tensor = TCUtils.createElement(result, mmsUri, "tensor");
            Element name = TCUtils.createElement(result, mmsUri, "name");
            name.setTextContent(tensField.getFirstChild().getTextContent());
            Element orderE = TCUtils.createElement(result, mmsUri, "order");
            orderE.setTextContent(order);
            tensor.appendChild(name);
            tensor.appendChild(orderE);
            //obtains his evolutionEquation
            NodeList tensorialPDEEqList = TCUtils.find(tensModel.getDocumentElement(), 
            		"//" + prefix + ":tensorialPDE[" + prefix + ":field = '" + tensField.getChildNodes().item(0).getTextContent() + "']");
            if (tensorialPDEEqList == null || tensorialPDEEqList.getLength() == 0) {
            	throw new TCException(TCException.TC001, "There is one evolution tensorial field with no associated evolution equation");
            }
            Node tensPDEEq = tensorialPDEEqList.item(0);
    	    //extract the indexNumber
    	    String idxNum = tensPDEEq.getChildNodes().item(1).getTextContent();
    	    int indexNumber = Integer.parseInt(idxNum);
        	//-------------------NEW---------------------------------
        	//--Transform each tensorial flux to canonical flux
        	//Now replaces the summation indexes.
    	    NodeList tensFlux = TCUtils.find(tensPDEEq, ".//" + prefix + ":flux");
    	    Node canFlux = null;
    	    List<Node> fluxes = new ArrayList<Node>();
    	    for (int k = 0; k < tensFlux.getLength(); k++) {
    	    	//Transform a tensorial formula from a flux to a canonical mathml
    	    	Node math = tensFlux.item(k).getChildNodes().item(1).getFirstChild();
    	       	String derIdx = tensFlux.item(k).getChildNodes().item(0).getTextContent();
    	    	canFlux = transformFormula(math, derIdx, null, spaceCoordinates, ord, indexNumber, true);
    	    	//extracts his components
    	    	NodeList canFluxes = TCUtils.find(canFlux, "//mt:semantics[mt:annotation-xml/properties/id = 'term']");
    	        for (int i2 = 0; i2 < canFluxes.getLength(); i2++) {
    	            Node flux = canFluxes.item(i2);
    	            fluxes.add(flux);           	    		
    	    	}            	    	
    	    }
    	    //--Transform the summation indexes of the source
    	    Node source = TCUtils.find(tensPDEEq, "./" + prefix + ":source").item(0);
    	    Node mathSource = source.getFirstChild().getFirstChild();
    	    Node sourceAux = transformFormula(mathSource, null, null, spaceCoordinates, ord, indexNumber, false);
    	    sourceAux = sourceAux.getFirstChild().getFirstChild();
    	    //Transform the summation indexes of parabolic terms
            //The shared summation indexes creates new parabolic terms
    	    NodeList tensPT = TCUtils.separateSharedSummIndex(tensModel, 
    	            TCUtils.find(tensPDEEq, ".//" + prefix + ":parabolicTerm"), spaceCoordinates);
            //We need a list of list, the P and Q terms can not be mixed
            ArrayList<ArrayList<Node>> pTerms = new ArrayList<ArrayList<Node>>();
            ArrayList<ArrayList<Node>> qTerms = new ArrayList<ArrayList<Node>>();
            transformParabolicTerms(tensPT, pTerms, qTerms, spaceCoordinates, ord, indexNumber);
            
    	    //grouping
    		//create all the possible combinations
    	    List<String[]> combList = TCUtils.generateCoordVariations(spaceCoordinates, ord);
        	//for each combination creates a new field 
    	    List<Node> fluxesWithComb = null;
    	    for (int j = 0; j < combList.size(); j++) {
                String[] comb = combList.get(j);            		
                //----------new field--------------------
                String fieldName = TCUtils.fieldName(tensField, comb); 
                if (!symmetricVars.containsKey(fieldName)) {
                    Element field = TCUtils.createElement(result, mmsUri, "field");
                    field.setTextContent(fieldName);
                    fields.add(field);
                    tensor.appendChild(field.cloneNode(true));
                    if (ord > 0) {
                        tensors.add(tensor);
                    }
                    //----------normalize and group fluxes---------------
                    //reemplazamos en cada flujo los indices libres por la combinacion actual
                    fluxesWithComb = util.replaceIndexes(fluxes, comb);
                    //para cada flujo calcular, simplificar y normalizar, utilizando la xsl
                    fluxesWithComb = util.normalizeFluxes(fluxesWithComb, symmetricVars);
                    //agrupamos segun el indice de la coordenada
                    Hashtable<String, List<Node>> groups = util.groupFluxes(fluxesWithComb);
                    //---------normalize source--------------
                    //reemplazamos en cada flujo los indices libres por la combinacion actual
                    Node sourceFinal = util.replaceIndexes(sourceAux, comb);
                    //para cada fuente calcular, simplificar y normalizar, utilizando la xsl
                    sourceFinal = util.normalizeFormula(sourceFinal, symmetricVars);
                    //---------normalize parabolic terms--------------
                    List<Hashtable<String, List<Node>>> pGroups = new ArrayList<Hashtable<String, List<Node>>>();
                    List<Hashtable<String, List<Node>>> qGroups = new ArrayList<Hashtable<String, List<Node>>>();
                    replaceNormaliceAndGroupPT(pGroups, qGroups, pTerms, qTerms, spaceCoordinates, comb, symmetricVars);
                    //------------new Evolution Equation--------------
                    Element evEquation = TCUtils.createElement(result, mmsUri, "evolutionEquation");
                    evolutionEquations.add(evEquation);
                    //evEquation name
                    Element evName = TCUtils.createElement(result, mmsUri, "equationName");
                    String eqName = tensPDEEq.getParentNode().getChildNodes().item(0).getTextContent();
                    eqName = addCombToName(eqName, comb, tensField);
                    evName.setTextContent(eqName);
                    evEquation.appendChild(evName);
                    //add the canonicalPDE to the evolution equation
                    Element canPDE = TCUtils.createElement(result, mmsUri, "canonicalPDE");
                    evEquation.appendChild(canPDE);
                    //field name in the canonical pde
                    Element fName = TCUtils.createElement(result, mmsUri, "field");
                    fName.setTextContent(fieldName);
                    canPDE.appendChild(fName);
                    //add time coordinate to the canonicalPDE
                    addTimeCoordinate(result, canPDE, timeCoordinate);
                    //cffd
                    Element cffd = TCUtils.createElement(result, mmsUri, "CFFD");
                    //canPDE.appendChild(cffd);
                    for (int k = 0; k < spaceCoordinates.length; k++) {
                        if (groups.containsKey(spaceCoordinates[k])) {
                            makeFlux(result, cffd, groups, spaceCoordinates[k]);
                        }
                    }
                    canPDE.appendChild(cffd);
                    //add the source
                    source = TCUtils.createElement(result, mmsUri, "source");
                    Element mathmlSource = TCUtils.createElement(result, mmsUri, "mathML");
                    source.appendChild(mathmlSource);
                    sourceFinal = TCUtils.find(sourceFinal, "*[1]").item(0);
                    Element math = TCUtils.createElement(result, mtUri, "math");
                    mathmlSource.appendChild(math);
                    Node sourceImported = result.importNode(sourceFinal, true);  
                    math.appendChild(sourceImported);
                    canPDE.appendChild(source);
                    //add parabolic terms
                    if (pGroups.size() > 0 && qGroups.size() > 0) {
                        Element pts = TCUtils.createElement(result, mmsUri, "parabolicTerms");
                        for (int n = 0; n < pGroups.size(); n++) {
                            for (int k = 0; k < spaceCoordinates.length; k++) {
                                for (int l = 0; l < spaceCoordinates.length; l++) {
                                    if (pGroups.get(n).containsKey(spaceCoordinates[k] + "&" + spaceCoordinates[l])
                                            && qGroups.get(n).containsKey(spaceCoordinates[k] + "&" + spaceCoordinates[l])) {
                                        makePT(result, pts, pGroups.get(n), qGroups.get(n), spaceCoordinates[k], spaceCoordinates[l]);
                                    }
                                }
                            }
                        }
                        canPDE.appendChild(pts);
                    }
                }
    	    }
        }
        return evolutionEquations;
    }

    /**
     * Transform the parabolic terms to independent terms.
     * @param tensPT                The tensorial parabolic terms
     * @param pTerms                The array to store the independent P terms
     * @param qTerms                The array to store the independent Q terms
     * @param spaceCoordinates      The spatial coordinates
     * @param ord                   The order of the equation
     * @param indexNumber           The number of indexes used
     * @throws TCException          TC00X External error
     */
    private void transformParabolicTerms(NodeList tensPT, ArrayList<ArrayList<Node>> pTerms, ArrayList<ArrayList<Node>> qTerms,
            String[] spaceCoordinates, int ord, int indexNumber) throws TCException {
        for (int k = 0; k < tensPT.getLength(); k++) {
            ArrayList<Node> pTermList = new ArrayList<Node>();
            ArrayList<Node> qTermList = new ArrayList<Node>();
            //Transform a tensorial formula from pt. to canonical mathml
            Node qTerm = tensPT.item(k).getChildNodes().item(2).getFirstChild().getFirstChild();
            Node pTerm = tensPT.item(k).getChildNodes().item(THREE).getFirstChild().getFirstChild();
            String derIdx = tensPT.item(k).getChildNodes().item(0).getTextContent();
            String secDerIdx = tensPT.item(k).getChildNodes().item(1).getTextContent();
            Node canQ = transformFormula(qTerm, derIdx, secDerIdx, spaceCoordinates, ord, indexNumber, false);
            Node canP = transformFormula(pTerm, derIdx, secDerIdx, spaceCoordinates, ord, indexNumber, false);
            //extracts his components
            NodeList canList = TCUtils.find(canQ, "//mt:semantics[mt:annotation-xml/properties/id = 'term']");
            if (canList.getLength() == 0) {
                qTermList.add(canQ.getFirstChild().getFirstChild());
            }
            for (int i2 = 0; i2 < canList.getLength(); i2++) {
                qTermList.add(canList.item(i2));                           
            }   
            canList = TCUtils.find(canP, "//mt:semantics[mt:annotation-xml/properties/id = 'term']");
            if (canList.getLength() == 0) {
                pTermList.add(canP.getFirstChild().getFirstChild());
            }
            for (int i2 = 0; i2 < canList.getLength(); i2++) {
                pTermList.add(canList.item(i2));                           
            } 
            pTerms.add(pTermList);
            qTerms.add(qTermList);
        }
    }
    
    /**
     * Replaces the index, normalices and group the parabolic terms.
     * @param pGroups                   The list to group de P terms
     * @param qGroups                   The list to group de Q terms
     * @param pTerms                    The transformed P terms
     * @param qTerms                    The transformed Q terms
     * @param spatialCoordinates        The spatial coordinates
     * @param comb                      The actual combination
     * @param symmetricVars             The symmetric variables
     * @throws TCException              TC00X External error
     */
    private void replaceNormaliceAndGroupPT(List<Hashtable<String, List<Node>>> pGroups, List<Hashtable<String, List<Node>>> qGroups, 
            ArrayList<ArrayList<Node>> pTerms, ArrayList<ArrayList<Node>> qTerms, String[] spatialCoordinates, String[] comb, 
            HashMap<String, SymmetryAction> symmetricVars) throws TCException {
        //reemplazamos en cada término los indices libres por la combinacion actual
        List<List<Node>> pWithComb = new ArrayList<List<Node>>();
        List<List<Node>> qWithComb = new ArrayList<List<Node>>();
        for (int k = 0; k < pTerms.size(); k++) {
            pWithComb.add(util.replaceIndexes(pTerms.get(k), comb));
            qWithComb.add(util.replaceIndexes(qTerms.get(k), comb));
        }
        //para cada término calcular, simplificar y normalizar, utilizando la xsl
        List<List<Node>> pNorm = new ArrayList<List<Node>>();
        List<List<Node>> qNorm = new ArrayList<List<Node>>();
        for (int k = 0; k < pWithComb.size(); k++) {
            pNorm.add(util.normalizePT(pWithComb.get(k), symmetricVars));
            qNorm.add(util.normalizePT(qWithComb.get(k), symmetricVars));
        }
        
        //agrupamos segun el indice de la coordenada
        for (int k = 0; k < pNorm.size(); k++) {
            pGroups.add(util.groupPT(pNorm.get(k), spatialCoordinates));
            qGroups.add(util.groupPT(qNorm.get(k), spatialCoordinates));
        }
    }
    
    /**
     * Creates the canoniocal parameters from the tensorial ones.
     * @param result                The canonical model
     * @param tensModel             The tensorial model
     * @param spatialCoordinates    The coordinates list
     * @return                      The parameters
     * @throws TCException          TC00X External error
     */
    private List<Node> makeParameters(Document result, Document tensModel, String[] spatialCoordinates) throws TCException {
    	//Prefixes of the root element
        String prefix = tensModel.getDocumentElement().getPrefix();
        NodeList parametersList =  TCUtils.find(tensModel.getDocumentElement(), 
                "/" + prefix + ":tensorialPhysicalModel" + "/" + prefix + ":parameter");
        List<Node> parameters = new ArrayList<Node>();
        for (int i = 0; i < parametersList.getLength(); i++) {
            Node parameter  = parametersList.item(i);
            String parameterName = parameter.getFirstChild().getTextContent();
            String parameterType = parameter.getFirstChild().getNextSibling().getTextContent();
            int ord = Integer.parseInt(parameter.getChildNodes().item(2).getTextContent());
            
            List<String[]> combList = TCUtils.generateCoordVariations(spatialCoordinates, ord);
            
            //for each combination creates a new field
            for (int j = 0; j < combList.size(); j++) {
                String[] comb = combList.get(j);
                String newParameterName = TCUtils.parameterName(parameterName, comb); 
                
                Element parameterNew = TCUtils.createElement(result, mmsUri, "parameter");
                Element name = TCUtils.createElement(result, mmsUri, "name");
                name.setTextContent(newParameterName);
                parameterNew.appendChild(name);
                Element type = TCUtils.createElement(result, mmsUri, "type");
                type.setTextContent(parameterType);
                parameterNew.appendChild(type);
                parameters.add(parameterNew);     
            }
        }
    	return parameters;
    }
    
    /**
     * Transforms the tensorial constrint equations to canonical ones.
     * @param result                The canonical model
     * @param tensModel             The tensorial model
     * @param spaceCoordinates      The spatial coordinates
     * @param constraintEquations   The constraint equations
     * @param symmetricVars         Information about the symmetries in variables
     * @throws TCException          TC001   Not a valid XML Document
     *                              TC00X External error
     */
    private void makeConstraints(Document result, Document tensModel, String[] spaceCoordinates, List<Node> constraintEquations, 
            HashMap<String, SymmetryAction> symmetricVars) throws TCException {
        //Prefixes of the root element
        String prefix = tensModel.getDocumentElement().getPrefix();
        
        NodeList constraintsList =  TCUtils.find(tensModel.getDocumentElement(), "/" + prefix + ":tensorialPhysicalModel" + "/" + prefix 
                + ":equationSet" + "/" + prefix + ":constraint" + "/" + prefix + ":mathematicalConstraint");
        		                                 
        for (int i = 0; i < constraintsList.getLength(); i++) {
            Node constraint  = constraintsList.item(i);
            //The order of the tensorial field
            String order = constraint.getChildNodes().item(1).getTextContent();            	
            int ord = Integer.parseInt(order);
            
            //obtains his equation
            Node tensPDEEq = constraint.getChildNodes().item(2);
        	
    	    //extract the indexNumber
    	    String idxNum = tensPDEEq.getChildNodes().item(0).getTextContent();
    	    int indexNumber = Integer.parseInt(idxNum);

        	//--Transform each tensorial flux to canonical flux
        	//Replace the summation indexes.
    	    NodeList tensFlux = TCUtils.find(tensPDEEq, ".//" + prefix + ":flux");
    	    Node canFlux = null;
    	    List<Node> fluxes = new ArrayList<Node>();
    	    for (int k = 0; k < tensFlux.getLength(); k++) {
    	    	//Transform a tensorial formula from a flux to a canonical mathml
    	    	Node math = tensFlux.item(k).getChildNodes().item(1).getFirstChild();
    	       	String derIdx = tensFlux.item(k).getChildNodes().item(0).getTextContent();
    	    	canFlux = transformFormula(math, derIdx, null, spaceCoordinates, ord, indexNumber, true);
    	    	//extracts his components
    	    	NodeList canFluxes = TCUtils.find(canFlux, "//mt:semantics[mt:annotation-xml/properties/id = 'term']");
    	        for (int i2 = 0; i2 < canFluxes.getLength(); i2++) {
    	            Node flux = canFluxes.item(i2);
    	            fluxes.add(flux);           	    		
    	    	}            	    	
    	    }
    	    
    	    //--Transform the summation indexes of the source
    	    Node source = TCUtils.find(tensPDEEq, "./" + prefix + ":source").item(0);
    	    Node mathSource = source.getFirstChild().getFirstChild();
    	    Node sourceAux = transformFormula(mathSource, null, null, spaceCoordinates, ord, indexNumber, false);
    	    sourceAux = sourceAux.getFirstChild().getFirstChild();
    	    
    	    //grouping
    		//create all the possible combinations
    	    List<String[]> combList = TCUtils.generateCoordVariations(spaceCoordinates, ord);        	        	
        	//for each combination creates a new constraint 
    	    List<Node> fluxesWithComb = null;
    	    for (int j = 0; j < combList.size(); j++) {
                String[] comb = combList.get(j);
                
                //----------normalize and group fluxes---------------
                //reemplazamos en cada flujo los indices libres por la combinacion actual
                fluxesWithComb = util.replaceIndexes(fluxes, comb);
                //para cada flujo calcular, simplificar y normalizar, utilizando la xsl
                fluxesWithComb = util.normalizeFluxes(fluxesWithComb, symmetricVars);
                //agrupamos segun el indice de la coordenada
                Hashtable<String, List<Node>> groups = util.groupFluxes(fluxesWithComb);
                
                //---------normalize source--------------
                //reemplazamos en cada flujo los indices libres por la combinacion actual
                Node sourceFinal = util.replaceIndexes(sourceAux, comb);
                //para cada flujo calcular, simplificar y normalizar, utilizando la xsl
                sourceFinal = util.normalizeFormula(sourceFinal, symmetricVars);
                
                //------------new Constraint--------------
                Element coEquation = TCUtils.createElement(result, mmsUri, "mathematicalConstraint");
                constraintEquations.add(coEquation);
                //evEquation name
                Element evName = TCUtils.createElement(result, mmsUri, "equationName");
                String eqName = tensPDEEq.getParentNode().getChildNodes().item(0).getTextContent();
                eqName = addCombToName(eqName, comb, null);
                evName.setTextContent(eqName);
                coEquation.appendChild(evName);
                //add the canonicalPDE to the evolution equation
                Element canPDE = TCUtils.createElement(result, mmsUri, "canonicalPDE");
                coEquation.appendChild(canPDE);
                //cffd
                Element cffd = TCUtils.createElement(result, mmsUri, "CFFD");
                //canPDE.appendChild(cffd);
                for (int k = 0; k < spaceCoordinates.length; k++) {
                    if (groups.containsKey(spaceCoordinates[k])) {
                        makeFlux(result, cffd, groups, spaceCoordinates[k]);
                    }
                }
                canPDE.appendChild(cffd);	
                //adds the source
                source = TCUtils.createElement(result, mmsUri, "source");
                Element mathmlSource = TCUtils.createElement(result, mmsUri, "mathML");
                source.appendChild(mathmlSource);
                sourceFinal = TCUtils.find(sourceFinal, "*[1]").item(0);
                Element math = TCUtils.createElement(result, mtUri, "math");
                mathmlSource.appendChild(math);
                Node sourceImported = result.importNode(sourceFinal, true);  
                math.appendChild(sourceImported);
                canPDE.appendChild(source);
    	    }
        }
    }
    
    /**
     * Adds some elements to a node.
     * @param root          The node
     * @param elements      The elements
     */
    private void addElements(Element root, List<Node> elements) {
    	for (int i = 0; i < elements.size(); i++) {
    	    root.appendChild(elements.get(i));
    	}
    }
    
    /**
     * Adds the coordinate combination to a name (equation...).
     * @param name          The name
     * @param comb          The combination
     * @param tensField     The field
     * @return              The new name
     */
    private String addCombToName(String name, String[] comb, Node tensField) {
    	String nameAux = new String(name);
    	String fieldName = null;
    	boolean addToEnd = true;
    	
    	//if comb es cero entonces devolvemos el nombre tal qual
    	if (comb.length > 0) {
    	    //si el nombre del field no es null entonces lo buscamos en el texto para añadirle comb
    	    if (tensField != null) {
    	        nameAux = "";
    	        fieldName = tensField.getChildNodes().item(0).getTextContent();
    	        String[] words = name.split(" ");      		      
    	        for (int i = 0; i < words.length; i++) {
    	            String word = words[i];
    	            nameAux = nameAux + word;    			  
    	            if (word.equals(fieldName)) {  
    	                addToEnd = false;
    	                for (int k = 0; k < comb.length; k++) {
    	                    nameAux = nameAux + comb[k];	
    	                }
    	            }
    	            if ((i + 1) < words.length) {
    	                nameAux = nameAux + " ";
    	            }
    	        }  
    	    }
    	    //si el nombre del field es null, o el field no esta, entonces añadimos comb al final del texto
    	    if (addToEnd) {    		   
    	        nameAux = nameAux + " ";
    	        for (int k = 0; k < comb.length; k++) {
    	            nameAux = nameAux + comb[k];	
    	        }
    	    }           	
    	}
    	    	
    	return nameAux;
    }
    
    /**
     * Create the time coordinate.
     * 
     * @param result        The document to construct the header
     * @param canPDE        The canonicalPDE
     * @param timeCoord     The time coordinate
     */
    private void addTimeCoordinate(Document result, Node canPDE, String timeCoord) {
        Element bcd = TCUtils.createElement(result, mmsUri, "BCD");
        Element coord = TCUtils.createElement(result, mmsUri, "coordinate");
        coord.setTextContent(timeCoord);
        bcd.appendChild(coord);
        canPDE.appendChild(bcd);            
    }
    
    /**
     * Creates the new canonical flux for a coordinate.
     * @param result                The canonical model
     * @param cffd                  The cffd element
     * @param groups                The canonical groups of fluxes
     * @param spaceCoordinate       The spatial coordinate.
     */
    private void makeFlux(Document result, Node cffd, Hashtable<String, List<Node>> groups, String spaceCoordinate) {
    	Element flux = TCUtils.createElement(result, mmsUri, "flux");
    	
    	//---------------new mathML----------------------
    	Element mathML = TCUtils.createElement(result, mmsUri, "mathML");
    	flux.appendChild(mathML);
    	Element math = TCUtils.createElement(result, mtUri, "math");
    	mathML.appendChild(math);
    	//--sum the formulas
    	List<Node> formulas = groups.get(spaceCoordinate);
    	if (formulas.size() > 1) {
            Element apply = TCUtils.createElement(result, mtUri, "apply");
            Element plus = TCUtils.createElement(result, mtUri, "plus");
            apply.appendChild(plus);
            math.appendChild(apply);
            for (int i = 0; i < formulas.size(); i++) {
            	Node m = result.importNode(formulas.get(i).cloneNode(true), true);
            	apply.appendChild(m);
            }
    	}
    	else {
            Node m = result.importNode(formulas.get(0).cloneNode(true), true);
            math.appendChild(m);
    	}
    	
    	//----------------new coordinate------------------
    	Element coord = TCUtils.createElement(result, mmsUri, "coordinate");
    	coord.setTextContent(spaceCoordinate);
    	flux.appendChild(coord);
    	cffd.appendChild(flux);
    }
    
    /**
     * Creates the new canonical parabolic term for the derivative coordinates.
     * @param result                The canonical model
     * @param pTerms                The parabolicTerms element
     * @param pGroups               The canonical groups of P terms
     * @param qGroups               The canonical groups of Q terms
     * @param firstSC               The first derivative
     * @param secondSC              The second derivative
     */
    private void makePT(Document result, Node pTerms, Hashtable<String, List<Node>> pGroups, Hashtable<String, List<Node>> qGroups, 
            String firstSC, String secondSC) {
           
        //---------------new mathML----------------------
        Element pMath = TCUtils.createElement(result, mtUri, "math");
        List<Node> pFormulas = pGroups.get(firstSC + "&" + secondSC);
        if (pFormulas.size() > 1) {
            Element apply = TCUtils.createElement(result, mtUri, "apply");
            Element plus = TCUtils.createElement(result, mtUri, "plus");
            apply.appendChild(plus);
            pMath.appendChild(apply);
            for (int i = 0; i < pFormulas.size(); i++) {
                Node m = result.importNode(pFormulas.get(i).cloneNode(true), true);
                apply.appendChild(m);
            }
        }
        else {
            Node m = result.importNode(pFormulas.get(0).cloneNode(true), true);
            pMath.appendChild(m);
        }
        //---------------new mathML----------------------
        Element qMath = TCUtils.createElement(result, mtUri, "math");
        List<Node> qFormulas = qGroups.get(firstSC + "&" + secondSC);
        if (qFormulas.size() > 1) {
            Element apply = TCUtils.createElement(result, mtUri, "apply");
            Element plus = TCUtils.createElement(result, mtUri, "plus");
            apply.appendChild(plus);
            qMath.appendChild(apply);
            for (int i = 0; i < qFormulas.size(); i++) {
                Node m = result.importNode(qFormulas.get(i).cloneNode(true), true);
                apply.appendChild(m);
            }
        }
        else {
            Node m = result.importNode(qFormulas.get(0).cloneNode(true), true);
            qMath.appendChild(m);
        }
        
        Element pt = TCUtils.createElement(result, mmsUri, "parabolicTerm");
        Element fd = TCUtils.createElement(result, mmsUri, "firstDerivative");
        fd.setTextContent(firstSC);
        Element sd = TCUtils.createElement(result, mmsUri, "secondDerivative");
        sd.setTextContent(secondSC);
        Element qTerm = TCUtils.createElement(result, mmsUri, "Qterm");
        Element pTerm = TCUtils.createElement(result, mmsUri, "Pterm");
        Element qMathml = TCUtils.createElement(result, mmsUri, "mathML");
        Element pMathml = TCUtils.createElement(result, mmsUri, "mathML");
        qMathml.appendChild(qMath);
        pMathml.appendChild(pMath);
        qTerm.appendChild(qMathml);
        pTerm.appendChild(pMathml);
        pt.appendChild(fd);
        pt.appendChild(sd);
        pt.appendChild(qTerm);
        pt.appendChild(pTerm);
        pTerms.appendChild(pt);
    }
   
    /**
     * Creates an auxiliary equation.
     * @param result            The canonical model
     * @param insSet            The parent of the new equation
     * @param formulas          The formulas that form the equation
     */
    private void makeSummation(Document result, Node insSet, List<Node> formulas) {
        /*
         * <mt:math>
                    <mt:apply>
                        <mt:plus/>
                        <mt:ci>q</mt:ci>
                        <mt:cn>0</mt:cn>
                    </mt:apply>
                </mt:math>
         **/
        //---------------new mathML----------------------
        Element math = TCUtils.createElement(result, mtUri, "math");
        insSet.appendChild(math);
        
        //--sum the formulas
        if (formulas.size() > 1) {
            Element apply2 = TCUtils.createElement(result, mtUri, "apply");
            Element plus = TCUtils.createElement(result, mtUri, "plus");
            apply2.appendChild(plus);
            math.appendChild(apply2);
            for (int i = 0; i < formulas.size(); i++) {
                Node m = result.importNode(formulas.get(i).cloneNode(true), true);
                apply2.appendChild(m);
            }
            math.appendChild(apply2);
        }
        else {
            if (formulas.size() == 1) {
                Node m = result.importNode(formulas.get(0).cloneNode(true), true);
                math.appendChild(m);
            }
            else {
                Element cn = TCUtils.createElement(result, mtUri, "cn");
                cn.setTextContent("0");
                math.appendChild(cn);
            }
        }               
    }
    
    /**
     * Transforms a formula from tensorial to canonical.
     * @param math              The formula
     * @param derIdx            The first derivative index
     * @param secDerIdx         The second derivative index
     * @param coordinates       The spatial coordinates
     * @param order             The order of the field
     * @param indexNumber       The number of the index
     * @param isFlux            True if the formula is a flux formula
     * @return                  The new formula
     * @throws TCException      TC00X External error
     */
    private Node transformFormula(Node math, String derIdx, String secDerIdx, String[] coordinates, int order, int indexNumber, boolean isFlux) 
        throws TCException {

        //----------creates the math tag needed for the xsl transformer-----
        Document result = TCUtils.builder.newDocument();                  
                
        //1.-------------processes the formula----------------        
        Node mathCpy = result.importNode(math.cloneNode(true), true);        
        //1.1.Preprocess: changes: (a - b) = (a + (-b)) | a(b + c) = ab + ac | a(bc) = abc
        mathCpy = util.preprocessFormula(result, mathCpy, order, isFlux);
        
        //1.2.addSemantics: adds all the semantics to identify terms and expansions
        Element coords = makeCoordinates(result, coordinates);
        int derivativeIdx = -1;
        if (derIdx != null) {
            derivativeIdx = Integer.parseInt(derIdx);
        }
        int secDerivativeIdx = -1;
        if (secDerIdx != null) {
            secDerivativeIdx = Integer.parseInt(secDerIdx);
        }
        addSemantics(mathCpy, order, coords, indexNumber, derivativeIdx, secDerivativeIdx, coordinates);        
        //2.----------------xsl transformation: only expansions--------------
        //Executes the xsl processing until the action is calculate
        result.appendChild(mathCpy);
        Document doc = util.applyXSL(result, "calculate");
    	return doc;               
    }
    
    /**
     * Makes the spatial coordinates of the canonical model.
     * @param result        The canonical model
     * @param coordinates   The coordinates
     * @return              The new coordinates
     */
    private Element makeCoordinates(Document result, String[] coordinates) {
    	Element coords = TCUtils.createElement(result, null, "coordinates");
        for (int i = 0; i < coordinates.length; i++) {
            Element coord = TCUtils.createElement(result, null, "coordinate");
            coord.setTextContent(coordinates[i]);
            coords.appendChild(coord);
    	}    	
    	return coords;
    }
 
    /**
     * Creates a new semantic property.
     * @param math                  The formula
     * @param id                    The identifier of the semantic property
     * @param action                The action for the semantic
     * @param coords                The spatial coordinates
     * @param idx                   The index
     * @param indexNumber           The index number
     * @param derivativeIndexes     The first and second derivative index numbers
     * @return                      The new formula
     */
    private Element makeSemPropsNew(Node math, String id, String action, Node coords, String idx, 
            int indexNumber, int[] derivativeIndexes) {
        int derivativeIndex = derivativeIndexes[0];
        int secDerivativeIndex = derivativeIndexes[1];
        Document result = math.getOwnerDocument();
    	//semantics tag
    	Element semantics = TCUtils.createElement(result, mtUri, "semantics");
    	semantics.appendChild(math);
    	//annotation tag
        Element annot = TCUtils.createElement(result, mtUri, "annotation-xml");
        annot.setAttribute("encoding", "xml");
        semantics.appendChild(annot);
        //properties tag
    	Element props = TCUtils.createElement(result, null, "properties");
    	annot.appendChild(props);
    	//id
        if (id != null) {
            Element idE = TCUtils.createElement(result, null, "id");
            idE.setTextContent(id);
            props.appendChild(idE);
    	}
    	//action
    	Element actionE = TCUtils.createElement(result, null, "action");
    	actionE.setTextContent(action);
    	props.appendChild(actionE);
    	//derivativeIndex
    	if (derivativeIndex > 0) {
    	    Element der = TCUtils.createElement(result, null, "derivativeIndex");
    	    der.setTextContent(TCUtils.indexes[derivativeIndex - 1]);
    	    props.appendChild(der);
        }
        //derivativeIndex
        if (secDerivativeIndex > 0) {
            Element der = TCUtils.createElement(result, null, "secDerivativeIndex");
            der.setTextContent(TCUtils.indexes[secDerivativeIndex - 1]);
            props.appendChild(der);
        }
    	//indexToReplace
    	if (idx != null) {
            Element der = TCUtils.createElement(result, null, "indexToReplace");
            der.setTextContent(idx);
            props.appendChild(der);
    	}    	
    	//coordinates
    	if (coords != null) {
    	    props.appendChild(coords);
    	}
    	//indexRemainingList
        Element indRemList = TCUtils.createElement(result, null, "indexRemainingList");    		
        props.appendChild(indRemList);        	
        //indexNumber
    	if (indexNumber > 0) {
            Element idxNumber = TCUtils.createElement(result, null, "indexNumber");
            idxNumber.setTextContent(String.valueOf(indexNumber));
            props.appendChild(idxNumber);
    	}
    	return semantics;
    }
    
    /**
     * Transforms the tensorial auxiliary fields and equations to canonical ones.
     * @param result                The canonical model
     * @param tensModel             The tensorial model
     * @param spaceCoordinates      The spatial coordinates
     * @param auxiliarFields        The auxiliary fields
     * @param auxiliarEquations     The auxiliary equations
     * @param symmetricVars         Information about the symmetries in variables
     * @throws TCException          TC001   Not a valid XML Document
     *                              TC00X External error
     */
    private void makeTensorialAuxiliarFields(Document result, Document tensModel, String[] spaceCoordinates, List<Node> auxiliarFields, 
            List<Node> auxiliarEquations, HashMap<String, SymmetryAction> symmetricVars) throws TCException {
    	//Prefixes of the root element
        String prefix = tensModel.getDocumentElement().getPrefix();
        NodeList tensFieldsList =  TCUtils.find(tensModel.getDocumentElement(), 
                "/" + prefix + ":tensorialPhysicalModel" + "/" + prefix + ":tensorialAuxiliarField");            
        for (int i = 0; i < tensFieldsList.getLength(); i++) {
            Node tensField  = tensFieldsList.item(i);
            //The order of the tensorial field
            String order = tensField.getChildNodes().item(1).getTextContent();            	
            int ord = Integer.parseInt(order);

            //obtains his tensorialEquation
            NodeList tensorialEquationList = TCUtils.find(tensModel.getDocumentElement(), "//" + prefix + ":tensorialEquation[" + prefix 
                    + ":auxiliarField = '" + tensField.getChildNodes().item(0).getTextContent() + "']");
            if (tensorialEquationList == null || tensorialEquationList.getLength() == 0) {
            	throw new TCException(TCException.TC001, "There is one auxiliar tensorial field with no associated auxiliar equation");
            }
            Node tensorialEquation = tensorialEquationList.item(0);
        	
        	//extract the indexNumber
    	    String idxNum = tensorialEquation.getChildNodes().item(1).getTextContent();
    	    int indexNumber = Integer.parseInt(idxNum);
    	    
    	    //--Transform each tensorial flux to canonical flux
        	//Replace the summation indexes.
    	    Node tensFormula = tensorialEquation.getChildNodes().item(2).getFirstChild();
    	    Node canAux = transformFormula(tensFormula, null, null, spaceCoordinates, ord, indexNumber, false);
    	    List<Node> canAuxList = new ArrayList<Node>();
    	    canAuxList.add(canAux.getFirstChild().getFirstChild());

        	//create all the possible combinations
    	    List<String[]> combList = TCUtils.generateCoordVariations(spaceCoordinates, ord);
        	
        	//for each combination creates a new auxiliarField            	
    	    for (int j = 0; j < combList.size(); j++) {
                String[] comb = combList.get(j);            		
                //----------new field--------------------
                String fieldName = TCUtils.fieldName(tensField, comb); 
                if (!symmetricVars.containsKey(fieldName)) {
                    
                    Element field = TCUtils.createElement(result, mmsUri, "auxiliarField");
                    field.setTextContent(fieldName);
                    auxiliarFields.add(field);
                    
                    //----------normalize---------------
                    //reemplazamos en cada flujo los indices libres por la combinacion actual
                    List<Node> canAuxSimp = util.replaceIndexes(canAuxList, comb);
                    //para cada flujo calcular, simplificar y normalizar, utilizando la xsl
                    canAuxSimp = util.normalizeFluxes(canAuxSimp, symmetricVars);
                    
                    //----------new auxiliar Equation-----------
                    Element auxEquation = TCUtils.createElement(result, mmsUri, "auxiliarEquation");
                    auxiliarEquations.add(auxEquation);
                    //evEquation name
                    Element evName = TCUtils.createElement(result, mmsUri, "equationName");
                    String eqName = tensorialEquation.getParentNode().getChildNodes().item(0).getTextContent();
                    eqName = addCombToName(eqName, comb, tensField);
                    evName.setTextContent(eqName);
                    auxEquation.appendChild(evName);
                    //add the instructionSet to the auxiliar equation
                    Element auxiliarEquationData = TCUtils.createElement(result, mmsUri, "auxiliarEquationData");
                    Element auxiliarField = TCUtils.createElement(result, mmsUri, "auxiliarField");
                    auxiliarField.setTextContent(fieldName);
                    auxiliarEquationData.appendChild(auxiliarField);
                    Element insSet = TCUtils.createElement(result, mmsUri, "instructionSet");
                    auxEquation.appendChild(auxiliarEquationData);
                    auxiliarEquationData.appendChild(insSet);
                    List<Node> formulas = new ArrayList<Node>();
                    for (int k = 0; k < canAuxSimp.size(); k++) {
                        Node formula = TCUtils.find(canAuxSimp.get(k), "*[1]").item(0);
                        formulas.add(formula);	
                    }
                    makeSummation(result, insSet, formulas);
                }
    	    }
        }
    }
    
    /**
     * Creates the characteristic decomposition for the physical model.
     * @param equationSet               The equation set to create the decomposition to
     * @param tensModel                 The tensorial model
     * @param spatialCoordinates        The spatial coordinates
     * @param symmetricVars             Information about the symmetries in variables
     * @throws TCException              TC00X - External error
     */
    private void makeCharDecomposition(Element equationSet, Document tensModel, String[] spatialCoordinates, 
            HashMap<String, SymmetryAction> symmetricVars) throws TCException {
        Document result = equationSet.getOwnerDocument();
        String prefix = result.getDocumentElement().getPrefix();
        NodeList charDecompList = TCUtils.find(tensModel, "//" + prefix + ":charDecomposition");
        if (charDecompList.getLength() > 0) {
            Element charDecomp = (Element) charDecompList.item(0);
            Element newCharDecomp = TCUtils.createElement(result, mmsUri, "charDecomposition");
            
            //generalCharDecomp
            NodeList angleCharDecompList = charDecomp.getElementsByTagNameNS(mmsUri, "generalCharDecomp");
            if (angleCharDecompList.getLength() > 0) {
                newCharDecomp.appendChild(makeGeneralCharDecom(result, tensModel, spatialCoordinates, (Element) angleCharDecompList.item(0), 
                        symmetricVars));
            }
            //coordCharDecomp
            NodeList coordCharDecompList = charDecomp.getElementsByTagNameNS(mmsUri, "coordCharDecomp");
            if (coordCharDecompList.getLength() > 0) {
                makeCoordCharDecom(result, tensModel, spatialCoordinates, coordCharDecompList, newCharDecomp, symmetricVars);
            }
            
            //MaxCharacteristicSpeed
            NodeList maxCharacteristicSpeed = charDecomp.getElementsByTagNameNS(mmsUri, "maxCharacteristicSpeed");
            if (maxCharacteristicSpeed.getLength() > 0) {
                makeMaxCharSpeed(newCharDecomp, tensModel, spatialCoordinates, maxCharacteristicSpeed.item(0), symmetricVars);
            }
            //AuxiliarEquations
            NodeList auxiliarDefinitions = charDecomp.getElementsByTagNameNS(mmsUri, "auxiliarDefinitions");
            if (auxiliarDefinitions.getLength() > 0) {
                for (int i = 0; i < auxiliarDefinitions.getLength(); i++) {
                    makeTensorialCharAuxiliarEq(newCharDecomp, tensModel, spatialCoordinates, auxiliarDefinitions.item(i), symmetricVars);
                }
            }
            equationSet.appendChild(newCharDecomp);
        }
    }
    
    /**
     * Makes coordinate characteristic decompositions.
     * @param result                    The physical model
     * @param tensModel                 The tensorial model
     * @param spatialCoordinates        The spatial coordinates
     * @param coordCharDecompList       The tensorial coordinate characteristic decomposition list
     * @param newCharDecomp             The physical characteristic decomposition general tag to add the new decompositions in.
     * @param symmetricVars             Information about the symmetries in variables
     * @throws TCException              TC00X - External error
     */
    private void makeCoordCharDecom(Document result, Document tensModel, String[] spatialCoordinates, NodeList coordCharDecompList, 
        Element newCharDecomp, HashMap<String, SymmetryAction> symmetricVars) throws TCException {
        String prefix = result.getDocumentElement().getPrefix();
        
        for (int ci = 0; ci < spatialCoordinates.length; ci++) {
            String coord = spatialCoordinates[ci];
            for (int cdi = 0; cdi < coordCharDecompList.getLength(); cdi++) {
                Element coordCharDecomp = (Element) coordCharDecompList.item(cdi);
                Element newCoordCharDecomp = TCUtils.createElement(result, mmsUri, "coordCharDecomp");
                
                //EigenSpace
                NodeList eigenSpaces = coordCharDecomp.getElementsByTagNameNS(mmsUri, "eigenSpace");
                for (int i = 0; i < eigenSpaces.getLength(); i++) {
                    Element eigenSpace = (Element) eigenSpaces.item(i);
                    Element newEigenSpace = TCUtils.createElement(result, mmsUri, "eigenSpace");
                    //Copy name
                    Element eigenSpaceName = (Element) eigenSpace.getFirstChild().cloneNode(true);
                    eigenSpaceName.setTextContent(eigenSpaceName.getTextContent() + coord);
                    newEigenSpace.appendChild(result.importNode(eigenSpaceName, true));
                    //Values
                    //extract the indexNumber
                    String idxNum = eigenSpace.getChildNodes().item(1).getFirstChild().getTextContent();
                    int indexNumber = Integer.parseInt(idxNum);
                    //--Transform the eigenValue
                    //Replace the summation indexes.
                    Node eigenValue = eigenSpace.getChildNodes().item(1).getLastChild().getFirstChild().cloneNode(true);
                    Element newEigenValue = TCUtils.createElement(result, mmsUri, "eigenValue");
                    Node canAux = transformFormula(eigenValue, null, null, spatialCoordinates, 1, indexNumber, false);
                    List<Node> canAuxSimp = new ArrayList<Node>();
                    canAuxSimp.add(canAux.getFirstChild().getFirstChild());
                    //reemplazamos en cada flujo los indices libres por la combinacion actual   
                    canAuxSimp = util.replaceIndexes(canAuxSimp, TCUtils.generateCoordVariations(spatialCoordinates, 1).get(ci));
                    //para cada flujo calcular, simplificar y normalizar, utilizando la xsl
                    canAuxSimp = util.normalizeFluxes(canAuxSimp, symmetricVars);
                    Element mathML = TCUtils.createElement(result, mmsUri, "mathML");
                    List<Node> formulas = new ArrayList<Node>();
                    for (int k = 0; k < canAuxSimp.size(); k++) {
                        Node formula = TCUtils.find(canAuxSimp.get(k), "*[1]").item(0);
                        formulas.add(formula);  
                    }
                    makeSummation(result, mathML, formulas);
                    newEigenValue.appendChild(mathML);
                    newEigenSpace.appendChild(newEigenValue);
                    
                    //Vectors
                    NodeList vectors = eigenSpace.getElementsByTagNameNS(mmsUri, "eigenVector");
                    for (int j = 0; j < vectors.getLength(); j++) {
                        Element vector = (Element) vectors.item(j);
                        Element newVector = TCUtils.createElement(result, mmsUri, "eigenVector");
                        //Get the name
                        Element newName = TCUtils.createElement(result, mmsUri, "vectorName");
                        newName.setTextContent(vector.getFirstChild().getTextContent() + coord);
                        newVector.appendChild(newName);
                        //--Transform the vector
                        NodeList coeffs = vector.getElementsByTagNameNS(mmsUri, "diagCoefficient");
                        for (int m = 0; m < coeffs.getLength(); m++) {
                            String fieldName = coeffs.item(m).getChildNodes().item(1).getTextContent();
                            int fieldOrder = 0;
                            String searchName = fieldName;
                            if (!fieldName.contains("_n")) {
                                if (fieldName.contains("_t")) {
                                    searchName = fieldName.substring(0, fieldName.indexOf("_"));
                                }
                                fieldOrder = Integer.valueOf(TCUtils.find(tensModel, "//" + prefix + ":tensorialField[" + prefix + ":name = '" 
                                        + searchName + "']/" + prefix + ":order").item(0).getTextContent());
                            }
                            List<String[]> combList2 = TCUtils.generateCoordVariations(spatialCoordinates, fieldOrder);
                            for (int n = 0; n < combList2.size(); n++) {
                                String[] comb2 = combList2.get(n);  
                                String fieldNameComb = TCUtils.parameterName(fieldName, comb2);
                                if (!symmetricVars.containsKey(fieldNameComb)) {
                                    //reemplazamos en cada flujo los indices libres por la combinacion actual   
                                    String[] comb = new String[1];
                                    comb[0] = coord;
                                    String[] newComb = TCUtils.mergeCombinations(comb, comb2);
                                    Node coefficient = createCoefficient(coeffs.item(m), fieldNameComb, newComb, spatialCoordinates, 
                                            fieldOrder + 1, "diagCoefficient", symmetricVars);
                                    if (coefficient != null) {
                                        newVector.appendChild(result.importNode(coefficient, true));
                                    }
                                }
                            }
                        }
                        newEigenSpace.appendChild(newVector);
                    }
                    newCoordCharDecomp.appendChild(newEigenSpace);
                }
                
                //Undiagonalization
                NodeList undiagonalizationList = coordCharDecomp.getElementsByTagNameNS(mmsUri, "undiagonalization");
                for (int i = 0; i < undiagonalizationList.getLength(); i++) {
                    Element undiagonalization = (Element) undiagonalizationList.item(i);
                    //The order of the field
                    Element field = (Element) undiagonalization.getFirstChild();
                    int ord = 0;
                    String searchName = field.getTextContent();
                    if (!field.getTextContent().contains("_n")) {
                        if (field.getTextContent().contains("_t")) {
                            searchName = field.getTextContent().substring(0, field.getTextContent().indexOf("_"));
                        }
                        ord = Integer.valueOf(TCUtils.find(tensModel, "//" + prefix + ":tensorialField[" + prefix + ":name = '" 
                                + searchName + "']/" + prefix + ":order").item(0).getTextContent());
                    }

                    //create all the possible combinations
                    List<String[]> combList = TCUtils.generateCoordVariations(spatialCoordinates, ord);
                    //for each combination creates a new undiagonalization              
                    for (int j = 0; j < combList.size(); j++) {
                        String[] comb = combList.get(j);        
                        Element newUndiagonalization = TCUtils.createElement(result, mmsUri, "undiagonalization");
                        //Field name
                        Element newField = (Element) field.cloneNode(true);
                        String fieldName = TCUtils.parameterName(field.getTextContent(), comb); 
                        newField.setTextContent(fieldName);
                        newUndiagonalization.appendChild(result.importNode(newField, true));
                        int fieldOrder = 0;
                        if (!fieldName.contains("_n")) {
                            if (fieldName.contains("_t")) {
                                fieldName = fieldName.substring(0, fieldName.indexOf("_"));
                            }
                            fieldOrder = Integer.parseInt(TCUtils.find(tensModel, "//" + prefix + ":tensorialField[" + prefix + ":name = '" 
                                + fieldName + "']/" + prefix + ":order").item(0).getTextContent());
                        }
                        //Coefficients
                        NodeList coeffs = undiagonalization.getElementsByTagNameNS(mmsUri, "undiagCoefficient");
                        for (int m = 0; m < coeffs.getLength(); m++) {
                            String vectorName = coeffs.item(m).getChildNodes().item(1).getTextContent();
                            String[] comb2 = new String[1];
                            comb2[0] = coord;
                            String vectorNameComb = vectorName + coord;
                            //reemplazamos en cada flujo los indices libres por la combinacion actual   
                            String[] newComb = TCUtils.mergeCombinations(comb, comb2);
                            Node coefficient = createCoefficient(coeffs.item(m), vectorNameComb, newComb, 
                                    spatialCoordinates, 1 + fieldOrder, "undiagCoefficient", symmetricVars);
                            if (coefficient != null) {
                                newUndiagonalization.appendChild(result.importNode(coefficient, true));
                            }
                        }
                        newCoordCharDecomp.appendChild(newUndiagonalization);
                    }
                }
                //EigenCoordinate
                Element newEigenCoordinate = TCUtils.createElement(result, mmsUri, "eigenCoordinate");
                newEigenCoordinate.setTextContent(coord);
                newCoordCharDecomp.appendChild(newEigenCoordinate);
                newCharDecomp.appendChild(newCoordCharDecomp);
            }
        }
    }
    
    /**
     * Makes angle characteristic decomposition.
     * @param result                    The physical model
     * @param tensModel                 The tensorial model
     * @param spatialCoordinates        The spatial coordinates
     * @param angleCharDecomp           The tensorial angle characteristic decomposition
     * @return                          The phisical angle characteristic decomposition
     * @param symmetricVars             Information about the symmetries in variables
     * @throws TCException              TC00X - External error
     */
    private Element makeGeneralCharDecom(Document result, Document tensModel, String[] spatialCoordinates, Element angleCharDecomp, 
            HashMap<String, SymmetryAction> symmetricVars) throws TCException {
        String prefix = result.getDocumentElement().getPrefix();
        Element newGeneralCharDecomp = TCUtils.createElement(result, mmsUri, "generalCharDecomp");
        //EigenSpace
        NodeList eigenSpaces = angleCharDecomp.getElementsByTagNameNS(mmsUri, "eigenSpace");
        for (int i = 0; i < eigenSpaces.getLength(); i++) {
            Element eigenSpace = (Element) eigenSpaces.item(i).cloneNode(true);
 
            int order = Integer.parseInt(eigenSpace.getChildNodes().item(1).getFirstChild().getTextContent());
            
            List<String[]> combList = TCUtils.generateCoordVariations(spatialCoordinates, order);
            for (int k = 0; k < combList.size(); k++) {
                Element newEigenSpace = TCUtils.createElement(result, mmsUri, "eigenSpace");
                String[] comb = combList.get(k);    
                //Copy name
                Element eigenSpaceName = (Element) eigenSpace.getFirstChild().cloneNode(true);
                eigenSpaceName.setTextContent(TCUtils.parameterName(eigenSpaceName.getTextContent(), comb));
                newEigenSpace.appendChild(result.importNode(eigenSpaceName, true));
                
                //Values
                //extract the indexNumber
                String idxNum = eigenSpace.getChildNodes().item(2).getFirstChild().getTextContent();
                int indexNumber = Integer.parseInt(idxNum);
                //--Transform the eigenValue
                //Replace the summation indexes.
                Node eigenValue = eigenSpace.getChildNodes().item(2).getLastChild().getFirstChild().cloneNode(true);
                Element newEigenValue = TCUtils.createElement(result, mmsUri, "eigenValue");

                Node canAux = transformFormula(eigenValue, null, null, spatialCoordinates, order, indexNumber, false);
                List<Node> canAuxSimp = new ArrayList<Node>();
                canAuxSimp.add(canAux.getFirstChild().getFirstChild());
                //reemplazamos en cada flujo los indices libres por la combinacion actual   
                canAuxSimp = util.replaceIndexes(canAuxSimp, TCUtils.generateCoordVariations(spatialCoordinates, 0).get(0));
                //para cada flujo calcular, simplificar y normalizar, utilizando la xsl
                canAuxSimp = util.normalizeFluxes(canAuxSimp, symmetricVars);
                Element mathML = TCUtils.createElement(result, mmsUri, "mathML");
                List<Node> formulas = new ArrayList<Node>();
                for (int l = 0; l < canAuxSimp.size(); l++) {
                    Node formula = TCUtils.find(canAuxSimp.get(l), "*[1]").item(0);
                    formulas.add(formula);  
                }
                makeSummation(result, mathML, formulas);
                newEigenValue.appendChild(mathML);
                newEigenSpace.appendChild(newEigenValue);
                
                //Vectors
                NodeList vectors = eigenSpace.getElementsByTagNameNS(mmsUri, "eigenVector");
                for (int j = 0; j < vectors.getLength(); j++) {
                    Element vector = (Element) vectors.item(j);
                    Element newVector = TCUtils.createElement(result, mmsUri, "eigenVector");
                    //Get the name
                    Element newName = TCUtils.createElement(result, mmsUri, "vectorName");
                    newName.setTextContent(TCUtils.parameterName(vector.getFirstChild().getTextContent(), comb));
                    newVector.appendChild(newName);
                    //--Transform the vector
                    NodeList coeffs = vector.getElementsByTagNameNS(mmsUri, "diagCoefficient");
                    for (int m = 0; m < coeffs.getLength(); m++) {
                        String fieldName = coeffs.item(m).getChildNodes().item(1).getTextContent();
                        int fieldOrder = 0;
                        String searchName = fieldName;
                        if (!fieldName.contains("_n")) {
                            if (fieldName.contains("_t")) {
                                searchName = fieldName.substring(0, fieldName.indexOf("_"));
                            }
                            fieldOrder = Integer.valueOf(TCUtils.find(tensModel, "//" + prefix + ":tensorialField[" + prefix + ":name = '" 
                                    + searchName + "']/" + prefix + ":order").item(0).getTextContent());
                        }
                        List<String[]> combList2 = TCUtils.generateCoordVariations(spatialCoordinates, fieldOrder);
                        for (int n = 0; n < combList2.size(); n++) {
                            String[] comb2 = combList2.get(n);  
                            String fieldNameComb = TCUtils.parameterName(fieldName, comb2);
                            if (!symmetricVars.containsKey(fieldNameComb)) {
                                //reemplazamos en cada flujo los indices libres por la combinacion actual   
                                String[] newComb = TCUtils.mergeCombinations(comb, comb2);
                                Node coefficient = createCoefficient(coeffs.item(m), fieldNameComb, newComb, 
                                        spatialCoordinates, fieldOrder + order, "diagCoefficient", symmetricVars);
                                if (coefficient != null) {
                                    newVector.appendChild(result.importNode(coefficient, true));
                                }
                            }
                        }
                    }
                    
                    newEigenSpace.appendChild(newVector);
                }
                newGeneralCharDecomp.appendChild(newEigenSpace);
            }
        }
        //Undiagonalization
        NodeList undiagonalizationList = angleCharDecomp.getElementsByTagNameNS(mmsUri, "undiagonalization");
        for (int i = 0; i < undiagonalizationList.getLength(); i++) {
            Element undiagonalization = (Element) undiagonalizationList.item(i);
            //The order of the field
            Element field = (Element) undiagonalization.getFirstChild();
            int ord = 0;
            String searchName = field.getTextContent();
            if (!field.getTextContent().contains("_n")) {
                if (field.getTextContent().contains("_t")) {
                    searchName = field.getTextContent().substring(0, field.getTextContent().indexOf("_"));
                }
                ord = Integer.valueOf(TCUtils.find(tensModel, "//" + prefix + ":tensorialField[" + prefix + ":name = '" 
                        + searchName + "']/" + prefix + ":order").item(0).getTextContent());
            }

            //create all the possible combinations
            List<String[]> combList = TCUtils.generateCoordVariations(spatialCoordinates, ord);
            //for each combination creates a new undiagonalization              
            for (int j = 0; j < combList.size(); j++) {
                String[] comb = combList.get(j);        
                Element newUndiagonalization = TCUtils.createElement(result, mmsUri, "undiagonalization");
                //Field name
                Element newField = (Element) field.cloneNode(true);
                String fieldName = TCUtils.parameterName(field.getTextContent(), comb); 
                newField.setTextContent(fieldName);
                newUndiagonalization.appendChild(result.importNode(newField, true));
                int fieldOrder = 0;
                if (!fieldName.contains("_n")) {
                    if (fieldName.contains("_t")) {
                        fieldName = fieldName.substring(0, fieldName.indexOf("_"));
                    }
                    fieldOrder = Integer.parseInt(TCUtils.find(tensModel, "//" + prefix + ":tensorialField[" + prefix + ":name = '" + fieldName 
                            + "']/" + prefix + ":order").item(0).getTextContent());
                }
                //Coefficients
                NodeList coeffs = undiagonalization.getElementsByTagNameNS(mmsUri, "undiagCoefficient");
                for (int m = 0; m < coeffs.getLength(); m++) {
                    String vectorName = coeffs.item(m).getChildNodes().item(1).getTextContent();
                    int vectorOrder = Integer.parseInt(TCUtils.find(undiagonalization.getParentNode(), "//" + prefix + ":eigenSpace[descendant::" 
                            + prefix + ":vectorName = '" + vectorName + "']/" + prefix + ":order").item(0).getTextContent());
                    List<String[]> combList2 = TCUtils.generateCoordVariations(spatialCoordinates, vectorOrder);
                    for (int n = 0; n < combList2.size(); n++) {
                        String[] comb2 = combList2.get(n);  
                        String vectorNameComb = TCUtils.parameterName(vectorName, comb2);
                        //reemplazamos en cada flujo los indices libres por la combinacion actual   
                        String[] newComb = TCUtils.mergeCombinations(comb, comb2);
                        Node coefficient = createCoefficient(coeffs.item(m), vectorNameComb, newComb, spatialCoordinates, 
                                vectorOrder + fieldOrder, "undiagCoefficient", symmetricVars);
                        if (coefficient != null) {
                            newUndiagonalization.appendChild(result.importNode(coefficient, true));
                        }
                    }
                }
                newGeneralCharDecomp.appendChild(newUndiagonalization);
            }
        }
        return newGeneralCharDecomp;
    }
    
    /**
     * Create the coefficient of a vector or undiagonalization.
     * @param coeff                 The tensorial coefficient
     * @param fieldNameComb         The field (or vector) name combination
     * @param newComb               The new combination
     * @param spatialCoordinates    The spatial coordinates
     * @param coeffOrder            The order of the coefficient
     * @param coefficientName       The coefficient name
     * @param symmetricVars         The symmetric variables
     * @return                      The canonical coefficient
     * @throws TCException          TC00X External error
     */
    private Element createCoefficient(Node coeff, String fieldNameComb, String[] newComb, String[] spatialCoordinates, 
            int coeffOrder, String coefficientName, HashMap<String, SymmetryAction> symmetricVars) throws TCException {
        Document result = coeff.getOwnerDocument();
        //extract the indexNumber
        int indexNumber = Integer.parseInt(coeff.getFirstChild().getTextContent());
        //Coefficient name
        Element coefficient = TCUtils.createElement(result, mmsUri, coefficientName);
        String fieldName;
        if (coefficientName.equals("diagCoefficient")) {
            fieldName = "field";
        }
        else {
            fieldName = "eigenVector";
        }
        Element field = TCUtils.createElement(result, mmsUri, fieldName);
        field.setTextContent(fieldNameComb);
        coefficient.appendChild(field);
        //Coefficient formula
        Node vectorFormula = coeff.getLastChild().getFirstChild().cloneNode(true);
        Node canAux = transformFormula(vectorFormula, null, null, spatialCoordinates, coeffOrder, indexNumber, false);
        List<Node> canAuxSimp = new ArrayList<Node>();
        canAuxSimp.add(canAux.getFirstChild().getFirstChild());
        //reemplazamos en cada flujo los indices libres por la combinacion actual   
        canAuxSimp = util.replaceIndexes(canAuxSimp, newComb);
        //para cada flujo calcular, simplificar y normalizar, utilizando la xsl
        canAuxSimp = util.normalizeFluxes(canAuxSimp, symmetricVars);
        if (canAuxSimp.size() != 0) {
            Element mathMLVector = null;
            if (coefficientName.equals("diagCoefficient")) {
                mathMLVector = TCUtils.createElement(result, mmsUri, "instructionSet");
            }
            else {
                mathMLVector = TCUtils.createElement(result, mmsUri, "mathML");
            }
            ArrayList<Node> formulas = new ArrayList<Node>();
            for (int l = 0; l < canAuxSimp.size(); l++) {
                Node formula = TCUtils.find(canAuxSimp.get(l), "*[1]").item(0);
                formulas.add(formula);  
            }
            makeSummation(result, mathMLVector, formulas);
            coefficient.appendChild(mathMLVector);
            return coefficient;
        }
        return null;
    }
    
    /**
     * Transforms the max characteristic speed.
     * @param insertNode            The parent node to insert the equation.
     * @param tensModel             The tensorial model
     * @param spaceCoordinates      The spatial coordinates
     * @param maxCharSpeed          The max char speed
     * @param symmetricVars         Symmetric information
     * @throws TCException          TC001   Not a valid XML Document
     *                              TC00X External error
     */
    private void makeMaxCharSpeed(Node insertNode, Document tensModel, String[] spaceCoordinates, Node maxCharSpeed, 
        HashMap<String, SymmetryAction> symmetricVars)throws TCException {
        Document result = insertNode.getOwnerDocument();        
  
        //extract the indexNumber
        String idxNum = maxCharSpeed.getChildNodes().item(0).getTextContent();
        int indexNumber = Integer.parseInt(idxNum);
        
        //--Transform each tensorial flux to canonical flux
        //Replace the summation indexes.
        Node tensFormula = maxCharSpeed.getChildNodes().item(1).getFirstChild();
        Node canAux = transformFormula(tensFormula, null, null, spaceCoordinates, 0, indexNumber, false);
        List<Node> canAuxList = new ArrayList<Node>();
        canAuxList.add(canAux.getFirstChild().getFirstChild());

        //create all the possible combinations
        List<String[]> combList = TCUtils.generateCoordVariations(spaceCoordinates, 0);
        
        //for each combination creates a new auxiliarField              
        for (int j = 0; j < combList.size(); j++) {
            String[] comb = combList.get(j); 
            
            //----------normalize---------------
            //reemplazamos en cada flujo los indices libres por la combinacion actual
            List<Node> canAuxSimp = util.replaceIndexes(canAuxList, comb);
            //para cada flujo calcular, simplificar y normalizar, utilizando la xsl
            canAuxSimp = util.normalizeFluxes(canAuxSimp, symmetricVars);
            
            //----------new auxiliar Equation-----------
            Element maxCharacteristicSpeed = TCUtils.createElement(result, mmsUri, "maxCharacteristicSpeed");
            insertNode.appendChild(maxCharacteristicSpeed);
            //add the instructionSet to the auxiliar equation
            Element insSet = TCUtils.createElement(result, mmsUri, "mathML");
            maxCharacteristicSpeed.appendChild(insSet);
            List<Node> formulas = new ArrayList<Node>();
            for (int k = 0; k < canAuxSimp.size(); k++) {
                Node formula = TCUtils.find(canAuxSimp.get(k), "*[1]").item(0);
                formulas.add(formula);  
            }
            
            makeSummation(result, insSet, formulas);
        }
    }
    
    /**
     * Transforms a tensorial characteristic auxiliar equation to canonical ones.
     * @param insertNode            The parent node to insert the equation.
     * @param tensModel             The tensorial model
     * @param spaceCoordinates      The spatial coordinates
     * @param auxiliarEquation      The tensorial auxiliar equation to add
     * @param symmetricVars         Symmetric information
     * @throws TCException          TC001   Not a valid XML Document
     *                              TC00X External error
     */
    private void makeTensorialCharAuxiliarEq(Node insertNode, Document tensModel, String[] spaceCoordinates, Node auxiliarEquation, 
            HashMap<String, SymmetryAction> symmetricVars) throws TCException {
        Document result = insertNode.getOwnerDocument();        
  
        //The order of the equation
        String order = auxiliarEquation.getChildNodes().item(1).getTextContent();              
        int ord = Integer.parseInt(order);
        
        //extract the indexNumber
        String idxNum = auxiliarEquation.getChildNodes().item(2).getTextContent();
        int indexNumber = Integer.parseInt(idxNum);
        
        //--Transform each tensorial flux to canonical flux
        //Replace the summation indexes.
        Node tensFormula = auxiliarEquation.getChildNodes().item(THREE).getFirstChild();
        Node canAux = transformFormula(tensFormula, null, null, spaceCoordinates, ord, indexNumber, false);
        List<Node> canAuxList = new ArrayList<Node>();
        canAuxList.add(canAux.getFirstChild().getFirstChild());

        //create all the possible combinations
        List<String[]> combList = TCUtils.generateCoordVariations(spaceCoordinates, ord);
        
        //for each combination creates a new auxiliarField              
        for (int j = 0; j < combList.size(); j++) {
            String[] comb = combList.get(j);                    
            //----------new field--------------------
            String varName = TCUtils.parameterName(auxiliarEquation.getChildNodes().item(0).getTextContent(), comb); 
                
            Element field = TCUtils.createElement(result, mmsUri, "auxiliarField");
            field.setTextContent(varName);
            
            //----------normalize---------------
            //reemplazamos en cada flujo los indices libres por la combinacion actual
            List<Node> canAuxSimp = util.replaceIndexes(canAuxList, comb);
            //para cada flujo calcular, simplificar y normalizar, utilizando la xsl
            canAuxSimp = util.normalizeFluxes(canAuxSimp, symmetricVars);
            
            //----------new auxiliar Equation-----------
            Element auxEquation = TCUtils.createElement(result, mmsUri, "auxiliarDefinitions");
            Element auxiliarVar = TCUtils.createElement(result, mmsUri, "auxiliarVar");
            Element insSet = TCUtils.createElement(result, mmsUri, "instructionSet");
            insertNode.appendChild(auxEquation);
            auxiliarVar.setTextContent(varName);
            auxEquation.appendChild(auxiliarVar);
            //add the instructionSet to the auxiliar equation
            auxEquation.appendChild(insSet);
            List<Node> formulas = new ArrayList<Node>();
            for (int k = 0; k < canAuxSimp.size(); k++) {
                Node formula = TCUtils.find(canAuxSimp.get(k), "*[1]").item(0);
                formulas.add(formula);  
            }
            makeSummation(result, insSet, formulas);
        }
    }
    
    /**
     * Adds some semantic information to a formula.
     * @param mathCpy               The formula
     * @param order                 The order of the field
     * @param coords                The coordinates
     * @param indexNumber           The index number
     * @param derivativeIndex       The derivative index number
     * @param secDerivativeIdx      The second derivative index number
     * @param coordinates           The spatial coordinates
     * @throws TCException          TC00X External error
     */
    private void addSemantics(Node mathCpy, int order, 
		Element coords, int indexNumber, int derivativeIndex, int secDerivativeIdx, String[] coordinates) throws TCException {
	
    	//Obtain the summation indexes of the formula
    	NodeList symbols = TCUtils.find(mathCpy, ".//mt:ci[mt:mmultiscripts]");
    	List<String> sumIdx = TCUtils.getSummationIndexes(symbols, order, coordinates);   
    	for (int i = 0; i < sumIdx.size(); i++) {
    	    addSemanticsForIdx(mathCpy, coords, indexNumber, derivativeIndex, secDerivativeIdx, sumIdx.get(i));
    	}
        
    	//-------------------ADD THE MAIN SEMANTICS------------------------
    	//encapsulamos con el semantics principal
    	//si el indice de la derivada es un índice libre entonces es un termino
        String id = "main";
        if (derivativeIndex <= order) {
            id = "term";
        }
    //    System.out.println("MATHCPY:" + TCUtils.domToString(mathCpy));
        int[] derivativeIndexes = new int[2];
        derivativeIndexes[0] = derivativeIndex;
        derivativeIndexes[1] = secDerivativeIdx;
    	Node mainSemantics = makeSemPropsNew(mathCpy.getFirstChild().cloneNode(true), id, "sum", 
    			coords.cloneNode(true), null, indexNumber, derivativeIndexes);
    	mathCpy.replaceChild(mainSemantics, mathCpy.getFirstChild());
    //	System.out.println("MATHCPY2:" + TCUtils.domToString(mathCpy));
    	
    }

    /**
     * Adds some semantic information for a index to a formula.
     * @param mathCpy               The formula
     * @param idx                   The index
     * @param coords                The coordinates
     * @param indexNumber           The index number
     * @param derivativeIndex       The derivative index number
     * @param secDerivativeIndex    The second derivative index number
     * @throws TCException          TC00X External error
     */
    private void addSemanticsForIdx(Node mathCpy, Element coords, int indexNumber, int derivativeIndex, int secDerivativeIndex, 
            String idx) throws TCException {
    	
      //  System.out.println("mathCpy:" + TCUtils.domToString(mathCpy));
    	//-------------add expansion semantics----------------
    	//cogemos todos los simbolos que tienen el indice indicado
    	NodeList symbols = TCUtils.find(mathCpy, ".//mt:ci[mt:mmultiscripts[mt:mi= '" + idx + "']]");
    	List<Node> symbolsList = new ArrayList<Node>();
    	for (int i = 0; i < symbols.getLength(); i++) {
    	    symbolsList.add(symbols.item(i));        		
    	}
    	while (symbolsList.size() > 0) {
            Node symbol = symbolsList.get(0);   
            Node minTree = null;
            String id = null;
            //si el indice de la derivada coincide con el índice del símbolo
            //entonces el minimo arbol es hasta la raiz
            if (derivativeIndex >= 1 && idx.equals(TCUtils.indexes[derivativeIndex - 1]) 
                    || secDerivativeIndex >= 1 && idx.equals(TCUtils.indexes[secDerivativeIndex - 1])) {
                minTree = mathCpy.getFirstChild();
                id = "term";
                //no necesitamos procesar el resto de simbolos, todos ya estan contenidos
                symbolsList.clear();
            }
            else {
                //obtenemos el mínimo arbol que lo contiene
                minTree = util.minimumTree(symbol, idx);
                id = "expansion";
                //updatamos la lista de symbolos que necesitamos procesar.
                symbolsList.remove(0);
                for (int j = symbolsList.size() - 1; j >= 0; j--) {
                    Node symb = symbolsList.get(j);     
                    if (minTree.compareDocumentPosition(symb) == (Node.DOCUMENT_POSITION_CONTAINED_BY + Node.DOCUMENT_POSITION_FOLLOWING)
            			   || minTree.compareDocumentPosition(symb) == Node.DOCUMENT_POSITION_CONTAINED_BY) {
                        symbolsList.remove(j);
                    }
                }
            }
            //lo encapsulamos en el semantics
            //si no tiene ningún semantics
            Node expSem = util.expSem(minTree);
            if (expSem == null) {
                int[] derivativeIndexes = new int[2];
                derivativeIndexes[0] = derivativeIndex;
                derivativeIndexes[1] = secDerivativeIndex;
                Node expSemantics = makeSemPropsNew(minTree.cloneNode(true), id, "expand", coords.cloneNode(true), 
                        idx, indexNumber, derivativeIndexes);
                Node prueba = minTree.getParentNode();
                prueba.replaceChild(expSemantics, minTree);
            }
            else {
            //	System.out.println("minTree:" + TCUtils.domToString(minTree));
           // 	System.out.println("expSem:" + TCUtils.domToString(expSem));
               //ya tiene un semantics	
                util.updateSemantics(expSem, idx, id);
            //    System.out.println("expSem2:" + TCUtils.domToString(expSem));
            }
    			        			        	    		    	
    	}	    	
    }
    
    /**
     * Checks the formula for tensorial correctness.
     * 1 - The Levi-Civita operator must have at least 3 indexes.
     * 2 - The Kronecker delta must have only 2 indexes.
     * 3 - All the free indexes must appear only once in every addend.
     * 4 - Every summation index must appear twice in every addend.
     * @param formula           The formula.
     * @param formula2          A second formula (only for parabolic terms)
     * @param freeIndexes       The number of free indexes of the formula (order of the field
     *                                                                            + 2 if parabolic term
     *                                                                            + 1 if flux
     *                                                                            + 0 otherwise)
     * @param coefOrder         The order of the formula coefficient (0 in case the formula is not a coefficient one) 
     * @param pt                if the formula is a parabolic term formula                                                                           
     * @return                  True if the formula is correct.
     * @throws TCException      TC004 Incorrect mathematical content in tensorial model
     *                          TC00X External
     */
    public boolean checkTensorialFormula(String formula, String formula2, int freeIndexes, int coefOrder, boolean pt) throws TCException {
        final int lcCounter = 7;
        final int dkCounter = 5;
        Document form = TCUtils.stringToDom(formula);
        //1 - The Levi-Civita operator must have at least 3 indexes.
        NodeList lcList = TCUtils.find(form, "//mt:mmultiscripts[mt:mi[text() = 'ε_LC']]"); 
        for (int i = 0; i < lcList.getLength(); i++) {
            if (lcList.item(i).getChildNodes().getLength() < lcCounter) {
                throw new TCException(TCException.TC004, "Levi-Civita operator must have at least 3 indexes");
            }
        }
        //2 - The Kronecker delta must have only 2 indexes.
        NodeList dkList = TCUtils.find(form, "//mt:mmultiscripts[mt:mi[text() = 'δ_K']]"); 
        for (int i = 0; i < dkList.getLength(); i++) {
            if (dkList.item(i).getChildNodes().getLength() != dkCounter) {
                throw new TCException(TCException.TC004, "Kronecker operator must have only 2 indexes");
            }
        }
        Document form2 = null;
        if (pt) {
            form2 = TCUtils.stringToDom(formula2);
            //1 - The Levi-Civita operator must have at least 3 indexes.
            lcList = TCUtils.find(form2, "//mt:mmultiscripts[mt:mi[text() = 'ε_LC']]"); 
            for (int i = 0; i < lcList.getLength(); i++) {
                if (lcList.item(i).getChildNodes().getLength() < lcCounter) {
                    throw new TCException(TCException.TC004, "Levi-Civita operator must have at least 3 indexes");
                }
            }
            //2 - The Kronecker delta must have only 2 indexes.
            dkList = TCUtils.find(form2, "//mt:mmultiscripts[mt:mi[text() = 'δ_K']]"); 
            for (int i = 0; i < dkList.getLength(); i++) {
                if (dkList.item(i).getChildNodes().getLength() != dkCounter) {
                    throw new TCException(TCException.TC004, "Kronecker operator must have only 2 indexes");
                }
            }
        }
        //Preprocess the formula to take all the addends
        ArrayList<Element> addends = new ArrayList<Element>();
        TCUtils.separateAddends(form.getDocumentElement(), addends);
        ArrayList<Element> addends2 = new ArrayList<Element>();
        if (pt) {
            TCUtils.separateAddends(form2.getDocumentElement(), addends2);
        }
        //3 - All the free indexes must appear only once in every addend.
        if (freeIndexes > 0) {
            //Get free indexes names
            ArrayList<String> indexes = new ArrayList<String>();
            for (int i = 0; i < freeIndexes + coefOrder; i++) {
                indexes.add(TCUtils.indexes[i]);
            }
            //Check individually every addend
            for (int i = 0; i < addends.size(); i++) {
                for (int j = 0; j < indexes.size(); j++) {
                    //It can not appear more than once
                    if (!pt) {
                        if (TCUtils.find(addends.get(i), ".//mt:mi[position() > 1][text()='" + indexes.get(j) + "']").getLength() > 1) {
                            throw new TCException(TCException.TC004, "All the free indexes must appear only once in every addend. Index \"" 
                                    + indexes.get(j) + "\" appears more than one time in the formula");
                        }
                     /*   if (i >= Math.min(coefOrder, freeIndexes) 
                                && TCUtils.find(addends.get(i), ".//mt:mi[position() > 1][text()='" + indexes.get(j) + "']").getLength() == 0) {
                            throw new TCException(TCException.TC004, "All the free indexes must appear only once in every addend. Index \"" 
                                    + indexes.get(j) + "\" does not appear in the formula");
                        }*/
                    }
                    else {
                        for (int k = 0; k < addends2.size(); k++) {
                            int pOccurences = TCUtils.find(addends.get(i), ".//mt:mi[position() > 1][text()='" + indexes.get(j) + "']").getLength();
                            int qOccurences = TCUtils.find(addends2.get(k), ".//mt:mi[position() > 1][text()='" + indexes.get(j) + "']").getLength();
                            if (pOccurences + qOccurences > 1) {
                                throw new TCException(TCException.TC004, "All the free indexes must appear only once in every addend. Index \"" 
                                        + indexes.get(j) + "\" appears more than one time in the formulas");
                            }
                          /*  if (pOccurences + qOccurences == 0) {
                                throw new TCException(TCException.TC004, "All the free indexes must appear only once in every addend. Index \"" 
                                        + indexes.get(j) + "\" does not appear in the formulas");
                            }*/
                        }
                    }
                }
            }
        }
        //4 - Every summation index must appear twice in every addend.
        //Get free indexes names
        ArrayList<String> indexes = new ArrayList<String>();
        for (int i = 0; i < freeIndexes + coefOrder; i++) {
            indexes.add(TCUtils.indexes[i]);
        }
        //Check individually every addend
        for (int i = 0; i < addends.size(); i++) {
            NodeList sumIndexes = TCUtils.find(addends.get(i), ".//mt:mi[position() > 1]");
            for (int j = 0; j < sumIndexes.getLength(); j++) {
                String index = sumIndexes.item(j).getTextContent();
                //It is a summation index and is over the coefficient and field order
                if (!indexes.contains(index)) {
                    //It must appear twice
                    if (!pt) {
                        if (TCUtils.find(addends.get(i), ".//mt:mi[position() > 1][text()='" + index + "']").getLength() != 2) {
                    	    throw new TCException(TCException.TC004, "Every summation index must appear twice in every addend. Index \"" 
                                         + index + "\" is not a pair index");	
                    	}
                    }
                    else {
                        for (int k = 0; k < addends2.size(); k++) {
                            int pOccurences = TCUtils.find(addends.get(i), ".//mt:mi[position() > 1][text()='" + index + "']").getLength();
                            int qOccurences = TCUtils.find(addends2.get(k), ".//mt:mi[position() > 1][text()='" + index + "']").getLength();
                            if (pOccurences + qOccurences != 2) {
                                throw new TCException(TCException.TC004, "Every summation index must appear twice in every addend. Index \"" 
                                        + index + "\" is not a pair index");   
                            }
                        }
                    }
                }
            }
        }
        return true;
    }
    
}
