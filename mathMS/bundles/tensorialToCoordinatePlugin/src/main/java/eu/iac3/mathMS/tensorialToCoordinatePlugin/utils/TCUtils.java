package eu.iac3.mathMS.tensorialToCoordinatePlugin.utils;

import eu.iac3.mathMS.tensorialToCoordinatePlugin.TCException;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import javax.xml.XMLConstants;
import javax.xml.namespace.NamespaceContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;
import javax.xml.xpath.XPathFactoryConfigurationException;

import net.sf.saxon.om.NamespaceConstant;

import org.apache.xml.serialize.OutputFormat;
import org.apache.xml.serialize.XMLSerializer;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;

/**
 * Utility class.
 */
public class TCUtils {
    static final int INDENTATION = 4;
    static final int THREE = 3;
    static final int XSLLIMIT = 1000;
    static String mtUri = "http://www.w3.org/1998/Math/MathML";
    static String mmsUri = "urn:mathms";
    private static XPathFactory factory;
    public static String[] indexes = {"i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w"};
    static Transformer transformer;
    static TransformerFactory tFactory;
    static final String XSL = "common" + File.separator + "xsl" + File.separator + "tensorialProcessing.xsl";
    public static DocumentBuilder builder;
    
    //XSL initialization
    static {
        try {
            tFactory = TransformerFactory.newInstance();
            transformer = tFactory.newTransformer(new StreamSource(XSL));
            builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        } 
        catch (TransformerConfigurationException e) {
            e.printStackTrace();
        }
        catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Constructor.
     * @param test  if the execution is a test (problem OSGI-system properties-Saxon with DOM)
     */
    public TCUtils(boolean test) {
        if (test) {
            System.setProperty("javax.xml.xpath.XPathFactory:" + NamespaceConstant.OBJECT_MODEL_SAXON,
                "net.sf.saxon.xpath.XPathFactoryImpl");
            try {
                factory = XPathFactory.newInstance(NamespaceConstant.OBJECT_MODEL_SAXON);
            } 
            catch (XPathFactoryConfigurationException e) {
                e.printStackTrace();
            }
        }
        else {
            factory = new net.sf.saxon.xpath.XPathFactoryImpl();
        }
    }
    
    /**
     * Converts a string to Dom.
     * 
     * @param str               The string
     * @return                  The Dom document
     * @throws TCException      TC001 - Not a valid XML Document
     *                          TC00X - External error
     */
    public static Document stringToDom(String str) throws TCException {
        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            docFactory.setNamespaceAware(true);
            docFactory.setIgnoringElementContentWhitespace(true);
            docFactory.setIgnoringComments(true);
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            InputStream is = new ByteArrayInputStream(str.getBytes("UTF-8"));
            Document result = docBuilder.parse(is);
            removeWhitespaceNodes(result.getDocumentElement());
            return result;
        }
        catch (SAXException e) {
            throw new TCException(TCException.TC001, e.toString());
        }
        catch (Exception e) {
            throw new TCException(TCException.TC00X, e.toString());
        }
    }
    
    /**
     * Converts a Dom node to string.
     * 
     * @param node              The dom node
     * @return                  The String
     * @throws TCException    AGDM007 - External error
     */
    public static String domToString(Node node) throws TCException {
        try {
            Transformer transformerDTS = TransformerFactory.newInstance().newTransformer();
            StreamResult result = new StreamResult(new ByteArrayOutputStream());
            DOMSource source = new DOMSource(node);
            transformerDTS.transform(source, result);
            return new String(((ByteArrayOutputStream) result.getOutputStream()).toByteArray(), "UTF-8");
        }
        catch (Exception e) {
            throw new TCException(TCException.TC00X, e.toString());
        }
    }
    
    /**
     * Executes an xpath query in the document. Using Saxon 9.1 implementation (xpath 2.0).
     * @param node              The document to find in
     * @param xpathQuery        The query
     * @return                  A node list with the results
     * @throws TCException      TC00X - External error
     */
    public static NodeList find(Node node, String xpathQuery) throws TCException {
        try {
            
            XPath xpath = factory.newXPath();
            
            NamespaceContext ctx = new NamespaceContext() {
                public String getNamespaceURI(String prefix) {
                    String uri;
                    if (prefix.equals("mms")) {
                        uri = "urn:mathms";  
                    }    
                    else {
                        if (prefix.equals("mt")) {
                            uri = "http://www.w3.org/1998/Math/MathML";
                        }
                        else {
                            uri = null;
                        }
                    }
                    return uri;
                }
                // Dummy implementation - not used!
                public Iterator < ? > getPrefixes(String val) {
                    return null;
                }
                // Dummy implemenation - not used!
                public String getPrefix(String uri) {
                    return null;
                }
            };

            xpath.setNamespaceContext(ctx);
            XPathExpression expr = xpath.compile(xpathQuery);

            return (NodeList) expr.evaluate(node, XPathConstants.NODESET);
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new TCException(TCException.TC00X, e.getMessage());
        } 
    }
    
    /**
     * Remove the white spaced nodes from a Dom element.
     * 
     * @param e The element to delete the whitespaces from
     */
    public static void removeWhitespaceNodes(Element e) {
        NodeList children = e.getChildNodes();
        for (int i = children.getLength() - 1; i >= 0; i--) {
            Node child = children.item(i);
            if (child instanceof Text && ((Text) child).getData().trim().length() == 0) {
                e.removeChild(child);
            }
            else if (child instanceof Element) {
                removeWhitespaceNodes((Element) child);
            }
        }
    }
    
    /**
     * Preprocess the tensorial model:
     * 1) Calculate index number of the equations.
     * 2) Replace the indexes referring to constant values of spatial coordinates
     * @param model         The tensorial model
     * @param coordinates   The spatial coordinates
     * @return              The new model
     * @throws TCException  TC00X - External error
     */
    public static Document preprocess(Document model, String[] coordinates) throws TCException {
        String prefix = model.getDocumentElement().getPrefix();
        //1) Calculate index number of the equations.
        NodeList equations = find(model, "//" + prefix + ":tensorialPDE|//" + prefix + ":tensorialEquation|//" + prefix + ":maxCharacteristicSpeed");
        for (int i = 0; i < equations.getLength(); i++) {
            //Calculate the number of indexes of the equation
            ArrayList<String> indexesUsed = new ArrayList<String>();
            NodeList indexesUsedList = find(equations.item(i), ".//mt:mmultiscripts/mt:*[position() > 1 and name() = 'mt:mi']");
            for (int j = 0; j < indexesUsedList.getLength(); j++) {
                if (!indexesUsed.contains(indexesUsedList.item(j).getTextContent())) {
                    indexesUsed.add(indexesUsedList.item(j).getTextContent());
                }
            }
            //Set the value to the tag
            Element indexNumberEl = (Element) equations.item(i).getFirstChild();
            if (!indexNumberEl.getLocalName().equals("indexNumber")) {
                indexNumberEl = (Element) indexNumberEl.getNextSibling();
            }
            indexNumberEl.setTextContent(String.valueOf(indexesUsed.size()));
        }
        // The characteristic info is a bit more difficult
        equations = find(model, "//" + prefix + ":eigenSpace");
        for (int i = 0; i < equations.getLength(); i++) {
            int order = 1;
            if (equations.item(i).getChildNodes().item(1).getLocalName().equals("order")) {
                order = Integer.parseInt(equations.item(i).getChildNodes().item(1).getTextContent());
            }
            //Calculate the number of indexes of the equation
            ArrayList<String> indexesUsed = new ArrayList<String>();
            for (int j = 0; j < order; j++) {
                indexesUsed.add(indexes[j]);
            }
            //EigenValue
            ArrayList<String> indexesUsedValue = new ArrayList<String>();
            indexesUsedValue.addAll(indexesUsed);
            NodeList indexesUsedList = find(equations.item(i), "./" + prefix 
                    + ":eigenValue//mt:mmultiscripts/mt:*[position() > 1 and name() = 'mt:mi']");
            for (int j = 0; j < indexesUsedList.getLength(); j++) {
                if (!indexesUsedValue.contains(indexesUsedList.item(j).getTextContent())) {
                    indexesUsedValue.add(indexesUsedList.item(j).getTextContent());
                }
            }
            Element indexNumberEl = null;
            if (equations.item(i).getChildNodes().item(1).getLocalName().equals("order")) {
                indexNumberEl = (Element) equations.item(i).getChildNodes().item(2).getFirstChild();
            }
            else {
                indexNumberEl = (Element) equations.item(i).getChildNodes().item(1).getFirstChild();
            }
            indexNumberEl.setTextContent(String.valueOf(indexesUsedValue.size()));
            //EigenVector
            NodeList diagCoefList = find(equations.item(i), ".//" + prefix + ":eigenVector/" + prefix + ":diagCoefficient");
            for (int j = 0; j < diagCoefList.getLength(); j++) {
                ArrayList<String> indexesUsedVector = new ArrayList<String>();
                indexesUsedVector.addAll(indexesUsed);
                String fieldName = diagCoefList.item(j).getChildNodes().item(1).getTextContent();
                if (!fieldName.contains("_n")) {
                    if (fieldName.contains("_t")) {
                        fieldName = fieldName.substring(0, fieldName.indexOf("_"));
                    }
                    int fieldOrder = Integer.parseInt(find(model, "//" + prefix + ":tensorialField[" + prefix + ":name = '" + fieldName 
                            + "']/" + prefix + ":order").item(0).getTextContent());
                    for (int l = indexesUsedVector.size() - order; l < fieldOrder; l++) {
                        indexesUsedVector.add(indexes[l + order]);
                    }
                }
                //Add indexed used in coefficient formulas
                indexesUsedList = find(diagCoefList.item(j), ".//mt:mmultiscripts/mt:*[position() > 1 and name() = 'mt:mi']");
                for (int k = 0; k < indexesUsedList.getLength(); k++) {
                    if (!indexesUsedVector.contains(indexesUsedList.item(k).getTextContent())) {
                        indexesUsedVector.add(indexesUsedList.item(k).getTextContent());
                    }
                }
                indexNumberEl = (Element) diagCoefList.item(j).getFirstChild();
                indexNumberEl.setTextContent(String.valueOf(indexesUsedVector.size()));
            }
        }
        //Undiagonalization
        NodeList undiagCoeffList = find(model, ".//" + prefix + ":undiagonalization/" + prefix + ":undiagCoefficient");
        for (int i = 0; i < undiagCoeffList.getLength(); i++) {
            String field = undiagCoeffList.item(i).getParentNode().getFirstChild().getTextContent();
            int fieldOrder = 0;
            ArrayList<String> indexesUsed = new ArrayList<String>();
            if (!field.contains("_n")) {
                if (field.contains("_t")) {
                    field = field.substring(0, field.indexOf("_"));
                }
                fieldOrder = Integer.parseInt(find(model, "//" + prefix + ":tensorialField[" + prefix + ":name = '" + field 
                        + "']/" + prefix + ":order").item(0).getTextContent());
         
                for (int j = 0; j < fieldOrder; j++) {
                    indexesUsed.add(indexes[j]);
                }
            }
            String vectorName = undiagCoeffList.item(i).getChildNodes().item(1).getTextContent();
            int vectorOrder = 1;
            if (find(undiagCoeffList.item(i).getParentNode().getParentNode(), "//" + prefix + ":eigenSpace[descendant::" 
                    + prefix + ":vectorName = '" + vectorName + "']/" + prefix + ":order").getLength() > 0) {
                vectorOrder = Integer.parseInt(find(undiagCoeffList.item(i).getParentNode().getParentNode(), "//" + prefix 
                        + ":eigenSpace[descendant::" + prefix + ":vectorName = '" + vectorName + "']/" + prefix 
                        + ":order").item(0).getTextContent());
            }
            for (int l = indexesUsed.size() - fieldOrder; l < vectorOrder; l++) {
                indexesUsed.add(indexes[l + fieldOrder]);
            }
            //Add indexed used in coefficient formulas
            NodeList indexesUsedList = find(undiagCoeffList.item(i), ".//mt:mmultiscripts/mt:*[position() > 1 and name() = 'mt:mi']");
            for (int k = 0; k < indexesUsedList.getLength(); k++) {
                if (!indexesUsed.contains(indexesUsedList.item(k).getTextContent())) {
                    indexesUsed.add(indexesUsedList.item(k).getTextContent());
                }
            }
            Element indexNumberEl = (Element) undiagCoeffList.item(i).getFirstChild();
            indexNumberEl.setTextContent(String.valueOf(indexesUsed.size()));
        }
        //2) Replace the indexes referring to constant values of spatial coordinates
        NodeList fixedIndexes = find(model, "//mt:mmultiscripts/mt:mn");
        for (int i = fixedIndexes.getLength() - 1; i >= 0; i--) {
            int indexValue = Integer.parseInt(fixedIndexes.item(i).getTextContent());
            if (indexValue <= coordinates.length) {
                Element mi = createElement(model, mtUri, "mi");
                mi.setTextContent(coordinates[indexValue - 1]);
                fixedIndexes.item(i).getParentNode().replaceChild(mi, fixedIndexes.item(i));
            }
            else {
                //If index does not exist the term must be set 0
                Element cn = createElement(model, mtUri, "cn");
                cn.setTextContent("0");
                fixedIndexes.item(i).getParentNode().getParentNode().getParentNode().replaceChild(cn, 
                        fixedIndexes.item(i).getParentNode().getParentNode());
            }
        }
        return model;
    }
    
    /**
     * Does some post processing before returning the canonical model:
     * 1 - Removes all the annotation-xml elements.
     * @param model             The canonical model
     * @return                  The new canonical model
     * @throws TCException      TC00X - External error
     */
    public static Document postProcess(Document model) throws TCException {
        NodeList annotations = find(model, "//mt:annotation-xml");
        for (int i = annotations.getLength() - 1; i >= 0; i--) {
            annotations.item(i).getParentNode().getParentNode().replaceChild(annotations.item(i).getParentNode().getFirstChild(), 
                    annotations.item(i).getParentNode());
        }
        return model;
    }
    
    /**
     * Method that validates the xml document against a XSD.
     * @param xmlDoc            The document to be validated
     * @param xsd               The schema to be checked against
     * @throws TCException      TC001 - XML document does not match XML Schema
     *                          TC00X - External error
     */   
    public static void schemaValidation(String xmlDoc, String xsd) throws TCException {
        try {
            // 1. Lookup a factory for the W3C XML Schema language
            SchemaFactory sFactory = 
                SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            // 2. Compile the schema. 
            // Here the schema is loaded from a java.io.File.
            String filePath = "common" + File.separator + "XSD" + File.separator + xsd;
            File schemaLocation = new File(filePath);
            Schema schema = sFactory.newSchema(schemaLocation);
            // 3. Get a validator from the schema.
            Validator validator = schema.newValidator();
            // 4. Parse the document you want to check.
            InputStream is = new ByteArrayInputStream(xmlDoc.getBytes("UTF-8"));
            Source source = new StreamSource(is);
            // 5. Check the document
            validator.validate(source);
        }
        catch (SAXException e) {
            throw new TCException(TCException.TC001, "Document: " + xmlDoc + "\n" + e.toString());
        }
        catch (Exception e) {
            throw new TCException(TCException.TC00X, e.toString());
        }
    }
    
    /**
     * Create an element in the Uri namespace.
     * @param doc           The document where to create the element
     * @param uri           The namespace uri
     * @param tagName       The tag name
     * @return              The element
     */
    public static Element createElement(Document doc, String uri, String tagName) {
        Element element;
        if (uri != null) {
            element = doc.createElementNS(uri, tagName);
            if (uri.equals("http://www.w3.org/1998/Math/MathML")) {
                element.setPrefix("mt");
            }
            if (uri.equals("urn:mathms")) {
                element.setPrefix("mms");
            }
        }
        else {
            element = doc.createElement(tagName); 
        }
        return element;
    }
    
    /**
     * Generates Variations with repetitions of a given elements caught in n groups.
     * @param chars         the elements to repit
     * @param n             the size of the group
     * @return              An array with the repetitions
     */
    public static List<String[]> generateCoordVariations(String[] chars, int n) {
    	List<String[]> groups = new ArrayList<String[]>();
    	if (n > 0) {
    	    for (int i = 0, j = 1; i < Math.pow(chars.length, n); i++, j = 1) {
            	String[] group = new String[n];
                for (int k = 0; k < n; k++) {
                    group[k] = chars[(i / j) % chars.length];
                    j *= chars.length;
                }
                String[] groupAux = new String[n];
                int idx = 0;
                for (int m = (n - 1); m >= 0; m--) {
                    groupAux[idx] = group[m];
                    idx++;
                }
            	groups.add(groupAux);
            }	
    	}
    	else {
            String[] group = new String[0];
            groups.add(group);
    	}
        return groups;
    }

    /**
     * Calculates the symmetries of the fields and the actions to be done.
     * @param tensModel                 The tensorial model
     * @param spatialCoordinates        The spatial coordinates
     * @return                          The actions for the fields 
     * @throws TCException              TC00X External error
     */
    public static HashMap<String, SymmetryAction> calculateSymmetries(Document tensModel, String[] spatialCoordinates) throws TCException {
        HashMap<String, SymmetryAction> symmetries = new HashMap<String, SymmetryAction>();
        //Calculate symmetries for every field
        String prefix = tensModel.getDocumentElement().getPrefix();
        NodeList tensFieldsList =  TCUtils.find(tensModel.getDocumentElement(), "/" + prefix + ":tensorialPhysicalModel" + "/" + prefix 
                + ":tensorialField[descendant::" + prefix + ":value = 'SYMMETRIC' or descendant::" + prefix + ":value = 'ANTISYMMETRIC']|/" 
                + prefix + ":tensorialPhysicalModel" + "/" + prefix + ":tensorialAuxiliarField[descendant::" + prefix 
                + ":value = 'SYMMETRIC' or descendant::" + prefix + ":value = 'ANTISYMMETRIC']");
        for (int i = 0; i < tensFieldsList.getLength(); i++) {
            Element tensField  = (Element) tensFieldsList.item(i);
            int order = Integer.parseInt(tensField.getChildNodes().item(1).getTextContent());
            
            //create all the possible combinations
            List<String[]> combList = TCUtils.generateCoordVariations(spatialCoordinates, order);
            ArrayList<String[]> basePermutation = new ArrayList<String[]>();
            
            //Get symmetries information
            ArrayList<ArrayList<Integer>> symIndexess = new ArrayList<ArrayList<Integer>>();
            ArrayList<String> symTypes = new ArrayList<String>();
            NodeList fieldSymmetries = tensField.getElementsByTagNameNS(mmsUri, "fieldSymmetry");
            for (int j = 0; j < fieldSymmetries.getLength(); j++) {
                ArrayList<Integer> symIndexes = new ArrayList<Integer>();
                String symType = fieldSymmetries.item(j).getLastChild().getTextContent();
                symTypes.add(symType);
                NodeList indexList = ((Element) fieldSymmetries.item(j)).getElementsByTagNameNS(mmsUri, "index");
                for (int k = 0; k < indexList.getLength(); k++) {
                    symIndexes.add(Integer.parseInt(indexList.item(k).getTextContent()));
                }
                symIndexess.add(symIndexes);
            }
            
            for (int j = 0; j < combList.size(); j++) {
                String[] comb = combList.get(j);
                
                boolean isSymmetric = false;
                boolean positiveSign = true;
                boolean toDelete = false;
                String baseVariable = "";
                for (int k = 0; k <basePermutation.size() && !isSymmetric; k++) {
                    isSymmetric = true;
                    positiveSign = true;
                    toDelete = false;
                    String[] permut = basePermutation.get(k);
                    baseVariable = fieldName(tensField, permut);
                    ArrayList<Integer> usedIndexes = createUsedIndexes(order);
                    //Check symmetric indexes
                    for (int l = 0; l < symIndexess.size(); l++) {
                        ArrayList<Integer> symIndexes = symIndexess.get(l);
                        ArrayList<String> coordUsedCandidate = new ArrayList<String>();
                        ArrayList<String> coordUsedList = new ArrayList<String>();
                        for (int m = 0; m < symIndexes.size(); m++) {
                            coordUsedCandidate.add(comb[symIndexes.get(m) - 1]);
                            coordUsedList.add(permut[symIndexes.get(m) - 1]);
                            usedIndexes.remove(usedIndexes.indexOf(symIndexes.get(m)));
                        }
                        int sign = calculateSign(coordUsedList, coordUsedCandidate);

                        for (int m = 0; m < symIndexes.size(); m++) {
                            coordUsedList.remove(coordUsedCandidate.get(m));
                        }
                        if (!coordUsedList.isEmpty()) {
                            isSymmetric = false;
                        }
                        else {
                            if (symTypes.get(l).equals("ANTISYMMETRIC")) {
                                if (sign == 0) {
                                    toDelete = true;
                                }
                                //Change sign
                                if (sign < 0) {
                                    if (positiveSign) {
                                        positiveSign = false;
                                    }
                                    else {
                                        positiveSign = true;
                                    }
                                }
                            }
                        }
                    }
                    if (isSymmetric && !toDelete) {
                        //Check free indexes
                        for (int l = 0; l < usedIndexes.size(); l++) {
                            if (!permut[usedIndexes.get(l) - 1].equals(comb[usedIndexes.get(l) - 1])) {
                                isSymmetric = false;
                            }
                        }
                    }
                }
                //Check for repeated indexes in antysymmetries
                if (checkRepeatedIndexes(comb, symIndexess, symTypes)) {
                    toDelete = true;
                }
                //Depending on the symmetries add it to a list or another
                if (toDelete) {
                    symmetries.put(fieldName(tensField, comb), new SymmetryAction("TODELETE", ""));
                }
                else {
                    if (isSymmetric) {
                        if (positiveSign) {
                            symmetries.put(fieldName(tensField, comb), new SymmetryAction("SYMMETRIC", baseVariable));
                        }
                        else {
                            symmetries.put(fieldName(tensField, comb), new SymmetryAction("ANTISYMMETRIC", baseVariable));
                        }
                    }
                    else {
                        basePermutation.add(comb);
                    }
                }
            }
        }
        
        return symmetries;
    }
    
    /**
     * Checks if there are repeated indexes in the antisymmetric permutation.
     * @param comb              The permutation
     * @param symIndexess       The indexes of the symmetries
     * @param symTypes          The type of symmetry
     * @return                  True if there are repeated indexes.
     */
    private static boolean checkRepeatedIndexes(String[] comb, ArrayList<ArrayList<Integer>> symIndexess, ArrayList<String> symTypes) {
        for (int i = 0; i < symIndexess.size(); i++) {
            if (symTypes.get(i).equals("ANTISYMMETRIC")) {
                ArrayList<Integer> symIndexes = symIndexess.get(i);
                for (int j = 0; j < symIndexes.size() - 1; j++) {
                    for (int k = j + 1; k < symIndexes.size(); k++) {
                        if (comb[symIndexes.get(j) - 1].equals(comb[symIndexes.get(k) - 1])) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }
    
    /**
     * Calculates the sign of a permutation compared to a base permutation.
     * @param coordUsedList             The base permutation
     * @param coordUsedCandidate        The permutation to find the sign
     * @return                          The sign of the permutation
     */
    private static int calculateSign(ArrayList<String> coordUsedList, ArrayList<String> coordUsedCandidate) {
        ArrayList<Integer> candPos = new ArrayList<Integer>();
        
        //Calculate index positions
        for (int i = 0; i < coordUsedList.size(); i++) {
            candPos.add(coordUsedList.indexOf(coordUsedCandidate.get(i)));
        }
        
        int sign = 1;
        for (int i = 0; i < candPos.size() - 1; i++) {
            for (int j = i + 1; j < candPos.size(); j++) {
                sign = sign * (candPos.get(j) - candPos.get(i));
            }
        }
        return sign;
        
    }
    

    /**
     * Creates an index array from 1 to order.
     * @param order     The order
     * @return          The index
     */
    private static ArrayList<Integer> createUsedIndexes(int order) {
        ArrayList<Integer> indexList = new ArrayList<Integer>();
        for (int i = 0; i < order; i++) {
            indexList.add(i + 1);
        }
        return indexList;
    }
    
    /**
     * Parses a xml with a xlst.
     * @param xml                           The xml to parse.
     * @param params                        Params to send to the xsl.
     * @return                              the result to apply the xlt to the xml.
     * @throws TCException                  TC00X External error
     */
    public static String xmlTransform(String xml, String[][] params) throws TCException {  
        try {
            ByteArrayInputStream input = new ByteArrayInputStream(xml.getBytes("UTF-8"));
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            
            // Set the parameters
            if (params != null) {
                for (int i = 0; i < params.length; i++) {
                    transformer.setParameter(params[i][0], params[i][1]);
                }   
            }
            transformer.transform(new StreamSource(input), new StreamResult(output));
            return output.toString("UTF-8");
        } 
        catch (Exception e) {
            throw new TCException(TCException.TC00X, e.getMessage());
        }
    }
    
    /**
     * Preprocess a formula to do the following:
     * 1) a - b = a + (-b).
     * 2) a(b + c) = ab + ac.
     * @param doc                   The problem
     * @param node                  The formula
     * @param order                 The order of the field
     * @param isFlux                True if is a flux formula
     * @return                      The new formula
     * @throws TCException          TC00X External error
     */
    public Node preprocessFormula(Document doc, Node node, int order, boolean isFlux) throws TCException {
    	Node resp = node.cloneNode(true);
    	
    	//a - b = a + (-b)    	
    	NodeList minusList = find(resp, ".//mt:apply[mt:minus][count(*) = 3]");
    	for (int i = 0; i < minusList.getLength(); i++) {
            Node applyMinus = minusList.item(i);    		
            applyMinus.replaceChild(createElement(doc, mtUri, "plus"), applyMinus.getFirstChild());
            Node operand = applyMinus.getChildNodes().item(2).cloneNode(true);
            Node applyNew = createElement(doc, mtUri, "apply");
            applyNew.appendChild(createElement(doc, mtUri, "minus"));
            applyNew.appendChild(operand);
            applyMinus.replaceChild(applyNew, applyMinus.getChildNodes().item(2));    		
    	}
    	
    	//a(b + c) = ab + ac
    	resp = distributiveProperty(doc, resp);
    	
    	
    	//Add J operator if necessary to the fluxes:
    	if (isFlux) {
    	    boolean haveDerivativeIdx = haveDerivativeIdx(resp, order);
    	    if (!haveDerivativeIdx) {
    	        resp = addUnityOperator(resp, order, doc);
    	    }	
    	}
		
		
		
    	return resp;
    }
    
    /**
     * Check if the formula have derivative index.
     * @param formula           The formula
     * @param order             The order of the field
     * @return                  True if it have a derivative index
     * @throws TCException      TC00X - External error
     */ 
    private boolean haveDerivativeIdx(Node formula, int order) throws TCException {
    	    	
        boolean have = true;        
        if (TCUtils.find(formula, ".//mt:mi[position() > 1][text()='" + indexes[order] + "']").getLength() == 0) {
            have = false;
        }
                
        return have;
    }
     
    
    /**
     * Adds the unity operator to a formula.
     * @param formula       The formula
     * @param order         The order of the field
     * @param doc           The problem
     * @return              The resulting formula
     */
    private Node addUnityOperator(Node formula, int order, Document doc) {
    	 
        Element newFormula = (Element) formula.cloneNode(true);
    	 
    	Node child = newFormula.getFirstChild().cloneNode(true);    	 
    	Node applyTimesNew = createElement(doc, mtUri, "apply");
        applyTimesNew.appendChild(createElement(doc, mtUri, "times"));
        applyTimesNew.appendChild(child);
        Node ci = createElement(doc, mtUri, "ci");
        Node mmultiscripts = createElement(doc, mtUri, "mmultiscripts");
        Node mi1 = createElement(doc, mtUri, "mi");
        mi1.setTextContent("ℑ");
        Node mi2 = createElement(doc, mtUri, "mi");
        mi2.setTextContent(indexes[order]);
        Node none = createElement(doc, mtUri, "none");
        ci.appendChild(mmultiscripts);
        mmultiscripts.appendChild(mi1);
        mmultiscripts.appendChild(mi2);
        mmultiscripts.appendChild(none);
        applyTimesNew.appendChild(ci);
   
        Node firstChild = newFormula.getFirstChild();
        newFormula.replaceChild(applyTimesNew, firstChild);    	 
         
        return newFormula;    	 
    }
     
    
    /**
     * Applies the distributive property recursively.
     * @param doc               The model
     * @param node              The formula
     * @return                  The new formula
     * @throws TCException      TC00X External error
     */
    private Node distributiveProperty(Document doc, Node node) throws TCException {
    	
    	Node resp = applyDistributiveProperty(doc, node);
    	String respString = domToString(resp);
    	String nodeString = domToString(node);
    	while (!respString.equals(nodeString)) {
            nodeString = new String(respString);
            resp = applyDistributiveProperty(doc, resp);
            respString = domToString(resp);
    	}
    	
    	return resp;
    }
    
    /**
     * Applies the distributive formula one time.
     * @param doc               The model
     * @param node              The formula
     * @return                  The new formula
     * @throws TCException      TC00X External error
     */
    private Node applyDistributiveProperty(Document doc, Node node) throws TCException {
    	Node resp = node.cloneNode(true);
    	
    	//cogemos todos los apply de multiplicacion que tienen almenos un hijo apply suma
    	NodeList timesList = find(resp, ".//mt:apply[mt:times][mt:apply[mt:plus]]"); 
    	for (int i = 0; i < timesList.getLength(); i++) {
            Node times = timesList.item(i);
            //cogemos la primera de las sumas
            Node plus = find(times, "./mt:apply[mt:plus]").item(0);
            //reemplazamos el times por una suma
            Node applyPlusNew = createElement(doc, mtUri, "apply");
            applyPlusNew.appendChild(createElement(doc, mtUri, "plus"));
            //por cada hijo del plus le añadimos una multiplicación
            NodeList plusChilds = find(plus, "./*[position() > 1]");
            for (int j = 0; j < plusChilds.getLength(); j++) {
                Node plusChild = plusChilds.item(j).cloneNode(true);
                Node applyNew = createElement(doc, mtUri, "apply");
                Node timesNew = createElement(doc, mtUri, "times");
                applyNew.appendChild(timesNew);
                applyNew.appendChild(plusChild);
                //añadimos a la multiplicacion todos los elementos de la multiplicacion original.
                //todas las otras sumas, menos la primera
                NodeList plusList = find(times, "./*");
                for (int k = 1; k < plusList.getLength(); k++) {
                    Node plusItem = plusList.item(k);
                    if (plusItem != plus) {
                        applyNew.appendChild(plusItem.cloneNode(true));	
                    }    				
                }    			    		
                applyPlusNew.appendChild(applyNew);
            }
    		//replaces the times with the plus
            times.getParentNode().replaceChild(applyPlusNew, times);
    	}
    	
    	return resp;
    }
    
    /**
     * Gets the summation indexes of some multiscripts symbols.
     * @param symbols               The symbol list
     * @param order                 The order of the field
     * @param coordinates           The spatial coordinates
     * @return                      The non free index list
     * @throws TCException          TC00X External error
     */
    public static List<String> getSummationIndexes(NodeList symbols, int order, String[] coordinates) throws TCException {
    	List<String> indexesList = new ArrayList<String>();
    	
    	//gets the indexes
    	HashSet<String> idxHash = new HashSet<String>();
    	for (int i = 0; i < symbols.getLength(); i++) {
            Node symbol = symbols.item(i);
            NodeList mi = TCUtils.find(symbol, ".//mt:mi[position() > 1]");            
            for (int j = 0; j < mi.getLength(); j++) {
            	String idx = mi.item(j).getTextContent();
            	idxHash.add(idx);            	
            }    		
    	}
        
        
        //add to the final list only the summation indexes
        for (Iterator<String> it  = idxHash.iterator(); it.hasNext(); ) {
            String idx = it.next();
            if (isSummationIndex(idx, order, coordinates)) {
                indexesList.add(idx);
            }
        }
    	
    	return indexesList;
    }
   
    /**
     * Checks if a given index is a non free index.
     * @param index                 The index
     * @param order                 The order of the equation
     * @param coordinates           The spatial coordinates
     * @return                      True if it is a non free index
     */
    private static boolean isSummationIndex(String index, int order, String[] coordinates) {
        //Check not to be a free index
        for (int i = 0; i < order; i++) {
            if (indexes[i].equals(index)) {
                return false;
            }
        }
        //Check not to be a coordinate
        for (int i = 0; i < coordinates.length; i++) {
            if (coordinates[i].equals(index)) {
                return false;
            }
        }
        return true;
    }
    /**
     * Returns the minimum tree of a symbol.
     * @param symbol            The symbol
     * @param idx               The index for the tree
     * @return                  The parent node of the tree
     * @throws TCException      TC00X External error
     */
    public Node minimumTree(Node symbol, String idx) throws TCException {
        Node minTree = symbol;
       
        //cogemos el padre del nodo
        Node parent = symbol.getParentNode();
        //numero de simbolos encontrados con el indice
        int numSym = 1;
        //mientras el padre no sea la raiz
        while (!isRoot(parent, true)) {
            //obtenemos los simbolos que tengan idx como indice
            NodeList symbols = find(parent, ".//mt:mmultiscripts[mt:mi = '" + idx + "']");
            //si es mayor a uno entonces el subarbol contiene el mismo indice
            if (symbols.getLength() > numSym) {
                minTree = parent;
                numSym = symbols.getLength();
            }
            parent = parent.getParentNode();
        }

    	        
    	return minTree;
    }
    
    /**
     * Makes a Expansion semantic to a formula.
     * @param node              The formula
     * @return                  The formula with the semantic information
     * @throws TCException      TC00X External error
     */
    public Node expSem(Node node) throws TCException {
    	boolean haveExpSem = false;
    	Node expSemNode = null;
    	
        if (node != null) {
            NodeList expId = null;
            if ("semantics".equals(node.getLocalName())) {
            	expId = find(node, "./mt:annotation-xml/properties/id");
            	expSemNode = node;
            }
            else if (node.getParentNode() != null && "semantics".equals(node.getParentNode().getLocalName())) {
            	expId = find(node.getParentNode(), "./mt:annotation-xml/properties/id");
            	expSemNode = node.getParentNode();
            }
            
            if (expId != null && expId.getLength() == 1 && ("expansion".equals(expId.item(0).getTextContent())
            		 || "term".equals(expId.item(0).getTextContent()))) {
            	haveExpSem = true;
            }
            if (!haveExpSem) {
            	expSemNode = null;
            }
    		
    	}    
    	
    	return expSemNode;
    }
    
    /**
     * Returns when a formula fragment is the root of the formula.
     * @param node          The formula
     * @param withPlus      If it is an adding formula
     * @return              True if root
     */
    private boolean isRoot(Node node, boolean withPlus) {
        if (node == null || ("math".equals(node.getLocalName())) || (node.getFirstChild() != null 
    			&& "plus".equals(node.getFirstChild().getLocalName()) && withPlus)) {
            return true;
    	}
    	return false;
    }
    
    /**
     * Updates the semantic information of a formula.
     * @param semantics             The formula
     * @param idx                   The index
     * @param id                    The semantic identifier
     * @throws TCException          TC00X External error
     */
    public void updateSemantics(Node semantics, String idx, String id) throws TCException {
    	
    	//check if idx is on the indexToReplace tag
    	NodeList indexToReplace = find(semantics, "./mt:annotation-xml/properties/indexToReplace");
    	if (!idx.equals(indexToReplace.item(0).getTextContent())) {
    		//check if idx is on the remainingList tag
            NodeList remainingIndexes = find(semantics, "./mt:annotation-xml/properties/indexToReplace/indexRemainingList/index");
            boolean found = false;
            int i = 0;
            while (!found && i < remainingIndexes.getLength()) {
            	if (idx.equals(remainingIndexes.item(i).getTextContent())) {
            	    found = true;
            	}
            	else {
            	    i++;
            	}
            }
            //if not in the list then adds the idx to the list
            if (!found) {
            	NodeList indexRemainingList = find(semantics, "./mt:annotation-xml/properties/indexRemainingList");
            	Node index = createElement(semantics.getOwnerDocument(), null, "index");
            	index.setTextContent(idx);
            	indexRemainingList.item(0).appendChild(index);    			
            }
    	}
    	//updates the id tag if not a term one
    	NodeList idTag = find(semantics, "./mt:annotation-xml/properties/id");
    	if (idTag != null && idTag.getLength() > 0 && !idTag.item(0).getTextContent().equals(id) && !idTag.item(0).getTextContent().equals("term")) {
    	    idTag.item(0).setTextContent(id);
    	}
    }
    
    /**
     * Replaces the tensorial indexes with the canonical coordinate combination in some formulas.
     * @param fluxes            The formulas
     * @param comb              The combination
     * @return                  The new formulas
     * @throws TCException      TC00X External error
     */
    public List<Node> replaceIndexes(List<Node> fluxes, String[] comb) throws TCException {
    	List<Node> fluxesNew = new ArrayList<Node>();
    	for (int i = 0; i < fluxes.size(); i++) {
            Node flux = fluxes.get(i).cloneNode(true);
            for (int j = 0; j < comb.length; j++) {
            	String coord = comb[j];
            	//replaces de indexes in the mmultiscripts
            	NodeList miList = find(flux, ".//mt:mi[. = '" + indexes[j] + "']");
            	for (int k = 0; k < miList.getLength(); k++) {
            	    miList.item(k).setTextContent(coord);
            	}
            	//if it is necessary add the derivativeCoordinate tag
            	NodeList derivativeIndex = find(flux, "./mt:annotation-xml/properties/derivativeIndex");
            	if (derivativeIndex.getLength() == 1 && derivativeIndex.item(0).getTextContent().equals(indexes[j])) {
            	    NodeList derivativeCoordinateList = find(flux, "./mt:annotation-xml/properties/derivativeCoordinate");
                    if (derivativeCoordinateList.getLength() == 1) {
                        derivativeCoordinateList.item(0).setTextContent(coord);
                    }
                    else {
                    	Document ownDoc = derivativeIndex.item(0).getOwnerDocument();
                    	Node derivativeCoordinate = createElement(ownDoc, null, "derivativeCoordinate");
                    	derivativeCoordinate.setTextContent(coord);        				
                    	derivativeIndex.item(0).getParentNode().appendChild(derivativeCoordinate);
                    }
            	}
            }
            fluxesNew.add(flux);
    	}
    	return fluxesNew;
    }
    
    /**
     * Replaces the tensorial indexes with the canonical coordinate combination in a formula.
     * @param formulaOld        The formula
     * @param comb              The combination
     * @return                  The new formulas
     * @throws TCException      TC00X External error
     */
    public Node replaceIndexes(Node formulaOld, String[] comb) throws TCException {
        Node formula = formulaOld.cloneNode(true);
        for (int j = 0; j < comb.length; j++) {
            String coord = comb[j];
            //replaces de indexes in the mmultiscripts
            NodeList miList = find(formula, ".//mt:mi[. = '" + indexes[j] + "']");
            for (int k = 0; k < miList.getLength(); k++) {
                miList.item(k).setTextContent(coord);
            }
            //if is necessari adds the derivativeCoordinate tag
            NodeList derivativeIndex = find(formula, "./mt:annotation-xml/properties/derivativeIndex");
            if (derivativeIndex.getLength() == 1 && derivativeIndex.item(0).getTextContent().equals(indexes[j])) {
                NodeList derivativeCoordinateList = find(formula, "./mt:annotation-xml/properties/derivativeCoordinate");
                if (derivativeCoordinateList.getLength() == 1) {
                    derivativeCoordinateList.item(0).setTextContent(coord);
            	}
            	else {
            	    Document ownDoc = derivativeIndex.item(0).getOwnerDocument();
            	    Node derivativeCoordinate = createElement(ownDoc, null, "derivativeCoordinate");
            	    derivativeCoordinate.setTextContent(coord);        				
            	    derivativeIndex.item(0).getParentNode().appendChild(derivativeCoordinate);
            	}
            }
    	}
    	return formula;
    }
    
    /**
     * Normalizes some formulas.
     * @param fluxes            The formulas
     * @param symmetricVars     Information about the symmetries in variables
     * @return                  The new formulas
     * @throws TCException      TC00X External error
     */
    public List<Node> normalizeFluxes(List<Node> fluxes, HashMap<String, SymmetryAction> symmetricVars) throws TCException {
    	Document auxDoc = null;
    	List<Node> fluxesNew = new ArrayList<Node>();
    	for (int i = 0; i < fluxes.size(); i++) {   
            auxDoc = builder.newDocument();
            Node flux = auxDoc.importNode(fluxes.get(i).cloneNode(true), true);
            Node math = createElement(auxDoc, mtUri, "math");
            math.appendChild(flux);
            auxDoc.appendChild(math);
            //calculates and normalizes the flux
            Document result = applyXSL(auxDoc, "simplify");
            //Replace symmetries
            replaceSymmetries(result, symmetricVars);
            //Simplify
            result = applyXSL(result, "end");
            //extracts his components
            NodeList canFluxes = TCUtils.find(result.getDocumentElement(), "//mt:semantics[mt:annotation-xml/properties/id = 'term']");
            for (int i2 = 0; i2 < canFluxes.getLength(); i2++) {
                if (!(canFluxes.item(i2).getFirstChild().getLocalName().equals("cn") 
                        && canFluxes.item(i2).getFirstChild().getTextContent().equals("0"))) {
                    fluxesNew.add(canFluxes.item(i2));   		
                }
            }      		
    	}
    	
    	return fluxesNew;
    }
    
    /**
     * Replaces the symmetric variables of a formula.
     * @param math              The formula
     * @param symmetricVars     The symmetry information
     * @throws TCException      TC00X External error
     */
    private void replaceSymmetries(Document math, HashMap<String, SymmetryAction> symmetricVars) throws TCException {
        if (math != null) {
            Iterator<String> symVarsIt = symmetricVars.keySet().iterator();
            while (symVarsIt.hasNext()) {
                String variable = symVarsIt.next();
                SymmetryAction action = symmetricVars.get(variable);
                NodeList occurrences = TCUtils.find(math, ".//mt:ci[text() = '" + variable + "']");
                //Create the substitute element
                Element substitute = null;
                if (action.getAction().equals("TODELETE")) {
                    substitute = TCUtils.createElement(math, mtUri, "cn");
                    substitute.setTextContent("0");
                }
                if (action.getAction().equals("SYMMETRIC")) {
                    substitute = TCUtils.createElement(math, mtUri, "ci");
                    substitute.setTextContent(action.getBaseVariable());
                }
                if (action.getAction().equals("ANTISYMMETRIC")) {
                    substitute = TCUtils.createElement(math, mtUri, "apply");
                    Element minus = TCUtils.createElement(math, mtUri, "minus");
                    Element ci = TCUtils.createElement(math, mtUri, "ci");
                    ci.setTextContent(action.getBaseVariable());
                    substitute.appendChild(minus);
                    substitute.appendChild(ci);
                }
                //Make the replacements
                for (int j = occurrences.getLength() - 1; j >= 0; j--) {
                    occurrences.item(j).getParentNode().replaceChild(substitute.cloneNode(true), occurrences.item(j));
                }
            }
        }
    }
    
    /**
     * Normalizes some formulas.
     * @param fluxes            The formulas
     * @param symmetricVars     Information about the symmetries in variables
     * @return                  The new formulas
     * @throws TCException      TC00X External error
     */
    public List<Node> normalizePT(List<Node> fluxes, HashMap<String, SymmetryAction> symmetricVars) throws TCException {
        Document auxDoc = null;
        List<Node> fluxesNew = new ArrayList<Node>();
        for (int i = 0; i < fluxes.size(); i++) {   
            auxDoc = builder.newDocument();
            Node flux = auxDoc.importNode(fluxes.get(i).cloneNode(true), true);
            Node math = createElement(auxDoc, mtUri, "math");
            math.appendChild(flux);
            auxDoc.appendChild(math);
            //calculates, simplifies and normalizes the flux
            Document result = applyXSL(auxDoc, "simplify");
            //Replace symmetries
            replaceSymmetries(result, symmetricVars);
            result = applyXSL(result, "end");
            //extracts his components
            NodeList canFluxes = TCUtils.find(result.getDocumentElement(), "//mt:semantics[mt:annotation-xml/properties/id = 'term']");
            if (canFluxes.getLength() == 0) {
                if (!(result.getDocumentElement().getFirstChild().getFirstChild().getLocalName().equals("cn") 
                        && result.getDocumentElement().getFirstChild().getFirstChild().getTextContent().equals("0"))) {
                    fluxesNew.add(result.getDocumentElement().getFirstChild());
                }
            }
            for (int i2 = 0; i2 < canFluxes.getLength(); i2++) {
                if (!(canFluxes.item(i2).getFirstChild().getLocalName().equals("cn") 
                        && canFluxes.item(i2).getFirstChild().getTextContent().equals("0"))) {
                    fluxesNew.add(canFluxes.item(i2));
                }
            }           
        }
        
        return fluxesNew;
    }
    
    /**
     * Normalizes a formula.
     * @param formula           The formula
     * @param symmetricVars     Information about the symmetries in variables
     * @throws TCException      TC00X External error
     * @return                  The new formula
     */
    public Node normalizeFormula(Node formula, HashMap<String, SymmetryAction> symmetricVars) throws TCException {
    	
    	Document auxDoc = null;
    	List<Node> fluxesNew = new ArrayList<Node>();   		 
    	auxDoc = builder.newDocument();
    	Node flux = auxDoc.importNode(formula.cloneNode(true), true);
    	Node math = createElement(auxDoc, mtUri, "math");
    	math.appendChild(flux);
    	auxDoc.appendChild(math);
    	//calculates, simplies and normalizes de flux
        Document result = applyXSL(auxDoc, "simplify");
        //replace symmetric variables
        replaceSymmetries(result, symmetricVars);
        result = applyXSL(result, "end");
    	//extracts his components
        NodeList canFluxes = TCUtils.find(result.getDocumentElement(), "//mt:semantics[mt:annotation-xml/properties/id = 'term']");
        for (int i2 = 0; i2 < canFluxes.getLength(); i2++) {
            fluxesNew.add(canFluxes.item(i2));   		
        }      		
    	
    	return fluxesNew.get(0);
    }
    
    /**
     * Checks for shared summation indexes and creates all the parabolic terms depending on them.
     * @param doc               The tensorial model
     * @param origPTerms        The original parabolic terms
     * @param coords            The spatial coordinates
     * @return                  The new parabolic terms
     * @throws TCException      TC00X External error
     */
    public static NodeList separateSharedSummIndex(Document doc, NodeList origPTerms, String[] coords) throws TCException {
        DocumentFragment newPTerms = doc.createDocumentFragment();
        for (int i = 0; i < origPTerms.getLength(); i++) {
            ArrayList<String> sharedIndexes = findSharedSummIndexes(origPTerms.item(i));
            //If it does not have shared summation indexes
            if (sharedIndexes.size() == 0) {
                newPTerms.appendChild(origPTerms.item(i).cloneNode(true));
            }
            //If it have shared summation indexes deploy all the combinations to create all the parabolic terms
            else {
                ArrayList<Node> pTerms = new ArrayList<Node>();
                pTerms.add(origPTerms.item(i).cloneNode(true));
                for (int j = 0; j < sharedIndexes.size(); j++) {
                    for (int k = pTerms.size() - 1; k >= 0; k--) {
                        Node tempPTerm = pTerms.get(k).cloneNode(true);
                        pTerms.remove(k);
                        for (int l = 0; l < coords.length; l++) {
                            pTerms.add(replaceIndex(tempPTerm.cloneNode(true), sharedIndexes.get(j), coords[l]));
                        }
                    }
                }
                //Add the new parabolic terms to the list
                for (int j = 0; j < pTerms.size(); j++) {
                    newPTerms.appendChild(pTerms.get(j).cloneNode(true));
                }
            }            
        }
        return newPTerms.getChildNodes();
    }
    
    /**
     * Gets the shared summation indexes of a parabolic term.
     * @param pTerm                 The parabolic term
     * @return                      The list of shared summation indexes
     * @throws TCException          TC00X External error
     */
    private static ArrayList<String> findSharedSummIndexes(Node pTerm) throws TCException {
        ArrayList<String> sharedIndexes = new ArrayList<String>();
        int secondDerivativeIndex = Integer.parseInt(pTerm.getChildNodes().item(1).getTextContent());
        ArrayList<String> freeIndexes = new ArrayList<String>();
        for (int j = 0; j < secondDerivativeIndex; j++) {
            freeIndexes.add(indexes[j]);
        }
        Element q = (Element) pTerm.getChildNodes().item(2);
        ArrayList<Element> qAddends = new ArrayList<Element>();
        separateAddends(q, qAddends);
        //Check individually every addend
        for (int j = 0; j < qAddends.size(); j++) {
            NodeList sumIndexes = TCUtils.find(qAddends.get(j), ".//mt:mi[position() > 1]");
            for (int k = 0; k < sumIndexes.getLength(); k++) {
                String index = sumIndexes.item(k).getTextContent();
                if (!freeIndexes.contains(index)) {
                    if (TCUtils.find(qAddends.get(j), ".//mt:mi[position() > 1][text()='" + index + "']").getLength() == 1) {
                        if (!sharedIndexes.contains(index)) {
                            sharedIndexes.add(index);
                        }
                    }
                }
            }
        }
        return sharedIndexes;
    }
    
    /**
     * Replaces the index with a given value.
     * @param term              The term with mathml content
     * @param index             The index to replace
     * @param value             The value used
     * @return                  The new term
     * @throws TCException      TC00X External error
     */
    private static Node replaceIndex(Node term, String index, String value) throws TCException {
        NodeList indexList = find(term, ".//mt:mi[position() > 1][text() = '" + index + "']");
        for (int i = 0; i < indexList.getLength(); i++) {
            indexList.item(i).setTextContent(value);
        }
        return term;
    }
    
    /**
     * Checks the consistency of the tensorial model:
     * 1 - Checking the order of the fields used.
     * 2 - Check free indexes on parabolic terms.
     * @param tensModel     The tensorial model
     * @throws TCException  TC004 Incorrect mathematical content in tensorial model
     *                      TC00X External error
     */
    public static void checkConsistency(Document tensModel) throws TCException {
        String prefix = tensModel.getDocumentElement().getPrefix();
        //1 - Checking the order of the fields used
        NodeList fields = find(tensModel, "/*/" + prefix + ":tensorialField|/*/" + prefix + ":tensorialAuxiliarField");
        for (int i = 0; i < fields.getLength(); i++) {
            String field = fields.item(i).getFirstChild().getTextContent();
            int order = Integer.parseInt(fields.item(i).getFirstChild().getNextSibling().getTextContent());
            NodeList occurrences = find(tensModel, "//mt:ci[text() = '" + field 
                    + "' and not(preceding-sibling::mt:csymbol[text() = 'tr'])]|//mt:mi[text() = '" + field + "']");
            for (int j = 0; j < occurrences.getLength(); j++) {
                Element occurrence = (Element) occurrences.item(j);
                if (occurrence.getLocalName().equals("ci")) {
                    if (order > 0) {
                        String equationName = find(occurrence, "ancestor::*[local-name() = 'evolutionEquation' or " 
                                + "local-name() = 'mathematicalConstraint' or local-name() = 'auxiliarEquation']/" 
                                + prefix + ":equationName").item(0).getTextContent();
                        throw new TCException(TCException.TC004, "Field " + field + " with zero-order used incorrectly on equation " + equationName);
                    }
                }
                else {
                    if (order != (occurrence.getParentNode().getChildNodes().getLength() - 1) / 2) {
                        String equationName = find(occurrence, "ancestor::*[local-name() = 'evolutionEquation' or " 
                                + "local-name() = 'mathematicalConstraint' or local-name() = 'auxiliarEquation']/" 
                                + prefix + ":equationName").item(0).getTextContent();
                        throw new TCException(TCException.TC004, "Field " + field + " with order " + order + " used incorrectly on equation " 
                                + equationName);
                    }
                }
            }
        }
        //2 - Check free indexes on parabolic terms
        NodeList pts = find(tensModel, "//" + prefix + ":parabolicTerm");
        for (int i = 0; i < pts.getLength(); i++) {
            int secondDerivativeIndex = Integer.parseInt(pts.item(i).getChildNodes().item(1).getTextContent());
            ArrayList<String> freeIndexes = new ArrayList<String>();
            for (int j = 0; j < secondDerivativeIndex; j++) {
                freeIndexes.add(indexes[j]);
            }
            Element q = (Element) pts.item(i).getChildNodes().item(2);
            Element p = (Element) pts.item(i).getChildNodes().item(THREE);
            ArrayList<Element> qAddends = new ArrayList<Element>();
            ArrayList<Element> pAddends = new ArrayList<Element>();
            separateAddends(q, qAddends);
            separateAddends(p, pAddends);
            //Check individually every addend
            ArrayList<String> sharedSummIndexes = new ArrayList<String>();
            for (int j = 0; j < qAddends.size(); j++) {
                NodeList sumIndexes = TCUtils.find(qAddends.get(j), ".//mt:mi[position() > 1]");
                for (int k = 0; k < sumIndexes.getLength(); k++) {
                    String index = sumIndexes.item(k).getTextContent();
                    if (!freeIndexes.contains(index)) {
                        //It must appear twice or once if it is shared
                        if (TCUtils.find(qAddends.get(j), ".//mt:mi[position() > 1][text()='" + index + "']").getLength() != 2) {
                            if (j == 0 && TCUtils.find(qAddends.get(j), ".//mt:mi[position() > 1][text()='" + index + "']").getLength() == 1) {
                                sharedSummIndexes.add(index);
                            }
                            else {
                                if (!sharedSummIndexes.contains(index) && TCUtils.find(qAddends.get(j), ".//mt:mi[position() > 1][text()='"
                                        + index + "']").getLength() == 1) {
                                    throw new TCException(TCException.TC004, "Summation index must appear once in every addend of P and Q. Index \"" 
                                            + index + "\" is not a shared index"); 
                                }
                                else {
                                    throw new TCException(TCException.TC004, "Every summation index must appear twice in every addend. Index \"" 
                                            + index + "\" is not a pair index"); 
                                }
                            }
                        }
                    }
                }
            }
            for (int j = 0; j < pAddends.size(); j++) {
                NodeList sumIndexes = TCUtils.find(pAddends.get(j), ".//mt:mi[position() > 1]");
                for (int k = 0; k < sumIndexes.getLength(); k++) {
                    String index = sumIndexes.item(k).getTextContent();
                    if (!freeIndexes.contains(index)) {
                        //It must appear twice or once if it is shared
                        if (TCUtils.find(pAddends.get(j), ".//mt:mi[position() > 1][text()='" + index + "']").getLength() != 2) {
                            if (!sharedSummIndexes.contains(index) && TCUtils.find(pAddends.get(j), ".//mt:mi[position() > 1][text()='"
                                    + index + "']").getLength() == 1) {
                                throw new TCException(TCException.TC004, "Summation index must appear once in every addend of P and Q. Index \"" 
                                        + index + "\" is not a pair index"); 
                            }
                            else {
                                if (TCUtils.find(pAddends.get(j), ".//mt:mi[position() > 1][text()='" + index + "']").getLength() > 2) {
                                    throw new TCException(TCException.TC004, "Every summation index must appear twice in every addend. Index \"" 
                                            + index + "\" is not a pair index"); 
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    /**
     * Applies the XSL to a formula until an action is reached.
     * @param doc               The formula
     * @param untilAction       The action
     * @return                  The new formula
     * @throws TCException      TC00X External error
     */
    public Document applyXSL(Document doc, String untilAction) throws TCException {
    	Document resultAux = null;        
        int i = 0;
        //System.out.println("Antes xsl:" + domToString(doc));
        resultAux = doc;
        String action = "sum";
        String actualAction = "";
        String actualResult = "";
        while (!untilAction.equals(action) && i < XSLLIMIT) {
        	//get actual data
            actualAction = TCUtils.find(resultAux.getDocumentElement(), 
                    "/mt:math/mt:semantics/mt:annotation-xml/properties/action").item(0).getTextContent();

            actualResult = domToString(resultAux);

            //apply xsl
            String xmlAux = xmlTransform(actualResult, null);
            
            //get new data
            resultAux = stringToDom(xmlAux);
            action = TCUtils.find(resultAux.getDocumentElement(), 
                    "/mt:math/mt:semantics/mt:annotation-xml/properties/action").item(0).getTextContent();

            //if old action is simplifly checks if we have to continue with simplifications
            if ("simplify".equals(actualAction)) {
            	//if simplification process doesn't change the xml then is time to normalize
            	if (actualResult.equals(domToString(resultAux))) {
            	    TCUtils.find(resultAux.getDocumentElement(), 
            	            "/mt:math/mt:semantics/mt:annotation-xml/properties/action").item(0).setTextContent("end");
            	    action = "end";
            	}
            }
            i++;
        }
        if (!untilAction.equals(action)) {
            throw new TCException(TCException.TC00X, "xsl applied more than " + XSLLIMIT + " times");
        }
        return resultAux;
    }
    
    /**
     * Group fluxes by their derivative coordinates.
     * @param fluxes                The fluxes
     * @return                      The grouping classification
     * @throws TCException          TC00X External error
     */
    public Hashtable<String, List<Node>> groupFluxes(List<Node> fluxes) throws TCException {
    	Hashtable<String, List<Node>> groups = new Hashtable<String , List<Node>>();
    	
        for (int i = 0; i < fluxes.size(); i++) {
            Node flux = fluxes.get(i);
            String derivativeCoord = TCUtils.find(flux, "./mt:annotation-xml/properties/derivativeCoordinate").item(0).getTextContent();
            Node formula = TCUtils.find(flux, "*[1]").item(0);
            if (groups.containsKey(derivativeCoord)) {
                groups.get(derivativeCoord).add(formula);
            }
            else {
                List<Node> list = new ArrayList<Node>();
                list.add(formula);
                groups.put(derivativeCoord, list);
            }            	    		
        }            	    	
	  
    	return groups;
    }
    
    /**
     * Group parabolic terms by their derivative coordinates.
     * @param terms                  The parabolic terms
     * @param spatialCoordinates    The spatial coordinates
     * @return                      The grouping classification
     * @throws TCException          TC00X External error
     */
    public Hashtable<String, List<Node>> groupPT(List<Node> terms, String[] spatialCoordinates) throws TCException {
        Hashtable<String, List<Node>> groups = new Hashtable<String , List<Node>>();
        
        for (int i = 0; i < terms.size(); i++) {
            Node term = terms.get(i);
            String derivativeCoord = "";
            if (TCUtils.find(term, "./mt:annotation-xml/properties/derivativeCoordinate").getLength() > 0) {
                derivativeCoord = TCUtils.find(term, "./mt:annotation-xml/properties/derivativeCoordinate").item(0).getTextContent();
            }
            String secDerivativeCoord = "";
            if (TCUtils.find(term, "./mt:annotation-xml/properties/secDerivativeCoordinate").getLength() > 0) {
                secDerivativeCoord = TCUtils.find(term, "./mt:annotation-xml/properties/secDerivativeCoordinate").item(0).getTextContent();
            }
            Node formula = TCUtils.find(term, "*[1]").item(0);
            if (derivativeCoord.equals("") && secDerivativeCoord.equals("")) {
                for (int j = 0; j < spatialCoordinates.length; j++) {
                    derivativeCoord = spatialCoordinates[j];
                    for (int k = 0; k < spatialCoordinates.length; k++) {
                        secDerivativeCoord = spatialCoordinates[k];
                        if (groups.containsKey(derivativeCoord + "&" + secDerivativeCoord)) {
                            groups.get(derivativeCoord + "&" + secDerivativeCoord).add(formula);
                        }
                        else {
                            List<Node> list = new ArrayList<Node>();
                            list.add(formula);
                            groups.put(derivativeCoord + "&" + secDerivativeCoord, list);
                        }
                    }
                }
            }
            else {
                if (derivativeCoord.equals("")) {
                    for (int j = 0; j < spatialCoordinates.length; j++) {
                        derivativeCoord = spatialCoordinates[j];
                        if (groups.containsKey(derivativeCoord + "&" + secDerivativeCoord)) {
                            groups.get(derivativeCoord + "&" + secDerivativeCoord).add(formula);
                        }
                        else {
                            List<Node> list = new ArrayList<Node>();
                            list.add(formula);
                            groups.put(derivativeCoord + "&" + secDerivativeCoord, list);
                        }
                    }
                } 
                else {
                    if (secDerivativeCoord.equals("")) {
                        for (int j = 0; j < spatialCoordinates.length; j++) {
                            secDerivativeCoord = spatialCoordinates[j];
                            if (groups.containsKey(derivativeCoord + "&" + secDerivativeCoord)) {
                                groups.get(derivativeCoord + "&" + secDerivativeCoord).add(formula);
                            }
                            else {
                                List<Node> list = new ArrayList<Node>();
                                list.add(formula);
                                groups.put(derivativeCoord + "&" + secDerivativeCoord, list);
                            }
                        }
                    }
                    else {
                        if (groups.containsKey(derivativeCoord + "&" + secDerivativeCoord)) {
                            groups.get(derivativeCoord + "&" + secDerivativeCoord).add(formula);
                        }
                        else {
                            List<Node> list = new ArrayList<Node>();
                            list.add(formula);
                            groups.put(derivativeCoord + "&" + secDerivativeCoord, list);
                        }  
                    }
                }
            }
        }                       
      
        return groups;
    }

    /**
     * Separate the addend of a formula recursively.
     * @param formula           The formula
     * @param addends           The addend list
     * @throws TCException      TC00X - External error
     */
    public static void separateAddends(Node formula, ArrayList<Element> addends) throws TCException {
        NodeList plusList = find(formula, ".//mt:apply[mt:plus] | .//mt:apply[mt:minus]");
        if (plusList.getLength() == 0) {
            addends.add((Element) formula.cloneNode(true));
        }
        else {
            NodeList children = plusList.item(0).getChildNodes();
            for (int i = 1; i < children.getLength(); i++) {
                Element newFormula = (Element) formula.cloneNode(true);
                Element parent = (Element) find(newFormula, ".//mt:apply[mt:plus] | .//mt:apply[mt:minus]").item(0);
                Element child = (Element) parent.getChildNodes().item(i);
                parent.getParentNode().replaceChild(child.cloneNode(true), parent);
                separateAddends(newFormula, addends);
            }
        }
    }
    
    /**
     * A method for the format of the output.
     * 
     * @param document          The document to be formatted
     * @return                  The document formatted
     * @throws TCException      TC00X - External error
     */
    public static String indent(String document) throws TCException {
        try {
            DocumentBuilder docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document doc = docBuilder.parse(new ByteArrayInputStream(document.getBytes("UTF-8")));

            OutputFormat format = new OutputFormat(doc);
            format.setIndenting(true);
            format.setIndent(INDENTATION);
            Writer output = new StringWriter();
            XMLSerializer serializer = new XMLSerializer(output, format);
            serializer.serialize(doc);

            return output.toString();
        }
        catch (Throwable e) {
            throw new TCException(TCException.TC00X, e.toString());
        }
    }
    
    
    /**
     * Creates a field name from a tensorial field and an coordinate combination.
     * @param tensField         The tensorial field
     * @param comb              The combination
     * @return                  The new field name
     */
    public static String fieldName(Node tensField, String[] comb) {
        String name = tensField.getChildNodes().item(0).getTextContent();
        for (int k = 0; k < comb.length; k++) {
            name = name + comb[k];  
        }
        return name;
    }
    
    /**
     * Creates a parameter name from a tensorial parameter and an coordinate combination.
     * @param parameter         The tensorial parameter
     * @param comb              The combination
     * @return                  The new field name
     */
    public static String parameterName(String parameter, String[] comb) {
        String name = parameter;
        for (int k = 0; k < comb.length; k++) {
            name = name + comb[k];  
        }
        return name;
    }
    
    /**
     * Merge two combinations en a new one.
     * @param c1    Combination one
     * @param c2    Combination two
     * @return      The new combination
     */
    public static String[] mergeCombinations(String[] c1, String[] c2) {
        String[]comb = new String[c1.length + c2.length];
        for (int k = 0; k < c1.length; k++) {
            comb[k] = c1[k];  
        }
        for (int k = 0; k < c2.length; k++) {
            comb[k + c1.length] = c2[k];  
        }
        return comb;
    }
}
