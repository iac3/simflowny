package eu.iac3.mathMS.tensorialToCoordinatePlugin;
/** 
 * TCException is the class Exception launched by the 
 * {@link TensorialToCoordinate TensorialToCoordinate} interface.
 * The exceptions that this class return are:
 * TC001	Not a valid XML Document
 * TC002	XML document does not match XML Schema
 * TC003	XML transform error
 * TC004    Incorrect mathematical content in tensorial model
 * TC00X    External error
 * @author      ----
 * @version     ----
 */
public class TCException extends Exception {

    private static final long serialVersionUID = 1L;
	
    public static final String TC001 = "Not a valid XML Document";
    public static final String TC002 = "XML document does not match XML Schema";
    public static final String TC003 = "XML transform error";
    public static final String TC004 = "Incorrect mathematical content in tensorial model";
    public static final String TC00X = "External error";
	
    //External error message
    private String extError;
	  /** 
	     * Constructor for known messages.
	     *
	     * @param msg  the message	     
	     */
    TCException(String msg) {
    	super(msg);
    }
	  
	  /** 
	     * Constructor.
	     *
	     * @param intMsg  the internal error message	    
	     * @param extMsg  the external error message
	     */
    public TCException(String intMsg, String extMsg) {
		super(intMsg);
        extError = extMsg;
    }
	  /** 
     * Returns the error code.
     *
     * @return the error code	     
     */
    public String getErrorCode() {
    	return super.getMessage();
    }
	  /** 
	     * Returns the error message as a string.
	     *
	     * @return the error message	     
	     */
    public String getMessage() {
    	if (extError == null) {
            return "TCException: " + super.getMessage();
    	}
    	else {
            return "TCException: " + super.getMessage() + " (" + extError + ")";
    	}
    }  
}
