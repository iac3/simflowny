package eu.iac3.mathMS.tensorialToCoordinatePlugin.utils;

/**
 * This class represents a symmetry action.
 * @author bminano
 *
 */
public class SymmetryAction {

    private String action;
    private String baseVariable;
    
    /**
     * Constructor.
     * @param act       Action
     * @param baseVar   Base variable
     */
    public SymmetryAction(String act, String baseVar) {
        action = act;
        baseVariable = baseVar;
    }

    public String getAction() {
        return action;
    }

    public String getBaseVariable() {
        return baseVariable;
    }
    
}
