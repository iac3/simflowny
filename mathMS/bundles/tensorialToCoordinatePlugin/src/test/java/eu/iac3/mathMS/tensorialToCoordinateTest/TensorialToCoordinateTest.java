/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.tensorialToCoordinateTest;

import eu.iac3.mathMS.tensorialToCoordinatePlugin.TCException;
import eu.iac3.mathMS.tensorialToCoordinatePlugin.TensorialToCoordinate;
import eu.iac3.mathMS.tensorialToCoordinatePlugin.TensorialToCoordinateImpl;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;


/** 
 * LatexPDFGeneratorTest class is a junit testcase to
 * test the {@link LatexPDFGenerator LatexPDFGenerator}.
 * It has eight tests:
 * Four tests to test the LaTeX generation, one 
 * for each schema type. 
 * Four tests to test the PDF generation, one
 * for each schema type.
 * @author      ----
 * @version     ----
 */
public class TensorialToCoordinateTest extends TestCase {
	     /**
	      * Create the test case.
	      *
	      * @param testName name of the test case
	      */
    public TensorialToCoordinateTest(String testName) {
	         super(testName);
    }

	     /**
	      * creates a new testsuite.
	      * @return the suite of tests being tested.
	      */
    public static Test suite() {
        return new TestSuite(TensorialToCoordinateTest.class);
    }

	     
	     
     /**            
      * Checks the {@link tensorialToCanonical tensorialToCanonical()}.
      */
    public void testTensorialCanonicalPDE() {
    	
        TensorialToCoordinate ttc = new TensorialToCoordinateImpl(null, false);
		
        String[] spaceCoords = {"x", "y", "z"};		
        String xml;
        try {
            xml = new String(getXml("src/test/resources/tensorialPhysicalModel.xml"), "UTF-8");
            String result = ttc.tensorialToCanonical(xml, spaceCoords, "t");
            String canonical = new String(getXml("src/test/resources/canonicalPhysicalModel.xml"), "UTF-8");
            assertEquals(canonical, result);
        } 
        catch (Exception e) {
            fail("Error testing tensorial to canonical: " + e.getMessage());
        }
    }
	
    /**
     * Check the inconsistencies of the model.
     */
    public void testConsistencyCheck() {
        
        TensorialToCoordinate ttc = new TensorialToCoordinateImpl(null, false);
        
        String[] spaceCoords = {"x", "y", "z"};     
        String xml;
        try {
            xml = new String(getXml("src/test/resources/tensorialInconsistent1.xml"), "UTF-8");
            ttc.tensorialToCanonical(xml, spaceCoords, "t");
            fail("Inconsistent model converted");
        } 
        catch (TCException e) {
            assertEquals(TCException.TC004, e.getErrorCode());
        }
        catch (Exception e) {
            fail("Error testing testConsistencyCheck: " + e.getMessage());
        }
        try {
            xml = new String(getXml("src/test/resources/tensorialInconsistent2.xml"), "UTF-8");
            ttc.tensorialToCanonical(xml, spaceCoords, "t");
            fail("Inconsistent model converted");
        } 
        catch (TCException e) {
            assertEquals(TCException.TC004, e.getErrorCode());
        }
        catch (Exception e) {
            fail("Error testing testConsistencyCheck: " + e.getMessage());
        }
    }
    
    /**
     * Check the inconsistencies of the model.
     */
    public void testFormulacheck() {
        
        TensorialToCoordinate ttc = new TensorialToCoordinateImpl(null, false);
             
        String xml;
        try {
            xml = new String(getXml("src/test/resources/formulaCheck.xml"), "UTF-8");
            assertTrue(ttc.checkTensorialFormula(xml, null, 1, 0, false));
        } 
        catch (Exception e) {
            fail("Error testing testConsistencyCheck: " + e.getMessage());
        }
        try {
            xml = new String(getXml("src/test/resources/formulaCheck1.xml"), "UTF-8");
            ttc.checkTensorialFormula(xml, null, 0, 0, false);
            fail("Inconsistent levi-civita operator check passed");
        } 
        catch (TCException e) {
            assertEquals(TCException.TC004, e.getErrorCode());
        }
        catch (Exception e) {
            fail("Error testing testConsistencyCheck: " + e.getMessage());
        }
        try {
            xml = new String(getXml("src/test/resources/formulaCheck2.xml"), "UTF-8");
            ttc.checkTensorialFormula(xml, null, 0, 0, false);
            fail("Inconsistent Kronecker operator check passed");
        } 
        catch (TCException e) {
            assertEquals(TCException.TC004, e.getErrorCode());
        }
        catch (Exception e) {
            fail("Error testing testConsistencyCheck: " + e.getMessage());
        }
        try {
            xml = new String(getXml("src/test/resources/formulaCheck3.xml"), "UTF-8");
            ttc.checkTensorialFormula(xml, null, 2, 0, false);
            fail("Inconsistent free indexes check passed");
        } 
        catch (TCException e) {
            assertEquals(TCException.TC004, e.getErrorCode());
        }
        catch (Exception e) {
            fail("Error testing testConsistencyCheck: " + e.getMessage());
        }
        try {
            xml = new String(getXml("src/test/resources/formulaCheck4.xml"), "UTF-8");
            ttc.checkTensorialFormula(xml, null, 2, 0, false);
            fail("Inconsistent summation indexes check passed");
        } 
        catch (TCException e) {
            assertEquals(TCException.TC004, e.getErrorCode());
        }
        catch (Exception e) {
            fail("Error testing testConsistencyCheck: " + e.getMessage());
        }
        try {
            xml = new String(getXml("src/test/resources/formulaCheck5.xml"), "UTF-8");
            ttc.checkTensorialFormula(xml, null, 2, 1, false);
        } 
        catch (Exception e) {
            fail("Error testing testConsistencyCheck: " + e.getMessage());
        }
        try {
            xml = new String(getXml("src/test/resources/formulaCheck6.xml"), "UTF-8");
            ttc.checkTensorialFormula(xml, null, 0, 2, false);
        } 
        catch (Exception e) {
            fail("Error testing testConsistencyCheck: " + e.getMessage());
        }
        try {
            xml = new String(getXml("src/test/resources/formulaCheck7.xml"), "UTF-8");
            ttc.checkTensorialFormula(xml, null, 1, 1, false);
            fail("Inconsistent summation indexes check passed");
        } 
        catch (TCException e) {
            assertEquals(TCException.TC004, e.getErrorCode());
        }
        catch (Exception e) {
            fail("Error testing testConsistencyCheck: " + e.getMessage());
        }
        try {
            xml = new String(getXml("src/test/resources/formulaCheck8a.xml"), "UTF-8");
            String xml2 = new String(getXml("src/test/resources/formulaCheck8b.xml"), "UTF-8");
            ttc.checkTensorialFormula(xml, xml2, 3, 0, true);
        } 
        catch (Exception e) {
            fail("Error testing testConsistencyCheck: " + e.getMessage());
        }
        try {
            xml = new String(getXml("src/test/resources/formulaCheck9a.xml"), "UTF-8");
            String xml2 = new String(getXml("src/test/resources/formulaCheck9b.xml"), "UTF-8");
            ttc.checkTensorialFormula(xml, xml2, 3, 0, true);
            fail("Inconsistent summation indexes check passed");
        } 
        catch (TCException e) {
            assertEquals(TCException.TC004, e.getErrorCode());
        }
        catch (Exception e) {
            fail("Error testing testConsistencyCheck: " + e.getMessage());
        }
    }
    
    /**            
     * Checks the {@link tensorialToCanonical tensorialToCanonical()} method with parabolic terms.
     */
    public void testParabolicTerms() {
           
        TensorialToCoordinate ttc = new TensorialToCoordinateImpl(null, false);
           
        String[] spaceCoords = {"x", "y"};     
        String xml;
        try {
            xml = new String(getXml("src/test/resources/tensorialPhysicalModelPT.xml"), "UTF-8");
            String result = ttc.tensorialToCanonical(xml, spaceCoords, "t");
            String canonical = new String(getXml("src/test/resources/canonicalPhysicalModelPT.xml"), "UTF-8");
            assertEquals(canonical, result);
        } 
        catch (Exception e) {
            fail("Error testing parabolic terms: " + e.getMessage());
        }
    }
    
    /**            
     * Checks the {@link tensorialToCanonical tensorialToCanonical()} method with characteristic decomposition.
     */
    public void testCharDecomposition() {
           
        TensorialToCoordinate ttc = new TensorialToCoordinateImpl(null, false);
           
        String[] spaceCoords = {"x", "y", "z"};     
        String xml;
        try {
            xml = new String(getXml("src/test/resources/tensorialPhysicalModelCD.xml"), "UTF-8");
            String result = ttc.tensorialToCanonical(xml, spaceCoords, "t");
            String canonical = new String(getXml("src/test/resources/canonicalPhysicalModelCD.xml"), "UTF-8");
            assertEquals(canonical, result);
        } 
        catch (Exception e) {
            fail("Error testing characteristic decomposition: " + e.getMessage());
        }
    }
    
    
    /**            
     * Checks the {@link tensorialToCanonical tensorialToCanonical()} symmetry behavior.
     */
    public void testSymmetries() {
       
        TensorialToCoordinate ttc = new TensorialToCoordinateImpl(null, false);
       
        String[] spaceCoords = {"x", "y", "z"};     
        String xml;
        try {
            xml = new String(getXml("src/test/resources/tensorialPhysicalModelSym.xml"), "UTF-8");
            String result = ttc.tensorialToCanonical(xml, spaceCoords, "t");
            String canonical = new String(getXml("src/test/resources/canonicalPhysicalModelSym.xml"), "UTF-8");
            assertEquals(canonical, result);
        } 
        catch (Exception e) {
            fail("Error testing tensorial to canonical: " + e.getMessage());
        }
    }
    
   /* public void testManual() {
        TensorialToCoordinate ttc = new TensorialToCoordinateImpl(null, false);
        String[] spaceCoords = {"x", "y", "z"};     
        String xml;
        try {
            System.out.println("Manual");
            xml = new String(getXml("src/test/resources/prueba.xml"), "UTF-8");
            String result = ttc.tensorialToCanonical(xml, spaceCoords, "t");
            FileOutputStream fos; 
            DataOutputStream dos;
            File file = new File("MyFile.xml");
            fos = new FileOutputStream(file);
            dos = new DataOutputStream(fos);
            dos.writeChars(result);
        } 
        catch (Exception e) {
            e.printStackTrace();
        }
    }*/
    	    
    /**
     * returns an array of bytes of a file.
     * @param filePath The path to the file.
     * @return return the content of a file as an array of bytes.
     */
    private static byte[] getXml(String filePath) {
    	ByteArrayOutputStream baos = new ByteArrayOutputStream();
    	InputStream is;    	
    	byte[] buf = null;
    	try {
            is = new FileInputStream(filePath);
            final int size = is.available();
            buf = new byte[size];
            int len = is.read(buf);
            while (len != -1) {
            	baos.write(buf, 0, len);
            	len = is.read(buf);
            }				
            is.close();
            baos.flush();
            baos.close();
            buf = baos.toByteArray();
    	} 
    	catch (FileNotFoundException e) {
    	    e.printStackTrace();
    	} 
    	catch (IOException e) {
    	    e.printStackTrace();
    	}

    	return buf;
    }
}
