<?xml version='1.0' encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:m="http://www.w3.org/1998/Math/MathML" 
                xmlns:exsl="http://exslt.org/common"
                extension-element-prefixes="exsl"
                xmlns:mt="http://www.w3.org/1998/Math/MathML"
                version="1.0">

<!-- ====================================================================== -->
<!-- Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license  -->
<!-- ====================================================================== -->

    <xsl:strip-space elements="m:*"/>
<!--_______________________________________________-->
<!--_________________principal_____________________-->
<!--_______________________________________________-->
    <xsl:template match="m:math[not(@mode) or @mode='inline'][not(@display)] | m:math[@display='inline']">
        <m:math>
        <xsl:apply-templates/>        
        </m:math>
    </xsl:template>
    
    <xsl:template match="m:math[@display='block'] | m:math[@mode='display'][not(@display)]">
        <m:math>
            <xsl:apply-templates/>        
        </m:math>
    </xsl:template>

<!--_____________________________________________________________________-->
<!--__________________accion principal expandir suma_____________________-->
<!--_____________________________________________________________________-->
    <xsl:template match="m:semantics[m:annotation-xml/properties/action = 'sum']">
        <!--ponemos el semantics principal calculando si hay que seguir expandiendo o se pasa a calculo-->
        <mt:semantics>
            <!--se crea el apply principal que sumara cada fragmento expandido-->
            <xsl:apply-templates select="*[1]" mode="expandCopy"/>
            <xsl:variable name="actionType">
                <xsl:choose>
                    <xsl:when test="count(//m:annotation-xml[properties/action = 'expand'][1]/properties/indexToReplace) &gt; 0">sum</xsl:when>
                    <xsl:otherwise>calculate</xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <mt:annotation-xml encoding="xml">
                <properties>
                    <id><xsl:value-of select="m:annotation-xml/properties/id"/></id>
                    <action><xsl:value-of select="$actionType"/></action>
                    <derivativeIndex><xsl:value-of select="m:annotation-xml/properties/derivativeIndex"/></derivativeIndex>
                    <xsl:if test="m:annotation-xml/properties/secDerivativeIndex">
                        <secDerivativeIndex><xsl:value-of select="m:annotation-xml/properties/secDerivativeIndex"/></secDerivativeIndex>
                    </xsl:if>
                    <coordinates>
                        <xsl:for-each select="m:annotation-xml/properties/coordinates/coordinate">
                            <coordinate><xsl:value-of select="."/></coordinate>
                        </xsl:for-each>
                    </coordinates>
                    <indexNumber><xsl:value-of select="m:annotation-xml/properties/indexNumber"/></indexNumber>
                </properties>
            </mt:annotation-xml>
         </mt:semantics>            
    </xsl:template>
    
    <!--____________________________________________________________________________-->
    <!--__________________accion secundaria expandir suma___________________________-->
    <!--__________________va copiando toda la entrada hasta que tiene que expandir__-->
    <!--____________________________________________________________________________-->    
    <xsl:template match="*" mode="expandCopy">
        <xsl:choose>
            <xsl:when test="name() = 'mt:semantics' and child::m:annotation-xml/properties/action = 'expand' 
                and (.//mt:semantics[count(m:annotation-xml[properties/action = 'expand']) = 0] or count(.//mt:semantics) = 0)">
                <!--se crea el apply principal que sumara cada fragmento expandido-->
                <mt:apply>
                    <mt:plus/>
                    <xsl:apply-templates select="."/>
                </mt:apply>
            </xsl:when>            
            <xsl:otherwise>
                <xsl:element name="{name()}">
                    <xsl:for-each select="@*">
                        <xsl:attribute name="{name()}">
                            <xsl:value-of select="."/>
                        </xsl:attribute>
                    </xsl:for-each>
                    <xsl:apply-templates select="*|text()" mode="expandCopy"/>
                </xsl:element>
            </xsl:otherwise>
        </xsl:choose>      
    </xsl:template>
   
<!--_________________________________________________________-->
<!--___________Expansion de cada fragmento de formula________-->
    <xsl:template match="m:semantics[m:annotation-xml/properties/action = 'expand']">
        <!--si ya no hay mas indices que expandir pasamos a calculo-->
        <xsl:variable name="actionType">
            <xsl:choose>
                <xsl:when test="count(m:annotation-xml/properties/indexRemainingList/index) &gt; 0">expand</xsl:when>
                <xsl:otherwise>calculate</xsl:otherwise>
            </xsl:choose>                
        </xsl:variable>       
                <!--Para cada coordenada se copia el fragmento y se substituye el indice por la coordenada-->
                <xsl:for-each select="m:annotation-xml/properties/coordinates/coordinate">
                    <mt:semantics>
                    <xsl:apply-templates select="../../../../*[1]" mode="expand">
                        <xsl:with-param name="index"><xsl:value-of select="../../indexToReplace"/></xsl:with-param>
                        <xsl:with-param name="coordinate"><xsl:value-of select="."/></xsl:with-param>
                    </xsl:apply-templates>
                        <!--para el nuevo fragmento se actuliza su meta informacion-->
                        <mt:annotation-xml encoding="xml">
                            <properties>
                                <id><xsl:value-of select="../../id"/></id>
                                <action><xsl:value-of select="$actionType"/></action>
                                <derivativeIndex><xsl:value-of select="../../derivativeIndex"/></derivativeIndex>
                                <xsl:if test="../../secDerivativeIndex">
                                    <secDerivativeIndex><xsl:value-of select="../../secDerivativeIndex"/></secDerivativeIndex>
                                </xsl:if>
                                <xsl:choose>
                                    <xsl:when test="$actionType='expand'">   
                                        <xsl:choose>
                                            <xsl:when test="count(../../derivativeCoordinate) &gt; 0">
                                                <derivativeCoordinate><xsl:value-of select="../../derivativeCoordinate"/></derivativeCoordinate>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <xsl:if test="../../derivativeIndex = ../../indexToReplace">
                                                    <derivativeCoordinate><xsl:value-of select="."/></derivativeCoordinate>
                                                </xsl:if>
                                            </xsl:otherwise>
                                        </xsl:choose>                                        
                                        <xsl:choose>
                                            <xsl:when test="count(../../secDerivativeCoordinate) &gt; 0">
                                                <secDerivativeCoordinate><xsl:value-of select="../../secDerivativeCoordinate"/></secDerivativeCoordinate>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <xsl:if test="../../secDerivativeIndex = ../../indexToReplace">
                                                    <secDerivativeCoordinate><xsl:value-of select="."/></secDerivativeCoordinate>
                                                </xsl:if>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                        <indexToReplace><xsl:value-of select="../../indexRemainingList/index[1]"/></indexToReplace>
                                        <coordinates>
                                            <xsl:for-each select="../../coordinates/coordinate">
                                                <coordinate><xsl:value-of select="."/></coordinate>
                                            </xsl:for-each>                                                                
                                        </coordinates>
                                        <indexRemainingList>
                                            <xsl:for-each select="../../indexRemainingList/index">
                                                <xsl:if test="position() &gt; 1"><index><xsl:value-of select="."/></index></xsl:if>
                                            </xsl:for-each>
                                        </indexRemainingList>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:choose>
                                            <xsl:when test="count(../../derivativeCoordinate) &gt; 0">
                                                <derivativeCoordinate><xsl:value-of select="../../derivativeCoordinate"/></derivativeCoordinate>
                                            </xsl:when>
                                            <xsl:when test="../../derivativeIndex = ../../indexToReplace">
                                                <derivativeCoordinate><xsl:value-of select="."/></derivativeCoordinate>
                                            </xsl:when>
                                            <!--xsl:otherwise>
                                                <derivativeCoordinate>#ERROR#</derivativeCoordinate>
                                                </xsl:otherwise-->
                                        </xsl:choose>  
                                        <xsl:choose>
                                            <xsl:when test="count(../../secDerivativeCoordinate) &gt; 0">
                                                <secDerivativeCoordinate><xsl:value-of select="../../secDerivativeCoordinate"/></secDerivativeCoordinate>
                                            </xsl:when>
                                            <xsl:when test="../../secDerivativeIndex = ../../indexToReplace">
                                                <secDerivativeCoordinate><xsl:value-of select="."/></secDerivativeCoordinate>
                                            </xsl:when>
                                            <!--xsl:otherwise>
                                                <derivativeCoordinate>#ERROR#</derivativeCoordinate>
                                                </xsl:otherwise-->
                                        </xsl:choose>  
                                        <coordinates>
                                            <xsl:for-each select="../../coordinates/coordinate">
                                                <coordinate><xsl:value-of select="."/></coordinate>
                                            </xsl:for-each>                                                                
                                        </coordinates>
                                    </xsl:otherwise>
                                </xsl:choose>                                        
                            </properties>        
                        </mt:annotation-xml>
                    </mt:semantics>
                </xsl:for-each>   
    </xsl:template>
    
    <!-- plantilla que copia el fragmento de mathml de entrada y substituye index por coordinate en la copia-->
    <xsl:template match="*" mode="expand">
        <xsl:param name="index"></xsl:param>
        <xsl:param name="coordinate"></xsl:param>
        <!--si es un tag de texto miramos si hay que hacer la substitucion por la coordenada-->
        <xsl:choose>
            <xsl:when test="text()">
                <xsl:element name="{name()}">
                <xsl:choose>
                    <xsl:when test=". =$index">
                        <xsl:value-of select="$coordinate"/>
                    </xsl:when>
                    <xsl:otherwise><xsl:value-of select="."/></xsl:otherwise>
                </xsl:choose>
                </xsl:element>    
            </xsl:when>
            <!--si es un tag no de texto lo copiamos con sus atributos y seguimos al siguiente recursivamente-->
            <xsl:otherwise>
                <xsl:element name="{name()}">
                    <xsl:for-each select="@*">
                        <xsl:attribute name="{name()}">
                            <xsl:value-of select="."/>
                        </xsl:attribute>
                    </xsl:for-each>
                    <xsl:apply-templates select="*|text()" mode="expand">
                        <xsl:with-param name="index"><xsl:value-of select="$index"/></xsl:with-param>
                        <xsl:with-param name="coordinate"><xsl:value-of select="$coordinate"/></xsl:with-param> 
                    </xsl:apply-templates>
                </xsl:element>
            </xsl:otherwise>
        </xsl:choose>        
    </xsl:template>
    <!--______________________________________________________________________________________________________________-->
    <!--_____________________________________________________________________-->
    <!--__________________accion principal calcular__________________________-->
    <!--_____________________________________________________________________-->
    <xsl:template match="m:semantics[m:annotation-xml/properties/action = 'calculate']">
        <!--ponemos el semantics principal despues de calcular hay que simplificar-->
        <mt:semantics>
            <!--se crea el apply principal que sumara cada fragmento expandido-->
             <xsl:apply-templates select="*[1]" mode="calculate"/>
            <mt:annotation-xml encoding="xml">
                <properties>
                    <id><xsl:value-of select="m:annotation-xml/properties/id"/></id>
                    <action>normalize</action>                    
                    <derivativeIndex><xsl:value-of select="m:annotation-xml/properties/derivativeIndex"/></derivativeIndex>
                    <xsl:if test="m:annotation-xml/properties/secDerivativeIndex">
                        <secDerivativeIndex><xsl:value-of select="m:annotation-xml/properties/secDerivativeIndex"/></secDerivativeIndex>
                    </xsl:if>
                    <derivativeCoordinate><xsl:value-of select="m:annotation-xml/properties/derivativeCoordinate"/></derivativeCoordinate>
                    <xsl:if test="m:annotation-xml/properties/secDerivativeCoordinate">
                        <secDerivativeCoordinate><xsl:value-of select="m:annotation-xml/properties/secDerivativeCoordinate"/></secDerivativeCoordinate>
                    </xsl:if>
                </properties>
            </mt:annotation-xml>
        </mt:semantics>            
    </xsl:template>
    
    <!--_________________________________________________________-->
    <!--___________plantilla general de calculo________-->
    <!-- Dependiendo del nombre del operador llama a una u otra plantilla para realizar el cálculo-->
    <xsl:template match="*" mode="calculate">
        <xsl:choose>
            <xsl:when test="name() = 'mt:ci' and child::m:mmultiscripts[1]/m:mi[1] = 'ε_LC'">
                <xsl:apply-templates select="." mode="calculatee"></xsl:apply-templates>
            </xsl:when>
            <xsl:when test="name() = 'mt:ci' and child::m:mmultiscripts[1]/m:mi[1] = 'δ_K'">
                <xsl:apply-templates select="." mode="calculated"></xsl:apply-templates>
            </xsl:when>
            <xsl:when test="name() = 'mt:ci' and child::m:mmultiscripts[1]/m:mi[1] = 'ℑ'">
                <xsl:apply-templates select="." mode="calculateJ"></xsl:apply-templates>
            </xsl:when>
            <xsl:when test="name() = 'mt:apply' and child::m:csymbol[1] = 'tr'">
                <xsl:apply-templates select="." mode="calculateTR"></xsl:apply-templates>
            </xsl:when>
            <xsl:otherwise>
                <xsl:element name="{name()}">
                    <xsl:for-each select="@*">
                        <xsl:attribute name="{name()}">
                            <xsl:value-of select="."/>
                        </xsl:attribute>
                    </xsl:for-each>
                    <xsl:apply-templates select="*|text()" mode="calculate"/>
                </xsl:element>
            </xsl:otherwise>
        </xsl:choose>      
    </xsl:template>
    
    <!--_________________________funcion que calcula Levi-Civita de n indices______________________-->
    <xsl:template match="m:ci[m:mmultiscripts[1]/m:mi[1] = 'ε_LC']" mode="calculatee">
        <xsl:variable name="n">
            <xsl:value-of select="count(m:mmultiscripts[1]/m:mi) - 1"/>
        </xsl:variable>
        
        <xsl:variable name="valorE">
            <xsl:call-template name="productI">
                <xsl:with-param name="n" select="$n" />
                <xsl:with-param name="i" select="1" />
            </xsl:call-template>
        </xsl:variable>
        <!--ponemos el valor calculado en lugar del ci-->
        <xsl:choose>
            <xsl:when test="$valorE = -1">
                <mt:apply>
                    <mt:minus/>
                    <mt:cn>1</mt:cn>
                </mt:apply>
            </xsl:when>
            <xsl:when test="$valorE = -0">
                <mt:cn>0</mt:cn>
            </xsl:when>
            <xsl:otherwise>                
                <xsl:element name="mt:cn">
                    <xsl:value-of select="$valorE"/>
                </xsl:element>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <!-- External infinite product (Levi-Civita) -->
    <xsl:template name="productI">
        <xsl:param name="n"/>
        <xsl:param name="i"/>
        <xsl:choose>
            <xsl:when test="($n - 1) = $i">
                <xsl:variable name="fact">
                    <xsl:call-template name="factorial">
                        <xsl:with-param name="number" select="$i" />
                    </xsl:call-template>
                </xsl:variable>
                <xsl:variable name="prod">
                    <xsl:call-template name="productJ">
                        <xsl:with-param name="n" select="$n" />
                        <xsl:with-param name="i" select="$i" />
                        <xsl:with-param name="j" select="$i+1" />
                    </xsl:call-template>
                </xsl:variable>
                <xsl:value-of select="$prod div $fact" />
            </xsl:when>
            <xsl:otherwise>
                <xsl:variable name="fact">
                    <xsl:call-template name="factorial">
                        <xsl:with-param name="number" select="$i" />
                    </xsl:call-template>
                </xsl:variable>
                <xsl:variable name="prod">
                    <xsl:call-template name="productJ">
                        <xsl:with-param name="n" select="$n" />
                        <xsl:with-param name="i" select="$i" />
                        <xsl:with-param name="j" select="$i+1" />
                    </xsl:call-template>
                </xsl:variable>
                <xsl:variable name="actual">
                    <xsl:value-of select="$prod div $fact" />
                </xsl:variable>
                <xsl:variable name="x">
                    <xsl:call-template name="productI">
                        <xsl:with-param name="n" select="$n" />
                        <xsl:with-param name="i" select="$i+1" />
                    </xsl:call-template>
                </xsl:variable>
                <xsl:value-of select="$actual * $x" />
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <!-- Internal infinite product -->
    <xsl:template name="productJ">
        <xsl:param name="n"/>
        <xsl:param name="i"/>
        <xsl:param name="j"/>
        <!-- Get the coordinate of the operator -->
        <xsl:variable name="mi">
            <xsl:value-of select="m:mmultiscripts[1]/m:mi[$i + 1]"/>
        </xsl:variable>
        <xsl:variable name="mj">
            <xsl:value-of select="m:mmultiscripts[1]/m:mi[$j + 1]"/>
        </xsl:variable>
        <!-- Get the position of the coordinate in the coordinate system -->
        <xsl:variable name="coordi">
            <xsl:for-each select="/m:math/m:semantics/m:annotation-xml/properties/coordinates/coordinate">
                <xsl:if test=". = $mi"><xsl:value-of select="position()"/></xsl:if>
            </xsl:for-each>            
        </xsl:variable>
        <xsl:variable name="coordj">
            <xsl:for-each select="/m:math/m:semantics/m:annotation-xml/properties/coordinates/coordinate">
                <xsl:if test=". = $mj"><xsl:value-of select="position()"/></xsl:if>
            </xsl:for-each>            
        </xsl:variable>
        <xsl:choose>
            <xsl:when test="$n = $j">
                <xsl:value-of select="$coordj - $coordi" />
            </xsl:when>
            <xsl:otherwise>
                <xsl:variable name="x">
                    <xsl:call-template name="productJ">
                        <xsl:with-param name="n" select="$n" />
                        <xsl:with-param name="i" select="$i" />
                        <xsl:with-param name="j" select="$j+1" />
                    </xsl:call-template>
                </xsl:variable>
                <xsl:value-of select="($coordj - $coordi) * $x" />
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <!-- Factorial -->
    <xsl:template name="factorial">
        <xsl:param name="number" />
        <xsl:choose>
            <xsl:when test="$number = 1">1</xsl:when>
            <xsl:otherwise>
                <xsl:variable name="x">
                    <xsl:call-template name="factorial">
                        <xsl:with-param name="number" select="$number - 1" />
                    </xsl:call-template>
                </xsl:variable>
                <xsl:value-of select="$number * $x" />
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <!--_______________________funcion que calcula delta 2 indices_________________-->
    <xsl:template match="m:ci[m:mmultiscripts[1]/m:mi[1] = 'δ_K']" mode="calculated">
        <xsl:variable name="mi1">
            <xsl:value-of select="m:mmultiscripts[1]/m:mi[2]"/>
        </xsl:variable>
        <xsl:variable name="mi2">
            <xsl:value-of select="m:mmultiscripts[1]/m:mi[3]"/>
        </xsl:variable>
       
        
        <xsl:variable name="valorE">
            <xsl:choose>
                <xsl:when test="$mi1 = $mi2">1</xsl:when>
                <xsl:otherwise>0</xsl:otherwise>
            </xsl:choose>
            
        </xsl:variable>
        <!--ponemos el valor calculado en lugar del ci-->             
        <xsl:element name="mt:cn">
            <xsl:value-of select="$valorE"/>
        </xsl:element>
    </xsl:template>
    
    <!--____________________funcion que calcula J de 1 indice_______________-->
    <xsl:template match="m:ci[m:mmultiscripts[1]/m:mi[1] = 'ℑ']" mode="calculateJ">
        <mt:cn>1</mt:cn>
    </xsl:template>
    
    <!--_______________________funcion que calcula TR de n indices_________________-->
    <xsl:template match="m:apply[m:csymbol[1] = 'tr']" mode="calculateTR">
        <xsl:variable name="idxNumber">
            <xsl:value-of select="/m:math/m:semantics/m:annotation-xml/properties/indexNumber"/>
        </xsl:variable>
        <xsl:variable name="field">
            <xsl:value-of select="./m:ci"/>
        </xsl:variable>
        <mt:apply>
            <mt:plus/>
            <!--recorrer coordenadas-->
            <xsl:for-each select="/m:math/m:semantics/m:annotation-xml/properties/coordinates/coordinate">
                <mt:ci><xsl:value-of select="$field"/><xsl:call-template name="printDiagonal">
                    <xsl:with-param name="i">
                        <xsl:value-of select="1"/>
                    </xsl:with-param>
                    <xsl:with-param name="count">
                        <xsl:value-of select="$idxNumber"/>
                    </xsl:with-param>
                    <xsl:with-param name="coordinate">
                        <xsl:value-of select="."/>
                    </xsl:with-param>
                </xsl:call-template></mt:ci>
            </xsl:for-each>
        </mt:apply>                
    </xsl:template>
    <!--imprime una misma coordenada count veces-->
    <xsl:template name="printDiagonal">
        <xsl:param name="i"      />
        <xsl:param name="count"  />
        <xsl:param name="coordinate"  />
        
        <!--begin_: RepeatTheLoopUntilFinished-->
        <xsl:if test="$i &lt;= $count">
            <xsl:value-of select="$coordinate" />
            <xsl:call-template name="printDiagonal">
                <xsl:with-param name="i">
                    <xsl:value-of select="$i + 1"/>
                </xsl:with-param>
                <xsl:with-param name="count">
                    <xsl:value-of select="$count"/>
                </xsl:with-param>
                <xsl:with-param name="coordinate">
                    <xsl:value-of select="$coordinate"/>
                </xsl:with-param>
            </xsl:call-template>
        </xsl:if>        
    </xsl:template>
    
    
    <!--______________________________________________________________________________________________________________-->
    <!--_____________________________________________________________________-->
    <!--__________________accion principal simplifica________________________-->
    <!--_____________________________________________________________________-->
    <xsl:template match="m:semantics[m:annotation-xml/properties/action = 'simplify']">
        <!--ponemos el semantics principal hay que normalizar nombres despues de simplificar: quitar indices-->
        <mt:semantics>
            <!--si el nodo esta vacio no hay nada que simplificar, ponemos end e indicamos que no es un termino-->
            <xsl:choose>
                <xsl:when test="*[1][name() = 'mt:annotation-xml']">
                    <!--se crea el apply principal que sumara cada fragmento expandido-->
                    <mt:annotation-xml encoding="xml">
                        <properties>
                            <id>noTerm</id>
                            <action>end</action>                
                            <derivativeIndex><xsl:value-of select="m:annotation-xml/properties/derivativeIndex"/></derivativeIndex>
                            <derivativeCoordinate><xsl:value-of select="m:annotation-xml/properties/derivativeCoordinate"/></derivativeCoordinate>
                            <xsl:if test="m:annotation-xml/properties/secDerivativeCoordinate">
                                <secDerivativeCoordinate><xsl:value-of select="m:annotation-xml/properties/secDerivativeCoordinate"/></secDerivativeCoordinate>
                            </xsl:if>
                        </properties>
                    </mt:annotation-xml>
                </xsl:when>
                <xsl:otherwise>
                    <!--se crea el apply principal que sumara cada fragmento expandido-->
                    <xsl:apply-templates select="*[1]" mode="simplify"/>
                    <mt:annotation-xml encoding="xml">
                        <properties>
                            <id><xsl:value-of select="m:annotation-xml/properties/id"/></id>
                            <action>simplify</action>                
                            <derivativeIndex><xsl:value-of select="m:annotation-xml/properties/derivativeIndex"/></derivativeIndex>
                            <derivativeCoordinate><xsl:value-of select="m:annotation-xml/properties/derivativeCoordinate"/></derivativeCoordinate>
                            <xsl:if test="m:annotation-xml/properties/secDerivativeCoordinate">
                                <secDerivativeCoordinate><xsl:value-of select="m:annotation-xml/properties/secDerivativeCoordinate"/></secDerivativeCoordinate>
                            </xsl:if>
                        </properties>
                    </mt:annotation-xml>
                </xsl:otherwise>
            </xsl:choose>                        
        </mt:semantics>            
    </xsl:template>
    
    <!--_________________________________________________________-->
    <!--___________Plantilla que realiza las diferentes simplificaciones sobre la formula________-->
    <!-- plantilla que copia el fragmento de mathml de entrada y substituye index por coordinate en la copia-->
    <xsl:template match="*" mode="simplify">        
        <xsl:choose>
            <!--se eliminan todos los tags semantics-->
            <xsl:when test="name() = 'mt:semantics'">
                <xsl:apply-templates select="*[(position() = 1)]" mode="simplify"/>
            </xsl:when>
            <!--se eliminan todo los tats annotation-xml-->
            <!--xsl:when test="name() = 'm:annotation-xml'"></xsl:when-->
            <!-- Simplificación trigonométrica -->
            <xsl:when test="(name() = 'mt:apply') and (count(./*[name() = 'mt:sin']) &gt; 0)">
                <xsl:choose>
                    <!-- 0º -->
                  <xsl:when test="(count(m:cn[. = '0'])  &gt; 0)">
                      <mt:cn>0</mt:cn>
                  </xsl:when>
                    <!-- 90º -->
                    <xsl:when test="(count(m:apply) &gt; 0) and count(m:apply/*[position() = 1][self::m:divide]) = 1 and count(m:apply/*[position() = 2][self::m:pi]) = 1 and count(m:apply/*[position() = 3][self::m:cn[text() = '2']]) = 1">
                        <mt:cn>1</mt:cn>
                    </xsl:when>
                    <!-- 180º -->
                    <xsl:when test="(count(m:pi)  &gt; 0)">
                        <mt:cn>0</mt:cn>
                    </xsl:when>
                    <!-- 270º -->
                    <xsl:when test="(count(m:apply) &gt; 0) and count(m:apply/*[position() = 1][self::m:divide]) = 1 and count(m:apply/*[position() = 2][self::m:apply]) = 1 and count(m:apply/m:apply/*[position() = 1][self::m:times]) = 1 and count(m:apply/m:apply/*[position() = 2][self::m:cn[text() ='3']]) = 1 and count(m:apply/m:apply/*[position() = 3][self::m:pi]) = 1 and count(m:apply/*[position() = 3][self::m:cn[text() = '2']]) = 1">
                        <mt:apply>
                            <mt:minus/>
                            <mt:cn>1</mt:cn>
                        </mt:apply>
                    </xsl:when>
                    <xsl:when test="(count(m:apply) &gt; 0) and count(m:apply/*[position() = 1][self::m:times]) = 1 and count(m:apply/*[position() = 2][self::m:apply]) = 1 and count(m:apply/m:apply/*[position() = 1][self::m:divide]) = 1 and count(m:apply/m:apply/*[position() = 2][self::m:cn[text() ='3']]) = 1 and count(m:apply/m:apply/*[position() = 3][self::m:cn[text() ='2']]) = 1 and count(m:apply/*[position() = 3][self::m:pi]) = 1">
                        <mt:apply>
                            <mt:minus/>
                            <mt:cn>1</mt:cn>
                        </mt:apply>
                    </xsl:when>
                    <!-- 360º -->
                    <xsl:when test="(count(m:apply) &gt; 0) and count(m:apply/*[position() = 1][self::m:times]) = 1 and count(m:apply/*[position() = 2][self::m:cn[text() = '2']]) = 1 and count(m:apply/*[position() = 3][self::m:pi]) = 1">
                        <mt:cn>0</mt:cn>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:element name="{name()}">
                            <xsl:for-each select="@*">
                                <xsl:attribute name="{name()}">
                                    <xsl:value-of select="."/>
                                </xsl:attribute>
                            </xsl:for-each>
                            <xsl:apply-templates select="*|text()" mode="simplify"/>
                        </xsl:element>
                    </xsl:otherwise>
                </xsl:choose>  
            </xsl:when>
            <xsl:when test="(name() = 'mt:apply') and (count(./*[name() = 'mt:cos']) &gt; 0)">
                <xsl:choose>
                    <!-- 0º -->
                    <xsl:when test="(count(m:cn[. = '0'])  &gt; 0)">
                        <mt:cn>1</mt:cn>
                    </xsl:when>
                    <!-- 90º -->
                    <xsl:when test="(count(m:apply) &gt; 0) and count(m:apply/*[position() = 1][self::m:divide]) = 1 and count(m:apply/*[position() = 2][self::m:pi]) = 1 and count(m:apply/*[position() = 3][self::m:cn[text() = '2']]) = 1">
                        <mt:cn>0</mt:cn>
                    </xsl:when>
                    <!-- 180º -->
                    <xsl:when test="(count(m:pi)  &gt; 0)">
                        <mt:apply>
                            <mt:minus/>
                            <mt:cn>1</mt:cn>
                        </mt:apply>
                    </xsl:when>
                    <!-- 270º -->
                    <xsl:when test="(count(m:apply) &gt; 0) and count(m:apply/*[position() = 1][self::m:divide]) = 1 and count(m:apply/*[position() = 2][self::m:apply]) = 1 and count(m:apply/m:apply/*[position() = 1][self::m:times]) = 1 and count(m:apply/m:apply/*[position() = 2][self::m:cn[text() ='3']]) = 1 and count(m:apply/m:apply/*[position() = 3][self::m:pi]) = 1 and count(m:apply/*[position() = 3][self::m:cn[text() = '2']]) = 1">
                        <mt:apply>
                            <mt:minus/>
                            <mt:cn>1</mt:cn>
                        </mt:apply>
                    </xsl:when>
                    <xsl:when test="(count(m:apply) &gt; 0) and count(m:apply/*[position() = 1][self::m:times]) = 1 and count(m:apply/*[position() = 2][self::m:apply]) = 1 and count(m:apply/m:apply/*[position() = 1][self::m:divide]) = 1 and count(m:apply/m:apply/*[position() = 2][self::m:cn[text() ='3']]) = 1 and count(m:apply/m:apply/*[position() = 3][self::m:cn[text() ='2']]) = 1 and count(m:apply/*[position() = 3][self::m:pi]) = 1">
                        <mt:cn>0</mt:cn>
                    </xsl:when>
                    <!-- 360º -->
                    <xsl:when test="(count(m:apply) &gt; 0) and count(m:apply/*[position() = 1][self::m:times]) = 1 and count(m:apply/*[position() = 2][self::m:cn[text() = '2']]) = 1 and count(m:apply/*[position() = 3][self::m:pi]) = 1">
                        <mt:cn>1</mt:cn>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:element name="{name()}">
                            <xsl:for-each select="@*">
                                <xsl:attribute name="{name()}">
                                    <xsl:value-of select="."/>
                                </xsl:attribute>
                            </xsl:for-each>
                            <xsl:apply-templates select="*|text()" mode="simplify"/>
                        </xsl:element>
                    </xsl:otherwise>
                </xsl:choose>  
            </xsl:when>
            <xsl:when test="(name() = 'mt:apply') and (count(./*[name() = 'mt:tan']) &gt; 0)">
                <xsl:choose>
                    <!-- 0º -->
                    <xsl:when test="(count(m:cn[. = '0'])  &gt; 0)">
                        <mt:cn>0</mt:cn>
                    </xsl:when>
                    <!-- 180º -->
                    <xsl:when test="(count(m:pi)  &gt; 0)">
                        <mt:cn>0</mt:cn>
                    </xsl:when>
                    <!-- 360º -->
                    <xsl:when test="(count(m:apply) &gt; 0) and count(m:apply/*[position() = 1][self::m:times]) = 1 and count(m:apply/*[position() = 2][self::m:cn[text() = '2']]) = 1 and count(m:apply/*[position() = 3][self::m:pi]) = 1">
                        <mt:cn>0</mt:cn>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:element name="{name()}">
                            <xsl:for-each select="@*">
                                <xsl:attribute name="{name()}">
                                    <xsl:value-of select="."/>
                                </xsl:attribute>
                            </xsl:for-each>
                            <xsl:apply-templates select="*|text()" mode="simplify"/>
                        </xsl:element>
                    </xsl:otherwise>
                </xsl:choose>  
            </xsl:when>
            <!--caso de simplificacion: cuando hay una multiplicacion-->
            <xsl:when test="(name() = 'mt:apply') and (count(./*[name() = 'mt:times']) &gt; 0)">
                <xsl:choose>
                    <!--cuando hay un operando que es 0 se quita la multiplicacion entera y se reemplaza por 0, por si la anterior también es multiplicación-->
                    <xsl:when test="(count(m:cn[. = '0'])  &gt; 0)">
                        <mt:cn>0</mt:cn>
                    </xsl:when>
                    <!--cuando hay un operando multiplicado por 1 se quita ese 1-->
                    <xsl:when test="(count(*) - count(m:cn[. = 1]) &lt;= 2)">
                        <xsl:apply-templates select="*[(position() &gt; 1) and . != '1']" mode="simplify"/>
                    </xsl:when>
                    <!--cuando hay operandos multiplicados por -1 se quita esos -1 y se pone el tag minus o no dependiendo del si hay pares o impares-->
                    <xsl:when test="(count(m:apply[count(*) = 2]/m:minus[following-sibling::m:cn = 1]) &gt; 0) and ((count(m:apply[count(*) = 2]/m:minus[following-sibling::m:cn = 1]) mod 2) = 1)">
                        <mt:apply>
                            <mt:minus/>
                            <mt:apply>
                            <xsl:for-each select="./*">
                                <xsl:if test="not(./m:minus[parent::m:apply and following-sibling::m:cn = 1  and count(parent::m:apply/*) = 2])"><xsl:apply-templates select="." mode="simplify"></xsl:apply-templates></xsl:if>
                            </xsl:for-each>     
                            </mt:apply>
                        </mt:apply>
                    </xsl:when>
                    <xsl:when test="(count(m:apply[count(*) = 2]/m:minus[following-sibling::m:cn = 1]) &gt; 0) and ((count(m:apply[count(*) = 2]/m:minus[following-sibling::m:cn = 1]) mod 2) = 0)">
                        <mt:apply>
                            <xsl:for-each select="./*">
                                <xsl:if test="not(./m:minus[parent::m:apply and following-sibling::m:cn = 1  and count(parent::m:apply/*) = 2])"><xsl:apply-templates select="." mode="simplify"></xsl:apply-templates></xsl:if>
                            </xsl:for-each>    
                        </mt:apply>  
                    </xsl:when>
                    <!--cuando hay operandos negativos se pone el tag minus o no dependiendo del si hay pares o impares-->
                    <xsl:when test="(count(m:apply[count(*) = 2]/m:minus) &gt; 0) and ((count(m:apply[count(*) = 2]/m:minus) mod 2) = 1)">
                        <mt:apply>
                            <mt:minus/>
                            <mt:apply>
                            <xsl:for-each select="./*">
                                <xsl:if test="./m:minus[parent::m:apply and count(parent::m:apply/*) = 2]"><xsl:apply-templates select="./*[2]" mode="simplify"></xsl:apply-templates></xsl:if>
                                <xsl:if test="not(./m:minus[parent::m:apply and count(parent::m:apply/*) = 2])"><xsl:apply-templates select="." mode="simplify"></xsl:apply-templates></xsl:if>
                            </xsl:for-each>   
                            </mt:apply>
                        </mt:apply>
                    </xsl:when>
                    <xsl:when test="(count(m:apply[count(*) = 2]/m:minus) &gt; 0) and ((count(m:apply[count(*) = 2]/m:minus) mod 2) = 0)">
                        <mt:apply>
                        <xsl:for-each select="./*">
                            <xsl:if test="./m:minus[parent::m:apply and count(parent::m:apply/*) = 2]"><xsl:apply-templates select="./*[2]" mode="simplify"></xsl:apply-templates></xsl:if>
                            <xsl:if test="not(./m:minus[parent::m:apply and count(parent::m:apply/*) = 2])"><xsl:apply-templates select="." mode="simplify"></xsl:apply-templates></xsl:if>
                        </xsl:for-each>
                        </mt:apply>   
                    </xsl:when>
                    <!--si no hay que simplificar copiamos todo el elemento y seguimos recursivamente-->
                    <xsl:otherwise>
                        <xsl:element name="{name()}">
                            <xsl:for-each select="@*">
                                <xsl:attribute name="{name()}">
                                    <xsl:value-of select="."/>
                                </xsl:attribute>
                            </xsl:for-each>
                            <xsl:apply-templates select="*|text()" mode="simplify"/>
                        </xsl:element>
                    </xsl:otherwise>
                </xsl:choose>                
            </xsl:when>
            <!--caso de simplificacion: cuando hay una division-->
            <xsl:when test="(name() = 'mt:apply') and (count(./*[name() = 'mt:divide']) &gt; 0)">
                <xsl:choose>
                    <!--cuando si el numerador es 0 se quita la division entera y se reemplaza por 0-->
                    <xsl:when test="(count(m:cn[. = '0'][position() = 1])  &gt; 0)"><mt:cn>0</mt:cn></xsl:when>
                    <!--cuando hay un operando dividido por 1 se quita ese 1-->
                    <xsl:when test="(count(*) - count(m:cn[. = 1]) &lt;= 2) and (count(./*[name() = 'mt:cn'][position() = 2][text() = '1']) &gt; 0)">
                        <xsl:apply-templates select="*[(position() &gt; 2) and . != '1']" mode="simplify"/>
                    </xsl:when>
                    <!--cuando hay un operando dividido por -1 se quita ese -1 y se pone el tag minus-->
                    <xsl:when test="(count(m:apply/m:minus[following-sibling::m:cn = 1]) &gt; 0) and count(child::*) = 3 and (count(./*[name() = 'mt:cn'][position() = 2][text() = '1']) &gt; 0)">
                        <!--quitamos el divide principal y el menos uno-->
                        <mt:apply>
                            <mt:minus/>
                            <xsl:for-each select=".">
                                <xsl:if test="count(./m:minus[following-sibling::m:cn = 1]) = 0"><xsl:apply-templates select="*[2]" mode="simplify"></xsl:apply-templates></xsl:if>
                            </xsl:for-each>                            
                        </mt:apply>
                    </xsl:when>
                    <!--si no hay que simplificar copiamos todo el elemento y seguimos recursivamente-->
                    <xsl:otherwise>
                        <xsl:element name="{name()}">
                            <xsl:for-each select="@*">
                                <xsl:attribute name="{name()}">
                                    <xsl:value-of select="."/>
                                </xsl:attribute>
                            </xsl:for-each>
                            <xsl:apply-templates select="*|text()" mode="simplify"/>
                        </xsl:element>
                    </xsl:otherwise>
                </xsl:choose>                
            </xsl:when>
            <!--cuando hay un 1 dentro de una multiplicacion se quita-->
            <xsl:when test="(name() = 'mt:cn' and . = '1' and count(parent::*/m:times[1]) &gt; 0)"></xsl:when>
            <!--cuando una suma/resta tiene un operando igual a 0 se elimina el operando-->
            <xsl:when test="(name() = 'mt:apply') and ((count(./*[name() = 'mt:plus']) &gt; 0) or (count(./*[name() = 'mt:minus']) &gt; 0))">
                <xsl:choose>
                    <!--si son solo dos hijos y es una suma, la operación se elimina y se queda el otro hijo como único operando-->
                    <xsl:when test="(count(*) - count(m:cn[. = 0]) = 2) and (count(./*[name() = 'mt:plus']) &gt; 0)">
                        <xsl:apply-templates select="*[(position() &gt; 1) and . != '0']" mode="simplify"/>
                    </xsl:when>
                    <xsl:when test="(count(*) - count(m:cn[. = 0]) = 1) and (count(./*[name() = 'mt:plus']) &gt; 0)">
                        <mt:cn>0</mt:cn>
                    </xsl:when>
                    <!--si son dos hijos, es resta y el operando 0 es el segundo la operación se elimina la operación de resta y se queda el otro hijo-->
                    <xsl:when test="(count(*) - count(m:cn[. = 0]) = 2) and (count(./*[name() = 'mt:minus']) &gt; 0) and (count(./*[name() = 'mt:cn'][position() = 3][text() = '0']) &gt; 0)">
                        <xsl:apply-templates select="*[(position() &gt; 1) and . != '0']" mode="simplify"/>
                    </xsl:when>
                    <!--si son dos hijos, es resta y el 0 es el primer operando se elimina el 0-->
                    <xsl:when test="(count(*) - count(m:cn[. = 0]) = 2) and (count(./*[name() = 'mt:minus']) &gt; 0) and (count(./*[name() = 'mt:cn'][position() = 2][text() != '0']) &gt; 0)">
                        <mt:apply>
                            <mt:minus/>
                            <xsl:apply-templates select="*[(position() &gt; 1) and . != '0']" mode="simplify"/>                         
                        </mt:apply>
                    </xsl:when>
                    <!--si son más de dos hijos y es suma solo se elimina el operando igual a 0-->
                    <xsl:when test="(count(*) - count(m:cn[. = 0]) &gt; 2) and (count(./*[name() = 'mt:plus']) &gt; 0)">
                        <mt:apply>
                            <mt:plus/>
                            <xsl:for-each select=".">
                                <xsl:apply-templates select="*[(position() &gt; 1) and . != '0']" mode="simplify"/>
                            </xsl:for-each>                            
                        </mt:apply>
                    </xsl:when>
                    <!--si son más de dos hijos, es resta y el 0 no es el primer operando se elimina el operando 0-->
                    <xsl:when test="(count(*) - count(m:cn[. = 0]) &gt; 2) and (count(./*[name() = 'mt:minus']) &gt; 0) and (count(./*[name() = 'mt:cn'][position() &gt; 2][text() != '0']) &gt; 0)">
                        <mt:apply>
                            <mt:minus/>
                            <xsl:for-each select=".">
                                <xsl:apply-templates select="*[(position() &gt; 1) and . != '0']" mode="simplify"/>
                            </xsl:for-each>                            
                        </mt:apply>
                    </xsl:when>
                    <!--Simplificación de "- -" = "+" -->
                    <xsl:when test="(name() = 'mt:apply') and (count(./mt:minus) = 1) and (count(./*) = 2) and (count(./mt:apply[mt:minus]/*) = 2)">
                        <xsl:apply-templates select="./*[position() = 2]/*[position() = 2]" mode="simplify"/>
                    </xsl:when>
                    <!--si no hay que simplificar copiamos todo el elemento y seguimos recursivamente-->
                    <xsl:otherwise>
                        <xsl:element name="{name()}">
                            <xsl:for-each select="@*">
                                <xsl:attribute name="{name()}">
                                    <xsl:value-of select="."/>
                                </xsl:attribute>
                            </xsl:for-each>
                            <xsl:apply-templates select="*|text()" mode="simplify"/>
                        </xsl:element>
                    </xsl:otherwise>
                </xsl:choose>    
            </xsl:when>
            <!--cuando es un apply que solo tiene un hijo se quita entero-->
            <xsl:when test="(name() = 'mt:apply') and (count(./*) &lt;= 1)"></xsl:when>
            <xsl:when test="(name() = 'mt:semantics') and (*[1][name() = 'mt:annotation-xml'])"></xsl:when>
            <!--si no hay que simplificar copiamos todo el elemento y seguimos recursivamente-->
            <xsl:otherwise>
                <xsl:element name="{name()}">
                    <xsl:for-each select="@*">
                        <xsl:attribute name="{name()}">
                            <xsl:value-of select="."/>
                        </xsl:attribute>
                    </xsl:for-each>
                    <xsl:apply-templates select="*|text()" mode="simplify"/>
                </xsl:element>
            </xsl:otherwise>
        </xsl:choose>      
    </xsl:template>
    
    
    <!--______________________________________________________________________________________________________________-->
    <!--_____________________________________________________________________-->
    <!--__________________accion principal normaliza________________________-->
    <!--_____________________________________________________________________-->
    <xsl:template match="m:semantics[m:annotation-xml/properties/action = 'normalize']">
        <!--ponemos el semantics principal hay que normalizar nombres despues de simplificar: quitar indices-->
        <mt:semantics>
            <!--se crea el apply principal que sumara cada fragmento expandido-->
            <xsl:apply-templates select="*[1]" mode="normalize"/>
            <mt:annotation-xml encoding="xml">
                <properties>
                    <id><xsl:value-of select="m:annotation-xml/properties/id"/></id>
                    <action>simplify</action>                
                    <derivativeIndex><xsl:value-of select="m:annotation-xml/properties/derivativeIndex"/></derivativeIndex>
                    <derivativeCoordinate><xsl:value-of select="m:annotation-xml/properties/derivativeCoordinate"/></derivativeCoordinate>
                    <xsl:if test="m:annotation-xml/properties/secDerivativeCoordinate">
                        <secDerivativeCoordinate><xsl:value-of select="m:annotation-xml/properties/secDerivativeCoordinate"/></secDerivativeCoordinate>
                    </xsl:if>
                </properties>
            </mt:annotation-xml>
        </mt:semantics>            
    </xsl:template>
    
    <!--_________________________________________________________-->
    <!--___________Plantilla que elimina los multiscripts poniendo los indices como una lista en los nombres________-->
    <xsl:template match="*" mode="normalize">        
        <xsl:choose>
            <xsl:when test="name() = 'mt:mmultiscripts'">
                <xsl:for-each select="mt:mi">
                    <xsl:value-of select="."/>
                </xsl:for-each>
            </xsl:when>
            <!--si hay semantics vacios los quitamos-->
            <xsl:when test="name() = 'm:semantics' and count(*) = 1"></xsl:when>
            <xsl:otherwise>
                <xsl:element name="{name()}">
                    <xsl:for-each select="@*">
                        <xsl:attribute name="{name()}">
                            <xsl:value-of select="."/>
                        </xsl:attribute>
                    </xsl:for-each>
                    <xsl:apply-templates select="*|text()" mode="normalize"/>
                </xsl:element>
            </xsl:otherwise>
        </xsl:choose>      
    </xsl:template>
        
</xsl:stylesheet>
