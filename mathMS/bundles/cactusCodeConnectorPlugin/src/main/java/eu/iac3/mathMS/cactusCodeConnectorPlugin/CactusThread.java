/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.cactusCodeConnectorPlugin;

import eu.iac3.mathMS.simulationThreadPlugin.SimulationThread;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;

import org.osgi.framework.BundleContext;
import org.osgi.service.log.LogService;
import org.osgi.util.tracker.ServiceTracker;

/**
 * The thread of the simulation.
 * @author bminyano
 *
 */
public class CactusThread extends SimulationThread {
    
    BundleContext context;
    static LogService logservice = null;
    boolean localSimulation;
    String[] commands;
    String workingDirectory;
    String toDelete;
    
    /**
     * Constructor for the thread.
     * @param problemName       The problem name
     * @param commands          The command and parameters to be executed
     * @param workingDirectory  The directory where the executable is
     * @param localSimulation   If the simulation is local or distributed
     * @param toDelete          Folder or file to delete
     * @param ctx               The bundle context
     */
    public CactusThread(String problemName, String[] commands, String workingDirectory, boolean localSimulation, 
            String toDelete, BundleContext ctx) {
        super(problemName);
        setDaemon(false);
        this.localSimulation = localSimulation;
        this.commands = commands;
        context = ctx;
        ServiceTracker logServiceTracker = new ServiceTracker(context, org.osgi.service.log.LogService.class.getName(), null);
        logServiceTracker.open();
        logservice = (LogService) logServiceTracker.getService();
        this.workingDirectory = workingDirectory;
        this.toDelete = toDelete;
    }
    /**
     * Run method.
     */
    public void run() {
        try {
            Process process = Runtime.getRuntime().exec(commands, null, new File(workingDirectory));
            //Prints the command in the log
            String errorLog = "";
            boolean error = false;
            if (logservice != null) {
                String msg = "";
                for (int i = 0; i < commands.length; i++) {
                    msg = msg + commands[i] + " ";
                }
                logservice.log(LogService.LOG_DEBUG, msg);
            }
            //Gets the result stream
            BufferedReader in = new BufferedReader(new InputStreamReader(process.getInputStream()));
            BufferedReader err = new BufferedReader(new InputStreamReader(process.getErrorStream()));
            String line; 
            String errors = "";
            while ((line = in.readLine()) != null) { 
                System.out.println("out: " + line);
                if (logservice != null) {
                    logservice.log(LogService.LOG_INFO, line);
                }
                if (line.indexOf("Error") >= 0) {
                    error = true;
                    errorLog = errorLog + line + System.getProperty("line.separator");
                }
                if (hasToStop()) {
                    process.destroy();
                    return;
                }
            }
            while ((line = err.readLine()) != null) { 
                System.out.println("out: " + line);
                errors = errors + line;
                if (logservice != null) {
                    logservice.log(LogService.LOG_ERROR, line);
                }
                if (localSimulation || line.indexOf("MPI") >= 0) {
                    error = true;
                }
                if (hasToStop()) {
                    process.destroy();
                    return;
                }
            }
            process.waitFor();
            //Delete temporal folder
            if (!localSimulation) {
                recursiveDelete(new File(toDelete));
            }
            //Delete the temporal files
            if (localSimulation) {
                new File(workingDirectory + File.separator + toDelete).delete();
                new File(workingDirectory + File.separator + "parameter.par").delete();
            }
            if (error) {
                //The simulation throws an error. So the SimulationThread is marked with an error.
                error();
                this.interrupt();
            }
        } 
        catch (Exception e) {
            System.exit(-1);
        }
    }
    
    /**
     * Recursive delete files.
     * @param dirPath   The file to delete
     */
    public void recursiveDelete(File dirPath) {
        String[] ls = dirPath.list();

        for (int idx = 0; idx < ls.length; idx++) {
            File file = new File(dirPath, ls [idx]);
            if (file.isDirectory()) {
                recursiveDelete(file);
            }
            file.delete();
        }
        dirPath.delete();
    }
}
