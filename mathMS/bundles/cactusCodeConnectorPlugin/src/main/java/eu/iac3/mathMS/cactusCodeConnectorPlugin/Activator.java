/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.cactusCodeConnectorPlugin;

import java.util.Dictionary;
import java.util.Hashtable;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

/** 
 * Activator implements the BundleActivator interface 
 * This class is used to register the
 * {@link CactusCodeConnector} as an OSGI Bundle webservice.
 * 
 * @author      ----
 * @version     ----
 */
public class Activator implements BundleActivator {
    private ServiceRegistration registration;

	/**
    * Method called by the OSGI manager when the 
    * Bundle is started.
    * @param context        Bundle context
    * @throws Exception     Bundle exception
    */
    public void start(BundleContext  context) throws Exception {
    	
    	String port;
    	
    	//Getting the actual ws port
        if (context.getProperty("org.osgi.service.ws.port") == null) {
        	//Default port
            port = "9090";
    	} 
        else {
            port = context.getProperty("org.osgi.service.ws.port");
    	}
    	
    	//Registration as a web service.
        Dictionary<String, String> props = new Hashtable<String, String>();
        props.put("osgi.remote.interfaces", "*");
        props.put("osgi.remote.configuration.type", "pojo");
        props.put("osgi.remote.configuration.pojo.address", "http://localhost:" + port + "/CactusCodeConnector");
        registration = context.registerService(CactusCodeConnector.class.getName(), new CodeCodeConnectorImpl(context), props);
    }
    /**
     * Method called by the OSGI manager when the 
     * Bundle is stopped.
     * @param context       Bundle context
     * @throws Exception    Bundle exception
     */
    public void stop(BundleContext context) throws Exception {
    	registration.unregister();
    }
}
