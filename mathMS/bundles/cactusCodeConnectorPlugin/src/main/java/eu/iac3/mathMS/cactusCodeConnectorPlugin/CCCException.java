/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.cactusCodeConnectorPlugin;

/** 
 * CCCException is the class Exception launched by the 
 * {@link CactusCodeConnector} interface.
 * The exceptions that this class return are:
 * CCC001 Not possible to create code for the platform
 * CCC002 Code does not exist
 * CCC003 Simulation platform not installed properly
 * CCC004 Compilation error
 * CCC005 The code has not been compiled
 * CCC006 Simulation error
 * CCC007 External error
 * @author      ----
 * @version     ----
 */
@SuppressWarnings("serial")
public class CCCException extends Exception {

    public static final String CCC001 = "Not possible to create code for the platform";
    public static final String CCC002 = "Code does not exist";
    public static final String CCC003 = "Simulation platform not installed properly";
    public static final String CCC004 = "Compilation error";
    public static final String CCC005 = "The code has not been compiled";
    public static final String CCC006 = "Simulation error";
    public static final String CCC007 = "External error";
	
    //External error message
    private String extMsg;
	  /** 
	     * Constructor for known messages.
	     *
	     * @param code  the message	     
	     */
    CCCException(String code) {
    	super(code);
    }
	  
	  /** 
	     * Constructor.
	     *
	     * @param code  the internal error message	    
	     * @param msg  the external error message
	     */
    public CCCException(String code, String msg) {
		super(code);
        this.extMsg = msg;
    }
	  /** 
     * Returns the error code.
     *
     * @return the error code	     
     */
    public String getErrorCode() {
    	return super.getMessage();
    }
	  /** 
	     * Returns the error message as a string.
	     *
	     * @return the error message	     
	     */
    public String getMessage() {
    	if (extMsg == null) {
            return "CCCException: " + super.getMessage();
    	}
    	else {
            return "CCCException: " + super.getMessage() + " (" + extMsg + ")";
    	}
    }  
}
