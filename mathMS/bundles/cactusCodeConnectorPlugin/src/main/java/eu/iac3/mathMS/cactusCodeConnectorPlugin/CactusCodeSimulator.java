/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.cactusCodeConnectorPlugin;

import eu.iac3.mathMS.cactusCodeConnectorPlugin.utils.CactusCodeConnectorUtils;
import eu.iac3.mathMS.simulationManagerPlugin.JobDefinition;
import eu.iac3.mathMS.simulationManagerPlugin.SMException;
import eu.iac3.mathMS.simulationManagerPlugin.SimulationExecType;
import eu.iac3.mathMS.simulationManagerPlugin.SimulationManager;
import eu.iac3.mathMS.simulationManagerPlugin.SimulationManagerImpl;

import java.io.File;

import org.osgi.framework.BundleContext;
import org.osgi.service.log.LogService;
import org.osgi.util.tracker.ServiceTracker;

/**
 * The processes to simulate code for Cactus code platform.
 * 
 * @author bminyano
 *
 */
public class CactusCodeSimulator {
    static final String NEWLINE = System.getProperty("line.separator"); 
    static CactusCodeConnectorUtils util = new CactusCodeConnectorUtils();
    BundleContext context;
    static LogService logservice = null;
//    private boolean exportResults = false;
    private SimulationManager sm;
    /**
     * Constructor to take the bundle context.
     * @param ctx       The bundle context
     */
    public CactusCodeSimulator(BundleContext ctx) {
        context = ctx;
        ServiceTracker logServiceTracker = new ServiceTracker(context, org.osgi.service.log.LogService.class.getName(), null);
        logServiceTracker.open();
        logservice = (LogService) logServiceTracker.getService();
        try {
            sm = new SimulationManagerImpl(context);
        } 
        catch (SMException e) {
            e.printStackTrace();
        } 
    }
    /**
     * Simulates a generated and compiled code in the simulation platform.
     * 
     * @param problemName       the name of the problem which code has been generated
     * @param problemPath       the path of the problem
     * @param parFile           The parameter file used in the simulation
     * @param nproc             The number of processes that are going to be generated
     * @param simType           If the simulation is in the local platform or in fura or unicore system
     * @param getResults        If true and is not a local simulation the results are taken from fura. 
     * @param version           the problem version
     * @return                  The id of the simulation.
     * @throws CCCException     CCC001 Not possible to create code for the platform      
     *                          CCC005 The code has not been compiled
     *                          CCC006 Simulation error
     *                          CCC007 External error
     */
    int simulate(String problemName, String problemPath, String parFile, int nproc, SimulationExecType simType, boolean getResults, 
            String version) throws CCCException {
        String exePath;
        String problemIdentifier = null;
        //Check that the code was compiled
        problemIdentifier = util.variableNormalize(problemName);
        try {
            //Check for the existence of the code
            String path = new File(problemPath).getAbsolutePath() + File.separator + problemIdentifier;
            if (!new File(path).exists()) {
                throw new CCCException(CCCException.CCC002, path);
            }
        } 
        catch (CCCException e) {
            throw e;
        }
        exePath = new File(problemPath).getAbsolutePath() + File.separator + "exe" + File.separator + "cactus_" + problemIdentifier;
        if (!new File(exePath).exists()) {
            throw new CCCException(CCCException.CCC005, "Compile the code first");
        }
        try {
            return simulate(problemName, simType, exePath, parFile, nproc, getResults, version);
        }
        catch (CCCException e) {
            throw e;
        } 
    }

   
    /**
     * Simulates a generated and compiled code in fura distributed cactus simulation platform.
     * @param problemName       the name of the problem which code has been generated
     * @param exePath           Path of the execution file
     * @param parFile           The parameter file used in the simulation
     * @param simType           Simulation type
     * @param nproc             The number of processes that are going to be generated
     * @param getResults        If true and is not a local simulation the results are taken from fura.
     * @param version           the version of the problem
     * @return                  The id of the simulation. 
     * @throws CCCException     CCC006 The code has not been compiled
     *                          CCC007 External error
     */
    int simulate(String problemName, SimulationExecType simType, String exePath, String parFile, int nproc, boolean getResults, 
            String version) throws CCCException {
    	int res = -1;
        //Configure
        String resultDirectory = exePath.substring(0, exePath.lastIndexOf(File.separator + "exe" + File.separator)) + File.separator + "result";
        File simDir = new File(resultDirectory);
        File exeDir = new File(exePath.substring(0, exePath.lastIndexOf(File.separator)));
        String executable = exePath.substring(exePath.lastIndexOf(File.separator) + 1);
        simDir.mkdirs();
        util.copyDirectory(exeDir, simDir);
        util.stringToFile(resultDirectory + File.separator + "parameter.input", parFile);
        //creates the job definition
        JobDefinition jobDef = new JobDefinition();
        jobDef.setExecutable(executable);
        if (SimulationExecType.local.compareTo(simType) == 0) {
            jobDef.setLocal(true);
        }
        else {                
            jobDef.setLocal(false);
            jobDef.setExportResults(getResults);
        }        
        jobDef.setMpi(true);
        jobDef.setNumProcesses(nproc);
        jobDef.setParamFile("parameter.input");
        jobDef.setPath(resultDirectory);
        jobDef.setPlatform(simType.name());
        jobDef.setProblemName(problemName);
        jobDef.setProblemVersion(version);
        try {
            res = sm.launchSimulation(jobDef);
        } 
        catch (SMException e) {
            e.printStackTrace();
            throw new CCCException(e.getErrorCode(), e.getMessage());
        }
        return res;
    }

   
}
