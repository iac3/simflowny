/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.cactusCodeConnectorPlugin;

import eu.iac3.mathMS.cactusCodeConnectorPlugin.utils.CactusCodeConnectorUtils;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Dictionary;

import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.service.log.LogService;
import org.osgi.util.tracker.ServiceTracker;

/**
 * The processes to compile code for Cactus code platform.
 * 
 * @author bminyano
 *
 */
public class CactusCodeCompilator {
    static final String NEWLINE = System.getProperty("line.separator"); 
    static CactusCodeConnectorUtils util = new CactusCodeConnectorUtils();
    BundleContext context;
    static LogService logservice = null;
    /**
     * Constructor to take the bundle context.
     * @param ctx       The bundle context
     */
    public CactusCodeCompilator(BundleContext ctx) {
        context = ctx;
        ServiceTracker logServiceTracker = new ServiceTracker(context, org.osgi.service.log.LogService.class.getName(), null);
        logServiceTracker.open();
        logservice = (LogService) logServiceTracker.getService();
    }
    /** 
     * Compiles the code generated in the simulation platform.
     *
     * @param problemName    the name of the problem which code has been generated
     * @param problemPath    the path of the problem
     * @return               the compiled exe file
     * @throws CCCException  CCC001 Not possible to create code for the platform
     *                       CCC002 Code does not exist
     *                       CCC003 Simulation platform not installed properly
     *                       CCC004 Compilation error
     *                       CCC007 External error
     */
    public String compileCode(String problemName, String problemPath) throws CCCException {
        String cactusHome;
        String path;
        String mpich;
        String hdf5dir;
        String fortran;
        String cc;
        String cxx;
        String libZDir;
        String problemIdentifier = null;
        BufferedReader in = null;
        BufferedReader br = null;
        try {
            //Check for the existence of the code
            problemIdentifier = util.variableNormalize(problemName);
            path = new File(problemPath).getAbsolutePath() + File.separator + problemIdentifier;
            if (!new File(path).exists()) {
                throw new CCCException(CCCException.CCC002, path);
            }
        } 
        catch (CCCException e) {
            throw e;
        }
        catch (Exception e) {
            throw new CCCException(CCCException.CCC007, e.getMessage());
        }
        try {
            //Get the cactus configuration
            ServiceReference serviceReference = context.getServiceReference(ConfigurationAdmin.class.getName());
            ConfigurationAdmin configAdmin = (ConfigurationAdmin) context.getService(serviceReference); 
            Configuration config = configAdmin.getConfiguration("eu.iac3.mathMS.CactusCodeConnectorPlugin"); 
            Dictionary<?, ?> properties = config.getProperties();
            if (properties != null) {
                cactusHome = (String) properties.get("eu.iac3.mathMS.CactusCodeConnectorPlugin.cactusHome");
                mpich = (String) properties.get("eu.iac3.mathMS.CactusCodeConnectorPlugin.mpichHome");
                hdf5dir = (String) properties.get("eu.iac3.mathMS.CactusCodeConnectorPlugin.hdf5Home");
                fortran = (String) properties.get("eu.iac3.mathMS.CactusCodeConnectorPlugin.fortranCompiler");
                cc = (String) properties.get("eu.iac3.mathMS.CactusCodeConnectorPlugin.cCompiler");
                cxx = (String) properties.get("eu.iac3.mathMS.CactusCodeConnectorPlugin.cppCompiler");
                libZDir = (String) properties.get("eu.iac3.mathMS.CactusCodeConnectorPlugin.libZDir");
            }
            else {
                cactusHome = System.getProperty("eu.iac3.mathMS.CactusCodeConnectorPlugin.cactusHome");
                mpich = System.getProperty("eu.iac3.mathMS.CactusCodeConnectorPlugin.mpichHome");
                hdf5dir = System.getProperty("eu.iac3.mathMS.CactusCodeConnectorPlugin.hdf5Home");
                fortran = System.getProperty("eu.iac3.mathMS.CactusCodeConnectorPlugin.fortranCompiler");
                cc = System.getProperty("eu.iac3.mathMS.CactusCodeConnectorPlugin.cCompiler");
                cxx = System.getProperty("eu.iac3.mathMS.CactusCodeConnectorPlugin.cppCompiler");
                libZDir = System.getProperty("eu.iac3.mathMS.CactusCodeConnectorPlugin.libZDir");
            }
            String targetPath = cactusHome  + File.separator + "arrangements" + File.separator + problemIdentifier;
            //Configuration checks
            configurationCheck(cactusHome, mpich, hdf5dir, fortran, cc, cxx);
            
            //Copy generated files to cactus platform
            util.copyDirectory(new File(path), new File(targetPath));
            //Modify thornlist and copy it to the targetPath
            createThornList(targetPath, problemIdentifier);

            //Create Fortran files from M4
            createFortranFromM4(targetPath, problemIdentifier);
            
            //Make configuration
            String[] commands = new String[]{"make", problemIdentifier + "-config", "CC=" + cc, "F90=" + fortran, "HDF5=yes", "HDF5_DIR=" + hdf5dir,
                "F90FLAGS=-fno-second-underscore", "CXX=" + cxx, "F77=" + fortran, "F77FLAGS=-fno-second-underscore", "THORNLIST=ThornList", 
                "THORNLIST_DIR=" + new File(targetPath).getAbsolutePath(), "MPI=MPICH", "MPICH_DIR=" + mpich, "PROMPT=no", "F90_OPTIMISE_FLAGS=-Os",
                "F77_OPTIMISE_FLAGS=-Os", "CXX_OPTIMISE_FLAGS=-Os", "C_OPTIMISE_FLAGS=-Os", "LIBZ_DIR=" + libZDir};
            Process process = Runtime.getRuntime().exec(commands, null, new File(cactusHome));
            String compilationErrors = "";
            String line;
            if (logservice != null) {
                String msg = "";
                for (int i = 0; i < commands.length; i++) {
                    msg = msg + commands[i] + " ";
                }
                logservice.log(LogService.LOG_DEBUG, msg);
            }
            in = new BufferedReader(new InputStreamReader(process.getInputStream()));
            while ((line = in.readLine()) != null) {
                compilationErrors = compilationErrors + line + NEWLINE;
                System.out.println(line);
                if (logservice != null) {
                    logservice.log(LogService.LOG_INFO, line);
                }
            }
            
            //Make compilation
            commands = new String[]{"make", problemIdentifier};
            process = Runtime.getRuntime().exec(commands, null, new File(cactusHome));
            if (logservice != null) {
                logservice.log(LogService.LOG_DEBUG, commands[0] + " " + commands[1]);
            }
            in = new BufferedReader(new InputStreamReader(process.getErrorStream()));
            br = new BufferedReader(new InputStreamReader(process.getInputStream()));
            while ((line = br.readLine()) != null) { 
                System.out.println(line);
                compilationErrors = compilationErrors + line + NEWLINE;
                if (logservice != null) {
                    logservice.log(LogService.LOG_INFO, line);
                }
            } 
            boolean compilationOK = true;
            
            while ((line = in.readLine()) != null) {
                if (logservice != null) {
                    logservice.log(LogService.LOG_ERROR, line);
                }
                compilationErrors = compilationErrors + line + NEWLINE;
                if (line.indexOf("Error") >= 0) {
                    compilationOK = false;
                }
            } 
            if (!compilationOK) {
                throw new CCCException(CCCException.CCC004, compilationErrors);
            }
            
            //Copy the execution file to the code folder
            String exeFile = cactusHome + File.separator + "exe" + File.separator + "cactus_" + problemIdentifier;
            targetPath = path.substring(0, path.lastIndexOf(problemIdentifier)) + "exe" + File.separator + "cactus_" + problemIdentifier;
            new File(path.substring(0, path.lastIndexOf(problemIdentifier)) + "exe" + File.separator).mkdirs();
            util.copyDirectory(new File(exeFile), new File(targetPath));
            
            //Change execution permissions
            commands = new String[]{"chmod", "ugo+rwx", problemIdentifier};
            process = Runtime.getRuntime().exec(commands, null, new File(path.substring(0, path.lastIndexOf(problemIdentifier)) + "exe"));
            
            return path.substring(0, path.lastIndexOf(problemIdentifier)) + "exe" + File.separator + "cactus_" + problemIdentifier;
        } 
        catch (IOException e) {
            throw new CCCException(CCCException.CCC007, e.getMessage());
        }
        catch (CCCException e) {
            throw e;
        }
        catch (Exception e) {
            throw new CCCException(CCCException.CCC007, e.getMessage());
        }
        finally {
            try {
                if (in != null) {
                    in.close();
                }
                if (br != null) {
                    br.close();
                }
            } 
            catch (IOException e) {
                throw new CCCException(CCCException.CCC007, e.getMessage());
            }
        }
    }
    
    /**
     * Create fortran files from m4 originals. ExecutionFlow and MappingSegments.
     * @param targetPath                The path of the problem
     * @param problemIdentifier         The name of the problem
     * @throws CCCException             CCC007 External error
     */
    private void createFortranFromM4(String targetPath, String problemIdentifier) throws CCCException {
        BufferedReader in = null;
        try {
            //Generate ExecutionFlow.F from M4 file
            String[] commands = new String[]{"m4", "ExecutionFlow.m4"};
            String executionFlowPath = targetPath + File.separator + problemIdentifier + File.separator + "src";
            Process process = Runtime.getRuntime().exec(commands, null, new File(executionFlowPath));
            if (logservice != null) {
                String msg = "";
                for (int i = 0; i < commands.length; i++) {
                    msg = msg + commands[i] + " ";
                }
                logservice.log(LogService.LOG_DEBUG, msg);
            }
            in = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line;
            FileWriter fstream = new FileWriter(executionFlowPath + File.separator + "ExecutionFlow.F");
            BufferedWriter out = new BufferedWriter(fstream);
            while ((line = in.readLine()) != null) { 
                out.write(line + NEWLINE);
                if (logservice != null) {
                    logservice.log(LogService.LOG_INFO, line);
                }
            }
            out.close();
            
            //Generate MappingSegments.F from M4 file
            commands = new String[]{"m4", "MappingSegments.m4"};
            executionFlowPath = targetPath + File.separator + problemIdentifier + File.separator + "src";
            process = Runtime.getRuntime().exec(commands, null, new File(executionFlowPath));
            if (logservice != null) {
                String msg = "";
                for (int i = 0; i < commands.length; i++) {
                    msg = msg + commands[i] + " ";
                }
                logservice.log(LogService.LOG_DEBUG, msg);
            }
            in = new BufferedReader(new InputStreamReader(process.getInputStream()));
            fstream = new FileWriter(executionFlowPath + File.separator + "MappingSegments.F");
            out = new BufferedWriter(fstream);
            while ((line = in.readLine()) != null) { 
                out.write(line + NEWLINE);
                if (logservice != null) {
                    logservice.log(LogService.LOG_INFO, line);
                }
            }
            out.close();
        }
        catch (IOException e) {
            throw new CCCException(CCCException.CCC007, e.getMessage());
        }
    }
   
    /**
     * Checks the configuration values.
     * @param cactusHome        Cactus home
     * @param mpich             Mpich home
     * @param hdf5dir           Hdf5 home
     * @param fortran           fortran compiler
     * @param cc                c compiler
     * @param cxx               c++ compiler
     * @throws CCCException     CCC003  Simulation platform not installed properly
     */
    private void configurationCheck(String cactusHome, String mpich, String hdf5dir, String fortran, String cc, String cxx) throws CCCException {
        if (!new File(cactusHome + File.separator + "arrangements").exists()) {
            throw new CCCException(CCCException.CCC003, "Arrangements folder not found in: " + cactusHome);
        }
        if (!new File(mpich + File.separator + "include" + File.separator + "mpi.h").exists()) {
            throw new CCCException(CCCException.CCC003, "mpi.h not found in '" + mpich + File.separator + "include'");
        }
        if (!new File(hdf5dir + File.separator + "include" + File.separator + "hdf5.h").exists()) {
            throw new CCCException(CCCException.CCC003, "hdf5.h not found in '" + hdf5dir + File.separator + "include'");
        }
        String[] commands = new String[]{fortran};
        try {
            Runtime.getRuntime().exec(commands, null);
        }
        catch (IOException e) {
            throw new CCCException(CCCException.CCC003, "Fortran compiler '" + fortran + "' not found.");
        }
        commands = new String[]{cc};
        try {
            Runtime.getRuntime().exec(commands, null);
        }
        catch (IOException e) {
            throw new CCCException(CCCException.CCC003, "C compiler '" + cc + "' not found.");
        }
        commands = new String[]{cxx};
        try {
            Runtime.getRuntime().exec(commands, null);
        }
        catch (IOException e) {
            throw new CCCException(CCCException.CCC003, "C++ compiler '" + cxx + "' not found.");
        }
    }
    
    /**
     * Creates the thorn list file for cactus.
     * @param targetPath    the path where to create the file.
     * @param pid           the problem identifier
     * @throws CCCException  CCC007 External error
     */
    private void createThornList(String targetPath, String pid) throws CCCException {
        try {
            InputStream is = getClass().getResourceAsStream("/ThornList");
            DataOutputStream out = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(targetPath + File.separator + "ThornList")));
            int c;
            while ((c = is.read()) != -1) {
                out.writeByte(c);
            }
            Writer osw = new OutputStreamWriter(out, "UTF-8");
            osw.write(new String(pid + "/" + pid + " # " + pid + " ( ) [ ] { }"));
            osw.close();
            is.close();
            out.close();
        } 
        catch (Exception e) {
            throw new CCCException(CCCException.CCC007, e.getMessage());
        }
    }
}
