/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.cactusCodeConnectorPlugin;

import eu.iac3.mathMS.simulationManagerPlugin.SimulationExecType;

import org.osgi.framework.BundleContext;
import org.osgi.service.log.LogService;
import org.osgi.util.tracker.ServiceTracker;

/** 
 * CodesGeneratorImpl implements the 
 * {@link CactusCodeConnector} interface.
 * It has the functions of create compile and simulate the code
 * in the specified simulation platform.
 *  
 * @author  ----
 * @version ----
 */
public class CodeCodeConnectorImpl implements CactusCodeConnector {
    
    static BundleContext context;
    static LogService logservice = null;
    private CactusCodeSimulator cactusSim;
    private CactusCodeCompilator cactusCom;
    
    /**
     * Constructor to take the bundle context.
     * @param ctx   the bundle context
     */
    public CodeCodeConnectorImpl(BundleContext ctx) {
        super();
        context = ctx;
        ServiceTracker logServiceTracker = new ServiceTracker(context, org.osgi.service.log.LogService.class.getName(), null);
        logServiceTracker.open();
        logservice = (LogService) logServiceTracker.getService();
        cactusSim = new CactusCodeSimulator(context);
        cactusCom = new CactusCodeCompilator(context);
    }
    
    /** 
     * Compiles the code generated in the simulation platform.
     *
     * @param problemName    the name of the problem which code has been generated
     * @param problemPath    the path of the problem
     * @return               the compiled exe file
     * @throws CCCException  CCC001 Not possible to create code for the platform
     *                       CCC002 Code does not exist
     *                       CCC003 Simulation platform not installed properly
     *                       CCC004 Compilation error
     *                       CCC007 External error
     */
    public String compileCode(String problemName, String problemPath) throws CCCException {
        return cactusCom.compileCode(problemName, problemPath);
    }
    
    /**
     * Simulates a generated and compiled code in the simulation platform.
     * @param problemName       the name of the problem which code has been generated
     * @param problemPath       the path of the problem
     * @param parFile           The parameter file used in the simulation
     * @param nproc             The number of processes that are going to be generated
     * @param simType           If the simulation is in the local platform or in unicore system
     * @param getResults        If true and is not a local simulation the results are taken. 
     * @param version           the problem version
     * @return                  The id of the simulation.
     * @throws CCCException     CCC001 Not possible to create code for the platform 
     *                          CCC005 The code has not been compiled
     *                          CCC006 Simulation error
     *                          CCC007 External error
     */
    public int simulate(String problemName, String problemPath, String parFile, int nproc, SimulationExecType simType, boolean getResults, 
            String version) throws CCCException {
        return cactusSim.simulate(problemName, problemPath, parFile, nproc, simType, getResults, version);
    }
}
