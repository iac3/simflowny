/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.cactusCodeConnectorPlugin;

import eu.iac3.mathMS.simulationManagerPlugin.SimulationExecType;

/** 
 * CodeGenerator is an interface that provides the functionality 
 * to create, compile and simulate the files for a selected simulation platform.
 * 
 * @author      ----
 * @version     ----
 */
public interface CactusCodeConnector {

    /** 
     * Compiles the code generated in the simulation platform.
     *
     * @param problemName    the name of the problem which code has been generated
     * @param problemPath    the path of the problem
     * @return               the compiled exe file
     * @throws CCCException  CCC001 Not possible to create code for the platform
     *                       CCC002 Code does not exist
     *                       CCC003 Simulation platform not installed properly
     *                       CCC004 Compilation error
     *                       CCC007 External error
     */
    String compileCode(String problemName, String problemPath) throws CCCException;
    
    /**
     * Simulates a generated and compiled code in the simulation platform.
     * @param problemName       the name of the problem which code has been generated
     * @param problemPath       the path of the problem
     * @param parFile           The parameter file used in the simulation
     * @param nproc             The number of processes that are going to be generated
     * @param simType           If the simulation is in the local platform or in unicore systems
     * @param getResults        If true and is not a local simulation the results are taken. 
     * @param version           the problem version
     * @return                  The id of the simulation.
     * @throws CCCException     CCC001 Not possible to create code for the platform 
     *                          CCC002 Code does not exist
     *                          CCC005 The code has not been compiled
     *                          CCC006 Simulation error
     *                          CCC007 External error
     */
    int simulate(String problemName, String problemPath, String parFile, int nproc, SimulationExecType simType, boolean getResults, 
            String version) throws CCCException;
}
