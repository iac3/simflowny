/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.cactusCodeConnectorPlugin.utils;

import eu.iac3.mathMS.cactusCodeConnectorPlugin.CCCException;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

/**
 * Utility class.
 * @author bminyano
 *
 */
public final class CactusCodeConnectorUtils {
    
    static final String XSL = "instructionToFortran.xsl";
    /**
     * Parses a xml with a xlst.
     * @param xml               The xml to parse.
     * @param params            Params to send to the xsl.
     * @param fileName          The file name for the XSL
     * @return                  the result to apply the xlt to the xml.
     * @throws CCCException     CCC007 External error
     */
    public byte[] xmlTransform(String xml, String[][] params, String fileName) throws CCCException {  
        String path = null;
        try {
            ByteArrayInputStream input = new ByteArrayInputStream(xml.getBytes("UTF-8"));
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            Transformer transformer = null;
            TransformerFactory tFactory = TransformerFactory.newInstance();
 
            path = "common" + File.separator + "XSLTmathml" + File.separator + fileName;
            transformer = tFactory.newTransformer(new StreamSource(path));
            // Set the parameters
            if (params != null) {
                for (int i = 0; i < params.length; i++) {
                    transformer.setParameter(params[i][0], params[i][1]);
                }   
            }
            transformer.transform(new StreamSource(input), new StreamResult(output));
            return output.toByteArray();
        } 
        catch (Exception e) {
            throw new CCCException(CCCException.CCC007, e.getMessage());
        }
    }
    
    /**
     * Copies a directory and its content to another.
     * @param sourceLocation    The source directory
     * @param targetLocation    The target directory
     * @throws CCCException     CCC007 External error
     */
    public void copyDirectory(File sourceLocation , File targetLocation) throws CCCException {
        
        try {
            final int mb = 1024;
            
            if (sourceLocation.isDirectory()) {
                if (!targetLocation.exists()) {
                    targetLocation.mkdirs();
                }
                
                String[] children = sourceLocation.list();
                for (int i = 0; i < children.length; i++) {
                    copyDirectory(new File(sourceLocation, children[i]),
                            new File(targetLocation, children[i]));
                }
            } 
            else {
                
                InputStream in = new FileInputStream(sourceLocation);
                OutputStream out = new FileOutputStream(targetLocation);
                
                // Copy the bits from instream to outstream
                byte[] buf = new byte[mb];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
                in.close();
                out.close();
            }
        } 
        catch (IOException e) {
            throw new CCCException(CCCException.CCC007, e.getMessage());
        }
    }
    
    /**
     * Checks if a string is numeric.
     * @param string    The string to test
     * @return          true if numeric, false if not
     */
    public static boolean isNumeric(String string) {
        try {
            Integer.parseInt(string);
            return true;
        } 
        catch (NumberFormatException nfe) {
            return false;
        }
    }
    
    /**
     * Creates a zip file from the input files.
     * @param zipName           The name of the zip file
     * @param fileNames         The file names inside the zip
     * @throws CCCException     CCC007 External error
     */
    public void zip(String zipName, String[] fileNames) throws CCCException {
        final int kbyte = 1024;
        // Create a buffer for reading the files
        byte[] buf = new byte[kbyte];
        
        try {
            // Create the ZIP file
            ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipName));
        
            // Compress the files
            for (int i = 0; i < fileNames.length; i++) {
                FileInputStream in = new FileInputStream(new File(fileNames[i]).getAbsolutePath());

                // Add ZIP entry to output stream.
                out.putNextEntry(new ZipEntry(fileNames[i].substring(fileNames[i].lastIndexOf(File.separator) + 1)));
        
                // Transfer bytes from the file to the ZIP file
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
        
                // Complete the entry
                out.closeEntry();
                in.close();
            }
            // Complete the ZIP file
            out.close();
        } 
        catch (IOException e) {
            throw new CCCException(CCCException.CCC005, e.getMessage());
        }
    }
    
    /**
     * Creates a file with the string as the content.
     * @param fileName          The file name
     * @param content           The content of the file
     * @throws CCCException     CCC007 External error
     */
    public void stringToFile(String fileName, String content) throws CCCException {
        try {
            BufferedWriter out = new BufferedWriter(new FileWriter(fileName));
            out.write(content);
            out.close();
        } 
        catch (IOException e) {
            throw new CCCException(CCCException.CCC007, e.getMessage());
        }
    }
    
    /**
     * Read a file as String.
     * 
     * @param is    The input stream of the file
     * @return      The resource as string
     */
    public static String convertStreamToString(InputStream is) {
        StringBuilder sb = null;
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } 
        catch (IOException e) {
            e.printStackTrace();
        } 
        finally {
            try {
                is.close();
            } 
            catch (IOException e) { 
                e.printStackTrace();
            }
        }
        return sb.toString();
    }
    
    /**
     * Recursive delete files.
     * @param dirPath   The file to delete
     */
    public void recursiveDelete(File dirPath) {
        String[] ls = dirPath.list();
         
        for (int idx = 0; ls != null && idx < ls.length; idx++) {
            File file = new File(dirPath, ls [idx]);
            if (file.isDirectory()) {
                recursiveDelete(file);
            }
            file.delete();
        }
        dirPath.delete();
    }
    
    /**
     * Normalize the name of a variable with Fortran rules and according entities.xsl.
     * @param variable          The original name of the variable
     * @return                  The variable normalized.
     * @throws CCCException     CCC001 Not possible to create code for the platform
     *                          CCC007 External error
     */
    public String variableNormalize(String variable) throws CCCException {
        final int fortranVariableLimit = 27;
        try {
            //Replace the greek characters. Process with xslt
            variable = "<mt:math xmlns:mt=\"http://www.w3.org/1998/Math/MathML\"><mt:ci>" + variable + "</mt:ci></mt:math>";
            variable = new String(xmlTransform(variable, null, XSL), "UTF-8");
            
            //The first character must be a letter
            while (isNumeric(variable.substring(0, 1))) {
                variable = variable.substring(1);
            }
            //The remaining characters, if any, may be letters, digits, or underscores
            variable = variable.replaceAll("[^a-zA-Z0-9_]", "");
            //It has no more than 27 characters
            if (variable.length() > fortranVariableLimit) {
                variable = variable.substring(0, fortranVariableLimit - 1);
            }
            return variable;
        }
        catch (IndexOutOfBoundsException e) {
            String msg = "The variable name " + variable + " is not valid. See Fortrant documentation. Should have at least one letter";
            throw new CCCException(CCCException.CCC001, msg);
        } 
        catch (Exception e) {
            throw new CCCException(CCCException.CCC007, e.getMessage());
        }
    }
}
