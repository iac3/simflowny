/*@@
  @file      ExecutionFlow.F
  @date      Mon May 10 08:41:27 GMT+01:00 2010
  @author    Manual
  @desc
             Execution flow for AdvectionBalsaranegShu2D
  @enddesc
  @version Manual
@@*/

define(FDOCi,((MAX($4(($5) + 1, ($6)), $4(($5) + 2, ($6))) * ($2(($5) + 2, ($6)) - $2(($5) + 1, ($6))) / 6.0 - MAX($4(($5), ($6)), $4(($5) + 1, ($6))) * ($2(($5) + 1, ($6)) - $2(($5), ($6))) / 2.0 + MAX($4(($5) - 1, ($6)), $4(($5), ($6))) * ($2(($5), ($6)) - $2(($5) - 1, ($6))) / 2.0 - MAX($4(($5) - 2, ($6)), $4(($5) - 1, ($6))) * ($2(($5) - 1, ($6)) - $2(($5) - 2, ($6))) / 6.0) + (4.0 * $3(($5) + 1, ($6)) / 3.0 - 4.0 * $3(($5) - 1, ($6)) / 3.0 + $3(($5) - 2, ($6)) / 6.0 - $3(($5) + 2, ($6)) / 6.0)) * 0.5 / deltax)dnl
define(FDOCj,((MAX($4(($5), ($6) + 1), $4(($5), ($6) + 2)) * ($2(($5), ($6) + 2) - $2(($5), ($6) + 1)) / 6.0 - MAX($4(($5), ($6)), $4(($5), ($6) + 1)) * ($2(($5), ($6) + 1) - $2(($5), ($6))) / 2.0 + MAX($4(($5), ($6) - 1), $4(($5), ($6))) * ($2(($5), ($6)) - $2(($5), ($6) - 1)) / 2.0 - MAX($4(($5), ($6) - 2), $4(($5), ($6) - 1)) * ($2(($5), ($6) - 1) - $2(($5), ($6) - 2)) / 6.0) + (4.0 * $3(($5), ($6) + 1) / 3.0 - 4.0 * $3(($5), ($6) - 1) / 3.0 + $3(($5), ($6) - 2) / 6.0 - $3(($5), ($6) + 2) / 6.0)) * 0.5 / deltay)dnl
define(FiphiCap_segmentI,($2))dnl

#include "cctk.h"
#include "cctk_Parameters.h"
#include "cctk_Arguments.h"
#include "cctk_Functions.h"

	subroutine AdvectionBalsaranegShu2D_execution(CCTK_ARGUMENTS)

		implicit none

		DECLARE_CCTK_ARGUMENTS
		DECLARE_CCTK_PARAMETERS
		DECLARE_CCTK_FUNCTIONS

!		Declare local variables
!		-----------------

		INTEGER i, iStart, iEnd
		INTEGER j, jStart, jEnd
		INTEGER istat, status
		CCTK_REAL fluxAccsubphiCap_1

		CCTK_REAL RK3
		CCTK_INT exitCond
		CCTK_REAL deltax
		CCTK_REAL deltay
		CCTK_REAL deltat

		deltax = 2.0/(cctk_gsh(1) - 4)
		deltay = (1.0 * 1.0 - (-1.0))/(cctk_gsh(2) - 4)
		deltat = CCTK_DELTA_TIME

!		Finalization condition
		if (cctk_time .eq. 2.0) then 
			call CCTK_Exit(exitCond , cctkGH, 0)
		end if

!		Evolution
		iStart = 1
		iEnd = cctk_lsh(1)
		jStart = 1
		jEnd = cctk_lsh(2)
		call CCTK_SyncGroup(status, cctkGH, "AdvectionBalsaranegShu2D::phiCapGroup")
		call CCTK_SyncGroup(status, cctkGH, "AdvectionBalsaranegShu2D::TemporalGroup")
		do j = jStart, jEnd
			do i = iStart, iEnd
!				Execute if segment or soft boundary extra point
!				Execute if segment or compatible segment
				if (segment(i, j) .eq. 1 .or. (segment(i, j) .eq. 2 .and. (((i + 1 .le. cctk_lsh(1) .and. segment(i + 1, j) .eq. 1) .or. (i + 2 .le. cctk_lsh(1) .and. segment(i + 2, j) .eq. 1) .or. (j + 1 .le. cctk_lsh(2) .and. segment(i, j + 1) .eq. 1) .or. (j + 2 .le. cctk_lsh(2) .and. segment(i, j + 2) .eq. 1)) .or. ((i - 1 .ge. 1 .and. segment(i - 1, j) .eq. 1) .or. (i - 2 .ge. 1 .and. segment(i - 2, j) .eq. 1) .or. (j - 1 .ge. 1 .and. segment(i, j - 1) .eq. 1) .or. (j - 2 .ge. 1 .and. segment(i, j - 2) .eq. 1))))) then
					FluxsubiphiCap_1(i, j) = FiphiCap_segmentI(CCTK_PASS_FTOF, phiCap_p(i, j))
					SpeedsubiphiCap_1(i, j) = 1.0
				end if
			end do
		end do
		call CCTK_SyncGroup(status, cctkGH, "AdvectionBalsaranegShu2D::phiCapGroup")
		call CCTK_SyncGroup(status, cctkGH, "AdvectionBalsaranegShu2D::TemporalGroup")
		do j = jStart, jEnd
			do i = iStart, iEnd
!				Execute if segment or soft boundary extra point
				if (segment(i, j) .eq. 1) then
					fluxAccsubphiCap_1 = 0.0
					fluxAccsubphiCap_1 = fluxAccsubphiCap_1 - FDOCi(CCTK_PASS_FTOF, phiCap_p, FluxsubiphiCap_1, SpeedsubiphiCap_1, i, j)
					K1subphiCap_1(i, j) = RK3(CCTK_PASS_FTOF, 1, phiCap_p, fluxAccsubphiCap_1, 0, 0, i, j)
					phiCapsupprime(i, j) = phiCap_p(i, j) + 1.0 / 3.0 * K1subphiCap_1(i, j)
				end if
			end do
		end do
		call CCTK_SyncGroup(status, cctkGH, "AdvectionBalsaranegShu2D::phiCapGroup")
		call CCTK_SyncGroup(status, cctkGH, "AdvectionBalsaranegShu2D::TemporalGroup")
		do j = jStart, jEnd
			do i = iStart, iEnd
!				Execute if segment or soft boundary extra point
!				Execute if segment or compatible segment
				if (segment(i, j) .eq. 1 .or. (segment(i, j) .eq. 2 .and. (((i + 1 .le. cctk_lsh(1) .and. segment(i + 1, j) .eq. 1) .or. (i + 2 .le. cctk_lsh(1) .and. segment(i + 2, j) .eq. 1) .or. (j + 1 .le. cctk_lsh(2) .and. segment(i, j + 1) .eq. 1) .or. (j + 2 .le. cctk_lsh(2) .and. segment(i, j + 2) .eq. 1)) .or. ((i - 1 .ge. 1 .and. segment(i - 1, j) .eq. 1) .or. (i - 2 .ge. 1 .and. segment(i - 2, j) .eq. 1) .or. (j - 1 .ge. 1 .and. segment(i, j - 1) .eq. 1) .or. (j - 2 .ge. 1 .and. segment(i, j - 2) .eq. 1))))) then
					FluxsubiphiCap_1(i, j) = FiphiCap_segmentI(CCTK_PASS_FTOF, phiCapsupprime(i, j))
					SpeedsubiphiCap_1(i, j) = 1.0
				end if
			end do
		end do
		call CCTK_SyncGroup(status, cctkGH, "AdvectionBalsaranegShu2D::phiCapGroup")
		call CCTK_SyncGroup(status, cctkGH, "AdvectionBalsaranegShu2D::TemporalGroup")
		do j = jStart, jEnd
			do i = iStart, iEnd
!				Execute if segment or soft boundary extra point
				if (segment(i, j) .eq. 1) then
					fluxAccsubphiCap_1 = 0.0
					fluxAccsubphiCap_1 = fluxAccsubphiCap_1 - FDOCi(CCTK_PASS_FTOF, phiCapsupprime, FluxsubiphiCap_1, SpeedsubiphiCap_1, i, j)
					K2subphiCap_1(i, j) = RK3(CCTK_PASS_FTOF, 2, phiCap_p, fluxAccsubphiCap_1, 0, 0, i, j)
					phiCapsupprimeprime(i, j) = phiCap_p(i, j) + 2.0 / 3.0 * K2subphiCap_1(i, j)
				end if
			end do
		end do
		call CCTK_SyncGroup(status, cctkGH, "AdvectionBalsaranegShu2D::phiCapGroup")
		call CCTK_SyncGroup(status, cctkGH, "AdvectionBalsaranegShu2D::TemporalGroup")
		do j = jStart, jEnd
			do i = iStart, iEnd
!				Execute if segment or soft boundary extra point
!				Execute if segment or compatible segment
				if (segment(i, j) .eq. 1 .or. (segment(i, j) .eq. 2 .and. (((i + 1 .le. cctk_lsh(1) .and. segment(i + 1, j) .eq. 1) .or. (i + 2 .le. cctk_lsh(1) .and. segment(i + 2, j) .eq. 1) .or. (j + 1 .le. cctk_lsh(2) .and. segment(i, j + 1) .eq. 1) .or. (j + 2 .le. cctk_lsh(2) .and. segment(i, j + 2) .eq. 1)) .or. ((i - 1 .ge. 1 .and. segment(i - 1, j) .eq. 1) .or. (i - 2 .ge. 1 .and. segment(i - 2, j) .eq. 1) .or. (j - 1 .ge. 1 .and. segment(i, j - 1) .eq. 1) .or. (j - 2 .ge. 1 .and. segment(i, j - 2) .eq. 1))))) then
					FluxsubiphiCap_1(i, j) = FiphiCap_segmentI(CCTK_PASS_FTOF, phiCapsupprimeprime(i, j))
					SpeedsubiphiCap_1(i, j) = 1.0
				end if
			end do
		end do
		call CCTK_SyncGroup(status, cctkGH, "AdvectionBalsaranegShu2D::phiCapGroup")
		call CCTK_SyncGroup(status, cctkGH, "AdvectionBalsaranegShu2D::TemporalGroup")
		do j = jStart, jEnd
			do i = iStart, iEnd
				if (segment(i, j) .eq. 1) then
					fluxAccsubphiCap_1 = 0.0
					fluxAccsubphiCap_1 = fluxAccsubphiCap_1 - FDOCi(CCTK_PASS_FTOF, phiCapsupprimeprime, FluxsubiphiCap_1, SpeedsubiphiCap_1, i, j)
					K3subphiCap_1(i, j) = RK3(CCTK_PASS_FTOF, 3, phiCap_p, fluxAccsubphiCap_1, 0, 0, i, j)
					phiCap(i, j) = RK3(CCTK_PASS_FTOF, 4, phiCap_p, 0, K1subphiCap_1, K3subphiCap_1, i, j)
				end if
			end do
		end do

		call CCTK_SyncGroup(status, cctkGH, "AdvectionBalsaranegShu2D::phiCapGroup")
		call CCTK_SyncGroup(status, cctkGH, "AdvectionBalsaranegShu2D::TemporalGroup")

	end subroutine AdvectionBalsaranegShu2D_execution