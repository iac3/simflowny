/*@@
  @file      MappingSegments.F
  @date      Mon May 10 08:41:27 GMT+01:00 2010
  @author    Manual
  @desc
             Mapping segments for AdvectionBalsaranegShu2D
  @enddesc
  @version Manual
@@*/

define(leF,(abs(($1) - ($2))/FPC_epsilon_Cactus .gt. 10 .and. FLOOR(abs(($1) - ($2))/FPC_epsilon_Cactus) .lt. 1) .or. ($1) < ($2))dnl
define(geF,(abs(($1) - ($2))/FPC_epsilon_Cactus .gt. 10 .and. FLOOR(abs(($1) - ($2))/FPC_epsilon_Cactus) .lt. 1) .or. ($2) < ($1))dnl

#include "cctk.h"
#include "cctk_Parameters.h"
#include "cctk_Arguments.h"
#include "cctk_Functions.h"

	subroutine AdvectionBalsaranegShu2D_mapping(CCTK_ARGUMENTS)

		implicit none

		DECLARE_CCTK_ARGUMENTS
		DECLARE_CCTK_PARAMETERS
		DECLARE_CCTK_FUNCTIONS

		CCTK_REAL FPC_epsilon_Cactus

!		Declare local variables
!		-----------------

		CCTK_INT i, iMapStart, iMapEnd, iterm, previousMapi, iWallAcc
		logical interiorMapi
		CCTK_INT j, jMapStart, jMapEnd, jterm, previousMapj, jWallAcc
		logical interiorMapj
		CCTK_INT minBlock(2), maxBlock(2), unionsI, facePointI, status, ie1, ie2, ie3, proc
		CCTK_REAL maxDistance, e1, e2, e3, SQRT3INV
		logical done 
		CCTK_REAL deltax
		CCTK_REAL deltay
		CCTK_REAL deltat

		deltax = 2.0/(cctk_gsh(1) - 4)
		deltay = (1.0 * 1.0 - (-1.0))/(cctk_gsh(2) - 4)
		deltat = CCTK_DELTA_TIME

		FPC_epsilon_Cactus = max(-20.0, max(20.0, max(20.0 - 20.0, max(20.0, max(-20.0, 20.0)))))*1e-10
		SQRT3INV = 1.0/SQRT(3.0)
!		Segment mapping
!		----------------
!		Segment: segmentI
		if (1 .le. cctk_ubnd(1) - cctk_lbnd(1) + 1 .and. NINT(1 + (1.0 - 0.0 - -1.0) / deltay) - cctk_lbnd(2) .le. cctk_ubnd(2) - cctk_lbnd(2) + 1 .and. NINT(1 + 2.0 / deltay) - cctk_lbnd(2) .ge. 1) then
			iMapStart = 1
			iMapEnd = cctk_lsh(1)
			if (NINT(1 + (1.0 - 0.0 - -1.0) / deltay) - cctk_lbnd(2) .gt. 1) then
				jMapStart = NINT(1 + (1.0 - 0.0 - -1.0) / deltay) - cctk_lbnd(2)
			else
				jMapStart = 1
			end if
			if (NINT(1 + 2.0 / deltay) - cctk_lbnd(2) .lt. cctk_ubnd(2) - cctk_lbnd(2) + 1) then
				jMapEnd = NINT(1 + 2.0 / deltay) - cctk_lbnd(2)
			else
				jMapEnd = cctk_lsh(2)
			end if
			do i = iMapStart, iMapEnd
				do j = jMapStart, jMapEnd
					segment(i, j) = 1
				end do
			end do
		end if
!		Segment: segmentS
		if (1 .le. cctk_ubnd(1) - cctk_lbnd(1) + 1 .and. NINT(1 + (1.0 - 0.0 - -1.0) / deltay) - cctk_lbnd(2) .le. cctk_ubnd(2) - cctk_lbnd(2) + 1 .and. NINT(1 + 2.0 / deltay) - cctk_lbnd(2) .ge. 1) then
			iMapStart = 1
			iMapEnd = cctk_lsh(1)
			if (NINT(1 + (1.0 - 0.0 - -1.0) / deltay) - cctk_lbnd(2) .gt. 1) then
				jMapStart = NINT(1 + (1.0 - 0.0 - -1.0) / deltay) - cctk_lbnd(2)
			else
				jMapStart = 1
			end if
			if (NINT(1 + 2.0 / deltay) - cctk_lbnd(2) .lt. cctk_ubnd(2) - cctk_lbnd(2) + 1) then
				jMapEnd = NINT(1 + 2.0 / deltay) - cctk_lbnd(2)
			else
				jMapEnd = cctk_lsh(2)
			end if
			do j = jMapStart, jMapEnd
				if(leF(-1.0 + (cctk_lbnd(1)) * deltax, -1.0) .and. geF(-1.0 + (cctk_ubnd(1)) * deltax, -1.0)) then
					segment(iMapStart, j) = 2
				end if
				if(leF(-1.0 + (cctk_lbnd(1)) * deltax, 1.0) .and. geF(-1.0 + (cctk_ubnd(1)) * deltax, 1.0)) then
					segment(iMapEnd, j) = 2
				end if
			end do
			do i = iMapStart, iMapEnd
				if(leF(-1.0 + (cctk_lbnd(2)) * deltay, 1.0 - 0.0) .and. geF(-1.0 + (cctk_ubnd(2)) * deltay, 1.0 - 0.0)) then
					segment(i, jMapStart) = 2
				end if
				if(leF(-1.0 + (cctk_lbnd(2)) * deltay, 1.0) .and. geF(-1.0 + (cctk_ubnd(2)) * deltay, 1.0)) then
					segment(i, jMapEnd) = 2
				end if
			end do
			do j = jMapStart, jMapEnd
				if(leF(-1.0 + (cctk_lbnd(1)) * deltax, -1.0) .and. geF(-1.0 + (cctk_ubnd(1)) * deltax, -1.0)) then
					call checkStencil(CCTK_PASS_FTOF, iMapStart, 1, j, 1, 2)
				end if
				if(leF(-1.0 + (cctk_lbnd(1)) * deltax, 1.0) .and. geF(-1.0 + (cctk_ubnd(1)) * deltax, 1.0)) then
					call checkStencil(CCTK_PASS_FTOF, iMapEnd, 2, j, 1, 2)
				end if
			end do
			do i = iMapStart, iMapEnd
				if(leF(-1.0 + (cctk_lbnd(2)) * deltay, 1.0 - 0.0) .and. geF(-1.0 + (cctk_ubnd(2)) * deltay, 1.0 - 0.0)) then
					call checkStencil(CCTK_PASS_FTOF, i, 1, jMapStart, 1, 2)
				end if
				if(leF(-1.0 + (cctk_lbnd(2)) * deltay, 1.0) .and. geF(-1.0 + (cctk_ubnd(2)) * deltay, 1.0)) then
					call checkStencil(CCTK_PASS_FTOF, i, 1, jMapEnd, 2, 2)
				end if
			end do
		end if

	end subroutine AdvectionBalsaranegShu2D_mapping
	recursive subroutine checkStencil(CCTK_ARGUMENTS, i, iOp, j, jOp, v)
		implicit none

		DECLARE_CCTK_ARGUMENTS
		DECLARE_CCTK_PARAMETERS
		DECLARE_CCTK_FUNCTIONS

		CCTK_INT, INTENT(IN) :: i, iOp, j, jOp, v
		logical iFilled, jFilled
		CCTK_INT iDirection, iIncrement, jDirection, jIncrement

		iFilled = .false.
		iDirection = iOp
		jFilled = .false.
		jDirection = jOp

		if ((i + 1 .le. cctk_lsh(1) .and. segment(i + 1, j) .eq. v) .or. (i - 1 .ge. 1 .and. segment(i - 1, j) .eq. v)) then
			if (i + 1 .le. cctk_lsh(1) .and. segment(i + 1, j) .eq. v) then
				iDirection = 1
			 end if
			if (i - 1 .ge. 1 .and. segment(i - 1, j) .eq. v) then
				iDirection = 2
			 end if
		else
			if (i + 1 .le. cctk_lsh(1) .and. segment(i + 1, j) .ne. v .and. i - 1 .ge. 1 .and. segment(i - 1, j) .ne. v) then
				if (iOp .eq. 1) then
					segment(i + 1, j) = v
					iDirection = 1
				else
					segment(i - 1, j) = v
					iDirection = 2
				end if
				iIncrement = 1
			else
				if ((i + 1 .le. cctk_lsh(1) .and. segment(i + 1, j) .ne. v) .or. (i - 1 .ge. 1 .and. segment(i - 1, j) .ne. v)) then
					if (i + 1 .le. cctk_lsh(1) .and. segment(i + 1, j) .ne. v) then
						segment(i + 1, j) = v
						iDirection = 1
					else
						segment(i - 1, j) = v
						iDirection = 2
					end if
					iIncrement = 1
				end if
			end if
			iFilled = .true.
		end if
		if ((j + 1 .le. cctk_lsh(2) .and. segment(i, j + 1) .eq. v) .or. (j - 1 .ge. 1 .and. segment(i, j - 1) .eq. v)) then
			if (j + 1 .le. cctk_lsh(2) .and. segment(i, j + 1) .eq. v) then
				jDirection = 1
			 end if
			if (j - 1 .ge. 1 .and. segment(i, j - 1) .eq. v) then
				jDirection = 2
			 end if
		else
			if (j + 1 .le. cctk_lsh(2) .and. segment(i, j + 1) .ne. v .and. j - 1 .ge. 1 .and. segment(i, j - 1) .ne. v) then
				if (jOp .eq. 1) then
					segment(i, j + 1) = v
					jDirection = 1
				else
					segment(i, j - 1) = v
					jDirection = 2
				end if
				jIncrement = 1
			else
				if ((j + 1 .le. cctk_lsh(2) .and. segment(i, j + 1) .ne. v) .or. (j - 1 .ge. 1 .and. segment(i, j - 1) .ne. v)) then
					if (j + 1 .le. cctk_lsh(2) .and. segment(i, j + 1) .ne. v) then
						segment(i, j + 1) = v
						jDirection = 1
					else
						segment(i, j - 1) = v
						jDirection = 2
					end if
					jIncrement = 1
				end if
			end if
			jFilled = .true.
		end if
		if (iFilled) then
			if (iDirection .eq. 1) then
				call checkStencil(CCTK_PASS_FTOF, i + iIncrement, iDirection, j, jDirection, v)
			else
				call checkStencil(CCTK_PASS_FTOF, i - iIncrement, iDirection, j, jDirection, v)
			end if
		end if
		if (jFilled) then
			if (jDirection .eq. 1) then
				call checkStencil(CCTK_PASS_FTOF, i, iDirection, j + jIncrement, jDirection, v)
			else
				call checkStencil(CCTK_PASS_FTOF, i, iDirection, j - jIncrement, jDirection, v)
			end if
		end if

	end subroutine checkStencil
