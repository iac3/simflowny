/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.cactusCodeConnectorPluginTest;

import eu.iac3.mathMS.cactusCodeConnectorPlugin.CCCException;
import eu.iac3.mathMS.cactusCodeConnectorPlugin.CactusCodeConnector;
import eu.iac3.mathMS.cactusCodeConnectorPlugin.CodeCodeConnectorImpl;
import eu.iac3.mathMS.cactusCodeConnectorPlugin.utils.CactusCodeConnectorUtils;
import eu.iac3.mathMS.simulationManagerPlugin.SimulationExecType;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.jar.Manifest;

import org.osgi.framework.Constants;
import org.springframework.osgi.test.AbstractConfigurableBundleCreatorTests;
import org.springframework.osgi.test.platform.OsgiPlatform;
import org.springframework.osgi.test.platform.Platforms;

/** 
 * PhysicalModelsTest class is a junit testcase to
 * test the {@link eu.iac3.mathMS.cactusCodeConnectorPlugin.CactusCodeConnector}.
 * 
 * @author      iac3Team
 * @version     1.0
 */
public class CactusCodeConnectorTest extends AbstractConfigurableBundleCreatorTests {

    /**
     * This method is used to set Felix as the 
     * testing platform.
     * Spring-osgi-test
     * 
     * @return OsgiPlatform
     */
    protected String getPlatformName() {
        return Platforms.FELIX;
    }
    
    /**
     * This method is used to set the system parameters
     * to the testing platform.
     * Spring-osgi-test
     * 
     * @return OsgiPlatform
     */
    protected OsgiPlatform createPlatform() {
        System.setProperty("file.encoding", "UTF-8");
        System.setProperty("org.osgi.service.http.port", "8080");
        System.setProperty("derby.system.home", "target" + File.separator + "Derby");
        
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream(".." + File.separator + ".." + File.separator + ".." + File.separator + "test.properties"));
            System.setProperty("eu.iac3.mathMS.CactusCodeConnectorPlugin.cactusHome", 
                    properties.getProperty("eu.iac3.mathMS.CactusCodeConnectorPlugin.cactusHome"));
            System.setProperty("eu.iac3.mathMS.CactusCodeConnectorPlugin.mpichHome", 
                    properties.getProperty("eu.iac3.mathMS.CactusCodeConnectorPlugin.mpichHome"));
            System.setProperty("eu.iac3.mathMS.CactusCodeConnectorPlugin.hdf5Home", 
                    properties.getProperty("eu.iac3.mathMS.CactusCodeConnectorPlugin.hdf5Home"));
            System.setProperty("eu.iac3.mathMS.CactusCodeConnectorPlugin.fortranCompiler", 
                    properties.getProperty("eu.iac3.mathMS.CactusCodeConnectorPlugin.fortranCompiler"));
            System.setProperty("eu.iac3.mathMS.CactusCodeConnectorPlugin.cCompiler", 
                    properties.getProperty("eu.iac3.mathMS.CactusCodeConnectorPlugin.cCompiler"));
            System.setProperty("eu.iac3.mathMS.CactusCodeConnectorPlugin.cppCompiler", 
                    properties.getProperty("eu.iac3.mathMS.CactusCodeConnectorPlugin.cppCompiler"));
            System.setProperty("eu.iac3.mathMS.CactusCodeConnectorPlugin.libZDir", 
                    properties.getProperty("eu.iac3.mathMS.CactusCodeConnectorPlugin.libZDir"));
        } 
        catch (IOException e) {
            fail("Configuration error: test.properties must be configured. " + e.getMessage());
        }

        return super.createPlatform();
    } 
    
    /**
     * This method is used to set the boot 
     * delegation packages.
     * Spring-osgi-test
     * 
     * @return The boot delegation Packages
     */
    protected List<String> getBootDelegationPackages() { 
    	List<String> defaults = new ArrayList<String>(); 
    	defaults.add("java.*"); 
    	return defaults; 
    } 
    
    /**
     * This method is used to set the manifest.
     * Spring-osgi-test
     * 
     * @return The manifest for the test bundle
     */
    protected Manifest getManifest() {
        // let the testing framework create/load the manifest
        Manifest mf = super.getManifest();
        mf.getMainAttributes().putValue(Constants.BUNDLE_REQUIREDEXECUTIONENVIRONMENT, "J2SE-1.5");
        mf.getMainAttributes().putValue(Constants.IMPORT_PACKAGE, ""
                + "eu.iac3.mathMS.codesDBPlugin, eu.iac3.mathMS.simulationManagerPlugin, eu.iac3.mathMS.simulationThreadPlugin,"
        		+ "javax.xml.transform,javax.xml.transform.stream,"
        		+ "org.osgi.service.cm,org.osgi.framework;version=\"1.4.0\", "
        		+ "junit.framework, org.apache.commons.logging, org.springframework.util, org.springframework.osgi.service, "
        		+ "org.springframework.osgi.util, org.springframework.osgi.test, org.springframework.context, "
        		+ "org.springframework.osgi.test.platform, "
        		+ "org.osgi.service.log, org.osgi.util.tracker ");
        mf.getMainAttributes().putValue(Constants.BUNDLE_CLASSPATH, "., lib/derby.jar");
        return mf;
    }
    
    /**
     * This method is used to set bundles
     * to be loaded in the test.
     * Spring-osgi-test
     * 
     * @return The bundles for the test
     */
    protected String[] getTestBundlesNames() {  
        return new String[] {  
            "org.apache.felix,org.osgi.compendium,1.2.0",
            "org.apache.commons,com.springsource.org.apache.commons.logging,1.1.1",
            "eu.iac3.thirdParty.bundles,xml-apis,1.0",
            "org.apache.felix,org.apache.felix.configadmin,1.0.10",
            "org.ops4j.pax.confman,pax-confman-propsloader,0.2.2",
            "org.ops4j.pax.web,pax-web-service,0.5.2",
            "org.ops4j.pax.web-extender,pax-web-ex-war,0.5.0",
            "org.ops4j.pax.web,pax-web-jsp,0.5.2",
            "org.apache.derby,com.springsource.org.apache.derby,10.4.1000003.648739",
            "eu.iac3.mathMS, codesDBPlugin, 2.4",
            "eu.iac3.mathMS, simulationThreadPlugin, 2.4", 
            "eu.iac3.mathMS, simulationManagerPlugin, 2.4"
        };  
    } 
    
    /**
     * Tests {@link eu.iac3.mathMS.cactusCodeConnectorPlugin.CactusCodeConnector #compileCode(String, String, String, Date, PlatformType)}.
     * There are the following tests:
     * Test 1 - Compilation ok
     * Test 2 - Compilation with errors
     * Test 3 - Trying to compile non existing code
     * Test 4 - Configuration error 1
     * Test 5 - Configuration error 2
     */
    public void testCompile() {
        System.out.println("Testing compile code");
        CactusCodeConnector cg;
        String adBasePath = new File("src" + File.separator + "test" + File.separator + "resources" + File.separator + "codeFiles" 
                + File.separator + "Advection2D" + File.separator + "cactus" + File.separator).getAbsolutePath();
        String wrongBasePath = new File("src" + File.separator + "test" + File.separator + "resources" + File.separator + "codeFiles" 
                + File.separator + "Advection2DW" + File.separator + "cactus" + File.separator).getAbsolutePath();
        //Test 1 - Compilation ok
        try {
            cg = new CodeCodeConnectorImpl(bundleContext);
            String path = cg.compileCode("Advection2D", adBasePath);
            assertTrue(new File(adBasePath + File.separator + "exe" + File.separator + "cactus_Advection2D").exists());
            assertEquals(adBasePath + File.separator + "exe" + File.separator + "cactus_Advection2D", path);
        }
        catch (CCCException e) {
            e.printStackTrace();
            if (CCCException.CCC004.equals(e.getErrorCode())) {
                System.out.println("Cannot test compilation procedure, MPICH must be installed.");
                fail("Error testing compile Test 1: " + e.getMessage());
            }
            else {
                fail("Error testing compile Test 1: " + e.getMessage());
            }
        } 
        catch (Exception e) {
            e.printStackTrace();
            fail("Error testing compile Test 1: " + e.getMessage());
        }
        //Test 2 - Compilation with errors
        try {
            cg = new CodeCodeConnectorImpl(bundleContext);
            cg.compileCode("Advection2DW", wrongBasePath);
            fail("Error testing compile Test 2: The compilation have errors!!!!");
        }
        catch (CCCException e) {
            assertEquals(CCCException.CCC004, e.getErrorCode());
        }
        //Test 3 - Trying to compile non existing code
        try {
            cg = new CodeCodeConnectorImpl(bundleContext);
            cg.compileCode("asdf", "src" + File.separator + "test" + File.separator + "nonexists");
            fail("Error testing compile Test 3: The code does not exist!!!!!");
        }
        catch (CCCException e) {
            assertEquals(CCCException.CCC002, e.getErrorCode());
        }
        //Test 4 - Configuration error 1
        try {
            System.setProperty("eu.iac3.mathMS.CactusCodeConnectorPlugin.hdf5Home", "/opt/noExist");
            cg = new CodeCodeConnectorImpl(bundleContext);
            cg.compileCode("Advection2D", adBasePath);
            fail("Error testing compile Test 4: The configuration is wrong!!!!!");
        }
        catch (CCCException e) {
            assertEquals(CCCException.CCC003, e.getErrorCode());
        }
        //Test 5 - Configuration error 2
        try {
            Properties properties = new Properties();
            properties.load(new FileInputStream(".." + File.separator + ".." + File.separator + ".." + File.separator + "test.properties"));
            System.setProperty("eu.iac3.mathMS.CactusCodeConnectorPlugin.hdf5Home", 
                    properties.getProperty("eu.iac3.mathMS.CactusCodeConnectorPlugin.hdf5Home"));
            System.setProperty("eu.iac3.mathMS.CactusCodeConnectorPlugin.fortranCompiler", "noExist");
            cg = new CodeCodeConnectorImpl(bundleContext);
            cg.compileCode("Advection2D", adBasePath);
            fail("Error testing compile Test 5: The configuration is wrong!!!!!");
        }
        catch (CCCException e) {
            assertEquals(CCCException.CCC003, e.getErrorCode());
        } 
        catch (Exception e) {
            fail("Error testing compile Test 5: " + e.getMessage());
        }
    }
    
   /* public void testManual() {
        System.out.println("Test manual");
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream(".." + File.separator + ".." + File.separator + ".." + File.separator + "test.properties"));
            System.setProperty("eu.iac3.mathMS.CactusCodeConnectorPlugin.cactusHome", 
                    properties.getProperty("eu.iac3.mathMS.CactusCodeConnectorPlugin.cactusHome"));
            System.setProperty("eu.iac3.mathMS.CactusCodeConnectorPlugin.mpichHome", 
                    properties.getProperty("eu.iac3.mathMS.CactusCodeConnectorPlugin.mpichHome"));
            System.setProperty("eu.iac3.mathMS.CactusCodeConnectorPlugin.hdf5Home", 
                    properties.getProperty("eu.iac3.mathMS.CactusCodeConnectorPlugin.hdf5Home"));
            System.setProperty("eu.iac3.mathMS.CactusCodeConnectorPlugin.fortranCompiler", 
                    properties.getProperty("eu.iac3.mathMS.CactusCodeConnectorPlugin.fortranCompiler"));
            System.setProperty("eu.iac3.mathMS.CactusCodeConnectorPlugin.cCompiler", 
                    properties.getProperty("eu.iac3.mathMS.CactusCodeConnectorPlugin.cCompiler"));
            System.setProperty("eu.iac3.mathMS.CactusCodeConnectorPlugin.cppCompiler", 
                    properties.getProperty("eu.iac3.mathMS.CactusCodeConnectorPlugin.cppCompiler"));
            System.setProperty("eu.iac3.mathMS.CactusCodeConnectorPlugin.libZDir", 
                    properties.getProperty("eu.iac3.mathMS.CactusCodeConnectorPlugin.libZDir"));
        CactusCodeConnector cg;
        String adBasePath = new File("src" + File.separator + "test" + File.separator + "resources").getAbsolutePath();
        //Test 1 - Compilation ok
            cg = new CodeCodeConnectorImpl(bundleContext);
            cg.compileCode("FDAsNozzle3D", adBasePath);
        }
        catch (CCCException e) {
            e.printStackTrace();
            if (CCCException.CCC004.equals(e.getErrorCode())) {
                System.out.println("Cannot test compilation procedure, MPICH must be installed.");
                fail("Error testing compile Test 1: " + e.getMessage());
            }
            else {
                fail("Error testing compile Test 1: " + e.getMessage());
            }
        } 
        catch (Exception e) {
            e.printStackTrace();
            fail("Error testing compile Test 1: " + e.getMessage());
        }
    }*/
    
    /**
     * Tests {@link eu.iac3.mathMS.cactusCodeConnectorPlugin.CactusCodeConnector #simulate(String, String, int)}.
     * There are the following tests:
     * 1 - Simulate existing and compiled code
     * 2 - Try to simulate existing and not compiled code
     * 3 - Try to simulate non existing code
     */
    
    /*public void testSimulate() {
        System.out.println("Testing simulate code");
        System.out.println("*********************");
        CactusCodeConnector cg;
        CactusCodeConnectorUtils util = new CactusCodeConnectorUtils();
        String adBasePath = new File("src" + File.separator + "test" + File.separator + "resources" + File.separator + "codeFiles" 
                + File.separator + "Advection2D" + File.separator + "cactus" + File.separator).getAbsolutePath();
        String testRemotePath = new File("src" + File.separator + "test" + File.separator + "resources" + File.separator + "generatedCode" 
                + File.separator).getAbsolutePath();
        //Test 1 - Simulate existing and compiled code
        //Local
        
        try {
            System.out.println("Local simulation");
            System.out.println("*********************");
            cg = new CodeCodeConnectorImpl(bundleContext);
            String par = CactusCodeConnectorUtils.convertStreamToString(getClass().getResourceAsStream("/advection2D.par"));
            cg.simulate("Advection2D", adBasePath, par, 1, SimulationExecType.local, false, "v1");
        }
        catch (Exception e) {
            fail("Error testing simulate Local Test 1: " + e.getMessage());
        }
        
         //Unicore
        //No getting result
        try {
            System.out.println("Unicore simulation not getting results");
            System.out.println("*********************");
            cg = new CodeCodeConnectorImpl(bundleContext);
            String par = CactusCodeConnectorUtils.convertStreamToString(getClass().getResourceAsStream("/problem.input"));
            cg.simulate("sim", testRemotePath, par, 2, SimulationExecType.unicore, false, "v1");
            final int sleep = 20000;
            Thread.sleep(sleep);
        }
        catch (Exception e) {
            System.out.println("**************************************************************************");
            System.out.println("WARNING. Error simulating with Unicore. The configuration is not set properly");
            System.out.println("**************************************************************************");
        }
        //Test 2 - Try to simulate existing and not compiled code
        //Local
        try {
            cg = new CodeCodeConnectorImpl(bundleContext);
            util.recursiveDelete(new File(adBasePath + File.separator + "exe"));
            cg.simulate("Advection2D", adBasePath, "", 1, SimulationExecType.local, false, "v1");
            fail("Error testing simulateLocal Test 2: Simulating not compiled code");
        }
        catch (CCCException e) {
            assertEquals(CCCException.CCC005, e.getErrorCode());
        }
        //Test 3 - Try to simulate non existing code
        try {
            cg = new CodeCodeConnectorImpl(bundleContext);
            cg.simulate("asdf", new File("src" + File.separator + "test" + File.separator + "nonexists").getAbsolutePath(), "", 1, 
                    SimulationExecType.local, false, "v1");
            fail("Error testing simulateLocal Test 3: Simulating not existing code");
        }
        catch (CCCException e) {
            assertEquals(CCCException.CCC002, e.getErrorCode());
        }
        
        util.recursiveDelete(new File(adBasePath + File.separator + "result"));
        util.recursiveDelete(new File(testRemotePath + File.separator + "result"));
        //Cleaning the arrangements and executable from cactus
        util.recursiveDelete(new File(".." + File.separator + ".." + File.separator + "thirdParty" + File.separator + "cactus" 
                + File.separator + "exe"));
        util.recursiveDelete(new File(".." + File.separator + ".." + File.separator + "thirdParty" + File.separator + "cactus" 
                + File.separator + "arrangements" + File.separator + "Advection2D"));
        util.recursiveDelete(new File(".." + File.separator + ".." + File.separator + "thirdParty" + File.separator + "cactus" 
                + File.separator + "arrangements" + File.separator + "Advection2DW"));      
    }*/
  
    /**
     * An auxiliar function to read a file as String skiping a number of lines from the begining.
     * 
     * @param is                                The input stream of the file
     * @param skipLines                         The number of lines to skip
     * @return                                  The resource as string
     * @throws UnsupportedEncodingException     Incorrect encoding of the file
     */
    public static String convertStreamToString(InputStream is, int skipLines) throws UnsupportedEncodingException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            int skip = 0;
            while ((line = reader.readLine()) != null) {
                if (skip == skipLines) {
                    sb.append(line + "\n");
                }
                else {
                    skip++;
                }
            }
        } 
        catch (IOException e) {
            e.printStackTrace();
        } 
        finally {
            try {
                is.close();
            } 
            catch (IOException e) { 
                e.printStackTrace();
            }
        }
        return sb.toString();
    }
}
