/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.codeGeneratorPluginTest;

import eu.iac3.mathMS.codeGeneratorPlugin.CodeGenerator;
import eu.iac3.mathMS.codeGeneratorPlugin.CodeGeneratorImpl;
import eu.iac3.mathMS.codeGeneratorPlugin.PlatformType;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.CodeGeneratorUtils;
import eu.iac3.mathMS.documentManagerPlugin.DocumentManager;
import eu.iac3.mathMS.documentManagerPlugin.DocumentManagerImpl;
import eu.iac3.mathMS.simflownyUtilsPlugin.SimflownyUtils;

import static org.ops4j.pax.exam.CoreOptions.bootDelegationPackages;
import static org.ops4j.pax.exam.CoreOptions.junitBundles;
import static org.ops4j.pax.exam.CoreOptions.mavenBundle;
import static org.ops4j.pax.exam.CoreOptions.options;
import static org.ops4j.pax.exam.CoreOptions.systemProperty;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Properties;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;
import org.junit.runner.RunWith;
import org.ops4j.pax.exam.Configuration;
import org.ops4j.pax.exam.Option;
import org.ops4j.pax.exam.junit.PaxExam;
import org.ops4j.pax.exam.spi.reactors.ExamReactorStrategy;
import org.ops4j.pax.exam.spi.reactors.PerSuite;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/** 
 * PhysicalModelsTest class is a junit testcase to
 * test the {@link eu.iac3.mathMS.codeGeneratorPlugin.CodeGenerator}.
 * 
 * @author      iac3Team
 * @version     1.0
 */
@RunWith(PaxExam.class)
@ExamReactorStrategy(PerSuite.class)
public class CodeGeneratorTest {

    //To measure time
    long startTime;
    
    @Rule 
    public TestName name = new TestName();
        
    /**
     * Configuration for Pax-Exam.
     * @return options
     * @throws IOException 
     * @throws FileNotFoundException 
     */
    @Configuration
    public Option[] config() throws FileNotFoundException, IOException {
    	
        Properties properties = new Properties();
        properties.load(new FileInputStream("../../../test.properties"));

    	
        return options(
    		bootDelegationPackages(
    				 "com.sun.*",
    				 "javax.management"
    				),
        	systemProperty("file.encoding").value("UTF-8"),
        	systemProperty("derby.system.home").value("target/Derby"),
        	systemProperty("exist.initdb").value("true"),
        	systemProperty("exist.home").value("."),
        	systemProperty("dataSourceName").value("SIMFLOWNY_DATASOURCE"),
        	systemProperty("ANTLR_USE_DIRECT_CLASS_LOADING").value("true"),
        	systemProperty("org.ops4j.pax.logging.DefaultServiceLog.level").value("WARN"),
        	systemProperty("eu.iac3.codeGeneratorPlugin.mpichHome").value(properties.getProperty("eu.iac3.codeGeneratorPlugin.mpichHome")),
    		systemProperty("eu.iac3.codeGeneratorPlugin.hdf5Home").value(properties.getProperty("eu.iac3.codeGeneratorPlugin.hdf5Home")),
    		systemProperty("eu.iac3.codeGeneratorPlugin.SAMRAIHome").value(properties.getProperty("eu.iac3.codeGeneratorPlugin.SAMRAIHome")),
    		systemProperty("eu.iac3.codeGeneratorPlugin.siloHome").value(properties.getProperty("eu.iac3.codeGeneratorPlugin.siloHome")),
        	
        	mavenBundle("org.apache.felix", "org.osgi.compendium", "1.4.0"),
        	mavenBundle("org.apache.felix", "org.apache.felix.log", "1.0.1"),
            mavenBundle("org.apache.commons", "commons-lang3", "3.7"),
            mavenBundle("org.apache.commons", "commons-text", "1.3"),
            mavenBundle("org.osgi", "org.osgi.enterprise", "5.0.0"), 
            mavenBundle("org.apache.felix", "org.apache.felix.scr.annotations", "1.12.0"),
            mavenBundle("org.apache.felix", "org.apache.felix.scr", "2.1.0"),
            mavenBundle("org.osgi", "org.osgi.dto", "1.1.0"),
            mavenBundle("org.osgi", "org.osgi.core", "6.0.0"),
            mavenBundle("eu.iac3.thirdParty.bundles", "xml-apis", "1.0"),
            mavenBundle("org.apache.servicemix.bundles", "org.apache.servicemix.bundles.relaxngDatatype", "2011.1_1"),
            mavenBundle("org.apache.servicemix.bundles", "org.apache.servicemix.bundles.xmlresolver", "1.2_5"),
            mavenBundle("org.apache.servicemix.bundles", "org.apache.servicemix.bundles.saxon", "9.8.0-10_1"),
            mavenBundle("org.apache.servicemix.bundles", "org.apache.servicemix.bundles.derby", "10.12.1.1_1"),
            mavenBundle("org.ops4j.pax.jdbc", "pax-jdbc-derby", "1.3.0"), 
            mavenBundle("eu.iac3.thirdParty.bundles.xsom", "xsom", "20140925"),
            mavenBundle("com.google.code.gson", "gson", "2.8.4"),
            mavenBundle("org.json", "json", "20180130"),
            mavenBundle("com.fasterxml.jackson.core", "jackson-core", "2.10.3"),
            mavenBundle("com.fasterxml.jackson.core", "jackson-annotations", "2.10.3"),
            mavenBundle("com.fasterxml.jackson.core", "jackson-databind", "2.10.3"),
            mavenBundle("eu.iac3.thirdParty.bundles.underscore", "underscore", "1.60"),
            mavenBundle("org.yaml", "snakeyaml", "1.26"),
            mavenBundle("com.fasterxml.jackson.dataformat", "jackson-dataformat-yaml", "2.10.3"),
            mavenBundle("eu.iac3.thirdParty.bundles.snuggletex", "snuggletex-core-upconversion", "1.2.2"),
            mavenBundle("org.mozilla", "rhino", "1.7.12"),
            mavenBundle("eu.iac3.mathMS", "simflownyUtilsPlugin", "3.1"),
        	mavenBundle("org.apache.felix", "org.apache.felix.configadmin", "1.9.0"),
        	mavenBundle("org.ops4j.pax.confman", "pax-confman-propsloader", "0.2.3"),
            mavenBundle("eu.iac3.mathMS", "codesDBPlugin", "3.1"),
            mavenBundle("org.ops4j.pax.logging", "pax-logging-api", "1.10.1"),
            mavenBundle("org.ops4j.pax.logging", "pax-logging-service", "1.10.1"),
            mavenBundle("org.ops4j.pax.logging", "pax-logging-log4j2", "1.10.1"),
            mavenBundle("com.mchange.c3p0", "com.springsource.com.mchange.v2.c3p0", "0.9.1.2"),
            mavenBundle("com.zaxxer", "HikariCP-java6", "2.3.13"),
            mavenBundle("org.quartz-scheduler", "quartz", "2.3.0"),
            mavenBundle("org.apache.servicemix.bundles", "org.apache.servicemix.bundles.regexp", "1.3_3"),
            mavenBundle("org.apache.servicemix.bundles", "org.apache.servicemix.bundles.lucene", "4.10.4_1"),
            mavenBundle("org.apache.servicemix.bundles", "org.apache.servicemix.bundles.lucene-sandbox", "4.10.4_1"),
            mavenBundle("org.apache.servicemix.bundles", "org.apache.servicemix.bundles.lucene-queries", "4.10.4_1"),
            mavenBundle("org.apache.servicemix.bundles", "org.apache.servicemix.bundles.lucene-queryparser", "4.10.4_1"),
            mavenBundle("org.apache.servicemix.bundles", "org.apache.servicemix.bundles.lucene-analyzers-common", "4.10.4_1"),
            mavenBundle("eu.iac3.thirdParty.bundles.exist", "exist", "4.1"),
            mavenBundle("eu.iac3.mathMS", "eXistDBWrapper", "3.1"),
            mavenBundle("eu.iac3.mathMS", "documentManagerPlugin", "3.1"),
            mavenBundle("eu.iac3.mathMS", "latexPdfGeneratorPlugin", "3.1"),
            junitBundles()
            );
    }
    
    /**
     * Initial setup for the tests.
     */
    @Before
    public void onSetUp() {
        startTime = System.currentTimeMillis();
    }
    
    /**
     * Close browser when last test.
     */
    @After
    public void onTearDown() {
        System.out.println(name.getMethodName() + ": " + (((float)(System.currentTimeMillis() - startTime)) / 60000));
    }
    
    
    /**
     * Tests {@link eu.iac3.mathMS.codeGeneratorPlugin.CodeGenerator #generateCode(String, String, PlatformType, String)}.
     * There are the following tests:
     * 1- Cactus code generation
     * 2- Simplat + SAMRAI code generation
     * 6- Not a valid problem. Must throw {@link eu.iac3.mathMS.codeGeneratorPlugin.CGException #CG001}
     * 7- Already existing code generation. Must throw {@link eu.iac3.mathMS.codeGeneratorPlugin.CGException #CG002}
     * 8- Crossed generation
     * @
     * @throws DMException 
     */
    //////@Test
    /*public void testGenerateCode() throws SUException, DMException {
        CodeGenerator cg;
        //final int headerLength = 9; 
        String discProblem = null;
        DocumentManager docMan = new DocumentManagerImpl();
        String ptSoil = "";

        try {
            ptSoil = docMan.importX3D(SimflownyUtils.xmlToJSON(CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/completex3d.xml"), 0), false), CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/complete.x3d"), 0), "");
            Document doc = CodeGeneratorUtils.stringToDom(SimflownyUtils.jsonToXML(ptSoil));
            ptSoil = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
        }
        catch (Exception e) {
            fail("Error configuring testGenerateCode: " + e.getMessage());
        }
       
        //Test 6- Not a valid problem
        try {
            cg = new CodeGeneratorImpl();
            cg.generateSAMRAICode("{\"text\":\".\",\"children\":[{\"tag\":\"agentBasedModelGraph\",\"jsonPath\":\"/mms:agentBasedModelGraph[1]\",\"order\":0,\"pureType\":\"none\",\"input\":\"none\",\"expanded\":true,\"minCard\":1,\"maxCard\":1,\"children\":[{\"tag\":\"head\",\"jsonPath\":\"/mms:agentBasedModelGraph[1]/mms:head[1]\",\"order\":0,\"pureType\":\"pairList\",\"input\":\"none\",\"expanded\":false,\"minCard\":1,\"maxCard\":1,\"children\":[{\"tag\":\"name\",\"jsonPath\":\"/mms:agentBasedModelGraph[1]/mms:head[1]/mms:name[1]\",\"order\":0,\"pureType\":\"pairListItem\",\"input\":\"Cashandgoods\",\"type\":\"string\",\"minCard\":1,\"maxCard\":1,\"leaf\":true},{\"tag\":\"id\",\"jsonPath\":\"/mms:agentBasedModelGraph[1]/mms:head[1]/mms:id[1]\",\"order\":1,\"pureType\":\"pairListItem\",\"input\":\"123456\",\"type\":\"string\",\"minCard\":1,\"maxCard\":1,\"leaf\":true},{\"tag\":\"version\",\"jsonPath\":\"/mms:agentBasedModelGraph[1]/mms:head[1]/mms:version[1]\",\"order\":2,\"pureType\":\"pairListItem\",\"input\":\"1\",\"type\":\"string\",\"minCard\":1,\"maxCard\":1,\"leaf\":true},{\"tag\":\"date\",\"jsonPath\":\"/mms:agentBasedModelGraph[1]/mms:head[1]/mms:date[1]\",\"order\":3,\"pureType\":\"pairListItem\",\"input\":\"2013-06-25T00:00:00\",\"type\":\"date\",\"minCard\":1,\"maxCard\":1,\"leaf\":true}]}]}]}");
            fail("Test 6. Code generated for a not valid problem!!!!");
        }
        catch (CGException e) {
            assertEquals(CGException.CG001, e.getErrorCode());
        }
        //Test 7- Already existing code generation
        try {
            cg = new CodeGeneratorImpl();
            CodesDB cdb = new CodesDBImpl(bundleContext);
            String id = cdb.addGeneratedCode("Complete Problem", "Test2", "Test2", PlatformType.cactus.toString());
            cdb.addCodeFile(id, "boundary", "file.txt", "File");
            discProblem = SimflownyUtils.xmlToJSON(CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/complete.xml"), 0).replaceAll("SEGMENTSOIL", ptSoil), false);
            cg.generateSAMRAICode(discProblem);
            fail("Test 7. Code generated for existing code!!!!");
        }
        catch (CGException e) {
            assertEquals(CGException.CG002, e.getErrorCode());
        }
        catch (Exception e) {
            fail("Test 7. Error generating code: " + e.getMessage());
        }
    }*/
    
   /* public void testABMMesh() throws SUException, DMException {
        CodeGenerator cg;
        //final int headerLength = 9; 
        String discProblem = null;
        DocumentManager docMan = new DocumentManagerImpl(bundleContext);
        

        //Test 3- Agent based problem on mesh
        try {
            cg = new CodeGeneratorImpl();
            //Model
            String model = docMan.saveDocument(SimflownyUtils.xmlToJSON(CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/Ising_lattice.xml"), 0), false), "");
            Document doc = CodeGeneratorUtils.stringToDom(su.jsonToXML(model));
            model = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();

            
            discProblem = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/Ising_lattice_problem.xml"), 0);
            discProblem = SimflownyUtils.xmlToJSON(discProblem.replaceAll("IsingLatticeID", model), false);

            cg.generateSAMRAICode(discProblem);
            String basePath = "codeFiles" + File.separator + "Ising on lattice" + File.separator + "Test3" + File.separator + "Test3" 
                + File.separator + "simPlatSAMRAI" + File.separator + "Isingonlattice" + File.separator;
            String comp = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/ABP_lattice/Functions.cpp"), 0);
            String result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src" + File.separator + "Functions.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/ABP_lattice/Functions.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src" + File.separator + "Functions.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/ABP_lattice/MainRestartData.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src" + File.separator + "MainRestartData.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/ABP_lattice/MainRestartData.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src" + File.separator + "MainRestartData.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/ABP_lattice/Problem.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src" + File.separator + "Problem.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/ABP_lattice/Problem.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src" + File.separator + "Problem.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/ABP_lattice/SAMRAIConnector.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src" + File.separator + "SAMRAIConnector.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/ABP_lattice/problem.input"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src" + File.separator + "problem.input"), 0);
            assertEquals(comp, result);
        }
        catch (Exception e) {
            e.printStackTrace();
            fail("Test 3. Error generating code: " + e.getMessage());
        }
    }
    
    public void testABMGraph() throws SUException, DMException {
        CodeGenerator cg;
        //final int headerLength = 9; 
        String discProblem = null;
        DocumentManager docMan = new DocumentManagerImpl(bundleContext);
        

        //Test 4- Agent based problem on graph
        try {
            cg = new CodeGeneratorImpl();
            //Model
            String model = docMan.saveDocument(SimflownyUtils.xmlToJSON(
                    CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/Cash_and_goods.xml"), 0), false), "");
            Document doc = CodeGeneratorUtils.stringToDom(su.jsonToXML(model));
            model = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
            
            discProblem = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/Cash_and_goods_problem.xml"), 0);
            discProblem = SimflownyUtils.xmlToJSON(discProblem.replaceAll("Cash_and_goodsID", model), false);

            cg.generateBoostCode(discProblem);
            String basePath = "codeFiles" + File.separator + "Cash and goods" + File.separator + "Test4" + File.separator + "Test4" 
                + File.separator + "simPlatSAMRAI" + File.separator + "Cashandgoods" + File.separator;
            String comp = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/ABP_graph/Cashandgoods.cpp"), 0);
            String result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src" + File.separator + "Cashandgoods.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/ABP_graph/problem.input"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src" + File.separator + "problem.input"), 0);
            assertEquals(comp, result);
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Test 4. Error generating code: " + e.getMessage());
        }
    }
    
    public void testABMMeshless() throws SUException, DMException {
        CodeGenerator cg;
        String discProblem = null;
        DocumentManager docMan = new DocumentManagerImpl(bundleContext);
        

        //Test 5- Agent based problem meshless
        try {
            cg = new CodeGeneratorImpl();
            //Model
            String model = docMan.saveDocument(SimflownyUtils.xmlToJSON(
                    CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/Collective_motion.xml"), 0), false), "");
            Document doc = CodeGeneratorUtils.stringToDom(su.jsonToXML(model));
            model = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
            
            discProblem = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/Collective_motion_problem.xml"), 0);
            discProblem = SimflownyUtils.xmlToJSON(discProblem.replaceAll("Collective_motionID", model), false);

            cg.generateSAMRAICode(discProblem);
            String basePath = "codeFiles" + File.separator + "Collective motion problem" + File.separator + "Test5" + File.separator + "Test5" 
                + File.separator + "simPlatSAMRAI" + File.separator + "Collectivemotionproblem" + File.separator;
            String comp = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/ABP_meshless/Functions.cpp"), 0);
            String result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src" + File.separator + "Functions.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/ABP_meshless/Functions.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src" + File.separator + "Functions.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/ABP_meshless/MainRestartData.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src" + File.separator + "MainRestartData.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/ABP_meshless/MainRestartData.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src" + File.separator + "MainRestartData.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/ABP_meshless/Problem.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src" + File.separator + "Problem.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/ABP_meshless/Problem.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src" + File.separator + "Problem.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/ABP_meshless/NonSync.C"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src" + File.separator + "NonSync.C"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/ABP_meshless/NonSync.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src" + File.separator + "NonSync.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/ABP_meshless/NonSyncs.C"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src" + File.separator + "NonSyncs.C"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/ABP_meshless/NonSyncs.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src" + File.separator + "NonSyncs.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/ABP_meshless/Particle.C"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src" + File.separator + "Particle.C"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/ABP_meshless/Particle.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src" + File.separator + "Particle.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/ABP_meshless/Particles.C"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src" + File.separator + "Particles.C"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/ABP_meshless/Particles.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src" + File.separator + "Particles.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/ABP_meshless/ParticleDataWriter.C"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src" + File.separator + "ParticleDataWriter.C"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/ABP_meshless/ParticleDataWriter.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src" + File.separator + "ParticleDataWriter.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/ABP_meshless/SAMRAIConnector.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src" + File.separator + "SAMRAIConnector.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/ABP_meshless/problem.input"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src" + File.separator + "problem.input"), 0);
            assertEquals(comp, result);
        }
        catch (Exception e) {
            fail("Test 5. Error generating code: " + e.getMessage());
        }
    }
    
    public void testGenericPDE() {
        CodeGenerator cg;
        String discProblem = null;
        try {
            
            cg = new CodeGeneratorImpl();
            discProblem = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/testSecondOrder.xml"), 0);
            discProblem = SimflownyUtils.xmlToJSON(discProblem, false);
            cg.generateSAMRAICode(discProblem);
            String basePath = "codeFiles" + File.separator + "GenericPDE" + File.separator + "Toni Arbona" + File.separator 
                + "1" + File.separator + "simPlatSAMRAI" + File.separator + "GenericPDE" + File.separator;
            String comp = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/genericPDE/Functions.cpp"), 0);
            String result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src" + File.separator + "Functions.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/genericPDE/Functions.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src" + File.separator + "Functions.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/genericPDE/MainRestartData.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src" + File.separator + "MainRestartData.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/genericPDE/MainRestartData.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src" + File.separator + "MainRestartData.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/genericPDE/Problem.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src" + File.separator + "Problem.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/genericPDE/Problem.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src" + File.separator + "Problem.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/genericPDE/SAMRAIConnector.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src" + File.separator + "SAMRAIConnector.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/genericPDE/problem.input"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src" + File.separator + "problem.input"), 0);
            assertEquals(comp, result);
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Test generic PDE. Error generating code: " + e.getMessage());
        }
    }*/
   
   
   
    /**
     * Tests {@link eu.iac3.mathMS.codeGeneratorPlugin.CodeGenerator #generateCode(String, String, PlatformType, String)} 
     * for segment movement.
     * @
     */
    /*public void testSegmentMovement() {
        CodeGenerator cg;
        String discProblem = null;
        
        try {
            String bag1, bag2;
            DocumentManager docMan = new DocumentManagerImpl(bundleContext);
            bag1 = docMan.importX3D(SimflownyUtils.xmlToJSON(CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/nozzle2D-bag0x3d.xml"), 0), false), CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/nozzle2D-bag0.x3d"), 0), "");
            Document doc = CodeGeneratorUtils.stringToDom(su.jsonToXML(bag1));
            bag1 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
            bag2 = docMan.importX3D(SimflownyUtils.xmlToJSON(CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/nozzle2D-bag1x3d.xml"), 0), false), CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/nozzle2D-bag1.x3d"), 0), "");
            doc = CodeGeneratorUtils.stringToDom(su.jsonToXML(bag2));
            bag2 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
            cg = new CodeGeneratorImpl();
            discProblem = SimflownyUtils.xmlToJSON(CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/Movement.xml"), 0), false);
            discProblem = discProblem.replaceAll("Bag1", bag1).replaceAll("Bag2", bag2);
        	
            cg.generateSAMRAICode(discProblem);
            String basePath = "codeFiles" + File.separator + "SegMov" + File.separator + "Test5" + File.separator + "Test5" 
                + File.separator + "simPlatSAMRAI" + File.separator + "SegMov" + File.separator;
            String comp = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/SegmentMovement/Functions.cpp"), 0);
            String result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src" + File.separator + "Functions.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/SegmentMovement/Functions.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src" + File.separator + "Functions.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/SegmentMovement/MainRestartData.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src" + File.separator + "MainRestartData.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/SegmentMovement/MainRestartData.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src" + File.separator + "MainRestartData.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/SegmentMovement/Problem.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src" + File.separator + "Problem.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/SegmentMovement/Problem.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src" + File.separator + "Problem.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/SegmentMovement/Interphase.C"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src" + File.separator + "Interphase.C"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/SegmentMovement/Interphase.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src" + File.separator + "Interphase.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/SegmentMovement/SAMRAIConnector.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src" + File.separator + "SAMRAIConnector.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/SegmentMovement/problem.input"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src" + File.separator + "problem.input"), 0);
            assertEquals(comp, result);
        }
        catch (Exception e) {
            e.printStackTrace();
            fail("Test 6. Error generating movement code: " + e.getMessage());
        }
    }*/
    
    @Test
    public void testIntegration1() {
    	String basePath = "../../../integration tests/1 - Basic";
        CodeGenerator cg;
        String discProblem = null;
        try {
            cg = new CodeGeneratorImpl();
            discProblem = SimflownyUtils.xmlToJSON(CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/discProblem/Advection with Sinus 2D.xml"), 0), false);

            cg.generateSAMRAICode(discProblem);
            String codePath = "codeFiles" + File.separator + "Advection with Sinus 2D" + File.separator + "Carles Bona Jr." + File.separator + "1" 
                + File.separator + "simPlatSAMRAI" + File.separator;
            File f = new File(codePath);
            ArrayList<String> names = new ArrayList<String>(Arrays.asList(f.list()));
            codePath = codePath + File.separator + names.get(0);
            String comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/Functions.cpp"), 0);
            String result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Functions.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/Functions.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Functions.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/MainRestartData.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/MainRestartData.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/MainRestartData.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/MainRestartData.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/Problem.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Problem.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/Problem.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Problem.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/SAMRAIConnector.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/SAMRAIConnector.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/problem.input"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/problem.input"), 0);
            assertEquals(comp, result);

        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Integration test 1. Error generating code: " + e.getMessage());
        }
    }
   
    @Test
    public void testIntegration2() {
    	String basePath = "../../../integration tests/2 - FVM";
        CodeGenerator cg;
        String discProblem = null;
        try {
            cg = new CodeGeneratorImpl();
            discProblem = SimflownyUtils.xmlToJSON(CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/discProblem/Advection with Sinus 2D.xml"), 0), false);

            cg.generateSAMRAICode(discProblem);
            String codePath = "codeFiles" + File.separator + "Advection with Sinus 2D" + File.separator + "Carles Bona Jr." + File.separator + "2" 
                + File.separator + "simPlatSAMRAI" + File.separator;
            File f = new File(codePath);
            ArrayList<String> names = new ArrayList<String>(Arrays.asList(f.list()));
            codePath = codePath + File.separator + names.get(0);
            String comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/Functions.cpp"), 0);
            String result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Functions.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/Functions.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Functions.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/MainRestartData.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/MainRestartData.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/MainRestartData.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/MainRestartData.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/Problem.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Problem.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/Problem.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Problem.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/SAMRAIConnector.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/SAMRAIConnector.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/problem.input"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/problem.input"), 0);
            assertEquals(comp, result);

        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Integration test 2. Error generating code: " + e.getMessage());
        }
    }
    
    @Test
    public void testIntegration3() {
    	String basePath = "../../../integration tests/3 - Advanced Schema 1D";
        CodeGenerator cg;
        String discProblem = null;
        try {
            cg = new CodeGeneratorImpl();
            discProblem = SimflownyUtils.xmlToJSON(CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/discProblem/Advection with Balsara-Shu 2D.xml"), 0), false);

            cg.generateSAMRAICode(discProblem);
            String codePath = "codeFiles" + File.separator + "Advection with Balsara-Shu 2D" + File.separator + "Carles Bona" + File.separator + "1" 
                + File.separator + "simPlatSAMRAI" + File.separator;
            File f = new File(codePath);
            ArrayList<String> names = new ArrayList<String>(Arrays.asList(f.list()));
            codePath = codePath + File.separator + names.get(0);
            String comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/Functions.cpp"), 0);
            String result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Functions.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/Functions.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Functions.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/MainRestartData.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/MainRestartData.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/MainRestartData.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/MainRestartData.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/Problem.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Problem.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/Problem.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Problem.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/SAMRAIConnector.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/SAMRAIConnector.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/problem.input"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/problem.input"), 0);
            assertEquals(comp, result);

        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Integration test 3. Error generating code: " + e.getMessage());
        }
    }
    
    @Test
    public void testIntegration4() {
    	String basePath = "../../../integration tests/4 - Parabolic Terms";
        CodeGenerator cg;
        String discProblem = null;
        
        try {
            cg = new CodeGeneratorImpl();
            discProblem = SimflownyUtils.xmlToJSON(CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/discProblem/Heat Problem 2D.xml"), 0), false);

            cg.generateSAMRAICode(discProblem);
            String codePath = "codeFiles" + File.separator + "Heat Problem 2D" + File.separator + "Toni Arbona" + File.separator + "1" 
                + File.separator + "simPlatSAMRAI" + File.separator;
            File f = new File(codePath);
            ArrayList<String> names = new ArrayList<String>(Arrays.asList(f.list()));
            codePath = codePath + File.separator + names.get(0);
            String comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/Functions.cpp"), 0);
            String result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Functions.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/Functions.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Functions.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/MainRestartData.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/MainRestartData.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/MainRestartData.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/MainRestartData.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/Problem.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Problem.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/Problem.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Problem.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/SAMRAIConnector.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/SAMRAIConnector.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/problem.input"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/problem.input"), 0);
            assertEquals(comp, result);

        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Integration test 4. Error generating code: " + e.getMessage());
        }
    }
    
    @Test
    public void testIntegration5() {
    	String basePath = "../../../integration tests/5 - 2D and Outflow Boundaries";
        CodeGenerator cg;
        String discProblem = null;
        
        try {
            cg = new CodeGeneratorImpl();
            discProblem = SimflownyUtils.xmlToJSON(CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath 
                    + "/discProblem/Wave Equation 2D central Gaussian profile.xml"), 0), false);

            cg.generateSAMRAICode(discProblem);
            String codePath = "codeFiles" + File.separator + "Wave Equation 2D central Gaussian profile" + File.separator + "Toni Arbona" 
                + File.separator + "1" + File.separator + "simPlatSAMRAI" + File.separator;
            File f = new File(codePath);
            ArrayList<String> names = new ArrayList<String>(Arrays.asList(f.list()));
            codePath = codePath + File.separator + names.get(0);
            String comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/Functions.cpp"), 0);
            String result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Functions.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/Functions.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Functions.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/MainRestartData.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/MainRestartData.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/MainRestartData.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/MainRestartData.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/Problem.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Problem.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/Problem.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Problem.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/SAMRAIConnector.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/SAMRAIConnector.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/problem.input"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/problem.input"), 0);
            assertEquals(comp, result);

        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Integration test 5. Error generating code: " + e.getMessage());
        }
    }
    
    @Test
    public void testIntegration6() {
    	String basePath = "../../../integration tests/6 - Advanced Boundaries and Shock Management";
        CodeGenerator cg;
        String discProblem = null;
        
        try {
            cg = new CodeGeneratorImpl();
            discProblem = SimflownyUtils.xmlToJSON(CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/discProblem/Euler with double mach.xml"), 0), false);

            cg.generateSAMRAICode(discProblem);
            String codePath = "codeFiles" + File.separator + "Euler with double mach" + File.separator + "Carles Bona" + File.separator + "1" 
                + File.separator + "simPlatSAMRAI" + File.separator;
            File f = new File(codePath);
            ArrayList<String> names = new ArrayList<String>(Arrays.asList(f.list()));
            codePath = codePath + File.separator + names.get(0);
            String comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/Functions.cpp"), 0);
            String result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Functions.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/Functions.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Functions.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/MainRestartData.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/MainRestartData.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/MainRestartData.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/MainRestartData.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/Problem.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Problem.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/Problem.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Problem.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/SAMRAIConnector.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/SAMRAIConnector.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/problem.input"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/problem.input"), 0);
            assertEquals(comp, result);

        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Integration test 6. Error generating code: " + e.getMessage());
        }
    }
   
    @Test
    public void testIntegration7() {
    	String basePath = "../../../integration tests/7 - Advanced Schema 2D";
        CodeGenerator cg;
        String discProblem = null;
        
        try {
            cg = new CodeGeneratorImpl();
            discProblem = SimflownyUtils.xmlToJSON(CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/discProblem/Euler Periodic Vortex.xml"), 0), false);

            cg.generateSAMRAICode(discProblem);
            String codePath = "codeFiles" + File.separator + "Euler Periodic Vortex" + File.separator + "Miquel Trias" + File.separator + "1" 
                + File.separator + "simPlatSAMRAI" + File.separator;
            File f = new File(codePath);
            ArrayList<String> names = new ArrayList<String>(Arrays.asList(f.list()));
            codePath = codePath + File.separator + names.get(0);
            String comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/Functions.cpp"), 0);
            String result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Functions.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/Functions.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Functions.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/MainRestartData.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/MainRestartData.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/MainRestartData.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/MainRestartData.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/Problem.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Problem.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/Problem.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Problem.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/SAMRAIConnector.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/SAMRAIConnector.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/problem.input"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/problem.input"), 0);
            assertEquals(comp, result);

        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Integration test 7. Error generating code: " + e.getMessage());
        }
    }
    
    @Test
    public void testIntegration8() {
    	String basePath = "../../../integration tests/8 - Non-reflecting Boundaries and Hyperbolic-Parabolic";
        CodeGenerator cg;
        String discProblem = null;
        
        try {
            cg = new CodeGeneratorImpl();
            discProblem = SimflownyUtils.xmlToJSON(CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/discProblem/Compressible Counterflow 2D.xml"), 0), false);

            cg.generateSAMRAICode(discProblem);
            String codePath = "codeFiles" + File.separator + "Compressible Counterflow 2D" + File.separator + "Miquel Trias" + File.separator + "1" 
                + File.separator + "simPlatSAMRAI" + File.separator;
            File f = new File(codePath);
            ArrayList<String> names = new ArrayList<String>(Arrays.asList(f.list()));
            codePath = codePath + File.separator + names.get(0);
            String comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/Functions.cpp"), 0);
            String result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Functions.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/Functions.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Functions.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/MainRestartData.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/MainRestartData.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/MainRestartData.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/MainRestartData.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/Problem.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Problem.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/Problem.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Problem.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/SAMRAIConnector.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/SAMRAIConnector.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/problem.input"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/problem.input"), 0);
            assertEquals(comp, result);

        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Integration test 8. Error generating code: " + e.getMessage());
        }
    }
    
    @Test
    public void testIntegration9() {
    	String basePath = "../../../integration tests/9 - AuxiliaryVariables";
        CodeGenerator cg;
        String discProblem = null;
        
        try {
            cg = new CodeGeneratorImpl();
            discProblem = SimflownyUtils.xmlToJSON(CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/discProblem/discProblem.xml"), 0), false);

            cg.generateSAMRAICode(discProblem);
            String codePath = "codeFiles" + File.separator + "AuxiliaryVariableTestProblem" + File.separator + "Borja Miñano" + File.separator + "1" 
                + File.separator + "simPlatSAMRAI" + File.separator;
            File f = new File(codePath);
            ArrayList<String> names = new ArrayList<String>(Arrays.asList(f.list()));
            codePath = codePath + File.separator + names.get(0);
            String comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/Functions.cpp"), 0);
            String result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Functions.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/Functions.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Functions.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/MainRestartData.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/MainRestartData.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/MainRestartData.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/MainRestartData.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/Problem.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Problem.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/Problem.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Problem.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/SAMRAIConnector.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/SAMRAIConnector.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/problem.input"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/problem.input"), 0);
            assertEquals(comp, result);

        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Integration test 9. Error generating code: " + e.getMessage());
        }
    }
   
    @Test
    public void testIntegration10() {
    	String basePath = "../../../integration tests/10 - 3D Extrapolations and Segment Interaction";
        CodeGenerator cg;
        String discProblem = null;
        
        try {
            cg = new CodeGeneratorImpl();
            DocumentManager docMan = new DocumentManagerImpl();
            String segmentID = docMan.importX3D(SimflownyUtils.xmlToJSON(CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/regions/planex3d.xml"), 0), false), CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/regions/Plane.x3d"), 0), "");
            Document doc = CodeGeneratorUtils.stringToDom(SimflownyUtils.jsonToXML(segmentID));
            segmentID = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
            discProblem = SimflownyUtils.xmlToJSON(CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/discProblem/FDAsNozzle 3D.xml"), 0), false);
            discProblem = discProblem.replaceAll("SEGMENTID", segmentID);
            cg.generateSAMRAICode(discProblem);
            String codePath = "codeFiles" + File.separator + "FDAsNozzle 3D" + File.separator + "Miquel Trias" + File.separator + "1" 
                + File.separator + "simPlatSAMRAI" + File.separator;
            File f = new File(codePath);
            ArrayList<String> names = new ArrayList<String>(Arrays.asList(f.list()));
            codePath = codePath + File.separator + names.get(0);
            String comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/Functions.cpp"), 0);
            String result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Functions.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/Functions.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Functions.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/MainRestartData.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/MainRestartData.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/MainRestartData.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/MainRestartData.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/Problem.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Problem.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/Problem.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Problem.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/SAMRAIConnector.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/SAMRAIConnector.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/problem.input"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/problem.input"), 0);
            assertEquals(comp, result);
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Integration test 10. Error generating code: " + e.getMessage());
        }
    }
    
    @Test
    public void testIntegration11() {
    	String basePath = "../../../integration tests/11 - SPH";
        CodeGenerator cg;
        String discProblem = null;
        
        try {
            cg = new CodeGeneratorImpl();
            discProblem = SimflownyUtils.xmlToJSON(CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/discProblem/Dam break 2D.xml"), 0), false);

            cg.generateSAMRAICode(discProblem);
            String codePath = "codeFiles" + File.separator + "Dam break 2D" + File.separator + "Toni Arbona" + File.separator + "1" 
                + File.separator + "simPlatSAMRAI" + File.separator;
            File f = new File(codePath);
            ArrayList<String> names = new ArrayList<String>(Arrays.asList(f.list()));
            codePath = codePath + File.separator + names.get(0);
            String comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/Functions.cpp"), 0);
            String result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Functions.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/Functions.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Functions.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/MainRestartData.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/MainRestartData.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/MainRestartData.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/MainRestartData.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/Problem.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Problem.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/Problem.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Problem.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/SAMRAIConnector.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/SAMRAIConnector.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/problem.input"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/problem.input"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/NonSync.C"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/NonSync.C"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/NonSync.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/NonSync.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/NonSyncs.C"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/NonSyncs.C"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/NonSyncs.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/NonSyncs.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/Particle_water.C"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Particle_water.C"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/Particles.C"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Particles.C"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/Particles.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Particles.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/ParticleDataWriter.C"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/ParticleDataWriter.C"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/ParticleDataWriter.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/ParticleDataWriter.h"), 0);
            assertEquals(comp, result);
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Integration test 11. Error generating code: " + e.getMessage());
        }
    }
    /*
    ////@Test
    public void testIntegration12() {
        String basePath = "../../../integration tests/12 - ABM on graph";
        CodeGenerator cg;
        String discProblem = null;
        DocumentManager docMan = null;
        
        try {
            docMan = new DocumentManagerImpl();

            String model = docMan.addDocument(SimflownyUtils.xmlToJSON(CodeGeneratorUtils.convertStreamToString(
                    new FileInputStream(basePath + "/model/Cash and goods_1_Miquel Trias.xml"), 0), false), "");
            Document doc = CodeGeneratorUtils.stringToDom(SimflownyUtils.jsonToXML(model));
            String id = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
            
            cg = new CodeGeneratorImpl();
            discProblem = SimflownyUtils.xmlToJSON(CodeGeneratorUtils.convertStreamToString(
                    new FileInputStream(basePath + "/problem/Cash and goods.xml"), 0).replaceAll("bb43430f-12de-4cd3-9296-a9d7a8949187", id), false);

            cg.generateBoostCode(discProblem);
            
            String codePath = "codeFiles" + File.separator + "Cash and goods" + File.separator + "Miquel Trias" + File.separator + "1" 
                + File.separator + "simPlatSAMRAI" + File.separator;
            File f = new File(codePath);
            ArrayList<String> names = new ArrayList<String>(Arrays.asList(f.list()));
            codePath = codePath + File.separator + names.get(0);
            String comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/Cashandgoods.cpp"), 0);
            String result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Cashandgoods.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/iac3_plod_generator.hpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/iac3_plod_generator.hpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/problem.input"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/problem.input"), 0);
            assertEquals(comp, result);
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Integration test 12. Error generating code: " + e.getMessage());
        }
    }
    
    ////@Test
    public void testIntegration13() {
        String basePath = "../../../integration tests/13 - ABM on spatial domain";
        CodeGenerator cg;
        String discProblem = null;
        DocumentManager docMan = null;
        
        try {
            docMan = new DocumentManagerImpl();
            String model = docMan.addDocument(SimflownyUtils.xmlToJSON(CodeGeneratorUtils.convertStreamToString(
                    new FileInputStream(basePath + "/model/Collective motion 2D_1_Toni Arbona.xml"), 0), false), "");
            Document doc = CodeGeneratorUtils.stringToDom(SimflownyUtils.jsonToXML(model));
            String id = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
            
            cg = new CodeGeneratorImpl();
            discProblem = SimflownyUtils.xmlToJSON(CodeGeneratorUtils.convertStreamToString(new FileInputStream(
                    basePath + "/problem/Collective motion problem.xml"), 0).replaceAll("918f5638-0a2b-4547-a254-8e8bf43c71f5", id), false);

            cg.generateSAMRAICode(discProblem);
            String codePath = "codeFiles" + File.separator + "Collective motion problem" + File.separator + "Toni Arbona" + File.separator + "1" 
                + File.separator + "simPlatSAMRAI" + File.separator;
            File f = new File(codePath);
            ArrayList<String> names = new ArrayList<String>(Arrays.asList(f.list()));
            codePath = codePath + File.separator + names.get(0);
            String comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/Functions.cpp"), 0);
            String result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Functions.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/Functions.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Functions.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/MainRestartData.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/MainRestartData.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/MainRestartData.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/MainRestartData.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/Problem.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Problem.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/Problem.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Problem.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAIConnector.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/SAMRAIConnector.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/problem.input"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/problem.input"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/NonSync.C"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/NonSync.C"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/NonSync.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/NonSync.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/NonSyncs.C"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/NonSyncs.C"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/NonSyncs.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/NonSyncs.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/Particle.C"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Particle.C"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/Particle.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Particle.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/Particles.C"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Particles.C"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/Particles.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Particles.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/ParticleDataWriter.C"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/ParticleDataWriter.C"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/ParticleDataWriter.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/ParticleDataWriter.h"), 0);
            assertEquals(comp, result);
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Integration test 13. Error generating code: " + e.getMessage());
        }
    }*/
    
    @Test
    public void testIntegration14() {
        String basePath = "../../../integration tests/14 - genericPDE";
        CodeGenerator cg;
        String discProblem = null;
        
        try {
            cg = new CodeGeneratorImpl();
            discProblem = SimflownyUtils.xmlToJSON(CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/discProblem/WaveEquation2D2ndOrder.xml"), 0), false);
            cg.generateSAMRAICode(discProblem);
            String codePath = "codeFiles" + File.separator + "Wave Equation 2D second order" + File.separator + "Toni Arbona" + File.separator + "Gauge" 
                + File.separator + "simPlatSAMRAI" + File.separator;
            File f = new File(codePath);
            ArrayList<String> names = new ArrayList<String>(Arrays.asList(f.list()));
            codePath = codePath + File.separator + names.get(0);
            String comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/Functions.cpp"), 0);
            String result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Functions.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/Functions.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Functions.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/MainRestartData.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/MainRestartData.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/MainRestartData.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/MainRestartData.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/Problem.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Problem.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/Problem.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Problem.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAIConnector.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/SAMRAIConnector.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/problem.input"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/problem.input"), 0);
            assertEquals(comp, result);
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Integration test 14. Error generating code: " + e.getMessage());
        }
    }
    
    
    @Test
    public void testIntegration15() {
        String basePath = "../../../integration tests/15 - ComplexGenericPDE";
        CodeGenerator cg;
        String discProblem = null;
        
        try {
            cg = new CodeGeneratorImpl();
            discProblem = SimflownyUtils.xmlToJSON(CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/discProblem/WaveTest.xml"), 0), false);
            cg.generateSAMRAICode(discProblem);
            String codePath = "codeFiles" + File.separator + "Integration Test 15" + File.separator + "Carlos Palenzuela" + File.separator + "15" 
                + File.separator + "simPlatSAMRAI" + File.separator;
            File f = new File(codePath);
            ArrayList<String> names = new ArrayList<String>(Arrays.asList(f.list()));
            codePath = codePath + File.separator + names.get(0);
            String comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/Functions.cpp"), 0);
            String result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Functions.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/Functions.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Functions.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/MainRestartData.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/MainRestartData.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/MainRestartData.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/MainRestartData.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/Problem.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Problem.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/Problem.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Problem.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAIConnector.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/SAMRAIConnector.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/problem.input"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/problem.input"), 0);
            assertEquals(comp, result);
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Integration test 15. Error generating code: " + e.getMessage());
        }
    }
    
    @Test
    public void testIntegration16() {
        String basePath = "../../../integration tests/16 - BlackHole3D";
        CodeGenerator cg;
        String discProblem = null;
        
        try {
            cg = new CodeGeneratorImpl();
            discProblem = SimflownyUtils.xmlToJSON(CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/discProblem/blackhole.xml"), 0), false);
            cg.generateSAMRAICode(discProblem);
            Thread.sleep(15000);
            String codePath = "codeFiles" + File.separator + "Einstein Boson Star" + File.separator + "Carlos Palenzuela" + File.separator + "ccz4 sf2" 
                + File.separator + "simPlatSAMRAI" + File.separator;
            File f = new File(codePath);
            ArrayList<String> names = new ArrayList<String>(Arrays.asList(f.list()));
            codePath = codePath + File.separator + names.get(0);
            String comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/Functions.cpp"), 0);
            String result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Functions.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/Functions.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Functions.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/MainRestartData.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/MainRestartData.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/MainRestartData.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/MainRestartData.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/Problem.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Problem.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/Problem.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Problem.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAIConnector.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/SAMRAIConnector.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/problem.input"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/problem.input"), 0);
            assertEquals(comp, result);
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Integration test 16. Error generating code: " + e.getMessage());
        }
    }
   
    @Test
    public void testIntegration17() {
        String basePath = "../../../integration tests/17 - NeutronStars";
        CodeGenerator cg;
        String discProblem = null;
        
        try {
            cg = new CodeGeneratorImpl();
            discProblem = SimflownyUtils.xmlToJSON(CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/discProblem/neutronStar.xml"), 0), false);
            cg.generateSAMRAICode(discProblem);
            Thread.sleep(15000);
            String codePath = "codeFiles" + File.separator + "Einstein Neutron Star" + File.separator + "Carlos Palenzuela" + File.separator + "ccz4 sf2" 
                + File.separator + "simPlatSAMRAI" + File.separator;
            File f = new File(codePath);
            ArrayList<String> names = new ArrayList<String>(Arrays.asList(f.list()));
            codePath = codePath + File.separator + names.get(0);
            String comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/Functions.cpp"), 0);
            String result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Functions.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/Functions.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Functions.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/MainRestartData.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/MainRestartData.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/MainRestartData.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/MainRestartData.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/Problem.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Problem.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/Problem.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Problem.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAIConnector.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/SAMRAIConnector.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/TimeInterpolator.C"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/TimeInterpolator.C"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/problem.input"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/problem.input"), 0);
            assertEquals(comp, result);
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Integration test 17. Error generating code: " + e.getMessage());
        }
    }
   
    @Test
    public void testIntegration18() {
        String basePath = "../../../integration tests/18 - conditionalEvolution";
        CodeGenerator cg;
        String discProblem = null;
        
        try {
            cg = new CodeGeneratorImpl();
            discProblem = SimflownyUtils.xmlToJSON(CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/discProblem/WaveEquation2D2ndOrder.xml"), 0), false);
            cg.generateSAMRAICode(discProblem);
            String codePath = "codeFiles" + File.separator + "Wave Equation 2D second order" + File.separator + "Toni Arbona" + File.separator + "Gauge" 
                + File.separator + "simPlatSAMRAI" + File.separator;
            File f = new File(codePath);
            ArrayList<String> names = new ArrayList<String>(Arrays.asList(f.list()));
            codePath = codePath + File.separator + names.get(0);
            String comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/Functions.cpp"), 0);
            String result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Functions.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/Functions.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Functions.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/MainRestartData.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/MainRestartData.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/MainRestartData.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/MainRestartData.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/Problem.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Problem.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/Problem.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Problem.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAIConnector.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/SAMRAIConnector.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/problem.input"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/problem.input"), 0);
            assertEquals(comp, result);
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Integration test 18. Error generating code: " + e.getMessage());
        }
    }
    
    @Test
    public void testIntegration19() {
    	String basePath = "../../../integration tests/19 - Particles";
        CodeGenerator cg;
        String discProblem = null;
        
        try {
            cg = new CodeGeneratorImpl();
            discProblem = SimflownyUtils.xmlToJSON(CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/discProblem/ParticleCode.xml"), 0), false);

            cg.generateSAMRAICode(discProblem);
            String codePath = "codeFiles" + File.separator + "Particle code" + File.separator + "Borja Miñano" + File.separator + "1" 
                + File.separator + "simPlatSAMRAI" + File.separator;
            File f = new File(codePath);
            ArrayList<String> names = new ArrayList<String>(Arrays.asList(f.list()));
            codePath = codePath + File.separator + names.get(0);
            String comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/Functions.cpp"), 0);
            String result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Functions.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/Functions.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Functions.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/Problem.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Problem.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/Problem.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Problem.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAIConnector.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/SAMRAIConnector.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/problem.input"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/problem.input"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/Particle_particles.C"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Particle_particles.C"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/Particle_particles.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Particle_particles.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/Particles.C"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Particles.C"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/Particles.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Particles.h"), 0);
            assertEquals(comp, result);
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Integration test 19. Error generating code: " + e.getMessage());
        }
    }
    
    @Test
    public void testIntegration20() {
    	String basePath = "../../../integration tests/20 - Two Schemas";
        CodeGenerator cg;
        String discProblem = null;
        
        try {
            cg = new CodeGeneratorImpl();
            discProblem = SimflownyUtils.xmlToJSON(CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/discProblem/TwoSchemas.xml"), 0), false);

            cg.generateSAMRAICode(discProblem);
            String codePath = "codeFiles" + File.separator + "Two Schemas" + File.separator + "Carlos Palenzuela" + File.separator + "1" 
                + File.separator + "simPlatSAMRAI" + File.separator;
            File f = new File(codePath);
            ArrayList<String> names = new ArrayList<String>(Arrays.asList(f.list()));
            codePath = codePath + File.separator + names.get(0);
            String comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/Functions.cpp"), 0);
            String result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Functions.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/Functions.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Functions.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/TimeInterpolator.C"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/TimeInterpolator.C"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/Problem.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Problem.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/Problem.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Problem.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/SAMRAIConnector.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/SAMRAIConnector.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/RefineAlgorithm.C"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/RefineAlgorithm.C"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/RefineAlgorithm.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/RefineAlgorithm.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/src/SAMRAI/problem.input"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/problem.input"), 0);
            assertEquals(comp, result);

        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Integration test 20. Error generating code: " + e.getMessage());
        }
    }
    
    @Test
    public void testSchemasRK4CD() {
        String basePath = "../../../schema validation tests/RK4 CD4";
        CodeGenerator cg;
        String discProblem = null;
        
        try {
            cg = new CodeGeneratorImpl();
            discProblem = SimflownyUtils.xmlToJSON(CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/discProblem/wave.xml"), 0), false);
            cg.generateSAMRAICode(discProblem);
            Thread.sleep(15000);
            String codePath = "codeFiles" + File.separator + "Wave equation marriage" + File.separator + "Toni Arbona" + File.separator + "RK4CD4" 
                + File.separator + "simPlatSAMRAI" + File.separator;
            File f = new File(codePath);
            ArrayList<String> names = new ArrayList<String>(Arrays.asList(f.list()));
            codePath = codePath + File.separator + names.get(0);
            String comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/Functions.cpp"), 0);
            String result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Functions.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/Functions.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Functions.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/MainRestartData.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/MainRestartData.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/MainRestartData.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/MainRestartData.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/Problem.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Problem.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/Problem.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Problem.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/SAMRAIConnector.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/SAMRAIConnector.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/TimeInterpolator.C"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/TimeInterpolator.C"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/problem.input"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/problem.input"), 0);
            assertEquals(comp, result);
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Schema Validation Test RK4CD. Error generating code: " + e.getMessage());
        }
    }
    
    @Test
    public void testSchemasRK4MP5() {
        String basePath = "../../../schema validation tests/RK4 MP5";
        CodeGenerator cg;
        String discProblem = null;
        
        try {
            cg = new CodeGeneratorImpl();
            discProblem = SimflownyUtils.xmlToJSON(CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/discProblem/wave.xml"), 0), false);
            cg.generateSAMRAICode(discProblem);
            Thread.sleep(15000);
            String codePath = "codeFiles" + File.separator + "Wave equation marriage" + File.separator + "Toni Arbona" + File.separator + "RK4MP5" 
                + File.separator + "simPlatSAMRAI" + File.separator;
            File f = new File(codePath);
            ArrayList<String> names = new ArrayList<String>(Arrays.asList(f.list()));
            codePath = codePath + File.separator + names.get(0);
            String comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/Functions.cpp"), 0);
            String result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Functions.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/Functions.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Functions.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/MainRestartData.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/MainRestartData.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/MainRestartData.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/MainRestartData.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/Problem.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Problem.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/Problem.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Problem.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/SAMRAIConnector.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/SAMRAIConnector.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/TimeInterpolator.C"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/TimeInterpolator.C"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/problem.input"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/problem.input"), 0);
            assertEquals(comp, result);
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Schema Validation Test RK4MP5. Error generating code: " + e.getMessage());
        }
    }
    
    @Test
    public void testSchemasRK4WENO5() {
        String basePath = "../../../schema validation tests/RK4 WENO5";
        CodeGenerator cg;
        String discProblem = null;
        
        try {
            cg = new CodeGeneratorImpl();
            discProblem = SimflownyUtils.xmlToJSON(CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/discProblem/wave.xml"), 0), false);
            cg.generateSAMRAICode(discProblem);
            Thread.sleep(15000);
            String codePath = "codeFiles" + File.separator + "Wave equation marriage" + File.separator + "Toni Arbona" + File.separator + "RK4WENO5" 
                + File.separator + "simPlatSAMRAI" + File.separator;
            File f = new File(codePath);
            ArrayList<String> names = new ArrayList<String>(Arrays.asList(f.list()));
            codePath = codePath + File.separator + names.get(0);
            String comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/Functions.cpp"), 0);
            String result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Functions.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/Functions.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Functions.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/MainRestartData.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/MainRestartData.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/MainRestartData.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/MainRestartData.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/Problem.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Problem.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/Problem.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Problem.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/SAMRAIConnector.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/SAMRAIConnector.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/TimeInterpolator.C"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/TimeInterpolator.C"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/problem.input"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/problem.input"), 0);
            assertEquals(comp, result);
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Schema Validation Test RK4WENO5. Error generating code: " + e.getMessage());
        }
    }
    
    @Test
    public void testSchemasRK4WENO5Z() {
        String basePath = "../../../schema validation tests/RK4 WENO5Z";
        CodeGenerator cg;
        String discProblem = null;
        
        try {
            cg = new CodeGeneratorImpl();
            discProblem = SimflownyUtils.xmlToJSON(CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/discProblem/wave.xml"), 0), false);
            cg.generateSAMRAICode(discProblem);
            Thread.sleep(15000);
            String codePath = "codeFiles" + File.separator + "Wave equation marriage" + File.separator + "Toni Arbona" + File.separator + "RK4WENO5Z" 
                + File.separator + "simPlatSAMRAI" + File.separator;
            File f = new File(codePath);
            ArrayList<String> names = new ArrayList<String>(Arrays.asList(f.list()));
            codePath = codePath + File.separator + names.get(0);
            String comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/Functions.cpp"), 0);
            String result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Functions.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/Functions.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Functions.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/MainRestartData.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/MainRestartData.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/MainRestartData.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/MainRestartData.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/Problem.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Problem.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/Problem.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Problem.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/SAMRAIConnector.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/SAMRAIConnector.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/TimeInterpolator.C"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/TimeInterpolator.C"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/problem.input"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/problem.input"), 0);
            assertEquals(comp, result);
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Schema Validation Test RK4WENO5Z. Error generating code: " + e.getMessage());
        }
    }
    
    @Test
    public void testSchemasRK4FDOC5() {
        String basePath = "../../../schema validation tests/RK4 FDOC5";
        CodeGenerator cg;
        String discProblem = null;
        
        try {
            cg = new CodeGeneratorImpl();
            discProblem = SimflownyUtils.xmlToJSON(CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/discProblem/wave.xml"), 0), false);
            cg.generateSAMRAICode(discProblem);
            Thread.sleep(15000);
            String codePath = "codeFiles" + File.separator + "Wave equation marriage" + File.separator + "Toni Arbona" + File.separator + "RK4FDOC5" 
                + File.separator + "simPlatSAMRAI" + File.separator;
            File f = new File(codePath);
            ArrayList<String> names = new ArrayList<String>(Arrays.asList(f.list()));
            codePath = codePath + File.separator + names.get(0);
            String comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/Functions.cpp"), 0);
            String result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Functions.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/Functions.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Functions.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/MainRestartData.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/MainRestartData.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/MainRestartData.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/MainRestartData.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/Problem.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Problem.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/Problem.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Problem.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/SAMRAIConnector.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/SAMRAIConnector.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/TimeInterpolator.C"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/TimeInterpolator.C"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/problem.input"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/problem.input"), 0);
            assertEquals(comp, result);
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Schema Validation Test RK4FDOC5. Error generating code: " + e.getMessage());
        }
    }
    
    @Test
    public void testSchemasRK3MP5() {
        String basePath = "../../../schema validation tests/RK3 MP5";
        CodeGenerator cg;
        String discProblem = null;
        
        try {
            cg = new CodeGeneratorImpl();
            discProblem = SimflownyUtils.xmlToJSON(CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/discProblem/wave.xml"), 0), false);
            cg.generateSAMRAICode(discProblem);
            Thread.sleep(15000);
            String codePath = "codeFiles" + File.separator + "Wave equation marriage" + File.separator + "Toni Arbona" + File.separator + "RK3MP5" 
                + File.separator + "simPlatSAMRAI" + File.separator;
            File f = new File(codePath);
            ArrayList<String> names = new ArrayList<String>(Arrays.asList(f.list()));
            codePath = codePath + File.separator + names.get(0);
            String comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/Functions.cpp"), 0);
            String result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Functions.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/Functions.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Functions.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/MainRestartData.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/MainRestartData.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/MainRestartData.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/MainRestartData.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/Problem.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Problem.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/Problem.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Problem.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/SAMRAIConnector.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/SAMRAIConnector.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/TimeInterpolator.C"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/TimeInterpolator.C"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/problem.input"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/problem.input"), 0);
            assertEquals(comp, result);
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Schema Validation Test RK3MP5. Error generating code: " + e.getMessage());
        }
    }
    
    @Test
    public void testSchemasRK3WENO3() {
        String basePath = "../../../schema validation tests/RK3 WENO3";
        CodeGenerator cg;
        String discProblem = null;
        
        try {
            cg = new CodeGeneratorImpl();
            discProblem = SimflownyUtils.xmlToJSON(CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/discProblem/wave.xml"), 0), false);
            cg.generateSAMRAICode(discProblem);
            Thread.sleep(15000);
            String codePath = "codeFiles" + File.separator + "Wave equation marriage" + File.separator + "Toni Arbona" + File.separator + "RK3WENO3" 
                + File.separator + "simPlatSAMRAI" + File.separator;
            File f = new File(codePath);
            ArrayList<String> names = new ArrayList<String>(Arrays.asList(f.list()));
            codePath = codePath + File.separator + names.get(0);
            String comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/Functions.cpp"), 0);
            String result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Functions.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/Functions.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Functions.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/MainRestartData.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/MainRestartData.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/MainRestartData.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/MainRestartData.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/Problem.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Problem.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/Problem.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Problem.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/SAMRAIConnector.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/SAMRAIConnector.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/TimeInterpolator.C"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/TimeInterpolator.C"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/problem.input"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/problem.input"), 0);
            assertEquals(comp, result);
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Schema Validation Test RK3WENO3. Error generating code: " + e.getMessage());
        }
    }
    
    @Test
    public void testSchemasRK3WENO5Z() {
        String basePath = "../../../schema validation tests/RK3 WENO5Z";
        CodeGenerator cg;
        String discProblem = null;
        
        try {
            cg = new CodeGeneratorImpl();
            discProblem = SimflownyUtils.xmlToJSON(CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/discProblem/wave.xml"), 0), false);
            cg.generateSAMRAICode(discProblem);
            Thread.sleep(15000);
            String codePath = "codeFiles" + File.separator + "Wave equation marriage" + File.separator + "Toni Arbona" + File.separator + "RK3WENO5Z" 
                + File.separator + "simPlatSAMRAI" + File.separator;
            File f = new File(codePath);
            ArrayList<String> names = new ArrayList<String>(Arrays.asList(f.list()));
            codePath = codePath + File.separator + names.get(0);
            String comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/Functions.cpp"), 0);
            String result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Functions.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/Functions.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Functions.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/MainRestartData.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/MainRestartData.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/MainRestartData.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/MainRestartData.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/Problem.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Problem.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/Problem.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Problem.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/SAMRAIConnector.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/SAMRAIConnector.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/TimeInterpolator.C"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/TimeInterpolator.C"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/problem.input"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/problem.input"), 0);
            assertEquals(comp, result);
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Schema Validation Test RK3WENO5Z. Error generating code: " + e.getMessage());
        }
    }
    
    
    @Test
    public void testSchemasRK3WENO3YC() {
        String basePath = "../../../schema validation tests/RK3 WENO3YC";
        CodeGenerator cg;
        String discProblem = null;
        
        try {
            cg = new CodeGeneratorImpl();
            discProblem = SimflownyUtils.xmlToJSON(CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/discProblem/wave.xml"), 0), false);
            cg.generateSAMRAICode(discProblem);
            Thread.sleep(15000);
            String codePath = "codeFiles" + File.separator + "Wave equation marriage" + File.separator + "Toni Arbona" + File.separator + "RK3WENO3YC" 
                + File.separator + "simPlatSAMRAI" + File.separator;
            File f = new File(codePath);
            ArrayList<String> names = new ArrayList<String>(Arrays.asList(f.list()));
            codePath = codePath + File.separator + names.get(0);
            String comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/Functions.cpp"), 0);
            String result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Functions.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/Functions.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Functions.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/MainRestartData.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/MainRestartData.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/MainRestartData.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/MainRestartData.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/Problem.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Problem.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/Problem.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Problem.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/SAMRAIConnector.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/SAMRAIConnector.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/TimeInterpolator.C"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/TimeInterpolator.C"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/problem.input"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/problem.input"), 0);
            assertEquals(comp, result);
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Schema Validation Test RK3WENO3YC. Error generating code: " + e.getMessage());
        }
    }
    
    @Test
    public void testSchemasRK3CD() {
        String basePath = "../../../schema validation tests/RK3 CD4";
        CodeGenerator cg;
        String discProblem = null;
        
        try {
            cg = new CodeGeneratorImpl();
            discProblem = SimflownyUtils.xmlToJSON(CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/discProblem/wave.xml"), 0), false);
            cg.generateSAMRAICode(discProblem);
            Thread.sleep(15000);
            String codePath = "codeFiles" + File.separator + "Wave equation marriage" + File.separator + "Toni Arbona" + File.separator + "RK3CD4" 
                + File.separator + "simPlatSAMRAI" + File.separator;
            File f = new File(codePath);
            ArrayList<String> names = new ArrayList<String>(Arrays.asList(f.list()));
            codePath = codePath + File.separator + names.get(0);
            String comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/Functions.cpp"), 0);
            String result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Functions.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/Functions.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Functions.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/MainRestartData.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/MainRestartData.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/MainRestartData.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/MainRestartData.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/Problem.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Problem.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/Problem.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Problem.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/SAMRAIConnector.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/SAMRAIConnector.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/TimeInterpolator.C"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/TimeInterpolator.C"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/problem.input"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/problem.input"), 0);
            assertEquals(comp, result);
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Schema Validation Test RK3CD. Error generating code: " + e.getMessage());
        }
    }
    
    @Test
    public void testSchemasRK3FDOC3() {
        String basePath = "../../../schema validation tests/RK3 FDOC3";
        CodeGenerator cg;
        String discProblem = null;
        
        try {
            cg = new CodeGeneratorImpl();
            discProblem = SimflownyUtils.xmlToJSON(CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/discProblem/wave.xml"), 0), false);
            cg.generateSAMRAICode(discProblem);
            Thread.sleep(15000);
            String codePath = "codeFiles" + File.separator + "Wave equation marriage" + File.separator + "Toni Arbona" + File.separator + "RK3FDOC3" 
                + File.separator + "simPlatSAMRAI" + File.separator;
            File f = new File(codePath);
            ArrayList<String> names = new ArrayList<String>(Arrays.asList(f.list()));
            codePath = codePath + File.separator + names.get(0);
            String comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/Functions.cpp"), 0);
            String result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Functions.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/Functions.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Functions.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/MainRestartData.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/MainRestartData.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/MainRestartData.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/MainRestartData.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/Problem.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Problem.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/Problem.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Problem.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/SAMRAIConnector.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/SAMRAIConnector.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/TimeInterpolator.C"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/TimeInterpolator.C"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/problem.input"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/problem.input"), 0);
            assertEquals(comp, result);
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Schema Validation Test RK3FDOC3. Error generating code: " + e.getMessage());
        }
    }
    
    @Test
    public void testSchemasIMEX3CD() {
        String basePath = "../../../schema validation tests/IMEX3 CD4";
        CodeGenerator cg;
        String discProblem = null;
        
        try {
            cg = new CodeGeneratorImpl();
            discProblem = SimflownyUtils.xmlToJSON(CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/discProblem/wave.xml"), 0), false);
            cg.generateSAMRAICode(discProblem);
            Thread.sleep(15000);
            String codePath = "codeFiles" + File.separator + "Wave equation marriage" + File.separator + "Toni Arbona" + File.separator + "IMEX3CD4" 
                + File.separator + "simPlatSAMRAI" + File.separator;
            File f = new File(codePath);
            ArrayList<String> names = new ArrayList<String>(Arrays.asList(f.list()));
            codePath = codePath + File.separator + names.get(0);
            String comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/Functions.cpp"), 0);
            String result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Functions.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/Functions.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Functions.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/MainRestartData.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/MainRestartData.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/MainRestartData.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/MainRestartData.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/Problem.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Problem.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/Problem.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Problem.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/SAMRAIConnector.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/SAMRAIConnector.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/TimeInterpolator.C"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/TimeInterpolator.C"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/problem.input"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/problem.input"), 0);
            assertEquals(comp, result);
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Schema Validation Test IMEX3CD4. Error generating code: " + e.getMessage());
        }
    }
    
    @Test
    public void testSchemasIMEX3MP5() {
        String basePath = "../../../schema validation tests/IMEX3 MP5";
        CodeGenerator cg;
        String discProblem = null;
        
        try {
            cg = new CodeGeneratorImpl();
            discProblem = SimflownyUtils.xmlToJSON(CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/discProblem/wave.xml"), 0), false);
            cg.generateSAMRAICode(discProblem);
            Thread.sleep(15000);
            String codePath = "codeFiles" + File.separator + "Wave equation marriage" + File.separator + "Toni Arbona" + File.separator + "IMEX3MP5" 
                + File.separator + "simPlatSAMRAI" + File.separator;
            File f = new File(codePath);
            ArrayList<String> names = new ArrayList<String>(Arrays.asList(f.list()));
            codePath = codePath + File.separator + names.get(0);
            String comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/Functions.cpp"), 0);
            String result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Functions.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/Functions.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Functions.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/MainRestartData.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/MainRestartData.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/MainRestartData.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/MainRestartData.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/Problem.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Problem.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/Problem.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Problem.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/SAMRAIConnector.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/SAMRAIConnector.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/TimeInterpolator.C"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/TimeInterpolator.C"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/problem.input"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/problem.input"), 0);
            assertEquals(comp, result);
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Schema Validation Test IMEX3MP5. Error generating code: " + e.getMessage());
        }
    }
    
    @Test
    public void testSchemasIMEX3WENO5Z() {
        String basePath = "../../../schema validation tests/IMEX3 WENO5Z";
        CodeGenerator cg;
        String discProblem = null;
        
        try {
            cg = new CodeGeneratorImpl();
            discProblem = SimflownyUtils.xmlToJSON(CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/discProblem/wave.xml"), 0), false);
            cg.generateSAMRAICode(discProblem);
            Thread.sleep(15000);
            String codePath = "codeFiles" + File.separator + "Wave equation marriage" + File.separator + "Toni Arbona" + File.separator + "IMEX3WENO5Z" 
                + File.separator + "simPlatSAMRAI" + File.separator;
            File f = new File(codePath);
            ArrayList<String> names = new ArrayList<String>(Arrays.asList(f.list()));
            codePath = codePath + File.separator + names.get(0);
            String comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/Functions.cpp"), 0);
            String result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Functions.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/Functions.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Functions.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/MainRestartData.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/MainRestartData.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/MainRestartData.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/MainRestartData.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/Problem.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Problem.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/Problem.h"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/Problem.h"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/SAMRAIConnector.cpp"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/SAMRAIConnector.cpp"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/TimeInterpolator.C"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/TimeInterpolator.C"), 0);
            assertEquals(comp, result);
            comp = CodeGeneratorUtils.convertStreamToString(new FileInputStream(basePath + "/code/problem.input"), 0);
            result = CodeGeneratorUtils.convertStreamToString(new FileInputStream(codePath + "/problem.input"), 0);
            assertEquals(comp, result);
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Schema Validation Test IMEX3WENO5Z. Error generating code: " + e.getMessage());
        }
    }
    
    //@Test
    public void testManual() {
        CodeGenerator cg;
        String discProblem = null;
        
        try {
            System.out.println("Manual");
            
            
            cg = new CodeGeneratorImpl();
            discProblem = SimflownyUtils.xmlToJSON(CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream(
              		"/testMarriage.xml"), 0), false);

            cg.generateSAMRAICode(discProblem);
            
            
            System.out.println("Fin manual");
            Thread.sleep(15000);
            
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Test 1. Error generating manual code: " + e.getMessage());
        }
    }

    /*public void testFind() {
        CodeGenerator cg;
        String discProblem = null;
        DocumentManagerImpl docMan = null;
        
        try {
            System.out.println("Manual");
            
            discProblem = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream(
            		"/testMHDAlfven.xml"), 0);

            new SAMRAIUtils();
            SAMRAIUtils.selectXSLTransformer(ProblemType.pde);
            
            Document dom = CodeGeneratorUtils.stringToDom(discProblem);
            new CodeGeneratorUtils();
            FileWriter fichero = new FileWriter("salidaSAXON.xml");
            PrintWriter pw = new PrintWriter(fichero);
            
            NodeList resultado = CodeGeneratorUtils.find(dom, "//mt:math[(not(parent::sml:if) or parent::sml:then) and (not(parent::sml:while) or parent::sml:loop) " 
                    + "and not(parent::sml:return) and not(ancestor::mms:applyIf) and not(ancestor::" 
                    + "mms:finalizationCondition)]/mt:apply/mt:eq/following-sibling::*[1][not(descendant-or-self::sml:sharedVariable)]");
            for (int i = 0; i < resultado.getLength(); i++) {
            	//System.out.println(CodeGeneratorUtils.xmlVarToString(resultado.item(i), true, true));
            	 //  pw.write(CodeGeneratorUtils.xmlVarToString(resultado.item(i), true, true) + "\n");
            	  //pw.write(resultado.item(i).getTextContent() + "\n");
            	//System.out.println(CodeGeneratorUtils.domToString(resultado.item(i)));
            	pw.write(SAMRAIUtils.xmlTransform(resultado.item(i), null) + "\n");
            }
            fichero.close();
            System.out.println(resultado.getLength());
            
            System.out.println("Fin manual");
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Test 1. Error generating manual code: " + e.getMessage());
        }
    }*/
    
   /* public void testFind3() {
        CodeGenerator cg;
        String discProblem = null;
        DocumentManagerImpl docMan = null;
        
        try {
        	new SAMRAIUtils();
            SAMRAIUtils.selectXSLTransformer(ProblemType.pde);
            System.out.println("Manual");
            
            discProblem = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream(
            		"/testManual.xml"), 0);
            Document dom = CodeGeneratorUtils.stringToDom(discProblem);

            new CodeGeneratorUtils();
            FileWriter fichero = new FileWriter("salidaSAXON3.xml");
            PrintWriter pw = new PrintWriter(fichero);

            Processor processor = new Processor(false);
            net.sf.saxon.s9api.DocumentBuilder db = processor.newDocumentBuilder();
            XdmNode xdm = db.wrap(dom);
            XPathCompiler xpath = processor.newXPathCompiler();
            xpath.declareNamespace("mt", "http://www.w3.org/1998/Math/MathML"); // not actually used, just
            xpath.declareNamespace("mms", "urn:mathms"); // not actually used, just
            xpath.declareNamespace("sml", "urn:simml"); // not actually used, just
            / *XdmValue resultado = xpath.evaluate("//mt:math[(not(parent::sml:if) or parent::sml:then) and (not(parent::sml:while) or parent::sml:loop) " 
                    + "and not(parent::sml:return) and not(ancestor::mms:applyIf) and not(ancestor::" 
                    + "mms:finalizationCondition)]/mt:apply/mt:eq/following-sibling::*[1][not(descendant-or-self::sml:sharedVariable)]", xdm);
* /
            XdmValue resultado = xpath.evaluate("/* /*", xdm);

            
            for (int i = 0; i < resultado.size(); i++) {
            	//System.out.println(CodeGeneratorUtils.xmlVarToString(resultado.item(i), true, true));
            	 //  pw.write(transform(sw, trans, processor, (XdmNode) resultado.itemAt(i)) + "\n");
            	  // System.out.println(i);
            	//System.out.println(CodeGeneratorUtils.domToString((Node) ((XdmNode) resultado.itemAt(i)).getExternalNode()));
            	//System.out.println(((XdmNode) resultado.itemAt(i)).toString());
            	Node a = (Node) ((XdmNode) resultado.itemAt(i)).getExternalNode();
            	System.out.println(CodeGeneratorUtils.domToString(a));

            	//  	pw.write(SAMRAIUtils.xmlTransform(((XdmNode) resultado.itemAt(i)).toString(), null) + "\n");
            	Element parent = (Element) a.getParentNode();
            	System.out.println(CodeGeneratorUtils.domToString(parent));
               	parent.removeChild(a);
            	System.out.println(CodeGeneratorUtils.domToString(parent));
            	pw.write(SAMRAIUtils.xmlTransform(parent, null) + "\n");
            	
            	
            }
            
      resultado = xpath.evaluate("/* /*", xdm);

            
            for (int i = 0; i < resultado.size(); i++) {
            	//System.out.println(CodeGeneratorUtils.xmlVarToString(resultado.item(i), true, true));
            	 //  pw.write(transform(sw, trans, processor, (XdmNode) resultado.itemAt(i)) + "\n");
            	  // System.out.println(i);
            	//System.out.println(CodeGeneratorUtils.domToString((Node) ((XdmNode) resultado.itemAt(i)).getExternalNode()));
            	//System.out.println(((XdmNode) resultado.itemAt(i)).toString());
            	Node a = CodeGeneratorUtils.stringToDom(((XdmNode) resultado.itemAt(i)).toString());
            	//  	pw.write(SAMRAIUtils.xmlTransform(((XdmNode) resultado.itemAt(i)).toString(), null) + "\n");
            	pw.write(SAMRAIUtils.xmlTransform(a, null) + "\n");
            	
            	
            }
            
            fichero.close();
            System.out.println(resultado.size());
            
            System.out.println("Fin manual");
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Test 1. Error generating manual code: " + e.getMessage());
        }
    }
*/
   /* public void testManual() {
        CodeGenerator cg;
        String discProblem = null;
        try {
            System.out.println("Manual");
            ModelManagerImpl mm = new ModelManagerImpl(bundleContext);
            ModelManagerImpl.createCollection(CollectionType.problemSegment);
            String ptSoil = mm.addModel(CodeGeneratorUtils.convertStreamToString(getClass().
                    getResourceAsStream("/cubo1-1.x3d"), 0), CollectionType.problemSegment);
            String ptSoil2 = mm.addModel(CodeGeneratorUtils.convertStreamToString(getClass().
                    getResourceAsStream("/cubo1-2.x3d"), 0), CollectionType.problemSegment);
            String ptSoil3 = mm.addModel(CodeGeneratorUtils.convertStreamToString(getClass().
                    getResourceAsStream("/cubo2-1.x3d"), 0), CollectionType.problemSegment);
            String ptSoil4 = mm.addModel(CodeGeneratorUtils.convertStreamToString(getClass().
                    getResourceAsStream("/cubo2-2.x3d"), 0), CollectionType.problemSegment);

            cg = new CodeGeneratorImpl();
            discProblem = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/Movement-Multiple.xml"), 0);
            discProblem = discProblem.replaceAll("cubo1-1", ptSoil);
            discProblem = discProblem.replaceAll("cubo1-2", ptSoil2);
            discProblem = discProblem.replaceAll("cubo2-1", ptSoil3);
            discProblem = discProblem.replaceAll("cubo2-2", ptSoil4);
            
        //    discProblem = discProblem.replaceAll("Bag2", ptSoil2);
          //  discProblem = discProblem.replaceAll("0100007f70efc0f6000001461388ac28", ptSoil3);
          //  discProblem = discProblem.replaceAll("0100007f70efc0f6000001439a14cece", ptSoil4);
        //    cg.generateCode("Manual", "Manual", PlatformType.cactus, discProblem);
            cg.generateCode("Manual", "Manual", PlatformType.simPlatSAMRAI, discProblem);
        }
        catch (Exception e) {
            e.printStackTrace();
            fail("Test 1. Error generating manual code: " + e.getMessage());
        }
    }*/
    
    
}
