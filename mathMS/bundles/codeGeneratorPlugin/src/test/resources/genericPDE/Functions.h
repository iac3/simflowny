#include "boost/shared_ptr.hpp"
#include "SAMRAI/tbox/Utilities.h"
#include <string>
#include <math.h>
#include <vector>


#define MIN(X,Y) ((X) < (Y) ? (X) : (Y))
#define MAX(X,Y) ((X) > (Y) ? (X) : (Y))
#define SIGN(X) (((X) > 0) - ((X) < 0))
#define greaterThan(a,b) (!((fabs((a) - (b))/1.0E-9 > 10 ? false: (floor(fabs((a) - (b))/1.0E-9) < 1)) || (a)<(b)))
#define lessThan(a,b) (!((fabs((a) - (b))/1.0E-9 > 10 ? false : (floor(fabs((a) - (b))/1.0E-9) < 1)) || (b)<(a)))
#define greaterEq(a,b) ((fabs((a) - (b))/1.0E-9 > 10 ? false : (floor(fabs((a) - (b))/1.0E-9) < 1)) || (b)<(a))
#define lessEq(a,b) ((fabs((a) - (b))/1.0E-9 > 10 ? false: (floor(fabs((a) - (b))/1.0E-9) < 1)) || (a)<(b))
#define equalsEq(a,b) ((fabs((a) - (b))/1.0E-9 > 10 ? false: (floor(fabs((a) - (b))/1.0E-9) < 1)))

double readFromFile1D(std::vector<double> position1, std::vector<double> data, double coord1);
double readFromFile2D(std::vector<double> position1, std::vector<double> position2, std::vector<std::vector<double> > data, double coord1, double coord2);
double readFromFile3D(std::vector<double> position1, std::vector<double> position2, std::vector<double> position3, std::vector<std::vector<std::vector<double> > > data, double coord1, double coord2, double coord3);

#define vector(v, i, j) (v)[i+ilast*(j)]



