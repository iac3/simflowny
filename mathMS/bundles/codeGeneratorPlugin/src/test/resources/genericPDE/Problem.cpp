#include "Problem.h"
#include "Functions.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <stack>
#include "hdf5.h"
#include <gsl/gsl_rng.h>
#include "SAMRAI/pdat/NodeData.h"
#include "SAMRAI/pdat/NodeVariable.h"
#include "SAMRAI/pdat/CellVariable.h"
#include "LagrangianPolynomicRefine.h"


#include "SAMRAI/tbox/Array.h"
#include "SAMRAI/hier/BoundaryBox.h"
#include "SAMRAI/hier/BoxContainer.h"
#include "SAMRAI/geom/CartesianPatchGeometry.h"
#include "SAMRAI/hier/VariableDatabase.h"
#include "SAMRAI/hier/PatchDataRestartManager.h"
#include "SAMRAI/hier/Index.h"
#include "SAMRAI/tbox/PIO.h"
#include "SAMRAI/tbox/Utilities.h"
#include "SAMRAI/tbox/Timer.h"
#include "SAMRAI/tbox/TimerManager.h"

#define MIN(X,Y) ((X) < (Y) ? (X) : (Y))
#define MAX(X,Y) ((X) > (Y) ? (X) : (Y))
#define SIGN(X) (((X) > 0) - ((X) < 0))
#define isEven(a) ((a) % 2 == 0 ? true : false)
#define greaterThan(a,b) (!((fabs((a) - (b))/1.0E-9 > 10 ? false: (floor(fabs((a) - (b))/1.0E-9) < 1)) || (a)<(b)))
#define lessThan(a,b) (!((fabs((a) - (b))/1.0E-9 > 10 ? false : (floor(fabs((a) - (b))/1.0E-9) < 1)) || (b)<(a)))
#define greaterEq(a,b) ((fabs((a) - (b))/1.0E-9 > 10 ? false : (floor(fabs((a) - (b))/1.0E-9) < 1)) || (b)<(a))
#define lessEq(a,b) ((fabs((a) - (b))/1.0E-9 > 10 ? false: (floor(fabs((a) - (b))/1.0E-9) < 1)) || (a)<(b))
#define equalsEq(a,b) ((fabs((a) - (b))/1.0E-9 > 10 ? false: (floor(fabs((a) - (b))/1.0E-9) < 1)))

#define vector(v, i, j) (v)[i+ilast*(j)]
#define vectorT(v, i, j) (v)[i+itlast*(j)]
#define indexiOf(i) (i - ((int)(i/(boxlast(0)-boxfirst(0)+1))*(boxlast(0)-boxfirst(0)+1)))
#define indexjOf(i) ((int)(i/(boxlast(0)-boxfirst(0)+1)))



#define SpatialDisc1_i(parflux, pari, parj, dx, simPlat_dt, ilast, jlast) ((vector(parflux, (pari) + 1, (parj)) - vector(parflux, (pari) - 1, (parj))) / (2.0 * dx[0]))

#define SpatialDisc1_j(parflux, pari, parj, dx, simPlat_dt, ilast, jlast) ((vector(parflux, (pari), (parj) + 1) - vector(parflux, (pari), (parj) - 1)) / (2.0 * dx[1]))

#define SpatialDisc2_i(paru, pari, parj, dx, simPlat_dt, ilast, jlast) ((((-vector(paru, (pari) + 2, (parj))) + 16.0 * vector(paru, (pari) + 1, (parj))) - 30.0 * vector(paru, (pari), (parj)) + 16.0 * vector(paru, (pari) - 1, (parj)) - vector(paru, (pari) - 2, (parj))) / (12.0 * (dx[0] * dx[0])))

#define SpatialDisc2_j(paru, pari, parj, dx, simPlat_dt, ilast, jlast) ((((-vector(paru, (pari), (parj) + 2)) + 16.0 * vector(paru, (pari), (parj) + 1)) - 30.0 * vector(paru, (pari), (parj)) + 16.0 * vector(paru, (pari), (parj) - 1) - vector(paru, (pari), (parj) - 2)) / (12.0 * (dx[1] * dx[1])))

#define timeDisc3_(parsources, parQ1, parQ2, pari, parj, dx, simPlat_dt, ilast, jlast) (vector(parQ1, (pari), (parj)) + simPlat_dt * (parsources))

#define timeDisc4_(parsources, parQ1, parQ2, pari, parj, dx, simPlat_dt, ilast, jlast) (1.0 / 4.0 * (3.0 * vector(parQ1, (pari), (parj)) + vector(parQ2, (pari), (parj)) + simPlat_dt * (parsources)))

#define timeDisc5_(parsources, parQ1, parQ2, pari, parj, dx, simPlat_dt, ilast, jlast) (1.0 / 3.0 * (vector(parQ1, (pari), (parj)) + 2.0 * vector(parQ2, (pari), (parj)) + 2.0 * simPlat_dt * (parsources)))


const gsl_rng_type * T;
gsl_rng *r_var;

//Timers
std::shared_ptr<tbox::Timer> t_step;
std::shared_ptr<tbox::Timer> t_moveParticles;

inline int Problem::GetExpoBase2(double d)
{
	int i = 0;
	((short *)(&i))[0] = (((short *)(&d))[3] & (short)32752); // _123456789ab____ & 0111111111110000
	return (i >> 4) - 1023;
}

bool	Problem::Equals(double d1, double d2)
{
	if (d1 == d2)
		return true;
	int e1 = GetExpoBase2(d1);
	int e2 = GetExpoBase2(d2);
	int e3 = GetExpoBase2(d1 - d2);
	if ((e3 - e2 < -48) && (e3 - e1 < -48))
		return true;
	return false;
}

/*
 * Constructor of the problem.
 */
Problem::Problem(const string& object_name, const tbox::Dimension& dim, std::shared_ptr<tbox::Database>& database, std::shared_ptr<geom::CartesianGridGeometry >& grid_geom, std::shared_ptr<hier::PatchHierarchy >& patch_hierarchy, const double dt, hier::BoxContainer& ba, const bool init_from_files, const vector<string> full_writer_variables, const vector<vector<string> > integralVariables): 
d_dim(dim), xfer::RefinePatchStrategy(), xfer::CoarsenPatchStrategy(), d_regridding_boxes(ba), d_full_writer_variables(full_writer_variables.begin(), full_writer_variables.end())
{
	//Setup the timers
	t_step = tbox::TimerManager::getManager()->getTimer("Step");
	t_moveParticles = tbox::TimerManager::getManager()->getTimer("Move particles");

	//Get the object name, the grid geometry and the patch hierarchy
  	d_grid_geometry = grid_geom;
	d_patch_hierarchy = patch_hierarchy;
	d_object_name = object_name;
	d_init_from_files = init_from_files;
	initial_dt = dt;


	for (vector<vector<string> >::const_iterator it = integralVariables.begin(); it != integralVariables.end(); ++it) {
		vector<string> vars = *it;
		for (vector<string>::const_iterator it2 = vars.begin() ; it2 != vars.end(); ++it2) {
			d_integralVariables.push_back(vars);
		}
	}


	//Get parameters
	gauge_evol = database->getDouble("gauge_evol");
	b = database->getDouble("b");
	agauss = database->getDouble("agauss");
	a = database->getDouble("a");
	tend = database->getDouble("tend");
	m = database->getDouble("m");
	bgauss = database->getDouble("bgauss");
	//Random initialization
	gsl_rng_env_setup();
	//Random for simulation
	const tbox::SAMRAI_MPI& mpi(tbox::SAMRAI_MPI::getSAMRAIWorld());
	int random_seed = database->getDouble("random_seed")*(mpi.getRank() + 1);
	r_var = gsl_rng_alloc(gsl_rng_ranlxs0);
	gsl_rng_set(r_var, random_seed);


    //Subcycling
	d_refinedTimeStepping = false;
	d_tappering = false;
	if (database->isString("subcycling")) {
		if (database->getString("subcycling") == "TAPPERING") {
			d_tappering = true;
			d_refinedTimeStepping = true;
		}
		if (database->getString("subcycling") == "BERGER-OLIGER") {
			d_tappering = false;
			d_refinedTimeStepping = true;
		}
	}


	//Regridding options
	d_regridding = false;
	if (database->isDatabase("regridding")) {
		regridding_db = database->getDatabase("regridding");
		if (regridding_db->isString("regridding_type")) {
			d_regridding_type = regridding_db->getString("regridding_type");
			d_regridding_level_threshold = regridding_db->getInteger("regridding_level_threshold");
			if (d_regridding_type == "GRADIENT") {
				d_regridding_field = regridding_db->getString("regridding_field");
				d_regridding_compressionFactor = regridding_db->getDouble("regridding_compressionFactor");
				d_regridding_mOffset = regridding_db->getDouble("regridding_mOffset");
				d_regridding = true;
			} else {
				if (d_regridding_type == "FUNCTION") {
					d_regridding_field = regridding_db->getString("regridding_function_field");
					d_regridding_threshold = regridding_db->getDouble("regridding_threshold");
					d_regridding = true;
				}
			}
		}
	}

	//Stencil of the discretization method
	int maxratio = 1;
	for (int il = 1; il < d_patch_hierarchy->getMaxNumberOfLevels(); il++) {
		const hier::IntVector ratio = d_patch_hierarchy->getRatioToCoarserLevel(il);
		maxratio = MAX(maxratio, ratio.max());
	}
	//Minimum region thickness
	d_regionMinThickness = 3;
	// number of ghosts stencil * (#substeps) * #parentlevelsteps
	d_ghost_width = 3 * (1 + 2 * d_tappering) * (1 + d_tappering * (maxratio - 1));

	
	//Register Fields and temporal fields into the variable database
	hier::VariableDatabase *vdb = hier::VariableDatabase::getDatabase();
  	std::shared_ptr<hier::VariableContext> d_cont_curr(vdb->getContext("Current"));
	std::shared_ptr< pdat::CellVariable<int> > mask(std::shared_ptr< pdat::CellVariable<int> >(new pdat::CellVariable<int>(d_dim, "samrai_mask",1)));
	d_mask_id = vdb->registerVariableAndContext(mask ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	IntegrateDataWriter::setMaskVariable(d_mask_id);
	std::shared_ptr< pdat::NodeVariable<double> > interior(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "interior",1)));
	d_interior_id = vdb->registerVariableAndContext(interior ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_interior_id);
	std::shared_ptr< pdat::NodeVariable<int> > nonSync(std::shared_ptr< pdat::NodeVariable<int> >(new pdat::NodeVariable<int>(d_dim, "nonSync",1)));
	d_nonSync_id = vdb->registerVariableAndContext(nonSync ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_nonSync_id);
	std::shared_ptr< pdat::NodeVariable<double> > interior_i(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "interior_i",1)));
	d_interior_i_id = vdb->registerVariableAndContext(interior_i ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_interior_i_id);
	std::shared_ptr< pdat::NodeVariable<double> > interior_j(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "interior_j",1)));
	d_interior_j_id = vdb->registerVariableAndContext(interior_j ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_interior_j_id);
	std::shared_ptr< pdat::NodeVariable<double> > FOV_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FOV_1",1)));
	d_FOV_1_id = vdb->registerVariableAndContext(FOV_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FOV_1_id);
	std::shared_ptr< pdat::NodeVariable<double> > FOV_xLower(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FOV_xLower",1)));
	d_FOV_xLower_id = vdb->registerVariableAndContext(FOV_xLower ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FOV_xLower_id);
	std::shared_ptr< pdat::NodeVariable<double> > FOV_xUpper(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FOV_xUpper",1)));
	d_FOV_xUpper_id = vdb->registerVariableAndContext(FOV_xUpper ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FOV_xUpper_id);
	std::shared_ptr< pdat::NodeVariable<double> > FOV_yLower(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FOV_yLower",1)));
	d_FOV_yLower_id = vdb->registerVariableAndContext(FOV_yLower ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FOV_yLower_id);
	std::shared_ptr< pdat::NodeVariable<double> > FOV_yUpper(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FOV_yUpper",1)));
	d_FOV_yUpper_id = vdb->registerVariableAndContext(FOV_yUpper ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FOV_yUpper_id);
	std::shared_ptr< pdat::NodeVariable<double> > alpha(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "alpha",1)));
	d_alpha_id = vdb->registerVariableAndContext(alpha ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_alpha_id);
	std::shared_ptr< pdat::NodeVariable<double> > alpha_p(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "alpha_p",1)));
	d_alpha_p_id = vdb->registerVariableAndContext(alpha_p ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_alpha_p_id);
	std::shared_ptr< pdat::NodeVariable<double> > beta_x(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "beta_x",1)));
	d_beta_x_id = vdb->registerVariableAndContext(beta_x ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_beta_x_id);
	std::shared_ptr< pdat::NodeVariable<double> > beta_x_p(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "beta_x_p",1)));
	d_beta_x_p_id = vdb->registerVariableAndContext(beta_x_p ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_beta_x_p_id);
	std::shared_ptr< pdat::NodeVariable<double> > beta_y(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "beta_y",1)));
	d_beta_y_id = vdb->registerVariableAndContext(beta_y ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_beta_y_id);
	std::shared_ptr< pdat::NodeVariable<double> > beta_y_p(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "beta_y_p",1)));
	d_beta_y_p_id = vdb->registerVariableAndContext(beta_y_p ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_beta_y_p_id);
	std::shared_ptr< pdat::NodeVariable<double> > phi(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "phi",1)));
	d_phi_id = vdb->registerVariableAndContext(phi ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_phi_id);
	std::shared_ptr< pdat::NodeVariable<double> > phi_p(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "phi_p",1)));
	d_phi_p_id = vdb->registerVariableAndContext(phi_p ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_phi_p_id);
	std::shared_ptr< pdat::NodeVariable<double> > K(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "K",1)));
	d_K_id = vdb->registerVariableAndContext(K ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_K_id);
	std::shared_ptr< pdat::NodeVariable<double> > K_p(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "K_p",1)));
	d_K_p_id = vdb->registerVariableAndContext(K_p ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_K_p_id);
	std::shared_ptr< pdat::NodeVariable<double> > ad_K_o0_t3_m0_l0_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "ad_K_o0_t3_m0_l0_1",1)));
	d_ad_K_o0_t3_m0_l0_1_id = vdb->registerVariableAndContext(ad_K_o0_t3_m0_l0_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_ad_K_o0_t3_m0_l0_1_id);
	std::shared_ptr< pdat::NodeVariable<double> > d_K_o0_t3_m0_l0_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "d_K_o0_t3_m0_l0_1",1)));
	d_d_K_o0_t3_m0_l0_1_id = vdb->registerVariableAndContext(d_K_o0_t3_m0_l0_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_d_K_o0_t3_m0_l0_1_id);
	std::shared_ptr< pdat::NodeVariable<double> > ad_beta_x_o0_t0_m0_l0_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "ad_beta_x_o0_t0_m0_l0_1",1)));
	d_ad_beta_x_o0_t0_m0_l0_1_id = vdb->registerVariableAndContext(ad_beta_x_o0_t0_m0_l0_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_ad_beta_x_o0_t0_m0_l0_1_id);
	std::shared_ptr< pdat::NodeVariable<double> > ad_beta_x_o0_t1_m0_l0_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "ad_beta_x_o0_t1_m0_l0_1",1)));
	d_ad_beta_x_o0_t1_m0_l0_1_id = vdb->registerVariableAndContext(ad_beta_x_o0_t1_m0_l0_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_ad_beta_x_o0_t1_m0_l0_1_id);
	std::shared_ptr< pdat::NodeVariable<double> > ad_beta_y_o0_t0_m0_l0_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "ad_beta_y_o0_t0_m0_l0_1",1)));
	d_ad_beta_y_o0_t0_m0_l0_1_id = vdb->registerVariableAndContext(ad_beta_y_o0_t0_m0_l0_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_ad_beta_y_o0_t0_m0_l0_1_id);
	std::shared_ptr< pdat::NodeVariable<double> > ad_beta_y_o0_t1_m0_l0_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "ad_beta_y_o0_t1_m0_l0_1",1)));
	d_ad_beta_y_o0_t1_m0_l0_1_id = vdb->registerVariableAndContext(ad_beta_y_o0_t1_m0_l0_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_ad_beta_y_o0_t1_m0_l0_1_id);
	std::shared_ptr< pdat::NodeVariable<double> > ad_phi_o0_t0_m0_l0_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "ad_phi_o0_t0_m0_l0_1",1)));
	d_ad_phi_o0_t0_m0_l0_1_id = vdb->registerVariableAndContext(ad_phi_o0_t0_m0_l0_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_ad_phi_o0_t0_m0_l0_1_id);
	std::shared_ptr< pdat::NodeVariable<double> > ad_phi_o0_t1_m0_l0_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "ad_phi_o0_t1_m0_l0_1",1)));
	d_ad_phi_o0_t1_m0_l0_1_id = vdb->registerVariableAndContext(ad_phi_o0_t1_m0_l0_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_ad_phi_o0_t1_m0_l0_1_id);
	std::shared_ptr< pdat::NodeVariable<double> > ad_K_o0_t0_m0_l0_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "ad_K_o0_t0_m0_l0_1",1)));
	d_ad_K_o0_t0_m0_l0_1_id = vdb->registerVariableAndContext(ad_K_o0_t0_m0_l0_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_ad_K_o0_t0_m0_l0_1_id);
	std::shared_ptr< pdat::NodeVariable<double> > ad_K_o0_t1_m0_l0_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "ad_K_o0_t1_m0_l0_1",1)));
	d_ad_K_o0_t1_m0_l0_1_id = vdb->registerVariableAndContext(ad_K_o0_t1_m0_l0_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_ad_K_o0_t1_m0_l0_1_id);
	std::shared_ptr< pdat::NodeVariable<double> > ad_K_o0_t2_m0_l0_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "ad_K_o0_t2_m0_l0_1",1)));
	d_ad_K_o0_t2_m0_l0_1_id = vdb->registerVariableAndContext(ad_K_o0_t2_m0_l0_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_ad_K_o0_t2_m0_l0_1_id);
	std::shared_ptr< pdat::NodeVariable<double> > ad_K_o0_t4_m0_l0_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "ad_K_o0_t4_m0_l0_1",1)));
	d_ad_K_o0_t4_m0_l0_1_id = vdb->registerVariableAndContext(ad_K_o0_t4_m0_l0_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_ad_K_o0_t4_m0_l0_1_id);
	std::shared_ptr< pdat::NodeVariable<double> > ad_K_o0_t6_m0_l0_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "ad_K_o0_t6_m0_l0_1",1)));
	d_ad_K_o0_t6_m0_l0_1_id = vdb->registerVariableAndContext(ad_K_o0_t6_m0_l0_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_ad_K_o0_t6_m0_l0_1_id);
	std::shared_ptr< pdat::NodeVariable<double> > ad_K_o0_t6_m1_l0_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "ad_K_o0_t6_m1_l0_1",1)));
	d_ad_K_o0_t6_m1_l0_1_id = vdb->registerVariableAndContext(ad_K_o0_t6_m1_l0_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_ad_K_o0_t6_m1_l0_1_id);
	std::shared_ptr< pdat::NodeVariable<double> > ad_K_o0_t7_m0_l0_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "ad_K_o0_t7_m0_l0_1",1)));
	d_ad_K_o0_t7_m0_l0_1_id = vdb->registerVariableAndContext(ad_K_o0_t7_m0_l0_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_ad_K_o0_t7_m0_l0_1_id);
	std::shared_ptr< pdat::NodeVariable<double> > ad_K_o0_t7_m1_l0_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "ad_K_o0_t7_m1_l0_1",1)));
	d_ad_K_o0_t7_m1_l0_1_id = vdb->registerVariableAndContext(ad_K_o0_t7_m1_l0_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_ad_K_o0_t7_m1_l0_1_id);
	std::shared_ptr< pdat::NodeVariable<double> > ad_K_o0_t8_m0_l0_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "ad_K_o0_t8_m0_l0_1",1)));
	d_ad_K_o0_t8_m0_l0_1_id = vdb->registerVariableAndContext(ad_K_o0_t8_m0_l0_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_ad_K_o0_t8_m0_l0_1_id);
	std::shared_ptr< pdat::NodeVariable<double> > ad_K_o0_t8_m1_l0_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "ad_K_o0_t8_m1_l0_1",1)));
	d_ad_K_o0_t8_m1_l0_1_id = vdb->registerVariableAndContext(ad_K_o0_t8_m1_l0_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_ad_K_o0_t8_m1_l0_1_id);
	std::shared_ptr< pdat::NodeVariable<double> > ad_K_o0_t9_m0_l0_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "ad_K_o0_t9_m0_l0_1",1)));
	d_ad_K_o0_t9_m0_l0_1_id = vdb->registerVariableAndContext(ad_K_o0_t9_m0_l0_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_ad_K_o0_t9_m0_l0_1_id);
	std::shared_ptr< pdat::NodeVariable<double> > ad_K_o0_t9_m1_l0_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "ad_K_o0_t9_m1_l0_1",1)));
	d_ad_K_o0_t9_m1_l0_1_id = vdb->registerVariableAndContext(ad_K_o0_t9_m1_l0_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_ad_K_o0_t9_m1_l0_1_id);
	std::shared_ptr< pdat::NodeVariable<double> > k1alpha(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "k1alpha",1)));
	d_k1alpha_id = vdb->registerVariableAndContext(k1alpha ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_k1alpha_id);
	std::shared_ptr< pdat::NodeVariable<double> > k1beta_x(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "k1beta_x",1)));
	d_k1beta_x_id = vdb->registerVariableAndContext(k1beta_x ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_k1beta_x_id);
	std::shared_ptr< pdat::NodeVariable<double> > k1beta_y(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "k1beta_y",1)));
	d_k1beta_y_id = vdb->registerVariableAndContext(k1beta_y ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_k1beta_y_id);
	std::shared_ptr< pdat::NodeVariable<double> > k1phi(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "k1phi",1)));
	d_k1phi_id = vdb->registerVariableAndContext(k1phi ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_k1phi_id);
	std::shared_ptr< pdat::NodeVariable<double> > k1K(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "k1K",1)));
	d_k1K_id = vdb->registerVariableAndContext(k1K ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_k1K_id);
	std::shared_ptr< pdat::NodeVariable<double> > k2alpha(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "k2alpha",1)));
	d_k2alpha_id = vdb->registerVariableAndContext(k2alpha ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_k2alpha_id);
	std::shared_ptr< pdat::NodeVariable<double> > k2beta_x(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "k2beta_x",1)));
	d_k2beta_x_id = vdb->registerVariableAndContext(k2beta_x ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_k2beta_x_id);
	std::shared_ptr< pdat::NodeVariable<double> > k2beta_y(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "k2beta_y",1)));
	d_k2beta_y_id = vdb->registerVariableAndContext(k2beta_y ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_k2beta_y_id);
	std::shared_ptr< pdat::NodeVariable<double> > k2phi(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "k2phi",1)));
	d_k2phi_id = vdb->registerVariableAndContext(k2phi ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_k2phi_id);
	std::shared_ptr< pdat::NodeVariable<double> > k2K(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "k2K",1)));
	d_k2K_id = vdb->registerVariableAndContext(k2K ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_k2K_id);


	//Refine and coarse algorithms

	d_bdry_fill_init = std::shared_ptr< xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());
	d_coarsen_algorithm = std::shared_ptr< xfer::CoarsenAlgorithm >(new xfer::CoarsenAlgorithm(d_dim));

	d_mapping_fill = std::shared_ptr< xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());
	d_fill_new_level    = std::shared_ptr< xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());

	//mapping communication

	std::shared_ptr< hier::RefineOperator > refine_operator_map = d_grid_geometry->lookupRefineOperator(interior, "LINEAR_REFINE");
	d_mapping_fill->registerRefine(d_interior_id,d_interior_id,d_interior_id, refine_operator_map);
	d_mapping_fill->registerRefine(d_interior_i_id,d_interior_i_id,d_interior_i_id, refine_operator_map);
	d_mapping_fill->registerRefine(d_interior_j_id,d_interior_j_id,d_interior_j_id, refine_operator_map);
	d_mapping_fill->registerRefine(d_FOV_1_id,d_FOV_1_id,d_FOV_1_id, refine_operator_map);
	d_mapping_fill->registerRefine(d_FOV_xLower_id,d_FOV_xLower_id,d_FOV_xLower_id, refine_operator_map);
	d_mapping_fill->registerRefine(d_FOV_xUpper_id,d_FOV_xUpper_id,d_FOV_xUpper_id, refine_operator_map);
	d_mapping_fill->registerRefine(d_FOV_yLower_id,d_FOV_yLower_id,d_FOV_yLower_id, refine_operator_map);
	d_mapping_fill->registerRefine(d_FOV_yUpper_id,d_FOV_yUpper_id,d_FOV_yUpper_id, refine_operator_map);


	//refine and coarsen operators
	string refine_op_name = "LINEAR_REFINE";
	int order = 0;
	if (database->isDatabase("regridding")) {
		regridding_db = database->getDatabase("regridding");
		if (regridding_db->isString("interpolator")) {
			refine_op_name = regridding_db->getString("interpolator");
			if (refine_op_name == "LINEAR_REFINE") {
				order = 1;
			}
			if (refine_op_name == "CUBIC_REFINE") {
				order = 3;
			}
			if (refine_op_name == "QUINTIC_REFINE") {
				order = 5;
			}
		}
	}
	std::shared_ptr< hier::RefineOperator > refine_operator, refine_operator_bound;
	std::shared_ptr< hier::CoarsenOperator > coarsen_operator = d_grid_geometry->lookupCoarsenOperator(alpha, "CONSTANT_COARSEN");
	if (order > 0) {
		std::shared_ptr< hier::RefineOperator > tmp_refine_operator(new LagrangianPolynomicRefine(false, order, d_dim));
		refine_operator = tmp_refine_operator;
		std::shared_ptr< hier::RefineOperator > tmp_refine_operator_bound(new LagrangianPolynomicRefine(true, order, d_dim));
		refine_operator_bound = tmp_refine_operator_bound;
	} else {
		refine_operator = d_grid_geometry->lookupRefineOperator(alpha, refine_op_name);
		refine_operator_bound = d_grid_geometry->lookupRefineOperator(alpha, refine_op_name);
	}

	time_interpolate_operator = new TimeInterpolator();
	std::shared_ptr<SAMRAI::hier::TimeInterpolateOperator> tio(time_interpolate_operator);


	//Register variables to the refineAlgorithm for boundaries

	d_bdry_fill_init->registerRefine(d_alpha_id,d_alpha_id,d_alpha_id,refine_operator);
	d_bdry_fill_init->registerRefine(d_beta_x_id,d_beta_x_id,d_beta_x_id,refine_operator);
	d_bdry_fill_init->registerRefine(d_beta_y_id,d_beta_y_id,d_beta_y_id,refine_operator);
	d_bdry_fill_init->registerRefine(d_phi_id,d_phi_id,d_phi_id,refine_operator);
	d_bdry_fill_init->registerRefine(d_K_id,d_K_id,d_K_id,refine_operator);


	//Register variables to the refineAlgorithm for filling new levels on regridding
	d_fill_new_level->registerRefine(d_alpha_id,d_alpha_id,d_alpha_id,refine_operator_bound);
	d_fill_new_level->registerRefine(d_beta_x_id,d_beta_x_id,d_beta_x_id,refine_operator_bound);
	d_fill_new_level->registerRefine(d_beta_y_id,d_beta_y_id,d_beta_y_id,refine_operator_bound);
	d_fill_new_level->registerRefine(d_phi_id,d_phi_id,d_phi_id,refine_operator_bound);
	d_fill_new_level->registerRefine(d_K_id,d_K_id,d_K_id,refine_operator_bound);


	//Register variables to the coarsenAlgorithm
	d_coarsen_algorithm->registerCoarsen(d_alpha_id,d_alpha_id,coarsen_operator);
	d_coarsen_algorithm->registerCoarsen(d_beta_x_id,d_beta_x_id,coarsen_operator);
	d_coarsen_algorithm->registerCoarsen(d_beta_y_id,d_beta_y_id,coarsen_operator);
	d_coarsen_algorithm->registerCoarsen(d_phi_id,d_phi_id,coarsen_operator);
	d_coarsen_algorithm->registerCoarsen(d_K_id,d_K_id,coarsen_operator);


}

/*
 * Destructor.
 */
Problem::~Problem() 
{
} 

/*
 * Initialize the data from a given level.
 */
void Problem::initializeLevelData (
   const std::shared_ptr<hier::PatchHierarchy >& hierarchy , 
   const int level_number ,
   const double init_data_time ,
   const bool can_be_refined ,
   const bool initial_time ,
   const std::shared_ptr<hier::PatchLevel >& old_level ,
   const bool allocate_data )
{
	std::shared_ptr< hier::PatchLevel > level(hierarchy->getPatchLevel(level_number));

	// Allocate storage needed to initialize level and fill data from coarser levels in AMR hierarchy.  
	level->allocatePatchData(d_interior_id, init_data_time);
	level->allocatePatchData(d_nonSync_id, init_data_time);
	level->allocatePatchData(d_interior_i_id, init_data_time);
	level->allocatePatchData(d_interior_j_id, init_data_time);
	level->allocatePatchData(d_alpha_id, init_data_time);
	level->allocatePatchData(d_alpha_p_id, init_data_time);
	level->allocatePatchData(d_beta_x_id, init_data_time);
	level->allocatePatchData(d_beta_x_p_id, init_data_time);
	level->allocatePatchData(d_beta_y_id, init_data_time);
	level->allocatePatchData(d_beta_y_p_id, init_data_time);
	level->allocatePatchData(d_phi_id, init_data_time);
	level->allocatePatchData(d_phi_p_id, init_data_time);
	level->allocatePatchData(d_K_id, init_data_time);
	level->allocatePatchData(d_K_p_id, init_data_time);
	level->allocatePatchData(d_ad_K_o0_t3_m0_l0_1_id, init_data_time);
	level->allocatePatchData(d_d_K_o0_t3_m0_l0_1_id, init_data_time);
	level->allocatePatchData(d_ad_beta_x_o0_t0_m0_l0_1_id, init_data_time);
	level->allocatePatchData(d_ad_beta_x_o0_t1_m0_l0_1_id, init_data_time);
	level->allocatePatchData(d_ad_beta_y_o0_t0_m0_l0_1_id, init_data_time);
	level->allocatePatchData(d_ad_beta_y_o0_t1_m0_l0_1_id, init_data_time);
	level->allocatePatchData(d_ad_phi_o0_t0_m0_l0_1_id, init_data_time);
	level->allocatePatchData(d_ad_phi_o0_t1_m0_l0_1_id, init_data_time);
	level->allocatePatchData(d_ad_K_o0_t0_m0_l0_1_id, init_data_time);
	level->allocatePatchData(d_ad_K_o0_t1_m0_l0_1_id, init_data_time);
	level->allocatePatchData(d_ad_K_o0_t2_m0_l0_1_id, init_data_time);
	level->allocatePatchData(d_ad_K_o0_t4_m0_l0_1_id, init_data_time);
	level->allocatePatchData(d_ad_K_o0_t6_m0_l0_1_id, init_data_time);
	level->allocatePatchData(d_ad_K_o0_t6_m1_l0_1_id, init_data_time);
	level->allocatePatchData(d_ad_K_o0_t7_m0_l0_1_id, init_data_time);
	level->allocatePatchData(d_ad_K_o0_t7_m1_l0_1_id, init_data_time);
	level->allocatePatchData(d_ad_K_o0_t8_m0_l0_1_id, init_data_time);
	level->allocatePatchData(d_ad_K_o0_t8_m1_l0_1_id, init_data_time);
	level->allocatePatchData(d_ad_K_o0_t9_m0_l0_1_id, init_data_time);
	level->allocatePatchData(d_ad_K_o0_t9_m1_l0_1_id, init_data_time);
	level->allocatePatchData(d_k1alpha_id, init_data_time);
	level->allocatePatchData(d_k1beta_x_id, init_data_time);
	level->allocatePatchData(d_k1beta_y_id, init_data_time);
	level->allocatePatchData(d_k1phi_id, init_data_time);
	level->allocatePatchData(d_k1K_id, init_data_time);
	level->allocatePatchData(d_k2alpha_id, init_data_time);
	level->allocatePatchData(d_k2beta_x_id, init_data_time);
	level->allocatePatchData(d_k2beta_y_id, init_data_time);
	level->allocatePatchData(d_k2phi_id, init_data_time);
	level->allocatePatchData(d_k2K_id, init_data_time);
	level->allocatePatchData(d_FOV_1_id, init_data_time);
	level->allocatePatchData(d_FOV_xLower_id, init_data_time);
	level->allocatePatchData(d_FOV_xUpper_id, init_data_time);
	level->allocatePatchData(d_FOV_yLower_id, init_data_time);
	level->allocatePatchData(d_FOV_yUpper_id, init_data_time);
	level->allocatePatchData(d_mask_id, init_data_time);


	//Fill a finer level with the data of the next coarse level.
	if (!initial_time && ((level_number > 0) || old_level)) {
		d_fill_new_level->createSchedule(level,old_level,level_number-1,hierarchy,this)->fillData(init_data_time, false);
	}

	//Mapping the current data for new level.
	if (initial_time) {
		mapDataOnPatch(init_data_time, initial_time, level_number, level);
	}

	//Fill a finer level with the data of the next coarse level.
	if ((level_number > 0) || old_level) {
		d_mapping_fill->createSchedule(level,old_level,level_number-1,hierarchy,this)->fillData(init_data_time, false);
		correctFOVS(level);
	}

	//Interphase mapping
	if (initial_time) {
		interphaseMapping(init_data_time, initial_time, level_number, level, 1);
	}


	//Initialize current data for new level.
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr< hier::Patch > patch = *p_it;
        		if (initial_time) {
	      		initializeDataOnPatch(*patch, init_data_time, initial_time);
        		}
	}
	//Post-initialization Sync.
    	if (initial_time) {

		double current_time = init_data_time;
		for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
			const std::shared_ptr< hier::Patch >& patch = *p_it;
			double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
			double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
			double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
			double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
			double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();

			//Get the dimensions of the patch
			const hier::Index boxfirst = patch->getBox().lower();
			const hier::Index boxlast = patch->getBox().upper();

			//Get delta spaces into an array. dx, dy, dz.
			const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
			const double* dx  = patch_geom->getDx();
			int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
			int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
			for (int j = 0; j < jlast; j++) {
				for (int i = 0; i < ilast; i++) {
					//Boundary for axis iis periodical
					//Boundary for axis jis periodical
				}
			}
		}
		d_bdry_fill_init->createSchedule(level,this)->fillData(init_data_time, false);

    	}
}



void Problem::initializeLevelIntegrator(
   const std::shared_ptr<mesh::GriddingAlgorithmStrategy>& gridding_alg)
{
}

double Problem::getLevelDt(
   const std::shared_ptr<hier::PatchLevel>& level,
   const double dt_time,
   const bool initial_time)
{
  
   TBOX_ASSERT(level);

   if (level->getLevelNumber() == 0) return initial_dt;

    double dt = initial_dt;
    const hier::IntVector ratio = level->getRatioToLevelZero();
    double local_dt = dt;
    for (int i = 0; i < 2; i++) {
        if (local_dt > dt / ratio[i]) {
            local_dt = dt / ratio[i];
        }
    }
    return local_dt;
}

double Problem::getMaxFinerLevelDt(
   const int finer_level_number,
   const double coarse_dt,
   const hier::IntVector& ratio)
{
   NULL_USE(finer_level_number);

   TBOX_ASSERT(ratio.min() > 0);

   return coarse_dt / double(ratio.max());
}

void Problem::standardLevelSynchronization(
   const std::shared_ptr<hier::PatchHierarchy>& hierarchy,
   const int coarsest_level,
   const int finest_level,
   const double sync_time,
   const std::vector<double>& old_times)
{

}

void Problem::synchronizeNewLevels(
   const std::shared_ptr<hier::PatchHierarchy>& hierarchy,
   const int coarsest_level,
   const int finest_level,
   const double sync_time,
   const bool initial_time)
{

}

void Problem::resetTimeDependentData(
   const std::shared_ptr<hier::PatchLevel>& level,
   const double new_time,
   const bool can_be_refined)
{
   TBOX_ASSERT(level);
   level->setTime(new_time);
}

void Problem::resetDataToPreadvanceState(
   const std::shared_ptr<hier::PatchLevel>& level)
{
    //cout<<"resetDataToPreadvanceState"<<endl;
}

/*
 * Map data on a patch. This mapping is done only at the begining of the simulation.
 */
void Problem::mapDataOnPatch(const double time, const bool initial_time, const int ln, const std::shared_ptr< hier::PatchLevel >& level)
{
	(void) time;
	const tbox::SAMRAI_MPI& mpi(tbox::SAMRAI_MPI::getSAMRAIWorld());
   	if (initial_time) {

		// Mapping		
		int i, iMapStart, iMapEnd, iterm, previousMapi, iWallAcc;
		bool interiorMapi;
		int j, jMapStart, jMapEnd, jterm, previousMapj, jWallAcc;
		bool interiorMapj;
		int minBlock[2], maxBlock[2], unionsI, facePointI, ie1, ie2, ie3, proc, pcounter, working, finished, pred;
		double maxDistance, e1, e2, e3;
		bool done, modif, workingArray[mpi.getSize()], finishedArray[mpi.getSize()], workingGlobal, finishedGlobal, workingPatchArray[level->getLocalNumberOfPatches()], finishedPatchArray[level->getLocalNumberOfPatches()], workingPatchGlobal, finishedPatchGlobal;
		int nodes = mpi.getSize();
		int patches = level->getLocalNumberOfPatches();

		double SQRT3INV = 1.0/sqrt(3.0);

		if (ln == 0) {
			//FOV initialization
			for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
				const std::shared_ptr< hier::Patch >& patch = *p_it;

				//Get the dimensions of the patch
				hier::Box pbox = patch->getBox();
				double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
				double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
				double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
				double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
				double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
				const hier::Index boxfirst = patch->getBox().lower();
				const hier::Index boxlast  = patch->getBox().upper();

				//Get delta spaces into an array. dx, dy, dz.
				const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
				const double* dx  = patch_geom->getDx();

				int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
				int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;

				for (i = 0; i < ilast; i++) {
					for (j = 0; j < jlast; j++) {
						vector(FOV_1, i, j) = 0;
						vector(FOV_xLower, i, j) = 0;
						vector(FOV_xUpper, i, j) = 0;
						vector(FOV_yLower, i, j) = 0;
						vector(FOV_yUpper, i, j) = 0;
					}
				}
			}
			d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);

			//Region: mainI
			for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
				const std::shared_ptr< hier::Patch >& patch = *p_it;

				//Get the dimensions of the patch
				hier::Box pbox = patch->getBox();
				double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
				double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
				double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
				double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
				double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
				double* interior_i = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_i_id).get())->getPointer();
				double* interior_j = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_j_id).get())->getPointer();
				double* interior = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_id).get())->getPointer();
				int* nonSync = ((pdat::NodeData<int> *) patch->getPatchData(d_nonSync_id).get())->getPointer();
				const hier::Index boxfirst = patch->getBox().lower();
				const hier::Index boxlast  = patch->getBox().upper();

				//Get delta spaces into an array. dx, dy, dz.
				const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
				const double* dx  = patch_geom->getDx();

				int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
				int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;

				iMapStart = 0;
				iMapEnd = d_grid_geometry->getPhysicalDomain().front().numberCells()[0];
				jMapStart = 0;
				jMapEnd = d_grid_geometry->getPhysicalDomain().front().numberCells()[1];
				for (i = iMapStart; i <= iMapEnd; i++) {
					for (j = jMapStart; j <= jMapEnd; j++) {
						if (i >= boxfirst(0) - d_ghost_width && i <= boxlast(0) + d_ghost_width && j >= boxfirst(1) - d_ghost_width && j <= boxlast(1) + d_ghost_width) {
							vector(FOV_1, i - boxfirst(0) + d_ghost_width, j - boxfirst(1) + d_ghost_width) = 100;
							vector(FOV_xLower, i - boxfirst(0) + d_ghost_width, j - boxfirst(1) + d_ghost_width) = 0;
							vector(FOV_xUpper, i - boxfirst(0) + d_ghost_width, j - boxfirst(1) + d_ghost_width) = 0;
							vector(FOV_yLower, i - boxfirst(0) + d_ghost_width, j - boxfirst(1) + d_ghost_width) = 0;
							vector(FOV_yUpper, i - boxfirst(0) + d_ghost_width, j - boxfirst(1) + d_ghost_width) = 0;
						}
					}
				}
				//Check stencil
				for (j = 0; j < jlast; j++) {
					for (i = 0; i < ilast; i++) {
						vector(interior_i, i, j) = 0;
						vector(interior_j, i, j) = 0;
					}
				}
				for (j = 0; j < jlast; j++) {
					for (i = 0; i < ilast; i++) {
						if (vector(FOV_1, i, j) > 0) {
							setStencilLimits(patch, i, j, d_FOV_1_id);
						}
					}
				}
			}
			d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);
			for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
				const std::shared_ptr< hier::Patch >& patch = *p_it;

				//Get the dimensions of the patch
				hier::Box pbox = patch->getBox();
				double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
				double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
				double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
				double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
				double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
				double* interior_i = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_i_id).get())->getPointer();
				double* interior_j = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_j_id).get())->getPointer();
				double* interior = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_id).get())->getPointer();
				int* nonSync = ((pdat::NodeData<int> *) patch->getPatchData(d_nonSync_id).get())->getPointer();
				const hier::Index boxfirst = patch->getBox().lower();
				const hier::Index boxlast  = patch->getBox().upper();

				//Get delta spaces into an array. dx, dy, dz.
				const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
				const double* dx  = patch_geom->getDx();

				int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
				int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;

				for (j = 0; j < jlast; j++) {
					for (i = 0; i < ilast; i++) {
						if ((abs(vector(interior_i, i, j)) > 1 || abs(vector(interior_j, i, j)) > 1) && (i < d_ghost_width || i >= ilast - d_ghost_width || j < d_ghost_width || j >= jlast - d_ghost_width)) {
							checkStencil(patch, i, j, d_FOV_1_id);
						}
					}
				}
				for (j = 0; j < jlast; j++) {
					for (i = 0; i < ilast; i++) {
						if (vector(interior_i, i, j) != 0 || vector(interior_j, i, j) != 0) {
							vector(FOV_1, i, j) = 100;
							vector(FOV_xLower, i, j) = 0;
							vector(FOV_xUpper, i, j) = 0;
							vector(FOV_yLower, i, j) = 0;
							vector(FOV_yUpper, i, j) = 0;
						}
					}
				}
			}
			d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);

		}
		//Boundaries Mapping
		for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
			const std::shared_ptr< hier::Patch >& patch = *p_it;

			//Get the dimensions of the patch
			hier::Box pbox = patch->getBox();
			double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
			double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
			double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
			double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
			double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
			const hier::Index boxfirst = patch->getBox().lower();
			const hier::Index boxlast  = patch->getBox().upper();

			//Get delta spaces into an array. dx, dy, dz.
			const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
			const double* dx  = patch_geom->getDx();

			int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
			int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;

			//y-Upper
			if (patch->getPatchGeometry()->getTouchesRegularBoundary(1, 1)) {
				for (j = jlast - d_ghost_width; j < jlast; j++) {
					for (i = 0; i < ilast; i++) {
						vector(FOV_yUpper, i, j) = 100;
						vector(FOV_1, i, j) = 0;
						vector(FOV_xLower, i, j) = 0;
						vector(FOV_xUpper, i, j) = 0;
						vector(FOV_yLower, i, j) = 0;
					}
				}
			}
			//y-Lower
			if (patch->getPatchGeometry()->getTouchesRegularBoundary(1, 0)) {
				for (j = 0; j < d_ghost_width; j++) {
					for (i = 0; i < ilast; i++) {
						vector(FOV_yLower, i, j) = 100;
						vector(FOV_1, i, j) = 0;
						vector(FOV_xLower, i, j) = 0;
						vector(FOV_xUpper, i, j) = 0;
						vector(FOV_yUpper, i, j) = 0;
					}
				}
			}
			//x-Upper
			if (patch->getPatchGeometry()->getTouchesRegularBoundary(0, 1)) {
				for (j = 0; j < jlast; j++) {
					for (i = ilast - d_ghost_width; i < ilast; i++) {
						vector(FOV_xUpper, i, j) = 100;
						vector(FOV_1, i, j) = 0;
						vector(FOV_xLower, i, j) = 0;
						vector(FOV_yLower, i, j) = 0;
						vector(FOV_yUpper, i, j) = 0;
					}
				}
			}
			//x-Lower
			if (patch->getPatchGeometry()->getTouchesRegularBoundary(0, 0)) {
				for (j = 0; j < jlast; j++) {
					for (i = 0; i < d_ghost_width; i++) {
						vector(FOV_xLower, i, j) = 100;
						vector(FOV_1, i, j) = 0;
						vector(FOV_xUpper, i, j) = 0;
						vector(FOV_yLower, i, j) = 0;
						vector(FOV_yUpper, i, j) = 0;
					}
				}
			}
		}
		d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);




   	}
}


/*
 * Sets the limit for the checkstencil routine
 */
void Problem::setStencilLimits(std::shared_ptr< hier::Patch > patch, int i, int j, int v) const {
	double* FOV = ((pdat::NodeData<double> *) patch->getPatchData(v).get())->getPointer();
	double* interior_i = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_i_id).get())->getPointer();
	double* interior_j = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_j_id).get())->getPointer();
	//Get the dimensions of the patch
	const hier::Index boxfirst = patch->getBox().lower();
	const hier::Index boxlast  = patch->getBox().upper();
	//Auxiliary definitions
	int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
	int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;

	int iStart, iEnd, currentGhosti, otherSideShifti, jStart, jEnd, currentGhostj, otherSideShiftj, shift;

	currentGhosti = d_ghost_width - 1;
	otherSideShifti = 0;
	currentGhostj = d_ghost_width - 1;
	otherSideShiftj = 0;
	//Checking width
	if ((i + 1 < ilast && vector(FOV, i + 1, j) == 0) ||  (i - 1 >= 0 && vector(FOV, i - 1, j) == 0)) {
		if (i + 1 < ilast && vector(FOV, i + 1, j) > 0) {
			bool stop_counting = false;
			for(int iti = i + 1; iti <= i + d_ghost_width - 1 && currentGhosti > 0; iti++) {
				if (iti < ilast  && vector(FOV, iti, j) > 0 && stop_counting == false) {
					currentGhosti--;
				} else {
					//First not interior point found
					if (iti < ilast  && vector(FOV, iti, j) == 0) {
						stop_counting = true;
					}
					//Physical boundary reach
					if (iti >= ilast - 3 && patch->getPatchGeometry()->getTouchesRegularBoundary (0, 1)) {
						stop_counting = true;
						//Calculate the number of cells the limit cannot grow
						if (iti + currentGhosti/2 >= ilast - 3) {
							otherSideShifti = (iti  + currentGhosti/2) - (ilast - 3);
						}
					}
				}
			}
		}
		if (i - 1 >= 0 && vector(FOV, i - 1, j) > 0) {
			bool stop_counting = false;
			for(int iti = i - 1; iti >= i - d_ghost_width + 1 && currentGhosti > 0; iti--) {
				if (iti >= 0  && vector(FOV, iti, j) > 0) {
					currentGhosti--;
				} else {
					//First not interior point found
					if (iti >= 0 && vector(FOV, iti, j) == 0) {
						stop_counting = true;
					}
					//Physical boundary reach
					if (iti < 3 && patch->getPatchGeometry()->getTouchesRegularBoundary (0, 0)) {
						stop_counting = true;
						//calculate the number of cells the limit cannot grow
						if (iti  -  ((currentGhosti) - currentGhosti/2) < 3) {
							otherSideShifti = 3 - (iti  - ((currentGhosti) - currentGhosti/2));
						}
					}
				}
			}
		}
		if (currentGhosti > 0) {
			if (i + 1 < ilast && vector(FOV, i + 1, j) > 0) {
				shift = 0;
				if(patch->getPatchGeometry()->getTouchesRegularBoundary (0, 0)) {
					while(i - ((currentGhosti) - currentGhosti/2) + shift < 3) {
						shift++;
					}
				}
				iStart = (currentGhosti - currentGhosti/2) - shift - otherSideShifti;
				iEnd = 0;
			} else {
				if (i - 1 >= 0 && vector(FOV, i - 1, j) > 0) {
					shift = 0;
					if(patch->getPatchGeometry()->getTouchesRegularBoundary (0, 1)) {
						while(i + currentGhosti/2 + shift >= ilast - 3) {
							shift--;
						}
					}
					iStart = 0;
					iEnd = currentGhosti/2 + shift + otherSideShifti;
				} else {
					shift = 0;
					if(patch->getPatchGeometry()->getTouchesRegularBoundary (0, 0)) {
						while(i - ((currentGhosti) - currentGhosti/2) + shift < 3) {
							shift++;
						}
					}
					if(patch->getPatchGeometry()->getTouchesRegularBoundary (0, 1)) {
						while(i + currentGhosti/2 + shift >= ilast - 3) {
							shift--;
						}
					}
					iStart = (currentGhosti - currentGhosti/2) - shift;
					iEnd = currentGhosti/2 + shift;
				}
			}
		} else {
			iStart = 0;
			iEnd = 0;
		}
	} else {
		iStart = 0;
		iEnd = 0;
	}
	if ((j + 1 < jlast && vector(FOV, i, j + 1) == 0) ||  (j - 1 >= 0 && vector(FOV, i, j - 1) == 0)) {
		if (j + 1 < jlast && vector(FOV, i, j + 1) > 0) {
			bool stop_counting = false;
			for(int itj = j + 1; itj <= j + d_ghost_width - 1 && currentGhostj > 0; itj++) {
				if (itj < jlast  && vector(FOV, i, itj) > 0 && stop_counting == false) {
					currentGhostj--;
				} else {
					//First not interior point found
					if (itj < jlast  && vector(FOV, i, itj) == 0) {
						stop_counting = true;
					}
					//Physical boundary reach
					if (itj >= jlast - 3 && patch->getPatchGeometry()->getTouchesRegularBoundary (1, 1)) {
						stop_counting = true;
						//Calculate the number of cells the limit cannot grow
						if (itj + currentGhostj/2 >= jlast - 3) {
							otherSideShiftj = (itj  + currentGhostj/2) - (jlast - 3);
						}
					}
				}
			}
		}
		if (j - 1 >= 0 && vector(FOV, i, j - 1) > 0) {
			bool stop_counting = false;
			for(int itj = j - 1; itj >= j - d_ghost_width + 1 && currentGhostj > 0; itj--) {
				if (itj >= 0  && vector(FOV, i, itj) > 0) {
					currentGhostj--;
				} else {
					//First not interior point found
					if (itj >= 0 && vector(FOV, i, itj) == 0) {
						stop_counting = true;
					}
					//Physical boundary reach
					if (itj < 3 && patch->getPatchGeometry()->getTouchesRegularBoundary (1, 0)) {
						stop_counting = true;
						//calculate the number of cells the limit cannot grow
						if (itj  -  ((currentGhostj) - currentGhostj/2) < 3) {
							otherSideShiftj = 3 - (itj  - ((currentGhostj) - currentGhostj/2));
						}
					}
				}
			}
		}
		if (currentGhostj > 0) {
			if (j + 1 < jlast && vector(FOV, i, j + 1) > 0) {
				shift = 0;
				if(patch->getPatchGeometry()->getTouchesRegularBoundary (1, 0)) {
					while(j - ((currentGhostj) - currentGhostj/2) + shift < 3) {
						shift++;
					}
				}
				jStart = (currentGhostj - currentGhostj/2) - shift - otherSideShiftj;
				jEnd = 0;
			} else {
				if (j - 1 >= 0 && vector(FOV, i, j - 1) > 0) {
					shift = 0;
					if(patch->getPatchGeometry()->getTouchesRegularBoundary (1, 1)) {
						while(j + currentGhostj/2 + shift >= jlast - 3) {
							shift--;
						}
					}
					jStart = 0;
					jEnd = currentGhostj/2 + shift + otherSideShiftj;
				} else {
					shift = 0;
					if(patch->getPatchGeometry()->getTouchesRegularBoundary (1, 0)) {
						while(j - ((currentGhostj) - currentGhostj/2) + shift < 3) {
							shift++;
						}
					}
					if(patch->getPatchGeometry()->getTouchesRegularBoundary (1, 1)) {
						while(j + currentGhostj/2 + shift >= jlast - 3) {
							shift--;
						}
					}
					jStart = (currentGhostj - currentGhostj/2) - shift;
					jEnd = currentGhostj/2 + shift;
				}
			}
		} else {
			jStart = 0;
			jEnd = 0;
		}
	} else {
		jStart = 0;
		jEnd = 0;
	}
	//Assigning stencil limits
	for(int iti = i - iStart; iti <= i + iEnd; iti++) {
		for(int itj = j - jStart; itj <= j + jEnd; itj++) {
			if(iti >= 0 && iti < ilast && itj >= 0 && itj < jlast && vector(FOV, iti, itj) == 0) {
				if (i - iti < 0) {
					vector(interior_i, iti, itj) = - (iStart + 1) - (i - iti);
				} else {
					vector(interior_i, iti, itj) = (iStart + 1) - (i - iti);
				}
				if (j - itj < 0) {
					vector(interior_j, iti, itj) = - (jStart + 1) - (j - itj);
				} else {
					vector(interior_j, iti, itj) = (jStart + 1) - (j - itj);
				}
			}
		}
	}
}

/*
 * Checks if the point has a stencil width
 */
void Problem::checkStencil(std::shared_ptr< hier::Patch > patch, int i, int j, int v) const {
	double* FOV = ((pdat::NodeData<double> *) patch->getPatchData(v).get())->getPointer();
	double* interior_i = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_i_id).get())->getPointer();
	double* interior_j = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_j_id).get())->getPointer();
	//Get the dimensions of the patch
	const hier::Index boxfirst = patch->getBox().lower();
	const hier::Index boxlast  = patch->getBox().upper();
	//Auxiliary definitions
	int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
	int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;

	int i_i = vector(interior_i, i, j);
	int iStart = MAX(0, i_i) - 1;
	int iEnd = MAX(0, -i_i) - 1;
	int i_j = vector(interior_j, i, j);
	int jStart = MAX(0, i_j) - 1;
	int jEnd = MAX(0, -i_j) - 1;

	for(int iti = i - iStart; iti <= i + iEnd; iti++) {
		for(int itj = j - jStart; itj <= j + jEnd; itj++) {
			if(iti >= 0 && iti < ilast && itj >= 0 && itj < jlast && vector(FOV, iti, itj) == 0) {
				vector(interior_i, iti, itj) = i_i  - (i - iti);
				vector(interior_j, iti, itj) = i_j  - (j - itj);
			}
		}
	}
}


// Point class for the floodfill algorithm
class Point {
private:
public:
	int i, j;
};

void Problem::floodfill(std::shared_ptr< hier::Patch > patch, int i, int j, int pred, int seg) const {

	double* FOV;
	switch(seg) {
		case 1:
			FOV = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
		break;
	}
	int* nonSync = ((pdat::NodeData<int> *) patch->getPatchData(d_nonSync_id).get())->getPointer();
	double* interior = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_id).get())->getPointer();
	//Get the dimensions of the patch
	const hier::Index boxfirst = patch->getBox().lower();
	const hier::Index boxlast  = patch->getBox().upper();
	//Auxiliary definitions
	int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
	int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;

	stack<Point> mystack;

	Point p;
	p.i = i;
	p.j = j;

	mystack.push(p);
	while(mystack.size() > 0) {
		p = mystack.top();
		mystack.pop();
		if (vector(nonSync, p.i, p.j) == 0) {
			vector(nonSync, p.i, p.j) = pred;
			vector(interior, p.i, p.j) = pred;
			if (pred == 2) {
				vector(FOV, p.i, p.j) = 100;
			}
			if (p.i - 1 >= 0 && vector(nonSync, p.i - 1, p.j) == 0) {
				Point np;
				np.i = p.i-1;
				np.j = p.j;
				mystack.push(np);
			}
			if (p.i + 1 < ilast && vector(nonSync, p.i + 1, p.j) == 0) {
				Point np;
				np.i = p.i+1;
				np.j = p.j;
				mystack.push(np);
			}
			if (p.j - 1 >= 0 && vector(nonSync, p.i, p.j - 1) == 0) {
				Point np;
				np.i = p.i;
				np.j = p.j-1;
				mystack.push(np);
			}
			if (p.j + 1 < jlast && vector(nonSync, p.i, p.j + 1) == 0) {
				Point np;
				np.i = p.i;
				np.j = p.j+1;
				mystack.push(np);
			}
		}
	}
}


/*
 * FOV correction for AMR
 */
void Problem::correctFOVS(const std::shared_ptr< hier::PatchLevel >& level) {
	int i, j, k;
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr< hier::Patch >& patch = *p_it;

		//Get the dimensions of the patch
		hier::Box pbox = patch->getBox();
		double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
		const hier::Index boxfirst = patch->getBox().lower();
		const hier::Index boxlast  = patch->getBox().upper();

		//Get delta spaces into an array. dx, dy, dz.
		const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
		const double* dx  = patch_geom->getDx();

		int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
		int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;

		for (i = 0; i < ilast; i++) {
			for (j = 0; j < jlast; j++) {
				if (vector(FOV_xLower, i, j) > 0) {
					vector(FOV_xLower, i, j) = 100;
					vector(FOV_1, i, j) = 0;
					vector(FOV_xUpper, i, j) = 0;
					vector(FOV_yLower, i, j) = 0;
					vector(FOV_yUpper, i, j) = 0;
				}
				if (vector(FOV_xUpper, i, j) > 0) {
					vector(FOV_xUpper, i, j) = 100;
					vector(FOV_1, i, j) = 0;
					vector(FOV_xLower, i, j) = 0;
					vector(FOV_yLower, i, j) = 0;
					vector(FOV_yUpper, i, j) = 0;
				}
				if (vector(FOV_yLower, i, j) > 0) {
					vector(FOV_yLower, i, j) = 100;
					vector(FOV_1, i, j) = 0;
					vector(FOV_xLower, i, j) = 0;
					vector(FOV_xUpper, i, j) = 0;
					vector(FOV_yUpper, i, j) = 0;
				}
				if (vector(FOV_yUpper, i, j) > 0) {
					vector(FOV_yUpper, i, j) = 100;
					vector(FOV_1, i, j) = 0;
					vector(FOV_xLower, i, j) = 0;
					vector(FOV_xUpper, i, j) = 0;
					vector(FOV_yLower, i, j) = 0;
				}
				if (vector(FOV_1, i, j) > 0) {
					vector(FOV_1, i, j) = 100;
					vector(FOV_xLower, i, j) = 0;
					vector(FOV_xUpper, i, j) = 0;
					vector(FOV_yLower, i, j) = 0;
					vector(FOV_yUpper, i, j) = 0;
				}
			}
		}
	}
}




void Problem::interphaseMapping(const double time ,const bool initial_time,const int ln, const std::shared_ptr< hier::PatchLevel >& level, const int remesh) {
	const tbox::SAMRAI_MPI& mpi(tbox::SAMRAI_MPI::getSAMRAIWorld());
	if (remesh == 1) {
		//Update extrapolation variables
	}
}


/*
 * Checks if the point has to be stalled
 */
bool Problem::checkStalled(std::shared_ptr< hier::Patch > patch, int i, int j, int v) const {
	double* FOV = ((pdat::NodeData<double> *) patch->getPatchData(v).get())->getPointer();
	double* interior = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_id).get())->getPointer();
	//Get the dimensions of the patch
	const hier::Index boxfirst = patch->getBox().lower();
	const hier::Index boxlast  = patch->getBox().upper();
	//Auxiliary definitions
	int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
	int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;

	int stencilAcc, stencilAccMax_i, stencilAccMax_j;
	bool notEnoughStencil = false;
	int FOV_threshold = 0;
	if (vector(FOV, i, j) <= FOV_threshold) {
		notEnoughStencil = true;
	} else {
		stencilAcc = 0;
		stencilAccMax_i = 0;
		for (int it1 = MAX(i-d_regionMinThickness, 0); it1 <= MIN(i+d_regionMinThickness, ilast); it1++) {
			if (vector(FOV, it1, j) > FOV_threshold) {
				stencilAcc++;
			} else {
				stencilAccMax_i = MAX(stencilAccMax_i, stencilAcc);
				stencilAcc = 0;
			}
		}
		stencilAccMax_i = MAX(stencilAccMax_i, stencilAcc);
		stencilAcc = 0;
		stencilAccMax_j = 0;
		for (int jt1 = MAX(j-d_regionMinThickness, 0); jt1 <= MIN(j+d_regionMinThickness, jlast); jt1++) {
			if (vector(FOV, i, jt1) > FOV_threshold) {
				stencilAcc++;
			} else {
				stencilAccMax_j = MAX(stencilAccMax_j, stencilAcc);
				stencilAcc = 0;
			}
		}
		stencilAccMax_j = MAX(stencilAccMax_j, stencilAcc);
		if ((stencilAccMax_i < d_regionMinThickness) || (stencilAccMax_j < d_regionMinThickness)) {
			notEnoughStencil = true;
		}
	}
	return notEnoughStencil;
}






/*
 * Initialize data on a patch. This initialization is done only at the begining of the simulation.
 */
void Problem::initializeDataOnPatch(hier::Patch& patch, 
                                    const double time,
                                    const bool initial_time)
{
	(void) time;
   	if (initial_time) {
		// Initial conditions		
		//Get fields, auxiliary fields and local variables that are going to be used.
		double* alpha = ((pdat::NodeData<double> *) patch.getPatchData(d_alpha_id).get())->getPointer();
		((pdat::NodeData<double> *) patch.getPatchData(d_alpha_id).get())->fillAll(0);
		double* beta_x = ((pdat::NodeData<double> *) patch.getPatchData(d_beta_x_id).get())->getPointer();
		((pdat::NodeData<double> *) patch.getPatchData(d_beta_x_id).get())->fillAll(0);
		double* beta_y = ((pdat::NodeData<double> *) patch.getPatchData(d_beta_y_id).get())->getPointer();
		((pdat::NodeData<double> *) patch.getPatchData(d_beta_y_id).get())->fillAll(0);
		double* K = ((pdat::NodeData<double> *) patch.getPatchData(d_K_id).get())->getPointer();
		((pdat::NodeData<double> *) patch.getPatchData(d_K_id).get())->fillAll(0);
		double* phi = ((pdat::NodeData<double> *) patch.getPatchData(d_phi_id).get())->getPointer();
		((pdat::NodeData<double> *) patch.getPatchData(d_phi_id).get())->fillAll(0);
		
		//Get the dimensions of the patch
		hier::Box pbox = patch.getBox();
		double* FOV_1 = ((pdat::NodeData<double> *) patch.getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch.getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch.getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch.getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch.getPatchData(d_FOV_yUpper_id).get())->getPointer();
		const hier::Index boxfirst = patch.getBox().lower();
		const hier::Index boxlast  = patch.getBox().upper();
		
		//Get delta spaces into an array. dx, dy, dz.
		const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch.getPatchGeometry()));
		const double* dx  = patch_geom->getDx();
		
		int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
		int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
		for(int j = 0; j < jlast; j++) {
			for(int i = 0; i < ilast; i++) {
				if (vector(FOV_1, i, j) > 0) {
					vector(alpha, i, j) = 1.0 - 0.4 * exp((-((d_grid_geometry->getXLower()[0] + ((i + boxfirst(0)) - d_ghost_width) * dx[0]) * (d_grid_geometry->getXLower()[0] + ((i + boxfirst(0)) - d_ghost_width) * dx[0]) + (d_grid_geometry->getXLower()[1] + ((j + boxfirst(1)) - d_ghost_width) * dx[1]) * (d_grid_geometry->getXLower()[1] + ((j + boxfirst(1)) - d_ghost_width) * dx[1]))) / 0.05);
					vector(beta_x, i, j) = (d_grid_geometry->getXLower()[0] + ((i + boxfirst(0)) - d_ghost_width) * dx[0]) * exp((-((d_grid_geometry->getXLower()[0] + ((i + boxfirst(0)) - d_ghost_width) * dx[0]) * (d_grid_geometry->getXLower()[0] + ((i + boxfirst(0)) - d_ghost_width) * dx[0]) + (d_grid_geometry->getXLower()[1] + ((j + boxfirst(1)) - d_ghost_width) * dx[1]) * (d_grid_geometry->getXLower()[1] + ((j + boxfirst(1)) - d_ghost_width) * dx[1]))) / 0.05);
					vector(beta_y, i, j) = (d_grid_geometry->getXLower()[1] + ((j + boxfirst(1)) - d_ghost_width) * dx[1]) * exp((-((d_grid_geometry->getXLower()[0] + ((i + boxfirst(0)) - d_ghost_width) * dx[0]) * (d_grid_geometry->getXLower()[0] + ((i + boxfirst(0)) - d_ghost_width) * dx[0]) + (d_grid_geometry->getXLower()[1] + ((j + boxfirst(1)) - d_ghost_width) * dx[1]) * (d_grid_geometry->getXLower()[1] + ((j + boxfirst(1)) - d_ghost_width) * dx[1]))) / 0.05);
					vector(K, i, j) = 0.0;
					vector(phi, i, j) = agauss * exp((-((d_grid_geometry->getXLower()[0] + ((i + boxfirst(0)) - d_ghost_width) * dx[0]) * (d_grid_geometry->getXLower()[0] + ((i + boxfirst(0)) - d_ghost_width) * dx[0]) + (d_grid_geometry->getXLower()[1] + ((j + boxfirst(1)) - d_ghost_width) * dx[1]) * (d_grid_geometry->getXLower()[1] + ((j + boxfirst(1)) - d_ghost_width) * dx[1]))) / bgauss);
				}
		
				//Boundaries Initialization
			}
		}
		
		

   	}
}

/*
 * Gets the coarser patch that contains the box
 */
const std::shared_ptr<hier::Patch >& Problem::getCoarserPatch(
	const std::shared_ptr< hier::PatchLevel >& level,
	const hier::Box interior, 
	const hier::IntVector ratio)
{
	const hier::Box& coarsenBox = hier::Box::coarsen(interior, ratio);
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr< hier::Patch >& patch = *p_it;
		const hier::Box& interior_C = patch->getBox();
		if (interior_C.intersects(coarsenBox)) {
			return patch;		
		}
	}
}

/*
 * Reset the hierarchy-dependent internal information.
 */
void Problem::resetHierarchyConfiguration (
   const std::shared_ptr<hier::PatchHierarchy >& new_hierarchy ,
   int coarsest_level ,
   int finest_level )
{
	int finest_hiera_level = new_hierarchy->getFinestLevelNumber();

   	//  If we have added or removed a level, resize the schedule arrays

	d_coarsen_schedule.resize(finest_hiera_level+1);
	d_bdry_sched_postCoarsen.resize(finest_hiera_level+1);
	d_sim_substep.resize(finest_hiera_level+1);
	//  Build coarsen and refine communication schedules.
	for (int ln = coarsest_level; ln <= finest_hiera_level; ln++) {
		std::shared_ptr< hier::PatchLevel > level(new_hierarchy->getPatchLevel(ln));
		d_bdry_sched_postCoarsen[ln] = d_bdry_fill_init->createSchedule(level);
		// coarsen schedule only for levels > 0
		if (ln > 0) {
			std::shared_ptr< hier::PatchLevel > coarser_level(new_hierarchy->getPatchLevel(ln-1));
			d_coarsen_schedule[ln] = d_coarsen_algorithm->createSchedule(coarser_level, level, NULL);
		}
	}

}




/*
 * This method sets the physical boundary conditions.
 */
void Problem::setPhysicalBoundaryConditions(
   hier::Patch& patch,
   const double fill_time,
   const hier::IntVector& ghost_width_to_fill)
{
	//Boundary must not be implemented in this method
}

/*
 * Set up external plotter to plot internal data from this class.  
 * Register variables appropriate for plotting.                       
 */
int Problem::setupPlotter(
  appu::VisItDataWriter &plotter
) const {
	if (!d_patch_hierarchy) {
		TBOX_ERROR(d_object_name << ": No hierarchy in\n"
			<< " Problem::setupPlotter\n"
                 	<< "The hierarchy must be set before calling\n"
                 	<< "this function.\n");
   	}
	plotter.registerPlotQuantity("FOV_1","SCALAR",d_FOV_1_id);
	hier::VariableDatabase *vdb = hier::VariableDatabase::getDatabase();
	for (vector<string>::const_iterator it = d_full_writer_variables.begin() ; it != d_full_writer_variables.end(); ++it) {
		string var_to_register = *it;
		if (!(vdb->checkVariableExists(var_to_register))) {
			TBOX_ERROR(d_object_name << ": Variable selected for 3D write not found:" <<  var_to_register);
		}
		int var_id = vdb->getVariable(var_to_register)->getInstanceIdentifier();
		plotter.registerPlotQuantity(var_to_register,"SCALAR",var_id);
	}


   	return 0;
}
/*
 * Set up external plotter to plot integration data from this class.
 * Register variables appropriate for plotting.
 */
int Problem::setupIntegralPlotter(vector<std::shared_ptr<IntegrateDataWriter> > &plotters) const {
	if (!d_patch_hierarchy) {
		TBOX_ERROR(d_object_name << ": No hierarchy inn"
		<< " Problem::setupIntegralPlottern"
		<< "The hierarchy must be set before callingn"
		<< "this function.n");
	}
	int i = 0;
	for (vector<std::shared_ptr<IntegrateDataWriter> >::const_iterator it = plotters.begin(); it != plotters.end(); ++it) {
		std::shared_ptr<IntegrateDataWriter> plotter = *it;
		hier::VariableDatabase *vdb = hier::VariableDatabase::getDatabase();
		vector<string> variables = d_integralVariables[i];
		for (vector<string>::const_iterator it2 = variables.begin() ; it2 != variables.end(); ++it2) {
			string var_to_register = *it2;
			if (!(vdb->checkVariableExists(var_to_register))) {
				TBOX_ERROR(d_object_name << ": Variable selected for Integration not found:" <<  var_to_register);
			}
			int var_id = vdb->getVariable(var_to_register)->getInstanceIdentifier();
			plotter->registerPlotQuantity(var_to_register,"SCALAR",var_id);
		}
		i++;
	}
	return 0;
}


/*
 * Perform a single step from the discretization schema algorithm   
 */
double Problem::advanceLevel(
   const std::shared_ptr<hier::PatchLevel>& level,
   const std::shared_ptr<hier::PatchHierarchy>& hierarchy,
   const double current_time,
   const double new_time,
   const bool first_step,
   const bool last_step,
   const bool regrid_advance)
{
	const int ln = level->getLevelNumber();
	// Calculation of number of current substep
	if (first_step) {
		d_sim_substep[ln] = d_patch_hierarchy->getRatioToCoarserLevel(ln).max();
	} else {
		d_sim_substep[ln] = d_sim_substep[ln] - 1;
	}

	const double simPlat_dt = new_time - current_time;
  const double level_ratio = level->getRatioToCoarserLevel().max();

	t_step->start();
  	// Shifting time
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch > patch = *p_it;
		std::shared_ptr< pdat::NodeData<double> > beta_y(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_beta_y_id)));
		std::shared_ptr< pdat::NodeData<double> > beta_y_p(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_beta_y_p_id)));
		std::shared_ptr< pdat::NodeData<double> > beta_x(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_beta_x_id)));
		std::shared_ptr< pdat::NodeData<double> > beta_x_p(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_beta_x_p_id)));
		std::shared_ptr< pdat::NodeData<double> > alpha(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_alpha_id)));
		std::shared_ptr< pdat::NodeData<double> > alpha_p(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_alpha_p_id)));
		std::shared_ptr< pdat::NodeData<double> > phi(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_phi_id)));
		std::shared_ptr< pdat::NodeData<double> > phi_p(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_phi_p_id)));
		std::shared_ptr< pdat::NodeData<double> > K(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_K_id)));
		std::shared_ptr< pdat::NodeData<double> > K_p(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_K_p_id)));
		std::shared_ptr< pdat::NodeData<double> > k1alpha(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_k1alpha_id)));
		std::shared_ptr< pdat::NodeData<double> > k1beta_x(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_k1beta_x_id)));
		std::shared_ptr< pdat::NodeData<double> > k1beta_y(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_k1beta_y_id)));
		std::shared_ptr< pdat::NodeData<double> > k1phi(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_k1phi_id)));
		std::shared_ptr< pdat::NodeData<double> > k1K(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_k1K_id)));
		std::shared_ptr< pdat::NodeData<double> > k2alpha(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_k2alpha_id)));
		std::shared_ptr< pdat::NodeData<double> > k2beta_x(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_k2beta_x_id)));
		std::shared_ptr< pdat::NodeData<double> > k2beta_y(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_k2beta_y_id)));
		std::shared_ptr< pdat::NodeData<double> > k2phi(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_k2phi_id)));
		std::shared_ptr< pdat::NodeData<double> > k2K(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_k2K_id)));

		const hier::Index boxfirst = patch->getBox().lower();
		const hier::Index boxlast  = patch->getBox().upper();
		K_p->copy(*K);
		phi_p->copy(*phi);
		alpha_p->copy(*alpha);
		beta_x_p->copy(*beta_x);
		beta_y_p->copy(*beta_y);
		beta_y_p->setTime(current_time);
		beta_x_p->setTime(current_time);
		alpha_p->setTime(current_time);
		phi_p->setTime(current_time);
		K_p->setTime(current_time);
		k1alpha->setTime(current_time + simPlat_dt);
		k1beta_x->setTime(current_time + simPlat_dt);
		k1beta_y->setTime(current_time + simPlat_dt);
		k1phi->setTime(current_time + simPlat_dt);
		k1K->setTime(current_time + simPlat_dt);
		k2alpha->setTime(current_time + simPlat_dt / 2.0);
		k2beta_x->setTime(current_time + simPlat_dt / 2.0);
		k2beta_y->setTime(current_time + simPlat_dt / 2.0);
		k2phi->setTime(current_time + simPlat_dt / 2.0);
		k2K->setTime(current_time + simPlat_dt / 2.0);
		alpha->setTime(current_time + simPlat_dt);
		beta_x->setTime(current_time + simPlat_dt);
		beta_y->setTime(current_time + simPlat_dt);
		phi->setTime(current_time + simPlat_dt);
		K->setTime(current_time + simPlat_dt);
	}
  	// Evolution
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch >& patch = *p_it;
		double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
	
		double* ad_K_o0_t3_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t3_m0_l0_1_id).get())->getPointer();
		double* phi_p = ((pdat::NodeData<double> *) patch->getPatchData(d_phi_p_id).get())->getPointer();
	
		//Get the dimensions of the patch
		hier::Box pbox = patch->getBox();
		const hier::Index boxfirst = patch->getBox().lower();
		const hier::Index boxlast = patch->getBox().upper();
	
		//Get delta spaces into an array. dx, dy, dz.
		std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
		const double* dx  = patch_geom->getDx();
	
		//Auxiliary definitions
		int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
		int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
		for(int j = 0; j < jlast; j++) {
			for(int i = 0; i < ilast; i++) {
				if ((vector(FOV_1, i, j) > 0 || (((i + 1 < ilast && vector(FOV_1, i + 1, j) > 0) || (j + 1 < jlast && vector(FOV_1, i, j + 1) > 0)) || ((i - 1 >= 0 && vector(FOV_1, i - 1, j) > 0) || (j - 1 >= 0 && vector(FOV_1, i, j - 1) > 0))) || (d_tappering && (neighbourTappering(FOV_1, i, j, ilast, jlast, 1, 0 * d_sim_substep[ln]))))) {
					vector(ad_K_o0_t3_m0_l0_1, i, j) = vector(phi_p, i, j);
				}
			}
		}
	}
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch >& patch = *p_it;
		double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
	
		double* d_K_o0_t3_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_d_K_o0_t3_m0_l0_1_id).get())->getPointer();
		double* ad_K_o0_t3_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t3_m0_l0_1_id).get())->getPointer();
		double* ad_beta_x_o0_t0_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_beta_x_o0_t0_m0_l0_1_id).get())->getPointer();
		double* phi_p = ((pdat::NodeData<double> *) patch->getPatchData(d_phi_p_id).get())->getPointer();
		double* ad_beta_x_o0_t1_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_beta_x_o0_t1_m0_l0_1_id).get())->getPointer();
		double* ad_beta_y_o0_t0_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_beta_y_o0_t0_m0_l0_1_id).get())->getPointer();
		double* ad_beta_y_o0_t1_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_beta_y_o0_t1_m0_l0_1_id).get())->getPointer();
		double* ad_phi_o0_t0_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_phi_o0_t0_m0_l0_1_id).get())->getPointer();
		double* ad_phi_o0_t1_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_phi_o0_t1_m0_l0_1_id).get())->getPointer();
		double* ad_K_o0_t0_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t0_m0_l0_1_id).get())->getPointer();
		double* K_p = ((pdat::NodeData<double> *) patch->getPatchData(d_K_p_id).get())->getPointer();
		double* ad_K_o0_t1_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t1_m0_l0_1_id).get())->getPointer();
		double* ad_K_o0_t2_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t2_m0_l0_1_id).get())->getPointer();
		double* ad_K_o0_t4_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t4_m0_l0_1_id).get())->getPointer();
		double* ad_K_o0_t6_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t6_m0_l0_1_id).get())->getPointer();
		double* ad_K_o0_t6_m1_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t6_m1_l0_1_id).get())->getPointer();
		double* alpha_p = ((pdat::NodeData<double> *) patch->getPatchData(d_alpha_p_id).get())->getPointer();
		double* ad_K_o0_t7_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t7_m0_l0_1_id).get())->getPointer();
		double* ad_K_o0_t7_m1_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t7_m1_l0_1_id).get())->getPointer();
		double* ad_K_o0_t8_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t8_m0_l0_1_id).get())->getPointer();
		double* ad_K_o0_t8_m1_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t8_m1_l0_1_id).get())->getPointer();
		double* ad_K_o0_t9_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t9_m0_l0_1_id).get())->getPointer();
		double* ad_K_o0_t9_m1_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t9_m1_l0_1_id).get())->getPointer();
	
		//Get the dimensions of the patch
		hier::Box pbox = patch->getBox();
		const hier::Index boxfirst = patch->getBox().lower();
		const hier::Index boxlast = patch->getBox().upper();
	
		//Get delta spaces into an array. dx, dy, dz.
		std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
		const double* dx  = patch_geom->getDx();
	
		//Auxiliary definitions
		int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
		int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
		for(int j = 0; j < jlast; j++) {
			for(int i = 0; i < ilast; i++) {
				if ((vector(FOV_1, i, j) > 0 || (((i + 1 < ilast && vector(FOV_1, i + 1, j) > 0) || (i + 2 < ilast && vector(FOV_1, i + 2, j) > 0) || (j + 1 < jlast && vector(FOV_1, i, j + 1) > 0) || (j + 2 < jlast && vector(FOV_1, i, j + 2) > 0)) || ((i - 1 >= 0 && vector(FOV_1, i - 1, j) > 0) || (i - 2 >= 0 && vector(FOV_1, i - 2, j) > 0) || (j - 1 >= 0 && vector(FOV_1, i, j - 1) > 0) || (j - 2 >= 0 && vector(FOV_1, i, j - 2) > 0))) || (d_tappering && (neighbourTappering(FOV_1, i, j, ilast, jlast, 2, 0 * d_sim_substep[ln]))))) {
					vector(d_K_o0_t3_m0_l0_1, i, j) = SpatialDisc1_i(ad_K_o0_t3_m0_l0_1, i, j, dx, simPlat_dt, ilast, jlast);
					vector(ad_beta_x_o0_t0_m0_l0_1, i, j) = vector(phi_p, i, j);
					vector(ad_beta_x_o0_t1_m0_l0_1, i, j) = vector(phi_p, i, j);
					vector(ad_beta_y_o0_t0_m0_l0_1, i, j) = vector(phi_p, i, j);
					vector(ad_beta_y_o0_t1_m0_l0_1, i, j) = vector(phi_p, i, j);
					vector(ad_phi_o0_t0_m0_l0_1, i, j) = vector(phi_p, i, j);
					vector(ad_phi_o0_t1_m0_l0_1, i, j) = vector(phi_p, i, j);
					vector(ad_K_o0_t0_m0_l0_1, i, j) = vector(K_p, i, j);
					vector(ad_K_o0_t1_m0_l0_1, i, j) = vector(K_p, i, j);
					vector(ad_K_o0_t2_m0_l0_1, i, j) = vector(phi_p, i, j);
					vector(ad_K_o0_t4_m0_l0_1, i, j) = vector(phi_p, i, j);
					vector(ad_K_o0_t6_m0_l0_1, i, j) = vector(phi_p, i, j);
					vector(ad_K_o0_t6_m1_l0_1, i, j) = vector(alpha_p, i, j);
					vector(ad_K_o0_t7_m0_l0_1, i, j) = vector(phi_p, i, j);
					vector(ad_K_o0_t7_m1_l0_1, i, j) = vector(alpha_p, i, j);
					vector(ad_K_o0_t8_m0_l0_1, i, j) = vector(phi_p, i, j);
					vector(ad_K_o0_t8_m1_l0_1, i, j) = vector(alpha_p, i, j);
					vector(ad_K_o0_t9_m0_l0_1, i, j) = vector(phi_p, i, j);
					vector(ad_K_o0_t9_m1_l0_1, i, j) = vector(alpha_p, i, j);
				}
			}
		}
	}
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch >& patch = *p_it;
		double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
	
		double* ad_beta_x_o0_t0_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_beta_x_o0_t0_m0_l0_1_id).get())->getPointer();
		double* ad_beta_x_o0_t1_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_beta_x_o0_t1_m0_l0_1_id).get())->getPointer();
		double* ad_beta_y_o0_t0_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_beta_y_o0_t0_m0_l0_1_id).get())->getPointer();
		double* ad_beta_y_o0_t1_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_beta_y_o0_t1_m0_l0_1_id).get())->getPointer();
		double* ad_phi_o0_t0_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_phi_o0_t0_m0_l0_1_id).get())->getPointer();
		double* ad_phi_o0_t1_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_phi_o0_t1_m0_l0_1_id).get())->getPointer();
		double* ad_K_o0_t0_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t0_m0_l0_1_id).get())->getPointer();
		double* ad_K_o0_t1_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t1_m0_l0_1_id).get())->getPointer();
		double* ad_K_o0_t2_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t2_m0_l0_1_id).get())->getPointer();
		double* d_K_o0_t3_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_d_K_o0_t3_m0_l0_1_id).get())->getPointer();
		double* ad_K_o0_t4_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t4_m0_l0_1_id).get())->getPointer();
		double* ad_K_o0_t6_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t6_m0_l0_1_id).get())->getPointer();
		double* ad_K_o0_t6_m1_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t6_m1_l0_1_id).get())->getPointer();
		double* ad_K_o0_t7_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t7_m0_l0_1_id).get())->getPointer();
		double* ad_K_o0_t7_m1_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t7_m1_l0_1_id).get())->getPointer();
		double* ad_K_o0_t8_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t8_m0_l0_1_id).get())->getPointer();
		double* ad_K_o0_t8_m1_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t8_m1_l0_1_id).get())->getPointer();
		double* ad_K_o0_t9_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t9_m0_l0_1_id).get())->getPointer();
		double* ad_K_o0_t9_m1_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t9_m1_l0_1_id).get())->getPointer();
		double* alpha_p = ((pdat::NodeData<double> *) patch->getPatchData(d_alpha_p_id).get())->getPointer();
		double* K_p = ((pdat::NodeData<double> *) patch->getPatchData(d_K_p_id).get())->getPointer();
		double* phi_p = ((pdat::NodeData<double> *) patch->getPatchData(d_phi_p_id).get())->getPointer();
		double* beta_x_p = ((pdat::NodeData<double> *) patch->getPatchData(d_beta_x_p_id).get())->getPointer();
		double* beta_y_p = ((pdat::NodeData<double> *) patch->getPatchData(d_beta_y_p_id).get())->getPointer();
		double* k1alpha = ((pdat::NodeData<double> *) patch->getPatchData(d_k1alpha_id).get())->getPointer();
		double* k1beta_x = ((pdat::NodeData<double> *) patch->getPatchData(d_k1beta_x_id).get())->getPointer();
		double* k1beta_y = ((pdat::NodeData<double> *) patch->getPatchData(d_k1beta_y_id).get())->getPointer();
		double* k1phi = ((pdat::NodeData<double> *) patch->getPatchData(d_k1phi_id).get())->getPointer();
		double* k1K = ((pdat::NodeData<double> *) patch->getPatchData(d_k1K_id).get())->getPointer();
		double d_beta_x_o0_t0_m0_l0_1, d_beta_x_o0_t1_m0_l0_1, d_beta_y_o0_t0_m0_l0_1, d_beta_y_o0_t1_m0_l0_1, d_phi_o0_t0_m0_l0_1, d_phi_o0_t1_m0_l0_1, d_K_o0_t0_m0_l0_1, d_K_o0_t1_m0_l0_1, d_K_o0_t2_m0_l0_1, d_K_o0_t3_m0_l1_1, d_K_o0_t4_m0_l0_1, d_K_o0_t6_m0_l0_1, d_K_o0_t6_m1_l0_1, d_K_o0_t7_m0_l0_1, d_K_o0_t7_m1_l0_1, d_K_o0_t8_m0_l0_1, d_K_o0_t8_m1_l0_1, d_K_o0_t9_m0_l0_1, d_K_o0_t9_m1_l0_1, d_alpha_o0_t0_m0_l0_1, d_phi_o0_t2_m0_l0_1, d_K_o0_t5_m0_l0_1, m_beta_x_o0_t0_l0_1, m_beta_x_o0_t1_l0_1, m_beta_y_o0_t0_l0_1, m_beta_y_o0_t1_l0_1, m_phi_o0_t0_l0_1, m_phi_o0_t1_l0_1, m_K_o0_t0_l0_1, m_K_o0_t1_l0_1, m_K_o0_t2_l0_1, m_K_o0_t3_l1_1, m_K_o0_t4_l0_1, m_K_o0_t6_l0_1, m_K_o0_t7_l0_1, m_K_o0_t8_l0_1, m_K_o0_t9_l0_1, RHS_alpha_1, RHS_beta_x_1, RHS_beta_y_1, RHS_phi_1, RHS_K_1;
	
		//Get the dimensions of the patch
		hier::Box pbox = patch->getBox();
		const hier::Index boxfirst = patch->getBox().lower();
		const hier::Index boxlast = patch->getBox().upper();
	
		//Get delta spaces into an array. dx, dy, dz.
		std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
		const double* dx  = patch_geom->getDx();
	
		//Auxiliary definitions
		int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
		int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
		for(int j = 0; j < jlast; j++) {
			for(int i = 0; i < ilast; i++) {
				if ((vector(FOV_1, i, j) > 0)) {
					d_beta_x_o0_t0_m0_l0_1 = SpatialDisc1_i(ad_beta_x_o0_t0_m0_l0_1, i, j, dx, simPlat_dt, ilast, jlast);
					d_beta_x_o0_t1_m0_l0_1 = SpatialDisc1_j(ad_beta_x_o0_t1_m0_l0_1, i, j, dx, simPlat_dt, ilast, jlast);
					d_beta_y_o0_t0_m0_l0_1 = SpatialDisc1_i(ad_beta_y_o0_t0_m0_l0_1, i, j, dx, simPlat_dt, ilast, jlast);
					d_beta_y_o0_t1_m0_l0_1 = SpatialDisc1_j(ad_beta_y_o0_t1_m0_l0_1, i, j, dx, simPlat_dt, ilast, jlast);
					d_phi_o0_t0_m0_l0_1 = SpatialDisc1_i(ad_phi_o0_t0_m0_l0_1, i, j, dx, simPlat_dt, ilast, jlast);
					d_phi_o0_t1_m0_l0_1 = SpatialDisc1_j(ad_phi_o0_t1_m0_l0_1, i, j, dx, simPlat_dt, ilast, jlast);
					d_K_o0_t0_m0_l0_1 = SpatialDisc1_i(ad_K_o0_t0_m0_l0_1, i, j, dx, simPlat_dt, ilast, jlast);
					d_K_o0_t1_m0_l0_1 = SpatialDisc1_j(ad_K_o0_t1_m0_l0_1, i, j, dx, simPlat_dt, ilast, jlast);
					d_K_o0_t2_m0_l0_1 = SpatialDisc2_i(ad_K_o0_t2_m0_l0_1, i, j, dx, simPlat_dt, ilast, jlast);
					d_K_o0_t3_m0_l1_1 = SpatialDisc1_j(d_K_o0_t3_m0_l0_1, i, j, dx, simPlat_dt, ilast, jlast);
					d_K_o0_t4_m0_l0_1 = SpatialDisc2_j(ad_K_o0_t4_m0_l0_1, i, j, dx, simPlat_dt, ilast, jlast);
					d_K_o0_t6_m0_l0_1 = SpatialDisc1_i(ad_K_o0_t6_m0_l0_1, i, j, dx, simPlat_dt, ilast, jlast);
					d_K_o0_t6_m1_l0_1 = SpatialDisc1_i(ad_K_o0_t6_m1_l0_1, i, j, dx, simPlat_dt, ilast, jlast);
					d_K_o0_t7_m0_l0_1 = SpatialDisc1_j(ad_K_o0_t7_m0_l0_1, i, j, dx, simPlat_dt, ilast, jlast);
					d_K_o0_t7_m1_l0_1 = SpatialDisc1_i(ad_K_o0_t7_m1_l0_1, i, j, dx, simPlat_dt, ilast, jlast);
					d_K_o0_t8_m0_l0_1 = SpatialDisc1_i(ad_K_o0_t8_m0_l0_1, i, j, dx, simPlat_dt, ilast, jlast);
					d_K_o0_t8_m1_l0_1 = SpatialDisc1_j(ad_K_o0_t8_m1_l0_1, i, j, dx, simPlat_dt, ilast, jlast);
					d_K_o0_t9_m0_l0_1 = SpatialDisc1_j(ad_K_o0_t9_m0_l0_1, i, j, dx, simPlat_dt, ilast, jlast);
					d_K_o0_t9_m1_l0_1 = SpatialDisc1_j(ad_K_o0_t9_m1_l0_1, i, j, dx, simPlat_dt, ilast, jlast);
					d_alpha_o0_t0_m0_l0_1 = -gauge_evol * vector(alpha_p, i, j);
					d_phi_o0_t2_m0_l0_1 = -vector(alpha_p, i, j) * vector(K_p, i, j);
					d_K_o0_t5_m0_l0_1 = vector(alpha_p, i, j) * (m * m) * vector(phi_p, i, j);
					m_beta_x_o0_t0_l0_1 = (-gauge_evol * vector(alpha_p, i, j) * a) * d_beta_x_o0_t0_m0_l0_1;
					m_beta_x_o0_t1_l0_1 = (-gauge_evol * vector(alpha_p, i, j) * b) * d_beta_x_o0_t1_m0_l0_1;
					m_beta_y_o0_t0_l0_1 = (-gauge_evol * vector(alpha_p, i, j) * b) * d_beta_y_o0_t0_m0_l0_1;
					m_beta_y_o0_t1_l0_1 = (-gauge_evol * vector(alpha_p, i, j) * a) * d_beta_y_o0_t1_m0_l0_1;
					m_phi_o0_t0_l0_1 = vector(beta_x_p, i, j) * d_phi_o0_t0_m0_l0_1;
					m_phi_o0_t1_l0_1 = vector(beta_y_p, i, j) * d_phi_o0_t1_m0_l0_1;
					m_K_o0_t0_l0_1 = vector(beta_x_p, i, j) * d_K_o0_t0_m0_l0_1;
					m_K_o0_t1_l0_1 = vector(beta_y_p, i, j) * d_K_o0_t1_m0_l0_1;
					m_K_o0_t2_l0_1 = (-vector(alpha_p, i, j) * a) * d_K_o0_t2_m0_l0_1;
					m_K_o0_t3_l1_1 = (-2.0 * vector(alpha_p, i, j) * b) * d_K_o0_t3_m0_l1_1;
					m_K_o0_t4_l0_1 = (-vector(alpha_p, i, j) * a) * d_K_o0_t4_m0_l0_1;
					m_K_o0_t6_l0_1 = (-a) * d_K_o0_t6_m0_l0_1 * d_K_o0_t6_m1_l0_1;
					m_K_o0_t7_l0_1 = (-b) * d_K_o0_t7_m0_l0_1 * d_K_o0_t7_m1_l0_1;
					m_K_o0_t8_l0_1 = (-b) * d_K_o0_t8_m0_l0_1 * d_K_o0_t8_m1_l0_1;
					m_K_o0_t9_l0_1 = (-a) * d_K_o0_t9_m0_l0_1 * d_K_o0_t9_m1_l0_1;
					RHS_alpha_1 = d_alpha_o0_t0_m0_l0_1;
					RHS_beta_x_1 = m_beta_x_o0_t0_l0_1 + m_beta_x_o0_t1_l0_1;
					RHS_beta_y_1 = m_beta_y_o0_t0_l0_1 + m_beta_y_o0_t1_l0_1;
					RHS_phi_1 = (m_phi_o0_t0_l0_1 + m_phi_o0_t1_l0_1) + d_phi_o0_t2_m0_l0_1;
					RHS_K_1 = ((((((((m_K_o0_t0_l0_1 + m_K_o0_t1_l0_1) + m_K_o0_t2_l0_1) + m_K_o0_t3_l1_1) + m_K_o0_t4_l0_1) + d_K_o0_t5_m0_l0_1) + m_K_o0_t6_l0_1) + m_K_o0_t7_l0_1) + m_K_o0_t8_l0_1) + m_K_o0_t9_l0_1;
					vector(k1alpha, i, j) = timeDisc3_(RHS_alpha_1, alpha_p, 0.0, i, j, dx, simPlat_dt, ilast, jlast);
					vector(k1beta_x, i, j) = timeDisc3_(RHS_beta_x_1, beta_x_p, 0.0, i, j, dx, simPlat_dt, ilast, jlast);
					vector(k1beta_y, i, j) = timeDisc3_(RHS_beta_y_1, beta_y_p, 0.0, i, j, dx, simPlat_dt, ilast, jlast);
					vector(k1phi, i, j) = timeDisc3_(RHS_phi_1, phi_p, 0.0, i, j, dx, simPlat_dt, ilast, jlast);
					vector(k1K, i, j) = timeDisc3_(RHS_K_1, K_p, 0.0, i, j, dx, simPlat_dt, ilast, jlast);
				}
			}
		}
	}
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch >& patch = *p_it;
		double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
	
		double* ad_K_o0_t3_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t3_m0_l0_1_id).get())->getPointer();
		double* k1phi = ((pdat::NodeData<double> *) patch->getPatchData(d_k1phi_id).get())->getPointer();
	
		//Get the dimensions of the patch
		hier::Box pbox = patch->getBox();
		const hier::Index boxfirst = patch->getBox().lower();
		const hier::Index boxlast = patch->getBox().upper();
	
		//Get delta spaces into an array. dx, dy, dz.
		std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
		const double* dx  = patch_geom->getDx();
	
		//Auxiliary definitions
		int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
		int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
		for(int j = 0; j < jlast; j++) {
			for(int i = 0; i < ilast; i++) {
				if ((vector(FOV_1, i, j) > 0 || (((i + 1 < ilast && vector(FOV_1, i + 1, j) > 0) || (j + 1 < jlast && vector(FOV_1, i, j + 1) > 0)) || ((i - 1 >= 0 && vector(FOV_1, i - 1, j) > 0) || (j - 1 >= 0 && vector(FOV_1, i, j - 1) > 0))) || (d_tappering && (neighbourTappering(FOV_1, i, j, ilast, jlast, 1, 0 * d_sim_substep[ln]))))) {
					vector(ad_K_o0_t3_m0_l0_1, i, j) = vector(k1phi, i, j);
				}
			}
		}
	}
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch >& patch = *p_it;
		double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
	
		double* d_K_o0_t3_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_d_K_o0_t3_m0_l0_1_id).get())->getPointer();
		double* ad_K_o0_t3_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t3_m0_l0_1_id).get())->getPointer();
		double* ad_beta_x_o0_t0_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_beta_x_o0_t0_m0_l0_1_id).get())->getPointer();
		double* k1phi = ((pdat::NodeData<double> *) patch->getPatchData(d_k1phi_id).get())->getPointer();
		double* ad_beta_x_o0_t1_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_beta_x_o0_t1_m0_l0_1_id).get())->getPointer();
		double* ad_beta_y_o0_t0_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_beta_y_o0_t0_m0_l0_1_id).get())->getPointer();
		double* ad_beta_y_o0_t1_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_beta_y_o0_t1_m0_l0_1_id).get())->getPointer();
		double* ad_phi_o0_t0_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_phi_o0_t0_m0_l0_1_id).get())->getPointer();
		double* ad_phi_o0_t1_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_phi_o0_t1_m0_l0_1_id).get())->getPointer();
		double* ad_K_o0_t0_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t0_m0_l0_1_id).get())->getPointer();
		double* k1K = ((pdat::NodeData<double> *) patch->getPatchData(d_k1K_id).get())->getPointer();
		double* ad_K_o0_t1_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t1_m0_l0_1_id).get())->getPointer();
		double* ad_K_o0_t2_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t2_m0_l0_1_id).get())->getPointer();
		double* ad_K_o0_t4_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t4_m0_l0_1_id).get())->getPointer();
		double* ad_K_o0_t6_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t6_m0_l0_1_id).get())->getPointer();
		double* ad_K_o0_t6_m1_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t6_m1_l0_1_id).get())->getPointer();
		double* k1alpha = ((pdat::NodeData<double> *) patch->getPatchData(d_k1alpha_id).get())->getPointer();
		double* ad_K_o0_t7_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t7_m0_l0_1_id).get())->getPointer();
		double* ad_K_o0_t7_m1_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t7_m1_l0_1_id).get())->getPointer();
		double* ad_K_o0_t8_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t8_m0_l0_1_id).get())->getPointer();
		double* ad_K_o0_t8_m1_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t8_m1_l0_1_id).get())->getPointer();
		double* ad_K_o0_t9_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t9_m0_l0_1_id).get())->getPointer();
		double* ad_K_o0_t9_m1_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t9_m1_l0_1_id).get())->getPointer();
	
		//Get the dimensions of the patch
		hier::Box pbox = patch->getBox();
		const hier::Index boxfirst = patch->getBox().lower();
		const hier::Index boxlast = patch->getBox().upper();
	
		//Get delta spaces into an array. dx, dy, dz.
		std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
		const double* dx  = patch_geom->getDx();
	
		//Auxiliary definitions
		int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
		int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
		for(int j = 0; j < jlast; j++) {
			for(int i = 0; i < ilast; i++) {
				if ((vector(FOV_1, i, j) > 0 || (((i + 1 < ilast && vector(FOV_1, i + 1, j) > 0) || (i + 2 < ilast && vector(FOV_1, i + 2, j) > 0) || (j + 1 < jlast && vector(FOV_1, i, j + 1) > 0) || (j + 2 < jlast && vector(FOV_1, i, j + 2) > 0)) || ((i - 1 >= 0 && vector(FOV_1, i - 1, j) > 0) || (i - 2 >= 0 && vector(FOV_1, i - 2, j) > 0) || (j - 1 >= 0 && vector(FOV_1, i, j - 1) > 0) || (j - 2 >= 0 && vector(FOV_1, i, j - 2) > 0))) || (d_tappering && (neighbourTappering(FOV_1, i, j, ilast, jlast, 2, 0 * d_sim_substep[ln]))))) {
					vector(d_K_o0_t3_m0_l0_1, i, j) = SpatialDisc1_i(ad_K_o0_t3_m0_l0_1, i, j, dx, simPlat_dt, ilast, jlast);
					vector(ad_beta_x_o0_t0_m0_l0_1, i, j) = vector(k1phi, i, j);
					vector(ad_beta_x_o0_t1_m0_l0_1, i, j) = vector(k1phi, i, j);
					vector(ad_beta_y_o0_t0_m0_l0_1, i, j) = vector(k1phi, i, j);
					vector(ad_beta_y_o0_t1_m0_l0_1, i, j) = vector(k1phi, i, j);
					vector(ad_phi_o0_t0_m0_l0_1, i, j) = vector(k1phi, i, j);
					vector(ad_phi_o0_t1_m0_l0_1, i, j) = vector(k1phi, i, j);
					vector(ad_K_o0_t0_m0_l0_1, i, j) = vector(k1K, i, j);
					vector(ad_K_o0_t1_m0_l0_1, i, j) = vector(k1K, i, j);
					vector(ad_K_o0_t2_m0_l0_1, i, j) = vector(k1phi, i, j);
					vector(ad_K_o0_t4_m0_l0_1, i, j) = vector(k1phi, i, j);
					vector(ad_K_o0_t6_m0_l0_1, i, j) = vector(k1phi, i, j);
					vector(ad_K_o0_t6_m1_l0_1, i, j) = vector(k1alpha, i, j);
					vector(ad_K_o0_t7_m0_l0_1, i, j) = vector(k1phi, i, j);
					vector(ad_K_o0_t7_m1_l0_1, i, j) = vector(k1alpha, i, j);
					vector(ad_K_o0_t8_m0_l0_1, i, j) = vector(k1phi, i, j);
					vector(ad_K_o0_t8_m1_l0_1, i, j) = vector(k1alpha, i, j);
					vector(ad_K_o0_t9_m0_l0_1, i, j) = vector(k1phi, i, j);
					vector(ad_K_o0_t9_m1_l0_1, i, j) = vector(k1alpha, i, j);
				}
			}
		}
	}
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch >& patch = *p_it;
		double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
	
		double* ad_beta_x_o0_t0_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_beta_x_o0_t0_m0_l0_1_id).get())->getPointer();
		double* ad_beta_x_o0_t1_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_beta_x_o0_t1_m0_l0_1_id).get())->getPointer();
		double* ad_beta_y_o0_t0_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_beta_y_o0_t0_m0_l0_1_id).get())->getPointer();
		double* ad_beta_y_o0_t1_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_beta_y_o0_t1_m0_l0_1_id).get())->getPointer();
		double* ad_phi_o0_t0_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_phi_o0_t0_m0_l0_1_id).get())->getPointer();
		double* ad_phi_o0_t1_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_phi_o0_t1_m0_l0_1_id).get())->getPointer();
		double* ad_K_o0_t0_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t0_m0_l0_1_id).get())->getPointer();
		double* ad_K_o0_t1_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t1_m0_l0_1_id).get())->getPointer();
		double* ad_K_o0_t2_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t2_m0_l0_1_id).get())->getPointer();
		double* d_K_o0_t3_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_d_K_o0_t3_m0_l0_1_id).get())->getPointer();
		double* ad_K_o0_t4_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t4_m0_l0_1_id).get())->getPointer();
		double* ad_K_o0_t6_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t6_m0_l0_1_id).get())->getPointer();
		double* ad_K_o0_t6_m1_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t6_m1_l0_1_id).get())->getPointer();
		double* ad_K_o0_t7_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t7_m0_l0_1_id).get())->getPointer();
		double* ad_K_o0_t7_m1_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t7_m1_l0_1_id).get())->getPointer();
		double* ad_K_o0_t8_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t8_m0_l0_1_id).get())->getPointer();
		double* ad_K_o0_t8_m1_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t8_m1_l0_1_id).get())->getPointer();
		double* ad_K_o0_t9_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t9_m0_l0_1_id).get())->getPointer();
		double* ad_K_o0_t9_m1_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t9_m1_l0_1_id).get())->getPointer();
		double* k1alpha = ((pdat::NodeData<double> *) patch->getPatchData(d_k1alpha_id).get())->getPointer();
		double* k1K = ((pdat::NodeData<double> *) patch->getPatchData(d_k1K_id).get())->getPointer();
		double* k1phi = ((pdat::NodeData<double> *) patch->getPatchData(d_k1phi_id).get())->getPointer();
		double* k1beta_x = ((pdat::NodeData<double> *) patch->getPatchData(d_k1beta_x_id).get())->getPointer();
		double* k1beta_y = ((pdat::NodeData<double> *) patch->getPatchData(d_k1beta_y_id).get())->getPointer();
		double* k2alpha = ((pdat::NodeData<double> *) patch->getPatchData(d_k2alpha_id).get())->getPointer();
		double* alpha_p = ((pdat::NodeData<double> *) patch->getPatchData(d_alpha_p_id).get())->getPointer();
		double* k2beta_x = ((pdat::NodeData<double> *) patch->getPatchData(d_k2beta_x_id).get())->getPointer();
		double* beta_x_p = ((pdat::NodeData<double> *) patch->getPatchData(d_beta_x_p_id).get())->getPointer();
		double* k2beta_y = ((pdat::NodeData<double> *) patch->getPatchData(d_k2beta_y_id).get())->getPointer();
		double* beta_y_p = ((pdat::NodeData<double> *) patch->getPatchData(d_beta_y_p_id).get())->getPointer();
		double* k2phi = ((pdat::NodeData<double> *) patch->getPatchData(d_k2phi_id).get())->getPointer();
		double* phi_p = ((pdat::NodeData<double> *) patch->getPatchData(d_phi_p_id).get())->getPointer();
		double* k2K = ((pdat::NodeData<double> *) patch->getPatchData(d_k2K_id).get())->getPointer();
		double* K_p = ((pdat::NodeData<double> *) patch->getPatchData(d_K_p_id).get())->getPointer();
		double d_beta_x_o0_t0_m0_l0_1, d_beta_x_o0_t1_m0_l0_1, d_beta_y_o0_t0_m0_l0_1, d_beta_y_o0_t1_m0_l0_1, d_phi_o0_t0_m0_l0_1, d_phi_o0_t1_m0_l0_1, d_K_o0_t0_m0_l0_1, d_K_o0_t1_m0_l0_1, d_K_o0_t2_m0_l0_1, d_K_o0_t3_m0_l1_1, d_K_o0_t4_m0_l0_1, d_K_o0_t6_m0_l0_1, d_K_o0_t6_m1_l0_1, d_K_o0_t7_m0_l0_1, d_K_o0_t7_m1_l0_1, d_K_o0_t8_m0_l0_1, d_K_o0_t8_m1_l0_1, d_K_o0_t9_m0_l0_1, d_K_o0_t9_m1_l0_1, d_alpha_o0_t0_m0_l0_1, d_phi_o0_t2_m0_l0_1, d_K_o0_t5_m0_l0_1, m_beta_x_o0_t0_l0_1, m_beta_x_o0_t1_l0_1, m_beta_y_o0_t0_l0_1, m_beta_y_o0_t1_l0_1, m_phi_o0_t0_l0_1, m_phi_o0_t1_l0_1, m_K_o0_t0_l0_1, m_K_o0_t1_l0_1, m_K_o0_t2_l0_1, m_K_o0_t3_l1_1, m_K_o0_t4_l0_1, m_K_o0_t6_l0_1, m_K_o0_t7_l0_1, m_K_o0_t8_l0_1, m_K_o0_t9_l0_1, RHS_alpha_1, RHS_beta_x_1, RHS_beta_y_1, RHS_phi_1, RHS_K_1;
	
		//Get the dimensions of the patch
		hier::Box pbox = patch->getBox();
		const hier::Index boxfirst = patch->getBox().lower();
		const hier::Index boxlast = patch->getBox().upper();
	
		//Get delta spaces into an array. dx, dy, dz.
		std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
		const double* dx  = patch_geom->getDx();
	
		//Auxiliary definitions
		int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
		int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
		for(int j = 0; j < jlast; j++) {
			for(int i = 0; i < ilast; i++) {
				if ((vector(FOV_1, i, j) > 0)) {
					d_beta_x_o0_t0_m0_l0_1 = SpatialDisc1_i(ad_beta_x_o0_t0_m0_l0_1, i, j, dx, simPlat_dt, ilast, jlast);
					d_beta_x_o0_t1_m0_l0_1 = SpatialDisc1_j(ad_beta_x_o0_t1_m0_l0_1, i, j, dx, simPlat_dt, ilast, jlast);
					d_beta_y_o0_t0_m0_l0_1 = SpatialDisc1_i(ad_beta_y_o0_t0_m0_l0_1, i, j, dx, simPlat_dt, ilast, jlast);
					d_beta_y_o0_t1_m0_l0_1 = SpatialDisc1_j(ad_beta_y_o0_t1_m0_l0_1, i, j, dx, simPlat_dt, ilast, jlast);
					d_phi_o0_t0_m0_l0_1 = SpatialDisc1_i(ad_phi_o0_t0_m0_l0_1, i, j, dx, simPlat_dt, ilast, jlast);
					d_phi_o0_t1_m0_l0_1 = SpatialDisc1_j(ad_phi_o0_t1_m0_l0_1, i, j, dx, simPlat_dt, ilast, jlast);
					d_K_o0_t0_m0_l0_1 = SpatialDisc1_i(ad_K_o0_t0_m0_l0_1, i, j, dx, simPlat_dt, ilast, jlast);
					d_K_o0_t1_m0_l0_1 = SpatialDisc1_j(ad_K_o0_t1_m0_l0_1, i, j, dx, simPlat_dt, ilast, jlast);
					d_K_o0_t2_m0_l0_1 = SpatialDisc2_i(ad_K_o0_t2_m0_l0_1, i, j, dx, simPlat_dt, ilast, jlast);
					d_K_o0_t3_m0_l1_1 = SpatialDisc1_j(d_K_o0_t3_m0_l0_1, i, j, dx, simPlat_dt, ilast, jlast);
					d_K_o0_t4_m0_l0_1 = SpatialDisc2_j(ad_K_o0_t4_m0_l0_1, i, j, dx, simPlat_dt, ilast, jlast);
					d_K_o0_t6_m0_l0_1 = SpatialDisc1_i(ad_K_o0_t6_m0_l0_1, i, j, dx, simPlat_dt, ilast, jlast);
					d_K_o0_t6_m1_l0_1 = SpatialDisc1_i(ad_K_o0_t6_m1_l0_1, i, j, dx, simPlat_dt, ilast, jlast);
					d_K_o0_t7_m0_l0_1 = SpatialDisc1_j(ad_K_o0_t7_m0_l0_1, i, j, dx, simPlat_dt, ilast, jlast);
					d_K_o0_t7_m1_l0_1 = SpatialDisc1_i(ad_K_o0_t7_m1_l0_1, i, j, dx, simPlat_dt, ilast, jlast);
					d_K_o0_t8_m0_l0_1 = SpatialDisc1_i(ad_K_o0_t8_m0_l0_1, i, j, dx, simPlat_dt, ilast, jlast);
					d_K_o0_t8_m1_l0_1 = SpatialDisc1_j(ad_K_o0_t8_m1_l0_1, i, j, dx, simPlat_dt, ilast, jlast);
					d_K_o0_t9_m0_l0_1 = SpatialDisc1_j(ad_K_o0_t9_m0_l0_1, i, j, dx, simPlat_dt, ilast, jlast);
					d_K_o0_t9_m1_l0_1 = SpatialDisc1_j(ad_K_o0_t9_m1_l0_1, i, j, dx, simPlat_dt, ilast, jlast);
					d_alpha_o0_t0_m0_l0_1 = -gauge_evol * vector(k1alpha, i, j);
					d_phi_o0_t2_m0_l0_1 = -vector(k1alpha, i, j) * vector(k1K, i, j);
					d_K_o0_t5_m0_l0_1 = vector(k1alpha, i, j) * (m * m) * vector(k1phi, i, j);
					m_beta_x_o0_t0_l0_1 = (-gauge_evol * vector(k1alpha, i, j) * a) * d_beta_x_o0_t0_m0_l0_1;
					m_beta_x_o0_t1_l0_1 = (-gauge_evol * vector(k1alpha, i, j) * b) * d_beta_x_o0_t1_m0_l0_1;
					m_beta_y_o0_t0_l0_1 = (-gauge_evol * vector(k1alpha, i, j) * b) * d_beta_y_o0_t0_m0_l0_1;
					m_beta_y_o0_t1_l0_1 = (-gauge_evol * vector(k1alpha, i, j) * a) * d_beta_y_o0_t1_m0_l0_1;
					m_phi_o0_t0_l0_1 = vector(k1beta_x, i, j) * d_phi_o0_t0_m0_l0_1;
					m_phi_o0_t1_l0_1 = vector(k1beta_y, i, j) * d_phi_o0_t1_m0_l0_1;
					m_K_o0_t0_l0_1 = vector(k1beta_x, i, j) * d_K_o0_t0_m0_l0_1;
					m_K_o0_t1_l0_1 = vector(k1beta_y, i, j) * d_K_o0_t1_m0_l0_1;
					m_K_o0_t2_l0_1 = (-vector(k1alpha, i, j) * a) * d_K_o0_t2_m0_l0_1;
					m_K_o0_t3_l1_1 = (-2.0 * vector(k1alpha, i, j) * b) * d_K_o0_t3_m0_l1_1;
					m_K_o0_t4_l0_1 = (-vector(k1alpha, i, j) * a) * d_K_o0_t4_m0_l0_1;
					m_K_o0_t6_l0_1 = (-a) * d_K_o0_t6_m0_l0_1 * d_K_o0_t6_m1_l0_1;
					m_K_o0_t7_l0_1 = (-b) * d_K_o0_t7_m0_l0_1 * d_K_o0_t7_m1_l0_1;
					m_K_o0_t8_l0_1 = (-b) * d_K_o0_t8_m0_l0_1 * d_K_o0_t8_m1_l0_1;
					m_K_o0_t9_l0_1 = (-a) * d_K_o0_t9_m0_l0_1 * d_K_o0_t9_m1_l0_1;
					RHS_alpha_1 = d_alpha_o0_t0_m0_l0_1;
					RHS_beta_x_1 = m_beta_x_o0_t0_l0_1 + m_beta_x_o0_t1_l0_1;
					RHS_beta_y_1 = m_beta_y_o0_t0_l0_1 + m_beta_y_o0_t1_l0_1;
					RHS_phi_1 = (m_phi_o0_t0_l0_1 + m_phi_o0_t1_l0_1) + d_phi_o0_t2_m0_l0_1;
					RHS_K_1 = ((((((((m_K_o0_t0_l0_1 + m_K_o0_t1_l0_1) + m_K_o0_t2_l0_1) + m_K_o0_t3_l1_1) + m_K_o0_t4_l0_1) + d_K_o0_t5_m0_l0_1) + m_K_o0_t6_l0_1) + m_K_o0_t7_l0_1) + m_K_o0_t8_l0_1) + m_K_o0_t9_l0_1;
					vector(k2alpha, i, j) = timeDisc4_(RHS_alpha_1, alpha_p, k1alpha, i, j, dx, simPlat_dt, ilast, jlast);
					vector(k2beta_x, i, j) = timeDisc4_(RHS_beta_x_1, beta_x_p, k1beta_x, i, j, dx, simPlat_dt, ilast, jlast);
					vector(k2beta_y, i, j) = timeDisc4_(RHS_beta_y_1, beta_y_p, k1beta_y, i, j, dx, simPlat_dt, ilast, jlast);
					vector(k2phi, i, j) = timeDisc4_(RHS_phi_1, phi_p, k1phi, i, j, dx, simPlat_dt, ilast, jlast);
					vector(k2K, i, j) = timeDisc4_(RHS_K_1, K_p, k1K, i, j, dx, simPlat_dt, ilast, jlast);
				}
			}
		}
	}
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch >& patch = *p_it;
		double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
	
		double* ad_K_o0_t3_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t3_m0_l0_1_id).get())->getPointer();
		double* k2phi = ((pdat::NodeData<double> *) patch->getPatchData(d_k2phi_id).get())->getPointer();
	
		//Get the dimensions of the patch
		hier::Box pbox = patch->getBox();
		const hier::Index boxfirst = patch->getBox().lower();
		const hier::Index boxlast = patch->getBox().upper();
	
		//Get delta spaces into an array. dx, dy, dz.
		std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
		const double* dx  = patch_geom->getDx();
	
		//Auxiliary definitions
		int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
		int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
		for(int j = 0; j < jlast; j++) {
			for(int i = 0; i < ilast; i++) {
				if ((vector(FOV_1, i, j) > 0 || (((i + 1 < ilast && vector(FOV_1, i + 1, j) > 0) || (j + 1 < jlast && vector(FOV_1, i, j + 1) > 0)) || ((i - 1 >= 0 && vector(FOV_1, i - 1, j) > 0) || (j - 1 >= 0 && vector(FOV_1, i, j - 1) > 0))) || (d_tappering && (neighbourTappering(FOV_1, i, j, ilast, jlast, 1, 0 * d_sim_substep[ln]))))) {
					vector(ad_K_o0_t3_m0_l0_1, i, j) = vector(k2phi, i, j);
				}
			}
		}
	}
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch >& patch = *p_it;
		double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
	
		double* d_K_o0_t3_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_d_K_o0_t3_m0_l0_1_id).get())->getPointer();
		double* ad_K_o0_t3_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t3_m0_l0_1_id).get())->getPointer();
		double* ad_beta_x_o0_t0_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_beta_x_o0_t0_m0_l0_1_id).get())->getPointer();
		double* k2phi = ((pdat::NodeData<double> *) patch->getPatchData(d_k2phi_id).get())->getPointer();
		double* ad_beta_x_o0_t1_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_beta_x_o0_t1_m0_l0_1_id).get())->getPointer();
		double* ad_beta_y_o0_t0_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_beta_y_o0_t0_m0_l0_1_id).get())->getPointer();
		double* ad_beta_y_o0_t1_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_beta_y_o0_t1_m0_l0_1_id).get())->getPointer();
		double* ad_phi_o0_t0_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_phi_o0_t0_m0_l0_1_id).get())->getPointer();
		double* ad_phi_o0_t1_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_phi_o0_t1_m0_l0_1_id).get())->getPointer();
		double* ad_K_o0_t0_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t0_m0_l0_1_id).get())->getPointer();
		double* k2K = ((pdat::NodeData<double> *) patch->getPatchData(d_k2K_id).get())->getPointer();
		double* ad_K_o0_t1_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t1_m0_l0_1_id).get())->getPointer();
		double* ad_K_o0_t2_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t2_m0_l0_1_id).get())->getPointer();
		double* ad_K_o0_t4_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t4_m0_l0_1_id).get())->getPointer();
		double* ad_K_o0_t6_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t6_m0_l0_1_id).get())->getPointer();
		double* ad_K_o0_t6_m1_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t6_m1_l0_1_id).get())->getPointer();
		double* k2alpha = ((pdat::NodeData<double> *) patch->getPatchData(d_k2alpha_id).get())->getPointer();
		double* ad_K_o0_t7_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t7_m0_l0_1_id).get())->getPointer();
		double* ad_K_o0_t7_m1_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t7_m1_l0_1_id).get())->getPointer();
		double* ad_K_o0_t8_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t8_m0_l0_1_id).get())->getPointer();
		double* ad_K_o0_t8_m1_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t8_m1_l0_1_id).get())->getPointer();
		double* ad_K_o0_t9_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t9_m0_l0_1_id).get())->getPointer();
		double* ad_K_o0_t9_m1_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t9_m1_l0_1_id).get())->getPointer();
	
		//Get the dimensions of the patch
		hier::Box pbox = patch->getBox();
		const hier::Index boxfirst = patch->getBox().lower();
		const hier::Index boxlast = patch->getBox().upper();
	
		//Get delta spaces into an array. dx, dy, dz.
		std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
		const double* dx  = patch_geom->getDx();
	
		//Auxiliary definitions
		int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
		int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
		for(int j = 0; j < jlast; j++) {
			for(int i = 0; i < ilast; i++) {
				if ((vector(FOV_1, i, j) > 0 || (((i + 1 < ilast && vector(FOV_1, i + 1, j) > 0) || (i + 2 < ilast && vector(FOV_1, i + 2, j) > 0) || (j + 1 < jlast && vector(FOV_1, i, j + 1) > 0) || (j + 2 < jlast && vector(FOV_1, i, j + 2) > 0)) || ((i - 1 >= 0 && vector(FOV_1, i - 1, j) > 0) || (i - 2 >= 0 && vector(FOV_1, i - 2, j) > 0) || (j - 1 >= 0 && vector(FOV_1, i, j - 1) > 0) || (j - 2 >= 0 && vector(FOV_1, i, j - 2) > 0))) || (d_tappering && (neighbourTappering(FOV_1, i, j, ilast, jlast, 2, 0 * d_sim_substep[ln]))))) {
					vector(d_K_o0_t3_m0_l0_1, i, j) = SpatialDisc1_i(ad_K_o0_t3_m0_l0_1, i, j, dx, simPlat_dt, ilast, jlast);
					vector(ad_beta_x_o0_t0_m0_l0_1, i, j) = vector(k2phi, i, j);
					vector(ad_beta_x_o0_t1_m0_l0_1, i, j) = vector(k2phi, i, j);
					vector(ad_beta_y_o0_t0_m0_l0_1, i, j) = vector(k2phi, i, j);
					vector(ad_beta_y_o0_t1_m0_l0_1, i, j) = vector(k2phi, i, j);
					vector(ad_phi_o0_t0_m0_l0_1, i, j) = vector(k2phi, i, j);
					vector(ad_phi_o0_t1_m0_l0_1, i, j) = vector(k2phi, i, j);
					vector(ad_K_o0_t0_m0_l0_1, i, j) = vector(k2K, i, j);
					vector(ad_K_o0_t1_m0_l0_1, i, j) = vector(k2K, i, j);
					vector(ad_K_o0_t2_m0_l0_1, i, j) = vector(k2phi, i, j);
					vector(ad_K_o0_t4_m0_l0_1, i, j) = vector(k2phi, i, j);
					vector(ad_K_o0_t6_m0_l0_1, i, j) = vector(k2phi, i, j);
					vector(ad_K_o0_t6_m1_l0_1, i, j) = vector(k2alpha, i, j);
					vector(ad_K_o0_t7_m0_l0_1, i, j) = vector(k2phi, i, j);
					vector(ad_K_o0_t7_m1_l0_1, i, j) = vector(k2alpha, i, j);
					vector(ad_K_o0_t8_m0_l0_1, i, j) = vector(k2phi, i, j);
					vector(ad_K_o0_t8_m1_l0_1, i, j) = vector(k2alpha, i, j);
					vector(ad_K_o0_t9_m0_l0_1, i, j) = vector(k2phi, i, j);
					vector(ad_K_o0_t9_m1_l0_1, i, j) = vector(k2alpha, i, j);
				}
			}
		}
	}
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch >& patch = *p_it;
		double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
	
		double* ad_beta_x_o0_t0_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_beta_x_o0_t0_m0_l0_1_id).get())->getPointer();
		double* ad_beta_x_o0_t1_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_beta_x_o0_t1_m0_l0_1_id).get())->getPointer();
		double* ad_beta_y_o0_t0_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_beta_y_o0_t0_m0_l0_1_id).get())->getPointer();
		double* ad_beta_y_o0_t1_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_beta_y_o0_t1_m0_l0_1_id).get())->getPointer();
		double* ad_phi_o0_t0_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_phi_o0_t0_m0_l0_1_id).get())->getPointer();
		double* ad_phi_o0_t1_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_phi_o0_t1_m0_l0_1_id).get())->getPointer();
		double* ad_K_o0_t0_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t0_m0_l0_1_id).get())->getPointer();
		double* ad_K_o0_t1_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t1_m0_l0_1_id).get())->getPointer();
		double* ad_K_o0_t2_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t2_m0_l0_1_id).get())->getPointer();
		double* d_K_o0_t3_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_d_K_o0_t3_m0_l0_1_id).get())->getPointer();
		double* ad_K_o0_t4_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t4_m0_l0_1_id).get())->getPointer();
		double* ad_K_o0_t6_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t6_m0_l0_1_id).get())->getPointer();
		double* ad_K_o0_t6_m1_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t6_m1_l0_1_id).get())->getPointer();
		double* ad_K_o0_t7_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t7_m0_l0_1_id).get())->getPointer();
		double* ad_K_o0_t7_m1_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t7_m1_l0_1_id).get())->getPointer();
		double* ad_K_o0_t8_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t8_m0_l0_1_id).get())->getPointer();
		double* ad_K_o0_t8_m1_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t8_m1_l0_1_id).get())->getPointer();
		double* ad_K_o0_t9_m0_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t9_m0_l0_1_id).get())->getPointer();
		double* ad_K_o0_t9_m1_l0_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ad_K_o0_t9_m1_l0_1_id).get())->getPointer();
		double* k2alpha = ((pdat::NodeData<double> *) patch->getPatchData(d_k2alpha_id).get())->getPointer();
		double* k2K = ((pdat::NodeData<double> *) patch->getPatchData(d_k2K_id).get())->getPointer();
		double* k2phi = ((pdat::NodeData<double> *) patch->getPatchData(d_k2phi_id).get())->getPointer();
		double* k2beta_x = ((pdat::NodeData<double> *) patch->getPatchData(d_k2beta_x_id).get())->getPointer();
		double* k2beta_y = ((pdat::NodeData<double> *) patch->getPatchData(d_k2beta_y_id).get())->getPointer();
		double* alpha = ((pdat::NodeData<double> *) patch->getPatchData(d_alpha_id).get())->getPointer();
		double* alpha_p = ((pdat::NodeData<double> *) patch->getPatchData(d_alpha_p_id).get())->getPointer();
		double* beta_x = ((pdat::NodeData<double> *) patch->getPatchData(d_beta_x_id).get())->getPointer();
		double* beta_x_p = ((pdat::NodeData<double> *) patch->getPatchData(d_beta_x_p_id).get())->getPointer();
		double* beta_y = ((pdat::NodeData<double> *) patch->getPatchData(d_beta_y_id).get())->getPointer();
		double* beta_y_p = ((pdat::NodeData<double> *) patch->getPatchData(d_beta_y_p_id).get())->getPointer();
		double* phi = ((pdat::NodeData<double> *) patch->getPatchData(d_phi_id).get())->getPointer();
		double* phi_p = ((pdat::NodeData<double> *) patch->getPatchData(d_phi_p_id).get())->getPointer();
		double* K = ((pdat::NodeData<double> *) patch->getPatchData(d_K_id).get())->getPointer();
		double* K_p = ((pdat::NodeData<double> *) patch->getPatchData(d_K_p_id).get())->getPointer();
		double d_beta_x_o0_t0_m0_l0_1, d_beta_x_o0_t1_m0_l0_1, d_beta_y_o0_t0_m0_l0_1, d_beta_y_o0_t1_m0_l0_1, d_phi_o0_t0_m0_l0_1, d_phi_o0_t1_m0_l0_1, d_K_o0_t0_m0_l0_1, d_K_o0_t1_m0_l0_1, d_K_o0_t2_m0_l0_1, d_K_o0_t3_m0_l1_1, d_K_o0_t4_m0_l0_1, d_K_o0_t6_m0_l0_1, d_K_o0_t6_m1_l0_1, d_K_o0_t7_m0_l0_1, d_K_o0_t7_m1_l0_1, d_K_o0_t8_m0_l0_1, d_K_o0_t8_m1_l0_1, d_K_o0_t9_m0_l0_1, d_K_o0_t9_m1_l0_1, d_alpha_o0_t0_m0_l0_1, d_phi_o0_t2_m0_l0_1, d_K_o0_t5_m0_l0_1, m_beta_x_o0_t0_l0_1, m_beta_x_o0_t1_l0_1, m_beta_y_o0_t0_l0_1, m_beta_y_o0_t1_l0_1, m_phi_o0_t0_l0_1, m_phi_o0_t1_l0_1, m_K_o0_t0_l0_1, m_K_o0_t1_l0_1, m_K_o0_t2_l0_1, m_K_o0_t3_l1_1, m_K_o0_t4_l0_1, m_K_o0_t6_l0_1, m_K_o0_t7_l0_1, m_K_o0_t8_l0_1, m_K_o0_t9_l0_1, RHS_alpha_1, RHS_beta_x_1, RHS_beta_y_1, RHS_phi_1, RHS_K_1;
	
		//Get the dimensions of the patch
		hier::Box pbox = patch->getBox();
		const hier::Index boxfirst = patch->getBox().lower();
		const hier::Index boxlast = patch->getBox().upper();
	
		//Get delta spaces into an array. dx, dy, dz.
		std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
		const double* dx  = patch_geom->getDx();
	
		//Auxiliary definitions
		int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
		int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
		for(int j = 0; j < jlast; j++) {
			for(int i = 0; i < ilast; i++) {
				if ((vector(FOV_1, i, j) > 0)) {
					d_beta_x_o0_t0_m0_l0_1 = SpatialDisc1_i(ad_beta_x_o0_t0_m0_l0_1, i, j, dx, simPlat_dt, ilast, jlast);
					d_beta_x_o0_t1_m0_l0_1 = SpatialDisc1_j(ad_beta_x_o0_t1_m0_l0_1, i, j, dx, simPlat_dt, ilast, jlast);
					d_beta_y_o0_t0_m0_l0_1 = SpatialDisc1_i(ad_beta_y_o0_t0_m0_l0_1, i, j, dx, simPlat_dt, ilast, jlast);
					d_beta_y_o0_t1_m0_l0_1 = SpatialDisc1_j(ad_beta_y_o0_t1_m0_l0_1, i, j, dx, simPlat_dt, ilast, jlast);
					d_phi_o0_t0_m0_l0_1 = SpatialDisc1_i(ad_phi_o0_t0_m0_l0_1, i, j, dx, simPlat_dt, ilast, jlast);
					d_phi_o0_t1_m0_l0_1 = SpatialDisc1_j(ad_phi_o0_t1_m0_l0_1, i, j, dx, simPlat_dt, ilast, jlast);
					d_K_o0_t0_m0_l0_1 = SpatialDisc1_i(ad_K_o0_t0_m0_l0_1, i, j, dx, simPlat_dt, ilast, jlast);
					d_K_o0_t1_m0_l0_1 = SpatialDisc1_j(ad_K_o0_t1_m0_l0_1, i, j, dx, simPlat_dt, ilast, jlast);
					d_K_o0_t2_m0_l0_1 = SpatialDisc2_i(ad_K_o0_t2_m0_l0_1, i, j, dx, simPlat_dt, ilast, jlast);
					d_K_o0_t3_m0_l1_1 = SpatialDisc1_j(d_K_o0_t3_m0_l0_1, i, j, dx, simPlat_dt, ilast, jlast);
					d_K_o0_t4_m0_l0_1 = SpatialDisc2_j(ad_K_o0_t4_m0_l0_1, i, j, dx, simPlat_dt, ilast, jlast);
					d_K_o0_t6_m0_l0_1 = SpatialDisc1_i(ad_K_o0_t6_m0_l0_1, i, j, dx, simPlat_dt, ilast, jlast);
					d_K_o0_t6_m1_l0_1 = SpatialDisc1_i(ad_K_o0_t6_m1_l0_1, i, j, dx, simPlat_dt, ilast, jlast);
					d_K_o0_t7_m0_l0_1 = SpatialDisc1_j(ad_K_o0_t7_m0_l0_1, i, j, dx, simPlat_dt, ilast, jlast);
					d_K_o0_t7_m1_l0_1 = SpatialDisc1_i(ad_K_o0_t7_m1_l0_1, i, j, dx, simPlat_dt, ilast, jlast);
					d_K_o0_t8_m0_l0_1 = SpatialDisc1_i(ad_K_o0_t8_m0_l0_1, i, j, dx, simPlat_dt, ilast, jlast);
					d_K_o0_t8_m1_l0_1 = SpatialDisc1_j(ad_K_o0_t8_m1_l0_1, i, j, dx, simPlat_dt, ilast, jlast);
					d_K_o0_t9_m0_l0_1 = SpatialDisc1_j(ad_K_o0_t9_m0_l0_1, i, j, dx, simPlat_dt, ilast, jlast);
					d_K_o0_t9_m1_l0_1 = SpatialDisc1_j(ad_K_o0_t9_m1_l0_1, i, j, dx, simPlat_dt, ilast, jlast);
					d_alpha_o0_t0_m0_l0_1 = -gauge_evol * vector(k2alpha, i, j);
					d_phi_o0_t2_m0_l0_1 = -vector(k2alpha, i, j) * vector(k2K, i, j);
					d_K_o0_t5_m0_l0_1 = vector(k2alpha, i, j) * (m * m) * vector(k2phi, i, j);
					m_beta_x_o0_t0_l0_1 = (-gauge_evol * vector(k2alpha, i, j) * a) * d_beta_x_o0_t0_m0_l0_1;
					m_beta_x_o0_t1_l0_1 = (-gauge_evol * vector(k2alpha, i, j) * b) * d_beta_x_o0_t1_m0_l0_1;
					m_beta_y_o0_t0_l0_1 = (-gauge_evol * vector(k2alpha, i, j) * b) * d_beta_y_o0_t0_m0_l0_1;
					m_beta_y_o0_t1_l0_1 = (-gauge_evol * vector(k2alpha, i, j) * a) * d_beta_y_o0_t1_m0_l0_1;
					m_phi_o0_t0_l0_1 = vector(k2beta_x, i, j) * d_phi_o0_t0_m0_l0_1;
					m_phi_o0_t1_l0_1 = vector(k2beta_y, i, j) * d_phi_o0_t1_m0_l0_1;
					m_K_o0_t0_l0_1 = vector(k2beta_x, i, j) * d_K_o0_t0_m0_l0_1;
					m_K_o0_t1_l0_1 = vector(k2beta_y, i, j) * d_K_o0_t1_m0_l0_1;
					m_K_o0_t2_l0_1 = (-vector(k2alpha, i, j) * a) * d_K_o0_t2_m0_l0_1;
					m_K_o0_t3_l1_1 = (-2.0 * vector(k2alpha, i, j) * b) * d_K_o0_t3_m0_l1_1;
					m_K_o0_t4_l0_1 = (-vector(k2alpha, i, j) * a) * d_K_o0_t4_m0_l0_1;
					m_K_o0_t6_l0_1 = (-a) * d_K_o0_t6_m0_l0_1 * d_K_o0_t6_m1_l0_1;
					m_K_o0_t7_l0_1 = (-b) * d_K_o0_t7_m0_l0_1 * d_K_o0_t7_m1_l0_1;
					m_K_o0_t8_l0_1 = (-b) * d_K_o0_t8_m0_l0_1 * d_K_o0_t8_m1_l0_1;
					m_K_o0_t9_l0_1 = (-a) * d_K_o0_t9_m0_l0_1 * d_K_o0_t9_m1_l0_1;
					RHS_alpha_1 = d_alpha_o0_t0_m0_l0_1;
					RHS_beta_x_1 = m_beta_x_o0_t0_l0_1 + m_beta_x_o0_t1_l0_1;
					RHS_beta_y_1 = m_beta_y_o0_t0_l0_1 + m_beta_y_o0_t1_l0_1;
					RHS_phi_1 = (m_phi_o0_t0_l0_1 + m_phi_o0_t1_l0_1) + d_phi_o0_t2_m0_l0_1;
					RHS_K_1 = ((((((((m_K_o0_t0_l0_1 + m_K_o0_t1_l0_1) + m_K_o0_t2_l0_1) + m_K_o0_t3_l1_1) + m_K_o0_t4_l0_1) + d_K_o0_t5_m0_l0_1) + m_K_o0_t6_l0_1) + m_K_o0_t7_l0_1) + m_K_o0_t8_l0_1) + m_K_o0_t9_l0_1;
					vector(alpha, i, j) = timeDisc5_(RHS_alpha_1, alpha_p, k2alpha, i, j, dx, simPlat_dt, ilast, jlast);
					vector(beta_x, i, j) = timeDisc5_(RHS_beta_x_1, beta_x_p, k2beta_x, i, j, dx, simPlat_dt, ilast, jlast);
					vector(beta_y, i, j) = timeDisc5_(RHS_beta_y_1, beta_y_p, k2beta_y, i, j, dx, simPlat_dt, ilast, jlast);
					vector(phi, i, j) = timeDisc5_(RHS_phi_1, phi_p, k2phi, i, j, dx, simPlat_dt, ilast, jlast);
					vector(K, i, j) = timeDisc5_(RHS_K_1, K_p, k2K, i, j, dx, simPlat_dt, ilast, jlast);
				}
			}
		}
	}
	

	t_step->stop();


	return simPlat_dt;
}

/*
 * Checks the finalization conditions              
 */
bool Problem::checkFinalization(const double current_time, const double simPlat_dt)
{
	if (greaterEq(current_time, tend)) { 
		return true;
	}
	return false;
	
	

}


bool Problem::neighbourTappering(const double* fov, const int ibase, const int jbase, const int ilast, const int jlast, const int stencil, const int limit) const {
	for(int j = stencil + 1; j <= limit; j++) {
		for(int i = stencil + 1; i <= limit; i++) {
			if ((ibase + i >= d_ghost_width && ibase + i < ilast - d_ghost_width && jbase + j >= d_ghost_width && jbase + j < jlast - d_ghost_width && vector(fov, ibase + i, jbase + j) > 0) || (ibase + i >= d_ghost_width && ibase + i < ilast - d_ghost_width && jbase - j >= d_ghost_width && jbase - j < jlast - d_ghost_width && vector(fov, ibase + i, jbase - j) > 0) || (ibase - i >= d_ghost_width && ibase - i < ilast - d_ghost_width && jbase + j >= d_ghost_width && jbase + j < jlast - d_ghost_width && vector(fov, ibase - i, jbase + j) > 0) || (ibase - i >= d_ghost_width && ibase - i < ilast - d_ghost_width && jbase - j >= d_ghost_width && jbase - j < jlast - d_ghost_width && vector(fov, ibase - i, jbase - j) > 0)) {
				return true;
			}
		}
	}
	return false;
}


/*
 *  Cell tagging routine - tag cells that require refinement based on a provided condition. 
 */
void Problem::applyGradientDetector(
   const std::shared_ptr< hier::PatchHierarchy >& hierarchy, 
   const int level_number,
   const double time, 
   const int tag_index,
   const bool initial_time,
   const bool uses_richardson_extrapolation_too) 
{
	if (d_regridding && level_number + 1 >= d_regridding_level_threshold) {
		hier::VariableDatabase *vdb = hier::VariableDatabase::getDatabase();
		if (!(vdb->checkVariableExists(d_regridding_field))) {
			TBOX_ERROR(d_object_name << ": Regridding field selected not found:n"					<<  d_regridding_field<<  "n");
		}

		std::shared_ptr< hier::PatchLevel > level(hierarchy->getPatchLevel(level_number));

		for (hier::PatchLevel::iterator ip(level->begin()); ip != level->end(); ++ip) {
			const std::shared_ptr< hier::Patch >& patch = *ip;
			int* tags = ((pdat::NodeData<int> *) patch->getPatchData(tag_index).get())->getPointer();
			const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
			const double* dx  = patch_geom->getDx();
			const hier::Index tfirst = patch->getPatchData(tag_index)->getGhostBox().lower();
			const hier::Index tlast  = patch->getPatchData(tag_index)->getGhostBox().upper();
			const hier::Index boxfirst = patch->getBox().lower();
			const hier::Index boxlast  = patch->getBox().upper();
			int ilast = boxlast(0)-boxfirst(0)+2+2*d_ghost_width;
			int itlast = tlast(0)-tfirst(0)+1;
			int jlast = boxlast(1)-boxfirst(1)+2+2*d_ghost_width;
			int jtlast = tlast(1)-tfirst(1)+1;
			if (d_regridding_type == "GRADIENT") {
				int regrid_field_id = vdb->getVariable(d_regridding_field)->getInstanceIdentifier();
				double* regrid_field = ((pdat::NodeData<double> *) patch->getPatchData(regrid_field_id).get())
->getPointer();
				for(int index1 = 0; index1 < (tlast(1)-tfirst(1))+1; index1++) {
					for(int index0 = 0; index0 < (tlast(0)-tfirst(0))+1; index0++) {
						vectorT(tags,index0,index1) = 0;
						if (vector(regrid_field, index0+d_ghost_width,index1+d_ghost_width)!=0) {
							if ((fabs(vector(regrid_field, index0+1+d_ghost_width, index1+d_ghost_width)-vector(regrid_field, index0+d_ghost_width, index1+d_ghost_width)) > d_regridding_compressionFactor * MIN(fabs(vector(regrid_field, index0+2+d_ghost_width, index1+d_ghost_width)-vector(regrid_field, index0+1+d_ghost_width, index1+d_ghost_width)) + d_regridding_mOffset * pow(dx[0], 2), fabs(vector(regrid_field, index0+d_ghost_width, index1+d_ghost_width)-vector(regrid_field, index0-1+d_ghost_width, index1+d_ghost_width)) + d_regridding_mOffset * pow(dx[0], 2)) ) ||(fabs(vector(regrid_field, index0+d_ghost_width, index1+1+d_ghost_width)-vector(regrid_field, index0+d_ghost_width, index1+d_ghost_width)) > d_regridding_compressionFactor * MIN(fabs(vector(regrid_field, index0+d_ghost_width, index1+2+d_ghost_width)-vector(regrid_field, index0+d_ghost_width, index1+1+d_ghost_width)) + d_regridding_mOffset * pow(dx[1], 2), fabs(vector(regrid_field, index0+d_ghost_width, index1+d_ghost_width)-vector(regrid_field, index0+d_ghost_width, index1-1+d_ghost_width)) + d_regridding_mOffset * pow(dx[1], 2)) ) ) {
								vectorT(tags,index0,index1) = 1;
							}
						}
					}
				}
			} else {
				if (d_regridding_type == "FUNCTION") {
					int regrid_field_id = vdb->getVariable(d_regridding_field)->getInstanceIdentifier();
					double* regrid_field = ((pdat::NodeData<double> *) patch->getPatchData(regrid_field_id).get())
	->getPointer();
					for(int index1 = 0; index1 < (tlast(1)-tfirst(1))+1; index1++) {
						for(int index0 = 0; index0 < (tlast(0)-tfirst(0))+1; index0++) {
							vectorT(tags,index0,index1) = 0;
							if (vector(regrid_field, index0+d_ghost_width,index1+d_ghost_width) > d_regridding_threshold) {
								vectorT(tags,index0,index1) = 1;
							}
						}
					}
	
				}
			}
		}
	}
}



