#include "Problem.h"
#include "Functions.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <stack>
#include "hdf5.h"
#include <time.h>
#include <silo.h>
#include <algorithm>
#include "float.h"
#include "NonSync.h"
#include "NonSyncs.h"
#include "SAMRAI/pdat/IndexVariable.h"
#include "SAMRAI/pdat/CellData.h"
#include "SAMRAI/pdat/CellVariable.h"
#include <gsl/gsl_rng.h>


#include "SAMRAI/tbox/Array.h"
#include "SAMRAI/hier/BoundaryBox.h"
#include "SAMRAI/hier/BoxContainer.h"
#include "SAMRAI/geom/CartesianPatchGeometry.h"
#include "SAMRAI/hier/VariableDatabase.h"
#include "SAMRAI/hier/PatchDataRestartManager.h"
#include "SAMRAI/hier/Index.h"
#include "SAMRAI/tbox/PIO.h"
#include "SAMRAI/tbox/Utilities.h"
#include "SAMRAI/tbox/Timer.h"
#include "SAMRAI/tbox/TimerManager.h"

#define MIN(X,Y) ((X) < (Y) ? (X) : (Y))
#define MAX(X,Y) ((X) > (Y) ? (X) : (Y))
#define SIGN(X) (((X) > 0) - ((X) < 0))
#define isEven(a) ((a) % 2 == 0 ? true : false)
#define greaterThan(a,b) (!((fabs((a) - (b))/1.0E-9 > 10 ? false: (floor(fabs((a) - (b))/1.0E-9) < 1)) || (a)<(b)))
#define lessThan(a,b) (!((fabs((a) - (b))/1.0E-9 > 10 ? false : (floor(fabs((a) - (b))/1.0E-9) < 1)) || (b)<(a)))
#define greaterEq(a,b) ((fabs((a) - (b))/1.0E-9 > 10 ? false : (floor(fabs((a) - (b))/1.0E-9) < 1)) || (b)<(a))
#define lessEq(a,b) ((fabs((a) - (b))/1.0E-9 > 10 ? false: (floor(fabs((a) - (b))/1.0E-9) < 1)) || (a)<(b))
#define equalsEq(a,b) ((fabs((a) - (b))/1.0E-9 > 10 ? false: (floor(fabs((a) - (b))/1.0E-9) < 1)))

#define vector(v, i, j) (v)[i+xlast*(j)]
#define vectorT(v, i, j) (v)[i+xtlast*(j)]
#define indexxOf(i) (i - ((int)(i/(boxlast(0)-boxfirst(0)+1))*(boxlast(0)-boxfirst(0)+1)))
#define indexyOf(i) ((int)(i/(boxlast(0)-boxfirst(0)+1)))




//Problem index box, patch geometry and deltas
const double* dx;
const gsl_rng_type * T;
gsl_rng *r_var;
gsl_rng *r_map;

//Timers
std::shared_ptr<tbox::Timer> t_step;
std::shared_ptr<tbox::Timer> t_moveParticles;

inline int Problem::GetExpoBase2(double d)
{
	int i = 0;
	((short *)(&i))[0] = (((short *)(&d))[3] & (short)32752); // _123456789ab____ & 0111111111110000
	return (i >> 4) - 1023;
}

bool	Problem::Equals(double d1, double d2)
{
	if (d1 == d2)
		return true;
	int e1 = GetExpoBase2(d1);
	int e2 = GetExpoBase2(d2);
	int e3 = GetExpoBase2(d1 - d2);
	if ((e3 - e2 < -48) && (e3 - e1 < -48))
		return true;
	return false;
}

/*
 * Constructor of the problem.
 */
Problem::Problem(const string& object_name, const tbox::Dimension& dim, std::shared_ptr<tbox::Database>& database, std::shared_ptr<geom::CartesianGridGeometry >& grid_geom, std::shared_ptr<hier::PatchHierarchy >& patch_hierarchy, const double dt, hier::BoxContainer& ba, const bool init_from_files, const vector<string> full_writer_variables): 
d_dim(dim), xfer::RefinePatchStrategy(), xfer::CoarsenPatchStrategy(), d_regridding_boxes(ba), d_full_writer_variables(full_writer_variables.begin(), full_writer_variables.end())
{
	//Setup the timers
	t_step = tbox::TimerManager::getManager()->getTimer("Step");
	t_moveParticles = tbox::TimerManager::getManager()->getTimer("Move particles");

	//Get the object name, the grid geometry and the patch hierarchy
  	d_grid_geometry = grid_geom;
	d_patch_hierarchy = patch_hierarchy;
	d_object_name = object_name;
	d_init_from_files = init_from_files;
	initial_dt = dt;




	//Get parameters
	dx  = grid_geom->getDx();
	v0 = database->getDouble("v0");
	eta = database->getDouble("eta");
	time_steps = database->getInteger("time_steps");
	std::shared_ptr<tbox::Database> particles_db(database->getDatabase("particles"));
	density_par = particles_db->getDouble("density_par");
	if (density_par <= 0) {
		TBOX_ERROR("Problem::Problem"
			<< "n      Error in parameter input:"
			<< "n      density_par must be greater than 0"<< std::endl);
	}
	particleDistribution = particles_db->getString("particle_distribution");
	influence_radius = particles_db->getDouble("influence_radius");
	if (influence_radius <= 0) {
		TBOX_ERROR("Problem::Problem"
			<< "n      Error in parameter input:"
			<< "n      influence_radius must be greater than 0"<< std::endl);
	}
	particleSeparation_x = dx[0]/density_par;
	particleSeparation_y = dx[1]/density_par;
	//Random initialization
	gsl_rng_env_setup();
	//Random for simulation
	const tbox::SAMRAI_MPI& mpi(tbox::SAMRAI_MPI::getSAMRAIWorld());
	int random_seed = database->getDouble("random_seed")*(mpi.getRank() + 1);
	r_var = gsl_rng_alloc(gsl_rng_ranlxs0);
	gsl_rng_set(r_var, random_seed);
	//Random for mapping pursposes
	r_map = gsl_rng_alloc(gsl_rng_ranlxs0);
	gsl_rng_set(r_map, database->getDouble("random_seed"));


    //Subcycling
	d_refinedTimeStepping = false;
	d_tappering = false;



	//Stencil of the discretization method
	dx  = grid_geom->getDx();
	numberOfParticles = density_par * (grid_geom->getXUpper()[0] - grid_geom->getXLower()[0]) * (grid_geom->getXUpper()[1] - grid_geom->getXLower()[1]) / ( dx[0] * dx[1]);
	numberOfCellsRadius_x = ceil(influence_radius/dx[0]);
	numberOfCellsRadius_y = ceil(influence_radius/dx[1]);
	d_ghost_width = MAX(numberOfCellsRadius_x, numberOfCellsRadius_y);

	
	//Register Fields and temporal fields into the variable database
	hier::VariableDatabase *vdb = hier::VariableDatabase::getDatabase();
  	std::shared_ptr<hier::VariableContext> d_cont_curr(vdb->getContext("Current"));
	std::shared_ptr< pdat::CellVariable<int> > interior(std::shared_ptr< pdat::CellVariable<int> >(new pdat::CellVariable<int>(d_dim, "interior",1)));
	d_interior_id = vdb->registerVariableAndContext(interior ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_interior_id);
	std::shared_ptr< pdat::CellVariable<int> > nonSync(std::shared_ptr< pdat::CellVariable<int> >(new pdat::CellVariable<int>(d_dim, "nonSync",1)));
	d_nonSync_id = vdb->registerVariableAndContext(nonSync ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_nonSync_id);
	std::shared_ptr< pdat::CellVariable<int> > region(std::shared_ptr< pdat::CellVariable<int> >(new pdat::CellVariable<int>(d_dim, "region",1)));
	d_region_id = vdb->registerVariableAndContext(region ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_region_id);
	std::shared_ptr< pdat::IndexVariable<NonSyncs, pdat::CellGeometry > > nonSyncP(std::shared_ptr< pdat::IndexVariable<NonSyncs, pdat::CellGeometry > >(new pdat::IndexVariable<NonSyncs, pdat::CellGeometry >(d_dim, "nonSyncP")));
	d_nonSyncP_id = vdb->registerVariableAndContext(nonSyncP ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_nonSyncP_id);
	std::shared_ptr< pdat::IndexVariable<Particles, pdat::CellGeometry > > problemVariable(std::shared_ptr< pdat::IndexVariable<Particles, pdat::CellGeometry > >(new pdat::IndexVariable<Particles, pdat::CellGeometry >(d_dim, "problemVariable")));
	d_problemVariable_id = vdb->registerVariableAndContext(problemVariable, d_cont_curr, hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_problemVariable_id);


	//Refine and coarse algorithms

	d_bdry_fill_init = std::shared_ptr< xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());
	d_bdry_fill_advance = std::shared_ptr< xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());
	d_coarsen_algorithm = std::shared_ptr< xfer::CoarsenAlgorithm >(new xfer::CoarsenAlgorithm(d_dim));

	d_mapping_fill = std::shared_ptr< xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());
	d_fill_new_level    = std::shared_ptr< xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());

	//mapping communication

	string mapping_op_name = "CONSTANT_REFINE";
	std::shared_ptr< hier::RefineOperator > refine_region2(d_grid_geometry->lookupRefineOperator(region, mapping_op_name));
	std::shared_ptr< hier::RefineOperator > refine_interior(d_grid_geometry->lookupRefineOperator(interior, mapping_op_name));
	d_mapping_fill->registerRefine(d_interior_id,d_interior_id,d_interior_id, refine_interior);
	d_mapping_fill->registerRefine(d_region_id,d_region_id,d_region_id, refine_region2);
	string particleMapping_op_name = "NO_REFINE";
	std::shared_ptr< hier::RefineOperator > refine_particle(d_grid_geometry->lookupRefineOperator(problemVariable, particleMapping_op_name));
	d_mapping_fill->registerRefine(d_problemVariable_id,d_problemVariable_id,d_problemVariable_id, refine_particle);


	//refine and coarsen operators
	string refine_op_name = "NO_REFINE";
	std::shared_ptr< hier::RefineOperator > refine = d_grid_geometry->lookupRefineOperator(problemVariable, refine_op_name);


	//Register variables to the refineAlgorithm for boundaries

	d_bdry_fill_init->registerRefine(d_problemVariable_id,d_problemVariable_id,d_problemVariable_id,refine);
	d_bdry_fill_advance->registerRefine(d_problemVariable_id,d_problemVariable_id,d_problemVariable_id,refine);


	//Register variables to the refineAlgorithm for filling new levels on regridding


	//Register variables to the coarsenAlgorithm


}

/*
 * Destructor.
 */
Problem::~Problem() 
{
} 

/*
 * Initialize the data from a given level.
 */
void Problem::initializeLevelData (
   const std::shared_ptr<hier::PatchHierarchy >& hierarchy , 
   const int level_number ,
   const double init_data_time ,
   const bool can_be_refined ,
   const bool initial_time ,
   const std::shared_ptr<hier::PatchLevel >& old_level ,
   const bool allocate_data )
{
	std::shared_ptr< hier::PatchLevel > level(hierarchy->getPatchLevel(level_number));

	// Allocate storage needed to initialize level and fill data from coarser levels in AMR hierarchy.  
	level->allocatePatchData(d_interior_id, init_data_time);
	level->allocatePatchData(d_nonSync_id, init_data_time);
	level->allocatePatchData(d_problemVariable_id, init_data_time);
	level->allocatePatchData(d_nonSyncP_id, init_data_time);
	level->allocatePatchData(d_region_id, init_data_time);




	//Mapping the current data for new level.
	if (initial_time) {
		mapDataOnPatch(init_data_time, initial_time, level_number, level);
	}





	//Initialize current data for new level.
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr< hier::Patch > patch = *p_it;
        		if (initial_time) {
	      		initializeDataOnPatch(*patch, init_data_time, initial_time);
        		}
	}
	//Post-initialization Sync.
    	if (initial_time) {

		d_bdry_fill_init->createSchedule(level,this)->fillData(init_data_time, false);
		for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
			const std::shared_ptr< hier::Patch > patch = *p_it;
			std::shared_ptr< pdat::IndexData<Particles, pdat::CellGeometry > > problemVariable(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles, pdat::CellGeometry >, hier::PatchData>(patch->getPatchData(d_problemVariable_id)));
			const hier::Index boxfirst = problemVariable->getGhostBox().lower();
			const hier::Index boxlast  = problemVariable->getGhostBox().upper();

			for (int index1 = boxfirst(1); index1 <= boxlast(1); index1++) {
				for (int index0 = boxfirst(0); index0 <= boxlast(0); index0++) {
					hier::Index idx(index0, index1);
					//Correct the position if there is periodical boundary
					Particles* part = problemVariable->getItem(idx);
					checkPosition(patch, index0, index1, part);
				}
			}
		}

    	}
}



void Problem::initializeLevelIntegrator(
   const std::shared_ptr<mesh::GriddingAlgorithmStrategy>& gridding_alg)
{
}

double Problem::getLevelDt(
   const std::shared_ptr<hier::PatchLevel>& level,
   const double dt_time,
   const bool initial_time)
{
  
   TBOX_ASSERT(level);

   if (level->getLevelNumber() == 0) return initial_dt;

    double dt = initial_dt;
    const hier::IntVector ratio = level->getRatioToLevelZero();
    double local_dt = dt;
    for (int i = 0; i < 2; i++) {
        if (local_dt > dt / ratio[i]) {
            local_dt = dt / ratio[i];
        }
    }
    return local_dt;
}

double Problem::getMaxFinerLevelDt(
   const int finer_level_number,
   const double coarse_dt,
   const hier::IntVector& ratio)
{
   NULL_USE(finer_level_number);

   TBOX_ASSERT(ratio.min() > 0);

   return coarse_dt / double(ratio.max());
}

void Problem::standardLevelSynchronization(
   const std::shared_ptr<hier::PatchHierarchy>& hierarchy,
   const int coarsest_level,
   const int finest_level,
   const double sync_time,
   const std::vector<double>& old_times)
{

}

void Problem::synchronizeNewLevels(
   const std::shared_ptr<hier::PatchHierarchy>& hierarchy,
   const int coarsest_level,
   const int finest_level,
   const double sync_time,
   const bool initial_time)
{

}

void Problem::resetTimeDependentData(
   const std::shared_ptr<hier::PatchLevel>& level,
   const double new_time,
   const bool can_be_refined)
{
   TBOX_ASSERT(level);
   level->setTime(new_time);
}

void Problem::resetDataToPreadvanceState(
   const std::shared_ptr<hier::PatchLevel>& level)
{
    //cout<<"resetDataToPreadvanceState"<<endl;
}

/*
 * Map data on a patch. This mapping is done only at the begining of the simulation.
 */
void Problem::mapDataOnPatch(const double time, const bool initial_time, const int ln, const std::shared_ptr< hier::PatchLevel >& level)
{
	(void) time;
	const tbox::SAMRAI_MPI& mpi(tbox::SAMRAI_MPI::getSAMRAIWorld());
   	if (initial_time) {

		// Mapping		
		int x, xterm, xMapStartC, xMapEndC, xl, xu;
		double xMapStart, xMapEnd, tmpx;
		xGlower = d_grid_geometry->getXLower()[0];
		xGupper = d_grid_geometry->getXUpper()[0];
		int y, yterm, yMapStartC, yMapEndC, yl, yu;
		double yMapStart, yMapEnd, tmpy;
		yGlower = d_grid_geometry->getXLower()[1];
		yGupper = d_grid_geometry->getXUpper()[1];
		int minBlock[2], maxBlock[2], unionsI, facePointI, ie1, ie2, ie3, proc, tmp, pred, nodes;
		double maxDistance, e1, e2, e3;
		double position[2], maxPosition[2], minPosition[2];
		bool workingArray[mpi.getSize()], finishedArray[mpi.getSize()], workingGlobal, finishedGlobal, working, finished, modif;
		double SQRT3INV = 1.0/sqrt(3.0);
		mapStencil = MAX(numberOfCellsRadius_x, numberOfCellsRadius_y);

		xlastG = d_grid_geometry->getPhysicalDomain().front().numberCells()[0] + 2 * d_ghost_width;
		ylastG = d_grid_geometry->getPhysicalDomain().front().numberCells()[1] + 2 * d_ghost_width;

		//Cell mapping
		for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
			const std::shared_ptr< hier::Patch > patch = *p_it;
			int* region = ((pdat::CellData<int> *) patch->getPatchData(d_region_id).get())->getPointer();
			int* interior = ((pdat::CellData<int> *) patch->getPatchData(d_interior_id).get())->getPointer();
			int* nonSync = ((pdat::CellData<int> *) patch->getPatchData(d_nonSync_id).get())->getPointer();
			const hier::Index boxfirst1 = patch->getBox().lower();
			const hier::Index boxlast1  = patch->getBox().upper();

			//Get delta spaces into an array. dx, dy, dz.
			const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
			const double* dx  = patch_geom->getDx();

			int xlast = boxlast1(0)-boxfirst1(0) + 1 + 2 * d_ghost_width;
			int ylast = boxlast1(1)-boxfirst1(1) + 1 + 2 * d_ghost_width;
			//Interior
			xMapStartC = 0 + d_ghost_width;
			xMapEndC = (boxlast1(0) - boxfirst1(0)) + d_ghost_width;
			yMapStartC = 0 + d_ghost_width;
			yMapEndC = (boxlast1(1) - boxfirst1(1)) + d_ghost_width;
			for (x = xMapStartC; x <= xMapEndC; x++) {
				for (y = yMapStartC; y <= yMapEndC; y++) {
					vector(region, x, y) = 1;
				}
			}
		}
		//Boundaries Mapping
		for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
			const std::shared_ptr< hier::Patch > patch = *p_it;
			int* region = ((pdat::CellData<int> *) patch->getPatchData(d_region_id).get())->getPointer();
			int* interior = ((pdat::CellData<int> *) patch->getPatchData(d_interior_id).get())->getPointer();
			int* nonSync = ((pdat::CellData<int> *) patch->getPatchData(d_nonSync_id).get())->getPointer();
			const hier::Index boxfirst1 = patch->getBox().lower();
			const hier::Index boxlast1  = patch->getBox().upper();

			//Get delta spaces into an array. dx, dy, dz.
			const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
			const double* dx  = patch_geom->getDx();

			int xlast = boxlast1(0)-boxfirst1(0) + 1 + 2 * d_ghost_width;
			int ylast = boxlast1(1)-boxfirst1(1) + 1 + 2 * d_ghost_width;
			//y-Upper
			if (patch->getPatchGeometry()->getTouchesRegularBoundary(1, 1)) {
				for (y = ylast - d_ghost_width; y < ylast; y++) {
					for (x = 0; x < xlast; x++) {
						vector(region, x, y) = -4;
					}
				}
			}
			//y-Lower
			if (patch->getPatchGeometry()->getTouchesRegularBoundary(1, 0)) {
				for (y = 0; y < d_ghost_width; y++) {
					for (x = 0; x < xlast; x++) {
						vector(region, x, y) = -3;
					}
				}
			}
			//x-Upper
			if (patch->getPatchGeometry()->getTouchesRegularBoundary(0, 1)) {
				for (y = 0; y < ylast; y++) {
					for (x = xlast - d_ghost_width; x < xlast; x++) {
						vector(region, x, y) = -2;
					}
				}
			}
			//x-Lower
			if (patch->getPatchGeometry()->getTouchesRegularBoundary(0, 0)) {
				for (y = 0; y < ylast; y++) {
					for (x = 0; x < d_ghost_width; x++) {
						vector(region, x, y) = -1;
					}
				}
			}
		}
		d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);

		//Particle mapping
		for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
			const std::shared_ptr< hier::Patch > patch = *p_it;
			int* region = ((pdat::CellData<int> *) patch->getPatchData(d_region_id).get())->getPointer();
			int* interior = ((pdat::CellData<int> *) patch->getPatchData(d_interior_id).get())->getPointer();
			int* nonSync = ((pdat::CellData<int> *) patch->getPatchData(d_nonSync_id).get())->getPointer();
			std::shared_ptr< pdat::IndexData<Particles, pdat::CellGeometry > > problemVariable(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles, pdat::CellGeometry >, hier::PatchData>(patch->getPatchData(d_problemVariable_id)));
			std::shared_ptr< pdat::IndexData<NonSyncs, pdat::CellGeometry > > nonSyncVariable(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< NonSyncs, pdat::CellGeometry >, hier::PatchData>(patch->getPatchData(d_nonSyncP_id)));
			const hier::Index boxfirst = problemVariable->getGhostBox().lower();
			const hier::Index boxlast  = problemVariable->getGhostBox().upper();
			const hier::Index boxfirst1 = patch->getBox().lower();
			const hier::Index boxlast1  = patch->getBox().upper();

			//Get delta spaces into an array. dx, dy, dz.
			const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
			const double* dx  = patch_geom->getDx();

			int xlast = boxlast1(0)-boxfirst1(0) + 1 + 2 * d_ghost_width;
			int ylast = boxlast1(1)-boxfirst1(1) + 1 + 2 * d_ghost_width;
			//Give particles support
			for (y = boxfirst(1); y <= boxlast(1); y++) {
				for (x = boxfirst(0); x <= boxlast(0); x++) {
					hier::Index idx(x, y);
					if (!problemVariable->isElement(idx)) {
						Particles* part = new Particles();
						problemVariable->addItemPointer(idx, part);
					}
					if (!nonSyncVariable->isElement(idx)) {
						NonSyncs* nonSyncPart = new NonSyncs();
						nonSyncVariable->addItemPointer(idx, nonSyncPart);
					}
				}
			}

			if (particleDistribution.compare("RANDOM") == 0) {
				//All domain mapping
				int numberOfParticles_withGhosts = density_par * (d_grid_geometry->getXUpper()[0] - d_grid_geometry->getXLower()[0]+ 2 * d_ghost_width * dx[0]) / influence_radius * (d_grid_geometry->getXUpper()[1] - d_grid_geometry->getXLower()[1]+ 2 * d_ghost_width * dx[1]) / influence_radius;
				for (int n = 0; n < numberOfParticles_withGhosts; n++) {
					position[0] = gsl_rng_uniform(r_map) * (d_grid_geometry->getXUpper()[0] - d_grid_geometry->getXLower()[0] + 2 * d_ghost_width * dx[0]) + d_grid_geometry->getXLower()[0] - d_ghost_width * dx[0];
					x = floor((position[0] - d_grid_geometry->getXLower()[0])/dx[0]);
					position[1] = gsl_rng_uniform(r_map) * (d_grid_geometry->getXUpper()[1] - d_grid_geometry->getXLower()[1] + 2 * d_ghost_width * dx[1]) + d_grid_geometry->getXLower()[1] - d_ghost_width * dx[1];
					y = floor((position[1] - d_grid_geometry->getXLower()[1])/dx[1]);
					if (x >= boxfirst(0) && x <= boxlast(0) && y >= boxfirst(1) && y <= boxlast(1)) {
						hier::Index idx(x, y);
						Particles* part = problemVariable->getItem(idx);
						Particle* particle = new Particle(0, position, vector(region, x - boxfirst(0), y - boxfirst(1)));
						part->addParticle(*particle);
						delete particle;
					}
				}
			} else {
				//Interior
				xMapEnd = d_grid_geometry->getXUpper()[0] + d_ghost_width * dx[0];
				yMapStart = d_grid_geometry->getXLower()[1] + particleSeparation_y/2.0 - (floor(0.5 + d_ghost_width * (dx[1]/particleSeparation_y)))*particleSeparation_y;
				yMapEnd = d_grid_geometry->getXUpper()[1] + d_ghost_width * dx[1];
				for (int count_y = 0; yMapStart + count_y * particleSeparation_y < yMapEnd; count_y++) {
					if (particleDistribution.compare("STAGGERED") == 0 && isEven(count_y)) {
						xMapStart = d_grid_geometry->getXLower()[0] - (floor(d_ghost_width * (dx[0]/particleSeparation_x)))*particleSeparation_x;
					} else {
						xMapStart = d_grid_geometry->getXLower()[0] + particleSeparation_x/2.0 - (floor(0.5 + d_ghost_width * (dx[0]/particleSeparation_x)))*particleSeparation_x;
					}
					for (int count_x = 0; xMapStart + count_x * particleSeparation_x < xMapEnd; count_x++) {
						position[0] = xMapStart + count_x * particleSeparation_x;
						x = floor((position[0] - d_grid_geometry->getXLower()[0])/dx[0]);
						position[1] = yMapStart + count_y * particleSeparation_y;
						y = floor((position[1] - d_grid_geometry->getXLower()[1])/dx[1]);
						if (x >= boxfirst(0) && x <= boxlast(0) && y >= boxfirst(1) && y <= boxlast(1)) {
							hier::Index idx(x, y);
							Particles* part = problemVariable->getItem(idx);
							Particle* particle = new Particle(0, position, 1);
							Particle* oldParticle = part->overlaps(*particle);
							if (oldParticle != NULL) {
								part->deleteParticle(*oldParticle);
							}
							part->addParticle(*particle);
							delete particle;
						}
					}
				}
			}
			//Boundaries Mapping
			xMapEnd = d_grid_geometry->getXUpper()[0] + d_ghost_width * dx[0];
			yMapStart = d_grid_geometry->getXLower()[1] + particleSeparation_y/2.0 - (floor(0.5 + d_ghost_width * (dx[1]/particleSeparation_y)))*particleSeparation_y;
			yMapEnd = d_grid_geometry->getXUpper()[1] + d_ghost_width * dx[1];
			for (int count_y = 0; yMapStart + count_y * particleSeparation_y < yMapEnd; count_y++) {
				if (particleDistribution.compare("STAGGERED") == 0 && isEven(count_y)) {
					xMapStart = d_grid_geometry->getXLower()[0] - (floor(d_ghost_width * (dx[0]/particleSeparation_x)))*particleSeparation_x;
				} else {
					xMapStart = d_grid_geometry->getXLower()[0] + particleSeparation_x/2.0 - (floor(0.5 + d_ghost_width * (dx[0]/particleSeparation_x)))*particleSeparation_x;
				}
				for (int count_x = 0; xMapStart + count_x * particleSeparation_x < xMapEnd; count_x++) {
					position[0] = xMapStart + count_x * particleSeparation_x;
					x = floor((position[0] - d_grid_geometry->getXLower()[0])/dx[0]);
					position[1] = yMapStart + count_y * particleSeparation_y;
					y = floor((position[1] - d_grid_geometry->getXLower()[1])/dx[1]);
					if (x >= boxfirst(0) && x <= boxlast(0) && y >= boxfirst(1) && y <= boxlast(1)) {
						//y-Upper
						if (y > boxlast1(1)) {
							hier::Index idx(x, y);
							Particles* part = problemVariable->getItem(idx);
							Particle* particle = new Particle(0, position, -4);
							Particle* oldParticle = part->overlaps(*particle);
							if (oldParticle != NULL) {
								part->deleteParticle(*oldParticle);
							}
							part->addParticle(*particle);
							delete particle;
						}
						//y-Lower
						if (y < boxfirst1(1)) {
							hier::Index idx(x, y);
							Particles* part = problemVariable->getItem(idx);
							Particle* particle = new Particle(0, position, -3);
							Particle* oldParticle = part->overlaps(*particle);
							if (oldParticle != NULL) {
								part->deleteParticle(*oldParticle);
							}
							part->addParticle(*particle);
							delete particle;
						}
						//x-Upper
						if (x > boxlast1(0)) {
							hier::Index idx(x, y);
							Particles* part = problemVariable->getItem(idx);
							Particle* particle = new Particle(0, position, -2);
							Particle* oldParticle = part->overlaps(*particle);
							if (oldParticle != NULL) {
								part->deleteParticle(*oldParticle);
							}
							part->addParticle(*particle);
							delete particle;
						}
						//x-Lower
						if (x < boxfirst1(0)) {
							hier::Index idx(x, y);
							Particles* part = problemVariable->getItem(idx);
							Particle* particle = new Particle(0, position, -1);
							Particle* oldParticle = part->overlaps(*particle);
							if (oldParticle != NULL) {
								part->deleteParticle(*oldParticle);
							}
							part->addParticle(*particle);
							delete particle;
						}
					}
				}
			}
		}


   	}
}








/*
 * Initialize data on a patch. This initialization is done only at the begining of the simulation.
 */
void Problem::initializeDataOnPatch(hier::Patch& patch, 
                                    const double time,
                                    const bool initial_time)
{
	(void) time;
   	if (initial_time) {
		// Initial conditions		
		//Get fields, auxiliary fields and local variables that are going to be used.
		std::shared_ptr< pdat::IndexData<Particles, pdat::CellGeometry > > problemVariable(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles, pdat::CellGeometry >, hier::PatchData>(patch.getPatchData(d_problemVariable_id)));
		int* region = ((pdat::CellData<int> *) patch.getPatchData(d_region_id).get())->getPointer();
		const hier::Index boxfirst = problemVariable->getGhostBox().lower();
		const hier::Index boxlast  = problemVariable->getGhostBox().upper();
		
		const hier::Index boxfirst1 = patch.getBox().lower();
		const hier::Index boxlast1 = patch.getBox().upper();
		
		int xlast = boxlast1(0)-boxfirst1(0) + 1 + 2 * d_ghost_width;
		int ylast = boxlast1(1)-boxfirst1(1) + 1 + 2 * d_ghost_width;
		
		
		for(int index1 = boxfirst(1); index1 <= boxlast(1); index1++) {
			for(int index0 = boxfirst(0); index0 <= boxlast(0); index0++) {
				hier::Index idx(index0, index1);
				Particles* part = problemVariable->getItem(idx);
				for (int pit = 0; pit < part->getNumberOfParticles(); pit++) {
					Particle* particle = part->getParticle(pit);
					particle->random = gsl_rng_uniform(r_var) * (3.14159265358979323846264338 - -3.14159265358979323846264338) + -3.14159265358979323846264338;
					particle->theta = particle->random;
					particle->speedx = v0 * cos(particle->theta);
					particle->speedy = v0 * sin(particle->theta);
				}
			}
		}
		
		

   	}
}

/*
 * Gets the coarser patch that contains the box
 */
const std::shared_ptr<hier::Patch >& Problem::getCoarserPatch(
	const std::shared_ptr< hier::PatchLevel >& level,
	const hier::Box interior, 
	const hier::IntVector ratio)
{
	const hier::Box& coarsenBox = hier::Box::coarsen(interior, ratio);
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr< hier::Patch >& patch = *p_it;
		const hier::Box& interior_C = patch->getBox();
		if (interior_C.intersects(coarsenBox)) {
			return patch;		
		}
	}
}

/*
 * Reset the hierarchy-dependent internal information.
 */
void Problem::resetHierarchyConfiguration (
   const std::shared_ptr<hier::PatchHierarchy >& new_hierarchy ,
   int coarsest_level ,
   int finest_level )
{
	int finest_hiera_level = new_hierarchy->getFinestLevelNumber();

   	//  If we have added or removed a level, resize the schedule arrays

	d_bdry_sched_advance.resize(finest_hiera_level+1);
	d_coarsen_schedule.resize(finest_hiera_level+1);
	//  Build coarsen and refine communication schedules.
	for (int ln = coarsest_level; ln <= finest_hiera_level; ln++) {
		std::shared_ptr< hier::PatchLevel > level(new_hierarchy->getPatchLevel(ln));
		d_bdry_sched_advance[ln] = d_bdry_fill_advance->createSchedule(level,ln-1,new_hierarchy,this);
		// coarsen schedule only for levels > 0
		if (ln > 0) {
			std::shared_ptr< hier::PatchLevel > coarser_level(new_hierarchy->getPatchLevel(ln-1));
			d_coarsen_schedule[ln] = d_coarsen_algorithm->createSchedule(coarser_level, level, NULL);
		}
	}

}




/*
 * This method sets the physical boundary conditions.
 */
void Problem::setPhysicalBoundaryConditions(
   hier::Patch& patch,
   const double fill_time,
   const hier::IntVector& ghost_width_to_fill)
{
	//Boundary must not be implemented in this method
}

/*
 * Set up external plotter to plot internal data from this class.  
 * Register variables appropriate for plotting.                       
 */
int Problem::setupPlotter(
  ParticleDataWriter &plotter
) const {
	if (!d_patch_hierarchy) {
		TBOX_ERROR(d_object_name << ": No hierarchy in\n"
			<< " Problem::setupPlotter\n"
                 	<< "The hierarchy must be set before calling\n"
                 	<< "this function.\n");
   	}
	set<string> fields;
	fields.insert("speedx");
	fields.insert("speedy");
	fields.insert("num");
	fields.insert("denom");
	fields.insert("random");
	fields.insert("neighbours");
	fields.insert("theta");
	hier::VariableDatabase *vdb = hier::VariableDatabase::getDatabase();
	for (vector<string>::const_iterator it = d_full_writer_variables.begin() ; it != d_full_writer_variables.end(); ++it) {
		string var_to_register = *it;
		if (!(fields.end() != fields.find(var_to_register))) {
			TBOX_ERROR(d_object_name << ": Variable selected for 3D write not found:" <<  var_to_register);
		}
		plotter.registerPlotVariable(var_to_register,d_problemVariable_id);
	}


   	return 0;
}


/*
 * Perform a single step from the discretization schema algorithm   
 */
double Problem::advanceLevel(
   const std::shared_ptr<hier::PatchLevel>& level,
   const std::shared_ptr<hier::PatchHierarchy>& hierarchy,
   const double current_time,
   const double new_time,
   const bool first_step,
   const bool last_step,
   const bool regrid_advance)
{
	const int ln = level->getLevelNumber();

	const double simPlat_dt = new_time - current_time;
  const double level_ratio = level->getRatioToCoarserLevel().max();

	t_step->start();
  	// Shifting time
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch > patch = *p_it;
		std::shared_ptr< pdat::IndexData<Particles, pdat::CellGeometry > > problemVariable(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles, pdat::CellGeometry >, hier::PatchData>(patch->getPatchData(d_problemVariable_id)));
		const hier::Index boxfirst = problemVariable->getGhostBox().lower();
		const hier::Index boxlast  = problemVariable->getGhostBox().upper();
		for (int index1 = boxfirst(1); index1 <= boxlast(1); index1++) {
			for (int index0 = boxfirst(0); index0 <= boxlast(0); index0++) {
				hier::Index idx(index0, index1);
				Particles* part = problemVariable->getItem(idx);
				for (int pit = part->getNumberOfParticles() - 1; pit >= 0; pit--) {
					Particle* particle = part->getParticle(pit);
					particle->positiony_p = particle->positiony;
					particle->positionx_p = particle->positionx;
				}
			}
		}
	}
  	// Evolution
	int pitbmin, index0min, index1min;
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch > patch = *p_it;
		std::shared_ptr< pdat::IndexData<Particles, pdat::CellGeometry > > problemVariable(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles, pdat::CellGeometry >, hier::PatchData>(patch->getPatchData(d_problemVariable_id)));
		const hier::Index boxfirst1 = patch->getBox().lower();
		const hier::Index boxlast1  = patch->getBox().upper();
		const hier::Index boxfirst = problemVariable->getGhostBox().lower();
		const hier::Index boxlast  = problemVariable->getGhostBox().upper();
		int xlast = boxlast1(0)-boxfirst1(0) + 1 + 2 * d_ghost_width;
		int ylast = boxlast1(1)-boxfirst1(1) + 1 + 2 * d_ghost_width;
	
		for (int index1 = boxfirst(1); index1 <= boxlast(1); index1++) {
			for (int index0 = boxfirst(0); index0 <= boxlast(0); index0++) {
				hier::Index idx(index0, index1);
				Particles* part = problemVariable->getItem(idx);
				for (int pit = part->getNumberOfParticles() - 1; pit >= 0; pit--) {
					Particle* particle = part->getParticle(pit);
					particle->num = 0.0;
	
					particle->denom = 0.0;
	
					particle->neighbours = 0.0;
	
				}
			}
		}
	}
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch > patch = *p_it;
		std::shared_ptr< pdat::IndexData<Particles, pdat::CellGeometry > > problemVariable(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles, pdat::CellGeometry >, hier::PatchData>(patch->getPatchData(d_problemVariable_id)));
		const hier::Index boxfirst1 = patch->getBox().lower();
		const hier::Index boxlast1  = patch->getBox().upper();
		const hier::Index boxfirst = problemVariable->getGhostBox().lower();
		const hier::Index boxlast  = problemVariable->getGhostBox().upper();
		int xlast = boxlast1(0)-boxfirst1(0) + 1 + 2 * d_ghost_width;
		int ylast = boxlast1(1)-boxfirst1(1) + 1 + 2 * d_ghost_width;
	
		for (int index1 = boxfirst(1); index1 <= boxlast(1); index1++) {
			for (int index0 = boxfirst(0); index0 <= boxlast(0); index0++) {
				hier::Index idx(index0, index1);
				Particles* part = problemVariable->getItem(idx);
				for (int pit = part->getNumberOfParticles() - 1; pit >= 0; pit--) {
					Particle* particle = part->getParticle(pit);
					int miniIndex = MAX(boxfirst(0), index0 - numberOfCellsRadius_x);
					int maxiIndex = MIN(boxlast(0), index0 + numberOfCellsRadius_x);
					int maxjIndex = MIN(boxlast(1), index1 + numberOfCellsRadius_y);
					for(int index1b = index1; index1b <= maxjIndex; index1b++) {
						if (index1b == index1)	{index0min = index0;}
						else								  {index0min = miniIndex;}
						for(int index0b = index0min; index0b <= maxiIndex; index0b++) {
							hier::Index idxb(index0b, index1b);
							Particles* partb = problemVariable->getItem(idxb);
							if ((index1b==index1) && (index0b==index0)) {pitbmin = pit+1;}
							else								  {pitbmin = 0;}
							for (int pitb = pitbmin; pitb < partb->getNumberOfParticles(); pitb++) {
								Particle* particleb = partb->getParticle(pitb);
								//A-B and B-A interactions
								double dist_1 = particleb->distance_p(particle);
								if ((dist_1 < influence_radius) && (dist_1 > 0.0)) {
									particle->num = particle->num + sin(particleb->theta);
									particle->denom = particle->denom + cos(particleb->theta);
									particle->neighbours = particle->neighbours + 1.0;
									particleb->num = particleb->num + sin(particle->theta);
									particleb->denom = particleb->denom + cos(particle->theta);
									particleb->neighbours = particleb->neighbours + 1.0;
								}
							}
						}
					}
	
				}
			}
		}
	}
	//Fill ghosts and periodical boundaries, but no other boundaries
	d_bdry_sched_advance[0]->fillData(current_time, false);
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch > patch = *p_it;
		std::shared_ptr< pdat::IndexData<Particles, pdat::CellGeometry > > problemVariable(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles, pdat::CellGeometry >, hier::PatchData>(patch->getPatchData(d_problemVariable_id)));
		const hier::Index boxfirst1 = patch->getBox().lower();
		const hier::Index boxlast1  = patch->getBox().upper();
		const hier::Index boxfirst = problemVariable->getGhostBox().lower();
		const hier::Index boxlast  = problemVariable->getGhostBox().upper();
		int xlast = boxlast1(0)-boxfirst1(0) + 1 + 2 * d_ghost_width;
		int ylast = boxlast1(1)-boxfirst1(1) + 1 + 2 * d_ghost_width;
	
		for (int index1 = boxfirst(1); index1 <= boxlast(1); index1++) {
			for (int index0 = boxfirst(0); index0 <= boxlast(0); index0++) {
				hier::Index idx(index0, index1);
				//Correct the position if there is periodical boundary
				Particles* part = problemVariable->getItem(idx);
				checkPosition(patch, index0, index1, part);
			}
		}
	}
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch > patch = *p_it;
		std::shared_ptr< pdat::IndexData<Particles, pdat::CellGeometry > > problemVariable(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles, pdat::CellGeometry >, hier::PatchData>(patch->getPatchData(d_problemVariable_id)));
		const hier::Index boxfirst1 = patch->getBox().lower();
		const hier::Index boxlast1  = patch->getBox().upper();
		const hier::Index boxfirst = problemVariable->getGhostBox().lower();
		const hier::Index boxlast  = problemVariable->getGhostBox().upper();
		int xlast = boxlast1(0)-boxfirst1(0) + 1 + 2 * d_ghost_width;
		int ylast = boxlast1(1)-boxfirst1(1) + 1 + 2 * d_ghost_width;
	
		for (int index1 = boxfirst(1); index1 <= boxlast(1); index1++) {
			for (int index0 = boxfirst(0); index0 <= boxlast(0); index0++) {
				hier::Index idx(index0, index1);
				Particles* part = problemVariable->getItem(idx);
				for (int pit = part->getNumberOfParticles() - 1; pit >= 0; pit--) {
					Particle* particle = part->getParticle(pit);
					particle->random = gsl_rng_uniform(r_var) * (3.14159265358979323846264338 - -3.14159265358979323846264338) + -3.14159265358979323846264338;
	
					if (particle->neighbours > 0.0) {
						particle->num = particle->num + eta * particle->neighbours * sin(particle->random);
	
						particle->denom = particle->denom + eta * particle->neighbours * cos(particle->random);
	
						particle->theta = atan(particle->num / particle->denom);
	
					}
	
					particle->speedx = v0 * cos(particle->theta);
	
					particle->speedy = v0 * sin(particle->theta);
	
				}
			}
		}
	}
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch > patch = *p_it;
		std::shared_ptr< pdat::IndexData<Particles, pdat::CellGeometry > > problemVariable(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles, pdat::CellGeometry >, hier::PatchData>(patch->getPatchData(d_problemVariable_id)));
		const hier::Index boxfirst1 = patch->getBox().lower();
		const hier::Index boxlast1  = patch->getBox().upper();
		const hier::Index boxfirst = problemVariable->getGhostBox().lower();
		const hier::Index boxlast  = problemVariable->getGhostBox().upper();
		int xlast = boxlast1(0)-boxfirst1(0) + 1 + 2 * d_ghost_width;
		int ylast = boxlast1(1)-boxfirst1(1) + 1 + 2 * d_ghost_width;
	
		for (int index1 = boxfirst(1); index1 <= boxlast(1); index1++) {
			for (int index0 = boxfirst(0); index0 <= boxlast(0); index0++) {
				hier::Index idx(index0, index1);
				Particles* part = problemVariable->getItem(idx);
				for (int pit = part->getNumberOfParticles() - 1; pit >= 0; pit--) {
					Particle* particle = part->getParticle(pit);
					particle->positionx = particle->positionx_p + simPlat_dt * particle->speedx;
					particle->positiony = particle->positiony_p + simPlat_dt * particle->speedy;
				}
			}
		}
	}
	//Fill ghosts and periodical boundaries, but no other boundaries
	d_bdry_sched_advance[0]->fillData(current_time, false);
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch > patch = *p_it;
		std::shared_ptr< pdat::IndexData<Particles, pdat::CellGeometry > > problemVariable(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles, pdat::CellGeometry >, hier::PatchData>(patch->getPatchData(d_problemVariable_id)));
		const hier::Index boxfirst1 = patch->getBox().lower();
		const hier::Index boxlast1  = patch->getBox().upper();
		const hier::Index boxfirst = problemVariable->getGhostBox().lower();
		const hier::Index boxlast  = problemVariable->getGhostBox().upper();
		int xlast = boxlast1(0)-boxfirst1(0) + 1 + 2 * d_ghost_width;
		int ylast = boxlast1(1)-boxfirst1(1) + 1 + 2 * d_ghost_width;
	
		for (int index1 = boxfirst(1); index1 <= boxlast(1); index1++) {
			for (int index0 = boxfirst(0); index0 <= boxlast(0); index0++) {
				hier::Index idx(index0, index1);
				//Correct the position if there is periodical boundary
				Particles* part = problemVariable->getItem(idx);
				checkPosition(patch, index0, index1, part);
			}
		}
	}
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch > patch = *p_it;
		std::shared_ptr< pdat::IndexData<Particles, pdat::CellGeometry > > problemVariable(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles, pdat::CellGeometry >, hier::PatchData>(patch->getPatchData(d_problemVariable_id)));
		const hier::Index boxfirst1 = patch->getBox().lower();
		const hier::Index boxlast1  = patch->getBox().upper();
		const hier::Index boxfirst = problemVariable->getGhostBox().lower();
		const hier::Index boxlast  = problemVariable->getGhostBox().upper();
		int xlast = boxlast1(0)-boxfirst1(0) + 1 + 2 * d_ghost_width;
		int ylast = boxlast1(1)-boxfirst1(1) + 1 + 2 * d_ghost_width;
	
		for (int index1 = boxfirst(1); index1 <= boxlast(1); index1++) {
			for (int index0 = boxfirst(0); index0 <= boxlast(0); index0++) {
				hier::Index idx(index0, index1);
				Particles* part = problemVariable->getItem(idx);
				for (int pit = part->getNumberOfParticles() - 1; pit >= 0; pit--) {
					Particle* particle = part->getParticle(pit);
					moveParticles(patch, problemVariable, part, particle, index0, index1, particle->positionx_p, particle->positiony_p, particle->positionx, particle->positiony, simPlat_dt, current_time, pit);
				}
			}
		}
	}
	//Fill ghosts and periodical boundaries, but no other boundaries
	d_bdry_sched_advance[0]->fillData(current_time, false);
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch > patch = *p_it;
		std::shared_ptr< pdat::IndexData<Particles, pdat::CellGeometry > > problemVariable(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles, pdat::CellGeometry >, hier::PatchData>(patch->getPatchData(d_problemVariable_id)));
		const hier::Index boxfirst1 = patch->getBox().lower();
		const hier::Index boxlast1  = patch->getBox().upper();
		const hier::Index boxfirst = problemVariable->getGhostBox().lower();
		const hier::Index boxlast  = problemVariable->getGhostBox().upper();
		int xlast = boxlast1(0)-boxfirst1(0) + 1 + 2 * d_ghost_width;
		int ylast = boxlast1(1)-boxfirst1(1) + 1 + 2 * d_ghost_width;
	
		for (int index1 = boxfirst(1); index1 <= boxlast(1); index1++) {
			for (int index0 = boxfirst(0); index0 <= boxlast(0); index0++) {
				hier::Index idx(index0, index1);
				//Correct the position if there is periodical boundary
				Particles* part = problemVariable->getItem(idx);
				checkPosition(patch, index0, index1, part);
			}
		}
	}
	

	t_step->stop();


	return simPlat_dt;
}

/*
 * Checks the finalization conditions              
 */
bool Problem::checkFinalization(const double current_time, const double simPlat_dt)
{
	if (current_time > time_steps) { 
		return true;
	}
	return false;
	
	

}




/*
 *  Cell tagging routine - tag cells that require refinement based on a provided condition. 
 */
void Problem::applyGradientDetector(
   const std::shared_ptr< hier::PatchHierarchy >& hierarchy, 
   const int level_number,
   const double time, 
   const int tag_index,
   const bool initial_time,
   const bool uses_richardson_extrapolation_too) 
{

}

/*
 * Move the particles if the problem have any
 */
void Problem::moveParticles(const std::shared_ptr<hier::Patch > patch, std::shared_ptr< pdat::IndexData<Particles, pdat::CellGeometry > > problemVariable, Particles* part, Particle* particle, const int index0, const int index1, const double oldPosx, const double oldPosy, const double newPosx, const double newPosy, const double simPlat_dt, const double current_time, int pit)
{
	const hier::Index boxfirst1 = patch->getBox().lower();
	const hier::Index boxlast1  = patch->getBox().upper();
	const hier::Index boxfirst = problemVariable->getGhostBox().lower();
	const hier::Index boxlast  = problemVariable->getGhostBox().upper();

	//Get delta spaces into an array. dx, dy, dz.
	std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
	int xlower = patch_geom->getXLower()[0];
	int xupper = patch_geom->getXUpper()[0];
	int ylower = patch_geom->getXLower()[1];
	int yupper = patch_geom->getXUpper()[1];
	int xlast = boxlast1(0)-boxfirst1(0) + 1 + 2 * d_ghost_width;
	int ylast = boxlast1(1)-boxfirst1(1) + 1 + 2 * d_ghost_width;

	double position[2];
	double newPosition[2];

	hier::Index idx(index0, index1);
	position[0] = (oldPosx - xGlower)/dx[0];
	position[1] = (oldPosy - yGlower)/dx[1];

	newPosition[0] = (newPosx - xGlower)/dx[0];
	int newIndex0 = index0;
	newPosition[1] = (newPosy - yGlower)/dx[1];
	int newIndex1 = index1;

	bool out = false;
	if (greaterEq(newPosition[0], boxlast(0)+1) || lessThan(newPosition[0], boxfirst(0))) {
		out = true;
	} else {
		if (lessThan(newPosition[0], index0) || greaterEq(newPosition[0], index0+1)) {
			newIndex0 = floor(newPosition[0]);
		}
	}
	if (greaterEq(newPosition[1], boxlast(1)+1) || lessThan(newPosition[1], boxfirst(1))) {
		out = true;
	} else {
		if (lessThan(newPosition[1], index1) || greaterEq(newPosition[1], index1+1)) {
			newIndex1 = floor(newPosition[1]);
		}
	}
	if (!out) {
		hier::Index newIdx(newIndex0, newIndex1);
		if (!(newIdx == idx)) {
			bool deleted = false;
			if (!deleted) {
				//Need to create copy of the particle. Erase an item from a vector calls the destructor of the object
				Particle* newParticle = new Particle(*particle);
				part->deleteParticle(*particle);
				Particles* destPart = problemVariable->getItem(newIdx);
				destPart->addParticle(*newParticle);
				delete newParticle;
			}
		}
	} else {
		part->deleteParticle(*particle);
	}
}
void Problem::checkPosition(const std::shared_ptr<hier::Patch >& patch, double index0, double index1, Particles* particles)
{
	const hier::Index boxfirst1 = patch->getBox().lower();
	const hier::Index boxlast1  = patch->getBox().upper();

	std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));

	int xlower = patch_geom->getXLower()[0];
	int xupper = patch_geom->getXUpper()[0];
	bool periodicL0 = xGlower == xlower && !patch->getPatchGeometry()->getTouchesRegularBoundary(0, 0);
	bool periodicU0 = xGupper == xupper && !patch->getPatchGeometry()->getTouchesRegularBoundary(0, 1);
	int ylower = patch_geom->getXLower()[1];
	int yupper = patch_geom->getXUpper()[1];
	bool periodicL1 = yGlower == ylower && !patch->getPatchGeometry()->getTouchesRegularBoundary(1, 0);
	bool periodicU1 = yGupper == yupper && !patch->getPatchGeometry()->getTouchesRegularBoundary(1, 1);
	if (index0 < boxfirst1[0] || index0 > boxlast1[0] || index1 < boxfirst1[1] || index1 > boxlast1[1] ) {
		for(int pit = 0; pit < particles->getNumberOfParticles(); pit++) {
			Particle* particle = particles->getParticle(pit);
			double positionx_p = particle->positionx_p;
			double positiony_p = particle->positiony_p;
			double positionx = particle->positionx;
			double positiony = particle->positiony;
			if (periodicL0 && index0 < boxfirst1[0] && positionx_p > xGlower) {
				positionx_p=positionx_p-(xGupper - xGlower);
			}
			if (periodicU0 && index0 > boxlast1[0] && positionx_p < xGupper) {
				positionx_p=positionx_p+(xGupper - xGlower);
			}
			if (periodicL1 && index1 < boxfirst1[1] && positiony_p > yGlower) {
				positiony_p=positiony_p-(yGupper - yGlower);
			}
			if (periodicU1 && index1 > boxlast1[1] && positiony_p < yGupper) {
				positiony_p=positiony_p+(yGupper - yGlower);
			}
			if (periodicL0 && index0 < boxfirst1[0] && positionx > xGlower) {
				positionx=positionx-(xGupper - xGlower);
			}
			if (periodicU0 && index0 > boxlast1[0] && positionx < xGupper) {
				positionx=positionx+(xGupper - xGlower);
			}
			if (periodicL1 && index1 < boxfirst1[1] && positiony > yGlower) {
				positiony=positiony-(yGupper - yGlower);
			}
			if (periodicU1 && index1 > boxlast1[1] && positiony < yGupper) {
				positiony=positiony+(yGupper - yGlower);
			}
			particle->positionx_p = positionx_p;
			particle->positiony_p = positiony_p;
			particle->positionx = positionx;
			particle->positiony = positiony;
		}
	}
}


/*
 * Initialization of the common variables in case of restarting.
 */
void Problem::initCommonVars(const std::shared_ptr<hier::PatchHierarchy >& hierarchy)
{
	dx  = d_grid_geometry->getDx();
	xGlower = d_grid_geometry->getXLower()[0];
	xGupper = d_grid_geometry->getXUpper()[0];
	xlastG = d_grid_geometry->getPhysicalDomain().front().numberCells()[0] + 2 * d_ghost_width;
	yGlower = d_grid_geometry->getXLower()[1];
	yGupper = d_grid_geometry->getXUpper()[1];
	ylastG = d_grid_geometry->getPhysicalDomain().front().numberCells()[1] + 2 * d_ghost_width;
}


