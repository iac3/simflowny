#include <iostream>
#include <gsl/gsl_rng.h>
#include <string>
#include <fcntl.h>
#include <sys/stat.h>
#include <queue>
#include <fstream>
#include <libconfig.h++>
#include <boost/graph/use_mpi.hpp>
#include <boost/graph/distributed/adjacency_list.hpp>
#include <boost/graph/distributed/mpi_process_group.hpp>
#include <boost/graph/graphviz.hpp>
#include <boost/property_map/property_map.hpp>
#include <boost/graph/properties.hpp>
#include <boost/graph/iteration_macros.hpp>
#include <iac3_plod_generator.hpp>

#define MAX(X,Y) ((X) > (Y) ? (X) : (Y))
#define MIN(X,Y) ((X) < (Y) ? (X) : (Y))

using namespace std;
using namespace boost;
using namespace libconfig;
using std::graph::distributed::mpi_process_group;

//Simulation parameters
int number_of_vertices, number_of_edges, seed, interval;
string outputDir;
bool output_icash, output_igoods, output_cash, output_goods, output_price = false;
double gamma_par;

//Model parameters
double lambda;
double mu;
double tau;
double eps;
int time_steps;

struct EdgeProperties {
	EdgeProperties() {
		icash = 0;
		igoods = 0;
	}
	template<typename Archiver>
	void serialize(Archiver& ar, const unsigned int /*version*/) {
		ar & icash& igoods;
};
	double icash;
	double igoods;
};

struct VertexProperties {
	VertexProperties() {
		cash = 0;
		goods = 0;
		price = 0;
	}
	template<typename Archiver>
	void serialize(Archiver& ar, const unsigned int /*version*/) {
		ar & cash& goods& price;
	}
	double cash;
	double goods;
	double price;
};

typedef std::adjacency_list<vecS, distributedS<mpi_process_group, vecS>, bidirectionalS, VertexProperties, EdgeProperties> Graph;
typedef graph_traits<Graph>::vertex_descriptor Vertex;
typedef graph_traits<Graph>::edge_descriptor Edge;
struct remote_key_e {
	remote_key_e(int p = -1, Edge l = Edge()) : processor(p){local_key = &l;  }
	int processor;
	Edge* local_key;
	template<typename Archiver>
	void serialize(Archiver& ar, const unsigned int /*version*/) {
		ar & processor & local_key;
	}
};

namespace boost {
template<>
struct hash<remote_key_e> {
	std::size_t operator()(const remote_key_e& key) const {
		std::size_t hash = hash_value(key.processor);
		hash_combine(hash, key.local_key);
		return hash;
	}
};
}

inline bool operator==(const remote_key_e& x, const remote_key_e& y) {
	return x.processor == y.processor && (*(x.local_key)) == (*(y.local_key));
}

struct remote_key_to_global_e {
	typedef readable_property_map_tag category;
	typedef remote_key_e key_type;
	typedef std::pair<int, Edge> value_type;
	typedef value_type reference;
};

inline std::pair<int, Edge>
get(remote_key_to_global_e, const remote_key_e& key) {
	return std::make_pair(key.processor, *(key.local_key));
}

//Synchronization reduction operation
template<typename T>
struct rank_accumulate_reducer {
	static const bool non_default_resolver = false;
	// The default rank of an unknown node
	template<typename K>
	T operator()(const K&) const {return T(0); }
	template<typename K>
	T operator()(const K&, const T& x, const T& y) const { return x; }
};

static void _mkdir(const char *dir) {
	char tmp[256];
	char *p = NULL;
	size_t len;
	snprintf(tmp, sizeof(tmp),"%s",dir);
	len = strlen(tmp);
	if(tmp[len - 1] == '/')
		tmp[len - 1] = 0;
	for(p = tmp + 1; *p; p++)
		if(*p == '/') {
			*p = 0;
			mkdir(tmp, S_IRWXU);
			*p = '/';
		}
	mkdir(tmp, S_IRWXU);
}

int main(int argc, char** argv) {
	int rank, size;
	int edge_count, node_count = 0;
	int outputCounter = 0;

	//Read parameter file
	if (argc < 2) {
		cout<<"USAGE: ./"<<argv[0]<<" parameter_file"<<endl;
		return  -1;
	}
	Config cfg;
	try {
		cfg.readFile(argv[1]);

		//Simulation parameters
		cfg.lookupValue("number_of_vertices", number_of_vertices);
		cfg.lookupValue("number_of_edges", number_of_edges);
		cfg.lookupValue("seed", seed);
		cfg.lookupValue("output_dir_name", outputDir);
		cfg.lookupValue("output_dump_interval", interval);
		cfg.lookupValue("gamma_par", gamma_par);

		Setting &vertex_properties = cfg.lookup("vertex_properties");
		node_count = vertex_properties.getLength();
		for (int n = 0; n < node_count; n++) {
			string field = vertex_properties[n];
			if(field.compare("cash") == 0)
				output_cash = true;
			if(field.compare("goods") == 0)
				output_goods = true;
			if(field.compare("price") == 0)
				output_price = true;
		}
		Setting &edge_properties = cfg.lookup("edge_properties");
		edge_count = edge_properties.getLength();
		for (int n = 0; n < edge_count; n++) {
			string field = edge_properties[n];
			if(field.compare("icash") == 0)
				output_icash = true;
			if(field.compare("igoods") == 0)
				output_igoods = true;
		}
		//Model parameters
		if (!cfg.lookupValue("lambda", lambda)) {
			int aux;
			cfg.lookupValue("lambda", aux);
			lambda = aux;
		}
		if (!cfg.lookupValue("mu", mu)) {
			int aux;
			cfg.lookupValue("mu", aux);
			mu = aux;
		}
		if (!cfg.lookupValue("tau", tau)) {
			int aux;
			cfg.lookupValue("tau", aux);
			tau = aux;
		}
		if (!cfg.lookupValue("eps", eps)) {
			int aux;
			cfg.lookupValue("eps", aux);
			eps = aux;
		}
		cfg.lookupValue("time_steps", time_steps);
	}
	catch (ParseException ex) {
		cout << "Problem reading parameter file " << ex.getError() << " at line " << ex.getLine() << endl;
		return -1;
	}

	std::mpi::environment env(argc, argv);
	//Random scale-free
	typedef std::plod_iterator<Graph> SFGen;
	Graph g_id(SFGen(number_of_vertices, gamma_par, seed, false), SFGen(), number_of_vertices);

	mpi_process_group pg = g_id.process_group();
	rank = graph::distributed::process_id(pg);
	int processors = graph::distributed::num_processes(pg);
	synchronize(g_id);

	typedef  property_map<Graph, vertex_index_t>::type id_map;
	id_map id = get(vertex_index, g_id);
	property_map<Graph, double EdgeProperties::*>::type icash = get(&EdgeProperties::icash, g_id);
	property_map<Graph, double EdgeProperties::*>::type igoods = get(&EdgeProperties::igoods, g_id);
	property_map<Graph, double VertexProperties::*>::type cash = get(&VertexProperties::cash, g_id);
	property_map<Graph, double VertexProperties::*>::type goods = get(&VertexProperties::goods, g_id);
	property_map<Graph, double VertexProperties::*>::type price = get(&VertexProperties::price, g_id);

	typedef property_map<Graph, double EdgeProperties::*>::type icash_map;
	typedef std::parallel::distributed_property_map<mpi_process_group,remote_key_to_global_e,icash_map> Dist_icash_Map;
	Dist_icash_Map d_icash = std::parallel::make_distributed_property_map(pg, remote_key_to_global_e(), icash);
	typedef property_map<Graph, double EdgeProperties::*>::type igoods_map;
	typedef std::parallel::distributed_property_map<mpi_process_group,remote_key_to_global_e,igoods_map> Dist_igoods_Map;
	Dist_igoods_Map d_igoods = std::parallel::make_distributed_property_map(pg, remote_key_to_global_e(), igoods);
	//Vertex id distributed property maps
	typedef property_map<Graph, vertex_index_t>::const_type VertexIndexMap;
	typedef iterator_property_map<std::vector<size_t>::iterator, VertexIndexMap> d_index_map;
	std::vector<size_t> index_S(num_vertices(g_id), std::numeric_limits<size_t>::max());
	d_index_map d_index(index_S.begin(), id);
	d_index.set_reduce(rank_accumulate_reducer<size_t>());

	//Vertex id initializations
	BGL_FORALL_VERTICES(v, g_id, Graph) {
		put(d_index, v, (number_of_vertices/processors) * rank + id[v]);
	}
	synchronize(pg);
	synchronize(d_index);

	const gsl_rng_type * T;
	gsl_rng_env_setup();
	gsl_rng *r_var = r_var;
	r_var = gsl_rng_alloc(gsl_rng_mt19937);
	gsl_rng_set(r_var, seed * (rank + 1));

	bool deleted_vertex = false;
	bool deleted_edge = false;
	dynamic_properties dp;
	dp.property("node_id", id);
	if (output_icash)
		dp.property("icash", icash);
	if (output_igoods)
		dp.property("igoods", igoods);
	if (output_cash)
		dp.property("cash", cash);
	if (output_goods)
		dp.property("goods", goods);
	if (output_price)
		dp.property("price", price);

	//Creation of the dump folder recursively
	_mkdir(outputDir.c_str());
	//------ Initialization -------
	BGL_FORALL_VERTICES(n_id, g_id, Graph) {
		cash[n_id] = 100;
		goods[n_id] = 10;
		price[n_id] = 0.1;
	}
	BGL_FORALL_EDGES(e_id, g_id, Graph) {
		icash[e_id] = 0;
		igoods[e_id] = 0;
	}

	//Write initial output file
	ofstream myfileI;
	std::ostringstream outputNameI;
	outputNameI << outputDir << "/initial.dot";
	myfileI.open (outputNameI.str().c_str());
	write_graphviz(myfileI, g_id, dp);
	myfileI.close();

	bool end_simulation = false;

	//------ Main loop -------
	for (int t_it = 0; ; t_it++) {
		//------ Finalization condition -------
		if (t_it >= time_steps) { 
			end_simulation = true;
		}
		if (end_simulation) {
			break;
		}

		//------ Execution flow -------
		BGL_FORALL_VERTICES(n_id, g_id, Graph) {
			double pacc;
			pacc = 0;
			BGL_FORALL_OUTEDGES(n_id, e_id, g_id, Graph) {
				pacc = pacc + 1 / price[target(e_id, g_id)];
			}
			BGL_FORALL_OUTEDGES(n_id, e_id, g_id, Graph) {
				icash[e_id] = 1 / (price[target(e_id, g_id)] * pacc) * lambda * cash[n_id];
			}
		}
		synchronize(pg);
		synchronize(icash);
		BGL_FORALL_VERTICES(n_id, g_id, Graph) {
			double cacc;
			cacc = 0;
			BGL_FORALL_INEDGES(n_id, e_id, g_id, Graph) {
				remote_key_e remote_e_id(rank, e_id);
				cacc = cacc + icash[e_id];
			}
			BGL_FORALL_INEDGES(n_id, e_id, g_id, Graph) {
				remote_key_e remote_e_id(rank, e_id);
				put(d_igoods, remote_e_id ,icash[e_id] / cacc * mu * goods[n_id]);
			}
		}
		synchronize(pg);
		synchronize(igoods);
		BGL_FORALL_VERTICES(n_id, g_id, Graph) {
			double cacc;
			cacc = 0;
			BGL_FORALL_INEDGES(n_id, e_id, g_id, Graph) {
				remote_key_e remote_e_id(rank, e_id);
				cacc = cacc + icash[e_id];
			}
			price[n_id] = cacc / (mu * goods[n_id]);
		}
		synchronize(pg);
		synchronize(price);
		//------ Topology change -------
		BGL_FORALL_VERTICES(n_id, g_id, Graph) {
			double min;
			min = 9999999;

			BGL_FORALL_OUTEDGES(n_id, e_id, g_id, Graph) {
				if (min > price[target(e_id, g_id)]) {
					min = price[target(e_id, g_id)];

				}
			}

			deleted_edge = true;
			while(deleted_edge) {
				deleted_edge = false;
				BGL_FORALL_OUTEDGES(n_id, e_id, g_id, Graph) {
					if ((price[target(e_id, g_id)] > (tau * min)) || (igoods[e_id] < eps)) {
						remove_edge(e_id, g_id);
						deleted_edge = true;

					}
					if (deleted_edge) {
						break;
					}
					if (deleted_vertex) {
						break;
					}
				}
			}

		}
		synchronize(pg);
		BGL_FORALL_VERTICES(n_id, g_id, Graph) {
			double cin;
			double cout;
			double gin;
			double gout;
			cin = 0;
			cout = 0;
			BGL_FORALL_INEDGES(n_id, e_id, g_id, Graph) {
				remote_key_e remote_e_id(rank, e_id);
				cin = cin + icash[e_id];
			}
			BGL_FORALL_OUTEDGES(n_id, e_id, g_id, Graph) {
				cout = cout + icash[e_id];
			}
			cash[n_id] = cash[n_id] + cin - cout;
			gin = 0;
			gout = 0;
			BGL_FORALL_INEDGES(n_id, e_id, g_id, Graph) {
				remote_key_e remote_e_id(rank, e_id);
				gout = gout + igoods[e_id];
			}
			BGL_FORALL_OUTEDGES(n_id, e_id, g_id, Graph) {
				gin = gin + igoods[e_id];
			}
			goods[n_id] = goods[n_id] + gin - gout;
		}
		synchronize(pg);
		synchronize(cash);
		synchronize(goods);
		//------ Output -------
		if ((t_it + 1) % interval == 0) {
			//Write dot file
			ofstream myfile;
			std::ostringstream outputName;
			outputName << outputDir << "/result"<<outputCounter<<".dot";
			myfile.open (outputName.str().c_str());
			write_graphviz(myfile, g_id, dp);
			myfile.close();

			outputCounter++;
		}
	}

	return 0;
}
