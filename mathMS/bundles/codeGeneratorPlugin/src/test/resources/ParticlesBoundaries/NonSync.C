#ifndef included_NonSyncC
#define included_NonSyncC

#include "NonSync.h"
#include <string.h>
#include <math.h>
#include "SAMRAI/hier/Index.h"

NonSync::NonSync()
{
}

inline int NonSync::GetExpoBase2(double d)
{
	int i = 0;
	((short *)(&i))[0] = (((short *)(&d))[3] & (short)32752); // _123456789ab____ & 0111111111110000
	return (i >> 4) - 1023;
}

bool	NonSync::equals(double d1, double d2)
{
	if (d1 == d2)
		return true;
	int e1 = GetExpoBase2(d1);
	int e2 = GetExpoBase2(d2);
	int e3 = GetExpoBase2(d1 - d2);
	if ((e3 - e2 < -48) && (e3 - e1 < -48))
		return true;
	return false;
}

NonSync::NonSync(const int nonSyncValue, const double* pos)
{
	positioni = pos[0];
	positionj = pos[1];
	positionk = pos[2];

	this->nonSyncValue = nonSyncValue;
}

NonSync::NonSync(const NonSync& data)
{
	this->positioni = data.positioni;
	this->positionj = data.positionj;
	this->positionk = data.positionk;

	nonSyncValue = data.nonSyncValue;
}

NonSync::~NonSync()
{
}

void NonSync::setnonSync(const int nonSync)
{
	this->nonSyncValue = nonSync;
}

void NonSync::setpositioni(const double pos)
{
	positioni = pos;
}

void NonSync::setpositionj(const double pos)
{
	positionj = pos;
}

void NonSync::setpositionk(const double pos)
{
	positionk = pos;
}



double NonSync::getpositioni()
{
	return positioni;
}

double NonSync::getpositionj()
{
	return positionj;
}

double NonSync::getpositionk()
{
	return positionk;
}



int NonSync::getnonSync()
{
	return nonSyncValue;
}


NonSync& NonSync::operator=(const NonSync& data)
{
	if (this != &data) {
		positioni = data.positioni;
		positionj = data.positionj;
		positionk = data.positionk;

		nonSyncValue = data.nonSyncValue;
	}
	return(*this);

}

bool NonSync::operator==(const NonSync& data)
{
	if (this != &data) {
		if (nonSyncValue != data.nonSyncValue) {
			return false;
		}
	}
	return true;
}

NonSync* NonSync::overlaps(const NonSync& data)
{
	if (!equals(positioni, data.positioni) || !equals(positionj, data.positionj) || !equals(positionk, data.positionk)) {
		return NULL;
	}
	return this;
}

void NonSync::packStream(tbox::MessageStream& stream)
{
	
}

void NonSync::unpackStream(tbox::MessageStream& stream,
                                const hier::IntVector& offset)
{
	
}

void NonSync::putToDatabase(
   std::shared_ptr<tbox::Database>& database, int nonSync)
{
	
}

void NonSync::getFromDatabase(
   std::shared_ptr<tbox::Database>& database, int nonSync)
{
	
}

#endif

