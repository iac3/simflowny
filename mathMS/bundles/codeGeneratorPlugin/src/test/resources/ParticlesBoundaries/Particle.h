#ifndef included_Particle
#define included_Particle

#include "SAMRAI/tbox/MessageStream.h"
#include "SAMRAI/hier/IntVector.h"
#include "SAMRAI/tbox/Database.h"
#include <vector>

/**
 * The SampleClass struct holds some dummy data and methods.  It's intent
 * is to indicate how a user could construct their own index data double.
 */
using namespace std;
using namespace SAMRAI;

class Particle
{
public:

	//Struct to order particle pointer neighbourhood in std::sort
	struct less_than
	{
		const bool operator()( Particle *a,  Particle * b) const {
			return a->getDistance() < b->getDistance();
		}
	};

	inline int GetExpoBase2(double d);

	bool	Equal(double d1, double d2);

	/**
	* The default constructor.
	*/
	Particle();

	/**
	* Constructor.
	*/
	Particle(const int region);

	/**
	* The complete constructor.
	*/
	Particle(const double fieldValue, const double* pos, const int region = 0);

	/**
	* Copy constructor.
	*/
	Particle(const Particle& data);
	Particle(const Particle& data, bool newId);

	~Particle();

	//Setters 
	void setId(const int id);
	void setFieldValue(string varName, double value);

	//Getters
	double getFieldValue(string varName);
	int getId();

	//Distance between particles
	double distance_p(Particle* data);
	double distance(Particle* data);
	double distanceSPprime(Particle* data);
	double distance(double* point);


	bool same(const Particle& data);
	Particle* overlaps(const Particle& data);
	double getDistance();

	Particle& operator=(const Particle& data);

	bool operator==(const Particle& data);

	void packStream(tbox::MessageStream& stream);
	void unpackStream(tbox::MessageStream& stream, const hier::IntVector& offset);
	void getFromDatabase(std::shared_ptr<tbox::Database>& database, int particle);
	void putToDatabase(std::shared_ptr<tbox::Database>& database, int particle);

	static size_t size() {
		size_t bytes = tbox::MessageStream::getSizeof<double>(9 + 1 + 297) + tbox::MessageStream::getSizeof<int>(4);
		return(bytes);
	};

   	//Field value of the particle
	double rho;
	double rho_p;
	double mx;
	double mx_p;
	double my;
	double my_p;
	double mz;
	double mz_p;
	double e;
	double e_p;
	double fx;
	double fx_p;
	double fy;
	double fy_p;
	double fz;
	double fz_p;
	double p;
	double p_p;
	double constraint1_1;
	double constraint2_1;
	double Aux_constraint_AccInit;
	double mass;
	double FluxSBirho_1;
	double FluxSBjrho_1;
	double FluxSBkrho_1;
	double FluxSBimx_1;
	double FluxSBjmx_1;
	double FluxSBkmx_1;
	double FluxSBimy_1;
	double FluxSBjmy_1;
	double FluxSBkmy_1;
	double FluxSBimz_1;
	double FluxSBjmz_1;
	double FluxSBkmz_1;
	double FluxSBie_1;
	double FluxSBje_1;
	double FluxSBke_1;
	double mxSPS_1;
	double mySPS_1;
	double mzSPS_1;
	double eSPS_1;
	double PSBiiSPmx_1;
	double PSBijSPmx_1;
	double PSBikSPmx_1;
	double PSBjiSPmx_1;
	double PSBjjSPmx_1;
	double PSBkiSPmx_1;
	double PSBkkSPmx_1;
	double PSBiiSPmy_1;
	double PSBijSPmy_1;
	double PSBjiSPmy_1;
	double PSBjjSPmy_1;
	double PSBjkSPmy_1;
	double PSBkjSPmy_1;
	double PSBkkSPmy_1;
	double PSBiiSPmz_1;
	double PSBikSPmz_1;
	double PSBjjSPmz_1;
	double PSBjkSPmz_1;
	double PSBkiSPmz_1;
	double PSBkjSPmz_1;
	double PSBkkSPmz_1;
	double PSBiiSPe_1;
	double PSBii2SPe_1;
	double PSBii3SPe_1;
	double PSBijSPe_1;
	double PSBij2SPe_1;
	double PSBikSPe_1;
	double PSBik2SPe_1;
	double PSBjiSPe_1;
	double PSBji2SPe_1;
	double PSBjjSPe_1;
	double PSBjj2SPe_1;
	double PSBjj3SPe_1;
	double PSBjkSPe_1;
	double PSBjk2SPe_1;
	double PSBkiSPe_1;
	double PSBki2SPe_1;
	double PSBkjSPe_1;
	double PSBkj2SPe_1;
	double PSBkkSPe_1;
	double PSBkk2SPe_1;
	double PSBkk3SPe_1;
	double QSBiiSPmx_1;
	double QSBijSPmx_1;
	double QSBikSPmx_1;
	double QSBjiSPmx_1;
	double QSBjjSPmx_1;
	double QSBkiSPmx_1;
	double QSBkkSPmx_1;
	double QSBiiSPmy_1;
	double QSBijSPmy_1;
	double QSBjiSPmy_1;
	double QSBjjSPmy_1;
	double QSBjkSPmy_1;
	double QSBkjSPmy_1;
	double QSBkkSPmy_1;
	double QSBiiSPmz_1;
	double QSBikSPmz_1;
	double QSBjjSPmz_1;
	double QSBjkSPmz_1;
	double QSBkiSPmz_1;
	double QSBkjSPmz_1;
	double QSBkkSPmz_1;
	double QSBiiSPe_1;
	double QSBii2SPe_1;
	double QSBii3SPe_1;
	double QSBijSPe_1;
	double QSBij2SPe_1;
	double QSBikSPe_1;
	double QSBik2SPe_1;
	double QSBjiSPe_1;
	double QSBji2SPe_1;
	double QSBjjSPe_1;
	double QSBjj2SPe_1;
	double QSBjj3SPe_1;
	double QSBjkSPe_1;
	double QSBjk2SPe_1;
	double QSBkiSPe_1;
	double QSBki2SPe_1;
	double QSBkjSPe_1;
	double QSBkj2SPe_1;
	double QSBkkSPe_1;
	double QSBkk2SPe_1;
	double QSBkk3SPe_1;
	double GradSBirho_1;
	double GradSBjrho_1;
	double GradSBkrho_1;
	double GradSBimx_1;
	double GradSBjmx_1;
	double GradSBkmx_1;
	double GradSBimy_1;
	double GradSBjmy_1;
	double GradSBkmy_1;
	double GradSBimz_1;
	double GradSBjmz_1;
	double GradSBkmz_1;
	double GradSBie_1;
	double GradSBje_1;
	double GradSBke_1;
	double dQdPSBiiSPmx_1;
	double dQdPSBijSPmx_1;
	double dQdPSBikSPmx_1;
	double dQdPSBjiSPmx_1;
	double dQdPSBjjSPmx_1;
	double dQdPSBkiSPmx_1;
	double dQdPSBkkSPmx_1;
	double dQdPSBiiSPmy_1;
	double dQdPSBijSPmy_1;
	double dQdPSBjiSPmy_1;
	double dQdPSBjjSPmy_1;
	double dQdPSBjkSPmy_1;
	double dQdPSBkjSPmy_1;
	double dQdPSBkkSPmy_1;
	double dQdPSBiiSPmz_1;
	double dQdPSBikSPmz_1;
	double dQdPSBjjSPmz_1;
	double dQdPSBjkSPmz_1;
	double dQdPSBkiSPmz_1;
	double dQdPSBkjSPmz_1;
	double dQdPSBkkSPmz_1;
	double dQdPSBiiSPe_1;
	double dQdPSBii2SPe_1;
	double dQdPSBii3SPe_1;
	double dQdPSBijSPe_1;
	double dQdPSBij2SPe_1;
	double dQdPSBikSPe_1;
	double dQdPSBik2SPe_1;
	double dQdPSBjiSPe_1;
	double dQdPSBji2SPe_1;
	double dQdPSBjjSPe_1;
	double dQdPSBjj2SPe_1;
	double dQdPSBjj3SPe_1;
	double dQdPSBjkSPe_1;
	double dQdPSBjk2SPe_1;
	double dQdPSBkiSPe_1;
	double dQdPSBki2SPe_1;
	double dQdPSBkjSPe_1;
	double dQdPSBkj2SPe_1;
	double dQdPSBkkSPe_1;
	double dQdPSBkk2SPe_1;
	double dQdPSBkk3SPe_1;
	double dP1SBijSPmx_1;
	double dP1SBikSPmx_1;
	double dP1SBjiSPmx_1;
	double dP1SBkiSPmx_1;
	double dP1SBijSPmy_1;
	double dP1SBjiSPmy_1;
	double dP1SBjkSPmy_1;
	double dP1SBkjSPmy_1;
	double dP1SBikSPmz_1;
	double dP1SBjkSPmz_1;
	double dP1SBkiSPmz_1;
	double dP1SBkjSPmz_1;
	double dP1SBijSPe_1;
	double dP1SBij2SPe_1;
	double dP1SBikSPe_1;
	double dP1SBik2SPe_1;
	double dP1SBjiSPe_1;
	double dP1SBji2SPe_1;
	double dP1SBjkSPe_1;
	double dP1SBjk2SPe_1;
	double dP1SBkiSPe_1;
	double dP1SBki2SPe_1;
	double dP1SBkjSPe_1;
	double dP1SBkj2SPe_1;
	double dP2SBijSPmx_1;
	double dP2SBikSPmx_1;
	double dP2SBjiSPmx_1;
	double dP2SBkiSPmx_1;
	double dP2SBijSPmy_1;
	double dP2SBjiSPmy_1;
	double dP2SBjkSPmy_1;
	double dP2SBkjSPmy_1;
	double dP2SBikSPmz_1;
	double dP2SBjkSPmz_1;
	double dP2SBkiSPmz_1;
	double dP2SBkjSPmz_1;
	double dP2SBijSPe_1;
	double dP2SBij2SPe_1;
	double dP2SBikSPe_1;
	double dP2SBik2SPe_1;
	double dP2SBjiSPe_1;
	double dP2SBji2SPe_1;
	double dP2SBjkSPe_1;
	double dP2SBjk2SPe_1;
	double dP2SBkiSPe_1;
	double dP2SBki2SPe_1;
	double dP2SBkjSPe_1;
	double dP2SBkj2SPe_1;
	double dQ1SBijSPmx_1;
	double dQ1SBikSPmx_1;
	double dQ1SBjiSPmx_1;
	double dQ1SBkiSPmx_1;
	double dQ1SBijSPmy_1;
	double dQ1SBjiSPmy_1;
	double dQ1SBjkSPmy_1;
	double dQ1SBkjSPmy_1;
	double dQ1SBikSPmz_1;
	double dQ1SBjkSPmz_1;
	double dQ1SBkiSPmz_1;
	double dQ1SBkjSPmz_1;
	double dQ1SBijSPe_1;
	double dQ1SBij2SPe_1;
	double dQ1SBikSPe_1;
	double dQ1SBik2SPe_1;
	double dQ1SBjiSPe_1;
	double dQ1SBji2SPe_1;
	double dQ1SBjkSPe_1;
	double dQ1SBjk2SPe_1;
	double dQ1SBkiSPe_1;
	double dQ1SBki2SPe_1;
	double dQ1SBkjSPe_1;
	double dQ1SBkj2SPe_1;
	double dQ2SBijSPmx_1;
	double dQ2SBikSPmx_1;
	double dQ2SBjiSPmx_1;
	double dQ2SBkiSPmx_1;
	double dQ2SBijSPmy_1;
	double dQ2SBjiSPmy_1;
	double dQ2SBjkSPmy_1;
	double dQ2SBkjSPmy_1;
	double dQ2SBikSPmz_1;
	double dQ2SBjkSPmz_1;
	double dQ2SBkiSPmz_1;
	double dQ2SBkjSPmz_1;
	double dQ2SBijSPe_1;
	double dQ2SBij2SPe_1;
	double dQ2SBikSPe_1;
	double dQ2SBik2SPe_1;
	double dQ2SBjiSPe_1;
	double dQ2SBji2SPe_1;
	double dQ2SBjkSPe_1;
	double dQ2SBjk2SPe_1;
	double dQ2SBkiSPe_1;
	double dQ2SBki2SPe_1;
	double dQ2SBkjSPe_1;
	double dQ2SBkj2SPe_1;
	double mxSPSprime_1;
	double mySPSprime_1;
	double mzSPSprime_1;
	double eSPSprime_1;
	double rhoSPprime;
	double mxSPprime;
	double mySPprime;
	double mzSPprime;
	double eSPprime;
	double fxSPprime;
	double fySPprime;
	double fzSPprime;
	double pSPprime;
	double WAcc;
	double rhoTmp;
	double Aux_constraint1_Acci;
	double Aux_constraint1_Accj;
	double Aux_constraint1_Acck;
	double Aux_constraint2_Acci;


   	//Positions of the particle (Global index position)
	double positioni_p;
	double positionj_p;
	double positionk_p;
	double positioni;
	double positionj;
	double positionk;
	double positioniSPprime;
	double positionjSPprime;
	double positionkSPprime;


	//Region information
	int region;
	int interior;
	int newRegion; //The region to become when entering the domain (boundaries)

private:
	//Particle identifier
	int id;

	//Particle counter
	static int counter;

	//Temporal variable for order the particles in the neighbourhood of another particle
	double _distance;

};

#endif
