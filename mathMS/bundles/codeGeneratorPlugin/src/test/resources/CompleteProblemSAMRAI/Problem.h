#include "RefineClasses.h"
#include "TimeInterpolateOperator.h"
#include "TimeRefinementIntegrator.h"
#include "RefineSchedule.h"
#include "RefineAlgorithm.h"
#include "StandardRefineTransactionFactory.h"

#include "RefineTimeTransaction.h"

#include "SAMRAI/SAMRAI_config.h"

#include "SAMRAI/hier/Box.h"
#include "SAMRAI/geom/CartesianGridGeometry.h"
#include "SAMRAI/tbox/Database.h"
#include "SAMRAI/mesh/StandardTagAndInitStrategy.h"
#include "SAMRAI/hier/Index.h"
#include "SAMRAI/hier/Patch.h"
#include "SAMRAI/xfer/CoarsenAlgorithm.h"
#include "SAMRAI/xfer/CoarsenSchedule.h"
#include "boost/shared_ptr.hpp"
#include "MainRestartData.h"
#include "SAMRAI/algs/TimeRefinementLevelStrategy.h"
#include "SlicerDataWriter.h"
#include "SphereDataWriter.h"
#include "SAMRAI/appu/VisItDataWriter.h"
#include "TimeInterpolator.h"
#include "IntegrateDataWriter.h"

using namespace std;
using namespace SAMRAI;

#define DIMENSIONS 3

#define POINTS_Other 22
#define UNIONS_Other 40
//Common information for region: Other
const double OtherPoints[POINTS_Other][DIMENSIONS] = {{-3.405693190253193, 1.7764630317687988, 3.4574400074892004}, {-3.405693190253193, 2.608680009841919, 1.8241219408922165}, {-3.405693190253193, 2.3219170570373535, 0.013574946682926692}, {-3.405693190253193, 1.0257090330123901, -1.2826341502256426}, {-3.4056931902531926, -0.7848380208015442, -1.569396149355892}, {-3.405693190253193, -2.4181559085845947, -0.7371780983991658}, {-3.405693190253193, -3.250372886657715, 0.896138936292407}, {-3.405693190253193, -2.9636099338531494, 2.706686962407109}, {-3.405693190253193, -1.6674009561538696, 4.002894986432072}, {-3.405693190253193, 0.14314700663089752, 4.289656985562321}, {2.526392800652568, 1.7764650583267212, 3.457437861721989}, {2.5263928006525678, 2.6086809635162354, 1.8241189606599773}, {2.526392800652568, 2.321916103363037, 0.013570953171726408}, {2.5263928006525678, 1.0257049798965454, -1.2826360575742748}, {2.5263928006525687, -0.7848430275917053, -1.5693940035886804}, {2.5263937543268846, -2.4181599617004395, -0.7371740452833214}, {2.526393754326885, -3.2503740787506104, 0.8961469363533224}, {2.526393754326885, -2.9636070728302, 2.706693995755192}, {2.5263928006525678, -1.6673940420150757, 4.002899039547916}, {2.526392800652568, 0.14315499365329742, 4.289656031888004}, {-3.405693190253193, -0.32084599137306213, 1.3601309247426951}, {2.526392800652568, -0.32084599137306213, 1.3601309247426954}}
;
const int OtherUnions[UNIONS_Other][DIMENSIONS] = {{20, 0, 1}, {21, 11, 10}, {20, 1, 2}, {21, 12, 11}, {20, 2, 3}, {21, 13, 12}, {20, 3, 4}, {21, 14, 13}, {20, 4, 5}, {21, 15, 14}, {20, 5, 6}, {21, 16, 15}, {20, 6, 7}, {21, 17, 16}, {20, 7, 8}, {21, 18, 17}, {20, 8, 9}, {21, 19, 18}, {9, 0, 20}, {21, 10, 19}, {19, 0, 9}, {10, 0, 19}, {8, 18, 9}, {18, 19, 9}, {7, 17, 8}, {17, 18, 8}, {6, 16, 7}, {16, 17, 7}, {5, 15, 6}, {15, 16, 6}, {4, 14, 5}, {14, 15, 5}, {3, 13, 4}, {13, 14, 4}, {2, 12, 3}, {12, 13, 3}, {1, 11, 2}, {11, 12, 2}, {0, 10, 1}, {10, 11, 1}};
#define POINTS_Soil 22
#define UNIONS_Soil 40
//Common information for region: Soil
const double SoilPoints[POINTS_Soil][DIMENSIONS] = {{-3.405693190253193, 1.7764630317687988, 3.4574400074892004}, {-3.405693190253193, 2.608680009841919, 1.8241219408922165}, {-3.405693190253193, 2.3219170570373535, 0.013574946682926692}, {-3.405693190253193, 1.0257090330123901, -1.2826341502256426}, {-3.4056931902531926, -0.7848380208015442, -1.569396149355892}, {-3.405693190253193, -2.4181559085845947, -0.7371780983991658}, {-3.405693190253193, -3.250372886657715, 0.896138936292407}, {-3.405693190253193, -2.9636099338531494, 2.706686962407109}, {-3.405693190253193, -1.6674009561538696, 4.002894986432072}, {-3.405693190253193, 0.14314700663089752, 4.289656985562321}, {2.526392800652568, 1.7764650583267212, 3.457437861721989}, {2.5263928006525678, 2.6086809635162354, 1.8241189606599773}, {2.526392800652568, 2.321916103363037, 0.013570953171726408}, {2.5263928006525678, 1.0257049798965454, -1.2826360575742748}, {2.5263928006525687, -0.7848430275917053, -1.5693940035886804}, {2.5263937543268846, -2.4181599617004395, -0.7371740452833214}, {2.526393754326885, -3.2503740787506104, 0.8961469363533224}, {2.526393754326885, -2.9636070728302, 2.706693995755192}, {2.5263928006525678, -1.6673940420150757, 4.002899039547916}, {2.526392800652568, 0.14315499365329742, 4.289656031888004}, {-3.405693190253193, -0.32084599137306213, 1.3601309247426951}, {2.526392800652568, -0.32084599137306213, 1.3601309247426954}}
;
const int SoilUnions[UNIONS_Soil][DIMENSIONS] = {{20, 0, 1}, {21, 11, 10}, {20, 1, 2}, {21, 12, 11}, {20, 2, 3}, {21, 13, 12}, {20, 3, 4}, {21, 14, 13}, {20, 4, 5}, {21, 15, 14}, {20, 5, 6}, {21, 16, 15}, {20, 6, 7}, {21, 17, 16}, {20, 7, 8}, {21, 18, 17}, {20, 8, 9}, {21, 19, 18}, {9, 0, 20}, {21, 10, 19}, {19, 0, 9}, {10, 0, 19}, {8, 18, 9}, {18, 19, 9}, {7, 17, 8}, {17, 18, 8}, {6, 16, 7}, {16, 17, 7}, {5, 15, 6}, {15, 16, 6}, {4, 14, 5}, {14, 15, 5}, {3, 13, 4}, {13, 14, 4}, {2, 12, 3}, {12, 13, 3}, {1, 11, 2}, {11, 12, 2}, {0, 10, 1}, {10, 11, 1}};


class Problem : 
   public mesh::StandardTagAndInitStrategy,
   public xfer::RefinePatchStrategy,
   public xfer::CoarsenPatchStrategy,
   public algs::TimeRefinementLevelStrategy
{
public:
	/*
	 * Constructor of the problem.
	 */
	Problem(
		const string& object_name,
		const tbox::Dimension& dim,
		std::shared_ptr<tbox::Database>& input_db,
		std::shared_ptr<geom::CartesianGridGeometry >& grid_geom, 
	   	std::shared_ptr<hier::PatchHierarchy >& patch_hierarchy, 
		const double dt, 
		hier::BoxContainer& ba,
		const bool init_from_files,
		const vector<string> full_writer_variables,
		const vector<vector<string> > sliceVariables,
		const vector<vector<string> > sphereVariables,
		const vector<vector<string> > integralVariables);
  
	/*
	 * Destructor.
	 */
	~Problem();

	/*
	 * Block of subcycling inherited methods.
	 */
	void initializeLevelIntegrator(
	      const std::shared_ptr<mesh::GriddingAlgorithmStrategy>& gridding_alg);

	double getLevelDt(
	      const std::shared_ptr<hier::PatchLevel>& level,
	      const double dt_time,
	      const bool initial_time);

	double getMaxFinerLevelDt(
	      const int finer_level_number,
	      const double coarse_dt,
	      const hier::IntVector& ratio_to_coarser);

	double advanceLevel(
	      const std::shared_ptr<hier::PatchLevel>& level,
	      const std::shared_ptr<hier::PatchHierarchy>& hierarchy,
	      const double current_time,
	      const double new_time,
	      const bool first_step,
	      const bool last_step,
	      const bool regrid_advance = false);

	void standardLevelSynchronization(
	      const std::shared_ptr<hier::PatchHierarchy>& hierarchy,
	      const int coarsest_level,
	      const int finest_level,
	      const double sync_time,
	      const std::vector<double>& old_times);

	void synchronizeNewLevels(
	      const std::shared_ptr<hier::PatchHierarchy>& hierarchy,
	      const int coarsest_level,
	      const int finest_level,
	      const double sync_time,
	      const bool initial_time);

	void resetTimeDependentData(
	      const std::shared_ptr<hier::PatchLevel>& level,
	      const double new_time,
	      const bool can_be_refined);

	void resetDataToPreadvanceState(
	      const std::shared_ptr<hier::PatchLevel>& level);

	bool usingRefinedTimestepping() const
	{
	      return d_refinedTimeStepping;
	} 

	/*
	 * Register the current iteration in the class.
	 */
	void registerIteration(int iter) {
		simPlat_iteration = iter;
	}

	/*
	 * Checks if the point is close to the region when using tappering.
	 */
	bool neighbourTappering(const double* fov, const int ibase, const int jbase, const int kbase, const int ilast, const int jlast, const int klast, const int stencil, const int limit) const;

	/*
	 * Initialize the data from a given level.
   	 */
	virtual void initializeLevelData(
		const std::shared_ptr<hier::PatchHierarchy >& hierarchy ,
		const int level_number ,
		const double init_data_time ,
		const bool can_be_refined ,
		const bool initial_time ,
		const std::shared_ptr<hier::PatchLevel >& old_level=std::shared_ptr<hier::PatchLevel>() ,
		const bool allocate_data = true);

	/*
	 * Reset the hierarchy-dependent internal information.
	 */
	virtual void resetHierarchyConfiguration(
		const std::shared_ptr<hier::PatchHierarchy >& new_hierarchy ,
		int coarsest_level ,
		int finest_level);

	/*
	 * Checks the finalization conditions              
	 */
	bool checkFinalization(
		const double simPlat_time, 
		const double simPlat_dt);

	/*
	 * This method sets the physical boundary conditions.
	*/
	void setPhysicalBoundaryConditions(
		hier::Patch& patch,
		const double fill_time,
		const hier::IntVector& ghost_width_to_fill);
	/*
	 * Set up external plotter to plot internal data from this class.        
	 * Tell the plotter about the refinement ratios.  Register variables     
	 * appropriate for plotting.                                            
	 */
	int setupPlotter(appu::VisItDataWriter &plotter ) const;
	int setupSlicePlotter(vector<std::shared_ptr<SlicerDataWriter> > &plotters) const;
	int setupSpherePlotter(vector<std::shared_ptr<SphereDataWriter> > &plotters) const;
	int setupIntegralPlotter(vector<std::shared_ptr<IntegrateDataWriter> > &plotters) const;


	/*
	 * Map data on a patch. This mapping is done only at the begining of the simulation.
	 */
	void mapDataOnPatch(const double time, const bool initial_time, const int ln, const std::shared_ptr< hier::PatchLevel >& level);

	/*
	 * Sets the limit for the checkstencil routine
	 */
	void setStencilLimits(std::shared_ptr< hier::Patch > patch, int i, int j, int k, int v) const;

	/*
	 * Checks if the point has a stencil width
	 */
	void checkStencil(std::shared_ptr< hier::Patch > patch, int i, int j, int k, int v) const;

	/*
	 * Flood-Fill algorithm
	 */
	void floodfill(std::shared_ptr< hier::Patch > patch, int i, int j, int k, int pred, int seg) const;

	/*
	 * FOV correction for AMR
	 */
	void correctFOVS(const std::shared_ptr< hier::PatchLevel >& level);



	/*
	 * Interphase mapping. Calculates the FOV and its variables.
	 */
	void interphaseMapping(const double time, const bool initial_time, const int ln, const std::shared_ptr< hier::PatchLevel >& level, const int remesh);




	/*
	 * Initialize data on a patch. This initialization is done only at the begining of the simulation.
	 */
	void initializeDataOnPatch(
		hier::Patch& patch,
		const double time,
       		const bool initial_time);

	/*
	 *  Cell tagging routine - tag cells that require refinement based on a provided condition. 
	 */
	void applyGradientDetector(
	   	const std::shared_ptr< hier::PatchHierarchy >& hierarchy, 
	   	const int level_number,
	   	const double time, 
	   	const int tag_index,
	   	const bool initial_time,
	   	const bool uses_richardson_extrapolation_too);

	/*
	* Return maximum stencil width needed for user-defined
	* data interpolation operations.  Default is to return
	* zero, assuming no user-defined operations provided.
	*/
	hier::IntVector getRefineOpStencilWidth(const tbox::Dimension &dim) const
	{
		return hier::IntVector::getZero(dim);
	}

	/*
	* Pre- and post-processing routines for implementing user-defined
	* spatial interpolation routines applied to variables.  The 
	* interpolation routines are used in the MOL AMR algorithm
	* for filling patch ghost cells before advancing data on a level
	* and after regridding a level to fill portions of the new level
	* from some coarser level.  These routines are called automatically
	* from within patch boundary filling schedules; thus, some concrete
	* function matching these signatures must be provided in the user's
	* patch model.  However, the routines only need to perform some 
	* operations when "USER_DEFINED_REFINE" is given as the interpolation 
	* method for some variable when the patch model registers variables
	* with the MOL integration algorithm, typically.  If the 
	* user does not provide operations that refine such variables in either 
	* of these routines, then they will not be refined.
	*
	* The order in which these operations are used in each patch 
	* boundary filling schedule is:
	* 
	* - \b (1) {Call user's preprocessRefine() routine.}
	* - \b (2) {Refine all variables with standard interpolation operators.}
	* - \b (3) {Call user's postprocessRefine() routine.}
	* 
	* 
	* Also, user routines that implement these functions must use 
	* data corresponding to the d_scratch context on both coarse and
	* fine patches.
	*/
	virtual void preprocessRefine(
		hier::Patch& fine,
		const hier::Patch& coarse,
                const hier::Box& fine_box,
                const hier::IntVector& ratio)
	{
		NULL_USE(fine);
		NULL_USE(coarse);
		NULL_USE(fine_box);
		NULL_USE(ratio);
	}
	virtual void postprocessRefine(
		hier::Patch& fine,
                const hier::Patch& coarse,
                const hier::Box& fine_box,
                const hier::IntVector& ratio)
	{
		NULL_USE(fine);
		NULL_USE(coarse);
		NULL_USE(fine_box);
		NULL_USE(ratio);
	}


	/*
	* Return maximum stencil width needed for user-defined
	* data coarsen operations.  Default is to return
	* zero, assuming no user-defined operations provided.
	*/
	hier::IntVector getCoarsenOpStencilWidth( const tbox::Dimension &dim ) const {
		return hier::IntVector::getZero(dim);
	}

	/*
	* Pre- and post-processing routines for implementing user-defined
	* spatial coarsening routines applied to variables.  The coarsening 
	* routines are used in the MOL AMR algorithm synchronizing 
	* coarse and fine levels when they have been integrated to the same
	* point.  These routines are called automatically from within the 
	* data synchronization coarsen schedules; thus, some concrete
	* function matching these signatures must be provided in the user's
	* patch model.  However, the routines only need to perform some
	* operations when "USER_DEFINED_COARSEN" is given as the coarsening
	* method for some variable when the patch model registers variables
	* with the MOL level integration algorithm, typically.  If the
	* user does not provide operations that coarsen such variables in either
	* of these routines, then they will not be coarsened.
	*
	* The order in which these operations are used in each coarsening
	* schedule is:
	* 
	* - \b (1) {Call user's preprocessCoarsen() routine.}
	* - \b (2) {Coarsen all variables with standard coarsening operators.}
	* - \b (3) {Call user's postprocessCoarsen() routine.}
	* 
	*
	* Also, user routines that implement these functions must use
	* corresponding to the d_new context on both coarse and fine patches
	* for time-dependent quantities.
	*/
	virtual void preprocessCoarsen(
		hier::Patch& coarse,
                const hier::Patch& fine,
                const hier::Box& coarse_box,
                const hier::IntVector& ratio)
	{
		NULL_USE(fine);
		NULL_USE(coarse);
		NULL_USE(coarse_box);
		NULL_USE(ratio);
	}
	virtual void postprocessCoarsen(
		hier::Patch& coarse,
                const hier::Patch& fine,
                const hier::Box& coarse_box,
                const hier::IntVector& ratio)
	{
		NULL_USE(fine);
		NULL_USE(coarse);
		NULL_USE(coarse_box);
		NULL_USE(ratio);
	}

	/*
	 * Computes the dt to be used
	 */
	double computeDt() const;

	

	/*
	 * Checks if the point has to be stalled
	 */
	bool checkStalled(std::shared_ptr< hier::Patch > patch, int i, int j, int k, int v) const;



	hier::BoxContainer d_regridding_boxes;

private:	 
	//Variables for the refine and coarsen algorithms

	std::shared_ptr< xfer::RefineAlgorithm > d_bdry_fill_init;
	std::vector< std::shared_ptr< xfer::RefineSchedule > > d_bdry_sched_postCoarsen;
	TimeInterpolator * time_interpolate_operator;
	std::shared_ptr< xfer::RefineAlgorithm > d_bdry_fill_advance1;
	std::vector< std::shared_ptr< xfer::RefineSchedule > > d_bdry_sched_advance1;
	std::shared_ptr< xfer::RefineAlgorithm > d_bdry_fill_advance4;
	std::vector< std::shared_ptr< xfer::RefineSchedule > > d_bdry_sched_advance4;
	std::shared_ptr< xfer::RefineAlgorithm > d_bdry_fill_advance6;
	std::vector< std::shared_ptr< xfer::RefineSchedule > > d_bdry_sched_advance6;
	std::shared_ptr< xfer::RefineAlgorithm > d_bdry_fill_advance7;
	std::vector< std::shared_ptr< xfer::RefineSchedule > > d_bdry_sched_advance7;

	std::shared_ptr< xfer::RefineAlgorithm > d_mapping_fill;
	std::shared_ptr< xfer::CoarsenAlgorithm > d_coarsen_algorithm;
	std::vector< std::shared_ptr< xfer::CoarsenSchedule > > d_coarsen_schedule;

	std::shared_ptr< xfer::RefineAlgorithm > d_fill_new_level;
	// Number of current substep
	std::vector< int > d_sim_substep;

	//Object name
	std::string d_object_name;

	//Pointers to the grid geometry and the patch hierarchy
   	std::shared_ptr<geom::CartesianGridGeometry > d_grid_geometry;
	std::shared_ptr<hier::PatchHierarchy > d_patch_hierarchy;

	//Identifiers of the fields and auxiliary fields
	int d_Ex_id, d_Ex_p_id, d_Ey_id, d_Ey_p_id, d_Ez_id, d_Ez_p_id, d_Bx_id, d_Bx_p_id, d_By_id, d_By_p_id, d_Bz_id, d_Bz_p_id, d_q_id, d_Jx_id, d_Jx_p_id, d_Jy_id, d_Jy_p_id, d_Jz_id, d_Jz_p_id, d_constraint1_4_id, d_constraint2_4_id, d_constraint1_5_id, d_constraint2_5_id, d_FluxSBjEx_7_id, d_FluxSBkEx_7_id, d_FluxSBiEy_7_id, d_FluxSBkEy_7_id, d_FluxSBiEz_7_id, d_FluxSBjEz_7_id, d_FluxSBjBx_7_id, d_FluxSBkBx_7_id, d_FluxSBiBy_7_id, d_FluxSBkBy_7_id, d_FluxSBiBz_7_id, d_FluxSBjBz_7_id, d_SpeedSBjEx_7_id, d_SpeedSBkEx_7_id, d_SpeedSBiEy_7_id, d_SpeedSBkEy_7_id, d_SpeedSBiEz_7_id, d_SpeedSBjEz_7_id, d_SpeedSBjBx_7_id, d_SpeedSBkBx_7_id, d_SpeedSBiBy_7_id, d_SpeedSBkBy_7_id, d_SpeedSBiBz_7_id, d_SpeedSBjBz_7_id, d_K1SBEx_7_id, d_K1SBEy_7_id, d_K1SBEz_7_id, d_K1SBBx_7_id, d_K1SBBy_7_id, d_K1SBBz_7_id, d_ExSPprime_7_id, d_EySPprime_7_id, d_EzSPprime_7_id, d_BxSPprime_7_id, d_BySPprime_7_id, d_BzSPprime_7_id, d_qSPprime_7_id, d_JxSPprime_7_id, d_JySPprime_7_id, d_JzSPprime_7_id, d_K2SBEx_7_id, d_K2SBEy_7_id, d_K2SBEz_7_id, d_K2SBBx_7_id, d_K2SBBy_7_id, d_K2SBBz_7_id, d_ExSPprimeprime_7_id, d_EySPprimeprime_7_id, d_EzSPprimeprime_7_id, d_BxSPprimeprime_7_id, d_BySPprimeprime_7_id, d_BzSPprimeprime_7_id, d_K3SBEx_7_id, d_K3SBEy_7_id, d_K3SBBy_7_id, d_ExSPprime_id, d_EySPprime_id, d_EzSPprime_id, d_BxSPprime_id, d_BySPprime_id, d_BzSPprime_id, d_FluxSBjEx_1_id, d_FluxSBkEx_1_id, d_FluxSBiEy_1_id, d_FluxSBkEy_1_id, d_FluxSBiEz_1_id, d_FluxSBjEz_1_id, d_FluxSBjBx_1_id, d_FluxSBkBx_1_id, d_FluxSBiBy_1_id, d_FluxSBkBy_1_id, d_FluxSBiBz_1_id, d_FluxSBjBz_1_id, d_SpeedSBjEx_1_id, d_SpeedSBkEx_1_id, d_SpeedSBiEy_1_id, d_SpeedSBkEy_1_id, d_SpeedSBiEz_1_id, d_SpeedSBjEz_1_id, d_SpeedSBjBx_1_id, d_SpeedSBkBx_1_id, d_SpeedSBiBy_1_id, d_SpeedSBkBy_1_id, d_SpeedSBiBz_1_id, d_SpeedSBjBz_1_id, d_K1SBEx_1_id, d_K1SBEy_1_id, d_K1SBEz_1_id, d_K1SBBx_1_id, d_K1SBBy_1_id, d_K1SBBz_1_id, d_ExSPprime_1_id, d_EySPprime_1_id, d_EzSPprime_1_id, d_BxSPprime_1_id, d_BySPprime_1_id, d_BzSPprime_1_id, d_ExSPprime_2_id, d_EySPprime_2_id, d_EzSPprime_2_id, d_BxSPprime_2_id, d_BySPprime_2_id, d_BzSPprime_2_id, d_ExSPprimeprime_2_id, d_EySPprimeprime_2_id, d_EzSPprimeprime_2_id, d_BxSPprimeprime_2_id, d_BySPprimeprime_2_id, d_BzSPprimeprime_2_id, d_FluxSBjEx_4_id, d_FluxSBkEx_4_id, d_FluxSBiEy_4_id, d_FluxSBkEy_4_id, d_FluxSBiEz_4_id, d_FluxSBjEz_4_id, d_FluxSBjBx_4_id, d_FluxSBkBx_4_id, d_FluxSBiBy_4_id, d_FluxSBkBy_4_id, d_FluxSBiBz_4_id, d_FluxSBjBz_4_id, d_SpeedSBjEx_4_id, d_SpeedSBkEx_4_id, d_SpeedSBiEy_4_id, d_SpeedSBkEy_4_id, d_SpeedSBiEz_4_id, d_SpeedSBjEz_4_id, d_SpeedSBjBx_4_id, d_SpeedSBkBx_4_id, d_SpeedSBiBy_4_id, d_SpeedSBkBy_4_id, d_SpeedSBiBz_4_id, d_SpeedSBjBz_4_id, d_K1SBEx_4_id, d_K1SBEy_4_id, d_K1SBEz_4_id, d_K1SBBx_4_id, d_K1SBBy_4_id, d_K1SBBz_4_id, d_ExSPprime_4_id, d_EySPprime_4_id, d_EzSPprime_4_id, d_BxSPprime_4_id, d_BySPprime_4_id, d_BzSPprime_4_id, d_K2SBEx_4_id, d_K2SBEy_4_id, d_K2SBEz_4_id, d_K2SBBx_4_id, d_K2SBBy_4_id, d_K2SBBz_4_id, d_ExSPprimeprime_4_id, d_EySPprimeprime_4_id, d_EzSPprimeprime_4_id, d_BxSPprimeprime_4_id, d_BySPprimeprime_4_id, d_BzSPprimeprime_4_id, d_K3SBEx_4_id, d_K3SBEy_4_id, d_K3SBEz_4_id, d_K3SBBx_4_id, d_K3SBBy_4_id, d_K3SBBz_4_id, d_FluxSBjEx_5_id, d_FluxSBkEx_5_id, d_FluxSBiEy_5_id, d_FluxSBkEy_5_id, d_FluxSBiEz_5_id, d_FluxSBjEz_5_id, d_FluxSBjBx_5_id, d_FluxSBkBx_5_id, d_FluxSBiBy_5_id, d_FluxSBkBy_5_id, d_FluxSBiBz_5_id, d_FluxSBjBz_5_id, d_SpeedSBjEx_5_id, d_SpeedSBkEx_5_id, d_SpeedSBiEy_5_id, d_SpeedSBkEy_5_id, d_SpeedSBiEz_5_id, d_SpeedSBjEz_5_id, d_SpeedSBjBx_5_id, d_SpeedSBkBx_5_id, d_SpeedSBiBy_5_id, d_SpeedSBkBy_5_id, d_SpeedSBiBz_5_id, d_SpeedSBjBz_5_id, d_K1SBEx_5_id, d_K1SBEy_5_id, d_K1SBEz_5_id, d_K1SBBx_5_id, d_K1SBBy_5_id, d_K1SBBz_5_id, d_ExSPprime_5_id, d_EySPprime_5_id, d_EzSPprime_5_id, d_BxSPprime_5_id, d_BySPprime_5_id, d_BzSPprime_5_id, d_K2SBEx_5_id, d_K2SBEy_5_id, d_K2SBEz_5_id, d_K2SBBx_5_id, d_K2SBBy_5_id, d_K2SBBz_5_id, d_ExSPprimeprime_5_id, d_EySPprimeprime_5_id, d_EzSPprimeprime_5_id, d_BxSPprimeprime_5_id, d_BySPprimeprime_5_id, d_BzSPprimeprime_5_id, d_K3SBEx_5_id, d_K3SBEy_5_id, d_K3SBEz_5_id, d_K3SBBx_5_id, d_K3SBBy_5_id, d_K3SBBz_5_id, d_d_i_Ey_id, d_d_j_Ey_id, d_d_k_Ey_id, d_d_i_Ex_id, d_d_j_Ex_id, d_d_k_Ex_id, d_stalled_1_id, d_stalled_2_id, d_stalled_4_id, d_stalled_5_id, d_stalled_7_id, d_stalled_8_id;

	//Parameter variables
	double c;
	double mu;


	//mapping fields
	int d_nonSync_id, d_interior_id, d_interior_i_id, d_interior_j_id, d_interior_k_id, d_FOV_1_id, d_FOV_2_id, d_FOV_4_id, d_FOV_5_id, d_FOV_7_id, d_FOV_8_id, d_FOV_xLower_id, d_FOV_xUpper_id, d_FOV_yLower_id, d_FOV_yUpper_id, d_FOV_zLower_id, d_FOV_zUpper_id;

	//Stencils of the discretization method variable
	int d_ghost_width, d_regionMinThickness;

	//initial dt
	double initial_dt;

   	const tbox::Dimension d_dim;

	//Subcycling variables
   	bool d_refinedTimeStepping, d_tappering;

	//Current iteration
	int simPlat_iteration;

	//Initialization from files
	bool d_init_from_files;

	//Variables to dump
	vector<string> d_full_writer_variables;
	vector<vector<string> > d_sliceVariables;
	vector<vector<string> > d_sphereVariables;

	vector<vector<string> > d_integralVariables;
	int d_mask_id;



	//regridding options
	std::shared_ptr<tbox::Database> regridding_db;
	bool d_regridding;
	std::string d_regridding_field, d_regridding_type;
	double d_regridding_threshold, d_regridding_compressionFactor, d_regridding_mOffset;
	int d_regridding_level_threshold;

	//Gets the coarser patch that includes the box.
	const std::shared_ptr<hier::Patch >& getCoarserPatch(
		const std::shared_ptr< hier::PatchLevel >& level,
		const hier::Box interior, 
		const hier::IntVector ratio);

	static bool Equals(double d1, double d2);
	static inline int GetExpoBase2(double d);
};


