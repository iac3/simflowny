#include "boost/shared_ptr.hpp"
#include "SAMRAI/tbox/Utilities.h"
#include <string>
#include <math.h>
#include <vector>


#define MIN(X,Y) ((X) < (Y) ? (X) : (Y))
#define MAX(X,Y) ((X) > (Y) ? (X) : (Y))
#define SIGN(X) (((X) > 0) - ((X) < 0))
#define greaterThan(a,b) (!((fabs((a) - (b))/1.0E-9 > 10 ? false: (floor(fabs((a) - (b))/1.0E-9) < 1)) || (a)<(b)))
#define lessThan(a,b) (!((fabs((a) - (b))/1.0E-9 > 10 ? false : (floor(fabs((a) - (b))/1.0E-9) < 1)) || (b)<(a)))
#define greaterEq(a,b) ((fabs((a) - (b))/1.0E-9 > 10 ? false : (floor(fabs((a) - (b))/1.0E-9) < 1)) || (b)<(a))
#define lessEq(a,b) ((fabs((a) - (b))/1.0E-9 > 10 ? false: (floor(fabs((a) - (b))/1.0E-9) < 1)) || (a)<(b))
#define equalsEq(a,b) ((fabs((a) - (b))/1.0E-9 > 10 ? false: (floor(fabs((a) - (b))/1.0E-9) < 1)))

double readFromFile1D(std::vector<double> position1, std::vector<double> data, double coord1);
double readFromFile2D(std::vector<double> position1, std::vector<double> position2, std::vector<std::vector<double> > data, double coord1, double coord2);
double readFromFile3D(std::vector<double> position1, std::vector<double> position2, std::vector<double> position3, std::vector<std::vector<std::vector<double> > > data, double coord1, double coord2, double coord3);

#define vector(v, i, j, k) (v)[i+ilast*(j+jlast*(k))]

#define POLINT_MACRO_LINEAR_1(y1, y2, i_ext, s_ext) (-i_ext*y1 + i_ext*y2 + s_ext*y2)
#define POLINT_MACRO_LINEAR_2(y1, y2, i_ext, s_ext) (-i_ext*y1 + i_ext*y2 - s_ext*y1)
#define POLINT_MACRO_QUADRATIC_1(y1,y2,y3, i_ext, s_ext) (0.5*(i_ext*i_ext*y1-2*i_ext*i_ext*y2+i_ext*i_ext*y3+i_ext*s_ext*y1-4*i_ext*s_ext*y2+3*i_ext*s_ext*y3+2*s_ext*s_ext*y3)/(s_ext*s_ext))
#define POLINT_MACRO_QUADRATIC_2(y1,y2,y3, i_ext, s_ext) (0.5*(i_ext*i_ext*y1-2*i_ext*i_ext*y2+i_ext*i_ext*y3+3*i_ext*s_ext*y1-4*i_ext*s_ext*y2+i_ext*s_ext*y3+2*s_ext*s_ext*y1)/(s_ext*s_ext))
#define POLINT_MACRO_CUBIC_1(y1,y2,y3, y4, i_ext, s_ext) (-(1.0/6.0)*(i_ext*i_ext*i_ext*y1-3*i_ext*i_ext*i_ext*y2+3*i_ext*i_ext*i_ext*y3-i_ext*i_ext*i_ext*y4+3*i_ext*i_ext*s_ext*y1-12*i_ext*i_ext*s_ext*y2+15*i_ext*i_ext*s_ext*y3-6*i_ext*i_ext*s_ext*y4+2*i_ext*s_ext*s_ext*y1-9*i_ext*s_ext*s_ext*y2+18*i_ext*s_ext*s_ext*y3-11*i_ext*s_ext*s_ext*y4-6*s_ext*s_ext*s_ext*y4)/(s_ext*s_ext*s_ext))
#define POLINT_MACRO_CUBIC_2(y1,y2,y3, y4, i_ext, s_ext) ((1.0/6.0)*(i_ext*i_ext*i_ext*y1-3*i_ext*i_ext*i_ext*y2+3*i_ext*i_ext*i_ext*y3-i_ext*i_ext*i_ext*y4+6*i_ext*i_ext*s_ext*y1-15*i_ext*i_ext*s_ext*y2+12*i_ext*i_ext*s_ext*y3-3*i_ext*i_ext*s_ext*y4+11*i_ext*s_ext*s_ext*y1-18*i_ext*s_ext*s_ext*y2+9*i_ext*s_ext*s_ext*y3-2*i_ext*s_ext*s_ext*y4+6*s_ext*s_ext*s_ext*y1)/(s_ext*s_ext*s_ext))

inline double RK3(int parstep, double* paru, double parsources, double* parK1, double* parK3, int pari, int parj, int park, const double* dx, const double simPlat_dt, const int ilast, const int jlast) {

	if (greaterEq(parstep, 1.0) && lessEq(parstep, 3.0)) {
		return simPlat_dt * parsources;
	}
	if (equalsEq(parstep, 4.0)) {
		return vector(paru, pari, parj, park) + 0.25 * (vector(parK1, pari, parj, park) + 3.0 * vector(parK3, pari, parj, park));
	}

};



