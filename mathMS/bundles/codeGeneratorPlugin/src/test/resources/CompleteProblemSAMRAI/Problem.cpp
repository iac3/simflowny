#include "Problem.h"
#include "Functions.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <stack>
#include "hdf5.h"
#include <gsl/gsl_rng.h>
#include "SAMRAI/pdat/NodeData.h"
#include "SAMRAI/pdat/NodeVariable.h"
#include "SAMRAI/pdat/CellVariable.h"
#include "LagrangianPolynomicRefine.h"


#include "SAMRAI/tbox/Array.h"
#include "SAMRAI/hier/BoundaryBox.h"
#include "SAMRAI/hier/BoxContainer.h"
#include "SAMRAI/geom/CartesianPatchGeometry.h"
#include "SAMRAI/hier/VariableDatabase.h"
#include "SAMRAI/hier/PatchDataRestartManager.h"
#include "SAMRAI/hier/Index.h"
#include "SAMRAI/tbox/PIO.h"
#include "SAMRAI/tbox/Utilities.h"
#include "SAMRAI/tbox/Timer.h"
#include "SAMRAI/tbox/TimerManager.h"

#define MIN(X,Y) ((X) < (Y) ? (X) : (Y))
#define MAX(X,Y) ((X) > (Y) ? (X) : (Y))
#define SIGN(X) (((X) > 0) - ((X) < 0))
#define isEven(a) ((a) % 2 == 0 ? true : false)
#define greaterThan(a,b) (!((fabs((a) - (b))/1.0E-9 > 10 ? false: (floor(fabs((a) - (b))/1.0E-9) < 1)) || (a)<(b)))
#define lessThan(a,b) (!((fabs((a) - (b))/1.0E-9 > 10 ? false : (floor(fabs((a) - (b))/1.0E-9) < 1)) || (b)<(a)))
#define greaterEq(a,b) ((fabs((a) - (b))/1.0E-9 > 10 ? false : (floor(fabs((a) - (b))/1.0E-9) < 1)) || (b)<(a))
#define lessEq(a,b) ((fabs((a) - (b))/1.0E-9 > 10 ? false: (floor(fabs((a) - (b))/1.0E-9) < 1)) || (a)<(b))
#define equalsEq(a,b) ((fabs((a) - (b))/1.0E-9 > 10 ? false: (floor(fabs((a) - (b))/1.0E-9) < 1)))

#define vector(v, i, j, k) (v)[i+ilast*(j+jlast*(k))]
#define vectorT(v, i, j, k) (v)[i+itlast*(j+jtlast*(k))]
#define indexiOf(i) (i - (int)(i/((boxlast(0)-boxfirst(0)+1)*(boxlast(1)-boxfirst(1)+1)))*((boxlast(0)-boxfirst(0)+1)*(boxlast(1)-boxfirst(1)+1)) - (int)((i - (int)(i/((boxlast(0)-boxfirst(0)+1)*(boxlast(1)-boxfirst(1)+1)))*((boxlast(0)-boxfirst(0)+1)*(boxlast(1)-boxfirst(1)+1)))/(boxlast(0)-boxfirst(0)+1))*(boxlast(0)-boxfirst(0)+1))
#define indexjOf(i) ((int)(i - (int)((i/((boxlast(0)-boxfirst(0)+1)*(boxlast(1)-boxfirst(1)+1)))*((boxlast(0)-boxfirst(0)+1)*(boxlast(1)-boxfirst(1)+1)))/(boxlast(0)-boxfirst(0)+1)))
#define indexkOf(i) ((int)(i/((boxlast(0)-boxfirst(0)+1)*(boxlast(1)-boxfirst(1)+1))))



#define FDOCi(paru, parflux, parspeed, pari, parj, park, dx, simPlat_dt, ilast, jlast) (((MAX(vector(parspeed, (pari) + 1, (parj), (park)), vector(parspeed, (pari) + 2, (parj), (park))) * (vector(paru, (pari) + 2, (parj), (park)) - vector(paru, (pari) + 1, (parj), (park))) / 6.0 - MAX(vector(parspeed, (pari), (parj), (park)), vector(parspeed, (pari) + 1, (parj), (park))) * (vector(paru, (pari) + 1, (parj), (park)) - vector(paru, (pari), (parj), (park))) / 2.0 + MAX(vector(parspeed, (pari) - 1, (parj), (park)), vector(parspeed, (pari), (parj), (park))) * (vector(paru, (pari), (parj), (park)) - vector(paru, (pari) - 1, (parj), (park))) / 2.0 - MAX(vector(parspeed, (pari) - 2, (parj), (park)), vector(parspeed, (pari) - 1, (parj), (park))) * (vector(paru, (pari) - 1, (parj), (park)) - vector(paru, (pari) - 2, (parj), (park))) / 6.0) + (4.0 * vector(parflux, (pari) + 1, (parj), (park)) / 3.0 - 4.0 * vector(parflux, (pari) - 1, (parj), (park)) / 3.0 + vector(parflux, (pari) - 2, (parj), (park)) / 6.0 - vector(parflux, (pari) + 2, (parj), (park)) / 6.0)) * 0.5 / dx[0])

#define FDOCj(paru, parflux, parspeed, pari, parj, park, dx, simPlat_dt, ilast, jlast) (((MAX(vector(parspeed, (pari), (parj) + 1, (park)), vector(parspeed, (pari), (parj) + 2, (park))) * (vector(paru, (pari), (parj) + 2, (park)) - vector(paru, (pari), (parj) + 1, (park))) / 6.0 - MAX(vector(parspeed, (pari), (parj), (park)), vector(parspeed, (pari), (parj) + 1, (park))) * (vector(paru, (pari), (parj) + 1, (park)) - vector(paru, (pari), (parj), (park))) / 2.0 + MAX(vector(parspeed, (pari), (parj) - 1, (park)), vector(parspeed, (pari), (parj), (park))) * (vector(paru, (pari), (parj), (park)) - vector(paru, (pari), (parj) - 1, (park))) / 2.0 - MAX(vector(parspeed, (pari), (parj) - 2, (park)), vector(parspeed, (pari), (parj) - 1, (park))) * (vector(paru, (pari), (parj) - 1, (park)) - vector(paru, (pari), (parj) - 2, (park))) / 6.0) + (4.0 * vector(parflux, (pari), (parj) + 1, (park)) / 3.0 - 4.0 * vector(parflux, (pari), (parj) - 1, (park)) / 3.0 + vector(parflux, (pari), (parj) - 2, (park)) / 6.0 - vector(parflux, (pari), (parj) + 2, (park)) / 6.0)) * 0.5 / dx[1])

#define FDOCk(paru, parflux, parspeed, pari, parj, park, dx, simPlat_dt, ilast, jlast) (((MAX(vector(parspeed, (pari), (parj), (park) + 1), vector(parspeed, (pari), (parj), (park) + 2)) * (vector(paru, (pari), (parj), (park) + 2) - vector(paru, (pari), (parj), (park) + 1)) / 6.0 - MAX(vector(parspeed, (pari), (parj), (park)), vector(parspeed, (pari), (parj), (park) + 1)) * (vector(paru, (pari), (parj), (park) + 1) - vector(paru, (pari), (parj), (park))) / 2.0 + MAX(vector(parspeed, (pari), (parj), (park) - 1), vector(parspeed, (pari), (parj), (park))) * (vector(paru, (pari), (parj), (park)) - vector(paru, (pari), (parj), (park) - 1)) / 2.0 - MAX(vector(parspeed, (pari), (parj), (park) - 2), vector(parspeed, (pari), (parj), (park) - 1)) * (vector(paru, (pari), (parj), (park) - 1) - vector(paru, (pari), (parj), (park) - 2)) / 6.0) + (4.0 * vector(parflux, (pari), (parj), (park) + 1) / 3.0 - 4.0 * vector(parflux, (pari), (parj), (park) - 1) / 3.0 + vector(parflux, (pari), (parj), (park) - 2) / 6.0 - vector(parflux, (pari), (parj), (park) + 2) / 6.0)) * 0.5 / dx[2])

#define FjEx_AirI(parBz, parc, dx, simPlat_dt, ilast, jlast) (-(parc) * (parBz))

#define FkEx_AirI(parBy, parc, dx, simPlat_dt, ilast, jlast) ((parc) * (parBy))

#define FkEy_AirI(parBx, parc, dx, simPlat_dt, ilast, jlast) (-(parc) * (parBx))

#define FjEz_OtherI(dx, simPlat_dt, ilast, jlast) (0.0)

#define Ficonstraint2_OtherI(parBx, dx, simPlat_dt, ilast, jlast) ((parBx))

#define Fjconstraint2_OtherI(parBy, dx, simPlat_dt, ilast, jlast) ((parBy))

#define Fkconstraint2_OtherI(parBz, dx, simPlat_dt, ilast, jlast) ((parBz))

#define Unit_MCD(i, j, k) ((((int)i) % 10 == 0 && ((int)j) % 10 == 0 && ((int)k) % 10 == 0) ? 10 : (((int)i) % 9 == 0 && ((int)j) % 9 == 0 && ((int)k) % 9 == 0) ? 9 : (((int)i) % 8 == 0 && ((int)j) % 8 == 0 && ((int)k) % 8 == 0) ? 8 : (((int)i) % 7 == 0 && ((int)j) % 7 == 0 && ((int)k) % 7 == 0) ? 7 : (((int)i) % 6 == 0 && ((int)j) % 6 == 0 && ((int)k) % 6 == 0) ? 6 : (((int)i) % 5 == 0 && ((int)j) % 5 == 0 && ((int)k) % 5 == 0) ? 5 : (((int)i) % 4 == 0 && ((int)j) % 4 == 0 && ((int)k) % 4 == 0) ? 4 : (((int)i) % 3 == 0 && ((int)j) % 3 == 0 && ((int)k) % 3 == 0) ? 3 : (((int)i) % 2 == 0 && ((int)j) % 2 == 0 && ((int)k) % 2 == 0) ? 2 : 1)


const gsl_rng_type * T;
gsl_rng *r_var;

//Timers
std::shared_ptr<tbox::Timer> t_step;
std::shared_ptr<tbox::Timer> t_moveParticles;

inline int Problem::GetExpoBase2(double d)
{
	int i = 0;
	((short *)(&i))[0] = (((short *)(&d))[3] & (short)32752); // _123456789ab____ & 0111111111110000
	return (i >> 4) - 1023;
}

bool	Problem::Equals(double d1, double d2)
{
	if (d1 == d2)
		return true;
	int e1 = GetExpoBase2(d1);
	int e2 = GetExpoBase2(d2);
	int e3 = GetExpoBase2(d1 - d2);
	if ((e3 - e2 < -48) && (e3 - e1 < -48))
		return true;
	return false;
}

/*
 * Constructor of the problem.
 */
Problem::Problem(const string& object_name, const tbox::Dimension& dim, std::shared_ptr<tbox::Database>& database, std::shared_ptr<geom::CartesianGridGeometry >& grid_geom, std::shared_ptr<hier::PatchHierarchy >& patch_hierarchy, const double dt, hier::BoxContainer& ba, const bool init_from_files, const vector<string> full_writer_variables, const vector<vector<string> > sliceVariables, const vector<vector<string> > sphereVariables, const vector<vector<string> > integralVariables): 
d_dim(dim), xfer::RefinePatchStrategy(), xfer::CoarsenPatchStrategy(), d_regridding_boxes(ba), d_full_writer_variables(full_writer_variables.begin(), full_writer_variables.end())
{
	//Setup the timers
	t_step = tbox::TimerManager::getManager()->getTimer("Step");
	t_moveParticles = tbox::TimerManager::getManager()->getTimer("Move particles");

	//Get the object name, the grid geometry and the patch hierarchy
  	d_grid_geometry = grid_geom;
	d_patch_hierarchy = patch_hierarchy;
	d_object_name = object_name;
	d_init_from_files = init_from_files;
	initial_dt = dt;

	for (vector<vector<string> >::const_iterator it = sliceVariables.begin(); it != sliceVariables.end(); ++it) {
		vector<string> vars = *it;
		for (vector<string>::const_iterator it2 = vars.begin() ; it2 != vars.end(); ++it2) {
			d_sliceVariables.push_back(vars);
		}
	}
	for (vector<vector<string> >::const_iterator it = sphereVariables.begin(); it != sphereVariables.end(); ++it) {
		vector<string> vars = *it;
		for (vector<string>::const_iterator it2 = vars.begin() ; it2 != vars.end(); ++it2) {
			d_sphereVariables.push_back(vars);
		}
	}
	for (vector<vector<string> >::const_iterator it = integralVariables.begin(); it != integralVariables.end(); ++it) {
		vector<string> vars = *it;
		for (vector<string>::const_iterator it2 = vars.begin() ; it2 != vars.end(); ++it2) {
			d_integralVariables.push_back(vars);
		}
	}


	//Get parameters
	c = database->getDouble("c");
	mu = database->getDouble("mu");
	//Random initialization
	gsl_rng_env_setup();
	//Random for simulation
	const tbox::SAMRAI_MPI& mpi(tbox::SAMRAI_MPI::getSAMRAIWorld());
	int random_seed = database->getDouble("random_seed")*(mpi.getRank() + 1);
	r_var = gsl_rng_alloc(gsl_rng_ranlxs0);
	gsl_rng_set(r_var, random_seed);


    //Subcycling
	d_refinedTimeStepping = false;
	d_tappering = false;
	if (database->isString("subcycling")) {
		if (database->getString("subcycling") == "TAPPERING") {
			d_tappering = true;
			d_refinedTimeStepping = true;
		}
		if (database->getString("subcycling") == "BERGER-OLIGER") {
			d_tappering = false;
			d_refinedTimeStepping = true;
		}
	}


	//Regridding options
	d_regridding = false;
	if (database->isDatabase("regridding")) {
		regridding_db = database->getDatabase("regridding");
		if (regridding_db->isString("regridding_type")) {
			d_regridding_type = regridding_db->getString("regridding_type");
			d_regridding_level_threshold = regridding_db->getInteger("regridding_level_threshold");
			if (d_regridding_type == "GRADIENT") {
				d_regridding_field = regridding_db->getString("regridding_field");
				d_regridding_compressionFactor = regridding_db->getDouble("regridding_compressionFactor");
				d_regridding_mOffset = regridding_db->getDouble("regridding_mOffset");
				d_regridding = true;
			} else {
				if (d_regridding_type == "FUNCTION") {
					d_regridding_field = regridding_db->getString("regridding_function_field");
					d_regridding_threshold = regridding_db->getDouble("regridding_threshold");
					d_regridding = true;
				}
			}
		}
	}

	//Stencil of the discretization method
	int maxratio = 1;
	for (int il = 1; il < d_patch_hierarchy->getMaxNumberOfLevels(); il++) {
		const hier::IntVector ratio = d_patch_hierarchy->getRatioToCoarserLevel(il);
		maxratio = MAX(maxratio, ratio.max());
	}
	//Minimum region thickness
	d_regionMinThickness = 4;
	// number of ghosts stencil * (#substeps) * #parentlevelsteps
	d_ghost_width = 3 * (1 + 2 * d_tappering) * (1 + d_tappering * (maxratio - 1));
	//Extrapolation extra stencil
	d_ghost_width = d_ghost_width + 4 - 1;

	
	//Register Fields and temporal fields into the variable database
	hier::VariableDatabase *vdb = hier::VariableDatabase::getDatabase();
  	std::shared_ptr<hier::VariableContext> d_cont_curr(vdb->getContext("Current"));
	std::shared_ptr< pdat::CellVariable<int> > mask(std::shared_ptr< pdat::CellVariable<int> >(new pdat::CellVariable<int>(d_dim, "samrai_mask",1)));
	d_mask_id = vdb->registerVariableAndContext(mask ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	IntegrateDataWriter::setMaskVariable(d_mask_id);
	std::shared_ptr< pdat::NodeVariable<double> > interior(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "interior",1)));
	d_interior_id = vdb->registerVariableAndContext(interior ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_interior_id);
	std::shared_ptr< pdat::NodeVariable<int> > nonSync(std::shared_ptr< pdat::NodeVariable<int> >(new pdat::NodeVariable<int>(d_dim, "nonSync",1)));
	d_nonSync_id = vdb->registerVariableAndContext(nonSync ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_nonSync_id);
	std::shared_ptr< pdat::NodeVariable<double> > interior_i(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "interior_i",1)));
	d_interior_i_id = vdb->registerVariableAndContext(interior_i ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_interior_i_id);
	std::shared_ptr< pdat::NodeVariable<double> > interior_j(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "interior_j",1)));
	d_interior_j_id = vdb->registerVariableAndContext(interior_j ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_interior_j_id);
	std::shared_ptr< pdat::NodeVariable<double> > interior_k(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "interior_k",1)));
	d_interior_k_id = vdb->registerVariableAndContext(interior_k ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_interior_k_id);
	std::shared_ptr< pdat::NodeVariable<double> > FOV_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FOV_1",1)));
	d_FOV_1_id = vdb->registerVariableAndContext(FOV_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FOV_1_id);
	std::shared_ptr< pdat::NodeVariable<double> > FOV_2(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FOV_2",1)));
	d_FOV_2_id = vdb->registerVariableAndContext(FOV_2 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FOV_2_id);
	std::shared_ptr< pdat::NodeVariable<double> > FOV_4(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FOV_4",1)));
	d_FOV_4_id = vdb->registerVariableAndContext(FOV_4 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FOV_4_id);
	std::shared_ptr< pdat::NodeVariable<double> > FOV_5(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FOV_5",1)));
	d_FOV_5_id = vdb->registerVariableAndContext(FOV_5 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FOV_5_id);
	std::shared_ptr< pdat::NodeVariable<double> > FOV_7(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FOV_7",1)));
	d_FOV_7_id = vdb->registerVariableAndContext(FOV_7 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FOV_7_id);
	std::shared_ptr< pdat::NodeVariable<double> > FOV_8(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FOV_8",1)));
	d_FOV_8_id = vdb->registerVariableAndContext(FOV_8 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FOV_8_id);
	std::shared_ptr< pdat::NodeVariable<double> > FOV_xLower(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FOV_xLower",1)));
	d_FOV_xLower_id = vdb->registerVariableAndContext(FOV_xLower ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FOV_xLower_id);
	std::shared_ptr< pdat::NodeVariable<double> > FOV_xUpper(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FOV_xUpper",1)));
	d_FOV_xUpper_id = vdb->registerVariableAndContext(FOV_xUpper ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FOV_xUpper_id);
	std::shared_ptr< pdat::NodeVariable<double> > FOV_yLower(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FOV_yLower",1)));
	d_FOV_yLower_id = vdb->registerVariableAndContext(FOV_yLower ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FOV_yLower_id);
	std::shared_ptr< pdat::NodeVariable<double> > FOV_yUpper(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FOV_yUpper",1)));
	d_FOV_yUpper_id = vdb->registerVariableAndContext(FOV_yUpper ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FOV_yUpper_id);
	std::shared_ptr< pdat::NodeVariable<double> > FOV_zLower(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FOV_zLower",1)));
	d_FOV_zLower_id = vdb->registerVariableAndContext(FOV_zLower ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FOV_zLower_id);
	std::shared_ptr< pdat::NodeVariable<double> > FOV_zUpper(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FOV_zUpper",1)));
	d_FOV_zUpper_id = vdb->registerVariableAndContext(FOV_zUpper ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FOV_zUpper_id);
	std::shared_ptr< pdat::NodeVariable<double> > Ex(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "Ex",1)));
	d_Ex_id = vdb->registerVariableAndContext(Ex ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_Ex_id);
	std::shared_ptr< pdat::NodeVariable<double> > Ex_p(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "Ex_p",1)));
	d_Ex_p_id = vdb->registerVariableAndContext(Ex_p ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_Ex_p_id);
	std::shared_ptr< pdat::NodeVariable<double> > Ey(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "Ey",1)));
	d_Ey_id = vdb->registerVariableAndContext(Ey ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_Ey_id);
	std::shared_ptr< pdat::NodeVariable<double> > Ey_p(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "Ey_p",1)));
	d_Ey_p_id = vdb->registerVariableAndContext(Ey_p ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_Ey_p_id);
	std::shared_ptr< pdat::NodeVariable<double> > Ez(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "Ez",1)));
	d_Ez_id = vdb->registerVariableAndContext(Ez ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_Ez_id);
	std::shared_ptr< pdat::NodeVariable<double> > Ez_p(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "Ez_p",1)));
	d_Ez_p_id = vdb->registerVariableAndContext(Ez_p ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_Ez_p_id);
	std::shared_ptr< pdat::NodeVariable<double> > Bx(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "Bx",1)));
	d_Bx_id = vdb->registerVariableAndContext(Bx ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_Bx_id);
	std::shared_ptr< pdat::NodeVariable<double> > Bx_p(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "Bx_p",1)));
	d_Bx_p_id = vdb->registerVariableAndContext(Bx_p ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_Bx_p_id);
	std::shared_ptr< pdat::NodeVariable<double> > By(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "By",1)));
	d_By_id = vdb->registerVariableAndContext(By ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_By_id);
	std::shared_ptr< pdat::NodeVariable<double> > By_p(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "By_p",1)));
	d_By_p_id = vdb->registerVariableAndContext(By_p ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_By_p_id);
	std::shared_ptr< pdat::NodeVariable<double> > Bz(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "Bz",1)));
	d_Bz_id = vdb->registerVariableAndContext(Bz ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_Bz_id);
	std::shared_ptr< pdat::NodeVariable<double> > Bz_p(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "Bz_p",1)));
	d_Bz_p_id = vdb->registerVariableAndContext(Bz_p ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_Bz_p_id);
	std::shared_ptr< pdat::NodeVariable<double> > q(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "q",1)));
	d_q_id = vdb->registerVariableAndContext(q ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_q_id);
	std::shared_ptr< pdat::NodeVariable<double> > Jx(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "Jx",1)));
	d_Jx_id = vdb->registerVariableAndContext(Jx ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_Jx_id);
	std::shared_ptr< pdat::NodeVariable<double> > Jx_p(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "Jx_p",1)));
	d_Jx_p_id = vdb->registerVariableAndContext(Jx_p ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_Jx_p_id);
	std::shared_ptr< pdat::NodeVariable<double> > Jy(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "Jy",1)));
	d_Jy_id = vdb->registerVariableAndContext(Jy ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_Jy_id);
	std::shared_ptr< pdat::NodeVariable<double> > Jy_p(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "Jy_p",1)));
	d_Jy_p_id = vdb->registerVariableAndContext(Jy_p ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_Jy_p_id);
	std::shared_ptr< pdat::NodeVariable<double> > Jz(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "Jz",1)));
	d_Jz_id = vdb->registerVariableAndContext(Jz ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_Jz_id);
	std::shared_ptr< pdat::NodeVariable<double> > Jz_p(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "Jz_p",1)));
	d_Jz_p_id = vdb->registerVariableAndContext(Jz_p ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_Jz_p_id);
	std::shared_ptr< pdat::NodeVariable<double> > constraint1_4(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "constraint1_4",1)));
	d_constraint1_4_id = vdb->registerVariableAndContext(constraint1_4 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_constraint1_4_id);
	std::shared_ptr< pdat::NodeVariable<double> > constraint2_4(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "constraint2_4",1)));
	d_constraint2_4_id = vdb->registerVariableAndContext(constraint2_4 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_constraint2_4_id);
	std::shared_ptr< pdat::NodeVariable<double> > constraint1_5(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "constraint1_5",1)));
	d_constraint1_5_id = vdb->registerVariableAndContext(constraint1_5 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_constraint1_5_id);
	std::shared_ptr< pdat::NodeVariable<double> > constraint2_5(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "constraint2_5",1)));
	d_constraint2_5_id = vdb->registerVariableAndContext(constraint2_5 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_constraint2_5_id);
	std::shared_ptr< pdat::NodeVariable<double> > FluxSBjEx_7(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FluxSBjEx_7",1)));
	d_FluxSBjEx_7_id = vdb->registerVariableAndContext(FluxSBjEx_7 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FluxSBjEx_7_id);
	std::shared_ptr< pdat::NodeVariable<double> > FluxSBkEx_7(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FluxSBkEx_7",1)));
	d_FluxSBkEx_7_id = vdb->registerVariableAndContext(FluxSBkEx_7 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FluxSBkEx_7_id);
	std::shared_ptr< pdat::NodeVariable<double> > FluxSBiEy_7(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FluxSBiEy_7",1)));
	d_FluxSBiEy_7_id = vdb->registerVariableAndContext(FluxSBiEy_7 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FluxSBiEy_7_id);
	std::shared_ptr< pdat::NodeVariable<double> > FluxSBkEy_7(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FluxSBkEy_7",1)));
	d_FluxSBkEy_7_id = vdb->registerVariableAndContext(FluxSBkEy_7 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FluxSBkEy_7_id);
	std::shared_ptr< pdat::NodeVariable<double> > FluxSBiEz_7(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FluxSBiEz_7",1)));
	d_FluxSBiEz_7_id = vdb->registerVariableAndContext(FluxSBiEz_7 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FluxSBiEz_7_id);
	std::shared_ptr< pdat::NodeVariable<double> > FluxSBjEz_7(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FluxSBjEz_7",1)));
	d_FluxSBjEz_7_id = vdb->registerVariableAndContext(FluxSBjEz_7 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FluxSBjEz_7_id);
	std::shared_ptr< pdat::NodeVariable<double> > FluxSBjBx_7(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FluxSBjBx_7",1)));
	d_FluxSBjBx_7_id = vdb->registerVariableAndContext(FluxSBjBx_7 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FluxSBjBx_7_id);
	std::shared_ptr< pdat::NodeVariable<double> > FluxSBkBx_7(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FluxSBkBx_7",1)));
	d_FluxSBkBx_7_id = vdb->registerVariableAndContext(FluxSBkBx_7 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FluxSBkBx_7_id);
	std::shared_ptr< pdat::NodeVariable<double> > FluxSBiBy_7(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FluxSBiBy_7",1)));
	d_FluxSBiBy_7_id = vdb->registerVariableAndContext(FluxSBiBy_7 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FluxSBiBy_7_id);
	std::shared_ptr< pdat::NodeVariable<double> > FluxSBkBy_7(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FluxSBkBy_7",1)));
	d_FluxSBkBy_7_id = vdb->registerVariableAndContext(FluxSBkBy_7 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FluxSBkBy_7_id);
	std::shared_ptr< pdat::NodeVariable<double> > FluxSBiBz_7(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FluxSBiBz_7",1)));
	d_FluxSBiBz_7_id = vdb->registerVariableAndContext(FluxSBiBz_7 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FluxSBiBz_7_id);
	std::shared_ptr< pdat::NodeVariable<double> > FluxSBjBz_7(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FluxSBjBz_7",1)));
	d_FluxSBjBz_7_id = vdb->registerVariableAndContext(FluxSBjBz_7 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FluxSBjBz_7_id);
	std::shared_ptr< pdat::NodeVariable<double> > SpeedSBjEx_7(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "SpeedSBjEx_7",1)));
	d_SpeedSBjEx_7_id = vdb->registerVariableAndContext(SpeedSBjEx_7 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_SpeedSBjEx_7_id);
	std::shared_ptr< pdat::NodeVariable<double> > SpeedSBkEx_7(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "SpeedSBkEx_7",1)));
	d_SpeedSBkEx_7_id = vdb->registerVariableAndContext(SpeedSBkEx_7 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_SpeedSBkEx_7_id);
	std::shared_ptr< pdat::NodeVariable<double> > SpeedSBiEy_7(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "SpeedSBiEy_7",1)));
	d_SpeedSBiEy_7_id = vdb->registerVariableAndContext(SpeedSBiEy_7 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_SpeedSBiEy_7_id);
	std::shared_ptr< pdat::NodeVariable<double> > SpeedSBkEy_7(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "SpeedSBkEy_7",1)));
	d_SpeedSBkEy_7_id = vdb->registerVariableAndContext(SpeedSBkEy_7 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_SpeedSBkEy_7_id);
	std::shared_ptr< pdat::NodeVariable<double> > SpeedSBiEz_7(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "SpeedSBiEz_7",1)));
	d_SpeedSBiEz_7_id = vdb->registerVariableAndContext(SpeedSBiEz_7 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_SpeedSBiEz_7_id);
	std::shared_ptr< pdat::NodeVariable<double> > SpeedSBjEz_7(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "SpeedSBjEz_7",1)));
	d_SpeedSBjEz_7_id = vdb->registerVariableAndContext(SpeedSBjEz_7 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_SpeedSBjEz_7_id);
	std::shared_ptr< pdat::NodeVariable<double> > SpeedSBjBx_7(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "SpeedSBjBx_7",1)));
	d_SpeedSBjBx_7_id = vdb->registerVariableAndContext(SpeedSBjBx_7 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_SpeedSBjBx_7_id);
	std::shared_ptr< pdat::NodeVariable<double> > SpeedSBkBx_7(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "SpeedSBkBx_7",1)));
	d_SpeedSBkBx_7_id = vdb->registerVariableAndContext(SpeedSBkBx_7 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_SpeedSBkBx_7_id);
	std::shared_ptr< pdat::NodeVariable<double> > SpeedSBiBy_7(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "SpeedSBiBy_7",1)));
	d_SpeedSBiBy_7_id = vdb->registerVariableAndContext(SpeedSBiBy_7 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_SpeedSBiBy_7_id);
	std::shared_ptr< pdat::NodeVariable<double> > SpeedSBkBy_7(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "SpeedSBkBy_7",1)));
	d_SpeedSBkBy_7_id = vdb->registerVariableAndContext(SpeedSBkBy_7 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_SpeedSBkBy_7_id);
	std::shared_ptr< pdat::NodeVariable<double> > SpeedSBiBz_7(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "SpeedSBiBz_7",1)));
	d_SpeedSBiBz_7_id = vdb->registerVariableAndContext(SpeedSBiBz_7 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_SpeedSBiBz_7_id);
	std::shared_ptr< pdat::NodeVariable<double> > SpeedSBjBz_7(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "SpeedSBjBz_7",1)));
	d_SpeedSBjBz_7_id = vdb->registerVariableAndContext(SpeedSBjBz_7 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_SpeedSBjBz_7_id);
	std::shared_ptr< pdat::NodeVariable<double> > K1SBEx_7(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "K1SBEx_7",1)));
	d_K1SBEx_7_id = vdb->registerVariableAndContext(K1SBEx_7 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_K1SBEx_7_id);
	std::shared_ptr< pdat::NodeVariable<double> > K1SBEy_7(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "K1SBEy_7",1)));
	d_K1SBEy_7_id = vdb->registerVariableAndContext(K1SBEy_7 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_K1SBEy_7_id);
	std::shared_ptr< pdat::NodeVariable<double> > K1SBEz_7(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "K1SBEz_7",1)));
	d_K1SBEz_7_id = vdb->registerVariableAndContext(K1SBEz_7 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_K1SBEz_7_id);
	std::shared_ptr< pdat::NodeVariable<double> > K1SBBx_7(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "K1SBBx_7",1)));
	d_K1SBBx_7_id = vdb->registerVariableAndContext(K1SBBx_7 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_K1SBBx_7_id);
	std::shared_ptr< pdat::NodeVariable<double> > K1SBBy_7(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "K1SBBy_7",1)));
	d_K1SBBy_7_id = vdb->registerVariableAndContext(K1SBBy_7 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_K1SBBy_7_id);
	std::shared_ptr< pdat::NodeVariable<double> > K1SBBz_7(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "K1SBBz_7",1)));
	d_K1SBBz_7_id = vdb->registerVariableAndContext(K1SBBz_7 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_K1SBBz_7_id);
	std::shared_ptr< pdat::NodeVariable<double> > ExSPprime_7(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "ExSPprime_7",1)));
	d_ExSPprime_7_id = vdb->registerVariableAndContext(ExSPprime_7 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_ExSPprime_7_id);
	std::shared_ptr< pdat::NodeVariable<double> > EySPprime_7(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "EySPprime_7",1)));
	d_EySPprime_7_id = vdb->registerVariableAndContext(EySPprime_7 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_EySPprime_7_id);
	std::shared_ptr< pdat::NodeVariable<double> > EzSPprime_7(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "EzSPprime_7",1)));
	d_EzSPprime_7_id = vdb->registerVariableAndContext(EzSPprime_7 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_EzSPprime_7_id);
	std::shared_ptr< pdat::NodeVariable<double> > BxSPprime_7(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "BxSPprime_7",1)));
	d_BxSPprime_7_id = vdb->registerVariableAndContext(BxSPprime_7 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_BxSPprime_7_id);
	std::shared_ptr< pdat::NodeVariable<double> > BySPprime_7(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "BySPprime_7",1)));
	d_BySPprime_7_id = vdb->registerVariableAndContext(BySPprime_7 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_BySPprime_7_id);
	std::shared_ptr< pdat::NodeVariable<double> > BzSPprime_7(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "BzSPprime_7",1)));
	d_BzSPprime_7_id = vdb->registerVariableAndContext(BzSPprime_7 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_BzSPprime_7_id);
	std::shared_ptr< pdat::NodeVariable<double> > qSPprime_7(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "qSPprime_7",1)));
	d_qSPprime_7_id = vdb->registerVariableAndContext(qSPprime_7 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_qSPprime_7_id);
	std::shared_ptr< pdat::NodeVariable<double> > JxSPprime_7(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "JxSPprime_7",1)));
	d_JxSPprime_7_id = vdb->registerVariableAndContext(JxSPprime_7 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_JxSPprime_7_id);
	std::shared_ptr< pdat::NodeVariable<double> > JySPprime_7(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "JySPprime_7",1)));
	d_JySPprime_7_id = vdb->registerVariableAndContext(JySPprime_7 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_JySPprime_7_id);
	std::shared_ptr< pdat::NodeVariable<double> > JzSPprime_7(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "JzSPprime_7",1)));
	d_JzSPprime_7_id = vdb->registerVariableAndContext(JzSPprime_7 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_JzSPprime_7_id);
	std::shared_ptr< pdat::NodeVariable<double> > K2SBEx_7(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "K2SBEx_7",1)));
	d_K2SBEx_7_id = vdb->registerVariableAndContext(K2SBEx_7 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_K2SBEx_7_id);
	std::shared_ptr< pdat::NodeVariable<double> > K2SBEy_7(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "K2SBEy_7",1)));
	d_K2SBEy_7_id = vdb->registerVariableAndContext(K2SBEy_7 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_K2SBEy_7_id);
	std::shared_ptr< pdat::NodeVariable<double> > K2SBEz_7(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "K2SBEz_7",1)));
	d_K2SBEz_7_id = vdb->registerVariableAndContext(K2SBEz_7 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_K2SBEz_7_id);
	std::shared_ptr< pdat::NodeVariable<double> > K2SBBx_7(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "K2SBBx_7",1)));
	d_K2SBBx_7_id = vdb->registerVariableAndContext(K2SBBx_7 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_K2SBBx_7_id);
	std::shared_ptr< pdat::NodeVariable<double> > K2SBBy_7(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "K2SBBy_7",1)));
	d_K2SBBy_7_id = vdb->registerVariableAndContext(K2SBBy_7 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_K2SBBy_7_id);
	std::shared_ptr< pdat::NodeVariable<double> > K2SBBz_7(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "K2SBBz_7",1)));
	d_K2SBBz_7_id = vdb->registerVariableAndContext(K2SBBz_7 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_K2SBBz_7_id);
	std::shared_ptr< pdat::NodeVariable<double> > ExSPprimeprime_7(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "ExSPprimeprime_7",1)));
	d_ExSPprimeprime_7_id = vdb->registerVariableAndContext(ExSPprimeprime_7 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_ExSPprimeprime_7_id);
	std::shared_ptr< pdat::NodeVariable<double> > EySPprimeprime_7(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "EySPprimeprime_7",1)));
	d_EySPprimeprime_7_id = vdb->registerVariableAndContext(EySPprimeprime_7 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_EySPprimeprime_7_id);
	std::shared_ptr< pdat::NodeVariable<double> > EzSPprimeprime_7(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "EzSPprimeprime_7",1)));
	d_EzSPprimeprime_7_id = vdb->registerVariableAndContext(EzSPprimeprime_7 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_EzSPprimeprime_7_id);
	std::shared_ptr< pdat::NodeVariable<double> > BxSPprimeprime_7(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "BxSPprimeprime_7",1)));
	d_BxSPprimeprime_7_id = vdb->registerVariableAndContext(BxSPprimeprime_7 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_BxSPprimeprime_7_id);
	std::shared_ptr< pdat::NodeVariable<double> > BySPprimeprime_7(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "BySPprimeprime_7",1)));
	d_BySPprimeprime_7_id = vdb->registerVariableAndContext(BySPprimeprime_7 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_BySPprimeprime_7_id);
	std::shared_ptr< pdat::NodeVariable<double> > BzSPprimeprime_7(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "BzSPprimeprime_7",1)));
	d_BzSPprimeprime_7_id = vdb->registerVariableAndContext(BzSPprimeprime_7 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_BzSPprimeprime_7_id);
	std::shared_ptr< pdat::NodeVariable<double> > K3SBEx_7(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "K3SBEx_7",1)));
	d_K3SBEx_7_id = vdb->registerVariableAndContext(K3SBEx_7 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_K3SBEx_7_id);
	std::shared_ptr< pdat::NodeVariable<double> > K3SBEy_7(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "K3SBEy_7",1)));
	d_K3SBEy_7_id = vdb->registerVariableAndContext(K3SBEy_7 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_K3SBEy_7_id);
	std::shared_ptr< pdat::NodeVariable<double> > K3SBBy_7(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "K3SBBy_7",1)));
	d_K3SBBy_7_id = vdb->registerVariableAndContext(K3SBBy_7 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_K3SBBy_7_id);
	std::shared_ptr< pdat::NodeVariable<double> > ExSPprime(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "ExSPprime",1)));
	d_ExSPprime_id = vdb->registerVariableAndContext(ExSPprime ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_ExSPprime_id);
	std::shared_ptr< pdat::NodeVariable<double> > EySPprime(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "EySPprime",1)));
	d_EySPprime_id = vdb->registerVariableAndContext(EySPprime ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_EySPprime_id);
	std::shared_ptr< pdat::NodeVariable<double> > EzSPprime(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "EzSPprime",1)));
	d_EzSPprime_id = vdb->registerVariableAndContext(EzSPprime ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_EzSPprime_id);
	std::shared_ptr< pdat::NodeVariable<double> > BxSPprime(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "BxSPprime",1)));
	d_BxSPprime_id = vdb->registerVariableAndContext(BxSPprime ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_BxSPprime_id);
	std::shared_ptr< pdat::NodeVariable<double> > BySPprime(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "BySPprime",1)));
	d_BySPprime_id = vdb->registerVariableAndContext(BySPprime ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_BySPprime_id);
	std::shared_ptr< pdat::NodeVariable<double> > BzSPprime(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "BzSPprime",1)));
	d_BzSPprime_id = vdb->registerVariableAndContext(BzSPprime ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_BzSPprime_id);
	std::shared_ptr< pdat::NodeVariable<double> > FluxSBjEx_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FluxSBjEx_1",1)));
	d_FluxSBjEx_1_id = vdb->registerVariableAndContext(FluxSBjEx_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FluxSBjEx_1_id);
	std::shared_ptr< pdat::NodeVariable<double> > FluxSBkEx_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FluxSBkEx_1",1)));
	d_FluxSBkEx_1_id = vdb->registerVariableAndContext(FluxSBkEx_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FluxSBkEx_1_id);
	std::shared_ptr< pdat::NodeVariable<double> > FluxSBiEy_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FluxSBiEy_1",1)));
	d_FluxSBiEy_1_id = vdb->registerVariableAndContext(FluxSBiEy_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FluxSBiEy_1_id);
	std::shared_ptr< pdat::NodeVariable<double> > FluxSBkEy_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FluxSBkEy_1",1)));
	d_FluxSBkEy_1_id = vdb->registerVariableAndContext(FluxSBkEy_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FluxSBkEy_1_id);
	std::shared_ptr< pdat::NodeVariable<double> > FluxSBiEz_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FluxSBiEz_1",1)));
	d_FluxSBiEz_1_id = vdb->registerVariableAndContext(FluxSBiEz_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FluxSBiEz_1_id);
	std::shared_ptr< pdat::NodeVariable<double> > FluxSBjEz_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FluxSBjEz_1",1)));
	d_FluxSBjEz_1_id = vdb->registerVariableAndContext(FluxSBjEz_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FluxSBjEz_1_id);
	std::shared_ptr< pdat::NodeVariable<double> > FluxSBjBx_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FluxSBjBx_1",1)));
	d_FluxSBjBx_1_id = vdb->registerVariableAndContext(FluxSBjBx_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FluxSBjBx_1_id);
	std::shared_ptr< pdat::NodeVariable<double> > FluxSBkBx_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FluxSBkBx_1",1)));
	d_FluxSBkBx_1_id = vdb->registerVariableAndContext(FluxSBkBx_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FluxSBkBx_1_id);
	std::shared_ptr< pdat::NodeVariable<double> > FluxSBiBy_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FluxSBiBy_1",1)));
	d_FluxSBiBy_1_id = vdb->registerVariableAndContext(FluxSBiBy_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FluxSBiBy_1_id);
	std::shared_ptr< pdat::NodeVariable<double> > FluxSBkBy_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FluxSBkBy_1",1)));
	d_FluxSBkBy_1_id = vdb->registerVariableAndContext(FluxSBkBy_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FluxSBkBy_1_id);
	std::shared_ptr< pdat::NodeVariable<double> > FluxSBiBz_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FluxSBiBz_1",1)));
	d_FluxSBiBz_1_id = vdb->registerVariableAndContext(FluxSBiBz_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FluxSBiBz_1_id);
	std::shared_ptr< pdat::NodeVariable<double> > FluxSBjBz_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FluxSBjBz_1",1)));
	d_FluxSBjBz_1_id = vdb->registerVariableAndContext(FluxSBjBz_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FluxSBjBz_1_id);
	std::shared_ptr< pdat::NodeVariable<double> > SpeedSBjEx_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "SpeedSBjEx_1",1)));
	d_SpeedSBjEx_1_id = vdb->registerVariableAndContext(SpeedSBjEx_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_SpeedSBjEx_1_id);
	std::shared_ptr< pdat::NodeVariable<double> > SpeedSBkEx_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "SpeedSBkEx_1",1)));
	d_SpeedSBkEx_1_id = vdb->registerVariableAndContext(SpeedSBkEx_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_SpeedSBkEx_1_id);
	std::shared_ptr< pdat::NodeVariable<double> > SpeedSBiEy_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "SpeedSBiEy_1",1)));
	d_SpeedSBiEy_1_id = vdb->registerVariableAndContext(SpeedSBiEy_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_SpeedSBiEy_1_id);
	std::shared_ptr< pdat::NodeVariable<double> > SpeedSBkEy_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "SpeedSBkEy_1",1)));
	d_SpeedSBkEy_1_id = vdb->registerVariableAndContext(SpeedSBkEy_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_SpeedSBkEy_1_id);
	std::shared_ptr< pdat::NodeVariable<double> > SpeedSBiEz_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "SpeedSBiEz_1",1)));
	d_SpeedSBiEz_1_id = vdb->registerVariableAndContext(SpeedSBiEz_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_SpeedSBiEz_1_id);
	std::shared_ptr< pdat::NodeVariable<double> > SpeedSBjEz_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "SpeedSBjEz_1",1)));
	d_SpeedSBjEz_1_id = vdb->registerVariableAndContext(SpeedSBjEz_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_SpeedSBjEz_1_id);
	std::shared_ptr< pdat::NodeVariable<double> > SpeedSBjBx_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "SpeedSBjBx_1",1)));
	d_SpeedSBjBx_1_id = vdb->registerVariableAndContext(SpeedSBjBx_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_SpeedSBjBx_1_id);
	std::shared_ptr< pdat::NodeVariable<double> > SpeedSBkBx_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "SpeedSBkBx_1",1)));
	d_SpeedSBkBx_1_id = vdb->registerVariableAndContext(SpeedSBkBx_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_SpeedSBkBx_1_id);
	std::shared_ptr< pdat::NodeVariable<double> > SpeedSBiBy_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "SpeedSBiBy_1",1)));
	d_SpeedSBiBy_1_id = vdb->registerVariableAndContext(SpeedSBiBy_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_SpeedSBiBy_1_id);
	std::shared_ptr< pdat::NodeVariable<double> > SpeedSBkBy_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "SpeedSBkBy_1",1)));
	d_SpeedSBkBy_1_id = vdb->registerVariableAndContext(SpeedSBkBy_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_SpeedSBkBy_1_id);
	std::shared_ptr< pdat::NodeVariable<double> > SpeedSBiBz_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "SpeedSBiBz_1",1)));
	d_SpeedSBiBz_1_id = vdb->registerVariableAndContext(SpeedSBiBz_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_SpeedSBiBz_1_id);
	std::shared_ptr< pdat::NodeVariable<double> > SpeedSBjBz_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "SpeedSBjBz_1",1)));
	d_SpeedSBjBz_1_id = vdb->registerVariableAndContext(SpeedSBjBz_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_SpeedSBjBz_1_id);
	std::shared_ptr< pdat::NodeVariable<double> > K1SBEx_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "K1SBEx_1",1)));
	d_K1SBEx_1_id = vdb->registerVariableAndContext(K1SBEx_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_K1SBEx_1_id);
	std::shared_ptr< pdat::NodeVariable<double> > K1SBEy_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "K1SBEy_1",1)));
	d_K1SBEy_1_id = vdb->registerVariableAndContext(K1SBEy_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_K1SBEy_1_id);
	std::shared_ptr< pdat::NodeVariable<double> > K1SBEz_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "K1SBEz_1",1)));
	d_K1SBEz_1_id = vdb->registerVariableAndContext(K1SBEz_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_K1SBEz_1_id);
	std::shared_ptr< pdat::NodeVariable<double> > K1SBBx_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "K1SBBx_1",1)));
	d_K1SBBx_1_id = vdb->registerVariableAndContext(K1SBBx_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_K1SBBx_1_id);
	std::shared_ptr< pdat::NodeVariable<double> > K1SBBy_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "K1SBBy_1",1)));
	d_K1SBBy_1_id = vdb->registerVariableAndContext(K1SBBy_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_K1SBBy_1_id);
	std::shared_ptr< pdat::NodeVariable<double> > K1SBBz_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "K1SBBz_1",1)));
	d_K1SBBz_1_id = vdb->registerVariableAndContext(K1SBBz_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_K1SBBz_1_id);
	std::shared_ptr< pdat::NodeVariable<double> > ExSPprime_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "ExSPprime_1",1)));
	d_ExSPprime_1_id = vdb->registerVariableAndContext(ExSPprime_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_ExSPprime_1_id);
	std::shared_ptr< pdat::NodeVariable<double> > EySPprime_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "EySPprime_1",1)));
	d_EySPprime_1_id = vdb->registerVariableAndContext(EySPprime_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_EySPprime_1_id);
	std::shared_ptr< pdat::NodeVariable<double> > EzSPprime_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "EzSPprime_1",1)));
	d_EzSPprime_1_id = vdb->registerVariableAndContext(EzSPprime_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_EzSPprime_1_id);
	std::shared_ptr< pdat::NodeVariable<double> > BxSPprime_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "BxSPprime_1",1)));
	d_BxSPprime_1_id = vdb->registerVariableAndContext(BxSPprime_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_BxSPprime_1_id);
	std::shared_ptr< pdat::NodeVariable<double> > BySPprime_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "BySPprime_1",1)));
	d_BySPprime_1_id = vdb->registerVariableAndContext(BySPprime_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_BySPprime_1_id);
	std::shared_ptr< pdat::NodeVariable<double> > BzSPprime_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "BzSPprime_1",1)));
	d_BzSPprime_1_id = vdb->registerVariableAndContext(BzSPprime_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_BzSPprime_1_id);
	std::shared_ptr< pdat::NodeVariable<double> > ExSPprime_2(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "ExSPprime_2",1)));
	d_ExSPprime_2_id = vdb->registerVariableAndContext(ExSPprime_2 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_ExSPprime_2_id);
	std::shared_ptr< pdat::NodeVariable<double> > EySPprime_2(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "EySPprime_2",1)));
	d_EySPprime_2_id = vdb->registerVariableAndContext(EySPprime_2 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_EySPprime_2_id);
	std::shared_ptr< pdat::NodeVariable<double> > EzSPprime_2(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "EzSPprime_2",1)));
	d_EzSPprime_2_id = vdb->registerVariableAndContext(EzSPprime_2 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_EzSPprime_2_id);
	std::shared_ptr< pdat::NodeVariable<double> > BxSPprime_2(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "BxSPprime_2",1)));
	d_BxSPprime_2_id = vdb->registerVariableAndContext(BxSPprime_2 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_BxSPprime_2_id);
	std::shared_ptr< pdat::NodeVariable<double> > BySPprime_2(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "BySPprime_2",1)));
	d_BySPprime_2_id = vdb->registerVariableAndContext(BySPprime_2 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_BySPprime_2_id);
	std::shared_ptr< pdat::NodeVariable<double> > BzSPprime_2(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "BzSPprime_2",1)));
	d_BzSPprime_2_id = vdb->registerVariableAndContext(BzSPprime_2 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_BzSPprime_2_id);
	std::shared_ptr< pdat::NodeVariable<double> > ExSPprimeprime_2(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "ExSPprimeprime_2",1)));
	d_ExSPprimeprime_2_id = vdb->registerVariableAndContext(ExSPprimeprime_2 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_ExSPprimeprime_2_id);
	std::shared_ptr< pdat::NodeVariable<double> > EySPprimeprime_2(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "EySPprimeprime_2",1)));
	d_EySPprimeprime_2_id = vdb->registerVariableAndContext(EySPprimeprime_2 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_EySPprimeprime_2_id);
	std::shared_ptr< pdat::NodeVariable<double> > EzSPprimeprime_2(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "EzSPprimeprime_2",1)));
	d_EzSPprimeprime_2_id = vdb->registerVariableAndContext(EzSPprimeprime_2 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_EzSPprimeprime_2_id);
	std::shared_ptr< pdat::NodeVariable<double> > BxSPprimeprime_2(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "BxSPprimeprime_2",1)));
	d_BxSPprimeprime_2_id = vdb->registerVariableAndContext(BxSPprimeprime_2 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_BxSPprimeprime_2_id);
	std::shared_ptr< pdat::NodeVariable<double> > BySPprimeprime_2(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "BySPprimeprime_2",1)));
	d_BySPprimeprime_2_id = vdb->registerVariableAndContext(BySPprimeprime_2 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_BySPprimeprime_2_id);
	std::shared_ptr< pdat::NodeVariable<double> > BzSPprimeprime_2(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "BzSPprimeprime_2",1)));
	d_BzSPprimeprime_2_id = vdb->registerVariableAndContext(BzSPprimeprime_2 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_BzSPprimeprime_2_id);
	std::shared_ptr< pdat::NodeVariable<double> > FluxSBjEx_4(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FluxSBjEx_4",1)));
	d_FluxSBjEx_4_id = vdb->registerVariableAndContext(FluxSBjEx_4 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FluxSBjEx_4_id);
	std::shared_ptr< pdat::NodeVariable<double> > FluxSBkEx_4(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FluxSBkEx_4",1)));
	d_FluxSBkEx_4_id = vdb->registerVariableAndContext(FluxSBkEx_4 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FluxSBkEx_4_id);
	std::shared_ptr< pdat::NodeVariable<double> > FluxSBiEy_4(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FluxSBiEy_4",1)));
	d_FluxSBiEy_4_id = vdb->registerVariableAndContext(FluxSBiEy_4 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FluxSBiEy_4_id);
	std::shared_ptr< pdat::NodeVariable<double> > FluxSBkEy_4(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FluxSBkEy_4",1)));
	d_FluxSBkEy_4_id = vdb->registerVariableAndContext(FluxSBkEy_4 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FluxSBkEy_4_id);
	std::shared_ptr< pdat::NodeVariable<double> > FluxSBiEz_4(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FluxSBiEz_4",1)));
	d_FluxSBiEz_4_id = vdb->registerVariableAndContext(FluxSBiEz_4 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FluxSBiEz_4_id);
	std::shared_ptr< pdat::NodeVariable<double> > FluxSBjEz_4(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FluxSBjEz_4",1)));
	d_FluxSBjEz_4_id = vdb->registerVariableAndContext(FluxSBjEz_4 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FluxSBjEz_4_id);
	std::shared_ptr< pdat::NodeVariable<double> > FluxSBjBx_4(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FluxSBjBx_4",1)));
	d_FluxSBjBx_4_id = vdb->registerVariableAndContext(FluxSBjBx_4 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FluxSBjBx_4_id);
	std::shared_ptr< pdat::NodeVariable<double> > FluxSBkBx_4(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FluxSBkBx_4",1)));
	d_FluxSBkBx_4_id = vdb->registerVariableAndContext(FluxSBkBx_4 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FluxSBkBx_4_id);
	std::shared_ptr< pdat::NodeVariable<double> > FluxSBiBy_4(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FluxSBiBy_4",1)));
	d_FluxSBiBy_4_id = vdb->registerVariableAndContext(FluxSBiBy_4 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FluxSBiBy_4_id);
	std::shared_ptr< pdat::NodeVariable<double> > FluxSBkBy_4(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FluxSBkBy_4",1)));
	d_FluxSBkBy_4_id = vdb->registerVariableAndContext(FluxSBkBy_4 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FluxSBkBy_4_id);
	std::shared_ptr< pdat::NodeVariable<double> > FluxSBiBz_4(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FluxSBiBz_4",1)));
	d_FluxSBiBz_4_id = vdb->registerVariableAndContext(FluxSBiBz_4 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FluxSBiBz_4_id);
	std::shared_ptr< pdat::NodeVariable<double> > FluxSBjBz_4(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FluxSBjBz_4",1)));
	d_FluxSBjBz_4_id = vdb->registerVariableAndContext(FluxSBjBz_4 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FluxSBjBz_4_id);
	std::shared_ptr< pdat::NodeVariable<double> > SpeedSBjEx_4(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "SpeedSBjEx_4",1)));
	d_SpeedSBjEx_4_id = vdb->registerVariableAndContext(SpeedSBjEx_4 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_SpeedSBjEx_4_id);
	std::shared_ptr< pdat::NodeVariable<double> > SpeedSBkEx_4(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "SpeedSBkEx_4",1)));
	d_SpeedSBkEx_4_id = vdb->registerVariableAndContext(SpeedSBkEx_4 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_SpeedSBkEx_4_id);
	std::shared_ptr< pdat::NodeVariable<double> > SpeedSBiEy_4(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "SpeedSBiEy_4",1)));
	d_SpeedSBiEy_4_id = vdb->registerVariableAndContext(SpeedSBiEy_4 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_SpeedSBiEy_4_id);
	std::shared_ptr< pdat::NodeVariable<double> > SpeedSBkEy_4(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "SpeedSBkEy_4",1)));
	d_SpeedSBkEy_4_id = vdb->registerVariableAndContext(SpeedSBkEy_4 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_SpeedSBkEy_4_id);
	std::shared_ptr< pdat::NodeVariable<double> > SpeedSBiEz_4(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "SpeedSBiEz_4",1)));
	d_SpeedSBiEz_4_id = vdb->registerVariableAndContext(SpeedSBiEz_4 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_SpeedSBiEz_4_id);
	std::shared_ptr< pdat::NodeVariable<double> > SpeedSBjEz_4(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "SpeedSBjEz_4",1)));
	d_SpeedSBjEz_4_id = vdb->registerVariableAndContext(SpeedSBjEz_4 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_SpeedSBjEz_4_id);
	std::shared_ptr< pdat::NodeVariable<double> > SpeedSBjBx_4(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "SpeedSBjBx_4",1)));
	d_SpeedSBjBx_4_id = vdb->registerVariableAndContext(SpeedSBjBx_4 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_SpeedSBjBx_4_id);
	std::shared_ptr< pdat::NodeVariable<double> > SpeedSBkBx_4(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "SpeedSBkBx_4",1)));
	d_SpeedSBkBx_4_id = vdb->registerVariableAndContext(SpeedSBkBx_4 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_SpeedSBkBx_4_id);
	std::shared_ptr< pdat::NodeVariable<double> > SpeedSBiBy_4(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "SpeedSBiBy_4",1)));
	d_SpeedSBiBy_4_id = vdb->registerVariableAndContext(SpeedSBiBy_4 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_SpeedSBiBy_4_id);
	std::shared_ptr< pdat::NodeVariable<double> > SpeedSBkBy_4(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "SpeedSBkBy_4",1)));
	d_SpeedSBkBy_4_id = vdb->registerVariableAndContext(SpeedSBkBy_4 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_SpeedSBkBy_4_id);
	std::shared_ptr< pdat::NodeVariable<double> > SpeedSBiBz_4(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "SpeedSBiBz_4",1)));
	d_SpeedSBiBz_4_id = vdb->registerVariableAndContext(SpeedSBiBz_4 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_SpeedSBiBz_4_id);
	std::shared_ptr< pdat::NodeVariable<double> > SpeedSBjBz_4(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "SpeedSBjBz_4",1)));
	d_SpeedSBjBz_4_id = vdb->registerVariableAndContext(SpeedSBjBz_4 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_SpeedSBjBz_4_id);
	std::shared_ptr< pdat::NodeVariable<double> > K1SBEx_4(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "K1SBEx_4",1)));
	d_K1SBEx_4_id = vdb->registerVariableAndContext(K1SBEx_4 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_K1SBEx_4_id);
	std::shared_ptr< pdat::NodeVariable<double> > K1SBEy_4(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "K1SBEy_4",1)));
	d_K1SBEy_4_id = vdb->registerVariableAndContext(K1SBEy_4 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_K1SBEy_4_id);
	std::shared_ptr< pdat::NodeVariable<double> > K1SBEz_4(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "K1SBEz_4",1)));
	d_K1SBEz_4_id = vdb->registerVariableAndContext(K1SBEz_4 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_K1SBEz_4_id);
	std::shared_ptr< pdat::NodeVariable<double> > K1SBBx_4(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "K1SBBx_4",1)));
	d_K1SBBx_4_id = vdb->registerVariableAndContext(K1SBBx_4 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_K1SBBx_4_id);
	std::shared_ptr< pdat::NodeVariable<double> > K1SBBy_4(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "K1SBBy_4",1)));
	d_K1SBBy_4_id = vdb->registerVariableAndContext(K1SBBy_4 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_K1SBBy_4_id);
	std::shared_ptr< pdat::NodeVariable<double> > K1SBBz_4(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "K1SBBz_4",1)));
	d_K1SBBz_4_id = vdb->registerVariableAndContext(K1SBBz_4 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_K1SBBz_4_id);
	std::shared_ptr< pdat::NodeVariable<double> > ExSPprime_4(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "ExSPprime_4",1)));
	d_ExSPprime_4_id = vdb->registerVariableAndContext(ExSPprime_4 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_ExSPprime_4_id);
	std::shared_ptr< pdat::NodeVariable<double> > EySPprime_4(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "EySPprime_4",1)));
	d_EySPprime_4_id = vdb->registerVariableAndContext(EySPprime_4 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_EySPprime_4_id);
	std::shared_ptr< pdat::NodeVariable<double> > EzSPprime_4(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "EzSPprime_4",1)));
	d_EzSPprime_4_id = vdb->registerVariableAndContext(EzSPprime_4 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_EzSPprime_4_id);
	std::shared_ptr< pdat::NodeVariable<double> > BxSPprime_4(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "BxSPprime_4",1)));
	d_BxSPprime_4_id = vdb->registerVariableAndContext(BxSPprime_4 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_BxSPprime_4_id);
	std::shared_ptr< pdat::NodeVariable<double> > BySPprime_4(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "BySPprime_4",1)));
	d_BySPprime_4_id = vdb->registerVariableAndContext(BySPprime_4 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_BySPprime_4_id);
	std::shared_ptr< pdat::NodeVariable<double> > BzSPprime_4(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "BzSPprime_4",1)));
	d_BzSPprime_4_id = vdb->registerVariableAndContext(BzSPprime_4 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_BzSPprime_4_id);
	std::shared_ptr< pdat::NodeVariable<double> > K2SBEx_4(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "K2SBEx_4",1)));
	d_K2SBEx_4_id = vdb->registerVariableAndContext(K2SBEx_4 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_K2SBEx_4_id);
	std::shared_ptr< pdat::NodeVariable<double> > K2SBEy_4(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "K2SBEy_4",1)));
	d_K2SBEy_4_id = vdb->registerVariableAndContext(K2SBEy_4 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_K2SBEy_4_id);
	std::shared_ptr< pdat::NodeVariable<double> > K2SBEz_4(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "K2SBEz_4",1)));
	d_K2SBEz_4_id = vdb->registerVariableAndContext(K2SBEz_4 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_K2SBEz_4_id);
	std::shared_ptr< pdat::NodeVariable<double> > K2SBBx_4(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "K2SBBx_4",1)));
	d_K2SBBx_4_id = vdb->registerVariableAndContext(K2SBBx_4 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_K2SBBx_4_id);
	std::shared_ptr< pdat::NodeVariable<double> > K2SBBy_4(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "K2SBBy_4",1)));
	d_K2SBBy_4_id = vdb->registerVariableAndContext(K2SBBy_4 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_K2SBBy_4_id);
	std::shared_ptr< pdat::NodeVariable<double> > K2SBBz_4(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "K2SBBz_4",1)));
	d_K2SBBz_4_id = vdb->registerVariableAndContext(K2SBBz_4 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_K2SBBz_4_id);
	std::shared_ptr< pdat::NodeVariable<double> > ExSPprimeprime_4(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "ExSPprimeprime_4",1)));
	d_ExSPprimeprime_4_id = vdb->registerVariableAndContext(ExSPprimeprime_4 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_ExSPprimeprime_4_id);
	std::shared_ptr< pdat::NodeVariable<double> > EySPprimeprime_4(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "EySPprimeprime_4",1)));
	d_EySPprimeprime_4_id = vdb->registerVariableAndContext(EySPprimeprime_4 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_EySPprimeprime_4_id);
	std::shared_ptr< pdat::NodeVariable<double> > EzSPprimeprime_4(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "EzSPprimeprime_4",1)));
	d_EzSPprimeprime_4_id = vdb->registerVariableAndContext(EzSPprimeprime_4 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_EzSPprimeprime_4_id);
	std::shared_ptr< pdat::NodeVariable<double> > BxSPprimeprime_4(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "BxSPprimeprime_4",1)));
	d_BxSPprimeprime_4_id = vdb->registerVariableAndContext(BxSPprimeprime_4 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_BxSPprimeprime_4_id);
	std::shared_ptr< pdat::NodeVariable<double> > BySPprimeprime_4(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "BySPprimeprime_4",1)));
	d_BySPprimeprime_4_id = vdb->registerVariableAndContext(BySPprimeprime_4 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_BySPprimeprime_4_id);
	std::shared_ptr< pdat::NodeVariable<double> > BzSPprimeprime_4(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "BzSPprimeprime_4",1)));
	d_BzSPprimeprime_4_id = vdb->registerVariableAndContext(BzSPprimeprime_4 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_BzSPprimeprime_4_id);
	std::shared_ptr< pdat::NodeVariable<double> > K3SBEx_4(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "K3SBEx_4",1)));
	d_K3SBEx_4_id = vdb->registerVariableAndContext(K3SBEx_4 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_K3SBEx_4_id);
	std::shared_ptr< pdat::NodeVariable<double> > K3SBEy_4(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "K3SBEy_4",1)));
	d_K3SBEy_4_id = vdb->registerVariableAndContext(K3SBEy_4 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_K3SBEy_4_id);
	std::shared_ptr< pdat::NodeVariable<double> > K3SBEz_4(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "K3SBEz_4",1)));
	d_K3SBEz_4_id = vdb->registerVariableAndContext(K3SBEz_4 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_K3SBEz_4_id);
	std::shared_ptr< pdat::NodeVariable<double> > K3SBBx_4(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "K3SBBx_4",1)));
	d_K3SBBx_4_id = vdb->registerVariableAndContext(K3SBBx_4 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_K3SBBx_4_id);
	std::shared_ptr< pdat::NodeVariable<double> > K3SBBy_4(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "K3SBBy_4",1)));
	d_K3SBBy_4_id = vdb->registerVariableAndContext(K3SBBy_4 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_K3SBBy_4_id);
	std::shared_ptr< pdat::NodeVariable<double> > K3SBBz_4(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "K3SBBz_4",1)));
	d_K3SBBz_4_id = vdb->registerVariableAndContext(K3SBBz_4 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_K3SBBz_4_id);
	std::shared_ptr< pdat::NodeVariable<double> > FluxSBjEx_5(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FluxSBjEx_5",1)));
	d_FluxSBjEx_5_id = vdb->registerVariableAndContext(FluxSBjEx_5 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FluxSBjEx_5_id);
	std::shared_ptr< pdat::NodeVariable<double> > FluxSBkEx_5(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FluxSBkEx_5",1)));
	d_FluxSBkEx_5_id = vdb->registerVariableAndContext(FluxSBkEx_5 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FluxSBkEx_5_id);
	std::shared_ptr< pdat::NodeVariable<double> > FluxSBiEy_5(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FluxSBiEy_5",1)));
	d_FluxSBiEy_5_id = vdb->registerVariableAndContext(FluxSBiEy_5 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FluxSBiEy_5_id);
	std::shared_ptr< pdat::NodeVariable<double> > FluxSBkEy_5(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FluxSBkEy_5",1)));
	d_FluxSBkEy_5_id = vdb->registerVariableAndContext(FluxSBkEy_5 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FluxSBkEy_5_id);
	std::shared_ptr< pdat::NodeVariable<double> > FluxSBiEz_5(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FluxSBiEz_5",1)));
	d_FluxSBiEz_5_id = vdb->registerVariableAndContext(FluxSBiEz_5 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FluxSBiEz_5_id);
	std::shared_ptr< pdat::NodeVariable<double> > FluxSBjEz_5(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FluxSBjEz_5",1)));
	d_FluxSBjEz_5_id = vdb->registerVariableAndContext(FluxSBjEz_5 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FluxSBjEz_5_id);
	std::shared_ptr< pdat::NodeVariable<double> > FluxSBjBx_5(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FluxSBjBx_5",1)));
	d_FluxSBjBx_5_id = vdb->registerVariableAndContext(FluxSBjBx_5 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FluxSBjBx_5_id);
	std::shared_ptr< pdat::NodeVariable<double> > FluxSBkBx_5(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FluxSBkBx_5",1)));
	d_FluxSBkBx_5_id = vdb->registerVariableAndContext(FluxSBkBx_5 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FluxSBkBx_5_id);
	std::shared_ptr< pdat::NodeVariable<double> > FluxSBiBy_5(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FluxSBiBy_5",1)));
	d_FluxSBiBy_5_id = vdb->registerVariableAndContext(FluxSBiBy_5 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FluxSBiBy_5_id);
	std::shared_ptr< pdat::NodeVariable<double> > FluxSBkBy_5(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FluxSBkBy_5",1)));
	d_FluxSBkBy_5_id = vdb->registerVariableAndContext(FluxSBkBy_5 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FluxSBkBy_5_id);
	std::shared_ptr< pdat::NodeVariable<double> > FluxSBiBz_5(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FluxSBiBz_5",1)));
	d_FluxSBiBz_5_id = vdb->registerVariableAndContext(FluxSBiBz_5 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FluxSBiBz_5_id);
	std::shared_ptr< pdat::NodeVariable<double> > FluxSBjBz_5(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FluxSBjBz_5",1)));
	d_FluxSBjBz_5_id = vdb->registerVariableAndContext(FluxSBjBz_5 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FluxSBjBz_5_id);
	std::shared_ptr< pdat::NodeVariable<double> > SpeedSBjEx_5(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "SpeedSBjEx_5",1)));
	d_SpeedSBjEx_5_id = vdb->registerVariableAndContext(SpeedSBjEx_5 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_SpeedSBjEx_5_id);
	std::shared_ptr< pdat::NodeVariable<double> > SpeedSBkEx_5(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "SpeedSBkEx_5",1)));
	d_SpeedSBkEx_5_id = vdb->registerVariableAndContext(SpeedSBkEx_5 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_SpeedSBkEx_5_id);
	std::shared_ptr< pdat::NodeVariable<double> > SpeedSBiEy_5(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "SpeedSBiEy_5",1)));
	d_SpeedSBiEy_5_id = vdb->registerVariableAndContext(SpeedSBiEy_5 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_SpeedSBiEy_5_id);
	std::shared_ptr< pdat::NodeVariable<double> > SpeedSBkEy_5(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "SpeedSBkEy_5",1)));
	d_SpeedSBkEy_5_id = vdb->registerVariableAndContext(SpeedSBkEy_5 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_SpeedSBkEy_5_id);
	std::shared_ptr< pdat::NodeVariable<double> > SpeedSBiEz_5(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "SpeedSBiEz_5",1)));
	d_SpeedSBiEz_5_id = vdb->registerVariableAndContext(SpeedSBiEz_5 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_SpeedSBiEz_5_id);
	std::shared_ptr< pdat::NodeVariable<double> > SpeedSBjEz_5(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "SpeedSBjEz_5",1)));
	d_SpeedSBjEz_5_id = vdb->registerVariableAndContext(SpeedSBjEz_5 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_SpeedSBjEz_5_id);
	std::shared_ptr< pdat::NodeVariable<double> > SpeedSBjBx_5(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "SpeedSBjBx_5",1)));
	d_SpeedSBjBx_5_id = vdb->registerVariableAndContext(SpeedSBjBx_5 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_SpeedSBjBx_5_id);
	std::shared_ptr< pdat::NodeVariable<double> > SpeedSBkBx_5(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "SpeedSBkBx_5",1)));
	d_SpeedSBkBx_5_id = vdb->registerVariableAndContext(SpeedSBkBx_5 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_SpeedSBkBx_5_id);
	std::shared_ptr< pdat::NodeVariable<double> > SpeedSBiBy_5(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "SpeedSBiBy_5",1)));
	d_SpeedSBiBy_5_id = vdb->registerVariableAndContext(SpeedSBiBy_5 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_SpeedSBiBy_5_id);
	std::shared_ptr< pdat::NodeVariable<double> > SpeedSBkBy_5(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "SpeedSBkBy_5",1)));
	d_SpeedSBkBy_5_id = vdb->registerVariableAndContext(SpeedSBkBy_5 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_SpeedSBkBy_5_id);
	std::shared_ptr< pdat::NodeVariable<double> > SpeedSBiBz_5(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "SpeedSBiBz_5",1)));
	d_SpeedSBiBz_5_id = vdb->registerVariableAndContext(SpeedSBiBz_5 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_SpeedSBiBz_5_id);
	std::shared_ptr< pdat::NodeVariable<double> > SpeedSBjBz_5(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "SpeedSBjBz_5",1)));
	d_SpeedSBjBz_5_id = vdb->registerVariableAndContext(SpeedSBjBz_5 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_SpeedSBjBz_5_id);
	std::shared_ptr< pdat::NodeVariable<double> > K1SBEx_5(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "K1SBEx_5",1)));
	d_K1SBEx_5_id = vdb->registerVariableAndContext(K1SBEx_5 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_K1SBEx_5_id);
	std::shared_ptr< pdat::NodeVariable<double> > K1SBEy_5(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "K1SBEy_5",1)));
	d_K1SBEy_5_id = vdb->registerVariableAndContext(K1SBEy_5 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_K1SBEy_5_id);
	std::shared_ptr< pdat::NodeVariable<double> > K1SBEz_5(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "K1SBEz_5",1)));
	d_K1SBEz_5_id = vdb->registerVariableAndContext(K1SBEz_5 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_K1SBEz_5_id);
	std::shared_ptr< pdat::NodeVariable<double> > K1SBBx_5(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "K1SBBx_5",1)));
	d_K1SBBx_5_id = vdb->registerVariableAndContext(K1SBBx_5 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_K1SBBx_5_id);
	std::shared_ptr< pdat::NodeVariable<double> > K1SBBy_5(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "K1SBBy_5",1)));
	d_K1SBBy_5_id = vdb->registerVariableAndContext(K1SBBy_5 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_K1SBBy_5_id);
	std::shared_ptr< pdat::NodeVariable<double> > K1SBBz_5(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "K1SBBz_5",1)));
	d_K1SBBz_5_id = vdb->registerVariableAndContext(K1SBBz_5 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_K1SBBz_5_id);
	std::shared_ptr< pdat::NodeVariable<double> > ExSPprime_5(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "ExSPprime_5",1)));
	d_ExSPprime_5_id = vdb->registerVariableAndContext(ExSPprime_5 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_ExSPprime_5_id);
	std::shared_ptr< pdat::NodeVariable<double> > EySPprime_5(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "EySPprime_5",1)));
	d_EySPprime_5_id = vdb->registerVariableAndContext(EySPprime_5 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_EySPprime_5_id);
	std::shared_ptr< pdat::NodeVariable<double> > EzSPprime_5(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "EzSPprime_5",1)));
	d_EzSPprime_5_id = vdb->registerVariableAndContext(EzSPprime_5 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_EzSPprime_5_id);
	std::shared_ptr< pdat::NodeVariable<double> > BxSPprime_5(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "BxSPprime_5",1)));
	d_BxSPprime_5_id = vdb->registerVariableAndContext(BxSPprime_5 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_BxSPprime_5_id);
	std::shared_ptr< pdat::NodeVariable<double> > BySPprime_5(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "BySPprime_5",1)));
	d_BySPprime_5_id = vdb->registerVariableAndContext(BySPprime_5 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_BySPprime_5_id);
	std::shared_ptr< pdat::NodeVariable<double> > BzSPprime_5(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "BzSPprime_5",1)));
	d_BzSPprime_5_id = vdb->registerVariableAndContext(BzSPprime_5 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_BzSPprime_5_id);
	std::shared_ptr< pdat::NodeVariable<double> > K2SBEx_5(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "K2SBEx_5",1)));
	d_K2SBEx_5_id = vdb->registerVariableAndContext(K2SBEx_5 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_K2SBEx_5_id);
	std::shared_ptr< pdat::NodeVariable<double> > K2SBEy_5(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "K2SBEy_5",1)));
	d_K2SBEy_5_id = vdb->registerVariableAndContext(K2SBEy_5 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_K2SBEy_5_id);
	std::shared_ptr< pdat::NodeVariable<double> > K2SBEz_5(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "K2SBEz_5",1)));
	d_K2SBEz_5_id = vdb->registerVariableAndContext(K2SBEz_5 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_K2SBEz_5_id);
	std::shared_ptr< pdat::NodeVariable<double> > K2SBBx_5(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "K2SBBx_5",1)));
	d_K2SBBx_5_id = vdb->registerVariableAndContext(K2SBBx_5 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_K2SBBx_5_id);
	std::shared_ptr< pdat::NodeVariable<double> > K2SBBy_5(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "K2SBBy_5",1)));
	d_K2SBBy_5_id = vdb->registerVariableAndContext(K2SBBy_5 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_K2SBBy_5_id);
	std::shared_ptr< pdat::NodeVariable<double> > K2SBBz_5(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "K2SBBz_5",1)));
	d_K2SBBz_5_id = vdb->registerVariableAndContext(K2SBBz_5 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_K2SBBz_5_id);
	std::shared_ptr< pdat::NodeVariable<double> > ExSPprimeprime_5(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "ExSPprimeprime_5",1)));
	d_ExSPprimeprime_5_id = vdb->registerVariableAndContext(ExSPprimeprime_5 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_ExSPprimeprime_5_id);
	std::shared_ptr< pdat::NodeVariable<double> > EySPprimeprime_5(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "EySPprimeprime_5",1)));
	d_EySPprimeprime_5_id = vdb->registerVariableAndContext(EySPprimeprime_5 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_EySPprimeprime_5_id);
	std::shared_ptr< pdat::NodeVariable<double> > EzSPprimeprime_5(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "EzSPprimeprime_5",1)));
	d_EzSPprimeprime_5_id = vdb->registerVariableAndContext(EzSPprimeprime_5 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_EzSPprimeprime_5_id);
	std::shared_ptr< pdat::NodeVariable<double> > BxSPprimeprime_5(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "BxSPprimeprime_5",1)));
	d_BxSPprimeprime_5_id = vdb->registerVariableAndContext(BxSPprimeprime_5 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_BxSPprimeprime_5_id);
	std::shared_ptr< pdat::NodeVariable<double> > BySPprimeprime_5(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "BySPprimeprime_5",1)));
	d_BySPprimeprime_5_id = vdb->registerVariableAndContext(BySPprimeprime_5 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_BySPprimeprime_5_id);
	std::shared_ptr< pdat::NodeVariable<double> > BzSPprimeprime_5(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "BzSPprimeprime_5",1)));
	d_BzSPprimeprime_5_id = vdb->registerVariableAndContext(BzSPprimeprime_5 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_BzSPprimeprime_5_id);
	std::shared_ptr< pdat::NodeVariable<double> > K3SBEx_5(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "K3SBEx_5",1)));
	d_K3SBEx_5_id = vdb->registerVariableAndContext(K3SBEx_5 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_K3SBEx_5_id);
	std::shared_ptr< pdat::NodeVariable<double> > K3SBEy_5(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "K3SBEy_5",1)));
	d_K3SBEy_5_id = vdb->registerVariableAndContext(K3SBEy_5 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_K3SBEy_5_id);
	std::shared_ptr< pdat::NodeVariable<double> > K3SBEz_5(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "K3SBEz_5",1)));
	d_K3SBEz_5_id = vdb->registerVariableAndContext(K3SBEz_5 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_K3SBEz_5_id);
	std::shared_ptr< pdat::NodeVariable<double> > K3SBBx_5(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "K3SBBx_5",1)));
	d_K3SBBx_5_id = vdb->registerVariableAndContext(K3SBBx_5 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_K3SBBx_5_id);
	std::shared_ptr< pdat::NodeVariable<double> > K3SBBy_5(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "K3SBBy_5",1)));
	d_K3SBBy_5_id = vdb->registerVariableAndContext(K3SBBy_5 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_K3SBBy_5_id);
	std::shared_ptr< pdat::NodeVariable<double> > K3SBBz_5(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "K3SBBz_5",1)));
	d_K3SBBz_5_id = vdb->registerVariableAndContext(K3SBBz_5 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_K3SBBz_5_id);
	std::shared_ptr< pdat::NodeVariable<double> > d_i_Ey(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "d_i_Ey",1)));
	d_d_i_Ey_id = vdb->registerVariableAndContext(d_i_Ey ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_d_i_Ey_id);
	std::shared_ptr< pdat::NodeVariable<double> > d_j_Ey(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "d_j_Ey",1)));
	d_d_j_Ey_id = vdb->registerVariableAndContext(d_j_Ey ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_d_j_Ey_id);
	std::shared_ptr< pdat::NodeVariable<double> > d_k_Ey(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "d_k_Ey",1)));
	d_d_k_Ey_id = vdb->registerVariableAndContext(d_k_Ey ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_d_k_Ey_id);
	std::shared_ptr< pdat::NodeVariable<double> > d_i_Ex(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "d_i_Ex",1)));
	d_d_i_Ex_id = vdb->registerVariableAndContext(d_i_Ex ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_d_i_Ex_id);
	std::shared_ptr< pdat::NodeVariable<double> > d_j_Ex(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "d_j_Ex",1)));
	d_d_j_Ex_id = vdb->registerVariableAndContext(d_j_Ex ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_d_j_Ex_id);
	std::shared_ptr< pdat::NodeVariable<double> > d_k_Ex(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "d_k_Ex",1)));
	d_d_k_Ex_id = vdb->registerVariableAndContext(d_k_Ex ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_d_k_Ex_id);
	std::shared_ptr< pdat::NodeVariable<double> > stalled_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "stalled_1",1)));
	d_stalled_1_id = vdb->registerVariableAndContext(stalled_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_stalled_1_id);
	std::shared_ptr< pdat::NodeVariable<double> > stalled_2(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "stalled_2",1)));
	d_stalled_2_id = vdb->registerVariableAndContext(stalled_2 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_stalled_2_id);
	std::shared_ptr< pdat::NodeVariable<double> > stalled_4(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "stalled_4",1)));
	d_stalled_4_id = vdb->registerVariableAndContext(stalled_4 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_stalled_4_id);
	std::shared_ptr< pdat::NodeVariable<double> > stalled_5(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "stalled_5",1)));
	d_stalled_5_id = vdb->registerVariableAndContext(stalled_5 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_stalled_5_id);
	std::shared_ptr< pdat::NodeVariable<double> > stalled_7(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "stalled_7",1)));
	d_stalled_7_id = vdb->registerVariableAndContext(stalled_7 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_stalled_7_id);
	std::shared_ptr< pdat::NodeVariable<double> > stalled_8(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "stalled_8",1)));
	d_stalled_8_id = vdb->registerVariableAndContext(stalled_8 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_stalled_8_id);


	//Refine and coarse algorithms

	d_bdry_fill_init = std::shared_ptr< xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());
	d_bdry_fill_advance1 = std::shared_ptr< xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());
	d_bdry_fill_advance4 = std::shared_ptr< xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());
	d_bdry_fill_advance6 = std::shared_ptr< xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());
	d_bdry_fill_advance7 = std::shared_ptr< xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());
	d_coarsen_algorithm = std::shared_ptr< xfer::CoarsenAlgorithm >(new xfer::CoarsenAlgorithm(d_dim));

	d_mapping_fill = std::shared_ptr< xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());
	d_fill_new_level    = std::shared_ptr< xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());

	//mapping communication

	std::shared_ptr< hier::RefineOperator > refine_operator_map = d_grid_geometry->lookupRefineOperator(interior, "LINEAR_REFINE");
	d_mapping_fill->registerRefine(d_interior_id,d_interior_id,d_interior_id, refine_operator_map);
	d_mapping_fill->registerRefine(d_interior_i_id,d_interior_i_id,d_interior_i_id, refine_operator_map);
	d_mapping_fill->registerRefine(d_interior_j_id,d_interior_j_id,d_interior_j_id, refine_operator_map);
	d_mapping_fill->registerRefine(d_interior_k_id,d_interior_k_id,d_interior_k_id, refine_operator_map);
	d_mapping_fill->registerRefine(d_FOV_1_id,d_FOV_1_id,d_FOV_1_id, refine_operator_map);
	d_mapping_fill->registerRefine(d_FOV_2_id,d_FOV_2_id,d_FOV_2_id, refine_operator_map);
	d_mapping_fill->registerRefine(d_FOV_4_id,d_FOV_4_id,d_FOV_4_id, refine_operator_map);
	d_mapping_fill->registerRefine(d_FOV_5_id,d_FOV_5_id,d_FOV_5_id, refine_operator_map);
	d_mapping_fill->registerRefine(d_FOV_7_id,d_FOV_7_id,d_FOV_7_id, refine_operator_map);
	d_mapping_fill->registerRefine(d_FOV_8_id,d_FOV_8_id,d_FOV_8_id, refine_operator_map);
	d_mapping_fill->registerRefine(d_FOV_xLower_id,d_FOV_xLower_id,d_FOV_xLower_id, refine_operator_map);
	d_mapping_fill->registerRefine(d_FOV_xUpper_id,d_FOV_xUpper_id,d_FOV_xUpper_id, refine_operator_map);
	d_mapping_fill->registerRefine(d_FOV_yLower_id,d_FOV_yLower_id,d_FOV_yLower_id, refine_operator_map);
	d_mapping_fill->registerRefine(d_FOV_yUpper_id,d_FOV_yUpper_id,d_FOV_yUpper_id, refine_operator_map);
	d_mapping_fill->registerRefine(d_FOV_zLower_id,d_FOV_zLower_id,d_FOV_zLower_id, refine_operator_map);
	d_mapping_fill->registerRefine(d_FOV_zUpper_id,d_FOV_zUpper_id,d_FOV_zUpper_id, refine_operator_map);
	d_mapping_fill->registerRefine(d_stalled_1_id,d_stalled_1_id,d_stalled_1_id, refine_operator_map);
	d_mapping_fill->registerRefine(d_stalled_2_id,d_stalled_2_id,d_stalled_2_id, refine_operator_map);
	d_mapping_fill->registerRefine(d_stalled_4_id,d_stalled_4_id,d_stalled_4_id, refine_operator_map);
	d_mapping_fill->registerRefine(d_stalled_5_id,d_stalled_5_id,d_stalled_5_id, refine_operator_map);
	d_mapping_fill->registerRefine(d_stalled_7_id,d_stalled_7_id,d_stalled_7_id, refine_operator_map);
	d_mapping_fill->registerRefine(d_stalled_8_id,d_stalled_8_id,d_stalled_8_id, refine_operator_map);


	//refine and coarsen operators
	string refine_op_name = "LINEAR_REFINE";
	int order = 0;
	if (database->isDatabase("regridding")) {
		regridding_db = database->getDatabase("regridding");
		if (regridding_db->isString("interpolator")) {
			refine_op_name = regridding_db->getString("interpolator");
			if (refine_op_name == "LINEAR_REFINE") {
				order = 1;
			}
			if (refine_op_name == "CUBIC_REFINE") {
				order = 3;
			}
			if (refine_op_name == "QUINTIC_REFINE") {
				order = 5;
			}
		}
	}
	std::shared_ptr< hier::RefineOperator > refine_operator, refine_operator_bound;
	std::shared_ptr< hier::CoarsenOperator > coarsen_operator = d_grid_geometry->lookupCoarsenOperator(Ex, "CONSTANT_COARSEN");
	if (order > 0) {
		std::shared_ptr< hier::RefineOperator > tmp_refine_operator(new LagrangianPolynomicRefine(false, order, d_dim));
		refine_operator = tmp_refine_operator;
		std::shared_ptr< hier::RefineOperator > tmp_refine_operator_bound(new LagrangianPolynomicRefine(true, order, d_dim));
		refine_operator_bound = tmp_refine_operator_bound;
	} else {
		refine_operator = d_grid_geometry->lookupRefineOperator(Ex, refine_op_name);
		refine_operator_bound = d_grid_geometry->lookupRefineOperator(Ex, refine_op_name);
	}

	time_interpolate_operator = new TimeInterpolator();
	std::shared_ptr<SAMRAI::hier::TimeInterpolateOperator> tio(time_interpolate_operator);


	//Register variables to the refineAlgorithm for boundaries

	d_bdry_fill_advance1->registerRefine(d_BzSPprime_5_id,d_BzSPprime_5_id,d_BzSPprime_5_id,refine_operator);
	d_bdry_fill_advance1->registerRefine(d_BySPprime_5_id,d_BySPprime_5_id,d_BySPprime_5_id,refine_operator);
	d_bdry_fill_advance1->registerRefine(d_BxSPprime_5_id,d_BxSPprime_5_id,d_BxSPprime_5_id,refine_operator);
	d_bdry_fill_advance1->registerRefine(d_EzSPprime_5_id,d_EzSPprime_5_id,d_EzSPprime_5_id,refine_operator);
	d_bdry_fill_advance1->registerRefine(d_EySPprime_5_id,d_EySPprime_5_id,d_EySPprime_5_id,refine_operator);
	d_bdry_fill_advance1->registerRefine(d_ExSPprime_5_id,d_ExSPprime_5_id,d_ExSPprime_5_id,refine_operator);
	d_bdry_fill_advance1->registerRefine(d_K1SBBz_5_id,d_K1SBBz_5_id,d_K1SBBz_5_id,refine_operator);
	d_bdry_fill_advance1->registerRefine(d_K1SBBy_5_id,d_K1SBBy_5_id,d_K1SBBy_5_id,refine_operator);
	d_bdry_fill_advance1->registerRefine(d_K1SBBx_5_id,d_K1SBBx_5_id,d_K1SBBx_5_id,refine_operator);
	d_bdry_fill_advance1->registerRefine(d_K1SBEz_5_id,d_K1SBEz_5_id,d_K1SBEz_5_id,refine_operator);
	d_bdry_fill_advance1->registerRefine(d_K1SBEy_5_id,d_K1SBEy_5_id,d_K1SBEy_5_id,refine_operator);
	d_bdry_fill_advance1->registerRefine(d_K1SBEx_5_id,d_K1SBEx_5_id,d_K1SBEx_5_id,refine_operator);
	d_bdry_fill_advance1->registerRefine(d_BzSPprime_4_id,d_BzSPprime_4_id,d_BzSPprime_4_id,refine_operator);
	d_bdry_fill_advance1->registerRefine(d_BySPprime_4_id,d_BySPprime_4_id,d_BySPprime_4_id,refine_operator);
	d_bdry_fill_advance1->registerRefine(d_BxSPprime_4_id,d_BxSPprime_4_id,d_BxSPprime_4_id,refine_operator);
	d_bdry_fill_advance1->registerRefine(d_EzSPprime_4_id,d_EzSPprime_4_id,d_EzSPprime_4_id,refine_operator);
	d_bdry_fill_advance1->registerRefine(d_EySPprime_4_id,d_EySPprime_4_id,d_EySPprime_4_id,refine_operator);
	d_bdry_fill_advance1->registerRefine(d_ExSPprime_4_id,d_ExSPprime_4_id,d_ExSPprime_4_id,refine_operator);
	d_bdry_fill_advance1->registerRefine(d_K1SBBz_4_id,d_K1SBBz_4_id,d_K1SBBz_4_id,refine_operator);
	d_bdry_fill_advance1->registerRefine(d_K1SBBy_4_id,d_K1SBBy_4_id,d_K1SBBy_4_id,refine_operator);
	d_bdry_fill_advance1->registerRefine(d_K1SBBx_4_id,d_K1SBBx_4_id,d_K1SBBx_4_id,refine_operator);
	d_bdry_fill_advance1->registerRefine(d_K1SBEz_4_id,d_K1SBEz_4_id,d_K1SBEz_4_id,refine_operator);
	d_bdry_fill_advance1->registerRefine(d_K1SBEy_4_id,d_K1SBEy_4_id,d_K1SBEy_4_id,refine_operator);
	d_bdry_fill_advance1->registerRefine(d_K1SBEx_4_id,d_K1SBEx_4_id,d_K1SBEx_4_id,refine_operator);
	d_bdry_fill_advance1->registerRefine(d_BzSPprime_1_id,d_BzSPprime_1_id,d_BzSPprime_1_id,refine_operator);
	d_bdry_fill_advance1->registerRefine(d_BySPprime_1_id,d_BySPprime_1_id,d_BySPprime_1_id,refine_operator);
	d_bdry_fill_advance1->registerRefine(d_BxSPprime_1_id,d_BxSPprime_1_id,d_BxSPprime_1_id,refine_operator);
	d_bdry_fill_advance1->registerRefine(d_EzSPprime_1_id,d_EzSPprime_1_id,d_EzSPprime_1_id,refine_operator);
	d_bdry_fill_advance1->registerRefine(d_EySPprime_1_id,d_EySPprime_1_id,d_EySPprime_1_id,refine_operator);
	d_bdry_fill_advance1->registerRefine(d_ExSPprime_1_id,d_ExSPprime_1_id,d_ExSPprime_1_id,refine_operator);
	d_bdry_fill_advance1->registerRefine(d_K1SBBz_1_id,d_K1SBBz_1_id,d_K1SBBz_1_id,refine_operator);
	d_bdry_fill_advance1->registerRefine(d_K1SBBy_1_id,d_K1SBBy_1_id,d_K1SBBy_1_id,refine_operator);
	d_bdry_fill_advance1->registerRefine(d_K1SBBx_1_id,d_K1SBBx_1_id,d_K1SBBx_1_id,refine_operator);
	d_bdry_fill_advance1->registerRefine(d_K1SBEz_1_id,d_K1SBEz_1_id,d_K1SBEz_1_id,refine_operator);
	d_bdry_fill_advance1->registerRefine(d_K1SBEy_1_id,d_K1SBEy_1_id,d_K1SBEy_1_id,refine_operator);
	d_bdry_fill_advance1->registerRefine(d_K1SBEx_1_id,d_K1SBEx_1_id,d_K1SBEx_1_id,refine_operator);
	d_bdry_fill_advance1->registerRefine(d_BzSPprime_7_id,d_BzSPprime_7_id,d_BzSPprime_7_id,refine_operator);
	d_bdry_fill_advance1->registerRefine(d_BySPprime_7_id,d_BySPprime_7_id,d_BySPprime_7_id,refine_operator);
	d_bdry_fill_advance1->registerRefine(d_BxSPprime_7_id,d_BxSPprime_7_id,d_BxSPprime_7_id,refine_operator);
	d_bdry_fill_advance1->registerRefine(d_EzSPprime_7_id,d_EzSPprime_7_id,d_EzSPprime_7_id,refine_operator);
	d_bdry_fill_advance1->registerRefine(d_EySPprime_7_id,d_EySPprime_7_id,d_EySPprime_7_id,refine_operator);
	d_bdry_fill_advance1->registerRefine(d_ExSPprime_7_id,d_ExSPprime_7_id,d_ExSPprime_7_id,refine_operator);
	d_bdry_fill_advance1->registerRefine(d_K1SBBz_7_id,d_K1SBBz_7_id,d_K1SBBz_7_id,refine_operator);
	d_bdry_fill_advance1->registerRefine(d_K1SBBy_7_id,d_K1SBBy_7_id,d_K1SBBy_7_id,refine_operator);
	d_bdry_fill_advance1->registerRefine(d_K1SBBx_7_id,d_K1SBBx_7_id,d_K1SBBx_7_id,refine_operator);
	d_bdry_fill_advance1->registerRefine(d_K1SBEz_7_id,d_K1SBEz_7_id,d_K1SBEz_7_id,refine_operator);
	d_bdry_fill_advance1->registerRefine(d_K1SBEy_7_id,d_K1SBEy_7_id,d_K1SBEy_7_id,refine_operator);
	d_bdry_fill_advance1->registerRefine(d_K1SBEx_7_id,d_K1SBEx_7_id,d_K1SBEx_7_id,refine_operator);
	d_bdry_fill_advance4->registerRefine(d_BzSPprimeprime_5_id,d_BzSPprimeprime_5_id,d_BzSPprimeprime_5_id,refine_operator);
	d_bdry_fill_advance4->registerRefine(d_BySPprimeprime_5_id,d_BySPprimeprime_5_id,d_BySPprimeprime_5_id,refine_operator);
	d_bdry_fill_advance4->registerRefine(d_BxSPprimeprime_5_id,d_BxSPprimeprime_5_id,d_BxSPprimeprime_5_id,refine_operator);
	d_bdry_fill_advance4->registerRefine(d_EzSPprimeprime_5_id,d_EzSPprimeprime_5_id,d_EzSPprimeprime_5_id,refine_operator);
	d_bdry_fill_advance4->registerRefine(d_EySPprimeprime_5_id,d_EySPprimeprime_5_id,d_EySPprimeprime_5_id,refine_operator);
	d_bdry_fill_advance4->registerRefine(d_ExSPprimeprime_5_id,d_ExSPprimeprime_5_id,d_ExSPprimeprime_5_id,refine_operator);
	d_bdry_fill_advance4->registerRefine(d_K2SBBz_5_id,d_K2SBBz_5_id,d_K2SBBz_5_id,refine_operator);
	d_bdry_fill_advance4->registerRefine(d_K2SBBy_5_id,d_K2SBBy_5_id,d_K2SBBy_5_id,refine_operator);
	d_bdry_fill_advance4->registerRefine(d_K2SBBx_5_id,d_K2SBBx_5_id,d_K2SBBx_5_id,refine_operator);
	d_bdry_fill_advance4->registerRefine(d_K2SBEz_5_id,d_K2SBEz_5_id,d_K2SBEz_5_id,refine_operator);
	d_bdry_fill_advance4->registerRefine(d_K2SBEy_5_id,d_K2SBEy_5_id,d_K2SBEy_5_id,refine_operator);
	d_bdry_fill_advance4->registerRefine(d_K2SBEx_5_id,d_K2SBEx_5_id,d_K2SBEx_5_id,refine_operator);
	d_bdry_fill_advance4->registerRefine(d_BzSPprimeprime_4_id,d_BzSPprimeprime_4_id,d_BzSPprimeprime_4_id,refine_operator);
	d_bdry_fill_advance4->registerRefine(d_BySPprimeprime_4_id,d_BySPprimeprime_4_id,d_BySPprimeprime_4_id,refine_operator);
	d_bdry_fill_advance4->registerRefine(d_BxSPprimeprime_4_id,d_BxSPprimeprime_4_id,d_BxSPprimeprime_4_id,refine_operator);
	d_bdry_fill_advance4->registerRefine(d_EzSPprimeprime_4_id,d_EzSPprimeprime_4_id,d_EzSPprimeprime_4_id,refine_operator);
	d_bdry_fill_advance4->registerRefine(d_EySPprimeprime_4_id,d_EySPprimeprime_4_id,d_EySPprimeprime_4_id,refine_operator);
	d_bdry_fill_advance4->registerRefine(d_ExSPprimeprime_4_id,d_ExSPprimeprime_4_id,d_ExSPprimeprime_4_id,refine_operator);
	d_bdry_fill_advance4->registerRefine(d_K2SBBz_4_id,d_K2SBBz_4_id,d_K2SBBz_4_id,refine_operator);
	d_bdry_fill_advance4->registerRefine(d_K2SBBy_4_id,d_K2SBBy_4_id,d_K2SBBy_4_id,refine_operator);
	d_bdry_fill_advance4->registerRefine(d_K2SBBx_4_id,d_K2SBBx_4_id,d_K2SBBx_4_id,refine_operator);
	d_bdry_fill_advance4->registerRefine(d_K2SBEz_4_id,d_K2SBEz_4_id,d_K2SBEz_4_id,refine_operator);
	d_bdry_fill_advance4->registerRefine(d_K2SBEy_4_id,d_K2SBEy_4_id,d_K2SBEy_4_id,refine_operator);
	d_bdry_fill_advance4->registerRefine(d_K2SBEx_4_id,d_K2SBEx_4_id,d_K2SBEx_4_id,refine_operator);
	d_bdry_fill_advance4->registerRefine(d_BzSPprimeprime_7_id,d_BzSPprimeprime_7_id,d_BzSPprimeprime_7_id,refine_operator);
	d_bdry_fill_advance4->registerRefine(d_BySPprimeprime_7_id,d_BySPprimeprime_7_id,d_BySPprimeprime_7_id,refine_operator);
	d_bdry_fill_advance4->registerRefine(d_BxSPprimeprime_7_id,d_BxSPprimeprime_7_id,d_BxSPprimeprime_7_id,refine_operator);
	d_bdry_fill_advance4->registerRefine(d_EzSPprimeprime_7_id,d_EzSPprimeprime_7_id,d_EzSPprimeprime_7_id,refine_operator);
	d_bdry_fill_advance4->registerRefine(d_EySPprimeprime_7_id,d_EySPprimeprime_7_id,d_EySPprimeprime_7_id,refine_operator);
	d_bdry_fill_advance4->registerRefine(d_ExSPprimeprime_7_id,d_ExSPprimeprime_7_id,d_ExSPprimeprime_7_id,refine_operator);
	d_bdry_fill_advance4->registerRefine(d_K2SBBz_7_id,d_K2SBBz_7_id,d_K2SBBz_7_id,refine_operator);
	d_bdry_fill_advance4->registerRefine(d_K2SBBy_7_id,d_K2SBBy_7_id,d_K2SBBy_7_id,refine_operator);
	d_bdry_fill_advance4->registerRefine(d_K2SBBx_7_id,d_K2SBBx_7_id,d_K2SBBx_7_id,refine_operator);
	d_bdry_fill_advance4->registerRefine(d_K2SBEz_7_id,d_K2SBEz_7_id,d_K2SBEz_7_id,refine_operator);
	d_bdry_fill_advance4->registerRefine(d_K2SBEy_7_id,d_K2SBEy_7_id,d_K2SBEy_7_id,refine_operator);
	d_bdry_fill_advance4->registerRefine(d_K2SBEx_7_id,d_K2SBEx_7_id,d_K2SBEx_7_id,refine_operator);
	d_bdry_fill_advance6->registerRefine(d_SpeedSBjBz_5_id,d_SpeedSBjBz_5_id,d_SpeedSBjBz_5_id,refine_operator);
	d_bdry_fill_advance6->registerRefine(d_SpeedSBiBz_5_id,d_SpeedSBiBz_5_id,d_SpeedSBiBz_5_id,refine_operator);
	d_bdry_fill_advance6->registerRefine(d_SpeedSBkBy_5_id,d_SpeedSBkBy_5_id,d_SpeedSBkBy_5_id,refine_operator);
	d_bdry_fill_advance6->registerRefine(d_SpeedSBiBy_5_id,d_SpeedSBiBy_5_id,d_SpeedSBiBy_5_id,refine_operator);
	d_bdry_fill_advance6->registerRefine(d_SpeedSBkBx_5_id,d_SpeedSBkBx_5_id,d_SpeedSBkBx_5_id,refine_operator);
	d_bdry_fill_advance6->registerRefine(d_SpeedSBjBx_5_id,d_SpeedSBjBx_5_id,d_SpeedSBjBx_5_id,refine_operator);
	d_bdry_fill_advance6->registerRefine(d_SpeedSBjEz_5_id,d_SpeedSBjEz_5_id,d_SpeedSBjEz_5_id,refine_operator);
	d_bdry_fill_advance6->registerRefine(d_SpeedSBiEz_5_id,d_SpeedSBiEz_5_id,d_SpeedSBiEz_5_id,refine_operator);
	d_bdry_fill_advance6->registerRefine(d_SpeedSBkEy_5_id,d_SpeedSBkEy_5_id,d_SpeedSBkEy_5_id,refine_operator);
	d_bdry_fill_advance6->registerRefine(d_SpeedSBiEy_5_id,d_SpeedSBiEy_5_id,d_SpeedSBiEy_5_id,refine_operator);
	d_bdry_fill_advance6->registerRefine(d_SpeedSBkEx_5_id,d_SpeedSBkEx_5_id,d_SpeedSBkEx_5_id,refine_operator);
	d_bdry_fill_advance6->registerRefine(d_SpeedSBjEx_5_id,d_SpeedSBjEx_5_id,d_SpeedSBjEx_5_id,refine_operator);
	d_bdry_fill_advance6->registerRefine(d_FluxSBjBz_5_id,d_FluxSBjBz_5_id,d_FluxSBjBz_5_id,refine_operator);
	d_bdry_fill_advance6->registerRefine(d_FluxSBiBz_5_id,d_FluxSBiBz_5_id,d_FluxSBiBz_5_id,refine_operator);
	d_bdry_fill_advance6->registerRefine(d_FluxSBkBy_5_id,d_FluxSBkBy_5_id,d_FluxSBkBy_5_id,refine_operator);
	d_bdry_fill_advance6->registerRefine(d_FluxSBiBy_5_id,d_FluxSBiBy_5_id,d_FluxSBiBy_5_id,refine_operator);
	d_bdry_fill_advance6->registerRefine(d_FluxSBkBx_5_id,d_FluxSBkBx_5_id,d_FluxSBkBx_5_id,refine_operator);
	d_bdry_fill_advance6->registerRefine(d_FluxSBjBx_5_id,d_FluxSBjBx_5_id,d_FluxSBjBx_5_id,refine_operator);
	d_bdry_fill_advance6->registerRefine(d_FluxSBjEz_5_id,d_FluxSBjEz_5_id,d_FluxSBjEz_5_id,refine_operator);
	d_bdry_fill_advance6->registerRefine(d_FluxSBiEz_5_id,d_FluxSBiEz_5_id,d_FluxSBiEz_5_id,refine_operator);
	d_bdry_fill_advance6->registerRefine(d_FluxSBkEy_5_id,d_FluxSBkEy_5_id,d_FluxSBkEy_5_id,refine_operator);
	d_bdry_fill_advance6->registerRefine(d_FluxSBiEy_5_id,d_FluxSBiEy_5_id,d_FluxSBiEy_5_id,refine_operator);
	d_bdry_fill_advance6->registerRefine(d_FluxSBkEx_5_id,d_FluxSBkEx_5_id,d_FluxSBkEx_5_id,refine_operator);
	d_bdry_fill_advance6->registerRefine(d_FluxSBjEx_5_id,d_FluxSBjEx_5_id,d_FluxSBjEx_5_id,refine_operator);
	d_bdry_fill_advance6->registerRefine(d_SpeedSBjBz_4_id,d_SpeedSBjBz_4_id,d_SpeedSBjBz_4_id,refine_operator);
	d_bdry_fill_advance6->registerRefine(d_SpeedSBiBz_4_id,d_SpeedSBiBz_4_id,d_SpeedSBiBz_4_id,refine_operator);
	d_bdry_fill_advance6->registerRefine(d_SpeedSBkBy_4_id,d_SpeedSBkBy_4_id,d_SpeedSBkBy_4_id,refine_operator);
	d_bdry_fill_advance6->registerRefine(d_SpeedSBiBy_4_id,d_SpeedSBiBy_4_id,d_SpeedSBiBy_4_id,refine_operator);
	d_bdry_fill_advance6->registerRefine(d_SpeedSBkBx_4_id,d_SpeedSBkBx_4_id,d_SpeedSBkBx_4_id,refine_operator);
	d_bdry_fill_advance6->registerRefine(d_SpeedSBjBx_4_id,d_SpeedSBjBx_4_id,d_SpeedSBjBx_4_id,refine_operator);
	d_bdry_fill_advance6->registerRefine(d_SpeedSBjEz_4_id,d_SpeedSBjEz_4_id,d_SpeedSBjEz_4_id,refine_operator);
	d_bdry_fill_advance6->registerRefine(d_SpeedSBiEz_4_id,d_SpeedSBiEz_4_id,d_SpeedSBiEz_4_id,refine_operator);
	d_bdry_fill_advance6->registerRefine(d_SpeedSBkEy_4_id,d_SpeedSBkEy_4_id,d_SpeedSBkEy_4_id,refine_operator);
	d_bdry_fill_advance6->registerRefine(d_SpeedSBiEy_4_id,d_SpeedSBiEy_4_id,d_SpeedSBiEy_4_id,refine_operator);
	d_bdry_fill_advance6->registerRefine(d_SpeedSBkEx_4_id,d_SpeedSBkEx_4_id,d_SpeedSBkEx_4_id,refine_operator);
	d_bdry_fill_advance6->registerRefine(d_SpeedSBjEx_4_id,d_SpeedSBjEx_4_id,d_SpeedSBjEx_4_id,refine_operator);
	d_bdry_fill_advance6->registerRefine(d_FluxSBjBz_4_id,d_FluxSBjBz_4_id,d_FluxSBjBz_4_id,refine_operator);
	d_bdry_fill_advance6->registerRefine(d_FluxSBiBz_4_id,d_FluxSBiBz_4_id,d_FluxSBiBz_4_id,refine_operator);
	d_bdry_fill_advance6->registerRefine(d_FluxSBkBy_4_id,d_FluxSBkBy_4_id,d_FluxSBkBy_4_id,refine_operator);
	d_bdry_fill_advance6->registerRefine(d_FluxSBiBy_4_id,d_FluxSBiBy_4_id,d_FluxSBiBy_4_id,refine_operator);
	d_bdry_fill_advance6->registerRefine(d_FluxSBkBx_4_id,d_FluxSBkBx_4_id,d_FluxSBkBx_4_id,refine_operator);
	d_bdry_fill_advance6->registerRefine(d_FluxSBjBx_4_id,d_FluxSBjBx_4_id,d_FluxSBjBx_4_id,refine_operator);
	d_bdry_fill_advance6->registerRefine(d_FluxSBjEz_4_id,d_FluxSBjEz_4_id,d_FluxSBjEz_4_id,refine_operator);
	d_bdry_fill_advance6->registerRefine(d_FluxSBiEz_4_id,d_FluxSBiEz_4_id,d_FluxSBiEz_4_id,refine_operator);
	d_bdry_fill_advance6->registerRefine(d_FluxSBkEy_4_id,d_FluxSBkEy_4_id,d_FluxSBkEy_4_id,refine_operator);
	d_bdry_fill_advance6->registerRefine(d_FluxSBiEy_4_id,d_FluxSBiEy_4_id,d_FluxSBiEy_4_id,refine_operator);
	d_bdry_fill_advance6->registerRefine(d_FluxSBkEx_4_id,d_FluxSBkEx_4_id,d_FluxSBkEx_4_id,refine_operator);
	d_bdry_fill_advance6->registerRefine(d_FluxSBjEx_4_id,d_FluxSBjEx_4_id,d_FluxSBjEx_4_id,refine_operator);
	d_bdry_fill_advance6->registerRefine(d_Bz_id,d_Bz_id,d_Bz_id,refine_operator);
	d_bdry_fill_advance6->registerRefine(d_By_id,d_By_id,d_By_id,refine_operator);
	d_bdry_fill_advance6->registerRefine(d_K3SBBy_7_id,d_K3SBBy_7_id,d_K3SBBy_7_id,refine_operator);
	d_bdry_fill_advance6->registerRefine(d_K3SBEy_7_id,d_K3SBEy_7_id,d_K3SBEy_7_id,refine_operator);
	d_bdry_fill_advance6->registerRefine(d_K3SBEx_7_id,d_K3SBEx_7_id,d_K3SBEx_7_id,refine_operator);
	d_bdry_fill_advance7->registerRefine(d_Bz_id,d_Bz_id,d_Bz_id,refine_operator);
	d_bdry_fill_advance7->registerRefine(d_By_id,d_By_id,d_By_id,refine_operator);
	d_bdry_fill_advance7->registerRefine(d_Bx_id,d_Bx_id,d_Bx_id,refine_operator);
	d_bdry_fill_advance7->registerRefine(d_Ez_id,d_Ez_id,d_Ez_id,refine_operator);
	d_bdry_fill_advance7->registerRefine(d_Ey_id,d_Ey_id,d_Ey_id,refine_operator);
	d_bdry_fill_advance7->registerRefine(d_Ex_id,d_Ex_id,d_Ex_id,refine_operator);
	d_bdry_fill_advance7->registerRefine(d_K3SBBz_5_id,d_K3SBBz_5_id,d_K3SBBz_5_id,refine_operator);
	d_bdry_fill_advance7->registerRefine(d_K3SBBy_5_id,d_K3SBBy_5_id,d_K3SBBy_5_id,refine_operator);
	d_bdry_fill_advance7->registerRefine(d_K3SBBx_5_id,d_K3SBBx_5_id,d_K3SBBx_5_id,refine_operator);
	d_bdry_fill_advance7->registerRefine(d_K3SBEz_5_id,d_K3SBEz_5_id,d_K3SBEz_5_id,refine_operator);
	d_bdry_fill_advance7->registerRefine(d_K3SBEy_5_id,d_K3SBEy_5_id,d_K3SBEy_5_id,refine_operator);
	d_bdry_fill_advance7->registerRefine(d_K3SBEx_5_id,d_K3SBEx_5_id,d_K3SBEx_5_id,refine_operator);
	d_bdry_fill_advance7->registerRefine(d_K3SBBz_4_id,d_K3SBBz_4_id,d_K3SBBz_4_id,refine_operator);
	d_bdry_fill_advance7->registerRefine(d_K3SBBy_4_id,d_K3SBBy_4_id,d_K3SBBy_4_id,refine_operator);
	d_bdry_fill_advance7->registerRefine(d_K3SBBx_4_id,d_K3SBBx_4_id,d_K3SBBx_4_id,refine_operator);
	d_bdry_fill_advance7->registerRefine(d_K3SBEz_4_id,d_K3SBEz_4_id,d_K3SBEz_4_id,refine_operator);
	d_bdry_fill_advance7->registerRefine(d_K3SBEy_4_id,d_K3SBEy_4_id,d_K3SBEy_4_id,refine_operator);
	d_bdry_fill_advance7->registerRefine(d_K3SBEx_4_id,d_K3SBEx_4_id,d_K3SBEx_4_id,refine_operator);
	d_bdry_fill_init->registerRefine(d_Ex_id,d_Ex_id,d_Ex_id,refine_operator);
	d_bdry_fill_init->registerRefine(d_Ey_id,d_Ey_id,d_Ey_id,refine_operator);
	d_bdry_fill_init->registerRefine(d_Ez_id,d_Ez_id,d_Ez_id,refine_operator);
	d_bdry_fill_init->registerRefine(d_Bx_id,d_Bx_id,d_Bx_id,refine_operator);
	d_bdry_fill_init->registerRefine(d_By_id,d_By_id,d_By_id,refine_operator);
	d_bdry_fill_init->registerRefine(d_Bz_id,d_Bz_id,d_Bz_id,refine_operator);
	d_bdry_fill_init->registerRefine(d_q_id,d_q_id,d_q_id,refine_operator);
	d_bdry_fill_init->registerRefine(d_Jx_id,d_Jx_id,d_Jx_id,refine_operator);
	d_bdry_fill_init->registerRefine(d_Jy_id,d_Jy_id,d_Jy_id,refine_operator);
	d_bdry_fill_init->registerRefine(d_Jz_id,d_Jz_id,d_Jz_id,refine_operator);


	//Register variables to the refineAlgorithm for filling new levels on regridding
	d_fill_new_level->registerRefine(d_Ex_id,d_Ex_id,d_Ex_id,refine_operator_bound);
	d_fill_new_level->registerRefine(d_Ey_id,d_Ey_id,d_Ey_id,refine_operator_bound);
	d_fill_new_level->registerRefine(d_Ez_id,d_Ez_id,d_Ez_id,refine_operator_bound);
	d_fill_new_level->registerRefine(d_Bx_id,d_Bx_id,d_Bx_id,refine_operator_bound);
	d_fill_new_level->registerRefine(d_By_id,d_By_id,d_By_id,refine_operator_bound);
	d_fill_new_level->registerRefine(d_Bz_id,d_Bz_id,d_Bz_id,refine_operator_bound);
	d_fill_new_level->registerRefine(d_q_id,d_q_id,d_q_id,refine_operator_bound);
	d_fill_new_level->registerRefine(d_Jx_id,d_Jx_id,d_Jx_id,refine_operator_bound);
	d_fill_new_level->registerRefine(d_Jy_id,d_Jy_id,d_Jy_id,refine_operator_bound);
	d_fill_new_level->registerRefine(d_Jz_id,d_Jz_id,d_Jz_id,refine_operator_bound);


	//Register variables to the coarsenAlgorithm
	d_coarsen_algorithm->registerCoarsen(d_Ex_id,d_Ex_id,coarsen_operator);
	d_coarsen_algorithm->registerCoarsen(d_Ey_id,d_Ey_id,coarsen_operator);
	d_coarsen_algorithm->registerCoarsen(d_Ez_id,d_Ez_id,coarsen_operator);
	d_coarsen_algorithm->registerCoarsen(d_Bx_id,d_Bx_id,coarsen_operator);
	d_coarsen_algorithm->registerCoarsen(d_By_id,d_By_id,coarsen_operator);
	d_coarsen_algorithm->registerCoarsen(d_Bz_id,d_Bz_id,coarsen_operator);
	d_coarsen_algorithm->registerCoarsen(d_q_id,d_q_id,coarsen_operator);
	d_coarsen_algorithm->registerCoarsen(d_Jx_id,d_Jx_id,coarsen_operator);
	d_coarsen_algorithm->registerCoarsen(d_Jy_id,d_Jy_id,coarsen_operator);
	d_coarsen_algorithm->registerCoarsen(d_Jz_id,d_Jz_id,coarsen_operator);


}

/*
 * Destructor.
 */
Problem::~Problem() 
{
} 

/*
 * Initialize the data from a given level.
 */
void Problem::initializeLevelData (
   const std::shared_ptr<hier::PatchHierarchy >& hierarchy , 
   const int level_number ,
   const double init_data_time ,
   const bool can_be_refined ,
   const bool initial_time ,
   const std::shared_ptr<hier::PatchLevel >& old_level ,
   const bool allocate_data )
{
	std::shared_ptr< hier::PatchLevel > level(hierarchy->getPatchLevel(level_number));

	// Allocate storage needed to initialize level and fill data from coarser levels in AMR hierarchy.  
	level->allocatePatchData(d_interior_id, init_data_time);
	level->allocatePatchData(d_nonSync_id, init_data_time);
	level->allocatePatchData(d_interior_i_id, init_data_time);
	level->allocatePatchData(d_interior_j_id, init_data_time);
	level->allocatePatchData(d_interior_k_id, init_data_time);
	level->allocatePatchData(d_Ex_id, init_data_time);
	level->allocatePatchData(d_Ex_p_id, init_data_time);
	level->allocatePatchData(d_Ey_id, init_data_time);
	level->allocatePatchData(d_Ey_p_id, init_data_time);
	level->allocatePatchData(d_Ez_id, init_data_time);
	level->allocatePatchData(d_Ez_p_id, init_data_time);
	level->allocatePatchData(d_Bx_id, init_data_time);
	level->allocatePatchData(d_Bx_p_id, init_data_time);
	level->allocatePatchData(d_By_id, init_data_time);
	level->allocatePatchData(d_By_p_id, init_data_time);
	level->allocatePatchData(d_Bz_id, init_data_time);
	level->allocatePatchData(d_Bz_p_id, init_data_time);
	level->allocatePatchData(d_q_id, init_data_time);
	level->allocatePatchData(d_Jx_id, init_data_time);
	level->allocatePatchData(d_Jx_p_id, init_data_time);
	level->allocatePatchData(d_Jy_id, init_data_time);
	level->allocatePatchData(d_Jy_p_id, init_data_time);
	level->allocatePatchData(d_Jz_id, init_data_time);
	level->allocatePatchData(d_Jz_p_id, init_data_time);
	level->allocatePatchData(d_constraint1_4_id, init_data_time);
	level->allocatePatchData(d_constraint2_4_id, init_data_time);
	level->allocatePatchData(d_constraint1_5_id, init_data_time);
	level->allocatePatchData(d_constraint2_5_id, init_data_time);
	level->allocatePatchData(d_FluxSBjEx_7_id, init_data_time);
	level->allocatePatchData(d_FluxSBkEx_7_id, init_data_time);
	level->allocatePatchData(d_FluxSBiEy_7_id, init_data_time);
	level->allocatePatchData(d_FluxSBkEy_7_id, init_data_time);
	level->allocatePatchData(d_FluxSBiEz_7_id, init_data_time);
	level->allocatePatchData(d_FluxSBjEz_7_id, init_data_time);
	level->allocatePatchData(d_FluxSBjBx_7_id, init_data_time);
	level->allocatePatchData(d_FluxSBkBx_7_id, init_data_time);
	level->allocatePatchData(d_FluxSBiBy_7_id, init_data_time);
	level->allocatePatchData(d_FluxSBkBy_7_id, init_data_time);
	level->allocatePatchData(d_FluxSBiBz_7_id, init_data_time);
	level->allocatePatchData(d_FluxSBjBz_7_id, init_data_time);
	level->allocatePatchData(d_SpeedSBjEx_7_id, init_data_time);
	level->allocatePatchData(d_SpeedSBkEx_7_id, init_data_time);
	level->allocatePatchData(d_SpeedSBiEy_7_id, init_data_time);
	level->allocatePatchData(d_SpeedSBkEy_7_id, init_data_time);
	level->allocatePatchData(d_SpeedSBiEz_7_id, init_data_time);
	level->allocatePatchData(d_SpeedSBjEz_7_id, init_data_time);
	level->allocatePatchData(d_SpeedSBjBx_7_id, init_data_time);
	level->allocatePatchData(d_SpeedSBkBx_7_id, init_data_time);
	level->allocatePatchData(d_SpeedSBiBy_7_id, init_data_time);
	level->allocatePatchData(d_SpeedSBkBy_7_id, init_data_time);
	level->allocatePatchData(d_SpeedSBiBz_7_id, init_data_time);
	level->allocatePatchData(d_SpeedSBjBz_7_id, init_data_time);
	level->allocatePatchData(d_K1SBEx_7_id, init_data_time);
	level->allocatePatchData(d_K1SBEy_7_id, init_data_time);
	level->allocatePatchData(d_K1SBEz_7_id, init_data_time);
	level->allocatePatchData(d_K1SBBx_7_id, init_data_time);
	level->allocatePatchData(d_K1SBBy_7_id, init_data_time);
	level->allocatePatchData(d_K1SBBz_7_id, init_data_time);
	level->allocatePatchData(d_ExSPprime_7_id, init_data_time);
	level->allocatePatchData(d_EySPprime_7_id, init_data_time);
	level->allocatePatchData(d_EzSPprime_7_id, init_data_time);
	level->allocatePatchData(d_BxSPprime_7_id, init_data_time);
	level->allocatePatchData(d_BySPprime_7_id, init_data_time);
	level->allocatePatchData(d_BzSPprime_7_id, init_data_time);
	level->allocatePatchData(d_qSPprime_7_id, init_data_time);
	level->allocatePatchData(d_JxSPprime_7_id, init_data_time);
	level->allocatePatchData(d_JySPprime_7_id, init_data_time);
	level->allocatePatchData(d_JzSPprime_7_id, init_data_time);
	level->allocatePatchData(d_K2SBEx_7_id, init_data_time);
	level->allocatePatchData(d_K2SBEy_7_id, init_data_time);
	level->allocatePatchData(d_K2SBEz_7_id, init_data_time);
	level->allocatePatchData(d_K2SBBx_7_id, init_data_time);
	level->allocatePatchData(d_K2SBBy_7_id, init_data_time);
	level->allocatePatchData(d_K2SBBz_7_id, init_data_time);
	level->allocatePatchData(d_ExSPprimeprime_7_id, init_data_time);
	level->allocatePatchData(d_EySPprimeprime_7_id, init_data_time);
	level->allocatePatchData(d_EzSPprimeprime_7_id, init_data_time);
	level->allocatePatchData(d_BxSPprimeprime_7_id, init_data_time);
	level->allocatePatchData(d_BySPprimeprime_7_id, init_data_time);
	level->allocatePatchData(d_BzSPprimeprime_7_id, init_data_time);
	level->allocatePatchData(d_K3SBEx_7_id, init_data_time);
	level->allocatePatchData(d_K3SBEy_7_id, init_data_time);
	level->allocatePatchData(d_K3SBBy_7_id, init_data_time);
	level->allocatePatchData(d_ExSPprime_id, init_data_time);
	level->allocatePatchData(d_EySPprime_id, init_data_time);
	level->allocatePatchData(d_EzSPprime_id, init_data_time);
	level->allocatePatchData(d_BxSPprime_id, init_data_time);
	level->allocatePatchData(d_BySPprime_id, init_data_time);
	level->allocatePatchData(d_BzSPprime_id, init_data_time);
	level->allocatePatchData(d_FluxSBjEx_1_id, init_data_time);
	level->allocatePatchData(d_FluxSBkEx_1_id, init_data_time);
	level->allocatePatchData(d_FluxSBiEy_1_id, init_data_time);
	level->allocatePatchData(d_FluxSBkEy_1_id, init_data_time);
	level->allocatePatchData(d_FluxSBiEz_1_id, init_data_time);
	level->allocatePatchData(d_FluxSBjEz_1_id, init_data_time);
	level->allocatePatchData(d_FluxSBjBx_1_id, init_data_time);
	level->allocatePatchData(d_FluxSBkBx_1_id, init_data_time);
	level->allocatePatchData(d_FluxSBiBy_1_id, init_data_time);
	level->allocatePatchData(d_FluxSBkBy_1_id, init_data_time);
	level->allocatePatchData(d_FluxSBiBz_1_id, init_data_time);
	level->allocatePatchData(d_FluxSBjBz_1_id, init_data_time);
	level->allocatePatchData(d_SpeedSBjEx_1_id, init_data_time);
	level->allocatePatchData(d_SpeedSBkEx_1_id, init_data_time);
	level->allocatePatchData(d_SpeedSBiEy_1_id, init_data_time);
	level->allocatePatchData(d_SpeedSBkEy_1_id, init_data_time);
	level->allocatePatchData(d_SpeedSBiEz_1_id, init_data_time);
	level->allocatePatchData(d_SpeedSBjEz_1_id, init_data_time);
	level->allocatePatchData(d_SpeedSBjBx_1_id, init_data_time);
	level->allocatePatchData(d_SpeedSBkBx_1_id, init_data_time);
	level->allocatePatchData(d_SpeedSBiBy_1_id, init_data_time);
	level->allocatePatchData(d_SpeedSBkBy_1_id, init_data_time);
	level->allocatePatchData(d_SpeedSBiBz_1_id, init_data_time);
	level->allocatePatchData(d_SpeedSBjBz_1_id, init_data_time);
	level->allocatePatchData(d_K1SBEx_1_id, init_data_time);
	level->allocatePatchData(d_K1SBEy_1_id, init_data_time);
	level->allocatePatchData(d_K1SBEz_1_id, init_data_time);
	level->allocatePatchData(d_K1SBBx_1_id, init_data_time);
	level->allocatePatchData(d_K1SBBy_1_id, init_data_time);
	level->allocatePatchData(d_K1SBBz_1_id, init_data_time);
	level->allocatePatchData(d_ExSPprime_1_id, init_data_time);
	level->allocatePatchData(d_EySPprime_1_id, init_data_time);
	level->allocatePatchData(d_EzSPprime_1_id, init_data_time);
	level->allocatePatchData(d_BxSPprime_1_id, init_data_time);
	level->allocatePatchData(d_BySPprime_1_id, init_data_time);
	level->allocatePatchData(d_BzSPprime_1_id, init_data_time);
	level->allocatePatchData(d_ExSPprime_2_id, init_data_time);
	level->allocatePatchData(d_EySPprime_2_id, init_data_time);
	level->allocatePatchData(d_EzSPprime_2_id, init_data_time);
	level->allocatePatchData(d_BxSPprime_2_id, init_data_time);
	level->allocatePatchData(d_BySPprime_2_id, init_data_time);
	level->allocatePatchData(d_BzSPprime_2_id, init_data_time);
	level->allocatePatchData(d_ExSPprimeprime_2_id, init_data_time);
	level->allocatePatchData(d_EySPprimeprime_2_id, init_data_time);
	level->allocatePatchData(d_EzSPprimeprime_2_id, init_data_time);
	level->allocatePatchData(d_BxSPprimeprime_2_id, init_data_time);
	level->allocatePatchData(d_BySPprimeprime_2_id, init_data_time);
	level->allocatePatchData(d_BzSPprimeprime_2_id, init_data_time);
	level->allocatePatchData(d_FluxSBjEx_4_id, init_data_time);
	level->allocatePatchData(d_FluxSBkEx_4_id, init_data_time);
	level->allocatePatchData(d_FluxSBiEy_4_id, init_data_time);
	level->allocatePatchData(d_FluxSBkEy_4_id, init_data_time);
	level->allocatePatchData(d_FluxSBiEz_4_id, init_data_time);
	level->allocatePatchData(d_FluxSBjEz_4_id, init_data_time);
	level->allocatePatchData(d_FluxSBjBx_4_id, init_data_time);
	level->allocatePatchData(d_FluxSBkBx_4_id, init_data_time);
	level->allocatePatchData(d_FluxSBiBy_4_id, init_data_time);
	level->allocatePatchData(d_FluxSBkBy_4_id, init_data_time);
	level->allocatePatchData(d_FluxSBiBz_4_id, init_data_time);
	level->allocatePatchData(d_FluxSBjBz_4_id, init_data_time);
	level->allocatePatchData(d_SpeedSBjEx_4_id, init_data_time);
	level->allocatePatchData(d_SpeedSBkEx_4_id, init_data_time);
	level->allocatePatchData(d_SpeedSBiEy_4_id, init_data_time);
	level->allocatePatchData(d_SpeedSBkEy_4_id, init_data_time);
	level->allocatePatchData(d_SpeedSBiEz_4_id, init_data_time);
	level->allocatePatchData(d_SpeedSBjEz_4_id, init_data_time);
	level->allocatePatchData(d_SpeedSBjBx_4_id, init_data_time);
	level->allocatePatchData(d_SpeedSBkBx_4_id, init_data_time);
	level->allocatePatchData(d_SpeedSBiBy_4_id, init_data_time);
	level->allocatePatchData(d_SpeedSBkBy_4_id, init_data_time);
	level->allocatePatchData(d_SpeedSBiBz_4_id, init_data_time);
	level->allocatePatchData(d_SpeedSBjBz_4_id, init_data_time);
	level->allocatePatchData(d_K1SBEx_4_id, init_data_time);
	level->allocatePatchData(d_K1SBEy_4_id, init_data_time);
	level->allocatePatchData(d_K1SBEz_4_id, init_data_time);
	level->allocatePatchData(d_K1SBBx_4_id, init_data_time);
	level->allocatePatchData(d_K1SBBy_4_id, init_data_time);
	level->allocatePatchData(d_K1SBBz_4_id, init_data_time);
	level->allocatePatchData(d_ExSPprime_4_id, init_data_time);
	level->allocatePatchData(d_EySPprime_4_id, init_data_time);
	level->allocatePatchData(d_EzSPprime_4_id, init_data_time);
	level->allocatePatchData(d_BxSPprime_4_id, init_data_time);
	level->allocatePatchData(d_BySPprime_4_id, init_data_time);
	level->allocatePatchData(d_BzSPprime_4_id, init_data_time);
	level->allocatePatchData(d_K2SBEx_4_id, init_data_time);
	level->allocatePatchData(d_K2SBEy_4_id, init_data_time);
	level->allocatePatchData(d_K2SBEz_4_id, init_data_time);
	level->allocatePatchData(d_K2SBBx_4_id, init_data_time);
	level->allocatePatchData(d_K2SBBy_4_id, init_data_time);
	level->allocatePatchData(d_K2SBBz_4_id, init_data_time);
	level->allocatePatchData(d_ExSPprimeprime_4_id, init_data_time);
	level->allocatePatchData(d_EySPprimeprime_4_id, init_data_time);
	level->allocatePatchData(d_EzSPprimeprime_4_id, init_data_time);
	level->allocatePatchData(d_BxSPprimeprime_4_id, init_data_time);
	level->allocatePatchData(d_BySPprimeprime_4_id, init_data_time);
	level->allocatePatchData(d_BzSPprimeprime_4_id, init_data_time);
	level->allocatePatchData(d_K3SBEx_4_id, init_data_time);
	level->allocatePatchData(d_K3SBEy_4_id, init_data_time);
	level->allocatePatchData(d_K3SBEz_4_id, init_data_time);
	level->allocatePatchData(d_K3SBBx_4_id, init_data_time);
	level->allocatePatchData(d_K3SBBy_4_id, init_data_time);
	level->allocatePatchData(d_K3SBBz_4_id, init_data_time);
	level->allocatePatchData(d_FluxSBjEx_5_id, init_data_time);
	level->allocatePatchData(d_FluxSBkEx_5_id, init_data_time);
	level->allocatePatchData(d_FluxSBiEy_5_id, init_data_time);
	level->allocatePatchData(d_FluxSBkEy_5_id, init_data_time);
	level->allocatePatchData(d_FluxSBiEz_5_id, init_data_time);
	level->allocatePatchData(d_FluxSBjEz_5_id, init_data_time);
	level->allocatePatchData(d_FluxSBjBx_5_id, init_data_time);
	level->allocatePatchData(d_FluxSBkBx_5_id, init_data_time);
	level->allocatePatchData(d_FluxSBiBy_5_id, init_data_time);
	level->allocatePatchData(d_FluxSBkBy_5_id, init_data_time);
	level->allocatePatchData(d_FluxSBiBz_5_id, init_data_time);
	level->allocatePatchData(d_FluxSBjBz_5_id, init_data_time);
	level->allocatePatchData(d_SpeedSBjEx_5_id, init_data_time);
	level->allocatePatchData(d_SpeedSBkEx_5_id, init_data_time);
	level->allocatePatchData(d_SpeedSBiEy_5_id, init_data_time);
	level->allocatePatchData(d_SpeedSBkEy_5_id, init_data_time);
	level->allocatePatchData(d_SpeedSBiEz_5_id, init_data_time);
	level->allocatePatchData(d_SpeedSBjEz_5_id, init_data_time);
	level->allocatePatchData(d_SpeedSBjBx_5_id, init_data_time);
	level->allocatePatchData(d_SpeedSBkBx_5_id, init_data_time);
	level->allocatePatchData(d_SpeedSBiBy_5_id, init_data_time);
	level->allocatePatchData(d_SpeedSBkBy_5_id, init_data_time);
	level->allocatePatchData(d_SpeedSBiBz_5_id, init_data_time);
	level->allocatePatchData(d_SpeedSBjBz_5_id, init_data_time);
	level->allocatePatchData(d_K1SBEx_5_id, init_data_time);
	level->allocatePatchData(d_K1SBEy_5_id, init_data_time);
	level->allocatePatchData(d_K1SBEz_5_id, init_data_time);
	level->allocatePatchData(d_K1SBBx_5_id, init_data_time);
	level->allocatePatchData(d_K1SBBy_5_id, init_data_time);
	level->allocatePatchData(d_K1SBBz_5_id, init_data_time);
	level->allocatePatchData(d_ExSPprime_5_id, init_data_time);
	level->allocatePatchData(d_EySPprime_5_id, init_data_time);
	level->allocatePatchData(d_EzSPprime_5_id, init_data_time);
	level->allocatePatchData(d_BxSPprime_5_id, init_data_time);
	level->allocatePatchData(d_BySPprime_5_id, init_data_time);
	level->allocatePatchData(d_BzSPprime_5_id, init_data_time);
	level->allocatePatchData(d_K2SBEx_5_id, init_data_time);
	level->allocatePatchData(d_K2SBEy_5_id, init_data_time);
	level->allocatePatchData(d_K2SBEz_5_id, init_data_time);
	level->allocatePatchData(d_K2SBBx_5_id, init_data_time);
	level->allocatePatchData(d_K2SBBy_5_id, init_data_time);
	level->allocatePatchData(d_K2SBBz_5_id, init_data_time);
	level->allocatePatchData(d_ExSPprimeprime_5_id, init_data_time);
	level->allocatePatchData(d_EySPprimeprime_5_id, init_data_time);
	level->allocatePatchData(d_EzSPprimeprime_5_id, init_data_time);
	level->allocatePatchData(d_BxSPprimeprime_5_id, init_data_time);
	level->allocatePatchData(d_BySPprimeprime_5_id, init_data_time);
	level->allocatePatchData(d_BzSPprimeprime_5_id, init_data_time);
	level->allocatePatchData(d_K3SBEx_5_id, init_data_time);
	level->allocatePatchData(d_K3SBEy_5_id, init_data_time);
	level->allocatePatchData(d_K3SBEz_5_id, init_data_time);
	level->allocatePatchData(d_K3SBBx_5_id, init_data_time);
	level->allocatePatchData(d_K3SBBy_5_id, init_data_time);
	level->allocatePatchData(d_K3SBBz_5_id, init_data_time);
	level->allocatePatchData(d_d_i_Ey_id, init_data_time);
	level->allocatePatchData(d_d_j_Ey_id, init_data_time);
	level->allocatePatchData(d_d_k_Ey_id, init_data_time);
	level->allocatePatchData(d_d_i_Ex_id, init_data_time);
	level->allocatePatchData(d_d_j_Ex_id, init_data_time);
	level->allocatePatchData(d_d_k_Ex_id, init_data_time);
	level->allocatePatchData(d_stalled_1_id, init_data_time);
	level->allocatePatchData(d_stalled_2_id, init_data_time);
	level->allocatePatchData(d_stalled_4_id, init_data_time);
	level->allocatePatchData(d_stalled_5_id, init_data_time);
	level->allocatePatchData(d_stalled_7_id, init_data_time);
	level->allocatePatchData(d_stalled_8_id, init_data_time);
	level->allocatePatchData(d_FOV_1_id, init_data_time);
	level->allocatePatchData(d_FOV_2_id, init_data_time);
	level->allocatePatchData(d_FOV_4_id, init_data_time);
	level->allocatePatchData(d_FOV_5_id, init_data_time);
	level->allocatePatchData(d_FOV_7_id, init_data_time);
	level->allocatePatchData(d_FOV_8_id, init_data_time);
	level->allocatePatchData(d_FOV_xLower_id, init_data_time);
	level->allocatePatchData(d_FOV_xUpper_id, init_data_time);
	level->allocatePatchData(d_FOV_yLower_id, init_data_time);
	level->allocatePatchData(d_FOV_yUpper_id, init_data_time);
	level->allocatePatchData(d_FOV_zLower_id, init_data_time);
	level->allocatePatchData(d_FOV_zUpper_id, init_data_time);
	level->allocatePatchData(d_mask_id, init_data_time);


	//Fill a finer level with the data of the next coarse level.
	if (!initial_time && ((level_number > 0) || old_level)) {
		d_fill_new_level->createSchedule(level,old_level,level_number-1,hierarchy,this)->fillData(init_data_time, false);
	}

	//Mapping the current data for new level.
	if (initial_time) {
		mapDataOnPatch(init_data_time, initial_time, level_number, level);
	}

	//Fill a finer level with the data of the next coarse level.
	if ((level_number > 0) || old_level) {
		d_mapping_fill->createSchedule(level,old_level,level_number-1,hierarchy,this)->fillData(init_data_time, false);
		correctFOVS(level);
	}

	//Interphase mapping
	if (initial_time) {
		interphaseMapping(init_data_time, initial_time, level_number, level, 1);
	}


	//Initialize current data for new level.
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr< hier::Patch > patch = *p_it;
        		if (initial_time) {
	      		initializeDataOnPatch(*patch, init_data_time, initial_time);
        		}
	}
	//Post-initialization Sync.
    	if (initial_time) {

		d_bdry_fill_init->createSchedule(level,this)->fillData(init_data_time, false);

    	}
}



void Problem::initializeLevelIntegrator(
   const std::shared_ptr<mesh::GriddingAlgorithmStrategy>& gridding_alg)
{
}

double Problem::getLevelDt(
   const std::shared_ptr<hier::PatchLevel>& level,
   const double dt_time,
   const bool initial_time)
{
  
   TBOX_ASSERT(level);

   if (level->getLevelNumber() == 0) return initial_dt;

    double dt = initial_dt;
    const hier::IntVector ratio = level->getRatioToLevelZero();
    double local_dt = dt;
    for (int i = 0; i < 2; i++) {
        if (local_dt > dt / ratio[i]) {
            local_dt = dt / ratio[i];
        }
    }
    return local_dt;
}

double Problem::getMaxFinerLevelDt(
   const int finer_level_number,
   const double coarse_dt,
   const hier::IntVector& ratio)
{
   NULL_USE(finer_level_number);

   TBOX_ASSERT(ratio.min() > 0);

   return coarse_dt / double(ratio.max());
}

void Problem::standardLevelSynchronization(
   const std::shared_ptr<hier::PatchHierarchy>& hierarchy,
   const int coarsest_level,
   const int finest_level,
   const double sync_time,
   const std::vector<double>& old_times)
{

}

void Problem::synchronizeNewLevels(
   const std::shared_ptr<hier::PatchHierarchy>& hierarchy,
   const int coarsest_level,
   const int finest_level,
   const double sync_time,
   const bool initial_time)
{

}

void Problem::resetTimeDependentData(
   const std::shared_ptr<hier::PatchLevel>& level,
   const double new_time,
   const bool can_be_refined)
{
   TBOX_ASSERT(level);
   level->setTime(new_time);
}

void Problem::resetDataToPreadvanceState(
   const std::shared_ptr<hier::PatchLevel>& level)
{
    //cout<<"resetDataToPreadvanceState"<<endl;
}

/*
 * Map data on a patch. This mapping is done only at the begining of the simulation.
 */
void Problem::mapDataOnPatch(const double time, const bool initial_time, const int ln, const std::shared_ptr< hier::PatchLevel >& level)
{
	(void) time;
	const tbox::SAMRAI_MPI& mpi(tbox::SAMRAI_MPI::getSAMRAIWorld());
   	if (initial_time) {

		// Mapping		
		int i, iMapStart, iMapEnd, iterm, previousMapi, iWallAcc;
		bool interiorMapi;
		int j, jMapStart, jMapEnd, jterm, previousMapj, jWallAcc;
		bool interiorMapj;
		int k, kMapStart, kMapEnd, kterm, previousMapk, kWallAcc;
		bool interiorMapk;
		int minBlock[3], maxBlock[3], unionsI, facePointI, ie1, ie2, ie3, proc, pcounter, working, finished, pred;
		double maxDistance, e1, e2, e3;
		bool done, modif, workingArray[mpi.getSize()], finishedArray[mpi.getSize()], workingGlobal, finishedGlobal, workingPatchArray[level->getLocalNumberOfPatches()], finishedPatchArray[level->getLocalNumberOfPatches()], workingPatchGlobal, finishedPatchGlobal;
		int nodes = mpi.getSize();
		int patches = level->getLocalNumberOfPatches();

		double SQRT3INV = 1.0/sqrt(3.0);

		if (ln == 0) {
			//FOV initialization
			for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
				const std::shared_ptr< hier::Patch >& patch = *p_it;

				//Get the dimensions of the patch
				hier::Box pbox = patch->getBox();
				double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
				double* FOV_2 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_2_id).get())->getPointer();
				double* FOV_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_4_id).get())->getPointer();
				double* FOV_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_5_id).get())->getPointer();
				double* FOV_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_7_id).get())->getPointer();
				double* FOV_8 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_8_id).get())->getPointer();
				double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
				double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
				double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
				double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
				double* FOV_zLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zLower_id).get())->getPointer();
				double* FOV_zUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zUpper_id).get())->getPointer();
				const hier::Index boxfirst = patch->getBox().lower();
				const hier::Index boxlast  = patch->getBox().upper();

				//Get delta spaces into an array. dx, dy, dz.
				const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
				const double* dx  = patch_geom->getDx();

				int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
				int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
				int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;

				for (i = 0; i < ilast; i++) {
					for (j = 0; j < jlast; j++) {
						for (k = 0; k < klast; k++) {
							vector(FOV_1, i, j, k) = 0;
							vector(FOV_2, i, j, k) = 0;
							vector(FOV_4, i, j, k) = 0;
							vector(FOV_5, i, j, k) = 0;
							vector(FOV_7, i, j, k) = 0;
							vector(FOV_8, i, j, k) = 0;
							vector(FOV_xLower, i, j, k) = 0;
							vector(FOV_xUpper, i, j, k) = 0;
							vector(FOV_yLower, i, j, k) = 0;
							vector(FOV_yUpper, i, j, k) = 0;
							vector(FOV_zLower, i, j, k) = 0;
							vector(FOV_zUpper, i, j, k) = 0;
						}
					}
				}
			}
			d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);

			//Region: AirI
			for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
				const std::shared_ptr< hier::Patch >& patch = *p_it;

				//Get the dimensions of the patch
				hier::Box pbox = patch->getBox();
				double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
				double* FOV_2 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_2_id).get())->getPointer();
				double* FOV_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_4_id).get())->getPointer();
				double* FOV_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_5_id).get())->getPointer();
				double* FOV_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_7_id).get())->getPointer();
				double* FOV_8 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_8_id).get())->getPointer();
				double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
				double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
				double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
				double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
				double* FOV_zLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zLower_id).get())->getPointer();
				double* FOV_zUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zUpper_id).get())->getPointer();
				double* interior_i = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_i_id).get())->getPointer();
				double* interior_j = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_j_id).get())->getPointer();
				double* interior_k = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_k_id).get())->getPointer();
				double* interior = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_id).get())->getPointer();
				int* nonSync = ((pdat::NodeData<int> *) patch->getPatchData(d_nonSync_id).get())->getPointer();
				const hier::Index boxfirst = patch->getBox().lower();
				const hier::Index boxlast  = patch->getBox().upper();

				//Get delta spaces into an array. dx, dy, dz.
				const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
				const double* dx  = patch_geom->getDx();

				int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
				int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
				int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;

				iMapStart = 0;
				iMapEnd = d_grid_geometry->getPhysicalDomain().front().numberCells()[0];
				jMapStart = 0;
				jMapEnd = d_grid_geometry->getPhysicalDomain().front().numberCells()[1];
				kMapStart = 0;
				kMapEnd = d_grid_geometry->getPhysicalDomain().front().numberCells()[2];
				for (i = iMapStart; i <= iMapEnd; i++) {
					for (j = jMapStart; j <= jMapEnd; j++) {
						for (k = kMapStart; k <= kMapEnd; k++) {
							if (i >= boxfirst(0) - d_ghost_width && i <= boxlast(0) + d_ghost_width && j >= boxfirst(1) - d_ghost_width && j <= boxlast(1) + d_ghost_width && k >= boxfirst(2) - d_ghost_width && k <= boxlast(2) + d_ghost_width) {
								vector(FOV_7, i - boxfirst(0) + d_ghost_width, j - boxfirst(1) + d_ghost_width, k - boxfirst(2) + d_ghost_width) = 100;
								vector(FOV_1, i - boxfirst(0) + d_ghost_width, j - boxfirst(1) + d_ghost_width, k - boxfirst(2) + d_ghost_width) = 0;
								vector(FOV_2, i - boxfirst(0) + d_ghost_width, j - boxfirst(1) + d_ghost_width, k - boxfirst(2) + d_ghost_width) = 0;
								vector(FOV_4, i - boxfirst(0) + d_ghost_width, j - boxfirst(1) + d_ghost_width, k - boxfirst(2) + d_ghost_width) = 0;
								vector(FOV_5, i - boxfirst(0) + d_ghost_width, j - boxfirst(1) + d_ghost_width, k - boxfirst(2) + d_ghost_width) = 0;
								vector(FOV_8, i - boxfirst(0) + d_ghost_width, j - boxfirst(1) + d_ghost_width, k - boxfirst(2) + d_ghost_width) = 0;
								vector(FOV_xLower, i - boxfirst(0) + d_ghost_width, j - boxfirst(1) + d_ghost_width, k - boxfirst(2) + d_ghost_width) = 0;
								vector(FOV_xUpper, i - boxfirst(0) + d_ghost_width, j - boxfirst(1) + d_ghost_width, k - boxfirst(2) + d_ghost_width) = 0;
								vector(FOV_yLower, i - boxfirst(0) + d_ghost_width, j - boxfirst(1) + d_ghost_width, k - boxfirst(2) + d_ghost_width) = 0;
								vector(FOV_yUpper, i - boxfirst(0) + d_ghost_width, j - boxfirst(1) + d_ghost_width, k - boxfirst(2) + d_ghost_width) = 0;
								vector(FOV_zLower, i - boxfirst(0) + d_ghost_width, j - boxfirst(1) + d_ghost_width, k - boxfirst(2) + d_ghost_width) = 0;
								vector(FOV_zUpper, i - boxfirst(0) + d_ghost_width, j - boxfirst(1) + d_ghost_width, k - boxfirst(2) + d_ghost_width) = 0;
							}
						}
					}
				}
				//Check stencil
				for (k = 0; k < klast; k++) {
					for (j = 0; j < jlast; j++) {
						for (i = 0; i < ilast; i++) {
							vector(interior_i, i, j, k) = 0;
							vector(interior_j, i, j, k) = 0;
							vector(interior_k, i, j, k) = 0;
						}
					}
				}
				for (k = 0; k < klast; k++) {
					for (j = 0; j < jlast; j++) {
						for (i = 0; i < ilast; i++) {
							if (vector(FOV_7, i, j, k) > 0) {
								setStencilLimits(patch, i, j, k, d_FOV_7_id);
							}
						}
					}
				}
			}
			d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);
			for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
				const std::shared_ptr< hier::Patch >& patch = *p_it;

				//Get the dimensions of the patch
				hier::Box pbox = patch->getBox();
				double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
				double* FOV_2 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_2_id).get())->getPointer();
				double* FOV_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_4_id).get())->getPointer();
				double* FOV_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_5_id).get())->getPointer();
				double* FOV_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_7_id).get())->getPointer();
				double* FOV_8 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_8_id).get())->getPointer();
				double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
				double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
				double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
				double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
				double* FOV_zLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zLower_id).get())->getPointer();
				double* FOV_zUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zUpper_id).get())->getPointer();
				double* interior_i = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_i_id).get())->getPointer();
				double* interior_j = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_j_id).get())->getPointer();
				double* interior_k = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_k_id).get())->getPointer();
				double* interior = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_id).get())->getPointer();
				int* nonSync = ((pdat::NodeData<int> *) patch->getPatchData(d_nonSync_id).get())->getPointer();
				const hier::Index boxfirst = patch->getBox().lower();
				const hier::Index boxlast  = patch->getBox().upper();

				//Get delta spaces into an array. dx, dy, dz.
				const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
				const double* dx  = patch_geom->getDx();

				int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
				int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
				int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;

				for (k = 0; k < klast; k++) {
					for (j = 0; j < jlast; j++) {
						for (i = 0; i < ilast; i++) {
							if ((abs(vector(interior_i, i, j, k)) > 1 || abs(vector(interior_j, i, j, k)) > 1 || abs(vector(interior_k, i, j, k)) > 1) && (i < d_ghost_width || i >= ilast - d_ghost_width || j < d_ghost_width || j >= jlast - d_ghost_width || k < d_ghost_width || k >= klast - d_ghost_width)) {
								checkStencil(patch, i, j, k, d_FOV_7_id);
							}
						}
					}
				}
				for (k = 0; k < klast; k++) {
					for (j = 0; j < jlast; j++) {
						for (i = 0; i < ilast; i++) {
							if (vector(interior_i, i, j, k) != 0 || vector(interior_j, i, j, k) != 0 || vector(interior_k, i, j, k) != 0) {
								vector(FOV_7, i, j, k) = 100;
								vector(FOV_1, i, j, k) = 0;
								vector(FOV_2, i, j, k) = 0;
								vector(FOV_4, i, j, k) = 0;
								vector(FOV_5, i, j, k) = 0;
								vector(FOV_8, i, j, k) = 0;
								vector(FOV_xLower, i, j, k) = 0;
								vector(FOV_xUpper, i, j, k) = 0;
								vector(FOV_yLower, i, j, k) = 0;
								vector(FOV_yUpper, i, j, k) = 0;
								vector(FOV_zLower, i, j, k) = 0;
								vector(FOV_zUpper, i, j, k) = 0;
							}
						}
					}
				}
			}
			d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);

			//Region: OtherI
			for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
				const std::shared_ptr< hier::Patch >& patch = *p_it;

				//Get the dimensions of the patch
				hier::Box pbox = patch->getBox();
				double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
				double* FOV_2 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_2_id).get())->getPointer();
				double* FOV_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_4_id).get())->getPointer();
				double* FOV_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_5_id).get())->getPointer();
				double* FOV_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_7_id).get())->getPointer();
				double* FOV_8 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_8_id).get())->getPointer();
				double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
				double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
				double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
				double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
				double* FOV_zLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zLower_id).get())->getPointer();
				double* FOV_zUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zUpper_id).get())->getPointer();
				double* interior_i = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_i_id).get())->getPointer();
				double* interior_j = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_j_id).get())->getPointer();
				double* interior_k = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_k_id).get())->getPointer();
				double* interior = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_id).get())->getPointer();
				int* nonSync = ((pdat::NodeData<int> *) patch->getPatchData(d_nonSync_id).get())->getPointer();
				const hier::Index boxfirst = patch->getBox().lower();
				const hier::Index boxlast  = patch->getBox().upper();

				//Get delta spaces into an array. dx, dy, dz.
				const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
				const double* dx  = patch_geom->getDx();

				int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
				int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
				int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;

				//Fill the faces
				for (unionsI = 0; unionsI < UNIONS_Other; unionsI++) {
					for (facePointI = 0; facePointI < DIMENSIONS; facePointI++) {
						iterm = round((OtherPoints[OtherUnions[unionsI][facePointI]][0] - d_grid_geometry->getXLower()[0]) / dx[0]);
						jterm = round((OtherPoints[OtherUnions[unionsI][facePointI]][1] - d_grid_geometry->getXLower()[1]) / dx[1]);
						kterm = round((OtherPoints[OtherUnions[unionsI][facePointI]][2] - d_grid_geometry->getXLower()[2]) / dx[2]);
						//Take maximum and minimum coordinates of the face
						if (facePointI == 0) {
							minBlock[0] = iterm;
							maxBlock[0] = iterm;
							minBlock[1] = jterm;
							maxBlock[1] = jterm;
							minBlock[2] = kterm;
							maxBlock[2] = kterm;
						} else {
							if (iterm < minBlock[0]) {
								minBlock[0] = iterm;
							}
							if (iterm > maxBlock[0]) {
								maxBlock[0] = iterm;
							}
							if (jterm < minBlock[1]) {
								minBlock[1] = jterm;
							}
							if (jterm > maxBlock[1]) {
								maxBlock[1] = jterm;
							}
							if (kterm < minBlock[2]) {
								minBlock[2] = kterm;
							}
							if (kterm > maxBlock[2]) {
								maxBlock[2] = kterm;
							}
						}
					}
					if (minBlock[0] <= boxlast(0) + 1 + d_ghost_width && maxBlock[0] >= boxfirst(0) - d_ghost_width && minBlock[1] <= boxlast(1) + 1 + d_ghost_width && maxBlock[1] >= boxfirst(1) - d_ghost_width && minBlock[2] <= boxlast(2) + 1 + d_ghost_width && maxBlock[2] >= boxfirst(2) - d_ghost_width) {
						maxDistance = MAX(abs(minBlock[2] - maxBlock[2]),MAX(abs(minBlock[1] - maxBlock[1]),abs(minBlock[0] - maxBlock[0])));
						for (ie3 = 0; ie3 < 3 * maxDistance; ie3++) {
							e3 = ie3 / (3 * maxDistance);
							for (ie2 = 0; ie2 < 3 * maxDistance; ie2++) {
								e2 = ie2 / (3 * maxDistance);
								for (ie1 = 0; ie1 < 3 * maxDistance; ie1++) {
									e1 = ie1 / (3 * maxDistance);
									if (abs(ie1 + ie2 + ie3 - 3 * maxDistance) <= SQRT3INV) {
										iterm = round(((OtherPoints[OtherUnions[unionsI][0]][0] - d_grid_geometry->getXLower()[0]) / dx[0]) * e1 + ((OtherPoints[OtherUnions[unionsI][1]][0] - d_grid_geometry->getXLower()[0]) / dx[0]) * e2 + ((OtherPoints[OtherUnions[unionsI][2]][0] - d_grid_geometry->getXLower()[0]) / dx[0]) * e3);
										jterm = round(((OtherPoints[OtherUnions[unionsI][0]][1] - d_grid_geometry->getXLower()[1]) / dx[1]) * e1 + ((OtherPoints[OtherUnions[unionsI][1]][1] - d_grid_geometry->getXLower()[1]) / dx[1]) * e2 + ((OtherPoints[OtherUnions[unionsI][2]][1] - d_grid_geometry->getXLower()[1]) / dx[1]) * e3);
										kterm = round(((OtherPoints[OtherUnions[unionsI][0]][2] - d_grid_geometry->getXLower()[2]) / dx[2]) * e1 + ((OtherPoints[OtherUnions[unionsI][1]][2] - d_grid_geometry->getXLower()[2]) / dx[2]) * e2 + ((OtherPoints[OtherUnions[unionsI][2]][2] - d_grid_geometry->getXLower()[2]) / dx[2]) * e3);
										if (iterm >= boxfirst(0) - d_ghost_width && iterm <= boxlast(0) + d_ghost_width && jterm >= boxfirst(1) - d_ghost_width && jterm <= boxlast(1) + d_ghost_width && kterm >= boxfirst(2) - d_ghost_width && kterm <= boxlast(2) + d_ghost_width) {
											int indexi = iterm - boxfirst(0) + d_ghost_width;
											int indexj = jterm - boxfirst(1) + d_ghost_width;
											int indexk = kterm - boxfirst(2) + d_ghost_width;
											if (patch->getPatchGeometry()->getTouchesRegularBoundary (0, 1) && i - boxfirst(0) + 1 + d_ghost_width == ilast - d_ghost_width) {
												indexi--;
											}
											if (patch->getPatchGeometry()->getTouchesRegularBoundary (1, 1) && j - boxfirst(1) + 1 + d_ghost_width == jlast - d_ghost_width) {
												indexj--;
											}
											if (patch->getPatchGeometry()->getTouchesRegularBoundary (2, 1) && k - boxfirst(2) + 1 + d_ghost_width == klast - d_ghost_width) {
												indexk--;
											}
											vector(FOV_5, indexi, indexj, indexk) = 100;
											vector(FOV_1, indexi, indexj, indexk) = 0;
											vector(FOV_2, indexi, indexj, indexk) = 0;
											vector(FOV_4, indexi, indexj, indexk) = 0;
											vector(FOV_7, indexi, indexj, indexk) = 0;
											vector(FOV_8, indexi, indexj, indexk) = 0;
											vector(FOV_xLower, indexi, indexj, indexk) = 0;
											vector(FOV_xUpper, indexi, indexj, indexk) = 0;
											vector(FOV_yLower, indexi, indexj, indexk) = 0;
											vector(FOV_yUpper, indexi, indexj, indexk) = 0;
											vector(FOV_zLower, indexi, indexj, indexk) = 0;
											vector(FOV_zUpper, indexi, indexj, indexk) = 0;
										}
									}
								}
							}
						}
					}
				}
			}
			d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);

			//Region: OtherI
			//Fill the interior of the region
			//Initialize the variables and set the walls as exterior
			for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
				const std::shared_ptr< hier::Patch >& patch = *p_it;

				//Get the dimensions of the patch
				hier::Box pbox = patch->getBox();
				double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
				double* FOV_2 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_2_id).get())->getPointer();
				double* FOV_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_4_id).get())->getPointer();
				double* FOV_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_5_id).get())->getPointer();
				double* FOV_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_7_id).get())->getPointer();
				double* FOV_8 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_8_id).get())->getPointer();
				double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
				double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
				double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
				double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
				double* FOV_zLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zLower_id).get())->getPointer();
				double* FOV_zUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zUpper_id).get())->getPointer();
				double* interior_i = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_i_id).get())->getPointer();
				double* interior_j = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_j_id).get())->getPointer();
				double* interior_k = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_k_id).get())->getPointer();
				double* interior = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_id).get())->getPointer();
				int* nonSync = ((pdat::NodeData<int> *) patch->getPatchData(d_nonSync_id).get())->getPointer();
				const hier::Index boxfirst = patch->getBox().lower();
				const hier::Index boxlast  = patch->getBox().upper();

				//Get delta spaces into an array. dx, dy, dz.
				const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
				const double* dx  = patch_geom->getDx();

				int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
				int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
				int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;

				for (k = 0; k < klast; k++) {
					for (j = 0; j < jlast; j++) {
						for (i = 0; i < ilast; i++) {
							//If wall, then it is considered exterior
							if (vector(FOV_5, i, j, k) > 0) {
								vector(interior, i, j, k) = 1;
								vector(nonSync, i, j, k) = 1;
							} else {
								vector(interior, i, j, k) = 0;
								vector(nonSync, i, j, k) = 0;
							}
						}
					}
				}
			}
			//Initial FloodFill call
			finishedGlobal = false;

			finishedPatchGlobal = false;

			pred = 1;
			//First floodfill
			pcounter = 0;
			for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
				const std::shared_ptr< hier::Patch >& patch = *p_it;

				//Get the dimensions of the patch
				hier::Box pbox = patch->getBox();
				double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
				double* FOV_2 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_2_id).get())->getPointer();
				double* FOV_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_4_id).get())->getPointer();
				double* FOV_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_5_id).get())->getPointer();
				double* FOV_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_7_id).get())->getPointer();
				double* FOV_8 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_8_id).get())->getPointer();
				double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
				double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
				double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
				double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
				double* FOV_zLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zLower_id).get())->getPointer();
				double* FOV_zUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zUpper_id).get())->getPointer();
				const hier::Index boxfirst = patch->getBox().lower();
				const hier::Index boxlast  = patch->getBox().upper();

				//Get delta spaces into an array. dx, dy, dz.
				const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
				const double* dx  = patch_geom->getDx();

				if (boxfirst(0) == 0 && boxfirst(1) == 0 && boxfirst(2) == 0) {
					floodfill(patch, 0, 0, 0, 1, 5);
					workingPatchArray[pcounter] = 1;
				}
				pcounter++;
			}
			if (nodes > 1) {
				d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);

				while (!finishedGlobal) {
					workingGlobal = true;
					//Filling started floodfill in all the patches
					while (workingGlobal) {
						working = 0;
						pcounter = 0;
						for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
							const std::shared_ptr< hier::Patch >& patch = *p_it;

							//Get the dimensions of the patch
							hier::Box pbox = patch->getBox();
							double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
							double* FOV_2 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_2_id).get())->getPointer();
							double* FOV_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_4_id).get())->getPointer();
							double* FOV_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_5_id).get())->getPointer();
							double* FOV_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_7_id).get())->getPointer();
							double* FOV_8 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_8_id).get())->getPointer();
							double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
							double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
							double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
							double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
							double* FOV_zLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zLower_id).get())->getPointer();
							double* FOV_zUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zUpper_id).get())->getPointer();
							double* interior_i = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_i_id).get())->getPointer();
							double* interior_j = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_j_id).get())->getPointer();
							double* interior_k = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_k_id).get())->getPointer();
							double* interior = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_id).get())->getPointer();
							int* nonSync = ((pdat::NodeData<int> *) patch->getPatchData(d_nonSync_id).get())->getPointer();
							const hier::Index boxfirst = patch->getBox().lower();
							const hier::Index boxlast  = patch->getBox().upper();

							//Get delta spaces into an array. dx, dy, dz.
							const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
							const double* dx  = patch_geom->getDx();

							int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
							int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
							int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;
							workingPatchArray[pcounter] = 0;
							for (k = 0; k < klast && !workingPatchArray[pcounter]; k++) {
								for (j = 0; j < jlast && !workingPatchArray[pcounter]; j++) {
									for (i = 0; i < ilast && !workingPatchArray[pcounter]; i++) {
										if (vector(nonSync, i, j, k) != vector(interior, i, j, k) && vector(nonSync, i, j, k) == 0) {
											floodfill(patch, i, j, k, pred, 5);
											workingPatchArray[pcounter] = 1;
											working = 1;
										} else {
											if (vector(nonSync, i, j, k) != vector(interior, i, j, k)) {
												vector(interior, i, j, k) = vector(nonSync, i, j, k);
												if (vector(nonSync, i, j, k) == 2) {
													vector(FOV_1, i, j, k) = 0;
													vector(FOV_2, i, j, k) = 0;
													vector(FOV_4, i, j, k) = 0;
													vector(FOV_5, i, j, k) = 100;
													vector(FOV_7, i, j, k) = 0;
													vector(FOV_8, i, j, k) = 0;
												}
											}
										}
									}
								}
							}
							pcounter++;
						}
						d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);
						//Local working variable communication
						workingGlobal = false;
						int *s = new int[1];
						s[0] = working;
						mpi.AllReduce(s, 1, MPI_MAX);
						if (s[0] == 1) {
							workingGlobal = true;
						}
					}

					//Looking for a new floodfill
					finished = 1;
					pcounter = 0;
					for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
						const std::shared_ptr< hier::Patch >& patch = *p_it;

						//Get the dimensions of the patch
						hier::Box pbox = patch->getBox();
						double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
						double* FOV_2 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_2_id).get())->getPointer();
						double* FOV_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_4_id).get())->getPointer();
						double* FOV_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_5_id).get())->getPointer();
						double* FOV_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_7_id).get())->getPointer();
						double* FOV_8 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_8_id).get())->getPointer();
						double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
						double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
						double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
						double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
						double* FOV_zLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zLower_id).get())->getPointer();
						double* FOV_zUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zUpper_id).get())->getPointer();
						double* interior_i = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_i_id).get())->getPointer();
						double* interior_j = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_j_id).get())->getPointer();
						double* interior_k = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_k_id).get())->getPointer();
						double* interior = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_id).get())->getPointer();
						int* nonSync = ((pdat::NodeData<int> *) patch->getPatchData(d_nonSync_id).get())->getPointer();
						const hier::Index boxfirst = patch->getBox().lower();
						const hier::Index boxlast  = patch->getBox().upper();

						//Get delta spaces into an array. dx, dy, dz.
						const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
						const double* dx  = patch_geom->getDx();

						int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
						int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
						int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;

						//Limits for the finished checking routine to avoid internal boundary problems
						int il = 0;
						if (!patch->getPatchGeometry()->getTouchesRegularBoundary (0, 0)) {
							il = 4;
						}
						int iu = ilast;
						if (!patch->getPatchGeometry()->getTouchesRegularBoundary (0, 1)) {
							iu = ilast - 8;
						}
						int jl = 0;
						if (!patch->getPatchGeometry()->getTouchesRegularBoundary (1, 0)) {
							jl = 4;
						}
						int ju = jlast;
						if (!patch->getPatchGeometry()->getTouchesRegularBoundary (1, 1)) {
							ju = jlast - 8;
						}
						int kl = 0;
						if (!patch->getPatchGeometry()->getTouchesRegularBoundary (2, 0)) {
							kl = 4;
						}
						int ku = klast;
						if (!patch->getPatchGeometry()->getTouchesRegularBoundary (2, 1)) {
							ku = klast - 8;
						}

						finishedPatchArray[pcounter] = 1;
						for (k = kl; k < ku && finishedPatchArray[pcounter]; k++) {
							for (j = jl; j < ju && finishedPatchArray[pcounter]; j++) {
								for (i = il; i < iu && finishedPatchArray[pcounter]; i++) {
									if (vector(nonSync, i, j, k) == 0) {
										finishedPatchArray[pcounter] = 0;
										finished = 0;
									}
								}
							}
						}
						pcounter++;
					}
					//Finished local variables communication
					for (proc = 0; proc < nodes; proc++) {
						if (proc == mpi.getRank()) {
							for (int dproc = 0; dproc < nodes; dproc++) {
								if (dproc != mpi.getRank()) {
									int *w = new int[1];
									w[0] = finished;
									int size = 1;
									mpi.Send(w, size, MPI_INT, dproc, 0);
								}
							}
							finishedArray[proc] = finished;
						} else {
							int *w = new int[1];
							int size = 1;
							tbox::SAMRAI_MPI::Status status;
							mpi.Recv(w, size, MPI_INT, proc, 0, &status);
							finishedArray[proc] = w[0];
						}
					}
					finishedGlobal = true;
					for (proc = 0; proc < nodes & finishedGlobal; proc++) {
						if (finishedArray[proc] == 0) {
							finishedGlobal = 0;
						}
					}

					//Only one processor could start a floodfill at the same time
					//Take the first who wants to
					int first = -1;
					int firstPatch = -1;
					for (proc = 0; proc < nodes && !finishedGlobal && first < 0; proc++) {
						if (finishedArray[proc] == 0) {
							first = proc;
							for (int pat = 0; pat < patches && !finishedPatchGlobal && firstPatch < 0; pat++) {
								if (finishedPatchArray[pat] == 0) {
									firstPatch = pat;
								}
							}
						}
					}
					if (pred == 1) {
						pred = 2;
					} else {
						pred = 1;
					}
					if (first == mpi.getRank() && !finishedGlobal) {
						pcounter = 0;
						for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
							const std::shared_ptr< hier::Patch >& patch = *p_it;

							//Get the dimensions of the patch
							hier::Box pbox = patch->getBox();
							double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
							double* FOV_2 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_2_id).get())->getPointer();
							double* FOV_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_4_id).get())->getPointer();
							double* FOV_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_5_id).get())->getPointer();
							double* FOV_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_7_id).get())->getPointer();
							double* FOV_8 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_8_id).get())->getPointer();
							double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
							double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
							double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
							double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
							double* FOV_zLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zLower_id).get())->getPointer();
							double* FOV_zUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zUpper_id).get())->getPointer();
							double* interior_i = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_i_id).get())->getPointer();
							double* interior_j = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_j_id).get())->getPointer();
							double* interior_k = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_k_id).get())->getPointer();
							double* interior = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_id).get())->getPointer();
							int* nonSync = ((pdat::NodeData<int> *) patch->getPatchData(d_nonSync_id).get())->getPointer();
							const hier::Index boxfirst = patch->getBox().lower();
							const hier::Index boxlast  = patch->getBox().upper();

							//Get delta spaces into an array. dx, dy, dz.
							const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
							const double* dx  = patch_geom->getDx();

							int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
							int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
							int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;

							if (firstPatch == pcounter && !finishedPatchGlobal) {
								for (k = 0; k < klast && !workingPatchArray[pcounter]; k++) {
									for (j = 0; j < jlast && !workingPatchArray[pcounter]; j++) {
										for (i = 0; i < ilast && !workingPatchArray[pcounter]; i++) {
											if (vector(nonSync, i, j, k) == 0) {
												floodfill(patch, i, j, k, pred, 5);
												workingPatchArray[pcounter] = 1;
											}
										}
									}
								}
							}
							pcounter++;
						}
					}
					d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);
				}
			}
			//Only one processor
			else {
				while (!finishedPatchGlobal) {
					d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);
					workingPatchGlobal = true;
					//Filling started floodfill in all the patches
					while (workingPatchGlobal) {
						pcounter = 0;
						for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
							const std::shared_ptr< hier::Patch >& patch = *p_it;

							//Get the dimensions of the patch
							hier::Box pbox = patch->getBox();
							double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
							double* FOV_2 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_2_id).get())->getPointer();
							double* FOV_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_4_id).get())->getPointer();
							double* FOV_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_5_id).get())->getPointer();
							double* FOV_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_7_id).get())->getPointer();
							double* FOV_8 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_8_id).get())->getPointer();
							double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
							double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
							double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
							double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
							double* FOV_zLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zLower_id).get())->getPointer();
							double* FOV_zUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zUpper_id).get())->getPointer();
							double* interior_i = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_i_id).get())->getPointer();
							double* interior_j = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_j_id).get())->getPointer();
							double* interior_k = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_k_id).get())->getPointer();
							double* interior = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_id).get())->getPointer();
							int* nonSync = ((pdat::NodeData<int> *) patch->getPatchData(d_nonSync_id).get())->getPointer();
							const hier::Index boxfirst = patch->getBox().lower();
							const hier::Index boxlast  = patch->getBox().upper();

							//Get delta spaces into an array. dx, dy, dz.
							const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
							const double* dx  = patch_geom->getDx();

							int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
							int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
							int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;

							workingPatchArray[pcounter] = 0;
							for (k = 0; k < klast && !workingPatchArray[pcounter]; k++) {
								for (j = 0; j < jlast && !workingPatchArray[pcounter]; j++) {
									for (i = 0; i < ilast && !workingPatchArray[pcounter]; i++) {
										if (vector(nonSync, i, j, k) != vector(interior, i, j, k) && vector(nonSync, i, j, k) ==0) {
											floodfill(patch, i, j, k, pred, 5);
											workingPatchArray[pcounter] = 1;
										} else {
											if (vector(nonSync, i, j, k) != vector(interior, i, j, k)) {
												vector(interior, i, j, k) = vector(nonSync, i, j, k);
												if (vector(nonSync, i, j, k) == 2) {
													vector(FOV_1, i, j, k) = 0;
													vector(FOV_2, i, j, k) = 0;
													vector(FOV_4, i, j, k) = 0;
													vector(FOV_5, i, j, k) = 100;
													vector(FOV_7, i, j, k) = 0;
													vector(FOV_8, i, j, k) = 0;
												}
											}
										}
									}
								}
							}
							pcounter++;
						}
						workingPatchGlobal = false;
						for (int pat = 0; pat < patches & !workingPatchGlobal; pat++) {
							if (workingPatchArray[pat] == 1) {
								workingPatchGlobal = true;
							}
						}
						d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);
					}
					//Looking for a new floodfill
					finished = 1;
					pcounter = 0;
					for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
						const std::shared_ptr< hier::Patch >& patch = *p_it;

						//Get the dimensions of the patch
						hier::Box pbox = patch->getBox();
						double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
						double* FOV_2 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_2_id).get())->getPointer();
						double* FOV_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_4_id).get())->getPointer();
						double* FOV_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_5_id).get())->getPointer();
						double* FOV_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_7_id).get())->getPointer();
						double* FOV_8 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_8_id).get())->getPointer();
						double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
						double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
						double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
						double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
						double* FOV_zLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zLower_id).get())->getPointer();
						double* FOV_zUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zUpper_id).get())->getPointer();
						double* interior_i = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_i_id).get())->getPointer();
						double* interior_j = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_j_id).get())->getPointer();
						double* interior_k = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_k_id).get())->getPointer();
						double* interior = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_id).get())->getPointer();
						int* nonSync = ((pdat::NodeData<int> *) patch->getPatchData(d_nonSync_id).get())->getPointer();
						const hier::Index boxfirst = patch->getBox().lower();
						const hier::Index boxlast  = patch->getBox().upper();

						//Get delta spaces into an array. dx, dy, dz.
						const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
						const double* dx  = patch_geom->getDx();

						int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
						int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
						int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;

						//Limits for the finished checking routine to avoid internal boundary problems
						int il = 0;
						if (!patch->getPatchGeometry()->getTouchesRegularBoundary (0, 0)) {
							il = 4;
						}
						int iu = ilast;
						if (!patch->getPatchGeometry()->getTouchesRegularBoundary (0, 1)) {
							iu = ilast - 8;
						}
						int jl = 0;
						if (!patch->getPatchGeometry()->getTouchesRegularBoundary (1, 0)) {
							jl = 4;
						}
						int ju = jlast;
						if (!patch->getPatchGeometry()->getTouchesRegularBoundary (1, 1)) {
							ju = jlast - 8;
						}
						int kl = 0;
						if (!patch->getPatchGeometry()->getTouchesRegularBoundary (2, 0)) {
							kl = 4;
						}
						int ku = klast;
						if (!patch->getPatchGeometry()->getTouchesRegularBoundary (2, 1)) {
							ku = klast - 8;
						}

						finishedPatchArray[pcounter] = 1;
						for (k = kl; k < ku && finishedPatchArray[pcounter]; k++) {
							for (j = jl; j < ju && finishedPatchArray[pcounter]; j++) {
								for (i = il; i < iu && finishedPatchArray[pcounter]; i++) {
									if (vector(nonSync, i, j, k) == 0) {
										finishedPatchArray[pcounter] = 0;
										finished = 0;
									}
								}
							}
						}
						pcounter++;
					}
					//Finished local variables communication
					finishedPatchGlobal = true;
					for (int pat = 0; pat < patches & finishedPatchGlobal; pat++) {
						if (finishedPatchArray[pat] == 0) {
							finishedPatchGlobal = 0;
						}
					}
					//Only one patch could start a floodfill at the same time
					//Take the first who wants to
					int firstPatch = -1;
					for (int pat = 0; pat < patches && !finishedPatchGlobal && firstPatch < 0; pat++) {
						if (finishedPatchArray[pat] == 0) {
							firstPatch = pat;
						}
					}
					if (pred == 1) {
						pred = 2;
					} else {
						pred = 1;
					}
					//New floodfill
					pcounter = 0;
					for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
						const std::shared_ptr< hier::Patch >& patch = *p_it;

						//Get the dimensions of the patch
						hier::Box pbox = patch->getBox();
						double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
						double* FOV_2 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_2_id).get())->getPointer();
						double* FOV_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_4_id).get())->getPointer();
						double* FOV_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_5_id).get())->getPointer();
						double* FOV_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_7_id).get())->getPointer();
						double* FOV_8 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_8_id).get())->getPointer();
						double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
						double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
						double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
						double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
						double* FOV_zLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zLower_id).get())->getPointer();
						double* FOV_zUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zUpper_id).get())->getPointer();
						double* interior_i = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_i_id).get())->getPointer();
						double* interior_j = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_j_id).get())->getPointer();
						double* interior_k = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_k_id).get())->getPointer();
						double* interior = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_id).get())->getPointer();
						int* nonSync = ((pdat::NodeData<int> *) patch->getPatchData(d_nonSync_id).get())->getPointer();
						const hier::Index boxfirst = patch->getBox().lower();
						const hier::Index boxlast  = patch->getBox().upper();

						//Get delta spaces into an array. dx, dy, dz.
						const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
						const double* dx  = patch_geom->getDx();

						int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
						int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
						int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;

						if (firstPatch == pcounter && !finishedPatchGlobal) {
							for (k = 0; k < klast && !workingPatchArray[pcounter]; k++) {
								for (j = 0; j < jlast && !workingPatchArray[pcounter]; j++) {
									for (i = 0; i < ilast && !workingPatchArray[pcounter]; i++) {
										if (vector(nonSync, i, j, k) == 0) {
											floodfill(patch, i, j, k, pred, 5);
											workingPatchArray[pcounter] = 1;
										}
									}
								}
							}
						}
						pcounter++;
					}
					d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);
				}
			}

			for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
				const std::shared_ptr< hier::Patch >& patch = *p_it;

				//Get the dimensions of the patch
				hier::Box pbox = patch->getBox();
				double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
				double* FOV_2 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_2_id).get())->getPointer();
				double* FOV_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_4_id).get())->getPointer();
				double* FOV_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_5_id).get())->getPointer();
				double* FOV_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_7_id).get())->getPointer();
				double* FOV_8 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_8_id).get())->getPointer();
				double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
				double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
				double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
				double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
				double* FOV_zLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zLower_id).get())->getPointer();
				double* FOV_zUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zUpper_id).get())->getPointer();
				double* interior_i = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_i_id).get())->getPointer();
				double* interior_j = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_j_id).get())->getPointer();
				double* interior_k = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_k_id).get())->getPointer();
				double* interior = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_id).get())->getPointer();
				int* nonSync = ((pdat::NodeData<int> *) patch->getPatchData(d_nonSync_id).get())->getPointer();
				const hier::Index boxfirst = patch->getBox().lower();
				const hier::Index boxlast  = patch->getBox().upper();

				//Get delta spaces into an array. dx, dy, dz.
				const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
				const double* dx  = patch_geom->getDx();

				int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
				int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
				int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;

				//Check stencil
				for (k = 0; k < klast; k++) {
					for (j = 0; j < jlast; j++) {
						for (i = 0; i < ilast; i++) {
							vector(interior_i, i, j, k) = 0;
							vector(interior_j, i, j, k) = 0;
							vector(interior_k, i, j, k) = 0;
						}
					}
				}
				for (k = 0; k < klast; k++) {
					for (j = 0; j < jlast; j++) {
						for (i = 0; i < ilast; i++) {
							if (vector(FOV_5, i, j, k) > 0) {
								setStencilLimits(patch, i, j, k, d_FOV_5_id);
							}
						}
					}
				}
			}
			d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);
			for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
				const std::shared_ptr< hier::Patch >& patch = *p_it;

				//Get the dimensions of the patch
				hier::Box pbox = patch->getBox();
				double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
				double* FOV_2 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_2_id).get())->getPointer();
				double* FOV_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_4_id).get())->getPointer();
				double* FOV_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_5_id).get())->getPointer();
				double* FOV_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_7_id).get())->getPointer();
				double* FOV_8 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_8_id).get())->getPointer();
				double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
				double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
				double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
				double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
				double* FOV_zLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zLower_id).get())->getPointer();
				double* FOV_zUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zUpper_id).get())->getPointer();
				double* interior_i = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_i_id).get())->getPointer();
				double* interior_j = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_j_id).get())->getPointer();
				double* interior_k = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_k_id).get())->getPointer();
				double* interior = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_id).get())->getPointer();
				int* nonSync = ((pdat::NodeData<int> *) patch->getPatchData(d_nonSync_id).get())->getPointer();
				const hier::Index boxfirst = patch->getBox().lower();
				const hier::Index boxlast  = patch->getBox().upper();

				//Get delta spaces into an array. dx, dy, dz.
				const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
				const double* dx  = patch_geom->getDx();

				int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
				int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
				int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;

				for (k = 0; k < klast; k++) {
					for (j = 0; j < jlast; j++) {
						for (i = 0; i < ilast; i++) {
							if ((abs(vector(interior_i, i, j, k)) > 1 || abs(vector(interior_j, i, j, k)) > 1 || abs(vector(interior_k, i, j, k)) > 1) && (i < d_ghost_width || i >= ilast - d_ghost_width || j < d_ghost_width || j >= jlast - d_ghost_width || k < d_ghost_width || k >= klast - d_ghost_width)) {
								checkStencil(patch, i, j, k, d_FOV_5_id);
							}
						}
					}
				}
				for (k = 0; k < klast; k++) {
					for (j = 0; j < jlast; j++) {
						for (i = 0; i < ilast; i++) {
							if (vector(interior_i, i, j, k) != 0 || vector(interior_j, i, j, k) != 0 || vector(interior_k, i, j, k) != 0) {
								vector(FOV_5, i, j, k) = 100;
								vector(FOV_1, i, j, k) = 0;
								vector(FOV_2, i, j, k) = 0;
								vector(FOV_4, i, j, k) = 0;
								vector(FOV_7, i, j, k) = 0;
								vector(FOV_8, i, j, k) = 0;
								vector(FOV_xLower, i, j, k) = 0;
								vector(FOV_xUpper, i, j, k) = 0;
								vector(FOV_yLower, i, j, k) = 0;
								vector(FOV_yUpper, i, j, k) = 0;
								vector(FOV_zLower, i, j, k) = 0;
								vector(FOV_zUpper, i, j, k) = 0;
							}
						}
					}
				}
			}
			d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);

			//Region: SoilS
			for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
				const std::shared_ptr< hier::Patch >& patch = *p_it;

				//Get the dimensions of the patch
				hier::Box pbox = patch->getBox();
				double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
				double* FOV_2 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_2_id).get())->getPointer();
				double* FOV_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_4_id).get())->getPointer();
				double* FOV_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_5_id).get())->getPointer();
				double* FOV_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_7_id).get())->getPointer();
				double* FOV_8 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_8_id).get())->getPointer();
				double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
				double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
				double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
				double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
				double* FOV_zLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zLower_id).get())->getPointer();
				double* FOV_zUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zUpper_id).get())->getPointer();
				double* interior_i = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_i_id).get())->getPointer();
				double* interior_j = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_j_id).get())->getPointer();
				double* interior_k = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_k_id).get())->getPointer();
				double* interior = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_id).get())->getPointer();
				int* nonSync = ((pdat::NodeData<int> *) patch->getPatchData(d_nonSync_id).get())->getPointer();
				const hier::Index boxfirst = patch->getBox().lower();
				const hier::Index boxlast  = patch->getBox().upper();

				//Get delta spaces into an array. dx, dy, dz.
				const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
				const double* dx  = patch_geom->getDx();

				int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
				int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
				int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;

				//Fill the faces
				for (unionsI = 0; unionsI < UNIONS_Soil; unionsI++) {
					for (facePointI = 0; facePointI < DIMENSIONS; facePointI++) {
						iterm = round((SoilPoints[SoilUnions[unionsI][facePointI]][0] - d_grid_geometry->getXLower()[0]) / dx[0]);
						jterm = round((SoilPoints[SoilUnions[unionsI][facePointI]][1] - d_grid_geometry->getXLower()[1]) / dx[1]);
						kterm = round((SoilPoints[SoilUnions[unionsI][facePointI]][2] - d_grid_geometry->getXLower()[2]) / dx[2]);
						//Take maximum and minimum coordinates of the face
						if (facePointI == 0) {
							minBlock[0] = iterm;
							maxBlock[0] = iterm;
							minBlock[1] = jterm;
							maxBlock[1] = jterm;
							minBlock[2] = kterm;
							maxBlock[2] = kterm;
						} else {
							if (iterm < minBlock[0]) {
								minBlock[0] = iterm;
							}
							if (iterm > maxBlock[0]) {
								maxBlock[0] = iterm;
							}
							if (jterm < minBlock[1]) {
								minBlock[1] = jterm;
							}
							if (jterm > maxBlock[1]) {
								maxBlock[1] = jterm;
							}
							if (kterm < minBlock[2]) {
								minBlock[2] = kterm;
							}
							if (kterm > maxBlock[2]) {
								maxBlock[2] = kterm;
							}
						}
					}
					if (minBlock[0] <= boxlast(0) + 1 + d_ghost_width && maxBlock[0] >= boxfirst(0) - d_ghost_width && minBlock[1] <= boxlast(1) + 1 + d_ghost_width && maxBlock[1] >= boxfirst(1) - d_ghost_width && minBlock[2] <= boxlast(2) + 1 + d_ghost_width && maxBlock[2] >= boxfirst(2) - d_ghost_width) {
						maxDistance = MAX(abs(minBlock[2] - maxBlock[2]),MAX(abs(minBlock[1] - maxBlock[1]),abs(minBlock[0] - maxBlock[0])));
						for (ie3 = 0; ie3 < 3 * maxDistance; ie3++) {
							e3 = ie3 / (3 * maxDistance);
							for (ie2 = 0; ie2 < 3 * maxDistance; ie2++) {
								e2 = ie2 / (3 * maxDistance);
								for (ie1 = 0; ie1 < 3 * maxDistance; ie1++) {
									e1 = ie1 / (3 * maxDistance);
									if (abs(ie1 + ie2 + ie3 - 3 * maxDistance) <= SQRT3INV) {
										iterm = round(((SoilPoints[SoilUnions[unionsI][0]][0] - d_grid_geometry->getXLower()[0]) / dx[0]) * e1 + ((SoilPoints[SoilUnions[unionsI][1]][0] - d_grid_geometry->getXLower()[0]) / dx[0]) * e2 + ((SoilPoints[SoilUnions[unionsI][2]][0] - d_grid_geometry->getXLower()[0]) / dx[0]) * e3);
										jterm = round(((SoilPoints[SoilUnions[unionsI][0]][1] - d_grid_geometry->getXLower()[1]) / dx[1]) * e1 + ((SoilPoints[SoilUnions[unionsI][1]][1] - d_grid_geometry->getXLower()[1]) / dx[1]) * e2 + ((SoilPoints[SoilUnions[unionsI][2]][1] - d_grid_geometry->getXLower()[1]) / dx[1]) * e3);
										kterm = round(((SoilPoints[SoilUnions[unionsI][0]][2] - d_grid_geometry->getXLower()[2]) / dx[2]) * e1 + ((SoilPoints[SoilUnions[unionsI][1]][2] - d_grid_geometry->getXLower()[2]) / dx[2]) * e2 + ((SoilPoints[SoilUnions[unionsI][2]][2] - d_grid_geometry->getXLower()[2]) / dx[2]) * e3);
										if (iterm >= boxfirst(0) - d_ghost_width && iterm <= boxlast(0) + d_ghost_width && jterm >= boxfirst(1) - d_ghost_width && jterm <= boxlast(1) + d_ghost_width && kterm >= boxfirst(2) - d_ghost_width && kterm <= boxlast(2) + d_ghost_width) {
											int indexi = iterm - boxfirst(0) + d_ghost_width;
											int indexj = jterm - boxfirst(1) + d_ghost_width;
											int indexk = kterm - boxfirst(2) + d_ghost_width;
											if (patch->getPatchGeometry()->getTouchesRegularBoundary (0, 1) && i - boxfirst(0) + 1 + d_ghost_width == ilast - d_ghost_width) {
												indexi--;
											}
											if (patch->getPatchGeometry()->getTouchesRegularBoundary (1, 1) && j - boxfirst(1) + 1 + d_ghost_width == jlast - d_ghost_width) {
												indexj--;
											}
											if (patch->getPatchGeometry()->getTouchesRegularBoundary (2, 1) && k - boxfirst(2) + 1 + d_ghost_width == klast - d_ghost_width) {
												indexk--;
											}
											vector(FOV_4, indexi, indexj, indexk) = 100;
											vector(FOV_1, indexi, indexj, indexk) = 0;
											vector(FOV_2, indexi, indexj, indexk) = 0;
											vector(FOV_5, indexi, indexj, indexk) = 0;
											vector(FOV_7, indexi, indexj, indexk) = 0;
											vector(FOV_8, indexi, indexj, indexk) = 0;
											vector(FOV_xLower, indexi, indexj, indexk) = 0;
											vector(FOV_xUpper, indexi, indexj, indexk) = 0;
											vector(FOV_yLower, indexi, indexj, indexk) = 0;
											vector(FOV_yUpper, indexi, indexj, indexk) = 0;
											vector(FOV_zLower, indexi, indexj, indexk) = 0;
											vector(FOV_zUpper, indexi, indexj, indexk) = 0;
										}
									}
								}
							}
						}
					}
				}
				//Check stencil
				for (k = 0; k < klast; k++) {
					for (j = 0; j < jlast; j++) {
						for (i = 0; i < ilast; i++) {
							vector(interior_i, i, j, k) = 0;
							vector(interior_j, i, j, k) = 0;
							vector(interior_k, i, j, k) = 0;
						}
					}
				}
				for (k = 0; k < klast; k++) {
					for (j = 0; j < jlast; j++) {
						for (i = 0; i < ilast; i++) {
							if (vector(FOV_4, i, j, k) > 0) {
								setStencilLimits(patch, i, j, k, d_FOV_4_id);
							}
						}
					}
				}
			}
			d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);
			for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
				const std::shared_ptr< hier::Patch >& patch = *p_it;

				//Get the dimensions of the patch
				hier::Box pbox = patch->getBox();
				double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
				double* FOV_2 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_2_id).get())->getPointer();
				double* FOV_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_4_id).get())->getPointer();
				double* FOV_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_5_id).get())->getPointer();
				double* FOV_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_7_id).get())->getPointer();
				double* FOV_8 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_8_id).get())->getPointer();
				double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
				double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
				double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
				double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
				double* FOV_zLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zLower_id).get())->getPointer();
				double* FOV_zUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zUpper_id).get())->getPointer();
				double* interior_i = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_i_id).get())->getPointer();
				double* interior_j = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_j_id).get())->getPointer();
				double* interior_k = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_k_id).get())->getPointer();
				double* interior = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_id).get())->getPointer();
				int* nonSync = ((pdat::NodeData<int> *) patch->getPatchData(d_nonSync_id).get())->getPointer();
				const hier::Index boxfirst = patch->getBox().lower();
				const hier::Index boxlast  = patch->getBox().upper();

				//Get delta spaces into an array. dx, dy, dz.
				const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
				const double* dx  = patch_geom->getDx();

				int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
				int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
				int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;

				for (k = 0; k < klast; k++) {
					for (j = 0; j < jlast; j++) {
						for (i = 0; i < ilast; i++) {
							if ((abs(vector(interior_i, i, j, k)) > 1 || abs(vector(interior_j, i, j, k)) > 1 || abs(vector(interior_k, i, j, k)) > 1) && (i < d_ghost_width || i >= ilast - d_ghost_width || j < d_ghost_width || j >= jlast - d_ghost_width || k < d_ghost_width || k >= klast - d_ghost_width)) {
								checkStencil(patch, i, j, k, d_FOV_4_id);
							}
						}
					}
				}
				for (k = 0; k < klast; k++) {
					for (j = 0; j < jlast; j++) {
						for (i = 0; i < ilast; i++) {
							if (vector(interior_i, i, j, k) != 0 || vector(interior_j, i, j, k) != 0 || vector(interior_k, i, j, k) != 0) {
								vector(FOV_4, i, j, k) = 100;
								vector(FOV_1, i, j, k) = 0;
								vector(FOV_2, i, j, k) = 0;
								vector(FOV_5, i, j, k) = 0;
								vector(FOV_7, i, j, k) = 0;
								vector(FOV_8, i, j, k) = 0;
								vector(FOV_xLower, i, j, k) = 0;
								vector(FOV_xUpper, i, j, k) = 0;
								vector(FOV_yLower, i, j, k) = 0;
								vector(FOV_yUpper, i, j, k) = 0;
								vector(FOV_zLower, i, j, k) = 0;
								vector(FOV_zUpper, i, j, k) = 0;
							}
						}
					}
				}
			}
			d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);

			//Region: InteriorMathI
			for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
				const std::shared_ptr< hier::Patch >& patch = *p_it;

				//Get the dimensions of the patch
				hier::Box pbox = patch->getBox();
				double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
				double* FOV_2 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_2_id).get())->getPointer();
				double* FOV_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_4_id).get())->getPointer();
				double* FOV_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_5_id).get())->getPointer();
				double* FOV_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_7_id).get())->getPointer();
				double* FOV_8 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_8_id).get())->getPointer();
				double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
				double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
				double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
				double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
				double* FOV_zLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zLower_id).get())->getPointer();
				double* FOV_zUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zUpper_id).get())->getPointer();
				double* interior_i = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_i_id).get())->getPointer();
				double* interior_j = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_j_id).get())->getPointer();
				double* interior_k = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_k_id).get())->getPointer();
				double* interior = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_id).get())->getPointer();
				int* nonSync = ((pdat::NodeData<int> *) patch->getPatchData(d_nonSync_id).get())->getPointer();
				const hier::Index boxfirst = patch->getBox().lower();
				const hier::Index boxlast  = patch->getBox().upper();

				//Get delta spaces into an array. dx, dy, dz.
				const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
				const double* dx  = patch_geom->getDx();

				int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
				int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
				int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;

				if (floor(18.0 / dx[0]) > d_ghost_width) {
					iMapStart = floor(18.0 / dx[0]);
				} else {
					iMapStart = 0;
				}
				if (floor(22.0 / dx[0]) < boxlast(0) - boxfirst(0) + d_ghost_width) {
					iMapEnd = floor(22.0 / dx[0]);
				} else {
					iMapEnd = boxlast(0) - boxfirst(0) + 2 * d_ghost_width;
				}
				if (floor(18.0 / dx[1]) > d_ghost_width) {
					jMapStart = floor(18.0 / dx[1]);
				} else {
					jMapStart = 0;
				}
				if (floor(22.0 / dx[1]) < boxlast(1) - boxfirst(1) + d_ghost_width) {
					jMapEnd = floor(22.0 / dx[1]);
				} else {
					jMapEnd = boxlast(1) - boxfirst(1) + 2 * d_ghost_width;
				}
				if (floor(18.0 / dx[2]) > d_ghost_width) {
					kMapStart = floor(18.0 / dx[2]);
				} else {
					kMapStart = 0;
				}
				if (floor(22.0 / dx[2]) < boxlast(2) - boxfirst(2) + d_ghost_width) {
					kMapEnd = floor(22.0 / dx[2]);
				} else {
					kMapEnd = boxlast(2) - boxfirst(2) + 2 * d_ghost_width;
				}
				for (i = iMapStart; i <= iMapEnd; i++) {
					for (j = jMapStart; j <= jMapEnd; j++) {
						for (k = kMapStart; k <= kMapEnd; k++) {
							if (i >= boxfirst(0) - d_ghost_width && i <= boxlast(0) + d_ghost_width && j >= boxfirst(1) - d_ghost_width && j <= boxlast(1) + d_ghost_width && k >= boxfirst(2) - d_ghost_width && k <= boxlast(2) + d_ghost_width) {
								vector(FOV_1, i - boxfirst(0) + d_ghost_width, j - boxfirst(1) + d_ghost_width, k - boxfirst(2) + d_ghost_width) = 100;
								vector(FOV_2, i - boxfirst(0) + d_ghost_width, j - boxfirst(1) + d_ghost_width, k - boxfirst(2) + d_ghost_width) = 0;
								vector(FOV_4, i - boxfirst(0) + d_ghost_width, j - boxfirst(1) + d_ghost_width, k - boxfirst(2) + d_ghost_width) = 0;
								vector(FOV_5, i - boxfirst(0) + d_ghost_width, j - boxfirst(1) + d_ghost_width, k - boxfirst(2) + d_ghost_width) = 0;
								vector(FOV_7, i - boxfirst(0) + d_ghost_width, j - boxfirst(1) + d_ghost_width, k - boxfirst(2) + d_ghost_width) = 0;
								vector(FOV_8, i - boxfirst(0) + d_ghost_width, j - boxfirst(1) + d_ghost_width, k - boxfirst(2) + d_ghost_width) = 0;
								vector(FOV_xLower, i - boxfirst(0) + d_ghost_width, j - boxfirst(1) + d_ghost_width, k - boxfirst(2) + d_ghost_width) = 0;
								vector(FOV_xUpper, i - boxfirst(0) + d_ghost_width, j - boxfirst(1) + d_ghost_width, k - boxfirst(2) + d_ghost_width) = 0;
								vector(FOV_yLower, i - boxfirst(0) + d_ghost_width, j - boxfirst(1) + d_ghost_width, k - boxfirst(2) + d_ghost_width) = 0;
								vector(FOV_yUpper, i - boxfirst(0) + d_ghost_width, j - boxfirst(1) + d_ghost_width, k - boxfirst(2) + d_ghost_width) = 0;
								vector(FOV_zLower, i - boxfirst(0) + d_ghost_width, j - boxfirst(1) + d_ghost_width, k - boxfirst(2) + d_ghost_width) = 0;
								vector(FOV_zUpper, i - boxfirst(0) + d_ghost_width, j - boxfirst(1) + d_ghost_width, k - boxfirst(2) + d_ghost_width) = 0;
							}
						}
					}
				}
				//Check stencil
				for (k = 0; k < klast; k++) {
					for (j = 0; j < jlast; j++) {
						for (i = 0; i < ilast; i++) {
							vector(interior_i, i, j, k) = 0;
							vector(interior_j, i, j, k) = 0;
							vector(interior_k, i, j, k) = 0;
						}
					}
				}
				for (k = 0; k < klast; k++) {
					for (j = 0; j < jlast; j++) {
						for (i = 0; i < ilast; i++) {
							if (vector(FOV_1, i, j, k) > 0) {
								setStencilLimits(patch, i, j, k, d_FOV_1_id);
							}
						}
					}
				}
			}
			d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);
			for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
				const std::shared_ptr< hier::Patch >& patch = *p_it;

				//Get the dimensions of the patch
				hier::Box pbox = patch->getBox();
				double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
				double* FOV_2 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_2_id).get())->getPointer();
				double* FOV_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_4_id).get())->getPointer();
				double* FOV_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_5_id).get())->getPointer();
				double* FOV_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_7_id).get())->getPointer();
				double* FOV_8 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_8_id).get())->getPointer();
				double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
				double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
				double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
				double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
				double* FOV_zLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zLower_id).get())->getPointer();
				double* FOV_zUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zUpper_id).get())->getPointer();
				double* interior_i = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_i_id).get())->getPointer();
				double* interior_j = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_j_id).get())->getPointer();
				double* interior_k = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_k_id).get())->getPointer();
				double* interior = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_id).get())->getPointer();
				int* nonSync = ((pdat::NodeData<int> *) patch->getPatchData(d_nonSync_id).get())->getPointer();
				const hier::Index boxfirst = patch->getBox().lower();
				const hier::Index boxlast  = patch->getBox().upper();

				//Get delta spaces into an array. dx, dy, dz.
				const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
				const double* dx  = patch_geom->getDx();

				int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
				int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
				int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;

				for (k = 0; k < klast; k++) {
					for (j = 0; j < jlast; j++) {
						for (i = 0; i < ilast; i++) {
							if ((abs(vector(interior_i, i, j, k)) > 1 || abs(vector(interior_j, i, j, k)) > 1 || abs(vector(interior_k, i, j, k)) > 1) && (i < d_ghost_width || i >= ilast - d_ghost_width || j < d_ghost_width || j >= jlast - d_ghost_width || k < d_ghost_width || k >= klast - d_ghost_width)) {
								checkStencil(patch, i, j, k, d_FOV_1_id);
							}
						}
					}
				}
				for (k = 0; k < klast; k++) {
					for (j = 0; j < jlast; j++) {
						for (i = 0; i < ilast; i++) {
							if (vector(interior_i, i, j, k) != 0 || vector(interior_j, i, j, k) != 0 || vector(interior_k, i, j, k) != 0) {
								vector(FOV_1, i, j, k) = 100;
								vector(FOV_2, i, j, k) = 0;
								vector(FOV_4, i, j, k) = 0;
								vector(FOV_5, i, j, k) = 0;
								vector(FOV_7, i, j, k) = 0;
								vector(FOV_8, i, j, k) = 0;
								vector(FOV_xLower, i, j, k) = 0;
								vector(FOV_xUpper, i, j, k) = 0;
								vector(FOV_yLower, i, j, k) = 0;
								vector(FOV_yUpper, i, j, k) = 0;
								vector(FOV_zLower, i, j, k) = 0;
								vector(FOV_zUpper, i, j, k) = 0;
							}
						}
					}
				}
			}
			d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);

		}
		//Boundaries Mapping
		for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
			const std::shared_ptr< hier::Patch >& patch = *p_it;

			//Get the dimensions of the patch
			hier::Box pbox = patch->getBox();
			double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
			double* FOV_2 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_2_id).get())->getPointer();
			double* FOV_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_4_id).get())->getPointer();
			double* FOV_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_5_id).get())->getPointer();
			double* FOV_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_7_id).get())->getPointer();
			double* FOV_8 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_8_id).get())->getPointer();
			double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
			double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
			double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
			double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
			double* FOV_zLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zLower_id).get())->getPointer();
			double* FOV_zUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zUpper_id).get())->getPointer();
			const hier::Index boxfirst = patch->getBox().lower();
			const hier::Index boxlast  = patch->getBox().upper();

			//Get delta spaces into an array. dx, dy, dz.
			const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
			const double* dx  = patch_geom->getDx();

			int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
			int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
			int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;

			//z-Upper
			if (patch->getPatchGeometry()->getTouchesRegularBoundary(2, 1)) {
				for (k = klast - d_ghost_width; k < klast; k++) {
					for (j = 0; j < jlast; j++) {
						for (i = 0; i < ilast; i++) {
							vector(FOV_zUpper, i, j, k) = 100;
							vector(FOV_1, i, j, k) = 0;
							vector(FOV_2, i, j, k) = 0;
							vector(FOV_4, i, j, k) = 0;
							vector(FOV_5, i, j, k) = 0;
							vector(FOV_7, i, j, k) = 0;
							vector(FOV_8, i, j, k) = 0;
							vector(FOV_xLower, i, j, k) = 0;
							vector(FOV_xUpper, i, j, k) = 0;
							vector(FOV_yLower, i, j, k) = 0;
							vector(FOV_yUpper, i, j, k) = 0;
							vector(FOV_zLower, i, j, k) = 0;
						}
					}
				}
			}
			//z-Lower
			if (patch->getPatchGeometry()->getTouchesRegularBoundary(2, 0)) {
				for (k = 0; k < d_ghost_width; k++) {
					for (j = 0; j < jlast; j++) {
						for (i = 0; i < ilast; i++) {
							vector(FOV_zLower, i, j, k) = 100;
							vector(FOV_1, i, j, k) = 0;
							vector(FOV_2, i, j, k) = 0;
							vector(FOV_4, i, j, k) = 0;
							vector(FOV_5, i, j, k) = 0;
							vector(FOV_7, i, j, k) = 0;
							vector(FOV_8, i, j, k) = 0;
							vector(FOV_xLower, i, j, k) = 0;
							vector(FOV_xUpper, i, j, k) = 0;
							vector(FOV_yLower, i, j, k) = 0;
							vector(FOV_yUpper, i, j, k) = 0;
							vector(FOV_zUpper, i, j, k) = 0;
						}
					}
				}
			}
			//y-Upper
			if (patch->getPatchGeometry()->getTouchesRegularBoundary(1, 1)) {
				for (k = 0; k < klast; k++) {
					for (j = jlast - d_ghost_width; j < jlast; j++) {
						for (i = 0; i < ilast; i++) {
							vector(FOV_yUpper, i, j, k) = 100;
							vector(FOV_1, i, j, k) = 0;
							vector(FOV_2, i, j, k) = 0;
							vector(FOV_4, i, j, k) = 0;
							vector(FOV_5, i, j, k) = 0;
							vector(FOV_7, i, j, k) = 0;
							vector(FOV_8, i, j, k) = 0;
							vector(FOV_xLower, i, j, k) = 0;
							vector(FOV_xUpper, i, j, k) = 0;
							vector(FOV_yLower, i, j, k) = 0;
							vector(FOV_zLower, i, j, k) = 0;
							vector(FOV_zUpper, i, j, k) = 0;
						}
					}
				}
			}
			//y-Lower
			if (patch->getPatchGeometry()->getTouchesRegularBoundary(1, 0)) {
				for (k = 0; k < klast; k++) {
					for (j = 0; j < d_ghost_width; j++) {
						for (i = 0; i < ilast; i++) {
							vector(FOV_yLower, i, j, k) = 100;
							vector(FOV_1, i, j, k) = 0;
							vector(FOV_2, i, j, k) = 0;
							vector(FOV_4, i, j, k) = 0;
							vector(FOV_5, i, j, k) = 0;
							vector(FOV_7, i, j, k) = 0;
							vector(FOV_8, i, j, k) = 0;
							vector(FOV_xLower, i, j, k) = 0;
							vector(FOV_xUpper, i, j, k) = 0;
							vector(FOV_yUpper, i, j, k) = 0;
							vector(FOV_zLower, i, j, k) = 0;
							vector(FOV_zUpper, i, j, k) = 0;
						}
					}
				}
			}
			//x-Upper
			if (patch->getPatchGeometry()->getTouchesRegularBoundary(0, 1)) {
				for (k = 0; k < klast; k++) {
					for (j = 0; j < jlast; j++) {
						for (i = ilast - d_ghost_width; i < ilast; i++) {
							vector(FOV_xUpper, i, j, k) = 100;
							vector(FOV_1, i, j, k) = 0;
							vector(FOV_2, i, j, k) = 0;
							vector(FOV_4, i, j, k) = 0;
							vector(FOV_5, i, j, k) = 0;
							vector(FOV_7, i, j, k) = 0;
							vector(FOV_8, i, j, k) = 0;
							vector(FOV_xLower, i, j, k) = 0;
							vector(FOV_yLower, i, j, k) = 0;
							vector(FOV_yUpper, i, j, k) = 0;
							vector(FOV_zLower, i, j, k) = 0;
							vector(FOV_zUpper, i, j, k) = 0;
						}
					}
				}
			}
			//x-Lower
			if (patch->getPatchGeometry()->getTouchesRegularBoundary(0, 0)) {
				for (k = 0; k < klast; k++) {
					for (j = 0; j < jlast; j++) {
						for (i = 0; i < d_ghost_width; i++) {
							vector(FOV_xLower, i, j, k) = 100;
							vector(FOV_1, i, j, k) = 0;
							vector(FOV_2, i, j, k) = 0;
							vector(FOV_4, i, j, k) = 0;
							vector(FOV_5, i, j, k) = 0;
							vector(FOV_7, i, j, k) = 0;
							vector(FOV_8, i, j, k) = 0;
							vector(FOV_xUpper, i, j, k) = 0;
							vector(FOV_yLower, i, j, k) = 0;
							vector(FOV_yUpper, i, j, k) = 0;
							vector(FOV_zLower, i, j, k) = 0;
							vector(FOV_zUpper, i, j, k) = 0;
						}
					}
				}
			}
		}
		d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);




   	}
}


/*
 * Sets the limit for the checkstencil routine
 */
void Problem::setStencilLimits(std::shared_ptr< hier::Patch > patch, int i, int j, int k, int v) const {
	double* FOV = ((pdat::NodeData<double> *) patch->getPatchData(v).get())->getPointer();
	double* interior_i = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_i_id).get())->getPointer();
	double* interior_j = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_j_id).get())->getPointer();
	double* interior_k = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_k_id).get())->getPointer();
	//Get the dimensions of the patch
	const hier::Index boxfirst = patch->getBox().lower();
	const hier::Index boxlast  = patch->getBox().upper();
	//Auxiliary definitions
	int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
	int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
	int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;

	int iStart, iEnd, currentGhosti, otherSideShifti, jStart, jEnd, currentGhostj, otherSideShiftj, kStart, kEnd, currentGhostk, otherSideShiftk, shift;

	currentGhosti = d_ghost_width - 1;
	otherSideShifti = 0;
	currentGhostj = d_ghost_width - 1;
	otherSideShiftj = 0;
	currentGhostk = d_ghost_width - 1;
	otherSideShiftk = 0;
	//Checking width
	if ((i + 1 < ilast && vector(FOV, i + 1, j, k) == 0) ||  (i - 1 >= 0 && vector(FOV, i - 1, j, k) == 0)) {
		if (i + 1 < ilast && vector(FOV, i + 1, j, k) > 0) {
			bool stop_counting = false;
			for(int iti = i + 1; iti <= i + d_ghost_width - 1 && currentGhosti > 0; iti++) {
				if (iti < ilast  && vector(FOV, iti, j, k) > 0 && stop_counting == false) {
					currentGhosti--;
				} else {
					//First not interior point found
					if (iti < ilast  && vector(FOV, iti, j, k) == 0) {
						stop_counting = true;
					}
					//Physical boundary reach
					if (iti >= ilast - 4 && patch->getPatchGeometry()->getTouchesRegularBoundary (0, 1)) {
						stop_counting = true;
						//Calculate the number of cells the limit cannot grow
						if (iti + currentGhosti/2 >= ilast - 4) {
							otherSideShifti = (iti  + currentGhosti/2) - (ilast - 4);
						}
					}
				}
			}
		}
		if (i - 1 >= 0 && vector(FOV, i - 1, j, k) > 0) {
			bool stop_counting = false;
			for(int iti = i - 1; iti >= i - d_ghost_width + 1 && currentGhosti > 0; iti--) {
				if (iti >= 0  && vector(FOV, iti, j, k) > 0) {
					currentGhosti--;
				} else {
					//First not interior point found
					if (iti >= 0 && vector(FOV, iti, j, k) == 0) {
						stop_counting = true;
					}
					//Physical boundary reach
					if (iti < 4 && patch->getPatchGeometry()->getTouchesRegularBoundary (0, 0)) {
						stop_counting = true;
						//calculate the number of cells the limit cannot grow
						if (iti  -  ((currentGhosti) - currentGhosti/2) < 4) {
							otherSideShifti = 4 - (iti  - ((currentGhosti) - currentGhosti/2));
						}
					}
				}
			}
		}
		if (currentGhosti > 0) {
			if (i + 1 < ilast && vector(FOV, i + 1, j, k) > 0) {
				shift = 0;
				if(patch->getPatchGeometry()->getTouchesRegularBoundary (0, 0)) {
					while(i - ((currentGhosti) - currentGhosti/2) + shift < 4) {
						shift++;
					}
				}
				iStart = (currentGhosti - currentGhosti/2) - shift - otherSideShifti;
				iEnd = 0;
			} else {
				if (i - 1 >= 0 && vector(FOV, i - 1, j, k) > 0) {
					shift = 0;
					if(patch->getPatchGeometry()->getTouchesRegularBoundary (0, 1)) {
						while(i + currentGhosti/2 + shift >= ilast - 4) {
							shift--;
						}
					}
					iStart = 0;
					iEnd = currentGhosti/2 + shift + otherSideShifti;
				} else {
					shift = 0;
					if(patch->getPatchGeometry()->getTouchesRegularBoundary (0, 0)) {
						while(i - ((currentGhosti) - currentGhosti/2) + shift < 4) {
							shift++;
						}
					}
					if(patch->getPatchGeometry()->getTouchesRegularBoundary (0, 1)) {
						while(i + currentGhosti/2 + shift >= ilast - 4) {
							shift--;
						}
					}
					iStart = (currentGhosti - currentGhosti/2) - shift;
					iEnd = currentGhosti/2 + shift;
				}
			}
		} else {
			iStart = 0;
			iEnd = 0;
		}
	} else {
		iStart = 0;
		iEnd = 0;
	}
	if ((j + 1 < jlast && vector(FOV, i, j + 1, k) == 0) ||  (j - 1 >= 0 && vector(FOV, i, j - 1, k) == 0)) {
		if (j + 1 < jlast && vector(FOV, i, j + 1, k) > 0) {
			bool stop_counting = false;
			for(int itj = j + 1; itj <= j + d_ghost_width - 1 && currentGhostj > 0; itj++) {
				if (itj < jlast  && vector(FOV, i, itj, k) > 0 && stop_counting == false) {
					currentGhostj--;
				} else {
					//First not interior point found
					if (itj < jlast  && vector(FOV, i, itj, k) == 0) {
						stop_counting = true;
					}
					//Physical boundary reach
					if (itj >= jlast - 4 && patch->getPatchGeometry()->getTouchesRegularBoundary (1, 1)) {
						stop_counting = true;
						//Calculate the number of cells the limit cannot grow
						if (itj + currentGhostj/2 >= jlast - 4) {
							otherSideShiftj = (itj  + currentGhostj/2) - (jlast - 4);
						}
					}
				}
			}
		}
		if (j - 1 >= 0 && vector(FOV, i, j - 1, k) > 0) {
			bool stop_counting = false;
			for(int itj = j - 1; itj >= j - d_ghost_width + 1 && currentGhostj > 0; itj--) {
				if (itj >= 0  && vector(FOV, i, itj, k) > 0) {
					currentGhostj--;
				} else {
					//First not interior point found
					if (itj >= 0 && vector(FOV, i, itj, k) == 0) {
						stop_counting = true;
					}
					//Physical boundary reach
					if (itj < 4 && patch->getPatchGeometry()->getTouchesRegularBoundary (1, 0)) {
						stop_counting = true;
						//calculate the number of cells the limit cannot grow
						if (itj  -  ((currentGhostj) - currentGhostj/2) < 4) {
							otherSideShiftj = 4 - (itj  - ((currentGhostj) - currentGhostj/2));
						}
					}
				}
			}
		}
		if (currentGhostj > 0) {
			if (j + 1 < jlast && vector(FOV, i, j + 1, k) > 0) {
				shift = 0;
				if(patch->getPatchGeometry()->getTouchesRegularBoundary (1, 0)) {
					while(j - ((currentGhostj) - currentGhostj/2) + shift < 4) {
						shift++;
					}
				}
				jStart = (currentGhostj - currentGhostj/2) - shift - otherSideShiftj;
				jEnd = 0;
			} else {
				if (j - 1 >= 0 && vector(FOV, i, j - 1, k) > 0) {
					shift = 0;
					if(patch->getPatchGeometry()->getTouchesRegularBoundary (1, 1)) {
						while(j + currentGhostj/2 + shift >= jlast - 4) {
							shift--;
						}
					}
					jStart = 0;
					jEnd = currentGhostj/2 + shift + otherSideShiftj;
				} else {
					shift = 0;
					if(patch->getPatchGeometry()->getTouchesRegularBoundary (1, 0)) {
						while(j - ((currentGhostj) - currentGhostj/2) + shift < 4) {
							shift++;
						}
					}
					if(patch->getPatchGeometry()->getTouchesRegularBoundary (1, 1)) {
						while(j + currentGhostj/2 + shift >= jlast - 4) {
							shift--;
						}
					}
					jStart = (currentGhostj - currentGhostj/2) - shift;
					jEnd = currentGhostj/2 + shift;
				}
			}
		} else {
			jStart = 0;
			jEnd = 0;
		}
	} else {
		jStart = 0;
		jEnd = 0;
	}
	if ((k + 1 < klast && vector(FOV, i, j, k + 1) == 0) ||  (k - 1 >= 0 && vector(FOV, i, j, k - 1) == 0)) {
		if (k + 1 < klast && vector(FOV, i, j, k + 1) > 0) {
			bool stop_counting = false;
			for(int itk = k + 1; itk <= k + d_ghost_width - 1 && currentGhostk > 0; itk++) {
				if (itk < klast  && vector(FOV, i, j, itk) > 0 && stop_counting == false) {
					currentGhostk--;
				} else {
					//First not interior point found
					if (itk < klast  && vector(FOV, i, j, itk) == 0) {
						stop_counting = true;
					}
					//Physical boundary reach
					if (itk >= klast - 4 && patch->getPatchGeometry()->getTouchesRegularBoundary (2, 1)) {
						stop_counting = true;
						//Calculate the number of cells the limit cannot grow
						if (itk + currentGhostk/2 >= klast - 4) {
							otherSideShiftk = (itk  + currentGhostk/2) - (klast - 4);
						}
					}
				}
			}
		}
		if (k - 1 >= 0 && vector(FOV, i, j, k - 1) > 0) {
			bool stop_counting = false;
			for(int itk = k - 1; itk >= k - d_ghost_width + 1 && currentGhostk > 0; itk--) {
				if (itk >= 0  && vector(FOV, i, j, itk) > 0) {
					currentGhostk--;
				} else {
					//First not interior point found
					if (itk >= 0 && vector(FOV, i, j, itk) == 0) {
						stop_counting = true;
					}
					//Physical boundary reach
					if (itk < 4 && patch->getPatchGeometry()->getTouchesRegularBoundary (2, 0)) {
						stop_counting = true;
						//calculate the number of cells the limit cannot grow
						if (itk  -  ((currentGhostk) - currentGhostk/2) < 4) {
							otherSideShiftk = 4 - (itk  - ((currentGhostk) - currentGhostk/2));
						}
					}
				}
			}
		}
		if (currentGhostk > 0) {
			if (k + 1 < klast && vector(FOV, i, j, k + 1) > 0) {
				shift = 0;
				if(patch->getPatchGeometry()->getTouchesRegularBoundary (2, 0)) {
					while(k - ((currentGhostk) - currentGhostk/2) + shift < 4) {
						shift++;
					}
				}
				kStart = (currentGhostk - currentGhostk/2) - shift - otherSideShiftk;
				kEnd = 0;
			} else {
				if (k - 1 >= 0 && vector(FOV, i, j, k - 1) > 0) {
					shift = 0;
					if(patch->getPatchGeometry()->getTouchesRegularBoundary (2, 1)) {
						while(k + currentGhostk/2 + shift >= klast - 4) {
							shift--;
						}
					}
					kStart = 0;
					kEnd = currentGhostk/2 + shift + otherSideShiftk;
				} else {
					shift = 0;
					if(patch->getPatchGeometry()->getTouchesRegularBoundary (2, 0)) {
						while(k - ((currentGhostk) - currentGhostk/2) + shift < 4) {
							shift++;
						}
					}
					if(patch->getPatchGeometry()->getTouchesRegularBoundary (2, 1)) {
						while(k + currentGhostk/2 + shift >= klast - 4) {
							shift--;
						}
					}
					kStart = (currentGhostk - currentGhostk/2) - shift;
					kEnd = currentGhostk/2 + shift;
				}
			}
		} else {
			kStart = 0;
			kEnd = 0;
		}
	} else {
		kStart = 0;
		kEnd = 0;
	}
	//Assigning stencil limits
	for(int iti = i - iStart; iti <= i + iEnd; iti++) {
		for(int itj = j - jStart; itj <= j + jEnd; itj++) {
			for(int itk = k - kStart; itk <= k + kEnd; itk++) {
				if(iti >= 0 && iti < ilast && itj >= 0 && itj < jlast && itk >= 0 && itk < klast && vector(FOV, iti, itj, itk) == 0) {
					if (i - iti < 0) {
						vector(interior_i, iti, itj, itk) = - (iStart + 1) - (i - iti);
					} else {
						vector(interior_i, iti, itj, itk) = (iStart + 1) - (i - iti);
					}
					if (j - itj < 0) {
						vector(interior_j, iti, itj, itk) = - (jStart + 1) - (j - itj);
					} else {
						vector(interior_j, iti, itj, itk) = (jStart + 1) - (j - itj);
					}
					if (k - itk < 0) {
						vector(interior_k, iti, itj, itk) = - (kStart + 1) - (k - itk);
					} else {
						vector(interior_k, iti, itj, itk) = (kStart + 1) - (k - itk);
					}
				}
			}
		}
	}
}

/*
 * Checks if the point has a stencil width
 */
void Problem::checkStencil(std::shared_ptr< hier::Patch > patch, int i, int j, int k, int v) const {
	double* FOV = ((pdat::NodeData<double> *) patch->getPatchData(v).get())->getPointer();
	double* interior_i = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_i_id).get())->getPointer();
	double* interior_j = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_j_id).get())->getPointer();
	double* interior_k = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_k_id).get())->getPointer();
	//Get the dimensions of the patch
	const hier::Index boxfirst = patch->getBox().lower();
	const hier::Index boxlast  = patch->getBox().upper();
	//Auxiliary definitions
	int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
	int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
	int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;

	int i_i = vector(interior_i, i, j, k);
	int iStart = MAX(0, i_i) - 1;
	int iEnd = MAX(0, -i_i) - 1;
	int i_j = vector(interior_j, i, j, k);
	int jStart = MAX(0, i_j) - 1;
	int jEnd = MAX(0, -i_j) - 1;
	int i_k = vector(interior_k, i, j, k);
	int kStart = MAX(0, i_k) - 1;
	int kEnd = MAX(0, -i_k) - 1;

	for(int iti = i - iStart; iti <= i + iEnd; iti++) {
		for(int itj = j - jStart; itj <= j + jEnd; itj++) {
			for(int itk = k - kStart; itk <= k + kEnd; itk++) {
				if(iti >= 0 && iti < ilast && itj >= 0 && itj < jlast && itk >= 0 && itk < klast && vector(FOV, iti, itj, itk) == 0) {
					vector(interior_i, iti, itj, itk) = i_i  - (i - iti);
					vector(interior_j, iti, itj, itk) = i_j  - (j - itj);
					vector(interior_k, iti, itj, itk) = i_k  - (k - itk);
				}
			}
		}
	}
}


// Point class for the floodfill algorithm
class Point {
private:
public:
	int i, j, k;
};

void Problem::floodfill(std::shared_ptr< hier::Patch > patch, int i, int j, int k, int pred, int seg) const {

	double* FOV;
	double* FOV_1;
	double* FOV_2;
	double* FOV_3;
	double* FOV_4;
	double* FOV_5;
	switch(seg) {
		case 1:
			FOV = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
			FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_2_id).get())->getPointer();
			FOV_2 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_4_id).get())->getPointer();
			FOV_3 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_5_id).get())->getPointer();
			FOV_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_7_id).get())->getPointer();
			FOV_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_8_id).get())->getPointer();
		break;
		case 2:
			FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
			FOV = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_2_id).get())->getPointer();
			FOV_2 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_4_id).get())->getPointer();
			FOV_3 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_5_id).get())->getPointer();
			FOV_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_7_id).get())->getPointer();
			FOV_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_8_id).get())->getPointer();
		break;
		case 4:
			FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
			FOV_2 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_2_id).get())->getPointer();
			FOV = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_4_id).get())->getPointer();
			FOV_3 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_5_id).get())->getPointer();
			FOV_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_7_id).get())->getPointer();
			FOV_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_8_id).get())->getPointer();
		break;
		case 5:
			FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
			FOV_2 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_2_id).get())->getPointer();
			FOV_3 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_4_id).get())->getPointer();
			FOV = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_5_id).get())->getPointer();
			FOV_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_7_id).get())->getPointer();
			FOV_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_8_id).get())->getPointer();
		break;
		case 7:
			FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
			FOV_2 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_2_id).get())->getPointer();
			FOV_3 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_4_id).get())->getPointer();
			FOV_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_5_id).get())->getPointer();
			FOV = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_7_id).get())->getPointer();
			FOV_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_8_id).get())->getPointer();
		break;
		case 8:
			FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
			FOV_2 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_2_id).get())->getPointer();
			FOV_3 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_4_id).get())->getPointer();
			FOV_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_5_id).get())->getPointer();
			FOV_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_7_id).get())->getPointer();
			FOV = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_8_id).get())->getPointer();
		break;
	}
	int* nonSync = ((pdat::NodeData<int> *) patch->getPatchData(d_nonSync_id).get())->getPointer();
	double* interior = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_id).get())->getPointer();
	//Get the dimensions of the patch
	const hier::Index boxfirst = patch->getBox().lower();
	const hier::Index boxlast  = patch->getBox().upper();
	//Auxiliary definitions
	int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
	int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
	int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;

	stack<Point> mystack;

	Point p;
	p.i = i;
	p.j = j;
	p.k = k;

	mystack.push(p);
	while(mystack.size() > 0) {
		p = mystack.top();
		mystack.pop();
		if (vector(nonSync, p.i, p.j, p.k) == 0) {
			vector(nonSync, p.i, p.j, p.k) = pred;
			vector(interior, p.i, p.j, p.k) = pred;
			if (pred == 2) {
				vector(FOV, p.i, p.j, p.k) = 100;
				vector(FOV_1, p.i, p.j, p.k) = 0;
				vector(FOV_2, p.i, p.j, p.k) = 0;
				vector(FOV_3, p.i, p.j, p.k) = 0;
				vector(FOV_4, p.i, p.j, p.k) = 0;
				vector(FOV_5, p.i, p.j, p.k) = 0;
			}
			if (p.i - 1 >= 0 && vector(nonSync, p.i - 1, p.j, p.k) == 0) {
				Point np;
				np.i = p.i-1;
				np.j = p.j;
				np.k = p.k;
				mystack.push(np);
			}
			if (p.i + 1 < ilast && vector(nonSync, p.i + 1, p.j, p.k) == 0) {
				Point np;
				np.i = p.i+1;
				np.j = p.j;
				np.k = p.k;
				mystack.push(np);
			}
			if (p.j - 1 >= 0 && vector(nonSync, p.i, p.j - 1, p.k) == 0) {
				Point np;
				np.i = p.i;
				np.j = p.j-1;
				np.k = p.k;
				mystack.push(np);
			}
			if (p.j + 1 < jlast && vector(nonSync, p.i, p.j + 1, p.k) == 0) {
				Point np;
				np.i = p.i;
				np.j = p.j+1;
				np.k = p.k;
				mystack.push(np);
			}
			if (p.k - 1 >= 0 && vector(nonSync, p.i, p.j, p.k - 1) == 0) {
				Point np;
				np.i = p.i;
				np.j = p.j;
				np.k = p.k-1;
				mystack.push(np);
			}
			if (p.k + 1 < klast && vector(nonSync, p.i, p.j, p.k + 1) == 0) {
				Point np;
				np.i = p.i;
				np.j = p.j;
				np.k = p.k+1;
				mystack.push(np);
			}
		}
	}
}


/*
 * FOV correction for AMR
 */
void Problem::correctFOVS(const std::shared_ptr< hier::PatchLevel >& level) {
	int i, j, k;
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr< hier::Patch >& patch = *p_it;

		//Get the dimensions of the patch
		hier::Box pbox = patch->getBox();
		double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_2 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_2_id).get())->getPointer();
		double* FOV_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_4_id).get())->getPointer();
		double* FOV_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_5_id).get())->getPointer();
		double* FOV_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_7_id).get())->getPointer();
		double* FOV_8 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_8_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
		double* FOV_zLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zLower_id).get())->getPointer();
		double* FOV_zUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zUpper_id).get())->getPointer();
		const hier::Index boxfirst = patch->getBox().lower();
		const hier::Index boxlast  = patch->getBox().upper();

		//Get delta spaces into an array. dx, dy, dz.
		const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
		const double* dx  = patch_geom->getDx();

		int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
		int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
		int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;

		for (i = 0; i < ilast; i++) {
			for (j = 0; j < jlast; j++) {
				for (k = 0; k < klast; k++) {
					if (vector(FOV_xLower, i, j, k) > 0) {
						vector(FOV_xLower, i, j, k) = 100;
						vector(FOV_1, i, j, k) = 0;
						vector(FOV_2, i, j, k) = 0;
						vector(FOV_4, i, j, k) = 0;
						vector(FOV_5, i, j, k) = 0;
						vector(FOV_7, i, j, k) = 0;
						vector(FOV_8, i, j, k) = 0;
						vector(FOV_xUpper, i, j, k) = 0;
						vector(FOV_yLower, i, j, k) = 0;
						vector(FOV_yUpper, i, j, k) = 0;
						vector(FOV_zLower, i, j, k) = 0;
						vector(FOV_zUpper, i, j, k) = 0;
					}
					if (vector(FOV_xUpper, i, j, k) > 0) {
						vector(FOV_xUpper, i, j, k) = 100;
						vector(FOV_1, i, j, k) = 0;
						vector(FOV_2, i, j, k) = 0;
						vector(FOV_4, i, j, k) = 0;
						vector(FOV_5, i, j, k) = 0;
						vector(FOV_7, i, j, k) = 0;
						vector(FOV_8, i, j, k) = 0;
						vector(FOV_xLower, i, j, k) = 0;
						vector(FOV_yLower, i, j, k) = 0;
						vector(FOV_yUpper, i, j, k) = 0;
						vector(FOV_zLower, i, j, k) = 0;
						vector(FOV_zUpper, i, j, k) = 0;
					}
					if (vector(FOV_yLower, i, j, k) > 0) {
						vector(FOV_yLower, i, j, k) = 100;
						vector(FOV_1, i, j, k) = 0;
						vector(FOV_2, i, j, k) = 0;
						vector(FOV_4, i, j, k) = 0;
						vector(FOV_5, i, j, k) = 0;
						vector(FOV_7, i, j, k) = 0;
						vector(FOV_8, i, j, k) = 0;
						vector(FOV_xLower, i, j, k) = 0;
						vector(FOV_xUpper, i, j, k) = 0;
						vector(FOV_yUpper, i, j, k) = 0;
						vector(FOV_zLower, i, j, k) = 0;
						vector(FOV_zUpper, i, j, k) = 0;
					}
					if (vector(FOV_yUpper, i, j, k) > 0) {
						vector(FOV_yUpper, i, j, k) = 100;
						vector(FOV_1, i, j, k) = 0;
						vector(FOV_2, i, j, k) = 0;
						vector(FOV_4, i, j, k) = 0;
						vector(FOV_5, i, j, k) = 0;
						vector(FOV_7, i, j, k) = 0;
						vector(FOV_8, i, j, k) = 0;
						vector(FOV_xLower, i, j, k) = 0;
						vector(FOV_xUpper, i, j, k) = 0;
						vector(FOV_yLower, i, j, k) = 0;
						vector(FOV_zLower, i, j, k) = 0;
						vector(FOV_zUpper, i, j, k) = 0;
					}
					if (vector(FOV_zLower, i, j, k) > 0) {
						vector(FOV_zLower, i, j, k) = 100;
						vector(FOV_1, i, j, k) = 0;
						vector(FOV_2, i, j, k) = 0;
						vector(FOV_4, i, j, k) = 0;
						vector(FOV_5, i, j, k) = 0;
						vector(FOV_7, i, j, k) = 0;
						vector(FOV_8, i, j, k) = 0;
						vector(FOV_xLower, i, j, k) = 0;
						vector(FOV_xUpper, i, j, k) = 0;
						vector(FOV_yLower, i, j, k) = 0;
						vector(FOV_yUpper, i, j, k) = 0;
						vector(FOV_zUpper, i, j, k) = 0;
					}
					if (vector(FOV_zUpper, i, j, k) > 0) {
						vector(FOV_zUpper, i, j, k) = 100;
						vector(FOV_1, i, j, k) = 0;
						vector(FOV_2, i, j, k) = 0;
						vector(FOV_4, i, j, k) = 0;
						vector(FOV_5, i, j, k) = 0;
						vector(FOV_7, i, j, k) = 0;
						vector(FOV_8, i, j, k) = 0;
						vector(FOV_xLower, i, j, k) = 0;
						vector(FOV_xUpper, i, j, k) = 0;
						vector(FOV_yLower, i, j, k) = 0;
						vector(FOV_yUpper, i, j, k) = 0;
						vector(FOV_zLower, i, j, k) = 0;
					}
					if (vector(FOV_1, i, j, k) > 0) {
						vector(FOV_1, i, j, k) = 100;
						vector(FOV_2, i, j, k) = 0;
						vector(FOV_4, i, j, k) = 0;
						vector(FOV_5, i, j, k) = 0;
						vector(FOV_7, i, j, k) = 0;
						vector(FOV_8, i, j, k) = 0;
						vector(FOV_xLower, i, j, k) = 0;
						vector(FOV_xUpper, i, j, k) = 0;
						vector(FOV_yLower, i, j, k) = 0;
						vector(FOV_yUpper, i, j, k) = 0;
						vector(FOV_zLower, i, j, k) = 0;
						vector(FOV_zUpper, i, j, k) = 0;
					}
					if (vector(FOV_4, i, j, k) > 0) {
						vector(FOV_4, i, j, k) = 100;
						vector(FOV_1, i, j, k) = 0;
						vector(FOV_2, i, j, k) = 0;
						vector(FOV_5, i, j, k) = 0;
						vector(FOV_7, i, j, k) = 0;
						vector(FOV_8, i, j, k) = 0;
						vector(FOV_xLower, i, j, k) = 0;
						vector(FOV_xUpper, i, j, k) = 0;
						vector(FOV_yLower, i, j, k) = 0;
						vector(FOV_yUpper, i, j, k) = 0;
						vector(FOV_zLower, i, j, k) = 0;
						vector(FOV_zUpper, i, j, k) = 0;
					}
					if (vector(FOV_5, i, j, k) > 0) {
						vector(FOV_5, i, j, k) = 100;
						vector(FOV_1, i, j, k) = 0;
						vector(FOV_2, i, j, k) = 0;
						vector(FOV_4, i, j, k) = 0;
						vector(FOV_7, i, j, k) = 0;
						vector(FOV_8, i, j, k) = 0;
						vector(FOV_xLower, i, j, k) = 0;
						vector(FOV_xUpper, i, j, k) = 0;
						vector(FOV_yLower, i, j, k) = 0;
						vector(FOV_yUpper, i, j, k) = 0;
						vector(FOV_zLower, i, j, k) = 0;
						vector(FOV_zUpper, i, j, k) = 0;
					}
					if (vector(FOV_7, i, j, k) > 0) {
						vector(FOV_7, i, j, k) = 100;
						vector(FOV_1, i, j, k) = 0;
						vector(FOV_2, i, j, k) = 0;
						vector(FOV_4, i, j, k) = 0;
						vector(FOV_5, i, j, k) = 0;
						vector(FOV_8, i, j, k) = 0;
						vector(FOV_xLower, i, j, k) = 0;
						vector(FOV_xUpper, i, j, k) = 0;
						vector(FOV_yLower, i, j, k) = 0;
						vector(FOV_yUpper, i, j, k) = 0;
						vector(FOV_zLower, i, j, k) = 0;
						vector(FOV_zUpper, i, j, k) = 0;
					}
				}
			}
		}
	}
}




void Problem::interphaseMapping(const double time ,const bool initial_time,const int ln, const std::shared_ptr< hier::PatchLevel >& level, const int remesh) {
	const tbox::SAMRAI_MPI& mpi(tbox::SAMRAI_MPI::getSAMRAIWorld());
	if (remesh == 1) {
		//Update extrapolation variables
		//Calculation of hard region distance variables
		for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
			const std::shared_ptr< hier::Patch >& patch = *p_it;

			//Get the dimensions of the patch
			const hier::Index boxfirst = patch->getBox().lower();
			const hier::Index boxlast  = patch->getBox().upper();
			const hier::IntVector ratio = level->getRatioToCoarserLevel();
			double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
			double* FOV_2 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_2_id).get())->getPointer();
			double* FOV_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_4_id).get())->getPointer();
			double* FOV_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_5_id).get())->getPointer();
			double* FOV_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_7_id).get())->getPointer();
			double* FOV_8 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_8_id).get())->getPointer();
			double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
			double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
			double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
			double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
			double* FOV_zLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zLower_id).get())->getPointer();
			double* FOV_zUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zUpper_id).get())->getPointer();
			double* stalled_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_stalled_1_id).get())->getPointer();
			double* stalled_2 = ((pdat::NodeData<double> *) patch->getPatchData(d_stalled_2_id).get())->getPointer();
			double* stalled_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_stalled_4_id).get())->getPointer();
			double* stalled_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_stalled_5_id).get())->getPointer();
			double* stalled_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_stalled_7_id).get())->getPointer();
			double* stalled_8 = ((pdat::NodeData<double> *) patch->getPatchData(d_stalled_8_id).get())->getPointer();
			//Hard region field distance variables
			double* d_i_Ey = ((pdat::NodeData<double> *) patch->getPatchData(d_d_i_Ey_id).get())->getPointer();
			double* d_j_Ey = ((pdat::NodeData<double> *) patch->getPatchData(d_d_j_Ey_id).get())->getPointer();
			double* d_k_Ey = ((pdat::NodeData<double> *) patch->getPatchData(d_d_k_Ey_id).get())->getPointer();
			double* d_i_Ex = ((pdat::NodeData<double> *) patch->getPatchData(d_d_i_Ex_id).get())->getPointer();
			double* d_j_Ex = ((pdat::NodeData<double> *) patch->getPatchData(d_d_j_Ex_id).get())->getPointer();
			double* d_k_Ex = ((pdat::NodeData<double> *) patch->getPatchData(d_d_k_Ex_id).get())->getPointer();

			int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
			int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
			int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;
			for (int i = 0; i < ilast; i++) {
				for (int j = 0; j < jlast; j++) {
					for (int k = 0; k < klast; k++) {
						vector(stalled_1, i, j, k) = checkStalled(patch, i, j, k, d_FOV_1_id);
						vector(stalled_2, i, j, k) = checkStalled(patch, i, j, k, d_FOV_2_id);
						vector(stalled_4, i, j, k) = checkStalled(patch, i, j, k, d_FOV_4_id);
						vector(stalled_5, i, j, k) = checkStalled(patch, i, j, k, d_FOV_5_id);
						vector(stalled_7, i, j, k) = checkStalled(patch, i, j, k, d_FOV_7_id);
						vector(stalled_8, i, j, k) = checkStalled(patch, i, j, k, d_FOV_8_id);
						vector(d_i_Ey, i, j, k) = 0;
						vector(d_j_Ey, i, j, k) = 0;
						vector(d_k_Ey, i, j, k) = 0;
						vector(d_i_Ex, i, j, k) = 0;
						vector(d_j_Ex, i, j, k) = 0;
						vector(d_k_Ex, i, j, k) = 0;
					}
				}
			}
		}
		d_mapping_fill->createSchedule(level, level)->fillData(time, true);
		for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
			const std::shared_ptr< hier::Patch >& patch = *p_it;

			//Get the dimensions of the patch
			const hier::Index boxfirst = patch->getBox().lower();
			const hier::Index boxlast  = patch->getBox().upper();
			const hier::IntVector ratio = level->getRatioToCoarserLevel();
			double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
			double* FOV_2 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_2_id).get())->getPointer();
			double* FOV_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_4_id).get())->getPointer();
			double* FOV_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_5_id).get())->getPointer();
			double* FOV_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_7_id).get())->getPointer();
			double* FOV_8 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_8_id).get())->getPointer();
			double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
			double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
			double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
			double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
			double* FOV_zLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zLower_id).get())->getPointer();
			double* FOV_zUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zUpper_id).get())->getPointer();
			double* stalled_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_stalled_1_id).get())->getPointer();
			double* stalled_2 = ((pdat::NodeData<double> *) patch->getPatchData(d_stalled_2_id).get())->getPointer();
			double* stalled_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_stalled_4_id).get())->getPointer();
			double* stalled_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_stalled_5_id).get())->getPointer();
			double* stalled_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_stalled_7_id).get())->getPointer();
			double* stalled_8 = ((pdat::NodeData<double> *) patch->getPatchData(d_stalled_8_id).get())->getPointer();
			//Hard region field distance variables
			double* d_i_Ey = ((pdat::NodeData<double> *) patch->getPatchData(d_d_i_Ey_id).get())->getPointer();
			double* d_j_Ey = ((pdat::NodeData<double> *) patch->getPatchData(d_d_j_Ey_id).get())->getPointer();
			double* d_k_Ey = ((pdat::NodeData<double> *) patch->getPatchData(d_d_k_Ey_id).get())->getPointer();
			double* d_i_Ex = ((pdat::NodeData<double> *) patch->getPatchData(d_d_i_Ex_id).get())->getPointer();
			double* d_j_Ex = ((pdat::NodeData<double> *) patch->getPatchData(d_d_j_Ex_id).get())->getPointer();
			double* d_k_Ex = ((pdat::NodeData<double> *) patch->getPatchData(d_d_k_Ex_id).get())->getPointer();

			int reGrid_i;
			if (ratio(0) == 0) reGrid_i = 1;
			else           reGrid_i = ratio(0);
			int reGrid_j;
			if (ratio(1) == 0) reGrid_j = 1;
			else           reGrid_j = ratio(1);
			int reGrid_k;
			if (ratio(2) == 0) reGrid_k = 1;
			else           reGrid_k = ratio(2);

			int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
			int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
			int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;
			int dist_i_tmp, dist_i_tmp_p, dist_i_tmp_m, dist_i_tmp_r;
			int dist_j_tmp, dist_j_tmp_p, dist_j_tmp_m, dist_j_tmp_r;
			int dist_k_tmp, dist_k_tmp_p, dist_k_tmp_m, dist_k_tmp_r;
			int dist_r, dist_r_tmp;

			for (int i = 0; i < ilast; i++) {
				for (int j = 0; j < jlast; j++) {
					for (int k = 0; k < klast; k++) {
						if (vector(FOV_8, i, j, k) > 0) {
							dist_i_tmp = 999;
							dist_i_tmp_p = 999;
							dist_i_tmp_m = 999;
							dist_i_tmp_r = 999;
							dist_j_tmp = 999;
							dist_j_tmp_p = 999;
							dist_j_tmp_m = 999;
							dist_j_tmp_r = 999;
							dist_k_tmp = 999;
							dist_k_tmp_p = 999;
							dist_k_tmp_m = 999;
							dist_k_tmp_r = 999;
							dist_r = 999;
							for (int Dist_i = -2 * reGrid_i; Dist_i <= 2 * reGrid_i; Dist_i++) {
								for (int Dist_j = -2 * reGrid_j; Dist_j <= 2 * reGrid_j; Dist_j++) {
									for (int Dist_k = -2 * reGrid_k; Dist_k <= 2 * reGrid_k; Dist_k++) {
										if (i + Dist_i >= 0 && i + Dist_i < ilast && j + Dist_j >= 0 && j + Dist_j < jlast && k + Dist_k >= 0 && k + Dist_k < klast && (!vector(stalled_1, i + Dist_i, j + Dist_j, k + Dist_k) || !vector(stalled_2, i + Dist_i, j + Dist_j, k + Dist_k))) {
											if (Dist_i < 0 && Dist_j == 0 && Dist_k == 0 && fabs(Dist_i) < fabs(dist_i_tmp_m)) {
												dist_i_tmp_m = -Dist_i;
											}
											if (Dist_i > 0 && Dist_j == 0 && Dist_k == 0 && fabs(Dist_i) < fabs(dist_i_tmp_p)) {
												dist_i_tmp_p = -Dist_i;
											}
											if (Dist_j < 0 && Dist_i == 0 && Dist_k == 0 && fabs(Dist_j) < fabs(dist_j_tmp_m)) {
												dist_j_tmp_m = -Dist_j;
											}
											if (Dist_j > 0 && Dist_i == 0 && Dist_k == 0 && fabs(Dist_j) < fabs(dist_j_tmp_p)) {
												dist_j_tmp_p = -Dist_j;
											}
											if (Dist_k < 0 && Dist_i == 0 && Dist_j == 0 && fabs(Dist_k) < fabs(dist_k_tmp_m)) {
												dist_k_tmp_m = -Dist_k;
											}
											if (Dist_k > 0 && Dist_i == 0 && Dist_j == 0 && fabs(Dist_k) < fabs(dist_k_tmp_p)) {
												dist_k_tmp_p = -Dist_k;
											}
											dist_r_tmp = Dist_i * Dist_i + Dist_j * Dist_j + Dist_k * Dist_k;
											if (dist_r_tmp < dist_r) {
												dist_r = dist_r_tmp;
												dist_i_tmp_r = -Dist_i;
												dist_j_tmp_r = -Dist_j;
												dist_k_tmp_r = -Dist_k;
											}
										}
									}
								}
							}
							if (fabs(dist_i_tmp_m) == fabs(dist_i_tmp_p) && dist_i_tmp_m < 999) {
								dist_i_tmp = 0;
								bool enough = true;
								for (int Dist_i = 0; Dist_i <= 1 && enough; Dist_i++) {
									if (!(i - dist_i_tmp_p + Dist_i < ilast && (!vector(stalled_1, i - dist_i_tmp_p + Dist_i, j, k) || !vector(stalled_2, i - dist_i_tmp_p + Dist_i, j, k)))) {
										enough = false;
									}
								}
								if (enough) {
									dist_i_tmp = dist_i_tmp_p;
								}
								enough = true;
								for (int Dist_i = -1; Dist_i <= 0 && enough; Dist_i++) {
									if (!(i - dist_i_tmp_m + Dist_i >= 0 && (!vector(stalled_1, i - dist_i_tmp_m + Dist_i, j, k) || !vector(stalled_2, i - dist_i_tmp_m + Dist_i, j, k)))) {
										enough = false;
									}
								}
								if (enough) {
									dist_i_tmp = dist_i_tmp_m;
								}
							} else if (fabs(dist_i_tmp_m) < fabs(dist_i_tmp_p)) {
								dist_i_tmp = dist_i_tmp_m;
							} else {
								dist_i_tmp = dist_i_tmp_p;
							}
							if (fabs(dist_j_tmp_m) == fabs(dist_j_tmp_p) && dist_j_tmp_m < 999) {
								dist_j_tmp = 0;
								bool enough = true;
								for (int Dist_j = 0; Dist_j <= 1 && enough; Dist_j++) {
									if (!(j - dist_j_tmp_p + Dist_j < jlast && (!vector(stalled_1, i, j - dist_j_tmp_p + Dist_j, k) || !vector(stalled_2, i, j - dist_j_tmp_p + Dist_j, k)))) {
										enough = false;
									}
								}
								if (enough) {
									dist_j_tmp = dist_j_tmp_p;
								}
								enough = true;
								for (int Dist_j = -1; Dist_j <= 0 && enough; Dist_j++) {
									if (!(j - dist_j_tmp_m + Dist_j >= 0 && (!vector(stalled_1, i, j - dist_j_tmp_m + Dist_j, k) || !vector(stalled_2, i, j - dist_j_tmp_m + Dist_j, k)))) {
										enough = false;
									}
								}
								if (enough) {
									dist_j_tmp = dist_j_tmp_m;
								}
							} else if (fabs(dist_j_tmp_m) < fabs(dist_j_tmp_p)) {
								dist_j_tmp = dist_j_tmp_m;
							} else {
								dist_j_tmp = dist_j_tmp_p;
							}
							if (fabs(dist_k_tmp_m) == fabs(dist_k_tmp_p) && dist_k_tmp_m < 999) {
								dist_k_tmp = 0;
								bool enough = true;
								for (int Dist_k = 0; Dist_k <= 1 && enough; Dist_k++) {
									if (!(k - dist_k_tmp_p + Dist_k < klast && (!vector(stalled_1, i, j, k - dist_k_tmp_p + Dist_k) || !vector(stalled_2, i, j, k - dist_k_tmp_p + Dist_k)))) {
										enough = false;
									}
								}
								if (enough) {
									dist_k_tmp = dist_k_tmp_p;
								}
								enough = true;
								for (int Dist_k = -1; Dist_k <= 0 && enough; Dist_k++) {
									if (!(k - dist_k_tmp_m + Dist_k >= 0 && (!vector(stalled_1, i, j, k - dist_k_tmp_m + Dist_k) || !vector(stalled_2, i, j, k - dist_k_tmp_m + Dist_k)))) {
										enough = false;
									}
								}
								if (enough) {
									dist_k_tmp = dist_k_tmp_m;
								}
							} else if (fabs(dist_k_tmp_m) < fabs(dist_k_tmp_p)) {
								dist_k_tmp = dist_k_tmp_m;
							} else {
								dist_k_tmp = dist_k_tmp_p;
							}
							if (dist_i_tmp == 999 && dist_j_tmp == 999 && dist_k_tmp == 999) {
								dist_i_tmp = dist_i_tmp_r;
								dist_j_tmp = dist_j_tmp_r;
								dist_k_tmp = dist_k_tmp_r;
							}
							if (dist_i_tmp != 999 && ((vector(d_i_Ex, i, j, k) == 0) || (fabs(dist_i_tmp) < fabs(vector(d_i_Ex, i, j, k))))) {
								vector(d_i_Ex, i, j, k) = dist_i_tmp;
							}
							if (dist_j_tmp != 999 && ((vector(d_j_Ex, i, j, k) == 0) || (fabs(dist_j_tmp) < fabs(vector(d_j_Ex, i, j, k))))) {
								vector(d_j_Ex, i, j, k) = dist_j_tmp;
							}
							if (dist_k_tmp != 999 && ((vector(d_k_Ex, i, j, k) == 0) || (fabs(dist_k_tmp) < fabs(vector(d_k_Ex, i, j, k))))) {
								vector(d_k_Ex, i, j, k) = dist_k_tmp;
							}
							for (int Dist_i = -3 * reGrid_i; Dist_i <= 3 * reGrid_i; Dist_i++) {
								for (int Dist_j = -3 * reGrid_j; Dist_j <= 3 * reGrid_j; Dist_j++) {
									for (int Dist_k = -3 * reGrid_k; Dist_k <= 3 * reGrid_k; Dist_k++) {
										if (i + Dist_i >= 0 && i + Dist_i < ilast && j + Dist_j >= 0 && j + Dist_j < jlast && k + Dist_k >= 0 && k + Dist_k < klast && (!vector(stalled_4, i + Dist_i, j + Dist_j, k + Dist_k))) {
											if (Dist_i < 0 && Dist_j == 0 && Dist_k == 0 && fabs(Dist_i) < fabs(dist_i_tmp_m)) {
												dist_i_tmp_m = -Dist_i;
											}
											if (Dist_i > 0 && Dist_j == 0 && Dist_k == 0 && fabs(Dist_i) < fabs(dist_i_tmp_p)) {
												dist_i_tmp_p = -Dist_i;
											}
											if (Dist_j < 0 && Dist_i == 0 && Dist_k == 0 && fabs(Dist_j) < fabs(dist_j_tmp_m)) {
												dist_j_tmp_m = -Dist_j;
											}
											if (Dist_j > 0 && Dist_i == 0 && Dist_k == 0 && fabs(Dist_j) < fabs(dist_j_tmp_p)) {
												dist_j_tmp_p = -Dist_j;
											}
											if (Dist_k < 0 && Dist_i == 0 && Dist_j == 0 && fabs(Dist_k) < fabs(dist_k_tmp_m)) {
												dist_k_tmp_m = -Dist_k;
											}
											if (Dist_k > 0 && Dist_i == 0 && Dist_j == 0 && fabs(Dist_k) < fabs(dist_k_tmp_p)) {
												dist_k_tmp_p = -Dist_k;
											}
											dist_r_tmp = Dist_i * Dist_i + Dist_j * Dist_j + Dist_k * Dist_k;
											if (dist_r_tmp < dist_r) {
												dist_r = dist_r_tmp;
												dist_i_tmp_r = -Dist_i;
												dist_j_tmp_r = -Dist_j;
												dist_k_tmp_r = -Dist_k;
											}
										}
									}
								}
							}
							if (fabs(dist_i_tmp_m) == fabs(dist_i_tmp_p) && dist_i_tmp_m < 999) {
								dist_i_tmp = 0;
								bool enough = true;
								for (int Dist_i = 0; Dist_i <= 2 && enough; Dist_i++) {
									if (!(i - dist_i_tmp_p + Dist_i < ilast && (!vector(stalled_4, i - dist_i_tmp_p + Dist_i, j, k)))) {
										enough = false;
									}
								}
								if (enough) {
									dist_i_tmp = dist_i_tmp_p;
								}
								enough = true;
								for (int Dist_i = -2; Dist_i <= 0 && enough; Dist_i++) {
									if (!(i - dist_i_tmp_m + Dist_i >= 0 && (!vector(stalled_4, i - dist_i_tmp_m + Dist_i, j, k)))) {
										enough = false;
									}
								}
								if (enough) {
									dist_i_tmp = dist_i_tmp_m;
								}
							} else if (fabs(dist_i_tmp_m) < fabs(dist_i_tmp_p)) {
								dist_i_tmp = dist_i_tmp_m;
							} else {
								dist_i_tmp = dist_i_tmp_p;
							}
							if (fabs(dist_j_tmp_m) == fabs(dist_j_tmp_p) && dist_j_tmp_m < 999) {
								dist_j_tmp = 0;
								bool enough = true;
								for (int Dist_j = 0; Dist_j <= 2 && enough; Dist_j++) {
									if (!(j - dist_j_tmp_p + Dist_j < jlast && (!vector(stalled_4, i, j - dist_j_tmp_p + Dist_j, k)))) {
										enough = false;
									}
								}
								if (enough) {
									dist_j_tmp = dist_j_tmp_p;
								}
								enough = true;
								for (int Dist_j = -2; Dist_j <= 0 && enough; Dist_j++) {
									if (!(j - dist_j_tmp_m + Dist_j >= 0 && (!vector(stalled_4, i, j - dist_j_tmp_m + Dist_j, k)))) {
										enough = false;
									}
								}
								if (enough) {
									dist_j_tmp = dist_j_tmp_m;
								}
							} else if (fabs(dist_j_tmp_m) < fabs(dist_j_tmp_p)) {
								dist_j_tmp = dist_j_tmp_m;
							} else {
								dist_j_tmp = dist_j_tmp_p;
							}
							if (fabs(dist_k_tmp_m) == fabs(dist_k_tmp_p) && dist_k_tmp_m < 999) {
								dist_k_tmp = 0;
								bool enough = true;
								for (int Dist_k = 0; Dist_k <= 2 && enough; Dist_k++) {
									if (!(k - dist_k_tmp_p + Dist_k < klast && (!vector(stalled_4, i, j, k - dist_k_tmp_p + Dist_k)))) {
										enough = false;
									}
								}
								if (enough) {
									dist_k_tmp = dist_k_tmp_p;
								}
								enough = true;
								for (int Dist_k = -2; Dist_k <= 0 && enough; Dist_k++) {
									if (!(k - dist_k_tmp_m + Dist_k >= 0 && (!vector(stalled_4, i, j, k - dist_k_tmp_m + Dist_k)))) {
										enough = false;
									}
								}
								if (enough) {
									dist_k_tmp = dist_k_tmp_m;
								}
							} else if (fabs(dist_k_tmp_m) < fabs(dist_k_tmp_p)) {
								dist_k_tmp = dist_k_tmp_m;
							} else {
								dist_k_tmp = dist_k_tmp_p;
							}
							if (dist_i_tmp == 999 && dist_j_tmp == 999 && dist_k_tmp == 999) {
								dist_i_tmp = dist_i_tmp_r;
								dist_j_tmp = dist_j_tmp_r;
								dist_k_tmp = dist_k_tmp_r;
							}
							if (dist_i_tmp != 999 && ((vector(d_i_Ex, i, j, k) == 0) || (fabs(dist_i_tmp) < fabs(vector(d_i_Ex, i, j, k))))) {
								vector(d_i_Ex, i, j, k) = dist_i_tmp;
							}
							if (dist_j_tmp != 999 && ((vector(d_j_Ex, i, j, k) == 0) || (fabs(dist_j_tmp) < fabs(vector(d_j_Ex, i, j, k))))) {
								vector(d_j_Ex, i, j, k) = dist_j_tmp;
							}
							if (dist_k_tmp != 999 && ((vector(d_k_Ex, i, j, k) == 0) || (fabs(dist_k_tmp) < fabs(vector(d_k_Ex, i, j, k))))) {
								vector(d_k_Ex, i, j, k) = dist_k_tmp;
							}
							for (int Dist_i = -2 * reGrid_i; Dist_i <= 2 * reGrid_i; Dist_i++) {
								for (int Dist_j = -2 * reGrid_j; Dist_j <= 2 * reGrid_j; Dist_j++) {
									for (int Dist_k = -2 * reGrid_k; Dist_k <= 2 * reGrid_k; Dist_k++) {
										if (i + Dist_i >= 0 && i + Dist_i < ilast && j + Dist_j >= 0 && j + Dist_j < jlast && k + Dist_k >= 0 && k + Dist_k < klast && (!vector(stalled_5, i + Dist_i, j + Dist_j, k + Dist_k))) {
											if (Dist_i < 0 && Dist_j == 0 && Dist_k == 0 && fabs(Dist_i) < fabs(dist_i_tmp_m)) {
												dist_i_tmp_m = -Dist_i;
											}
											if (Dist_i > 0 && Dist_j == 0 && Dist_k == 0 && fabs(Dist_i) < fabs(dist_i_tmp_p)) {
												dist_i_tmp_p = -Dist_i;
											}
											if (Dist_j < 0 && Dist_i == 0 && Dist_k == 0 && fabs(Dist_j) < fabs(dist_j_tmp_m)) {
												dist_j_tmp_m = -Dist_j;
											}
											if (Dist_j > 0 && Dist_i == 0 && Dist_k == 0 && fabs(Dist_j) < fabs(dist_j_tmp_p)) {
												dist_j_tmp_p = -Dist_j;
											}
											if (Dist_k < 0 && Dist_i == 0 && Dist_j == 0 && fabs(Dist_k) < fabs(dist_k_tmp_m)) {
												dist_k_tmp_m = -Dist_k;
											}
											if (Dist_k > 0 && Dist_i == 0 && Dist_j == 0 && fabs(Dist_k) < fabs(dist_k_tmp_p)) {
												dist_k_tmp_p = -Dist_k;
											}
											dist_r_tmp = Dist_i * Dist_i + Dist_j * Dist_j + Dist_k * Dist_k;
											if (dist_r_tmp < dist_r) {
												dist_r = dist_r_tmp;
												dist_i_tmp_r = -Dist_i;
												dist_j_tmp_r = -Dist_j;
												dist_k_tmp_r = -Dist_k;
											}
										}
									}
								}
							}
							if (fabs(dist_i_tmp_m) == fabs(dist_i_tmp_p) && dist_i_tmp_m < 999) {
								dist_i_tmp = 0;
								bool enough = true;
								for (int Dist_i = 0; Dist_i <= 1 && enough; Dist_i++) {
									if (!(i - dist_i_tmp_p + Dist_i < ilast && (!vector(stalled_5, i - dist_i_tmp_p + Dist_i, j, k)))) {
										enough = false;
									}
								}
								if (enough) {
									dist_i_tmp = dist_i_tmp_p;
								}
								enough = true;
								for (int Dist_i = -1; Dist_i <= 0 && enough; Dist_i++) {
									if (!(i - dist_i_tmp_m + Dist_i >= 0 && (!vector(stalled_5, i - dist_i_tmp_m + Dist_i, j, k)))) {
										enough = false;
									}
								}
								if (enough) {
									dist_i_tmp = dist_i_tmp_m;
								}
							} else if (fabs(dist_i_tmp_m) < fabs(dist_i_tmp_p)) {
								dist_i_tmp = dist_i_tmp_m;
							} else {
								dist_i_tmp = dist_i_tmp_p;
							}
							if (fabs(dist_j_tmp_m) == fabs(dist_j_tmp_p) && dist_j_tmp_m < 999) {
								dist_j_tmp = 0;
								bool enough = true;
								for (int Dist_j = 0; Dist_j <= 1 && enough; Dist_j++) {
									if (!(j - dist_j_tmp_p + Dist_j < jlast && (!vector(stalled_5, i, j - dist_j_tmp_p + Dist_j, k)))) {
										enough = false;
									}
								}
								if (enough) {
									dist_j_tmp = dist_j_tmp_p;
								}
								enough = true;
								for (int Dist_j = -1; Dist_j <= 0 && enough; Dist_j++) {
									if (!(j - dist_j_tmp_m + Dist_j >= 0 && (!vector(stalled_5, i, j - dist_j_tmp_m + Dist_j, k)))) {
										enough = false;
									}
								}
								if (enough) {
									dist_j_tmp = dist_j_tmp_m;
								}
							} else if (fabs(dist_j_tmp_m) < fabs(dist_j_tmp_p)) {
								dist_j_tmp = dist_j_tmp_m;
							} else {
								dist_j_tmp = dist_j_tmp_p;
							}
							if (fabs(dist_k_tmp_m) == fabs(dist_k_tmp_p) && dist_k_tmp_m < 999) {
								dist_k_tmp = 0;
								bool enough = true;
								for (int Dist_k = 0; Dist_k <= 1 && enough; Dist_k++) {
									if (!(k - dist_k_tmp_p + Dist_k < klast && (!vector(stalled_5, i, j, k - dist_k_tmp_p + Dist_k)))) {
										enough = false;
									}
								}
								if (enough) {
									dist_k_tmp = dist_k_tmp_p;
								}
								enough = true;
								for (int Dist_k = -1; Dist_k <= 0 && enough; Dist_k++) {
									if (!(k - dist_k_tmp_m + Dist_k >= 0 && (!vector(stalled_5, i, j, k - dist_k_tmp_m + Dist_k)))) {
										enough = false;
									}
								}
								if (enough) {
									dist_k_tmp = dist_k_tmp_m;
								}
							} else if (fabs(dist_k_tmp_m) < fabs(dist_k_tmp_p)) {
								dist_k_tmp = dist_k_tmp_m;
							} else {
								dist_k_tmp = dist_k_tmp_p;
							}
							if (dist_i_tmp == 999 && dist_j_tmp == 999 && dist_k_tmp == 999) {
								dist_i_tmp = dist_i_tmp_r;
								dist_j_tmp = dist_j_tmp_r;
								dist_k_tmp = dist_k_tmp_r;
							}
							if (dist_i_tmp != 999 && ((vector(d_i_Ex, i, j, k) == 0) || (fabs(dist_i_tmp) < fabs(vector(d_i_Ex, i, j, k))))) {
								vector(d_i_Ex, i, j, k) = dist_i_tmp;
							}
							if (dist_j_tmp != 999 && ((vector(d_j_Ex, i, j, k) == 0) || (fabs(dist_j_tmp) < fabs(vector(d_j_Ex, i, j, k))))) {
								vector(d_j_Ex, i, j, k) = dist_j_tmp;
							}
							if (dist_k_tmp != 999 && ((vector(d_k_Ex, i, j, k) == 0) || (fabs(dist_k_tmp) < fabs(vector(d_k_Ex, i, j, k))))) {
								vector(d_k_Ex, i, j, k) = dist_k_tmp;
							}
						}
						if (vector(FOV_5, i, j, k) > 0) {
							dist_i_tmp = 999;
							dist_i_tmp_p = 999;
							dist_i_tmp_m = 999;
							dist_i_tmp_r = 999;
							dist_j_tmp = 999;
							dist_j_tmp_p = 999;
							dist_j_tmp_m = 999;
							dist_j_tmp_r = 999;
							dist_k_tmp = 999;
							dist_k_tmp_p = 999;
							dist_k_tmp_m = 999;
							dist_k_tmp_r = 999;
							dist_r = 999;
							for (int Dist_i = -2 * reGrid_i; Dist_i <= 2 * reGrid_i; Dist_i++) {
								for (int Dist_j = -2 * reGrid_j; Dist_j <= 2 * reGrid_j; Dist_j++) {
									for (int Dist_k = -2 * reGrid_k; Dist_k <= 2 * reGrid_k; Dist_k++) {
										if (i + Dist_i >= 0 && i + Dist_i < ilast && j + Dist_j >= 0 && j + Dist_j < jlast && k + Dist_k >= 0 && k + Dist_k < klast && (!vector(stalled_1, i + Dist_i, j + Dist_j, k + Dist_k) || !vector(stalled_2, i + Dist_i, j + Dist_j, k + Dist_k))) {
											if (Dist_i < 0 && Dist_j == 0 && Dist_k == 0 && fabs(Dist_i) < fabs(dist_i_tmp_m)) {
												dist_i_tmp_m = -Dist_i;
											}
											if (Dist_i > 0 && Dist_j == 0 && Dist_k == 0 && fabs(Dist_i) < fabs(dist_i_tmp_p)) {
												dist_i_tmp_p = -Dist_i;
											}
											if (Dist_j < 0 && Dist_i == 0 && Dist_k == 0 && fabs(Dist_j) < fabs(dist_j_tmp_m)) {
												dist_j_tmp_m = -Dist_j;
											}
											if (Dist_j > 0 && Dist_i == 0 && Dist_k == 0 && fabs(Dist_j) < fabs(dist_j_tmp_p)) {
												dist_j_tmp_p = -Dist_j;
											}
											if (Dist_k < 0 && Dist_i == 0 && Dist_j == 0 && fabs(Dist_k) < fabs(dist_k_tmp_m)) {
												dist_k_tmp_m = -Dist_k;
											}
											if (Dist_k > 0 && Dist_i == 0 && Dist_j == 0 && fabs(Dist_k) < fabs(dist_k_tmp_p)) {
												dist_k_tmp_p = -Dist_k;
											}
											dist_r_tmp = Dist_i * Dist_i + Dist_j * Dist_j + Dist_k * Dist_k;
											if (dist_r_tmp < dist_r) {
												dist_r = dist_r_tmp;
												dist_i_tmp_r = -Dist_i;
												dist_j_tmp_r = -Dist_j;
												dist_k_tmp_r = -Dist_k;
											}
										}
									}
								}
							}
							if (fabs(dist_i_tmp_m) == fabs(dist_i_tmp_p) && dist_i_tmp_m < 999) {
								dist_i_tmp = 0;
								bool enough = true;
								for (int Dist_i = 0; Dist_i <= 1 && enough; Dist_i++) {
									if (!(i - dist_i_tmp_p + Dist_i < ilast && (!vector(stalled_1, i - dist_i_tmp_p + Dist_i, j, k) || !vector(stalled_2, i - dist_i_tmp_p + Dist_i, j, k)))) {
										enough = false;
									}
								}
								if (enough) {
									dist_i_tmp = dist_i_tmp_p;
								}
								enough = true;
								for (int Dist_i = -1; Dist_i <= 0 && enough; Dist_i++) {
									if (!(i - dist_i_tmp_m + Dist_i >= 0 && (!vector(stalled_1, i - dist_i_tmp_m + Dist_i, j, k) || !vector(stalled_2, i - dist_i_tmp_m + Dist_i, j, k)))) {
										enough = false;
									}
								}
								if (enough) {
									dist_i_tmp = dist_i_tmp_m;
								}
							} else if (fabs(dist_i_tmp_m) < fabs(dist_i_tmp_p)) {
								dist_i_tmp = dist_i_tmp_m;
							} else {
								dist_i_tmp = dist_i_tmp_p;
							}
							if (fabs(dist_j_tmp_m) == fabs(dist_j_tmp_p) && dist_j_tmp_m < 999) {
								dist_j_tmp = 0;
								bool enough = true;
								for (int Dist_j = 0; Dist_j <= 1 && enough; Dist_j++) {
									if (!(j - dist_j_tmp_p + Dist_j < jlast && (!vector(stalled_1, i, j - dist_j_tmp_p + Dist_j, k) || !vector(stalled_2, i, j - dist_j_tmp_p + Dist_j, k)))) {
										enough = false;
									}
								}
								if (enough) {
									dist_j_tmp = dist_j_tmp_p;
								}
								enough = true;
								for (int Dist_j = -1; Dist_j <= 0 && enough; Dist_j++) {
									if (!(j - dist_j_tmp_m + Dist_j >= 0 && (!vector(stalled_1, i, j - dist_j_tmp_m + Dist_j, k) || !vector(stalled_2, i, j - dist_j_tmp_m + Dist_j, k)))) {
										enough = false;
									}
								}
								if (enough) {
									dist_j_tmp = dist_j_tmp_m;
								}
							} else if (fabs(dist_j_tmp_m) < fabs(dist_j_tmp_p)) {
								dist_j_tmp = dist_j_tmp_m;
							} else {
								dist_j_tmp = dist_j_tmp_p;
							}
							if (fabs(dist_k_tmp_m) == fabs(dist_k_tmp_p) && dist_k_tmp_m < 999) {
								dist_k_tmp = 0;
								bool enough = true;
								for (int Dist_k = 0; Dist_k <= 1 && enough; Dist_k++) {
									if (!(k - dist_k_tmp_p + Dist_k < klast && (!vector(stalled_1, i, j, k - dist_k_tmp_p + Dist_k) || !vector(stalled_2, i, j, k - dist_k_tmp_p + Dist_k)))) {
										enough = false;
									}
								}
								if (enough) {
									dist_k_tmp = dist_k_tmp_p;
								}
								enough = true;
								for (int Dist_k = -1; Dist_k <= 0 && enough; Dist_k++) {
									if (!(k - dist_k_tmp_m + Dist_k >= 0 && (!vector(stalled_1, i, j, k - dist_k_tmp_m + Dist_k) || !vector(stalled_2, i, j, k - dist_k_tmp_m + Dist_k)))) {
										enough = false;
									}
								}
								if (enough) {
									dist_k_tmp = dist_k_tmp_m;
								}
							} else if (fabs(dist_k_tmp_m) < fabs(dist_k_tmp_p)) {
								dist_k_tmp = dist_k_tmp_m;
							} else {
								dist_k_tmp = dist_k_tmp_p;
							}
							if (dist_i_tmp == 999 && dist_j_tmp == 999 && dist_k_tmp == 999) {
								dist_i_tmp = dist_i_tmp_r;
								dist_j_tmp = dist_j_tmp_r;
								dist_k_tmp = dist_k_tmp_r;
							}
							if (dist_i_tmp != 999 && ((vector(d_i_Ey, i, j, k) == 0) || (fabs(dist_i_tmp) < fabs(vector(d_i_Ey, i, j, k))))) {
								vector(d_i_Ey, i, j, k) = dist_i_tmp;
							}
							if (dist_j_tmp != 999 && ((vector(d_j_Ey, i, j, k) == 0) || (fabs(dist_j_tmp) < fabs(vector(d_j_Ey, i, j, k))))) {
								vector(d_j_Ey, i, j, k) = dist_j_tmp;
							}
							if (dist_k_tmp != 999 && ((vector(d_k_Ey, i, j, k) == 0) || (fabs(dist_k_tmp) < fabs(vector(d_k_Ey, i, j, k))))) {
								vector(d_k_Ey, i, j, k) = dist_k_tmp;
							}
							for (int Dist_i = -3 * reGrid_i; Dist_i <= 3 * reGrid_i; Dist_i++) {
								for (int Dist_j = -3 * reGrid_j; Dist_j <= 3 * reGrid_j; Dist_j++) {
									for (int Dist_k = -3 * reGrid_k; Dist_k <= 3 * reGrid_k; Dist_k++) {
										if (i + Dist_i >= 0 && i + Dist_i < ilast && j + Dist_j >= 0 && j + Dist_j < jlast && k + Dist_k >= 0 && k + Dist_k < klast && (!vector(stalled_4, i + Dist_i, j + Dist_j, k + Dist_k))) {
											if (Dist_i < 0 && Dist_j == 0 && Dist_k == 0 && fabs(Dist_i) < fabs(dist_i_tmp_m)) {
												dist_i_tmp_m = -Dist_i;
											}
											if (Dist_i > 0 && Dist_j == 0 && Dist_k == 0 && fabs(Dist_i) < fabs(dist_i_tmp_p)) {
												dist_i_tmp_p = -Dist_i;
											}
											if (Dist_j < 0 && Dist_i == 0 && Dist_k == 0 && fabs(Dist_j) < fabs(dist_j_tmp_m)) {
												dist_j_tmp_m = -Dist_j;
											}
											if (Dist_j > 0 && Dist_i == 0 && Dist_k == 0 && fabs(Dist_j) < fabs(dist_j_tmp_p)) {
												dist_j_tmp_p = -Dist_j;
											}
											if (Dist_k < 0 && Dist_i == 0 && Dist_j == 0 && fabs(Dist_k) < fabs(dist_k_tmp_m)) {
												dist_k_tmp_m = -Dist_k;
											}
											if (Dist_k > 0 && Dist_i == 0 && Dist_j == 0 && fabs(Dist_k) < fabs(dist_k_tmp_p)) {
												dist_k_tmp_p = -Dist_k;
											}
											dist_r_tmp = Dist_i * Dist_i + Dist_j * Dist_j + Dist_k * Dist_k;
											if (dist_r_tmp < dist_r) {
												dist_r = dist_r_tmp;
												dist_i_tmp_r = -Dist_i;
												dist_j_tmp_r = -Dist_j;
												dist_k_tmp_r = -Dist_k;
											}
										}
									}
								}
							}
							if (fabs(dist_i_tmp_m) == fabs(dist_i_tmp_p) && dist_i_tmp_m < 999) {
								dist_i_tmp = 0;
								bool enough = true;
								for (int Dist_i = 0; Dist_i <= 2 && enough; Dist_i++) {
									if (!(i - dist_i_tmp_p + Dist_i < ilast && (!vector(stalled_4, i - dist_i_tmp_p + Dist_i, j, k)))) {
										enough = false;
									}
								}
								if (enough) {
									dist_i_tmp = dist_i_tmp_p;
								}
								enough = true;
								for (int Dist_i = -2; Dist_i <= 0 && enough; Dist_i++) {
									if (!(i - dist_i_tmp_m + Dist_i >= 0 && (!vector(stalled_4, i - dist_i_tmp_m + Dist_i, j, k)))) {
										enough = false;
									}
								}
								if (enough) {
									dist_i_tmp = dist_i_tmp_m;
								}
							} else if (fabs(dist_i_tmp_m) < fabs(dist_i_tmp_p)) {
								dist_i_tmp = dist_i_tmp_m;
							} else {
								dist_i_tmp = dist_i_tmp_p;
							}
							if (fabs(dist_j_tmp_m) == fabs(dist_j_tmp_p) && dist_j_tmp_m < 999) {
								dist_j_tmp = 0;
								bool enough = true;
								for (int Dist_j = 0; Dist_j <= 2 && enough; Dist_j++) {
									if (!(j - dist_j_tmp_p + Dist_j < jlast && (!vector(stalled_4, i, j - dist_j_tmp_p + Dist_j, k)))) {
										enough = false;
									}
								}
								if (enough) {
									dist_j_tmp = dist_j_tmp_p;
								}
								enough = true;
								for (int Dist_j = -2; Dist_j <= 0 && enough; Dist_j++) {
									if (!(j - dist_j_tmp_m + Dist_j >= 0 && (!vector(stalled_4, i, j - dist_j_tmp_m + Dist_j, k)))) {
										enough = false;
									}
								}
								if (enough) {
									dist_j_tmp = dist_j_tmp_m;
								}
							} else if (fabs(dist_j_tmp_m) < fabs(dist_j_tmp_p)) {
								dist_j_tmp = dist_j_tmp_m;
							} else {
								dist_j_tmp = dist_j_tmp_p;
							}
							if (fabs(dist_k_tmp_m) == fabs(dist_k_tmp_p) && dist_k_tmp_m < 999) {
								dist_k_tmp = 0;
								bool enough = true;
								for (int Dist_k = 0; Dist_k <= 2 && enough; Dist_k++) {
									if (!(k - dist_k_tmp_p + Dist_k < klast && (!vector(stalled_4, i, j, k - dist_k_tmp_p + Dist_k)))) {
										enough = false;
									}
								}
								if (enough) {
									dist_k_tmp = dist_k_tmp_p;
								}
								enough = true;
								for (int Dist_k = -2; Dist_k <= 0 && enough; Dist_k++) {
									if (!(k - dist_k_tmp_m + Dist_k >= 0 && (!vector(stalled_4, i, j, k - dist_k_tmp_m + Dist_k)))) {
										enough = false;
									}
								}
								if (enough) {
									dist_k_tmp = dist_k_tmp_m;
								}
							} else if (fabs(dist_k_tmp_m) < fabs(dist_k_tmp_p)) {
								dist_k_tmp = dist_k_tmp_m;
							} else {
								dist_k_tmp = dist_k_tmp_p;
							}
							if (dist_i_tmp == 999 && dist_j_tmp == 999 && dist_k_tmp == 999) {
								dist_i_tmp = dist_i_tmp_r;
								dist_j_tmp = dist_j_tmp_r;
								dist_k_tmp = dist_k_tmp_r;
							}
							if (dist_i_tmp != 999 && ((vector(d_i_Ey, i, j, k) == 0) || (fabs(dist_i_tmp) < fabs(vector(d_i_Ey, i, j, k))))) {
								vector(d_i_Ey, i, j, k) = dist_i_tmp;
							}
							if (dist_j_tmp != 999 && ((vector(d_j_Ey, i, j, k) == 0) || (fabs(dist_j_tmp) < fabs(vector(d_j_Ey, i, j, k))))) {
								vector(d_j_Ey, i, j, k) = dist_j_tmp;
							}
							if (dist_k_tmp != 999 && ((vector(d_k_Ey, i, j, k) == 0) || (fabs(dist_k_tmp) < fabs(vector(d_k_Ey, i, j, k))))) {
								vector(d_k_Ey, i, j, k) = dist_k_tmp;
							}
							for (int Dist_i = -2 * reGrid_i; Dist_i <= 2 * reGrid_i; Dist_i++) {
								for (int Dist_j = -2 * reGrid_j; Dist_j <= 2 * reGrid_j; Dist_j++) {
									for (int Dist_k = -2 * reGrid_k; Dist_k <= 2 * reGrid_k; Dist_k++) {
										if (i + Dist_i >= 0 && i + Dist_i < ilast && j + Dist_j >= 0 && j + Dist_j < jlast && k + Dist_k >= 0 && k + Dist_k < klast && (!vector(stalled_7, i + Dist_i, j + Dist_j, k + Dist_k) || !vector(stalled_8, i + Dist_i, j + Dist_j, k + Dist_k))) {
											if (Dist_i < 0 && Dist_j == 0 && Dist_k == 0 && fabs(Dist_i) < fabs(dist_i_tmp_m)) {
												dist_i_tmp_m = -Dist_i;
											}
											if (Dist_i > 0 && Dist_j == 0 && Dist_k == 0 && fabs(Dist_i) < fabs(dist_i_tmp_p)) {
												dist_i_tmp_p = -Dist_i;
											}
											if (Dist_j < 0 && Dist_i == 0 && Dist_k == 0 && fabs(Dist_j) < fabs(dist_j_tmp_m)) {
												dist_j_tmp_m = -Dist_j;
											}
											if (Dist_j > 0 && Dist_i == 0 && Dist_k == 0 && fabs(Dist_j) < fabs(dist_j_tmp_p)) {
												dist_j_tmp_p = -Dist_j;
											}
											if (Dist_k < 0 && Dist_i == 0 && Dist_j == 0 && fabs(Dist_k) < fabs(dist_k_tmp_m)) {
												dist_k_tmp_m = -Dist_k;
											}
											if (Dist_k > 0 && Dist_i == 0 && Dist_j == 0 && fabs(Dist_k) < fabs(dist_k_tmp_p)) {
												dist_k_tmp_p = -Dist_k;
											}
											dist_r_tmp = Dist_i * Dist_i + Dist_j * Dist_j + Dist_k * Dist_k;
											if (dist_r_tmp < dist_r) {
												dist_r = dist_r_tmp;
												dist_i_tmp_r = -Dist_i;
												dist_j_tmp_r = -Dist_j;
												dist_k_tmp_r = -Dist_k;
											}
										}
									}
								}
							}
							if (fabs(dist_i_tmp_m) == fabs(dist_i_tmp_p) && dist_i_tmp_m < 999) {
								dist_i_tmp = 0;
								bool enough = true;
								for (int Dist_i = 0; Dist_i <= 1 && enough; Dist_i++) {
									if (!(i - dist_i_tmp_p + Dist_i < ilast && (!vector(stalled_7, i - dist_i_tmp_p + Dist_i, j, k) || !vector(stalled_8, i - dist_i_tmp_p + Dist_i, j, k)))) {
										enough = false;
									}
								}
								if (enough) {
									dist_i_tmp = dist_i_tmp_p;
								}
								enough = true;
								for (int Dist_i = -1; Dist_i <= 0 && enough; Dist_i++) {
									if (!(i - dist_i_tmp_m + Dist_i >= 0 && (!vector(stalled_7, i - dist_i_tmp_m + Dist_i, j, k) || !vector(stalled_8, i - dist_i_tmp_m + Dist_i, j, k)))) {
										enough = false;
									}
								}
								if (enough) {
									dist_i_tmp = dist_i_tmp_m;
								}
							} else if (fabs(dist_i_tmp_m) < fabs(dist_i_tmp_p)) {
								dist_i_tmp = dist_i_tmp_m;
							} else {
								dist_i_tmp = dist_i_tmp_p;
							}
							if (fabs(dist_j_tmp_m) == fabs(dist_j_tmp_p) && dist_j_tmp_m < 999) {
								dist_j_tmp = 0;
								bool enough = true;
								for (int Dist_j = 0; Dist_j <= 1 && enough; Dist_j++) {
									if (!(j - dist_j_tmp_p + Dist_j < jlast && (!vector(stalled_7, i, j - dist_j_tmp_p + Dist_j, k) || !vector(stalled_8, i, j - dist_j_tmp_p + Dist_j, k)))) {
										enough = false;
									}
								}
								if (enough) {
									dist_j_tmp = dist_j_tmp_p;
								}
								enough = true;
								for (int Dist_j = -1; Dist_j <= 0 && enough; Dist_j++) {
									if (!(j - dist_j_tmp_m + Dist_j >= 0 && (!vector(stalled_7, i, j - dist_j_tmp_m + Dist_j, k) || !vector(stalled_8, i, j - dist_j_tmp_m + Dist_j, k)))) {
										enough = false;
									}
								}
								if (enough) {
									dist_j_tmp = dist_j_tmp_m;
								}
							} else if (fabs(dist_j_tmp_m) < fabs(dist_j_tmp_p)) {
								dist_j_tmp = dist_j_tmp_m;
							} else {
								dist_j_tmp = dist_j_tmp_p;
							}
							if (fabs(dist_k_tmp_m) == fabs(dist_k_tmp_p) && dist_k_tmp_m < 999) {
								dist_k_tmp = 0;
								bool enough = true;
								for (int Dist_k = 0; Dist_k <= 1 && enough; Dist_k++) {
									if (!(k - dist_k_tmp_p + Dist_k < klast && (!vector(stalled_7, i, j, k - dist_k_tmp_p + Dist_k) || !vector(stalled_8, i, j, k - dist_k_tmp_p + Dist_k)))) {
										enough = false;
									}
								}
								if (enough) {
									dist_k_tmp = dist_k_tmp_p;
								}
								enough = true;
								for (int Dist_k = -1; Dist_k <= 0 && enough; Dist_k++) {
									if (!(k - dist_k_tmp_m + Dist_k >= 0 && (!vector(stalled_7, i, j, k - dist_k_tmp_m + Dist_k) || !vector(stalled_8, i, j, k - dist_k_tmp_m + Dist_k)))) {
										enough = false;
									}
								}
								if (enough) {
									dist_k_tmp = dist_k_tmp_m;
								}
							} else if (fabs(dist_k_tmp_m) < fabs(dist_k_tmp_p)) {
								dist_k_tmp = dist_k_tmp_m;
							} else {
								dist_k_tmp = dist_k_tmp_p;
							}
							if (dist_i_tmp == 999 && dist_j_tmp == 999 && dist_k_tmp == 999) {
								dist_i_tmp = dist_i_tmp_r;
								dist_j_tmp = dist_j_tmp_r;
								dist_k_tmp = dist_k_tmp_r;
							}
							if (dist_i_tmp != 999 && ((vector(d_i_Ey, i, j, k) == 0) || (fabs(dist_i_tmp) < fabs(vector(d_i_Ey, i, j, k))))) {
								vector(d_i_Ey, i, j, k) = dist_i_tmp;
							}
							if (dist_j_tmp != 999 && ((vector(d_j_Ey, i, j, k) == 0) || (fabs(dist_j_tmp) < fabs(vector(d_j_Ey, i, j, k))))) {
								vector(d_j_Ey, i, j, k) = dist_j_tmp;
							}
							if (dist_k_tmp != 999 && ((vector(d_k_Ey, i, j, k) == 0) || (fabs(dist_k_tmp) < fabs(vector(d_k_Ey, i, j, k))))) {
								vector(d_k_Ey, i, j, k) = dist_k_tmp;
							}
						}
					}
				}
			}
		}
		d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);
	}
}


/*
 * Checks if the point has to be stalled
 */
bool Problem::checkStalled(std::shared_ptr< hier::Patch > patch, int i, int j, int k, int v) const {
	double* FOV = ((pdat::NodeData<double> *) patch->getPatchData(v).get())->getPointer();
	double* interior = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_id).get())->getPointer();
	//Get the dimensions of the patch
	const hier::Index boxfirst = patch->getBox().lower();
	const hier::Index boxlast  = patch->getBox().upper();
	//Auxiliary definitions
	int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
	int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
	int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;

	int stencilAcc, stencilAccMax_i, stencilAccMax_j, stencilAccMax_k;
	bool notEnoughStencil = false;
	int FOV_threshold = 0;
	if (vector(FOV, i, j, k) <= FOV_threshold) {
		notEnoughStencil = true;
	} else {
		stencilAcc = 0;
		stencilAccMax_i = 0;
		for (int it1 = MAX(i-d_regionMinThickness, 0); it1 <= MIN(i+d_regionMinThickness, ilast); it1++) {
			if (vector(FOV, it1, j, k) > FOV_threshold) {
				stencilAcc++;
			} else {
				stencilAccMax_i = MAX(stencilAccMax_i, stencilAcc);
				stencilAcc = 0;
			}
		}
		stencilAccMax_i = MAX(stencilAccMax_i, stencilAcc);
		stencilAcc = 0;
		stencilAccMax_j = 0;
		for (int jt1 = MAX(j-d_regionMinThickness, 0); jt1 <= MIN(j+d_regionMinThickness, jlast); jt1++) {
			if (vector(FOV, i, jt1, k) > FOV_threshold) {
				stencilAcc++;
			} else {
				stencilAccMax_j = MAX(stencilAccMax_j, stencilAcc);
				stencilAcc = 0;
			}
		}
		stencilAccMax_j = MAX(stencilAccMax_j, stencilAcc);
		stencilAcc = 0;
		stencilAccMax_k = 0;
		for (int kt1 = MAX(k-d_regionMinThickness, 0); kt1 <= MIN(k+d_regionMinThickness, klast); kt1++) {
			if (vector(FOV, i, j, kt1) > FOV_threshold) {
				stencilAcc++;
			} else {
				stencilAccMax_k = MAX(stencilAccMax_k, stencilAcc);
				stencilAcc = 0;
			}
		}
		stencilAccMax_k = MAX(stencilAccMax_k, stencilAcc);
		if ((stencilAccMax_i < d_regionMinThickness) || (stencilAccMax_j < d_regionMinThickness) || (stencilAccMax_k < d_regionMinThickness)) {
			notEnoughStencil = true;
		}
	}
	return notEnoughStencil;
}






/*
 * Initialize data on a patch. This initialization is done only at the begining of the simulation.
 */
void Problem::initializeDataOnPatch(hier::Patch& patch, 
                                    const double time,
                                    const bool initial_time)
{
	(void) time;
   	if (initial_time) {
		// Initial conditions		
		//Get fields, auxiliary fields and local variables that are going to be used.
		double* Bx = ((pdat::NodeData<double> *) patch.getPatchData(d_Bx_id).get())->getPointer();
		((pdat::NodeData<double> *) patch.getPatchData(d_Bx_id).get())->fillAll(0);
		double* By = ((pdat::NodeData<double> *) patch.getPatchData(d_By_id).get())->getPointer();
		((pdat::NodeData<double> *) patch.getPatchData(d_By_id).get())->fillAll(0);
		double* Bz = ((pdat::NodeData<double> *) patch.getPatchData(d_Bz_id).get())->getPointer();
		((pdat::NodeData<double> *) patch.getPatchData(d_Bz_id).get())->fillAll(0);
		double* Ex = ((pdat::NodeData<double> *) patch.getPatchData(d_Ex_id).get())->getPointer();
		((pdat::NodeData<double> *) patch.getPatchData(d_Ex_id).get())->fillAll(0);
		double* Ey = ((pdat::NodeData<double> *) patch.getPatchData(d_Ey_id).get())->getPointer();
		((pdat::NodeData<double> *) patch.getPatchData(d_Ey_id).get())->fillAll(0);
		double* Ez = ((pdat::NodeData<double> *) patch.getPatchData(d_Ez_id).get())->getPointer();
		((pdat::NodeData<double> *) patch.getPatchData(d_Ez_id).get())->fillAll(0);
		double* q = ((pdat::NodeData<double> *) patch.getPatchData(d_q_id).get())->getPointer();
		((pdat::NodeData<double> *) patch.getPatchData(d_q_id).get())->fillAll(0);
		double* Jx = ((pdat::NodeData<double> *) patch.getPatchData(d_Jx_id).get())->getPointer();
		((pdat::NodeData<double> *) patch.getPatchData(d_Jx_id).get())->fillAll(0);
		double* Jy = ((pdat::NodeData<double> *) patch.getPatchData(d_Jy_id).get())->getPointer();
		((pdat::NodeData<double> *) patch.getPatchData(d_Jy_id).get())->fillAll(0);
		double* Jz = ((pdat::NodeData<double> *) patch.getPatchData(d_Jz_id).get())->getPointer();
		((pdat::NodeData<double> *) patch.getPatchData(d_Jz_id).get())->fillAll(0);
		
		//Get the dimensions of the patch
		hier::Box pbox = patch.getBox();
		double* FOV_1 = ((pdat::NodeData<double> *) patch.getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_2 = ((pdat::NodeData<double> *) patch.getPatchData(d_FOV_2_id).get())->getPointer();
		double* FOV_4 = ((pdat::NodeData<double> *) patch.getPatchData(d_FOV_4_id).get())->getPointer();
		double* FOV_5 = ((pdat::NodeData<double> *) patch.getPatchData(d_FOV_5_id).get())->getPointer();
		double* FOV_7 = ((pdat::NodeData<double> *) patch.getPatchData(d_FOV_7_id).get())->getPointer();
		double* FOV_8 = ((pdat::NodeData<double> *) patch.getPatchData(d_FOV_8_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch.getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch.getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch.getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch.getPatchData(d_FOV_yUpper_id).get())->getPointer();
		double* FOV_zLower = ((pdat::NodeData<double> *) patch.getPatchData(d_FOV_zLower_id).get())->getPointer();
		double* FOV_zUpper = ((pdat::NodeData<double> *) patch.getPatchData(d_FOV_zUpper_id).get())->getPointer();
		const hier::Index boxfirst = patch.getBox().lower();
		const hier::Index boxlast  = patch.getBox().upper();
		
		//Get delta spaces into an array. dx, dy, dz.
		const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch.getPatchGeometry()));
		const double* dx  = patch_geom->getDx();
		
		int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
		int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
		int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;
		for(int k = 0; k < klast; k++) {
			for(int j = 0; j < jlast; j++) {
				for(int i = 0; i < ilast; i++) {
					if (vector(FOV_7, i, j, k) > 0 || vector(FOV_8, i, j, k) > 0) {
						if ((lessEq((d_grid_geometry->getXLower()[0] + ((i + boxfirst(0)) - d_ghost_width) * dx[0]), 3.141592654) && greaterEq((d_grid_geometry->getXLower()[0] + ((i + boxfirst(0)) - d_ghost_width) * dx[0]), (-3.141592654))) && ((lessEq((d_grid_geometry->getXLower()[1] + ((j + boxfirst(1)) - d_ghost_width) * dx[1]), 3.141592654) && greaterEq((d_grid_geometry->getXLower()[1] + ((j + boxfirst(1)) - d_ghost_width) * dx[1]), (-3.141592654))) && (lessEq((d_grid_geometry->getXLower()[2] + ((k + boxfirst(2)) - d_ghost_width) * dx[2]), 3.141592654) && greaterEq((d_grid_geometry->getXLower()[2] + ((k + boxfirst(2)) - d_ghost_width) * dx[2]), (-3.141592654))))) {
							vector(Bx, i, j, k) = cos(d_grid_geometry->getXLower()[0] + ((i + boxfirst(0)) - d_ghost_width) * dx[0]) * cos(d_grid_geometry->getXLower()[1] + ((j + boxfirst(1)) - d_ghost_width) * dx[1]) * cos(d_grid_geometry->getXLower()[2] + ((k + boxfirst(2)) - d_ghost_width) * dx[2]);
							vector(By, i, j, k) = cos(d_grid_geometry->getXLower()[0] + ((i + boxfirst(0)) - d_ghost_width) * dx[0]) * cos(d_grid_geometry->getXLower()[1] + ((j + boxfirst(1)) - d_ghost_width) * dx[1]) * cos(d_grid_geometry->getXLower()[2] + ((k + boxfirst(2)) - d_ghost_width) * dx[2]);
							vector(Bz, i, j, k) = (sin(d_grid_geometry->getXLower()[0] + ((i + boxfirst(0)) - d_ghost_width) * dx[0]) * cos(d_grid_geometry->getXLower()[1] + ((j + boxfirst(1)) - d_ghost_width) * dx[1]) + cos(d_grid_geometry->getXLower()[0] + ((i + boxfirst(0)) - d_ghost_width) * dx[0]) * sin(d_grid_geometry->getXLower()[1] + ((j + boxfirst(1)) - d_ghost_width) * dx[1])) * sin(d_grid_geometry->getXLower()[2] + ((k + boxfirst(2)) - d_ghost_width) * dx[2]);
						}
		
						if ((greaterThan((d_grid_geometry->getXLower()[0] + ((i + boxfirst(0)) - d_ghost_width) * dx[0]), 3.141592654) || lessThan((d_grid_geometry->getXLower()[0] + ((i + boxfirst(0)) - d_ghost_width) * dx[0]), (-3.141592654))) || ((greaterThan((d_grid_geometry->getXLower()[1] + ((j + boxfirst(1)) - d_ghost_width) * dx[1]), 3.141592654) || lessThan((d_grid_geometry->getXLower()[1] + ((j + boxfirst(1)) - d_ghost_width) * dx[1]), (-3.141592654))) || (greaterThan((d_grid_geometry->getXLower()[2] + ((k + boxfirst(2)) - d_ghost_width) * dx[2]), 3.141592654) || lessThan((d_grid_geometry->getXLower()[2] + ((k + boxfirst(2)) - d_ghost_width) * dx[2]), (-3.141592654))))) {
							vector(Bx, i, j, k) = 0.0;
							vector(By, i, j, k) = 0.0;
							vector(Bz, i, j, k) = 0.0;
						}
		
						vector(Ex, i, j, k) = 0.0;
						vector(Ey, i, j, k) = 0.0;
						vector(Ez, i, j, k) = 0.0;
						vector(q, i, j, k) = 0.0;
						vector(Jx, i, j, k) = 0.0;
						vector(Jy, i, j, k) = 0.0;
						vector(Jz, i, j, k) = 0.0;
					}
		
					if (vector(FOV_5, i, j, k) > 0) {
						vector(Bx, i, j, k) = 0.0;
						vector(By, i, j, k) = 0.0;
						vector(Bz, i, j, k) = 0.0;
						vector(Ex, i, j, k) = 0.0;
						vector(Ey, i, j, k) = 0.0;
						vector(Ez, i, j, k) = 0.0;
						vector(q, i, j, k) = 0.0;
						vector(Jx, i, j, k) = 0.0;
						vector(Jy, i, j, k) = 0.0;
						vector(Jz, i, j, k) = 0.0;
					}
		
					if (vector(FOV_4, i, j, k) > 0) {
						vector(Bx, i, j, k) = 0.0;
						vector(By, i, j, k) = 0.0;
						vector(Bz, i, j, k) = 0.0;
						vector(Ex, i, j, k) = 0.0;
						vector(Ey, i, j, k) = 0.0;
						vector(Ez, i, j, k) = 0.0;
						vector(q, i, j, k) = 0.0;
						vector(Jx, i, j, k) = 0.0;
						vector(Jy, i, j, k) = 0.0;
						vector(Jz, i, j, k) = 0.0;
					}
		
					if (vector(FOV_1, i, j, k) > 0 || vector(FOV_2, i, j, k) > 0) {
						if ((lessEq((d_grid_geometry->getXLower()[0] + ((i + boxfirst(0)) - d_ghost_width) * dx[0]), 3.141592654) && greaterEq((d_grid_geometry->getXLower()[0] + ((i + boxfirst(0)) - d_ghost_width) * dx[0]), (-3.141592654))) && ((lessEq((d_grid_geometry->getXLower()[1] + ((j + boxfirst(1)) - d_ghost_width) * dx[1]), 3.141592654) && greaterEq((d_grid_geometry->getXLower()[1] + ((j + boxfirst(1)) - d_ghost_width) * dx[1]), (-3.141592654))) && (lessEq((d_grid_geometry->getXLower()[2] + ((k + boxfirst(2)) - d_ghost_width) * dx[2]), 3.141592654) && greaterEq((d_grid_geometry->getXLower()[2] + ((k + boxfirst(2)) - d_ghost_width) * dx[2]), (-3.141592654))))) {
							vector(Bx, i, j, k) = cos(d_grid_geometry->getXLower()[0] + ((i + boxfirst(0)) - d_ghost_width) * dx[0]) * cos(d_grid_geometry->getXLower()[1] + ((j + boxfirst(1)) - d_ghost_width) * dx[1]) * cos(d_grid_geometry->getXLower()[2] + ((k + boxfirst(2)) - d_ghost_width) * dx[2]);
							vector(By, i, j, k) = cos(d_grid_geometry->getXLower()[0] + ((i + boxfirst(0)) - d_ghost_width) * dx[0]) * cos(d_grid_geometry->getXLower()[1] + ((j + boxfirst(1)) - d_ghost_width) * dx[1]) * cos(d_grid_geometry->getXLower()[2] + ((k + boxfirst(2)) - d_ghost_width) * dx[2]);
							vector(Bz, i, j, k) = (sin(d_grid_geometry->getXLower()[0] + ((i + boxfirst(0)) - d_ghost_width) * dx[0]) * cos(d_grid_geometry->getXLower()[1] + ((j + boxfirst(1)) - d_ghost_width) * dx[1]) + cos(d_grid_geometry->getXLower()[0] + ((i + boxfirst(0)) - d_ghost_width) * dx[0]) * sin(d_grid_geometry->getXLower()[1] + ((j + boxfirst(1)) - d_ghost_width) * dx[1])) * sin(d_grid_geometry->getXLower()[2] + ((k + boxfirst(2)) - d_ghost_width) * dx[2]);
						}
		
						if ((greaterThan((d_grid_geometry->getXLower()[0] + ((i + boxfirst(0)) - d_ghost_width) * dx[0]), 3.141592654) || lessThan((d_grid_geometry->getXLower()[0] + ((i + boxfirst(0)) - d_ghost_width) * dx[0]), (-3.141592654))) || ((greaterThan((d_grid_geometry->getXLower()[1] + ((j + boxfirst(1)) - d_ghost_width) * dx[1]), 3.141592654) || lessThan((d_grid_geometry->getXLower()[1] + ((j + boxfirst(1)) - d_ghost_width) * dx[1]), (-3.141592654))) || (greaterThan((d_grid_geometry->getXLower()[2] + ((k + boxfirst(2)) - d_ghost_width) * dx[2]), 3.141592654) || lessThan((d_grid_geometry->getXLower()[2] + ((k + boxfirst(2)) - d_ghost_width) * dx[2]), (-3.141592654))))) {
							vector(Bx, i, j, k) = 0.0;
							vector(By, i, j, k) = 0.0;
							vector(Bz, i, j, k) = 0.0;
						}
		
						vector(Ex, i, j, k) = 0.0;
						vector(Ey, i, j, k) = 0.0;
						vector(Ez, i, j, k) = 0.0;
						vector(q, i, j, k) = 0.0;
						vector(Jx, i, j, k) = 0.0;
						vector(Jy, i, j, k) = 0.0;
						vector(Jz, i, j, k) = 0.0;
					}
		
					//Boundaries Initialization
				}
			}
		}
		
		

   	}
}

/*
 * Gets the coarser patch that contains the box
 */
const std::shared_ptr<hier::Patch >& Problem::getCoarserPatch(
	const std::shared_ptr< hier::PatchLevel >& level,
	const hier::Box interior, 
	const hier::IntVector ratio)
{
	const hier::Box& coarsenBox = hier::Box::coarsen(interior, ratio);
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr< hier::Patch >& patch = *p_it;
		const hier::Box& interior_C = patch->getBox();
		if (interior_C.intersects(coarsenBox)) {
			return patch;		
		}
	}
}

/*
 * Reset the hierarchy-dependent internal information.
 */
void Problem::resetHierarchyConfiguration (
   const std::shared_ptr<hier::PatchHierarchy >& new_hierarchy ,
   int coarsest_level ,
   int finest_level )
{
	int finest_hiera_level = new_hierarchy->getFinestLevelNumber();

   	//  If we have added or removed a level, resize the schedule arrays

	d_bdry_sched_advance1.resize(finest_hiera_level+1);
	d_bdry_sched_advance4.resize(finest_hiera_level+1);
	d_bdry_sched_advance6.resize(finest_hiera_level+1);
	d_bdry_sched_advance7.resize(finest_hiera_level+1);
	d_coarsen_schedule.resize(finest_hiera_level+1);
	d_bdry_sched_postCoarsen.resize(finest_hiera_level+1);
	d_sim_substep.resize(finest_hiera_level+1);
	//  Build coarsen and refine communication schedules.
	for (int ln = coarsest_level; ln <= finest_hiera_level; ln++) {
		std::shared_ptr< hier::PatchLevel > level(new_hierarchy->getPatchLevel(ln));
		d_bdry_sched_advance1[ln] = d_bdry_fill_advance1->createSchedule(level,ln-1,new_hierarchy,this);
		d_bdry_sched_advance4[ln] = d_bdry_fill_advance4->createSchedule(level,ln-1,new_hierarchy,this);
		d_bdry_sched_advance6[ln] = d_bdry_fill_advance6->createSchedule(level,ln-1,new_hierarchy,this);
		d_bdry_sched_advance7[ln] = d_bdry_fill_advance7->createSchedule(level,ln-1,new_hierarchy,this);
		d_bdry_sched_postCoarsen[ln] = d_bdry_fill_init->createSchedule(level);
		// coarsen schedule only for levels > 0
		if (ln > 0) {
			std::shared_ptr< hier::PatchLevel > coarser_level(new_hierarchy->getPatchLevel(ln-1));
			d_coarsen_schedule[ln] = d_coarsen_algorithm->createSchedule(coarser_level, level, NULL);
		}
	}

}




/*
 * This method sets the physical boundary conditions.
 */
void Problem::setPhysicalBoundaryConditions(
   hier::Patch& patch,
   const double fill_time,
   const hier::IntVector& ghost_width_to_fill)
{
	//Boundary must not be implemented in this method
}

/*
 * Set up external plotter to plot internal data from this class.  
 * Register variables appropriate for plotting.                       
 */
int Problem::setupPlotter(
  appu::VisItDataWriter &plotter
) const {
	if (!d_patch_hierarchy) {
		TBOX_ERROR(d_object_name << ": No hierarchy in\n"
			<< " Problem::setupPlotter\n"
                 	<< "The hierarchy must be set before calling\n"
                 	<< "this function.\n");
   	}
	plotter.registerPlotQuantity("FOV_1","SCALAR",d_FOV_1_id);
	plotter.registerPlotQuantity("FOV_2","SCALAR",d_FOV_2_id);
	plotter.registerPlotQuantity("FOV_4","SCALAR",d_FOV_4_id);
	plotter.registerPlotQuantity("FOV_5","SCALAR",d_FOV_5_id);
	plotter.registerPlotQuantity("FOV_7","SCALAR",d_FOV_7_id);
	plotter.registerPlotQuantity("FOV_8","SCALAR",d_FOV_8_id);
	hier::VariableDatabase *vdb = hier::VariableDatabase::getDatabase();
	for (vector<string>::const_iterator it = d_full_writer_variables.begin() ; it != d_full_writer_variables.end(); ++it) {
		string var_to_register = *it;
		if (!(vdb->checkVariableExists(var_to_register))) {
			TBOX_ERROR(d_object_name << ": Variable selected for 3D write not found:" <<  var_to_register);
		}
		int var_id = vdb->getVariable(var_to_register)->getInstanceIdentifier();
		plotter.registerPlotQuantity(var_to_register,"SCALAR",var_id);
	}


   	return 0;
}
/*
 * Set up external plotter to plot sliced data from this class.
 * Register variables appropriate for plotting.
 */
int Problem::setupSlicePlotter(vector<std::shared_ptr<SlicerDataWriter> > &plotters) const {
	if (!d_patch_hierarchy) {
	TBOX_ERROR(d_object_name << ": No hierarchy in\n"
		<< " Problem::setupSlicePlotter\n"
		<< "The hierarchy must be set before calling\n"
		<< "this function.\n");
	}
	int i = 0;
	for (vector<std::shared_ptr<SlicerDataWriter> >::const_iterator it = plotters.begin(); it != plotters.end(); ++it) {
		std::shared_ptr<SlicerDataWriter> plotter = *it;
		plotter->registerPlotQuantity("FOV_1","SCALAR",d_FOV_1_id);
		plotter->registerPlotQuantity("FOV_2","SCALAR",d_FOV_2_id);
		plotter->registerPlotQuantity("FOV_4","SCALAR",d_FOV_4_id);
		plotter->registerPlotQuantity("FOV_5","SCALAR",d_FOV_5_id);
		plotter->registerPlotQuantity("FOV_7","SCALAR",d_FOV_7_id);
		plotter->registerPlotQuantity("FOV_8","SCALAR",d_FOV_8_id);
		hier::VariableDatabase *vdb = hier::VariableDatabase::getDatabase();
		vector<string> variables = d_sliceVariables[i];
		for (vector<string>::const_iterator it2 = variables.begin() ; it2 != variables.end(); ++it2) {
			string var_to_register = *it2;
			if (!(vdb->checkVariableExists(var_to_register))) {
				TBOX_ERROR(d_object_name << ": Variable selected for Slice not found:" <<  var_to_register);
			}
			int var_id = vdb->getVariable(var_to_register)->getInstanceIdentifier();
			plotter->registerPlotQuantity(var_to_register,"SCALAR",var_id);
		}
		i++;
	}

	return 0;
}

/*
 * Set up external plotter to plot spherical data from this class.
 * Register variables appropriate for plotting.
 */
int Problem::setupSpherePlotter(vector<std::shared_ptr<SphereDataWriter> > &plotters) const {
	if (!d_patch_hierarchy) {
	TBOX_ERROR(d_object_name << ": No hierarchy in\n"
		<< " Problem::setupSpherePlotter\n"
		<< "The hierarchy must be set before calling\n"
		<< "this function.\n");
	}
	int i = 0;
	for (vector<std::shared_ptr<SphereDataWriter> >::const_iterator it = plotters.begin(); it != plotters.end(); ++it) {
		std::shared_ptr<SphereDataWriter> plotter = *it;
		plotter->registerPlotQuantity("FOV_1","SCALAR",d_FOV_1_id);
		plotter->registerPlotQuantity("FOV_2","SCALAR",d_FOV_2_id);
		plotter->registerPlotQuantity("FOV_4","SCALAR",d_FOV_4_id);
		plotter->registerPlotQuantity("FOV_5","SCALAR",d_FOV_5_id);
		plotter->registerPlotQuantity("FOV_7","SCALAR",d_FOV_7_id);
		plotter->registerPlotQuantity("FOV_8","SCALAR",d_FOV_8_id);
		hier::VariableDatabase *vdb = hier::VariableDatabase::getDatabase();
		vector<string> variables = d_sphereVariables[i];
		for (vector<string>::const_iterator it2 = variables.begin() ; it2 != variables.end(); ++it2) {
			string var_to_register = *it2;
			if (!(vdb->checkVariableExists(var_to_register))) {
				TBOX_ERROR(d_object_name << ": Variable selected for Sphere not found:" <<  var_to_register);
			}
			int var_id = vdb->getVariable(var_to_register)->getInstanceIdentifier();
			plotter->registerPlotQuantity(var_to_register,"SCALAR",var_id);
		}
		i++;
	}

	return 0;
}
/*
 * Set up external plotter to plot integration data from this class.
 * Register variables appropriate for plotting.
 */
int Problem::setupIntegralPlotter(vector<std::shared_ptr<IntegrateDataWriter> > &plotters) const {
	if (!d_patch_hierarchy) {
		TBOX_ERROR(d_object_name << ": No hierarchy inn"
		<< " Problem::setupIntegralPlottern"
		<< "The hierarchy must be set before callingn"
		<< "this function.n");
	}
	int i = 0;
	for (vector<std::shared_ptr<IntegrateDataWriter> >::const_iterator it = plotters.begin(); it != plotters.end(); ++it) {
		std::shared_ptr<IntegrateDataWriter> plotter = *it;
		hier::VariableDatabase *vdb = hier::VariableDatabase::getDatabase();
		vector<string> variables = d_integralVariables[i];
		for (vector<string>::const_iterator it2 = variables.begin() ; it2 != variables.end(); ++it2) {
			string var_to_register = *it2;
			if (!(vdb->checkVariableExists(var_to_register))) {
				TBOX_ERROR(d_object_name << ": Variable selected for Integration not found:" <<  var_to_register);
			}
			int var_id = vdb->getVariable(var_to_register)->getInstanceIdentifier();
			plotter->registerPlotQuantity(var_to_register,"SCALAR",var_id);
		}
		i++;
	}
	return 0;
}


/*
 * Perform a single step from the discretization schema algorithm   
 */
double Problem::advanceLevel(
   const std::shared_ptr<hier::PatchLevel>& level,
   const std::shared_ptr<hier::PatchHierarchy>& hierarchy,
   const double current_time,
   const double new_time,
   const bool first_step,
   const bool last_step,
   const bool regrid_advance)
{
	const int ln = level->getLevelNumber();
	// Calculation of number of current substep
	if (first_step) {
		d_sim_substep[ln] = d_patch_hierarchy->getRatioToCoarserLevel(ln).max();
	} else {
		d_sim_substep[ln] = d_sim_substep[ln] - 1;
	}

	const double simPlat_dt = new_time - current_time;
  const double level_ratio = level->getRatioToCoarserLevel().max();

	t_step->start();
  	// Shifting time
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch > patch = *p_it;
		std::shared_ptr< pdat::NodeData<double> > By(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_By_id)));
		std::shared_ptr< pdat::NodeData<double> > By_p(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_By_p_id)));
		std::shared_ptr< pdat::NodeData<double> > Bx(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_Bx_id)));
		std::shared_ptr< pdat::NodeData<double> > Bx_p(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_Bx_p_id)));
		std::shared_ptr< pdat::NodeData<double> > Jz(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_Jz_id)));
		std::shared_ptr< pdat::NodeData<double> > Jz_p(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_Jz_p_id)));
		std::shared_ptr< pdat::NodeData<double> > q(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_q_id)));
		std::shared_ptr< pdat::NodeData<double> > Ey(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_Ey_id)));
		std::shared_ptr< pdat::NodeData<double> > Ey_p(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_Ey_p_id)));
		std::shared_ptr< pdat::NodeData<double> > Ez(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_Ez_id)));
		std::shared_ptr< pdat::NodeData<double> > Ez_p(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_Ez_p_id)));
		std::shared_ptr< pdat::NodeData<double> > Jx(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_Jx_id)));
		std::shared_ptr< pdat::NodeData<double> > Jx_p(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_Jx_p_id)));
		std::shared_ptr< pdat::NodeData<double> > Jy(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_Jy_id)));
		std::shared_ptr< pdat::NodeData<double> > Jy_p(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_Jy_p_id)));
		std::shared_ptr< pdat::NodeData<double> > Bz(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_Bz_id)));
		std::shared_ptr< pdat::NodeData<double> > Bz_p(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_Bz_p_id)));
		std::shared_ptr< pdat::NodeData<double> > Ex(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_Ex_id)));
		std::shared_ptr< pdat::NodeData<double> > Ex_p(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_Ex_p_id)));

		const hier::Index boxfirst = patch->getBox().lower();
		const hier::Index boxlast  = patch->getBox().upper();
		Ex_p->copy(*Ex);
		Bz_p->copy(*Bz);
		Jy_p->copy(*Jy);
		Jx_p->copy(*Jx);
		Ez_p->copy(*Ez);
		Ey_p->copy(*Ey);
		Jz_p->copy(*Jz);
		Bx_p->copy(*Bx);
		By_p->copy(*By);
	}
  	// Evolution
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch >& patch = *p_it;
		double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_2 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_2_id).get())->getPointer();
		double* FOV_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_4_id).get())->getPointer();
		double* FOV_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_5_id).get())->getPointer();
		double* FOV_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_7_id).get())->getPointer();
		double* FOV_8 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_8_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
		double* FOV_zLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zLower_id).get())->getPointer();
		double* FOV_zUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zUpper_id).get())->getPointer();
	
		double* FluxSBjEx_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjEx_7_id).get())->getPointer();
		double* Bz_p = ((pdat::NodeData<double> *) patch->getPatchData(d_Bz_p_id).get())->getPointer();
		double* FluxSBkEx_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkEx_7_id).get())->getPointer();
		double* By_p = ((pdat::NodeData<double> *) patch->getPatchData(d_By_p_id).get())->getPointer();
		double* FluxSBiEy_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiEy_7_id).get())->getPointer();
		double* FluxSBkEy_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkEy_7_id).get())->getPointer();
		double* Bx_p = ((pdat::NodeData<double> *) patch->getPatchData(d_Bx_p_id).get())->getPointer();
		double* FluxSBiEz_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiEz_7_id).get())->getPointer();
		double* FluxSBjEz_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjEz_7_id).get())->getPointer();
		double* FluxSBjBx_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjBx_7_id).get())->getPointer();
		double* Ez_p = ((pdat::NodeData<double> *) patch->getPatchData(d_Ez_p_id).get())->getPointer();
		double* FluxSBkBx_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkBx_7_id).get())->getPointer();
		double* Ey_p = ((pdat::NodeData<double> *) patch->getPatchData(d_Ey_p_id).get())->getPointer();
		double* FluxSBiBy_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiBy_7_id).get())->getPointer();
		double* FluxSBkBy_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkBy_7_id).get())->getPointer();
		double* Ex_p = ((pdat::NodeData<double> *) patch->getPatchData(d_Ex_p_id).get())->getPointer();
		double* FluxSBiBz_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiBz_7_id).get())->getPointer();
		double* FluxSBjBz_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjBz_7_id).get())->getPointer();
		double* SpeedSBjEx_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjEx_7_id).get())->getPointer();
		double* SpeedSBkEx_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkEx_7_id).get())->getPointer();
		double* SpeedSBiEy_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiEy_7_id).get())->getPointer();
		double* SpeedSBkEy_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkEy_7_id).get())->getPointer();
		double* SpeedSBiEz_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiEz_7_id).get())->getPointer();
		double* SpeedSBjEz_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjEz_7_id).get())->getPointer();
		double* SpeedSBjBx_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjBx_7_id).get())->getPointer();
		double* SpeedSBkBx_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkBx_7_id).get())->getPointer();
		double* SpeedSBiBy_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiBy_7_id).get())->getPointer();
		double* SpeedSBkBy_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkBy_7_id).get())->getPointer();
		double* SpeedSBiBz_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiBz_7_id).get())->getPointer();
		double* SpeedSBjBz_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjBz_7_id).get())->getPointer();
		double* FluxSBjEx_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjEx_1_id).get())->getPointer();
		double* FluxSBkEx_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkEx_1_id).get())->getPointer();
		double* FluxSBiEy_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiEy_1_id).get())->getPointer();
		double* FluxSBkEy_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkEy_1_id).get())->getPointer();
		double* FluxSBiEz_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiEz_1_id).get())->getPointer();
		double* FluxSBjEz_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjEz_1_id).get())->getPointer();
		double* FluxSBjBx_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjBx_1_id).get())->getPointer();
		double* FluxSBkBx_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkBx_1_id).get())->getPointer();
		double* FluxSBiBy_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiBy_1_id).get())->getPointer();
		double* FluxSBkBy_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkBy_1_id).get())->getPointer();
		double* FluxSBiBz_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiBz_1_id).get())->getPointer();
		double* FluxSBjBz_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjBz_1_id).get())->getPointer();
		double* SpeedSBjEx_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjEx_1_id).get())->getPointer();
		double* SpeedSBkEx_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkEx_1_id).get())->getPointer();
		double* SpeedSBiEy_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiEy_1_id).get())->getPointer();
		double* SpeedSBkEy_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkEy_1_id).get())->getPointer();
		double* SpeedSBiEz_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiEz_1_id).get())->getPointer();
		double* SpeedSBjEz_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjEz_1_id).get())->getPointer();
		double* SpeedSBjBx_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjBx_1_id).get())->getPointer();
		double* SpeedSBkBx_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkBx_1_id).get())->getPointer();
		double* SpeedSBiBy_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiBy_1_id).get())->getPointer();
		double* SpeedSBkBy_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkBy_1_id).get())->getPointer();
		double* SpeedSBiBz_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiBz_1_id).get())->getPointer();
		double* SpeedSBjBz_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjBz_1_id).get())->getPointer();
		double* FluxSBjEx_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjEx_4_id).get())->getPointer();
		double* FluxSBkEx_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkEx_4_id).get())->getPointer();
		double* FluxSBiEy_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiEy_4_id).get())->getPointer();
		double* FluxSBkEy_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkEy_4_id).get())->getPointer();
		double* FluxSBiEz_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiEz_4_id).get())->getPointer();
		double* FluxSBjEz_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjEz_4_id).get())->getPointer();
		double* FluxSBjBx_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjBx_4_id).get())->getPointer();
		double* FluxSBkBx_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkBx_4_id).get())->getPointer();
		double* FluxSBiBy_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiBy_4_id).get())->getPointer();
		double* FluxSBkBy_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkBy_4_id).get())->getPointer();
		double* FluxSBiBz_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiBz_4_id).get())->getPointer();
		double* FluxSBjBz_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjBz_4_id).get())->getPointer();
		double* SpeedSBjEx_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjEx_4_id).get())->getPointer();
		double* SpeedSBkEx_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkEx_4_id).get())->getPointer();
		double* SpeedSBiEy_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiEy_4_id).get())->getPointer();
		double* SpeedSBkEy_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkEy_4_id).get())->getPointer();
		double* SpeedSBiEz_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiEz_4_id).get())->getPointer();
		double* SpeedSBjEz_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjEz_4_id).get())->getPointer();
		double* SpeedSBjBx_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjBx_4_id).get())->getPointer();
		double* SpeedSBkBx_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkBx_4_id).get())->getPointer();
		double* SpeedSBiBy_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiBy_4_id).get())->getPointer();
		double* SpeedSBkBy_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkBy_4_id).get())->getPointer();
		double* SpeedSBiBz_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiBz_4_id).get())->getPointer();
		double* SpeedSBjBz_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjBz_4_id).get())->getPointer();
		double* FluxSBjEx_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjEx_5_id).get())->getPointer();
		double* FluxSBkEx_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkEx_5_id).get())->getPointer();
		double* FluxSBiEy_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiEy_5_id).get())->getPointer();
		double* FluxSBkEy_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkEy_5_id).get())->getPointer();
		double* FluxSBiEz_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiEz_5_id).get())->getPointer();
		double* FluxSBjEz_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjEz_5_id).get())->getPointer();
		double* FluxSBjBx_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjBx_5_id).get())->getPointer();
		double* FluxSBkBx_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkBx_5_id).get())->getPointer();
		double* FluxSBiBy_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiBy_5_id).get())->getPointer();
		double* FluxSBkBy_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkBy_5_id).get())->getPointer();
		double* FluxSBiBz_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiBz_5_id).get())->getPointer();
		double* FluxSBjBz_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjBz_5_id).get())->getPointer();
		double* SpeedSBjEx_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjEx_5_id).get())->getPointer();
		double* SpeedSBkEx_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkEx_5_id).get())->getPointer();
		double* SpeedSBiEy_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiEy_5_id).get())->getPointer();
		double* SpeedSBkEy_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkEy_5_id).get())->getPointer();
		double* SpeedSBiEz_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiEz_5_id).get())->getPointer();
		double* SpeedSBjEz_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjEz_5_id).get())->getPointer();
		double* SpeedSBjBx_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjBx_5_id).get())->getPointer();
		double* SpeedSBkBx_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkBx_5_id).get())->getPointer();
		double* SpeedSBiBy_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiBy_5_id).get())->getPointer();
		double* SpeedSBkBy_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkBy_5_id).get())->getPointer();
		double* SpeedSBiBz_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiBz_5_id).get())->getPointer();
		double* SpeedSBjBz_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjBz_5_id).get())->getPointer();
	
		//Get the dimensions of the patch
		hier::Box pbox = patch->getBox();
		const hier::Index boxfirst = patch->getBox().lower();
		const hier::Index boxlast = patch->getBox().upper();
	
		//Get delta spaces into an array. dx, dy, dz.
		std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
		const double* dx  = patch_geom->getDx();
	
		//Auxiliary definitions
		int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
		int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
		int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;
		for(int k = 0; k < klast; k++) {
			for(int j = 0; j < jlast; j++) {
				for(int i = 0; i < ilast; i++) {
					if ((vector(FOV_7, i, j, k) > 0 || (((i + 1 < ilast && vector(FOV_7, i + 1, j, k) > 0) || (i + 2 < ilast && vector(FOV_7, i + 2, j, k) > 0) || (j + 1 < jlast && vector(FOV_7, i, j + 1, k) > 0) || (j + 2 < jlast && vector(FOV_7, i, j + 2, k) > 0) || (k + 1 < klast && vector(FOV_7, i, j, k + 1) > 0) || (k + 2 < klast && vector(FOV_7, i, j, k + 2) > 0)) || ((i - 1 >= 0 && vector(FOV_7, i - 1, j, k) > 0) || (i - 2 >= 0 && vector(FOV_7, i - 2, j, k) > 0) || (j - 1 >= 0 && vector(FOV_7, i, j - 1, k) > 0) || (j - 2 >= 0 && vector(FOV_7, i, j - 2, k) > 0) || (k - 1 >= 0 && vector(FOV_7, i, j, k - 1) > 0) || (k - 2 >= 0 && vector(FOV_7, i, j, k - 2) > 0))) || (d_tappering && (neighbourTappering(FOV_7, i, j, k, ilast, jlast, klast, 2, 6 * d_sim_substep[ln]))))) {
						vector(FluxSBjEx_7, i, j, k) = FjEx_AirI(vector(Bz_p, i, j, k), c, dx, simPlat_dt, ilast, jlast);
						vector(FluxSBkEx_7, i, j, k) = FkEx_AirI(vector(By_p, i, j, k), c, dx, simPlat_dt, ilast, jlast);
						vector(FluxSBiEy_7, i, j, k) = FiEy_AirI(vector(Bz_p, i, j, k), c, dx, simPlat_dt, ilast, jlast);
						vector(FluxSBkEy_7, i, j, k) = FkEy_AirI(vector(Bx_p, i, j, k), c, dx, simPlat_dt, ilast, jlast);
						vector(FluxSBiEz_7, i, j, k) = FiEz_AirI(vector(By_p, i, j, k), c, dx, simPlat_dt, ilast, jlast);
						vector(FluxSBjEz_7, i, j, k) = FjEz_AirI(vector(Bx_p, i, j, k), c, dx, simPlat_dt, ilast, jlast);
						vector(FluxSBjBx_7, i, j, k) = FjBx_AirI(vector(Ez_p, i, j, k), c, dx, simPlat_dt, ilast, jlast);
						vector(FluxSBkBx_7, i, j, k) = FkBx_AirI(vector(Ey_p, i, j, k), c, dx, simPlat_dt, ilast, jlast);
						vector(FluxSBiBy_7, i, j, k) = FiBy_AirI(vector(Ez_p, i, j, k), c, dx, simPlat_dt, ilast, jlast);
						vector(FluxSBkBy_7, i, j, k) = FkBy_AirI(vector(Ex_p, i, j, k), c, dx, simPlat_dt, ilast, jlast);
						vector(FluxSBiBz_7, i, j, k) = FiBz_AirI(vector(Ey_p, i, j, k), c, dx, simPlat_dt, ilast, jlast);
						vector(FluxSBjBz_7, i, j, k) = FjBz_AirI(vector(Ex_p, i, j, k), c, dx, simPlat_dt, ilast, jlast);
						vector(SpeedSBjEx_7, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBkEx_7, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBiEy_7, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBkEy_7, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBiEz_7, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBjEz_7, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBjBx_7, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBkBx_7, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBiBy_7, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBkBy_7, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBiBz_7, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBjBz_7, i, j, k) = MAX(0.0, MAX(c, -c));
					}
					if ((vector(FOV_5, i, j, k) > 0 || (((i + 1 < ilast && vector(FOV_5, i + 1, j, k) > 0) || (i + 2 < ilast && vector(FOV_5, i + 2, j, k) > 0) || (j + 1 < jlast && vector(FOV_5, i, j + 1, k) > 0) || (j + 2 < jlast && vector(FOV_5, i, j + 2, k) > 0) || (k + 1 < klast && vector(FOV_5, i, j, k + 1) > 0) || (k + 2 < klast && vector(FOV_5, i, j, k + 2) > 0)) || ((i - 1 >= 0 && vector(FOV_5, i - 1, j, k) > 0) || (i - 2 >= 0 && vector(FOV_5, i - 2, j, k) > 0) || (j - 1 >= 0 && vector(FOV_5, i, j - 1, k) > 0) || (j - 2 >= 0 && vector(FOV_5, i, j - 2, k) > 0) || (k - 1 >= 0 && vector(FOV_5, i, j, k - 1) > 0) || (k - 2 >= 0 && vector(FOV_5, i, j, k - 2) > 0))) || (d_tappering && (neighbourTappering(FOV_5, i, j, k, ilast, jlast, klast, 2, 6 * d_sim_substep[ln]))))) {
						vector(FluxSBjEx_5, i, j, k) = FjEx_OtherI(dx, simPlat_dt, ilast, jlast);
						vector(FluxSBkEx_5, i, j, k) = FkEx_OtherI(dx, simPlat_dt, ilast, jlast);
						vector(FluxSBiEy_5, i, j, k) = FiEy_OtherI(dx, simPlat_dt, ilast, jlast);
						vector(FluxSBkEy_5, i, j, k) = FkEy_OtherI(dx, simPlat_dt, ilast, jlast);
						vector(FluxSBiEz_5, i, j, k) = FiEz_OtherI(dx, simPlat_dt, ilast, jlast);
						vector(FluxSBjEz_5, i, j, k) = FjEz_OtherI(dx, simPlat_dt, ilast, jlast);
						vector(FluxSBjBx_5, i, j, k) = FjBx_OtherI(dx, simPlat_dt, ilast, jlast);
						vector(FluxSBkBx_5, i, j, k) = FkBx_OtherI(dx, simPlat_dt, ilast, jlast);
						vector(FluxSBiBy_5, i, j, k) = FiBy_OtherI(dx, simPlat_dt, ilast, jlast);
						vector(FluxSBkBy_5, i, j, k) = FkBy_OtherI(dx, simPlat_dt, ilast, jlast);
						vector(FluxSBiBz_5, i, j, k) = FiBz_OtherI(dx, simPlat_dt, ilast, jlast);
						vector(FluxSBjBz_5, i, j, k) = FjBz_OtherI(dx, simPlat_dt, ilast, jlast);
						vector(SpeedSBjEx_5, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBkEx_5, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBiEy_5, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBkEy_5, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBiEz_5, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBjEz_5, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBjBx_5, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBkBx_5, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBiBy_5, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBkBy_5, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBiBz_5, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBjBz_5, i, j, k) = MAX(0.0, MAX(c, -c));
					}
					if ((vector(FOV_4, i, j, k) > 0 || (((i + 1 < ilast && vector(FOV_4, i + 1, j, k) > 0) || (j + 1 < jlast && vector(FOV_4, i, j + 1, k) > 0) || (k + 1 < klast && vector(FOV_4, i, j, k + 1) > 0)) || ((i - 1 >= 0 && vector(FOV_4, i - 1, j, k) > 0) || (j - 1 >= 0 && vector(FOV_4, i, j - 1, k) > 0) || (k - 1 >= 0 && vector(FOV_4, i, j, k - 1) > 0))) || (d_tappering && (neighbourTappering(FOV_4, i, j, k, ilast, jlast, klast, 1, 5 * d_sim_substep[ln]))))) {
						vector(FluxSBjEx_4, i, j, k) = FjEx_SoilS(dx, simPlat_dt, ilast, jlast);
						vector(FluxSBkEx_4, i, j, k) = FkEx_SoilS(dx, simPlat_dt, ilast, jlast);
						vector(FluxSBiEy_4, i, j, k) = FiEy_SoilS(dx, simPlat_dt, ilast, jlast);
						vector(FluxSBkEy_4, i, j, k) = FkEy_SoilS(dx, simPlat_dt, ilast, jlast);
						vector(FluxSBiEz_4, i, j, k) = FiEz_SoilS(dx, simPlat_dt, ilast, jlast);
						vector(FluxSBjEz_4, i, j, k) = FjEz_SoilS(dx, simPlat_dt, ilast, jlast);
						vector(FluxSBjBx_4, i, j, k) = FjBx_SoilS(dx, simPlat_dt, ilast, jlast);
						vector(FluxSBkBx_4, i, j, k) = FkBx_SoilS(dx, simPlat_dt, ilast, jlast);
						vector(FluxSBiBy_4, i, j, k) = FiBy_SoilS(dx, simPlat_dt, ilast, jlast);
						vector(FluxSBkBy_4, i, j, k) = FkBy_SoilS(dx, simPlat_dt, ilast, jlast);
						vector(FluxSBiBz_4, i, j, k) = FiBz_SoilS(dx, simPlat_dt, ilast, jlast);
						vector(FluxSBjBz_4, i, j, k) = FjBz_SoilS(dx, simPlat_dt, ilast, jlast);
						vector(SpeedSBjEx_4, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBkEx_4, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBiEy_4, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBkEy_4, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBiEz_4, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBjEz_4, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBjBx_4, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBkBx_4, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBiBy_4, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBkBy_4, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBiBz_4, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBjBz_4, i, j, k) = MAX(0.0, MAX(c, -c));
					}
					if ((vector(FOV_1, i, j, k) > 0 || (((i + 1 < ilast && vector(FOV_1, i + 1, j, k) > 0) || (i + 2 < ilast && vector(FOV_1, i + 2, j, k) > 0) || (j + 1 < jlast && vector(FOV_1, i, j + 1, k) > 0) || (j + 2 < jlast && vector(FOV_1, i, j + 2, k) > 0) || (k + 1 < klast && vector(FOV_1, i, j, k + 1) > 0) || (k + 2 < klast && vector(FOV_1, i, j, k + 2) > 0)) || ((i - 1 >= 0 && vector(FOV_1, i - 1, j, k) > 0) || (i - 2 >= 0 && vector(FOV_1, i - 2, j, k) > 0) || (j - 1 >= 0 && vector(FOV_1, i, j - 1, k) > 0) || (j - 2 >= 0 && vector(FOV_1, i, j - 2, k) > 0) || (k - 1 >= 0 && vector(FOV_1, i, j, k - 1) > 0) || (k - 2 >= 0 && vector(FOV_1, i, j, k - 2) > 0))) || (d_tappering && (neighbourTappering(FOV_1, i, j, k, ilast, jlast, klast, 2, 2 * d_sim_substep[ln]))))) {
						vector(FluxSBjEx_1, i, j, k) = FjEx_AirI(vector(Bz_p, i, j, k), c, dx, simPlat_dt, ilast, jlast);
						vector(FluxSBkEx_1, i, j, k) = FkEx_AirI(vector(By_p, i, j, k), c, dx, simPlat_dt, ilast, jlast);
						vector(FluxSBiEy_1, i, j, k) = FiEy_AirI(vector(Bz_p, i, j, k), c, dx, simPlat_dt, ilast, jlast);
						vector(FluxSBkEy_1, i, j, k) = FkEy_AirI(vector(Bx_p, i, j, k), c, dx, simPlat_dt, ilast, jlast);
						vector(FluxSBiEz_1, i, j, k) = FiEz_AirI(vector(By_p, i, j, k), c, dx, simPlat_dt, ilast, jlast);
						vector(FluxSBjEz_1, i, j, k) = FjEz_AirI(vector(Bx_p, i, j, k), c, dx, simPlat_dt, ilast, jlast);
						vector(FluxSBjBx_1, i, j, k) = FjBx_AirI(vector(Ez_p, i, j, k), c, dx, simPlat_dt, ilast, jlast);
						vector(FluxSBkBx_1, i, j, k) = FkBx_AirI(vector(Ey_p, i, j, k), c, dx, simPlat_dt, ilast, jlast);
						vector(FluxSBiBy_1, i, j, k) = FiBy_AirI(vector(Ez_p, i, j, k), c, dx, simPlat_dt, ilast, jlast);
						vector(FluxSBkBy_1, i, j, k) = FkBy_AirI(vector(Ex_p, i, j, k), c, dx, simPlat_dt, ilast, jlast);
						vector(FluxSBiBz_1, i, j, k) = FiBz_AirI(vector(Ey_p, i, j, k), c, dx, simPlat_dt, ilast, jlast);
						vector(FluxSBjBz_1, i, j, k) = FjBz_AirI(vector(Ex_p, i, j, k), c, dx, simPlat_dt, ilast, jlast);
						vector(SpeedSBjEx_1, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBkEx_1, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBiEy_1, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBkEy_1, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBiEz_1, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBjEz_1, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBjBx_1, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBkBx_1, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBiBy_1, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBkBy_1, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBiBz_1, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBjBz_1, i, j, k) = MAX(0.0, MAX(c, -c));
					}
				}
			}
		}
	}
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch >& patch = *p_it;
		double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_2 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_2_id).get())->getPointer();
		double* FOV_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_4_id).get())->getPointer();
		double* FOV_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_5_id).get())->getPointer();
		double* FOV_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_7_id).get())->getPointer();
		double* FOV_8 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_8_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
		double* FOV_zLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zLower_id).get())->getPointer();
		double* FOV_zUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zUpper_id).get())->getPointer();
	
		double* Jx_p = ((pdat::NodeData<double> *) patch->getPatchData(d_Jx_p_id).get())->getPointer();
		double* Jy_p = ((pdat::NodeData<double> *) patch->getPatchData(d_Jy_p_id).get())->getPointer();
		double* Jz_p = ((pdat::NodeData<double> *) patch->getPatchData(d_Jz_p_id).get())->getPointer();
		double* Ex_p = ((pdat::NodeData<double> *) patch->getPatchData(d_Ex_p_id).get())->getPointer();
		double* FluxSBjEx_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjEx_7_id).get())->getPointer();
		double* SpeedSBjEx_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjEx_7_id).get())->getPointer();
		double* FluxSBkEx_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkEx_7_id).get())->getPointer();
		double* SpeedSBkEx_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkEx_7_id).get())->getPointer();
		double* Ey_p = ((pdat::NodeData<double> *) patch->getPatchData(d_Ey_p_id).get())->getPointer();
		double* FluxSBiEy_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiEy_7_id).get())->getPointer();
		double* SpeedSBiEy_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiEy_7_id).get())->getPointer();
		double* FluxSBkEy_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkEy_7_id).get())->getPointer();
		double* SpeedSBkEy_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkEy_7_id).get())->getPointer();
		double* Ez_p = ((pdat::NodeData<double> *) patch->getPatchData(d_Ez_p_id).get())->getPointer();
		double* FluxSBiEz_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiEz_7_id).get())->getPointer();
		double* SpeedSBiEz_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiEz_7_id).get())->getPointer();
		double* FluxSBjEz_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjEz_7_id).get())->getPointer();
		double* SpeedSBjEz_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjEz_7_id).get())->getPointer();
		double* Bx_p = ((pdat::NodeData<double> *) patch->getPatchData(d_Bx_p_id).get())->getPointer();
		double* FluxSBjBx_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjBx_7_id).get())->getPointer();
		double* SpeedSBjBx_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjBx_7_id).get())->getPointer();
		double* FluxSBkBx_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkBx_7_id).get())->getPointer();
		double* SpeedSBkBx_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkBx_7_id).get())->getPointer();
		double* By_p = ((pdat::NodeData<double> *) patch->getPatchData(d_By_p_id).get())->getPointer();
		double* FluxSBiBy_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiBy_7_id).get())->getPointer();
		double* SpeedSBiBy_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiBy_7_id).get())->getPointer();
		double* FluxSBkBy_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkBy_7_id).get())->getPointer();
		double* SpeedSBkBy_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkBy_7_id).get())->getPointer();
		double* Bz_p = ((pdat::NodeData<double> *) patch->getPatchData(d_Bz_p_id).get())->getPointer();
		double* FluxSBiBz_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiBz_7_id).get())->getPointer();
		double* SpeedSBiBz_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiBz_7_id).get())->getPointer();
		double* FluxSBjBz_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjBz_7_id).get())->getPointer();
		double* SpeedSBjBz_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjBz_7_id).get())->getPointer();
		double* K1SBEx_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_K1SBEx_7_id).get())->getPointer();
		double* K1SBEy_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_K1SBEy_7_id).get())->getPointer();
		double* K1SBEz_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_K1SBEz_7_id).get())->getPointer();
		double* K1SBBx_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_K1SBBx_7_id).get())->getPointer();
		double* K1SBBy_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_K1SBBy_7_id).get())->getPointer();
		double* K1SBBz_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_K1SBBz_7_id).get())->getPointer();
		double* ExSPprime_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_ExSPprime_7_id).get())->getPointer();
		double* EySPprime_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_EySPprime_7_id).get())->getPointer();
		double* EzSPprime_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_EzSPprime_7_id).get())->getPointer();
		double* BxSPprime_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_BxSPprime_7_id).get())->getPointer();
		double* BySPprime_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_BySPprime_7_id).get())->getPointer();
		double* BzSPprime_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_BzSPprime_7_id).get())->getPointer();
		double* FluxSBjEx_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjEx_1_id).get())->getPointer();
		double* SpeedSBjEx_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjEx_1_id).get())->getPointer();
		double* FluxSBkEx_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkEx_1_id).get())->getPointer();
		double* SpeedSBkEx_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkEx_1_id).get())->getPointer();
		double* FluxSBiEy_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiEy_1_id).get())->getPointer();
		double* SpeedSBiEy_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiEy_1_id).get())->getPointer();
		double* FluxSBkEy_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkEy_1_id).get())->getPointer();
		double* SpeedSBkEy_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkEy_1_id).get())->getPointer();
		double* FluxSBiEz_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiEz_1_id).get())->getPointer();
		double* SpeedSBiEz_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiEz_1_id).get())->getPointer();
		double* FluxSBjEz_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjEz_1_id).get())->getPointer();
		double* SpeedSBjEz_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjEz_1_id).get())->getPointer();
		double* FluxSBjBx_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjBx_1_id).get())->getPointer();
		double* SpeedSBjBx_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjBx_1_id).get())->getPointer();
		double* FluxSBkBx_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkBx_1_id).get())->getPointer();
		double* SpeedSBkBx_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkBx_1_id).get())->getPointer();
		double* FluxSBiBy_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiBy_1_id).get())->getPointer();
		double* SpeedSBiBy_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiBy_1_id).get())->getPointer();
		double* FluxSBkBy_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkBy_1_id).get())->getPointer();
		double* SpeedSBkBy_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkBy_1_id).get())->getPointer();
		double* FluxSBiBz_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiBz_1_id).get())->getPointer();
		double* SpeedSBiBz_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiBz_1_id).get())->getPointer();
		double* FluxSBjBz_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjBz_1_id).get())->getPointer();
		double* SpeedSBjBz_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjBz_1_id).get())->getPointer();
		double* K1SBEx_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_K1SBEx_1_id).get())->getPointer();
		double* K1SBEy_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_K1SBEy_1_id).get())->getPointer();
		double* K1SBEz_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_K1SBEz_1_id).get())->getPointer();
		double* K1SBBx_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_K1SBBx_1_id).get())->getPointer();
		double* K1SBBy_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_K1SBBy_1_id).get())->getPointer();
		double* K1SBBz_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_K1SBBz_1_id).get())->getPointer();
		double* ExSPprime_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_ExSPprime_1_id).get())->getPointer();
		double* EySPprime_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_EySPprime_1_id).get())->getPointer();
		double* EzSPprime_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_EzSPprime_1_id).get())->getPointer();
		double* BxSPprime_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_BxSPprime_1_id).get())->getPointer();
		double* BySPprime_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_BySPprime_1_id).get())->getPointer();
		double* BzSPprime_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_BzSPprime_1_id).get())->getPointer();
		double* FluxSBjEx_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjEx_4_id).get())->getPointer();
		double* SpeedSBjEx_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjEx_4_id).get())->getPointer();
		double* FluxSBkEx_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkEx_4_id).get())->getPointer();
		double* SpeedSBkEx_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkEx_4_id).get())->getPointer();
		double* FluxSBiEy_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiEy_4_id).get())->getPointer();
		double* SpeedSBiEy_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiEy_4_id).get())->getPointer();
		double* FluxSBkEy_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkEy_4_id).get())->getPointer();
		double* SpeedSBkEy_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkEy_4_id).get())->getPointer();
		double* FluxSBiEz_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiEz_4_id).get())->getPointer();
		double* SpeedSBiEz_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiEz_4_id).get())->getPointer();
		double* FluxSBjEz_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjEz_4_id).get())->getPointer();
		double* SpeedSBjEz_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjEz_4_id).get())->getPointer();
		double* FluxSBjBx_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjBx_4_id).get())->getPointer();
		double* SpeedSBjBx_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjBx_4_id).get())->getPointer();
		double* FluxSBkBx_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkBx_4_id).get())->getPointer();
		double* SpeedSBkBx_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkBx_4_id).get())->getPointer();
		double* FluxSBiBy_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiBy_4_id).get())->getPointer();
		double* SpeedSBiBy_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiBy_4_id).get())->getPointer();
		double* FluxSBkBy_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkBy_4_id).get())->getPointer();
		double* SpeedSBkBy_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkBy_4_id).get())->getPointer();
		double* FluxSBiBz_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiBz_4_id).get())->getPointer();
		double* SpeedSBiBz_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiBz_4_id).get())->getPointer();
		double* FluxSBjBz_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjBz_4_id).get())->getPointer();
		double* SpeedSBjBz_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjBz_4_id).get())->getPointer();
		double* K1SBEx_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_K1SBEx_4_id).get())->getPointer();
		double* K1SBEy_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_K1SBEy_4_id).get())->getPointer();
		double* K1SBEz_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_K1SBEz_4_id).get())->getPointer();
		double* K1SBBx_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_K1SBBx_4_id).get())->getPointer();
		double* K1SBBy_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_K1SBBy_4_id).get())->getPointer();
		double* K1SBBz_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_K1SBBz_4_id).get())->getPointer();
		double* ExSPprime_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_ExSPprime_4_id).get())->getPointer();
		double* EySPprime_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_EySPprime_4_id).get())->getPointer();
		double* EzSPprime_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_EzSPprime_4_id).get())->getPointer();
		double* BxSPprime_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_BxSPprime_4_id).get())->getPointer();
		double* BySPprime_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_BySPprime_4_id).get())->getPointer();
		double* BzSPprime_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_BzSPprime_4_id).get())->getPointer();
		double* FluxSBjEx_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjEx_5_id).get())->getPointer();
		double* SpeedSBjEx_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjEx_5_id).get())->getPointer();
		double* FluxSBkEx_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkEx_5_id).get())->getPointer();
		double* SpeedSBkEx_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkEx_5_id).get())->getPointer();
		double* FluxSBiEy_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiEy_5_id).get())->getPointer();
		double* SpeedSBiEy_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiEy_5_id).get())->getPointer();
		double* FluxSBkEy_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkEy_5_id).get())->getPointer();
		double* SpeedSBkEy_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkEy_5_id).get())->getPointer();
		double* FluxSBiEz_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiEz_5_id).get())->getPointer();
		double* SpeedSBiEz_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiEz_5_id).get())->getPointer();
		double* FluxSBjEz_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjEz_5_id).get())->getPointer();
		double* SpeedSBjEz_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjEz_5_id).get())->getPointer();
		double* FluxSBjBx_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjBx_5_id).get())->getPointer();
		double* SpeedSBjBx_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjBx_5_id).get())->getPointer();
		double* FluxSBkBx_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkBx_5_id).get())->getPointer();
		double* SpeedSBkBx_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkBx_5_id).get())->getPointer();
		double* FluxSBiBy_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiBy_5_id).get())->getPointer();
		double* SpeedSBiBy_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiBy_5_id).get())->getPointer();
		double* FluxSBkBy_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkBy_5_id).get())->getPointer();
		double* SpeedSBkBy_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkBy_5_id).get())->getPointer();
		double* FluxSBiBz_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiBz_5_id).get())->getPointer();
		double* SpeedSBiBz_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiBz_5_id).get())->getPointer();
		double* FluxSBjBz_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjBz_5_id).get())->getPointer();
		double* SpeedSBjBz_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjBz_5_id).get())->getPointer();
		double* K1SBEx_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_K1SBEx_5_id).get())->getPointer();
		double* K1SBEy_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_K1SBEy_5_id).get())->getPointer();
		double* K1SBEz_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_K1SBEz_5_id).get())->getPointer();
		double* K1SBBx_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_K1SBBx_5_id).get())->getPointer();
		double* K1SBBy_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_K1SBBy_5_id).get())->getPointer();
		double* K1SBBz_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_K1SBBz_5_id).get())->getPointer();
		double* ExSPprime_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_ExSPprime_5_id).get())->getPointer();
		double* EySPprime_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_EySPprime_5_id).get())->getPointer();
		double* EzSPprime_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_EzSPprime_5_id).get())->getPointer();
		double* BxSPprime_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_BxSPprime_5_id).get())->getPointer();
		double* BySPprime_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_BySPprime_5_id).get())->getPointer();
		double* BzSPprime_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_BzSPprime_5_id).get())->getPointer();
		double fluxAccSBEx_7, fluxAccSBEy_7, fluxAccSBEz_7, fluxAccSBBx_7, fluxAccSBBy_7, fluxAccSBBz_7, fluxAccSBEx_1, fluxAccSBEy_1, fluxAccSBEz_1, fluxAccSBBx_1, fluxAccSBBy_1, fluxAccSBBz_1, fluxAccSBEx_4, fluxAccSBEy_4, fluxAccSBEz_4, fluxAccSBBx_4, fluxAccSBBy_4, fluxAccSBBz_4, fluxAccSBEx_5, fluxAccSBEy_5, fluxAccSBEz_5, fluxAccSBBx_5, fluxAccSBBy_5, fluxAccSBBz_5;
	
		//Get the dimensions of the patch
		hier::Box pbox = patch->getBox();
		const hier::Index boxfirst = patch->getBox().lower();
		const hier::Index boxlast = patch->getBox().upper();
	
		//Get delta spaces into an array. dx, dy, dz.
		std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
		const double* dx  = patch_geom->getDx();
	
		//Auxiliary definitions
		int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
		int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
		int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;
		for(int k = 0; k < klast; k++) {
			for(int j = 0; j < jlast; j++) {
				for(int i = 0; i < ilast; i++) {
					if ((vector(FOV_7, i, j, k) > 0) && (i + 2 < ilast && i - 2 >= 0 && j + 2 < jlast && j - 2 >= 0 && k + 2 < klast && k - 2 >= 0)) {
						fluxAccSBEx_7 = -mu * vector(Jx_p, i, j, k);
						fluxAccSBEy_7 = -mu * vector(Jy_p, i, j, k);
						fluxAccSBEz_7 = -mu * vector(Jz_p, i, j, k);
						fluxAccSBBx_7 = 0.0;
						fluxAccSBBy_7 = 0.0;
						fluxAccSBBz_7 = 0.0;
						fluxAccSBEx_7 = fluxAccSBEx_7 - FDOCj(Ex_p, FluxSBjEx_7, SpeedSBjEx_7, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBEx_7 = fluxAccSBEx_7 - FDOCk(Ex_p, FluxSBkEx_7, SpeedSBkEx_7, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBEy_7 = fluxAccSBEy_7 - FDOCi(Ey_p, FluxSBiEy_7, SpeedSBiEy_7, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBEy_7 = fluxAccSBEy_7 - FDOCk(Ey_p, FluxSBkEy_7, SpeedSBkEy_7, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBEz_7 = fluxAccSBEz_7 - FDOCi(Ez_p, FluxSBiEz_7, SpeedSBiEz_7, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBEz_7 = fluxAccSBEz_7 - FDOCj(Ez_p, FluxSBjEz_7, SpeedSBjEz_7, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBBx_7 = fluxAccSBBx_7 - FDOCj(Bx_p, FluxSBjBx_7, SpeedSBjBx_7, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBBx_7 = fluxAccSBBx_7 - FDOCk(Bx_p, FluxSBkBx_7, SpeedSBkBx_7, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBBy_7 = fluxAccSBBy_7 - FDOCi(By_p, FluxSBiBy_7, SpeedSBiBy_7, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBBy_7 = fluxAccSBBy_7 - FDOCk(By_p, FluxSBkBy_7, SpeedSBkBy_7, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBBz_7 = fluxAccSBBz_7 - FDOCi(Bz_p, FluxSBiBz_7, SpeedSBiBz_7, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBBz_7 = fluxAccSBBz_7 - FDOCj(Bz_p, FluxSBjBz_7, SpeedSBjBz_7, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(K1SBEx_7, i, j, k) = RK3(1, Ex_p, fluxAccSBEx_7, 0.0, 0.0, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(K1SBEy_7, i, j, k) = RK3(1, Ey_p, fluxAccSBEy_7, 0.0, 0.0, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(K1SBEz_7, i, j, k) = RK3(1, Ez_p, fluxAccSBEz_7, 0.0, 0.0, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(K1SBBx_7, i, j, k) = RK3(1, Bx_p, fluxAccSBBx_7, 0.0, 0.0, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(K1SBBy_7, i, j, k) = RK3(1, By_p, fluxAccSBBy_7, 0.0, 0.0, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(K1SBBz_7, i, j, k) = RK3(1, Bz_p, fluxAccSBBz_7, 0.0, 0.0, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(ExSPprime_7, i, j, k) = vector(Ex_p, i, j, k) + 1.0 / 3.0 * vector(K1SBEx_7, i, j, k);
						vector(EySPprime_7, i, j, k) = vector(Ey_p, i, j, k) + 1.0 / 3.0 * vector(K1SBEy_7, i, j, k);
						vector(EzSPprime_7, i, j, k) = vector(Ez_p, i, j, k) + 1.0 / 3.0 * vector(K1SBEz_7, i, j, k);
						vector(BxSPprime_7, i, j, k) = vector(Bx_p, i, j, k) + 1.0 / 3.0 * vector(K1SBBx_7, i, j, k);
						vector(BySPprime_7, i, j, k) = vector(By_p, i, j, k) + 1.0 / 3.0 * vector(K1SBBy_7, i, j, k);
						vector(BzSPprime_7, i, j, k) = vector(Bz_p, i, j, k) + 1.0 / 3.0 * vector(K1SBBz_7, i, j, k);
					}
					if ((vector(FOV_5, i, j, k) > 0) && (i + 2 < ilast && i - 2 >= 0 && j + 2 < jlast && j - 2 >= 0 && k + 2 < klast && k - 2 >= 0)) {
						fluxAccSBEx_5 = 0.0;
						fluxAccSBEy_5 = 0.0;
						fluxAccSBEz_5 = 0.0;
						fluxAccSBBx_5 = 0.0;
						fluxAccSBBy_5 = 0.0;
						fluxAccSBBz_5 = 0.0;
						fluxAccSBEx_5 = fluxAccSBEx_5 - FDOCj(Ex_p, FluxSBjEx_5, SpeedSBjEx_5, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBEx_5 = fluxAccSBEx_5 - FDOCk(Ex_p, FluxSBkEx_5, SpeedSBkEx_5, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBEy_5 = fluxAccSBEy_5 - FDOCi(Ey_p, FluxSBiEy_5, SpeedSBiEy_5, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBEy_5 = fluxAccSBEy_5 - FDOCk(Ey_p, FluxSBkEy_5, SpeedSBkEy_5, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBEz_5 = fluxAccSBEz_5 - FDOCi(Ez_p, FluxSBiEz_5, SpeedSBiEz_5, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBEz_5 = fluxAccSBEz_5 - FDOCj(Ez_p, FluxSBjEz_5, SpeedSBjEz_5, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBBx_5 = fluxAccSBBx_5 - FDOCj(Bx_p, FluxSBjBx_5, SpeedSBjBx_5, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBBx_5 = fluxAccSBBx_5 - FDOCk(Bx_p, FluxSBkBx_5, SpeedSBkBx_5, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBBy_5 = fluxAccSBBy_5 - FDOCi(By_p, FluxSBiBy_5, SpeedSBiBy_5, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBBy_5 = fluxAccSBBy_5 - FDOCk(By_p, FluxSBkBy_5, SpeedSBkBy_5, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBBz_5 = fluxAccSBBz_5 - FDOCi(Bz_p, FluxSBiBz_5, SpeedSBiBz_5, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBBz_5 = fluxAccSBBz_5 - FDOCj(Bz_p, FluxSBjBz_5, SpeedSBjBz_5, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(K1SBEx_5, i, j, k) = RK3(1, Ex_p, fluxAccSBEx_5, 0.0, 0.0, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(K1SBEy_5, i, j, k) = RK3(1, Ey_p, fluxAccSBEy_5, 0.0, 0.0, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(K1SBEz_5, i, j, k) = RK3(1, Ez_p, fluxAccSBEz_5, 0.0, 0.0, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(K1SBBx_5, i, j, k) = RK3(1, Bx_p, fluxAccSBBx_5, 0.0, 0.0, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(K1SBBy_5, i, j, k) = RK3(1, By_p, fluxAccSBBy_5, 0.0, 0.0, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(K1SBBz_5, i, j, k) = RK3(1, Bz_p, fluxAccSBBz_5, 0.0, 0.0, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(ExSPprime_5, i, j, k) = vector(Ex_p, i, j, k) + 1.0 / 3.0 * vector(K1SBEx_5, i, j, k);
						vector(EySPprime_5, i, j, k) = vector(Ey_p, i, j, k) + 1.0 / 3.0 * vector(K1SBEy_5, i, j, k);
						vector(EzSPprime_5, i, j, k) = vector(Ez_p, i, j, k) + 1.0 / 3.0 * vector(K1SBEz_5, i, j, k);
						vector(BxSPprime_5, i, j, k) = vector(Bx_p, i, j, k) + 1.0 / 3.0 * vector(K1SBBx_5, i, j, k);
						vector(BySPprime_5, i, j, k) = vector(By_p, i, j, k) + 1.0 / 3.0 * vector(K1SBBy_5, i, j, k);
						vector(BzSPprime_5, i, j, k) = vector(Bz_p, i, j, k) + 1.0 / 3.0 * vector(K1SBBz_5, i, j, k);
					}
					if ((vector(FOV_4, i, j, k) > 0) && (i + 1 < ilast && i - 1 >= 0 && j + 1 < jlast && j - 1 >= 0 && k + 1 < klast && k - 1 >= 0)) {
						fluxAccSBEx_4 = 0.0;
						fluxAccSBEy_4 = 0.0;
						fluxAccSBEz_4 = 0.0;
						fluxAccSBBx_4 = 0.0;
						fluxAccSBBy_4 = 0.0;
						fluxAccSBBz_4 = 0.0;
						fluxAccSBEx_4 = fluxAccSBEx_4 - FDOCj(Ex_p, FluxSBjEx_4, SpeedSBjEx_4, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBEx_4 = fluxAccSBEx_4 - FDOCk(Ex_p, FluxSBkEx_4, SpeedSBkEx_4, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBEy_4 = fluxAccSBEy_4 - FDOCi(Ey_p, FluxSBiEy_4, SpeedSBiEy_4, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBEy_4 = fluxAccSBEy_4 - FDOCk(Ey_p, FluxSBkEy_4, SpeedSBkEy_4, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBEz_4 = fluxAccSBEz_4 - FDOCi(Ez_p, FluxSBiEz_4, SpeedSBiEz_4, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBEz_4 = fluxAccSBEz_4 - FDOCj(Ez_p, FluxSBjEz_4, SpeedSBjEz_4, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBBx_4 = fluxAccSBBx_4 - FDOCj(Bx_p, FluxSBjBx_4, SpeedSBjBx_4, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBBx_4 = fluxAccSBBx_4 - FDOCk(Bx_p, FluxSBkBx_4, SpeedSBkBx_4, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBBy_4 = fluxAccSBBy_4 - FDOCi(By_p, FluxSBiBy_4, SpeedSBiBy_4, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBBy_4 = fluxAccSBBy_4 - FDOCk(By_p, FluxSBkBy_4, SpeedSBkBy_4, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBBz_4 = fluxAccSBBz_4 - FDOCi(Bz_p, FluxSBiBz_4, SpeedSBiBz_4, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBBz_4 = fluxAccSBBz_4 - FDOCj(Bz_p, FluxSBjBz_4, SpeedSBjBz_4, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(K1SBEx_4, i, j, k) = RK3(1, Ex_p, fluxAccSBEx_4, 0.0, 0.0, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(K1SBEy_4, i, j, k) = RK3(1, Ey_p, fluxAccSBEy_4, 0.0, 0.0, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(K1SBEz_4, i, j, k) = RK3(1, Ez_p, fluxAccSBEz_4, 0.0, 0.0, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(K1SBBx_4, i, j, k) = RK3(1, Bx_p, fluxAccSBBx_4, 0.0, 0.0, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(K1SBBy_4, i, j, k) = RK3(1, By_p, fluxAccSBBy_4, 0.0, 0.0, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(K1SBBz_4, i, j, k) = RK3(1, Bz_p, fluxAccSBBz_4, 0.0, 0.0, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(ExSPprime_4, i, j, k) = vector(Ex_p, i, j, k) + 1.0 / 3.0 * vector(K1SBEx_4, i, j, k);
						vector(EySPprime_4, i, j, k) = vector(Ey_p, i, j, k) + 1.0 / 3.0 * vector(K1SBEy_4, i, j, k);
						vector(EzSPprime_4, i, j, k) = vector(Ez_p, i, j, k) + 1.0 / 3.0 * vector(K1SBEz_4, i, j, k);
						vector(BxSPprime_4, i, j, k) = vector(Bx_p, i, j, k) + 1.0 / 3.0 * vector(K1SBBx_4, i, j, k);
						vector(BySPprime_4, i, j, k) = vector(By_p, i, j, k) + 1.0 / 3.0 * vector(K1SBBy_4, i, j, k);
						vector(BzSPprime_4, i, j, k) = vector(Bz_p, i, j, k) + 1.0 / 3.0 * vector(K1SBBz_4, i, j, k);
					}
					if ((vector(FOV_1, i, j, k) > 0) && (i + 2 < ilast && i - 2 >= 0 && j + 2 < jlast && j - 2 >= 0 && k + 2 < klast && k - 2 >= 0)) {
						fluxAccSBEx_1 = -mu * vector(Jx_p, i, j, k);
						fluxAccSBEy_1 = -mu * vector(Jy_p, i, j, k);
						fluxAccSBEz_1 = -mu * vector(Jz_p, i, j, k);
						fluxAccSBBx_1 = 0.0;
						fluxAccSBBy_1 = 0.0;
						fluxAccSBBz_1 = 0.0;
						fluxAccSBEx_1 = fluxAccSBEx_1 - FDOCj(Ex_p, FluxSBjEx_1, SpeedSBjEx_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBEx_1 = fluxAccSBEx_1 - FDOCk(Ex_p, FluxSBkEx_1, SpeedSBkEx_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBEy_1 = fluxAccSBEy_1 - FDOCi(Ey_p, FluxSBiEy_1, SpeedSBiEy_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBEy_1 = fluxAccSBEy_1 - FDOCk(Ey_p, FluxSBkEy_1, SpeedSBkEy_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBEz_1 = fluxAccSBEz_1 - FDOCi(Ez_p, FluxSBiEz_1, SpeedSBiEz_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBEz_1 = fluxAccSBEz_1 - FDOCj(Ez_p, FluxSBjEz_1, SpeedSBjEz_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBBx_1 = fluxAccSBBx_1 - FDOCj(Bx_p, FluxSBjBx_1, SpeedSBjBx_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBBx_1 = fluxAccSBBx_1 - FDOCk(Bx_p, FluxSBkBx_1, SpeedSBkBx_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBBy_1 = fluxAccSBBy_1 - FDOCi(By_p, FluxSBiBy_1, SpeedSBiBy_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBBy_1 = fluxAccSBBy_1 - FDOCk(By_p, FluxSBkBy_1, SpeedSBkBy_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBBz_1 = fluxAccSBBz_1 - FDOCi(Bz_p, FluxSBiBz_1, SpeedSBiBz_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBBz_1 = fluxAccSBBz_1 - FDOCj(Bz_p, FluxSBjBz_1, SpeedSBjBz_1, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(K1SBEx_1, i, j, k) = RK3(1, Ex_p, fluxAccSBEx_1, 0.0, 0.0, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(K1SBEy_1, i, j, k) = RK3(1, Ey_p, fluxAccSBEy_1, 0.0, 0.0, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(K1SBEz_1, i, j, k) = RK3(1, Ez_p, fluxAccSBEz_1, 0.0, 0.0, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(K1SBBx_1, i, j, k) = RK3(1, Bx_p, fluxAccSBBx_1, 0.0, 0.0, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(K1SBBy_1, i, j, k) = RK3(1, By_p, fluxAccSBBy_1, 0.0, 0.0, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(K1SBBz_1, i, j, k) = RK3(1, Bz_p, fluxAccSBBz_1, 0.0, 0.0, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(ExSPprime_1, i, j, k) = vector(Ex_p, i, j, k) + 1.0 / 3.0 * vector(K1SBEx_1, i, j, k);
						vector(EySPprime_1, i, j, k) = vector(Ey_p, i, j, k) + 1.0 / 3.0 * vector(K1SBEy_1, i, j, k);
						vector(EzSPprime_1, i, j, k) = vector(Ez_p, i, j, k) + 1.0 / 3.0 * vector(K1SBEz_1, i, j, k);
						vector(BxSPprime_1, i, j, k) = vector(Bx_p, i, j, k) + 1.0 / 3.0 * vector(K1SBBx_1, i, j, k);
						vector(BySPprime_1, i, j, k) = vector(By_p, i, j, k) + 1.0 / 3.0 * vector(K1SBBy_1, i, j, k);
						vector(BzSPprime_1, i, j, k) = vector(Bz_p, i, j, k) + 1.0 / 3.0 * vector(K1SBBz_1, i, j, k);
					}
				}
			}
		}
	}
	if (!d_tappering) {
		//Fill ghosts and periodical boundaries
		d_bdry_sched_advance1[ln]->fillData(current_time, false);
	}
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch >& patch = *p_it;
		double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_2 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_2_id).get())->getPointer();
		double* FOV_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_4_id).get())->getPointer();
		double* FOV_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_5_id).get())->getPointer();
		double* FOV_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_7_id).get())->getPointer();
		double* FOV_8 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_8_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
		double* FOV_zLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zLower_id).get())->getPointer();
		double* FOV_zUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zUpper_id).get())->getPointer();
	
		//Hard region field distance variables
		double* d_i_Ey = ((pdat::NodeData<double> *) patch->getPatchData(d_d_i_Ey_id).get())->getPointer();
		double* d_j_Ey = ((pdat::NodeData<double> *) patch->getPatchData(d_d_j_Ey_id).get())->getPointer();
		double* d_k_Ey = ((pdat::NodeData<double> *) patch->getPatchData(d_d_k_Ey_id).get())->getPointer();
		double* d_i_Ex = ((pdat::NodeData<double> *) patch->getPatchData(d_d_i_Ex_id).get())->getPointer();
		double* d_j_Ex = ((pdat::NodeData<double> *) patch->getPatchData(d_d_j_Ex_id).get())->getPointer();
		double* d_k_Ex = ((pdat::NodeData<double> *) patch->getPatchData(d_d_k_Ex_id).get())->getPointer();
	
		double* ExSPprime = ((pdat::NodeData<double> *) patch->getPatchData(d_ExSPprime_id).get())->getPointer();
		double* EySPprime = ((pdat::NodeData<double> *) patch->getPatchData(d_EySPprime_id).get())->getPointer();
		double* EzSPprime = ((pdat::NodeData<double> *) patch->getPatchData(d_EzSPprime_id).get())->getPointer();
		double* BxSPprime = ((pdat::NodeData<double> *) patch->getPatchData(d_BxSPprime_id).get())->getPointer();
		double* BySPprime = ((pdat::NodeData<double> *) patch->getPatchData(d_BySPprime_id).get())->getPointer();
		double* BzSPprime = ((pdat::NodeData<double> *) patch->getPatchData(d_BzSPprime_id).get())->getPointer();
		double* ExSPprime_2 = ((pdat::NodeData<double> *) patch->getPatchData(d_ExSPprime_2_id).get())->getPointer();
		double* EySPprime_2 = ((pdat::NodeData<double> *) patch->getPatchData(d_EySPprime_2_id).get())->getPointer();
		double* EzSPprime_2 = ((pdat::NodeData<double> *) patch->getPatchData(d_EzSPprime_2_id).get())->getPointer();
		double* BxSPprime_2 = ((pdat::NodeData<double> *) patch->getPatchData(d_BxSPprime_2_id).get())->getPointer();
		double* BySPprime_2 = ((pdat::NodeData<double> *) patch->getPatchData(d_BySPprime_2_id).get())->getPointer();
		double* BzSPprime_2 = ((pdat::NodeData<double> *) patch->getPatchData(d_BzSPprime_2_id).get())->getPointer();
		double* EySPprime_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_EySPprime_5_id).get())->getPointer();
	
		//Get the dimensions of the patch
		hier::Box pbox = patch->getBox();
		const hier::Index boxfirst = patch->getBox().lower();
		const hier::Index boxlast = patch->getBox().upper();
	
		//Get delta spaces into an array. dx, dy, dz.
		std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
		const double* dx  = patch_geom->getDx();
	
		//Auxiliary definitions
		int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
		int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
		int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;
		for(int k = 0; k < klast; k++) {
			for(int j = 0; j < jlast; j++) {
				for(int i = 0; i < ilast; i++) {
						vector(extrapolatedSBExSPi, i, j, k) = vector(ExSPprime, i, j, k);
						vector(extrapolatedSBExSPj, i, j, k) = vector(ExSPprime, i, j, k);
						vector(extrapolatedSBExSPk, i, j, k) = vector(ExSPprime, i, j, k);
						vector(extrapolatedSBEySPi, i, j, k) = vector(EySPprime, i, j, k);
						vector(extrapolatedSBEySPj, i, j, k) = vector(EySPprime, i, j, k);
						vector(extrapolatedSBEySPk, i, j, k) = vector(EySPprime, i, j, k);
						vector(extrapolatedSBEzSPi, i, j, k) = vector(EzSPprime, i, j, k);
						vector(extrapolatedSBEzSPj, i, j, k) = vector(EzSPprime, i, j, k);
						vector(extrapolatedSBEzSPk, i, j, k) = vector(EzSPprime, i, j, k);
						vector(extrapolatedSBBxSPi, i, j, k) = vector(BxSPprime, i, j, k);
						vector(extrapolatedSBBxSPj, i, j, k) = vector(BxSPprime, i, j, k);
						vector(extrapolatedSBBxSPk, i, j, k) = vector(BxSPprime, i, j, k);
						vector(extrapolatedSBBySPi, i, j, k) = vector(BySPprime, i, j, k);
						vector(extrapolatedSBBySPj, i, j, k) = vector(BySPprime, i, j, k);
						vector(extrapolatedSBBySPk, i, j, k) = vector(BySPprime, i, j, k);
						vector(extrapolatedSBBzSPi, i, j, k) = vector(BzSPprime, i, j, k);
						vector(extrapolatedSBBzSPj, i, j, k) = vector(BzSPprime, i, j, k);
						vector(extrapolatedSBBzSPk, i, j, k) = vector(BzSPprime, i, j, k);
						vector(extrapolatedSBqSPi, i, j, k) = vector(qSPprime, i, j, k);
						vector(extrapolatedSBqSPj, i, j, k) = vector(qSPprime, i, j, k);
						vector(extrapolatedSBqSPk, i, j, k) = vector(qSPprime, i, j, k);
						vector(extrapolatedSBJxSPi, i, j, k) = vector(JxSPprime, i, j, k);
						vector(extrapolatedSBJxSPj, i, j, k) = vector(JxSPprime, i, j, k);
						vector(extrapolatedSBJxSPk, i, j, k) = vector(JxSPprime, i, j, k);
						vector(extrapolatedSBJySPi, i, j, k) = vector(JySPprime, i, j, k);
						vector(extrapolatedSBJySPj, i, j, k) = vector(JySPprime, i, j, k);
						vector(extrapolatedSBJySPk, i, j, k) = vector(JySPprime, i, j, k);
						vector(extrapolatedSBJzSPi, i, j, k) = vector(JzSPprime, i, j, k);
						vector(extrapolatedSBJzSPj, i, j, k) = vector(JzSPprime, i, j, k);
						vector(extrapolatedSBJzSPk, i, j, k) = vector(JzSPprime, i, j, k);
				}
			}
		}
		for(int k = 0; k < klast; k++) {
			for(int j = 0; j < jlast; j++) {
				for(int i = 0; i < ilast; i++) {
					if ((vector(FOV_xLower, i, j, k) > 0) && ((i + 1 < ilast && (vector(FOV_7,i + 1, j, k) > 0 || vector(FOV_8, i + 1, j, k) > 0)) || (i + 2 < ilast && (vector(FOV_7,i + 2, j, k) > 0 || vector(FOV_8, i + 2, j, k) > 0)) || (i + 3 < ilast && (vector(FOV_7,i + 3, j, k) > 0 || vector(FOV_8, i + 3, j, k) > 0)))) {
						vector(ExSPprime, (d_ghost_width - 1) - i, j, k) = vector(ExSPprime, (d_ghost_width - 1) - i + 1, j, k);
						vector(EySPprime, (d_ghost_width - 1) - i, j, k) = vector(EySPprime, (d_ghost_width - 1) - i + 1, j, k);
						vector(EzSPprime, (d_ghost_width - 1) - i, j, k) = vector(EzSPprime, (d_ghost_width - 1) - i + 1, j, k);
						vector(BxSPprime, (d_ghost_width - 1) - i, j, k) = vector(BxSPprime, (d_ghost_width - 1) - i + 1, j, k);
						vector(BySPprime, (d_ghost_width - 1) - i, j, k) = vector(BySPprime, (d_ghost_width - 1) - i + 1, j, k);
						vector(BzSPprime, (d_ghost_width - 1) - i, j, k) = vector(BzSPprime, (d_ghost_width - 1) - i + 1, j, k);
					}
					if ((vector(FOV_xUpper, i, j, k) > 0) && ((i - 1 >= 0 && (vector(FOV_7, i - 1, j, k) > 0 || vector(FOV_8, i - 1, j, k) > 0)) || (i - 2 >= 0 && (vector(FOV_7, i - 2, j, k) > 0 || vector(FOV_8, i - 2, j, k) > 0)) || (i - 3 >= 0 && (vector(FOV_7, i - 3, j, k) > 0 || vector(FOV_8, i - 3, j, k) > 0)))) {
						vector(ExSPprime, i, j, k) = vector(ExSPprime, i - 1, j, k);
						vector(EySPprime, i, j, k) = vector(EySPprime, i - 1, j, k);
						vector(EzSPprime, i, j, k) = vector(EzSPprime, i - 1, j, k);
						vector(BxSPprime, i, j, k) = vector(BxSPprime, i - 1, j, k);
						vector(BySPprime, i, j, k) = vector(BySPprime, i - 1, j, k);
						vector(BzSPprime, i, j, k) = vector(BzSPprime, i - 1, j, k);
					}
					if ((vector(FOV_yLower, i, j, k) > 0) && ((j + 1 < jlast && (vector(FOV_7,i, j + 1, k) > 0 || vector(FOV_8, i, j + 1, k) > 0)) || (j + 2 < jlast && (vector(FOV_7,i, j + 2, k) > 0 || vector(FOV_8, i, j + 2, k) > 0)) || (j + 3 < jlast && (vector(FOV_7,i, j + 3, k) > 0 || vector(FOV_8, i, j + 3, k) > 0)))) {
						vector(ExSPprime, i, (d_ghost_width - 1) - j, k) = vector(ExSPprime, i, (d_ghost_width - 1) - j + 1, k);
						vector(EySPprime, i, (d_ghost_width - 1) - j, k) = vector(EySPprime, i, (d_ghost_width - 1) - j + 1, k);
						vector(EzSPprime, i, (d_ghost_width - 1) - j, k) = vector(EzSPprime, i, (d_ghost_width - 1) - j + 1, k);
						vector(BxSPprime, i, (d_ghost_width - 1) - j, k) = vector(BxSPprime, i, (d_ghost_width - 1) - j + 1, k);
						vector(BySPprime, i, (d_ghost_width - 1) - j, k) = vector(BySPprime, i, (d_ghost_width - 1) - j + 1, k);
						vector(BzSPprime, i, (d_ghost_width - 1) - j, k) = vector(BzSPprime, i, (d_ghost_width - 1) - j + 1, k);
					}
					if ((vector(FOV_yUpper, i, j, k) > 0) && ((j - 1 >= 0 && (vector(FOV_7, i, j - 1, k) > 0 || vector(FOV_8, i, j - 1, k) > 0)) || (j - 2 >= 0 && (vector(FOV_7, i, j - 2, k) > 0 || vector(FOV_8, i, j - 2, k) > 0)) || (j - 3 >= 0 && (vector(FOV_7, i, j - 3, k) > 0 || vector(FOV_8, i, j - 3, k) > 0)))) {
						vector(ExSPprime, i, j, k) = vector(ExSPprime, i, j - 1, k);
						vector(EySPprime, i, j, k) = vector(EySPprime, i, j - 1, k);
						vector(EzSPprime, i, j, k) = vector(EzSPprime, i, j - 1, k);
						vector(BxSPprime, i, j, k) = vector(BxSPprime, i, j - 1, k);
						vector(BySPprime, i, j, k) = vector(BySPprime, i, j - 1, k);
						vector(BzSPprime, i, j, k) = vector(BzSPprime, i, j - 1, k);
					}
					if ((vector(FOV_zLower, i, j, k) > 0) && ((k + 1 < klast && (vector(FOV_7,i, j, k + 1) > 0 || vector(FOV_8, i, j, k + 1) > 0)) || (k + 2 < klast && (vector(FOV_7,i, j, k + 2) > 0 || vector(FOV_8, i, j, k + 2) > 0)) || (k + 3 < klast && (vector(FOV_7,i, j, k + 3) > 0 || vector(FOV_8, i, j, k + 3) > 0)))) {
						vector(ExSPprime, i, j, (d_ghost_width - 1) - k) = vector(ExSPprime, i, j, (d_ghost_width - 1) - k + 1);
						vector(EySPprime, i, j, (d_ghost_width - 1) - k) = vector(EySPprime, i, j, (d_ghost_width - 1) - k + 1);
						vector(EzSPprime, i, j, (d_ghost_width - 1) - k) = vector(EzSPprime, i, j, (d_ghost_width - 1) - k + 1);
						vector(BxSPprime, i, j, (d_ghost_width - 1) - k) = vector(BxSPprime, i, j, (d_ghost_width - 1) - k + 1);
						vector(BySPprime, i, j, (d_ghost_width - 1) - k) = vector(BySPprime, i, j, (d_ghost_width - 1) - k + 1);
						vector(BzSPprime, i, j, (d_ghost_width - 1) - k) = vector(BzSPprime, i, j, (d_ghost_width - 1) - k + 1);
					}
					if ((vector(FOV_zUpper, i, j, k) > 0) && ((k - 1 >= 0 && (vector(FOV_7, i, j, k - 1) > 0 || vector(FOV_8, i, j, k - 1) > 0)) || (k - 2 >= 0 && (vector(FOV_7, i, j, k - 2) > 0 || vector(FOV_8, i, j, k - 2) > 0)) || (k - 3 >= 0 && (vector(FOV_7, i, j, k - 3) > 0 || vector(FOV_8, i, j, k - 3) > 0)))) {
						vector(ExSPprime, i, j, k) = vector(ExSPprime, i, j, k - 1);
						vector(EySPprime, i, j, k) = vector(EySPprime, i, j, k - 1);
						vector(EzSPprime, i, j, k) = vector(EzSPprime, i, j, k - 1);
						vector(BxSPprime, i, j, k) = vector(BxSPprime, i, j, k - 1);
						vector(BySPprime, i, j, k) = vector(BySPprime, i, j, k - 1);
						vector(BzSPprime, i, j, k) = vector(BzSPprime, i, j, k - 1);
					}
					if (vector(FOV_8, i, j, k) > 0) {
						//Region field extrapolations
						if ((vector(d_i_Ex, i, j, k) != 0 || vector(d_j_Ex, i, j, k) != 0 || vector(d_k_Ex, i, j, k) != 0)) {
							vector(ExSPprime, i, j, k) = 0.0;
						}
					}
					if (vector(FOV_5, i, j, k) > 0) {
						//Region field extrapolations
						if ((vector(d_i_Ey, i, j, k) != 0 || vector(d_j_Ey, i, j, k) != 0 || vector(d_k_Ey, i, j, k) != 0)) {
							vector(EySPprime_5, i, j, k) = 0.0;
						}
					}
					if ((vector(FOV_xLower, i, j, k) > 0) && ((i + 1 < ilast && (vector(FOV_1,i + 1, j, k) > 0 || vector(FOV_2, i + 1, j, k) > 0)) || (i + 2 < ilast && (vector(FOV_1,i + 2, j, k) > 0 || vector(FOV_2, i + 2, j, k) > 0)) || (i + 3 < ilast && (vector(FOV_1,i + 3, j, k) > 0 || vector(FOV_2, i + 3, j, k) > 0)))) {
						vector(ExSPprime_2, (d_ghost_width - 1) - i, j, k) = vector(ExSPprime_2, (d_ghost_width - 1) - i + 1, j, k);
						vector(EySPprime_2, (d_ghost_width - 1) - i, j, k) = vector(EySPprime_2, (d_ghost_width - 1) - i + 1, j, k);
						vector(EzSPprime_2, (d_ghost_width - 1) - i, j, k) = vector(EzSPprime_2, (d_ghost_width - 1) - i + 1, j, k);
						vector(BxSPprime_2, (d_ghost_width - 1) - i, j, k) = vector(BxSPprime_2, (d_ghost_width - 1) - i + 1, j, k);
						vector(BySPprime_2, (d_ghost_width - 1) - i, j, k) = vector(BySPprime_2, (d_ghost_width - 1) - i + 1, j, k);
						vector(BzSPprime_2, (d_ghost_width - 1) - i, j, k) = vector(BzSPprime_2, (d_ghost_width - 1) - i + 1, j, k);
					}
					if ((vector(FOV_xUpper, i, j, k) > 0) && ((i - 1 >= 0 && (vector(FOV_1, i - 1, j, k) > 0 || vector(FOV_2, i - 1, j, k) > 0)) || (i - 2 >= 0 && (vector(FOV_1, i - 2, j, k) > 0 || vector(FOV_2, i - 2, j, k) > 0)) || (i - 3 >= 0 && (vector(FOV_1, i - 3, j, k) > 0 || vector(FOV_2, i - 3, j, k) > 0)))) {
						vector(ExSPprime_2, i, j, k) = vector(ExSPprime_2, i - 1, j, k);
						vector(EySPprime_2, i, j, k) = vector(EySPprime_2, i - 1, j, k);
						vector(EzSPprime_2, i, j, k) = vector(EzSPprime_2, i - 1, j, k);
						vector(BxSPprime_2, i, j, k) = vector(BxSPprime_2, i - 1, j, k);
						vector(BySPprime_2, i, j, k) = vector(BySPprime_2, i - 1, j, k);
						vector(BzSPprime_2, i, j, k) = vector(BzSPprime_2, i - 1, j, k);
					}
					if ((vector(FOV_yLower, i, j, k) > 0) && ((j + 1 < jlast && (vector(FOV_1,i, j + 1, k) > 0 || vector(FOV_2, i, j + 1, k) > 0)) || (j + 2 < jlast && (vector(FOV_1,i, j + 2, k) > 0 || vector(FOV_2, i, j + 2, k) > 0)) || (j + 3 < jlast && (vector(FOV_1,i, j + 3, k) > 0 || vector(FOV_2, i, j + 3, k) > 0)))) {
						vector(ExSPprime_2, i, (d_ghost_width - 1) - j, k) = vector(ExSPprime_2, i, (d_ghost_width - 1) - j + 1, k);
						vector(EySPprime_2, i, (d_ghost_width - 1) - j, k) = vector(EySPprime_2, i, (d_ghost_width - 1) - j + 1, k);
						vector(EzSPprime_2, i, (d_ghost_width - 1) - j, k) = vector(EzSPprime_2, i, (d_ghost_width - 1) - j + 1, k);
						vector(BxSPprime_2, i, (d_ghost_width - 1) - j, k) = vector(BxSPprime_2, i, (d_ghost_width - 1) - j + 1, k);
						vector(BySPprime_2, i, (d_ghost_width - 1) - j, k) = vector(BySPprime_2, i, (d_ghost_width - 1) - j + 1, k);
						vector(BzSPprime_2, i, (d_ghost_width - 1) - j, k) = vector(BzSPprime_2, i, (d_ghost_width - 1) - j + 1, k);
					}
					if ((vector(FOV_yUpper, i, j, k) > 0) && ((j - 1 >= 0 && (vector(FOV_1, i, j - 1, k) > 0 || vector(FOV_2, i, j - 1, k) > 0)) || (j - 2 >= 0 && (vector(FOV_1, i, j - 2, k) > 0 || vector(FOV_2, i, j - 2, k) > 0)) || (j - 3 >= 0 && (vector(FOV_1, i, j - 3, k) > 0 || vector(FOV_2, i, j - 3, k) > 0)))) {
						vector(ExSPprime_2, i, j, k) = vector(ExSPprime_2, i, j - 1, k);
						vector(EySPprime_2, i, j, k) = vector(EySPprime_2, i, j - 1, k);
						vector(EzSPprime_2, i, j, k) = vector(EzSPprime_2, i, j - 1, k);
						vector(BxSPprime_2, i, j, k) = vector(BxSPprime_2, i, j - 1, k);
						vector(BySPprime_2, i, j, k) = vector(BySPprime_2, i, j - 1, k);
						vector(BzSPprime_2, i, j, k) = vector(BzSPprime_2, i, j - 1, k);
					}
					if ((vector(FOV_zLower, i, j, k) > 0) && ((k + 1 < klast && (vector(FOV_1,i, j, k + 1) > 0 || vector(FOV_2, i, j, k + 1) > 0)) || (k + 2 < klast && (vector(FOV_1,i, j, k + 2) > 0 || vector(FOV_2, i, j, k + 2) > 0)) || (k + 3 < klast && (vector(FOV_1,i, j, k + 3) > 0 || vector(FOV_2, i, j, k + 3) > 0)))) {
						vector(ExSPprime_2, i, j, (d_ghost_width - 1) - k) = vector(ExSPprime_2, i, j, (d_ghost_width - 1) - k + 1);
						vector(EySPprime_2, i, j, (d_ghost_width - 1) - k) = vector(EySPprime_2, i, j, (d_ghost_width - 1) - k + 1);
						vector(EzSPprime_2, i, j, (d_ghost_width - 1) - k) = vector(EzSPprime_2, i, j, (d_ghost_width - 1) - k + 1);
						vector(BxSPprime_2, i, j, (d_ghost_width - 1) - k) = vector(BxSPprime_2, i, j, (d_ghost_width - 1) - k + 1);
						vector(BySPprime_2, i, j, (d_ghost_width - 1) - k) = vector(BySPprime_2, i, j, (d_ghost_width - 1) - k + 1);
						vector(BzSPprime_2, i, j, (d_ghost_width - 1) - k) = vector(BzSPprime_2, i, j, (d_ghost_width - 1) - k + 1);
					}
					if ((vector(FOV_zUpper, i, j, k) > 0) && ((k - 1 >= 0 && (vector(FOV_1, i, j, k - 1) > 0 || vector(FOV_2, i, j, k - 1) > 0)) || (k - 2 >= 0 && (vector(FOV_1, i, j, k - 2) > 0 || vector(FOV_2, i, j, k - 2) > 0)) || (k - 3 >= 0 && (vector(FOV_1, i, j, k - 3) > 0 || vector(FOV_2, i, j, k - 3) > 0)))) {
						vector(ExSPprime_2, i, j, k) = vector(ExSPprime_2, i, j, k - 1);
						vector(EySPprime_2, i, j, k) = vector(EySPprime_2, i, j, k - 1);
						vector(EzSPprime_2, i, j, k) = vector(EzSPprime_2, i, j, k - 1);
						vector(BxSPprime_2, i, j, k) = vector(BxSPprime_2, i, j, k - 1);
						vector(BySPprime_2, i, j, k) = vector(BySPprime_2, i, j, k - 1);
						vector(BzSPprime_2, i, j, k) = vector(BzSPprime_2, i, j, k - 1);
					}
				}
			}
		}
		for(int k = 0; k < klast; k++) {
			for(int j = 0; j < jlast; j++) {
				for(int i = 0; i < ilast; i++) {
					if ((vector(FOV_xLower, i, j, k) > 0) && ((i + 1 < ilast && (vector(FOV_7,i + 1, j, k) > 0 || vector(FOV_8, i + 1, j, k) > 0)) || (i + 2 < ilast && (vector(FOV_7,i + 2, j, k) > 0 || vector(FOV_8, i + 2, j, k) > 0)) || (i + 3 < ilast && (vector(FOV_7,i + 3, j, k) > 0 || vector(FOV_8, i + 3, j, k) > 0)))) {
						vector(extrapolatedSBExSPi, i, j, k) = vector(ExSPprime, i, j, k);
						vector(extrapolatedSBExSPj, i, j, k) = vector(ExSPprime, i, j, k);
						vector(extrapolatedSBExSPk, i, j, k) = vector(ExSPprime, i, j, k);
						vector(extrapolatedSBEySPi, i, j, k) = vector(EySPprime, i, j, k);
						vector(extrapolatedSBEySPj, i, j, k) = vector(EySPprime, i, j, k);
						vector(extrapolatedSBEySPk, i, j, k) = vector(EySPprime, i, j, k);
						vector(extrapolatedSBEzSPi, i, j, k) = vector(EzSPprime, i, j, k);
						vector(extrapolatedSBEzSPj, i, j, k) = vector(EzSPprime, i, j, k);
						vector(extrapolatedSBEzSPk, i, j, k) = vector(EzSPprime, i, j, k);
						vector(extrapolatedSBBxSPi, i, j, k) = vector(BxSPprime, i, j, k);
						vector(extrapolatedSBBxSPj, i, j, k) = vector(BxSPprime, i, j, k);
						vector(extrapolatedSBBxSPk, i, j, k) = vector(BxSPprime, i, j, k);
						vector(extrapolatedSBBySPi, i, j, k) = vector(BySPprime, i, j, k);
						vector(extrapolatedSBBySPj, i, j, k) = vector(BySPprime, i, j, k);
						vector(extrapolatedSBBySPk, i, j, k) = vector(BySPprime, i, j, k);
						vector(extrapolatedSBBzSPi, i, j, k) = vector(BzSPprime, i, j, k);
						vector(extrapolatedSBBzSPj, i, j, k) = vector(BzSPprime, i, j, k);
						vector(extrapolatedSBBzSPk, i, j, k) = vector(BzSPprime, i, j, k);
						vector(extrapolatedSBqSPi, i, j, k) = vector(qSPprime, i, j, k);
						vector(extrapolatedSBqSPj, i, j, k) = vector(qSPprime, i, j, k);
						vector(extrapolatedSBqSPk, i, j, k) = vector(qSPprime, i, j, k);
						vector(extrapolatedSBJxSPi, i, j, k) = vector(JxSPprime, i, j, k);
						vector(extrapolatedSBJxSPj, i, j, k) = vector(JxSPprime, i, j, k);
						vector(extrapolatedSBJxSPk, i, j, k) = vector(JxSPprime, i, j, k);
						vector(extrapolatedSBJySPi, i, j, k) = vector(JySPprime, i, j, k);
						vector(extrapolatedSBJySPj, i, j, k) = vector(JySPprime, i, j, k);
						vector(extrapolatedSBJySPk, i, j, k) = vector(JySPprime, i, j, k);
						vector(extrapolatedSBJzSPi, i, j, k) = vector(JzSPprime, i, j, k);
						vector(extrapolatedSBJzSPj, i, j, k) = vector(JzSPprime, i, j, k);
						vector(extrapolatedSBJzSPk, i, j, k) = vector(JzSPprime, i, j, k);
					}
					if ((vector(FOV_xUpper, i, j, k) > 0) && ((i - 1 >= 0 && (vector(FOV_7, i - 1, j, k) > 0 || vector(FOV_8, i - 1, j, k) > 0)) || (i - 2 >= 0 && (vector(FOV_7, i - 2, j, k) > 0 || vector(FOV_8, i - 2, j, k) > 0)) || (i - 3 >= 0 && (vector(FOV_7, i - 3, j, k) > 0 || vector(FOV_8, i - 3, j, k) > 0)))) {
						vector(extrapolatedSBExSPi, i, j, k) = vector(ExSPprime, i, j, k);
						vector(extrapolatedSBExSPj, i, j, k) = vector(ExSPprime, i, j, k);
						vector(extrapolatedSBExSPk, i, j, k) = vector(ExSPprime, i, j, k);
						vector(extrapolatedSBEySPi, i, j, k) = vector(EySPprime, i, j, k);
						vector(extrapolatedSBEySPj, i, j, k) = vector(EySPprime, i, j, k);
						vector(extrapolatedSBEySPk, i, j, k) = vector(EySPprime, i, j, k);
						vector(extrapolatedSBEzSPi, i, j, k) = vector(EzSPprime, i, j, k);
						vector(extrapolatedSBEzSPj, i, j, k) = vector(EzSPprime, i, j, k);
						vector(extrapolatedSBEzSPk, i, j, k) = vector(EzSPprime, i, j, k);
						vector(extrapolatedSBBxSPi, i, j, k) = vector(BxSPprime, i, j, k);
						vector(extrapolatedSBBxSPj, i, j, k) = vector(BxSPprime, i, j, k);
						vector(extrapolatedSBBxSPk, i, j, k) = vector(BxSPprime, i, j, k);
						vector(extrapolatedSBBySPi, i, j, k) = vector(BySPprime, i, j, k);
						vector(extrapolatedSBBySPj, i, j, k) = vector(BySPprime, i, j, k);
						vector(extrapolatedSBBySPk, i, j, k) = vector(BySPprime, i, j, k);
						vector(extrapolatedSBBzSPi, i, j, k) = vector(BzSPprime, i, j, k);
						vector(extrapolatedSBBzSPj, i, j, k) = vector(BzSPprime, i, j, k);
						vector(extrapolatedSBBzSPk, i, j, k) = vector(BzSPprime, i, j, k);
						vector(extrapolatedSBqSPi, i, j, k) = vector(qSPprime, i, j, k);
						vector(extrapolatedSBqSPj, i, j, k) = vector(qSPprime, i, j, k);
						vector(extrapolatedSBqSPk, i, j, k) = vector(qSPprime, i, j, k);
						vector(extrapolatedSBJxSPi, i, j, k) = vector(JxSPprime, i, j, k);
						vector(extrapolatedSBJxSPj, i, j, k) = vector(JxSPprime, i, j, k);
						vector(extrapolatedSBJxSPk, i, j, k) = vector(JxSPprime, i, j, k);
						vector(extrapolatedSBJySPi, i, j, k) = vector(JySPprime, i, j, k);
						vector(extrapolatedSBJySPj, i, j, k) = vector(JySPprime, i, j, k);
						vector(extrapolatedSBJySPk, i, j, k) = vector(JySPprime, i, j, k);
						vector(extrapolatedSBJzSPi, i, j, k) = vector(JzSPprime, i, j, k);
						vector(extrapolatedSBJzSPj, i, j, k) = vector(JzSPprime, i, j, k);
						vector(extrapolatedSBJzSPk, i, j, k) = vector(JzSPprime, i, j, k);
					}
					if ((vector(FOV_yLower, i, j, k) > 0) && ((j + 1 < jlast && (vector(FOV_7,i, j + 1, k) > 0 || vector(FOV_8, i, j + 1, k) > 0)) || (j + 2 < jlast && (vector(FOV_7,i, j + 2, k) > 0 || vector(FOV_8, i, j + 2, k) > 0)) || (j + 3 < jlast && (vector(FOV_7,i, j + 3, k) > 0 || vector(FOV_8, i, j + 3, k) > 0)))) {
						vector(extrapolatedSBExSPi, i, j, k) = vector(ExSPprime, i, j, k);
						vector(extrapolatedSBExSPj, i, j, k) = vector(ExSPprime, i, j, k);
						vector(extrapolatedSBExSPk, i, j, k) = vector(ExSPprime, i, j, k);
						vector(extrapolatedSBEySPi, i, j, k) = vector(EySPprime, i, j, k);
						vector(extrapolatedSBEySPj, i, j, k) = vector(EySPprime, i, j, k);
						vector(extrapolatedSBEySPk, i, j, k) = vector(EySPprime, i, j, k);
						vector(extrapolatedSBEzSPi, i, j, k) = vector(EzSPprime, i, j, k);
						vector(extrapolatedSBEzSPj, i, j, k) = vector(EzSPprime, i, j, k);
						vector(extrapolatedSBEzSPk, i, j, k) = vector(EzSPprime, i, j, k);
						vector(extrapolatedSBBxSPi, i, j, k) = vector(BxSPprime, i, j, k);
						vector(extrapolatedSBBxSPj, i, j, k) = vector(BxSPprime, i, j, k);
						vector(extrapolatedSBBxSPk, i, j, k) = vector(BxSPprime, i, j, k);
						vector(extrapolatedSBBySPi, i, j, k) = vector(BySPprime, i, j, k);
						vector(extrapolatedSBBySPj, i, j, k) = vector(BySPprime, i, j, k);
						vector(extrapolatedSBBySPk, i, j, k) = vector(BySPprime, i, j, k);
						vector(extrapolatedSBBzSPi, i, j, k) = vector(BzSPprime, i, j, k);
						vector(extrapolatedSBBzSPj, i, j, k) = vector(BzSPprime, i, j, k);
						vector(extrapolatedSBBzSPk, i, j, k) = vector(BzSPprime, i, j, k);
						vector(extrapolatedSBqSPi, i, j, k) = vector(qSPprime, i, j, k);
						vector(extrapolatedSBqSPj, i, j, k) = vector(qSPprime, i, j, k);
						vector(extrapolatedSBqSPk, i, j, k) = vector(qSPprime, i, j, k);
						vector(extrapolatedSBJxSPi, i, j, k) = vector(JxSPprime, i, j, k);
						vector(extrapolatedSBJxSPj, i, j, k) = vector(JxSPprime, i, j, k);
						vector(extrapolatedSBJxSPk, i, j, k) = vector(JxSPprime, i, j, k);
						vector(extrapolatedSBJySPi, i, j, k) = vector(JySPprime, i, j, k);
						vector(extrapolatedSBJySPj, i, j, k) = vector(JySPprime, i, j, k);
						vector(extrapolatedSBJySPk, i, j, k) = vector(JySPprime, i, j, k);
						vector(extrapolatedSBJzSPi, i, j, k) = vector(JzSPprime, i, j, k);
						vector(extrapolatedSBJzSPj, i, j, k) = vector(JzSPprime, i, j, k);
						vector(extrapolatedSBJzSPk, i, j, k) = vector(JzSPprime, i, j, k);
					}
					if ((vector(FOV_yUpper, i, j, k) > 0) && ((j - 1 >= 0 && (vector(FOV_7, i, j - 1, k) > 0 || vector(FOV_8, i, j - 1, k) > 0)) || (j - 2 >= 0 && (vector(FOV_7, i, j - 2, k) > 0 || vector(FOV_8, i, j - 2, k) > 0)) || (j - 3 >= 0 && (vector(FOV_7, i, j - 3, k) > 0 || vector(FOV_8, i, j - 3, k) > 0)))) {
						vector(extrapolatedSBExSPi, i, j, k) = vector(ExSPprime, i, j, k);
						vector(extrapolatedSBExSPj, i, j, k) = vector(ExSPprime, i, j, k);
						vector(extrapolatedSBExSPk, i, j, k) = vector(ExSPprime, i, j, k);
						vector(extrapolatedSBEySPi, i, j, k) = vector(EySPprime, i, j, k);
						vector(extrapolatedSBEySPj, i, j, k) = vector(EySPprime, i, j, k);
						vector(extrapolatedSBEySPk, i, j, k) = vector(EySPprime, i, j, k);
						vector(extrapolatedSBEzSPi, i, j, k) = vector(EzSPprime, i, j, k);
						vector(extrapolatedSBEzSPj, i, j, k) = vector(EzSPprime, i, j, k);
						vector(extrapolatedSBEzSPk, i, j, k) = vector(EzSPprime, i, j, k);
						vector(extrapolatedSBBxSPi, i, j, k) = vector(BxSPprime, i, j, k);
						vector(extrapolatedSBBxSPj, i, j, k) = vector(BxSPprime, i, j, k);
						vector(extrapolatedSBBxSPk, i, j, k) = vector(BxSPprime, i, j, k);
						vector(extrapolatedSBBySPi, i, j, k) = vector(BySPprime, i, j, k);
						vector(extrapolatedSBBySPj, i, j, k) = vector(BySPprime, i, j, k);
						vector(extrapolatedSBBySPk, i, j, k) = vector(BySPprime, i, j, k);
						vector(extrapolatedSBBzSPi, i, j, k) = vector(BzSPprime, i, j, k);
						vector(extrapolatedSBBzSPj, i, j, k) = vector(BzSPprime, i, j, k);
						vector(extrapolatedSBBzSPk, i, j, k) = vector(BzSPprime, i, j, k);
						vector(extrapolatedSBqSPi, i, j, k) = vector(qSPprime, i, j, k);
						vector(extrapolatedSBqSPj, i, j, k) = vector(qSPprime, i, j, k);
						vector(extrapolatedSBqSPk, i, j, k) = vector(qSPprime, i, j, k);
						vector(extrapolatedSBJxSPi, i, j, k) = vector(JxSPprime, i, j, k);
						vector(extrapolatedSBJxSPj, i, j, k) = vector(JxSPprime, i, j, k);
						vector(extrapolatedSBJxSPk, i, j, k) = vector(JxSPprime, i, j, k);
						vector(extrapolatedSBJySPi, i, j, k) = vector(JySPprime, i, j, k);
						vector(extrapolatedSBJySPj, i, j, k) = vector(JySPprime, i, j, k);
						vector(extrapolatedSBJySPk, i, j, k) = vector(JySPprime, i, j, k);
						vector(extrapolatedSBJzSPi, i, j, k) = vector(JzSPprime, i, j, k);
						vector(extrapolatedSBJzSPj, i, j, k) = vector(JzSPprime, i, j, k);
						vector(extrapolatedSBJzSPk, i, j, k) = vector(JzSPprime, i, j, k);
					}
					if ((vector(FOV_zLower, i, j, k) > 0) && ((k + 1 < klast && (vector(FOV_7,i, j, k + 1) > 0 || vector(FOV_8, i, j, k + 1) > 0)) || (k + 2 < klast && (vector(FOV_7,i, j, k + 2) > 0 || vector(FOV_8, i, j, k + 2) > 0)) || (k + 3 < klast && (vector(FOV_7,i, j, k + 3) > 0 || vector(FOV_8, i, j, k + 3) > 0)))) {
						vector(extrapolatedSBExSPi, i, j, k) = vector(ExSPprime, i, j, k);
						vector(extrapolatedSBExSPj, i, j, k) = vector(ExSPprime, i, j, k);
						vector(extrapolatedSBExSPk, i, j, k) = vector(ExSPprime, i, j, k);
						vector(extrapolatedSBEySPi, i, j, k) = vector(EySPprime, i, j, k);
						vector(extrapolatedSBEySPj, i, j, k) = vector(EySPprime, i, j, k);
						vector(extrapolatedSBEySPk, i, j, k) = vector(EySPprime, i, j, k);
						vector(extrapolatedSBEzSPi, i, j, k) = vector(EzSPprime, i, j, k);
						vector(extrapolatedSBEzSPj, i, j, k) = vector(EzSPprime, i, j, k);
						vector(extrapolatedSBEzSPk, i, j, k) = vector(EzSPprime, i, j, k);
						vector(extrapolatedSBBxSPi, i, j, k) = vector(BxSPprime, i, j, k);
						vector(extrapolatedSBBxSPj, i, j, k) = vector(BxSPprime, i, j, k);
						vector(extrapolatedSBBxSPk, i, j, k) = vector(BxSPprime, i, j, k);
						vector(extrapolatedSBBySPi, i, j, k) = vector(BySPprime, i, j, k);
						vector(extrapolatedSBBySPj, i, j, k) = vector(BySPprime, i, j, k);
						vector(extrapolatedSBBySPk, i, j, k) = vector(BySPprime, i, j, k);
						vector(extrapolatedSBBzSPi, i, j, k) = vector(BzSPprime, i, j, k);
						vector(extrapolatedSBBzSPj, i, j, k) = vector(BzSPprime, i, j, k);
						vector(extrapolatedSBBzSPk, i, j, k) = vector(BzSPprime, i, j, k);
						vector(extrapolatedSBqSPi, i, j, k) = vector(qSPprime, i, j, k);
						vector(extrapolatedSBqSPj, i, j, k) = vector(qSPprime, i, j, k);
						vector(extrapolatedSBqSPk, i, j, k) = vector(qSPprime, i, j, k);
						vector(extrapolatedSBJxSPi, i, j, k) = vector(JxSPprime, i, j, k);
						vector(extrapolatedSBJxSPj, i, j, k) = vector(JxSPprime, i, j, k);
						vector(extrapolatedSBJxSPk, i, j, k) = vector(JxSPprime, i, j, k);
						vector(extrapolatedSBJySPi, i, j, k) = vector(JySPprime, i, j, k);
						vector(extrapolatedSBJySPj, i, j, k) = vector(JySPprime, i, j, k);
						vector(extrapolatedSBJySPk, i, j, k) = vector(JySPprime, i, j, k);
						vector(extrapolatedSBJzSPi, i, j, k) = vector(JzSPprime, i, j, k);
						vector(extrapolatedSBJzSPj, i, j, k) = vector(JzSPprime, i, j, k);
						vector(extrapolatedSBJzSPk, i, j, k) = vector(JzSPprime, i, j, k);
					}
					if ((vector(FOV_zUpper, i, j, k) > 0) && ((k - 1 >= 0 && (vector(FOV_7, i, j, k - 1) > 0 || vector(FOV_8, i, j, k - 1) > 0)) || (k - 2 >= 0 && (vector(FOV_7, i, j, k - 2) > 0 || vector(FOV_8, i, j, k - 2) > 0)) || (k - 3 >= 0 && (vector(FOV_7, i, j, k - 3) > 0 || vector(FOV_8, i, j, k - 3) > 0)))) {
						vector(extrapolatedSBExSPi, i, j, k) = vector(ExSPprime, i, j, k);
						vector(extrapolatedSBExSPj, i, j, k) = vector(ExSPprime, i, j, k);
						vector(extrapolatedSBExSPk, i, j, k) = vector(ExSPprime, i, j, k);
						vector(extrapolatedSBEySPi, i, j, k) = vector(EySPprime, i, j, k);
						vector(extrapolatedSBEySPj, i, j, k) = vector(EySPprime, i, j, k);
						vector(extrapolatedSBEySPk, i, j, k) = vector(EySPprime, i, j, k);
						vector(extrapolatedSBEzSPi, i, j, k) = vector(EzSPprime, i, j, k);
						vector(extrapolatedSBEzSPj, i, j, k) = vector(EzSPprime, i, j, k);
						vector(extrapolatedSBEzSPk, i, j, k) = vector(EzSPprime, i, j, k);
						vector(extrapolatedSBBxSPi, i, j, k) = vector(BxSPprime, i, j, k);
						vector(extrapolatedSBBxSPj, i, j, k) = vector(BxSPprime, i, j, k);
						vector(extrapolatedSBBxSPk, i, j, k) = vector(BxSPprime, i, j, k);
						vector(extrapolatedSBBySPi, i, j, k) = vector(BySPprime, i, j, k);
						vector(extrapolatedSBBySPj, i, j, k) = vector(BySPprime, i, j, k);
						vector(extrapolatedSBBySPk, i, j, k) = vector(BySPprime, i, j, k);
						vector(extrapolatedSBBzSPi, i, j, k) = vector(BzSPprime, i, j, k);
						vector(extrapolatedSBBzSPj, i, j, k) = vector(BzSPprime, i, j, k);
						vector(extrapolatedSBBzSPk, i, j, k) = vector(BzSPprime, i, j, k);
						vector(extrapolatedSBqSPi, i, j, k) = vector(qSPprime, i, j, k);
						vector(extrapolatedSBqSPj, i, j, k) = vector(qSPprime, i, j, k);
						vector(extrapolatedSBqSPk, i, j, k) = vector(qSPprime, i, j, k);
						vector(extrapolatedSBJxSPi, i, j, k) = vector(JxSPprime, i, j, k);
						vector(extrapolatedSBJxSPj, i, j, k) = vector(JxSPprime, i, j, k);
						vector(extrapolatedSBJxSPk, i, j, k) = vector(JxSPprime, i, j, k);
						vector(extrapolatedSBJySPi, i, j, k) = vector(JySPprime, i, j, k);
						vector(extrapolatedSBJySPj, i, j, k) = vector(JySPprime, i, j, k);
						vector(extrapolatedSBJySPk, i, j, k) = vector(JySPprime, i, j, k);
						vector(extrapolatedSBJzSPi, i, j, k) = vector(JzSPprime, i, j, k);
						vector(extrapolatedSBJzSPj, i, j, k) = vector(JzSPprime, i, j, k);
						vector(extrapolatedSBJzSPk, i, j, k) = vector(JzSPprime, i, j, k);
					}
					if ((vector(FOV_xLower, i, j, k) > 0) && ((i + 1 < ilast && (vector(FOV_5,i + 1, j, k) > 0)) || (i + 2 < ilast && (vector(FOV_5,i + 2, j, k) > 0)) || (i + 3 < ilast && (vector(FOV_5,i + 3, j, k) > 0)))) {
						vector(extrapolatedSBExSPi, i, j, k) = vector(ExSPprime_5, i, j, k);
						vector(extrapolatedSBExSPj, i, j, k) = vector(ExSPprime_5, i, j, k);
						vector(extrapolatedSBExSPk, i, j, k) = vector(ExSPprime_5, i, j, k);
						vector(extrapolatedSBEySPi, i, j, k) = vector(EySPprime_5, i, j, k);
						vector(extrapolatedSBEySPj, i, j, k) = vector(EySPprime_5, i, j, k);
						vector(extrapolatedSBEySPk, i, j, k) = vector(EySPprime_5, i, j, k);
						vector(extrapolatedSBEzSPi, i, j, k) = vector(EzSPprime_5, i, j, k);
						vector(extrapolatedSBEzSPj, i, j, k) = vector(EzSPprime_5, i, j, k);
						vector(extrapolatedSBEzSPk, i, j, k) = vector(EzSPprime_5, i, j, k);
						vector(extrapolatedSBBxSPi, i, j, k) = vector(BxSPprime_5, i, j, k);
						vector(extrapolatedSBBxSPj, i, j, k) = vector(BxSPprime_5, i, j, k);
						vector(extrapolatedSBBxSPk, i, j, k) = vector(BxSPprime_5, i, j, k);
						vector(extrapolatedSBBySPi, i, j, k) = vector(BySPprime_5, i, j, k);
						vector(extrapolatedSBBySPj, i, j, k) = vector(BySPprime_5, i, j, k);
						vector(extrapolatedSBBySPk, i, j, k) = vector(BySPprime_5, i, j, k);
						vector(extrapolatedSBBzSPi, i, j, k) = vector(BzSPprime_5, i, j, k);
						vector(extrapolatedSBBzSPj, i, j, k) = vector(BzSPprime_5, i, j, k);
						vector(extrapolatedSBBzSPk, i, j, k) = vector(BzSPprime_5, i, j, k);
						vector(extrapolatedSBqSPi, i, j, k) = vector(qSPprime_5, i, j, k);
						vector(extrapolatedSBqSPj, i, j, k) = vector(qSPprime_5, i, j, k);
						vector(extrapolatedSBqSPk, i, j, k) = vector(qSPprime_5, i, j, k);
						vector(extrapolatedSBJxSPi, i, j, k) = vector(JxSPprime_5, i, j, k);
						vector(extrapolatedSBJxSPj, i, j, k) = vector(JxSPprime_5, i, j, k);
						vector(extrapolatedSBJxSPk, i, j, k) = vector(JxSPprime_5, i, j, k);
						vector(extrapolatedSBJySPi, i, j, k) = vector(JySPprime_5, i, j, k);
						vector(extrapolatedSBJySPj, i, j, k) = vector(JySPprime_5, i, j, k);
						vector(extrapolatedSBJySPk, i, j, k) = vector(JySPprime_5, i, j, k);
						vector(extrapolatedSBJzSPi, i, j, k) = vector(JzSPprime_5, i, j, k);
						vector(extrapolatedSBJzSPj, i, j, k) = vector(JzSPprime_5, i, j, k);
						vector(extrapolatedSBJzSPk, i, j, k) = vector(JzSPprime_5, i, j, k);
					}
					if ((vector(FOV_xUpper, i, j, k) > 0) && ((i - 1 >= 0 && (vector(FOV_5, i - 1, j, k) > 0)) || (i - 2 >= 0 && (vector(FOV_5, i - 2, j, k) > 0)) || (i - 3 >= 0 && (vector(FOV_5, i - 3, j, k) > 0)))) {
						vector(extrapolatedSBExSPi, i, j, k) = vector(ExSPprime_5, i, j, k);
						vector(extrapolatedSBExSPj, i, j, k) = vector(ExSPprime_5, i, j, k);
						vector(extrapolatedSBExSPk, i, j, k) = vector(ExSPprime_5, i, j, k);
						vector(extrapolatedSBEySPi, i, j, k) = vector(EySPprime_5, i, j, k);
						vector(extrapolatedSBEySPj, i, j, k) = vector(EySPprime_5, i, j, k);
						vector(extrapolatedSBEySPk, i, j, k) = vector(EySPprime_5, i, j, k);
						vector(extrapolatedSBEzSPi, i, j, k) = vector(EzSPprime_5, i, j, k);
						vector(extrapolatedSBEzSPj, i, j, k) = vector(EzSPprime_5, i, j, k);
						vector(extrapolatedSBEzSPk, i, j, k) = vector(EzSPprime_5, i, j, k);
						vector(extrapolatedSBBxSPi, i, j, k) = vector(BxSPprime_5, i, j, k);
						vector(extrapolatedSBBxSPj, i, j, k) = vector(BxSPprime_5, i, j, k);
						vector(extrapolatedSBBxSPk, i, j, k) = vector(BxSPprime_5, i, j, k);
						vector(extrapolatedSBBySPi, i, j, k) = vector(BySPprime_5, i, j, k);
						vector(extrapolatedSBBySPj, i, j, k) = vector(BySPprime_5, i, j, k);
						vector(extrapolatedSBBySPk, i, j, k) = vector(BySPprime_5, i, j, k);
						vector(extrapolatedSBBzSPi, i, j, k) = vector(BzSPprime_5, i, j, k);
						vector(extrapolatedSBBzSPj, i, j, k) = vector(BzSPprime_5, i, j, k);
						vector(extrapolatedSBBzSPk, i, j, k) = vector(BzSPprime_5, i, j, k);
						vector(extrapolatedSBqSPi, i, j, k) = vector(qSPprime_5, i, j, k);
						vector(extrapolatedSBqSPj, i, j, k) = vector(qSPprime_5, i, j, k);
						vector(extrapolatedSBqSPk, i, j, k) = vector(qSPprime_5, i, j, k);
						vector(extrapolatedSBJxSPi, i, j, k) = vector(JxSPprime_5, i, j, k);
						vector(extrapolatedSBJxSPj, i, j, k) = vector(JxSPprime_5, i, j, k);
						vector(extrapolatedSBJxSPk, i, j, k) = vector(JxSPprime_5, i, j, k);
						vector(extrapolatedSBJySPi, i, j, k) = vector(JySPprime_5, i, j, k);
						vector(extrapolatedSBJySPj, i, j, k) = vector(JySPprime_5, i, j, k);
						vector(extrapolatedSBJySPk, i, j, k) = vector(JySPprime_5, i, j, k);
						vector(extrapolatedSBJzSPi, i, j, k) = vector(JzSPprime_5, i, j, k);
						vector(extrapolatedSBJzSPj, i, j, k) = vector(JzSPprime_5, i, j, k);
						vector(extrapolatedSBJzSPk, i, j, k) = vector(JzSPprime_5, i, j, k);
					}
					if ((vector(FOV_yLower, i, j, k) > 0) && ((j + 1 < jlast && (vector(FOV_5,i, j + 1, k) > 0)) || (j + 2 < jlast && (vector(FOV_5,i, j + 2, k) > 0)) || (j + 3 < jlast && (vector(FOV_5,i, j + 3, k) > 0)))) {
						vector(extrapolatedSBExSPi, i, j, k) = vector(ExSPprime_5, i, j, k);
						vector(extrapolatedSBExSPj, i, j, k) = vector(ExSPprime_5, i, j, k);
						vector(extrapolatedSBExSPk, i, j, k) = vector(ExSPprime_5, i, j, k);
						vector(extrapolatedSBEySPi, i, j, k) = vector(EySPprime_5, i, j, k);
						vector(extrapolatedSBEySPj, i, j, k) = vector(EySPprime_5, i, j, k);
						vector(extrapolatedSBEySPk, i, j, k) = vector(EySPprime_5, i, j, k);
						vector(extrapolatedSBEzSPi, i, j, k) = vector(EzSPprime_5, i, j, k);
						vector(extrapolatedSBEzSPj, i, j, k) = vector(EzSPprime_5, i, j, k);
						vector(extrapolatedSBEzSPk, i, j, k) = vector(EzSPprime_5, i, j, k);
						vector(extrapolatedSBBxSPi, i, j, k) = vector(BxSPprime_5, i, j, k);
						vector(extrapolatedSBBxSPj, i, j, k) = vector(BxSPprime_5, i, j, k);
						vector(extrapolatedSBBxSPk, i, j, k) = vector(BxSPprime_5, i, j, k);
						vector(extrapolatedSBBySPi, i, j, k) = vector(BySPprime_5, i, j, k);
						vector(extrapolatedSBBySPj, i, j, k) = vector(BySPprime_5, i, j, k);
						vector(extrapolatedSBBySPk, i, j, k) = vector(BySPprime_5, i, j, k);
						vector(extrapolatedSBBzSPi, i, j, k) = vector(BzSPprime_5, i, j, k);
						vector(extrapolatedSBBzSPj, i, j, k) = vector(BzSPprime_5, i, j, k);
						vector(extrapolatedSBBzSPk, i, j, k) = vector(BzSPprime_5, i, j, k);
						vector(extrapolatedSBqSPi, i, j, k) = vector(qSPprime_5, i, j, k);
						vector(extrapolatedSBqSPj, i, j, k) = vector(qSPprime_5, i, j, k);
						vector(extrapolatedSBqSPk, i, j, k) = vector(qSPprime_5, i, j, k);
						vector(extrapolatedSBJxSPi, i, j, k) = vector(JxSPprime_5, i, j, k);
						vector(extrapolatedSBJxSPj, i, j, k) = vector(JxSPprime_5, i, j, k);
						vector(extrapolatedSBJxSPk, i, j, k) = vector(JxSPprime_5, i, j, k);
						vector(extrapolatedSBJySPi, i, j, k) = vector(JySPprime_5, i, j, k);
						vector(extrapolatedSBJySPj, i, j, k) = vector(JySPprime_5, i, j, k);
						vector(extrapolatedSBJySPk, i, j, k) = vector(JySPprime_5, i, j, k);
						vector(extrapolatedSBJzSPi, i, j, k) = vector(JzSPprime_5, i, j, k);
						vector(extrapolatedSBJzSPj, i, j, k) = vector(JzSPprime_5, i, j, k);
						vector(extrapolatedSBJzSPk, i, j, k) = vector(JzSPprime_5, i, j, k);
					}
					if ((vector(FOV_yUpper, i, j, k) > 0) && ((j - 1 >= 0 && (vector(FOV_5, i, j - 1, k) > 0)) || (j - 2 >= 0 && (vector(FOV_5, i, j - 2, k) > 0)) || (j - 3 >= 0 && (vector(FOV_5, i, j - 3, k) > 0)))) {
						vector(extrapolatedSBExSPi, i, j, k) = vector(ExSPprime_5, i, j, k);
						vector(extrapolatedSBExSPj, i, j, k) = vector(ExSPprime_5, i, j, k);
						vector(extrapolatedSBExSPk, i, j, k) = vector(ExSPprime_5, i, j, k);
						vector(extrapolatedSBEySPi, i, j, k) = vector(EySPprime_5, i, j, k);
						vector(extrapolatedSBEySPj, i, j, k) = vector(EySPprime_5, i, j, k);
						vector(extrapolatedSBEySPk, i, j, k) = vector(EySPprime_5, i, j, k);
						vector(extrapolatedSBEzSPi, i, j, k) = vector(EzSPprime_5, i, j, k);
						vector(extrapolatedSBEzSPj, i, j, k) = vector(EzSPprime_5, i, j, k);
						vector(extrapolatedSBEzSPk, i, j, k) = vector(EzSPprime_5, i, j, k);
						vector(extrapolatedSBBxSPi, i, j, k) = vector(BxSPprime_5, i, j, k);
						vector(extrapolatedSBBxSPj, i, j, k) = vector(BxSPprime_5, i, j, k);
						vector(extrapolatedSBBxSPk, i, j, k) = vector(BxSPprime_5, i, j, k);
						vector(extrapolatedSBBySPi, i, j, k) = vector(BySPprime_5, i, j, k);
						vector(extrapolatedSBBySPj, i, j, k) = vector(BySPprime_5, i, j, k);
						vector(extrapolatedSBBySPk, i, j, k) = vector(BySPprime_5, i, j, k);
						vector(extrapolatedSBBzSPi, i, j, k) = vector(BzSPprime_5, i, j, k);
						vector(extrapolatedSBBzSPj, i, j, k) = vector(BzSPprime_5, i, j, k);
						vector(extrapolatedSBBzSPk, i, j, k) = vector(BzSPprime_5, i, j, k);
						vector(extrapolatedSBqSPi, i, j, k) = vector(qSPprime_5, i, j, k);
						vector(extrapolatedSBqSPj, i, j, k) = vector(qSPprime_5, i, j, k);
						vector(extrapolatedSBqSPk, i, j, k) = vector(qSPprime_5, i, j, k);
						vector(extrapolatedSBJxSPi, i, j, k) = vector(JxSPprime_5, i, j, k);
						vector(extrapolatedSBJxSPj, i, j, k) = vector(JxSPprime_5, i, j, k);
						vector(extrapolatedSBJxSPk, i, j, k) = vector(JxSPprime_5, i, j, k);
						vector(extrapolatedSBJySPi, i, j, k) = vector(JySPprime_5, i, j, k);
						vector(extrapolatedSBJySPj, i, j, k) = vector(JySPprime_5, i, j, k);
						vector(extrapolatedSBJySPk, i, j, k) = vector(JySPprime_5, i, j, k);
						vector(extrapolatedSBJzSPi, i, j, k) = vector(JzSPprime_5, i, j, k);
						vector(extrapolatedSBJzSPj, i, j, k) = vector(JzSPprime_5, i, j, k);
						vector(extrapolatedSBJzSPk, i, j, k) = vector(JzSPprime_5, i, j, k);
					}
					if ((vector(FOV_zLower, i, j, k) > 0) && ((k + 1 < klast && (vector(FOV_5,i, j, k + 1) > 0)) || (k + 2 < klast && (vector(FOV_5,i, j, k + 2) > 0)) || (k + 3 < klast && (vector(FOV_5,i, j, k + 3) > 0)))) {
						vector(extrapolatedSBExSPi, i, j, k) = vector(ExSPprime_5, i, j, k);
						vector(extrapolatedSBExSPj, i, j, k) = vector(ExSPprime_5, i, j, k);
						vector(extrapolatedSBExSPk, i, j, k) = vector(ExSPprime_5, i, j, k);
						vector(extrapolatedSBEySPi, i, j, k) = vector(EySPprime_5, i, j, k);
						vector(extrapolatedSBEySPj, i, j, k) = vector(EySPprime_5, i, j, k);
						vector(extrapolatedSBEySPk, i, j, k) = vector(EySPprime_5, i, j, k);
						vector(extrapolatedSBEzSPi, i, j, k) = vector(EzSPprime_5, i, j, k);
						vector(extrapolatedSBEzSPj, i, j, k) = vector(EzSPprime_5, i, j, k);
						vector(extrapolatedSBEzSPk, i, j, k) = vector(EzSPprime_5, i, j, k);
						vector(extrapolatedSBBxSPi, i, j, k) = vector(BxSPprime_5, i, j, k);
						vector(extrapolatedSBBxSPj, i, j, k) = vector(BxSPprime_5, i, j, k);
						vector(extrapolatedSBBxSPk, i, j, k) = vector(BxSPprime_5, i, j, k);
						vector(extrapolatedSBBySPi, i, j, k) = vector(BySPprime_5, i, j, k);
						vector(extrapolatedSBBySPj, i, j, k) = vector(BySPprime_5, i, j, k);
						vector(extrapolatedSBBySPk, i, j, k) = vector(BySPprime_5, i, j, k);
						vector(extrapolatedSBBzSPi, i, j, k) = vector(BzSPprime_5, i, j, k);
						vector(extrapolatedSBBzSPj, i, j, k) = vector(BzSPprime_5, i, j, k);
						vector(extrapolatedSBBzSPk, i, j, k) = vector(BzSPprime_5, i, j, k);
						vector(extrapolatedSBqSPi, i, j, k) = vector(qSPprime_5, i, j, k);
						vector(extrapolatedSBqSPj, i, j, k) = vector(qSPprime_5, i, j, k);
						vector(extrapolatedSBqSPk, i, j, k) = vector(qSPprime_5, i, j, k);
						vector(extrapolatedSBJxSPi, i, j, k) = vector(JxSPprime_5, i, j, k);
						vector(extrapolatedSBJxSPj, i, j, k) = vector(JxSPprime_5, i, j, k);
						vector(extrapolatedSBJxSPk, i, j, k) = vector(JxSPprime_5, i, j, k);
						vector(extrapolatedSBJySPi, i, j, k) = vector(JySPprime_5, i, j, k);
						vector(extrapolatedSBJySPj, i, j, k) = vector(JySPprime_5, i, j, k);
						vector(extrapolatedSBJySPk, i, j, k) = vector(JySPprime_5, i, j, k);
						vector(extrapolatedSBJzSPi, i, j, k) = vector(JzSPprime_5, i, j, k);
						vector(extrapolatedSBJzSPj, i, j, k) = vector(JzSPprime_5, i, j, k);
						vector(extrapolatedSBJzSPk, i, j, k) = vector(JzSPprime_5, i, j, k);
					}
					if ((vector(FOV_zUpper, i, j, k) > 0) && ((k - 1 >= 0 && (vector(FOV_5, i, j, k - 1) > 0)) || (k - 2 >= 0 && (vector(FOV_5, i, j, k - 2) > 0)) || (k - 3 >= 0 && (vector(FOV_5, i, j, k - 3) > 0)))) {
						vector(extrapolatedSBExSPi, i, j, k) = vector(ExSPprime_5, i, j, k);
						vector(extrapolatedSBExSPj, i, j, k) = vector(ExSPprime_5, i, j, k);
						vector(extrapolatedSBExSPk, i, j, k) = vector(ExSPprime_5, i, j, k);
						vector(extrapolatedSBEySPi, i, j, k) = vector(EySPprime_5, i, j, k);
						vector(extrapolatedSBEySPj, i, j, k) = vector(EySPprime_5, i, j, k);
						vector(extrapolatedSBEySPk, i, j, k) = vector(EySPprime_5, i, j, k);
						vector(extrapolatedSBEzSPi, i, j, k) = vector(EzSPprime_5, i, j, k);
						vector(extrapolatedSBEzSPj, i, j, k) = vector(EzSPprime_5, i, j, k);
						vector(extrapolatedSBEzSPk, i, j, k) = vector(EzSPprime_5, i, j, k);
						vector(extrapolatedSBBxSPi, i, j, k) = vector(BxSPprime_5, i, j, k);
						vector(extrapolatedSBBxSPj, i, j, k) = vector(BxSPprime_5, i, j, k);
						vector(extrapolatedSBBxSPk, i, j, k) = vector(BxSPprime_5, i, j, k);
						vector(extrapolatedSBBySPi, i, j, k) = vector(BySPprime_5, i, j, k);
						vector(extrapolatedSBBySPj, i, j, k) = vector(BySPprime_5, i, j, k);
						vector(extrapolatedSBBySPk, i, j, k) = vector(BySPprime_5, i, j, k);
						vector(extrapolatedSBBzSPi, i, j, k) = vector(BzSPprime_5, i, j, k);
						vector(extrapolatedSBBzSPj, i, j, k) = vector(BzSPprime_5, i, j, k);
						vector(extrapolatedSBBzSPk, i, j, k) = vector(BzSPprime_5, i, j, k);
						vector(extrapolatedSBqSPi, i, j, k) = vector(qSPprime_5, i, j, k);
						vector(extrapolatedSBqSPj, i, j, k) = vector(qSPprime_5, i, j, k);
						vector(extrapolatedSBqSPk, i, j, k) = vector(qSPprime_5, i, j, k);
						vector(extrapolatedSBJxSPi, i, j, k) = vector(JxSPprime_5, i, j, k);
						vector(extrapolatedSBJxSPj, i, j, k) = vector(JxSPprime_5, i, j, k);
						vector(extrapolatedSBJxSPk, i, j, k) = vector(JxSPprime_5, i, j, k);
						vector(extrapolatedSBJySPi, i, j, k) = vector(JySPprime_5, i, j, k);
						vector(extrapolatedSBJySPj, i, j, k) = vector(JySPprime_5, i, j, k);
						vector(extrapolatedSBJySPk, i, j, k) = vector(JySPprime_5, i, j, k);
						vector(extrapolatedSBJzSPi, i, j, k) = vector(JzSPprime_5, i, j, k);
						vector(extrapolatedSBJzSPj, i, j, k) = vector(JzSPprime_5, i, j, k);
						vector(extrapolatedSBJzSPk, i, j, k) = vector(JzSPprime_5, i, j, k);
					}
					if ((vector(FOV_xLower, i, j, k) > 0) && ((i + 1 < ilast && (vector(FOV_1,i + 1, j, k) > 0 || vector(FOV_2, i + 1, j, k) > 0)) || (i + 2 < ilast && (vector(FOV_1,i + 2, j, k) > 0 || vector(FOV_2, i + 2, j, k) > 0)) || (i + 3 < ilast && (vector(FOV_1,i + 3, j, k) > 0 || vector(FOV_2, i + 3, j, k) > 0)))) {
						vector(extrapolatedSBExSPi, i, j, k) = vector(ExSPprime_2, i, j, k);
						vector(extrapolatedSBExSPj, i, j, k) = vector(ExSPprime_2, i, j, k);
						vector(extrapolatedSBExSPk, i, j, k) = vector(ExSPprime_2, i, j, k);
						vector(extrapolatedSBEySPi, i, j, k) = vector(EySPprime_2, i, j, k);
						vector(extrapolatedSBEySPj, i, j, k) = vector(EySPprime_2, i, j, k);
						vector(extrapolatedSBEySPk, i, j, k) = vector(EySPprime_2, i, j, k);
						vector(extrapolatedSBEzSPi, i, j, k) = vector(EzSPprime_2, i, j, k);
						vector(extrapolatedSBEzSPj, i, j, k) = vector(EzSPprime_2, i, j, k);
						vector(extrapolatedSBEzSPk, i, j, k) = vector(EzSPprime_2, i, j, k);
						vector(extrapolatedSBBxSPi, i, j, k) = vector(BxSPprime_2, i, j, k);
						vector(extrapolatedSBBxSPj, i, j, k) = vector(BxSPprime_2, i, j, k);
						vector(extrapolatedSBBxSPk, i, j, k) = vector(BxSPprime_2, i, j, k);
						vector(extrapolatedSBBySPi, i, j, k) = vector(BySPprime_2, i, j, k);
						vector(extrapolatedSBBySPj, i, j, k) = vector(BySPprime_2, i, j, k);
						vector(extrapolatedSBBySPk, i, j, k) = vector(BySPprime_2, i, j, k);
						vector(extrapolatedSBBzSPi, i, j, k) = vector(BzSPprime_2, i, j, k);
						vector(extrapolatedSBBzSPj, i, j, k) = vector(BzSPprime_2, i, j, k);
						vector(extrapolatedSBBzSPk, i, j, k) = vector(BzSPprime_2, i, j, k);
						vector(extrapolatedSBqSPi, i, j, k) = vector(qSPprime_2, i, j, k);
						vector(extrapolatedSBqSPj, i, j, k) = vector(qSPprime_2, i, j, k);
						vector(extrapolatedSBqSPk, i, j, k) = vector(qSPprime_2, i, j, k);
						vector(extrapolatedSBJxSPi, i, j, k) = vector(JxSPprime_2, i, j, k);
						vector(extrapolatedSBJxSPj, i, j, k) = vector(JxSPprime_2, i, j, k);
						vector(extrapolatedSBJxSPk, i, j, k) = vector(JxSPprime_2, i, j, k);
						vector(extrapolatedSBJySPi, i, j, k) = vector(JySPprime_2, i, j, k);
						vector(extrapolatedSBJySPj, i, j, k) = vector(JySPprime_2, i, j, k);
						vector(extrapolatedSBJySPk, i, j, k) = vector(JySPprime_2, i, j, k);
						vector(extrapolatedSBJzSPi, i, j, k) = vector(JzSPprime_2, i, j, k);
						vector(extrapolatedSBJzSPj, i, j, k) = vector(JzSPprime_2, i, j, k);
						vector(extrapolatedSBJzSPk, i, j, k) = vector(JzSPprime_2, i, j, k);
					}
					if ((vector(FOV_xUpper, i, j, k) > 0) && ((i - 1 >= 0 && (vector(FOV_1, i - 1, j, k) > 0 || vector(FOV_2, i - 1, j, k) > 0)) || (i - 2 >= 0 && (vector(FOV_1, i - 2, j, k) > 0 || vector(FOV_2, i - 2, j, k) > 0)) || (i - 3 >= 0 && (vector(FOV_1, i - 3, j, k) > 0 || vector(FOV_2, i - 3, j, k) > 0)))) {
						vector(extrapolatedSBExSPi, i, j, k) = vector(ExSPprime_2, i, j, k);
						vector(extrapolatedSBExSPj, i, j, k) = vector(ExSPprime_2, i, j, k);
						vector(extrapolatedSBExSPk, i, j, k) = vector(ExSPprime_2, i, j, k);
						vector(extrapolatedSBEySPi, i, j, k) = vector(EySPprime_2, i, j, k);
						vector(extrapolatedSBEySPj, i, j, k) = vector(EySPprime_2, i, j, k);
						vector(extrapolatedSBEySPk, i, j, k) = vector(EySPprime_2, i, j, k);
						vector(extrapolatedSBEzSPi, i, j, k) = vector(EzSPprime_2, i, j, k);
						vector(extrapolatedSBEzSPj, i, j, k) = vector(EzSPprime_2, i, j, k);
						vector(extrapolatedSBEzSPk, i, j, k) = vector(EzSPprime_2, i, j, k);
						vector(extrapolatedSBBxSPi, i, j, k) = vector(BxSPprime_2, i, j, k);
						vector(extrapolatedSBBxSPj, i, j, k) = vector(BxSPprime_2, i, j, k);
						vector(extrapolatedSBBxSPk, i, j, k) = vector(BxSPprime_2, i, j, k);
						vector(extrapolatedSBBySPi, i, j, k) = vector(BySPprime_2, i, j, k);
						vector(extrapolatedSBBySPj, i, j, k) = vector(BySPprime_2, i, j, k);
						vector(extrapolatedSBBySPk, i, j, k) = vector(BySPprime_2, i, j, k);
						vector(extrapolatedSBBzSPi, i, j, k) = vector(BzSPprime_2, i, j, k);
						vector(extrapolatedSBBzSPj, i, j, k) = vector(BzSPprime_2, i, j, k);
						vector(extrapolatedSBBzSPk, i, j, k) = vector(BzSPprime_2, i, j, k);
						vector(extrapolatedSBqSPi, i, j, k) = vector(qSPprime_2, i, j, k);
						vector(extrapolatedSBqSPj, i, j, k) = vector(qSPprime_2, i, j, k);
						vector(extrapolatedSBqSPk, i, j, k) = vector(qSPprime_2, i, j, k);
						vector(extrapolatedSBJxSPi, i, j, k) = vector(JxSPprime_2, i, j, k);
						vector(extrapolatedSBJxSPj, i, j, k) = vector(JxSPprime_2, i, j, k);
						vector(extrapolatedSBJxSPk, i, j, k) = vector(JxSPprime_2, i, j, k);
						vector(extrapolatedSBJySPi, i, j, k) = vector(JySPprime_2, i, j, k);
						vector(extrapolatedSBJySPj, i, j, k) = vector(JySPprime_2, i, j, k);
						vector(extrapolatedSBJySPk, i, j, k) = vector(JySPprime_2, i, j, k);
						vector(extrapolatedSBJzSPi, i, j, k) = vector(JzSPprime_2, i, j, k);
						vector(extrapolatedSBJzSPj, i, j, k) = vector(JzSPprime_2, i, j, k);
						vector(extrapolatedSBJzSPk, i, j, k) = vector(JzSPprime_2, i, j, k);
					}
					if ((vector(FOV_yLower, i, j, k) > 0) && ((j + 1 < jlast && (vector(FOV_1,i, j + 1, k) > 0 || vector(FOV_2, i, j + 1, k) > 0)) || (j + 2 < jlast && (vector(FOV_1,i, j + 2, k) > 0 || vector(FOV_2, i, j + 2, k) > 0)) || (j + 3 < jlast && (vector(FOV_1,i, j + 3, k) > 0 || vector(FOV_2, i, j + 3, k) > 0)))) {
						vector(extrapolatedSBExSPi, i, j, k) = vector(ExSPprime_2, i, j, k);
						vector(extrapolatedSBExSPj, i, j, k) = vector(ExSPprime_2, i, j, k);
						vector(extrapolatedSBExSPk, i, j, k) = vector(ExSPprime_2, i, j, k);
						vector(extrapolatedSBEySPi, i, j, k) = vector(EySPprime_2, i, j, k);
						vector(extrapolatedSBEySPj, i, j, k) = vector(EySPprime_2, i, j, k);
						vector(extrapolatedSBEySPk, i, j, k) = vector(EySPprime_2, i, j, k);
						vector(extrapolatedSBEzSPi, i, j, k) = vector(EzSPprime_2, i, j, k);
						vector(extrapolatedSBEzSPj, i, j, k) = vector(EzSPprime_2, i, j, k);
						vector(extrapolatedSBEzSPk, i, j, k) = vector(EzSPprime_2, i, j, k);
						vector(extrapolatedSBBxSPi, i, j, k) = vector(BxSPprime_2, i, j, k);
						vector(extrapolatedSBBxSPj, i, j, k) = vector(BxSPprime_2, i, j, k);
						vector(extrapolatedSBBxSPk, i, j, k) = vector(BxSPprime_2, i, j, k);
						vector(extrapolatedSBBySPi, i, j, k) = vector(BySPprime_2, i, j, k);
						vector(extrapolatedSBBySPj, i, j, k) = vector(BySPprime_2, i, j, k);
						vector(extrapolatedSBBySPk, i, j, k) = vector(BySPprime_2, i, j, k);
						vector(extrapolatedSBBzSPi, i, j, k) = vector(BzSPprime_2, i, j, k);
						vector(extrapolatedSBBzSPj, i, j, k) = vector(BzSPprime_2, i, j, k);
						vector(extrapolatedSBBzSPk, i, j, k) = vector(BzSPprime_2, i, j, k);
						vector(extrapolatedSBqSPi, i, j, k) = vector(qSPprime_2, i, j, k);
						vector(extrapolatedSBqSPj, i, j, k) = vector(qSPprime_2, i, j, k);
						vector(extrapolatedSBqSPk, i, j, k) = vector(qSPprime_2, i, j, k);
						vector(extrapolatedSBJxSPi, i, j, k) = vector(JxSPprime_2, i, j, k);
						vector(extrapolatedSBJxSPj, i, j, k) = vector(JxSPprime_2, i, j, k);
						vector(extrapolatedSBJxSPk, i, j, k) = vector(JxSPprime_2, i, j, k);
						vector(extrapolatedSBJySPi, i, j, k) = vector(JySPprime_2, i, j, k);
						vector(extrapolatedSBJySPj, i, j, k) = vector(JySPprime_2, i, j, k);
						vector(extrapolatedSBJySPk, i, j, k) = vector(JySPprime_2, i, j, k);
						vector(extrapolatedSBJzSPi, i, j, k) = vector(JzSPprime_2, i, j, k);
						vector(extrapolatedSBJzSPj, i, j, k) = vector(JzSPprime_2, i, j, k);
						vector(extrapolatedSBJzSPk, i, j, k) = vector(JzSPprime_2, i, j, k);
					}
					if ((vector(FOV_yUpper, i, j, k) > 0) && ((j - 1 >= 0 && (vector(FOV_1, i, j - 1, k) > 0 || vector(FOV_2, i, j - 1, k) > 0)) || (j - 2 >= 0 && (vector(FOV_1, i, j - 2, k) > 0 || vector(FOV_2, i, j - 2, k) > 0)) || (j - 3 >= 0 && (vector(FOV_1, i, j - 3, k) > 0 || vector(FOV_2, i, j - 3, k) > 0)))) {
						vector(extrapolatedSBExSPi, i, j, k) = vector(ExSPprime_2, i, j, k);
						vector(extrapolatedSBExSPj, i, j, k) = vector(ExSPprime_2, i, j, k);
						vector(extrapolatedSBExSPk, i, j, k) = vector(ExSPprime_2, i, j, k);
						vector(extrapolatedSBEySPi, i, j, k) = vector(EySPprime_2, i, j, k);
						vector(extrapolatedSBEySPj, i, j, k) = vector(EySPprime_2, i, j, k);
						vector(extrapolatedSBEySPk, i, j, k) = vector(EySPprime_2, i, j, k);
						vector(extrapolatedSBEzSPi, i, j, k) = vector(EzSPprime_2, i, j, k);
						vector(extrapolatedSBEzSPj, i, j, k) = vector(EzSPprime_2, i, j, k);
						vector(extrapolatedSBEzSPk, i, j, k) = vector(EzSPprime_2, i, j, k);
						vector(extrapolatedSBBxSPi, i, j, k) = vector(BxSPprime_2, i, j, k);
						vector(extrapolatedSBBxSPj, i, j, k) = vector(BxSPprime_2, i, j, k);
						vector(extrapolatedSBBxSPk, i, j, k) = vector(BxSPprime_2, i, j, k);
						vector(extrapolatedSBBySPi, i, j, k) = vector(BySPprime_2, i, j, k);
						vector(extrapolatedSBBySPj, i, j, k) = vector(BySPprime_2, i, j, k);
						vector(extrapolatedSBBySPk, i, j, k) = vector(BySPprime_2, i, j, k);
						vector(extrapolatedSBBzSPi, i, j, k) = vector(BzSPprime_2, i, j, k);
						vector(extrapolatedSBBzSPj, i, j, k) = vector(BzSPprime_2, i, j, k);
						vector(extrapolatedSBBzSPk, i, j, k) = vector(BzSPprime_2, i, j, k);
						vector(extrapolatedSBqSPi, i, j, k) = vector(qSPprime_2, i, j, k);
						vector(extrapolatedSBqSPj, i, j, k) = vector(qSPprime_2, i, j, k);
						vector(extrapolatedSBqSPk, i, j, k) = vector(qSPprime_2, i, j, k);
						vector(extrapolatedSBJxSPi, i, j, k) = vector(JxSPprime_2, i, j, k);
						vector(extrapolatedSBJxSPj, i, j, k) = vector(JxSPprime_2, i, j, k);
						vector(extrapolatedSBJxSPk, i, j, k) = vector(JxSPprime_2, i, j, k);
						vector(extrapolatedSBJySPi, i, j, k) = vector(JySPprime_2, i, j, k);
						vector(extrapolatedSBJySPj, i, j, k) = vector(JySPprime_2, i, j, k);
						vector(extrapolatedSBJySPk, i, j, k) = vector(JySPprime_2, i, j, k);
						vector(extrapolatedSBJzSPi, i, j, k) = vector(JzSPprime_2, i, j, k);
						vector(extrapolatedSBJzSPj, i, j, k) = vector(JzSPprime_2, i, j, k);
						vector(extrapolatedSBJzSPk, i, j, k) = vector(JzSPprime_2, i, j, k);
					}
					if ((vector(FOV_zLower, i, j, k) > 0) && ((k + 1 < klast && (vector(FOV_1,i, j, k + 1) > 0 || vector(FOV_2, i, j, k + 1) > 0)) || (k + 2 < klast && (vector(FOV_1,i, j, k + 2) > 0 || vector(FOV_2, i, j, k + 2) > 0)) || (k + 3 < klast && (vector(FOV_1,i, j, k + 3) > 0 || vector(FOV_2, i, j, k + 3) > 0)))) {
						vector(extrapolatedSBExSPi, i, j, k) = vector(ExSPprime_2, i, j, k);
						vector(extrapolatedSBExSPj, i, j, k) = vector(ExSPprime_2, i, j, k);
						vector(extrapolatedSBExSPk, i, j, k) = vector(ExSPprime_2, i, j, k);
						vector(extrapolatedSBEySPi, i, j, k) = vector(EySPprime_2, i, j, k);
						vector(extrapolatedSBEySPj, i, j, k) = vector(EySPprime_2, i, j, k);
						vector(extrapolatedSBEySPk, i, j, k) = vector(EySPprime_2, i, j, k);
						vector(extrapolatedSBEzSPi, i, j, k) = vector(EzSPprime_2, i, j, k);
						vector(extrapolatedSBEzSPj, i, j, k) = vector(EzSPprime_2, i, j, k);
						vector(extrapolatedSBEzSPk, i, j, k) = vector(EzSPprime_2, i, j, k);
						vector(extrapolatedSBBxSPi, i, j, k) = vector(BxSPprime_2, i, j, k);
						vector(extrapolatedSBBxSPj, i, j, k) = vector(BxSPprime_2, i, j, k);
						vector(extrapolatedSBBxSPk, i, j, k) = vector(BxSPprime_2, i, j, k);
						vector(extrapolatedSBBySPi, i, j, k) = vector(BySPprime_2, i, j, k);
						vector(extrapolatedSBBySPj, i, j, k) = vector(BySPprime_2, i, j, k);
						vector(extrapolatedSBBySPk, i, j, k) = vector(BySPprime_2, i, j, k);
						vector(extrapolatedSBBzSPi, i, j, k) = vector(BzSPprime_2, i, j, k);
						vector(extrapolatedSBBzSPj, i, j, k) = vector(BzSPprime_2, i, j, k);
						vector(extrapolatedSBBzSPk, i, j, k) = vector(BzSPprime_2, i, j, k);
						vector(extrapolatedSBqSPi, i, j, k) = vector(qSPprime_2, i, j, k);
						vector(extrapolatedSBqSPj, i, j, k) = vector(qSPprime_2, i, j, k);
						vector(extrapolatedSBqSPk, i, j, k) = vector(qSPprime_2, i, j, k);
						vector(extrapolatedSBJxSPi, i, j, k) = vector(JxSPprime_2, i, j, k);
						vector(extrapolatedSBJxSPj, i, j, k) = vector(JxSPprime_2, i, j, k);
						vector(extrapolatedSBJxSPk, i, j, k) = vector(JxSPprime_2, i, j, k);
						vector(extrapolatedSBJySPi, i, j, k) = vector(JySPprime_2, i, j, k);
						vector(extrapolatedSBJySPj, i, j, k) = vector(JySPprime_2, i, j, k);
						vector(extrapolatedSBJySPk, i, j, k) = vector(JySPprime_2, i, j, k);
						vector(extrapolatedSBJzSPi, i, j, k) = vector(JzSPprime_2, i, j, k);
						vector(extrapolatedSBJzSPj, i, j, k) = vector(JzSPprime_2, i, j, k);
						vector(extrapolatedSBJzSPk, i, j, k) = vector(JzSPprime_2, i, j, k);
					}
					if ((vector(FOV_zUpper, i, j, k) > 0) && ((k - 1 >= 0 && (vector(FOV_1, i, j, k - 1) > 0 || vector(FOV_2, i, j, k - 1) > 0)) || (k - 2 >= 0 && (vector(FOV_1, i, j, k - 2) > 0 || vector(FOV_2, i, j, k - 2) > 0)) || (k - 3 >= 0 && (vector(FOV_1, i, j, k - 3) > 0 || vector(FOV_2, i, j, k - 3) > 0)))) {
						vector(extrapolatedSBExSPi, i, j, k) = vector(ExSPprime_2, i, j, k);
						vector(extrapolatedSBExSPj, i, j, k) = vector(ExSPprime_2, i, j, k);
						vector(extrapolatedSBExSPk, i, j, k) = vector(ExSPprime_2, i, j, k);
						vector(extrapolatedSBEySPi, i, j, k) = vector(EySPprime_2, i, j, k);
						vector(extrapolatedSBEySPj, i, j, k) = vector(EySPprime_2, i, j, k);
						vector(extrapolatedSBEySPk, i, j, k) = vector(EySPprime_2, i, j, k);
						vector(extrapolatedSBEzSPi, i, j, k) = vector(EzSPprime_2, i, j, k);
						vector(extrapolatedSBEzSPj, i, j, k) = vector(EzSPprime_2, i, j, k);
						vector(extrapolatedSBEzSPk, i, j, k) = vector(EzSPprime_2, i, j, k);
						vector(extrapolatedSBBxSPi, i, j, k) = vector(BxSPprime_2, i, j, k);
						vector(extrapolatedSBBxSPj, i, j, k) = vector(BxSPprime_2, i, j, k);
						vector(extrapolatedSBBxSPk, i, j, k) = vector(BxSPprime_2, i, j, k);
						vector(extrapolatedSBBySPi, i, j, k) = vector(BySPprime_2, i, j, k);
						vector(extrapolatedSBBySPj, i, j, k) = vector(BySPprime_2, i, j, k);
						vector(extrapolatedSBBySPk, i, j, k) = vector(BySPprime_2, i, j, k);
						vector(extrapolatedSBBzSPi, i, j, k) = vector(BzSPprime_2, i, j, k);
						vector(extrapolatedSBBzSPj, i, j, k) = vector(BzSPprime_2, i, j, k);
						vector(extrapolatedSBBzSPk, i, j, k) = vector(BzSPprime_2, i, j, k);
						vector(extrapolatedSBqSPi, i, j, k) = vector(qSPprime_2, i, j, k);
						vector(extrapolatedSBqSPj, i, j, k) = vector(qSPprime_2, i, j, k);
						vector(extrapolatedSBqSPk, i, j, k) = vector(qSPprime_2, i, j, k);
						vector(extrapolatedSBJxSPi, i, j, k) = vector(JxSPprime_2, i, j, k);
						vector(extrapolatedSBJxSPj, i, j, k) = vector(JxSPprime_2, i, j, k);
						vector(extrapolatedSBJxSPk, i, j, k) = vector(JxSPprime_2, i, j, k);
						vector(extrapolatedSBJySPi, i, j, k) = vector(JySPprime_2, i, j, k);
						vector(extrapolatedSBJySPj, i, j, k) = vector(JySPprime_2, i, j, k);
						vector(extrapolatedSBJySPk, i, j, k) = vector(JySPprime_2, i, j, k);
						vector(extrapolatedSBJzSPi, i, j, k) = vector(JzSPprime_2, i, j, k);
						vector(extrapolatedSBJzSPj, i, j, k) = vector(JzSPprime_2, i, j, k);
						vector(extrapolatedSBJzSPk, i, j, k) = vector(JzSPprime_2, i, j, k);
					}
				}
			}
		}
	}
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch >& patch = *p_it;
		double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_2 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_2_id).get())->getPointer();
		double* FOV_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_4_id).get())->getPointer();
		double* FOV_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_5_id).get())->getPointer();
		double* FOV_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_7_id).get())->getPointer();
		double* FOV_8 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_8_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
		double* FOV_zLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zLower_id).get())->getPointer();
		double* FOV_zUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zUpper_id).get())->getPointer();
	
		double* qSPprime_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_qSPprime_7_id).get())->getPointer();
		double* JxSPprime_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_JxSPprime_7_id).get())->getPointer();
		double* JySPprime_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_JySPprime_7_id).get())->getPointer();
		double* JzSPprime_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_JzSPprime_7_id).get())->getPointer();
		double* FluxSBjEx_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjEx_7_id).get())->getPointer();
		double* BzSPprime_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_BzSPprime_7_id).get())->getPointer();
		double* FluxSBkEx_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkEx_7_id).get())->getPointer();
		double* BySPprime_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_BySPprime_7_id).get())->getPointer();
		double* FluxSBiEy_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiEy_7_id).get())->getPointer();
		double* FluxSBkEy_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkEy_7_id).get())->getPointer();
		double* BxSPprime_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_BxSPprime_7_id).get())->getPointer();
		double* FluxSBiEz_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiEz_7_id).get())->getPointer();
		double* FluxSBjEz_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjEz_7_id).get())->getPointer();
		double* FluxSBjBx_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjBx_7_id).get())->getPointer();
		double* EzSPprime_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_EzSPprime_7_id).get())->getPointer();
		double* FluxSBkBx_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkBx_7_id).get())->getPointer();
		double* EySPprime_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_EySPprime_7_id).get())->getPointer();
		double* FluxSBiBy_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiBy_7_id).get())->getPointer();
		double* FluxSBkBy_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkBy_7_id).get())->getPointer();
		double* ExSPprime_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_ExSPprime_7_id).get())->getPointer();
		double* FluxSBiBz_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiBz_7_id).get())->getPointer();
		double* FluxSBjBz_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjBz_7_id).get())->getPointer();
		double* SpeedSBjEx_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjEx_7_id).get())->getPointer();
		double* SpeedSBkEx_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkEx_7_id).get())->getPointer();
		double* SpeedSBiEy_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiEy_7_id).get())->getPointer();
		double* SpeedSBkEy_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkEy_7_id).get())->getPointer();
		double* SpeedSBiEz_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiEz_7_id).get())->getPointer();
		double* SpeedSBjEz_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjEz_7_id).get())->getPointer();
		double* SpeedSBjBx_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjBx_7_id).get())->getPointer();
		double* SpeedSBkBx_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkBx_7_id).get())->getPointer();
		double* SpeedSBiBy_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiBy_7_id).get())->getPointer();
		double* SpeedSBkBy_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkBy_7_id).get())->getPointer();
		double* SpeedSBiBz_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiBz_7_id).get())->getPointer();
		double* SpeedSBjBz_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjBz_7_id).get())->getPointer();
		double* FluxSBjEx_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjEx_4_id).get())->getPointer();
		double* FluxSBkEx_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkEx_4_id).get())->getPointer();
		double* FluxSBiEy_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiEy_4_id).get())->getPointer();
		double* FluxSBkEy_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkEy_4_id).get())->getPointer();
		double* FluxSBiEz_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiEz_4_id).get())->getPointer();
		double* FluxSBjEz_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjEz_4_id).get())->getPointer();
		double* FluxSBjBx_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjBx_4_id).get())->getPointer();
		double* FluxSBkBx_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkBx_4_id).get())->getPointer();
		double* FluxSBiBy_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiBy_4_id).get())->getPointer();
		double* FluxSBkBy_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkBy_4_id).get())->getPointer();
		double* FluxSBiBz_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiBz_4_id).get())->getPointer();
		double* FluxSBjBz_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjBz_4_id).get())->getPointer();
		double* SpeedSBjEx_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjEx_4_id).get())->getPointer();
		double* SpeedSBkEx_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkEx_4_id).get())->getPointer();
		double* SpeedSBiEy_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiEy_4_id).get())->getPointer();
		double* SpeedSBkEy_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkEy_4_id).get())->getPointer();
		double* SpeedSBiEz_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiEz_4_id).get())->getPointer();
		double* SpeedSBjEz_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjEz_4_id).get())->getPointer();
		double* SpeedSBjBx_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjBx_4_id).get())->getPointer();
		double* SpeedSBkBx_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkBx_4_id).get())->getPointer();
		double* SpeedSBiBy_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiBy_4_id).get())->getPointer();
		double* SpeedSBkBy_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkBy_4_id).get())->getPointer();
		double* SpeedSBiBz_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiBz_4_id).get())->getPointer();
		double* SpeedSBjBz_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjBz_4_id).get())->getPointer();
		double* FluxSBjEx_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjEx_5_id).get())->getPointer();
		double* FluxSBkEx_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkEx_5_id).get())->getPointer();
		double* FluxSBiEy_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiEy_5_id).get())->getPointer();
		double* FluxSBkEy_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkEy_5_id).get())->getPointer();
		double* FluxSBiEz_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiEz_5_id).get())->getPointer();
		double* FluxSBjEz_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjEz_5_id).get())->getPointer();
		double* FluxSBjBx_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjBx_5_id).get())->getPointer();
		double* FluxSBkBx_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkBx_5_id).get())->getPointer();
		double* FluxSBiBy_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiBy_5_id).get())->getPointer();
		double* FluxSBkBy_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkBy_5_id).get())->getPointer();
		double* FluxSBiBz_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiBz_5_id).get())->getPointer();
		double* FluxSBjBz_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjBz_5_id).get())->getPointer();
		double* SpeedSBjEx_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjEx_5_id).get())->getPointer();
		double* SpeedSBkEx_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkEx_5_id).get())->getPointer();
		double* SpeedSBiEy_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiEy_5_id).get())->getPointer();
		double* SpeedSBkEy_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkEy_5_id).get())->getPointer();
		double* SpeedSBiEz_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiEz_5_id).get())->getPointer();
		double* SpeedSBjEz_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjEz_5_id).get())->getPointer();
		double* SpeedSBjBx_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjBx_5_id).get())->getPointer();
		double* SpeedSBkBx_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkBx_5_id).get())->getPointer();
		double* SpeedSBiBy_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiBy_5_id).get())->getPointer();
		double* SpeedSBkBy_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkBy_5_id).get())->getPointer();
		double* SpeedSBiBz_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiBz_5_id).get())->getPointer();
		double* SpeedSBjBz_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjBz_5_id).get())->getPointer();
	
		//Get the dimensions of the patch
		hier::Box pbox = patch->getBox();
		const hier::Index boxfirst = patch->getBox().lower();
		const hier::Index boxlast = patch->getBox().upper();
	
		//Get delta spaces into an array. dx, dy, dz.
		std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
		const double* dx  = patch_geom->getDx();
	
		//Auxiliary definitions
		int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
		int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
		int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;
		for(int k = 0; k < klast; k++) {
			for(int j = 0; j < jlast; j++) {
				for(int i = 0; i < ilast; i++) {
					if ((vector(FOV_7, i, j, k) > 0 || (((i + 1 < ilast && vector(FOV_7, i + 1, j, k) > 0) || (i + 2 < ilast && vector(FOV_7, i + 2, j, k) > 0) || (j + 1 < jlast && vector(FOV_7, i, j + 1, k) > 0) || (j + 2 < jlast && vector(FOV_7, i, j + 2, k) > 0) || (k + 1 < klast && vector(FOV_7, i, j, k + 1) > 0) || (k + 2 < klast && vector(FOV_7, i, j, k + 2) > 0)) || ((i - 1 >= 0 && vector(FOV_7, i - 1, j, k) > 0) || (i - 2 >= 0 && vector(FOV_7, i - 2, j, k) > 0) || (j - 1 >= 0 && vector(FOV_7, i, j - 1, k) > 0) || (j - 2 >= 0 && vector(FOV_7, i, j - 2, k) > 0) || (k - 1 >= 0 && vector(FOV_7, i, j, k - 1) > 0) || (k - 2 >= 0 && vector(FOV_7, i, j, k - 2) > 0))) || (d_tappering && (neighbourTappering(FOV_7, i, j, k, ilast, jlast, klast, 2, 6 * d_sim_substep[ln] - 2))))) {
						vector(qSPprime_7, i, j, k) = 0.0;
						vector(JxSPprime_7, i, j, k) = 0.0;
						vector(JySPprime_7, i, j, k) = 0.0;
						vector(JzSPprime_7, i, j, k) = 0.0;
						vector(FluxSBjEx_7, i, j, k) = FjEx_AirI(vector(BzSPprime_7, i, j, k), c, dx, simPlat_dt, ilast, jlast);
						vector(FluxSBkEx_7, i, j, k) = FkEx_AirI(vector(BySPprime_7, i, j, k), c, dx, simPlat_dt, ilast, jlast);
						vector(FluxSBiEy_7, i, j, k) = FiEy_AirI(vector(BzSPprime_7, i, j, k), c, dx, simPlat_dt, ilast, jlast);
						vector(FluxSBkEy_7, i, j, k) = FkEy_AirI(vector(BxSPprime_7, i, j, k), c, dx, simPlat_dt, ilast, jlast);
						vector(FluxSBiEz_7, i, j, k) = FiEz_AirI(vector(BySPprime_7, i, j, k), c, dx, simPlat_dt, ilast, jlast);
						vector(FluxSBjEz_7, i, j, k) = FjEz_AirI(vector(BxSPprime_7, i, j, k), c, dx, simPlat_dt, ilast, jlast);
						vector(FluxSBjBx_7, i, j, k) = FjBx_AirI(vector(EzSPprime_7, i, j, k), c, dx, simPlat_dt, ilast, jlast);
						vector(FluxSBkBx_7, i, j, k) = FkBx_AirI(vector(EySPprime_7, i, j, k), c, dx, simPlat_dt, ilast, jlast);
						vector(FluxSBiBy_7, i, j, k) = FiBy_AirI(vector(EzSPprime_7, i, j, k), c, dx, simPlat_dt, ilast, jlast);
						vector(FluxSBkBy_7, i, j, k) = FkBy_AirI(vector(ExSPprime_7, i, j, k), c, dx, simPlat_dt, ilast, jlast);
						vector(FluxSBiBz_7, i, j, k) = FiBz_AirI(vector(EySPprime_7, i, j, k), c, dx, simPlat_dt, ilast, jlast);
						vector(FluxSBjBz_7, i, j, k) = FjBz_AirI(vector(ExSPprime_7, i, j, k), c, dx, simPlat_dt, ilast, jlast);
						vector(SpeedSBjEx_7, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBkEx_7, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBiEy_7, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBkEy_7, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBiEz_7, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBjEz_7, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBjBx_7, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBkBx_7, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBiBy_7, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBkBy_7, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBiBz_7, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBjBz_7, i, j, k) = MAX(0.0, MAX(c, -c));
					}
					if ((vector(FOV_5, i, j, k) > 0 || (((i + 1 < ilast && vector(FOV_5, i + 1, j, k) > 0) || (i + 2 < ilast && vector(FOV_5, i + 2, j, k) > 0) || (j + 1 < jlast && vector(FOV_5, i, j + 1, k) > 0) || (j + 2 < jlast && vector(FOV_5, i, j + 2, k) > 0) || (k + 1 < klast && vector(FOV_5, i, j, k + 1) > 0) || (k + 2 < klast && vector(FOV_5, i, j, k + 2) > 0)) || ((i - 1 >= 0 && vector(FOV_5, i - 1, j, k) > 0) || (i - 2 >= 0 && vector(FOV_5, i - 2, j, k) > 0) || (j - 1 >= 0 && vector(FOV_5, i, j - 1, k) > 0) || (j - 2 >= 0 && vector(FOV_5, i, j - 2, k) > 0) || (k - 1 >= 0 && vector(FOV_5, i, j, k - 1) > 0) || (k - 2 >= 0 && vector(FOV_5, i, j, k - 2) > 0))) || (d_tappering && (neighbourTappering(FOV_5, i, j, k, ilast, jlast, klast, 2, 6 * d_sim_substep[ln] - 2))))) {
						vector(FluxSBjEx_5, i, j, k) = FjEx_OtherI(dx, simPlat_dt, ilast, jlast);
						vector(FluxSBkEx_5, i, j, k) = FkEx_OtherI(dx, simPlat_dt, ilast, jlast);
						vector(FluxSBiEy_5, i, j, k) = FiEy_OtherI(dx, simPlat_dt, ilast, jlast);
						vector(FluxSBkEy_5, i, j, k) = FkEy_OtherI(dx, simPlat_dt, ilast, jlast);
						vector(FluxSBiEz_5, i, j, k) = FiEz_OtherI(dx, simPlat_dt, ilast, jlast);
						vector(FluxSBjEz_5, i, j, k) = FjEz_OtherI(dx, simPlat_dt, ilast, jlast);
						vector(FluxSBjBx_5, i, j, k) = FjBx_OtherI(dx, simPlat_dt, ilast, jlast);
						vector(FluxSBkBx_5, i, j, k) = FkBx_OtherI(dx, simPlat_dt, ilast, jlast);
						vector(FluxSBiBy_5, i, j, k) = FiBy_OtherI(dx, simPlat_dt, ilast, jlast);
						vector(FluxSBkBy_5, i, j, k) = FkBy_OtherI(dx, simPlat_dt, ilast, jlast);
						vector(FluxSBiBz_5, i, j, k) = FiBz_OtherI(dx, simPlat_dt, ilast, jlast);
						vector(FluxSBjBz_5, i, j, k) = FjBz_OtherI(dx, simPlat_dt, ilast, jlast);
						vector(SpeedSBjEx_5, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBkEx_5, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBiEy_5, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBkEy_5, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBiEz_5, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBjEz_5, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBjBx_5, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBkBx_5, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBiBy_5, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBkBy_5, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBiBz_5, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBjBz_5, i, j, k) = MAX(0.0, MAX(c, -c));
					}
					if ((vector(FOV_4, i, j, k) > 0 || (((i + 1 < ilast && vector(FOV_4, i + 1, j, k) > 0) || (i + 2 < ilast && vector(FOV_4, i + 2, j, k) > 0) || (i + 3 < ilast && vector(FOV_4, i + 3, j, k) > 0) || (j + 1 < jlast && vector(FOV_4, i, j + 1, k) > 0) || (j + 2 < jlast && vector(FOV_4, i, j + 2, k) > 0) || (j + 3 < jlast && vector(FOV_4, i, j + 3, k) > 0) || (k + 1 < klast && vector(FOV_4, i, j, k + 1) > 0) || (k + 2 < klast && vector(FOV_4, i, j, k + 2) > 0) || (k + 3 < klast && vector(FOV_4, i, j, k + 3) > 0)) || ((i - 1 >= 0 && vector(FOV_4, i - 1, j, k) > 0) || (i - 2 >= 0 && vector(FOV_4, i - 2, j, k) > 0) || (i - 3 >= 0 && vector(FOV_4, i - 3, j, k) > 0) || (j - 1 >= 0 && vector(FOV_4, i, j - 1, k) > 0) || (j - 2 >= 0 && vector(FOV_4, i, j - 2, k) > 0) || (j - 3 >= 0 && vector(FOV_4, i, j - 3, k) > 0) || (k - 1 >= 0 && vector(FOV_4, i, j, k - 1) > 0) || (k - 2 >= 0 && vector(FOV_4, i, j, k - 2) > 0) || (k - 3 >= 0 && vector(FOV_4, i, j, k - 3) > 0))) || (d_tappering && (neighbourTappering(FOV_4, i, j, k, ilast, jlast, klast, 3, 5 * d_sim_substep[ln] - 1))))) {
						vector(FluxSBjEx_4, i, j, k) = FjEx_SoilS(dx, simPlat_dt, ilast, jlast);
						vector(FluxSBkEx_4, i, j, k) = FkEx_SoilS(dx, simPlat_dt, ilast, jlast);
						vector(FluxSBiEy_4, i, j, k) = FiEy_SoilS(dx, simPlat_dt, ilast, jlast);
						vector(FluxSBkEy_4, i, j, k) = FkEy_SoilS(dx, simPlat_dt, ilast, jlast);
						vector(FluxSBiEz_4, i, j, k) = FiEz_SoilS(dx, simPlat_dt, ilast, jlast);
						vector(FluxSBjEz_4, i, j, k) = FjEz_SoilS(dx, simPlat_dt, ilast, jlast);
						vector(FluxSBjBx_4, i, j, k) = FjBx_SoilS(dx, simPlat_dt, ilast, jlast);
						vector(FluxSBkBx_4, i, j, k) = FkBx_SoilS(dx, simPlat_dt, ilast, jlast);
						vector(FluxSBiBy_4, i, j, k) = FiBy_SoilS(dx, simPlat_dt, ilast, jlast);
						vector(FluxSBkBy_4, i, j, k) = FkBy_SoilS(dx, simPlat_dt, ilast, jlast);
						vector(FluxSBiBz_4, i, j, k) = FiBz_SoilS(dx, simPlat_dt, ilast, jlast);
						vector(FluxSBjBz_4, i, j, k) = FjBz_SoilS(dx, simPlat_dt, ilast, jlast);
						vector(SpeedSBjEx_4, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBkEx_4, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBiEy_4, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBkEy_4, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBiEz_4, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBjEz_4, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBjBx_4, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBkBx_4, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBiBy_4, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBkBy_4, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBiBz_4, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBjBz_4, i, j, k) = MAX(0.0, MAX(c, -c));
					}
				}
			}
		}
	}
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch >& patch = *p_it;
		double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_2 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_2_id).get())->getPointer();
		double* FOV_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_4_id).get())->getPointer();
		double* FOV_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_5_id).get())->getPointer();
		double* FOV_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_7_id).get())->getPointer();
		double* FOV_8 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_8_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
		double* FOV_zLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zLower_id).get())->getPointer();
		double* FOV_zUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zUpper_id).get())->getPointer();
	
		double* JxSPprime_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_JxSPprime_7_id).get())->getPointer();
		double* JySPprime_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_JySPprime_7_id).get())->getPointer();
		double* JzSPprime_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_JzSPprime_7_id).get())->getPointer();
		double* ExSPprime_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_ExSPprime_7_id).get())->getPointer();
		double* FluxSBjEx_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjEx_7_id).get())->getPointer();
		double* SpeedSBjEx_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjEx_7_id).get())->getPointer();
		double* FluxSBkEx_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkEx_7_id).get())->getPointer();
		double* SpeedSBkEx_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkEx_7_id).get())->getPointer();
		double* EySPprime_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_EySPprime_7_id).get())->getPointer();
		double* FluxSBiEy_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiEy_7_id).get())->getPointer();
		double* SpeedSBiEy_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiEy_7_id).get())->getPointer();
		double* FluxSBkEy_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkEy_7_id).get())->getPointer();
		double* SpeedSBkEy_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkEy_7_id).get())->getPointer();
		double* EzSPprime_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_EzSPprime_7_id).get())->getPointer();
		double* FluxSBiEz_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiEz_7_id).get())->getPointer();
		double* SpeedSBiEz_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiEz_7_id).get())->getPointer();
		double* FluxSBjEz_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjEz_7_id).get())->getPointer();
		double* SpeedSBjEz_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjEz_7_id).get())->getPointer();
		double* BxSPprime_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_BxSPprime_7_id).get())->getPointer();
		double* FluxSBjBx_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjBx_7_id).get())->getPointer();
		double* SpeedSBjBx_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjBx_7_id).get())->getPointer();
		double* FluxSBkBx_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkBx_7_id).get())->getPointer();
		double* SpeedSBkBx_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkBx_7_id).get())->getPointer();
		double* BySPprime_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_BySPprime_7_id).get())->getPointer();
		double* FluxSBiBy_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiBy_7_id).get())->getPointer();
		double* SpeedSBiBy_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiBy_7_id).get())->getPointer();
		double* FluxSBkBy_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkBy_7_id).get())->getPointer();
		double* SpeedSBkBy_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkBy_7_id).get())->getPointer();
		double* BzSPprime_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_BzSPprime_7_id).get())->getPointer();
		double* FluxSBiBz_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiBz_7_id).get())->getPointer();
		double* SpeedSBiBz_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiBz_7_id).get())->getPointer();
		double* FluxSBjBz_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjBz_7_id).get())->getPointer();
		double* SpeedSBjBz_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjBz_7_id).get())->getPointer();
		double* K2SBEx_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_K2SBEx_7_id).get())->getPointer();
		double* Ex_p = ((pdat::NodeData<double> *) patch->getPatchData(d_Ex_p_id).get())->getPointer();
		double* K2SBEy_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_K2SBEy_7_id).get())->getPointer();
		double* Ey_p = ((pdat::NodeData<double> *) patch->getPatchData(d_Ey_p_id).get())->getPointer();
		double* K2SBEz_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_K2SBEz_7_id).get())->getPointer();
		double* Ez_p = ((pdat::NodeData<double> *) patch->getPatchData(d_Ez_p_id).get())->getPointer();
		double* K2SBBx_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_K2SBBx_7_id).get())->getPointer();
		double* Bx_p = ((pdat::NodeData<double> *) patch->getPatchData(d_Bx_p_id).get())->getPointer();
		double* K2SBBy_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_K2SBBy_7_id).get())->getPointer();
		double* By_p = ((pdat::NodeData<double> *) patch->getPatchData(d_By_p_id).get())->getPointer();
		double* K2SBBz_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_K2SBBz_7_id).get())->getPointer();
		double* Bz_p = ((pdat::NodeData<double> *) patch->getPatchData(d_Bz_p_id).get())->getPointer();
		double* ExSPprimeprime_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_ExSPprimeprime_7_id).get())->getPointer();
		double* EySPprimeprime_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_EySPprimeprime_7_id).get())->getPointer();
		double* EzSPprimeprime_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_EzSPprimeprime_7_id).get())->getPointer();
		double* BxSPprimeprime_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_BxSPprimeprime_7_id).get())->getPointer();
		double* BySPprimeprime_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_BySPprimeprime_7_id).get())->getPointer();
		double* BzSPprimeprime_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_BzSPprimeprime_7_id).get())->getPointer();
		double* ExSPprime_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_ExSPprime_4_id).get())->getPointer();
		double* FluxSBjEx_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjEx_4_id).get())->getPointer();
		double* SpeedSBjEx_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjEx_4_id).get())->getPointer();
		double* FluxSBkEx_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkEx_4_id).get())->getPointer();
		double* SpeedSBkEx_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkEx_4_id).get())->getPointer();
		double* EySPprime_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_EySPprime_4_id).get())->getPointer();
		double* FluxSBiEy_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiEy_4_id).get())->getPointer();
		double* SpeedSBiEy_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiEy_4_id).get())->getPointer();
		double* FluxSBkEy_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkEy_4_id).get())->getPointer();
		double* SpeedSBkEy_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkEy_4_id).get())->getPointer();
		double* EzSPprime_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_EzSPprime_4_id).get())->getPointer();
		double* FluxSBiEz_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiEz_4_id).get())->getPointer();
		double* SpeedSBiEz_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiEz_4_id).get())->getPointer();
		double* FluxSBjEz_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjEz_4_id).get())->getPointer();
		double* SpeedSBjEz_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjEz_4_id).get())->getPointer();
		double* BxSPprime_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_BxSPprime_4_id).get())->getPointer();
		double* FluxSBjBx_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjBx_4_id).get())->getPointer();
		double* SpeedSBjBx_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjBx_4_id).get())->getPointer();
		double* FluxSBkBx_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkBx_4_id).get())->getPointer();
		double* SpeedSBkBx_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkBx_4_id).get())->getPointer();
		double* BySPprime_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_BySPprime_4_id).get())->getPointer();
		double* FluxSBiBy_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiBy_4_id).get())->getPointer();
		double* SpeedSBiBy_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiBy_4_id).get())->getPointer();
		double* FluxSBkBy_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkBy_4_id).get())->getPointer();
		double* SpeedSBkBy_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkBy_4_id).get())->getPointer();
		double* BzSPprime_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_BzSPprime_4_id).get())->getPointer();
		double* FluxSBiBz_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiBz_4_id).get())->getPointer();
		double* SpeedSBiBz_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiBz_4_id).get())->getPointer();
		double* FluxSBjBz_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjBz_4_id).get())->getPointer();
		double* SpeedSBjBz_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjBz_4_id).get())->getPointer();
		double* K2SBEx_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_K2SBEx_4_id).get())->getPointer();
		double* K2SBEy_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_K2SBEy_4_id).get())->getPointer();
		double* K2SBEz_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_K2SBEz_4_id).get())->getPointer();
		double* K2SBBx_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_K2SBBx_4_id).get())->getPointer();
		double* K2SBBy_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_K2SBBy_4_id).get())->getPointer();
		double* K2SBBz_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_K2SBBz_4_id).get())->getPointer();
		double* ExSPprimeprime_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_ExSPprimeprime_4_id).get())->getPointer();
		double* EySPprimeprime_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_EySPprimeprime_4_id).get())->getPointer();
		double* EzSPprimeprime_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_EzSPprimeprime_4_id).get())->getPointer();
		double* BxSPprimeprime_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_BxSPprimeprime_4_id).get())->getPointer();
		double* BySPprimeprime_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_BySPprimeprime_4_id).get())->getPointer();
		double* BzSPprimeprime_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_BzSPprimeprime_4_id).get())->getPointer();
		double* ExSPprime_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_ExSPprime_5_id).get())->getPointer();
		double* FluxSBjEx_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjEx_5_id).get())->getPointer();
		double* SpeedSBjEx_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjEx_5_id).get())->getPointer();
		double* FluxSBkEx_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkEx_5_id).get())->getPointer();
		double* SpeedSBkEx_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkEx_5_id).get())->getPointer();
		double* EySPprime_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_EySPprime_5_id).get())->getPointer();
		double* FluxSBiEy_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiEy_5_id).get())->getPointer();
		double* SpeedSBiEy_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiEy_5_id).get())->getPointer();
		double* FluxSBkEy_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkEy_5_id).get())->getPointer();
		double* SpeedSBkEy_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkEy_5_id).get())->getPointer();
		double* EzSPprime_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_EzSPprime_5_id).get())->getPointer();
		double* FluxSBiEz_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiEz_5_id).get())->getPointer();
		double* SpeedSBiEz_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiEz_5_id).get())->getPointer();
		double* FluxSBjEz_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjEz_5_id).get())->getPointer();
		double* SpeedSBjEz_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjEz_5_id).get())->getPointer();
		double* BxSPprime_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_BxSPprime_5_id).get())->getPointer();
		double* FluxSBjBx_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjBx_5_id).get())->getPointer();
		double* SpeedSBjBx_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjBx_5_id).get())->getPointer();
		double* FluxSBkBx_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkBx_5_id).get())->getPointer();
		double* SpeedSBkBx_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkBx_5_id).get())->getPointer();
		double* BySPprime_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_BySPprime_5_id).get())->getPointer();
		double* FluxSBiBy_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiBy_5_id).get())->getPointer();
		double* SpeedSBiBy_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiBy_5_id).get())->getPointer();
		double* FluxSBkBy_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkBy_5_id).get())->getPointer();
		double* SpeedSBkBy_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkBy_5_id).get())->getPointer();
		double* BzSPprime_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_BzSPprime_5_id).get())->getPointer();
		double* FluxSBiBz_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiBz_5_id).get())->getPointer();
		double* SpeedSBiBz_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiBz_5_id).get())->getPointer();
		double* FluxSBjBz_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjBz_5_id).get())->getPointer();
		double* SpeedSBjBz_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjBz_5_id).get())->getPointer();
		double* K2SBEx_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_K2SBEx_5_id).get())->getPointer();
		double* K2SBEy_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_K2SBEy_5_id).get())->getPointer();
		double* K2SBEz_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_K2SBEz_5_id).get())->getPointer();
		double* K2SBBx_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_K2SBBx_5_id).get())->getPointer();
		double* K2SBBy_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_K2SBBy_5_id).get())->getPointer();
		double* K2SBBz_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_K2SBBz_5_id).get())->getPointer();
		double* ExSPprimeprime_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_ExSPprimeprime_5_id).get())->getPointer();
		double* EySPprimeprime_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_EySPprimeprime_5_id).get())->getPointer();
		double* EzSPprimeprime_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_EzSPprimeprime_5_id).get())->getPointer();
		double* BxSPprimeprime_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_BxSPprimeprime_5_id).get())->getPointer();
		double* BySPprimeprime_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_BySPprimeprime_5_id).get())->getPointer();
		double* BzSPprimeprime_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_BzSPprimeprime_5_id).get())->getPointer();
		double fluxAccSBEx_7, fluxAccSBEy_7, fluxAccSBEz_7, fluxAccSBBx_7, fluxAccSBBy_7, fluxAccSBBz_7, fluxAccSBEx_4, fluxAccSBEy_4, fluxAccSBEz_4, fluxAccSBBx_4, fluxAccSBBy_4, fluxAccSBBz_4, fluxAccSBEx_5, fluxAccSBEy_5, fluxAccSBEz_5, fluxAccSBBx_5, fluxAccSBBy_5, fluxAccSBBz_5;
	
		//Get the dimensions of the patch
		hier::Box pbox = patch->getBox();
		const hier::Index boxfirst = patch->getBox().lower();
		const hier::Index boxlast = patch->getBox().upper();
	
		//Get delta spaces into an array. dx, dy, dz.
		std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
		const double* dx  = patch_geom->getDx();
	
		//Auxiliary definitions
		int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
		int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
		int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;
		for(int k = 0; k < klast; k++) {
			for(int j = 0; j < jlast; j++) {
				for(int i = 0; i < ilast; i++) {
					if ((vector(FOV_7, i, j, k) > 0) && (i + 2 < ilast && i - 2 >= 0 && j + 2 < jlast && j - 2 >= 0 && k + 2 < klast && k - 2 >= 0)) {
						fluxAccSBEx_7 = -mu * vector(JxSPprime_7, i, j, k);
						fluxAccSBEy_7 = -mu * vector(JySPprime_7, i, j, k);
						fluxAccSBEz_7 = -mu * vector(JzSPprime_7, i, j, k);
						fluxAccSBBx_7 = 0.0;
						fluxAccSBBy_7 = 0.0;
						fluxAccSBBz_7 = 0.0;
						fluxAccSBEx_7 = fluxAccSBEx_7 - FDOCj(ExSPprime_7, FluxSBjEx_7, SpeedSBjEx_7, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBEx_7 = fluxAccSBEx_7 - FDOCk(ExSPprime_7, FluxSBkEx_7, SpeedSBkEx_7, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBEy_7 = fluxAccSBEy_7 - FDOCi(EySPprime_7, FluxSBiEy_7, SpeedSBiEy_7, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBEy_7 = fluxAccSBEy_7 - FDOCk(EySPprime_7, FluxSBkEy_7, SpeedSBkEy_7, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBEz_7 = fluxAccSBEz_7 - FDOCi(EzSPprime_7, FluxSBiEz_7, SpeedSBiEz_7, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBEz_7 = fluxAccSBEz_7 - FDOCj(EzSPprime_7, FluxSBjEz_7, SpeedSBjEz_7, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBBx_7 = fluxAccSBBx_7 - FDOCj(BxSPprime_7, FluxSBjBx_7, SpeedSBjBx_7, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBBx_7 = fluxAccSBBx_7 - FDOCk(BxSPprime_7, FluxSBkBx_7, SpeedSBkBx_7, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBBy_7 = fluxAccSBBy_7 - FDOCi(BySPprime_7, FluxSBiBy_7, SpeedSBiBy_7, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBBy_7 = fluxAccSBBy_7 - FDOCk(BySPprime_7, FluxSBkBy_7, SpeedSBkBy_7, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBBz_7 = fluxAccSBBz_7 - FDOCi(BzSPprime_7, FluxSBiBz_7, SpeedSBiBz_7, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBBz_7 = fluxAccSBBz_7 - FDOCj(BzSPprime_7, FluxSBjBz_7, SpeedSBjBz_7, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(K2SBEx_7, i, j, k) = RK3(2, Ex_p, fluxAccSBEx_7, 0.0, 0.0, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(K2SBEy_7, i, j, k) = RK3(2, Ey_p, fluxAccSBEy_7, 0.0, 0.0, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(K2SBEz_7, i, j, k) = RK3(2, Ez_p, fluxAccSBEz_7, 0.0, 0.0, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(K2SBBx_7, i, j, k) = RK3(2, Bx_p, fluxAccSBBx_7, 0.0, 0.0, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(K2SBBy_7, i, j, k) = RK3(2, By_p, fluxAccSBBy_7, 0.0, 0.0, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(K2SBBz_7, i, j, k) = RK3(2, Bz_p, fluxAccSBBz_7, 0.0, 0.0, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(ExSPprimeprime_7, i, j, k) = vector(Ex_p, i, j, k) + 2.0 / 3.0 * vector(K2SBEx_7, i, j, k);
						vector(EySPprimeprime_7, i, j, k) = vector(Ey_p, i, j, k) + 2.0 / 3.0 * vector(K2SBEy_7, i, j, k);
						vector(EzSPprimeprime_7, i, j, k) = vector(Ez_p, i, j, k) + 2.0 / 3.0 * vector(K2SBEz_7, i, j, k);
						vector(BxSPprimeprime_7, i, j, k) = vector(Bx_p, i, j, k) + 2.0 / 3.0 * vector(K2SBBx_7, i, j, k);
						vector(BySPprimeprime_7, i, j, k) = vector(By_p, i, j, k) + 2.0 / 3.0 * vector(K2SBBy_7, i, j, k);
						vector(BzSPprimeprime_7, i, j, k) = vector(Bz_p, i, j, k) + 2.0 / 3.0 * vector(K2SBBz_7, i, j, k);
					}
					if ((vector(FOV_5, i, j, k) > 0) && (i + 2 < ilast && i - 2 >= 0 && j + 2 < jlast && j - 2 >= 0 && k + 2 < klast && k - 2 >= 0)) {
						fluxAccSBEx_5 = 0.0;
						fluxAccSBEy_5 = 0.0;
						fluxAccSBEz_5 = 0.0;
						fluxAccSBBx_5 = 0.0;
						fluxAccSBBy_5 = 0.0;
						fluxAccSBBz_5 = 0.0;
						fluxAccSBEx_5 = fluxAccSBEx_5 - FDOCj(ExSPprime_5, FluxSBjEx_5, SpeedSBjEx_5, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBEx_5 = fluxAccSBEx_5 - FDOCk(ExSPprime_5, FluxSBkEx_5, SpeedSBkEx_5, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBEy_5 = fluxAccSBEy_5 - FDOCi(EySPprime_5, FluxSBiEy_5, SpeedSBiEy_5, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBEy_5 = fluxAccSBEy_5 - FDOCk(EySPprime_5, FluxSBkEy_5, SpeedSBkEy_5, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBEz_5 = fluxAccSBEz_5 - FDOCi(EzSPprime_5, FluxSBiEz_5, SpeedSBiEz_5, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBEz_5 = fluxAccSBEz_5 - FDOCj(EzSPprime_5, FluxSBjEz_5, SpeedSBjEz_5, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBBx_5 = fluxAccSBBx_5 - FDOCj(BxSPprime_5, FluxSBjBx_5, SpeedSBjBx_5, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBBx_5 = fluxAccSBBx_5 - FDOCk(BxSPprime_5, FluxSBkBx_5, SpeedSBkBx_5, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBBy_5 = fluxAccSBBy_5 - FDOCi(BySPprime_5, FluxSBiBy_5, SpeedSBiBy_5, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBBy_5 = fluxAccSBBy_5 - FDOCk(BySPprime_5, FluxSBkBy_5, SpeedSBkBy_5, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBBz_5 = fluxAccSBBz_5 - FDOCi(BzSPprime_5, FluxSBiBz_5, SpeedSBiBz_5, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBBz_5 = fluxAccSBBz_5 - FDOCj(BzSPprime_5, FluxSBjBz_5, SpeedSBjBz_5, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(K2SBEx_5, i, j, k) = RK3(2, Ex_p, fluxAccSBEx_5, 0.0, 0.0, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(K2SBEy_5, i, j, k) = RK3(2, Ey_p, fluxAccSBEy_5, 0.0, 0.0, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(K2SBEz_5, i, j, k) = RK3(2, Ez_p, fluxAccSBEz_5, 0.0, 0.0, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(K2SBBx_5, i, j, k) = RK3(2, Bx_p, fluxAccSBBx_5, 0.0, 0.0, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(K2SBBy_5, i, j, k) = RK3(2, By_p, fluxAccSBBy_5, 0.0, 0.0, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(K2SBBz_5, i, j, k) = RK3(2, Bz_p, fluxAccSBBz_5, 0.0, 0.0, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(ExSPprimeprime_5, i, j, k) = vector(Ex_p, i, j, k) + 2.0 / 3.0 * vector(K2SBEx_5, i, j, k);
						vector(EySPprimeprime_5, i, j, k) = vector(Ey_p, i, j, k) + 2.0 / 3.0 * vector(K2SBEy_5, i, j, k);
						vector(EzSPprimeprime_5, i, j, k) = vector(Ez_p, i, j, k) + 2.0 / 3.0 * vector(K2SBEz_5, i, j, k);
						vector(BxSPprimeprime_5, i, j, k) = vector(Bx_p, i, j, k) + 2.0 / 3.0 * vector(K2SBBx_5, i, j, k);
						vector(BySPprimeprime_5, i, j, k) = vector(By_p, i, j, k) + 2.0 / 3.0 * vector(K2SBBy_5, i, j, k);
						vector(BzSPprimeprime_5, i, j, k) = vector(Bz_p, i, j, k) + 2.0 / 3.0 * vector(K2SBBz_5, i, j, k);
					}
					if ((vector(FOV_4, i, j, k) > 0) && (i + 3 < ilast && i - 3 >= 0 && j + 3 < jlast && j - 3 >= 0 && k + 3 < klast && k - 3 >= 0)) {
						fluxAccSBEx_4 = 0.0;
						fluxAccSBEy_4 = 0.0;
						fluxAccSBEz_4 = 0.0;
						fluxAccSBBx_4 = 0.0;
						fluxAccSBBy_4 = 0.0;
						fluxAccSBBz_4 = 0.0;
						fluxAccSBEx_4 = fluxAccSBEx_4 - FDOCj(ExSPprime_4, FluxSBjEx_4, SpeedSBjEx_4, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBEx_4 = fluxAccSBEx_4 - FDOCk(ExSPprime_4, FluxSBkEx_4, SpeedSBkEx_4, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBEy_4 = fluxAccSBEy_4 - FDOCi(EySPprime_4, FluxSBiEy_4, SpeedSBiEy_4, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBEy_4 = fluxAccSBEy_4 - FDOCk(EySPprime_4, FluxSBkEy_4, SpeedSBkEy_4, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBEz_4 = fluxAccSBEz_4 - FDOCi(EzSPprime_4, FluxSBiEz_4, SpeedSBiEz_4, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBEz_4 = fluxAccSBEz_4 - FDOCj(EzSPprime_4, FluxSBjEz_4, SpeedSBjEz_4, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBBx_4 = fluxAccSBBx_4 - FDOCj(BxSPprime_4, FluxSBjBx_4, SpeedSBjBx_4, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBBx_4 = fluxAccSBBx_4 - FDOCk(BxSPprime_4, FluxSBkBx_4, SpeedSBkBx_4, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBBy_4 = fluxAccSBBy_4 - FDOCi(BySPprime_4, FluxSBiBy_4, SpeedSBiBy_4, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBBy_4 = fluxAccSBBy_4 - FDOCk(BySPprime_4, FluxSBkBy_4, SpeedSBkBy_4, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBBz_4 = fluxAccSBBz_4 - FDOCi(BzSPprime_4, FluxSBiBz_4, SpeedSBiBz_4, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBBz_4 = fluxAccSBBz_4 - FDOCj(BzSPprime_4, FluxSBjBz_4, SpeedSBjBz_4, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(K2SBEx_4, i, j, k) = RK3(2, Ex_p, fluxAccSBEx_4, 0.0, 0.0, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(K2SBEy_4, i, j, k) = RK3(2, Ey_p, fluxAccSBEy_4, 0.0, 0.0, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(K2SBEz_4, i, j, k) = RK3(2, Ez_p, fluxAccSBEz_4, 0.0, 0.0, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(K2SBBx_4, i, j, k) = RK3(2, Bx_p, fluxAccSBBx_4, 0.0, 0.0, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(K2SBBy_4, i, j, k) = RK3(2, By_p, fluxAccSBBy_4, 0.0, 0.0, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(K2SBBz_4, i, j, k) = RK3(2, Bz_p, fluxAccSBBz_4, 0.0, 0.0, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(ExSPprimeprime_4, i, j, k) = vector(Ex_p, i, j, k) + 2.0 / 3.0 * vector(K2SBEx_4, i, j, k);
						vector(EySPprimeprime_4, i, j, k) = vector(Ey_p, i, j, k) + 2.0 / 3.0 * vector(K2SBEy_4, i, j, k);
						vector(EzSPprimeprime_4, i, j, k) = vector(Ez_p, i, j, k) + 2.0 / 3.0 * vector(K2SBEz_4, i, j, k);
						vector(BxSPprimeprime_4, i, j, k) = vector(Bx_p, i, j, k) + 2.0 / 3.0 * vector(K2SBBx_4, i, j, k);
						vector(BySPprimeprime_4, i, j, k) = vector(By_p, i, j, k) + 2.0 / 3.0 * vector(K2SBBy_4, i, j, k);
						vector(BzSPprimeprime_4, i, j, k) = vector(Bz_p, i, j, k) + 2.0 / 3.0 * vector(K2SBBz_4, i, j, k);
					}
				}
			}
		}
	}
	if (!d_tappering) {
		//Fill ghosts and periodical boundaries
		d_bdry_sched_advance4[ln]->fillData(current_time, false);
	}
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch >& patch = *p_it;
		double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_2 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_2_id).get())->getPointer();
		double* FOV_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_4_id).get())->getPointer();
		double* FOV_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_5_id).get())->getPointer();
		double* FOV_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_7_id).get())->getPointer();
		double* FOV_8 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_8_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
		double* FOV_zLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zLower_id).get())->getPointer();
		double* FOV_zUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zUpper_id).get())->getPointer();
	
		//Hard region field distance variables
		double* d_i_Ey = ((pdat::NodeData<double> *) patch->getPatchData(d_d_i_Ey_id).get())->getPointer();
		double* d_j_Ey = ((pdat::NodeData<double> *) patch->getPatchData(d_d_j_Ey_id).get())->getPointer();
		double* d_k_Ey = ((pdat::NodeData<double> *) patch->getPatchData(d_d_k_Ey_id).get())->getPointer();
		double* d_i_Ex = ((pdat::NodeData<double> *) patch->getPatchData(d_d_i_Ex_id).get())->getPointer();
		double* d_j_Ex = ((pdat::NodeData<double> *) patch->getPatchData(d_d_j_Ex_id).get())->getPointer();
		double* d_k_Ex = ((pdat::NodeData<double> *) patch->getPatchData(d_d_k_Ex_id).get())->getPointer();
	
		double* ExSPprimeprime_2 = ((pdat::NodeData<double> *) patch->getPatchData(d_ExSPprimeprime_2_id).get())->getPointer();
		double* EySPprimeprime_2 = ((pdat::NodeData<double> *) patch->getPatchData(d_EySPprimeprime_2_id).get())->getPointer();
		double* EzSPprimeprime_2 = ((pdat::NodeData<double> *) patch->getPatchData(d_EzSPprimeprime_2_id).get())->getPointer();
		double* BxSPprimeprime_2 = ((pdat::NodeData<double> *) patch->getPatchData(d_BxSPprimeprime_2_id).get())->getPointer();
		double* BySPprimeprime_2 = ((pdat::NodeData<double> *) patch->getPatchData(d_BySPprimeprime_2_id).get())->getPointer();
		double* BzSPprimeprime_2 = ((pdat::NodeData<double> *) patch->getPatchData(d_BzSPprimeprime_2_id).get())->getPointer();
	
		//Get the dimensions of the patch
		hier::Box pbox = patch->getBox();
		const hier::Index boxfirst = patch->getBox().lower();
		const hier::Index boxlast = patch->getBox().upper();
	
		//Get delta spaces into an array. dx, dy, dz.
		std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
		const double* dx  = patch_geom->getDx();
	
		//Auxiliary definitions
		int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
		int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
		int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;
		for(int k = 0; k < klast; k++) {
			for(int j = 0; j < jlast; j++) {
				for(int i = 0; i < ilast; i++) {
					if ((vector(FOV_xLower, i, j, k) > 0) && ((i + 1 < ilast && (vector(FOV_1,i + 1, j, k) > 0 || vector(FOV_2, i + 1, j, k) > 0)) || (i + 2 < ilast && (vector(FOV_1,i + 2, j, k) > 0 || vector(FOV_2, i + 2, j, k) > 0)) || (i + 3 < ilast && (vector(FOV_1,i + 3, j, k) > 0 || vector(FOV_2, i + 3, j, k) > 0)))) {
						vector(ExSPprimeprime_2, (d_ghost_width - 1) - i, j, k) = vector(ExSPprimeprime_2, (d_ghost_width - 1) - i + 1, j, k);
						vector(EySPprimeprime_2, (d_ghost_width - 1) - i, j, k) = vector(EySPprimeprime_2, (d_ghost_width - 1) - i + 1, j, k);
						vector(EzSPprimeprime_2, (d_ghost_width - 1) - i, j, k) = vector(EzSPprimeprime_2, (d_ghost_width - 1) - i + 1, j, k);
						vector(BxSPprimeprime_2, (d_ghost_width - 1) - i, j, k) = vector(BxSPprimeprime_2, (d_ghost_width - 1) - i + 1, j, k);
						vector(BySPprimeprime_2, (d_ghost_width - 1) - i, j, k) = vector(BySPprimeprime_2, (d_ghost_width - 1) - i + 1, j, k);
						vector(BzSPprimeprime_2, (d_ghost_width - 1) - i, j, k) = vector(BzSPprimeprime_2, (d_ghost_width - 1) - i + 1, j, k);
					}
					if ((vector(FOV_xUpper, i, j, k) > 0) && ((i - 1 >= 0 && (vector(FOV_1, i - 1, j, k) > 0 || vector(FOV_2, i - 1, j, k) > 0)) || (i - 2 >= 0 && (vector(FOV_1, i - 2, j, k) > 0 || vector(FOV_2, i - 2, j, k) > 0)) || (i - 3 >= 0 && (vector(FOV_1, i - 3, j, k) > 0 || vector(FOV_2, i - 3, j, k) > 0)))) {
						vector(ExSPprimeprime_2, i, j, k) = vector(ExSPprimeprime_2, i - 1, j, k);
						vector(EySPprimeprime_2, i, j, k) = vector(EySPprimeprime_2, i - 1, j, k);
						vector(EzSPprimeprime_2, i, j, k) = vector(EzSPprimeprime_2, i - 1, j, k);
						vector(BxSPprimeprime_2, i, j, k) = vector(BxSPprimeprime_2, i - 1, j, k);
						vector(BySPprimeprime_2, i, j, k) = vector(BySPprimeprime_2, i - 1, j, k);
						vector(BzSPprimeprime_2, i, j, k) = vector(BzSPprimeprime_2, i - 1, j, k);
					}
					if ((vector(FOV_yLower, i, j, k) > 0) && ((j + 1 < jlast && (vector(FOV_1,i, j + 1, k) > 0 || vector(FOV_2, i, j + 1, k) > 0)) || (j + 2 < jlast && (vector(FOV_1,i, j + 2, k) > 0 || vector(FOV_2, i, j + 2, k) > 0)) || (j + 3 < jlast && (vector(FOV_1,i, j + 3, k) > 0 || vector(FOV_2, i, j + 3, k) > 0)))) {
						vector(ExSPprimeprime_2, i, (d_ghost_width - 1) - j, k) = vector(ExSPprimeprime_2, i, (d_ghost_width - 1) - j + 1, k);
						vector(EySPprimeprime_2, i, (d_ghost_width - 1) - j, k) = vector(EySPprimeprime_2, i, (d_ghost_width - 1) - j + 1, k);
						vector(EzSPprimeprime_2, i, (d_ghost_width - 1) - j, k) = vector(EzSPprimeprime_2, i, (d_ghost_width - 1) - j + 1, k);
						vector(BxSPprimeprime_2, i, (d_ghost_width - 1) - j, k) = vector(BxSPprimeprime_2, i, (d_ghost_width - 1) - j + 1, k);
						vector(BySPprimeprime_2, i, (d_ghost_width - 1) - j, k) = vector(BySPprimeprime_2, i, (d_ghost_width - 1) - j + 1, k);
						vector(BzSPprimeprime_2, i, (d_ghost_width - 1) - j, k) = vector(BzSPprimeprime_2, i, (d_ghost_width - 1) - j + 1, k);
					}
					if ((vector(FOV_yUpper, i, j, k) > 0) && ((j - 1 >= 0 && (vector(FOV_1, i, j - 1, k) > 0 || vector(FOV_2, i, j - 1, k) > 0)) || (j - 2 >= 0 && (vector(FOV_1, i, j - 2, k) > 0 || vector(FOV_2, i, j - 2, k) > 0)) || (j - 3 >= 0 && (vector(FOV_1, i, j - 3, k) > 0 || vector(FOV_2, i, j - 3, k) > 0)))) {
						vector(ExSPprimeprime_2, i, j, k) = vector(ExSPprimeprime_2, i, j - 1, k);
						vector(EySPprimeprime_2, i, j, k) = vector(EySPprimeprime_2, i, j - 1, k);
						vector(EzSPprimeprime_2, i, j, k) = vector(EzSPprimeprime_2, i, j - 1, k);
						vector(BxSPprimeprime_2, i, j, k) = vector(BxSPprimeprime_2, i, j - 1, k);
						vector(BySPprimeprime_2, i, j, k) = vector(BySPprimeprime_2, i, j - 1, k);
						vector(BzSPprimeprime_2, i, j, k) = vector(BzSPprimeprime_2, i, j - 1, k);
					}
					if ((vector(FOV_zLower, i, j, k) > 0) && ((k + 1 < klast && (vector(FOV_1,i, j, k + 1) > 0 || vector(FOV_2, i, j, k + 1) > 0)) || (k + 2 < klast && (vector(FOV_1,i, j, k + 2) > 0 || vector(FOV_2, i, j, k + 2) > 0)) || (k + 3 < klast && (vector(FOV_1,i, j, k + 3) > 0 || vector(FOV_2, i, j, k + 3) > 0)))) {
						vector(ExSPprimeprime_2, i, j, (d_ghost_width - 1) - k) = vector(ExSPprimeprime_2, i, j, (d_ghost_width - 1) - k + 1);
						vector(EySPprimeprime_2, i, j, (d_ghost_width - 1) - k) = vector(EySPprimeprime_2, i, j, (d_ghost_width - 1) - k + 1);
						vector(EzSPprimeprime_2, i, j, (d_ghost_width - 1) - k) = vector(EzSPprimeprime_2, i, j, (d_ghost_width - 1) - k + 1);
						vector(BxSPprimeprime_2, i, j, (d_ghost_width - 1) - k) = vector(BxSPprimeprime_2, i, j, (d_ghost_width - 1) - k + 1);
						vector(BySPprimeprime_2, i, j, (d_ghost_width - 1) - k) = vector(BySPprimeprime_2, i, j, (d_ghost_width - 1) - k + 1);
						vector(BzSPprimeprime_2, i, j, (d_ghost_width - 1) - k) = vector(BzSPprimeprime_2, i, j, (d_ghost_width - 1) - k + 1);
					}
					if ((vector(FOV_zUpper, i, j, k) > 0) && ((k - 1 >= 0 && (vector(FOV_1, i, j, k - 1) > 0 || vector(FOV_2, i, j, k - 1) > 0)) || (k - 2 >= 0 && (vector(FOV_1, i, j, k - 2) > 0 || vector(FOV_2, i, j, k - 2) > 0)) || (k - 3 >= 0 && (vector(FOV_1, i, j, k - 3) > 0 || vector(FOV_2, i, j, k - 3) > 0)))) {
						vector(ExSPprimeprime_2, i, j, k) = vector(ExSPprimeprime_2, i, j, k - 1);
						vector(EySPprimeprime_2, i, j, k) = vector(EySPprimeprime_2, i, j, k - 1);
						vector(EzSPprimeprime_2, i, j, k) = vector(EzSPprimeprime_2, i, j, k - 1);
						vector(BxSPprimeprime_2, i, j, k) = vector(BxSPprimeprime_2, i, j, k - 1);
						vector(BySPprimeprime_2, i, j, k) = vector(BySPprimeprime_2, i, j, k - 1);
						vector(BzSPprimeprime_2, i, j, k) = vector(BzSPprimeprime_2, i, j, k - 1);
					}
				}
			}
		}
	}
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch >& patch = *p_it;
		double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_2 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_2_id).get())->getPointer();
		double* FOV_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_4_id).get())->getPointer();
		double* FOV_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_5_id).get())->getPointer();
		double* FOV_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_7_id).get())->getPointer();
		double* FOV_8 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_8_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
		double* FOV_zLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zLower_id).get())->getPointer();
		double* FOV_zUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zUpper_id).get())->getPointer();
	
		double* EzSPprimeprime_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_EzSPprimeprime_7_id).get())->getPointer();
		double* FluxSBjEz_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjEz_7_id).get())->getPointer();
		double* SpeedSBjEz_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjEz_7_id).get())->getPointer();
		double* BxSPprimeprime_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_BxSPprimeprime_7_id).get())->getPointer();
		double* FluxSBjBx_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjBx_7_id).get())->getPointer();
		double* SpeedSBjBx_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjBx_7_id).get())->getPointer();
		double* FluxSBkBx_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkBx_7_id).get())->getPointer();
		double* SpeedSBkBx_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkBx_7_id).get())->getPointer();
		double* BySPprimeprime_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_BySPprimeprime_7_id).get())->getPointer();
		double* FluxSBiBy_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiBy_7_id).get())->getPointer();
		double* SpeedSBiBy_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiBy_7_id).get())->getPointer();
		double* FluxSBkBy_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkBy_7_id).get())->getPointer();
		double* SpeedSBkBy_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkBy_7_id).get())->getPointer();
		double* BzSPprimeprime_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_BzSPprimeprime_7_id).get())->getPointer();
		double* FluxSBiBz_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiBz_7_id).get())->getPointer();
		double* SpeedSBiBz_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiBz_7_id).get())->getPointer();
		double* FluxSBjBz_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjBz_7_id).get())->getPointer();
		double* SpeedSBjBz_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjBz_7_id).get())->getPointer();
		double* K3SBEx_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_K3SBEx_7_id).get())->getPointer();
		double* Ex_p = ((pdat::NodeData<double> *) patch->getPatchData(d_Ex_p_id).get())->getPointer();
		double* K3SBEy_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_K3SBEy_7_id).get())->getPointer();
		double* Ey_p = ((pdat::NodeData<double> *) patch->getPatchData(d_Ey_p_id).get())->getPointer();
		double* K3SBBy_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_K3SBBy_7_id).get())->getPointer();
		double* By_p = ((pdat::NodeData<double> *) patch->getPatchData(d_By_p_id).get())->getPointer();
		double* By = ((pdat::NodeData<double> *) patch->getPatchData(d_By_id).get())->getPointer();
		double* K1SBBy_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_K1SBBy_7_id).get())->getPointer();
		double* Bz = ((pdat::NodeData<double> *) patch->getPatchData(d_Bz_id).get())->getPointer();
		double* Bz_p = ((pdat::NodeData<double> *) patch->getPatchData(d_Bz_p_id).get())->getPointer();
		double* K1SBBz_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_K1SBBz_7_id).get())->getPointer();
		double* FluxSBjEx_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjEx_4_id).get())->getPointer();
		double* FluxSBkEx_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkEx_4_id).get())->getPointer();
		double* FluxSBiEy_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiEy_4_id).get())->getPointer();
		double* FluxSBkEy_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkEy_4_id).get())->getPointer();
		double* FluxSBiEz_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiEz_4_id).get())->getPointer();
		double* FluxSBjEz_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjEz_4_id).get())->getPointer();
		double* FluxSBjBx_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjBx_4_id).get())->getPointer();
		double* FluxSBkBx_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkBx_4_id).get())->getPointer();
		double* FluxSBiBy_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiBy_4_id).get())->getPointer();
		double* FluxSBkBy_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkBy_4_id).get())->getPointer();
		double* FluxSBiBz_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiBz_4_id).get())->getPointer();
		double* FluxSBjBz_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjBz_4_id).get())->getPointer();
		double* SpeedSBjEx_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjEx_4_id).get())->getPointer();
		double* SpeedSBkEx_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkEx_4_id).get())->getPointer();
		double* SpeedSBiEy_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiEy_4_id).get())->getPointer();
		double* SpeedSBkEy_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkEy_4_id).get())->getPointer();
		double* SpeedSBiEz_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiEz_4_id).get())->getPointer();
		double* SpeedSBjEz_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjEz_4_id).get())->getPointer();
		double* SpeedSBjBx_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjBx_4_id).get())->getPointer();
		double* SpeedSBkBx_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkBx_4_id).get())->getPointer();
		double* SpeedSBiBy_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiBy_4_id).get())->getPointer();
		double* SpeedSBkBy_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkBy_4_id).get())->getPointer();
		double* SpeedSBiBz_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiBz_4_id).get())->getPointer();
		double* SpeedSBjBz_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjBz_4_id).get())->getPointer();
		double* FluxSBjEx_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjEx_5_id).get())->getPointer();
		double* FluxSBkEx_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkEx_5_id).get())->getPointer();
		double* FluxSBiEy_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiEy_5_id).get())->getPointer();
		double* FluxSBkEy_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkEy_5_id).get())->getPointer();
		double* FluxSBiEz_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiEz_5_id).get())->getPointer();
		double* FluxSBjEz_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjEz_5_id).get())->getPointer();
		double* FluxSBjBx_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjBx_5_id).get())->getPointer();
		double* FluxSBkBx_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkBx_5_id).get())->getPointer();
		double* FluxSBiBy_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiBy_5_id).get())->getPointer();
		double* FluxSBkBy_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkBy_5_id).get())->getPointer();
		double* FluxSBiBz_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiBz_5_id).get())->getPointer();
		double* FluxSBjBz_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjBz_5_id).get())->getPointer();
		double* SpeedSBjEx_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjEx_5_id).get())->getPointer();
		double* SpeedSBkEx_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkEx_5_id).get())->getPointer();
		double* SpeedSBiEy_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiEy_5_id).get())->getPointer();
		double* SpeedSBkEy_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkEy_5_id).get())->getPointer();
		double* SpeedSBiEz_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiEz_5_id).get())->getPointer();
		double* SpeedSBjEz_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjEz_5_id).get())->getPointer();
		double* SpeedSBjBx_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjBx_5_id).get())->getPointer();
		double* SpeedSBkBx_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkBx_5_id).get())->getPointer();
		double* SpeedSBiBy_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiBy_5_id).get())->getPointer();
		double* SpeedSBkBy_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkBy_5_id).get())->getPointer();
		double* SpeedSBiBz_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiBz_5_id).get())->getPointer();
		double* SpeedSBjBz_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjBz_5_id).get())->getPointer();
		double fluxAccSBEx_7, fluxAccSBEy_7, fluxAccSBEz_7, fluxAccSBBx_7, fluxAccSBBy_7, fluxAccSBBz_7;
	
		//Get the dimensions of the patch
		hier::Box pbox = patch->getBox();
		const hier::Index boxfirst = patch->getBox().lower();
		const hier::Index boxlast = patch->getBox().upper();
	
		//Get delta spaces into an array. dx, dy, dz.
		std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
		const double* dx  = patch_geom->getDx();
	
		//Auxiliary definitions
		int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
		int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
		int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;
		for(int k = 0; k < klast; k++) {
			for(int j = 0; j < jlast; j++) {
				for(int i = 0; i < ilast; i++) {
					if ((vector(FOV_7, i, j, k) > 0) && (i + 2 < ilast && i - 2 >= 0 && j + 2 < jlast && j - 2 >= 0 && k + 2 < klast && k - 2 >= 0)) {
						fluxAccSBEx_7 = -mu * JxSPprimeprime(i, j, k);
						fluxAccSBEy_7 = -mu * JySPprimeprime(i, j, k);
						fluxAccSBEz_7 = -mu * JzSPprimeprime(i, j, k);
						fluxAccSBEz_7 = fluxAccSBEz_7 - FDOCj(EzSPprimeprime_7, FluxSBjEz_7, SpeedSBjEz_7, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBBx_7 = fluxAccSBBx_7 - FDOCj(BxSPprimeprime_7, FluxSBjBx_7, SpeedSBjBx_7, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBBx_7 = fluxAccSBBx_7 - FDOCk(BxSPprimeprime_7, FluxSBkBx_7, SpeedSBkBx_7, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBBy_7 = fluxAccSBBy_7 - FDOCi(BySPprimeprime_7, FluxSBiBy_7, SpeedSBiBy_7, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBBy_7 = fluxAccSBBy_7 - FDOCk(BySPprimeprime_7, FluxSBkBy_7, SpeedSBkBy_7, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBBz_7 = fluxAccSBBz_7 - FDOCi(BzSPprimeprime_7, FluxSBiBz_7, SpeedSBiBz_7, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBBz_7 = fluxAccSBBz_7 - FDOCj(BzSPprimeprime_7, FluxSBjBz_7, SpeedSBjBz_7, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(K3SBEx_7, i, j, k) = RK3(3, Ex_p, fluxAccSBEx_7, 0.0, 0.0, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(K3SBEy_7, i, j, k) = RK3(3, Ey_p, fluxAccSBEy_7, 0.0, 0.0, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(K3SBBy_7, i, j, k) = RK3(3, By_p, fluxAccSBBy_7, 0.0, 0.0, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(By, i, j, k) = RK3(4, By_p, 0.0, K1SBBy_7, K3SBBy_7, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(Bz, i, j, k) = RK3(4, Bz_p, 0.0, K1SBBz_7, K3SBBz, i, j, k, dx, simPlat_dt, ilast, jlast);
					}
					if ((vector(FOV_5, i, j, k) > 0 || (((i + 1 < ilast && vector(FOV_5, i + 1, j, k) > 0) || (i + 2 < ilast && vector(FOV_5, i + 2, j, k) > 0) || (j + 1 < jlast && vector(FOV_5, i, j + 1, k) > 0) || (j + 2 < jlast && vector(FOV_5, i, j + 2, k) > 0) || (k + 1 < klast && vector(FOV_5, i, j, k + 1) > 0) || (k + 2 < klast && vector(FOV_5, i, j, k + 2) > 0)) || ((i - 1 >= 0 && vector(FOV_5, i - 1, j, k) > 0) || (i - 2 >= 0 && vector(FOV_5, i - 2, j, k) > 0) || (j - 1 >= 0 && vector(FOV_5, i, j - 1, k) > 0) || (j - 2 >= 0 && vector(FOV_5, i, j - 2, k) > 0) || (k - 1 >= 0 && vector(FOV_5, i, j, k - 1) > 0) || (k - 2 >= 0 && vector(FOV_5, i, j, k - 2) > 0))) || (d_tappering && (neighbourTappering(FOV_5, i, j, k, ilast, jlast, klast, 2, 6 * d_sim_substep[ln] - 4))))) {
						vector(FluxSBjEx_5, i, j, k) = FjEx_OtherI(dx, simPlat_dt, ilast, jlast);
						vector(FluxSBkEx_5, i, j, k) = FkEx_OtherI(dx, simPlat_dt, ilast, jlast);
						vector(FluxSBiEy_5, i, j, k) = FiEy_OtherI(dx, simPlat_dt, ilast, jlast);
						vector(FluxSBkEy_5, i, j, k) = FkEy_OtherI(dx, simPlat_dt, ilast, jlast);
						vector(FluxSBiEz_5, i, j, k) = FiEz_OtherI(dx, simPlat_dt, ilast, jlast);
						vector(FluxSBjEz_5, i, j, k) = FjEz_OtherI(dx, simPlat_dt, ilast, jlast);
						vector(FluxSBjBx_5, i, j, k) = FjBx_OtherI(dx, simPlat_dt, ilast, jlast);
						vector(FluxSBkBx_5, i, j, k) = FkBx_OtherI(dx, simPlat_dt, ilast, jlast);
						vector(FluxSBiBy_5, i, j, k) = FiBy_OtherI(dx, simPlat_dt, ilast, jlast);
						vector(FluxSBkBy_5, i, j, k) = FkBy_OtherI(dx, simPlat_dt, ilast, jlast);
						vector(FluxSBiBz_5, i, j, k) = FiBz_OtherI(dx, simPlat_dt, ilast, jlast);
						vector(FluxSBjBz_5, i, j, k) = FjBz_OtherI(dx, simPlat_dt, ilast, jlast);
						vector(SpeedSBjEx_5, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBkEx_5, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBiEy_5, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBkEy_5, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBiEz_5, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBjEz_5, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBjBx_5, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBkBx_5, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBiBy_5, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBkBy_5, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBiBz_5, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBjBz_5, i, j, k) = MAX(0.0, MAX(c, -c));
					}
					if ((vector(FOV_4, i, j, k) > 0 || (((i + 1 < ilast && vector(FOV_4, i + 1, j, k) > 0) || (j + 1 < jlast && vector(FOV_4, i, j + 1, k) > 0) || (k + 1 < klast && vector(FOV_4, i, j, k + 1) > 0)) || ((i - 1 >= 0 && vector(FOV_4, i - 1, j, k) > 0) || (j - 1 >= 0 && vector(FOV_4, i, j - 1, k) > 0) || (k - 1 >= 0 && vector(FOV_4, i, j, k - 1) > 0))) || (d_tappering && (neighbourTappering(FOV_4, i, j, k, ilast, jlast, klast, 1, 5 * d_sim_substep[ln] - 4))))) {
						vector(FluxSBjEx_4, i, j, k) = FjEx_SoilS(dx, simPlat_dt, ilast, jlast);
						vector(FluxSBkEx_4, i, j, k) = FkEx_SoilS(dx, simPlat_dt, ilast, jlast);
						vector(FluxSBiEy_4, i, j, k) = FiEy_SoilS(dx, simPlat_dt, ilast, jlast);
						vector(FluxSBkEy_4, i, j, k) = FkEy_SoilS(dx, simPlat_dt, ilast, jlast);
						vector(FluxSBiEz_4, i, j, k) = FiEz_SoilS(dx, simPlat_dt, ilast, jlast);
						vector(FluxSBjEz_4, i, j, k) = FjEz_SoilS(dx, simPlat_dt, ilast, jlast);
						vector(FluxSBjBx_4, i, j, k) = FjBx_SoilS(dx, simPlat_dt, ilast, jlast);
						vector(FluxSBkBx_4, i, j, k) = FkBx_SoilS(dx, simPlat_dt, ilast, jlast);
						vector(FluxSBiBy_4, i, j, k) = FiBy_SoilS(dx, simPlat_dt, ilast, jlast);
						vector(FluxSBkBy_4, i, j, k) = FkBy_SoilS(dx, simPlat_dt, ilast, jlast);
						vector(FluxSBiBz_4, i, j, k) = FiBz_SoilS(dx, simPlat_dt, ilast, jlast);
						vector(FluxSBjBz_4, i, j, k) = FjBz_SoilS(dx, simPlat_dt, ilast, jlast);
						vector(SpeedSBjEx_4, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBkEx_4, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBiEy_4, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBkEy_4, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBiEz_4, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBjEz_4, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBjBx_4, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBkBx_4, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBiBy_4, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBkBy_4, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBiBz_4, i, j, k) = MAX(0.0, MAX(c, -c));
						vector(SpeedSBjBz_4, i, j, k) = MAX(0.0, MAX(c, -c));
					}
				}
			}
		}
	}
	if (!d_tappering) {
		//Fill ghosts and periodical boundaries
		d_bdry_sched_advance6[ln]->fillData(current_time, false);
	}
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch >& patch = *p_it;
		double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_2 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_2_id).get())->getPointer();
		double* FOV_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_4_id).get())->getPointer();
		double* FOV_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_5_id).get())->getPointer();
		double* FOV_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_7_id).get())->getPointer();
		double* FOV_8 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_8_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
		double* FOV_zLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zLower_id).get())->getPointer();
		double* FOV_zUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zUpper_id).get())->getPointer();
	
		double* ExSPprimeprime_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_ExSPprimeprime_4_id).get())->getPointer();
		double* FluxSBjEx_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjEx_4_id).get())->getPointer();
		double* SpeedSBjEx_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjEx_4_id).get())->getPointer();
		double* FluxSBkEx_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkEx_4_id).get())->getPointer();
		double* SpeedSBkEx_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkEx_4_id).get())->getPointer();
		double* EySPprimeprime_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_EySPprimeprime_4_id).get())->getPointer();
		double* FluxSBiEy_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiEy_4_id).get())->getPointer();
		double* SpeedSBiEy_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiEy_4_id).get())->getPointer();
		double* FluxSBkEy_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkEy_4_id).get())->getPointer();
		double* SpeedSBkEy_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkEy_4_id).get())->getPointer();
		double* EzSPprimeprime_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_EzSPprimeprime_4_id).get())->getPointer();
		double* FluxSBiEz_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiEz_4_id).get())->getPointer();
		double* SpeedSBiEz_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiEz_4_id).get())->getPointer();
		double* FluxSBjEz_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjEz_4_id).get())->getPointer();
		double* SpeedSBjEz_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjEz_4_id).get())->getPointer();
		double* BxSPprimeprime_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_BxSPprimeprime_4_id).get())->getPointer();
		double* FluxSBjBx_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjBx_4_id).get())->getPointer();
		double* SpeedSBjBx_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjBx_4_id).get())->getPointer();
		double* FluxSBkBx_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkBx_4_id).get())->getPointer();
		double* SpeedSBkBx_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkBx_4_id).get())->getPointer();
		double* BySPprimeprime_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_BySPprimeprime_4_id).get())->getPointer();
		double* FluxSBiBy_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiBy_4_id).get())->getPointer();
		double* SpeedSBiBy_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiBy_4_id).get())->getPointer();
		double* FluxSBkBy_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkBy_4_id).get())->getPointer();
		double* SpeedSBkBy_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkBy_4_id).get())->getPointer();
		double* BzSPprimeprime_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_BzSPprimeprime_4_id).get())->getPointer();
		double* FluxSBiBz_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiBz_4_id).get())->getPointer();
		double* SpeedSBiBz_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiBz_4_id).get())->getPointer();
		double* FluxSBjBz_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjBz_4_id).get())->getPointer();
		double* SpeedSBjBz_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjBz_4_id).get())->getPointer();
		double* K3SBEx_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_K3SBEx_4_id).get())->getPointer();
		double* Ex_p = ((pdat::NodeData<double> *) patch->getPatchData(d_Ex_p_id).get())->getPointer();
		double* K3SBEy_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_K3SBEy_4_id).get())->getPointer();
		double* Ey_p = ((pdat::NodeData<double> *) patch->getPatchData(d_Ey_p_id).get())->getPointer();
		double* K3SBEz_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_K3SBEz_4_id).get())->getPointer();
		double* Ez_p = ((pdat::NodeData<double> *) patch->getPatchData(d_Ez_p_id).get())->getPointer();
		double* K3SBBx_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_K3SBBx_4_id).get())->getPointer();
		double* Bx_p = ((pdat::NodeData<double> *) patch->getPatchData(d_Bx_p_id).get())->getPointer();
		double* K3SBBy_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_K3SBBy_4_id).get())->getPointer();
		double* By_p = ((pdat::NodeData<double> *) patch->getPatchData(d_By_p_id).get())->getPointer();
		double* K3SBBz_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_K3SBBz_4_id).get())->getPointer();
		double* Bz_p = ((pdat::NodeData<double> *) patch->getPatchData(d_Bz_p_id).get())->getPointer();
		double* Ex = ((pdat::NodeData<double> *) patch->getPatchData(d_Ex_id).get())->getPointer();
		double* K1SBEx_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_K1SBEx_4_id).get())->getPointer();
		double* Ey = ((pdat::NodeData<double> *) patch->getPatchData(d_Ey_id).get())->getPointer();
		double* K1SBEy_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_K1SBEy_4_id).get())->getPointer();
		double* Ez = ((pdat::NodeData<double> *) patch->getPatchData(d_Ez_id).get())->getPointer();
		double* K1SBEz_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_K1SBEz_4_id).get())->getPointer();
		double* Bx = ((pdat::NodeData<double> *) patch->getPatchData(d_Bx_id).get())->getPointer();
		double* K1SBBx_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_K1SBBx_4_id).get())->getPointer();
		double* By = ((pdat::NodeData<double> *) patch->getPatchData(d_By_id).get())->getPointer();
		double* K1SBBy_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_K1SBBy_4_id).get())->getPointer();
		double* Bz = ((pdat::NodeData<double> *) patch->getPatchData(d_Bz_id).get())->getPointer();
		double* K1SBBz_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_K1SBBz_4_id).get())->getPointer();
		double* ExSPprimeprime_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_ExSPprimeprime_5_id).get())->getPointer();
		double* FluxSBjEx_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjEx_5_id).get())->getPointer();
		double* SpeedSBjEx_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjEx_5_id).get())->getPointer();
		double* FluxSBkEx_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkEx_5_id).get())->getPointer();
		double* SpeedSBkEx_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkEx_5_id).get())->getPointer();
		double* EySPprimeprime_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_EySPprimeprime_5_id).get())->getPointer();
		double* FluxSBiEy_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiEy_5_id).get())->getPointer();
		double* SpeedSBiEy_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiEy_5_id).get())->getPointer();
		double* FluxSBkEy_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkEy_5_id).get())->getPointer();
		double* SpeedSBkEy_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkEy_5_id).get())->getPointer();
		double* EzSPprimeprime_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_EzSPprimeprime_5_id).get())->getPointer();
		double* FluxSBiEz_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiEz_5_id).get())->getPointer();
		double* SpeedSBiEz_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiEz_5_id).get())->getPointer();
		double* FluxSBjEz_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjEz_5_id).get())->getPointer();
		double* SpeedSBjEz_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjEz_5_id).get())->getPointer();
		double* BxSPprimeprime_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_BxSPprimeprime_5_id).get())->getPointer();
		double* FluxSBjBx_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjBx_5_id).get())->getPointer();
		double* SpeedSBjBx_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjBx_5_id).get())->getPointer();
		double* FluxSBkBx_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkBx_5_id).get())->getPointer();
		double* SpeedSBkBx_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkBx_5_id).get())->getPointer();
		double* BySPprimeprime_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_BySPprimeprime_5_id).get())->getPointer();
		double* FluxSBiBy_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiBy_5_id).get())->getPointer();
		double* SpeedSBiBy_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiBy_5_id).get())->getPointer();
		double* FluxSBkBy_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBkBy_5_id).get())->getPointer();
		double* SpeedSBkBy_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBkBy_5_id).get())->getPointer();
		double* BzSPprimeprime_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_BzSPprimeprime_5_id).get())->getPointer();
		double* FluxSBiBz_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBiBz_5_id).get())->getPointer();
		double* SpeedSBiBz_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBiBz_5_id).get())->getPointer();
		double* FluxSBjBz_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FluxSBjBz_5_id).get())->getPointer();
		double* SpeedSBjBz_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_SpeedSBjBz_5_id).get())->getPointer();
		double* K3SBEx_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_K3SBEx_5_id).get())->getPointer();
		double* K3SBEy_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_K3SBEy_5_id).get())->getPointer();
		double* K3SBEz_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_K3SBEz_5_id).get())->getPointer();
		double* K3SBBx_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_K3SBBx_5_id).get())->getPointer();
		double* K3SBBy_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_K3SBBy_5_id).get())->getPointer();
		double* K3SBBz_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_K3SBBz_5_id).get())->getPointer();
		double* K1SBEx_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_K1SBEx_5_id).get())->getPointer();
		double* K1SBEy_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_K1SBEy_5_id).get())->getPointer();
		double* K1SBEz_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_K1SBEz_5_id).get())->getPointer();
		double* K1SBBx_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_K1SBBx_5_id).get())->getPointer();
		double* K1SBBy_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_K1SBBy_5_id).get())->getPointer();
		double* K1SBBz_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_K1SBBz_5_id).get())->getPointer();
		double fluxAccSBEx_4, fluxAccSBEy_4, fluxAccSBEz_4, fluxAccSBBx_4, fluxAccSBBy_4, fluxAccSBBz_4, fluxAccSBEx_5, fluxAccSBEy_5, fluxAccSBEz_5, fluxAccSBBx_5, fluxAccSBBy_5, fluxAccSBBz_5;
	
		//Get the dimensions of the patch
		hier::Box pbox = patch->getBox();
		const hier::Index boxfirst = patch->getBox().lower();
		const hier::Index boxlast = patch->getBox().upper();
	
		//Get delta spaces into an array. dx, dy, dz.
		std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
		const double* dx  = patch_geom->getDx();
	
		//Auxiliary definitions
		int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
		int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
		int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;
		for(int k = 0; k < klast; k++) {
			for(int j = 0; j < jlast; j++) {
				for(int i = 0; i < ilast; i++) {
					if ((vector(FOV_5, i, j, k) > 0) && (i + 2 < ilast && i - 2 >= 0 && j + 2 < jlast && j - 2 >= 0 && k + 2 < klast && k - 2 >= 0)) {
						fluxAccSBEx_5 = 0.0;
						fluxAccSBEy_5 = 0.0;
						fluxAccSBEz_5 = 0.0;
						fluxAccSBBx_5 = 0.0;
						fluxAccSBBy_5 = 0.0;
						fluxAccSBBz_5 = 0.0;
						fluxAccSBEx_5 = fluxAccSBEx_5 - FDOCj(ExSPprimeprime_5, FluxSBjEx_5, SpeedSBjEx_5, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBEx_5 = fluxAccSBEx_5 - FDOCk(ExSPprimeprime_5, FluxSBkEx_5, SpeedSBkEx_5, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBEy_5 = fluxAccSBEy_5 - FDOCi(EySPprimeprime_5, FluxSBiEy_5, SpeedSBiEy_5, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBEy_5 = fluxAccSBEy_5 - FDOCk(EySPprimeprime_5, FluxSBkEy_5, SpeedSBkEy_5, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBEz_5 = fluxAccSBEz_5 - FDOCi(EzSPprimeprime_5, FluxSBiEz_5, SpeedSBiEz_5, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBEz_5 = fluxAccSBEz_5 - FDOCj(EzSPprimeprime_5, FluxSBjEz_5, SpeedSBjEz_5, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBBx_5 = fluxAccSBBx_5 - FDOCj(BxSPprimeprime_5, FluxSBjBx_5, SpeedSBjBx_5, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBBx_5 = fluxAccSBBx_5 - FDOCk(BxSPprimeprime_5, FluxSBkBx_5, SpeedSBkBx_5, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBBy_5 = fluxAccSBBy_5 - FDOCi(BySPprimeprime_5, FluxSBiBy_5, SpeedSBiBy_5, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBBy_5 = fluxAccSBBy_5 - FDOCk(BySPprimeprime_5, FluxSBkBy_5, SpeedSBkBy_5, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBBz_5 = fluxAccSBBz_5 - FDOCi(BzSPprimeprime_5, FluxSBiBz_5, SpeedSBiBz_5, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBBz_5 = fluxAccSBBz_5 - FDOCj(BzSPprimeprime_5, FluxSBjBz_5, SpeedSBjBz_5, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(K3SBEx_5, i, j, k) = RK3(3, Ex_p, fluxAccSBEx_5, 0.0, 0.0, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(K3SBEy_5, i, j, k) = RK3(3, Ey_p, fluxAccSBEy_5, 0.0, 0.0, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(K3SBEz_5, i, j, k) = RK3(3, Ez_p, fluxAccSBEz_5, 0.0, 0.0, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(K3SBBx_5, i, j, k) = RK3(3, Bx_p, fluxAccSBBx_5, 0.0, 0.0, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(K3SBBy_5, i, j, k) = RK3(3, By_p, fluxAccSBBy_5, 0.0, 0.0, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(K3SBBz_5, i, j, k) = RK3(3, Bz_p, fluxAccSBBz_5, 0.0, 0.0, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(Ex, i, j, k) = RK3(4, Ex_p, 0.0, K1SBEx_5, K3SBEx_5, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(Ey, i, j, k) = RK3(4, Ey_p, 0.0, K1SBEy_5, K3SBEy_5, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(Ez, i, j, k) = RK3(4, Ez_p, 0.0, K1SBEz_5, K3SBEz_5, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(Bx, i, j, k) = RK3(4, Bx_p, 0.0, K1SBBx_5, K3SBBx_5, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(By, i, j, k) = RK3(4, By_p, 0.0, K1SBBy_5, K3SBBy_5, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(Bz, i, j, k) = RK3(4, Bz_p, 0.0, K1SBBz_5, K3SBBz_5, i, j, k, dx, simPlat_dt, ilast, jlast);
					}
					if ((vector(FOV_4, i, j, k) > 0) && (i + 1 < ilast && i - 1 >= 0 && j + 1 < jlast && j - 1 >= 0 && k + 1 < klast && k - 1 >= 0)) {
						fluxAccSBEx_4 = 0.0;
						fluxAccSBEy_4 = 0.0;
						fluxAccSBEz_4 = 0.0;
						fluxAccSBBx_4 = 0.0;
						fluxAccSBBy_4 = 0.0;
						fluxAccSBBz_4 = 0.0;
						fluxAccSBEx_4 = fluxAccSBEx_4 - FDOCj(ExSPprimeprime_4, FluxSBjEx_4, SpeedSBjEx_4, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBEx_4 = fluxAccSBEx_4 - FDOCk(ExSPprimeprime_4, FluxSBkEx_4, SpeedSBkEx_4, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBEy_4 = fluxAccSBEy_4 - FDOCi(EySPprimeprime_4, FluxSBiEy_4, SpeedSBiEy_4, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBEy_4 = fluxAccSBEy_4 - FDOCk(EySPprimeprime_4, FluxSBkEy_4, SpeedSBkEy_4, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBEz_4 = fluxAccSBEz_4 - FDOCi(EzSPprimeprime_4, FluxSBiEz_4, SpeedSBiEz_4, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBEz_4 = fluxAccSBEz_4 - FDOCj(EzSPprimeprime_4, FluxSBjEz_4, SpeedSBjEz_4, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBBx_4 = fluxAccSBBx_4 - FDOCj(BxSPprimeprime_4, FluxSBjBx_4, SpeedSBjBx_4, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBBx_4 = fluxAccSBBx_4 - FDOCk(BxSPprimeprime_4, FluxSBkBx_4, SpeedSBkBx_4, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBBy_4 = fluxAccSBBy_4 - FDOCi(BySPprimeprime_4, FluxSBiBy_4, SpeedSBiBy_4, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBBy_4 = fluxAccSBBy_4 - FDOCk(BySPprimeprime_4, FluxSBkBy_4, SpeedSBkBy_4, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBBz_4 = fluxAccSBBz_4 - FDOCi(BzSPprimeprime_4, FluxSBiBz_4, SpeedSBiBz_4, i, j, k, dx, simPlat_dt, ilast, jlast);
						fluxAccSBBz_4 = fluxAccSBBz_4 - FDOCj(BzSPprimeprime_4, FluxSBjBz_4, SpeedSBjBz_4, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(K3SBEx_4, i, j, k) = RK3(3, Ex_p, fluxAccSBEx_4, 0.0, 0.0, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(K3SBEy_4, i, j, k) = RK3(3, Ey_p, fluxAccSBEy_4, 0.0, 0.0, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(K3SBEz_4, i, j, k) = RK3(3, Ez_p, fluxAccSBEz_4, 0.0, 0.0, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(K3SBBx_4, i, j, k) = RK3(3, Bx_p, fluxAccSBBx_4, 0.0, 0.0, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(K3SBBy_4, i, j, k) = RK3(3, By_p, fluxAccSBBy_4, 0.0, 0.0, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(K3SBBz_4, i, j, k) = RK3(3, Bz_p, fluxAccSBBz_4, 0.0, 0.0, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(Ex, i, j, k) = RK3(4, Ex_p, 0.0, K1SBEx_4, K3SBEx_4, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(Ey, i, j, k) = RK3(4, Ey_p, 0.0, K1SBEy_4, K3SBEy_4, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(Ez, i, j, k) = RK3(4, Ez_p, 0.0, K1SBEz_4, K3SBEz_4, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(Bx, i, j, k) = RK3(4, Bx_p, 0.0, K1SBBx_4, K3SBBx_4, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(By, i, j, k) = RK3(4, By_p, 0.0, K1SBBy_4, K3SBBy_4, i, j, k, dx, simPlat_dt, ilast, jlast);
						vector(Bz, i, j, k) = RK3(4, Bz_p, 0.0, K1SBBz_4, K3SBBz_4, i, j, k, dx, simPlat_dt, ilast, jlast);
					}
				}
			}
		}
	}
	if (ln > 0 && (last_step || !d_refinedTimeStepping)) {
		d_coarsen_schedule[ln]->coarsenData();
		d_bdry_sched_postCoarsen[ln - 1]->fillData(current_time, false);
	}
	if (!d_tappering || last_step ) {
		//Fill ghosts and periodical boundaries
		d_bdry_sched_advance7[ln]->fillData(current_time, false);
	}
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch >& patch = *p_it;
		double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_2 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_2_id).get())->getPointer();
		double* FOV_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_4_id).get())->getPointer();
		double* FOV_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_5_id).get())->getPointer();
		double* FOV_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_7_id).get())->getPointer();
		double* FOV_8 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_8_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
		double* FOV_zLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zLower_id).get())->getPointer();
		double* FOV_zUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zUpper_id).get())->getPointer();
	
		//Hard region field distance variables
		double* d_i_Ey = ((pdat::NodeData<double> *) patch->getPatchData(d_d_i_Ey_id).get())->getPointer();
		double* d_j_Ey = ((pdat::NodeData<double> *) patch->getPatchData(d_d_j_Ey_id).get())->getPointer();
		double* d_k_Ey = ((pdat::NodeData<double> *) patch->getPatchData(d_d_k_Ey_id).get())->getPointer();
		double* d_i_Ex = ((pdat::NodeData<double> *) patch->getPatchData(d_d_i_Ex_id).get())->getPointer();
		double* d_j_Ex = ((pdat::NodeData<double> *) patch->getPatchData(d_d_j_Ex_id).get())->getPointer();
		double* d_k_Ex = ((pdat::NodeData<double> *) patch->getPatchData(d_d_k_Ex_id).get())->getPointer();
	
		double* Ex = ((pdat::NodeData<double> *) patch->getPatchData(d_Ex_id).get())->getPointer();
		double* Ey = ((pdat::NodeData<double> *) patch->getPatchData(d_Ey_id).get())->getPointer();
		double* Ez = ((pdat::NodeData<double> *) patch->getPatchData(d_Ez_id).get())->getPointer();
		double* Bx = ((pdat::NodeData<double> *) patch->getPatchData(d_Bx_id).get())->getPointer();
		double* By = ((pdat::NodeData<double> *) patch->getPatchData(d_By_id).get())->getPointer();
		double* Bz = ((pdat::NodeData<double> *) patch->getPatchData(d_Bz_id).get())->getPointer();
	
		//Get the dimensions of the patch
		hier::Box pbox = patch->getBox();
		const hier::Index boxfirst = patch->getBox().lower();
		const hier::Index boxlast = patch->getBox().upper();
	
		//Get delta spaces into an array. dx, dy, dz.
		std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
		const double* dx  = patch_geom->getDx();
	
		//Auxiliary definitions
		int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
		int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
		int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;
		for(int k = 0; k < klast; k++) {
			for(int j = 0; j < jlast; j++) {
				for(int i = 0; i < ilast; i++) {
					if ((vector(FOV_xLower, i, j, k) > 0) && ((i + 1 < ilast && (vector(FOV_1,i + 1, j, k) > 0 || vector(FOV_2, i + 1, j, k) > 0)) || (i + 2 < ilast && (vector(FOV_1,i + 2, j, k) > 0 || vector(FOV_2, i + 2, j, k) > 0)) || (i + 3 < ilast && (vector(FOV_1,i + 3, j, k) > 0 || vector(FOV_2, i + 3, j, k) > 0)))) {
						vector(Ex, (d_ghost_width - 1) - i, j, k) = vector(Ex, (d_ghost_width - 1) - i + 1, j, k);
						vector(Ey, (d_ghost_width - 1) - i, j, k) = vector(Ey, (d_ghost_width - 1) - i + 1, j, k);
						vector(Ez, (d_ghost_width - 1) - i, j, k) = vector(Ez, (d_ghost_width - 1) - i + 1, j, k);
						vector(Bx, (d_ghost_width - 1) - i, j, k) = vector(Bx, (d_ghost_width - 1) - i + 1, j, k);
						vector(By, (d_ghost_width - 1) - i, j, k) = vector(By, (d_ghost_width - 1) - i + 1, j, k);
						vector(Bz, (d_ghost_width - 1) - i, j, k) = vector(Bz, (d_ghost_width - 1) - i + 1, j, k);
					}
					if ((vector(FOV_xUpper, i, j, k) > 0) && ((i - 1 >= 0 && (vector(FOV_1, i - 1, j, k) > 0 || vector(FOV_2, i - 1, j, k) > 0)) || (i - 2 >= 0 && (vector(FOV_1, i - 2, j, k) > 0 || vector(FOV_2, i - 2, j, k) > 0)) || (i - 3 >= 0 && (vector(FOV_1, i - 3, j, k) > 0 || vector(FOV_2, i - 3, j, k) > 0)))) {
						vector(Ex, i, j, k) = vector(Ex, i - 1, j, k);
						vector(Ey, i, j, k) = vector(Ey, i - 1, j, k);
						vector(Ez, i, j, k) = vector(Ez, i - 1, j, k);
						vector(Bx, i, j, k) = vector(Bx, i - 1, j, k);
						vector(By, i, j, k) = vector(By, i - 1, j, k);
						vector(Bz, i, j, k) = vector(Bz, i - 1, j, k);
					}
					if ((vector(FOV_yLower, i, j, k) > 0) && ((j + 1 < jlast && (vector(FOV_1,i, j + 1, k) > 0 || vector(FOV_2, i, j + 1, k) > 0)) || (j + 2 < jlast && (vector(FOV_1,i, j + 2, k) > 0 || vector(FOV_2, i, j + 2, k) > 0)) || (j + 3 < jlast && (vector(FOV_1,i, j + 3, k) > 0 || vector(FOV_2, i, j + 3, k) > 0)))) {
						vector(Ex, i, (d_ghost_width - 1) - j, k) = vector(Ex, i, (d_ghost_width - 1) - j + 1, k);
						vector(Ey, i, (d_ghost_width - 1) - j, k) = vector(Ey, i, (d_ghost_width - 1) - j + 1, k);
						vector(Ez, i, (d_ghost_width - 1) - j, k) = vector(Ez, i, (d_ghost_width - 1) - j + 1, k);
						vector(Bx, i, (d_ghost_width - 1) - j, k) = vector(Bx, i, (d_ghost_width - 1) - j + 1, k);
						vector(By, i, (d_ghost_width - 1) - j, k) = vector(By, i, (d_ghost_width - 1) - j + 1, k);
						vector(Bz, i, (d_ghost_width - 1) - j, k) = vector(Bz, i, (d_ghost_width - 1) - j + 1, k);
					}
					if ((vector(FOV_yUpper, i, j, k) > 0) && ((j - 1 >= 0 && (vector(FOV_1, i, j - 1, k) > 0 || vector(FOV_2, i, j - 1, k) > 0)) || (j - 2 >= 0 && (vector(FOV_1, i, j - 2, k) > 0 || vector(FOV_2, i, j - 2, k) > 0)) || (j - 3 >= 0 && (vector(FOV_1, i, j - 3, k) > 0 || vector(FOV_2, i, j - 3, k) > 0)))) {
						vector(Ex, i, j, k) = vector(Ex, i, j - 1, k);
						vector(Ey, i, j, k) = vector(Ey, i, j - 1, k);
						vector(Ez, i, j, k) = vector(Ez, i, j - 1, k);
						vector(Bx, i, j, k) = vector(Bx, i, j - 1, k);
						vector(By, i, j, k) = vector(By, i, j - 1, k);
						vector(Bz, i, j, k) = vector(Bz, i, j - 1, k);
					}
					if ((vector(FOV_zLower, i, j, k) > 0) && ((k + 1 < klast && (vector(FOV_1,i, j, k + 1) > 0 || vector(FOV_2, i, j, k + 1) > 0)) || (k + 2 < klast && (vector(FOV_1,i, j, k + 2) > 0 || vector(FOV_2, i, j, k + 2) > 0)) || (k + 3 < klast && (vector(FOV_1,i, j, k + 3) > 0 || vector(FOV_2, i, j, k + 3) > 0)))) {
						vector(Ex, i, j, (d_ghost_width - 1) - k) = vector(Ex, i, j, (d_ghost_width - 1) - k + 1);
						vector(Ey, i, j, (d_ghost_width - 1) - k) = vector(Ey, i, j, (d_ghost_width - 1) - k + 1);
						vector(Ez, i, j, (d_ghost_width - 1) - k) = vector(Ez, i, j, (d_ghost_width - 1) - k + 1);
						vector(Bx, i, j, (d_ghost_width - 1) - k) = vector(Bx, i, j, (d_ghost_width - 1) - k + 1);
						vector(By, i, j, (d_ghost_width - 1) - k) = vector(By, i, j, (d_ghost_width - 1) - k + 1);
						vector(Bz, i, j, (d_ghost_width - 1) - k) = vector(Bz, i, j, (d_ghost_width - 1) - k + 1);
					}
					if ((vector(FOV_zUpper, i, j, k) > 0) && ((k - 1 >= 0 && (vector(FOV_1, i, j, k - 1) > 0 || vector(FOV_2, i, j, k - 1) > 0)) || (k - 2 >= 0 && (vector(FOV_1, i, j, k - 2) > 0 || vector(FOV_2, i, j, k - 2) > 0)) || (k - 3 >= 0 && (vector(FOV_1, i, j, k - 3) > 0 || vector(FOV_2, i, j, k - 3) > 0)))) {
						vector(Ex, i, j, k) = vector(Ex, i, j, k - 1);
						vector(Ey, i, j, k) = vector(Ey, i, j, k - 1);
						vector(Ez, i, j, k) = vector(Ez, i, j, k - 1);
						vector(Bx, i, j, k) = vector(Bx, i, j, k - 1);
						vector(By, i, j, k) = vector(By, i, j, k - 1);
						vector(Bz, i, j, k) = vector(Bz, i, j, k - 1);
					}
				}
			}
		}
	}
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch >& patch = *p_it;
		double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_2 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_2_id).get())->getPointer();
		double* FOV_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_4_id).get())->getPointer();
		double* FOV_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_5_id).get())->getPointer();
		double* FOV_7 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_7_id).get())->getPointer();
		double* FOV_8 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_8_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
		double* FOV_zLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zLower_id).get())->getPointer();
		double* FOV_zUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_zUpper_id).get())->getPointer();
	
		double* constraint1_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_constraint1_4_id).get())->getPointer();
		double* q = ((pdat::NodeData<double> *) patch->getPatchData(d_q_id).get())->getPointer();
		double* Ex = ((pdat::NodeData<double> *) patch->getPatchData(d_Ex_id).get())->getPointer();
		double* Ey = ((pdat::NodeData<double> *) patch->getPatchData(d_Ey_id).get())->getPointer();
		double* Ez = ((pdat::NodeData<double> *) patch->getPatchData(d_Ez_id).get())->getPointer();
		double* constraint2_4 = ((pdat::NodeData<double> *) patch->getPatchData(d_constraint2_4_id).get())->getPointer();
		double* Bx = ((pdat::NodeData<double> *) patch->getPatchData(d_Bx_id).get())->getPointer();
		double* By = ((pdat::NodeData<double> *) patch->getPatchData(d_By_id).get())->getPointer();
		double* Bz = ((pdat::NodeData<double> *) patch->getPatchData(d_Bz_id).get())->getPointer();
		double* constraint1_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_constraint1_5_id).get())->getPointer();
		double* constraint2_5 = ((pdat::NodeData<double> *) patch->getPatchData(d_constraint2_5_id).get())->getPointer();
	
		//Get the dimensions of the patch
		hier::Box pbox = patch->getBox();
		const hier::Index boxfirst = patch->getBox().lower();
		const hier::Index boxlast = patch->getBox().upper();
	
		//Get delta spaces into an array. dx, dy, dz.
		std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
		const double* dx  = patch_geom->getDx();
	
		//Auxiliary definitions
		int ilast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
		int jlast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
		int klast = boxlast(2)-boxfirst(2) + 2 + 2 * d_ghost_width;
		for(int k = 0; k < klast; k++) {
			for(int j = 0; j < jlast; j++) {
				for(int i = 0; i < ilast; i++) {
					if ((vector(FOV_5, i, j, k) > 0 || (((i + 1 < ilast && vector(FOV_5, i + 1, j, k) > 0) || (i + 2 < ilast && vector(FOV_5, i + 2, j, k) > 0) || (j + 1 < jlast && vector(FOV_5, i, j + 1, k) > 0) || (j + 2 < jlast && vector(FOV_5, i, j + 2, k) > 0) || (k + 1 < klast && vector(FOV_5, i, j, k + 1) > 0) || (k + 2 < klast && vector(FOV_5, i, j, k + 2) > 0)) || ((i - 1 >= 0 && vector(FOV_5, i - 1, j, k) > 0) || (i - 2 >= 0 && vector(FOV_5, i - 2, j, k) > 0) || (j - 1 >= 0 && vector(FOV_5, i, j - 1, k) > 0) || (j - 2 >= 0 && vector(FOV_5, i, j - 2, k) > 0) || (k - 1 >= 0 && vector(FOV_5, i, j, k - 1) > 0) || (k - 2 >= 0 && vector(FOV_5, i, j, k - 2) > 0))) || (d_tappering && (neighbourTappering(FOV_5, i, j, k, ilast, jlast, klast, 2, 6 * d_sim_substep[ln] - 6))))) {
						vector(constraint1_5, i, j, k) = (mu * vector(q, i, j, k)) / c - (((Ficonstraint1_OtherI(vector(Ex, int(i + 1), j, k), dx, simPlat_dt, ilast, jlast) - Ficonstraint1_OtherI(vector(Ex, int(i - 1), j, k), dx, simPlat_dt, ilast, jlast)) / (2.0 * dx[0]) + (Fjconstraint1_OtherI(vector(Ey, i, int(j + 1), k), dx, simPlat_dt, ilast, jlast) - Fjconstraint1_OtherI(vector(Ey, i, int(j - 1), k), dx, simPlat_dt, ilast, jlast)) / (2.0 * dx[1])) + (Fkconstraint1_OtherI(vector(Ez, i, j, int(k + 1)), dx, simPlat_dt, ilast, jlast) - Fkconstraint1_OtherI(vector(Ez, i, j, int(k - 1)), dx, simPlat_dt, ilast, jlast)) / (2.0 * dx[2]));
						vector(constraint2_5, i, j, k) = 0.0 - (((Ficonstraint2_OtherI(vector(Bx, int(i + 1), j, k), dx, simPlat_dt, ilast, jlast) - Ficonstraint2_OtherI(vector(Bx, int(i - 1), j, k), dx, simPlat_dt, ilast, jlast)) / (2.0 * dx[0]) + (Fjconstraint2_OtherI(vector(By, i, int(j + 1), k), dx, simPlat_dt, ilast, jlast) - Fjconstraint2_OtherI(vector(By, i, int(j - 1), k), dx, simPlat_dt, ilast, jlast)) / (2.0 * dx[1])) + (Fkconstraint2_OtherI(vector(Bz, i, j, int(k + 1)), dx, simPlat_dt, ilast, jlast) - Fkconstraint2_OtherI(vector(Bz, i, j, int(k - 1)), dx, simPlat_dt, ilast, jlast)) / (2.0 * dx[2]));
					}
					if ((vector(FOV_4, i, j, k) > 0)) {
						vector(constraint1_4, i, j, k) = (mu * vector(q, i, j, k)) / c - (((Ficonstraint1_SoilS(vector(Ex, int(i + 1), j, k), dx, simPlat_dt, ilast, jlast) - Ficonstraint1_SoilS(vector(Ex, int(i - 1), j, k), dx, simPlat_dt, ilast, jlast)) / (2.0 * dx[0]) + (Fjconstraint1_SoilS(vector(Ey, i, int(j + 1), k), dx, simPlat_dt, ilast, jlast) - Fjconstraint1_SoilS(vector(Ey, i, int(j - 1), k), dx, simPlat_dt, ilast, jlast)) / (2.0 * dx[1])) + (Fkconstraint1_SoilS(vector(Ez, i, j, int(k + 1)), dx, simPlat_dt, ilast, jlast) - Fkconstraint1_SoilS(vector(Ez, i, j, int(k - 1)), dx, simPlat_dt, ilast, jlast)) / (2.0 * dx[2]));
						vector(constraint2_4, i, j, k) = 0.0 - (((Ficonstraint2_SoilS(vector(Bx, int(i + 1), j, k), dx, simPlat_dt, ilast, jlast) - Ficonstraint2_SoilS(vector(Bx, int(i - 1), j, k), dx, simPlat_dt, ilast, jlast)) / (2.0 * dx[0]) + (Fjconstraint2_SoilS(vector(By, i, int(j + 1), k), dx, simPlat_dt, ilast, jlast) - Fjconstraint2_SoilS(vector(By, i, int(j - 1), k), dx, simPlat_dt, ilast, jlast)) / (2.0 * dx[1])) + (Fkconstraint2_SoilS(vector(Bz, i, j, int(k + 1)), dx, simPlat_dt, ilast, jlast) - Fkconstraint2_SoilS(vector(Bz, i, j, int(k - 1)), dx, simPlat_dt, ilast, jlast)) / (2.0 * dx[2]));
					}
				}
			}
		}
	}
	

	t_step->stop();


	return simPlat_dt;
}

/*
 * Checks the finalization conditions              
 */
bool Problem::checkFinalization(const double current_time, const double simPlat_dt)
{
	if (greaterThan(current_time, 20.0)) { 
		return true;
	}
	return false;
	
	

}


bool Problem::neighbourTappering(const double* fov, const int ibase, const int jbase, const int kbase, const int ilast, const int jlast, const int klast, const int stencil, const int limit) const {
	for(int k = stencil + 1; k <= limit; k++) {
		for(int j = stencil + 1; j <= limit; j++) {
			for(int i = stencil + 1; i <= limit; i++) {
				if ((ibase + i >= d_ghost_width && ibase + i < ilast - d_ghost_width && jbase + j >= d_ghost_width && jbase + j < jlast - d_ghost_width && kbase + k >= d_ghost_width && kbase + k < klast - d_ghost_width && vector(fov, ibase + i, jbase + j, kbase + k) > 0) || (ibase + i >= d_ghost_width && ibase + i < ilast - d_ghost_width && jbase + j >= d_ghost_width && jbase + j < jlast - d_ghost_width && kbase - k >= d_ghost_width && kbase - k < klast - d_ghost_width && vector(fov, ibase + i, jbase + j, kbase - k) > 0) || (ibase + i >= d_ghost_width && ibase + i < ilast - d_ghost_width && jbase - j >= d_ghost_width && jbase - j < jlast - d_ghost_width && kbase + k >= d_ghost_width && kbase + k < klast - d_ghost_width && vector(fov, ibase + i, jbase - j, kbase + k) > 0) || (ibase + i >= d_ghost_width && ibase + i < ilast - d_ghost_width && jbase - j >= d_ghost_width && jbase - j < jlast - d_ghost_width && kbase - k >= d_ghost_width && kbase - k < klast - d_ghost_width && vector(fov, ibase + i, jbase - j, kbase - k) > 0) || (ibase - i >= d_ghost_width && ibase - i < ilast - d_ghost_width && jbase + j >= d_ghost_width && jbase + j < jlast - d_ghost_width && kbase + k >= d_ghost_width && kbase + k < klast - d_ghost_width && vector(fov, ibase - i, jbase + j, kbase + k) > 0) || (ibase - i >= d_ghost_width && ibase - i < ilast - d_ghost_width && jbase + j >= d_ghost_width && jbase + j < jlast - d_ghost_width && kbase - k >= d_ghost_width && kbase - k < klast - d_ghost_width && vector(fov, ibase - i, jbase + j, kbase - k) > 0) || (ibase - i >= d_ghost_width && ibase - i < ilast - d_ghost_width && jbase - j >= d_ghost_width && jbase - j < jlast - d_ghost_width && kbase + k >= d_ghost_width && kbase + k < klast - d_ghost_width && vector(fov, ibase - i, jbase - j, kbase + k) > 0) || (ibase - i >= d_ghost_width && ibase - i < ilast - d_ghost_width && jbase - j >= d_ghost_width && jbase - j < jlast - d_ghost_width && kbase - k >= d_ghost_width && kbase - k < klast - d_ghost_width && vector(fov, ibase - i, jbase - j, kbase - k) > 0)) {
					return true;
				}
			}
		}
	}
	return false;
}


/*
 *  Cell tagging routine - tag cells that require refinement based on a provided condition. 
 */
void Problem::applyGradientDetector(
   const std::shared_ptr< hier::PatchHierarchy >& hierarchy, 
   const int level_number,
   const double time, 
   const int tag_index,
   const bool initial_time,
   const bool uses_richardson_extrapolation_too) 
{
	if (d_regridding && level_number + 1 >= d_regridding_level_threshold) {
		hier::VariableDatabase *vdb = hier::VariableDatabase::getDatabase();
		if (!(vdb->checkVariableExists(d_regridding_field))) {
			TBOX_ERROR(d_object_name << ": Regridding field selected not found:n"					<<  d_regridding_field<<  "n");
		}

		std::shared_ptr< hier::PatchLevel > level(hierarchy->getPatchLevel(level_number));

		for (hier::PatchLevel::iterator ip(level->begin()); ip != level->end(); ++ip) {
			const std::shared_ptr< hier::Patch >& patch = *ip;
			int* tags = ((pdat::NodeData<int> *) patch->getPatchData(tag_index).get())->getPointer();
			const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
			const double* dx  = patch_geom->getDx();
			const hier::Index tfirst = patch->getPatchData(tag_index)->getGhostBox().lower();
			const hier::Index tlast  = patch->getPatchData(tag_index)->getGhostBox().upper();
			const hier::Index boxfirst = patch->getBox().lower();
			const hier::Index boxlast  = patch->getBox().upper();
			int ilast = boxlast(0)-boxfirst(0)+2+2*d_ghost_width;
			int itlast = tlast(0)-tfirst(0)+1;
			int jlast = boxlast(1)-boxfirst(1)+2+2*d_ghost_width;
			int jtlast = tlast(1)-tfirst(1)+1;
			int klast = boxlast(2)-boxfirst(2)+2+2*d_ghost_width;
			int ktlast = tlast(2)-tfirst(2)+1;
			if (d_regridding_type == "GRADIENT") {
				int regrid_field_id = vdb->getVariable(d_regridding_field)->getInstanceIdentifier();
				double* regrid_field = ((pdat::NodeData<double> *) patch->getPatchData(regrid_field_id).get())
->getPointer();
				for(int index2 = 0; index2 < (tlast(2)-tfirst(2))+1; index2++) {
					for(int index1 = 0; index1 < (tlast(1)-tfirst(1))+1; index1++) {
						for(int index0 = 0; index0 < (tlast(0)-tfirst(0))+1; index0++) {
							vectorT(tags,index0,index1,index2) = 0;
							if (vector(regrid_field, index0+d_ghost_width,index1+d_ghost_width,index2+d_ghost_width)!=0) {
								if ((fabs(vector(regrid_field, index0+1+d_ghost_width, index1+d_ghost_width, index2+d_ghost_width)-vector(regrid_field, index0+d_ghost_width, index1+d_ghost_width, index2+d_ghost_width)) > d_regridding_compressionFactor * MIN(fabs(vector(regrid_field, index0+2+d_ghost_width, index1+d_ghost_width, index2+d_ghost_width)-vector(regrid_field, index0+1+d_ghost_width, index1+d_ghost_width, index2+d_ghost_width)) + d_regridding_mOffset * pow(dx[0], 2), fabs(vector(regrid_field, index0+d_ghost_width, index1+d_ghost_width, index2+d_ghost_width)-vector(regrid_field, index0-1+d_ghost_width, index1+d_ghost_width, index2+d_ghost_width)) + d_regridding_mOffset * pow(dx[0], 2)) ) ||(fabs(vector(regrid_field, index0+d_ghost_width, index1+1+d_ghost_width, index2+d_ghost_width)-vector(regrid_field, index0+d_ghost_width, index1+d_ghost_width, index2+d_ghost_width)) > d_regridding_compressionFactor * MIN(fabs(vector(regrid_field, index0+d_ghost_width, index1+2+d_ghost_width, index2+d_ghost_width)-vector(regrid_field, index0+d_ghost_width, index1+1+d_ghost_width, index2+d_ghost_width)) + d_regridding_mOffset * pow(dx[1], 2), fabs(vector(regrid_field, index0+d_ghost_width, index1+d_ghost_width, index2+d_ghost_width)-vector(regrid_field, index0+d_ghost_width, index1-1+d_ghost_width, index2+d_ghost_width)) + d_regridding_mOffset * pow(dx[1], 2)) ) ||(fabs(vector(regrid_field, index0+d_ghost_width, index1+d_ghost_width, index2+1+d_ghost_width)-vector(regrid_field, index0+d_ghost_width, index1+d_ghost_width, index2+d_ghost_width)) > d_regridding_compressionFactor * MIN(fabs(vector(regrid_field, index0+d_ghost_width, index1+d_ghost_width, index2+2+d_ghost_width)-vector(regrid_field, index0+d_ghost_width, index1+d_ghost_width, index2+1+d_ghost_width)) + d_regridding_mOffset * pow(dx[2], 2), fabs(vector(regrid_field, index0+d_ghost_width, index1+d_ghost_width, index2+d_ghost_width)-vector(regrid_field, index0+d_ghost_width, index1+d_ghost_width, index2-1+d_ghost_width)) + d_regridding_mOffset * pow(dx[2], 2)) ) ) {
									vectorT(tags,index0,index1,index2) = 1;
								}
							}
						}
					}
				}
			} else {
				if (d_regridding_type == "FUNCTION") {
					int regrid_field_id = vdb->getVariable(d_regridding_field)->getInstanceIdentifier();
					double* regrid_field = ((pdat::NodeData<double> *) patch->getPatchData(regrid_field_id).get())
	->getPointer();
					for(int index2 = 0; index2 < (tlast(2)-tfirst(2))+1; index2++) {
						for(int index1 = 0; index1 < (tlast(1)-tfirst(1))+1; index1++) {
							for(int index0 = 0; index0 < (tlast(0)-tfirst(0))+1; index0++) {
								vectorT(tags,index0,index1,index2) = 0;
								if (vector(regrid_field, index0+d_ghost_width,index1+d_ghost_width,index2+d_ghost_width) > d_regridding_threshold) {
									vectorT(tags,index0,index1,index2) = 1;
								}
							}
						}
					}
	
				}
			}
		}
	}
}



