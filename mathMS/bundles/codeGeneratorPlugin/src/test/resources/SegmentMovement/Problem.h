
#include "SAMRAI/SAMRAI_config.h"

#include "SAMRAI/hier/Box.h"
#include "SAMRAI/geom/CartesianGridGeometry.h"
#include "SAMRAI/tbox/Database.h"
#include "SAMRAI/mesh/StandardTagAndInitStrategy.h"
#include "SAMRAI/hier/Index.h"
#include "SAMRAI/hier/Patch.h"
#include "SAMRAI/xfer/RefineAlgorithm.h"
#include "SAMRAI/xfer/RefineSchedule.h"
#include "SAMRAI/xfer/CoarsenAlgorithm.h"
#include "SAMRAI/xfer/CoarsenSchedule.h"
#include "boost/shared_ptr.hpp"
#include "MainRestartData.h"
#include "SAMRAI/appu/VisItDataWriter.h"
#include "SAMRAI/pdat/IndexData.h"
#include "Interphase.h"
using namespace std;
using namespace SAMRAI;

#define DIMENSIONS 2

#define TIMESLICES 2
const double WaterTime[TIMESLICES] = {0, 2.5};
#define POINTS_Water 25
#define UNIONS_Water 24
//Common information for segment: Water
static int WaterOrientation[UNIONS_Water] = {9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9};
const double WaterPoints[TIMESLICES][POINTS_Water][DIMENSIONS] = {{{-0.27774800001497374, 5.24890677299406E-7}, {-0.2777479887012044, 5.211589601959156E-7}, {-0.13495149612511, 2.532196321998187E-7}, {-0.1349515074388774, 2.5695134863859916E-7}, {-0.1349514999333979, 2.544757522893116E-7}, {-0.09495149850568381, 1.7816352213255197E-7}, {-0.0949515023177245, 1.794208800426001E-7}, {-0.07226651250310871, 1.3933098446021813E-7}, {-0.07226650118933929, 1.355992673567278E-7}, {-0.004980960117090382, 9.34656826502956E-9}, {-0.029980959882369835, 5.998756098492343E-8}, {-0.005561562390521883, 1.4535090895263534E-8}, {-0.011188825134553836, 2.9321683616394377E-8}, {-0.027433169416850256, 5.7547203438231594E-8}, {-0.006064266091049386, 1.670350486938722E-8}, {-0.005107356870689502, 1.1881707074545706E-8}, {-0.022088573996730466, 4.9351120945161365E-8}, {-0.029104613130995237, 5.9154396109490756E-8}, {-0.009030622426020457, 2.4377316783200844E-8}, {-0.007265320081908275, 2.015602989905603E-8}, {-0.019634574650244933, 4.519671499610405E-8}, {-0.03918355791269471, 7.577893073195147E-8}, {-0.02542662030353541, 5.461006846184977E-8}, {-0.01561777661720708, 3.789112562617164E-8}, {-0.005044153357400826, 1.0578720985858363E-8}}
, 
{{-0.27774401842470275, 5.248832063552325E-7}, {-0.2777440071109334, 5.211514892517421E-7}, {-0.1349480032929262, 2.5321307834759466E-7}, {-0.1349480146066936, 2.569447947863751E-7}, {-0.13494800710121407, 2.5446919843708756E-7}, {-0.0949474990340194, 1.7815601763623398E-7}, {-0.09494800352507621, 1.7941431500632786E-7}, {-0.07226301371046041, 1.3932441942394591E-7}, {-0.07226300239669099, 1.3559270232045559E-7}, {-0.004977085070121935, 9.339297235716978E-9}, {-0.029977112402549598, 5.99803416818336E-8}, {-0.0053199540260455815, 1.131525175000482E-8}, {-0.007564406191886886, 1.6190946336208286E-8}, {-0.02593760946208344, 5.2137977154718994E-8}, {-0.005611354698064155, 1.2073705172901197E-8}, {-0.0051035035003447185, 1.0791681905410376E-8}, {-0.019004109611859984, 3.864405824013543E-8}, {-0.027700911969083683, 5.5584863454964885E-8}, {-0.006491205403991405, 1.4001661394458123E-8}, {-0.005993655155856492, 1.291111136177472E-8}, {-0.014116058489884715, 2.9187332644978136E-8}, {-0.03917970745264224, 7.577170583683756E-8}, {-0.022603809118037443, 4.5617648793052205E-8}, {-0.009376657139229472, 1.9848747467510422E-8}, {-0.0050403032698773794, 1.0571496789747469E-8}}
};
const int WaterUnions[UNIONS_Water][DIMENSIONS] = {{5, 2}, {4, 6}, {1, 0}, {0, 3}, {3, 4}, {6, 7}, {8, 5}, {7, 10}, {2, 1}, {11, 15}, {18, 19}, {12, 18}, {19, 14}, {17, 13}, {10, 17}, {14, 11}, {13, 22}, {22, 16}, {20, 23}, {16, 20}, {23, 12}, {15, 24}, {24, 9}, {9, 8}};
static double WaterCellPoints[TIMESLICES][POINTS_Water][DIMENSIONS] = {{{1e10, 1e10}, {1e10, 1e10}, {1e10, 1e10}, {1e10, 1e10}, {1e10, 1e10}, {1e10, 1e10}, {1e10, 1e10}, {1e10, 1e10}, {1e10, 1e10}, {1e10, 1e10}, {1e10, 1e10}, {1e10, 1e10}, {1e10, 1e10}, {1e10, 1e10}, {1e10, 1e10}, {1e10, 1e10}, {1e10, 1e10}, {1e10, 1e10}, {1e10, 1e10}, {1e10, 1e10}, {1e10, 1e10}, {1e10, 1e10}, {1e10, 1e10}, {1e10, 1e10}, {1e10, 1e10}}, 
{{1e10, 1e10}, {1e10, 1e10}, {1e10, 1e10}, {1e10, 1e10}, {1e10, 1e10}, {1e10, 1e10}, {1e10, 1e10}, {1e10, 1e10}, {1e10, 1e10}, {1e10, 1e10}, {1e10, 1e10}, {1e10, 1e10}, {1e10, 1e10}, {1e10, 1e10}, {1e10, 1e10}, {1e10, 1e10}, {1e10, 1e10}, {1e10, 1e10}, {1e10, 1e10}, {1e10, 1e10}, {1e10, 1e10}, {1e10, 1e10}, {1e10, 1e10}, {1e10, 1e10}, {1e10, 1e10}}};


class Problem : 
   public mesh::StandardTagAndInitStrategy,
   public xfer::RefinePatchStrategy,
   public xfer::CoarsenPatchStrategy
{
public:
	/*
	 * Constructor of the problem.
	 */
	Problem(
		const string& object_name,
		const tbox::Dimension& dim,
		std::shared_ptr<tbox::Database>& input_db,
		std::shared_ptr<geom::CartesianGridGeometry >& grid_geom, 
	   	std::shared_ptr<hier::PatchHierarchy >& patch_hierarchy, 
		const double dt, 
		hier::BoxContainer& ba,
		const bool init_from_files);
  
	/*
	 * Destructor.
	 */
	~Problem();
 
	/*
	 * Initialize the data from a given level.
   	 */
	virtual void initializeLevelData(
		const std::shared_ptr<hier::PatchHierarchy >& hierarchy ,
		const int level_number ,
		const double init_data_time ,
		const bool can_be_refined ,
		const bool initial_time ,
		const std::shared_ptr<hier::PatchLevel >& old_level=std::shared_ptr<hier::PatchLevel>() ,
		const bool allocate_data = true);

	/*
	 * Reset the hierarchy-dependent internal information.
	 */
	virtual void resetHierarchyConfiguration(
		const std::shared_ptr<hier::PatchHierarchy >& new_hierarchy ,
		int coarsest_level ,
		int finest_level);

	/*
	 * Perform a single step from the discretization schema algorithm   
	 */
	void step(
		const double simPlat_dt, 
		const double simPlat_time, 
		const int simPlat_iteration);

	/*
	 * Checks the finalization conditions              
	 */
	bool checkFinalization(
		const double simPlat_time, 
		const double simPlat_dt);

	/*
	 * This method sets the physical boundary conditions.
	*/
	void setPhysicalBoundaryConditions(
		hier::Patch& patch,
		const double fill_time,
		const hier::IntVector& ghost_width_to_fill);
	/*
	 * Set up external plotter to plot internal data from this class.        
	 * Tell the plotter about the refinement ratios.  Register variables     
	 * appropriate for plotting.                                            
	 */
	int setupPlotter(appu::VisItDataWriter &plotter ) const;

	/*
	 * Map data on a patch. This mapping is done only at the begining of the simulation.
	 */
	void mapDataOnPatch(const double time, const bool initial_time, const int ln, const std::shared_ptr< hier::PatchLevel >& level, int timeSlice);

	/*
	 * Sets the limit for the checkstencil routine
	 */
	void setStencilLimits(std::shared_ptr< hier::Patch > patch, int i, int j, int v) const;

	/*
	 * Checks if the point has a stencil width
	 */
	bool checkStencil(std::shared_ptr< hier::Patch > patch, int i, int j, int v) const;

	/*
	 * Flood-Fill algorithm
	 */
	void floodfill(std::shared_ptr< hier::Patch > patch, int i, int j, int pred, int seg) const;



	/*
	 * Interphase mapping. Calculates the FOV and its variables.
	 */
	void interphaseMapping(const double time, const bool initial_time, const int ln, const std::shared_ptr< hier::PatchLevel >& level, const int remesh);




	/*
	 * Perform the segment movement
	 */
	void interphaseMovement(const double time, int rrho_id, int rmr_id, int rmz_id, int Null_id, int rrhoDL_id, int rho_id, int mr_id, int mz_id, int P_id, int tau_id, double dt, int* remesh);
	void getCellPoints(const std::shared_ptr< hier::PatchLevel > level, int timeSlice);
	void getTimeSlices(MainRestartData* mrd);
	void putTimeSlices(MainRestartData* mrd);

	/*
	 * Initialize data on a patch. This initialization is done only at the begining of the simulation.
	 */
	void initializeDataOnPatch(
		hier::Patch& patch,
		const double time,
       		const bool initial_time) const;

	void initializeDataFromFile(
		hier::Patch& patch,
       		const bool initial_time) const;

	void mapFileToField(
		double* field,
		std::string fieldName,
		const hier::Index boxfirst,
		const int ilast, 
		const int ilastG, 
		const int jlast, 
		const int jlastG) const;


	/*
	 *  Cell tagging routine - tag cells that require refinement based on a provided condition. 
	 */
	void applyGradientDetector(
	   	std::shared_ptr< hier::PatchHierarchy >& hierarchy, 
	   	const int level_number,
	   	const double time, 
	   	const int tag_index,
	   	const bool initial_time,
	   	const bool uses_richardson_extrapolation_too);

	/*
	* Return maximum stencil width needed for user-defined
	* data interpolation operations.  Default is to return
	* zero, assuming no user-defined operations provided.
	*/
	hier::IntVector getRefineOpStencilWidth(const tbox::Dimension &dim) const
	{
		return hier::IntVector::getZero(dim);
	}

	/*
	* Pre- and post-processing routines for implementing user-defined
	* spatial interpolation routines applied to variables.  The 
	* interpolation routines are used in the MOL AMR algorithm
	* for filling patch ghost cells before advancing data on a level
	* and after regridding a level to fill portions of the new level
	* from some coarser level.  These routines are called automatically
	* from within patch boundary filling schedules; thus, some concrete
	* function matching these signatures must be provided in the user's
	* patch model.  However, the routines only need to perform some 
	* operations when "USER_DEFINED_REFINE" is given as the interpolation 
	* method for some variable when the patch model registers variables
	* with the MOL integration algorithm, typically.  If the 
	* user does not provide operations that refine such variables in either 
	* of these routines, then they will not be refined.
	*
	* The order in which these operations are used in each patch 
	* boundary filling schedule is:
	* 
	* - \b (1) {Call user's preprocessRefine() routine.}
	* - \b (2) {Refine all variables with standard interpolation operators.}
	* - \b (3) {Call user's postprocessRefine() routine.}
	* 
	* 
	* Also, user routines that implement these functions must use 
	* data corresponding to the d_scratch context on both coarse and
	* fine patches.
	*/
	virtual void preprocessRefine(
		hier::Patch& fine,
		const hier::Patch& coarse,
                const hier::Box& fine_box,
                const hier::IntVector& ratio)
	{
		NULL_USE(fine);
		NULL_USE(coarse);
		NULL_USE(fine_box);
		NULL_USE(ratio);
	}
	virtual void postprocessRefine(
		hier::Patch& fine,
                const hier::Patch& coarse,
                const hier::Box& fine_box,
                const hier::IntVector& ratio)
	{
		NULL_USE(fine);
		NULL_USE(coarse);
		NULL_USE(fine_box);
		NULL_USE(ratio);
	}


	/*
	* Return maximum stencil width needed for user-defined
	* data coarsen operations.  Default is to return
	* zero, assuming no user-defined operations provided.
	*/
	hier::IntVector getCoarsenOpStencilWidth( const tbox::Dimension &dim ) const {
		return hier::IntVector::getZero(dim);
	}

	/*
	* Pre- and post-processing routines for implementing user-defined
	* spatial coarsening routines applied to variables.  The coarsening 
	* routines are used in the MOL AMR algorithm synchronizing 
	* coarse and fine levels when they have been integrated to the same
	* point.  These routines are called automatically from within the 
	* data synchronization coarsen schedules; thus, some concrete
	* function matching these signatures must be provided in the user's
	* patch model.  However, the routines only need to perform some
	* operations when "USER_DEFINED_COARSEN" is given as the coarsening
	* method for some variable when the patch model registers variables
	* with the MOL level integration algorithm, typically.  If the
	* user does not provide operations that coarsen such variables in either
	* of these routines, then they will not be coarsened.
	*
	* The order in which these operations are used in each coarsening
	* schedule is:
	* 
	* - \b (1) {Call user's preprocessCoarsen() routine.}
	* - \b (2) {Coarsen all variables with standard coarsening operators.}
	* - \b (3) {Call user's postprocessCoarsen() routine.}
	* 
	*
	* Also, user routines that implement these functions must use
	* corresponding to the d_new context on both coarse and fine patches
	* for time-dependent quantities.
	*/
	virtual void preprocessCoarsen(
		hier::Patch& coarse,
                const hier::Patch& fine,
                const hier::Box& coarse_box,
                const hier::IntVector& ratio)
	{
		NULL_USE(fine);
		NULL_USE(coarse);
		NULL_USE(coarse_box);
		NULL_USE(ratio);
	}
	virtual void postprocessCoarsen(
		hier::Patch& coarse,
                const hier::Patch& fine,
                const hier::Box& coarse_box,
                const hier::IntVector& ratio)
	{
		NULL_USE(fine);
		NULL_USE(coarse);
		NULL_USE(coarse_box);
		NULL_USE(ratio);
	}

	/*
	 * Computes the dt to be used
	 */
	double computeDt() const;

	

	/*
	 * Checks if the point has to be stalled
	 */
	bool checkStalled(std::shared_ptr< hier::Patch > patch, int i, int j, int v) const;



	hier::BoxContainer d_regridding_boxes;

private:	 
	//Variables for the refine and coarsen algorithms

	std::shared_ptr< xfer::RefineAlgorithm > d_bdry_fill_init;
	std::shared_ptr< xfer::RefineAlgorithm > d_bdry_fill_advance1;
	std::vector< std::shared_ptr< xfer::RefineSchedule > > d_bdry_sched_advance1;
	std::shared_ptr< xfer::RefineAlgorithm > d_bdry_fill_advance2;
	std::vector< std::shared_ptr< xfer::RefineSchedule > > d_bdry_sched_advance2;
	std::shared_ptr< xfer::RefineAlgorithm > d_bdry_fill_advance3;
	std::vector< std::shared_ptr< xfer::RefineSchedule > > d_bdry_sched_advance3;
	std::shared_ptr< xfer::RefineAlgorithm > d_bdry_fill_advance4;
	std::vector< std::shared_ptr< xfer::RefineSchedule > > d_bdry_sched_advance4;
	std::shared_ptr< xfer::RefineAlgorithm > d_bdry_fill_advance5;
	std::vector< std::shared_ptr< xfer::RefineSchedule > > d_bdry_sched_advance5;
	std::shared_ptr< xfer::RefineAlgorithm > d_bdry_fill_advance6;
	std::vector< std::shared_ptr< xfer::RefineSchedule > > d_bdry_sched_advance6;
	std::shared_ptr< xfer::RefineAlgorithm > d_bdry_fill_advance7;
	std::vector< std::shared_ptr< xfer::RefineSchedule > > d_bdry_sched_advance7;
	std::shared_ptr< xfer::RefineAlgorithm > d_bdry_fill_advance8;
	std::vector< std::shared_ptr< xfer::RefineSchedule > > d_bdry_sched_advance8;
	std::shared_ptr< xfer::RefineAlgorithm > d_bdry_fill_advance9;
	std::vector< std::shared_ptr< xfer::RefineSchedule > > d_bdry_sched_advance9;
	std::shared_ptr< xfer::RefineAlgorithm > d_bdry_fill_advance10;
	std::vector< std::shared_ptr< xfer::RefineSchedule > > d_bdry_sched_advance10;
	std::shared_ptr< xfer::RefineAlgorithm > d_bdry_fill_advance11;
	std::vector< std::shared_ptr< xfer::RefineSchedule > > d_bdry_sched_advance11;
	std::shared_ptr< xfer::RefineAlgorithm > d_bdry_fill_advance12;
	std::vector< std::shared_ptr< xfer::RefineSchedule > > d_bdry_sched_advance12;
	std::shared_ptr< xfer::RefineAlgorithm > d_bdry_fill_advance13;
	std::vector< std::shared_ptr< xfer::RefineSchedule > > d_bdry_sched_advance13;
	std::shared_ptr< xfer::RefineAlgorithm > d_bdry_fill_advance14;
	std::vector< std::shared_ptr< xfer::RefineSchedule > > d_bdry_sched_advance14;
	std::shared_ptr< xfer::RefineAlgorithm > d_bdry_fill_advance15;
	std::vector< std::shared_ptr< xfer::RefineSchedule > > d_bdry_sched_advance15;
	std::shared_ptr< xfer::RefineAlgorithm > d_mapping_fill2;

	std::shared_ptr< xfer::RefineAlgorithm > d_mapping_fill;
   	std::shared_ptr< xfer::CoarsenAlgorithm > d_coarsen_algorithm;
   	std::vector< std::shared_ptr< xfer::CoarsenSchedule > > d_coarsen_schedule;
	std::shared_ptr< xfer::RefineAlgorithm > d_fill_new_level;

	//Object name
	std::string d_object_name;

	//Pointers to the grid geometry and the patch hierarchy
   	std::shared_ptr<geom::CartesianGridGeometry > d_grid_geometry;
	std::shared_ptr<hier::PatchHierarchy > d_patch_hierarchy;

	//Identifiers of the fields and auxiliary fields
	int d_rrho_id, d_rrho_p_id, d_rmr_id, d_rmr_p_id, d_rmz_id, d_rmz_p_id, d_Null_id, d_Null_p_id, d_rrhoDL_id, d_rrhoDL_p_id, d_rho_id, d_mr_id, d_mr_p_id, d_mz_id, d_P_id, d_P_p_id, d_tau_id, d_tau_p_id, d_wnp_0_id, d_wnm_0_id, d_wtz_0_id, d_wtr_0_id, d_DL_0_id, d_NullEigen_1_id, d_extrapolatedSBrhoSPi_id, d_extrapolatedSBrhoSPj_id, d_extrapolatedSBmzSPi_id, d_extrapolatedSBmzSPj_id, d_extrapolatedSBmrSPi_id, d_extrapolatedSBmrSPj_id, d_extrapolatedSBPSPi_id, d_extrapolatedSBPSPj_id, d_extrapolatedSBtauSPi_id, d_extrapolatedSBtauSPj_id, d_Flux_extSBrrhoSPi_1_id, d_Flux_extSBrrhoSPj_1_id, d_Flux_extSBrmzSPi_1_id, d_Flux_extSBrmzSPj_1_id, d_Flux_extSBrmrSPi_1_id, d_Flux_extSBrmrSPj_1_id, d_Flux_extSBrrhoDLSPi_1_id, d_Flux_extSBrrhoDLSPj_1_id, d_Speedi_1_id, d_Speedj_1_id, d_k1rrho_id, d_k1rmz_id, d_k1rmr_id, d_k1rrhoDL_id, d_k1rho_id, d_k1mz_id, d_k1mr_id, d_k1P_id, d_k1tau_id, d_k2rrho_id, d_k2rmz_id, d_k2rmr_id, d_k2rrhoDL_id, d_k2rho_id, d_k2mz_id, d_k2mr_id, d_k2P_id, d_k2tau_id, d_Flux_extSBNullSPi_3_id, d_Flux_extSBNullSPj_3_id, d_Speedi_3_id, d_Speedj_3_id, d_k1Null_id, d_k2Null_id, d_extrapolatedSBrrhoSPi_id, d_extrapolatedSBrrhoSPj_id, d_extrapolatedSBrmzSPi_id, d_extrapolatedSBrmzSPj_id, d_extrapolatedSBrmrSPi_id, d_extrapolatedSBrmrSPj_id, d_extrapolatedSBrrhoDLSPi_id, d_extrapolatedSBrrhoDLSPj_id, d_extrapolatedSBNullSPi_id, d_extrapolatedSBNullSPj_id, d_Flux_extSBrrhoSPi_id, d_Flux_extSBrrhoSPj_id, d_Flux_extSBrmzSPi_id, d_Flux_extSBrmzSPj_id, d_Flux_extSBrmrSPi_id, d_Flux_extSBrmrSPj_id, d_Flux_extSBrrhoDLSPi_id, d_Flux_extSBrrhoDLSPj_id, d_Flux_extSBNullSPi_id, d_Flux_extSBNullSPj_id, d_d_i_rrho_id, d_d_j_rrho_id, d_d_i_rmr_id, d_d_j_rmr_id, d_d_i_rmz_id, d_d_j_rmz_id, d_d_i_rrhoDL_id, d_d_j_rrhoDL_id, d_d_i_rho_id, d_d_j_rho_id, d_d_i_mr_id, d_d_j_mr_id, d_d_i_mz_id, d_d_j_mz_id, d_d_i_P_id, d_d_j_P_id, d_d_i_tau_id, d_d_j_tau_id, d_d_i_Null_id, d_d_j_Null_id, d_stalled_1_id, d_stalled_3_id, d_interphase_id, d_soporte_id, d_LS_1_id, d_ls_1_00_id, d_ls_1_01_id, d_ls_1_02_id, d_ls_1_10_id, d_ls_1_11_id, d_ls_1_12_id, d_ls_1_20_id, d_ls_1_21_id, d_ls_1_22_id, d_LS_3_id, d_ls_3_00_id, d_ls_3_01_id, d_ls_3_02_id, d_ls_3_10_id, d_ls_3_11_id, d_ls_3_12_id, d_ls_3_20_id, d_ls_3_21_id, d_ls_3_22_id, d_normali_13_id, d_velocityi_13_id, d_normalj_13_id, d_velocityj_13_id;

	//Parameter variables
	double v0;
	double gz;
	double lambdaout;
	double mu;
	double tend;
	double vt0;
	double gamma;
	double lambdain;
	double c0;
	double rho0;
	double zeta;
	double gr;
	double Paux;
	int corridor_width;

	//Refine operators
	std::shared_ptr< hier::RefineOperator > refine_rrhoDL, refine_mr, refine_P, refine_tau, refine_rmr, refine_mz, refine_rho, refine_rrho, refine_Null, refine_rmz, refine_segment;

	double zLower, zUpper, zGlower, zGupper, rLower, rUpper, rGlower, rGupper;

	//mapping fields
	int d_nonSync_id, d_interior_id, d_FOV_1_id, d_FOV_3_id, d_FOV_rLower_id, d_FOV_rUpper_id, d_FOV_zLower_id, d_FOV_zUpper_id;

	//Stencil of the discretization method variable
	int d_ghost_width;

	//initial dt
	double initial_dt;

   	const tbox::Dimension d_dim;

	//Initialization from files
	bool d_init_from_files;

	//regridding options
	std::shared_ptr<tbox::Database> regridding_db;
	bool d_regridding;
	std::string d_regridding_field, d_regridding_type;
	double d_regridding_compressionFactor, d_regridding_mOffset, d_regridding_threshold;

	//Gets the coarser patch that includes the box.
	const std::shared_ptr<hier::Patch >& getCoarserPatch(
		const std::shared_ptr< hier::PatchLevel >& level,
		const hier::Box interior, 
		const hier::IntVector ratio);

	//Force to refine the boundaries of new levels after a regridding
	void fillBoundariesAfterRegrid(
		const std::shared_ptr<hier::PatchHierarchy >& hierarchy,
		const std::shared_ptr< hier::PatchLevel >& level,
		const int coarser_level);

	static bool Equals(double d1, double d2);
	static inline int GetExpoBase2(double d);
};


