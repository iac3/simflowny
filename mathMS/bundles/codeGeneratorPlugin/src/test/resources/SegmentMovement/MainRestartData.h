#ifndef included_MainRestartData
#define included_MainRestartData

#include "SAMRAI/SAMRAI_config.h"
#include "SAMRAI/tbox/Database.h"
#include "boost/shared_ptr.hpp"
#include "SAMRAI/tbox/Serializable.h"
#ifndef included_String
#include <string>
#define included_String
#endif

using namespace std;
using namespace SAMRAI;

/**
 * Class MainRestartData is a concrete subclass of tbox::Serializable that is 
 * used for storing and accessing the data in main that is necessary for 
 * restart.
 */

class MainRestartData: public tbox::Serializable
{
public:
   /**
    * The constructor for the serializable base class does nothing interesting.
    */
   MainRestartData(const string& object_name,
                   std::shared_ptr<tbox::Database>& input_db);

   /**
    * The virtual destructor for the serializable base class does nothing
    * interesting.
    */
   virtual ~MainRestartData();

   /**
    * Returns d_start_time.
    */
   virtual double getStartTime();

   /**
    * Returns d_regrid_step.
    */
   virtual int getRegridStep();

   /**
    * Returns d_loop_time.
    */
   virtual double getLoopTime();

   /**
    * Sets d_loop_time.
    */
   virtual void setLoopTime(const double loop_time);

   /**
    * Returns d_iteration_number.
    */
   virtual int getIterationNumber();

   /**
    * Sets d_iteration_number.
    */
   virtual void setIterationNumber(const int iter_num);

   /**
    * Writes out d_max_timesteps, d_start_time, d_end_time,
    * d_regrid_step, d_tag_buffer, d_loop_time, d_iteration_number.
    */
   virtual void putToRestart(const std::shared_ptr<tbox::Database>& db) const;

	virtual void addWaterCellPoints(double* x3d);
	virtual double* getWaterCellPoints();


private:
   /**
    * Reads in max_timesteps, start_time, end_time,
    * regrid_step, tag_buffer from the specified input database.
    * Any values from the input database override values found
    * in the restart database.
    */
   virtual void getFromInput( std::shared_ptr<tbox::Database>& input_db,
                              bool is_from_restart);

   /**
    * Reads in d_max_timesteps, d_start_time, d_end_time,
    * d_regrid_step, d_tag_buffer, d_loop_time, d_iteration_number
    * from the specified restart database.
    */
   virtual void getFromRestart(); 

   double d_start_time;
   int d_regrid_step;
   double d_loop_time;
   int d_iteration_number;

   string d_object_name;
	double WaterCellPoints[2 * 25 * 2];

};

#endif
