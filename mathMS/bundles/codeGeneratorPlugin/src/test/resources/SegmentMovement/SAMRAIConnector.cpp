#include <stdio.h>
#include "string.h"

#include "Problem.h"

#include "SAMRAI/tbox/SAMRAIManager.h"
#include "SAMRAI/tbox/Database.h"
#include "SAMRAI/tbox/InputDatabase.h"
#include "SAMRAI/tbox/InputManager.h"
#include "SAMRAI/tbox/RestartManager.h"
#include "SAMRAI/tbox/SAMRAI_MPI.h"
#include "boost/shared_ptr.hpp"
#include "SAMRAI/tbox/PIO.h"
#include "SAMRAI/tbox/Utilities.h"
#include "SAMRAI/tbox/Timer.h"
#include "SAMRAI/tbox/TimerManager.h"
#include "SAMRAI/mesh/BergerRigoutsos.h"
#include "SAMRAI/geom/CartesianGridGeometry.h"
#include "SAMRAI/mesh/GriddingAlgorithm.h"
#include "SAMRAI/mesh/TreeLoadBalancer.h"
#include "SAMRAI/hier/PatchHierarchy.h"
#include "MainRestartData.h"
#include "SAMRAI/mesh/StandardTagAndInitialize.h"
#include "SAMRAI/appu/VisItDataWriter.h"

// SAMRAI namespaces
using namespace std;
using namespace SAMRAI;

//Global variables
Problem* problem;

int iteration_num;
double loop_time;
double dt;
int viz_dump_interval;
int output_interval;
string viz_dump_dirname;
int regridding_interval;
int restore_num = 0;
bool is_from_restart = false;
bool init_from_files = false;


int main( int argc, char* argv[] )
{
	tbox::SAMRAI_MPI::init(&argc, &argv);
	tbox::SAMRAIManager::initialize();
	tbox::SAMRAIManager::startup();
	const tbox::SAMRAI_MPI& mpi(tbox::SAMRAI_MPI::getSAMRAIWorld());
	tbox::SAMRAIManager::setMaxNumberPatchDataEntries(117);

    	std::shared_ptr<tbox::InputDatabase> input_db(new tbox::InputDatabase("input_db"));
    	std::string input_file(argv[1]);
    	tbox::InputManager::getManager()->parseInputFile(input_file, input_db);
    	if (!input_db->keyExists("Main")) return -1;
    	std::shared_ptr<tbox::Database> main_db(input_db->getDatabase("Main"));
	dt = main_db->getDouble("dt");

     	const tbox::Dimension dim(static_cast<unsigned short>(2));

     // Get the restart manager and root restart database.  If run is from restart, open the restart file.
	is_from_restart = main_db->getBool("start_from_restart");
	int restart_interval = main_db->getInteger("restart_interval");
	string restart_write_dirname = main_db->getStringWithDefault("restart_dirname", ".");
	const bool write_restart = (restart_interval > 0) && !(restart_write_dirname.empty());
	init_from_files = main_db->getBool("init_from_files");

	tbox::RestartManager* restart_manager = tbox::RestartManager::getManager();

	if (is_from_restart) {
		restore_num = main_db->getInteger("restart_iteration");
		restart_manager->openRestartFile(restart_write_dirname, restore_num, mpi.getSize());
	}

	MainRestartData* main_restart_data = new MainRestartData("MainRestartData",main_db);
	regridding_interval = main_restart_data->getRegridStep();

	loop_time = main_restart_data->getLoopTime();
	int loop_cycle = main_restart_data->getIterationNumber();
	iteration_num = main_restart_data->getIterationNumber();

	//Setting the timers
	tbox::TimerManager::createManager(input_db->getDatabase("TimerManager"));
	std::shared_ptr<tbox::Timer> t_output(tbox::TimerManager::getManager()->getTimer("OutputGeneration"));
	std::shared_ptr<tbox::Timer> t_regrid(tbox::TimerManager::getManager()->getTimer("Regridding"));
	

	//Mesh creation
	std::shared_ptr<geom::CartesianGridGeometry > grid_geometry(new geom::CartesianGridGeometry(dim,"CartesianGeometry", input_db->getDatabase("CartesianGeometry")));
	std::shared_ptr<hier::PatchHierarchy > patch_hierarchy(new hier::PatchHierarchy("PatchHierarchy", grid_geometry, input_db->getDatabase("PatchHierarchy")));
	hier::BoxContainer* ba = new hier::BoxContainer();
    	std::string problem_name("Problem");
	std::shared_ptr<tbox::Database> problem_db(input_db->getDatabase("Problem"));
	problem = new Problem(problem_name, dim, problem_db,grid_geometry, patch_hierarchy, dt, *ba, init_from_files);
	std::shared_ptr<mesh::StandardTagAndInitialize > sti(new mesh::StandardTagAndInitialize("StandardTagAndInitialize", problem, input_db->getDatabase("StandardTagAndInitialize")));
	std::shared_ptr<mesh::BergerRigoutsos > box_generator(new mesh::BergerRigoutsos(dim));
        std::shared_ptr<mesh::TreeLoadBalancer> load_balancer(new mesh::TreeLoadBalancer(dim,"LoadBalancer", input_db->getDatabase("LoadBalancer")));
        load_balancer->setSAMRAI_MPI(tbox::SAMRAI_MPI::getSAMRAIWorld());
	std::shared_ptr< mesh::GriddingAlgorithm > gridding_algorithm(new mesh::GriddingAlgorithm(patch_hierarchy, "GriddingAlgorithm", input_db->getDatabase("GriddingAlgorithm"), sti, box_generator, load_balancer));
	if (!gridding_algorithm) return -1;

	//Setup the output
      	output_interval = 0;
      	if (main_db->keyExists("output_interval")) {
	 	output_interval = main_db->getInteger("output_interval");
      	}

	//Setup the hdf5 outputs
      	viz_dump_interval = 0;
      	if (main_db->keyExists("hfd5_dump_interval")) {
	 	viz_dump_interval = main_db->getInteger("hfd5_dump_interval");
      	}
      	int visit_number_procs_per_file = 1;
      	if ( viz_dump_interval > 0 ) {
	 	if (main_db->keyExists("hdf5_dump_dirname")) {
	    		viz_dump_dirname = main_db->getStringWithDefault("hdf5_dump_dirname", ".");
	 	}
		else {
			//The directory for the hdf5 output parameter does not exist 
			return -1;
		}
      	}

 	std::shared_ptr<appu::VisItDataWriter > visit_data_writer(new appu::VisItDataWriter(dim,"Problem VisIt Writer", viz_dump_dirname));

	//Prints the banner of the simulation

	cout << "|-----------------------SIMPLAT--------------------------|" << endl;
	cout << "    Simulation "  << endl;
	cout << "|--------------------------------------------------------|" << endl;
	if ( viz_dump_interval > 0 ) {
		cout << "|--------------------------------------------------------|" << endl;
		cout << "    HDF5 "  << endl;
		cout << "    Output directory:  " << viz_dump_dirname << endl;
		cout << "    Snapshot interval: " << viz_dump_interval << endl;
		cout << "|--------------------------------------------------------|" << endl;
	}
	//Initialization of the mesh
	std::vector<int> tag_buffer_array(patch_hierarchy->getMaxNumberOfLevels());
	for (int il = 0; il < patch_hierarchy->getMaxNumberOfLevels(); il++) {
		tag_buffer_array[il] = 0;
	}
    std::vector<double> regrid_start_time(patch_hierarchy->getMaxNumberOfLevels());

  	/*
   	 * Make the coarsest patch level where we will be solving.
  	 */
	if (tbox::RestartManager::getManager()->isFromRestart()) {
	 	patch_hierarchy->initializeHierarchy();
		gridding_algorithm->getTagAndInitializeStrategy()->resetHierarchyConfiguration(patch_hierarchy, 0, patch_hierarchy->getFinestLevelNumber()); 

		problem->getTimeSlices(main_restart_data);

	} else {
		gridding_algorithm->makeCoarsestLevel(loop_time);
		for (int lnum = 0; patch_hierarchy->levelCanBeRefined(lnum); lnum++) {
		 	gridding_algorithm->makeFinerLevel(tag_buffer_array[lnum], true ,loop_cycle, loop_time);
		}
		problem->putTimeSlices(main_restart_data);

	}

	//Print the initial data.
	std::shared_ptr< hier::PatchLevel > level(patch_hierarchy->getPatchLevel(0));
	level->recursivePrint(cout);
	problem->setupPlotter(*visit_data_writer);
      	if ( viz_dump_interval > 0  && !tbox::RestartManager::getManager()->isFromRestart() ) {
		visit_data_writer->writePlotData(patch_hierarchy, iteration_num, loop_time); 
	}

	//Close the restart file
	tbox::RestartManager::getManager()->closeRestartFile();

	//Simulation steps
	dt = problem->computeDt();
	while (!problem->checkFinalization(loop_time, dt)) {
		//Do a step
		iteration_num++;

		if ( output_interval > 0 ) {
			if ( (iteration_num % output_interval) == 0 ) {
				cout << "|--------------------------------------------------------|" << endl;
				cout << "    At begining of timestep # " << iteration_num - 1 << endl;
				cout << "    Simulation time is " << loop_time << endl;
				cout << "|--------------------------------------------------------|" << endl;
			}
		}

		loop_time += dt;
		problem->step(dt, loop_time, iteration_num - 1);
		dt = problem->computeDt();

		main_restart_data->setLoopTime(loop_time);
		main_restart_data->setIterationNumber(iteration_num);

		//Regrid if necessary at specified intervals.
		if ( main_restart_data->getRegridStep() > 0 ) {
			if ( (iteration_num % main_restart_data->getRegridStep()) == 0 ) {
				t_regrid->start();
				gridding_algorithm->regridAllFinerLevels(0, tag_buffer_array, iteration_num, loop_time, regrid_start_time);
				t_regrid->stop();
			}
		}

		if ( output_interval > 0 ) {
			if ( (iteration_num % output_interval) == 0 ) {
				cout << "|--------------------------------------------------------|" << endl;
				cout << "    At end of timestep # " << iteration_num - 1 << endl;
				cout << "    Simulation time is " << loop_time << endl;
				cout << "|--------------------------------------------------------|" << endl;
				//Print timers
				tbox::TimerManager::getManager()->print(cout);
			}
		}

		 /*
		  * At specified intervals, write out data files for plotting.
		  * The viz_data_writer dumps data in a format that can
		  * be processed by the Vizamrai tool. 
		  */
		if ( viz_dump_interval > 0 ) {
			if ( (iteration_num % viz_dump_interval) == 0 ) {
				t_output->start();
				visit_data_writer->writePlotData(patch_hierarchy, iteration_num, loop_time);
				t_output->stop();
			}
		}

		//Output restart data
		if ( write_restart && (0 == iteration_num % restart_interval) ) {
   			tbox::RestartManager::getManager()->writeRestartFile(restart_write_dirname, iteration_num);
 		}
	}

	//Print the end output
	cout << "    Simulation finished:" << endl;
	cout << "           Iterations: " << iteration_num << endl;
	cout << "           Time: " << loop_time  << endl;	
	cout << "|--------------------------------------------------------|" << endl;

	tbox::TimerManager::getManager()->print(cout);

	//Close all the objects
	visit_data_writer.reset();
     	gridding_algorithm.reset();
	load_balancer.reset();
	box_generator.reset(); 
	sti.reset();
	if (problem) delete problem;
	if (main_restart_data) delete main_restart_data;

	patch_hierarchy.reset();
	grid_geometry.reset();
	input_db.reset();
	main_db.reset();
   	tbox::SAMRAIManager::shutdown();
   	tbox::SAMRAIManager::finalize();
   	tbox::SAMRAI_MPI::finalize();
	return 0;
}
