#include "boost/shared_ptr.hpp"
#include "Functions.h"
#include <string>
using namespace std;

#define MIN(X,Y) ((X) < (Y) ? (X) : (Y))
#define MAX(X,Y) ((X) > (Y) ? (X) : (Y))
#define SIGN(X) (((X) > 0) - ((X) < 0))
#define vector(v, i, j) (v)[i+ilast*(j)]

