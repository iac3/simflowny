#include "MainRestartData.h"
#include "SAMRAI/tbox/PIO.h"
#include "SAMRAI/tbox/RestartManager.h"
#include "SAMRAI/tbox/Utilities.h"

/*
*************************************************************************
*                                                                       *
* Constructor                                                           *
*                                                                       *
*************************************************************************
*/

MainRestartData::MainRestartData(const string& object_name,std::shared_ptr<tbox::Database>& input_db):d_object_name(object_name)
{
	TBOX_ASSERT(input_db);

	tbox::RestartManager::getManager()->registerRestartItem(d_object_name, this);

	/*
	* Initialize object with data read from given input/restart databases.
	*/
	bool is_from_restart = tbox::RestartManager::getManager()->isFromRestart();
	if (is_from_restart){
		getFromRestart();
	}
	getFromInput(input_db,is_from_restart);

	/* if not starting from restart file, set loop_time and iteration_number */
	if (!is_from_restart) {
		d_loop_time = d_start_time;
		d_iteration_number = 0;
	}
}

/*
*************************************************************************
*                                                                       *
* Destructor                                                            *
*                                                                       *
*************************************************************************
*/

MainRestartData::~MainRestartData()
{
}

/*
*************************************************************************
*                                                                       *
* Accessor methods for data                                             *
*                                                                       *
*************************************************************************
*/

double MainRestartData::getStartTime()
{
	return d_start_time;
}

int MainRestartData::getRegridStep()
{
	return d_regrid_step;
}

double MainRestartData::getLoopTime()
{
	return d_loop_time;
}

void MainRestartData::setLoopTime(const double loop_time)
{
	d_loop_time = loop_time;
}

int MainRestartData::getIterationNumber()
{
	return d_iteration_number;
}

void MainRestartData::setIterationNumber(const int iter_num)
{
	d_iteration_number = iter_num;
}

	void MainRestartData::addWaterCellPoints(double* x3d) {
		for (int ts = 0; ts < 2; ts++) {
			for (int point = 0; point < 25; point++) {
				for (int coord = 0; coord < 2; coord++) {
					WaterCellPoints[ts * (25 * 2) + point * 2 + coord] = x3d[ts * (25 * 2) + point * 2 + coord];
				}
			}
		}
	}

		double* MainRestartData::getWaterCellPoints() {
			return WaterCellPoints;
		}


/*
*************************************************************************
*                                                                       *
* tbox::Database input/output methods                                         *
*                                                                       *
*************************************************************************
*/
void MainRestartData::putToRestart(const std::shared_ptr<tbox::Database>& db) const
{
	TBOX_ASSERT(db);

	db->putDouble("d_start_time",d_start_time);
	db->putInteger("regridding_interval",d_regrid_step);
	db->putDouble("d_loop_time",d_loop_time);
	db->putInteger("d_iteration_number",d_iteration_number);
	db->putDoubleArray("WaterCellPoints", WaterCellPoints, 2 * 25 * 2);

}

void MainRestartData::getFromInput( std::shared_ptr<tbox::Database>& input_db, bool is_from_restart)
{
   TBOX_ASSERT(input_db);

	d_start_time = 0.0;

	if (input_db->keyExists("regridding_interval")) {
		d_regrid_step = input_db->getInteger("regridding_interval");
	} else {
		d_regrid_step = 0;
	}

}

void MainRestartData::getFromRestart()
{
	std::shared_ptr<tbox::Database> root_db(tbox::RestartManager::getManager()->getRootDatabase());

   	if (!root_db->isDatabase(d_object_name)) {
      		TBOX_ERROR("Restart database corresponding to " << d_object_name << " not found in the restart file.");
   	}
   	std::shared_ptr<tbox::Database> restart_db(root_db->getDatabase(d_object_name));

	d_start_time = restart_db->getDouble("d_start_time");
	d_regrid_step = restart_db->getInteger("regridding_interval");
	d_loop_time = restart_db->getDouble("d_loop_time");
	d_iteration_number = restart_db->getInteger("d_iteration_number");
	restart_db->getDoubleArray("WaterCellPoints", WaterCellPoints, 2 * 25 * 2);

}
