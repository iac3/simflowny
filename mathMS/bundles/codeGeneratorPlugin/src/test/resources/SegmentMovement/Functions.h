#include "boost/shared_ptr.hpp"
#include <string>
using namespace std;

#define MIN(X,Y) ((X) < (Y) ? (X) : (Y))
#define MAX(X,Y) ((X) > (Y) ? (X) : (Y))
#define SIGN(X) (((X) > 0) - ((X) < 0))
#define vector(v, i, j) (v)[i+ilast*(j)]

inline void extrapolate_field(double* pard_i_f, double* parfrom_i_f, double* parto_i_f, int pari, double* pard_j_f, double* parfrom_j_f, double* parto_j_f, int parj, double* parto_f, int* parFOV, int* parStalled, const int ilast, const int jlast) {
	double sign_i_ext;
	double sign_j_ext;
	double Ex_ext;
	double Ey_ext;
	double Exy_ext;
	double Ex2_ext;
	double Ew_ext;
	double Ex3_ext;
	double Ex4_ext;
	double Ex2y_ext;
	double Sxx_ext;
	double Sx2x_ext;
	double Sx2x2_ext;
	double Sxy_ext;
	double Sx2y_ext;
	double a2_ext;
	double a1_ext;
	double a0_ext;

	sign_i_ext = SIGN(vector(pard_i_f, pari, parj));
	sign_j_ext = SIGN(vector(pard_j_f, pari, parj));
	if (vector(pard_i_f, pari, parj) != 0.0) {
		Ex_ext = vector(parFOV, int(pari - (vector(pard_i_f, pari, parj) + 2 * sign_i_ext)), parj) * (pari - (vector(pard_i_f, pari, parj) + 2.0 * sign_i_ext)) + (vector(parFOV, int(pari - (vector(pard_i_f, pari, parj) + 1 * sign_i_ext)), parj) * (pari - (vector(pard_i_f, pari, parj) + 1.0 * sign_i_ext)) + vector(parFOV, int(pari - vector(pard_i_f, pari, parj)), parj) * (pari - (vector(pard_i_f, pari, parj) + 0.0 * sign_i_ext)));
		Ey_ext = vector(parFOV, int(pari - (vector(pard_i_f, pari, parj) + 2 * sign_i_ext)), parj) * vector(parfrom_i_f, int(pari - (vector(pard_i_f, pari, parj) + 2 * sign_i_ext)), parj) + (vector(parFOV, int(pari - (vector(pard_i_f, pari, parj) + 1 * sign_i_ext)), parj) * vector(parfrom_i_f, int(pari - (vector(pard_i_f, pari, parj) + 1 * sign_i_ext)), parj) + vector(parFOV, int(pari - vector(pard_i_f, pari, parj)), parj) * vector(parfrom_i_f, int(pari - vector(pard_i_f, pari, parj)), parj));
		Exy_ext = vector(parFOV, int(pari - (vector(pard_i_f, pari, parj) + 2 * sign_i_ext)), parj) * (pari - (vector(pard_i_f, pari, parj) + 2.0 * sign_i_ext)) * vector(parfrom_i_f, int(pari - (vector(pard_i_f, pari, parj) + 2 * sign_i_ext)), parj) + (vector(parFOV, int(pari - (vector(pard_i_f, pari, parj) + 1 * sign_i_ext)), parj) * (pari - (vector(pard_i_f, pari, parj) + 1.0 * sign_i_ext)) * vector(parfrom_i_f, int(pari - (vector(pard_i_f, pari, parj) + 1 * sign_i_ext)), parj) + vector(parFOV, int(pari - vector(pard_i_f, pari, parj)), parj) * (pari - (vector(pard_i_f, pari, parj) + 0.0 * sign_i_ext)) * vector(parfrom_i_f, int(pari - vector(pard_i_f, pari, parj)), parj));
		Ex2_ext = vector(parFOV, int(pari - (vector(pard_i_f, pari, parj) + 2 * sign_i_ext)), parj) * ((pari - (vector(pard_i_f, pari, parj) + 2.0 * sign_i_ext))*(pari - (vector(pard_i_f, pari, parj) + 2.0 * sign_i_ext))) + (vector(parFOV, int(pari - (vector(pard_i_f, pari, parj) + 1 * sign_i_ext)), parj) * ((pari - (vector(pard_i_f, pari, parj) + 1.0 * sign_i_ext))*(pari - (vector(pard_i_f, pari, parj) + 1.0 * sign_i_ext))) + vector(parFOV, int(pari - vector(pard_i_f, pari, parj)), parj) * ((pari - (vector(pard_i_f, pari, parj) + 0.0 * sign_i_ext))*(pari - (vector(pard_i_f, pari, parj) + 0.0 * sign_i_ext))));
		Ew_ext = vector(parFOV, int(pari - (vector(pard_i_f, pari, parj) + 2 * sign_i_ext)), parj) + (vector(parFOV, int(pari - (vector(pard_i_f, pari, parj) + 1 * sign_i_ext)), parj) + vector(parFOV, int(pari - vector(pard_i_f, pari, parj)), parj));
		Ex3_ext = vector(parFOV, int(pari - (vector(pard_i_f, pari, parj) + 2 * sign_i_ext)), parj) * ((pari - (vector(pard_i_f, pari, parj) + 2.0 * sign_i_ext))*(pari - (vector(pard_i_f, pari, parj) + 2.0 * sign_i_ext))*(pari - (vector(pard_i_f, pari, parj) + 2.0 * sign_i_ext))) + (vector(parFOV, int(pari - (vector(pard_i_f, pari, parj) + 1 * sign_i_ext)), parj) * ((pari - (vector(pard_i_f, pari, parj) + 1.0 * sign_i_ext))*(pari - (vector(pard_i_f, pari, parj) + 1.0 * sign_i_ext))*(pari - (vector(pard_i_f, pari, parj) + 1.0 * sign_i_ext))) + vector(parFOV, int(pari - vector(pard_i_f, pari, parj)), parj) * ((pari - (vector(pard_i_f, pari, parj) + 0.0 * sign_i_ext))*(pari - (vector(pard_i_f, pari, parj) + 0.0 * sign_i_ext))*(pari - (vector(pard_i_f, pari, parj) + 0.0 * sign_i_ext))));
		Ex4_ext = vector(parFOV, int(pari - (vector(pard_i_f, pari, parj) + 2 * sign_i_ext)), parj) * ((pari - (vector(pard_i_f, pari, parj) + 2.0 * sign_i_ext))*(pari - (vector(pard_i_f, pari, parj) + 2.0 * sign_i_ext))*(pari - (vector(pard_i_f, pari, parj) + 2.0 * sign_i_ext))*(pari - (vector(pard_i_f, pari, parj) + 2.0 * sign_i_ext))) + (vector(parFOV, int(pari - (vector(pard_i_f, pari, parj) + 1 * sign_i_ext)), parj) * ((pari - (vector(pard_i_f, pari, parj) + 1.0 * sign_i_ext))*(pari - (vector(pard_i_f, pari, parj) + 1.0 * sign_i_ext))*(pari - (vector(pard_i_f, pari, parj) + 1.0 * sign_i_ext))*(pari - (vector(pard_i_f, pari, parj) + 1.0 * sign_i_ext))) + vector(parFOV, int(pari - vector(pard_i_f, pari, parj)), parj) * ((pari - (vector(pard_i_f, pari, parj) + 0.0 * sign_i_ext))*(pari - (vector(pard_i_f, pari, parj) + 0.0 * sign_i_ext))*(pari - (vector(pard_i_f, pari, parj) + 0.0 * sign_i_ext))*(pari - (vector(pard_i_f, pari, parj) + 0.0 * sign_i_ext))));
		Ex2y_ext = vector(parFOV, int(pari - (vector(pard_i_f, pari, parj) + 2 * sign_i_ext)), parj) * ((pari - (vector(pard_i_f, pari, parj) + 2.0 * sign_i_ext))*(pari - (vector(pard_i_f, pari, parj) + 2.0 * sign_i_ext))) * vector(parfrom_i_f, int(pari - (vector(pard_i_f, pari, parj) + 2 * sign_i_ext)), parj) + (vector(parFOV, int(pari - (vector(pard_i_f, pari, parj) + 1 * sign_i_ext)), parj) * ((pari - (vector(pard_i_f, pari, parj) + 1.0 * sign_i_ext))*(pari - (vector(pard_i_f, pari, parj) + 1.0 * sign_i_ext))) * vector(parfrom_i_f, int(pari - (vector(pard_i_f, pari, parj) + 1 * sign_i_ext)), parj) + vector(parFOV, int(pari - vector(pard_i_f, pari, parj)), parj) * ((pari - (vector(pard_i_f, pari, parj) + 0.0 * sign_i_ext))*(pari - (vector(pard_i_f, pari, parj) + 0.0 * sign_i_ext))) * vector(parfrom_i_f, int(pari - vector(pard_i_f, pari, parj)), parj));
		Sxx_ext = Ex2_ext - (Ex_ext*Ex_ext) / Ew_ext;
		Sx2x_ext = Ex3_ext - (Ex_ext * Ex2_ext) / Ew_ext;
		Sx2x2_ext = Ex4_ext - (Ex2_ext*Ex2_ext) / Ew_ext;
		Sxy_ext = Exy_ext - (Ey_ext * Ex_ext) / Ew_ext;
		Sx2y_ext = Ex2y_ext - (Ey_ext * Ex2_ext) / Ew_ext;
		a2_ext = (Sx2y_ext * Sxx_ext - Sxy_ext * Sx2x_ext) / (Sx2x2_ext * Sxx_ext - Sx2x_ext*Sx2x_ext);
		a1_ext = (Sxy_ext - a2_ext * Sx2x_ext) / Sxx_ext;
		a0_ext = (Ey_ext - (a1_ext * Ex_ext + a2_ext * Ex2_ext)) / Ew_ext;
		if (vector(parStalled, pari, parj)) {
			vector(parto_i_f, pari, parj) = a0_ext + (a1_ext * pari + a2_ext * (pari*pari));
		} else {
			vector(parto_i_f, pari, parj) = vector(parFOV, pari, parj) / 100.0 * vector(parto_i_f, pari, parj) + (100.0 - vector(parFOV, pari, parj)) / 100.0 * (a0_ext + (a1_ext * pari + a2_ext * (pari*pari)));
		}
	}
	if (vector(pard_j_f, pari, parj) != 0.0) {
		Ex_ext = vector(parFOV, pari, int(parj - (vector(pard_j_f, pari, parj) + 2 * sign_j_ext))) * (parj - (vector(pard_j_f, pari, parj) + 2.0 * sign_j_ext)) + (vector(parFOV, pari, int(parj - (vector(pard_j_f, pari, parj) + 1 * sign_j_ext))) * (parj - (vector(pard_j_f, pari, parj) + 1.0 * sign_j_ext)) + vector(parFOV, pari, int(parj - vector(pard_j_f, pari, parj))) * (parj - (vector(pard_j_f, pari, parj) + 0.0 * sign_j_ext)));
		Ey_ext = vector(parFOV, pari, int(parj - (vector(pard_j_f, pari, parj) + 2 * sign_j_ext))) * vector(parfrom_j_f, pari, int(parj - (vector(pard_j_f, pari, parj) + 2 * sign_j_ext))) + (vector(parFOV, pari, int(parj - (vector(pard_j_f, pari, parj) + 1 * sign_j_ext))) * vector(parfrom_j_f, pari, int(parj - (vector(pard_j_f, pari, parj) + 1 * sign_j_ext))) + vector(parFOV, pari, int(parj - vector(pard_j_f, pari, parj))) * vector(parfrom_j_f, pari, int(parj - vector(pard_j_f, pari, parj))));
		Exy_ext = vector(parFOV, pari, int(parj - (vector(pard_j_f, pari, parj) + 2 * sign_j_ext))) * (parj - (vector(pard_j_f, pari, parj) + 2.0 * sign_j_ext)) * vector(parfrom_j_f, pari, int(parj - (vector(pard_j_f, pari, parj) + 2 * sign_j_ext))) + (vector(parFOV, pari, int(parj - (vector(pard_j_f, pari, parj) + 1 * sign_j_ext))) * (parj - (vector(pard_j_f, pari, parj) + 1.0 * sign_j_ext)) * vector(parfrom_j_f, pari, int(parj - (vector(pard_j_f, pari, parj) + 1 * sign_j_ext))) + vector(parFOV, pari, int(parj - vector(pard_j_f, pari, parj))) * (parj - (vector(pard_j_f, pari, parj) + 0.0 * sign_j_ext)) * vector(parfrom_j_f, pari, int(parj - vector(pard_j_f, pari, parj))));
		Ex2_ext = vector(parFOV, pari, int(parj - (vector(pard_j_f, pari, parj) + 2 * sign_j_ext))) * ((parj - (vector(pard_j_f, pari, parj) + 2.0 * sign_j_ext))*(parj - (vector(pard_j_f, pari, parj) + 2.0 * sign_j_ext))) + (vector(parFOV, pari, int(parj - (vector(pard_j_f, pari, parj) + 1 * sign_j_ext))) * ((parj - (vector(pard_j_f, pari, parj) + 1.0 * sign_j_ext))*(parj - (vector(pard_j_f, pari, parj) + 1.0 * sign_j_ext))) + vector(parFOV, pari, int(parj - vector(pard_j_f, pari, parj))) * ((parj - (vector(pard_j_f, pari, parj) + 0.0 * sign_j_ext))*(parj - (vector(pard_j_f, pari, parj) + 0.0 * sign_j_ext))));
		Ew_ext = vector(parFOV, pari, int(parj - (vector(pard_j_f, pari, parj) + 2 * sign_j_ext))) + (vector(parFOV, pari, int(parj - (vector(pard_j_f, pari, parj) + 1 * sign_j_ext))) + vector(parFOV, pari, int(parj - vector(pard_j_f, pari, parj))));
		Ex3_ext = vector(parFOV, pari, int(parj - (vector(pard_j_f, pari, parj) + 2 * sign_j_ext))) * ((parj - (vector(pard_j_f, pari, parj) + 2.0 * sign_j_ext))*(parj - (vector(pard_j_f, pari, parj) + 2.0 * sign_j_ext))*(parj - (vector(pard_j_f, pari, parj) + 2.0 * sign_j_ext))) + (vector(parFOV, pari, int(parj - (vector(pard_j_f, pari, parj) + 1 * sign_j_ext))) * ((parj - (vector(pard_j_f, pari, parj) + 1.0 * sign_j_ext))*(parj - (vector(pard_j_f, pari, parj) + 1.0 * sign_j_ext))*(parj - (vector(pard_j_f, pari, parj) + 1.0 * sign_j_ext))) + vector(parFOV, pari, int(parj - vector(pard_j_f, pari, parj))) * ((parj - (vector(pard_j_f, pari, parj) + 0.0 * sign_j_ext))*(parj - (vector(pard_j_f, pari, parj) + 0.0 * sign_j_ext))*(parj - (vector(pard_j_f, pari, parj) + 0.0 * sign_j_ext))));
		Ex4_ext = vector(parFOV, pari, int(parj - (vector(pard_j_f, pari, parj) + 2 * sign_j_ext))) * ((parj - (vector(pard_j_f, pari, parj) + 2.0 * sign_j_ext))*(parj - (vector(pard_j_f, pari, parj) + 2.0 * sign_j_ext))*(parj - (vector(pard_j_f, pari, parj) + 2.0 * sign_j_ext))*(parj - (vector(pard_j_f, pari, parj) + 2.0 * sign_j_ext))) + (vector(parFOV, pari, int(parj - (vector(pard_j_f, pari, parj) + 1 * sign_j_ext))) * ((parj - (vector(pard_j_f, pari, parj) + 1.0 * sign_j_ext))*(parj - (vector(pard_j_f, pari, parj) + 1.0 * sign_j_ext))*(parj - (vector(pard_j_f, pari, parj) + 1.0 * sign_j_ext))*(parj - (vector(pard_j_f, pari, parj) + 1.0 * sign_j_ext))) + vector(parFOV, pari, int(parj - vector(pard_j_f, pari, parj))) * ((parj - (vector(pard_j_f, pari, parj) + 0.0 * sign_j_ext))*(parj - (vector(pard_j_f, pari, parj) + 0.0 * sign_j_ext))*(parj - (vector(pard_j_f, pari, parj) + 0.0 * sign_j_ext))*(parj - (vector(pard_j_f, pari, parj) + 0.0 * sign_j_ext))));
		Ex2y_ext = vector(parFOV, pari, int(parj - (vector(pard_j_f, pari, parj) + 2 * sign_j_ext))) * ((parj - (vector(pard_j_f, pari, parj) + 2.0 * sign_j_ext))*(parj - (vector(pard_j_f, pari, parj) + 2.0 * sign_j_ext))) * vector(parfrom_j_f, pari, int(parj - (vector(pard_j_f, pari, parj) + 2 * sign_j_ext))) + (vector(parFOV, pari, int(parj - (vector(pard_j_f, pari, parj) + 1 * sign_j_ext))) * ((parj - (vector(pard_j_f, pari, parj) + 1.0 * sign_j_ext))*(parj - (vector(pard_j_f, pari, parj) + 1.0 * sign_j_ext))) * vector(parfrom_j_f, pari, int(parj - (vector(pard_j_f, pari, parj) + 1 * sign_j_ext))) + vector(parFOV, pari, int(parj - vector(pard_j_f, pari, parj))) * ((parj - (vector(pard_j_f, pari, parj) + 0.0 * sign_j_ext))*(parj - (vector(pard_j_f, pari, parj) + 0.0 * sign_j_ext))) * vector(parfrom_j_f, pari, int(parj - vector(pard_j_f, pari, parj))));
		Sxx_ext = Ex2_ext - (Ex_ext*Ex_ext) / Ew_ext;
		Sx2x_ext = Ex3_ext - (Ex_ext * Ex2_ext) / Ew_ext;
		Sx2x2_ext = Ex4_ext - (Ex2_ext*Ex2_ext) / Ew_ext;
		Sxy_ext = Exy_ext - (Ey_ext * Ex_ext) / Ew_ext;
		Sx2y_ext = Ex2y_ext - (Ey_ext * Ex2_ext) / Ew_ext;
		a2_ext = (Sx2y_ext * Sxx_ext - Sxy_ext * Sx2x_ext) / (Sx2x2_ext * Sxx_ext - Sx2x_ext*Sx2x_ext);
		a1_ext = (Sxy_ext - a2_ext * Sx2x_ext) / Sxx_ext;
		a0_ext = (Ey_ext - (a1_ext * Ex_ext + a2_ext * Ex2_ext)) / Ew_ext;
		if (vector(parStalled, pari, parj)) {
			vector(parto_j_f, pari, parj) = a0_ext + (a1_ext * parj + a2_ext * (parj*parj));
		} else {
			vector(parto_j_f, pari, parj) = vector(parFOV, pari, parj) / 100.0 * vector(parto_j_f, pari, parj) + (100.0 - vector(parFOV, pari, parj)) / 100.0 * (a0_ext + (a1_ext * parj + a2_ext * (parj*parj)));
		}
	}
	if ((vector(pard_i_f, pari, parj) != 0.0) && (vector(pard_j_f, pari, parj) != 0.0)) {
		vector(parto_f, pari, parj) = (vector(parto_i_f, pari, parj) + vector(parto_j_f, pari, parj)) / 2.0;
	} else {
		if ((vector(pard_i_f, pari, parj) != 0.0) && (vector(pard_j_f, pari, parj) == 0.0)) {
			vector(parto_f, pari, parj) = vector(parto_i_f, pari, parj);
			vector(parto_j_f, pari, parj) = vector(parto_f, pari, parj);
		} else {
			if ((vector(pard_i_f, pari, parj) == 0.0) && (vector(pard_j_f, pari, parj) != 0.0)) {
				vector(parto_f, pari, parj) = vector(parto_j_f, pari, parj);
				vector(parto_i_f, pari, parj) = vector(parto_f, pari, parj);
			}
		}
	}

};

inline void extrapolate_flux(double* pard_i_f, double* parfrom_i_f, double* parto_i_f, int pari, double* pard_j_f, double* parfrom_j_f, double* parto_j_f, int parj, int* parFOV, int* parStalled, const int ilast, const int jlast) {
	double sign_i_ext;
	double sign_j_ext;
	double Ex_ext;
	double Ey_ext;
	double Exy_ext;
	double Ex2_ext;
	double Ew_ext;
	double Ex3_ext;
	double Ex4_ext;
	double Ex2y_ext;
	double Sxx_ext;
	double Sx2x_ext;
	double Sx2x2_ext;
	double Sxy_ext;
	double Sx2y_ext;
	double a2_ext;
	double a1_ext;
	double a0_ext;

	sign_i_ext = SIGN(vector(pard_i_f, pari, parj));
	sign_j_ext = SIGN(vector(pard_j_f, pari, parj));
	if (vector(pard_i_f, pari, parj) != 0.0) {
		Ex_ext = vector(parFOV, int(pari - (vector(pard_i_f, pari, parj) + 2 * sign_i_ext)), parj) * (pari - (vector(pard_i_f, pari, parj) + 2.0 * sign_i_ext)) + (vector(parFOV, int(pari - (vector(pard_i_f, pari, parj) + 1 * sign_i_ext)), parj) * (pari - (vector(pard_i_f, pari, parj) + 1.0 * sign_i_ext)) + vector(parFOV, int(pari - vector(pard_i_f, pari, parj)), parj) * (pari - (vector(pard_i_f, pari, parj) + 0.0 * sign_i_ext)));
		Ey_ext = vector(parFOV, int(pari - (vector(pard_i_f, pari, parj) + 2 * sign_i_ext)), parj) * vector(parfrom_i_f, int(pari - (vector(pard_i_f, pari, parj) + 2 * sign_i_ext)), parj) + (vector(parFOV, int(pari - (vector(pard_i_f, pari, parj) + 1 * sign_i_ext)), parj) * vector(parfrom_i_f, int(pari - (vector(pard_i_f, pari, parj) + 1 * sign_i_ext)), parj) + vector(parFOV, int(pari - vector(pard_i_f, pari, parj)), parj) * vector(parfrom_i_f, int(pari - vector(pard_i_f, pari, parj)), parj));
		Exy_ext = vector(parFOV, int(pari - (vector(pard_i_f, pari, parj) + 2 * sign_i_ext)), parj) * (pari - (vector(pard_i_f, pari, parj) + 2.0 * sign_i_ext)) * vector(parfrom_i_f, int(pari - (vector(pard_i_f, pari, parj) + 2 * sign_i_ext)), parj) + (vector(parFOV, int(pari - (vector(pard_i_f, pari, parj) + 1 * sign_i_ext)), parj) * (pari - (vector(pard_i_f, pari, parj) + 1.0 * sign_i_ext)) * vector(parfrom_i_f, int(pari - (vector(pard_i_f, pari, parj) + 1 * sign_i_ext)), parj) + vector(parFOV, int(pari - vector(pard_i_f, pari, parj)), parj) * (pari - (vector(pard_i_f, pari, parj) + 0.0 * sign_i_ext)) * vector(parfrom_i_f, int(pari - vector(pard_i_f, pari, parj)), parj));
		Ex2_ext = vector(parFOV, int(pari - (vector(pard_i_f, pari, parj) + 2 * sign_i_ext)), parj) * ((pari - (vector(pard_i_f, pari, parj) + 2.0 * sign_i_ext))*(pari - (vector(pard_i_f, pari, parj) + 2.0 * sign_i_ext))) + (vector(parFOV, int(pari - (vector(pard_i_f, pari, parj) + 1 * sign_i_ext)), parj) * ((pari - (vector(pard_i_f, pari, parj) + 1.0 * sign_i_ext))*(pari - (vector(pard_i_f, pari, parj) + 1.0 * sign_i_ext))) + vector(parFOV, int(pari - vector(pard_i_f, pari, parj)), parj) * ((pari - (vector(pard_i_f, pari, parj) + 0.0 * sign_i_ext))*(pari - (vector(pard_i_f, pari, parj) + 0.0 * sign_i_ext))));
		Ew_ext = vector(parFOV, int(pari - (vector(pard_i_f, pari, parj) + 2 * sign_i_ext)), parj) + (vector(parFOV, int(pari - (vector(pard_i_f, pari, parj) + 1 * sign_i_ext)), parj) + vector(parFOV, int(pari - vector(pard_i_f, pari, parj)), parj));
		Ex3_ext = vector(parFOV, int(pari - (vector(pard_i_f, pari, parj) + 2 * sign_i_ext)), parj) * ((pari - (vector(pard_i_f, pari, parj) + 2.0 * sign_i_ext))*(pari - (vector(pard_i_f, pari, parj) + 2.0 * sign_i_ext))*(pari - (vector(pard_i_f, pari, parj) + 2.0 * sign_i_ext))) + (vector(parFOV, int(pari - (vector(pard_i_f, pari, parj) + 1 * sign_i_ext)), parj) * ((pari - (vector(pard_i_f, pari, parj) + 1.0 * sign_i_ext))*(pari - (vector(pard_i_f, pari, parj) + 1.0 * sign_i_ext))*(pari - (vector(pard_i_f, pari, parj) + 1.0 * sign_i_ext))) + vector(parFOV, int(pari - vector(pard_i_f, pari, parj)), parj) * ((pari - (vector(pard_i_f, pari, parj) + 0.0 * sign_i_ext))*(pari - (vector(pard_i_f, pari, parj) + 0.0 * sign_i_ext))*(pari - (vector(pard_i_f, pari, parj) + 0.0 * sign_i_ext))));
		Ex4_ext = vector(parFOV, int(pari - (vector(pard_i_f, pari, parj) + 2 * sign_i_ext)), parj) * ((pari - (vector(pard_i_f, pari, parj) + 2.0 * sign_i_ext))*(pari - (vector(pard_i_f, pari, parj) + 2.0 * sign_i_ext))*(pari - (vector(pard_i_f, pari, parj) + 2.0 * sign_i_ext))*(pari - (vector(pard_i_f, pari, parj) + 2.0 * sign_i_ext))) + (vector(parFOV, int(pari - (vector(pard_i_f, pari, parj) + 1 * sign_i_ext)), parj) * ((pari - (vector(pard_i_f, pari, parj) + 1.0 * sign_i_ext))*(pari - (vector(pard_i_f, pari, parj) + 1.0 * sign_i_ext))*(pari - (vector(pard_i_f, pari, parj) + 1.0 * sign_i_ext))*(pari - (vector(pard_i_f, pari, parj) + 1.0 * sign_i_ext))) + vector(parFOV, int(pari - vector(pard_i_f, pari, parj)), parj) * ((pari - (vector(pard_i_f, pari, parj) + 0.0 * sign_i_ext))*(pari - (vector(pard_i_f, pari, parj) + 0.0 * sign_i_ext))*(pari - (vector(pard_i_f, pari, parj) + 0.0 * sign_i_ext))*(pari - (vector(pard_i_f, pari, parj) + 0.0 * sign_i_ext))));
		Ex2y_ext = vector(parFOV, int(pari - (vector(pard_i_f, pari, parj) + 2 * sign_i_ext)), parj) * ((pari - (vector(pard_i_f, pari, parj) + 2.0 * sign_i_ext))*(pari - (vector(pard_i_f, pari, parj) + 2.0 * sign_i_ext))) * vector(parfrom_i_f, int(pari - (vector(pard_i_f, pari, parj) + 2 * sign_i_ext)), parj) + (vector(parFOV, int(pari - (vector(pard_i_f, pari, parj) + 1 * sign_i_ext)), parj) * ((pari - (vector(pard_i_f, pari, parj) + 1.0 * sign_i_ext))*(pari - (vector(pard_i_f, pari, parj) + 1.0 * sign_i_ext))) * vector(parfrom_i_f, int(pari - (vector(pard_i_f, pari, parj) + 1 * sign_i_ext)), parj) + vector(parFOV, int(pari - vector(pard_i_f, pari, parj)), parj) * ((pari - (vector(pard_i_f, pari, parj) + 0.0 * sign_i_ext))*(pari - (vector(pard_i_f, pari, parj) + 0.0 * sign_i_ext))) * vector(parfrom_i_f, int(pari - vector(pard_i_f, pari, parj)), parj));
		Sxx_ext = Ex2_ext - (Ex_ext*Ex_ext) / Ew_ext;
		Sx2x_ext = Ex3_ext - (Ex_ext * Ex2_ext) / Ew_ext;
		Sx2x2_ext = Ex4_ext - (Ex2_ext*Ex2_ext) / Ew_ext;
		Sxy_ext = Exy_ext - (Ey_ext * Ex_ext) / Ew_ext;
		Sx2y_ext = Ex2y_ext - (Ey_ext * Ex2_ext) / Ew_ext;
		a2_ext = (Sx2y_ext * Sxx_ext - Sxy_ext * Sx2x_ext) / (Sx2x2_ext * Sxx_ext - Sx2x_ext*Sx2x_ext);
		a1_ext = (Sxy_ext - a2_ext * Sx2x_ext) / Sxx_ext;
		a0_ext = (Ey_ext - (a1_ext * Ex_ext + a2_ext * Ex2_ext)) / Ew_ext;
		if (vector(parStalled, pari, parj)) {
			vector(parto_i_f, pari, parj) = a0_ext + (a1_ext * pari + a2_ext * (pari*pari));
		} else {
			vector(parto_i_f, pari, parj) = vector(parFOV, pari, parj) / 100.0 * vector(parto_i_f, pari, parj) + (100.0 - vector(parFOV, pari, parj)) / 100.0 * (a0_ext + (a1_ext * pari + a2_ext * (pari*pari)));
		}
	}
	if (vector(pard_j_f, pari, parj) != 0.0) {
		Ex_ext = vector(parFOV, pari, int(parj - (vector(pard_j_f, pari, parj) + 2 * sign_j_ext))) * (parj - (vector(pard_j_f, pari, parj) + 2.0 * sign_j_ext)) + (vector(parFOV, pari, int(parj - (vector(pard_j_f, pari, parj) + 1 * sign_j_ext))) * (parj - (vector(pard_j_f, pari, parj) + 1.0 * sign_j_ext)) + vector(parFOV, pari, int(parj - vector(pard_j_f, pari, parj))) * (parj - (vector(pard_j_f, pari, parj) + 0.0 * sign_j_ext)));
		Ey_ext = vector(parFOV, pari, int(parj - (vector(pard_j_f, pari, parj) + 2 * sign_j_ext))) * vector(parfrom_j_f, pari, int(parj - (vector(pard_j_f, pari, parj) + 2 * sign_j_ext))) + (vector(parFOV, pari, int(parj - (vector(pard_j_f, pari, parj) + 1 * sign_j_ext))) * vector(parfrom_j_f, pari, int(parj - (vector(pard_j_f, pari, parj) + 1 * sign_j_ext))) + vector(parFOV, pari, int(parj - vector(pard_j_f, pari, parj))) * vector(parfrom_j_f, pari, int(parj - vector(pard_j_f, pari, parj))));
		Exy_ext = vector(parFOV, pari, int(parj - (vector(pard_j_f, pari, parj) + 2 * sign_j_ext))) * (parj - (vector(pard_j_f, pari, parj) + 2.0 * sign_j_ext)) * vector(parfrom_j_f, pari, int(parj - (vector(pard_j_f, pari, parj) + 2 * sign_j_ext))) + (vector(parFOV, pari, int(parj - (vector(pard_j_f, pari, parj) + 1 * sign_j_ext))) * (parj - (vector(pard_j_f, pari, parj) + 1.0 * sign_j_ext)) * vector(parfrom_j_f, pari, int(parj - (vector(pard_j_f, pari, parj) + 1 * sign_j_ext))) + vector(parFOV, pari, int(parj - vector(pard_j_f, pari, parj))) * (parj - (vector(pard_j_f, pari, parj) + 0.0 * sign_j_ext)) * vector(parfrom_j_f, pari, int(parj - vector(pard_j_f, pari, parj))));
		Ex2_ext = vector(parFOV, pari, int(parj - (vector(pard_j_f, pari, parj) + 2 * sign_j_ext))) * ((parj - (vector(pard_j_f, pari, parj) + 2.0 * sign_j_ext))*(parj - (vector(pard_j_f, pari, parj) + 2.0 * sign_j_ext))) + (vector(parFOV, pari, int(parj - (vector(pard_j_f, pari, parj) + 1 * sign_j_ext))) * ((parj - (vector(pard_j_f, pari, parj) + 1.0 * sign_j_ext))*(parj - (vector(pard_j_f, pari, parj) + 1.0 * sign_j_ext))) + vector(parFOV, pari, int(parj - vector(pard_j_f, pari, parj))) * ((parj - (vector(pard_j_f, pari, parj) + 0.0 * sign_j_ext))*(parj - (vector(pard_j_f, pari, parj) + 0.0 * sign_j_ext))));
		Ew_ext = vector(parFOV, pari, int(parj - (vector(pard_j_f, pari, parj) + 2 * sign_j_ext))) + (vector(parFOV, pari, int(parj - (vector(pard_j_f, pari, parj) + 1 * sign_j_ext))) + vector(parFOV, pari, int(parj - vector(pard_j_f, pari, parj))));
		Ex3_ext = vector(parFOV, pari, int(parj - (vector(pard_j_f, pari, parj) + 2 * sign_j_ext))) * ((parj - (vector(pard_j_f, pari, parj) + 2.0 * sign_j_ext))*(parj - (vector(pard_j_f, pari, parj) + 2.0 * sign_j_ext))*(parj - (vector(pard_j_f, pari, parj) + 2.0 * sign_j_ext))) + (vector(parFOV, pari, int(parj - (vector(pard_j_f, pari, parj) + 1 * sign_j_ext))) * ((parj - (vector(pard_j_f, pari, parj) + 1.0 * sign_j_ext))*(parj - (vector(pard_j_f, pari, parj) + 1.0 * sign_j_ext))*(parj - (vector(pard_j_f, pari, parj) + 1.0 * sign_j_ext))) + vector(parFOV, pari, int(parj - vector(pard_j_f, pari, parj))) * ((parj - (vector(pard_j_f, pari, parj) + 0.0 * sign_j_ext))*(parj - (vector(pard_j_f, pari, parj) + 0.0 * sign_j_ext))*(parj - (vector(pard_j_f, pari, parj) + 0.0 * sign_j_ext))));
		Ex4_ext = vector(parFOV, pari, int(parj - (vector(pard_j_f, pari, parj) + 2 * sign_j_ext))) * ((parj - (vector(pard_j_f, pari, parj) + 2.0 * sign_j_ext))*(parj - (vector(pard_j_f, pari, parj) + 2.0 * sign_j_ext))*(parj - (vector(pard_j_f, pari, parj) + 2.0 * sign_j_ext))*(parj - (vector(pard_j_f, pari, parj) + 2.0 * sign_j_ext))) + (vector(parFOV, pari, int(parj - (vector(pard_j_f, pari, parj) + 1 * sign_j_ext))) * ((parj - (vector(pard_j_f, pari, parj) + 1.0 * sign_j_ext))*(parj - (vector(pard_j_f, pari, parj) + 1.0 * sign_j_ext))*(parj - (vector(pard_j_f, pari, parj) + 1.0 * sign_j_ext))*(parj - (vector(pard_j_f, pari, parj) + 1.0 * sign_j_ext))) + vector(parFOV, pari, int(parj - vector(pard_j_f, pari, parj))) * ((parj - (vector(pard_j_f, pari, parj) + 0.0 * sign_j_ext))*(parj - (vector(pard_j_f, pari, parj) + 0.0 * sign_j_ext))*(parj - (vector(pard_j_f, pari, parj) + 0.0 * sign_j_ext))*(parj - (vector(pard_j_f, pari, parj) + 0.0 * sign_j_ext))));
		Ex2y_ext = vector(parFOV, pari, int(parj - (vector(pard_j_f, pari, parj) + 2 * sign_j_ext))) * ((parj - (vector(pard_j_f, pari, parj) + 2.0 * sign_j_ext))*(parj - (vector(pard_j_f, pari, parj) + 2.0 * sign_j_ext))) * vector(parfrom_j_f, pari, int(parj - (vector(pard_j_f, pari, parj) + 2 * sign_j_ext))) + (vector(parFOV, pari, int(parj - (vector(pard_j_f, pari, parj) + 1 * sign_j_ext))) * ((parj - (vector(pard_j_f, pari, parj) + 1.0 * sign_j_ext))*(parj - (vector(pard_j_f, pari, parj) + 1.0 * sign_j_ext))) * vector(parfrom_j_f, pari, int(parj - (vector(pard_j_f, pari, parj) + 1 * sign_j_ext))) + vector(parFOV, pari, int(parj - vector(pard_j_f, pari, parj))) * ((parj - (vector(pard_j_f, pari, parj) + 0.0 * sign_j_ext))*(parj - (vector(pard_j_f, pari, parj) + 0.0 * sign_j_ext))) * vector(parfrom_j_f, pari, int(parj - vector(pard_j_f, pari, parj))));
		Sxx_ext = Ex2_ext - (Ex_ext*Ex_ext) / Ew_ext;
		Sx2x_ext = Ex3_ext - (Ex_ext * Ex2_ext) / Ew_ext;
		Sx2x2_ext = Ex4_ext - (Ex2_ext*Ex2_ext) / Ew_ext;
		Sxy_ext = Exy_ext - (Ey_ext * Ex_ext) / Ew_ext;
		Sx2y_ext = Ex2y_ext - (Ey_ext * Ex2_ext) / Ew_ext;
		a2_ext = (Sx2y_ext * Sxx_ext - Sxy_ext * Sx2x_ext) / (Sx2x2_ext * Sxx_ext - Sx2x_ext*Sx2x_ext);
		a1_ext = (Sxy_ext - a2_ext * Sx2x_ext) / Sxx_ext;
		a0_ext = (Ey_ext - (a1_ext * Ex_ext + a2_ext * Ex2_ext)) / Ew_ext;
		if (vector(parStalled, pari, parj)) {
			vector(parto_j_f, pari, parj) = a0_ext + (a1_ext * parj + a2_ext * (parj*parj));
		} else {
			vector(parto_j_f, pari, parj) = vector(parFOV, pari, parj) / 100.0 * vector(parto_j_f, pari, parj) + (100.0 - vector(parFOV, pari, parj)) / 100.0 * (a0_ext + (a1_ext * parj + a2_ext * (parj*parj)));
		}
	}

};

