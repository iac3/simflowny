#ifndef included_Particle
#define included_Particle

#include "SAMRAI/tbox/MessageStream.h"
#include "SAMRAI/hier/IntVector.h"
#include "SAMRAI/tbox/Database.h"
#include <vector>

/**
 * The SampleClass struct holds some dummy data and methods.  It's intent
 * is to indicate how a user could construct their own index data double.
 */
using namespace std;
using namespace SAMRAI;

class Particle
{
public:

	//Struct to order particle pointer neighbourhood in std::sort
	struct less_than
	{
		const bool operator()( Particle *a,  Particle * b) const {
			return a->getDistance() < b->getDistance();
		}
	};

	inline int GetExpoBase2(double d);

	bool	Equal(double d1, double d2);

	/**
	* The default constructor.
	*/
	Particle();

	/**
	* Constructor.
	*/
	Particle(const int region);

	/**
	* The complete constructor.
	*/
	Particle(const double fieldValue, const double* pos, const int region = 0);

	/**
	* Copy constructor.
	*/
	Particle(const Particle& data);
	Particle(const Particle& data, bool newId);

	~Particle();

	//Setters 
	void setId(const int id);
	void setFieldValue(string varName, double value);

	//Getters
	double getFieldValue(string varName);
	int getId();

	//Distance between particles
	double distance_p(Particle* data);
	double distance(Particle* data);
	double distanceSPprime(Particle* data);
	double distance(double* point);


	bool same(const Particle& data);
	Particle* overlaps(const Particle& data);
	double getDistance();

	Particle& operator=(const Particle& data);

	bool operator==(const Particle& data);

	void packStream(tbox::MessageStream& stream);
	void unpackStream(tbox::MessageStream& stream, const hier::IntVector& offset);
	void getFromDatabase(std::shared_ptr<tbox::Database>& database, int particle);
	void putToDatabase(std::shared_ptr<tbox::Database>& database, int particle);

	static size_t size() {
		size_t bytes = tbox::MessageStream::getSizeof<double>(9 + 1 + 104) + tbox::MessageStream::getSizeof<int>(4);
		return(bytes);
	};

   	//Field value of the particle
	double rho;
	double rho_p;
	double mx;
	double mx_p;
	double my;
	double my_p;
	double mz;
	double mz_p;
	double e;
	double e_p;
	double fx;
	double fx_p;
	double fy;
	double fy_p;
	double fz;
	double fz_p;
	double p;
	double p_p;
	double mass;
	double FluxSBirho_3;
	double FluxSBjrho_3;
	double FluxSBkrho_3;
	double FluxSBimx_3;
	double FluxSBjmx_3;
	double FluxSBkmx_3;
	double FluxSBimy_3;
	double FluxSBjmy_3;
	double FluxSBkmy_3;
	double FluxSBimz_3;
	double FluxSBjmz_3;
	double FluxSBkmz_3;
	double FluxSBie_3;
	double FluxSBje_3;
	double FluxSBke_3;
	double mxSPS_3;
	double mySPS_3;
	double mzSPS_3;
	double eSPS_3;
	double GradSBirho_3;
	double GradSBjrho_3;
	double GradSBkrho_3;
	double GradSBimx_3;
	double GradSBjmx_3;
	double GradSBkmx_3;
	double GradSBimy_3;
	double GradSBjmy_3;
	double GradSBkmy_3;
	double GradSBimz_3;
	double GradSBjmz_3;
	double GradSBkmz_3;
	double GradSBie_3;
	double GradSBje_3;
	double GradSBke_3;
	double mxSPSprime_3;
	double mySPSprime_3;
	double mzSPSprime_3;
	double eSPSprime_3;
	double rhoSPprime;
	double mxSPprime;
	double mySPprime;
	double mzSPprime;
	double eSPprime;
	double fxSPprime;
	double fySPprime;
	double fzSPprime;
	double pSPprime;
	double FluxSBirho_1;
	double FluxSBjrho_1;
	double FluxSBkrho_1;
	double FluxSBimx_1;
	double FluxSBjmx_1;
	double FluxSBkmx_1;
	double FluxSBimy_1;
	double FluxSBjmy_1;
	double FluxSBkmy_1;
	double FluxSBimz_1;
	double FluxSBjmz_1;
	double FluxSBkmz_1;
	double FluxSBie_1;
	double FluxSBje_1;
	double FluxSBke_1;
	double mxSPS_1;
	double mySPS_1;
	double mzSPS_1;
	double eSPS_1;
	double GradSBirho_1;
	double GradSBjrho_1;
	double GradSBkrho_1;
	double GradSBimx_1;
	double GradSBjmx_1;
	double GradSBkmx_1;
	double GradSBimy_1;
	double GradSBjmy_1;
	double GradSBkmy_1;
	double GradSBimz_1;
	double GradSBjmz_1;
	double GradSBkmz_1;
	double GradSBie_1;
	double GradSBje_1;
	double GradSBke_1;
	double mxSPSprime_1;
	double mySPSprime_1;
	double mzSPSprime_1;
	double eSPSprime_1;


   	//Positions of the particle (Global index position)
	double positioni_p;
	double positionj_p;
	double positionk_p;
	double positioni;
	double positionj;
	double positionk;
	double positioniSPprime;
	double positionjSPprime;
	double positionkSPprime;


	//Region information
	int region;
	int interior;
	int newRegion; //The region to become when entering the domain (boundaries)

private:
	//Particle identifier
	int id;

	//Particle counter
	static int counter;

	//Temporal variable for order the particles in the neighbourhood of another particle
	double _distance;

};

#endif
