#include "Problem.h"
#include "Functions.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <stack>
#include "hdf5.h"
#include <time.h>
#include <silo.h>
#include <algorithm>
#include "float.h"
#include "NonSync.h"
#include "NonSyncs.h"
#include "SAMRAI/pdat/IndexVariable.h"
#include "SAMRAI/pdat/CellData.h"
#include "SAMRAI/pdat/CellVariable.h"
#include <gsl/gsl_rng.h>


#include "SAMRAI/tbox/Array.h"
#include "SAMRAI/hier/BoundaryBox.h"
#include "SAMRAI/hier/BoxContainer.h"
#include "SAMRAI/geom/CartesianPatchGeometry.h"
#include "SAMRAI/hier/VariableDatabase.h"
#include "SAMRAI/hier/PatchDataRestartManager.h"
#include "SAMRAI/hier/Index.h"
#include "SAMRAI/tbox/PIO.h"
#include "SAMRAI/tbox/Utilities.h"
#include "SAMRAI/tbox/Timer.h"
#include "SAMRAI/tbox/TimerManager.h"

#define MIN(X,Y) ((X) < (Y) ? (X) : (Y))
#define MAX(X,Y) ((X) > (Y) ? (X) : (Y))
#define SIGN(X) (((X) > 0) - ((X) < 0))
#define isEven(a) ((a) % 2 == 0 ? true : false)
#define greaterThan(a,b) (!((fabs((a) - (b))/1.0E-9 > 10 ? false: (floor(fabs((a) - (b))/1.0E-9) < 1)) || (a)<(b)))
#define lessThan(a,b) (!((fabs((a) - (b))/1.0E-9 > 10 ? false : (floor(fabs((a) - (b))/1.0E-9) < 1)) || (b)<(a)))
#define greaterEq(a,b) ((fabs((a) - (b))/1.0E-9 > 10 ? false : (floor(fabs((a) - (b))/1.0E-9) < 1)) || (b)<(a))
#define lessEq(a,b) ((fabs((a) - (b))/1.0E-9 > 10 ? false: (floor(fabs((a) - (b))/1.0E-9) < 1)) || (a)<(b))
#define equalsEq(a,b) ((fabs((a) - (b))/1.0E-9 > 10 ? false: (floor(fabs((a) - (b))/1.0E-9) < 1)))

#define vector(v, i, j, k) (v)[i+ilast*(j+jlast*(k))]
#define vectorT(v, i, j, k) (v)[i+itlast*(j+jtlast*(k))]
#define indexiOf(i) (i - (int)(i/((boxlast(0)-boxfirst(0)+1)*(boxlast(1)-boxfirst(1)+1)))*((boxlast(0)-boxfirst(0)+1)*(boxlast(1)-boxfirst(1)+1)) - (int)((i - (int)(i/((boxlast(0)-boxfirst(0)+1)*(boxlast(1)-boxfirst(1)+1)))*((boxlast(0)-boxfirst(0)+1)*(boxlast(1)-boxfirst(1)+1)))/(boxlast(0)-boxfirst(0)+1))*(boxlast(0)-boxfirst(0)+1))
#define indexjOf(i) ((int)(i - (int)((i/((boxlast(0)-boxfirst(0)+1)*(boxlast(1)-boxfirst(1)+1)))*((boxlast(0)-boxfirst(0)+1)*(boxlast(1)-boxfirst(1)+1)))/(boxlast(0)-boxfirst(0)+1)))
#define indexkOf(i) ((int)(i/((boxlast(0)-boxfirst(0)+1)*(boxlast(1)-boxfirst(1)+1))))



#define gW(pard, dx, simPlat_dt) ((-105.0) / (16.0 * 3.1415926535897932384626433832795028841971693993751 * (influenceRadius_0 * influenceRadius_0 * influenceRadius_0 * influenceRadius_0)) * ((1.0 - 0.5 * (pard) / influenceRadius_0) * (1.0 - 0.5 * (pard) / influenceRadius_0) * (1.0 - 0.5 * (pard) / influenceRadius_0)))

#define Firho_cubeI(parmx, dx, simPlat_dt) ((parmx))

#define Fjrho_cubeI(parmy, dx, simPlat_dt) ((parmy))

#define Fkrho_cubeI(parmz, dx, simPlat_dt) ((parmz))

#define Fimx_cubeI(parrho, parmx, parp, dx, simPlat_dt) (((parmx) * (parmx)) / (parrho) + (parp))

#define Fjmx_cubeI(parrho, parmx, parmy, dx, simPlat_dt) (((parmx) * (parmy)) / (parrho))

#define Fkmx_cubeI(parrho, parmx, parmz, dx, simPlat_dt) (((parmx) * (parmz)) / (parrho))

#define Fimy_cubeI(parrho, parmx, parmy, dx, simPlat_dt) (((parmy) * (parmx)) / (parrho))

#define Fjmy_cubeI(parrho, parmy, parp, dx, simPlat_dt) (((parmy) * (parmy)) / (parrho) + (parp))

#define Fkmy_cubeI(parrho, parmy, parmz, dx, simPlat_dt) (((parmy) * (parmz)) / (parrho))

#define Fimz_cubeI(parrho, parmx, parmz, dx, simPlat_dt) (((parmz) * (parmx)) / (parrho))

#define Fjmz_cubeI(parrho, parmy, parmz, dx, simPlat_dt) (((parmz) * (parmy)) / (parrho))

#define Fkmz_cubeI(parrho, parmz, parp, dx, simPlat_dt) (((parmz) * (parmz)) / (parrho) + (parp))

#define Fie_cubeI(parrho, parmx, pare, parp, dx, simPlat_dt) (((pare) + (parp)) * (parmx) / (parrho))

#define Fje_cubeI(parrho, parmy, pare, parp, dx, simPlat_dt) (((pare) + (parp)) * (parmy) / (parrho))

#define Fke_cubeI(parrho, parmz, pare, parp, dx, simPlat_dt) (((pare) + (parp)) * (parmz) / (parrho))


//Problem index box, patch geometry and deltas
const double* dx;
const gsl_rng_type * T;
gsl_rng *r_var;

//Timers
std::shared_ptr<tbox::Timer> t_step;
std::shared_ptr<tbox::Timer> t_moveParticles;

inline int Problem::GetExpoBase2(double d)
{
	int i = 0;
	((short *)(&i))[0] = (((short *)(&d))[3] & (short)32752); // _123456789ab____ & 0111111111110000
	return (i >> 4) - 1023;
}

bool	Problem::Equals(double d1, double d2)
{
	if (d1 == d2)
		return true;
	int e1 = GetExpoBase2(d1);
	int e2 = GetExpoBase2(d2);
	int e3 = GetExpoBase2(d1 - d2);
	if ((e3 - e2 < -48) && (e3 - e1 < -48))
		return true;
	return false;
}

/*
 * Constructor of the problem.
 */
Problem::Problem(const string& object_name, const tbox::Dimension& dim, std::shared_ptr<tbox::Database>& database, std::shared_ptr<geom::CartesianGridGeometry >& grid_geom, std::shared_ptr<hier::PatchHierarchy >& patch_hierarchy, const double dt, hier::BoxContainer& ba, const bool init_from_files, const vector<string> full_writer_variables): 
d_dim(dim), xfer::RefinePatchStrategy(), xfer::CoarsenPatchStrategy(), d_regridding_boxes(ba), d_full_writer_variables(full_writer_variables.begin(), full_writer_variables.end())
{
	//Setup the timers
	t_step = tbox::TimerManager::getManager()->getTimer("Step");
	t_moveParticles = tbox::TimerManager::getManager()->getTimer("Move particles");

	//Get the object name, the grid geometry and the patch hierarchy
  	d_grid_geometry = grid_geom;
	d_patch_hierarchy = patch_hierarchy;
	d_object_name = object_name;
	d_init_from_files = init_from_files;
	initial_dt = dt;




	//Get parameters
	dx  = grid_geom->getDx();
	lambda = database->getDouble("lambda");
	gamma = database->getDouble("gamma");
	pfz = database->getDouble("pfz");
	mu = database->getDouble("mu");
	pfx = database->getDouble("pfx");
	pfy = database->getDouble("pfy");
	std::shared_ptr<tbox::Database> particles_db(database->getDatabase("particles"));
	particleDistribution = particles_db->getString("particle_distribution");
	particleSeparation_x = (grid_geom->getXUpper()[0] - grid_geom->getXLower()[0])/particles_db->getInteger("number_of_particles_x");
	if (particleSeparation_x <= 0) {
		TBOX_ERROR("Problem::Problem"
			<< "n      Error in parameter input:"
			<< "n      number_of_particles_x must be greater than 0"<< std::endl);
	}
	particleSeparation_y = (grid_geom->getXUpper()[1] - grid_geom->getXLower()[1])/particles_db->getInteger("number_of_particles_y");
	if (particleSeparation_y <= 0) {
		TBOX_ERROR("Problem::Problem"
			<< "n      Error in parameter input:"
			<< "n      number_of_particles_y must be greater than 0"<< std::endl);
	}
	particleSeparation_z = (grid_geom->getXUpper()[2] - grid_geom->getXLower()[2])/particles_db->getInteger("number_of_particles_z");
	if (particleSeparation_z <= 0) {
		TBOX_ERROR("Problem::Problem"
			<< "n      Error in parameter input:"
			<< "n      number_of_particles_z must be greater than 0"<< std::endl);
	}
	influenceRadius_0 = MAX(particleSeparation_x, MAX(particleSeparation_y, particleSeparation_z)) * particles_db->getDouble("influenceRadius_0");
	//Random initialization
	gsl_rng_env_setup();
	//Random for simulation
	const tbox::SAMRAI_MPI& mpi(tbox::SAMRAI_MPI::getSAMRAIWorld());
	int random_seed = database->getDouble("random_seed")*(mpi.getRank() + 1);
	r_var = gsl_rng_alloc(gsl_rng_ranlxs0);
	gsl_rng_set(r_var, random_seed);


    //Subcycling
	d_refinedTimeStepping = false;
	d_tappering = false;



	//Stencil of the discretization method
	d_ghost_width = MAX(ceil(2*particles_db->getDouble("influenceRadius_0")*(particleSeparation_x/dx[0])), MAX(ceil(2*particles_db->getDouble("influenceRadius_0")*(particleSeparation_y/dx[1])), ceil(2*particles_db->getDouble("influenceRadius_0")*(particleSeparation_z/dx[2]))));

	
	//Register Fields and temporal fields into the variable database
	hier::VariableDatabase *vdb = hier::VariableDatabase::getDatabase();
  	std::shared_ptr<hier::VariableContext> d_cont_curr(vdb->getContext("Current"));
	std::shared_ptr< pdat::CellVariable<int> > interior(std::shared_ptr< pdat::CellVariable<int> >(new pdat::CellVariable<int>(d_dim, "interior",1)));
	d_interior_id = vdb->registerVariableAndContext(interior ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_interior_id);
	std::shared_ptr< pdat::CellVariable<int> > nonSync(std::shared_ptr< pdat::CellVariable<int> >(new pdat::CellVariable<int>(d_dim, "nonSync",1)));
	d_nonSync_id = vdb->registerVariableAndContext(nonSync ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_nonSync_id);
	std::shared_ptr< pdat::CellVariable<int> > region(std::shared_ptr< pdat::CellVariable<int> >(new pdat::CellVariable<int>(d_dim, "region",1)));
	d_region_id = vdb->registerVariableAndContext(region ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_region_id);
	std::shared_ptr< pdat::IndexVariable<NonSyncs, pdat::CellGeometry > > nonSyncP(std::shared_ptr< pdat::IndexVariable<NonSyncs, pdat::CellGeometry > >(new pdat::IndexVariable<NonSyncs, pdat::CellGeometry >(d_dim, "nonSyncP")));
	d_nonSyncP_id = vdb->registerVariableAndContext(nonSyncP ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_nonSyncP_id);
	std::shared_ptr< pdat::IndexVariable<Particles, pdat::CellGeometry > > problemVariable(std::shared_ptr< pdat::IndexVariable<Particles, pdat::CellGeometry > >(new pdat::IndexVariable<Particles, pdat::CellGeometry >(d_dim, "problemVariable")));
	d_problemVariable_id = vdb->registerVariableAndContext(problemVariable, d_cont_curr, hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_problemVariable_id);
	std::shared_ptr< pdat::CellVariable<double> > interior_i(std::shared_ptr< pdat::CellVariable<double> >(new pdat::CellVariable<double>(d_dim, "interior_i",1)));
	d_interior_i_id = vdb->registerVariableAndContext(interior_i ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_interior_i_id);
	std::shared_ptr< pdat::CellVariable<double> > interior_j(std::shared_ptr< pdat::CellVariable<double> >(new pdat::CellVariable<double>(d_dim, "interior_j",1)));
	d_interior_j_id = vdb->registerVariableAndContext(interior_j ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_interior_j_id);
	std::shared_ptr< pdat::CellVariable<double> > interior_k(std::shared_ptr< pdat::CellVariable<double> >(new pdat::CellVariable<double>(d_dim, "interior_k",1)));
	d_interior_k_id = vdb->registerVariableAndContext(interior_k ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_interior_k_id);
	std::shared_ptr< pdat::CellVariable<int> > seg1(std::shared_ptr< pdat::CellVariable<int> >(new pdat::CellVariable<int>(d_dim, "seg1")));
	d_seg1_id = vdb->registerVariableAndContext(seg1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_seg1_id);
	std::shared_ptr< pdat::CellVariable<int> > segL1(std::shared_ptr< pdat::CellVariable<int> >(new pdat::CellVariable<int>(d_dim, "segL1")));
	d_segL1_id = vdb->registerVariableAndContext(segL1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_segL1_id);
	std::shared_ptr< pdat::CellVariable<int> > seg3(std::shared_ptr< pdat::CellVariable<int> >(new pdat::CellVariable<int>(d_dim, "seg3")));
	d_seg3_id = vdb->registerVariableAndContext(seg3 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_seg3_id);
	std::shared_ptr< pdat::CellVariable<int> > segL3(std::shared_ptr< pdat::CellVariable<int> >(new pdat::CellVariable<int>(d_dim, "segL3")));
	d_segL3_id = vdb->registerVariableAndContext(segL3 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_segL3_id);
	std::shared_ptr< pdat::CellVariable<int> > segxLower(std::shared_ptr< pdat::CellVariable<int> >(new pdat::CellVariable<int>(d_dim, "segxLower")));
	d_segxLower_id = vdb->registerVariableAndContext(segxLower ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_segxLower_id);
	std::shared_ptr< pdat::CellVariable<int> > segLxLower(std::shared_ptr< pdat::CellVariable<int> >(new pdat::CellVariable<int>(d_dim, "segLxLower")));
	d_segLxLower_id = vdb->registerVariableAndContext(segLxLower ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_segLxLower_id);
	std::shared_ptr< pdat::CellVariable<int> > segxUpper(std::shared_ptr< pdat::CellVariable<int> >(new pdat::CellVariable<int>(d_dim, "segxUpper")));
	d_segxUpper_id = vdb->registerVariableAndContext(segxUpper ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_segxUpper_id);
	std::shared_ptr< pdat::CellVariable<int> > segLxUpper(std::shared_ptr< pdat::CellVariable<int> >(new pdat::CellVariable<int>(d_dim, "segLxUpper")));
	d_segLxUpper_id = vdb->registerVariableAndContext(segLxUpper ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_segLxUpper_id);
	std::shared_ptr< pdat::CellVariable<int> > segyLower(std::shared_ptr< pdat::CellVariable<int> >(new pdat::CellVariable<int>(d_dim, "segyLower")));
	d_segyLower_id = vdb->registerVariableAndContext(segyLower ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_segyLower_id);
	std::shared_ptr< pdat::CellVariable<int> > segLyLower(std::shared_ptr< pdat::CellVariable<int> >(new pdat::CellVariable<int>(d_dim, "segLyLower")));
	d_segLyLower_id = vdb->registerVariableAndContext(segLyLower ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_segLyLower_id);
	std::shared_ptr< pdat::CellVariable<int> > segyUpper(std::shared_ptr< pdat::CellVariable<int> >(new pdat::CellVariable<int>(d_dim, "segyUpper")));
	d_segyUpper_id = vdb->registerVariableAndContext(segyUpper ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_segyUpper_id);
	std::shared_ptr< pdat::CellVariable<int> > segLyUpper(std::shared_ptr< pdat::CellVariable<int> >(new pdat::CellVariable<int>(d_dim, "segLyUpper")));
	d_segLyUpper_id = vdb->registerVariableAndContext(segLyUpper ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_segLyUpper_id);
	std::shared_ptr< pdat::CellVariable<int> > segzLower(std::shared_ptr< pdat::CellVariable<int> >(new pdat::CellVariable<int>(d_dim, "segzLower")));
	d_segzLower_id = vdb->registerVariableAndContext(segzLower ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_segzLower_id);
	std::shared_ptr< pdat::CellVariable<int> > segLzLower(std::shared_ptr< pdat::CellVariable<int> >(new pdat::CellVariable<int>(d_dim, "segLzLower")));
	d_segLzLower_id = vdb->registerVariableAndContext(segLzLower ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_segLzLower_id);
	std::shared_ptr< pdat::CellVariable<int> > segzUpper(std::shared_ptr< pdat::CellVariable<int> >(new pdat::CellVariable<int>(d_dim, "segzUpper")));
	d_segzUpper_id = vdb->registerVariableAndContext(segzUpper ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_segzUpper_id);
	std::shared_ptr< pdat::CellVariable<int> > segLzUpper(std::shared_ptr< pdat::CellVariable<int> >(new pdat::CellVariable<int>(d_dim, "segLzUpper")));
	d_segLzUpper_id = vdb->registerVariableAndContext(segLzUpper ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_segLzUpper_id);


	//Refine and coarse algorithms

	d_bdry_fill_init = std::shared_ptr< xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());
	d_bdry_fill_advance = std::shared_ptr< xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());
	d_coarsen_algorithm = std::shared_ptr< xfer::CoarsenAlgorithm >(new xfer::CoarsenAlgorithm(d_dim));

	d_mapping_fill = std::shared_ptr< xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());
	d_fill_new_level    = std::shared_ptr< xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());

	//mapping communication

	string mapping_op_name = "CONSTANT_REFINE";
	std::shared_ptr< hier::RefineOperator > refine_region2(d_grid_geometry->lookupRefineOperator(region, mapping_op_name));
	std::shared_ptr< hier::RefineOperator > refine_interior(d_grid_geometry->lookupRefineOperator(interior, mapping_op_name));
	d_mapping_fill->registerRefine(d_interior_id,d_interior_id,d_interior_id, refine_interior);
	d_mapping_fill->registerRefine(d_region_id,d_region_id,d_region_id, refine_region2);
	string particleMapping_op_name = "NO_REFINE";
	std::shared_ptr< hier::RefineOperator > refine_particle(d_grid_geometry->lookupRefineOperator(problemVariable, particleMapping_op_name));
	d_mapping_fill->registerRefine(d_problemVariable_id,d_problemVariable_id,d_problemVariable_id, refine_particle);


	//refine and coarsen operators
	string refine_op_name = "NO_REFINE";
	std::shared_ptr< hier::RefineOperator > refine = d_grid_geometry->lookupRefineOperator(problemVariable, refine_op_name);


	//Register variables to the refineAlgorithm for boundaries

	d_bdry_fill_init->registerRefine(d_problemVariable_id,d_problemVariable_id,d_problemVariable_id,refine);
	d_bdry_fill_advance->registerRefine(d_problemVariable_id,d_problemVariable_id,d_problemVariable_id,refine);
	//Region's cell influence communication
	std::shared_ptr< hier::RefineOperator > refine3(d_grid_geometry->lookupRefineOperator(seg3, refine_op_name));
	std::shared_ptr< hier::RefineOperator > refineL3(d_grid_geometry->lookupRefineOperator(segL3, refine_op_name));
	d_bdry_fill_advance->registerRefine(d_seg3_id,d_seg3_id,d_seg3_id,refine3);
	d_bdry_fill_advance->registerRefine(d_segL3_id,d_segL3_id,d_segL3_id,refineL3);
	std::shared_ptr< hier::RefineOperator > refine1(d_grid_geometry->lookupRefineOperator(seg1, refine_op_name));
	std::shared_ptr< hier::RefineOperator > refineL1(d_grid_geometry->lookupRefineOperator(segL1, refine_op_name));
	d_bdry_fill_advance->registerRefine(d_seg1_id,d_seg1_id,d_seg1_id,refine1);
	d_bdry_fill_advance->registerRefine(d_segL1_id,d_segL1_id,d_segL1_id,refineL1);
	std::shared_ptr< hier::RefineOperator > refinexLower(d_grid_geometry->lookupRefineOperator(segxLower, refine_op_name));
	std::shared_ptr< hier::RefineOperator > refineLxLower(d_grid_geometry->lookupRefineOperator(segLxLower, refine_op_name));
	d_bdry_fill_advance->registerRefine(d_segxLower_id,d_segxLower_id,d_segxLower_id,refinexLower);
	d_bdry_fill_advance->registerRefine(d_segLxLower_id,d_segLxLower_id,d_segLxLower_id,refineLxLower);
	std::shared_ptr< hier::RefineOperator > refinexUpper(d_grid_geometry->lookupRefineOperator(segxUpper, refine_op_name));
	std::shared_ptr< hier::RefineOperator > refineLxUpper(d_grid_geometry->lookupRefineOperator(segLxUpper, refine_op_name));
	d_bdry_fill_advance->registerRefine(d_segxUpper_id,d_segxUpper_id,d_segxUpper_id,refinexUpper);
	d_bdry_fill_advance->registerRefine(d_segLxUpper_id,d_segLxUpper_id,d_segLxUpper_id,refineLxUpper);
	std::shared_ptr< hier::RefineOperator > refineyLower(d_grid_geometry->lookupRefineOperator(segyLower, refine_op_name));
	std::shared_ptr< hier::RefineOperator > refineLyLower(d_grid_geometry->lookupRefineOperator(segLyLower, refine_op_name));
	d_bdry_fill_advance->registerRefine(d_segyLower_id,d_segyLower_id,d_segyLower_id,refineyLower);
	d_bdry_fill_advance->registerRefine(d_segLyLower_id,d_segLyLower_id,d_segLyLower_id,refineLyLower);
	std::shared_ptr< hier::RefineOperator > refineyUpper(d_grid_geometry->lookupRefineOperator(segyUpper, refine_op_name));
	std::shared_ptr< hier::RefineOperator > refineLyUpper(d_grid_geometry->lookupRefineOperator(segLyUpper, refine_op_name));
	d_bdry_fill_advance->registerRefine(d_segyUpper_id,d_segyUpper_id,d_segyUpper_id,refineyUpper);
	d_bdry_fill_advance->registerRefine(d_segLyUpper_id,d_segLyUpper_id,d_segLyUpper_id,refineLyUpper);
	std::shared_ptr< hier::RefineOperator > refinezLower(d_grid_geometry->lookupRefineOperator(segzLower, refine_op_name));
	std::shared_ptr< hier::RefineOperator > refineLzLower(d_grid_geometry->lookupRefineOperator(segLzLower, refine_op_name));
	d_bdry_fill_advance->registerRefine(d_segzLower_id,d_segzLower_id,d_segzLower_id,refinezLower);
	d_bdry_fill_advance->registerRefine(d_segLzLower_id,d_segLzLower_id,d_segLzLower_id,refineLzLower);
	std::shared_ptr< hier::RefineOperator > refinezUpper(d_grid_geometry->lookupRefineOperator(segzUpper, refine_op_name));
	std::shared_ptr< hier::RefineOperator > refineLzUpper(d_grid_geometry->lookupRefineOperator(segLzUpper, refine_op_name));
	d_bdry_fill_advance->registerRefine(d_segzUpper_id,d_segzUpper_id,d_segzUpper_id,refinezUpper);
	d_bdry_fill_advance->registerRefine(d_segLzUpper_id,d_segLzUpper_id,d_segLzUpper_id,refineLzUpper);


	//Register variables to the refineAlgorithm for filling new levels on regridding


	//Register variables to the coarsenAlgorithm


}

/*
 * Destructor.
 */
Problem::~Problem() 
{
} 

/*
 * Initialize the data from a given level.
 */
void Problem::initializeLevelData (
   const std::shared_ptr<hier::PatchHierarchy >& hierarchy , 
   const int level_number ,
   const double init_data_time ,
   const bool can_be_refined ,
   const bool initial_time ,
   const std::shared_ptr<hier::PatchLevel >& old_level ,
   const bool allocate_data )
{
	std::shared_ptr< hier::PatchLevel > level(hierarchy->getPatchLevel(level_number));

	// Allocate storage needed to initialize level and fill data from coarser levels in AMR hierarchy.  
	level->allocatePatchData(d_interior_id, init_data_time);
	level->allocatePatchData(d_nonSync_id, init_data_time);
	level->allocatePatchData(d_interior_i_id, init_data_time);
	level->allocatePatchData(d_interior_j_id, init_data_time);
	level->allocatePatchData(d_interior_k_id, init_data_time);
	level->allocatePatchData(d_seg1_id, init_data_time);
	level->allocatePatchData(d_segL1_id, init_data_time);
	level->allocatePatchData(d_seg3_id, init_data_time);
	level->allocatePatchData(d_segL3_id, init_data_time);
	level->allocatePatchData(d_segxLower_id, init_data_time);
	level->allocatePatchData(d_segLxLower_id, init_data_time);
	level->allocatePatchData(d_segxUpper_id, init_data_time);
	level->allocatePatchData(d_segLxUpper_id, init_data_time);
	level->allocatePatchData(d_segyLower_id, init_data_time);
	level->allocatePatchData(d_segLyLower_id, init_data_time);
	level->allocatePatchData(d_segyUpper_id, init_data_time);
	level->allocatePatchData(d_segLyUpper_id, init_data_time);
	level->allocatePatchData(d_segzLower_id, init_data_time);
	level->allocatePatchData(d_segLzLower_id, init_data_time);
	level->allocatePatchData(d_segzUpper_id, init_data_time);
	level->allocatePatchData(d_segLzUpper_id, init_data_time);
	level->allocatePatchData(d_problemVariable_id, init_data_time);
	level->allocatePatchData(d_nonSyncP_id, init_data_time);
	level->allocatePatchData(d_region_id, init_data_time);




	//Mapping the current data for new level.
	if (initial_time) {
		mapDataOnPatch(init_data_time, initial_time, level_number, level);
	}





	//Initialize current data for new level.
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr< hier::Patch > patch = *p_it;
        		if (initial_time) {
	      		initializeDataOnPatch(*patch, init_data_time, initial_time);
        		}
	}
	//Post-initialization Sync.
    	if (initial_time) {

		d_bdry_fill_init->createSchedule(level,this)->fillData(init_data_time, false);
		for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
			const std::shared_ptr< hier::Patch > patch = *p_it;
			std::shared_ptr< pdat::IndexData<Particles, pdat::CellGeometry > > problemVariable(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles, pdat::CellGeometry >, hier::PatchData>(patch->getPatchData(d_problemVariable_id)));
			for (int index2 = boxfirst(2); index2 <= boxlast(2); index2++) {
				for (int index1 = boxfirst(1); index1 <= boxlast(1); index1++) {
					for (int index0 = boxfirst(0); index0 <= boxlast(0); index0++) {
						hier::Index idx(index0, index1, index2);
						//Correct the position if there is periodical boundary
						Particles* part = problemVariable->getItem(idx);
						checkPosition(patch, index0, index1, index2, part);
					}
				}
			}
		}

    	}
}



void Problem::initializeLevelIntegrator(
   const std::shared_ptr<mesh::GriddingAlgorithmStrategy>& gridding_alg)
{
}

double Problem::getLevelDt(
   const std::shared_ptr<hier::PatchLevel>& level,
   const double dt_time,
   const bool initial_time)
{
  
   TBOX_ASSERT(level);

   if (level->getLevelNumber() == 0) return initial_dt;

    double dt = initial_dt;
    const hier::IntVector ratio = level->getRatioToLevelZero();
    double local_dt = dt;
    for (int i = 0; i < 2; i++) {
        if (local_dt > dt / ratio[i]) {
            local_dt = dt / ratio[i];
        }
    }
    return local_dt;
}

double Problem::getMaxFinerLevelDt(
   const int finer_level_number,
   const double coarse_dt,
   const hier::IntVector& ratio)
{
   NULL_USE(finer_level_number);

   TBOX_ASSERT(ratio.min() > 0);

   return coarse_dt / double(ratio.max());
}

void Problem::standardLevelSynchronization(
   const std::shared_ptr<hier::PatchHierarchy>& hierarchy,
   const int coarsest_level,
   const int finest_level,
   const double sync_time,
   const std::vector<double>& old_times)
{

}

void Problem::synchronizeNewLevels(
   const std::shared_ptr<hier::PatchHierarchy>& hierarchy,
   const int coarsest_level,
   const int finest_level,
   const double sync_time,
   const bool initial_time)
{

}

void Problem::resetTimeDependentData(
   const std::shared_ptr<hier::PatchLevel>& level,
   const double new_time,
   const bool can_be_refined)
{
   TBOX_ASSERT(level);
   level->setTime(new_time);
}

void Problem::resetDataToPreadvanceState(
   const std::shared_ptr<hier::PatchLevel>& level)
{
    //cout<<"resetDataToPreadvanceState"<<endl;
}

/*
 * Map data on a patch. This mapping is done only at the begining of the simulation.
 */
void Problem::mapDataOnPatch(const double time, const bool initial_time, const int ln, const std::shared_ptr< hier::PatchLevel >& level)
{
	(void) time;
	const tbox::SAMRAI_MPI& mpi(tbox::SAMRAI_MPI::getSAMRAIWorld());
   	if (initial_time) {

		// Mapping		
		int i, iterm, iMapStartC, iMapEndC, il, iu;
		double iMapStart, iMapEnd, tmpi;
		xGlower = d_grid_geometry->getXLower()[0];
		xGupper = d_grid_geometry->getXUpper()[0];
		int j, jterm, jMapStartC, jMapEndC, jl, ju;
		double jMapStart, jMapEnd, tmpj;
		yGlower = d_grid_geometry->getXLower()[1];
		yGupper = d_grid_geometry->getXUpper()[1];
		int k, kterm, kMapStartC, kMapEndC, kl, ku;
		double kMapStart, kMapEnd, tmpk;
		zGlower = d_grid_geometry->getXLower()[2];
		zGupper = d_grid_geometry->getXUpper()[2];
		int minBlock[3], maxBlock[3], unionsI, facePointI, ie1, ie2, ie3, proc, tmp, pred, nodes;
		double maxDistance, e1, e2, e3;
		double position[3], maxPosition[3], minPosition[3];
		bool workingArray[mpi.getSize()], finishedArray[mpi.getSize()], workingGlobal, finishedGlobal, working, finished, modif;
		double SQRT3INV = 1.0/sqrt(3.0);
		mapStencilx = ceil(2*(influenceRadius_0/particleSeparation_x));
		mapStencily = ceil(2*(influenceRadius_0/particleSeparation_y));
		mapStencilz = ceil(2*(influenceRadius_0/particleSeparation_z));
		mapStencil = MAX(MAX(mapStencilx, mapStencily), mapStencilz);

		ilastG = d_grid_geometry->getPhysicalDomain().front().numberCells()[0] + 2 * d_ghost_width;
		jlastG = d_grid_geometry->getPhysicalDomain().front().numberCells()[1] + 2 * d_ghost_width;
		klastG = d_grid_geometry->getPhysicalDomain().front().numberCells()[2] + 2 * d_ghost_width;

		//Cell mapping
		//Region: cube2I
		for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
			const std::shared_ptr< hier::Patch > patch = *p_it;
			int* region = ((pdat::CellData<int> *) patch->getPatchData(d_region_id).get())->getPointer();
			int* interior_i = ((pdat::CellData<int> *) patch->getPatchData(d_interior_i_id).get())->getPointer();
			int* interior_j = ((pdat::CellData<int> *) patch->getPatchData(d_interior_j_id).get())->getPointer();
			int* interior_k = ((pdat::CellData<int> *) patch->getPatchData(d_interior_k_id).get())->getPointer();
			int* interior = ((pdat::CellData<int> *) patch->getPatchData(d_interior_id).get())->getPointer();
			int* nonSync = ((pdat::CellData<int> *) patch->getPatchData(d_nonSync_id).get())->getPointer();
			const hier::Index boxfirst1 = patch->getBox().lower();
			const hier::Index boxlast1  = patch->getBox().upper();

			//Get delta spaces into an array. dx, dy, dz.
			const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
			const double* dx  = patch_geom->getDx();

			int ilast = boxlast1(0)-boxfirst1(0) + 1 + 2 * d_ghost_width;
			int jlast = boxlast1(1)-boxfirst1(1) + 1 + 2 * d_ghost_width;
			int klast = boxlast1(2)-boxfirst1(2) + 1 + 2 * d_ghost_width;
			if (0 + d_ghost_width <= boxlast1(0) - boxfirst1(0) + d_ghost_width && (boxlast1(0) - boxfirst1(0)) + d_ghost_width >= d_ghost_width && 0 + d_ghost_width <= boxlast1(1) - boxfirst1(1) + d_ghost_width && (boxlast1(1) - boxfirst1(1)) + d_ghost_width >= d_ghost_width && 0 + d_ghost_width <= boxlast1(2) - boxfirst1(2) + d_ghost_width && (boxlast1(2) - boxfirst1(2)) + d_ghost_width >= d_ghost_width) {
				iMapStartC = 0 + d_ghost_width;
				iMapEndC = (boxlast1(0) - boxfirst1(0)) + d_ghost_width;
				jMapStartC = 0 + d_ghost_width;
				jMapEndC = (boxlast1(1) - boxfirst1(1)) + d_ghost_width;
				kMapStartC = 0 + d_ghost_width;
				kMapEndC = (boxlast1(2) - boxfirst1(2)) + d_ghost_width;
				for (i = iMapStartC; i <= iMapEndC; i++) {
					for (j = jMapStartC; j <= jMapEndC; j++) {
						for (k = kMapStartC; k <= kMapEndC; k++) {
							vector(region, i, j, k) = 3;
						}
					}
				}
				//Check stencil
				for (k = 0; k < klast; k++) {
					for (j = 0; j < jlast; j++) {
						for (i = 0; i < ilast; i++) {
							vector(interior_i, i, j, k) = 0;
							vector(interior_j, i, j, k) = 0;
							vector(interior_k, i, j, k) = 0;
						}
					}
				}
				for (k = 0; k < klast; k++) {
					for (j = 0; j < jlast; j++) {
						for (i = 0; i < ilast; i++) {
							if (vector(region, i, j, k) == 3) {
								setStencilLimits(patch, i, j, k, 3);
							}
						}
					}
				}
			}
		}
		d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);
		for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
			const std::shared_ptr< hier::Patch > patch = *p_it;
			int* region = ((pdat::CellData<int> *) patch->getPatchData(d_region_id).get())->getPointer();
			int* interior_i = ((pdat::CellData<int> *) patch->getPatchData(d_interior_i_id).get())->getPointer();
			int* interior_j = ((pdat::CellData<int> *) patch->getPatchData(d_interior_j_id).get())->getPointer();
			int* interior_k = ((pdat::CellData<int> *) patch->getPatchData(d_interior_k_id).get())->getPointer();
			int* interior = ((pdat::CellData<int> *) patch->getPatchData(d_interior_id).get())->getPointer();
			int* nonSync = ((pdat::CellData<int> *) patch->getPatchData(d_nonSync_id).get())->getPointer();
			const hier::Index boxfirst1 = patch->getBox().lower();
			const hier::Index boxlast1  = patch->getBox().upper();

			//Get delta spaces into an array. dx, dy, dz.
			const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
			const double* dx  = patch_geom->getDx();

			int ilast = boxlast1(0)-boxfirst1(0) + 1 + 2 * d_ghost_width;
			int jlast = boxlast1(1)-boxfirst1(1) + 1 + 2 * d_ghost_width;
			int klast = boxlast1(2)-boxfirst1(2) + 1 + 2 * d_ghost_width;
			for (k = 0; k < klast; k++) {
				for (j = 0; j < jlast; j++) {
					for (i = 0; i < ilast; i++) {
						if ((abs(vector(interior_i, i, j, k)) > 1 || abs(vector(interior_j, i, j, k)) > 1 || abs(vector(interior_k, i, j, k)) > 1) && (i < d_ghost_width || i >= ilast - d_ghost_width || j < d_ghost_width || j >= jlast - d_ghost_width || k < d_ghost_width || k >= klast - d_ghost_width)) {
							checkStencilCell(patch, i, j, k, 3);
						}
					}
				}
			}
			for (k = 0; k < klast; k++) {
				for (j = 0; j < jlast; j++) {
					for (i = 0; i < ilast; i++) {
						if (vector(interior_i, i, j, k) != 0 || vector(interior_j, i, j, k) != 0 || vector(interior_k, i, j, k) != 0) {
							vector(region, i, j, k) = 3;
						}
					}
				}
			}
		}
		d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);

		//Region: cubeI
		for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
			const std::shared_ptr< hier::Patch > patch = *p_it;
			int* region = ((pdat::CellData<int> *) patch->getPatchData(d_region_id).get())->getPointer();
			int* interior_i = ((pdat::CellData<int> *) patch->getPatchData(d_interior_i_id).get())->getPointer();
			int* interior_j = ((pdat::CellData<int> *) patch->getPatchData(d_interior_j_id).get())->getPointer();
			int* interior_k = ((pdat::CellData<int> *) patch->getPatchData(d_interior_k_id).get())->getPointer();
			int* interior = ((pdat::CellData<int> *) patch->getPatchData(d_interior_id).get())->getPointer();
			int* nonSync = ((pdat::CellData<int> *) patch->getPatchData(d_nonSync_id).get())->getPointer();
			const hier::Index boxfirst1 = patch->getBox().lower();
			const hier::Index boxlast1  = patch->getBox().upper();

			//Get delta spaces into an array. dx, dy, dz.
			const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
			const double* dx  = patch_geom->getDx();

			int ilast = boxlast1(0)-boxfirst1(0) + 1 + 2 * d_ghost_width;
			int jlast = boxlast1(1)-boxfirst1(1) + 1 + 2 * d_ghost_width;
			int klast = boxlast1(2)-boxfirst1(2) + 1 + 2 * d_ghost_width;
			if (floor(0.5 / dx[0]) - boxfirst1(0) + d_ghost_width <= boxlast1(0) - boxfirst1(0) + d_ghost_width && floor(1.5 / dx[0]) - boxfirst1(0) + d_ghost_width >= d_ghost_width && floor(0.5 / dx[1]) - boxfirst1(1) + d_ghost_width <= boxlast1(1) - boxfirst1(1) + d_ghost_width && floor(1.5 / dx[1]) - boxfirst1(1) + d_ghost_width >= d_ghost_width && floor(0.5 / dx[2]) - boxfirst1(2) + d_ghost_width <= boxlast1(2) - boxfirst1(2) + d_ghost_width && floor(1.5 / dx[2]) - boxfirst1(2) + d_ghost_width >= d_ghost_width) {
				if (floor(0.5 / dx[0]) - boxfirst1(0) + d_ghost_width > d_ghost_width) {
					iMapStartC = floor(0.5 / dx[0]) - boxfirst1(0) + d_ghost_width;
				} else {
					iMapStartC = 0;
				}
				if (floor(1.5 / dx[0]) - boxfirst1(0) + d_ghost_width < boxlast1(0) - boxfirst1(0) + d_ghost_width) {
					iMapEndC = floor(1.5 / dx[0]) - boxfirst1(0) + d_ghost_width;
				} else {
					iMapEndC = boxlast1(0) - boxfirst1(0) + 2 * d_ghost_width;
				}
				if (floor(0.5 / dx[1]) - boxfirst1(1) + d_ghost_width > d_ghost_width) {
					jMapStartC = floor(0.5 / dx[1]) - boxfirst1(1) + d_ghost_width;
				} else {
					jMapStartC = 0;
				}
				if (floor(1.5 / dx[1]) - boxfirst1(1) + d_ghost_width < boxlast1(1) - boxfirst1(1) + d_ghost_width) {
					jMapEndC = floor(1.5 / dx[1]) - boxfirst1(1) + d_ghost_width;
				} else {
					jMapEndC = boxlast1(1) - boxfirst1(1) + 2 * d_ghost_width;
				}
				if (floor(0.5 / dx[2]) - boxfirst1(2) + d_ghost_width > d_ghost_width) {
					kMapStartC = floor(0.5 / dx[2]) - boxfirst1(2) + d_ghost_width;
				} else {
					kMapStartC = 0;
				}
				if (floor(1.5 / dx[2]) - boxfirst1(2) + d_ghost_width < boxlast1(2) - boxfirst1(2) + d_ghost_width) {
					kMapEndC = floor(1.5 / dx[2]) - boxfirst1(2) + d_ghost_width;
				} else {
					kMapEndC = boxlast1(2) - boxfirst1(2) + 2 * d_ghost_width;
				}
				for (i = iMapStartC; i <= iMapEndC; i++) {
					for (j = jMapStartC; j <= jMapEndC; j++) {
						for (k = kMapStartC; k <= kMapEndC; k++) {
							vector(region, i, j, k) = 1;
						}
					}
				}
				//Check stencil
				for (k = 0; k < klast; k++) {
					for (j = 0; j < jlast; j++) {
						for (i = 0; i < ilast; i++) {
							vector(interior_i, i, j, k) = 0;
							vector(interior_j, i, j, k) = 0;
							vector(interior_k, i, j, k) = 0;
						}
					}
				}
				for (k = 0; k < klast; k++) {
					for (j = 0; j < jlast; j++) {
						for (i = 0; i < ilast; i++) {
							if (vector(region, i, j, k) == 1) {
								setStencilLimits(patch, i, j, k, 1);
							}
						}
					}
				}
			}
		}
		d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);
		for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
			const std::shared_ptr< hier::Patch > patch = *p_it;
			int* region = ((pdat::CellData<int> *) patch->getPatchData(d_region_id).get())->getPointer();
			int* interior_i = ((pdat::CellData<int> *) patch->getPatchData(d_interior_i_id).get())->getPointer();
			int* interior_j = ((pdat::CellData<int> *) patch->getPatchData(d_interior_j_id).get())->getPointer();
			int* interior_k = ((pdat::CellData<int> *) patch->getPatchData(d_interior_k_id).get())->getPointer();
			int* interior = ((pdat::CellData<int> *) patch->getPatchData(d_interior_id).get())->getPointer();
			int* nonSync = ((pdat::CellData<int> *) patch->getPatchData(d_nonSync_id).get())->getPointer();
			const hier::Index boxfirst1 = patch->getBox().lower();
			const hier::Index boxlast1  = patch->getBox().upper();

			//Get delta spaces into an array. dx, dy, dz.
			const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
			const double* dx  = patch_geom->getDx();

			int ilast = boxlast1(0)-boxfirst1(0) + 1 + 2 * d_ghost_width;
			int jlast = boxlast1(1)-boxfirst1(1) + 1 + 2 * d_ghost_width;
			int klast = boxlast1(2)-boxfirst1(2) + 1 + 2 * d_ghost_width;
			for (k = 0; k < klast; k++) {
				for (j = 0; j < jlast; j++) {
					for (i = 0; i < ilast; i++) {
						if ((abs(vector(interior_i, i, j, k)) > 1 || abs(vector(interior_j, i, j, k)) > 1 || abs(vector(interior_k, i, j, k)) > 1) && (i < d_ghost_width || i >= ilast - d_ghost_width || j < d_ghost_width || j >= jlast - d_ghost_width || k < d_ghost_width || k >= klast - d_ghost_width)) {
							checkStencilCell(patch, i, j, k, 1);
						}
					}
				}
			}
			for (k = 0; k < klast; k++) {
				for (j = 0; j < jlast; j++) {
					for (i = 0; i < ilast; i++) {
						if (vector(interior_i, i, j, k) != 0 || vector(interior_j, i, j, k) != 0 || vector(interior_k, i, j, k) != 0) {
							vector(region, i, j, k) = 1;
						}
					}
				}
			}
		}
		d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);

		//Region: cubeS
		for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
			const std::shared_ptr< hier::Patch > patch = *p_it;
			int* region = ((pdat::CellData<int> *) patch->getPatchData(d_region_id).get())->getPointer();
			int* interior_i = ((pdat::CellData<int> *) patch->getPatchData(d_interior_i_id).get())->getPointer();
			int* interior_j = ((pdat::CellData<int> *) patch->getPatchData(d_interior_j_id).get())->getPointer();
			int* interior_k = ((pdat::CellData<int> *) patch->getPatchData(d_interior_k_id).get())->getPointer();
			int* interior = ((pdat::CellData<int> *) patch->getPatchData(d_interior_id).get())->getPointer();
			int* nonSync = ((pdat::CellData<int> *) patch->getPatchData(d_nonSync_id).get())->getPointer();
			const hier::Index boxfirst1 = patch->getBox().lower();
			const hier::Index boxlast1  = patch->getBox().upper();

			//Get delta spaces into an array. dx, dy, dz.
			const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
			const double* dx  = patch_geom->getDx();

			int ilast = boxlast1(0)-boxfirst1(0) + 1 + 2 * d_ghost_width;
			int jlast = boxlast1(1)-boxfirst1(1) + 1 + 2 * d_ghost_width;
			int klast = boxlast1(2)-boxfirst1(2) + 1 + 2 * d_ghost_width;
			if (floor(0.5 / dx[0]) - boxfirst1(0) + d_ghost_width <= boxlast1(0) - boxfirst1(0) + d_ghost_width && floor(1.5 / dx[0]) - boxfirst1(0) + d_ghost_width >= d_ghost_width && floor(0.5 / dx[1]) - boxfirst1(1) + d_ghost_width <= boxlast1(1) - boxfirst1(1) + d_ghost_width && floor(1.5 / dx[1]) - boxfirst1(1) + d_ghost_width >= d_ghost_width && floor(0.5 / dx[2]) - boxfirst1(2) + d_ghost_width <= boxlast1(2) - boxfirst1(2) + d_ghost_width && floor(1.5 / dx[2]) - boxfirst1(2) + d_ghost_width >= d_ghost_width) {
			if (floor(0.5 / dx[0]) - boxfirst1(0) + d_ghost_width > d_ghost_width) {
				iMapStartC = floor(0.5 / dx[0]) - boxfirst1(0) + d_ghost_width;
			} else {
				iMapStartC = 0;
			}
			if (floor(1.5 / dx[0]) - boxfirst1(0) + d_ghost_width < boxlast1(0) - boxfirst1(0) + d_ghost_width) {
				iMapEndC = floor(1.5 / dx[0]) - boxfirst1(0) + d_ghost_width;
			} else {
				iMapEndC = ilast;
			}
			if (floor(0.5 / dx[1]) - boxfirst1(1) + d_ghost_width > d_ghost_width) {
				jMapStartC = floor(0.5 / dx[1]) - boxfirst1(1) + d_ghost_width;
			} else {
				jMapStartC = 0;
			}
			if (floor(1.5 / dx[1]) - boxfirst1(1) + d_ghost_width < boxlast1(1) - boxfirst1(1) + d_ghost_width) {
				jMapEndC = floor(1.5 / dx[1]) - boxfirst1(1) + d_ghost_width;
			} else {
				jMapEndC = jlast;
			}
			if (floor(0.5 / dx[2]) - boxfirst1(2) + d_ghost_width > d_ghost_width) {
				kMapStartC = floor(0.5 / dx[2]) - boxfirst1(2) + d_ghost_width;
			} else {
				kMapStartC = 0;
			}
			if (floor(1.5 / dx[2]) - boxfirst1(2) + d_ghost_width < boxlast1(2) - boxfirst1(2) + d_ghost_width) {
				kMapEndC = floor(1.5 / dx[2]) - boxfirst1(2) + d_ghost_width;
			} else {
				kMapEndC = klast;
			}
			for (k = kMapStartC; k <= kMapEndC; k++) {
				for (j = jMapStartC; j <= jMapEndC; j++) {
					if (lessEq(patch_geom->getXLower()[0], -0.5) && greaterEq(patch_geom->getXUpper()[0], -0.5)) {
						vector(region, iMapStartC, j, k) = 2;
					}
					if (lessEq(patch_geom->getXLower()[0], 0.5) && greaterEq(patch_geom->getXUpper()[0], 0.5)) {
						vector(region, iMapEndC - 1, j, k) = 2;
					}
				}
			}
			for (k = kMapStartC; k <= kMapEndC; k++) {
				for (i = iMapStartC; i <= iMapEndC; i++) {
					if (lessEq(patch_geom->getXLower()[1], -0.5) && greaterEq(patch_geom->getXUpper()[1], -0.5)) {
						vector(region, i, jMapStartC, k) = 2;
					}
					if (lessEq(patch_geom->getXLower()[1], 0.5) && greaterEq(patch_geom->getXUpper()[1], 0.5)) {
						vector(region, i, jMapEndC - 1, k) = 2;
					}
				}
			}
			for (j = jMapStartC; j <= jMapEndC; j++) {
				for (i = iMapStartC; i <= iMapEndC; i++) {
					if (lessEq(patch_geom->getXLower()[2], -0.5) && greaterEq(patch_geom->getXUpper()[2], -0.5)) {
						vector(region, i, j, kMapStartC) = 2;
					}
					if (lessEq(patch_geom->getXLower()[2], 0.5) && greaterEq(patch_geom->getXUpper()[2], 0.5)) {
						vector(region, i, j, kMapEndC - 1) = 2;
					}
				}
			}
				//Check the stencil
				//Check stencil
				for (k = 0; k < klast; k++) {
					for (j = 0; j < jlast; j++) {
						for (i = 0; i < ilast; i++) {
							vector(interior_i, i, j, k) = 0;
							vector(interior_j, i, j, k) = 0;
							vector(interior_k, i, j, k) = 0;
						}
					}
				}
				for (k = 0; k < klast; k++) {
					for (j = 0; j < jlast; j++) {
						for (i = 0; i < ilast; i++) {
							if (vector(region, i, j, k) == 2) {
								setStencilLimits(patch, i, j, k, 2);
							}
						}
					}
				}
			}
		}
		d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);
		for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
			const std::shared_ptr< hier::Patch > patch = *p_it;
			int* region = ((pdat::CellData<int> *) patch->getPatchData(d_region_id).get())->getPointer();
			int* interior_i = ((pdat::CellData<int> *) patch->getPatchData(d_interior_i_id).get())->getPointer();
			int* interior_j = ((pdat::CellData<int> *) patch->getPatchData(d_interior_j_id).get())->getPointer();
			int* interior_k = ((pdat::CellData<int> *) patch->getPatchData(d_interior_k_id).get())->getPointer();
			int* interior = ((pdat::CellData<int> *) patch->getPatchData(d_interior_id).get())->getPointer();
			int* nonSync = ((pdat::CellData<int> *) patch->getPatchData(d_nonSync_id).get())->getPointer();
			const hier::Index boxfirst1 = patch->getBox().lower();
			const hier::Index boxlast1  = patch->getBox().upper();

			//Get delta spaces into an array. dx, dy, dz.
			const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
			const double* dx  = patch_geom->getDx();

			int ilast = boxlast1(0)-boxfirst1(0) + 1 + 2 * d_ghost_width;
			int jlast = boxlast1(1)-boxfirst1(1) + 1 + 2 * d_ghost_width;
			int klast = boxlast1(2)-boxfirst1(2) + 1 + 2 * d_ghost_width;
			for (k = 0; k < klast; k++) {
				for (j = 0; j < jlast; j++) {
					for (i = 0; i < ilast; i++) {
						if ((abs(vector(interior_i, i, j, k)) > 1 || abs(vector(interior_j, i, j, k)) > 1 || abs(vector(interior_k, i, j, k)) > 1) && (i < d_ghost_width || i >= ilast - d_ghost_width || j < d_ghost_width || j >= jlast - d_ghost_width || k < d_ghost_width || k >= klast - d_ghost_width)) {
							checkStencilCell(patch, i, j, k, 2);
						}
					}
				}
			}
			for (k = 0; k < klast; k++) {
				for (j = 0; j < jlast; j++) {
					for (i = 0; i < ilast; i++) {
						if (vector(interior_i, i, j, k) != 0 || vector(interior_j, i, j, k) != 0 || vector(interior_k, i, j, k) != 0) {
							vector(region, i, j, k) = 2;
						}
					}
				}
			}
		}
		d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);

		//Boundaries Mapping
		for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
			const std::shared_ptr< hier::Patch > patch = *p_it;
			int* region = ((pdat::CellData<int> *) patch->getPatchData(d_region_id).get())->getPointer();
			int* interior_i = ((pdat::CellData<int> *) patch->getPatchData(d_interior_i_id).get())->getPointer();
			int* interior_j = ((pdat::CellData<int> *) patch->getPatchData(d_interior_j_id).get())->getPointer();
			int* interior_k = ((pdat::CellData<int> *) patch->getPatchData(d_interior_k_id).get())->getPointer();
			int* interior = ((pdat::CellData<int> *) patch->getPatchData(d_interior_id).get())->getPointer();
			int* nonSync = ((pdat::CellData<int> *) patch->getPatchData(d_nonSync_id).get())->getPointer();
			const hier::Index boxfirst1 = patch->getBox().lower();
			const hier::Index boxlast1  = patch->getBox().upper();

			//Get delta spaces into an array. dx, dy, dz.
			const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
			const double* dx  = patch_geom->getDx();

			int ilast = boxlast1(0)-boxfirst1(0) + 1 + 2 * d_ghost_width;
			int jlast = boxlast1(1)-boxfirst1(1) + 1 + 2 * d_ghost_width;
			int klast = boxlast1(2)-boxfirst1(2) + 1 + 2 * d_ghost_width;
			//z-Upper
			if (patch->getPatchGeometry()->getTouchesRegularBoundary(2, 1)) {
				for (k = klast - d_ghost_width; k < klast; k++) {
					for (j = 0; j < jlast; j++) {
						for (i = 0; i < ilast; i++) {
							vector(region, i, j, k) = -6;
						}
					}
				}
			}
			//z-Lower
			if (patch->getPatchGeometry()->getTouchesRegularBoundary(2, 0)) {
				for (k = 0; k < d_ghost_width; k++) {
					for (j = 0; j < jlast; j++) {
						for (i = 0; i < ilast; i++) {
							vector(region, i, j, k) = -5;
						}
					}
				}
			}
			//y-Upper
			if (patch->getPatchGeometry()->getTouchesRegularBoundary(1, 1)) {
				for (k = 0; k < klast; k++) {
					for (j = jlast - d_ghost_width; j < jlast; j++) {
						for (i = 0; i < ilast; i++) {
							vector(region, i, j, k) = -4;
						}
					}
				}
			}
			//y-Lower
			if (patch->getPatchGeometry()->getTouchesRegularBoundary(1, 0)) {
				for (k = 0; k < klast; k++) {
					for (j = 0; j < d_ghost_width; j++) {
						for (i = 0; i < ilast; i++) {
							vector(region, i, j, k) = -3;
						}
					}
				}
			}
			//x-Upper
			if (patch->getPatchGeometry()->getTouchesRegularBoundary(0, 1)) {
				for (k = 0; k < klast; k++) {
					for (j = 0; j < jlast; j++) {
						for (i = ilast - d_ghost_width; i < ilast; i++) {
							vector(region, i, j, k) = -2;
						}
					}
				}
			}
			//x-Lower
			if (patch->getPatchGeometry()->getTouchesRegularBoundary(0, 0)) {
				for (k = 0; k < klast; k++) {
					for (j = 0; j < jlast; j++) {
						for (i = 0; i < d_ghost_width; i++) {
							vector(region, i, j, k) = -1;
						}
					}
				}
			}
		}
		d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);


		//Particle mapping
		for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
			const std::shared_ptr< hier::Patch > patch = *p_it;
			int* seg1 = ((pdat::CellData<int> *) patch->getPatchData(d_seg1_id).get())->getPointer();
			int* segL1 = ((pdat::CellData<int> *) patch->getPatchData(d_segL1_id).get())->getPointer();
			int* seg3 = ((pdat::CellData<int> *) patch->getPatchData(d_seg3_id).get())->getPointer();
			int* segL3 = ((pdat::CellData<int> *) patch->getPatchData(d_segL3_id).get())->getPointer();
			int* segxLower = ((pdat::CellData<int> *) patch->getPatchData(d_segxLower_id).get())->getPointer();
			int* segLxLower = ((pdat::CellData<int> *) patch->getPatchData(d_segLxLower_id).get())->getPointer();
			int* segxUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segxUpper_id).get())->getPointer();
			int* segLxUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segLxUpper_id).get())->getPointer();
			int* segyLower = ((pdat::CellData<int> *) patch->getPatchData(d_segyLower_id).get())->getPointer();
			int* segLyLower = ((pdat::CellData<int> *) patch->getPatchData(d_segLyLower_id).get())->getPointer();
			int* segyUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segyUpper_id).get())->getPointer();
			int* segLyUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segLyUpper_id).get())->getPointer();
			int* segzLower = ((pdat::CellData<int> *) patch->getPatchData(d_segzLower_id).get())->getPointer();
			int* segLzLower = ((pdat::CellData<int> *) patch->getPatchData(d_segLzLower_id).get())->getPointer();
			int* segzUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segzUpper_id).get())->getPointer();
			int* segLzUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segLzUpper_id).get())->getPointer();
			int* region = ((pdat::CellData<int> *) patch->getPatchData(d_region_id).get())->getPointer();
			int* interior_i = ((pdat::CellData<int> *) patch->getPatchData(d_interior_i_id).get())->getPointer();
			int* interior_j = ((pdat::CellData<int> *) patch->getPatchData(d_interior_j_id).get())->getPointer();
			int* interior_k = ((pdat::CellData<int> *) patch->getPatchData(d_interior_k_id).get())->getPointer();
			int* interior = ((pdat::CellData<int> *) patch->getPatchData(d_interior_id).get())->getPointer();
			int* nonSync = ((pdat::CellData<int> *) patch->getPatchData(d_nonSync_id).get())->getPointer();
			std::shared_ptr< pdat::IndexData<Particles, pdat::CellGeometry > > problemVariable(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles, pdat::CellGeometry >, hier::PatchData>(patch->getPatchData(d_problemVariable_id)));
			std::shared_ptr< pdat::IndexData<NonSyncs, pdat::CellGeometry > > nonSyncVariable(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< NonSyncs, pdat::CellGeometry >, hier::PatchData>(patch->getPatchData(d_nonSyncP_id)));
			const hier::Index boxfirst = problemVariable->getGhostBox().lower();
			const hier::Index boxlast  = problemVariable->getGhostBox().upper();
			const hier::Index boxfirst1 = patch->getBox().lower();
			const hier::Index boxlast1  = patch->getBox().upper();

			//Get delta spaces into an array. dx, dy, dz.
			const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
			const double* dx  = patch_geom->getDx();

			int ilast = boxlast1(0)-boxfirst1(0) + 1 + 2 * d_ghost_width;
			int jlast = boxlast1(1)-boxfirst1(1) + 1 + 2 * d_ghost_width;
			int klast = boxlast1(2)-boxfirst1(2) + 1 + 2 * d_ghost_width;
			//Give particles support
			for (k = boxfirst(2); k <= boxlast(2); k++) {
				for (j = boxfirst(1); j <= boxlast(1); j++) {
					for (i = boxfirst(0); i <= boxlast(0); i++) {
						vector(seg3, i - boxfirst(0), j - boxfirst(1), k - boxfirst(2)) = -1;
						vector(segL3, i - boxfirst(0), j - boxfirst(1), k - boxfirst(2)) = -1;
						vector(seg1, i - boxfirst(0), j - boxfirst(1), k - boxfirst(2)) = -1;
						vector(segL1, i - boxfirst(0), j - boxfirst(1), k - boxfirst(2)) = -1;
						vector(segxLower, i - boxfirst(0), j - boxfirst(1), k - boxfirst(2)) = -1;
						vector(segLxLower, i - boxfirst(0), j - boxfirst(1), k - boxfirst(2)) = -1;
						vector(segxUpper, i - boxfirst(0), j - boxfirst(1), k - boxfirst(2)) = -1;
						vector(segLxUpper, i - boxfirst(0), j - boxfirst(1), k - boxfirst(2)) = -1;
						vector(segyLower, i - boxfirst(0), j - boxfirst(1), k - boxfirst(2)) = -1;
						vector(segLyLower, i - boxfirst(0), j - boxfirst(1), k - boxfirst(2)) = -1;
						vector(segyUpper, i - boxfirst(0), j - boxfirst(1), k - boxfirst(2)) = -1;
						vector(segLyUpper, i - boxfirst(0), j - boxfirst(1), k - boxfirst(2)) = -1;
						vector(segzLower, i - boxfirst(0), j - boxfirst(1), k - boxfirst(2)) = -1;
						vector(segLzLower, i - boxfirst(0), j - boxfirst(1), k - boxfirst(2)) = -1;
						vector(segzUpper, i - boxfirst(0), j - boxfirst(1), k - boxfirst(2)) = -1;
						vector(segLzUpper, i - boxfirst(0), j - boxfirst(1), k - boxfirst(2)) = -1;
						hier::Index idx(i, j, k);
						if (!problemVariable->isElement(idx)) {
							Particles* part = new Particles();
							problemVariable->addItemPointer(idx, part);
						}
						if (!nonSyncVariable->isElement(idx)) {
							NonSyncs* nonSyncPart = new NonSyncs();
							nonSyncVariable->addItemPointer(idx, nonSyncPart);
						}
					}
				}
			}

			//Region: cube2I
			iMapEnd = d_grid_geometry->getXUpper()[0] + d_ghost_width * dx[0];
			jMapEnd = d_grid_geometry->getXUpper()[1] + d_ghost_width * dx[1];
			kMapStart = d_grid_geometry->getXLower()[2] + particleSeparation_z/2.0 - (floor(0.5 + d_ghost_width * (dx[2]/particleSeparation_z)))*particleSeparation_z;
			kMapEnd = d_grid_geometry->getXUpper()[2] + d_ghost_width * dx[2];
			for (int count_k = 0; kMapStart + count_k * particleSeparation_z < kMapEnd; count_k++) {
				if (particleDistribution.compare("STAGGERED") == 0 && isEven(count_k)) {
					iMapStart = d_grid_geometry->getXLower()[0] - (floor(d_ghost_width * (dx[0]/particleSeparation_x)))*particleSeparation_x;
					jMapStart = d_grid_geometry->getXLower()[1] - (floor(d_ghost_width * (dx[1]/particleSeparation_y)))*particleSeparation_y;
				} else {
					iMapStart = d_grid_geometry->getXLower()[0] + particleSeparation_x/2.0 - (floor(0.5 + d_ghost_width * (dx[0]/particleSeparation_x)))*particleSeparation_x;
					jMapStart = d_grid_geometry->getXLower()[1] + particleSeparation_y/2.0 - (floor(0.5 + d_ghost_width * (dx[1]/particleSeparation_y)))*particleSeparation_y;
				}
				for (int count_j = 0; jMapStart + count_j * particleSeparation_y < jMapEnd; count_j++) {
					for (int count_i = 0; iMapStart + count_i * particleSeparation_x < iMapEnd; count_i++) {
						position[0] = iMapStart + count_i * particleSeparation_x;
						i = floor((position[0] - d_grid_geometry->getXLower()[0])/dx[0]);
						position[1] = jMapStart + count_j * particleSeparation_y;
						j = floor((position[1] - d_grid_geometry->getXLower()[1])/dx[1]);
						position[2] = kMapStart + count_k * particleSeparation_z;
						k = floor((position[2] - d_grid_geometry->getXLower()[2])/dx[2]);
						if (i >= boxfirst(0) && i <= boxlast(0) && j >= boxfirst(1) && j <= boxlast(1) && k >= boxfirst(2) && k <= boxlast(2)) {
							hier::Index idx(i, j, k);
							Particles* part = problemVariable->getItem(idx);
							Particle* particle = new Particle(0, position, 3);
							Particle* oldParticle = part->overlaps(*particle);
							if (oldParticle != NULL) {
								part->deleteParticle(*oldParticle);
							}
							part->addParticle(*particle);
							delete particle;
						}
					}
				}
			}
			//Region: cubeI
			iMapEnd = d_grid_geometry->getXUpper()[0] + d_ghost_width * dx[0];
			jMapEnd = d_grid_geometry->getXUpper()[1] + d_ghost_width * dx[1];
			kMapStart = d_grid_geometry->getXLower()[2] + particleSeparation_z/2.0 - (floor(0.5 + d_ghost_width * (dx[2]/particleSeparation_z)))*particleSeparation_z;
			kMapEnd = d_grid_geometry->getXUpper()[2] + d_ghost_width * dx[2];
			for (int count_k = 0; kMapStart + count_k * particleSeparation_z < kMapEnd; count_k++) {
				if (particleDistribution.compare("STAGGERED") == 0 && isEven(count_k)) {
					iMapStart = d_grid_geometry->getXLower()[0] - (floor(d_ghost_width * (dx[0]/particleSeparation_x)))*particleSeparation_x;
					jMapStart = d_grid_geometry->getXLower()[1] - (floor(d_ghost_width * (dx[1]/particleSeparation_y)))*particleSeparation_y;
				} else {
					iMapStart = d_grid_geometry->getXLower()[0] + particleSeparation_x/2.0 - (floor(0.5 + d_ghost_width * (dx[0]/particleSeparation_x)))*particleSeparation_x;
					jMapStart = d_grid_geometry->getXLower()[1] + particleSeparation_y/2.0 - (floor(0.5 + d_ghost_width * (dx[1]/particleSeparation_y)))*particleSeparation_y;
				}
				for (int count_j = 0; jMapStart + count_j * particleSeparation_y < jMapEnd; count_j++) {
					for (int count_i = 0; iMapStart + count_i * particleSeparation_x < iMapEnd; count_i++) {
						position[0] = iMapStart + count_i * particleSeparation_x;
						i = floor((position[0] - d_grid_geometry->getXLower()[0])/dx[0]);
						position[1] = jMapStart + count_j * particleSeparation_y;
						j = floor((position[1] - d_grid_geometry->getXLower()[1])/dx[1]);
						position[2] = kMapStart + count_k * particleSeparation_z;
						k = floor((position[2] - d_grid_geometry->getXLower()[2])/dx[2]);
						if (i >= boxfirst(0) && i <= boxlast(0) && j >= boxfirst(1) && j <= boxlast(1) && k >= boxfirst(2) && k <= boxlast(2) && greaterEq(position[0], -0.5) && lessEq(position[0], 0.5) && greaterEq(position[1], -0.5) && lessEq(position[1], 0.5) && greaterEq(position[2], -0.5) && lessEq(position[2], 0.5)) {
							hier::Index idx(i, j, k);
							Particles* part = problemVariable->getItem(idx);
							Particle* particle = new Particle(0, position, 2);
							Particle* oldParticle = part->overlaps(*particle);
							if (oldParticle != NULL) {
								part->deleteParticle(*oldParticle);
							}
							part->addParticle(*particle);
							delete particle;
						}
					}
				}
			}
			//Region: cubeS
			iMapEnd = d_grid_geometry->getXUpper()[0] + d_ghost_width * dx[0];
			jMapEnd = d_grid_geometry->getXUpper()[1] + d_ghost_width * dx[1];
			kMapStart = d_grid_geometry->getXLower()[2] + particleSeparation_z/2.0 - (floor(0.5 + d_ghost_width * (dx[2]/particleSeparation_z)))*particleSeparation_z;
			kMapEnd = d_grid_geometry->getXUpper()[2] + d_ghost_width * dx[2];
			for (int count_k = 0; kMapStart + count_k * particleSeparation_z < kMapEnd; count_k++) {
				if (particleDistribution.compare("STAGGERED") == 0 && isEven(count_k)) {
					iMapStart = d_grid_geometry->getXLower()[0] - (floor(d_ghost_width * (dx[0]/particleSeparation_x)))*particleSeparation_x;
					jMapStart = d_grid_geometry->getXLower()[1] - (floor(d_ghost_width * (dx[1]/particleSeparation_y)))*particleSeparation_y;
				} else {
					iMapStart = d_grid_geometry->getXLower()[0] + particleSeparation_x/2.0 - (floor(0.5 + d_ghost_width * (dx[0]/particleSeparation_x)))*particleSeparation_x;
					jMapStart = d_grid_geometry->getXLower()[1] + particleSeparation_y/2.0 - (floor(0.5 + d_ghost_width * (dx[1]/particleSeparation_y)))*particleSeparation_y;
				}
				for (int count_j = 0; jMapStart + count_j * particleSeparation_y < jMapEnd; count_j++) {
					if (lessEq(patch_geom->getXLower()[0], -0.5) && greaterEq(patch_geom->getXUpper()[0], -0.5)) {
					if (particleDistribution.compare("STAGGERED") == 0 && isEven(count_k)) {
						position[0] = iMapStart + (floor((-0.5 - -1.0)/ particleSeparation_x) + floor((d_ghost_width*(dx[0]/particleSeparation_x)))) * particleSeparation_x;
					} else {
						position[0] = iMapStart + (floor((-0.5 - -1.0)/ particleSeparation_x) + floor(0.5 + d_ghost_width*(dx[0]/particleSeparation_x))) * particleSeparation_x;
					}
					i = floor((position[0] - d_grid_geometry->getXLower()[0])/dx[0]);
					position[1] = jMapStart + count_j * particleSeparation_y;
					j = floor((position[1] - d_grid_geometry->getXLower()[1])/dx[1]);
					position[2] = kMapStart + count_k * particleSeparation_z;
					k = floor((position[2] - d_grid_geometry->getXLower()[2])/dx[2]);
						if (i >= boxfirst(0) && i <= boxlast(0) && j >= boxfirst(1) && j <= boxlast(1) && k >= boxfirst(2) && k <= boxlast(2) && greaterEq(position[1], -0.5) && lessEq(position[1], 0.5) && greaterEq(position[2], -0.5) && lessEq(position[2], 0.5)) {
							hier::Index idx(i, j, k);
							Particles* part = problemVariable->getItem(idx);
							Particle* particle = new Particle(0, position, 3);
							Particle* oldParticle = part->overlaps(*particle);
							if (oldParticle != NULL) {
								part->deleteParticle(*oldParticle);
							}
							part->addParticle(*particle);
							delete particle;
						}
					}
					if (lessEq(patch_geom->getXLower()[0], 0.5) && greaterEq(patch_geom->getXUpper()[0], 0.5)) {
					if (particleDistribution.compare("STAGGERED") == 0 && isEven(count_k)) {
						position[0] = iMapStart + (floor((0.5 - -1.0)/ particleSeparation_x) + floor((d_ghost_width*(dx[0]/particleSeparation_x)))) * particleSeparation_x;
					} else {
						position[0] = iMapStart + (floor((0.5 - -1.0)/ particleSeparation_x) + floor(0.5 + d_ghost_width*(dx[0]/particleSeparation_x))) * particleSeparation_x;
					}
					i = floor((position[0] - d_grid_geometry->getXLower()[0])/dx[0]);
					position[1] = jMapStart + count_j * particleSeparation_y;
					j = floor((position[1] - d_grid_geometry->getXLower()[1])/dx[1]);
					position[2] = kMapStart + count_k * particleSeparation_z;
					k = floor((position[2] - d_grid_geometry->getXLower()[2])/dx[2]);
						if (i >= boxfirst(0) && i <= boxlast(0) && j >= boxfirst(1) && j <= boxlast(1) && k >= boxfirst(2) && k <= boxlast(2) && greaterEq(position[1], -0.5) && lessEq(position[1], 0.5) && greaterEq(position[2], -0.5) && lessEq(position[2], 0.5)) {
							hier::Index idx(i, j, k);
							Particles* part = problemVariable->getItem(idx);
							Particle* particle = new Particle(0, position, 3);
							Particle* oldParticle = part->overlaps(*particle);
							if (oldParticle != NULL) {
								part->deleteParticle(*oldParticle);
							}
							part->addParticle(*particle);
							delete particle;
						}
					}
				}
			}
			for (int count_k = 0; kMapStart + count_k * particleSeparation_z < kMapEnd; count_k++) {
				if (particleDistribution.compare("STAGGERED") == 0 && isEven(count_k)) {
					iMapStart = d_grid_geometry->getXLower()[0] - (floor(d_ghost_width * (dx[0]/particleSeparation_x)))*particleSeparation_x;
					jMapStart = d_grid_geometry->getXLower()[1] - (floor(d_ghost_width * (dx[1]/particleSeparation_y)))*particleSeparation_y;
				} else {
					iMapStart = d_grid_geometry->getXLower()[0] + particleSeparation_x/2.0 - (floor(0.5 + d_ghost_width * (dx[0]/particleSeparation_x)))*particleSeparation_x;
					jMapStart = d_grid_geometry->getXLower()[1] + particleSeparation_y/2.0 - (floor(0.5 + d_ghost_width * (dx[1]/particleSeparation_y)))*particleSeparation_y;
				}
				for (int count_i = 0; iMapStart + count_i * particleSeparation_x < iMapEnd; count_i++) {
					if (lessEq(patch_geom->getXLower()[1], -0.5) && greaterEq(patch_geom->getXUpper()[1], -0.5)) {
					position[0] = iMapStart + count_i * particleSeparation_x;
					i = floor((position[0] - d_grid_geometry->getXLower()[0])/dx[0]);
					if (particleDistribution.compare("STAGGERED") == 0 && isEven(count_k)) {
						position[1] = jMapStart + (floor((-0.5 - -1.0)/ particleSeparation_y) + floor((d_ghost_width*(dx[1]/particleSeparation_y)))) * particleSeparation_y;
					} else {
						position[1] = jMapStart + (floor((-0.5 - -1.0)/ particleSeparation_y) + floor(0.5 + d_ghost_width*(dx[1]/particleSeparation_y))) * particleSeparation_y;
					}
					j = floor((position[1] - d_grid_geometry->getXLower()[1])/dx[1]);
					position[2] = kMapStart + count_k * particleSeparation_z;
					k = floor((position[2] - d_grid_geometry->getXLower()[2])/dx[2]);
						if (i >= boxfirst(0) && i <= boxlast(0) && j >= boxfirst(1) && j <= boxlast(1) && k >= boxfirst(2) && k <= boxlast(2) && greaterEq(position[0], -0.5) && lessEq(position[0], 0.5) && greaterEq(position[2], -0.5) && lessEq(position[2], 0.5)) {
							hier::Index idx(i, j, k);
							Particles* part = problemVariable->getItem(idx);
							Particle* particle = new Particle(0, position, 3);
							Particle* oldParticle = part->overlaps(*particle);
							if (oldParticle != NULL) {
								part->deleteParticle(*oldParticle);
							}
							part->addParticle(*particle);
							delete particle;
						}
					}
					if (lessEq(patch_geom->getXLower()[1], 0.5) && greaterEq(patch_geom->getXUpper()[1], 0.5)) {
					position[0] = iMapStart + count_i * particleSeparation_x;
					i = floor((position[0] - d_grid_geometry->getXLower()[0])/dx[0]);
					if (particleDistribution.compare("STAGGERED") == 0 && isEven(count_k)) {
						position[1] = jMapStart + (floor((0.5 - -1.0)/ particleSeparation_y) + floor((d_ghost_width*(dx[1]/particleSeparation_y)))) * particleSeparation_y;
					} else {
						position[1] = jMapStart + (floor((0.5 - -1.0)/ particleSeparation_y) + floor(0.5 + d_ghost_width*(dx[1]/particleSeparation_y))) * particleSeparation_y;
					}
					j = floor((position[1] - d_grid_geometry->getXLower()[1])/dx[1]);
					position[2] = kMapStart + count_k * particleSeparation_z;
					k = floor((position[2] - d_grid_geometry->getXLower()[2])/dx[2]);
						if (i >= boxfirst(0) && i <= boxlast(0) && j >= boxfirst(1) && j <= boxlast(1) && k >= boxfirst(2) && k <= boxlast(2) && greaterEq(position[0], -0.5) && lessEq(position[0], 0.5) && greaterEq(position[2], -0.5) && lessEq(position[2], 0.5)) {
							hier::Index idx(i, j, k);
							Particles* part = problemVariable->getItem(idx);
							Particle* particle = new Particle(0, position, 3);
							Particle* oldParticle = part->overlaps(*particle);
							if (oldParticle != NULL) {
								part->deleteParticle(*oldParticle);
							}
							part->addParticle(*particle);
							delete particle;
						}
					}
				}
			}
			if (particleDistribution.compare("STAGGERED") == 0 && isEven(0)) {
				iMapStart = d_grid_geometry->getXLower()[0] - (floor(d_ghost_width * (dx[0]/particleSeparation_x)))*particleSeparation_x;
				jMapStart = d_grid_geometry->getXLower()[1] - (floor(d_ghost_width * (dx[1]/particleSeparation_y)))*particleSeparation_y;
			} else {
				iMapStart = d_grid_geometry->getXLower()[0] + particleSeparation_x/2.0 - (floor(0.5 + d_ghost_width * (dx[0]/particleSeparation_x)))*particleSeparation_x;
				jMapStart = d_grid_geometry->getXLower()[1] + particleSeparation_y/2.0 - (floor(0.5 + d_ghost_width * (dx[1]/particleSeparation_y)))*particleSeparation_y;
			}
			for (int count_j = 0; jMapStart + count_j * particleSeparation_y < jMapEnd; count_j++) {
				for (int count_i = 0; iMapStart + count_i * particleSeparation_x < iMapEnd; count_i++) {
					if (lessEq(patch_geom->getXLower()[2], -0.5) && greaterEq(patch_geom->getXUpper()[2], -0.5)) {
					position[0] = iMapStart + count_i * particleSeparation_x;
					i = floor((position[0] - d_grid_geometry->getXLower()[0])/dx[0]);
					position[1] = jMapStart + count_j * particleSeparation_y;
					j = floor((position[1] - d_grid_geometry->getXLower()[1])/dx[1]);
					position[2] = kMapStart + (floor((-0.5 - -1.0)/ particleSeparation_z) + floor(0.5 + d_ghost_width*(dx[2]/particleSeparation_z))) * particleSeparation_z;
					k = floor((position[2] - d_grid_geometry->getXLower()[2])/dx[2]);
						if (i >= boxfirst(0) && i <= boxlast(0) && j >= boxfirst(1) && j <= boxlast(1) && k >= boxfirst(2) && k <= boxlast(2) && greaterEq(position[0], -0.5) && lessEq(position[0], 0.5) && greaterEq(position[1], -0.5) && lessEq(position[1], 0.5)) {
							hier::Index idx(i, j, k);
							Particles* part = problemVariable->getItem(idx);
							Particle* particle = new Particle(0, position, 3);
							Particle* oldParticle = part->overlaps(*particle);
							if (oldParticle != NULL) {
								part->deleteParticle(*oldParticle);
							}
							part->addParticle(*particle);
							delete particle;
						}
					}
				}
			}
			if (particleDistribution.compare("STAGGERED") == 0 && isEven((int)(floor((0.5 - d_grid_geometry->getXLower()[2])/ particleSeparation_z) + floor(0.5 + d_ghost_width*(dx[2]/particleSeparation_z))))) {
				iMapStart = d_grid_geometry->getXLower()[0] - (floor(d_ghost_width * (dx[0]/particleSeparation_x)))*particleSeparation_x;
				jMapStart = d_grid_geometry->getXLower()[1] - (floor(d_ghost_width * (dx[1]/particleSeparation_y)))*particleSeparation_y;
			} else {
				iMapStart = d_grid_geometry->getXLower()[0] + particleSeparation_x/2.0 - (floor(0.5 + d_ghost_width * (dx[0]/particleSeparation_x)))*particleSeparation_x;
				jMapStart = d_grid_geometry->getXLower()[1] + particleSeparation_y/2.0 - (floor(0.5 + d_ghost_width * (dx[1]/particleSeparation_y)))*particleSeparation_y;
			}
			for (int count_j = 0; jMapStart + count_j * particleSeparation_y < jMapEnd; count_j++) {
				for (int count_i = 0; iMapStart + count_i * particleSeparation_x < iMapEnd; count_i++) {
					if (lessEq(patch_geom->getXLower()[2], 0.5) && greaterEq(patch_geom->getXUpper()[2], 0.5)) {
					position[0] = iMapStart + count_i * particleSeparation_x;
					i = floor((position[0] - d_grid_geometry->getXLower()[0])/dx[0]);
					position[1] = jMapStart + count_j * particleSeparation_y;
					j = floor((position[1] - d_grid_geometry->getXLower()[1])/dx[1]);
					position[2] = kMapStart + (floor((0.5 - -1.0)/ particleSeparation_z) + floor(0.5 + d_ghost_width*(dx[2]/particleSeparation_z))) * particleSeparation_z;
					k = floor((position[2] - d_grid_geometry->getXLower()[2])/dx[2]);
						if (i >= boxfirst(0) && i <= boxlast(0) && j >= boxfirst(1) && j <= boxlast(1) && k >= boxfirst(2) && k <= boxlast(2) && greaterEq(position[0], -0.5) && lessEq(position[0], 0.5) && greaterEq(position[1], -0.5) && lessEq(position[1], 0.5)) {
							hier::Index idx(i, j, k);
							Particles* part = problemVariable->getItem(idx);
							Particle* particle = new Particle(0, position, 3);
							Particle* oldParticle = part->overlaps(*particle);
							if (oldParticle != NULL) {
								part->deleteParticle(*oldParticle);
							}
							part->addParticle(*particle);
							delete particle;
						}
					}
				}
			}
			if (mapStencil > 1) {
				//Check the stencil
				for (int count_k = 0; kMapStart + count_k * particleSeparation_z < kMapEnd; count_k++) {
					if (particleDistribution.compare("STAGGERED") == 0 && isEven(count_k)) {
						iMapStart = d_grid_geometry->getXLower()[0] - (floor(d_ghost_width * (dx[0]/particleSeparation_x)))*particleSeparation_x;
						jMapStart = d_grid_geometry->getXLower()[1] - (floor(d_ghost_width * (dx[1]/particleSeparation_y)))*particleSeparation_y;
					} else {
						iMapStart = d_grid_geometry->getXLower()[0] + particleSeparation_x/2.0 - (floor(0.5 + d_ghost_width * (dx[0]/particleSeparation_x)))*particleSeparation_x;
						jMapStart = d_grid_geometry->getXLower()[1] + particleSeparation_y/2.0 - (floor(0.5 + d_ghost_width * (dx[1]/particleSeparation_y)))*particleSeparation_y;
					}
					for (int count_j = 0; jMapStart + count_j * particleSeparation_y < jMapEnd; count_j++) {
						if (floor((jMapStart + count_j * particleSeparation_y - d_grid_geometry->getXLower()[1])/dx[1]) >= boxfirst(1) && floor((jMapStart + count_j * particleSeparation_y - d_grid_geometry->getXLower()[1])/dx[1]) <= boxlast(1) && floor((kMapStart + count_k * particleSeparation_z - d_grid_geometry->getXLower()[2])/dx[2]) >= boxfirst(2) && floor((kMapStart + count_k * particleSeparation_z - d_grid_geometry->getXLower()[2])/dx[2]) <= boxlast(2)) {
							position[1] = jMapStart + count_j * particleSeparation_y;
							position[2] = kMapStart + count_k * particleSeparation_z;
							if (lessEq(patch_geom->getXLower()[0], -0.5) && greaterEq(patch_geom->getXUpper()[0], -0.5) && greaterEq(position[1], -0.5) && lessEq(position[1], 0.5) && greaterEq(position[2], -0.5) && lessEq(position[2], 0.5)) {
								if (particleDistribution.compare("STAGGERED") == 0 && isEven(count_k)) {
									checkStencil(patch, floor((-0.5 - -1.0)/ particleSeparation_x) + floor((d_ghost_width*(dx[0]/particleSeparation_x))), 1, count_j, 1, count_k, 1, 3);
								} else {
									checkStencil(patch, floor((-0.5 - -1.0)/ particleSeparation_x) + floor(0.5 + d_ghost_width*(dx[0]/particleSeparation_x)), 1, count_j, 1, count_k, 1, 3);
								}
							}
							if (lessEq(patch_geom->getXLower()[0], 0.5) && greaterEq(patch_geom->getXUpper()[0], 0.5) && greaterEq(position[1], -0.5) && lessEq(position[1], 0.5) && greaterEq(position[2], -0.5) && lessEq(position[2], 0.5)) {
								if (particleDistribution.compare("STAGGERED") == 0 && isEven(count_k)) {
									checkStencil(patch, floor((0.5 - -1.0)/ particleSeparation_x) + floor((d_ghost_width*(dx[0]/particleSeparation_x))), 2, count_j, 1, count_k, 1, 3);
								} else {
									checkStencil(patch, floor((0.5 - -1.0)/ particleSeparation_x) + floor(0.5 + d_ghost_width*(dx[0]/particleSeparation_x)), 2, count_j, 1, count_k, 1, 3);
								}
							}
						}
					}
				}
				for (int count_k = 0; kMapStart + count_k * particleSeparation_z < kMapEnd; count_k++) {
					if (particleDistribution.compare("STAGGERED") == 0 && isEven(count_k)) {
						iMapStart = d_grid_geometry->getXLower()[0] - (floor(d_ghost_width * (dx[0]/particleSeparation_x)))*particleSeparation_x;
						jMapStart = d_grid_geometry->getXLower()[1] - (floor(d_ghost_width * (dx[1]/particleSeparation_y)))*particleSeparation_y;
					} else {
						iMapStart = d_grid_geometry->getXLower()[0] + particleSeparation_x/2.0 - (floor(0.5 + d_ghost_width * (dx[0]/particleSeparation_x)))*particleSeparation_x;
						jMapStart = d_grid_geometry->getXLower()[1] + particleSeparation_y/2.0 - (floor(0.5 + d_ghost_width * (dx[1]/particleSeparation_y)))*particleSeparation_y;
					}
					for (int count_i = 0; iMapStart + count_i * particleSeparation_x < iMapEnd; count_i++) {
						if (floor((iMapStart + count_i * particleSeparation_x - d_grid_geometry->getXLower()[0])/dx[0]) >= boxfirst(0) && floor((iMapStart + count_i * particleSeparation_x - d_grid_geometry->getXLower()[0])/dx[0]) <= boxlast(0) && floor((kMapStart + count_k * particleSeparation_z - d_grid_geometry->getXLower()[2])/dx[2]) >= boxfirst(2) && floor((kMapStart + count_k * particleSeparation_z - d_grid_geometry->getXLower()[2])/dx[2]) <= boxlast(2)) {
							position[0] = iMapStart + count_i * particleSeparation_x;
							position[2] = kMapStart + count_k * particleSeparation_z;
							if (lessEq(patch_geom->getXLower()[1], -0.5) && greaterEq(patch_geom->getXUpper()[1], -0.5) && greaterEq(position[0], -0.5) && lessEq(position[0], 0.5) && greaterEq(position[2], -0.5) && lessEq(position[2], 0.5)) {
								if (particleDistribution.compare("STAGGERED") == 0 && isEven(count_k)) {
									checkStencil(patch, count_i, 1, floor((-0.5 - -1.0)/ particleSeparation_y) + floor((d_ghost_width*(dx[1]/particleSeparation_y))), 1, count_k, 1, 3);
								} else {
									checkStencil(patch, count_i, 1, floor((-0.5 - -1.0)/ particleSeparation_y) + floor(0.5 + d_ghost_width*(dx[1]/particleSeparation_y)), 1, count_k, 1, 3);
								}
							}
							if (lessEq(patch_geom->getXLower()[1], 0.5) && greaterEq(patch_geom->getXUpper()[1], 0.5) && greaterEq(position[0], -0.5) && lessEq(position[0], 0.5) && greaterEq(position[2], -0.5) && lessEq(position[2], 0.5)) {
								if (particleDistribution.compare("STAGGERED") == 0 && isEven(count_k)) {
									checkStencil(patch, count_i, 1, floor((0.5 - -1.0)/ particleSeparation_y) + floor((d_ghost_width*(dx[1]/particleSeparation_y))), 2, count_k, 1, 3);
								} else {
									checkStencil(patch, count_i, 1, floor((0.5 - -1.0)/ particleSeparation_y) + floor(0.5 + d_ghost_width*(dx[1]/particleSeparation_y)), 2, count_k, 1, 3);
								}
							}
						}
					}
				}
				if (particleDistribution.compare("STAGGERED") == 0 && isEven(0)) {
					iMapStart = d_grid_geometry->getXLower()[0] - (floor(d_ghost_width * (dx[0]/particleSeparation_x)))*particleSeparation_x;
					jMapStart = d_grid_geometry->getXLower()[1] - (floor(d_ghost_width * (dx[1]/particleSeparation_y)))*particleSeparation_y;
				} else {
					iMapStart = d_grid_geometry->getXLower()[0] + particleSeparation_x/2.0 - (floor(0.5 + d_ghost_width * (dx[0]/particleSeparation_x)))*particleSeparation_x;
					jMapStart = d_grid_geometry->getXLower()[1] + particleSeparation_y/2.0 - (floor(0.5 + d_ghost_width * (dx[1]/particleSeparation_y)))*particleSeparation_y;
				}
				for (int count_j = 0; jMapStart + count_j * particleSeparation_y < jMapEnd; count_j++) {
					for (int count_i = 0; iMapStart + count_i * particleSeparation_x < iMapEnd; count_i++) {
						if (floor((iMapStart + count_i * particleSeparation_x - d_grid_geometry->getXLower()[0])/dx[0]) >= boxfirst(0) && floor((iMapStart + count_i * particleSeparation_x - d_grid_geometry->getXLower()[0])/dx[0]) <= boxlast(0) && floor((jMapStart + count_j * particleSeparation_y - d_grid_geometry->getXLower()[1])/dx[1]) >= boxfirst(1) && floor((jMapStart + count_j * particleSeparation_y - d_grid_geometry->getXLower()[1])/dx[1]) <= boxlast(1)) {
							position[0] = iMapStart + count_i * particleSeparation_x;
							position[1] = jMapStart + count_j * particleSeparation_y;
							if (lessEq(patch_geom->getXLower()[2], -0.5) && greaterEq(patch_geom->getXUpper()[2], -0.5) && greaterEq(position[0], -0.5) && lessEq(position[0], 0.5) && greaterEq(position[1], -0.5) && lessEq(position[1], 0.5)) {
								checkStencil(patch, count_i, 1, count_j, 1, floor((-0.5 - -1.0)/ particleSeparation_z) + floor(0.5 + d_ghost_width*(dx[2]/particleSeparation_z)), 1, 3);
							}
					}
				}
			}
			if (particleDistribution.compare("STAGGERED") == 0 && isEven((int)(floor((0.5 - -1.0)/ particleSeparation_z) + floor(0.5 + d_ghost_width*(dx[2]/particleSeparation_z))))) {
				iMapStart = d_grid_geometry->getXLower()[0] - (floor(d_ghost_width * (dx[0]/particleSeparation_x)))*particleSeparation_x;
				jMapStart = d_grid_geometry->getXLower()[1] - (floor(d_ghost_width * (dx[1]/particleSeparation_y)))*particleSeparation_y;
			} else {
				iMapStart = d_grid_geometry->getXLower()[0] + particleSeparation_x/2.0 - (floor(0.5 + d_ghost_width * (dx[0]/particleSeparation_x)))*particleSeparation_x;
				jMapStart = d_grid_geometry->getXLower()[1] + particleSeparation_y/2.0 - (floor(0.5 + d_ghost_width * (dx[1]/particleSeparation_y)))*particleSeparation_y;
			}
			for (int count_j = 0; jMapStart + count_j * particleSeparation_y < jMapEnd; count_j++) {
				for (int count_i = 0; iMapStart + count_i * particleSeparation_x < iMapEnd; count_i++) {
					if (floor((iMapStart + count_i * particleSeparation_x - d_grid_geometry->getXLower()[0])/dx[0]) >= boxfirst(0) && floor((iMapStart + count_i * particleSeparation_x - d_grid_geometry->getXLower()[0])/dx[0]) <= boxlast(0) && floor((jMapStart + count_j * particleSeparation_y - d_grid_geometry->getXLower()[1])/dx[1]) >= boxfirst(1) && floor((jMapStart + count_j * particleSeparation_y - d_grid_geometry->getXLower()[1])/dx[1]) <= boxlast(1)) {
							position[0] = iMapStart + count_i * particleSeparation_x;
							position[1] = jMapStart + count_j * particleSeparation_y;
							if (lessEq(patch_geom->getXLower()[2], 0.5) && greaterEq(patch_geom->getXUpper()[2], 0.5) && greaterEq(position[0], -0.5) && lessEq(position[0], 0.5) && greaterEq(position[1], -0.5) && lessEq(position[1], 0.5)) {
								checkStencil(patch, count_i, 1, count_j, 1, floor((0.5 - -1.0)/ particleSeparation_z) + floor(0.5 + d_ghost_width*(dx[2]/particleSeparation_z)), 2, 3);
							}
						}
					}
				}
			}
			//Boundaries Mapping
			iMapEnd = d_grid_geometry->getXUpper()[0] + d_ghost_width * dx[0];
			jMapEnd = d_grid_geometry->getXUpper()[1] + d_ghost_width * dx[1];
			kMapStart = d_grid_geometry->getXLower()[2] + particleSeparation_z/2.0 - (floor(0.5 + d_ghost_width * (dx[2]/particleSeparation_z)))*particleSeparation_z;
			kMapEnd = d_grid_geometry->getXUpper()[2] + d_ghost_width * dx[2];
			for (int count_k = 0; kMapStart + count_k * particleSeparation_z < kMapEnd; count_k++) {
				if (particleDistribution.compare("STAGGERED") == 0 && isEven(count_k)) {
					iMapStart = d_grid_geometry->getXLower()[0] - (floor(d_ghost_width * (dx[0]/particleSeparation_x)))*particleSeparation_x;
					jMapStart = d_grid_geometry->getXLower()[1] - (floor(d_ghost_width * (dx[1]/particleSeparation_y)))*particleSeparation_y;
				} else {
					iMapStart = d_grid_geometry->getXLower()[0] + particleSeparation_x/2.0 - (floor(0.5 + d_ghost_width * (dx[0]/particleSeparation_x)))*particleSeparation_x;
					jMapStart = d_grid_geometry->getXLower()[1] + particleSeparation_y/2.0 - (floor(0.5 + d_ghost_width * (dx[1]/particleSeparation_y)))*particleSeparation_y;
				}
				for (int count_j = 0; jMapStart + count_j * particleSeparation_y < jMapEnd; count_j++) {
					for (int count_i = 0; iMapStart + count_i * particleSeparation_x < iMapEnd; count_i++) {
						position[0] = iMapStart + count_i * particleSeparation_x;
						i = floor((position[0] - d_grid_geometry->getXLower()[0])/dx[0]);
						position[1] = jMapStart + count_j * particleSeparation_y;
						j = floor((position[1] - d_grid_geometry->getXLower()[1])/dx[1]);
						position[2] = kMapStart + count_k * particleSeparation_z;
						k = floor((position[2] - d_grid_geometry->getXLower()[2])/dx[2]);
						if (i >= boxfirst(0) && i <= boxlast(0) && j >= boxfirst(1) && j <= boxlast(1) && k >= boxfirst(2) && k <= boxlast(2)) {
							//z-Upper
							if (k > boxlast1(2)) {
								hier::Index idx(i, j, k);
								Particles* part = problemVariable->getItem(idx);
								Particle* particle = new Particle(0, position, -6);
								Particle* oldParticle = part->overlaps(*particle);
								if (oldParticle != NULL) {
									part->deleteParticle(*oldParticle);
								}
								part->addParticle(*particle);
								delete particle;
							}
							//z-Lower
							if (k < boxfirst1(2)) {
								hier::Index idx(i, j, k);
								Particles* part = problemVariable->getItem(idx);
								Particle* particle = new Particle(0, position, -5);
								Particle* oldParticle = part->overlaps(*particle);
								if (oldParticle != NULL) {
									part->deleteParticle(*oldParticle);
								}
								part->addParticle(*particle);
								delete particle;
							}
							//y-Upper
							if (j > boxlast1(1)) {
								hier::Index idx(i, j, k);
								Particles* part = problemVariable->getItem(idx);
								Particle* particle = new Particle(0, position, -4);
								Particle* oldParticle = part->overlaps(*particle);
								if (oldParticle != NULL) {
									part->deleteParticle(*oldParticle);
								}
								part->addParticle(*particle);
								delete particle;
							}
							//y-Lower
							if (j < boxfirst1(1)) {
								hier::Index idx(i, j, k);
								Particles* part = problemVariable->getItem(idx);
								Particle* particle = new Particle(0, position, -3);
								Particle* oldParticle = part->overlaps(*particle);
								if (oldParticle != NULL) {
									part->deleteParticle(*oldParticle);
								}
								part->addParticle(*particle);
								delete particle;
							}
							//x-Upper
							if (i > boxlast1(0)) {
								hier::Index idx(i, j, k);
								Particles* part = problemVariable->getItem(idx);
								Particle* particle = new Particle(0, position, -2);
								Particle* oldParticle = part->overlaps(*particle);
								if (oldParticle != NULL) {
									part->deleteParticle(*oldParticle);
								}
								part->addParticle(*particle);
								delete particle;
							}
							//x-Lower
							if (i < boxfirst1(0)) {
								hier::Index idx(i, j, k);
								Particles* part = problemVariable->getItem(idx);
								Particle* particle = new Particle(0, position, -1);
								Particle* oldParticle = part->overlaps(*particle);
								if (oldParticle != NULL) {
									part->deleteParticle(*oldParticle);
								}
								part->addParticle(*particle);
								delete particle;
							}
						}
					}
				}
			}
		}


   	}
}


/*
 * Checks if the point has a stencil width
 */
void Problem::checkStencil(const hier::Patch& patch, int i, int iOp, int j, int jOp, int k, int kOp, int v) const {
	bool iFilled, jFilled, kFilled;
	int iDirection, iIncrement, jDirection, jIncrement, kDirection, kIncrement;

	std::shared_ptr< pdat::IndexData<Particles, pdat::CellGeometry > > problemVariable(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles, pdat::CellGeometry >, hier::PatchData>(patch.getPatchData(d_problemVariable_id)));
	const hier::Index boxfirst = problemVariable->getGhostBox().lower();
	const hier::Index boxlast  = problemVariable->getGhostBox().upper();
	const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch.getPatchGeometry()));
	const double* dx  = patch_geom->getDx();

	double position[3], tmp;
	//Auxiliary definitions
	iFilled = false;
	iDirection = iOp;
	jFilled = false;
	jDirection = jOp;
	kFilled = false;
	kDirection = kOp;

	bool enough;
	bool assigned;
	//Enough stencil
	enough = false;
	for(int it = i - mapStencilx - 1; it < i + mapStencilx - 1 && !enough; it++) {
		bool incorrectRegion = false;
		for(int it2 = 0; it2 <= mapStencilx - 1 && !incorrectRegion; it2++) {
			if (particleDistribution.compare("STAGGERED") == 0 && isEven(k)) {
				position[0] = d_grid_geometry->getXLower()[0] - (floor(d_ghost_width * (dx[0]/particleSeparation_x)))*particleSeparation_x + (it + it2)*particleSeparation_x;
				position[1] = d_grid_geometry->getXLower()[1] - (floor(d_ghost_width * (dx[1]/particleSeparation_y)))*particleSeparation_y + (j)*particleSeparation_y;
				position[2] = d_grid_geometry->getXLower()[2] + particleSeparation_z/2.0 - (floor(0.5 + d_ghost_width * (dx[2]/particleSeparation_z)))*particleSeparation_z + (k)*particleSeparation_z;
			} else {
				position[0] = d_grid_geometry->getXLower()[0] + particleSeparation_x/2.0 - (floor(0.5 + d_ghost_width * (dx[0]/particleSeparation_x)))*particleSeparation_x + (it + it2)*particleSeparation_x;
				position[1] = d_grid_geometry->getXLower()[1] + particleSeparation_y/2.0 - (floor(0.5 + d_ghost_width * (dx[1]/particleSeparation_y)))*particleSeparation_y + (j)*particleSeparation_y;
				position[2] = d_grid_geometry->getXLower()[2] + particleSeparation_z/2.0 - (floor(0.5 + d_ghost_width * (dx[2]/particleSeparation_z)))*particleSeparation_z + (k)*particleSeparation_z;
			}
			hier::Index idx(floor((position[0] - d_grid_geometry->getXLower()[0])/dx[0]), floor((position[1] - d_grid_geometry->getXLower()[1])/dx[1]), floor((position[2] - d_grid_geometry->getXLower()[2])/dx[2]));
			if (floor((position[0] - d_grid_geometry->getXLower()[0])/dx[0]) <= boxlast(0) && floor((position[0] - d_grid_geometry->getXLower()[0])/dx[0]) >= boxfirst(0) && floor((position[1] - d_grid_geometry->getXLower()[1])/dx[1]) <= boxlast(1) && floor((position[1] - d_grid_geometry->getXLower()[1])/dx[1]) >= boxfirst(1) && floor((position[2] - d_grid_geometry->getXLower()[2])/dx[2]) <= boxlast(2) && floor((position[2] - d_grid_geometry->getXLower()[2])/dx[2]) >= boxfirst(2)) {
				Particle* particle = new Particle(0, position, v);
				Particles* part = problemVariable->getItem(idx);
				if (!part->contains(*particle)) {
					incorrectRegion = true;
				}
				delete particle;
			} else {
				if (floor((position[0] - d_grid_geometry->getXLower()[0])/dx[0]) > boxlast(0) || floor((position[0] - d_grid_geometry->getXLower()[0])/dx[0]) < boxfirst(0) || floor((position[1] - d_grid_geometry->getXLower()[1])/dx[1]) > boxlast(1) || floor((position[1] - d_grid_geometry->getXLower()[1])/dx[1]) < boxfirst(1) || floor((position[2] - d_grid_geometry->getXLower()[2])/dx[2]) > boxlast(2) || floor((position[2] - d_grid_geometry->getXLower()[2])/dx[2]) < boxfirst(2)) {
					incorrectRegion = true;
				}
			}
		}
		if (!incorrectRegion) {
			if (mapStencilx % 2 == 0) {
				if (mapStencilx/2 + it <= i) {
					iDirection = 2;
				} else {
					iDirection = 1;
				}
			} else {
				if (mapStencilx/2 + it < i) {
					iDirection = 2;
				} else {
					if (mapStencilx/2 + it == i) {
						iDirection = iOp;
					} else {
						iDirection = 1;
					}
				}
			}
			enough = true;
		}
	}
	//Not enough stencil
	assigned = false;
	if (!enough) {
		for(int it = 1; it <= mapStencilx - 1 && !assigned; it++) {
			tmp = (int)(floor(d_grid_geometry->getXUpper()[0]/particleSeparation_x - d_grid_geometry->getXLower()[0]/particleSeparation_x + 2 * d_ghost_width * (dx[0]/particleSeparation_x))) - 1;
			if (particleDistribution.compare("STAGGERED") == 0 && isEven(k)) {
				position[0] = d_grid_geometry->getXLower()[0] - (floor(d_ghost_width * (dx[0]/particleSeparation_x)))*particleSeparation_x + tmp * particleSeparation_x;
			} else {
				position[0] = d_grid_geometry->getXLower()[0] + particleSeparation_x/2.0 - (floor(0.5 + d_ghost_width * (dx[0]/particleSeparation_x)))*particleSeparation_x + tmp * particleSeparation_x;
			}
			if (Equals(position[0], 1.0 + d_ghost_width * dx[0])) {
				tmp = tmp - 1;
			}
			if (iOp == 1 && i + it <= tmp) {
				if (particleDistribution.compare("STAGGERED") == 0 && isEven(k)) {
					position[0] = d_grid_geometry->getXLower()[0] - (floor(d_ghost_width * (dx[0]/particleSeparation_x)))*particleSeparation_x + (i + it)*particleSeparation_x;
					position[1] = d_grid_geometry->getXLower()[1] - (floor(d_ghost_width * (dx[1]/particleSeparation_y)))*particleSeparation_y + (j)*particleSeparation_y;
					position[2] = d_grid_geometry->getXLower()[2] + particleSeparation_z/2.0 - (floor(0.5 + d_ghost_width * (dx[2]/particleSeparation_z)))*particleSeparation_z + (k)*particleSeparation_z;
				} else {
					position[0] = d_grid_geometry->getXLower()[0] + particleSeparation_x/2.0 - (floor(0.5 + d_ghost_width * (dx[0]/particleSeparation_x)))*particleSeparation_x + (i + it)*particleSeparation_x;
					position[1] = d_grid_geometry->getXLower()[1] + particleSeparation_y/2.0 - (floor(0.5 + d_ghost_width * (dx[1]/particleSeparation_y)))*particleSeparation_y + (j)*particleSeparation_y;
					position[2] = d_grid_geometry->getXLower()[2] + particleSeparation_z/2.0 - (floor(0.5 + d_ghost_width * (dx[2]/particleSeparation_z)))*particleSeparation_z + (k)*particleSeparation_z;
				}
				if (floor((position[0] - d_grid_geometry->getXLower()[0])/dx[0]) <= boxlast(0) && floor((position[0] - d_grid_geometry->getXLower()[0])/dx[0]) >= boxfirst(0) && floor((position[1] - d_grid_geometry->getXLower()[1])/dx[1]) <= boxlast(1) && floor((position[1] - d_grid_geometry->getXLower()[1])/dx[1]) >= boxfirst(1) && floor((position[2] - d_grid_geometry->getXLower()[2])/dx[2]) <= boxlast(2) && floor((position[2] - d_grid_geometry->getXLower()[2])/dx[2]) >= boxfirst(2)) {
					hier::Index idx(floor((position[0] - d_grid_geometry->getXLower()[0])/dx[0]), floor((position[1] - d_grid_geometry->getXLower()[1])/dx[1]), floor((position[2] - d_grid_geometry->getXLower()[2])/dx[2]));
					Particle* particle = new Particle(0, position, v);
					Particles* part = problemVariable->getItem(idx);
					if (!part->contains(*particle)) {
						Particle* oldParticle = part->overlaps(*particle);
						if (oldParticle != NULL) {
							part->deleteParticle(*oldParticle);
						}
						part->addParticle(*particle);
						iDirection = 1;
						iIncrement = it;
						assigned = true;
						iFilled = true;
					}
					delete particle;
				}
			} else {
				if (particleDistribution.compare("STAGGERED") == 0 && isEven(k)) {
					position[0] = d_grid_geometry->getXLower()[0] - (floor(d_ghost_width * (dx[0]/particleSeparation_x)))*particleSeparation_x + (i - it)*particleSeparation_x;
					position[1] = d_grid_geometry->getXLower()[1] - (floor(d_ghost_width * (dx[1]/particleSeparation_y)))*particleSeparation_y + (j)*particleSeparation_y;
					position[2] = d_grid_geometry->getXLower()[2] + particleSeparation_z/2.0 - (floor(0.5 + d_ghost_width * (dx[2]/particleSeparation_z)))*particleSeparation_z + (k)*particleSeparation_z;
				} else {
					position[0] = d_grid_geometry->getXLower()[0] + particleSeparation_x/2.0 - (floor(0.5 + d_ghost_width * (dx[0]/particleSeparation_x)))*particleSeparation_x + (i - it)*particleSeparation_x;
					position[1] = d_grid_geometry->getXLower()[1] + particleSeparation_y/2.0 - (floor(0.5 + d_ghost_width * (dx[1]/particleSeparation_y)))*particleSeparation_y + (j)*particleSeparation_y;
					position[2] = d_grid_geometry->getXLower()[2] + particleSeparation_z/2.0 - (floor(0.5 + d_ghost_width * (dx[2]/particleSeparation_z)))*particleSeparation_z + (k)*particleSeparation_z;
				}
				if (floor((position[0] - d_grid_geometry->getXLower()[0])/dx[0]) <= boxlast(0) && floor((position[0] - d_grid_geometry->getXLower()[0])/dx[0]) >= boxfirst(0) && floor((position[1] - d_grid_geometry->getXLower()[1])/dx[1]) <= boxlast(1) && floor((position[1] - d_grid_geometry->getXLower()[1])/dx[1]) >= boxfirst(1) && floor((position[2] - d_grid_geometry->getXLower()[2])/dx[2]) <= boxlast(2) && floor((position[2] - d_grid_geometry->getXLower()[2])/dx[2]) >= boxfirst(2)) {
					hier::Index idx(floor((position[0] - d_grid_geometry->getXLower()[0])/dx[0]), floor((position[1] - d_grid_geometry->getXLower()[1])/dx[1]), floor((position[2] - d_grid_geometry->getXLower()[2])/dx[2]));
					Particle* particle = new Particle(0, position, v);
					Particles* part = problemVariable->getItem(idx);
					if (!part->contains(*particle)) {
						Particle* oldParticle = part->overlaps(*particle);
						if (oldParticle != NULL) {
							part->deleteParticle(*oldParticle);
						}
						part->addParticle(*particle);
						iDirection = 2;
						iIncrement = it;
						assigned = true;
						iFilled = true;
					}
					delete particle;
				}
			}
		}
	}
	//Enough stencil
	enough = false;
	for(int jt = j - mapStencily - 1; jt < j + mapStencily - 1 && !enough; jt++) {
		bool incorrectRegion = false;
		for(int jt2 = 0; jt2 <= mapStencily - 1 && !incorrectRegion; jt2++) {
			if (particleDistribution.compare("STAGGERED") == 0 && isEven(k)) {
				position[0] = d_grid_geometry->getXLower()[0] - (floor(d_ghost_width * (dx[0]/particleSeparation_x)))*particleSeparation_x + (i)*particleSeparation_x;
				position[1] = d_grid_geometry->getXLower()[1] - (floor(d_ghost_width * (dx[1]/particleSeparation_y)))*particleSeparation_y + (jt + jt2)*particleSeparation_y;
				position[2] = d_grid_geometry->getXLower()[2] + particleSeparation_z/2.0 - (floor(0.5 + d_ghost_width * (dx[2]/particleSeparation_z)))*particleSeparation_z + (k)*particleSeparation_z;
			} else {
				position[0] = d_grid_geometry->getXLower()[0] + particleSeparation_x/2.0 - (floor(0.5 + d_ghost_width * (dx[0]/particleSeparation_x)))*particleSeparation_x + (i)*particleSeparation_x;
				position[1] = d_grid_geometry->getXLower()[1] + particleSeparation_y/2.0 - (floor(0.5 + d_ghost_width * (dx[1]/particleSeparation_y)))*particleSeparation_y + (jt + jt2)*particleSeparation_y;
				position[2] = d_grid_geometry->getXLower()[2] + particleSeparation_z/2.0 - (floor(0.5 + d_ghost_width * (dx[2]/particleSeparation_z)))*particleSeparation_z + (k)*particleSeparation_z;
			}
			hier::Index idx(floor((position[0] - d_grid_geometry->getXLower()[0])/dx[0]), floor((position[1] - d_grid_geometry->getXLower()[1])/dx[1]), floor((position[2] - d_grid_geometry->getXLower()[2])/dx[2]));
			if (floor((position[0] - d_grid_geometry->getXLower()[0])/dx[0]) <= boxlast(0) && floor((position[0] - d_grid_geometry->getXLower()[0])/dx[0]) >= boxfirst(0) && floor((position[1] - d_grid_geometry->getXLower()[1])/dx[1]) <= boxlast(1) && floor((position[1] - d_grid_geometry->getXLower()[1])/dx[1]) >= boxfirst(1) && floor((position[2] - d_grid_geometry->getXLower()[2])/dx[2]) <= boxlast(2) && floor((position[2] - d_grid_geometry->getXLower()[2])/dx[2]) >= boxfirst(2)) {
				Particle* particle = new Particle(0, position, v);
				Particles* part = problemVariable->getItem(idx);
				if (!part->contains(*particle)) {
					incorrectRegion = true;
				}
				delete particle;
			} else {
				if (floor((position[0] - d_grid_geometry->getXLower()[0])/dx[0]) > boxlast(0) || floor((position[0] - d_grid_geometry->getXLower()[0])/dx[0]) < boxfirst(0) || floor((position[1] - d_grid_geometry->getXLower()[1])/dx[1]) > boxlast(1) || floor((position[1] - d_grid_geometry->getXLower()[1])/dx[1]) < boxfirst(1) || floor((position[2] - d_grid_geometry->getXLower()[2])/dx[2]) > boxlast(2) || floor((position[2] - d_grid_geometry->getXLower()[2])/dx[2]) < boxfirst(2)) {
					incorrectRegion = true;
				}
			}
		}
		if (!incorrectRegion) {
			if (mapStencily % 2 == 0) {
				if (mapStencily/2 + jt <= j) {
					jDirection = 2;
				} else {
					jDirection = 1;
				}
			} else {
				if (mapStencily/2 + jt < j) {
					jDirection = 2;
				} else {
					if (mapStencily/2 + jt == j) {
						jDirection = jOp;
					} else {
						jDirection = 1;
					}
				}
			}
			enough = true;
		}
	}
	//Not enough stencil
	assigned = false;
	if (!enough) {
		for(int jt = 1; jt <= mapStencily - 1 && !assigned; jt++) {
			tmp = (int)(floor(d_grid_geometry->getXUpper()[1]/particleSeparation_y - d_grid_geometry->getXLower()[1]/particleSeparation_y + 2 * d_ghost_width * (dx[1]/particleSeparation_y))) - 1;
			if (particleDistribution.compare("STAGGERED") == 0 && isEven(k)) {
				position[1] = d_grid_geometry->getXLower()[1] - (floor(d_ghost_width * (dx[1]/particleSeparation_y)))*particleSeparation_y + tmp * particleSeparation_y;
			} else {
				position[1] = d_grid_geometry->getXLower()[1] + particleSeparation_y/2.0 - (floor(0.5 + d_ghost_width * (dx[1]/particleSeparation_y)))*particleSeparation_y + tmp * particleSeparation_y;
			}
			if (Equals(position[1], 1.0 + d_ghost_width * dx[1])) {
				tmp = tmp - 1;
			}
			if (jOp == 1 && j + jt <= tmp) {
				if (particleDistribution.compare("STAGGERED") == 0 && isEven(k)) {
					position[0] = d_grid_geometry->getXLower()[0] - (floor(d_ghost_width * (dx[0]/particleSeparation_x)))*particleSeparation_x + (i)*particleSeparation_x;
					position[1] = d_grid_geometry->getXLower()[1] - (floor(d_ghost_width * (dx[1]/particleSeparation_y)))*particleSeparation_y + (j + jt)*particleSeparation_y;
					position[2] = d_grid_geometry->getXLower()[2] + particleSeparation_z/2.0 - (floor(0.5 + d_ghost_width * (dx[2]/particleSeparation_z)))*particleSeparation_z + (k)*particleSeparation_z;
				} else {
					position[0] = d_grid_geometry->getXLower()[0] + particleSeparation_x/2.0 - (floor(0.5 + d_ghost_width * (dx[0]/particleSeparation_x)))*particleSeparation_x + (i)*particleSeparation_x;
					position[1] = d_grid_geometry->getXLower()[1] + particleSeparation_y/2.0 - (floor(0.5 + d_ghost_width * (dx[1]/particleSeparation_y)))*particleSeparation_y + (j + jt)*particleSeparation_y;
					position[2] = d_grid_geometry->getXLower()[2] + particleSeparation_z/2.0 - (floor(0.5 + d_ghost_width * (dx[2]/particleSeparation_z)))*particleSeparation_z + (k)*particleSeparation_z;
				}
				if (floor((position[0] - d_grid_geometry->getXLower()[0])/dx[0]) <= boxlast(0) && floor((position[0] - d_grid_geometry->getXLower()[0])/dx[0]) >= boxfirst(0) && floor((position[1] - d_grid_geometry->getXLower()[1])/dx[1]) <= boxlast(1) && floor((position[1] - d_grid_geometry->getXLower()[1])/dx[1]) >= boxfirst(1) && floor((position[2] - d_grid_geometry->getXLower()[2])/dx[2]) <= boxlast(2) && floor((position[2] - d_grid_geometry->getXLower()[2])/dx[2]) >= boxfirst(2)) {
					hier::Index idx(floor((position[0] - d_grid_geometry->getXLower()[0])/dx[0]), floor((position[1] - d_grid_geometry->getXLower()[1])/dx[1]), floor((position[2] - d_grid_geometry->getXLower()[2])/dx[2]));
					Particle* particle = new Particle(0, position, v);
					Particles* part = problemVariable->getItem(idx);
					if (!part->contains(*particle)) {
						Particle* oldParticle = part->overlaps(*particle);
						if (oldParticle != NULL) {
							part->deleteParticle(*oldParticle);
						}
						part->addParticle(*particle);
						jDirection = 1;
						jIncrement = jt;
						assigned = true;
						jFilled = true;
					}
					delete particle;
				}
			} else {
				if (particleDistribution.compare("STAGGERED") == 0 && isEven(k)) {
					position[0] = d_grid_geometry->getXLower()[0] - (floor(d_ghost_width * (dx[0]/particleSeparation_x)))*particleSeparation_x + (i)*particleSeparation_x;
					position[1] = d_grid_geometry->getXLower()[1] - (floor(d_ghost_width * (dx[1]/particleSeparation_y)))*particleSeparation_y + (j - jt)*particleSeparation_y;
					position[2] = d_grid_geometry->getXLower()[2] + particleSeparation_z/2.0 - (floor(0.5 + d_ghost_width * (dx[2]/particleSeparation_z)))*particleSeparation_z + (k)*particleSeparation_z;
				} else {
					position[0] = d_grid_geometry->getXLower()[0] + particleSeparation_x/2.0 - (floor(0.5 + d_ghost_width * (dx[0]/particleSeparation_x)))*particleSeparation_x + (i)*particleSeparation_x;
					position[1] = d_grid_geometry->getXLower()[1] + particleSeparation_y/2.0 - (floor(0.5 + d_ghost_width * (dx[1]/particleSeparation_y)))*particleSeparation_y + (j - jt)*particleSeparation_y;
					position[2] = d_grid_geometry->getXLower()[2] + particleSeparation_z/2.0 - (floor(0.5 + d_ghost_width * (dx[2]/particleSeparation_z)))*particleSeparation_z + (k)*particleSeparation_z;
				}
				if (floor((position[0] - d_grid_geometry->getXLower()[0])/dx[0]) <= boxlast(0) && floor((position[0] - d_grid_geometry->getXLower()[0])/dx[0]) >= boxfirst(0) && floor((position[1] - d_grid_geometry->getXLower()[1])/dx[1]) <= boxlast(1) && floor((position[1] - d_grid_geometry->getXLower()[1])/dx[1]) >= boxfirst(1) && floor((position[2] - d_grid_geometry->getXLower()[2])/dx[2]) <= boxlast(2) && floor((position[2] - d_grid_geometry->getXLower()[2])/dx[2]) >= boxfirst(2)) {
					hier::Index idx(floor((position[0] - d_grid_geometry->getXLower()[0])/dx[0]), floor((position[1] - d_grid_geometry->getXLower()[1])/dx[1]), floor((position[2] - d_grid_geometry->getXLower()[2])/dx[2]));
					Particle* particle = new Particle(0, position, v);
					Particles* part = problemVariable->getItem(idx);
					if (!part->contains(*particle)) {
						Particle* oldParticle = part->overlaps(*particle);
						if (oldParticle != NULL) {
							part->deleteParticle(*oldParticle);
						}
						part->addParticle(*particle);
						jDirection = 2;
						jIncrement = jt;
						assigned = true;
						jFilled = true;
					}
					delete particle;
				}
			}
		}
	}
	//Enough stencil
	enough = false;
	for(int kt = k - mapStencilz - 1; kt < k + mapStencilz - 1 && !enough; kt++) {
		bool incorrectRegion = false;
		for(int kt2 = 0; kt2 <= mapStencilz - 1 && !incorrectRegion; kt2++) {
			if (particleDistribution.compare("STAGGERED") == 0 && isEven(kt + kt2)) {
				position[0] = d_grid_geometry->getXLower()[0] - (floor(d_ghost_width * (dx[0]/particleSeparation_x)))*particleSeparation_x + (i)*particleSeparation_x;
				position[1] = d_grid_geometry->getXLower()[1] - (floor(d_ghost_width * (dx[1]/particleSeparation_y)))*particleSeparation_y + (j)*particleSeparation_y;
				position[2] = d_grid_geometry->getXLower()[2] + particleSeparation_z/2.0 - (floor(0.5 + d_ghost_width * (dx[2]/particleSeparation_z)))*particleSeparation_z + (kt + kt2)*particleSeparation_z;
			} else {
				position[0] = d_grid_geometry->getXLower()[0] + particleSeparation_x/2.0 - (floor(0.5 + d_ghost_width * (dx[0]/particleSeparation_x)))*particleSeparation_x + (i)*particleSeparation_x;
				position[1] = d_grid_geometry->getXLower()[1] + particleSeparation_y/2.0 - (floor(0.5 + d_ghost_width * (dx[1]/particleSeparation_y)))*particleSeparation_y + (j)*particleSeparation_y;
				position[2] = d_grid_geometry->getXLower()[2] + particleSeparation_z/2.0 - (floor(0.5 + d_ghost_width * (dx[2]/particleSeparation_z)))*particleSeparation_z + (kt + kt2)*particleSeparation_z;
			}
			hier::Index idx(floor((position[0] - d_grid_geometry->getXLower()[0])/dx[0]), floor((position[1] - d_grid_geometry->getXLower()[1])/dx[1]), floor((position[2] - d_grid_geometry->getXLower()[2])/dx[2]));
			if (floor((position[0] - d_grid_geometry->getXLower()[0])/dx[0]) <= boxlast(0) && floor((position[0] - d_grid_geometry->getXLower()[0])/dx[0]) >= boxfirst(0) && floor((position[1] - d_grid_geometry->getXLower()[1])/dx[1]) <= boxlast(1) && floor((position[1] - d_grid_geometry->getXLower()[1])/dx[1]) >= boxfirst(1) && floor((position[2] - d_grid_geometry->getXLower()[2])/dx[2]) <= boxlast(2) && floor((position[2] - d_grid_geometry->getXLower()[2])/dx[2]) >= boxfirst(2)) {
				Particle* particle = new Particle(0, position, v);
				Particles* part = problemVariable->getItem(idx);
				if (!part->contains(*particle)) {
					incorrectRegion = true;
				}
				delete particle;
			} else {
				if (floor((position[0] - d_grid_geometry->getXLower()[0])/dx[0]) > boxlast(0) || floor((position[0] - d_grid_geometry->getXLower()[0])/dx[0]) < boxfirst(0) || floor((position[1] - d_grid_geometry->getXLower()[1])/dx[1]) > boxlast(1) || floor((position[1] - d_grid_geometry->getXLower()[1])/dx[1]) < boxfirst(1) || floor((position[2] - d_grid_geometry->getXLower()[2])/dx[2]) > boxlast(2) || floor((position[2] - d_grid_geometry->getXLower()[2])/dx[2]) < boxfirst(2)) {
					incorrectRegion = true;
				}
			}
		}
		if (!incorrectRegion) {
			if (mapStencilz % 2 == 0) {
				if (mapStencilz/2 + kt <= k) {
					kDirection = 2;
				} else {
					kDirection = 1;
				}
			} else {
				if (mapStencilz/2 + kt < k) {
					kDirection = 2;
				} else {
					if (mapStencilz/2 + kt == k) {
						kDirection = kOp;
					} else {
						kDirection = 1;
					}
				}
			}
			enough = true;
		}
	}
	//Not enough stencil
	assigned = false;
	if (!enough) {
		for(int kt = 1; kt <= mapStencilz - 1 && !assigned; kt++) {
			tmp = (int)(floor(d_grid_geometry->getXUpper()[2]/particleSeparation_z - d_grid_geometry->getXLower()[2]/particleSeparation_z + 2 * d_ghost_width * (dx[2]/particleSeparation_z))) - 1;
			position[2] = d_grid_geometry->getXLower()[2] + particleSeparation_z/2.0 - (floor(0.5 + d_ghost_width * (dx[2]/particleSeparation_z)))*particleSeparation_z + tmp * particleSeparation_z;
			if (Equals(position[2], 1.0 + d_ghost_width * dx[2])) {
				tmp = tmp - 1;
			}
			if (kOp == 1 && k + kt <= tmp) {
				if (particleDistribution.compare("STAGGERED") == 0 && isEven(k + kt)) {
					position[0] = d_grid_geometry->getXLower()[0] - (floor(d_ghost_width * (dx[0]/particleSeparation_x)))*particleSeparation_x + (i)*particleSeparation_x;
					position[1] = d_grid_geometry->getXLower()[1] - (floor(d_ghost_width * (dx[1]/particleSeparation_y)))*particleSeparation_y + (j)*particleSeparation_y;
					position[2] = d_grid_geometry->getXLower()[2] + particleSeparation_z/2.0 - (floor(0.5 + d_ghost_width * (dx[2]/particleSeparation_z)))*particleSeparation_z + (k + kt)*particleSeparation_z;
				} else {
					position[0] = d_grid_geometry->getXLower()[0] + particleSeparation_x/2.0 - (floor(0.5 + d_ghost_width * (dx[0]/particleSeparation_x)))*particleSeparation_x + (i)*particleSeparation_x;
					position[1] = d_grid_geometry->getXLower()[1] + particleSeparation_y/2.0 - (floor(0.5 + d_ghost_width * (dx[1]/particleSeparation_y)))*particleSeparation_y + (j)*particleSeparation_y;
					position[2] = d_grid_geometry->getXLower()[2] + particleSeparation_z/2.0 - (floor(0.5 + d_ghost_width * (dx[2]/particleSeparation_z)))*particleSeparation_z + (k + kt)*particleSeparation_z;
				}
				if (floor((position[0] - d_grid_geometry->getXLower()[0])/dx[0]) <= boxlast(0) && floor((position[0] - d_grid_geometry->getXLower()[0])/dx[0]) >= boxfirst(0) && floor((position[1] - d_grid_geometry->getXLower()[1])/dx[1]) <= boxlast(1) && floor((position[1] - d_grid_geometry->getXLower()[1])/dx[1]) >= boxfirst(1) && floor((position[2] - d_grid_geometry->getXLower()[2])/dx[2]) <= boxlast(2) && floor((position[2] - d_grid_geometry->getXLower()[2])/dx[2]) >= boxfirst(2)) {
					hier::Index idx(floor((position[0] - d_grid_geometry->getXLower()[0])/dx[0]), floor((position[1] - d_grid_geometry->getXLower()[1])/dx[1]), floor((position[2] - d_grid_geometry->getXLower()[2])/dx[2]));
					Particle* particle = new Particle(0, position, v);
					Particles* part = problemVariable->getItem(idx);
					if (!part->contains(*particle)) {
						Particle* oldParticle = part->overlaps(*particle);
						if (oldParticle != NULL) {
							part->deleteParticle(*oldParticle);
						}
						part->addParticle(*particle);
						kDirection = 1;
						kIncrement = kt;
						assigned = true;
						kFilled = true;
					}
					delete particle;
				}
			} else {
				if (particleDistribution.compare("STAGGERED") == 0 && isEven(k - kt)) {
					position[0] = d_grid_geometry->getXLower()[0] - (floor(d_ghost_width * (dx[0]/particleSeparation_x)))*particleSeparation_x + (i)*particleSeparation_x;
					position[1] = d_grid_geometry->getXLower()[1] - (floor(d_ghost_width * (dx[1]/particleSeparation_y)))*particleSeparation_y + (j)*particleSeparation_y;
					position[2] = d_grid_geometry->getXLower()[2] + particleSeparation_z/2.0 - (floor(0.5 + d_ghost_width * (dx[2]/particleSeparation_z)))*particleSeparation_z + (k - kt)*particleSeparation_z;
				} else {
					position[0] = d_grid_geometry->getXLower()[0] + particleSeparation_x/2.0 - (floor(0.5 + d_ghost_width * (dx[0]/particleSeparation_x)))*particleSeparation_x + (i)*particleSeparation_x;
					position[1] = d_grid_geometry->getXLower()[1] + particleSeparation_y/2.0 - (floor(0.5 + d_ghost_width * (dx[1]/particleSeparation_y)))*particleSeparation_y + (j)*particleSeparation_y;
					position[2] = d_grid_geometry->getXLower()[2] + particleSeparation_z/2.0 - (floor(0.5 + d_ghost_width * (dx[2]/particleSeparation_z)))*particleSeparation_z + (k - kt)*particleSeparation_z;
				}
				if (floor((position[0] - d_grid_geometry->getXLower()[0])/dx[0]) <= boxlast(0) && floor((position[0] - d_grid_geometry->getXLower()[0])/dx[0]) >= boxfirst(0) && floor((position[1] - d_grid_geometry->getXLower()[1])/dx[1]) <= boxlast(1) && floor((position[1] - d_grid_geometry->getXLower()[1])/dx[1]) >= boxfirst(1) && floor((position[2] - d_grid_geometry->getXLower()[2])/dx[2]) <= boxlast(2) && floor((position[2] - d_grid_geometry->getXLower()[2])/dx[2]) >= boxfirst(2)) {
					hier::Index idx(floor((position[0] - d_grid_geometry->getXLower()[0])/dx[0]), floor((position[1] - d_grid_geometry->getXLower()[1])/dx[1]), floor((position[2] - d_grid_geometry->getXLower()[2])/dx[2]));
					Particle* particle = new Particle(0, position, v);
					Particles* part = problemVariable->getItem(idx);
					if (!part->contains(*particle)) {
						Particle* oldParticle = part->overlaps(*particle);
						if (oldParticle != NULL) {
							part->deleteParticle(*oldParticle);
						}
						part->addParticle(*particle);
						kDirection = 2;
						kIncrement = kt;
						assigned = true;
						kFilled = true;
					}
					delete particle;
				}
			}
		}
	}
	if (iFilled) {
		if (iDirection == 1) {
			checkStencil(patch, i + iIncrement, iDirection, j, jDirection, k, kDirection, v);
		} else {
			checkStencil(patch, i - iIncrement, iDirection, j, jDirection, k, kDirection, v);
		}
	}
	if (jFilled) {
		if (jDirection == 1) {
			checkStencil(patch, i, iDirection, j + jIncrement, jDirection, k, kDirection, v);
		} else {
			checkStencil(patch, i, iDirection, j - jIncrement, jDirection, k, kDirection, v);
		}
	}
	if (kFilled) {
		if (kDirection == 1) {
			checkStencil(patch, i, iDirection, j, jDirection, k + kIncrement, kDirection, v);
		} else {
			checkStencil(patch, i, iDirection, j, jDirection, k - kIncrement, kDirection, v);
		}
	}

}

// Point class for the floodfill algorithm
class Point {
private:
public:
	int i, j, k;
};

void Problem::floodfill(const hier::Patch& patch, int i, int j, int k, int pred, int seg) const {

	double position[3], newPosition[3];
	std::shared_ptr< pdat::IndexData<Particles, pdat::CellGeometry > > problemVariable(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles, pdat::CellGeometry >, hier::PatchData>(patch.getPatchData(d_problemVariable_id)));
	std::shared_ptr< pdat::IndexData<NonSyncs, pdat::CellGeometry > > nonSyncVariable(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< NonSyncs, pdat::CellGeometry >, hier::PatchData>(patch.getPatchData(d_nonSyncP_id)));

	const hier::Index boxfirst = problemVariable->getGhostBox().lower();
	const hier::Index boxlast  = problemVariable->getGhostBox().upper();

	const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch.getPatchGeometry()));
	const double* dx  = patch_geom->getDx();
	int indexi;
	int indexj;
	int indexk;

	stack<Point> mystack;

	Point p;
	p.i = i;
	p.j = j;
	p.k = k;

	mystack.push(p);
	while(mystack.size() > 0) {
		p = mystack.top();
		mystack.pop();
		if (particleDistribution.compare("STAGGERED") == 0 && isEven(p.k)) {
			position[0] = d_grid_geometry->getXLower()[0] - (floor(d_ghost_width * (dx[0]/particleSeparation_x)))*particleSeparation_x + (p.i)*particleSeparation_x;
			position[1] = d_grid_geometry->getXLower()[1] - (floor(d_ghost_width * (dx[1]/particleSeparation_y)))*particleSeparation_y + (p.j)*particleSeparation_y;
			position[2] = d_grid_geometry->getXLower()[2] + particleSeparation_z/2.0 - (floor(0.5 + d_ghost_width * (dx[2]/particleSeparation_z)))*particleSeparation_z + (p.k)*particleSeparation_z;
		} else {
			position[0] = d_grid_geometry->getXLower()[0] + particleSeparation_x/2.0 - (floor(0.5 + d_ghost_width * (dx[0]/particleSeparation_x)))*particleSeparation_x + (p.i)*particleSeparation_x;
			position[1] = d_grid_geometry->getXLower()[1] + particleSeparation_y/2.0 - (floor(0.5 + d_ghost_width * (dx[1]/particleSeparation_y)))*particleSeparation_y + (p.j)*particleSeparation_y;
			position[2] = d_grid_geometry->getXLower()[2] + particleSeparation_z/2.0 - (floor(0.5 + d_ghost_width * (dx[2]/particleSeparation_z)))*particleSeparation_z + (p.k)*particleSeparation_z;
		}
		indexi = floor((position[0] - d_grid_geometry->getXLower()[0])/dx[0]);
		indexj = floor((position[1] - d_grid_geometry->getXLower()[1])/dx[1]);
		indexk = floor((position[2] - d_grid_geometry->getXLower()[2])/dx[2]);
		hier::Index idx(indexi, indexj, indexk);
		if (indexi >= boxfirst(0) && indexi <= boxlast(0) && indexj >= boxfirst(1) && indexj <= boxlast(1) && indexk >= boxfirst(2) && indexk <= boxlast(2)) {
			Particle* particle = new Particle(0, position, 0);
			Particles* part = problemVariable->getItem(idx);
			Particle* oldParticle = part->overlaps(*particle);
			NonSyncs* nonSyncpart = nonSyncVariable->getItem(idx);
			NonSync* nonSyncparticle = new NonSync(0, position);
			NonSync* nonSyncoldParticle = nonSyncpart->overlaps(*nonSyncparticle);
			if (nonSyncoldParticle->getnonSync() == 0) {
				nonSyncoldParticle->setnonSync(pred);
				oldParticle->interior = pred;
				if (pred == 2) {
					oldParticle->region = seg;
				}
				if (greaterEq(position[0] - particleSeparation_x, patch_geom->getXLower()[0] - dx[0]*d_ghost_width)) {
					if (particleDistribution.compare("STAGGERED") == 0 && isEven(p.k)) {
						newPosition[0] = d_grid_geometry->getXLower()[0] - (floor(d_ghost_width * (dx[0]/particleSeparation_x)))*particleSeparation_x + (p.i - 1)*particleSeparation_x;
						newPosition[1] = d_grid_geometry->getXLower()[1] - (floor(d_ghost_width * (dx[1]/particleSeparation_y)))*particleSeparation_y + (p.j)*particleSeparation_y;
						newPosition[2] = d_grid_geometry->getXLower()[2] + particleSeparation_z/2.0 - (floor(0.5 + d_ghost_width * (dx[2]/particleSeparation_z)))*particleSeparation_z + (p.k)*particleSeparation_z;
					} else {
						newPosition[0] = d_grid_geometry->getXLower()[0] + particleSeparation_x/2.0 - (floor(0.5 + d_ghost_width * (dx[0]/particleSeparation_x)))*particleSeparation_x + (p.i - 1)*particleSeparation_x;
						newPosition[1] = d_grid_geometry->getXLower()[1] + particleSeparation_y/2.0 - (floor(0.5 + d_ghost_width * (dx[1]/particleSeparation_y)))*particleSeparation_y + (p.j)*particleSeparation_y;
						newPosition[2] = d_grid_geometry->getXLower()[2] + particleSeparation_z/2.0 - (floor(0.5 + d_ghost_width * (dx[2]/particleSeparation_z)))*particleSeparation_z + (p.k)*particleSeparation_z;
					}
					indexi = floor((newPosition[0] - d_grid_geometry->getXLower()[0])/dx[0]);
					indexj = floor((newPosition[1] - d_grid_geometry->getXLower()[1])/dx[1]);
					indexk = floor((newPosition[2] - d_grid_geometry->getXLower()[2])/dx[2]);
					hier::Index idx(indexi, indexj, indexk);
					if (indexi >= boxfirst(0) && indexi <= boxlast(0) && indexj >= boxfirst(1) && indexj <= boxlast(1) && indexk >= boxfirst(2) && indexk <= boxlast(2)) {
						NonSyncs* newnonSyncpart = nonSyncVariable->getItem(idx);
						NonSync* newnonSyncparticle = new NonSync(0, newPosition);
						NonSync* newnonSyncoldParticle = newnonSyncpart->overlaps(*newnonSyncparticle);
						if (newnonSyncoldParticle->getnonSync() == 0) {
							Point np;
							np.i = p.i-1;
							np.j = p.j;
							np.k = p.k;
							mystack.push(np);
						}
						delete newnonSyncparticle;
					}
				}
				if (lessThan(position[0] + particleSeparation_x, patch_geom->getXUpper()[0] + dx[0]*d_ghost_width)) {
					if (particleDistribution.compare("STAGGERED") == 0 && isEven(p.k)) {
						newPosition[0] = d_grid_geometry->getXLower()[0] - (floor(d_ghost_width * (dx[0]/particleSeparation_x)))*particleSeparation_x + (p.i + 1)*particleSeparation_x;
						newPosition[1] = d_grid_geometry->getXLower()[1] - (floor(d_ghost_width * (dx[1]/particleSeparation_y)))*particleSeparation_y + (p.j)*particleSeparation_y;
						newPosition[2] = d_grid_geometry->getXLower()[2] + particleSeparation_z/2.0 - (floor(0.5 + d_ghost_width * (dx[2]/particleSeparation_z)))*particleSeparation_z + (p.k)*particleSeparation_z;
					} else {
						newPosition[0] = d_grid_geometry->getXLower()[0] + particleSeparation_x/2.0 - (floor(0.5 + d_ghost_width * (dx[0]/particleSeparation_x)))*particleSeparation_x + (p.i + 1)*particleSeparation_x;
						newPosition[1] = d_grid_geometry->getXLower()[1] + particleSeparation_y/2.0 - (floor(0.5 + d_ghost_width * (dx[1]/particleSeparation_y)))*particleSeparation_y + (p.j)*particleSeparation_y;
						newPosition[2] = d_grid_geometry->getXLower()[2] + particleSeparation_z/2.0 - (floor(0.5 + d_ghost_width * (dx[2]/particleSeparation_z)))*particleSeparation_z + (p.k)*particleSeparation_z;
					}
					indexi = floor((newPosition[0] - d_grid_geometry->getXLower()[0])/dx[0]);
					indexj = floor((newPosition[1] - d_grid_geometry->getXLower()[1])/dx[1]);
					indexk = floor((newPosition[2] - d_grid_geometry->getXLower()[2])/dx[2]);
					hier::Index idx(indexi, indexj, indexk);
					if (indexi >= boxfirst(0) && indexi <= boxlast(0) && indexj >= boxfirst(1) && indexj <= boxlast(1) && indexk >= boxfirst(2) && indexk <= boxlast(2)) {
						NonSyncs* newnonSyncpart = nonSyncVariable->getItem(idx);
						NonSync* newnonSyncparticle = new NonSync(0, newPosition);
						NonSync* newnonSyncoldParticle = newnonSyncpart->overlaps(*newnonSyncparticle);
						if (newnonSyncoldParticle->getnonSync() == 0) {
							Point np;
							np.i = p.i+1;
							np.j = p.j;
							np.k = p.k;
							mystack.push(np);
						}
						delete newnonSyncparticle;
					}
				}
				if (greaterEq(position[1] - particleSeparation_y, patch_geom->getXLower()[1] - dx[1]*d_ghost_width)) {
					if (particleDistribution.compare("STAGGERED") == 0 && isEven(p.k)) {
						newPosition[0] = d_grid_geometry->getXLower()[0] - (floor(d_ghost_width * (dx[0]/particleSeparation_x)))*particleSeparation_x + (p.i)*particleSeparation_x;
						newPosition[1] = d_grid_geometry->getXLower()[1] - (floor(d_ghost_width * (dx[1]/particleSeparation_y)))*particleSeparation_y + (p.j - 1)*particleSeparation_y;
						newPosition[2] = d_grid_geometry->getXLower()[2] + particleSeparation_z/2.0 - (floor(0.5 + d_ghost_width * (dx[2]/particleSeparation_z)))*particleSeparation_z + (p.k)*particleSeparation_z;
					} else {
						newPosition[0] = d_grid_geometry->getXLower()[0] + particleSeparation_x/2.0 - (floor(0.5 + d_ghost_width * (dx[0]/particleSeparation_x)))*particleSeparation_x + (p.i)*particleSeparation_x;
						newPosition[1] = d_grid_geometry->getXLower()[1] + particleSeparation_y/2.0 - (floor(0.5 + d_ghost_width * (dx[1]/particleSeparation_y)))*particleSeparation_y + (p.j - 1)*particleSeparation_y;
						newPosition[2] = d_grid_geometry->getXLower()[2] + particleSeparation_z/2.0 - (floor(0.5 + d_ghost_width * (dx[2]/particleSeparation_z)))*particleSeparation_z + (p.k)*particleSeparation_z;
					}
					indexi = floor((newPosition[0] - d_grid_geometry->getXLower()[0])/dx[0]);
					indexj = floor((newPosition[1] - d_grid_geometry->getXLower()[1])/dx[1]);
					indexk = floor((newPosition[2] - d_grid_geometry->getXLower()[2])/dx[2]);
					hier::Index idx(indexi, indexj, indexk);
					if (indexi >= boxfirst(0) && indexi <= boxlast(0) && indexj >= boxfirst(1) && indexj <= boxlast(1) && indexk >= boxfirst(2) && indexk <= boxlast(2)) {
						NonSyncs* newnonSyncpart = nonSyncVariable->getItem(idx);
						NonSync* newnonSyncparticle = new NonSync(0, newPosition);
						NonSync* newnonSyncoldParticle = newnonSyncpart->overlaps(*newnonSyncparticle);
						if (newnonSyncoldParticle->getnonSync() == 0) {
							Point np;
							np.i = p.i;
							np.j = p.j-1;
							np.k = p.k;
							mystack.push(np);
						}
						delete newnonSyncparticle;
					}
				}
				if (lessThan(position[1] + particleSeparation_y, patch_geom->getXUpper()[1] + dx[1]*d_ghost_width)) {
					if (particleDistribution.compare("STAGGERED") == 0 && isEven(p.k)) {
						newPosition[0] = d_grid_geometry->getXLower()[0] - (floor(d_ghost_width * (dx[0]/particleSeparation_x)))*particleSeparation_x + (p.i)*particleSeparation_x;
						newPosition[1] = d_grid_geometry->getXLower()[1] - (floor(d_ghost_width * (dx[1]/particleSeparation_y)))*particleSeparation_y + (p.j + 1)*particleSeparation_y;
						newPosition[2] = d_grid_geometry->getXLower()[2] + particleSeparation_z/2.0 - (floor(0.5 + d_ghost_width * (dx[2]/particleSeparation_z)))*particleSeparation_z + (p.k)*particleSeparation_z;
					} else {
						newPosition[0] = d_grid_geometry->getXLower()[0] + particleSeparation_x/2.0 - (floor(0.5 + d_ghost_width * (dx[0]/particleSeparation_x)))*particleSeparation_x + (p.i)*particleSeparation_x;
						newPosition[1] = d_grid_geometry->getXLower()[1] + particleSeparation_y/2.0 - (floor(0.5 + d_ghost_width * (dx[1]/particleSeparation_y)))*particleSeparation_y + (p.j + 1)*particleSeparation_y;
						newPosition[2] = d_grid_geometry->getXLower()[2] + particleSeparation_z/2.0 - (floor(0.5 + d_ghost_width * (dx[2]/particleSeparation_z)))*particleSeparation_z + (p.k)*particleSeparation_z;
					}
					indexi = floor((newPosition[0] - d_grid_geometry->getXLower()[0])/dx[0]);
					indexj = floor((newPosition[1] - d_grid_geometry->getXLower()[1])/dx[1]);
					indexk = floor((newPosition[2] - d_grid_geometry->getXLower()[2])/dx[2]);
					hier::Index idx(indexi, indexj, indexk);
					if (indexi >= boxfirst(0) && indexi <= boxlast(0) && indexj >= boxfirst(1) && indexj <= boxlast(1) && indexk >= boxfirst(2) && indexk <= boxlast(2)) {
						NonSyncs* newnonSyncpart = nonSyncVariable->getItem(idx);
						NonSync* newnonSyncparticle = new NonSync(0, newPosition);
						NonSync* newnonSyncoldParticle = newnonSyncpart->overlaps(*newnonSyncparticle);
						if (newnonSyncoldParticle->getnonSync() == 0) {
							Point np;
							np.i = p.i;
							np.j = p.j+1;
							np.k = p.k;
							mystack.push(np);
						}
						delete newnonSyncparticle;
					}
				}
				if (greaterEq(position[2] - particleSeparation_z, patch_geom->getXLower()[2] - dx[2]*d_ghost_width)) {
					if (particleDistribution.compare("STAGGERED") == 0 && isEven(p.k - 1)) {
						newPosition[0] = d_grid_geometry->getXLower()[0] - (floor(d_ghost_width * (dx[0]/particleSeparation_x)))*particleSeparation_x + (p.i)*particleSeparation_x;
						newPosition[1] = d_grid_geometry->getXLower()[1] - (floor(d_ghost_width * (dx[1]/particleSeparation_y)))*particleSeparation_y + (p.j)*particleSeparation_y;
						newPosition[2] = d_grid_geometry->getXLower()[2] + particleSeparation_z/2.0 - (floor(0.5 + d_ghost_width * (dx[2]/particleSeparation_z)))*particleSeparation_z + (p.k - 1)*particleSeparation_z;
					} else {
						newPosition[0] = d_grid_geometry->getXLower()[0] + particleSeparation_x/2.0 - (floor(0.5 + d_ghost_width * (dx[0]/particleSeparation_x)))*particleSeparation_x + (p.i)*particleSeparation_x;
						newPosition[1] = d_grid_geometry->getXLower()[1] + particleSeparation_y/2.0 - (floor(0.5 + d_ghost_width * (dx[1]/particleSeparation_y)))*particleSeparation_y + (p.j)*particleSeparation_y;
						newPosition[2] = d_grid_geometry->getXLower()[2] + particleSeparation_z/2.0 - (floor(0.5 + d_ghost_width * (dx[2]/particleSeparation_z)))*particleSeparation_z + (p.k - 1)*particleSeparation_z;
					}
					indexi = floor((newPosition[0] - d_grid_geometry->getXLower()[0])/dx[0]);
					indexj = floor((newPosition[1] - d_grid_geometry->getXLower()[1])/dx[1]);
					indexk = floor((newPosition[2] - d_grid_geometry->getXLower()[2])/dx[2]);
					hier::Index idx(indexi, indexj, indexk);
					if (indexi >= boxfirst(0) && indexi <= boxlast(0) && indexj >= boxfirst(1) && indexj <= boxlast(1) && indexk >= boxfirst(2) && indexk <= boxlast(2)) {
						NonSyncs* newnonSyncpart = nonSyncVariable->getItem(idx);
						NonSync* newnonSyncparticle = new NonSync(0, newPosition);
						NonSync* newnonSyncoldParticle = newnonSyncpart->overlaps(*newnonSyncparticle);
						if (newnonSyncoldParticle->getnonSync() == 0) {
							Point np;
							np.i = p.i;
							np.j = p.j;
							np.k = p.k-1;
							mystack.push(np);
						}
						delete newnonSyncparticle;
					}
				}
				if (lessThan(position[2] + particleSeparation_z, patch_geom->getXUpper()[2] + dx[2]*d_ghost_width)) {
					if (particleDistribution.compare("STAGGERED") == 0 && isEven(p.k + 1)) {
						newPosition[0] = d_grid_geometry->getXLower()[0] - (floor(d_ghost_width * (dx[0]/particleSeparation_x)))*particleSeparation_x + (p.i)*particleSeparation_x;
						newPosition[1] = d_grid_geometry->getXLower()[1] - (floor(d_ghost_width * (dx[1]/particleSeparation_y)))*particleSeparation_y + (p.j)*particleSeparation_y;
						newPosition[2] = d_grid_geometry->getXLower()[2] + particleSeparation_z/2.0 - (floor(0.5 + d_ghost_width * (dx[2]/particleSeparation_z)))*particleSeparation_z + (p.k + 1)*particleSeparation_z;
					} else {
						newPosition[0] = d_grid_geometry->getXLower()[0] + particleSeparation_x/2.0 - (floor(0.5 + d_ghost_width * (dx[0]/particleSeparation_x)))*particleSeparation_x + (p.i)*particleSeparation_x;
						newPosition[1] = d_grid_geometry->getXLower()[1] + particleSeparation_y/2.0 - (floor(0.5 + d_ghost_width * (dx[1]/particleSeparation_y)))*particleSeparation_y + (p.j)*particleSeparation_y;
						newPosition[2] = d_grid_geometry->getXLower()[2] + particleSeparation_z/2.0 - (floor(0.5 + d_ghost_width * (dx[2]/particleSeparation_z)))*particleSeparation_z + (p.k + 1)*particleSeparation_z;
					}
					indexi = floor((newPosition[0] - d_grid_geometry->getXLower()[0])/dx[0]);
					indexj = floor((newPosition[1] - d_grid_geometry->getXLower()[1])/dx[1]);
					indexk = floor((newPosition[2] - d_grid_geometry->getXLower()[2])/dx[2]);
					hier::Index idx(indexi, indexj, indexk);
					if (indexi >= boxfirst(0) && indexi <= boxlast(0) && indexj >= boxfirst(1) && indexj <= boxlast(1) && indexk >= boxfirst(2) && indexk <= boxlast(2)) {
						NonSyncs* newnonSyncpart = nonSyncVariable->getItem(idx);
						NonSync* newnonSyncparticle = new NonSync(0, newPosition);
						NonSync* newnonSyncoldParticle = newnonSyncpart->overlaps(*newnonSyncparticle);
						if (newnonSyncoldParticle->getnonSync() == 0) {
							Point np;
							np.i = p.i;
							np.j = p.j;
							np.k = p.k+1;
							mystack.push(np);
						}
						delete newnonSyncparticle;
					}
				}
			}
			delete nonSyncparticle;
			delete particle;
		}
	}
}


/*
 * Checks if the point has a stencil width
 */
void Problem::setStencilLimits(std::shared_ptr< hier::Patch > patch, int i, int j, int k, int v) const {
	int* region = ((pdat::CellData<int> *) patch->getPatchData(d_region_id).get())->getPointer();
	int* interior_i = ((pdat::CellData<int> *) patch->getPatchData(d_interior_i_id).get())->getPointer();
	int* interior_j = ((pdat::CellData<int> *) patch->getPatchData(d_interior_j_id).get())->getPointer();
	int* interior_k = ((pdat::CellData<int> *) patch->getPatchData(d_interior_k_id).get())->getPointer();
	std::shared_ptr< pdat::IndexData<Particles, pdat::CellGeometry > > problemVariable(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles, pdat::CellGeometry >, hier::PatchData>(patch->getPatchData(d_problemVariable_id)));
	const hier::Index boxfirst = problemVariable->getGhostBox().lower();
	const hier::Index boxlast  = problemVariable->getGhostBox().upper();
	//Auxiliary definitions
	int ilast = boxlast(0)-boxfirst(0) + 1;
	int jlast = boxlast(1)-boxfirst(1) + 1;
	int klast = boxlast(2)-boxfirst(2) + 1;

	int iStart, iEnd, currentGhosti, otherSideShifti, jStart, jEnd, currentGhostj, otherSideShiftj, kStart, kEnd, currentGhostk, otherSideShiftk, shift;
	currentGhosti = d_ghost_width - 1;
	otherSideShifti = 0;
	currentGhostj = d_ghost_width - 1;
	otherSideShiftj = 0;
	currentGhostk = d_ghost_width - 1;
	otherSideShiftk = 0;
	//Checking width
	if ((i + 1 < ilast && vector(region, i + 1, j, k) != v) ||  (i - 1 >= 0 && vector(region, i - 1, j, k) != v)) {
		if (i + 1 < ilast && vector(region, i + 1, j, k) == v) {
			bool stop_counting = false;
			for(int iti = i + 1; iti <= i + d_ghost_width - 1 && currentGhosti > 0; iti++) {
				if (iti < ilast  && vector(region, iti, j, k) == v && stop_counting == false) {
					currentGhosti--;
				} else {
					//First not interior point found
					if (iti < ilast  && vector(region, iti, j, k) != v) {
						stop_counting = true;
					}
					//Physical boundary reach
					if (iti >= ilast - d_ghost_width && patch->getPatchGeometry()->getTouchesRegularBoundary (0, 1)) {
						stop_counting = true;
						//Calculate the number of cells the limit cannot grow
						if (iti + currentGhosti/2 >= ilast - d_ghost_width) {
							otherSideShifti = (iti  + currentGhosti/2) - (ilast - d_ghost_width);
						}
					}
				}
			}
		}
		if (i - 1 >= 0 && vector(region, i - 1, j, k) == v) {
			bool stop_counting = false;
			for(int iti = i - 1; iti >= i - d_ghost_width + 1 && currentGhosti > 0; iti--) {
				if (iti >= 0  && vector(region, i - 1, j, k) == v) {
					currentGhosti--;
				} else {
					//First not interior point found
					if (iti >= 0 && vector(region, iti, j, k) != v) {
						stop_counting = true;
					}
					//Physical boundary reach
					if (iti < d_ghost_width && patch->getPatchGeometry()->getTouchesRegularBoundary (0, 0)) {
						stop_counting = true;
						//calculate the number of cells the limit cannot grow
						if (iti  -  ((currentGhosti) - currentGhosti/2) < d_ghost_width) {
							otherSideShifti = d_ghost_width - (iti  - ((currentGhosti) - currentGhosti/2));
						}
					}
				}
			}
		}
		if (currentGhosti > 0) {
			if (i + 1 < ilast && vector(region, i + 1, j, k) == v) {
				shift = 0;
				if(patch->getPatchGeometry()->getTouchesRegularBoundary (0, 0)) {
					while(i - ((currentGhosti) - currentGhosti/2) + shift < d_ghost_width) {
						shift++;
					}
				}
				iStart = (currentGhosti - currentGhosti/2) - shift - otherSideShifti;
				iEnd = 0;
			} else {
				if (i - 1 >= 0 && vector(region, i - 1, j, k) == v) {
					shift = 0;
					if(patch->getPatchGeometry()->getTouchesRegularBoundary (0, 1)) {
						while(i + currentGhosti/2 + shift >= ilast - d_ghost_width) {
							shift--;
						}
					}
					iStart = 0;
					iEnd = currentGhosti/2 + shift + otherSideShifti;
				} else {
					shift = 0;
					if(patch->getPatchGeometry()->getTouchesRegularBoundary (0, 0)) {
						while(i - ((currentGhosti) - currentGhosti/2) + shift < d_ghost_width) {
							shift++;
						}
					}
					if(patch->getPatchGeometry()->getTouchesRegularBoundary (0, 1)) {
						while(i + currentGhosti/2 + shift >= ilast - d_ghost_width) {
							shift--;
						}
					}
					iStart = (currentGhosti - currentGhosti/2) - shift;
					iEnd = currentGhosti/2 + shift;
				}
			}
		} else {
			iStart = 0;
			iEnd = 0;
		}
	} else {
		iStart = 0;
		iEnd = 0;
	}
	if ((j + 1 < jlast && vector(region, i, j + 1, k) != v) ||  (j - 1 >= 0 && vector(region, i, j - 1, k) != v)) {
		if (j + 1 < jlast && vector(region, i, j + 1, k) == v) {
			bool stop_counting = false;
			for(int itj = j + 1; itj <= j + d_ghost_width - 1 && currentGhostj > 0; itj++) {
				if (itj < jlast  && vector(region, i, itj, k) == v && stop_counting == false) {
					currentGhostj--;
				} else {
					//First not interior point found
					if (itj < jlast  && vector(region, i, itj, k) != v) {
						stop_counting = true;
					}
					//Physical boundary reach
					if (itj >= jlast - d_ghost_width && patch->getPatchGeometry()->getTouchesRegularBoundary (1, 1)) {
						stop_counting = true;
						//Calculate the number of cells the limit cannot grow
						if (itj + currentGhostj/2 >= jlast - d_ghost_width) {
							otherSideShiftj = (itj  + currentGhostj/2) - (jlast - d_ghost_width);
						}
					}
				}
			}
		}
		if (j - 1 >= 0 && vector(region, i, j - 1, k) == v) {
			bool stop_counting = false;
			for(int itj = j - 1; itj >= j - d_ghost_width + 1 && currentGhostj > 0; itj--) {
				if (itj >= 0  && vector(region, i, j - 1, k) == v) {
					currentGhostj--;
				} else {
					//First not interior point found
					if (itj >= 0 && vector(region, i, itj, k) != v) {
						stop_counting = true;
					}
					//Physical boundary reach
					if (itj < d_ghost_width && patch->getPatchGeometry()->getTouchesRegularBoundary (1, 0)) {
						stop_counting = true;
						//calculate the number of cells the limit cannot grow
						if (itj  -  ((currentGhostj) - currentGhostj/2) < d_ghost_width) {
							otherSideShiftj = d_ghost_width - (itj  - ((currentGhostj) - currentGhostj/2));
						}
					}
				}
			}
		}
		if (currentGhostj > 0) {
			if (j + 1 < jlast && vector(region, i, j + 1, k) == v) {
				shift = 0;
				if(patch->getPatchGeometry()->getTouchesRegularBoundary (1, 0)) {
					while(j - ((currentGhostj) - currentGhostj/2) + shift < d_ghost_width) {
						shift++;
					}
				}
				jStart = (currentGhostj - currentGhostj/2) - shift - otherSideShiftj;
				jEnd = 0;
			} else {
				if (j - 1 >= 0 && vector(region, i, j - 1, k) == v) {
					shift = 0;
					if(patch->getPatchGeometry()->getTouchesRegularBoundary (1, 1)) {
						while(j + currentGhostj/2 + shift >= jlast - d_ghost_width) {
							shift--;
						}
					}
					jStart = 0;
					jEnd = currentGhostj/2 + shift + otherSideShiftj;
				} else {
					shift = 0;
					if(patch->getPatchGeometry()->getTouchesRegularBoundary (1, 0)) {
						while(j - ((currentGhostj) - currentGhostj/2) + shift < d_ghost_width) {
							shift++;
						}
					}
					if(patch->getPatchGeometry()->getTouchesRegularBoundary (1, 1)) {
						while(j + currentGhostj/2 + shift >= jlast - d_ghost_width) {
							shift--;
						}
					}
					jStart = (currentGhostj - currentGhostj/2) - shift;
					jEnd = currentGhostj/2 + shift;
				}
			}
		} else {
			jStart = 0;
			jEnd = 0;
		}
	} else {
		jStart = 0;
		jEnd = 0;
	}
	if ((k + 1 < klast && vector(region, i, j, k + 1) != v) ||  (k - 1 >= 0 && vector(region, i, j, k - 1) != v)) {
		if (k + 1 < klast && vector(region, i, j, k + 1) == v) {
			bool stop_counting = false;
			for(int itk = k + 1; itk <= k + d_ghost_width - 1 && currentGhostk > 0; itk++) {
				if (itk < klast  && vector(region, i, j, itk) == v && stop_counting == false) {
					currentGhostk--;
				} else {
					//First not interior point found
					if (itk < klast  && vector(region, i, j, itk) != v) {
						stop_counting = true;
					}
					//Physical boundary reach
					if (itk >= klast - d_ghost_width && patch->getPatchGeometry()->getTouchesRegularBoundary (2, 1)) {
						stop_counting = true;
						//Calculate the number of cells the limit cannot grow
						if (itk + currentGhostk/2 >= klast - d_ghost_width) {
							otherSideShiftk = (itk  + currentGhostk/2) - (klast - d_ghost_width);
						}
					}
				}
			}
		}
		if (k - 1 >= 0 && vector(region, i, j, k - 1) == v) {
			bool stop_counting = false;
			for(int itk = k - 1; itk >= k - d_ghost_width + 1 && currentGhostk > 0; itk--) {
				if (itk >= 0  && vector(region, i, j, k - 1) == v) {
					currentGhostk--;
				} else {
					//First not interior point found
					if (itk >= 0 && vector(region, i, j, itk) != v) {
						stop_counting = true;
					}
					//Physical boundary reach
					if (itk < d_ghost_width && patch->getPatchGeometry()->getTouchesRegularBoundary (2, 0)) {
						stop_counting = true;
						//calculate the number of cells the limit cannot grow
						if (itk  -  ((currentGhostk) - currentGhostk/2) < d_ghost_width) {
							otherSideShiftk = d_ghost_width - (itk  - ((currentGhostk) - currentGhostk/2));
						}
					}
				}
			}
		}
		if (currentGhostk > 0) {
			if (k + 1 < klast && vector(region, i, j, k + 1) == v) {
				shift = 0;
				if(patch->getPatchGeometry()->getTouchesRegularBoundary (2, 0)) {
					while(k - ((currentGhostk) - currentGhostk/2) + shift < d_ghost_width) {
						shift++;
					}
				}
				kStart = (currentGhostk - currentGhostk/2) - shift - otherSideShiftk;
				kEnd = 0;
			} else {
				if (k - 1 >= 0 && vector(region, i, j, k - 1) == v) {
					shift = 0;
					if(patch->getPatchGeometry()->getTouchesRegularBoundary (2, 1)) {
						while(k + currentGhostk/2 + shift >= klast - d_ghost_width) {
							shift--;
						}
					}
					kStart = 0;
					kEnd = currentGhostk/2 + shift + otherSideShiftk;
				} else {
					shift = 0;
					if(patch->getPatchGeometry()->getTouchesRegularBoundary (2, 0)) {
						while(k - ((currentGhostk) - currentGhostk/2) + shift < d_ghost_width) {
							shift++;
						}
					}
					if(patch->getPatchGeometry()->getTouchesRegularBoundary (2, 1)) {
						while(k + currentGhostk/2 + shift >= klast - d_ghost_width) {
							shift--;
						}
					}
					kStart = (currentGhostk - currentGhostk/2) - shift;
					kEnd = currentGhostk/2 + shift;
				}
			}
		} else {
			kStart = 0;
			kEnd = 0;
		}
	} else {
		kStart = 0;
		kEnd = 0;
	}
	//Assigning stencil limits
	for(int iti = i - iStart; iti <= i + iEnd; iti++) {
		for(int itj = j - jStart; itj <= j + jEnd; itj++) {
			for(int itk = k - kStart; itk <= k + kEnd; itk++) {
				if(iti >= 0 && iti < ilast && itj >= 0 && itj < jlast && itk >= 0 && itk < klast && vector(region, iti, itj, itk) != v) {
					if (i - iti < 0) {
						vector(interior_i, iti, itj, itk) = - (iStart + 1) - (i - iti);
					} else {
						vector(interior_i, iti, itj, itk) = (iStart + 1) - (i - iti);
					}
					if (j - itj < 0) {
						vector(interior_j, iti, itj, itk) = - (jStart + 1) - (j - itj);
					} else {
						vector(interior_j, iti, itj, itk) = (jStart + 1) - (j - itj);
					}
					if (k - itk < 0) {
						vector(interior_k, iti, itj, itk) = - (kStart + 1) - (k - itk);
					} else {
						vector(interior_k, iti, itj, itk) = (kStart + 1) - (k - itk);
					}
				}
			}
		}
	}
}

/*
 * Checks if the point has a stencil width
 */
void Problem::checkStencilCell(std::shared_ptr< hier::Patch > patch, int i, int j, int k, int v) const {
	int* region = ((pdat::CellData<int> *) patch->getPatchData(d_region_id).get())->getPointer();
	int* interior_i = ((pdat::CellData<int> *) patch->getPatchData(d_interior_i_id).get())->getPointer();
	int* interior_j = ((pdat::CellData<int> *) patch->getPatchData(d_interior_j_id).get())->getPointer();
	int* interior_k = ((pdat::CellData<int> *) patch->getPatchData(d_interior_k_id).get())->getPointer();
	std::shared_ptr< pdat::IndexData<Particles, pdat::CellGeometry > > problemVariable(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles, pdat::CellGeometry >, hier::PatchData>(patch->getPatchData(d_problemVariable_id)));

	const hier::Index boxfirst = problemVariable->getGhostBox().lower();
	const hier::Index boxlast  = problemVariable->getGhostBox().upper();
	//Auxiliary definitions
	int ilast = boxlast(0)-boxfirst(0) + 1;
	int jlast = boxlast(1)-boxfirst(1) + 1;
	int klast = boxlast(2)-boxfirst(2) + 1;

	int i_i = vector(interior_i, i, j, k);
	int iStart = MAX(0, i_i) - 1;
	int iEnd = MAX(0, -i_i) - 1;
	int i_j = vector(interior_j, i, j, k);
	int jStart = MAX(0, i_j) - 1;
	int jEnd = MAX(0, -i_j) - 1;
	int i_k = vector(interior_k, i, j, k);
	int kStart = MAX(0, i_k) - 1;
	int kEnd = MAX(0, -i_k) - 1;

	for(int iti = i - iStart; iti <= i + iEnd; iti++) {
		for(int itj = j - jStart; itj <= j + jEnd; itj++) {
			for(int itk = k - kStart; itk <= k + kEnd; itk++) {
				if(iti >= 0 && iti < ilast && itj >= 0 && itj < jlast && itk >= 0 && itk < klast && vector(region, iti, itj, itk) != v) {
					vector(interior_i, iti, itj, itk) = i_i  - (i - iti);
					vector(interior_j, iti, itj, itk) = i_j  - (j - itj);
					vector(interior_k, iti, itj, itk) = i_k  - (k - itk);
				}
			}
		}
	}
}


void Problem::floodfillCell(const hier::Patch& patch, int i, int j, int k, int pred, int seg) const {

	int* nonSync = ((pdat::CellData<int> *) patch.getPatchData(d_nonSync_id).get())->getPointer();
	int* interior = ((pdat::CellData<int> *) patch.getPatchData(d_interior_id).get())->getPointer();
	int* region = ((pdat::CellData<int> *) patch.getPatchData(d_region_id).get())->getPointer();
	std::shared_ptr< pdat::IndexData<Particles, pdat::CellGeometry > > problemVariable(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles, pdat::CellGeometry >, hier::PatchData>(patch.getPatchData(d_problemVariable_id)));
	const hier::Index boxfirst = problemVariable->getGhostBox().lower();
	const hier::Index boxlast  = problemVariable->getGhostBox().upper();

	//Auxiliary definitions
	int ilast = boxlast(0)-boxfirst(0) + 1;
	int jlast = boxlast(1)-boxfirst(1) + 1;
	int klast = boxlast(2)-boxfirst(2) + 1;

	stack<Point> mystack;

	Point p;
	p.i = i;
	p.j = j;
	p.k = k;

	mystack.push(p);
	while(mystack.size() > 0) {
		p = mystack.top();
		mystack.pop();
		if (vector(nonSync, p.i, p.j, p.k) == 0) {
			vector(nonSync, p.i, p.j, p.k) = pred;
			vector(interior, p.i, p.j, p.k) = pred;
			if (pred == 2) {
				vector(region, p.i, p.j, p.k) = seg;
			}
			if (p.i - 1 >= 0 && vector(nonSync, p.i - 1, p.j, p.k) == 0) {
				Point np;
				np.i = p.i-1;
				np.j = p.j;
				np.k = p.k;
				mystack.push(np);
			}
			if (p.i + 1 < ilast && vector(nonSync, p.i + 1, p.j, p.k) == 0) {
				Point np;
				np.i = p.i+1;
				np.j = p.j;
				np.k = p.k;
				mystack.push(np);
			}
			if (p.j - 1 >= 0 && vector(nonSync, p.i, p.j - 1, p.k) == 0) {
				Point np;
				np.i = p.i;
				np.j = p.j-1;
				np.k = p.k;
				mystack.push(np);
			}
			if (p.j + 1 < jlast && vector(nonSync, p.i, p.j + 1, p.k) == 0) {
				Point np;
				np.i = p.i;
				np.j = p.j+1;
				np.k = p.k;
				mystack.push(np);
			}
			if (p.k - 1 >= 0 && vector(nonSync, p.i, p.j, p.k - 1) == 0) {
				Point np;
				np.i = p.i;
				np.j = p.j;
				np.k = p.k-1;
				mystack.push(np);
			}
			if (p.k + 1 < klast && vector(nonSync, p.i, p.j, p.k + 1) == 0) {
				Point np;
				np.i = p.i;
				np.j = p.j;
				np.k = p.k+1;
				mystack.push(np);
			}
		}
	}
}








/*
 * Initialize data on a patch. This initialization is done only at the begining of the simulation.
 */
void Problem::initializeDataOnPatch(hier::Patch& patch, 
                                    const double time,
                                    const bool initial_time)
{
	(void) time;
   	if (initial_time) {
		// Initial conditions		
		//Get fields, auxiliary fields and local variables that are going to be used.
		std::shared_ptr< pdat::IndexData<Particles, pdat::CellGeometry > > problemVariable(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles, pdat::CellGeometry >, hier::PatchData>(patch.getPatchData(d_problemVariable_id)));
		int* seg1 = ((pdat::CellData<int> *) patch.getPatchData(d_seg1_id).get())->getPointer();
		int* segL1 = ((pdat::CellData<int> *) patch.getPatchData(d_segL1_id).get())->getPointer();
		int* seg3 = ((pdat::CellData<int> *) patch.getPatchData(d_seg3_id).get())->getPointer();
		int* segL3 = ((pdat::CellData<int> *) patch.getPatchData(d_segL3_id).get())->getPointer();
		int* segxLower = ((pdat::CellData<int> *) patch.getPatchData(d_segxLower_id).get())->getPointer();
		int* segLxLower = ((pdat::CellData<int> *) patch.getPatchData(d_segLxLower_id).get())->getPointer();
		int* segxUpper = ((pdat::CellData<int> *) patch.getPatchData(d_segxUpper_id).get())->getPointer();
		int* segLxUpper = ((pdat::CellData<int> *) patch.getPatchData(d_segLxUpper_id).get())->getPointer();
		int* segyLower = ((pdat::CellData<int> *) patch.getPatchData(d_segyLower_id).get())->getPointer();
		int* segLyLower = ((pdat::CellData<int> *) patch.getPatchData(d_segLyLower_id).get())->getPointer();
		int* segyUpper = ((pdat::CellData<int> *) patch.getPatchData(d_segyUpper_id).get())->getPointer();
		int* segLyUpper = ((pdat::CellData<int> *) patch.getPatchData(d_segLyUpper_id).get())->getPointer();
		int* segzLower = ((pdat::CellData<int> *) patch.getPatchData(d_segzLower_id).get())->getPointer();
		int* segLzLower = ((pdat::CellData<int> *) patch.getPatchData(d_segLzLower_id).get())->getPointer();
		int* segzUpper = ((pdat::CellData<int> *) patch.getPatchData(d_segzUpper_id).get())->getPointer();
		int* segLzUpper = ((pdat::CellData<int> *) patch.getPatchData(d_segLzUpper_id).get())->getPointer();
		int* region = ((pdat::CellData<int> *) patch.getPatchData(d_region_id).get())->getPointer();
		const hier::Index boxfirst = problemVariable->getGhostBox().lower();
		const hier::Index boxlast  = problemVariable->getGhostBox().upper();
		
		const hier::Index boxfirst1 = patch.getBox().lower();
		const hier::Index boxlast1 = patch.getBox().upper();
		
		int ilast = boxlast1(0)-boxfirst1(0) + 1 + 2 * d_ghost_width;
		int jlast = boxlast1(1)-boxfirst1(1) + 1 + 2 * d_ghost_width;
		int klast = boxlast1(2)-boxfirst1(2) + 1 + 2 * d_ghost_width;
		
		
		
		for(int index2 = boxfirst(2); index2 <= boxlast(2); index2++) {
			for(int index1 = boxfirst(1); index1 <= boxlast(1); index1++) {
				for(int index0 = boxfirst(0); index0 <= boxlast(0); index0++) {
					hier::Index idx(index0, index1, index2);
					Particles* part = problemVariable->getItem(idx);
					for (int pit = 0; pit < part->getNumberOfParticles(); pit++) {
						Particle* particle = part->getParticle(pit);
						if (particle->region == 3 || particle->region == 4) {
							particle->rho = 1.0;
							particle->mx = 0.0;
							particle->my = 0.0;
							particle->mz = 0.0;
							particle->e = 1.0;
							particle->fx = 0.01;
							particle->fy = 0.0;
							particle->fz = 0.0;
							particle->p = 1.5;
							particle->mass = particle->rho * particleSeparation_x * particleSeparation_y * particleSeparation_z;
						}
		
						if (particle->region == 1 || particle->region == 2) {
							particle->rho = 1.0;
							particle->mx = 0.0;
							particle->my = 0.0;
							particle->mz = 0.0;
							particle->e = 1.0;
							particle->fx = 0.01;
							particle->fy = 0.0;
							particle->fz = 0.0;
							particle->p = 1.5;
							particle->mass = particle->rho * particleSeparation_x * particleSeparation_y * particleSeparation_z;
						}
		
						//Boundaries Initialization
					}
				}
			}
		}
		
		//Delete invalid particles
		for(int index2 = boxfirst(2); index2 <= boxlast(2); index2++) {
			for(int index1 = boxfirst(1); index1 <= boxlast(1); index1++) {
				for(int index0 = boxfirst(0); index0 <= boxlast(0); index0++) {
					hier::Index idx(index0, index1, index2);
					Particles* part = problemVariable->getItem(idx);
					for (int pit = part->getNumberOfParticles() - 1; pit >= 0; pit--) {
						Particle* particle = part->getParticle(pit);
						if (particle->region == 3 || particle->region == 4) {
							if (Equals(particle->rho,0)) {
								part->deleteParticle(*particle);
							}
						}
						if (particle->region == 1 || particle->region == 2) {
							if (Equals(particle->rho,0)) {
								part->deleteParticle(*particle);
							}
						}
						if (particle->region == 0) {
							part->deleteParticle(*particle);
						}
					}
				}
			}
		}
		//Region's cells influence initialization
		for(int index2 = boxfirst(2); index2 <= boxlast(2); index2++) {
			for(int index1 = boxfirst(1); index1 <= boxlast(1); index1++) {
				for(int index0 = boxfirst(0); index0 <= boxlast(0); index0++) {
					hier::Index idx(index0, index1, index2);
					Particles* part = problemVariable->getItem(idx);
					for (int pit = part->getNumberOfParticles() - 1; pit >= 0; pit--) {
						Particle* particle = part->getParticle(pit);
						if (particle->region == 4 || particle->region == 3) {
							vector(segL3, index0 - boxfirst(0), index1 - boxfirst(1), index2 - boxfirst(2)) = 1;
						}
						if (particle->region == 2 || particle->region == 1) {
							vector(segL1, index0 - boxfirst(0), index1 - boxfirst(1), index2 - boxfirst(2)) = 1;
						}
						if (particle->region == -1) {
							vector(segLxLower, index0 - boxfirst(0), index1 - boxfirst(1), index2 - boxfirst(2)) = 1;
						}
						if (particle->region == -2) {
							vector(segLxUpper, index0 - boxfirst(0), index1 - boxfirst(1), index2 - boxfirst(2)) = 1;
						}
						if (particle->region == -3) {
							vector(segLyLower, index0 - boxfirst(0), index1 - boxfirst(1), index2 - boxfirst(2)) = 1;
						}
						if (particle->region == -4) {
							vector(segLyUpper, index0 - boxfirst(0), index1 - boxfirst(1), index2 - boxfirst(2)) = 1;
						}
						if (particle->region == -5) {
							vector(segLzLower, index0 - boxfirst(0), index1 - boxfirst(1), index2 - boxfirst(2)) = 1;
						}
						if (particle->region == -6) {
							vector(segLzUpper, index0 - boxfirst(0), index1 - boxfirst(1), index2 - boxfirst(2)) = 1;
						}
					}
					if (vector(segL3, index0 - boxfirst(0), index1 - boxfirst(1), index2 - boxfirst(2)) == -1) {
						vector(segL3, index0 - boxfirst(0), index1 - boxfirst(1), index2 - boxfirst(2)) = 0;
					}
					if (vector(segL1, index0 - boxfirst(0), index1 - boxfirst(1), index2 - boxfirst(2)) == -1) {
						vector(segL1, index0 - boxfirst(0), index1 - boxfirst(1), index2 - boxfirst(2)) = 0;
					}
					if (vector(segLxLower, index0 - boxfirst(0), index1 - boxfirst(1), index2 - boxfirst(2)) == -1) {
						vector(segLxLower, index0 - boxfirst(0), index1 - boxfirst(1), index2 - boxfirst(2)) = 0;
					}
					if (vector(segLxUpper, index0 - boxfirst(0), index1 - boxfirst(1), index2 - boxfirst(2)) == -1) {
						vector(segLxUpper, index0 - boxfirst(0), index1 - boxfirst(1), index2 - boxfirst(2)) = 0;
					}
					if (vector(segLyLower, index0 - boxfirst(0), index1 - boxfirst(1), index2 - boxfirst(2)) == -1) {
						vector(segLyLower, index0 - boxfirst(0), index1 - boxfirst(1), index2 - boxfirst(2)) = 0;
					}
					if (vector(segLyUpper, index0 - boxfirst(0), index1 - boxfirst(1), index2 - boxfirst(2)) == -1) {
						vector(segLyUpper, index0 - boxfirst(0), index1 - boxfirst(1), index2 - boxfirst(2)) = 0;
					}
					if (vector(segLzLower, index0 - boxfirst(0), index1 - boxfirst(1), index2 - boxfirst(2)) == -1) {
						vector(segLzLower, index0 - boxfirst(0), index1 - boxfirst(1), index2 - boxfirst(2)) = 0;
					}
					if (vector(segLzUpper, index0 - boxfirst(0), index1 - boxfirst(1), index2 - boxfirst(2)) == -1) {
						vector(segLzUpper, index0 - boxfirst(0), index1 - boxfirst(1), index2 - boxfirst(2)) = 0;
					}
		
					int miniIndex = index0 - ceil((2 * influenceRadius_0 + particleSeparation_x)/dx[0]);
					int maxiIndex = index0 + ceil((2 * influenceRadius_0 + particleSeparation_x)/dx[0]);
					if (miniIndex < boxfirst(0)) {
						miniIndex = boxfirst(0);
					}
					if (maxiIndex > boxlast(0)) {
						maxiIndex = boxlast(0);
					}
					int minjIndex = index1 - ceil((2 * influenceRadius_0 + particleSeparation_y)/dx[1]);
					int maxjIndex = index1 + ceil((2 * influenceRadius_0 + particleSeparation_y)/dx[1]);
					if (minjIndex < boxfirst(1)) {
						minjIndex = boxfirst(1);
					}
					if (maxjIndex > boxlast(1)) {
						maxjIndex = boxlast(1);
					}
					int minkIndex = index2 - ceil((2 * influenceRadius_0 + particleSeparation_z)/dx[2]);
					int maxkIndex = index2 + ceil((2 * influenceRadius_0 + particleSeparation_z)/dx[2]);
					if (minkIndex < boxfirst(2)) {
						minkIndex = boxfirst(2);
					}
					if (maxkIndex > boxlast(2)) {
						maxkIndex = boxlast(2);
					}
					for(int index2b = minkIndex; index2b <= maxkIndex; index2b++) {
						for(int index1b = minjIndex; index1b <= maxjIndex; index1b++) {
							for(int index0b = miniIndex; index0b <= maxiIndex; index0b++) {
								if (vector(segL3, index0 - boxfirst(0), index1 - boxfirst(1), index2 - boxfirst(2)) == 1) {
									vector(seg3, index0b - boxfirst(0), index1b - boxfirst(1), index2b - boxfirst(2)) = 1;
								}
								if (vector(segL1, index0 - boxfirst(0), index1 - boxfirst(1), index2 - boxfirst(2)) == 1) {
									vector(seg1, index0b - boxfirst(0), index1b - boxfirst(1), index2b - boxfirst(2)) = 1;
								}
								if (vector(segLxLower, index0 - boxfirst(0), index1 - boxfirst(1), index2 - boxfirst(2)) == 1) {
									vector(segxLower, index0b - boxfirst(0), index1b - boxfirst(1), index2b - boxfirst(2)) = 1;
								}
								if (vector(segLxUpper, index0 - boxfirst(0), index1 - boxfirst(1), index2 - boxfirst(2)) == 1) {
									vector(segxUpper, index0b - boxfirst(0), index1b - boxfirst(1), index2b - boxfirst(2)) = 1;
								}
								if (vector(segLyLower, index0 - boxfirst(0), index1 - boxfirst(1), index2 - boxfirst(2)) == 1) {
									vector(segyLower, index0b - boxfirst(0), index1b - boxfirst(1), index2b - boxfirst(2)) = 1;
								}
								if (vector(segLyUpper, index0 - boxfirst(0), index1 - boxfirst(1), index2 - boxfirst(2)) == 1) {
									vector(segyUpper, index0b - boxfirst(0), index1b - boxfirst(1), index2b - boxfirst(2)) = 1;
								}
								if (vector(segLzLower, index0 - boxfirst(0), index1 - boxfirst(1), index2 - boxfirst(2)) == 1) {
									vector(segzLower, index0b - boxfirst(0), index1b - boxfirst(1), index2b - boxfirst(2)) = 1;
								}
								if (vector(segLzUpper, index0 - boxfirst(0), index1 - boxfirst(1), index2 - boxfirst(2)) == 1) {
									vector(segzUpper, index0b - boxfirst(0), index1b - boxfirst(1), index2b - boxfirst(2)) = 1;
								}
							}
						}
					}
					if (vector(seg3, index0 - boxfirst(0), index1 - boxfirst(1), index2 - boxfirst(2)) == -1) {
						vector(seg3, index0 - boxfirst(0), index1 - boxfirst(1), index2 - boxfirst(2)) = 0;
					}
					if (vector(seg1, index0 - boxfirst(0), index1 - boxfirst(1), index2 - boxfirst(2)) == -1) {
						vector(seg1, index0 - boxfirst(0), index1 - boxfirst(1), index2 - boxfirst(2)) = 0;
					}
					if (vector(segxLower, index0 - boxfirst(0), index1 - boxfirst(1), index2 - boxfirst(2)) == -1) {
						vector(segxLower, index0 - boxfirst(0), index1 - boxfirst(1), index2 - boxfirst(2)) = 0;
					}
					if (vector(segxUpper, index0 - boxfirst(0), index1 - boxfirst(1), index2 - boxfirst(2)) == -1) {
						vector(segxUpper, index0 - boxfirst(0), index1 - boxfirst(1), index2 - boxfirst(2)) = 0;
					}
					if (vector(segyLower, index0 - boxfirst(0), index1 - boxfirst(1), index2 - boxfirst(2)) == -1) {
						vector(segyLower, index0 - boxfirst(0), index1 - boxfirst(1), index2 - boxfirst(2)) = 0;
					}
					if (vector(segyUpper, index0 - boxfirst(0), index1 - boxfirst(1), index2 - boxfirst(2)) == -1) {
						vector(segyUpper, index0 - boxfirst(0), index1 - boxfirst(1), index2 - boxfirst(2)) = 0;
					}
					if (vector(segzLower, index0 - boxfirst(0), index1 - boxfirst(1), index2 - boxfirst(2)) == -1) {
						vector(segzLower, index0 - boxfirst(0), index1 - boxfirst(1), index2 - boxfirst(2)) = 0;
					}
					if (vector(segzUpper, index0 - boxfirst(0), index1 - boxfirst(1), index2 - boxfirst(2)) == -1) {
						vector(segzUpper, index0 - boxfirst(0), index1 - boxfirst(1), index2 - boxfirst(2)) = 0;
					}
				}
			}
		}
		

   	}
}

/*
 * Gets the coarser patch that contains the box
 */
const std::shared_ptr<hier::Patch >& Problem::getCoarserPatch(
	const std::shared_ptr< hier::PatchLevel >& level,
	const hier::Box interior, 
	const hier::IntVector ratio)
{
	const hier::Box& coarsenBox = hier::Box::coarsen(interior, ratio);
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr< hier::Patch >& patch = *p_it;
		const hier::Box& interior_C = patch->getBox();
		if (interior_C.intersects(coarsenBox)) {
			return patch;		
		}
	}
}

/*
 * Reset the hierarchy-dependent internal information.
 */
void Problem::resetHierarchyConfiguration (
   const std::shared_ptr<hier::PatchHierarchy >& new_hierarchy ,
   int coarsest_level ,
   int finest_level )
{
	int finest_hiera_level = new_hierarchy->getFinestLevelNumber();

   	//  If we have added or removed a level, resize the schedule arrays

	d_bdry_sched_advance.resize(finest_hiera_level+1);
	d_coarsen_schedule.resize(finest_hiera_level+1);
	//  Build coarsen and refine communication schedules.
	for (int ln = coarsest_level; ln <= finest_hiera_level; ln++) {
		std::shared_ptr< hier::PatchLevel > level(new_hierarchy->getPatchLevel(ln));
		d_bdry_sched_advance[ln] = d_bdry_fill_advance->createSchedule(level,ln-1,new_hierarchy,this);
		// coarsen schedule only for levels > 0
		if (ln > 0) {
			std::shared_ptr< hier::PatchLevel > coarser_level(new_hierarchy->getPatchLevel(ln-1));
			d_coarsen_schedule[ln] = d_coarsen_algorithm->createSchedule(coarser_level, level, NULL);
		}
	}

}




/*
 * This method sets the physical boundary conditions.
 */
void Problem::setPhysicalBoundaryConditions(
   hier::Patch& patch,
   const double fill_time,
   const hier::IntVector& ghost_width_to_fill)
{
	//Boundary must not be implemented in this method
}

/*
 * Set up external plotter to plot internal data from this class.  
 * Register variables appropriate for plotting.                       
 */
int Problem::setupPlotter(
  ParticleDataWriter &plotter
) const {
	if (!d_patch_hierarchy) {
		TBOX_ERROR(d_object_name << ": No hierarchy in\n"
			<< " Problem::setupPlotter\n"
                 	<< "The hierarchy must be set before calling\n"
                 	<< "this function.\n");
   	}
	set<string> fields;
	fields.insert("rho");
	fields.insert("mx");
	fields.insert("my");
	fields.insert("mz");
	fields.insert("e");
	fields.insert("fx");
	fields.insert("fy");
	fields.insert("fz");
	fields.insert("p");
	hier::VariableDatabase *vdb = hier::VariableDatabase::getDatabase();
	for (vector<string>::const_iterator it = d_full_writer_variables.begin() ; it != d_full_writer_variables.end(); ++it) {
		string var_to_register = *it;
		if (!(fields.end() != fields.find(var_to_register))) {
			TBOX_ERROR(d_object_name << ": Variable selected for 3D write not found:" <<  var_to_register);
		}
		plotter.registerPlotVariable(var_to_register,d_problemVariable_id);
	}


   	return 0;
}


/*
 * Perform a single step from the discretization schema algorithm   
 */
double Problem::advanceLevel(
   const std::shared_ptr<hier::PatchLevel>& level,
   const std::shared_ptr<hier::PatchHierarchy>& hierarchy,
   const double current_time,
   const double new_time,
   const bool first_step,
   const bool last_step,
   const bool regrid_advance)
{
	const int ln = level->getLevelNumber();

	const double simPlat_dt = new_time - current_time;
  const double level_ratio = level->getRatioToCoarserLevel().max();

	t_step->start();
  	// Shifting time
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch > patch = *p_it;
		std::shared_ptr< pdat::IndexData<Particles, pdat::CellGeometry > > problemVariable(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles, pdat::CellGeometry >, hier::PatchData>(patch->getPatchData(d_problemVariable_id)));
		const hier::Index boxfirst = problemVariable->getGhostBox().lower();
		const hier::Index boxlast  = problemVariable->getGhostBox().upper();
		for (int index2 = boxfirst(2); index2 <= boxlast(2); index2++) {
			for (int index1 = boxfirst(1); index1 <= boxlast(1); index1++) {
				for (int index0 = boxfirst(0); index0 <= boxlast(0); index0++) {
					hier::Index idx(index0, index1, index2);
					Particles* part = problemVariable->getItem(idx);
					for (int pit = part->getNumberOfParticles() - 1; pit >= 0; pit--) {
						Particle* particle = part->getParticle(pit);
						particle->rho_p = particle->rho;
						particle->my_p = particle->my;
						particle->mz_p = particle->mz;
						particle->mx_p = particle->mx;
						particle->fy_p = particle->fy;
						particle->p_p = particle->p;
						particle->fx_p = particle->fx;
						particle->e_p = particle->e;
						particle->fz_p = particle->fz;
						particle->positionk_p = particle->positionk;
						particle->positionj_p = particle->positionj;
						particle->positioni_p = particle->positioni;
					}
				}
			}
		}
	}
  	// Evolution
	int pitbmin, index0min, index1min;
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch > patch = *p_it;
		std::shared_ptr< pdat::IndexData<Particles, pdat::CellGeometry > > problemVariable(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles, pdat::CellGeometry >, hier::PatchData>(patch->getPatchData(d_problemVariable_id)));
		int* seg1 = ((pdat::CellData<int> *) patch->getPatchData(d_seg1_id).get())->getPointer();
		int* segL1 = ((pdat::CellData<int> *) patch->getPatchData(d_segL1_id).get())->getPointer();
		int* seg3 = ((pdat::CellData<int> *) patch->getPatchData(d_seg3_id).get())->getPointer();
		int* segL3 = ((pdat::CellData<int> *) patch->getPatchData(d_segL3_id).get())->getPointer();
		int* segxLower = ((pdat::CellData<int> *) patch->getPatchData(d_segxLower_id).get())->getPointer();
		int* segLxLower = ((pdat::CellData<int> *) patch->getPatchData(d_segLxLower_id).get())->getPointer();
		int* segxUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segxUpper_id).get())->getPointer();
		int* segLxUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segLxUpper_id).get())->getPointer();
		int* segyLower = ((pdat::CellData<int> *) patch->getPatchData(d_segyLower_id).get())->getPointer();
		int* segLyLower = ((pdat::CellData<int> *) patch->getPatchData(d_segLyLower_id).get())->getPointer();
		int* segyUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segyUpper_id).get())->getPointer();
		int* segLyUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segLyUpper_id).get())->getPointer();
		int* segzLower = ((pdat::CellData<int> *) patch->getPatchData(d_segzLower_id).get())->getPointer();
		int* segLzLower = ((pdat::CellData<int> *) patch->getPatchData(d_segLzLower_id).get())->getPointer();
		int* segzUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segzUpper_id).get())->getPointer();
		int* segLzUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segLzUpper_id).get())->getPointer();
		int* region = ((pdat::CellData<int> *) patch->getPatchData(d_region_id).get())->getPointer();
		const hier::Index boxfirst1 = patch->getBox().lower();
		const hier::Index boxlast1  = patch->getBox().upper();
		const hier::Index boxfirst = problemVariable->getGhostBox().lower();
		const hier::Index boxlast  = problemVariable->getGhostBox().upper();
		int ilast = boxlast1(0)-boxfirst1(0) + 1 + 2 * d_ghost_width;
		int jlast = boxlast1(1)-boxfirst1(1) + 1 + 2 * d_ghost_width;
		int klast = boxlast1(2)-boxfirst1(2) + 1 + 2 * d_ghost_width;
	
		for (int index2 = boxfirst(2); index2 <= boxlast(2); index2++) {
			for (int index1 = boxfirst(1); index1 <= boxlast(1); index1++) {
				for (int index0 = boxfirst(0); index0 <= boxlast(0); index0++) {
					hier::Index idx(index0, index1, index2);
					Particles* part = problemVariable->getItem(idx);
					for (int pit = part->getNumberOfParticles() - 1; pit >= 0; pit--) {
						Particle* particle = part->getParticle(pit);
						if (vector(seg3, index0 - boxfirst(0), index1 - boxfirst(1), index2 - boxfirst(2)) == 1) {
							particle->FluxSBirho_3 = Firho_cubeI(particle->mx_p, dx, simPlat_dt);
							particle->FluxSBjrho_3 = Fjrho_cubeI(particle->my_p, dx, simPlat_dt);
							particle->FluxSBkrho_3 = Fkrho_cubeI(particle->mz_p, dx, simPlat_dt);
							particle->FluxSBimx_3 = Fimx_cubeI(particle->rho_p, particle->mx_p, particle->p_p, dx, simPlat_dt);
							particle->FluxSBjmx_3 = Fjmx_cubeI(particle->rho_p, particle->mx_p, particle->my_p, dx, simPlat_dt);
							particle->FluxSBkmx_3 = Fkmx_cubeI(particle->rho_p, particle->mx_p, particle->mz_p, dx, simPlat_dt);
							particle->FluxSBimy_3 = Fimy_cubeI(particle->rho_p, particle->mx_p, particle->my_p, dx, simPlat_dt);
							particle->FluxSBjmy_3 = Fjmy_cubeI(particle->rho_p, particle->my_p, particle->p_p, dx, simPlat_dt);
							particle->FluxSBkmy_3 = Fkmy_cubeI(particle->rho_p, particle->my_p, particle->mz_p, dx, simPlat_dt);
							particle->FluxSBimz_3 = Fimz_cubeI(particle->rho_p, particle->mx_p, particle->mz_p, dx, simPlat_dt);
							particle->FluxSBjmz_3 = Fjmz_cubeI(particle->rho_p, particle->my_p, particle->mz_p, dx, simPlat_dt);
							particle->FluxSBkmz_3 = Fkmz_cubeI(particle->rho_p, particle->mz_p, particle->p_p, dx, simPlat_dt);
							particle->FluxSBie_3 = Fie_cubeI(particle->rho_p, particle->mx_p, particle->e_p, particle->p_p, dx, simPlat_dt);
							particle->FluxSBje_3 = Fje_cubeI(particle->rho_p, particle->my_p, particle->e_p, particle->p_p, dx, simPlat_dt);
							particle->FluxSBke_3 = Fke_cubeI(particle->rho_p, particle->mz_p, particle->e_p, particle->p_p, dx, simPlat_dt);
							particle->mxSPS_3 = particle->mx_p / particle->rho_p;
							particle->mySPS_3 = particle->my_p / particle->rho_p;
							particle->mzSPS_3 = particle->mz_p / particle->rho_p;
							particle->eSPS_3 = particle->e_p / particle->rho_p;
							particle->GradSBirho_3 = 0.0;
							particle->GradSBjrho_3 = 0.0;
							particle->GradSBkrho_3 = 0.0;
							particle->GradSBimx_3 = 0.0;
							particle->GradSBjmx_3 = 0.0;
							particle->GradSBkmx_3 = 0.0;
							particle->GradSBimy_3 = 0.0;
							particle->GradSBjmy_3 = 0.0;
							particle->GradSBkmy_3 = 0.0;
							particle->GradSBimz_3 = 0.0;
							particle->GradSBjmz_3 = 0.0;
							particle->GradSBkmz_3 = 0.0;
							particle->GradSBie_3 = 0.0;
							particle->GradSBje_3 = 0.0;
							particle->GradSBke_3 = 0.0;
						}
						if (vector(seg1, index0 - boxfirst(0), index1 - boxfirst(1), index2 - boxfirst(2)) == 1) {
							particle->FluxSBirho_1 = Firho_cubeI(particle->mx_p, dx, simPlat_dt);
							particle->FluxSBjrho_1 = Fjrho_cubeI(particle->my_p, dx, simPlat_dt);
							particle->FluxSBkrho_1 = Fkrho_cubeI(particle->mz_p, dx, simPlat_dt);
							particle->FluxSBimx_1 = Fimx_cubeI(particle->rho_p, particle->mx_p, particle->p_p, dx, simPlat_dt);
							particle->FluxSBjmx_1 = Fjmx_cubeI(particle->rho_p, particle->mx_p, particle->my_p, dx, simPlat_dt);
							particle->FluxSBkmx_1 = Fkmx_cubeI(particle->rho_p, particle->mx_p, particle->mz_p, dx, simPlat_dt);
							particle->FluxSBimy_1 = Fimy_cubeI(particle->rho_p, particle->mx_p, particle->my_p, dx, simPlat_dt);
							particle->FluxSBjmy_1 = Fjmy_cubeI(particle->rho_p, particle->my_p, particle->p_p, dx, simPlat_dt);
							particle->FluxSBkmy_1 = Fkmy_cubeI(particle->rho_p, particle->my_p, particle->mz_p, dx, simPlat_dt);
							particle->FluxSBimz_1 = Fimz_cubeI(particle->rho_p, particle->mx_p, particle->mz_p, dx, simPlat_dt);
							particle->FluxSBjmz_1 = Fjmz_cubeI(particle->rho_p, particle->my_p, particle->mz_p, dx, simPlat_dt);
							particle->FluxSBkmz_1 = Fkmz_cubeI(particle->rho_p, particle->mz_p, particle->p_p, dx, simPlat_dt);
							particle->FluxSBie_1 = Fie_cubeI(particle->rho_p, particle->mx_p, particle->e_p, particle->p_p, dx, simPlat_dt);
							particle->FluxSBje_1 = Fje_cubeI(particle->rho_p, particle->my_p, particle->e_p, particle->p_p, dx, simPlat_dt);
							particle->FluxSBke_1 = Fke_cubeI(particle->rho_p, particle->mz_p, particle->e_p, particle->p_p, dx, simPlat_dt);
							particle->mxSPS_1 = particle->mx_p / particle->rho_p;
							particle->mySPS_1 = particle->my_p / particle->rho_p;
							particle->mzSPS_1 = particle->mz_p / particle->rho_p;
							particle->eSPS_1 = particle->e_p / particle->rho_p;
							particle->GradSBirho_1 = 0.0;
							particle->GradSBjrho_1 = 0.0;
							particle->GradSBkrho_1 = 0.0;
							particle->GradSBimx_1 = 0.0;
							particle->GradSBjmx_1 = 0.0;
							particle->GradSBkmx_1 = 0.0;
							particle->GradSBimy_1 = 0.0;
							particle->GradSBjmy_1 = 0.0;
							particle->GradSBkmy_1 = 0.0;
							particle->GradSBimz_1 = 0.0;
							particle->GradSBjmz_1 = 0.0;
							particle->GradSBkmz_1 = 0.0;
							particle->GradSBie_1 = 0.0;
							particle->GradSBje_1 = 0.0;
							particle->GradSBke_1 = 0.0;
						}
					}
				}
			}
		}
	}
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch > patch = *p_it;
		std::shared_ptr< pdat::IndexData<Particles, pdat::CellGeometry > > problemVariable(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles, pdat::CellGeometry >, hier::PatchData>(patch->getPatchData(d_problemVariable_id)));
		int* seg1 = ((pdat::CellData<int> *) patch->getPatchData(d_seg1_id).get())->getPointer();
		int* segL1 = ((pdat::CellData<int> *) patch->getPatchData(d_segL1_id).get())->getPointer();
		int* seg3 = ((pdat::CellData<int> *) patch->getPatchData(d_seg3_id).get())->getPointer();
		int* segL3 = ((pdat::CellData<int> *) patch->getPatchData(d_segL3_id).get())->getPointer();
		int* segxLower = ((pdat::CellData<int> *) patch->getPatchData(d_segxLower_id).get())->getPointer();
		int* segLxLower = ((pdat::CellData<int> *) patch->getPatchData(d_segLxLower_id).get())->getPointer();
		int* segxUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segxUpper_id).get())->getPointer();
		int* segLxUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segLxUpper_id).get())->getPointer();
		int* segyLower = ((pdat::CellData<int> *) patch->getPatchData(d_segyLower_id).get())->getPointer();
		int* segLyLower = ((pdat::CellData<int> *) patch->getPatchData(d_segLyLower_id).get())->getPointer();
		int* segyUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segyUpper_id).get())->getPointer();
		int* segLyUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segLyUpper_id).get())->getPointer();
		int* segzLower = ((pdat::CellData<int> *) patch->getPatchData(d_segzLower_id).get())->getPointer();
		int* segLzLower = ((pdat::CellData<int> *) patch->getPatchData(d_segLzLower_id).get())->getPointer();
		int* segzUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segzUpper_id).get())->getPointer();
		int* segLzUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segLzUpper_id).get())->getPointer();
		int* region = ((pdat::CellData<int> *) patch->getPatchData(d_region_id).get())->getPointer();
		const hier::Index boxfirst1 = patch->getBox().lower();
		const hier::Index boxlast1  = patch->getBox().upper();
		const hier::Index boxfirst = problemVariable->getGhostBox().lower();
		const hier::Index boxlast  = problemVariable->getGhostBox().upper();
		int ilast = boxlast1(0)-boxfirst1(0) + 1 + 2 * d_ghost_width;
		int jlast = boxlast1(1)-boxfirst1(1) + 1 + 2 * d_ghost_width;
		int klast = boxlast1(2)-boxfirst1(2) + 1 + 2 * d_ghost_width;
	
		double dist_3;
		double dist_1;
		for (int index2 = boxfirst(2); index2 <= boxlast(2); index2++) {
			for (int index1 = boxfirst(1); index1 <= boxlast(1); index1++) {
				for (int index0 = boxfirst(0); index0 <= boxlast(0); index0++) {
					hier::Index idx(index0, index1, index2);
					Particles* part = problemVariable->getItem(idx);
					for (int pit = part->getNumberOfParticles() - 1; pit >= 0; pit--) {
						Particle* particle = part->getParticle(pit);
						if (vector(seg3, index0 - boxfirst(0), index1 - boxfirst(1), index2 - boxfirst(2)) == 1) {
							miniIndex = MAX(boxfirst(0), index0 - ceil((2.0 * influenceRadius_0)/dx[0]));
							maxiIndex = MIN(boxlast(0), index0 + ceil((2.0 * influenceRadius_0)/dx[0]));
							minjIndex = MAX(boxfirst(1), index1 - ceil((2.0 * influenceRadius_0)/dx[1]));
							maxjIndex = MIN(boxlast(1), index1 + ceil((2.0 * influenceRadius_0)/dx[1]));
							maxkIndex = MIN(boxlast(2), index2 + ceil((2.0 * influenceRadius_0)/dx[2]));
							for(int index2b = index2; index2b <= maxkIndex; index2b++) {
								if (index2b == index2)	{index1min = index1;}
								else					  {index1min = minjIndex;}
								for(int index1b = index1min; index1b <= maxjIndex; index1b++) {
									if ((index2b == index2) && (index1b == index1))	{index0min = index0;}
									else								  {index0min = miniIndex;}
									for(int index0b = index0min; index0b <= maxiIndex; index0b++) {
										if(vector(seg3, index0b - boxfirst(0), index1b - boxfirst(1), index2b - boxfirst(2)) == 1) {				
											hier::Index idxb(index0b, index1b, index2b);
											Particles* partb = problemVariable->getItem(idxb);
											if ( (index2b==index2) && (index1b==index1) && (index0b==index0)) {pitbmin = pit+1;}
											else								  {pitbmin = 0;}
											for (int pitb = pitbmin; pitb < partb->getNumberOfParticles(); pitb++) {
												Particle* particleb = partb->getParticle(pitb);
												//A-B and B-A interactions
												dist_3 = particleb->distance_p(particle);
												if (lessThan(dist_3, (2.0 * influenceRadius_0)) && greaterThan(dist_3, 0.0)) {
													particle->GradSBimx_3 = particle->GradSBimx_3 + particleb->mass * ((particleb->FluxSBimx_3 - particleb->mxSPS_3 * particleb->FluxSBirho_3) / (particleb->rho_p * particleb->rho_p) + (particle->FluxSBimx_3 - particle->mxSPS_3 * particle->FluxSBirho_3) / (particle->rho_p * particle->rho_p)) * gW(dist_3, dx, simPlat_dt) * (particle->positioni_p - particleb->positioni_p);
													particle->GradSBjmx_3 = particle->GradSBjmx_3 + particleb->mass * ((particleb->FluxSBjmx_3 - particleb->mxSPS_3 * particleb->FluxSBjrho_3) / (particleb->rho_p * particleb->rho_p) + (particle->FluxSBjmx_3 - particle->mxSPS_3 * particle->FluxSBjrho_3) / (particle->rho_p * particle->rho_p)) * gW(dist_3, dx, simPlat_dt) * (particle->positionj_p - particleb->positionj_p);
													particle->GradSBkmx_3 = particle->GradSBkmx_3 + particleb->mass * ((particleb->FluxSBkmx_3 - particleb->mxSPS_3 * particleb->FluxSBkrho_3) / (particleb->rho_p * particleb->rho_p) + (particle->FluxSBkmx_3 - particle->mxSPS_3 * particle->FluxSBkrho_3) / (particle->rho_p * particle->rho_p)) * gW(dist_3, dx, simPlat_dt) * (particle->positionk_p - particleb->positionk_p);
													particle->GradSBimy_3 = particle->GradSBimy_3 + particleb->mass * ((particleb->FluxSBimy_3 - particleb->mySPS_3 * particleb->FluxSBirho_3) / (particleb->rho_p * particleb->rho_p) + (particle->FluxSBimy_3 - particle->mySPS_3 * particle->FluxSBirho_3) / (particle->rho_p * particle->rho_p)) * gW(dist_3, dx, simPlat_dt) * (particle->positioni_p - particleb->positioni_p);
													particle->GradSBjmy_3 = particle->GradSBjmy_3 + particleb->mass * ((particleb->FluxSBjmy_3 - particleb->mySPS_3 * particleb->FluxSBjrho_3) / (particleb->rho_p * particleb->rho_p) + (particle->FluxSBjmy_3 - particle->mySPS_3 * particle->FluxSBjrho_3) / (particle->rho_p * particle->rho_p)) * gW(dist_3, dx, simPlat_dt) * (particle->positionj_p - particleb->positionj_p);
													particle->GradSBkmy_3 = particle->GradSBkmy_3 + particleb->mass * ((particleb->FluxSBkmy_3 - particleb->mySPS_3 * particleb->FluxSBkrho_3) / (particleb->rho_p * particleb->rho_p) + (particle->FluxSBkmy_3 - particle->mySPS_3 * particle->FluxSBkrho_3) / (particle->rho_p * particle->rho_p)) * gW(dist_3, dx, simPlat_dt) * (particle->positionk_p - particleb->positionk_p);
													particle->GradSBimz_3 = particle->GradSBimz_3 + particleb->mass * ((particleb->FluxSBimz_3 - particleb->mzSPS_3 * particleb->FluxSBirho_3) / (particleb->rho_p * particleb->rho_p) + (particle->FluxSBimz_3 - particle->mzSPS_3 * particle->FluxSBirho_3) / (particle->rho_p * particle->rho_p)) * gW(dist_3, dx, simPlat_dt) * (particle->positioni_p - particleb->positioni_p);
													particle->GradSBjmz_3 = particle->GradSBjmz_3 + particleb->mass * ((particleb->FluxSBjmz_3 - particleb->mzSPS_3 * particleb->FluxSBjrho_3) / (particleb->rho_p * particleb->rho_p) + (particle->FluxSBjmz_3 - particle->mzSPS_3 * particle->FluxSBjrho_3) / (particle->rho_p * particle->rho_p)) * gW(dist_3, dx, simPlat_dt) * (particle->positionj_p - particleb->positionj_p);
													particle->GradSBkmz_3 = particle->GradSBkmz_3 + particleb->mass * ((particleb->FluxSBkmz_3 - particleb->mzSPS_3 * particleb->FluxSBkrho_3) / (particleb->rho_p * particleb->rho_p) + (particle->FluxSBkmz_3 - particle->mzSPS_3 * particle->FluxSBkrho_3) / (particle->rho_p * particle->rho_p)) * gW(dist_3, dx, simPlat_dt) * (particle->positionk_p - particleb->positionk_p);
													particle->GradSBie_3 = particle->GradSBie_3 + particleb->mass * ((particleb->FluxSBie_3 - particleb->eSPS_3 * particleb->FluxSBirho_3) / (particleb->rho_p * particleb->rho_p) + (particle->FluxSBie_3 - particle->eSPS_3 * particle->FluxSBirho_3) / (particle->rho_p * particle->rho_p)) * gW(dist_3, dx, simPlat_dt) * (particle->positioni_p - particleb->positioni_p);
													particle->GradSBje_3 = particle->GradSBje_3 + particleb->mass * ((particleb->FluxSBje_3 - particleb->eSPS_3 * particleb->FluxSBjrho_3) / (particleb->rho_p * particleb->rho_p) + (particle->FluxSBje_3 - particle->eSPS_3 * particle->FluxSBjrho_3) / (particle->rho_p * particle->rho_p)) * gW(dist_3, dx, simPlat_dt) * (particle->positionj_p - particleb->positionj_p);
													particle->GradSBke_3 = particle->GradSBke_3 + particleb->mass * ((particleb->FluxSBke_3 - particleb->eSPS_3 * particleb->FluxSBkrho_3) / (particleb->rho_p * particleb->rho_p) + (particle->FluxSBke_3 - particle->eSPS_3 * particle->FluxSBkrho_3) / (particle->rho_p * particle->rho_p)) * gW(dist_3, dx, simPlat_dt) * (particle->positionk_p - particleb->positionk_p);
													particle->GradSBirho_3 = particle->GradSBirho_3 + particle->rho_p * particleb->mass / particleb->rho_p * (particleb->FluxSBirho_3 / particleb->rho_p - particle->FluxSBirho_3 / particle->rho_p) * gW(dist_3, dx, simPlat_dt) * (particle->positioni_p - particleb->positioni_p);
													particle->GradSBjrho_3 = particle->GradSBjrho_3 + particle->rho_p * particleb->mass / particleb->rho_p * (particleb->FluxSBjrho_3 / particleb->rho_p - particle->FluxSBjrho_3 / particle->rho_p) * gW(dist_3, dx, simPlat_dt) * (particle->positionj_p - particleb->positionj_p);
													particle->GradSBkrho_3 = particle->GradSBkrho_3 + particle->rho_p * particleb->mass / particleb->rho_p * (particleb->FluxSBkrho_3 / particleb->rho_p - particle->FluxSBkrho_3 / particle->rho_p) * gW(dist_3, dx, simPlat_dt) * (particle->positionk_p - particleb->positionk_p);
												}
												dist_3 = particleb->distance_p(particle);
												if (lessThan(dist_3, (2.0 * influenceRadius_0)) && greaterThan(dist_3, 0.0)) {
													particleb->GradSBimx_3 = particleb->GradSBimx_3 + particle->mass * ((particle->FluxSBimx_3 - particle->mxSPS_3 * particle->FluxSBirho_3) / (particle->rho_p * particle->rho_p) + (particleb->FluxSBimx_3 - particleb->mxSPS_3 * particleb->FluxSBirho_3) / (particleb->rho_p * particleb->rho_p)) * gW(dist_3, dx, simPlat_dt) * (particleb->positioni_p - particle->positioni_p);
													particleb->GradSBjmx_3 = particleb->GradSBjmx_3 + particle->mass * ((particle->FluxSBjmx_3 - particle->mxSPS_3 * particle->FluxSBjrho_3) / (particle->rho_p * particle->rho_p) + (particleb->FluxSBjmx_3 - particleb->mxSPS_3 * particleb->FluxSBjrho_3) / (particleb->rho_p * particleb->rho_p)) * gW(dist_3, dx, simPlat_dt) * (particleb->positionj_p - particle->positionj_p);
													particleb->GradSBkmx_3 = particleb->GradSBkmx_3 + particle->mass * ((particle->FluxSBkmx_3 - particle->mxSPS_3 * particle->FluxSBkrho_3) / (particle->rho_p * particle->rho_p) + (particleb->FluxSBkmx_3 - particleb->mxSPS_3 * particleb->FluxSBkrho_3) / (particleb->rho_p * particleb->rho_p)) * gW(dist_3, dx, simPlat_dt) * (particleb->positionk_p - particle->positionk_p);
													particleb->GradSBimy_3 = particleb->GradSBimy_3 + particle->mass * ((particle->FluxSBimy_3 - particle->mySPS_3 * particle->FluxSBirho_3) / (particle->rho_p * particle->rho_p) + (particleb->FluxSBimy_3 - particleb->mySPS_3 * particleb->FluxSBirho_3) / (particleb->rho_p * particleb->rho_p)) * gW(dist_3, dx, simPlat_dt) * (particleb->positioni_p - particle->positioni_p);
													particleb->GradSBjmy_3 = particleb->GradSBjmy_3 + particle->mass * ((particle->FluxSBjmy_3 - particle->mySPS_3 * particle->FluxSBjrho_3) / (particle->rho_p * particle->rho_p) + (particleb->FluxSBjmy_3 - particleb->mySPS_3 * particleb->FluxSBjrho_3) / (particleb->rho_p * particleb->rho_p)) * gW(dist_3, dx, simPlat_dt) * (particleb->positionj_p - particle->positionj_p);
													particleb->GradSBkmy_3 = particleb->GradSBkmy_3 + particle->mass * ((particle->FluxSBkmy_3 - particle->mySPS_3 * particle->FluxSBkrho_3) / (particle->rho_p * particle->rho_p) + (particleb->FluxSBkmy_3 - particleb->mySPS_3 * particleb->FluxSBkrho_3) / (particleb->rho_p * particleb->rho_p)) * gW(dist_3, dx, simPlat_dt) * (particleb->positionk_p - particle->positionk_p);
													particleb->GradSBimz_3 = particleb->GradSBimz_3 + particle->mass * ((particle->FluxSBimz_3 - particle->mzSPS_3 * particle->FluxSBirho_3) / (particle->rho_p * particle->rho_p) + (particleb->FluxSBimz_3 - particleb->mzSPS_3 * particleb->FluxSBirho_3) / (particleb->rho_p * particleb->rho_p)) * gW(dist_3, dx, simPlat_dt) * (particleb->positioni_p - particle->positioni_p);
													particleb->GradSBjmz_3 = particleb->GradSBjmz_3 + particle->mass * ((particle->FluxSBjmz_3 - particle->mzSPS_3 * particle->FluxSBjrho_3) / (particle->rho_p * particle->rho_p) + (particleb->FluxSBjmz_3 - particleb->mzSPS_3 * particleb->FluxSBjrho_3) / (particleb->rho_p * particleb->rho_p)) * gW(dist_3, dx, simPlat_dt) * (particleb->positionj_p - particle->positionj_p);
													particleb->GradSBkmz_3 = particleb->GradSBkmz_3 + particle->mass * ((particle->FluxSBkmz_3 - particle->mzSPS_3 * particle->FluxSBkrho_3) / (particle->rho_p * particle->rho_p) + (particleb->FluxSBkmz_3 - particleb->mzSPS_3 * particleb->FluxSBkrho_3) / (particleb->rho_p * particleb->rho_p)) * gW(dist_3, dx, simPlat_dt) * (particleb->positionk_p - particle->positionk_p);
													particleb->GradSBie_3 = particleb->GradSBie_3 + particle->mass * ((particle->FluxSBie_3 - particle->eSPS_3 * particle->FluxSBirho_3) / (particle->rho_p * particle->rho_p) + (particleb->FluxSBie_3 - particleb->eSPS_3 * particleb->FluxSBirho_3) / (particleb->rho_p * particleb->rho_p)) * gW(dist_3, dx, simPlat_dt) * (particleb->positioni_p - particle->positioni_p);
													particleb->GradSBje_3 = particleb->GradSBje_3 + particle->mass * ((particle->FluxSBje_3 - particle->eSPS_3 * particle->FluxSBjrho_3) / (particle->rho_p * particle->rho_p) + (particleb->FluxSBje_3 - particleb->eSPS_3 * particleb->FluxSBjrho_3) / (particleb->rho_p * particleb->rho_p)) * gW(dist_3, dx, simPlat_dt) * (particleb->positionj_p - particle->positionj_p);
													particleb->GradSBke_3 = particleb->GradSBke_3 + particle->mass * ((particle->FluxSBke_3 - particle->eSPS_3 * particle->FluxSBkrho_3) / (particle->rho_p * particle->rho_p) + (particleb->FluxSBke_3 - particleb->eSPS_3 * particleb->FluxSBkrho_3) / (particleb->rho_p * particleb->rho_p)) * gW(dist_3, dx, simPlat_dt) * (particleb->positionk_p - particle->positionk_p);
													particleb->GradSBirho_3 = particleb->GradSBirho_3 + particleb->rho_p * particle->mass / particle->rho_p * (particle->FluxSBirho_3 / particle->rho_p - particleb->FluxSBirho_3 / particleb->rho_p) * gW(dist_3, dx, simPlat_dt) * (particleb->positioni_p - particle->positioni_p);
													particleb->GradSBjrho_3 = particleb->GradSBjrho_3 + particleb->rho_p * particle->mass / particle->rho_p * (particle->FluxSBjrho_3 / particle->rho_p - particleb->FluxSBjrho_3 / particleb->rho_p) * gW(dist_3, dx, simPlat_dt) * (particleb->positionj_p - particle->positionj_p);
													particleb->GradSBkrho_3 = particleb->GradSBkrho_3 + particleb->rho_p * particle->mass / particle->rho_p * (particle->FluxSBkrho_3 / particle->rho_p - particleb->FluxSBkrho_3 / particleb->rho_p) * gW(dist_3, dx, simPlat_dt) * (particleb->positionk_p - particle->positionk_p);
												}
											}
										}				
									}
								}
							}
						}
						if (vector(seg1, index0 - boxfirst(0), index1 - boxfirst(1), index2 - boxfirst(2)) == 1) {
							miniIndex = MAX(boxfirst(0), index0 - ceil((2.0 * influenceRadius_0)/dx[0]));
							maxiIndex = MIN(boxlast(0), index0 + ceil((2.0 * influenceRadius_0)/dx[0]));
							minjIndex = MAX(boxfirst(1), index1 - ceil((2.0 * influenceRadius_0)/dx[1]));
							maxjIndex = MIN(boxlast(1), index1 + ceil((2.0 * influenceRadius_0)/dx[1]));
							maxkIndex = MIN(boxlast(2), index2 + ceil((2.0 * influenceRadius_0)/dx[2]));
							for(int index2b = index2; index2b <= maxkIndex; index2b++) {
								if (index2b == index2)	{index1min = index1;}
								else					  {index1min = minjIndex;}
								for(int index1b = index1min; index1b <= maxjIndex; index1b++) {
									if ((index2b == index2) && (index1b == index1))	{index0min = index0;}
									else								  {index0min = miniIndex;}
									for(int index0b = index0min; index0b <= maxiIndex; index0b++) {
										if(vector(seg1, index0b - boxfirst(0), index1b - boxfirst(1), index2b - boxfirst(2)) == 1) {				
											hier::Index idxb(index0b, index1b, index2b);
											Particles* partb = problemVariable->getItem(idxb);
											if ( (index2b==index2) && (index1b==index1) && (index0b==index0)) {pitbmin = pit+1;}
											else								  {pitbmin = 0;}
											for (int pitb = pitbmin; pitb < partb->getNumberOfParticles(); pitb++) {
												Particle* particleb = partb->getParticle(pitb);
												//A-B and B-A interactions
												dist_1 = particleb->distance_p(particle);
												if (lessThan(dist_1, (2.0 * influenceRadius_0)) && greaterThan(dist_1, 0.0)) {
													particle->GradSBimx_1 = particle->GradSBimx_1 + particleb->mass * ((particleb->FluxSBimx_1 - particleb->mxSPS_1 * particleb->FluxSBirho_1) / (particleb->rho_p * particleb->rho_p) + (particle->FluxSBimx_1 - particle->mxSPS_1 * particle->FluxSBirho_1) / (particle->rho_p * particle->rho_p)) * gW(dist_1, dx, simPlat_dt) * (particle->positioni_p - particleb->positioni_p);
													particle->GradSBjmx_1 = particle->GradSBjmx_1 + particleb->mass * ((particleb->FluxSBjmx_1 - particleb->mxSPS_1 * particleb->FluxSBjrho_1) / (particleb->rho_p * particleb->rho_p) + (particle->FluxSBjmx_1 - particle->mxSPS_1 * particle->FluxSBjrho_1) / (particle->rho_p * particle->rho_p)) * gW(dist_1, dx, simPlat_dt) * (particle->positionj_p - particleb->positionj_p);
													particle->GradSBkmx_1 = particle->GradSBkmx_1 + particleb->mass * ((particleb->FluxSBkmx_1 - particleb->mxSPS_1 * particleb->FluxSBkrho_1) / (particleb->rho_p * particleb->rho_p) + (particle->FluxSBkmx_1 - particle->mxSPS_1 * particle->FluxSBkrho_1) / (particle->rho_p * particle->rho_p)) * gW(dist_1, dx, simPlat_dt) * (particle->positionk_p - particleb->positionk_p);
													particle->GradSBimy_1 = particle->GradSBimy_1 + particleb->mass * ((particleb->FluxSBimy_1 - particleb->mySPS_1 * particleb->FluxSBirho_1) / (particleb->rho_p * particleb->rho_p) + (particle->FluxSBimy_1 - particle->mySPS_1 * particle->FluxSBirho_1) / (particle->rho_p * particle->rho_p)) * gW(dist_1, dx, simPlat_dt) * (particle->positioni_p - particleb->positioni_p);
													particle->GradSBjmy_1 = particle->GradSBjmy_1 + particleb->mass * ((particleb->FluxSBjmy_1 - particleb->mySPS_1 * particleb->FluxSBjrho_1) / (particleb->rho_p * particleb->rho_p) + (particle->FluxSBjmy_1 - particle->mySPS_1 * particle->FluxSBjrho_1) / (particle->rho_p * particle->rho_p)) * gW(dist_1, dx, simPlat_dt) * (particle->positionj_p - particleb->positionj_p);
													particle->GradSBkmy_1 = particle->GradSBkmy_1 + particleb->mass * ((particleb->FluxSBkmy_1 - particleb->mySPS_1 * particleb->FluxSBkrho_1) / (particleb->rho_p * particleb->rho_p) + (particle->FluxSBkmy_1 - particle->mySPS_1 * particle->FluxSBkrho_1) / (particle->rho_p * particle->rho_p)) * gW(dist_1, dx, simPlat_dt) * (particle->positionk_p - particleb->positionk_p);
													particle->GradSBimz_1 = particle->GradSBimz_1 + particleb->mass * ((particleb->FluxSBimz_1 - particleb->mzSPS_1 * particleb->FluxSBirho_1) / (particleb->rho_p * particleb->rho_p) + (particle->FluxSBimz_1 - particle->mzSPS_1 * particle->FluxSBirho_1) / (particle->rho_p * particle->rho_p)) * gW(dist_1, dx, simPlat_dt) * (particle->positioni_p - particleb->positioni_p);
													particle->GradSBjmz_1 = particle->GradSBjmz_1 + particleb->mass * ((particleb->FluxSBjmz_1 - particleb->mzSPS_1 * particleb->FluxSBjrho_1) / (particleb->rho_p * particleb->rho_p) + (particle->FluxSBjmz_1 - particle->mzSPS_1 * particle->FluxSBjrho_1) / (particle->rho_p * particle->rho_p)) * gW(dist_1, dx, simPlat_dt) * (particle->positionj_p - particleb->positionj_p);
													particle->GradSBkmz_1 = particle->GradSBkmz_1 + particleb->mass * ((particleb->FluxSBkmz_1 - particleb->mzSPS_1 * particleb->FluxSBkrho_1) / (particleb->rho_p * particleb->rho_p) + (particle->FluxSBkmz_1 - particle->mzSPS_1 * particle->FluxSBkrho_1) / (particle->rho_p * particle->rho_p)) * gW(dist_1, dx, simPlat_dt) * (particle->positionk_p - particleb->positionk_p);
													particle->GradSBie_1 = particle->GradSBie_1 + particleb->mass * ((particleb->FluxSBie_1 - particleb->eSPS_1 * particleb->FluxSBirho_1) / (particleb->rho_p * particleb->rho_p) + (particle->FluxSBie_1 - particle->eSPS_1 * particle->FluxSBirho_1) / (particle->rho_p * particle->rho_p)) * gW(dist_1, dx, simPlat_dt) * (particle->positioni_p - particleb->positioni_p);
													particle->GradSBje_1 = particle->GradSBje_1 + particleb->mass * ((particleb->FluxSBje_1 - particleb->eSPS_1 * particleb->FluxSBjrho_1) / (particleb->rho_p * particleb->rho_p) + (particle->FluxSBje_1 - particle->eSPS_1 * particle->FluxSBjrho_1) / (particle->rho_p * particle->rho_p)) * gW(dist_1, dx, simPlat_dt) * (particle->positionj_p - particleb->positionj_p);
													particle->GradSBke_1 = particle->GradSBke_1 + particleb->mass * ((particleb->FluxSBke_1 - particleb->eSPS_1 * particleb->FluxSBkrho_1) / (particleb->rho_p * particleb->rho_p) + (particle->FluxSBke_1 - particle->eSPS_1 * particle->FluxSBkrho_1) / (particle->rho_p * particle->rho_p)) * gW(dist_1, dx, simPlat_dt) * (particle->positionk_p - particleb->positionk_p);
													particle->GradSBirho_1 = particle->GradSBirho_1 + particle->rho_p * particleb->mass / particleb->rho_p * (particleb->FluxSBirho_1 / particleb->rho_p - particle->FluxSBirho_1 / particle->rho_p) * gW(dist_1, dx, simPlat_dt) * (particle->positioni_p - particleb->positioni_p);
													particle->GradSBjrho_1 = particle->GradSBjrho_1 + particle->rho_p * particleb->mass / particleb->rho_p * (particleb->FluxSBjrho_1 / particleb->rho_p - particle->FluxSBjrho_1 / particle->rho_p) * gW(dist_1, dx, simPlat_dt) * (particle->positionj_p - particleb->positionj_p);
													particle->GradSBkrho_1 = particle->GradSBkrho_1 + particle->rho_p * particleb->mass / particleb->rho_p * (particleb->FluxSBkrho_1 / particleb->rho_p - particle->FluxSBkrho_1 / particle->rho_p) * gW(dist_1, dx, simPlat_dt) * (particle->positionk_p - particleb->positionk_p);
												}
												dist_1 = particleb->distance_p(particle);
												if (lessThan(dist_1, (2.0 * influenceRadius_0)) && greaterThan(dist_1, 0.0)) {
													particleb->GradSBimx_1 = particleb->GradSBimx_1 + particle->mass * ((particle->FluxSBimx_1 - particle->mxSPS_1 * particle->FluxSBirho_1) / (particle->rho_p * particle->rho_p) + (particleb->FluxSBimx_1 - particleb->mxSPS_1 * particleb->FluxSBirho_1) / (particleb->rho_p * particleb->rho_p)) * gW(dist_1, dx, simPlat_dt) * (particleb->positioni_p - particle->positioni_p);
													particleb->GradSBjmx_1 = particleb->GradSBjmx_1 + particle->mass * ((particle->FluxSBjmx_1 - particle->mxSPS_1 * particle->FluxSBjrho_1) / (particle->rho_p * particle->rho_p) + (particleb->FluxSBjmx_1 - particleb->mxSPS_1 * particleb->FluxSBjrho_1) / (particleb->rho_p * particleb->rho_p)) * gW(dist_1, dx, simPlat_dt) * (particleb->positionj_p - particle->positionj_p);
													particleb->GradSBkmx_1 = particleb->GradSBkmx_1 + particle->mass * ((particle->FluxSBkmx_1 - particle->mxSPS_1 * particle->FluxSBkrho_1) / (particle->rho_p * particle->rho_p) + (particleb->FluxSBkmx_1 - particleb->mxSPS_1 * particleb->FluxSBkrho_1) / (particleb->rho_p * particleb->rho_p)) * gW(dist_1, dx, simPlat_dt) * (particleb->positionk_p - particle->positionk_p);
													particleb->GradSBimy_1 = particleb->GradSBimy_1 + particle->mass * ((particle->FluxSBimy_1 - particle->mySPS_1 * particle->FluxSBirho_1) / (particle->rho_p * particle->rho_p) + (particleb->FluxSBimy_1 - particleb->mySPS_1 * particleb->FluxSBirho_1) / (particleb->rho_p * particleb->rho_p)) * gW(dist_1, dx, simPlat_dt) * (particleb->positioni_p - particle->positioni_p);
													particleb->GradSBjmy_1 = particleb->GradSBjmy_1 + particle->mass * ((particle->FluxSBjmy_1 - particle->mySPS_1 * particle->FluxSBjrho_1) / (particle->rho_p * particle->rho_p) + (particleb->FluxSBjmy_1 - particleb->mySPS_1 * particleb->FluxSBjrho_1) / (particleb->rho_p * particleb->rho_p)) * gW(dist_1, dx, simPlat_dt) * (particleb->positionj_p - particle->positionj_p);
													particleb->GradSBkmy_1 = particleb->GradSBkmy_1 + particle->mass * ((particle->FluxSBkmy_1 - particle->mySPS_1 * particle->FluxSBkrho_1) / (particle->rho_p * particle->rho_p) + (particleb->FluxSBkmy_1 - particleb->mySPS_1 * particleb->FluxSBkrho_1) / (particleb->rho_p * particleb->rho_p)) * gW(dist_1, dx, simPlat_dt) * (particleb->positionk_p - particle->positionk_p);
													particleb->GradSBimz_1 = particleb->GradSBimz_1 + particle->mass * ((particle->FluxSBimz_1 - particle->mzSPS_1 * particle->FluxSBirho_1) / (particle->rho_p * particle->rho_p) + (particleb->FluxSBimz_1 - particleb->mzSPS_1 * particleb->FluxSBirho_1) / (particleb->rho_p * particleb->rho_p)) * gW(dist_1, dx, simPlat_dt) * (particleb->positioni_p - particle->positioni_p);
													particleb->GradSBjmz_1 = particleb->GradSBjmz_1 + particle->mass * ((particle->FluxSBjmz_1 - particle->mzSPS_1 * particle->FluxSBjrho_1) / (particle->rho_p * particle->rho_p) + (particleb->FluxSBjmz_1 - particleb->mzSPS_1 * particleb->FluxSBjrho_1) / (particleb->rho_p * particleb->rho_p)) * gW(dist_1, dx, simPlat_dt) * (particleb->positionj_p - particle->positionj_p);
													particleb->GradSBkmz_1 = particleb->GradSBkmz_1 + particle->mass * ((particle->FluxSBkmz_1 - particle->mzSPS_1 * particle->FluxSBkrho_1) / (particle->rho_p * particle->rho_p) + (particleb->FluxSBkmz_1 - particleb->mzSPS_1 * particleb->FluxSBkrho_1) / (particleb->rho_p * particleb->rho_p)) * gW(dist_1, dx, simPlat_dt) * (particleb->positionk_p - particle->positionk_p);
													particleb->GradSBie_1 = particleb->GradSBie_1 + particle->mass * ((particle->FluxSBie_1 - particle->eSPS_1 * particle->FluxSBirho_1) / (particle->rho_p * particle->rho_p) + (particleb->FluxSBie_1 - particleb->eSPS_1 * particleb->FluxSBirho_1) / (particleb->rho_p * particleb->rho_p)) * gW(dist_1, dx, simPlat_dt) * (particleb->positioni_p - particle->positioni_p);
													particleb->GradSBje_1 = particleb->GradSBje_1 + particle->mass * ((particle->FluxSBje_1 - particle->eSPS_1 * particle->FluxSBjrho_1) / (particle->rho_p * particle->rho_p) + (particleb->FluxSBje_1 - particleb->eSPS_1 * particleb->FluxSBjrho_1) / (particleb->rho_p * particleb->rho_p)) * gW(dist_1, dx, simPlat_dt) * (particleb->positionj_p - particle->positionj_p);
													particleb->GradSBke_1 = particleb->GradSBke_1 + particle->mass * ((particle->FluxSBke_1 - particle->eSPS_1 * particle->FluxSBkrho_1) / (particle->rho_p * particle->rho_p) + (particleb->FluxSBke_1 - particleb->eSPS_1 * particleb->FluxSBkrho_1) / (particleb->rho_p * particleb->rho_p)) * gW(dist_1, dx, simPlat_dt) * (particleb->positionk_p - particle->positionk_p);
													particleb->GradSBirho_1 = particleb->GradSBirho_1 + particleb->rho_p * particle->mass / particle->rho_p * (particle->FluxSBirho_1 / particle->rho_p - particleb->FluxSBirho_1 / particleb->rho_p) * gW(dist_1, dx, simPlat_dt) * (particleb->positioni_p - particle->positioni_p);
													particleb->GradSBjrho_1 = particleb->GradSBjrho_1 + particleb->rho_p * particle->mass / particle->rho_p * (particle->FluxSBjrho_1 / particle->rho_p - particleb->FluxSBjrho_1 / particleb->rho_p) * gW(dist_1, dx, simPlat_dt) * (particleb->positionj_p - particle->positionj_p);
													particleb->GradSBkrho_1 = particleb->GradSBkrho_1 + particleb->rho_p * particle->mass / particle->rho_p * (particle->FluxSBkrho_1 / particle->rho_p - particleb->FluxSBkrho_1 / particleb->rho_p) * gW(dist_1, dx, simPlat_dt) * (particleb->positionk_p - particle->positionk_p);
												}
											}
										}				
									}
								}
							}
						}
					}
				}
			}
		}
	}
	//Fill ghosts and periodical boundaries, but no other boundaries
	d_bdry_sched_advance[0]->fillData(current_time, false);
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch > patch = *p_it;
		std::shared_ptr< pdat::IndexData<Particles, pdat::CellGeometry > > problemVariable(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles, pdat::CellGeometry >, hier::PatchData>(patch->getPatchData(d_problemVariable_id)));
		int* seg1 = ((pdat::CellData<int> *) patch->getPatchData(d_seg1_id).get())->getPointer();
		int* segL1 = ((pdat::CellData<int> *) patch->getPatchData(d_segL1_id).get())->getPointer();
		int* seg3 = ((pdat::CellData<int> *) patch->getPatchData(d_seg3_id).get())->getPointer();
		int* segL3 = ((pdat::CellData<int> *) patch->getPatchData(d_segL3_id).get())->getPointer();
		int* segxLower = ((pdat::CellData<int> *) patch->getPatchData(d_segxLower_id).get())->getPointer();
		int* segLxLower = ((pdat::CellData<int> *) patch->getPatchData(d_segLxLower_id).get())->getPointer();
		int* segxUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segxUpper_id).get())->getPointer();
		int* segLxUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segLxUpper_id).get())->getPointer();
		int* segyLower = ((pdat::CellData<int> *) patch->getPatchData(d_segyLower_id).get())->getPointer();
		int* segLyLower = ((pdat::CellData<int> *) patch->getPatchData(d_segLyLower_id).get())->getPointer();
		int* segyUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segyUpper_id).get())->getPointer();
		int* segLyUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segLyUpper_id).get())->getPointer();
		int* segzLower = ((pdat::CellData<int> *) patch->getPatchData(d_segzLower_id).get())->getPointer();
		int* segLzLower = ((pdat::CellData<int> *) patch->getPatchData(d_segLzLower_id).get())->getPointer();
		int* segzUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segzUpper_id).get())->getPointer();
		int* segLzUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segLzUpper_id).get())->getPointer();
		int* region = ((pdat::CellData<int> *) patch->getPatchData(d_region_id).get())->getPointer();
		const hier::Index boxfirst1 = patch->getBox().lower();
		const hier::Index boxlast1  = patch->getBox().upper();
		const hier::Index boxfirst = problemVariable->getGhostBox().lower();
		const hier::Index boxlast  = problemVariable->getGhostBox().upper();
		int ilast = boxlast1(0)-boxfirst1(0) + 1 + 2 * d_ghost_width;
		int jlast = boxlast1(1)-boxfirst1(1) + 1 + 2 * d_ghost_width;
		int klast = boxlast1(2)-boxfirst1(2) + 1 + 2 * d_ghost_width;
	
		for (int index2 = boxfirst(2); index2 <= boxlast(2); index2++) {
			for (int index1 = boxfirst(1); index1 <= boxlast(1); index1++) {
				for (int index0 = boxfirst(0); index0 <= boxlast(0); index0++) {
					hier::Index idx(index0, index1, index2);
					//Correct the position if there is any periodical boundary
					Particles* part = problemVariable->getItem(idx);
					checkPosition(patch, index0, index1, index2, part);
				}
			}
		}
	}
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch > patch = *p_it;
		std::shared_ptr< pdat::IndexData<Particles, pdat::CellGeometry > > problemVariable(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles, pdat::CellGeometry >, hier::PatchData>(patch->getPatchData(d_problemVariable_id)));
		int* seg1 = ((pdat::CellData<int> *) patch->getPatchData(d_seg1_id).get())->getPointer();
		int* segL1 = ((pdat::CellData<int> *) patch->getPatchData(d_segL1_id).get())->getPointer();
		int* seg3 = ((pdat::CellData<int> *) patch->getPatchData(d_seg3_id).get())->getPointer();
		int* segL3 = ((pdat::CellData<int> *) patch->getPatchData(d_segL3_id).get())->getPointer();
		int* segxLower = ((pdat::CellData<int> *) patch->getPatchData(d_segxLower_id).get())->getPointer();
		int* segLxLower = ((pdat::CellData<int> *) patch->getPatchData(d_segLxLower_id).get())->getPointer();
		int* segxUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segxUpper_id).get())->getPointer();
		int* segLxUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segLxUpper_id).get())->getPointer();
		int* segyLower = ((pdat::CellData<int> *) patch->getPatchData(d_segyLower_id).get())->getPointer();
		int* segLyLower = ((pdat::CellData<int> *) patch->getPatchData(d_segLyLower_id).get())->getPointer();
		int* segyUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segyUpper_id).get())->getPointer();
		int* segLyUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segLyUpper_id).get())->getPointer();
		int* segzLower = ((pdat::CellData<int> *) patch->getPatchData(d_segzLower_id).get())->getPointer();
		int* segLzLower = ((pdat::CellData<int> *) patch->getPatchData(d_segLzLower_id).get())->getPointer();
		int* segzUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segzUpper_id).get())->getPointer();
		int* segLzUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segLzUpper_id).get())->getPointer();
		int* region = ((pdat::CellData<int> *) patch->getPatchData(d_region_id).get())->getPointer();
		const hier::Index boxfirst1 = patch->getBox().lower();
		const hier::Index boxlast1  = patch->getBox().upper();
		const hier::Index boxfirst = problemVariable->getGhostBox().lower();
		const hier::Index boxlast  = problemVariable->getGhostBox().upper();
		int ilast = boxlast1(0)-boxfirst1(0) + 1 + 2 * d_ghost_width;
		int jlast = boxlast1(1)-boxfirst1(1) + 1 + 2 * d_ghost_width;
		int klast = boxlast1(2)-boxfirst1(2) + 1 + 2 * d_ghost_width;
	
		double fluxAccSBmx_3;
		double fluxAccSBmy_3;
		double fluxAccSBmz_3;
		double fluxAccSBe_3;
		double fluxAccSBrho_3;
		double fluxAccSBmx_1;
		double fluxAccSBmy_1;
		double fluxAccSBmz_1;
		double fluxAccSBe_1;
		double fluxAccSBrho_1;
		for (int index2 = boxfirst(2); index2 <= boxlast(2); index2++) {
			for (int index1 = boxfirst(1); index1 <= boxlast(1); index1++) {
				for (int index0 = boxfirst(0); index0 <= boxlast(0); index0++) {
					hier::Index idx(index0, index1, index2);
					Particles* part = problemVariable->getItem(idx);
					for (int pit = part->getNumberOfParticles() - 1; pit >= 0; pit--) {
						Particle* particle = part->getParticle(pit);
						if (vector(seg3, index0 - boxfirst(0), index1 - boxfirst(1), index2 - boxfirst(2)) == 1) {
							fluxAccSBmx_3 = (particle->rho_p * particle->fx_p) / particle->rho_p;
							fluxAccSBmy_3 = (particle->rho_p * particle->fy_p) / particle->rho_p;
							fluxAccSBmz_3 = (particle->rho_p * particle->fz_p) / particle->rho_p;
							fluxAccSBe_3 = (particle->fx_p * particle->mx_p + particle->fy_p * particle->my_p + particle->fz_p * particle->mz_p) / particle->rho_p;
							fluxAccSBrho_3 = 0.0;
							fluxAccSBrho_3 = fluxAccSBrho_3 - particle->GradSBirho_3;
							fluxAccSBrho_3 = fluxAccSBrho_3 - particle->GradSBjrho_3;
							fluxAccSBrho_3 = fluxAccSBrho_3 - particle->GradSBkrho_3;
							fluxAccSBmx_3 = fluxAccSBmx_3 - particle->GradSBimx_3;
							fluxAccSBmx_3 = fluxAccSBmx_3 - particle->GradSBjmx_3;
							fluxAccSBmx_3 = fluxAccSBmx_3 - particle->GradSBkmx_3;
							fluxAccSBmy_3 = fluxAccSBmy_3 - particle->GradSBimy_3;
							fluxAccSBmy_3 = fluxAccSBmy_3 - particle->GradSBjmy_3;
							fluxAccSBmy_3 = fluxAccSBmy_3 - particle->GradSBkmy_3;
							fluxAccSBmz_3 = fluxAccSBmz_3 - particle->GradSBimz_3;
							fluxAccSBmz_3 = fluxAccSBmz_3 - particle->GradSBjmz_3;
							fluxAccSBmz_3 = fluxAccSBmz_3 - particle->GradSBkmz_3;
							fluxAccSBe_3 = fluxAccSBe_3 - particle->GradSBie_3;
							fluxAccSBe_3 = fluxAccSBe_3 - particle->GradSBje_3;
							fluxAccSBe_3 = fluxAccSBe_3 - particle->GradSBke_3;
							particle->mxSPSprime_3 = particle->mx_p / particle->rho_p + simPlat_dt * 0.5 * fluxAccSBmx_3;
							particle->mySPSprime_3 = particle->my_p / particle->rho_p + simPlat_dt * 0.5 * fluxAccSBmy_3;
							particle->mzSPSprime_3 = particle->mz_p / particle->rho_p + simPlat_dt * 0.5 * fluxAccSBmz_3;
							particle->eSPSprime_3 = particle->e_p / particle->rho_p + simPlat_dt * 0.5 * fluxAccSBe_3;
							if (particle->region == 3 || particle->newRegion == 3) {
								particle->rhoSPprime = particle->rho_p + simPlat_dt * 0.5 * fluxAccSBrho_3;
								particle->mxSPprime = particle->mxSPSprime_3 * particle->rhoSPprime;
								particle->mySPprime = particle->mySPSprime_3 * particle->rhoSPprime;
								particle->mzSPprime = particle->mzSPSprime_3 * particle->rhoSPprime;
								particle->eSPprime = particle->eSPSprime_3 * particle->rhoSPprime;
								particle->positioniSPprime = particle->positioni_p + simPlat_dt * 0.5 * particle->FluxSBirho_3 / particle->rho_p;
								particle->positionjSPprime = particle->positionj_p + simPlat_dt * 0.5 * particle->FluxSBjrho_3 / particle->rho_p;
								particle->positionkSPprime = particle->positionk_p + simPlat_dt * 0.5 * particle->FluxSBkrho_3 / particle->rho_p;
							}
						}
						if (vector(seg1, index0 - boxfirst(0), index1 - boxfirst(1), index2 - boxfirst(2)) == 1) {
							fluxAccSBmx_1 = (particle->rho_p * particle->fx_p) / particle->rho_p;
							fluxAccSBmy_1 = (particle->rho_p * particle->fy_p) / particle->rho_p;
							fluxAccSBmz_1 = (particle->rho_p * particle->fz_p) / particle->rho_p;
							fluxAccSBe_1 = (particle->fx_p * particle->mx_p + particle->fy_p * particle->my_p + particle->fz_p * particle->mz_p) / particle->rho_p;
							fluxAccSBrho_1 = 0.0;
							fluxAccSBrho_1 = fluxAccSBrho_1 - particle->GradSBirho_1;
							fluxAccSBrho_1 = fluxAccSBrho_1 - particle->GradSBjrho_1;
							fluxAccSBrho_1 = fluxAccSBrho_1 - particle->GradSBkrho_1;
							fluxAccSBmx_1 = fluxAccSBmx_1 - particle->GradSBimx_1;
							fluxAccSBmx_1 = fluxAccSBmx_1 - particle->GradSBjmx_1;
							fluxAccSBmx_1 = fluxAccSBmx_1 - particle->GradSBkmx_1;
							fluxAccSBmy_1 = fluxAccSBmy_1 - particle->GradSBimy_1;
							fluxAccSBmy_1 = fluxAccSBmy_1 - particle->GradSBjmy_1;
							fluxAccSBmy_1 = fluxAccSBmy_1 - particle->GradSBkmy_1;
							fluxAccSBmz_1 = fluxAccSBmz_1 - particle->GradSBimz_1;
							fluxAccSBmz_1 = fluxAccSBmz_1 - particle->GradSBjmz_1;
							fluxAccSBmz_1 = fluxAccSBmz_1 - particle->GradSBkmz_1;
							fluxAccSBe_1 = fluxAccSBe_1 - particle->GradSBie_1;
							fluxAccSBe_1 = fluxAccSBe_1 - particle->GradSBje_1;
							fluxAccSBe_1 = fluxAccSBe_1 - particle->GradSBke_1;
							particle->mxSPSprime_1 = particle->mx_p / particle->rho_p + simPlat_dt * 0.5 * fluxAccSBmx_1;
							particle->mySPSprime_1 = particle->my_p / particle->rho_p + simPlat_dt * 0.5 * fluxAccSBmy_1;
							particle->mzSPSprime_1 = particle->mz_p / particle->rho_p + simPlat_dt * 0.5 * fluxAccSBmz_1;
							particle->eSPSprime_1 = particle->e_p / particle->rho_p + simPlat_dt * 0.5 * fluxAccSBe_1;
							if (particle->region == 1 || particle->newRegion == 1) {
								particle->rhoSPprime = particle->rho_p + simPlat_dt * 0.5 * fluxAccSBrho_1;
								particle->mxSPprime = particle->mxSPSprime_1 * particle->rhoSPprime;
								particle->mySPprime = particle->mySPSprime_1 * particle->rhoSPprime;
								particle->mzSPprime = particle->mzSPSprime_1 * particle->rhoSPprime;
								particle->eSPprime = particle->eSPSprime_1 * particle->rhoSPprime;
								particle->positioniSPprime = particle->positioni_p + simPlat_dt * 0.5 * particle->FluxSBirho_1 / particle->rho_p;
								particle->positionjSPprime = particle->positionj_p + simPlat_dt * 0.5 * particle->FluxSBjrho_1 / particle->rho_p;
								particle->positionkSPprime = particle->positionk_p + simPlat_dt * 0.5 * particle->FluxSBkrho_1 / particle->rho_p;
							}
						}
					}
				}
			}
		}
	}
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch > patch = *p_it;
		std::shared_ptr< pdat::IndexData<Particles, pdat::CellGeometry > > problemVariable(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles, pdat::CellGeometry >, hier::PatchData>(patch->getPatchData(d_problemVariable_id)));
		int* seg1 = ((pdat::CellData<int> *) patch->getPatchData(d_seg1_id).get())->getPointer();
		int* segL1 = ((pdat::CellData<int> *) patch->getPatchData(d_segL1_id).get())->getPointer();
		int* seg3 = ((pdat::CellData<int> *) patch->getPatchData(d_seg3_id).get())->getPointer();
		int* segL3 = ((pdat::CellData<int> *) patch->getPatchData(d_segL3_id).get())->getPointer();
		int* segxLower = ((pdat::CellData<int> *) patch->getPatchData(d_segxLower_id).get())->getPointer();
		int* segLxLower = ((pdat::CellData<int> *) patch->getPatchData(d_segLxLower_id).get())->getPointer();
		int* segxUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segxUpper_id).get())->getPointer();
		int* segLxUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segLxUpper_id).get())->getPointer();
		int* segyLower = ((pdat::CellData<int> *) patch->getPatchData(d_segyLower_id).get())->getPointer();
		int* segLyLower = ((pdat::CellData<int> *) patch->getPatchData(d_segLyLower_id).get())->getPointer();
		int* segyUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segyUpper_id).get())->getPointer();
		int* segLyUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segLyUpper_id).get())->getPointer();
		int* segzLower = ((pdat::CellData<int> *) patch->getPatchData(d_segzLower_id).get())->getPointer();
		int* segLzLower = ((pdat::CellData<int> *) patch->getPatchData(d_segLzLower_id).get())->getPointer();
		int* segzUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segzUpper_id).get())->getPointer();
		int* segLzUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segLzUpper_id).get())->getPointer();
		int* region = ((pdat::CellData<int> *) patch->getPatchData(d_region_id).get())->getPointer();
		const hier::Index boxfirst1 = patch->getBox().lower();
		const hier::Index boxlast1  = patch->getBox().upper();
		const hier::Index boxfirst = problemVariable->getGhostBox().lower();
		const hier::Index boxlast  = problemVariable->getGhostBox().upper();
		int ilast = boxlast1(0)-boxfirst1(0) + 1 + 2 * d_ghost_width;
		int jlast = boxlast1(1)-boxfirst1(1) + 1 + 2 * d_ghost_width;
		int klast = boxlast1(2)-boxfirst1(2) + 1 + 2 * d_ghost_width;
	
		for (int index2 = boxfirst(2); index2 <= boxlast(2); index2++) {
			for (int index1 = boxfirst(1); index1 <= boxlast(1); index1++) {
				for (int index0 = boxfirst(0); index0 <= boxlast(0); index0++) {
					hier::Index idx(index0, index1, index2);
					Particles* part = problemVariable->getItem(idx);
					for (int pit = part->getNumberOfParticles() - 1; pit >= 0; pit--) {
						Particle* particle = part->getParticle(pit);
						if (particle->region == 3 || particle->newRegion == 3) {
							moveParticles(patch, problemVariable, part, particle, index0, index1, index2, particle->positioni_p, particle->positionj_p, particle->positionk_p, particle->positioniSPprime, particle->positionjSPprime, particle->positionkSPprime, simPlat_dt, current_time, pit);
							continue;
						}
						if (particle->region == 1 || particle->newRegion == 1) {
							moveParticles(patch, problemVariable, part, particle, index0, index1, index2, particle->positioni_p, particle->positionj_p, particle->positionk_p, particle->positioniSPprime, particle->positionjSPprime, particle->positionkSPprime, simPlat_dt, current_time, pit);
							continue;
						}
						if (particle->region == 2 || particle->newRegion == 2) {
							moveParticles(patch, problemVariable, part, particle, index0, index1, index2, particle->positioni_p, particle->positionj_p, particle->positionk_p, particle->positioniSPprime, particle->positionjSPprime, particle->positionkSPprime, simPlat_dt, current_time, pit);
							continue;
						}
					}
				}
			}
		}
	}
	//Fill ghosts and periodical boundaries, but no other boundaries
	d_bdry_sched_advance[0]->fillData(current_time, false);
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch > patch = *p_it;
		std::shared_ptr< pdat::IndexData<Particles, pdat::CellGeometry > > problemVariable(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles, pdat::CellGeometry >, hier::PatchData>(patch->getPatchData(d_problemVariable_id)));
		int* seg1 = ((pdat::CellData<int> *) patch->getPatchData(d_seg1_id).get())->getPointer();
		int* segL1 = ((pdat::CellData<int> *) patch->getPatchData(d_segL1_id).get())->getPointer();
		int* seg3 = ((pdat::CellData<int> *) patch->getPatchData(d_seg3_id).get())->getPointer();
		int* segL3 = ((pdat::CellData<int> *) patch->getPatchData(d_segL3_id).get())->getPointer();
		int* segxLower = ((pdat::CellData<int> *) patch->getPatchData(d_segxLower_id).get())->getPointer();
		int* segLxLower = ((pdat::CellData<int> *) patch->getPatchData(d_segLxLower_id).get())->getPointer();
		int* segxUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segxUpper_id).get())->getPointer();
		int* segLxUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segLxUpper_id).get())->getPointer();
		int* segyLower = ((pdat::CellData<int> *) patch->getPatchData(d_segyLower_id).get())->getPointer();
		int* segLyLower = ((pdat::CellData<int> *) patch->getPatchData(d_segLyLower_id).get())->getPointer();
		int* segyUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segyUpper_id).get())->getPointer();
		int* segLyUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segLyUpper_id).get())->getPointer();
		int* segzLower = ((pdat::CellData<int> *) patch->getPatchData(d_segzLower_id).get())->getPointer();
		int* segLzLower = ((pdat::CellData<int> *) patch->getPatchData(d_segLzLower_id).get())->getPointer();
		int* segzUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segzUpper_id).get())->getPointer();
		int* segLzUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segLzUpper_id).get())->getPointer();
		int* region = ((pdat::CellData<int> *) patch->getPatchData(d_region_id).get())->getPointer();
		const hier::Index boxfirst1 = patch->getBox().lower();
		const hier::Index boxlast1  = patch->getBox().upper();
		const hier::Index boxfirst = problemVariable->getGhostBox().lower();
		const hier::Index boxlast  = problemVariable->getGhostBox().upper();
		int ilast = boxlast1(0)-boxfirst1(0) + 1 + 2 * d_ghost_width;
		int jlast = boxlast1(1)-boxfirst1(1) + 1 + 2 * d_ghost_width;
		int klast = boxlast1(2)-boxfirst1(2) + 1 + 2 * d_ghost_width;
	
		for (int index2 = boxfirst(2); index2 <= boxlast(2); index2++) {
			for (int index1 = boxfirst(1); index1 <= boxlast(1); index1++) {
				for (int index0 = boxfirst(0); index0 <= boxlast(0); index0++) {
					hier::Index idx(index0, index1, index2);
					//Correct the position if there is any periodical boundary
					Particles* part = problemVariable->getItem(idx);
					checkPosition(patch, index0, index1, index2, part);
				}
			}
		}
	}
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch > patch = *p_it;
		std::shared_ptr< pdat::IndexData<Particles, pdat::CellGeometry > > problemVariable(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles, pdat::CellGeometry >, hier::PatchData>(patch->getPatchData(d_problemVariable_id)));
		int* seg1 = ((pdat::CellData<int> *) patch->getPatchData(d_seg1_id).get())->getPointer();
		int* segL1 = ((pdat::CellData<int> *) patch->getPatchData(d_segL1_id).get())->getPointer();
		int* seg3 = ((pdat::CellData<int> *) patch->getPatchData(d_seg3_id).get())->getPointer();
		int* segL3 = ((pdat::CellData<int> *) patch->getPatchData(d_segL3_id).get())->getPointer();
		int* segxLower = ((pdat::CellData<int> *) patch->getPatchData(d_segxLower_id).get())->getPointer();
		int* segLxLower = ((pdat::CellData<int> *) patch->getPatchData(d_segLxLower_id).get())->getPointer();
		int* segxUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segxUpper_id).get())->getPointer();
		int* segLxUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segLxUpper_id).get())->getPointer();
		int* segyLower = ((pdat::CellData<int> *) patch->getPatchData(d_segyLower_id).get())->getPointer();
		int* segLyLower = ((pdat::CellData<int> *) patch->getPatchData(d_segLyLower_id).get())->getPointer();
		int* segyUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segyUpper_id).get())->getPointer();
		int* segLyUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segLyUpper_id).get())->getPointer();
		int* segzLower = ((pdat::CellData<int> *) patch->getPatchData(d_segzLower_id).get())->getPointer();
		int* segLzLower = ((pdat::CellData<int> *) patch->getPatchData(d_segLzLower_id).get())->getPointer();
		int* segzUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segzUpper_id).get())->getPointer();
		int* segLzUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segLzUpper_id).get())->getPointer();
		int* region = ((pdat::CellData<int> *) patch->getPatchData(d_region_id).get())->getPointer();
		const hier::Index boxfirst1 = patch->getBox().lower();
		const hier::Index boxlast1  = patch->getBox().upper();
		const hier::Index boxfirst = problemVariable->getGhostBox().lower();
		const hier::Index boxlast  = problemVariable->getGhostBox().upper();
		int ilast = boxlast1(0)-boxfirst1(0) + 1 + 2 * d_ghost_width;
		int jlast = boxlast1(1)-boxfirst1(1) + 1 + 2 * d_ghost_width;
		int klast = boxlast1(2)-boxfirst1(2) + 1 + 2 * d_ghost_width;
	
		for (int index2 = boxfirst(2); index2 <= boxlast(2); index2++) {
			for (int index1 = boxfirst(1); index1 <= boxlast(1); index1++) {
				for (int index0 = boxfirst(0); index0 <= boxlast(0); index0++) {
					hier::Index idx(index0, index1, index2);
					Particles* part = problemVariable->getItem(idx);
					for (int pit = part->getNumberOfParticles() - 1; pit >= 0; pit--) {
						Particle* particle = part->getParticle(pit);
						if (vector(seg3, index0 - boxfirst(0), index1 - boxfirst(1), index2 - boxfirst(2)) == 1) {
							if (particle->region == 3 || particle->newRegion == 3) {
								particle->fxSPprime = pfx;
								particle->fySPprime = pfy;
								particle->fzSPprime = pfz;
								particle->pSPprime = (gamma - 1.0) * (particle->eSPprime - particle->rhoSPprime * (((particle->mxSPprime / particle->rhoSPprime) * (particle->mxSPprime / particle->rhoSPprime)) / 2.0 + ((particle->mySPprime / particle->rhoSPprime) * (particle->mySPprime / particle->rhoSPprime)) / 2.0 + ((particle->mzSPprime / particle->rhoSPprime) * (particle->mzSPprime / particle->rhoSPprime)) / 2.0));
							}
							particle->FluxSBirho_3 = Firho_cubeI(particle->mxSPprime, dx, simPlat_dt);
							particle->FluxSBjrho_3 = Fjrho_cubeI(particle->mySPprime, dx, simPlat_dt);
							particle->FluxSBkrho_3 = Fkrho_cubeI(particle->mzSPprime, dx, simPlat_dt);
						}
						if (vector(seg1, index0 - boxfirst(0), index1 - boxfirst(1), index2 - boxfirst(2)) == 1) {
							if (particle->region == 1 || particle->newRegion == 1) {
								particle->fxSPprime = pfx;
								particle->fySPprime = pfy;
								particle->fzSPprime = pfz;
								particle->pSPprime = (gamma - 1.0) * (particle->eSPprime - particle->rhoSPprime * (((particle->mxSPprime / particle->rhoSPprime) * (particle->mxSPprime / particle->rhoSPprime)) / 2.0 + ((particle->mySPprime / particle->rhoSPprime) * (particle->mySPprime / particle->rhoSPprime)) / 2.0 + ((particle->mzSPprime / particle->rhoSPprime) * (particle->mzSPprime / particle->rhoSPprime)) / 2.0));
							}
							particle->FluxSBirho_1 = Firho_cubeI(particle->mxSPprime, dx, simPlat_dt);
							particle->FluxSBjrho_1 = Fjrho_cubeI(particle->mySPprime, dx, simPlat_dt);
							particle->FluxSBkrho_1 = Fkrho_cubeI(particle->mzSPprime, dx, simPlat_dt);
						}
						if (vector(seg3, index0 - boxfirst(0), index1 - boxfirst(1), index2 - boxfirst(2)) == 1) {
							particle->FluxSBimx_3 = Fimx_cubeI(particle->rhoSPprime, particle->mxSPprime, particle->pSPprime, dx, simPlat_dt);
							particle->FluxSBjmx_3 = Fjmx_cubeI(particle->rhoSPprime, particle->mxSPprime, particle->mySPprime, dx, simPlat_dt);
							particle->FluxSBkmx_3 = Fkmx_cubeI(particle->rhoSPprime, particle->mxSPprime, particle->mzSPprime, dx, simPlat_dt);
							particle->FluxSBimy_3 = Fimy_cubeI(particle->rhoSPprime, particle->mxSPprime, particle->mySPprime, dx, simPlat_dt);
							particle->FluxSBjmy_3 = Fjmy_cubeI(particle->rhoSPprime, particle->mySPprime, particle->pSPprime, dx, simPlat_dt);
							particle->FluxSBkmy_3 = Fkmy_cubeI(particle->rhoSPprime, particle->mySPprime, particle->mzSPprime, dx, simPlat_dt);
							particle->FluxSBimz_3 = Fimz_cubeI(particle->rhoSPprime, particle->mxSPprime, particle->mzSPprime, dx, simPlat_dt);
							particle->FluxSBjmz_3 = Fjmz_cubeI(particle->rhoSPprime, particle->mySPprime, particle->mzSPprime, dx, simPlat_dt);
							particle->FluxSBkmz_3 = Fkmz_cubeI(particle->rhoSPprime, particle->mzSPprime, particle->pSPprime, dx, simPlat_dt);
							particle->FluxSBie_3 = Fie_cubeI(particle->rhoSPprime, particle->mxSPprime, particle->eSPprime, particle->pSPprime, dx, simPlat_dt);
							particle->FluxSBje_3 = Fje_cubeI(particle->rhoSPprime, particle->mySPprime, particle->eSPprime, particle->pSPprime, dx, simPlat_dt);
							particle->FluxSBke_3 = Fke_cubeI(particle->rhoSPprime, particle->mzSPprime, particle->eSPprime, particle->pSPprime, dx, simPlat_dt);
							particle->mxSPSprime_3 = particle->mxSPprime / particle->rhoSPprime;
							particle->mySPSprime_3 = particle->mySPprime / particle->rhoSPprime;
							particle->mzSPSprime_3 = particle->mzSPprime / particle->rhoSPprime;
							particle->eSPSprime_3 = particle->eSPprime / particle->rhoSPprime;
							particle->GradSBirho_3 = 0.0;
							particle->GradSBjrho_3 = 0.0;
							particle->GradSBkrho_3 = 0.0;
							particle->GradSBimx_3 = 0.0;
							particle->GradSBjmx_3 = 0.0;
							particle->GradSBkmx_3 = 0.0;
							particle->GradSBimy_3 = 0.0;
							particle->GradSBjmy_3 = 0.0;
							particle->GradSBkmy_3 = 0.0;
							particle->GradSBimz_3 = 0.0;
							particle->GradSBjmz_3 = 0.0;
							particle->GradSBkmz_3 = 0.0;
							particle->GradSBie_3 = 0.0;
							particle->GradSBje_3 = 0.0;
							particle->GradSBke_3 = 0.0;
						}
						if (vector(seg1, index0 - boxfirst(0), index1 - boxfirst(1), index2 - boxfirst(2)) == 1) {
							particle->FluxSBimx_1 = Fimx_cubeI(particle->rhoSPprime, particle->mxSPprime, particle->pSPprime, dx, simPlat_dt);
							particle->FluxSBjmx_1 = Fjmx_cubeI(particle->rhoSPprime, particle->mxSPprime, particle->mySPprime, dx, simPlat_dt);
							particle->FluxSBkmx_1 = Fkmx_cubeI(particle->rhoSPprime, particle->mxSPprime, particle->mzSPprime, dx, simPlat_dt);
							particle->FluxSBimy_1 = Fimy_cubeI(particle->rhoSPprime, particle->mxSPprime, particle->mySPprime, dx, simPlat_dt);
							particle->FluxSBjmy_1 = Fjmy_cubeI(particle->rhoSPprime, particle->mySPprime, particle->pSPprime, dx, simPlat_dt);
							particle->FluxSBkmy_1 = Fkmy_cubeI(particle->rhoSPprime, particle->mySPprime, particle->mzSPprime, dx, simPlat_dt);
							particle->FluxSBimz_1 = Fimz_cubeI(particle->rhoSPprime, particle->mxSPprime, particle->mzSPprime, dx, simPlat_dt);
							particle->FluxSBjmz_1 = Fjmz_cubeI(particle->rhoSPprime, particle->mySPprime, particle->mzSPprime, dx, simPlat_dt);
							particle->FluxSBkmz_1 = Fkmz_cubeI(particle->rhoSPprime, particle->mzSPprime, particle->pSPprime, dx, simPlat_dt);
							particle->FluxSBie_1 = Fie_cubeI(particle->rhoSPprime, particle->mxSPprime, particle->eSPprime, particle->pSPprime, dx, simPlat_dt);
							particle->FluxSBje_1 = Fje_cubeI(particle->rhoSPprime, particle->mySPprime, particle->eSPprime, particle->pSPprime, dx, simPlat_dt);
							particle->FluxSBke_1 = Fke_cubeI(particle->rhoSPprime, particle->mzSPprime, particle->eSPprime, particle->pSPprime, dx, simPlat_dt);
							particle->mxSPSprime_1 = particle->mxSPprime / particle->rhoSPprime;
							particle->mySPSprime_1 = particle->mySPprime / particle->rhoSPprime;
							particle->mzSPSprime_1 = particle->mzSPprime / particle->rhoSPprime;
							particle->eSPSprime_1 = particle->eSPprime / particle->rhoSPprime;
							particle->GradSBirho_1 = 0.0;
							particle->GradSBjrho_1 = 0.0;
							particle->GradSBkrho_1 = 0.0;
							particle->GradSBimx_1 = 0.0;
							particle->GradSBjmx_1 = 0.0;
							particle->GradSBkmx_1 = 0.0;
							particle->GradSBimy_1 = 0.0;
							particle->GradSBjmy_1 = 0.0;
							particle->GradSBkmy_1 = 0.0;
							particle->GradSBimz_1 = 0.0;
							particle->GradSBjmz_1 = 0.0;
							particle->GradSBkmz_1 = 0.0;
							particle->GradSBie_1 = 0.0;
							particle->GradSBje_1 = 0.0;
							particle->GradSBke_1 = 0.0;
						}
					}
				}
			}
		}
	}
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch > patch = *p_it;
		std::shared_ptr< pdat::IndexData<Particles, pdat::CellGeometry > > problemVariable(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles, pdat::CellGeometry >, hier::PatchData>(patch->getPatchData(d_problemVariable_id)));
		int* seg1 = ((pdat::CellData<int> *) patch->getPatchData(d_seg1_id).get())->getPointer();
		int* segL1 = ((pdat::CellData<int> *) patch->getPatchData(d_segL1_id).get())->getPointer();
		int* seg3 = ((pdat::CellData<int> *) patch->getPatchData(d_seg3_id).get())->getPointer();
		int* segL3 = ((pdat::CellData<int> *) patch->getPatchData(d_segL3_id).get())->getPointer();
		int* segxLower = ((pdat::CellData<int> *) patch->getPatchData(d_segxLower_id).get())->getPointer();
		int* segLxLower = ((pdat::CellData<int> *) patch->getPatchData(d_segLxLower_id).get())->getPointer();
		int* segxUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segxUpper_id).get())->getPointer();
		int* segLxUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segLxUpper_id).get())->getPointer();
		int* segyLower = ((pdat::CellData<int> *) patch->getPatchData(d_segyLower_id).get())->getPointer();
		int* segLyLower = ((pdat::CellData<int> *) patch->getPatchData(d_segLyLower_id).get())->getPointer();
		int* segyUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segyUpper_id).get())->getPointer();
		int* segLyUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segLyUpper_id).get())->getPointer();
		int* segzLower = ((pdat::CellData<int> *) patch->getPatchData(d_segzLower_id).get())->getPointer();
		int* segLzLower = ((pdat::CellData<int> *) patch->getPatchData(d_segLzLower_id).get())->getPointer();
		int* segzUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segzUpper_id).get())->getPointer();
		int* segLzUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segLzUpper_id).get())->getPointer();
		int* region = ((pdat::CellData<int> *) patch->getPatchData(d_region_id).get())->getPointer();
		const hier::Index boxfirst1 = patch->getBox().lower();
		const hier::Index boxlast1  = patch->getBox().upper();
		const hier::Index boxfirst = problemVariable->getGhostBox().lower();
		const hier::Index boxlast  = problemVariable->getGhostBox().upper();
		int ilast = boxlast1(0)-boxfirst1(0) + 1 + 2 * d_ghost_width;
		int jlast = boxlast1(1)-boxfirst1(1) + 1 + 2 * d_ghost_width;
		int klast = boxlast1(2)-boxfirst1(2) + 1 + 2 * d_ghost_width;
	
		double dist_3;
		double dist_1;
		for (int index2 = boxfirst(2); index2 <= boxlast(2); index2++) {
			for (int index1 = boxfirst(1); index1 <= boxlast(1); index1++) {
				for (int index0 = boxfirst(0); index0 <= boxlast(0); index0++) {
					hier::Index idx(index0, index1, index2);
					Particles* part = problemVariable->getItem(idx);
					for (int pit = part->getNumberOfParticles() - 1; pit >= 0; pit--) {
						Particle* particle = part->getParticle(pit);
						if (vector(seg3, index0 - boxfirst(0), index1 - boxfirst(1), index2 - boxfirst(2)) == 1) {
							miniIndex = MAX(boxfirst(0), index0 - ceil((2.0 * influenceRadius_0)/dx[0]));
							maxiIndex = MIN(boxlast(0), index0 + ceil((2.0 * influenceRadius_0)/dx[0]));
							minjIndex = MAX(boxfirst(1), index1 - ceil((2.0 * influenceRadius_0)/dx[1]));
							maxjIndex = MIN(boxlast(1), index1 + ceil((2.0 * influenceRadius_0)/dx[1]));
							maxkIndex = MIN(boxlast(2), index2 + ceil((2.0 * influenceRadius_0)/dx[2]));
							for(int index2b = index2; index2b <= maxkIndex; index2b++) {
								if (index2b == index2)	{index1min = index1;}
								else					  {index1min = minjIndex;}
								for(int index1b = index1min; index1b <= maxjIndex; index1b++) {
									if ((index2b == index2) && (index1b == index1))	{index0min = index0;}
									else								  {index0min = miniIndex;}
									for(int index0b = index0min; index0b <= maxiIndex; index0b++) {
										if(vector(seg3, index0b - boxfirst(0), index1b - boxfirst(1), index2b - boxfirst(2)) == 1) {				
											hier::Index idxb(index0b, index1b, index2b);
											Particles* partb = problemVariable->getItem(idxb);
											if ( (index2b==index2) && (index1b==index1) && (index0b==index0)) {pitbmin = pit+1;}
											else								  {pitbmin = 0;}
											for (int pitb = pitbmin; pitb < partb->getNumberOfParticles(); pitb++) {
												Particle* particleb = partb->getParticle(pitb);
												//A-B and B-A interactions
												dist_3 = particleb->distanceSPprime(particle);
												if (lessThan(dist_3, (2.0 * influenceRadius_0)) && greaterThan(dist_3, 0.0)) {
													particle->GradSBimx_3 = particle->GradSBimx_3 + particleb->mass * ((particleb->FluxSBimx_3 - particleb->mxSPSprime_3 * particleb->FluxSBirho_3) / (particleb->rhoSPprime * particleb->rhoSPprime) + (particle->FluxSBimx_3 - particle->mxSPSprime_3 * particle->FluxSBirho_3) / (particle->rhoSPprime * particle->rhoSPprime)) * gW(dist_3, dx, simPlat_dt) * (particle->positioniSPprime - particleb->positioniSPprime);
													particle->GradSBjmx_3 = particle->GradSBjmx_3 + particleb->mass * ((particleb->FluxSBjmx_3 - particleb->mxSPSprime_3 * particleb->FluxSBjrho_3) / (particleb->rhoSPprime * particleb->rhoSPprime) + (particle->FluxSBjmx_3 - particle->mxSPSprime_3 * particle->FluxSBjrho_3) / (particle->rhoSPprime * particle->rhoSPprime)) * gW(dist_3, dx, simPlat_dt) * (particle->positionjSPprime - particleb->positionjSPprime);
													particle->GradSBkmx_3 = particle->GradSBkmx_3 + particleb->mass * ((particleb->FluxSBkmx_3 - particleb->mxSPSprime_3 * particleb->FluxSBkrho_3) / (particleb->rhoSPprime * particleb->rhoSPprime) + (particle->FluxSBkmx_3 - particle->mxSPSprime_3 * particle->FluxSBkrho_3) / (particle->rhoSPprime * particle->rhoSPprime)) * gW(dist_3, dx, simPlat_dt) * (particle->positionkSPprime - particleb->positionkSPprime);
													particle->GradSBimy_3 = particle->GradSBimy_3 + particleb->mass * ((particleb->FluxSBimy_3 - particleb->mySPSprime_3 * particleb->FluxSBirho_3) / (particleb->rhoSPprime * particleb->rhoSPprime) + (particle->FluxSBimy_3 - particle->mySPSprime_3 * particle->FluxSBirho_3) / (particle->rhoSPprime * particle->rhoSPprime)) * gW(dist_3, dx, simPlat_dt) * (particle->positioniSPprime - particleb->positioniSPprime);
													particle->GradSBjmy_3 = particle->GradSBjmy_3 + particleb->mass * ((particleb->FluxSBjmy_3 - particleb->mySPSprime_3 * particleb->FluxSBjrho_3) / (particleb->rhoSPprime * particleb->rhoSPprime) + (particle->FluxSBjmy_3 - particle->mySPSprime_3 * particle->FluxSBjrho_3) / (particle->rhoSPprime * particle->rhoSPprime)) * gW(dist_3, dx, simPlat_dt) * (particle->positionjSPprime - particleb->positionjSPprime);
													particle->GradSBkmy_3 = particle->GradSBkmy_3 + particleb->mass * ((particleb->FluxSBkmy_3 - particleb->mySPSprime_3 * particleb->FluxSBkrho_3) / (particleb->rhoSPprime * particleb->rhoSPprime) + (particle->FluxSBkmy_3 - particle->mySPSprime_3 * particle->FluxSBkrho_3) / (particle->rhoSPprime * particle->rhoSPprime)) * gW(dist_3, dx, simPlat_dt) * (particle->positionkSPprime - particleb->positionkSPprime);
													particle->GradSBimz_3 = particle->GradSBimz_3 + particleb->mass * ((particleb->FluxSBimz_3 - particleb->mzSPSprime_3 * particleb->FluxSBirho_3) / (particleb->rhoSPprime * particleb->rhoSPprime) + (particle->FluxSBimz_3 - particle->mzSPSprime_3 * particle->FluxSBirho_3) / (particle->rhoSPprime * particle->rhoSPprime)) * gW(dist_3, dx, simPlat_dt) * (particle->positioniSPprime - particleb->positioniSPprime);
													particle->GradSBjmz_3 = particle->GradSBjmz_3 + particleb->mass * ((particleb->FluxSBjmz_3 - particleb->mzSPSprime_3 * particleb->FluxSBjrho_3) / (particleb->rhoSPprime * particleb->rhoSPprime) + (particle->FluxSBjmz_3 - particle->mzSPSprime_3 * particle->FluxSBjrho_3) / (particle->rhoSPprime * particle->rhoSPprime)) * gW(dist_3, dx, simPlat_dt) * (particle->positionjSPprime - particleb->positionjSPprime);
													particle->GradSBkmz_3 = particle->GradSBkmz_3 + particleb->mass * ((particleb->FluxSBkmz_3 - particleb->mzSPSprime_3 * particleb->FluxSBkrho_3) / (particleb->rhoSPprime * particleb->rhoSPprime) + (particle->FluxSBkmz_3 - particle->mzSPSprime_3 * particle->FluxSBkrho_3) / (particle->rhoSPprime * particle->rhoSPprime)) * gW(dist_3, dx, simPlat_dt) * (particle->positionkSPprime - particleb->positionkSPprime);
													particle->GradSBie_3 = particle->GradSBie_3 + particleb->mass * ((particleb->FluxSBie_3 - particleb->eSPSprime_3 * particleb->FluxSBirho_3) / (particleb->rhoSPprime * particleb->rhoSPprime) + (particle->FluxSBie_3 - particle->eSPSprime_3 * particle->FluxSBirho_3) / (particle->rhoSPprime * particle->rhoSPprime)) * gW(dist_3, dx, simPlat_dt) * (particle->positioniSPprime - particleb->positioniSPprime);
													particle->GradSBje_3 = particle->GradSBje_3 + particleb->mass * ((particleb->FluxSBje_3 - particleb->eSPSprime_3 * particleb->FluxSBjrho_3) / (particleb->rhoSPprime * particleb->rhoSPprime) + (particle->FluxSBje_3 - particle->eSPSprime_3 * particle->FluxSBjrho_3) / (particle->rhoSPprime * particle->rhoSPprime)) * gW(dist_3, dx, simPlat_dt) * (particle->positionjSPprime - particleb->positionjSPprime);
													particle->GradSBke_3 = particle->GradSBke_3 + particleb->mass * ((particleb->FluxSBke_3 - particleb->eSPSprime_3 * particleb->FluxSBkrho_3) / (particleb->rhoSPprime * particleb->rhoSPprime) + (particle->FluxSBke_3 - particle->eSPSprime_3 * particle->FluxSBkrho_3) / (particle->rhoSPprime * particle->rhoSPprime)) * gW(dist_3, dx, simPlat_dt) * (particle->positionkSPprime - particleb->positionkSPprime);
													particle->GradSBirho_3 = particle->GradSBirho_3 + particle->rhoSPprime * particleb->mass / particleb->rhoSPprime * (particleb->FluxSBirho_3 / particleb->rhoSPprime - particle->FluxSBirho_3 / particle->rhoSPprime) * gW(dist_3, dx, simPlat_dt) * (particle->positioniSPprime - particleb->positioniSPprime);
													particle->GradSBjrho_3 = particle->GradSBjrho_3 + particle->rhoSPprime * particleb->mass / particleb->rhoSPprime * (particleb->FluxSBjrho_3 / particleb->rhoSPprime - particle->FluxSBjrho_3 / particle->rhoSPprime) * gW(dist_3, dx, simPlat_dt) * (particle->positionjSPprime - particleb->positionjSPprime);
													particle->GradSBkrho_3 = particle->GradSBkrho_3 + particle->rhoSPprime * particleb->mass / particleb->rhoSPprime * (particleb->FluxSBkrho_3 / particleb->rhoSPprime - particle->FluxSBkrho_3 / particle->rhoSPprime) * gW(dist_3, dx, simPlat_dt) * (particle->positionkSPprime - particleb->positionkSPprime);
												}
												dist_3 = particleb->distanceSPprime(particle);
												if (lessThan(dist_3, (2.0 * influenceRadius_0)) && greaterThan(dist_3, 0.0)) {
													particleb->GradSBimx_3 = particleb->GradSBimx_3 + particle->mass * ((particle->FluxSBimx_3 - particle->mxSPSprime_3 * particle->FluxSBirho_3) / (particle->rhoSPprime * particle->rhoSPprime) + (particleb->FluxSBimx_3 - particleb->mxSPSprime_3 * particleb->FluxSBirho_3) / (particleb->rhoSPprime * particleb->rhoSPprime)) * gW(dist_3, dx, simPlat_dt) * (particleb->positioniSPprime - particle->positioniSPprime);
													particleb->GradSBjmx_3 = particleb->GradSBjmx_3 + particle->mass * ((particle->FluxSBjmx_3 - particle->mxSPSprime_3 * particle->FluxSBjrho_3) / (particle->rhoSPprime * particle->rhoSPprime) + (particleb->FluxSBjmx_3 - particleb->mxSPSprime_3 * particleb->FluxSBjrho_3) / (particleb->rhoSPprime * particleb->rhoSPprime)) * gW(dist_3, dx, simPlat_dt) * (particleb->positionjSPprime - particle->positionjSPprime);
													particleb->GradSBkmx_3 = particleb->GradSBkmx_3 + particle->mass * ((particle->FluxSBkmx_3 - particle->mxSPSprime_3 * particle->FluxSBkrho_3) / (particle->rhoSPprime * particle->rhoSPprime) + (particleb->FluxSBkmx_3 - particleb->mxSPSprime_3 * particleb->FluxSBkrho_3) / (particleb->rhoSPprime * particleb->rhoSPprime)) * gW(dist_3, dx, simPlat_dt) * (particleb->positionkSPprime - particle->positionkSPprime);
													particleb->GradSBimy_3 = particleb->GradSBimy_3 + particle->mass * ((particle->FluxSBimy_3 - particle->mySPSprime_3 * particle->FluxSBirho_3) / (particle->rhoSPprime * particle->rhoSPprime) + (particleb->FluxSBimy_3 - particleb->mySPSprime_3 * particleb->FluxSBirho_3) / (particleb->rhoSPprime * particleb->rhoSPprime)) * gW(dist_3, dx, simPlat_dt) * (particleb->positioniSPprime - particle->positioniSPprime);
													particleb->GradSBjmy_3 = particleb->GradSBjmy_3 + particle->mass * ((particle->FluxSBjmy_3 - particle->mySPSprime_3 * particle->FluxSBjrho_3) / (particle->rhoSPprime * particle->rhoSPprime) + (particleb->FluxSBjmy_3 - particleb->mySPSprime_3 * particleb->FluxSBjrho_3) / (particleb->rhoSPprime * particleb->rhoSPprime)) * gW(dist_3, dx, simPlat_dt) * (particleb->positionjSPprime - particle->positionjSPprime);
													particleb->GradSBkmy_3 = particleb->GradSBkmy_3 + particle->mass * ((particle->FluxSBkmy_3 - particle->mySPSprime_3 * particle->FluxSBkrho_3) / (particle->rhoSPprime * particle->rhoSPprime) + (particleb->FluxSBkmy_3 - particleb->mySPSprime_3 * particleb->FluxSBkrho_3) / (particleb->rhoSPprime * particleb->rhoSPprime)) * gW(dist_3, dx, simPlat_dt) * (particleb->positionkSPprime - particle->positionkSPprime);
													particleb->GradSBimz_3 = particleb->GradSBimz_3 + particle->mass * ((particle->FluxSBimz_3 - particle->mzSPSprime_3 * particle->FluxSBirho_3) / (particle->rhoSPprime * particle->rhoSPprime) + (particleb->FluxSBimz_3 - particleb->mzSPSprime_3 * particleb->FluxSBirho_3) / (particleb->rhoSPprime * particleb->rhoSPprime)) * gW(dist_3, dx, simPlat_dt) * (particleb->positioniSPprime - particle->positioniSPprime);
													particleb->GradSBjmz_3 = particleb->GradSBjmz_3 + particle->mass * ((particle->FluxSBjmz_3 - particle->mzSPSprime_3 * particle->FluxSBjrho_3) / (particle->rhoSPprime * particle->rhoSPprime) + (particleb->FluxSBjmz_3 - particleb->mzSPSprime_3 * particleb->FluxSBjrho_3) / (particleb->rhoSPprime * particleb->rhoSPprime)) * gW(dist_3, dx, simPlat_dt) * (particleb->positionjSPprime - particle->positionjSPprime);
													particleb->GradSBkmz_3 = particleb->GradSBkmz_3 + particle->mass * ((particle->FluxSBkmz_3 - particle->mzSPSprime_3 * particle->FluxSBkrho_3) / (particle->rhoSPprime * particle->rhoSPprime) + (particleb->FluxSBkmz_3 - particleb->mzSPSprime_3 * particleb->FluxSBkrho_3) / (particleb->rhoSPprime * particleb->rhoSPprime)) * gW(dist_3, dx, simPlat_dt) * (particleb->positionkSPprime - particle->positionkSPprime);
													particleb->GradSBie_3 = particleb->GradSBie_3 + particle->mass * ((particle->FluxSBie_3 - particle->eSPSprime_3 * particle->FluxSBirho_3) / (particle->rhoSPprime * particle->rhoSPprime) + (particleb->FluxSBie_3 - particleb->eSPSprime_3 * particleb->FluxSBirho_3) / (particleb->rhoSPprime * particleb->rhoSPprime)) * gW(dist_3, dx, simPlat_dt) * (particleb->positioniSPprime - particle->positioniSPprime);
													particleb->GradSBje_3 = particleb->GradSBje_3 + particle->mass * ((particle->FluxSBje_3 - particle->eSPSprime_3 * particle->FluxSBjrho_3) / (particle->rhoSPprime * particle->rhoSPprime) + (particleb->FluxSBje_3 - particleb->eSPSprime_3 * particleb->FluxSBjrho_3) / (particleb->rhoSPprime * particleb->rhoSPprime)) * gW(dist_3, dx, simPlat_dt) * (particleb->positionjSPprime - particle->positionjSPprime);
													particleb->GradSBke_3 = particleb->GradSBke_3 + particle->mass * ((particle->FluxSBke_3 - particle->eSPSprime_3 * particle->FluxSBkrho_3) / (particle->rhoSPprime * particle->rhoSPprime) + (particleb->FluxSBke_3 - particleb->eSPSprime_3 * particleb->FluxSBkrho_3) / (particleb->rhoSPprime * particleb->rhoSPprime)) * gW(dist_3, dx, simPlat_dt) * (particleb->positionkSPprime - particle->positionkSPprime);
													particleb->GradSBirho_3 = particleb->GradSBirho_3 + particleb->rhoSPprime * particle->mass / particle->rhoSPprime * (particle->FluxSBirho_3 / particle->rhoSPprime - particleb->FluxSBirho_3 / particleb->rhoSPprime) * gW(dist_3, dx, simPlat_dt) * (particleb->positioniSPprime - particle->positioniSPprime);
													particleb->GradSBjrho_3 = particleb->GradSBjrho_3 + particleb->rhoSPprime * particle->mass / particle->rhoSPprime * (particle->FluxSBjrho_3 / particle->rhoSPprime - particleb->FluxSBjrho_3 / particleb->rhoSPprime) * gW(dist_3, dx, simPlat_dt) * (particleb->positionjSPprime - particle->positionjSPprime);
													particleb->GradSBkrho_3 = particleb->GradSBkrho_3 + particleb->rhoSPprime * particle->mass / particle->rhoSPprime * (particle->FluxSBkrho_3 / particle->rhoSPprime - particleb->FluxSBkrho_3 / particleb->rhoSPprime) * gW(dist_3, dx, simPlat_dt) * (particleb->positionkSPprime - particle->positionkSPprime);
												}
											}
										}				
									}
								}
							}
						}
						if (vector(seg1, index0 - boxfirst(0), index1 - boxfirst(1), index2 - boxfirst(2)) == 1) {
							miniIndex = MAX(boxfirst(0), index0 - ceil((2.0 * influenceRadius_0)/dx[0]));
							maxiIndex = MIN(boxlast(0), index0 + ceil((2.0 * influenceRadius_0)/dx[0]));
							minjIndex = MAX(boxfirst(1), index1 - ceil((2.0 * influenceRadius_0)/dx[1]));
							maxjIndex = MIN(boxlast(1), index1 + ceil((2.0 * influenceRadius_0)/dx[1]));
							maxkIndex = MIN(boxlast(2), index2 + ceil((2.0 * influenceRadius_0)/dx[2]));
							for(int index2b = index2; index2b <= maxkIndex; index2b++) {
								if (index2b == index2)	{index1min = index1;}
								else					  {index1min = minjIndex;}
								for(int index1b = index1min; index1b <= maxjIndex; index1b++) {
									if ((index2b == index2) && (index1b == index1))	{index0min = index0;}
									else								  {index0min = miniIndex;}
									for(int index0b = index0min; index0b <= maxiIndex; index0b++) {
										if(vector(seg1, index0b - boxfirst(0), index1b - boxfirst(1), index2b - boxfirst(2)) == 1) {				
											hier::Index idxb(index0b, index1b, index2b);
											Particles* partb = problemVariable->getItem(idxb);
											if ( (index2b==index2) && (index1b==index1) && (index0b==index0)) {pitbmin = pit+1;}
											else								  {pitbmin = 0;}
											for (int pitb = pitbmin; pitb < partb->getNumberOfParticles(); pitb++) {
												Particle* particleb = partb->getParticle(pitb);
												//A-B and B-A interactions
												dist_1 = particleb->distanceSPprime(particle);
												if (lessThan(dist_1, (2.0 * influenceRadius_0)) && greaterThan(dist_1, 0.0)) {
													particle->GradSBimx_1 = particle->GradSBimx_1 + particleb->mass * ((particleb->FluxSBimx_1 - particleb->mxSPSprime_1 * particleb->FluxSBirho_1) / (particleb->rhoSPprime * particleb->rhoSPprime) + (particle->FluxSBimx_1 - particle->mxSPSprime_1 * particle->FluxSBirho_1) / (particle->rhoSPprime * particle->rhoSPprime)) * gW(dist_1, dx, simPlat_dt) * (particle->positioniSPprime - particleb->positioniSPprime);
													particle->GradSBjmx_1 = particle->GradSBjmx_1 + particleb->mass * ((particleb->FluxSBjmx_1 - particleb->mxSPSprime_1 * particleb->FluxSBjrho_1) / (particleb->rhoSPprime * particleb->rhoSPprime) + (particle->FluxSBjmx_1 - particle->mxSPSprime_1 * particle->FluxSBjrho_1) / (particle->rhoSPprime * particle->rhoSPprime)) * gW(dist_1, dx, simPlat_dt) * (particle->positionjSPprime - particleb->positionjSPprime);
													particle->GradSBkmx_1 = particle->GradSBkmx_1 + particleb->mass * ((particleb->FluxSBkmx_1 - particleb->mxSPSprime_1 * particleb->FluxSBkrho_1) / (particleb->rhoSPprime * particleb->rhoSPprime) + (particle->FluxSBkmx_1 - particle->mxSPSprime_1 * particle->FluxSBkrho_1) / (particle->rhoSPprime * particle->rhoSPprime)) * gW(dist_1, dx, simPlat_dt) * (particle->positionkSPprime - particleb->positionkSPprime);
													particle->GradSBimy_1 = particle->GradSBimy_1 + particleb->mass * ((particleb->FluxSBimy_1 - particleb->mySPSprime_1 * particleb->FluxSBirho_1) / (particleb->rhoSPprime * particleb->rhoSPprime) + (particle->FluxSBimy_1 - particle->mySPSprime_1 * particle->FluxSBirho_1) / (particle->rhoSPprime * particle->rhoSPprime)) * gW(dist_1, dx, simPlat_dt) * (particle->positioniSPprime - particleb->positioniSPprime);
													particle->GradSBjmy_1 = particle->GradSBjmy_1 + particleb->mass * ((particleb->FluxSBjmy_1 - particleb->mySPSprime_1 * particleb->FluxSBjrho_1) / (particleb->rhoSPprime * particleb->rhoSPprime) + (particle->FluxSBjmy_1 - particle->mySPSprime_1 * particle->FluxSBjrho_1) / (particle->rhoSPprime * particle->rhoSPprime)) * gW(dist_1, dx, simPlat_dt) * (particle->positionjSPprime - particleb->positionjSPprime);
													particle->GradSBkmy_1 = particle->GradSBkmy_1 + particleb->mass * ((particleb->FluxSBkmy_1 - particleb->mySPSprime_1 * particleb->FluxSBkrho_1) / (particleb->rhoSPprime * particleb->rhoSPprime) + (particle->FluxSBkmy_1 - particle->mySPSprime_1 * particle->FluxSBkrho_1) / (particle->rhoSPprime * particle->rhoSPprime)) * gW(dist_1, dx, simPlat_dt) * (particle->positionkSPprime - particleb->positionkSPprime);
													particle->GradSBimz_1 = particle->GradSBimz_1 + particleb->mass * ((particleb->FluxSBimz_1 - particleb->mzSPSprime_1 * particleb->FluxSBirho_1) / (particleb->rhoSPprime * particleb->rhoSPprime) + (particle->FluxSBimz_1 - particle->mzSPSprime_1 * particle->FluxSBirho_1) / (particle->rhoSPprime * particle->rhoSPprime)) * gW(dist_1, dx, simPlat_dt) * (particle->positioniSPprime - particleb->positioniSPprime);
													particle->GradSBjmz_1 = particle->GradSBjmz_1 + particleb->mass * ((particleb->FluxSBjmz_1 - particleb->mzSPSprime_1 * particleb->FluxSBjrho_1) / (particleb->rhoSPprime * particleb->rhoSPprime) + (particle->FluxSBjmz_1 - particle->mzSPSprime_1 * particle->FluxSBjrho_1) / (particle->rhoSPprime * particle->rhoSPprime)) * gW(dist_1, dx, simPlat_dt) * (particle->positionjSPprime - particleb->positionjSPprime);
													particle->GradSBkmz_1 = particle->GradSBkmz_1 + particleb->mass * ((particleb->FluxSBkmz_1 - particleb->mzSPSprime_1 * particleb->FluxSBkrho_1) / (particleb->rhoSPprime * particleb->rhoSPprime) + (particle->FluxSBkmz_1 - particle->mzSPSprime_1 * particle->FluxSBkrho_1) / (particle->rhoSPprime * particle->rhoSPprime)) * gW(dist_1, dx, simPlat_dt) * (particle->positionkSPprime - particleb->positionkSPprime);
													particle->GradSBie_1 = particle->GradSBie_1 + particleb->mass * ((particleb->FluxSBie_1 - particleb->eSPSprime_1 * particleb->FluxSBirho_1) / (particleb->rhoSPprime * particleb->rhoSPprime) + (particle->FluxSBie_1 - particle->eSPSprime_1 * particle->FluxSBirho_1) / (particle->rhoSPprime * particle->rhoSPprime)) * gW(dist_1, dx, simPlat_dt) * (particle->positioniSPprime - particleb->positioniSPprime);
													particle->GradSBje_1 = particle->GradSBje_1 + particleb->mass * ((particleb->FluxSBje_1 - particleb->eSPSprime_1 * particleb->FluxSBjrho_1) / (particleb->rhoSPprime * particleb->rhoSPprime) + (particle->FluxSBje_1 - particle->eSPSprime_1 * particle->FluxSBjrho_1) / (particle->rhoSPprime * particle->rhoSPprime)) * gW(dist_1, dx, simPlat_dt) * (particle->positionjSPprime - particleb->positionjSPprime);
													particle->GradSBke_1 = particle->GradSBke_1 + particleb->mass * ((particleb->FluxSBke_1 - particleb->eSPSprime_1 * particleb->FluxSBkrho_1) / (particleb->rhoSPprime * particleb->rhoSPprime) + (particle->FluxSBke_1 - particle->eSPSprime_1 * particle->FluxSBkrho_1) / (particle->rhoSPprime * particle->rhoSPprime)) * gW(dist_1, dx, simPlat_dt) * (particle->positionkSPprime - particleb->positionkSPprime);
													particle->GradSBirho_1 = particle->GradSBirho_1 + particle->rhoSPprime * particleb->mass / particleb->rhoSPprime * (particleb->FluxSBirho_1 / particleb->rhoSPprime - particle->FluxSBirho_1 / particle->rhoSPprime) * gW(dist_1, dx, simPlat_dt) * (particle->positioniSPprime - particleb->positioniSPprime);
													particle->GradSBjrho_1 = particle->GradSBjrho_1 + particle->rhoSPprime * particleb->mass / particleb->rhoSPprime * (particleb->FluxSBjrho_1 / particleb->rhoSPprime - particle->FluxSBjrho_1 / particle->rhoSPprime) * gW(dist_1, dx, simPlat_dt) * (particle->positionjSPprime - particleb->positionjSPprime);
													particle->GradSBkrho_1 = particle->GradSBkrho_1 + particle->rhoSPprime * particleb->mass / particleb->rhoSPprime * (particleb->FluxSBkrho_1 / particleb->rhoSPprime - particle->FluxSBkrho_1 / particle->rhoSPprime) * gW(dist_1, dx, simPlat_dt) * (particle->positionkSPprime - particleb->positionkSPprime);
												}
												dist_1 = particleb->distanceSPprime(particle);
												if (lessThan(dist_1, (2.0 * influenceRadius_0)) && greaterThan(dist_1, 0.0)) {
													particleb->GradSBimx_1 = particleb->GradSBimx_1 + particle->mass * ((particle->FluxSBimx_1 - particle->mxSPSprime_1 * particle->FluxSBirho_1) / (particle->rhoSPprime * particle->rhoSPprime) + (particleb->FluxSBimx_1 - particleb->mxSPSprime_1 * particleb->FluxSBirho_1) / (particleb->rhoSPprime * particleb->rhoSPprime)) * gW(dist_1, dx, simPlat_dt) * (particleb->positioniSPprime - particle->positioniSPprime);
													particleb->GradSBjmx_1 = particleb->GradSBjmx_1 + particle->mass * ((particle->FluxSBjmx_1 - particle->mxSPSprime_1 * particle->FluxSBjrho_1) / (particle->rhoSPprime * particle->rhoSPprime) + (particleb->FluxSBjmx_1 - particleb->mxSPSprime_1 * particleb->FluxSBjrho_1) / (particleb->rhoSPprime * particleb->rhoSPprime)) * gW(dist_1, dx, simPlat_dt) * (particleb->positionjSPprime - particle->positionjSPprime);
													particleb->GradSBkmx_1 = particleb->GradSBkmx_1 + particle->mass * ((particle->FluxSBkmx_1 - particle->mxSPSprime_1 * particle->FluxSBkrho_1) / (particle->rhoSPprime * particle->rhoSPprime) + (particleb->FluxSBkmx_1 - particleb->mxSPSprime_1 * particleb->FluxSBkrho_1) / (particleb->rhoSPprime * particleb->rhoSPprime)) * gW(dist_1, dx, simPlat_dt) * (particleb->positionkSPprime - particle->positionkSPprime);
													particleb->GradSBimy_1 = particleb->GradSBimy_1 + particle->mass * ((particle->FluxSBimy_1 - particle->mySPSprime_1 * particle->FluxSBirho_1) / (particle->rhoSPprime * particle->rhoSPprime) + (particleb->FluxSBimy_1 - particleb->mySPSprime_1 * particleb->FluxSBirho_1) / (particleb->rhoSPprime * particleb->rhoSPprime)) * gW(dist_1, dx, simPlat_dt) * (particleb->positioniSPprime - particle->positioniSPprime);
													particleb->GradSBjmy_1 = particleb->GradSBjmy_1 + particle->mass * ((particle->FluxSBjmy_1 - particle->mySPSprime_1 * particle->FluxSBjrho_1) / (particle->rhoSPprime * particle->rhoSPprime) + (particleb->FluxSBjmy_1 - particleb->mySPSprime_1 * particleb->FluxSBjrho_1) / (particleb->rhoSPprime * particleb->rhoSPprime)) * gW(dist_1, dx, simPlat_dt) * (particleb->positionjSPprime - particle->positionjSPprime);
													particleb->GradSBkmy_1 = particleb->GradSBkmy_1 + particle->mass * ((particle->FluxSBkmy_1 - particle->mySPSprime_1 * particle->FluxSBkrho_1) / (particle->rhoSPprime * particle->rhoSPprime) + (particleb->FluxSBkmy_1 - particleb->mySPSprime_1 * particleb->FluxSBkrho_1) / (particleb->rhoSPprime * particleb->rhoSPprime)) * gW(dist_1, dx, simPlat_dt) * (particleb->positionkSPprime - particle->positionkSPprime);
													particleb->GradSBimz_1 = particleb->GradSBimz_1 + particle->mass * ((particle->FluxSBimz_1 - particle->mzSPSprime_1 * particle->FluxSBirho_1) / (particle->rhoSPprime * particle->rhoSPprime) + (particleb->FluxSBimz_1 - particleb->mzSPSprime_1 * particleb->FluxSBirho_1) / (particleb->rhoSPprime * particleb->rhoSPprime)) * gW(dist_1, dx, simPlat_dt) * (particleb->positioniSPprime - particle->positioniSPprime);
													particleb->GradSBjmz_1 = particleb->GradSBjmz_1 + particle->mass * ((particle->FluxSBjmz_1 - particle->mzSPSprime_1 * particle->FluxSBjrho_1) / (particle->rhoSPprime * particle->rhoSPprime) + (particleb->FluxSBjmz_1 - particleb->mzSPSprime_1 * particleb->FluxSBjrho_1) / (particleb->rhoSPprime * particleb->rhoSPprime)) * gW(dist_1, dx, simPlat_dt) * (particleb->positionjSPprime - particle->positionjSPprime);
													particleb->GradSBkmz_1 = particleb->GradSBkmz_1 + particle->mass * ((particle->FluxSBkmz_1 - particle->mzSPSprime_1 * particle->FluxSBkrho_1) / (particle->rhoSPprime * particle->rhoSPprime) + (particleb->FluxSBkmz_1 - particleb->mzSPSprime_1 * particleb->FluxSBkrho_1) / (particleb->rhoSPprime * particleb->rhoSPprime)) * gW(dist_1, dx, simPlat_dt) * (particleb->positionkSPprime - particle->positionkSPprime);
													particleb->GradSBie_1 = particleb->GradSBie_1 + particle->mass * ((particle->FluxSBie_1 - particle->eSPSprime_1 * particle->FluxSBirho_1) / (particle->rhoSPprime * particle->rhoSPprime) + (particleb->FluxSBie_1 - particleb->eSPSprime_1 * particleb->FluxSBirho_1) / (particleb->rhoSPprime * particleb->rhoSPprime)) * gW(dist_1, dx, simPlat_dt) * (particleb->positioniSPprime - particle->positioniSPprime);
													particleb->GradSBje_1 = particleb->GradSBje_1 + particle->mass * ((particle->FluxSBje_1 - particle->eSPSprime_1 * particle->FluxSBjrho_1) / (particle->rhoSPprime * particle->rhoSPprime) + (particleb->FluxSBje_1 - particleb->eSPSprime_1 * particleb->FluxSBjrho_1) / (particleb->rhoSPprime * particleb->rhoSPprime)) * gW(dist_1, dx, simPlat_dt) * (particleb->positionjSPprime - particle->positionjSPprime);
													particleb->GradSBke_1 = particleb->GradSBke_1 + particle->mass * ((particle->FluxSBke_1 - particle->eSPSprime_1 * particle->FluxSBkrho_1) / (particle->rhoSPprime * particle->rhoSPprime) + (particleb->FluxSBke_1 - particleb->eSPSprime_1 * particleb->FluxSBkrho_1) / (particleb->rhoSPprime * particleb->rhoSPprime)) * gW(dist_1, dx, simPlat_dt) * (particleb->positionkSPprime - particle->positionkSPprime);
													particleb->GradSBirho_1 = particleb->GradSBirho_1 + particleb->rhoSPprime * particle->mass / particle->rhoSPprime * (particle->FluxSBirho_1 / particle->rhoSPprime - particleb->FluxSBirho_1 / particleb->rhoSPprime) * gW(dist_1, dx, simPlat_dt) * (particleb->positioniSPprime - particle->positioniSPprime);
													particleb->GradSBjrho_1 = particleb->GradSBjrho_1 + particleb->rhoSPprime * particle->mass / particle->rhoSPprime * (particle->FluxSBjrho_1 / particle->rhoSPprime - particleb->FluxSBjrho_1 / particleb->rhoSPprime) * gW(dist_1, dx, simPlat_dt) * (particleb->positionjSPprime - particle->positionjSPprime);
													particleb->GradSBkrho_1 = particleb->GradSBkrho_1 + particleb->rhoSPprime * particle->mass / particle->rhoSPprime * (particle->FluxSBkrho_1 / particle->rhoSPprime - particleb->FluxSBkrho_1 / particleb->rhoSPprime) * gW(dist_1, dx, simPlat_dt) * (particleb->positionkSPprime - particle->positionkSPprime);
												}
											}
										}				
									}
								}
							}
						}
					}
				}
			}
		}
	}
	//Fill ghosts and periodical boundaries, but no other boundaries
	d_bdry_sched_advance[0]->fillData(current_time, false);
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch > patch = *p_it;
		std::shared_ptr< pdat::IndexData<Particles, pdat::CellGeometry > > problemVariable(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles, pdat::CellGeometry >, hier::PatchData>(patch->getPatchData(d_problemVariable_id)));
		int* seg1 = ((pdat::CellData<int> *) patch->getPatchData(d_seg1_id).get())->getPointer();
		int* segL1 = ((pdat::CellData<int> *) patch->getPatchData(d_segL1_id).get())->getPointer();
		int* seg3 = ((pdat::CellData<int> *) patch->getPatchData(d_seg3_id).get())->getPointer();
		int* segL3 = ((pdat::CellData<int> *) patch->getPatchData(d_segL3_id).get())->getPointer();
		int* segxLower = ((pdat::CellData<int> *) patch->getPatchData(d_segxLower_id).get())->getPointer();
		int* segLxLower = ((pdat::CellData<int> *) patch->getPatchData(d_segLxLower_id).get())->getPointer();
		int* segxUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segxUpper_id).get())->getPointer();
		int* segLxUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segLxUpper_id).get())->getPointer();
		int* segyLower = ((pdat::CellData<int> *) patch->getPatchData(d_segyLower_id).get())->getPointer();
		int* segLyLower = ((pdat::CellData<int> *) patch->getPatchData(d_segLyLower_id).get())->getPointer();
		int* segyUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segyUpper_id).get())->getPointer();
		int* segLyUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segLyUpper_id).get())->getPointer();
		int* segzLower = ((pdat::CellData<int> *) patch->getPatchData(d_segzLower_id).get())->getPointer();
		int* segLzLower = ((pdat::CellData<int> *) patch->getPatchData(d_segLzLower_id).get())->getPointer();
		int* segzUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segzUpper_id).get())->getPointer();
		int* segLzUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segLzUpper_id).get())->getPointer();
		int* region = ((pdat::CellData<int> *) patch->getPatchData(d_region_id).get())->getPointer();
		const hier::Index boxfirst1 = patch->getBox().lower();
		const hier::Index boxlast1  = patch->getBox().upper();
		const hier::Index boxfirst = problemVariable->getGhostBox().lower();
		const hier::Index boxlast  = problemVariable->getGhostBox().upper();
		int ilast = boxlast1(0)-boxfirst1(0) + 1 + 2 * d_ghost_width;
		int jlast = boxlast1(1)-boxfirst1(1) + 1 + 2 * d_ghost_width;
		int klast = boxlast1(2)-boxfirst1(2) + 1 + 2 * d_ghost_width;
	
		for (int index2 = boxfirst(2); index2 <= boxlast(2); index2++) {
			for (int index1 = boxfirst(1); index1 <= boxlast(1); index1++) {
				for (int index0 = boxfirst(0); index0 <= boxlast(0); index0++) {
					hier::Index idx(index0, index1, index2);
					//Correct the position if there is any periodical boundary
					Particles* part = problemVariable->getItem(idx);
					checkPosition(patch, index0, index1, index2, part);
				}
			}
		}
	}
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch > patch = *p_it;
		std::shared_ptr< pdat::IndexData<Particles, pdat::CellGeometry > > problemVariable(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles, pdat::CellGeometry >, hier::PatchData>(patch->getPatchData(d_problemVariable_id)));
		int* seg1 = ((pdat::CellData<int> *) patch->getPatchData(d_seg1_id).get())->getPointer();
		int* segL1 = ((pdat::CellData<int> *) patch->getPatchData(d_segL1_id).get())->getPointer();
		int* seg3 = ((pdat::CellData<int> *) patch->getPatchData(d_seg3_id).get())->getPointer();
		int* segL3 = ((pdat::CellData<int> *) patch->getPatchData(d_segL3_id).get())->getPointer();
		int* segxLower = ((pdat::CellData<int> *) patch->getPatchData(d_segxLower_id).get())->getPointer();
		int* segLxLower = ((pdat::CellData<int> *) patch->getPatchData(d_segLxLower_id).get())->getPointer();
		int* segxUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segxUpper_id).get())->getPointer();
		int* segLxUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segLxUpper_id).get())->getPointer();
		int* segyLower = ((pdat::CellData<int> *) patch->getPatchData(d_segyLower_id).get())->getPointer();
		int* segLyLower = ((pdat::CellData<int> *) patch->getPatchData(d_segLyLower_id).get())->getPointer();
		int* segyUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segyUpper_id).get())->getPointer();
		int* segLyUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segLyUpper_id).get())->getPointer();
		int* segzLower = ((pdat::CellData<int> *) patch->getPatchData(d_segzLower_id).get())->getPointer();
		int* segLzLower = ((pdat::CellData<int> *) patch->getPatchData(d_segLzLower_id).get())->getPointer();
		int* segzUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segzUpper_id).get())->getPointer();
		int* segLzUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segLzUpper_id).get())->getPointer();
		int* region = ((pdat::CellData<int> *) patch->getPatchData(d_region_id).get())->getPointer();
		const hier::Index boxfirst1 = patch->getBox().lower();
		const hier::Index boxlast1  = patch->getBox().upper();
		const hier::Index boxfirst = problemVariable->getGhostBox().lower();
		const hier::Index boxlast  = problemVariable->getGhostBox().upper();
		int ilast = boxlast1(0)-boxfirst1(0) + 1 + 2 * d_ghost_width;
		int jlast = boxlast1(1)-boxfirst1(1) + 1 + 2 * d_ghost_width;
		int klast = boxlast1(2)-boxfirst1(2) + 1 + 2 * d_ghost_width;
	
		double fluxAccSBmx_3;
		double fluxAccSBmy_3;
		double fluxAccSBmz_3;
		double fluxAccSBe_3;
		double fluxAccSBrho_3;
		double fluxAccSBmx_1;
		double fluxAccSBmy_1;
		double fluxAccSBmz_1;
		double fluxAccSBe_1;
		double fluxAccSBrho_1;
		for (int index2 = boxfirst(2); index2 <= boxlast(2); index2++) {
			for (int index1 = boxfirst(1); index1 <= boxlast(1); index1++) {
				for (int index0 = boxfirst(0); index0 <= boxlast(0); index0++) {
					hier::Index idx(index0, index1, index2);
					Particles* part = problemVariable->getItem(idx);
					for (int pit = part->getNumberOfParticles() - 1; pit >= 0; pit--) {
						Particle* particle = part->getParticle(pit);
						if (vector(seg3, index0 - boxfirst(0), index1 - boxfirst(1), index2 - boxfirst(2)) == 1) {
							fluxAccSBmx_3 = (particle->rhoSPprime * particle->fxSPprime) / particle->rhoSPprime;
							fluxAccSBmy_3 = (particle->rhoSPprime * particle->fySPprime) / particle->rhoSPprime;
							fluxAccSBmz_3 = (particle->rhoSPprime * particle->fzSPprime) / particle->rhoSPprime;
							fluxAccSBe_3 = (particle->fxSPprime * particle->mxSPprime + particle->fySPprime * particle->mySPprime + particle->fzSPprime * particle->mzSPprime) / particle->rhoSPprime;
							fluxAccSBrho_3 = 0.0;
							fluxAccSBrho_3 = fluxAccSBrho_3 - particle->GradSBirho_3;
							fluxAccSBrho_3 = fluxAccSBrho_3 - particle->GradSBjrho_3;
							fluxAccSBrho_3 = fluxAccSBrho_3 - particle->GradSBkrho_3;
							fluxAccSBmx_3 = fluxAccSBmx_3 - particle->GradSBimx_3;
							fluxAccSBmx_3 = fluxAccSBmx_3 - particle->GradSBjmx_3;
							fluxAccSBmx_3 = fluxAccSBmx_3 - particle->GradSBkmx_3;
							fluxAccSBmy_3 = fluxAccSBmy_3 - particle->GradSBimy_3;
							fluxAccSBmy_3 = fluxAccSBmy_3 - particle->GradSBjmy_3;
							fluxAccSBmy_3 = fluxAccSBmy_3 - particle->GradSBkmy_3;
							fluxAccSBmz_3 = fluxAccSBmz_3 - particle->GradSBimz_3;
							fluxAccSBmz_3 = fluxAccSBmz_3 - particle->GradSBjmz_3;
							fluxAccSBmz_3 = fluxAccSBmz_3 - particle->GradSBkmz_3;
							fluxAccSBe_3 = fluxAccSBe_3 - particle->GradSBie_3;
							fluxAccSBe_3 = fluxAccSBe_3 - particle->GradSBje_3;
							fluxAccSBe_3 = fluxAccSBe_3 - particle->GradSBke_3;
							particle->mxSPSprime_3 = 2.0 * (particle->mx_p / particle->rho_p + simPlat_dt * 0.5 * fluxAccSBmx_3) - particle->mx_p / particle->rho_p;
							particle->mySPSprime_3 = 2.0 * (particle->my_p / particle->rho_p + simPlat_dt * 0.5 * fluxAccSBmy_3) - particle->my_p / particle->rho_p;
							particle->mzSPSprime_3 = 2.0 * (particle->mz_p / particle->rho_p + simPlat_dt * 0.5 * fluxAccSBmz_3) - particle->mz_p / particle->rho_p;
							particle->eSPSprime_3 = 2.0 * (particle->e_p / particle->rho_p + simPlat_dt * 0.5 * fluxAccSBe_3) - particle->e_p / particle->rho_p;
							if (particle->region == 3 || particle->newRegion == 3) {
								particle->rho = 2.0 * (particle->rho_p + simPlat_dt * 0.5 * fluxAccSBrho_3) - particle->rho_p;
								particle->mx = particle->mxSPSprime_3 * particle->rho;
								particle->my = particle->mySPSprime_3 * particle->rho;
								particle->mz = particle->mzSPSprime_3 * particle->rho;
								particle->e = particle->eSPSprime_3 * particle->rho;
								particle->positioni = 2.0 * (particle->positioni_p + simPlat_dt * 0.5 * particle->FluxSBirho_3 / particle->rho_p) - particle->positioni_p;
								particle->positionj = 2.0 * (particle->positionj_p + simPlat_dt * 0.5 * particle->FluxSBjrho_3 / particle->rho_p) - particle->positionj_p;
								particle->positionk = 2.0 * (particle->positionk_p + simPlat_dt * 0.5 * particle->FluxSBkrho_3 / particle->rho_p) - particle->positionk_p;
							}
						}
						if (vector(seg1, index0 - boxfirst(0), index1 - boxfirst(1), index2 - boxfirst(2)) == 1) {
							fluxAccSBmx_1 = (particle->rhoSPprime * particle->fxSPprime) / particle->rhoSPprime;
							fluxAccSBmy_1 = (particle->rhoSPprime * particle->fySPprime) / particle->rhoSPprime;
							fluxAccSBmz_1 = (particle->rhoSPprime * particle->fzSPprime) / particle->rhoSPprime;
							fluxAccSBe_1 = (particle->fxSPprime * particle->mxSPprime + particle->fySPprime * particle->mySPprime + particle->fzSPprime * particle->mzSPprime) / particle->rhoSPprime;
							fluxAccSBrho_1 = 0.0;
							fluxAccSBrho_1 = fluxAccSBrho_1 - particle->GradSBirho_1;
							fluxAccSBrho_1 = fluxAccSBrho_1 - particle->GradSBjrho_1;
							fluxAccSBrho_1 = fluxAccSBrho_1 - particle->GradSBkrho_1;
							fluxAccSBmx_1 = fluxAccSBmx_1 - particle->GradSBimx_1;
							fluxAccSBmx_1 = fluxAccSBmx_1 - particle->GradSBjmx_1;
							fluxAccSBmx_1 = fluxAccSBmx_1 - particle->GradSBkmx_1;
							fluxAccSBmy_1 = fluxAccSBmy_1 - particle->GradSBimy_1;
							fluxAccSBmy_1 = fluxAccSBmy_1 - particle->GradSBjmy_1;
							fluxAccSBmy_1 = fluxAccSBmy_1 - particle->GradSBkmy_1;
							fluxAccSBmz_1 = fluxAccSBmz_1 - particle->GradSBimz_1;
							fluxAccSBmz_1 = fluxAccSBmz_1 - particle->GradSBjmz_1;
							fluxAccSBmz_1 = fluxAccSBmz_1 - particle->GradSBkmz_1;
							fluxAccSBe_1 = fluxAccSBe_1 - particle->GradSBie_1;
							fluxAccSBe_1 = fluxAccSBe_1 - particle->GradSBje_1;
							fluxAccSBe_1 = fluxAccSBe_1 - particle->GradSBke_1;
							particle->mxSPSprime_1 = 2.0 * (particle->mx_p / particle->rho_p + simPlat_dt * 0.5 * fluxAccSBmx_1) - particle->mx_p / particle->rho_p;
							particle->mySPSprime_1 = 2.0 * (particle->my_p / particle->rho_p + simPlat_dt * 0.5 * fluxAccSBmy_1) - particle->my_p / particle->rho_p;
							particle->mzSPSprime_1 = 2.0 * (particle->mz_p / particle->rho_p + simPlat_dt * 0.5 * fluxAccSBmz_1) - particle->mz_p / particle->rho_p;
							particle->eSPSprime_1 = 2.0 * (particle->e_p / particle->rho_p + simPlat_dt * 0.5 * fluxAccSBe_1) - particle->e_p / particle->rho_p;
							if (particle->region == 1 || particle->newRegion == 1) {
								particle->rho = 2.0 * (particle->rho_p + simPlat_dt * 0.5 * fluxAccSBrho_1) - particle->rho_p;
								particle->mx = particle->mxSPSprime_1 * particle->rho;
								particle->my = particle->mySPSprime_1 * particle->rho;
								particle->mz = particle->mzSPSprime_1 * particle->rho;
								particle->e = particle->eSPSprime_1 * particle->rho;
								particle->positioni = 2.0 * (particle->positioni_p + simPlat_dt * 0.5 * particle->FluxSBirho_1 / particle->rho_p) - particle->positioni_p;
								particle->positionj = 2.0 * (particle->positionj_p + simPlat_dt * 0.5 * particle->FluxSBjrho_1 / particle->rho_p) - particle->positionj_p;
								particle->positionk = 2.0 * (particle->positionk_p + simPlat_dt * 0.5 * particle->FluxSBkrho_1 / particle->rho_p) - particle->positionk_p;
							}
						}
					}
				}
			}
		}
	}
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch > patch = *p_it;
		std::shared_ptr< pdat::IndexData<Particles, pdat::CellGeometry > > problemVariable(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles, pdat::CellGeometry >, hier::PatchData>(patch->getPatchData(d_problemVariable_id)));
		int* seg1 = ((pdat::CellData<int> *) patch->getPatchData(d_seg1_id).get())->getPointer();
		int* segL1 = ((pdat::CellData<int> *) patch->getPatchData(d_segL1_id).get())->getPointer();
		int* seg3 = ((pdat::CellData<int> *) patch->getPatchData(d_seg3_id).get())->getPointer();
		int* segL3 = ((pdat::CellData<int> *) patch->getPatchData(d_segL3_id).get())->getPointer();
		int* segxLower = ((pdat::CellData<int> *) patch->getPatchData(d_segxLower_id).get())->getPointer();
		int* segLxLower = ((pdat::CellData<int> *) patch->getPatchData(d_segLxLower_id).get())->getPointer();
		int* segxUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segxUpper_id).get())->getPointer();
		int* segLxUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segLxUpper_id).get())->getPointer();
		int* segyLower = ((pdat::CellData<int> *) patch->getPatchData(d_segyLower_id).get())->getPointer();
		int* segLyLower = ((pdat::CellData<int> *) patch->getPatchData(d_segLyLower_id).get())->getPointer();
		int* segyUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segyUpper_id).get())->getPointer();
		int* segLyUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segLyUpper_id).get())->getPointer();
		int* segzLower = ((pdat::CellData<int> *) patch->getPatchData(d_segzLower_id).get())->getPointer();
		int* segLzLower = ((pdat::CellData<int> *) patch->getPatchData(d_segLzLower_id).get())->getPointer();
		int* segzUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segzUpper_id).get())->getPointer();
		int* segLzUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segLzUpper_id).get())->getPointer();
		int* region = ((pdat::CellData<int> *) patch->getPatchData(d_region_id).get())->getPointer();
		const hier::Index boxfirst1 = patch->getBox().lower();
		const hier::Index boxlast1  = patch->getBox().upper();
		const hier::Index boxfirst = problemVariable->getGhostBox().lower();
		const hier::Index boxlast  = problemVariable->getGhostBox().upper();
		int ilast = boxlast1(0)-boxfirst1(0) + 1 + 2 * d_ghost_width;
		int jlast = boxlast1(1)-boxfirst1(1) + 1 + 2 * d_ghost_width;
		int klast = boxlast1(2)-boxfirst1(2) + 1 + 2 * d_ghost_width;
	
		for (int index2 = boxfirst(2); index2 <= boxlast(2); index2++) {
			for (int index1 = boxfirst(1); index1 <= boxlast(1); index1++) {
				for (int index0 = boxfirst(0); index0 <= boxlast(0); index0++) {
					hier::Index idx(index0, index1, index2);
					Particles* part = problemVariable->getItem(idx);
					for (int pit = part->getNumberOfParticles() - 1; pit >= 0; pit--) {
						Particle* particle = part->getParticle(pit);
						if (particle->region == 3 || particle->newRegion == 3) {
							moveParticles(patch, problemVariable, part, particle, index0, index1, index2, particle->positioni_p, particle->positionj_p, particle->positionk_p, particle->positioni, particle->positionj, particle->positionk, simPlat_dt, current_time, pit);
							continue;
						}
						if (particle->region == 1 || particle->newRegion == 1) {
							moveParticles(patch, problemVariable, part, particle, index0, index1, index2, particle->positioni_p, particle->positionj_p, particle->positionk_p, particle->positioni, particle->positionj, particle->positionk, simPlat_dt, current_time, pit);
							continue;
						}
						if (particle->region == 2 || particle->newRegion == 2) {
							moveParticles(patch, problemVariable, part, particle, index0, index1, index2, particle->positioni_p, particle->positionj_p, particle->positionk_p, particle->positioni, particle->positionj, particle->positionk, simPlat_dt, current_time, pit);
							continue;
						}
					}
				}
			}
		}
	}
	//Fill ghosts and periodical boundaries, but no other boundaries
	d_bdry_sched_advance[0]->fillData(current_time, false);
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch > patch = *p_it;
		std::shared_ptr< pdat::IndexData<Particles, pdat::CellGeometry > > problemVariable(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles, pdat::CellGeometry >, hier::PatchData>(patch->getPatchData(d_problemVariable_id)));
		int* seg1 = ((pdat::CellData<int> *) patch->getPatchData(d_seg1_id).get())->getPointer();
		int* segL1 = ((pdat::CellData<int> *) patch->getPatchData(d_segL1_id).get())->getPointer();
		int* seg3 = ((pdat::CellData<int> *) patch->getPatchData(d_seg3_id).get())->getPointer();
		int* segL3 = ((pdat::CellData<int> *) patch->getPatchData(d_segL3_id).get())->getPointer();
		int* segxLower = ((pdat::CellData<int> *) patch->getPatchData(d_segxLower_id).get())->getPointer();
		int* segLxLower = ((pdat::CellData<int> *) patch->getPatchData(d_segLxLower_id).get())->getPointer();
		int* segxUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segxUpper_id).get())->getPointer();
		int* segLxUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segLxUpper_id).get())->getPointer();
		int* segyLower = ((pdat::CellData<int> *) patch->getPatchData(d_segyLower_id).get())->getPointer();
		int* segLyLower = ((pdat::CellData<int> *) patch->getPatchData(d_segLyLower_id).get())->getPointer();
		int* segyUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segyUpper_id).get())->getPointer();
		int* segLyUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segLyUpper_id).get())->getPointer();
		int* segzLower = ((pdat::CellData<int> *) patch->getPatchData(d_segzLower_id).get())->getPointer();
		int* segLzLower = ((pdat::CellData<int> *) patch->getPatchData(d_segLzLower_id).get())->getPointer();
		int* segzUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segzUpper_id).get())->getPointer();
		int* segLzUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segLzUpper_id).get())->getPointer();
		int* region = ((pdat::CellData<int> *) patch->getPatchData(d_region_id).get())->getPointer();
		const hier::Index boxfirst1 = patch->getBox().lower();
		const hier::Index boxlast1  = patch->getBox().upper();
		const hier::Index boxfirst = problemVariable->getGhostBox().lower();
		const hier::Index boxlast  = problemVariable->getGhostBox().upper();
		int ilast = boxlast1(0)-boxfirst1(0) + 1 + 2 * d_ghost_width;
		int jlast = boxlast1(1)-boxfirst1(1) + 1 + 2 * d_ghost_width;
		int klast = boxlast1(2)-boxfirst1(2) + 1 + 2 * d_ghost_width;
	
		for (int index2 = boxfirst(2); index2 <= boxlast(2); index2++) {
			for (int index1 = boxfirst(1); index1 <= boxlast(1); index1++) {
				for (int index0 = boxfirst(0); index0 <= boxlast(0); index0++) {
					hier::Index idx(index0, index1, index2);
					//Correct the position if there is any periodical boundary
					Particles* part = problemVariable->getItem(idx);
					checkPosition(patch, index0, index1, index2, part);
				}
			}
		}
	}
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch > patch = *p_it;
		std::shared_ptr< pdat::IndexData<Particles, pdat::CellGeometry > > problemVariable(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles, pdat::CellGeometry >, hier::PatchData>(patch->getPatchData(d_problemVariable_id)));
		int* seg1 = ((pdat::CellData<int> *) patch->getPatchData(d_seg1_id).get())->getPointer();
		int* segL1 = ((pdat::CellData<int> *) patch->getPatchData(d_segL1_id).get())->getPointer();
		int* seg3 = ((pdat::CellData<int> *) patch->getPatchData(d_seg3_id).get())->getPointer();
		int* segL3 = ((pdat::CellData<int> *) patch->getPatchData(d_segL3_id).get())->getPointer();
		int* segxLower = ((pdat::CellData<int> *) patch->getPatchData(d_segxLower_id).get())->getPointer();
		int* segLxLower = ((pdat::CellData<int> *) patch->getPatchData(d_segLxLower_id).get())->getPointer();
		int* segxUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segxUpper_id).get())->getPointer();
		int* segLxUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segLxUpper_id).get())->getPointer();
		int* segyLower = ((pdat::CellData<int> *) patch->getPatchData(d_segyLower_id).get())->getPointer();
		int* segLyLower = ((pdat::CellData<int> *) patch->getPatchData(d_segLyLower_id).get())->getPointer();
		int* segyUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segyUpper_id).get())->getPointer();
		int* segLyUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segLyUpper_id).get())->getPointer();
		int* segzLower = ((pdat::CellData<int> *) patch->getPatchData(d_segzLower_id).get())->getPointer();
		int* segLzLower = ((pdat::CellData<int> *) patch->getPatchData(d_segLzLower_id).get())->getPointer();
		int* segzUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segzUpper_id).get())->getPointer();
		int* segLzUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segLzUpper_id).get())->getPointer();
		int* region = ((pdat::CellData<int> *) patch->getPatchData(d_region_id).get())->getPointer();
		const hier::Index boxfirst1 = patch->getBox().lower();
		const hier::Index boxlast1  = patch->getBox().upper();
		const hier::Index boxfirst = problemVariable->getGhostBox().lower();
		const hier::Index boxlast  = problemVariable->getGhostBox().upper();
		int ilast = boxlast1(0)-boxfirst1(0) + 1 + 2 * d_ghost_width;
		int jlast = boxlast1(1)-boxfirst1(1) + 1 + 2 * d_ghost_width;
		int klast = boxlast1(2)-boxfirst1(2) + 1 + 2 * d_ghost_width;
	
		for (int index2 = boxfirst(2); index2 <= boxlast(2); index2++) {
			for (int index1 = boxfirst(1); index1 <= boxlast(1); index1++) {
				for (int index0 = boxfirst(0); index0 <= boxlast(0); index0++) {
					hier::Index idx(index0, index1, index2);
					Particles* part = problemVariable->getItem(idx);
					for (int pit = part->getNumberOfParticles() - 1; pit >= 0; pit--) {
						Particle* particle = part->getParticle(pit);
						if (vector(seg3, index0 - boxfirst(0), index1 - boxfirst(1), index2 - boxfirst(2)) == 1) {
							if (particle->region == 3 || particle->newRegion == 3) {
								particle->fx = pfx;
								particle->fy = pfy;
								particle->fz = pfz;
								particle->p = (gamma - 1.0) * (particle->e - particle->rho * (((particle->mx / particle->rho) * (particle->mx / particle->rho)) / 2.0 + ((particle->my / particle->rho) * (particle->my / particle->rho)) / 2.0 + ((particle->mz / particle->rho) * (particle->mz / particle->rho)) / 2.0));
							}
						}
						if (vector(seg1, index0 - boxfirst(0), index1 - boxfirst(1), index2 - boxfirst(2)) == 1) {
							if (particle->region == 1 || particle->newRegion == 1) {
								particle->fx = pfx;
								particle->fy = pfy;
								particle->fz = pfz;
								particle->p = (gamma - 1.0) * (particle->e - particle->rho * (((particle->mx / particle->rho) * (particle->mx / particle->rho)) / 2.0 + ((particle->my / particle->rho) * (particle->my / particle->rho)) / 2.0 + ((particle->mz / particle->rho) * (particle->mz / particle->rho)) / 2.0));
							}
						}
					}
				}
			}
		}
	}
	//Delete invalid particles and particles inside reflection boundaries
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch > patch = *p_it;
		std::shared_ptr< pdat::IndexData<Particles, pdat::CellGeometry > > problemVariable(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles, pdat::CellGeometry >, hier::PatchData>(patch->getPatchData(d_problemVariable_id)));
		int* seg1 = ((pdat::CellData<int> *) patch->getPatchData(d_seg1_id).get())->getPointer();
		int* segL1 = ((pdat::CellData<int> *) patch->getPatchData(d_segL1_id).get())->getPointer();
		int* seg3 = ((pdat::CellData<int> *) patch->getPatchData(d_seg3_id).get())->getPointer();
		int* segL3 = ((pdat::CellData<int> *) patch->getPatchData(d_segL3_id).get())->getPointer();
		int* segxLower = ((pdat::CellData<int> *) patch->getPatchData(d_segxLower_id).get())->getPointer();
		int* segLxLower = ((pdat::CellData<int> *) patch->getPatchData(d_segLxLower_id).get())->getPointer();
		int* segxUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segxUpper_id).get())->getPointer();
		int* segLxUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segLxUpper_id).get())->getPointer();
		int* segyLower = ((pdat::CellData<int> *) patch->getPatchData(d_segyLower_id).get())->getPointer();
		int* segLyLower = ((pdat::CellData<int> *) patch->getPatchData(d_segLyLower_id).get())->getPointer();
		int* segyUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segyUpper_id).get())->getPointer();
		int* segLyUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segLyUpper_id).get())->getPointer();
		int* segzLower = ((pdat::CellData<int> *) patch->getPatchData(d_segzLower_id).get())->getPointer();
		int* segLzLower = ((pdat::CellData<int> *) patch->getPatchData(d_segLzLower_id).get())->getPointer();
		int* segzUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segzUpper_id).get())->getPointer();
		int* segLzUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segLzUpper_id).get())->getPointer();
		int* region = ((pdat::CellData<int> *) patch->getPatchData(d_region_id).get())->getPointer();
		const hier::Index boxfirst1 = patch->getBox().lower();
		const hier::Index boxlast1  = patch->getBox().upper();
		const hier::Index boxfirst = problemVariable->getGhostBox().lower();
		const hier::Index boxlast  = problemVariable->getGhostBox().upper();
		int ilast = boxlast1(0)-boxfirst1(0) + 1 + 2 * d_ghost_width;
		int jlast = boxlast1(1)-boxfirst1(1) + 1 + 2 * d_ghost_width;
		int klast = boxlast1(2)-boxfirst1(2) + 1 + 2 * d_ghost_width;
	
		for (int index2 = boxfirst(2); index2 <= boxlast(2); index2++) {
			for (int index1 = boxfirst(1); index1 <= boxlast(1); index1++) {
				for (int index0 = boxfirst(0); index0 <= boxlast(0); index0++) {
					hier::Index idx(index0, index1, index2);
					Particles* part = problemVariable->getItem(idx);
					for (int pit = part->getNumberOfParticles() - 1; pit >= 0; pit--) {
						Particle* particle = part->getParticle(pit);
						if (particle->region == 3 || particle->region == 4) {
							if (Equals(particle->rho,0)) {
								part->deleteParticle(*particle);
							}
						}
						else if (particle->region == 1 || particle->region == 2) {
							if (Equals(particle->rho,0)) {
								part->deleteParticle(*particle);
							}
						}
					}
				}
			}
		}
	}
	

	t_step->stop();


	return simPlat_dt;
}

/*
 * Checks the finalization conditions              
 */
bool Problem::checkFinalization(const double current_time, const double simPlat_dt)
{
	if (greaterEq(current_time, 2.0)) { 
		return true;
	}
	return false;
	
	

}




/*
 *  Cell tagging routine - tag cells that require refinement based on a provided condition. 
 */
void Problem::applyGradientDetector(
   const std::shared_ptr< hier::PatchHierarchy >& hierarchy, 
   const int level_number,
   const double time, 
   const int tag_index,
   const bool initial_time,
   const bool uses_richardson_extrapolation_too) 
{

}

/*
 * Move the particles if the problem have any
 */
void Problem::moveParticles(const std::shared_ptr<hier::Patch > patch, std::shared_ptr< pdat::IndexData<Particles, pdat::CellGeometry > > problemVariable, Particles* part, Particle* particle, const int index0, const int index1, const int index2, const double oldPosi, const double oldPosj, const double oldPosk, const double newPosi, const double newPosj, const double newPosk, const double simPlat_dt, const double current_time, int pit)
{
	int* seg1 = ((pdat::CellData<int> *) patch->getPatchData(d_seg1_id).get())->getPointer();
	int* segL1 = ((pdat::CellData<int> *) patch->getPatchData(d_segL1_id).get())->getPointer();
	int* seg3 = ((pdat::CellData<int> *) patch->getPatchData(d_seg3_id).get())->getPointer();
	int* segL3 = ((pdat::CellData<int> *) patch->getPatchData(d_segL3_id).get())->getPointer();
	int* segxLower = ((pdat::CellData<int> *) patch->getPatchData(d_segxLower_id).get())->getPointer();
	int* segLxLower = ((pdat::CellData<int> *) patch->getPatchData(d_segLxLower_id).get())->getPointer();
	int* segxUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segxUpper_id).get())->getPointer();
	int* segLxUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segLxUpper_id).get())->getPointer();
	int* segyLower = ((pdat::CellData<int> *) patch->getPatchData(d_segyLower_id).get())->getPointer();
	int* segLyLower = ((pdat::CellData<int> *) patch->getPatchData(d_segLyLower_id).get())->getPointer();
	int* segyUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segyUpper_id).get())->getPointer();
	int* segLyUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segLyUpper_id).get())->getPointer();
	int* segzLower = ((pdat::CellData<int> *) patch->getPatchData(d_segzLower_id).get())->getPointer();
	int* segLzLower = ((pdat::CellData<int> *) patch->getPatchData(d_segLzLower_id).get())->getPointer();
	int* segzUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segzUpper_id).get())->getPointer();
	int* segLzUpper = ((pdat::CellData<int> *) patch->getPatchData(d_segLzUpper_id).get())->getPointer();
	int* region = ((pdat::CellData<int> *) patch->getPatchData(d_region_id).get())->getPointer();

	const hier::Index boxfirst1 = patch->getBox().lower();
	const hier::Index boxlast1  = patch->getBox().upper();
	const hier::Index boxfirst = problemVariable->getGhostBox().lower();
	const hier::Index boxlast  = problemVariable->getGhostBox().upper();

	//Get delta spaces into an array. dx, dy, dz.
	std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
	int xlower = patch_geom->getXLower()[0];
	int xupper = patch_geom->getXUpper()[0];
	int ylower = patch_geom->getXLower()[1];
	int yupper = patch_geom->getXUpper()[1];
	int zlower = patch_geom->getXLower()[2];
	int zupper = patch_geom->getXUpper()[2];
	int ilast = boxlast1(0)-boxfirst1(0) + 1 + 2 * d_ghost_width;
	int jlast = boxlast1(1)-boxfirst1(1) + 1 + 2 * d_ghost_width;
	int klast = boxlast1(2)-boxfirst1(2) + 1 + 2 * d_ghost_width;

	double position[3];
	double newPosition[3];

	hier::Index idx(index0, index1, index2);
	position[0] = (oldPosi - xGlower)/dx[0];
	position[1] = (oldPosj - yGlower)/dx[1];
	position[2] = (oldPosk - zGlower)/dx[2];

	newPosition[0] = (newPosi - xGlower)/dx[0];
	int newIndex0 = index0;
	newPosition[1] = (newPosj - yGlower)/dx[1];
	int newIndex1 = index1;
	newPosition[2] = (newPosk - zGlower)/dx[2];
	int newIndex2 = index2;

	bool out = false;
	if (greaterEq(newPosition[0], boxlast(0)+1) || lessThan(newPosition[0], boxfirst(0))) {
		out = true;
	} else {
		if (lessThan(newPosition[0], index0) || greaterEq(newPosition[0], index0+1)) {
			newIndex0 = floor(newPosition[0]);
		}
	}
	if (greaterEq(newPosition[1], boxlast(1)+1) || lessThan(newPosition[1], boxfirst(1))) {
		out = true;
	} else {
		if (lessThan(newPosition[1], index1) || greaterEq(newPosition[1], index1+1)) {
			newIndex1 = floor(newPosition[1]);
		}
	}
	if (greaterEq(newPosition[2], boxlast(2)+1) || lessThan(newPosition[2], boxfirst(2))) {
		out = true;
	} else {
		if (lessThan(newPosition[2], index2) || greaterEq(newPosition[2], index2+1)) {
			newIndex2 = floor(newPosition[2]);
		}
	}
	if (!out) {
		hier::Index newIdx(newIndex0, newIndex1, newIndex2);
		if (!(newIdx == idx)) {
			bool deleted = false;
			if (!deleted) {
				//Need to create copy of the particle. Erase an item from a vector calls the destructor of the object
				Particle* newParticle = new Particle(*particle);
				part->deleteParticle(*particle);
				Particles* destPart = problemVariable->getItem(newIdx);
				destPart->addParticle(*newParticle);
				//Recalculation of region's cell influence
				//New cell calculation
				if (newParticle->region == 3 || newParticle->region == 4) {
					if (vector(segL3, newIndex0 - boxfirst(0), newIndex1 - boxfirst(1), newIndex2 - boxfirst(2)) == 0) {
						vector(segL3, newIndex0 - boxfirst(0), newIndex1 - boxfirst(1), newIndex2 - boxfirst(2)) = 1;
						int miniIndex = newIndex0 - ceil((2 * influenceRadius_0 + particleSeparation_x)/dx[0]);
						int maxiIndex = newIndex0 + ceil((2 * influenceRadius_0 + particleSeparation_x)/dx[0]);
						if (miniIndex < boxfirst(0)) {
							miniIndex = boxfirst(0);
						}
						if (maxiIndex > boxlast(0)) {
							maxiIndex = boxlast(0);
						}
						int minjIndex = newIndex1 - ceil((2 * influenceRadius_0 + particleSeparation_y)/dx[1]);
						int maxjIndex = newIndex1 + ceil((2 * influenceRadius_0 + particleSeparation_y)/dx[1]);
						if (minjIndex < boxfirst(1)) {
							minjIndex = boxfirst(1);
						}
						if (maxjIndex > boxlast(1)) {
							maxjIndex = boxlast(1);
						}
						int minkIndex = newIndex2 - ceil((2 * influenceRadius_0 + particleSeparation_z)/dx[2]);
						int maxkIndex = newIndex2 + ceil((2 * influenceRadius_0 + particleSeparation_z)/dx[2]);
						if (minkIndex < boxfirst(2)) {
							minkIndex = boxfirst(2);
						}
						if (maxkIndex > boxlast(2)) {
							maxkIndex = boxlast(2);
						}
						for(int index2b = minkIndex; index2b <= maxkIndex; index2b++) {
							for(int index1b = minjIndex; index1b <= maxjIndex; index1b++) {
								for(int index0b = miniIndex; index0b <= maxiIndex; index0b++) {
									if (vector(seg3, index0b - boxfirst(0), index1b - boxfirst(1), index2b - boxfirst(2)) == 0) {
										vector(seg3, index0b - boxfirst(0), index1b - boxfirst(1), index2b - boxfirst(2)) = 1;
									}
								}
							}
						}
					}
				}
				if (newParticle->region == 1 || newParticle->region == 2) {
					if (vector(segL1, newIndex0 - boxfirst(0), newIndex1 - boxfirst(1), newIndex2 - boxfirst(2)) == 0) {
						vector(segL1, newIndex0 - boxfirst(0), newIndex1 - boxfirst(1), newIndex2 - boxfirst(2)) = 1;
						int miniIndex = newIndex0 - ceil((2 * influenceRadius_0 + particleSeparation_x)/dx[0]);
						int maxiIndex = newIndex0 + ceil((2 * influenceRadius_0 + particleSeparation_x)/dx[0]);
						if (miniIndex < boxfirst(0)) {
							miniIndex = boxfirst(0);
						}
						if (maxiIndex > boxlast(0)) {
							maxiIndex = boxlast(0);
						}
						int minjIndex = newIndex1 - ceil((2 * influenceRadius_0 + particleSeparation_y)/dx[1]);
						int maxjIndex = newIndex1 + ceil((2 * influenceRadius_0 + particleSeparation_y)/dx[1]);
						if (minjIndex < boxfirst(1)) {
							minjIndex = boxfirst(1);
						}
						if (maxjIndex > boxlast(1)) {
							maxjIndex = boxlast(1);
						}
						int minkIndex = newIndex2 - ceil((2 * influenceRadius_0 + particleSeparation_z)/dx[2]);
						int maxkIndex = newIndex2 + ceil((2 * influenceRadius_0 + particleSeparation_z)/dx[2]);
						if (minkIndex < boxfirst(2)) {
							minkIndex = boxfirst(2);
						}
						if (maxkIndex > boxlast(2)) {
							maxkIndex = boxlast(2);
						}
						for(int index2b = minkIndex; index2b <= maxkIndex; index2b++) {
							for(int index1b = minjIndex; index1b <= maxjIndex; index1b++) {
								for(int index0b = miniIndex; index0b <= maxiIndex; index0b++) {
									if (vector(seg1, index0b - boxfirst(0), index1b - boxfirst(1), index2b - boxfirst(2)) == 0) {
										vector(seg1, index0b - boxfirst(0), index1b - boxfirst(1), index2b - boxfirst(2)) = 1;
									}
								}
							}
						}
					}
				}
				if (newParticle->region == -1) {
					if (vector(segLxLower, newIndex0 - boxfirst(0), newIndex1 - boxfirst(1), newIndex2 - boxfirst(2)) == 0) {
						vector(segLxLower, newIndex0 - boxfirst(0), newIndex1 - boxfirst(1), newIndex2 - boxfirst(2)) = 1;
						int miniIndex = newIndex0 - ceil((2 * influenceRadius_0 + particleSeparation_x)/dx[0]);
						int maxiIndex = newIndex0 + ceil((2 * influenceRadius_0 + particleSeparation_x)/dx[0]);
						if (miniIndex < boxfirst(0)) {
							miniIndex = boxfirst(0);
						}
						if (maxiIndex > boxlast(0)) {
							maxiIndex = boxlast(0);
						}
						int minjIndex = newIndex1 - ceil((2 * influenceRadius_0 + particleSeparation_y)/dx[1]);
						int maxjIndex = newIndex1 + ceil((2 * influenceRadius_0 + particleSeparation_y)/dx[1]);
						if (minjIndex < boxfirst(1)) {
							minjIndex = boxfirst(1);
						}
						if (maxjIndex > boxlast(1)) {
							maxjIndex = boxlast(1);
						}
						int minkIndex = newIndex2 - ceil((2 * influenceRadius_0 + particleSeparation_z)/dx[2]);
						int maxkIndex = newIndex2 + ceil((2 * influenceRadius_0 + particleSeparation_z)/dx[2]);
						if (minkIndex < boxfirst(2)) {
							minkIndex = boxfirst(2);
						}
						if (maxkIndex > boxlast(2)) {
							maxkIndex = boxlast(2);
						}
						for(int index2b = minkIndex; index2b <= maxkIndex; index2b++) {
							for(int index1b = minjIndex; index1b <= maxjIndex; index1b++) {
								for(int index0b = miniIndex; index0b <= maxiIndex; index0b++) {
									if (vector(segxLower, index0b - boxfirst(0), index1b - boxfirst(1), index2b - boxfirst(2)) == 0) {
										vector(segxLower, index0b - boxfirst(0), index1b - boxfirst(1), index2b - boxfirst(2)) = 1;
									}
								}
							}
						}
					}
				}
				if (newParticle->region == -2) {
					if (vector(segLxUpper, newIndex0 - boxfirst(0), newIndex1 - boxfirst(1), newIndex2 - boxfirst(2)) == 0) {
						vector(segLxUpper, newIndex0 - boxfirst(0), newIndex1 - boxfirst(1), newIndex2 - boxfirst(2)) = 1;
						int miniIndex = newIndex0 - ceil((2 * influenceRadius_0 + particleSeparation_x)/dx[0]);
						int maxiIndex = newIndex0 + ceil((2 * influenceRadius_0 + particleSeparation_x)/dx[0]);
						if (miniIndex < boxfirst(0)) {
							miniIndex = boxfirst(0);
						}
						if (maxiIndex > boxlast(0)) {
							maxiIndex = boxlast(0);
						}
						int minjIndex = newIndex1 - ceil((2 * influenceRadius_0 + particleSeparation_y)/dx[1]);
						int maxjIndex = newIndex1 + ceil((2 * influenceRadius_0 + particleSeparation_y)/dx[1]);
						if (minjIndex < boxfirst(1)) {
							minjIndex = boxfirst(1);
						}
						if (maxjIndex > boxlast(1)) {
							maxjIndex = boxlast(1);
						}
						int minkIndex = newIndex2 - ceil((2 * influenceRadius_0 + particleSeparation_z)/dx[2]);
						int maxkIndex = newIndex2 + ceil((2 * influenceRadius_0 + particleSeparation_z)/dx[2]);
						if (minkIndex < boxfirst(2)) {
							minkIndex = boxfirst(2);
						}
						if (maxkIndex > boxlast(2)) {
							maxkIndex = boxlast(2);
						}
						for(int index2b = minkIndex; index2b <= maxkIndex; index2b++) {
							for(int index1b = minjIndex; index1b <= maxjIndex; index1b++) {
								for(int index0b = miniIndex; index0b <= maxiIndex; index0b++) {
									if (vector(segxUpper, index0b - boxfirst(0), index1b - boxfirst(1), index2b - boxfirst(2)) == 0) {
										vector(segxUpper, index0b - boxfirst(0), index1b - boxfirst(1), index2b - boxfirst(2)) = 1;
									}
								}
							}
						}
					}
				}
				if (newParticle->region == -3) {
					if (vector(segLyLower, newIndex0 - boxfirst(0), newIndex1 - boxfirst(1), newIndex2 - boxfirst(2)) == 0) {
						vector(segLyLower, newIndex0 - boxfirst(0), newIndex1 - boxfirst(1), newIndex2 - boxfirst(2)) = 1;
						int miniIndex = newIndex0 - ceil((2 * influenceRadius_0 + particleSeparation_x)/dx[0]);
						int maxiIndex = newIndex0 + ceil((2 * influenceRadius_0 + particleSeparation_x)/dx[0]);
						if (miniIndex < boxfirst(0)) {
							miniIndex = boxfirst(0);
						}
						if (maxiIndex > boxlast(0)) {
							maxiIndex = boxlast(0);
						}
						int minjIndex = newIndex1 - ceil((2 * influenceRadius_0 + particleSeparation_y)/dx[1]);
						int maxjIndex = newIndex1 + ceil((2 * influenceRadius_0 + particleSeparation_y)/dx[1]);
						if (minjIndex < boxfirst(1)) {
							minjIndex = boxfirst(1);
						}
						if (maxjIndex > boxlast(1)) {
							maxjIndex = boxlast(1);
						}
						int minkIndex = newIndex2 - ceil((2 * influenceRadius_0 + particleSeparation_z)/dx[2]);
						int maxkIndex = newIndex2 + ceil((2 * influenceRadius_0 + particleSeparation_z)/dx[2]);
						if (minkIndex < boxfirst(2)) {
							minkIndex = boxfirst(2);
						}
						if (maxkIndex > boxlast(2)) {
							maxkIndex = boxlast(2);
						}
						for(int index2b = minkIndex; index2b <= maxkIndex; index2b++) {
							for(int index1b = minjIndex; index1b <= maxjIndex; index1b++) {
								for(int index0b = miniIndex; index0b <= maxiIndex; index0b++) {
									if (vector(segyLower, index0b - boxfirst(0), index1b - boxfirst(1), index2b - boxfirst(2)) == 0) {
										vector(segyLower, index0b - boxfirst(0), index1b - boxfirst(1), index2b - boxfirst(2)) = 1;
									}
								}
							}
						}
					}
				}
				if (newParticle->region == -4) {
					if (vector(segLyUpper, newIndex0 - boxfirst(0), newIndex1 - boxfirst(1), newIndex2 - boxfirst(2)) == 0) {
						vector(segLyUpper, newIndex0 - boxfirst(0), newIndex1 - boxfirst(1), newIndex2 - boxfirst(2)) = 1;
						int miniIndex = newIndex0 - ceil((2 * influenceRadius_0 + particleSeparation_x)/dx[0]);
						int maxiIndex = newIndex0 + ceil((2 * influenceRadius_0 + particleSeparation_x)/dx[0]);
						if (miniIndex < boxfirst(0)) {
							miniIndex = boxfirst(0);
						}
						if (maxiIndex > boxlast(0)) {
							maxiIndex = boxlast(0);
						}
						int minjIndex = newIndex1 - ceil((2 * influenceRadius_0 + particleSeparation_y)/dx[1]);
						int maxjIndex = newIndex1 + ceil((2 * influenceRadius_0 + particleSeparation_y)/dx[1]);
						if (minjIndex < boxfirst(1)) {
							minjIndex = boxfirst(1);
						}
						if (maxjIndex > boxlast(1)) {
							maxjIndex = boxlast(1);
						}
						int minkIndex = newIndex2 - ceil((2 * influenceRadius_0 + particleSeparation_z)/dx[2]);
						int maxkIndex = newIndex2 + ceil((2 * influenceRadius_0 + particleSeparation_z)/dx[2]);
						if (minkIndex < boxfirst(2)) {
							minkIndex = boxfirst(2);
						}
						if (maxkIndex > boxlast(2)) {
							maxkIndex = boxlast(2);
						}
						for(int index2b = minkIndex; index2b <= maxkIndex; index2b++) {
							for(int index1b = minjIndex; index1b <= maxjIndex; index1b++) {
								for(int index0b = miniIndex; index0b <= maxiIndex; index0b++) {
									if (vector(segyUpper, index0b - boxfirst(0), index1b - boxfirst(1), index2b - boxfirst(2)) == 0) {
										vector(segyUpper, index0b - boxfirst(0), index1b - boxfirst(1), index2b - boxfirst(2)) = 1;
									}
								}
							}
						}
					}
				}
				if (newParticle->region == -5) {
					if (vector(segLzLower, newIndex0 - boxfirst(0), newIndex1 - boxfirst(1), newIndex2 - boxfirst(2)) == 0) {
						vector(segLzLower, newIndex0 - boxfirst(0), newIndex1 - boxfirst(1), newIndex2 - boxfirst(2)) = 1;
						int miniIndex = newIndex0 - ceil((2 * influenceRadius_0 + particleSeparation_x)/dx[0]);
						int maxiIndex = newIndex0 + ceil((2 * influenceRadius_0 + particleSeparation_x)/dx[0]);
						if (miniIndex < boxfirst(0)) {
							miniIndex = boxfirst(0);
						}
						if (maxiIndex > boxlast(0)) {
							maxiIndex = boxlast(0);
						}
						int minjIndex = newIndex1 - ceil((2 * influenceRadius_0 + particleSeparation_y)/dx[1]);
						int maxjIndex = newIndex1 + ceil((2 * influenceRadius_0 + particleSeparation_y)/dx[1]);
						if (minjIndex < boxfirst(1)) {
							minjIndex = boxfirst(1);
						}
						if (maxjIndex > boxlast(1)) {
							maxjIndex = boxlast(1);
						}
						int minkIndex = newIndex2 - ceil((2 * influenceRadius_0 + particleSeparation_z)/dx[2]);
						int maxkIndex = newIndex2 + ceil((2 * influenceRadius_0 + particleSeparation_z)/dx[2]);
						if (minkIndex < boxfirst(2)) {
							minkIndex = boxfirst(2);
						}
						if (maxkIndex > boxlast(2)) {
							maxkIndex = boxlast(2);
						}
						for(int index2b = minkIndex; index2b <= maxkIndex; index2b++) {
							for(int index1b = minjIndex; index1b <= maxjIndex; index1b++) {
								for(int index0b = miniIndex; index0b <= maxiIndex; index0b++) {
									if (vector(segzLower, index0b - boxfirst(0), index1b - boxfirst(1), index2b - boxfirst(2)) == 0) {
										vector(segzLower, index0b - boxfirst(0), index1b - boxfirst(1), index2b - boxfirst(2)) = 1;
									}
								}
							}
						}
					}
				}
				if (newParticle->region == -6) {
					if (vector(segLzUpper, newIndex0 - boxfirst(0), newIndex1 - boxfirst(1), newIndex2 - boxfirst(2)) == 0) {
						vector(segLzUpper, newIndex0 - boxfirst(0), newIndex1 - boxfirst(1), newIndex2 - boxfirst(2)) = 1;
						int miniIndex = newIndex0 - ceil((2 * influenceRadius_0 + particleSeparation_x)/dx[0]);
						int maxiIndex = newIndex0 + ceil((2 * influenceRadius_0 + particleSeparation_x)/dx[0]);
						if (miniIndex < boxfirst(0)) {
							miniIndex = boxfirst(0);
						}
						if (maxiIndex > boxlast(0)) {
							maxiIndex = boxlast(0);
						}
						int minjIndex = newIndex1 - ceil((2 * influenceRadius_0 + particleSeparation_y)/dx[1]);
						int maxjIndex = newIndex1 + ceil((2 * influenceRadius_0 + particleSeparation_y)/dx[1]);
						if (minjIndex < boxfirst(1)) {
							minjIndex = boxfirst(1);
						}
						if (maxjIndex > boxlast(1)) {
							maxjIndex = boxlast(1);
						}
						int minkIndex = newIndex2 - ceil((2 * influenceRadius_0 + particleSeparation_z)/dx[2]);
						int maxkIndex = newIndex2 + ceil((2 * influenceRadius_0 + particleSeparation_z)/dx[2]);
						if (minkIndex < boxfirst(2)) {
							minkIndex = boxfirst(2);
						}
						if (maxkIndex > boxlast(2)) {
							maxkIndex = boxlast(2);
						}
						for(int index2b = minkIndex; index2b <= maxkIndex; index2b++) {
							for(int index1b = minjIndex; index1b <= maxjIndex; index1b++) {
								for(int index0b = miniIndex; index0b <= maxiIndex; index0b++) {
									if (vector(segzUpper, index0b - boxfirst(0), index1b - boxfirst(1), index2b - boxfirst(2)) == 0) {
										vector(segzUpper, index0b - boxfirst(0), index1b - boxfirst(1), index2b - boxfirst(2)) = 1;
									}
								}
							}
						}
					}
				}
				//Old cell calculation
				Particles* part = problemVariable->getItem(idx);
				bool change = true;
				for (int pit = part->getNumberOfParticles() - 1; pit >= 0; pit--) {
					Particle* particle = part->getParticle(pit);
					if ((particle->region == 3 && newParticle->region ==3) || (particle->region == 4 && newParticle->region ==4)) {
						change = false;
					}
					if ((particle->region == 1 && newParticle->region ==1) || (particle->region == 2 && newParticle->region ==2)) {
						change = false;
					}
					if (particle->region == -1 && newParticle->region == -1) {
						change = false;
					}
					if (particle->region == -2 && newParticle->region == -2) {
						change = false;
					}
					if (particle->region == -3 && newParticle->region == -3) {
						change = false;
					}
					if (particle->region == -4 && newParticle->region == -4) {
						change = false;
					}
					if (particle->region == -5 && newParticle->region == -5) {
						change = false;
					}
					if (particle->region == -6 && newParticle->region == -6) {
						change = false;
					}
				}
				if ((newParticle->region == 3 || newParticle->region == 4) && change) {
					vector(segL3, index0 - boxfirst(0), index1 - boxfirst(1), index2 - boxfirst(2)) = 0;
				}
				if ((newParticle->region == 1 || newParticle->region == 2) && change) {
					vector(segL1, index0 - boxfirst(0), index1 - boxfirst(1), index2 - boxfirst(2)) = 0;
				}
				if (newParticle->region == -1 && change) {
					vector(segLxLower, index0 - boxfirst(0), index1 - boxfirst(1), index2 - boxfirst(2)) = 0;
				}
				if (newParticle->region == -2 && change) {
					vector(segLxUpper, index0 - boxfirst(0), index1 - boxfirst(1), index2 - boxfirst(2)) = 0;
				}
				if (newParticle->region == -3 && change) {
					vector(segLyLower, index0 - boxfirst(0), index1 - boxfirst(1), index2 - boxfirst(2)) = 0;
				}
				if (newParticle->region == -4 && change) {
					vector(segLyUpper, index0 - boxfirst(0), index1 - boxfirst(1), index2 - boxfirst(2)) = 0;
				}
				if (newParticle->region == -5 && change) {
					vector(segLzLower, index0 - boxfirst(0), index1 - boxfirst(1), index2 - boxfirst(2)) = 0;
				}
				if (newParticle->region == -6 && change) {
					vector(segLzUpper, index0 - boxfirst(0), index1 - boxfirst(1), index2 - boxfirst(2)) = 0;
				}
				if (change) {
					int miniIndex = index0 - ceil((2 * influenceRadius_0 + particleSeparation_x)/dx[0]);
					int maxiIndex = index0 + ceil((2 * influenceRadius_0 + particleSeparation_x)/dx[0]);
					if (miniIndex < boxfirst(0)) {
						miniIndex = boxfirst(0);
					}
					if (maxiIndex > boxlast(0)) {
						maxiIndex = boxlast(0);
					}
					int minjIndex = index1 - ceil((2 * influenceRadius_0 + particleSeparation_y)/dx[1]);
					int maxjIndex = index1 + ceil((2 * influenceRadius_0 + particleSeparation_y)/dx[1]);
					if (minjIndex < boxfirst(1)) {
						minjIndex = boxfirst(1);
					}
					if (maxjIndex > boxlast(1)) {
						maxjIndex = boxlast(1);
					}
					int minkIndex = index2 - ceil((2 * influenceRadius_0 + particleSeparation_z)/dx[2]);
					int maxkIndex = index2 + ceil((2 * influenceRadius_0 + particleSeparation_z)/dx[2]);
					if (minkIndex < boxfirst(2)) {
						minkIndex = boxfirst(2);
					}
					if (maxkIndex > boxlast(2)) {
						maxkIndex = boxlast(2);
					}
					for(int index2b = minkIndex; index2b <= maxkIndex; index2b++) {
						for(int index1b = minjIndex; index1b <= maxjIndex; index1b++) {
							for(int index0b = miniIndex; index0b <= maxiIndex; index0b++) {
								vector(seg3, index0b - boxfirst(0), index1b - boxfirst(1), index2b - boxfirst(2)) == -1;
								vector(seg1, index0b - boxfirst(0), index1b - boxfirst(1), index2b - boxfirst(2)) == -1;
								vector(segxLower, index0b - boxfirst(0), index1b - boxfirst(1), index2b - boxfirst(2)) == -1;
								vector(segxUpper, index0b - boxfirst(0), index1b - boxfirst(1), index2b - boxfirst(2)) == -1;
								vector(segyLower, index0b - boxfirst(0), index1b - boxfirst(1), index2b - boxfirst(2)) == -1;
								vector(segyUpper, index0b - boxfirst(0), index1b - boxfirst(1), index2b - boxfirst(2)) == -1;
								vector(segzLower, index0b - boxfirst(0), index1b - boxfirst(1), index2b - boxfirst(2)) == -1;
								vector(segzUpper, index0b - boxfirst(0), index1b - boxfirst(1), index2b - boxfirst(2)) == -1;
								int miniIndexb = index0b - ceil((2 * influenceRadius_0 + particleSeparation_x)/dx[0]);
								int maxiIndexb = index0b + ceil((2 * influenceRadius_0 + particleSeparation_x)/dx[0]);
								if (miniIndexb < boxfirst(0)) {
									miniIndexb = boxfirst(0);
								}
								if (maxiIndexb > boxlast(0)) {
									maxiIndexb = boxlast(0);
								}
								int minjIndexb = index1b - ceil((2 * influenceRadius_0 + particleSeparation_y)/dx[1]);
								int maxjIndexb = index1b + ceil((2 * influenceRadius_0 + particleSeparation_y)/dx[1]);
								if (minjIndexb < boxfirst(1)) {
									minjIndexb = boxfirst(1);
								}
								if (maxjIndexb > boxlast(1)) {
									maxjIndexb = boxlast(1);
								}
								int minkIndexb = index2b - ceil((2 * influenceRadius_0 + particleSeparation_z)/dx[2]);
								int maxkIndexb = index2b + ceil((2 * influenceRadius_0 + particleSeparation_z)/dx[2]);
								if (minkIndexb < boxfirst(2)) {
									minkIndexb = boxfirst(2);
								}
								if (maxkIndexb > boxlast(2)) {
									maxkIndexb = boxlast(2);
								}
								for(int index2c = minkIndexb; index2c <= maxkIndexb; index2c++) {
									for(int index1c = minjIndexb; index1c <= maxjIndexb; index1c++) {
										for(int index0c = miniIndexb; index0c <= maxiIndexb; index0c++) {
											hier::Index idxc(index0c, index1c, index2c);
											Particles* partb = problemVariable->getItem(idxc);
											for (int pitb = 0; pitb < partb->getNumberOfParticles(); pitb++) {
												Particle* particleb = partb->getParticle(pitb);
												if (vector(segL3, index0c - boxfirst(0), index1c - boxfirst(1), index2c - boxfirst(2)) == 1) {
													vector(seg3, index0b - boxfirst(0), index1b - boxfirst(1), index2b - boxfirst(2)) = 1;
												}
												if (vector(segL1, index0c - boxfirst(0), index1c - boxfirst(1), index2c - boxfirst(2)) == 1) {
													vector(seg1, index0b - boxfirst(0), index1b - boxfirst(1), index2b - boxfirst(2)) = 1;
												}
												if (vector(segLxLower, index0c - boxfirst(0), index1c - boxfirst(1), index2c - boxfirst(2)) == 1) {
													vector(segxLower, index0b - boxfirst(0), index1b - boxfirst(1), index2b - boxfirst(2)) = 1;
												}
												if (vector(segLxUpper, index0c - boxfirst(0), index1c - boxfirst(1), index2c - boxfirst(2)) == 1) {
													vector(segxUpper, index0b - boxfirst(0), index1b - boxfirst(1), index2b - boxfirst(2)) = 1;
												}
												if (vector(segLyLower, index0c - boxfirst(0), index1c - boxfirst(1), index2c - boxfirst(2)) == 1) {
													vector(segyLower, index0b - boxfirst(0), index1b - boxfirst(1), index2b - boxfirst(2)) = 1;
												}
												if (vector(segLyUpper, index0c - boxfirst(0), index1c - boxfirst(1), index2c - boxfirst(2)) == 1) {
													vector(segyUpper, index0b - boxfirst(0), index1b - boxfirst(1), index2b - boxfirst(2)) = 1;
												}
												if (vector(segLzLower, index0c - boxfirst(0), index1c - boxfirst(1), index2c - boxfirst(2)) == 1) {
													vector(segzLower, index0b - boxfirst(0), index1b - boxfirst(1), index2b - boxfirst(2)) = 1;
												}
												if (vector(segLzUpper, index0c - boxfirst(0), index1c - boxfirst(1), index2c - boxfirst(2)) == 1) {
													vector(segzUpper, index0b - boxfirst(0), index1b - boxfirst(1), index2b - boxfirst(2)) = 1;
												}
											}
										}
									}
								}
								if (vector(seg3, index0b - boxfirst(0), index1b - boxfirst(1), index2b - boxfirst(2)) == -1) {
									vector(seg3, index0b - boxfirst(0), index1b - boxfirst(1), index2b - boxfirst(2)) = 0;
								}
								if (vector(seg1, index0b - boxfirst(0), index1b - boxfirst(1), index2b - boxfirst(2)) == -1) {
									vector(seg1, index0b - boxfirst(0), index1b - boxfirst(1), index2b - boxfirst(2)) = 0;
								}
								if (vector(segxLower, index0b - boxfirst(0), index1b - boxfirst(1), index2b - boxfirst(2)) == -1) {
									vector(segxLower, index0b - boxfirst(0), index1b - boxfirst(1), index2b - boxfirst(2)) = 0;
								}
								if (vector(segxUpper, index0b - boxfirst(0), index1b - boxfirst(1), index2b - boxfirst(2)) == -1) {
									vector(segxUpper, index0b - boxfirst(0), index1b - boxfirst(1), index2b - boxfirst(2)) = 0;
								}
								if (vector(segyLower, index0b - boxfirst(0), index1b - boxfirst(1), index2b - boxfirst(2)) == -1) {
									vector(segyLower, index0b - boxfirst(0), index1b - boxfirst(1), index2b - boxfirst(2)) = 0;
								}
								if (vector(segyUpper, index0b - boxfirst(0), index1b - boxfirst(1), index2b - boxfirst(2)) == -1) {
									vector(segyUpper, index0b - boxfirst(0), index1b - boxfirst(1), index2b - boxfirst(2)) = 0;
								}
								if (vector(segzLower, index0b - boxfirst(0), index1b - boxfirst(1), index2b - boxfirst(2)) == -1) {
									vector(segzLower, index0b - boxfirst(0), index1b - boxfirst(1), index2b - boxfirst(2)) = 0;
								}
								if (vector(segzUpper, index0b - boxfirst(0), index1b - boxfirst(1), index2b - boxfirst(2)) == -1) {
									vector(segzUpper, index0b - boxfirst(0), index1b - boxfirst(1), index2b - boxfirst(2)) = 0;
								}
							}
						}
					}
				}
				delete newParticle;
			}
		}
	} else {
		part->deleteParticle(*particle);
	}
}
void Problem::checkPosition(const std::shared_ptr<hier::Patch >& patch, double index0, double index1, double index2, Particles* particles)
{
	const hier::Index boxfirst1 = patch->getBox().lower();
	const hier::Index boxlast1  = patch->getBox().upper();

	std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));

	int xlower = patch_geom->getXLower()[0];
	int xupper = patch_geom->getXUpper()[0];
	bool periodicL0 = xGlower == xlower && !patch->getPatchGeometry()->getTouchesRegularBoundary(0, 0);
	bool periodicU0 = xGupper == xupper && !patch->getPatchGeometry()->getTouchesRegularBoundary(0, 1);
	int ylower = patch_geom->getXLower()[1];
	int yupper = patch_geom->getXUpper()[1];
	bool periodicL1 = yGlower == ylower && !patch->getPatchGeometry()->getTouchesRegularBoundary(1, 0);
	bool periodicU1 = yGupper == yupper && !patch->getPatchGeometry()->getTouchesRegularBoundary(1, 1);
	int zlower = patch_geom->getXLower()[2];
	int zupper = patch_geom->getXUpper()[2];
	bool periodicL2 = zGlower == zlower && !patch->getPatchGeometry()->getTouchesRegularBoundary(2, 0);
	bool periodicU2 = zGupper == zupper && !patch->getPatchGeometry()->getTouchesRegularBoundary(2, 1);
	if (index0 < boxfirst1[0] || index0 > boxlast1[0] || index1 < boxfirst1[1] || index1 > boxlast1[1] || index2 < boxfirst1[2] || index2 > boxlast1[2] ) {
		for(int pit = 0; pit < particles->getNumberOfParticles(); pit++) {
			Particle* particle = particles->getParticle(pit);
			double positioni_p = particle->positioni_p;
			double positionj_p = particle->positionj_p;
			double positionk_p = particle->positionk_p;
			double positioni = particle->positioni;
			double positionj = particle->positionj;
			double positionk = particle->positionk;
			double positioniSPprime = particle->positioniSPprime;
			double positionjSPprime = particle->positionjSPprime;
			double positionkSPprime = particle->positionkSPprime;
			if (periodicL0 && index0 < boxfirst1[0] && positioni_p > xGlower) {
				positioni_p=positioni_p-(xGupper - xGlower);
			}
			if (periodicU0 && index0 > boxlast1[0] && positioni_p < xGupper) {
				positioni_p=positioni_p+(xGupper - xGlower);
			}
			if (periodicL1 && index1 < boxfirst1[1] && positionj_p > yGlower) {
				positionj_p=positionj_p-(yGupper - yGlower);
			}
			if (periodicU1 && index1 > boxlast1[1] && positionj_p < yGupper) {
				positionj_p=positionj_p+(yGupper - yGlower);
			}
			if (periodicL2 && index2 < boxfirst1[2] && positionk_p > zGlower) {
				positionk_p=positionk_p-(zGupper - zGlower);
			}
			if (periodicU2 && index2 > boxlast1[2] && positionk_p < zGupper) {
				positionk_p=positionk_p+(zGupper - zGlower);
			}
			if (periodicL0 && index0 < boxfirst1[0] && positioni > xGlower) {
				positioni=positioni-(xGupper - xGlower);
			}
			if (periodicU0 && index0 > boxlast1[0] && positioni < xGupper) {
				positioni=positioni+(xGupper - xGlower);
			}
			if (periodicL1 && index1 < boxfirst1[1] && positionj > yGlower) {
				positionj=positionj-(yGupper - yGlower);
			}
			if (periodicU1 && index1 > boxlast1[1] && positionj < yGupper) {
				positionj=positionj+(yGupper - yGlower);
			}
			if (periodicL2 && index2 < boxfirst1[2] && positionk > zGlower) {
				positionk=positionk-(zGupper - zGlower);
			}
			if (periodicU2 && index2 > boxlast1[2] && positionk < zGupper) {
				positionk=positionk+(zGupper - zGlower);
			}
			if (periodicL0 && index0 < boxfirst1[0] && positioniSPprime > xGlower) {
				positioniSPprime=positioniSPprime-(xGupper - xGlower);
			}
			if (periodicU0 && index0 > boxlast1[0] && positioniSPprime < xGupper) {
				positioniSPprime=positioniSPprime+(xGupper - xGlower);
			}
			if (periodicL1 && index1 < boxfirst1[1] && positionjSPprime > yGlower) {
				positionjSPprime=positionjSPprime-(yGupper - yGlower);
			}
			if (periodicU1 && index1 > boxlast1[1] && positionjSPprime < yGupper) {
				positionjSPprime=positionjSPprime+(yGupper - yGlower);
			}
			if (periodicL2 && index2 < boxfirst1[2] && positionkSPprime > zGlower) {
				positionkSPprime=positionkSPprime-(zGupper - zGlower);
			}
			if (periodicU2 && index2 > boxlast1[2] && positionkSPprime < zGupper) {
				positionkSPprime=positionkSPprime+(zGupper - zGlower);
			}
			particle->positioni_p = positioni_p;
			particle->positionj_p = positionj_p;
			particle->positionk_p = positionk_p;
			particle->positioni = positioni;
			particle->positionj = positionj;
			particle->positionk = positionk;
			particle->positioniSPprime = positioniSPprime;
			particle->positionjSPprime = positionjSPprime;
			particle->positionkSPprime = positionkSPprime;
		}
	}
}


/*
 * Initialization of the common variables in case of restarting.
 */
void Problem::initCommonVars(const std::shared_ptr<hier::PatchHierarchy >& hierarchy)
{
	dx  = d_grid_geometry->getDx();
	xGlower = d_grid_geometry->getXLower()[0];
	xGupper = d_grid_geometry->getXUpper()[0];
	ilastG = d_grid_geometry->getPhysicalDomain().front().numberCells()[0] + 2 * d_ghost_width;
	yGlower = d_grid_geometry->getXLower()[1];
	yGupper = d_grid_geometry->getXUpper()[1];
	jlastG = d_grid_geometry->getPhysicalDomain().front().numberCells()[1] + 2 * d_ghost_width;
	zGlower = d_grid_geometry->getXLower()[2];
	zGupper = d_grid_geometry->getXUpper()[2];
	klastG = d_grid_geometry->getPhysicalDomain().front().numberCells()[2] + 2 * d_ghost_width;
}


