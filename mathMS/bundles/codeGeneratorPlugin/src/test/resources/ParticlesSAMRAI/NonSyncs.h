#ifndef included_NonSyncs
#define included_NonSyncs

#include "NonSync.h"
#include "SAMRAI/pdat/CellIndex.h"
#include <vector>
#include <string>
#include <iostream>
#include "SAMRAI/tbox/MessageStream.h"
#include "SAMRAI/tbox/Database.h"

/**
 * The SampleClass struct holds some dummy data and methods.  It's intent
 * is to indicate how a user could construct their own index data type.
 */
using namespace std;
using namespace SAMRAI;

class NonSyncs
{
public:
   /**
    * The default constructor.
    */
   NonSyncs();

   /**
    * Copy constructor.
    */
   NonSyncs(const NonSyncs& data);

   /**
    * The assignment operator copies the data of the argument cell.
    */
   NonSyncs& operator=(const NonSyncs& cell);

   /**
    * The assignment operator copies the data of the argument cell.
    */
   bool operator==(const NonSyncs& cell);


   ~NonSyncs();

   //Calculates the average value of the cell from the values of the NonSyncs
   void calculateAverage();

   //Adds the NonSync to the list
   void addNonSync(const NonSync& data);

   //delete NonSync from the list
   void deleteNonSync(const NonSync& data);

   int getNumberOfNonSyncs();

   NonSync* overlaps(const NonSync& data);

   vector<NonSync > getNonSyncList();

   //Gets the NonSync in the position i
   NonSync* getNonSync(const int index);

   /**
    * The copySourceItem() method allows SampleIndexData to be a templated
    * data type for IndexData - i.e. IndexData<SampleIndexData>.  In
    * addition to this method, the other methods that must be defined are
    * getDataStreamSize(), packStream(), unpackStream() for communication,
    * putToDatabase(), getFromDatabase for restart.  These are described
    * below. 
    */
   void copySourceItem(hier::Index& index, 
                       const hier::IntVector& src_offset, 
                       NonSyncs& src_item);

   /**
    * The following functions enable parallel communication with SampleIndexDatas.
    * They are used in SAMRAI communication infrastructure to 
    * specify the number of bytes of data stored in each SampleIndexData object,
    * and to pack and unpack the data to the specified stream.
    */
   size_t getDataStreamSize();
   void packStream(tbox::MessageStream& stream);
   void unpackStream(tbox::MessageStream& stream, const hier::IntVector& offset);

   /**
    * These functions are used to read/write SampleIndexData data to/from 
    * restart.
    */
   void getFromRestart(std::shared_ptr<tbox::Database>& database);
   void putToRestart(std::shared_ptr<tbox::Database>& database);

private:
   //NonSync list inside a cell
   vector<NonSync > nonSyncList;

};

#endif
