#include "Problem.h"
#include "Functions.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <stack>
#include "hdf5.h"
#include <gsl/gsl_rng.h>
#include "SAMRAI/pdat/NodeData.h"
#include "SAMRAI/pdat/NodeVariable.h"
#include "SAMRAI/pdat/CellVariable.h"
#include "LagrangianPolynomicRefine.h"


#include "SAMRAI/tbox/Array.h"
#include "SAMRAI/hier/BoundaryBox.h"
#include "SAMRAI/hier/BoxContainer.h"
#include "SAMRAI/geom/CartesianPatchGeometry.h"
#include "SAMRAI/hier/VariableDatabase.h"
#include "SAMRAI/hier/PatchDataRestartManager.h"
#include "SAMRAI/hier/Index.h"
#include "SAMRAI/tbox/PIO.h"
#include "SAMRAI/tbox/Utilities.h"
#include "SAMRAI/tbox/Timer.h"
#include "SAMRAI/tbox/TimerManager.h"

#define MIN(X,Y) ((X) < (Y) ? (X) : (Y))
#define MAX(X,Y) ((X) > (Y) ? (X) : (Y))
#define SIGN(X) (((X) > 0) - ((X) < 0))
#define isEven(a) ((a) % 2 == 0 ? true : false)
#define greaterThan(a,b) (!((fabs((a) - (b))/1.0E-9 > 10 ? false: (floor(fabs((a) - (b))/1.0E-9) < 1)) || (a)<(b)))
#define lessThan(a,b) (!((fabs((a) - (b))/1.0E-9 > 10 ? false : (floor(fabs((a) - (b))/1.0E-9) < 1)) || (b)<(a)))
#define greaterEq(a,b) ((fabs((a) - (b))/1.0E-9 > 10 ? false : (floor(fabs((a) - (b))/1.0E-9) < 1)) || (b)<(a))
#define lessEq(a,b) ((fabs((a) - (b))/1.0E-9 > 10 ? false: (floor(fabs((a) - (b))/1.0E-9) < 1)) || (a)<(b))
#define equalsEq(a,b) ((fabs((a) - (b))/1.0E-9 > 10 ? false: (floor(fabs((a) - (b))/1.0E-9) < 1)))

#define vector(v, i, j) (v)[i+xlast*(j)]
#define vectorT(v, i, j) (v)[i+xtlast*(j)]
#define indexxOf(i) (i - ((int)(i/(boxlast(0)-boxfirst(0)+1))*(boxlast(0)-boxfirst(0)+1)))
#define indexyOf(i) ((int)(i/(boxlast(0)-boxfirst(0)+1)))




const gsl_rng_type * T;
gsl_rng *r_var;

//Timers
std::shared_ptr<tbox::Timer> t_step;
std::shared_ptr<tbox::Timer> t_moveParticles;

inline int Problem::GetExpoBase2(double d)
{
	int i = 0;
	((short *)(&i))[0] = (((short *)(&d))[3] & (short)32752); // _123456789ab____ & 0111111111110000
	return (i >> 4) - 1023;
}

bool	Problem::Equals(double d1, double d2)
{
	if (d1 == d2)
		return true;
	int e1 = GetExpoBase2(d1);
	int e2 = GetExpoBase2(d2);
	int e3 = GetExpoBase2(d1 - d2);
	if ((e3 - e2 < -48) && (e3 - e1 < -48))
		return true;
	return false;
}

/*
 * Constructor of the problem.
 */
Problem::Problem(const string& object_name, const tbox::Dimension& dim, std::shared_ptr<tbox::Database>& database, std::shared_ptr<geom::CartesianGridGeometry >& grid_geom, std::shared_ptr<hier::PatchHierarchy >& patch_hierarchy, const double dt, hier::BoxContainer& ba, const bool init_from_files, const vector<string> full_writer_variables, const vector<vector<string> > integralVariables): 
d_dim(dim), xfer::RefinePatchStrategy(), xfer::CoarsenPatchStrategy(), d_regridding_boxes(ba), d_full_writer_variables(full_writer_variables.begin(), full_writer_variables.end())
{
	//Setup the timers
	t_step = tbox::TimerManager::getManager()->getTimer("Step");
	t_moveParticles = tbox::TimerManager::getManager()->getTimer("Move particles");

	//Get the object name, the grid geometry and the patch hierarchy
  	d_grid_geometry = grid_geom;
	d_patch_hierarchy = patch_hierarchy;
	d_object_name = object_name;
	d_init_from_files = init_from_files;
	initial_dt = dt;


	for (vector<vector<string> >::const_iterator it = integralVariables.begin(); it != integralVariables.end(); ++it) {
		vector<string> vars = *it;
		for (vector<string>::const_iterator it2 = vars.begin() ; it2 != vars.end(); ++it2) {
			d_integralVariables.push_back(vars);
		}
	}


	//Get parameters
	energy_period = database->getInteger("energy_period");
	time_steps = database->getInteger("time_steps");
	temperature = database->getDouble("temperature");
	//Random initialization
	gsl_rng_env_setup();
	//Random for simulation
	const tbox::SAMRAI_MPI& mpi(tbox::SAMRAI_MPI::getSAMRAIWorld());
	int random_seed = database->getDouble("random_seed")*(mpi.getRank() + 1);
	r_var = gsl_rng_alloc(gsl_rng_ranlxs0);
	gsl_rng_set(r_var, random_seed);


    //Subcycling
	d_refinedTimeStepping = false;
	d_tappering = false;
	if (database->isString("subcycling")) {
		if (database->getString("subcycling") == "TAPPERING") {
			d_tappering = true;
			d_refinedTimeStepping = true;
		}
		if (database->getString("subcycling") == "BERGER-OLIGER") {
			d_tappering = false;
			d_refinedTimeStepping = true;
		}
	}


	//Regridding options
	d_regridding = false;
	if (database->isDatabase("regridding")) {
		regridding_db = database->getDatabase("regridding");
		if (regridding_db->isString("regridding_type")) {
			d_regridding_type = regridding_db->getString("regridding_type");
			d_regridding_level_threshold = regridding_db->getInteger("regridding_level_threshold");
			if (d_regridding_type == "GRADIENT") {
				d_regridding_field = regridding_db->getString("regridding_field");
				d_regridding_compressionFactor = regridding_db->getDouble("regridding_compressionFactor");
				d_regridding_mOffset = regridding_db->getDouble("regridding_mOffset");
				d_regridding = true;
			} else {
				if (d_regridding_type == "FUNCTION") {
					d_regridding_field = regridding_db->getString("regridding_function_field");
					d_regridding_threshold = regridding_db->getDouble("regridding_threshold");
					d_regridding = true;
				}
			}
		}
	}

	//Stencil of the discretization method
	int maxratio = 1;
	for (int il = 1; il < d_patch_hierarchy->getMaxNumberOfLevels(); il++) {
		const hier::IntVector ratio = d_patch_hierarchy->getRatioToCoarserLevel(il);
		maxratio = MAX(maxratio, ratio.max());
	}
	//Minimum region thickness
	d_regionMinThickness = 1;
	// number of ghosts stencil * (#substeps) * #parentlevelsteps
	d_ghost_width = 1 * (1 + 0 * d_tappering) * (1 + d_tappering * (maxratio - 1));

	
	//Register Fields and temporal fields into the variable database
	hier::VariableDatabase *vdb = hier::VariableDatabase::getDatabase();
  	std::shared_ptr<hier::VariableContext> d_cont_curr(vdb->getContext("Current"));
	std::shared_ptr< pdat::CellVariable<int> > mask(std::shared_ptr< pdat::CellVariable<int> >(new pdat::CellVariable<int>(d_dim, "samrai_mask",1)));
	d_mask_id = vdb->registerVariableAndContext(mask ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	IntegrateDataWriter::setMaskVariable(d_mask_id);
	std::shared_ptr< pdat::NodeVariable<double> > interior(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "interior",1)));
	d_interior_id = vdb->registerVariableAndContext(interior ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_interior_id);
	std::shared_ptr< pdat::NodeVariable<int> > nonSync(std::shared_ptr< pdat::NodeVariable<int> >(new pdat::NodeVariable<int>(d_dim, "nonSync",1)));
	d_nonSync_id = vdb->registerVariableAndContext(nonSync ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_nonSync_id);
	std::shared_ptr< pdat::NodeVariable<double> > interior_x(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "interior_x",1)));
	d_interior_x_id = vdb->registerVariableAndContext(interior_x ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_interior_x_id);
	std::shared_ptr< pdat::NodeVariable<double> > interior_y(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "interior_y",1)));
	d_interior_y_id = vdb->registerVariableAndContext(interior_y ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_interior_y_id);
	std::shared_ptr< pdat::NodeVariable<double> > FOV_1(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FOV_1",1)));
	d_FOV_1_id = vdb->registerVariableAndContext(FOV_1 ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FOV_1_id);
	std::shared_ptr< pdat::NodeVariable<double> > FOV_xLower(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FOV_xLower",1)));
	d_FOV_xLower_id = vdb->registerVariableAndContext(FOV_xLower ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FOV_xLower_id);
	std::shared_ptr< pdat::NodeVariable<double> > FOV_xUpper(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FOV_xUpper",1)));
	d_FOV_xUpper_id = vdb->registerVariableAndContext(FOV_xUpper ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FOV_xUpper_id);
	std::shared_ptr< pdat::NodeVariable<double> > FOV_yLower(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FOV_yLower",1)));
	d_FOV_yLower_id = vdb->registerVariableAndContext(FOV_yLower ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FOV_yLower_id);
	std::shared_ptr< pdat::NodeVariable<double> > FOV_yUpper(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "FOV_yUpper",1)));
	d_FOV_yUpper_id = vdb->registerVariableAndContext(FOV_yUpper ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_FOV_yUpper_id);
	std::shared_ptr< pdat::NodeVariable<double> > spin(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "spin",1)));
	d_spin_id = vdb->registerVariableAndContext(spin ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_spin_id);
	std::shared_ptr< pdat::NodeVariable<double> > energy(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, "energy",1)));
	d_energy_id = vdb->registerVariableAndContext(energy ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));
	hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_energy_id);


	//Refine and coarse algorithms

	d_bdry_fill_init = std::shared_ptr< xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());
	d_bdry_fill_advance0 = std::shared_ptr< xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());
	d_coarsen_algorithm = std::shared_ptr< xfer::CoarsenAlgorithm >(new xfer::CoarsenAlgorithm(d_dim));

	d_mapping_fill = std::shared_ptr< xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());
	d_fill_new_level    = std::shared_ptr< xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());

	//mapping communication

	d_mapping_fill->registerRefine(d_FOV_1_id,d_FOV_1_id,d_FOV_1_id, refine_operator_map);
	d_mapping_fill->registerRefine(d_FOV_xLower_id,d_FOV_xLower_id,d_FOV_xLower_id, refine_operator_map);
	d_mapping_fill->registerRefine(d_FOV_xUpper_id,d_FOV_xUpper_id,d_FOV_xUpper_id, refine_operator_map);
	d_mapping_fill->registerRefine(d_FOV_yLower_id,d_FOV_yLower_id,d_FOV_yLower_id, refine_operator_map);
	d_mapping_fill->registerRefine(d_FOV_yUpper_id,d_FOV_yUpper_id,d_FOV_yUpper_id, refine_operator_map);


	//refine and coarsen operators
	string refine_op_name = "LINEAR_REFINE";
	int order = 0;
	if (database->isDatabase("regridding")) {
		regridding_db = database->getDatabase("regridding");
		if (regridding_db->isString("interpolator")) {
			refine_op_name = regridding_db->getString("interpolator");
			if (refine_op_name == "LINEAR_REFINE") {
				order = 1;
			}
			if (refine_op_name == "CUBIC_REFINE") {
				order = 3;
			}
			if (refine_op_name == "QUINTIC_REFINE") {
				order = 5;
			}
		}
	}
	std::shared_ptr< hier::RefineOperator > refine_operator, refine_operator_bound;
	std::shared_ptr< hier::CoarsenOperator > coarsen_operator = d_grid_geometry->lookupCoarsenOperator(spin, "CONSTANT_COARSEN");
	if (order > 0) {
		std::shared_ptr< hier::RefineOperator > tmp_refine_operator(new LagrangianPolynomicRefine(false, order, d_dim));
		refine_operator = tmp_refine_operator;
		std::shared_ptr< hier::RefineOperator > tmp_refine_operator_bound(new LagrangianPolynomicRefine(true, order, d_dim));
		refine_operator_bound = tmp_refine_operator_bound;
	} else {
		refine_operator = d_grid_geometry->lookupRefineOperator(spin, refine_op_name);
		refine_operator_bound = d_grid_geometry->lookupRefineOperator(spin, refine_op_name);
	}

	time_interpolate_operator = new TimeInterpolator();
	std::shared_ptr<SAMRAI::hier::TimeInterpolateOperator> tio(time_interpolate_operator);


	//Register variables to the refineAlgorithm for boundaries

	d_bdry_fill_init->registerRefine(d_spin_id,d_spin_id,d_spin_id,refine_operator);
	d_bdry_fill_advance0->registerRefine(d_spin_id,d_spin_id,d_spin_id,refine_operator);
	d_bdry_fill_init->registerRefine(d_energy_id,d_energy_id,d_energy_id,refine_operator);
	d_bdry_fill_advance0->registerRefine(d_energy_id,d_energy_id,d_energy_id,refine_operator);


	//Register variables to the refineAlgorithm for filling new levels on regridding
	d_fill_new_level->registerRefine(d_spin_id,d_spin_id,d_spin_id,refine_operator_bound);
	d_fill_new_level->registerRefine(d_energy_id,d_energy_id,d_energy_id,refine_operator_bound);


	//Register variables to the coarsenAlgorithm


}

/*
 * Destructor.
 */
Problem::~Problem() 
{
} 

/*
 * Initialize the data from a given level.
 */
void Problem::initializeLevelData (
   const std::shared_ptr<hier::PatchHierarchy >& hierarchy , 
   const int level_number ,
   const double init_data_time ,
   const bool can_be_refined ,
   const bool initial_time ,
   const std::shared_ptr<hier::PatchLevel >& old_level ,
   const bool allocate_data )
{
	std::shared_ptr< hier::PatchLevel > level(hierarchy->getPatchLevel(level_number));

	// Allocate storage needed to initialize level and fill data from coarser levels in AMR hierarchy.  
	level->allocatePatchData(d_interior_id, init_data_time);
	level->allocatePatchData(d_nonSync_id, init_data_time);
	level->allocatePatchData(d_interior_x_id, init_data_time);
	level->allocatePatchData(d_interior_y_id, init_data_time);
	level->allocatePatchData(d_spin_id, init_data_time);
	level->allocatePatchData(d_energy_id, init_data_time);
	level->allocatePatchData(d_FOV_1_id, init_data_time);
	level->allocatePatchData(d_FOV_xLower_id, init_data_time);
	level->allocatePatchData(d_FOV_xUpper_id, init_data_time);
	level->allocatePatchData(d_FOV_yLower_id, init_data_time);
	level->allocatePatchData(d_FOV_yUpper_id, init_data_time);
	level->allocatePatchData(d_mask_id, init_data_time);


	//Fill a finer level with the data of the next coarse level.
	if (!initial_time && ((level_number > 0) || old_level)) {
		d_fill_new_level->createSchedule(level,old_level,level_number-1,hierarchy,this)->fillData(init_data_time, false);
	}

	//Mapping the current data for new level.
	if (initial_time) {
		mapDataOnPatch(init_data_time, initial_time, level_number, level);
	}

	//Fill a finer level with the data of the next coarse level.
	if ((level_number > 0) || old_level) {
		d_mapping_fill->createSchedule(level,old_level,level_number-1,hierarchy,this)->fillData(init_data_time, false);
		correctFOVS(level);
	}



	//Initialize current data for new level.
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr< hier::Patch > patch = *p_it;
        		if (initial_time) {
	      		initializeDataOnPatch(*patch, init_data_time, initial_time);
        		}
	}
	//Post-initialization Sync.
    	if (initial_time) {

		d_bdry_fill_init->createSchedule(level,this)->fillData(init_data_time, false);

    	}
}



void Problem::initializeLevelIntegrator(
   const std::shared_ptr<mesh::GriddingAlgorithmStrategy>& gridding_alg)
{
}

double Problem::getLevelDt(
   const std::shared_ptr<hier::PatchLevel>& level,
   const double dt_time,
   const bool initial_time)
{
  
   TBOX_ASSERT(level);

   if (level->getLevelNumber() == 0) return initial_dt;

    double dt = initial_dt;
    const hier::IntVector ratio = level->getRatioToLevelZero();
    double local_dt = dt;
    for (int i = 0; i < 2; i++) {
        if (local_dt > dt / ratio[i]) {
            local_dt = dt / ratio[i];
        }
    }
    return local_dt;
}

double Problem::getMaxFinerLevelDt(
   const int finer_level_number,
   const double coarse_dt,
   const hier::IntVector& ratio)
{
   NULL_USE(finer_level_number);

   TBOX_ASSERT(ratio.min() > 0);

   return coarse_dt / double(ratio.max());
}

void Problem::standardLevelSynchronization(
   const std::shared_ptr<hier::PatchHierarchy>& hierarchy,
   const int coarsest_level,
   const int finest_level,
   const double sync_time,
   const std::vector<double>& old_times)
{

}

void Problem::synchronizeNewLevels(
   const std::shared_ptr<hier::PatchHierarchy>& hierarchy,
   const int coarsest_level,
   const int finest_level,
   const double sync_time,
   const bool initial_time)
{

}

void Problem::resetTimeDependentData(
   const std::shared_ptr<hier::PatchLevel>& level,
   const double new_time,
   const bool can_be_refined)
{
   TBOX_ASSERT(level);
   level->setTime(new_time);
}

void Problem::resetDataToPreadvanceState(
   const std::shared_ptr<hier::PatchLevel>& level)
{
    //cout<<"resetDataToPreadvanceState"<<endl;
}

/*
 * Map data on a patch. This mapping is done only at the begining of the simulation.
 */
void Problem::mapDataOnPatch(const double time, const bool initial_time, const int ln, const std::shared_ptr< hier::PatchLevel >& level)
{
	(void) time;
	const tbox::SAMRAI_MPI& mpi(tbox::SAMRAI_MPI::getSAMRAIWorld());
   	if (initial_time) {

		// Mapping		
		int x, xMapStart, xMapEnd, xterm, previousMapx, xWallAcc;
		bool interiorMapx;
		int y, yMapStart, yMapEnd, yterm, previousMapy, yWallAcc;
		bool interiorMapy;
		int minBlock[2], maxBlock[2], unionsI, facePointI, ie1, ie2, ie3, proc, pcounter, working, finished, pred;
		double maxDistance, e1, e2, e3;
		bool done, modif, workingArray[mpi.getSize()], finishedArray[mpi.getSize()], workingGlobal, finishedGlobal, workingPatchArray[level->getLocalNumberOfPatches()], finishedPatchArray[level->getLocalNumberOfPatches()], workingPatchGlobal, finishedPatchGlobal;
		int nodes = mpi.getSize();
		int patches = level->getLocalNumberOfPatches();
		double SQRT3INV = 1.0/sqrt(3.0);

		//FOV initialization
		for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
			const std::shared_ptr< hier::Patch >& patch = *p_it;

			//Get the dimensions of the patch
			hier::Box pbox = patch->getBox();
			double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
			double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
			double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
			double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
			double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
			double* interior = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_id).get())->getPointer();
			int* nonSync = ((pdat::NodeData<int> *) patch->getPatchData(d_nonSync_id).get())->getPointer();
			const hier::Index boxfirst = patch->getBox().lower();
			const hier::Index boxlast  = patch->getBox().upper();

			//Get delta spaces into an array. dx, dy, dz.
			const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
			const double* dx  = patch_geom->getDx();

			int xlast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
			int ylast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;

			for (x = 0; x < xlast; x++) {
				for (y = 0; y < ylast; y++) {
					vector(FOV_1, x, y) = 0;
					vector(FOV_xLower, x, y) = 0;
					vector(FOV_xUpper, x, y) = 0;
					vector(FOV_yLower, x, y) = 0;
					vector(FOV_yUpper, x, y) = 0;
				}
			}
		}
		d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);

		if (ln == 0) {
			//Interior
			for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
				const std::shared_ptr< hier::Patch >& patch = *p_it;

				//Get the dimensions of the patch
				hier::Box pbox = patch->getBox();
				double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
				double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
				double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
				double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
				double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
				double* interior = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_id).get())->getPointer();
				int* nonSync = ((pdat::NodeData<int> *) patch->getPatchData(d_nonSync_id).get())->getPointer();
				const hier::Index boxfirst = patch->getBox().lower();
				const hier::Index boxlast  = patch->getBox().upper();

				//Get delta spaces into an array. dx, dy, dz.
				const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
				const double* dx  = patch_geom->getDx();

				int xlast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
				int ylast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;

				xMapStart = 0;
				xMapEnd = d_grid_geometry->getPhysicalDomain().front().numberCells()[0] - 1;
				yMapStart = 0;
				yMapEnd = d_grid_geometry->getPhysicalDomain().front().numberCells()[1] - 1;
				for (x = xMapStart; x <= xMapEnd; x++) {
					for (y = yMapStart; y <= yMapEnd; y++) {
						if (x >= boxfirst(0) - d_ghost_width && x <= boxlast(0) + d_ghost_width && y >= boxfirst(1) - d_ghost_width && y <= boxlast(1) + d_ghost_width) {
							vector(FOV_1, x - boxfirst(0) + d_ghost_width, y - boxfirst(1) + d_ghost_width) = 100;
							vector(FOV_xLower, x - boxfirst(0) + d_ghost_width, y - boxfirst(1) + d_ghost_width) = 0;
							vector(FOV_xUpper, x - boxfirst(0) + d_ghost_width, y - boxfirst(1) + d_ghost_width) = 0;
							vector(FOV_yLower, x - boxfirst(0) + d_ghost_width, y - boxfirst(1) + d_ghost_width) = 0;
							vector(FOV_yUpper, x - boxfirst(0) + d_ghost_width, y - boxfirst(1) + d_ghost_width) = 0;
						}
					}
				}
			}
			d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);

		}
		//Boundaries Mapping
		for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
			const std::shared_ptr< hier::Patch >& patch = *p_it;

			//Get the dimensions of the patch
			hier::Box pbox = patch->getBox();
			double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
			double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
			double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
			double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
			double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
			const hier::Index boxfirst = patch->getBox().lower();
			const hier::Index boxlast  = patch->getBox().upper();

			//Get delta spaces into an array. dx, dy, dz.
			const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
			const double* dx  = patch_geom->getDx();

			int xlast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
			int ylast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;

			//y-Upper
			if (patch->getPatchGeometry()->getTouchesRegularBoundary(1, 1)) {
				for (y = ylast - d_ghost_width; y < ylast; y++) {
					for (x = 0; x < xlast; x++) {
						vector(FOV_yUpper, x, y) = 100;
						vector(FOV_1, x, y) = 0;
						vector(FOV_xLower, x, y) = 0;
						vector(FOV_xUpper, x, y) = 0;
						vector(FOV_yLower, x, y) = 0;
					}
				}
			}
			//y-Lower
			if (patch->getPatchGeometry()->getTouchesRegularBoundary(1, 0)) {
				for (y = 0; y < d_ghost_width; y++) {
					for (x = 0; x < xlast; x++) {
						vector(FOV_yLower, x, y) = 100;
						vector(FOV_1, x, y) = 0;
						vector(FOV_xLower, x, y) = 0;
						vector(FOV_xUpper, x, y) = 0;
						vector(FOV_yUpper, x, y) = 0;
					}
				}
			}
			//x-Upper
			if (patch->getPatchGeometry()->getTouchesRegularBoundary(0, 1)) {
				for (y = 0; y < ylast; y++) {
					for (x = xlast - d_ghost_width; x < xlast; x++) {
						vector(FOV_xUpper, x, y) = 100;
						vector(FOV_1, x, y) = 0;
						vector(FOV_xLower, x, y) = 0;
						vector(FOV_yLower, x, y) = 0;
						vector(FOV_yUpper, x, y) = 0;
					}
				}
			}
			//x-Lower
			if (patch->getPatchGeometry()->getTouchesRegularBoundary(0, 0)) {
				for (y = 0; y < ylast; y++) {
					for (x = 0; x < d_ghost_width; x++) {
						vector(FOV_xLower, x, y) = 100;
						vector(FOV_1, x, y) = 0;
						vector(FOV_xUpper, x, y) = 0;
						vector(FOV_yLower, x, y) = 0;
						vector(FOV_yUpper, x, y) = 0;
					}
				}
			}
		}
		d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);




   	}
}








/*
 * Initialize data on a patch. This initialization is done only at the begining of the simulation.
 */
void Problem::initializeDataOnPatch(hier::Patch& patch, 
                                    const double time,
                                    const bool initial_time)
{
	(void) time;
   	if (initial_time) {
		// Initial conditions		
		//Get fields, auxiliary fields and local variables that are going to be used.
		double* spin = ((pdat::NodeData<double> *) patch.getPatchData(d_spin_id).get())->getPointer();
		((pdat::NodeData<double> *) patch.getPatchData(d_spin_id).get())->fillAll(0);
		
		//Get the dimensions of the patch
		hier::Box pbox = patch.getBox();
		double* FOV_1 = ((pdat::NodeData<double> *) patch.getPatchData(d_FOV_1_id).get())->getPointer();
		double* FOV_xLower = ((pdat::NodeData<double> *) patch.getPatchData(d_FOV_xLower_id).get())->getPointer();
		double* FOV_xUpper = ((pdat::NodeData<double> *) patch.getPatchData(d_FOV_xUpper_id).get())->getPointer();
		double* FOV_yLower = ((pdat::NodeData<double> *) patch.getPatchData(d_FOV_yLower_id).get())->getPointer();
		double* FOV_yUpper = ((pdat::NodeData<double> *) patch.getPatchData(d_FOV_yUpper_id).get())->getPointer();
		const hier::Index boxfirst = patch.getBox().lower();
		const hier::Index boxlast  = patch.getBox().upper();
		
		//Get delta spaces into an array. dx, dy, dz.
		const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch.getPatchGeometry()));
		const double* dx  = patch_geom->getDx();
		
		int xlast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
		int ylast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
		for(int index1 = 0; index1 < ylast; index1++) {
			for(int index0 = 0; index0 < xlast; index0++) {
				if (vector(FOV_1, index0, index1) > 0) {
					vector(spin, index0, index1) = int(gsl_rng_uniform(r_var) * (1 + 1));
				}
		
			}
		}
		
		

   	}
}

/*
 * Gets the coarser patch that contains the box
 */
const std::shared_ptr<hier::Patch >& Problem::getCoarserPatch(
	const std::shared_ptr< hier::PatchLevel >& level,
	const hier::Box interior, 
	const hier::IntVector ratio)
{
	const hier::Box& coarsenBox = hier::Box::coarsen(interior, ratio);
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr< hier::Patch >& patch = *p_it;
		const hier::Box& interior_C = patch->getBox();
		if (interior_C.intersects(coarsenBox)) {
			return patch;		
		}
	}
}

/*
 * Reset the hierarchy-dependent internal information.
 */
void Problem::resetHierarchyConfiguration (
   const std::shared_ptr<hier::PatchHierarchy >& new_hierarchy ,
   int coarsest_level ,
   int finest_level )
{
	int finest_hiera_level = new_hierarchy->getFinestLevelNumber();

   	//  If we have added or removed a level, resize the schedule arrays

	d_bdry_sched_advance0.resize(finest_hiera_level+1);
	d_coarsen_schedule.resize(finest_hiera_level+1);
	//  Build coarsen and refine communication schedules.
	for (int ln = coarsest_level; ln <= finest_hiera_level; ln++) {
		std::shared_ptr< hier::PatchLevel > level(new_hierarchy->getPatchLevel(ln));
		d_bdry_sched_advance0[ln] = d_bdry_fill_advance0->createSchedule(level,ln-1,new_hierarchy,this);
		// coarsen schedule only for levels > 0
		if (ln > 0) {
			std::shared_ptr< hier::PatchLevel > coarser_level(new_hierarchy->getPatchLevel(ln-1));
			d_coarsen_schedule[ln] = d_coarsen_algorithm->createSchedule(coarser_level, level, NULL);
		}
	}

}




/*
 * This method sets the physical boundary conditions.
 */
void Problem::setPhysicalBoundaryConditions(
   hier::Patch& patch,
   const double fill_time,
   const hier::IntVector& ghost_width_to_fill)
{
	//Boundary must not be implemented in this method
}

/*
 * Set up external plotter to plot internal data from this class.  
 * Register variables appropriate for plotting.                       
 */
int Problem::setupPlotter(
  appu::VisItDataWriter &plotter
) const {
	if (!d_patch_hierarchy) {
		TBOX_ERROR(d_object_name << ": No hierarchy in\n"
			<< " Problem::setupPlotter\n"
                 	<< "The hierarchy must be set before calling\n"
                 	<< "this function.\n");
   	}
	plotter.registerPlotQuantity("FOV_1","SCALAR",d_FOV_1_id);
	hier::VariableDatabase *vdb = hier::VariableDatabase::getDatabase();
	for (vector<string>::const_iterator it = d_full_writer_variables.begin() ; it != d_full_writer_variables.end(); ++it) {
		string var_to_register = *it;
		if (!(vdb->checkVariableExists(var_to_register))) {
			TBOX_ERROR(d_object_name << ": Variable selected for 3D write not found:" <<  var_to_register);
		}
		int var_id = vdb->getVariable(var_to_register)->getInstanceIdentifier();
		plotter.registerPlotQuantity(var_to_register,"SCALAR",var_id);
	}


   	return 0;
}
/*
 * Set up external plotter to plot integration data from this class.
 * Register variables appropriate for plotting.
 */
int Problem::setupIntegralPlotter(vector<std::shared_ptr<IntegrateDataWriter> > &plotters) const {
	if (!d_patch_hierarchy) {
		TBOX_ERROR(d_object_name << ": No hierarchy inn"
		<< " Problem::setupIntegralPlottern"
		<< "The hierarchy must be set before callingn"
		<< "this function.n");
	}
	int i = 0;
	for (vector<std::shared_ptr<IntegrateDataWriter> >::const_iterator it = plotters.begin(); it != plotters.end(); ++it) {
		std::shared_ptr<IntegrateDataWriter> plotter = *it;
		hier::VariableDatabase *vdb = hier::VariableDatabase::getDatabase();
		vector<string> variables = d_integralVariables[i];
		for (vector<string>::const_iterator it2 = variables.begin() ; it2 != variables.end(); ++it2) {
			string var_to_register = *it2;
			if (!(vdb->checkVariableExists(var_to_register))) {
				TBOX_ERROR(d_object_name << ": Variable selected for Integration not found:" <<  var_to_register);
			}
			int var_id = vdb->getVariable(var_to_register)->getInstanceIdentifier();
			plotter->registerPlotQuantity(var_to_register,"SCALAR",var_id);
		}
		i++;
	}
	return 0;
}


/*
 * Perform a single step from the discretization schema algorithm   
 */
double Problem::advanceLevel(
   const std::shared_ptr<hier::PatchLevel>& level,
   const std::shared_ptr<hier::PatchHierarchy>& hierarchy,
   const double current_time,
   const double new_time,
   const bool first_step,
   const bool last_step,
   const bool regrid_advance)
{
	const int ln = level->getLevelNumber();
	// Calculation of number of current substep
	if (first_step) {
		d_sim_substep[ln] = d_patch_hierarchy->getRatioToCoarserLevel(ln).max();
	} else {
		d_sim_substep[ln] = d_sim_substep[ln] - 1;
	}

	const double simPlat_dt = new_time - current_time;
  const double level_ratio = level->getRatioToCoarserLevel().max();

	t_step->start();
  	// Shifting time
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr<hier::Patch > patch = *p_it;
		std::shared_ptr< pdat::NodeData<double> > spin(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_spin_id)));
		std::shared_ptr< pdat::NodeData<double> > energy(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_energy_id)));

		const hier::Index boxfirst = patch->getBox().lower();
		const hier::Index boxlast  = patch->getBox().upper();
	}
  	// Evolution
	for (int ln=0; ln<=d_patch_hierarchy->getFinestLevelNumber(); ++ln ) {
		std::shared_ptr<hier::PatchLevel > level(d_patch_hierarchy->getPatchLevel(ln));
		//Fill ghosts and periodical boundaries, but no other boundaries
		d_bdry_sched_advance0[ln]->fillData(current_time, false);
		for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
			const std::shared_ptr<hier::Patch >& patch = *p_it;
			double* FOV_1 = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_1_id).get())->getPointer();
			double* FOV_xLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xLower_id).get())->getPointer();
			double* FOV_xUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_xUpper_id).get())->getPointer();
			double* FOV_yLower = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yLower_id).get())->getPointer();
			double* FOV_yUpper = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_yUpper_id).get())->getPointer();
	
			double* spin = ((pdat::NodeData<double> *) patch->getPatchData(d_spin_id).get())->getPointer();
			double* energy = ((pdat::NodeData<double> *) patch->getPatchData(d_energy_id).get())->getPointer();
			double tmpEner;
	
			//Get the dimensions of the patch
			hier::Box pbox = patch->getBox();
			const hier::Index boxfirst = patch->getBox().lower();
			const hier::Index boxlast = patch->getBox().upper();
	
			//Get delta spaces into an array. dx, dy, dz.
			std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
			const double* dx  = patch_geom->getDx();
	
			//Auxiliary definitions
			int xlast = boxlast(0)-boxfirst(0) + 2 + 2 * d_ghost_width;
			int ylast = boxlast(1)-boxfirst(1) + 2 + 2 * d_ghost_width;
			int selected_cell = (boxlast(0) - boxfirst(0) + 1) * (boxlast(1) - boxfirst(1) + 1) * gsl_rng_uniform(r_var);
			int index0 = indexxOf(selected_cell) + d_ghost_width;
			int index1 = indexyOf(selected_cell) + d_ghost_width;
	
			tmpEner = 0.0;
	
			int min0_index = MAX(0, index0 - 1);
			int max0_index = MIN(xlast - 1, index0 + 1);
			int min1_index = MAX(0, index1 - 1);
			int max1_index = MIN(ylast - 1, index1 + 1);
			for(int index1_n = min1_index; index1_n <= max1_index; index1_n++) {
				for(int index0_n = min0_index; index0_n <= max0_index; index0_n++) {
					if (((index0_n != index0) || (index1_n != index1))
					//Do not use corners. Remove the following line if you want to use corners for the calculation
					&& !(((index0_n == index0 + 1 && index1_n == index1 + 1) || (index0_n == index0 + 1 && index1_n == index1 - 1) || (index0_n == index0 - 1 && index1_n == index1 + 1) || (index0_n == index0 - 1 && index1_n == index1 - 1)))
					) {
						if (vector(spin, index0_n, index1_n) == vector(spin, index0, index1)) {
							tmpEner = tmpEner - 1.0;
						} else {
							tmpEner = tmpEner + 1.0;
	
						}
					}
				}
			}
	
			if ((tmpEner >= 0.0) || (exp((2.0 * tmpEner) / temperature) > gsl_rng_uniform(r_var))) {
				vector(spin, index0, index1) = 1.0 - vector(spin, index0, index1);
	
			}
	
			if ((int(simPlat_iteration) % int(energy_period)) == 0.0) {
				for(int index1 = 1; index1 < ylast - 1; index1++) {
					for(int index0 = 1; index0 < xlast - 1; index0++) {
						vector(energy, index0, index1) = 0.0;
						int min0_index = MAX(0, index0 - 1);
						int max0_index = MIN(xlast - 1, index0 + 1);
						int min1_index = MAX(0, index1 - 1);
						int max1_index = MIN(ylast - 1, index1 + 1);
						for(int index1_n = min1_index; index1_n <= max1_index; index1_n++) {
							for(int index0_n = min0_index; index0_n <= max0_index; index0_n++) {
								if (((index0_n != index0) || (index1_n != index1))
								//Do not use corners. Remove the following line if you want to use corners for the calculation
								&& !(((index0_n == index0 + 1 && index1_n == index1 + 1) || (index0_n == index0 + 1 && index1_n == index1 - 1) || (index0_n == index0 - 1 && index1_n == index1 + 1) || (index0_n == index0 - 1 && index1_n == index1 - 1)))
								) {
									if (vector(spin, index0_n, index1_n) == vector(spin, index0, index1)) {
										vector(energy, index0, index1) = vector(energy, index0, index1) - 1.0;
									} else {
										vector(energy, index0, index1) = vector(energy, index0, index1) + 1.0;
	
									}
								}
							}
						}
					}
				}
	
			}
	
	
		}
	}
	

	t_step->stop();


	return simPlat_dt;
}

/*
 * Checks the finalization conditions              
 */
bool Problem::checkFinalization(const double current_time, const double simPlat_dt)
{
	if (current_time > time_steps) { 
		return true;
	}
	return false;
	
	

}


bool Problem::neighbourTappering(const double* fov, const int xbase, const int ybase, const int xlast, const int ylast, const int stencil, const int limit) const {
	for(int y = stencil + 1; y <= limit; y++) {
		for(int x = stencil + 1; x <= limit; x++) {
			if ((xbase + x >= d_ghost_width && xbase + x < xlast - d_ghost_width && ybase + y >= d_ghost_width && ybase + y < ylast - d_ghost_width && vector(fov, xbase + x, ybase + y) > 0) || (xbase + x >= d_ghost_width && xbase + x < xlast - d_ghost_width && ybase - y >= d_ghost_width && ybase - y < ylast - d_ghost_width && vector(fov, xbase + x, ybase - y) > 0) || (xbase - x >= d_ghost_width && xbase - x < xlast - d_ghost_width && ybase + y >= d_ghost_width && ybase + y < ylast - d_ghost_width && vector(fov, xbase - x, ybase + y) > 0) || (xbase - x >= d_ghost_width && xbase - x < xlast - d_ghost_width && ybase - y >= d_ghost_width && ybase - y < ylast - d_ghost_width && vector(fov, xbase - x, ybase - y) > 0)) {
				return true;
			}
		}
	}
	return false;
}


/*
 *  Cell tagging routine - tag cells that require refinement based on a provided condition. 
 */
void Problem::applyGradientDetector(
   const std::shared_ptr< hier::PatchHierarchy >& hierarchy, 
   const int level_number,
   const double time, 
   const int tag_index,
   const bool initial_time,
   const bool uses_richardson_extrapolation_too) 
{
	if (d_regridding && level_number + 1 >= d_regridding_level_threshold) {
		hier::VariableDatabase *vdb = hier::VariableDatabase::getDatabase();
		if (!(vdb->checkVariableExists(d_regridding_field))) {
			TBOX_ERROR(d_object_name << ": Regridding field selected not found:n"					<<  d_regridding_field<<  "n");
		}

		std::shared_ptr< hier::PatchLevel > level(hierarchy->getPatchLevel(level_number));

		for (hier::PatchLevel::iterator ip(level->begin()); ip != level->end(); ++ip) {
			const std::shared_ptr< hier::Patch >& patch = *ip;
			int* tags = ((pdat::NodeData<int> *) patch->getPatchData(tag_index).get())->getPointer();
			const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));
			const double* dx  = patch_geom->getDx();
			const hier::Index tfirst = patch->getPatchData(tag_index)->getGhostBox().lower();
			const hier::Index tlast  = patch->getPatchData(tag_index)->getGhostBox().upper();
			const hier::Index boxfirst = patch->getBox().lower();
			const hier::Index boxlast  = patch->getBox().upper();
			int xlast = boxlast(0)-boxfirst(0)+2+2*d_ghost_width;
			int xtlast = tlast(0)-tfirst(0)+1;
			int ylast = boxlast(1)-boxfirst(1)+2+2*d_ghost_width;
			int ytlast = tlast(1)-tfirst(1)+1;
			if (d_regridding_type == "GRADIENT") {
				int regrid_field_id = vdb->getVariable(d_regridding_field)->getInstanceIdentifier();
				double* regrid_field = ((pdat::NodeData<double> *) patch->getPatchData(regrid_field_id).get())
->getPointer();
				for(int index1 = 0; index1 < (tlast(1)-tfirst(1))+1; index1++) {
					for(int index0 = 0; index0 < (tlast(0)-tfirst(0))+1; index0++) {
						vectorT(tags,index0,index1) = 0;
						if (vector(regrid_field, index0+d_ghost_width,index1+d_ghost_width)!=0) {
							if ((fabs(vector(regrid_field, index0+1+d_ghost_width, index1+d_ghost_width)-vector(regrid_field, index0+d_ghost_width, index1+d_ghost_width)) > d_regridding_compressionFactor * MIN(fabs(vector(regrid_field, index0+2+d_ghost_width, index1+d_ghost_width)-vector(regrid_field, index0+1+d_ghost_width, index1+d_ghost_width)) + d_regridding_mOffset * pow(dx[0], 2), fabs(vector(regrid_field, index0+d_ghost_width, index1+d_ghost_width)-vector(regrid_field, index0-1+d_ghost_width, index1+d_ghost_width)) + d_regridding_mOffset * pow(dx[0], 2)) ) ||(fabs(vector(regrid_field, index0+d_ghost_width, index1+1+d_ghost_width)-vector(regrid_field, index0+d_ghost_width, index1+d_ghost_width)) > d_regridding_compressionFactor * MIN(fabs(vector(regrid_field, index0+d_ghost_width, index1+2+d_ghost_width)-vector(regrid_field, index0+d_ghost_width, index1+1+d_ghost_width)) + d_regridding_mOffset * pow(dx[1], 2), fabs(vector(regrid_field, index0+d_ghost_width, index1+d_ghost_width)-vector(regrid_field, index0+d_ghost_width, index1-1+d_ghost_width)) + d_regridding_mOffset * pow(dx[1], 2)) ) ) {
								vectorT(tags,index0,index1) = 1;
							}
						}
					}
				}
			} else {
				if (d_regridding_type == "FUNCTION") {
					int regrid_field_id = vdb->getVariable(d_regridding_field)->getInstanceIdentifier();
					double* regrid_field = ((pdat::NodeData<double> *) patch->getPatchData(regrid_field_id).get())
	->getPointer();
					for(int index1 = 0; index1 < (tlast(1)-tfirst(1))+1; index1++) {
						for(int index0 = 0; index0 < (tlast(0)-tfirst(0))+1; index0++) {
							vectorT(tags,index0,index1) = 0;
							if (vector(regrid_field, index0+d_ghost_width,index1+d_ghost_width) > d_regridding_threshold) {
								vectorT(tags,index0,index1) = 1;
							}
						}
					}
	
				}
			}
		}
	}
}



