/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.codeGeneratorPlugin.utils;

import eu.iac3.mathMS.codeGeneratorPlugin.CGException;
import eu.iac3.mathMS.documentManagerPlugin.DMException;
import eu.iac3.mathMS.documentManagerPlugin.DocumentManager;
import eu.iac3.mathMS.documentManagerPlugin.DocumentManagerImpl;
import eu.iac3.mathMS.simflownyUtilsPlugin.SimflownyUtils;
import eu.iac3.mathMS.simflownyUtilsPlugin.SimflownyUtilsImpl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Set;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

/**
 * Utility class for SAMRAI code generation.
 * @author bminyano
 *
 */
public final class SAMRAIUtils {

    static String mtUri = "http://www.w3.org/1998/Math/MathML";
    static String mmsUri =  "urn:mathms";
    static String smlUri =  "urn:simml";
    static final String XSL_PDE = "common" + File.separator + "XSLTmathmlToC" + File.separator + "instructionToC.xsl";
    static final String XSL_ABM = "common" + File.separator + "XSLTABMToC" + File.separator + "instructionToC.xsl";
    static final String XSL_CA = "common" + File.separator + "XSLTCAonMeshToC" + File.separator + "instructionToC.xsl";
    static final String INDENT = "\u0009";
    static final String NEWLINE = System.getProperty("line.separator"); 
    static final int THREE = 3; 
    static Transformer transformerPDE = null;
    static Transformer transformerABM = null;
    static Transformer transformerCAGraph = null;
    static TransformerFactory tFactory = null;
    static SimflownyUtils su;
    static Transformer transformerSelected;
    
    /**
     * Private constructor.
     */
    public SAMRAIUtils() {
        try {
            tFactory = new net.sf.saxon.TransformerFactoryImpl();
        
            transformerABM = tFactory.newTransformer(new StreamSource(XSL_ABM));
            transformerPDE = tFactory.newTransformer(new StreamSource(XSL_PDE));
            transformerCAGraph = tFactory.newTransformer(new StreamSource(XSL_CA));
            
            su = new SimflownyUtilsImpl();
        } 
        catch (Exception e) {
            e.printStackTrace();
        }
    };
    
    /**
     * A static selection of the XSLT origin from the problem type.
     * For performance purposes.
     * @param problemType    The problem type
     */
    public static void selectXSLTransformer(String problemType) {
        if (problemType.equals("agentBasedProblemGraph")) {
            transformerSelected = transformerCAGraph;
        } 
        else {
            transformerSelected = transformerPDE;
        }
    }
    
    /**
     * Parses a xml with a xlst.
     * @param xml                   The xml to parse.
     * @param params                Params to send to the xsl.
     * @return                      the result to apply the xlt to the xml.
     * @throws CGException          CG00X External error
     */
    public static String xmlTransform(String xml, String[][] params) throws CGException {  
        try {
            ByteArrayInputStream input = new ByteArrayInputStream(xml.getBytes("UTF-8"));
            ByteArrayOutputStream output = new ByteArrayOutputStream();
                        
            // Set the parameters
            if (params != null) {
                for (int i = 0; i < params.length; i++) {
                    transformerSelected.setParameter(params[i][0], params[i][1]);
                }   
            }

            transformerSelected.transform(new StreamSource(input), new StreamResult(output));
            return output.toString("UTF-8");
        } 
        catch (Exception e) {
        	e.printStackTrace();
            throw new CGException(CGException.CG00X, e.getMessage());
        }
    }
    
    /**
     * Parses a xml with a xlst.
     * @param xml               The xml to parse.
     * @param params            Params to send to the xsl.
     * @return                  the result to apply the xlt to the xml.
     * @throws CGException      CG00X External error
     */
    public static String xmlTransform(Node xml, String[][] params) throws CGException {  
        try {
            return xmlTransform(CodeGeneratorUtils.domToString(xml), params);
        } 
        catch (Exception e) {
        	e.printStackTrace();
            throw new CGException(CGException.CG00X, e.getMessage());
        }
    }

    /**
     * Parses a xml with a xlst.
     * @param xml                           The xml to parse.
     * @param params                        Params to send to the xsl.
     * @param nodeParamName                 The node parameter name for the xslt
     * @param nodeParam                     The node parameter for the xslt
     * @return                              the result to apply the xlt to the xml.
     * @throws CGException                  CG00X External error
     */
    public static String xmlTransform(Node xml, String[][] params, String nodeParamName, Node nodeParam) throws CGException {  
        try {
            ByteArrayOutputStream output = new ByteArrayOutputStream();


            // Set the parameters
            if (params != null) {
                for (int i = 0; i < params.length; i++) {
                    transformerSelected.setParameter(params[i][0], params[i][1]);
                }   
            }

            if (nodeParamName != null) {
                transformerSelected.setParameter(nodeParamName, nodeParam);
            }
            
            DOMSource domSource = new DOMSource(xml);
            transformerSelected.transform(domSource, new StreamResult(output));
            return output.toString("UTF-8");
        } 
        catch (Exception e) {
            e.printStackTrace();
            throw new CGException(CGException.CG00X, e.getMessage());
        }
    }
    
    /**
     * Get the SAMRAI FVM type from the function parameter type.
     * @param type              The function parameter type
     * @param isDoubleArray     If the variable is a double pointer
     * @return                  SAMRAI type
     * @throws CGException      CG003 Not possible to create code for the platform
     */
    public static String getFVMType(String type, boolean isDoubleArray) throws CGException {
        if (type.toLowerCase().equals("field") || isDoubleArray) {
            return "double*";
        }
        if (type.toLowerCase().equals("int")) {
            return "int";
        }
        if (type.toLowerCase().equals("real") || type.toLowerCase().equals("function")) {
            return "double";
        }
        if (type.toLowerCase().equals("string")) {
            throw new CGException(CGException.CG003, "The type " + type.toLowerCase() + " is not permitted due to low time performance");
        }
        if (type.toLowerCase().equals("boolean")) {
            return "bool";
        }
        throw new CGException(CGException.CG003, "The type " + type.toLowerCase() + " is not permitted");
    }
    
    /**
     * Get the SAMRAI particles type from the function parameter type.
     * @param type              The function parameter type
     * @param dimensions        The number of spatial dimensions of the problem
     * @return                  SAMRAI type
     * @throws CGException      CG003 Not possible to create code for the platform
     */
    public static String getParticlesType(String type, int dimensions) throws CGException {
        if (type.toLowerCase().equals("int")) {
            return "int";
        }
        if (type.toLowerCase().equals("real")) {
            return "double";
        }
        if (type.toLowerCase().equals("string")) {
            throw new CGException(CGException.CG003, "The type " + type.toLowerCase() + " is not permitted due to low time performance");
        }
        if (type.toLowerCase().equals("boolean")) {
            return "bool";
        }
        if (type.toLowerCase().equals("field")) {
            return "std::shared_ptr< pdat::IndexData<Particles, pdat::CellGeometry > >";
        }
        throw new CGException(CGException.CG003, "The type " + type.toLowerCase() + " is not permitted");
    }
    
    /**
     * Creates an index for the spatial coordinates.
     * @param coordinates       The spatial coordinates
     * @param i                 Iteration i
     * @param j                 Iteration j
     * @param operation         Operation to apply (+ or -)
     * @param params            true if this index is for parameters or a normal index.
     * @return                  The code.
     */
    public static String createIndex(ArrayList<String> coordinates, int i, int j, String operation, boolean params) {
        String result = "";
        for (int k = 0; k < coordinates.size(); k++) {
            if (i == k) {
                if (params) {
                    result = result + coordinates.get(k) + " " + operation + " " + coordinates.get(k) + "Increment, " + coordinates.get(k) 
                        + "Direction, ";
                }
                else {
                    result = result + coordinates.get(k) + " " + operation + " " + (j + 1) + ", ";
                }
            }
            else {
                if (params) {
                    result = result + coordinates.get(k) + ", " + coordinates.get(k) + "Direction, ";
                }
                else {
                    result = result + coordinates.get(k) + ", "; 
                }
            }
        }
        return result.substring(0, result.lastIndexOf(","));
    }
    
    /**
     * Normalize the name of a variable with C rules and according entities.xsl.
     * @param variable          The original name of the variable
     * @return                  The variable normalized.
     * @throws CGException      CG00X External error
     */
    public static String variableNormalize(String variable) throws CGException {
        try {
            //Replace the greek characters. Process with xslt
            variable = "<mt:math xmlns:mt=\"http://www.w3.org/1998/Math/MathML\"><mt:ci>" + variable + "</mt:ci></mt:math>";
            variable = xmlTransform(variable, null);
            
            //The first character must be a letter
            while (CodeGeneratorUtils.isNumber(variable.substring(0, 1))) {
                variable = variable.substring(1);
            }
            //The remaining characters, if any, may be letters, digits, or underscores
            variable = variable.replaceAll("[^a-zA-Z0-9_]", "");
            
            return variable;
        }
        catch (Exception e) {
            throw new CGException(CGException.CG00X, e.getMessage());
        }
    }
    
    /**
     * Preprocess the problem to adapt to special simPlat issues.
     * 1 - add dx and simPlat_dt to the parameters on function calls
     * 2 - current iteration number
     * 3 - grid values replacement
     * 4a - coordinate usage to mesh or particle position
     * 4 - continuous coordinate usage
     * 5 - delta substitution with dx array or particle_separation
     * 6 - delta t substitution with simPlat_dt
     * 7 - Format numbers
     * 8 - tag currentTimeStep to current_time
     * 9 - tags ratio, substepNumber, newTime
     * 10 - Replace unnecessary namespaces in ci tags
     * 11 - change temporal variables for all the regions (Interior and Surface type too) 
     * 		Change analysis variable names to use evolution variables to safe memory (performance)
     * 12 - Replace regionInteriorId and regionSurfaceId tags
     * 13 - Replace particleSeparationProduct tags (only particles)
     * 14 - Replace influenceRadius tags (only particles)
     * 15 - Replace iterationNumber tags
     * 16 - Replace stencil tags
     * 17 - Extrapolation FOV processing
     * 19 - Region movement preprocess
     * 20 - Hard region distance variables simplification
     * 21 - Particle Movement
     * @param problemDoc        The problem
     * @param pi                The problem information
     * @param fvm               If the preprocess is a fvm or particles code
     * @return                  The problem ready to be processed
     * @throws CGException      CG004 External error
     */
    public static Document preprocess(Document problemDoc, ProblemInfo pi, boolean fvm) 
        throws CGException {
        //4a - coordinate usage to mesh or particle position
        String timeCoordinate = CodeGeneratorUtils.find(problemDoc, "/*/mms:coordinates/mms:timeCoordinate").item(0).getTextContent();
        for (int i = 0; i < pi.getCoordinates().size(); i++) {
        	String discCoord = pi.getCoordinates().get(i);
        	String contCoord = CodeGeneratorUtils.discToCont(problemDoc, discCoord);
        	//Mesh
        	NodeList contCoordinates = CodeGeneratorUtils.find(problemDoc, "//mt:math[not(ancestor::sml:iterateOverParticles)]//mt:ci[text() = '" + contCoord + "']");
            String importString = "<mt:math xmlns:mt=\"http://www.w3.org/1998/Math/MathML\" xmlns:sml=\"urn:simml\" ><mt:apply>"
                    + "<mt:ci>" + contCoord + "coord</mt:ci><mt:ci>" + discCoord + "</mt:ci></mt:apply></mt:math>";
            Document importTransform = CodeGeneratorUtils.stringToDom(importString);
            Element newNode = (Element) problemDoc.importNode(importTransform.getDocumentElement().getFirstChild(), true);
            for (int j = contCoordinates.getLength() - 1; j >= 0; j--) {
            	contCoordinates.item(j).getParentNode().replaceChild(newNode.cloneNode(true), contCoordinates.item(j));
            }
        	//Particles
        	contCoordinates = CodeGeneratorUtils.find(problemDoc, "//mt:math[ancestor::sml:iterateOverParticles]//mt:ci[text() = '" + contCoord + "']");
            newNode = CodeGeneratorUtils.createElement(problemDoc, mtUri, "ci");
            Element particlePosition = CodeGeneratorUtils.createElement(problemDoc, smlUri, "particlePosition");
            particlePosition.setTextContent(discCoord);
            Element coordinate = CodeGeneratorUtils.createElement(problemDoc, mtUri, "ci");
            Element time = CodeGeneratorUtils.createElement(problemDoc, mtUri, "ci");
            Element apply = CodeGeneratorUtils.createElement(problemDoc, mtUri, "apply");
            Element msup = CodeGeneratorUtils.createElement(problemDoc, mtUri, "msup");
            Element plus = CodeGeneratorUtils.createElement(problemDoc, mtUri, "plus");
            Element one = CodeGeneratorUtils.createElement(problemDoc, mtUri, "cn");
            coordinate.appendChild(particlePosition);
            time.setTextContent("(" + timeCoordinate + ")");
            one.setTextContent("1");
            newNode.appendChild(msup);
            msup.appendChild(coordinate);
            msup.appendChild(apply);
            apply.appendChild(plus);
            apply.appendChild(time);
            apply.appendChild(one);
            for (int j = contCoordinates.getLength() - 1; j >= 0; j--) {
            	contCoordinates.item(j).getParentNode().replaceChild(newNode.cloneNode(true), contCoordinates.item(j));
            }
        }
        //1 - add dx and simPlat_dt to the parameters on function calls
        Element dx = CodeGeneratorUtils.createElement(problemDoc, mtUri, "ci");
        dx.setTextContent("dx");
        Element simPlatDt = CodeGeneratorUtils.createElement(problemDoc, mtUri, "ci");
        simPlatDt.setTextContent("simPlat_dt");
        Element ilast = CodeGeneratorUtils.createElement(problemDoc, mtUri, "ci");
        ilast.setTextContent("ilast");
        Element jlast = CodeGeneratorUtils.createElement(problemDoc, mtUri, "ci");
        jlast.setTextContent("jlast");
        NodeList functions = CodeGeneratorUtils.find(problemDoc, ".//sml:functionCall|.//sml:flux");
        for (int j = 0; j < functions.getLength(); j++) {
            functions.item(j).appendChild(dx.cloneNode(true));
            if (!functions.item(j).getFirstChild().getTextContent().startsWith("extrapolate_")) {
                functions.item(j).appendChild(simPlatDt.cloneNode(true));
            }
            if (fvm) {
                functions.item(j).appendChild(ilast.cloneNode(true));
                functions.item(j).appendChild(jlast.cloneNode(true));
            }
        }
        //2 - Replace the value of the time coordinate with the current iteration number.
        NodeList found = CodeGeneratorUtils.find(problemDoc, "//mt:ci[text() = '(" + timeCoordinate + ")' and not(ancestor::mt:msup)]");
        Element iteration = CodeGeneratorUtils.createElement(problemDoc, mtUri, "apply");
        Element div = CodeGeneratorUtils.createElement(problemDoc, mtUri, "divide");
        Element time = CodeGeneratorUtils.createElement(problemDoc, mtUri, "ci");
        time.setTextContent("current_time");
        Element dt = CodeGeneratorUtils.createElement(problemDoc, mtUri, "ci");
        dt.setTextContent("simPlat_dt");
        iteration.appendChild(div);
        iteration.appendChild(time);
        iteration.appendChild(dt);
        for (int i = 0; i < found.getLength(); i++) {
            found.item(i).getParentNode().replaceChild(iteration.cloneNode(true), found.item(i));
        }
        found = CodeGeneratorUtils.find(problemDoc, "//sml:timeCoordinate[not(ancestor::mms:inputVariable or ancestor::mms:outputVariable)]");
        time = CodeGeneratorUtils.createElement(problemDoc, mtUri, "ci");
        time.setTextContent("interp_time");
        for (int i = 0; i < found.getLength(); i++) {
            found.item(i).getParentNode().replaceChild(time.cloneNode(true), found.item(i));
        }
        found = CodeGeneratorUtils.find(problemDoc, "//mt:ci[sml:timeCoordinate[ancestor::mms:inputVariable or ancestor::mms:outputVariable]]");
        time = CodeGeneratorUtils.createElement(problemDoc, mtUri, "ci");
        time.setTextContent("(" + pi.getTimeCoord() + ")");
        for (int i = 0; i < found.getLength(); i++) {
            found.item(i).getParentNode().replaceChild(time.cloneNode(true), found.item(i));
        }
        //3 - Grid value tags replacement
        String query = "//*[local-name() = 'minRegionValue' or local-name() = 'maxRegionValue']";
        NodeList minGridValues = CodeGeneratorUtils.find(problemDoc, query);
        for (int i = minGridValues.getLength() - 1; i >= 0; i--) {
            String coord = minGridValues.item(i).getTextContent();
            minGridValues.item(i).setTextContent(String.valueOf(pi.getCoordinates().indexOf(coord)));
        }
        //4 - continuous coordinate usage
        //5 - delta substitution with dx array or particle_separation
        for (int i = 0; i < pi.getCoordinates().size(); i++) {
        	String contCoord = CodeGeneratorUtils.discToCont(problemDoc, pi.getCoordinates().get(i));
	        NodeList deltaUse = CodeGeneratorUtils.find(problemDoc,
	                "//sml:cellDeltaSpacing[text() = '" + contCoord + "']");
	        for (int j = 0; j < deltaUse.getLength(); j++) {
	        	deltaUse.item(j).setTextContent("dx[" + i + "]");
	        }
	        deltaUse = CodeGeneratorUtils.find(problemDoc,
	                "//sml:particleDeltaSpacing[text() = '" + contCoord + "']");
	        for (int j = 0; j < deltaUse.getLength(); j++) {
	            deltaUse.item(j).setTextContent("particleSeparation_" + contCoord);
	        }
        }
        NodeList deltas = CodeGeneratorUtils.find(problemDoc, "/*/mms:implicitParameters/mms:implicitParameter[mms:type ='delta_space']");
        for (int i = 0; i < deltas.getLength(); i++) {
            if (fvm) {
                //4 - continuous spatial coordinate usage
                //Node to search
                Element apply = CodeGeneratorUtils.createElement(problemDoc, mtUri, "apply");
                Element times = CodeGeneratorUtils.createElement(problemDoc, mtUri, "times");
                Element coord = CodeGeneratorUtils.createElement(problemDoc, mtUri, "ci");
                coord.setTextContent(deltas.item(i).getLastChild().getTextContent());
                Element delta = CodeGeneratorUtils.createElement(problemDoc, mtUri, "ci");
                delta.setTextContent(deltas.item(i).getFirstChild().getNextSibling().getTextContent());
                apply.appendChild(times);
                apply.appendChild(coord);
                apply.appendChild(delta);
                //New node
                Element newApply = createDecrementElement(problemDoc, (Element) deltas.item(i), i, pi.getSpatialDimensions(), fvm);

                //Search and replace in the problem
                CodeGeneratorUtils.nodeReplace(problemDoc.getDocumentElement(), apply, newApply);
            }
            //5 - delta substitution with dx array or particle_separation
            NodeList deltaUse = CodeGeneratorUtils.find(problemDoc,
                    "//mt:ci[text() = '" + deltas.item(i).getFirstChild().getNextSibling().getTextContent() + "' and not(ancestor::sml:iterateOverParticles)]");
            for (int j = 0; j < deltaUse.getLength(); j++) {
            	deltaUse.item(j).setTextContent("dx[" + (i % pi.getSpatialDimensions()) + "]");
            }
            deltaUse = CodeGeneratorUtils.find(problemDoc,
                    "//mt:ci[text() = '" + deltas.item(i).getFirstChild().getNextSibling().getTextContent() + "' and ancestor::sml:iterateOverParticles]");
            for (int j = 0; j < deltaUse.getLength(); j++) {
                deltaUse.item(j).setTextContent("particleSeparation_" 
                        + deltas.item(i).getFirstChild().getNextSibling().getTextContent().substring(1));
            }
            //Replaces in CellPosition
            Element applyPlus = CodeGeneratorUtils.createElement(problemDoc, mtUri, "apply");
            Element plus = CodeGeneratorUtils.createElement(problemDoc, mtUri, "plus");
            Element minRegionValue = CodeGeneratorUtils.createElement(problemDoc, smlUri, "minRegionValue");
            minRegionValue.setTextContent(String.valueOf(pi.getCoordinates().indexOf(deltas.item(i).getLastChild().getTextContent())));
            
         
            Element newApply = CodeGeneratorUtils.createElement(problemDoc, mtUri, "apply");
            Element newTimes = CodeGeneratorUtils.createElement(problemDoc, mtUri, "times");
            Element newApplyMinus = CodeGeneratorUtils.createElement(problemDoc, mtUri, "apply");
            Element newMinus = CodeGeneratorUtils.createElement(problemDoc, mtUri, "minus");
            Element newApplyPlus = CodeGeneratorUtils.createElement(problemDoc, mtUri, "apply");
            Element newPlus = CodeGeneratorUtils.createElement(problemDoc, mtUri, "plus");
            Element newCoord = CodeGeneratorUtils.createElement(problemDoc, mtUri, "ci");
            newCoord.setTextContent(deltas.item(i).getLastChild().getTextContent());
            Element newApplyIfirst = CodeGeneratorUtils.createElement(problemDoc, mtUri, "apply");
            Element newIfirst = CodeGeneratorUtils.createElement(problemDoc, mtUri, "ci");
            newIfirst.setTextContent("boxfirst");
            Element newIfirstIndex = CodeGeneratorUtils.createElement(problemDoc, mtUri, "cn");
            newIfirstIndex.setTextContent(String.valueOf(i % pi.getSpatialDimensions()));
            Element newStencil = CodeGeneratorUtils.createElement(problemDoc, mtUri, "ci");
            newStencil.setTextContent("d_ghost_width");
            Element newDelta = CodeGeneratorUtils.createElement(problemDoc, mtUri, "ci");
            newDelta.setTextContent("dx[" + (i % pi.getSpatialDimensions()) + "]");
            newApply.appendChild(newTimes);
            newApply.appendChild(newApplyMinus);
            newApply.appendChild(newDelta);
            newApplyMinus.appendChild(newMinus);
            newApplyMinus.appendChild(newApplyPlus);
            newApplyMinus.appendChild(newStencil);
            newApplyPlus.appendChild(newPlus);
            newApplyPlus.appendChild(newCoord);
            newApplyPlus.appendChild(newApplyIfirst);
            newApplyIfirst.appendChild(newIfirst);
            newApplyIfirst.appendChild(newIfirstIndex);
            
            applyPlus.appendChild(plus);
            applyPlus.appendChild(minRegionValue);
            applyPlus.appendChild(newApply);
            
            NodeList cellPosition = CodeGeneratorUtils.find(problemDoc,
                    "//sml:cellPosition[text() = '" + deltas.item(i).getLastChild().getTextContent() + "']");
            for (int j = cellPosition.getLength() - 1; j >= 0; j--) {
            	cellPosition.item(j).getParentNode().replaceChild(applyPlus.cloneNode(true), cellPosition.item(j));
            }
        }
        //4 - continuous time coordinate usage
        deltas = CodeGeneratorUtils.find(problemDoc, "/*/mms:implicitParameters/mms:implicitParameter[mms:type ='delta_time']");
        for (int i = 0; i < deltas.getLength(); i++) {
            Element coord = CodeGeneratorUtils.createElement(problemDoc, mtUri, "ci");
            coord.setTextContent("(" + deltas.item(i).getLastChild().getTextContent() + ")");
            Element ci = CodeGeneratorUtils.createElement(problemDoc, mtUri, "ci");
            Element timeCoordinateElement = CodeGeneratorUtils.createElement(problemDoc, smlUri, "timeCoordinate");
            ci.appendChild(timeCoordinateElement);
        	CodeGeneratorUtils.nodeReplace(problemDoc.getDocumentElement(), ci, coord);
        }
        //6 - delta t  substitution with simPlat_dt
        deltaTReplacement(problemDoc);
        //7 - Format numbers
        formatNumbers(problemDoc);
        //8 - Change tag currentTime to cctk_time
        NodeList currentTimes = CodeGeneratorUtils.find(problemDoc, "//sml:currentTime");
        for (int i = currentTimes.getLength() - 1; i >= 0; i--) {
            Element ci = CodeGeneratorUtils.createElement(problemDoc, mtUri, "ci");
            ci.setTextContent("current_time");
            currentTimes.item(i).getParentNode().replaceChild(ci, currentTimes.item(i));
        }
        problemDoc.getDocumentElement().setAttribute("xmlns:mt", "http://www.w3.org/1998/Math/MathML");
        //9 - tags ratio, substepNumber, newTime
        NodeList tags = CodeGeneratorUtils.find(problemDoc, "//sml:newTime");
        Element ci = CodeGeneratorUtils.createElement(problemDoc, mtUri, "ci");
        ci.setTextContent("new_time");
        for (int i = tags.getLength() - 1; i >= 0; i--) {
        	tags.item(i).getParentNode().replaceChild(ci.cloneNode(true), tags.item(i));
        }
        tags = CodeGeneratorUtils.find(problemDoc, "//sml:substepNumber");
        ci = CodeGeneratorUtils.createElement(problemDoc, mtUri, "ci");
        ci.setTextContent("interp_step");
        for (int i = tags.getLength() - 1; i >= 0; i--) {
        	tags.item(i).getParentNode().replaceChild(ci.cloneNode(true), tags.item(i));
        }
        tags = CodeGeneratorUtils.find(problemDoc, "//sml:refinementRatio");
        ci = CodeGeneratorUtils.createElement(problemDoc, mtUri, "ci");
        ci.setTextContent("interp_ratio");
        for (int i = tags.getLength() - 1; i >= 0; i--) {
        	tags.item(i).getParentNode().replaceChild(ci.cloneNode(true), tags.item(i));
        }
        //16 - Replace stencil tags
        NodeList stencil = CodeGeneratorUtils.find(problemDoc, "//sml:stencil");
        for (int i = stencil.getLength() - 1; i >= 0; i--) {
            Element cn = CodeGeneratorUtils.createElement(problemDoc, mtUri, "cn");
            cn.setTextContent(String.valueOf(pi.getSchemaStencil()));
            stencil.item(i).getParentNode().replaceChild(cn, stencil.item(i));
        }
        //10 - Replace unnecessary namespaces in ci tags
        String problemDocString = CodeGeneratorUtils.domToString(problemDoc);
        problemDocString = problemDocString.replaceAll("<mt:ci xmlns:mt=\"http://www.w3.org/1998/Math/MathML\">", "<mt:ci>");        
        Document newDoc = CodeGeneratorUtils.stringToDom(problemDocString);
        //17 - Extrapolation FOV processing
        CodeGeneratorUtils.exFOVProcessing(newDoc, pi);
        //11 - change temporal variables names for all the regions (Interior and Surface type too) 
        //Change analysis variable names to use evolution variables to save memory (performance)
        CodeGeneratorUtils.changeVariableNames(newDoc, pi, true);
        //12 - Replace regionInteriorId and regionSurfaceId tags
        CodeGeneratorUtils.replaceRegionIdTags(newDoc, pi);
        //13 - replace particleSeparationProduct tags 
        replaceParticleSeparationProductTags(problemDoc, newDoc, pi.getCoordinates());
        //15 - Replace iterationNumber tags
        NodeList iterationNumber = CodeGeneratorUtils.find(newDoc, ".//sml:iterationNumber");
        Element iterationElement = CodeGeneratorUtils.createElement(newDoc, mtUri, "ci");
        iterationElement.setTextContent("simPlat_iteration");
        for (int j = 0; j < iterationNumber.getLength(); j++) {
            iterationNumber.item(j).getParentNode().replaceChild(iterationElement.cloneNode(true), iterationNumber.item(j));
        }
        //19 - Region movement preprocess
       // segMovPreprocess(newDoc, pi);
        //20 - Hard region distance variables simplification
        simplifyHardRegionDistances(newDoc, pi);
        //21 - Particle Movement
        processParticleMovement(newDoc, pi);
        return newDoc;
    }
    
    /**
     * Preprocess particleMovement tag to add parameters to the call.
     * @param problem			The problem to simplify
     * @param pi				The process info
     * @throws CGException		CG00X External error
     */
    private static void processParticleMovement(Document problem, ProblemInfo pi) throws CGException {
    	for (String speciesName: pi.getParticleSpecies()) {
    		NodeList movements = CodeGeneratorUtils.find(problem, "//sml:moveParticles[ancestor::sml:iterateOverParticles[@speciesNameAtt = '" + speciesName + "']]");
            for (int i = movements.getLength() - 1; i >= 0; i--) {
            	Element moveParticles = (Element) movements.item(i);
    	        Element math = CodeGeneratorUtils.createElement(problem, mtUri, "math");
    	        Element apply = CodeGeneratorUtils.createElement(problem, mtUri, "apply");
    	        Element move = CodeGeneratorUtils.createElement(problem, mtUri, "ci");
    	        move.setTextContent("moveParticles<Particle_" + speciesName + ">");
    	        Element patch = CodeGeneratorUtils.createElement(problem, mtUri, "ci");
    	        patch.setTextContent("patch");
    	        Element particleVariables = CodeGeneratorUtils.createElement(problem, mtUri, "ci");
    	        particleVariables.setTextContent("particleVariables_" + speciesName);
    	        Element part = CodeGeneratorUtils.createElement(problem, mtUri, "ci");
    	        part.setTextContent("part_" + speciesName);
    	        Element particle = CodeGeneratorUtils.createElement(problem, mtUri, "ci");
    	        particle.setTextContent("particle");
    	        apply.appendChild(move);
    	        apply.appendChild(patch);
    	        apply.appendChild(particleVariables);
    	        apply.appendChild(part);
    	        apply.appendChild(particle);
    	        for (int k = 0; k < pi.getCoordinates().size(); k++) {
    	        	Element indexApply = CodeGeneratorUtils.createElement(problem, mtUri, "apply");
    	        	Element plus = CodeGeneratorUtils.createElement(problem, mtUri, "plus");
    	        	Element index = CodeGeneratorUtils.createElement(problem, mtUri, "ci");
    	            index.setTextContent(pi.getCoordinates().get(k));
    	            Element boxfirstApply = CodeGeneratorUtils.createElement(problem, mtUri, "apply");
    	            Element boxfirst = CodeGeneratorUtils.createElement(problem, mtUri, "ci");
    	            boxfirst.setTextContent("boxfirstP");
    	            Element bfIndex = CodeGeneratorUtils.createElement(problem, mtUri, "cn");
    	            bfIndex.setTextContent(String.valueOf(k));
    	            boxfirstApply.appendChild(boxfirst);
    	            boxfirstApply.appendChild(bfIndex);
    	            indexApply.appendChild(plus);
    	            indexApply.appendChild(index);
    	            indexApply.appendChild(boxfirstApply);
    	            apply.appendChild(indexApply);
    	        }
    	        NodeList positions = moveParticles.getChildNodes();
    	        for (int k = 0; k < positions.getLength(); k++) {
    	            apply.appendChild(positions.item(k).getFirstChild().cloneNode(true));
    	        }
    	        Element simPlatDt = CodeGeneratorUtils.createElement(problem, mtUri, "ci");
    	        simPlatDt.setTextContent("simPlat_dt");
    	        apply.appendChild(simPlatDt);
    	        Element simPlatTime = CodeGeneratorUtils.createElement(problem, mtUri, "ci");
    	        simPlatTime.setTextContent("current_time");
    	        apply.appendChild(simPlatTime);
    	        Element pit = CodeGeneratorUtils.createElement(problem, mtUri, "ci");
    	        pit.setTextContent("pit");
    	        apply.appendChild(pit);
    	        //Particle step (count number of movement preceding)
    	        Element step = CodeGeneratorUtils.createElement(problem, mtUri, "cn");
    	        Element tmp = (Element) moveParticles.getParentNode();
    	        while (!tmp.getLocalName().equals("iterateOverCells")) {
    	        	tmp = (Element) tmp.getParentNode();
    	        }
    	        int counter = 1;
    	        while (tmp.getPreviousSibling() != null) {
    	        	tmp = (Element) tmp.getPreviousSibling();
    	        	if (tmp.getLocalName().equals("iterateOverCells") && tmp.getElementsByTagNameNS(smlUri, "moveParticles").getLength() > 0) {
    	        		counter++;
    	        	}
    	        }
    	        step.setTextContent(String.valueOf(counter));
    	        apply.appendChild(step);
    	        //Level influence radius (AMR on particles)
    	        Element levelInfluenceRadius = CodeGeneratorUtils.createElement(problem, mtUri, "ci");
    	        levelInfluenceRadius.setTextContent("level_influenceRadius");
    	        apply.appendChild(levelInfluenceRadius);
    	        
    	        math.appendChild(apply);
    	        moveParticles.getParentNode().replaceChild(math, moveParticles);
            }
    	}
    }
    
    /**
     * Simplifies the distance variables of hard regions to reduce RAM in simulations.
     * @param problem			The problem to simplify
     * @param pi				The process info
     * @throws CGException		CG00X External error
     */
    public static void simplifyHardRegionDistances(Document problem, ProblemInfo pi) throws CGException {
    	LinkedHashMap<String, ArrayList<String>> groups = pi.getExtrapolatedFieldGroups();
    	if (pi.getExtrapolatedFieldGroups() != null) {
        	Iterator<String> groupsIt = groups.keySet().iterator();
        	while (groupsIt.hasNext()) {
        		String groupName = groupsIt.next();
        		ArrayList<String> fields = groups.get(groupName);
        		for (int i = 0; i < fields.size(); i++) {
        			String field = fields.get(i);
        			for (int j = 0; j < pi.getDimensions(); j++) {
        				String coord = pi.getCoordinates().get(j);
        				NodeList distances = CodeGeneratorUtils.find(problem, "//mt:ci[text() = 'd_" + coord + "_" + field + "']");
        				for (int k = 0; k < distances.getLength(); k++) {
        					distances.item(k).setTextContent("d_" + coord + "_" + groupName);
        				}
        			}
        		}
        	}
    	}
    }
   
    /**
     * Preprocess for region movement.
     * 1 - Accumulate fields and shared variables proportional to the FOV
     * 2 - Accumulate FOVs
     * 3 - Extrapolate functions modification
     * @param problem           The problem
     * @param pi                The problem info
     * @throws CGException      CG00X External error
     */
   /* public static void segMovPreprocess(Document problem, ProblemInfo pi) throws CGException {
        if (pi.getMovementInfo().itMoves()) {
            for (int i = 0; i < pi.getRegions().size(); i++) {
                String segName = pi.getRegions().get(i);
                //1 - Accumulate fields and shared variables proportional to the FOV
                //2 - Accumulate FOVs
                NodeList leftTerms = CodeGeneratorUtils.find(problem, "//mms:regionGroup[mms:regionGroupName = '"
                        + segName + "']//mms:interiorExecutionFlow//mt:math[not(ancestor::sml:boundary)]/mt:apply/mt:eq/following-sibling::*[1]" 
                        + "[name()='sml:sharedVariable' and mt:apply]");
                accFieldsFovs(problem, leftTerms, (2 * i + 1), pi);
                leftTerms = CodeGeneratorUtils.find(problem, "//mms:regionGroup[mms:regionGroupName = '"
                        + segName + "']//mms:surfaceExecutionFlow//mt:math[not(ancestor::sml:boundary)]/mt:apply/mt:eq/following-sibling::*[1]" 
                        + "[name()='sml:sharedVariable' and mt:apply]");
                accFieldsFovs(problem, leftTerms, (2 * i + 2), pi);
                Iterator<String> fields = pi.getFieldTimeLevels().keySet().iterator();
                while (fields.hasNext()) {
                    String field = fields.next();
                    leftTerms = CodeGeneratorUtils.find(problem, "//mms:regionGroup[mms:regionGroupName = '"
                            + segName + "']//mms:interiorExecutionFlow//mt:math[not(ancestor::sml:boundary)]/mt:apply/mt:eq/following-sibling::*[1]" 
                            + "[name()='mt:apply' and mt:ci/mt:msup[mt:ci = '" + field + "' and mt:apply[mt:ci = '(" 
                            + pi.getTimeCoord() + ")' and mt:plus and mt:cn[text() = '1']]]]");
                    accFieldsFovs(problem, leftTerms, (2 * i + 1), pi);
                    leftTerms = CodeGeneratorUtils.find(problem, "//mms:regionGroup[mms:regionGroupName = '"
                            + segName + "']//mms:surfaceExecutionFlow//mt:math[not(ancestor::sml:boundary)]/mt:apply/mt:eq/following-sibling::*[1]" 
                            + "[name()='mt:apply' and mt:ci/mt:msup[mt:ci = '" + field + "' and mt:apply[mt:ci = '(" 
                            + pi.getTimeCoord() + ")' and mt:plus and mt:cn[text() = '1']]]]");
                    accFieldsFovs(problem, leftTerms, (2 * i + 2), pi);
                }
            }
            //3 - Extrapolate functions modification
            NodeList functions = CodeGeneratorUtils.find(problem, "//mms:function[starts-with(" 
                    + "mms:functionName, 'extrapolate_')]/mms:functionParameters");
            for (int i = 0; i < functions.getLength(); i++) {
                //Add stalled parameter
                Element functionParameter = CodeGeneratorUtils.createElement(problem, mmsUri, "functionParameter");
                Element name = CodeGeneratorUtils.createElement(problem, mmsUri, "name");
                name.setTextContent("Stalled");
                Element type = CodeGeneratorUtils.createElement(problem, mmsUri, "type");
                type.setTextContent("field");
                functionParameter.appendChild(name);
                functionParameter.appendChild(type);
                functions.item(i).appendChild(functionParameter);
                
                int fovs = CodeGeneratorUtils.find(functions.item(i), "./mms:functionParameter["
                        + "mms:name[starts-with(text(), 'FOV')]]").getLength();
                
                //Sumarize FOVs
                Element applyFov = CodeGeneratorUtils.createElement(problem, mtUri, "apply");
                if (fovs == 1) {
                    Element fov = CodeGeneratorUtils.createElement(problem, mtUri, "ci");
                    fov.setTextContent("FOV");
                    applyFov.appendChild(fov);
                    applyFov.appendChild(CodeGeneratorUtils.deployCoordinates(problem, pi.getCoordinates()));
                }
                else {
                    Element apply = CodeGeneratorUtils.createElement(problem, mtUri, "apply");
                    for (int j = 0; j < fovs; j++) {
                        Element applytmp = CodeGeneratorUtils.createElement(problem, mtUri, "apply");
                        Element fov = CodeGeneratorUtils.createElement(problem, mtUri, "ci");
                        fov.setTextContent("FOV_" + j);
                        applytmp.appendChild(fov);
                        applytmp.appendChild(CodeGeneratorUtils.deployCoordinates(problem, pi.getCoordinates()));
                        if (j == 0) {
                            apply = applytmp;
                        }
                        else {
                            Element applyplus = CodeGeneratorUtils.createElement(problem, mtUri, "apply");
                            Element plus = CodeGeneratorUtils.createElement(problem, mtUri, "plus");
                            applyplus.appendChild(plus);
                            applyplus.appendChild(apply.cloneNode(true));
                            applyplus.appendChild(applytmp);
                            apply = applyplus;
                        }
                    }
                    applyFov = apply;
                }
                
                //Modify extrapolation assignment
                for (int j = 0; j < pi.getCoordinates().size(); j++) {
                    String coord = pi.getCoordinates().get(j);
                    NodeList assignments = CodeGeneratorUtils.find(functions.item(i).getParentNode(), ".//mt:math[mt:apply/mt:eq/"
                            + "following-sibling::*[1][name()='mt:apply' and (descendant::mt:ci = 'to_" + coord + "_f')]]");
                    Element assignment = (Element) assignments.item(0);
                    Element ifElem = CodeGeneratorUtils.createElement(problem, smlUri, "if");
                    Element then = CodeGeneratorUtils.createElement(problem, smlUri, "then");
                    Element elseElem = CodeGeneratorUtils.createElement(problem, smlUri, "else");
                    Element cond = CodeGeneratorUtils.createElement(problem, mtUri, "math");
                    Element condApply = CodeGeneratorUtils.createElement(problem, mtUri, "apply");
                    Element ciApply = CodeGeneratorUtils.createElement(problem, mtUri, "ci");
                    ciApply.setTextContent("Stalled");
                    ifElem.appendChild(cond);
                    ifElem.appendChild(then);
                    ifElem.appendChild(elseElem);
                    cond.appendChild(condApply);
                    condApply.appendChild(ciApply);
                    condApply.appendChild(CodeGeneratorUtils.deployCoordinates(problem, pi.getCoordinates()));
                    then.appendChild(assignment.cloneNode(true));
                    Element math = CodeGeneratorUtils.createElement(problem, mtUri, "math");
                    Element apply = CodeGeneratorUtils.createElement(problem, mtUri, "apply");
                    Element eq = CodeGeneratorUtils.createElement(problem, mtUri, "eq");
                    Element plus = CodeGeneratorUtils.createElement(problem, mtUri, "plus");
                    Element apply2 = CodeGeneratorUtils.createElement(problem, mtUri, "apply");
                    Element apply3 = CodeGeneratorUtils.createElement(problem, mtUri, "apply");
                    Element times = CodeGeneratorUtils.createElement(problem, mtUri, "times");
                    Element apply4 = CodeGeneratorUtils.createElement(problem, mtUri, "apply");
                    Element divide = CodeGeneratorUtils.createElement(problem, mtUri, "divide");
                    Element hundred = CodeGeneratorUtils.createElement(problem, mtUri, "cn");
                    hundred.setTextContent("100.0");
                    Element apply5 = CodeGeneratorUtils.createElement(problem, mtUri, "apply");
                    Element apply6 = CodeGeneratorUtils.createElement(problem, mtUri, "apply");
                    Element apply7 = CodeGeneratorUtils.createElement(problem, mtUri, "apply");
                    Element minus = CodeGeneratorUtils.createElement(problem, mtUri, "minus");
                    math.appendChild(apply);
                    apply.appendChild(eq);
                    apply.appendChild(assignment.getFirstChild().getFirstChild().getNextSibling().cloneNode(true));
                    apply.appendChild(apply2);
                    apply2.appendChild(plus);
                    apply2.appendChild(apply3);
                    apply2.appendChild(apply5);
                    apply3.appendChild(times);
                    apply3.appendChild(apply4);
                    apply3.appendChild(assignment.getFirstChild().getFirstChild().getNextSibling().cloneNode(true));
                    apply4.appendChild(divide);
                    apply4.appendChild(applyFov.cloneNode(true));
                    apply4.appendChild(hundred);
                    apply5.appendChild(times.cloneNode(true));
                    apply5.appendChild(apply6);
                    apply5.appendChild(assignment.getFirstChild().getLastChild().cloneNode(true));
                    apply6.appendChild(divide.cloneNode(true));
                    apply6.appendChild(apply7);
                    apply6.appendChild(hundred.cloneNode(true));
                    apply7.appendChild(minus);
                    apply7.appendChild(hundred.cloneNode(true));
                    apply7.appendChild(applyFov.cloneNode(true));
                    elseElem.appendChild(math);
                    
                    assignment.getParentNode().replaceChild(ifElem, assignment);
                }
            }
            NodeList calls = CodeGeneratorUtils.find(problem, "//mt:math/*[local-name() = 'functionCall' and (mt:ci[starts-with(text(), "
                    + "'extrapolate_field') or starts-with(text(), 'extrapolate_flux')])]");
            for (int j = 0; j < calls.getLength(); j++) {
                Element stalled = CodeGeneratorUtils.createElement(problem, mtUri, "ci");
                NodeList fovs = CodeGeneratorUtils.find(calls.item(j), "./mt:ci[starts-with(text(), 'FOV_')]");
                Node fov = fovs.item(fovs.getLength() - 1);
                int segId = Integer.parseInt(fov.getTextContent().substring(fov.getTextContent().lastIndexOf("_") + 1));
                if (segId % 2 == 0) {
                    segId--;
                }
                stalled.setTextContent("stalled_" + segId);
                fov.getParentNode().insertBefore(stalled, fov.getNextSibling());
            }
        }
        
    }*/
    
    /**
     * Creates the instructions to accumulate the fields and shared fields in the assignments.
     * It also accumulates the FOVs used.
     * 1 - Accumulate fields and shared variables proportional to the FOV
     * 2 - Accumulate FOVs
     * @param problem       The problem
     * @param list          The list of variables 
     * @param segId         The region id
     * @param pi            The problem info
     * @throws CGException  CG00X External error
     */
    /*private static void accFieldsFovs(Document problem, NodeList list, int segId, ProblemInfo pi) throws CGException {
        for (int j = list.getLength() - 1; j >= 0; j--) {
            String varString = new String(SAMRAIUtils.xmlTransform(list.item(j), null));
            if (varString.indexOf("(") != -1) {
                varString = varString.substring(0, varString.indexOf("("));
            }
            if (pi.getVarFields().contains(varString)) {
                //1 - Accumulate fields and shared variables proportional to the FOV
                Element var = (Element) list.item(j);
                String variable = new String(xmlTransform(var.getFirstChild(), null));
                Element parent = (Element) var.getParentNode();
                Element apply = CodeGeneratorUtils.createElement(problem, mtUri, "apply");
                Element plus = CodeGeneratorUtils.createElement(problem, mtUri, "plus");
                Element apply2 = CodeGeneratorUtils.createElement(problem, mtUri, "apply");
                Element times = CodeGeneratorUtils.createElement(problem, mtUri, "times");
                Element applyFov = CodeGeneratorUtils.createElement(problem, mtUri, "apply");
                Element fov = CodeGeneratorUtils.createElement(problem, mtUri, "ci");
                fov.setTextContent("FOV_" + segId);
                apply.appendChild(plus);
                apply.appendChild(var.cloneNode(true));
                apply.appendChild(apply2);
                applyFov.appendChild(fov);
                applyFov.appendChild(CodeGeneratorUtils.deployCoordinates(problem, pi.getCoordinates()));
                apply2.appendChild(times);
                apply2.appendChild(applyFov);
                apply2.appendChild(var.getNextSibling().cloneNode(true));
                parent.replaceChild(apply, var.getNextSibling());
                
                //2 - Accumulate FOVs
                Element math = CodeGeneratorUtils.createElement(problem, mtUri, "math");
                apply = CodeGeneratorUtils.createElement(problem, mtUri, "apply");
                apply2 = CodeGeneratorUtils.createElement(problem, mtUri, "apply");
                Element eq = CodeGeneratorUtils.createElement(problem, mtUri, "eq");
                Element acc = CodeGeneratorUtils.createElement(problem, mtUri, "ci");
                plus = CodeGeneratorUtils.createElement(problem, mtUri, "plus");
                acc.setTextContent("FOVAcc_" + variable);
                math.appendChild(apply);
                apply.appendChild(eq);
                apply.appendChild(acc);
                apply.appendChild(apply2);
                apply2.appendChild(plus);
                apply2.appendChild(acc.cloneNode(true));
                apply2.appendChild(applyFov.cloneNode(true));
               
                if (parent.getParentNode().getNextSibling() != null) {
                    parent.getParentNode().getParentNode().insertBefore(math, parent.getParentNode().getNextSibling());
                }
                else {
                    parent.getParentNode().getParentNode().appendChild(math);
                }
            }
        } 
    }*/
    
    /**
     * Create the initialization for the extrapolation block.
     * @param pi                The problem information
     * @param problem           The problem
     * @return                  The code
     * @throws CGException      CG00X External error
     */
    public static ArrayList<String> extrapolationFunctionVariables(Document problem, ProblemInfo pi) throws CGException {

        ArrayList<String> result = new ArrayList<String>();
        NodeList extrapolations = CodeGeneratorUtils.find(problem, "//sml:fieldExtrapolation/mt:math/sml:functionCall");
        if (extrapolations.getLength() > 0) {
            //Generic PDE call
            if (pi.getExtrapolationType() == ExtrapolationType.generalPDE) {
                for (int j = 0; j < extrapolations.getLength(); j++) {
                    NodeList parameters = extrapolations.item(j).getChildNodes();
                    String finalTo = new String(CodeGeneratorUtils.xmlVarToString(parameters.item(pi.getDimensions() * 2 + 1), false, false));
                    if (!result.contains(finalTo)) {
                        result.add(finalTo);
                    }
                }
            }
            //Balance Law PDE call
            else if (pi.getExtrapolationType() == ExtrapolationType.axisPDE) {
                for (int j = 0; j < extrapolations.getLength(); j++) {
                    NodeList parameters = extrapolations.item(j).getChildNodes();
                    for (int i = 0; i < pi.getDimensions(); i++) {
                        String to = new String(CodeGeneratorUtils.xmlVarToString(parameters.item(i * 3 + 2), false, false));
                        if (!result.contains(to)) {
                            result.add(to);
                        }
                    }
                    String finalTo = new String(CodeGeneratorUtils.xmlVarToString(parameters.item(pi.getDimensions() * 3 + 1), false, false));
                    if (!result.contains(finalTo)) {
                        result.add(finalTo);
                    }
                }
            }
            else {
                for (int j = 0; j < extrapolations.getLength(); j++) {
                    NodeList parameters = extrapolations.item(j).getChildNodes();
                    for (int i = 0; i < pi.getDimensions(); i++) {
                        String from = new String(CodeGeneratorUtils.xmlVarToString(parameters.item(i * 4 + 2), false, false));
                        if (!result.contains(from)) {
                            result.add(from);
                        }
                        String to = new String(CodeGeneratorUtils.xmlVarToString(parameters.item(i * 4 + THREE), false, false));
                        if (!result.contains(to)) {
                            result.add(to);
                        }
                    }
                    if (4 * pi.getDimensions() + 2 < parameters.getLength()) {
                        String finalTo = new String(CodeGeneratorUtils.xmlVarToString(parameters.item(pi.getDimensions() * 4 + 1), false, false));
                        if (!result.contains(finalTo)) {
                            result.add(finalTo);
                        }
                    } 
                }
            }
        }
        return result;
    }
    
    /**
     * Replaces the InteriorDomainContext tag with a if clause.
     * @param instruction   The instruction
     * @param coords        The coordinates
     * @throws CGException  CG00X External error
     */
    private static void replaceInteriorDomainContextTags(Node instruction, ArrayList<String> coords) throws CGException {
        Document doc = instruction.getOwnerDocument();
        Element ifE = CodeGeneratorUtils.createElement(doc, smlUri, "if");
        Element cond = CodeGeneratorUtils.createElement(doc, mtUri, "math");
        Element then = CodeGeneratorUtils.createElement(doc, smlUri, "then");
        ifE.appendChild(cond);
        ifE.appendChild(then);
        
        Element applyTmp = null;
        for (int i = 0; i < coords.size(); i++) {
            Element apply = CodeGeneratorUtils.createElement(doc, mtUri, "apply");
            Element and = CodeGeneratorUtils.createElement(doc, mtUri, "and");
            Element applyG = CodeGeneratorUtils.createElement(doc, mtUri, "apply");
            Element applyL = CodeGeneratorUtils.createElement(doc, mtUri, "apply");
            Element lt = CodeGeneratorUtils.createElement(doc, mtUri, "lt");
            Element gt = CodeGeneratorUtils.createElement(doc, mtUri, "geq");
            Element coord = CodeGeneratorUtils.createElement(doc, mtUri, "ci");
            coord.setTextContent(coords.get(i));
            Element ghost = CodeGeneratorUtils.createElement(doc, mtUri, "ci");
            ghost.setTextContent("d_ghost_width");
            Element last = CodeGeneratorUtils.createElement(doc, mtUri, "ci");
            last.setTextContent(coords.get(i) + "last");
            Element applyMinus = CodeGeneratorUtils.createElement(doc, mtUri, "apply");
            Element minus = CodeGeneratorUtils.createElement(doc, mtUri, "minus");
            
            apply.appendChild(and);
            apply.appendChild(applyG);
            apply.appendChild(applyL);
            applyG.appendChild(gt);
            applyG.appendChild(coord);
            applyG.appendChild(ghost);
            applyL.appendChild(lt);
            applyL.appendChild(coord.cloneNode(true));
            applyL.appendChild(applyMinus);
            applyMinus.appendChild(minus);
            applyMinus.appendChild(last);
            applyMinus.appendChild(ghost.cloneNode(true));
            
            if (i == 0) {
                applyTmp = apply;
            }
            else {
                Element applyTmp2 = CodeGeneratorUtils.createElement(doc, mtUri, "apply");
                Element and2 = CodeGeneratorUtils.createElement(doc, mtUri, "and");
                applyTmp2.appendChild(and2);
                applyTmp2.appendChild(applyTmp);
                applyTmp2.appendChild(apply);
                applyTmp = (Element) applyTmp2.cloneNode(true);
            }
        }
        cond.appendChild(applyTmp);
        
        NodeList interior = CodeGeneratorUtils.find(instruction, ".//sml:interiorDomainContext");
        for (int i = interior.getLength() - 1; i >= 0; i--) {
            Element newIf = (Element) ifE.cloneNode(true);
            NodeList children = interior.item(i).getChildNodes();
            for (int j = 0; j < children.getLength(); j++) {
                newIf.getLastChild().appendChild(children.item(j).cloneNode(true));
            }
            interior.item(i).getParentNode().replaceChild(newIf, interior.item(i));
        }
    }
    
    /**
     * Replaces the spatial discretization tags with a if clause.
     * @param instruction   The instruction
     * @param coords        The coordinates
     * @param regionId      The current region id
     * @param problem       The problem
     * @throws CGException  CG00X External error
     */
    private static void replaceSpatialDiscretizationTags(Node instruction, ArrayList<String> coords, int regionId, 
            Document problem) throws CGException {
        Document doc = instruction.getOwnerDocument();
      
        NodeList spaceDiscretizations = CodeGeneratorUtils.find(instruction, ".//sml:spatialDiscretizations");
        for (int i = spaceDiscretizations.getLength() - 1; i >= 0; i--) {
            DocumentFragment replacement = doc.createDocumentFragment();
            //Adding main spatial discretization
            NodeList spaceDiscretizationList = CodeGeneratorUtils.find(spaceDiscretizations.item(i), "./sml:spatialDiscretization");
            for (int j = spaceDiscretizationList.getLength() - 1; j >= 0; j--) {
                SpatialCondition sc = new SpatialCondition((Element) spaceDiscretizationList.item(j).getFirstChild(), problem, true);
                //Append all children in a fragment
                DocumentFragment instructions = doc.createDocumentFragment();
                NodeList children = spaceDiscretizationList.item(j).getChildNodes();
                for (int k = 1; k < children.getLength(); k++) {
                	instructions.appendChild(children.item(k).cloneNode(true));
                }
                //Create condition
                Element mainCondition = sc.createCondition(doc, coords, regionId);
                //If condition exists add the mathematical expressions to the 'then' tag 
                if (mainCondition != null) {
                    mainCondition.getLastChild().appendChild(instructions);
                    replacement.appendChild(mainCondition);
                }
                //If the condition does not exist shortcut the mathematical expressions
                else {
                    replacement.appendChild(instructions);
                }
            }
            
            //Adding boundary spatial discretization
            NodeList boundaryDiscretizations = CodeGeneratorUtils.find(spaceDiscretizations.item(i), ".//sml:boundarySpatialDiscretization");
            for (int j = 0; j < boundaryDiscretizations.getLength(); j++) {
                SpatialCondition sc = 
                        new SpatialCondition((Element) boundaryDiscretizations.item(j).getFirstChild(), instruction.getOwnerDocument(), false);
                Element boundaryCondition = sc.createCondition(doc, coords, regionId);
                NodeList children = CodeGeneratorUtils.find(boundaryDiscretizations.item(j), "./mt:math");
                for (int k = 0; k < children.getLength(); k++) {
                    boundaryCondition.getLastChild().appendChild(children.item(k).cloneNode(true));
                }
                replacement.appendChild(boundaryCondition);
            }
            spaceDiscretizations.item(i).getParentNode().replaceChild(replacement, spaceDiscretizations.item(i));
        }
    }
    
    /**
     * Replaces the deltas t with simPlat_dt or Problem::computeDt() depending if they are located in the initialization or not.
     * @param problemDoc        The problem
     * @throws CGException      CG00X External error
     */
    private static void deltaTReplacement(Document problemDoc) throws CGException {
        String query = "/*/mms:implicitParameters/mms:implicitParameter[mms:type ='delta_time']";
        String deltat = CodeGeneratorUtils.find(problemDoc, query).item(0).getFirstChild().getNextSibling().getTextContent();
        Element deltatCi = CodeGeneratorUtils.createElement(problemDoc, mtUri, "ci");
        deltatCi.setTextContent(deltat);
        Element newDeltaT;
        NodeList deltats = CodeGeneratorUtils.find(problemDoc, "//mt:ci[text() = '" + deltat + "']");
        for (int i = deltats.getLength() - 1; i >= 0; i--) {
            if (CodeGeneratorUtils.find(deltats.item(i), 
                    "./ancestor::mms:initialConditions|./ancestor::mms:postInitialConditions").getLength() == 0) {
                newDeltaT = CodeGeneratorUtils.createElement(problemDoc, mtUri, "ci");
                newDeltaT.setTextContent("simPlat_dt");
            }
            else {
                newDeltaT = CodeGeneratorUtils.createElement(problemDoc, mtUri, "ci");
                newDeltaT.setTextContent("initial_dt");
            }
            deltats.item(i).getParentNode().replaceChild(newDeltaT, deltats.item(i));
        }
    }
    
    /**
     * Force numbers used to be real (not field indexes).
     * @param originalDoc       The document where to find
     * @throws CGException      CG00X External error
     */
    private static void formatNumbers(Document originalDoc) throws CGException {
        //Force numbers used to be real (not field indexes)
        NodeList numbers = CodeGeneratorUtils.find(originalDoc, 
                "//mt:cn[not(contains(text(), '.')) and not(ancestor::mt:apply/*[1][name() = 'mt:ci']) and not(ancestor::sml:sharedVariable) " 
                + "and not(contains(text(), '\"')) and not (contains(text(), \"'\")) and not(ancestor::mt:apply/mt:ci = 'ReflectionCounter')" 
                + "and not(ancestor::mt:msubsup or ancestor::mt:msub or ancestor::mt:msup) "
                + "and not(ancestor::mt:apply/*[1][name() = 'sml:regionInteriorId']) and "
                + "not(ancestor::mt:apply/*[1][name() = 'sml:regionSurfaceId'])]");
        for (int i = 0; i < numbers.getLength(); i++) {
            numbers.item(i).setTextContent(numbers.item(i).getTextContent() + ".0");
        }
        //Correct negative numbers whose negative value is inside the tag
        numbers = CodeGeneratorUtils.find(originalDoc, 
                "//mt:cn[starts-with(text(), '-')] ");
        for (int i = numbers.getLength() - 1; i >= 0 ; i--) {
            Element negative = CodeGeneratorUtils.createElement(originalDoc, mtUri, "apply");
            Element minus = CodeGeneratorUtils.createElement(originalDoc, mtUri, "minus");
            Element newCn = CodeGeneratorUtils.createElement(originalDoc, mtUri, "cn");
            newCn.setTextContent(numbers.item(i).getTextContent().substring(1));
            negative.appendChild(minus);
            negative.appendChild(newCn);
            numbers.item(i).getParentNode().replaceChild(negative, numbers.item(i));
        }
    }
    
    /**
     * Replace particleSeparationProduct tags.
     * @param originalDoc       The original document
     * @param newDoc            The document where to replace
     * @param coordinates       The spatial coordinates
     * @throws CGException      CG00X External error
     */
    private static void replaceParticleSeparationProductTags(Document originalDoc, Document newDoc, ArrayList<String> coordinates) throws CGException {
        Element volElem = CodeGeneratorUtils.createElement(newDoc, mtUri, "ci");
        volElem.setTextContent("particleSeparation_" + CodeGeneratorUtils.discToCont(originalDoc, coordinates.get(0)));
        Element times = CodeGeneratorUtils.createElement(newDoc, mtUri, "times");
        for (int i = 1; i < coordinates.size(); i++) {
            Element volElem2 = CodeGeneratorUtils.createElement(newDoc, mtUri, "ci");
            volElem2.setTextContent("particleSeparation_" + CodeGeneratorUtils.discToCont(originalDoc, coordinates.get(i)));
            
            Element tmp = CodeGeneratorUtils.createElement(newDoc, mtUri, "apply");
            tmp.appendChild(times.cloneNode(false));
            tmp.appendChild(volElem);
            tmp.appendChild(volElem2);
            volElem = tmp;
        }
        NodeList volumes = CodeGeneratorUtils.find(newDoc, "//sml:particleSeparationProduct");
        for (int i = 0; i < volumes.getLength(); i++) {
            volumes.item(i).getParentNode().replaceChild(volElem.cloneNode(true), volumes.item(i));
        }
    }
    
    /**
     * Create the decrement index element when using deltas.
     * @param doc               The document
     * @param delta             The delta
     * @param index             The index of the delta inside the delta array
     * @param dimensions        The number spatial dimensions of the problem
     * @param fvm               If the element to generate is a fvm or particle code.
     * @return                  The element to use
     */
    private static Element createDecrementElement(Document doc, Element delta, int index, int dimensions, boolean fvm) {
        Element newApply;
        if (fvm) {
            newApply = CodeGeneratorUtils.createElement(doc, mtUri, "apply");
            Element newTimes = CodeGeneratorUtils.createElement(doc, mtUri, "times");
            Element newApplyMinus = CodeGeneratorUtils.createElement(doc, mtUri, "apply");
            Element newMinus = CodeGeneratorUtils.createElement(doc, mtUri, "minus");
            Element newApplyPlus = CodeGeneratorUtils.createElement(doc, mtUri, "apply");
            Element newPlus = CodeGeneratorUtils.createElement(doc, mtUri, "plus");
            Element newCoord = CodeGeneratorUtils.createElement(doc, mtUri, "ci");
            newCoord.setTextContent(delta.getLastChild().getTextContent());
            Element newApplyIfirst = CodeGeneratorUtils.createElement(doc, mtUri, "apply");
            Element newIfirst = CodeGeneratorUtils.createElement(doc, mtUri, "ci");
            newIfirst.setTextContent("boxfirst");
            Element newIfirstIndex = CodeGeneratorUtils.createElement(doc, mtUri, "cn");
            newIfirstIndex.setTextContent(String.valueOf(index % dimensions));
            Element newStencil = CodeGeneratorUtils.createElement(doc, mtUri, "ci");
            newStencil.setTextContent("d_ghost_width");
            Element newDelta = CodeGeneratorUtils.createElement(doc, mtUri, "ci");
            newDelta.setTextContent(delta.getFirstChild().getNextSibling().getTextContent());
            newApply.appendChild(newTimes);
            newApply.appendChild(newApplyMinus);
            newApply.appendChild(newDelta);
            newApplyMinus.appendChild(newMinus);
            newApplyMinus.appendChild(newApplyPlus);
            newApplyMinus.appendChild(newStencil);
            newApplyPlus.appendChild(newPlus);
            newApplyPlus.appendChild(newCoord);
            newApplyPlus.appendChild(newApplyIfirst);
            newApplyIfirst.appendChild(newIfirst);
            newApplyIfirst.appendChild(newIfirstIndex);
        }
        else {
            newApply = CodeGeneratorUtils.createElement(doc, mtUri, "apply");
            Element tendsto = CodeGeneratorUtils.createElement(doc, mtUri, "tendsto");
            Element ci1 = CodeGeneratorUtils.createElement(doc, mtUri, "ci");
            ci1.setTextContent("particle");
            Element ci2 = CodeGeneratorUtils.createElement(doc, mtUri, "ci");
            ci2.setTextContent("getposition" + delta.getFirstChild().getNextSibling().getNextSibling().getTextContent() + "()");
            newApply.appendChild(tendsto);
            newApply.appendChild(ci1);
            newApply.appendChild(ci2);
        }
        
        return newApply;
    }
    
    /**
     * Creates the mathml structure to access the position of a particle.
     * @param doc       The document
     * @param coord     The position coordinate
     * @return          The element
     */
    private static Element createPosition(Document doc, String coord) {
        Element newApply = CodeGeneratorUtils.createElement(doc, mtUri, "apply");
        Element tendsto = CodeGeneratorUtils.createElement(doc, mtUri, "tendsto");
        Element ci1 = CodeGeneratorUtils.createElement(doc, mtUri, "ci");
        ci1.setTextContent("particle");
        Element ci2 = CodeGeneratorUtils.createElement(doc, mtUri, "ci");
        ci2.setTextContent("position" + coord);
        newApply.appendChild(tendsto);
        newApply.appendChild(ci1);
        newApply.appendChild(ci2);
        return newApply;
    }
    
    /**
     * Find in the document all the variables that have to be registered in the system. 
     * This variables are fields and auxiliary variables that requires array storage.
     * @param doc               The problem document
     * @param fieldTimeLevels   The field time levels
     * @param coordinates       The spatial coordinates
     * @param extrapolationType	The extrapolation type
     * @return                  All the variables in an array list
     * @throws CGException      CG003 Not possible to create code for the platform
     *                           CG00X External error
     */
    public static ArrayList<String> getVariables(Document doc, LinkedHashMap<String, Integer> fieldTimeLevels, 
            ArrayList<String> coordinates, ExtrapolationType extrapolationType) throws CGException {
        //Take all the fields and variables that derives from the fields (phi -> phi_p, phi_p_p, etc.)
        ArrayList<String> candidates = new ArrayList<String>();
        Iterator<String> fieldEnum = fieldTimeLevels.keySet().iterator();
        while (fieldEnum.hasNext()) {
            String field = fieldEnum.next();
            int timeLevel = fieldTimeLevels.get(field);
            field = SAMRAIUtils.variableNormalize(field);
            candidates.add(field);
            //Fields sub p
            for (int i = 1; i < timeLevel; i++) {
                field = field + "_p";
                candidates.add(field);
            }
        }
        //Take all the variables that are assigned inside the code. They will need to be auxiliary.
        Set<Node> leftTerms = CodeGeneratorUtils.findWithoutDuplicates(doc, ".//mt:math[(not(parent::sml:if) or parent::sml:then) and "
            + "(not(parent::sml:while) or parent::sml:loop) and not(parent::sml:return) and"
            + " not(ancestor::mms:applyIf) and not(ancestor::mms:finalizationCondition)]/mt:apply/mt:eq/following-sibling::*[1][name()='mt:apply' "
            + "or name()='sml:sharedVariable']");
        Iterator<Node> termsIt = leftTerms.iterator();
        while (termsIt.hasNext()) {
            String variable = CodeGeneratorUtils.xmlVarToString(termsIt.next().getFirstChild(), false, false);
            if (!candidates.contains(variable)) {
                candidates.add(variable);
            }   
        }
        //Function variables
        String query = ".//sml:fieldExtrapolation//sml:functionCall";
        NodeList extrapolations = CodeGeneratorUtils.find(doc, query);
        for (int j = extrapolations.getLength() - 1; j >= 0; j--) {
            NodeList params = extrapolations.item(j).getChildNodes();
            for (int i = 0; i < coordinates.size(); i++) {
                if (extrapolationType == ExtrapolationType.axisPDE) {
                	String variable = CodeGeneratorUtils.xmlVarToString(params.item(i * 3 + 2).cloneNode(true), false, false);
                    if (!candidates.contains(variable)) {
                        candidates.add(variable);
                    }
                }
            }
            //If the extrapolation function is for fields the field should also be extrapolated (not only the axis variables)
            if (params.item(0).getTextContent().equals("extrapolate_field")) {
                String variable = "";
            	switch (extrapolationType) {
	        		case axisPDE:
	        			variable = 
	        			CodeGeneratorUtils.xmlVarToString(params.item(coordinates.size() * 3 + 1).cloneNode(true), false, false);
	        			break;
	        		case generalPDE:
	        			variable = 
	        			CodeGeneratorUtils.xmlVarToString(params.item(coordinates.size() * 2 + 1).cloneNode(true), false, false);
	        			break;
	        	}
                if (!candidates.contains(variable)) {
                    candidates.add(variable);
                }
            }
        }
        //If there is extrapolation, all the fields have extrapolated variables.
        if (extrapolations.getLength() > 1) {
            NodeList fields = CodeGeneratorUtils.find(doc, "/*/mms:fields/mms:field|/*/mms:auxiliaryFields/mms:auxiliaryField");
            for (int i = 0; i < coordinates.size(); i++) {
                String coord = coordinates.get(i);
                for (int j = 0; j < fields.getLength(); j++) {
                    String field = SAMRAIUtils.variableNormalize(fields.item(j).getTextContent());
                    String variable = "extrapolatedSB" + field + "SP" + coord;
                    if (!candidates.contains(variable)) {
                        candidates.add(variable);
                    } 
                }
            }
        }
        return candidates;
    }
    
    /**
     * Return the analysis fields from the problem.
     * @param problem           The problem
     * @return                  A list of analysis fields
     * @throws CGException      CG00X External error
     */
    public static ArrayList<String> getAnalysisFields(Document problem) throws CGException {
        ArrayList<String> analysisFields = new ArrayList<String>();
        NodeList fields = CodeGeneratorUtils.find(problem, "/*/mms:analysisFields/mms:analysisField");
        for (int i = 0; i < fields.getLength(); i++) {
            analysisFields.add(SAMRAIUtils.variableNormalize(fields.item(i).getTextContent()));
        }
        return analysisFields;
    }
    
    /**
     * Return the auxiliary fields from the problem.
     * @param problem           The problem
     * @return                  A list of auxiliary fields
     * @throws CGException      CG00X External error
     */
    public static ArrayList<String> getAuxiliaryFields(Document problem) throws CGException {
        ArrayList<String> auxFields = new ArrayList<String>();
        NodeList fields = CodeGeneratorUtils.find(problem, "/*/mms:auxiliaryFields/mms:auxiliaryField");
        for (int i = 0; i < fields.getLength(); i++) {
            auxFields.add(SAMRAIUtils.variableNormalize(fields.item(i).getTextContent()));
        }
        return auxFields;
    }
    
    /**
     * Return the post refinement auxiliary fields from the problem.
     * @param problem           The problem
     * @param auxiliaryFields	Auxiliary fields
     * @return                  A list of auxiliary fields
     * @throws CGException      CG00X External error
     */
    public static ArrayList<String> setPostRefinementAuxiliaryFields(Document problem, ArrayList<String> auxiliaryFields) throws CGException {
        ArrayList<String> candidates = new ArrayList<String>();
        //Take all the variables that are assigned inside the code. They will need to be auxiliary.
        Set<Node> leftTerms = CodeGeneratorUtils.findWithoutDuplicates(problem, "//mms:postRefinementAlgorithm//mt:math[(not(parent::sml:if) or parent::sml:then) and "
            + "(not(parent::sml:while) or parent::sml:loop) and not(parent::sml:return) and"
            + " not(ancestor::mms:applyIf) and not(ancestor::mms:finalizationCondition)]/mt:apply/mt:eq/following-sibling::*[1]");
        Iterator<Node> termsIt = leftTerms.iterator();
        while (termsIt.hasNext()) {
            String variable = CodeGeneratorUtils.xmlVarToString(termsIt.next(), false, false);
            if (auxiliaryFields.contains(variable) && !candidates.contains(variable)) {
                candidates.add(variable);
            }   
        }
        return candidates;
    }
    
    /**
     * Gets the map of position variables not from the code but from the spatial coordinates directly. (ABM_meshless).
     * @param doc               The problem
     * @param pi                The problem info
     * @return                  The variables
     * @throws CGException      CG00X - External error
     */
    public static ArrayList<String> getPositionVariablesFromCoords(Document doc, ProblemInfo pi) throws CGException {
    	ArrayList<String> posVars = new ArrayList<String>();
        for (int i = 0; i < pi.getCoordinates().size(); i++) {
            String coord = pi.getCoordinates().get(i);
            posVars.add(coord);  
        }
        return posVars;
    }
    
    /**
     * Get the variable names used for positioning the particles during the execution.
     * @param doc               The xml document to search into
     * @param pi                The problem information
     * @return                  A hashtable with the variables and the corresponding coordinate
     * @throws CGException      CG00X - External error
     */
    public static ArrayList<String> getPositionVariables(Document doc, ProblemInfo pi, String species) throws CGException {
        try {
            String[][] xslParams = new String [4][2];
            xslParams[0][0] = "condition";
            xslParams[0][1] = "false";
            xslParams[1][0] = "indent";
            xslParams[1][1] = "0";
            xslParams[2][0] = "dimensions";
            xslParams[2][1] = String.valueOf(pi.getDimensions());
            xslParams[3][0] = "timeCoord";
            xslParams[3][1] = pi.getTimeCoord();
            
            ArrayList<String> posVars = new ArrayList<String>();

            LinkedHashMap<String, Integer> positionTimeLevels = pi.getPositionTimeLevels();
            Iterator<String> it = positionTimeLevels.keySet().iterator();
            while (it.hasNext()) {
                String position = it.next();
                int timeLevels = positionTimeLevels.get(position);
                for (int i = 0; i < timeLevels; i++) {
                	posVars.add(position);
                    position = position + "_p";
                }
            }
            
            for (int i = 0; i < pi.getCoordinates().size(); i++) {
                String coord = pi.getCoordinates().get(i);
                String contCoord = CodeGeneratorUtils.discToCont(doc, coord);
                NodeList leftTerms = CodeGeneratorUtils.find(doc, ".//mt:math[(not(parent::sml:if) or parent::sml:then) and (not(parent::sml:while)" 
                        + " or parent::sml:loop) and not(parent::sml:return) and not(ancestor::mms:condition) and "
                        + "not(ancestor::mms:finalizationCondition) and ancestor::sml:iterateOverParticles[@speciesNameAtt = '" + species + "']]/mt:apply/mt:eq[following-sibling::*[1][name() = 'mt:ci' and "
                        + "(descendant::sml:particlePosition[text() = '" + contCoord + "'])]]");
                for (int  j = 0; j < leftTerms.getLength(); j++) {
                    String variable = new String(SAMRAIUtils.xmlTransform(leftTerms.item(j).getNextSibling().cloneNode(true), xslParams));
                    if (!posVars.contains(variable)) {
                        posVars.add(variable);
                    }
                }
            }
            
            return posVars;
        } 
        catch (Exception e) {
            throw new CGException(CGException.CG00X, e.getMessage());
        }
    }
    
    /**
     * Preprocess an instruction block to do the following changes:
     * 1 - Remove auxiliary field instructions.
     * @param instruction       The instruction in xml
     * @param pi                The problem information
     * @return                  The instruction ready to be processed by the XSL
     * @throws CGException      CG00X External error
     */
    /*public static Node preProcessFVMBlockMove(Node instruction, ProblemInfo pi) throws CGException { 
        //1 - Remove auxiliary field instructions.
        NodeList vars = CodeGeneratorUtils.find(instruction, ".//mt:math[not(ancestor::sml:boundary) and mt:apply/mt:eq/following-sibling::*[1]" 
                + "[name()='sml:sharedVariable' and mt:apply]]|.//mt:math[not(ancestor::sml:boundary) and mt:apply/mt:eq/"
                + "following-sibling::*[1][name()='mt:apply' and mt:ci/mt:msup[mt:apply[mt:ci = '(" 
                + pi.getTimeCoord() + ")' and mt:plus and mt:cn[text() = '1']]]]]");
        for (int j = vars.getLength() - 1; j >= 0; j--) {
            String variable = new String(SAMRAIUtils.xmlTransform(vars.item(j).getFirstChild().getFirstChild().getNextSibling(), null));
            //Take only the identifier. The indexes will be ignored
            if (variable.indexOf("(") != -1) {
                variable = variable.substring(0, variable.indexOf("("));
            }
            if (!pi.getVarFields().contains(variable)) {
                vars.item(j).getParentNode().removeChild(vars.item(j)); 
            }
        }
        
        return instruction;
    }*/
    
    /**
     * Preprocess an instruction block to do the following changes:
     * 1a - Mesh variable access.
     * 1b - Particle variable access.
     * 2 - Cast ReflectionCounter as a int.
     * 3 - Cast complex indexes as int.
     * 4 - Replace interiorDomainContext tags
     * 5 - Modify code for neighbourParticle tags
     * 6 - Replace particle positions. 
     * @param instruction       The instruction in xml
     * @param pi                The problem information
     * @param functions         If the preprocess if for a function
     * @param isMacro           If the instruction is a macro function
     * @return                  The instruction ready to be processed by the XSL
     * @throws CGException      CG00X External error
     */
    public static Node preProcessPDEBlock(Node instruction, ProblemInfo pi, boolean functions, boolean isMacro) throws CGException {
        ArrayList<String> vars = pi.getVariables();
        ArrayList<String> extrapolVars = pi.getExtrapolVars();
        ArrayList<String> extrapolFuncVars = pi.getExtrapolFuncVars();
        
        ArrayList<String> coords = pi.getCoordinates();
        //5 - Replace SpatialDiscretization tags
        replaceSpatialDiscretizationTags(instruction, pi.getCoordinates(), pi.getCurrentRegionId(), pi.getProblem());
        //1a - Mesh variable access
        Element vector = CodeGeneratorUtils.createElement(instruction.getOwnerDocument(), mtUri, "ci");
        vector.setTextContent("vector");
        NodeList variablesUsed = CodeGeneratorUtils.find(instruction, ".//mt:apply[child::*[1][name() = 'mt:ci'] and not(ancestor::sml:iterateOverParticles or ancestor::sml:neighbourParticle)]"
                + "|.//mt:apply[child::*[1][name() = 'mt:msup' and not(ancestor::sml:iterateOverParticles or ancestor::sml:neighbourParticle)]/*[1][name() = 'mt:ci']]|.//mt:apply[child::*[1][name() = 'sml:regionInteriorId'] and not(ancestor::sml:iterateOverParticles or ancestor::sml:neighbourParticle)]");
        for (int j = 0; j < variablesUsed.getLength(); j++) {
            String variable = CodeGeneratorUtils.xmlVarToString(variablesUsed.item(j), false, false);
            if (vars.contains(variable) 
                    || variable.startsWith("FOV_") 
                    || extrapolVars.contains(variable) 
                    || extrapolFuncVars.contains(variable)) {
                variablesUsed.item(j).insertBefore(vector.cloneNode(true), variablesUsed.item(j).getFirstChild());
            }
        }
        //1b - Particle variable access
        Element particleAccess = CodeGeneratorUtils.createElement(instruction.getOwnerDocument(), smlUri, "particleAccess");
        variablesUsed = CodeGeneratorUtils.find(instruction, ".//mt:apply[child::*[1][name() = 'mt:ci'] and (ancestor::sml:iterateOverParticles or ancestor::sml:neighbourParticle)]"
                + "|.//mt:apply[child::*[1][name() = 'mt:msup' and (ancestor::sml:iterateOverParticles or ancestor::sml:neighbourParticle)]/*[1][name() = 'mt:ci']]|.//mt:apply[child::*[1][name() = 'sml:regionInteriorId'] and (ancestor::sml:iterateOverParticles or ancestor::sml:neighbourParticle)]");
        for (int j = 0; j < variablesUsed.getLength(); j++) {
            String variable = CodeGeneratorUtils.xmlVarToString(variablesUsed.item(j), false, false);
            if (vars.contains(variable) 
                    || variable.startsWith("FOV_") 
                    || extrapolVars.contains(variable) 
                    || extrapolFuncVars.contains(variable)) {
            	//Insert particle access
                variablesUsed.item(j).insertBefore(particleAccess.cloneNode(true), variablesUsed.item(j).getFirstChild());
            }
        }
        //2 - Cast ReflectionCounter as a int.
        NodeList reflectionCounters = CodeGeneratorUtils.find(instruction, 
                ".//mt:ci[text() = 'ReflectionCounter' and not(ancestor::mt:apply[mt:eq]/mt:ci[text() = 'ReflectionCounter'])]");
        for (int j = 0; j < reflectionCounters.getLength(); j++) {
            reflectionCounters.item(j).setTextContent("int(ReflectionCounter)");
        }
        if (!isMacro) {
            //3 - Cast complex indexes as int.
            variablesUsed = CodeGeneratorUtils.find(instruction, ".//mt:apply[child::mt:ci = 'vector']");
            for (int j = 0; j < variablesUsed.getLength(); j++) {
                NodeList children = variablesUsed.item(j).getChildNodes();
                for (int k = 2; k < children.getLength(); k++) {
                    boolean complexFound = false;
                    NodeList cis = CodeGeneratorUtils.find(children.item(k), 
                            ".[self::mt:ci]|.[self::mt:apply[child::*[1][name() = 'mt:ci']]]|.[self::sml:sharedVariable[mt:apply]]"
                            + "|.[self::mt:apply[child::*[1][name() = 'mt:msup']/*[1][name() = 'mt:ci']]]|.//mt:apply[child::*[1][name() = 'mt:ci']]"
                            + "|.//mt:apply[child::*[1][name() = 'mt:msup']/*[1][name() = 'mt:ci']]|.//mt:ci");
                    for (int l = 0; l < cis.getLength() && !complexFound; l++) {
                        String variable = cis.item(l).getTextContent();
                        //Remove "par" prefix from parameter variables in functions 
                        if (functions) {  
                            variable = variable.substring(THREE);
                        }
                        if (!coords.contains(variable)) {
                            complexFound = true;
                        }
                    }
                    if (complexFound) {
                        Element apply = CodeGeneratorUtils.createElement(instruction.getOwnerDocument(), mtUri, "apply");
                        Element intElem = CodeGeneratorUtils.createElement(instruction.getOwnerDocument(), mtUri, "int");
                        apply.appendChild(intElem);
                        apply.appendChild(children.item(k).cloneNode(true));
                        children.item(k).getParentNode().replaceChild(apply, children.item(k));
                    }
                }
            }
        }
        //4 - Replace interiorDomainContext tags
        replaceInteriorDomainContextTags(instruction, pi.getCoordinates());
        
        //6 - Replace particle positions
        for (int i = 0; i < pi.getContCoordinates().size(); i++) {
            String coord = pi.getContCoordinates().get(i);
            NodeList positions = CodeGeneratorUtils.find(instruction, ".//mt:ci[not(ancestor::mt:ci) and descendant::sml:particlePosition[text() = '" + coord + "']]");
            for (int j = positions.getLength() - 1; j >= 0; j--) {
            	Element apply = CodeGeneratorUtils.createElement(instruction.getOwnerDocument(), mtUri, "apply");
            	apply.appendChild(particleAccess.cloneNode(true));
            	apply.appendChild(positions.item(j).cloneNode(true));
            	NodeList pos = apply.getElementsByTagNameNS(smlUri, "particlePosition");
                for (int k = pos.getLength() - 1; k >= 0; k--) {
                    Text text = instruction.getOwnerDocument().createTextNode("position" + coord);
                    pos.item(k).getParentNode().replaceChild(text, pos.item(k));
                }
                positions.item(j).getParentNode().replaceChild(apply, positions.item(j));
            }
        }
        
        //5
        NodeList neighbours = CodeGeneratorUtils.find(instruction, ".//sml:particleAccess[ancestor::sml:neighbourParticle]");
        for (int i = neighbours.getLength() - 1; i >= 0; i--) {
            Element setFieldValue = CodeGeneratorUtils.createElement(instruction.getOwnerDocument(), smlUri, "particleAccessb");
            neighbours.item(i).getParentNode().replaceChild(setFieldValue, neighbours.item(i));
        }

        //Remove neighbourParticle
        neighbours = CodeGeneratorUtils.find(instruction, ".//sml:neighbourParticle");
        for (int i = neighbours.getLength() - 1; i >= 0; i--) {
        	DocumentFragment children = instruction.getOwnerDocument().createDocumentFragment();
            NodeList npChildren = neighbours.item(i).getChildNodes();
            for (int j = 0; j < npChildren.getLength(); j++) {
            	children.appendChild(npChildren.item(j).cloneNode(true));
            }
            Node parent = neighbours.item(i).getParentNode();
            parent.replaceChild(children, neighbours.item(i));
        }
        
        return instruction;
    }
    
    /**
     * Preprocess an instruction block to do the following changes:
     * 1 - Replacement of the deltas
     * 2 - Change the access to a get for the right part of instructions.
     * 3 - Change the access to a set for assignations of fields. 
     * 4 - Replace discrete coordinate utilization with position of the particle. 
     * 5 - Modify code for neighbourParticle tags
     * 6 - Modify particleDistance structure
     * 7 - Implement loop performance: B-A & A-B calculation at same iteration
     * @param instruction       The instruction in xml
     * @param vars              The variables used in the problem
     * @param coordinates       The spatial coordinates
     * @param movement          For movement the process 4 changes.
     * @param pi                The problem information
     * @return                  The instruction ready to be processed by the XSL
     * @throws CGException      CG00X External error
     */
    public static Element preProcessParticlesBlock(Node instruction, ArrayList<String> vars, ArrayList<String> coordinates, boolean movement, 
            ProblemInfo pi) throws CGException {
        Document doc = instruction.getOwnerDocument();
        int dims = coordinates.size();
    
        //1
        NodeList deltas = CodeGeneratorUtils.find(doc, "/*/mms:implicitParameters/mms:implicitParameter[mms:type ='delta_space']");
        for (int i = 0; i < deltas.getLength(); i++) {
            //Node to search
            Element apply = CodeGeneratorUtils.createElement(doc, mtUri, "apply");
            Element times = CodeGeneratorUtils.createElement(doc, mtUri, "times");
            Element coord = CodeGeneratorUtils.createElement(doc, mtUri, "ci");
            coord.setTextContent(deltas.item(i).getLastChild().getTextContent());
            Element delta = CodeGeneratorUtils.createElement(doc, mtUri, "ci");
            delta.setTextContent(deltas.item(i).getFirstChild().getNextSibling().getTextContent());
            apply.appendChild(times);
            apply.appendChild(coord);
            apply.appendChild(delta);
            //New node
            Element newApply = createDecrementElement(doc, (Element) deltas.item(i), i, dims, false);
            //Search and replace in the problem
            CodeGeneratorUtils.nodeReplace(instruction, apply, newApply);
            NodeList deltaUse = CodeGeneratorUtils.find(instruction,
                    ".//mt:ci[text() = '" + deltas.item(i).getFirstChild().getNextSibling().getTextContent() + "']");
            for (int j = 0; j < deltaUse.getLength(); j++) {
                deltaUse.item(j).setTextContent("dx[" + (i % dims) + "]");
            }
        }
        //ABM does not have implicit parameters
        if (pi.isHasABMMeshlessFields()) {
            for (int i = 0; i < coordinates.size(); i++) {
                Element coord = CodeGeneratorUtils.createElement(doc, mtUri, "ci");
                coord.setTextContent(coordinates.get(i));
                //Search and replace in the problem
                CodeGeneratorUtils.nodeReplace(instruction, coord, createPosition(doc, coordinates.get(i)));
            }

        }
        
        //2
        String query = ".//mt:apply[child::*[1][name() = 'mt:ci' or ancestor::sml:sharedVariable] and " 
                + "((parent::sml:sharedVariable[not(preceding-sibling::mt:eq) or count(preceding-sibling::*) > 1]) or "
                + "(not(parent::sml:sharedVariable) " 
                + "and not(preceding-sibling::mt:eq) or count(preceding-sibling::*) > 1) or"
                + " (ancestor::mt:math[parent::sml:if] and not(ancestor::mt:math[parent::sml:then])) or (ancestor::mt:math[parent::sml:while] and "
                + "not(ancestor::mt:math[parent::sml:loop])))]|.//mt:ci[parent::sml:functionCall]|.//sml:functionCall/sml:sharedVariable/mt:ci|" 
                + ".//mt:apply[child::*[1][name() = 'mt:msup' and not(preceding-sibling::mt:eq and position() != 2 and" 
                + " (not(ancestor::mt:math[parent::sml:if]) or ancestor::mt:math[parent::sml:then]) " 
                + "and (not(ancestor::mt:math[parent::sml:while]) or ancestor::mt:math[parent::sml:loop]))]/*[1][name() = 'mt:ci' or" 
                + " ancestor::sml:sharedVariable]]";
        NodeList variablesUsed = CodeGeneratorUtils.find(instruction, query);
        for (int i = 0; i < variablesUsed.getLength(); i++) {
            String variable = xmlTransform(variablesUsed.item(i), null);
            if (variable.indexOf("(") != -1) {
                variable = variable.substring(0, variable.indexOf("("));
                if (vars.contains(variable) || variable.equals("region") || variable.equals("newRegion")) {
                    Element apply = CodeGeneratorUtils.createElement(instruction.getOwnerDocument(), mtUri, "apply");
                    Element getFieldValue = CodeGeneratorUtils.createElement(instruction.getOwnerDocument(), smlUri, "getFieldValue");
                    apply.appendChild(getFieldValue);
                    apply.appendChild(variablesUsed.item(i).getFirstChild().cloneNode(true));
                    //Once the first child of the equal has been moved, the second child converts into the first child
                    variablesUsed.item(i).getParentNode().replaceChild(apply, variablesUsed.item(i));
                }
            }
        }
  
        //3
        variablesUsed = CodeGeneratorUtils.find(instruction, ".//mt:apply/mt:eq[(not(ancestor::mt:math[parent::sml:if]) "
                    + "or ancestor::mt:math[parent::sml:then]) and (not(ancestor::mt:math[parent::sml:while]) or "
                    + "ancestor::mt:math[parent::sml:loop])]");
        for (int i = variablesUsed.getLength() - 1; i >= 0; i--) {
            String variable = xmlTransform(variablesUsed.item(i).getNextSibling(), null);
            if (variable.indexOf("(") != -1) {
                variable = variable.substring(0, variable.indexOf("("));
                if (vars.contains(variable) || variable.equals("region") || variable.equals("newRegion")) {
                    Element apply = CodeGeneratorUtils.createElement(instruction.getOwnerDocument(), mtUri, "apply");
                    Element setFieldValue = CodeGeneratorUtils.createElement(instruction.getOwnerDocument(), smlUri, "setFieldValue");
                    apply.appendChild(setFieldValue);
                    if (variablesUsed.item(i).getNextSibling().getLocalName().equals("sharedVariable")) {
                        Element sharedVariable = CodeGeneratorUtils.createElement(instruction.getOwnerDocument(), smlUri, "sharedVariable");
                        sharedVariable.appendChild(variablesUsed.item(i).getNextSibling().getFirstChild().getFirstChild().cloneNode(true));
                        apply.appendChild(sharedVariable);
                    }
                    else {
                        apply.appendChild(variablesUsed.item(i).getNextSibling().getFirstChild().cloneNode(true));
                    }
                    //Once the first child of the equal has been moved, the second child converts into the first child
                    apply.appendChild(variablesUsed.item(i).getNextSibling().getNextSibling().cloneNode(true));
                    variablesUsed.item(i).getParentNode().getParentNode().replaceChild(apply, variablesUsed.item(i).getParentNode());
                }
            }
        }
        
        //4
        for (int i = 0; i < coordinates.size(); i++) {
            String coord = coordinates.get(i);
            //right-used variables
            NodeList positions = CodeGeneratorUtils.find(instruction, ".//mt:ci[not(((preceding-sibling::mt:eq and position() != 2) or " 
                + "preceding-sibling::sml:sharedVariable[preceding-sibling::mt:eq and position() != 2]) and " 
                + "(not(ancestor::mt:math[parent::sml:if]) or ancestor::mt:math[parent::sml:then]) and (not(ancestor::mt:math[parent::sml:while]) " 
                + "or ancestor::mt:math[parent::sml:loop])) and child::*[name() = 'mt:msup' or name() = 'mt:msub' or name() = 'mt:msubsup']/*"
                + "[position() = 1 and name() = 'mt:ci' and text() = '" + coord + "']]");
            for (int j = 0; j < positions.getLength(); j++) {
                if (movement) {
                    Element ci = CodeGeneratorUtils.createElement(instruction.getOwnerDocument(), mtUri, "ci");
                    ci.setTextContent("newPosition[" + i + "]");
                    positions.item(j).getParentNode().replaceChild(ci, positions.item(j));
                }
                else {
                    Element apply = CodeGeneratorUtils.createElement(instruction.getOwnerDocument(), mtUri, "apply");
                    Element setFieldValue = CodeGeneratorUtils.createElement(instruction.getOwnerDocument(), smlUri, "getFieldValue");
                    apply.appendChild(setFieldValue);
                    positions.item(j).getFirstChild().getFirstChild().setTextContent("position" + coord);
                    apply.appendChild(positions.item(j).cloneNode(true));
                    positions.item(j).getParentNode().replaceChild(apply, positions.item(j));
                }
            }
            //left-assigned variables
            positions = CodeGeneratorUtils.find(instruction, ".//*[(preceding-sibling::mt:eq and position() = 2 and (not(ancestor::mt:math[" 
                + "parent::sml:if]) or ancestor::mt:math[parent::sml:then]) and (not(ancestor::mt:math[parent::sml:while]) or " 
                + "ancestor::mt:math[parent::sml:loop])) and (name() = 'mt:ci' or parent::sml:sharedVariable) and descendant::*[name() = 'mt:msup' or"
                + " name() = 'mt:msub' or name() = 'mt:msubsup']/*[position() = 1 and (name() = 'mt:ci' or ancestor::sml:sharedVariable) " 
                + "and text() = '" + coord + "']]");
            for (int j = 0; j < positions.getLength(); j++) {
                Element apply = CodeGeneratorUtils.createElement(instruction.getOwnerDocument(), mtUri, "apply");
                Element setFieldValue = CodeGeneratorUtils.createElement(instruction.getOwnerDocument(), smlUri, "setFieldValue");
                apply.appendChild(setFieldValue);
                positions.item(j).getFirstChild().getFirstChild().setTextContent("position" + coord);
                apply.appendChild(positions.item(j).cloneNode(true));
                apply.appendChild(positions.item(j).getParentNode().getLastChild().cloneNode(true));
                positions.item(j).getParentNode().getParentNode().replaceChild(apply, positions.item(j).getParentNode());
            }
        }
        
        //5
        NodeList neighbours = CodeGeneratorUtils.find(instruction, ".//sml:setFieldValue[ancestor::sml:neighbourParticle]");
        for (int i = neighbours.getLength() - 1; i >= 0; i--) {
            Element setFieldValue = CodeGeneratorUtils.createElement(instruction.getOwnerDocument(), smlUri, "setFieldValueb");
            neighbours.item(i).getParentNode().replaceChild(setFieldValue, neighbours.item(i));
        }
        neighbours = CodeGeneratorUtils.find(instruction, ".//sml:getFieldValue[ancestor::sml:neighbourParticle]");
        for (int i = neighbours.getLength() - 1; i >= 0; i--) {
            Element setFieldValue = CodeGeneratorUtils.createElement(instruction.getOwnerDocument(), smlUri, "getFieldValueb");
            neighbours.item(i).getParentNode().replaceChild(setFieldValue, neighbours.item(i));
        }
        //Remove neighbourParticle
        neighbours = CodeGeneratorUtils.find(instruction, ".//sml:neighbourParticle");
        for (int i = neighbours.getLength() - 1; i >= 0; i--) {
        	DocumentFragment children = instruction.getOwnerDocument().createDocumentFragment();
            NodeList npChildren = neighbours.item(i).getChildNodes();
            for (int j = 0; j < npChildren.getLength(); j++) {
            	children.appendChild(npChildren.item(j).cloneNode(true));
            }
            Node parent = neighbours.item(i).getParentNode();
            parent.replaceChild(children, neighbours.item(i));
        }
        
        //6
        NodeList distances = CodeGeneratorUtils.find(instruction, ".//sml:particleDistance");
        for (int i = distances.getLength() - 1; i >= 0; i--) {
            Element apply = CodeGeneratorUtils.createElement(instruction.getOwnerDocument(), mtUri, "apply");
            Element particle = CodeGeneratorUtils.createElement(instruction.getOwnerDocument(), mtUri, "ci");
            particle.setTextContent("particle");
            Element child = (Element) distances.item(i);
            Element parent = (Element) child.getParentNode();
            while (!parent.getNodeName().equals("mt:apply")) {
                child = parent;
                parent = (Element) child.getParentNode();
            }
            apply.appendChild(child.cloneNode(true));
            apply.appendChild(particle);
            parent.replaceChild(apply, child);
        }
        //7
        implementParticleLoopPerformance(instruction);
        
        return (Element) instruction;
    }
    
    /**
     * Implement loop performance: B-A & A-B calculation at same iteration.
     * @param instruction       The node where to implement the performance
     * @throws CGException      CG00X External error
     */
    private static void implementParticleLoopPerformance(Node instruction) throws CGException {
        NodeList sumOver = CodeGeneratorUtils.find(instruction, ".//sml:iterateOverInteractions");
        for (int i = 0; i < sumOver.getLength(); i++) {
            DocumentFragment baInteractions = instruction.getOwnerDocument().createDocumentFragment();
            //Add inside operations to the if
            NodeList children = sumOver.item(i).getChildNodes();
            //The first child is the influence radius. It has to be ignored. Start at 1.
            for (int j = 1; j < children.getLength(); j++) {
                baInteractions.appendChild(children.item(j).cloneNode(true));
            }
            //Swap A and B particles of the if
            NodeList swaps = CodeGeneratorUtils.find(baInteractions,
                    ".//sml:setFieldValueb|.//sml:setFieldValue|.//sml:getFieldValueb|.//sml:getFieldValue");
            for (int j = swaps.getLength() - 1; j >= 0; j--) {
                String nodeName = swaps.item(j).getLocalName();
                if (nodeName.equals("setFieldValueb")) {
                    Element newOp = CodeGeneratorUtils.createElement(instruction.getOwnerDocument(), smlUri, "setFieldValue");
                    swaps.item(j).getParentNode().replaceChild(newOp, swaps.item(j));
                }
                if (nodeName.equals("setFieldValue")) {
                    Element newOp = CodeGeneratorUtils.createElement(instruction.getOwnerDocument(), smlUri, "setFieldValueb");
                    swaps.item(j).getParentNode().replaceChild(newOp, swaps.item(j));
                }
                if (nodeName.equals("getFieldValueb")) {
                    Element newOp = CodeGeneratorUtils.createElement(instruction.getOwnerDocument(), smlUri, "getFieldValue");
                    swaps.item(j).getParentNode().replaceChild(newOp, swaps.item(j));
                }
                if (nodeName.equals("getFieldValue")) {
                    Element newOp = CodeGeneratorUtils.createElement(instruction.getOwnerDocument(), smlUri, "getFieldValueb");
                    swaps.item(j).getParentNode().replaceChild(newOp, swaps.item(j));
                }
            }
            //Add the if to the end of the operations
            sumOver.item(i).appendChild(baInteractions);
        }
    }
    
    /**
     * Get a variable declaration for the variables used in the scope that are not declared.
     * @param scope             The scope of the variables
     * @param pi           		Problem information
     * @param indent            The actual indent to apply in the declarations
     * @param excludedVars		Variables to exlude in the declaration
     * @param patchAcess        The patch of the variable
     * @param init              If there must be a synchronization
     * @param insideFunction	Declaration inside a function
     * @return                  The declaration
     * @throws CGException      CG00X External error
     */
    public static String getPDEVariableDeclaration(Node scope, ProblemInfo pi, int indent, ArrayList<String> excludedVars, 
            String patchAcess, boolean init, boolean insideFunction) throws CGException {
    	ArrayList<String> coords = pi.getCoordinates();
    	ArrayList<String> vars = pi.getVariables();
    	ArrayList<String> exFuncVars = pi.getExtrapolFuncVars();
    	ExtrapolationType extrapolationType = pi.getExtrapolationType();
    	ArrayList<String> particleVars = pi.getVariableInfo().getAllParticleVariables();
    	Set<String> positionVars = pi.getVariableInfo().getAllPositionVariables();
    	
    	ArrayList<String> excluded = new ArrayList<String>(excludedVars);
        FileReadInformation fri = pi.getFileReadInfo();
        for (String fileName : fri.getFileNameInfo().keySet()) {
        	int nCoords = fri.getFileNameInfo().get(fileName);
            excluded.add("vars_data_" + fileName);
            excluded.add("vars_size_" + fileName);
            for (int i = 0; i < nCoords; i++) {
            	excluded.add("coord" + i + "_data_" + fileName);
            	excluded.add("coord" + i + "_dx_" + fileName);
            	excluded.add("coord" + i + "_size_" + fileName);
            }
    	}
    	excluded.addAll(pi.getFunctionNames());
    	
        String doubleVars = "";
        String integerVars = "";
        //Set the actual indent string
        String currentIndent = "";
        for (int i = 0; i < indent; i++) {
            currentIndent = currentIndent + INDENT;
        }

        String declaration = "";

        HashSet<String> variables = new HashSet<String>();
      
        ArrayList<String> excludedFromFunctionCalls = new ArrayList<String>();
        for (int k = 0; k < coords.size(); k++) {
            //Initialization of the variables
            String coordString = coords.get(k);
            excludedFromFunctionCalls.add(coordString + "last");
        }
        excludedFromFunctionCalls.add("dx");
        excludedFromFunctionCalls.add("simPlat_dt");
        NodeList paramList = CodeGeneratorUtils.find(scope.getOwnerDocument(), "/*/mms:parameters/mms:parameter");
        for (int i = 0; i < paramList.getLength(); i++) {
            String name = SAMRAIUtils.variableNormalize(paramList.item(i).getFirstChild().getTextContent());
            excludedFromFunctionCalls.add(name);
        }
        
        //If in Initialization, add all fields.
        if (init) {
        	NodeList fields = CodeGeneratorUtils.find(scope.getOwnerDocument(), "/*/mms:fields/mms:field|/*/mms:auxiliaryFields/mms:auxiliaryField");
        	for (int j = 0; j < fields.getLength(); j++) {
                String variable = SAMRAIUtils.variableNormalize(fields.item(j).getTextContent());
                if (!particleVars.contains(variable)) {
	                declaration = declaration + currentIndent + "double* " + variable + " = ((pdat::NodeData<double> *) " 
	                        + patchAcess + "getPatchData(d_" + variable + "_id).get())->getPointer();" + NEWLINE
	                        + currentIndent + "((pdat::NodeData<double> *) patch.getPatchData(d_" 
	                        + variable + "_id).get())->fillAll(0);" + NEWLINE;
	                variables.add(variable);
                }
        	}
        }
        
        //Get variables used in the right side and inside a function
        String query = ".//mt:apply[child::*[1][name() = 'mt:ci' or ancestor::sml:sharedVariable]]|.//mt:ci[parent::sml:functionCall or parent::sml:readFromFile or ancestor::mms:externalInitialCondition]|"
                + ".//sml:sharedVariable[parent::sml:functionCall]|.//mt:apply[child::*[1][name() = 'mt:msup']/*[1][name() = 'mt:ci' " 
                + "or ancestor::sml:sharedVariable]]";
        Set<Node> variablesUsed = CodeGeneratorUtils.findWithoutDuplicates(scope, query);
        Iterator<Node> variableIt = variablesUsed.iterator();
        while (variableIt.hasNext()) {
            String variable = CodeGeneratorUtils.xmlVarToString(variableIt.next(), false, false);
            if (!positionVars.contains(variable) && !particleVars.contains(variable) && !variables.contains(variable) && (vars.contains(variable) || exFuncVars.contains(variable)) && !excluded.contains(variable) && !variable.equals("region") && !variable.equals("newRegion")) {
            	if (!insideFunction) {
                    declaration = declaration + currentIndent + "double* " + variable + " = ((pdat::NodeData<double> *) " 
                            + patchAcess + "getPatchData(d_" + variable + "_id).get())->getPointer();" + NEWLINE;
            	}
            	else {
                    doubleVars = doubleVars + ", " + variable;
            	}
                variables.add(variable);	
            }
        }
        //Get the variables assigned in the code
        query = ".//mt:math[(not(parent::sml:if) or parent::sml:then) and (not(parent::sml:while) or parent::sml:loop) and not(parent::sml:return) " 
                + "and not(ancestor::mms:applyIf) and not(ancestor::mms:finalizationCondition)]/mt:apply/mt:eq/following-sibling::*[1][self::mt:*]";
        variablesUsed = CodeGeneratorUtils.findWithoutDuplicates(scope, query);
        variableIt = variablesUsed.iterator();
        while (variableIt.hasNext()) {
            Node varNode = variableIt.next();
            String variable = CodeGeneratorUtils.xmlVarToString(varNode, false, false);
            if (!positionVars.contains(variable) && !particleVars.contains(variable) && !variables.contains(variable) && !excluded.contains(variable) && !variable.equals("influenceRadius") && !variable.equals("particle_distance") && !variable.equals("region") && !variable.equals("newRegion")) {
                if (CodeGeneratorUtils.isMeshVar(varNode) && !insideFunction) {
                    if (vars.contains(variable) || exFuncVars.contains(variable)) {
                        declaration = declaration + currentIndent + "double* " + variable + " = ((pdat::NodeData<double> *) " 
                                + patchAcess + "getPatchData(d_" + variable + "_id).get())->getPointer();" + NEWLINE;
                        if (init) {
                            declaration = declaration + currentIndent + "((pdat::NodeData<double> *) patch.getPatchData(d_" 
                                    + variable + "_id).get())->fillAll(0);" + NEWLINE;
                        }
                        variables.add(variable);
                    }
                }
                else {
                    if (variable.startsWith("ReflectionCounter_")) {
                        integerVars = integerVars + ", " + variable;
                    }
                    else {
                        doubleVars = doubleVars + ", " + variable;
                    }
                    variables.add(variable);
                }
            }
        }
        //Get variables used inside a function
        query = ".//mt:ci[position() > 1][(parent::sml:functionCall or parent::sml:readFromFile) and not(ancestor::sml:fieldExtrapolation)]";
        variablesUsed = CodeGeneratorUtils.findWithoutDuplicates(scope, query);
        variableIt = variablesUsed.iterator();
        while (variableIt.hasNext()) {
            Node varNode = variableIt.next();
            String variable = CodeGeneratorUtils.xmlVarToString(varNode, false, false);
            if (!positionVars.contains(variable) && !particleVars.contains(variable) && !variables.contains(variable) && !excludedFromFunctionCalls.contains(variable) && !coords.contains(variable) && !excluded.contains(variable) && !CodeGeneratorUtils.isMeshVar(varNode) && !variable.equals("influenceRadius") && !variable.equals("particle_distance")) {
                doubleVars = doubleVars + ", " + variable;
                variables.add(variable);
            }
        }
        //Fill with extrapolation variables used in function calls.
        NodeList extrapolations = CodeGeneratorUtils.find(scope, ".//sml:fieldExtrapolation/mt:math/sml:functionCall");        
        for (int j = 0; j < extrapolations.getLength(); j++) {
            NodeList parameters = CodeGeneratorUtils.find(extrapolations.item(j), "./*");
            if (extrapolationType == ExtrapolationType.axisPDE) {
            	for (int i = 0; i < coords.size(); i++) {
                    String to = new String(CodeGeneratorUtils.xmlVarToString(parameters.item(i * 3 + 2), false, false));
                    if (!particleVars.contains(to) && !variables.contains(to)) {
                        declaration = declaration + currentIndent + "double* " + to + " = ((pdat::NodeData<double> *) " 
                                + patchAcess + "getPatchData(d_" + to + "_id).get())->getPointer();" + NEWLINE;
                        variables.add(to);
                    }
                }
            }
            
            if (parameters.item(0).getTextContent().equals("extrapolate_field")) {
            	String finalTo = "";
            	switch (extrapolationType) {
            		case generalPDE:
            			finalTo = new String(CodeGeneratorUtils.xmlVarToString(parameters.item(coords.size() * 2 + 1), false, false));
            			break;
            		case axisPDE:
            			finalTo = new String(CodeGeneratorUtils.xmlVarToString(parameters.item(coords.size() * 3 + 1), false, false));
            			break;
            	}
                if (!particleVars.contains(finalTo) && !variables.contains(finalTo)) {
                    declaration = declaration + currentIndent + "double* " + finalTo + " = ((pdat::CellData<double> *) " 
                            + patchAcess + "getPatchData(d_" + finalTo + "_id).get())->getPointer();" + NEWLINE;
                    variables.add(finalTo);
                }
            } 
        }
        extrapolations = CodeGeneratorUtils.find(scope, ".//sml:fieldExtrapolation/mt:math/sml:functionCall/mt:ci[text() = 'extrapolate_field']");
        if (extrapolations.getLength() > 0) {
            NodeList fields = CodeGeneratorUtils.find(scope.getOwnerDocument(), "/*/mms:fields/mms:field|/*/mms:auxiliaryFields/mms:auxiliaryField");
            if (extrapolationType != ExtrapolationType.generalPDE) {
                for (int i = 0; i < coords.size(); i++) {
                    for (int j = 0; j < fields.getLength(); j++) {
                        String field = SAMRAIUtils.variableNormalize(fields.item(j).getTextContent());
                        String variable = "extrapolatedSB" + field + "SP" + coords.get(i);
                        if (!particleVars.contains(variable) && !variables.contains(variable)) {
                            declaration = declaration + currentIndent + "double* " + variable + " = ((pdat::NodeData<double> *) " 
                                    + patchAcess + "getPatchData(d_" + variable + "_id).get())->getPointer();" + NEWLINE;
                            variables.add(variable);
                        }
                    }
                }
            }
        }
        if (!doubleVars.equals("")) {
            doubleVars = currentIndent + "double " + doubleVars.substring(2) + ";" + NEWLINE;
        }
        if (!integerVars.equals("")) {
            integerVars = currentIndent + "int " + integerVars.substring(2) + ";" + NEWLINE;
        }
        return declaration + doubleVars + integerVars;
    }
    
    /**
     * Get a variable declaration for the variables used in the scope that are not declared.
     * @param scope             The scope of the variables
     * @param pi           		Problem information
     * @param indent            The actual indent to apply in the declarations
     * @param excludedVars		Variables to exlude in the declaration
     * @return                  The declaration
     * @throws CGException      CG00X External error
     */
    public static String getPDELocalVariableDeclaration(Node scope, ProblemInfo pi, int indent, ArrayList<String> excludedVars) throws CGException {
    	ArrayList<String> coords = pi.getCoordinates();
    	ArrayList<String> particleVars = pi.getVariableInfo().getAllParticleVariables();
    	Set<String> positionVars = pi.getVariableInfo().getAllPositionVariables();
    	
        String doubleVars = "";
        String integerVars = "";
        //Set the actual indent string
        String currentIndent = "";
        for (int i = 0; i < indent; i++) {
            currentIndent = currentIndent + INDENT;
        }

        String declaration = "";

        HashSet<String> variables = new HashSet<String>();
        ArrayList<String> excludedFromFunctionCalls = new ArrayList<String>();
        for (int k = 0; k < coords.size(); k++) {
            //Initialization of the variables
            String coordString = coords.get(k);
            excludedFromFunctionCalls.add(coordString + "last");
        }
        excludedFromFunctionCalls.add("dx");
        excludedFromFunctionCalls.add("simPlat_dt");
        NodeList paramList = CodeGeneratorUtils.find(scope.getOwnerDocument(), "/*/mms:parameters/mms:parameter");
        for (int i = 0; i < paramList.getLength(); i++) {
            String name = SAMRAIUtils.variableNormalize(paramList.item(i).getFirstChild().getTextContent());
            excludedFromFunctionCalls.add(name);
        }
        
        //Get the variables assigned in the code
        String query = ".//mt:math[(not(parent::sml:if) or parent::sml:then) and (not(parent::sml:while) or parent::sml:loop) and not(parent::sml:return) " 
                + "and not(ancestor::mms:applyIf) and not(ancestor::mms:finalizationCondition)]/mt:apply/mt:eq/following-sibling::*[1][self::mt:*]";
        Set<Node> variablesUsed = CodeGeneratorUtils.findWithoutDuplicates(scope, query);
        Iterator<Node> variableIt = variablesUsed.iterator();
        while (variableIt.hasNext()) {
            Node varNode = variableIt.next();
            String variable = CodeGeneratorUtils.xmlVarToString(varNode, false, false);
            if (!positionVars.contains(variable) && !particleVars.contains(variable) && !variables.contains(variable) && !excludedVars.contains(variable) && !variable.equals("influenceRadius") && !variable.equals("particle_distance") && !variable.equals("region") && !variable.equals("newRegion")) {
                if (!CodeGeneratorUtils.isMeshVar(varNode)) {
                    if (variable.startsWith("ReflectionCounter_")) {
                        integerVars = integerVars + ", " + variable;
                    }
                    else {
                        doubleVars = doubleVars + ", " + variable;
                    }
                    variables.add(variable);
                }
            }
        }
        //Get variables used inside a function
        query = ".//mt:ci[position() > 1][parent::sml:functionCall and not(ancestor::sml:fieldExtrapolation)]";
        variablesUsed = CodeGeneratorUtils.findWithoutDuplicates(scope, query);
        variableIt = variablesUsed.iterator();
        while (variableIt.hasNext()) {
            Node varNode = variableIt.next();
            String variable = CodeGeneratorUtils.xmlVarToString(varNode, false, false);
            if (!positionVars.contains(variable) && !particleVars.contains(variable) && !variables.contains(variable) && !excludedFromFunctionCalls.contains(variable) && !coords.contains(variable) && !excludedVars.contains(variable) && !CodeGeneratorUtils.isMeshVar(varNode) && !variable.equals("influenceRadius") && !variable.equals("particle_distance")) {
                doubleVars = doubleVars + ", " + variable;
                variables.add(variable);
            }
        }
        
        if (!doubleVars.equals("")) {
            doubleVars = currentIndent + "double " + doubleVars.substring(2) + ";" + NEWLINE;
        }
        if (!integerVars.equals("")) {
            integerVars = currentIndent + "int " + integerVars.substring(2) + ";" + NEWLINE;
        }
        return declaration + doubleVars + integerVars;
    }
    
    /**
     * Adds the hard distance field variables declaration.
     * @param coords            The coordinates of the problem
     * @param fieldGroups      	The hard region extrapolated field groups
     * @param indent            The indent
     * @return                  The code
     * @throws CGException      CG003 Not possible to create code for the platform
     *                          CG00X External error
     */
    public static String addHardFieldDistDeclaration(ArrayList<String> coords, LinkedHashMap<String, ArrayList<String>> fieldGroups, int indent) throws CGException {
        //Set the actual indent string
        String actualIndent = "";
        for (int i = 0; i < indent; i++) {
            actualIndent = actualIndent + INDENT;
        }
        String result = "";
        //Fill with distance variables for hard region boundaries
    	Iterator<String> groupsIt = fieldGroups.keySet().iterator();
    	while (groupsIt.hasNext()) {
     		String groupName = SAMRAIUtils.variableNormalize(groupsIt.next());
            for (int j = 0; j < coords.size(); j++) {
                result = result + actualIndent + "double* d_" + coords.get(j) + "_" + groupName 
                        + " = ((pdat::NodeData<double> *) patch->getPatchData(d_d_" + coords.get(j) 
                        + "_" + groupName + "_id).get())->getPointer();" + NEWLINE;
            }		
    	}
        
        return result;
    }
    
    /**
     * Get a variable declaration for the variables used in the scope that are not declared.
     * @param scope             The scope of the variables
     * @param indent            The actual indent to apply in the declarations
     * @param pi                The problem information
     * @return                  The declaration
     * @throws CGException      CG00X External error
     */
    public static String getParticlesVariableDeclaration(Node scope, int indent, ProblemInfo pi) throws CGException {
        String[][] xslParams = new String [5][2];
        xslParams[0][0] = "condition";
        xslParams[0][1] = "false";
        xslParams[1][0] = "indent";
        xslParams[1][1] = "0";
        xslParams[2][0] = "dimensions";
        xslParams[2][1] = String.valueOf(pi.getDimensions());
        xslParams[3][0] = "coords";
        if (pi.isHasParticleFields()) {
            xslParams[3][1] = pi.getCoordinates().toString();
        }
        else {
            xslParams[3][1] = "";
        }
        xslParams[4][0] = "timeCoord";
        xslParams[4][1] = pi.getTimeCoord();
        //Set the actual indent string
        String actualIndent = "";
        for (int i = 0; i < indent; i++) {
            actualIndent = actualIndent + INDENT;
        }
        
        //Get variables used in the right side and inside a function
        String declaration = "";
        HashSet<String> variables = new HashSet<String>();

        //Get the variables assigned in the code
        String query = ".//mt:math[(not(parent::sml:if) or parent::sml:then) and (not(parent::sml:while) or parent::sml:loop) "
            + "and not(parent::sml:return) and " 
            + "not(ancestor::mms:applyIf) and not(ancestor::mms:finalizationCondition)]/mt:apply/mt:eq/following-sibling::*[1]";
        NodeList leftTerms = CodeGeneratorUtils.find(scope, query);
        for (int i = 0; i < leftTerms.getLength(); i++) {
       
            String variable = SAMRAIUtils.xmlTransform(leftTerms.item(i), xslParams);
            if (((!variable.contains("(") && pi.isHasParticleFields()) || //SPH
                    (!variable.contains("particle->") && pi.isHasABMMeshlessFields())) //ABM
                    && !variables.contains(variable) && !pi.getVariableInfo().getAllPositionVariables().contains(variable)) {
                declaration = declaration + actualIndent + "double " + variable + ";" + NEWLINE;
                variables.add(variable);
            }
        }
        
        return declaration;
    }
    
    /**
     * Get the assigned variables that has to be synchronized after the current block.
     * Get also the asigned variables inside extrapolation functions.
     * @param blockScope        The actual block
     * @param variables         All the variables of the problem
     * @param dims              The spatial dimensions
     * @param extrapolationType	The extrapolation type
     * @return                  The variables assigned in the block
     * @throws CGException      CG00X External error
     */
    public static ArrayList<String> getSyncVariables(Node blockScope, ArrayList<String> variables, int dims, 
    		ExtrapolationType extrapolationType) throws CGException {
        ArrayList<String> candidates = new ArrayList<String>();
        //Assigned variables
        String query = ".//mt:math/mt:apply[not(ancestor::sml:iterateOverParticles) and not(ancestor::mt:math[parent::sml:if or @sml:type='noSync']) and not(ancestor::mt:math[parent::sml:while])]/mt:eq/following-sibling::*[1]" 
	                    + "[name()='sml:sharedVariable' or name()='mt:apply']";
        NodeList variablesUsed = CodeGeneratorUtils.find(blockScope, query);
        for (int j = variablesUsed.getLength() - 1; j >= 0; j--) {
            String variable = CodeGeneratorUtils.xmlVarToString(variablesUsed.item(j), true, false);
            if (variable != null && !candidates.contains(variable) && variables.contains(variable)) {
                candidates.add(variable);
            }
        }
        //Function variables
        query = ".//sml:fieldExtrapolation//sml:functionCall";
        NodeList extrapolations = CodeGeneratorUtils.find(blockScope, query);
        for (int j = extrapolations.getLength() - 1; j >= 0; j--) {
            NodeList params = extrapolations.item(j).getChildNodes();
            for (int i = 0; i < dims; i++) {
                if (extrapolationType == ExtrapolationType.axisPDE) {
                	String variable = CodeGeneratorUtils.xmlVarToString(params.item(i * 3 + 2).cloneNode(true), false, false);
                    if (!candidates.contains(variable) && variables.contains(variable)) {
                        candidates.add(variable);
                    }
                }
            }
            //If the extrapolation function is for fields the field should also be extrapolated (not only the axis variables)
      
            if (params.item(0).getTextContent().equals("extrapolate_field")) {
            	String variable = "";
            	switch (extrapolationType) {
            		case axisPDE:
            			variable = CodeGeneratorUtils.xmlVarToString(params.item(dims * 3 + 1).cloneNode(true), false, false);
            			break;
            		case generalPDE:
            			variable = CodeGeneratorUtils.xmlVarToString(params.item(dims * 2 + 1).cloneNode(true), false, false);
            			break;
            	}
                if (!candidates.contains(variable) && variables.contains(variable)) {
                    candidates.add(variable);
                }
            }
        }
        return candidates;
    }
   
    /**
     * Get the used variables that has to be synchronized after the current block.
     * @param blockScope        The actual block
     * @param variables         All the variables of the problem
     * @return                  The variables assigned in the block
     * @throws CGException      CG00X External error
     */
    public static ArrayList<String> getRightVariables(Node blockScope, ArrayList<String> variables) throws CGException {
        ArrayList<String> candidates = new ArrayList<String>();
        String query = ".//mt:apply[child::*[1][name() = 'mt:ci' or ancestor::sml:sharedVariable]]|.//mt:ci[parent::sml:functionCall]|" 
            + ".//sml:sharedVariable[parent::sml:functionCall]|.//mt:apply[child::*[1][name() = 'mt:msup']/*[1][name() = 'mt:ci' or " 
            + "parent::sml:sharedVariable]]";
        NodeList variablesUsed = CodeGeneratorUtils.find(blockScope, query);
        for (int j = variablesUsed.getLength() - 1; j >= 0; j--) {
            String variable = SAMRAIUtils.xmlTransform(variablesUsed.item(j), null);
            if (variable.indexOf("(") != -1) {
                variable = variable.substring(0, variable.indexOf("("));
                if (!candidates.contains(variable) && variables.contains(variable)) {
                    candidates.add(variable);
                }
            }
        }
        
        return candidates;
    }
    
    /**
     * Gets the field variables with the SAMRAI normalization.
     * @param ftl               The field time levels
     * @return                  The list of field variables
     * @throws CGException      CG003 Not possible to create code for the platform 
     *                          CG00X External error
     */
    public static ArrayList<String> getFieldVariables(LinkedHashMap<String, Integer> ftl) throws CGException {
        ArrayList<String> fields = new ArrayList<String>();
        Iterator<String> fieldIt = ftl.keySet().iterator();
        while (fieldIt.hasNext()) {
            String field = fieldIt.next();
            int timeLevel = ftl.get(field);
            field = SAMRAIUtils.variableNormalize(field);
            fields.add(field);
            for (int i = 1; i < timeLevel; i++) {
                field = field + "_p";
                fields.add(field);
            }
        }
        return fields;
    }
    
    /**
     * Gets the field variables with the SAMRAI normalization.
     * @param ftl               The field time levels
     * @param field				Field filter
     * @return                  The list of field variables
     * @throws CGException      CG003 Not possible to create code for the platform 
     *                          CG00X External error
     */
    public static ArrayList<String> getFieldVariables(LinkedHashMap<String, Integer> ftl, String field) throws CGException {
        ArrayList<String> fields = new ArrayList<String>();
        Iterator<String> fieldIt = ftl.keySet().iterator();
        while (fieldIt.hasNext()) {
            String f = fieldIt.next();
            int timeLevel = ftl.get(f);
            f = SAMRAIUtils.variableNormalize(f);
            if (f.equals(field)) {
	            fields.add(f);
	            for (int i = 1; i < timeLevel; i++) {
	                f = f + "_p";
	                fields.add(f);
	            }
            }
        }
        return fields;
    }
    
    /**
     * Gets the left condition in a boundary.
     * @param calculateCorners   	If corners have to be checked
     * @param stencil               The stencil
     * @param regionBaseNumber     	The number of the equivalent internal region
     * @param i                     Iteration i
     * @param coordinates           The spatial coordinates
     * @param regions              	The region identifiers
     * @return                      The code
     */
    public static String getLeftCondition(boolean calculateCorners, int stencil, int regionBaseNumber, int i, ArrayList<String> coordinates,
            ArrayList<String> regions) {
        String condition = "";
        for (int l = 0; l < stencil; l++) {
            String stencilIndex = "";
            for (int m = 0; m < coordinates.size(); m++) {
                if (i == m) {
                    stencilIndex =  stencilIndex + coordinates.get(m) + " + " + (l + 1) + ", ";
                }
                else {
                    stencilIndex =  stencilIndex + coordinates.get(m) + ", ";
                }
            }
            stencilIndex = stencilIndex.substring(0, stencilIndex.lastIndexOf(","));
            condition = condition + "(" +  coordinates.get(i) + " + " + (l + 1) + " < " + coordinates.get(i) + "last && (";
            if (regions.contains(String.valueOf(regionBaseNumber))) {
                condition = condition + "vector(FOV_" + regionBaseNumber + "," + stencilIndex + ") > 0";
            }
            if (regions.contains(String.valueOf(regionBaseNumber)) && regions.contains(String.valueOf(regionBaseNumber + 1))) {
                condition = condition + " || ";
            }
            if (regions.contains(String.valueOf(regionBaseNumber + 1))) {
                condition = condition + "vector(FOV_" + (regionBaseNumber + 1) + ", " + stencilIndex + ") > 0";
            }
            condition = condition + ")) || ";
        }
        
        return condition.substring(0, condition.lastIndexOf(" ||"))
                + getOperatedConditionDiagonal(calculateCorners, i, "+", coordinates, stencil, regionBaseNumber, regions);
    }
    
    /**
     * Generate the condition to check diagonals of a point in the mesh. 
     * @param calculateCorners 		If corners have to be checked
     * @param coordinates       	The coordinates
     * @param coord             	Coordinate of the condition
     * @param coordOperation		coordinate operation
     * @param stencil           	The stencil of the problem
     * @param regionBaseNumber 		The region id
     * @param regions          		The region identifiers
     * @return                  	The condition
     */     
    private static String getOperatedConditionDiagonal(boolean calculateCorners, int coord, String coordOperation, ArrayList<String> coordinates, int stencil, 
            int regionBaseNumber, ArrayList<String> regions) {
        if (calculateCorners) {
            String diagonal = "";
            
            ArrayList<Integer> elements = new ArrayList<Integer>();
            elements.add(0);
            for (int j = 1; j <= stencil; j++) {
            	elements.add(j);
            	elements.add(-j);
            }
            ArrayList<ArrayList<Integer>> combinations = CodeGeneratorUtils.createCombinationsInt(coordinates.size(), elements);

            for (int i = 0; i < combinations.size(); i++) {
            	ArrayList<Integer> combination = combinations.get(i);
             	int zeros = 0; 
            	//Given index should be operated
                if ((combination.get(coord) > 0 && coordOperation.equals("+")) || (combination.get(coord) < 0 && coordOperation.equals("-"))) {
                    String stencilIndex = "";
                    String checkIndex = "";
                	for (int k = 0; k < combination.size(); k++) {
                		Integer op = combination.get(k);
                		if (op > 0) {
                			stencilIndex = stencilIndex + coordinates.get(k) + " + " + op + ", ";
                            checkIndex = checkIndex + coordinates.get(k) + " + " + op + " < " + coordinates.get(k) + "last && ";
                		}
                		if (op < 0) {
                            stencilIndex = stencilIndex + coordinates.get(k) + " - " + Math.abs(op) + ", ";
                            checkIndex = checkIndex + coordinates.get(k) + " - " + Math.abs(op) + " >= 0 && ";
                		}
                		if (op == 0) {
                            stencilIndex = stencilIndex + coordinates.get(k) + ", ";
                            zeros++;
                		}
                	}
                	if (zeros < coordinates.size() - 1) {
	                    stencilIndex = stencilIndex.substring(0, stencilIndex.lastIndexOf(","));
	                    checkIndex = checkIndex.substring(0, checkIndex.lastIndexOf(" && "));
	                    
	                    diagonal = diagonal + "(" + checkIndex + " && (";
	                    if (regions.contains(String.valueOf(regionBaseNumber))) {
	                        diagonal = diagonal + "vector(FOV_" + regionBaseNumber + ", " + stencilIndex + ") > 0";
	                    }
	                    if (regions.contains(String.valueOf(regionBaseNumber)) 
	                            && regions.contains(String.valueOf(regionBaseNumber + 1))) {
	                        diagonal = diagonal + " || ";
	                    }
	                    if (regions.contains(String.valueOf(regionBaseNumber + 1))) {
	                        diagonal = diagonal + "vector(FOV_" + (regionBaseNumber + 1) + ", " + stencilIndex + ") > 0";
	                    }
	                    diagonal = diagonal + ")) || ";
                	}
                }
            }
            return " || (" + diagonal.substring(0, diagonal.lastIndexOf(" || ")) + ")";
        }
		return "";
    }
    
    /**
     * Gets the right condition in a boundary.
     * @param calculateCorners 		If corners have to be checked
     * @param stencil               The stencil
     * @param regionBaseNumber      The number of the equivalent internal region
     * @param i                     Iteration i
     * @param coordinates           The spatial coordinates
     * @param regions               The region identifiers
     * @return                      The code
     */
    public static String getRightCondition(boolean calculateCorners, int stencil, int regionBaseNumber, int i, ArrayList<String> coordinates,
            ArrayList<String> regions) {
        String condition = "";
        for (int l = 0; l < stencil; l++) {
            String stencilIndex = "";
            for (int m = 0; m < coordinates.size(); m++) {
                if (i == m) {
                    stencilIndex =  stencilIndex + coordinates.get(m) + " - " + (l + 1) + ", ";
                }
                else {
                    stencilIndex =  stencilIndex + coordinates.get(m) + ", ";
                }
            }
            stencilIndex = stencilIndex.substring(0, stencilIndex.lastIndexOf(","));
            condition = condition + "(" +  coordinates.get(i) + " - " + (l + 1) + " >= 0 && (";
            if (regions.contains(String.valueOf(regionBaseNumber))) {
                condition = condition + "vector(FOV_" + regionBaseNumber + ", " + stencilIndex + ") > 0";
            }
            if (regions.contains(String.valueOf(regionBaseNumber)) && regions.contains(String.valueOf(regionBaseNumber + 1))) {
                condition = condition + " || ";
            }
            if (regions.contains(String.valueOf(regionBaseNumber + 1))) {
                condition = condition + "vector(FOV_" + (regionBaseNumber + 1) + ", " + stencilIndex + ") > 0";
            }
            condition = condition + ")) || ";
        }
        return condition.substring(0, condition.lastIndexOf(" ||")) 
        		+ getOperatedConditionDiagonal(calculateCorners, i, "-", coordinates, stencil, regionBaseNumber, regions);
    }
    
    
    /**
     * Creates the x3d arrays to be global variables.
     * @param pi                The problem information
     * @return                  The arrays
     * @throws CGException      CG003 Not possible to create code for the platform
     *                          CG00X External error
     */
    public static String x3dArrays(ProblemInfo pi) throws CGException {
        try {
            String result = "";
            //TODO añadir soporte x3d a abm
            if (pi.isHasParticleFields() || pi.isHasMeshFields()) {
            	DocumentManager docMan = new DocumentManagerImpl();
                Document problem = pi.getProblem();
                //int previousTS = 0;
                for (int si = pi.getRegionPrecedence().size() - 1; si >= 0; si--) {
                    String regName = pi.getRegionPrecedence().get(si);
                    Element regionInfo = (Element) CodeGeneratorUtils.find(problem, 
                            "/*/mms:region[descendant::mms:name = '" + regName + "']|/*/mms:subregions/mms:subregion[descendant::mms:name = '" 
                                    + regName + "']").item(0);
                    String x3ds = "";
                    String timeslicesDef = "";
                    if (CodeGeneratorUtils.find(regionInfo, ".//mms:x3d").getLength() > 0) {
                        //TODO borrar
                    //    int normalsNumber = 0;
                        
                        int pointNumber = 0;
                        int unionNumber = 0;
                       /* if (pi.getMovementInfo().itMoves()
                                && CodeGeneratorUtils.find(regionInfo, ".//mms:movement//mms:x3dRegionId").getLength() > 0) {
                            int ts = CodeGeneratorUtils.find(regionInfo, ".//mms:movement//mms:x3dRegionId").getLength();
                            if (previousTS == 0) {
                                previousTS = ts;
                            }
                            else {
                                if (previousTS != ts) {
                                    throw new CGException(CGException.CG003, 
                                            "X3D movement is incorrect. All the x3d must have the same number time steps.");
                                }
                            }
                            String cellPoints = "";
                            String timeslices = "";
                            String pointsS = "";
                            String unionsS = "";
                            int pPointNumber = 0;
                            for (int i = 0; i < ts; i++) {
                                X3DRegion x3d = CodeGeneratorUtils.parseX3dRegion(docMan.getX3D(CodeGeneratorUtils.find(regionInfo, 
                                        ".//mms:movement//mms:x3dRegionId").item(i).getTextContent()));
                                //Fill the point data
                                double[][] samraiPoints = getPoints(x3d);
                                pointNumber = samraiPoints.length;
                                                    
                                //Fill the face data (point unions)
                                String[] unions = getUnions(x3d);
    
                                //If 2D, remove the interior points and reset unions
                                if (pi.getDimensions() == 2) {
                                    samraiPoints = CodeGeneratorUtils.removeInteriorPoints(samraiPoints, unions);
                                    pointNumber = samraiPoints.length;
                                    //Unions from 3D to 2D (contour)
                                    unions = CodeGeneratorUtils.removeRepeatedUnions(unions);
                                }
                                //Some controls
                                if (i > 0) {
                                    if (pointNumber != pPointNumber) {
                                        throw new CGException(CGException.CG003, "Movement for region " + regName 
                                                + " is incorrect. All the x3d must have the same number of points.");
                                    }
                                }
                                else {
                                    pPointNumber = pointNumber;
                                }
                                if (i > 0) {
                                    if (unionNumber != unions.length) {
                                        throw new CGException(CGException.CG003, "Movement for region " + regName 
                                                + " is incorrect. All the x3d must have the same number of unions.");
                                    }
                                }
                                unionNumber = unions.length;
                                
                                
                                String cp = "";
                                for (int j = 0; j < pointNumber; j++) {
                                    cp = cp + "{";
                                    for (int k = 0; k < pi.getDimensions(); k++) {
                                        cp = cp + "1e10, ";
                                    }
                                    cp = cp.substring(0, cp.length() - 2) + "}, ";
                                    if (j % 100 == 99) {
                                        cp = cp + NEWLINE;
                                    }
                                }
                                cellPoints = cellPoints + "{" + cp.substring(0, cp.length() - 2) + "}, " + NEWLINE;
                                String tsTmp = CodeGeneratorUtils.find(regionInfo, 
                                    ".//mms:movement//mms:timeSlice/mms:time").item(i).getTextContent();
                                timeslices = timeslices + tsTmp + ", ";
                                
                                //Print points
                                pointsS = pointsS + printPoints(samraiPoints, pi.getDimensions()) + ", " + NEWLINE;
                                
                                //Print unions
                                if (i == 0) {
                                    unionsS = printUnions(unions);
                                }
                            }
                            cellPoints = cellPoints.substring(0, cellPoints.lastIndexOf(", "));
                            timeslices = timeslices.substring(0, timeslices.lastIndexOf(", "));
                            pointsS = pointsS.substring(0, pointsS.lastIndexOf(", "));
                            String orientationInit = "";
                            for (int i = 0; i < unionNumber; i++) {
                                orientationInit = orientationInit + "9, ";  
                                if (i % 100 == 99) {
                                    orientationInit = orientationInit + NEWLINE;
                                }
                            }
                            orientationInit = orientationInit.substring(0, orientationInit.length() - 2);
                            //Only one time
                            timeslicesDef = "#define TIMESLICES " + ts + NEWLINE
                                    + "const double " + regName + "Time[TIMESLICES] = {" + timeslices + "};" + NEWLINE;
                            //Common information
                            x3ds = x3ds + "//Common information for region: " + regName + NEWLINE
                                    + "static int " + regName + "Orientation[UNIONS_" + regName + "] = {" + orientationInit + "};" + NEWLINE
                                    + "const double " + regName + "Points[TIMESLICES][POINTS_" + regName + "][DIMENSIONS] = {" 
                                    + pointsS + "};" + NEWLINE
                                    + "const int " + regName + "Unions[UNIONS_" + regName + "][DIMENSIONS] = " + unionsS + ";" + NEWLINE
                                    + "static double " + regName + "CellPoints[TIMESLICES][POINTS_" + regName + "][DIMENSIONS] = {" 
                                    + cellPoints + "};" + NEWLINE;
                        }
                        else {*/
                            X3DRegion x3d = CodeGeneratorUtils.parseX3dRegion(docMan.getX3D(CodeGeneratorUtils.find(regionInfo, 
                                  ".//mms:x3d/mms:id").item(0).getTextContent()));
    
                            //Common information
                            x3ds = x3ds + "//Common information for region: " + regName + NEWLINE;
                            
                            //Fill the point data
                          //  double[][] samraiNormals = getNormals(x3d);
                            double[][] samraiPoints = getPoints(x3d);
                            pointNumber = samraiPoints.length;
                        //    normalsNumber = samraiNormals.length;
                                                
                            //Fill the face data (point unions)
                            String[] unions = getUnions(x3d);
    
                            //If 2D, remove the interior points and reset unions
                            if (pi.getDimensions() == 2) {
                                samraiPoints = CodeGeneratorUtils.removeInteriorPoints(samraiPoints, unions);
                                pointNumber = samraiPoints.length;
                                //Unions from 3D to 2D (contour)
                                unions = CodeGeneratorUtils.removeRepeatedUnions(unions);
                            }
                            unionNumber = unions.length;
                            
                            //TODO borrar normales
                          //  x3ds = x3ds + "const double " + regName + "Normals[NORMALS_" + regName + "][DIMENSIONS] = " 
                          //          + printNormals(samraiNormals, pi.getDimensions()) + ";" + NEWLINE;
                            
                            
                            //Print points
                            x3ds = x3ds + "const double " + regName + "Points[POINTS_" + regName + "][DIMENSIONS] = " 
                                    + printPoints(samraiPoints, pi.getDimensions()) + ";" + NEWLINE;
                            
                            //Print unions
                            x3ds = x3ds + "const int " + regName + "Unions[UNIONS_" + regName + "][DIMENSIONS] = " 
                                    + printUnions(unions) + ";" + NEWLINE;
                       // }
    
                        result = result + timeslicesDef
                      //          + "#define NORMALS_" + regName + " " + normalsNumber + NEWLINE
                                + "#define POINTS_" + regName + " " + pointNumber + NEWLINE
                                + "#define UNIONS_" + regName + " " + unionNumber + NEWLINE
                                + x3ds;
                    }
                }
            }
            return result;
        }
        catch (DMException e) {
            throw new CGException(CGException.CG00X, e.getMessage());
        } 
    }
   
    
    
    //TODO borrar
    public static double[][] getNormals(X3DRegion x3d) {
        //X3D is always a 3D point file. If the problem is 2D the last coordinate (Z) will not be generated
        String normalAttr = x3d.getNormalAttr();
        double[][] samraiNormals = null;
        int lenght = 0;
        String[] normalStrings;
        if (normalAttr.indexOf(",") == -1) {
            normalStrings = normalAttr.substring(0, normalAttr.lastIndexOf(" ")).split(" ");
            Point[] points = new Point[normalStrings.length / THREE];
            samraiNormals = new double[normalStrings.length / THREE][THREE];
            lenght = normalStrings.length / THREE;
            for (int j = 0; j < lenght; j++) {
                points[j] = new Point(normalStrings[THREE * j] + " " + normalStrings[THREE * j + 1] + " " + normalStrings[THREE * j + 2]);
                double[] coords = points[j].getCoordinate();
                for (int k = 0; k < coords.length; k++) {
                    samraiNormals[j][k] = coords[k];
                }
            }
        }
        else {
            normalStrings = normalAttr.substring(0, normalAttr.lastIndexOf(",")).split(", ");
            Point[] points = new Point[normalStrings.length];
            samraiNormals = new double[normalStrings.length][THREE];
            lenght = normalStrings.length;
            for (int j = 0; j < normalStrings.length; j++) {
                points[j] = new Point(normalStrings[j]);
                double[] coords = points[j].getCoordinate();
                for (int k = 0; k < coords.length; k++) {
                    samraiNormals[j][k] = coords[k];
                }
            }
        }
        
        //Apply transformations to points
        CodeGeneratorUtils.applyTransformations(x3d, samraiNormals);
        return samraiNormals;
    }
    /**
     * Print normals.
     * @param samraiNormals	Samrai normals
     * @param dimensions	Number of dimensions
     * @return				Code
     */
   /* private static String printNormals(double[][] samraiNormals, int dimensions) {
        final int oneHundred = 100;
        final int ninetyNine = 99;
        //StringBuffer needed for performance when x3d is big
        StringBuffer pointString = new StringBuffer("{");
        for (int i = 0; i < samraiNormals.length; i++) {
            pointString.append("{");
            for (int j = 0; j < dimensions; j++) {
                if (j == dimensions - 1) {
                    pointString.append(samraiNormals[i][j] + "}, ");
                }
                else {
                    pointString.append(samraiNormals[i][j] + ", ");
                }
            }
            if (i % oneHundred == ninetyNine) {
                pointString.append(NEWLINE);
            }
        }
        return pointString.toString().substring(0, pointString.toString().length() - 2) + "}" + NEWLINE;
    }*/
    
    /**
     * Prints the points in a string.
     * @param samraiPoints      The points
     * @param dimensions        The dimensions
     * @return                  The string
     */
    private static String printPoints(double[][] samraiPoints, int dimensions) {
        final int oneHundred = 100;
        final int ninetyNine = 99;
        //StringBuffer needed for performance when x3d is big
        StringBuffer pointString = new StringBuffer("{");
        for (int i = 0; i < samraiPoints.length; i++) {
            pointString.append("{");
            for (int j = 0; j < dimensions; j++) {
                if (j == dimensions - 1) {
                    pointString.append(samraiPoints[i][j] + "}, ");
                }
                else {
                    pointString.append(samraiPoints[i][j] + ", ");
                }
            }
            if (i % oneHundred == ninetyNine) {
                pointString.append(NEWLINE);
            }
        }
        return pointString.toString().substring(0, pointString.toString().length() - 2) + "}" + NEWLINE;
    }
    
    /**
     * Prints the unions in a string.
     * @param unions            The unions
     * @return                  The string
     * @throws CGException      CG003 Not possible to create code for the platform
     */
    private static String printUnions(String[] unions) throws CGException {
        final int oneHundred = 100;
        final int ninetyNine = 99;
        StringBuffer unionString = new StringBuffer("{");
        int counter = 0;
        try {
            //The last point in the face is the first point, not to be stored
            for (int i = 0; i < unions.length; i++) {
                if (!unions[i].equals("")) {
                    String[] union = unions[i].trim().split(" +");
                    unionString.append("{");
                    for (int j = 0; j < union.length; j++) {
                        if (j == union.length - 1) {
                            unionString.append(Integer.parseInt(union[j]) + "}, ");
                        }
                        else {
                            unionString.append(Integer.parseInt(union[j]) + ", ");
                        }
                    }
                    if (counter % oneHundred == ninetyNine) {
                        unionString.append(NEWLINE);
                    }
                    counter++;
                }
            }
        }
        catch (Exception e) {
            throw new CGException(CGException.CG003, "The x3d is malformed, non integer found in unions");
        }
        return unionString.toString().substring(0, unionString.toString().length() - 2) + "}";
    }
    
    /**
     * Obtains the unions of the x3d.
     * @param x3d               The x3d
     * @return                  The unions
     * @throws CGException      CG003 Not possible to create code for the platform
     */
    private static String[] getUnions(X3DRegion x3d) throws CGException {
        String coordIndex = x3d.getCoordIndexAttr();
        String[] unions;
        //The data could have commas or not...
        if (coordIndex.contains(",")) {
            unions = coordIndex.substring(0, coordIndex.lastIndexOf(" -1,")).split(" -1, ");
        }
        else {
            unions = coordIndex.substring(0, coordIndex.lastIndexOf(" -1")).split(" -1 ");
        }
        int facePointsNumber = unions[0].trim().split(" +").length;
        if (facePointsNumber != THREE) {
            throw new CGException(CGException.CG003, "The x3d must be formed by triangles");
        }
        return unions;
    }
    
    /**
     * Obtains the points of the x3d.
     * @param x3d   The x3d region
     * @return      The points
     */
    public static double[][] getPoints(X3DRegion x3d) {
        //X3D is always a 3D point file. If the problem is 2D the last coordinate (Z) will not be generated
        String pointAttr = x3d.getPointAttr();
        double[][] samraiPoints = null;
        int lenght = 0;
        String[] pointStrings;
        if (pointAttr.indexOf(",") == -1) {
            pointStrings = pointAttr.substring(0, pointAttr.lastIndexOf(" ")).split(" ");
            Point[] points = new Point[pointStrings.length / THREE];
            samraiPoints = new double[pointStrings.length / THREE][THREE];
            lenght = pointStrings.length / THREE;
            for (int j = 0; j < lenght; j++) {
                points[j] = new Point(pointStrings[THREE * j] + " " + pointStrings[THREE * j + 1] + " " + pointStrings[THREE * j + 2]);
                double[] coords = points[j].getCoordinate();
                for (int k = 0; k < coords.length; k++) {
                    samraiPoints[j][k] = coords[k];
                }
            }
        }
        else {
            pointStrings = pointAttr.substring(0, pointAttr.lastIndexOf(",")).split(", ");
            Point[] points = new Point[pointStrings.length];
            samraiPoints = new double[pointStrings.length][THREE];
            lenght = pointStrings.length;
            for (int j = 0; j < pointStrings.length; j++) {
                points[j] = new Point(pointStrings[j]);
                double[] coords = points[j].getCoordinate();
                for (int k = 0; k < coords.length; k++) {
                    samraiPoints[j][k] = coords[k];
                }
            }
        }
        
        //Apply transformations to points
        CodeGeneratorUtils.applyTransformations(x3d, samraiPoints);
        return samraiPoints;
    }
    
    /**
     * Create the special loop structure due to the use of flat or maximal dissipation.
     * The first corner should have problems to get flat value if the rest of the cells are not assigned.
     * The general loop must start from the center of the domain and is divided in 4 (2D) or 8 (3D) loops
     * to cover all the cells.
     * @param dimensions        The number of coordinates
     * @param pi                The problem info
     * @param region           The region code
     * @param actualIndent      The actual indentation
     * @return                  The code
     */
    public static String createFlatLoopStructure(int dimensions, ProblemInfo pi, String region, String actualIndent) {
        if (dimensions == THREE) {
            return createExecutionBlockLoopBound(pi, true, true, true)
                + region
                + createCellLoopEnd(actualIndent, dimensions)
                + createExecutionBlockLoopBound(pi, true, true, false)
                + region
                + createCellLoopEnd(actualIndent, dimensions)
                + createExecutionBlockLoopBound(pi, true, false, false)
                + region
                + createCellLoopEnd(actualIndent, dimensions)
                + createExecutionBlockLoopBound(pi, true, false, true)
                + region
                + createCellLoopEnd(actualIndent, dimensions)
                + createExecutionBlockLoopBound(pi, false, true, true)
                + region
                + createCellLoopEnd(actualIndent, dimensions)
                + createExecutionBlockLoopBound(pi, false, true, false)
                + region
                + createCellLoopEnd(actualIndent, dimensions)
                + createExecutionBlockLoopBound(pi, false, false, false)
                + region
                + createCellLoopEnd(actualIndent, dimensions)
                + createExecutionBlockLoopBound(pi, false, false, true)
                + region
                + createCellLoopEnd(actualIndent, dimensions); 
        }
		return createExecutionBlockLoopBound(pi, true, true, false)
		    + region
		    + createCellLoopEnd(actualIndent, dimensions)
		    + createExecutionBlockLoopBound(pi, true, false, false)
		    + region
		    + createCellLoopEnd(actualIndent, dimensions)
		    + createExecutionBlockLoopBound(pi, false, true, false)
		    + region
		    + createCellLoopEnd(actualIndent, dimensions)
		    + createExecutionBlockLoopBound(pi, false, false, false)
		    + region
		    + createCellLoopEnd(actualIndent, dimensions);
    }
    
    /**
     * Creates the block loop starting at the center of the domain. This is necessary for flat and maximal
     * dissipation boundaries when parabolic terms are used.
     * @param pi            The problem information
     * @param xForward      The direction of the loop (true forward; false backward) for the 1st coordinate
     * @param yForward      The direction of the loop (true forward; false backward) for the 2nd coordinate
     * @param zForward      The direction of the loop (true forward; false backward) for the 3rd coordinate
     * @return              The loop
     */
    private static String createExecutionBlockLoopBound(ProblemInfo pi, boolean xForward, boolean yForward, boolean zForward) {
        String block = "";
        
        String actualIndent = INDENT + INDENT;
        for (int k = pi.getCoordinates().size() - 1; k >= 0; k--) {
            String coordString = pi.getCoordinates().get(k);
            //Loop iteration
            if (k == 2) {
                if (zForward) {
                    block =  block + actualIndent + "for(int " + coordString + " = " + coordString + "last/2; " 
                        + coordString + " < " + coordString + "last; " + coordString + "++) {" + NEWLINE;
                }
                else {
                    block =  block + actualIndent + "for(int " + coordString + " = " + coordString + "last/2 - 1; " 
                        + coordString + " >= 0; " + coordString + "--) {" + NEWLINE; 
                }
            }
            if (k == 1) {
                if (yForward) {
                    block =  block + actualIndent + "for(int " + coordString + " = " + coordString + "last/2; " 
                        + coordString + " < " + coordString + "last; " + coordString + "++) {" + NEWLINE;
                }
                else {
                    block =  block + actualIndent + "for(int " + coordString + " = " + coordString + "last/2 - 1; " 
                        + coordString + " >= 0; " + coordString + "--) {" + NEWLINE; 
                }
            }
            if (k == 0) {
                if (xForward) {
                    block =  block + actualIndent + "for(int " + coordString + " = " + coordString + "last/2; " 
                        + coordString + " < " + coordString + "last; " + coordString + "++) {" + NEWLINE;
                }
                else {
                    block =  block + actualIndent + "for(int " + coordString + " = " + coordString + "last/2 - 1; " 
                        + coordString + " >= 0; " + coordString + "--) {" + NEWLINE; 
                }
            }
            actualIndent = actualIndent + INDENT;
        }
        
        return block;
    }
    
    /**
     * Creates the loop end for an execution block.
     * @param actualIndent      The actual indent
     * @param dimensions        The spatial dimensions
     * @return                  The code
     */
    public static String createCellLoopEnd(String actualIndent, int dimensions) {
        String block = "";
        for (int k = 0; k < dimensions; k++) {
            actualIndent = actualIndent.substring(0, actualIndent.lastIndexOf(INDENT));
            block =  block + actualIndent + "}" + NEWLINE;
        }
        
        return block;
    }
    
    /**
     * Create a hard region condition.
     * @param coordIndex    The coordinates indexes
     * @param coords        The coordinates
     * @param field         The field for the condition
     * @return              The code
     */
    public static String hardRegionCondition(String coordIndex, ArrayList<String> coords, String field) {
        String condition = "";
        for (int i = 0; i < coords.size(); i++) {
            condition = condition + "vector(d_" + coords.get(i) + "_" + field + ", " + coordIndex + ") != 0 || ";
        }
        return "(" + condition.substring(0, condition.lastIndexOf(" ||")) + ")";
    }
    
    /**
     * Create the condition for a boundary depending on its axis and side.
     * @param pi                The problem info
     * @param axis              The axis of the boundary
     * @param side              The side of the boundary
     * @param particleb         If the condition is against particleb
     * @return                  The condition
     * @throws CGException      CG00X External error
     */
    public static String createBoundAxisSideCondition(ProblemInfo pi, String axis, String side, boolean particleb) throws CGException {
        String result = "";
        String b = "";
        if (particleb) {
            b = "b";
        }
        ArrayList<String> coordinates = pi.getCoordinates();
        
        ArrayList<Integer> axisIds = new ArrayList<Integer>();
        if (axis.toLowerCase().equals("all")) {
            for (int i = 0; i < coordinates.size(); i++) {
                axisIds.add(1 + i * 2);
            }
        }
        else {
            axisIds.add(1 + coordinates.indexOf(CodeGeneratorUtils.contToDisc(pi.getProblem(), axis)) * 2); 
        }
        ArrayList<Integer> boundIds = new ArrayList<Integer>();
        for (int i = 0; i < axisIds.size(); i++) {
            if (side.toLowerCase().equals("all") || side.toLowerCase().equals("lower")) {
                boundIds.add(axisIds.get(i));
            }
            if (side.toLowerCase().equals("all") || side.toLowerCase().equals("upper")) {
                boundIds.add(axisIds.get(i) + 1);
            }
        }
        for (int i = 0; i < boundIds.size(); i++) {
            result = result + "particle" + b + "->region == -" + boundIds.get(i) + " || ";
        }
        result = result.substring(0, result.lastIndexOf(" ||"));
        
        return result;
    }
    
    /**
     * Generates the FOVs declaration.
     * @param regionIds    	The region identifiers
     * @param indent        The actual indentation
     * @param patchAccess  	The patch of the variables
     * @return              The code
     */
    public static String createFOVDeclaration(ArrayList<String> regionIds, String indent, String patchAccess) {
        String fov = "";
        
        for (int i = 0; i < regionIds.size(); i++) {
            String segId = regionIds.get(i);
            fov = fov + indent + "double* FOV_" + segId + " = ((pdat::NodeData<double> *) " + patchAccess 
                    + "getPatchData(d_FOV_" + segId + "_id).get())->getPointer();" + NEWLINE;
        }
        
        return fov;
    }
    
    /**
     * Gets the lasts(ilast, jlast...) code.
     * @param pi                The problem information
     * @param ind               The actual indent
     * @param nodes				True if variables are located in nodes instead of cells
     * @return                  The code
     */
    public static String getLasts(ProblemInfo pi, String ind, boolean nodes) {
        String lasts = "";
        int extra = 1;
        if (nodes) {
        	extra = 2;
        }
        for (int i = 0; i < pi.getDimensions(); i++) {
            //Initialization of the variables
            String coordString = pi.getCoordinates().get(i);
            lasts =  lasts + ind + "int " + coordString + "last = boxlast(" + i + ")-boxfirst(" 
                + i + ") + " + extra + " + 2 * d_ghost_width;" + NEWLINE;
        }
        return lasts;
    }
    
    /**
     * Generates the instructions calculating the cell last array index in particle codes.
     * @param coords            The coordinates
     * @param currentIndent     The indentation for the instructions
     * @return                  The instructions
     */
    public static String generateLasts(ArrayList<String> coords, String currentIndent) {
        String lasts = "";
        for (int i = 0; i < coords.size(); i++) {
            //Initialization of the variables
            String coordString = coords.get(i);
            lasts =  lasts + currentIndent + "int " + coordString + "last = boxlast1(" + i + ")-boxfirst1(" 
                + i + ") + 2 + 2 * d_ghost_width;" + NEWLINE;
        }
        return lasts;
    }
    
    /**
     * Create the calls to checkPosition to correct the position of the particles at the boundaries when there are periodical boundaries.
     * @param dimensions		The dimensions of the problem
     * @return					The code
     */
    public static String checkPositions(ProblemInfo pi, int dimensions, String currIndent, boolean withDeclaration) {
        String block = "";
        String coordIndex = "";
        
        if (withDeclaration) {
	        for (String speciesName : pi.getParticleSpecies()) {
	        	block = block + currIndent + "std::shared_ptr< pdat::IndexData<Particles<Particle_" + speciesName + ">, pdat::CellGeometry > > particleVariables_" + speciesName + "(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles<Particle_" + speciesName + ">, pdat::CellGeometry >, hier::PatchData>(patch->getPatchData(d_particleVariables_" + speciesName + "_id)));" + NEWLINE;
	    	}
	        block = block + currIndent + "const hier::Index boxfirst = particleVariables_" + pi.getParticleSpecies().toArray()[0] + "->getGhostBox().lower();" + NEWLINE
	        		+ currIndent + "const hier::Index boxlast = particleVariables_" + pi.getParticleSpecies().toArray()[0] + "->getGhostBox().upper();" + NEWLINE;
        }
        //Loop iteration
        for (int i = dimensions - 1; i >= 0; i--) {
            block =  block + currIndent + "for (int index" + i + " = boxfirst(" + i + "); index" + i + " <= boxlast(" + i + "); index" + i 
                + "++) {" + NEWLINE;
            currIndent = currIndent + INDENT;
            coordIndex =  "index" + i + ", " + coordIndex;
        }
        coordIndex = coordIndex.substring(0, coordIndex.lastIndexOf(","));
        
        //Create Index for indexData
        block = block + currIndent + "hier::Index idx(" + coordIndex + ");" + NEWLINE
        	+ currIndent + "//Correct the position if there is any periodical boundary" + NEWLINE;
        
        //call checkposition
        for (String speciesName: pi.getParticleSpecies()) {
	        block = block + currIndent + "Particles<Particle_" + speciesName + ">* part_" + speciesName + " = particleVariables_" + speciesName + "->getItem(idx);" + NEWLINE
	            + currIndent + "checkPosition(patch, " + coordIndex + ", part_" + speciesName + ");" + NEWLINE;
        }
        
        //Close loop
        for (int i = 0; i < dimensions; i++) {
            currIndent = currIndent.substring(1);
            block =  block + currIndent + "}" + NEWLINE;
        }
		
        return block;
    }
}
