/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.codeGeneratorPlugin.fvm;

import eu.iac3.mathMS.codeGeneratorPlugin.CGException;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.CodeGeneratorUtils;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.ExtrapolationType;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.ProblemInfo;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.SAMRAIUtils;

import java.util.ArrayList;
import java.util.HashMap;

import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Initializations for FVM code in SAMRAI.
 * @author bminyano
 *
 */
public final class SAMRAIPDEInitializations {
    static final String NL = System.getProperty("line.separator");
    static final String IND = "\u0009";
    static final int THREE = 3;
    static final String MTURI = "http://www.w3.org/1998/Math/MathML";
    static final String MMSURI = "urn:mathms";
    
    /**
     * Private constructor.
     */
    private SAMRAIPDEInitializations() { };
    
    /**
     * Creates the file for the initialization.
     * @param pi                    The problem information
     * @return                      The file
     * @throws CGException          CG004 External error
     */
    public static String create(ProblemInfo pi) throws CGException {
        String result;
        Document doc = pi.getProblem();
        
        //Parameters for the xsl transformation. It is not a condition formula
        String[][] xslParams = new String [4][2];
        xslParams[0][0] =  "condition";
        xslParams[0][1] =  "false";
        xslParams[1][0] =  "indent";
        xslParams[1][1] =  "0";
        xslParams[2][0] =  "dimensions";
        xslParams[2][1] =  String.valueOf(pi.getDimensions());
        xslParams[3][0] = "timeCoord";
        xslParams[3][1] = pi.getTimeCoord();
        
        Element coordinates = CodeGeneratorUtils.createElement(pi.getProblem(), null, "coords");
        
        int dimensions = pi.getSpatialDimensions();
        try {
            result = "//Get fields, auxiliary fields and local variables that are going to be used." + NL;
                
            //Declaration of variables
            DocumentFragment scope = doc.createDocumentFragment();
            NodeList fc = CodeGeneratorUtils.find(doc, "/mms:discretizedProblem/mms:region/mms:initialConditions/*|/mms:discretizedProblem/mms:subregions/mms:subregion/mms:initialConditions/*");
            for (int i = 0; i < fc.getLength(); i++) {
                scope.appendChild(fc.item(i).cloneNode(true));
            }
            if (pi.isHasParticleFields()) {
            	for (String speciesName : pi.getParticleSpecies()) {
            		result = result + IND + "std::shared_ptr< pdat::IndexData<Particles<Particle_" + speciesName + ">, pdat::CellGeometry > > particleVariables_" + speciesName + "(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles<Particle_" + speciesName + ">, pdat::CellGeometry >, hier::PatchData>(patch.getPatchData(d_particleVariables_" + speciesName + "_id)));" + NL;
            	}
            }
            result = result + SAMRAIUtils.getPDEVariableDeclaration(scope, pi, 0, new ArrayList<String>(), 
            		"patch.", true, false) + NL;
            
            result = result + "//Get the dimensions of the patch" + NL 
                + "hier::Box pbox = patch.getBox();" + NL
                + SAMRAIUtils.createFOVDeclaration(pi.getRegionIds(), "", "patch.")
                + "const hier::Index boxfirst = patch.getBox().lower();" + NL
                + "const hier::Index boxlast  = patch.getBox().upper();" + NL + NL;
            if (pi.isHasParticleFields()) {
            	result = result + "const hier::Index boxfirstP = particleVariables_" + pi.getParticleSpecies().toArray()[0] + "->getGhostBox().lower();" + NL
            			+ "const hier::Index boxlastP = particleVariables_" + pi.getParticleSpecies().toArray()[0] + "->getGhostBox().upper();" + NL + NL;
            }
            result = result + "//Get delta spaces into an array. dx, dy, dz." + NL
                + "const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch.getPatchGeometry()));" + NL
                + "const double* dx  = patch_geom->getDx();" + NL + NL;

            //Do the condition
            //Initialization of the variables and setting the loop iteration
            String particleIndex = "";
            String particleCellIndexCondition = "";
            for (int i = 0; i < dimensions; i++) {
                //Initialization of the variables
                String coordString = pi.getCoordinates().get(i);
                result = result + "int " + coordString + "last = boxlast(" + i + ")-boxfirst(" + i + ") + 2 + 2 * d_ghost_width;" + NL;
                particleIndex = particleIndex + coordString + " + boxfirstP(" + i + "), ";
                particleCellIndexCondition = particleCellIndexCondition + coordString + " + 1 < " + coordString + "last && ";
            }
            particleIndex = particleIndex.substring(0, particleIndex.lastIndexOf(","));
            particleCellIndexCondition = particleCellIndexCondition.substring(0,  particleCellIndexCondition.lastIndexOf(" &&"));
            
            result = result + addExternalInitialConditions(pi);
            
            String currIndent = "";
            String coordIndex = "";
            String loopInit = "";
            for (int i = dimensions - 1; i >= 0; i--) {
            	//Loop iteration
                String coordString = pi.getCoordinates().get(i);
                loopInit = loopInit + currIndent + "for(int " + coordString + " = 0; " 
                        + coordString + " < " + coordString + "last; " + coordString + "++) {" + NL;
                currIndent = currIndent + IND;
                coordIndex =  pi.getCoordinates().get(i) + ", " + coordIndex;
            }
            coordIndex = coordIndex.substring(0, coordIndex.lastIndexOf(","));
            String instructions = "";
            //Mesh Initialization
            //For every region (first the lower precedents)
            for (int i = pi.getRegions().size() - 1; i >= 0; i--) {
                String regionName = pi.getRegions().get(i);
                boolean hasInterior = pi.getRegionIds().contains(String.valueOf((i * 2) + 1));
                boolean hasSurface = pi.getRegionIds().contains(String.valueOf((i * 2) + 2));
                  
                String meshInitialCondition = "";
                
                //Region condition before applying the conditions
                NodeList initConditions = CodeGeneratorUtils.find(doc, "/mms:discretizedProblem/mms:region/mms:initialConditions[preceding-sibling::" 
                        + "mms:name = '" + regionName + "']/mms:initialCondition|/mms:discretizedProblem/mms:subregions/mms:subregion/mms:initialConditions[preceding-sibling::" 
                        + "mms:name = '" + regionName + "']/mms:initialCondition");
                //Mesh Initialization
                if (initConditions.getLength() > 0) {
                    xslParams[0][1] =  "false";
                    xslParams[1][1] = String.valueOf(currIndent.length() + 1);
	                for (int j = 0; j < initConditions.getLength(); j++) {
	                    //mathematical expressions in the initial conditions
	                    NodeList expressions = CodeGeneratorUtils.find(initConditions.item(j), "./sml:simml/sml:iterateOverCells/*[not(local-name() = 'iterateOverParticles')]");
	                    for (int k = 0; k < expressions.getLength(); k++) {
	                    	meshInitialCondition = meshInitialCondition + SAMRAIUtils.xmlTransform(
	                                SAMRAIUtils.preProcessPDEBlock(expressions.item(k), pi, false, false), 
	                                xslParams, null, coordinates);
	                    }
	                }
	                
	                if (!meshInitialCondition.equals("")) {
	                	String condition = currIndent + "if (";
		                if (hasInterior) {
		                	condition = condition + "vector(FOV_" + ((i * 2) + 1) + ", " + coordIndex + ") > 0";
		                }
		                if (hasInterior && hasSurface) {
		                	condition = condition + " || ";
		                }
		                if (hasSurface) {
		                	condition = condition + "vector(FOV_" + ((i * 2) + 2) + ", " + coordIndex + ") > 0";
		                }
		                condition = condition + ") {" + NL;
		                instructions = instructions + condition
		                		+ meshInitialCondition
		                		+ currIndent + "}" + NL + NL;
	                }
                }
            }
            //Particle Initialization
            //For every region (first the lower precedents)
            String particleSpeciesInitialCond = "";
            for (String speciesName: pi.getParticleSpecies()) {
                String globalParticleInitialCondition = "";
	            for (int i = pi.getRegions().size() - 1; i >= 0; i--) {
	                String regionName = pi.getRegions().get(i);
	                  
	                String particleInitialCondition = "";
	                //Region condition before applying the conditions
	                NodeList initConditions = CodeGeneratorUtils.find(doc, "/mms:discretizedProblem/mms:region/mms:initialConditions[preceding-sibling::" 
	                        + "mms:name = '" + regionName + "']/mms:initialCondition|/mms:discretizedProblem/mms:subregions/mms:subregion/mms:initialConditions[preceding-sibling::" 
	                        + "mms:name = '" + regionName + "']/mms:initialCondition");
	                //Mesh Initialization
	                if (initConditions.getLength() > 0) {
	                    xslParams[0][1] =  "false";
	                    xslParams[1][1] = String.valueOf(currIndent.length() + 1);
          
		                for (int j = 0; j < initConditions.getLength(); j++) {
		                    //mathematical expressions in the initial conditions
		                    xslParams[1][1] = String.valueOf(currIndent.length() + 3);
		                    NodeList expressions = CodeGeneratorUtils.find(initConditions.item(j), "./sml:simml/sml:iterateOverCells/sml:iterateOverParticles[@speciesNameAtt = '" + speciesName + "']/*");
		                    for (int k = 0; k < expressions.getLength(); k++) {
		                    	particleInitialCondition = particleInitialCondition + SAMRAIUtils.xmlTransform(
		                                SAMRAIUtils.preProcessPDEBlock(expressions.item(k), pi, false, false), 
		                                xslParams, null, coordinates);
		                    }
	                    }
	                }
	                if (!particleInitialCondition.equals("")) {
	                	globalParticleInitialCondition = globalParticleInitialCondition + currIndent + IND + IND + "if (particle->region == " + ((i * 2) + 1) + " || particle->region == " + ((i * 2) + 2) + ") {" + NL
	                			+ particleInitialCondition
	                     		+ currIndent + IND + IND + "}" + NL + NL;
	                }
                }
	            String initializeBoundariesParticles = initializeBoundariesParticles(pi, currIndent, speciesName);
	            if (!globalParticleInitialCondition.equals("") || !initializeBoundariesParticles.equals("")) {
	            	particleSpeciesInitialCond = particleSpeciesInitialCond + currIndent + IND + "Particles<Particle_"  + speciesName + ">* part_"  + speciesName + " = particleVariables_"  + speciesName + "->getItem(idx);" + NL
	        				+ currIndent + IND + "for (int pit = part_"  + speciesName + "->getNumberOfParticles() - 1; pit >= 0; pit--) {" + NL
	        				+ currIndent + IND + IND + "Particle_"  + speciesName + "* particle = part_"  + speciesName + "->getParticle(pit);" + NL
	        				+ globalParticleInitialCondition 
	        				+ initializeBoundariesParticles
	            			+ currIndent + IND + "}" + NL;
	            }
            }
            if (!particleSpeciesInitialCond.equals("")) {
            	instructions = instructions + currIndent + "if (patch.getPatchLevelNumber() == 0 && (" + particleCellIndexCondition + ")) {" + NL  
        				+ currIndent + IND + "hier::Index idx(" + particleIndex + ");" + NL
        				+ particleSpeciesInitialCond
        				+ currIndent + "}" + NL;
            }
            
            instructions = instructions + initializeBoundariesMesh(pi, currIndent);
            if (!instructions.equals("")) {
            	result = result + loopInit + instructions;
                //close the loop iteration
                for (int i = 0; i < dimensions; i++) {
                    currIndent = currIndent.substring(0, currIndent.lastIndexOf(IND));
                    result =  result + currIndent + "}" + NL;
                }
            }
            
            //Post-initialization synchronization and extrapolation
            result = result + postInitialCondition(pi);
            return result;
        }
        catch (CGException e) {
            throw e;
        }
        catch (Exception e) {
        	e.printStackTrace(System.out);
            throw new CGException(CGException.CG00X, e.getMessage());
        } 
    }
    
    /**
     * Initialization of the boundaries that need to.
     * @param problemInfo       The problem info
     * @param currentIndent      The indentation for the code
     * @return                  The code
     * @throws CGException      CG00X External error
     */
    private static String initializeBoundariesMesh(ProblemInfo problemInfo, String currentIndent) throws CGException {
        String result = "";
        Document problem = problemInfo.getProblem();
        String[][] xslParams = new String [2][2];
        xslParams[0][0] =  "condition";
        xslParams[0][1] =  "false";
        xslParams[1][0] =  "indent";
        xslParams[1][1] =  "0";
        
        NodeList boundPolicies = CodeGeneratorUtils.find(problem, "/mms:discretizedProblem/mms:boundaryConditions/mms:boundaryPolicy[descendant::mms:initialConditions]");
        for (int i = 0; i < boundPolicies.getLength(); i++) {
            boolean meshData = false;
            String meshBoundInit = "";
            
            Element boundaryRegionGroups = (Element) boundPolicies.item(i).getFirstChild();
            meshBoundInit = meshBoundInit + currentIndent + "if (" + createBoundSegGroupCondition(problemInfo, boundaryRegionGroups) 
                    + ") {" + NL;
            NodeList boundConditions = CodeGeneratorUtils.find(boundPolicies.item(i), "./mms:boundaryCondition[descendant::" 
                    + "mms:initialConditions]");
            for (int j = 0; j < boundConditions.getLength(); j++) {
                String axis = ((Element) boundConditions.item(j)).getElementsByTagNameNS(MMSURI, "axis").item(0).getTextContent();
                String side = ((Element) boundConditions.item(j)).getElementsByTagNameNS(MMSURI, "side").item(0).getTextContent();
                meshBoundInit = meshBoundInit + currentIndent + IND + "if (" + createBoundAxisSideCondition(problemInfo, axis, side) 
                        + ") {" + NL;
                //condition of the boundary
                NodeList boundaryApplyIf = CodeGeneratorUtils.find(boundConditions.item(j), "./mms:applyIf");
                if (boundaryApplyIf.getLength() > 0) {
                    xslParams[0][1] = "true";
                    Element condition = (Element) boundaryApplyIf.item(0);
                    meshBoundInit = meshBoundInit + currentIndent + IND + IND + "if (" + SAMRAIUtils.xmlTransform(SAMRAIUtils.preProcessPDEBlock(
                            condition.getFirstChild().cloneNode(true), problemInfo, false, false), 
                            xslParams) + ") {" + NL;
                    currentIndent = currentIndent + IND;
                }
                
                //Initial conditions mesh
                xslParams[1][1] = String.valueOf(currentIndent.length() + 2);
                xslParams[0][1] = "false";
                NodeList initConditions = CodeGeneratorUtils.find(boundConditions.item(j), "./mms:type//mms:initialCondition/sml:simml/sml:iterateOverCells/*[not(local-name() = 'iterateOverParticles')]");
                for (int k = 0; k < initConditions.getLength(); k++) {
                    Node condition = initConditions.item(k);
                	meshData = true;
                	meshBoundInit = meshBoundInit 
                        + SAMRAIUtils.xmlTransform(SAMRAIUtils.preProcessPDEBlock(condition, problemInfo, false, false), 
                                xslParams);
                }
                //close condition of the boundary
                if (boundaryApplyIf.getLength() > 0) {
                	currentIndent = currentIndent.substring(1);
                    meshBoundInit = meshBoundInit + currentIndent + IND + IND + "}" + NL;
                }
                meshBoundInit = meshBoundInit + currentIndent + IND + "}" + NL;
            }
            meshBoundInit = meshBoundInit + currentIndent + "}" + NL;
            if (meshData) {
            	result = result + meshBoundInit;
            }
        }

        if (!result.equals("")) {
        	result = currentIndent + "//Boundaries Initialization" + NL + result;
        }
        return result;
    }
    
    /**
     * Initialization of the boundaries that need to.
     * @param problemInfo       The problem info
     * @param currentIndent      The indentation for the code
     * @return                  The code
     * @throws CGException      CG00X External error
     */
    private static String initializeBoundariesParticles(ProblemInfo problemInfo, String currentIndent, String speciesName) throws CGException {
        String result = "";
        Document problem = problemInfo.getProblem();
        String[][] xslParams = new String [2][2];
        xslParams[0][0] =  "condition";
        xslParams[0][1] =  "false";
        xslParams[1][0] =  "indent";
        xslParams[1][1] =  "0";
            
        String particleIndex = "";
        String particleCellIndexCondition = "";
        for (int i = 0; i < problemInfo.getDimensions(); i++) {
        	String coord = problemInfo.getCoordinates().get(i);
            particleIndex = particleIndex + coord + " + boxfirstP(" + i + "), ";
            particleCellIndexCondition = particleCellIndexCondition + coord + " + 1 < " + coord + "last && ";
        }
        particleIndex = particleIndex.substring(0, particleIndex.lastIndexOf(","));
        particleCellIndexCondition = particleCellIndexCondition.substring(0,  particleCellIndexCondition.lastIndexOf(" &&"));
        
        
        NodeList boundPolicies = CodeGeneratorUtils.find(problem, "/mms:discretizedProblem/mms:boundaryConditions/mms:boundaryPolicy[descendant::mms:initialConditions]");
        for (int i = 0; i < boundPolicies.getLength(); i++) {
            boolean particleData = false;
            String particleBoundInit = "";
            
            NodeList boundConditions = CodeGeneratorUtils.find(boundPolicies.item(i), "./mms:boundaryCondition[descendant::" 
                    + "mms:initialConditions]");
            for (int j = 0; j < boundConditions.getLength(); j++) {
                String axis = ((Element) boundConditions.item(j)).getElementsByTagNameNS(MMSURI, "axis").item(0).getTextContent();
                String side = ((Element) boundConditions.item(j)).getElementsByTagNameNS(MMSURI, "side").item(0).getTextContent();

                particleBoundInit = particleBoundInit + currentIndent + IND + IND + "if (" + SAMRAIUtils.createBoundAxisSideCondition(problemInfo, axis, side, false) 
                + ") {" + NL;
                //condition of the boundary
                NodeList boundaryApplyIf = CodeGeneratorUtils.find(boundConditions.item(j), "./mms:applyIf");
                if (boundaryApplyIf.getLength() > 0) {
                    xslParams[0][1] = "true";
                    Element condition = (Element) boundaryApplyIf.item(0);

                    particleBoundInit = particleBoundInit + currentIndent +  IND + "if (" + SAMRAIUtils.xmlTransform(SAMRAIUtils.preProcessPDEBlock(
                            condition.getFirstChild().cloneNode(true), problemInfo, false, false), 
                            xslParams) + ") {" + NL;
                    currentIndent = currentIndent + IND;
                }
                
                //Initial conditions particles
                xslParams[0][1] = "false";
                xslParams[1][1] = String.valueOf(currentIndent.length() + 3);
                NodeList initConditions = CodeGeneratorUtils.find(boundConditions.item(j), "./mms:type//mms:initialCondition/sml:simml/sml:iterateOverCells/sml:iterateOverParticles[@speciesNameAtt = '" + speciesName + "']/*");
                for (int k = 0; k < initConditions.getLength(); k++) {
                    Node condition = initConditions.item(k);
                    particleData = true;
                    particleBoundInit = particleBoundInit 
                        + SAMRAIUtils.xmlTransform(SAMRAIUtils.preProcessPDEBlock(condition, problemInfo, false, false), 
                                xslParams);
                }
                
                //close condition of the boundary
                if (boundaryApplyIf.getLength() > 0) {
                	currentIndent = currentIndent.substring(1);
                    particleBoundInit = particleBoundInit + currentIndent + IND + "}" + NL;
                }
                particleBoundInit = particleBoundInit + currentIndent + IND + IND + "}" + NL;

            }
            if (particleData) {
            	result = result + particleBoundInit;
            }
        }
        if (!result.equals("")) {
        	result = currentIndent + IND + IND + "//Boundaries Initialization" + NL + result;
        }
        return result;
    }
    
    /**
     * Creates the synchronization post initialization.
     * Needed by periodical and fixed boundary conditions.
     * @param pi                The problem information
     * @return                  The code
     * @throws CGException      CG003 Not possible to create code for the platform
     *                          CG004 External error
     */
    private static String postInitialCondition(ProblemInfo pi) throws CGException {
        int dim = pi.getDimensions();
        Document doc = pi.getProblem();
        String index = "";
        String loop = "";
        String endLoop = "";
        String currentIndent = IND + IND + IND;
        String iterators = "";
        for (int i = dim - 1; i >= 0; i--) {
            String coord = pi.getCoordinates().get(i);
            index = ", " + coord + index;
            loop = loop + currentIndent + "for (int " + coord + " = 0; " + coord + " < " + coord + "last; " + coord + "++) {" + NL;
            endLoop = currentIndent + "}" + NL + endLoop;
            currentIndent = currentIndent + IND;
            iterators = IND + IND + IND + "int " + coord + "last = boxlast(" + i + ")-boxfirst(" + i + ") + 2 + 2 * d_ghost_width;" + NL + iterators;
        }
        index = index.substring(2);
        //Check if parabolic terms are used
        boolean parabolicTerms = CodeGeneratorUtils.find(doc, "//*[@sml:type='parabolicTerm' or @type='parabolicTerm' or @cornerCalculationAtt='true']").getLength() > 0;
        //Check if flat or maximal dissipation
        boolean orderedBoundaries = CodeGeneratorUtils.find(doc, "/mms:discretizedProblem/mms:boundaryConditions/mms:boundaryPolicy/mms:boundaryCondition[mms:type/mms:flat or " 
                + "mms:type/mms:maximalDissipation or mms:type/mms:reflection]").getLength() > 0 && pi.isHasMeshFields();
     
        String checkPosition = "";
        if (pi.isHasParticleFields() && CodeGeneratorUtils.find(doc, "/mms:discretizedProblem/mms:boundaryConditions/mms:boundaryPolicy/mms:boundaryCondition[mms:type/mms:periodical]").getLength() > 0) {
            checkPosition = SAMRAIUtils.checkPositions(pi, dim, IND + IND + IND, false);
        }
        if (!checkPosition.equals("")) {
        	String particleVariables = "";
        	for (String speciesName : pi.getParticleSpecies()) {
        		particleVariables = particleVariables + IND + IND + IND + "std::shared_ptr< pdat::IndexData<Particles<Particle_" + speciesName + ">, pdat::CellGeometry > > particleVariables_" + speciesName + "(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles<Particle_" + speciesName + ">, pdat::CellGeometry >, hier::PatchData>(patch->getPatchData(d_particleVariables_" + speciesName + "_id)));" + NL;
  	    	}
            checkPosition = IND + IND + "for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {" + NL
                + IND + IND + IND + "const std::shared_ptr< hier::Patch > patch = *p_it;" + NL
                + particleVariables
                + IND + IND + IND + "const hier::Index boxfirst = particleVariables_" + pi.getParticleSpecies().toArray()[0] + "->getGhostBox().lower();" + NL
        		+ IND + IND + IND + "const hier::Index boxlast = particleVariables_" + pi.getParticleSpecies().toArray()[0] + "->getGhostBox().upper();" + NL
                + checkPosition
                + IND + IND + "}" + NL;
        }

        String boxIndexParticles = "";
        String particleVars = "";
        if (pi.isHasParticleFields()) {
        	boxIndexParticles = boxIndexParticles + IND + IND + IND + "const hier::Index boxfirstP = particleVariables_" + pi.getParticleSpecies().toArray()[0] + "->getGhostBox().lower();" + NL
        			+ IND + IND + IND + "const hier::Index boxlastP = particleVariables_" + pi.getParticleSpecies().toArray()[0] + "->getGhostBox().upper();" + NL + NL;
        	particleVars = IND + IND + "double level_influenceRadius = influenceRadius/MAX(1,level_ratio);" + NL
                    + IND + IND + "double volume_level_factor = 1.0 / MAX(1, level->getRatioToLevelZero().getProduct());" + NL;
        }
        String commonVariables = IND + IND + IND + "//Get the dimensions of the patch" + NL
                + IND + IND + IND + "const hier::Index boxfirst = patch->getBox().lower();" + NL
                + IND + IND + IND + "const hier::Index boxlast = patch->getBox().upper();" + NL + NL
                + boxIndexParticles
                + IND + IND + IND + "//Get delta spaces into an array. dx, dy, dz." + NL
                + IND + IND + IND + "const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));" + NL
                + IND + IND + IND + "const double* dx  = patch_geom->getDx();" + NL
                + iterators;
        
        StringBuffer code = new StringBuffer();
        int blocks = CodeGeneratorUtils.getBlocksNum(doc, "/mms:discretizedProblem/mms:region/mms:postInitialConditions/mms:postInitialCondition/sml:simml|"
        		+ "/mms:discretizedProblem/mms:subregions/mms:subregion/mms:postInitialConditions/mms:postInitialCondition/sml:simml");
        for (int i = 0; i < blocks; i++) {
            String block = "";
            //patch loop and synchronizations
            boolean boundary = CodeGeneratorUtils.find(doc, "/mms:discretizedProblem/mms:region/mms:postInitialConditions/mms:postInitialCondition/sml:simml/*[position() = " + (i + 1) 
                + " and local-name() = 'boundary']/*|/mms:discretizedProblem/mms:subregions/mms:subregion/mms:postInitialConditions/mms:postInitialCondition/sml:simml/*[position() = " + (i + 1) 
                + " and local-name() = 'boundary']/*").getLength() > 0;
            block = IND + IND + "for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {" + NL
                    + IND + IND + IND + "const std::shared_ptr< hier::Patch >& patch = *p_it;" + NL
                    + SAMRAIUtils.createFOVDeclaration(pi.getRegionIds(), IND + IND + IND, "patch->");
            //Extrapolation
            boolean extrapolation = CodeGeneratorUtils.find(doc, "/mms:discretizedProblem/mms:region/mms:postInitialConditions/mms:postInitialCondition/sml:simml/*[position() = " + (i + 1) 
                + " and local-name() = 'boundary']//sml:fieldExtrapolation|/mms:discretizedProblem/mms:subregions/mms:subregion/mms:postInitialConditions/mms:postInitialCondition/sml:simml/*[position() = " + (i + 1) 
                + " and local-name() = 'boundary']//sml:fieldExtrapolation").getLength() > 0;
                    
            //Declaration of variables
            DocumentFragment scope = doc.createDocumentFragment();
            
            NodeList fc = CodeGeneratorUtils.find(doc, "/mms:discretizedProblem/mms:region/mms:postInitialConditions/mms:postInitialCondition/sml:simml/*[" + (i + 1) + "]|"
            		+ "/mms:discretizedProblem/mms:subregions/mms:subregion/mms:postInitialConditions/mms:postInitialCondition/sml:simml/*[" + (i + 1) + "]");
            for (int j = 0; j < fc.getLength(); j++) {
                scope.appendChild(fc.item(j).cloneNode(true));
            }      
            block = block + SAMRAIUtils.getPDEVariableDeclaration(scope, pi, 3, new ArrayList<String>(), 
            		"patch->", false, false) + NL;
            if (!pi.getHardRegions().isEmpty() && boundary) {
            	block = block + IND + IND + IND + "//Hard region field distance variables" + NL
                        + SAMRAIUtils.addHardFieldDistDeclaration(pi.getCoordinates(), pi.getExtrapolatedFieldGroups(), 3) + NL;
            }
            if (pi.isHasParticleFields()) {
            	for (String speciesName : pi.getParticleSpecies()) {
                	block = block + IND + IND + IND + "std::shared_ptr< pdat::IndexData<Particles<Particle_" + speciesName + ">, pdat::CellGeometry > > particleVariables_" + speciesName + "(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles<Particle_" + speciesName + ">, pdat::CellGeometry >, hier::PatchData>(patch->getPatchData(d_particleVariables_" + speciesName + "_id)));" + NL;
            	}
            }
            block = block + commonVariables;
            if (CodeGeneratorUtils.find(doc, "/mms:discretizedProblem/mms:region/mms:postInitialConditions/mms:postInitialCondition/sml:simml/*[position() = " + (i + 1) + "]//sml:iterateOverParticlesFromCell|"
            		+ "/mms:discretizedProblem/mms:subregions/mms:subregion/mms:postInitialConditions/mms:postInitialCondition/sml:simml/*[position() = " + (i + 1) + "]//sml:iterateOverParticlesFromCell").getLength() > 0) {
            	block = block + IND + IND + IND + "double* position = new double[" + pi.getDimensions() + "];" + NL;
            }
            //Extrapolation initialization loop
            if (extrapolation && pi.getExtrapolationType() != ExtrapolationType.generalPDE) {
                block = block + loop
                        + extrapolationInitialization(pi, currentIndent)
                        + endLoop;
            }

            //Common Loop and initialization
            if (!(boundary && parabolicTerms && orderedBoundaries) && !pi.isHasABMMeshFields()) {
                block = block + loop;
            }

            String region = "";
            //For every region (first the lower precedents)
            String extrapolationEndAssignments = "";
           // String auxNorm = "";
            for (int j = pi.getRegions().size() - 1; j >= 0; j--) {
                String regionName = pi.getRegions().get(j);
                boolean boundPTerms = CodeGeneratorUtils.find(doc, "//*[mms:name = '" + regionName + "']//*[@sml:type='parabolicTerm' or @type='parabolicTerm' or @cornerCalculationAtt='true']").getLength() > 0;
                //Interior execution flow
                region = region + SAMRAIPDEExecution.regionExecutionFlow(regionName, currentIndent, i, j, "interior", pi, 
                		"//*[mms:name = '" + regionName + "']//mms:postInitialCondition[mms:regionType = 'interior']",
                        boundary && parabolicTerms && orderedBoundaries, boundPTerms);
                //Surface execution flow
                region = region + SAMRAIPDEExecution.regionExecutionFlow(regionName, currentIndent, i, j, "surface", pi,
                		"//*[mms:name = '" + regionName + "']//mms:postInitialCondition[mms:regionType = 'surface']",
                        boundary && parabolicTerms && orderedBoundaries, boundPTerms);
                if (extrapolation && pi.getExtrapolationType() != ExtrapolationType.generalPDE) {
                	//*[mms:name = '" + regionName + "']//mms:postInitialCondition
                    NodeList boundaryList = CodeGeneratorUtils.find(doc, "//*[mms:name = '" + regionName + "']//mms:postInitialCondition[mms:regionType = 'interior']/sml:simml/*[position() = " + (i + 1) + " and child::*]");
                    for (int k = 0; k < boundaryList.getLength(); k++) {
                        Element postInit = (Element) boundaryList.item(k);
                        extrapolationEndAssignments = extrapolationEndAssignments + extrapolationFinalization(pi, currentIndent, postInit, 
                                (j * 2) + 1);
                    }
                    boundaryList = CodeGeneratorUtils.find(doc, "//*[mms:name = '" + regionName + "']//mms:postInitialCondition[mms:regionType = 'surface']/sml:simml/*[position() = " + (i + 1) + " and child::*]");
                    for (int k = 0; k < boundaryList.getLength(); k++) {
                        Element postInit = (Element) boundaryList.item(k);
                        extrapolationEndAssignments = extrapolationEndAssignments + extrapolationFinalization(pi, currentIndent, postInit, 
                                (j * 2) + 1);
                    }
                }
            }
            //Check if the structure must be special for the type of boundaries (flat, reflection and maximal dissipation)
            if (boundary && parabolicTerms && orderedBoundaries) {
                block = block + SAMRAIUtils.createFlatLoopStructure(pi.getDimensions(), pi, region, currentIndent);
            }
            else {
                block = block + region; 
            }
            //close the loop iteration
            if (!(boundary && parabolicTerms && orderedBoundaries)) {
                block = block + SAMRAIUtils.createCellLoopEnd(currentIndent, pi.getDimensions());
            }
            //Extrapolation finalization
            if (extrapolation && !extrapolationEndAssignments.equals("")) {
                block = block + loop
                        + extrapolationEndAssignments
                        + endLoop;
            }
            //Close loop 
            if (CodeGeneratorUtils.find(doc, "//mms:postInitialCondition/sml:simml/*[position() = " + (i + 1) + "]//sml:iterateOverParticlesFromCell").getLength() > 0) {
            	block = block + IND + IND + IND + "delete[] position;" + NL;
            }
            block = block + IND + IND + "}" + NL;
            if (!region.equals("")) {
                code = code.append(block);
            }
        }        
        
        return "#postInitSync#" + NL
        	+ IND + IND + "//First synchronization from initialization" + NL
        	+ IND + IND + "d_bdry_fill_init->createSchedule(level,this)->fillData(init_data_time, false);" + NL
            + IND + IND + "double current_time = init_data_time;" + NL
            + IND + IND + "const double level_ratio = level->getRatioToCoarserLevel().max();" + NL
            + particleVars
            + IND + IND + "double simPlat_dt = 0;" + NL
            + code.toString()
            + IND + IND + "//Last synchronization from initialization" + NL
            + IND + IND + "d_bdry_fill_init->createSchedule(level,this)->fillData(init_data_time, false);" + NL
            + checkPosition;
    }
    
    /**
     * Creates the condition for the boundary near to its region groups.
     * @param pi                    The problem info
     * @param boundRegionGroups    The region groups of the boundary
     * @return                      The condition
     * @throws CGException          CG00X External error
     */
    private static String createBoundSegGroupCondition(ProblemInfo pi, Element boundRegionGroups) throws CGException {
        ArrayList<String> coordinates = pi.getCoordinates();
        ArrayList<String> regions = pi.getRegions();
        int stencil = pi.getSchemaStencil();
        String condition = "";
        String index = "";
        String positiveCompatible = "";
        String negativeCompatible = "";
        for (int i = 0; i < coordinates.size(); i++) {
            for (int j = 0; j < stencil; j++) {
                String positiveStencilIndex = "";
                String negativeStencilIndex = "";
                String positiveIndexCheck = "";
                String negativeIndexCheck = "";
                for (int k = 0; k < coordinates.size(); k++) {
                    if (i == k) {
                        positiveStencilIndex = 
                            positiveStencilIndex + coordinates.get(k) + " + " + (j + 1) + ", ";
                        negativeStencilIndex = 
                            negativeStencilIndex + coordinates.get(k) + " - " + (j + 1) + ", ";
                        positiveIndexCheck = coordinates.get(k) + " + " + (j + 1) + " < " + coordinates.get(k) + "last";
                        negativeIndexCheck = coordinates.get(k) + " - " + (j + 1) + " >= 0";
                    }
                    else {
                        positiveStencilIndex = positiveStencilIndex + coordinates.get(k) + ", ";
                        negativeStencilIndex = negativeStencilIndex + coordinates.get(k) + ", ";
                    }
                }
                positiveStencilIndex = positiveStencilIndex.substring(0, positiveStencilIndex.lastIndexOf(","));
                negativeStencilIndex = negativeStencilIndex.substring(0, negativeStencilIndex.lastIndexOf(","));
                
                positiveCompatible = positiveCompatible + "(" + positiveIndexCheck + " && (";
                negativeCompatible = negativeCompatible + "(" + negativeIndexCheck + " && (";
                
                NodeList boundRegions = boundRegionGroups.getChildNodes();
                for (int l = 0; l < boundRegions.getLength(); l++) {
                    String regionName = boundRegions.item(l).getTextContent();
                    
                    boolean hasInterior = pi.getRegionIds().contains(String.valueOf((regions.indexOf(regionName) * 2) + 1));
                    boolean hasSurface = pi.getRegionIds().contains(String.valueOf((regions.indexOf(regionName) * 2) + 2));
                    if (hasInterior) {
                        positiveCompatible = positiveCompatible + "vector(FOV_" + (regions.indexOf(regionName) * 2 + 1) + ", " 
                                + positiveStencilIndex + ") > 0 || ";
                        negativeCompatible = negativeCompatible + "vector(FOV_" + (regions.indexOf(regionName) * 2 + 1) + ", " 
                                + negativeStencilIndex + ") > 0 || ";
                    }
                    if (hasSurface) {
                        positiveCompatible = positiveCompatible + "vector(FOV_" + (regions.indexOf(regionName) * 2 + 2) + ", " 
                                + positiveStencilIndex + ") > 0 || ";
                        negativeCompatible = negativeCompatible + "vector(FOV_" + (regions.indexOf(regionName) * 2 + 2) + ", " 
                                + negativeStencilIndex + ") > 0 || ";
                    }  
                }
                
                positiveCompatible = positiveCompatible.substring(0, positiveCompatible.lastIndexOf(" ||")) + ")) || ";
                negativeCompatible = negativeCompatible.substring(0, negativeCompatible.lastIndexOf(" ||")) + ")) || ";
            }
            index = index + coordinates.get(i) + ", ";
        }
        positiveCompatible = positiveCompatible.substring(0, positiveCompatible.lastIndexOf(" ||"));
        negativeCompatible = negativeCompatible.substring(0, negativeCompatible.lastIndexOf(" ||"));
        index = index.substring(0, index.lastIndexOf(","));
        

        condition = condition + "(" + positiveCompatible + ") || (" + negativeCompatible + ")"
            + getDiagonal(pi, coordinates, stencil, regions, boundRegionGroups) + " || ";
        
        return condition.substring(0, condition.lastIndexOf(" ||"));
    }
    
    /**
     * Generate the condition to check diagonals of a point in the mesh. Only if parabolic terms fluxes are used.
     * @param pi                    problem info
     * @param regions              The regions
     * @param coordinates           The coordinates
     * @param stencil               The stencil of the problem
     * @param boundRegionGroups    The region groups of the boundary
     * @return                      The condition
     * @throws CGException          CG00X External error
     */     
    private static String getDiagonal(ProblemInfo pi, ArrayList<String> coordinates, int stencil, 
            ArrayList<String> regions, Element boundRegionGroups) throws CGException {
        final int four = 4;
        final int three = 3;
        ArrayList<String> regionIds = pi.getRegionIds();
        boolean generalParabolicTerms = CodeGeneratorUtils.find(pi.getProblem(), 
                "//*[@sml:type='parabolicTerm' or @type='parabolicTerm' or @cornerCalculationAtt='true']").getLength() > 0;
        if (generalParabolicTerms) {
            String diagonal = "";
            for (int i = 0; i < coordinates.size(); i++) {
                for (int j = 0; j < stencil; j++) {
                    for (int k = i + 1; k < coordinates.size(); k++) {
                        for (int l = 0; l < stencil; l++) {
                            for (int s2 = 0; s2 < four; s2++) {
                                String stencilIndex = "";
                                String checkIndex = "";
                                for (int m = 0; m < coordinates.size(); m++) {
                                    if (i == m) {
                                        if (s2 == 0 || s2 == 1) {
                                            stencilIndex = stencilIndex + coordinates.get(m) + " + " + (j + 1) + ", ";
                                            checkIndex = checkIndex + coordinates.get(m) + " + " + (j + 1) + " < " + coordinates.get(m) + "last && ";
                                        }
                                        else {
                                            stencilIndex = stencilIndex + coordinates.get(m) + " - " + (j + 1) + ", ";
                                            checkIndex = checkIndex + coordinates.get(m) + " - " + (j + 1) + " >= 0 && ";
                                        }
                                    }
                                    else {
                                        if (m == k) {
                                            if (s2 == 1 || s2 == three) {
                                                stencilIndex = stencilIndex + coordinates.get(m) + " + " + (l + 1) + ", ";
                                                checkIndex = checkIndex + coordinates.get(m) + " + " + (l + 1) + " < " + coordinates.get(m) 
                                                    + "last && ";
                                            }
                                            else {
                                                stencilIndex = 
                                                    stencilIndex + coordinates.get(m) + " - " + (l + 1) + ", ";
                                                checkIndex = checkIndex + coordinates.get(m) + " - " + (l + 1) + " >= 0 && ";
                                            }
                                        }
                                        else {
                                            stencilIndex = stencilIndex + coordinates.get(m) + ", ";
                                        }
                                    }
                                }
                                stencilIndex = stencilIndex.substring(0, stencilIndex.lastIndexOf(","));
                                checkIndex = checkIndex.substring(0, checkIndex.lastIndexOf(" && "));
                                
                                diagonal = diagonal + "((" + checkIndex + ") && (";
                                NodeList boundRegions = boundRegionGroups.getChildNodes();
                                for (int m = 0; m < boundRegions.getLength(); m++) {
                                    String regionName = boundRegions.item(m).getTextContent();
                                    boolean parabolicTerms = CodeGeneratorUtils.find(pi.getProblem(), "//*[" 
                                        + "mms:name = '" + regionName + "']//*[@sml:type='parabolicTerm' or @type='parabolicTerm' or @cornerCalculationAtt='true']").getLength() > 0;
                                
                                    if (parabolicTerms) {
                                        boolean hasInterior = regionIds.contains(String.valueOf((regions.indexOf(regionName) * 2) + 1));
                                        boolean hasSurface = regionIds.contains(String.valueOf((regions.indexOf(regionName) * 2) + 2));
                                        if (hasInterior) {
                                            diagonal = diagonal + "vector(FOV_" + (regions.indexOf(regionName) * 2 + 1) + ", " 
                                                    + stencilIndex + ") > 0 || ";
                                        }
                                        if (hasSurface) {
                                            diagonal = diagonal + "vector(FOV_" + (regions.indexOf(regionName) * 2 + 2) + ", " 
                                                    + stencilIndex + ") > 0 || ";
                                        }        
                                    }
                                }
                                diagonal = diagonal.substring(0, diagonal.lastIndexOf(" ||")) + ")) || ";
                            }
                        }
                    }
                }
            }
            return " || (" + diagonal.substring(0, diagonal.lastIndexOf(" || ")) + ")";
        }
		return "";
    }
    
    /**
     * Create the condition for a boundary depending on its axis and side.
     * @param pi                The problem info
     * @param axis              The axis of the boundary
     * @param side              The side of the boundary
     * @return                  The condition
     * @throws CGException      CG00X External error
     */
    public static String createBoundAxisSideCondition(ProblemInfo pi, String axis, String side) throws CGException {
        String result = "";
        ArrayList<String> coordinates = pi.getCoordinates();
        String coordIndex = "";
        for (int j = coordinates.size() - 1; j >= 0; j--) {
            coordIndex = coordinates.get(j) + ", " + coordIndex;
        }
        coordIndex = coordIndex.substring(0, coordIndex.lastIndexOf(","));
        
        ArrayList<String> axisIds = new ArrayList<String>();
        if (axis.toLowerCase().equals("all")) {
            for (int i = 0; i < coordinates.size(); i++) {
                axisIds.add(CodeGeneratorUtils.discToCont(pi.getProblem(), coordinates.get(i)));
            }
        }
        else {
            axisIds.add(axis); 
        }
        ArrayList<String> boundIds = new ArrayList<String>();
        for (int i = 0; i < axisIds.size(); i++) {
            if (side.toLowerCase().equals("all") || side.toLowerCase().equals("lower")) {
                boundIds.add(axisIds.get(i) + "Lower");
            }
            if (side.toLowerCase().equals("all") || side.toLowerCase().equals("upper")) {
                boundIds.add(axisIds.get(i) + "Upper");
            }
        }
        for (int i = 0; i < boundIds.size(); i++) {
            result = result + "vector(FOV_" + boundIds.get(i) + ", " + coordIndex + ") > 0 || ";
        }
        result = result.substring(0, result.lastIndexOf(" ||"));
        
        return result;
    }
    
    /**
     * Create the initialization for the extrapolation block.
     * @param pi                The problem information
     * @param indent            The actual indent
     * @return                  The code
     * @throws CGException      CG00X External error
     */
    private static String extrapolationInitialization(ProblemInfo pi, String indent) throws CGException {
        String result = "";
        Document doc = pi.getProblem();
        String index = "";
        for (int i = 0; i < pi.getDimensions(); i++) {
            index = index + pi.getCoordinates().get(i) + ", ";
        }
        index = index.substring(0, index.length() - 2);
        ArrayList<String> variables = new ArrayList<String>();
        NodeList blockList = CodeGeneratorUtils.find(doc, "/mms:discretizedProblem/mms:region/mms:postInitialConditions/mms:postInitialCondition//sml:fieldExtrapolation|"
        		+ "/mms:discretizedProblem/mms:subregions/mms:subregion/mms:postInitialConditions/mms:postInitialCondition//sml:fieldExtrapolation");
        for (int i = 0; i < blockList.getLength(); i++) {
            Element variable = (Element) blockList.item(i);
            String field = variable.getAttribute("fieldAtt");
            for (int j = 0; j < pi.getDimensions(); j++) {
                String coord = pi.getCoordinates().get(j);
                if (!variables.contains("extrapolatedSB" + field + "SP" + coord)) {
                    variables.add("extrapolatedSB" + field + "SP" + coord);
                    result = result + indent + "vector(extrapolatedSB" + SAMRAIUtils.variableNormalize(field) + "SP" + coord 
                            + ", " + index + ") = vector(" + SAMRAIUtils.variableNormalize(field) + ", " + index + ");" + NL;
                }    
            }
        }
        return result;
    }
    
    /**
     * Create the finalization instructions after an extrapolation & boundaries. After a boundary calculation the extrapolation axis variables
     * must be reassigned for the boundaries that does not calculate them. 
     * @param pi                    The problem info.
     * @param indent                The actual indent  
     * @param boundary              The boundary xml code
     * @param regionBaseNumber     	The region base number
     * @return                      The code
     * @throws CGException          CG00X External error
     */
    private static String extrapolationFinalization(ProblemInfo pi, String indent, Node boundary, int regionBaseNumber) throws CGException {
        String result = "";
        String index = "";
        for (int i = 0; i < pi.getDimensions(); i++) {
            index = index + pi.getCoordinates().get(i) + ", ";
        }
        index = index.substring(0, index.length() - 2);
        ArrayList<String> fields = new ArrayList<String>();
        NodeList fieldList = CodeGeneratorUtils.find(pi.getProblem(), "/*/mms:fields/mms:field|/*/mms:auxiliaryFields/mms:auxiliaryField");
        for (int i = 0; i < fieldList.getLength(); i++) {
            fields.add(fieldList.item(i).getTextContent());
        }
       
      
        for (int i = 0; i < pi.getDimensions(); i++) {
            String coordString = pi.getCoordinates().get(i);
            String contCoordString = pi.getContCoordinates().get(i);
            if (pi.getPeriodicalBoundary().get(contCoordString).equals("noperiodical")) {
                ArrayList<String> candidates = new ArrayList<String>();
                candidates.addAll(fields);
                NodeList extrapolatedFields = CodeGeneratorUtils.find(boundary, 
                        ".//sml:axis[@axisAtt = '" + coordString + "']/sml:side[@sideAtt = 'lower']//sml:fieldExtrapolation/@fieldAtt");
                for (int j = 0; j < extrapolatedFields.getLength(); j++) {
                    candidates.remove(extrapolatedFields.item(j).getTextContent());
                }
                if (!candidates.isEmpty()) {
                    String axis = CodeGeneratorUtils.discToCont(pi.getProblem(), coordString);
                    int boundId = pi.getCoordinates().indexOf(CodeGeneratorUtils.contToDisc(pi.getProblem(), axis)) * 2 + 1;
                    result = result + indent + "if ((vector(FOV_" + CodeGeneratorUtils.getRegionName(pi.getProblem(), -boundId, 
                            pi.getCoordinates()) + ", " + index + ") > 0) && (" + SAMRAIUtils.getLeftCondition(false, 
                                    pi.getSchemaStencil(), regionBaseNumber, i, pi.getCoordinates(), pi.getRegionIds()) + ")) {" + NL;
                    for (int k = 0; k < candidates.size(); k++) {
                        String field = candidates.get(k);
                        for (int l = 0; l < pi.getDimensions(); l++) {
                            String coord = pi.getCoordinates().get(l);
                            result = result + indent + IND + "vector(extrapolatedSB" + SAMRAIUtils.variableNormalize(field) 
                                    + "SP" + coord + ", " + index + ") = vector(" + SAMRAIUtils.variableNormalize(field) + ", " 
                                    + index + ");" + NL;
                        }
                    }
                    result = result + indent + "}" + NL;
                }
                
                
                candidates = new ArrayList<String>();
                candidates.addAll(fields);
                extrapolatedFields = CodeGeneratorUtils.find(boundary, 
                        ".//sml:axis[@axisAtt = '" + coordString + "']/sml:side[@sideAtt = 'upper']//sml:fieldExtrapolation/@fieldAtt");
                for (int j = 0; j < extrapolatedFields.getLength(); j++) {
                    candidates.remove(extrapolatedFields.item(j).getTextContent());
                }
                if (!candidates.isEmpty()) {
                    String axis = CodeGeneratorUtils.discToCont(pi.getProblem(), coordString);
                    int boundId = pi.getCoordinates().indexOf(CodeGeneratorUtils.contToDisc(pi.getProblem(), axis)) * 2 + 2;
                    result = result + indent + "if ((vector(FOV_" + CodeGeneratorUtils.getRegionName(pi.getProblem(), -boundId, 
                            pi.getCoordinates()) + ", " + index + ") > 0) && (" + SAMRAIUtils.getRightCondition(false, 
                                    pi.getSchemaStencil(), regionBaseNumber, i, pi.getCoordinates(), pi.getRegionIds()) + ")) {" + NL;
                    for (int k = 0; k < candidates.size(); k++) {
                        String field = candidates.get(k);
                        for (int l = 0; l < pi.getDimensions(); l++) {
                            String coord = pi.getCoordinates().get(l);
                            result = result + indent + IND + "vector(extrapolatedSB" + SAMRAIUtils.variableNormalize(field) 
                                    + "SP" + coord + ", " + index + ") = vector(" + SAMRAIUtils.variableNormalize(field) + ", " 
                                    + index + ");" + NL;
                        }
                    }
                    result = result + indent + "}" + NL;
                }
            }
        }
        
        return result;
    }
    
    /**
     * Creates the code for an external initial condition call if used.
     * @param pi			The problem information
     * @return				The code
     * @throws CGException	CG00X External error
     */
    private static String addExternalInitialConditions(ProblemInfo pi) throws CGException {
    	HashMap<String, String> parameters = pi.getVariableInfo().getParameters();
    	//Only one external initialization allowed
        NodeList externalCond = CodeGeneratorUtils.find(pi.getProblem(), "/mms:discretizedProblem/mms:region/mms:initialConditions/mms:externalInitialCondition|"
        		+ "/mms:discretizedProblem/mms:subregions/mms:subregion/mms:initialConditions/mms:externalInitialCondition");
        if (externalCond.getLength() > 1) {
        	throw new CGException(CGException.CG004, "Only one external initial condition allowed");
        }
        if (externalCond.getLength() > 0) {
        	NodeList inputs = CodeGeneratorUtils.find(externalCond.item(0), "./mms:inputVariables/mms:inputVariable");
        	String params = String.valueOf(inputs.getLength());
        	for (int i = 0; i < inputs.getLength(); i++) {
        		Node input = inputs.item(i);
        		String accessor = "";
        		//Parameter arguments must be passed by reference
        		if (parameters.containsKey(SAMRAIUtils.xmlTransform(input.getFirstChild().getFirstChild(), null))) {
        			accessor = "&";
        		}
        		params = params + ", " + accessor + SAMRAIUtils.xmlTransform(input.getFirstChild().getFirstChild(), null);
        	}
        	params = params + ", " + "dx";
        	String domainInit = "";
        	String patchInit = "";
        	String coordLast = "";
        	for (int i = 0; i < pi.getDimensions(); i++) {
        		domainInit = domainInit + ", d_grid_geometry->getXLower()[" + i + "]";
        		patchInit = patchInit + ", boxfirst[" + i + "]";
        		coordLast = coordLast + ", " + pi.getCoordinates().get(i) + "last";
        	}
           	params = params + domainInit + patchInit + ", d_ghost_width" + coordLast;
        	return "SAMRAI_External_Data::external_loadData(" + params + ");" + NL;
        }
        return "";
    }
}
