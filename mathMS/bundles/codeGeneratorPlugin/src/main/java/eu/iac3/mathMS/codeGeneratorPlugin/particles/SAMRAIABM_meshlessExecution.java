/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.codeGeneratorPlugin.particles;

import eu.iac3.mathMS.codeGeneratorPlugin.CGException;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.CodeGeneratorUtils;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.ProblemInfo;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.SAMRAIUtils;

import java.util.ArrayList;

import org.osgi.service.log.LogService;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * Execution flow for particles in SAMRAI.
 * @author bminyano
 *
 */
public final class SAMRAIABM_meshlessExecution {
    static final String NL = System.getProperty("line.separator");
    static final String IND = "\u0009";
    static final int THREE = 3;
    static final int FOUR = 4;
    static final int FIVE = 5;
    static final String MTURI = "http://www.w3.org/1998/Math/MathML";
    static final String MMSURI = "urn:mathms";
    static LogService logservice;
    
    /**
     * Private constructor.
     */
    private SAMRAIABM_meshlessExecution() { };
    
    /**
     * Create the code for the execution step.
     * @param pi                    The problem information
     * @return                      The file
     * @throws CGException          CG00X External error
     */
    public static String create(ProblemInfo pi) throws CGException {
        String result = "";
        Document doc = pi.getProblem();
        int dimensions = pi.getSpatialDimensions();
        String currentIndent = IND;
        try {
            String[][] xslParams = new String [6][2];
            xslParams[0][0] = "condition";
            xslParams[0][1] = "false";
            xslParams[1][0] = "indent";
            xslParams[2][0] = "dimensions";
            xslParams[2][1] = String.valueOf(pi.getDimensions());
            xslParams[3][0] = "timeCoord";
            xslParams[3][1] = pi.getTimeCoord();
            xslParams[4][0] = "hardCondition";
            xslParams[5][0] = "segCondition";
            xslParams[4][1] = "";
            xslParams[5][1] = "";
            
            Element coordinates = CodeGeneratorUtils.createElement(pi.getProblem(), null, "coords");
            for (int i = 0; i < pi.getDimensions(); i++) {
                String coord = pi.getCoordinates().get(i);
                Element coordE = CodeGeneratorUtils.createElement(pi.getProblem(), null, coord);
                coordE.setTextContent(coord);
                coordinates.appendChild(coordE);  
            }
            //Check if there are periodical boundaries
            boolean periodical = CodeGeneratorUtils.find(doc, "//mms:boundaryCondition/mms:type/mms:periodical").getLength() > 0;
            //Control when the synchronizations has to be launched
            boolean sync = false;
            //Variable declaration
            DocumentFragment globalScope = doc.createDocumentFragment();
            String query = "//mms:rules//sml:algorithm";
            NodeList scopes = CodeGeneratorUtils.find(doc, query);
            for (int k = 0; k < scopes.getLength(); k++) {
                globalScope.appendChild(scopes.item(k).cloneNode(true));
            }
            
            String indexCell = "";
            for (int i = 0; i < pi.getDimensions(); i++) {
                indexCell = indexCell + "index" + i + " - boxfirst(" + i + "), ";
            }
            indexCell = indexCell.substring(0, indexCell.lastIndexOf(","));
            DocumentFragment scope = doc.createDocumentFragment();
            
            NodeList rules = CodeGeneratorUtils.find(doc, "//mms:ruleExecutionOrder/mms:rule");
            String instructions = "";
            if (pi.getEvolutionStep().equals("AllAgents")) {
                String insideDomain = "";
                for (int k = 0; k < dimensions; k++) {
                    //Initialization of the variables
                    currentIndent = currentIndent + IND;
                    insideDomain = insideDomain + "index" + k + " >= boxfirst1(" + k + ") && index" + k + " <= boxlast1(" + k + ") && ";
                }
                insideDomain = insideDomain.substring(0, insideDomain.lastIndexOf(" && "));
                String cellInit = createExecutionBlockLoop(IND, dimensions)
                        + currentIndent + "Particles* part = particleVariables->getItem(idx);" + NL
                        + createParticleAccess(currentIndent, dimensions);
                String cellEnd = currentIndent + "}" + NL + SAMRAIUtils.createCellLoopEnd(currentIndent, dimensions);
                
                currentIndent = currentIndent + IND;
                //Create code for all elements
                Element previousRule = null;
                DocumentFragment instructionAcc = doc.createDocumentFragment();
                for (int i = 0; i < rules.getLength(); i++) {
                    Element ruleOrder = (Element) rules.item(i).getChildNodes();
                    String ruleName = ruleOrder.getTextContent();
                    String ruleType = CodeGeneratorUtils.find(doc, 
                            "//*[(local-name() = 'updateRule' or local-name() = 'gatherRule') and descendant::" 
                            + "mms:name = '" + ruleName + "']").item(0).getLocalName();
                    scopes = CodeGeneratorUtils.find(doc, "//*[(local-name() = 'updateRule' or local-name() = 'gatherRule') and descendant::" 
                            + "mms:name = '" + ruleName + "']//sml:algorithm");
                    for (int k = 0; k < scopes.getLength(); k++) {
                        scope.appendChild(scopes.item(k).cloneNode(true));
                    }
                    boolean unifiable = false;
                    if (previousRule != null) {
                        unifiable = CodeGeneratorUtils.checkUnifiable(null, ruleOrder, previousRule, pi.getVariables(), 
                                pi.getDimensions());
                    }
                    //Patch iterators
                    if (!unifiable) {
                        if (i > 0) {
                            xslParams[1][1] = String.valueOf(currentIndent.length());
                            NodeList blocks = CodeGeneratorUtils.unifyInstructions(instructionAcc, pi).getChildNodes();
                            //NodeList blocks = instructionAcc.getChildNodes();
                            for (int j = 0; j < blocks.getLength(); j++) {
                                instructions = instructions + SAMRAIUtils.xmlTransform(blocks.item(j), xslParams, "coords", coordinates) + NL;
                            }
                            
                            result = result + patchIterator(pi.getProblem(), pi)
                                    + SAMRAIUtils.getParticlesVariableDeclaration(scope, 1, pi)
                                    + cellInit + instructions + cellEnd
                                    + "}" + NL;
                            scope = doc.createDocumentFragment();
                            //If there are periodical boundaries the particle position has to be corrected.
                            if (sync) {
                                result = result + syncs();
                                //If there are periodical boundaries the particle position has to be corrected.
                                if (periodical) {
                                    result = result + patchIterator(pi.getProblem(), pi)
                                        + checkPositions(dimensions)
                                        + "}" + NL;
                                }
                            }

                            instructions = "";
                            instructionAcc = doc.createDocumentFragment();
                        }
                        sync = false;
                    }
                    
                    NodeList algorithm = CodeGeneratorUtils.find(doc, "//*[(local-name() = 'updateRule' or local-name() = 'gatherRule') and " 
                            + "descendant::mms:name = '" + ruleName + "']//sml:algorithm/*");
                    for (int k = 0; k < algorithm.getLength(); k++) {
                        Element block = SAMRAIUtils.preProcessParticlesBlock(algorithm.item(k).cloneNode(true), pi.getVariables(),
                                pi.getCoordinates(), false, pi);
                        instructionAcc.appendChild(block);
                        
                    }
                    
                    //If the actual block has stencil > 0 then the next block has to synchronize
                    DocumentFragment blockScope = doc.createDocumentFragment();
                    Element blockList = (Element) CodeGeneratorUtils.find(doc, "//*[(local-name() = 'updateRule' or local-name() = 'gatherRule')"
                            + " and descendant::mms:name = '" + ruleName + "']").item(0).cloneNode(true);
                    blockScope.appendChild(blockList);
                    if (ruleType.equals("gatherRule")) {
                        sync = true;
                    }
                    previousRule = (Element) ruleOrder.cloneNode(true);
                }
                xslParams[1][1] = String.valueOf(currentIndent.length());
                NodeList blocks = CodeGeneratorUtils.unifyInstructions(instructionAcc, pi).getChildNodes();
                for (int j = 0; j < blocks.getLength(); j++) {
                    instructions = instructions + SAMRAIUtils.xmlTransform(blocks.item(j), xslParams, "coords", coordinates)
                            + NL;
                }
                result = result + patchIterator(pi.getProblem(), pi)
                    + SAMRAIUtils.getParticlesVariableDeclaration(scope, 1, pi)
                    + cellInit + instructions + cellEnd
                    + "}" + NL;
                
                //Movement related 
                if (CodeGeneratorUtils.find(pi.getProblem(), "//mms:mesh/mms:motion").getLength() > 0) {
                    //Set new positions
                    String positions = calculateNewPositions(pi, currentIndent);
                    if (sync) {
                        if (sync) {
                            result = result + syncs();
                            //If there are periodical boundaries the particle position has to be corrected.
                            if (periodical) {
                                result = result + patchIterator(pi.getProblem(), pi)
                                    + checkPositions(dimensions)
                                    + "}" + NL;
                            }
                        }
                        sync = false;
                    }
                    result = result + patchIterator(pi.getProblem(), pi)
                        + SAMRAIUtils.getParticlesVariableDeclaration(scope, 1, pi)
                        + cellInit + positions + cellEnd
                        + "}" + NL;
                    result = result + syncs()
                        + patchIterator(pi.getProblem(), pi)
                        + checkPositions(dimensions)
                        + "}" + NL;
                    //Particle movement
                    String movement = createMovement(pi, currentIndent);
                    result = result + patchIterator(pi.getProblem(), pi)
                        + cellInit + movement + cellEnd
                        + "}" + NL;
                    sync = true;
                }

                
                //The last step is the boundary calculations
                String boundaries = SAMRAIABM_meshlessBoundaries.createBoundaries(IND + IND + IND, pi, indexCell);
                if (!boundaries.equals("")) {
                    if (sync) {
                        if (sync) {
                            result = result + syncs();
                            //If there are periodical boundaries the particle position has to be corrected.
                            if (periodical) {
                                result = result + patchIterator(pi.getProblem(), pi)
                                + checkPositions(dimensions)
                                + "}" + NL;
                            }
                        }
                        sync = false;
                    }
                    result = result + patchIterator(pi.getProblem(), pi)
                        + SAMRAIUtils.getParticlesVariableDeclaration(scope, 1, pi)
                        + cellInit + boundaries + cellEnd
                        + "}" + NL;
                }
                if (sync) {
                    result = result + syncs();
                    if (periodical) {
                        result = result + patchIterator(pi.getProblem(), pi)
                            + checkPositions(dimensions)
                            + "}" + NL;
                    }
                }
            }
            else  {
                xslParams[1][1] = "0";
                result = result + "int pit = 0;" + NL
                        + syncs();
                if (periodical) {
                    result = result + patchIterator(pi.getProblem(), pi)
                        + checkPositions(dimensions)
                        + "}" + NL;
                }
                //Selection of the particle
                result = result + patchIterator(pi.getProblem(), pi)
                    + SAMRAIUtils.getParticlesVariableDeclaration(globalScope, 1, pi)
                    + createRandomSelection(pi.getCoordinates());
                //Single variables
                result = result + createSingleVars(pi.getProblem(), pi.getCoordinates());
                DocumentFragment instructionAcc = doc.createDocumentFragment();
                for (int i = 0; i < rules.getLength(); i++) {
                    Element ruleOrder = (Element) rules.item(i);
                    String ruleName = ruleOrder.getTextContent();
                    NodeList algorithm = CodeGeneratorUtils.find(doc, 
                            "//*[(local-name() = 'updateRule' or local-name() = 'gatherRule') and descendant::"
                            + "mms:name = '" + ruleName + "']//sml:algorithm/*");
                    for (int k = 0; k < algorithm.getLength(); k++) {
                        Element block = SAMRAIUtils.preProcessParticlesBlock(algorithm.item(k).cloneNode(true), pi.getVariables(), 
                                pi.getCoordinates(), false, pi);
                        instructionAcc.appendChild(block);
                    }
                }
                NodeList blocks = CodeGeneratorUtils.unifyInstructions(instructionAcc, pi).getChildNodes();
                //NodeList blocks = instructionAcc.getChildNodes();
                for (int j = 0; j < blocks.getLength(); j++) {
                    result = result + SAMRAIUtils.xmlTransform(blocks.item(j), xslParams, "coords", coordinates) + NL;
                }
                result = result + "}" + NL;
                //Movement
                result = result + calculateNewPositions(pi, currentIndent) + NL;
                result = result + createMovement(pi, currentIndent) + NL;
                //The last step is the boundary calculations
                result = result + SAMRAIABM_meshlessBoundaries.createBoundaries("", pi, indexCell) + NL;
            }
            
            return result;
        } 
        catch (CGException e) {
            throw e;
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            throw new CGException(CGException.CG00X, e.getMessage());
        }
    }
    
    /**
     * Create minimum and maximum indexes limits for possible loops.
     * @param problem       The problem
     * @param coords        The coordinates
     * @return              The code
     * @throws CGException  CG00X External error
     */
    private static String createSingleVars(Document problem, ArrayList<String> coords) throws CGException {
        String result = "";
        //Iterate over interaction variables.
        if (CodeGeneratorUtils.find(problem, "//mms:rules//sml:iterateOverInteractions").getLength() > 0) {
            for (int i = 0; i < coords.size(); i++) {
                result = result + "int min" + coords.get(i) + "Index = MAX(boxfirst(" + i + "), index" + i + " - numberOfCellsRadius_" 
                        + coords.get(i) + ");" + NL
                        + "int max" + coords.get(i) + "Index = MAX(boxlast(" + i + "), index" + i + " + numberOfCellsRadius_" + coords.get(i) 
                        + ");" + NL;
            }
        }
        return result;
    }
    
    /**
     * Create the variables for the random selection of a particle.
     * @param coords    The coordinates
     * @return          The code
     */
    private static String createRandomSelection(ArrayList<String> coords) {
        String size = "";
        String indexes = "";
        String indexes2 = "";
        for (int i = 0; i < coords.size(); i++) {
            String coord = coords.get(i);
            size = size + "(boxlast1(" + i + ") - boxfirst1(" + i + ")) * ";
            indexes = indexes + "int index" + i + " = index" + coord + "Of(selected_cell);" + NL;
            indexes2 = indexes2 + "index" + i + ", ";
        }
        indexes2 = indexes2.substring(0, indexes2.lastIndexOf(", "));
        
        return "int selected_cell = " + size + "gsl_rng_uniform(r_var);" + NL
                + indexes + NL
                + "hier::Index idx(" + indexes2 + ");" + NL
                + "Particles* part = particleVariables->getItem(idx);" + NL
                + "Particle* particle = part->getParticle(part->getNumberOfParticles() * gsl_rng_uniform(r_var));" + NL;
    }
    
    /**
     * Create the code to calculate the new position.
     * @param pi            The problem information
     * @param indent        The indentation
     * @return              The code
     * @throws CGException  CG00X External error
     */
    private static String calculateNewPositions(ProblemInfo pi, String indent) throws CGException {
        String result = "";

        for (int i = 0; i < pi.getDimensions(); i++) {
            String coord = pi.getCoordinates().get(i);
            String posVar = "";
            try {
                posVar = CodeGeneratorUtils.find(pi.getProblem(), "//mms:agentSpeed[mms:coordinate = '" + coord + "']/" 
                        + "mms:agentProperty").item(0).getTextContent();
            } 
            catch (Exception e) {
                throw new CGException(CGException.CG001, "Agent speed not defined for coordinate " + coord);
            }
            result = result + indent + "particle->position" + coord + " = particle->position" + coord + "_p + simPlat_dt * particle->" + posVar 
                    + ";" + NL;
        }

        return result;
    }
    
    /**
     * Creates the code to move the particles.
     * @param pi        The process info
     * @param indent    The actual indent
     * @return          The code
     */
    private static String createMovement(ProblemInfo pi, String indent) {
        String result = "";
        String indexes = "";
        String pos = "";
        String posP = "";
        for (int i = 0; i < pi.getDimensions(); i++) {
            String coord = pi.getCoordinates().get(i);
            indexes = indexes + "index" + i + ", ";
            posP = posP + "particle->position" + coord + "_p, ";
            pos = pos + "particle->position" + coord + ", ";
        }
        
        result = result + indent + "moveParticles(patch, particleVariables, part, particle, " + indexes + posP + pos + "simPlat_dt, current_time, pit);" + NL;

        return result;
    }
    
    /**
     * Create the code to access the data inside the particles.
     * @param actualIndent      The actual indentation
     * @param dimensions        The number of spatial dimensions
     * @return                  The code
     */
    public static String createParticleAccess(String actualIndent, int dimensions) {
        String particleAccess = "";
        //Access index data and iterate over all the particles in the Particles data
        particleAccess = particleAccess + actualIndent + "for (int pit = part->getNumberOfParticles() - 1; pit >= 0; pit--) {" + NL;
        actualIndent = actualIndent + IND;
        particleAccess = particleAccess + actualIndent + "Particle* particle = part->getParticle(pit);" + NL;
        return particleAccess;
    }
    
    
    /**
     * Code for syncs.
     * @return        The code
     */
    private static String syncs() {
        return "//Fill ghosts and periodical boundaries, but no other boundaries" + NL
            + "d_bdry_sched_advance[0]->fillData(current_time, false);" + NL;
    }

    /**
     * Creates the loop for an execution block.
     * @param indent                The indentation
     * @param dimensions            The spatial dimensions
     * @return                      The loop
     */
    public static String createExecutionBlockLoop(String indent, int dimensions) {
        String block = "";
        String coordIndex = "";
        
        String actualIndent = indent;
        for (int i = dimensions - 1; i >= 0; i--) {
            //Loop iteration
            block =  block + actualIndent + "for (int index" + i + " = boxfirst(" + i + "); index" + i + " <= boxlast(" + i + "); index" + i 
                + "++) {" + NL;
            actualIndent = actualIndent + IND;
            coordIndex =  "index" + i + ", " + coordIndex;
        }
        coordIndex = coordIndex.substring(0, coordIndex.lastIndexOf(","));
        
        return block + actualIndent + "hier::Index idx(" + coordIndex + ");" + NL;
    }
    
    /**
     * Creates the loop end for an execution block.
     * @param actualIndent      The actual indent
     * @param dimensions        The spatial dimensions
     * @return                  The code
     */
    public static String createExecutionBlockLoopEnd(String actualIndent, int dimensions) {
        String block = "";
        for (int i = 0; i < dimensions; i++) {
            actualIndent = actualIndent.substring(1);
            block =  block + actualIndent + "}" + NL;
        }
        
        return block;
    }
    
    /**
     * Create the calls to checkPosition to correct the position of the particles at the boundaries when there are periodical boundaries.
     * @param dimensions        The dimensions of the problem
     * @return                    The code
     */
    private static String checkPositions(int dimensions) {
        String block = "";
        String coordIndex = "";
        String currentIndent = IND;
        
        //Loop iteration
        for (int i = dimensions - 1; i >= 0; i--) {
            block =  block + currentIndent + "for (int index" + i + " = boxfirst(" + i + "); index" + i + " <= boxlast(" + i + "); index" + i 
                + "++) {" + NL;
            currentIndent = currentIndent + IND;
            coordIndex =  "index" + i + ", " + coordIndex;
        }
        coordIndex = coordIndex.substring(0, coordIndex.lastIndexOf(","));
        
        //Create Index for indexData
        block = block + currentIndent + "hier::Index idx(" + coordIndex + ");" + NL
            + currentIndent + "//Correct the position if there is periodical boundary" + NL;
        
        //call checkposition
        block = block + currentIndent + "Particles* part = particleVariables->getItem(idx);" + NL
            + currentIndent + "checkPosition(patch, " + coordIndex + ", part);" + NL;
        
        //Close loop
        for (int i = 0; i < dimensions; i++) {
            currentIndent = currentIndent.substring(1);
            block =  block + currentIndent + "}" + NL;
        }
        
        return block;
    }
    
    /**
     * Code for the patch iteration and syncs.
     * @param problem           The problem
     * @param pi                The problem information
     * @return                  The code
     * @throws CGException 
     */
    private static String patchIterator(Document problem, ProblemInfo pi) throws CGException {
        return "for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {" + NL
            + IND + "const std::shared_ptr<hier::Patch > patch = *p_it;" + NL
            + IND + "std::shared_ptr< pdat::IndexData<Particles, pdat::CellGeometry > > particleVariables(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles, pdat::CellGeometry >, hier::PatchData>(patch->getPatchData(d_particleVariables_id)));" + NL
            + IND + "const hier::Index boxfirst1 = patch->getBox().lower();" + NL
            + IND + "const hier::Index boxlast1  = patch->getBox().upper();" + NL
            + IND + "const hier::Index boxfirst = particleVariables->getGhostBox().lower();" + NL
            + IND + "const hier::Index boxlast  = particleVariables->getGhostBox().upper();" + NL
            + SAMRAIUtils.generateLasts(pi.getCoordinates(), IND) + NL;
    }
    
}
