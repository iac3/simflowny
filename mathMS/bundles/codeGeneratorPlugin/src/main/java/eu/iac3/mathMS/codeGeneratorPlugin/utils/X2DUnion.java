package eu.iac3.mathMS.codeGeneratorPlugin.utils;

/**
 * Class that represents a 2D union (line).
 * @author bminyano
 *
 */
public class X2DUnion {

    String a;
    String b;
    
    /**
     * Constructor.
     * @param pointA    point a
     * @param pointB    point b
     */
    public X2DUnion(String pointA, String pointB) {
        a = pointA;
        b = pointB;
    }

    public String getA() {
        return a;
    }

    public String getB() {
        return b;
    }
    
    /**
     * Comparison.
     * @param comp  Element to compare to
     * @return      true if equals
     */
    public boolean equals(Object comp) {
        return (a.equals(((X2DUnion)comp).getA()) && b.equals(((X2DUnion)comp).getB())) 
                || (b.equals(((X2DUnion)comp).getA()) && a.equals(((X2DUnion)comp).getB()));
    }
}
