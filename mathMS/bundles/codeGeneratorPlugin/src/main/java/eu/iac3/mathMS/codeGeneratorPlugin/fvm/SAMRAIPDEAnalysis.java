package eu.iac3.mathMS.codeGeneratorPlugin.fvm;

import java.util.ArrayList;

import org.osgi.service.log.LogService;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import eu.iac3.mathMS.codeGeneratorPlugin.CGException;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.CodeGeneratorUtils;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.ProblemInfo;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.SAMRAIUtils;

public final class SAMRAIPDEAnalysis {
    static final String NL = System.getProperty("line.separator");
    static final String IND = "\u0009";
    static LogService logservice;
    /**
     * Private constructor.
     */
    private SAMRAIPDEAnalysis() { };
    
    
    /**
     * Create the code for the analysis.
     * @param pi                    The problem information
     * @return                      The file
     * @throws CGException          CG004 External error
     */
    public static String create(ProblemInfo pi) throws CGException {
        StringBuffer result = new StringBuffer();
        Document doc = pi.getProblem();
        
        int dimensions = pi.getSpatialDimensions();
        String currIndent = IND + IND;
        try {
            //Control when the synchronizations has to be executed
            boolean sync = false;
            //Iterate over all the blocks
            int blocks = CodeGeneratorUtils.getBlocksNum(doc, "//mms:analysis/sml:simml");
            
            String commonVariables = IND + IND + "//Get the dimensions of the patch" + NL 
                + IND + IND + "hier::Box pbox = patch->getBox();" + NL
                + IND + IND + "const hier::Index boxfirst = patch->getBox().lower();" + NL
                + IND + IND + "const hier::Index boxlast = patch->getBox().upper();" + NL + NL
                + IND + IND + "//Get delta spaces into an array. dx, dy, dz." + NL
                + IND + IND + "std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));" + NL
                + IND + IND + "const double* dx  = patch_geom->getDx();" + NL + NL
                + IND + IND + "//Auxiliary definitions" + NL;
            String cellLoop = SAMRAIPDEExecution.createCellLoop(pi);
            //Initialization of the iteration variables
            for (int k = 0; k < dimensions; k++) {
                //Initialization of the variables
                String coordString = pi.getCoordinates().get(k);
                commonVariables =  commonVariables + currIndent + "int " + coordString + "last = boxlast(" + k + ")-boxfirst(" 
                        + k + ") + 2 + 2 * d_ghost_width;" + NL;
            }
            //Indent the nested code
            for (int j = 0; j < dimensions; j++) {
                currIndent = IND + currIndent;
            }   
            String cellLoopEnd = SAMRAIUtils.createCellLoopEnd(currIndent, dimensions);
            String endPatchIteration = IND + "}" + NL
                    + "}" + NL;

            
            //Iterate over the blocks
            for (int i = 0; i < blocks; i++) {
                String block = "";
                //patch loop and synchronizations
                block = patchIterator(pi.getRegionIds(), sync, dimensions, i);
                //Reset sync variable
                if (sync) {
                    sync = false;
                }
                //If the current block has stencil > 0 then the next block has to synchronize
                DocumentFragment blockScope = doc.createDocumentFragment();
                NodeList blockList = CodeGeneratorUtils.find(doc, CodeGeneratorUtils.getAnalysisSynchronizationQuery(i));
                for (int j = 0; j < blockList.getLength(); j++) {
                    blockScope.appendChild(blockList.item(j).cloneNode(true));
                }
                if (blockList.getLength() > 0) {
                    sync = true;
                }
                //Get scope for the variable declarations
                DocumentFragment scope = doc.createDocumentFragment();
                String query = "/mms:discretizedProblem/mms:analysis/sml:simml/*[" + (i + 1) + "]";
                NodeList scopes = CodeGeneratorUtils.find(doc, query);
                for (int k = 0; k < scopes.getLength(); k++) {
                    scope.appendChild(scopes.item(k).cloneNode(true));
                }
                block = block + SAMRAIUtils.getPDEVariableDeclaration(scope, pi, 2, new ArrayList<String>(), 
                		"patch->", false, false) + NL
                    + commonVariables;
                     
                //Cell Loop
                block = block + cellLoop;
                
                //For every region (first the lower precedents) process the instructions
                String analysis = analysis(currIndent, i, pi);             
                block = block + analysis;
                
                //close cell loop
                block = block + cellLoopEnd;
                //close patch loops
                block = block + endPatchIteration;
                
                if (!analysis.equals("")) {
                    result.append(block);
                }
            }
            return result.toString();
        } 
        catch (CGException e) {
            e.printStackTrace(System.out);
            throw e;
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            throw new CGException(CGException.CG00X, e.getMessage());
        }
    }
    
    /**
     * Generates the analysis code for a region.
     * @param pi                The problem common information
     * @param currentIndent      The actual indent 
     * @param blockNum          The block number
     * @return                  The code
     * @throws CGException      CG00X External error
     */
    private static String analysis(String currentIndent, int blockNum, ProblemInfo pi) throws CGException {
        Document doc = pi.getProblem();
        String result = "";
        String[][] xslParams = new String [3][2];
        xslParams[0][0] =  "condition";
        xslParams[0][1] =  "false";
        xslParams[1][0] =  "indent";
        xslParams[2][0] =  "dimensions";
        xslParams[2][1] =  String.valueOf(pi.getDimensions());
                
        int dimensions = pi.getSpatialDimensions();
        //Generate the index for the variables
        String index = "";
        for (int i = 0; i < dimensions; i++) {
            index =  index + pi.getCoordinates().get(i) + ", ";
        }
        index = index.substring(0, index.lastIndexOf(","));
        //Region analysis
        NodeList blockList = CodeGeneratorUtils.find(doc, "/mms:discretizedProblem/mms:analysis/sml:simml/*[" + (blockNum + 1) + "]");
        for (int i = 0; i < blockList.getLength(); i++) {
            Element block = (Element) blockList.item(i).cloneNode(true);
            block = (Element) SAMRAIUtils.preProcessPDEBlock(block, pi, false, false);
            if (block.hasChildNodes()) {                
                //Check that the stencil points needed by the method are available
                int stencil = Integer.parseInt(block.getAttribute("stencilAtt"));
      
                //Add condition to calculate only inside of the array limits when there are stencil
                if (stencil > 0) {
                    result = result + currentIndent + "if (" + SAMRAIPDEExecution.getLimitCondition(pi.getCoordinates(), stencil) + ") {" + NL;
                    currentIndent = currentIndent + IND;
                }
                //Instruction to code
                xslParams[1][1] = String.valueOf(currentIndent.length());
                result = result + SAMRAIUtils.xmlTransform(block, xslParams);

                if (stencil > 0) {
                    currentIndent = currentIndent.substring(1);
                    result = result + currentIndent + "}" + NL;
                }
            }
        }
        return result;
    }
    
    /**
     * Code for the patch iteration and syncs.
     * @param regions          Thte regions of the problem
     * @param sync              If there is a pending sync.
     * @param dimensions        The dimensions of the problem
     * @param blockNumber       The number of the block for the synchronization
     * @return                  The code
     */
    static String patchIterator(ArrayList<String> regions, boolean sync, int dimensions, int blockNumber) {
        String block = "for (int ln=0; ln<=d_patch_hierarchy->getFinestLevelNumber(); ++ln ) {" + NL
            + IND + "std::shared_ptr<hier::PatchLevel > level(d_patch_hierarchy->getPatchLevel(ln));" + NL;
        if (sync) {
            block = block + IND + "//Fill ghosts and periodical boundaries, but no other boundaries" + NL
                + IND + "d_bdry_sched_analysis" + blockNumber + "[ln]->fillData(current_time, false);" + NL;
        }
        return block + IND + "for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {" + NL
            + IND + IND + "const std::shared_ptr<hier::Patch >& patch = *p_it;" + NL
            + NL;
    }
}
