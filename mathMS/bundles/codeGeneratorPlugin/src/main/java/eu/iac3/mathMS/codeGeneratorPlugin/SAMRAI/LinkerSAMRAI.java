/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.codeGeneratorPlugin.SAMRAI;

import eu.iac3.mathMS.codeGeneratorPlugin.CGException;
import eu.iac3.mathMS.codeGeneratorPlugin.fvm.SAMRAIPDEExecution;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.CodeGeneratorUtils;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.FileReadInformation;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.ProblemInfo;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.SAMRAIUtils;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.TimeInterpolationInfo;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.TimeInterpolationInfo.TimeInterpolator;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.VariableInfo;
import eu.iac3.mathMS.codesDBPlugin.CodesDB;
import eu.iac3.mathMS.codesDBPlugin.CodesDBImpl;

import java.io.File;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceReference;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.service.log.LogService;
import org.osgi.util.tracker.ServiceTracker;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

/**
 * Compiles the code.
 * @author bminyano
 *
 */
public class LinkerSAMRAI {
    static BundleContext context;
    static LogService logservice = null;
    static final String IND = "\u0009";
    static final String NL = System.getProperty("line.separator");
    static final int THREE = 3;
    static final double ABSERROR = 1e-10;
    boolean activeSlicers;
    static final int INTERPHASELENGTH = 3;
    ArrayList<String> varsToAllocateAfterRestart;
    
    /**
     * Constructor to take the bundle context.
     */
    public LinkerSAMRAI() {
        super();
        context = FrameworkUtil.getBundle(getClass()).getBundleContext();
        if (context != null) {
            @SuppressWarnings({ "rawtypes", "unchecked" })
			ServiceTracker logServiceTracker = new ServiceTracker(context, org.osgi.service.log.LogService.class.getName(), null);
            logServiceTracker.open();
            logservice = (LogService) logServiceTracker.getService();
        }
    }
    
    /**
     * Link intermediate code with SAMRAI.
     * @param id                The problem identifier
     * @param executionStep     The execution step code
     * @param initializations   The initialization code
     * @param finalization      The finalization code
     * @param mapping           The mapping code
     * @param functions         The functions code
     * @param analysis          The analysis code
     * @param synchronization   The synchronization code
     * @param movement          The movement code
     * @param pi 				The problem information
     * @return                  The problem name
     * @throws CGException      CG00X External error
     */
    public String link(String id, String executionStep, String initializations, String finalization, 
            String mapping, String functions, String analysis, String synchronization, String movement, ProblemInfo pi) throws CGException {
        try {
            CodesDB cdb = new CodesDBImpl();
            //Getting the configuration simPlat
            String mpich;
            String hdf5;
            String samrai;
            String silo;
            @SuppressWarnings("rawtypes")
			ServiceReference serviceReference = context.getServiceReference(ConfigurationAdmin.class.getName());
            @SuppressWarnings("unchecked")
			ConfigurationAdmin configAdmin = (ConfigurationAdmin) context.getService(serviceReference); 
            Configuration config = configAdmin.getConfiguration("eu.iac3.mathMS.codeGeneratorPlugin"); 
            Dictionary< ? , ? > properties = config.getProperties();
            if (properties != null) {
                mpich = (String) properties.get("eu.iac3.codeGeneratorPlugin.mpichHome");
                hdf5 = (String) properties.get("eu.iac3.codeGeneratorPlugin.hdf5Home");
                samrai = (String) properties.get("eu.iac3.codeGeneratorPlugin.SAMRAIHome");
                silo = (String) properties.get("eu.iac3.codeGeneratorPlugin.siloHome");
            }
            else {
                mpich = System.getProperty("eu.iac3.codeGeneratorPlugin.mpichHome");
                hdf5 = System.getProperty("eu.iac3.codeGeneratorPlugin.hdf5Home");
                samrai = System.getProperty("eu.iac3.codeGeneratorPlugin.SAMRAIHome");
                silo = System.getProperty("eu.iac3.codeGeneratorPlugin.siloHome");
            }
            if (logservice != null) {
                logservice.log(LogService.LOG_INFO, "SimPlat configuration");
                logservice.log(LogService.LOG_INFO, "MPICH: " + mpich);
                logservice.log(LogService.LOG_INFO, "HDF5: " + hdf5);
                logservice.log(LogService.LOG_INFO, "SAMRAI: " + samrai);
                logservice.log(LogService.LOG_INFO, "SILO: " + silo);
            }
            
            //Name
            String problemName = pi.getProblemName();
            //Spatial dimensions
            int dimensions = pi.getDimensions();
            if (dimensions == 1) {
                throw new CGException(CGException.CG004, "Simplat + SAMRAI does not allow 1D simulation");
            }
  
            //Mesh
            activeSlicers = dimensions == 3 && !pi.isHasABMMeshFields() && !pi.isHasABMMeshlessFields();
                                  
            //Create SAMRAIConnector.cpp
            cdb.addCodeFile(id, "library", "SAMRAIConnector.cpp",
                    createSAMRAIConnector(pi));
            //Create Problem.cpp
            cdb.addCodeFile(id, "sources", "Problem.cpp",
                    createProblem(pi, executionStep, initializations, finalization, 
                    mapping, analysis, synchronization, movement));
            //Create Problem.h
            cdb.addCodeFile(id, "headers", "Problem.h",
                    createProblemH(pi, mapping, movement, synchronization));
            //Copy Commons.cpp
            cdb.addCodeFile(id, "sources", "Commons.cpp",
            		CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/Commons.cpp"), 0));
            //Create Commons.h
            cdb.addCodeFile(id, "headers", "Commons.h",
                    createCommonsH(pi));
            //Create Function.cpp
            cdb.addCodeFile(id, "sources", "Functions.cpp",
                    createFunctions(functions, pi));
            //Create Function.h
            cdb.addCodeFile(id, "headers", "Functions.h",
                    createFunctionsH(functions, pi));
            //Copy MainRestartData.cpp
            cdb.addCodeFile(id, "library", "MainRestartData.cpp",
                    createMainRestartData(pi));
            //Copy MainRestartData.h
            cdb.addCodeFile(id, "library", "MainRestartData.h",
                    createMainRestartDataH(pi));
            //Extra files for particles
            if (pi.isHasParticleFields() || pi.isHasABMMeshlessFields()) {
                //Create Particle.C
            	for (String speciesName : pi.getParticleSpecies()) {
                    cdb.addCodeFile(id, "sources", "Particle_" + speciesName + ".C",
                            createParticle(pi, speciesName));
                    //Create Particle.h
                    cdb.addCodeFile(id, "headers", "Particle_" + speciesName + ".h",
                            createParticleH(pi, speciesName));
            	}
                //Create Particles.C
                cdb.addCodeFile(id, "library", "Particles.C", 
                        createParticles(pi.getTimeInterpolationInfo(), pi.getVariableInfo().getParticleVariables()));
                //Create Particles.h
                cdb.addCodeFile(id, "library", "Particles.h", 
                		createParticlesH(pi.getTimeInterpolationInfo(), pi.getVariableInfo().getParticleVariables()));
                //Create ParticleDataWriter.C
                cdb.addCodeFile(id, "library", "ParticleDataWriter.C",
                        createParticleDataWriter(pi));
                //Copy ParticleDataWriter.h
                cdb.addCodeFile(id, "library", "ParticleDataWriter.h", 
                		createParticleDataWriterH(pi));
                //Create NonSync.C
                cdb.addCodeFile(id, "library", "NonSync.C",
                        createNonSync(pi));
                //Create NonSync.h
                cdb.addCodeFile(id, "library", "NonSync.h",
                        createNonSyncH(pi));
                //Copy NonSyncs.C
                cdb.addCodeFile(id, "library", "NonSyncs.C", 
                        CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/NonSyncs.C"), 0));
                //Copy NonSyncs.h
                cdb.addCodeFile(id, "library", "NonSyncs.h", 
                        CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/NonSyncs.h"), 0));
                //Create ParticleRefine.C
                cdb.addCodeFile(id, "library", "ParticleRefine.C",
                        createParticleRefine(pi));
                //Create ParticleRefine.h
                cdb.addCodeFile(id, "library", "ParticleRefine.h",
                        createParticleRefineH(pi));
            }
            if (pi.isHasMeshFields() || pi.isHasABMMeshFields() || pi.isHasParticleFields()) {
                //Copy LagrangianPolynomicRefine.C
                cdb.addCodeFile(id, "library", "LagrangianPolynomicRefine.C",
                		CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/LagrangianPolynomicRefine.C"), 0));
                //Copy LagrangianPolynomicRefine.h
                cdb.addCodeFile(id, "library", "LagrangianPolynomicRefine.h",
                		CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/LagrangianPolynomicRefine.h"), 0));
                //Create TimeInterpolator.C
                cdb.addCodeFile(id, "library", "TimeInterpolator.C",
                    createTimeInterpolator(pi.getTimeInterpolationInfo(), pi));
	            //Create TimeInterpolator.h
	            cdb.addCodeFile(id, "library", "TimeInterpolator.h",
                    createTimeInterpolatorH(pi));
            }
            //Create RefineClasses.C
            cdb.addCodeFile(id, "library", "RefineClasses.C",
                createRefineClasses(pi.getTimeInterpolationInfo()));
            //Create RefineClasses.h
            cdb.addCodeFile(id, "library", "RefineClasses.h",
                createRefineClassesH(pi.getTimeInterpolationInfo()));	
            //Create RefineTimeTransaction.C
            cdb.addCodeFile(id, "library", "RefineTimeTransaction.C",
                createRefineTimeTransaction(pi.getTimeInterpolationInfo()));
            //Create RefineTimeTransaction.h
            cdb.addCodeFile(id, "library", "RefineTimeTransaction.h",
                createRefineTimeTransactionH(pi.getTimeInterpolationInfo()));
            //Create RefineAlgorithm.C
            cdb.addCodeFile(id, "library", "RefineAlgorithm.C",
                createRefineAlgorithm(pi.getTimeInterpolationInfo()));
            //Create RefineAlgorithm.h
            cdb.addCodeFile(id, "library", "RefineAlgorithm.h",
                createRefineAlgorithmH(pi.getTimeInterpolationInfo()));		            
            //Copy TimeInterpolateOperator.C
            cdb.addCodeFile(id, "library", "TimeInterpolateOperator.C", 
                    CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/TimeInterpolateOperator.C"), 0));
            //Create TimeInterpolateOperator.h
            cdb.addCodeFile(id, "library", "TimeInterpolateOperator.h",
                    createTimeInterpolateOperatorH(pi.getTimeInterpolationInfo()));
            //Copy TimeRefinementIntegrator.C
            cdb.addCodeFile(id, "library", "TimeRefinementIntegrator.C", 
                    CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/TimeRefinementIntegrator.C"), 0));
            //Copy TimeRefinementIntegrator.h
            cdb.addCodeFile(id, "library", "TimeRefinementIntegrator.h", 
                    CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/TimeRefinementIntegrator.h"), 0));
            //Copy StandardRefineTransactionFactory.C
            cdb.addCodeFile(id, "library", "StandardRefineTransactionFactory.C", 
                    CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/StandardRefineTransactionFactory.C"), 0));
            //Copy StandardRefineTransactionFactory.h
            cdb.addCodeFile(id, "library", "StandardRefineTransactionFactory.h", 
                    CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/StandardRefineTransactionFactory.h"), 0));
            //Copy RefineCopyTransaction.C
            cdb.addCodeFile(id, "library", "RefineCopyTransaction.C", 
                    CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/RefineCopyTransaction.C"), 0));
            //Copy RefineCopyTransaction.h
            cdb.addCodeFile(id, "library", "RefineCopyTransaction.h", 
                    CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/RefineCopyTransaction.h"), 0));
            //Copy RefineSchedule.C
            cdb.addCodeFile(id, "library", "RefineSchedule.C", 
                    CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/RefineSchedule.C"), 0));
            //Copy RefineSchedule.h
            cdb.addCodeFile(id, "library", "RefineSchedule.h", 
                    CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/RefineSchedule.h"), 0));
            if (pi.getX3dMovRegions() != null) {
                //Create Particle.C
                cdb.addCodeFile(id, "sources", "Interphase.C",
                        createInterphase(pi));
                //Create Particle.h
                cdb.addCodeFile(id, "headers", "Interphase.h",
                        createInterphaseH(pi));  
            }
            if (activeSlicers) {
                //Copy SlicerDataWriter.C
                cdb.addCodeFile(id, "library", "SlicerDataWriter.C", 
                        CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/SlicerDataWriter.C"), 0));
                //Copy SlicerDataWriter.h
                cdb.addCodeFile(id, "library", "SlicerDataWriter.h", 
                        CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/SlicerDataWriter.h"), 0));
                //Copy SphereDataWriter.C
                cdb.addCodeFile(id, "library", "SphereDataWriter.C", 
                        CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/SphereDataWriter.C"), 0));
                //Copy SphereDataWriter.h
                cdb.addCodeFile(id, "library", "SphereDataWriter.h", 
                        CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/SphereDataWriter.h"), 0));
            }
            if (pi.isHasMeshFields()) {
	            //Copy IntegrateDataWriter.C
	            cdb.addCodeFile(id, "library", "IntegrateDataWriter.C", 
	            		CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/IntegrateDataWriter.C"), 0));
	            //Copy IntegrateDataWriter.h
	            cdb.addCodeFile(id, "library", "IntegrateDataWriter.h", 
	            		CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/IntegrateDataWriter.h"), 0));
	          //Copy PointDataWriter.C
	            cdb.addCodeFile(id, "library", "PointDataWriter.C", 
	            		CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/PointDataWriter.C"), 0));
	            //Copy PointDataWriter.h
	            cdb.addCodeFile(id, "library", "PointDataWriter.h", 
	            		CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/PointDataWriter.h"), 0));
            }
            cdb.addCodeFile(id, "makefile", "Makefile",
                    createMakefile(mpich, hdf5, samrai, silo, problemName, pi));
            if (pi.hasExternalInitialCondition()) {
                //Copy ExternalInitialCondition examples
	            cdb.addCodeFile(id, "library", "ExternalInitialData_lorene_bns.cpp", 
	            		CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/ExternalInitialData_lorene_bns.cpp"), 0));
	            cdb.addCodeFile(id, "library", "ExternalInitialData_lorene_bns.h", 
	            		CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/ExternalInitialData_lorene_bns.h"), 0));
	            cdb.addCodeFile(id, "library", "ExternalInitialData_magstar.cpp", 
	            		CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/ExternalInitialData_magstar.cpp"), 0));
	            cdb.addCodeFile(id, "library", "ExternalInitialData_magstar.h", 
	            		CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/ExternalInitialData_magstar.h"), 0));
	            cdb.addCodeFile(id, "library", "ExternalInitialData_lorene_bns_piecewiseEoS.cpp", 
	            		CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/ExternalInitialData_lorene_bns_piecewiseEoS.cpp"), 0));
	            cdb.addCodeFile(id, "library", "ExternalInitialData_lorene_bns_piecewiseEoS.h", 
	            		CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/ExternalInitialData_lorene_bns_piecewiseEoS.h"), 0));
	            cdb.addCodeFile(id, "library", "ExternalInitialData_magstar_piecewiseEoS.cpp", 
	            		CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/ExternalInitialData_magstar_piecewiseEoS.cpp"), 0));
	            cdb.addCodeFile(id, "library", "ExternalInitialData_magstar_piecewiseEoS.h", 
	            		CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/ExternalInitialData_magstar_piecewiseEoS.h"), 0));
	            cdb.addCodeFile(id, "library", "ExternalInitialData_neutron_star_2D.cpp", 
	            		CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/ExternalInitialData_neutron_star_2D.cpp"), 0));
	            cdb.addCodeFile(id, "library", "ExternalInitialData_neutron_star_2D.h", 
	            		CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/ExternalInitialData_neutron_star_2D.h"), 0));
	            cdb.addCodeFile(id, "library", "ExternalInitialData_lorene_bhns_piecewiseEoS.cpp", 
	            		CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/ExternalInitialData_lorene_bhns_piecewiseEoS.cpp"), 0));
	            cdb.addCodeFile(id, "library", "ExternalInitialData_lorene_bhns_piecewiseEoS.h", 
	            		CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/ExternalInitialData_lorene_bhns_piecewiseEoS.h"), 0));
	            cdb.addCodeFile(id, "library", "ExternalInitialData_magstar_genericEoS.cpp", 
	            		CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/ExternalInitialData_magstar_genericEoS.cpp"), 0));
	            cdb.addCodeFile(id, "library", "ExternalInitialData_magstar_genericEoS.h", 
	            		CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/ExternalInitialData_magstar_genericEoS.h"), 0));
	            cdb.addCodeFile(id, "library", "ExternalInitialData_lorene_bns_genericEoS.cpp", 
	            		CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/ExternalInitialData_lorene_bns_genericEoS.cpp"), 0));
	            cdb.addCodeFile(id, "library", "ExternalInitialData_lorene_bns_genericEoS.h", 
	            		CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/ExternalInitialData_lorene_bns_genericEoS.h"), 0));
            }
            
            //README
            String file = "";
            if (pi.isHasMeshFields() || pi.isHasParticleFields()) {
                file = "/README_SAMRAI_PDE";
            }
            if (pi.isHasABMMeshlessFields()) {
                file = "/README_SAMRAI_ABM_meshless";
            }
            if (pi.isHasABMMeshFields()) {
                file = "/README_SAMRAI_ABM_lattice";
            }
            cdb.addCodeFile(id, "readme", "README",
                    CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream(file), 0));
            
            return problemName;
        } 
        catch (NumberFormatException e) {
            e.printStackTrace();
            throw new CGException(CGException.CG004, "Configuration.xml contains invalid values");
        } 
        catch (CGException e) {
            throw e;
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            throw new CGException(CGException.CG00X, e.getMessage());
        }
    }
    
    /**
     * Creates a makefile to compile manually.
     * @param mpi				The lib location
     * @param hdf5				The lib location
     * @param samrai			The lib location
     * @param silo				The lib location
     * @param exeName			Executable name
     * @param pi				The problem information
     * @return                  The code
     * @throws CGException      CG00X External error
     */
    private String createMakefile(String mpi, String hdf5, String samrai, String silo, String exeName, ProblemInfo pi) throws CGException {
    	String previousComprobations = "";
    	if (pi.hasExternalInitialCondition()) {
    		previousComprobations = "ifneq (,$(wildcard ./ExternalInitialData.cpp))" + NL
    				+ IND + "NOT_FOUND = 1" + NL
    				+ "endif" + NL
    				+ "ifneq (,$(wildcard ./ExternalInitialData.h))" + NL
    				+ IND + "NOT_FOUND = 1" + NL
    				+ "endif" + NL
    				+ "define n" + NL
    				+ "" + NL
    				+ "" + NL
    				+ "endef" + NL
    				+ "ifndef NOT_FOUND" + NL
    				+ "$(error ExternalInitialData.cpp or ExternalInitialData.h not found. $n \\"
    						+ "Any of the providen external initial data classes can be used. $n \\"
    						+ "Rename the proper files, set path to external libraries and compile again.)" + NL
    				+ "endif" + NL;
    	}
    	String includes = " -I. $(EXTERNAL_INCLUDE) -I$(MPI_HOME)" + File.separator + "include";

        if (pi.isHasABMMeshlessFields() || pi.isHasParticleFields()) {
        	includes = includes + " -I$(SILO_HOME)" + File.separator + "include";
        }
        includes = includes + " -I$(HDF5_HOME)" + File.separator + "include -I$(SAMRAI_HOME)" + File.separator + "include";
        String fileDeps = "Commons.h";
        String fileObjs = "Commons.o";
        String fileComps = "";
        String problemDeps = "";
        String functionDeps = "";
        String samraiDeps = "";
       	if (pi.hasExternalInitialCondition()) {
       		includes = includes + " -I$(LORENE_HOME)/C++/Include -I$(LORENE_HOME)/Export/C++/Include";
       		fileDeps = fileDeps + " ExternalInitialData.h";
       		fileObjs = fileObjs + " ExternalInitialData.o";
       		fileComps = fileComps + "ExternalInitialData.o: ExternalInitialData.cpp ExternalInitialData.h" + NL
       				+ IND + "$(CXX) $(PERFFLAGS) -c -o $@ $< $(CFLAGS)" + NL + NL;
       		problemDeps = problemDeps + " ExternalInitialData.h";
       	}
        //Code files
        if (pi.isHasABMMeshFields() || pi.isHasMeshFields() || pi.isHasParticleFields()) {
       		fileDeps = fileDeps + " TimeInterpolator.h LagrangianPolynomicRefine.h";
       		fileObjs = fileObjs + " TimeInterpolator.o LagrangianPolynomicRefine.o";
       		fileComps = fileComps + "TimeInterpolator.o: TimeInterpolator.C TimeInterpolator.h TimeInterpolateOperator.h Commons.h" + NL
       				+ IND + "$(CXX) $(PERFFLAGS) -c -o $@ $< $(CFLAGS)" + NL + NL
       				+ "LagrangianPolynomicRefine.o: LagrangianPolynomicRefine.C LagrangianPolynomicRefine.h RefineTimeTransaction.h" + NL
       				+ IND + "$(CXX) $(PERFFLAGS) -c -o $@ $< $(CFLAGS)" + NL + NL;
       		problemDeps = problemDeps + " TimeInterpolator.h LagrangianPolynomicRefine.h";
        }
        if (pi.isHasMeshFields()) {
       		fileDeps = fileDeps + " IntegrateDataWriter.h PointDataWriter.h";
       		fileObjs = fileObjs + " IntegrateDataWriter.o PointDataWriter.o";
       		fileComps = fileComps + "IntegrateDataWriter.o: IntegrateDataWriter.C IntegrateDataWriter.h" + NL
       				+ IND + "$(CXX) $(PERFFLAGS) -c -o $@ $< $(CFLAGS)" + NL + NL
       				+ "PointDataWriter.o: PointDataWriter.C PointDataWriter.h" + NL
       				+ IND + "$(CXX) $(PERFFLAGS) -c -o $@ $< $(CFLAGS)" + NL + NL;
       		problemDeps = problemDeps + " IntegrateDataWriter.h PointDataWriter.h";
        }
        
   		fileDeps = fileDeps + " RefineSchedule.h RefineAlgorithm.h StandardRefineTransactionFactory.h RefineClasses.h"
        		+ " RefineCopyTransaction.h RefineTimeTransaction.h TimeRefinementIntegrator.h";
   		fileObjs = fileObjs + " RefineSchedule.o RefineAlgorithm.o StandardRefineTransactionFactory.o RefineClasses.o"
        		+ " RefineCopyTransaction.o RefineTimeTransaction.o TimeRefinementIntegrator.o";
   		fileComps = fileComps + "RefineSchedule.o: RefineSchedule.C RefineSchedule.h RefineClasses.h TimeInterpolateOperator.h RefineTimeTransaction.h RefineCopyTransaction.h" + NL
   				+ IND + "$(CXX) $(PERFFLAGS) -c -o $@ $< $(CFLAGS)" + NL + NL
   				+ "RefineAlgorithm.o: RefineAlgorithm.C RefineAlgorithm.h TimeInterpolateOperator.h RefineClasses.h" + NL
   				+ IND + "$(CXX) $(PERFFLAGS) -c -o $@ $< $(CFLAGS)" + NL + NL
   				+ "StandardRefineTransactionFactory.o: StandardRefineTransactionFactory.C StandardRefineTransactionFactory.h RefineClasses.h" + NL
   				+ IND + "$(CXX) $(PERFFLAGS) -c -o $@ $< $(CFLAGS)" + NL + NL
   				+ "RefineClasses.o: RefineClasses.C RefineClasses.h TimeInterpolateOperator.h" + NL
   				+ IND + "$(CXX) $(PERFFLAGS) -c -o $@ $< $(CFLAGS)" + NL + NL
   				+ "RefineCopyTransaction.o: RefineCopyTransaction.C RefineCopyTransaction.h RefineClasses.h" + NL
   				+ IND + "$(CXX) $(PERFFLAGS) -c -o $@ $< $(CFLAGS)" + NL + NL
   				+ "RefineTimeTransaction.o: RefineTimeTransaction.C RefineTimeTransaction.h RefineClasses.h TimeInterpolateOperator.h" + NL
   				+ IND + "$(CXX) $(PERFFLAGS) -c -o $@ $< $(CFLAGS)" + NL + NL
   				+ "TimeRefinementIntegrator.o: TimeRefinementIntegrator.C TimeRefinementIntegrator.h" + NL
   				+ IND + "$(CXX) $(PERFFLAGS) -c -o $@ $< $(CFLAGS)" + NL + NL
   				;

        /*if (pi.getX3dMovRegions() != null) {
            commands.add("Interphase.C");
        }*/
   	  if (pi.isHasABMMeshlessFields() || pi.isHasParticleFields()) {
       		fileDeps = fileDeps + " NonSync.h NonSyncs.h Particles.h ParticleRefine.h";
       		fileObjs = fileObjs + " NonSync.o NonSyncs.o Particles.o ParticleRefine.o";

       		problemDeps = problemDeps + " ParticleDataWriter.h ParticleRefine.h NonSync.h NonSyncs.h Particles.h";
       		functionDeps = functionDeps + " Particles.h Problem.h";
       		samraiDeps = samraiDeps + " ParticleDataWriter.h ParticleRefine.h";
       		String particleDeps = "";
       		for (String speciesName: pi.getParticleSpecies()) {
           		fileDeps = fileDeps + " Particle_" + speciesName + ".h";
           		fileObjs = fileObjs + " Particle_" + speciesName + ".o";
          		fileComps = fileComps + "Particle_" + speciesName + ".o: Particle_" + speciesName + ".C Particle_" + speciesName + ".h" + NL
           				+ IND + "$(CXX) $(PERFFLAGS) -c -o $@ $< $(CFLAGS)" + NL + NL;
          		particleDeps = particleDeps + " Particle_" + speciesName + ".h";
       		}
       		problemDeps = problemDeps + " ParticleDataWriter.h ParticleRefine.h NonSync.h NonSyncs.h Particles.h" + particleDeps;
       		fileComps = fileComps + "NonSync.o: NonSync.C NonSync.h" + NL
       				+ IND + "$(CXX) $(PERFFLAGS) -c -o $@ $< $(CFLAGS)" + NL + NL
       				+ "ParticleDataWriter.o: ParticleDataWriter.C ParticleDataWriter.h Particles.h" + particleDeps + NL
       				+ IND + "$(CXX) $(PERFFLAGS) -c -o $@ $< $(CFLAGS)" + NL + NL
       				+ "NonSyncs.o: NonSyncs.C NonSyncs.h NonSync.h" + NL
       				+ IND + "$(CXX) $(PERFFLAGS) -c -o $@ $< $(CFLAGS)" + NL + NL
       				+ "Particles.o: Particles.C Particles.h" + particleDeps + NL
       				+ IND + "$(CXX) $(PERFFLAGS) -c -o $@ $< $(CFLAGS)" + NL + NL
       				+ "ParticleRefine.o: ParticleRefine.C ParticleRefine.h RefineTimeTransaction.h Particles.h" + particleDeps + NL
       				+ IND + "$(CXX) $(PERFFLAGS) -c -o $@ $< $(CFLAGS)" + NL + NL;

        }
        if (activeSlicers) {
       		fileDeps = fileDeps + " SlicerDataWriter.h SphereDataWriter.h";
       		fileObjs = fileObjs + " SlicerDataWriter.o SphereDataWriter.o";
       		fileComps = fileComps + "SlicerDataWriter.o: SlicerDataWriter.C SlicerDataWriter.h" + NL
       				+ IND + "$(CXX) $(PERFFLAGS) -c -o $@ $< $(CFLAGS)" + NL + NL
       				+ "SphereDataWriter.o: SphereDataWriter.C SphereDataWriter.h" + NL
       				+ IND + "$(CXX) $(PERFFLAGS) -c -o $@ $< $(CFLAGS)" + NL + NL;
       		problemDeps = problemDeps + " SlicerDataWriter.h SphereDataWriter.h";
        }
   		fileDeps = fileDeps + " Problem.h Functions.h MainRestartData.h";
   		fileObjs = fileObjs + " SAMRAIConnector.o Problem.o Functions.o MainRestartData.o";
	 	if (pi.isHasABMMeshlessFields() || pi.isHasParticleFields()) {
	 		fileDeps = fileDeps + " ParticleDataWriter.h";
       		fileObjs = fileObjs + " ParticleDataWriter.o";
	 	}
   		fileComps = fileComps + "SAMRAIConnector.o: SAMRAIConnector.cpp Problem.h MainRestartData.h TimeRefinementIntegrator.h" + samraiDeps + NL
   				+ IND + "$(CXX) $(PERFFLAGS) -c -o $@ $< $(CFLAGS)" + NL + NL
   				+ "Problem.o: Problem.cpp Problem.h Functions.h RefineClasses.h Commons.h TimeInterpolateOperator.h TimeRefinementIntegrator.h RefineSchedule.h RefineAlgorithm.h StandardRefineTransactionFactory.h RefineTimeTransaction.h MainRestartData.h" + problemDeps + NL
   				+ IND + "$(CXX) $(PERFFLAGS) -c -o $@ $< $(CFLAGS)" + NL + NL
   				+ "Functions.o: Functions.cpp Functions.h Commons.h " + functionDeps + NL
   				+ IND + "$(CXX) $(PERFFLAGS) -c -o $@ $< $(CFLAGS)" + NL + NL
   				+ "Commons.o: Commons.cpp Commons.h"+ NL
   				+ IND + "$(CXX) $(PERFFLAGS) -c -o $@ $< $(CFLAGS)" + NL + NL
   				+ "MainRestartData.o: MainRestartData.cpp MainRestartData.h" + NL
   				+ IND + "$(CXX) $(PERFFLAGS) -c -o $@ $< $(CFLAGS)" + NL + NL;
        
        //Libraries
        String libraries = "$(EXTERNAL_LIB)";
       	String siloHome = "";
       	String externalICHome = "";
       	if (pi.hasExternalInitialCondition()) {
       		externalICHome = "LORENE_HOME=/path/to/Lorene" + NL;
       		libraries = libraries + " -L$(LORENE_HOME)/Lib";
       	}
        if (pi.isHasABMMeshlessFields() || pi.isHasParticleFields()) {
        	siloHome = "SILO_HOME=" + silo + NL;
        	libraries = libraries + " -L$(SILO_HOME)" + File.separator + "lib";
        }
        libraries = libraries + " -L$(MPI_HOME)" + File.separator + "lib -L$(HDF5_HOME)" + File.separator + "lib"
      			+ " -L$(SAMRAI_HOME)" + File.separator + "lib"
      			+ " -lSAMRAI_algs -lSAMRAI_appu -lSAMRAI_geom -lSAMRAI_hier -lSAMRAI_math -lSAMRAI_mesh -lSAMRAI_pdat"
      			+ " -lSAMRAI_solv -lSAMRAI_tbox -lSAMRAI_xfer";
        if (pi.isHasABMMeshlessFields() || pi.isHasParticleFields()) {
        	libraries = libraries + " -lsiloh5";
        }
        libraries = libraries + " -lmpi -lmpi_cxx ";
       	if (pi.hasExternalInitialCondition()) {
       		libraries = libraries + " -llorene_export -llorene -llorenef77";
       	}
       	libraries = libraries + " -ldl -lpthread -lutil -lhdf5 -lgsl -lgslcblas";
    	if (pi.hasExternalInitialCondition()) {
    		libraries = libraries + " -llapack -lblas -lX11 -lfftw3 -lstdc++";
       	}
    	libraries = libraries + " -lm";
    	 
    	return previousComprobations
    			+ "SHELL = /bin/sh" + NL
    			+ "HDF5_HOME=" + hdf5 + NL
    			+ "SAMRAI_HOME=" + samrai + NL
    			+ "MPI_HOME=" + mpi + NL
    			+ siloHome
    			+ externalICHome
    			+ "#Uncomment next line for external ReprimAnd EOS (https://zenodo.org/record/3785075  Requires installation)" + NL
    			+ "#EXTERNAL_EOS_HOME=/paht/of/RePrimAnd" + NL
    			+ "ifeq ($(CXX),g++)" + NL
    			+ "  PERFFLAGS=-fno-math-errno -fno-signaling-nans -fomit-frame-pointer -O3" + NL
    			+ "endif" + NL
    			+ "ifeq ($(CXX),gcc)" + NL
    			+ "  PERFFLAGS=-fno-math-errno -fno-signaling-nans -fomit-frame-pointer -O3" + NL
    			+ "endif" + NL
    			+ "ifeq ($(CXX),icc)" + NL
    			+ "  PERFFLAGS=-O3 -xHost -ipo -fno-math-errno -fno-signaling-nans -fp-model precise -fp-model source" + NL
    			+ "endif" + NL
    			+ "ifeq ($(CXX),icpc)" + NL
    			+ "  PERFFLAGS=-O3 -xHost -ipo -fno-math-errno -fno-signaling-nans -fp-model precise -fp-model source" + NL
    			+ "endif" + NL
    			+ "ifdef EXTERNAL_EOS_HOME" + NL
    			+ "  PERFFLAGS += -DEXTERNAL_EOS" + NL
    			+ "  EXTERNAL_INCLUDE += -I$(EXTERNAL_EOS_HOME)/include" + NL
    			+ "  EXTERNAL_LIB += -L$(EXTERNAL_EOS_HOME)/lib/x86_64-linux-gnu -lRePrimAnd" + NL
    			+ "endif" + NL + NL
    			+ "CFLAGS= -std=c++14" + includes + NL
    			+ "LIBS= " + libraries + NL + NL
    			+ "DEPS =" + fileDeps + NL + NL
    			+ "OBJ =" + fileObjs + NL + NL
    			+ exeName + ": $(OBJ)" + NL
    			+ IND + "$(CXX) $(PERFFLAGS) -o $@ $^ $(CFLAGS) $(LIBS)" + NL + NL
    			+ fileComps
    			+ "clean:" + NL  
    			+ IND + "/bin/rm *.o " + exeName + NL;
    }
    
   /**
     * Creates the SAMRAIConnector.cpp file from the template.
     * @param pi		        The problem information
     * @return                  The code
     * @throws CGException      CG00X External error
     */
    private String createSAMRAIConnector(ProblemInfo pi) throws CGException {
        try {
            boolean analysis = pi.getVariableInfo().getOutputVarsAnalysis().size() > 0;
            int extraVars = 50;
            int dimensions = pi.getDimensions();
            //Get the template
            String connector = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/SAMRAIConnector.cpp"), 0);
            
            //Dimension substitution
            connector = connector.replaceAll("#DIM#", String.valueOf(dimensions));
            
            //Datawriter
            String slicerInclude = "";
            if (activeSlicers) {
                slicerInclude = "#include \"SlicerDataWriter.h\"" + NL
                        + "#include \"SphereDataWriter.h\"";
            }
            connector = connector.replace("#slicerInclude#", slicerInclude);
            String dataWriterInclude = "";
            String particleAverageDeclaration = "";
            String dataWriterParameters = "";
            String dataWriterDeclaration = "";
            String initialPlot = "";
            String particleAverage = "";
            String forceOnePatch = "";
            String plotterSetup = "";
            if (pi.isHasABMMeshFields() || pi.isHasMeshFields()) {
            	dataWriterParameters = ", viz_mesh_dump_interval, full_mesh_writer_variables, visit_data_writer";
            	dataWriterInclude = "#include \"SAMRAI/appu/VisItDataWriter.h\"" + NL;
            	dataWriterDeclaration = IND + "std::shared_ptr<appu::VisItDataWriter > visit_data_writer(new appu::VisItDataWriter(dim,\"Mesh VisIt Writer\", viz_mesh_dump_dirname));" + NL;
                initialPlot = IND + "if (viz_mesh_dump_interval > 0 && !tbox::RestartManager::getManager()->isFromRestart()) {" + NL 
                		+ IND + IND + "visit_data_writer->writePlotData( patch_hierarchy, iteration_num, loop_time);" + NL
                		+ IND + "}" + NL;
                plotterSetup = IND + "problem->setupPlotterMesh(*visit_data_writer);" + NL;
            }
            if (pi.isHasABMMeshlessFields() || pi.isHasParticleFields()) {
            	dataWriterParameters = dataWriterParameters + ", viz_particle_dump_interval, full_particle_writer_variables, particle_data_writer";
            	dataWriterInclude = dataWriterInclude + "#include \"ParticleDataWriter.h\"" + NL;
                particleAverageDeclaration = "bool particleAverage = false;";
                dataWriterDeclaration = dataWriterDeclaration + IND + "std::shared_ptr<ParticleDataWriter > particle_data_writer(new ParticleDataWriter(dim,\"Particle Writer\", viz_particle_dump_dirname, particleAverage));" + NL;
                initialPlot = initialPlot + IND + "if (viz_particle_dump_interval > 0 && !tbox::RestartManager::getManager()->isFromRestart()) {" + NL
                		+ IND + IND + "particle_data_writer->writePlotData(grid_geometry, patch_hierarchy, iteration_num, loop_time);" + NL
                		+ IND + "}" + NL;
                particleAverage = IND + "if (input_db->getDatabase(\"Problem\")->getDatabase(\"particles\")"
                    + "->keyExists(\"print_average\")) {" + NL
                    + IND + IND + "particleAverage = input_db->getDatabase(\"Problem\")->getDatabase(\"particles\")->getBool(\"print_average\");" + NL
                    + IND + "}";
                if (pi.isHasABMMeshlessFields()) {
                    String patchSize = "";
                    for (int i = 0; i < dimensions; i++) {
                        patchSize = patchSize + IND + "patchSize[" + i + "] = domain.front().upper(" + i + ") - domain.front().lower(" + i + ") + " 
                            + (1 + 2 * pi.getSchemaStencil()) + ";" + NL;
                    }
                	forceOnePatch = IND + "//Force one patch per processor" + NL
	                    + IND + "hier::BoxContainer domain(input_db->getDatabase(\"CartesianGeometry\")" 
	                    + "->getDatabaseBoxVector(\"domain_boxes\"));" + NL
	                    + IND + "input_db->putDatabase(\"PatchHierarchy\");" + NL
	                    + IND + "std::shared_ptr<tbox::Database> grid_db(input_db->getDatabase(\"GriddingAlgorithm\"));" + NL
	                    + IND + "grid_db->putDatabase(\"largest_patch_size\");" + NL
	                    + IND + "std::shared_ptr<tbox::Database> largest_patch_db(grid_db->getDatabase(\"largest_patch_size\"));" + NL
	                    + IND + "int patchSize[" + dimensions + "];" + NL
	                    + patchSize
	                    + IND + "largest_patch_db->putIntegerArray(\"level_0\",patchSize, " + dimensions + ");" + NL
	                    + IND + "grid_db->putInteger(\"max_levels\", 1);" + NL
	                    + IND + "grid_db->putDatabase(\"ratio_to_coarser\");" + NL;
                }
                plotterSetup = plotterSetup + IND + "problem->setupPlotterParticles(*particle_data_writer);" + NL;
            }
            
            connector = connector.replace("#DataWriterInclude#", dataWriterInclude);
            connector = connector.replace("#ParticleAverageDeclaration#", particleAverageDeclaration);
            connector = connector.replaceAll("#DataWriterParameters#", dataWriterParameters);
            connector = connector.replaceAll("#InitialPlot#", initialPlot);
            connector = connector.replace("#ParticleAverage#", particleAverage);
            connector = connector.replace("#force one patch#", forceOnePatch);
            connector = connector.replaceAll("#DataWriterDeclaration#", dataWriterDeclaration);


            //Slicer data writer
            String slicerDeclarations = "";
            String slicerInputParameters = "";
            String slicerParameter = "";
            String slicerFirstOutput = "";
            String slicerPeriodicalWrite = "";
            String analysisIntegrationConfiguration = "";
            String analysisPointConfiguration = "";
            if (analysis) {
            	analysisIntegrationConfiguration = 
            			  IND + IND + IND + "integralAnalysis.push_back(integral_db->getBool(\"activate_analysis\"));" + NL;
            	analysisPointConfiguration = 
          			  IND + IND + IND + "pointAnalysis.push_back(point_db->getBool(\"activate_analysis\"));" + NL;
            }
            if (activeSlicers) {
                String analysisSlicerConfiguration = "";
                String analysisSphereConfiguration = "";
                slicerDeclarations = "vector<std::shared_ptr<SlicerDataWriter > > sliceWriters;" + NL
                        + "vector<int> sliceIntervals;" + NL
                        + "vector<set<string> > sliceVariables;" + NL
                        + "vector<std::shared_ptr<SphereDataWriter > > sphereWriters;" + NL
                        + "vector<int> sphereIntervals;" + NL
                        + "vector<set<string> > sphereVariables;" + NL;
                if (analysis) {
                    analysisSlicerConfiguration = 
                            IND + IND + IND + "sliceAnalysis.push_back(slicer_db->getBool(\"activate_analysis\"));" + NL;
                    analysisSphereConfiguration = 
                            IND + IND + IND + "sphereAnalysis.push_back(sphere_db->getBool(\"activate_analysis\"));" + NL;
                    slicerDeclarations = slicerDeclarations + "vector<bool> sliceAnalysis;" + NL
                    		 + "vector<bool> sphereAnalysis;" + NL;
                }
                slicerInputParameters = IND + "//Slices" + NL
                        + IND + "for (int i = 0; i < n_at_commands; ++i) {" + NL
                        + IND + IND + "std::string at_name = \"slice_\" + tbox::Utilities::intToString(i);" + NL
                        + IND + IND + "if (writer_db->keyExists(at_name)) {" + NL
                        + IND + IND + IND + "std::shared_ptr<tbox::Database> slicer_db(writer_db->getDatabase(at_name));" + NL
                        + IND + IND + IND + "string slicer_dirname;" + NL
                        + IND + IND + IND + "if (slicer_db->keyExists(\"hdf5_dump_dirname\")) {" + NL
                        + IND + IND + IND + IND + "slicer_dirname = slicer_db->getStringWithDefault(\"hdf5_dump_dirname\", \".\");" + NL
                        + IND + IND + IND + "} else {" + NL
                        + IND + IND + IND + IND + "std::cerr << \"Error in parameter file: "
                                + "Slice output must have parameter 'hdf5_dump_dirname'.\" << endl;" + NL
                        + IND + IND + IND + IND + "return -1;" + NL
                        + IND + IND + IND + "}" + NL
                        + IND + IND + IND + "int plane_normal_axis;" + NL
                        + IND + IND + IND + "if (slicer_db->keyExists(\"plane_normal_axis\")) {" + NL
                        + IND + IND + IND + IND + "plane_normal_axis = slicer_db->getInteger(\"plane_normal_axis\");" + NL
                        + IND + IND + IND + "} else {" + NL
                        + IND + IND + IND + IND + "std::cerr << \"Error in parameter file: "
                                + "Slice output must have parameter 'plane_normal_axis'.\" << endl;" + NL
                        + IND + IND + IND + IND + "return -1;" + NL
                        + IND + IND + IND + "}" + NL
                        + IND + IND + IND + "double distance_to_origin;" + NL
                        + IND + IND + IND + "if (slicer_db->keyExists(\"distance_to_origin\")) {" + NL
                        + IND + IND + IND + IND + "distance_to_origin = slicer_db->getFloat(\"distance_to_origin\");" + NL
                        + IND + IND + IND + "} else {" + NL
                        + IND + IND + IND + IND + "std::cerr << \"Error in parameter file: "
                                + "Slice output must have parameter 'distance_to_origin'.\" << endl;" + NL
                        + IND + IND + IND + IND + "return -1;" + NL
                        + IND + IND + IND + "}" + NL
                        + IND + IND + IND + "std::shared_ptr<SlicerDataWriter> slicer(new SlicerDataWriter(at_name, slicer_dirname, "
                                + "plane_normal_axis, distance_to_origin, visit_number_procs_per_file));" + NL
                        + IND + IND + IND + "sliceWriters.push_back(slicer);" + NL
                        + IND + IND + IND + "int slicerInterval = 0;" + NL
                        + IND + IND + IND + "if (slicer_db->keyExists(\"hdf5_dump_interval\")) {" + NL
                        + IND + IND + IND + IND + "slicerInterval = slicer_db->getInteger(\"hdf5_dump_interval\");" + NL
                        + IND + IND + IND + IND + "sliceIntervals.push_back(slicerInterval);" + NL
                        + IND + IND + IND + "} else {" + NL
                        + IND + IND + IND + IND + "std::cerr << \"Error in parameter file: "
                                + "Slice output must have parameter 'hdf5_dump_interval'.\" << endl;" + NL
                        + IND + IND + IND + IND + "return -1;" + NL
                        + IND + IND + IND + "}" + NL
                        + IND + IND + IND + "if (slicer_db->keyExists(\"variables\")) {" + NL
                        + IND + IND + IND + IND + "vector<string> tmp = slicer_db->getStringVector(\"variables\");" + NL
                        + IND + IND + IND + IND + "sliceVariables.push_back(set<string>(tmp.begin(), tmp.end()));" + NL
                        + IND + IND + IND + "} else {" +  NL
                        + IND + IND + IND + IND + "std::cerr << \"Error in parameter file: "
                                + "Slice output must have parameter 'variables'.\" << endl;" + NL
                        + IND + IND + IND + IND + "return -1;" + NL
                        + IND + IND + IND + "}" + NL
                        + IND + IND + IND + "if (slicerInterval > 0) {" + NL
                        + IND + IND + IND + IND + "output_information << \"    Slicer output  \" << endl;" + NL
                        + IND + IND + IND + IND + "output_information << \"    Output directory:  \" << slicer_dirname << endl;" + NL
                        + IND + IND + IND + IND + "output_information << \"    Snapshot interval: \" << slicerInterval << endl;" + NL
                        + IND + IND + IND + IND + "output_information << \"    Plane normal axis: \" << plane_normal_axis << endl;" + NL
                        + IND + IND + IND + IND + "output_information << \"    Distance to origin: \" << distance_to_origin << endl;" + NL
                        + IND + IND + IND + IND + "output_information << \"|--------------------------------------------------------|\" << endl;" + NL
            			+ IND + IND + IND + "}" + NL
                        + analysisSlicerConfiguration
                        + IND + IND + "} else {" + NL
                        + IND + IND + IND + "//End loop when no more slice_x found" + NL
                        + IND + IND + IND + "break;" + NL
                        + IND + IND + "}" + NL
                        + IND + "}" + NL
                        + IND + "//Spheres" + NL
                        + IND + "for (int i = 0; i < n_at_commands; ++i) {" + NL
                        + IND + IND + "std::string at_name = \"sphere_\" + tbox::Utilities::intToString(i);" + NL
                        + IND + IND + "if (writer_db->keyExists(at_name)) {" + NL
                        + IND + IND + IND + "std::shared_ptr<tbox::Database> sphere_db(writer_db->getDatabase(at_name));" + NL
                        + IND + IND + IND + "string sphere_dirname;" + NL
                        + IND + IND + IND + "if (sphere_db->keyExists(\"hdf5_dump_dirname\")) {" + NL
                        + IND + IND + IND + IND + "sphere_dirname = sphere_db->getStringWithDefault(\"hdf5_dump_dirname\", \".\");" + NL
                        + IND + IND + IND + "} else {" + NL
                        + IND + IND + IND + IND + "std::cerr << \"Error in parameter file: "
                                + "Spherical output must have parameter 'hdf5_dump_dirname'.\" << endl;" + NL
                        + IND + IND + IND + IND + "return -1;" + NL
                        + IND + IND + IND + "}" + NL
                        + IND + IND + IND + "double radius;" + NL
                        + IND + IND + IND + "if (sphere_db->keyExists(\"radius\")) {" + NL
                        + IND + IND + IND + IND + "radius = sphere_db->getDouble(\"radius\");" + NL
                        + IND + IND + IND + "} else {" + NL
                        + IND + IND + IND + IND + "std::cerr << \"Error in parameter file: "
                                + "Spherical output must have parameter 'radius'.\" << endl;" + NL
                        + IND + IND + IND + IND + "return -1;" + NL
                        + IND + IND + IND + "}" + NL
                        + IND + IND + IND + "if (!sphere_db->keyExists(\"center\")) {" + NL
                        + IND + IND + IND + IND + "std::cerr << \"Error in parameter file: "
                                + "Spherical output must have parameter 'center'.\" << endl;" + NL
                        + IND + IND + IND + IND + "return -1;" + NL
                        + IND + IND + IND + "}" + NL
                        + IND + IND + IND + "std::vector<double> center(sphere_db->getDoubleVector(\"center\"));" + NL
                        + IND + IND + IND + "if(center.size() != 3) {" + NL
                        + IND + IND + IND + IND + "std::cerr << \"Error in parameter file: Sphere center should be composed by three values."
                                + " (e.g. 0.0, 2.5, 4.7)\" << endl;" + NL
                        + IND + IND + IND + IND + "return -1;" + NL
                        + IND + IND + IND + "}" + NL
                        + IND + IND + IND + "if (!sphere_db->keyExists(\"resolution\")) {" + NL
                        + IND + IND + IND + IND + "std::cerr << \"Error in parameter file: "
                                + "Spherical output must have parameter 'resolution'.\" << endl;" + NL
                        + IND + IND + IND + IND + "return -1;" + NL
                        + IND + IND + IND + "}" + NL
                        + IND + IND + IND + "std::vector<int> resolution(sphere_db->getIntegerVector(\"resolution\"));" + NL
                        + IND + IND + IND + "if(resolution.size() != 2) {" + NL
                        + IND + IND + IND + IND + "std::cerr << \"Error in parameter file: Spherical resolution should be composed by two values."
                                + " (e.g. 30, 40)\" << endl;" + NL
                        + IND + IND + IND + IND + "return -1;" + NL
                        + IND + IND + IND + "}" + NL
                        + IND + IND + IND + "std::shared_ptr<SphereDataWriter> sphere(new SphereDataWriter(at_name, sphere_dirname, "
                                + "radius, center, resolution));" + NL
                        + IND + IND + IND + "sphereWriters.push_back(sphere);" + NL
                        + IND + IND + IND + "int sphereInterval = 0;" + NL
                        + IND + IND + IND + "if (sphere_db->keyExists(\"hdf5_dump_interval\")) {" + NL
                        + IND + IND + IND + IND + "sphereInterval = sphere_db->getInteger(\"hdf5_dump_interval\");" + NL
                        + IND + IND + IND + IND + "sphereIntervals.push_back(sphereInterval);" + NL
                        + IND + IND + IND + "} else {" + NL
                        + IND + IND + IND + IND + "std::cerr << \"Error in parameter file: "
                                + "Spherical output must have parameter 'hdf5_dump_interval'.\" << endl;" + NL
                        + IND + IND + IND + IND + "return -1;" + NL
                        + IND + IND + IND + "}" + NL
                        + IND + IND + IND + "if (sphere_db->keyExists(\"variables\")) {" + NL
                        + IND + IND + IND + IND + "vector<string> tmp = sphere_db->getStringVector(\"variables\");" + NL
                        + IND + IND + IND + IND + "sphereVariables.push_back(set<string>(tmp.begin(), tmp.end()));" + NL
                        + IND + IND + IND + "} else {" +  NL
                        + IND + IND + IND + IND + "std::cerr << \"Error in parameter file: "
                                + "Spherical output must have parameter 'variables'.\" << endl;" + NL
                        + IND + IND + IND + IND + "return -1;" + NL
                        + IND + IND + IND + "}" + NL
                        + IND + IND + IND + "if (sphereInterval > 0) {" + NL
                        + IND + IND + IND + IND + "std::string result;" + NL
                        + IND + IND + IND + IND + "for (std::vector<double>::iterator it = center.begin(); it != center.end(); ++it) {" + NL
                        + IND + IND + IND + IND + IND + "std::ostringstream strs;" + NL
                        + IND + IND + IND + IND + IND + "strs << (*it);" + NL
                        + IND + IND + IND + IND + IND + "result += strs.str() + \" \";" + NL
                        + IND + IND + IND + IND + "}" + NL
                        + IND + IND + IND + IND + "output_information << \"    Spherical output  \" << endl;" + NL
                        + IND + IND + IND + IND + "output_information << \"    Output directory:  \" << sphere_dirname << endl;" + NL
                        + IND + IND + IND + IND + "output_information << \"    Snapshot interval: \" << sphereInterval << endl;" + NL
                        + IND + IND + IND + IND + "output_information << \"    Center: \" << result << endl;" + NL
                        + IND + IND + IND + IND + "output_information << \"    Radius: \" << radius << endl;" + NL
                        + IND + IND + IND + IND + "output_information << \"    Resolution: \" << resolution[0] << \" \" << resolution[1] << endl;" + NL
                        + IND + IND + IND + IND + "output_information << \"|--------------------------------------------------------|\" << endl;" + NL
            			+ IND + IND + IND + "}" + NL
                        + analysisSphereConfiguration
                        + IND + IND + "} else {" + NL
                        + IND + IND + IND + "//End loop when no more sphere_x found" + NL
                        + IND + IND + IND + "break;" + NL
                        + IND + IND + "}" + NL
                        + IND + "}";
                slicerParameter = ", sliceIntervals, sliceVariables, sliceWriters, sphereIntervals, sphereVariables, sphereWriters";
                if (analysis) {
                	slicerParameter = slicerParameter + ", sliceAnalysis, sphereAnalysis";
                }
                slicerFirstOutput = IND + "//Slice output" + NL
                        + IND + "if (sliceIntervals.size() > 0 && !tbox::RestartManager::getManager()->isFromRestart()) {" + NL
                        + IND + IND + "int i = 0;" + NL
                        + IND + IND + "for (std::vector<std::shared_ptr<SlicerDataWriter> >::iterator it = sliceWriters.begin(); "
                                + "it != sliceWriters.end(); ++it) {" + NL
                        + IND + IND + IND + "if (sliceIntervals[i] > 0) {" + NL
                        + IND + IND + IND + IND + "(*it)->writePlotData(patch_hierarchy, iteration_num, loop_time);" + NL 
                        + IND + IND + IND + "}" + NL
                        + IND + IND + IND + "i++;" + NL
                        + IND + IND + "}" + NL
                        + IND + "}" + NL
                        + IND + "//Spherical output" + NL
                        + IND + "if (sphereIntervals.size() > 0 && !tbox::RestartManager::getManager()->isFromRestart()) {" + NL
                        + IND + IND + "int i = 0;" + NL
                        + IND + IND + "for (std::vector<std::shared_ptr<SphereDataWriter> >::iterator it = sphereWriters.begin(); "
                                + "it != sphereWriters.end(); ++it) {" + NL
                        + IND + IND + IND + "if (sphereIntervals[i] > 0) {" + NL
                        + IND + IND + IND + IND + "(*it)->writePlotData(patch_hierarchy, iteration_num, loop_time);" + NL 
                        + IND + IND + IND + "}" + NL
                        + IND + IND + IND + "i++;" + NL
                        + IND + IND + "}" + NL
                        + IND + "}";
                plotterSetup = plotterSetup + IND + "problem->setupSlicePlotter(sliceWriters);" + NL
                        + IND + "problem->setupSpherePlotter(sphereWriters);" + NL;
            }
            String integrationDeclarations = "";
            String integrationInputParameters = "";
            String integrationParameters = "";
            String integrationFirstOutput = "";
            String integrationPeriodicalWrite = "";
            String pointDeclarations = "";
            String pointInputParameters = "";
            String pointParameters = "";
            String pointFirstOutput = "";
            String pointPeriodicalWrite = "";
            if (pi.isHasMeshFields()) {
            	integrationDeclarations = "vector<std::shared_ptr<IntegrateDataWriter > > integrateDataWriters;" + NL
            			+ "vector<int> integralIntervals;" + NL
            			+ "vector<set<string> > integralVariables;" + NL;
            	pointDeclarations = "vector<std::shared_ptr<PointDataWriter > > pointDataWriters;" + NL
            			+ "vector<int> pointIntervals;" + NL
            			+ "vector<set<string> > pointVariables;" + NL;
            	if (analysis) {
            		integrationDeclarations = integrationDeclarations + "vector<bool> integralAnalysis;" + NL;
            		pointDeclarations = pointDeclarations + "vector<bool> pointAnalysis;" + NL;
            	}
            	integrationInputParameters = IND + "//Integrals" + NL
                        + IND + "n_at_commands = static_cast<int>(writer_db->getAllKeys().size());" + NL
            			+ IND + "for (int i = 0; i < n_at_commands; ++i) {" + NL
            			+ IND + IND + "std::string at_name = \"integration_\" + tbox::Utilities::intToString(i);" + NL
            			+ IND + IND + "if (writer_db->keyExists(at_name)) {" + NL
            			+ IND + IND + IND + "std::shared_ptr<tbox::Database> integral_db(writer_db->getDatabase(at_name));" + NL
            			+ IND + IND + IND + "string integral_dirname;" + NL
            			+ IND + IND + IND + "if (integral_db->keyExists(\"ascii_dump_dirname\")) {" + NL
            			+ IND + IND + IND + IND + "integral_dirname = integral_db->getStringWithDefault(\"ascii_dump_dirname\", \".\");" + NL
            			+ IND + IND + IND + "} else {" + NL
            			+ IND + IND + IND + IND + "std::cerr << \"Error in parameter file: Integration output must have parameter 'ascii_dump_dirname'.\" << endl;" + NL
            			+ IND + IND + IND + IND + "return -1;" + NL
            			+ IND + IND + IND + "}" + NL
            			+ IND + IND + IND + "std::vector<std::string> calculation(integral_db->getStringVector(\"calculation\"));" + NL + NL
            			+ IND + IND + IND + "std::shared_ptr<IntegrateDataWriter> integral(new IntegrateDataWriter(calculation, at_name, integral_dirname));" + NL
            			+ IND + IND + IND + "integrateDataWriters.push_back(integral);" + NL
            			+ IND + IND + IND + "int integralInterval = 0;" + NL
            			+ IND + IND + IND + "if (integral_db->keyExists(\"ascii_dump_interval\")) {" + NL
            			+ IND + IND + IND + IND + "integralInterval = integral_db->getInteger(\"ascii_dump_interval\");" + NL
            			+ IND + IND + IND + IND + "integralIntervals.push_back(integralInterval);" + NL
            			+ IND + IND + IND + "} else {" + NL
            			+ IND + IND + IND + IND + "std::cerr << \"Error in parameter file: Integration output must have parameter 'ascii_dump_interval'.\" << endl;" + NL
            			+ IND + IND + IND + IND + "return -1;" + NL
            			+ IND + IND + IND + "}" + NL
            			+ IND + IND + IND + "if (integral_db->keyExists(\"variables\")) {" + NL
            			+ IND + IND + IND + IND + "vector<string> tmp = integral_db->getStringVector(\"variables\");" + NL
                        + IND + IND + IND + IND + "integralVariables.push_back(set<string>(tmp.begin(), tmp.end()));" + NL
            			+ IND + IND + IND + "} else {" + NL
            			+ IND + IND + IND + IND + "std::cerr << \"Error in parameter file: Integration output must have parameter 'variables'.\" << endl;" + NL
            			+ IND + IND + IND + IND + "return -1;" + NL
            			+ IND + IND + IND + "}" + NL
            			+ IND + IND + IND + "if (integralInterval > 0) {" + NL
            			+ IND + IND + IND + IND + "std::string result;" + NL
            			+ IND + IND + IND + IND + "for (std::vector<string>::iterator it = calculation.begin(); it != calculation.end(); ++it) {" + NL
            			+ IND + IND + IND + IND + IND + "result += (*it) + \" \";" + NL
            			+ IND + IND + IND + IND + "}" + NL
            			+ IND + IND + IND + IND + "output_information << \"    Integration output  \" << endl;" + NL
            			+ IND + IND + IND + IND + "output_information << \"    Output directory:  \" << integral_dirname << endl;" + NL
            			+ IND + IND + IND + IND + "output_information << \"    Snapshot interval: \" << integralInterval << endl;" + NL
            			+ IND + IND + IND + IND + "output_information << \"    Calculation: \" << result << endl;" + NL
            			+ IND + IND + IND + IND + "output_information << \"|--------------------------------------------------------|\" << endl;" + NL
            			+ IND + IND + IND + "}" + NL
            			+ analysisIntegrationConfiguration
            			+ IND + IND + "} else {" + NL
            			+ IND + IND + IND + "//End loop when no more integral_x found" + NL
            			+ IND + IND + IND + "break;" + NL
            			+ IND + IND + "}" + NL
            			+ IND + "}" + NL;
            	pointInputParameters = IND + "//Points" + NL
                        + IND + "n_at_commands = static_cast<int>(writer_db->getAllKeys().size());" + NL
            			+ IND + "for (int i = 0; i < n_at_commands; ++i) {" + NL
            			+ IND + IND + "std::string at_name = \"point_\" + tbox::Utilities::intToString(i);" + NL
            			+ IND + IND + "if (writer_db->keyExists(at_name)) {" + NL
            			+ IND + IND + IND + "std::shared_ptr<tbox::Database> point_db(writer_db->getDatabase(at_name));" + NL
            			+ IND + IND + IND + "string point_dirname;" + NL
            			+ IND + IND + IND + "if (point_db->keyExists(\"ascii_dump_dirname\")) {" + NL
            			+ IND + IND + IND + IND + "point_dirname = point_db->getStringWithDefault(\"ascii_dump_dirname\", \".\");" + NL
            			+ IND + IND + IND + "} else {" + NL
            			+ IND + IND + IND + IND + "std::cerr << \"Error in parameter file: Point output must have parameter 'ascii_dump_dirname'.\" << endl;" + NL
            			+ IND + IND + IND + IND + "return -1;" + NL
            			+ IND + IND + IND + "}" + NL
            			+ IND + IND + IND + "std::vector<double> coordinates(point_db->getDoubleVector(\"coordinates\"));" + NL + NL
            			+ IND + IND + IND + "std::shared_ptr<PointDataWriter> point(new PointDataWriter(coordinates, at_name, point_dirname));" + NL
            			+ IND + IND + IND + "pointDataWriters.push_back(point);" + NL
            			+ IND + IND + IND + "int pointInterval = 0;" + NL
            			+ IND + IND + IND + "if (point_db->keyExists(\"ascii_dump_interval\")) {" + NL
            			+ IND + IND + IND + IND + "pointInterval = point_db->getInteger(\"ascii_dump_interval\");" + NL
            			+ IND + IND + IND + IND + "pointIntervals.push_back(pointInterval);" + NL
            			+ IND + IND + IND + "} else {" + NL
            			+ IND + IND + IND + IND + "std::cerr << \"Error in parameter file: Point output must have parameter 'ascii_dump_interval'.\" << endl;" + NL
            			+ IND + IND + IND + IND + "return -1;" + NL
            			+ IND + IND + IND + "}" + NL
            			+ IND + IND + IND + "if (point_db->keyExists(\"variables\")) {" + NL
            			+ IND + IND + IND + IND + "vector<string> tmp = point_db->getStringVector(\"variables\");" + NL
                        + IND + IND + IND + IND + "pointVariables.push_back(set<string>(tmp.begin(), tmp.end()));" + NL
            			+ IND + IND + IND + "} else {" + NL
            			+ IND + IND + IND + IND + "std::cerr << \"Error in parameter file: Point output must have parameter 'variables'.\" << endl;" + NL
            			+ IND + IND + IND + IND + "return -1;" + NL
            			+ IND + IND + IND + "}" + NL
            			+ IND + IND + IND + "if (pointInterval > 0) {" + NL
            			+ IND + IND + IND + IND + "std::stringstream ss;" + NL
            			+ IND + IND + IND + IND + "for(size_t i = 0; i < coordinates.size(); ++i) {" + NL
            			+ IND + IND + IND + IND + IND + "if(i != 0)" + NL
            			+ IND + IND + IND + IND + IND + IND + "ss << \",\";" + NL
            			+ IND + IND + IND + IND + IND + "ss << coordinates[i];" + NL
            			+ IND + IND + IND + IND + "}" + NL
            			+ IND + IND + IND + IND + "std::string result = ss.str();" + NL
            			+ IND + IND + IND + IND + "output_information << \"    Point output  \" << endl;" + NL
            			+ IND + IND + IND + IND + "output_information << \"    Output directory:  \" << point_dirname << endl;" + NL
            			+ IND + IND + IND + IND + "output_information << \"    Snapshot interval: \" << pointInterval << endl;" + NL
            			+ IND + IND + IND + IND + "output_information << \"    Coordinates: \" << result << endl;" + NL
            			+ IND + IND + IND + IND + "output_information << \"|--------------------------------------------------------|\" << endl;" + NL
            			+ IND + IND + IND + "}" + NL
            			+ analysisPointConfiguration
            			+ IND + IND + "} else {" + NL
            			+ IND + IND + IND + "//End loop when no more point_x found" + NL
            			+ IND + IND + IND + "break;" + NL
            			+ IND + IND + "}" + NL
            			+ IND + "}" + NL;
            	integrationParameters = ", integralIntervals, integralVariables, integrateDataWriters";
            	pointParameters = ", pointIntervals, pointVariables, pointDataWriters";
            	if (analysis) {
            		integrationParameters = integrationParameters + ", integralAnalysis";
            		pointParameters = pointParameters + ", pointAnalysis";
            	}
            	integrationFirstOutput = IND + "//Integral output" + NL
            			+ IND + "if (integralIntervals.size() > 0 && !tbox::RestartManager::getManager()->isFromRestart()) {" + NL
            			+ IND + IND + "int i = 0;" + NL
            			+ IND + IND + "for (std::vector<std::shared_ptr<IntegrateDataWriter> >::iterator it = integrateDataWriters.begin(); it != integrateDataWriters.end(); ++it) {" + NL
            			+ IND + IND + IND + "if (integralIntervals[i] > 0) {" + NL
            			+ IND + IND + IND + IND + "(*it)->writePlotData(patch_hierarchy, iteration_num, loop_time);" + NL
            			+ IND + IND + IND + "}" + NL
            			+ IND + IND + IND + "i++;" + NL
            			+ IND + IND + "}" + NL
            			+ IND + "}" + NL;
            	pointFirstOutput = IND + "//Point output" + NL
            			+ IND + "if (pointIntervals.size() > 0 && !tbox::RestartManager::getManager()->isFromRestart()) {" + NL
            			+ IND + IND + "int i = 0;" + NL
            			+ IND + IND + "for (std::vector<std::shared_ptr<PointDataWriter> >::iterator it = pointDataWriters.begin(); it != pointDataWriters.end(); ++it) {" + NL
            			+ IND + IND + IND + "if (pointIntervals[i] > 0) {" + NL
            			+ IND + IND + IND + IND + "(*it)->writePlotData(patch_hierarchy, iteration_num, loop_time);" + NL
            			+ IND + IND + IND + "}" + NL
            			+ IND + IND + IND + "i++;" + NL
            			+ IND + IND + "}" + NL
            			+ IND + "}" + NL;
            	plotterSetup = plotterSetup + IND + "problem->setupIntegralPlotter(integrateDataWriters);" + NL;
            	plotterSetup = plotterSetup + IND + "problem->setupPointPlotter(pointDataWriters);" + NL;
            }
            connector = connector.replace("#SlicerDeclarations#", slicerDeclarations);
            connector = connector.replace("#IntegrationDeclaration#", integrationDeclarations + pointDeclarations);
            connector = connector.replace("#SlicerInputParameters#", slicerInputParameters);
            connector = connector.replace("#IntegrationInputParameters#", integrationInputParameters + pointInputParameters);
            connector = connector.replace("#IntegrationParameters#", integrationParameters + pointParameters);
            connector = connector.replace("#slicerParameter#", slicerParameter);
            connector = connector.replace("#slicerFirstOutput#", slicerFirstOutput);
            connector = connector.replace("#IntegrationFirstOutput#", integrationFirstOutput + pointFirstOutput);
            connector = connector.replace("#slicerPeriodicalWrite#", slicerPeriodicalWrite);
            connector = connector.replace("#integrationPeriodicalWrite#", integrationPeriodicalWrite + pointPeriodicalWrite);
            connector = connector.replaceAll("#PlotterSetup#", plotterSetup);
                        
            String meshInputParameters = "";
            String fullMeshDeclaration = "";
            if (pi.isHasMeshFields() || pi.isHasABMMeshFields()) {            	
            	meshInputParameters = IND + "//Full dump mesh" + NL
            			+ IND + "if (writer_db->isDatabase(\"full_dump_mesh\")) {" + NL
            			+ IND + IND + "std::shared_ptr<tbox::Database> full_writer_db(writer_db->getDatabase(\"full_dump_mesh\"));" + NL
            			+ IND + IND + "//Evolution dump" + NL
            			+ IND + IND + "if (full_writer_db->keyExists(\"hdf5_dump_interval\")) {" + NL
            			+ IND + IND + IND + "viz_mesh_dump_interval = full_writer_db->getInteger(\"hdf5_dump_interval\");" + NL
            			+ IND + IND + "}" + NL
            			+ IND + IND + "if ( viz_mesh_dump_interval > 0) {" + NL
            			+ IND + IND + IND + "if (full_writer_db->keyExists(\"hdf5_dump_dirname\")) {" + NL
            			+ IND + IND + IND + IND + "viz_mesh_dump_dirname = full_writer_db->getStringWithDefault(\"hdf5_dump_dirname\", \".\");" + NL
            			+ IND + IND + IND + "} else {" + NL
            			+ IND + IND + IND + IND + "//The directory for the hdf5 output parameter does not exist" + NL 
            			+ IND + IND + IND + IND + "return -1;" + NL
            			+ IND + IND + IND + "}" + NL
            			+ IND + IND + IND + "if (full_writer_db->keyExists(\"variables\")) {" + NL
            			+ IND + IND + IND + IND + "full_mesh_writer_variables = full_writer_db->getStringVector(\"variables\");" + NL
            			+ IND + IND + IND + "} else {" + NL
            			+ IND + IND + IND + IND + "//The directory for the hdf5 output parameter does not exist" + NL 
            			+ IND + IND + IND + IND + "return -1;" + NL
            			+ IND + IND + IND + "}" + NL
            			+ IND + IND + IND + "output_information << \"    Full mesh domain output  \" << endl;" + NL
            			+ IND + IND + IND + "output_information << \"    Output directory:  \" << viz_mesh_dump_dirname << endl;" + NL
            			+ IND + IND + IND + "output_information << \"    Snapshot interval: \" << viz_mesh_dump_interval << endl;" + NL
            			+ IND + IND + IND + "output_information << \"|--------------------------------------------------------|\" << endl;" + NL
            			+ IND + IND + "}" + NL
            			+ IND + "}" + NL;
            	fullMeshDeclaration = "string viz_mesh_dump_dirname;" + NL
            			+ "vector<string> full_mesh_writer_variables;" + NL
            			+ "int viz_mesh_dump_interval;" + NL;
            }
            String particleInputParameters = "";
            if (pi.isHasParticleFields() || pi.isHasABMMeshlessFields()) {
            	particleInputParameters = IND + "//Full dump particles" + NL
            			+ IND + "if (writer_db->isDatabase(\"full_dump_particles\")) {" + NL
            			+ IND + IND + "std::shared_ptr<tbox::Database> full_writer_db(writer_db->getDatabase(\"full_dump_particles\"));" + NL
            			+ IND + IND + "//Evolution dump" + NL
            			+ IND + IND + "if (full_writer_db->keyExists(\"hdf5_dump_interval\")) {" + NL
            			+ IND + IND + IND + "viz_particle_dump_interval = full_writer_db->getInteger(\"hdf5_dump_interval\");" + NL
            			+ IND + IND + "}" + NL
            			+ IND + IND + "if ( viz_particle_dump_interval > 0) {" + NL
            			+ IND + IND + IND + "if (full_writer_db->keyExists(\"hdf5_dump_dirname\")) {" + NL
            			+ IND + IND + IND + IND + "viz_particle_dump_dirname = full_writer_db->getStringWithDefault(\"hdf5_dump_dirname\", \".\");" + NL
            			+ IND + IND + IND + "} else {" + NL
            			+ IND + IND + IND + IND + "//The directory for the hdf5 output parameter does not exist" + NL 
            			+ IND + IND + IND + IND + "return -1;" + NL
            			+ IND + IND + IND + "}" + NL
            			+ IND + IND + IND + "if (full_writer_db->keyExists(\"variables\")) {" + NL
            			+ IND + IND + IND + IND + "full_particle_writer_variables = full_writer_db->getStringVector(\"variables\");" + NL
            			+ IND + IND + IND + "} else {" + NL
            			+ IND + IND + IND + IND + "//The directory for the hdf5 output parameter does not exist" + NL 
            			+ IND + IND + IND + IND + "return -1;" + NL
            			+ IND + IND + IND + "}" + NL
            			+ IND + IND + IND + "output_information << \"    Full particle domain output  \" << endl;" + NL
            			+ IND + IND + IND + "output_information << \"    Output directory:  \" << viz_particle_dump_dirname << endl;" + NL
            			+ IND + IND + IND + "output_information << \"    Snapshot interval: \" << viz_particle_dump_interval << endl;" + NL
            			+ IND + IND + IND + "output_information << \"|--------------------------------------------------------|\" << endl;" + NL
            			+ IND + IND + "}" + NL
            			+ IND + "}" + NL;
               	fullMeshDeclaration = fullMeshDeclaration + "string viz_particle_dump_dirname;" + NL
            			+ "vector<string> full_particle_writer_variables;" + NL
            			+ "int viz_particle_dump_interval;" + NL;
            }
            connector = connector.replace("#MeshInputParameters#", meshInputParameters);
            connector = connector.replace("#ParticleInputParameters#", particleInputParameters);
            connector = connector.replace("#FullMeshDeclaration#", fullMeshDeclaration);
            
            //Post initial data
            String postInit = "";
            if (analysis) {
                postInit = IND + "problem->postInit();" + NL;
            }
            connector = connector.replace("#PostInit#", postInit);
            
            //#ParticleCommonInit#
            if (pi.isHasParticleFields() || pi.isHasABMMeshlessFields()) {
                connector = connector.replace("#ParticleCommonInit#", IND + IND + "problem->initCommonVars(patch_hierarchy);" + NL);
            }
            else {
                connector = connector.replace("#ParticleCommonInit#", "");
            }
            
            //Variable maximum index: variables + region + interior + extra SAMRAI variables
            if (pi.isHasABMMeshlessFields() || pi.isHasParticleFields()) {
            	extraVars = 1 + extraVars;
            }
            if (pi.isHasABMMeshFields() || pi.isHasMeshFields()) {
            	extraVars = pi.getVariableInfo().getMeshVariables().size() + extraVars;
            }
            connector = connector.replaceAll("#NUMVARS#", String.valueOf(extraVars));
                        
            String deltas = "";
            if (pi.getDimensions() == 3) {
            	deltas = "deltas[0]/ratio[0]<<\" \"<<deltas[1]/ratio[1]<<\" \"<<deltas[2]/ratio[2]" + NL;
            }
            else {
            	deltas = "deltas[0]/ratio[0]<<\" \"<<deltas[1]/ratio[1]" + NL;
            }
            
            connector = connector.replaceAll("#deltas#", deltas);
            
            String refinementConversion = "";
            if (!pi.isHasABMMeshlessFields()) {
            	refinementConversion = IND + "//Refinement domain to index conversion" + NL
            			+ IND + "std::shared_ptr<tbox::Database> sti_db = input_db->getDatabase(\"StandardTagAndInitialize\");" + NL
            			+ IND + "n_at_commands = static_cast<int>(sti_db->getAllKeys().size());" + NL
            			+ IND + "for (int i = 0; i < n_at_commands; ++i) {" + NL
            			+ IND + IND + "std::string at_name = \"at_\" + tbox::Utilities::intToString(i);" + NL
            			+ IND + IND + "std::shared_ptr<tbox::Database> at_db(sti_db->getDatabase(at_name));" + NL
            			+ IND + IND + "int n_tag_keys = static_cast<int>(at_db->getAllKeys().size()) - 1;" + NL
            			+ IND + IND + "for (int j = 0; j < n_tag_keys; ++j) {" + NL
            			+ IND + IND + IND + "std::string tag_name = \"tag_\" + tbox::Utilities::intToString(j);" + NL
            			+ IND + IND + IND + "std::shared_ptr<tbox::Database> this_tag_db(at_db->getDatabase(tag_name));" + NL
            			+ IND + IND + IND + "std::string tagging_method = this_tag_db->getString(\"tagging_method\");" + NL + NL
            			+ IND + IND + IND + "if (tagging_method == \"REFINE_BOXES\") {" + NL
            			+ IND + IND + IND + IND + "std::vector<std::string> level_keys = this_tag_db->getAllKeys();" + NL
            			+ IND + IND + IND + IND + "int n_level_keys = static_cast<int>(level_keys.size());" + NL + NL
            			+ IND + IND + IND + IND + "// For each level specified, read the refine boxes." + NL
            			+ IND + IND + IND + IND + "int current_ratio_x = 1;" + NL
            			+ IND + IND + IND + IND + "int current_ratio_y = 1;" + NL
            			+ IND + IND + IND + IND + "int current_ratio_z = 1;" + NL
            			+ IND + IND + IND + IND + "for (int k = 0; k < n_level_keys; ++k) {" + NL
            			+ IND + IND + IND + IND + IND + "hier::BoxContainer level_boxes;" + NL
            			+ IND + IND + IND + IND + IND + "if (level_keys[k] == \"tagging_method\") {" + NL
            			+ IND + IND + IND + IND + IND + IND + "continue;" + NL
            			+ IND + IND + IND + IND + IND + "}" + NL
            			+ IND + IND + IND + IND + IND + "int level = atoi(level_keys[k].substr(6).c_str());" + NL
            			+ IND + IND + IND + IND + IND + "if (level < patch_hierarchy->getMaxNumberOfLevels()) {" + NL
            			+ IND + IND + IND + IND + IND + IND + "current_ratio_x = current_ratio_x * patch_hierarchy->getRatioToCoarserLevel(level)[0];" + NL
            			+ IND + IND + IND + IND + IND + IND + "current_ratio_y = current_ratio_y * patch_hierarchy->getRatioToCoarserLevel(level)[1];" + NL
            			+ IND + IND + IND + IND + IND + IND + "if (dim == tbox::Dimension(3)) {" + NL
            			+ IND + IND + IND + IND + IND + IND + IND + "current_ratio_z = current_ratio_z * patch_hierarchy->getRatioToCoarserLevel(level)[2];" + NL
            			+ IND + IND + IND + IND + IND + IND + "}" + NL
            			+ IND + IND + IND + IND + IND + IND + "std::shared_ptr<tbox::Database> level_db(this_tag_db->getDatabase(level_keys[k]));" + NL
            			+ IND + IND + IND + IND + IND + IND + "std::vector<std::string> box_keys =level_db->getAllKeys();" + NL
            			+ IND + IND + IND + IND + IND + IND + "int n_box_keys = static_cast<int>(box_keys.size());" + NL
            			+ IND + IND + IND + IND + IND + IND + "std::vector<tbox::DatabaseBox> box_vector;" + NL
            			+ IND + IND + IND + IND + IND + IND + "for (int l = 0; l < n_box_keys; ++l) {" + NL
            			+ IND + IND + IND + IND + IND + IND + IND + "std::string box_name = \"box_\" + tbox::Utilities::intToString(l);" + NL
            			+ IND + IND + IND + IND + IND + IND + IND + "std::shared_ptr<tbox::Database> box_db(level_db->getDatabase(box_name));" + NL
            			+ IND + IND + IND + IND + IND + IND + IND + "std::vector<double> lower(box_db->getDoubleVector(\"x_lo\"));" + NL
            			+ IND + IND + IND + IND + IND + IND + IND + "std::vector<double> upper(box_db->getDoubleVector(\"x_up\"));" + NL + NL
            			+ IND + IND + IND + IND + IND + IND + IND + "int* low = new int[dim.getValue()];" + NL
            			+ IND + IND + IND + IND + IND + IND + IND + "int* up = new int[dim.getValue()];" + NL
            			+ IND + IND + IND + IND + IND + IND + IND + "low[0] = floor((lower[0] - grid_geometry->getXLower()[0]) / (grid_geometry->getDx()[0]/current_ratio_x));" + NL
            			+ IND + IND + IND + IND + IND + IND + IND + "low[1] = floor((lower[1] - grid_geometry->getXLower()[1]) / (grid_geometry->getDx()[1]/current_ratio_y));" + NL
            			+ IND + IND + IND + IND + IND + IND + IND + "if (dim == tbox::Dimension(3)) {" + NL
            			+ IND + IND + IND + IND + IND + IND + IND + IND + "low[2] = floor((lower[2] - grid_geometry->getXLower()[2]) / (grid_geometry->getDx()[2]/current_ratio_z));" + NL
            			+ IND + IND + IND + IND + IND + IND + IND + "}" + NL + NL
            			+ IND + IND + IND + IND + IND + IND + IND + "up[0] = ceil((upper[0] - grid_geometry->getXLower()[0]) / (grid_geometry->getDx()[0]/current_ratio_x)) - 1;" + NL
            			+ IND + IND + IND + IND + IND + IND + IND + "up[1] = ceil((upper[1] - grid_geometry->getXLower()[1]) / (grid_geometry->getDx()[1]/current_ratio_y)) - 1;" + NL
            			+ IND + IND + IND + IND + IND + IND + IND + "if (dim == tbox::Dimension(3)) {" + NL
            			+ IND + IND + IND + IND + IND + IND + IND + IND + "up[2] = ceil((upper[2] - grid_geometry->getXLower()[2]) / (grid_geometry->getDx()[2]/current_ratio_z)) - 1;" + NL
            			+ IND + IND + IND + IND + IND + IND + IND + "}" + NL + NL
            			+ IND + IND + IND + IND + IND + IND + IND + "tbox::DatabaseBox box(dim, low, up);" + NL
            			+ IND + IND + IND + IND + IND + IND + IND + "box_vector.push_back(box);" + NL
            			+ IND + IND + IND + IND + IND + IND + IND + "delete[] low;" + NL
            			+ IND + IND + IND + IND + IND + IND + IND + "delete[] up;" + NL
            			+ IND + IND + IND + IND + IND + IND + "}" + NL
            			+ IND + IND + IND + IND + IND + IND + "this_tag_db->putDatabase(level_keys[k]);" + NL
            			+ IND + IND + IND + IND + IND + IND + "std::shared_ptr<tbox::Database> new_level_db(this_tag_db->getDatabase(level_keys[k]));" + NL
            			+ IND + IND + IND + IND + IND + IND + "new_level_db->putDatabaseBoxVector(\"boxes\", box_vector);" + NL
            			+ IND + IND + IND + IND + IND + "}" + NL
            			+ IND + IND + IND + IND + IND + "else {" + NL
            			+ IND + IND + IND + IND + IND + IND + "std::vector<tbox::DatabaseBox> box_vector;" + NL
            			+ IND + IND + IND + IND + IND + IND + "int* low = new int[dim.getValue()];" + NL
            			+ IND + IND + IND + IND + IND + IND + "low[0] = 0;" + NL
            			+ IND + IND + IND + IND + IND + IND + "low[1] = 0;" + NL
            			+ IND + IND + IND + IND + IND + IND + "low[2] = 0;" + NL
            			+ IND + IND + IND + IND + IND + IND + "tbox::DatabaseBox box(dim, low, low);" + NL
            			+ IND + IND + IND + IND + IND + IND + "box_vector.push_back(box);" + NL
            			+ IND + IND + IND + IND + IND + IND + "this_tag_db->putDatabase(level_keys[k]);" + NL
            			+ IND + IND + IND + IND + IND + IND + "std::shared_ptr<tbox::Database> new_level_db(this_tag_db->getDatabase(level_keys[k]));" + NL
            			+ IND + IND + IND + IND + IND + IND + "new_level_db->putDatabaseBoxVector(\"boxes\", box_vector);" + NL
            			+ IND + IND + IND + IND + IND + IND + "delete[] low;" + NL
            			+ IND + IND + IND + IND + IND + "}" + NL
            			+ IND + IND + IND + IND + "}" + NL
            			+ IND + IND + IND + "}" + NL
            			+ IND + IND + "}" + NL
            			+ IND + "}" + NL;
            } 
            else {
            	refinementConversion = IND + "std::shared_ptr<tbox::Database> sti_db = input_db->getDatabase(\"StandardTagAndInitialize\");" + NL;
            }
            connector = connector.replaceAll("#refinementConversion#", refinementConversion);
                        
            return connector;
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            throw new CGException(CGException.CG00X, e.getMessage());
        }
    }
    
    /**
     * Creates the problem.cpp file from the template.
     * @param pi                The problem information
     * @param executionStep     The execution step code
     * @param initializations   The initialization code
     * @param finalization      The finalization code
     * @param mapping           The mapping code
     * @param analysis          The analysis code
     * @param synchronization   The synchronization code
     * @param movement          The movement code
     * @return                  The code
     * @throws CGException      CG00X External error
     */
    private String createProblem(ProblemInfo pi, String executionStep, String initializations, String finalization, 
            String mapping, String analysis, String synchronization, String movement) throws CGException {
        //Get the template
        String problem = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/Problem.cpp"), 0);
        VariableInfo vi = pi.getVariableInfo();
        HashMap<String, String> fields = vi.getMeshFields();
        HashMap<String, String> vectors = vi.getVectors();
        ArrayList<String> outputVarsAnalysis = vi.getOutputVarsAnalysis();
        HashMap<String, String> parameters = vi.getParameters();
        ArrayList<String> variables = vi.getMeshVariables();
        int dimensions = pi.getDimensions();
        ArrayList<String> coordinates = pi.getCoordinates();
        ArrayList<String> contCoordinates = pi.getContCoordinates();
                
        //Dimension replace
        problem = problem.replaceAll("#DIM#", String.valueOf(dimensions));
        //Include replace
        String includes = "#include <gsl/gsl_rng.h>" + NL
        		+ "#include <random>" + NL
        		+ "#include \"SAMRAI/pdat/CellVariable.h\"" + NL;
        if (pi.isHasParticleFields() || pi.isHasABMMeshlessFields()) {
        	includes = includes + "#include <time.h>" + NL
                    + "#include <silo.h>" + NL
                    + "#include <algorithm>" + NL
                    + "#include \"float.h\"" + NL
                    + "#include \"NonSync.h\"" + NL
                    + "#include \"NonSyncs.h\"" + NL  
                    + "#include \"SAMRAI/pdat/IndexVariable.h\"" + NL
                    + "#include \"SAMRAI/pdat/CellData.h\"" + NL;
        }
        if (pi.isHasABMMeshFields() || pi.isHasMeshFields() || pi.isHasParticleFields()) {
            /*if (pi.getX3dMovRegions() != null) {
            includes = includes + "#include \"SAMRAI/pdat/IndexVariable.h\"";
        	}*/
        	includes = includes
                    + "#include \"SAMRAI/pdat/NodeData.h\"" + NL
                    + "#include \"SAMRAI/pdat/NodeVariable.h\"" + NL
                    + "#include \"LagrangianPolynomicRefine.h\"" + NL;
        }
        problem = problem.replace("#includes#", includes);
        
        //Writer instance configuration
        String sliceVariablesParam = "";
        String sliceVariablesInitialization = "";
        String sliceVariablesInitializationInline = "";
        if (activeSlicers) {
            sliceVariablesParam = ", const vector<int> slicer_output_period, const vector<set<string> > sliceVariables, vector<std::shared_ptr<SlicerDataWriter > >sliceWriters, const vector<int> sphere_output_period, const vector<set<string> > sphereVariables, vector<std::shared_ptr<SphereDataWriter > > sphereWriters";
            if (vi.getOutputVarsAnalysis().size() > 0) {
            	sliceVariablesParam = sliceVariablesParam + ", const vector<bool> slicer_analysis_output, const vector<bool> sphere_analysis_output";
            }
            sliceVariablesInitialization = IND + "for (vector<set<string> >::const_iterator it = sliceVariables.begin(); "
                    + "it != sliceVariables.end(); ++it) {" + NL
                + IND + IND + "set<string> vars = *it;" + NL
                + IND + IND + "for (set<string>::const_iterator it2 = vars.begin() ; it2 != vars.end(); ++it2) {" + NL
                + IND + IND + IND + "d_sliceVariables.push_back(vars);" + NL
                + IND + IND + "}" + NL
                + IND + "}" + NL
                + IND + "for (vector<set<string> >::const_iterator it = sphereVariables.begin(); it != sphereVariables.end(); ++it) {" + NL
                + IND + IND + "set<string> vars = *it;" + NL
                + IND + IND + "for (set<string>::const_iterator it2 = vars.begin() ; it2 != vars.end(); ++it2) {" + NL
                + IND + IND + IND + "d_sphereVariables.push_back(vars);" + NL
                + IND + IND + "}" + NL
                + IND + "}";
            sliceVariablesInitializationInline = ", d_sliceWriters(sliceWriters.begin(), sliceWriters.end()), d_sphereWriters(sphereWriters.begin(), sphereWriters.end()), d_slicer_output_period(slicer_output_period.begin(), slicer_output_period.end()), d_sphere_output_period(sphere_output_period.begin(), sphere_output_period.end())";
            if (vi.getOutputVarsAnalysis().size() > 0) {
            	sliceVariablesInitializationInline = sliceVariablesInitializationInline + ", analysis_slice_dump(slicer_analysis_output.begin(), slicer_analysis_output.end()), analysis_sphere_dump(sphere_analysis_output.begin(), sphere_analysis_output.end())";
            }
        }
        problem = problem.replaceFirst("#sliceVariablesParam#", sliceVariablesParam);
        problem = problem.replaceFirst("#sliceVariablesInitialization#", sliceVariablesInitialization);
        problem = problem.replaceFirst("#sliceVariablesInitializationInline#", sliceVariablesInitializationInline);
        String integrationVariablesParam = "";
        String integrationVariablesInitialization = "";
        String integrationVariablesInitializationInline = "";
        String pointVariablesParam = "";
        String pointVariablesInitialization = "";
        String pointVariablesInitializationInline = "";
        if (pi.isHasMeshFields()) {
         	integrationVariablesParam = ", const vector<int> integration_output_period, const vector<set<string> > integralVariables, vector<std::shared_ptr<IntegrateDataWriter > > integrateDataWriters";
         	pointVariablesParam = ", const vector<int> point_output_period, const vector<set<string> > pointVariables, vector<std::shared_ptr<PointDataWriter > > pointDataWriters";
         	if (vi.getOutputVarsAnalysis().size() > 0) {
         		integrationVariablesParam = integrationVariablesParam + ", const vector<bool> integration_analysis_output";
         		pointVariablesParam = pointVariablesParam + ", const vector<bool> point_analysis_output";
         	}
        	integrationVariablesInitialization = IND + "for (vector<set<string> >::const_iterator it = integralVariables.begin(); it != integralVariables.end(); ++it) {" + NL
        			+ IND + IND + "set<string> vars = *it;" + NL
        			+ IND + IND + "for (set<string>::const_iterator it2 = vars.begin() ; it2 != vars.end(); ++it2) {" + NL
        			+ IND + IND + IND + "d_integralVariables.push_back(vars);" + NL
        			+ IND + IND + "}" + NL
        			+ IND + "}" + NL;
        	pointVariablesInitialization = IND + "for (vector<set<string> >::const_iterator it = pointVariables.begin(); it != pointVariables.end(); ++it) {" + NL
        			+ IND + IND + "set<string> vars = *it;" + NL
        			+ IND + IND + "for (set<string>::const_iterator it2 = vars.begin() ; it2 != vars.end(); ++it2) {" + NL
        			+ IND + IND + IND + "d_pointVariables.push_back(vars);" + NL
        			+ IND + IND + "}" + NL
        			+ IND + "}" + NL;
        	integrationVariablesInitializationInline = ", d_integrateDataWriters(integrateDataWriters.begin(), integrateDataWriters.end()), d_integration_output_period(integration_output_period.begin(), integration_output_period.end())";
        	pointVariablesInitializationInline = ", d_pointDataWriters(pointDataWriters.begin(), pointDataWriters.end()), d_point_output_period(point_output_period.begin(), point_output_period.end())";
         	if (vi.getOutputVarsAnalysis().size() > 0) {
         		integrationVariablesInitializationInline = integrationVariablesInitializationInline + ", analysis_integration_dump(integration_analysis_output.begin(), integration_analysis_output.end())";
         		pointVariablesInitializationInline = pointVariablesInitializationInline + ", analysis_point_dump(point_analysis_output.begin(), point_analysis_output.end())";
         	}
        }
        problem = problem.replaceFirst("#integrationVariablesParam#", integrationVariablesParam + pointVariablesParam);
        problem = problem.replaceFirst("#integrationVariablesInitialization#", integrationVariablesInitialization + pointVariablesInitialization);
        problem = problem.replaceFirst("#integrationVariablesInitializationInline#", integrationVariablesInitializationInline + pointVariablesInitializationInline);
        
        String subcyclingParams = IND + "d_refinedTimeStepping = false;" + NL
			+ IND + "if (database->isString(\"subcycling\")) {" + NL
			+ IND + IND + "if (database->getString(\"subcycling\") == \"BERGER-OLIGER\") {" + NL
			+ IND + IND + IND + "d_refinedTimeStepping = true;" + NL
			+ IND + IND + "}" + NL
			+ IND + "}" + NL; 
        problem = problem.replaceFirst("#subcycling#", subcyclingParams);
        //Global variables
        problem = replaceGlobalVariables(problem, pi);
        //Regridding parameters
        problem = replaceRegridding(problem, pi);
        //Stencil replace
        problem = replaceStencil(problem, pi, vi);
        //Replacements referencing to variables
        problem = replaceVariables(problem, dimensions, variables, fields, pi.getDerivativeAuxiliaryFields(), pi.getPostRefinementAuxiliaryFields(), synchronization, pi.getRegionIds(), pi.getCoordinates(), outputVarsAnalysis, pi.getExtrapolatedFieldGroups(), pi);
        //Parameter declaration
        problem = problem.replaceFirst("#parameter declaration#", getParameterDeclaration(pi, parameters, contCoordinates));
        //Reset Hierarchy configuration
        String sync = synchronization.substring(
                synchronization.indexOf("#Problem variables#") + "#Problem variables#".length(), 
                synchronization.indexOf("#resetHierarchyConfiguration#"));
        
        if (pi.getX3dMovRegions() != null) {
            sync = sync + NL + IND + "d_mapping_fill2 = std::shared_ptr< xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());" + NL;
        }
        problem = problem.replaceAll("#fill variables#", sync);
        
        String fillNewLevelAux = "";
        if (pi.getPostRefinementAuxiliaryFields().size() > 0) {
        	fillNewLevelAux = IND + "d_fill_new_level_aux    = std::shared_ptr< xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());" + NL;
        }
        if (pi.getDerivativeAuxiliaryFields().size() > 0) {
        	fillNewLevelAux = fillNewLevelAux + IND + "d_fill_new_level_der_aux    = std::shared_ptr< xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());" + NL;
        }
        problem = problem.replaceFirst("#fillNewLevelAux#", fillNewLevelAux);
        
        
        String refOp = "";
        if (pi.getX3dMovRegions() != null) {
            refOp = refOp + IND + "string mapping_inter = \"NO_REFINE\";" + NL;
        }
        problem = problem.replaceAll("#refineOperators#", refOp);

        String postNewLevelFunction = "";
        if (CodeGeneratorUtils.find(pi.getProblem(), "/*/mms:region/mms:postRefinementAlgorithm/*/sml:simml|"
        		+ "/*/mms:subregions/mms:subregion/mms:postRefinementAlgorithm/*/sml:simml").getLength() > 0) {
        	postNewLevelFunction = SAMRAIPDEExecution.createPostNewLevelAlgorithm(pi);
        }
        problem = problem.replaceFirst("#postNewLevelFunction#", postNewLevelFunction);
        
        String derivativeAuxFieldSyncrhonization = "";
        if (CodeGeneratorUtils.auxiliaryFieldsHasDerivatives(pi.getProblem())) {
        	derivativeAuxFieldSyncrhonization = IND + "for (int fine_ln = finest_level; fine_ln > coarsest_level; --fine_ln) {" + NL
        			+ IND + IND + "std::shared_ptr<hier::PatchLevel> fine_level(hierarchy->getPatchLevel(fine_ln));" + NL
        			+ IND + IND + "d_bdry_fill_init_aux->createSchedule(fine_level, this)->fillData(sync_time, true);" + NL
        			+ IND + "}" + NL;
        }
        problem = problem.replaceFirst("#derivativeAuxFieldSyncrhonization#", derivativeAuxFieldSyncrhonization);
        
        //Reset Hierarchy configuration
        problem = problem.replaceAll("#resetHierarchyConfiguration#", synchronization.substring(
                synchronization.indexOf("#resetHierarchyConfiguration#") + "#resetHierarchyConfiguration#".length(), 
                synchronization.indexOf("#Variable registrations#")));
        //Mapping
        String mapDataArgs = "const double time, const bool initial_time, const int ln, const std::shared_ptr< hier::PatchLevel >& level";
        if (pi.getX3dMovRegions() != null && pi.getX3dMovRegions().getType().equals("Fixed")) {
            mapDataArgs = mapDataArgs + ", int timeSlice";
        }
        problem = problem.replaceFirst("#mapDataArgs#", mapDataArgs);
        problem = problem.replaceFirst("#mapping#", mapping.substring(0, mapping.lastIndexOf("#mappingFunctions#")));
        problem = problem.replaceFirst("#mappingFunctions#", mapping.substring(mapping.lastIndexOf("#mappingFunctions#") 
                + "#mappingFunctions#".length(), mapping.lastIndexOf("#mappingDeclarations#")));
        String finer = "";
        if (!pi.isHasABMMeshlessFields()) {
            finer = finer + IND + "//Fill a finer level with the data of the next coarse level." + NL
                    + IND + "if ((level_number > 0) || old_level) {" + NL
                    + IND + IND + "d_mapping_fill->createSchedule(level,old_level,level_number-1,hierarchy,this)->" 
                    + "fillData(init_data_time, false);" + NL
                    + IND + IND + "correctFOVS(level);" + NL
                    + IND + "}";
        }
        problem = problem.replaceFirst("#finerLevelMapping#", finer);
        String mapDataCall = "";
        if (pi.getX3dMovRegions() == null || !pi.getX3dMovRegions().getType().equals("Fixed")) {
            mapDataCall = IND + "if (initial_time || level_number == 0) {" + NL
            		+ IND + IND + "mapDataOnPatch(init_data_time, initial_time, level_number, level);" + NL
            		+ IND + "}" + NL;
        }
        else {
            mapDataCall = IND + "for (int timeSlice = TIMESLICES - 1; timeSlice >= 0; timeSlice--) {" + NL
                    + IND + IND + "mapDataOnPatch(init_data_time, initial_time, level_number, level, timeSlice);" + NL + NL
                    + IND + IND + "//Interphase mapping" + NL
                    + IND + IND + "interphaseMapping(init_data_time, initial_time, level_number, level, 1);" + NL + NL
                    + IND + IND + "//Get cell points" + NL
                    + IND + IND + "getCellPoints(level, timeSlice);" + NL
                    + IND + "}" + NL;
        }
        problem = problem.replaceFirst("#mapDataCall#", mapDataCall);
        //interphase
        String interphaseUse = "";
        if (pi.isHasMeshFields() || pi.isHasParticleFields()) {
            interphaseUse = IND + "//Interphase mapping" + NL
                     + IND + "if (initial_time || level_number == 0) {" + NL
                     + IND + IND + "interphaseMapping(init_data_time, initial_time, level_number, level, 1);" + NL
                     + IND + "}" + NL;
        }
        problem = problem.replaceFirst("#interphaseMappingUse#", interphaseUse);
        String mappingFunction = "";
        if (pi.isHasMeshFields() || pi.isHasParticleFields()) {
            mappingFunction = mapping.substring(mapping.lastIndexOf("#interphaseMappingFunctions#") 
                    + "#interphaseMappingFunctions#".length(), mapping.lastIndexOf("#interphaseMappingDeclarations#"));
        }
        problem = problem.replaceFirst("#interphaseMappingFunction#", mappingFunction);
        if (pi.getX3dMovRegions() != null) {
            problem = problem.replaceFirst("#interphaseMovementFunction#", movement.substring(mapping.lastIndexOf("#interphaseMovementFunction#") 
                    + "#interphaseMovementFunction#".length() + 1, movement.lastIndexOf("#interphaseMovementDeclaration#")));
        }
        else {
            problem = problem.replaceFirst("#interphaseMovementFunction#", "");  
        }
        
        //Initial conditions
        problem = problem.replaceFirst("#initial conditions#", indent(initializations.substring(0, initializations.indexOf("#postInitSync")), 2));

        String levelInfluenceRadius = "";
        if (pi.isHasABMMeshlessFields() || pi.isHasParticleFields()) {
        	levelInfluenceRadius = IND + "double level_influenceRadius = influenceRadius/MAX(1,level_ratio);" + NL
        			+ IND + "double volume_level_factor = 1.0 / MAX(1, level->getRatioToLevelZero().getProduct());" + NL;
        	for (String speciesName: pi.getParticleSpecies()) {
             	levelInfluenceRadius = levelInfluenceRadius + IND + "time_interpolate_operator_particles_" + speciesName + "->setRatio(level_ratio);" + NL
             			+ IND + "refine_particle_operator_" + speciesName + "->setStep(0);" + NL
             			+ IND + "time_interpolate_operator_particles_" + speciesName + "->setStep(0);" + NL
             			+ IND + "time_interpolate_operator_particles_" + speciesName + "->setTimeSubstepNumber(bo_substep_iteration[ln]);" + NL;
        	}
        }
        if (pi.isHasMeshFields()) {
        	for (TimeInterpolator ti: pi.getTimeInterpolationInfo().getTimeInterpolators().values()) {
        		levelInfluenceRadius = levelInfluenceRadius + IND + "time_interpolate_operator_mesh" + ti.getId() + "->setRatio(level_ratio);" + NL
             			+ IND + "time_interpolate_operator_mesh" + ti.getId() + "->setStep(0);" + NL
             			+ IND + "time_interpolate_operator_mesh" + ti.getId() + "->setTimeSubstepNumber(bo_substep_iteration[ln]);" + NL;	
        	}
        }
        problem = problem.replaceFirst("#level_influenceRadius#", levelInfluenceRadius);

        //Post Initial conditions synchronization
        problem = problem.replaceFirst("#postInitSync#", initializations.substring(initializations.indexOf("#postInitSync") 
                + "#postInitSync#".length()));
        String fillBoundCall = "";
        if (pi.isHasABMMeshFields() || pi.isHasMeshFields()) {
        	String auxSchedule = "";
        	if (pi.getPostRefinementAuxiliaryFields().size() > 0) {
        		auxSchedule = IND + IND + "d_fill_new_level_aux->createSchedule(level,this)->fillData(init_data_time, false);" + NL;
        	}
        	if (pi.getDerivativeAuxiliaryFields().size() > 0) {
        		auxSchedule = auxSchedule + IND + IND + "d_fill_new_level_der_aux->createSchedule(level,old_level,level_number-1,hierarchy,this)->"
                        + "fillData(init_data_time, false);" + NL;
        	}
        	String processNewLevelCall = "";
            if (CodeGeneratorUtils.find(pi.getProblem(), "/*/mms:region/mms:postRefinementAlgorithm/*/sml:simml|"
            		+ "/*/mms:subregions/mms:subregion/mms:postRefinementAlgorithm/*/sml:simml").getLength() > 0) {
        		processNewLevelCall = IND + IND + "postNewLevel(level);" + NL;
        	}
            fillBoundCall = IND + "//Fill a finer level with the data of the next coarse level." + NL
                    + IND + "if (!initial_time && ((level_number > 0) || old_level)) {" + NL
                    + IND + IND + "d_fill_new_level->createSchedule(level,old_level,level_number-1,hierarchy,this)->"
                    + "fillData(init_data_time, false);" + NL
                    + processNewLevelCall
                    + auxSchedule
                    + IND + "}";
        }
        problem = problem.replaceFirst("#fillBoundaries#", fillBoundCall);

        String slicePlotter = "";
        if (activeSlicers) {
            String slicedFovs = "";
            ArrayList<String> regions = pi.getRegionIds();
            for (int i = 0; i < regions.size(); i++) {
                if (CodeGeneratorUtils.isNumber(regions.get(i))) {
                    String var = "FOV_" + regions.get(i);
                    slicedFovs = slicedFovs + IND + IND + "plotter->registerPlotQuantity(\"" + var + "\",\"SCALAR\",d_" + var + "_id);" + NL;
                }
            }
            slicePlotter = "/*" + NL
                    + " * Set up external plotter to plot sliced data from this class." + NL  
                    + " * Register variables appropriate for plotting." + NL                       
                    + " */" + NL
                    + "int Problem::setupSlicePlotter(vector<std::shared_ptr<SlicerDataWriter> > &plotters) const {" + NL
                    + IND + "if (!d_patch_hierarchy) {" + NL
                    + IND + "TBOX_ERROR(d_object_name << \": No hierarchy in\\\\n\"" + NL
                    + IND + IND + "<< \" Problem::setupSlicePlotter\\\\n\"" + NL
                    + IND + IND + "<< \"The hierarchy must be set before calling\\\\n\"" + NL
                    + IND + IND + "<< \"this function.\\\\n\");" + NL
                    + IND + "}" + NL
                    + IND + "int i = 0;" + NL
                    + IND + "for (vector<std::shared_ptr<SlicerDataWriter> >::const_iterator it = plotters.begin();"
                            + " it != plotters.end(); ++it) {" + NL
                    + IND + IND + "std::shared_ptr<SlicerDataWriter> plotter = *it;" + NL
                    + slicedFovs
                    + IND + IND + "hier::VariableDatabase *vdb = hier::VariableDatabase::getDatabase();" + NL
                    + IND + IND + "set<string> variables = d_sliceVariables[i];" + NL
                    + IND + IND + "for (set<string>::const_iterator it2 = variables.begin() ; it2 != variables.end(); ++it2) {" + NL
                    + IND + IND + IND + "string var_to_register = *it2;" + NL
                    + IND + IND + IND + "if (!(vdb->checkVariableExists(var_to_register))) {" + NL
                    + IND + IND + IND + IND + "TBOX_ERROR(d_object_name << \": Variable selected for Slice not found:\" <<  var_to_register);" + NL
                    + IND + IND + IND + "}" + NL
                    + IND + IND + IND + "int var_id = vdb->getVariable(var_to_register)->getInstanceIdentifier();" + NL
                    + IND + IND + IND + "plotter->registerPlotQuantity(var_to_register,\"SCALAR\",var_id);" + NL
                    + IND + IND + "}" + NL
                    + IND + IND + "i++;" + NL
                    + IND + "}" + NL + NL
                    + IND + "return 0;" + NL
                    + "}" + NL + NL 
                    + "/*" + NL
                    + " * Set up external plotter to plot spherical data from this class." + NL  
                    + " * Register variables appropriate for plotting." + NL                       
                    + " */" + NL
                    + "int Problem::setupSpherePlotter(vector<std::shared_ptr<SphereDataWriter> > &plotters) const {" + NL
                    + IND + "if (!d_patch_hierarchy) {" + NL
                    + IND + "TBOX_ERROR(d_object_name << \": No hierarchy in\\\\n\"" + NL
                    + IND + IND + "<< \" Problem::setupSpherePlotter\\\\n\"" + NL
                    + IND + IND + "<< \"The hierarchy must be set before calling\\\\n\"" + NL
                    + IND + IND + "<< \"this function.\\\\n\");" + NL
                    + IND + "}" + NL
                    + IND + "int i = 0;" + NL
                    + IND + "for (vector<std::shared_ptr<SphereDataWriter> >::const_iterator it = plotters.begin();"
                            + " it != plotters.end(); ++it) {" + NL
                    + IND + IND + "std::shared_ptr<SphereDataWriter> plotter = *it;" + NL
                    + slicedFovs
                    + IND + IND + "hier::VariableDatabase *vdb = hier::VariableDatabase::getDatabase();" + NL
                    + IND + IND + "set<string> variables = d_sphereVariables[i];" + NL
                    + IND + IND + "for (set<string>::const_iterator it2 = variables.begin() ; it2 != variables.end(); ++it2) {" + NL
                    + IND + IND + IND + "string var_to_register = *it2;" + NL
                    + IND + IND + IND + "if (!(vdb->checkVariableExists(var_to_register))) {" + NL
                    + IND + IND + IND + IND + "TBOX_ERROR(d_object_name << \": Variable selected for Sphere not found:\" <<  var_to_register);" + NL
                    + IND + IND + IND + "}" + NL
                    + IND + IND + IND + "int var_id = vdb->getVariable(var_to_register)->getInstanceIdentifier();" + NL
                    + IND + IND + IND + "plotter->registerPlotQuantity(var_to_register,\"SCALAR\",var_id);" + NL
                    + IND + IND + "}" + NL
                    + IND + IND + "i++;" + NL
                    + IND + "}" + NL + NL
                    + IND + "return 0;" + NL
                    + "}" + NL;
        }
        String particlePlotter = "";
        if (pi.isHasABMMeshlessFields() || pi.isHasParticleFields()) {
        	particlePlotter = "/*" + NL
        			+ " * Set up external plotter to plot internal data from this class." + NL
        			+ " * Register variables appropriate for plotting." + NL
        			+ " */" + NL
        			+ "int Problem::setupPlotterParticles(ParticleDataWriter &plotter) const {" + NL
        			+ IND + "if (!d_patch_hierarchy) {" + NL
        			+ IND + IND + "TBOX_ERROR(d_object_name << \": No hierarchy in\\n\"" + NL
        			+ IND + IND + "<< \" Problem::setupPlotter\\n\"" + NL
        			+ IND + IND + IND + "<< \"The hierarchy must be set before calling\\n\"" + NL
        			+ IND + IND + IND + "<< \"this function.\\n\");" + NL
        			+ IND + "}" + NL
        			+ IND + "hier::VariableDatabase *vdb = hier::VariableDatabase::getDatabase();" + NL + NL 
        			+ IND + "for (set<string>::const_iterator it = d_full_particle_writer_variables.begin() ; it != d_full_particle_writer_variables.end(); ++it) {" + NL + NL 
        			+ IND + IND + "string var_to_register = *it;" + NL
        			+ registerVariablesForOutputParticles(pi.getVariableInfo().getParticleVariables().keySet())
        			+ IND + "}" + NL
        			+ IND + "return 0;" + NL
        			+ "}" + NL;
        }
        String meshPlotter = "";
        if (pi.isHasABMMeshFields() || pi.isHasMeshFields()) {
        	meshPlotter = "/*" + NL
        			+ " * Set up external plotter to plot internal data from this class." + NL
        			+ " * Register variables appropriate for plotting." + NL
        			+ " */" + NL
        			+ "int Problem::setupPlotterMesh(appu::VisItDataWriter &plotter) const {" + NL
        			+ IND + "if (!d_patch_hierarchy) {" + NL
        			+ IND + IND + "TBOX_ERROR(d_object_name << \": No hierarchy in\\n\"" + NL
        			+ IND + IND + IND + "<< \" Problem::setupPlotter\\n\"" + NL
        			+ IND + IND + IND + "<< \"The hierarchy must be set before calling\\n\"" + NL
        			+ IND + IND + IND + "<< \"this function.\\n\");" + NL
        			+ IND + "}" + NL
        			+ registerVariablesForOutput(pi.getRegionIds())
        			+ IND + "return 0;" + NL
        			+ "}" + NL;
        }
        String integrationPlotter = "";
        String pointPlotter = "";
        if (pi.isHasMeshFields()) {
        	integrationPlotter = "/*" + NL
        			+ " * Set up external plotter to plot integration data from this class." + NL
        			+ " * Register variables appropriate for plotting." + NL
        			+ " */" + NL
        			+ "int Problem::setupIntegralPlotter(vector<std::shared_ptr<IntegrateDataWriter> > &plotters) const {" + NL
        			+ IND + "if (!d_patch_hierarchy) {" + NL
        			+ IND + IND + "TBOX_ERROR(d_object_name << \": No hierarchy in\\n\"" + NL
        			+ IND + IND + "<< \" Problem::setupIntegralPlotter\\n\"" + NL
        			+ IND + IND + "<< \"The hierarchy must be set before calling\\n\"" + NL
        			+ IND + IND + "<< \"this function.\\n\");" + NL
        			+ IND + "}" + NL
        			+ IND + "int i = 0;" + NL
        			+ IND + "for (vector<std::shared_ptr<IntegrateDataWriter> >::const_iterator it = plotters.begin(); it != plotters.end(); ++it) {" + NL
        			+ IND + IND + "std::shared_ptr<IntegrateDataWriter> plotter = *it;" + NL
        			+ IND + IND + "hier::VariableDatabase *vdb = hier::VariableDatabase::getDatabase();" + NL
        			+ IND + IND + "set<string> variables = d_integralVariables[i];" + NL
        			+ IND + IND + "for (set<string>::const_iterator it2 = variables.begin() ; it2 != variables.end(); ++it2) {" + NL
        			+ IND + IND + IND + "string var_to_register = *it2;" + NL
        			+ IND + IND + IND + "if (!(vdb->checkVariableExists(var_to_register))) {" + NL
        			+ IND + IND + IND + IND + "TBOX_ERROR(d_object_name << \": Variable selected for Integration not found:\" <<  var_to_register);" + NL
        			+ IND + IND + IND + "}" + NL
        			+ IND + IND + IND + "int var_id = vdb->getVariable(var_to_register)->getInstanceIdentifier();" + NL
        			+ IND + IND + IND + "plotter->registerPlotQuantity(var_to_register,\"SCALAR\",var_id);" + NL
        			+ IND + IND + "}" + NL
        			+ IND + IND + "i++;" + NL
        			+ IND + "}" + NL
        			+ IND + "return 0;" + NL
        			+ "}" + NL; 
        	pointPlotter = "/*" + NL
        			+ " * Set up external plotter to plot point data from this class." + NL
        			+ " * Register variables appropriate for plotting." + NL
        			+ " */" + NL
        			+ "int Problem::setupPointPlotter(vector<std::shared_ptr<PointDataWriter> > &plotters) const {" + NL
        			+ IND + "int i = 0;" + NL
        			+ IND + "for (vector<std::shared_ptr<PointDataWriter> >::const_iterator it = plotters.begin(); it != plotters.end(); ++it) {" + NL
        			+ IND + IND + "std::shared_ptr<PointDataWriter> plotter = *it;" + NL
        			+ IND + IND + "hier::VariableDatabase *vdb = hier::VariableDatabase::getDatabase();" + NL
        			+ IND + IND + "set<string> variables = d_pointVariables[i];" + NL
        			+ IND + IND + "for (set<string>::const_iterator it2 = variables.begin() ; it2 != variables.end(); ++it2) {" + NL
        			+ IND + IND + IND + "string var_to_register = *it2;" + NL
        			+ IND + IND + IND + "if (!(vdb->checkVariableExists(var_to_register))) {" + NL
        			+ IND + IND + IND + IND + "TBOX_ERROR(d_object_name << \": Variable selected for Point not found:\" <<  var_to_register);" + NL
        			+ IND + IND + IND + "}" + NL
        			+ IND + IND + IND + "int var_id = vdb->getVariable(var_to_register)->getInstanceIdentifier();" + NL
        			+ IND + IND + IND + "plotter->registerPlotQuantity(var_to_register,\"SCALAR\",var_id);" + NL
        			+ IND + IND + "}" + NL
        			+ IND + IND + "i++;" + NL
        			+ IND + "}" + NL
        			+ IND + "return 0;" + NL
        			+ "}" + NL; 
        }
        problem = problem.replaceFirst("#setupSimflownyPlotters#", meshPlotter + particlePlotter + slicePlotter + integrationPlotter + pointPlotter);
        
        //Remeshing
        String remeshing = "";
        if (pi.getX3dMovRegions() != null) {
            remeshing = IND + "int remesh[1] = {0};" + NL
                    + IND + "if (simPlat_iteration == 1) remesh[0] = 1;" + NL;
        }
        problem = problem.replaceFirst("#remeshing#", remeshing);
        //Shift time steps
        problem = problem.replaceFirst("#shift variables#", createShifts(fields, vectors, coordinates, pi));
        //Evolution step
        problem = problem.replaceFirst("#evolution step#", indent(executionStep, 1));
        //Analysis
        String analysisInstructions = "";
        if (!analysis.equals("")) {
        	String calculateAnalysisSlicer = "";
        	String calculateAnalysisIntegral = "";
        	String calculateAnalysisPoint = "";
        	if (activeSlicers) {
        		calculateAnalysisSlicer = IND + "for (int i = 0; i < next_slice_dump_iteration.size(); i++) {" + NL
					+ IND + IND + "if (analysis_slice_dump[i] && previous_iteration < next_slice_dump_iteration[i] && outputCycle >= next_slice_dump_iteration[i]) {" + NL
					+ IND + IND + IND + "calculate = true;" + NL
					+ IND + IND + "}" + NL
					+ IND + "}" + NL
					+ IND + "for (int i = 0; i < next_sphere_dump_iteration.size(); i++) {" + NL
					+ IND + IND + "if (analysis_sphere_dump[i] && previous_iteration < next_sphere_dump_iteration[i] && outputCycle >= next_sphere_dump_iteration[i]) {" + NL
					+ IND + IND + IND + "calculate = true;" + NL
					+ IND + IND + "}" + NL
					+ IND + "}" + NL;
        	}
            if (pi.isHasMeshFields() || pi.isHasABMMeshFields()) {
            	calculateAnalysisIntegral = IND + "for (int i = 0; i < next_integration_dump_iteration.size(); i++) {" + NL
            			+ IND + IND + "if (analysis_integration_dump[i] && previous_iteration < next_integration_dump_iteration[i] && outputCycle >= next_integration_dump_iteration[i]) {" + NL
            			+ IND + IND + IND + "calculate = true;" + NL
            			+ IND + IND + "}" + NL
            			+ IND + "}" + NL;
              	calculateAnalysisPoint = IND + "for (int i = 0; i < next_point_dump_iteration.size(); i++) {" + NL
            			+ IND + IND + "if (analysis_point_dump[i] && previous_iteration < next_point_dump_iteration[i] && outputCycle >= next_point_dump_iteration[i]) {" + NL
            			+ IND + IND + IND + "calculate = true;" + NL
            			+ IND + IND + "}" + NL
            			+ IND + "}" + NL;
            }
            analysisInstructions = IND + "//Analysis" + NL
                    + IND + "bool calculate = false;" + NL
                    + IND + "if (previous_iteration < next_mesh_dump_iteration && outputCycle >= next_mesh_dump_iteration) {" + NL
                    + IND + IND + "calculate = true;" + NL
                    + IND + "}" + NL
                    + calculateAnalysisSlicer
                    + calculateAnalysisIntegral
                    + calculateAnalysisPoint
                    + IND + "if (calculate) {" + NL
                    + indent(analysis, 2) + NL
                    + IND + "}" + NL;
        }
        problem = problem.replaceFirst("#analysis#", analysisInstructions);
        
        String analysisInit = "";
        if (!analysis.equals("")) {
            analysisInit = "void Problem::postInit(){" + NL
                    + IND + "double current_time = 0;" + NL 
                    + indent(analysis, 1)
                    + "}" + NL;
        }
        
        problem = problem.replaceFirst("#analysisInitialization#", analysisInit);
        //Finalization check
        problem = problem.replaceFirst("#finalization condition#", indent(finalization, 1));
        //Gradient detector
        problem = replaceRegridding(problem, dimensions, coordinates, pi);
        //Particles functions
        String particlesFunctions = "";
        if (pi.isHasABMMeshlessFields() || pi.isHasParticleFields()) {
        	particlesFunctions = createParticlesFunctions(pi, vi, movement);
        }
        problem = problem.replaceFirst("#particlesFunctions#", particlesFunctions);
        
        //Open external files
        String readTables = "";
        FileReadInformation fri = pi.getFileReadInfo();
        for (String fileName : fri.getFileNameInfo().keySet()) {
        	int nCoords = fri.getFileNameInfo().get(fileName);
        	String params = "";
            for (int k = 0; k < nCoords; k++) {
            	params = params + "&coord" + k + "_data_" + fileName + ", coord" + k + "_size_" + fileName + ", coord" + k + "_dx_" + fileName + ", ";
            }

            readTables = readTables + IND + "readTable(\"" + fileName + ".h5\", " + params + "&vars_data_" + fileName + ", vars_size_" + fileName + ");" + NL;
        }
        problem = problem.replaceFirst("#readTables#", readTables);
        
        String output = "";
        if (pi.isHasMeshFields() || pi.isHasABMMeshFields()) {
        	output = IND + "if (ln == hierarchy->getFinestLevelNumber() && viz_mesh_dump_interval > 0) {" + NL
				+ IND + IND + "if (previous_iteration < next_mesh_dump_iteration && outputCycle >= next_mesh_dump_iteration) {" + NL
				+ IND + IND + IND + "d_visit_data_writer->writePlotData(hierarchy, outputCycle, new_time);" + NL
				+ IND + IND + IND + "next_mesh_dump_iteration = next_mesh_dump_iteration + viz_mesh_dump_interval;" + NL
				+ IND + IND + IND + "while (outputCycle >= next_mesh_dump_iteration) {" + NL
				+ IND + IND + IND + IND + "next_mesh_dump_iteration = next_mesh_dump_iteration + viz_mesh_dump_interval;" + NL
				+ IND + IND + IND + "}" + NL
				+ IND + IND + "}" + NL
				+ IND + "}" + NL;
        }
        if (pi.isHasParticleFields() || pi.isHasABMMeshlessFields()) {
        	output = output + IND + "if (ln == hierarchy->getFinestLevelNumber() && viz_particle_dump_interval > 0) {" + NL
    				+ IND + IND + "if (previous_iteration < next_particle_dump_iteration && outputCycle >= next_particle_dump_iteration) {" + NL
    				+ IND + IND + IND + "d_particle_data_writer->writePlotData(d_grid_geometry, hierarchy, outputCycle, new_time);" + NL
    				+ IND + IND + IND + "next_particle_dump_iteration = next_particle_dump_iteration + viz_particle_dump_interval;" + NL
    				+ IND + IND + IND + "while (outputCycle >= next_particle_dump_iteration) {" + NL
    				+ IND + IND + IND + IND + "next_particle_dump_iteration = next_particle_dump_iteration + viz_particle_dump_interval;" + NL
    				+ IND + IND + IND + "}" + NL
    				+ IND + IND + "}" + NL
    				+ IND + "}" + NL;;
        }
        if (activeSlicers) {
        	output = output + IND + "//Slicer output" + NL
        			+ IND + "if (ln == hierarchy->getFinestLevelNumber() && d_slicer_output_period.size() > 0) {" + NL
			        + IND + IND + "int i = 0;" + NL
					+ IND + IND + "for (std::vector<std::shared_ptr<SlicerDataWriter> >::iterator it = d_sliceWriters.begin(); it != d_sliceWriters.end(); ++it) {" + NL
					+ IND + IND + IND + "if (d_slicer_output_period[i] > 0 && previous_iteration < next_slice_dump_iteration[i] && outputCycle >= next_slice_dump_iteration[i]) {" + NL
					+ IND + IND + IND + IND + "(*it)->writePlotData(hierarchy, outputCycle, new_time);" + NL
					+ IND + IND + IND + IND + "next_slice_dump_iteration[i] = next_slice_dump_iteration[i] + d_slicer_output_period[i];" + NL
					+ IND + IND + IND + IND + "while (outputCycle >= next_slice_dump_iteration[i]) {" + NL
					+ IND + IND + IND + IND + IND + "next_slice_dump_iteration[i] = next_slice_dump_iteration[i] + d_slicer_output_period[i];" + NL
					+ IND + IND + IND + IND + "}" + NL
					+ IND + IND + IND + "}" + NL
					+ IND + IND + IND + "i++;" + NL
					+ IND + IND + "}" + NL
					+ IND + "}" + NL + NL
					+ IND + "//Spherical output" + NL
					+ IND + "if (ln == hierarchy->getFinestLevelNumber() && d_sphere_output_period.size() > 0) {" + NL
					+ IND + IND + "int i = 0;" + NL
					+ IND + IND + "for (std::vector<std::shared_ptr<SphereDataWriter> >::iterator it = d_sphereWriters.begin(); it != d_sphereWriters.end(); ++it) {" + NL
					+ IND + IND + IND + "if (d_sphere_output_period[i] > 0 && previous_iteration < next_sphere_dump_iteration[i] && outputCycle >= next_sphere_dump_iteration[i]) {" + NL
					+ IND + IND + IND + IND + "(*it)->writePlotData(hierarchy, outputCycle, new_time);" + NL
					+ IND + IND + IND + IND + "next_sphere_dump_iteration[i] = next_sphere_dump_iteration[i] + d_sphere_output_period[i];" + NL
					+ IND + IND + IND + IND + "while (outputCycle >= next_sphere_dump_iteration[i]) {" + NL
					+ IND + IND + IND + IND + IND + "next_sphere_dump_iteration[i] = next_sphere_dump_iteration[i] + d_sphere_output_period[i];" + NL
					+ IND + IND + IND + IND + "}" + NL
					+ IND + IND + IND + "}" + NL
					+ IND + IND + IND + "i++;" + NL
					+ IND + IND + "}" + NL
					+ IND + "}" + NL + NL;
        }
        if (pi.isHasMeshFields()) {
        	output = output + IND + "//Integration output" + NL
        			+ IND + "if (ln == hierarchy->getFinestLevelNumber() && d_integration_output_period.size() > 0) {" + NL
        			+ IND + IND + "int i = 0;" + NL
        			+ IND + IND + "for (std::vector<std::shared_ptr<IntegrateDataWriter> >::iterator it = d_integrateDataWriters.begin(); it != d_integrateDataWriters.end(); ++it) {" + NL
        			+ IND + IND + IND + "if (d_integration_output_period[i] > 0 && previous_iteration < next_integration_dump_iteration[i] && outputCycle >= next_integration_dump_iteration[i]) {" + NL
        			+ IND + IND + IND + IND + "(*it)->writePlotData(hierarchy, outputCycle, new_time);" + NL
					+ IND + IND + IND + IND + "next_integration_dump_iteration[i] = next_integration_dump_iteration[i] + d_integration_output_period[i];" + NL
					+ IND + IND + IND + IND + "while (outputCycle >= next_integration_dump_iteration[i]) {" + NL
					+ IND + IND + IND + IND + IND + "next_integration_dump_iteration[i] = next_integration_dump_iteration[i] + d_integration_output_period[i];" + NL
					+ IND + IND + IND + IND + "}" + NL
        			+ IND + IND + IND + "}" + NL
        			+ IND + IND + IND + "i++;" + NL
        			+ IND + IND + "}" + NL	
        			+ IND + "}" + NL + NL
        			+ IND + "//Point output" + NL
         			+ IND + "if (ln == hierarchy->getFinestLevelNumber() && d_point_output_period.size() > 0) {" + NL
         			+ IND + IND + "int i = 0;" + NL
         			+ IND + IND + "for (std::vector<std::shared_ptr<PointDataWriter> >::iterator it = d_pointDataWriters.begin(); it != d_pointDataWriters.end(); ++it) {" + NL
         			+ IND + IND + IND + "if (d_point_output_period[i] > 0 && previous_iteration < next_point_dump_iteration[i] && outputCycle >= next_point_dump_iteration[i]) {" + NL
         			+ IND + IND + IND + IND + "(*it)->writePlotData(hierarchy, outputCycle, new_time);" + NL
 					+ IND + IND + IND + IND + "next_point_dump_iteration[i] = next_point_dump_iteration[i] + d_point_output_period[i];" + NL
 					+ IND + IND + IND + IND + "while (outputCycle >= next_point_dump_iteration[i]) {" + NL
 					+ IND + IND + IND + IND + IND + "next_point_dump_iteration[i] = next_point_dump_iteration[i] + d_point_output_period[i];" + NL
 					+ IND + IND + IND + IND + "}" + NL
         			+ IND + IND + IND + "}" + NL
         			+ IND + IND + IND + "i++;" + NL
         			+ IND + IND + "}" + NL	
         			+ IND + "}" + NL + NL;
        }
        problem = problem.replaceFirst("#output#", output);
        
        String writerVariablesParam = "";
        String dataWriterVariable = "";
        if (pi.isHasMeshFields() || pi.isHasABMMeshFields()) {
        	writerVariablesParam = ", const int mesh_output_period, const vector<string> full_mesh_writer_variables, std::shared_ptr<appu::VisItDataWriter>& mesh_data_writer";
        	dataWriterVariable = ", viz_mesh_dump_interval(mesh_output_period), d_full_mesh_writer_variables(full_mesh_writer_variables.begin(), full_mesh_writer_variables.end()), d_visit_data_writer(mesh_data_writer)";
        }
        if (pi.isHasParticleFields() || pi.isHasABMMeshlessFields()) {
        	writerVariablesParam = writerVariablesParam + ", const int particle_output_period, const vector<string> full_particle_writer_variables, std::shared_ptr<ParticleDataWriter>& particle_data_writer";
        	dataWriterVariable = dataWriterVariable + ", viz_particle_dump_interval(particle_output_period), d_full_particle_writer_variables(full_particle_writer_variables.begin(), full_particle_writer_variables.end()), d_particle_data_writer(particle_data_writer)";
        }
    	problem = problem.replace("#writerVariablesParam#", writerVariablesParam);
    	problem = problem.replaceAll("#DataWriterVariable#", dataWriterVariable);
        
        String getFromRestart = "";
        String putToRestart = "";
        if (activeSlicers) {
        	putToRestart = putToRestart + IND + "mrd.setNextSliceDumpIteration(next_slice_dump_iteration);" + NL
        		+ IND + "mrd.setNextSphereDumpIteration(next_sphere_dump_iteration);" + NL;
        	getFromRestart = getFromRestart + IND + "next_slice_dump_iteration = mrd.getNextSliceDumpIteration();" + NL
        		+ IND + "next_sphere_dump_iteration = mrd.getNextSphereDumpIteration();" + NL;
        }
        if (pi.isHasABMMeshFields() || pi.isHasMeshFields()) {
        	putToRestart = putToRestart + IND + "mrd.setNextMeshDumpIteration(next_mesh_dump_iteration);" + NL
        			+ IND + "mrd.setNextIntegrationDumpIteration(next_integration_dump_iteration);" + NL
        			+ IND + "mrd.setNextPointDumpIteration(next_point_dump_iteration);" + NL;
        	getFromRestart = getFromRestart + IND + "next_mesh_dump_iteration = mrd.getNextMeshDumpIteration();" + NL
        			+ IND + "next_integration_dump_iteration = mrd.getNextIntegrationDumpIteration();" + NL
        			+ IND + "next_point_dump_iteration = mrd.getNextPointDumpIteration();" + NL;
        }
        if (pi.isHasABMMeshlessFields() || pi.isHasParticleFields()) {
        	putToRestart = putToRestart + IND + "mrd.setNextParticleDumpIteration(next_particle_dump_iteration);" + NL;
        	getFromRestart = getFromRestart + IND + "next_particle_dump_iteration = mrd.getNextParticleDumpIteration();" + NL;
        }
        
        problem = problem.replaceAll("#putToRestart#", putToRestart);
        problem = problem.replaceAll("#getFromRestart#", getFromRestart);
        
        return problem;
    }
    
    /**
     * Replaces the tag #register variables for output#.
     * @param regions       The region identifiers
     * @return              The problem file
     */
    private String registerVariablesForOutput(ArrayList<String> regions) {         
        String initializeSet = "";
        String fovRegister = "";
        String originVariable = "";
        for (int i = 0; i < regions.size(); i++) {
            if (CodeGeneratorUtils.isNumber(regions.get(i))) {
                String var = "FOV_" + regions.get(i);
                fovRegister = fovRegister + IND + "plotter.registerPlotQuantity(\"" + var + "\",\"SCALAR\",d_" + var + "_id);" + NL;
            }
        }
        originVariable = "d_full_mesh_writer_variables";
        String outputRegister = initializeSet + fovRegister
                + IND + "hier::VariableDatabase *vdb = hier::VariableDatabase::getDatabase();" + NL
                + IND + "for (set<string>::const_iterator it = " + originVariable + ".begin() ; "
                + "it != " + originVariable + ".end(); ++it) {" + NL
                + IND + IND + "string var_to_register = *it;" + NL
                + IND + IND + "if (!(vdb->checkVariableExists(var_to_register))) {" + NL
                + IND + IND + IND + "TBOX_ERROR(d_object_name << \": Variable selected for 3D write not found:\" <<  var_to_register);" + NL
                + IND + IND + "}" + NL
                + IND + IND + "int var_id = vdb->getVariable(var_to_register)->getInstanceIdentifier();" + NL
                + IND + IND + "plotter.registerPlotQuantity(var_to_register,\"SCALAR\",var_id);" + NL
                + IND + "}" + NL;
        return outputRegister;
    }
    
    /**
     * Replaces the tag #register variables for output#.
     * @param problem       The problem file
     * @param regions       The region identifiers
     * @param dimensions    The dimensions of the problem
     * @param outputVars    The variables to use in the results
     * @param type			OutputType
     * @return              The problem file
     */
    private String registerVariablesForOutputParticles(Set<String> particleSpecies) {         

        String comparison = "";
        String register = "";
        
        for (String name : particleSpecies) {
        	comparison = comparison + "!(particleVariableList_" + name + ".end() != std::find(particleVariableList_" + name + ".begin(), particleVariableList_" + name + ".end(), var_to_register)) && ";
        	register = register + IND + IND + "if (particleVariableList_" + name + ".end() != std::find(particleVariableList_" + name + ".begin(), particleVariableList_" + name + ".end(), var_to_register)) {" +NL 
                    + IND + IND + IND + "plotter.registerPlotVariable<Particle_" + name + ">(var_to_register,d_particleVariables_" + name + "_id);" + NL
                    + IND + IND + "}" + NL;
        }
        comparison = comparison.substring(0, comparison.lastIndexOf(" &&"));
        
        return IND + IND + "if (" + comparison + ") {" + NL 
                + IND + IND + IND + "TBOX_ERROR(d_object_name << \": Variable selected for Particle write not found:\" <<  var_to_register);" + NL
                + IND + IND + "}" + NL
                + register;
    }
    
    /**
     * Creates the global variables code.
     * @param problem       The problem file
     * @param pi 			The problem information
     * @return              The problem file
     */
    private String replaceGlobalVariables(String problem, ProblemInfo pi) {
    	String globalVariables = "const gsl_rng_type * T;" + NL
                + "gsl_rng *r_var;" + NL;
    	if (pi.isHasABMMeshlessFields() || pi.isHasParticleFields()) {
    		globalVariables = globalVariables + "gsl_rng *r_map;" + NL
    				+ "std::default_random_engine generator;" + NL;
    	}
    	ArrayList<String> coords = pi.getCoordinates();

        if (pi.getX3dMovRegions() != null) {
            String dec = "";
            String cond1p1 = "";
            String cond1p2 = "";
            String closePar = "";
            String cond2 = "";
            String par1 = "";
            String par2 = "";
            String assing = ""; 
            for (int i = 0; i < coords.size(); i++) {
                String coord = coords.get(i);
                dec = dec + IND + "int " + coord + ", p" + coord + ";" + NL;
                
                cond1p1 = cond1p1 + coord + " < d." + coord + " || " + coord + " == d." + coord + " && (";
                closePar = closePar + ")";
                if (i == 0) {
                    cond1p2 = "p" + coord + " < d.p" + coord;
                }
                else {
                    cond1p2 = cond1p2 + "|| (p" + coords.get(i - 1) + " == d.p" + coords.get(i - 1) + " && p" + coord + " < d.p" + coord;
                    closePar = closePar + ")";
                }
                cond2 = cond2 + " && " + coord + " == d." + coord + " && p" + coord + " == d.p" + coord;
                par1 = par1 + "int " + coord + ", ";
                par2 = par2 + "int p" + coord + ", ";
                assing = assing + coord + "(" + coord + "),p" + coord + "(p" + coord + "),";
            }
            String cond1 = "(" + cond1p1 + cond1p2 + closePar + ")";
            
            globalVariables = globalVariables + "struct particle{" + NL
                    + dec
                    + IND + "double dist;" + NL
                    + IND + "bool operator<(const particle & d ) const {" + NL
                    + IND + IND + "return (fabs(dist) < fabs(d.dist) || (equalsEq(fabs(dist), fabs(d.dist)) && " + cond1 + "));" + NL
                    + IND + "}" + NL
                    + IND + "bool operator==( const particle & d ) const {" + NL
                    + IND + IND + "return (equalsEq(dist, d.dist)" + cond2 + ");" + NL
                    + IND + "}" + NL
                    + IND + "particle(" + par1 + par2 + "double d ) : " + assing + "dist(d) { }" + NL
                    + IND + "particle(){}" + NL
                    + "};" + NL + NL;
        }
        return problem.replaceFirst("#global variables#", globalVariables);
    }
     
    /**
     * Generate the variables from the parameter input file.
     * @param pi				Problem information
     * @param parameters        The parameters of the problem
     * @param contCoordinates   The continuous coordinates of the problem
     * @return                  The code
     * @throws CGException      SP001 Incorrect source code
     */
    private String getParameterDeclaration(ProblemInfo pi, HashMap<String, String> parameters, ArrayList<String> contCoordinates
            ) throws CGException {
        String parameterDeclaration = "";

        if (pi.isHasParticleFields() || pi.isHasABMMeshlessFields()) {
            parameterDeclaration = parameterDeclaration + IND + "const double* dx  = grid_geom->getDx();" + NL;
        }
        
        Iterator<String> param = parameters.keySet().iterator();
        while (param.hasNext()) {
            String name = param.next();
            String type = parameters.get(name);
            String typeFunction = getType(type);
            parameterDeclaration = parameterDeclaration + IND + name + " = database->" + typeFunction + "(\"" + name + "\");" + NL;
        }
        
        if (pi.isHasParticleFields()) {
            String radiusVars = IND + "influenceRadius = particles_common_db->getDouble(\"influenceRadius\");" + NL;
            parameterDeclaration = parameterDeclaration + IND + "std::shared_ptr<tbox::Database> particles_common_db(database->" 
                + "getDatabase(\"particles\"));" + NL;
            for (String speciesName: pi.getParticleSpecies()) {
            	parameterDeclaration = parameterDeclaration + IND + "std::shared_ptr<tbox::Database> " + speciesName + "_db(particles_common_db->getDatabase(\"" + speciesName + "\"));" + NL
            			+ IND + "particleDistribution_" + speciesName + " = " + speciesName + "_db->getString(\"particle_distribution\");" + NL
            			+ IND + "domain_offset_factor_" + speciesName + " = " + speciesName + "_db->getDoubleVector(\"domain_offset_factor\");" + NL
            			+ IND + "normal_mean_" + speciesName + " = " + speciesName + "_db->getDoubleVector(\"normal_mean\");" + NL
            			+ IND + "normal_stddev_" + speciesName + " = " + speciesName + "_db->getDoubleVector(\"normal_stddev\");" + NL
            			+ IND + "box_min_" + speciesName + " = " + speciesName + "_db->getDoubleVector(\"box_min\");" + NL
            			+ IND + "box_max_" + speciesName + " = " + speciesName + "_db->getDoubleVector(\"box_max\");" + NL
            			+ IND + "number_of_particles_" + speciesName + " = " + speciesName + "_db->getDoubleVector(\"number_of_particles\");" + NL;
            	String offsetCondition = "";
            	for (int i = 0; i < contCoordinates.size(); i++) {
            		offsetCondition = offsetCondition + "domain_offset_factor_" + speciesName + "[" + i + "] < 0 || domain_offset_factor_" + speciesName + "[" + i + "] > 1 || ";
            		parameterDeclaration = parameterDeclaration + IND + "particleSeparation_" + speciesName + ".push_back((grid_geom->getXUpper()[" + i + "] - grid_geom->getXLower()[" + i + "])/number_of_particles_" + speciesName + "[" + i + "]);" + NL;
            	}
            	offsetCondition = offsetCondition.substring(0, offsetCondition.lastIndexOf("||"));
        		parameterDeclaration = parameterDeclaration +  IND + "if (" + offsetCondition + ") {" + NL
        				+ IND + IND + "TBOX_ERROR(\"Problem::Problem\"" + NL
        				+ IND + IND + IND + "<< \"n      Error in parameter input:\"" + NL
        				+ IND + IND + IND + "<< \"n      domain_offset_factor_" + speciesName + " must be between [0, 1]\"<< std::endl);" + NL
        				+ IND + "}" + NL;
            }
            for (int i = 0; i < contCoordinates.size(); i++) {
                String coord = contCoordinates.get(i);
                String totalNumberOfParticles = "";
                for (String speciesName: pi.getParticleSpecies()) {
                	totalNumberOfParticles = totalNumberOfParticles + "number_of_particles_" + speciesName + "[" + i + "] + ";
                }
                totalNumberOfParticles = totalNumberOfParticles.substring(0, totalNumberOfParticles.lastIndexOf(" +"));
                if (pi.getParticleSpecies().size() > 1) {
                	totalNumberOfParticles = "(" + totalNumberOfParticles + ") / " + pi.getParticleSpecies().size() + ";" + NL;
                }
                parameterDeclaration = parameterDeclaration + IND + "int number_of_particles_" + coord + " = " + totalNumberOfParticles + ";" + NL
            		+ IND + "particleSeparation_" + coord + " = (grid_geom->getXUpper()[" + i + "] - grid_geom->getXLower()[" + i + "])/number_of_particles_" + coord  + ";" + NL
                    + IND + "if (particleSeparation_" + coord  + " <= 0) {" + NL
                    + IND + IND + "TBOX_ERROR(\"Problem::Problem\"" + NL
                    + IND + IND + IND + "<< \"\\n      Error in parameter input:\"" + NL
                    + IND + IND + IND + "<< \"\\n      number_of_particles_" + coord  + " must be greater than 0\"<< std::endl);" + NL
                    + IND + "}" + NL;
            }
            parameterDeclaration = parameterDeclaration + radiusVars;
        }
        if (pi.isHasABMMeshlessFields()) {
            String smoothVars = IND + "influenceRadius = particles_common_db->getDouble(\"influenceRadius\");" + NL
                    + IND + "if (influenceRadius <= 0) {" + NL
                    + IND + IND + "TBOX_ERROR(\"Problem::Problem\"" + NL
                    + IND + IND + IND + "<< \"\\n      Error in parameter input:\"" + NL
                    + IND + IND + IND + "<< \"\\n      influenceRadius must be greater than 0\"<< std::endl);" + NL
                    + IND + "}" + NL;
            parameterDeclaration = parameterDeclaration + IND + "std::shared_ptr<tbox::Database> particles_common_db(database->" 
                + "getDatabase(\"particles\"));" + NL
                + IND + "density_par = particles_common_db->getDouble(\"density_par\");" + NL
                + IND + "if (density_par <= 0) {" + NL
                + IND + IND + "TBOX_ERROR(\"Problem::Problem\"" + NL
                + IND + IND + IND + "<< \"\\n      Error in parameter input:\"" + NL
                + IND + IND + IND + "<< \"\\n      density_par must be greater than 0\"<< std::endl);" + NL
                + IND + "}" + NL
                + IND + "particleDistribution = particles_common_db->getString(\"particle_distribution\");" + NL
                + smoothVars;
            for (int i = 0; i < contCoordinates.size(); i++) {
                String coord = contCoordinates.get(i);
                parameterDeclaration = parameterDeclaration + IND + "particleSeparation_" + coord + " = dx[" + i + "]/density_par;" + NL;
            }
        }
        parameterDeclaration = parameterDeclaration + IND + "//Random initialization" + NL
                + IND + "gsl_rng_env_setup();" + NL;
        parameterDeclaration = parameterDeclaration + IND + "//Random for simulation" + NL
                + IND + "const tbox::SAMRAI_MPI& mpi(tbox::SAMRAI_MPI::getSAMRAIWorld());" + NL
                + IND + "int random_seed = database->getDouble(\"random_seed\")*(mpi.getRank() + 1);" + NL
                + IND + "r_var = gsl_rng_alloc(gsl_rng_ranlxs0);" + NL
                + IND + "gsl_rng_set(r_var, random_seed);" + NL;
        if (pi.isHasABMMeshlessFields() || pi.isHasParticleFields()) {
            parameterDeclaration = parameterDeclaration + IND + "//Random for mapping pursposes" + NL
                    + IND + "r_map = gsl_rng_alloc(gsl_rng_ranlxs0);" + NL
                    + IND + "gsl_rng_set(r_map, database->getDouble(\"random_seed\"));" + NL;
        }

        String initSlicers = "";
        String initIntegration = "";
        String initPoint = "";
        String rearrangOutput = "";
        if (activeSlicers) {
        	initSlicers = IND + IND + "for (std::vector<int>::iterator it = d_slicer_output_period.begin(); it != d_slicer_output_period.end(); ++it) {" + NL
            		+ IND + IND + IND + "next_slice_dump_iteration.push_back((*it));" + NL
            		+ IND + IND + "}" + NL
            		+ IND + IND + "for (std::vector<int>::iterator it = d_sphere_output_period.begin(); it != d_sphere_output_period.end(); ++it) {" + NL
            		+ IND + IND + IND + "next_sphere_dump_iteration.push_back((*it));" + NL
            		+ IND + IND + "}" + NL;
          	rearrangOutput = rearrangOutput + IND + IND + "if (d_slicer_output_period.size() < next_slice_dump_iteration.size()) {" + NL
          			+ IND + IND + IND + "TBOX_ERROR(\"Number of slices cannot be reduced after a checkpoint.\");" + NL
          			+ IND + IND + "}" + NL
          			+ IND + IND + "for (int il = 0; il < d_slicer_output_period.size(); il++) {" + NL
          			+ IND + IND + IND + "if (il >= next_slice_dump_iteration.size()) {" + NL
          			+ IND + IND + IND + IND + "next_slice_dump_iteration.push_back(0);" + NL
          			+ IND + IND + IND + "}" + NL
          			+ IND + IND + IND + "if (next_slice_dump_iteration[il] == 0 && d_slicer_output_period[il] > 0) {" + NL
          			+ IND + IND + IND + IND + "next_slice_dump_iteration[il] = current_iteration[d_patch_hierarchy->getNumberOfLevels() - 1] + d_slicer_output_period[il];" + NL
          			+ IND + IND + IND + "}" + NL
          			+ IND + IND + "}" + NL
          			+ IND + IND + "if (d_sphere_output_period.size() < next_sphere_dump_iteration.size()) {" + NL
          			+ IND + IND + IND + "TBOX_ERROR(\"Number of spheres cannot be reduced after a checkpoint.\");" + NL
          			+ IND + IND + "}" + NL
          			+ IND + IND + "for (int il = 0; il < d_sphere_output_period.size(); il++) {" + NL
          			+ IND + IND + IND + "if (il >= next_sphere_dump_iteration.size()) {" + NL
          			+ IND + IND + IND + IND + "next_sphere_dump_iteration.push_back(0);" + NL
          			+ IND + IND + IND + "}" + NL
          			+ IND + IND + IND + "if (next_sphere_dump_iteration[il] == 0 && d_sphere_output_period[il] > 0) {" + NL
          			+ IND + IND + IND + IND + "next_sphere_dump_iteration[il] = current_iteration[d_patch_hierarchy->getNumberOfLevels() - 1] + d_sphere_output_period[il];" + NL
          			+ IND + IND + IND + "}" + NL
          			+ IND + IND + "}" + NL;
        }
        String initDumps = "";
        if (pi.isHasABMMeshFields() || pi.isHasMeshFields()) {
        	initIntegration = IND + IND + "for (std::vector<int>::iterator it = d_integration_output_period.begin(); it != d_integration_output_period.end(); ++it) {" + NL
            		+ IND + IND + IND + "next_integration_dump_iteration.push_back((*it));" + NL
            		+ IND + IND + "}" + NL;
        	initPoint = IND + IND + "for (std::vector<int>::iterator it = d_point_output_period.begin(); it != d_point_output_period.end(); ++it) {" + NL
            		+ IND + IND + IND + "next_point_dump_iteration.push_back((*it));" + NL
            		+ IND + IND + "}" + NL;
         	rearrangOutput = rearrangOutput + IND + IND + "if (d_integration_output_period.size() < next_integration_dump_iteration.size()) {" + NL
         			+ IND + IND + IND + "TBOX_ERROR(\"Number of integrations cannot be reduced after a checkpoint.\");" + NL
         			+ IND + IND + "}" + NL
         			+ IND + IND + "for (int il = 0; il < d_integration_output_period.size(); il++) {" + NL
         			+ IND + IND + IND + "if (il >= next_integration_dump_iteration.size()) {" + NL
         			+ IND + IND + IND + IND + "next_integration_dump_iteration.push_back(0);" + NL
         			+ IND + IND + IND + "}" + NL
         			+ IND + IND + IND + "if (next_integration_dump_iteration[il] == 0 && d_integration_output_period[il] > 0) {" + NL
         			+ IND + IND + IND + IND + "next_integration_dump_iteration[il] = current_iteration[d_patch_hierarchy->getNumberOfLevels() - 1] + d_integration_output_period[il];" + NL
         			+ IND + IND + IND + "}" + NL
         			+ IND + IND + "}" + NL
         			+ IND + IND + "if (d_point_output_period.size() < next_point_dump_iteration.size()) {" + NL
          			+ IND + IND + IND + "TBOX_ERROR(\"Number of point output cannot be reduced after a checkpoint.\");" + NL
          			+ IND + IND + "}" + NL
          			+ IND + IND + "for (int il = 0; il < d_point_output_period.size(); il++) {" + NL
          			+ IND + IND + IND + "if (il >= next_point_dump_iteration.size()) {" + NL
          			+ IND + IND + IND + IND + "next_point_dump_iteration.push_back(0);" + NL
          			+ IND + IND + IND + "}" + NL
          			+ IND + IND + IND + "if (next_point_dump_iteration[il] == 0 && d_point_output_period[il] > 0) {" + NL
          			+ IND + IND + IND + IND + "next_point_dump_iteration[il] = current_iteration[d_patch_hierarchy->getNumberOfLevels() - 1] + d_point_output_period[il];" + NL
          			+ IND + IND + IND + "}" + NL
          			+ IND + IND + "}" + NL;
         	initDumps = IND + IND + "next_mesh_dump_iteration = viz_mesh_dump_interval;" + NL;
        }
        if (pi.isHasParticleFields() || pi.isHasABMMeshlessFields()) {
         	initDumps = initDumps + IND + IND + "next_particle_dump_iteration = viz_particle_dump_interval;" + NL;
        }
        
        return parameterDeclaration
        		+ IND + "for (int il = 0; il < d_patch_hierarchy->getMaxNumberOfLevels(); il++) {" + NL
        		+ IND + IND + "bo_substep_iteration.push_back(0);" + NL
        		+ IND + "}" + NL
        		+ IND + "//Initialization from input file" + NL
        		+ IND + "if (!d_init_from_restart) {" + NL
        		+ initDumps
        		+ initSlicers
        		+ initIntegration
        		+ initPoint
        		+ NL
        		+ IND + IND + "//Iteration counter" + NL
        		+ IND + IND + "for (int il = 0; il < d_patch_hierarchy->getMaxNumberOfLevels(); il++) {" + NL
        		+ IND + IND + IND + "current_iteration.push_back(0);" + NL
        		+ IND + IND + "}" + NL
        		+ IND + "}" + NL
        		+ IND + "//Initialization from restart file" + NL
        		+ IND + "else {" + NL
        		+ IND + IND + "getFromRestart(mrd);" + NL
           		+ rearrangOutput
        		+ IND + "}" + NL;
    }
    
    /**
     * Create code for shifting the fields.
     * @param fields           	 	The fields
     * @param vectors           	The vectors
     * @param coordinates       	The spacial coordinates
     * @param pi 					The problem information
     * @return                  	The shift variable block 
     * @throws CGException 
     */
    private String createShifts(HashMap<String, String> fields, HashMap<String, String> vectors, ArrayList<String> coordinates, 
    		ProblemInfo pi) throws CGException {
    	LinkedHashMap<String, Integer> positionTimeLevels = pi.getPositionTimeLevels();
    	TimeInterpolationInfo ti = pi.getTimeInterpolationInfo();
        String shiftVarDeclaration = "";
        String timeSetting = "";
        if (pi.isHasMeshFields() || pi.isHasABMMeshFields()) {
        	ArrayList<String> added = new ArrayList<String>();
            Iterator<String> fieldIt = fields.keySet().iterator();
            String name = "";
            while (fieldIt.hasNext()) {
                name = fieldIt.next();
                if (!CodeGeneratorUtils.isAuxiliaryField(pi, name) && !pi.getVariableInfo().getAllParticleVariables().contains(name)) {
	                int timeSteps = Integer.parseInt(fields.get(name));
	                shiftVarDeclaration = shiftVarDeclaration + IND + IND + "std::shared_ptr< pdat::NodeData<double> > " + name 
	                    + "(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_" + name + "_id)));" + NL;
	                added.add(name);
	                for (int i = 1; i < timeSteps; i++) {
	                    name = name + "_p";
	                    shiftVarDeclaration = shiftVarDeclaration + IND + IND + "std::shared_ptr< pdat::NodeData<double> > " 
	                        + name + "(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_" + name + "_id)));" + NL;
	                    added.add(name);
	                }
                }
            }
            Iterator<String> vectorIt = vectors.keySet().iterator();
            while (vectorIt.hasNext()) {
                name = vectorIt.next();
                int timeSteps = Integer.parseInt(vectors.get(name));
                if (timeSteps > 1) {
                    shiftVarDeclaration = shiftVarDeclaration + IND + IND + "std::shared_ptr< pdat::NodeData<double> > " + name 
                            + "(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_" + name + "_id)));" + NL;
                    added.add(name);
                }
                for (int i = 1; i < timeSteps; i++) {
                    name = name + "_p";
                    shiftVarDeclaration = shiftVarDeclaration + IND + IND + "std::shared_ptr< pdat::NodeData<double> > " 
                        + name + "(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_" + name + "_id)));" + NL;
                    added.add(name);
                }
            }
            for (TimeInterpolator timeInterp: ti.getTimeInterpolators().values()) {
                for (int i = 0; i < timeInterp.getVarAtStep().size(); i++) {
                	ArrayList<String> stepVariables = timeInterp.getStepVariables(pi, i, pi.getVariableInfo().getMeshVariables());
                	for (int j = 0; j < stepVariables.size(); j++) {
                		String variable = stepVariables.get(j);
                     	if (!added.contains(variable)) {
                     		shiftVarDeclaration = shiftVarDeclaration + IND + IND + "std::shared_ptr< pdat::NodeData<double> > " + variable 
                                    + "(SAMRAI_SHARED_PTR_CAST<pdat::NodeData<double>, hier::PatchData>(patch->getPatchData(d_" + variable + "_id)));" + NL;
                            added.add(name);
                            added.add(variable);
                     	}
                   	}
                	
                }
            }
        }
        
        String shift = "";
        String index = "";
        String timeInterpolationDx = "";
        if (pi.isHasParticleFields() || pi.isHasABMMeshlessFields()) {
            String currentIndent = IND + IND;
            String loop = "";
            String endLoop = "";
            String tmpdx = "";
            for (int i = coordinates.size() - 1; i >= 0; i--) {
                loop = loop + currentIndent + "for (int index" + i + " = boxfirst(" + i + "); index" + i + " <= boxlast(" + i + "); index" 
                    + i + "++) {" + NL;
                endLoop = currentIndent + "}" + NL + endLoop;
                currentIndent = currentIndent + IND;
                index = ", index" + i + index;
                tmpdx = IND + IND + "tmpdx[" + i + "] = dx[" + i + "] * hierarchy->getRatioToCoarserLevel(ln)[" + i + "];" + NL + tmpdx;
            }
            index = index.substring(2);
            String speciesShift = "";
            for (Entry<String, ArrayList<String>> entry : pi.getVariableInfo().getParticleFields().entrySet()) {
            	String speciesName = entry.getKey();
            	
                shiftVarDeclaration = shiftVarDeclaration + IND + IND + "std::shared_ptr< pdat::IndexData<Particles<Particle_" + speciesName + ">, pdat::CellGeometry > > "
                        + "particleVariables_" + speciesName + "(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles<Particle_" + speciesName + ">, pdat::CellGeometry >, hier::PatchData>(patch->getPatchData(d_particleVariables_" + speciesName + "_id)));" + NL;
            	
                String fieldShift = "";
                for (String name: pi.getVariableInfo().getParticleFields().get(speciesName)) {
                	String var = SAMRAIUtils.variableNormalize(name);
	                int timeSteps = pi.getFieldTimeLevels().get(name);
	                for (int i = 1; i < timeSteps; i++) {
	                    fieldShift = fieldShift + currentIndent + IND + "particle->" + var  + "_p = particle->" + var + ";" + NL;
	                    var = var + "_p";
	                }
                }
                String positionShift = "";
                String name = "";
                Iterator<String> positionIt = positionTimeLevels.keySet().iterator();
                while (positionIt.hasNext()) {
                    name = positionIt.next();
                    int timeSteps = positionTimeLevels.get(name);
                    for (int i = 1; i < timeSteps; i++) {
                        positionShift = positionShift + currentIndent + IND + "particle->" + name  + "_p = particle->" + name + ";" + NL;
                        name = name + "_p";
                    }
                }
                String particleTimeSetting = "";
                if (ti.getTimeInterpolators().size() > 0) {
	                for (TimeInterpolator timeInterp: ti.getTimeInterpolators().values()) {
	               		particleTimeSetting = particleTimeSetting + currentIndent + "part_" + speciesName + "->setTime_p(current_time);" + NL;
	                    for (int i = 0; i < timeInterp.getVarAtStep().size(); i++) {
	                    	float factor = timeInterp.getTimestepFactor().get(i);
	                      	String timeIncrement = "";
	                      	if (factor != 0 && factor != 1) {
	                    		timeIncrement = " + simPlat_dt * " + factor;
	                    	}
	                    	if (factor == 1) {
	                    		timeIncrement = " + simPlat_dt";
	                    	}
	                    	String variable = SAMRAIUtils.xmlTransform(timeInterp.getVarAtStep().get(i), null);
	                    	variable = variable.substring(0, variable.indexOf("("));
	                   		particleTimeSetting = particleTimeSetting + currentIndent + "part_" + speciesName + "->setTime" + variable + "(current_time" + timeIncrement + ");" + NL;
	                    }
	                }
                }
                else {
               		particleTimeSetting = currentIndent + "part_" + speciesName + "->setTime_p(current_time);" + NL
               			 + currentIndent + "part_" + speciesName + "->setTime(new_time);" + NL;
                }
            	speciesShift = speciesShift + currentIndent + "Particles<Particle_" + speciesName + ">* part_" + speciesName + " = particleVariables_" + speciesName + "->getItem(idx);" + NL
                        + particleTimeSetting                
                        + currentIndent + "for (int pit = part_" + speciesName + "->getNumberOfParticles() - 1; pit >= 0; pit--) {" + NL
                        + currentIndent + IND + "Particle_" + speciesName + "* particle = part_" + speciesName + "->getParticle(pit);" + NL
                        + fieldShift + positionShift
                        + currentIndent + "}" + NL;
            }
            shiftVarDeclaration = shiftVarDeclaration + IND + IND + "const hier::Index boxfirst = particleVariables_" + pi.getVariableInfo().getParticleFields().keySet().toArray()[0] + "->getGhostBox().lower();" + NL
                    + IND + IND + "const hier::Index boxlast  = particleVariables_" + pi.getVariableInfo().getParticleFields().keySet().toArray()[0] + "->getGhostBox().upper();" + NL;
            
            shift = loop
                + currentIndent + "hier::Index idx(" + index + ");" + NL
                + speciesShift
                + endLoop;
            timeInterpolationDx = IND + IND + "//Get delta spaces into an array. dx, dy, dz." + NL
            		+ IND + IND + "std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));" + NL
            		+ IND + IND + "const double* dx  = patch_geom->getDx();" + NL
            		+ IND + IND + "double* tmpdx = new double[" + pi.getDimensions() + "];" + NL
            		+ tmpdx;
            for (String speciesName: pi.getParticleSpecies()) {
            	 timeInterpolationDx = timeInterpolationDx + IND + IND + "time_interpolate_operator_particles_" + speciesName + "->setDx(tmpdx);" + NL;
            }
       	 	timeInterpolationDx = timeInterpolationDx + IND + IND + "delete[] tmpdx;" + NL;
        }
        if (pi.isHasMeshFields() || pi.isHasABMMeshFields()) {
            Iterator<String> fieldIt = fields.keySet().iterator();
            String name = "";
            while (fieldIt.hasNext()) {
                name = fieldIt.next();
                if (!CodeGeneratorUtils.isAuxiliaryField(pi, name) && !pi.getVariableInfo().getAllParticleVariables().contains(name)) {
	                if (pi.getTimeInterpolationInfo().isTimeInterpolable(name, pi)) {
	                	timeSetting = timeSetting + IND + IND + name + "_p->setTime(current_time);" + NL;
	                }
	                int timeSteps = Integer.parseInt(fields.get(name));
	                for (int i = 1; i < timeSteps; i++) {
	                    shift = IND + IND + name + "_p->copy(*" + name + ");" + NL + shift;
	                    name = name + "_p";
	                }
	            }
            }
            Iterator<String> vectorIt = vectors.keySet().iterator();
            while (vectorIt.hasNext()) {
                name = vectorIt.next();
                int timeSteps = Integer.parseInt(vectors.get(name));
                for (int i = 1; i < timeSteps; i++) {
                    shift = IND + IND + name + "_p->copy(*" + name + ");" + NL + shift;
                    name = name + "_p";
                }
            }
            for (TimeInterpolator timeInterp: ti.getTimeInterpolators().values()) {
                for (int i = 0; i < timeInterp.getVarAtStep().size(); i++) {
                	ArrayList<String> stepVariables = timeInterp.getStepVariables(pi, i, pi.getVariableInfo().getMeshVariables());
                	float factor = timeInterp.getTimestepFactor().get(i);
                  	String timeIncrement = "";
                	if (factor != 0 && factor != 1) {
                		timeIncrement = " + simPlat_dt * " + factor;
                	}
                	if (factor == 1) {
                		timeIncrement = " + simPlat_dt";
                	}
                	for (int j = 0; j < stepVariables.size(); j++) {
                     	timeSetting = timeSetting + IND + IND + stepVariables.get(j) + "->setTime(current_time" + timeIncrement + ");" + NL;	
                	}
                	
                }
            }
        }
        return IND + "for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {" + NL
            + IND + IND + "const std::shared_ptr<hier::Patch > patch = *p_it;" + NL
            + shiftVarDeclaration
            + timeInterpolationDx
            + shift
            + timeSetting
            + IND + "}";
    }
    
    /**
     * Create the macros for the vector. Only if a fvm code.
     * @param dimensions        The spatial dimensions
     * @param coords            The spatial coordinates
     * @param pi 				The problem information
     * @return                  The code
     */
    private String createVectorMacros(int dimensions, ArrayList<String> coords, ProblemInfo pi) {
        String vectorMacro = "";
        if (dimensions == 2) {
            vectorMacro = "#define vector(v, i, j) (v)[i+" + coords.get(0) + "last*(j)]" + NL
            	+ "#define vectorCell(v, i, j) (v)[i+(" + coords.get(0) + "last - 1)*(j)]" + NL
                + "#define vectorT(v, i, j) (v)[i+" + coords.get(0) + "tlast*(j)]" + NL
                + "#define index" + coords.get(0) + "Of(i) (i - ((int)(i/(boxlast(0)-boxfirst(0)+1))*(boxlast(0)-boxfirst(0)+1)))" + NL
                + "#define index" + coords.get(1) + "Of(i) ((int)(i/(boxlast(0)-boxfirst(0)+1)))" + NL
                + "#define " + pi.getContCoordinates().get(0) + "coord(i) (((d_grid_geometry->getXLower()[0] + ((i + boxfirst(0)) - d_ghost_width) * dx[0])))" + NL
                + "#define " + pi.getContCoordinates().get(1) + "coord(j) (((d_grid_geometry->getXLower()[1] + ((j + boxfirst(1)) - d_ghost_width) * dx[1])))" + NL;
        }
        else {
            vectorMacro = "#define vector(v, i, j, k) (v)[i+" + coords.get(0) + "last*(j+" + coords.get(1) + "last*(k))]" + NL
            	+ "#define vectorCell(v, i, j, k) (v)[i+(" + coords.get(0) + "last - 1)*(j+(" + coords.get(1) + "last - 1)*(k))]" + NL
                + "#define vectorT(v, i, j, k) (v)[i+" + coords.get(0) + "tlast*(j+" + coords.get(1) + "tlast*(k))]" + NL
                + "#define index" + coords.get(0) + "Of(i) (i - (int)(i/((boxlast(0)-boxfirst(0)+1)*(boxlast(1)-boxfirst(1)+1)))*(("
                + "boxlast(0)-boxfirst(0)+1)*(boxlast(1)-boxfirst(1)+1)) - (int)((i - (int)(i/((boxlast(0)-boxfirst(0)+1)*("
                + "boxlast(1)-boxfirst(1)+1)))*((boxlast(0)-boxfirst(0)+1)*(boxlast(1)-boxfirst(1)+1)))/(boxlast(0)-boxfirst(0)+1))*("
                + "boxlast(0)-boxfirst(0)+1))" + NL
                + "#define index" + coords.get(1) + "Of(i) ((int)(i - (int)((i/((boxlast(0)-boxfirst(0)+1)*(boxlast(1)-boxfirst(1)+1)))*"
                + "((boxlast(0)-boxfirst(0)+1)*(boxlast(1)-boxfirst(1)+1)))/(boxlast(0)-boxfirst(0)+1)))" + NL
                + "#define index" + coords.get(2) + "Of(i) ((int)(i/((boxlast(0)-boxfirst(0)+1)*(boxlast(1)-boxfirst(1)+1))))" + NL
                + "#define " + pi.getContCoordinates().get(0) + "coord(i) (((d_grid_geometry->getXLower()[0] + ((i + boxfirst(0)) - d_ghost_width) * dx[0])))" + NL
                + "#define " + pi.getContCoordinates().get(1) + "coord(j) (((d_grid_geometry->getXLower()[1] + ((j + boxfirst(1)) - d_ghost_width) * dx[1])))" + NL
                + "#define " + pi.getContCoordinates().get(2) + "coord(k) (((d_grid_geometry->getXLower()[2] + ((k + boxfirst(2)) - d_ghost_width) * dx[2])))" + NL;
        }
        if (pi.getX3dMovRegions() != null) {
            if (dimensions == 2) {
                vectorMacro = vectorMacro + "#define vectorP_r(v, sop, k, l) (sop->v[k][l])" + NL
                           + "#define vectorP(v, i, j, p1, p2) ((v)[p1][p2])[i+" + coords.get(0) + "last*(j)]" + NL;
            }
            else {
                vectorMacro = vectorMacro + "#define vectorP_r(v, sop, k, l, m) (sop->v[k][l][m])" + NL
                    + "#define vectorP(v, i, j, k, p1, p2, p3) ((v)[p1][p2][p3])[i+" + coords.get(0) + "last*(j+" + coords.get(1) 
                    + "last*(k))]" + NL;
            }
        }
        return vectorMacro;
    }
    
    /**
     * Gets the code of the type.
     * @param type              Type given
     * @return                  Code call
     * @throws CGException      SP001 Incorrect source code
     */
    private String getType(String type) throws CGException {
        String typeFunction = "";
        if (type.toUpperCase().equals("DOUBLE")) {
            typeFunction = "getDouble";
        } 
        if (type.toUpperCase().equals("INT")) {
            typeFunction = "getInteger";
        }
        if (type.toUpperCase().equals("STRING")) {
            typeFunction = "getString";
        }
        if (type.toUpperCase().equals("BOOLEAN")) {
            typeFunction = "getBool";
        }
        if (typeFunction.equals("")) {
            throw new CGException(CGException.CG001, "Parameter type not valid.");
        }
        return typeFunction;
    }
    
    /**
     * Add the regridding definition to the code. Only if it is a fvm code.
     * @param problem           The Problem.cpp code
     * @param pi				Problem information
     * @return                  The Problem.cpp modified
     */
    private String replaceRegridding(String problem, ProblemInfo pi) {
    	String regridding = "";
        if (pi.isHasMeshFields() || pi.isHasParticleFields() || pi.isHasABMMeshFields()) {
        	regridding = IND + "//Regridding options" + NL
                    + IND + "d_regridding = false;" + NL
                    + IND + "if (database->isDatabase(\"regridding\")) {" + NL
                    + IND + IND + "regridding_db = database->getDatabase(\"regridding\");" + NL
                    + IND + IND + "d_regridding_buffer = regridding_db->getDouble(\"regridding_buffer\");" + NL
                    + IND + IND + "int smallest_patch_size = d_patch_hierarchy->getSmallestPatchSize(0).min();" + NL
                    + IND + IND + "for (int il = 1; il < d_patch_hierarchy->getMaxNumberOfLevels(); il++) {" + NL
                    + IND + IND + IND + "smallest_patch_size = MIN(smallest_patch_size, d_patch_hierarchy->getSmallestPatchSize(il).min());" + NL
                    + IND + IND + "}" + NL
                    + IND + IND + "if (d_regridding_buffer > smallest_patch_size) {" + NL
                    + IND + IND + IND + "TBOX_ERROR(\"Error: Regridding_buffer parameter (\"<<d_regridding_buffer<<\") cannot be greater than smallest_patch_size minimum value(\"<<smallest_patch_size<<\")\");" + NL
                    + IND + IND + "}" + NL
                    + IND + IND + "if (regridding_db->isString(\"regridding_type\")) {" + NL
                    + IND + IND + IND + "d_regridding_type = regridding_db->getString(\"regridding_type\");" + NL
                    + IND + IND + IND + "d_regridding_min_level = regridding_db->getInteger(\"regridding_min_level\");" + NL
                    + IND + IND + IND + "d_regridding_max_level = regridding_db->getInteger(\"regridding_max_level\");" + NL
                    + IND + IND + IND + "if (d_regridding_type == \"GRADIENT\") {" + NL
                    + IND + IND + IND + IND + "d_regridding_field = regridding_db->getString(\"regridding_field\");" + NL
                    + IND + IND + IND + IND + "d_regridding_compressionFactor = regridding_db->getDouble(\"regridding_compressionFactor\");" + NL
                    + IND + IND + IND + IND + "d_regridding_mOffset = regridding_db->getDouble(\"regridding_mOffset\");" + NL
                    + IND + IND + IND + IND + "d_regridding = true;" + NL
                    + IND + IND + IND + "} else {" + NL
                    + IND + IND + IND + IND + "if (d_regridding_type == \"FUNCTION\") {" + NL
                    + IND + IND + IND + IND + IND + "d_regridding_field = regridding_db->getString(\"regridding_function_field\");" + NL
                    + IND + IND + IND + IND + IND + "d_regridding_threshold = regridding_db->getDouble(\"regridding_threshold\");" + NL
                    + IND + IND + IND + IND + IND + "d_regridding = true;" + NL
                    + IND + IND + IND + IND + "} else {" + NL
                    + IND + IND + IND + IND + IND + "if (d_regridding_type == \"SHADOW\") {" + NL
                    + IND + IND + IND + IND + IND + IND + "std::string* fields = new std::string[2];" + NL
                    + IND + IND + IND + IND + IND + IND + "regridding_db->getStringArray(\"regridding_fields\", fields, 2);" + NL
                    + IND + IND + IND + IND + IND + IND + "d_regridding_field = fields[0];" + NL
                    + IND + IND + IND + IND + IND + IND + "d_regridding_field_shadow = fields[1];" + NL
                    + IND + IND + IND + IND + IND + IND + "d_regridding_error = regridding_db->getDouble(\"regridding_error\");" + NL
                    + IND + IND + IND + IND + IND + IND + "d_regridding = true;" + NL
                    + IND + IND + IND + IND + IND + IND + "delete[] fields;" + NL
                    + IND + IND + IND + IND + IND + "}" + NL
                    + IND + IND + IND + IND + "}" + NL
                    + IND + IND + IND + "}" + NL
                    + IND + IND + "}" + NL
                    + IND + "}" + NL;
        }
        return problem.replaceFirst("#regridding#", regridding);
    }
    
    /**
     * Calculates and replaces the stencil of the problem.
     * @param problem       The problem being generated
     * @param pi            The problem information
     * @param vi            The variables information
     * @return              The code with the stencil
     */
    private String replaceStencil(String problem, ProblemInfo pi, VariableInfo vi) {
        ArrayList<String> coordinates = pi.getContCoordinates();
        String ghostWidth = "";
        String meshStencil = "";
        String particleStencil = "";
        if (pi.isHasABMMeshlessFields() || pi.isHasParticleFields()) {
            if (pi.getDimensions() == 2) {
            	particleStencil = "MAX(ceil(" + pi.getCompactSupportRatio() + " * influenceRadius/dx[0]), ceil(" + pi.getCompactSupportRatio() + " * influenceRadius/dx[1]))";
            }
            else {
            	particleStencil = "MAX(ceil(" + pi.getCompactSupportRatio() + " * influenceRadius/dx[0]), MAX(ceil(" + pi.getCompactSupportRatio() + " * influenceRadius/dx[1]), ceil(" + pi.getCompactSupportRatio() + " * influenceRadius/dx[2])))";
            }
        }
        if (pi.isHasABMMeshFields() || pi.isHasMeshFields()) {
        	ghostWidth = IND + "int maxratio = 1;" + NL
            		+ IND + "for (int il = 1; il < d_patch_hierarchy->getMaxNumberOfLevels(); il++) {" + NL
            		+ IND + IND + "const hier::IntVector ratio = d_patch_hierarchy->getRatioToCoarserLevel(il);" + NL
            		+ IND + IND + "maxratio = MAX(maxratio, ratio.max());" + NL
            		+ IND + "}" + NL
            		+ IND + "//Minimum region thickness" + NL
            		+ IND + "d_regionMinThickness = " + Math.max(pi.getSchemaStencil(), pi.getExtrapolationStencil()) + ";" + NL;
        	meshStencil = String.valueOf(pi.getSchemaStencil());
            if (pi.getX3dMovRegions() != null) {
                for (int i = 0; i < coordinates.size(); i++) {
                    String coord = coordinates.get(i);
                    ghostWidth = ghostWidth + IND + coord + "Lower = d_grid_geometry->getXLower()[" + i + "];" + NL
                            + IND + coord + "Upper = d_grid_geometry->getXUpper()[" + i + "];" + NL
                            + IND + "corridor_width = database->getDouble(\"corridor_width\");" + NL;
                }
            }
        }
        if ((!meshStencil.equals("")) && (!particleStencil.equals(""))) {
        	ghostWidth = ghostWidth + IND + "int mesh_stencil = " + meshStencil + ";" + NL;
        	ghostWidth = ghostWidth + IND + "int particle_stencil = " + particleStencil + ";" + NL;
        	if (pi.getExtrapolationStencil() > 0) {
        		ghostWidth = ghostWidth + IND + "//Extrapolation extra stencil" + NL
            			+ IND + "mesh_stencil = mesh_stencil + " + pi.getExtrapolationStencil() + " - 1;" + NL;
        	}
        	ghostWidth = ghostWidth + IND + "d_ghost_width = MAX(mesh_stencil, particle_stencil);" + NL;
        } 
        else {
        	if (!meshStencil.equals("")) {
            	ghostWidth = ghostWidth + IND + "d_ghost_width = " + meshStencil + ";" + NL;
               	if (pi.getExtrapolationStencil() > 0) {
            		ghostWidth = ghostWidth + IND + "//Extrapolation extra stencil" + NL
                			+ IND + "d_ghost_width = d_ghost_width + " + pi.getExtrapolationStencil() + " - 1;" + NL;
            	}
        	}
        	else {
            	ghostWidth = ghostWidth + IND + "d_ghost_width = " + particleStencil + ";" + NL;
        	}
        }
        return problem.replaceFirst("#d_ghost_width#", ghostWidth);
    }
    
    /**
     * Replaces the tags related to variables with the corresponding values.
     * @param problem       		The problem with the tags
     * @param dimensions    		The number of spatial dimensions
     * @param variables     		The variables used
     * @param fields        		The fields used
     * @param syncInfo      		Synchronization information
     * @param regions      			The region ids of the problem
     * @param coords        		The spatial coordinates
     * @param outputVarsAnalysis 	The variable analysis
     * @return              		The problem without the tags
     */
    private String replaceVariables(String problem, int dimensions, ArrayList<String> variables, HashMap<String, String> fields, ArrayList<String> derAuxiliaryFields, ArrayList<String> postRefAuxiliaryFields, 
            String syncInfo, ArrayList<String> regions, ArrayList<String> coords, ArrayList<String> outputVarsAnalysis, LinkedHashMap<String, ArrayList<String>> extrapolatedFieldGroups, ProblemInfo pi) {
    	varsToAllocateAfterRestart = new ArrayList<String>();
    	ArrayList<String> exFieldGroups = new ArrayList<String>();
    	if (extrapolatedFieldGroups != null) {
	    	Iterator<String> exFieldGroupsIt = extrapolatedFieldGroups.keySet().iterator();
	    	while (exFieldGroupsIt.hasNext()) {
	    		String var = exFieldGroupsIt.next();
	    		for (int i = 0; i < coords.size(); i++) {
	    			exFieldGroups.add("d_" + coords.get(i) + "_" + var);	
	    		}
	    	}
    	}
    	
        String variableDeclaration = "";
        String refineCoarsenOperators = "";
        String refineNewLevels = "";
        String allocate = "";
        String deallocate = "";
        if (pi.isHasMeshFields()) {
        	variableDeclaration = IND + "std::shared_ptr< pdat::CellVariable<int> > mask(std::shared_ptr< pdat::CellVariable<int> >(new pdat::CellVariable<int>(d_dim, \"samrai_mask\",1)));" + NL
        			+ IND + "d_mask_id = vdb->registerVariableAndContext(mask ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));" + NL
        			+ IND + "IntegrateDataWriter::setMaskVariable(d_mask_id);" + NL;
        }
        if (pi.isHasABMMeshFields() || pi.isHasMeshFields() || pi.isHasParticleFields()) {
        	variableDeclaration = variableDeclaration + IND + "std::shared_ptr< pdat::NodeVariable<double> > interior(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, \"regridding_value\",1)));" + NL
            		+ IND + "d_interior_regridding_value_id = vdb->registerVariableAndContext(interior ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));" + NL
            		+ IND + "std::shared_ptr< pdat::NodeVariable<int> > nonSync(std::shared_ptr< pdat::NodeVariable<int> >(new pdat::NodeVariable<int>(d_dim, \"regridding_tag\",1)));" + NL
            		+ IND + "d_nonSync_regridding_tag_id = vdb->registerVariableAndContext(nonSync ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));" + NL;
            for (int i = 0; i < coords.size(); i++) {
                String coord = coords.get(i);
                variableDeclaration = variableDeclaration + IND + "std::shared_ptr< pdat::NodeVariable<double> > interior_" + coord 
                        + "(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, \"interior_" + coord + "\",1)));" + NL
                        + IND + "d_interior_" + coord + "_id = vdb->registerVariableAndContext(interior_" + coord 
                        + " ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));" + NL;
            }
            for (int i = 0; i < regions.size(); i++) {
                String var = "FOV_" + regions.get(i);
                variableDeclaration = variableDeclaration + IND + "std::shared_ptr< pdat::NodeVariable<double> > " + var 
                        + "(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, \"" + var + "\",1)));" + NL
                        + IND + "d_" + var + "_id = vdb->registerVariableAndContext(" + var + " ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));"
                        + NL
                        + IND + "hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_" + var + "_id);" + NL;
            }
        	String timeInterpolatorInit = "";
        	for (TimeInterpolator ti: pi.getTimeInterpolationInfo().getTimeInterpolators().values()) {
        		timeInterpolatorInit = timeInterpolatorInit + IND + "std::shared_ptr<SAMRAI::hier::TimeInterpolateOperator> tio_mesh" + ti.getId() + "(new TimeInterpolator(d_grid_geometry, \"mesh\"));" + NL
                		+ IND + "time_interpolate_operator_mesh" + ti.getId() + " = std::dynamic_pointer_cast<TimeInterpolator>(tio_mesh" + ti.getId() + ");" + NL;	
        	}
        	refineCoarsenOperators = IND + "string refine_op_name = \"LINEAR_REFINE\";" + NL
        			+ IND + "int order = 0;" + NL 
                    + IND + "if (database->isDatabase(\"regridding\")) {" + NL
                    + IND + IND + "regridding_db = database->getDatabase(\"regridding\");" + NL
                    + IND + IND + "if (regridding_db->isString(\"interpolator\")) {" + NL
                    + IND + IND + IND + "refine_op_name = regridding_db->getString(\"interpolator\");" + NL
                    + IND + IND + IND + "if (refine_op_name == \"LINEAR_REFINE\") {" + NL
                    + IND + IND + IND + IND + "order = 1;" + NL
                    + IND + IND + IND + "}" + NL
                    + IND + IND + IND + "if (refine_op_name == \"CUBIC_REFINE\") {" + NL
                    + IND + IND + IND + IND + "order = 3;" + NL
                    + IND + IND + IND + "}" + NL
                    + IND + IND + IND + "if (refine_op_name == \"QUINTIC_REFINE\") {" + NL
                    + IND + IND + IND + IND + "order = 5;" + NL
                    + IND + IND + IND + "}" + NL
                    + IND + IND + "}" + NL
                    + IND + "}" + NL
                    + IND + "std::shared_ptr< hier::RefineOperator > refine_operator, refine_operator_bound;" + NL
            		+ IND + "std::shared_ptr< hier::CoarsenOperator > coarsen_operator = d_grid_geometry->lookupCoarsenOperator(FOV_" + regions.get(0) + ", \"CONSTANT_COARSEN\");" + NL
            		+ IND + "if (order > 0) {" + NL
            		+ IND + IND + "std::shared_ptr< hier::RefineOperator > tmp_refine_operator(new LagrangianPolynomicRefine(false, order, d_patch_hierarchy, d_dim));" + NL
            		+ IND + IND + "refine_operator = tmp_refine_operator;" + NL
            		+ IND + IND + "std::shared_ptr< hier::RefineOperator > tmp_refine_operator_bound(new LagrangianPolynomicRefine(true, order, d_patch_hierarchy, d_dim));" + NL
            		+ IND + IND + "refine_operator_bound = tmp_refine_operator_bound;" + NL
            		+ IND + "} else {" + NL
            		+ IND + IND + "refine_operator = d_grid_geometry->lookupRefineOperator(FOV_" + regions.get(0) + ", refine_op_name);" + NL
            		+ IND + IND + "refine_operator_bound = d_grid_geometry->lookupRefineOperator(FOV_" + regions.get(0) + ", refine_op_name);" + NL
            		+ IND + "}" + NL + NL
            		+ timeInterpolatorInit;
        	for (String speciesName : pi.getParticleSpecies()) {
        		refineCoarsenOperators = refineCoarsenOperators + IND + "std::shared_ptr<SAMRAI::hier::TimeInterpolateOperator> tio_particles_" + speciesName + "(new TimeInterpolator(d_grid_geometry, \"particle_" + speciesName + "\"));" + NL
        				+ IND + "time_interpolate_operator_particles_" + speciesName + " = std::dynamic_pointer_cast<TimeInterpolator>(tio_particles_" + speciesName + ");" + NL;
        	}
        	//Mask variable for integration writer
            if ( pi.isHasMeshFields()) {
            	varsToAllocateAfterRestart.add("d_mask_id");
            }
        	varsToAllocateAfterRestart.add("d_interior_regridding_value_id");
        	varsToAllocateAfterRestart.add("d_nonSync_regridding_tag_id");
            for (int i = 0; i < coords.size(); i++) {
                String coord = coords.get(i);
                allocate = allocate + IND + "level->allocatePatchData(d_interior_" + coord + "_id, init_data_time);" + NL;
                varsToAllocateAfterRestart.add("d_interior_" + coord + "_id");
            }
            for (int i = 0; i < regions.size(); i++) {
                String var = "FOV_" + regions.get(i);
                allocate = allocate + IND + "level->allocatePatchData(d_" + var + "_id, init_data_time);" + NL;
            }
        }
        if (pi.isHasABMMeshlessFields() || pi.isHasParticleFields()) {
            variableDeclaration = variableDeclaration
                + IND + "std::shared_ptr< pdat::IndexVariable<NonSyncs, pdat::CellGeometry > > nonSyncP(std::shared_ptr< pdat::IndexVariable<" 
                + "NonSyncs, pdat::CellGeometry > >(new pdat::IndexVariable<NonSyncs, pdat::CellGeometry >(d_dim, \"nonSyncP\")));" + NL
                + IND + "d_nonSyncP_id = vdb->registerVariableAndContext(nonSyncP ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));" + NL;
            varsToAllocateAfterRestart.add("d_interior_regridding_value_id");
            varsToAllocateAfterRestart.add("d_nonSync_regridding_tag_id");
            varsToAllocateAfterRestart.add("d_nonSyncP_id");

            for (String speciesName : pi.getParticleSpecies()) {
                allocate = allocate + IND + "level->allocatePatchData(d_particleVariables_" + speciesName + "_id, init_data_time);" + NL;
                variableDeclaration = variableDeclaration
                        + IND + "std::shared_ptr< pdat::IndexVariable<Particles<Particle_" + speciesName + ">, pdat::CellGeometry > > particleVariables_" + speciesName + "("
                        + "std::shared_ptr< pdat::IndexVariable<Particles<Particle_" + speciesName + ">, pdat::CellGeometry > >(new pdat::IndexVariable<Particles<Particle_" + speciesName + ">, pdat::CellGeometry >"
                        + "(d_dim, \"particleVariables_" + speciesName + "\")));" + NL
                        + IND + "d_particleVariables_" + speciesName + "_id = vdb->registerVariableAndContext(particleVariables_" + speciesName + ", d_cont_curr, " 
                        + "hier::IntVector(d_dim, d_ghost_width));" + NL
                        + IND + "hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_particleVariables_" + speciesName + "_id);" + NL;
            }
            allocate = allocate + IND + "level->allocatePatchData(d_nonSyncP_id, init_data_time);" + NL;
            deallocate = deallocate + IND + "level->deallocatePatchData(d_nonSyncP_id);" + NL;
        }
        
        if (pi.isHasABMMeshFields() || pi.isHasMeshFields()) {
	        for (int i = 0; i < variables.size(); i++) {
	            String var = variables.get(i);
	            if (!pi.getVariableInfo().getAllParticleVariables().contains(var)) {
		            if (var.startsWith("stalled_")) {
		                variableDeclaration = variableDeclaration + IND + "std::shared_ptr< pdat::NodeVariable<double> > " + var 
		                        + "(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, \"" + var + "\",1)));" + NL
		                        + IND + "d_" + var + "_id = vdb->registerVariableAndContext(" + var 
		                        + " ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));" + NL
		                        + IND + "hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_" + var + "_id);" + NL;
		            }
		            else {
		                variableDeclaration = variableDeclaration + IND + "std::shared_ptr< pdat::NodeVariable<double> > " + var 
		                    + "(std::shared_ptr< pdat::NodeVariable<double> >(new pdat::NodeVariable<double>(d_dim, \"" + var + "\",1)));" + NL
		                    + IND + "d_" + var + "_id = vdb->registerVariableAndContext(" + var + " ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));"
		                    + NL;
		                if (!fields.containsKey(var) && !exFieldGroups.contains(var) && !var.startsWith("extrapolatedSB")) {
		                    varsToAllocateAfterRestart.add("d_" + var + "_id");
		                }
		                else {
		                	variableDeclaration = variableDeclaration + IND + "hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_" + var + "_id);" + NL;
		                }
		            }
		            //When creating new level refine fields and extrapolated fields if they exist
		            if (fields.containsKey(var) || var.startsWith("extrapolatedSB") || outputVarsAnalysis.contains(var)) {
		            	if (postRefAuxiliaryFields.contains(var)) {
			                refineNewLevels = refineNewLevels + IND + "d_fill_new_level_aux->registerRefine(d_" + var + "_id,d_" + var + "_id,d_" + var 
			                        + "_id,refine_operator_bound);" + NL;
		            	}
		            	if (derAuxiliaryFields.contains(var)) {
			                refineNewLevels = refineNewLevels + IND + "d_fill_new_level_der_aux->registerRefine(d_" + var + "_id,d_" + var + "_id,d_" + var 
			                        + "_id,refine_operator_bound);" + NL;
		            	}
		                refineNewLevels = refineNewLevels + IND + "d_fill_new_level->registerRefine(d_" + var + "_id,d_" + var + "_id,d_" + var 
		                        + "_id,refine_operator_bound);" + NL;
		            }
		
		            allocate = allocate + IND + "level->allocatePatchData(d_" + var + "_id, init_data_time);" + NL;
	            }
	        }
        }
        if (pi.isHasParticleFields()) {
            for (int i = 0; i < coords.size(); i++) {
                String coord = coords.get(i);
                allocate = allocate + IND + "level->allocatePatchData(d_interior_" + coord + "_id, init_data_time);" + NL;
                varsToAllocateAfterRestart.add("d_interior_" + coord + "_id");
            }
            for (Entry<String, ArrayList<String>> entry : pi.getVariableInfo().getParticleVariables().entrySet()) {
            	String speciesName = entry.getKey();
            	for (String var : entry.getValue()) {
            		variableDeclaration = variableDeclaration + IND + "particleVariableList_" + speciesName + ".push_back(\"" + var + "\");" + NL;	
            	}
        		variableDeclaration = variableDeclaration + IND + "particleVariableList_" + speciesName + ".push_back(\"id_" + speciesName + "\");" + NL
        				+ IND + "particleVariableList_" + speciesName + ".push_back(\"procId_" + speciesName + "\");" + NL;
            }
        }

        //TODO region movement
        /*if (pi.getX3dMovRegions() != null) {
            ArrayList<String> tmp = new ArrayList<String>();
            for (int i = 0; i < regions.size(); i++) {
                String segId = regions.get(i);
                if (isNumeric(segId)) {
                    tmp.add(segId);
                }
            }
            while (!tmp.isEmpty()) {
                String id = tmp.remove(0);
                for (int i = 0; i < tmp.size(); i++) {
                    String comb = id + tmp.get(i);
                    for (int j = 0; j < dimensions; j++) {
                        String coord = coords.get(j);
                        allocate = allocate + IND + "level->allocatePatchData(d_normal" + coord + "_" + comb + "_id, init_data_time);" + NL
                                + IND + "level->allocatePatchData(d_velocity" + coord + "_" + comb + "_id, init_data_time);" + NL;
                        variableDeclaration = variableDeclaration + IND + "std::shared_ptr< pdat::CellVariable<double> > normal" + coord + "_" 
                            + comb + "(std::shared_ptr< pdat::CellVariable<double> >(new pdat::CellVariable<double>(d_dim, \"normal" + coord + "_"
                            + comb + "\",1)));" + NL
                            + IND + "d_normal" + coord + "_" + comb + "_id = vdb->registerVariableAndContext(normal" + coord + "_" + comb 
                            + " ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));" + NL
                            + IND + "hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_normal" + coord + "_" + comb + "_id);" + NL
                            + IND + "std::shared_ptr< pdat::CellVariable<double> > velocity" + coord + "_" 
                            + comb + "(std::shared_ptr< pdat::CellVariable<double> >(new pdat::CellVariable<double>(d_dim, \"velocity" + coord + "_"
                            + comb + "\",1)));" + NL
                            + IND + "d_velocity" + coord + "_" + comb + "_id = vdb->registerVariableAndContext(velocity" + coord + "_" + comb 
                            + " ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));" + NL
                            + IND + "hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_velocity" + coord + "_" + comb + "_id);"
                            + NL;
                    }
                }
            }
            
            allocate = allocate + IND + "level->allocatePatchData(d_interphase_id, init_data_time);" + NL;
            variableDeclaration = variableDeclaration + IND + "std::shared_ptr< pdat::IndexVariable<Interphase, pdat::CellGeometry > > interphase("
                    + "std::shared_ptr< pdat::IndexVariable<Interphase, pdat::CellGeometry > >(new pdat::IndexVariable<Interphase, "
                    + "pdat::CellGeometry >(d_dim, \"interphase\")));" + NL
                    + IND + "d_interphase_id = vdb->registerVariableAndContext(interphase, d_cont_curr, " 
                    + "hier::IntVector(d_dim, d_ghost_width));" + NL
                    + IND + "hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_interphase_id);" + NL
                    + IND + "std::shared_ptr< pdat::CellVariable<int> > soporte(std::shared_ptr< pdat::CellVariable<int> >("
                    + "new pdat::CellVariable<int>(d_dim, \"soporte\", 1)));" + NL
                    + IND + "d_soporte_id = vdb->registerVariableAndContext(soporte ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));" + NL
                    + IND + "hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_soporte_id);" + NL;
            ArrayList<String> elements = new ArrayList<String>();
            for (int i = 0; i < INTERPHASELENGTH; i++) {
                elements.add(String.valueOf(i));
            }
            ArrayList<String> combinations = createCombinations(dimensions, elements);
            for (int i = 0; i < regions.size(); i++) {
                String segId = regions.get(i);
                if (isNumeric(segId)) {
                    variableDeclaration = variableDeclaration + IND + "std::shared_ptr< pdat::CellVariable<double> > LS_" + segId 
                            + "(std::shared_ptr< pdat::CellVariable<double> >(new pdat::CellVariable<double>(d_dim, \"LS_" + segId + "\",1)));" + NL
                            + IND + "d_LS_" + segId + "_id = vdb->registerVariableAndContext(LS_" + segId + ", d_cont_curr, "
                            + "hier::IntVector(d_dim, d_ghost_width));" + NL
                            + IND + "hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_LS_" + segId + "_id);" + NL;
                    for (int j = 0; j < combinations.size(); j++) {
                        String comb = combinations.get(j);
                        String aIndex = "";
                        for (int k = 0; k < comb.length(); k++) {
                            aIndex = aIndex + "[" + comb.substring(k, k + 1) + "]";
                        }
                        variableDeclaration = variableDeclaration + IND + "std::shared_ptr< pdat::CellVariable<double> > ls_" + segId + "_" + comb 
                            + "(std::shared_ptr< pdat::CellVariable<double> >(new pdat::CellVariable<double>(d_dim, \"ls_" + segId 
                            + "_" + comb + "\",1)));" + NL
                            + IND + "d_ls_" + segId + "_" + comb + "_id = vdb->registerVariableAndContext(ls_" + segId + "_" + comb
                            + " ,d_cont_curr ,hier::IntVector(d_dim, d_ghost_width));" + NL
                            + IND + "hier::PatchDataRestartManager::getManager()->registerPatchDataForRestart(d_ls_" + segId + "_" + comb + "_id);" + NL;
                    }
                }
            }
        }*/

        problem = problem.replaceFirst("#variableDeclaration and register#", variableDeclaration);
        String mappingCommunication = "";
        if (pi.isHasMeshFields() || pi.isHasParticleFields()) {
            mappingCommunication = mappingCommunication
            		+ IND + "std::shared_ptr< hier::RefineOperator > refine_operator_map = d_grid_geometry->lookupRefineOperator(interior, \"LINEAR_REFINE\");" + NL
            		+ IND + "d_mapping_fill->registerRefine(d_interior_regridding_value_id,d_interior_regridding_value_id,d_interior_regridding_value_id, refine_operator_map);" + NL;
            for (int i = 0; i < coords.size(); i++) {
                String coord = coords.get(i);
                mappingCommunication = mappingCommunication + IND + "d_mapping_fill->registerRefine(d_interior_" + coord + "_id,d_interior_" + coord 
                        + "_id,d_interior_" + coord + "_id, refine_operator_map);" + NL;
            }
        }
        if (pi.isHasABMMeshlessFields()) {
            mappingCommunication = mappingCommunication + IND + "string mapping_op_name = \"CONSTANT_REFINE\";"  + NL
                + IND + "std::shared_ptr< hier::RefineOperator > refine_interior(d_grid_geometry->lookupRefineOperator(interior, mapping_op_name));" + NL
                + IND + "d_mapping_fill->registerRefine(d_interior_regridding_value_id,d_interior_regridding_value_id,d_interior_regridding_value_id, refine_interior);" + NL;
        } 
        if (pi.isHasABMMeshlessFields() || pi.isHasParticleFields()) {
        	String particleSeparations = "";
        	for (int i = 0; i < pi.getDimensions(); i++) {
        		String coord = pi.getContCoordinates().get(i);
        		particleSeparations = particleSeparations + "particleSeparation_" + coord + ", ";
        	}
        	particleSeparations = particleSeparations.substring(0, particleSeparations.lastIndexOf(","));
        	for (String speciesName: pi.getParticleSpecies()) {
        		mappingCommunication = mappingCommunication + IND + "refine_particle_operator_" + speciesName + " = new ParticleRefine<Particle_" + speciesName + ">(d_patch_hierarchy, d_grid_geometry, " + particleSeparations + ", d_ghost_width, influenceRadius);" + NL
	            	+ IND + "std::shared_ptr< hier::RefineOperator > refine_particles_" + speciesName + "(refine_particle_operator_" + speciesName + ");" + NL
	                + IND + "d_mapping_fill->registerRefine(d_particleVariables_" + speciesName + "_id,d_particleVariables_" + speciesName + "_id,d_particleVariables_" + speciesName + "_id, refine_particles_" + speciesName + ");" + NL;
        	}
        } 
        if (pi.isHasMeshFields() || pi.isHasABMMeshFields() || pi.isHasParticleFields()) {
            for (int i = 0; i < regions.size(); i++) {
                String var = "FOV_" + regions.get(i);
                mappingCommunication = mappingCommunication + IND + "d_mapping_fill->registerRefine(d_" + var + "_id,d_" + var + "_id,d_" + var + "_id, refine_operator_map);" + NL;
            }
            for (int i = 0; i < variables.size(); i++) {
                String var = variables.get(i);
                if (var.startsWith("stalled_")) {
                    mappingCommunication = mappingCommunication + IND + "d_mapping_fill->registerRefine(d_" + var + "_id,d_" + var + "_id,d_" + var + "_id, refine_operator_map);" + NL;
                }
            }
        }
        /*if (pi.getX3dMovRegions() != null) {
            allocate = allocate + IND + "level->allocatePatchData(d_soporte_id, init_data_time);" + NL;
            ArrayList<String> elements = new ArrayList<String>();
            for (int i = 0; i < INTERPHASELENGTH; i++) {
                elements.add(String.valueOf(i));
            }
            ArrayList<String> combinations = createCombinations(dimensions, elements);
            for (int i = 0; i < regions.size(); i++) {
                String segId = regions.get(i);
                if (isNumeric(segId)) {
                    allocate = allocate + IND + "level->allocatePatchData(d_LS_" + segId + "_id, init_data_time);" + NL;
                    for (int j = 0; j < combinations.size(); j++) {
                        String comb = combinations.get(j);
                        String aIndex = "";
                        for (int k = 0; k < comb.length(); k++) {
                            aIndex = aIndex + "[" + comb.substring(k, k + 1) + "]";
                        }
                        mappingCommunication = mappingCommunication + IND + "std::shared_ptr< hier::RefineOperator > refine_ls_" 
                                + segId + "_" + comb
                                + "(d_grid_geometry->lookupRefineOperator(ls_" + segId + "_" + comb + ", mapping_inter));" + NL
                                + IND + "d_mapping_fill->registerRefine(d_ls_" + segId + "_" + comb 
                                + "_id,d_ls_" + segId + "_" + comb + "_id,d_ls_" + segId + "_" + comb + "_id, refine_ls_" + segId + "_" 
                                + comb + ");" + NL
                                + IND + "d_mapping_fill2->registerRefine(d_ls_" + segId + "_" + comb 
                                + "_id,d_ls_" + segId + "_" + comb + "_id,d_ls_" + segId + "_" + comb + "_id, refine_ls_" + segId + "_" 
                                + comb + ");" + NL;
                        allocate = allocate + IND + "level->allocatePatchData(d_ls_" + segId + "_" + comb + "_id, init_data_time);" + NL;
                    }
                }
            }
        }*/
        problem = problem.replaceFirst("#mappingCommunication#", mappingCommunication);
        problem = problem.replaceFirst("#refine coarsen operators#", refineCoarsenOperators);
        problem = problem.replaceFirst("#refine boundaries register#", syncInfo.substring(syncInfo.indexOf("#Variable registrations#") 
                + "#Variable registrations#".length(), syncInfo.indexOf("#Coarsen declarations#")));
        String indent = IND;
        String access = "";
        ArrayList<String> combinations = CodeGeneratorUtils.createCombinations(coords);
        for (int i = 0; i < coords.size(); i++) {
        	indent = indent + IND;
        }
        for (int i = 0; i < combinations.size(); i++) {
        	String index = "";
        	String limitCheck = "";
        	for (int j = 0; j < combinations.get(i).length(); j++) {
        		String coord = coords.get(j);
        		if (combinations.get(i).substring(j).startsWith("+")) {
        			index = index + coord + "base + " + coord + ", ";
        			limitCheck = limitCheck + coord + "base + " + coord + " >= d_ghost_width && " + coord + "base + " + coord + " < " + coord + "last - d_ghost_width && "; 
        		}
        		else {
        			index = index + coord + "base - " + coord + ", ";
        			limitCheck = limitCheck + coord + "base - " + coord + " >= d_ghost_width && " + coord + "base - " + coord + " < " + coord + "last - d_ghost_width && ";
        		}
        	}
        	index = index.substring(0, index.lastIndexOf(", "));
        	access = access + "(" + limitCheck + "vector(fov, " + index + ") > 0) || ";
        }
        access = access.substring(0, access.lastIndexOf(" ||"));
                
        if (pi.isHasABMMeshlessFields()) {
            problem = problem.replaceFirst("#refine new levels register#", "");
        }
        else {
            problem = problem.replaceFirst("#refine new levels register#", refineNewLevels);
        }
        if (pi.isHasABMMeshlessFields()) {
            problem = problem.replaceFirst("#coarsen boundaries register#", "");
        }
        else {
            problem = problem.replaceFirst("#coarsen boundaries register#", syncInfo.substring(syncInfo.indexOf("#Coarsen registrations#") 
                    + "#Coarsen registrations#".length()));
        }
        if (pi.isHasMeshFields()) {
            allocate = allocate + IND + "level->allocatePatchData(d_mask_id, init_data_time);" + NL;	
        }
        String allocateAfterRestart = "";
        if (varsToAllocateAfterRestart.size() > 0) {
        	allocateAfterRestart = IND + "for (int il = 0; il < d_patch_hierarchy->getNumberOfLevels(); il++) {" + NL
        			+ IND + IND + "std::shared_ptr< hier::PatchLevel > level(d_patch_hierarchy->getPatchLevel(il));" + NL;
        	for (int i = 0; i < varsToAllocateAfterRestart.size(); i++) {
        		allocateAfterRestart = allocateAfterRestart + IND + IND + "level->allocatePatchData(" + varsToAllocateAfterRestart.get(i) + ");" + NL;
        	}
        	allocateAfterRestart = allocateAfterRestart + IND + "}" + NL;
        }
        problem = problem.replaceFirst("#allocateAfterRestart#", allocateAfterRestart);
        problem = problem.replaceFirst("#deallocate variables#", deallocate);
        return problem.replaceFirst("#allocate variables#", allocate);
    }
    
    /**
     * Replaces #condition to regrid# tag with the regridding operations.
     * @param problem       The problem to place the regridding options
     * @param dimensions    The number of spatial dimensions
     * @param coordinates   The coordinates
     * @param pi			Problem information
     * @return              The problem with the tag replaced.
     */
    private String replaceRegridding(String problem, int dimensions, ArrayList<String> coordinates, ProblemInfo pi) {
        if (pi.isHasABMMeshlessFields()) {
            return problem.replaceFirst("#applyGradientDetector#", "");
        }
		String regrid = "";
		String condition = "";
		String conditionInsideIndex = "";
		String conditionInsideIndexB = "";
		String distanceB = "";
		String startLoop = "";
		String startLoopParticles = "";
		String startLoopB = "";
		String startLoopTags = "";
		String endLoop = "";
		String endLoopB = "";
		String loopIndent = IND + IND + IND;
		String loopIndentB = "";
		String indexesTag = "";
		String indexesTagB = "";
		String indexesRegriddingTag = "";
		String indexesRegriddingTagB = "";
		//Main loop
		for (int i = dimensions - 1; i >= 0; i--) {
		    startLoop = startLoop + loopIndent + "for(int index" + i + " = 0; index" + i + " < " + coordinates.get(i) + "last; index" 
		        + i + "++) {" + NL;
		    startLoopParticles = startLoopParticles + loopIndent + "for(int index" + i + " = 0; index" + i + " < " + coordinates.get(i) + "last - 1; index" + i + "++) {" + NL;
		    startLoopB = startLoopB + loopIndentB + "for(int index" + i + "b = MAX(0, index" + i + " - buffer_left); index" + i + "b < MIN(index" + i + " + buffer_left + 1, " + coordinates.get(i) + "last); index" 
		            + i + "b++) {" + NL;
		    startLoopTags = startLoopTags + loopIndent + "for(int index" + i + " = 0; index" + i + " < (tlast(" + i + ")-tfirst(" + i + "))+1; index" 
		            + i + "++) {" + NL;
		    endLoop = loopIndent + "}" + NL + endLoop;
		    endLoopB = loopIndentB + "}" + NL + endLoopB;
		    loopIndent = loopIndent + IND;
		    loopIndentB = loopIndentB + IND;
		}
		for (int i = 0; i < dimensions; i++) {
		    loopIndent = loopIndent.substring(0, loopIndent.lastIndexOf(IND));
		}
		//GRADIENT
		String gradient = "";
		String additionalTags;
		String regriddingBuffer;
		String idx = "";
		for (int i = 0; i < dimensions; i++) {
		    String indexPlus1 = getIndexOperated(dimensions, i, "+1");
		    String indexPlus2 = getIndexOperated(dimensions, i, "+2");
		    String indexMinus1 = getIndexOperated(dimensions, i, "-1");
		    String index = "";
		    for (int j = 0; j < dimensions; j++) {
		        index = index + "index" + j + "+d_ghost_width, ";
		    }
		    index = index.substring(0, index.lastIndexOf(","));
		    condition = condition + "(fabs(vector(regrid_field, " + indexPlus1 + ")-vector(regrid_field, " + index + ")) > "
		        + "d_regridding_compressionFactor * MIN(fabs(vector(regrid_field, " + indexPlus2 + ")-vector(regrid_field, " + indexPlus1 
		        + ")) + d_regridding_mOffset * pow(dx[" + i + "], 2), " + "fabs(vector(regrid_field, " + index + ")-vector(regrid_field, " 
		        + indexMinus1 + ")) + d_regridding_mOffset * pow(dx[" + i + "], 2)) ) ||";
		    loopIndent = loopIndent + IND;
		    indexesTag = indexesTag + "index" + i + " - d_ghost_width, ";
		    indexesTagB = indexesTagB + "index" + i + "b - d_ghost_width, ";
		    indexesRegriddingTag = indexesRegriddingTag + "index" + i + ", ";
		    indexesRegriddingTagB = indexesRegriddingTagB + "index" + i + "b, ";
		    conditionInsideIndexB = conditionInsideIndexB + " && index" + i + "b >= d_ghost_width && index" + i + "b < (tlast(" + i + ")-tfirst(" + i + "))+1 + d_ghost_width";
		    conditionInsideIndex = conditionInsideIndex + " && index" + i + " >= d_ghost_width && index" + i + " < (tlast(" + i + ")-tfirst(" + i + "))+1 + d_ghost_width";
		    if (i == 0) {
		    	distanceB = "abs(index0b - index0)";
		    }
		    else {
		    	distanceB = "MAX(" + distanceB + ", abs(index" + i + "b - index" + i + "))";
		    }
		    idx = idx + "index" + i + " + boxfirstP(" + i + "), ";
		}
		indexesTag = indexesTag.substring(0, indexesTag.lastIndexOf(","));
		indexesTagB = indexesTagB.substring(0, indexesTagB.lastIndexOf(","));
		indexesRegriddingTag = indexesRegriddingTag.substring(0, indexesRegriddingTag.lastIndexOf(","));
		indexesRegriddingTagB = indexesRegriddingTagB.substring(0, indexesRegriddingTagB.lastIndexOf(","));
		idx =  idx.substring(0,  idx.lastIndexOf(","));
		conditionInsideIndex = conditionInsideIndex.substring(" && ".length());
		condition = condition.substring(0, condition.lastIndexOf("||"));
		if (dimensions == 2) {
			additionalTags = loopIndent + IND + "//SAMRAI tagging" + NL
		  			+ loopIndent + IND + "if (index0 > d_ghost_width && index0 < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1 > d_ghost_width && index1 < (tlast(1)-tfirst(1))+1 + d_ghost_width) {" + NL
		  			+ loopIndent + IND + IND + "vectorT(tags, index0-d_ghost_width - 1, index1-d_ghost_width - 1) = 1;" + NL
		  			+ loopIndent + IND + "}" + NL
		  			+ loopIndent + IND + "if (index0 > d_ghost_width && index0 < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1 >= d_ghost_width && index1 < (tlast(1)-tfirst(1))+1 + d_ghost_width) {" + NL
		  			+ loopIndent + IND + IND + "vectorT(tags, index0-d_ghost_width - 1, index1-d_ghost_width) = 1;" + NL
		  			+ loopIndent + IND + "}" + NL
		  			+ loopIndent + IND + "if (index0 >= d_ghost_width && index0 < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1 > d_ghost_width && index1 < (tlast(1)-tfirst(1))+1 + d_ghost_width) {" + NL
		  			+ loopIndent + IND + IND + "vectorT(tags, index0-d_ghost_width, index1-d_ghost_width - 1) = 1;" + NL
		  			+ loopIndent + IND + "}" + NL
		  			+ loopIndent + IND + "//Informative tagging" + NL
		  			+ loopIndent + IND + "if (index0 > 0 && index1 > 0) {" + NL
		  			+ loopIndent + IND + IND + "if (vector(regridding_tag,index0-1,index1-1) != 1)" + NL
		  			+ loopIndent + IND + IND + IND + "vector(regridding_tag,index0-1,index1-1) = 1;" + NL
		  			+ loopIndent + IND + "}" + NL
		  			+ loopIndent + IND + "if (index0 > 0) {" + NL
		  			+ loopIndent + IND + IND + "if (vector(regridding_tag,index0-1,index1) != 1)" + NL
		  			+ loopIndent + IND + IND + IND + "vector(regridding_tag,index0-1,index1) = 1;" + NL
		  			+ loopIndent + IND + "}" + NL
		  			+ loopIndent + IND + "if (index1 > 0) {" + NL
		  			+ loopIndent + IND + IND + "if (vector(regridding_tag,index0,index1-1) != 1)" + NL
		  			+ loopIndent + IND + IND + IND + "vector(regridding_tag,index0,index1-1) = 1;" + NL
		  			+ loopIndent + IND + "}" + NL
					+ loopIndent + IND + "if (d_regridding_buffer > 0) {" + NL
					+ loopIndent + IND + IND + "int distance;" + NL
					+ loopIndent + IND + IND + "for(int index1b = MAX(0, index1 - d_regridding_buffer - 1); index1b < MIN(index1 + d_regridding_buffer + 1, jlast); index1b++) {" + NL
					+ loopIndent + IND + IND + IND + "for(int index0b = MAX(0, index0 - d_regridding_buffer - 1); index0b < MIN(index0 + d_regridding_buffer + 1, ilast); index0b++) {" + NL
					+ loopIndent + IND + IND + IND + IND + "int distx = (index0b - index0);" + NL
					+ loopIndent + IND + IND + IND + IND + "if (distx < 0) {" + NL
					+ loopIndent + IND + IND + IND + IND + IND + "distx++;" + NL
					+ loopIndent + IND + IND + IND + IND + "}" + NL
					+ loopIndent + IND + IND + IND + IND + "int disty = (index1b - index1);" + NL
					+ loopIndent + IND + IND + IND + IND + "if (disty < 0) {" + NL
					+ loopIndent + IND + IND + IND + IND + IND + "disty++;" + NL
					+ loopIndent + IND + IND + IND + IND + "}" + NL
					+ loopIndent + IND + IND + IND + IND + "distance = 1 + MAX(abs(distx), abs(disty));" + NL
					+ loopIndent + IND + IND + IND + IND + "if (index0b >= d_ghost_width && index0b < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1b >= d_ghost_width && index1b < (tlast(1)-tfirst(1))+1 + d_ghost_width) {" + NL
					+ loopIndent + IND + IND + IND + IND + IND + "vectorT(tags,index0b-d_ghost_width,index1b-d_ghost_width) = 1;" + NL
					+ loopIndent + IND + IND + IND + IND + "}" + NL													
					+ loopIndent + IND + IND + IND + IND + "if (vector(regridding_tag,index0b,index1b) == 0 || vector(regridding_tag,index0b,index1b) > distance) {" + NL
					+ loopIndent + IND + IND + IND + IND + IND + "vector(regridding_tag,index0b,index1b) = distance;" + NL
					+ loopIndent + IND + IND + IND + IND + "}" + NL
					+ loopIndent + IND + IND + IND + "}" + NL
					+ loopIndent + IND + IND + "}" + NL
					+ loopIndent + IND + "}" + NL;
			regriddingBuffer = loopIndent + IND + IND + "if (d_regridding_buffer > 0) {" + NL
				+ loopIndent + IND + IND + IND + "int distance;" + NL
				+ loopIndent + IND + IND + IND + "for(int index1b = MAX(0, index1 - d_regridding_buffer); index1b < MIN(index1 + d_regridding_buffer + 1, jlast); index1b++) {" + NL
				+ loopIndent + IND + IND + IND + IND + "for(int index0b = MAX(0, index0 - d_regridding_buffer); index0b < MIN(index0 + d_regridding_buffer + 1, ilast); index0b++) {" + NL
				+ loopIndent + IND + IND + IND + IND + IND + "int distx = (index0b - index0);" + NL
				+ loopIndent + IND + IND + IND + IND + IND + "int disty = (index1b - index1);" + NL
				+ loopIndent + IND + IND + IND + IND + IND + "distance = 1 + MAX(abs(distx), abs(disty));" + NL
				+ loopIndent + IND + IND + IND + IND + IND + "if (index0b >= d_ghost_width && index0b < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1b >= d_ghost_width && index1b < (tlast(1)-tfirst(1))+1 + d_ghost_width) {" + NL
				+ loopIndent + IND + IND + IND + IND + IND + IND + "vectorT(tags,index0b-d_ghost_width,index1b-d_ghost_width) = 1;" + NL
				+ loopIndent + IND + IND + IND + IND + IND + "}" + NL
				+ loopIndent + IND + IND + IND + IND + IND + "if (vector(regridding_tag,index0b,index1b) == 0 || vector(regridding_tag,index0b,index1b) > distance) {" + NL
				+ loopIndent + IND + IND + IND + IND + IND + IND + "vector(regridding_tag,index0b,index1b) = distance;" + NL
				+ loopIndent + IND + IND + IND + IND + IND + "}" + NL
				+ loopIndent + IND + IND + IND + IND + "}" + NL
				+ loopIndent + IND + IND + IND + "}" + NL
				+ loopIndent + IND + IND + "}" + NL;
		}
		else {
		  	additionalTags = loopIndent + IND + "//SAMRAI tagging" + NL
		  			+ loopIndent + IND + "if (index0 > d_ghost_width && index0 < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1 > d_ghost_width && index1 < (tlast(1)-tfirst(1))+1 + d_ghost_width && index2 > d_ghost_width && index2 < (tlast(2)-tfirst(2))+1 + d_ghost_width) {" + NL
		  			+ loopIndent + IND + IND + "vectorT(tags, index0-d_ghost_width - 1, index1-d_ghost_width - 1, index2-d_ghost_width - 1) = 1;" + NL
		  			+ loopIndent + IND + "}" + NL
		  			+ loopIndent + IND + "if (index0 >= d_ghost_width && index0 < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1 > d_ghost_width && index1 < (tlast(1)-tfirst(1))+1 + d_ghost_width && index2 > d_ghost_width && index2 < (tlast(2)-tfirst(2))+1 + d_ghost_width) {" + NL
		  			+ loopIndent + IND + IND + "vectorT(tags, index0-d_ghost_width, index1-d_ghost_width - 1, index2-d_ghost_width - 1) = 1;" + NL
		  			+ loopIndent + IND + "}" + NL
		  			+ loopIndent + IND + "if (index0 > d_ghost_width && index0 < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1 >= d_ghost_width && index1 < (tlast(1)-tfirst(1))+1 + d_ghost_width && index2 > d_ghost_width && index2 < (tlast(2)-tfirst(2))+1 + d_ghost_width) {" + NL
		  			+ loopIndent + IND + IND + "vectorT(tags, index0-d_ghost_width - 1, index1-d_ghost_width, index2-d_ghost_width - 1) = 1;" + NL
		  			+ loopIndent + IND + "}" + NL
		  			+ loopIndent + IND + "if (index0 >= d_ghost_width && index0 < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1 >= d_ghost_width && index1 < (tlast(1)-tfirst(1))+1 + d_ghost_width && index2 > d_ghost_width && index2 < (tlast(2)-tfirst(2))+1 + d_ghost_width) {" + NL
		  			+ loopIndent + IND + IND + "vectorT(tags, index0-d_ghost_width, index1-d_ghost_width, index2-d_ghost_width - 1) = 1;" + NL
		  			+ loopIndent + IND + "}" + NL
		  			+ loopIndent + IND + "if (index0 > d_ghost_width && index0 < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1 > d_ghost_width && index1 < (tlast(1)-tfirst(1))+1 + d_ghost_width && index2 >= d_ghost_width && index2 < (tlast(2)-tfirst(2))+1 + d_ghost_width) {" + NL
		  			+ loopIndent + IND + IND + "vectorT(tags, index0-d_ghost_width - 1, index1-d_ghost_width - 1, index2-d_ghost_width) = 1;" + NL
		  			+ loopIndent + IND + "}" + NL
		  			+ loopIndent + IND + "if (index0 > d_ghost_width && index0 < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1 >= d_ghost_width && index1 < (tlast(1)-tfirst(1))+1 + d_ghost_width && index2 >= d_ghost_width && index2 < (tlast(2)-tfirst(2))+1 + d_ghost_width) {" + NL
		  			+ loopIndent + IND + IND + "vectorT(tags, index0-d_ghost_width - 1, index1-d_ghost_width, index2-d_ghost_width) = 1;" + NL
		  			+ loopIndent + IND + "}" + NL
		  			+ loopIndent + IND + "if (index0 >= d_ghost_width && index0 < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1 > d_ghost_width && index1 < (tlast(1)-tfirst(1))+1 + d_ghost_width && index2 >= d_ghost_width && index2 < (tlast(2)-tfirst(2))+1 + d_ghost_width) {" + NL
		  			+ loopIndent + IND + IND + "vectorT(tags, index0-d_ghost_width, index1-d_ghost_width - 1, index2-d_ghost_width) = 1;" + NL
		  			+ loopIndent + IND + "}" + NL
		  			+ loopIndent + IND + "//Informative tagging" + NL
		  			+ loopIndent + IND + "if (index0 > 0 && index1 > 0 && index2 > 0) {" + NL
		  			+ loopIndent + IND + IND + "if (vector(regridding_tag,index0-1,index1-1,index2-1) != 1)" + NL
		  			+ loopIndent + IND + IND + IND + "vector(regridding_tag,index0-1,index1-1,index2-1) = 1;" + NL
		  			+ loopIndent + IND + "}" + NL
		  			+ loopIndent + IND + "if (index1 > 0 && index2 > 0) {" + NL
		  			+ loopIndent + IND + IND + "if (vector(regridding_tag,index0,index1-1,index2-1) != 1)" + NL
		  			+ loopIndent + IND + IND + IND + "vector(regridding_tag,index0,index1-1,index2-1) = 1;" + NL
		  			+ loopIndent + IND + "}" + NL
		  			+ loopIndent + IND + "if (index0 > 0 && index2 > 0) {" + NL
		  			+ loopIndent + IND + IND + "if (vector(regridding_tag,index0-1,index1,index2-1) != 1)" + NL
		  			+ loopIndent + IND + IND + IND + "vector(regridding_tag,index0-1,index1,index2-1) = 1;" + NL
		  			+ loopIndent + IND + "}" + NL
		  			+ loopIndent + IND + "if (index2 > 0) {" + NL
		  			+ loopIndent + IND + IND + "if (vector(regridding_tag,index0,index1,index2-1) != 1)" + NL
		  			+ loopIndent + IND + IND + IND + "vector(regridding_tag,index0,index1,index2-1) = 1;" + NL
		  			+ loopIndent + IND + "}" + NL
		  			+ loopIndent + IND + "if (index0 > 0 && index1 > 0) {" + NL
		  			+ loopIndent + IND + IND + "if (vector(regridding_tag,index0-1,index1-1,index2) != 1)" + NL
		  			+ loopIndent + IND + IND + IND + "vector(regridding_tag,index0-1,index1-1,index2) = 1;" + NL
		  			+ loopIndent + IND + "}" + NL
		  			+ loopIndent + IND + "if (index0 > 0) {" + NL
		  			+ loopIndent + IND + IND + "if (vector(regridding_tag,index0-1,index1,index2) != 1)" + NL
		  			+ loopIndent + IND + IND + IND + "vector(regridding_tag,index0-1,index1,index2) = 1;" + NL
		  			+ loopIndent + IND + "}" + NL
		  			+ loopIndent + IND + "if (index1 > 0) {" + NL
		  			+ loopIndent + IND + IND + "if (vector(regridding_tag,index0,index1-1,index2) != 1)" + NL
		  			+ loopIndent + IND + IND + IND + "vector(regridding_tag,index0,index1-1,index2) = 1;" + NL
		  			+ loopIndent + IND + "}" + NL
					+ loopIndent + IND + "if (d_regridding_buffer > 0) {" + NL
					+ loopIndent + IND + IND + "int distance;" + NL
					+ loopIndent + IND + IND + "for(int index2b = MAX(0, index2 - d_regridding_buffer - 1); index2b < MIN(index2 + d_regridding_buffer + 1, klast); index2b++) {" + NL
					+ loopIndent + IND + IND + IND + "for(int index1b = MAX(0, index1 - d_regridding_buffer - 1); index1b < MIN(index1 + d_regridding_buffer + 1, jlast); index1b++) {" + NL
					+ loopIndent + IND + IND + IND + IND + "for(int index0b = MAX(0, index0 - d_regridding_buffer - 1); index0b < MIN(index0 + d_regridding_buffer + 1, ilast); index0b++) {" + NL
					+ loopIndent + IND + IND + IND + IND + IND + "int distx = (index0b - index0);" + NL
					+ loopIndent + IND + IND + IND + IND + IND + "if (distx < 0) {" + NL
					+ loopIndent + IND + IND + IND + IND + IND + IND + "distx++;" + NL
					+ loopIndent + IND + IND + IND + IND + IND + "}" + NL
					+ loopIndent + IND + IND + IND + IND + IND + "int disty = (index1b - index1);" + NL
					+ loopIndent + IND + IND + IND + IND + IND + "if (disty < 0) {" + NL
					+ loopIndent + IND + IND + IND + IND + IND + IND + "disty++;" + NL
					+ loopIndent + IND + IND + IND + IND + IND + "}" + NL
					+ loopIndent + IND + IND + IND + IND + IND + "int distz = (index2b - index2);" + NL
					+ loopIndent + IND + IND + IND + IND + IND + "if (distz < 0) {" + NL
					+ loopIndent + IND + IND + IND + IND + IND + IND + "distz++;" + NL
					+ loopIndent + IND + IND + IND + IND + IND + "}" + NL
					+ loopIndent + IND + IND + IND + IND + IND + "distance = 1 + MAX(MAX(abs(distx), abs(disty)), abs(distz));" + NL
					+ loopIndent + IND + IND + IND + IND + IND + "if (index0b >= d_ghost_width && index0b < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1b >= d_ghost_width && index1b < (tlast(1)-tfirst(1))+1 + d_ghost_width && index2b >= d_ghost_width && index2b < (tlast(2)-tfirst(2))+1 + d_ghost_width) {" + NL
					+ loopIndent + IND + IND + IND + IND + IND + IND + "vectorT(tags,index0b-d_ghost_width,index1b-d_ghost_width,index2b-d_ghost_width) = 1;" + NL
					+ loopIndent + IND + IND + IND + IND + IND + "}" + NL													
					+ loopIndent + IND + IND + IND + IND + IND + "if (vector(regridding_tag,index0b,index1b,index2b) == 0 || vector(regridding_tag,index0b,index1b,index2b) > distance) {" + NL
					+ loopIndent + IND + IND + IND + IND + IND + IND + "vector(regridding_tag,index0b,index1b,index2b) = distance;" + NL
					+ loopIndent + IND + IND + IND + IND + IND + "}" + NL
					+ loopIndent + IND + IND + IND + IND + "}" + NL
					+ loopIndent + IND + IND + IND + "}" + NL
					+ loopIndent + IND + IND + "}" + NL
					+ loopIndent + IND + "}" + NL;
			regriddingBuffer = loopIndent + IND + IND + "if (d_regridding_buffer > 0) {" + NL
				+ loopIndent + IND + IND + IND + "int distance;" + NL
				+ loopIndent + IND + IND + IND + "for(int index2b = MAX(0, index2 - d_regridding_buffer); index2b < MIN(index2 + d_regridding_buffer + 1, klast); index2b++) {" + NL
				+ loopIndent + IND + IND + IND + IND + "for(int index1b = MAX(0, index1 - d_regridding_buffer); index1b < MIN(index1 + d_regridding_buffer + 1, jlast); index1b++) {" + NL
				+ loopIndent + IND + IND + IND + IND + IND + "for(int index0b = MAX(0, index0 - d_regridding_buffer); index0b < MIN(index0 + d_regridding_buffer + 1, ilast); index0b++) {" + NL
				+ loopIndent + IND + IND + IND + IND + IND + IND + "int distx = (index0b - index0);" + NL
				+ loopIndent + IND + IND + IND + IND + IND + IND + "int disty = (index1b - index1);" + NL
				+ loopIndent + IND + IND + IND + IND + IND + IND + "int distz = (index2b - index2);" + NL
				+ loopIndent + IND + IND + IND + IND + IND + IND + "distance = 1 + MAX(MAX(abs(distx), abs(disty)), abs(distz));" + NL
				+ loopIndent + IND + IND + IND + IND + IND + IND + "if (index0b >= d_ghost_width && index0b < (tlast(0)-tfirst(0))+1 + d_ghost_width && index1b >= d_ghost_width && index1b < (tlast(1)-tfirst(1))+1 + d_ghost_width && index2b >= d_ghost_width && index2b < (tlast(2)-tfirst(2))+1 + d_ghost_width) {" + NL
				+ loopIndent + IND + IND + IND + IND + IND + IND + IND + "vectorT(tags,index0b-d_ghost_width,index1b-d_ghost_width,index2b-d_ghost_width) = 1;" + NL
				+ loopIndent + IND + IND + IND + IND + IND + IND + "}" + NL
				+ loopIndent + IND + IND + IND + IND + IND + IND + "if (vector(regridding_tag,index0b,index1b,index2b) == 0 || vector(regridding_tag,index0b,index1b,index2b) > distance) {" + NL
				+ loopIndent + IND + IND + IND + IND + IND + IND + IND + "vector(regridding_tag,index0b,index1b,index2b) = distance;" + NL
				+ loopIndent + IND + IND + IND + IND + IND + IND + "}" + NL
				+ loopIndent + IND + IND + IND + IND + IND + "}" + NL
				+ loopIndent + IND + IND + IND + IND + "}" + NL
				+ loopIndent + IND + IND + IND + "}" + NL
				+ loopIndent + IND + IND + "}" + NL;
		}
		gradient = IND + IND + IND + "int regrid_field_id = vdb->getVariable(d_regridding_field)->getInstanceIdentifier();" + NL
		    + IND + IND + IND + "double* regrid_field = ((pdat::NodeData<double> *) patch->getPatchData(regrid_field_id).get())->getPointer();" + NL 
			+ IND + IND + IND + "double* regridding_value = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_regridding_value_id).get())->getPointer();" + NL 
		    + startLoopParticles
		    + loopIndent + "if (vector(regrid_field, " + indexesRegriddingTag + ")!=0) {" + NL 
		    + loopIndent + IND + "if (" + condition + ") {" + NL 
		    + loopIndent + IND + IND + "if (" + conditionInsideIndex + ") {" + NL
		    + loopIndent + IND + IND + IND + "vectorT(tags," + indexesTag + ") = 1;" + NL
		    + loopIndent + IND + IND + "}" + NL
		    + loopIndent + IND + IND + "vector(regridding_tag," + indexesRegriddingTag + ") = 1;" + NL
		    + indent(additionalTags, 1)
		    + loopIndent + IND + "}" + NL 
		    + loopIndent + "}" + NL 
		    + endLoop;
		String gradientParticles = IND + IND + IND + IND + "TBOX_ERROR(d_object_name << \": GRADIENT tagging not valid for particles, use FUNCTION or SHADOW instead:\");" + NL;
		//FUNCTION
		String function = IND + IND + IND + "int regrid_field_id = vdb->getVariable(d_regridding_field)->getInstanceIdentifier();" + NL
		    + IND + IND + IND + "double* regrid_field = ((pdat::NodeData<double> *) patch->getPatchData(regrid_field_id).get())->getPointer();" + NL 
			+ IND + IND + IND + "double* regridding_value = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_regridding_value_id).get())->getPointer();" + NL 
		    + startLoop
		    + loopIndent + "vector(regridding_value, " + indexesRegriddingTag + ") = vector(regrid_field, " + indexesRegriddingTag + ");" + NL
		    + loopIndent + "if (vector(regrid_field, " + indexesRegriddingTag + ") > d_regridding_threshold) {" + NL
		    + loopIndent + IND + "if (" + conditionInsideIndex + ") {" + NL
		    + loopIndent + IND + IND + "vectorT(tags," + indexesTag + ") = 1;" + NL
		    + loopIndent + IND + "}" + NL
		    + loopIndent + IND + "vector(regridding_tag," + indexesRegriddingTag + ") = 1;" + NL
		    + additionalTags
		    + loopIndent + "}" + NL           
		    + endLoop;
		//SHADOW
		String shadow = IND + IND + IND + "if (!initial_time) {" + NL 
			+ IND + IND + IND + IND + "if (!(vdb->checkVariableExists(d_regridding_field_shadow))) {" + NL
			+ IND + IND + IND + IND + IND + "TBOX_ERROR(d_object_name << \": Regridding field selected not found:\" <<  d_regridding_field_shadow<<  \"\");" + NL
			+ IND + IND + IND + IND + "}" + NL	
			+ IND + IND + IND + IND + "int regrid_field_id = vdb->getVariable(d_regridding_field)->getInstanceIdentifier();" + NL
		    + IND + IND + IND + IND + "double* regrid_field1 = ((pdat::NodeData<double> *) patch->getPatchData(regrid_field_id).get())->getPointer();" + NL 
			+ IND + IND + IND + IND + "int regrid_field_shadow_id = vdb->getVariable(d_regridding_field_shadow)->getInstanceIdentifier();" + NL
		    + IND + IND + IND + IND + "double* regrid_field2 = ((pdat::NodeData<double> *) patch->getPatchData(regrid_field_shadow_id).get())->getPointer();" + NL
			+ IND + IND + IND + IND + "double* regridding_value = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_regridding_value_id).get())->getPointer();" + NL 
		    + indent(startLoop, 1)
		    + loopIndent + IND + "double error = 2 * fabs(vector(regrid_field1, " + indexesRegriddingTag + ") - vector(regrid_field2, " + indexesRegriddingTag + "))/fabs(vector(regrid_field1, " + indexesRegriddingTag + ") + vector(regrid_field2, " + indexesRegriddingTag + "));" + NL
		    + loopIndent + IND + "vector(regridding_value, " + indexesRegriddingTag + ") = error;" + NL
		    + loopIndent + IND + "if (error > d_regridding_error) {" + NL
		    + loopIndent + IND + IND + "if (" + conditionInsideIndex + ") {" + NL
		    + loopIndent + IND + IND + IND + "vectorT(tags," + indexesTag + ") = 1;" + NL
		    + loopIndent + IND + IND + "}" + NL
		    + loopIndent + IND + IND + "vector(regridding_tag," + indexesRegriddingTag + ") = 1;" + NL
		    + indent(additionalTags, 1)
		    + loopIndent + IND + "}" + NL           
		    + indent(endLoop, 1)
		    + IND + IND + IND + "}" + NL;
		
		String lasts = "";
		for (int i = 0; i < dimensions; i++) {
		    lasts = lasts + IND + IND + IND + "int " + coordinates.get(i) + "last = boxlast(" + i + ")-boxfirst(" + i + ")+2+2*d_ghost_width;" 
		        + NL
		        + IND + IND + IND + "int " + coordinates.get(i) + "tlast = tlast(" + i + ")-tfirst(" + i + ")+1;" + NL;
		}
		
		String particleRegridTagging = "";
		String particleCheck = "";
		for (String speciesName: pi.getParticleSpecies()) {
		    String functionParticles = IND + IND + IND + "std::shared_ptr< pdat::IndexData<Particles<Particle_" + speciesName + ">, pdat::CellGeometry > > particleVariables_" + speciesName + "(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles<Particle_" + speciesName + ">, pdat::CellGeometry >, hier::PatchData>(patch->getPatchData(d_particleVariables_" + speciesName + "_id)));" + NL
		        	+ IND + IND + IND + "double* regridding_value = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_regridding_value_id).get())->getPointer();" + NL + NL
		        	+ IND + IND + IND + "const hier::Index boxfirstP = particleVariables_" + speciesName + "->getGhostBox().lower();" + NL
		        	+ IND + IND + IND + "const hier::Index boxlastP = particleVariables_" + speciesName + "->getGhostBox().upper();" + NL + NL
		        	+ startLoopParticles
		        	+ loopIndent + "hier::Index idx(" + idx + ");" + NL
		        	+ loopIndent + "Particles<Particle_" + speciesName + ">* part_" + speciesName + " = particleVariables_" + speciesName + "->getItem(idx);" + NL
		        	+ loopIndent + "for (int pit = part_" + speciesName + "->getNumberOfParticles() - 1; pit >= 0; pit--) {" + NL
		        	+ loopIndent + IND + "Particle_" + speciesName + "* particle = part_" + speciesName + "->getParticle(pit);" + NL
		        	+ loopIndent + IND + "vector(regridding_value, " + indexesRegriddingTag + ") = particle->getFieldValue(d_regridding_field);" + NL
		        	+ loopIndent + IND + "if (vector(regridding_value, " + indexesRegriddingTag + ") > d_regridding_threshold) {" + NL
		        	+ loopIndent + IND + IND + "if (" + conditionInsideIndex + ") {" + NL
		        	+ loopIndent + IND + IND + IND + "vectorT(tags," + indexesTag + ") = 1;" + NL
		        	+ loopIndent + IND + IND + "}" + NL
		        	+ loopIndent + IND + IND + "vector(regridding_tag," + indexesRegriddingTag + ") = 1;" + NL
		        	+ regriddingBuffer
		        	+ loopIndent + IND + "}" + NL	            	
		        	+ loopIndent + "}" + NL
					+ endLoop;
		    String shadowParticles = IND + IND + IND + "if (std::find(particleVariableList_" + speciesName + ".begin(), particleVariableList_" + speciesName + ".end(), d_regridding_field_shadow) == particleVariableList_" + speciesName + ".end()) {" + NL
		        	+ IND + IND + IND + IND + "TBOX_ERROR(d_object_name << \": Regridding field selected not found:\" <<  d_regridding_field_shadow<<  \"\");" + NL
		        	+ IND + IND + IND + "}" + NL
		        	+ IND + IND + IND + "std::shared_ptr< pdat::IndexData<Particles<Particle_" + speciesName + ">, pdat::CellGeometry > > particleVariables_" + speciesName + "(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles<Particle_" + speciesName + ">, pdat::CellGeometry >, hier::PatchData>(patch->getPatchData(d_particleVariables_" + speciesName + "_id)));" + NL
		        	+ IND + IND + IND + "double* regridding_value = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_regridding_value_id).get())->getPointer();" + NL
		        	+ IND + IND + IND + "const hier::Index boxfirstP = particleVariables_" + speciesName + "->getGhostBox().lower();" + NL
		        	+ IND + IND + IND + "const hier::Index boxlastP = particleVariables_" + speciesName + "->getGhostBox().upper();" + NL + NL
		            + startLoopParticles
		        	+ loopIndent + "hier::Index idx(" + idx + ");" + NL
		        	+ loopIndent + "Particles<Particle_" + speciesName + ">* part_" + speciesName + " = particleVariables_" + speciesName + "->getItem(idx);" + NL
		        	+ loopIndent + "for (int pit = part_" + speciesName + "->getNumberOfParticles() - 1; pit >= 0; pit--) {" + NL
		        	+ loopIndent + IND + "Particle_" + speciesName + "* particle = part_" + speciesName + "->getParticle(pit);" + NL
		            + loopIndent + IND + "double error = fabs(particle->getFieldValue(d_regridding_field) - particle->getFieldValue(d_regridding_field_shadow));" + NL
		            + loopIndent + IND + "vector(regridding_value, " + indexesRegriddingTag + ") = error;" + NL
		            + loopIndent + IND + "if (error > d_regridding_error) {" + NL
		            + loopIndent + IND + IND + "if (" + conditionInsideIndex + ") {" + NL
		            + loopIndent + IND + IND + IND + "vectorT(tags," + indexesTag + ") = 1;" + NL
		            + loopIndent + IND + IND + "}" + NL
		            + loopIndent + IND + IND + "vector(regridding_tag," + indexesRegriddingTag + ") = 1;" + NL
		            + regriddingBuffer
		          	+ loopIndent + IND + "}" + NL	
		            + loopIndent + "}" + NL           
		            + endLoop;
			particleRegridTagging = particleRegridTagging + IND + IND + IND + "if (std::find(particleVariableList_" + speciesName + ".begin(), particleVariableList_" + speciesName + ".end(), d_regridding_field) != particleVariableList_" + speciesName + ".end()) {" + NL
		            + IND + IND + IND + IND + "//Particle " + speciesName + NL
		            + IND + IND + IND + IND + "if (d_regridding_type == \"GRADIENT\") {" + NL
		            + indent(gradientParticles, 2)
		            + IND + IND + IND + IND + "} else {" + NL
		            + IND + IND + IND + IND + IND + "if (d_regridding_type == \"FUNCTION\") {" + NL
		            + indent(functionParticles, 3)
		            + IND + IND + IND + IND + IND + "} else {" + NL
		            + IND + IND + IND + IND + IND + IND + "if (d_regridding_type == \"SHADOW\") {" + NL
		            + indent(shadowParticles, 4)
		            + IND + IND + IND + IND + IND + IND + "}" + NL
		            + IND + IND + IND + IND + IND + "}" + NL
		            + IND + IND + IND + IND + "}" + NL
		            + IND + IND + IND + "}" + NL;
			particleCheck = particleCheck + " && std::find(particleVariableList_" + speciesName + ".begin(), particleVariableList_" + speciesName + ".end(), d_regridding_field) == particleVariableList_" + speciesName + ".end()";
		}
		
		regrid = IND + "if (d_regridding && level_number + 1 >= d_regridding_min_level && level_number + 1 <= d_regridding_max_level) {" + NL
		        + IND + IND + "hier::VariableDatabase *vdb = hier::VariableDatabase::getDatabase();" + NL
		        + IND + IND + "if (!(vdb->checkVariableExists(d_regridding_field))" + particleCheck + ") {" + NL
		        + IND + IND + IND + "TBOX_ERROR(d_object_name << \": Regridding field selected not found:\\n\"" + IND
		        + IND + IND + IND + IND + "<<  d_regridding_field<<  \"\\n\");" + NL
		        + IND + IND + "}" + NL + NL    
		        + IND + IND + "std::shared_ptr< hier::PatchLevel > level(hierarchy->getPatchLevel(level_number));"
		        + NL + NL
		        + IND + IND + "for (hier::PatchLevel::iterator ip(level->begin()); ip != level->end(); ++ip) {" + NL
		        + IND + IND + IND + "const std::shared_ptr< hier::Patch >& patch = *ip;" + NL
		        + IND + IND + IND + "int* tags = ((pdat::CellData<int> *) patch->getPatchData(tag_index).get())->getPointer();" + NL
		        + IND + IND + IND + "int* regridding_tag = ((pdat::NodeData<int> *) patch->getPatchData(d_nonSync_regridding_tag_id).get())->getPointer();" + NL
		        + IND + IND + IND + "const std::shared_ptr<geom::CartesianPatchGeometry > " 
		        + "patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));" + NL
		        + IND + IND + IND + "const double* dx  = patch_geom->getDx();" + NL
		        + IND + IND + IND + "const hier::Index tfirst = patch->getPatchData(tag_index)->getGhostBox()" 
		        + ".lower();" + NL
		        + IND + IND + IND + "const hier::Index tlast  = patch->getPatchData(tag_index)->getGhostBox()" 
		        + ".upper();" + NL
		        + IND + IND + IND + "const hier::Index boxfirst = patch->getBox().lower();" + NL
		        + IND + IND + IND + "const hier::Index boxlast  = patch->getBox().upper();" + NL
		        + lasts
		        + startLoopTags
		        + loopIndent + "vectorT(tags," + indexesRegriddingTag + ") = 0;" + NL
		        + endLoop
		        + startLoop
		        + loopIndent + "vector(regridding_tag," + indexesRegriddingTag + ") = 0;" + NL
		        + endLoop
		        + IND + IND + IND + "if (vdb->checkVariableExists(d_regridding_field)) {" + NL
		        + IND + IND + IND + IND + "//Mesh" + NL
		        + IND + IND + IND + IND + "if (d_regridding_type == \"GRADIENT\") {" + NL
		        + indent(gradient, 2)
		        + IND + IND + IND + IND + "} else {" + NL
		        + IND + IND + IND + IND + IND + "if (d_regridding_type == \"FUNCTION\") {" + NL
		        + indent(function, 3)
		        + IND + IND + IND + IND + IND + "} else {" + NL
		        + IND + IND + IND + IND + IND + IND + "if (d_regridding_type == \"SHADOW\") {" + NL
		        + indent(shadow, 4)
		        + IND + IND + IND + IND + IND + IND + "}" + NL
		        + IND + IND + IND + IND + IND + "}" + NL
		        + IND + IND + IND + IND + "}" + NL
		        + IND + IND + IND + "}" + NL
		        + particleRegridTagging
		        + IND + IND + "}" + NL
		        + IND + IND + "//Buffer synchronization if needed" + NL
		        + IND + IND + "if (d_regridding_buffer > d_ghost_width) {" + NL
		        + IND + IND + IND + "d_tagging_fill->createSchedule(level)->fillData(0, false);" + NL
		        + IND + IND + "}" + NL
		        + IND + IND + "if (d_regridding_buffer > 0) {" + NL
		        + IND + IND + IND + "for (hier::PatchLevel::iterator ip(level->begin()); ip != level->end(); ++ip) {" + NL
		        + IND + IND + IND + IND + "const std::shared_ptr< hier::Patch >& patch = *ip;" + NL
		        + IND + IND + IND + IND + "int* tags = ((pdat::CellData<int> *) patch->getPatchData(tag_index).get())->getPointer();" + NL
		        + IND + IND + IND + IND + "int* regridding_tag = ((pdat::NodeData<int> *) patch->getPatchData(d_nonSync_regridding_tag_id).get())->getPointer();" + NL
		        + IND + IND + IND + IND + "const std::shared_ptr<geom::CartesianPatchGeometry > " 
		        + "patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));" + NL
		        + IND + IND + IND + IND + "const double* dx  = patch_geom->getDx();" + NL
		        + IND + IND + IND + IND + "const hier::Index tfirst = patch->getPatchData(tag_index)->getGhostBox()" 
		        + ".lower();" + NL
		        + IND + IND + IND + IND + "const hier::Index tlast  = patch->getPatchData(tag_index)->getGhostBox()" 
		        + ".upper();" + NL
		        + IND + IND + IND + IND + "const hier::Index boxfirst = patch->getBox().lower();" + NL
		        + IND + IND + IND + IND + "const hier::Index boxlast  = patch->getBox().upper();" + NL
		        + indent(lasts, 1)
		        + indent(startLoop, 1)
		        + loopIndent + IND + "int value = vector(regridding_tag, " + indexesRegriddingTag + ");" + NL
		        + loopIndent + IND + "if (value > 0 && value < 1 + d_regridding_buffer) {" + NL
		        + loopIndent + IND + IND + "int buffer_left = 1 + d_regridding_buffer - value;" + NL
		        + loopIndent + IND + IND + "int distance;" + NL
		        + indent(startLoopB, dimensions + 5)
		        + loopIndent + loopIndentB + IND + IND + "distance = " + distanceB + ";" + NL
		        + loopIndent + loopIndentB + IND + IND + "if (distance > 0" + conditionInsideIndexB + ") {" + NL
		        + loopIndent + loopIndentB + IND + IND + IND + "vectorT(tags, " + indexesTagB + ") = 1;" + NL
		        + loopIndent + loopIndentB + IND + IND + "}" + NL													
		        + loopIndent + loopIndentB + IND + IND + "if (distance > 0 && (vector(regridding_tag," + indexesRegriddingTagB + ") == 0 || vector(regridding_tag, " + indexesRegriddingTagB + ") > value + distance)) {" + NL
		        + loopIndent + loopIndentB + IND + IND + IND + "vector(regridding_tag, " + indexesRegriddingTagB + ") = value + distance;" + NL
		        + loopIndent + loopIndentB + IND + IND + "}" + NL
		        + indent(endLoopB, dimensions + 5)
		        + loopIndent + IND + "}" + NL
		        + indent(endLoop, 1)
		        + IND + IND + IND + "}" + NL
		        + IND + IND + "}" + NL
		        + IND + "}";
		
		return problem.replaceFirst("#applyGradientDetector#", regrid);
    }
    
    /**
     * Get code to access the index with a given arithmetic operation.
     * @param dimensions        The number of spatial coordinates
     * @param coordIndex        The number of the index to operate
     * @param operation         The operation
     * @return                  The index code
     */
    private String getIndexOperated(int dimensions, int coordIndex, String operation) {
        String index = "";
        for (int j = 0; j < dimensions; j++) {
            if (coordIndex == j) {
                index = index + "index" + j + operation + "+d_ghost_width, ";
            }
            else {
                index = index + "index" + j + "+d_ghost_width, ";
            }
        }
        return index.substring(0, index.lastIndexOf(","));
    }
    
    /**
     * Adds the particles functions.
     * @param pi            The problem information
     * @param movement      The movement code
     * @param vi            The variables indormation
     * @return              The problem with the tag replaced.
     * @throws CGException  CG00X External error
     */
    private String createParticlesFunctions(ProblemInfo pi, VariableInfo vi, String movement) throws CGException {
        String moveParticles = movement.substring(0, movement.indexOf("#"));
        String checkPosition = createCheckPositionProblem(pi.getContCoordinates(), pi.getVariableInfo(), pi.getTimeInterpolationInfo().getTimeInterpolators().size() > 1);
        String checkRegion = "";
        String initCommonVars = createInitCommonVars(pi.getContCoordinates(), pi.getCoordinates(), pi.getRegionIds());
        if (movement.indexOf("#checkRegion#") != -1) {
            checkRegion = movement.substring(movement.indexOf("#checkRegion#") + "#checkRegion#".length() + 1);
        }
        return moveParticles + checkPosition + checkRegion + NL + initCommonVars;
    }
 
    /**
     * Creates the initialization routine for the common variables in case of a regridding.
     * @param coords        The continuous spatial coordinates
     * @param discCoords    The discrete spatial coordinates
     * @param regions      The region ids of the problem
     * @return              The code
     */
    private String createInitCommonVars(ArrayList<String> coords, ArrayList<String> discCoords, ArrayList<String> regions) {
        int dims = coords.size();
        String coordDependentInit = "";
        for (int i = 0; i < dims; i++) {
            coordDependentInit = coordDependentInit + IND + coords.get(i) + "Glower = d_grid_geometry->getXLower()[" + i + "];" + NL
                + IND + coords.get(i) + "Gupper = d_grid_geometry->getXUpper()[" + i + "];" + NL
                + IND + discCoords.get(i) + "lastG = d_grid_geometry->getPhysicalDomain().front().numberCells()[" + i + "] + 2 * d_ghost_width;" + NL;
        }
        return "/*" + NL
            + " * Initialization of the common variables in case of restarting." + NL
            + " */" + NL
            + "void Problem::initCommonVars(const std::shared_ptr<hier::PatchHierarchy >& hierarchy)" + NL
            + "{" + NL
            + coordDependentInit
            + "}" + NL;
    }
    
    /**
     * create function ChecPosition.
     * @param contCoords            The continuous spatial coordinates
     * @param posVariables          The position variables
     * @return                      The function
     */
    private String createCheckPositionProblem(ArrayList<String> contCoords, VariableInfo vi, boolean multipleInterpolators) {
        String argumentsIndex = "";
        int dimensions = contCoords.size();
        
        for (int i = 0; i < dimensions; i++) {
            argumentsIndex = argumentsIndex + "int index" + i + ", ";
        }
        argumentsIndex = argumentsIndex.substring(0, argumentsIndex.lastIndexOf(", "));
        
        String checks = "";
        String indent = IND;
        for (Map.Entry<String, ArrayList<String>> entry : vi.getPositionVariables().entrySet()) {
        	String speciesName = entry.getKey();
        	ArrayList<String> posVariables = entry.getValue();
        	if (multipleInterpolators) {
        		checks = checks + IND + "if (std::is_same<T, Particle_" + speciesName + ">::value) {" + NL;
        		indent = indent + IND;
        	}
        	checks = checks + createCheckPosition(contCoords, posVariables, "->");
        	if (multipleInterpolators) {
        		checks = checks + IND + "}" + NL;
        	}
        }
        
        return "template<class T>" + NL
        	+ "void Problem::checkPosition(const std::shared_ptr<hier::Patch >& patch, " + argumentsIndex + ", Particles<T>* particles)" + NL
            + "{" + NL
            + IND + "const hier::Index boxfirst1 = patch->getBox().lower();" + NL
            + IND + "const hier::Index boxlast1  = patch->getBox().upper();" + NL + NL
            + checks
            + "}" + NL + NL;
    }
    
    /**
     * create function ChecPosition.
     * @param contCoords            The continuous spatial coordinates
     * @param posVariables          The position variables
     * @return                      The function
     */
    private String createCheckPositionParticleRefine(ArrayList<String> contCoords, VariableInfo vi, boolean multipleInterpolators) {
        String argumentsIndex = "";
        int dimensions = contCoords.size();
        for (int i = 0; i < dimensions; i++) {
            argumentsIndex = argumentsIndex + "int index" + i + ", ";
        }
        argumentsIndex = argumentsIndex.substring(0, argumentsIndex.lastIndexOf(", "));
        
        String checks = "";
        String indent = IND;
        for (Map.Entry<String, ArrayList<String>> entry : vi.getPositionVariables().entrySet()) {
        	String speciesName = entry.getKey();
        	ArrayList<String> posVariables = entry.getValue();
        	if (multipleInterpolators) {
        		checks = checks + IND + "if (std::is_same<T, Particle_" + speciesName + ">::value) {" + NL;
        		indent = indent + IND;
        	}
        	checks = checks + createCheckPosition(contCoords, posVariables, ".");
        	if (multipleInterpolators) {
        		checks = checks + IND + "}" + NL;
        	}
        }
        
        return "template<class T>" + NL
        	+ "void ParticleRefine<T>::checkPosition(const hier::Patch& patch, const hier::Box& box, " + argumentsIndex + ", Particles<T>& particles) const" + NL
            + "{" + NL
            + IND + "const hier::Index boxfirst1 = box.lower();" + NL
            + IND + "const hier::Index boxlast1  = box.upper();" + NL + NL
            + checks
            + "}" + NL + NL;
    }
    
    /**
     * create function ChecPosition.
     * @param contCoords            The continuous spatial coordinates
     * @param posVariables          The position variables
     * @param pointer				Pointer for classes
     * @return                      The function
     */
    private String createCheckPosition(ArrayList<String> contCoords, ArrayList<String> posVariables, String pointer) {
        String checking = "";
        String argumentsIndex = "";
        String checkCondition = "";
        String declarations = "";
        String setPositions = "";
        int dimensions = contCoords.size();
        
        for (int i = 0; i < posVariables.size(); i++) {
            String variable = posVariables.get(i);
            for (int j = 0; j < dimensions; j++) {
                if (variable.contains("position" + contCoords.get(j))) {
                    checking = checking + IND + IND + IND + "if (periodicL" + j + " && index" + j + " < boxfirst1[" + j + "] && "
                        + variable + " > " + contCoords.get(j) + "Glower) {" + NL
                        + IND + IND + IND + IND + variable + " = " + variable + " - (" + contCoords.get(j) + "Gupper - " 
                        + contCoords.get(j) + "Glower);" + NL
                        + IND + IND + IND + "}" + NL
                        + IND + IND + IND + "if (periodicU" + j + " && index" + j + " > boxlast1[" + j + "] && " + variable + " < " 
                        + contCoords.get(j) + "Gupper) {" + NL
                        + IND + IND + IND + IND + variable + " = " + variable + " + (" + contCoords.get(j) + "Gupper - " 
                        + contCoords.get(j) + "Glower);" + NL
                        + IND + IND + IND + "}" + NL;
                    declarations = declarations + IND + IND + IND + "double " + variable + " = particle->" + variable + ";" + NL;
                    setPositions = setPositions + IND + IND + IND + "particle->" + variable + " = " + variable + ";" + NL;
                }
            }
        }
        String lowerUppersAndPeriodic = "";
        for (int i = 0; i < dimensions; i++) {
            String contCoord = contCoords.get(i);
            argumentsIndex = argumentsIndex + "int index" + i + ", ";
            checkCondition = checkCondition + "index" + i + " < boxfirst1[" + i + "] || index" + i + " > boxlast1[" + i + "] || ";
            lowerUppersAndPeriodic = lowerUppersAndPeriodic + IND + "double " + contCoord + "lower = patch_geom->getXLower()[" + i + "];" + NL
                    + IND + "double " + contCoord + "upper = patch_geom->getXUpper()[" + i + "];" + NL
                    + IND + "bool periodicL" + i + " = greaterEq(" + contCoord + "Glower, " + contCoord 
                    + "lower) && !patch_geom->getTouchesRegularBoundary(" + i + ", 0);" + NL
                    + IND + "bool periodicU" + i + " = lessEq(" + contCoord + "Gupper, " + contCoord 
                    + "upper) && !patch_geom->getTouchesRegularBoundary(" + i + ", 1);" + NL;
        }
        argumentsIndex = argumentsIndex.substring(0, argumentsIndex.lastIndexOf(", "));
        checkCondition = checkCondition.substring(0, checkCondition.length() - "|| ".length());
        return IND + "std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch" + pointer + "getPatchGeometry()));" + NL + NL
            + lowerUppersAndPeriodic
            + IND + "if (" + checkCondition + ") {" + NL
            + IND + IND + "for(int pit = 0; pit < particles" + pointer + "getNumberOfParticles(); pit++) {" + NL
            + IND + IND + IND + "T* particle = particles" + pointer + "getParticle(pit);" + NL
            + declarations
            + checking
            + setPositions
            + IND + IND + "}" + NL
            + IND + "}" + NL;
    }
  
    /**
     * Creates the problem.h file from the template.
     * @param pi                The problem info
     * @param mapping           The mapping code
     * @param synchronization   The synchronization code
     * @param movement          The movement code
     * @return                  The code
     * @throws CGException      CG00X External error
     */
    private String createProblemH(ProblemInfo pi, String mapping, String movement, String synchronization) throws CGException {
        //Get the template
        String problemH = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/Problem.h"), 0);
        VariableInfo vi = pi.getVariableInfo();
        ArrayList<String> variables = vi.getMeshVariables();
        ArrayList<String> partVariables = vi.getAllParticleVariables();
        HashMap<String, String> parameters = vi.getParameters();
        ArrayList<String> coordinates = pi.getCoordinates();
        ArrayList<String> contCoordinates = pi.getContCoordinates();
        ArrayList<String> regions = pi.getRegionIds();
        
        //Dimension substitution
        int dimensions = pi.getDimensions();
        problemH = problemH.replaceAll("#DIM#", String.valueOf(dimensions));
        
        //Writers
        String slicerWriter = "";
        String sliceVariablesParam = "";
        String slicerVariable = "";
        if (activeSlicers) {
            slicerWriter = IND + "int setupSlicePlotter(vector<std::shared_ptr<SlicerDataWriter> > &plotters) const;" + NL
                    + IND + "int setupSpherePlotter(vector<std::shared_ptr<SphereDataWriter> > &plotters) const;" + NL;
            sliceVariablesParam = "," + NL
            		+ IND + IND + "const vector<int> slicer_output_period," + NL
                    + IND + IND + "const vector<set<string> > sliceVariables," + NL
                    + IND + IND + "vector<std::shared_ptr<SlicerDataWriter > >sliceWriters," + NL
                	+ IND + IND + "const vector<int> sphere_output_period," + NL
                    + IND + IND + "const vector<set<string> > sphereVariables," + NL
                    + IND + IND + "vector<std::shared_ptr<SphereDataWriter > > sphereWriters";
            if (vi.getOutputVarsAnalysis().size() > 0) {
            	sliceVariablesParam = sliceVariablesParam + "," + NL
            			+ IND + IND + "const vector<bool> slicer_analysis_output," + NL
            			 + IND + IND + "const vector<bool> sphere_analysis_output";
            }
            slicerVariable = IND + "vector<set<string> > d_sliceVariables;" + NL
                    + IND + "vector<set<string> > d_sphereVariables;" + NL;
        }
        String integrationVariablesParam = "";
        String pointVariablesParam = "";
        String integrationWriter = "";
        String pointWriter = "";
        String integrationVariable = "";
        String pointVariable = "";
        String meshWriter = "";
        if (pi.isHasMeshFields()) {
        	integrationVariablesParam = "," + NL
        			+ IND + IND + "const vector<int> integration_output_period," + NL
        			+ IND + IND + "const vector<set<string> > integralVariables," + NL
        			+ IND + IND + "vector<std::shared_ptr<IntegrateDataWriter > > integrateDataWriters";
        	pointVariablesParam = "," + NL
        			+ IND + IND + "const vector<int> point_output_period," + NL
        			+ IND + IND + "const vector<set<string> > pointVariables," + NL
        			+ IND + IND + "vector<std::shared_ptr<PointDataWriter > > pointDataWriters";
        	if (vi.getOutputVarsAnalysis().size() > 0) {
        		integrationVariablesParam = integrationVariablesParam + "," + NL
       				 + IND + IND + "const vector<bool> integration_analysis_output";
        		pointVariablesParam = pointVariablesParam + "," + NL
          				 + IND + IND + "const vector<bool> point_analysis_output";
       	 	}
        	integrationWriter = IND + "int setupIntegralPlotter(vector<std::shared_ptr<IntegrateDataWriter> > &plotters) const;" + NL;
        	pointWriter = IND + "int setupPointPlotter(vector<std::shared_ptr<PointDataWriter> > &plotters) const;" + NL;
        	integrationVariable = IND + "vector<set<string> > d_integralVariables;" + NL
        			+ IND + "int d_mask_id;" + NL;
        	pointVariable = IND + "vector<set<string> > d_pointVariables;" + NL;
        	meshWriter = IND + "int setupPlotterMesh(appu::VisItDataWriter &plotter ) const;" + NL;
        }
        String particleWriter = "";
        if (pi.isHasABMMeshlessFields() || pi.isHasParticleFields()) {
        	particleWriter = IND + "int setupPlotterParticles(ParticleDataWriter &plotter ) const;" + NL;

        }
        problemH = problemH.replaceFirst("#setupSimflownyPlotters#", meshWriter + particleWriter + slicerWriter + integrationWriter + pointWriter);
        problemH = problemH.replaceFirst("#sliceVariablesParam#", sliceVariablesParam);
        problemH = problemH.replaceFirst("#integrationVariablesParam#", integrationVariablesParam + pointVariablesParam);
        problemH = problemH.replaceFirst("#slicerVariable#", slicerVariable);
        problemH = problemH.replaceFirst("#integrationVariable#", integrationVariable + pointVariable);
        
        //Includes
        String includes = "";
        String particlesFunctions = "";
        String writerVariablesParam = "";
        String regridding = "";
        if (activeSlicers) {
            includes = "#include \"SlicerDataWriter.h\"" + NL
                    + "#include \"SphereDataWriter.h\"" + NL;
        }
        if (pi.isHasABMMeshFields() || pi.isHasMeshFields()) {
            includes = includes + "#include \"SAMRAI/appu/VisItDataWriter.h\"" + NL;
            
            if (pi.isHasMeshFields()) {
                includes = includes
                		+ "#include \"IntegrateDataWriter.h\"" + NL
                		+ "#include \"PointDataWriter.h\"" + NL;
            }
            if (pi.hasExternalInitialCondition()) {
                includes = includes + "#include \"ExternalInitialData.h\"" + NL;
            }
            if (pi.getX3dMovRegions() != null) {
                includes = includes + "#include \"SAMRAI/pdat/IndexData.h\"" + NL
                        + "#include \"Interphase.h\"";
            }
            writerVariablesParam = IND + IND + "const int mesh_output_period," + NL
            		+ IND + IND + "const vector<string> full_mesh_writer_variables," + NL
            		+ IND + IND + "std::shared_ptr<appu::VisItDataWriter>& data_writer, " + NL;
        }
        if (pi.isHasABMMeshlessFields() || pi.isHasParticleFields()) {
            includes = includes + "#include \"Particles.h\"" + NL
                    + "#include \"SAMRAI/pdat/CellData.h\"" + NL
                    + "#include \"SAMRAI/pdat/IndexData.h\"" + NL
                    + "#include \"SAMRAI/pdat/CellVariable.h\"" + NL
                    + "#include \"SAMRAI/pdat/CellGeometry.h\"" + NL
                    + "#include \"ParticleDataWriter.h\"" + NL
            		+ "#include \"ParticleRefine.h\"" + NL;
            //Particles Functions
            particlesFunctions = getParticlesFunctions(coordinates, movement);
            writerVariablesParam = writerVariablesParam + IND + IND + "const int particles_output_period," + NL
            		+ IND + IND + "const vector<string> full_particle_writer_variables," + NL
            		+ IND + IND + "std::shared_ptr<ParticleDataWriter>& particle_data_writer, " + NL;
        }
        if (pi.isHasABMMeshFields() || pi.isHasMeshFields() || pi.isHasParticleFields()) {
            includes = includes + "#include \"TimeInterpolator.h\"" + NL;
        	regridding = IND + "//regridding options" + NL
                    + IND + "std::shared_ptr<tbox::Database> regridding_db;" + NL
                    + IND + "bool d_regridding;" + NL
                    + IND + "std::string d_regridding_field, d_regridding_type, d_regridding_field_shadow;" + NL
                    + IND + "double d_regridding_threshold, d_regridding_compressionFactor, d_regridding_mOffset, d_regridding_error, d_regridding_buffer;" + NL
                    + IND + "int d_regridding_min_level, d_regridding_max_level;" + NL;
        }
        problemH = problemH.replaceFirst("#regridding#", regridding);
        problemH = problemH.replaceFirst("#includes#", includes);
        problemH = problemH.replaceFirst("#ParticlesFunctions#", particlesFunctions);
        writerVariablesParam = writerVariablesParam.substring(0, writerVariablesParam.lastIndexOf(","));
        problemH = problemH.replaceFirst("#writerVariablesParam#", writerVariablesParam);

        
        //Post initialization
        String postInit = "";
        if (vi.getOutputVarsAnalysis().size() > 0) {
            postInit = IND + "void postInit();" + NL;
        }
        problemH = problemH.replaceFirst("#PostInit#", postInit);
        
        //mapping functions
        String mappingFunctions = IND + "void mapDataOnPatch(const double time, const bool initial_time, const int ln, const " 
                + "std::shared_ptr< hier::PatchLevel >& level);" + NL + mapping.substring(mapping.indexOf("#mappingDeclarations#") 
                + "#mappingDeclarations#".length(), mapping.indexOf("#endMappingDeclarations#"));
        if (pi.isHasParticleFields()) {
        	mappingFunctions = mappingFunctions+ IND + "template<class T>" + NL
        			+ IND + "void mapDataOnParticles(const std::shared_ptr< hier::PatchLevel >& level, std::string particleDistribution, "
        					+ "std::vector<double> number_of_particles, std::vector<double> particleSeparation, "
        					+ "std::vector<double> domain_offset_factor, std::vector<double> normal_mean, "
        					+ "std::vector<double> normal_stddev, std::vector<double> box_min, std::vector<double> box_max);" + NL;
        }
        problemH = problemH.replaceFirst("#mappingFunctions#", mappingFunctions);
                    
        //interphase functions
        String interphase = "";
        if (!pi.isHasABMMeshlessFields()) {
            interphase = IND + "/*" + NL
                    + IND + " * Interphase mapping. Calculates the FOV and its variables." + NL
                    + IND + " */" + NL
                    + IND + "void interphaseMapping(const double time, const bool initial_time, const int ln, const std::shared_ptr< "
                    + "hier::PatchLevel >& level, const int remesh);" + NL + NL;
        }
        problemH = problemH.replaceFirst("#interphaseMappingFunction#", interphase);
        if (pi.isHasABMMeshlessFields()) {
            problemH = problemH.replaceFirst("#interphaseMappingDeclarations#", "");
        }
        else {
            problemH = problemH.replaceFirst("#interphaseMappingDeclarations#", 
                    mapping.substring(mapping.indexOf("#interphaseMappingDeclarations#") 
                    + "#interphaseMappingDeclarations#".length(), mapping.indexOf("#x3dArrays#")));
        }
        
        if (pi.getX3dMovRegions() != null) {
            problemH = problemH.replaceFirst("#interphaseMovementFunction#", 
                    movement.substring(movement.indexOf("#interphaseMovementDeclaration#") 
                    + "#interphaseMovementDeclaration#".length()));
        }
        else {
            problemH = problemH.replaceFirst("#interphaseMovementFunction#", "");
        }
        
        //x3d arrays
        problemH = problemH.replaceFirst("#x3dArrays#", "#define DIMENSIONS " + pi.getDimensions() + NL
                 + mapping.substring(mapping.indexOf("#x3dArrays#") + "#x3dArrays#".length()));    
        //Variable declaration
        String vars = IND + "int ";
        if (pi.isHasABMMeshlessFields() || pi.isHasParticleFields()) {
            for (String speciesName: pi.getParticleSpecies()) {
            	vars = vars + "d_particleVariables_" + speciesName + "_id, ";
            }
        }
        if (pi.isHasABMMeshFields() || pi.isHasMeshFields()) {
            for (int i = 0; i < variables.size(); i++) {
                String name = variables.get(i);
                if (!partVariables.contains(name)) {
                	vars = vars + "d_" + name + "_id, ";
                }
            }
            if (pi.getX3dMovRegions() != null) {
                vars = vars + "d_interphase_id, d_soporte_id, ";
                ArrayList<String> elements = new ArrayList<String>();
                for (int i = 0; i < INTERPHASELENGTH; i++) {
                    elements.add(String.valueOf(i));
                }
                ArrayList<String> combinations = CodeGeneratorUtils.createCombinations(pi.getDimensions(), elements);
                for (int i = 0; i < regions.size(); i++) {
                    String segId = regions.get(i);
                    if (CodeGeneratorUtils.isNumber(segId)) {
                        vars = vars + "d_LS_" + segId + "_id, ";
                        for (int j = 0; j < combinations.size(); j++) {
                            String comb = combinations.get(j);
                            vars = vars + "d_ls_" + segId + "_" + comb + "_id, ";
                        }
                    }
                }
                ArrayList<String> tmp = new ArrayList<String>();
                tmp.addAll(regions);
                while (!tmp.isEmpty()) {
                    String id = tmp.remove(0);
                    if (CodeGeneratorUtils.isNumber(id)) {
                        for (int i = 0; i < tmp.size(); i++) {
                            if (CodeGeneratorUtils.isNumber(tmp.get(i))) {
                                String comb = id + tmp.get(i);
                                for (int j = 0; j < pi.getDimensions(); j++) {
                                    String coord = pi.getCoordinates().get(j);
                                    vars = vars + "d_normal" + coord + "_" + comb + "_id, d_velocity" + coord + "_" + comb + "_id, ";
                                }
                            }
                        }
                    }
                }
            }
        }
        vars = vars.substring(0, vars.lastIndexOf(",")) + ";" + NL;
        //File declarations
        FileReadInformation fri = pi.getFileReadInfo();
        for (String fileName : fri.getFileNameInfo().keySet()) {
        	int nCoords = fri.getFileNameInfo().get(fileName);
        	String coordData = "";
            for (int k = 0; k < nCoords; k++) {
              	coordData = coordData + IND + "double* coord" + k + "_data_" + fileName + ";" + NL
            			+ IND + "double coord" + k + "_dx_" + fileName + ";" + NL
            			+ IND + "int coord" + k + "_size_" + fileName + ";" + NL; 
            }
            vars = vars + IND + "//Files to read" + NL
            		+ IND + "double* vars_data_" + fileName + ";" + NL
            		+ IND + "int vars_size_" + fileName + ";" + NL
            		+ coordData;
        }
        problemH = problemH.replaceFirst("#field declaration#", vars);
        
        //Sync. Variables
        String syncVars = synchronization.substring(synchronization.indexOf("#Problem.h variables#") 
                + "#Problem.h variables#".length(), synchronization.indexOf("#Problem variables#"));
        if (pi.getX3dMovRegions() != null) {
            syncVars = syncVars + IND + "std::shared_ptr< xfer::RefineAlgorithm > d_mapping_fill2;" + NL;
        }
        problemH = problemH.replaceFirst("#Problem.h variables#", syncVars);
        problemH = problemH.replaceFirst("#coarsen declarations#", synchronization.substring(synchronization.indexOf("#Coarsen declarations#") 
                + "#Coarsen declarations#".length(), synchronization.indexOf("#Coarsen registrations#")));        
         
        String fill_new_aux = "";
        if (pi.getPostRefinementAuxiliaryFields().size() > 0) {
        	fill_new_aux = ", d_fill_new_level_aux";
        }
    	if (pi.getDerivativeAuxiliaryFields().size() > 0) {
    		fill_new_aux = fill_new_aux + ", d_fill_new_level_der_aux";
    	}
        problemH = problemH.replaceFirst("#fill_new_aux#", fill_new_aux);
        
        
        //Parameter declaration
        problemH = problemH.replaceFirst("#parameter declaration#", getParameterDeclaration(parameters, pi));
        //Variables for SPH or ABM
        String particleVariables = "";
        if (pi.isHasParticleFields()) {
        	String speciesDistribution = "";
        	String speciesVectors = "";
        	for (String speciesName: pi.getParticleSpecies()) {
        		speciesDistribution = speciesDistribution + "particleDistribution_" + speciesName + ", ";
        		speciesVectors = speciesVectors + "number_of_particles_" + speciesName + ", particleSeparation_" + speciesName 
        				+ ", domain_offset_factor_" + speciesName + ", normal_mean_" + speciesName + ", normal_stddev_" + speciesName 
        				+ ", box_min_" + speciesName + ", box_max_" + speciesName + ", ";
        	}
        	speciesDistribution = speciesDistribution.substring(0, speciesDistribution.lastIndexOf(", "));
        	speciesVectors = speciesVectors.substring(0, speciesVectors.lastIndexOf(", "));
            particleVariables = IND + "//Particle variables" + NL
                + IND + "double influenceRadius;" +  NL
                + IND + "string " + speciesDistribution + ";" +  NL
                + IND + "std::vector<double> " + speciesVectors + ";" + NL 
                + IND + "int mapStencil;" +  NL;
            for (int i = 0; i < contCoordinates.size(); i++) {
                String coord = contCoordinates.get(i);
                String discCoord = coordinates.get(i);
                particleVariables = particleVariables 
                    + IND + "int mapStencil" + coord + ";" + NL
                    + IND + "std::vector<std::string> resultVars_" + coord + ";" + NL
                    + IND + "double* vars_" + coord + ";" + NL
                    + IND + "double particleSeparation_" + coord + ";" + NL
                    + IND + "double " + coord + "Glower, " + coord + "Gupper;" + NL
                    + IND + "int min" + discCoord + "Index, max" + discCoord + "Index;" + NL
                    + IND + "int " + discCoord + "lastG;" + NL;
            }
        }
        if (pi.isHasABMMeshlessFields()) {
            particleVariables = IND + "//Particle variables" + NL
                + IND + "double influenceRadius;" +  NL
                + IND + "string particleDistribution;" + NL
                + IND + "int mapStencil;" +  NL
                + IND + "double density_par;" +  NL
                + IND + "int numberOfParticles;" + NL;
            for (int i = 0; i < contCoordinates.size(); i++) {
                String coord = contCoordinates.get(i);
                String discCoord = coordinates.get(i);
                particleVariables = particleVariables 
                    + IND + "double numberOfCellsRadius_" + coord + ";" + NL
                    + IND + "double " + coord + "Glower, " + coord + "Gupper;" + NL
                    + IND + "int min" + discCoord + "Index, max" + discCoord + "Index;" + NL
                    + IND + "int " + discCoord + "lastG;" + NL
                    + IND + "double particleSeparation_" + coord + ";" + NL;
            }
        }
        //Variables for region movement
        if (pi.getX3dMovRegions() != null) {
            particleVariables = IND + "double ";
            for (int i = 0; i < coordinates.size(); i++) {
                String coord = contCoordinates.get(i);
                particleVariables = particleVariables + coord + "Glower, " + coord + "Gupper, ";
            }
            particleVariables = particleVariables.substring(0, particleVariables.length() - 2) + ";" + NL;
        }
        problemH = problemH.replaceFirst("#particles variables#", particleVariables);
        String mappingVariables = "";
        for (int i = 0; i < pi.getDimensions(); i++) {
            String coord = pi.getCoordinates().get(i);
            mappingVariables = mappingVariables + ", d_interior_" + coord + "_id";
        }
        if (pi.isHasABMMeshlessFields() || pi.isHasParticleFields()) {
        	mappingVariables = mappingVariables + ", d_nonSyncP_id, d_region_id";
        }
        if (pi.isHasABMMeshFields() || pi.isHasMeshFields() || pi.isHasParticleFields()) {
            for (int i = 0; i < regions.size(); i++) {
            	mappingVariables = mappingVariables + ", d_FOV_" + regions.get(i) + "_id";
            }
        }
        problemH = problemH.replaceFirst("#mapping#", mappingVariables);
        String fileWriterVariables = "";
        if (activeSlicers) {
        	fileWriterVariables = fileWriterVariables + IND + "vector<std::shared_ptr<SlicerDataWriter > > d_sliceWriters;" + NL
        			+ IND + "vector<std::shared_ptr<SphereDataWriter > > d_sphereWriters;" + NL
        			+ IND + "vector<int> d_slicer_output_period;" + NL
        			+ IND + "vector<int> d_sphere_output_period;" + NL
        			+ IND + "vector<int> next_slice_dump_iteration;" + NL
        			+ IND + "vector<int> next_sphere_dump_iteration;" + NL;
        	if (vi.getOutputVarsAnalysis().size() > 0) {
        		fileWriterVariables = fileWriterVariables + IND + "vector<bool> analysis_slice_dump;" + NL
        				 + IND + "vector<bool> analysis_sphere_dump;" + NL;
	       	}
        }
        if (pi.isHasABMMeshFields() || pi.isHasMeshFields()) {
        	fileWriterVariables = fileWriterVariables + IND + "int viz_mesh_dump_interval;" + NL
        			+ IND + "int next_mesh_dump_iteration;" + NL
        			+ IND + "set<string> d_full_mesh_writer_variables;" + NL
        			+ IND + "std::shared_ptr<appu::VisItDataWriter > d_visit_data_writer;" + NL;
            if (pi.isHasMeshFields()) {
            	fileWriterVariables = fileWriterVariables + IND + "vector<std::shared_ptr<IntegrateDataWriter > > d_integrateDataWriters;" + NL
            			+ IND + "vector<int> d_integration_output_period;" + NL
            			+ IND + "vector<int> next_integration_dump_iteration;" + NL
            			+ IND + "vector<std::shared_ptr<PointDataWriter > > d_pointDataWriters;" + NL
            			+ IND + "vector<int> d_point_output_period;" + NL
            			+ IND + "vector<int> next_point_dump_iteration;" + NL;
            	if (vi.getOutputVarsAnalysis().size() > 0) {
            		fileWriterVariables = fileWriterVariables + IND + "vector<bool> analysis_integration_dump;" + NL
            				 + IND + "vector<bool> analysis_point_dump;" + NL;
    	       	}
            }
        }
        if (pi.isHasABMMeshlessFields() || pi.isHasParticleFields()) {
        	fileWriterVariables = fileWriterVariables + IND + "int viz_particle_dump_interval;" + NL
        			+ IND + "int next_particle_dump_iteration;" + NL
        			+ IND + "set<string> d_full_particle_writer_variables;" + NL
        			+ IND + "std::shared_ptr<ParticleDataWriter> d_particle_data_writer;" + NL;
        }
        problemH = problemH.replaceFirst("#fileWriterVariables#", fileWriterVariables);
        
        String postNewLevelFunction = "";
        if (CodeGeneratorUtils.find(pi.getProblem(), "/*/mms:region/mms:postRefinementAlgorithm/*/sml:simml|"
        		+ "/*/mms:subregions/mms:subregion/mms:postRefinementAlgorithm/*/sml:simml").getLength() > 0) {
        	postNewLevelFunction = IND + "virtual void postNewLevel(const std::shared_ptr< hier::PatchLevel >& level);";
        }
       	problemH = problemH.replaceAll("#postNewLevelFunction#", postNewLevelFunction);
       	String particleVariableList = "";
       	for (String speciesName : pi.getParticleSpecies()) {
       		particleVariableList = particleVariableList + IND + "vector<string> particleVariableList_" + speciesName + ";" + NL;
       	}
       	problemH = problemH.replaceAll("#particleVariableList#", particleVariableList);
        
        return problemH;
    }
  
    /**
     * Creates the commons header.
     * @param pi				Problem information
     * @return					The code
     * @throws CGException		CG00X External error
     */
    private String createCommonsH(ProblemInfo pi) throws CGException {
        String commonsH = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/Commons.h"), 0);
        
        String constMacros = "";
        //Constants
        HashMap<String, String> constants = pi.getConstants();
        constMacros = "//Simulation constants " + NL;
        Iterator<String> constantsIt = constants.keySet().iterator();
        while(constantsIt.hasNext()) {
        	String constant = constantsIt.next();
        	String value = constants.get(constant);
        	constMacros = constMacros + "#define " + constant + " " + value + NL;
        }
        
        //Vector macro replace
        constMacros = constMacros + createVectorMacros(pi.getDimensions(), pi.getCoordinates(), pi);
        
        commonsH = commonsH.replaceFirst("#constMacros#", constMacros);
        
        return commonsH;
    }
    
    /**
     * Gets the parameter declarations.
     * @param parameters        The parameters
     * @param pi 				The problem information
     * @return                  The declaration code
     * @throws CGException      SP001 Incorrect source code
     */
    private String getParameterDeclaration(HashMap<String, String> parameters, ProblemInfo pi) throws CGException {
        String parameterDeclaration = "";
        Iterator<String> param = parameters.keySet().iterator();
        while (param.hasNext()) {
            String name = param.next();
            String type = parameters.get(name);
            String typeDeclare;
            if (type.toUpperCase().equals("DOUBLE")) {
                typeDeclare = "double ";
            } 
            else {
                if (type.toUpperCase().equals("INT")) {
                    typeDeclare = "int ";
                }
                else {
                    if (type.toUpperCase().equals("STRING")) {
                        typeDeclare = "std::string ";
                    }
                    else {
                        if (type.toUpperCase().equals("BOOLEAN")) {
                            typeDeclare = "bool ";
                        }
                        else {
                            throw new CGException(CGException.CG001, "Parameter type not valid.");
                        }
                    }
                }
            }
            parameterDeclaration = parameterDeclaration + IND + typeDeclare + name + ";" + NL;
        }
        if (pi.getX3dMovRegions() != null) {
            parameterDeclaration = parameterDeclaration + IND + "int corridor_width;" + NL;
        }
        return parameterDeclaration;
    }
    
    /**
     * Get the particle functions declarations.
     * @param coordinates   The spatial coordinates
     * @param movement      The movement file
     * @return              The declarations
     */
    private String getParticlesFunctions(ArrayList<String> coordinates, String movement) {
        int dims = coordinates.size();
        //Arguments
        String arguments = "";
        for (int i = 0; i < coordinates.size(); i++) {
            arguments = arguments + "int " + coordinates.get(i) + ", ";
        }
        arguments = arguments.substring(0, arguments.length() - 2);
        //CheckRegion
        String checkRegion = "";
        if (movement.lastIndexOf("#checkRegion#") != -1) {
            String newIndex = "";
            String index = "";
            for (int i = 0; i < dims; i++) {
                newIndex = newIndex + ", const int newIndex" + i;
                index = index + ", const int index" + i;
            }
            checkRegion = IND + "template<class T>" + NL
            		+ IND + "void checkRegion(T* particle" + newIndex + index 
                    + ", const hier::Index boxfirst1, const hier::Index boxlast1);" + NL;
        }
        //MoveDeclaration
        String moveDeclaration = "";
        if (movement.lastIndexOf("#checkRegion#") != -1) {
            moveDeclaration  = CodeGeneratorUtils.incrementIndent(movement.substring(movement.indexOf("#moveDeclarations#") + "#moveDeclarations#".length() + 1,
                    movement.indexOf("#checkRegion#")), 1);
        }
        else {
            moveDeclaration  = CodeGeneratorUtils.incrementIndent(movement.substring(movement.indexOf("#moveDeclarations#") + "#moveDeclarations#".length() + 1), 1);
        }
        
        return checkRegion
                + IND + "template<class T>" + NL
                + IND + "void checkPosition(const std::shared_ptr<hier::Patch >& patch, " + arguments + ", Particles<T>* particles);" + NL
                + moveDeclaration
                + IND + "void initCommonVars(const std::shared_ptr<hier::PatchHierarchy >& hierarchy);" + NL
                + IND + "template<class T>" + NL
                + IND + "void calculateCellInfluence(hier::Patch& patch) const;" + NL;
    }
    
    /**
     * Create particle.c file.
     * @param pi                The problem info
     * @return                  The code
     * @throws CGException      CG00X External error
     */
    private String createParticle(ProblemInfo pi, String speciesName) throws CGException {
        try {
            //Get the template
            String particle = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/Particle.C"), 0);
            VariableInfo vi = pi.getVariableInfo();
            ArrayList<String> variables = vi.getParticleVariables().get(speciesName);
            ArrayList<String> posVariables = pi.getVariableInfo().getPositionVariables(speciesName);
            ArrayList<String> contCoords = pi.getContCoordinates();
            //Set positions
            String posInit = "";
            String posInit2 = "";
            String posAssign = "";
            String db1p = "";
            String pdb1 = "";
            String overlaps = "";
            for (int i = 0; i < posVariables.size(); i++) {
                String name = posVariables.get(i);
                posInit2 = posInit2 + IND + "this->" + name + " = data." + name + ";" + NL;
                posAssign = posAssign + IND + IND + name + " = data." + name + ";" + NL;
                db1p = db1p + IND + "dbuffer1[" + (variables.size() + i) + "] = " + name + ";" + NL;
                pdb1 = pdb1 + IND + name + " = dbuffer1[" + (variables.size() + i) + "];" + NL;
                for (int j = 0; j < contCoords.size(); j++) {
                    if (name.equals("position" + contCoords.get(j))) {
                        posInit = posInit + IND + name + " = pos[" + j + "];" + NL;
                        overlaps = overlaps + "!Equal(" + name + ", data." + name + ") || ";
                    }
                }
            }
            if (!overlaps.equals("")) {
            	overlaps = overlaps.substring(0, overlaps.lastIndexOf(" ||"));
            }
            
            particle = particle.replaceFirst("#position initial assignment#", posInit);
            particle = particle.replaceAll("#position initial assignment 2#", posInit2);
            particle = particle.replaceAll("#dbuffer1 = position#", db1p);
            particle = particle.replaceAll("#position = dbuffer1#", pdb1);
            particle = particle.replaceFirst("#position assignation#", posAssign);
            particle = particle.replaceAll("#positionsNumber#", String.valueOf(posVariables.size()));
            particle = particle.replaceAll("#overlaps#", overlaps);
            //Fields
            String fieldInit = "";
            String fieldInit2 = "";
            String db1f = "";
            String fdb1 = "";
            String fieldAssign = "";
            for (int i = 0; i < variables.size(); i++) {
                String name = variables.get(i);
                fieldInit = fieldInit + IND + "this->" + name + " = fieldValue;" + NL;
                fieldInit2 = fieldInit2 + IND + "this->" + name + " = data." + name + ";" + NL;
                fieldAssign = fieldAssign + IND + IND + name + " = data." + name + ";" + NL;
                db1f = db1f + IND + "dbuffer1[" + i + "] = " + name + ";" + NL;
                fdb1 = fdb1 + IND + name + " = dbuffer1[" + i + "];" + NL;
            }
            String hasField = "";
            String getFieldValue = "";
            String setFieldValue = "";
            for (String var : variables) {
                getFieldValue = getFieldValue + IND + "if (varName == \"" + var + "\")" + NL
                    + IND + IND + "return " + var + ";" + NL + NL;
                hasField = hasField + IND + "if (varName == \"" + var + "\")" + NL
	                    + IND + IND + "return true;" + NL + NL;
                setFieldValue = setFieldValue + IND + "if (varName == \"" + var + "\")" + NL
                        + IND + IND + var + " = value;" + NL + NL;
            }
            particle = particle.replaceFirst("#field initial assignment#", fieldInit);
            particle = particle.replaceAll("#field initial assignment 2#", fieldInit2);
            particle = particle.replaceAll("#dbuffer1 = field#", db1f);
            particle = particle.replaceAll("#field = dbuffer1#", fdb1);
            particle = particle.replaceFirst("#field assignation#", fieldAssign);
            particle = particle.replaceFirst("#hasField#", hasField);
            particle = particle.replaceFirst("#getFieldValue#", getFieldValue);
            particle = particle.replaceFirst("#setFieldValue#", setFieldValue);
            particle = particle.replaceAll("#fieldsNumber#", String.valueOf(variables.size()));
            particle = particle.replaceAll("#SpeciesName#", speciesName);

            return particle;
        } 
        catch (Exception e) {
        	e.printStackTrace();
            throw new CGException(CGException.CG00X, e.getMessage());
        }
    }
    
    /**
     * Get the code for the distances between particles.
     * @param contCoords        The continuous coordinates
     * @param posVariables      The position variables
     * @param speciesName		Species name
     * @return                  The code
     */
    private String getDistances(ArrayList<String> contCoords, ArrayList<String> posVariables, String speciesName) {
        String distances = "";
        ArrayList<String> candidates = new ArrayList<String>();
        for (int i = 0; i < posVariables.size(); i++) {
            String name = posVariables.get(i);
            name = name.replaceAll("position[A-Za-z]", "position");
            for (int j = 0; j < contCoords.size(); j++) {
                String distName = name.replaceAll("position", "");
                if (!candidates.contains(distName)) {
                    String distDec = "";
                    String distPoint = "";
                    String product = "";
                    for (int k = 0; k < contCoords.size(); k++) {
                        String coord = contCoords.get(k);
                        String position = name.replaceAll("position", "position" + coord);
                        distDec = distDec + IND + IND + "double " + coord + "d = (data->" + position + " - " 
                            + position + ");" + NL;
                        distPoint = distPoint + IND + IND + "double " + coord + "d = (point[" + k + "] - " 
                            + position + ");" + NL;

                        product = product + " + " + coord + "d * " + coord + "d";
                    }
                    product = product.substring(" + ".length());
                    distances = distances + IND + "inline double distance" + distName + "(Particle_" + speciesName + "* data)" + NL
                        + IND + "{" + NL
                        + distDec
                        + IND + IND + "return sqrt(" + product + ");" + NL
                        + IND + "}" + NL + NL
                        + IND + "inline double distance" + distName + "(double* point)" + NL
                        + IND + "{" + NL
                        + distPoint
                        + IND + IND + "return sqrt(" + product + ");" + NL
                        + IND + "}" + NL;
                    candidates.add(distName);
                }
            }
        }
        return distances;
    }
    
    /**
     * Create particle.h file.
     * @param pi                The problem info
     * @return                  The code
     * @throws CGException      CG00X External error
     */
    private String createParticleH(ProblemInfo pi, String speciesName) throws CGException {
        try {
            //Get the template
            String particleH = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/Particle.h"), 0);
            VariableInfo vi = pi.getVariableInfo();
            ArrayList<String> variables = vi.getParticleVariables().get(speciesName);
            ArrayList<String> posVariables = vi.getPositionVariables(speciesName);
            ArrayList<String> contCoords = pi.getContCoordinates();

            //Set positions
            String position = "";
            String distance = "";
            String overlaps = "";
            ArrayList<String> candidates = new ArrayList<String>();
            for (int i = 0; i < posVariables.size(); i++) {
                String name = posVariables.get(i);
                position = position + IND + "double " + name + ";" + NL;
                name = name.replaceAll("position[A-Za-z]", "position");
                for (int j = 0; j < contCoords.size(); j++) {
                    String distName = name.replaceAll("position", "");
                    if (!candidates.contains(distName)) {
                        candidates.add(distName);
                        distance = distance + IND + "double distance" + distName + "(Particle* data);" + NL
                        	+ IND + "double distance" + distName + "(double* point);" + NL;
                    }
                    if (posVariables.get(i).equals("position" + contCoords.get(j))) {
                        overlaps = overlaps + "!Equal(" + posVariables.get(i) + ", data." + posVariables.get(i) + ") || ";
                    }
                }
            }
            if (!overlaps.equals("")) {
            	overlaps = overlaps.substring(0, overlaps.lastIndexOf(" ||"));
            }
                       
            particleH = particleH.replaceFirst("#positions#", position);
            particleH = particleH.replaceFirst("#distances#", getDistances(contCoords, posVariables, speciesName));
            particleH = particleH.replaceFirst("#positionsNumber#", String.valueOf(posVariables.size()));
            particleH = particleH.replaceAll("#overlaps#", overlaps);
            
            //Fields
            String field = "";
            for (int i = 0; i < variables.size(); i++) {
                String name = variables.get(i);
                field = field + IND + "double " + name + ";" + NL;
            }
            
            particleH = particleH.replaceFirst("#fields#", field);
            particleH = particleH.replaceAll("#fieldsNumber#", String.valueOf(variables.size()));
            particleH = particleH.replaceAll("#SpeciesName#", speciesName);
            
            return particleH;
        } 
        catch (Exception e) {
            throw new CGException(CGException.CG00X, e.getMessage());
        }
    }
    
    
    /**
     * Create particles.C file.
     * @param ti                The time interpolation info
     * @return                  The code
     * @throws CGException      CG00X External error
     */
    private String createParticles(TimeInterpolationInfo ti, HashMap<String, ArrayList<String>> particleVariables) throws CGException {
        String particles = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/Particles.C"), 0);
        
        String timeInitialization = "";
        String packtime = "";
        String unpacktime = "";
        String puttime = "";
        String gettime = "";
        if (ti.getTimeInterpolators().size() > 0) {
        	for (TimeInterpolator timeInterp: ti.getTimeInterpolators().values()) {
	        	int nvars = timeInterp.getVarAtStep().size() + 1;
	        	timeInitialization = timeInitialization + IND + "time_p = 0;" + NL;
	        	packtime = packtime + IND + "double dbuffer[" + nvars + "];" + NL
	        			+ IND + "dbuffer[0] = time_p;" + NL;
	        	puttime = puttime + IND + "double dbuffer[" + nvars + "];" + NL
	        			+ IND + "dbuffer[0] = time_p;" + NL;
	        	unpacktime = unpacktime + IND + "double dbuffer[" + nvars + "];" + NL
	        			+ IND + "stream.unpack(dbuffer, " + nvars + ");" + NL
	        			+ IND + "time_p = dbuffer[0];" + NL;
	        	gettime = gettime + IND + "double dbuffer[" + nvars + "];" + NL
	        			+ IND + "database->getDoubleArray(\"dbuffer\", dbuffer, " + nvars + ");" + NL
	        			+ IND + "time_p = dbuffer[0];" + NL;
	            for (int i = 0; i < timeInterp.getVarAtStep().size(); i++) {
	            	String variable = SAMRAIUtils.xmlTransform(timeInterp.getVarAtStep().get(i), null);
	            	variable = variable.substring(0, variable.indexOf("("));
	            	timeInitialization = timeInitialization + IND + "time" + variable + " = 0;" + NL;
	            	packtime = packtime + IND + "dbuffer[" + (i + 1) + "] = time" + variable + ";" + NL;
	            	puttime = puttime + IND + "dbuffer[" + (i + 1) + "] = time" + variable + ";" + NL;
	            	unpacktime = unpacktime + IND + "time" + variable + " = dbuffer[" + (i + 1) + "];" + NL;
	            	gettime = gettime + IND + "time" + variable + " = dbuffer[" + (i + 1) + "];" + NL;
	            }
	        	packtime = packtime + IND + "stream.pack(dbuffer, " + nvars + ");" + NL;
	        	puttime = puttime + IND + "database->putDoubleArray(\"dbuffer\", dbuffer, " + nvars + ");" + NL;
        	}
        }
        else {
        	timeInitialization = timeInitialization + IND + "time_p = 0;" + NL
        			 + IND + "time = 0;" + NL;
        	packtime = packtime + IND + "double dbuffer[2];" + NL
        			+ IND + "dbuffer[0] = time_p;" + NL
        			+ IND + "dbuffer[1] = time;" + NL;
        	puttime = puttime + IND + "double dbuffer[2];" + NL
        			+ IND + "dbuffer[0] = time_p;" + NL
        			+ IND + "dbuffer[1] = time;" + NL;
        	unpacktime = unpacktime + IND + "double dbuffer[2];" + NL
        			+ IND + "stream.unpack(dbuffer, 2);" + NL
        			+ IND + "time_p = dbuffer[0];" + NL
        			+ IND + "time = dbuffer[1];" + NL;
        	gettime = gettime + IND + "double dbuffer[2];" + NL
        			+ IND + "database->getDoubleArray(\"dbuffer\", dbuffer, 2);" + NL
        			+ IND + "time_p = dbuffer[0];" + NL
        			+ IND + "time = dbuffer[1];" + NL;
        	packtime = packtime + IND + "stream.pack(dbuffer, 2);" + NL;
        	puttime = puttime + IND + "database->putDoubleArray(\"dbuffer\", dbuffer, 2);" + NL;
        }

        particles = particles.replaceAll("#timeInitialization#", timeInitialization);
        particles = particles.replaceFirst("#packtime#", packtime);
        particles = particles.replaceFirst("#unpacktime#", unpacktime);
        particles = particles.replaceFirst("#puttime#", puttime);
        particles = particles.replaceFirst("#gettime#", gettime);
        
        String particleInstances = "";
        for (String speciesName : particleVariables.keySet()) {
        	particleInstances = particleInstances + "template class Particles<Particle_" + speciesName + ">;" + NL;
        }
        particles = particles.replaceFirst("#Particle_instances#", particleInstances);

        return particles;
    }
    
    /**
     * Create particles.h file.
     * @param ti                The time interpolation info
     * @return                  The code
     * @throws CGException      CG00X External error
     */
    private String createParticlesH(TimeInterpolationInfo ti, HashMap<String, ArrayList<String>> particleVariables) throws CGException {
        String particles = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/Particles.h"), 0);

        String timeFunctions = "";
        String timeVariables = "";
        String timeSize = "";
        if (ti.getTimeInterpolators().size() > 0) {
        	for (TimeInterpolator timeInterp: ti.getTimeInterpolators().values()) {
	        	timeSize = " + tbox::MessageStream::getSizeof<double>(" + (timeInterp.getVarAtStep().size() + 1) + ")";
	        	timeFunctions = timeFunctions + IND + "inline void setTime_p(double time)" + NL
	        			+ IND + "{" + NL
	        			+ IND + IND + "this->time_p = time;" + NL
	        			+ IND + "}" + NL;
	        	timeVariables = timeVariables + IND + "double time_p";
	            for (int i = 0; i < timeInterp.getVarAtStep().size(); i++) {
	            	String variable = SAMRAIUtils.xmlTransform(timeInterp.getVarAtStep().get(i), null);
	            	variable = variable.substring(0, variable.indexOf("("));
	            	timeFunctions = timeFunctions + IND + "inline void setTime" + variable + "(double time)" + NL
	            		 	+ IND + "{" + NL
	            			+ IND + IND + "this->time" + variable + " = time;" + NL
	            			+ IND + "}" + NL;
	            	timeVariables = timeVariables + ", time" + variable;
	            }
	            timeVariables = timeVariables + ";";
        	}
        }
        else {
        	timeSize = " + tbox::MessageStream::getSizeof<double>(2)";
        	timeFunctions = timeFunctions + IND + "inline void setTime_p(double time)" + NL
          			+ IND + "{" + NL
        			+ IND + IND + "this->time_p = time;" + NL
        			+ IND + "}" + NL
        			+ IND + "void setTime(double time);" + NL
        			+ IND + "{" + NL
        			+ IND + IND + "this->time = time;" + NL
        			+ IND + "}" + NL;
        	timeVariables = timeVariables + IND + "double time_p, time;" + NL;
        }
        particles = particles.replaceFirst("#timeFunctions#", timeFunctions);
        particles = particles.replaceFirst("#timeVariables#", timeVariables);
        particles = particles.replaceFirst("#timeSize#", timeSize);
        
        String particleSpecies = "";
        for (String speciesName : particleVariables.keySet()) {
        	particleSpecies = particleSpecies + "#include \"Particle_" + speciesName + ".h\"" + NL;
        }
        particles = particles.replaceFirst("#Particle_species#", particleSpecies);
        

        return particles;
    }
    
    
    /**
     * Create particle.h file.
     * @param pi                The problem info
     * @return                  The code
     * @throws CGException      CG00X External error
     */
    private String createParticleDataWriter(ProblemInfo pi) throws CGException {
        try {
            //Get the template
            String particleDataWriter = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/ParticleDataWriter.C"), 0);
            
            ArrayList<String> coords = pi.getContCoordinates();

            String positions = "";
            for (int i = 0;i < coords.size(); i++) {
                positions = positions + IND + IND + IND + IND + "G_coords[" + i + "][i] = particle->position" 
                    + coords.get(i) + ";" + NL;
            }
            
            particleDataWriter = particleDataWriter.replaceAll("#getPosition#", positions);
            particleDataWriter = particleDataWriter.replaceAll("#DIM#", String.valueOf(pi.getDimensions()));  
   
            if (pi.getDimensions() == THREE) {
                particleDataWriter = particleDataWriter.replaceAll("#3DI#", "");
                particleDataWriter = particleDataWriter.replaceAll("#3DF#", "");
                while (particleDataWriter.indexOf("#2DI#") > 0) {
                    particleDataWriter = particleDataWriter.substring(0, particleDataWriter.indexOf("#2DI#")) 
                            + particleDataWriter.substring(particleDataWriter.indexOf("#2DF#") + "#2DF#".length());
                }
            }
            else {
                particleDataWriter = particleDataWriter.replaceAll("#2DI#", "");
                particleDataWriter = particleDataWriter.replaceAll("#2DF#", "");
                while (particleDataWriter.indexOf("#3DI#") > 0) {
                    particleDataWriter = particleDataWriter.substring(0, particleDataWriter.indexOf("#3DI#")) 
                            + particleDataWriter.substring(particleDataWriter.indexOf("#3DF#") + "#3DF#".length());
                }
            }
            String suffix = "";
            String meshNamesDec = "";
            String meshNamesInit = "";
            String meshNamesAssig = "";
            String meshNamesDel1 = "";
            String meshNamesDel2 = "";
            String averageMesh = "";
            String particleIteratorWrite = "";
            String packRegularAndDerivedData = "";
            for (String speciesName : pi.getParticleSpecies()) {
            	suffix = suffix + IND + "if (std::is_same<T, Particle_" + speciesName + ">::value) {" + NL
            			+ IND + IND + "suffix = \"_" + speciesName + "\";" + NL
            			+ IND + "}" + NL;
            	meshNamesDec = meshNamesDec + IND + IND + IND + "char **meshnames_" + speciesName + "  = new char*[patchCounter];" + NL;
            	meshNamesInit = meshNamesInit + IND + IND + IND + IND + IND + "meshnames_" + speciesName + "[i] = new char[128];" + NL;
            	meshNamesAssig = meshNamesAssig + IND + IND + IND + IND + IND + "sprintf(meshnames_" + speciesName + "[i], \"%s%s%s/averageMesh_" + speciesName + "\", filename, levelName, patchName);" + NL;
            	meshNamesDel1 = meshNamesDel1 + IND + IND + IND + "delete [] meshnames_" + speciesName + "[i];" + NL;
            	meshNamesDel2 = meshNamesDel2 + IND + IND + "delete [] meshnames_" + speciesName + ";" + NL;
            	averageMesh = averageMesh + IND + IND + "sprintf(mesh, \"averageMesh_" + speciesName + "\");" + NL
            			+ IND + IND + "DBPutMultimesh(sumdb, mesh, patchCounter, meshnames_" + speciesName + ", meshtypes,optlist);" + NL;
            	particleIteratorWrite = particleIteratorWrite + IND + "for (std::list<ParticleItem>::iterator ipi(d_plot_items_" + speciesName + ".begin());ipi != d_plot_items_" + speciesName + ".end(); ipi++) {" + NL
            			+ IND + IND + "std::string vname = ipi->d_visit_var_name;" + NL + NL
            			+ IND + IND + "char **meshnames  = new char*[patchCounter];" + NL
			            + IND + IND + "char **varnames  = new char*[patchCounter];" + NL
			            + IND + IND + "char **segnames  = new char*[patchCounter];" + NL
			            + IND + IND + "char **avnames  = new char*[patchCounter];" + NL
			            + IND + IND + "int *meshtypes  = new int[patchCounter];" + NL
			            + IND + IND + "int *vartypes  = new int[patchCounter];" + NL
			            + IND + IND + "int *vartypes2  = new int[patchCounter];" + NL + NL
			            + IND + IND + "int i = 0;" + NL
			            + IND + IND + "for (int ln = coarsest_plot_level; ln <= finest_plot_level; ln++) {" + NL
			            + IND + IND + IND + "std::shared_ptr< hier::PatchLevel > patch_level(hierarchy->getPatchLevel(ln));" + NL
			            + IND + IND + IND + "char levelName[128];" + NL
			            + IND + IND + IND + "sprintf(levelName, \"/level_%05d\", ln);" + NL
			            + IND + IND + IND + "for (int pn = 0; pn < patch_level->getGlobalNumberOfPatches(); pn++) {" + NL
			            + IND + IND + IND + IND + "int proc_num = hierarchy->getPatchLevel(ln)->getProcessorMapping().getProcessorMapping()[pn];" + NL
			            + IND + IND + IND + IND + "char patchName[128];" + NL
			            + IND + IND + IND + IND + "char filename[128];" + NL
			            + IND + IND + IND + IND + "avnames[i] = new char[128];" + NL
			            + IND + IND + IND + IND + "sprintf(patchName, \"/patch_%05d\",pn);" + NL
			            + IND + IND + IND + IND + "sprintf(filename, \"processor_cluster.%05d.silo:\", d_processor_in_file_cluster_number[proc_num]);" + NL
			            + IND + IND + IND + IND + "sprintf(avnames[i], \"%s%s%s/%sAverage\", filename, levelName, patchName, vname.c_str());" + NL
			            + IND + IND + IND + IND + "meshnames[i] = new char[128];" + NL
			            + IND + IND + IND + IND + "varnames[i] = new char[128];" + NL
			            + IND + IND + IND + IND + "segnames[i] = new char[128];" + NL
			            + IND + IND + IND + IND + "sprintf(meshnames[i], \"%s%s%s/%sPointMesh\", filename, levelName, patchName, vname.c_str());" + NL
			            + IND + IND + IND + IND + "sprintf(varnames[i], \"%s%s%s/%s\", filename, levelName, patchName, vname.c_str());" + NL
			            + IND + IND + IND + IND + "sprintf(segnames[i], \"%s%s%s/%sRegion\", filename, levelName, patchName, vname.c_str());" + NL
			            + IND + IND + IND + IND + "meshtypes[i] = DB_POINTMESH;" + NL
			            + IND + IND + IND + IND + "vartypes[i]  = DB_POINTVAR;" + NL
			            + IND + IND + IND + IND + "vartypes2[i]  = DB_QUADVAR;" + NL
			            + IND + IND + IND + IND + "i++;" + NL
			            + IND + IND + IND + "}" + NL
			            + IND + IND + "}" + NL + NL
			            + IND + IND + "//Meshes" + NL
			            + IND + IND + "DBoptlist *optlist = DBMakeOptlist(2);" + NL
			            + IND + IND + "DBAddOption(optlist, DBOPT_CYCLE, &d_time_step_number);" + NL
			            + IND + IND + "DBAddOption(optlist, DBOPT_DTIME, &simulation_time);" + NL
			            + IND + IND + "char mesh[128];" + NL
			            + IND + IND + "sprintf(mesh, \"%sPointMesh\", vname.c_str());" + NL
			            + IND + IND + "DBPutMultimesh(sumdb, mesh, patchCounter, meshnames, meshtypes,optlist);" + NL
			            + IND + IND + "DBFreeOptlist(optlist);" + NL + NL
			            + IND + IND + "//Problem extents" + NL
			            + IND + IND + "int ndims = 1;" + NL
			            + IND + IND + "double extents[] = {grid_geometry->getXLower()[0], grid_geometry->getXUpper()[0], grid_geometry->getXLower()[1], grid_geometry->getXUpper()[1], grid_geometry->getXLower()[2], grid_geometry->getXUpper()[2]};" + NL
			            + IND + IND + "int dims[] = {6};" + NL
			            + IND + IND + "char name[128];" + NL
			            + IND + IND + "sprintf(name, \"problem_extents\");" + NL
			            + IND + IND + "DBWrite(sumdb, name, &extents, dims, ndims, DB_DOUBLE);" + NL + NL
			            + IND + IND + "//Variables, Regions and averages" + NL
			            + IND + IND + "optlist = DBMakeOptlist(3);" + NL
			            + IND + IND + "DBAddOption(optlist, DBOPT_MMESH_NAME, mesh);" + NL
			            + IND + IND + "DBAddOption(optlist, DBOPT_CYCLE, &d_time_step_number);" + NL
			            + IND + IND + "DBAddOption(optlist, DBOPT_DTIME, &simulation_time);" + NL
			            + IND + IND + "DBPutMultivar(sumdb, vname.c_str(), patchCounter, varnames, vartypes,optlist);" + NL
			            + IND + IND + "char region[128];" + NL
			            + IND + IND + "sprintf(region, \"%sRegion\", vname.c_str());" + NL
			            + IND + IND + "DBPutMultivar(sumdb, region, patchCounter, segnames, vartypes,optlist);" + NL
			            + IND + IND + "DBFreeOptlist(optlist);" + NL
			            + IND + IND + "if (d_plotAverages) {" + NL
			            + IND + IND + IND + "sprintf(mesh, \"averageMesh_" + speciesName + "\");" + NL
			            + IND + IND + IND + "optlist = DBMakeOptlist(3);" + NL
			            + IND + IND + IND + "DBAddOption(optlist, DBOPT_MMESH_NAME, mesh);" + NL
			            + IND + IND + IND + "DBAddOption(optlist, DBOPT_CYCLE, &d_time_step_number);" + NL
			            + IND + IND + IND + "DBAddOption(optlist, DBOPT_DTIME, &simulation_time);" + NL
			            + IND + IND + IND + "char average[128];" + NL
			            + IND + IND + IND + "sprintf(average, \"%sAverage\", vname.c_str());" + NL
			            + IND + IND + IND + "DBPutMultivar(sumdb, average, patchCounter, avnames, vartypes2,optlist);" + NL
			            + IND + IND + IND + "DBFreeOptlist(optlist);" + NL
			            + IND + IND + "}" + NL + NL
			            + IND + IND + "for (int i = 0; i < patchCounter ; i++) {" + NL
			            + IND + IND + IND + "delete [] avnames[i];" + NL
			            + IND + IND + IND + "delete [] meshnames[i];" + NL
			            + IND + IND + IND + "delete [] segnames[i];" + NL
			            + IND + IND + IND + "delete [] varnames[i];" + NL
			            + IND + IND + "}" + NL
			            + IND + IND + "delete [] meshnames;" + NL
			            + IND + IND + "delete [] segnames;" + NL
			            + IND + IND + "delete [] avnames;" + NL
			            + IND + IND + "delete [] varnames;" + NL
			            + IND + IND + "delete [] meshtypes;" + NL
			            + IND + IND + "delete [] vartypes;" + NL
			            + IND + IND + "delete [] vartypes2;" + NL
			            + IND + "}" + NL;
            	packRegularAndDerivedData = packRegularAndDerivedData + IND + "for (std::list<ParticleItem>::iterator ipi(d_plot_items_" + speciesName + ".begin());ipi != d_plot_items_" + speciesName + ".end(); ipi++) {" + NL
            			+ IND + IND + "/*" + NL
            			+ IND + IND + " * If its derived data, pack via the derived writer." + NL
            			+ IND + IND + " * Otherwise, pack with local private method." + NL
            			+ IND + IND + " */" + NL
            			+ IND + IND + "bool data_exists_on_patch = false;" + NL
            			+ IND + IND + "int patch_data_id = -1;" + NL
            			+ IND + IND + "/*" + NL
            			+ IND + IND + " * Check if patch data id has been reset on the level.  If" + NL
            			+ IND + IND + " * not, just use the original registered data id." + NL
            			+ IND + IND + " */" + NL
            			+ IND + IND + "patch_data_id = ipi->d_patch_data_index;" + NL
            			+ IND + IND + "if (static_cast<int>(ipi->d_level_patch_data_index.size()) > level_number) {" + NL
            			+ IND + IND + IND + "patch_data_id = ipi->d_level_patch_data_index[level_number];" + NL
            			+ IND + IND + "}" + NL + NL
            			+ IND + IND + "data_exists_on_patch = patch.checkAllocated(patch_data_id);" + NL
            			+ IND + IND + "if (data_exists_on_patch) {" + NL
            			+ IND + IND + IND + "/*" + NL
            			+ IND + IND + IND + " * Write to disk" + NL
            			+ IND + IND + IND + " */" + NL
            			+ IND + IND + IND + "std::string vname = ipi->d_visit_var_name;" + NL
            			+ IND + IND + IND + "writeDouble<Particle_" + speciesName + ">(vname, grid_geometry, hierarchy,patch_data_id, patch ,simulation_time);" + NL
            			+ IND + IND + "}" + NL
            			+ IND + "} // iterate over vars" + NL;
            }
            particleDataWriter = particleDataWriter.replaceFirst("#Suffix#", suffix);
            particleDataWriter = particleDataWriter.replaceFirst("#meshNamesDec#", meshNamesDec);
            particleDataWriter = particleDataWriter.replaceFirst("#meshNamesInit#", meshNamesInit);
            particleDataWriter = particleDataWriter.replaceFirst("#meshNamesAssig#", meshNamesAssig);
            particleDataWriter = particleDataWriter.replaceFirst("#meshNamesDel1#", meshNamesDel1);
            particleDataWriter = particleDataWriter.replaceFirst("#meshNamesDel2#", meshNamesDel2);
            particleDataWriter = particleDataWriter.replaceFirst("#averageMesh#", averageMesh);
            particleDataWriter = particleDataWriter.replaceFirst("#ParticleIteratorWrite#", particleIteratorWrite);
            particleDataWriter = particleDataWriter.replaceFirst("#packRegularAndDerivedData#", packRegularAndDerivedData);
            
            return particleDataWriter;
        } 
        catch (Exception e) {
            throw new CGException(CGException.CG00X, e.getMessage());
        }
    }
    
    /**
     * Create particle.h file.
     * @param pi                The problem info
     * @return                  The code
     * @throws CGException      CG00X External error
     */
    private String createParticleDataWriterH(ProblemInfo pi) throws CGException {
        try {
            //Get the template
            String particleDataWriterH = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/ParticleDataWriter.h"), 0);
            
            String plotItems = "";
            String registerPlotVariable1 = "";
            String registerPlotVariable2 = "";
            for (String speciesName : pi.getParticleSpecies()) {
            	plotItems = plotItems + IND + "std::list<ParticleItem> d_plot_items_" + speciesName + ";" + NL;
            	registerPlotVariable1 = registerPlotVariable1 + IND + "if (std::is_same<T, Particle_" + speciesName + ">::value) {" + NL
            			+ IND + IND + "for (std::list<ParticleItem>::iterator ipi(d_plot_items_" + speciesName + ".begin()); ipi != d_plot_items_" + speciesName + ".end(); ipi++) {" + NL
            			+ IND + IND + IND + "if (ipi->d_var_name == variable_name) {" + NL
            			+ IND + IND + IND + IND + "TBOX_ERROR(\"ParticleDataWriter::registerPlotVariable()\"" + NL
            			+ IND + IND + IND + IND + "<< \"    Attempting to register variable with name \"" + NL
            			+ IND + IND + IND + IND + "<< variable_name << \"    more than once.\" << std::endl);" + NL
            			+ IND + IND + IND + "}" + NL
            			+ IND + IND + "}" + NL
            			+ IND + "}" + NL;
            	registerPlotVariable2 = registerPlotVariable2 + IND + "if (std::is_same<T, Particle_" + speciesName + ">::value) {" + NL
            			+ IND + IND + "d_plot_items_" + speciesName + ".push_back(plotitem);" + NL
            			+ IND + "}" + NL;
            }
            
            particleDataWriterH = particleDataWriterH.replaceFirst("#plotItems#", plotItems);
            particleDataWriterH = particleDataWriterH.replaceFirst("#registerPlotVariable1#", registerPlotVariable1);
            particleDataWriterH = particleDataWriterH.replaceFirst("#registerPlotVariable2#", registerPlotVariable2);
            
            return particleDataWriterH;
        } 
        catch (Exception e) {
            throw new CGException(CGException.CG00X, e.getMessage());
        }
    }
    
    
    /**
     * Creates the function.cpp from the source.
     * @param functions         The functions code
     * @param pi                The problem info
     * @return                  The code
     * @throws CGException      CG00X External error
     */
    private String createFunctions(String functions, ProblemInfo pi) throws CGException {
        //Get the template
        String functionsFile = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/Functions.cpp"), 0);
        
        String includes = "";
        if (pi.isHasParticleFields() || pi.isHasABMMeshlessFields()) {
            includes = "#include \"SAMRAI/pdat/CellData.h\"" + NL
                    + "#include \"Problem.h\"" + NL;
        }
        functionsFile = functionsFile.replaceFirst("#includes#", includes);
        
        String readFromFile = "";
        ArrayList<Integer> created = new ArrayList<Integer>();
        FileReadInformation fri = pi.getFileReadInfo();
        for (String fileName : fri.getFileNameInfo().keySet()) {
        	int nCoords = fri.getFileNameInfo().get(fileName);
        	if (!created.contains(nCoords)) {
        		created.add(nCoords);
	        	String params = "";
	        	String readCoords = "";
	        	String coordSizes = "";
	        	String coordLoopsInit = "";
	        	String coordLoopsEnd = "";
	        	String currentInd = IND + IND;
	            for (int k = 0; k < nCoords; k++) {
	            	params = params + "double **coord" + k + ", int &coord" + k + "Size, double &coord" + k + "Dx, ";
	            	readCoords = readCoords + IND + "dataset = H5Dopen(file, \"coord" + k + "\", H5P_DEFAULT);" + NL
	            			+ IND + "dataspace = H5Dget_space(dataset);" + NL
	            			+ IND + "rank = H5Sget_simple_extent_ndims(dataspace);" + NL
	            			+ IND + "dims = new hsize_t[rank];" + NL
	            			+ IND + "H5Sget_simple_extent_dims(dataspace, dims, dims);" + NL
	            			+ IND + "*coord" + k + " = new double[dims[0]];" + NL
	            			+ IND + "coord" + k + "Size = dims[0];" + NL
	            			+ IND + "coordName = \"coord" + k + "\";" + NL
	            			+ IND + "READ_EOS_HDF5(coordName, *coord" + k + ", H5T_NATIVE_DOUBLE, H5S_ALL);" + NL
	            			+ IND + "coord" + k + "Dx = ((*coord" + k + ")[coord" + k + "Size - 1] - (*coord" + k + ")[0])/(coord" + k + "Size-1);" + NL;
	            	coordSizes = coordSizes + "coord" + k + "Size * ";
	            }
	            coordSizes = coordSizes.substring(0, coordSizes.lastIndexOf("*"));
	            String oldInd = "iv";
	            String newInd = "";
	            for (int k = nCoords - 1; k >= 0; k--) {
	            	coordLoopsInit = coordLoopsInit + currentInd + "for(int i" + k + " = 0; i" + k + "<coord" + k + "Size; i" + k + "++) {" + NL;
	            	coordLoopsEnd = currentInd + "}" + NL + coordLoopsEnd;
	            	currentInd = currentInd + IND;
	            	oldInd = "(i" + k + " + coord" + k + "Size * " + oldInd + ")";
	            	if (k == nCoords - 1) {
	            		newInd = newInd + "i" + k;
	            	}
	            	else {
	            		newInd ="(i" + k + " + coord" + k + "Size * " + newInd + ")";
	            	}
	            }
	            newInd = "iv + nVars * " + newInd;	            
	    	  
	            readFromFile = readFromFile + "/*" + NL
	            		+ " * Reads a table in hdf5 file and stores all relevant data." + NL
	            		+ " * Only inputFile is an input parameter. The others are filled in this routine." + NL
	            		+ " *    inputFile:         Name of the table" + NL
	            		+ " *    coord0, ...:       variable storing values of table dimensions" + NL
	            		+ " *    coord0Size, ...:   size of table dimensions" + NL
	            		+ " *    coord0Dx, ...:     spacing of table dimensions" + NL
	            		+ " *    data:              variable data from the table" + NL
	            		+ " *    nVars:             number of variables in the table" + NL
	            		+ " */" + NL
	            		+ "void readTable(std::string inputFile, " + params + "double** data, int& nVars) {" + NL
	            		+ IND + "std::cout<<\"*******************************\"<<std::endl;" + NL
	            		+ IND + "std::cout<<\"Reading table file: \"<<inputFile<<std::endl;" + NL
	            		+ IND + "std::cout<<\"*******************************\"<<std::endl;" + NL + NL
	            		+ IND + "hid_t file;" + NL
	            		+ IND + "file = H5Fopen(inputFile.c_str(), H5F_ACC_RDONLY, H5P_DEFAULT);" + NL + NL	
	            		+ IND + "// Use these two defines to easily read in a lot of variables in the same way" + NL
	            		+ IND + "// The first reads in one variable of a given type completely" + NL
	            		+ IND + "#define READ_EOS_HDF5(NAME,VAR,TYPE,MEM)                                      \\\\" + NL
	            		+ IND + "do {                                                                        \\\\" + NL
	            		+ IND + IND + "hid_t dataset;                                                            \\\\" + NL
	            		+ IND + IND + "dataset = H5Dopen(file, NAME.c_str(), H5P_DEFAULT);                                \\\\" + NL
	            		+ IND + IND + "H5Dread(dataset, TYPE, MEM, H5S_ALL, H5P_DEFAULT, VAR);       \\\\" + NL
	            		+ IND + IND + "H5Dclose(dataset);                                            \\\\" + NL
	            		+ IND + "} while (0)" + NL
	            		+ IND + "// The second reads a given variable into a hyperslab of the alltables_temp array" + NL
	            		+ IND + "#define READ_EOSTABLE_HDF5(NAME,OFF)                                     \\\\" + NL
	            		+ IND + "do {                                                                   \\\\" + NL
	            		+ IND + IND + "hsize_t offset[2]     = {static_cast<hsize_t>(OFF),0};                                     \\\\" + NL
	            		+ IND + IND + "H5Sselect_hyperslab(mem3, H5S_SELECT_SET, offset, NULL, var3, NULL); \\\\" + NL
	            		+ IND + IND + "READ_EOS_HDF5(NAME,data_temp,H5T_NATIVE_DOUBLE,mem3);           \\\\" + NL
	            		+ IND + "} while (0)" + NL + NL
	            		+ IND + "// Read size of tables" + NL
	            		+ IND + "hid_t dataset;" + NL
	            		+ IND + "dataset = H5Dopen(file, \"nvars\", H5P_DEFAULT);" + NL
	            		+ IND + "H5Dread(dataset, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, H5P_DEFAULT, &nVars);" + NL
	            		+ IND + "H5Dclose(dataset);" + NL + NL
	            		+ IND + "int rank;" + NL
	            		+ IND + "hid_t dataspace;" + NL
	            		+ IND + "std::string coordName;" + NL
	            		+ IND + "hsize_t* dims;" + NL
	            		+ IND + "// Read table coordinates" + NL
	            		+ readCoords + NL	
	            		+ IND + "// Allocate memory for table" + NL
	            		+ IND + "double* data_temp;" + NL
	            		+ IND + "if (!(data_temp = (double*)malloc(" + coordSizes + " * nVars * sizeof(double)))) {" + NL
	            		+ IND + IND + "TBOX_ERROR(\"Cannot allocate memory for input table \"<<inputFile);" + NL
	            		+ IND + "}" + NL + NL
	            		+ IND + "// Prepare HDF5 to read hyperslabs into data" + NL
	            		+ IND + "hsize_t table_dims[2] = {static_cast<hsize_t>(nVars), static_cast<hsize_t>((hsize_t)" + coordSizes + ")};" + NL
	            		+ IND + "hsize_t var3[2]       = { 1, (hsize_t)" + coordSizes + "};" + NL
	            		+ IND + "hid_t mem3 =  H5Screate_simple(2, table_dims, NULL);" + NL
	            		+ IND + "// Read data" + NL
	            		+ IND + "for (int i = 0; i < nVars; i++) {" + NL
	            		+ IND + IND + "std::string varName = \"var\" + std::to_string(i);" + NL
	            		+ IND + IND + "READ_EOSTABLE_HDF5(varName,  i);" + NL
	            		+ IND + "}" + NL
	            		+ IND + "H5Fclose(file);" + NL + NL	
	            		+ IND + "// change ordering of alltables array so that" + NL
	            		+ IND + "// the table kind is the fastest changing index" + NL
	            		+ IND + "if (!(*data = (double*)malloc(" + coordSizes + " * nVars * sizeof(double)))) {" + NL
	            		+ IND + IND + "TBOX_ERROR(\"Cannot allocate memory for input table \"<<inputFile);" + NL
	            		+ IND + "}" + NL
	            		+ IND + "for(int iv = 0;iv<nVars;iv++) {" + NL
	            		+ coordLoopsInit
	            		+ currentInd + "int indold = " + oldInd + ";" + NL
	            		+ currentInd + "int indnew = " + newInd + ";" + NL
	            		+ currentInd + "(*data)[indnew] = data_temp[indold];" + NL
	            		+ coordLoopsEnd
	            		+ IND + "}" + NL
	            		+ IND + "// free memory of temporary array" + NL
	            		+ IND + "free(data_temp);" + NL
	            		+ "}" + NL;
        	}
        }
        
        functionsFile = functionsFile.replaceFirst("#functions#", functions.substring(0, functions.indexOf("#functions#")) + readFromFile);
        
        return functionsFile;
    }
    
    /**
     * Creates the function.h from the source.
     * @param functions         The functions code
     * @param pi                The problem info
     * @return                  The code
     * @throws CGException      CG00X External error
     */
    private String createFunctionsH(String functions, ProblemInfo pi) throws CGException {   
        String functionsFile = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/Functions.h"), 0);
        
        String includes = "";
        if (pi.isHasParticleFields() || pi.isHasABMMeshlessFields()) {
            includes = "#include \"SAMRAI/pdat/CellData.h\"" + NL
                    + "#include \"SAMRAI/pdat/CellGeometry.h\"" + NL
                    + "#include \"Particles.h\"" + NL
                    + "#include \"SAMRAI/hier/Index.h\"" + NL
                    + "#include \"SAMRAI/pdat/IndexData.h\"" + NL;
        }
        functionsFile = functionsFile.replaceFirst("#includes#", includes);
                
        String readFunctions = "";
        String initFileReadFunctions = "";
       	String findCoordFromFile = "";
        ArrayList<Integer> createdInitFileRead = new ArrayList<Integer>();
        
        FileReadInformation fri = pi.getFileReadInfo();
        //Create readTable header
        for (String fileName : fri.getFileNameInfo().keySet()) {
        	int nCoords = fri.getFileNameInfo().get(fileName);
         	if (!createdInitFileRead.contains(nCoords)) {
	        	String params = "";
	            for (int k = 0; k < nCoords; k++) {
	              	params = params + "double **coord" + k + ", int &coord" + k + "Size, double &coord" + k + "Dx, ";
	            }
	            initFileReadFunctions = initFileReadFunctions + "void readTable(std::string inputFile, " + params + "double** data, int& nVars);" + NL;
         	}
        }
        
        //Create quintic read from table
        for (Integer nDimensions : fri.getNumberQuinticReads().keySet()) {
        	Set<Integer> nOutputVars = fri.getNumberQuinticReads().get(nDimensions);
	    	for (Integer i : nOutputVars) {
	        	String coordPars = "";
	        	String getIndicesAndCoefs = "";
	        	String coordVarPars = "";
	        	String coefs = "";
	            for (int k = 0; k < nDimensions; k++) {
	            	coefs = coefs + "coefs" + k + "[i] * ";
	            	coordPars = coordPars + "double coord" + k + ", ";
	            	coordVarPars = coordVarPars + "double *coord" + k + "Data, double coord" + k + "Dx, int coord" + k + "Size, ";
	            	getIndicesAndCoefs = getIndicesAndCoefs + IND + "int i" + k + "  = int((coord" + k + " - coord" + k + "Data[0])/coord" + k + "Dx);" + NL + NL
    	            		+ IND + "if (i" + k + " >= coord" + k + "Size || i" + k + " < 0) {" + NL
    	            		+ IND + IND + "TBOX_ERROR(\"Mapping File: the input file does not have a value for coordinate \"<<coord" + k + ");" + NL
    	            		+ IND + "}" + NL + NL
    	            		+ IND + "int i" + k + "b = std::min(std::max(i" + k + "-2,0),coord" + k + "Size-6);" + NL
    	            		+ IND + "double coefs" + k + "[6];" + NL
    	            		+ IND + "calculateCoefficientsMapFile(coefs" + k + ", coord" + k + ", &coord" + k + "Data[i" + k + "b]);" + NL;
	            }
	            String index = "";
	            for (int k = nDimensions - 1; k >= 0; k--) {
	            	if (k == nDimensions - 1) {
	            		index = index + "(i" + k + "b + i)";
	            	}
	            	else {
	            		index ="(i" + k + "b + i + coord" + k + "Size * " + index + ")";
	            	}
	            }
	            index = "nVars * " + index;	 
	            String dataInit = "";
	            String dataValue = "";
	        	String dataVarPars = "";
	        	String dataIndexPars = "";
	            for (int k = 0; k < i; k++) {
	            	dataInit = dataInit + IND + "data" + k + " = 0;" + NL;
	            	dataValue = dataValue + IND + IND + "data" + k + " = data" + k + " + " + coefs + " varData[data" + k + "Index + " + index + "];" + NL;
	            	dataVarPars = dataVarPars + "double &data" + k + ", ";
	            	dataIndexPars = dataIndexPars + "int data" + k + "Index, ";
	            }
	            
	            readFunctions = readFunctions
	            		+ "/*" + NL
	            		+ " * Reads fields from a table interpolating in given coordinates." + NL
	            		+ " * Uses quintic lagrangian interpolation." + NL
	            		+ " *    nCoords:            number of coordinates of table" + NL
	            		+ " *    nOutFields:         number of fields to read" + NL
	            		+ " *    coord0, ...:        coordinate values to search in the table" + NL
	            		+ " *    data0, ...:         output variables to store interpolated fields" + NL
	            		+ " *    data0Index, ...:    index of fields inside the table" + NL
	            		+ " *    coord0Data, ...:    variables storing all values of table coordinates" + NL
	            		+ " *    coord0Dx, ...:      spacing of table coordinates" + NL
	            		+ " *    coord0Size, ...:    size of table coordinates" + NL
	            		+ " *    varData, ...:       table data" + NL
	            		+ " *    nVars, ...:         number of variables in the table" + NL
	            		+ " */" + NL
	            		+ "inline void readFromFileQuintic(int nCoords, int nOutFields, " + coordPars + dataVarPars + dataIndexPars + coordVarPars + "double *varData, int nVars) {" + NL
	            		+ getIndicesAndCoefs
	            		+ dataInit
	            		+ IND + "for (int i = 0; i < 6; i++) {" + NL
	            		+ dataValue
	            		+ IND + "}" + NL
	            		+ "}" + NL;
        	}
    	}
        
        //Create linear reads
    	for (Integer nVarsOutput : fri.getLinearRead1D()) {
        	String dataVarPars = "";
        	String dataIndexPars = "";
        	String interpolations = "";
            for (int k = 0; k < nVarsOutput; k++) {
            	dataVarPars = dataVarPars + "double &data" + k + ", ";
            	dataIndexPars = dataIndexPars + "int data" + k + "Index, ";
            	interpolations = interpolations + IND + "idx[0] = data" + k + "Index + nVars * (ix);" + NL
            			 + IND + "idx[1] = data" + k + "Index + nVars * (ix - 1);" + NL
            			 + IND  + "// set up aux vars for interpolation" + NL
            			 + IND + "fh[0] = varData[idx[0]];" + NL
            			 + IND + "fh[1] = varData[idx[1]];" + NL
            			 + IND + "// set up coeffs of interpolation polynomical and evaluate function values" + NL
            			 + IND + "a1 = fh[0];" + NL
            			 + IND + "a2 = dxi   * ( fh[1] - fh[0] );" + NL
            			 + IND + "data" + k + " = a1 +  a2 * delx;" + NL;
            }            
            readFunctions = readFunctions + "/*" + NL
                    + " * Reads fields from a 1D table interpolating in the give coordinate." + NL
                    + " * Uses Trilinear interpolation." + NL
                    + " * Table must be equidistant." + NL
                    + " *    nCoords:            number of coordinates of table" + NL
                    + " *    nOutFields:         number of fields to read" + NL
                    + " *    coord0:             coordinate value to search in the table" + NL
                    + " *    data0, ...:         output variables to store interpolated fields" + NL
                    + " *    data0Index, ...:    index of fields inside the table" + NL
                    + " *    coord0Data:         variable storing all values of table coordinate" + NL
                    + " *    coord0Dx:           spacing of table coordinate" + NL
                    + " *    coord0Size:         size of table coordinate" + NL
                    + " *    varData, ...:       table data" + NL
                    + " *    nVars, ...:         number of variables in the table" + NL
                    + " */" + NL
            		+ "inline void readFromFileLinear1D(int nCoords, int nOutFields, double coord0, "  + dataVarPars + dataIndexPars + "double *coord0Data, double coord0Dx, int coord0Size, double *varData, int nVars) {" + NL
            		+ IND + "// determine spacing parameters of table" + NL
            		+ IND + "double dx = coord0Dx;" + NL
            		+ IND + "double dxi = 1. / dx;" + NL
            		+ IND + "double fh[2];" + NL
            		+ IND + "double a1, a2;" + NL
            		+ IND + "// determine location in table" + NL
            		+ IND + "int idx[2];" + NL
            		+ IND + "int ix  = 1 + int((coord0 - coord0Data[0] - 1.e-10)/coord0Dx);" + NL
            		+ IND + "ix = MAX( 1, MIN( ix, coord0Size - 1 ) );" + NL
            		+ IND  + "// set up aux vars for interpolation" + NL
            		+ IND + "double delx = coord0Data[ix] - coord0;" + NL
            		+ interpolations
            		+ "}" + NL;
    	}
    	for (Integer nVarsOutput : fri.getLinearRead2D()) {
        	String dataVarPars = "";
        	String dataIndexPars = "";
        	String interpolations = "";
            for (int k = 0; k < nVarsOutput; k++) {
            	dataVarPars = dataVarPars + "double &data" + k + ", ";
            	dataIndexPars = dataIndexPars + "int data" + k + "Index, ";
            	interpolations = interpolations + IND + "idx[0] = data" + k + "Index + nVars * (ix + coord0Size * (iy));" + NL
            			+ IND + "idx[1] = data" + k + "Index + nVars * (ix - 1 + coord0Size * (iy));" + NL
            			+ IND + "idx[2] = data" + k + "Index + nVars * (ix + coord0Size * (iy - 1));" + NL
            			+ IND + "idx[3] = data" + k + "Index + nVars * (ix - 1 + coord0Size * (iy - 1));" + NL
            			+ IND  + "// set up aux vars for interpolation" + NL
            			+ IND + "fh[0] = varData[idx[0]];" + NL
            			+ IND + "fh[1] = varData[idx[1]];" + NL
            			+ IND + "fh[2] = varData[idx[2]];" + NL
            			+ IND + "fh[3] = varData[idx[3]];" + NL
            			+ IND + "// set up coeffs of interpolation polynomical and evaluate function values" + NL
            			+ IND + "a1 = fh[0];" + NL
            			+ IND + "a2 = dxi   * ( fh[1] - fh[0] );" + NL
            			+ IND + "a3 = dyi   * ( fh[2] - fh[0] );" + NL
            			+ IND + "a4 = dxyi  * ( fh[3] - fh[1] - fh[2] + fh[0] );" + NL
            			+ IND + "data" + k + " = a1 +  a2 * delx +  a3 * dely + a5 * delx * dely;" + NL;
            }            
            readFunctions = readFunctions + "/*" + NL
                    + " * Reads fields from a 2D table interpolating in give coordinates." + NL
                    + " * Uses Trilinear interpolation." + NL
                    + " * Table must be equidistant." + NL
                    + " *    nCoords:            number of coordinates of table" + NL
                    + " *    nOutFields:         number of fields to read" + NL
                    + " *    coord0, ...:        coordinate values to search in the table" + NL
                    + " *    data0, ...:         output variables to store interpolated fields" + NL
                    + " *    data0Index, ...:    index of fields inside the table" + NL
                    + " *    coord0Data, ...:    variables storing all values of table coordinates" + NL
                    + " *    coord0Dx, ...:      spacing of table coordinates" + NL
                    + " *    coord0Size, ...:    size of table coordinates" + NL
                    + " *    varData, ...:       table data" + NL
                    + " *    nVars, ...:         number of variables in the table" + NL
                    + " */" + NL
                    + "inline void readFromFileLinear2D(int nCoords, int nOutFields, double coord0, double coord1, "  + dataVarPars + dataIndexPars + "double *coord0Data, double coord0Dx, int coord0Size, double *coord1Data, double coord1Dx, int coord1Size, double *varData, int nVars) {" + NL
                    + IND + "// determine spacing parameters of table" + NL
            		+ IND + "double dx = coord0Dx;" + NL
            		+ IND + "double dy = coord1Dx;" + NL
            		+ IND + "double dxi = 1. / dx;" + NL
            		+ IND + "double dyi = 1. / dy;" + NL
            		+ IND + "double dxyi  = dxi * dyi;" + NL
            		+ IND + "double fh[4];" + NL
            		+ IND + "double a1, a2, a3, a4;" + NL
            		+ IND + "// determine location in table" + NL
            		+ IND + "int idx[4];" + NL
            		+ IND + "int ix  = 1 + int((coord0 - coord0Data[0] - 1.e-10)/coord0Dx);" + NL
            		+ IND + "int iy  = 1 + int((coord1 - coord1Data[0] - 1.e-10)/coord1Dx);" + NL
            		+ IND + "ix = MAX( 1, MIN( ix, coord0Size - 1 ) );" + NL
            		+ IND + "iy = MAX( 1, MIN( iy, coord1Size - 1 ) );" + NL
            		+ IND  + "// set up aux vars for interpolation" + NL
            		+ IND + "double delx = coord0Data[ix] - coord0;" + NL
            		+ IND + "double dely = coord1Data[iy] - coord1;" + NL
            		+ interpolations
            		+ "}" + NL;
    	}
    	for (Integer nVarsOutput : fri.getLinearRead3D()) {
        	String dataVarPars = "";
        	String dataIndexPars = "";
        	String interpolations = "";
            for (int k = 0; k < nVarsOutput; k++) {
            	dataVarPars = dataVarPars + "double &data" + k + ", ";
            	dataIndexPars = dataIndexPars + "int data" + k + "Index, ";
            	interpolations = interpolations + IND + "idx[0] = data" + k + "Index + nVars * (ix + coord0Size * (iy + coord1Size * (iz)));" + NL
            			+ IND + "idx[1] = data" + k + "Index + nVars * (ix - 1 + coord0Size * (iy + coord1Size * (iz)));" + NL
            			+ IND + "idx[2] = data" + k + "Index + nVars * (ix + coord0Size * (iy - 1 + coord1Size * (iz)));" + NL
            			+ IND + "idx[3] = data" + k + "Index + nVars * (ix + coord0Size * (iy + coord1Size * (iz - 1)));" + NL
            			+ IND + "idx[4] = data" + k + "Index + nVars * (ix - 1 + coord0Size * (iy - 1 + coord1Size * (iz)));" + NL
            			+ IND + "idx[5] = data" + k + "Index + nVars * (ix - 1 + coord0Size * (iy + coord1Size * (iz - 1)));" + NL
            			+ IND + "idx[6] = data" + k + "Index + nVars * (ix + coord0Size * (iy - 1 + coord1Size * (iz - 1)));" + NL
            			+ IND + "idx[7] = data" + k + "Index + nVars * (ix - 1 + coord0Size * (iy - 1 + coord1Size * (iz - 1)));" + NL
            			+ IND  + "// set up aux vars for interpolation" + NL
            			+ IND + "fh[0] = varData[idx[0]];" + NL
            			+ IND + "fh[1] = varData[idx[1]];" + NL
            			+ IND + "fh[2] = varData[idx[2]];" + NL
            			+ IND + "fh[3] = varData[idx[3]];" + NL
            			+ IND + "fh[4] = varData[idx[4]];" + NL
            			+ IND + "fh[5] = varData[idx[5]];" + NL
            			+ IND + "fh[6] = varData[idx[6]];" + NL
            			+ IND + "fh[7] = varData[idx[7]];" + NL
            			+ IND + "// set up coeffs of interpolation polynomical and evaluate function values" + NL
            			+ IND + "a1 = fh[0];" + NL
            			+ IND + "a2 = dxi   * ( fh[1] - fh[0] );" + NL
            			+ IND + "a3 = dyi   * ( fh[2] - fh[0] );" + NL
            			+ IND + "a4 = dzi   * ( fh[3] - fh[0] );" + NL
            			+ IND + "a5 = dxyi  * ( fh[4] - fh[1] - fh[2] + fh[0] );" + NL
            			+ IND + "a6 = dxzi  * ( fh[5] - fh[1] - fh[3] + fh[0] );" + NL
            			+ IND + "a7 = dyzi  * ( fh[6] - fh[2] - fh[3] + fh[0] );" + NL
            			+ IND + "a8 = dxyzi * ( fh[7] - fh[0] + fh[1] + fh[2] + fh[3] - fh[4] - fh[5] - fh[6] );" + NL
            			+ IND + "data" + k + " = a1 +  a2 * delx +  a3 * dely +  a4 * delz +  a5 * delx * dely +  a6 * delx * delz +  a7 * dely * delz +  a8 * delx * dely * delz;" + NL;
            }            
            readFunctions = readFunctions + "/*" + NL
                    + " * Reads fields from a 3D table interpolating in give coordinates." + NL
                    + " * Uses Trilinear interpolation." + NL
                    + " * Table must be equidistant." + NL
                    + " *    nCoords:            number of coordinates of table" + NL
                    + " *    nOutFields:         number of fields to read" + NL
                    + " *    coord0, ...:        coordinate values to search in the table" + NL
                    + " *    data0, ...:         output variables to store interpolated fields" + NL
                    + " *    data0Index, ...:    index of fields inside the table" + NL
                    + " *    coord0Data, ...:    variables storing all values of table coordinates" + NL
                    + " *    coord0Dx, ...:      spacing of table coordinates" + NL
                    + " *    coord0Size, ...:    size of table coordinates" + NL
                    + " *    varData, ...:       table data" + NL
                    + " *    nVars, ...:         number of variables in the table" + NL
                    + " */" + NL
                    + "inline void readFromFileLinear3D(int nCoords, int nOutFields, double coord0, double coord1, double coord2, "  + dataVarPars + dataIndexPars + "double *coord0Data, double coord0Dx, int coord0Size, double *coord1Data, double coord1Dx, int coord1Size, double *coord2Data, double coord2Dx, int coord2Size, double *varData, int nVars) {" + NL
                    + IND + "// determine spacing parameters of table" + NL
            		+ IND + "double dx = coord0Dx;" + NL
            		+ IND + "double dy = coord1Dx;" + NL
            		+ IND + "double dz = coord2Dx;" + NL
            		+ IND + "double dxi = 1. / dx;" + NL
            		+ IND + "double dyi = 1. / dy;" + NL
            		+ IND + "double dzi = 1. / dz;" + NL
            		+ IND + "double dxyi  = dxi * dyi;" + NL
            		+ IND + "double dxzi  = dxi * dzi;" + NL
            	    + IND + "double dyzi  = dyi * dzi;" + NL
            	    + IND + "double dxyzi = dxi * dyi * dzi;" + NL
            		+ IND + "double fh[8];" + NL
            		+ IND + "double a1, a2, a3, a4, a5, a6, a7, a8;" + NL
            		+ IND + "// determine location in table" + NL
            		+ IND + "int idx[8];" + NL
            		+ IND + "int ix  = 1 + int((coord0 - coord0Data[0] - 1.e-10)/coord0Dx);" + NL
            		+ IND + "int iy  = 1 + int((coord1 - coord1Data[0] - 1.e-10)/coord1Dx);" + NL
            		+ IND + "int iz  = 1 + int((coord2 - coord2Data[0] - 1.e-10)/coord2Dx);" + NL
            		+ IND + "ix = MAX( 1, MIN( ix, coord0Size - 1 ) );" + NL
            		+ IND + "iy = MAX( 1, MIN( iy, coord1Size - 1 ) );" + NL
            		+ IND + "iz = MAX( 1, MIN( iz, coord2Size - 1 ) );" + NL
            		+ IND  + "// set up aux vars for interpolation" + NL
            		+ IND + "double delx = coord0Data[ix] - coord0;" + NL
            		+ IND + "double dely = coord1Data[iy] - coord1;" + NL
            		+ IND + "double delz = coord2Data[iz] - coord2;" + NL
            		+ interpolations
            		+ "}" + NL;
    	}
    	
        //Create linear reads with derivatives
    	for (Integer nVarsOutput : fri.getLinearReadWithDerivatives1D()) {
        	String dataVarPars = "";
        	String dataIndexPars = "";
        	String interpolations = "";
        	String dataDerPars = "";
            for (int k = 0; k < nVarsOutput; k++) {
            	dataVarPars = dataVarPars + "double &data" + k + ", ";
            	dataDerPars = dataDerPars + "double &der" + k + "Coord0, ";
            	dataIndexPars = dataIndexPars + "int data" + k + "Index, ";
            	interpolations = interpolations + IND + "idx[0] = data" + k + "Index + nVars * (ix);" + NL
            			 + IND + "idx[1] = data" + k + "Index + nVars * (ix - 1);" + NL
             	         + IND  + "// set up aux vars for interpolation" + NL
            			 + IND + "fh[0] = varData[idx[0]];" + NL
            			 + IND + "fh[1] = varData[idx[1]];" + NL
            			 + IND + "// set up coeffs of interpolation polynomical and evaluate function values" + NL
            			 + IND + "a1 = fh[0];" + NL
            			 + IND + "a2 = dxi   * ( fh[1] - fh[0] );" + NL
            			 + IND + "der" + k + "Coord0 = - a2;" + NL
            			 + IND + "data" + k + " = a1 +  a2 * delx;" + NL;
            }            
            readFunctions = readFunctions + "/*" + NL
                    + " * Reads fields from a 1D table interpolating in give coordinates." + NL
                    + " * It also calculates the derivatives of the fields with coordinates." + NL
                    + " * Uses Trilinear interpolation." + NL
                    + " * Table must be equidistant." + NL
                    + " *    nCoords:            number of coordinates of table" + NL
                    + " *    nOutFields:         number of fields to read" + NL
                    + " *    coord0, ...:        coordinate values to search in the table" + NL
                    + " *    data0, ...:         output variables to store interpolated fields" + NL
                    + " *    data0Index, ...:    index of fields inside the table" + NL
                    + " *    der0Coord0, ...:    output variables where derivatives are calculated" + NL
                    + " *    coord0Data, ...:    variables storing all values of table coordinates" + NL
                    + " *    coord0Dx, ...:      spacing of table coordinates" + NL
                    + " *    coord0Size, ...:    size of table coordinates" + NL
                    + " *    varData, ...:       table data" + NL
                    + " *    nVars, ...:         number of variables in the table" + NL
                    + " */" + NL
            		+ "inline void readFromFileLinearWithDerivatives1D(int nCoords, int nOutFields, double coord0, "  + dataVarPars + dataIndexPars + dataDerPars + "double *coord0Data, double coord0Dx, int coord0Size, double *varData, int nVars) {" + NL
            		+ IND + "// determine spacing parameters of table" + NL
            		+ IND + "double dx = coord0Dx;" + NL
            		+ IND + "double dxi = 1. / dx;" + NL
            		+ IND + "double fh[2];" + NL
            		+ IND + "int idx[2];" + NL
            		+ IND + "double a1, a2;" + NL
            		+ IND + "// determine location in table" + NL
            		+ IND + "int ix  = 1 + int((coord0 - coord0Data[0] - 1.e-10)/coord0Dx);" + NL
            		+ IND + "ix = MAX( 1, MIN( ix, coord0Size - 1 ) );" + NL
        			+ IND  + "// set up aux vars for interpolation" + NL
            		+ IND + "double delx = coord0Data[ix] - coord0;" + NL
            		+ interpolations
            		+ "}" + NL;
    	}
    	for (Integer nVarsOutput : fri.getLinearReadWithDerivatives2D()) {
        	String dataVarPars = "";
        	String dataIndexPars = "";
        	String interpolations = "";
         	String dataDerPars = "";
            for (int k = 0; k < nVarsOutput; k++) {
            	dataVarPars = dataVarPars + "double &data" + k + ", ";
            	dataDerPars = dataDerPars + "double &der" + k + "Coord0, double &der" + k + "Coord1, ";
            	dataIndexPars = dataIndexPars + "int data" + k + "Index, ";
            	interpolations = interpolations + IND + "idx[0] = data" + k + "Index + nVars * (ix + coord0Size * (iy));" + NL
            			+ IND + "idx[1] = data" + k + "Index + nVars * (ix - 1 + coord0Size * (iy));" + NL
            			+ IND + "idx[2] = data" + k + "Index + nVars * (ix + coord0Size * (iy - 1));" + NL
            			+ IND + "idx[3] = data" + k + "Index + nVars * (ix - 1 + coord0Size * (iy - 1));" + NL
            			+ IND  + "// set up aux vars for interpolation" + NL
            			+ IND + "fh[0] = varData[idx[0]];" + NL
            			+ IND + "fh[1] = varData[idx[1]];" + NL
            			+ IND + "fh[2] = varData[idx[2]];" + NL
            			+ IND + "fh[3] = varData[idx[3]];" + NL
            			+ IND + "// set up coeffs of interpolation polynomical and evaluate function values" + NL
            			+ IND + "a1 = fh[0];" + NL
            			+ IND + "a2 = dxi   * ( fh[1] - fh[0] );" + NL
            			+ IND + "a3 = dyi   * ( fh[2] - fh[0] );" + NL
            			+ IND + "a4 = dxyi  * ( fh[3] - fh[1] - fh[2] + fh[0] );" + NL
              			+ IND + "der" + k + "Coord0 = - a2;" + NL
            			+ IND + "der" + k + "Coord1 = - a3;" + NL
            			+ IND + "data" + k + " = a1 +  a2 * delx +  a3 * dely + a5 * delx * dely;" + NL;
            }            
            readFunctions = readFunctions + "/*" + NL
                    + " * Reads fields from a 2D table interpolating in give coordinates." + NL
                    + " * It also calculates the derivatives of the fields with coordinates." + NL
                    + " * Uses Trilinear interpolation." + NL
                    + " * Table must be equidistant." + NL
                    + " *    nCoords:            number of coordinates of table" + NL
                    + " *    nOutFields:         number of fields to read" + NL
                    + " *    coord0, ...:        coordinate values to search in the table" + NL
                    + " *    data0, ...:         output variables to store interpolated fields" + NL
                    + " *    data0Index, ...:    index of fields inside the table" + NL
                    + " *    der0Coord0, ...:    output variables where derivatives are calculated" + NL
                    + " *    coord0Data, ...:    variables storing all values of table coordinates" + NL
                    + " *    coord0Dx, ...:      spacing of table coordinates" + NL
                    + " *    coord0Size, ...:    size of table coordinates" + NL
                    + " *    varData, ...:       table data" + NL
                    + " *    nVars, ...:         number of variables in the table" + NL
                    + " */" + NL
                    + "inline void readFromFileLinearWithDerivatives2D(int nCoords, int nOutFields, double coord0, double coord1, "  + dataVarPars + dataIndexPars + dataDerPars +"double *coord0Data, double coord0Dx, int coord0Size, double *coord1Data, double coord1Dx, int coord1Size, double *varData, int nVars) {" + NL
                    + IND + "// determine spacing parameters of table" + NL
            		+ IND + "double dx = coord0Dx;" + NL
            		+ IND + "double dy = coord1Dx;" + NL
            		+ IND + "double dxi = 1. / dx;" + NL
            		+ IND + "double dyi = 1. / dy;" + NL
            		+ IND + "double dxyi  = dxi * dyi;" + NL
            		+ IND + "double fh[4];" + NL
            		+ IND + "double a1, a2, a3, a4;" + NL
            		+ IND + "// determine location in table" + NL
            		+ IND + "int idx[4];" + NL
            		+ IND + "int ix  = 1 + int((coord0 - coord0Data[0] - 1.e-10)/coord0Dx);" + NL
            		+ IND + "int iy  = 1 + int((coord1 - coord1Data[0] - 1.e-10)/coord1Dx);" + NL
            		+ IND + "ix = MAX( 1, MIN( ix, coord0Size - 1 ) );" + NL
            		+ IND + "iy = MAX( 1, MIN( iy, coord1Size - 1 ) );" + NL
            		+ IND  + "// set up aux vars for interpolation" + NL
            		+ IND + "double delx = coord0Data[ix] - coord0;" + NL
            		+ IND + "double dely = coord1Data[iy] - coord1;" + NL
            		+ interpolations
            		+ "}" + NL;
    	}
    	for (Integer nVarsOutput : fri.getLinearReadWithDerivatives3D()) {
        	String dataVarPars = "";
        	String dataIndexPars = "";
        	String interpolations = "";
        	String dataDerPars = "";
            for (int k = 0; k < nVarsOutput; k++) {
            	dataVarPars = dataVarPars + "double &data" + k + ", ";
            	dataDerPars = dataDerPars + "double &der" + k + "Coord0, double &der" + k + "Coord1, double &der" + k + "Coord2, ";
            	dataIndexPars = dataIndexPars + "int data" + k + "Index, ";
            	interpolations = interpolations + IND + "idx[0] = data" + k + "Index + nVars * (ix + coord0Size * (iy + coord1Size * (iz)));" + NL
            			+ IND + "idx[1] = data" + k + "Index + nVars * (ix - 1 + coord0Size * (iy + coord1Size * (iz)));" + NL
            			+ IND + "idx[2] = data" + k + "Index + nVars * (ix + coord0Size * (iy - 1 + coord1Size * (iz)));" + NL
            			+ IND + "idx[3] = data" + k + "Index + nVars * (ix + coord0Size * (iy + coord1Size * (iz - 1)));" + NL
            			+ IND + "idx[4] = data" + k + "Index + nVars * (ix - 1 + coord0Size * (iy - 1 + coord1Size * (iz)));" + NL
            			+ IND + "idx[5] = data" + k + "Index + nVars * (ix - 1 + coord0Size * (iy + coord1Size * (iz - 1)));" + NL
            			+ IND + "idx[6] = data" + k + "Index + nVars * (ix + coord0Size * (iy - 1 + coord1Size * (iz - 1)));" + NL
            			+ IND + "idx[7] = data" + k + "Index + nVars * (ix - 1 + coord0Size * (iy - 1 + coord1Size * (iz - 1)));" + NL
            			+ IND  + "// set up aux vars for interpolation" + NL
            			+ IND + "fh[0] = varData[idx[0]];" + NL
            			+ IND + "fh[1] = varData[idx[1]];" + NL
            			+ IND + "fh[2] = varData[idx[2]];" + NL
            			+ IND + "fh[3] = varData[idx[3]];" + NL
            			+ IND + "fh[4] = varData[idx[4]];" + NL
            			+ IND + "fh[5] = varData[idx[5]];" + NL
            			+ IND + "fh[6] = varData[idx[6]];" + NL
            			+ IND + "fh[7] = varData[idx[7]];" + NL
            			+ IND + "// set up coeffs of interpolation polynomical and evaluate function values" + NL
            			+ IND + "a1 = fh[0];" + NL
            			+ IND + "a2 = dxi   * ( fh[1] - fh[0] );" + NL
            			+ IND + "a3 = dyi   * ( fh[2] - fh[0] );" + NL
            			+ IND + "a4 = dzi   * ( fh[3] - fh[0] );" + NL
            			+ IND + "a5 = dxyi  * ( fh[4] - fh[1] - fh[2] + fh[0] );" + NL
            			+ IND + "a6 = dxzi  * ( fh[5] - fh[1] - fh[3] + fh[0] );" + NL
            			+ IND + "a7 = dyzi  * ( fh[6] - fh[2] - fh[3] + fh[0] );" + NL
            			+ IND + "a8 = dxyzi * ( fh[7] - fh[0] + fh[1] + fh[2] + fh[3] - fh[4] - fh[5] - fh[6] );" + NL
            			+ IND + "der" + k + "Coord0 = - a2;" + NL
            			+ IND + "der" + k + "Coord1 = - a3;" + NL
            			+ IND + "der" + k + "Coord2 = - a4;" + NL
            			+ IND + "data" + k + " = a1 +  a2 * delx +  a3 * dely +  a4 * delz +  a5 * delx * dely +  a6 * delx * delz +  a7 * dely * delz +  a8 * delx * dely * delz;" + NL;
            }            
            readFunctions = readFunctions + "/*" + NL
                    + " * Reads fields from a 3D table interpolating in give coordinates." + NL
                    + " * It also calculates the derivatives of the fields with coordinates." + NL
                    + " * Uses Trilinear interpolation." + NL
                    + " * Table must be equidistant." + NL
                    + " *    nCoords:            number of coordinates of table" + NL
                    + " *    nOutFields:         number of fields to read" + NL
                    + " *    coord0, ...:        coordinate values to search in the table" + NL
                    + " *    data0, ...:         output variables to store interpolated fields" + NL
                    + " *    data0Index, ...:    index of fields inside the table" + NL
                    + " *    der0Coord0, ...:    output variables where derivatives are calculated" + NL
                    + " *    coord0Data, ...:    variables storing all values of table coordinates" + NL
                    + " *    coord0Dx, ...:      spacing of table coordinates" + NL
                    + " *    coord0Size, ...:    size of table coordinates" + NL
                    + " *    varData, ...:       table data" + NL
                    + " *    nVars, ...:         number of variables in the table" + NL
                    + " */" + NL
                    + "inline void readFromFileLinearWithDerivatives3D(int nCoords, int nOutFields, double coord0, double coord1, double coord2, "  + dataVarPars + dataIndexPars + dataDerPars + "double *coord0Data, double coord0Dx, int coord0Size, double *coord1Data, double coord1Dx, int coord1Size, double *coord2Data, double coord2Dx, int coord2Size, double *varData, int nVars) {" + NL
                    + IND + "// determine spacing parameters of table" + NL
            		+ IND + "double dx = coord0Dx;" + NL
            		+ IND + "double dy = coord1Dx;" + NL
            		+ IND + "double dz = coord2Dx;" + NL
            		+ IND + "double dxi = 1. / dx;" + NL
            		+ IND + "double dyi = 1. / dy;" + NL
            		+ IND + "double dzi = 1. / dz;" + NL
            		+ IND + "double dxyi  = dxi * dyi;" + NL
            		+ IND + "double dxzi  = dxi * dzi;" + NL
            	    + IND + "double dyzi  = dyi * dzi;" + NL
            	    + IND + "double dxyzi = dxi * dyi * dzi;" + NL
            		+ IND + "double fh[8];" + NL
            		+ IND + "double a1, a2, a3, a4, a5, a6, a7, a8;" + NL
            		+ IND + "// determine location in table" + NL
            		+ IND + "int idx[8];" + NL
            		+ IND + "int ix  = 1 + int((coord0 - coord0Data[0] - 1.e-10)/coord0Dx);" + NL
            		+ IND + "int iy  = 1 + int((coord1 - coord1Data[0] - 1.e-10)/coord1Dx);" + NL
            		+ IND + "int iz  = 1 + int((coord2 - coord2Data[0] - 1.e-10)/coord2Dx);" + NL
            		+ IND + "ix = MAX( 1, MIN( ix, coord0Size - 1 ) );" + NL
            		+ IND + "iy = MAX( 1, MIN( iy, coord1Size - 1 ) );" + NL
            		+ IND + "iz = MAX( 1, MIN( iz, coord2Size - 1 ) );" + NL
            		+ IND  + "// set up aux vars for interpolation" + NL
            		+ IND + "double delx = coord0Data[ix] - coord0;" + NL
            		+ IND + "double dely = coord1Data[iy] - coord1;" + NL
            		+ IND + "double delz = coord2Data[iz] - coord2;" + NL
            		+ interpolations
            		+ "}" + NL;
    	}
        
        //Create read dimension from table
        findCoordFromFile = findCoordFromFile + createFindCoordFromFile(fri.getCoordReads1D(), 1);
        findCoordFromFile = findCoordFromFile + createFindCoordFromFile(fri.getCoordReads2D(), 2);
        findCoordFromFile = findCoordFromFile + createFindCoordFromFile(fri.getCoordReads3D(), 3);
        
        functionsFile = functionsFile.replaceFirst("#functions#",  readFunctions + NL + findCoordFromFile + NL
                + functions.substring(functions.indexOf("#functions#") + ("#functions#" + NL).length()) + initFileReadFunctions);
        return functionsFile;
    }
    
    private String createFindCoordFromFile(Set<Integer> coordFromFile, int nDimensions) {
    	String findCoordFromFile = "";    
     	for (Integer dimensionOutput : coordFromFile) {
 			String findCoordFromFileParams = "";
 			String findCoordFromFileParams2 = "";
 			String callParamsCoords = "";
 			String callParamsCoordsC1 = "";
 			String callParamsCoordsC2 = "";
 			String callParamsCoordsCtmp = "";
 			String callParamsCoordsData = "";
 	         for (int k = 0; k < nDimensions; k++) {
 	        	 findCoordFromFileParams = findCoordFromFileParams + "double coord" + k + ", ";
 	        	 findCoordFromFileParams2 = findCoordFromFileParams2 + "double* coord" + k + "Data, double coord" + k + "Dx, int coord" + k + "Size, ";
 	        	 if (k != dimensionOutput ) {
 	        		 callParamsCoords = callParamsCoords + "coord" + k + ", ";
 	        		callParamsCoordsC1 = callParamsCoordsC1 + "coord" + k + ", ";
 	        		callParamsCoordsC2 = callParamsCoordsC2 + "coord" + k + ", ";
 	        		callParamsCoordsCtmp = callParamsCoordsCtmp + "coord" + k + ", ";
 	        	 }
 	        	 else {
 	        		 callParamsCoords = callParamsCoords + "c, ";
 	        		callParamsCoordsC1 = callParamsCoordsC1 + "c1, ";
 	        		callParamsCoordsC2 = callParamsCoordsC2 + "c2, ";
 	        		callParamsCoordsCtmp = callParamsCoordsCtmp + "ctmp, ";
 	        	 }
 	        	 callParamsCoordsData = callParamsCoordsData + "coord" + k + "Data, coord" + k + "Dx, coord" + k + "Size, ";
            }
 			findCoordFromFile = findCoordFromFile + "/*" + NL
                    + " * Given a field in the table and all coordinates but one, it finds the remnaining coordinate that fits better with the values." + NL
                    + " *    nCoords:            number of coordinates of table" + NL
                    + " *    nInFields:          number of fields given" + NL
                    + " *    coord0, ...:        coordinate values to search in the table" + NL
                    + " *    datain:             field used to search the coordinate" + NL
                    + " *    dataIndex:          index of field in the table" + NL
                    + " *    coord0Data, ...:    variables storing all values of table coordinates" + NL
                    + " *    coord0Dx, ...:      spacing of table coordinates" + NL
                    + " *    coord0Size, ...:    size of table coordinates" + NL
                    + " *    varData, ...:       table data" + NL
                    + " *    nVars, ...:         number of variables in the table" + NL
                    + " */" + NL 
                    + "inline double findCoordFromFile" + nDimensions + "D_" + dimensionOutput + "(int nCoords, int nInFields, " + findCoordFromFileParams + "double datain, int dataIndex, " + findCoordFromFileParams2 + "double *varData, int nVars) {" + NL
 					+ IND + "// local vars" + NL
 					+ IND + "double cmax = coord" + dimensionOutput + "Data[coord" + dimensionOutput + "Size-1]; // max " + NL
 					+ IND + "double cmin = coord" + dimensionOutput + "Data[0]; // min" + NL + NL
 					+ IND + "// setting up some vars" + NL
 					+ IND + "double data0 = datain;" + NL
 					+ IND + "double data1 = data0;" + NL
 					+ IND + "double data, data2;" + NL
 					+ IND + "double c0 = coord" + dimensionOutput + "; " + NL
 					+ IND + "double c = coord" + dimensionOutput + "; " + NL
 					+ IND + "double c1 = c; " + NL + NL
 					+ IND + "// step 1: do we already have the right temperature" + NL
 					+ IND + "readFromFileLinear" + nDimensions + "D(nCoords, nInFields, " + callParamsCoords + "data, dataIndex, " + callParamsCoordsData + "varData, nVars);" + NL
 					+ IND + "if(fabs(data-data0) < 1e-6*fabs(data0)) {" + NL
 					+ IND + IND + "return coord" + dimensionOutput + ";" + NL
 					+ IND + "}" + NL
 					+ IND + "// try bisection" + NL
 					+ IND + "int itmax = 100;" + NL
 					+ IND + "// temporary local vars" + NL
 					+ IND + "double c2;" + NL
 					+ IND + "double data1a;" + NL
 					+ IND + "double data2a;" + NL
 					+ IND + "// prepare" + NL
 					+ IND + "c = c0; " + NL
 					+ IND + "// Bisection by value" + NL
 					+ IND + "c1 = MIN(c0 + 2 * coord" + dimensionOutput + "Dx, cmax);" + NL
 					+ IND + "c2 = MAX(c0 - 2 * coord" + dimensionOutput + "Dx, cmin);" + NL
 					+ IND + "readFromFileLinear3D(nCoords, nInFields, coord0, c1, coord2, data1a, dataIndex, coord0Data, coord0Dx, coord0Size, coord1Data, coord1Dx, coord1Size, coord2Data, coord2Dx, coord2Size, varData, nVars);" + NL
 					+ IND + "readFromFileLinear3D(nCoords, nInFields, coord0, c2, coord2, data2a, dataIndex, coord0Data, coord0Dx, coord0Size, coord1Data, coord1Dx, coord1Size, coord2Data, coord2Dx, coord2Size, varData, nVars);" + NL
 					+ IND + "data1=data1a-data0;" + NL
 					+ IND + "data2=data2a-data0;" + NL
 					+ IND + "int bcount = 0;" + NL
 					+ IND + "int maxbcount = 80;" + NL
 					+ IND + "bool notBracketed = false;" + NL
 					+ IND + "while(data1*data2 >= 0.0) {" + NL
 					+ IND + IND + "c1 = MIN(c1 + 2 * coord" + dimensionOutput + "Dx,cmax);" + NL
 					+ IND + IND + "c2 = MAX(c2 - 2 * coord" + dimensionOutput + "Dx,cmin);" + NL
 					+ IND + IND + "readFromFileLinear3D(nCoords, nInFields, coord0, c1, coord2, data1a, dataIndex, coord0Data, coord0Dx, coord0Size, coord1Data, coord1Dx, coord1Size, coord2Data, coord2Dx, coord2Size, varData, nVars);" + NL
 					+ IND + IND + "readFromFileLinear3D(nCoords, nInFields, coord0, c2, coord2, data2a, dataIndex, coord0Data, coord0Dx, coord0Size, coord1Data, coord1Dx, coord1Size, coord2Data, coord2Dx, coord2Size, varData, nVars);" + NL
 					+ IND + IND + "data1=data1a-data0;" + NL
 					+ IND + IND + "data2=data2a-data0;" + NL
 					+ IND + IND + "bcount++;" + NL
 					+ IND + IND + "if(bcount >= maxbcount) {" + NL
 					+ IND + IND + IND + "notBracketed = true;" + NL
 					+ IND + IND + IND + "break;" + NL
 					+ IND + IND + "}" + NL
 					+ IND + "} " + NL
 					+ IND + "if (!notBracketed) {" + NL
 					+ IND + IND + "//data1 and data2 may have been modified in loop above, so we check here to see if either is zero." + NL
 					+ IND + IND + "if (equalsEq(data1, 0.0)) {" + NL
 					+ IND + IND + IND + "return c1;" + NL
 					+ IND + IND + "}" + NL
 					+ IND + IND + "if (equalsEq(data2, 0.0)) {" + NL
 					+ IND + IND + IND + "return c2;" + NL
 					+ IND + IND + "}" + NL
 					+ IND + IND + "double dc, cmid, fmid;" + NL
 					+ IND + IND + "if(data1 < 0.0) {" + NL
 					+ IND + IND + IND + "c = c1;" + NL
 					+ IND + IND + IND + "dc = c2 - c1;" + NL
 					+ IND + IND + "} else {" + NL
 					+ IND + IND + IND + "c = c2;" + NL
 					+ IND + IND + IND + "dc = c1 - c2;" + NL
 					+ IND + IND + "}" + NL
 					+ IND + IND + "for(int it=0;it<itmax;it++) {" + NL
 					+ IND + IND + IND + "dc = dc * 0.5;" + NL
 					+ IND + IND + IND + "cmid = c + dc;" + NL
 					+ IND + IND + IND + "readFromFileLinear3D(nCoords, nInFields, coord0, cmid, coord2, data, dataIndex, coord0Data, coord0Dx, coord0Size, coord1Data, coord1Dx, coord1Size, coord2Data, coord2Dx, coord2Size, varData, nVars);" + NL
 					+ IND + IND + IND + "fmid=data-data0;" + NL
 					+ IND + IND + IND + "if(fmid <= 0.0) c=cmid;" + NL
 					+ IND + IND + IND + "if(fabs(data - data0) <= fabs(1e-10*data0)) {" + NL
 					+ IND + IND + IND + IND + "return cmid;" + NL
 					+ IND + IND + IND + "}" + NL
 					+ IND + IND + "} " + NL
 					+ IND + "}" + NL
 					+ IND + "//Try bisection by index" + NL
 					+ IND + "int i0  = int((c0 - coord1Data[0]) / coord1Dx);  // index of point closest to lto" + NL	
 					+ IND + "int ilo = MIN(MAX(0, i0), coord" + dimensionOutput + "Size-2);" + NL
 					+ IND + "int ihi = MAX(MIN(coord" + dimensionOutput + "Size-1, i0 + 1), 1);" + NL + NL
 					+ IND + "c1 = coord" + dimensionOutput + "Data[ilo];" + NL
 					+ IND + "c2 = coord" + dimensionOutput + "Data[ihi];" + NL
 					+ IND + "readFromFileLinear" + nDimensions + "D(nCoords, nInFields, " + callParamsCoordsC1 + "data1a, dataIndex, " + callParamsCoordsData + "varData, nVars);" + NL
 					+ IND + "readFromFileLinear" + nDimensions + "D(nCoords, nInFields, " + callParamsCoordsC2 + "data2a, dataIndex, " + callParamsCoordsData + "varData, nVars);" + NL
 					+ IND + "data1=data1a-data0;" + NL
 					+ IND + "data2=data2a-data0;" + NL
 					+ IND + "// iterate until we bracket the right eps, but enforce" + NL
 					+ IND + "// ddata/dc > 0, so eps(c1) > eps(c2)" + NL
 					+ IND + "if(data1*data2 >= 0.0) {" + NL
 					+ IND + IND + "if (data1 > 0.0) {" + NL
 					+ IND + IND + IND + "int k = 0;" + NL
 					+ IND + IND + IND + "while (ilo >= 1 && data1 > 0.0) {" + NL
 					+ IND + IND + IND + IND + "ilo = MAX(0,ilo - 2*k - 1);" + NL
 					+ IND + IND + IND + IND + "c1 = coord1Data[ilo];" + NL
 					+ IND + IND + IND + IND + "readFromFileLinear" + nDimensions + "D(nCoords, nInFields, " + callParamsCoordsC1 + "data1a, dataIndex, " + callParamsCoordsData + "varData, nVars);" + NL
 					+ IND + IND + IND + IND + "data1 = data1a - data0;" + NL
 					+ IND + IND + IND + IND + "k++;" + NL
 					+ IND + IND + IND + "}" + NL
 					+ IND + IND + IND + "if (data1 > 0.0) {" + NL
 					+ IND + IND + IND + IND + "return coord" + dimensionOutput + "Data[0];" + NL
 					+ IND + IND + IND + "}" + NL
 					+ IND + IND + "}" + NL
 					+ IND + IND + "if (data2 < 0.0) {" + NL
 					+ IND + IND + IND + "int k = 0;" + NL
 					+ IND + IND + IND + "while (ihi <= coord" + dimensionOutput + "Size-2 && data2 < 0.0) {" + NL
 					+ IND + IND + IND + IND + "ihi = MIN(coord" + dimensionOutput + "Size - 1, ihi + 2*k + 1);" + NL
 					+ IND + IND + IND + IND + "c2 = coord" + dimensionOutput + "Data[ihi];" + NL
 					+ IND + IND + IND + IND + "readFromFileLinear" + nDimensions + "D(nCoords, nInFields, " + callParamsCoordsC2 + "data2a, dataIndex, " + callParamsCoordsData + "varData, nVars);" + NL
 					+ IND + IND + IND + IND + "data2 = data2a - data0;" + NL
 					+ IND + IND + IND + IND + "k++;" + NL
 					+ IND + IND + IND + "}" + NL
 					+ IND + IND + IND + "if (data2 < 0.0) {" + NL
 					+ IND + IND + IND + IND + "return coord" + dimensionOutput + "Data[coord" + dimensionOutput + "Size - 1];" + NL
 					+ IND + IND + IND + "}" + NL
 					+ IND + IND + "}" + NL
 					+ IND + "}" + NL
 					+ IND + "//data1 and data2 may have been modified in loop above, so we check here to see if either is zero." + NL
 					+ IND + "if (equalsEq(data1, 0.0)) {" + NL
 					+ IND + IND + "return coord" + dimensionOutput + "Data[ilo];" + NL
 					+ IND + "}" + NL
 					+ IND + "if (equalsEq(data2, 0.0)) {" + NL
 					+ IND + IND + "return coord" + dimensionOutput + "Data[ihi];" + NL
 					+ IND + "}" + NL + NL  
 					+ IND + "int jj = 0;" + NL
 					+ IND + "bool found = false;" + NL
 					+ IND + "if ((ihi - ilo) == 1) {" + NL
 					+ IND + IND + "found = true;" + NL
 					+ IND + "}" + NL + NL
 					+ IND + "itmax = 10;" + NL
 					+ IND + "while (jj < itmax && !found) {" + NL
 					+ IND + IND + "int ii = (ihi + ilo) / 2;" + NL
 					+ IND + IND + "double ctmp = coord" + dimensionOutput + "Data[ii];" + NL
 					+ IND + IND + "readFromFileLinear" + nDimensions + "D(nCoords, nInFields, " + callParamsCoordsCtmp + "data, dataIndex, " + callParamsCoordsData + "varData, nVars);" + NL
 					+ IND + IND + "if (fabs(data - data0) < 1.0e-15*fabs(data0)) {" + NL
 					+ IND + IND + IND + "return ctmp;" + NL
 					+ IND + IND + "} else if (data < data0) {" + NL
 					+ IND + IND + IND + "ilo = ii;" + NL
 					+ IND + IND + IND + "data1a = data;" + NL
 					+ IND + IND + "} else if (data > data0) {" + NL
 					+ IND + IND + IND + "ihi = ii;" + NL
 					+ IND + IND + IND + "data2a = data;" + NL
 					+ IND + IND + "}" + NL
 					+ IND + IND + "if ((ihi - ilo) == 1) {" + NL
 					+ IND + IND + IND + "found = true;" + NL
 					+ IND + IND + "}" + NL
 					+ IND + IND + "jj++;" + NL
 					+ IND + "}" + NL + NL
 					+ IND + "if (found) {" + NL
 					+ IND + IND + "//The root has been bracketed.  Now use linear interpolation to find the value." + NL
 					+ IND + IND + "return (data0 - data1a)*(coord" + dimensionOutput + "Data[ihi] - coord" + dimensionOutput + "Data[ilo]) / (data2a - data1a) + coord" + dimensionOutput + "Data[ilo];" + NL
 					+ IND + "}" + NL + NL
 					+ IND + "TBOX_ERROR(\"findCoordFromFile" + nDimensions + "D_" + dimensionOutput + ": cannot find the coordinate \"<<coord" + dimensionOutput + "<<\" from \"<<data);" + NL
 					+ "}" + NL;
     	}
         return findCoordFromFile;
    }
    
    /**
     * Create NonSync.c file.
     * @param pi                The problem info
     * @return                  The code
     * @throws CGException      CG00X External error
     */
    private String createNonSync(ProblemInfo pi) throws CGException {
        try {
            //Get the template
            String nonSync = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/NonSync.C"), 0);
            //Set positions
            String pos1 = "";
            String pos2 = "";
            String pos3 = "";
            String getPositions = "";
            String setPositions = "";
            String condition = "";
            for (int i = 0; i < pi.getDimensions(); i++) {
                String name = pi.getContCoordinates().get(i);
                pos1 = pos1 + IND + "position" + name + " = pos[" + i + "];" + NL;
                pos2 = pos2 + IND + "this->position" + name + " = data.position" + name + ";" + NL;
                pos3 = pos3 + IND + IND + "position" + name + " = data.position" + name + ";" + NL;
                getPositions = getPositions + "double NonSync::getposition" + name + "()" + NL
                        + "{" + NL
                        + IND + "return position" + name + ";" + NL
                        + "}" + NL + NL;
                setPositions = setPositions + "void NonSync::setposition" + name + "(const double pos)" + NL
                        + "{" + NL
                        + IND + "position" + name + " = pos;" + NL
                        + "}" + NL + NL;
                condition = condition + "!equals(position" + name + ", data.position" + name + ") || "; 
            }
            condition = condition.substring(0, condition.lastIndexOf(" ||"));
            
            nonSync = nonSync.replaceFirst("#pos1#", pos1);
            nonSync = nonSync.replaceFirst("#pos2#", pos2);
            nonSync = nonSync.replaceFirst("#pos3#", pos3);
            nonSync = nonSync.replaceFirst("#getPosition#", getPositions);
            nonSync = nonSync.replaceFirst("#setPosition#", setPositions);
            nonSync = nonSync.replaceFirst("#condition#", condition);
            nonSync = nonSync.replaceFirst("#DIM#", String.valueOf(pi.getDimensions()));
            
            return nonSync;
        } 
        catch (Exception e) {
            throw new CGException(CGException.CG00X, e.getMessage());
        }
    }
    
    /**
     * Create nonSync.h file.
     * @param pi                The problem info
     * @return                  The code
     * @throws CGException      CG00X External error
     */
    private String createNonSyncH(ProblemInfo pi) throws CGException {
        try {
            //Get the template
            String nonSyncH = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/NonSync.h"), 0);
            //Set positions
            String positions = "";
            String getPositions = "";
            String setPositions = "";
            for (String coord: pi.getContCoordinates()) {
                positions = positions + IND + "double position" + coord + ";" + NL;
                getPositions = getPositions + IND + "double getposition" + coord + "();" + NL;
                setPositions = setPositions + IND + "void setposition" + coord + "(const double pos);" + NL;
            }
            
            nonSyncH = nonSyncH.replaceFirst("#positions#", positions);
            nonSyncH = nonSyncH.replaceFirst("#getPositions#", getPositions);
            nonSyncH = nonSyncH.replaceFirst("#setPositions#", setPositions);
            nonSyncH = nonSyncH.replaceFirst("#DIM#", String.valueOf(pi.getDimensions()));
            
            return nonSyncH;
        } 
        catch (Exception e) {
            throw new CGException(CGException.CG00X, e.getMessage());
        }
    }
    
    /**
     * Create ParticleRefine.C file.
     * @param pi                The problem info
     * @return                  The code
     * @throws CGException      CG00X External error
     */
    private String createParticleRefine(ProblemInfo pi) throws CGException {
        try {
            //Get the template
            String particleRefine = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/ParticleRefine.C"), 0);
            String constructorParams = "";
            String coordParams = "";
            String assignments = "";
            String coordParamsCall = "";
            String particleSeparation = "";
            String initPos = "";
            String index = "";
            String newIndex = "";
            String newIndexLimits = "";
            String lastPos = "";
            String indices = "";
            for (int i = 0; i < pi.getDimensions(); i++) {
                String coord = pi.getCoordinates().get(i);
                String contCoord = CodeGeneratorUtils.discToCont(pi.getProblem(), coord);
                constructorParams = constructorParams + IND + "const double initial_particle_separation_" + contCoord + "," + NL;
                coordParams = coordParams + ", int min" + contCoord + ", int max" + contCoord + ", const double init_pos_" + contCoord + ", const double particle_separation_f_" + contCoord + ", int " + coord + coord + "";
                assignments = assignments + IND + contCoord + "Glower = grid_geom->getXLower()[" + i + "];" + NL
                		+ IND + contCoord + "Gupper = grid_geom->getXUpper()[" + i + "];" + NL
                		+ IND + "d_initial_particle_separation_" + contCoord + " = initial_particle_separation_" + contCoord + ";" + NL;
                coordParamsCall = coordParamsCall + ", ifirstf[" + i + "], ilastf[" + i + "], init_pos_" + contCoord + ", particle_separation_f_" + contCoord + ", " + coord + coord;
                particleSeparation = particleSeparation + IND + "double particle_separation_f_" + contCoord + " = d_initial_particle_separation_" + contCoord + " / ratioTo0[" + i + "];" + IND;
                initPos = initPos + IND + "double init_pos_" + contCoord + " = - particle_separation_f_" + contCoord + " * int(ratioTo0[" + i + "] / 2);" + NL
                		+ IND + "if (ratioTo0[" + i + "] % 2 == 0) {" + NL
                		+ IND + IND + "init_pos_" + contCoord + " = init_pos_" + contCoord + " + particle_separation_f_" + contCoord + " / 2;" + NL
            			+ IND + "}" + NL;
                lastPos = lastPos + IND + "double pos" + contCoord + " = newP->position" + contCoord + "_p;" + NL;
                indices = indices + IND + "double newPos" + contCoord + " = (pos" + contCoord + " - " + contCoord + "Glower)/dx[" + i + "];" + NL
                		+ IND + "int newIndex" + i + " = floor(newPos" + contCoord + "+ 1.0E-10);" + NL;
                index = index + coord + ", ";
                newIndex = newIndex + "newIndex" + i + ", ";
                newIndexLimits = newIndexLimits + "min" + contCoord + " <= newIndex" + i + " && newIndex" + i + " <= max" + contCoord + " && ";
            }
            index = index.substring(0, index.lastIndexOf(","));
            newIndex = newIndex.substring(0, newIndex.lastIndexOf(","));
            newIndexLimits = newIndexLimits.substring(0, newIndexLimits.lastIndexOf(" &&"));
            String fineLoopBegin = "";
            String coarseLoopBegin = "";
            String endLoop = "";
            String loopIndent = IND;
            String ratioLoopBegin = "";
            for (int i = pi.getDimensions() - 1; i >= 0; i--) {
                String coord = pi.getCoordinates().get(i);
            	fineLoopBegin = fineLoopBegin + loopIndent + "for(int " + coord + " = ifirstf[" + i + "]; " + coord + " <= ilastf[" + i + "]; " + coord + "++) {" + NL;
            	coarseLoopBegin = coarseLoopBegin + loopIndent + "for(int " + coord + " = floor(ifirstf[" + i + "]/ ratio[" + i + "]) - 1; " + coord + " <= ceil(ilastf[" + i + "]/ ratio[" + i + "]) + 1; " + coord + "++) {" + NL;
            	endLoop = loopIndent + "}" + NL + endLoop;
            	ratioLoopBegin = ratioLoopBegin + loopIndent + "for (int " + coord + coord + " = 0; " + coord + coord + " < ratioTo0[" + i + "]; " + coord + coord + "++) {" + NL;
            	loopIndent = loopIndent + IND;
            }
            
            String refineParticles = particleSeparation + NL
            		+ initPos + NL
            		+ IND + "hier::Box coarseLevelBox = d_patch_hierarchy->getPatchLevel(coarse_level)->getBoxes().getBoundingBox();" + NL
            		+ fineLoopBegin
            		+ loopIndent + "hier::Index idx(" + index + ");" + NL
                    + loopIndent + "Particles<T>* part = new Particles<T>();" + NL
                    + loopIndent + "fdata->replaceAddItemPointer(idx, part);" + NL
            		+ endLoop + NL
            		+ "//One extra coarse cell is needed, one coarse particle can create fine particles in positions of near coarse cells" + NL
            		+ coarseLoopBegin
            		+ loopIndent + "hier::Index idx(" + index + ");" + NL
            		+ loopIndent + "Particles<T>* part = cdata->getItem(idx);" + NL
            		+ loopIndent + "checkPosition(coarse, coarseLevelBox, " + index + ", *part);" + NL
            		+ loopIndent + "for (int pit = part->getNumberOfParticles() - 1; pit >= 0; pit--) {" + NL
            		+ loopIndent + IND + "T* particle = part->getParticle(pit);" + NL
                    + indent(ratioLoopBegin
                    		+ loopIndent + "addParticleToFine(cdata, fdata, *particle" + coordParamsCall + ", dx);" + NL
                    		+ endLoop + NL, loopIndent.length())
                    + loopIndent + "}" + NL
      				+ endLoop + NL;
            
            
            String addParticleToFine = "";
            
            String particleRefineInstances = "";
            for (String speciesName: pi.getParticleSpecies()) {
            	particleRefineInstances = particleRefineInstances + "template class ParticleRefine<Particle_" + speciesName + ">;" + NL;
            }
            
            String lastStep = "";
            boolean multipleTimeInterpolators = pi.getTimeInterpolationInfo().getTimeInterpolators().size() > 1;
            for(TimeInterpolator ti: pi.getTimeInterpolationInfo().getTimeInterpolators().values()) {

                String posSelector = "";
                for (int i = 0; i < ti.getVarAtStep().size(); i++) {
                	String positions = "";
                	ArrayList<String> pos = ti.getStepPositions(pi, i);
                	for (int j = 0; j < pos.size(); j++) {
                		positions = positions + IND + IND + "pos" + pi.getContCoordinates().get(j) + " = newP->" + pos.get(j) + ";" + NL;
                	}
                	posSelector = posSelector + IND + "if (d_step == " + (i + 1) + ") {" + NL
                			+ positions
                			+ IND + "}" + NL;
                }
            	for (String speciesName: pi.getParticleSpecies()) {
            		String currInd = IND;
            		String particleField = pi.getVariableInfo().getParticleFields().get(speciesName).get(0);
            		if (ti.getFields().contains(particleField)) {
                    	ArrayList<String> posVariables = pi.getVariableInfo().getPositionVariables(speciesName);
                        ArrayList<String> contCoords = pi.getContCoordinates();
                        String posInit = "";
                        for (int i = 0; i < posVariables.size(); i++) {
                            String name = posVariables.get(i);
                            String coord = "";
                            for (int j = 0; j < contCoords.size(); j++) {
                                if (name.contains("position" + contCoords.get(j))) {
                                	coord = contCoords.get(j);
                                }
                            }
                            String discCoord = pi.getCoordinates().get(contCoords.indexOf(coord));
                            posInit = posInit + IND + "newP->" + name + " = newP->" + name + " + init_pos_" + coord + " + " + discCoord + discCoord + " * particle_separation_f_" + coord + ";" + NL;
                        }
		            	if (multipleTimeInterpolators) {
		            		lastStep = lastStep + "if (std::is_same<T, Particle_" + speciesName + ">::value) ";
		            		addParticleToFine = addParticleToFine + IND + "if (std::is_same<T, Particle_" + speciesName + ">::value) {" + NL;
		            		currInd = currInd + IND;
		            	}
		            	lastStep = IND + lastStep + "d_step = " + String.valueOf(ti.getVarAtStep().size()) + ";"  + NL;
		            	
		                addParticleToFine = posInit
		                		+ lastPos
		                		+ posSelector
		                		+ indices
		                		+ currInd + "if (" + newIndexLimits + ") {" + NL
		                		+ currInd + IND + "hier::Index newIdx(" + newIndex + ");" + NL
		                		+ currInd + IND + "Particles<T>* destPart = fdata->getItem(newIdx);" + NL
		                		+ currInd + IND + "destPart->addParticle(*newP);" + NL
		                		+ currInd + "}" + NL;
		                if (multipleTimeInterpolators) {
		                	addParticleToFine = addParticleToFine + IND + "}" + NL;
		                }
            		}
            	}
            }
                        
            particleRefine = particleRefine.replaceFirst("#constructorParams#", constructorParams);
            particleRefine = particleRefine.replaceFirst("#coordParams#", coordParams);
            particleRefine = particleRefine.replaceFirst("#assignments#", assignments);
            
            
            particleRefine = particleRefine.replaceFirst("#lastStep#", lastStep);
            particleRefine = particleRefine.replaceFirst("#checkPosition#", createCheckPositionParticleRefine(pi.getContCoordinates(), pi.getVariableInfo(), multipleTimeInterpolators));
            particleRefine = particleRefine.replaceFirst("#refine_particles#", refineParticles);
            particleRefine = particleRefine.replaceFirst("#addParticleToFine#", addParticleToFine);
            particleRefine = particleRefine.replaceFirst("#particleRefineInstances#", particleRefineInstances);

            return particleRefine;
        } 
        catch (Exception e) {
        	e.printStackTrace(System.out);
            throw new CGException(CGException.CG00X, e.getMessage());
        }
    }
    
    /**
     * Create ParticleRefine.h file.
     * @param pi                The problem info
     * @return                  The code
     * @throws CGException      CG00X External error
     */
    private String createParticleRefineH(ProblemInfo pi) throws CGException {
        try {
            //Get the template
            String particleRefine = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/ParticleRefine.h"), 0);
            String dimVariables = IND + "double ";
            String constructorParams = "";
            String coordParams = "";
            String checkPosParams = "";
            for (int i = 0; i < pi.getDimensions(); i++) {
                String coord = pi.getCoordinates().get(i);
                String contCoord = CodeGeneratorUtils.discToCont(pi.getProblem(), coord);
                dimVariables = dimVariables + contCoord + "Glower, " + contCoord + "Gupper, d_initial_particle_separation_" + contCoord + ", ";
                constructorParams = constructorParams + IND + "const double initial_particle_separation_" + contCoord + ", " + NL;
                coordParams = coordParams + ", int min" + contCoord + ", int max" + contCoord + ", const double init_pos_" + contCoord + ", const double particle_separation_f_" + contCoord + ", int " + coord + coord + "";
                checkPosParams = checkPosParams + ", int index" + i;
            }
            dimVariables = dimVariables.substring(0, dimVariables.lastIndexOf(",")) + ";" + NL;
            String particleSpeciesIncludes = "";
            for (String speciesName : pi.getParticleSpecies()) {
            	particleSpeciesIncludes = particleSpeciesIncludes + "#include \"Particle_" + speciesName + ".h\"" + NL;
            }
            particleRefine = particleRefine.replaceFirst("#dimVariables#", dimVariables);
            particleRefine = particleRefine.replaceFirst("#constructorParams#", constructorParams);
            particleRefine = particleRefine.replaceFirst("#coordParams#", coordParams);
            particleRefine = particleRefine.replaceFirst("#checkPosParams#", checkPosParams);
            particleRefine = particleRefine.replaceFirst("#ParticleSpeciesIncludes#", particleSpeciesIncludes);
            
            return particleRefine;
        } 
        catch (Exception e) {
            throw new CGException(CGException.CG00X, e.getMessage());
        }
    }
    
    /**
     * Create MainRestartData.cpp file.
     * @param pi                The problem info
     * @return                  The code
     * @throws CGException      CG00X External error
     */
    private String createMainRestartData(ProblemInfo pi) throws CGException {
        try {
            //Get the template
            String mainRestartData = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/MainRestartData.cpp"), 0);
           
            X3DMovInfo xinfo = pi.getX3dMovRegions();
            String putArrays = "";
            String getArrays = "";
            String functions = "";
            if (xinfo != null) {
            	int ts = xinfo.getTimeslices();
                for (int i = 0; i < xinfo.getName().size(); i++) {
                    String segName = xinfo.getName().get(i);
                    int segSize = xinfo.getSize().get(i);
                    getArrays = getArrays + IND + "restart_db->getDoubleArray(\"" + segName + "CellPoints\", " + segName + "CellPoints, " 
                            + ts + " * " + segSize + " * " + pi.getDimensions() + ");" + NL;
                    putArrays = putArrays + IND + "db->putDoubleArray(\"" + segName + "CellPoints\", " + segName + "CellPoints, " + ts 
                            + " * " + segSize + " * " + pi.getDimensions() + ");" + NL;
                    functions = functions + IND + "void MainRestartData::add" + segName + "CellPoints(double* x3d) {" + NL
                            + IND + IND + "for (int ts = 0; ts < " + ts + "; ts++) {" + NL
                            + IND + IND + IND + "for (int point = 0; point < " + segSize + "; point++) {" + NL
                            + IND + IND + IND + IND + "for (int coord = 0; coord < " + pi.getDimensions() + "; coord++) {" + NL
                            + IND + IND + IND + IND + IND + segName + "CellPoints[ts * (" + segSize + " * " + pi.getDimensions() 
                            + ") + point * " + pi.getDimensions() + " + coord] = x3d[ts * (" + segSize + " * " + pi.getDimensions() 
                            + ") + point * " + pi.getDimensions() + " + coord];" + NL
                            + IND + IND + IND + IND + "}" + NL
                            + IND + IND + IND + "}" + NL
                            + IND + IND + "}" + NL
                            + IND + "}" + NL + NL
                            + IND + IND + "double* MainRestartData::get" + segName + "CellPoints() {" + NL
                            + IND + IND + IND + "return " + segName + "CellPoints;" + NL
                            + IND + IND + "}" + NL;
                }
            }
            mainRestartData = mainRestartData.replaceFirst("#Movement functions#", functions);
    
            String sliceFunctions = "";
            if (activeSlicers) {
            	sliceFunctions = "/**" + NL
            			+ " * Returns d_next_slice_dump_iteration." + NL
            			+ " */" + NL
            			+ "vector<int> MainRestartData::getNextSliceDumpIteration()" + NL
            			+ "{" + NL
            			+ IND + "return d_next_slice_dump_iteration;" + NL
            			+ "}" + NL + NL
            			+ "/**" + NL
            			+ " * Sets d_next_slice_dump_iteration." + NL
            			+ " */" + NL
            			+ "void MainRestartData::setNextSliceDumpIteration(const vector<int> next_slice_dump_iteration)" + NL
              			+ "{" + NL
            			+ IND + "d_next_slice_dump_iteration = next_slice_dump_iteration;" + NL
            			+ "}" + NL + NL
            			+ "/**" + NL
            			+ " * Returns d_next_sphere_dump_iteration." + NL
            			+ " */" + NL
            			+ "vector<int> MainRestartData::getNextSphereDumpIteration()" + NL
              			+ "{" + NL
            			+ IND + "return d_next_sphere_dump_iteration;" + NL
            			+ "}" + NL + NL
            			+ "/**" + NL
            			+ " * Sets d_next_sphere_dump_iteration." + NL
            			+ " */" + NL
            			+ "void MainRestartData::setNextSphereDumpIteration(const vector<int> next_sphere_dump_iteration)" + NL
              			+ "{" + NL
            			+ IND + "d_next_sphere_dump_iteration = next_sphere_dump_iteration;" + NL
            			+ "}" + NL + NL;
            	putArrays = putArrays + IND + "if (d_next_slice_dump_iteration.size() > 0) {" + NL
            			+ IND + IND + "db->putIntegerVector(\"d_next_slice_dump_iteration\",d_next_slice_dump_iteration);" + NL
            			+ IND + "}" + NL
            			+ IND + "if (d_next_sphere_dump_iteration.size() > 0) {" + NL
            			+ IND + IND + "db->putIntegerVector(\"d_next_sphere_dump_iteration\",d_next_sphere_dump_iteration);" + NL
            			+ IND + "}" + NL;
            	getArrays = getArrays + IND + "if (restart_db->keyExists(\"d_next_slice_dump_iteration\")) {" + NL
            			+ IND + IND + "d_next_slice_dump_iteration = restart_db->getIntegerVector(\"d_next_slice_dump_iteration\");" + NL
            			+ IND + "}" + NL
            			+ IND + "if (restart_db->keyExists(\"d_next_sphere_dump_iteration\")) {" + NL
            			+ IND + IND + "d_next_sphere_dump_iteration = restart_db->getIntegerVector(\"d_next_sphere_dump_iteration\");" + NL
            			+ IND + "}" + NL;
            }
            String integrationFunctions = "";
            String pointFunctions = "";
            String dumpFunctions = "";
            if (pi.isHasABMMeshFields() || pi.isHasMeshFields()) {
            	dumpFunctions = "/**" + NL
            			+ " * Returns d_next_mesh_dump_iteration." + NL
            			+ " */" + NL
            			+ "int MainRestartData::getNextMeshDumpIteration()" + NL
            			+ "{" + NL
            			+ IND + "return d_next_mesh_dump_iteration;" + NL
            			+ "}" + NL + NL
            			+ "/**" + NL
            			+ " * Sets d_next_mesh_dump_iteration." + NL
            			+ " */" + NL
            			+ "void MainRestartData::setNextMeshDumpIteration(const int next_mesh_dump_iteration)" + NL
            			+ "{" + NL
            			+ IND + "d_next_mesh_dump_iteration = next_mesh_dump_iteration;" + NL
            			+ "}" + NL + NL;
            	integrationFunctions = IND + "/**" + NL
            			+ " * Returns d_next_integration_dump_iteration." + NL
            			+ " */" + NL
            			+ "vector<int> MainRestartData::getNextIntegrationDumpIteration()" + NL
            			+ "{" + NL
            			+ IND + "return d_next_integration_dump_iteration;" + NL
            			+ "}" + NL + NL
            			+ "/**" + NL
            			+ " * Sets d_next_integration_dump_iteration." + NL
            			+ " */" + NL
            			+ "void MainRestartData::setNextIntegrationDumpIteration(const vector<int> next_integration_dump_iteration)" + NL
            			+ "{" + NL
            			+ IND + "d_next_integration_dump_iteration = next_integration_dump_iteration;" + NL
            			+ "}" + NL + NL;
            	pointFunctions = IND + "/**" + NL
            			+ " * Returns d_next_point_dump_iteration." + NL
            			+ " */" + NL
            			+ "vector<int> MainRestartData::getNextPointDumpIteration()" + NL
            			+ "{" + NL
            			+ IND + "return d_next_point_dump_iteration;" + NL
            			+ "}" + NL + NL
            			+ "/**" + NL
            			+ " * Sets d_next_point_dump_iteration." + NL
            			+ " */" + NL
            			+ "void MainRestartData::setNextPointDumpIteration(const vector<int> next_point_dump_iteration)" + NL
            			+ "{" + NL
            			+ IND + "d_next_point_dump_iteration = next_point_dump_iteration;" + NL
            			+ "}" + NL + NL;

            	putArrays = putArrays + IND + "db->putInteger(\"d_next_mesh_dump_iteration\",d_next_mesh_dump_iteration);" + NL
            			+ IND + "if (d_next_integration_dump_iteration.size() > 0) {" + NL
            			+ IND + IND + "db->putIntegerVector(\"d_next_integration_dump_iteration\",d_next_integration_dump_iteration);" + NL
            			+ IND + "}" + NL
            			+ IND + "if (d_next_point_dump_iteration.size() > 0) {" + NL
            			+ IND + IND + "db->putIntegerVector(\"d_next_point_dump_iteration\",d_next_point_dump_iteration);" + NL
            			+ IND + "}" + NL;
            	getArrays = getArrays + IND + "d_next_mesh_dump_iteration = restart_db->getInteger(\"d_next_mesh_dump_iteration\");" + NL
            			+ IND + "if (restart_db->keyExists(\"d_next_integration_dump_iteration\")) {" + NL
            			+ IND + IND + "d_next_integration_dump_iteration = restart_db->getIntegerVector(\"d_next_integration_dump_iteration\");" + NL
            			+ IND + "}" + NL
            			+ IND + "if (restart_db->keyExists(\"d_next_point_dump_iteration\")) {" + NL
            			+ IND + IND + "d_next_point_dump_iteration = restart_db->getIntegerVector(\"d_next_point_dump_iteration\");" + NL
            			+ IND + "}" + NL;
            }
            if (pi.isHasABMMeshlessFields() || pi.isHasParticleFields()) {
            	dumpFunctions = dumpFunctions + "/**" + NL
            			+ " * Returns d_next_particle_dump_iteration." + NL
            			+ " */" + NL
            			+ "int MainRestartData::getNextParticleDumpIteration()" + NL
            			+ "{" + NL
            			+ IND + "return d_next_particle_dump_iteration;" + NL
            			+ "}" + NL + NL
            			+ "/**" + NL
            			+ " * Sets d_next_particle_dump_iteration." + NL
            			+ " */" + NL
            			+ "void MainRestartData::setNextParticleDumpIteration(const int next_particle_dump_iteration)" + NL
            			+ "{" + NL
            			+ IND + "d_next_particle_dump_iteration = next_particle_dump_iteration;" + NL
            			+ "}" + NL + NL;
                putArrays = putArrays + IND + "db->putInteger(\"d_next_particle_dump_iteration\",d_next_particle_dump_iteration);" + NL;
                getArrays = getArrays + IND + "d_next_particle_dump_iteration = restart_db->getInteger(\"d_next_particle_dump_iteration\");" + NL;
            }
            putArrays = putArrays + IND + "db->putIntegerVector(\"d_current_iteration\",d_current_iteration);" + NL;
            getArrays = getArrays + IND + "d_current_iteration = restart_db->getIntegerVector(\"d_current_iteration\");" + NL;

            
            String analysisFunctions = "";
            if (pi.getVariableInfo().getOutputVarsAnalysis().size() > 0) {
            	analysisFunctions = "/**" + NL
		             + " * Returns d_next_analysis_dump_iteration." + NL
		             + " */" + NL
		             + "int MainRestartData::getNextDumpAnalysisIteration()" + NL
		 			 + "{" + NL
        			 + IND + "return d_next_analysis_dump_iteration;" + NL
        			 + "}" + NL + NL
		             + "/**" + NL
		             + " * Sets d_next_analysis_dump_iteration" + NL
		             + " */" + NL
		             + "void MainRestartData::setNextDumpAnalysisIteration(int next_analysis_dump_iteration)" + NL
		 			+ "{" + NL
        			+ IND + "d_next_analysis_dump_iteration = next_analysis_dump_iteration;" + NL
        			+ "}" + NL + NL;
                putArrays = putArrays + IND + "db->putInteger(\"d_next_analysis_dump_iteration\",d_next_analysis_dump_iteration);" + NL;
                getArrays = getArrays + IND + "d_next_analysis_dump_iteration = restart_db->getInteger(\"d_next_analysis_dump_iteration\");" + NL;
            }
           
            mainRestartData = mainRestartData.replaceFirst("#DumpFunctions#", dumpFunctions);
        	mainRestartData = mainRestartData.replaceFirst("#AnalysisFunctions#", analysisFunctions);
            mainRestartData = mainRestartData.replaceFirst("#SliceFunctions#", sliceFunctions);
            mainRestartData = mainRestartData.replaceFirst("#IntegrationFunctions#", integrationFunctions + pointFunctions);
            mainRestartData = mainRestartData.replaceFirst("#putArrays#", putArrays);
            mainRestartData = mainRestartData.replaceFirst("#getArrays#", getArrays);
            
            return mainRestartData;
        } 
        catch (Exception e) {
        	e.printStackTrace(System.out);
            throw new CGException(CGException.CG00X, e.getMessage());
        }
    }
    
    /**
     * Create MainRestartData.h file.
     * @param pi                The problem info
     * @return                  The code
     * @throws CGException      CG00X External error
     */
    private String createMainRestartDataH( ProblemInfo pi) throws CGException {
        try {
            //Get the template
            String mainRestartDataH = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/MainRestartData.h"), 0);
           
            X3DMovInfo xinfo = pi.getX3dMovRegions();
            String objects = "";
            String functions = "";
            if (xinfo != null) {
                int ts = xinfo.getTimeslices();
                for (int i = 0; i < xinfo.getName().size(); i++) {
                    String segName = xinfo.getName().get(i);
                    int segSize = xinfo.getSize().get(i);
                    objects = objects + IND + "double " + segName + "CellPoints[" + ts + " * " + segSize + " * " + pi.getDimensions() + "];" + NL;
                    functions = functions + IND + "virtual void add" + segName + "CellPoints(double* x3d);" + NL
                            + IND + "virtual double* get" + segName + "CellPoints();" + NL;
                }	
            }
            mainRestartDataH = mainRestartDataH.replaceFirst("#Movement objects#", objects);
            mainRestartDataH = mainRestartDataH.replaceFirst("#Movement functions#", functions);

            String sliceFunctions = "";
            String outputVariables = "";
            if (activeSlicers) {
            	sliceFunctions = IND + "/**" + NL
            			+ IND + " * Returns d_next_slice_dump_iteration." + NL
            			+ IND + " */" + NL
            			+ IND + "vector<int> getNextSliceDumpIteration();" + NL + NL
            			+ IND + "/**" + NL
            			+ IND + " * Sets d_next_slice_dump_iteration." + NL
            			+ IND + " */" + NL
            			+ IND + "void setNextSliceDumpIteration(const vector<int> next_slice_dump_iteration);" + NL + NL
            			+ IND + "/**" + NL
            			+ IND + " * Returns d_next_sphere_dump_iteration." + NL
            			+ IND + " */" + NL
            			+ IND + "vector<int> getNextSphereDumpIteration();" + NL + NL
            			+ IND + "/**" + NL
            			+ IND + " * Sets d_next_sphere_dump_iteration." + NL
            			+ IND + " */" + NL
            			+ IND + "void setNextSphereDumpIteration(const vector<int> next_sphere_dump_iteration);" + NL + NL;
            	outputVariables = IND + "vector<int> d_next_slice_dump_iteration;" + NL
            			+ IND + "vector<int> d_next_sphere_dump_iteration;" + NL;
            }
            String integrationFunctions = "";
            String pointFunctions = "";
            String dumpFunctions = "";
            if (pi.isHasABMMeshFields() || pi.isHasMeshFields()) {
            	dumpFunctions = IND + "/**" + NL
            			+ IND + " * Returns d_next_mesh_dump_iteration." + NL
            			+ IND + " */" + NL
            			+ IND + "int getNextMeshDumpIteration();" + NL + NL
            			+ IND + "/**" + NL
            			+ IND + " * Sets d_next_mesh_dump_iteration." + NL
            			+ IND + " */" + NL
            			+ IND + "void setNextMeshDumpIteration(int next_mesh_dump_iteration);" + NL + NL;
            	integrationFunctions = IND + "/**" + NL
            			+ IND + " * Returns d_next_integration_dump_iteration." + NL
            			+ IND + " */" + NL
            			+ IND + "vector<int> getNextIntegrationDumpIteration();" + NL + NL
            			+ IND + "/**" + NL
            			+ IND + " * Sets d_next_integration_dump_iteration." + NL
            			+ IND + " */" + NL
            			+ IND + "void setNextIntegrationDumpIteration(const vector<int> next_integration_dump_iteration);" + NL + NL;
            	pointFunctions = IND + "/**" + NL
            			+ IND + " * Returns d_next_point_dump_iteration." + NL
            			+ IND + " */" + NL
            			+ IND + "vector<int> getNextPointDumpIteration();" + NL + NL
            			+ IND + "/**" + NL
            			+ IND + " * Sets d_next_point_dump_iteration." + NL
            			+ IND + " */" + NL
            			+ IND + "void setNextPointDumpIteration(const vector<int> next_point_dump_iteration);" + NL + NL;
            	outputVariables = outputVariables + IND + "int d_next_mesh_dump_iteration;" + NL
            			+ IND + "vector<int> d_next_integration_dump_iteration;" + NL
            			+ IND + "vector<int> d_next_point_dump_iteration;" + NL;
            }
            if (pi.isHasABMMeshlessFields() || pi.isHasParticleFields()) {
            	outputVariables = outputVariables + IND + "int d_next_particle_dump_iteration;" + NL;
            	dumpFunctions = dumpFunctions + IND + "/**" + NL
            			+ IND + " * Returns d_next_particle_dump_iteration." + NL
            			+ IND + " */" + NL
            			+ IND + "int getNextParticleDumpIteration();" + NL + NL
            			+ IND + "/**" + NL
            			+ IND + " * Sets d_next_particle_dump_iteration." + NL
            			+ IND + " */" + NL
            			+ IND + "void setNextParticleDumpIteration(const int next_particle_dump_iteration);" + NL + NL;
            }
            String analysisFunctions = "";
            if (pi.getVariableInfo().getOutputVarsAnalysis().size() > 0) {
            	analysisFunctions = IND + "/**" + NL
		             + IND + " * Returns d_next_analysis_dump_iteration." + NL
		             + IND + " */" + NL
		             + IND + "int getNextDumpAnalysisIteration();" + NL + NL
		             + IND + "/**" + NL
		             + IND + " * Sets d_next_analysis_dump_iteration" + NL
		             + IND + " */" + NL
		             + IND + "void setNextDumpAnalysisIteration(int next_analysis_dump_iteration);" + NL + NL;
            	outputVariables = outputVariables + IND + "int d_next_analysis_dump_iteration;" + NL;
            }
            mainRestartDataH = mainRestartDataH.replaceFirst("#DumpFunctions#", dumpFunctions);
        	mainRestartDataH = mainRestartDataH.replaceFirst("#AnalysisFunctions#", analysisFunctions);
            mainRestartDataH = mainRestartDataH.replaceFirst("#SliceFunctions#", sliceFunctions);
            mainRestartDataH = mainRestartDataH.replaceFirst("#IntegrationFunctions#", integrationFunctions + pointFunctions);
            mainRestartDataH = mainRestartDataH.replaceFirst("#outputVariables#", outputVariables);

            return mainRestartDataH;
        } 
        catch (Exception e) {
            throw new CGException(CGException.CG00X, e.getMessage());
        }
    }
    
    /**
     * Create interphase.c file.
     * @param pi                The problem info
     * @return                  The code
     * @throws CGException      CG00X External error
     */
    private String createInterphase(ProblemInfo pi) throws CGException {
        try {
            //Get the template
            String interphase = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/Interphase.C"), 0);
            VariableInfo vi = pi.getVariableInfo();
            ArrayList<String> coords = pi.getCoordinates();
            HashMap<String, String> fields = vi.getMeshFields();

            int memSize = (int) Math.pow(INTERPHASELENGTH, coords.size());
            //Set positions
            String initialization = "";
            String equal = "";
            String copy = "";
            int numOfDoubles = 0;
            String buffer = "";
            String toBuffer = "";
            String pack = "";
            String toFields = "";
            String unpack = "";
            String toDB = "";
            String fromDB = "";
            int counter = 0;
            for (int i = 0; i < coords.size(); i++) {
                String coord = coords.get(i);
                initialization = initialization + IND + "memset(position" + coord + ", 0, " + memSize + "*sizeof(double));" + NL
                        + IND + "memset(velocity" + coord + ", 0, " + memSize + "*sizeof(double));" + NL
                        + IND + "memset(normal" + coord + ", 0, " + memSize + "*sizeof(double));" + NL;
                equal = equal + IND + "memcpy(position" + coord + ",  data.position" + coord + ", " + memSize + "*sizeof(double));" + NL
                        + IND + "memcpy(velocity" + coord + ", data.velocity" + coord + ", " + memSize + "*sizeof(double));" + NL
                        + IND + "memcpy(normal" + coord + ", data.normal" + coord + ", " + memSize + "*sizeof(double));" + NL;
                copy = copy + IND + "memcpy(position" + coord + ",  src_item.position" + coord + ", " + memSize + "*sizeof(double));" + NL
                        + IND + "memcpy(velocity" + coord + ", src_item.velocity" + coord + ", " + memSize + "*sizeof(double));" + NL
                        + IND + "memcpy(normal" + coord + ", src_item.normal" + coord + ", " + memSize + "*sizeof(double));" + NL;
                numOfDoubles = THREE + numOfDoubles;
                buffer = buffer + IND + "double dbuffer" + (counter + 1) + "[" + memSize + "];" + NL
                        + IND + "double dbuffer" + (counter + 2) + "[" + memSize + "];" + NL
                        + IND + "double dbuffer" + (counter + THREE) + "[" + memSize + "];" + NL;
                toBuffer = toBuffer + IND + "memcpy(dbuffer" + (counter + 1) + ", position" + coord + ", " + memSize + " * sizeof(double));" + NL
                        + IND + "memcpy(dbuffer" + (counter + 2) + ", velocity" + coord + ", " + memSize + " * sizeof(double));" + NL
                        + IND + "memcpy(dbuffer" + (counter + THREE) + ", normal" + coord + ", " + memSize + " * sizeof(double));" + NL;
                pack = pack + IND + "stream.pack(dbuffer" + (counter + 1) + ", " + memSize + ");" + NL
                        + IND + "stream.pack(dbuffer" + (counter + 2) + ", " + memSize + ");" + NL
                        + IND + "stream.pack(dbuffer" + (counter + THREE) + ", " + memSize + ");" + NL;
                unpack = unpack + IND + "stream.unpack(dbuffer" + (counter + 1) + ", " + memSize + ");" + NL
                        + IND + "stream.unpack(dbuffer" + (counter + 2) + ", " + memSize + ");" + NL
                        + IND + "stream.unpack(dbuffer" + (counter + THREE) + ", " + memSize + ");" + NL;
                toFields = toFields + IND + "memcpy(position" + coord + ", dbuffer" + (counter + 1) + ", " + memSize + " * sizeof(double));" + NL
                        + IND + "memcpy(velocity" + coord + ", dbuffer" + (counter + 2) + ", " + memSize + " * sizeof(double));" + NL
                        + IND + "memcpy(normal" + coord + ", dbuffer" + (counter + THREE) + ", " + memSize + " * sizeof(double));" + NL;
                toDB = toDB + IND + "database->putDoubleArray(\"position" + coord + "\", dbuffer" + (counter + 1) + ", " + memSize + ");" + NL
                        + IND + "database->putDoubleArray(\"velocity" + coord + "\", dbuffer" + (counter + 2) + ", " + memSize + ");" + NL
                        + IND + "database->putDoubleArray(\"normal" + coord + "\", dbuffer" + (counter + THREE) + ", " + memSize + ");" + NL;
                fromDB = fromDB + IND + "database->getDoubleArray(\"position" + coord + "\", dbuffer" + (counter + 1) + ", " + memSize + ");" + NL
                        + IND + "database->getDoubleArray(\"velocity" + coord + "\", dbuffer" + (counter + 2) + ", " + memSize + ");" + NL
                        + IND + "database->getDoubleArray(\"normal" + coord + "\", dbuffer" + (counter + THREE) + ", " + memSize + ");" + NL;
                counter = counter + THREE;
            }
            initialization = initialization + IND + "memset(seg, 0, " + memSize + "*sizeof(int));" + NL
                    + IND + "memset(int_seg, 0, " + memSize + "*sizeof(int));" + NL;
            equal = equal + IND + "memcpy(seg, data.seg, " + memSize + "*sizeof(int));" + NL
                    + IND + "memcpy(int_seg, data.int_seg, " + memSize + "*sizeof(int));" + NL;
            copy = copy + IND + "memcpy(seg, src_item.seg, " + memSize + "*sizeof(int));" + NL
                    + IND + "memcpy(int_seg, src_item.int_seg, " + memSize + "*sizeof(int));" + NL;
            buffer = buffer + IND + "int dbuffer" + (counter + 1) + "[" + memSize + "];" + NL
                    + IND + "int dbuffer" + (counter + 2) + "[" + memSize + "];" + NL;
            toBuffer = toBuffer + IND + "memcpy(dbuffer" + (counter + 1) + ", seg, " + memSize + " * sizeof(int));" + NL
                    + IND + "memcpy(dbuffer" + (counter + 2) + ", int_seg, " + memSize + " * sizeof(int));" + NL;
            pack = pack + IND + "stream.pack(dbuffer" + (counter + 1) + ", " + memSize + ");" + NL
                    + IND + "stream.pack(dbuffer" + (counter + 2) + ", " + memSize + ");" + NL;
            unpack = unpack + IND + "stream.unpack(dbuffer" + (counter + 1) + ", " + memSize + ");" + NL
                    + IND + "stream.unpack(dbuffer" + (counter + 2) + ", " + memSize + ");" + NL;
            toFields = toFields + IND + "memcpy(seg, dbuffer" + (counter + 1) + ", " + memSize + " * sizeof(int));" + NL
                    + IND + "memcpy(int_seg, dbuffer" + (counter + 2) + ", " + memSize + " * sizeof(int));" + NL;
            toDB = toDB + IND + "database->putIntegerArray(\"seg\", dbuffer" + (counter + 1) + ", " + memSize + ");" + NL
                    + IND + "database->putIntegerArray(\"int_seg\", dbuffer" + (counter + 2) + ", " + memSize + ");" + NL;
            fromDB = fromDB + IND + "database->getIntegerArray(\"seg\", dbuffer" + (counter + 1) + ", " + memSize + ");" + NL
                    + IND + "database->getIntegerArray(\"int_seg\", dbuffer" + (counter + 2) + ", " + memSize + ");" + NL;
            counter = counter + 2;
            Iterator<String> fieldIt = fields.keySet().iterator();
            while (fieldIt.hasNext()) {
                String field = fieldIt.next();
                initialization = initialization + IND + "memset(" + field + ", 0, " + memSize + "*sizeof(double));" + NL;
                equal = equal + IND + "memcpy(" + field + ", data." + field + ", " + memSize + "*sizeof(double));" + NL;
                copy = copy + IND + "memcpy(" + field + ", src_item." + field + ", " + memSize + "*sizeof(double));" + NL;
                buffer = buffer + IND + "double dbuffer" + (counter + 1) + "[" + memSize + "];" + NL;
                toBuffer = toBuffer + IND + "memcpy(dbuffer" + (counter + 1) + ", " + field + ", " + memSize + " * sizeof(double));" + NL;
                pack = pack + IND + "stream.pack(dbuffer" + (counter + 1) + ", " + memSize + ");" + NL;
                unpack = unpack + IND + "stream.unpack(dbuffer" + (counter + 1) + ", " + memSize + ");" + NL;
                toFields = toFields + IND + "memcpy(" + field + ", dbuffer" + (counter + 1) + ", " + memSize + " * sizeof(double));" + NL;
                toDB = toDB + IND + "database->putDoubleArray(\"" + field + "\", dbuffer" + (counter + 1) + ", " + memSize + ");" + NL;
                fromDB = fromDB + IND + "database->getDoubleArray(\"" + field + "\", dbuffer" + (counter + 1) + ", " + memSize + ");" + NL;
                numOfDoubles++;
                counter++;
            }
            String packStream = buffer + NL + toBuffer + NL + pack;
            String unpackStream = buffer + NL + unpack + NL + toFields;
            String putToDatabase = buffer + NL + toBuffer + NL + toDB;
            String getFromDatabase = buffer + NL + fromDB + NL + toFields;
            
            interphase = interphase.replaceFirst("#Initialization#", initialization);
            interphase = interphase.replaceFirst("#Equal#", equal);
            interphase = interphase.replaceFirst("#Copy#", copy);
            interphase = interphase.replaceFirst("#NUMOFDOUBLES#", String.valueOf(numOfDoubles));
            interphase = interphase.replaceAll("#SIZE#", String.valueOf(memSize));
            interphase = interphase.replaceAll("#packStream#", packStream);
            interphase = interphase.replaceAll("#unpackStream#", unpackStream);
            interphase = interphase.replaceAll("#putToDatabase#", putToDatabase);
            interphase = interphase.replaceAll("#getFromDatabase#", getFromDatabase);
            return interphase;
        } 
        catch (Exception e) {
            throw new CGException(CGException.CG00X, e.getMessage());
        }
    }
    
    /**
     * Create interphase.h file.
     * @param pi                The problem info
     * @return                  The code
     * @throws CGException      CG00X External error
     */
    private String createInterphaseH(ProblemInfo pi) throws CGException {
        try {
            //Get the template
            String interphaseH = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/Interphase.h"), 0);
            VariableInfo vi = pi.getVariableInfo();
            ArrayList<String> coords = pi.getCoordinates();
            HashMap<String, String> fields = vi.getMeshFields();

            String array = "";
            for (int i = 0; i < coords.size(); i++) {
                array = array + "[" + INTERPHASELENGTH + "]";
            }
            
            String variables = "";
            for (int i = 0; i < coords.size(); i++) {
                String coord = coords.get(i);
                variables = variables + IND + "double position" + coord + array + ";" + NL
                        + IND + "double velocity" + coord + array + ";" + NL
                        + IND + "double normal" + coord + array + ";" + NL;
            }
            
            Iterator<String> fieldIt = fields.keySet().iterator();
            while (fieldIt.hasNext()) {
                String field = fieldIt.next();
                variables = variables + IND + "double " + field + array + ";" + NL;
            }
            
            return interphaseH.replaceFirst("#variables#", variables);
            
        } 
        catch (Exception e) {
            throw new CGException(CGException.CG00X, e.getMessage());
        }
    }
    
    /**
     * Create TimeInterpolator.C file.
     * @param timeInterpolationInfo	Time interpolation information
     * @param pi 					The problem information
     * @return                  	The code
     * @throws CGException      	CG00X External error
     */
    private String createTimeInterpolator(TimeInterpolationInfo timeInterpolationInfo, ProblemInfo pi) throws CGException {
        //Get the template
        String timeInterpolator = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/TimeInterpolator.C"), 0);
        
        String interpolation = "";
        String timeInterpolate = "";
        for (TimeInterpolator ti: timeInterpolationInfo.getTimeInterpolators().values()) {
            String lasts = "";
            String initLoop = "";
            String initLoopPart = "";
            String initLoopPart2 = "";
            String endLoop = "";
            String endLoopPart = "";
            String currentIND = IND + IND + IND;
            String currentINDPart = IND + IND;
            String patchCondition = "";
            String posCondition = "";
            String index = "";
            String posIndex = "";
            String indexDilo = "";
            String indexIlo = "";
            String posUseParticles = "";
            String positionAssignment = "";
            String newPos = "";
            String posIndexDec = "";
            for (int i = pi.getCoordinates().size() - 1; i >= 0; i--) {
                String coord = pi.getCoordinates().get(i);
                if (i < pi.getCoordinates().size() - 1) {
                	lasts = IND + IND + IND + "int " + coord + "last = ihi(" + i + ")-ilo(" + i + ") + 2;" + NL
            			+ IND + IND + IND + "int d" + coord + "last = dihi(" + i + ")-dilo(" + i + ") + 2;" + NL + lasts;
                }
                patchCondition = coord + " >= dilo[" + i + "] && " + coord + " <= dihi[" + i + "] && " + patchCondition;
                posCondition = coord + coord + " >= dilo[" + i + "] && " + coord + coord + " <= dihi[" + i + "] && " + posCondition;
                index = ", " + coord + index;
                posIndex = ", " + coord + coord + posIndex;
                indexDilo = ", " + coord + " - dilo[" + i + "]" + indexDilo;
                indexIlo = ", " + coord + " - ilo[" + i + "]" + indexIlo;
                initLoop = initLoop + currentIND + "for(int " + coord + " = ifirstf[" + i + "] - ghosts; " + coord + " <= ilastf[" + i + "] + ghosts; " + coord + "++) {" + NL;
                initLoopPart = initLoopPart + currentINDPart + "for(int " + coord + " = dilo[" + i + "]; " + coord + " <= dihi[" + i + "]; " + coord + "++) {" + NL;
                initLoopPart2 = initLoopPart2 + currentINDPart + "for(int " + coord + " = ifirstf[" + i + "] - ghosts; " + coord + " <= ilastf[" + i + "] + ghosts; " + coord + "++) {" + NL;
                endLoop = currentIND + "}" + NL + endLoop;
                endLoopPart = currentINDPart + "}" + NL + endLoopPart;
                currentIND = currentIND + IND;
                currentINDPart = currentINDPart + IND;
            }
            for (int i = 0; i < pi.getCoordinates().size(); i++) {
                String coord = pi.getCoordinates().get(i);
                //Particles
                String contCoord = pi.getContCoordinates().get(i);
            	ArrayList<String> refineTimeParticles = ti.getPositionRefineTimeParticles(contCoord);
            	String paramsCallParticles = "";
            	for (int j = 0; j < refineTimeParticles.size(); j++) {
            		String fieldVar = refineTimeParticles.get(j);
                	paramsCallParticles = paramsCallParticles + ", particle->" + fieldVar;
            	}
                posUseParticles = posUseParticles + currentINDPart + IND + IND + "interpolation(simPlat_dt" + paramsCallParticles + ");" + NL;
                positionAssignment = positionAssignment + currentINDPart + IND + IND + "pos" + contCoord + " = particle->position" + contCoord + "_p;" + NL;
                newPos = newPos + currentINDPart + IND + IND + "double newPos" + contCoord + " = (pos" + contCoord + " - " + contCoord + "Glower)/d" + contCoord + ";" + NL;
                posIndexDec = posIndexDec + currentINDPart + IND + IND + "int " + coord + coord + " = floor(newPos" + contCoord + "+ 1.0E-10);" + NL;
            }
            patchCondition = patchCondition.substring(0, patchCondition.lastIndexOf(" &&"));
            posCondition = posCondition.substring(0, posCondition.lastIndexOf(" &&"));
            index = index.substring(2);
            posIndex = posIndex.substring(2);
        	
        	String varParamsFunc = "";
        	String varParams = "";
        	String varDecls = "";
        	String calculateTimes = "";
        	String varVals = "";
        	String assignment = "";
        	String varsInit = "";
        	String varUsage = "";
        	String vector = "vector2D";
        	String paramsCall = "";
        	String calculateTimesParticles = IND + IND + "current_time = tbox::MathUtilities<double>::Min(current_time, part_tmp->time_p);" + NL
        			+ IND + IND + "new_time = tbox::MathUtilities<double>::Max(new_time, part_tmp->time_p);" + NL;
        	if (pi.getDimensions() == 3) {
        		vector = "vector3D";
        	}
            ArrayList<String> vars = ti.getAlgorithmGenericVars();
            for (int i = 0; i < vars.size(); i++) {
            	String varName = vars.get(i);
            	varParamsFunc = varParamsFunc + IND + IND + "double& " + varName + "," + NL;
            	varParams = varParams + IND + "const hier::PatchData& src_data_" + varName + "," + NL;
            	varDecls = varDecls + IND + IND + "const pdat::NodeData<double>* " + varName + "_dat = CPP_CAST<const pdat::NodeData<double> *>(&src_data_" + varName + ");" + NL;
            	calculateTimes = calculateTimes + IND + IND + "current_time = tbox::MathUtilities<double>::Min(current_time, " + varName + "_dat->getTime());" + NL
            			+ IND + IND + "new_time = tbox::MathUtilities<double>::Max(new_time, " + varName + "_dat->getTime());" + NL;
            	varVals = varVals + IND + IND + IND + "const double* field_" + varName + " = " + varName + "_dat->getPointer(d);" + NL;
            	
            	assignment = assignment + currentIND + IND + "if (interp_step == " + i + ") {" + NL
            			+ currentIND + IND + IND + "d" + vector + "(field" + indexDilo + ") = " + varName + ";" + NL
            			+ currentIND + IND + "}" + NL;
            	varsInit = varsInit + currentIND + IND + "double " + varName + " = " + vector + "(field_" + varName + indexIlo + ");" + NL;
            	paramsCall = paramsCall + ", " + varName;
            	

            }
            varParamsFunc = varParamsFunc.substring(0, varParamsFunc.lastIndexOf(","));
            varParams = varParams.substring(0, varParams.lastIndexOf(","));
            for (int i = 0; i < ti.getVarAtStep().size(); i++) {
            	//Particles
            	String variable = SAMRAIUtils.xmlTransform(ti.getVarAtStep().get(i), null);
            	variable = variable.substring(0, variable.indexOf("("));
            	calculateTimesParticles = calculateTimesParticles + IND + IND + "current_time = tbox::MathUtilities<double>::Min(current_time, part_tmp->time" + variable + ");" + NL
            			+ IND + IND + "new_time = tbox::MathUtilities<double>::Max(new_time, part_tmp->time" + variable + ");" + NL;
            	positionAssignment = positionAssignment + IND + IND + IND + IND + IND + IND + "if (interp_step == " + (i + 1) + ") {" + NL;
                for (int j = 0; j < pi.getContCoordinates().size(); j++) {
                    String coord = pi.getContCoordinates().get(j);
             		Element varAtStep = (Element) ti.getVarAtStep().get(i).cloneNode(true);
    				NodeList fieldTags = CodeGeneratorUtils.find(varAtStep, ".//sml:field");
    				Text fieldValue = varAtStep.getOwnerDocument().createTextNode("position" + coord);
    				for (int k = fieldTags.getLength() - 1; k >= 0; k--) {
    					fieldTags.item(k).getParentNode().replaceChild(fieldValue, fieldTags.item(k));
    				}
    				String var = SAMRAIUtils.xmlTransform(varAtStep, null);
    				var = var.substring(0, var.indexOf("("));
                    positionAssignment = positionAssignment + IND + IND + IND + IND + IND + IND + IND + "pos" + coord + " = particle->" + var +";" + NL;
                }
                positionAssignment = positionAssignment + IND + IND + IND + IND + IND + IND + "}" + NL;
            }
            
            if (ti.getVarAtStep().size() == 0) {
            	calculateTimesParticles = calculateTimesParticles + IND + IND + "current_time = tbox::MathUtilities<double>::Min(current_time, part_tmp->time);" + NL
            			+ IND + IND + "new_time = tbox::MathUtilities<double>::Max(new_time, part_tmp->time);" + NL;
            }
            
            varUsage = varsInit
            		+ currentIND + IND + "interpolation(simPlat_dt" + paramsCall + ");" + NL
            		+ assignment;
            
            Element customAlgorithm = ti.getAlgorithm();
            String algorithm = "";
            if (customAlgorithm != null) {
            	//Instructions
                String[][] params = new String [THREE][2];
                params[0][0] = "condition";
                params[0][1] = "false";
                params[1][0] = "indent";
                params[1][1] = "1";
                params[2][0] =  "dimensions";
                params[2][1] =  String.valueOf(pi.getDimensions());

            	algorithm = algorithm + SAMRAIUtils.getPDEVariableDeclaration(customAlgorithm, pi, 1, 
            			ti.getAlgorithmGenericVars(), "patch->", false, true) + NL;
            	Element instSet = (Element) SAMRAIUtils.preProcessPDEBlock(customAlgorithm.cloneNode(true),
                        pi, true, false);
                NodeList instructionList = instSet.getChildNodes();
                for (int j = 0; j < instructionList.getLength(); j++) {
                	algorithm = algorithm + SAMRAIUtils.xmlTransform(instructionList.item(j), params);
                }
            }
            //Default linear interpolation
            else {
            	algorithm = IND + "double tt = (interp_time - current_time)/(new_time - current_time);" + NL
            			+ IND + "unp1 = (1 - tt) * un + tt * unp1;" + NL;
            }
            
        	interpolation = interpolation + "inline void TimeInterpolator::interpolation(" + NL
        			+ IND + "double simPlat_dt," + NL
        			+ varParamsFunc + ") const {" + NL
        			+ algorithm
        			+ "}" + NL;
        	
            String particle_code = "";
            if (pi.isHasParticleFields()) {
            	String indexFirst = "";
                for (int i = 0; i < pi.getContCoordinates().size(); i++) {
                	indexFirst = indexFirst + "ifirstf[" + i + "], ";
                }
                indexFirst = indexFirst.substring(0, indexFirst.lastIndexOf(","));
            	for (Entry<String, ArrayList<String>> entry : pi.getVariableInfo().getParticleFields().entrySet()) {
            		String speciesName = entry.getKey();
            		String varUseParticles = "";
                    Iterator<String> fieldIt = pi.getFieldTimeLevels().keySet().iterator();
                    while (fieldIt.hasNext()) {
                        String field = fieldIt.next();
                        if (pi.getVariableInfo().getParticleFields().get(speciesName).contains(field) && !CodeGeneratorUtils.isAuxiliaryField(pi, SAMRAIUtils.variableNormalize(field))) {
            	        	ArrayList<String> refineTimeParticles = timeInterpolationInfo.getFieldRefineTimeParticles(field);
            	        	String paramsCallParticles = "";
            	        	for (int i = 0; i < refineTimeParticles.size(); i++) {
            	        		String fieldVar = refineTimeParticles.get(i);
            	            	paramsCallParticles = paramsCallParticles + ", particle->" + fieldVar;
            	        	}
            	            varUseParticles = varUseParticles + currentINDPart + IND + IND + "interpolation(simPlat_dt" + paramsCallParticles + ");" + NL;
                        }
                    }
            		particle_code = particle_code + IND + "if (d_discType.compare(\"particle_" + speciesName + "\") == 0) {" + NL
            				+ IND + IND + "const pdat::IndexData<Particles<Particle_" + speciesName + ">, pdat::CellGeometry>* un_dat = CPP_CAST<const pdat::IndexData<Particles<Particle_" + speciesName + ">, pdat::CellGeometry> *>(&src_data_un);" + NL + NL
            				+ IND + IND + "pdat::IndexData<Particles<Particle_" + speciesName + ">, pdat::CellGeometry>* dst_dat = CPP_CAST<pdat::IndexData<Particles<Particle_" + speciesName + ">, pdat::CellGeometry> *>(&dst_data);" + NL
            				+ IND + IND + "TBOX_ASSERT(dst_dat != 0);" + NL
            				+ IND + IND + "TBOX_ASSERT((where * dst_dat->getGhostBox()).isSpatiallyEqual(where));" + NL + NL
            				+ IND + IND + "const hier::Index ilo = un_dat->getGhostBox().lower();" + NL
            				+ IND + IND + "const hier::Index ihi = un_dat->getGhostBox().upper();" + NL + NL
            				+ IND + IND + "const hier::Index dilo = dst_dat->getGhostBox().lower();" + NL
            				+ IND + IND + "const hier::Index dihi = dst_dat->getGhostBox().upper();" + NL + NL
            				+ IND + IND + "const hier::Index ifirstf = where.lower();" + NL
            				+ IND + IND + "const hier::Index ilastf = where.upper();" + NL + NL
            				+ IND + IND + "int ghosts = dst_data.getGhostCellWidth()[0];" + NL + NL
            				+ IND + IND + "new_time = 0;" + NL
            				+ IND + IND + "hier::Index idx_tmp(" + indexFirst + ");" + NL + NL
            				+ IND + IND + "Particles<Particle_" + speciesName + ">* part_tmp = un_dat->getItem(idx_tmp);" + NL
	            			+ calculateTimesParticles
	            			+ IND + IND + "const double dt = new_time - current_time;" + NL + NL
	            			+ IND + IND + "TBOX_ASSERT(dt > 0);" + NL + NL
	            			+ IND + IND + "interp_time = dst_dat->getTime();" + NL + NL
	            			+ IND + IND + "double posx, posy, posz;" + NL + NL
	            			+ IND + IND + "TBOX_ASSERT((current_time < interp_time || tbox::MathUtilities<double>::equalEps(current_time, interp_time)) &&" + NL
	            			+ IND + IND + IND + "(interp_time < new_time || tbox::MathUtilities<double>::equalEps(interp_time, new_time)));" + NL + NL      
	            			+ initLoopPart
	            			+ currentINDPart + "hier::Index idx(" + index + ");" + NL
	            			+ currentINDPart + "Particles<Particle_" + speciesName + ">* destPart = new Particles<Particle_" + speciesName + ">();" + NL
	            			+ currentINDPart + "dst_dat->replaceAddItemPointer(idx, destPart);" + NL
	            			+ endLoopPart
	            			+ initLoopPart2
	            			+ currentINDPart + "if (" + patchCondition + ") {" + NL
	            			+ currentINDPart + IND + "double simPlat_dt = dt;" + NL
	            			+ currentINDPart + IND + "hier::Index idx(" + index + ");" + NL
	            			+ currentINDPart + IND + "Particles<Particle_" + speciesName + ">* part = un_dat->getItem(idx);" + NL
	            			+ currentINDPart + IND + "for (int pit = part->getNumberOfParticles() - 1; pit >= 0; pit--) {" + NL
	            			+ currentINDPart + IND + IND + "Particle_" + speciesName + "* particle_src = part->getParticle(pit);" + NL
	            			+ currentINDPart + IND + IND + "Particle_" + speciesName + "* particle = new Particle_" + speciesName + "(*particle_src, false);" + NL
	            			+ varUseParticles
	            			+ posUseParticles
	            			+ positionAssignment
	            			+ newPos
	            			+ posIndexDec
	            			+ currentINDPart + IND + IND + "if (" + posCondition + ") {" + NL
	            			+ currentINDPart + IND + IND + IND + "hier::Index idx2(" + posIndex + ");" + NL
	            			+ currentINDPart + IND + IND + IND + "Particles<Particle_" + speciesName + ">* destPart = dst_dat->getItem(idx2);" + NL
	            			+ currentINDPart + IND + IND + IND + "destPart->addParticle(*particle);" + NL
	            			+ currentINDPart + IND + IND + "}" + NL
	            			+ currentINDPart + IND + IND + "delete particle;" + NL
	            			+ currentINDPart + IND + "}" + NL
	            			+ currentINDPart + "}" + NL
	            			+ endLoopPart
	            			+ IND + "}" + NL;
            	}
            }
        	
        	timeInterpolate = timeInterpolate + "void TimeInterpolator::timeInterpolate(" + NL
        			+ IND + "hier::PatchData& dst_data," + NL
        			+ IND + "const hier::Box& where," + NL
        			+ IND + "const hier::BoxOverlap& overlap," + NL
        			+ varParams + ") const" + NL
        			+ "{" + NL
        			+ IND + "t_interpolate->start();" + NL
        			+ IND + "const tbox::Dimension& dim(where.getDim());" + NL
        			+ IND + "current_time = tbox::MathUtilities<double>::getMax();" + NL + NL
        			+ IND + "if (d_discType.compare(\"mesh\") == 0) {" + NL
        			+ varDecls
        			+ IND + IND + "pdat::NodeData<double>* dst_dat = CPP_CAST<pdat::NodeData<double> *>(&dst_data);" + NL + NL
        			+ IND + IND + "TBOX_ASSERT(dst_dat != 0);" + NL
        			+ IND + IND + "TBOX_ASSERT((where * dst_dat->getGhostBox()).isSpatiallyEqual(where));" + NL + NL
        			+ IND + IND + "const hier::Index ilo = " + vars.get(0) + "_dat->getGhostBox().lower();" + NL
            		+ IND + IND + "const hier::Index ihi = " + vars.get(0) + "_dat->getGhostBox().upper();" + NL
        			+ IND + IND + "const hier::Index dilo = dst_dat->getGhostBox().lower();" + NL
        			+ IND + IND + "const hier::Index dihi = dst_dat->getGhostBox().upper();" + NL + NL
        			+ IND + IND + "const hier::Index ifirstf = where.lower();" + NL
        			+ IND + IND + "const hier::Index ilastf = where.upper();" + NL + NL
        			+ IND + IND + "int ghosts = dst_data.getGhostCellWidth()[0];" + NL + NL
        			+ IND + IND + "new_time = 0;" + NL
        			+ calculateTimes
        			+ IND + IND + "const double dt = new_time - current_time;" + NL
        			+ IND + IND + "interp_time = dst_dat->getTime();" + NL + NL
        			+ IND + IND + "TBOX_ASSERT(dt > 0);" + NL
        			+ IND + IND + "TBOX_ASSERT((current_time < interp_time ||" + NL
        			+ IND + IND + IND + "tbox::MathUtilities<double>::equalEps(current_time, interp_time)) &&" + NL
        			+ IND + IND + IND + "(interp_time < new_time ||" + NL
        			+ IND + IND + IND + "tbox::MathUtilities<double>::equalEps(interp_time, new_time)));" + NL
        			+ IND + IND + "for (int d = 0; d < dst_dat->getDepth(); ++d) {" + NL
        			+ IND + IND + IND + "double* field = dst_dat->getPointer(d);" + NL
        			+ varVals + NL
        			+ lasts + NL
        			+ initLoop
        			+ currentIND + "if (" + patchCondition + ") {" + NL
        			+ currentIND + IND + "double simPlat_dt = dt;" + NL
        			+ varUsage
        			+ currentIND + "}" + NL
        			+ endLoop
        			+ IND + IND + "}" + NL
        			+ IND + "}" + NL
        			+ particle_code
        			+ IND + "t_interpolate->stop();" + NL
        			+ "}" + NL;
        }

        String gridLimitAssing = "";
        if (pi.getDimensions() == 3) {
        	gridLimitAssing = IND + "zGlower = grid_geom->getXLower()[2];" + NL
        			+ IND + "zGupper = grid_geom->getXUpper()[2];" + NL;
        }

        String includeParticles = "";
        if (pi.isHasParticleFields()) {
        	includeParticles = "#include \"Particles.h\"" + NL;
        	for (Entry<String, ArrayList<String>> entry : pi.getVariableInfo().getParticleFields().entrySet()) {
        		String speciesName = entry.getKey();
        		includeParticles = includeParticles + "#include \"Particle_" + speciesName + ".h\"" + NL;
        	}
        }
        
        timeInterpolator = timeInterpolator.replace("#TIMEINTERPOLATE#", timeInterpolate);
        timeInterpolator = timeInterpolator.replace("#INTERPOLATION#", interpolation);
        timeInterpolator = timeInterpolator.replace("#includes#", includeParticles);
        timeInterpolator = timeInterpolator.replace("#gridLimitAssing#", gridLimitAssing);
        
        
        return timeInterpolator;
    }
      
    /**
     * Create TimeInterpolator.h file.
     * @param pi 				The problem information
     * @return                  The code
     * @throws CGException      CG00X External error
     */
    private String createTimeInterpolatorH(ProblemInfo pi) throws CGException {
    	TimeInterpolationInfo timeInterpolationInfo = pi.getTimeInterpolationInfo();
    	//Get the template
        String timeInterpolator = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/TimeInterpolator.h"), 0);
        
        String timeInterpolate = "";
        String interpolation = "";
        for (TimeInterpolator ti: timeInterpolationInfo.getTimeInterpolators().values()) {    
	        String varParams = "";
	        String varParamsFunc = "";
	        ArrayList<String> substepGenericVars = ti.getAlgorithmGenericVars();
	        for (int i = 0; i < substepGenericVars.size(); i++) {
	        	String varName = substepGenericVars.get(i);
	        	varParams = varParams + IND + IND + "const hier::PatchData& src_data_" + varName + "," + NL;
	        	varParamsFunc = varParamsFunc + IND + IND + "double& " + varName + "," + NL;
	        }
	        varParams = varParams.substring(0, varParams.lastIndexOf(","));
	        varParamsFunc = varParamsFunc.substring(0, varParamsFunc.lastIndexOf(","));
	        
	        timeInterpolate = timeInterpolate + IND + "void timeInterpolate(" + NL
	        		+ IND + IND + "hier::PatchData& dst_data," + NL
	        		+ IND + IND + "const hier::Box& where," + NL
	        		+ IND + IND + "const hier::BoxOverlap& overlap," + NL
	        		+ varParams + ") const;" + NL;
	        interpolation = interpolation + IND + "void interpolation(" + NL
	        		+ IND + IND + "double simPlat_dt," + NL
	        		+ varParamsFunc + ") const;" + NL;
        }
        
        timeInterpolator = timeInterpolator.replace("#TIMEINTERPOLATE#", timeInterpolate);
        timeInterpolator = timeInterpolator.replace("#INTERPOLATION#", interpolation);
        
        String dx = "";
        if (pi.getDimensions() > 2) {
        	dx = dx + IND + "dz = tmpdx[2];" + NL;
        }
        timeInterpolator = timeInterpolator.replace("#DX_ASSIGNMENT#", dx);
        return timeInterpolator;
    }
    
    /**
     * Create RefineClasses.C file.
     * @param timeInterpolationInfo	Time interpolation information
     * @return                  The code
     * @throws CGException      CG00X External error
     */
    private String createRefineClasses(TimeInterpolationInfo timeInterpolationInfo) throws CGException {
        //Get the template
        String refineClasses = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/RefineClasses.C"), 0);
        
        String varComp = "";
        Set<String> alreadySet = new HashSet<String>();
        for (TimeInterpolator ti: timeInterpolationInfo.getTimeInterpolators().values()) {
	        ArrayList<String> vars = ti.getAlgorithmGenericVars();
	        for (int i = 0; i < vars.size(); i++) {
	        	String varName = vars.get(i);
	        	if (!alreadySet.contains(varName)) {
	        		alreadySet.add(varName);
	        		varComp = varComp + IND + IND + "equivalent &= patchDataMatch(data1.d_src_t" + varName + ", data2.d_src_t" + varName + ", pd);" + NL;
	        	}
	        }
        }
      
        refineClasses = refineClasses.replace("#VARIABLES_COMP#", varComp);
        
        return refineClasses;
    }
    
    /**
     * Create RefineClasses.h file.
     * @param timeInterpolationInfo	Time interpolation information
     * @return                  The code
     * @throws CGException      CG00X External error
     */
    private String createRefineClassesH(TimeInterpolationInfo timeInterpolationInfo) throws CGException {
        //Get the template
        String refineClasses = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/RefineClasses.h"), 0);
        
        String variables = "";
        Set<String> alreadySet = new HashSet<String>();
        for (TimeInterpolator ti: timeInterpolationInfo.getTimeInterpolators().values()) {
	        ArrayList<String> vars = ti.getAlgorithmGenericVars();
	        for (int i = 0; i < vars.size(); i++) {
	        	String varName = vars.get(i);
	        	if (!alreadySet.contains(varName)) {
	        		alreadySet.add(varName);
	        		variables = variables + IND + "int d_src_t" + varName + ";" + NL;
	        	}
	        }
        }
              
        refineClasses = refineClasses.replace("#VARIABLES#", variables);
        
        return refineClasses;
    }
    
    /**
     * Create RefineTimeTransaction.C file.
     * @param timeInterpolationInfo	Time interpolation information
     * @return                  The code
     * @throws CGException      CG00X External error
     */
    private String createRefineTimeTransaction(TimeInterpolationInfo timeInterpolationInfo) throws CGException {
        //Get the template
        String refineTimeTransaction = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/RefineTimeTransaction.C"), 0);
      
        boolean multipleInterpolators = timeInterpolationInfo.getTimeInterpolators().size() > 1;
        String currentIND = IND;
        String interpolate1 = "";
        String interpolate2 = "";
        String interpolate3 = "";
        String functions = "";
        for (TimeInterpolator ti: timeInterpolationInfo.getTimeInterpolators().values()) {
        	if (multipleInterpolators) {
        		interpolate1 = interpolate1 + currentIND + "if (d_refine_data[d_item_id]->d_time_schema == " + ti.getId() + ") {" + NL;
        		interpolate2 = interpolate2 + currentIND + "if (d_refine_data[d_item_id]->d_time_schema == " + ti.getId() + ") {" + NL;
        		interpolate3 = interpolate3 + currentIND + "if (d_refine_data[d_item_id]->d_time_schema == " + ti.getId() + ") {" + NL;
        		currentIND = currentIND + IND;
        	}
        	
        	String interpParams = "";
        	String params = "";
        	String call = "";
            ArrayList<String> vars = ti.getAlgorithmGenericVars();
            for (int i = 0; i < vars.size(); i++) {
            	String varName = vars.get(i);
            	interpParams = interpParams + currentIND + IND + "d_src_patch->getPatchData(d_refine_data[d_item_id]->d_src_t" + varName + ")," + NL;
            	params = params + IND + "const std::shared_ptr<hier::PatchData>& pd_" + varName + "," + NL;
            	call = call + "*pd_" + varName + ", ";
            }
            interpParams = interpParams.substring(0, interpParams.lastIndexOf(","));
            params = params.substring(0, params.lastIndexOf(","));
            call = call.substring(0, call.lastIndexOf(","));
        	
        	interpolate1 = interpolate1 + currentIND +  "timeInterpolate(" + NL
        			+ currentIND + IND + "*temporary_patch_data," + NL
        			+ currentIND + IND + "*d_overlap," + NL
        			+ interpParams + ");" + NL;
        	interpolate2 = interpolate2 + currentIND +  "timeInterpolate(" + NL
        			+ currentIND + IND + "scratch_data, *d_overlap," + NL
        			+ interpParams + ");" + NL;
        	interpolate3 = interpolate3 + currentIND +  "timeInterpolate(" + NL
        			+ currentIND + IND + "*temp," + NL
        			+ currentIND + IND + "*d_overlap," + NL
        			+ interpParams + ");" + NL;
        	
        	if (multipleInterpolators) {
        		currentIND = currentIND.substring(1);
        		interpolate1 = interpolate1 + currentIND + "}" + NL;
        		interpolate2 = interpolate2 + currentIND + "}" + NL;
        		interpolate3 = interpolate3 + currentIND + "}" + NL;
        	}
        	
        	functions = functions + "void" + NL
        			+ "RefineTimeTransaction::timeInterpolate(" + NL
        			+ IND + "hier::PatchData& pd_dst," + NL
        			+ IND + "const hier::BoxOverlap& overlap," + NL
        			+ params + ")" + NL
        			+ "{" + NL
        			+ IND + "d_refine_data[d_item_id]->d_optime->timeInterpolate(pd_dst, d_box, overlap, " + call + ");" + NL
        			+ "}" + NL + NL;
        }
        
        
        refineTimeTransaction = refineTimeTransaction.replace("#INTERPOLATE1#", interpolate1);
        refineTimeTransaction = refineTimeTransaction.replace("#INTERPOLATE2#", interpolate2);
        refineTimeTransaction = refineTimeTransaction.replace("#INTERPOLATE3#", interpolate3);
        refineTimeTransaction = refineTimeTransaction.replace("#FUNCTIONS#", functions);
        
        return refineTimeTransaction;
    }
    
    /**
     * Create RefineTimeTransaction.h file.
     * @param timeInterpolationInfo	Time interpolation information
     * @return                  The code
     * @throws CGException      CG00X External error
     */
    private String createRefineTimeTransactionH(TimeInterpolationInfo timeInterpolationInfo) throws CGException {
        //Get the template
        String refineTimeTransaction = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/RefineTimeTransaction.h"), 0);
        
        String timeInterpolateFunctions = "";
        for (TimeInterpolator ti: timeInterpolationInfo.getTimeInterpolators().values()) {
        	String params = "";
            ArrayList<String> vars = ti.getAlgorithmGenericVars();
            for (int i = 0; i < vars.size(); i++) {
            	String varName = vars.get(i);
            	params = params + IND + IND + "const std::shared_ptr<hier::PatchData>& pd_" + varName + "," + NL;
            }
            params = params.substring(0, params.lastIndexOf(","));
        	timeInterpolateFunctions = timeInterpolateFunctions + IND + "void" + NL
        			+ IND + "timeInterpolate(" + NL
        			+ IND + IND + "hier::PatchData& pd_dst," + NL
        			+ IND + IND + "const hier::BoxOverlap& overlap," + NL
        			+ params + ");" + NL + NL;
        }
        refineTimeTransaction = refineTimeTransaction.replace("#timeInterpolate#", timeInterpolateFunctions);
        
        return refineTimeTransaction;
    }
    
    /**
     * Create RefineAlgorithm.C file.
     * @param timeInterpolationInfo	Time interpolation information
     * @return                  The code
     * @throws CGException      CG00X External error
     */
    private String createRefineAlgorithm(TimeInterpolationInfo timeInterpolationInfo) throws CGException {
        //Get the template
        String refineAlgorithm = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/RefineAlgorithm.C"), 0);
        
        String refine = "";
        for (TimeInterpolator ti: timeInterpolationInfo.getTimeInterpolators().values()) {
	        String params = "";
	        String assignData = "";
	        ArrayList<String> vars = ti.getAlgorithmGenericVars();
	        for (int i = 0; i < vars.size(); i++) {
	        	String varName = vars.get(i);
	        	params = params + IND + "const int src_t" + varName + "," + NL;
	        	assignData = assignData + IND + "data.d_src_t" + varName + " = src_t" + varName + ";" + NL;
	        }
        	refine = refine + "void RefineAlgorithm::registerRefine(" + NL
        			+ IND + "const int dst," + NL
        			+ IND + "const int src," + NL
        			+ params
        			+ IND + "const int scratch," + NL
        			+ IND + "const std::shared_ptr<hier::RefineOperator>& oprefine," + NL
        			+ IND + "const std::shared_ptr<hier::TimeInterpolateOperator>& optime," + NL
        			+ IND + "const std::shared_ptr<VariableFillPattern>& var_fill_pattern," + NL
        			+ IND + "const std::vector<int>& work_ids)" + NL
        			+ "{" + NL
        			+ IND + "TBOX_ASSERT(optime);" + NL
        			+ IND + "if (d_schedule_created) {" + NL
        			+ IND + IND + "TBOX_ERROR(\"RefineAlgorithm::registerRefine error...\"" + NL
        			+ IND + IND + "<< \"\\nCannot call registerRefine with a RefineAlgorithm object\"" + NL
        			+ IND + IND + "<< \"\\nthat has already been used to create a schedule.\"" + NL
        			+ IND + IND + "<< std::endl);" + NL
        			+ IND + "}" + NL + NL
        			+ IND + "RefineClasses::Data data;" + NL
        			+ IND + "data.d_time_schema = " + ti.getId() + ";" + NL
        			+ IND + "data.d_dst = dst;" + NL
        			+ IND + "data.d_src = src;" + NL
        			+ assignData
        			+ IND + "data.d_scratch = scratch;" + NL
        			+ IND + "data.d_fine_bdry_reps_var = hier::VariableDatabase::getDatabase()->" + NL
        			+ IND + IND + "getPatchDescriptor()->getPatchDataFactory(dst)->fineBoundaryRepresentsVariable();" + NL
        			+ IND + "data.d_time_interpolate = true;" + NL
        			+ IND + "data.d_oprefine = oprefine;" + NL
        			+ IND + "data.d_optime = optime;" + NL
        			+ IND + "data.d_tag = -1;" + NL
        			+ IND + "if (var_fill_pattern) {" + NL
        			+ IND + IND + "data.d_var_fill_pattern = var_fill_pattern;" + NL
        			+ IND + "} else {" + NL
        			+ IND + IND + "data.d_var_fill_pattern.reset(new BoxGeometryVariableFillPattern());" + NL
        			+ IND + "}" + NL
        			+ IND + "data.d_work = work_ids;" + NL
        			+ IND + "d_refine_classes->insertEquivalenceClassItem(data);" + NL
        			+ "}" + NL + NL;
        	
        }
        
        refineAlgorithm = refineAlgorithm.replace("#REFINE_SUBSTEPS#", refine);
        
        return refineAlgorithm;
    }
    
    /**
     * Create RefineAlgorithm.h file.
     * @param timeInterpolationInfo	Time interpolation information
     * @return                  The code
     * @throws CGException      CG00X External error
     */
    private String createRefineAlgorithmH(TimeInterpolationInfo timeInterpolationInfo) throws CGException {
        //Get the template
        String refineAlgorithm = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/RefineAlgorithm.h"), 0);
        
        String refine = "";
        for (TimeInterpolator ti: timeInterpolationInfo.getTimeInterpolators().values()) {
	        String params = "";
	        ArrayList<String> vars = ti.getAlgorithmGenericVars();
	        for (int i = 0; i < vars.size(); i++) {
	        	String varName = vars.get(i);
	        	params = params + IND + "const int src_t" + varName + "," + NL;
	        }
            refine = refine + "void registerRefine(" + NL
            		+ IND + "const int dst," + NL
            		+ IND + "const int src," + NL
            		+ params
            		+ IND + "const int scratch," + NL
            		+ IND + "const std::shared_ptr<hier::RefineOperator>& oprefine," + NL
            		+ IND + "const std::shared_ptr<hier::TimeInterpolateOperator>& optime," + NL
            		+ IND + "const std::shared_ptr<VariableFillPattern>& var_fill_pattern = std::shared_ptr<VariableFillPattern>()," + NL
            		+ IND + "const std::vector<int>& work_ids = std::vector<int>());" + NL + NL;
        }

        refineAlgorithm = refineAlgorithm.replace("#REFINE_SUBSTEPS#", refine);
        
        return refineAlgorithm;
    }
    
    /**
     * Create TimeInterpolateOperator.h file.
     * @param timeInterpolationInfo	Time interpolation information
     * @return                  The code
     * @throws CGException      CG00X External error
     */
    private String createTimeInterpolateOperatorH(TimeInterpolationInfo timeInterpolationInfo) throws CGException {
        //Get the template
        String timeInterpolateOperator = CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/TimeInterpolateOperator.h"), 0);
      
        String timeInterpolateFunctions = "";
        for (TimeInterpolator ti: timeInterpolationInfo.getTimeInterpolators().values()) {
        	String params = "";
            ArrayList<String> vars = ti.getAlgorithmGenericVars();
            for (int i = 0; i < vars.size(); i++) {
            	String varName = vars.get(i);
            	params = params + IND + IND + "const PatchData& src_data_" + varName + "," + NL;
            }
            params = params.substring(0, params.lastIndexOf(","));
        	timeInterpolateFunctions = timeInterpolateFunctions + IND + "virtual void" + NL
        			+ IND + "timeInterpolate(" + NL
        			+ IND + IND + "PatchData& dst_data," + NL
        			+ IND + IND + "const Box& where," + NL
        			+ IND + IND + "const BoxOverlap& overlap," + NL
        			+ params + ") const = 0;" + NL + NL;
        }
        timeInterpolateOperator = timeInterpolateOperator.replace("#timeInterpolate#", timeInterpolateFunctions);
        
        return timeInterpolateOperator;
    }
    
    /**
     * Indent the content of the string n tabulators.
     * @param input     The input string to indent
     * @param indent    The number of indentations.
     * @return          The string indented
     */
    private String indent(String input, int indent) {
        String result = input;
        String indentation = "";
        for (int i = 0; i < indent; i++) {
            indentation = indentation + IND;
        }
        result = indentation + result;
        return result.replaceAll(NL, NL + indentation) + NL;
    }
    
    
    /**
     * Check if the string is a double.
     * @param str   The string to check
     * @return      True if parseable to double
     */
    public static boolean isNumeric(String str) {  
        try {  
            Double.parseDouble(str);  
        }  
        catch (NumberFormatException nfe) {  
            return false;  
        }  
        return true;  
    }
   
}
