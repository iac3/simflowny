/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.codeGeneratorPlugin.utils;

import eu.iac3.mathMS.codeGeneratorPlugin.CGException;
import eu.iac3.mathMS.codeGeneratorPlugin.SAMRAI.X3DMovInfo;
import eu.iac3.mathMS.codeGeneratorPlugin.graph.BOOSTABM_graphCode;
import eu.iac3.mathMS.codesDBPlugin.CodesDB;
import eu.iac3.mathMS.codesDBPlugin.DCException;
import eu.iac3.mathMS.documentManagerPlugin.DMException;
import eu.iac3.mathMS.documentManagerPlugin.DocumentManager;
import eu.iac3.mathMS.documentManagerPlugin.DocumentManagerImpl;
import eu.iac3.mathMS.simflownyUtilsPlugin.SUException;
import eu.iac3.mathMS.simflownyUtilsPlugin.SimflownyUtils;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

import javax.xml.XMLConstants;
import javax.xml.namespace.NamespaceContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.osgi.framework.BundleContext;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;


/**
 * Utility class.
 * @author bminyano
 *
 */
public class CodeGeneratorUtils {
    
    public static final String MTURI = "http://www.w3.org/1998/Math/MathML";
    public static final String MMSURI = "urn:mathms";
    public static final String SMLURI = "urn:simml";
    static final String SPURI = "urn:simPlat";
    static XPathFactory factory = XPathFactory.newInstance();
    static final String IND = "\u0009";
    static final String NL = System.getProperty("line.separator"); 
    static XPath xpath;
    static final int THREE = 3; 
    static final int INDENTATION = 4;
    static boolean test;
    
    //XPath initialization
    
    /**
     * Constructor.
     */
    public CodeGeneratorUtils() {
        factory = new net.sf.saxon.xpath.XPathFactoryImpl();
        xpath = factory.newXPath();
        NamespaceContext ctx = new NamespaceContext() {
            public String getNamespaceURI(String prefix) {
                String uri = "";
                if (prefix.equals("mms")) {
                    uri = "urn:mathms";  
                }    
                else {
                    if (prefix.equals("mt")) {
                        uri = "http://www.w3.org/1998/Math/MathML";
                    }
                    else {
                        if (prefix.equals("sml")) {
                            uri = "urn:simml";
                        }
                    }
                }
                return uri;
            }
            // Dummy implementation - not used!
            public Iterator < ? > getPrefixes(String val) {
                return null;
            }
            // Dummy implemenation - not used!
            public String getPrefix(String uri) {
                return null;
            }
        };

        xpath.setNamespaceContext(ctx);
    }
    
    /**
     * Checks if a string value is a number.
     * @param value     The string value
     * @return          True if number
     *                  False if not
     */
    public static boolean isNumber(String value) {
        String tmp = value;
        if (value.endsWith("d0")) {
            tmp = tmp.substring(0, tmp.lastIndexOf("d0"));  
        }
        try {
            Float.parseFloat(tmp);
            return true;
        }
        catch (NumberFormatException e) {
            return false;
        }
    }
    
    /**
     * Creates a file with the string content.
     * @param path          The path of the new file
     * @param content       The content
     * @throws CGException  CG00X External error
     */
    public static void stringToFile(String path, String content) throws CGException {
        try {
            PrintWriter out = new PrintWriter(path);
            out.write(content);
            out.close();
        } 
        catch (FileNotFoundException e) {
            throw new CGException(CGException.CG00X, e.toString());
        }
        
    }
    
    /**
     * Gets the number form a value.
     * @param value     The value
     * @return          The number
     */
    public static double toNumber(String value) {
        String tmp = value;
        if (value.endsWith("d0")) {
            tmp = tmp.substring(0, tmp.lastIndexOf("d0"));  
        }
        try {
            return Float.parseFloat(tmp);
        }
        catch (NumberFormatException e) {
            return 0;
        }
    }
        
    /**
     * Gets the problem type.
     * @param doc           The problem
     * @return              The type
     * @throws CGException  CG001 XML document is not valid
     *                       CG00X External error
     */
    public static boolean isGraphProblem(Document doc) throws CGException {
        Element root = doc.getDocumentElement();
        if (root.getLocalName().equals("agentBasedProblemGraph")) {
            return true;
        }
        return false;
    }
    
    /**
     * Gets the problem type.
     * @param doc           The problem
     * @return              The type
     * @throws CGException  CG001 XML document is not valid
     *                       CG00X External error
     */
    public static boolean isABMMeshlessProblem(Document doc) throws CGException {
        Element root = doc.getDocumentElement();
        if (root.getLocalName().equals("agentBasedProblemSpatialDomain")) {
            String spatialDomainType = find(doc, "//mms:mesh/mms:type").item(0).getTextContent();
            if (spatialDomainType.equals("Unstructured")) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Gets the problem type.
     * @param doc           The problem
     * @return              The type
     * @throws CGException  CG001 XML document is not valid
     *                       CG00X External error
     */
    public static boolean isABMMeshProblem(Document doc) throws CGException {
        Element root = doc.getDocumentElement();
        if (root.getLocalName().equals("agentBasedProblemSpatialDomain")) {
            String spatialDomainType = find(doc, "//mms:mesh/mms:type").item(0).getTextContent();
            if (spatialDomainType.equals("Structured")) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Gets the problem type.
     * @param doc           The problem
     * @return              The type
     * @throws CGException  CG001 XML document is not valid
     *                       CG00X External error
     */
    public static boolean hasMeshFields(Document doc) throws CGException {
        Element root = doc.getDocumentElement();
        if (root.getLocalName().equals("discretizedProblem")) {
            return find(doc, "//sml:iterateOverCells[*[local-name() != 'iterateOverParticles' and local-name() != 'moveParticles']]").getLength() > 0;
        }
        return false;
    }
    

    /**
     * Gets the problem type.
     * @param doc           The problem
     * @return              The type
     * @throws CGException  CG001 XML document is not valid
     *                       CG00X External error
     */
    public static boolean hasParticleFields(Document doc) throws CGException {
        Element root = doc.getDocumentElement();
        if (root.getLocalName().equals("discretizedProblem")) {
            return find(doc, "//sml:iterateOverCells[*[local-name() = 'iterateOverParticles']]").getLength() > 0;
        }
        return false;
    }
    
    /**
     * Gets the particle species.
     * @param doc           The problem
     * @return              The particle species
     * @throws CGException  CG001 XML document is not valid
     *                       CG00X External error
     */
    public static ArrayList<String> getParticleSpecies(Document doc) throws CGException {
        ArrayList<String> species = new ArrayList<String>();
        NodeList iterators = find(doc, "//sml:iterateOverParticles");
        for (int i = 0; i < iterators.getLength(); i++) {
        	String speciesName = ((Element) iterators.item(i)).getAttribute("speciesNameAtt");
        	if (!species.contains(speciesName)) {
        		species.add(speciesName);
        	}
        }
        return species;
    }
    
    public static HashMap<String, String> getConstants(Document doc) throws CGException {
    	HashMap<String, String> constants = new HashMap<String, String>();
        NodeList iterators = find(doc, "/*/mms:constants/mms:constant");
        for (int i = 0; i < iterators.getLength(); i++) {
        	String name = iterators.item(i).getFirstChild().getTextContent();
        	String value = iterators.item(i).getFirstChild().getNextSibling().getTextContent();
        	constants.put(name, value);
        }
        return constants;
    }
    
    /**
     * Checks if the extrapolation is local.
     * @param problem		The problem
     * @param dimensions	Number of spatial coordinates
     * @return				True if local extrapolation
     * @throws CGException	CG00X External error
     */
    public static ExtrapolationType getExtrapolationType(Document problem, int dimensions) throws CGException {
        NodeList extrapolations = find(problem, "//sml:fieldExtrapolation/mt:math/sml:functionCall");
        if (extrapolations.getLength() > 0) {
            //PDE call
            if (extrapolations.item(0).getChildNodes().getLength() == dimensions * 2 + 3) {
            	return ExtrapolationType.generalPDE;
            }
			if (extrapolations.item(0).getChildNodes().getLength() == dimensions * 3 + 3) {
				return ExtrapolationType.axisPDE;
			}
        }
        return ExtrapolationType.generalPDE;
    }
    
    /**
     * Replace the equivalences in the model using problem equivalences.
     * @param docProblem        The problem
     * @throws CGException      CG00X External error
     */
    public static void replaceEquivalences(Document docProblem) throws CGException {
        try {
            DocumentManager docMan = new DocumentManagerImpl();

            NodeList models = docProblem.getElementsByTagName("mms:model");
            for (int i = models.getLength() - 1; i >= 0; i--) {
                Element modelElem = (Element) models.item(i);
                String modelId = modelElem.getElementsByTagName("mms:id").item(0).getTextContent();
                String model = SimflownyUtils.jsonToXML(docMan.getDocument(modelId));
                Document docModel = stringToDom(model);
                //Getting all the equalities
                NodeList agentPropertyEquivalences = modelElem.getElementsByTagName("mms:agentPropertyEquivalence");
                NodeList vertexPropertyEquivalences = modelElem.getElementsByTagName("mms:vertexPropertyEquivalence");
                NodeList edgePropertyEquivalences = modelElem.getElementsByTagName("mms:edgePropertyEquivalence");
                NodeList coordinateEqualities = modelElem.getElementsByTagName("mms:coordinateEquivalence");
                NodeList parameterEquivalences = modelElem.getElementsByTagName("mms:parameterEquivalence");
                
                // 1-Replacing the model values of field, parameters and coordinates with the case values
                // There could be crossed equivalences, so a temporal intermediate name has to be used 
                //Change to temporal names
                equivalenceReplace(docProblem, docModel, agentPropertyEquivalences, "agentProperty", true);
                equivalenceReplace(docProblem, docModel, vertexPropertyEquivalences, "vertexProperty", true);
                equivalenceReplace(docProblem, docModel, edgePropertyEquivalences, "edgeProperty", true);
                equivalenceReplace(docProblem, docModel, coordinateEqualities, "coordinate", true);
                equivalenceReplace(docProblem, docModel, parameterEquivalences, null, true);
                //Change to new names
                equivalenceReplace(docProblem, docModel, agentPropertyEquivalences, "agentProperty", false);
                equivalenceReplace(docProblem, docModel, vertexPropertyEquivalences, "vertexProperty", false);
                equivalenceReplace(docProblem, docModel, edgePropertyEquivalences, "edgeProperty", false);
                equivalenceReplace(docProblem, docModel, coordinateEqualities, "coordinate", false);
                equivalenceReplace(docProblem, docModel, parameterEquivalences, null, false);
                      
                //Import gather and update rules
                Element rules = (Element) find(docModel, "//mms:rules").item(0);
                modelElem.getParentNode().appendChild(docProblem.importNode(rules.cloneNode(true), true));
                //Import topology if it exists
                NodeList topology = find(docModel, "//mms:topologyChange");
                if (topology.getLength() > 0) {
                    modelElem.getParentNode().appendChild(docProblem.importNode(topology.item(0).cloneNode(true), true));
                }
                //Import execution order
                Element executionOrder = (Element) find(docModel, "//mms:ruleExecutionOrder").item(0);
                modelElem.getParentNode().appendChild(docProblem.importNode(executionOrder.cloneNode(true), true));
            }
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            throw new CGException(CGException.CG00X, e.toString());
        }
    }
        
    /**
     * Replaces all the occurrences of the equivalences in a model.
     * 
     * @param docProblem            The problem document
     * @param docModel              The model document where to replace
     * @param equivalence           The equivalences to be used
     * @param toTemporal            If true the name would be temporal, else the name would be the final name
     * @param tag                   An additional tag to replace apart from mt:ci
     *                                  Depending of the equality type the parameters could be:
     *                                      a - field (field, auxiliary field)
     *                                      b - coordinate (coordinates)
     *                                      c - null (parameters)
     * @throws CGException          CG00X External error
     */
    private static void equivalenceReplace(Document docProblem, Document docModel, NodeList equivalence, String tag, 
            boolean toTemporal) throws CGException {
        //Replace all the equalities
        if (equivalence.getLength() > 0) {
            NodeList replacements = ((Element) equivalence.item(0)).getElementsByTagName("mms:equivalence");
            for (int j = 0; j < replacements.getLength(); j++) {
                Element replacement = (Element) replacements.item(j);
                String oldName;
                String newName;
                if (toTemporal) {
                    oldName = replacement.getLastChild().getTextContent();
                    newName = replacement.getFirstChild().getTextContent() + "#TMP";
                }
                else {
                    oldName = replacement.getFirstChild().getTextContent() + "#TMP";
                    newName = replacement.getFirstChild().getTextContent(); 
                }
                Node oldCi = createElement(docModel, MTURI, "ci");
                //Get the modelData
                oldCi.setTextContent(oldName);
                Node newCi = createElement(docModel, MTURI, "ci");
                //Get the caseData
                newCi.setTextContent(newName);
                //Replace the old ci value in all the document.
                nodeReplace(docModel.getDocumentElement(), oldCi, newCi);
                // If there is additional tag to replace
                if (tag != null) {
                    oldCi = createElement(docModel, MMSURI, tag);
                    oldCi.setTextContent(oldName);
                    newCi = createElement(docModel, MMSURI, tag);
                    newCi.setTextContent(newName);
                    //Replace the old field value in all the document.
                    nodeReplace(docModel.getDocumentElement(), oldCi, newCi);
                    if (tag.equals("field")) {
                        oldCi = createElement(docModel, MMSURI, "auxiliaryField");
                        oldCi.setTextContent(oldName);
                        newCi = createElement(docModel, MMSURI, "auxiliaryField");
                        newCi.setTextContent(newName); 
                        //Replace the old field value in all the document.
                        nodeReplace(docModel.getDocumentElement(), oldCi, newCi);
                    }
                    //Additional replacements by tags
                    if (tag.equals("coordinate")) {
                        oldCi = createElement(docModel, MMSURI, "firstDerivative");
                        oldCi.setTextContent(oldName);
                        newCi = createElement(docModel, MMSURI, "firstDerivative");
                        newCi.setTextContent(newName);
                        nodeReplace(docModel.getDocumentElement(), oldCi, newCi);
                        oldCi = createElement(docModel, MMSURI, "secondDerivative");
                        oldCi.setTextContent(oldName);
                        newCi = createElement(docModel, MMSURI, "secondDerivative");
                        newCi.setTextContent(newName);
                        nodeReplace(docModel.getDocumentElement(), oldCi, newCi);
                        //This should replace only the with the base coordinates
                        String defName = newName;
                        oldCi = createElement(docModel, MMSURI, "eigenCoordinate");
                        oldCi.setTextContent(oldName);
                        newCi = createElement(docModel, MMSURI, "eigenCoordinate");
                        newCi.setTextContent(defName);
                        nodeReplace(docModel.getDocumentElement(), oldCi, newCi);
                    }
                }
            }
        }
    }
    
    /**
     * Checks if the variables are used in the fragment of document.
     * @param fragment          The fragment to search in
     * @param doc               The problem that have the information about the variables
     * @return                  True is any variable is used.
     * @throws CGException      CG00X External error
     */
    public static boolean usesVariable(Node fragment, Document doc) throws CGException {
        NodeList fields = CodeGeneratorUtils.find(doc, "./*/mms:field|/*/mms:auxiliaryField|/*/mms:interactionField|/*/mms:edgeField");
        for (int i = 0; i < fields.getLength(); i++) {
            NodeList variablesUsed = CodeGeneratorUtils.find(fragment, ".//mt:ci[text() = '" + fields.item(i).getTextContent() 
                    + "']|.//mt:ci[starts-with(text(), 'constraint')]");
            if (variablesUsed.getLength() > 0) {
                return true;
            }
        }
        return false;
    }
    
    
    /**
     * Method that validates the xml document with the discretized problem schema.
     * 
     * @param xmlDoc         The document to be validated
     * @param pt             The problem type
     * @throws CGException   CG001 XML document does not match XML Schema
     *                       CG00X External error
     */
    public static void schemaValidation(String xmlDoc, String pt) throws CGException {
        try {
            // 1. Lookup a factory for the W3C XML Schema language
            SchemaFactory fact = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            fact.setFeature("http://apache.org/xml/features/validation/schema-full-checking", false);
        
            // 2. Compile the schema. 
            // Here the schema is loaded from a java.io.File.
            String filePath = "common" + File.separator + "XSD" + File.separator + pt + ".xsd";
            File schemaLocation = new File(filePath);
            Schema schema = fact.newSchema(schemaLocation);
            // 3. Get a validator from the schema.
            Validator validator = schema.newValidator();
        
            // 4. Parse the document you want to check.
            InputStream is = new ByteArrayInputStream(xmlDoc.getBytes("UTF-8"));
            Source source = new StreamSource(is);
        
            // 5. Check the document
            validator.validate(source);
        }
        catch (SAXException e) {
            e.printStackTrace(System.out);
            throw new CGException(CGException.CG001, e.toString());
        }
        catch (Exception e) {
            throw new CGException(CGException.CG00X, e.toString());
        }
    }

    
    /**
     * Converts a string to Dom.
     * 
     * @param str               The string
     * @return                  The Dom document
     * @throws CGException      CG001 Not a valid XML Document
     *                          CG00X External error
     */
    public static Document stringToDom(String str) throws CGException {
        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            docFactory.setNamespaceAware(true);
            docFactory.setIgnoringElementContentWhitespace(true);
            docFactory.setIgnoringComments(true);
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            InputStream is = new ByteArrayInputStream(str.getBytes("UTF-8"));
            Document doc = docBuilder.parse(is);
            removeWhitespaceNodes(doc.getDocumentElement());
            return doc;
        }
        catch (SAXException e) {
            throw new CGException(CGException.CG001, e.toString());
        }
        catch (Exception e) {
            throw new CGException(CGException.CG00X, e.toString());
        }
    }
    
    /**
     * Converts a node to string.
     * 
     * @param node              The node
     * @return                  The String
     * @throws CGException      CG00X External error
     */
    public static String domToString(Node node) throws CGException {
        try {
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            //StreamResult result = new StreamResult(new StringWriter());
            StreamResult result = new StreamResult(new ByteArrayOutputStream());
            DOMSource source = new DOMSource(node);
            transformer.transform(source, result);
            return new String(((ByteArrayOutputStream) result.getOutputStream()).toByteArray(), "UTF-8");
        }
        catch (Exception e) {
            throw new CGException(CGException.CG00X, e.toString());
        }
    }
    
    /**
     * Remove the whitespaced nodes from a Dom element.
     * 
     * @param e The element to delete the whitespaces from
     */
    public static void removeWhitespaceNodes(Element e) {
        NodeList children = e.getChildNodes();
        for (int i = children.getLength() - 1; i >= 0; i--) {
            Node child = children.item(i);
            if (child instanceof Text && ((Text) child).getData().trim().length() == 0) {
                e.removeChild(child);
            }
            else if (child instanceof Element) {
                removeWhitespaceNodes((Element) child);
            }
        }
    }
    
    /**
     * Executes an xpath query in the document.
     * @param node          The document to find in
     * @param xpathQuery    The query
     * @return              A nodelist with the results
     * @throws CGException  CG00X External error
     */
    public static NodeList find(Node node, String xpathQuery) throws CGException {
        try {
            XPathExpression expr = xpath.compile(xpathQuery);

            return (NodeList) expr.evaluate(node, XPathConstants.NODESET);
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new CGException(CGException.CG00X, e.getMessage());
        }
    }
    
    /**
     * Executes an xpath query in the document and return unique values.
     * @param node          The document to find in
     * @param xpathQuery    The query
     * @return              A nodelist with the results
     * @throws CGException  CG00X External error
     */
    public static Set<Node> findWithoutDuplicates(Node node, String xpathQuery) throws CGException {
        try {
            XPathExpression expr = xpath.compile(xpathQuery);
            Set<Node> uniqueNodes = new LinkedHashSet<Node>();
            Set<String> uniqueValues = new HashSet<String>();
            NodeList nodes = (NodeList)expr.evaluate(node, XPathConstants.NODESET);
            for (int i = 0; i < nodes.getLength(); i++) {
                if (uniqueValues.add(nodes.item(i).getTextContent())) {
                	  uniqueNodes.add(nodes.item(i));
                }
            }
            return uniqueNodes;
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new CGException(CGException.CG00X, e.getMessage());
        }
    }
    
    /**
     * Replaces the occurrences of old node with a new node in a element.
     * @param scope          The root element
     * @param oldNode       The old node
     * @param newNode       The new node
     * @throws CGException  CG00X External error
     */
    public static void nodeReplace(Node scope, Node oldNode, Node newNode) throws CGException {
        NodeList candidates = find(scope, printTree(oldNode, 0));
        for (int i = candidates.getLength() - 1; i >= 0; i--) {
            candidates.item(i).getParentNode().replaceChild(newNode.cloneNode(true), candidates.item(i));
        }
    }
    
    /**
     * Creates a Xpath with the information of the node. Recursive.
     * @param doc           The node to create.
     * @param position      The position in the code of the node.
     * @return              A Xpath for the node
     */
    public static String printTree(Node doc, int position) {
        try {
            String acc;
            if (doc.getNodeName().equals("#text")) {
                return "text() = \"" + doc.getNodeValue() + "\"";
            }
          
            if (position == 0) {
                acc = ".//" + doc.getNodeName();
            }
            else {
                acc = "name(*[" + position + "]";
            }
          
            NodeList nl = doc.getChildNodes();
            acc = acc + "[";
            NamedNodeMap nnm = doc.getAttributes();
            for (int i = 0; i < nnm.getLength(); i++) {
                acc = acc + "@" + nnm.item(i).getNodeName() + " = " + nnm.item(i).getNodeValue() + " and ";
            }
            if (nnm.getLength() == 0) {
                acc = acc + "not(attribute::*) and ";
            }
                
            for (int i = 0; i < nl.getLength(); i++) {
                Node node = nl.item(i);
                acc = acc + printTree(node, i + 1) + " and ";
            }
            acc = acc.substring(0, acc.lastIndexOf(" and"));
            acc = acc + "]";
          
            if (position > 0) {
                acc = acc + ") = \"" + doc.getNodeName() + "\"";
            }
            return acc;
        } 
        catch (Throwable e) {
            return null;
        }
    }
    
    /**
     * Create an element in the Uri namespace.
     * @param doc           The document where to create the element
     * @param uri           The namespace uri
     * @param tagName       The tag name
     * @return              The element
     */
    public static Element createElement(Document doc, String uri, String tagName) {
        Element element;
        if (uri != null) {
            element = doc.createElementNS(uri, tagName);
            if (uri.equals("http://www.w3.org/1998/Math/MathML")) {
                element.setPrefix("mt");
            }
            if (uri.equals("urn:mathms")) {
                element.setPrefix("mms");
            }
            if (uri.equals("urn:simml")) {
                element.setPrefix("sml");
            }
        }
        else {
            element = doc.createElement(tagName); 
        }
        return element;
    }
    
    /**
     * Gets if a coordinate has periodical boundaries.
     * @param doc               The problem
     * @param coordinates       The problem spatial coordinates
     * @return                  A hashmap with the info for the periodical boundaries
     * @throws CGException      CG00X External error
     */
    public static LinkedHashMap<String, String> getPeriodicalBoundaries(Document doc, ArrayList<String> coordinates) throws CGException {
        LinkedHashMap<String, String> result = new LinkedHashMap<String, String>();
        for (int i = 0; i < coordinates.size(); i++) {
            String coord = coordinates.get(i);
            if (find(doc, "/*/mms:boundaryConditions/mms:boundaryPolicy/mms:boundaryCondition[mms:type/mms:periodical and (mms:axis = 'All' or mms:axis = '" + coord + "')]").getLength() > 0) {
                result.put(coord, "periodical");
            }
            else {
                result.put(coord, "noperiodical");
            }
        }
        return result;
    }
    
    /**
     * Get the time levels of each field into a hash table for later processes.
     * @param doc               The xml document to search into
     * @return                  A hashtable with the fields and their timelevels
     * @throws CGException      CG00X - External error
     */
    public static LinkedHashMap<String, Integer> getFieldTimeLevels(Document doc) throws CGException {
        try {
            LinkedHashMap<String, Integer> fieldTimeLevels = new LinkedHashMap<String, Integer>();
            //Fields
            String query = "/*/mms:fields/mms:field|/*/mms:agentProperties/mms:agentProperty|"
                    + "/*/mms:vertexProperties/mms:vertexProperty";
            NodeList fields = find(doc, query);
            for (int i = 0; i < fields.getLength(); i++) {
                Element field = (Element) fields.item(i);
                //Fill the field time levels table
                fieldTimeLevels.put(field.getTextContent(), 2);
            }
            //Fields and vectors
            query = "/*/mms:auxiliaryFields/mms:auxiliaryField|"
                    + "/*/mms:vectorNames/mms:vectorName";
            fields = find(doc, query);
            for (int i = 0; i < fields.getLength(); i++) {
                Element field = (Element) fields.item(i);
                //Fill the field time levels table
                fieldTimeLevels.put(field.getTextContent(), 1);
            }
            
            return fieldTimeLevels;
        } 
        catch (Exception e) {
            throw new CGException(CGException.CG00X, e.getMessage());
        }
    }
    
    /**
     * Get the function names.
     * @param doc               The xml document to search into
     * @return                  A list of function names
     * @throws CGException      CG00X - External error
     */
    public static ArrayList<String> getFunctionNames(Document doc) throws CGException {
    	ArrayList<String> funcNames = new ArrayList<String>();
        NodeList names = find(doc, "/*/mms:functions/mms:function/mms:functionName");
        for (int i = 0; i < names.getLength(); i++) {
            funcNames.add(names.item(i).getTextContent());
        }
        return funcNames;
    }
    
    /**
     * Gets the interaction fields.
     * @param doc               The problem
     * @return                  The interaction fields
     * @throws CGException      CG00X - External error
     */
    public static ArrayList<String> getEdgeFields(Document doc) throws CGException {
        ArrayList<String> fields = new ArrayList<String>();
        //by default one time level is necessary
        //Get all the minus-operated time levels
        String query = "/*/mms:edgeProperties/mms:edgeProperty";
        NodeList edgeFields = find(doc, query);
        for (int j = 0; j < edgeFields.getLength(); j++) {
            //Get all the last child that is the time component
            fields.add(SAMRAIUtils.variableNormalize(edgeFields.item(j).getTextContent()));
        }
        
        return fields;
    }
    
    /**
     * Get the time levels of a particle position variable depending on the time steps in the code.
     * @param doc               The document with the information
     * @param coords          	The coordinates
     * @param timeCoord         The time coordinate
     * @return                  The time level of the field
     * @throws CGException      CG00X - External error
     */
    public static LinkedHashMap<String, Integer> getPositionTimeLevels(Document doc, ArrayList<String> coords, String timeCoord) throws CGException {
    	LinkedHashMap<String, Integer> timeLevels = new LinkedHashMap<String, Integer>();
    	for (int i = 0; i < coords.size(); i++) {
    		String coord = coords.get(i);
    		String contCoord = CodeGeneratorUtils.discToCont(doc, coord);
	    	//by default one time level is necessary
	        int levels = 1;
	        //Get all the minus-operated time levels
	        String query = "//mms:interiorExecutionFlow//mt:ci/mt:msup[mt:ci[position() = 1 and sml:particlePosition[text() ='" 
	            + contCoord + "']] and mt:apply[mt:minus and mt:ci[starts-with(text()[1], '(') or text() = '" + timeCoord + "']]]"
	            + "|//mms:surfaceExecutionFlow//mt:ci/mt:msup[mt:ci[position() = 1 and sml:particlePosition[text() ='" 
	            + contCoord + "']] and mt:apply[mt:minus and mt:ci[starts-with(text()[1], '(') or text() = '" + timeCoord + "']]]";
	        NodeList ciFields = find(doc, query);
	        if (ciFields.getLength() > 0) {
	            for (int j = 0; j < ciFields.getLength(); j++) {
	                //Get all the last child that is the time component
	                int timeLevel = Integer.parseInt(ciFields.item(j).getLastChild().getLastChild().getTextContent());
	                if (timeLevel > levels) {
	                	levels = timeLevel;
	                }
	            }
	            levels = levels + 2;
	        }
	        else {
	            //Get the time step without operations
	            query = "//mms:interiorExecutionFlow//mt:ci/mt:msup[position() = 1 and mt:ci[sml:particlePosition[text() ='" 
	            + contCoord + "']] and mt:ci[starts-with(text()[1], '(') or text() = '" + timeCoord + "']]|"
	                + "//mms:surfaceExecutionFlow//mt:ci/mt:msup[position() = 1 and mt:ci[sml:particlePosition[text() ='" 
	            + contCoord + "']] and mt:ci[starts-with(text()[1], '(') or text() = '" + timeCoord + "']]";
	            ciFields = find(doc, query);
	            if (ciFields.getLength() > 0) {
	                levels = 2;
	            }
	        }
	        timeLevels.put("position" + contCoord, levels);
    	}
    	return timeLevels;
    }
    
    /**
     * Gets the evolution step method.
     * @param problem             The problem
     * @return                    The evolution step
     * @throws CGException        CG001 XML document is not valid
     */
    public static String getEvolutionStep(Document problem) throws CGException {
        try {
            return find(problem, "//mms:evolutionStep").item(0).getTextContent();
        }
        catch (Exception e) {
            throw new CGException(CGException.CG001, "EvolutionStep tag not defined");
        }
    }
        
    /**
     * Gets the schema stencil used in the problem in a timestep.
     * @param problem       the problem
     * @return              the stencil
     * @throws CGException  CG00X External error
     */
    public static int getSchemaStencil(Document problem) throws CGException {
        int stencil = 0;
         
        String query = "/*/mms:region//mms:stencil|/*/mms:subregions/mms:subregion//mms:stencil|//mms:maximumStepStencil";
        NodeList stencils = find(problem, query);
        for (int i = 0; i < stencils.getLength(); i++) {
            if (Integer.parseInt(stencils.item(i).getTextContent()) > stencil) {
                stencil = Integer.parseInt(stencils.item(i).getTextContent());
            }
        }
        return Math.max(1, stencil);
    }
    
    /**
     * Gets the extrapolation stencil used in the problem.
     * @param problem       the problem
     * @return              the stencil
     * @throws CGException  CG00X External error
     */
    public static int getExtrapolationStencil(Document problem) throws CGException {
    	// Extra stencil is needed only in case of multiple regions
        if (find(problem, "//sml:fieldExtrapolation").getLength() > 0 && find(problem, "//mms:subregion").getLength() > 0) {
            return Integer.parseInt(find(problem, "//mms:extrapolationMethod/mms:stencil").item(0).getTextContent());
        }
        return 0;
    }
    
    /**
     * Gets the maximum accumulated stencil used in the steps.
     * @param problem       the problem
     * @param maxStencil	The maximum stencil on the whole problem
     * @return              the stencil
     * @throws CGException  CG00X External error
     */
    public static int getStepStencil(Document problem, int maxStencil) throws CGException {
        int stencil = 0;
        
        //Find maximum step stencil in PDE
        String query = "//mms:maximumStepStencil";
        NodeList stencils = find(problem, query);
        if (stencils.getLength() > 0) {
	        for (int i = 0; i < stencils.getLength(); i++) {
	        	stencil = Math.max(stencil, Integer.parseInt(stencils.item(i).getTextContent()));
	        }
	        if (find(problem, "//sml:fieldExtrapolation").getLength() > 0) {
	            int extrapolationStencil = Integer.parseInt(find(problem, "//mms:extrapolationMethod/mms:stencil").item(0).getTextContent());
	            return Math.max(stencil, extrapolationStencil + stencil - 1);
	        }
	        return stencil;
        }
        //Default stencil if no information of step stencil available
        return maxStencil;
    }   
    
    /**
     * Creates the region precedence from a topological sorting algorithm.
     * @param doc               The document with the precedence information
     * @return                  An arrayList with the ordered regions
     * @throws CGException      CG003 Not possible to create code for the platform
     *                          CG00X External error
     */
    public static ArrayList<String> getRegionPrecedence(Document doc) throws CGException {
        ArrayList<String> result = new ArrayList<String>();
        Queue<String> noEdges = new LinkedList<String>();
        Hashtable<String, ArrayList<String>> edges = new Hashtable<String, ArrayList<String>>();
        
        //Fill the graph with the dependencies
        NodeList regionList = find(doc, "/*/mms:region[descendant::mms:interiorModel]/mms:name|/*/mms:subregions/mms:subregion[descendant::mms:interiorModel or descendant::mms:surfaceModel]/mms:name");
        for (int i = 0; i < regionList.getLength(); i++) {
            String regionName = regionList.item(i).getTextContent();
            NodeList precedences = find(doc, "/*/mms:subregionPrecedence/mms:precedence[mms:surrogateRegion = '" 
                    + regionName + "']/mms:prevalentRegion");
            if (precedences.getLength() == 0) {
                noEdges.offer(regionName);
            }
            else {
                ArrayList<String> precedent = new ArrayList<String>();
                for (int j = 0; j < precedences.getLength(); j++) {
                    precedent.add(precedences.item(j).getTextContent());
                }
                edges.put(regionName, precedent);
            }
        }
        
        //Topological sorting algorithm (See http://en.wikipedia.org/wiki/Topological_sort)
        while (!noEdges.isEmpty()) {
            String n = noEdges.poll();
            result.add(n);
            //for each node m with an edge e from n to m do
            Enumeration<String> e = edges.keys();
            while (e.hasMoreElements()) {
                String m = e.nextElement();
                //remove edge e from the graph (if not exist nothing happens)
                ArrayList<String> precedences = edges.get(m);
                precedences.remove(n);
                //if m has no other incoming edges then insert m into S (and remove it from the edges)
                if (precedences.isEmpty()) {
                    noEdges.offer(m);
                    edges.remove(m);
                } 
                //put the new edge relation into the edges of m
                else {
                    edges.put(m, precedences);
                }
            }
        }
        //Cycle control
        if (!edges.isEmpty()) {
            throw new CGException(CGException.CG003, "There are cycles in the region precedences");
        }
        
        return result;
    }
        
    /**
     * Creates the boundary precedence from a precedence list.
     * @param doc               The document with the precedence information
     * @return                  An arrayList with the ordered regions
     * @throws CGException      CG00X External error
     */
    public static ArrayList<String> getBoundariesPrecedence(Document doc) throws CGException {
        ArrayList<String> result = new ArrayList<String>();

        NodeList boundaryPrecedence = find(doc, "/*/mms:boundaryPrecedence/mms:boundary");
        for (int i = 0; i < boundaryPrecedence.getLength(); i++) {
            result.add(boundaryPrecedence.item(i).getTextContent());
        }
        //If precedence is not set, set a default order
        if (boundaryPrecedence.getLength() == 0) {
            NodeList coords = find(doc, "/*/mms:coordinates/mms:spatialCoordinates/mms:spatialCoordinate");
            for (int i = 0; i < coords.getLength(); i++) {
                String coord = discToCont(doc, coords.item(i).getTextContent());
                result.add(coord + "-Lower");
                result.add(coord + "-Upper");
            }
        }
        return result;
    }
    
    /**
     * Creates the region group list.
     * @param doc               The document with the group information
     * @param regionPrecedence The precedence between regions
     * @return                  An arrayList of region groups
     * @throws CGException      CG00X External error
     */
    public static ArrayList<String> getRegions(Document doc, ArrayList<String> regionPrecedence) throws CGException {
        ArrayList<String> result = new ArrayList<String>();
        
        //Fill the graph with the dependencies
        for (int i = 0; i < regionPrecedence.size(); i++) {
            String regionName = find(doc, "/*/mms:region[descendant::mms:name = '" 
                + regionPrecedence.get(i) + "']/mms:name|/*/mms:subregions/mms:subregion[descendant::mms:name = '" 
                + regionPrecedence.get(i) + "']/mms:name").item(0).getTextContent();
            if (!result.contains(regionName)) {
                result.add(regionName);
            }
        }
        return result;
    }
    
    /**
     * Return the spatial coordinates of the problem.
     * @param problem           The problem
     * @return                  the spatial coordinates of the problem.
     * @throws CGException      CG00X External error
     */
    public static ArrayList<String> getProblemSpatialCoordinates(Document problem) throws CGException {
        ArrayList<String> coords = new ArrayList<String>();
        NodeList coordList =  find(problem, "/*/mms:coordinates/mms:spatialCoordinates/mms:spatialCoordinate");
        for (int i = 0; i < coordList.getLength(); i++) {
            coords.add(coordList.item(i).getTextContent());
        }
        return coords;
    }
    
    /**
     * Return the continuous spatial coordinates of the problem.
     * @param pi           		The problem information
     * @return                  the spatial coordinates of the problem.
     * @throws CGException      CG00X External error
     */
    public static ArrayList<String> getProblemContSpatialCoordinates(ProblemInfo pi) throws CGException {
        ArrayList<String> coords = new ArrayList<String>();
        for (int i = 0; i < pi.getCoordinates().size(); i++) {
        	coords.add(CodeGeneratorUtils.discToCont(pi.getProblem(), pi.getCoordinates().get(i)));
        }
        return coords;
    }
    
    /**
     * Return the time coordinate of the problem.
     * @param problem           The problem
     * @return                  the time coordinate of the problem.
     * @throws CGException      CG00X External error
     */
    public static String getProblemTimeCoordinate(Document problem) throws CGException {
        return find(problem, "/*/mms:coordinates/mms:timeCoordinate").item(0).getTextContent();
    }
    /**
     * Get the grid limits.
     * @param coordinates       The coordinates of the problem
     * @param problem           The problem
     * @return                  Return the limits for all the base spatial coordinates
     * @throws CGException      CG00X External error
     */
    public static LinkedHashMap<String, CoordinateInfo> getGridLimits(ArrayList<String> coordinates, Document problem) 
            throws CGException {
        LinkedHashMap<String, CoordinateInfo> limits = new LinkedHashMap<String, CoordinateInfo>();
        
        for (int k = 0; k < coordinates.size(); k++) {
            String coord = coordinates.get(k);
            NodeList coordList =  find(problem, "/*/mms:region/mms:spatialDomain/mms:coordinateLimits[mms:coordinate = '" 
                    + coord + "']|//mms:mesh//mms:coordinateLimits[mms:coordinate = '" + coord + "']");
            for (int i = 0; i < coordList.getLength(); i++) {
                Element min = (Element) coordList.item(i).getFirstChild().getNextSibling();
                Element max = (Element) min.getNextSibling();
                CoordinateInfo ci = new CoordinateInfo();
                ci.setName(coord);
                ci.setMin((Element) min.getFirstChild().getFirstChild());
                ci.setMax((Element) max.getFirstChild().getFirstChild());
                limits.put(coord, ci);
            }
        }
        
        return limits;
    }
    
    /**
     * Get the limits of the regions in every local coordinate.
     * @param problem           The problem with the information
     * @param context           The bundle context
     * @return                  The structure with the resulting information
     * @throws CGException      CG00X External error
     */
    public static LinkedHashMap<String, LinkedHashMap<String, CoordinateInfo>> getRegionLimits(Document problem, BundleContext context) 
            throws CGException {
        try {
            LinkedHashMap<String, LinkedHashMap<String, CoordinateInfo>> limits = 
                    new LinkedHashMap<String, LinkedHashMap<String, CoordinateInfo>>();
            
            LinkedHashMap<String, CoordinateInfo> cis;
            NodeList regions = find(problem, "/*/mms:region|/*/mms:subregions/mms:subregion");
            for (int i = 0; i < regions.getLength(); i++) {
                cis = new LinkedHashMap<String, CoordinateInfo>();
                String regionName = regions.item(i).getFirstChild().getTextContent();
                //Process a x3d region
                if (find(regions.item(i), ".//mms:x3d").getLength() > 0) {
                    DocumentManager docMan = new DocumentManagerImpl();
                    X3DRegion region = parseX3dRegion(docMan.getX3D(find(regions.item(i), "./mms:location/mms:x3d/mms:id")
                                    .item(0).getTextContent()));
                    String query = "/*/mms:spatialCoordinates/mms:spatialCoordinate";
                    
                    //Fill the point data
                    String pointAttr = region.getPointAttr();
                    double[][] transformedPoints = null;
                    if (pointAttr.indexOf(",") == -1) {
                        String[] pointStrings = pointAttr.substring(0, pointAttr.lastIndexOf(" ")).split(" ");
                        //X3D is always a 3D point file. If the problem is 2D the last coordinate (Z) will not be generated
                        transformedPoints = new double[(pointStrings.length - 1) / THREE][THREE];
                        for (int j = 0; j < (pointStrings.length - 1) / THREE; j++) {
                            transformedPoints[j][0] = Double.valueOf(pointStrings[j * THREE]);
                            transformedPoints[j][1] = Double.valueOf(pointStrings[j * THREE + 1]);
                            transformedPoints[j][2] = Double.valueOf(pointStrings[j * THREE + 2]);
                        }
                    } 
                    else {
                        String[] pointStrings = pointAttr.substring(0, pointAttr.lastIndexOf(",")).split(", ");
                        Point[] points = new Point[pointStrings.length];
                        //X3D is always a 3D point file. If the problem is 2D the last coordinate (Z) will not be generated
                        transformedPoints = new double[pointStrings.length][THREE];
                        for (int j = 0; j < pointStrings.length; j++) {
                            points[j] = new Point(pointStrings[j]);
                            double[] coords = points[j].getCoordinate();
                            for (int k = 0; k < coords.length; k++) {
                                transformedPoints[j][k] = coords[k];
                            }
                        }
                    }
                    applyTransformations(region, transformedPoints);
                    NodeList coordinates = find(problem, query);
                    for (int j = 0; j < coordinates.getLength(); j++) {
                        CoordinateInfo ci = new CoordinateInfo();
                        ci.setName(coordinates.item(j).getTextContent());
                        ci.setMin(getX3dMin(problem, j, transformedPoints));
                        ci.setMax(getX3dMax(problem, j, transformedPoints));
                        cis.put(coordinates.item(j).getTextContent(), ci);
                    }
                    
                }
                //Process a mathematical region
                if (find(regions.item(i), ".//mms:coordinateLimits").getLength() > 0) {
                    NodeList coordinates = find(regions.item(i), ".//mms:coordinateLimits");
                    for (int j = 0; j < coordinates.getLength(); j++) {
                        String coordinateName = coordinates.item(j).getFirstChild().getTextContent();
                        Element min = (Element) coordinates.item(j).getFirstChild().getNextSibling();
                        Element max = (Element) min.getNextSibling();
                        CoordinateInfo ci = new CoordinateInfo();
                        ci.setName(coordinateName);
                        ci.setMin((Element) min.getFirstChild());
                        ci.setMax((Element) max.getFirstChild());
                        cis.put(coordinateName, ci);
                    }
                }
                limits.put(regionName, cis);
            }
            
            return limits;
        } 
        catch (DMException e) {
        	e.printStackTrace(System.out);
            throw new CGException(CGException.CG00X, e.getMessage());
        }
    }
    
    /**
     * A method for the format of the output.
     * 
     * @param document          The document to be formatted
     * @return                  The document formatted
     * @throws CGException      CG00X - External error
     */
    private static String indent(String document) throws CGException {
        try {
            DocumentBuilder docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document doc = docBuilder.parse(new ByteArrayInputStream(document.getBytes("UTF-8")));

            // Setup pretty print options
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

            // Return pretty print xml string
            StringWriter stringWriter = new StringWriter();
            transformer.transform(new DOMSource(doc), new StreamResult(stringWriter));
            return stringWriter.toString();
        }
        catch (Throwable e) {
            throw new CGException(CGException.CG00X, e.toString());
        }
    }
    
    /**
     * Get the important information from the x3d region file.
     * @param region           The x3d region
     * @return                  The structure with the resulting information
     * @throws CGException      CG00X External error
     */
    public static X3DRegion parseX3dRegion(String region) throws CGException {
        try {
            region = indent(region);
            X3DRegion x3d = new X3DRegion();
            BufferedReader in = new BufferedReader(new StringReader(region));
            ArrayList<ArrayList<String>> tranforms = new ArrayList<ArrayList<String>>();
            ArrayList<String> tranformGroup = new ArrayList<String>();
            StringBuffer transform = new StringBuffer();
            StringBuffer coordIndex = new StringBuffer();
            StringBuffer points = new StringBuffer();
            //TODO borrar
          //  StringBuffer normals = new StringBuffer();
            
            String str;
            boolean coord = false;
            boolean point = false;
            boolean openTransform = false;
            //boolean normal = false;
            tranformGroup = new ArrayList<String>();
            int pendingId = 0;
            while ((str = in.readLine()) != null) {
                if (coord == true || str.indexOf("coordIndex=\"") > -1) {
                    if (coord) {
                        if (str.indexOf("\"") > -1) {
                            coordIndex.append(" " + str.substring(0, str.indexOf("\"")).trim());
                            coord = false;
                        }
                        else {
                            coordIndex.append(" " + str.trim() + " ");
                            coord = true;
                        }
                    }
                    else {
                        if (str.indexOf("\"", str.indexOf("coordIndex=\"") + "coordIndex=\"".length()) > -1) {
                            coordIndex.append(str.substring(str.indexOf("coordIndex=\"") + "coordIndex=\"".length(), str.indexOf("\"", 
                                    str.indexOf("coordIndex=\"") + "coordIndex=\"".length())));
                            coord = false;
                        }
                        else {
                            coordIndex.append(str.substring(str.indexOf("coordIndex=\"") + "coordIndex=\"".length()).trim() + " ");
                            coord = true;
                        }
                    }
                } 
                if (point == true || str.indexOf("point=\"") > -1) {
                    if (point) {
                        if (str.indexOf("\"") > -1) {
                            points.append(" " + str.substring(0, str.indexOf("\"")).trim());
                            point = false;
                        }
                        else {
                            points.append(" " + str.trim() + " ");
                            point = true;
                        }
                    }
                    else {
                        if (str.indexOf("\"", str.indexOf("point=\"") + "point=\"".length()) > -1) {
                            points.append(str.substring(str.indexOf("point=\"") + "point=\"".length(), str.indexOf("\"", 
                                    str.indexOf("point=\"") + "point=\"".length())));
                            point = false;
                        }
                        else {
                            points.append(str.substring(str.indexOf("point=\"") + "point=\"".length()).trim() + " ");
                            point = true;
                        }
                    }
                }
                //TODO borrar
             /*   if (normal == true || str.indexOf("vector=\"") > -1) {
                    if (normal) {
                        if (str.indexOf("\"") > -1) {
                            normals.append(" " + str.substring(0, str.indexOf("\"")).trim());
                            normal = false;
                        }
                        else {
                            normals.append(" " + str.trim() + " ");
                            normal = true;
                        }
                    }
                    else {
                        if (str.indexOf("\"", str.indexOf("vector=\"") + "vector".length()) > -1) {
                            normals.append(str.substring(str.indexOf("vector=\"") + "vector=\"".length(), str.indexOf("\"", 
                                    str.indexOf("vector=\"") + "vector=\"".length())));
                            normal = false;
                        }
                        else {
                            normals.append(str.substring(str.indexOf("vector=\"") + "vector=\"".length()).trim() + " ");
                            normal = true;
                        }
                    }
                }*/
                
                int index = 0;
                while (pendingId > 0 || str.indexOf("rotation=\"", index) > -1 || str.indexOf("scale=\"", index) > -1 
                        || str.indexOf("translation=\"", index) > -1 || str.indexOf("<Transform", index) > -1 
                        || (str.indexOf(">", index) > -1 && openTransform)) {
                    if (pendingId == 0 && !openTransform && str.indexOf("<Transform", index) > -1 && (str.indexOf("scale=\"", index) == -1 
                            || str.indexOf("<Transform", index) < str.indexOf("scale=\"", index)) && (str.indexOf("translation=\"", index) == -1 
                            || str.indexOf("<Transform", index) < str.indexOf("translation=\"", index)) && (str.indexOf("rotation=\"", index) == -1 
                            || str.indexOf("<Transform", index) < str.indexOf("rotation=\"", index)) && (str.indexOf(">", index) == -1 
                            || str.indexOf("<Transform", index) < str.indexOf(">", index))) {
                        tranformGroup = new ArrayList<String>();    
                        index = str.indexOf("<Transform", index) + 1;
                        openTransform = true;
                    }
                    if (pendingId == 0 && openTransform && str.indexOf(">", index) > -1 && (str.indexOf("scale=\"", index) == -1 
                            || str.indexOf(">", index) < str.indexOf("scale=\"", index)) && (str.indexOf("translation=\"", index) == -1 
                            || str.indexOf(">", index) < str.indexOf("translation=\"", index)) && (str.indexOf("rotation=\"", index) == -1 
                            || str.indexOf(">", index) < str.indexOf("rotation=\"", index)) && (str.indexOf("<Transform", index) == -1 
                            || str.indexOf(">", index) < str.indexOf("<Transform", index))) {
                        tranforms.add(tranformGroup);
                        index = str.indexOf(">", index) + 1;
                        openTransform = false;
                    }
                    if (pendingId == 1 || (str.indexOf("rotation=\"", index) > -1 && (str.indexOf("scale=\"", index) == -1 
                            || str.indexOf("rotation=\"", index) < str.indexOf("scale=\"", index)) && (str.indexOf("translation=\"", index) == -1 
                            || str.indexOf("rotation=\"", index) < str.indexOf("translation=\"", index)))) {
                        if (pendingId == 1) {
                            if (str.indexOf("\"", index) > -1) {
                                transform.append(" " + str.substring(index, str.indexOf("\"", index)).trim());
                                pendingId = 0;
                                tranformGroup.add(transform.toString());
                                transform.delete(0, transform.length());
                                index = str.indexOf("\"", index);
                            }
                            else {
                                transform.append(" " + str.substring(index).trim() + " ");
                                pendingId = 1;
                                index = 0;
                                str = in.readLine().trim();
                            }
                        }
                        else {
                            if (str.indexOf("\"", str.indexOf("rotation=\"", index) + "rotation=\"".length()) > -1) {
                                tranformGroup.add("r" + str.substring(str.indexOf("rotation=\"", index) + "rotation=\"".length(), 
                                        str.indexOf("\"", str.indexOf("rotation=\"", index) + "rotation=\"".length())));
                                index = str.indexOf("\"", str.indexOf("rotation=\"", index) + "rotation=\"".length());
                            }
                            else {
                                transform.append("r" + str.substring(str.indexOf("rotation=\"", index) + "rotation=\"".length()).trim() + " ");
                                pendingId = 1;
                                index = 0;
                                str = in.readLine().trim();
                            }
                        }
                    }
                    if (pendingId == 2 || (str.indexOf("scale=\"", index) > -1 && (str.indexOf("rotation=\"", index) == -1 
                            || str.indexOf("scale=\"", index) < str.indexOf("rotation=\"", index)) && (str.indexOf("translation=\"", index) == -1
                            || str.indexOf("scale=\"", index) < str.indexOf("translation=\"", index)))) {
                        if (pendingId == 2) {
                            if (str.indexOf("\"", index) > -1) {
                                transform.append(" " + str.substring(index, str.indexOf("\"", index)).trim());
                                pendingId = 0;
                                tranformGroup.add(transform.toString());
                                transform.delete(0, transform.length());
                                index = str.indexOf("\"", index);
                            }
                            else {
                                transform.append(" " + str.substring(index).trim() + " ");
                                pendingId = 2;
                                index = 0;
                                str = in.readLine().trim();
                            }
                        }
                        else {
                            if (str.indexOf("\"", str.indexOf("scale=\"", index) + "scale=\"".length()) > -1) {
                                tranformGroup.add("s" + str.substring(str.indexOf("scale=\"", index) + "scale=\"".length(), 
                                        str.indexOf("\"", str.indexOf("scale=\"", index) + "scale=\"".length())));
                                index = str.indexOf("\"", str.indexOf("scale=\"", index) + "scale=\"".length());
                            }
                            else {
                                transform.append("s" + str.substring(str.indexOf("scale=\"", index) + "scale=\"".length()).trim() + " ");
                                pendingId = 2;
                                index = 0;
                                str = in.readLine().trim();
                            }   
                        }
                    }
                    if (pendingId == THREE || (str.indexOf("translation=\"", index) > -1 && (str.indexOf("scale=\"", index) == -1 
                                || str.indexOf("translation=\"", index) < str.indexOf("scale=\"", index)) 
                                && (str.indexOf("rotation=\"", index) == -1 
                                || str.indexOf("translation=\"", index) < str.indexOf("rotation=\"", index)))) {
                        if (pendingId == THREE) {
                            if (str.indexOf("\"", index) > -1) {
                                transform.append(" " + str.substring(index, str.indexOf("\"", index)).trim());
                                pendingId = 0;
                                tranformGroup.add(transform.toString());
                                transform.delete(0, transform.length());
                                index = str.indexOf("\"", index);
                            }
                            else {
                                transform.append(" " + str.substring(index).trim() + " ");
                                pendingId = THREE;
                                index = 0;
                                str = in.readLine().trim();
                            }
                        }
                        else {
                            if (str.indexOf("\"", str.indexOf("translation=\"", index) + "translation=\"".length()) > -1) {
                                tranformGroup.add("t" + str.substring(str.indexOf("translation=\"", index) + "translation=\"".length(), 
                                        str.indexOf("\"", str.indexOf("translation=\"", index) + "translation=\"".length())));
                                index = str.indexOf("\"", str.indexOf("translation=\"", index) + "translation=\"".length());
                            }
                            else {
                                transform.append("t" + str.substring(str.indexOf("translation=\"", index) 
                                        + "translation=\"".length()).trim() + " ");
                                pendingId = THREE;
                                index = 0;
                                str = in.readLine().trim();
                            }
                        }
                    }
                }
            }
            x3d.setCoordIndexAttr(coordIndex.toString());
            x3d.setPointAttr(points.toString());
            //TODO borrar
         //   x3d.setNormalAttr(normals.toString());
            
            x3d.setTransforms(tranforms);
            in.close();
            return x3d;
        } 
        catch (IOException e) {
            throw new CGException(CGException.CG00X, "X3D region not valid: " + e.getMessage());
        }
    }
    
    
    
    /**
     * Apply the transformations of the x3d document to the points.
     * @param x3d                   The original x3d
     * @param samraiPoints          The points of the x3d without process
     */
    public static void applyTransformations(X3DRegion x3d, double[][] samraiPoints) {
        //Although no transformation is defined the x3d document have all the identity transformations
        ArrayList<ArrayList<String>> transforms = x3d.getTransforms();
        for (int i = transforms.size() - 1; i >= 0; i--) {
            ArrayList<String> transform = transforms.get(i);
            //The order of transformations for x3d is scale rotation translation
            //scale
            for (int j = transform.size() - 1; j >= 0; j--) {
                String attribute = transforms.get(i).get(j);
                if (attribute.startsWith("s")) {
                    String[] scaleString = attribute.substring(1).split(" +");
                    double[] scale = new double[scaleString.length];
                    for (int k = 0; k < scaleString.length; k++) {
                        scale[k] = Double.parseDouble(scaleString[k]);
                    }
                    CodeGeneratorUtils.applyScale(scale, samraiPoints);
                }
            }
            //rotation
            for (int j = transform.size() - 1; j >= 0; j--) {
                String attribute = transforms.get(i).get(j);
                if (attribute.startsWith("r")) {
                    String[] rotationString = attribute.substring(1).split(" +");
                    double[] rotation = new double[rotationString.length];
                    for (int k = 0; k < rotationString.length; k++) {
                        rotation[k] = Double.parseDouble(rotationString[k]);
                    }
                    CodeGeneratorUtils.applyRotation(rotation, samraiPoints);
                }
            }
            //translation
            for (int j = transform.size() - 1; j >= 0; j--) {
                String attribute = transforms.get(i).get(j);
                if (attribute.startsWith("t")) {
                    String[] translationString = attribute.substring(1).split(" +");
                    double[] translation = new double[translationString.length];
                    for (int k = 0; k < translationString.length; k++) {
                        translation[k] = Double.parseDouble(translationString[k]);
                    }
                    CodeGeneratorUtils.applyTranslation(translation, samraiPoints);
                }
            }
        }
    }
    
    /**
     * Gets the minimum value of the coordinate on the problem.
     * @param problem           The problem
     * @param coordinate        The coordinate
     * @return                  The value
     * @throws CGException      CG00X External error
     */
    public static Element getMinCoordinate(Document problem, String coordinate) throws CGException {
        String query = "/*/mms:region//mms:coordinateLimits[descendant::mms:coordinate = '" 
            + coordinate + "']/mms:coordinateMin|//mms:mesh//mms:coordinateLimits[descendant::mms:coordinate = '" 
            + coordinate + "']/mms:coordinateMin";
        return (Element) find(problem, query).item(0).getFirstChild().getFirstChild();
    }
    
    /**
     * Gets the maximum value of the coordinate on the problem.
     * @param problem           The problem
     * @param coordinate        The coordinate
     * @return                  The value
     * @throws CGException      CG00X External error
     */
    public static Element getMaxCoordinate(Document problem, String coordinate) throws CGException {
        String query = "/*/mms:region/mms:coordinateLimits[descendant::mms:coordinate = '" + coordinate + "']" 
            + "/mms:coordinateMax|//mms:mesh//mms:coordinateLimits[descendant::mms:coordinate = '" + coordinate + "']" 
            + "/mms:coordinateMax";
        return (Element) find(problem, query).item(0).getFirstChild().getFirstChild();
    }
    
    /**
     * Gets the x3d minimum point for the coordinate.
     * @param region           The x3d region
     * @param coordNumber       The coordinate to take the minimum
     * @param points            The points with all the transformations done
     * @return                  The mathml element with the minimum
     */
    private static Element getX3dMin(Document region, int coordNumber, double[][] points) {
        
        //Process point atribute
        double min = points[0][coordNumber];
        for (int i = 1; i < points.length; i++) {
            if (min > points[i][coordNumber]) {
                min = points[i][coordNumber];
            }
        }
        
        //Create the element
        Element math = createElement(region, MTURI, "math");
        if (min < 0) {
            Element apply = createElement(region, MTURI, "apply");
            Element minus = createElement(region, MTURI, "minus");
            Element minElement = createElement(region, MTURI, "ci");
            minElement.setTextContent(String.valueOf(min));
            apply.appendChild(minus);
            apply.appendChild(minElement);
            math.appendChild(apply);
        }
        else {
            Element minElement = createElement(region, MTURI, "ci");
            minElement.setTextContent(String.valueOf(min));
            math.appendChild(minElement);
        }
        return math;
    }
    
    /**
     * Gets the x3d maximum point for the coordinate.
     * @param region           The x3d region
     * @param coordNumber       The coordinate to take the maximum
     * @param points            The points with all the transformations done
     * @return                  The mathml element with the maximum
     */
    private static Element getX3dMax(Document region, int coordNumber, double[][] points) {
        //Process point atribute
        double max = points[0][coordNumber];
        for (int i = 1; i < points.length; i++) {
            if (max < points[i][coordNumber]) {
                max = points[i][coordNumber];
            }
        }
        
        //Create the element
        Element math = createElement(region, MTURI, "math");
        if (max < 0) {
            Element apply = createElement(region, MTURI, "apply");
            Element minus = createElement(region, MTURI, "minus");
            Element maxElement = createElement(region, MTURI, "ci");
            maxElement.setTextContent(String.valueOf(max));
            apply.appendChild(minus);
            apply.appendChild(maxElement);
            math.appendChild(apply);
        }
        else {
            Element maxElement = createElement(region, MTURI, "ci");
            maxElement.setTextContent(String.valueOf(max));
            math.appendChild(maxElement);
        }
        return math;
    }
    
    /**
     * Change temporal variables names for all the regions (Interior and Surface type too).
     * Change analysis variable names to use evolution variables to safe memory (performance)
     * @param doc               The document
     * @param pi                The problem info
     * @param samrai            True samrai, false cactus
     * @throws CGException      CG00X External error
     */
    public static void changeVariableNames(Document doc, ProblemInfo pi, boolean samrai) throws CGException {
    
        LinkedHashMap<String, Integer> fieldTimeLevels = pi.getFieldTimeLevels();
        ArrayList<String> regions = pi.getRegions();
        //Get the fields, auxiliary fields and vectors and add them to the exclusion list for renaming
        HashSet<String> excludedVariables = new HashSet<String>();
        Iterator<String> fieldEnum = fieldTimeLevels.keySet().iterator();
        while (fieldEnum.hasNext()) {
            String field = fieldEnum.next();
            int timeLevel = fieldTimeLevels.get(field);
            field = SAMRAIUtils.variableNormalize(field);
            excludedVariables.add(field);
            for (int i = 1; i < timeLevel; i++) {
                field = field + "_p";
                excludedVariables.add(field);
            }
        }
        //Add also the position variables.
        String[][] xslParams = new String [4][2];
        xslParams[0][0] = "condition";
        xslParams[0][1] = "false";
        xslParams[1][0] = "indent";
        xslParams[1][1] = "0";
        xslParams[2][0] = "dimensions";
        xslParams[2][1] = String.valueOf(pi.getDimensions());
        xslParams[3][0] = "timeCoord";
        xslParams[3][1] = pi.getTimeCoord();
        NodeList leftTerms = CodeGeneratorUtils.find(doc, ".//mt:math[(not(parent::sml:if) or parent::sml:then) and (not(parent::sml:while)" 
                + " or parent::sml:loop) and not(parent::sml:return) and not(ancestor::mms:condition) and "
                + "not(ancestor::mms:finalizationCondition)]/mt:apply/mt:eq[following-sibling::*[1][name() = 'mt:ci' and "
                + "(descendant::sml:particlePosition)]]");
        for (int  j = 0; j < leftTerms.getLength(); j++) {
            String variable = new String(SAMRAIUtils.xmlTransform(leftTerms.item(j).getNextSibling().cloneNode(true), xslParams));
            excludedVariables.add(variable);
        }
        
        ArrayList<Node> newEvolutionTemporalVariables = new ArrayList<Node>();
        //Extrapolation variable change
        ArrayList<Element> auxOriginal = new ArrayList<Element>();
        HashSet<String> addedVariables = new HashSet<String>();
        addedVariables.addAll(excludedVariables);
        String query = ".//mt:math[(not(parent::sml:if) or parent::sml:then) and (not(parent::sml:while) or parent::sml:loop) " 
            + "and not(parent::sml:return) and not(ancestor::mms:applyIf) and not(ancestor::" 
            + "mms:finalizationCondition)]/mt:apply/mt:eq/following-sibling::*[1][not(descendant-or-self::sml:sharedVariable)]";
        Set<Node> terms = CodeGeneratorUtils.findWithoutDuplicates(doc, query);
        Iterator<Node> termsIt = terms.iterator();
        while (termsIt.hasNext()) {
            Node term = termsIt.next();
            String variable = xmlVarToString(term, false, false);
            if (!addedVariables.contains(variable) && !variable.equals("newRegion")) {
                addedVariables.add(variable);
                auxOriginal.add((Element) term.cloneNode(true));
            }
        }
        
        //Make the replacements for every region
        for (int i = 0; i < regions.size(); i++) {
            String regionName = regions.get(i);
            //Get postInitVariables
            ArrayList<String> postInitVars = getInitVars(doc, regionName, samrai);
            //Interior execution flow
            NodeList scope = CodeGeneratorUtils.find(doc, "//*[descendant::" 
                    + "mms:name = '" + regionName + "' and (local-name()='region' or local-name()='subregion')]//*[local-name() = 'interiorExecutionFlow' or local-name() = 'interiorAlgorithm']"
                    + "|//*[mms:name = '" + regionName + "']//mms:postInitialCondition[mms:regionType = 'interior']");
            if (scope.getLength() > 0) {
	            ArrayList<Element> auxVariables = new ArrayList<Element>();
	            addedVariables = new HashSet<String>();
	            addedVariables.addAll(excludedVariables);
	            //Get temporal variables assigned in the code
	            query = ".//mt:math[(not(parent::sml:if) "
	                + "or parent::sml:then) and (not(parent::sml:while) or parent::sml:loop) " 
	                + "and not(parent::sml:return) and not(ancestor::sml:boundary//sml:fieldExtrapolation)]/mt:apply/mt:eq/following-sibling::*[1]" 
	                + "[self::mt:apply and not(descendant::sml:particlePosition)]";
	            for (int j = 0; j < scope.getLength(); j++) {
		            terms = CodeGeneratorUtils.findWithoutDuplicates(scope.item(j), query);
		            termsIt = terms.iterator();
		            while (termsIt.hasNext()) {
		                Node term = termsIt.next();
		                String variable = xmlVarToString(term, false, false);
		                if (variable.indexOf("->") != -1) {
		                    variable = variable.substring(variable.indexOf("->") + 2);
		                }
		                if (!addedVariables.contains(variable) && !variable.equals("newRegion") && !postInitVars.contains(variable)) {
		                    addedVariables.add(variable);
		                    auxVariables.add((Element) term);
		                	if (!hasAncestor(term, "iterateOverParticles")) {
		                		Element tmp = (Element) term.getFirstChild().cloneNode(true);
		                        NodeList cis = CodeGeneratorUtils.find(tmp, "self::*[self::mt:ci]|.//mt:ci");
		    	                boolean added = false;
		    	                for (int k = cis.getLength() - 1; k >= 0 && !added; k--) {
		    	                    if (!cis.item(k).getTextContent().startsWith("(")) {
		    	                        cis.item(k).setTextContent(cis.item(k).getTextContent() + "_" + (2 * i + 1));
		    	                        added = true;
		    	                    }
		    	                }
		                		newEvolutionTemporalVariables.add(tmp.cloneNode(true));
		                	}
		                }
		            }
	            }
	            //Create new variable name
	            for (int j = 0; j < auxVariables.size(); j++) {
	                Element originalCi = (Element) auxVariables.get(j).getFirstChild().cloneNode(true);
	
	                //Create the new variable adding the region number at the end of the variable name
	                Element newCi = (Element) originalCi.cloneNode(true);
	                NodeList cis = CodeGeneratorUtils.find(newCi, "self::*[self::mt:ci]|.//mt:ci");
	                boolean added = false;
	                for (int k = cis.getLength() - 1; k >= 0 && !added; k--) {
	                    if (!cis.item(k).getTextContent().startsWith("(")) {
	                        cis.item(k).setTextContent(cis.item(k).getTextContent() + "_" + (2 * i + 1));
	                        added = true;
	                    }
	                }
	                if (added) {
	                	//Replace all the occurrences of the variable by the new name in the interior execution flow
	                	for (int k = 0; k < scope.getLength(); k++) {
	                		CodeGeneratorUtils.nodeReplace(scope.item(k), originalCi, newCi);
	                	}
	                }
	            }
            }
            //Surface execution flow
            scope = CodeGeneratorUtils.find(doc, "/*/mms:subregions/mms:subregion[descendant::" 
                    + "mms:name = '" + regionName + "']//*[local-name() = 'surfaceExecutionFlow' or local-name() = 'surfaceAlgorithm']"
                    + "//*[mms:name = '" + regionName + "']//mms:postInitialCondition[mms:regionType = 'surface']");
            if (scope.getLength() > 0) {
            	ArrayList<Element> auxVariables = new ArrayList<Element>();
	            addedVariables = new HashSet<String>();
	            addedVariables.addAll(excludedVariables);
	            //Take the temporal variables assigned in the code
	            query = ".//mt:math[(not(parent::sml:if) "
	                + "or parent::sml:then) and (not(parent::sml:while) or parent::sml:loop) and " 
	                + "not(parent::sml:return) and not(ancestor::sml:boundary//sml:fieldExtrapolation) and not(ancestor::mms:applyIf) "
	                + "and not(ancestor::mms:finalizationCondition)]/mt:apply/mt:eq/following-sibling::*[1][self::mt:apply and not(descendant::sml:particlePosition)]";
	            for (int j = 0; j < scope.getLength(); j++) {
		            terms = CodeGeneratorUtils.findWithoutDuplicates(scope.item(j), query);
		            termsIt = terms.iterator();
		            while (termsIt.hasNext()) {
		                Node term = termsIt.next();
		                String variable = xmlVarToString(term, false, false);
		                if (!addedVariables.contains(variable) && !variable.equals("newRegion") && !postInitVars.contains(variable)) {
		                    addedVariables.add(variable);
		                    auxVariables.add((Element) term);
		                	if (!hasAncestor(term, "iterateOverParticles")) {
		                		Element tmp = (Element) term.getFirstChild().cloneNode(true);
		                        NodeList cis = CodeGeneratorUtils.find(tmp, "self::*[self::mt:ci]|.//mt:ci");
		    	                boolean added = false;
		    	                for (int k = cis.getLength() - 1; k >= 0 && !added; k--) {
		    	                    if (!cis.item(k).getTextContent().startsWith("(")) {
		    	                        cis.item(k).setTextContent(cis.item(k).getTextContent() + "_" + (2 * i + 2));
		    	                        added = true;
		    	                    }
		    	                }
		                		newEvolutionTemporalVariables.add(tmp.cloneNode(true));
		                	}
		                }
		            }
	            }
		
	            //Create new variable name
	            for (int j = 0; j < auxVariables.size(); j++) {
	                Element originalCi = (Element) auxVariables.get(j).getFirstChild().cloneNode(true);
	
	                //Create the new variable adding the region number at the end of the variable name
	                Element newCi = (Element) originalCi.cloneNode(true);
	                NodeList cis = CodeGeneratorUtils.find(newCi, "self::*[self::mt:ci]|.//mt:ci");
	                boolean added = false;
	                for (int k = cis.getLength() - 1; k >= 0 && !added; k--) {
	                    if (!cis.item(k).getTextContent().startsWith("(")) {
	                        cis.item(k).setTextContent(cis.item(k).getTextContent() + "_" + (2 * i + 2));
	                        added = true;
	                    }
	                }
	                if (added) {
	                    //Replace all the occurrences of the variable by the new name in the surface execution flow
	                 	for (int k = 0; k < scope.getLength(); k++) {
	                		CodeGeneratorUtils.nodeReplace(scope.item(k), originalCi, newCi);
	                	}
	                }
	            }
            }
                        
            //Interior
            ArrayList<String> incompFields = pi.getHardRegionFields(regionName + "I");
            if (incompFields != null) {
                for (int j = 0; j < incompFields.size(); j++) {
                    String field = incompFields.get(j);
                    query = "//*[(local-name() = 'region' or local-name() = 'subregion') and descendant::mms:name = '" + regionName + "']//" 
                            + "mms:interiorExecutionFlow//sml:boundary[@typeAtt = 'flux']//sml:fieldExtrapolation[@fieldAtt = '" + field + "']"
                            + "|//*[mms:name = '" + regionName + "']//mms:postInitialCondition[mms:regionType = 'interior']//sml:fieldExtrapolation[@fieldAtt = '" + field + "']";
                    NodeList extrapolations = CodeGeneratorUtils.find(doc, query);
                    for (int l = extrapolations.getLength() - 1; l >= 0; l--) {
                        //DocumentFragment newExt = doc.createDocumentFragment();
                        Element extrapol = (Element) extrapolations.item(l);
                        ArrayList<String> interactions = CodeGeneratorUtils.getRegionInteractions(regionName + "I", pi.getHardRegions(), 
                                pi.getRegions());
                        for (int k = 0; k < interactions.size(); k++) {
                            ArrayList<String> hardFields = CodeGeneratorUtils.filterHardFields(regionName + "I", interactions.get(k), 
                                    pi.getHardRegions());
                            String segName = interactions.get(k);
                            if (hardFields.contains(field)
                                    && pi.getRegionIds().contains(String.valueOf(2 * pi.getRegions().indexOf(segName) + 1))) {
                                int segId = 2 * pi.getRegions().indexOf(segName) + 1;
                                //Create new variable name
                                ArrayList<Element> auxVariables = new ArrayList<Element>();
                                auxVariables.addAll(auxOriginal);
                                for (int n = 0; n < auxVariables.size(); n++) {
                                    Element originalCi;
                                    if (auxVariables.get(n).getLocalName().equals("apply")) {
                                        originalCi = (Element) auxVariables.get(n).getFirstChild().cloneNode(true);
                                    }
                                    else {
                                        originalCi = (Element) auxVariables.get(n).cloneNode(true);
                                    }
            
                                    //Create the new variable adding the region number at the end of the variable name
                                    Element newCi = (Element) originalCi.cloneNode(true);
                                    NodeList cis = CodeGeneratorUtils.find(newCi, "self::*[self::mt:ci]|.//mt:ci");
                                    boolean added = false;
                                    for (int m = cis.getLength() - 1; m >= 0 && !added; m--) {
                                        if (!cis.item(m).getTextContent().startsWith("(")) {
                                            cis.item(m).setTextContent(cis.item(m).getTextContent() + "_" + segId);
                                            added = true;
                                        }
                                    }
                                    if (added) {
                                        //Replace all the occurrences of the variable by the new name in the interior execution flow
                                        CodeGeneratorUtils.nodeReplace(extrapol, originalCi, newCi);
                                    }
                                }
                            }
                            if (hardFields.contains(field) 
                                    && pi.getRegionIds().contains(String.valueOf(2 * pi.getRegions().indexOf(segName) + 2))) {
                                int segId = 2 * pi.getRegions().indexOf(segName) + 2;
                                //Create new variable name
                                ArrayList<Element> auxVariables = new ArrayList<Element>();
                                auxVariables.addAll(auxOriginal);
                                for (int n = 0; n < auxVariables.size(); n++) {
                                    Element originalCi;
                                    if (auxVariables.get(n).getLocalName().equals("apply")) {
                                        originalCi = (Element) auxVariables.get(n).getFirstChild().cloneNode(true);
                                    }
                                    else {
                                        originalCi = (Element) auxVariables.get(n).cloneNode(true);
                                    }
            
                                    //Create the new variable adding the region number at the end of the variable name
                                    Element newCi = (Element) originalCi.cloneNode(true);
                                    NodeList cis = CodeGeneratorUtils.find(newCi, "self::*[self::mt:ci]|.//mt:ci");
                                    boolean added = false;
                                    for (int m = cis.getLength() - 1; m >= 0 && !added; m--) {
                                        if (!cis.item(m).getTextContent().startsWith("(")) {
                                            cis.item(m).setTextContent(cis.item(m).getTextContent() + "_" + segId);
                                            added = true;
                                        }
                                    }
                                    if (added) {
                                        //Replace all the occurrences of the variable by the new name in the interior execution flow
                                        CodeGeneratorUtils.nodeReplace(extrapol, originalCi, newCi);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            //Surface
            incompFields = pi.getHardRegionFields(regionName + "S");
            if (incompFields != null) {
                for (int j = 0; j < incompFields.size(); j++) {
                    String field = incompFields.get(j);
                    query = "/*/mms:subregions/mms:subregion[descendant::mms:name = '" + regionName + "']/" 
                            + "mms:surfaceExecutionFlow/sml:boundary[@typeAtt = 'flux']//sml:fieldExtrapolation[@fieldAtt = '" + field + "']"
                            + "|//*[mms:name = '" + regionName + "']//mms:postInitialCondition[mms:regionType = 'surface']//sml:fieldExtrapolation[@fieldAtt = '" + field + "']";
                    NodeList extrapolations = CodeGeneratorUtils.find(doc, query);
                    for (int l = extrapolations.getLength() - 1; l >= 0; l--) {
                        //DocumentFragment newExt = doc.createDocumentFragment();
                        Element extrapol = (Element) extrapolations.item(l);
                        ArrayList<String> interactions = CodeGeneratorUtils.getRegionInteractions(regionName + "S", pi.getHardRegions(), 
                                pi.getRegions());
                        for (int k = 0; k < interactions.size(); k++) {
                            ArrayList<String> hardFields = CodeGeneratorUtils.filterHardFields(regionName + "S", interactions.get(k), 
                                    pi.getHardRegions());
                            String segName = interactions.get(k);
                            if (hardFields.contains(field)
                                    && pi.getRegionIds().contains(String.valueOf(2 * pi.getRegions().indexOf(segName) + 1))) {
                                int segId = 2 * pi.getRegions().indexOf(segName) + 1;
                                //Create new variable name
                                ArrayList<Element> auxVariables = new ArrayList<Element>();
                                auxVariables.addAll(auxOriginal);
                                for (int n = 0; n < auxVariables.size(); n++) {
                                    Element originalCi;
                                    if (auxVariables.get(n).getLocalName().equals("apply")) {
                                        originalCi = (Element) auxVariables.get(n).getFirstChild().cloneNode(true);
                                    }
                                    else {
                                        originalCi = (Element) auxVariables.get(n).cloneNode(true);
                                    }
            
                                    //Create the new variable adding the region number at the end of the variable name
                                    Element newCi = (Element) originalCi.cloneNode(true);
                                    NodeList cis = CodeGeneratorUtils.find(newCi, "self::*[self::mt:ci]|.//mt:ci");
                                    boolean added = false;
                                    for (int m = cis.getLength() - 1; m >= 0 && !added; m--) {
                                        if (!cis.item(m).getTextContent().startsWith("(")) {
                                            cis.item(m).setTextContent(cis.item(m).getTextContent() + "_" + segId);
                                            added = true;
                                        }
                                    }
                                    if (added) {
                                        //Replace all the occurrences of the variable by the new name in the interior execution flow
                                        CodeGeneratorUtils.nodeReplace(extrapol, originalCi, newCi);
                                    }
                                }
                            }
                            if (hardFields.contains(field)
                                    && pi.getRegionIds().contains(String.valueOf(2 * pi.getRegions().indexOf(segName) + 2))) {
                                int segId = 2 * pi.getRegions().indexOf(segName) + 2;
                                //Create new variable name
                                ArrayList<Element> auxVariables = new ArrayList<Element>();
                                auxVariables.addAll(auxOriginal);
                                for (int n = 0; n < auxVariables.size(); n++) {
                                    Element originalCi;
                                    if (auxVariables.get(n).getLocalName().equals("apply")) {
                                        originalCi = (Element) auxVariables.get(n).getFirstChild().cloneNode(true);
                                    }
                                    else {
                                        originalCi = (Element) auxVariables.get(n).cloneNode(true);
                                    }
            
                                    //Create the new variable adding the region number at the end of the variable name
                                    Element newCi = (Element) originalCi.cloneNode(true);
                                    NodeList cis = CodeGeneratorUtils.find(newCi, "self::*[self::mt:ci]|.//mt:ci");
                                    boolean added = false;
                                    for (int m = cis.getLength() - 1; m >= 0 && !added; m--) {
                                        if (!cis.item(m).getTextContent().startsWith("(")) {
                                            cis.item(m).setTextContent(cis.item(m).getTextContent() + "_" + segId);
                                            added = true;
                                        }
                                    }
                                    if (added) {
                                        //Replace all the occurrences of the variable by the new name in the interior execution flow
                                        CodeGeneratorUtils.nodeReplace(extrapol, originalCi, newCi);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        //Change analysis variable names to use evolution variables to safe memory (performance)
        NodeList scope = CodeGeneratorUtils.find(doc, "/*/mms:analysis");
        if (scope.getLength() > 0) {
            addedVariables.clear();
            addedVariables.addAll(pi.getAnalysisFields());
            query = ".//mt:math[(not(parent::sml:if) or parent::sml:then) and (not(parent::sml:while) or parent::sml:loop)]"
            		+ "/mt:apply/mt:eq/following-sibling::*[1][mt:ci]";
            terms = CodeGeneratorUtils.findWithoutDuplicates(scope.item(0), query);
            termsIt = terms.iterator();
            int counter = 0;
            while (termsIt.hasNext()) {
                Node term = termsIt.next();
                String variable = xmlVarToString(term, false, false);
                if (!addedVariables.contains(variable) && counter < newEvolutionTemporalVariables.size()) {
                    Element originalCi = (Element) term.getFirstChild().cloneNode(true);
                    CodeGeneratorUtils.nodeReplace(scope.item(0), originalCi, newEvolutionTemporalVariables.get(counter));
                    counter++;
                }
            }
        }
    }
  
  
    /**
     * Obtains the initialized variables. Including the post-initialization variables.
     * @param problem           The problem
     * @param regionName       The region name
     * @param samrai            A samrai code
     * @return                  The variables
     * @throws CGException      CG00X External error
     */
    public static ArrayList<String> getInitVars(Document problem, String regionName, boolean samrai) throws CGException {
        ArrayList<String> vars = new ArrayList<String>();
        //Take the temporal variables assigned in the code
        String query = "//*[(local-name() = 'region' or local-name() = 'subregion') and descendant::mms:name = '" + regionName + "']//" 
            + "mms:initialCondition//mt:math[(not(parent::sml:if) or parent::sml:then) and (not(parent::sml:while) or parent::sml:loop) " 
            + "and not(parent::sml:return)]/mt:apply/mt:eq[not(following-sibling::*[1] = preceding-sibling::mt:math/mt:apply/mt:eq/" 
            + "following-sibling::*[1][name()='mt:apply'])]/following-sibling::*[1][not(descendant-or-self::sml:sharedVariable)]";
        Set<Node> terms = CodeGeneratorUtils.findWithoutDuplicates(problem, query);
        Iterator<Node> termsIt = terms.iterator();
        while (termsIt.hasNext()) {
            String variable = "";
            if (samrai) {
                variable = new String(SAMRAIUtils.xmlTransform(termsIt.next(), null));
            }
            else {
                variable = new String(CactusUtils.xmlTransform(termsIt.next(), null));
            }
            //Take only the identifier. The indexes will be ignored
            if (variable.indexOf("(") != -1) {
                variable = variable.substring(0, variable.indexOf("("));
            }
            if (!vars.contains(variable)) {
                vars.add(variable);
            }
        }
        
        return vars;
    }
    
    /**
     * Gets the number of execution flow blocks for all the regions.
     * @param doc           The document
     * @param query         The query to search
     * @return              The number of blocks
     * @throws CGException  CG00X External error
     */
    public static int getBlocksNum(Document doc, String query) throws CGException {
        int max = 0;
        NodeList blocks = find(doc, query);
        for (int i = 0; i < blocks.getLength(); i++) {
            if (blocks.item(i).getChildNodes().getLength() > max) {
                max = blocks.item(i).getChildNodes().getLength();
            }
        }
        return max;
    }
    
    /**
     * Gets the region relations from the problem.
     * @param doc           The problem
     * @param regions      The regions of the problem
     * @return              The region relation in a hashmap
     * @throws CGException  CG00X External error
     */
    public static LinkedHashMap<String, LinkedHashMap<String, String>> getRegionRelations(Document doc, ArrayList<String> regions) 
            throws CGException {
        LinkedHashMap<String, LinkedHashMap<String, String>> regionRelation = new LinkedHashMap<String, LinkedHashMap<String, String>>();
        for (int i = 0; i < regions.size(); i++) {
            String regionName = regions.get(i);
            LinkedHashMap<String, String> relations = new LinkedHashMap<String, String>();
            NodeList relationList = find(doc, "/*/mms:regionRelations/mms:regionRelation[mms:regionGroupA = '" + regionName + "']"
                    + "|/*/mms:regionRelations/mms:regionRelation[mms:regionGroupB = '" + regionName + "']");
            for (int j = 0; j < relationList.getLength(); j++) {
                String nameA = relationList.item(j).getFirstChild().getTextContent();
                String nameB = relationList.item(j).getFirstChild().getNextSibling().getTextContent();
                String relation = relationList.item(j).getLastChild().getTextContent();
                if (nameA.equals(regionName)) {
                    relations.put(nameB, relation);
                }
                else {
                    relations.put(nameA, relation);
                }
            }
            regionRelation.put(regionName, relations);
        }
        return regionRelation;
    }
    
    /**
     * Checks if a number is odd.
     * @param number    The number to check
     * @return          True if odd.
     */
    public static boolean isOdd(int number) {
        if (number % 2 != 0)  {
            return true; 
        }
		return false;
    }
    
    /**
     * Replaces the element coordinate by an expression within a given scope.
     * @param scope             The place to search
     * @param problem           The problem document
     * @param expression        The expression to create
     * @param coordinate        The spatial coordinate to replace
     * @throws CGException      CG00X External error
     */
    public static void replaceExpressionInCi(Element scope, Document problem, String expression, String coordinate) throws CGException {
        try {
            NodeList uses = CodeGeneratorUtils.find(scope, ".//mt:ci[text() = '" + coordinate + "']");
            Element point = CodeGeneratorUtils.createElement(problem, MTURI, "ci");
            point.setTextContent(expression);
            for (int j = uses.getLength() - 1; j >= 0; j--) {
                uses.item(j).getParentNode().replaceChild(point.cloneNode(true), uses.item(j));
            }
        } 
        catch (Exception e) {
            throw new CGException(CGException.CG00X, e.getMessage());
        }
    }
        
    /**
     * Get the stencil of every region.
     * @param problem           The problem with the information
     * @param regions          The regions of the problem
     * @return                  The structure with the resulting information
     * @throws CGException      CG00X External error
     */
    public static LinkedHashMap<String, Integer> getRegionStencil(Document problem, ArrayList<String> regions) 
        throws CGException {
        LinkedHashMap<String, Integer> stencils = new LinkedHashMap<String, Integer>();
        
        for (int i = 0; i < regions.size(); i++) {
            String regionName = regions.get(i);
            String query = "/*/mms:region[mms:name[text() = '" + regionName + "']]//mms:stencil|"
            		+ "/*/mms:subregions/mms:subregion[mms:name[text() = '" + regionName + "']]//mms:stencil";
            stencils.put(regionName, Integer.parseInt(find(problem, query).item(0).getTextContent()));
        }
        return stencils;
    }
    
    /**
     * Gets the maximum stencil from the discretization schemas.
     * @param regionStencils       The list of schemas stencils
     * @return                      The maximum
     */
    public static int getMaxSchemasStencil(LinkedHashMap<String, Integer> regionStencils) {
        int maxStencil = 0;
        Iterator<String> it = regionStencils.keySet().iterator();
        while (it.hasNext()) {
            String region = it.next();
            int stencil = regionStencils.get(region);
            if (stencil > maxStencil) {
                maxStencil = stencil;
            }
        }
        return maxStencil;
    }
    
    /**
     * Check if the function could be a macro function.
     * The requisite is to have only a return child
     * @param function      The function
     * @return              True if is a macro function
     * @throws CGException  CG00X External error
     */
    public static boolean isMacroFunction(Element function) throws CGException {
        if (function.getLastChild().getFirstChild().getChildNodes().getLength() > 1) {
            return false;
        }
        if (function.getLastChild().getFirstChild().getFirstChild().getLocalName().equals("return") 
                && CodeGeneratorUtils.find(function, ".//mt:piecewise").getLength() == 0) {
            return true;   
        }
		return false;
    }
    
    /**
     * Applies the scale factors to the points.
     * @param scaleFactor   the scale factor
     * @param points        the points
     */
    public static void applyScale(double[] scaleFactor, double[] points) {
        for (int i = 0; i < points.length / scaleFactor.length; i++) {
            for (int j = 0; j < scaleFactor.length; j++) {
                points[i + j * points.length / scaleFactor.length] = points[i + j * points.length / scaleFactor.length] * scaleFactor[j];
            }
        }
    }
    
    /**
     * Applies the translation factors to the points.
     * @param translationFactor     the translation factor
     * @param points                the points
     */
    public static void applyTranslation(double[] translationFactor, double[] points) {
        for (int i = 0; i < points.length / translationFactor.length; i++) {
            for (int j = 0; j < translationFactor.length; j++) {
                points[i + j * points.length / translationFactor.length] = 
                    points[i + j * points.length / translationFactor.length] + translationFactor[j];
            }
        }
    }
   
    /**
     * Applies the rotation factors to the points.
     * @param rotationFactor        the rotation factor
     * @param points                the points
     */
    public static void applyRotation(double[] rotationFactor, double[] points) {
        final int three = 3;
        int pointNumber = rotationFactor.length - 1;
        double angle = rotationFactor[rotationFactor.length - 1];
        if (pointNumber == 2) {
            //Rotation in axis x
            if (rotationFactor[0] == 1 || rotationFactor[0] == -1) {
                //y' = ycos(a)
                for (int i = 0; i < points.length / pointNumber; i++) {
                    points[i + points.length / pointNumber] = points[i + points.length / pointNumber] * Math.cos(rotationFactor[0] * angle);
                }
            }
            //Rotation in axis y
            if (rotationFactor[1] == 1 || rotationFactor[1] == -1) {
                //x' = xcos(a)
                for (int i = 0; i < points.length / pointNumber; i++) {
                    points[i] = points[i] * Math.cos(rotationFactor[1] * angle);
                }
            }
        }
        if (pointNumber == three) {
            //Rotation in axis x
            if (rotationFactor[0] == 1 || rotationFactor[0] == -1) {
                for (int i = 0; i < points.length / pointNumber; i++) {
                    //y' = ycos(a) - zsin(a)
                    //z' = zcos(a) + ysin(a)
                    double y = points[i + points.length / pointNumber];
                    double z = points[i + 2 * points.length / pointNumber];
                    points[i + points.length / pointNumber] = y * Math.cos(rotationFactor[0] * angle) - z * Math.sin(rotationFactor[0] * angle);
                    points[i + 2 * points.length / pointNumber] = z * Math.cos(rotationFactor[0] * angle) + y * Math.sin(rotationFactor[0] * angle);
                }
            }
            //Rotation in axis y
            if (rotationFactor[1] == 1 || rotationFactor[1] == -1) {
                //x' = xcos(a) + zsin(a)
                //z' = zcos(a) - xsin(a)
                for (int i = 0; i < points.length / pointNumber; i++) {
                    double x = points[i];
                    double z = points[i + 2 * points.length / pointNumber];
                    points[i] = x * Math.cos(rotationFactor[1] * angle) + z * Math.sin(rotationFactor[1] * angle);
                    points[i + 2 * points.length / pointNumber] = z * Math.cos(rotationFactor[1] * angle) - x * Math.sin(rotationFactor[1] * angle);
                }
            }
            //Rotation in axis z
            if (rotationFactor[2] == 1 || rotationFactor[2] == -1) {
                //x' = xcos(a) - ysin(a)
                //y' = ycos(a) + xsin(a)
                for (int i = 0; i < points.length / pointNumber; i++) {
                    double x = points[i];
                    double y = points[i + points.length / pointNumber];
                    points[i] = x * Math.cos(rotationFactor[2] * angle) - y * Math.sin(rotationFactor[2] * angle);
                    points[i + points.length / pointNumber] = y * Math.cos(rotationFactor[2] * angle) + x * Math.sin(rotationFactor[2] * angle);
                } 
            }
        }
    }
    
    /**
     * Applies the scale factors to the points.
     * @param scaleFactor   the scale factor
     * @param points        the points
     */
    public static void applyScale(double[] scaleFactor, double[][] points) {
        for (int i = 0; i < points.length; i++) {
            for (int j = 0; j < scaleFactor.length; j++) {
                points[i][j] = points[i][j] * scaleFactor[j];
            }
        }
    }
    
    /**
     * Applies the translation factors to the points.
     * @param translationFactor     the translation factor
     * @param points                the points
     */
    public static void applyTranslation(double[] translationFactor, double[][] points) {
        for (int i = 0; i < points.length; i++) {
            for (int j = 0; j < translationFactor.length; j++) {
                points[i][j] = points[i][j] + translationFactor[j];
            }
        }
    }
    
    /**
     * Applies the rotation factors to the points.
     * @param rotationFactor        the rotation factor
     * @param points                the points
     */
    public static void applyRotation(double[] rotationFactor, double[][] points) {
        final int three = 3;
        int pointNumber = rotationFactor.length - 1;
        double angle = rotationFactor[rotationFactor.length - 1];
        if (pointNumber == 2) {
            double cos = Math.cos(angle);
            double sin = Math.sin(angle);
            double ux = rotationFactor[0];
            double uy = rotationFactor[1];
            for (int i = 0; i < points.length; i++) {
                double x = points[i][0];
                double y = points[i][1];
                points[i][0] = ux + (x - ux) * cos - (y - uy) * sin;
                points[i][1] = uy + (x - ux) * sin + (y - uy) * cos;
            }
        }
        if (pointNumber == three) {
            //Rotation in arbitrary axis
            double cos = Math.cos(angle);
            double sin = Math.sin(angle);
            double ux = rotationFactor[0];
            double uy = rotationFactor[1];
            double uz = rotationFactor[2];
            for (int i = 0; i < points.length; i++) {
                //http://en.wikipedia.org/wiki/Rotation_matrix#Rotation_matrix_from_axis_and_angle
                double x = points[i][0];
                double y = points[i][1];
                double z = points[i][2];
                points[i][0] = x * (cos + ux * ux * (1 - cos)) + y * (ux * uy * (1 - cos) - uz * sin) +  z * (ux * uz * (1 - cos) + uy * sin);
                points[i][1] = x * (uy * ux * (1 - cos) + uz * sin) + y * (cos + uy * uy * (1 - cos)) +  z * (uy * uz * (1 - cos) - ux * sin);
                points[i][2] = x * (uz * ux * (1 - cos) - uy * sin) + y * (uz * uy * (1 - cos) + ux * sin) + z * (cos + uz * uz * (1 - cos));
            }
        }
    }
    
    /**
     * Replace the continuous coordinates of the coordinate limits with the discrete coordinates.
     * @param doc               The problem
     * @throws CGException      CG00X External error
     */
    public static void replaceDefinitions(Document doc) throws CGException {
        NodeList coordinates = find(doc, "//mms:coordinateLimits/mms:coordinate");
        for (int i = 0; i < coordinates.getLength(); i++) {
            String discCoord = find(doc, "/*/mms:implicitParameters/mms:implicitParameter[mms:value = '\u0394" 
                    + coordinates.item(i).getTextContent() + "']/mms:coordinate").item(0).getTextContent();
            coordinates.item(i).setTextContent(discCoord);
        }
    }
    
    /**
     * Gets the continuous name of the discrete coordinate.
     * @param doc               The problem
     * @param discCoord         The discrete coordinate
     * @return                  The discrete coordinate
     * @throws CGException      CGException - CG00X External error
     */
    public static String discToCont(Document doc, String discCoord) throws CGException {
        return CodeGeneratorUtils.find(doc, "/*/mms:implicitParameters/mms:implicitParameter[mms:coordinate = '" 
                + discCoord + "']/mms:value").item(0).getTextContent().substring(1);
    }
    
    /**
     * Gets the continuous name of the discrete coordinate.
     * @param doc               The problem
     * @param contCoord         The continuous coordinate
     * @return                  The discrete coordinate
     * @throws CGException      CG00X External error
     */
    public static String contToDisc(Document doc, String contCoord) throws CGException {
        return CodeGeneratorUtils.find(doc, "/*/mms:implicitParameters/mms:implicitParameter[mms:value = '\u0394" 
                + contCoord + "']/mms:coordinate").item(0).getTextContent();
    }
    
    /**
     * Add implicit parameters for the abm and ca_on_graph problems.
     * @param doc               The problem
     * @throws CGException      CG00X External error
     */
    public static void addImplicitParameters(Document doc) throws CGException {
        NodeList coordList =  find(doc, "/*/mms:coordinates/mms:timeCoordinate|/*/mms:coordinates/mms:spatialCoordinates/mms:spatialCoordinate");
        for (int i = 0; i < coordList.getLength(); i++) {
            String coord = coordList.item(i).getTextContent();
            Node implicitParameter = createElement(doc, MMSURI, "implicitParameter");
            Element mesh = (Element) find(doc, "//mms:mesh").item(0);
            doc.getDocumentElement().insertBefore(implicitParameter, mesh);
            //Type
            Node ipType = createElement(doc, MMSURI, "type");
            if (coordList.item(i).getLocalName().equals("timeCoordinate")) {
                ipType.setTextContent("delta_time");
            } 
            else {
                ipType.setTextContent("delta_space");
            }
            implicitParameter.appendChild(ipType.cloneNode(true));
            //Name
            Node ipValue = createElement(doc, MMSURI, "value");
            ipValue.setTextContent("\u0394" + coord);
            implicitParameter.appendChild(ipValue.cloneNode(true));
            //Coordinate
            Node ipCoord = createElement(doc, MMSURI, "coordinate");
            ipCoord.setTextContent(coord);
            implicitParameter.appendChild(ipCoord.cloneNode(true));
        }
    }
    
    /**
     * Increments the indentation of a code.
     * @param block     The code block
     * @param number    The number of indents
     * @return          The new code
     */
    public static String incrementIndent(String block, int number) {
    	if (block.length() == 0) {
    		return "";
    	}
        String addingIndent = "";
        for (int i = 0; i < number; i++) {
            addingIndent = addingIndent + IND;
        }
        block = block.replaceAll(NL, NL + addingIndent);
         
        return addingIndent + block.substring(0, block.length() - number);
    }
    
    /**
     * Checks if a string could be converted to a double.
     * @param in        The input string
     * @return          The checking
     */
    public static boolean doubleable(String in) {
        try {
            Double.valueOf(in);
        }
        catch (Exception e) {
            return false;
        }
        return true;
    }
    
    /**
     * Replaces the fields from a mathml to use the complete structure.
     * @param doc               The problem
     * @param mathml            The mathml to change
     * @param coords            The coordinates of the problem
     * @return                  The base element for the boundaries
     * @throws CGException      CG00X External error
     */
    public static Node replaceFields(Document doc, Node mathml, ArrayList<String> coords) throws CGException {
        //Get the coordinate information
        DocumentFragment coordinates = doc.createDocumentFragment();
        for (int k = 0; k < coords.size(); k++) {
            Element coordinate = CodeGeneratorUtils.createElement(doc, MTURI, "ci");
            coordinate.setTextContent(coords.get(k));
            coordinates.appendChild(coordinate);
        }
        String timeCoord = CodeGeneratorUtils.find(doc, "/*/mms:coordinates/mms:timeCoordinate").item(0).getTextContent();
        
        //Replace all the fields
        NodeList fieldList = CodeGeneratorUtils.find(doc, "/*/mms:fields/mms:field|/*/mms:auxiliaryField/mms:auxiliaryField|/*/mms:vectorName");
        for (int i = 0; i < fieldList.getLength(); i++) {
            //Creates the complete structure for field
            Element ci = CodeGeneratorUtils.createElement(doc, MTURI, "ci");
            Element ciField = CodeGeneratorUtils.createElement(doc, MTURI, "ci");
            ciField.setTextContent(fieldList.item(i).getTextContent());
            Element fieldApply = CodeGeneratorUtils.createElement(doc, MTURI, "apply");
            Element msup = CodeGeneratorUtils.createElement(doc, MTURI, "msup");
            Element timeApply = CodeGeneratorUtils.createElement(doc, MTURI, "apply");
            Element plus = CodeGeneratorUtils.createElement(doc, MTURI, "plus");
            Element time = CodeGeneratorUtils.createElement(doc, MTURI, "ci");
            time.setTextContent("(" + timeCoord + ")");
            Element one = CodeGeneratorUtils.createElement(doc, MTURI, "cn");
            one.setTextContent("1");
            fieldApply.appendChild(ci);
            ci.appendChild(msup);
            msup.appendChild(ciField);
            msup.appendChild(timeApply);
            timeApply.appendChild(plus);
            timeApply.appendChild(time);
            timeApply.appendChild(one);
            fieldApply.appendChild(coordinates.cloneNode(true));
            //Replace all the occurrences
            NodeList occurrences = CodeGeneratorUtils.find(mathml, ".//mt:ci[text() = '" + fieldList.item(i).getTextContent() + "']"
                + "|self::mt:ci[text() = '" + fieldList.item(i).getTextContent() + "']");
            for (int j = occurrences.getLength() - 1; j >= 0; j--) {
                if (occurrences.item(j).getParentNode() == null) {
                    mathml = fieldApply.cloneNode(true);
                }
                else {
                    occurrences.item(j).getParentNode().replaceChild(fieldApply.cloneNode(true), occurrences.item(j));
                }
            }
        }
        return mathml;
    }
   
    /**
     * Creates an structure to store the fields not available from a region if it have hard region boundaries.
     * @param problem           The problem
     * @param coords            the spatial coordinates
     * @return                  The structure
     * @throws CGException      CG00X External error
     */
    public static HashMap<String, ArrayList<String>> getHardRegions(Document problem, ArrayList<String> coords) throws CGException {
        HashMap<String, ArrayList<String>> hardRegions = new HashMap<String, ArrayList<String>>();
        NodeList fields = find(problem, "/*/mms:fields/mms:field|/*/mms:auxiliaryFields/mms:auxiliaryField");
        //Regions extrapolations
        NodeList regions = find(problem, "/*/mms:region|/*/mms:subregions/mms:subregion");
        for (int i = 0; i < regions.getLength(); i++) {
            String regionName = regions.item(i).getFirstChild().getTextContent() + "I";
            ArrayList<String> hardFields = new ArrayList<String>();
            for (int j = 0; j < fields.getLength(); j++) {
                if (find(regions.item(i), ".//mms:interiorExecutionFlow//sml:fieldExtrapolation[@fieldAtt='" 
                        + fields.item(j).getTextContent() + "' and not(parent::sml:side[@sideAtt='lower'] or "
                        + "parent::sml:side[@sideAtt='upper'])]").getLength() > 0) {
                    hardFields.add(fields.item(j).getTextContent());
                }
            }
            if (hardFields.size() > 0) {
                hardRegions.put(regionName, hardFields);
            }
            regionName = regions.item(i).getFirstChild().getTextContent() + "S";
            hardFields = new ArrayList<String>();
            for (int j = 0; j < fields.getLength(); j++) {
                if (find(regions.item(i), ".//mms:surfaceExecutionFlow//sml:fieldExtrapolation[@fieldAtt='" 
                        + fields.item(j).getTextContent() + "' and not(parent::sml:side[@sideAtt='lower'] or "
                        + "parent::sml:side[@sideAtt='upper'])]").getLength() > 0) {
                    hardFields.add(fields.item(j).getTextContent());
                }
            }
            if (hardFields.size() > 0) {
                hardRegions.put(regionName, hardFields);
            }
        }
        //Boundary extrapolations
        for (int i = 0; i < coords.size(); i++) {
            String axis = discToCont(problem, coords.get(i));
            //Lower
            ArrayList<String> hardFields = new ArrayList<String>();
            NodeList fieldExtrapols = find(problem, ".//sml:fieldExtrapolation[parent::sml:side[@sideAtt='lower'] and "
                    + "ancestor::sml:axis[@axisAtt='" + coords.get(i) + "']]");
            for (int j = 0; j < fieldExtrapols.getLength(); j++) {
                if (!hardFields.contains(((Element) fieldExtrapols.item(j)).getAttribute("fieldAtt"))) {
                    hardFields.add(((Element) fieldExtrapols.item(j)).getAttribute("fieldAtt"));
                }
            }
            if (hardFields.size() > 0) {
                hardRegions.put(axis + "-Lower", hardFields);
            }
            //Upper
            hardFields = new ArrayList<String>();
            fieldExtrapols = find(problem, ".//sml:fieldExtrapolation[parent::sml:side[@sideAtt='upper'] and "
                    + "ancestor::sml:axis[@axisAtt='" + coords.get(i) + "']]");
            for (int j = 0; j < fieldExtrapols.getLength(); j++) {
                if (!hardFields.contains(((Element) fieldExtrapols.item(j)).getAttribute("fieldAtt"))) {
                    hardFields.add(((Element) fieldExtrapols.item(j)).getAttribute("fieldAtt"));
                }
            }
            if (hardFields.size() > 0) {
                hardRegions.put(axis + "-Upper", hardFields);
            }
        }
        return hardRegions;
    }
   
    
    /**
     * Filters the fields that have to be calculated in the extrapolation of a concrete hard region interaction.
     * @param regionName       The region where to calculate the fields
     * @param destSegName       The related region with hard boundary
     * @param hardRegions      The hard region boundaries
     * @return                  The filtered fields
     */
    public static ArrayList<String> filterHardFields(String regionName, String destSegName, HashMap<String, ArrayList<String>> hardRegions) {
        ArrayList<String> fieldsA = new ArrayList<String>();
        fieldsA.addAll(hardRegions.get(regionName));
        ArrayList<String> fieldsB = new ArrayList<String>();
        
        if (hardRegions.containsKey(destSegName + "I")) {
            fieldsB.addAll(hardRegions.get(destSegName + "I"));
        }
        if (hardRegions.containsKey(destSegName + "S")) {
            fieldsB.addAll(hardRegions.get(destSegName + "S"));
        }
       
        fieldsA.removeAll(fieldsB);
        return fieldsA;
    }
    
    /**
     * Gets the regions that have hard interactions with the given one.
     * @param regionName        The region given
     * @param hardRegions       All the hard interactions
     * @param regions           The regions of the problem
     * @return                  The list of regions
     */
    public static ArrayList<String> getRegionInteractions(String regionName, HashMap<String, ArrayList<String>> hardRegions,
            ArrayList<String> regions) {
        ArrayList<String> regionInteractions = new ArrayList<String>();
        ArrayList<String> fieldsA = hardRegions.get(regionName);
        for (int i = 0; i < regions.size(); i++) {
            String region = regions.get(i);
            if (!regionName.equals(region)) {
                if (!hardRegions.containsKey(region)) {
                    regionInteractions.add(region);
                }
                else {
                    ArrayList<String> fieldsB = hardRegions.get(region);
                    if (!(fieldsB.containsAll(fieldsA))) {
                        regionInteractions.add(region);
                    }
                }
            }
        }
        return regionInteractions;
    }
  
    /**
     * Get the region identifiers (including boundaries) for all the regions from the region groups.
     * @param doc               	The problem
     * @param regions     			The region groups
     * @param boundaryPrecedence	The boundary precedence
     * @param pi                	The problem information
     * @return                  	The ids
     * @throws CGException      	CG00X External error
     */
    public static ArrayList<String> getRegionIds(Document doc, ArrayList<String> regions, ArrayList<String> boundaryPrecedence, ProblemInfo pi) throws CGException {
        ArrayList<String> regionIds = new ArrayList<String>();
        for (int j = 0; j < regions.size(); j++) {
            String regionName = regions.get(j);
            boolean hasInterior = CodeGeneratorUtils.find(doc, "//mms:interiorExecutionFlow[preceding-sibling::mms:name = '"
                    + regionName + "']").getLength() > 0;
            if (hasInterior) {
                regionIds.add(String.valueOf(j * 2 + 1));
            }
            boolean hasSurface = CodeGeneratorUtils.find(doc, "//mms:surfaceExecutionFlow[preceding-sibling::mms:name = '" 
                    + regionName + "']").getLength() > 0;
            if (hasSurface) {
                regionIds.add(String.valueOf(j * 2 + 2));
            }
        }
        
        //ABM problems only have one region
        if (pi.isHasABMMeshFields() || pi.isHasABMMeshlessFields() || pi.isHasGraphFields()) {
            regionIds.add("1");
        }
        
        //Boundaries
        for (int i = 0; i < boundaryPrecedence.size(); i++) {
            String boundName = boundaryPrecedence.get(i);
            String axis = boundName.substring(0, boundName.lastIndexOf("-"));
            String side = boundName.substring(boundName.lastIndexOf("-") + 1);
            boundName = axis + side;
            regionIds.add(boundName);
        }
        return regionIds;
    }
    
    /**
     * Gets the identifier of the region from its name.
     * @param doc               The document
     * @param regions     The regions
     * @param regionName       The region name
     * @return                  The region id
     * @throws CGException      CG00X External error
     */
    public static String getRegionId(Document doc, ArrayList<String> regions, String regionName) throws CGException {
        for (int j = 0; j < regions.size(); j++) {
            String segName = regions.get(j);
            if (segName.equals(regionName)) {
                boolean hasSurface = CodeGeneratorUtils.find(doc, "//mms:surfaceExecutionFlow[preceding-sibling::mms:name = '" 
                        + regionName + "']").getLength() > 0;
                if (hasSurface) {
                    return String.valueOf(j * 2 + 2);
                }
                boolean hasInterior = CodeGeneratorUtils.find(doc, "//mms:interiorExecutionFlow[preceding-sibling::mms:name = '" 
                        + regionName + "']").getLength() > 0;
                if (hasInterior) {
                    return String.valueOf(j * 2 + 1);
                }
            }
        }
        //If not a interior region the region id is the name of the boundary
        return regionName;
    }
    
    /**
     * Gets the extrapolation variables.
     * @param pi            	ProblemInfo
     * @return                  The variables
     * @throws CGException      CG003 Not possible to create code for the platform
     *                          CG00X External error
     */
    public static ArrayList<String> getExtrapolVars(ProblemInfo pi) throws CGException {
        ArrayList<String> vars = new ArrayList<String>();
    	LinkedHashMap<String, ArrayList<String>> groups = pi.getExtrapolatedFieldGroups();
    	if (pi.getExtrapolatedFieldGroups() != null) {
        	Iterator<String> groupsIt = groups.keySet().iterator();
        	while (groupsIt.hasNext()) {
         		String groupName = SAMRAIUtils.variableNormalize(groupsIt.next());
                for (int j = 0; j < pi.getCoordinates().size(); j++) {
                    vars.add("d_" + pi.getCoordinates().get(j) + "_" + groupName);
                }		
        	}
    	}
        return vars;
    }
    
    /**
     * Obtain the variable information from the problem.
     * @param pi				The problem information
     * @return					Variable information
     * @throws CGException		CG00X - External error
     */
    public static VariableInfo getVariableInfo(ProblemInfo pi) throws CGException {
    	HashMap<String, ArrayList<String>> particleVariables = new HashMap<String, ArrayList<String>>();
    	HashMap<String, ArrayList<String>> particleFields = new HashMap<String, ArrayList<String>>();
    	HashMap<String, ArrayList<String>> particlePositions = new HashMap<String, ArrayList<String>>();
    	Set<String> allParticlePositions = new HashSet<String>();
        //Insert positions if particles
        if (pi.isHasParticleFields()) {
        	for (String species : pi.getParticleSpecies()) {
        		particleVariables.put(species, getParticleVariables(pi.getProblem(), pi.getFieldTimeLevels(), species));
        		particleFields.put(species, getParticleFields(pi.getProblem(), species));
                if (pi.isHasParticleFields()) {
                	particlePositions.put(species, SAMRAIUtils.getPositionVariables(pi.getProblem(), pi, species));
        		}
                if (pi.isHasABMMeshlessFields()) {
                	particlePositions.put(species, SAMRAIUtils.getPositionVariablesFromCoords(pi.getProblem(), pi));
                }
                allParticlePositions.addAll(particlePositions.get(species));
        	}
        }
    	return new VariableInfo(CodeGeneratorUtils.getMeshVariables(pi.getProblem(), pi, allParticlePositions), particleFields, particleVariables, particlePositions);
    }
    
    /**
     * Gets the variables stored in particles.
     * @param problem		The problem
     * @param ftl			Field time levels
     * @param species		Species
     * @return				Particle variables
     * @throws CGException	CG00X - External error
     */
    private static ArrayList<String> getParticleVariables(Document problem, LinkedHashMap<String, Integer> ftl, String species) throws CGException {
    	ArrayList<String> variables = new ArrayList<String>();
        //Get the variables assigned in the code
        String query = ".//mt:math[(not(parent::sml:if) or parent::sml:then) and (not(parent::sml:while) or parent::sml:loop) and not(parent::sml:return) " 
                + "and not(ancestor::mms:applyIf) and not(ancestor::mms:finalizationCondition) and ancestor::sml:iterateOverParticles[@speciesNameAtt = '" + species + "']]/mt:apply/mt:eq/following-sibling::*[1]";
        Set<Node> variablesUsed = CodeGeneratorUtils.findWithoutDuplicates(problem, query);
        Iterator<Node> variableIt = variablesUsed.iterator();
        while (variableIt.hasNext()) {
            Node varNode = variableIt.next();
            String variable = CodeGeneratorUtils.xmlVarToString(varNode, true, false);
            if (variable != null && !variables.contains(variable) && !variable.equals("region") && !variable.equals("newRegion")) {
                variables.add(variable);
                //Add timelevels
                ArrayList<String> fieldVars = SAMRAIUtils.getFieldVariables(ftl, variable);
            	for (int i = 0; i < fieldVars.size(); i++) {
            		String fieldVar = fieldVars.get(i);
                    if (!variables.contains(fieldVar)) {
                    	variables.add(fieldVar);
                    }
            	}
            }
        }
        return variables;
    }
    
    /**
     * Gets the variables stored in particles.
     * @param problem		The problem
     * @param species		Species
     * @return				Particle variables
     * @throws CGException	CG00X - External error
     */
    private static ArrayList<String> getParticleFields(Document problem, String species) throws CGException {
    	ArrayList<String> variables = new ArrayList<String>();
        //Get the variables assigned in the code
        String query = ".//mms:fieldDiscretizationType[mms:discretizationType/mms:particles/text() = '" + species + "']//mms:field";
        Set<Node> variablesUsed = CodeGeneratorUtils.findWithoutDuplicates(problem, query);
        Iterator<Node> variableIt = variablesUsed.iterator();
        while (variableIt.hasNext()) {
            String variable = variableIt.next().getTextContent();
            if (!variables.contains(variable)) {
                variables.add(variable);
            }
        }
        return variables;
    }
      
    /**
     * Get the region movement information.
     * @param pi			Problem information
     * @param context		OSGI bundle context
     * @return				Movement information
     * @throws CGException  CG00X - External error
     */
    public static X3DMovInfo getX3dMovRegions(ProblemInfo pi, BundleContext context) throws CGException {
        ArrayList<String> x3dNames = new ArrayList<String>();
        ArrayList<Integer> x3dSizes = new ArrayList<Integer>();
        int timeslices = 0;
        String segMovement;
        if (pi.getMovementInfo().getX3dMovRegions().size() > 0) {
        	segMovement = "Fixed";
            for (int i = 0; i < pi.getMovementInfo().getX3dMovRegions().size(); i++) {
            	String segName = pi.getMovementInfo().getX3dMovRegions().get(i);
                x3dNames.add(segName);
            	
                DocumentManager docMan;
                X3DRegion x3d;
				try {
					docMan = new DocumentManagerImpl();
					x3d = CodeGeneratorUtils.parseX3dRegion(docMan.getX3D(CodeGeneratorUtils.find(pi.getProblem(), 
                        "//mms:regionGroup[mms:regionGroupName = '" + segName + "']//" 
                        + "mms:movement//mms:x3dRegionId").item(0).getTextContent()));
				} 
				catch (Exception e) {
					throw new CGException(CGException.CG00X, e.getMessage());
				}
                //Fill the point data
                double[][] samraiPoints = SAMRAIUtils.getPoints(x3d);                
                x3dSizes.add(samraiPoints.length);
            }
        }
        else {
        	segMovement = "Equations";
        }
        return new X3DMovInfo(segMovement, x3dNames, timeslices, x3dSizes);
    }
    
    /**
     * Returns the variables(fields and shared fields) that derive from fields.
     * @param doc           The problem
     * @param timeCoord     The time coordinate
     * @param pt            The problem type
     * @param samrai        If the code is SAMRAI
     * @return              The vars
     * @throws CGException  CG00X External error
     */
    //TODO Only needed for movement purposes
   /* public static ArrayList<String> getVarFields(Document doc, String timeCoord, ProblemType pt, boolean samrai) throws CGException {
        NodeList fields = find(doc, "/* /mms:fields/mms:field");
        ArrayList<String> varFields = new ArrayList<String>();
        for (int i = 0; i < fields.getLength(); i++) {
            String field = fields.item(i).getTextContent();
            NodeList vars = CodeGeneratorUtils.find(doc, "//mt:math/mt:apply/mt:eq/following-sibling::*[1]" 
                    + "[name()='sml:sharedVariable' and descendant::mt:ci[(starts-with(text(), '" + field + "') or ends-with(text(), '" 
                    + field + "'))]]|//mt:math/mt:apply/mt:eq/following-sibling::*[1]" 
                    + "[name()='mt:apply' and mt:ci/mt:msup[mt:ci = '" + field + "' and mt:apply[mt:ci = '(" 
                    + timeCoord + ")' and mt:plus and mt:cn[text() = '1']]]]");
            for (int j = 0; j < vars.getLength(); j++) {
                String variable = xmlVarToString(vars.item(j), false, false);
                if (!varFields.contains(variable)) {
                    varFields.add(variable);
                }
            }
        }
        return varFields;
    }*/
    
    /**
     * Returns the variables(fields and shared fields) that derive from fields.
     * @param doc           The problem
     * @param timeCoord     The time coordinate
     * @return              The vars
     * @throws CGException  CG00X External error
     */
    public static LinkedHashMap<String, ArrayList<String>> getFieldVariables(Document doc, String timeCoord) throws CGException {
    	LinkedHashMap<String, ArrayList<String>> fieldsVariables = new LinkedHashMap<String, ArrayList<String>>();
    	NodeList discretizations = doc.getDocumentElement().getElementsByTagNameNS(MMSURI, "PDEDiscretization");
    	for (int i = 0; i < discretizations.getLength(); i++) {
    		Element discretization = (Element) discretizations.item(i);
        	NodeList fields = discretization.getElementsByTagNameNS(MMSURI, "field");
        	NodeList substepVars = discretization.getElementsByTagNameNS(MMSURI, "outputVariable");
        	for (int j = 0; j < fields.getLength(); j++) {
        		String field = fields.item(j).getTextContent();
        		ArrayList<String> varFields = new ArrayList<String>();
        		if (fieldsVariables.containsKey(field)) {
        			varFields = fieldsVariables.get(field);
        		}
        		//Also add field at previous time
                Element fieldEl = createElement(doc, MTURI, "ci");
                fieldEl.setTextContent(field);
        		String variable = xmlVarToString(fieldEl, false, false) + "_p";
        		if (!varFields.contains(variable)) {
                    varFields.add(variable);
                }
        		for (int k = 0; k < substepVars.getLength(); k++) {
        			Element variableEl = (Element) substepVars.item(k).cloneNode(true);
        			NodeList fieldTags = CodeGeneratorUtils.find(variableEl, ".//sml:field");
	    			Text fieldValue = variableEl.getOwnerDocument().createTextNode(field);
	    			for (int l = fieldTags.getLength() - 1; l >= 0; l--) {
	    				fieldTags.item(l).getParentNode().replaceChild(fieldValue, fieldTags.item(l));
	    			}
	    			NodeList timeCoordinate = CodeGeneratorUtils.find(variableEl, ".//sml:timeCoordinate");
	    			Text timeTxt = variableEl.getOwnerDocument().createTextNode("(" + timeCoord + ")");
	    			for (int l = timeCoordinate.getLength() - 1; l >= 0; l--) {
	    				timeCoordinate.item(l).getParentNode().replaceChild(timeTxt, timeCoordinate.item(l));
	    			}
        		  	variable = xmlVarToString(variableEl, false, false);
        		    if (!varFields.contains(variable)) {
	                    varFields.add(variable);
	                }
        		}

        	    fieldsVariables.put(field, varFields);
        	}
    	}
        return fieldsVariables;
    }
    
    /**
     * Get variables element for the configuration.xml.
     * @param originalDoc   The original problem
     * @param pi            The problem info
     * @return              The element
     * @throws CGException  CG004 - External error
     */
    public static Element getMeshVariables(Document originalDoc, ProblemInfo pi, Set<String> posVars) throws CGException {
        LinkedHashMap<String, Integer> ftl = pi.getFieldTimeLevels();
        ArrayList<String> analysisFields = pi.getAnalysisFields();
        ArrayList<String> intFields = pi.getEdgeFields();
        
        Element variables = CodeGeneratorUtils.createElement(originalDoc, SPURI, "variables");
        //Fill all the fields
        NodeList fields = CodeGeneratorUtils.find(originalDoc, "/*/mms:fields/mms:field|/*/mms:agentProperties/mms:agentProperty"
                + "|/*/mms:vertexProperties/mms:vertexProperty|/*/mms:auxiliaryFields/mms:auxiliaryField");
        for (int i = 0; i < fields.getLength(); i++) {
        	if (find(originalDoc, "//mt:ci[text() = '" + fields.item(i).getTextContent() + "' and not(ancestor::mms:function or ancestor::sml:iterateOverParticles or ancestor::sml:iterateOverParticlesFromCell or ancestor::mms:modelCharacteristicDecompositions)]").getLength() > 0) {
	            Element field = CodeGeneratorUtils.createElement(originalDoc, SPURI, "field");
	            Element name = CodeGeneratorUtils.createElement(originalDoc, SPURI, "name");
	            name.setTextContent(SAMRAIUtils.variableNormalize(fields.item(i).getTextContent()));
	            Element timeSteps = CodeGeneratorUtils.createElement(originalDoc, SPURI, "timeSteps");
	            timeSteps.setTextContent(String.valueOf(ftl.get(fields.item(i).getTextContent())));
	            field.appendChild(name);
	            field.appendChild(timeSteps);
	            variables.appendChild(field);
        	}
        }
        //Fill all the interaction fields
        if (pi.isHasABMMeshlessFields()) {
            for (int i = 0; i < intFields.size(); i++) {
                Element field = CodeGeneratorUtils.createElement(originalDoc, SPURI, "edgeProperty");
                Element name = CodeGeneratorUtils.createElement(originalDoc, SPURI, "name");
                name.setTextContent(SAMRAIUtils.variableNormalize(intFields.get(i)));
                Element timeSteps = CodeGeneratorUtils.createElement(originalDoc, SPURI, "timeSteps");
                timeSteps.setTextContent("1");
                field.appendChild(name);
                field.appendChild(timeSteps);
                variables.appendChild(field);
            }
        }
        //Fill all the analysis fields
        for (int i = 0; i < analysisFields.size(); i++) {
            Element analysis = CodeGeneratorUtils.createElement(originalDoc, SPURI, "analysisField");
            analysis.setTextContent(analysisFields.get(i));
            variables.appendChild(analysis);
        }
        //Fill all the parameters
        NodeList params = CodeGeneratorUtils.find(originalDoc, "/*/mms:parameters/mms:parameter");
        for (int i = 0; i < params.getLength(); i++) {
            Element parameter = CodeGeneratorUtils.createElement(originalDoc, SPURI, "parameter");
            Element name = CodeGeneratorUtils.createElement(originalDoc, SPURI, "name");
            name.setTextContent(SAMRAIUtils.variableNormalize(params.item(i).getFirstChild().getTextContent()));
            Element type = CodeGeneratorUtils.createElement(originalDoc, SPURI, "type");
            type.setTextContent(SAMRAIUtils.getFVMType(params.item(i).getFirstChild().getNextSibling().getTextContent(), false));
            parameter.appendChild(name);
            parameter.appendChild(type);
            variables.appendChild(parameter);
        }
        //Fill all the vectors
        NodeList vectors = CodeGeneratorUtils.find(originalDoc, "/*/mms:vectorNames/mms:vectorName");
        for (int i = 0; i < vectors.getLength(); i++) {
            Element vector = CodeGeneratorUtils.createElement(originalDoc, SPURI, "vector");
            Element name = CodeGeneratorUtils.createElement(originalDoc, SPURI, "name");
            name.setTextContent(SAMRAIUtils.variableNormalize(vectors.item(i).getTextContent()));
            Element timeSteps = CodeGeneratorUtils.createElement(originalDoc, SPURI, "timeSteps");
            timeSteps.setTextContent(String.valueOf(1));
            vector.appendChild(name);
            vector.appendChild(timeSteps);
            variables.appendChild(vector);
        }

        //Find all the auxiliary variables
        ArrayList<String> candidates = new ArrayList<String>();
        String query = ".//mt:math[(not(parent::sml:if) or parent::sml:then) and (not(parent::sml:while) or parent::sml:loop) "
            + "and not(parent::sml:return or ancestor::mms:applyIf or ancestor::" 
            + "mms:finalizationCondition or ancestor::mms:function or ancestor::sml:iterateOverParticles or ancestor::sml:iterateOverParticlesFromCell or ancestor::mms:modelCharacteristicDecompositions)]/mt:apply/mt:eq/following-sibling::*[1]";
        NodeList leftTerms = CodeGeneratorUtils.find(originalDoc, query);
        for (int i = 0; i < leftTerms.getLength(); i++) {
            String variable = xmlVarToString(leftTerms.item(i), true, false);
            if (variable != null && !ftl.containsKey(variable) && !candidates.contains(variable) 
                    && !posVars.contains(variable) && !variable.equals("newRegion")) {
                candidates.add(variable);
            }
        }
        //Fill with extrapolation variables used in function calls.
        if (pi.isHasParticleFields() || pi.isHasMeshFields()) {
            ArrayList<String> exFuncVars = pi.getExtrapolFuncVars();
            for (int i = 0; i < exFuncVars.size(); i++) {
                String variable = exFuncVars.get(i);
                if (!ftl.containsKey(variable) && !candidates.contains(variable) 
                        && !posVars.contains(variable) && !variable.equals("newRegion")) {
                    candidates.add(variable);
                }
            }
            //If there is extrapolation, all the fields have extrapolated variables.
            if (exFuncVars.size() > 1 && pi.getExtrapolationType() != ExtrapolationType.generalPDE) {
                for (int i = 0; i < pi.getCoordinates().size(); i++) {
                    String coord = pi.getCoordinates().get(i);
                    for (int j = 0; j < fields.getLength(); j++) {
                        String field = SAMRAIUtils.variableNormalize(fields.item(j).getTextContent());
                        String variable = "extrapolatedSB" + field + "SP" + coord;
                        if (!candidates.contains(variable)) {
                            candidates.add(variable);
                        } 
                    }
                }
            }
        }
        //Fill with distance variables for hard region boundaries
    	ArrayList<String> distanceVars = pi.getExtrapolVars();
        for (int j = 0; j < distanceVars.size(); j++) {
            candidates.add(distanceVars.get(j));
        }		

        if (!pi.isHasABMMeshlessFields()) {
            ArrayList<String> segNBound = new ArrayList<String>();
            for (int j = pi.getRegions().size() - 1; j >= 0; j--) {
                String regionName = pi.getRegions().get(j);
                segNBound.add(regionName + "I");
                segNBound.add(regionName + "S");
            }
            for (int j = pi.getBoundariesPrecedence().size() - 1; j >= 0; j--) {
                String boundName = pi.getBoundariesPrecedence().get(j);
                segNBound.add(boundName);
            }
            for (int j = 0; j < segNBound.size(); j++) {
                String regionName = segNBound.get(j);
                if (pi.getHardRegionFields(regionName) != null) {
                    ArrayList<String> interactions = CodeGeneratorUtils.getRegionInteractions(regionName, pi.getHardRegions(), 
                            pi.getRegions());
                    for (int k = 0; k < interactions.size(); k++) {
                        ArrayList<String> hardFields = CodeGeneratorUtils.filterHardFields(regionName, interactions.get(k), pi.getHardRegions());
                        if (hardFields.size() > 0) {
                            String segName = interactions.get(k);
                            int segId = 2 * pi.getRegions().indexOf(segName) + 1;
                            if (pi.getRegionIds().contains(String.valueOf(segId))) {
                                if (!candidates.contains("stalled_" + segId)) {
                                    candidates.add("stalled_" + segId);
                                }
                            }
                            segId = 2 * pi.getRegions().indexOf(segName) + 2;
                            if (pi.getRegionIds().contains(String.valueOf(segId))) {
                                if (!candidates.contains("stalled_" + segId)) {
                                    candidates.add("stalled_" + segId);
                                }
                            }
                        }
                    }
                }
            }
            
            //Clean the fields from the candidates
            Iterator<String> fieldEnum = ftl.keySet().iterator();
            while (fieldEnum.hasNext()) {
                candidates.remove(SAMRAIUtils.variableNormalize(fieldEnum.next()));
            }
            for (int i = 0; i < analysisFields.size(); i++) {
                candidates.remove(analysisFields.get(i));
            }
            for (int i = 0; i < candidates.size(); i++) {
                Element auxiliary = CodeGeneratorUtils.createElement(originalDoc, SPURI, "auxiliary");
                auxiliary.setTextContent(candidates.get(i));
                variables.appendChild(auxiliary);
            }
        }
        return variables;
    }
    
    /**
     * Checks when a field is an auxiliary one.
     * @param pi           		The problem information
     * @param field             The field
     * @return                  True if auxiliary
     * @throws CGException      CG00X - External error
     */
    public static boolean isAuxiliaryField(ProblemInfo pi, String field) throws CGException {
        return pi.getAuxiliaryFields().contains(field);
    }
    
    /**
     * Replaces the regionInteriorId and regionSurfaceId tags with the identifier of the region.
     * @param doc               The document
     * @param pi                The problem information
     * @throws CGException      CG00X External error
     */
    public static void replaceRegionIdTags(Document doc, ProblemInfo pi) throws CGException {
        for (int i = pi.getRegionPrecedence().size() - 1; i >= 0; i--) {
            String region = pi.getRegionPrecedence().get(i);
            int regionId = i * 2 + 1;
            NodeList regionIds = CodeGeneratorUtils.find(doc, 
                    "/*/mms:region[mms:name = '" + region + "']//" 
                    + "sml:regionInteriorId[not(string(.)) and not(ancestor::sml:iterateOverParticles)]|"
                    + "/*/mms:subregions/mms:subregion[mms:name = '" + region + "' and not(ancestor::sml:iterateOverParticles)]//" 
                    + "sml:regionInteriorId[not(string(.)) and not(ancestor::sml:iterateOverParticles)]|"
                    + "//sml:regionInteriorId[text() = '" + region + "' and not(ancestor::sml:iterateOverParticles)]");
            for (int j = regionIds.getLength() - 1; j >= 0; j--) {
                Element ci = CodeGeneratorUtils.createElement(doc, MTURI, "ci");
                ci.setTextContent("FOV_" + String.valueOf(regionId));
                regionIds.item(j).getParentNode().replaceChild(ci, regionIds.item(j));
            }
            regionIds = CodeGeneratorUtils.find(doc, 
                    "/*/mms:region[mms:name = '" + region + "']//" 
                    + "sml:regionInteriorId[not(string(.)) and ancestor::sml:iterateOverParticles]|"
                    + "/*/mms:subregions/mms:subregion[mms:name = '" + region + "' and ancestor::sml:iterateOverParticles]//" 
                    + "sml:regionInteriorId[not(string(.)) and ancestor::sml:iterateOverParticles]|"
                    + "//sml:regionInteriorId[text() = '" + region + "' and ancestor::sml:iterateOverParticles]");
            for (int j = regionIds.getLength() - 1; j >= 0; j--) {
                Element cn = CodeGeneratorUtils.createElement(doc, MTURI, "cn");
                cn.setTextContent(String.valueOf(regionId));
                regionIds.item(j).getParentNode().replaceChild(cn, regionIds.item(j));
            }
            regionId = i * 2 + 2;
            regionIds = CodeGeneratorUtils.find(doc,
                    "/*/mms:region[mms:name = '" + region + "']//" 
                    + "sml:regionSurfaceId[not(string(.)) and not(ancestor::sml:iterateOverParticles)]|"
                    + "/*/mms:subregions/mms:subregion[mms:name = '" + region + "' and not(ancestor::sml:iterateOverParticles)]//" 
                    + "sml:regionSurfaceId[not(string(.)) and not(ancestor::sml:iterateOverParticles)]|"
                    + "//sml:regionSurfaceId[text() = '" + region + "' and not(ancestor::sml:iterateOverParticles)]");
            for (int j = regionIds.getLength() - 1; j >= 0; j--) {
                Element ci = CodeGeneratorUtils.createElement(doc, MTURI, "ci");
                ci.setTextContent("FOV_" + String.valueOf(regionId));
                regionIds.item(j).getParentNode().replaceChild(ci, regionIds.item(j));
            }
            regionIds = CodeGeneratorUtils.find(doc,
                    "/*/mms:region[mms:name = '" + region + "']//" 
                    + "sml:regionSurfaceId[not(string(.)) and ancestor::sml:iterateOverParticles]|"
                    + "/*/mms:subregions/mms:subregion[mms:name = '" + region + "' and ancestor::sml:iterateOverParticles]//" 
                    + "sml:regionSurfaceId[not(string(.)) and ancestor::sml:iterateOverParticles]|"
                    + "//sml:regionSurfaceId[text() = '" + region + "' and ancestor::sml:iterateOverParticles]");
            for (int j = regionIds.getLength() - 1; j >= 0; j--) {
                Element cn = CodeGeneratorUtils.createElement(doc, MTURI, "cn");
                cn.setTextContent(String.valueOf(regionId));
                regionIds.item(j).getParentNode().replaceChild(cn, regionIds.item(j));
            }
        }
        
        for (int i = 0; i < pi.getCoordinates().size(); i++) {
            String coordinate = discToCont(doc, pi.getCoordinates().get(i));
            NodeList regionIds = CodeGeneratorUtils.find(doc, 
                    "//sml:regionInteriorId[text() = '" + coordinate + "-Lower' and not(ancestor::sml:iterateOverParticles)]");
            for (int j = regionIds.getLength() - 1; j >= 0; j--) {
                Element ci = CodeGeneratorUtils.createElement(doc, MTURI, "ci");
                ci.setTextContent("FOV_" + coordinate + "Lower");
                regionIds.item(j).getParentNode().replaceChild(ci, regionIds.item(j));
            }
            regionIds = CodeGeneratorUtils.find(doc, 
                    "//sml:regionInteriorId[text() = '" + coordinate + "-Lower' and ancestor::sml:iterateOverParticles]");
            for (int j = regionIds.getLength() - 1; j >= 0; j--) {
                Element apply = CodeGeneratorUtils.createElement(doc, MTURI, "apply");
                Element minus = CodeGeneratorUtils.createElement(doc, MTURI, "minus");
                Element cn = CodeGeneratorUtils.createElement(doc, MTURI, "cn");
                cn.setTextContent(String.valueOf(pi.getCoordinates().indexOf(CodeGeneratorUtils.contToDisc(doc, coordinate)) * 2 + 1));
                apply.appendChild(minus);
                apply.appendChild(cn);
                regionIds.item(j).getParentNode().replaceChild(apply, regionIds.item(j));
            }
            regionIds = CodeGeneratorUtils.find(doc, 
                    "//sml:regionInteriorId[text() = '" + coordinate + "-Upper' and not(ancestor::sml:iterateOverParticles)]");
            for (int j = regionIds.getLength() - 1; j >= 0; j--) {
                Element ci = CodeGeneratorUtils.createElement(doc, MTURI, "ci");
                ci.setTextContent("FOV_" + coordinate + "Upper");
                regionIds.item(j).getParentNode().replaceChild(ci, regionIds.item(j));
            }
            regionIds = CodeGeneratorUtils.find(doc, 
                    "//sml:regionInteriorId[text() = '" + coordinate + "-Upper' and ancestor::sml:iterateOverParticles]");
            for (int j = regionIds.getLength() - 1; j >= 0; j--) {
                Element apply = CodeGeneratorUtils.createElement(doc, MTURI, "apply");
                Element minus = CodeGeneratorUtils.createElement(doc, MTURI, "minus");
                Element cn = CodeGeneratorUtils.createElement(doc, MTURI, "cn");
                cn.setTextContent(String.valueOf(pi.getCoordinates().indexOf(CodeGeneratorUtils.contToDisc(doc, coordinate)) * 2 + 2));
                apply.appendChild(minus);
                apply.appendChild(cn);
                regionIds.item(j).getParentNode().replaceChild(apply, regionIds.item(j));
            }
        }
    }
    
    /**
     * Checks when a region name is a boundary.
     * @param segGroup      The regions
     * @param segName       The name
     * @return              True if it is a boundary.
     */
    public static boolean isBoundary(ArrayList<String> segGroup, String segName) {
        return !segGroup.contains(segName);
    }
    
    /**
     * Get the identifier of a boundary.
     * @param segName       The region name
     * @return              The identifier
     */
    public static String getBoundaryId(String segName) {
        return segName.substring(0, segName.indexOf("-")) + segName.substring(segName.indexOf("-") + 1);
    }
    
    /**
     * Check when a node is in a list.
     * @param list      The list of nodes
     * @param node      The node to check
     * @return          True if contained
     */
    public static boolean contain(ArrayList<Node> list, Node node) {
        for (int i = 0; i < list.size(); i++) {
            if (node.isEqualNode(list.get(i))) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Get the region identifier name from the integer identifier.
     * @param doc               The problem
     * @param regionId         The region identifier number
     * @param coords            The coordinates
     * @return                  The identifier name
     * @throws CGException      CG00X External error
     */
    public static String getRegionName(Document doc, int regionId, ArrayList<String> coords) throws CGException {
        String segString = "";
        if (regionId < 0) {
            regionId = -1 * regionId;
            segString = CodeGeneratorUtils.discToCont(doc, coords.get((regionId - 1) / 2));
            if (regionId % 2 == 0) {
                segString = segString + "Upper";
            }
            else {
                segString = segString + "Lower"; 
            }
        }
        else {
            segString = String.valueOf(regionId);
        }
        return segString;
    }
    
    /**
     * Remove the repeated unions for a X2D unions list.
     * @param triangles     The triangle unions.
     * @return              The list of line unions.
     */
    public static String[] removeRepeatedUnions(String[] triangles) {
        ArrayList<X2DUnion> unions = new ArrayList<X2DUnion>();
        ArrayList<X2DUnion> repeatedUnions = new ArrayList<X2DUnion>();
        for (int i = 0; i < triangles.length; i++) {
            String[] union = triangles[i].trim().split(" +");
            X2DUnion a = new X2DUnion(union[0], union[1]);
            if (!unions.contains(a) && !repeatedUnions.contains(a)) {
                unions.add(a);
            }
            else {
                if (unions.contains(a)) {
                    repeatedUnions.add(a);
                    unions.remove(a);
                }
            }
            X2DUnion b = new X2DUnion(union[1], union[2]);
            if (!unions.contains(b) && !repeatedUnions.contains(b)) {
                unions.add(b);
            }
            else {
                if (unions.contains(b)) {
                    repeatedUnions.add(b);
                    unions.remove(b);
                }
            }
            X2DUnion c = new X2DUnion(union[2], union[0]);
            if (!unions.contains(c) && !repeatedUnions.contains(c)) {
                unions.add(c);
            }
            else {
                if (unions.contains(c)) {
                    repeatedUnions.add(c);
                    unions.remove(c);
                }
            }
        }
        String[] unionReturn = new String[unions.size()];
        for (int i = 0; i < unions.size(); i++) {
            unionReturn[i] = unions.get(i).getA() + " " + unions.get(i).getB();
        }
        return unionReturn;
    }
    
    /**
     * Remove the interior points from the point array and reset the unions.
     * @param points        The points
     * @param unions        The unions
     * @return              The contour points
     */
    public static double[][] removeInteriorPoints(double[][] points, String[] unions) {
        ArrayList<Point> newPoints = new ArrayList<Point>();
           
        int removedPoints = 0;
        for (int i = 0; i < points.length; i++) {
            boolean found = false;
            for (int k = 0; k < unions.length; k++) {
                String[] union = unions[k].trim().split(" +");
                for (int j = 0; j < union.length; j++) {
                    if (union[j].equals(String.valueOf(i - removedPoints))) {
                        found = true;
                    }
                }
            }
            if (found) {
                newPoints.add(new Point(points[i][0], points[i][1], points[i][2]));
            }
            else {
                removedPoints++;
                for (int k = 0; k < unions.length; k++) {
                    String[] union = unions[k].trim().split(" +");
                    int aInt = Integer.parseInt(union[0]);
                    int bInt = Integer.parseInt(union[1]);
                    if (aInt >= i) {
                        aInt--;
                    }
                    if (bInt >= i) {
                        bInt--;
                    }
                    unions[k] = aInt + " " + bInt;
                }
            }
        }
        double[][] pointsReturn = new double[newPoints.size()][THREE];
        for (int i = 0; i < newPoints.size(); i++) {
            pointsReturn[i][0] = newPoints.get(i).getPoint(0);
            pointsReturn[i][1] = newPoints.get(i).getPoint(1);
            pointsReturn[i][2] = newPoints.get(i).getPoint(2);
        }
        return pointsReturn;
    }
    
    /**
     * Remove the interior points from the point array and reset the unions.
     * @param points        The points
     * @param unions        The unions
     * @return              The contour points
     */
    public static double[] removeInteriorPoints(double[] points, String[] unions) {
        ArrayList<Point> newPoints = new ArrayList<Point>();
           
        int removedPoints = 0;
        for (int i = 0; i < points.length / THREE; i++) {
            boolean found = false;
            for (int k = 0; k < unions.length; k++) {
                String[] union = unions[k].trim().split(" +");
                for (int j = 0; j < union.length; j++) {
                    if (union[j].equals(String.valueOf(i - removedPoints))) {
                        found = true;
                    }
                }
            }
            if (found) {
                newPoints.add(new Point(points[i], points[i + points.length / THREE], points[i + 2 * points.length / THREE]));
            }
            else {
                removedPoints++;
                for (int k = 0; k < unions.length; k++) {
                    String[] union = unions[k].trim().split(" +");
                    int aInt = Integer.parseInt(union[0]);
                    int bInt = Integer.parseInt(union[1]);
                    if (aInt >= i) {
                        aInt--;
                    }
                    if (bInt >= i) {
                        bInt--;
                    }
                    unions[k] = aInt + " " + bInt;
                }
            }
        }
        double[] pointsReturn = new double[newPoints.size() * 2];
        for (int i = 0; i < newPoints.size(); i++) {
            pointsReturn[i] = newPoints.get(i).getPoint(0);
            pointsReturn[i + newPoints.size()] = newPoints.get(i).getPoint(1);
        }
        return pointsReturn;
    }
    
    /**
     * Get the original field variable from the boundary.
     * Auxiliary fields are bypassed.
     * @param scope             The boundary scope
     * @param fields            All the fields
     * @param currentField      The field for the variable
     * @param extrapolationType	The extrapolation type
     * @param ndims				Number of dimensions
     * @return                  The variable
     * @throws CGException      CG003 Not possible to create code for the platform
     *                          CG00X External error
     */
    public static Element getOriginalField(Node scope, ArrayList<String> fields, String currentField, 
    		ExtrapolationType extrapolationType, int ndims) throws CGException {
        boolean found = false;
        Element originalField = null;
        for (int i = 0; i < fields.size() && !found; i++) {
            String field = fields.get(i);
            //Take field from function call
            int position = 0;
            switch (extrapolationType) {
	            case axisPDE:
	            	position = ndims * 3 + 2;
	            	break;
	            case generalPDE:
	            	position = ndims * 2 + 2;
	            	break;
            }
            NodeList originalFieldList = find(scope, ".//sml:fieldExtrapolation[@fieldAtt = '" + field 
                        + "']//sml:functionCall/*[position() = " + position + "]");
            if (originalFieldList.getLength() > 0) {
                found = true;
                originalField = (Element) originalFieldList.item(0).cloneNode(true);
                Element fieldElem = (Element) find(originalField, ".//mt:ci[ends-with(text(), '" + field + "')]").item(0);
                String value = fieldElem.getTextContent();
                fieldElem.setTextContent(value.substring(0, value.lastIndexOf(field)) + currentField);
            }
            else {
                originalFieldList = find(scope, ".//sml:fieldExtrapolation[@fieldAtt = '" + field 
                        + "']//mt:math[(not(parent::sml:if) or parent::sml:then) and (not(parent::sml:while) or "
                        + "parent::sml:loop)]/mt:apply/mt:eq[not(following-sibling::*[1][name()='mt:apply'] = "
                        + "preceding-sibling::mt:math/mt:apply/mt:eq/following-sibling::*[1][(name()='mt:apply' or "
                        + "name()='sml:sharedVariable')]//descendant-or-self::mt:apply)]/following-sibling::*[1]" 
                        + "[(name()='mt:apply' or name()='sml:sharedVariable') and not(descendant::mt:ci = 'extrapolated')"
                        + " and descendant::mt:ci = '" + field + "']//descendant-or-self::mt:apply");
                if (originalFieldList.getLength() > 0) {
                    found = true;
                    originalField = (Element) originalFieldList.item(0).cloneNode(true);
                    Element fieldElem = (Element) find(originalField, ".//mt:ci [text() = '" + field + "']").item(0);
                    fieldElem.setTextContent(currentField);
                } 
                else {
                    //Check if field is contained by another field
                    boolean contained = false;
                    for (int j = 0; j < fields.size() && !contained; j++) {
                        String newField = fields.get(j);
                        if (newField.contains(field) && i != j) {
                            contained = true;
                        }
                    }
                    if (!contained) {
                        originalFieldList = find(scope, ".//sml:fieldExtrapolation[@fieldAtt = '" + field 
                                + "']//mt:math[(not(parent::sml:if) or parent::sml:then) and (not(parent::sml:while) or "
                                + "parent::sml:loop)]/mt:apply/mt:eq[not(following-sibling::*[1][name()='mt:apply'] = "
                                + "preceding-sibling::mt:math/mt:apply/mt:eq/following-sibling::*[1][(name()='mt:apply' or "
                                + "name()='sml:sharedVariable')]//descendant-or-self::mt:apply)]/following-sibling::"
                                + "*[1][(name()='mt:apply' or name()='sml:sharedVariable')  and not(descendant::mt:ci = 'extrapolated')"
                                + " and descendant::mt:ci[ends-with(text(), '"
                                + field + "')]]//descendant-or-self::mt:apply");
                        if (originalFieldList.getLength() > 0) {
                            found = true;
                            originalField = (Element) originalFieldList.item(0).cloneNode(true);
                            Element fieldElem = (Element) find(originalField, ".//mt:ci[ends-with(text(), '" + field + "')]").item(0);
                            String value = fieldElem.getTextContent();
                            fieldElem.setTextContent(value.substring(0, value.lastIndexOf(field)) + currentField);
                        }
                        
                        originalFieldList = find(scope, ".//mt:math[(not(parent::sml:if) or parent::sml:then) and (not(parent::sml:while) or "
                                + "parent::sml:loop)]/mt:apply/mt:eq[not(following-sibling::*[1][name()='mt:apply'] = "
                                + "preceding-sibling::mt:math/mt:apply/mt:eq/following-sibling::*[1][(name()='mt:apply' or "
                                + "name()='sml:sharedVariable')]//descendant-or-self::mt:apply)]/following-sibling::"
                                + "*[1][(name()='mt:apply' or name()='sml:sharedVariable') and not(descendant::mt:ci = 'extrapolated') and "
                                + "descendant::mt:ci[ends-with(text(), '" + field + "')]]//descendant-or-self::mt:apply");
                        if (originalFieldList.getLength() > 0) {
                            found = true;
                            originalField = (Element) originalFieldList.item(0).cloneNode(true);
                            Element fieldElem = (Element) find(originalField, ".//mt:ci[ends-with(text(), '" + field + "')]").item(0);
                            String value = fieldElem.getTextContent();
                            fieldElem.setTextContent(value.substring(0, value.lastIndexOf(field)) + currentField);
                        }
                    }
                }
            }
        }
        if (originalField == null) {
            throw new CGException(CGException.CG003, "No variable found in getOriginalField for " + currentField + ", please contact developer.");
        }
        return (Element) originalField.getFirstChild();
    }
    
    /**
     * Process the interaction tags for the region interaction to use FOV variables.
     * @param problem       The problem
     * @param pi            The problem info
     * @throws CGException  CG00X External error
     */
    public static void exFOVProcessing(Document problem, ProblemInfo pi) throws CGException {
        ArrayList<String> regions = pi.getRegions();
        for (int i = 0; i < regions.size(); i++) {
            String regionName = regions.get(i);
            //Interior
            ArrayList<String> incompFields = pi.getHardRegionFields(regionName + "I");
            if (incompFields != null) {
                for (int j = 0; j < incompFields.size(); j++) {
                    String field = incompFields.get(j);
                    String query = "/*/mms:region[descendant::mms:name = '" + regionName 
                            + "']//sml:fieldExtrapolation[@fieldAtt = '" + field + "']//sml:functionCall"
                            + "|/*/mms:subregions/mms:subregion[descendant::mms:name = '" + regionName 
                            + "']//sml:fieldExtrapolation[@fieldAtt = '" + field + "']//sml:functionCall";
                    NodeList extrapolations = CodeGeneratorUtils.find(problem, query);
                    for (int l = extrapolations.getLength() - 1; l >= 0; l--) {
                        DocumentFragment fovFragment = problem.createDocumentFragment();
                        Element extrapol = (Element) extrapolations.item(l);
                        ArrayList<String> interactions = CodeGeneratorUtils.getRegionInteractions(regionName + "I", pi.getHardRegions(), 
                                pi.getRegions());
                        int counter = 0;
                        for (int k = 0; k < interactions.size(); k++) {
                            ArrayList<String> hardFields = CodeGeneratorUtils.filterHardFields(regionName + "I", interactions.get(k), 
                                    pi.getHardRegions());
                            String segName = interactions.get(k);
                            //Interior
                            if (hardFields.contains(field) 
                                    && pi.getRegionIds().contains(String.valueOf(2 * pi.getRegions().indexOf(segName) + 1))) {
                                counter++;
                                int segId = 2 * pi.getRegions().indexOf(segName) + 1;
                                Element fov = CodeGeneratorUtils.createElement(problem, MTURI, "ci");
                                fov.setTextContent("FOV_" + String.valueOf(segId));
                                fovFragment.appendChild(fov);
                            }
                            //Surface
                            if (hardFields.contains(field) 
                                    && pi.getRegionIds().contains(String.valueOf(2 * pi.getRegions().indexOf(segName) + 2))) {
                                counter++;
                                int segId = 2 * pi.getRegions().indexOf(segName) + 2;
                                Element fov = CodeGeneratorUtils.createElement(problem, MTURI, "ci");
                                fov.setTextContent("FOV_" + String.valueOf(segId));
                                fovFragment.appendChild(fov);
                            }
                        }
                        Element fovNode = null;
                        if (extrapol.getElementsByTagName("sml:interactionRegionId").getLength() > 0) {
                            fovNode = (Element) extrapol.getElementsByTagName("sml:interactionRegionId").item(0);
                        }
                        extrapol.replaceChild(fovFragment.cloneNode(true), fovNode);
                        if (counter > 1) {
                            //Change function calling
                            Element newName = CodeGeneratorUtils.createElement(problem, MTURI, "ci");
                            Element oldName = (Element) extrapol.getFirstChild();
                            newName.setTextContent(oldName.getTextContent() + "_" + counter);
                            extrapol.replaceChild(newName, oldName);
                            //check if the function already exists. If not then create it
                            if (CodeGeneratorUtils.find(problem, "/*/mms:functions/mms:function[mms:functionName = '" 
                                    + oldName.getTextContent() + "_" + counter + "']").getLength() == 0) {
                                Element baseFunc = (Element) CodeGeneratorUtils.find(problem, "/*/mms:functions/mms:function[" 
                                        + "mms:functionName = '" + oldName.getTextContent() + "']").item(0).cloneNode(true);
                                CodeGeneratorUtils.summarizeFOV(baseFunc, counter);
                                CodeGeneratorUtils.find(problem, "/*/mms:functions/mms:functions").item(0).appendChild(baseFunc);
                            }
                        }
                    }
                }
            }
            //Surface
            incompFields = pi.getHardRegionFields(regionName + "S");
            if (incompFields != null) {
                for (int j = 0; j < incompFields.size(); j++) {
                    String field = incompFields.get(j);
                    String query = "/*/mms:region[descendant::mms:name = '" + regionName 
                            + "']//sml:fieldExtrapolation[@fieldAtt = '" + field + "']//sml:functionCall"
                            + "|/*/mms:subregions/mms:subregion/mms:subregion[descendant::mms:name = '" + regionName 
                            + "']//sml:fieldExtrapolation[@fieldAtt = '" + field + "']//sml:functionCall";
                    NodeList extrapolations = CodeGeneratorUtils.find(problem, query);
                    for (int l = extrapolations.getLength() - 1; l >= 0; l--) {
                        DocumentFragment fovFragment = problem.createDocumentFragment();
                        Element extrapol = (Element) extrapolations.item(l);
                        ArrayList<String> interactions = CodeGeneratorUtils.getRegionInteractions(regionName + "S", pi.getHardRegions(), 
                                pi.getRegions());
                        int counter = 0;
                        for (int k = 0; k < interactions.size(); k++) {
                            ArrayList<String> hardFields = CodeGeneratorUtils.filterHardFields(regionName + "S", interactions.get(k), 
                                    pi.getHardRegions());
                            String segName = interactions.get(k);
                            //Interior
                            if (hardFields.contains(field) 
                                    && pi.getRegionIds().contains(String.valueOf(2 * pi.getRegions().indexOf(segName) + 1))) {
                                counter++;
                                int segId = 2 * pi.getRegions().indexOf(segName) + 1;
                                Element fov = CodeGeneratorUtils.createElement(problem, MTURI, "ci");
                                fov.setTextContent("FOV_" + String.valueOf(segId));
                                fovFragment.appendChild(fov);
                            }
                            //Surface
                            if (hardFields.contains(field) 
                                    && pi.getRegionIds().contains(String.valueOf(2 * pi.getRegions().indexOf(segName) + 2))) {
                                counter++;
                                int segId = 2 * pi.getRegions().indexOf(segName) + 2;
                                Element fov = CodeGeneratorUtils.createElement(problem, MTURI, "ci");
                                fov.setTextContent("FOV_" + String.valueOf(segId));
                                fovFragment.appendChild(fov);
                            }
                        }
                        Element fovNode = null;
                        if (extrapol.getElementsByTagName("sml:interactionRegionId").getLength() > 0) {
                            fovNode = (Element) extrapol.getElementsByTagName("sml:interactionRegionId").item(0);
                        }
                        extrapol.replaceChild(fovFragment.cloneNode(true), fovNode);
                        if (counter > 1) {
                            //Change function calling
                            Element newName = CodeGeneratorUtils.createElement(problem, MTURI, "ci");
                            Element oldName = (Element) extrapol.getFirstChild();
                            newName.setTextContent(oldName.getTextContent() + "_" + counter);
                            extrapol.replaceChild(newName, oldName);
                            //check if the function already exists. If not then create it
                            if (CodeGeneratorUtils.find(problem, "/*/mms:functions/mms:function[mms:functionName = '" 
                                    + oldName.getTextContent() + "_" + counter + "']").getLength() == 0) {
                                Element baseFunc = (Element) CodeGeneratorUtils.find(problem, "/*/mms:functions/mms:function[" 
                                        + "mms:functionName = '" + oldName.getTextContent() + "']").item(0).cloneNode(true);
                                CodeGeneratorUtils.summarizeFOV(baseFunc, counter);
                                CodeGeneratorUtils.find(problem, "/*/mms:functions/mms:functions").item(0).appendChild(baseFunc);
                            }
                        }
                    }
                }
            }
        }
    }
    
    /**
     * Modify the extrapolation function to summarize all the fovs.
     * @param function      The function to modify
     * @param counter       The number of fovs needed
     * @throws CGException  CG00X External error
     */
    public static void summarizeFOV(Element function, int counter) throws CGException {
        Document doc = function.getOwnerDocument();
        //Name modification
        function.getFirstChild().setTextContent(function.getFirstChild().getTextContent() + "_" + counter);
        //Parameter modification
        Element originalFov = (Element) find(function, ".//mms:functionParameter[mms:name = 'FOV']").item(0);
        DocumentFragment fovFragD = doc.createDocumentFragment();
        for (int i = 0; i < counter; i++) {
            //Parameter modification
            Element newFOV = (Element) originalFov.cloneNode(true);
            newFOV.getFirstChild().setTextContent(newFOV.getFirstChild().getTextContent() + "_" + i);
            fovFragD.appendChild(newFOV);
        }
        originalFov.getParentNode().replaceChild(fovFragD, originalFov);
        //code modification
        NodeList fovs = find(function, ".//mt:apply[mt:ci = 'FOV']");
        for (int i = fovs.getLength() - 1; i >= 0; i--) {
            Element baseFov = (Element) fovs.item(i);
            Element fovSUM = null;
            for (int j = 0; j < counter; j++) {
                if (j == 0) {
                    fovSUM = (Element) baseFov.cloneNode(true);
                    fovSUM.getFirstChild().setTextContent(fovSUM.getFirstChild().getTextContent() + "_" + j);
                }
                else {
                    Element apply = createElement(doc, MTURI, "apply");
                    Element plus = createElement(doc, MTURI, "plus");
                    Element fov = (Element) baseFov.cloneNode(true);
                    fov.getFirstChild().setTextContent(fov.getFirstChild().getTextContent() + "_" + j);
                    apply.appendChild(plus);
                    apply.appendChild(fov);
                    apply.appendChild(fovSUM.cloneNode(true));
                    fovSUM = apply;
                }
            }
            baseFov.getParentNode().replaceChild(fovSUM, baseFov);
        }
    }
    
    /**
     * Creates all the combinations in n-dimensions for a list of elements.
     * @param dimensions    The dimensions
     * @param elements      The elements
     * @return              The combinations
     */
    public static ArrayList<String> createCombinations(int dimensions, ArrayList<String> elements) {
        ArrayList<String> combinations = new ArrayList<String>();
        for (int i = 0; i < dimensions; i++) {
            if (i == 0) {
                for (int j = 0; j < elements.size(); j++) {
                    combinations.add(elements.get(j));
                }
            }
            else {
                ArrayList<String> combinationsTmp = new ArrayList<String>();
                for (int j = 0; j < combinations.size(); j++) {
                    for (int k = 0; k < elements.size(); k++) {
                        String combination = combinations.get(j) + elements.get(k);
                        combinationsTmp.add(combination);
                    }
                }
                combinations.clear();
                combinations.addAll(combinationsTmp);
            }
        }
        return combinations;
    }
    
    /**
     * Creates all the combinations in n-dimensions for a list of elements.
     * @param dimensions    The dimensions
     * @param elements      The elements
     * @return              The combinations
     */
    public static ArrayList<ArrayList<Integer>> createCombinationsInt(int dimensions, ArrayList<Integer> elements) {
        ArrayList<ArrayList<Integer>> combinations = new ArrayList<ArrayList<Integer>>();
        for (int i = 0; i < dimensions; i++) {
            if (i == 0) {
                for (int j = 0; j < elements.size(); j++) {
                	ArrayList<Integer> tmp = new ArrayList<Integer>();
                	tmp.add(elements.get(j));
                    combinations.add(tmp);
                }
            }
            else {
                ArrayList<ArrayList<Integer>> combinationsTmp = new ArrayList<ArrayList<Integer>>();
                for (int j = 0; j < combinations.size(); j++) {
                    for (int k = 0; k < elements.size(); k++) {
                    	ArrayList<Integer> combination = new ArrayList<Integer>(combinations.get(j));
                    	combination.add(elements.get(k));
                        combinationsTmp.add(combination);
                    }
                }
                combinations.clear();
                combinations.addAll(combinationsTmp);
            }
        }
        return combinations;
    }
    
    /**
     * Sets the information relative to region movement.
     * @param doc           The problem
     * @return              The movement information
     * @throws CGException  CG00X External error
     */
    public static MovementInfo getMovementInfo(Document doc) throws CGException {
        boolean moves = find(doc, "//mms:movement//mms:type[text() = 'Fixed' or text() = 'Equations']").getLength() > 0;
        ArrayList<String> x3ds = new ArrayList<String>();
        ArrayList<String> segMov = new ArrayList<String>();
        NodeList regNames = find(doc, "//mms:region[descendant::mms:movement//mms:type[text() = 'Fixed']]/mms:name|"
        		+ "//mms:subregion[descendant::mms:movement//mms:type[text() = 'Fixed']]/mms:name"); 
        for (int i = 0; i < regNames.getLength(); i++) {
            x3ds.add(regNames.item(i).getTextContent());
        }
        regNames = find(doc, "/*/mms:region[descendant::mms:movement//mms:type[text() = 'Fixed' or text() = 'Equations']]" 
                + "/mms:name|/*/mms:subregions/mms:subregion[descendant::mms:movement//mms:type[text() = 'Fixed' or text() = 'Equations']]" 
                + "/mms:name"); 
        for (int i = 0; i < regNames.getLength(); i++) {
            segMov.add(regNames.item(i).getTextContent());
        }
        return new MovementInfo(moves, x3ds, segMov);
    }
    
    /**
     * Gets the fields of the problem.
     * @param doc           The problem
     * @return              List of fields
     * @throws CGException  CG00X External error
     */
    public static ArrayList<String> getFields(Document doc) throws CGException {
        ArrayList<String> fields = new ArrayList<String>();
        NodeList fieldList = CodeGeneratorUtils.find(doc, "/*/mms:fields/mms:field|/*/mms:auxiliaryFields/mms:auxiliaryField");
        for (int i = 0; i < fieldList.getLength(); i++) {
            fields.add(fieldList.item(i).getTextContent());
        }
        
        return fields;
    }
    
    /**
     * Creates all the combinations for interpolation purposes.
     * @param dim               The dimension of the problem
     * @return                  The interaction list
     */
    public static ArrayList<String> createInterpolationCombinations(int dim) {
        ArrayList<String> combinations = new ArrayList<String>();
        
        combinations.add("m1");
        combinations.add("p0");
        combinations.add("p1");
        combinations.add("p2");
        
        for (int j = 0; j < dim - 1; j++) {
            ArrayList<String> combinationsTMP = new ArrayList<String>();
            combinationsTMP.addAll(combinations);
            combinations.clear();
            for (int i = 0; i < combinationsTMP.size(); i++) {
                String newComb = combinationsTMP.get(i);
                combinations.add(newComb + "m1");
                combinations.add(newComb + "p0");
                combinations.add(newComb + "p1");
                combinations.add(newComb + "p2");
            }            
        }
        return combinations;
    }
    
    /**
     * Creates all the combinations for interpolation purposes.
     * @param ind               The indentation
     * @param dim               The dimension of the problem
     * @param coords            The coordinates
     * @param part              If its a particle problem
     * @return                  The interaction list
     */
    public static String createMapInterpolation(String ind, int dim, ArrayList<String> coords, boolean part) {
        String index = "";
        for (int i = 0; i < dim; i++) {
            index = index + coords.get(i) + ", ";
        }
        index = index.substring(0, index.lastIndexOf(", "));
        String interpolation = "";
      //Bicubic interpolation
        if (dim == 2) {
            String interp = "a00 + a10*xaux + a20*xaux*xaux + a30*xaux*xaux*xaux + yaux*(a01 + a11*xaux + a21*xaux*xaux + a31*xaux*xaux*xaux) "
                    + "+ yaux*yaux*(a02 + a12*xaux + a22*xaux*xaux + a32*xaux*xaux*xaux) + yaux*yaux*yaux*(a03 + a13*xaux + a23*xaux*xaux "
                    + "+ a33*xaux*xaux*xaux)";
            if (part) {
                interpolation = ind + "particle->setFieldValue(field, " + interp + ");" + NL;
            }
            else {
                interpolation = ind + "vector(field, " + index + ") = " + interp + ";" + NL;
            }
            return ind + "double a00 = fp0p0;" + NL
                    + ind + "double a10 = (-fm1p0 + fp1p0)/2.;" + NL
                    + ind + "double a20 = fm1p0 - (5*fp0p0)/2. + 2*fp1p0 - fp2p0/2.;" + NL
                    + ind + "double a30 = (-fm1p0 + 3*fp0p0 - 3*fp1p0 + fp2p0)/2.;" + NL
                    + ind + "double a01 = (-fp0m1 + fp0p1)/2.;" + NL
                    + ind + "double a11 = (fm1m1 - fm1p1 - fp1m1 + fp1p1)/4.;" + NL
                    + ind + "double a21 = (-2*fm1m1 + 2*fm1p1 + 5*fp0m1 - 5*fp0p1 - 4*fp1m1 + 4*fp1p1 + fp2m1 - fp2p1)/4.;" + NL
                    + ind + "double a31 = (fm1m1 - fm1p1 - 3*fp0m1 + 3*fp0p1 + 3*fp1m1 - 3*fp1p1 - fp2m1 + fp2p1)/4.;" + NL
                    + ind + "double a02 = fp0m1 - (5*fp0p0)/2. + 2*fp0p1 - fp0p2/2.;" + NL
                    + ind + "double a12 = (-2*fm1m1 + 5*fm1p0 - 4*fm1p1 + fm1p2 + 2*fp1m1 - 5*fp1p0 + 4*fp1p1 - fp1p2)/4.;" + NL
                    + ind + "double a22 = (4*fm1m1 - 10*fm1p0 + 8*fm1p1 - 2*fm1p2 - 10*fp0m1 + 25*fp0p0 - 20*fp0p1 + 5*fp0p2 "
                    + "+ 8*fp1m1 - 20*fp1p0 + 16*fp1p1 - 4*fp1p2 - 2*fp2m1 + 5*fp2p0 - 4*fp2p1 + fp2p2)/4.;" + NL
                    + ind + "double a32 = (-2*fm1m1 + 5*fm1p0 - 4*fm1p1 + fm1p2 + 6*fp0m1 - 15*fp0p0 + 12*fp0p1 - 3*fp0p2 - 6*fp1m1 " 
                    + "+ 15*fp1p0 - 12*fp1p1 + 3*fp1p2 + 2*fp2m1 - 5*fp2p0 + 4*fp2p1 - fp2p2)/4.;" + NL
                    + ind + "double a03 = (-fp0m1 + 3*fp0p0 - 3*fp0p1 + fp0p2)/2.;" + NL
                    + ind + "double a13 = (fm1m1 - 3*fm1p0 + 3*fm1p1 - fm1p2 - fp1m1 + 3*fp1p0 - 3*fp1p1 + fp1p2)/4.;" + NL
                    + ind + "double a23 = (-2*fm1m1 + 6*fm1p0 - 6*fm1p1 + 2*fm1p2 + 5*fp0m1 - 15*fp0p0 + 15*fp0p1 - 5*fp0p2 - 4*fp1m1 "
                    + "+ 12*fp1p0 - 12*fp1p1 + 4*fp1p2 + fp2m1 - 3*fp2p0 + 3*fp2p1 - fp2p2)/4.;" + NL
                    + ind + "double a33 = (fm1m1 - 3*fm1p0 + 3*fm1p1 - fm1p2 - 3*fp0m1 + 9*fp0p0 - 9*fp0p1 + 3*fp0p2 + 3*fp1m1 - 9*fp1p0 "
                    + "+ 9*fp1p1 - 3*fp1p2 - fp2m1 + 3*fp2p0 - 3*fp2p1 + fp2p2)/4.;" + NL
                    + interpolation;
        }
		String interp = "a000 + a100*xaux + a200*xaux*xaux + a300*xaux*xaux*xaux + yaux*(a010 + a110*xaux + a210*xaux*xaux"
		        + " + a310*xaux*xaux*xaux) + yaux*yaux*(a020 + a120*xaux + a220*xaux*xaux + a320*xaux*xaux*xaux) + yaux*yaux*yaux*(a030 " 
		        + "+ a130*xaux + a230*xaux*xaux + a330*xaux*xaux*xaux) + zaux * (a001 + a101*xaux + a201*xaux*xaux + a301*xaux*xaux*xaux "
		        + "+ yaux*(a011 + a111*xaux + a211*xaux*xaux + a311*xaux*xaux*xaux) + yaux*yaux*(a021 + a121*xaux + a221*xaux*xaux "
		        + "+ a321*xaux*xaux*xaux) + yaux*yaux*yaux*(a031 + a131*xaux + a231*xaux*xaux + a331*xaux*xaux*xaux)) + zaux*zaux * (a002 "
		        + "+ a102*xaux + a202*xaux*xaux + a302*xaux*xaux*xaux + yaux*(a012 + a112*xaux + a212*xaux*xaux + a312*xaux*xaux*xaux) "
		        + "+ yaux*yaux*(a022 + a122*xaux + a222*xaux*xaux + a322*xaux*xaux*xaux) + yaux*yaux*yaux*(a032 + a132*xaux + a232*xaux*xaux "
		        + "+ a332*xaux*xaux*xaux)) + zaux*zaux*zaux * (a003 + a103*xaux + a203*xaux*xaux + a303*xaux*xaux*xaux + yaux*(a013 "
		        + "+ a113*xaux + a213*xaux*xaux + a313*xaux*xaux*xaux) + yaux*yaux*(a023 + a123*xaux + a223*xaux*xaux + a323*xaux*xaux*xaux) "
		        + "+ yaux*yaux*yaux*(a033 + a133*xaux + a233*xaux*xaux + a333*xaux*xaux*xaux))"; 
		if (part) {
		    interpolation = ind + "particle->setFieldValue(field, " + interp + ");" + NL;
		}
		else {
		    interpolation = ind + "vector(field, " + index + ") = " + interp + ";" + NL;
		}
		return ind + "double a000 = fp0p0p0;" + NL
		        + ind + "double a100 = (-fm1p0p0 + fp1p0p0)/2.;" + NL
		        + ind + "double a200 = fm1p0p0 - (5*fp0p0p0)/2. + 2*fp1p0p0 - fp2p0p0/2.;" + NL
		        + ind + "double a300 = (-fm1p0p0 + 3*fp0p0p0 - 3*fp1p0p0 + fp2p0p0)/2.;" + NL
		        + ind + "double a010 = (-fp0m1p0 + fp0p1p0)/2.;" + NL
		        + ind + "double a110 = (fm1m1p0 - fm1p1p0 - fp1m1p0 + fp1p1p0)/4.;" + NL
		        + ind + "double a210 = (-2*fm1m1p0 + 2*fm1p1p0 + 5*fp0m1p0 - 5*fp0p1p0 - 4*fp1m1p0 + 4*fp1p1p0 + fp2m1p0 - fp2p1p0)/4.;" + NL
		        + ind + "double a310 = (fm1m1p0 - fm1p1p0 - 3*fp0m1p0 + 3*fp0p1p0 + 3*fp1m1p0 - 3*fp1p1p0 - fp2m1p0 + fp2p1p0)/4.;" + NL
		        + ind + "double a020 = fp0m1p0 - (5*fp0p0p0)/2. + 2*fp0p1p0 - fp0p2p0/2.;" + NL
		        + ind + "double a120 = (-2*fm1m1p0 + 5*fm1p0p0 - 4*fm1p1p0 + fm1p2p0 + 2*fp1m1p0 - 5*fp1p0p0 + 4*fp1p1p0 - fp1p2p0)/4.;" + NL
		        + ind + "double a220 = (4*fm1m1p0 - 10*fm1p0p0 + 8*fm1p1p0 - 2*fm1p2p0 - 10*fp0m1p0 + 25*fp0p0p0 - 20*fp0p1p0 + 5*fp0p2p0"
		        + " + 8*fp1m1p0 - 20*fp1p0p0 + 16*fp1p1p0 - 4*fp1p2p0 - 2*fp2m1p0 + 5*fp2p0p0 - 4*fp2p1p0 + fp2p2p0)/4.;" + NL
		        + ind + "double a320 = (-2*fm1m1p0 + 5*fm1p0p0 - 4*fm1p1p0 + fm1p2p0 + 6*fp0m1p0 - 15*fp0p0p0 + 12*fp0p1p0 - 3*fp0p2p0"
		        + " - 6*fp1m1p0 + 15*fp1p0p0 - 12*fp1p1p0 + 3*fp1p2p0 + 2*fp2m1p0 - 5*fp2p0p0 + 4*fp2p1p0 - fp2p2p0)/4.;" + NL
		        + ind + "double a030 = (-fp0m1p0 + 3*fp0p0p0 - 3*fp0p1p0 + fp0p2p0)/2.;" + NL
		        + ind + "double a130 = (fm1m1p0 - 3*fm1p0p0 + 3*fm1p1p0 - fm1p2p0 - fp1m1p0 + 3*fp1p0p0 - 3*fp1p1p0 + fp1p2p0)/4.;" + NL
		        + ind + "double a230 = (-2*fm1m1p0 + 6*fm1p0p0 - 6*fm1p1p0 + 2*fm1p2p0 + 5*fp0m1p0 - 15*fp0p0p0 + 15*fp0p1p0 - 5*fp0p2p0 "
		        + "- 4*fp1m1p0 + 12*fp1p0p0 - 12*fp1p1p0 + 4*fp1p2p0 + fp2m1p0 - 3*fp2p0p0 + 3*fp2p1p0 - fp2p2p0)/4.;" + NL
		        + ind + "double a330 = (fm1m1p0 - 3*fm1p0p0 + 3*fm1p1p0 - fm1p2p0 - 3*fp0m1p0 + 9*fp0p0p0 - 9*fp0p1p0 + 3*fp0p2p0"
		        + " + 3*fp1m1p0 - 9*fp1p0p0 + 9*fp1p1p0 - 3*fp1p2p0 - fp2m1p0 + 3*fp2p0p0 - 3*fp2p1p0 + fp2p2p0)/4.;" + NL
		        + ind + "double a001 = (-fp0p0m1 + fp0p0p1)/2.;" + NL
		        + ind + "double a101 = (fm1p0m1 - fm1p0p1 - fp1p0m1 + fp1p0p1)/4.;" + NL
		        + ind + "double a201 = (-2*fm1p0m1 + 2*fm1p0p1 + 5*fp0p0m1 - 5*fp0p0p1 - 4*fp1p0m1 + 4*fp1p0p1 + fp2p0m1 - fp2p0p1)/4.;" + NL
		        + ind + "double a301 = (fm1p0m1 - fm1p0p1 - 3*fp0p0m1 + 3*fp0p0p1 + 3*fp1p0m1 - 3*fp1p0p1 - fp2p0m1 + fp2p0p1)/4.;" + NL
		        + ind + "double a011 = (fp0m1m1 - fp0m1p1 - fp0p1m1 + fp0p1p1)/4.;" + NL
		        + ind + "double a111 = (-fm1m1m1 + fm1m1p1 + fm1p1m1 - fm1p1p1 + fp1m1m1 - fp1m1p1 - fp1p1m1 + fp1p1p1)/8.;" + NL
		        + ind + "double a211 = (2*fm1m1m1 - 2*fm1m1p1 - 2*fm1p1m1 + 2*fm1p1p1 - 5*fp0m1m1 + 5*fp0m1p1 + 5*fp0p1m1 - 5*fp0p1p1"
		        + " + 4*fp1m1m1 - 4*fp1m1p1 - 4*fp1p1m1 + 4*fp1p1p1 - fp2m1m1 + fp2m1p1 + fp2p1m1 - fp2p1p1)/8.;" + NL
		        + ind + "double a311 = (-fm1m1m1 + fm1m1p1 + fm1p1m1 - fm1p1p1 + 3*fp0m1m1 - 3*fp0m1p1 - 3*fp0p1m1 + 3*fp0p1p1 - 3*fp1m1m1"
		        + " + 3*fp1m1p1 + 3*fp1p1m1 - 3*fp1p1p1 + fp2m1m1 - fp2m1p1 - fp2p1m1 + fp2p1p1)/8.;" + NL
		        + ind + "double a021 = (-2*fp0m1m1 + 2*fp0m1p1 + 5*fp0p0m1 - 5*fp0p0p1 - 4*fp0p1m1 + 4*fp0p1p1 + fp0p2m1 - fp0p2p1)/4.;" + NL
		        + ind + "double a121 = (2*fm1m1m1 - 2*fm1m1p1 - 5*fm1p0m1 + 5*fm1p0p1 + 4*fm1p1m1 - 4*fm1p1p1 - fm1p2m1 + fm1p2p1"
		        + " - 2*fp1m1m1 + 2*fp1m1p1 + 5*fp1p0m1 - 5*fp1p0p1 - 4*fp1p1m1 + 4*fp1p1p1 + fp1p2m1 - fp1p2p1)/8.;" + NL
		        + ind + "double a221 = (-4*fm1m1m1 + 4*fm1m1p1 + 10*fm1p0m1 - 10*fm1p0p1 - 8*fm1p1m1 + 8*fm1p1p1 + 2*fm1p2m1 - 2*fm1p2p1"
		        + " + 10*fp0m1m1 - 10*fp0m1p1 - 25*fp0p0m1 + 25*fp0p0p1 + 20*fp0p1m1 - 20*fp0p1p1 - 5*fp0p2m1 + 5*fp0p2p1 - 8*fp1m1m1"
		        + " + 8*fp1m1p1 + 20*fp1p0m1 - 20*fp1p0p1 - 16*fp1p1m1 + 16*fp1p1p1 + 4*fp1p2m1 - 4*fp1p2p1 + 2*fp2m1m1 - 2*fp2m1p1"
		        + " - 5*fp2p0m1 + 5*fp2p0p1 + 4*fp2p1m1 - 4*fp2p1p1 - fp2p2m1 + fp2p2p1)/8.;" + NL
		        + ind + "double a321 = (2*fm1m1m1 - 2*fm1m1p1 - 5*fm1p0m1 + 5*fm1p0p1 + 4*fm1p1m1 - 4*fm1p1p1 - fm1p2m1 + fm1p2p1"
		        + " - 6*fp0m1m1 + 6*fp0m1p1 + 15*fp0p0m1 - 15*fp0p0p1 - 12*fp0p1m1 + 12*fp0p1p1 + 3*fp0p2m1 - 3*fp0p2p1 + 6*fp1m1m1"
		        + " - 6*fp1m1p1 - 15*fp1p0m1 + 15*fp1p0p1 + 12*fp1p1m1 - 12*fp1p1p1 - 3*fp1p2m1 + 3*fp1p2p1 - 2*fp2m1m1 + 2*fp2m1p1 "
		        + "+ 5*fp2p0m1 - 5*fp2p0p1 - 4*fp2p1m1 + 4*fp2p1p1 + fp2p2m1 - fp2p2p1)/8.;" + NL
		        + ind + "double a031 = (fp0m1m1 - fp0m1p1 - 3*fp0p0m1 + 3*fp0p0p1 + 3*fp0p1m1 - 3*fp0p1p1 - fp0p2m1 + fp0p2p1)/4.;" + NL
		        + ind + "double a131 = (-fm1m1m1 + fm1m1p1 + 3*fm1p0m1 - 3*fm1p0p1 - 3*fm1p1m1 + 3*fm1p1p1 + fm1p2m1 - fm1p2p1 + fp1m1m1"
		        + " - fp1m1p1 - 3*fp1p0m1 + 3*fp1p0p1 + 3*fp1p1m1 - 3*fp1p1p1 - fp1p2m1 + fp1p2p1)/8.;" + NL
		        + ind + "double a231 = (2*fm1m1m1 - 2*fm1m1p1 - 6*fm1p0m1 + 6*fm1p0p1 + 6*fm1p1m1 - 6*fm1p1p1 - 2*fm1p2m1 + 2*fm1p2p1" 
		        + " - 5*fp0m1m1 + 5*fp0m1p1 + 15*fp0p0m1 - 15*fp0p0p1 - 15*fp0p1m1 + 15*fp0p1p1 + 5*fp0p2m1 - 5*fp0p2p1 + 4*fp1m1m1 " 
		        + "- 4*fp1m1p1 - 12*fp1p0m1 + 12*fp1p0p1 + 12*fp1p1m1 - 12*fp1p1p1 - 4*fp1p2m1 + 4*fp1p2p1 - fp2m1m1 + fp2m1p1 + 3*fp2p0m1"
		        + " - 3*fp2p0p1 - 3*fp2p1m1 + 3*fp2p1p1 + fp2p2m1 - fp2p2p1)/8.;" + NL
		        + ind + "double a331 = (-fm1m1m1 + fm1m1p1 + 3*fm1p0m1 - 3*fm1p0p1 - 3*fm1p1m1 + 3*fm1p1p1 + fm1p2m1 - fm1p2p1 + 3*fp0m1m1"
		        + " - 3*fp0m1p1 - 9*fp0p0m1 + 9*fp0p0p1 + 9*fp0p1m1 - 9*fp0p1p1 - 3*fp0p2m1 + 3*fp0p2p1 - 3*fp1m1m1 + 3*fp1m1p1 + 9*fp1p0m1"
		        + " - 9*fp1p0p1 - 9*fp1p1m1 + 9*fp1p1p1 + 3*fp1p2m1 - 3*fp1p2p1 + fp2m1m1 - fp2m1p1 - 3*fp2p0m1 + 3*fp2p0p1 + 3*fp2p1m1"
		        + " - 3*fp2p1p1 - fp2p2m1 + fp2p2p1)/8.;" + NL
		        + ind + "double a002 = fp0p0m1 - (5*fp0p0p0)/2. + 2*fp0p0p1 - fp0p0p2/2.;" + NL
		        + ind + "double a102 = (-2*fm1p0m1 + 5*fm1p0p0 - 4*fm1p0p1 + fm1p0p2 + 2*fp1p0m1 - 5*fp1p0p0 + 4*fp1p0p1 - fp1p0p2)/4.;" + NL
		        + ind + "double a202 = (4*fm1p0m1 - 10*fm1p0p0 + 8*fm1p0p1 - 2*fm1p0p2 - 10*fp0p0m1 + 25*fp0p0p0 - 20*fp0p0p1 + 5*fp0p0p2"
		        + " + 8*fp1p0m1 - 20*fp1p0p0 + 16*fp1p0p1 - 4*fp1p0p2 - 2*fp2p0m1 + 5*fp2p0p0 - 4*fp2p0p1 + fp2p0p2)/4.;" + NL
		        + ind + "double a302 = (-2*fm1p0m1 + 5*fm1p0p0 - 4*fm1p0p1 + fm1p0p2 + 6*fp0p0m1 - 15*fp0p0p0 + 12*fp0p0p1 - 3*fp0p0p2" 
		        + " - 6*fp1p0m1 + 15*fp1p0p0 - 12*fp1p0p1 + 3*fp1p0p2 + 2*fp2p0m1 - 5*fp2p0p0 + 4*fp2p0p1 - fp2p0p2)/4.;" + NL
		        + ind + "double a012 = (-2*fp0m1m1 + 5*fp0m1p0 - 4*fp0m1p1 + fp0m1p2 + 2*fp0p1m1 - 5*fp0p1p0 + 4*fp0p1p1 - fp0p1p2)/4.;" + NL
		        + ind + "double a112 = (2*fm1m1m1 - 5*fm1m1p0 + 4*fm1m1p1 - fm1m1p2 - 2*fm1p1m1 + 5*fm1p1p0 - 4*fm1p1p1 + fm1p1p2 " 
		        + "- 2*fp1m1m1 + 5*fp1m1p0 - 4*fp1m1p1 + fp1m1p2 + 2*fp1p1m1 - 5*fp1p1p0 + 4*fp1p1p1 - fp1p1p2)/8.;" + NL
		        + ind + "double a212 = (-4*fm1m1m1 + 10*fm1m1p0 - 8*fm1m1p1 + 2*fm1m1p2 + 4*fm1p1m1 - 10*fm1p1p0 + 8*fm1p1p1 - 2*fm1p1p2"
		        + " + 10*fp0m1m1 - 25*fp0m1p0 + 20*fp0m1p1 - 5*fp0m1p2 - 10*fp0p1m1 + 25*fp0p1p0 - 20*fp0p1p1 + 5*fp0p1p2 - 8*fp1m1m1 " 
		        + "+ 20*fp1m1p0 - 16*fp1m1p1 + 4*fp1m1p2 + 8*fp1p1m1 - 20*fp1p1p0 + 16*fp1p1p1 - 4*fp1p1p2 + 2*fp2m1m1 - 5*fp2m1p0 "
		        + "+ 4*fp2m1p1 - fp2m1p2 - 2*fp2p1m1 + 5*fp2p1p0 - 4*fp2p1p1 + fp2p1p2)/8.;" + NL
		        + ind + "double a312 = (2*fm1m1m1 - 5*fm1m1p0 + 4*fm1m1p1 - fm1m1p2 - 2*fm1p1m1 + 5*fm1p1p0 - 4*fm1p1p1 + fm1p1p2 " 
		        + "- 6*fp0m1m1 + 15*fp0m1p0 - 12*fp0m1p1 + 3*fp0m1p2 + 6*fp0p1m1 - 15*fp0p1p0 + 12*fp0p1p1 - 3*fp0p1p2 + 6*fp1m1m1 " 
		        + "- 15*fp1m1p0 + 12*fp1m1p1 - 3*fp1m1p2 - 6*fp1p1m1 + 15*fp1p1p0 - 12*fp1p1p1 + 3*fp1p1p2 - 2*fp2m1m1 + 5*fp2m1p0 " 
		        + "- 4*fp2m1p1 + fp2m1p2 + 2*fp2p1m1 - 5*fp2p1p0 + 4*fp2p1p1 - fp2p1p2)/8.;" + NL
		        + ind + "double a022 = (4*fp0m1m1 - 10*fp0m1p0 + 8*fp0m1p1 - 2*fp0m1p2 - 10*fp0p0m1 + 25*fp0p0p0 - 20*fp0p0p1 + 5*fp0p0p2 "
		        + "+ 8*fp0p1m1 - 20*fp0p1p0 + 16*fp0p1p1 - 4*fp0p1p2 - 2*fp0p2m1 + 5*fp0p2p0 - 4*fp0p2p1 + fp0p2p2)/4.;" + NL
		        + ind + "double a122 = (-4*fm1m1m1 + 10*fm1m1p0 - 8*fm1m1p1 + 2*fm1m1p2 + 10*fm1p0m1 - 25*fm1p0p0 + 20*fm1p0p1 - 5*fm1p0p2 "
		        + "- 8*fm1p1m1 + 20*fm1p1p0 - 16*fm1p1p1 + 4*fm1p1p2 + 2*fm1p2m1 - 5*fm1p2p0 + 4*fm1p2p1 - fm1p2p2 + 4*fp1m1m1 - 10*fp1m1p0"
		        + " + 8*fp1m1p1 - 2*fp1m1p2 - 10*fp1p0m1 + 25*fp1p0p0 - 20*fp1p0p1 + 5*fp1p0p2 + 8*fp1p1m1 - 20*fp1p1p0 + 16*fp1p1p1 "
		        + "- 4*fp1p1p2 - 2*fp1p2m1 + 5*fp1p2p0 - 4*fp1p2p1 + fp1p2p2)/8.;" + NL
		        + ind + "double a222 = (8*fm1m1m1 - 20*fm1m1p0 + 16*fm1m1p1 - 4*fm1m1p2 - 20*fm1p0m1 + 50*fm1p0p0 - 40*fm1p0p1 + 10*fm1p0p2"
		        + " + 16*fm1p1m1 - 40*fm1p1p0 + 32*fm1p1p1 - 8*fm1p1p2 - 4*fm1p2m1 + 10*fm1p2p0 - 8*fm1p2p1 + 2*fm1p2p2 - 20*fp0m1m1 " 
		        + "+ 50*fp0m1p0 - 40*fp0m1p1 + 10*fp0m1p2 + 50*fp0p0m1 - 125*fp0p0p0 + 100*fp0p0p1 - 25*fp0p0p2 - 40*fp0p1m1 + 100*fp0p1p0"
		        + " - 80*fp0p1p1 + 20*fp0p1p2 + 10*fp0p2m1 - 25*fp0p2p0 + 20*fp0p2p1 - 5*fp0p2p2 + 16*fp1m1m1 - 40*fp1m1p0 + 32*fp1m1p1 " 
		        + "- 8*fp1m1p2 - 40*fp1p0m1 + 100*fp1p0p0 - 80*fp1p0p1 + 20*fp1p0p2 + 32*fp1p1m1 - 80*fp1p1p0 + 64*fp1p1p1 - 16*fp1p1p2"
		        + " - 8*fp1p2m1 + 20*fp1p2p0 - 16*fp1p2p1 + 4*fp1p2p2 - 4*fp2m1m1 + 10*fp2m1p0 - 8*fp2m1p1 + 2*fp2m1p2 + 10*fp2p0m1"
		        + " - 25*fp2p0p0 + 20*fp2p0p1 - 5*fp2p0p2 - 8*fp2p1m1 + 20*fp2p1p0 - 16*fp2p1p1 + 4*fp2p1p2 + 2*fp2p2m1 - 5*fp2p2p0 " 
		        + "+ 4*fp2p2p1 - fp2p2p2)/8.;" + NL
		        + ind + "double a322 = (-4*fm1m1m1 + 10*fm1m1p0 - 8*fm1m1p1 + 2*fm1m1p2 + 10*fm1p0m1 - 25*fm1p0p0 + 20*fm1p0p1 - 5*fm1p0p2 "
		        + "- 8*fm1p1m1 + 20*fm1p1p0 - 16*fm1p1p1 + 4*fm1p1p2 + 2*fm1p2m1 - 5*fm1p2p0 + 4*fm1p2p1 - fm1p2p2 + 12*fp0m1m1 - 30*fp0m1p0"
		        + " + 24*fp0m1p1 - 6*fp0m1p2 - 30*fp0p0m1 + 75*fp0p0p0 - 60*fp0p0p1 + 15*fp0p0p2 + 24*fp0p1m1 - 60*fp0p1p0 + 48*fp0p1p1" 
		        + " - 12*fp0p1p2 - 6*fp0p2m1 + 15*fp0p2p0 - 12*fp0p2p1 + 3*fp0p2p2 - 12*fp1m1m1 + 30*fp1m1p0 - 24*fp1m1p1 + 6*fp1m1p2"
		        + " + 30*fp1p0m1 - 75*fp1p0p0 + 60*fp1p0p1 - 15*fp1p0p2 - 24*fp1p1m1 + 60*fp1p1p0 - 48*fp1p1p1 + 12*fp1p1p2 + 6*fp1p2m1"
		        + " - 15*fp1p2p0 + 12*fp1p2p1 - 3*fp1p2p2 + 4*fp2m1m1 - 10*fp2m1p0 + 8*fp2m1p1 - 2*fp2m1p2 - 10*fp2p0m1 + 25*fp2p0p0 " 
		        + "- 20*fp2p0p1 + 5*fp2p0p2 + 8*fp2p1m1 - 20*fp2p1p0 + 16*fp2p1p1 - 4*fp2p1p2 - 2*fp2p2m1 + 5*fp2p2p0 - 4*fp2p2p1 " 
		        + "+ fp2p2p2)/8.;" + NL
		        + ind + "double a032 = (-2*fp0m1m1 + 5*fp0m1p0 - 4*fp0m1p1 + fp0m1p2 + 6*fp0p0m1 - 15*fp0p0p0 + 12*fp0p0p1 - 3*fp0p0p2 "
		        + "- 6*fp0p1m1 + 15*fp0p1p0 - 12*fp0p1p1 + 3*fp0p1p2 + 2*fp0p2m1 - 5*fp0p2p0 + 4*fp0p2p1 - fp0p2p2)/4.;" + NL
		        + ind + "double a132 = (2*fm1m1m1 - 5*fm1m1p0 + 4*fm1m1p1 - fm1m1p2 - 6*fm1p0m1 + 15*fm1p0p0 - 12*fm1p0p1 + 3*fm1p0p2 "
		        + "+ 6*fm1p1m1 - 15*fm1p1p0 + 12*fm1p1p1 - 3*fm1p1p2 - 2*fm1p2m1 + 5*fm1p2p0 - 4*fm1p2p1 + fm1p2p2 - 2*fp1m1m1 + 5*fp1m1p0"
		        + " - 4*fp1m1p1 + fp1m1p2 + 6*fp1p0m1 - 15*fp1p0p0 + 12*fp1p0p1 - 3*fp1p0p2 - 6*fp1p1m1 + 15*fp1p1p0 - 12*fp1p1p1"
		        + " + 3*fp1p1p2 + 2*fp1p2m1 - 5*fp1p2p0 + 4*fp1p2p1 - fp1p2p2)/8.;" + NL
		        + ind + "double a232 = (-4*fm1m1m1 + 10*fm1m1p0 - 8*fm1m1p1 + 2*fm1m1p2 + 12*fm1p0m1 - 30*fm1p0p0 + 24*fm1p0p1 - 6*fm1p0p2"
		        + " - 12*fm1p1m1 + 30*fm1p1p0 - 24*fm1p1p1 + 6*fm1p1p2 + 4*fm1p2m1 - 10*fm1p2p0 + 8*fm1p2p1 - 2*fm1p2p2 + 10*fp0m1m1"
		        + " - 25*fp0m1p0 + 20*fp0m1p1 - 5*fp0m1p2 - 30*fp0p0m1 + 75*fp0p0p0 - 60*fp0p0p1 + 15*fp0p0p2 + 30*fp0p1m1 - 75*fp0p1p0" 
		        + " + 60*fp0p1p1 - 15*fp0p1p2 - 10*fp0p2m1 + 25*fp0p2p0 - 20*fp0p2p1 + 5*fp0p2p2 - 8*fp1m1m1 + 20*fp1m1p0 - 16*fp1m1p1"
		        + " + 4*fp1m1p2 + 24*fp1p0m1 - 60*fp1p0p0 + 48*fp1p0p1 - 12*fp1p0p2 - 24*fp1p1m1 + 60*fp1p1p0 - 48*fp1p1p1 + 12*fp1p1p2"
		        + " + 8*fp1p2m1 - 20*fp1p2p0 + 16*fp1p2p1 - 4*fp1p2p2 + 2*fp2m1m1 - 5*fp2m1p0 + 4*fp2m1p1 - fp2m1p2 - 6*fp2p0m1"
		        + " + 15*fp2p0p0 - 12*fp2p0p1 + 3*fp2p0p2 + 6*fp2p1m1 - 15*fp2p1p0 + 12*fp2p1p1 - 3*fp2p1p2 - 2*fp2p2m1 + 5*fp2p2p0"
		        + " - 4*fp2p2p1 + fp2p2p2)/8.;" + NL
		        + ind + "double a332 = (2*fm1m1m1 - 5*fm1m1p0 + 4*fm1m1p1 - fm1m1p2 - 6*fm1p0m1 + 15*fm1p0p0 - 12*fm1p0p1 + 3*fm1p0p2"
		        + " + 6*fm1p1m1 - 15*fm1p1p0 + 12*fm1p1p1 - 3*fm1p1p2 - 2*fm1p2m1 + 5*fm1p2p0 - 4*fm1p2p1 + fm1p2p2 - 6*fp0m1m1"
		        + " + 15*fp0m1p0 - 12*fp0m1p1 + 3*fp0m1p2 + 18*fp0p0m1 - 45*fp0p0p0 + 36*fp0p0p1 - 9*fp0p0p2 - 18*fp0p1m1 + 45*fp0p1p0"
		        + " - 36*fp0p1p1 + 9*fp0p1p2 + 6*fp0p2m1 - 15*fp0p2p0 + 12*fp0p2p1 - 3*fp0p2p2 + 6*fp1m1m1 - 15*fp1m1p0 + 12*fp1m1p1" 
		        + " - 3*fp1m1p2 - 18*fp1p0m1 + 45*fp1p0p0 - 36*fp1p0p1 + 9*fp1p0p2 + 18*fp1p1m1 - 45*fp1p1p0 + 36*fp1p1p1 - 9*fp1p1p2 " 
		        + "- 6*fp1p2m1 + 15*fp1p2p0 - 12*fp1p2p1 + 3*fp1p2p2 - 2*fp2m1m1 + 5*fp2m1p0 - 4*fp2m1p1 + fp2m1p2 + 6*fp2p0m1 - 15*fp2p0p0"
		        + " + 12*fp2p0p1 - 3*fp2p0p2 - 6*fp2p1m1 + 15*fp2p1p0 - 12*fp2p1p1 + 3*fp2p1p2 + 2*fp2p2m1 - 5*fp2p2p0 + 4*fp2p2p1" 
		        + " - fp2p2p2)/8.;" + NL
		        + ind + "double a003 = (-fp0p0m1 + 3*fp0p0p0 - 3*fp0p0p1 + fp0p0p2)/2.;" + NL
		        + ind + "double a103 = (fm1p0m1 - 3*fm1p0p0 + 3*fm1p0p1 - fm1p0p2 - fp1p0m1 + 3*fp1p0p0 - 3*fp1p0p1 + fp1p0p2)/4.;" + NL
		        + ind + "double a203 = (-2*fm1p0m1 + 6*fm1p0p0 - 6*fm1p0p1 + 2*fm1p0p2 + 5*fp0p0m1 - 15*fp0p0p0 + 15*fp0p0p1 - 5*fp0p0p2"
		        + " - 4*fp1p0m1 + 12*fp1p0p0 - 12*fp1p0p1 + 4*fp1p0p2 + fp2p0m1 - 3*fp2p0p0 + 3*fp2p0p1 - fp2p0p2)/4.;" + NL
		        + ind + "double a303 = (fm1p0m1 - 3*fm1p0p0 + 3*fm1p0p1 - fm1p0p2 - 3*fp0p0m1 + 9*fp0p0p0 - 9*fp0p0p1 + 3*fp0p0p2 "
		        + "+ 3*fp1p0m1 - 9*fp1p0p0 + 9*fp1p0p1 - 3*fp1p0p2 - fp2p0m1 + 3*fp2p0p0 - 3*fp2p0p1 + fp2p0p2)/4.;" + NL
		        + ind + "double a013 = (fp0m1m1 - 3*fp0m1p0 + 3*fp0m1p1 - fp0m1p2 - fp0p1m1 + 3*fp0p1p0 - 3*fp0p1p1 + fp0p1p2)/4.;" + NL
		        + ind + "double a113 = (-fm1m1m1 + 3*fm1m1p0 - 3*fm1m1p1 + fm1m1p2 + fm1p1m1 - 3*fm1p1p0 + 3*fm1p1p1 - fm1p1p2 + fp1m1m1"
		        + " - 3*fp1m1p0 + 3*fp1m1p1 - fp1m1p2 - fp1p1m1 + 3*fp1p1p0 - 3*fp1p1p1 + fp1p1p2)/8.;" + NL
		        + ind + "double a213 = (2*fm1m1m1 - 6*fm1m1p0 + 6*fm1m1p1 - 2*fm1m1p2 - 2*fm1p1m1 + 6*fm1p1p0 - 6*fm1p1p1 + 2*fm1p1p2 " 
		        + "- 5*fp0m1m1 + 15*fp0m1p0 - 15*fp0m1p1 + 5*fp0m1p2 + 5*fp0p1m1 - 15*fp0p1p0 + 15*fp0p1p1 - 5*fp0p1p2 + 4*fp1m1m1 " 
		        + "- 12*fp1m1p0 + 12*fp1m1p1 - 4*fp1m1p2 - 4*fp1p1m1 + 12*fp1p1p0 - 12*fp1p1p1 + 4*fp1p1p2 - fp2m1m1 + 3*fp2m1p0 " 
		        + "- 3*fp2m1p1 + fp2m1p2 + fp2p1m1 - 3*fp2p1p0 + 3*fp2p1p1 - fp2p1p2)/8.;" + NL
		        + ind + "double a313 = (-fm1m1m1 + 3*fm1m1p0 - 3*fm1m1p1 + fm1m1p2 + fm1p1m1 - 3*fm1p1p0 + 3*fm1p1p1 - fm1p1p2 + 3*fp0m1m1"
		        + " - 9*fp0m1p0 + 9*fp0m1p1 - 3*fp0m1p2 - 3*fp0p1m1 + 9*fp0p1p0 - 9*fp0p1p1 + 3*fp0p1p2 - 3*fp1m1m1 + 9*fp1m1p0 - 9*fp1m1p1"
		        + " + 3*fp1m1p2 + 3*fp1p1m1 - 9*fp1p1p0 + 9*fp1p1p1 - 3*fp1p1p2 + fp2m1m1 - 3*fp2m1p0 + 3*fp2m1p1 - fp2m1p2 - fp2p1m1 " 
		        + "+ 3*fp2p1p0 - 3*fp2p1p1 + fp2p1p2)/8.;" + NL
		        + ind + "double a023 = (-2*fp0m1m1 + 6*fp0m1p0 - 6*fp0m1p1 + 2*fp0m1p2 + 5*fp0p0m1 - 15*fp0p0p0 + 15*fp0p0p1 - 5*fp0p0p2 "
		        + "- 4*fp0p1m1 + 12*fp0p1p0 - 12*fp0p1p1 + 4*fp0p1p2 + fp0p2m1 - 3*fp0p2p0 + 3*fp0p2p1 - fp0p2p2)/4.;" + NL
		        + ind + "double a123 = (2*fm1m1m1 - 6*fm1m1p0 + 6*fm1m1p1 - 2*fm1m1p2 - 5*fm1p0m1 + 15*fm1p0p0 - 15*fm1p0p1 + 5*fm1p0p2 " 
		        + "+ 4*fm1p1m1 - 12*fm1p1p0 + 12*fm1p1p1 - 4*fm1p1p2 - fm1p2m1 + 3*fm1p2p0 - 3*fm1p2p1 + fm1p2p2 - 2*fp1m1m1 + 6*fp1m1p0 "
		        + "- 6*fp1m1p1 + 2*fp1m1p2 + 5*fp1p0m1 - 15*fp1p0p0 + 15*fp1p0p1 - 5*fp1p0p2 - 4*fp1p1m1 + 12*fp1p1p0 - 12*fp1p1p1 " 
		        + "+ 4*fp1p1p2 + fp1p2m1 - 3*fp1p2p0 + 3*fp1p2p1 - fp1p2p2)/8.;" + NL
		        + ind + "double a223 = (-4*fm1m1m1 + 12*fm1m1p0 - 12*fm1m1p1 + 4*fm1m1p2 + 10*fm1p0m1 - 30*fm1p0p0 + 30*fm1p0p1 " 
		        + "- 10*fm1p0p2 - 8*fm1p1m1 + 24*fm1p1p0 - 24*fm1p1p1 + 8*fm1p1p2 + 2*fm1p2m1 - 6*fm1p2p0 + 6*fm1p2p1 - 2*fm1p2p2 " 
		        + "+ 10*fp0m1m1 - 30*fp0m1p0 + 30*fp0m1p1 - 10*fp0m1p2 - 25*fp0p0m1 + 75*fp0p0p0 - 75*fp0p0p1 + 25*fp0p0p2 + 20*fp0p1m1 "
		        + "- 60*fp0p1p0 + 60*fp0p1p1 - 20*fp0p1p2 - 5*fp0p2m1 + 15*fp0p2p0 - 15*fp0p2p1 + 5*fp0p2p2 - 8*fp1m1m1 + 24*fp1m1p0 "
		        + "- 24*fp1m1p1 + 8*fp1m1p2 + 20*fp1p0m1 - 60*fp1p0p0 + 60*fp1p0p1 - 20*fp1p0p2 - 16*fp1p1m1 + 48*fp1p1p0 - 48*fp1p1p1 "
		        + "+ 16*fp1p1p2 + 4*fp1p2m1 - 12*fp1p2p0 + 12*fp1p2p1 - 4*fp1p2p2 + 2*fp2m1m1 - 6*fp2m1p0 + 6*fp2m1p1 - 2*fp2m1p2 " 
		        + "- 5*fp2p0m1 + 15*fp2p0p0 - 15*fp2p0p1 + 5*fp2p0p2 + 4*fp2p1m1 - 12*fp2p1p0 + 12*fp2p1p1 - 4*fp2p1p2 - fp2p2m1 " 
		        + "+ 3*fp2p2p0 - 3*fp2p2p1 + fp2p2p2)/8.;" + NL
		        + ind + "double a323 = (2*fm1m1m1 - 6*fm1m1p0 + 6*fm1m1p1 - 2*fm1m1p2 - 5*fm1p0m1 + 15*fm1p0p0 - 15*fm1p0p1 + 5*fm1p0p2 "
		        + "+ 4*fm1p1m1 - 12*fm1p1p0 + 12*fm1p1p1 - 4*fm1p1p2 - fm1p2m1 + 3*fm1p2p0 - 3*fm1p2p1 + fm1p2p2 - 6*fp0m1m1 + 18*fp0m1p0" 
		        + " - 18*fp0m1p1 + 6*fp0m1p2 + 15*fp0p0m1 - 45*fp0p0p0 + 45*fp0p0p1 - 15*fp0p0p2 - 12*fp0p1m1 + 36*fp0p1p0 - 36*fp0p1p1 "
		        + "+ 12*fp0p1p2 + 3*fp0p2m1 - 9*fp0p2p0 + 9*fp0p2p1 - 3*fp0p2p2 + 6*fp1m1m1 - 18*fp1m1p0 + 18*fp1m1p1 - 6*fp1m1p2 " 
		        + "- 15*fp1p0m1 + 45*fp1p0p0 - 45*fp1p0p1 + 15*fp1p0p2 + 12*fp1p1m1 - 36*fp1p1p0 + 36*fp1p1p1 - 12*fp1p1p2 - 3*fp1p2m1 "
		        + "+ 9*fp1p2p0 - 9*fp1p2p1 + 3*fp1p2p2 - 2*fp2m1m1 + 6*fp2m1p0 - 6*fp2m1p1 + 2*fp2m1p2 + 5*fp2p0m1 - 15*fp2p0p0 " 
		        + "+ 15*fp2p0p1 - 5*fp2p0p2 - 4*fp2p1m1 + 12*fp2p1p0 - 12*fp2p1p1 + 4*fp2p1p2 + fp2p2m1 - 3*fp2p2p0 + 3*fp2p2p1 " 
		        + "- fp2p2p2)/8.;" + NL
		        + ind + "double a033 = (fp0m1m1 - 3*fp0m1p0 + 3*fp0m1p1 - fp0m1p2 - 3*fp0p0m1 + 9*fp0p0p0 - 9*fp0p0p1 + 3*fp0p0p2 " 
		        + "+ 3*fp0p1m1 - 9*fp0p1p0 + 9*fp0p1p1 - 3*fp0p1p2 - fp0p2m1 + 3*fp0p2p0 - 3*fp0p2p1 + fp0p2p2)/4.;" + NL
		        + ind + "double a133 = (-fm1m1m1 + 3*fm1m1p0 - 3*fm1m1p1 + fm1m1p2 + 3*fm1p0m1 - 9*fm1p0p0 + 9*fm1p0p1 - 3*fm1p0p2 " 
		        + "- 3*fm1p1m1 + 9*fm1p1p0 - 9*fm1p1p1 + 3*fm1p1p2 + fm1p2m1 - 3*fm1p2p0 + 3*fm1p2p1 - fm1p2p2 + fp1m1m1 - 3*fp1m1p0 "
		        + "+ 3*fp1m1p1 - fp1m1p2 - 3*fp1p0m1 + 9*fp1p0p0 - 9*fp1p0p1 + 3*fp1p0p2 + 3*fp1p1m1 - 9*fp1p1p0 + 9*fp1p1p1 - 3*fp1p1p2 "
		        + "- fp1p2m1 + 3*fp1p2p0 - 3*fp1p2p1 + fp1p2p2)/8.;" + NL
		        + ind + "double a233 = (2*fm1m1m1 - 6*fm1m1p0 + 6*fm1m1p1 - 2*fm1m1p2 - 6*fm1p0m1 + 18*fm1p0p0 - 18*fm1p0p1 + 6*fm1p0p2"
		        + " + 6*fm1p1m1 - 18*fm1p1p0 + 18*fm1p1p1 - 6*fm1p1p2 - 2*fm1p2m1 + 6*fm1p2p0 - 6*fm1p2p1 + 2*fm1p2p2 - 5*fp0m1m1 " 
		        + "+ 15*fp0m1p0 - 15*fp0m1p1 + 5*fp0m1p2 + 15*fp0p0m1 - 45*fp0p0p0 + 45*fp0p0p1 - 15*fp0p0p2 - 15*fp0p1m1 + 45*fp0p1p0 "
		        + "- 45*fp0p1p1 + 15*fp0p1p2 + 5*fp0p2m1 - 15*fp0p2p0 + 15*fp0p2p1 - 5*fp0p2p2 + 4*fp1m1m1 - 12*fp1m1p0 + 12*fp1m1p1 " 
		        + "- 4*fp1m1p2 - 12*fp1p0m1 + 36*fp1p0p0 - 36*fp1p0p1 + 12*fp1p0p2 + 12*fp1p1m1 - 36*fp1p1p0 + 36*fp1p1p1 - 12*fp1p1p2 "
		        + "- 4*fp1p2m1 + 12*fp1p2p0 - 12*fp1p2p1 + 4*fp1p2p2 - fp2m1m1 + 3*fp2m1p0 - 3*fp2m1p1 + fp2m1p2 + 3*fp2p0m1 - 9*fp2p0p0 "
		        + "+ 9*fp2p0p1 - 3*fp2p0p2 - 3*fp2p1m1 + 9*fp2p1p0 - 9*fp2p1p1 + 3*fp2p1p2 + fp2p2m1 - 3*fp2p2p0 + 3*fp2p2p1 - fp2p2p2)/8.;" + NL
		        + ind + "double a333 = (-fm1m1m1 + 3*fm1m1p0 - 3*fm1m1p1 + fm1m1p2 + 3*fm1p0m1 - 9*fm1p0p0 + 9*fm1p0p1 - 3*fm1p0p2 " 
		        + "- 3*fm1p1m1 + 9*fm1p1p0 - 9*fm1p1p1 + 3*fm1p1p2 + fm1p2m1 - 3*fm1p2p0 + 3*fm1p2p1 - fm1p2p2 + 3*fp0m1m1 - 9*fp0m1p0 " 
		        + "+ 9*fp0m1p1 - 3*fp0m1p2 - 9*fp0p0m1 + 27*fp0p0p0 - 27*fp0p0p1 + 9*fp0p0p2 + 9*fp0p1m1 - 27*fp0p1p0 + 27*fp0p1p1 " 
		        + "- 9*fp0p1p2 - 3*fp0p2m1 + 9*fp0p2p0 - 9*fp0p2p1 + 3*fp0p2p2 - 3*fp1m1m1 + 9*fp1m1p0 - 9*fp1m1p1 + 3*fp1m1p2 + 9*fp1p0m1"
		        + " - 27*fp1p0p0 + 27*fp1p0p1 - 9*fp1p0p2 - 9*fp1p1m1 + 27*fp1p1p0 - 27*fp1p1p1 + 9*fp1p1p2 + 3*fp1p2m1 - 9*fp1p2p0 " 
		        + "+ 9*fp1p2p1 - 3*fp1p2p2 + fp2m1m1 - 3*fp2m1p0 + 3*fp2m1p1 - fp2m1p2 - 3*fp2p0m1 + 9*fp2p0p0 - 9*fp2p0p1 + 3*fp2p0p2 "
		        + "+ 3*fp2p1m1 - 9*fp2p1p0 + 9*fp2p1p1 - 3*fp2p1p2 - fp2p2m1 + 3*fp2p2p0 - 3*fp2p2p1 + fp2p2p2)/8.;" + NL
		        + interpolation;
    }
    
    /**
     * Check whether two rules of code are unifiable.
     * @param intFields         The interaction fields
     * @param rPost             The posterior rule
     * @param rPre              The previous rule
     * @param variables         The variables in the problem
     * @param dims              Number of dimensions
     * @return                  True if unifiable
     * @throws CGException      CG00X External error
     */
    public static boolean checkUnifiable(ArrayList<String> intFields, Element rPost, Element rPre, ArrayList<String> variables, 
            int dims) 
        throws CGException {
        Document doc = rPost.getOwnerDocument();
        String ruleNamePost = rPost.getTextContent();
        String ruleNamePre = rPre.getTextContent();
        if (ruleNamePre.equals("Topology change") || ruleNamePost.equals("Topology change")) {
            return false;
        }
        
        Element rulePost = (Element) find(doc, "//*[(local-name() = 'updateRule' or local-name() = 'gatherRule') and descendant::mms:name = '"
                + ruleNamePost + "']").item(0);
        Element rulePre = (Element) find(doc, "//*[(local-name() = 'updateRule' or local-name() = 'gatherRule') and descendant::mms:name = '"
                + ruleNamePre + "']").item(0);
        String typePost = rulePost.getLocalName();
        String typePre = rulePre.getLocalName();
        if (!typePost.equals(typePre)) {
            return false;
        }
        if (intFields != null) {
            if (variableIncompatibilityInterElements(intFields, rulePre, rulePost)) {
                return false;
            }
        }

        if (typePost.equals(typePre) && typePost.equals("updateRule")) {
            return true;
        }
        
        ArrayList<String> syncVars = SAMRAIUtils.getSyncVariables(rulePre, variables, dims, ExtrapolationType.generalPDE);
        ArrayList<String> rightVars = SAMRAIUtils.getRightVariables(rulePost, variables);
        for (int i = 0; i < syncVars.size(); i++) {
            if (rightVars.contains(syncVars.get(i))) {
                return false;
            }
        }
        return true;
    }
    
    /**
     * Gets the instruction set from a rule in ABM.
     * @param problem       The problem
     * @param ruleName      The rule name
     * @return              The instruction set
     * @throws CGException  CG00X External error
     */
    public static Element getRuleCode(Document problem, String ruleName) throws CGException {
        
        if (ruleName.equals("Topology change")) {
            return (Element) CodeGeneratorUtils.find(problem, "//mms:topologyChange//sml:algorithm").item(0);  
        }
        
        return (Element) CodeGeneratorUtils.find(problem, "//*[(local-name() = 'updateRule' or local-name() = 'gatherRule') and descendant::"
                + "mms:name = '" + ruleName + "']//sml:algorithm").item(0);
    }
    
    /**
     * Gets the type from a rule in ABM.
     * @param problem       The problem
     * @param ruleName      The rule name
     * @return              The type of the rule
     * @throws CGException  CG00X External error
     */
    public static String getRuleType(Document problem, String ruleName) throws CGException {
        if (ruleName.equals("Topology change")) {
            return "topology";
        }
        return CodeGeneratorUtils.find(problem, "//*[(local-name() = 'updateRule' or local-name() = 'gatherRule') and descendant::"
                + "mms:name = '" + ruleName + "']").item(0).getLocalName();
    }
    
    /**
     * Checks when two elements have incompatible read/write variables for a possible unification for specific variables. 
     * @param vars          The list of variables.
     * @param elementPre    The previous element
     * @param elementPost   The posterior element
     * @return              True if incompatible
     * @throws CGException  CG00X External error
     */
    public static boolean variableIncompatibilityInterElements(ArrayList<String> vars, Element elementPre, Element elementPost) throws CGException {
        ArrayList<String> assignedIn = BOOSTABM_graphCode.getAssignedVarsInteraction(vars, elementPre, "in");
        ArrayList<String> assignedOut = BOOSTABM_graphCode.getAssignedVarsInteraction(vars, elementPre, "out");
        if (BOOSTABM_graphCode.checkUsed(elementPost, assignedOut, "in") 
                || BOOSTABM_graphCode.checkUsed(elementPost, assignedIn, "out")) {
            return true;
        }
        return false;
    }
    
    /**
     * Checks when two elements have incompatible read/write variables for a possible unification.
     * @param elementPre    The previous element
     * @param elementPost   The posterior element
     * @return              True if incompatible
     * @throws CGException  CG00X External error
     */
    public static boolean variableIncompatibility(Element elementPre, Element elementPost) throws CGException {
        ArrayList<String> assignedIn = BOOSTABM_graphCode.getAssigned(elementPre, "in");
        ArrayList<String> assignedOut = BOOSTABM_graphCode.getAssigned(elementPre, "out");
        if (BOOSTABM_graphCode.checkUsed(elementPost, assignedOut, "out") 
                || BOOSTABM_graphCode.checkUsed(elementPost, assignedIn, "in")) {
            return true;
        }
        
        return false;
    }
    
    /**
     * Deploys all the spatial coordinates as XML.
     * @param doc       The problem
     * @param coords    The spatial coordinates
     * @return          The xml coordinates
     */
    public static DocumentFragment deployCoordinates(Document doc, ArrayList<String> coords) {
        DocumentFragment result = doc.createDocumentFragment();
        for (int i = 0; i < coords.size(); i++) {
            Element coord = createElement(doc, MTURI, "ci");
            coord.setTextContent(coords.get(i));
            result.appendChild(coord);
        }
        return result;
    }
    
    /**
     * Deploys all the spatial coordinates with an operation in a given coordinate as XML.
     * @param doc           The problem
     * @param coords        The spatial coordinates
     * @param coordinate    The coordinate to operate to
     * @param operation     The kind of operation to apply
     * @param value         The value to operate
     * @return              The xml coordinates
     */
    public static DocumentFragment deployOperatedCoordinates(Document doc, ArrayList<String> coords, String coordinate, String operation, int value) {
        DocumentFragment result = doc.createDocumentFragment();
        for (int i = 0; i < coords.size(); i++) {
            Element coord = createElement(doc, MTURI, "ci");
            coord.setTextContent(coords.get(i));
            if (coords.get(i).equals(coordinate)) {
                Element apply = createElement(doc, MTURI, "apply");
                Element op = createElement(doc, MTURI, operation);
                Element num = createElement(doc, MTURI, "cn");
                num.setTextContent(String.valueOf(value));
                apply.appendChild(op);
                apply.appendChild(coord);
                apply.appendChild(num);
                result.appendChild(apply);
            }
            else {
                result.appendChild(coord);
            }
        }
        return result;
    }
    
    /**
     * Unifies the instructions in a document fragment recursively.
     * @param instructions      The instruction set
     * @param pi                The problem info
     * @return                  The instruction set unified
     * @throws CGException      CG00X External error
     */
    public static DocumentFragment unifyInstructions(DocumentFragment instructions, ProblemInfo pi) throws CGException {
        DocumentFragment result = instructions.getOwnerDocument().createDocumentFragment();
        NodeList oChildren = instructions.getChildNodes();
        while (oChildren.getLength() > 0) {   
            Element ochild = (Element) oChildren.item(0);
            result.appendChild(ochild);
            String tag = ochild.getLocalName();
            //Unify specific instructions
            if (tag.equals("if") || tag.equals("while") || tag.equals("iterateOverEdges") || tag.equals("iterateOverInteractions") 
                    || tag.equals("iterateOverAgents") || tag.equals("iterateOverVertices")) {
                while (oChildren.getLength() > 0) {
                    Element candidate = (Element) oChildren.item(0);
                    String candidateTag = candidate.getLocalName();
                    if (tag.equals(candidateTag) 
                        && ((tag.equals("if") && ochild.getFirstChild().isEqualNode(candidate.getFirstChild())) 
                        || (tag.equals("while") && ochild.getFirstChild().isEqualNode(candidate.getFirstChild())) 
                        || ((tag.equals("iterateOverEdges") || tag.equals("iterateOverInteractions")) 
                                && !variableIncompatibility(ochild, candidate) 
                            && (((ochild.getFirstChild().isEqualNode(candidate.getFirstChild())) && pi.isHasABMMeshlessFields())
                            || (!pi.isHasABMMeshlessFields() 
                                && ochild.getAttribute("directionAtt").equals(candidate.getAttribute("directionAtt"))))) 
                        || tag.equals("iterateOverAgents") || tag.equals("iterateOverVertices"))) {
                        Element newChild = (Element) ochild.cloneNode(false);
                        int begin = 0;
                        if (tag.equals("if") || tag.equals("while") || ((tag.equals("iterateOverEdges") || tag.equals("iterateOverInteractions")) && pi.isHasABMMeshlessFields())) {
                            newChild.appendChild(candidate.getChildNodes().item(0).cloneNode(true));
                            //Add "then" or "loop" tag
                            if (tag.equals("if") || tag.equals("while")) {
                                newChild.appendChild(candidate.getChildNodes().item(1).cloneNode(false));
                            }
                            else {
                                begin = 1;
                            }
                        }
                        DocumentFragment instructionsToUnify = instructions.getOwnerDocument().createDocumentFragment();
                        NodeList ochildChildren;
                        if (tag.equals("if") || tag.equals("while")) {
                            ochildChildren = ochild.getLastChild().getChildNodes();
                        }
                        else {
                            ochildChildren = ochild.getChildNodes();
                        }
                        for (int k = begin; k < ochildChildren.getLength(); k++) {
                            instructionsToUnify.appendChild(ochildChildren.item(k).cloneNode(true));
                        }
                        NodeList candidateChildren;
                        if (tag.equals("if") || tag.equals("while")) {
                            candidateChildren = candidate.getLastChild().getChildNodes();
                        }
                        else {
                            candidateChildren = candidate.getChildNodes();
                        }
                        for (int k = begin; k < candidateChildren.getLength(); k++) {
                            instructionsToUnify.appendChild(candidateChildren.item(k).cloneNode(true));
                        }
                        DocumentFragment unified = unifyInstructions(instructionsToUnify, pi);
                        for (int k = 0; k < unified.getChildNodes().getLength(); k++) {
                            if (tag.equals("if") || tag.equals("while")) {
                                newChild.getLastChild().appendChild(unified.getChildNodes().item(k).cloneNode(true));
                            }
                            else {
                                newChild.appendChild(unified.getChildNodes().item(k).cloneNode(true));
                            }
                        }
                        result.replaceChild(newChild, result.getLastChild());
                        instructions.removeChild(candidate);
                        ochild = newChild;
                    }
                    else {
                        break;
                    }
                }
            }
        }
        return result;
    }
    
    /**
     * Gets the degree distribution of the graph.
     * @param problem           The problem
     * @return                  The degree distribution if available
     * @throws CGException      CG001 XML document is not valid
     */
    public static String getDegreeDistribution(Document problem) throws CGException {
        try {
            if (find(problem, "/*/mms:graphDefinition/mms:externalSpecification").getLength() > 0) {
                return "ExternalSpecification";
            }
            return find(problem, "/*/mms:graphDefinition/mms:topologicalSpecification/mms:degreeDistribution").item(0).getTextContent();
        }
        catch (CGException e) {
            throw new CGException(CGException.CG001, "Incorrect graph definition");
        }
    }
    
    /**
     * Gets the edge directionality of the graph.
     * @param problem           The problem
     * @return                  The edge directionality if available
     * @throws CGException      CG001 XML document is not valid
     */
    public static String getEdgeDirectionality(Document problem) throws CGException {
        try {
            if (find(problem, "/*/mms:graphDefinition/mms:externalSpecification").getLength() > 0) {
                return null;
            }
            return find(problem, "/*/mms:graphDefinition/mms:topologicalSpecification/mms:edgeDirectionality").item(0).getTextContent();
        } 
        catch (CGException e) {
            throw new CGException(CGException.CG001, "Incorrect graph definition.");
        }
    }
    
    /**
     * Creates a generatedCode document with the code id.
     * @param name          The name for the header
     * @param author        The author for the header
     * @param version       The version for the header
     * @param date          The date for the header
     * @param platform      The platform for the header
     * @param id            The code id
     * @return              A generatedCode document
     * @throws CGException  CG00X External error
     */
    public static String createCodeDocument(String name, String author, String version, Date date, String platform, String id) throws CGException {
        //Creation of the problem result skeleton
        DocumentBuilder builder;
        try {
            builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document document = builder.newDocument();
            
            //Root
            String rootName = "generatedCode";
            Element root = createElement(document, MMSURI, rootName);
            document.appendChild(root);
            
            //The header are the first child of the root element.
            Element header = createElement(document, MMSURI, "head");
            root.appendChild(header);
            //Name
            Element nameE = createElement(document, MMSURI, "name");
            nameE.setTextContent(name);
            header.appendChild(nameE);
            //Empty id
            Element idE = createElement(document, MMSURI, "id");
            header.appendChild(idE);
            //Author
            Element authorE = createElement(document, MMSURI, "author");
            authorE.setTextContent(author);
            header.appendChild(authorE);
            //Version
            Element versionE = createElement(document, MMSURI, "version");
            versionE.setTextContent(version);
            header.appendChild(versionE);
            //Date
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            Element dateE = createElement(document, MMSURI, "date");
            dateE.setTextContent(formatter.format(date));
            header.appendChild(dateE);
            //Platform
            Element platformE = createElement(document, MMSURI, "platform");
            platformE.setTextContent(platform);
            header.appendChild(platformE);
            
            //Code id
            Element codeId = createElement(document, MMSURI, "codeId");
            codeId.setTextContent(id);
            root.appendChild(codeId);
            
            return domToString(document);
        } 
        catch (ParserConfigurationException e) {
            throw new CGException(CGException.CG00X, e.getMessage());
        }
    }
    
    
    /**
     * An auxiliar function to read a file as String skiping a number of lines from the begining.
     * 
     * @param is                                The input stream of the file
     * @param skipLines                         The number of lines to skip
     * @return                                  The resource as string
     * @throws CGException                      Incorrect encoding of the file
     */
    public static String convertStreamToString(InputStream is, int skipLines) throws CGException {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            StringBuilder sb = new StringBuilder();
            String line = null;
            try {
                int skip = 0;
                while ((line = reader.readLine()) != null) {
                    if (skip == skipLines) {
                        sb.append(line + "\n");
                    }
                    else {
                        skip++;
                    }
                }
            } 
            catch (IOException e) {
                e.printStackTrace();
            } 
            finally {
                try {
                    is.close();
                } 
                catch (IOException e) { 
                    e.printStackTrace();
                }
            }
            return sb.toString();
        }
        catch (Exception e) {
            throw new CGException(CGException.CG00X, e.getMessage());
        }
    }
    
    /**
     * Creates a makefile to compile manually.
     * @param compilationCmd    The compilation command
     * @return                  The file
     * @throws CGException      CG00X External error
     */
    public static String createMakefile(String[] compilationCmd) throws CGException {
        String makefile = "default:" + NL + IND;
        for (int i = 0; i < compilationCmd.length; i++) {
            makefile = makefile + compilationCmd[i] + " ";
        }
        
        return makefile;
    }
    
    /**
     * Creates a running script for the code.
     * @param problemIdentifier The name of the problem
     * @return                  The script code
     * @throws CGException      CG003 Not possible to create code for the platform
     *                          CG00X External error
     */
    public static String runSh(String problemIdentifier) throws CGException {
        String problemName = SAMRAIUtils.variableNormalize(problemIdentifier);
        return "make" + NL
                    + "./" + problemName + " problem.input";
    }
    
    /**
     * Expression evaluator. Evaluates a simple mathematical expression.
     * @param str       The input expression
     * @return          The value
     */
    public static double eval(final String str) {
        return new Object() {
            int pos = -1, ch;

            void nextChar() {
                ch = (++pos < str.length()) ? str.charAt(pos) : -1;
            }

            boolean eat(int charToEat) {
                while (ch == ' ') nextChar();
                if (ch == charToEat) {
                    nextChar();
                    return true;
                }
                return false;
            }

            double parse() {
                nextChar();
                double x = parseExpression();
                if (pos < str.length()) throw new RuntimeException("Unexpected: " + (char)ch);
                return x;
            }

            // Grammar:
            // expression = term | expression `+` term | expression `-` term
            // term = factor | term `*` factor | term `/` factor
            // factor = `+` factor | `-` factor | `(` expression `)`
            //        | number | functionName factor | factor `^` factor

            double parseExpression() {
                double x = parseTerm();
                for ( ; ; ) {
                    if      (eat('+')) x += parseTerm(); // addition
                    else if (eat('-')) x -= parseTerm(); // subtraction
                    else return x;
                }
            }

            double parseTerm() {
                double x = parseFactor();
                for ( ; ; ) {
                    if      (eat('*')) x *= parseFactor(); // multiplication
                    else if (eat('/')) x /= parseFactor(); // division
                    else return x;
                }
            }

            double parseFactor() {
                if (eat('+')) return parseFactor(); // unary plus
                if (eat('-')) return -parseFactor(); // unary minus

                double x;
                int startPos = this.pos;
                if (eat('(')) { // parentheses
                    x = parseExpression();
                    eat(')');
                } 
                else if ((ch >= '0' && ch <= '9') || ch == '.') { // numbers
                    while ((ch >= '0' && ch <= '9') || ch == '.') nextChar();
                    x = Double.parseDouble(str.substring(startPos, this.pos));
                } 
                else if (ch >= 'a' && ch <= 'z') { // functions
                    while (ch >= 'a' && ch <= 'z') nextChar();
                    String func = str.substring(startPos, this.pos);
                    x = parseFactor();
                    if (func.equals("sqrt")) x = Math.sqrt(x);
                    else if (func.equals("sin")) x = Math.sin(Math.toRadians(x));
                    else if (func.equals("cos")) x = Math.cos(Math.toRadians(x));
                    else if (func.equals("tan")) x = Math.tan(Math.toRadians(x));
                    else throw new RuntimeException("Unknown function: " + func);
                } 
                else {
                    throw new RuntimeException("Unexpected: " + (char)ch);
                }

                if (eat('^')) x = Math.pow(x, parseFactor()); // exponentiation

                return x;
            }
        }.parse();
    }
    
    /**
     * Returns the query used to check if the given block needs synchronization operations.
     * @param block     The block
     * @return          The query
     */
    public static String getSynchronizationQuery(int block) {
        return "//mms:interiorExecutionFlow/sml:simml/*[position() = " + (block + 1) 
                + " and local-name()='iterateOverCells' and (@stencilAtt > 0 or "
                + "descendant::sml:interiorDomainContext or descendant::sml:spatialDiscretization)]|//" 
                + "mms:surfaceExecutionFlow/sml:simml/*[position() = " + (block + 1) 
                + " and local-name()='iterateOverCells' and (@stencilAtt > 0 or "
                + "descendant::sml:interiorDomainContext or descendant::sml:spatialDiscretization)]";
    }
    
    /**
     * Returns the query used to check if the given block needs synchronization operations.
     * @param block     The block
     * @return          The query
     */
    public static String getAnalysisSynchronizationQuery(int block) {
        return "/mms:discretizedProblem/mms:analysis/sml:simml/*[position() = " + (block + 1) 
                + " and (@stencilAtt > 0 or descendant::sml:spatialDiscretization)]";
    }
    
    /**
     * Conversion of a xml node variable in a string variable.
     * @param xmlVar            The xml variable
     * @param onlyMeshVars      Only convert mesh variables
     * @param removeTimeSteps   Remove the previous time steps suffixes from the variable name
     * @return                  The variable string
     */
    public static String xmlVarToString(Node xmlVar, boolean onlyMeshVars, boolean removeTimeSteps) {
        Node tmp = xmlVar.cloneNode(true);
        //Remove shared variable tag
        if (tmp.getLocalName().equals("sharedVariable")) {
            tmp = tmp.getFirstChild();
        }
        //Remove coordinates
        if (tmp.getLocalName().equals("apply")) {
            tmp = tmp.getFirstChild();
        }
        else {
            if (onlyMeshVars) {
                return null;
            }
        }
        //Process recursively the variable
        return mathToString((Element) tmp, removeTimeSteps);
    }
    
    /**
     * Checks if a variable is a mesh variable.
     * @param xmlVar        The variable to check
     * @return              True if mesh variable
     */
    public static boolean isMeshVar(Node xmlVar) {
        Node tmp = xmlVar.cloneNode(true);
        //Remove shared variable tag
        if (tmp.getLocalName().equals("sharedVariable")) {
            tmp = tmp.getFirstChild();
        }
        //Remove coordinates
        if (tmp.getLocalName().equals("apply")) {
            return true;
        }
		return false;
    }
    
    /**
     * Converts recursively a node variable into a string variable.
     * @param xml               The node variable element
     * @param removeTimeSteps   Remove the previous time steps suffixes from the variable name
     * @return                  The current variable name
     */
    public static String mathToString(Element xml, boolean removeTimeSteps) {
        //ci element with mixed content
        if (xml.getLocalName().equals("ci") && xml.getChildNodes().getLength() > 1) {
        	NodeList children = xml.getChildNodes();
        	String text = "";
        	for (int i = 0; i < children.getLength(); i++) {
        		Node child = children.item(i);
        		if (child.getNodeType() == Node.TEXT_NODE) {
        			text = text + child.getTextContent().replaceAll("'", "prime").replaceAll("\\*", "ast").replaceAll("\\+", "pos")
                            .replaceAll("-", "neg").replaceAll("#", "sharp").replaceAll("%", "percent").replaceAll("\u0025B", "epsilon")
                            .replaceAll("\u00AC", "negative").replaceAll("\u00B5", "mu").replaceAll("\u00D7", "times")
                            .replaceAll("\u00E6", "ae").replaceAll("\u0391", "Alpha").replaceAll("\u0392", "Beta")
                            .replaceAll("\u0393", "Gamma").replaceAll("\u0394", "delta").replaceAll("\u0395", "Epsilon")
                            .replaceAll("\u0396", "Zeta").replaceAll("\u0397", "Eta").replaceAll("\u0398", "Theta")
                            .replaceAll("\u0399", "Iota").replaceAll("\u039A", "Kappa").replaceAll("\u039B", "Lambda")
                            .replaceAll("\u039C", "Mu").replaceAll("\u039D", "Nu").replaceAll("\u039E", "Xi")
                            .replaceAll("\u039F", "Omicron").replaceAll("\u03A0", "Pi").replaceAll("\u03A1", "Rho")
                            .replaceAll("\u03A3", "Sigma").replaceAll("\u03A4", "Tau").replaceAll("\u03A5", "Upsilon")
                            .replaceAll("\u03A6", "Phi").replaceAll("\u03A7", "Chi").replaceAll("\u03A8", "Psi")
                            .replaceAll("\u03A9", "Omega").replaceAll("\u03B1", "alpha").replaceAll("\u03B2", "beta")
                            .replaceAll("\u03B3", "gamma").replaceAll("\u03B4", "deltaSmall").replaceAll("\u03B5", "epsilon")
                            .replaceAll("\u03B6", "zeta").replaceAll("\u03B7", "eta").replaceAll("\u03B8", "theta")
                            .replaceAll("\u03B9", "iota").replaceAll("\u03BA", "kappa").replaceAll("\u03BB", "lambda")
                            .replaceAll("\u03BC", "mu").replaceAll("\u03BD", "nu").replaceAll("\u03BE", "xi")
                            .replaceAll("\u03BF", "o").replaceAll("\u03C0", "pi").replaceAll("\u03C1", "rho")
                            .replaceAll("\u03C2", "stigma").replaceAll("\u03C3", "sigma").replaceAll("\u03C4", "tau")
                            .replaceAll("\u03C5", "upsilon").replaceAll("\u03C6", "phi").replaceAll("\u03C7", "chi")
                            .replaceAll("\u03C8", "psi").replaceAll("\u03C9", "omega").replaceAll("\u03D1", "theta")
                            .replaceAll("\u03D2", "upsilon").replaceAll("\u03D5", "phi").replaceAll("\u03D6", "pi")
                            .replaceAll("\u03F0", "kappa").replaceAll("\u03F1", "rho").replaceAll("\"", "quot")
                            .replaceAll("\u03B7", "eta").replaceAll("\u03B7", "eta").replaceAll("\u03B7", "eta");
        		}
        		else {
        			text = text + mathToString((Element) child, removeTimeSteps);
        		}
        	}
        	return text;
        }
        if ((xml.getLocalName().equals("ci") || xml.getLocalName().equals("cn")) && xml.getFirstChild().getNodeType() == Node.TEXT_NODE) {
            return xml.getTextContent().replaceAll("'", "prime").replaceAll("\\*", "ast").replaceAll("\\+", "pos")
                    .replaceAll("-", "neg").replaceAll("#", "sharp").replaceAll("%", "percent").replaceAll("\u0025B", "epsilon")
                    .replaceAll("\u00AC", "negative").replaceAll("\u00B5", "mu").replaceAll("\u00D7", "times")
                    .replaceAll("\u00E6", "ae").replaceAll("\u0391", "Alpha").replaceAll("\u0392", "Beta")
                    .replaceAll("\u0393", "Gamma").replaceAll("\u0394", "delta").replaceAll("\u0395", "Epsilon")
                    .replaceAll("\u0396", "Zeta").replaceAll("\u0397", "Eta").replaceAll("\u0398", "Theta")
                    .replaceAll("\u0399", "Iota").replaceAll("\u039A", "Kappa").replaceAll("\u039B", "Lambda")
                    .replaceAll("\u039C", "Mu").replaceAll("\u039D", "Nu").replaceAll("\u039E", "Xi")
                    .replaceAll("\u039F", "Omicron").replaceAll("\u03A0", "Pi").replaceAll("\u03A1", "Rho")
                    .replaceAll("\u03A3", "Sigma").replaceAll("\u03A4", "Tau").replaceAll("\u03A5", "Upsilon")
                    .replaceAll("\u03A6", "Phi").replaceAll("\u03A7", "Chi").replaceAll("\u03A8", "Psi")
                    .replaceAll("\u03A9", "Omega").replaceAll("\u03B1", "alpha").replaceAll("\u03B2", "beta")
                    .replaceAll("\u03B3", "gamma").replaceAll("\u03B4", "deltaSmall").replaceAll("\u03B5", "epsilon")
                    .replaceAll("\u03B6", "zeta").replaceAll("\u03B7", "eta").replaceAll("\u03B8", "theta")
                    .replaceAll("\u03B9", "iota").replaceAll("\u03BA", "kappa").replaceAll("\u03BB", "lambda")
                    .replaceAll("\u03BC", "mu").replaceAll("\u03BD", "nu").replaceAll("\u03BE", "xi")
                    .replaceAll("\u03BF", "o").replaceAll("\u03C0", "pi").replaceAll("\u03C1", "rho")
                    .replaceAll("\u03C2", "stigma").replaceAll("\u03C3", "sigma").replaceAll("\u03C4", "tau")
                    .replaceAll("\u03C5", "upsilon").replaceAll("\u03C6", "phi").replaceAll("\u03C7", "chi")
                    .replaceAll("\u03C8", "psi").replaceAll("\u03C9", "omega").replaceAll("\u03D1", "theta")
                    .replaceAll("\u03D2", "upsilon").replaceAll("\u03D5", "phi").replaceAll("\u03D6", "pi")
                    .replaceAll("\u03F0", "kappa").replaceAll("\u03F1", "rho").replaceAll("\"", "quot")
                    .replaceAll("\u03B7", "eta").replaceAll("\u03B7", "eta").replaceAll("\u03B7", "eta");
        }
        //particleDistance
        if (xml.getLocalName().equals("particleDistance")) {
            return "particle_distance";
        }
        //particlePosition
        if (xml.getLocalName().equals("particlePosition")) {
            return "position" + xml.getTextContent();
        }
        //msubsup
        if (xml.getLocalName().equals("msubsup")) {
            NodeList subtags = xml.getChildNodes();
            return mathToString((Element) subtags.item(0), removeTimeSteps) + "SB" + mathToString((Element) subtags.item(1), removeTimeSteps) 
                + "SP" + mathToString((Element) subtags.item(2), removeTimeSteps);
        }
        //msub
        if (xml.getLocalName().equals("msub")) {
            NodeList subtags = xml.getChildNodes();
            return mathToString((Element) subtags.item(0), removeTimeSteps) + "SB" + mathToString((Element) subtags.item(1), removeTimeSteps);
        }
        //msup (special attention to time steps)
        if (xml.getLocalName().equals("msup")) {
            NodeList subtags = xml.getChildNodes();
            String value = mathToString((Element) subtags.item(0), removeTimeSteps);
            if (subtags.item(1).getFirstChild().getNodeType() == Node.ELEMENT_NODE) {
                if (subtags.item(1).getChildNodes().item(1).getTextContent().startsWith("(")) {
                    if (!removeTimeSteps) {
                        if (subtags.item(1).getChildNodes().item(0).getLocalName().equals("minus")) {
                           int steps = Integer.parseInt(subtags.item(1).getChildNodes().item(2).getTextContent()) + 1;
                           for (int i = 0; i < steps; i++) {
                               value = value + "_p";
                           }
                        }
                    }
                }
                else {
                    value = value + "SP" +  mathToString((Element) subtags.item(1), removeTimeSteps);
                }
            }
            else {
                if (subtags.item(1).getTextContent().startsWith("(")) {
                    if (!removeTimeSteps) {
                        value = value + "_p";
                    }
                }
                else {
                    value = value + "SP" +  mathToString((Element) subtags.item(1), removeTimeSteps);
                }
            }
            return value;
        }
        //Others
        String value = "";
        NodeList subtags = xml.getChildNodes();
        for (int i = 0; i < subtags.getLength(); i++) {
            if (subtags.item(i).getNodeType() == Node.ELEMENT_NODE) {
                value = value + mathToString((Element) subtags.item(i), removeTimeSteps);
            }
        }
        return value;
    }
    
    /**
     * Gets the information for external file reading (from table).
     * It process the function to add extra information required.
     * It adds parameters to the functions calling sml:readFromFile and sml:findCoordFromFile
     * @param doc               The problem
     * @return                  The files to read
     * @throws CGException      CG00X External error
     */
    static public FileReadInformation processFilesToRead(Document doc) throws CGException {
        NodeList operations = find(doc, "//sml:readFromFileQuintic|//sml:readFromFileLinear1D|//sml:readFromFileLinear2D|//sml:readFromFileLinear3D|"
        		+ "//sml:readFromFileLinearWithDerivatives1D|//sml:readFromFileLinearWithDerivatives2D|//sml:readFromFileLinearWithDerivatives3D|"
        		+ "//sml:findCoordFromFile1D|//sml:findCoordFromFile2D|//sml:findCoordFromFile3D");
        FileReadInformation fileReadInfo = new FileReadInformation();
        HashMap<String, ArrayList<String>> modifiedFunctions = new HashMap<String, ArrayList<String>>();
        HashMap<String, ArrayList<Integer>> modifiedArguments = new HashMap<String, ArrayList<Integer>>(); 
        for (int i = operations.getLength() - 1; i >= 0; i--) {
        	//Get file read information data
            String fileName = operations.item(i).getFirstChild().getTextContent();
            int numberCoordinates = Integer.parseInt(operations.item(i).getFirstChild().getNextSibling().getTextContent());
            int numberReads = Integer.parseInt(operations.item(i).getFirstChild().getNextSibling().getNextSibling().getTextContent());
            int coordRead = 0;
            if (operations.item(i).getLocalName().startsWith("findCoordFromFile")) {
            	coordRead = Integer.parseInt(operations.item(i).getFirstChild().getNextSibling().getNextSibling().getNextSibling().getTextContent());
            	operations.item(i).removeChild(operations.item(i).getFirstChild().getNextSibling().getNextSibling().getNextSibling());
            	//Always set to 1 data when using findCoordFromFile
            	operations.item(i).getFirstChild().getNextSibling().getNextSibling().setTextContent("1");
            	numberReads = 1;
            	
            }
            fileReadInfo.addInstruction(operations.item(i).getLocalName(), fileName, numberCoordinates, numberReads, coordRead);
            
        	//Remove table name
        	operations.item(i).removeChild(operations.item(i).getFirstChild());
            //Add extra information parameters
        	ArrayList<String> newParameters = new ArrayList<String>();
            //Adding coordinate variables
            for(int j = 0; j < numberCoordinates; j++) {
            	Element data = CodeGeneratorUtils.createElement(doc, MTURI, "ci");
            	data.setTextContent("coord" + j + "_data_" + fileName);
            	Element dx = CodeGeneratorUtils.createElement(doc, MTURI, "ci");
            	dx.setTextContent("coord" + j + "_dx_" + fileName);
            	Element size = CodeGeneratorUtils.createElement(doc, MTURI, "ci");
            	size.setTextContent("coord" + j + "_size_" + fileName);
            	operations.item(i).appendChild(data);
            	operations.item(i).appendChild(dx);
            	operations.item(i).appendChild(size);
            	newParameters.add("field coord" + j + "_data_" + fileName);
            	newParameters.add("real coord" + j + "_dx_" + fileName);
            	newParameters.add("int coord" + j + "_size_" + fileName);
            }
            //Adding data variable
          	Element data = CodeGeneratorUtils.createElement(doc, MTURI, "ci");
          	data.setTextContent("vars_data_" + fileName);
        	operations.item(i).appendChild(data);
        	newParameters.add("field vars_data_" + fileName);
           	Element dataSize = CodeGeneratorUtils.createElement(doc, MTURI, "ci");
           	dataSize.setTextContent("vars_size_" + fileName);
        	operations.item(i).appendChild(dataSize);
        	newParameters.add("int vars_size_" + fileName);
        	//Add suffix
        	if (operations.item(i).getLocalName().startsWith("findCoordFromFile")) {
        	 	Element findCoordFromFile = CodeGeneratorUtils.createElement(doc, SMLURI, operations.item(i).getLocalName() + "_" + coordRead);
        	 	NodeList children = operations.item(i).getChildNodes();
        	 	for (int j = 0; j < children.getLength(); j++) {
        	 		findCoordFromFile.appendChild(children.item(j).cloneNode(true));
        	 	}  
        	 	operations.item(i).getParentNode().replaceChild(findCoordFromFile, operations.item(i));
        	}
        	//Add parameters to parent functions (avoiding parameter repetition)
        	Node function = operations.item(i).getParentNode();
        	while(function != null && !function.getLocalName().equals("discretizedProblem") && !function.getLocalName().equals("function")) {
        		function = function.getParentNode();
        	}
        	if (function != null && function.getLocalName().equals("function")) {
	        	String functionName = function.getFirstChild().getTextContent();
	        	if (!modifiedFunctions.containsKey(functionName)) {
	        		modifiedFunctions.put(functionName, new ArrayList<String>());
	        	}
	        	if (!modifiedFunctions.get(functionName).contains(fileName)) {
	        		ArrayList<String> addedData = modifiedFunctions.get(functionName);
	        		addedData.add(fileName);
	        		modifiedFunctions.put(functionName, addedData);
		        	addFilesToReadParameters(function, newParameters);
		        	ArrayList<Node> newFunctions = getFunctionsCalling(doc, functionName);
		        	for (Node f : newFunctions) {
		        		addFilesToReadProcess(f, modifiedFunctions, fileName, newParameters);
		        	}
	        	}	
	        	//If any function where new parameters have been added is passed as argument of another function, 
	            //the argument parameters and function calls must be modified too
	            Iterator<String> funcsIt =  modifiedFunctions.keySet().iterator();
	        	while (funcsIt.hasNext()) {
	        		functionName = funcsIt.next();
	        		NodeList calls = find(doc, "//sml:functionCall[*[position() > 1][text() = '" + functionName + "']]");
	        		for (int j = 0; j < calls.getLength(); j++) {
	        			String parentFunction = calls.item(j).getFirstChild().getTextContent();
	        			if (!modifiedArguments.containsKey(parentFunction)) {
	        				modifiedArguments.put(parentFunction, new ArrayList<Integer>());
	        			}
	        			//Get argument position
	        			NodeList parameters = calls.item(j).getChildNodes();
	            		int argPosition = 0;
	            	  	while (!parameters.item(argPosition).getTextContent().equals(functionName)) {
	            	  		argPosition++;
	            	  	}
	            	  	//First argument is parent function name
	            	  	argPosition--;
	            	  	//Check that is not already added
	            	  	ArrayList<Integer> argNumber = modifiedArguments.get(parentFunction);
	            		if (!argNumber.contains(argPosition)) {
	            			argNumber.add(argPosition);
	            			modifiedArguments.put(parentFunction, argNumber);
	            			//Add table access arguments
	            			Element functionParameter = (Element) find(doc, "/*/mms:functions/mms:function[mms:functionName = '" + parentFunction + "']//mms:functionParameter[position() = " + (argPosition + 1) + "]").item(0);
	            		  	if (!functionParameter.getFirstChild().getNextSibling().getTextContent().equals("function")) {
	                	  		throw new CGException(CGException.CG003, "Cannot find correct function parameter. Contact with a developer.");
	                	  	}
	            		  	for (String param : newParameters) {
		                		Element functionParameterType = createElement(doc, MMSURI, "functionParameterType");
		                		String paramType = param.substring(0, param.indexOf(" "));
		                  		functionParameterType.setTextContent(paramType);
		                  		functionParameter.getLastChild().appendChild(functionParameterType);
		                	}
	            		  	String functionParamName = functionParameter.getFirstChild().getTextContent();
	            		  	NodeList paramCalls = find(doc, "/*/mms:functions/mms:function[mms:functionName = '" + parentFunction + "']//sml:functionCall[*[position() = 1][text() = '" + functionParamName + "']]");
	            		  	for (int k = 0; k < paramCalls.getLength(); k++) {
	            		  		for (String param : newParameters) {
	            		    		Element ci = createElement(doc, MTURI, "ci");
	            		    		String paramName = param.substring(param.indexOf(" ") + 1);
	            		    		ci.setTextContent(paramName);
	            		    		paramCalls.item(k).appendChild(ci);
	            		    	}
	            		  	}
	            		}
	        		}
	        	}
        	}
        }
        return fileReadInfo;
    }
    
    private static void addFilesToReadProcess(Node function, HashMap<String, ArrayList<String>> modifiedFunctions, String fileName, ArrayList<String> newParameters) throws CGException {
      	String functionName = function.getFirstChild().getTextContent();
    	if (!modifiedFunctions.containsKey(functionName)) {
    		modifiedFunctions.put(functionName, new ArrayList<String>());
    	}
     	if (!modifiedFunctions.get(functionName).contains(fileName)) {
     		ArrayList<String> addedData = modifiedFunctions.get(functionName);
    		addedData.add(fileName);
    		modifiedFunctions.put(functionName, addedData);
    		addFilesToReadParameters(function, newParameters);
    		ArrayList<Node> functionsCalling = getFunctionsCalling(function.getOwnerDocument(), functionName);
    		for (int i = 0; i < functionsCalling.size(); i++) {
    			addFilesToReadProcess(functionsCalling.get(i), modifiedFunctions, fileName, newParameters);
    		}
     	}
    }
    
    /**
     * Adds parameters to function and its calls. 
     * @param function			Function
     * @param newParameters		Parameters to add
     * @throws CGException		CG00X External error
     */
    private static void addFilesToReadParameters(Node function, ArrayList<String> newParameters) throws CGException {
    	Document doc = function.getOwnerDocument();
      	String functionName = function.getFirstChild().getTextContent();
      	DocumentFragment callParams = doc.createDocumentFragment();
    	//Adding parameters to function definition
    	Node functionParameters = ((Element)function).getElementsByTagNameNS(MMSURI, "functionParameters").item(0);
    	for (String param : newParameters) {
    		Element ci = createElement(doc, MTURI, "ci");
    		Element parameter = createElement(doc, MMSURI, "functionParameter");
    		Element name = createElement(doc, MMSURI, "name");
    		Element type = createElement(doc, MMSURI, "type");
    		String paramName = param.substring(param.indexOf(" ") + 1);
    		String paramType = param.substring(0, param.indexOf(" "));
			name.setTextContent(paramName);
			type.setTextContent(paramType);
    		parameter.appendChild(name);
    		parameter.appendChild(type);
    		functionParameters.appendChild(parameter);
    		ci.setTextContent(name.getTextContent());
    		callParams.appendChild(ci);
    	}
    	//Adding parameters to function calls
    	NodeList calls = find(doc, "//sml:functionCall[*[position() = 1][text() = '" + functionName + "']]");
    	for (int i = 0; i < calls.getLength(); i++) {
    		calls.item(i).appendChild(callParams.cloneNode(true));
    	}
    }
    
    /**
     * Gets all functions calling given function name
     * @param doc				Document to search into
     * @param functionName		Function name
     * @return					List of callers
     * @throws CGException		CG00X External error
     */
    private static ArrayList<Node> getFunctionsCalling(Document doc, String functionName) throws CGException {
    	ArrayList<Node> results = new ArrayList<Node>();
    	Set<String> functions = new HashSet<String>();
    	//Direct calls
    	NodeList calls = find(doc, "/*/mms:functions/mms:function//sml:functionCall[mt:ci[text() = '" + functionName + "']]");
    	for (int i = 0; i < calls.getLength(); i++) {
    		functions.add(calls.item(i).getFirstChild().getTextContent());
    	
    	}
    	for (String f : functions) {
    		results.add(find(doc, "/*/mms:functions/mms:function[mms:functionName = '" + f + "']").item(0));
    	}
    	return results;
    }
    
    /**
     * Creates groups from extrapolation fields that can be extrapolated in the same points.
     * @param hardRegions	The incompatible regions and their fields
     * @param problemFields	The fields
     * @return				The group information
     */
    public static LinkedHashMap<String, ArrayList<String>> getExtrapolatedFieldGroups(HashMap<String, ArrayList<String>> hardRegions, 
    		Set<String> problemFields) {
    	LinkedHashMap<String, ArrayList<String>> extrapolatedFieldGroups = new LinkedHashMap<String, ArrayList<String>>();
    	LinkedHashSet<String> extrapolatedFields = new LinkedHashSet<String>();
    	//Get all extrapolated fields for all the regions
    	Iterator<String> regions = hardRegions.keySet().iterator();
    	while (regions.hasNext()) {
    		String region = regions.next();
    		extrapolatedFields.addAll(hardRegions.get(region));
    	}
    	ArrayList<String> extrapolatedFieldsList = new ArrayList<String>();
    	extrapolatedFieldsList.addAll(extrapolatedFields);
    	//Iterate until there is no change
    	boolean change = true;
    	while (change) {
    		change = false;
    		//Try pairs of fields or field & group
    		for (int i = 0; i < extrapolatedFieldsList.size() - 1 && !change; i++) {
    			String fieldA = extrapolatedFieldsList.get(i);
        		for (int j = i + 1; j < extrapolatedFieldsList.size() && !change; j++) {
        			String fieldB = extrapolatedFieldsList.get(j);
        			//Check if the pair is indivisible in every region
        			regions = hardRegions.keySet().iterator();
        			boolean indivisible = true;
        	    	while (indivisible && regions.hasNext()) {
        	    		String region = regions.next();
        				boolean containsA = hardRegions.get(region).contains(fieldA) || 
        						(extrapolatedFieldGroups.containsKey(fieldA) && 
        								hardRegions.get(region).containsAll(extrapolatedFieldGroups.get(fieldA)));
        				boolean containsB = hardRegions.get(region).contains(fieldB) || 
        						(extrapolatedFieldGroups.containsKey(fieldB) && 
        								hardRegions.get(region).containsAll(extrapolatedFieldGroups.get(fieldB)));
        	    		if ((containsA && !containsB) || 
        	    				(containsB && !containsA)) {
        	    			indivisible = false;
        	    		}
        	    	}
        	    	//If is indivisible, add the new group and modify the list of fields
        	    	if (indivisible) {
        	    		extrapolatedFieldsList.remove(fieldB);
        	    		extrapolatedFieldsList.remove(fieldA);
        	    		extrapolatedFieldsList.add(fieldA + "_" + fieldB);
        	    		ArrayList<String> fields = new ArrayList<String>();
        	    		if (extrapolatedFieldGroups.containsKey(fieldA)) {
        	    			fields.addAll(extrapolatedFieldGroups.get(fieldA));
        	    			extrapolatedFieldGroups.remove(fieldA);
        	    		}
        	    		else {
        	    			fields.add(fieldA);
        	    		}
        	    		if (extrapolatedFieldGroups.containsKey(fieldB)) {
        	    			fields.addAll(extrapolatedFieldGroups.get(fieldB));
        	    			extrapolatedFieldGroups.remove(fieldB);
        	    		}
        	    		else {
        	    			fields.add(fieldB);
        	    		}
        	    		extrapolatedFieldGroups.put(fieldA + "_" + fieldB, fields);
        	    		change = true;
        	    	}
        		}
    		}
    	}
    	//Add all remaining fields
		for (int i = 0; i < extrapolatedFieldsList.size(); i++) {
			String field = extrapolatedFieldsList.get(i);
			if (!extrapolatedFieldGroups.containsKey(field)) {
	    		ArrayList<String> fields = new ArrayList<String>();
	    		fields.add(field);
	    		extrapolatedFieldGroups.put(field, fields);
			}
		}
		//Order fields inside groups following problem order
		Iterator<String> extrapolatedFieldGroupsIterator = extrapolatedFieldGroups.keySet().iterator();
    	while (extrapolatedFieldGroupsIterator.hasNext()) {
    		String fieldGroup = extrapolatedFieldGroupsIterator.next();
    		ArrayList<String> unorderedGroupFields = extrapolatedFieldGroups.get(fieldGroup);
    		ArrayList<String> orderedGroupFields = new ArrayList<String>();
    		Iterator<String> originalFields = problemFields.iterator();
    		while (originalFields.hasNext()) {
    			String field = originalFields.next();
    			if (unorderedGroupFields.contains(field)) {
    				orderedGroupFields.add(field);
    			}
    		}
    		extrapolatedFieldGroups.put(fieldGroup, orderedGroupFields);
    	}
    	return extrapolatedFieldGroups;
    }
    
    /**
     * Creates all the combinations signs.
     * @param coordinates       The coordinates
     * @return                  The interaction list
     */
    public static ArrayList<String> createCombinations(ArrayList<String> coordinates) {
        ArrayList<String> combinations = new ArrayList<String>();
        
        combinations.add("+");
        combinations.add("-");
        
        for (int j = 0; j < coordinates.size() - 1; j++) {
            ArrayList<String> combinationsTMP = new ArrayList<String>();
            combinationsTMP.addAll(combinations);
            combinations.clear();
            for (int i = 0; i < combinationsTMP.size(); i++) {
                String newComb = combinationsTMP.get(i);
                combinations.add(newComb + "+");
                combinations.add(newComb + "-");
            }            
        }
        
        return combinations;
    }
    
    /**
     * Check if there is external initial conditions.
     * @param doc			The problem
     * @return				If there is external initial conditions
     * @throws CGException	CG00X External error
     */
    public static boolean getHasExternalInitialCondition(Document doc) throws CGException {
    	return find(doc, "//mms:externalInitialCondition").getLength() > 0;
    }
    
    /**
     * Checks if a variable is assigned inside the code.
     * @param variable		The variable to check
     * @param code			The code
     * @return				True if assigned
     * @throws CGException	CG00X External error
     */
    public static boolean isAssigned(String variable, Node code) throws CGException {
    	return find(code, ".//mt:math[(not(parent::sml:if) or parent::sml:then) and (not(parent::sml:while) or parent::sml:loop) and not(parent::sml:return) " 
                + "and not(ancestor::mms:applyIf) and not(ancestor::mms:finalizationCondition)]/mt:apply/mt:eq/following-sibling::*[1][self::mt:ci[text() = '" + variable + "']]").getLength() > 0;
    }
    
    /**
     * Checks whether the auxiliary Equations Algorithm has derivatives. (It happens when the last loop has children)
     * @param doc			The problem
     * @return				True if any auxiliary field equation has derivatives.
     * @throws CGException	CG00X External error
     */
    public static boolean auxiliaryFieldsHasDerivatives(Document doc) throws CGException {
    	NodeList auxFieldAlgorithms = find(doc, "//mms:auxiliaryEquationsAlgorithm//sml:simml");
    	for (int i = 0; i < auxFieldAlgorithms.getLength(); i++) {
    		if (auxFieldAlgorithms.item(i).getLastChild().hasChildNodes()) {
    			return true;
    		}
    	}
    	return false;
    }
    
    /**
     * Checks if there is a reflection condition for particles.
     * @param pi			The problem info
     * @param axis			The axis
     * @param side			The side
     * @return				True if boundary exists
     * @throws CGException	CG00X External error
     */
    public static boolean isParticleReflectionCondition(ProblemInfo pi, String axis, String side) throws CGException {
    	NodeList reflectedFields = CodeGeneratorUtils.find(pi.getProblem(), "/*/mms:boundaryConditions/mms:boundaryPolicy/mms:boundaryCondition[mms:type/mms:reflection and (mms:axis = '" + axis + "' or mms:axis = 'All') and (mms:side = '" + side + "' or mms:side = 'All')]/mms:fields/*");
    	for (int i = 0; i < reflectedFields.getLength(); i++) {
    		if (!pi.getVariableInfo().getMeshVariables().contains(reflectedFields.item(i).getTextContent())) {
    			return true;
    		}
    	}
    	return false;
    }
    
    /**
     * Gets the compact support ratio for particles.
     * @param problem		The problem
     * @return				The compact support ratio
     * @throws CGException 	CG00X External error
     */
    public static int getCompactSupportRatio(Document problem) throws CGException {
    	int compactSupportRatio = 1;
        NodeList csr = find(problem, "//@compactSupportRatioAtt");
    	for (int i = 0; i < csr.getLength(); i++) {
    		int localCSR = Integer.parseInt(csr.item(i).getTextContent());
	        if (localCSR > compactSupportRatio) {
	        	compactSupportRatio = localCSR;
	        }
    	}
        return compactSupportRatio;
    }
    
    /**
     * Loads in the code database the given document and its dependencies.
     * @param codeId			Code id
     * @param cdb				Codes database
     * @param id				Document Id
     * @param alreadyLoaded		To avoid redundancies
     * @throws DMException
     * @throws CGException
     * @throws SUException
     * @throws DCException
     */
    public static void loadDocumentAndDependecies(String codeId, CodesDB cdb, String id, HashSet<String> alreadyLoaded) throws DMException, CGException, SUException, DCException {
    	System.out.println("Voy a añadir " + id);
		String schema = SimflownyUtils.jsonToXML(new DocumentManagerImpl().getDocument(id));
		Document document = stringToDom(schema);
		cdb.addCodeFile(codeId, "documentation", document.getDocumentElement().getLocalName() + "-" + id + ".xml", schema);
		alreadyLoaded.add(id);
		NodeList ids = find(document, "//*[ matches(., '^([0-9a-fA-F]){8}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){12}$')]");
		for (int i = 0; i < ids.getLength(); i++) {
			if (!alreadyLoaded.contains(ids.item(i).getTextContent())) {
				loadDocumentAndDependecies(codeId, cdb, ids.item(i).getTextContent(), alreadyLoaded);
			}
		}
    }
    
    /**
     * Check if the given element has the ancestor tag.
     * @param element		The element to check
     * @param ancestor		Ancestor tag to check
     * @return				True if having the ancestor
     */
    public static boolean hasAncestor(Node element, String ancestor) {
    	Node parent = element.getParentNode();
    	while (parent != null && !parent.getLocalName().equals("simml")) {
    		if (parent.getLocalName().equals(ancestor)) {
    			return true;
    		}
    		parent = parent.getParentNode();
    	}
    	return false;
    }
}
