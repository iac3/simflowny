/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.codeGeneratorPlugin.utils;

import eu.iac3.mathMS.codeGeneratorPlugin.SAMRAI.X3DMovInfo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;

import org.w3c.dom.Document;

/**
 * Common information of the problem.
 * @author bminyano
 *
 */
public class ProblemInfo {
    private Document problem;
    private LinkedHashMap<String, Integer> fieldTimeLevels;
    private LinkedHashMap<String, Integer> positionTimeLevels;
    private ArrayList<String> coordinates;
    private ArrayList<String> contCoordinates;
    private String timeCoord;
    private HashSet<String> identifiers;
    private LinkedHashMap<String, String> periodicalBoundary;
    private ArrayList<String> variables;
    private ArrayList<String> extrapolVars = new ArrayList<String>();
    private ArrayList<String> extrapolFuncVars = new ArrayList<String>();
    private ArrayList<String> regions;
    private ArrayList<String> regionIds;
    private ArrayList<String> regionPrecedence;
    private ArrayList<String> boundariesPrecedence;
    private LinkedHashMap<String, LinkedHashMap<String, String>> regionRelations;
    private LinkedHashMap<String, CoordinateInfo> gridLimits;
    private String regionName;
    private int schemaStencil;
    private int extrapolationStencil;
    private int stepStencil;
    private CactusVarGroupInfo groupInfo;
    private String scopeField;
    private HashMap<String, ArrayList<String>> hardRegions = new HashMap<String, ArrayList<String>>();
    private MovementInfo movementInfo;
    private boolean hasParticleFields = false;
    private boolean hasMeshFields = false;
    private boolean hasABMMeshFields = false;
    private boolean hasABMMeshlessFields = false;
    private boolean hasGraphFields = false;
    private String evolutionStep;
    private String degreeDistribution;
    private String edgeDirectionality;
    private ArrayList<String> edgeFields = new ArrayList<String>();
    //private ArrayList<String> varFields = new ArrayList<String>(); //TODO movement only
    private ArrayList<String> analysisFields = new ArrayList<String>();
    private ArrayList<String> auxiliaryFields = new ArrayList<String>();
    private ArrayList<String> postRefinementAuxiliaryFields = new ArrayList<String>();
    private LinkedHashMap<String, ArrayList<String>> fieldVariables = new LinkedHashMap<String, ArrayList<String>>();
    private int currentRegionId;
    private FileReadInformation fileReadInfo;
    private ExtrapolationType extrapolationType;
    private LinkedHashMap<String, ArrayList<String>> extrapolatedFieldGroups;
    private TimeInterpolationInfo timeInterpolationInfo;    
    private X3DMovInfo x3dMov;
    private String problemName;
    private VariableInfo variableInfo;
    private boolean hasExternalInitialCondition;
    private int compactSupportRatio = 1;
    private ArrayList<String> particleSpecies;
    private ArrayList<String> functionNames;
    private HashMap<String, String> constants;
    
    /**
     * Gets the fields that are not available from a region in hard region boundaries.
     * @param rn            The region name
     * @return              The fields
     */
    
    public ArrayList<String> getHardRegionFields(String rn) {
        return hardRegions.get(rn);
    }
	public ArrayList<String> getParticleSpecies() {
		return particleSpecies;
	}
	public void setParticleSpecies(ArrayList<String> particleSpecies) {
		this.particleSpecies = particleSpecies;
	}
	public LinkedHashMap<String, Integer> getPositionTimeLevels() {
		return positionTimeLevels;
	}
	public void setPositionTimeLevels(LinkedHashMap<String, Integer> positionTimeLevels) {
		this.positionTimeLevels = positionTimeLevels;
	}
	public boolean isHasParticleFields() {
		return hasParticleFields;
	}
	public void setHasParticleFields(boolean hasParticleFields) {
		this.hasParticleFields = hasParticleFields;
	}
	public boolean isHasMeshFields() {
		return hasMeshFields;
	}
	public void setHasMeshFields(boolean hasMeshFields) {
		this.hasMeshFields = hasMeshFields;
	}
	public boolean isHasABMMeshFields() {
		return hasABMMeshFields;
	}
	public void setHasABMMeshFields(boolean hasABMMeshFields) {
		this.hasABMMeshFields = hasABMMeshFields;
	}
	public boolean isHasABMMeshlessFields() {
		return hasABMMeshlessFields;
	}
	public void setHasABMMeshlessFields(boolean hasABMMeshlessFields) {
		this.hasABMMeshlessFields = hasABMMeshlessFields;
	}
	public boolean isHasGraphFields() {
		return hasGraphFields;
	}
	public void setHasGraphFields(boolean hasGraphFields) {
		this.hasGraphFields = hasGraphFields;
	}
	public VariableInfo getVariableInfo() {
		return variableInfo;
	}
	public void setVariableInfo(VariableInfo variableInfo) {
		this.variableInfo = variableInfo;
	}
	public LinkedHashMap<String, ArrayList<String>> getFieldVariables() {
		return fieldVariables;
	}
	public void setFieldVariables(LinkedHashMap<String, ArrayList<String>> fieldVariables) {
		this.fieldVariables = fieldVariables;
	}
	public String getProblemName() {
		return problemName;
	}
	public void setProblemName(String problemName) {
		this.problemName = problemName;
	}

	public ArrayList<String> getFunctionNames() {
		return functionNames;
	}
	public void setFunctionNames(ArrayList<String> functionNames) {
		this.functionNames = functionNames;
	}
	public int getCompactSupportRatio() {
		return compactSupportRatio;
	}
	public void setCompactSupportRatio(int compactSupportRatio) {
		this.compactSupportRatio = compactSupportRatio;
	}
	public X3DMovInfo getX3dMovRegions() {
        return x3dMov;
    }
    public void setX3dMovRegions(X3DMovInfo x3dMovRegions) {
        this.x3dMov = new X3DMovInfo(x3dMovRegions);
    }
    public ArrayList<String> getContCoordinates() {
        return contCoordinates;
    }
    public void setContCoordinates(ArrayList<String> contCoordinates) {
        this.contCoordinates = contCoordinates;
    }
    public HashMap<String, ArrayList<String>> getHardRegions() {
        return hardRegions;
    }
    public void setHardRegions(HashMap<String, ArrayList<String>> hardRegions) {
        this.hardRegions = hardRegions;
    }
    public String getScopeField() {
        return scopeField;
    }
    public void setScopeField(String scopeField) {
        this.scopeField = scopeField;
    }
    public ArrayList<String> getBoundariesPrecedence() {
        return boundariesPrecedence;
    }
    public void setBoundariesPrecedence(ArrayList<String> boundariesPrecedence) {
        this.boundariesPrecedence = boundariesPrecedence;
    }
    public ArrayList<String> getExtrapolFuncVars() {
        return extrapolFuncVars;
    }
    public void setExtrapolFuncVars(ArrayList<String> extrapolFuncVars) {
        this.extrapolFuncVars = extrapolFuncVars;
    }
    public String getTimeCoord() {
        return timeCoord;
    }
    public void setTimeCoord(String timeCoord) {
        this.timeCoord = timeCoord;
    }
    public Document getProblem() {
        return problem;
    }
    public void setProblem(Document doc) {
        this.problem = doc;
    }
    public int getSchemaStencil() {
        return schemaStencil;
    }
    public void setSchemaStencil(int stencil) {
        this.schemaStencil = stencil;
    }
    public int getExtrapolationStencil() {
        return extrapolationStencil;
    }
    public void setExtrapolationStencil(int stencil) {
        this.extrapolationStencil = stencil;
    }
    public String getRegionName() {
        return regionName;
    }
    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }
    public LinkedHashMap<String, Integer> getFieldTimeLevels() {
        return fieldTimeLevels;
    }
    public ArrayList<String> getCoordinates() {
        return coordinates;
    }
    public HashSet<String> getIdentifiers() {
        return identifiers;
    }
    public LinkedHashMap<String, String> getPeriodicalBoundary() {
        return periodicalBoundary;
    }
    public ArrayList<String> getVariables() {
        return variables;
    }
    public ArrayList<String> getRegions() {
        return regions;
    }
    public LinkedHashMap<String, LinkedHashMap<String, String>> getRegionRelations() {
        return regionRelations;
    }
    public LinkedHashMap<String, CoordinateInfo> getGridLimits() {
        return gridLimits;
    }
    public void setFieldTimeLevels(LinkedHashMap<String, Integer> fieldTimeLevels) {
        this.fieldTimeLevels = fieldTimeLevels;
    }
    public void setCoordinates(ArrayList<String> coordinates) {
        this.coordinates = coordinates;
    }
    public void setIdentifiers(HashSet<String> identifiers) {
        this.identifiers = identifiers;
    }
    public void setPeriodicalBoundary(LinkedHashMap<String, String> periodicalBoundary) {
        this.periodicalBoundary = periodicalBoundary;
    }
    public void setVariables(ArrayList<String> variables) {
        this.variables = variables;
    }
    public ArrayList<String> getExtrapolVars() {
        return extrapolVars;
    }
    public void setExtrapolVars(ArrayList<String> extrapolVars) {
        this.extrapolVars = extrapolVars;
    }
    public void setRegions(ArrayList<String> regions) {
        this.regions = regions;
    }
    public void setRegionRelations(
            LinkedHashMap<String, LinkedHashMap<String, String>> regionRelations) {
        this.regionRelations = regionRelations;
    }
    public void setGridLimits(LinkedHashMap<String, CoordinateInfo> gridLimits) {
        this.gridLimits = gridLimits;
    }
    public CactusVarGroupInfo getGroupInfo() {
        return groupInfo;
    }
    public void setGroupInfo(CactusVarGroupInfo groupInfo) {
        this.groupInfo = groupInfo;
    }
    public int getSpatialDimensions() {
        return coordinates.size();
    }
    public ArrayList<String> getRegionPrecedence() {
        return regionPrecedence;
    }
    public void setRegionPrecedence(ArrayList<String> regionPrecedence) {
        this.regionPrecedence = regionPrecedence;
    }
    public ArrayList<String> getRegionIds() {
        return regionIds;
    }
    public void setRegionIds(ArrayList<String> regionIds) {
        this.regionIds = regionIds;
    }
    public MovementInfo getMovementInfo() {
        return movementInfo;
    }
    public void setMovementInfo(MovementInfo mi) {
        this.movementInfo = mi;
    }
    public ArrayList<String> getPostRefinementAuxiliaryFields() {
		return postRefinementAuxiliaryFields;
	}
	public void setPostRefinementAuxiliaryFields(ArrayList<String> postRefinementAuxiliaryFields) {
		this.postRefinementAuxiliaryFields = postRefinementAuxiliaryFields;
	}
	/**
	 * Check if there is external initial condition.
	 * @return 	true if external initial condition
	 */
	public boolean hasExternalInitialCondition() {
		return hasExternalInitialCondition;
	}
	public void setHasExternalInitialCondition(boolean hasExternalInitialCondition) {
		this.hasExternalInitialCondition = hasExternalInitialCondition;
	}
	public HashMap<String, String> getConstants() {
		return constants;
	}
	public void setConstants(HashMap<String, String> constants) {
		this.constants = constants;
	}
	/*public ArrayList<String> getVarFields() {
        return varFields;
    }
    public void setVarFields(ArrayList<String> varFields) {
        this.varFields = varFields;
    }*/
    public String getEvolutionStep() {
		return evolutionStep;
	}
	public void setEvolutionStep(String evolutionStep) {
		this.evolutionStep = evolutionStep;
	}
	public String getDegreeDistribution() {
		return degreeDistribution;
	}
	public void setDegreeDistribution(String degreeDistribution) {
		this.degreeDistribution = degreeDistribution;
	}
	public ArrayList<String> getEdgeFields() {
		return edgeFields;
	}
   public ArrayList<String> getAnalysisFields() {
        return analysisFields;
    }
	public void setEdgeFields(ArrayList<String> edgeFields) {
		this.edgeFields = edgeFields;
	}
	public void setAnalysisFields(ArrayList<String> analysisFields) {
        this.analysisFields = analysisFields;
    }
	public String getEdgeDirectionality() {
		return edgeDirectionality;
	}
	public void setEdgeDirectionality(String edgeDirectionality) {
		this.edgeDirectionality = edgeDirectionality;
	}
	
	public ExtrapolationType getExtrapolationType() {
		return extrapolationType;
	}
	public void setExtrapolationType(ExtrapolationType extrapolationType) {
		this.extrapolationType = extrapolationType;
	}
	
    public FileReadInformation getFileReadInfo() {
		return fileReadInfo;
	}
	public void setFileReadInfo(FileReadInformation fileReadInfo) {
		this.fileReadInfo = fileReadInfo;
	}
	public int getCurrentRegionId() {
        return currentRegionId;
    }
    public void setCurrentRegionId(int currentRegionId) {
        this.currentRegionId = currentRegionId;
    }
    
    public TimeInterpolationInfo getTimeInterpolationInfo() {
		return timeInterpolationInfo;
	}
	public void setTimeInterpolationInfo(TimeInterpolationInfo timeInterpolationInfo) {
		this.timeInterpolationInfo = timeInterpolationInfo;
	}
	public int getStepStencil() {
		return stepStencil;
	}
	public void setStepStencil(int stepStencil) {
		this.stepStencil = stepStencil;
	}
	public LinkedHashMap<String, ArrayList<String>> getExtrapolatedFieldGroups() {
		return extrapolatedFieldGroups;
	}
	public void setExtrapolatedFieldGroups(LinkedHashMap<String, ArrayList<String>> extrapolatedFieldGroups) {
		this.extrapolatedFieldGroups = extrapolatedFieldGroups;
	}
	
	public ArrayList<String> getAuxiliaryFields() {
		return auxiliaryFields;
	}
	public void setAuxiliaryFields(ArrayList<String> auxiliaryFields) {
		this.auxiliaryFields = auxiliaryFields;
	}
	/** 
     * Returns the region ids that belong to internal regions. 
     * @return  The id list.
     */
    public ArrayList<String> getInteriorRegionIds() {
        ArrayList<String> tmp = new ArrayList<String>();
        for (int i = 0; i < regionIds.size(); i++) {
            if (CodeGeneratorUtils.isNumber(regionIds.get(i))) {
                tmp.add(regionIds.get(i));
            }
        }
        return tmp;
    }
    
    /**
     * Gets the derivative auxiliary fields.
     * @return	the dervative auxiliary fields
     */
    public ArrayList<String> getDerivativeAuxiliaryFields() {
    	ArrayList<String> tmp = new ArrayList<String>();
        for (int i = 0; i < auxiliaryFields.size(); i++) {
        	if (!postRefinementAuxiliaryFields.contains(auxiliaryFields.get(i))) {
        		tmp.add(auxiliaryFields.get(i));
        	}
        }
        return tmp;
    }
    
    /**
     * Returns the group name for a extrapolated field.
     * @param field		Field finding its group
     * @return			The extrapolated field group
     */
    public String getExtrapolatedGroupName(String field) {
    	Iterator<String> groupsIt = extrapolatedFieldGroups.keySet().iterator();
    	while (groupsIt.hasNext()) {
    		String groupName = groupsIt.next();
    		if (extrapolatedFieldGroups.get(groupName).contains(field)) {
    			return groupName;
    		}
    	}
    	return null;
    }
    
    /**
     * Get the number of dimensions of the problem.
     * @return      The number of spatial dimensions
     */
    public int getDimensions() {
        if (coordinates == null)
            return 0;
        return coordinates.size();
    }
    
}
