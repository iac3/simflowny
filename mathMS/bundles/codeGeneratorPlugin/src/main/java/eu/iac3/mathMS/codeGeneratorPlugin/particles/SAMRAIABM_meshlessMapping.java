/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.codeGeneratorPlugin.particles;

import eu.iac3.mathMS.codeGeneratorPlugin.CGException;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.CodeGeneratorUtils;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.ProblemInfo;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.SAMRAIUtils;

import java.io.File;
import java.util.ArrayList;

import org.w3c.dom.Document;

/**
 * Mapping of the particle problem for SAMRAI.
 * @author bminyano
 *
 */
public final class SAMRAIABM_meshlessMapping {
    
    static final String IND = "\u0009";
    static String mtUri = "http://www.w3.org/1998/Math/MathML";
    static final int THREE = 3;
    static final int FOUR = 4;
    static final String NL = System.getProperty("line.separator"); 
    static final String XSL = "common" + File.separator + "XSLTmathmlToC" + File.separator + "instructionToC.xsl";
    
    /**
     * Private constructor.
     */
    private SAMRAIABM_meshlessMapping() { };
    
    /**
     * Generates the mapping file.
     * @param pi                    The problem information
     * @return                      The code
     * @throws CGException          CG004 External error
     */
    public static String create(ProblemInfo pi) throws CGException {
        try {
            //Parameters for the xsl transformation. It is not a condition formula
            String[][] xslParams = new String [THREE][2];
            xslParams[0][0] =  "condition";
            xslParams[0][1] =  "false";
            xslParams[1][0] =  "indent";
            xslParams[1][1] =  "0";
            xslParams[2][0] =  "dimensions";
            xslParams[2][1] =  String.valueOf(pi.getCoordinates());
            
            int dimensions = pi.getSpatialDimensions();
            int stencil = pi.getSchemaStencil();
            
            String mapping = "";
            //Getting all the variables used in the initial conditions
            Document problem = pi.getProblem();
            
            //Declaration of spatial coordinates, and its variables for start and end.
            for (int i = 0; i < pi.getSpatialDimensions(); i++) {
                String coord = SAMRAIUtils.variableNormalize(pi.getCoordinates().get(i));
                String contCoorc = CodeGeneratorUtils.discToCont(pi.getProblem(), coord);
                mapping = mapping + IND + IND + "int " + coord + ", " + coord + "term, " + coord + "MapStartC, " + coord + "MapEndC, "
                    + coord + "l, " + coord + "u;" + NL
                    + IND + IND + "double " + coord + "MapStart, " + coord + "MapEnd, tmp" + coord + ";" + NL
                    + IND + IND + contCoorc + "Glower = d_grid_geometry->getXLower()[" + i + "];" + NL
                    + IND + IND + contCoorc + "Gupper = d_grid_geometry->getXUpper()[" + i + "];" + NL;
            }
           
            //Global variable declarations
            mapping = mapping + IND + IND + "int minBlock[" + dimensions + "], maxBlock[" 
                + dimensions + "], unionsI, facePointI, ie1, ie2, ie3, proc, tmp, pred, nodes;" + NL
                + IND + IND + "double maxDistance, e1, e2, e3;" + NL
                + IND + IND + "double position[" + dimensions + "], maxPosition[" + dimensions + "], minPosition[" + dimensions + "];" + NL
                + IND + IND + "bool workingArray[mpi.getSize()], finishedArray[mpi.getSize()], workingGlobal, finishedGlobal, "
                        + "working, finished, modif;" + NL
                + IND + IND + "double SQRT3INV = 1.0/sqrt(3.0);" + NL
                + getMaxStencil(problem, stencil, pi.getCoordinates());
            
            for (int i = 0; i < dimensions; i++) {
                //Initialization of the variables
                String coordString = pi.getCoordinates().get(i);
                mapping =  mapping + IND + IND + coordString + "lastG = d_grid_geometry->getPhysicalDomain().front().numberCells()[" + i 
                    + "] + 2 * d_ghost_width;" + NL;
            }
            
            //Cell mapping must be separated from particles. The cell mapping is mandatory to check boundary conditions.
            mapping = mapping + NL + IND + IND + "//Cell mapping" + NL
                    + getPatchLoop(problem, pi, IND + IND, false)
                    + mapMathematicalInteriorCell(pi.getCoordinates())
                    + IND + IND + "}" + NL;
            //Boundary mapping
            mapping = mapping + mapBoundariesCell(problem, pi);
            //Particle mapping
            mapping = mapping + IND + IND + "//Particle mapping" + NL
                    + getPatchLoop(problem, pi, IND + IND, true)
                    + particleSupport(pi.getProblem(), pi.getCoordinates()) + NL
                    + IND + IND + IND + "if (particleDistribution.compare(\"RANDOM\") == 0) {" + NL
                    + mapDomain(pi.getCoordinates())
                    + IND + IND + IND + "} else {" + NL
                    + mapMathematicalInterior(pi.getCoordinates())
                    + IND + IND + IND + "}" + NL
                    + mapBoundaries(pi)
                    + IND + IND + "}";
                
            return mapping + NL + NL
                    + "#mappingFunctions#" + NL
                    + "#mappingDeclarations#" + NL
                    + "#endMappingDeclarations#" + NL
                    + "#x3dArrays#" + NL;
        } 
        catch (CGException e) {
            throw e;
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new CGException(CGException.CG00X, e.getMessage());
        }
    }
    
    /**
     * Gets the maximum stencil of the problem. Depends on the simulation parameters.
     * @param problem       The problem
     * @param stencil       The cell stencil
     * @param coordinates   The coordinates
     * @return              The code
     * @throws CGException  CG004 External error
     */
    private static String getMaxStencil(Document problem, int stencil, ArrayList<String> coordinates) throws CGException {
        //Max stencil
        String maxStencil = "numberOfCellsRadius_" + CodeGeneratorUtils.discToCont(problem, coordinates.get(0));
        int dimensions = coordinates.size();
        String localStencil = "";
        for (int i = 0; i < dimensions; i++) {
            String coord = CodeGeneratorUtils.discToCont(problem, coordinates.get(i));
            String stencilAcc = "";
            stencilAcc = stencilAcc + "ceil(" + stencil + "*(influenceRadius/particleSeparation_" 
                + coord + "))";
            if (i > 0) {
                    maxStencil = "MAX(" + maxStencil + ", numberOfCellsRadius_" + coord + ")";
            }
        }
        return localStencil
            + IND + IND + "mapStencil = " + maxStencil + ";" + NL + NL;
    }
    
    /**
     * Gives the particle support to SAMRAI grid. Instantiates the Particles class over every cell.
     * @param doc               The problem
     * @param coords            The spatial coordinates
     * @return                  The code
     * @throws CGException      CG00X External error
     */
    private static String particleSupport(Document doc, ArrayList<String> coords) throws CGException {
        int dim = coords.size();
        String loop = "";
        String index = "";
        String actualIndent = IND + IND + IND;
        String endLoop = "";
        String indexCell = "";
        for (int i = dim - 1; i >= 0; i--) {
            String coord = coords.get(i);
            loop = loop + actualIndent + "for (" + coord + " = boxfirst(" + i + "); " + coord + " <= boxlast(" + i + "); " + coord + "++) {" + NL;
            endLoop = actualIndent + "}" + NL + endLoop;
            actualIndent = actualIndent + IND;
            index = ", " + coord + index;
            indexCell = ", " + coord + " - boxfirst(" + i + ")" + indexCell;
        }
        index = index.substring(2);
        indexCell = indexCell.substring(2);
    
        return IND + IND + IND + "//Give particles support" + NL
            + loop
            + actualIndent + "hier::Index idx(" + index + ");" + NL
            + actualIndent + "if (!particleVariables->isElement(idx)) {" + NL
            + actualIndent + IND + "Particles* part = new Particles();" + NL
            + actualIndent + IND + "particleVariables->addItemPointer(idx, part);" + NL
            + actualIndent + "}" + NL
            + actualIndent + "if (!nonSyncVariable->isElement(idx)) {" + NL
            + actualIndent + IND + "NonSyncs* nonSyncPart = new NonSyncs();" + NL
            + actualIndent + IND + "nonSyncVariable->addItemPointer(idx, nonSyncPart);" + NL
            + actualIndent + "}" + NL
            + endLoop;
    }
    
    
    
    /**
     * Maps the mathematical region interior.
     * @param coordinates       The coordinates
     * @return                  The mapping
     * @throws CGException      CG004 External error
     */
    private static String mapMathematicalInterior(ArrayList<String> coordinates) throws CGException {
        String[][] xslParams = new String [THREE][2];
        xslParams[0][0] = "condition";
        xslParams[0][1] = "true";
        xslParams[1][0] = "indent";
        xslParams[1][1] = "1";
        xslParams[2][0] =  "dimensions";
        xslParams[2][1] =  String.valueOf(coordinates.size());
        
        String loop = "";
        String endLoop = "";
        String index = "";
        String preLoop = "";
        String limitCondition = "";
        String positions = "";
        String indent = IND + IND + IND + IND;
        for (int j = coordinates.size() - 1; j >= 0; j--) {
            String discCoord = coordinates.get(j);
            loop = loop + indent + "for (int count_" + discCoord + " = 0; " + discCoord + "MapStart + count_" + discCoord + " * particleSeparation_"
                + discCoord + " < " + discCoord + "MapEnd; count_" + discCoord + "++) {" + NL;
            //Staggering
            if (j == coordinates.size() - 1) {
                loop = loop + createStaggeringControl("count_" + discCoord, indent + IND, coordinates);
            }
            endLoop = indent + "}" + NL + endLoop;
            indent = indent + IND;
        }
        
        for (int i = 0; i < coordinates.size(); i++) {
            String discCoord = coordinates.get(i);
            //Staggering
            if (i == coordinates.size() - 1) {
                preLoop = preLoop + IND + IND + IND + IND + discCoord + "MapStart = d_grid_geometry->getXLower()[" + i + "] + particleSeparation_" 
                    + discCoord + "/2.0 - (floor(0.5 + d_ghost_width * (dx[" + i + "]/particleSeparation_" 
                    + discCoord + ")))*particleSeparation_" + discCoord + ";" + NL;
            }
            preLoop = preLoop + IND + IND + IND + IND + discCoord + "MapEnd = d_grid_geometry->getXUpper()[" + i + "] + d_ghost_width * dx[" 
                + i + "];" + NL;

            index = index + discCoord + ", ";
            positions = positions + indent + "position[" + i + "] = " + discCoord + "MapStart + count_" + discCoord + " * particleSeparation_" 
                + discCoord + ";" + NL
                + indent + discCoord + " = floor((position[" + i + "] - d_grid_geometry->getXLower()[" + i + "])/dx[" + i + "]);" + NL;
            limitCondition = limitCondition + discCoord + " >= boxfirst(" + i + ") && " + discCoord + " <= boxlast(" + i + ") && ";
            
        }
        
        limitCondition = limitCondition.substring(0, limitCondition.lastIndexOf(" &&"));
        index = index.substring(0, index.lastIndexOf(","));
        return IND + IND + IND + IND + "//Interior" + NL
            + preLoop + loop
            + positions
            + indent + "if (" + limitCondition + ") {" + NL
            + indent + IND + "hier::Index idx(" + index + ");" + NL
            + indent + IND + "Particles* part = particleVariables->getItem(idx);" + NL
            + indent + IND + "Particle* particle = new Particle(0, position, 1);"
            + NL
            + indent + IND + "Particle* oldParticle = part->overlaps(*particle);" + NL
            + indent + IND + "if (oldParticle != NULL) {" + NL
            + indent + IND + IND + "part->deleteParticle(*oldParticle);" + NL
            + indent + IND + "}" + NL
            + indent + IND + "part->addParticle(*particle);" + NL
            + indent + IND + "delete particle;" + NL
            + indent + "}" + NL
            + endLoop;
    }
    
    /**
     * Creates the staggering control.
     * @param checkVar          The variable used to check the staggering condition
     * @param indent            The actual indent
     * @param coordinates       The spatial coordinates
     * @return                  The code
     * @throws CGException      CG004 External error
     */
    private static String createStaggeringControl(String checkVar, String indent, ArrayList<String> coordinates) throws CGException {
        String with = "";
        String without = "";
        for (int i = 0; i < coordinates.size() - 1; i++) {
            String coord = coordinates.get(i);
            without = without + indent + IND + coord + "MapStart = d_grid_geometry->getXLower()[" + i + "] - (floor(d_ghost_width * (dx[" 
                + i + "]/particleSeparation_" + coord + ")))*particleSeparation_" + coord + ";" + NL;
            with = with + indent + IND + coord + "MapStart = d_grid_geometry->getXLower()[" + i + "] + particleSeparation_" + coord 
                + "/2.0 - (floor(0.5 + d_ghost_width * (dx[" + i + "]/particleSeparation_" + coord + ")))*particleSeparation_" 
                + coord + ";" + NL;
        }
        return indent + "if (particleDistribution.compare(\"STAGGERED\") == 0 && isEven(" + checkVar + ")) {" + NL
            + without
            + indent + "} else {" + NL
            + with
            + indent + "}" + NL;
    }
    
    /**
     * Maps the mathematical region interior.
     * @param coordinates       The coordinates
     * @return                  The mapping
     * @throws CGException      CG004 External error
     */
    private static String mapMathematicalInteriorCell(ArrayList<String> coordinates) throws CGException {
        String[][] xslParams = new String [THREE][2];
        xslParams[0][0] = "condition";
        xslParams[0][1] = "true";
        xslParams[1][0] = "indent";
        xslParams[1][1] = "0";
        xslParams[2][0] =  "dimensions";
        xslParams[2][1] =  String.valueOf(coordinates.size());
                
        
        String loop = "";
        String endLoop = "";
        String index = "";
        String preLoop = "";
        String indent = IND + IND + IND;
        for (int i = 0; i < coordinates.size(); i++) {
            String coord = coordinates.get(i);
            //The same coordinate system
            String minValue = "0 + d_ghost_width";
            String maxValue = "(boxlast1(" + i + ") - boxfirst1(" + i + ")) + d_ghost_width";
            preLoop = preLoop + IND + IND + IND + coord + "MapStartC = " + minValue + ";" + NL;
            preLoop = preLoop + IND + IND + IND + coord + "MapEndC = " + maxValue + ";" + NL;
            
            loop = loop + indent + "for (" + coordinates.get(i) + " = " + coordinates.get(i) + "MapStartC; " 
                + coordinates.get(i) + " <= " + coordinates.get(i) + "MapEndC; " + coordinates.get(i) + "++) {" + NL;
            index = index + coordinates.get(i) + ", ";
            endLoop = indent + "}" + NL + endLoop;
            indent = indent + IND;
        }
        String loopsStencil = "";
        index = index.substring(0, index.lastIndexOf(","));
        
        return IND + IND + IND + "//Interior" + NL 
            + preLoop + loop + indent + "vector(region, " + index + ") = 1;" + NL + endLoop
            + loopsStencil;
    }
    
    /**
     * Map the boundary conditions in the cells.
     * @param problem           The problem
     * @param problemInfo       The problem information
     * @return                  The code
     * @throws CGException      CG00X External error
     */
    private static String mapBoundariesCell(Document problem, ProblemInfo problemInfo) throws CGException {
        String result = IND + IND + "//Boundaries Mapping" + NL
                + getPatchLoop(problem, problemInfo, IND + IND, false);
        ArrayList<String> boundPrecedence = problemInfo.getBoundariesPrecedence();
        ArrayList<String> coordinates = problemInfo.getCoordinates();
        String ind = IND + IND + IND + IND;
        String index = "";
        for (int i = 0; i < coordinates.size(); i++) {
            String coord = coordinates.get(i);
            index = index + coord + ", ";
        }
        index = index.substring(0, index.lastIndexOf(","));
        
        for (int i = boundPrecedence.size() - 1; i >= 0; i--) {
            String axis = boundPrecedence.get(i).substring(0, boundPrecedence.get(i).indexOf("-"));
            String side = boundPrecedence.get(i).substring(boundPrecedence.get(i).indexOf("-") + 1);
            int boundIndex = coordinates.indexOf(CodeGeneratorUtils.contToDisc(problemInfo.getProblem(), axis)) * 2 + 1;
            int sideIdx = 0;
            if (side.toLowerCase().equals("upper")) {
                boundIndex++;
                sideIdx = 1;
            }
            
            result = result + IND + IND + IND + "//" + boundPrecedence.get(i) + NL
                    + IND + IND + IND + "if (patch->getPatchGeometry()->getTouchesRegularBoundary(" 
                    + coordinates.indexOf(CodeGeneratorUtils.contToDisc(problemInfo.getProblem(), axis)) + ", " + sideIdx + ")) {" + NL;
            if (side.toLowerCase().equals("lower")) {
                String loop = "";
                String endLoop = "";
                String tmpInd = ind;
                for (int j = coordinates.size() - 1; j >= 0; j--) {
                    String coord = coordinates.get(j);
                    if (j == coordinates.indexOf(CodeGeneratorUtils.contToDisc(problemInfo.getProblem(), axis))) {
                        loop = loop + tmpInd + "for (" + coord + " = 0; " + coord + " < d_ghost_width; " + coord + "++) {" + NL;
                    }
                    else {
                        loop = loop + tmpInd + "for (" + coord + " = 0; " + coord + " < " + coord + "last; " + coord + "++) {" + NL;
                    }
                    endLoop = tmpInd + "}" + NL + endLoop;
                    tmpInd = tmpInd + IND;
                }
                result = result + loop
                    + tmpInd + "vector(region, " + index + ") = -" + boundIndex + ";" + NL
                    + endLoop;
            }
            else {
                String loop = "";
                String endLoop = "";
                String tmpInd = ind;
                for (int j = coordinates.size() - 1; j >= 0; j--) {
                    String coord = coordinates.get(j);
                    if (j == coordinates.indexOf(CodeGeneratorUtils.contToDisc(problemInfo.getProblem(), axis))) {
                        loop = loop + tmpInd + "for (" + coord + " = " + coord + "last - d_ghost_width; " + coord + " < " + coord + "last; " 
                                + coord + "++) {" + NL;
                    }
                    else {
                        loop = loop + tmpInd + "for (" + coord + " = 0; " + coord + " < " + coord + "last; " + coord + "++) {" + NL;
                    }
                    endLoop = tmpInd + "}" + NL + endLoop;
                    tmpInd = tmpInd + IND;
                }
                result = result + loop
                    + tmpInd + "vector(region, " + index + ") = -" + boundIndex + ";" + NL
                    + endLoop;
            }
            result = result + IND + IND + IND + "}" + NL;
        }
        result = result + IND + IND + "}" + NL
                + IND + IND + "d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);" + NL + NL;
        
        return result;
    }
    
    /**
     * Map the boundary conditions.
     * @param pi                The problem information
     * @return                  The code
     * @throws CGException      CG00X External error
     */
    private static String mapBoundaries(ProblemInfo pi) throws CGException {
        String result = IND + IND + IND + "//Boundaries Mapping" + NL;
        ArrayList<String> boundPrecedence = pi.getBoundariesPrecedence();
        ArrayList<String> coordinates = pi.getCoordinates();
        String ind = IND + IND + IND;
        
        String limits = "";
        String loop = "";
        String endLoop = "";
        String positions = "";
        String index = "";
        String limitCondition = "";
        for (int j = coordinates.size() - 1; j >= 0; j--) {
            String discCoord = coordinates.get(j);
            String contCoord = CodeGeneratorUtils.discToCont(pi.getProblem(), discCoord);
            loop = loop + ind + "for (int count_" + discCoord + " = 0; " + discCoord + "MapStart + count_" + discCoord + " * particleSeparation_"
                + contCoord + " < " + discCoord + "MapEnd; count_" + discCoord + "++) {" + NL;
            //Staggering
            if (j == coordinates.size() - 1) {
                loop = loop + createStaggeringControl("count_" + discCoord, ind +  IND, coordinates);
            }
            endLoop = ind + "}" + NL + endLoop;
            ind = ind + IND;
        }
        for (int i = 0; i < coordinates.size(); i++) {
            String coord = coordinates.get(i);
            String contCoord = CodeGeneratorUtils.discToCont(pi.getProblem(), coord);
            if (i + 1 == coordinates.size()) {
                limits = limits + IND + IND + IND + coord + "MapStart = d_grid_geometry->getXLower()[" + i + "] + particleSeparation_" + contCoord 
                    + "/2.0 - (floor(0.5 + d_ghost_width * (dx[" + i + "]/particleSeparation_" + contCoord + ")))*particleSeparation_" 
                    + contCoord + ";" + NL;
            }
            limits = limits + IND + IND + IND + coord + "MapEnd = d_grid_geometry->getXUpper()[" + i + "] + d_ghost_width * dx[" + i + "];" + NL;
            positions = positions + ind + "position[" + i + "] = " + coord + "MapStart + count_" + coord + " * particleSeparation_" 
                    + contCoord + ";" + NL
                    + ind + coord + " = floor((position[" + i + "] - d_grid_geometry->getXLower()[" + i + "])/dx[" + i + "]);" + NL;
            index = index + coord + ", ";
            limitCondition = limitCondition + coord + " >= boxfirst(" + i + ") && " + coord + " <= boxlast(" + i + ") && ";
        }
        index = index.substring(0, index.lastIndexOf(","));
        limitCondition = limitCondition.substring(0, limitCondition.lastIndexOf(" &&"));
        String boundaries = "";
        for (int i = boundPrecedence.size() - 1; i >= 0; i--) {
            String axis = boundPrecedence.get(i).substring(0, boundPrecedence.get(i).indexOf("-"));
            String side = boundPrecedence.get(i).substring(boundPrecedence.get(i).indexOf("-") + 1);
            int boundIndex = coordinates.indexOf(CodeGeneratorUtils.contToDisc(pi.getProblem(), axis)) * 2 + 1;
            String condition = "";
            if (side.toLowerCase().equals("upper")) {
                boundIndex++;
                condition = CodeGeneratorUtils.contToDisc(pi.getProblem(), axis) + " > boxlast1(" 
                        + coordinates.indexOf(CodeGeneratorUtils.contToDisc(pi.getProblem(), axis)) + ")";
            }
            else {
                condition = CodeGeneratorUtils.contToDisc(pi.getProblem(), axis) + " < boxfirst1(" 
                        + coordinates.indexOf(CodeGeneratorUtils.contToDisc(pi.getProblem(), axis)) + ")";
            }
            boundaries = boundaries + ind + IND + "//" + boundPrecedence.get(i) + NL
                    + ind + IND + "if (" + condition + ") {" + NL
                    + ind + IND + IND + "hier::Index idx(" + index + ");" + NL
                    + ind + IND + IND + "Particles* part = particleVariables->getItem(idx);" + NL
                    + ind + IND + IND + "Particle* particle = new Particle(0, position, -" + boundIndex + ");" + NL
                    + ind + IND + IND + "Particle* oldParticle = part->overlaps(*particle);" + NL
                    + ind + IND + IND + "if (oldParticle != NULL) {" + NL
                    + ind + IND + IND + IND + "part->deleteParticle(*oldParticle);" + NL
                    + ind + IND + IND + "}" + NL
                    + ind + IND + IND + "part->addParticle(*particle);" + NL
                    + ind + IND + IND + "delete particle;" + NL
                    + ind + IND + "}" + NL;
        }
        
        result = result + limits
                + loop
                + positions
                + ind + "if (" + limitCondition + ") {" + NL
                + boundaries
                + ind + "}" + NL
                + endLoop;
        
        return result;
    }
    
    
    
    /**
     * Maps the mathematical region interior.
     * @param coordinates       The coordinates
     * @return                  The mapping
     * @throws CGException      CG004 External error
     */
    private static String mapDomain(ArrayList<String> coordinates) throws CGException {
        String domainSize = "";
        String positions = "";
        String index = "";
        String indexBox = "";
        String limitCondition = "";
        for (int i = 0; i < coordinates.size(); i++) {
            String discCoord = coordinates.get(i);

            domainSize = domainSize + " * (d_grid_geometry->getXUpper()[" + i + "] - d_grid_geometry->getXLower()[" 
                    + i + "]+ 2 * d_ghost_width * dx[" + i + "]) / influence_radius";
            positions = positions + IND + IND + IND + IND + IND + "position[" + i + "] = gsl_rng_uniform(r_map) * (d_grid_geometry->getXUpper()[" + i 
                    + "] - d_grid_geometry->getXLower()[" + i + "] + 2 * d_ghost_width * dx[" + i + "]) + d_grid_geometry->getXLower()[" + i 
                    + "] - d_ghost_width * dx[" + i + "];" + NL
                    + IND + IND + IND + IND + IND + discCoord + " = floor((position[" + i + "] - d_grid_geometry->getXLower()[" + i + "])/dx[" 
                    + i + "]);" + NL;
            limitCondition = limitCondition + discCoord + " >= boxfirst(" + i + ") && " + discCoord + " <= boxlast(" + i + ") && ";
            index = index + discCoord + ", ";
            indexBox = indexBox + discCoord + " - boxfirst(" + i + "), ";
        }
        limitCondition = limitCondition.substring(0, limitCondition.lastIndexOf(" &&"));
        index = index.substring(0, index.lastIndexOf(","));
        indexBox = indexBox.substring(0, indexBox.lastIndexOf(","));
        
        return IND + IND + IND + IND + "//All domain mapping" + NL
            + IND + IND + IND + IND + "int numberOfParticles_withGhosts = density_par" + domainSize + ";" + NL
            + IND + IND + IND + IND + "for (int n = 0; n < numberOfParticles_withGhosts; n++) {" + NL
            + positions
            + IND + IND + IND + IND + IND + "if (" + limitCondition + ") {" + NL
            + IND + IND + IND + IND + IND + IND + "hier::Index idx(" + index + ");" + NL
            + IND + IND + IND + IND + IND + IND + "Particles* part = particleVariables->getItem(idx);" + NL
            + IND + IND + IND + IND + IND + IND + "Particle* particle = new Particle(0, position, vector(region, " + indexBox + "));" + NL
            + IND + IND + IND + IND + IND + IND + "part->addParticle(*particle);" + NL
            + IND + IND + IND + IND + IND + IND + "delete particle;" + NL
            + IND + IND + IND + IND + IND + "}" + NL
            + IND + IND + IND + IND + "}" + NL;
    }
    
    /**
     * Generates the initial patch iterator and its common variables.
     * @param problem       The problem
     * @param pi            The problem information
     * @param ind           The current indent
     * @param particleMap   When the loop is generated for particle or cell mapping
     * @return              The instructions
     * @throws CGException 
     */
    private static String getPatchLoop(Document problem, ProblemInfo pi, String ind, boolean particleMap) throws CGException {
        String particleVars = "";
        if (particleMap) {
            particleVars = ind + IND + "std::shared_ptr< pdat::IndexData<Particles, pdat::CellGeometry > > "
                    + "particleVariables(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles, pdat::CellGeometry >, hier::PatchData>(patch->getPatchData(d_particleVariables_id)));" + NL
                    + ind + IND + "std::shared_ptr< pdat::IndexData<NonSyncs, pdat::CellGeometry > > "
                            + "nonSyncVariable(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< NonSyncs, pdat::CellGeometry >, hier::PatchData>(patch->getPatchData(d_nonSyncP_id)));" + NL
                    + ind + IND + "const hier::Index boxfirst = particleVariables->getGhostBox().lower();" + NL
                    + ind + IND + "const hier::Index boxlast  = particleVariables->getGhostBox().upper();" + NL;
        }
    
        return  ind + "for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {" + NL
                + ind + IND + "const std::shared_ptr< hier::Patch > patch = *p_it;" + NL
                + ind + IND + "int* region = ((pdat::CellData<int> *) patch->getPatchData(d_region_id).get())->getPointer();" + NL
                + ind + IND + "int* interior = ((pdat::CellData<int> *) patch->getPatchData(d_interior_regridding_value_id).get())->getPointer();" + NL
                + ind + IND + "int* nonSync = ((pdat::CellData<int> *) patch->getPatchData(d_nonSync_regridding_tag_id).get())->getPointer();" + NL
                + particleVars
                + ind + IND + "const hier::Index boxfirst1 = patch->getBox().lower();" + NL
                + ind + IND + "const hier::Index boxlast1  = patch->getBox().upper();" + NL + NL
                + ind + IND + "//Get delta spaces into an array. dx, dy, dz." + NL
                + ind + IND + "const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));" + NL
                + ind + IND + "const double* dx  = patch_geom->getDx();" + NL + NL
                + SAMRAIUtils.generateLasts(pi.getCoordinates(), ind + IND);
    }
}
