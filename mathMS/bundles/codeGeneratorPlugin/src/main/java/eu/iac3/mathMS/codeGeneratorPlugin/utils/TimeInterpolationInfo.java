/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.codeGeneratorPlugin.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

import eu.iac3.mathMS.codeGeneratorPlugin.CGException;


/**
 * Class with the relevant information of the time interpolation.
 * @author bminyano
 *
 */
public class TimeInterpolationInfo {
	private final AtomicInteger count = new AtomicInteger(0);
	
	
	public class TimeInterpolator {
		
		final int id; 
		String timeDiscretizationId;
	    private ArrayList<Float> timestepFactor;
	    private Element algorithm;
	    private ArrayList<String> algorithmGenericVars;
	    private ArrayList<Element> algorithmParams;
	    private ArrayList<Element> varAtStep;
	    private Set<String> fields;
	    
		public TimeInterpolator(String timeDiscretizationId, ArrayList<Float> timestepFactor, Element algorithm,
				ArrayList<String> algorithmGenericVars, ArrayList<Element> algorithmParams,
				ArrayList<Element> varAtStep, Set<String> fields) {
			super();
			this.timeDiscretizationId = timeDiscretizationId;
			this.timestepFactor = timestepFactor;
			this.algorithm = algorithm;
			this.algorithmGenericVars = algorithmGenericVars;
			this.algorithmParams = algorithmParams;
			this.varAtStep = varAtStep;
			this.fields = fields;
			id = count.incrementAndGet(); 
		}

		/**
		 * Add new fields to the interpolator.
		 * @param newFields	Fields to add
		 */
		public void addFields(Set<String> newFields) {
			fields.addAll(newFields);
		}
		
		public String getTimeDiscretizationId() {
			return timeDiscretizationId;
		}

		public Set<String> getFields() {
			return fields;
		}

		public Element getAlgorithm() {
			return algorithm;
		}

		public void setAlgorithm(Element algorithm) {
			this.algorithm = algorithm;
		}

		public ArrayList<Element> getVarAtStep() {
			return varAtStep;
		}

		public void setVarAtStep(ArrayList<Element> varAtStep) {
			this.varAtStep = varAtStep;
		}

		public ArrayList<Float> getTimestepFactor() {
			return timestepFactor;
		}

		public ArrayList<String> getAlgorithmGenericVars() {
			return algorithmGenericVars;
		}

		public void setAlgorithmGenericVars(ArrayList<String> algorithmGenericVars) {
			this.algorithmGenericVars = algorithmGenericVars;
		}

	    
		public ArrayList<Element> getAlgorithmParams() {
			return algorithmParams;
		}

		public int getId() {
			return id;
		}

	    /**
	     * Replaces the fields in a step variable template.
	     * @param pi				The problem information
	     * @param step				The number of step variable template
	     * @param includedVars		List of included variables
	     * @return					Step variables
	     * @throws CGException		CG00X External error
	     */
	    public ArrayList<String> getStepVariables(ProblemInfo pi, int step, ArrayList<String> includedVars) throws CGException {
	    	ArrayList<String> variables = new ArrayList<String>();
	    	for (String fieldName: fields) {
	    		String fieldString = SAMRAIUtils.variableNormalize(fieldName);
	    		if (!CodeGeneratorUtils.isAuxiliaryField(pi, fieldString) && includedVars.contains(fieldString)) {
		    		Element variable = (Element) varAtStep.get(step).cloneNode(true);
					NodeList fieldTags = CodeGeneratorUtils.find(variable, ".//sml:field");
					Text fieldValue = variable.getOwnerDocument().createTextNode(fieldName);
					for (int k = fieldTags.getLength() - 1; k >= 0; k--) {
						fieldTags.item(k).getParentNode().replaceChild(fieldValue, fieldTags.item(k));
					}
					String var = SAMRAIUtils.xmlTransform(variable, null);
					if (var.indexOf("(") > 0) {
						var = var.substring(0, var.indexOf("("));
					}
		    		variables.add(var);
	    		}
	    	}
	    	return variables;
	    }
		
		/**
		 * Get positions for time refine algorithm for a given coordinate.
		 * @param coordinate		The coordinate to get variables from
		 * @return					The coordinate variables
		 * @throws CGException		CG00X External error
		 */
	    public ArrayList<String> getPositionRefineTimeParticles(String coordinate) throws CGException {
	    	ArrayList<String> refineVars = new ArrayList<String>();
			//Custom refinement
			if (algorithm != null) {
				for (int j = 0; j < algorithmParams.size(); j++) {
	    			Element variable = (Element) algorithmParams.get(j).cloneNode(true);
	    			NodeList fieldTags = CodeGeneratorUtils.find(variable, ".//sml:field");
	        		Element particlePosition = CodeGeneratorUtils.createElement(variable.getOwnerDocument(), CodeGeneratorUtils.SMLURI, "particlePosition");
	        		particlePosition.setTextContent(coordinate);
	        		for (int k = fieldTags.getLength() - 1; k >= 0; k--) {
	    				fieldTags.item(k).getParentNode().replaceChild(particlePosition, fieldTags.item(k));
	    			}
	    			String var = SAMRAIUtils.xmlTransform(variable.getFirstChild().getFirstChild(), null);
	    			if (var.indexOf("(") > 0) {
	    				var = var.substring(0, var.indexOf("("));
	    			}
	    			refineVars.add(SAMRAIUtils.variableNormalize(var));
	    		}
			}
			//Linear refinement
			else {
		    	refineVars.add("position" + coordinate + "_p");
				refineVars.add("position" + coordinate);
			}
			return refineVars;
	    }
	    
	    /**
	     * Replaces the fields in a step variable template by the coordinates.
	     * @param pi				The problem information
	     * @param step				The number of step variable template
	     * @return					Step variables
	     * @throws CGException		CG00X External error
	     */
	    public ArrayList<String> getStepPositions(ProblemInfo pi, int step) throws CGException {
	    	ArrayList<String> variables = new ArrayList<String>();
	    	for (int i = 0; i < pi.getCoordinates().size(); i++) {
	    		String coordinate = pi.getContCoordinates().get(i);
	    		Element variable = (Element) varAtStep.get(step).cloneNode(true);
	    		Element particlePosition = CodeGeneratorUtils.createElement(variable.getOwnerDocument(), CodeGeneratorUtils.SMLURI, "particlePosition");
	    		particlePosition.setTextContent(coordinate);
				NodeList fieldTags = CodeGeneratorUtils.find(variable, ".//sml:field");
				for (int k = fieldTags.getLength() - 1; k >= 0; k--) {
					fieldTags.item(k).getParentNode().replaceChild(particlePosition, fieldTags.item(k));
				}
				String var = SAMRAIUtils.xmlTransform(variable, null);
				if (var.indexOf("(") > 0) {
					var = var.substring(0, var.indexOf("("));
				}
	    		variables.add(var);
	    	}
	    	return variables;
	    }
		
	    @Override
	    public boolean equals(Object obj) {
	    	return  (obj instanceof TimeInterpolator) &&  timeDiscretizationId.equals(((TimeInterpolator) obj).getTimeDiscretizationId());
	    }
	}
    
	ArrayList<Float> longestTimestepFactors;
	//A time interpolator is shared by multiple fields
	LinkedHashMap<String, TimeInterpolator> timeInterpolators;
	
    /**
     * Constructor.
     * @param problem        	The problem
     * @throws CGException 
     */
    public TimeInterpolationInfo(Document problem) throws CGException {
    	timeInterpolators = new LinkedHashMap<String, TimeInterpolator>();
    	
    	NodeList discretizations = CodeGeneratorUtils.find(problem, "//mms:PDEDiscretization[descendant::mms:timeInterpolation]");
    	for (int j = 0; j < discretizations.getLength(); j++) {
    		ArrayList<String> algorithmGenericVars;
        	ArrayList<Float> timestepFactors = new ArrayList<Float>();
        	Element discretization = (Element) discretizations.item(j);
        	
        	String schemaId = CodeGeneratorUtils.find(discretization, ".//mms:timeDiscretization/mms:id").item(0).getTextContent();
        	
    		Element timeInterpolation = (Element) discretization.getElementsByTagNameNS(CodeGeneratorUtils.MMSURI, "timeInterpolation").item(0);
        	NodeList factors = CodeGeneratorUtils.find(timeInterpolation, ".//mms:timestepFactor");
            for (int i = 0; i < factors.getLength(); i++) {
            	timestepFactors.add(Float.parseFloat(factors.item(i).getTextContent()));	
            }
            if (j == 0) {
            	longestTimestepFactors = new ArrayList<Float>(timestepFactors);
            }
            else {
            	if (timestepFactors.size() > longestTimestepFactors.size()) {
                  	longestTimestepFactors = new ArrayList<Float>(timestepFactors);
            	}
            }
            
            ArrayList<Element> varAtStep = new ArrayList<Element>();
        	NodeList vars = CodeGeneratorUtils.find(timeInterpolation, ".//mms:outputVariable");
            for (int i = 0; i < vars.getLength(); i++) {
            	varAtStep.add((Element) vars.item(i).getFirstChild().getFirstChild().cloneNode(true));	
            }
            Element algorithm = null;
            ArrayList<Element> algorithmParams = new ArrayList<Element>();
            algorithmGenericVars = calculateDefaultAlgorithmGenericParams();
            if (CodeGeneratorUtils.find(timeInterpolation, "./mms:timeInterpolator").getLength() > 0) {
             	Element function = (Element) CodeGeneratorUtils.find(timeInterpolation, "./mms:timeInterpolator").item(0).cloneNode(true);
            	algorithm = (Element) function.getElementsByTagNameNS(CodeGeneratorUtils.SMLURI, "simml").item(0);
            	algorithmParams = calculateAlgorithmParams((Element) function.getLastChild());
            	algorithmGenericVars = calculateAlgorithmGenericParams((Element) function.getFirstChild().getFirstChild().getNextSibling());	
            }
            Set<String> fields = new HashSet<String>();
            NodeList fieldEls = discretization.getElementsByTagNameNS(CodeGeneratorUtils.MMSURI, "field");
            for (int i = 0; i < fieldEls.getLength(); i++) {
            	fields.add(fieldEls.item(i).getTextContent());
            }
            
            TimeInterpolator ti = timeInterpolators.get(schemaId);
            if (ti == null) {
            	ti = new TimeInterpolator(schemaId, timestepFactors, algorithm, algorithmGenericVars, algorithmParams,
    				varAtStep, fields);
            }
            else {
            	ti.addFields(fields);	
            }
            timeInterpolators.put(schemaId, ti);
    	}     
    }
    
    
    
    public LinkedHashMap<String, TimeInterpolator> getTimeInterpolators() {
		return timeInterpolators;
	}



	public ArrayList<Float> getLongestTimestepFactors() {
		return longestTimestepFactors;
	}

	/**
     * Calculates the algorithm generic parameters from input.
     * @param inputVars		The input variables
     */
    private ArrayList<String> calculateAlgorithmGenericParams(Element inputVars) {
    	ArrayList<String> algorithmGenericVars = new ArrayList<String>();
    	NodeList params = inputVars.getChildNodes();
    	for (int i = 0; i < params.getLength(); i++) {
    		String paramName = params.item(i).getFirstChild().getTextContent();
    		algorithmGenericVars.add(paramName);
    	}
    	return algorithmGenericVars;
    }
    /**
     * Calculates the default algorithm generic parameters.
     * @return 
     */
    private ArrayList<String> calculateDefaultAlgorithmGenericParams() {
    	return new ArrayList<String>(Arrays.asList("un", "unp1"));
    }

    
    /**
     * Calculates the algorithm parameters from input.
     * @param inputVars		The input variables
     * @return 
     */
    private ArrayList<Element> calculateAlgorithmParams(Element inputVars) {
    	ArrayList<Element> algorithmParams = new ArrayList<Element>();
    	for (int i = 0; i < inputVars.getChildNodes().getLength(); i++) {
    		Element variable = (Element) inputVars.getChildNodes().item(i).cloneNode(true);
			algorithmParams.add(variable);
    	}
    	return algorithmParams;
    }
    
    /**
     * Check if the time interpolation is available for a variable given.
     * @param variable		The variable to check
     * @param pi			The problem information
     * @return				True if interpolable
     * @throws CGException 
     */
    public boolean isTimeInterpolable(String variable, ProblemInfo pi) throws CGException {
    	LinkedHashMap<String, ArrayList<String>> fieldVariables = pi.getFieldVariables();
    	Iterator<String> fields = fieldVariables.keySet().iterator();
    	while (fields.hasNext()) {
    		String field = fields.next();
    		ArrayList<String> refineVars = getFieldRefineTimeParticles(field);
    		if (refineVars.contains(variable)) {
    			if (pi.getFieldTimeLevels().get(field) > 1) {
    				return true;
    			}
				return false;
    		}
    	}
    	return false;
    }
    
    /**
	 * Get the variable ids to register time refinement for a variable.
	 * @param syncVar			The variable to synchronize
	 * @param fieldVariables	The variables derived from fields
	 * @return					The registration variables
	 * @throws CGException		CG003 Not possible to create code for the platform
	 *		 					CG00X External error
	 */
    public String getRegisterRefineVariables(String syncVar, LinkedHashMap<String, ArrayList<String>> fieldVariables) throws CGException {
    	//Search in every field
    	Iterator<String> fields = fieldVariables.keySet().iterator();
    	while (fields.hasNext()) {
    		String field = fields.next();
    		ArrayList<String> variables = fieldVariables.get(field);
    		if (variables.contains(syncVar)) {
    			ArrayList<String> refineVars = getFieldRefineTimeParticles(field);
    			String refineVarDec = "";
    			for (String f: refineVars) {
    				refineVarDec = refineVarDec + "d_" + f + "_id,";
    			}
    			return refineVarDec.substring(0, refineVarDec.lastIndexOf(","));
    		}
    	}
    	throw new CGException(CGException.CG003, "Variables for time interpolation not found.");
    }
    
	/**
	 * Get variables for time refine algorithm for a given field.
	 * @param field				The field to get variables from
	 * @return					The field variables
	 * @throws CGException		CG00X External error
	 */
    public ArrayList<String> getFieldRefineTimeParticles(String field) throws CGException {
    	ArrayList<String> refineVars = new ArrayList<String>();
		for (TimeInterpolator ti: timeInterpolators.values()) {
			if (ti.getFields().contains(field)) {
				//Custom refinement
				if (ti.getAlgorithm() != null) {
					ArrayList<Element> algParams = ti.getAlgorithmParams();
					for (int j = 0; j < algParams.size(); j++) {
		    			Element variable = (Element) algParams.get(j).cloneNode(true);
		    			NodeList fieldTags = CodeGeneratorUtils.find(variable, ".//sml:field");
		    			Text fieldValue = variable.getOwnerDocument().createTextNode(field);
		    			for (int k = fieldTags.getLength() - 1; k >= 0; k--) {
		    				fieldTags.item(k).getParentNode().replaceChild(fieldValue, fieldTags.item(k));
		    			}
		    			String var = SAMRAIUtils.xmlTransform(variable.getFirstChild().getFirstChild(), null);
		    			if (var.indexOf("(") > 0) {
		    				var = var.substring(0, var.indexOf("("));
		    			}
		    			refineVars.add(SAMRAIUtils.variableNormalize(var));
		    		}
				}
				//Linear refinement
				else {
			    	refineVars.add(SAMRAIUtils.variableNormalize(field) + "_p");
					refineVars.add(SAMRAIUtils.variableNormalize(field));
				}
			}
		}
		return refineVars;
    }
    
    /**
     * Returns the time interpolator for given variable.
     * @param variable		Substep variable
     * @return				Time interpolator
     * @throws CGException 
     */
    public TimeInterpolator getTimeInterpolatorForField(String variable) throws CGException {
    	String normVar = SAMRAIUtils.variableNormalize(variable);
    	for (TimeInterpolator ti: timeInterpolators.values()) {
        	for (String field: ti.getFields()) {
        		ArrayList<String> refineVars = getFieldRefineTimeParticles(field);
        		if (refineVars.contains(normVar)) {
        			return ti;
        		}
        	}
    	}
    	throw new CGException(CGException.CG003, "Time interpolator for variable " + variable + " not found.");
    }
    
}
