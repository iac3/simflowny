/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.codeGeneratorPlugin.particles;

import eu.iac3.mathMS.codeGeneratorPlugin.CGException;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.CodeGeneratorUtils;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.ProblemInfo;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.SAMRAIUtils;

import java.util.ArrayList;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Class to generate the movement file.
 * @author bminyano
 *
 */
public final class SAMRAIABM_meshlessMovement {

    static final String NL = System.getProperty("line.separator");
    static final String IND = "\u0009";
    static final String MTURI = "http://www.w3.org/1998/Math/MathML";
    static final int THREE = 3;
    static final String MMSURI = "urn:mathms";
    
    /**
     * Private constructor.
     */
    private SAMRAIABM_meshlessMovement() { };
    
    /**
     * Create the movement file.
     * @param pi                The problem information
     * @return                  The code for the movement
     * @throws CGException      CG003 Not possible to create code for the platform
     *                          CG004 External error
     */
    public static String create(ProblemInfo pi) throws CGException {
        Document doc = pi.getProblem();
        int dims = pi.getCoordinates().size();

        String index = "";
        String indexPar = "";
        String newIndexPar = "";
        String newIndex = "";
        String oldPosition = "";
        String newPosition = "";
        String checkMovement = "";
        String oldPos = "";
        String newPos = "";
        String lowersAndUppers = "";
        for (int i = 0; i < dims; i++) {
            String coord = pi.getCoordinates().get(i);
            String contCoord = CodeGeneratorUtils.discToCont(doc, coord);
            oldPosition = oldPosition + IND + "position[" + i + "] = (oldPos" + coord + " - " + contCoord + "Glower)/dx[" + i 
                + "];" + NL;
            newPosition = newPosition + IND + "newPosition[" + i + "] = (newPos" + coord + " - " + contCoord + "Glower)/dx[" + i 
                + "];" + NL
                + IND + "int newIndex" + i + " = index" + i + ";" + NL;
            checkMovement = checkMovement + IND + "if (greaterEq(newPosition[" + i + "], boxlast(" + i + ")+1) || newPosition["
                + i + "] < boxfirst(" + i + ")) {" + NL
                + IND + IND + "out = true;" + NL
                + IND + "} else {" + NL
                + IND + IND + "if (newPosition[" + i + "] < index" + i + " || greaterEq(newPosition[" + i + "], index" + i + "+1)) {"
                + NL
                + IND + IND + IND + "newIndex" + i + " = floor(newPosition[" + i + "]);" + NL
                + IND + IND + "}" + NL
                + IND + "}" + NL;
            lowersAndUppers = lowersAndUppers + IND + "int " + contCoord + "lower = patch_geom->getXLower()[" + i + "];" + NL
                    + IND + "int " + contCoord + "upper = patch_geom->getXUpper()[" + i + "];" + NL;
            index = index + "index" + i + ", ";
            indexPar = indexPar + "const int index" + i + ", ";
            newIndexPar = newIndexPar + "const int newIndex" + i + ", ";
            newIndex = newIndex + "newIndex" + i + ", ";

            oldPos = oldPos + "const double oldPos" + coord + ", ";
            newPos = newPos + "const double newPos" + coord + ", ";
        }
        index = index.substring(0, index.length() - 2);
        indexPar = indexPar.substring(0, indexPar.length() - 2);
        newIndexPar = newIndexPar.substring(0, newIndexPar.length() - 2);
        newIndex = newIndex.substring(0, newIndex.length() - 2);
        oldPos = oldPos.substring(0, oldPos.length() - 2);
        newPos = newPos.substring(0, newPos.length() - 2);
        
        return "/*" + NL
            + " * Move the particles if the problem have any" + NL
            + " */" + NL
            + "void Problem::moveParticles(const std::shared_ptr<hier::Patch > patch, std::shared_ptr< pdat::IndexData<Particles, "
            + "pdat::CellGeometry > > particleVariables, Particles* part, Particle* particle, " + indexPar 
            + ", " + oldPos + ", " + newPos + ", const double simPlat_dt, const double current_time, int pit)" + NL
            + "{" + NL
            + IND + "const hier::Index boxfirst1 = patch->getBox().lower();" + NL
            + IND + "const hier::Index boxlast1  = patch->getBox().upper();" + NL
            + IND + "const hier::Index boxfirst = particleVariables->getGhostBox().lower();" + NL
            + IND + "const hier::Index boxlast  = particleVariables->getGhostBox().upper();" + NL + NL
            + IND + "//Get delta spaces into an array. dx, dy, dz." + NL
            + IND + "std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));" + NL
            + lowersAndUppers
            + SAMRAIUtils.generateLasts(pi.getCoordinates(), IND) + NL
            + IND + "double position[" + dims + "];" + NL
            + IND + "double newPosition[" + dims + "];" + NL + NL
            + IND + "hier::Index idx(" + index + ");" + NL
            + oldPosition + NL
            + newPosition + NL
            + IND + "bool out = false;" + NL
            + checkMovement
            + IND + "if (!out) {" + NL
            + IND + IND + "hier::Index newIdx(" + newIndex + ");" + NL
            + IND + IND + "if (!(newIdx == idx)) {" + NL
            + changeCell(pi, index, newIndex)
            + IND + IND + "}" + NL
            + IND + "} else {" + NL    
            + IND + IND + "part->deleteParticle(*particle);" + NL
            + IND + "}" + NL
            + "}" + NL
            + "#moveDeclarations#" + NL
            + "void moveParticles(const std::shared_ptr<hier::Patch > patch, std::shared_ptr< pdat::IndexData<Particles, pdat::CellGeometry > > "
            + "particleVariables, Particles* part, Particle* particle, " + indexPar 
            + ", " + oldPos + ", " + newPos + ", const double simPlat_dt, const double current_time, int pit);" + NL
            + checkRegion(pi.getProblem(), pi.getCoordinates(), pi.getRegions(), newIndexPar, indexPar);
    }
    
    /**
     * Checks if there is any boundary that implies a region change when a particle comes in or goes out.
     * @param doc               The problem
     * @return                  True if a region checking should be done after a movement 
     * @throws CGException      CG004 External error
     */
    private static boolean containsRegionChangingBoundary(Document doc) throws CGException {
        String query = "//mms:boundaryCondition[descendant::mms:type = 'Non-Periodical']";
        return CodeGeneratorUtils.find(doc, query).getLength() > 0;
    }
    
    /**
     * Create the code for the checkRegion routine.
     * This routine has to check if a particle inside a boundary has gone out inside the interior and assigns the interior region.
     * @param doc               The problem
     * @param coords            The spatial coordinates
     * @param regionGroup      The list of regionGroup names
     * @param newIndexPar       new index parameters
     * @param indexPar          index parameters
     * @return                  The code
     * @throws CGException      CG004 External error
     */
    private static String checkRegion(Document doc, ArrayList<String> coords, ArrayList<String> regionGroup, String newIndexPar, 
            String indexPar) throws CGException {
        String result = "";
        String[][] xslParams = new String [THREE][2];
        xslParams[0][0] =  "condition";
        xslParams[0][1] =  "true";
        xslParams[1][0] =  "indent";
        xslParams[1][1] =  "0";
        xslParams[2][0] =  "dimensions";
        xslParams[2][1] =  String.valueOf(coords.size());
        
        NodeList boundaries = CodeGeneratorUtils.find(doc, "//mms:boundaryCondition");
        for (int j = 0; j < boundaries.getLength(); j++) {
            String type = boundaries.item(j).getFirstChild().getTextContent();
            if (type.toLowerCase().equals("non-periodical")) {
                String axis = boundaries.item(j).getFirstChild().getNextSibling().getTextContent();
                String side = boundaries.item(j).getFirstChild().getNextSibling().getNextSibling().getTextContent();
                String startCondition = "";
                String endCondition = "";
                String indentCondition = IND + IND;
                //condition of the boundary
                if (((Element) boundaries.item(j)).getElementsByTagNameNS(MMSURI, "condition").getLength() > 0) {
                    Element condition = (Element) ((Element) boundaries.item(j)).getElementsByTagNameNS(MMSURI, "condition").item(0);
                    startCondition = IND + IND + "if (" + SAMRAIUtils.xmlTransform(setNPPosition(
                            condition.getFirstChild().getFirstChild(), coords), xslParams) + ") {" + NL;
                    endCondition = IND + IND + "}" + NL;
                    indentCondition = IND + IND + IND;
                }
                for (int k = 0; k < coords.size(); k++) {
                    String coord = coords.get(k);
                    if (axis.toLowerCase().equals("all") || coord.equals(CodeGeneratorUtils.contToDisc(doc, axis))) {
                        if (side.toLowerCase().equals("all") || side.toLowerCase().equals("lower")) {
                            result = result + IND + "if (index" + k + " < boxfirst1(" + k + ") && newIndex" + k + " >= boxfirst1(" + k 
                                    + ") && particle->region == -" + (2 * k + 1) + ") {" + NL
                                    + startCondition
                                    + indentCondition + "particle->region = particle->newRegion;" + NL
                                    + endCondition
                                    + IND + "}" + NL;
                        }
                        if (side.toLowerCase().equals("all") || side.toLowerCase().equals("upper")) {
                            result = result + IND + "if (index" + k + " > boxlast1(" + k + ") && newIndex" + k + " <= boxlast1(" + k 
                                    + ") && particle->region == -" + (2 * k + 2) + ") {" + NL
                                    + startCondition
                                    + indentCondition + "particle->region = particle->newRegion;" + NL
                                    + endCondition
                                    + IND + "}" + NL;
                        }
                    }
                }
            }
        }
        if (!result.equals("")) {
            result = "#checkRegion#" + NL
                + "void Problem::checkRegion(Particle* particle, " + newIndexPar + ", " + indexPar 
                + ", const hier::Index boxfirst1, const hier::Index boxlast1) {" + NL
                + result
                + "}";
        }
        return result;
    }
    
    /**
     * Creates the code to change the cell in particle movement.
     * If an internal boundary enters a static or fixed boundary it must disappear.
     * If an static, maximal dissipation or static boundary [INFLOW condition] enters the problem a new particle must
     * be created in the boundary.
     * @param pi                The problem information
     * @param index             The index string
     * @param newIndex          The new index string
     * @return                  The code
     * @throws CGException      CG004 External error
     */
    private static String changeCell(ProblemInfo pi, String index, String newIndex) throws CGException {
        Document doc = pi.getProblem();
        
        String checkRegionCall = "";
        if (containsRegionChangingBoundary(doc)) {
            checkRegionCall = getMoveIntoBoundary(pi, IND + IND + IND + IND);
            checkRegionCall = checkRegionCall + IND + IND + IND + IND 
                + "//Check the region for particle moving outside or inside the boundary" + NL
                + IND + IND + IND + IND + "checkRegion(particle, " + newIndex + ", " + index + ", boxfirst1, boxlast1);" + NL;
        }
        
        return IND + IND + IND + "bool deleted = false;" + NL
            + IND + IND + IND + "if (!deleted) {" + NL
            + checkRegionCall
            + IND + IND + IND + IND + "//Need to create copy of the particle. Erase an item from a vector calls the destructor " 
            + "of the object" + NL
            + IND + IND + IND + IND + "Particle* newParticle = new Particle(*particle);" + NL
            + IND + IND + IND + IND + "part->deleteParticle(*particle);" + NL
            + IND + IND + IND + IND + "Particles* destPart = particleVariables->getItem(newIdx);" + NL
            + IND + IND + IND + IND + "destPart->addParticle(*newParticle);" + NL
            + IND + IND + IND + IND + "delete newParticle;" + NL
            + IND + IND + IND + "}" + NL;
    }
    
    /**
     * Set the region for the particles entering boundaries.
     * @param pi                The problem information
     * @param actualInd         The actual indentation
     * @return                  The code
     * @throws CGException      CG004 External error
     */
    private static String getMoveIntoBoundary(ProblemInfo pi, String actualInd) throws CGException {
        ArrayList<String> coords = pi.getCoordinates();
        int dims = pi.getDimensions();
        
        //Parameters for the xsl transformation. It is not a condition formula
        String[][] xslParams = new String [THREE][2];
        xslParams[0][0] =  "condition";
        xslParams[0][1] =  "false";
        xslParams[1][0] =  "indent";
        xslParams[1][1] =  "0";
        xslParams[2][0] =  "dimensions";
        xslParams[2][1] =  String.valueOf(pi.getDimensions());
        
        String index = "";
        String newIndex = "";
        for (int i = 0; i < dims; i++) {
            index = index + "index" + i + " - boxfirst(" + i + "), ";
            newIndex = newIndex + "newIndex" + i + " - boxfirst(" + i + "), ";

        }
        index = index.substring(0, index.length() - 2);
        newIndex = newIndex.substring(0, newIndex.length() - 2);
        

        String enteringReflection = "";
        for (int k = 0; k < coords.size(); k++) {
            enteringReflection = enteringReflection + actualInd + IND + "if (newIndex" + k + " < boxfirst1(" + k + ")) {" + NL
                + actualInd + IND + IND + "particle->newRegion = -" + (k * 2 + 1) + ";" + NL
                + actualInd + IND + "}" + NL
                + actualInd + IND + "if (newIndex" + k + " > boxlast1(" + k + ")) {" + NL
                + actualInd + IND + IND + "particle->newRegion = -" + (k * 2 + 2) + ";" + NL
                + actualInd + IND + "}" + NL;
        }
        
        return actualInd + "//Moving into a boundary. Change region id" + NL
                + actualInd + "if (vector(region, " + newIndex + ") < 0) {" + NL
                + enteringReflection
                + actualInd + "}" + NL;
    }
    
    /**
     * Modify the element to use the position of the new particle.
     * @param instruction       The instruction to modify
     * @param coordinates       The spatial coordinates
     * @return                  The instruction modified
     * @throws CGException      CG004 External error
     */
    private static Element setNPPosition(Node instruction, ArrayList<String> coordinates) throws CGException {
        for (int i = 0; i < coordinates.size(); i++) {
            String coord = coordinates.get(i);
            NodeList positions = CodeGeneratorUtils.find(instruction, ".//mt:ci[not(preceding-sibling::mt:eq and position() != 2) and " 
                    + "child::*[name() = 'mt:msup' or name() = 'mt:msub' or name() = 'mt:msubsup']/*[position() = 1 and name() = 'mt:ci'" 
                    + " and text() = '" + coord + "']]");
            for (int j = positions.getLength() - 1; j >= 0; j--) {
                Element ci = CodeGeneratorUtils.createElement(instruction.getOwnerDocument(), MTURI, "ci");
                ci.setTextContent("newPosition[" + i + "]");
                positions.item(j).getParentNode().replaceChild(ci, positions.item(j));
            }
        }
        return (Element) instruction;
    }
    
}
