/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.codeGeneratorPlugin;

import eu.iac3.mathMS.codeGeneratorPlugin.SAMRAI.LinkerSAMRAI;
import eu.iac3.mathMS.codeGeneratorPlugin.fvm.SAMRAIABM_meshExecution;
import eu.iac3.mathMS.codeGeneratorPlugin.fvm.SAMRAIABM_meshInitializations;
import eu.iac3.mathMS.codeGeneratorPlugin.fvm.SAMRAIABM_meshMapping;
import eu.iac3.mathMS.codeGeneratorPlugin.fvm.SAMRAIPDEAnalysis;
import eu.iac3.mathMS.codeGeneratorPlugin.fvm.SAMRAIPDEExecution;
import eu.iac3.mathMS.codeGeneratorPlugin.fvm.SAMRAIPDEFunctions;
import eu.iac3.mathMS.codeGeneratorPlugin.fvm.SAMRAIPDEInitializations;
import eu.iac3.mathMS.codeGeneratorPlugin.fvm.SAMRAIPDEMapping;
import eu.iac3.mathMS.codeGeneratorPlugin.fvm.SAMRAIPDEMovement;
import eu.iac3.mathMS.codeGeneratorPlugin.particles.SAMRAIABM_meshlessExecution;
import eu.iac3.mathMS.codeGeneratorPlugin.particles.SAMRAIABM_meshlessFunctions;
import eu.iac3.mathMS.codeGeneratorPlugin.particles.SAMRAIABM_meshlessInitializations;
import eu.iac3.mathMS.codeGeneratorPlugin.particles.SAMRAIABM_meshlessMapping;
import eu.iac3.mathMS.codeGeneratorPlugin.particles.SAMRAIABM_meshlessMovement;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.CodeGeneratorUtils;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.ExtrapolationType;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.ProblemInfo;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.SAMRAIUtils;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.TimeInterpolationInfo;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.TimeInterpolationInfo.TimeInterpolator;
import eu.iac3.mathMS.codesDBPlugin.CodeResult;
import eu.iac3.mathMS.codesDBPlugin.CodesDB;
import eu.iac3.mathMS.codesDBPlugin.CodesDBImpl;
import eu.iac3.mathMS.codesDBPlugin.DCException;
import eu.iac3.mathMS.documentManagerPlugin.DocumentManagerImpl;
import eu.iac3.mathMS.latexPdfGeneratorPlugin.LatexPDFGeneratorImpl;
import eu.iac3.mathMS.simflownyUtilsPlugin.SimflownyUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;

import org.osgi.service.log.LogService;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * The processes to generate the code for SimPlat platform.
 * 
 * @author bminyano
 *
 */
public class SimPlatSAMRAICodeGenerator extends CodeGeneratorImpl {
    
    static final String NL = System.getProperty("line.separator");
    static final String IND = "\u0009";
    final String mtUri = "http://www.w3.org/1998/Math/MathML";
    static String spUri = "urn:simPlat";
    static String mmsUri = "urn:mathms";
    static final int THREE = 3;
    static final int INTERPHASELENGTH = 3;
     
    /**
     * Constructor to take the bundle context.
     */
    public SimPlatSAMRAICodeGenerator() {
        super();
        new SAMRAIUtils();
    }
    /**
     * Generates the code for SimPlat.
     * 
     * @param document            the document to generate the code from
     * @return                  the document of the generated code
     * @throws CGException      CG001 XML document does not match xml schema
     *                           CG002 Code already exist.
     *                           CG003 Not possible to create code for the platform
     *                           CG004 External error
     */
    public String generateSAMRAICode(String document) throws CGException {
        CodesDB cdb = null;
        String id = null;
        try {
            long startInicial = System.currentTimeMillis();
            String xmlDoc = SimflownyUtils.jsonToXML(document);
            
            Document doc = CodeGeneratorUtils.stringToDom(xmlDoc);
            SAMRAIUtils.selectXSLTransformer("discretizedProblem");
            //Validation of the problem received
            CodeGeneratorUtils.schemaValidation(xmlDoc, "discretizedProblem");

            //Check that the code was not generated previously. We can delete the manual work over the files.
            Date now = new Date();
            String name = doc.getDocumentElement().getFirstChild().getFirstChild().getTextContent();
            cdb = new CodesDBImpl();
            String author = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(mmsUri, "author").item(0).getTextContent();
            String version = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(mmsUri, "version").item(0).getTextContent();
            CodeResult[] crArray = cdb.getCodeList(name, author, version, now, now, PlatformType.simPlatSAMRAI.toString());
            if (crArray != null) {
                throw new CGException(CGException.CG002);
            }
            String problemIdentifier = SAMRAIUtils.variableNormalize(name);

            //Get the common information
            ProblemInfo pi = new ProblemInfo();
            pi.setConstants(CodeGeneratorUtils.getConstants(doc));
            pi.setParticleSpecies(CodeGeneratorUtils.getParticleSpecies(doc));
            pi.setProblemName(problemIdentifier);
            pi.setHasABMMeshFields(CodeGeneratorUtils.isABMMeshProblem(doc));
            pi.setHasABMMeshlessFields(CodeGeneratorUtils.isABMMeshlessProblem(doc));
            pi.setHasMeshFields(CodeGeneratorUtils.hasMeshFields(doc));
            pi.setHasParticleFields(CodeGeneratorUtils.hasParticleFields(doc));
            
            if (pi.isHasABMMeshFields() || pi.isHasABMMeshlessFields()) {
                CodeGeneratorUtils.addImplicitParameters(doc);
                CodeGeneratorUtils.replaceEquivalences(doc);
            }
            CodeGeneratorUtils.replaceDefinitions(doc);
            pi.setCompactSupportRatio(CodeGeneratorUtils.getCompactSupportRatio(doc));
            pi.setFileReadInfo(CodeGeneratorUtils.processFilesToRead(doc));
            pi.setAnalysisFields(SAMRAIUtils.getAnalysisFields(doc));
            pi.setAuxiliaryFields(SAMRAIUtils.getAuxiliaryFields(doc));
            pi.setPostRefinementAuxiliaryFields(SAMRAIUtils.setPostRefinementAuxiliaryFields(doc, pi.getAuxiliaryFields()));
            pi.setCoordinates(CodeGeneratorUtils.getProblemSpatialCoordinates(doc));
            pi.setTimeCoord(CodeGeneratorUtils.getProblemTimeCoordinate(doc));
            pi.setGridLimits(CodeGeneratorUtils.getGridLimits(pi.getCoordinates(), doc));
            pi.setBoundariesPrecedence(CodeGeneratorUtils.getBoundariesPrecedence(doc));
            pi.setHasExternalInitialCondition(CodeGeneratorUtils.getHasExternalInitialCondition(doc));
            pi.setRegionPrecedence(CodeGeneratorUtils.getRegionPrecedence(doc));
            pi.setRegions(CodeGeneratorUtils.getRegions(doc, pi.getRegionPrecedence()));
            pi.setRegionIds(CodeGeneratorUtils.getRegionIds(doc, pi.getRegions(), pi.getBoundariesPrecedence(), pi));
            pi.setRegionRelations(CodeGeneratorUtils.getRegionRelations(doc, pi.getRegions()));
            pi.setSchemaStencil(CodeGeneratorUtils.getSchemaStencil(doc));
            pi.setExtrapolationStencil(CodeGeneratorUtils.getExtrapolationStencil(doc));
            pi.setStepStencil(CodeGeneratorUtils.getStepStencil(doc, pi.getSchemaStencil()));
            pi.setFieldTimeLevels(CodeGeneratorUtils.getFieldTimeLevels(doc));
            pi.setFunctionNames(CodeGeneratorUtils.getFunctionNames(doc));
         //   pi.setMovementInfo(CodeGeneratorUtils.getMovementInfo(doc));
            if (pi.isHasABMMeshFields() || pi.isHasABMMeshlessFields()) {
                pi.setEvolutionStep(CodeGeneratorUtils.getEvolutionStep(doc));
            }
            if (pi.isHasParticleFields()) {
                pi.setPositionTimeLevels(CodeGeneratorUtils.getPositionTimeLevels(doc, pi.getCoordinates(), pi.getTimeCoord()));
            }
            if (pi.isHasABMMeshlessFields()) {
                pi.setPositionTimeLevels(CodeGeneratorUtils.getPositionTimeLevels(doc, pi.getCoordinates(), pi.getTimeCoord()));
            }
            if (pi.isHasMeshFields() || pi.isHasParticleFields()) {
                pi.setHardRegions(CodeGeneratorUtils.getHardRegions(doc, pi.getCoordinates()));
                pi.setExtrapolationType(CodeGeneratorUtils.getExtrapolationType(doc, pi.getDimensions()));
               //pi.setVarFields(CodeGeneratorUtils.getVarFields(doc, pi.getTimeCoord(), pi.getProblemType(), true)); //TODO movement ancient code
                pi.setFieldVariables(CodeGeneratorUtils.getFieldVariables(doc, pi.getTimeCoord()));
                pi.setExtrapolatedFieldGroups(CodeGeneratorUtils.getExtrapolatedFieldGroups(pi.getHardRegions(), pi.getFieldTimeLevels().keySet()));
                pi.setExtrapolFuncVars(SAMRAIUtils.extrapolationFunctionVariables(doc, pi));
            }
            if (pi.isHasABMMeshFields()) {
            	pi.setExtrapolationType(ExtrapolationType.generalPDE);
            }
            
            //Preprocess the code to avoid platform specific issues
            if (pi.isHasMeshFields() || pi.isHasParticleFields() || pi.isHasABMMeshFields()) {
                doc = SAMRAIUtils.preprocess(doc, pi, true);
            }
            else {
                doc = SAMRAIUtils.preprocess(doc, pi, false);
            }
            pi.setVariables(SAMRAIUtils.getVariables(doc, pi.getFieldTimeLevels(), pi.getCoordinates(), 
            		pi.getExtrapolationType()));
            pi.setProblem(doc);
            pi.setExtrapolVars(CodeGeneratorUtils.getExtrapolVars(pi));
            pi.setContCoordinates(CodeGeneratorUtils.getProblemContSpatialCoordinates(pi));
            pi.setPeriodicalBoundary(CodeGeneratorUtils.getPeriodicalBoundaries(doc, pi.getContCoordinates()));
            pi.setTimeInterpolationInfo(new TimeInterpolationInfo(doc));
            pi.setVariableInfo(CodeGeneratorUtils.getVariableInfo(pi));
            //TODO movement
            /*if (pi.getMovementInfo().itMoves()) {
            	pi.setX3dMovRegions(CodeGeneratorUtils.getX3dMovRegions(pi, context));
            }*/
            if (pi.getSpatialDimensions() == 1) {
                throw new CGException(CGException.CG003, "Simplat + SAMRAI does not allow 1D simulation");
            }
            //Creation of the code
            if (logservice != null) {
                logservice.log(LogService.LOG_INFO, "Generating code: " + problemIdentifier);
            }
 
            id = cdb.addGeneratedCode(name, author, version, PlatformType.simPlatSAMRAI.toString());
            String executionStep = "";
            String initializations = "";
            String finalization = "";
            String mapping = "";
            String functions = "";
            String analysis = "";
            String synchronization = "";
            String movement = "";
            if (logservice != null) {
                logservice.log(LogService.LOG_INFO, "intermediate configuration created");
            }
            //finalization.simPlat
            if (pi.isHasMeshFields() || pi.isHasParticleFields()) {
                finalization = finalizationSimPlat(pi);
            } 
            else {
                finalization = finalizationABM(pi);
            }
            
            if (logservice != null) {
                logservice.log(LogService.LOG_INFO, "intermediate finalization created");
            }
            //synchronization.simPlat
            if (pi.isHasMeshFields() || pi.isHasParticleFields()) {
                synchronization = synchronizationSimPlat(pi);
            }
            else {
                synchronization = synchronizationABMSimPlat(pi);
            }
            if (logservice != null) {
                logservice.log(LogService.LOG_INFO, "intermediate synchronization created");
            }
            if (pi.isHasMeshFields() || pi.isHasParticleFields()) {
                //mapping
                mapping = SAMRAIPDEMapping.create(pi);
                //initializations
                initializations = SAMRAIPDEInitializations.create(pi);
                //executionStep
                executionStep = SAMRAIPDEExecution.create(pi);
                //functions
                functions = SAMRAIPDEFunctions.create(pi);
                //analysis
                analysis = SAMRAIPDEAnalysis.create(pi);
            }
            if (pi.isHasABMMeshFields()) {
                //mapping.simPlat
                mapping = SAMRAIABM_meshMapping.create(pi);
                //initializations.simPlat
                initializations = SAMRAIABM_meshInitializations.create(pi);
                //executionStep.simPlat
                executionStep = SAMRAIABM_meshExecution.create(pi);
                //functions.simPlat
                functions = SAMRAIPDEFunctions.create(pi);
            }
            if (pi.isHasABMMeshlessFields()) {
                //mapping.simPlat
                mapping = SAMRAIABM_meshlessMapping.create(pi);
                //initializations.simPlat
                initializations = SAMRAIABM_meshlessInitializations.create(pi);
                //executionStep.simPlat
                executionStep = SAMRAIABM_meshlessExecution.create(pi);
                //functions.simPlat
                functions = SAMRAIABM_meshlessFunctions.create(pi);
            }
            
            if (logservice != null) {
                logservice.log(LogService.LOG_INFO, "intermediate mapping created");
            }
            if (logservice != null) {
                logservice.log(LogService.LOG_INFO, "intermediate initializations created");
            }
            if (logservice != null) {
                logservice.log(LogService.LOG_INFO, "intermediate executionStep created");
            }
            if (logservice != null) {
                logservice.log(LogService.LOG_INFO, "intermediate functions created");
            }
            //particleMovement.simPlat
            if (pi.isHasParticleFields()) {
                movement = SAMRAIPDEMovement.create(pi);
                if (logservice != null) {
                    logservice.log(LogService.LOG_INFO, "intermediate particleMovement created");
                }
            }
            if (pi.isHasABMMeshlessFields()) {
                movement = SAMRAIABM_meshlessMovement.create(pi);
                if (logservice != null) {
                    logservice.log(LogService.LOG_INFO, "intermediate particleMovement created");
                }
            }
            //movement.simPlat
          /*  if (pi.getMovementInfo().itMoves()) {
                movement = SAMRAIFVMMovement.create(pi);
            }*/
            //problem.input
            cdb.addCodeFile(id, "parameter", "problem.input", 
                    problemInput(pi));
            if (logservice != null) {
                logservice.log(LogService.LOG_INFO, "problem.input created");
            }
                        
            //run.sh
            cdb.addCodeFile(id, "compiler", "run.sh", 
                    CodeGeneratorUtils.runSh(problemIdentifier));
            if (logservice != null) {
                logservice.log(LogService.LOG_INFO, "run.sh created");
            }
            
            System.out.println("Documentacion");
            //Documentation (PDFs)
            //Models
            try {
            	NodeList models = CodeGeneratorUtils.find(doc, "//mms:modelId");
            	for (int i = 0; i < models.getLength(); i++) {
	            	String model = new DocumentManagerImpl().getDocument(models.item(i).getTextContent());
		            cdb.addCodeFile(id, "documentation", "model_" + models.item(i).getTextContent() + ".pdf", 
		            		new LatexPDFGeneratorImpl().xmlToPdf(model));
            	}
            } 
            catch (Exception e) {
            	System.out.println("Error adding model xml or pfd");
            }
            //Problem
            try {
            	String contProblem = new DocumentManagerImpl().getDocument(doc.getElementsByTagNameNS(mmsUri, "continuousProblemId").item(0).getTextContent());
	            cdb.addCodeFile(id, "documentation", "problem.pdf", 
	            		new LatexPDFGeneratorImpl().xmlToPdf(contProblem));
            } 
            catch (Exception e) {
            	System.out.println("Error adding problem xml or pfd");
            }
            //Dependencies
            try {
        		String docId = CodeGeneratorUtils.find(doc, "/*/mms:head/mms:id").item(0).getTextContent();
        		System.out.println("Añadir dependencias de " + docId);
        	    CodeGeneratorUtils.loadDocumentAndDependecies(id, cdb, docId, new HashSet<String>());
            } 
            catch (Exception e) {
            	e.printStackTrace(System.out);
            	System.out.println("Error adding policy xml");
            }
        
            //Link intermediate files to SAMRAI
            new LinkerSAMRAI().link(id, executionStep, initializations, finalization, 
                    mapping, functions, analysis, synchronization, movement, pi);
            System.out.println("Final " + (System.currentTimeMillis() - startInicial));
            return SimflownyUtils.xmlToJSON(CodeGeneratorUtils.createCodeDocument(name, author, version, now, PlatformType.simPlatSAMRAI.toString(), id), false);
        }
        catch (CGException e) {
            e.printStackTrace();
            //If any exception occured, delete the generated code.
            if (id != null) {
                try {
                    cdb.deleteGeneratedCode(id);
                } 
                catch (DCException e1) {
                    e1.printStackTrace();
                    throw new CGException(CGException.CG00X, e1.getMessage());
                }
            }
            throw e;
        }
        catch (Exception e) {
            e.printStackTrace();
            //If any exception occured, delete the generated code.
            if (id != null) {
                try {
                    cdb.deleteGeneratedCode(id);
                } 
                catch (DCException e1) {
                    e1.printStackTrace();
                    throw new CGException(CGException.CG00X, e1.getMessage());
                }
            }
            throw new CGException(CGException.CG00X, e.getMessage());
        }
    }
      
    /**
     * Creates the finalization check.
     * @param pi                The problem information
     * @return                  The file
     * @throws CGException      CG004 External error
     */
    private String finalizationABM(ProblemInfo pi) throws CGException {
        String result = "";
        Document doc = pi.getProblem();
        String[][] xslParams = new String [4][2];
        xslParams[0][0] =  "condition";
        xslParams[0][1] =  "false";
        xslParams[1][0] =  "indent";
        xslParams[1][1] =  "0";
        xslParams[2][0] =  "dimensions";
        xslParams[2][1] =  String.valueOf(pi.getDimensions());
        xslParams[3][0] =  "timeCoord";
        xslParams[3][1] =  pi.getTimeCoord();
        final int andLength = 4;
        
        try {
            xslParams[0][1] =  "true";
            xslParams[1][1] =  "0";
            NodeList finalization = CodeGeneratorUtils.find(doc, "/*/mms:finalizationConditions/mms:finalizationCondition");
            
            int dimensions = pi.getSpatialDimensions();
            
            for (int i = 0; i < finalization.getLength(); i++) {
                String actualIndent = "";
                Node condition = finalization.item(i);
                NodeList mathExpressions = condition.getChildNodes();
                
                //If the finalization condition uses a variable the code has to loop over all the patches.
                if (CodeGeneratorUtils.usesVariable(condition, doc)) {
                    result = result + "for ( ln=0; ln<=d_patch_hierarchy->getFinestLevelNumber(); ++ln ) {" + NL
                        + IND + "std::shared_ptr<hier::PatchLevel > level(d_patch_hierarchy->getPatchLevel(ln));" + NL
                        + IND + "//Fill ghosts and periodical boundaries, but no other boundaries" + NL
                        + IND + "d_bdry_sched_advance[ln]->fillData(current_time, false);" + NL
                        + IND + "for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {" + NL
                        + IND + IND + "const std::shared_ptr<hier::Patch >& patch = *p_it;" + NL;
                
                    result = result + SAMRAIUtils.getPDEVariableDeclaration(condition, pi, 2, new ArrayList<String>(), 
                            "patch->", false, false) + NL;
                    
                    result = result + IND + IND + "//Get the dimensions of the patch" + NL 
                        + IND + IND + "hier::Box pbox = patch->getBox();" + NL
                        + IND + IND + "const hier::Index boxfirst = patch->getBox().lower();" + NL
                        + IND + IND + "const hier::Index boxlast  = patch->getBox().upper();" + NL + NL
                        + IND + IND + "//Get delta spaces into an array. dx, dy, dz." + NL
                        + IND + IND + "const std::shared_ptr<geom::CartesianPatchGeometry<" + dimensions 
                        + "> > patch_geom(patch->getPatchGeometry(), SAMRAI_SHARED_PTR_CAST_TAG);" + NL
                        + IND + IND + "const double* dx  = patch_geom->getDx();" + NL + NL;
                    
                    actualIndent = IND + IND;
                    //Start the evolution for the region.
                    //Put the parameter "condition" to false for the xsl parameters
                    xslParams[0][1] =  "false";
                    xslParams[1][1] =  "2";
                    //Initialization of the variables for iteration
                    for (int j = 0; j < dimensions; j++) {
                        //Initialization of the variables
                        String coordString = pi.getCoordinates().get(i);
                        result =  result + actualIndent + "int " + coordString + "last = boxlast(" + j + ")-boxfirst(" 
                            + j + ") + 1 + d_ghost_width;" + NL;
                    }
                    for (int j = dimensions - 1; j >= 0; j--) {
                        String coordString = pi.getCoordinates().get(i);
                        //Loop iteration
                        result =  result + actualIndent + "for(int " + coordString + " = d_ghost_width; " 
                            + coordString + " < " + coordString + "last; " + coordString + "++) {" + NL;
                        actualIndent = actualIndent + IND;
                    }
                }
                
                //If there is a condition
                boolean applyIf = false;
                if (mathExpressions.item(0).getLocalName().equals("applyIf")) {
                    xslParams[1][1] =  "0";
                    applyIf = true;
                    String instruction = SAMRAIUtils.xmlTransform(mathExpressions.item(0).getFirstChild().getFirstChild(), xslParams);
                    result = result + actualIndent + "if (" + instruction + ") {" + NL;
                    actualIndent = actualIndent + IND;
                }
                
                String endCondition = "";
                //Finalization conditions
                mathExpressions = mathExpressions.item(mathExpressions.getLength() - 1).getChildNodes();
                for (int k = 0; k < mathExpressions.getLength(); k++) {
                    Node math = mathExpressions.item(k);
                    endCondition = endCondition + " && " 
                        + SAMRAIUtils.xmlTransform(math, xslParams);
                }
                endCondition = endCondition.substring(andLength);
                result = result + actualIndent + "if (" + endCondition + ") { " + NL + actualIndent + IND 
                    + "return true;" + NL + actualIndent + "}" + NL;
                
                //Close if clause if exists
                if (applyIf) {
                    actualIndent = actualIndent.substring(0, actualIndent.lastIndexOf(IND));
                    result = result + actualIndent + "}" + NL + NL;
                }
                
                if (CodeGeneratorUtils.usesVariable(condition, doc)) {
                    
                    //close the loop iteration
                    for (int j = 0; j < dimensions; j++) {
                        actualIndent = actualIndent.substring(0, actualIndent.lastIndexOf(IND));
                        result =  result + actualIndent + "}" + NL;
                    }
                    
                    result = result + IND + "}" + NL
                        + "}" + NL;
                }
                
            }
            result = result + "return false;" + NL + NL;
            
            return result;
        }
        catch (CGException e) {
            throw e;
        }
    }

    /**
     * Creates the finalization check.
     * @param pi                The problem information
     * @return                  The file
     * @throws CGException      CG004 External error
     */
    private String finalizationSimPlat(ProblemInfo pi) throws CGException {
        String result = "";
        Document doc = pi.getProblem();
        String[][] xslParams = new String [4][2];
        xslParams[0][0] =  "condition";
        xslParams[0][1] =  "false";
        xslParams[1][0] =  "indent";
        xslParams[1][1] =  "0";
        xslParams[2][0] =  "dimensions";
        xslParams[2][1] =  String.valueOf(pi.getDimensions());
        xslParams[3][0] =  "timeCoord";
        xslParams[3][1] =  pi.getTimeCoord();
        final int andLength = 4;
        
        try {
            xslParams[0][1] =  "true";
            xslParams[1][1] =  "0";
            NodeList finalization = CodeGeneratorUtils.find(doc, "/*/mms:finalizationConditions/mms:finalizationCondition");
            
            int dimensions = pi.getSpatialDimensions();
            
            for (int i = 0; i < finalization.getLength(); i++) {
                String actualIndent = "";
                NodeList equations = finalization.item(i).getChildNodes();
                
                //If the finalization condition uses a variable the code has to loop over all the patches.
                if (CodeGeneratorUtils.usesVariable(finalization.item(i), doc)) {
                    result = result + "for ( ln=0; ln<=d_patch_hierarchy->getFinestLevelNumber(); ++ln ) {" + NL
                        + IND + "std::shared_ptr<hier::PatchLevel > level(d_patch_hierarchy->getPatchLevel(ln));" + NL
                        + IND + "//Fill ghosts and periodical boundaries, but no other boundaries" + NL
                        + IND + "d_bdry_sched_advance[ln]->fillData(current_time, false);" + NL
                        + IND + "for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {" + NL
                        + IND + IND + "const std::shared_ptr<hier::Patch >& patch = *p_it;" + NL;
                
                    result = result + SAMRAIUtils.getPDEVariableDeclaration(finalization.item(i), pi, 2, new ArrayList<String>(), 
                            "patch->", false, false) + NL;
                    
                    result = result + IND + IND + "//Get the dimensions of the patch" + NL 
                        + IND + IND + "hier::Box pbox = patch->getBox();" + NL
                        + IND + IND + "const hier::Index boxfirst = patch->getBox().lower();" + NL
                        + IND + IND + "const hier::Index boxlast  = patch->getBox().upper();" + NL + NL
                        + IND + IND + "//Get delta spaces into an array. dx, dy, dz." + NL
                        + IND + IND + "const std::shared_ptr<geom::CartesianPatchGeometry<" + dimensions 
                        + "> > patch_geom(patch->getPatchGeometry(), SAMRAI_SHARED_PTR_CAST_TAG);" + NL
                        + IND + IND + "const double* dx  = patch_geom->getDx();" + NL + NL;
                    
                    actualIndent = IND + IND;
                    //Start the evolution for the region.
                    //Put the parameter "condition" to false for the xsl parameters
                    xslParams[0][1] =  "false";
                    xslParams[1][1] =  "2";
                    //Initialization of the variables for iteration
                    for (int j = 0; j < dimensions; j++) {
                        //Initialization of the variables
                        String coordString = pi.getCoordinates().get(i);
                        result =  result + actualIndent + "int " + coordString + "last = boxlast(" + j + ")-boxfirst(" 
                            + j + ") + 2 + d_ghost_width;" + NL;
                    }
                    for (int j = dimensions - 1; j >= 0; j--) {
                        String coordString = pi.getCoordinates().get(i);
                        //Loop iteration
                        result =  result + actualIndent + "for(int " + coordString + " = d_ghost_width; " 
                            + coordString + " < " + coordString + "last; " + coordString + "++) {" + NL;
                        actualIndent = actualIndent + IND;
                    }
                }
                
                int equationsFrom = 0;
                //If there is a condition
                if (finalization.item(i).getLocalName().equals("condition")) {
                    xslParams[1][1] =  "0";
                    equationsFrom = 1;
                    String instruction = SAMRAIUtils.xmlTransform(equations.item(0).getFirstChild().getFirstChild(), xslParams);
                    result = result + actualIndent + "if (" + instruction + ") {" + NL;
                    actualIndent = actualIndent + IND;
                }
                
                String endCondition = "";
                //Finalization conditions
                for (int k = equationsFrom; k < equations.getLength(); k++) {
                    endCondition = endCondition + " && " 
                        + SAMRAIUtils.xmlTransform(equations.item(k).getLastChild().getFirstChild(), xslParams);
                }
                endCondition = endCondition.substring(andLength);
                result = result + actualIndent + "if (" + endCondition + ") { " + NL + actualIndent + IND 
                    + "return true;" + NL + actualIndent + "}" + NL;
                
                //Close if clause if exists
                if (equationsFrom == 1) {
                    actualIndent = actualIndent.substring(0, actualIndent.lastIndexOf(IND));
                    result = result + actualIndent + "}" + NL + NL;
                }
                
                if (CodeGeneratorUtils.usesVariable(finalization.item(i), doc)) {
                    
                    //close the loop iteration
                    for (int j = 0; j < dimensions; j++) {
                        actualIndent = actualIndent.substring(0, actualIndent.lastIndexOf(IND));
                        result =  result + actualIndent + "}" + NL;
                    }
                    
                    result = result + IND + "}" + NL
                        + "}" + NL;
                }
                
            }
            result = result + "return false;" + NL + NL;
            
            return result;
        }
        catch (CGException e) {
            throw e;
        }
    }
    
    /**
     * Creates the synchronization information check.
     * @param pi                The problem information
     * @return                  The file
     * @throws CGException      CG004 External error
     */
    private String synchronizationSimPlat(ProblemInfo pi) throws CGException {
    	String problemHVariables = "";
    	for (TimeInterpolator ti: pi.getTimeInterpolationInfo().getTimeInterpolators().values()) {
    		problemHVariables = problemHVariables + IND + "std::shared_ptr<TimeInterpolator> time_interpolate_operator_mesh" + ti.getId() + ";" + NL;	
    	}
        for (String speciesName : pi.getParticleSpecies()) {
        	problemHVariables = problemHVariables + IND + "std::shared_ptr<TimeInterpolator> time_interpolate_operator_particles_" + speciesName + ";" + NL;
        }
        String problemVariables = "";
        String resetHierarchyConfiguration1 = "";
        String resetHierarchyConfiguration2 = "";
        String variableRegistrations = "";
        String coarsenRegistrations = "";
        if (pi.isHasMeshFields()) {
            Document doc = pi.getProblem();
            //Execution flow synchronization
            //Iterate over all the blocks
            int blocks = CodeGeneratorUtils.getBlocksNum(doc, "//mms:interiorExecutionFlow/sml:simml|//mms:surfaceExecutionFlow/sml:simml");
            for (int i = 0; i < blocks; i++) {
                //If the current block has stencil > 0 then a synchronization must be done
                boolean sync = CodeGeneratorUtils.find(doc, "//sml:simml/*[position() = " + (i + 1) 
                        + " and local-name() = 'iterateOverCells' and (@stencilAtt > 0 or @subStepSyncAtt = 'true') and not(ancestor::mms:postInitialConditions "
                        + "or ancestor::mms:analysis)]/*").getLength() > 0;
                boolean syncBoundaries = CodeGeneratorUtils.find(doc, "//sml:simml/*[position() = " + (i + 1) 
                        + " and local-name() = 'boundary' and (descendant::sml:fieldExtrapolation and @typeAtt = 'flux') "
                        + "and not(ancestor::mms:postInitialConditions or ancestor::mms:analysis)]/*").getLength() > 0;
                boolean subStepSync = CodeGeneratorUtils.find(doc, "//sml:simml/*[position() = " + (i + 1) 
                        + " and ((local-name() = 'iterateOverCells' or local-name() = 'boundary') and @subStepSyncAtt) "
                        + "and not(ancestor::mms:postInitialConditions or ancestor::mms:analysis)]/*").getLength() > 0;
                if (sync || syncBoundaries) {
                    DocumentFragment blockScope = doc.createDocumentFragment();
                    NodeList blockList = CodeGeneratorUtils.find(doc, "//sml:simml/*[position() = " + (i + 1) 
                    		+ " and (local-name() = 'iterateOverCells' or local-name() = 'boundary') and "
                    		+ "not(ancestor::mms:postInitialConditions or ancestor::mms:analysis)]/*");
                    for (int j = 0; j < blockList.getLength(); j++) {
                        blockScope.appendChild(blockList.item(j).cloneNode(true));
                    }
                	ArrayList<String> syncVars = 
                			SAMRAIUtils.getSyncVariables(blockScope, pi.getVariables(), pi.getDimensions(), pi.getExtrapolationType());
                 	String varSincro = "";
                 	String varTimeSincro = "";
                 	String indent = IND;
                    if (subStepSync) {
                    	indent = indent + IND;
                    }
                    boolean syncCode = false;
                    for (int j = 0; j < syncVars.size(); j++) {
                    	String var = syncVars.get(j);
                    	//Synchronize variables
                		syncCode = true;
                    	varSincro = varSincro + indent + "d_bdry_fill_advance" + i + "->registerRefine(d_" + var 
                             + "_id,d_" + var + "_id,d_" + var + "_id,refine_operator);" + NL;
                    	//Synchronization variables
                        if (subStepSync) {
                        	//If there is multiple time levels, make time interpolation
                        	if (pi.getTimeInterpolationInfo().isTimeInterpolable(var, pi)) {
                        		varTimeSincro = varTimeSincro + indent + "d_bdry_fill_advance" + i + "->registerRefine(d_" + var 
	                                    + "_id,d_" + var + "_id," + pi.getTimeInterpolationInfo().getRegisterRefineVariables(var, 
	                                    		pi.getFieldVariables()) + ",d_" + var + "_id,refine_operator,tio_mesh" + pi.getTimeInterpolationInfo().getTimeInterpolatorForField(var).getId() + ");" + NL;	
                        	}
                        	else {
                        		varTimeSincro = varTimeSincro + indent + "d_bdry_fill_advance" + i + "->registerRefine(d_" + var 
                                        + "_id,d_" + var + "_id,d_" + var + "_id,refine_operator);" + NL;
                        	}
                        }
                    }
                    if (syncCode) {
                    	problemHVariables = problemHVariables + IND + "std::shared_ptr< xfer::RefineAlgorithm > d_bdry_fill_advance" + i + ";" + NL
                                + IND + "std::vector< std::shared_ptr< xfer::RefineSchedule > > d_bdry_sched_advance" + i + ";" + NL;
                    	problemVariables = problemVariables + IND + "d_bdry_fill_advance" + i + " = std::shared_ptr< " 
                                + "xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());" + NL;
                    	resetHierarchyConfiguration1 = resetHierarchyConfiguration1 + IND + "d_bdry_sched_advance" + i 
                                + ".resize(finest_hiera_level+1);" + NL;
                    	resetHierarchyConfiguration2 = resetHierarchyConfiguration2 + IND + IND + "d_bdry_sched_advance" + i + "[ln] = " 
                    			+ "d_bdry_fill_advance" + i + "->createSchedule(level,ln-1,new_hierarchy,this);" + NL;
                        if (subStepSync) {
                        	variableRegistrations = variableRegistrations + IND + "if (d_refinedTimeStepping) {" + NL
                        			+ varTimeSincro
                        			+ IND + "} else {" + NL
                        			+ varSincro
                        			+ IND + "}" + NL;
                        }
                        else {
                        	variableRegistrations = variableRegistrations + varSincro;
                        }
                    }
                }
            }
                            
            //Iterate over all the blocks
            blocks = CodeGeneratorUtils.getBlocksNum(doc, "/*/mms:analysis/sml:simml");
            for (int i = 0; i < blocks; i++) {
            	boolean sync = CodeGeneratorUtils.find(doc, "/*/mms:analysis/sml:simml/*[position() = " + (i + 1) 
                        + " and local-name() = 'iterateOverCells' and @stencilAtt > 0]/*").getLength() > 0;
                //patch loop and synchronizations
                if (sync) {
                    DocumentFragment blockScope = doc.createDocumentFragment();
                    NodeList blockList = CodeGeneratorUtils.find(doc,  CodeGeneratorUtils.getAnalysisSynchronizationQuery(i));
                    for (int j = 0; j < blockList.getLength(); j++) {
                        blockScope.appendChild(blockList.item(j).cloneNode(true));
                    }
                	ArrayList<String> syncVars = 
                			SAMRAIUtils.getSyncVariables(blockScope, pi.getVariables(), pi.getDimensions(), pi.getExtrapolationType());
                	
                    problemHVariables = problemHVariables + IND + "std::shared_ptr< xfer::RefineAlgorithm > d_bdry_fill_analysis" 
                        + i + ";" + NL
                        + IND + "std::vector< std::shared_ptr< xfer::RefineSchedule > > d_bdry_sched_analysis" + i + ";" + NL;
                    problemVariables = problemVariables + IND + "d_bdry_fill_analysis" + i + " = std::shared_ptr< " 
                        + "xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());" + NL;
                    resetHierarchyConfiguration1 = resetHierarchyConfiguration1 + IND + "d_bdry_sched_analysis" + i 
                        + ".resize(finest_hiera_level+1);" + NL;
                    resetHierarchyConfiguration2 = resetHierarchyConfiguration2 + IND + IND + "d_bdry_sched_analysis" + i + "[ln] = " 
                        + "d_bdry_fill_analysis" + i + "->createSchedule(level,this);" + NL;
                    for (int j = 0; j < syncVars.size(); j++) {
                        String var = syncVars.get(j);
                        variableRegistrations = variableRegistrations + IND + "d_bdry_fill_analysis" + i + "->registerRefine(d_" + var 
                            + "_id,d_" + var + "_id,d_" + var + "_id,refine_operator);" + NL;
                    }
                }
            }                
            
            NodeList fields = CodeGeneratorUtils.find(doc, "/*/mms:fields/mms:field|/*/mms:auxiliaryFields/mms:auxiliaryField");
            for (int i = 0; i < fields.getLength(); i++) {
                String field = SAMRAIUtils.variableNormalize(fields.item(i).getTextContent());
                if (!pi.getVariableInfo().getAllParticleVariables().contains(field)) {
	                variableRegistrations = variableRegistrations + IND + "d_bdry_fill_init->registerRefine(d_" + field + "_id,d_" + field 
	                    + "_id,d_" + field + "_id,refine_operator);" + NL
	                    + IND + "d_bdry_post_coarsen->registerRefine(d_" + field + "_id,d_" + field 
	                    + "_id,d_" + field + "_id,refine_operator);" + NL;
	                coarsenRegistrations = coarsenRegistrations + IND + "d_coarsen_algorithm->registerCoarsen(d_" + field + "_id,d_" 
	                    + field + "_id,coarsen_operator);" + NL;
                }
            }
            ArrayList<String> variables = new ArrayList<String>();
            NodeList blockList = CodeGeneratorUtils.find(doc, "//mms:postInitialCondition//sml:fieldExtrapolation");
            for (int i = 0; i < blockList.getLength(); i++) {
                Element variable = (Element) blockList.item(i);
                String field = variable.getAttribute("fieldAtt");
                String normField = SAMRAIUtils.variableNormalize(field);
                if (pi.getExtrapolationType() != ExtrapolationType.generalPDE) {
                    for (int j = 0; j < pi.getDimensions(); j++) {
                        String coord = pi.getCoordinates().get(j);
                        if (!variables.contains("extrapolatedSB" + field + "SP" + coord)) {
                            variables.add("extrapolatedSB" + field + "SP" + coord);
                            
                            variableRegistrations = variableRegistrations + IND + "d_bdry_fill_init->registerRefine(d_extrapolatedSB" 
                                    + normField + "SP" + coord + "_id , d_extrapolatedSB" + normField + "SP" + coord + "_id, d_extrapolatedSB" 
                                    + normField + "SP" + coord + "_id, refine_operator);" + NL
                                    + IND + "d_bdry_post_coarsen->registerRefine(d_extrapolatedSB" 
                                    + normField + "SP" + coord + "_id , d_extrapolatedSB" + normField + "SP" + coord + "_id, d_extrapolatedSB" 
                                    + normField + "SP" + coord + "_id, refine_operator);" + NL;
                            
                        }    
                    }
                }
            }
        }
        //All the fields are inside the particle variable for Particles
        if (pi.isHasParticleFields()) {
    
            problemVariables = problemVariables + IND + "d_bdry_fill_advance = std::shared_ptr< xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());" + NL;
            resetHierarchyConfiguration1 = resetHierarchyConfiguration1 + IND + "d_bdry_sched_advance.resize(finest_hiera_level+1);" + NL;
            resetHierarchyConfiguration2 = resetHierarchyConfiguration2 + IND + IND + "d_bdry_sched_advance[ln] = d_bdry_fill_advance->createSchedule(level,ln-1,new_hierarchy,this);" + NL;
            for (String speciesName : pi.getParticleSpecies()) {
                String timeVars = "";
                TimeInterpolator ti = pi.getTimeInterpolationInfo().getTimeInterpolatorForField(pi.getVariableInfo().getParticleFields().get(speciesName).get(0));
                for (int i = 0; i < ti.getVarAtStep().size(); i++) {
                	timeVars = timeVars + ",d_particleVariables_" + speciesName + "_id";
                }
                if (ti.getAlgorithm() == null) {
                	timeVars = ",d_particleVariables_" + speciesName + "_id";
                }
            	
            	
	            variableRegistrations = variableRegistrations + IND + "d_bdry_fill_init->registerRefine(d_particleVariables_" + speciesName + "_id,d_particleVariables_" + speciesName + "_id,d_particleVariables_" + speciesName + "_id,refine_particles_" + speciesName + ");" + NL
	            		+ IND + "if (d_refinedTimeStepping) {" + NL
	            		+ IND + IND + "d_bdry_fill_advance->registerRefine(d_particleVariables_" + speciesName + "_id,d_particleVariables_" + speciesName + "_id,d_particleVariables_" + speciesName + "_id" + timeVars + ",d_particleVariables_" + speciesName + "_id,refine_particles_" + speciesName + ", tio_particles_" + speciesName + ");" + NL
	            		+ IND + "} else {" + NL
	            		+ IND + IND + "d_bdry_fill_advance->registerRefine(d_particleVariables_" + speciesName + "_id,d_particleVariables_" + speciesName + "_id,d_particleVariables_" + speciesName + "_id,refine_particles_" + speciesName + ");" + NL
	            		+ IND + "}" + NL;
	            problemHVariables = problemHVariables + IND + "ParticleRefine<Particle_" + speciesName + "> * refine_particle_operator_" + speciesName + ";" + NL;
            }
            problemHVariables = problemHVariables + IND + "std::shared_ptr< xfer::RefineAlgorithm > d_bdry_fill_advance;" + NL
            		+ IND + "std::vector< std::shared_ptr< xfer::RefineSchedule > > d_bdry_sched_advance;" + NL;
        }
        return "#Problem.h variables#" + NL
                + IND + "std::shared_ptr< xfer::RefineAlgorithm > d_bdry_fill_init;" + NL
                + IND + "std::shared_ptr< xfer::RefineAlgorithm > d_bdry_post_coarsen;" + NL
                + IND + "std::vector< std::shared_ptr< xfer::RefineSchedule > > d_bdry_sched_postCoarsen;" + NL
                + problemHVariables
                + "#Problem variables#" + NL
                + IND + "d_bdry_fill_init = std::shared_ptr< xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());" + NL
                + IND + "d_bdry_post_coarsen = std::shared_ptr< xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());" + NL
                + problemVariables
                + IND + "d_coarsen_algorithm = std::shared_ptr< xfer::CoarsenAlgorithm >(new xfer::CoarsenAlgorithm(d_dim));" + NL
                + "#resetHierarchyConfiguration#" + NL
                + resetHierarchyConfiguration1
                + IND + "d_coarsen_schedule.resize(finest_hiera_level+1);" + NL
                + IND + "d_bdry_sched_postCoarsen.resize(finest_hiera_level+1);" + NL
                + IND + "//  Build coarsen and refine communication schedules." + NL
                + IND + "for (int ln = coarsest_level; ln <= finest_hiera_level; ln++) {" + NL
                + IND + IND + "std::shared_ptr< hier::PatchLevel > level(new_hierarchy->getPatchLevel(ln));" + NL
                + resetHierarchyConfiguration2
                + IND + IND + "d_bdry_sched_postCoarsen[ln] = d_bdry_post_coarsen->createSchedule(level);" + NL
                + IND + IND + "// coarsen schedule only for levels > 0" + NL
                + IND + IND + "if (ln > 0) {" + NL
                + IND + IND + IND + "std::shared_ptr< hier::PatchLevel > coarser_level(new_hierarchy->getPatchLevel(ln-1));" + NL
                + IND + IND + IND + "d_coarsen_schedule[ln] = d_coarsen_algorithm->createSchedule(coarser_level, level, NULL);" + NL
                + IND + IND + "}" + NL
                + IND + "}" + NL
                + "#Variable registrations#" + NL
                + variableRegistrations
                + "#Coarsen declarations#"
                + IND + "std::shared_ptr< xfer::CoarsenAlgorithm > d_coarsen_algorithm;" + NL
               	+ IND + "std::vector< std::shared_ptr< xfer::CoarsenSchedule > > d_coarsen_schedule;" + NL
                + "#Coarsen registrations#"
                + coarsenRegistrations;
    }
    
    /**
     * Creates the synchronization information check.
     * @param pi                The problem information
     * @return                  The file
     * @throws CGException      CG004 External error
     */
    private String synchronizationABMSimPlat(ProblemInfo pi) throws CGException {
        if (pi.isHasABMMeshFields() && pi.getEvolutionStep().equals("AllAgents")) {
            Document doc = pi.getProblem();
            String problemHVariables = "";
            String problemVariables = "";
            String resetHierarchyConfiguration1 = "";
            String resetHierarchyConfiguration2 = "";
            String variableRegistrations = "";
            try {
                //Control when the synchronizations has to be launched
                boolean sync = false;
                ArrayList<String> syncVars = new ArrayList<String>();
                NodeList elements = CodeGeneratorUtils.find(doc, "//mms:executionOrder/mms:element");
                Element previousElement = null;
                //Initialization of the variables for iteration
                for (int i = 0; i < elements.getLength(); i++) {
                    Element element = (Element) elements.item(i);
                    String elementName = element.getTextContent();
                    String elementType = CodeGeneratorUtils.find(doc, "//*[(local-name() = 'update' or local-name() = 'gather') and descendant::" 
                            + "mms:name = '" + elementName + "']").item(0).getLocalName();
                    boolean unifiable = false;
                    if (previousElement != null) {
                        unifiable = CodeGeneratorUtils.checkUnifiable(null, element, previousElement, pi.getVariables(), 
                                pi.getDimensions());
                    }
                    //patch loop and synchronizations
                    if (sync && !unifiable) {
                        problemHVariables = problemHVariables + IND + "std::shared_ptr< xfer::RefineAlgorithm > d_bdry_fill_advance" 
                            + i + ";" + NL
                            + IND + "std::vector< std::shared_ptr< xfer::RefineSchedule > > d_bdry_sched_advance" + i + ";" + NL;
                        problemVariables = problemVariables + IND + "d_bdry_fill_advance" + i + " = std::shared_ptr< " 
                            + "xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());" + NL;
                        resetHierarchyConfiguration1 = resetHierarchyConfiguration1 + IND + "d_bdry_sched_advance" + i 
                            + ".resize(finest_hiera_level+1);" + NL;
                        resetHierarchyConfiguration2 = resetHierarchyConfiguration2 + IND + IND + "d_bdry_sched_advance" + i + "[ln] = " 
                            + "d_bdry_fill_advance" + i + "->createSchedule(level,ln-1,new_hierarchy,this);" + NL;
                        for (int j = 0; j < syncVars.size(); j++) {
                            String var = syncVars.get(j);
                            variableRegistrations = variableRegistrations + IND + "d_bdry_fill_advance" + i + "->registerRefine(d_" + var 
                                + "_id,d_" + var + "_id,d_" + var + "_id,refine_operator);" + NL;
                        }
                        sync = false;
                        syncVars.clear();
                    }
                    
                    //If the actual block has stencil > 0 then the next block has to synchronize
                    DocumentFragment blockScope = doc.createDocumentFragment();
                    Element blockList = (Element) CodeGeneratorUtils.find(doc, "//*[(local-name() = 'update' or local-name() = 'gather')" 
                            + " and descendant::mms:name = '" + elementName + "']").item(0).cloneNode(true);
                    blockScope.appendChild(blockList);
                    if (elementType.equals("gather")) {
                        sync = true;
                        syncVars.addAll(SAMRAIUtils.getSyncVariables(blockScope, pi.getVariables(), pi.getDimensions(), pi.getExtrapolationType()));
                    }
                    previousElement = (Element) element.cloneNode(true);
                    
                }
                //Call the synchronization if still pending
                if (sync) {
                    problemHVariables = problemHVariables + IND + "std::shared_ptr< xfer::RefineAlgorithm > d_bdry_fill_advance" 
                        + elements.getLength() + ";" 
                        + NL
                        + IND + "std::vector< std::shared_ptr< xfer::RefineSchedule > > d_bdry_sched_advance" + elements.getLength() 
                        + ";" + NL;
                }
                
                NodeList properties = CodeGeneratorUtils.find(doc, "/*/mms:agentProperties/mms:agentProperty");
                for (int i = 0; i < properties.getLength(); i++) {
                    String property = SAMRAIUtils.variableNormalize(properties.item(i).getTextContent());
                    variableRegistrations = variableRegistrations + IND + "d_bdry_fill_init->registerRefine(d_" + property + "_id,d_" + property 
                        + "_id,d_" + property + "_id,refine_operator);" + NL;
                }
                
                return "#Problem.h variables#" + NL
                    + IND + "std::shared_ptr< xfer::RefineAlgorithm > d_bdry_fill_init;" + NL
                    + problemHVariables
                    + "#Problem variables#" + NL
                    + IND + "d_bdry_fill_init = std::shared_ptr< xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());" + NL
                    + problemVariables
                    + IND + "d_coarsen_algorithm = std::shared_ptr< xfer::CoarsenAlgorithm >(new xfer::CoarsenAlgorithm(d_dim));" + NL
                    + "#resetHierarchyConfiguration#" + NL
                    + resetHierarchyConfiguration1
                    + IND + "d_coarsen_schedule.resize(finest_hiera_level+1);" + NL
                    + IND + "//  Build coarsen and refine communication schedules." + NL
                    + IND + "for (int ln = coarsest_level; ln <= finest_hiera_level; ln++) {" + NL
                    + IND + IND + "std::shared_ptr< hier::PatchLevel > level(new_hierarchy->getPatchLevel(ln));" + NL
                    + resetHierarchyConfiguration2
                    + IND + IND + "// coarsen schedule only for levels > 0" + NL
                    + IND + IND + "if (ln > 0) {" + NL
                    + IND + IND + IND + "std::shared_ptr< hier::PatchLevel > coarser_level("
                    + "new_hierarchy->getPatchLevel(ln-1));" + NL
                    + IND + IND + IND + "d_coarsen_schedule[ln] = d_coarsen_algorithm->createSchedule(coarser_level, level, NULL);" + NL
                    + IND + IND + "}" + NL
                    + IND + "}" + NL
                    + "#Variable registrations#" + NL
                    + variableRegistrations
                    + "#Coarsen declarations#"
                    + IND + "std::shared_ptr< xfer::CoarsenAlgorithm > d_coarsen_algorithm;" + NL
                   	+ IND + "std::vector< std::shared_ptr< xfer::CoarsenSchedule > > d_coarsen_schedule;" + NL
                    + "#Coarsen registrations#";
            }
            catch (CGException e) {
                throw e;
            }
        }
        if (pi.isHasABMMeshFields() && pi.getEvolutionStep().equals("SingleAgent")) {
            Document doc = pi.getProblem();
            String variableRegistrations = "";
            NodeList properties = CodeGeneratorUtils.find(doc, "/*/mms:agentProperties/mms:agentProperty");
            for (int i = 0; i < properties.getLength(); i++) {
                String property = SAMRAIUtils.variableNormalize(properties.item(i).getTextContent());
                variableRegistrations = variableRegistrations + IND + "d_bdry_fill_init->registerRefine(d_" + property + "_id,d_" + property 
                    + "_id,d_" + property + "_id,refine_operator);" + NL;
                variableRegistrations = variableRegistrations + IND + "d_bdry_fill_advance0->registerRefine(d_" + property + "_id,d_" + property 
                        + "_id,d_" + property + "_id,refine_operator);" + NL;
            }
            return "#Problem.h variables#" + NL
                    + IND + "std::shared_ptr< xfer::RefineAlgorithm > d_bdry_fill_init;" + NL
                    + IND + "std::shared_ptr< xfer::RefineAlgorithm > d_bdry_fill_advance0;" + NL
                    + IND + "std::vector< std::shared_ptr< xfer::RefineSchedule > > d_bdry_sched_advance0;" + NL
                    + "#Problem variables#" + NL
                    + IND + "d_bdry_fill_init = std::shared_ptr< xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());" + NL
                    + IND + "d_bdry_fill_advance0 = std::shared_ptr< xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());" + NL
                    + IND + "d_coarsen_algorithm = std::shared_ptr< xfer::CoarsenAlgorithm >(new xfer::CoarsenAlgorithm(d_dim));" + NL
                    + "#resetHierarchyConfiguration#" + NL
                    + IND + "d_bdry_sched_advance0.resize(finest_hiera_level+1);" + NL
                    + IND + "d_coarsen_schedule.resize(finest_hiera_level+1);" + NL
                    + IND + "//  Build coarsen and refine communication schedules." + NL
                    + IND + "for (int ln = coarsest_level; ln <= finest_hiera_level; ln++) {" + NL
                    + IND + IND + "std::shared_ptr< hier::PatchLevel > level(new_hierarchy->getPatchLevel(ln));" + NL
                    + IND + IND + "d_bdry_sched_advance0[ln] = d_bdry_fill_advance0->createSchedule(level,ln-1,new_hierarchy,this);" + NL
                    + IND + IND + "// coarsen schedule only for levels > 0" + NL
                    + IND + IND + "if (ln > 0) {" + NL
                    + IND + IND + IND + "std::shared_ptr< hier::PatchLevel > coarser_level("
                    + "new_hierarchy->getPatchLevel(ln-1));" + NL
                    + IND + IND + IND + "d_coarsen_schedule[ln] = d_coarsen_algorithm->createSchedule(coarser_level, level, NULL);" + NL
                    + IND + IND + "}" + NL
                    + IND + "}" + NL
                    + "#Variable registrations#" + NL
                    + variableRegistrations
                    + "#Coarsen declarations#"
                    + IND + "std::shared_ptr< xfer::CoarsenAlgorithm > d_coarsen_algorithm;" + NL
                   	+ IND + "std::vector< std::shared_ptr< xfer::CoarsenSchedule > > d_coarsen_schedule;" + NL
                    + "#Coarsen registrations#";
        }
        //All the fields are inside the particle variable for Particles
        if (pi.isHasABMMeshlessFields()) {            
            return "#Problem.h variables#" + NL
                + IND + "std::shared_ptr< xfer::RefineAlgorithm > d_bdry_fill_init;" + NL
                + IND + "std::shared_ptr< xfer::RefineAlgorithm > d_bdry_fill_advance;" + NL
                + IND + "std::vector< std::shared_ptr< xfer::RefineSchedule > > d_bdry_sched_advance;" + NL
                + "#Problem variables#" + NL
                + IND + "d_bdry_fill_init = std::shared_ptr< xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());" + NL
                + IND + "d_bdry_fill_advance = std::shared_ptr< xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());" + NL
                + IND + "d_coarsen_algorithm = std::shared_ptr< xfer::CoarsenAlgorithm >(new xfer::CoarsenAlgorithm(d_dim));" + NL
                + "#resetHierarchyConfiguration#" + NL
                + IND + "d_bdry_sched_advance.resize(finest_hiera_level+1);" + NL
                + IND + "d_coarsen_schedule.resize(finest_hiera_level+1);" + NL
                + IND + "//  Build coarsen and refine communication schedules." + NL
                + IND + "for (int ln = coarsest_level; ln <= finest_hiera_level; ln++) {" + NL
                + IND + IND + "std::shared_ptr< hier::PatchLevel > level(new_hierarchy->getPatchLevel(ln));" + NL
                + IND + IND + "d_bdry_sched_advance[ln] = d_bdry_fill_advance->createSchedule(level,ln-1,new_hierarchy,this);" + NL
                + IND + IND + "// coarsen schedule only for levels > 0" + NL
                + IND + IND + "if (ln > 0) {" + NL
                + IND + IND + IND + "std::shared_ptr< hier::PatchLevel > coarser_level("
                + "new_hierarchy->getPatchLevel(ln-1));" + NL
                + IND + IND + IND + "d_coarsen_schedule[ln] = d_coarsen_algorithm->createSchedule(coarser_level, level, NULL);" + NL
                + IND + IND + "}" + NL
                + IND + "}" + NL
                + "#Variable registrations#" + NL
                + IND + "d_bdry_fill_init->registerRefine(d_particleVariables_id,d_particleVariables_id,d_particleVariables_id,refine_particles);" + NL
                + IND + "d_bdry_fill_advance->registerRefine(d_particleVariables_id,d_particleVariables_id,d_particleVariables_id,refine_particles);" + NL
                + "#Coarsen declarations#"
                + IND + "std::shared_ptr< xfer::CoarsenAlgorithm > d_coarsen_algorithm;" + NL
               	+ IND + "std::vector< std::shared_ptr< xfer::CoarsenSchedule > > d_coarsen_schedule;" + NL
                + "#Coarsen registrations#";
        }
        throw new CGException(CGException.CG003, "Problem type not found");
    }
    
    /**
     * Create an example of the input required in the simPlat + SAMRAI platform.
     * @param pi                    The problem information
     * @return                      The file
     * @throws CGException          CG00X External error
     */
    private String problemInput(ProblemInfo pi) throws CGException {
        Document doc = pi.getProblem();
        try {
            //Domain limits
            String domainBoxLow = "(";
            String domainBoxUp = "(";
            String coarserRatio = "";
            String largestPatch = "";
            String smallestPatch = "";
            for (int i = 0; i < pi.getSpatialDimensions(); i++) {
                domainBoxLow = domainBoxLow + "0, ";
                domainBoxUp = domainBoxUp + "99, ";
                coarserRatio = coarserRatio + "2, ";
                largestPatch = largestPatch + "1000, ";
                smallestPatch = smallestPatch + "10, ";
            }
            domainBoxLow = domainBoxLow.substring(0, domainBoxLow.lastIndexOf(",")) + ")";
            domainBoxUp = domainBoxUp.substring(0, domainBoxUp.lastIndexOf(",")) + ")";
            coarserRatio = coarserRatio.substring(0, coarserRatio.lastIndexOf(","));
            largestPatch = largestPatch.substring(0, largestPatch.lastIndexOf(","));
            smallestPatch = smallestPatch.substring(0, smallestPatch.lastIndexOf(","));
            String periodicBounds = "";
            for (int i = 0; i < pi.getCoordinates().size(); i++) {
                if (pi.getPeriodicalBoundary().get(pi.getContCoordinates().get(i)).equals("periodical")) {
                    periodicBounds = periodicBounds + "1, ";
                }
                else {
                    periodicBounds = periodicBounds + "0, ";
                }
            }
            periodicBounds = periodicBounds.substring(0, periodicBounds.lastIndexOf(","));
            //Dump configuration
            String meshVariables = "";
            for (String field : pi.getVariableInfo().getMeshFields().keySet()) {
              	meshVariables = meshVariables + ", \"" + SAMRAIUtils.variableNormalize(field) + "\"";
            }
            if (!meshVariables.equals("")) {
            	meshVariables = meshVariables.substring(2);	
            }
            String particleVariables = "";
            for (ArrayList<String> fields : pi.getVariableInfo().getParticleFields().values()) {
            	  for (String field : fields) {
            		  particleVariables = particleVariables + ", \"" + SAMRAIUtils.variableNormalize(field) + "\"";
            	  }
            }
            for (String speciesName: pi.getParticleSpecies()) {
            	particleVariables = particleVariables + ", \"id_" + speciesName + "\", \"procId_" + speciesName + "\"";	
            }
            if (!particleVariables.equals("")) {
            	particleVariables = particleVariables.substring(2);	
            }
            String analysisFields = "";
            if (pi.getAnalysisFields().size() > 0) {
                for (int i = 0; i < pi.getAnalysisFields().size(); i++) {
                    analysisFields = analysisFields + ", \"" + pi.getAnalysisFields().get(i) + "\"";
                }
            }
            String full_mesh = "";
            String full_particles = "";
            if (pi.isHasABMMeshFields() || pi.isHasMeshFields()) {
            	full_mesh = IND + "full_dump_mesh {" + NL
                        + IND + IND + "hdf5_dump_interval = 1         //In finest level cycle units" + NL
                        + IND + IND + "hdf5_dump_dirname = \"outputDir_mesh\"" + NL
                        + IND + IND + "variables = " + meshVariables + analysisFields + "      // variables to dump" + NL
                        + IND + "}" + NL;
            }
            if (pi.isHasABMMeshlessFields() || pi.isHasParticleFields()) {
            	full_particles = IND + "full_dump_particles {" + NL
                        + IND + IND + "hdf5_dump_interval = 1         //In finest level cycle units" + NL
                        + IND + IND + "hdf5_dump_dirname = \"outputDir_particles\"" + NL
                        + IND + IND + "variables = " + particleVariables + "      // variables to dump" + NL
                        + IND + "}" + NL;
            }
            
            //Region
            String regionLow = "";
            String regionUp = "";
            String regionRefineLow = "";
            String regionRefineUp = "";
            String[][] xslParams = new String [THREE][2];
            xslParams[0][0] =  "condition";
            xslParams[0][1] =  "true";
            xslParams[1][0] =  "indent";
            xslParams[1][1] =  "0";
            xslParams[2][0] =  "dimensions";
            xslParams[2][1] =  String.valueOf(pi.getDimensions());
            for (int i = 0; i < pi.getCoordinates().size(); i++) {
                //Domain
                NodeList coordLimits = CodeGeneratorUtils.find(doc, "//mms:region//mms:coordinateLimits" 
                        + "[descendant::mms:coordinate ='" + pi.getCoordinates().get(i) + "']|//mms:mesh//mms:coordinateLimits" 
                        + "[descendant::mms:coordinate ='" + pi.getCoordinates().get(i) + "']");
                Element domainCoord = (Element) coordLimits.item(0);
                Element coordinateMin = (Element) domainCoord.getFirstChild().getNextSibling().getFirstChild().getFirstChild();
                regionLow = regionLow + SAMRAIUtils.xmlTransform(coordinateMin, xslParams) + ", ";
                try {
                	double low = Double.parseDouble(SAMRAIUtils.xmlTransform(coordinateMin, xslParams).replaceAll("M_PI", "3.1415926535897932384626433832795028841971693993751"));
                    regionRefineLow = regionRefineLow + (low / 2) + ", ";
                }
                catch (Exception e) {
                    regionRefineLow = regionRefineLow + "(" + SAMRAIUtils.xmlTransform(coordinateMin, xslParams) + ")/2, ";
                }
                Element coordinateMax = (Element) domainCoord.getFirstChild().getNextSibling().getNextSibling().getFirstChild().getFirstChild();
                regionUp = regionUp + SAMRAIUtils.xmlTransform(coordinateMax, xslParams) + ", ";
                try {
                	double up = Double.parseDouble(SAMRAIUtils.xmlTransform(coordinateMax, xslParams).replaceAll("M_PI", "3.1415926535897932384626433832795028841971693993751"));
                	regionRefineUp = regionRefineUp + (up / 2) + ", ";
                }
                catch (Exception e) {
                	regionRefineUp = regionRefineUp + "(" + SAMRAIUtils.xmlTransform(coordinateMax, xslParams) + ")/2, ";
                }
            }
            regionLow = regionLow.substring(0, regionLow.lastIndexOf(",")).replaceAll("M_PI", "3.1415926535897932384626433832795028841971693993751");
            regionUp = regionUp.substring(0, regionUp.lastIndexOf(",")).replaceAll("M_PI", "3.1415926535897932384626433832795028841971693993751");
            regionRefineLow = regionRefineLow.substring(0, regionRefineLow.lastIndexOf(",")).replaceAll("M_PI", "3.1415926535897932384626433832795028841971693993751");
            regionRefineUp = regionRefineUp.substring(0, regionRefineUp.lastIndexOf(",")).replaceAll("M_PI", "3.1415926535897932384626433832795028841971693993751");
            
            String input =  "//Problem specific parameters" + NL
                + "Problem {" + NL;
            String griddingParameters = "";
            String regridding = "";
            String amr = "";
            if (pi.isHasABMMeshlessFields()) {
                griddingParameters = "GriddingAlgorithm {" + NL
                        + "}" + NL + NL;
            }
            else {
                if (pi.isHasParticleFields()) {
                    input = input + particlesInputSPH(doc, pi); 
                }
                amr = IND + "// The following parameters sets level initialization and cell tagging routines. Two methods are implemented in Simflowny, REFINE_BOXES (FMR) and GRADIENT_DETECTOR (AMR)" + NL
                		+ IND + "// Tagging methods may be activated at specific cycles and/or times. It is possible to use combinations of these two methods (e.g., use gradient detection and static refine boxes at the same cycle/time). " + NL
                		+ IND + "// The order in which they are executed is fixed (gradient detection first, and refine boxes second)." + NL
                		+ IND + "// The general input syntax is as follows:" + NL
                		+ IND + "//at_0 input section describing a set of tagging methods to apply to a cycle or time" + NL
                		+ IND + IND + "//cycle = integer cycle at which the set of tagging methods is activated one of cycle or time must be supplied" + NL
                		+ IND + IND + "//time = double time at which the set of tagging methods is activated one of cycle or time must be supplied" + NL
                		+ IND + IND + "//tag_0 first tagging method in this set of tagging methods" + NL
                		+ IND + IND + IND + "//tagging_method = one of GRADIENT_DETECTOR, REFINE_BOXES, NONE" + NL
                		+ IND + IND + IND + "//level_m required if tagging_method is REFINE_BOXES, the static boxes for the mth level" + NL
                		+ IND + IND + IND + IND + "//box_0 required domain limits specifying refine domains" + NL
                		+ IND + IND + IND + IND + IND + "//x_lo lower end of refinement box" + NL
                		+ IND + IND + IND + IND + IND + "//x_up upper end of refinement box" + NL
                		+ IND + IND + IND + IND + "//. . ." + NL
                		+ IND + IND + IND + "//level_n" + NL
                		+ IND + IND + "//. . ." + NL
                		+ IND + IND + "//tag_n" + NL
                		+ IND + "//. . ." + NL
                		+ IND + "//at_n" + NL + NL
                		+ IND + "// Mixed AMR + FMR example" + NL
                        + IND + "/*at_0{" + NL 
                        + IND + IND + "// Activates this configuration from given cycle or time (Only one option allowed)" + NL
                        + IND + IND + "cycle=0" + NL
                        + IND + IND + "//time=0" + NL
                        + IND + IND + "tag_0{       //FMR" + NL
                        + IND + IND + IND + "tagging_method = \"REFINE_BOXES\"" + NL
                        + IND + IND + IND + "level_0 {" + NL
                        + IND + IND + IND + IND + "box_0 {" + NL
                        + IND + IND + IND + IND + IND + "x_lo = " + regionRefineLow + "     // lower end of refinement box." + NL
                        + IND + IND + IND + IND + IND + "x_up = " + regionRefineUp + "  // upper end of refinement box." + NL
                        + IND + IND + IND + IND + "}" + NL
                        + IND + IND + IND + "}" + NL
                        + IND + IND + IND + "level_1 {" + NL
                        + IND + IND + IND + IND + "box_0 {" + NL
                        + IND + IND + IND + IND + IND + "x_lo = " + regionRefineLow + "     // lower end of refinement box." + NL
                        + IND + IND + IND + IND + IND + "x_up = " + regionRefineUp + "  // upper end of refinement box." + NL
                        + IND + IND + IND + IND + "}" + NL
                        + IND + IND + IND + "}" + NL
                        + IND + IND + "}" + NL
                        + IND + IND + "tag_1{       //AMR" + NL
                        + IND + IND + IND + "// Configuration parameters of the AMR must be set in problem/regridding database above" + NL
                        + IND + IND + IND + "tagging_method = \"GRADIENT_DETECTOR\"" + NL
                        + IND + IND + "}" + NL
                        + IND + "}*/" + NL;
                String keyShadowField = pi.getFieldVariables().keySet().iterator().next();
                String fieldsShadow = "\"" + pi.getFieldVariables().get(keyShadowField).get(0) + "\" \"" + pi.getFieldVariables().get(keyShadowField).get(pi.getFieldVariables().get(keyShadowField).size() - 1) + "\"";
                regridding = IND + "//Regridding options" + NL
                    + IND + "regridding {" + NL
                    + IND + IND + "//There are currently three regridding strategies, Gradient, Function and Shadow" + NL
                    + IND + IND + "// Spatial interpolator" + NL
                    + IND + IND + "interpolator = \"QUINTIC_REFINE\"    //\"LINEAR_REFINE\", \"CUBIC_REFINE\" or \"QUINTIC_REFINE\"" + NL
                    + IND + IND + "//Common regridding parameters" + NL
                    + IND + IND + "regridding_buffer = 0	//Number of extra cells surrounding regridding criteria (this value cannot be greater than smallest_patch_size)" + NL
                    + IND + IND + "regridding_min_level = 1	//First level number to check regridding from" + NL
                    + IND + IND + "regridding_max_level = 100	//Last level number to check regridding from" + NL
                    + IND + IND + "// GRADIENT. A cell will be refined when the gradient of the field on any neighbour cell is greater than the next nearest cell gradient with a compression factor and an offset. " + NL
                    + IND + IND + "// The conditional formula is: ∇f(i) > C * MIN(∇f(i+1), ∇f(i-1)) + O * dx^2." + NL
                    + IND + IND + "//regridding_type = \"GRADIENT\"" + NL
                    + IND + IND + "//regridding_field = \"" + pi.getVariables().get(0) + "\"	// is the field in which to check gradient" + NL
                    + IND + IND + "//regridding_compressionFactor = 3	// is the compression factor for the tagging algorithm (*C* in the formula)" + NL
                    + IND + IND + "//regridding_mOffset = 50			// is the offset for the tagging algorithm (*O* in the formula)" + NL
                    + IND + IND + "// FUNCTION. The cell is refined if the provided field is greater than a given threshold." + NL
                    + IND + IND + "regridding_type = \"FUNCTION\"" + NL
                    + IND + IND + "regridding_function_field = \"" + pi.getVariables().get(0) + "\"		// is the field in which to check the threshold." + NL
                    + IND + IND + "regridding_threshold = 0.5			//is the threshold for the field" + NL
                    + IND + IND + "// SHADOW. Two same field at different or equivalent times are compared, if the relative difference is greater than a given error, the cell is to be refined."
                    + IND + IND + "//regridding_type = \"SHADOW\"" + NL
                    + IND + IND + "//regridding_fields = " + fieldsShadow + "		// the two fields to compare" + NL
                    + IND + IND + "//regridding_error = 1e-5			// is the relative error threshold" + NL
                    + IND + "}" + NL;
                griddingParameters = "PatchHierarchy {" + NL
                	+ IND + "// Maximum number of levels in hierarchy (coarse + refinement)." + NL
                    + IND + "max_levels = 1" + NL
                    + IND + "// Vector with the resolution ratio to next coarser level" + NL
                    + IND + "ratio_to_coarser {" + NL
                    + IND + IND + "level_1 = " + coarserRatio + NL
                    + IND + "}" + NL + NL
                    + IND + "// Largest patch allowed in hierarchy" + NL
                    + IND + "largest_patch_size {" + NL
                    + IND + IND + "level_0 = " + largestPatch + NL
                    + IND + "}" + NL
                    + IND + "// Smallest patch allowed in hierarchy" + NL
                    + IND + "smallest_patch_size {" + NL
                    + IND + IND + "level_0 = " + smallestPatch + NL
                    + IND + "}" + NL
                    + IND + "// Minimum de number of cells between levels" + NL
                    + IND + "proper_nesting_buffer = 1" + NL
                    + "}" + NL + NL
                    + "GriddingAlgorithm {" + NL
                    + "}" + NL + NL;
            }
            String slicerConfiguration = "";
            String integrationConfiguration = "";
            String pointConfiguration = "";
            String varsAndAnalysis = meshVariables;
            if (!analysisFields.equals("")) {
                varsAndAnalysis = varsAndAnalysis + analysisFields;
            }
            String activateAnalysis = "";
            if (pi.getAnalysisFields().size() > 0) {
                activateAnalysis = IND + IND + "activate_analysis = TRUE       //TRUE of FALSE to activate analysis variables" + NL;
            }
            if (pi.getDimensions() == 3 && pi.isHasMeshFields()) {
                slicerConfiguration = IND + "// Example of 2D output configuration" + NL
                            + IND + "/*slice_0 {" + NL
                            + IND + IND + "plane_normal_axis = 1            //Value must be 1, 2 or 3" + NL
                            + IND + IND + "distance_to_origin = 0          //In domain units" + NL
                            + IND + IND + "variables = " + varsAndAnalysis + NL
                            + IND + IND + "hdf5_dump_interval = 1 	 	//In finest level cycle units" + NL
                            + IND + IND + "hdf5_dump_dirname = \"outputDir_slice0\"" + NL
                            + activateAnalysis
                            + IND + "}*/" + NL
                            + IND + "// Example of Spherical output configuration" + NL
                            + IND + "/*sphere_0 {" + NL
                            + IND + IND + "center = 0, 0, 0            //In domain units" + NL
                            + IND + IND + "radius = 1          //In domain units" + NL
                            + IND + IND + "resolution = 160, 80     // number of points for theta[0,2pi], phi[0,pi]" + NL
                            + IND + IND + "variables = " + varsAndAnalysis + NL
                            + IND + IND + "hdf5_dump_interval = 1		//In finest level cycle units" + NL
                            + IND + IND + "hdf5_dump_dirname = \"outputDir_sphere0\"" + NL
                            + activateAnalysis
                            + IND + "}*/" + NL;
            }
            if (pi.isHasMeshFields()) {
                integrationConfiguration = IND + "/*integration_0 {" + NL
                		+ IND + IND + "variables = " + varsAndAnalysis + "" + NL
                		+ IND + IND + "calculation = \"INTEGRAL\", \"L2NORM\", \"MIN\", \"MAX\", \"ABSMIN\", \"ABSMAX\"" + NL
                		+ IND + IND + "ascii_dump_interval = 1		//In finest level cycle units" + NL
                		+ IND + IND + "ascii_dump_dirname = \"outputDir_integral\"" + NL
                		+ activateAnalysis
            			+ IND + "}*/" + NL;
                pointConfiguration = IND + "/*point_0 {" + NL
                		+ IND + IND + "variables = " + varsAndAnalysis + "" + NL
                		+ IND + IND + "coordinates = 0, 0, 0            //In domain units" + NL
                		+ IND + IND + "ascii_dump_interval = 1		//In finest level cycle units" + NL
                		+ IND + IND + "ascii_dump_dirname = \"outputDir_point\"" + NL
                		+ activateAnalysis
            			+ IND + "}*/" + NL;
            }
            String segMovement = "";
           /* if (pi.getMovementInfo().itMoves()) {
                segMovement = INDENT + "corridor_width = 3" + NEWLINE;
            }*/
            String subycling = IND + "subcycling = \"BERGER-OLIGER\" // \"BERGER-OLIGER\", \"DISABLED\"" + NL;
        	String timeRefinementIntegratorParams = IND + "// Number of timesteps between each regrid of the hierarchy (in coarsest level units)" + NL
        			+ IND + "regrid_interval = 1" + NL
        			+ IND + "// Array of integer values (one for each level that may be refined) representing the number of cells by which tagged cells are buffered before clustering into boxes." + NL
         			+ IND + "tag_buffer = 0, 0, 0" + NL
        			+ IND + "// If simulation restart from a checkpoint, the following parameter indicates if TimeRefinementIntegrator database parameters override checkpointed values." + NL
        			+ IND + "read_on_restart = TRUE" + NL;
            String timeIntegrator = "TimeRefinementIntegrator{" + NL
        			+ timeRefinementIntegratorParams
        			+ "}" + NL + NL;
            return input + getParametersInput(IND, doc)
                + IND + "random_seed = 0" + NL
                + subycling
                + regridding
                + IND + "/*" + NL
                + IND + " * External EOS parameters" + NL
                + IND + " * ReprimAnd EO" + NL
                + IND + " * External library based on https://arxiv.org/abs/2005.01821" + NL
                + IND + " * Requires installation of the library at https://zenodo.org/record/4075317#.X9nVJ8J7nJk" + NL
                + IND + " */" + NL
                + IND + "external_EOS {" + NL
                + IND + IND + "// 0:Ideal Gas, 1:Hybrid, 2:Tabular" + NL
                + IND + IND + "eos_type = 2" + NL
                + IND + IND + "atmo_rho = 1e-12" + NL
                + IND + IND + "atmo_Ye = 0.5" + NL
                + IND + IND + "rho_strict = 5.0e-5" + NL
                + IND + IND + "c2p_acc = 1e-8" + NL 
                + IND + IND + "max_b = 10.0" + NL
                + IND + IND + "max_z = 1000.0" + NL
                + IND + IND + "//Parameters for Ideal Gas and Hybrid EOS" + NL
                + IND + IND + "max_rho = 1e6" + NL
                + IND + IND + "max_eps = 11.0" + NL
                + IND + IND + "gamma_th = 1.75" + NL
                + IND + "}" + NL                     
                + segMovement
                + "}" + NL + NL
                + "Main {" + NL
                + IND + "// Screen ouput information interval (In coarser level cycle units). It includes the information of all levels." + NL
                + IND + "output_interval = 1            	  // zero to turn off" + NL
                + IND + "// Screen information interval of execution time (In coarser level cycle units)" + NL
                + IND + "timer_output_interval = 1            // zero to turn off" + NL
                + IND + "// Restart configuration" + NL
                + IND + "// The following parameter set if the simulation starts from a checkpoint or from scratch" + NL
                + IND + "start_from_restart = FALSE" + NL
                + IND + "// Interval to save checkpoints for restarting (In coarser level cycle units)" + NL
                + IND + "restart_interval = 0" + NL
                + IND + "// Iteration to restart from (if start_from_restart = TRUE) (In coarser level cycle units)" + NL
                + IND + "restart_iteration = 0" + NL
                + IND + "// Directory to store the checkpoints and to take the checkpoint for a restart" + NL
                + IND + "restart_dirname = \"checkpoint.restart\""  + NL
                + IND + "// To force a processor rebalance" + NL
                + IND + "rebalance_processors = FALSE" + NL                
                + IND + "// dt. It should be calculated as < CFL / Vmax * (x_hi - x_lo) / Nx. " + NL
                + IND + "// Being CFL a factor than depends on the time integration method. In 3D, for Runge-Kutta 3 is 0.25, for Runge-Kutta 4 is 0.4." + NL
                + IND + "// Vmax is the maximum characteristic speed." + NL
                + IND + "// Nx is the number of cells in an axis." + NL
                + IND + "dt = " + calculateDt(regionLow, regionUp) + NL
                + IND + "// Clustering type (See details below)" + NL
                + IND + "clustering_type = \"BergerRigoutsos\" // TileClustering or BergerRigoutsos" + NL
                + IND + "// Load Balancer (See details below)" + NL
                + IND + "partitioner_type = \"CascadePartitioner\" // CascadePartitioner, TreeLoadBalancer or ChopAndPackLoadBalancer" + NL
                + "}" + NL + NL
                + "FileWriter {" + NL
                + full_mesh
                + full_particles
                + slicerConfiguration
                + integrationConfiguration
                + pointConfiguration
                + "}" + NL + NL
                + timeIntegrator
                + "CartesianGeometry {" + NL
                + IND + "// Boxes representing the index space for the entire domain on the coarsest mesh level" + NL
                + IND + "domain_boxes = [ " + domainBoxLow + " , " + domainBoxUp + " ]" + NL
                + IND + "// Values representing the spatial coordinates of the lower corner of the physical domain." + NL
                + IND + "x_lo = " + regionLow + NL
                + IND + "// Values representing the spatial coordinates of the upper corner of the physical domain." + NL
                + IND + "x_up = " + regionUp + NL
                + IND + "// Parameter given by Simflowny. Do not modify" + NL
                + IND + "periodic_dimension = " + periodicBounds + NL
                + "}" + NL + NL
                + "StandardTagAndInitialize{" + NL
                + amr
                + "}" + NL + NL
                + griddingParameters
                + "ChopAndPackLoadBalancer {" + NL
                + IND + "// Load balancing routines for AMR hierarchy levels based on either uniform or non-uniform workload estimate" + NL
                + IND + "// String value indicating the type of bin packing to use to map patches to processors. " + NL
                + IND + "// Currently, two options are supported: \"GREEDY\" and \"SPATIAL\". The \"GREEDY\" method simply maps each patch (box) to the first processor (bin), in ascending tbox::MPI process number, whose difference between the average workload and its current workload is less than the workload of the patch in question. " + NL
                + IND + "// The \"SPATIAL\" method first constructs an ordering of the patches (boxes) by passing a Morton-type curve through the center of each box. Then, it attempts to map the patches to processors by assigning patches that are near each other on the curve to the same processor. " + NL
                + IND + "//bin_pack_method = \"SPATIAL\"	//SPATIAL or GREEDY" + NL
                + IND + "// Boolean flag to control chopping of level boxes when the union of the input boxes for the load balancing routine is a single box. The default value is false, which means that the domain will be chopped to make patch boxes based on the (single box) union of the boxes describing the level regardless of the input boxes. "
                + IND + "// When the value is set to true, the domain will be chopped by chopping each of the input boxes." + NL
                + IND + "//ignore_level_box_union_is_single_box = FALSE" + NL
        		+ "}" + NL + NL
        		+ "CascadePartitioner {" + NL
        		+ IND + "// Cascade partitioning algorithm. The algorithm is described in the article \"Advances in Patch-Based Adaptive Mesh Refinement Scalability\"" + NL
        		+ IND + "// Tile size when using tile mode. Tile mode restricts box cuts to tile boundaries. Default is 1, which is equivalent to no restriction." + NL
        		+ IND + "//tile_size = 1,1,1" + NL
        		+ IND + "// Fraction of ideal load a process can take on in order to reduce box cutting and load movement. Higher values often reduce partitioning time and box count but produce less balanced work loads. " + NL
        		+ IND + "// Surplus work greater than this tolerance can still result due to other constraints, such as minimum box size."
        		+ IND + "//flexible_load_tolerance = 0.05" + NL
        		+ IND + "// This parameter limits how many processes may receive the load of one process in a load distribution cycle. If a process has too much initial load, this limit causes the load to distribute the load over multiple cycles. " + NL
        		+ IND + "// It alleviates the bottle-neck of one process having to work with too many other processes in any cycle." + NL
        		+ IND + "//max_spread_procs = 500" + NL
        		+ "}" + NL + NL
        		+ "TreeLoadBalancer {" + NL
        		+ IND + "// Tree-based load balancer." + NL
        		+ IND + "// Tile size when using tile mode. Tile mode restricts box cuts to tile boundaries. Default is 1, which is equivalent to no restriction" + NL
        		+ IND + "//tile_size = 1,1,1" + NL
        		+ IND + "// Fraction of ideal load a process can take on in order to reduce box cutting and load movement. Higher values often reduce partitioning time and box count but produce less balanced work loads. " + NL
        		+ IND + "// Surplus work greater than this tolerance can still result due to other constraints, such as minimum box size." + NL
        		+ IND + "//flexible_load_tolerance = 0.05" + NL
        		+ IND + "// This parameter limits how many processes may receive the load of one process in a load distribution cycle. If a process has too much initial load, this limit causes the load to distribute the load over multiple cycles. " + NL
        		+ IND + "// It alleviates the bottle-neck of one process having to work with too many other processes in any cycle." + NL
        		+ IND + "//max_cycle_spread_procs = 500" + NL
        		+ "}" + NL + NL
        		+ "TileClustering {" + NL
        		+ IND + "// Tiled patch clustering algorithm" + NL
        		+ IND + "// Tile size in the index space of the tag level" + NL
        		+ IND + "//tile_size = 8,8,8" + NL
        		+ "}" + NL + NL
        		+ "BergerRigoutsos {" + NL
        		+ IND + "// Asynchronous Berger-Rigoutsos implementation of clustering strategy" + NL
        		+ IND + "// Whether to sort the output. This makes the normally non-deterministic ordering deterministic and the results repeatable." + NL
        		+ IND + "//sort_output_nodes = FALSE" + NL
        		+ IND + "// The maximum cluster size allowed. This parameter is not critical to clustering but limiting the cluster size may improve performance of load balancing algorithms" + NL
        		+ IND + "//max_box_size = 100, 100, 100" + NL
        		+ IND + "// Threshold for the ratio of the total number of cells in two boxes into which a box may be split and the number of cells in the original box. " + NL
        		+ IND + "// If that ratio is greater than the combine efficiency, the box will not be split. " + NL
        		+ IND + "// This tolerance helps users avoids splitting up portions of the domain into into very small patches which can increase the overhead of AMR operations. Multiple values in the array are handled similar to efficiency_tolerance." + NL
        		+ IND + "//combine_efficiency = 0.8" + NL
        		+ IND + "// Minimum fraction of tagged cells to total cells in boxes used to construct patches on a new level. If the ratio is below the tolerance value, the box may be split into smaller boxes and pieces removed until the ratio becomes greater than or equal to the tolerance. " + NL
        		+ IND + "// This tolerance helps users control the amount of extra refined cells created (beyond those tagged explicitly) that is typical in patch-based AMR computations. The index of the value in the array corresponds to the number of the level to which the tolerance value applies. " + NL
        		+ IND + "// If more values are given than the maximum number of levels allowed in the hierarchy, extra values will be ignored. If fewer values are given, then the last value given will be used for each level without a specified input value. For example, if only a single value is specified, then that value will be used on all levels." + NL
        		+ IND + "//efficiency_tolerance = 0.8" + NL
        		+ "}" + NL + NL
                + "TimerManager{" + NL
        		+ IND + "// This database includes some time printing for profiling purposes. It details the time the simulation spends writing to disk, regridding and performing the calculations." + NL
        		+ IND + "// List of the active timers. The user can remove any not desired." + NL
                + IND + "timer_list = \"OutputGeneration\", \"Regridding\", \"Step\"" + NL
                + IND + "// Timers that use up less than print_threshold percent of the overall run time are not printed." + NL
                + IND + "print_threshold = 0" + NL
                + IND + "// Print times measured on individual processors." + NL
                + IND + "print_processor = FALSE" + NL
                + IND + "// Print maximum time spent on any processor, and the processor ID that incurred the time" + NL
            	+ IND + "print_max = TRUE" + NL
                + "}";
        } 
        catch (Exception e) {
            throw new CGException(CGException.CG00X, e.getMessage());
        }
    }
    
    /**
     * Calculates the delta t to fit Courant condition.
     * @param minCoords The coordinates minimum
     * @param maxCoords The coordinates maximum
     * @return          An appropriate Dt
     */
    private String calculateDt(String minCoords, String maxCoords) {
        String[] min = minCoords.split(", ");
        String[] max = maxCoords.split(", ");
        boolean valid = true;
        double minDx = Integer.MAX_VALUE;
        //Mathematical evaluation
        for (int i = 0; i < min.length && valid; i++) {
            try {
                double value = CodeGeneratorUtils.eval("((" + max[i] + " - (" + min[i] + "))/100)^2/10");
                minDx = Math.min(minDx, value);
            } 
            catch (Exception e) {
                valid = false;
            }
        }        
        //If the expression is not evaluable set a default Dt
        if (!valid) {
            return "0.0001";
        }
		return String.valueOf(minDx);
        
    }
    
    /**
     * Creates the parameters lines for the input example file.
     * @param ind           The indentation
     * @param doc           The problem
     * @return              The parameters
     * @throws CGException  CG003 Not possible to create code for the platform 
     *                       CG00X External error
     */
    private String getParametersInput(String ind, Document doc) throws CGException {
        String parameters = "";
        NodeList paramList = CodeGeneratorUtils.find(doc, "/*/mms:parameters/mms:parameter");
        for (int i = 0; i < paramList.getLength(); i++) {
            String name = SAMRAIUtils.variableNormalize(paramList.item(i).getFirstChild().getTextContent());
            String type = paramList.item(i).getFirstChild().getNextSibling().getTextContent();
            String defaultValue = paramList.item(i).getLastChild().getTextContent();
            parameters = parameters + ind + SAMRAIUtils.variableNormalize(name) + " = ";
            //No default value, so create a new one
            if (type.equals(defaultValue)) {
                if (type.equals("REAL")) {
                    parameters = parameters + "1.0" + NL;
                }
                if (type.equals("INT")) {
                    parameters = parameters + "1" + NL;
                }
                if (type.equals("STRING")) {
                    parameters = parameters + "\"\"" + NL;
                }
                if (type.equals("BOOLEAN")) {
                    parameters = parameters + "true" + NL;
                }
            }
            else {
                if (type.equals("REAL") && defaultValue.matches("[0-9]+")) {
                	defaultValue = defaultValue + ".0";
                }
                parameters = parameters + defaultValue + NL;
            }
        }
        return parameters;
    }
    
    /**
     * Add particles parameter database. 
     * @param doc           The problem document
     * @param coords        The spatial coordinates of the problem
     * @return              The code
     * @throws CGException  CG004 External error
     */
    private String particlesInputSPH(Document doc, ProblemInfo pi) throws CGException {
        try {
            String part1 = "";
        	String offset = "";
        	String boxLimitsMin = "";
        	String boxLimitsMax = "";
            for (int i = 0; i < pi.getCoordinates().size(); i++) {
                part1 = part1 + "400, ";
                offset = offset + "0.5, ";
                boxLimitsMin = boxLimitsMin + "0.0, ";
                boxLimitsMax = boxLimitsMax + "0.0, ";
            }
            part1 = part1.substring(0, part1.lastIndexOf(", "));
            offset = offset.substring(0, offset.lastIndexOf(", "));
            boxLimitsMin = boxLimitsMin.substring(0, boxLimitsMin.lastIndexOf(", "));
            boxLimitsMax = boxLimitsMax.substring(0, boxLimitsMax.lastIndexOf(", "));
            String speciesParameters = "";
            for (String speciesName: pi.getParticleSpecies()) {
            	speciesParameters = speciesParameters + IND + IND + speciesName + " {" + NL
            			+ IND + IND + IND + "//Number of particles by dimension" + NL
            			+ IND + IND + IND + "number_of_particles =" + part1 + NL
            			+ IND + IND + IND + "//Domain offset factor for regular, box or staggered distribution (In particle separation units [0, 1])" + NL
            			+ IND + IND + IND + "domain_offset_factor = " + offset + NL
            			+ IND + IND + IND + "//Distribution limits for box distribution" + NL
            			+ IND + IND + IND + "box_min = " + boxLimitsMin + NL
            			+ IND + IND + IND + "box_max = " + boxLimitsMax + NL
            			+ IND + IND + IND + "//Mean and std deviation for normal distribution (In domain units)" + NL
            			+ IND + IND + IND + "normal_mean = " + offset + NL
            			+ IND + IND + IND + "normal_stddev = " + offset + NL
                        + IND + IND + IND + "//Particle distribution" + NL 
                        + IND + IND + IND + "particle_distribution = \"REGULAR\"  //REGULAR, STAGGERED, BOX, NORMAL or RANDOM" + NL 
            			+ IND + IND + "}" + NL;
            }
            
            return IND + "particles {" + NL
                + speciesParameters
                + IND + IND + "//Print particles average in cells" + NL
                + IND + IND + "print_average = FALSE" + NL
                + IND + IND + "//Influence radius (in domain units, recommended value = 1.3 * particle separation)" + NL
                + IND + IND + "influenceRadius = 0.1" + NL
 
                + IND + "}" + NL;
        } 
        catch (Exception e) {
            throw new CGException(CGException.CG00X, e.getMessage());
        }
    }
}
