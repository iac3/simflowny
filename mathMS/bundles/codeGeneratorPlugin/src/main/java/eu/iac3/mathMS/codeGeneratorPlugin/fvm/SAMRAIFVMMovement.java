package eu.iac3.mathMS.codeGeneratorPlugin.fvm;

import eu.iac3.mathMS.codeGeneratorPlugin.CGException;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.CodeGeneratorUtils;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.ProblemInfo;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.SAMRAIUtils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Region movement for FVM code in SAMRAI.
 * @author bminyano
 *
 */
public final class SAMRAIFVMMovement {

    static final String NL = System.getProperty("line.separator");
    static final String IND = "\u0009";
    static final String MTURI = "http://www.w3.org/1998/Math/MathML";
    static final int INTERPHASELENGTH = 3;
    static final int THREE = 3;
    
    /**
     * Default constructor.
     */
    private SAMRAIFVMMovement() { };
    
    /**
     * Creates the movement function and declaration.
     * @param pi                    The problem information
     * @return                      The file
     * @throws CGException          CG004 External error
     */
    public static String create(ProblemInfo pi) throws CGException {
        
        return "#interphaseMovementFunction#" + NL
                + createFunction(pi) + NL
                + createGetCellPoints(pi)
                + createRestoreCellPoints(pi)
                + "#interphaseMovementDeclaration#" + NL
                + createDeclaration(pi) + NL
                + createGetCellPointsDeclaration(pi)
                + createRestoreCellPointsDeclaration(pi);
    }
    
    /**
     * Creates the function for the region movement.
     * @param pi            The problem information
     * @return              The code generated
     * @throws CGException  CG00X External error
     */
    private static String createFunction(ProblemInfo pi) throws CGException {
        String parameters = "";
        ArrayList<String> fields = CodeGeneratorUtils.getFields(pi.getProblem());
        for (int i = 0; i < fields.size(); i++) {
            parameters = parameters + "int " + SAMRAIUtils.variableNormalize(fields.get(i)) + "_id, ";
        }
        return "/*" + NL
            + " * Perform the region movement" + NL
            + " */" + NL
            + "void Problem::interphaseMovement(const double time, " + parameters + "double dt, int* remesh) {" + NL
            + IND + "const tbox::SAMRAI_MPI& mpi(tbox::SAMRAI_MPI::getSAMRAIWorld());" + NL
            + calculateTimes(pi, IND) + NL
            + fieldMappingToInterphase(pi) + NL
            + IND + "mpi.AllReduce(remesh, 1, MPI_MAX);" + NL
            + IND + "if (remesh[0] == 1) {" + NL
            + remeshing(pi, IND + IND) + NL
            + addRemoveParticleSupport(pi, IND + IND) + NL
            + IND + "}" + NL
            + IND + "for (int ln=0; ln<=d_patch_hierarchy->getFinestLevelNumber(); ++ln ) {" + NL
            + IND + IND + "std::shared_ptr<hier::PatchLevel > level(d_patch_hierarchy->getPatchLevel(ln));" + NL
            + IND + IND + "d_mapping_fill->createSchedule(level, level)->fillData(time, true);" + NL
            + IND + IND + "interphaseMapping(time, false, ln, level, remesh[0]);" + NL
            + IND + "}" + NL
            + "}";
    }
    
    /**
     * Creates the code to map the fields to the interphase class variables.
     * @param pi                The Problem information
     * @return                  The code
     * @throws CGException      CG00X External error
     */
    private static String fieldMappingToInterphase(ProblemInfo pi) throws CGException {
        String fieldDeclaration = "";
        ArrayList<String> fields = CodeGeneratorUtils.getFields(pi.getProblem());
        for (int i = 0; i < fields.size(); i++) {
            String field = SAMRAIUtils.variableNormalize(fields.get(i));
            fieldDeclaration = fieldDeclaration + IND + IND + IND + "double* " + field + " = ((pdat::CellData<double> *) "
                    + "patch->getPatchData(" + field + "_id).get())->getPointer();" + NL;
        }
        String indent = IND + IND + IND;
        String indent1 = indent + IND;
        String vectorIndex = "";
        String index = "";
        String pIndex = "";
        String remeshCondition = "";
        String minDx = "";
        String values = "";
        for (int i = 0; i < pi.getDimensions(); i++) {
            String coord = pi.getCoordinates().get(i);
            vectorIndex = vectorIndex + coord + " - boxfirst1(" + i + "), ";
            index = index + coord + ", ";
            pIndex = pIndex + "p" + (i + 1) + ", ";
            indent = indent + IND;
            indent1 = indent1 + IND + IND;
            if (i == 0) {
                minDx = "dx[" + i + "]";
            }
            else {
                minDx = "min<double>(" + minDx + ", dx[" + i + "])";
            }
        }
        index = index.substring(0, index.length() - 2);
        vectorIndex = vectorIndex.substring(0, vectorIndex.length() - 2);
        pIndex = pIndex.substring(0, pIndex.length() - 2);
        String aCoefs = "";
        ArrayList<String> elements = new ArrayList<String>();
        for (int i = 0; i < INTERPHASELENGTH; i++) {
            elements.add(String.valueOf(i));
        }
        ArrayList<String> combinations = CodeGeneratorUtils.createCombinations(pi.getDimensions(), elements);  
        for (int i = 0; i < pi.getDimensions(); i++) {
            String coord = pi.getCoordinates().get(i);
            values = values + indent1 + "double pos" + coord + " = vectorP_r(position" + coord + ", sop, " + pIndex + ");" + NL
                    + indent1 + "double normal" + coord + " = vectorP_r(normal" + coord + ", sop, " + pIndex + ");" + NL;
            remeshCondition = remeshCondition + "fabs(vectorP_r(position" + coord + ", sop, " + pIndex + ") - (" + coord + " + p" + (i + 1) 
                    + " * 1.0/" + INTERPHASELENGTH + ".0 + 1.0/" + (INTERPHASELENGTH * 2) + ".0) * dx[" + i + "]) >= " + minDx + "/" 
                    + (INTERPHASELENGTH * 2 + 1) + ".0 || ";
            aCoefs = aCoefs + IND + IND + IND + "double A" + coord + "[4];" + NL;
            String sum = "";
            String sum2 = "";
            for (int j = 0; j < combinations.size(); j++) {
                String comb = combinations.get(j);
                String pIndex2 = "";
                String aIndex = "";
                for (int k = 0; k < comb.length(); k++) {
                    pIndex2 = pIndex2 + comb.substring(k, k + 1) + ", ";
                    aIndex = aIndex + "[" + comb.substring(k, k + 1) + "]";
                }
                pIndex2 = pIndex2.substring(0, pIndex2.lastIndexOf(","));
                sum = sum + indent + IND + IND + IND + "vectorP_r(velocity" + coord + ", sop, " + pIndex2 + ") + " + NL;
                sum2 = sum2 + indent + IND + IND + IND + "accel_" + coord + aIndex + " + " + NL;
            }
            sum = sum.substring(0, sum.lastIndexOf(" +"));
            sum2 = sum2.substring(0, sum2.lastIndexOf(" +"));
        }
        remeshCondition = remeshCondition.substring(0, remeshCondition.lastIndexOf(" ||"));
        String shftValues = "";
        for (int i = 0; i < INTERPHASELENGTH; i++) {
            if (i >= INTERPHASELENGTH / 2) {
                shftValues = shftValues + "1, ";
            }
            else {
                shftValues = shftValues + "0, ";
            }
        }
        shftValues = shftValues.substring(0, shftValues.length() - 2);
        
        String movement = "";
        Set<String> orderMovement = new LinkedHashSet<String>();
        orderMovement.addAll(pi.getMovementInfo().getX3dMovRegions());
        orderMovement.addAll(pi.getMovementInfo().getMovRegions());
        Iterator<String> it =  orderMovement.iterator();
        String indent2 = indent1;
        while (it.hasNext()) {
            String segName = it.next(); 
            String segId = CodeGeneratorUtils.getRegionId(pi.getProblem(), pi.getRegions(), segName);
            String id = CodeGeneratorUtils.getRegionId(pi.getProblem(), pi.getRegions(), segName);
            if (orderMovement.size() > 1 && it.hasNext()) {
                String cond = "vectorP_r(seg, sop, p1, p2) == " + segId;
                if (pi.getMovementInfo().getX3dMovRegions().contains(segName)) {
                    cond = cond + " || vectorP_r(int_seg, sop, p1, p2) == " + segId;
                }
                movement = indent2 + "if (" + cond + ") {" + NL
                        + movement + movement(pi, indent2 + IND, id, segName)
                        + indent2 + "} else {" + NL;
                indent2 = indent2 + IND;
            }
            else {
                movement = movement + movement(pi, indent2, id, segName);
            }
        }
        if (orderMovement.size() > 1) {
            for (int i = 0; i < orderMovement.size() - 1; i++) {
                indent2 = indent2.substring(1);
                movement = movement + indent2 + "}" + NL;
            }
        }
        
        return IND + "//Field mapping to interphase and movement" + NL        
               + IND + "for (int ln = 0; ln <= d_patch_hierarchy->getFinestLevelNumber(); ++ln) {" + NL
               + IND + IND + "std::shared_ptr<hier::PatchLevel > level(d_patch_hierarchy->getPatchLevel(ln));" + NL
               + IND + IND + "//Fill ghosts and periodical boundaries, but no other boundaries" + NL
               + IND + IND + "d_mapping_fill->createSchedule(level, level)->fillData(time, true);" + NL
               + IND + IND + "for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {" + NL
               + IND + IND + IND + "const std::shared_ptr<hier::Patch >& patch = *p_it;" + NL
               + IND + IND + IND + "std::shared_ptr< pdat::IndexData<Interphase, pdat::CellGeometry > > " 
               + "interphase(patch->getPatchData(d_interphase_id), SAMRAI_SHARED_PTR_CAST_TAG);" + NL + NL
               + lsDeclaration(pi, IND + IND + IND)
               + fieldDeclaration
               + IND + IND + IND + "int* soporte = ((pdat::CellData<int> *) patch->getPatchData(d_soporte_id).get())->getPointer();"
               + NL + NL
               + IND + IND + IND + "const hier::Index boxfirst = patch->getBox().lower();" + NL
               + IND + IND + IND + "const hier::Index boxlast  = patch->getBox().upper();" + NL
               + IND + IND + IND + "const hier::Index boxfirst1 = interphase->getGhostBox().lower();" + NL
               + IND + IND + IND + "const hier::Index boxlast1  = interphase->getGhostBox().upper();" + NL + NL
               + IND + IND + IND + "//Get delta spaces into an array. dx, dy, dz." + NL
               + IND + IND + IND + "const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));" + NL
               + IND + IND + IND + "const double* dx  = patch_geom->getDx();" + NL + NL
               + SAMRAIUtils.getLasts(pi, IND + IND + IND, true) + NL
               + aCoefs
               + IND + IND + IND + "int shft[" + INTERPHASELENGTH + "] = {" + shftValues + "};" + NL
               + IND + IND + IND + "//Check position" + NL
               + checkPosition(pi,  IND + IND + IND)
               + IND + IND + IND + "//Field mapping to interphase" + NL
               + fieldMapping(pi, IND + IND + IND)
               + IND + IND + IND + "//Movement" + NL
               + loopInit(pi.getCoordinates(), IND + IND + IND, false)
               + indent + "if (vector(soporte, " + vectorIndex + ") == 1) {" + NL
               + indent + IND + "hier::Index idx(" + index + ");" + NL
               + indent + IND + "Interphase* sop = interphase->getItem(idx);" + NL
               + pLoopInit(pi.getCoordinates(), indent + IND, false, false)
               + values
               + movement
               + indent1 + "if (" + remeshCondition + ") {" + NL
               + indent1 + IND + "remesh[0] = 1;" + NL
               + indent1 + "}" + NL
               + loopEnd(pi.getDimensions(), indent + IND)
               + indent + "}" + NL
               + loopEnd(pi.getDimensions(), IND + IND + IND)
               + IND + IND + "}" + NL
               + IND + "}" + NL;
    }
    
    /**
     * Creates the code that moves the region.
     * @param pi            The process info
     * @param ind           The indent
     * @param segId         The region identifier
     * @param segName       The region name
     * @return              The code
     * @throws CGException  CG00X External error
     */
    private static String movement(ProblemInfo pi, String ind, String segId, String segName) throws CGException {
        String vectorIndex = "";
        String pIndex = "";
        for (int i = 0; i < pi.getDimensions(); i++) {
            String coord = pi.getCoordinates().get(i);
            pIndex = pIndex + "p" + (i + 1) + ", ";
            vectorIndex = vectorIndex + coord + " - boxfirst1(" + i + "), ";
        }
        pIndex = pIndex.substring(0, pIndex.length() - 2);
        vectorIndex = vectorIndex.substring(0, vectorIndex.length() - 2);
        String position = "";
        String normalCond = "";
        String velAt0 = "";
        String x = "";
        String ab = ind + IND + "double ";
        String dest = "";
        String index = "";
        for (int i = 0; i < pi.getDimensions(); i++) {
            String coord = pi.getCoordinates().get(i);
            position = position + ind + "vectorP_r(position" + coord + ", sop, " + pIndex + ") = vectorP_r(position" + coord + ", sop, " 
                    + pIndex + ") + vectorP_r(velocity" + coord + ", sop," + pIndex + ") * dt;" + NL;
            normalCond = normalCond + "equalsEq(normal" + coord + ", 1) && ";
            velAt0 = velAt0 + ind + IND + "vectorP_r(velocity" + coord + ", sop, " + pIndex + ") = 0;" + NL;
            x = x + ind + IND + "double x" + coord + " = pos" + coord + " + normal" + coord + " * ls_" + segId + "_v;" + NL;
            ab = ab + "a" + coord + ", b" + coord + ", ";
            if (pi.getDimensions() == THREE) {
                ab = ab + "c" + coord + ", ";
            }
            String var = "C";
            if (i == 0) {
                var = "A";
            }
            if (i == 1) {
                var = "B";
            }
            for (int j = 0; j < pi.getDimensions(); j++) {
                String coord2 = pi.getCoordinates().get(j);
                dest = dest + ind + IND + IND + "double dest" + coord2 + var + " = " + segName + "TimeCoeff[" + segName + "Unions[unions][" + i 
                    + "]][" + j + "][0] + " + segName + "TimeCoeff[" + segName + "Unions[unions][" + i + "]][" + j + "][1]*tfrac_p_" 
                    + segName + " + " + segName + "TimeCoeff[" + segName + "Unions[unions][" + i + "]][" + j + "][2]*tfrac_p_" 
                    + segName + "*tfrac_p_" + segName + " + " + segName + "TimeCoeff[" + segName + "Unions[unions][" + i + "]][" + j 
                    + "][3]*tfrac_p_" + segName + "*tfrac_p_" + segName + "*tfrac_p_" + segName + ";" + NL;
            }
            //The default values is that the nearest point is A.
            index = index + ind + IND + IND + "int index" + var + " = " + i + ";" + NL;
        }
        String pointIndex = ind + IND + "int pointAIndex, pointBIndex";
        String mods;
        if (pi.getDimensions() == THREE) {
            pointIndex = pointIndex + ", pointCIndex;" + NL;
            mods = "modab, modac";
        }
        else {
            pointIndex = pointIndex + ";" + NL;
            mods = "modab";
        }
        
        ab = ab.substring(0, ab.length() - 2) + ";" + NL;
        normalCond = normalCond.substring(0, normalCond.lastIndexOf(" &&"));
        String velocity = "";
        if (CodeGeneratorUtils.find(pi.getProblem(), "//mms:regionGroup[mms:regionGroupName = '" 
                + segName + "']//mms:movement/mms:type[text() = 'Fixed']").getLength() > 0) {
            velocity = ind + "// Movement from x3ds" + NL
                   + ind + "//Get values" + NL
                   + ind + "double ls_" + segId + "_v = fabs(vectorP(ls_" + segId + ", " + vectorIndex + ", " + pIndex + "));" + NL
                   + ind + "if ((" + normalCond + ") || equalsEq(timedif_" + segName + ", 0)) {" + NL
                   + velAt0
                   + ind + "} else {" + NL
                   + ind + IND + "//Get X" + NL
                   + x + NL
                   + ind + IND + "double dist2 = 1e10;" + NL
                   + ab
                   + pointIndex
                   + ind + IND + "double " + mods + ";" + NL
                   + ind + IND + "int unionsI;" + NL
                   + ind + IND + "for (int unions = 0; unions < UNIONS_" + segName + "; unions++) {" + NL
                   + dest
                   + index 
                   + setNearestPoint(pi, ind + IND + IND)
                   + setNearestUnion(pi, ind + IND + IND, segName)
                   + ind + IND + "}" + NL
                   + accelCalc(pi, ind + IND, segName)
                   + ind + "}" + NL;
        }
        else {
            //Parameters for the xsl transformation. It is not a condition formula
            String[][] xslParams = new String [4][2];
            xslParams[0][0] =  "condition";
            xslParams[0][1] =  "false";
            xslParams[1][0] =  "indent";
            xslParams[1][1] =  String.valueOf(ind.length());
            xslParams[2][0] =  "dimensions";
            xslParams[2][1] =  String.valueOf(pi.getDimensions());
            xslParams[3][0] = "timeCoord";
            xslParams[3][1] = pi.getTimeCoord();
            velocity = ind + "// Movement from equations" + NL;
            NodeList movementEqs = CodeGeneratorUtils.find(pi.getProblem(), "//mms:regionGroup[" 
                    + "mms:regionGroupName = '" + segName + "']//mms:movementEquation");
            for (int i = 0; i < movementEqs.getLength(); i++) {
                velocity = velocity + SAMRAIUtils.xmlTransform(
                        movPreprocess(pi, movementEqs.item(i)).getFirstChild(), xslParams, null, null);
            }
        }
        
        return ind + "//Movement for region " + segName + NL
                + velocity + position;
    }
    
    /**
     * Preprocess special movement variables from the mathML.
     * @param pi            Problem information
     * @param instruction   The instruction.
     * @return              The new instruction
     * @throws CGException  CG00X External error
     */
    private static Node movPreprocess(ProblemInfo pi, Node instruction) throws CGException  {
        DocumentFragment pIndex = pi.getProblem().createDocumentFragment();
        for (int j = 0; j < pi.getDimensions(); j++) {
            Element ci = CodeGeneratorUtils.createElement(pi.getProblem(), MTURI, "ci");
            ci.setTextContent("p" + (j + 1));
            pIndex.appendChild(ci);
        }
        //Vectorize velocity and position variables
        for (int j = 0; j < pi.getCoordinates().size(); j++) {
            String coord = pi.getCoordinates().get(j);
            NodeList vars = CodeGeneratorUtils.find(instruction, ".//mt:ci[text() = 'position" + coord + "' or text() = 'velocity" + coord + "']");
            for (int i = vars.getLength() - 1; i >= 0; i--) {
                Element apply = CodeGeneratorUtils.createElement(pi.getProblem(), MTURI, "apply");
                Element vector = CodeGeneratorUtils.createElement(pi.getProblem(), MTURI, "ci");
                vector.setTextContent("vectorP_r");
                Element sop = CodeGeneratorUtils.createElement(pi.getProblem(), MTURI, "ci");
                sop.setTextContent("sop");
                apply.appendChild(vector);
                apply.appendChild(vars.item(i).cloneNode(true));
                apply.appendChild(sop);
                apply.appendChild(pIndex.cloneNode(true));
                
                vars.item(i).getParentNode().replaceChild(apply, vars.item(i));
            }
        }
        return instruction;
    }
    
    /**
     * Creates the code to calculate the acceleration and velocity.
     * @param pi        The problem info
     * @param ind       The indent
     * @param segName   The region name
     * @return          The code
     */
    private static String accelCalc(ProblemInfo pi, String ind, String segName) {
        String adists = "";
        String v = "";
        String v2 = "";
        for (int i = 0; i < pi.getDimensions(); i++) {
            String coord = pi.getCoordinates().get(i);
            adists = adists + ind + "double ax" + coord + " = x" + coord + " - a" + coord + ";" + NL
                    + ind + "double ab" + coord + " = b" + coord + " - a" + coord + ";" + NL;
            v = v + ind + "double va" + coord + " = d_tfrac_p_dt_" + segName + " * (" + segName + "TimeCoeff[pointAIndex][" + i + "][1] + 2.0*" 
                    + segName + "TimeCoeff[pointAIndex][" + i + "][2]*tfrac_p_" + segName + " + 3.0*" + segName + "TimeCoeff[pointAIndex][" + i 
                    + "][3]*tfrac_p_" + segName + "*tfrac_p_" + segName + " );" + NL
                    + ind + "double vb" + coord + " = d_tfrac_p_dt_" + segName + " * (" + segName + "TimeCoeff[pointBIndex][" + i + "][1] + 2.0*" 
                    + segName + "TimeCoeff[pointBIndex][" + i + "][2]*tfrac_p_" + segName + " + 3.0*" + segName + "TimeCoeff[pointBIndex][" + i 
                    + "][3]*tfrac_p_" + segName + "*tfrac_p_" + segName + " );" + NL;
            v2 = v2 + ind + "double vab" + coord + " = vb" + coord + " - va" + coord + ";" + NL;
            if (pi.getDimensions() == THREE) {
                adists = adists + ind + "double ac" + coord + " = c" + coord + " - a" + coord + ";" + NL;
                v = v + ind + "double vc" + coord + " = d_tfrac_p_dt_" + segName + " * (" + segName + "TimeCoeff[pointCIndex][" + i + "][1] + 2.0*" 
                        + segName + "TimeCoeff[pointCIndex][" + i + "][2]*tfrac_p_" + segName + " + 3.0*" + segName + "TimeCoeff[pointCIndex][" + i 
                        + "][3]*tfrac_p_" + segName + "*tfrac_p_" + segName + " );" + NL;
                v2 = v2 + ind + "double vac" + coord + " = vc" + coord + " - va" + coord + ";" + NL;
            }
        }
        
        String accCalcs = "";
        if (pi.getDimensions() == 2) {
            String coord1 = pi.getCoordinates().get(0);
            String coord2 = pi.getCoordinates().get(1);
            accCalcs = ind + "double par_lambda = (ax" + coord1 + "*ab" + coord1 + " + ax" + coord2 + "*ab" + coord2 + ") / (modab*modab);" + NL
                    + ind + "double par_d = (ax" + coord1 + "*ab" + coord2 + " - ax" + coord2 + "*ab" + coord1 + ") / modab;" + NL + NL
                    + ind + "double aux1 = (ab" + coord1 + "*vab" + coord1 + " + ab" + coord2 + "*vab" + coord2 + ") / (modab*modab*modab);" + NL
                    + ind + "vectorP_r(velocity" + coord1 + ", sop, p1, p2) = va" + coord1 + " + par_lambda*vab" + coord1 + " + par_d * ( vab" 
                    + coord2 + "/modab - ab" + coord2 + "*aux1);" + NL
                    + ind + "vectorP_r(velocity" + coord2 + ", sop, p1, p2) = va" + coord2 + " + par_lambda*vab" + coord2 + " + par_d * (-vab" 
                    + coord1 + "/modab + ab" + coord1 + "*aux1);" + NL + NL;
        }
        else {
            String coord1 = pi.getCoordinates().get(0);
            String coord2 = pi.getCoordinates().get(1);
            String coord3 = pi.getCoordinates().get(2);
            String vectorialProducts = "";
            for (int i = 0; i < THREE; i++) {
                String coord = pi.getCoordinates().get(i);
                String coorda = pi.getCoordinates().get(((i + 1) % THREE + THREE) % THREE);
                String coordb = pi.getCoordinates().get(((i + 2) % THREE + THREE) % THREE);
                vectorialProducts = vectorialProducts + ind + "double vpvab_ac" + coord + " = vab" + coorda + "*ac" + coordb + " - vab" + coordb
                        + "*ac" + coorda + ";" + NL
                        + ind + "double vpab_vac" + coord + " = ab" + coorda + "*vac" + coordb + " - ab" + coordb + "*vac" + coorda + ";" + NL
                        + ind + "double vpab_ac" + coord + " = ab" + coorda + "*ac" + coordb + " - ab" + coordb + "*ac" + coorda + ";" + NL;
            }
            accCalcs = ind + "double pab_ac = ab" + coord1 + "*ac" + coord1 + " + ab" + coord2 + "*ac" + coord2 + " + ab" + coord3 + "*ac" + coord3 
                    + ";" + NL 
                    + ind + "double pax_ab = ax" + coord1 + "*ab" + coord1 + " + ax" + coord2 + "*ab" + coord2 + " + ax" + coord3 + "*ab" + coord3 
                    + ";" + NL
                    + ind + "double pax_ac = ax" + coord1 + "*ac" + coord1 + " + ax" + coord2 + "*ac" + coord2 + " + ax" + coord3 + "*ac" + coord3 
                    + ";" + NL 
                    + vectorialProducts
                    + ind + "double modvpab_ac = vpvab_ac" + coord1 + " * vpvab_ac" + coord1 + " + vpvab_ac" + coord2 + " * vpvab_ac" + coord2 
                    + " + vpvab_ac" + coord3 + " * vpvab_ac" + coord3 + ";" + NL 
                    + ind + "double par_lambda = (pax_ab*(modac*modac) - pax_ac*pab_ac) / (modab*modab*modac*modac - pab_ac);" + NL
                    + ind + "double par_gamma = (pax_ac*(modab*modab) - pax_ab*pab_ac) / (modab*modab*modac*modac - pab_ac);" + NL
                    + ind + "double par_d = (ax" + coord1 + "*vpab_ac" + coord1 + " + ax" + coord2 + "*vpab_ac" + coord2 + " + ax" + coord3 
                    + "*vpab_ac" + coord3 + ") / modvpab_ac;" + NL + NL
                    + ind + "double aux1 = (vpab_ac" + coord1 + "*(vpvab_ac" + coord1 + " + vpab_vac" + coord1 + ") + vpab_ac" + coord2 
                    + "*(vpvab_ac" + coord2 + " + vpab_vac" + coord2 + ") + vpab_ac" + coord3 + "*(vpvab_ac" + coord3 + " + vpab_vac" + coord3 
                    + "));" + NL 
                    + ind + "vectorP_r(velocity" + coord1 + ", sop, p1, p2, p3) = va" + coord1 + " + par_lambda*vab" + coord1 + " + par_gamma*vac"
                    + coord1 + " + par_d * ((vpvab_ac" + coord1 + " + vpab_vac" + coord1 + ")/modvpab_ac - (vpab_ac" + coord1 + ")*aux1);" + NL
                    + ind + "vectorP_r(velocity" + coord2 + ", sop, p1, p2, p3) = va" + coord2 + " + par_lambda*vab" + coord2 + " + par_gamma*vac"
                    + coord2 + " + par_d * ((vpvab_ac" + coord2 + " + vpab_vac" + coord2 + ")/modvpab_ac - (vpab_ac" + coord2 + ")*aux1);" + NL + NL
                    + ind + "vectorP_r(velocity" + coord3 + ", sop, p1, p2, p3) = va" + coord3 + " + par_lambda*vab" + coord3 + " + par_gamma*vac"
                    + coord3 + " + par_d * ((vpvab_ac" + coord3 + " + vpab_vac" + coord3 + ")/modvpab_ac - (vpab_ac" + coord3 + ")*aux1);" + NL;
        }
        
        return adists
                + v
                + v2
                + accCalcs; 
    }

    /**
     * Creates the code to establish which is the nearest union from the x3d.
     * @param pi            The problem info
     * @param ind           The indent
     * @param regionName   The region name
     * @return              The code
     */
    private static String setNearestUnion(ProblemInfo pi, String ind, String regionName) {
        if (pi.getDimensions() == 2) {
            String coord1 = pi.getCoordinates().get(0);
            String coord2 = pi.getCoordinates().get(1);
            return ind + "double modtmp = sqrt((dest" + coord1 + "A - dest" + coord1 + "B)*(dest" + coord1 + "A - dest" + coord1 + "B) + (dest" 
                    + coord2 + "A - dest" + coord2 + "B)*(dest" + coord2 + "A - dest" + coord2 + "B));" + NL
                    + ind + "if (modtmp > 0) {" + NL
                    + ind + IND + "double distLineTmp = ((x" + coord1 + " - dest" + coord1 + "A)*(dest" + coord2 + "B - dest" + coord2 
                    + "A) - (x" + coord2 + " - dest" + coord2 + "A)*(dest" + coord1 + "B - dest" + coord1 + "A)) / modtmp;" + NL
                    + ind + IND + "double proj = ((x" + coord1 + "-dest" + coord1 + "A)*(dest" + coord1 + "B - dest" + coord1 + "A) + (x" 
                    + coord2 + "-dest" + coord2 + "A)*(dest" + coord2 + "B - dest" + coord2 + "A))/modtmp;" + NL
                    + ind + IND + "double distSeg2;" + NL
                    + ind + IND + "if (proj < 0) {" + NL
                    + ind + IND + IND + "distSeg2 = (distLineTmp*distLineTmp + proj*proj);" + NL
                    + ind + IND + "} else {" + NL
                    + ind + IND + IND + "distSeg2 = distLineTmp*distLineTmp;" + NL
                    + ind + IND + "}" + NL
                    + ind + IND + "if (distSeg2 < dist2 && (modtmp > min<double>(dx[0], dx[1])/5 || (proj >= 0 && proj <= modtmp))) {" + NL
                    + ind + IND + IND + "dist2 = distSeg2;" + NL
                    + ind + IND + IND + "a" + coord1 + " = dest" + coord1 + "A;" + NL
                    + ind + IND + IND + "a" + coord2 + " = dest" + coord2 + "A;" + NL
                    + ind + IND + IND + "b" + coord1 + " = dest" + coord1 + "B;" + NL
                    + ind + IND + IND + "b" + coord2 + " = dest" + coord2 + "B;" + NL
                    + ind + IND + IND + "pointAIndex = " + regionName + "Unions[unions][indexA];" + NL
                    + ind + IND + IND + "pointBIndex = " + regionName + "Unions[unions][indexB];" + NL
                    + ind + IND + IND + "modab = modtmp;" + NL
                    + ind + IND + IND + "unionsI = unions;" + NL
                    + ind + IND + "}" + NL
                    + ind + "}" + NL;
        }
		String coord1 = pi.getCoordinates().get(0);
		String coord2 = pi.getCoordinates().get(1);
		String coord3 = pi.getCoordinates().get(2);
		return ind + "double modabxac = sqrt( pow(((dest" + coord2 + "B - dest" + coord2 + "A) * (dest" + coord3 + "C - dest" 
		        + coord3 + "A)  - (dest" + coord3 + "B - dest" + coord3 + "A) * (dest" + coord2 + "C - dest" + coord2 
		        + "A)), 2) + pow(((dest" + coord3 + "B - dest" + coord3 + "A) * (dest" + coord1 + "C - dest" + coord1 
		        + "A)  - (dest" + coord1 + "B - dest" + coord1 + "A) * (dest" + coord3 + "C - dest" + coord3 
		        + "A)), 2) + pow(((dest" + coord1 + "B - dest" + coord1 + "A) * (dest" + coord2 + "C - dest" + coord2 
		        + "A)  - (dest" + coord2 + "B - dest" + coord2 + "A) * (dest" + coord1 + "C - dest" + coord1 + "A)), 2) );" + NL
		        + ind + "if (modabxac > 0) {" + NL
		        + ind + IND + "double dist = ((x" + coord1 + " - dest" + coord1 + "A)*((dest" + coord2 + "B - dest" + coord2 
		        + "A) * (dest" + coord3 + "C - dest" + coord3 + "A)  - (dest" + coord3 + "B - dest" + coord3 + "A) * (dest" 
		        + coord2 + "C - dest" + coord2 + "A)) + (x" + coord2 + " - dest" + coord2 + "A)*((dest" + coord3 + "B - dest" 
		        + coord3 + "A) * (dest" + coord1 + "C - dest" + coord1 + "A)  - (dest" + coord1 + "B - dest" + coord1 
		        + "A) * (dest" + coord3 + "C - dest" + coord3 + "A)) + (x" + coord3 + " - dest" + coord3 + "A)*((dest" + coord1 
		        + "B - dest" + coord1 + "A) * (dest" + coord2 + "C - dest" + coord2 + "A)  - (dest" + coord2 + "B - dest" + coord2 
		        + "A) * (dest" + coord1 + "C - dest" + coord1 + "A))) / modabxac;" + NL
		        + ind + IND + "double modabtmp = sqrt( (dest" + coord1 + "B - dest" + coord1 + "A)*(dest" + coord1 + "B - dest" 
		        + coord1 + "A) + (dest" + coord2 + "B - dest" + coord2 + "A)*(dest" + coord2 + "B - dest" + coord2 + "A) + (dest" 
		        + coord3 + "B - dest" + coord3 + "A)*(dest" + coord3 + "B - dest" + coord3 + "A) );" + NL
		        + ind + IND + "double modactmp = sqrt( (dest" + coord1 + "C - dest" + coord1 + "A)*(dest" + coord1 + "C - dest" 
		        + coord1 + "A) + (dest" + coord2 + "C - dest" + coord2 + "A)*(dest" + coord2 + "C - dest" + coord2 + "A) + (dest" 
		        + coord3 + "C - dest" + coord3 + "A)*(dest" + coord3 + "C - dest" + coord3 + "A) );" + NL
		        + ind + IND + "double abac = (dest" + coord1 + "B - dest" + coord1 + "A) * (dest" + coord1 + "C - dest" + coord1 
		        + "A) + (dest" + coord2 + "B - dest" + coord2 + "A) * (dest" + coord2 + "C - dest" + coord2 + "A) + (dest" 
		        + coord3 + "B - dest" + coord3 + "A) * (dest" + coord3 + "C - dest" + coord3 + "A);" + NL
		        + ind + IND + "double axab = (x" + coord1 + " - dest" + coord1 + "A) * (dest" + coord1 + "B - dest" + coord1 
		        + "A) + (x" + coord2 + " - dest" + coord2 + "A) * (dest" + coord2 + "B - dest" + coord2 + "A) + (x" + coord3 
		        + " - dest" + coord3 + "A) * (dest" + coord3 + "B - dest" + coord3 + "A);" + NL
		        + ind + IND + "double axac = (x" + coord1 + " - dest" + coord1 + "A) * (dest" + coord1 + "C - dest" + coord1 
		        + "A) + (x" + coord2 + " - dest" + coord2 + "A) * (dest" + coord2 + "C - dest" + coord2 + "A) + (x" + coord3 
		        + " - dest" + coord3 + "A) * (dest" + coord3 + "C - dest" + coord3 + "A);" + NL
		        + ind + IND + "double denom = modabtmp * modabtmp * modactmp * modactmp - abac * abac;" + NL
		        + ind + IND + "double lambda_mov = (axab * modactmp * modactmp - axac * abac) / denom;" + NL + NL
		        + ind + IND + "double gamma_mov = (axac * modabtmp * modabtmp - axab * abac) / denom;" + NL + NL
		        + ind + IND + "double dist_p = 0;" + NL
		        + ind + IND + "if (lambda_mov >= 0 && gamma_mov >= 0 && lambda_mov + gamma_mov > 1) {" + NL
		        + ind + IND + IND + "double bcac = (dest" + coord1 + "C - dest" + coord1 + "B) * (dest" + coord1 + "C - dest" 
		        + coord1 + "A) + (dest" + coord2 + "C - dest" + coord2 + "B) * (dest" + coord2 + "C - dest" + coord2 + "A) + (dest"
		        + coord3 + "C - dest" + coord3 + "B) * (dest" + coord3 + "C - dest" + coord3 + "A);" + NL
		        + ind + IND + IND + "double bcab = (dest" + coord1 + "C - dest" + coord1 + "B) * (dest" + coord1 + "B - dest"
		        + coord1 + "A) + (dest" + coord2 + "C - dest" + coord2 + "B) * (dest" + coord2 + "B - dest" + coord2 + "A) + (dest" 
		        + coord3 + "C - dest" + coord3 + "B) * (dest" + coord3 + "B - dest" + coord3 + "A);" + NL
		        + ind + IND + IND + "dist_p = (((x" + coord1 + " - dest" + coord1 + "B) * (dest" + coord1 + "B - dest" + coord1 
		        + "A) + (x" + coord2 + " - dest" + coord2 + "B) * (dest" + coord2 + "B - dest" + coord2 + "A) + (x" + coord3 
		        + " - dest" + coord3 + "B) * (dest" + coord3 + "B - dest" + coord3 + "A)) * bcac - ((x" + coord1 + " - dest" 
		        + coord1 + "B) * (dest" + coord1 + "C - dest" + coord1 + "A) + (x" + coord2 + " - dest" + coord2 + "B) * (dest" 
		        + coord2 + "C - dest" + coord2 + "A) + (x" + coord3 + " - dest" + coord3 + "B) * (dest" + coord3 + "C - dest" 
		        + coord3 + "A)) * bcab) / fabs((dest" + coord1 + "B - dest" + coord1 + "A) * bcac + (dest" + coord2 + "B - dest"
		        + coord2 + "A) * bcac + (dest" + coord3 + "B - dest" + coord3 + "A) * bcac - ((dest" + coord1 + "C - dest" + coord1
		        + "A) * bcab + (dest" + coord2 + "C - dest" + coord2 + "A) * bcab + (dest" + coord3 + "C - dest" + coord3 
		        + "A) * bcab));" + NL
		        + ind + IND + "} else {" + NL
		        + ind + IND + IND + "if (lambda_mov < 0 && gamma_mov < 0) {" + NL
		        + ind + IND + IND + IND + "dist_p = sqrt(lambda_mov * lambda_mov * modabtmp * modabtmp + 2 * lambda_mov * " 
		        + "gamma_mov * abac + gamma_mov * gamma_mov * modactmp * modactmp);" + NL
		        + ind + IND + IND + "} else {" + NL
		        + ind + IND + IND + IND + "if (lambda_mov >= 0 && gamma_mov < 0) {" + NL
		        + ind + IND + IND + IND + IND + "double proj_mov = axab / modabtmp;" + NL
		        + ind + IND + IND + IND + IND + "if (proj_mov < 0) {" + NL
		        + ind + IND + IND + IND + IND + IND + "dist_p = sqrt(lambda_mov * lambda_mov * modabtmp * modabtmp + 2 *" 
		        + " lambda_mov * gamma_mov * abac + gamma_mov * gamma_mov * modactmp * modactmp);" + NL
		        + ind + IND + IND + IND + IND + "} else {" + NL
		        + ind + IND + IND + IND + IND + IND + "dist_p = (axab * abac - axac * modabtmp * modabtmp) / fabs((dest" + coord1 
		        + "B - dest" + coord1 + "A) * abac + (dest" + coord2 + "B - dest" + coord2 + "A) * abac + (dest" + coord3 
		        + "B - dest" + coord3 + "A) * abac - ((dest" + coord1 + "C - dest" + coord1 + "A) * modabtmp * modabtmp + (dest" 
		        + coord2 + "C - dest" + coord2 + "A) * modabtmp * modabtmp + (dest" + coord3 + "C - dest" + coord3 
		        + "A) * modabtmp * modabtmp));" + NL
		        + ind + IND + IND + IND + IND + "}" + NL
		        + ind + IND + IND + IND + "} else {" + NL
		        + ind + IND + IND + IND + IND + "if (lambda_mov < 0 && gamma_mov >= 0) {" + NL
		        + ind + IND + IND + IND + IND + IND + "double proj_mov = axac / modactmp;" + NL
		        + ind + IND + IND + IND + IND + IND + "if (proj_mov < 0) {" + NL
		        + ind + IND + IND + IND + IND + IND + IND + "dist_p = sqrt(lambda_mov * lambda_mov * modabtmp * modabtmp + "
		        + "2 * lambda_mov * gamma_mov * abac + gamma_mov * gamma_mov * modactmp * modactmp);" + NL
		        + ind + IND + IND + IND + IND + IND + "} else {" + NL
		        + ind + IND + IND + IND + IND + IND + IND + "dist_p = (axab * modactmp * modactmp - axac * abac) / fabs((dest" 
		        + coord1 + "B - dest" + coord1 + "A) * modactmp * modactmp + (dest" + coord2 + "B - dest" + coord2 
		        + "A) * modactmp * modactmp + (dest" + coord3 + "B - dest" + coord3 + "A) * modactmp * modactmp - ((dest" + coord1 
		        + "C - dest" + coord1 + "A) * abac + (dest" + coord2 + "C - dest" + coord2 + "A) * abac + (dest" + coord3 
		        + "C - dest" + coord3 + "A) * abac));" + NL
		        + ind + IND + IND + IND + IND + IND + "}" + NL
		        + ind + IND + IND + IND + IND + "}" + NL
		        + ind + IND + IND + IND + "}" + NL
		        + ind + IND + IND + "}" + NL
		        + ind + IND + "}" + NL
		        + ind + IND + "dist = sqrt(dist * dist + dist_p * dist_p);" + NL
		        + ind + IND + "if (dist < dist2) {" + NL
		        + ind + IND + IND + "dist2 = dist;" + NL
		        + ind + IND + IND + "a" + coord1 + " = dest" + coord1 + "A;" + NL
		        + ind + IND + IND + "a" + coord2 + " = dest" + coord2 + "A;" + NL
		        + ind + IND + IND + "a" + coord3 + " = dest" + coord3 + "A;" + NL
		        + ind + IND + IND + "b" + coord1 + " = dest" + coord1 + "B;" + NL
		        + ind + IND + IND + "b" + coord2 + " = dest" + coord2 + "B;" + NL
		        + ind + IND + IND + "b" + coord3 + " = dest" + coord3 + "B;" + NL
		        + ind + IND + IND + "c" + coord1 + " = dest" + coord1 + "C;" + NL
		        + ind + IND + IND + "c" + coord2 + " = dest" + coord2 + "C;" + NL
		        + ind + IND + IND + "c" + coord3 + " = dest" + coord3 + "C;" + NL
		        + ind + IND + IND + "pointAIndex = " + regionName + "Unions[unions][indexA];" + NL
		        + ind + IND + IND + "pointBIndex = " + regionName + "Unions[unions][indexB];" + NL
		        + ind + IND + IND + "pointCIndex = " + regionName + "Unions[unions][indexC];" + NL
		        + ind + IND + IND + "modab = modabtmp;" + NL
		        + ind + IND + IND + "modac = modactmp;" + NL
		        + ind + IND + IND + "unionsI = unions;" + NL
		        + ind + IND + "}" + NL
		        + ind + "}" + NL;
    }
    
    /**
     * Creates the code to set the union nearest point to x. 
     * @param pi        The problem info
     * @param ind       The indent
     * @return          The code
     */
    private static String setNearestPoint(ProblemInfo pi, String ind) {
        String nearest = "";
        for (int i = 1; i < pi.getDimensions(); i++) {
            String var = "C";
            if (i == 0) {
                var = "A";
            }
            if (i == 1) {
                var = "B";
            }
            String baseComp = "";
            for (int j = 0; j < pi.getDimensions(); j++) {
                String coord = pi.getCoordinates().get(j);
                baseComp = baseComp + "(dest" + coord + var + " - x" + coord + ") * (dest" + coord + var + " - x" + coord + ") + ";
            }
            baseComp = baseComp.substring(0, baseComp.lastIndexOf(" +")) + " < ";
            String comp = "";
            String indexes = "";
            for (int j = 0; j < pi.getDimensions(); j++) {
                String var2 = "C";
                if (j == 0) {
                    var2 = "A";
                }
                if (j == 1) {
                    var2 = "B";
                }
                String coordDist = "";
                for (int k = 0; k < pi.getDimensions(); k++) {
                    String coord2 = pi.getCoordinates().get(k);
                    coordDist = coordDist + "(dest" + coord2 + var2 + " - x" + coord2 + ") * (dest" + coord2 + var2 + " - x" + coord2 + ") + ";
                }
                coordDist = coordDist.substring(0, coordDist.lastIndexOf(" +"));
                if (i != j) {
                    comp = comp + baseComp + coordDist + " && ";
                }
                indexes = indexes + ind + IND + "index" + var2 + " = " 
                        + (((j - i) % pi.getDimensions() + pi.getDimensions()) % pi.getDimensions()) + ";" + NL;
            }
            String shifts = "";
            String tmpDec = "";
            for (int k = 0; k < pi.getDimensions(); k++) {
                String coord = pi.getCoordinates().get(k);
                String tmps = "";
                String shift = "";
                for (int j = 0; j < pi.getDimensions(); j++) {
                    String var2 = "C";
                    if (j == 0) {
                        var2 = "A";
                    }
                    if (j == 1) {
                        var2 = "B";
                    }
                    String var3 = "C";
                    if ((((j - i) % pi.getDimensions() + pi.getDimensions()) % pi.getDimensions()) == 0) {
                        var3 = "A";
                    }
                    if ((((j - i) % pi.getDimensions() + pi.getDimensions()) % pi.getDimensions()) == 1) {
                        var3 = "B";
                    }
                    
                    tmps = tmps + ind + IND + "tmp" + var2 + " = dest" + coord + var2 + ";" + NL;
                    shift = shift + ind + IND + "dest" + coord + var2 + " = tmp" + var3 + ";" + NL;
                }
                shifts = shifts + tmps + shift;
                String var2 = "C";
                if (k == 0) {
                    var2 = "A";
                }
                if (k == 1) {
                    var2 = "B";
                }
                tmpDec = tmpDec + ind + IND + "double tmp" + var2 + ";" + NL;
            }
            comp = comp.substring(0, comp.lastIndexOf(" &&"));
            nearest = nearest + ind + "if (" + comp + ") {" + NL
                    + tmpDec
                    + shifts
                    + indexes
                    + ind + "}" + NL;
        }
        return nearest;
    }
    
    /**
     * Creates the code to do the field mapping.
     * @param pi            The problem info
     * @param ind           The indent
     * @return              The code
     * @throws CGException  CG00X External error
     */
    private static String fieldMapping(ProblemInfo pi, String ind) throws CGException {
        String[][] xslParams = new String [THREE][2];
        xslParams[0][0] = "condition";
        xslParams[0][1] = "true";
        xslParams[1][0] = "indent";
        xslParams[1][1] = "0";
        xslParams[2][0] =  "dimensions";
        xslParams[2][1] =  String.valueOf(pi.getCoordinates().size());
        String vectorIndex = "";
        String index = "";
        String pIndex = "";
        String loopInit = "";
        String pos = "";
        String indent = ind;
        for (int i = pi.getDimensions() - 1; i >= 0; i--) {
            String coord = pi.getCoordinates().get(i);
            loopInit = loopInit + indent + "for (int " + coord + " = boxfirst1(" + i + "); " + coord + " <= boxlast1(" + i + "); " + coord + "++) {"
                    + NL
                    + indent + IND + "int " + coord + "_shft = 0;" + NL
                    + indent + IND + "if (" + coord + " - 1 == boxfirst1(" + i + ")) {   // one potential missing point on the bottom" + NL
                    + indent + IND + IND + coord + "_shft = 1;" + NL
                    + indent + IND + "}" + NL
                    + indent + IND + "if (" + coord + " == boxfirst1(" + i + ")) {   // two potential missing points on the bottom" + NL
                    + indent + IND + IND + coord + "_shft = 2;" + NL
                    + indent + IND + "}" + NL
                    + indent + IND + "if (" + coord + " + 1 == boxlast1(" + i + ")) {    // one potential missing point on the top" + NL
                    + indent + IND + IND + coord + "_shft = -1;" + NL
                    + indent + IND + "}" + NL
                    + indent + IND + "if (" + coord + " == boxlast1(" + i + ")) {    // two potential missing points on the top" + NL
                    + indent + IND + IND + coord + "_shft = -2;" + NL
                    + indent + IND + "}" + NL;
            indent = indent + IND;
        }
        String arrayDec = "";
        String tmpLoopInit = "";
        String indent2 = indent + IND;
        String arrayIndex = "";
        String vShfIndex = "";
        for (int i = 0; i < pi.getDimensions(); i++) {
            String coord = pi.getCoordinates().get(i);
            vectorIndex = vectorIndex + coord + " - boxfirst1(" + i + "), ";
            index = index + coord + ", ";
            pIndex = pIndex + "p" + (i + 1) + ", ";
            pos = pos + indent + IND + "double pos" + coord + " = (" + coord + " - 0.5) * dx[" + i + "] " 
                    + "+ d_grid_geometry->getXLower()[" + i + "];" + NL;
            arrayDec = arrayDec + "[5]";
            tmpLoopInit = tmpLoopInit + indent2 + "for (int " + coord + "1 = " + coord + " - 2; " + coord + "1 <= " + coord + " + 2; " 
                    + coord + "1++) {" + NL;
            indent2 = indent2 + IND;
            arrayIndex = arrayIndex + "[" + coord + "1 - " + coord + " + 2]";
            vShfIndex = vShfIndex + coord + "1 + " + coord + "_shft - boxfirst1(" + i + "), ";
        }
        pIndex = pIndex.substring(0, pIndex.length() - 2);
        
        index = index.substring(0, index.length() - 2);
        vectorIndex = vectorIndex.substring(0, vectorIndex.length() - 2);
        vShfIndex = vShfIndex.substring(0, vShfIndex.length() - 2);
        String coefs = "";
        String indexes = "";
        for (int i = 0; i < pi.getDimensions(); i++) {
            String coord = pi.getCoordinates().get(i);
            coefs = coefs + indent2 + "double aux" + coord + "1 = ( vectorP_r(position" + coord + ", sop, " + pIndex + ") - (pos" + coord 
                    + " + shft[p" + (i + 1) + "] * dx[" + i + "] + d_grid_geometry->getXLower()[" + i + "]) ) / dx[" + i + "];" + NL
                    + indent2 + "double aux" + coord + "2 = aux" + coord + "1*aux" + coord + "1;" + NL
                    + indent2 + "double aux" + coord + "3 = aux" + coord + "1*aux" + coord + "2;" + NL
                    + indent2 + "int c" + coord + "_shft = 0;" + NL
                    + indent2 + "if (" + coord + "_shft - shft[p" + (i + 1) + "] == 1) {                // one missing point on the left" + NL
                    + indent2 + IND + "c" + coord + "_shft = 1;" + NL
                    + indent2 + IND + "A" + coord + "[0] = 1 - 2.0*aux" + coord + "1 + 1.5*aux" + coord + "2 - 0.5*aux" + coord + "3;" + NL
                    + indent2 + IND + "A" + coord + "[1] = 3.5*aux" + coord + "1 - 4.0*aux" + coord + "2 + 1.5*aux" + coord + "3;" + NL
                    + indent2 + IND + "A" + coord + "[2] = -2.0*aux" + coord + "1 + 3.5*aux" + coord + "2 - 1.5*aux" + coord + "3;" + NL
                    + indent2 + IND + "A" + coord + "[3] = 0.5*aux" + coord + "1 - aux" + coord + "2 + 0.5*aux" + coord + "3;" + NL
                    + indent2 + "}" + NL
                    + indent2 + "else if (" + coord + "_shft - shft[p" + (i + 1) + "] == 2) {           // two missing points on the left" + NL
                    + indent2 + IND + "c" + coord + "_shft = 2;" + NL                                
                    + indent2 + IND + "A" + coord + "[0] = 3.0 - 3.0*aux" + coord + "1 + 1.5*aux" + coord + "2 - 0.5*aux" + coord + "3;" + NL
                    + indent2 + IND + "A" + coord + "[1] = -3.0 + 5.5*aux" + coord + "1 - 4*aux" + coord + "2 + 1.5*aux" + coord + "3;" + NL
                    + indent2 + IND + "A" + coord + "[2] = 1.0 - 3.0*aux" + coord + "1 + 3.5*aux" + coord + "2 - 1.5*aux" + coord + "3;" + NL
                    + indent2 + IND + "A" + coord + "[3] = 0.5*aux" + coord + "1 - aux" + coord + "2 + 0.5*aux" + coord + "3;" + NL
                    + indent2 + "}" + NL
                    + indent2 + "else if (" + coord + "_shft + (1-shft[p" + (i + 1) + "]) == -1) {      // one missing point on the right" + NL
                    + indent2 + IND + "c" + coord + "_shft = -1;" + NL                               
                    + indent2 + IND + "A" + coord + "[0] = 0.5*aux" + coord + "2 - 0.5*aux" + coord + "3;" + NL
                    + indent2 + IND + "A" + coord + "[1] = -0.5*aux" + coord + "1 - aux" + coord + "2 + 1.5*aux" + coord + "3;" + NL
                    + indent2 + IND + "A" + coord + "[2] = 1.0 + 0.5*aux" + coord + "2 - 1.5*aux" + coord + "3;" + NL
                    + indent2 + IND + "A" + coord + "[3] = 0.5*aux" + coord + "1 + 0.5*aux" + coord + "3;" + NL
                    + indent2 + "}" + NL
                    + indent2 + "else if (" + coord + "_shft + (1-shft[p" + (i + 1) + "]) == -2) {      // two missing points on the right" + NL
                    + indent2 + IND + "c" + coord + "_shft = -2;" + NL
                    + indent2 + IND + "A" + coord + "[0] = 0.5*aux" + coord + "2 - 0.5*aux" + coord + "3;" + NL
                    + indent2 + IND + "A" + coord + "[1] = 0.5*aux" + coord + "1 - aux" + coord + "2 + 1.5*aux" + coord + "3;" + NL
                    + indent2 + IND + "A" + coord + "[2] = -2.0*aux" + coord + "1 + 0.5*aux" + coord + "2 - 1.5*aux" + coord + "3;" + NL
                    + indent2 + IND + "A" + coord + "[3] = 1.0 + 1.5*aux" + coord + "1 + 0.5*aux" + coord + "3;" + NL
                    + indent2 + "}" + NL
                    + indent2 + "else {                                     // no missing points" + NL
                    + indent2 + IND + "A" + coord + "[0] = -0.5*aux" + coord + "1 + aux" + coord + "2 - 0.5*aux" + coord + "3;" + NL
                    + indent2 + IND + "A" + coord + "[1] = 1 - 2.5*aux" + coord + "2 + 1.5*aux" + coord + "3;" + NL
                    + indent2 + IND + "A" + coord + "[2] = 0.5*aux" + coord + "1 + 2*aux" + coord + "2 - 1.5*aux" + coord + "3;" + NL
                    + indent2 + IND + "A" + coord + "[3] = -0.5*aux" + coord + "2 + 0.5*aux" + coord + "3;" + NL
                    + indent2 + "}" + NL + NL;
            indexes = indexes + indent2 + "int " + coord + "0 = shft[p" + (i + 1) + "] + c" + coord + "_shft - " + coord + "_shft + 0;" + NL
                    + indent2 + "int " + coord + "1 = shft[p" + (i + 1) + "] + c" + coord + "_shft - " + coord + "_shft + 1;" + NL
                    + indent2 + "int " + coord + "2 = shft[p" + (i + 1) + "] + c" + coord + "_shft - " + coord + "_shft + 2;" + NL
                    + indent2 + "int " + coord + "3 = shft[p" + (i + 1) + "] + c" + coord + "_shft - " + coord + "_shft + 3;" + NL;
        }
        
        String fieldDec = "";
        String fieldInit = "";
        String fieldMapping = "";
        ArrayList<String> elements = new ArrayList<String>();
        for (int i = 0; i < 4; i++) {
            elements.add(String.valueOf(i));
        }
        ArrayList<String> combinations = CodeGeneratorUtils.createCombinations(pi.getDimensions(), elements);  
        ArrayList<String> fields = CodeGeneratorUtils.getFields(pi.getProblem());
        for (int k = 0; k < fields.size(); k++) {
            String field = SAMRAIUtils.variableNormalize(fields.get(k));
            fieldDec = fieldDec + indent + IND + "double " + field + "tmp" + arrayDec + ";" + NL;
            fieldInit = fieldInit + indent2 + field + "tmp" + arrayIndex + " = vector(" + field + ", " + vShfIndex + ");" + NL;
            String sum = "";
            for (int i = 0; i < combinations.size(); i++) {
                String comb = combinations.get(i);
                String coefAcc = "";
                String aIndex = "";
                for (int j = 0; j < pi.getDimensions(); j++) {
                    String coord = pi.getCoordinates().get(j);
                    coefAcc = coefAcc + "A" + coord + "[" + comb.substring(j, j + 1) + "] * ";
                    aIndex = aIndex + "[" + coord + comb.substring(j, j + 1) + "]";
                }
                sum = sum + coefAcc + field + "tmp" + aIndex + " + ";
            }
            sum = sum.substring(0, sum.lastIndexOf(" + "));
            fieldMapping = fieldMapping + indent2 + "vectorP_r(" + field + ", sop, " + pIndex + ") = " + sum + ";" + NL;
        }
        return loopInit
                + indent + "if (vector(soporte, " + vectorIndex + ") == 1) {" + NL
                + indent + IND + "hier::Index idx(" + index + ");" + NL
                + indent + IND + "Interphase* sop = interphase->getItem(idx);" + NL + NL
                + pos
                + fieldDec
                + tmpLoopInit
                + fieldInit
                + loopEnd(pi.getDimensions(), indent + IND)
                + pLoopInit(pi.getCoordinates(), indent + IND, false, false)
                + coefs
                + indexes
                + fieldMapping
                + loopEnd(pi.getDimensions(), indent + IND)
                + indent + "}" + NL
                + loopEnd(pi.getDimensions(), ind);
    }
    
    /**
     * Creates the code to do the remeshing. 
     * @param pi            The problem information
     * @param ind           The indent
     * @return              The code
     * @throws CGException  CG00X External error
     */
    private static String remeshing(ProblemInfo pi, String ind) throws CGException {
        String index = "";
        String vectorIndex = "";
        for (int i = 0; i < pi.getDimensions(); i++) {
            String coord = pi.getCoordinates().get(i);
            index = index + coord + ", ";
            vectorIndex = vectorIndex + coord + " + boxfirst1(" + i + "), ";
        }
        index = index.substring(0, index.length() - 2);
        vectorIndex = vectorIndex.substring(0, vectorIndex.length() - 2);
        
        String remesh = "";
        // -----------------------------------------------------------------------------------------------------------------
        // OPTION A: When velocity is computed from external x3d -- A new LS mapping, besides the remeshing itself, is done
        // -----------------------------------------------------------------------------------------------------------------
        if (CodeGeneratorUtils.find(pi.getProblem(), "//mms:movement/mms:type[text() = 'Fixed']").getLength() > 0) {
            remesh = remesh + remeshFromX3D(pi, ind + IND + IND);
        }
        // ---------------------------------------------------------------------------------
        // OPTION B: When velocity is computed from evolution fields -- A remeshing is done
        // ---------------------------------------------------------------------------------
        if (CodeGeneratorUtils.find(pi.getProblem(), "//mms:movement/mms:type[text() = 'Equations']").getLength() > 0) {
            remesh = remesh + remeshFromFields(pi, ind + IND + IND);
        }
        
        return ind + "//Remeshing and projection" + NL
                + ind + "for (int ln = 0; ln <= d_patch_hierarchy->getFinestLevelNumber(); ++ln) {" + NL
                + ind + IND + "std::shared_ptr<hier::PatchLevel > level(d_patch_hierarchy->getPatchLevel(ln));" + NL
                + ind + IND + "//Fill ghosts and periodical boundaries, but no other boundaries" + NL
                + ind + IND + "d_mapping_fill->createSchedule(level, level)->fillData(time, true);" + NL
                + ind + IND + "for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {" + NL
                + ind + IND + IND + "const std::shared_ptr<hier::Patch >& patch = *p_it;" + NL
                + ind + IND + IND + "std::shared_ptr< pdat::IndexData<Interphase, pdat::CellGeometry > >" 
                + " interphase(patch->getPatchData(d_interphase_id), SAMRAI_SHARED_PTR_CAST_TAG);" + NL
                + lsDeclaration(pi, ind + IND + IND)
                + ind + IND + IND + "int* soporte = ((pdat::CellData<int> *) patch->getPatchData(d_soporte_id).get())" 
                + "->getPointer();" + NL
                + ind + IND + IND + "const hier::Index boxfirst = patch->getBox().lower();" + NL
                + ind + IND + IND + "const hier::Index boxlast  = patch->getBox().upper();" + NL
                + ind + IND + IND + "const hier::Index boxfirst1 = interphase->getGhostBox().lower();" + NL
                + ind + IND + IND + "const hier::Index boxlast1  = interphase->getGhostBox().upper();" + NL + NL
                + ind + IND + IND + "//Get delta spaces into an array. dx, dy, dz." + NL
                + ind + IND + IND + "const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));" + NL
                + ind + IND + IND + "const double* dx  = patch_geom->getDx();" + NL + NL
                + SAMRAIUtils.getLasts(pi, ind + IND + IND, true) + NL
                + createThresholds(pi, ind + IND + IND)
                + ind + IND + IND + "//Check position" + NL
                + checkPosition(pi, ind + IND + IND)
                + remesh 
                + ind + IND + "}" + NL
                + ind + "}" + NL;
    }
    
    /**
     * Creates the code to set the axis thresholds.
     * @param pi            The problem info
     * @param ind           The indent
     * @return              The code
     * @throws CGException  CG00X External error
     */
    private static String createThresholds(ProblemInfo pi, String ind) throws CGException {
        String thresholds = "";
        String max = "";
        for (int i = 0; i < pi.getDimensions(); i++) {
            if (i == 0) {
                max = max + "dx[" + i + "]";
            }
            else {
                max = "max<double>(" + max + ", dx[" + i + "])";
            }
        }
        for (int i = 0; i < pi.getDimensions(); i++) {
            String coord = pi.getCoordinates().get(i);
            String contCoord = CodeGeneratorUtils.discToCont(pi.getProblem(), coord);
            thresholds = thresholds + ind + "int threshold" + contCoord + " = ceil(corridor_width*" + max + "/dx[" 
                    + i + "]);" + NL;
        }
        return thresholds;
    }
    
    /**
     * Creates the code to add and remove the particle support.
     * @param pi            The Problem information
     * @param ind           The indent
     * @return              The code
     * @throws CGException  CG00X External error
     */
    private static String addRemoveParticleSupport(ProblemInfo pi, String ind) throws CGException {
        String[][] xslParams = new String [THREE][2];
        xslParams[0][0] = "condition";
        xslParams[0][1] = "true";
        xslParams[1][0] = "indent";
        xslParams[1][1] = "0";
        xslParams[2][0] =  "dimensions";
        xslParams[2][1] =  String.valueOf(pi.getCoordinates().size());
        String actualInd = ind + IND + IND;
        String actualInd2 = ind + IND + IND + IND;
        String vectorIndex = "";
        String vectorIndex1 = "";
        String index = "";
        String index1 = "";
        String pIndex = "";
        String neighbours = "";
        for (int i = 0; i < pi.getDimensions(); i++) {
            String coord = pi.getCoordinates().get(i);
            actualInd = actualInd + IND;
            actualInd2 = actualInd2 + IND + IND;
            vectorIndex = vectorIndex + coord + " - boxfirst1(" + i + "), ";
            vectorIndex1 = vectorIndex1 + coord + "1 - boxfirst1(" + i + "), ";
            index = index + coord + ", ";
            index1 = index1 + coord + "1, ";
            pIndex = pIndex + "p" + (i + 1) + ", ";
            neighbours = neighbours + coord + "1 >= boxfirst1(" + i + ") && " + coord + "1 <= boxlast1(" + i + ")  && ";
        }
        String loopInit = "";
        String loopEnd = "";
        String actualInd3 = actualInd + IND + IND + IND;
        String actualInd4 = actualInd + IND + IND + IND;
        for (int i = pi.getDimensions() - 1; i >= 0; i--) {
            String coord = pi.getCoordinates().get(i);
            loopInit = loopInit + actualInd3 + "for (int " + coord + "1 = " + coord + " - 1; " + coord + "1 <= " + coord + " + 1; " + coord
                    + "1++) {" + NL;
            loopEnd = actualInd3 + "}" + NL + loopEnd;
            actualInd3 = actualInd3 + IND;
            actualInd4 = actualInd4 + IND + IND;
        }
        vectorIndex = vectorIndex.substring(0, vectorIndex.length() - 2);
        vectorIndex1 = vectorIndex1.substring(0, vectorIndex1.length() - 2);
        index = index.substring(0, index.length() - 2);
        index1 = index1.substring(0, index1.length() - 2);
        pIndex = pIndex.substring(0, pIndex.length() - 2);
        
        String levelSetMaxThresholdCondition = "";
        String levelSetMinThresholdCondition = "";
        String lsSigns = "";
        String cellLs = "";
        String ls = "";
        String lsSignCalculation = "";
        String id = "";
        for (int i = 0; i < pi.getInteriorRegionIds().size(); i++) {
            id = pi.getInteriorRegionIds().get(i);
            levelSetMaxThresholdCondition = levelSetMaxThresholdCondition + "lessThan(fabs(vectorP(ls_" + id + ", " + vectorIndex + ", " 
                    + pIndex + ")), maxThreshold) || ";
            lsSigns = lsSigns + actualInd + IND + IND + "int signLS_" + id + " = 1;" + NL;
            levelSetMinThresholdCondition = levelSetMinThresholdCondition + "lessThan(fabs(vectorP(ls_" + id + ", " + vectorIndex + ", " 
                    + pIndex + ")), minThreshold) || ";
            lsSignCalculation = lsSignCalculation + actualInd2 + IND + IND + "signLS_" + id + " = vectorP(ls_" + id + ", " + vectorIndex + ", " 
                    + pIndex + ") > 0 ? 1 : -1;" + NL;
            ls = ls + actualInd4 + IND + "vectorP(ls_" + id + ", " + vectorIndex1 + ", " + pIndex + ") = signLS_" + id + " * 99;" + NL;
            cellLs = cellLs + actualInd3 + IND + "vector(LS_" + id + ", " + vectorIndex1 + ") = signLS_" + id + " * 99;" + NL;
        }
        levelSetMaxThresholdCondition = levelSetMaxThresholdCondition + "fabs(vectorP(ls_" + id + ", " + vectorIndex + ", " + pIndex + ")) > 98";
        levelSetMinThresholdCondition = levelSetMinThresholdCondition.substring(0, levelSetMinThresholdCondition.lastIndexOf(" ||"));
        
        String position = "";
        for (int i = 0; i < pi.getDimensions(); i++) {
            String coord = pi.getCoordinates().get(i);
            position = position + actualInd4 + IND + "vectorP_r(position" + coord + ", inter, " + pIndex + ") = (" + coord + "1 + shift" + coord 
                    + ") * dx[" + i + "] + d_grid_geometry->getXLower()[" + i + "];" + NL;
        }
        return ind + "//Add/remove particle support" + NL
                + ind + "for (int ln = 0; ln <= d_patch_hierarchy->getFinestLevelNumber(); ++ln) {" + NL
                + ind + IND + "std::shared_ptr<hier::PatchLevel > level(d_patch_hierarchy->getPatchLevel(ln));" + NL
                + ind + IND + "//Fill ghosts and periodical boundaries, but no other boundaries" + NL
                + ind + IND + "d_mapping_fill->createSchedule(level, level)->fillData(time, true);" + NL
                + ind + IND + "for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {" + NL
                + ind + IND + IND + "const std::shared_ptr<hier::Patch >& patch = *p_it;" + NL
                + ind + IND + IND + "std::shared_ptr< pdat::IndexData<Interphase, pdat::CellGeometry > >"
                + " interphase(patch->getPatchData(d_interphase_id), SAMRAI_SHARED_PTR_CAST_TAG);" + NL
                + lsDeclaration(pi, ind + IND + IND)
                + SAMRAIUtils.createFOVDeclaration(pi.getRegionIds(), ind + IND + IND, "patch->")
                + lsNormalDeclaration(pi, ind + IND + IND, false, false)
                + ind + IND + IND + "int* soporte = ((pdat::CellData<int> *) patch->getPatchData(d_soporte_id).get())"
                + "->getPointer();" + NL
                + ind + IND + IND + "const hier::Index boxfirst = patch->getBox().lower();" + NL
                + ind + IND + IND + "const hier::Index boxlast  = patch->getBox().upper();" + NL
                + ind + IND + IND + "const hier::Index boxfirst1 = interphase->getGhostBox().lower();" + NL
                + ind + IND + IND + "const hier::Index boxlast1  = interphase->getGhostBox().upper();" + NL + NL
                + ind + IND + IND + "//Get delta spaces into an array. dx, dy, dz." + NL
                + ind + IND + IND + "const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));" + NL
                + ind + IND + IND + "const double* dx  = patch_geom->getDx();" + NL + NL
                + SAMRAIUtils.getLasts(pi, ind + IND + IND, true)
                + getMaxMinThresholds(pi.getDimensions(), ind + IND + IND)
                + loopInit(pi.getCoordinates(), ind + IND + IND, false)
                + actualInd + "if (vector(soporte, " + vectorIndex + ") == 1) {" + NL
                + actualInd + IND + "hier::Index idx(" + index + ");" + NL
                + actualInd + IND + "Interphase* sop = interphase->getItem(idx);" + NL
                + actualInd + IND + "//Removing support" + NL
                + actualInd + IND + "bool remove = true;" + NL + NL
                + pLoopInit(pi.getCoordinates(), actualInd + IND, false, false)
                + actualInd2 + "if (" + levelSetMaxThresholdCondition + ") {" + NL
                + actualInd2 + IND + "remove = false;" + NL
                + actualInd2 + "}" + NL
                + loopEnd(pi.getDimensions(), actualInd + IND)
                + actualInd + IND + "if (remove) {" + NL
                + actualInd + IND + IND + "interphase->removeItem(idx);" + NL
                + actualInd + IND + IND + "vector(soporte, " + vectorIndex + ") = 0;" + NL
                + actualInd + IND + "} else {" + NL
                + actualInd + IND + IND + "//Adding support" + NL
                + actualInd + IND + IND + "bool add = false;" + NL
                + lsSigns
                + pLoopInit(pi.getCoordinates(), actualInd + IND + IND, false, false)
                + actualInd2 + IND + "if (" + levelSetMinThresholdCondition + ") {" + NL
                + actualInd2 + IND + IND + "add = true;" + NL
                + lsSignCalculation
                + actualInd2 + IND + "}" + NL
                + loopEnd(pi.getDimensions(), actualInd + IND + IND)
                + actualInd + IND + IND + "if (add) {" + NL
                + loopInit
                + actualInd3 + "if (" + neighbours + "vector(soporte, " + vectorIndex1 + ") != 1) {" + NL
                + actualInd3 + IND + "hier::Index idx1(" + index1 + ");" + NL + NL
                + actualInd3 + IND + "Interphase* inter = new Interphase();" + NL
                + actualInd3 + IND + "interphase->addItemPointer(idx1, inter);" + NL
                + actualInd3 + IND + "vector(soporte, " + vectorIndex1 + ") = 1;" + NL + NL
                + pLoopInit(pi.getCoordinates(), actualInd3 + IND, true, false)
                + position
                + ls
                + loopEnd(pi.getDimensions(), actualInd3 + IND)
                + cellLs
                + actualInd3 + "}" + NL
                + loopEnd
                + actualInd + IND + IND + "}" + NL
                + actualInd + IND + "}" + NL
                + actualInd + "}" + NL
                + loopEnd(pi.getDimensions(), ind + IND + IND)
                + ind + IND + "}" + NL
                + ind + "}" + NL;
    }
    
    /**
     * Creates the declaration for the region movement function.
     * @param pi            The problem information
     * @return              The code generated
     * @throws CGException  CG00X External error
     */
    private static String createDeclaration(ProblemInfo pi) throws CGException {
        String parameters = "";
        ArrayList<String> fields = CodeGeneratorUtils.getFields(pi.getProblem());
        for (int i = 0; i < fields.size(); i++) {
            parameters = parameters + "int " + SAMRAIUtils.variableNormalize(fields.get(i)) + "_id, ";
        }
        return IND + "/*" + NL
            + IND + " * Perform the region movement" + NL
            + IND + " */" + NL
            + IND + "void interphaseMovement(const double time, " + parameters + "double dt, int* remesh);";
    }
    
    /**
     * Creates the initial loop clauses.
     * @param coords    The coordinates
     * @param baseInd   The indentation
     * @param boxfirst  If the loop is for boxfirst
     * @return          The code
     */
    private static String loopInit(ArrayList<String> coords, String baseInd, boolean boxfirst) {
        String loop = "";
        String actualInd = baseInd;
        String one = "";
        if (!boxfirst) {
            one = "1";
        }
        for (int i = coords.size() - 1; i >= 0; i--) {
            String coord = coords.get(i);
            loop = loop + actualInd + "for (int " + coord + " = boxfirst" + one + "(" + i + "); " + coord + " <= boxlast" + one + "(" + i + "); " 
                    + coord + "++) {" + NL;
            actualInd = actualInd + IND;
        }
        
        return loop;
    }
    
    
    /**
     * Creates the initial loop clauses.
     * @param coords        The coordinates
     * @param baseInd       The indentation
     * @param coordlimit    The coordinate for the boundary loop
     * @param lower         If the boundary is lower
     * @return              The code
     */
    private static String loopInitBound(ArrayList<String> coords, String baseInd, int coordlimit, boolean lower) {
        String loop = "";
        String actualInd = baseInd;
        for (int i = coords.size() - 1; i >= 0; i--) {
            String coord = coords.get(i);
            if (i == coordlimit) {
                if (lower) {
                    loop = loop + actualInd + "for (int " + coord + " = boxfirst1(" + i + "); " + coord + " < boxfirst(" + i + "); " 
                            + coord + "++) {" + NL;
                }
                else {
                    loop = loop + actualInd + "for (int " + coord + " = boxlast(" + i + ") + 1; " + coord + " <= boxlast1(" + i + "); " 
                            + coord + "++) {" + NL;
                }
            }
            else {
                loop = loop + actualInd + "for (int " + coord + " = boxfirst1(" + i + "); " + coord + " <= boxlast1(" + i + "); " 
                        + coord + "++) {" + NL;
            }
            actualInd = actualInd + IND;
        }
        
        return loop;
    }
    
    /**
     * Creates the initial loop clauses.
     * @param coords    The coordinates
     * @param baseInd   The indentation
     * @return          The code
     */
    private static String loopInitComplete(ArrayList<String> coords, String baseInd) {
        String loop = "";
        String actualInd = baseInd;
        for (int i = coords.size() - 1; i >= 0; i--) {
            String coord = coords.get(i);
            loop = loop + actualInd + "for (int " + coord + " = 0; " + coord + " < " + coord + "last; " + coord + "++) {" + NL;
            actualInd = actualInd + IND;
        }
        
        return loop;
    }
    
    /**
     * Creates the initial loop clauses.
     * @param problem       The problem
     * @param coords        The coordinates
     * @param baseInd       The indentation
     * @return              The code
     * @throws CGException 
     */
    private static String loopInitThreshold(Document problem, ArrayList<String> coords, String baseInd) throws CGException {
        String loop = "";
        String actualInd = baseInd;
        for (int i = coords.size() - 1; i >= 0; i--) {
            String coord = coords.get(i);
            String contCoord = CodeGeneratorUtils.discToCont(problem, coord);
            loop = loop + actualInd + "for (int " + coord + "1 = " + coord + " - threshold" + contCoord + "; " + coord + "1 <= " 
                    + coord + " + threshold" + contCoord + "; " + coord + "1++) {" + NL;
            actualInd = actualInd + IND;
        }
        
        return loop;
    }
    
    /**
     * Creates the initial loop clauses with an operation on a determined coordinate.
     * @param problem       The problem
     * @param coords        The coordinates
     * @param baseInd       The indentation
     * @param index         The index of the coordinate to perform the operation
     * @param op            The operation to perform
     * @return              The code
     * @throws CGException 
     */
    private static String loopInitThresholdBound(Document problem, ArrayList<String> coords, String baseInd, int index, String op)
            throws CGException {
        String loop = "";
        String actualInd = baseInd;
        for (int i = coords.size() - 1; i >= 0; i--) {
            String coord = coords.get(i);
            String contCoord = CodeGeneratorUtils.discToCont(problem, coord);
            if (i == index) {
                loop = loop + actualInd + "for (int " + coord + "1 = " + coord + " " + op + " 1; " + coord + "1 <= " 
                        + coord + " + threshold" + contCoord + "; " + coord + "1++) {" + NL;
            }
            else {
                loop = loop + actualInd + "for (int " + coord + "1 = " + coord + " - threshold" + contCoord + "; " + coord + "1 <= " 
                        + coord + " + threshold" + contCoord + "; " + coord + "1++) {" + NL;
            }
            actualInd = actualInd + IND;
        }
        
        return loop;
    }
        
    /**
     * Creates the initial loop clauses.
     * @param coords    The coordinates
     * @param baseInd   The indentation
     * @param withIndex If the cell index have to be generated
     * @param shifts    If the shifts have to be generated
     * @param cond      The condition
     * @return          The code
     */
    private static String loopInitPart(ArrayList<String> coords, String baseInd, boolean withIndex, boolean shifts, String cond) {
        String loop = "";
        String actualInd = baseInd;
        for (int i = coords.size() - 1; i >= 0; i--) {
            loop = loop + actualInd + "for (int p" + (i + 1) + " = 0; p" + (i + 1) + " < 3" + cond + "; p" + (i + 1) + "++) {" + NL;
            if (withIndex) {
                loop = loop + actualInd + IND + "int index" + (i + 1) + " = (" + coords.get(i) + " + 1 - boxfirst1(" + i + ")) * 3 + p"
                        + (i + 1) + ";" + NL;
            }
            if (shifts) {
                loop = loop + actualInd + IND + "double shift" + coords.get(i) + " = p" + (i + 1) + " * 1.0 / 3.0 + 1.0 / 6.0;" + NL;
            }
            actualInd = actualInd + IND;
        }
        
        return loop;
    }
    /**
     * Creates the initial loop clauses.
     * @param coords    The coordinates
     * @param baseInd   The indentation
     * @param val       The value to subtract
     * @param cond      The condition
     * @return          The code
     */
    private static String loopInitNested(ArrayList<String> coords, String baseInd, int val, String cond) {
        String loop = "";
        String actualInd = baseInd;
        for (int i = coords.size() - 1; i >= 0; i--) {
            String coord = coords.get(i);
            loop = loop + actualInd + "for (int " + coord + "1 = " + coord + " - " + val + "; " + coord + "1 <= " + coord + " + " + val + cond + "; " 
                    + coord + "1++) {" + NL;
            actualInd = actualInd + IND;
        }
        return loop;
    }
    
    /**
     * Creates the initial loop clauses.
     * @param coords    The coordinates
     * @param baseInd   The indentation
     * @return          The code
     */
    private static String loopInitNestedMin(ArrayList<String> coords, String baseInd) {
        String loop = "";
        String actualInd = baseInd;
        for (int i = coords.size() - 1; i >= 0; i--) {
            String coord = coords.get(i);
            loop = loop + actualInd + "for (int " + coord + "1 = " + coord + "Min; " + coord + "1 <= " + coord + "Max; " 
                    + coord + "1++) {" + NL;
            actualInd = actualInd + IND;
        }
        return loop;
    }
    
    /**
     * Create the initial loop clauses using thresholds.
     * @param coords    The coordinates
     * @param baseInd   The indentation
     * @return          The code
     */
    private static String loopInitLasts(ArrayList<String> coords, String baseInd) {
        String loop = "";
        String actualInd = baseInd;
        for (int i = coords.size() - 1; i >= 0; i--) {
            String coord = coords.get(i);
            loop = loop + actualInd + "for (int " + coord + " = 0; " + coord + " < " + coord + "last; " + coord + "++) {" + NL;
            actualInd = actualInd + IND;
        }
        
        return loop;
    }
    /**
     * Create the initial loop clauses without ghost points.
     * @param coords    The coordinates
     * @param baseInd   The indentation
     * @return          The code
     */
    private static String loopInitNoGhosts(ArrayList<String> coords, String baseInd) {
        String loop = "";
        String actualInd = baseInd;
        for (int i = coords.size() - 1; i >= 0; i--) {
            String coord = coords.get(i);
            loop = loop + actualInd + "for (int " + coord + " = d_ghost_width; " + coord + " < " + coord + "last - d_ghost_width; "
                    + coord + "++) {" + NL;
            actualInd = actualInd + IND;
        }
        
        return loop;
    }
    
    /**
     * Creates the initial loop clauses for the particles.
     * @param coords        The coordinates
     * @param baseInd       The indent
     * @param withShifts    If the shift variables must be created
     * @param withIndex     If the index variables must be created
     * @return              The code
     */
    private static String pLoopInit(ArrayList<String> coords, String baseInd, boolean withShifts, boolean withIndex) {
        String loop = "";
        String actualInd = baseInd;
        for (int i = coords.size() - 1; i >= 0; i--) {
            String coord = coords.get(i);
            loop = loop + actualInd + "for (int p" + (i + 1) + " = 0; p" + (i + 1)  + " < " + INTERPHASELENGTH + "; p" + (i + 1)  + "++) {" + NL;
            if (withShifts) {
                loop = loop + actualInd + IND + "double shift" + coord + " = p" + (i + 1)  + " * 1.0/" + INTERPHASELENGTH + ".0 + 1.0/" 
                        + (INTERPHASELENGTH * 2) + ".0;" + NL;
            }
            if (withIndex) {
                loop = loop + actualInd + IND + "int index" + (i + 1) + " = (" + coord + " + 1 - boxfirst1(" + i + ")) * " + INTERPHASELENGTH 
                        + " + p" + (i + 1)  + ";" + NL;
            }
            actualInd = actualInd + IND;
        }
        
        return loop;
    }
    
    /**
     * Creates the closing loop clauses.
     * @param dims          The number of coordinates
     * @param baseInd       The indent
     * @return              The code
     */
    private static String loopEnd(int dims, String baseInd) {
        String loop = "";
        String actualInd = baseInd;
        for (int i = dims - 1; i >= 0; i--) {
            loop = actualInd + "}" + NL + loop;
            actualInd = actualInd + IND;
        }
        
        return loop;
    }
    
    /**
     * Create the code to check and correct the position of the interphase particles in case of periodical boundaries.
     * @param pi            The problem info
     * @param indent        The indent
     * @return              The code
     * @throws CGException  CG00X External error
     */
    private static String checkPosition(ProblemInfo pi, String indent) throws CGException {
        String indent1 = indent;
        String indent2 = indent;
        String index = "";
        String pIndex = "";
        String vectorIndex = "";
        for (int i = 0; i < pi.getDimensions(); i++) {
            String coord = pi.getCoordinates().get(i);
            indent1 = indent1 + IND;
            indent2 = indent2 + IND + IND;
            index = index + coord + ", ";
            pIndex = pIndex + "p" + (i + 1) + ", ";
            vectorIndex = vectorIndex + coord + " - boxfirst1(" + i + "), ";
        }
        index = index.substring(0, index.length() - 2);
        pIndex = pIndex.substring(0, pIndex.length() - 2);
        vectorIndex = vectorIndex.substring(0, vectorIndex.length() - 2);
        String checkPosition = "";
        for (int i = 0; i < pi.getDimensions(); i++) {
            String coord = pi.getCoordinates().get(i);
            String contCoord = CodeGeneratorUtils.discToCont(pi.getProblem(), coord);
            checkPosition = checkPosition + loopInitBound(pi.getCoordinates(), indent, i, true)
                    + indent1 + "if (vector(soporte, " + vectorIndex + ") == 1) {" + NL
                    + indent1 + IND + "hier::Index idx(" + index + ");" + NL
                    + indent1 + IND + "Interphase* sop = interphase->getItem(idx);" + NL
                    + pLoopInit(pi.getCoordinates(), indent1 + IND, false, false)
                    + indent2 + IND + "if (vectorP_r(position" + coord + ", sop, " + pIndex + ") > " + contCoord + "Gupper) {" + NL
                    + indent2 + IND + IND + "vectorP_r(position" + coord + ", sop, " + pIndex + ") = vectorP_r(position" + coord + ", sop, " + pIndex 
                    + ") - (" + contCoord + "Upper - " + contCoord + "Lower);" + NL
                    + indent2 + IND + "}" + NL
                    + loopEnd(pi.getDimensions(), indent1 + IND)
                    + indent1 + "}" + NL
                    + loopEnd(pi.getDimensions(), indent)
                    + loopInitBound(pi.getCoordinates(), indent, i, false)
                    + indent1 + "if (vector(soporte, " + vectorIndex + ") == 1) {" + NL
                    + indent1 + IND + "hier::Index idx(" + index + ");" + NL
                    + indent1 + IND + "Interphase* sop = interphase->getItem(idx);" + NL
                    + pLoopInit(pi.getCoordinates(), indent1 + IND, false, false)
                    + indent2 + IND + "if (vectorP_r(position" + coord + ", sop, " + pIndex + ") < " + contCoord + "Glower) {" + NL
                    + indent2 + IND + IND + "vectorP_r(position" + coord + ", sop, " + pIndex + ") = vectorP_r(position" + coord + ", sop, " + pIndex
                    + ") + (" + contCoord + "Upper - " + contCoord + "Lower);" + NL
                    + indent2 + IND + "}" + NL
                    + loopEnd(pi.getDimensions(), indent1 + IND)
                    + indent1 + "}" + NL
                    + loopEnd(pi.getDimensions(), indent);
        }
        return checkPosition;
    }
    
    /**
     * Creates the code to allocate and initialize the temporal variables used for performance reasons.
     * @param pi        The problem info
     * @param ind       The indent
     * @param withLS    If the level set variables must be created
     * @return          The code
     */
    private static String tmpMalloc(ProblemInfo pi, String ind, boolean withLS) {
        String code = "";
        String indent = ind;
        
        for (int i = 0; i < pi.getDimensions(); i++) {
            String coord = pi.getCoordinates().get(i);
            String pointerNumber = "";
            String arrayIndex = "";
            for (int j = 0; j < pi.getDimensions() - i; j++) {
                pointerNumber = pointerNumber + "*";
            }
            for (int j = 0; j < i; j++) {
                String coord2 = pi.getCoordinates().get(j);
                arrayIndex = arrayIndex + "[" + coord2 + "]";
            }
            for (int j = 0; j < pi.getDimensions(); j++) {
                String coord2 = pi.getCoordinates().get(j);
                code = code + indent + "position" + coord2 + "_rm" + arrayIndex + " = (double " + pointerNumber + ") malloc((" + coord 
                        + "last + 2) * " + INTERPHASELENGTH + " * sizeof(double " + pointerNumber.substring(1) + "));" + NL
                        + indent + "velocity" + coord2 + "_rm" + arrayIndex + " = (double " + pointerNumber + ") malloc((" + coord + "last + 2) * " 
                        + INTERPHASELENGTH + " * sizeof(double " + pointerNumber.substring(1) + "));" + NL;
            }
            if (withLS) {
                for (int j = 0; j < pi.getInteriorRegionIds().size(); j++) {
                    String id = pi.getInteriorRegionIds().get(j);
                    code = code + indent + "ls_" + id + "_rm" + arrayIndex + " = (double " + pointerNumber + ") malloc((" + coord 
                        + "last + 2) * " + INTERPHASELENGTH + " * sizeof(double " + pointerNumber.substring(1) + "));" + NL;
                }
            }
            code = code + indent + "for(int " + coord + " = 0; " + coord + " < (" + coord + "last + 2) * " + INTERPHASELENGTH + "; " 
                    + coord + "++) {" + NL;
            indent = indent + IND;
            
        }
        String arrayIndex = "";
        for (int j = 0; j < pi.getDimensions(); j++) {
            String coord = pi.getCoordinates().get(j);
            arrayIndex = arrayIndex + "[" + coord + "]";
        }
        for (int j = 0; j < pi.getDimensions(); j++) {
            String coord = pi.getCoordinates().get(j);
            code = code + indent + "position" + coord + "_rm" + arrayIndex + " = 9999;" + NL
                    + indent + "velocity" + coord + "_rm" + arrayIndex + " = 0;" + NL;
        }
        if (withLS) {
            for (int j = 0; j < pi.getInteriorRegionIds().size(); j++) {
                String id = pi.getInteriorRegionIds().get(j);
                code = code + indent + "ls_" + id + "_rm" + arrayIndex + " = 9999;" + NL;
            }
        }
        for (int i = 0; i < pi.getDimensions(); i++) {
            indent = indent.substring(1);  
            code = code + indent + "}" + NL;

        }
        return code;
    }
    
    /**
     * Creates the code to initialize the temporal variables used for performance.
     * @param pi        The problem info
     * @param indent    The indent
     * @param withLS    If the level set variables must be calculated
     * @return          The code
     */
    private static String tmpInitialization(ProblemInfo pi, String indent, boolean withLS) {
        String initTmp = "";
        ArrayList<String> elements = new ArrayList<String>();
        for (int i = 0; i < INTERPHASELENGTH; i++) {
            elements.add(String.valueOf(i));
        }
        ArrayList<String> combinations = CodeGeneratorUtils.createCombinations(pi.getDimensions(), elements);        
        for (int i = 0; i < pi.getDimensions(); i++) {
            String coord = pi.getCoordinates().get(i);
            for (int j = 0; j < combinations.size(); j++) {
                String comb = combinations.get(j);
                String combIndex = "";
                String arrayIndex = "";
                String index = "";
                for (int k = 0; k < comb.length(); k++) {
                    String arrayCoord = pi.getCoordinates().get(k);
                    combIndex = combIndex + comb.substring(k, k + 1) + ", ";
                    arrayIndex = arrayIndex + "[(" + arrayCoord + " + 1) * " + INTERPHASELENGTH + " + " + comb.substring(k, k + 1) + "]";
                    index = index + arrayCoord + ", ";
                }
                combIndex = combIndex.substring(0, combIndex.length() - 2);
                initTmp = initTmp + indent + "position" + coord + "_rm" + arrayIndex + " = vectorP_r(position" + coord + ", sop, " + combIndex 
                        + ");" + NL
                        + indent + "velocity" + coord + "_rm" + arrayIndex + " = vectorP_r(velocity" + coord + ", sop, " + combIndex + ");" + NL;
            }
            if (withLS) {
                for (int j = 0; j < combinations.size(); j++) {
                    String comb = combinations.get(j);
                    String combIndex = "";
                    String arrayIndex = "";
                    String index = "";
                    for (int k = 0; k < comb.length(); k++) {
                        String arrayCoord = pi.getCoordinates().get(k);
                        combIndex = combIndex + comb.substring(k, k + 1) + ", ";
                        arrayIndex = arrayIndex + "[(" + arrayCoord + " + 1) * " + INTERPHASELENGTH + " + " + comb.substring(k, k + 1) + "]";
                        index = index + arrayCoord + ", ";
                    }
                    combIndex = combIndex.substring(0, combIndex.length() - 2);
                    for (int k = 0; k < pi.getInteriorRegionIds().size(); k++) {
                        String id = pi.getInteriorRegionIds().get(k);
                        initTmp = initTmp + indent + "ls_" + id + "_rm" + arrayIndex + " = vectorP(ls_" + id + ", " + index + combIndex + ");" + NL;
                    }
                }
            }
        }
        return initTmp;
    }
    
    /**
     * Creates the code that do the remeshing in case of movement forced by x3d files.
     * @param pi            The problem info
     * @param ind           The indent
     * @return              The code
     * @throws CGException  CG00X External error
     */
    private static String remeshFromX3D(ProblemInfo pi, String ind) throws CGException {
        String[][] xslParams = new String [THREE][2];
        xslParams[0][0] = "condition";
        xslParams[0][1] = "true";
        xslParams[1][0] = "indent";
        xslParams[1][1] = "0";
        xslParams[2][0] =  "dimensions";
        xslParams[2][1] =  String.valueOf(pi.getCoordinates().size());
        String tmpDeclaration = "";
        String indent = ind;
        String indent2 = ind + IND;
        String index = "";
        String pIndex = "";
        String vectorIndex = "";
        String pointer = "";
        String position = "";
        ArrayList<String> elements = new ArrayList<String>();
        for (int i = 0; i < INTERPHASELENGTH; i++) {
            elements.add(String.valueOf(i));
        }
        ArrayList<String> combinations = CodeGeneratorUtils.createCombinations(pi.getDimensions(), elements); 
        for (int i = pi.getDimensions() - 1; i >= 0; i--) {
            pointer = pointer + "*";
            indent = indent + IND;
            indent2 = indent2 + IND + IND;
            pIndex = "p" + (i + 1) + ", " + pIndex;
        }
        pIndex = pIndex.substring(0, pIndex.length() - 2);
        for (int i = 0; i < pi.getDimensions(); i++) {
            String coord = pi.getCoordinates().get(i);
            tmpDeclaration = tmpDeclaration + ind + "double " + pointer + "position" + coord + "_rm;" + NL
                    + ind + "double " + pointer + "velocity" + coord + "_rm;" + NL;
            index = index + coord + ", ";

            vectorIndex = vectorIndex + coord + " + boxfirst1(" + i + "), ";
            for (int j = 0; j < combinations.size(); j++) {
                String comb = combinations.get(j);
                String combIndex = "";
                String arrayIndex = "";
                for (int k = 0; k < comb.length(); k++) {
                    String arrayCoord = pi.getCoordinates().get(k);
                    combIndex = combIndex + comb.substring(1, k + 1) + ", ";
                    arrayIndex = arrayIndex + "[(" + arrayCoord + " + 1) * " + INTERPHASELENGTH + " + " + comb.substring(1, k + 1) + "]";
                }
                combIndex = combIndex.substring(0, combIndex.length() - 2);
            }
            
            position = position + indent2 + "vectorP_r(position" + coord + ", sop, " + pIndex + ") = (" + coord + " + boxfirst1(" + i 
                    + ") + shift" + coord + ") * dx[" + i + "] + d_grid_geometry->getXLower()[" + i + "];" + NL;
        }
        index = index.substring(0, index.length() - 2);
        vectorIndex = vectorIndex.substring(0, vectorIndex.length() - 2);
        
        String lsInit = "";
        String lsCalculations = "";
        for (int i = 0; i < pi.getRegions().size(); i++) {
            String segName = pi.getRegions().get(i);
            String segId = CodeGeneratorUtils.getRegionId(pi.getProblem(), pi.getRegions(), segName);
            for (int j = 0; j < combinations.size(); j++) {
                String comb = combinations.get(j);
                String combIndex = "";
                for (int k = 0; k < comb.length(); k++) {
                    combIndex = combIndex + comb.substring(k, k + 1) + ", ";
                }
                combIndex = combIndex.substring(0, combIndex.length() - 2);
                lsInit = lsInit + indent + "vectorP(ls_" + segId + ", " + index + ", " + combIndex + ") = 99999999999;" + NL;
            }
        }
        for (int i = 0; i < pi.getMovementInfo().getX3dMovRegions().size(); i++) {
            String segName = pi.getMovementInfo().getX3dMovRegions().get(i);
            lsCalculations = lsCalculations + lsCalculation(pi, ind, segName, true);
        }
        return tmpDeclaration
              + tmpMalloc(pi, ind, false)
              + loopInitLasts(pi.getCoordinates(), ind)
              + lsInit
              + indent + "if (vector(soporte, " + index + ") == 1) {" + NL
              + indent + IND + "hier::Index idx(" + vectorIndex + ");" + NL
              + indent + IND + "Interphase* sop = interphase->getItem(idx);" + NL
              + tmpInitialization(pi, indent + IND, false)
              + pLoopInit(pi.getCoordinates(), indent + IND, true, false)
              + position
              + loopEnd(pi.getDimensions(), indent + IND)
              + indent + "}" + NL
              + loopEnd(pi.getDimensions(), ind)
              + ind + "int minBlock[" + pi.getDimensions() + "], maxBlock[" + pi.getDimensions() + "];" + NL
              + lsCalculations
              + addBoundaryParticleSupport(pi, ind)
              + remeshVelocities(pi, ind)
              + ind + "//Free temporal arrays" + NL
              + tmpFree(pi, ind, false);
    }
    
    /**
     * Creates the code to perform the level set calculation.
     * @param pi                The problem info
     * @param ind               The indent
     * @param segName           The region name
     * @param fromRemesh        If the calculation comes from the remesh
     * @return                  The code
     * @throws CGException      CG00X External error
     */
    private static String lsCalculation(ProblemInfo pi, String ind, String segName, boolean fromRemesh) throws CGException {
        String maxMin = "";
        String indexComparator = "";
        String segId = CodeGeneratorUtils.getRegionId(pi.getProblem(), pi.getRegions(), segName);
        String vars = "";
        for (int i = 0; i < pi.getDimensions(); i++) {
            String coord = pi.getCoordinates().get(i);
            maxMin = maxMin + ind + IND + IND + IND + "minBlock[" + i + "] = " + coord + "term;" + NL
                    + ind + IND + IND + IND + "maxBlock[" + i + "] = " + coord + "term;" + NL;
            indexComparator = indexComparator + ind + IND + IND + IND + "if (" + coord + "term < minBlock[" + i + "]) {" + NL
                    + ind + IND + IND + IND + IND + "minBlock[" + i + "] = " + coord + "term;" + NL
                    + ind + IND + IND + IND + "}" + NL
                    + ind + IND + IND + IND + "if (" + coord + "term > maxBlock[" + i + "]) {" + NL
                    + ind + IND + IND + IND + IND + "maxBlock[" + i + "] = " + coord + "term;" + NL
                    + ind + IND + IND + IND + "}" + NL;
            vars = vars + "a" + coord + ", b" + coord + ", ";
            if (pi.getDimensions() == 3) {
                vars = vars + "c" + coord + ", ";
            }
        }
        String lsFirstApproach;
        if (pi.getDimensions() == 2) {
            lsFirstApproach = lsFirstApproach2D(pi, ind + IND + IND, segName, segId, fromRemesh);
        }
        else {
            lsFirstApproach = lsFirstApproach3D(pi, ind + IND + IND, segName, segId, fromRemesh);
        }
        
        vars = vars.substring(0, vars.lastIndexOf(","));
        return ind + "for (int unionsI = 0; unionsI < UNIONS_" + segName + "; unionsI++) {" + NL
                + ind + IND + "double " + vars + ";" + NL
                + ind + IND + "bool validUnion = true;" + NL + NL
                + ind + IND + "for (int facePointI = 0; facePointI < DIMENSIONS; facePointI++) {" + NL
                + getTerms(pi, ind + IND + IND, segName, fromRemesh)
                + unionInDomainCheck(pi, ind + IND + IND)
                + ind + IND + IND + "//Take maximum and minimum coordinates of the face" + NL
                + ind + IND + IND + "if (facePointI == 0) {" + NL
                + maxMin
                + ind + IND + IND + "} else {" + NL
                + indexComparator
                + ind + IND + IND + "}" + NL
                + ind + IND + "}" + NL + NL
                + ind + IND + "if (validUnion) {" + NL
                + lsFirstApproach
                + ind + IND + "}" + NL
                + ind + "}" + NL;
    }
    
    
    /**
     * Creates the code (2D) to calculate the first approach for level set variables.
     * @param pi                The problem info
     * @param ind               The indent
     * @param regionName       The regionName
     * @param lsId              The level set identifier
     * @param fromRemesh        If the calculations come from the remesh 
     * @return                  The code
     * @throws CGException      CG00X External error
     */
    private static String lsFirstApproach2D(ProblemInfo pi, String ind, String regionName, String lsId, boolean fromRemesh) throws CGException {
        String[][] xslParams = new String [THREE][2];
        xslParams[0][0] = "condition";
        xslParams[0][1] = "true";
        xslParams[1][0] = "indent";
        xslParams[1][1] = "0";
        xslParams[2][0] =  "dimensions";
        xslParams[2][1] =  String.valueOf(pi.getCoordinates().size());
        String indent = ind;
        String indent2 = ind + IND + IND;
        String index = "";
        String vectorIndex = "";
        String startEnd = "";
        String cond = "";
        String forStart = "";
        ArrayList<String> elements = new ArrayList<String>();
        for (int i = 0; i < INTERPHASELENGTH; i++) {
            elements.add(String.valueOf(i));
        }
        for (int i = pi.getDimensions() - 1; i >= 0; i--) {
            String coord = pi.getCoordinates().get(i);
            String contCoord = CodeGeneratorUtils.discToCont(pi.getProblem(), coord);
            forStart = forStart + indent + IND + "for (int " + coord + " = start" + contCoord + "; " + coord + " <= end" + contCoord 
                    + "; " + coord + "++) {" + NL;
            indent = indent + IND;
            indent2 = indent2 + IND + IND;
        }
        String modab = "";
        for (int i = 0; i < pi.getDimensions(); i++) {
            String coord = pi.getCoordinates().get(i);
            String contCoord = CodeGeneratorUtils.discToCont(pi.getProblem(), coord);
            index = index + coord + ", ";
            vectorIndex = vectorIndex + coord + " - boxfirst1(" + i + "), ";
            startEnd = startEnd + ind + "int start" + contCoord + " = minBlock[" + i + "] - threshold" + contCoord + ";" + NL
                    + ind + "int end" + contCoord + " = maxBlock[" + i + "] + threshold" + contCoord + ";" + NL
                    + ind + "if (start" + contCoord + " < boxfirst(" + i + ")) {" + NL
                    + ind + IND + "start" + contCoord + " = boxfirst(" + i + ");" + NL
                    + ind + "}" + NL
                    + ind + "if (end" + contCoord + " > boxlast(" + i + ")) {" + NL
                    + ind + IND + "end" + contCoord + " = boxlast(" + i + ");" + NL
                    + ind + "}" + NL;
            cond = cond + "start" + contCoord + " <= boxlast(" + i + ") + d_ghost_width && end" + contCoord + " >= boxfirst(" + i 
                    + ") - d_ghost_width && ";
            
            modab = modab + "(a" + coord + " - b" + coord + ") * (a" + coord + " - b" + coord + ") + ";
        }
        modab = modab.substring(0, modab.lastIndexOf(" + "));
        index = index.substring(0, index.length() - 2);
        vectorIndex = vectorIndex.substring(0, vectorIndex.length() - 2);
        
        String accs = "";
        String accsAssign = "";
        if (pi.getInteriorRegionIds().size() >= 3 && !fromRemesh) {
            for (int i = 0; i < pi.getInteriorRegionIds().size(); i++) {
                String id = pi.getInteriorRegionIds().get(i);
                accs = accs + ind + IND + "int acc_" + id + " = 0;" + NL;
                accsAssign = accsAssign + indent + IND + "acc_" + id + " = acc_" + id + " + vector(FOV_" + id + " ," + vectorIndex + ");" + NL;
            }
        }
        
        return ind + "double modab = sqrt(" + modab + ");" + NL
                + startEnd
                + ind + "if (" + cond + "lessThan(0, modab)) {" + NL
                + accs
                + forStart
                + accsAssign
                + indent + IND + "if (vector(soporte, " + vectorIndex + ") == 1) {" + NL
                + indent + IND + IND + "hier::Index idx(" + index + ");" + NL
                + indent + IND + IND + "Interphase* sop = interphase->getItem(idx);" + NL
                + pLoopInit(pi.getCoordinates(), indent + IND + IND, false, false)
                + lsMappingCalculation2D(pi, indent2, regionName, lsId, fromRemesh)
                + loopEnd(pi.getDimensions(), indent + IND + IND)
                + indent + IND + "}" + NL
                + loopEnd(pi.getDimensions(), ind + IND)
                + ind + "}" + NL;
    }
   
    /**
     * Creates the code (2D) to calculate the first approach for level set variables.
     * @param pi                The problem info
     * @param ind               The indent
     * @param regionName       The regionName
     * @param lsId              The level set identifier
     * @param fromRemesh        If the calculations come from the remesh 
     * @return                  The code
     * @throws CGException      CG00X External error
     */
    private static String lsFirstApproach3D(ProblemInfo pi, String ind, String regionName, String lsId, boolean fromRemesh) throws CGException {
        String[][] xslParams = new String [THREE][2];
        xslParams[0][0] = "condition";
        xslParams[0][1] = "true";
        xslParams[1][0] = "indent";
        xslParams[1][1] = "0";
        xslParams[2][0] =  "dimensions";
        xslParams[2][1] =  String.valueOf(pi.getCoordinates().size());
        String indent = ind;
        String indent2 = ind + IND + IND;
        String index = "";
        String vectorIndex = "";
        String startEnd = "";
        String cond = "";
        String forStart = "";
        ArrayList<String> elements = new ArrayList<String>();
        for (int i = 0; i < INTERPHASELENGTH; i++) {
            elements.add(String.valueOf(i));
        }
        for (int i = pi.getDimensions() - 1; i >= 0; i--) {
            String coord = pi.getCoordinates().get(i);
            String contCoord = CodeGeneratorUtils.discToCont(pi.getProblem(), coord);
            forStart = forStart + indent + IND + "for (int " + coord + " = start" + contCoord + "; " + coord + " <= end" + contCoord 
                    + "; " + coord + "++) {" + NL;
            indent = indent + IND;
            indent2 = indent2 + IND + IND;
        }
        String origSave = "";
        String origLoad = "";
        for (int i = 0; i < pi.getDimensions(); i++) {
            String coord = pi.getCoordinates().get(i);
            String contCoord = CodeGeneratorUtils.discToCont(pi.getProblem(), coord);
            index = index + coord + ", ";
            vectorIndex = vectorIndex + coord + " - boxfirst1(" + i + "), ";
            startEnd = startEnd + ind + "int start" + contCoord + " = minBlock[" + i + "] - threshold" + contCoord + ";" + NL
                    + ind + "int end" + contCoord + " = maxBlock[" + i + "] + threshold" + contCoord + ";" + NL
                    + ind + "if (start" + contCoord + " < boxfirst(" + i + ")) {" + NL
                    + ind + IND + "start" + contCoord + " = boxfirst(" + i + ");" + NL
                    + ind + "}" + NL
                    + ind + "if (end" + contCoord + " > boxlast(" + i + ")) {" + NL
                    + ind + IND + "end" + contCoord + " = boxlast(" + i + ");" + NL
                    + ind + "}" + NL;
            cond = cond + "start" + contCoord + " <= boxlast(" + i + ") + d_ghost_width && end" + contCoord + " >= boxfirst(" + i 
                    + ") - d_ghost_width && ";
            String varName = "a";
            if (i == 1) {
                varName = "b";
            }
            if (i == 2) {
                varName = "c";
            }
            for (int j = 0; j < pi.getDimensions(); j++) {
                String coord2 = pi.getCoordinates().get(j);
                origSave = origSave + ind + "double orig_" + varName + coord2 + " = " + varName + coord2 + ";" + NL;
                origLoad = origLoad + indent2 + varName + coord2 + " = orig_" + varName + coord2 + ";" + NL;
            }
        }
        index = index.substring(0, index.length() - 2);
        vectorIndex = vectorIndex.substring(0, vectorIndex.length() - 2);
        cond = cond.substring(0, cond.lastIndexOf(" &&"));
        
        
        String accs = "";
        String accsAssign = "";
        if (pi.getInteriorRegionIds().size() >= 3 && !fromRemesh) {
            for (int i = 0; i < pi.getInteriorRegionIds().size(); i++) {
                String id = pi.getInteriorRegionIds().get(i);
                accs = accs + ind + IND + "int acc_" + id + " = 0;" + NL;
                accsAssign = accsAssign + indent + IND + "acc_" + id + " = acc_" + id + " + vector(FOV_" + id + " ," + vectorIndex + ");" + NL;
            }
        }
        
        return startEnd
                + origSave
                + ind + "if (" + cond + ") {" + NL
                + accs
                + forStart
                + accsAssign
                + indent + IND + "if (vector(soporte, " + vectorIndex + ") == 1) {" + NL
                + indent + IND + IND + "hier::Index idx(" + index + ");" + NL
                + indent + IND + IND + "Interphase* sop = interphase->getItem(idx);" + NL
                + pLoopInit(pi.getCoordinates(), indent + IND + IND, false, false)
                + origLoad
                + lsMappingCalculation3D(pi, indent2, regionName, lsId, fromRemesh)
                + loopEnd(pi.getDimensions(), indent + IND + IND)
                + indent + IND + "}" + NL
                + loopEnd(pi.getDimensions(), ind + IND)
                + ind + "}" + NL;
    }
    
    /**
     * Creates the code that do the remeshing in case of movement forced by the fields.
     * @param pi            The problem info
     * @param ind           The indent
     * @return              The code
     * @throws CGException  CG00X External error
     */
    private static String remeshFromFields(ProblemInfo pi, String ind) throws CGException {
        String[][] xslParams = new String [THREE][2];
        xslParams[0][0] = "condition";
        xslParams[0][1] = "true";
        xslParams[1][0] = "indent";
        xslParams[1][1] = "0";
        xslParams[2][0] =  "dimensions";
        xslParams[2][1] =  String.valueOf(pi.getCoordinates().size());
        String indent = ind;
        String indent2 = ind + IND;
        String index = "";
        String pIndex = "";
        String vectorIndex = "";
        String vectorIndexPlus = "";
        String tmpDeclaration = "";
        String pointer = "";
        for (int i = 0; i < pi.getDimensions(); i++) {
            String coord = pi.getCoordinates().get(i);
            pointer = pointer + "*";
            indent = indent + IND;
            indent2 = indent2 + IND + IND;
            index = index + coord + ", ";
            pIndex = pIndex + "p" + (i + 1) + ", ";
            vectorIndex = vectorIndex + coord + " - boxfirst1(" + i + "), ";
            vectorIndexPlus = vectorIndexPlus + coord + " + boxfirst1(" + i + "), ";
        }
        pIndex = pIndex.substring(0, pIndex.length() - 2);
        String position = "";
        for (int i = 0; i < pi.getDimensions(); i++) {
            String coord = pi.getCoordinates().get(i);
            tmpDeclaration = tmpDeclaration + ind + "double " + pointer + "position" + coord + "_rm;" + NL
                    + ind + "double " + pointer + "velocity" + coord + "_rm;" + NL;
            position = position + indent2 + "vectorP_r(position" + coord + ", sopO, " + pIndex + ") = (" + coord + " + shift" + coord 
                    + ") * dx[" + i + "] + d_grid_geometry->getXLower()[" + i + "];" + NL;
        }
        for (int j = 0; j < pi.getInteriorRegionIds().size(); j++) {
            String id = pi.getInteriorRegionIds().get(j);
            tmpDeclaration = tmpDeclaration + ind + "double " + pointer + "ls_" + id + "_rm;" + NL;
        }
        index = index.substring(0, index.length() - 2);
        vectorIndex = vectorIndex.substring(0, vectorIndex.length() - 2);
        vectorIndexPlus = vectorIndexPlus.substring(0, vectorIndexPlus.length() - 2);
        return tmpDeclaration
                + tmpMalloc(pi, ind, true)
                + loopInitLasts(pi.getCoordinates(), ind)
                + indent + "if (vector(soporte, " + index + ") == 1) {" + NL
                + indent + IND + "hier::Index idx(" + vectorIndexPlus + ");" + NL
                + indent + IND + "Interphase* sop = interphase->getItem(idx);" + NL
                + tmpInitialization(pi, indent + IND, true)
                + indent + "}" + NL
                + loopEnd(pi.getDimensions(), ind) + NL
                + loopInit(pi.getCoordinates(), ind, false)
                + indent + "if (vector(soporte, " + vectorIndex + ") == 1) {" + NL
                + indent + IND + "hier::Index idx(" + index + ");" + NL
                + indent + IND + "Interphase* sopO = interphase->getItem(idx);" + NL
                + pLoopInit(pi.getCoordinates(), indent + IND, true, true)
                + calculateKernel(pi, indent2, true)
                + position 
                + lsProjectionMethod(pi, indent2)
                + loopEnd(pi.getDimensions(), indent + IND)
                + ind + IND + "}" + NL
                + loopEnd(pi.getDimensions(), ind) + NL 
                + tmpFree(pi, ind, true);
    }
    
    /**
     * Creates the code to project the level sets.
     * @param pi    The problem info
     * @param ind   The indent
     * @return      The code
     */
    private static String lsProjectionMethod(ProblemInfo pi, String ind) {
        String smallest = "";
        String smallest2 = "";
        String projection = "";
        String vectorIndex = "";
        String pIndex = "";
        for (int i = 0; i < pi.getDimensions(); i++) {
            String coord = pi.getCoordinates().get(i);
            pIndex = pIndex + "p" + (i + 1) + ", ";
            vectorIndex = vectorIndex + coord + " - boxfirst1(" + i + "), ";
        }
        pIndex = pIndex.substring(0, pIndex.length() - 2);
        for (int i = 0; i < pi.getInteriorRegionIds().size(); i++) {
            String id = pi.getInteriorRegionIds().get(i);
            String region = pi.getRegions().get(Integer.parseInt(id) - 1 / 2);
            smallest = smallest + ind + "if (vectorP(ls_" + id + ", " + vectorIndex + pIndex + ") < m1) {" + NL
                    + ind + IND + "m1 = vectorP(ls_" + id + ", " + vectorIndex + pIndex + ");" + NL
                    + ind + "}" + NL;
            smallest2 = smallest2 + ind + "if (vectorP(ls_" + id + ", " + vectorIndex + pIndex + ") < m2 && vectorP(ls_" + id + ", " 
                    + vectorIndex + pIndex + ") > m1) {" + NL
                    + ind + IND + "m2 = vectorP(ls_" + id + ", " + vectorIndex + pIndex + ");" + NL
                    + ind + "}" + NL;
            //The regions moved by x3d haven't to be kernelled.
            if (!pi.getMovementInfo().getX3dMovRegions().contains(region)) {
                projection = projection + ind + "vectorP(ls_" + id + ", " + vectorIndex + pIndex + ") = vectorP(ls_" + id + ", " 
                        + vectorIndex + pIndex + ") - (m1 + m2) / 2.0;" + NL;
            }
        }
        return ind + "double m1 = 999999999999;" + NL
                + ind + "double m2 = 999999999999;" + NL
                + smallest
                + smallest2
                + projection;
    }
    
    /**
     * Creates the instructions to calculate the variables (level sets and velocities) from a kernel.
     * @param pi                The problem info
     * @param ind               The indent
     * @param withLS            If the level set variables must be calculated
     * @return                  The code
     * @throws CGException      CG00X External error
     */
    private static String calculateKernel(ProblemInfo pi, String ind, boolean withLS) throws CGException {
        String[][] xslParams = new String [THREE][2];
        xslParams[0][0] = "condition";
        xslParams[0][1] = "true";
        xslParams[1][0] = "indent";
        xslParams[1][1] = "0";
        xslParams[2][0] =  "dimensions";
        xslParams[2][1] =  String.valueOf(pi.getCoordinates().size());
        String kernTerms = "";
        String kernelAcc = "";
        ArrayList<String> elements = new ArrayList<String>();
        for (int i = -2; i <= 2; i++) {
            if (i >= 0) {
                elements.add("+" + String.valueOf(i));
            }
            else {
                elements.add(String.valueOf(i));
            }
        }
        ArrayList<String> combinations = CodeGeneratorUtils.createCombinations(pi.getDimensions(), elements);
        for (int i = 0; i < combinations.size(); i++) {
            String comb = combinations.get(i);
            String combId = comb.replaceAll("-", "m").replaceAll("\\+", "p");
            kernTerms = kernTerms + ind + "double kern" + combId + " = ";
            String index = "";
            for (int j = 0; j < comb.length() / 2; j++) {
                String op = comb.substring(j * 2, j * 2 + 1);
                String quantity = "";
                if (!comb.substring(j * 2 + 1, j * 2 + 2).equals("0")) {
                    quantity = comb.substring(j * 2 + 1, j * 2 + 2);
                } 
                else {
                    op = "";
                }
                index = index + "[index" + (j + 1) + op + quantity + "]";
            }
            for (int j = 0; j < pi.getDimensions(); j++) {
                String coord = pi.getCoordinates().get(j);
                kernTerms = kernTerms + "kern(fabs((" + coord + " + shift" + coord + ") * dx[" + j 
                        + "] + d_grid_geometry->getXLower()[" + j + "] - position" 
                        + coord + "_rm" + index + ") /(dx[" + j + "]/" + INTERPHASELENGTH + ".0)) * ";
            }
            kernTerms = kernTerms.substring(0, kernTerms.lastIndexOf(" *")) + ";" + NL;
            kernelAcc = kernelAcc + "kern" + combId + " + ";
        }
        kernelAcc = kernelAcc.substring(0, kernelAcc.lastIndexOf(" +"));
        String calculations = "";
        String vectorIndex = "";
        String pIndex = "";
        for (int i = 0; i < pi.getDimensions(); i++) {
            String coord = pi.getCoordinates().get(i);
            pIndex = pIndex + "p" + (i + 1) + ", ";
            vectorIndex = vectorIndex + coord + " - boxfirst1(" + i + "), ";
        }
        pIndex = pIndex.substring(0, pIndex.length() - 2);
        vectorIndex = vectorIndex.substring(0, vectorIndex.length() - 2);
        if (withLS) {
            for (int i = 0; i < pi.getInteriorRegionIds().size(); i++) {
                String id = pi.getInteriorRegionIds().get(i);
                calculations = calculations + ind + "vectorP(ls_" + id + ", " + vectorIndex + ", " + pIndex + ") = (" + NL;
                for (int j = 0; j < combinations.size(); j++) {
                    String comb = combinations.get(j);
                    String combId = combinations.get(j).replaceAll("-", "m").replaceAll("\\+", "p");
                    String index = "";
                    for (int k = 0; k < comb.length() / 2; k++) {
                        String op = comb.substring(k * 2, k * 2 + 1);
                        String quantity = "";
                        if (!comb.substring(k * 2 + 1, k * 2 + 2).equals("0")) {
                            quantity = comb.substring(k * 2 + 1, k * 2 + 2);
                        } 
                        else {
                            op = "";
                        }
                        index = index + "[index" + (k + 1) + op + quantity + "]";
                    }
                    calculations = calculations + ind + IND + "ls_" + id + "_rm" + index + " * kern" + combId + " + " + NL;
                }
                calculations = calculations.substring(0, calculations.lastIndexOf(" +")) + ") / kernacc;" + NL;
            }
        }
        for (int i = 0; i < pi.getDimensions(); i++) {
            String coord = pi.getCoordinates().get(i);
            calculations = calculations + ind + "vectorP_r(velocity" + coord + ", sopO, " + pIndex + ") = (" + NL;
            for (int j = 0; j < combinations.size(); j++) {
                String comb = combinations.get(j);
                String combId = combinations.get(j).replaceAll("-", "m").replaceAll("\\+", "p");
                String index = "";
                for (int k = 0; k < comb.length() / 2; k++) {
                    String op = comb.substring(k * 2, k * 2 + 1);
                    String quantity = "";
                    if (!comb.substring(k * 2 + 1, k * 2 + 2).equals("0")) {
                        quantity = comb.substring(k * 2 + 1, k * 2 + 2);
                    } 
                    else {
                        op = "";
                    }
                    index = index + "[index" + (k + 1) + op + quantity + "]";
                }
                calculations = calculations + ind + IND + "velocity" + coord + "_rm" + index + " * kern" + combId + " + " + NL;
            }
            calculations = calculations.substring(0, calculations.lastIndexOf(" +")) + ") / kernacc;" + NL;
        }
        
        return kernTerms + NL
                + ind + "double kernacc = " + kernelAcc + ";" + NL
                + calculations;
    }
    
    /**
     * Creates the code that do the remesh on the velocities.
     * @param pi            The problem information
     * @param ind           The indent
     * @return              The code
     * @throws CGException  CG00X External error
     */
    private static String remeshVelocities(ProblemInfo pi, String ind) throws CGException {
        String indent = ind;
        String indent2 = ind + IND;
        String index = "";
        String vectorIndex = "";
        for (int i = 0; i < pi.getDimensions(); i++) {
            String coord = pi.getCoordinates().get(i);
            index = index + coord + ", ";
            vectorIndex = vectorIndex + coord + " - boxfirst1(" + i + "), ";
            indent = indent + IND;
            indent2 = indent2 + IND + IND;
        }
        index = index.substring(0, index.length() - 2);
        vectorIndex = vectorIndex.substring(0, vectorIndex.length() - 2);
        return ind + "// And a remeshing of velocities" + NL
                + loopInit(pi.getCoordinates(), ind, false)
                + indent + "if (vector(soporte, " + vectorIndex + ") == 1) {" + NL
                + indent + IND + "hier::Index idx(" + index + ");" + NL
                + indent + IND + "Interphase* sopO = interphase->getItem(idx);" + NL
                + pLoopInit(pi.getCoordinates(), indent + IND, true, true)
                + calculateKernel(pi, indent2, false)
                + loopEnd(pi.getDimensions(), indent + IND)
                + indent + "}" + NL
                + loopEnd(pi.getDimensions(), ind) + NL;
    }
    
    /**
     * Creates the code that add support for interphase particles in boundary cells that need it.
     * @param pi            The problem info
     * @param ind           The indent
     * @return              The code
     * @throws CGException  CG00X External error
     */
    private static String addBoundaryParticleSupport(ProblemInfo pi, String ind) throws CGException {
        String[][] xslParams = new String [THREE][2];
        xslParams[0][0] = "condition";
        xslParams[0][1] = "true";
        xslParams[1][0] = "indent";
        xslParams[1][1] = "0";
        xslParams[2][0] =  "dimensions";
        xslParams[2][1] =  String.valueOf(pi.getCoordinates().size());
        String indent = ind;
        String index = "";
        String pIndex = "";
        String index1 = "";
        String vectorIndex = "";
        String vectorIndex1 = "";
        String supportCond = "";
        String coordMinMax = "";
        String coordAuxVars = "";
        String loopStart = "";
        String diffCond = "";
        String position = "";
        for (int i = 0; i < pi.getDimensions(); i++) {
            String coord = pi.getCoordinates().get(i);
            vectorIndex = vectorIndex + coord + " - boxfirst1(" + i + "), ";
            indent = indent + IND;
        } 
        vectorIndex = vectorIndex.substring(0, vectorIndex.length() - 2);
        String indent2 = indent + IND;
        String indent3 = indent + IND + IND;
        for (int i = 0; i < pi.getDimensions(); i++) {
            String coord = pi.getCoordinates().get(i);
            index = index + coord + ", ";
            index1 = index1 + coord + "1, ";
            vectorIndex1 = vectorIndex1 + coord + "1 - boxfirst1(" + i + "), ";
            supportCond = supportCond + "(" + coord + " == boxfirst(" + i + ") && touch" + coord + "l) || (" + coord + " == boxlast(" + i 
                    + ") && touch" + coord + "u) || ";
            coordMinMax = coordMinMax + indent + IND + "int " + coord + "Min = " + coord + ";" + NL
                    + indent + IND + "int " + coord + "Max = " + coord + ";" + NL                      
                    + indent + IND + "if (" + coord + " == boxfirst(" + i + ")) " + coord + "Min = " + coord + " - d_ghost_width;" + NL
                    + indent + IND + "if (" + coord + " == boxlast(" + i + ")) " + coord + "Max = " + coord + " + d_ghost_width;" + NL;
            String pIndexVL = "";
            String pIndexVU = "";
            for (int j = 0; j < pi.getDimensions(); j++) {
                if (i == j) {
                    pIndexVU = pIndexVU + (INTERPHASELENGTH - 1) + ", ";
                }
                else {
                    pIndexVU = pIndexVU + "0, ";
                }
                pIndexVL = pIndexVL + "0, ";
            }   
            pIndexVL = pIndexVL.substring(0, pIndexVL.length() - 2);
            pIndexVU = pIndexVU.substring(0, pIndexVU.length() - 2);
            coordAuxVars = coordAuxVars + indent + IND + "double posAux_" + coord + "_lwr = vectorP_r(position" + coord + ", sop, " 
                    + pIndexVL + ");" + NL
                    + indent + IND + "double posAux_" + coord + "_upr = vectorP_r(position" + coord + ", sop, " + pIndexVU + ");" + NL;
            loopStart = loopStart + indent2 + "for (int " + coord + "1 = " + coord + "Min; " + coord + "1 <= " + coord + "Max; " + coord 
                    + "1++) {" + NL;
            indent2 = indent2 + IND;
            diffCond = diffCond + coord + "1 != " + coord + " || ";
            indent3 = indent3 + IND + IND;
            pIndex = pIndex + "p" + (i + 1) + ", ";
        }
        pIndex = pIndex.substring(0, pIndex.length() - 2);
        for (int i = 0; i < pi.getDimensions(); i++) {
            String coord = pi.getCoordinates().get(i);
            position = position + indent3 + "vectorP_r(position" + coord + ", inter, " + pIndex + ") = (" + coord + "1 + shift" + coord 
                    + ") * dx[" + i + "] + d_grid_geometry->getXLower()[" + i + "];" + NL;
        }
        index = index.substring(0, index.length() - 2);
        index1 = index1.substring(0, index1.length() - 2);
        vectorIndex1 = vectorIndex1.substring(0, vectorIndex1.length() - 2);
        supportCond = supportCond.substring(0, supportCond.lastIndexOf(" ||"));
        diffCond = diffCond.substring(0, diffCond.lastIndexOf(" ||"));
        
        String lsAuxVars = "";
        String lsCalculation = "";
        ArrayList<String> elements = new ArrayList<String>();
        elements.add("l");
        elements.add("u");
        ArrayList<String> combinations = CodeGeneratorUtils.createCombinations(pi.getDimensions(), elements);
        for (int j = 0; j < pi.getInteriorRegionIds().size(); j++) {
            String id = pi.getInteriorRegionIds().get(j);
            for (int k = 0; k < combinations.size(); k++) {
                String comb = combinations.get(k);
                String limitIndex = "";
                for (int l = 0; l < comb.length(); l++) {
                    if (comb.substring(l, l + 1).equals("l")) {
                        limitIndex = limitIndex + "0, ";
                    }
                    else {
                        limitIndex = limitIndex + (INTERPHASELENGTH - 1) + ", "; 
                    }
                }
                limitIndex = limitIndex.substring(0, limitIndex.length() - 2);
                lsAuxVars = lsAuxVars + indent + IND + "double valAux_" + id + "_" + comb + " = vectorP(ls_" + id + ", " + vectorIndex + ", " 
                        + limitIndex + ");" + NL;
            }
            lsCalculation = lsCalculation + calculateInterpolation(pi, indent3, id);
        }
        return ind + "// **************************************************************************************************** //" + NL
                + ind + "//Adding particle support to (only EXTERNAL) boundaries (basically, we apply a LINEAR INTERPOLATION condition for LS)" + NL
                + getMaxMinThresholds(pi.getDimensions(), ind)
                + getTouchVars(pi.getCoordinates(), ind, false)
                + loopInit(pi.getCoordinates(), ind, true)
                + indent + "if ((" + supportCond + ") && vector(soporte, " + vectorIndex + ") == 1 ) {" + NL
                + indent + IND + "hier::Index idx(" + index + ");" + NL
                + indent + IND + "Interphase* sop = interphase->getItem(idx);" + NL + NL
                + coordMinMax
                + indent + IND + "// Auxiliary data for extrapolation" + NL
                + coordAuxVars
                + lsAuxVars
                + indent + IND + "//Adding support" + NL
                + loopStart                        
                + indent2 + "if (" + diffCond + ") {" + NL
                + indent2 + IND + "hier::Index idx1(" + index1 + ");" + NL
                + indent2 + IND + "Interphase* inter;" + NL
                + indent2 + IND + "if (vector(soporte, " + vectorIndex1 + ") == 1) {" + NL
                + indent2 + IND + IND + "inter = interphase->getItem(idx1);" + NL
                + indent2 + IND + "} else {" + NL
                + indent2 + IND + IND + "inter = new Interphase();" + NL
                + indent2 + IND + IND + "interphase->addItemPointer(idx1, inter);" + NL
                + indent2 + IND + IND + "vector(soporte, " + vectorIndex1 + ") = 1;" + NL
                + indent2 + IND + "}" + NL + NL
                + pLoopInit(pi.getCoordinates(), indent2 + IND, true, false)
                + position
                + lsCalculation
                + loopEnd(pi.getDimensions(), indent2 + IND)
                + indent2 + "}" + NL
                + loopEnd(pi.getDimensions(), indent + IND)
                + indent + "}" + NL
                + loopEnd(pi.getDimensions(), ind);
    }
    
    /**
     * Creates the code to calculate the level set interpolation at boundaries. 
     * @param pi                The problem information
     * @param ind               The indent
     * @param lsId              The level set identifier
     * @return                  The code
     * @throws CGException      CG00X External error
     */
    private static String calculateInterpolation(ProblemInfo pi, String ind, String lsId) throws CGException {
        String pIndex = "";
        String vectorIndex = "";
        for (int i = 0; i < pi.getDimensions(); i++) {
            String coord = pi.getCoordinates().get(i);
            vectorIndex = vectorIndex + coord + "1 - boxfirst1(" + i + "), ";
            pIndex = pIndex + "p" + (i + 1) + ", ";
        } 
        pIndex = pIndex.substring(0, pIndex.length() - 2);
        ArrayList<String> elements = new ArrayList<String>();
        elements.add("l");
        elements.add("u");
        String varAcc = "";
        String auxs = "";
        for (int i = 0; i < pi.getDimensions() - 1; i++) {
            ArrayList<String> combinations = CodeGeneratorUtils.createCombinations(pi.getDimensions() - (i + 1), elements);
            String coord = pi.getCoordinates().get(i);
            String contCoord = CodeGeneratorUtils.discToCont(pi.getProblem(), coord);
            String oldVarAcc = varAcc;
            varAcc = varAcc + contCoord;
            for (int j = 0; j < combinations.size(); j++) {
                String comb = combinations.get(j);
                auxs = auxs + ind + "double valAux_" + lsId + "_" + varAcc + comb + " = valAux_" + lsId + "_" + oldVarAcc + "l" + comb 
                        + " + (valAux_" + lsId + "_" + oldVarAcc + "u" + comb + " - valAux_" + lsId + "_" + oldVarAcc + "l" + comb + ") / (posAux_" 
                        + coord + "_upr - posAux_" + coord + "_lwr) * (vectorP_r(position" + coord + ", inter, " + pIndex + ") - posAux_" + coord 
                        + "_lwr);" + NL;
            }
        }
        String lastCoord = pi.getCoordinates().get(pi.getDimensions() - 1);
        return auxs 
                + ind + "vectorP(ls_" + lsId + ", " + vectorIndex + pIndex + ") = valAux_" + lsId + "_" + varAcc + "l + (valAux_" + lsId + "_" 
                + varAcc + "u - valAux_" + lsId + "_" + varAcc + "l)/(posAux_" + lastCoord + "_upr-posAux_" + lastCoord + "_lwr) * "
                + "(vectorP_r(position" + lastCoord + ", inter, " + pIndex + ") - posAux_" + lastCoord + "_lwr);" + NL;
    }
    
    /**
     * Creates the xterms for x3d calculation purposes. 
     * @param pi                The problem information
     * @param ind               The indent
     * @param x3dName           The x3d region name
     * @param withTimeCoeffs    If the calculations must use the time coeffitients
     * @return                  The code
     * @throws CGException      CG00X External error
     */
    private static String getTerms(ProblemInfo pi, String ind, String x3dName, boolean withTimeCoeffs) throws CGException {
        String[][] xslParams = new String [THREE][2];
        xslParams[0][0] = "condition";
        xslParams[0][1] = "true";
        xslParams[1][0] = "indent";
        xslParams[1][1] = "0";
        xslParams[2][0] =  "dimensions";
        xslParams[2][1] =  String.valueOf(pi.getCoordinates().size());
        ArrayList<String> coords = pi.getCoordinates();
        String termDeclaration = ind + "int ";
        String a = "";
        String aIterm = "";
        String b = "";
        String bIterm = "";
        String c = "";
        String cIterm = "";
        for (int i = 0; i < coords.size(); i++) {
            String coord = coords.get(i);
            termDeclaration = termDeclaration + coord + "term, ";
            if (withTimeCoeffs) {
                a = a + ind + IND + "a" + coord + " = " + x3dName + "TimeCoeff[" + x3dName + "Unions[unionsI][facePointI]][" + i + "][0] + " + x3dName
                        + "TimeCoeff[" + x3dName + "Unions[unionsI][facePointI]][" + i + "][1]*tfrac_" + x3dName + " + " + x3dName + "TimeCoeff[" 
                        + x3dName + "Unions[unionsI][facePointI]][" + i + "][2]*tfrac_" + x3dName + "*tfrac_" + x3dName + " + " + x3dName 
                        + "TimeCoeff[" + x3dName + "Unions[unionsI][facePointI]][" + i + "][3]*tfrac_" + x3dName + "*tfrac_" + x3dName + "*tfrac_" 
                        + x3dName + ";" + NL;  
                b = b + ind + IND + "b" + coord + " = " + x3dName + "TimeCoeff[" + x3dName + "Unions[unionsI][facePointI]][" + i + "][0] + " 
                        + x3dName + "TimeCoeff[" + x3dName + "Unions[unionsI][facePointI]][" + i + "][1]*tfrac_" + x3dName + " + " + x3dName 
                        + "TimeCoeff[" + x3dName + "Unions[unionsI][facePointI]][" + i + "][2]*tfrac_" + x3dName + "*tfrac_" + x3dName + " + " 
                        + x3dName + "TimeCoeff[" + x3dName + "Unions[unionsI][facePointI]][" + i + "][3]*tfrac_" + x3dName + "*tfrac_" + x3dName 
                        + "*tfrac_" + x3dName + ";" + NL;
                c = c + ind + IND + "c" + coord + " = " + x3dName + "TimeCoeff[" + x3dName + "Unions[unionsI][facePointI]][" + i + "][0] + " 
                        + x3dName + "TimeCoeff[" + x3dName + "Unions[unionsI][facePointI]][" + i + "][1]*tfrac_" + x3dName + " + " + x3dName 
                        + "TimeCoeff[" + x3dName + "Unions[unionsI][facePointI]][" + i + "][2]*tfrac_" + x3dName + "*tfrac_" + x3dName + " + " 
                        + x3dName + "TimeCoeff[" + x3dName + "Unions[unionsI][facePointI]][" + i + "][3]*tfrac_" + x3dName + "*tfrac_" + x3dName 
                        + "*tfrac_" + x3dName + ";" + NL;
            }
            else {
                a = a + ind + IND + "a" + coord + " = " + x3dName + "CellPoints[timeSlice][" + x3dName + "Unions[unionsI][facePointI]][" + i + "];" 
                        + NL;
                b = b + ind + IND + "b" + coord + " = " + x3dName + "CellPoints[timeSlice][" + x3dName + "Unions[unionsI][facePointI]][" + i + "];" 
                        + NL;
                c = c + ind + IND + "c" + coord + " = " + x3dName + "CellPoints[timeSlice][" + x3dName + "Unions[unionsI][facePointI]][" + i + "];" 
                        + NL;
            }

            
            aIterm = aIterm + ind + IND + coord + "term = floor((a" + coord + " - d_grid_geometry->getXLower()[" + i + "]) / dx[" + i + "]);" + NL;
            bIterm = bIterm + ind + IND + coord + "term = floor((b" + coord + " - d_grid_geometry->getXLower()[" + i + "]) / dx[" + i + "]);" + NL;
            cIterm = cIterm + ind + IND + coord + "term = floor((c" + coord + " - d_grid_geometry->getXLower()[" + i + "]) / dx[" + i + "]);" + NL;
        }
        termDeclaration = termDeclaration.substring(0, termDeclaration.length() - 2) + ";" + NL;
        
        String threeDPart = "";
        if (pi.getDimensions() == THREE) {
            threeDPart = ind + "if (facePointI == 2) {" + NL
                + c
                + cIterm
                + ind + "}" + NL;
        }
        
        return termDeclaration
                + ind + "if (facePointI == 0) {" + NL
                + a
                + aIterm
                + ind + "}" + NL
                + ind + "if (facePointI == 1) {" + NL
                + b
                + bIterm
                + ind + "}" + NL
                + threeDPart;
    }
    
    /**
     * Creates the code that free the memory for the temporal variables created for performance reasons.
     * @param pi        The problem information
     * @param ind       The indent
     * @param withLS    If the level set variables must be freed
     * @return          The code
     */
    private static String tmpFree(ProblemInfo pi, String ind, boolean withLS) {
        String code = "";
        ArrayList<String> coords = pi.getCoordinates();
        String indent = ind;
        for (int i = coords.size() - 1; i >= 1; i--) {
            indent = indent + IND;
        }
        for (int i = coords.size() - 2; i >= 0; i--) {
            indent = indent.substring(1);
            String arrayIndex = "";
            for (int j = 0; j < i + 1; j++) {
                String coord2 = coords.get(j);
                arrayIndex = arrayIndex + "[" + coord2 + "]";
            }
            String coord = coords.get(i);
            code = indent + "for(int " + coord + " = 0; " + coord + " < (" + coord 
                    + "last + 2) * " + INTERPHASELENGTH + "; " + coord + "++) {" + NL
                    + code;
            for (int j = 0; j < coords.size(); j++) {
                String coord2 = coords.get(j);
                code = code + indent + IND + "free(position" + coord2 + "_rm" + arrayIndex + ");" + NL
                        + indent + IND + "free(velocity" + coord2 + "_rm" + arrayIndex + ");" + NL;
            }
            if (withLS) {
                for (int j = 0; j < pi.getInteriorRegionIds().size(); j++) {
                    String id = pi.getInteriorRegionIds().get(j);
                    code = code + indent + IND + "free(ls_" + id + "_rm" + arrayIndex + ");" + NL;
                }
            }
            code = code + indent + "}" + NL;
            indent = indent + IND;
        }
        for (int j = 0; j < coords.size(); j++) {
            String coord2 = coords.get(j);
            code = code + ind + "free(position" + coord2 + "_rm);" + NL
                    + ind + "free(velocity" + coord2 + "_rm);" + NL;
        }
        if (withLS) {
            for (int j = 0; j < pi.getInteriorRegionIds().size(); j++) {
                String id = pi.getInteriorRegionIds().get(j);
                code = code + ind + "free(ls_" + id + "_rm);" + NL;
            }
        }
        return code;
    }
    
    /**
     * Creates the code that checks if the unions are valid for a patch.
     * @param pi            The problem information
     * @param ind           The indent
     * @return              The code
     * @throws CGException  CG00X External error 
     */
    private static String unionInDomainCheck(ProblemInfo pi, String ind) throws CGException {
        String lowUpAux = "";
        String cond = "";
        String temporalVars = "";
        String comparisons = "";
        String pointIndPatchCond = "";
        String outCond = "";
        String auxs = "";
        String validation = "";
        String coord0 = pi.getCoordinates().get(0);
        String contCoord0 = CodeGeneratorUtils.discToCont(pi.getProblem(), coord0);
        for (int i = 0; i < pi.getDimensions(); i++) {
            String coord = pi.getCoordinates().get(i);
            String contCoord = CodeGeneratorUtils.discToCont(pi.getProblem(), coord);
            lowUpAux = lowUpAux + ind + "double " + contCoord + "LowerAux = " + contCoord + "Lower + d_ghost_width*dx[" + i + "];" + NL
                    + ind + "double " + contCoord + "UpperAux = " + contCoord + "Upper - d_ghost_width*dx[" + i + "];" + NL;
            String varname = "a";
            String altVarname1 = "b";
            if (i == 1) {
                varname = "b";
                altVarname1 = "a";
            }
            if (i == 2) {
                varname = "c";
            }
            String varAdj = "";
            String varAdj2 = "";
            String varAssign = "";
            for (int j = 0; j < pi.getDimensions(); j++) {
                String coord2 = pi.getCoordinates().get(j);
                String contCoord2 = CodeGeneratorUtils.discToCont(pi.getProblem(), coord2);
                cond = cond + varname + coord2 + "Tmp > " + contCoord2 + "UpperAux || " + varname + coord2 + "Tmp < " + contCoord2 + "LowerAux || ";
                temporalVars = temporalVars + ind + IND + "double " + varname + coord2 + "Tmp = " + varname + coord2 + NL;
                String assignment = ind + IND + IND + IND + varname + coord2 + "Tmp = max<double>(" + contCoord2 
                        + "LowerAux, min<double>(" + contCoord2 + "UpperAux, " + varname + coord2 + "Tmp));" + NL;
                for (int k = 0; k < pi.getDimensions(); k++) {
                    String coord3 = pi.getCoordinates().get(k);
                    if (j != k) {
                        assignment = assignment + ind + IND + IND + IND + varname + coord3 + "Tmp = aj + (bj - aj)/(bi - ai) * (aiTmp - ai);" + NL;
                    }
                    
                }
                comparisons = comparisons + ind + IND + IND + "if (" + varname + coord2 + "Tmp > " + contCoord2 + "UpperAux || " + varname 
                        + coord2 + "Tmp < " + contCoord2 + "LowerAux) {" + NL
                        + ind + IND + IND + IND + "aiTmp = max<double>(xLowerAux, min<double>(xUpperAux, aiTmp));" + NL
                        + assignment
                        + ind + IND + IND + "}" + NL;
                pointIndPatchCond = pointIndPatchCond + "(" + varname + coord2 + " >= boxfirst(" + j + ") && " + varname + coord2 
                        + " <= boxlast(" + j + ")) || ";
                
                outCond = outCond + varname + coord2 + " > " + contCoord2 + "UpperAux || " + varname + coord2 + " < " + contCoord2 + "LowerAux || ";
                auxs = auxs + ind + IND + IND + "double " + varname + coord2 + "Aux = " + varname + coord2 + ";" + NL;
                String points = "";
                String points2 = "";
                String pointsMax = "";
                String pointsMax2 = "";
                for (int k = 0; k < pi.getDimensions(); k++) {
                    String coord3 = pi.getCoordinates().get(k);
                    if (k == j) {
                        pointsMax = pointsMax + ind + IND + IND + IND + varname + coord2 + " = max<double>(" + contCoord2 + "LowerAux, min<double>(" 
                                + contCoord2 + "UpperAux, " + varname + coord2 + "));" + NL;
                        pointsMax2 = pointsMax2 + ind + IND + IND + IND + varname + coord2 + " = max<double>(" + contCoord2 + "Lower, min<double>(" 
                                + contCoord2 + "Upper, " + varname + coord2 + "));" + NL;
                    }
                    else {
                        points = points + createInterpolation2D(ind + IND + IND + IND, varname + coord2, varname + coord3, varname 
                                + coord2 + "Aux", varname + coord3 + "Aux", altVarname1 + coord2 + "Aux", altVarname1 + coord3 + "Aux");
                        points2 = points2 + createInterpolation2D(ind + IND + IND + IND, varname + coord2, varname + coord3, varname 
                                + coord2 + "Aux", varname + coord3 + "Aux", altVarname1 + coord2 + "Aux", altVarname1 + coord3 + "Aux");
                    }
                }
                varAdj = varAdj + ind + IND + IND + "if ( (" + varname + coord2 + " > " + contCoord2 + "UpperAux) || (" + varname 
                        + coord2 + " < " + contCoord2 + "LowerAux) ) {" + NL
                        + pointsMax
                        + points
                        + ind + IND + IND + "}" + NL;
                varAdj2 = varAdj2 + ind + IND + IND + "if ( (" + varname + coord2 + " > " + contCoord2 + "Upper) || (" + varname 
                        + coord2 + " < " + contCoord2 + "Lower) ) {" + NL
                        + pointsMax2
                        + points2
                        + ind + IND + IND + "}" + NL;
                varAssign = varAssign + ind + IND + IND + varname + coord2 + " = " + varname + coord2 + "Aux;" + NL;
            }
            
            validation = validation + varAdj
                    + ind + IND + IND + "if ( (" + varname + coord0 + " > " + contCoord0 + "UpperAux) || (" + varname + coord0 + " < " 
                    + contCoord0 + "LowerAux) ) {" + NL
                    + ind + IND + IND + IND + "validUnion = false;" + NL
                    + ind + IND + IND + "}" + NL
                    + varAssign
                    + varAdj2;
        }
        cond = cond.substring(0, cond.lastIndexOf(" ||"));
        pointIndPatchCond = pointIndPatchCond.substring(0, pointIndPatchCond.lastIndexOf(" ||"));
        outCond = outCond.substring(0, outCond.lastIndexOf(" ||"));
        
        String check = "";
        if (pi.getDimensions() == 2) {
            check = ind + IND + "if (" + outCond + ") {" + NL
                    + auxs
            		+ validation
                    + ind + IND + "}" + NL;
            
        }
        else {
            check = ind + IND + "if (!(" + pointIndPatchCond + ")) {" + NL
                    + calculateLandas3D(pi, ind + IND + IND)
                    + ind + IND + IND + "if(!(" + calculateLandas3DCond(pi) + ")) {" + NL
                    + ind + IND + IND + IND + "validUnion = false;" + NL
                    + ind + IND + IND + "}" + NL
                    + ind + IND + "}" + NL;
                    
        }
        
        return lowUpAux
                + ind + "if (facePointI == DIMENSIONS - 1) {" + NL
                + check
                + ind + "}" + NL;
    }

    /**
     * Creates the interpolation of a point in 2D. 
     * @param ind               The indent
     * @param x                 Interpolation params
     * @param y                 Interpolation params
     * @param xa                Interpolation params
     * @param ya                Interpolation params
     * @param xb                Interpolation params
     * @param yb                Interpolation params              
     * @return                  The interpolation
     * @throws CGException      CG00X External error
     */
    private static String createInterpolation2D(String ind, String x, String y, String xa, String ya, String xb, String yb) throws CGException {
        return ind + y + " = " + ya + " + (" + x + " - " + xa + ") * (" + yb + " - " + ya + ") / (" + xb + " - " + xa + ");" + NL;
    }
    
    
    /**
     * Creates the check for the landas in 3D used to check if a x3d triangle intersect with the domain. 
     * @param pi                The problem info
     * @return                  The landas
     * @throws CGException      CG00X External error
     */
    private static String calculateLandas3DCond(ProblemInfo pi) throws CGException {
        String landas = "";
        for (int i = 0; i < pi.getDimensions(); i++) {
            for (int j = 0; j < 4; j++) {
                String op1 = "Lower";
                String op2 = "Lower";
                if (j > 1) {
                    op1 = "Upper";
                }
                if (j % 2 == 1) {
                    op2 = "Upper";   
                }
                landas = landas + "(l1" + i + op1 + op2 + " >= 0 && l1" + i + op1 + op2 + " <= 1 && "
                        + "l2" + i + op1 + op2 + " >= 0 && l2" + i + op1 + op2 + " <= 1 && "
                        + "l3" + i + op1 + op2 + " >= 0 && l3" + i + op1 + op2 + " <= 1) || ";
            }
        }
        return landas.substring(0, landas.lastIndexOf(" ||"));
    }
    
    /**
     * Creates the landas in 3D used to check if a x3d triangle intersect with the domain. 
     * @param pi                The problem info
     * @param ind               The indent
     * @return                  The landas
     * @throws CGException      CG00X External error
     */
    private static String calculateLandas3D(ProblemInfo pi, String ind) throws CGException {
        String landas = "";
        for (int i = 0; i < pi.getDimensions(); i++) {
            String coord1 = "";
            String coord2 = "";
            if (i == 0) {
                coord1 = pi.getCoordinates().get(1);
                coord2 = pi.getCoordinates().get(2);
            }
            if (i == 1) {
                coord1 = pi.getCoordinates().get(2);
                coord2 = pi.getCoordinates().get(0);
            }
            if (i == 2) {
                coord1 = pi.getCoordinates().get(1);
                coord2 = pi.getCoordinates().get(0);
            }
            String contCoord1 = CodeGeneratorUtils.discToCont(pi.getProblem(), coord1);
            String contCoord2 = CodeGeneratorUtils.discToCont(pi.getProblem(), coord2);
            for (int j = 0; j < 4; j++) {
                String op1 = "Lower";
                String op2 = "Lower";
                if (j > 1) {
                    op1 = "Upper";
                }
                if (j % 2 == 1) {
                    op2 = "Upper";   
                }
                landas = landas + ind + "double l1" + i + op1 + op2 + " = ((" + contCoord1 + op1 + "Aux - a" + coord1 + ") * (c" 
                        + coord2 + " - a" + coord2 + ") - (" + contCoord2 + op2 + " - a" + coord2 + ") * (c" + coord1 + " - a" + coord1
                        + "))/((c" + coord2 + " - a" + coord2 + ") * (b" + coord1 + " - a" + coord1 + ") - (c" + coord1 + " - a" + coord1
                        + ") * (b" + coord2 + " - a" + coord2 + "));" + NL
                        + ind + "double l2" + i + op1 + op2 + " = ((" + contCoord2 + op2 + "Aux - a" + coord2 + ") * (b" + coord1 + " - a" + coord1 
                        + ") - (" + contCoord1 + op1 + " - a" + coord1 + ") * (b" + coord2 + " - a" + coord2 + "))/((c" + coord2 + " - a" + coord2 
                        + ") * (b" + coord1 + " - a" + coord1 + ") - (c" + coord1 + " - a" + coord1 + ") * (b" + coord2 + " - a" + coord2 + "));" + NL
                        + ind + "double l3" + i + op1 + op2 + " = l1" + i + op1 + op2 + " + l2" + i + op1 + op2 + ";" + NL;
            }
        }
        return landas;
    }
    
    /**
     * Creates the code for the max and min threshold variables.
     * @param dimensions    The dimensions of the problem
     * @param indent        The indent
     * @return              The thresholds
     */
    private static String getMaxMinThresholds(int dimensions, String indent) {
        String deltas = "";
        String deltaSquare = "";
        for (int i = 0; i < dimensions; i++) {
            if (i > 0) {
                deltas = "max<double>(" + deltas + ", dx[" + i + "])";
            }
            else {
                deltas = deltas + "dx[" + i + "]";
            }
            deltaSquare = deltaSquare + "dx[" + i + "] * dx[" + i + "] + ";
        }
        
        deltaSquare = deltaSquare.substring(0, deltaSquare.lastIndexOf(" + "));
        return indent + "double minThreshold = (corridor_width * " + deltas + ") - 0.5 * sqrt(" + deltaSquare + ");" + NL
                + indent + "double maxThreshold = (corridor_width * " + deltas + ") + 0.5 * sqrt(" + deltaSquare + ");" + NL; 
        
    }
    
    /**
     * Creates the code that initialize the touch variables.
     * @param coords        The coordinates
     * @param indent        The indent
     * @param withPeriodic  True to initialize also the periodic touch variables
     * @return              The code
     */
    private static String getTouchVars(ArrayList<String> coords, String indent, boolean withPeriodic) {
        String vars = "";
        for (int i = 0;i < coords.size(); i++) {
            String coord = coords.get(i);
            vars = vars + indent + "bool touch" + coord + "l = patch_geom->getTouchesRegularBoundary (" + i + ", 0);" + NL
                    + indent + "bool touch" + coord + "u = patch_geom->getTouchesRegularBoundary (" + i + ", 1);" + NL;
            if (withPeriodic) {
                vars = vars + indent + "bool touchP" + coord + "l = !touch" + coord + "l && (patch_geom->getXLower()[" + i 
                        + "] == d_grid_geometry->getXLower()[" + i + "]);" + NL
                        + indent + "bool touchP" + coord + "u = !touch" + coord + "u && (patch_geom->getXUpper()[" + i 
                        + "] == d_grid_geometry->getXUpper()[" + i + "]);" + NL;
            }
        }
        return vars;
    }
    
    /**
     * Creates the code to map the level set variables (2D).
     * @param pi                The problem info
     * @param ind               The indent
     * @param segName           The regionName
     * @param lsId              The level set identifier
     * @param fromRemesh        If the calculations come from the remesh
     * @return                  The code
     */
    private static String lsMappingCalculation2D(ProblemInfo pi, String ind, String segName, String lsId, boolean fromRemesh) {
        String x = "";
        String pIndex = "";
        String vectorIndex = "";
        String c = "";
        for (int i = 0; i < pi.getDimensions(); i++) {
            String coord = pi.getCoordinates().get(i);
            pIndex = pIndex + "p" + (i + 1) + ", ";
            vectorIndex = vectorIndex + coord + " - boxfirst1(" + i + "), ";
        }
        pIndex = pIndex.substring(0, pIndex.length() - 2);
        vectorIndex = vectorIndex.substring(0, vectorIndex.length() - 2);
        String condA = "";
        String condB = "";
        for (int i = 0; i < pi.getDimensions(); i++) {
            String coord = pi.getCoordinates().get(i);
            x = x + ind + "double x" + coord + " = vectorP_r(position" + coord + ", sop, " + pIndex + ");" + NL;
            condA = condA + "(x" + coord + " - a" + coord + ")*(x" + coord + " - a" + coord + ") + ";
            condB = condB + "(x" + coord + " - b" + coord + ")*(x" + coord + " - b" + coord + ") + ";
            if (fromRemesh) {
                c = c + ind + IND + IND + IND + IND + "double cc" + coord + " = " + segName + "TimeCoeff[" + segName + "Unions[unionsI2][0]][" + i
                    + "][0] + " + segName + "TimeCoeff[" + segName + "Unions[unionsI2][0]][" + i + "][1]*tfrac_" + segName + " + " + segName 
                    + "TimeCoeff[" + segName + "Unions[unionsI2][0]][" + i + "][2]*tfrac_" + segName + "*tfrac_" + segName + " + " + segName 
                    + "TimeCoeff[" + segName + "Unions[unionsI2][0]][" + i + "][3]*tfrac_" + segName + "*tfrac_" + segName + "*tfrac_" 
                    + segName + ";" + NL
                    + ind + IND + IND + IND + IND + "double cc" + coord + "2 = " + segName + "TimeCoeff[" + segName + "Unions[unionsI2][1]][" + i 
                    + "][0] + " + segName + "TimeCoeff[" + segName + "Unions[unionsI2][1]][" + i + "][1]*tfrac_" + segName + " + " + segName 
                    + "TimeCoeff[" + segName + "Unions[unionsI2][1]][" + i + "][2]*tfrac_" + segName + "*tfrac_" + segName + " + " + segName 
                    + "TimeCoeff[" + segName + "Unions[unionsI2][1]][" + i + "][3]*tfrac_" + segName + "*tfrac_" + segName + "*tfrac_" 
                    + segName + ";" + NL;
            }
            else {
                c = c + ind + IND + IND + IND + IND + "double cc" + coord + " = " + segName 
                        + "CellPoints[timeSlice][" + segName + "Unions[unionsI2][0]][" + i + "];" + NL
                        + ind + IND + IND + IND + IND + "double cc" + coord + "2 = " + segName 
                        + "CellPoints[timeSlice][" + segName + "Unions[unionsI2][1]][" + i + "];" + NL;
            }
        }
        condA = condA.substring(0, condA.lastIndexOf(" + "));
        condB = condB.substring(0, condB.lastIndexOf(" + "));
        
        //Selection of region
        String segLSAssignment = "";
        if (pi.getInteriorRegionIds().size() <= 2) {
            for (int i = 0; i < pi.getInteriorRegionIds().size(); i++) {
                String id = pi.getInteriorRegionIds().get(i);
                if (!id.equals(lsId) && !pi.getMovementInfo().getX3dMovRegions().contains(id)) {
                    segLSAssignment = ind + IND + IND + "vectorP(ls_" + id + ", " + vectorIndex + ", " + pIndex + ") = -dist;" + NL;
                }
            }
        }
        else {
            for (int i = 0; i < pi.getInteriorRegionIds().size(); i++) {
                String id = pi.getInteriorRegionIds().get(i);
                String segNameT = pi.getRegions().get((Integer.parseInt(id)) / 2);
                if (!id.equals(lsId) && !pi.getMovementInfo().getX3dMovRegions().contains(segNameT)) {
                    String cond = "";
                    if (fromRemesh) {
                        cond = "fabs(dist) < fabs(vectorP(ls_" + id + ", " + vectorIndex + ", " + pIndex + "))";
                    }
                    else {
                        for (int j = 0; j < pi.getInteriorRegionIds().size(); j++) {
                            String id2 = pi.getInteriorRegionIds().get(j);
                            if (!id.equals(lsId) && !id.equals(id2)) {
                                cond = cond + "acc_" + id + " > acc_" + id2 + " && ";
                            }
                        }
                        cond = cond.substring(0, cond.lastIndexOf(" &&"));
                    }
                    segLSAssignment = segLSAssignment + ind + IND + IND + "if (" + cond + ") {" + NL
                            + ind + IND + IND + IND + "vectorP(ls_" + id + ", " + vectorIndex + ", " + pIndex + ") = -dist;" + NL
                            + ind + IND + IND + "}" + NL;
                }
            }
        }
        
        String coord1 = pi.getCoordinates().get(0);
        String coord2 = pi.getCoordinates().get(1);
        return x
                + ind + "double dist = ((x" + coord1 + " - a" + coord1 + ")*(b" + coord2 + " - a" + coord2 + ") - (x" + coord2 + " - a" 
                + coord2 + ")*(b" + coord1 + " - a" + coord1 + ")) / modab;" + NL
                + ind + "double proj = ((x" + coord1 + " - a" + coord1 + ")*(b" + coord1 + " - a" + coord1 + ") + (x" + coord2 + " - a" 
                + coord2 + ")*(b" + coord2 + " - a" + coord2 + ")) / modab;" + NL + NL
                + ind + "if (fabs(dist) < fabs(vectorP(ls_" + lsId + ", " + vectorIndex + ", " + pIndex + "))) {" + NL
                + ind + IND + "if (proj >= 0 && proj <= modab) {" + NL
                + ind + IND + IND + "dist = dist * " + segName + "Orientation[unionsI];" + NL
                + ind + IND + "} else {" + NL
                + ind + IND + IND + "dist = dist / fabs(dist) * min<double>(sqrt(dist * dist + proj * proj), sqrt(dist * dist " 
                + "+ (proj - modab) * (proj - modab)));" + NL
                + ind + IND + IND + "if (fabs(dist) < fabs(vectorP(ls_" + lsId + ", " + vectorIndex + ", " + pIndex + "))) {" + NL
                + ind + IND + IND + IND + "double np" + coord1 + " = a" + coord1 + ";" + NL
                + ind + IND + IND + IND + "double np" + coord2 + " = a" + coord2 + ";" + NL
                + ind + IND + IND + IND + "bool antb = true;" + NL
                + ind + IND + IND + IND + "if (" + condA + " > " + condB + ") {" + NL
                + ind + IND + IND + IND + IND + "np" + coord1 + " = b" + coord1 + ";" + NL
                + ind + IND + IND + IND + IND + "np" + coord2 + " = b" + coord2 + ";" + NL
                + ind + IND + IND + IND + IND + "antb = false;" + NL
                + ind + IND + IND + IND + "}" + NL
                + ind + IND + IND + IND + "double c" + coord1 + ", c" + coord2 + ";" + NL
                + ind + IND + IND + IND + "bool found = false;" + NL
                + ind + IND + IND + IND + "for (int unionsI2 = 0; unionsI2 < UNIONS_" + segName + " && !found; unionsI2++) {" + NL
                + c
                + ind + IND + IND + IND + IND + "if ((equalsEq(cc" + coord1 + ", np" + coord1 + ") && equalsEq(cc" + coord2 + ", np" + coord2 
                + ")) && unionsI != unionsI2 && (!equalsEq(cc" + coord1 + "2, np" + coord1 + ") || !equalsEq(cc" + coord2 + "2, np" + coord2 + "))) {" + NL
                + ind + IND + IND + IND + IND + IND + "c" + coord1 + " = cc" + coord1 + "2;" + NL
                + ind + IND + IND + IND + IND + IND + "c" + coord2 + " = cc" + coord2 + "2;" + NL
                + ind + IND + IND + IND + IND + IND + "found = true;" + NL
                + ind + IND + IND + IND + IND + "}" + NL
                + ind + IND + IND + IND + IND + "if ((equalsEq(cc" + coord1 + "2, np" + coord1 + ") && equalsEq(cc" + coord2 + "2, np" + coord2 
                + ")) && unionsI != unionsI2 && (!equalsEq(cc" + coord1 + ", np" + coord1 + ") || !equalsEq(cc" + coord2 + ", np" + coord2 + "))) {" + NL
                + ind + IND + IND + IND + IND + IND + "c" + coord1 + " = cc" + coord1 + ";" + NL
                + ind + IND + IND + IND + IND + IND + "c" + coord2 + " = cc" + coord2 + ";" + NL
                + ind + IND + IND + IND + IND + IND + "found = true;" + NL
                + ind + IND + IND + IND + IND + "}" + NL
                + ind + IND + IND + IND + "}" + NL
                + ind + IND + IND + IND + "double signABC = (a" + coord1 + " - c" + coord1 + ") * (b" + coord2 + " - c" + coord2 + ") - (a" + coord2 
                + " - c" + coord2 + ") * (b" + coord1 + " - c" + coord1 + ");" + NL
                + ind + IND + IND + IND + "double signABX = (a" + coord1 + " - x" + coord1 + ") * (b" + coord2 + " - x" + coord2 + ") - (a" + coord2 
                + " - x" + coord2 + ") * (b" + coord1 + " - x" + coord1 + ");" + NL
                + ind + IND + IND + IND + "double signBCX = (b" + coord1 + " - x" + coord1 + ") * (c" + coord2 + " - x" + coord2 + ") - (b" + coord2 
                + " - x" + coord2 + ") * (c" + coord1 + " - x" + coord1 + ");" + NL
                + ind + IND + IND + IND + "double signCAX = (c" + coord1 + " - x" + coord1 + ") * (a" + coord2 + " - x" + coord2 + ") - (c" + coord2 
                + " - x" + coord2 + ") * (a" + coord1 + " - x" + coord1 + ");" + NL
                + ind + IND + IND + IND + "if (equalsEq(signABC, 0)) {" + NL
                + ind + IND + IND + IND + IND + "dist = dist*" + segName + "Orientation[unionsI];" + NL
                + ind + IND + IND + IND + "} else {" + NL
                + ind + IND + IND + IND + IND + "if ((signABC > 0 && signABX > 0  && (signBCX > 0 || antb) && (signCAX > 0 || !antb)) || "
                + "(signABC < 0 && signABX < 0 && (signBCX < 0 || antb) && (signCAX < 0 || !antb))) {" + NL
                + ind + IND + IND + IND + IND + IND + "dist = - signABC/fabs(signABC) *fabs(dist)*" + segName + "Orientation[unionsI];" + NL
                + ind + IND + IND + IND + IND + "} else {" + NL
                + ind + IND + IND + IND + IND + IND + "dist = signABC/fabs(signABC) *fabs(dist)*" + segName + "Orientation[unionsI];" + NL
                + ind + IND + IND + IND + IND + "}" + NL
                + ind + IND + IND + IND + "}" + NL
                + ind + IND + IND + "}" + NL
                + ind + IND + "}" + NL
                + ind + IND + "if (fabs(dist) < fabs(vectorP(ls_" + lsId + ", " + vectorIndex + ", " + pIndex + "))) {" + NL
                + ind + IND + IND + "vectorP(ls_" + lsId + ", " + vectorIndex + ", " + pIndex + ") = dist;" + NL
                + segLSAssignment
                + ind + IND + "}" + NL
                + ind + "}" + NL;
    }
    
    /**
     * Creates the code to map the level set variables (2D).
     * @param pi                The problem info
     * @param ind               The indent
     * @param segName           The regionName
     * @param lsId              The level set identifier
     * @param fromRemesh        If the calculations come from the remesh
     * @return                  The code
     */
    private static String lsMappingCalculation3D(ProblemInfo pi, String ind, String segName, String lsId, boolean fromRemesh) {
        String x = "";
        String pIndex = "";
        String vectorIndex = "";
        String c = "";
        for (int i = 0; i < pi.getDimensions(); i++) {
            String coord = pi.getCoordinates().get(i);
            pIndex = pIndex + "p" + (i + 1) + ", ";
            vectorIndex = vectorIndex + coord + " - boxfirst1(" + i + "), ";
        }
        pIndex = pIndex.substring(0, pIndex.length() - 2);
        vectorIndex = vectorIndex.substring(0, vectorIndex.length() - 2);
        for (int i = 0; i < pi.getDimensions(); i++) {
            String coord = pi.getCoordinates().get(i);
            x = x + ind + "double x" + coord + " = vectorP_r(position" + coord + ", sop, " + pIndex + ");" + NL;
            if (fromRemesh) {
                c = c + ind + IND + IND + IND + "double cc" + coord + "1 = " + segName + "TimeCoeff[" + segName + "Unions[unionsI2][0]][" + i
                    + "][0] + " + segName + "TimeCoeff[" + segName + "Unions[unionsI2][0]][" + i + "][1]*tfrac_" + segName + " + " + segName 
                    + "TimeCoeff[" + segName + "Unions[unionsI2][0]][" + i + "][2]*tfrac_" + segName + "*tfrac_" + segName + " + " + segName 
                    + "TimeCoeff[" + segName + "Unions[unionsI2][0]][" + i + "][3]*tfrac_" + segName + "*tfrac_" + segName + "*tfrac_" 
                    + segName + ";" + NL
                    + ind + IND + IND + IND + "double cc" + coord + "2 = " + segName + "TimeCoeff[" + segName + "Unions[unionsI2][1]][" + i 
                    + "][0] + " + segName + "TimeCoeff[" + segName + "Unions[unionsI2][1]][" + i + "][1]*tfrac_" + segName + " + " + segName 
                    + "TimeCoeff[" + segName + "Unions[unionsI2][1]][" + i + "][2]*tfrac_" + segName + "*tfrac_" + segName + " + " + segName 
                    + "TimeCoeff[" + segName + "Unions[unionsI2][1]][" + i + "][3]*tfrac_" + segName + "*tfrac_" + segName + "*tfrac_" 
                    + segName + ";" + NL
                    + ind + IND + IND + IND + "double cc" + coord + "3 = " + segName + "TimeCoeff[" + segName + "Unions[unionsI2][2]][" + i 
                    + "][0] + " + segName + "TimeCoeff[" + segName + "Unions[unionsI2][2]][" + i + "][1]*tfrac_" + segName + " + " + segName 
                    + "TimeCoeff[" + segName + "Unions[unionsI2][2]][" + i + "][2]*tfrac_" + segName + "*tfrac_" + segName + " + " + segName 
                    + "TimeCoeff[" + segName + "Unions[unionsI2][2]][" + i + "][3]*tfrac_" + segName + "*tfrac_" + segName + "*tfrac_" 
                    + segName + ";" + NL;
            }
            else {
                c = c + ind + IND + IND + IND + "double cc" + coord + "1 = " + segName 
                        + "CellPoints[timeSlice][" + segName + "Unions[unionsI2][0]][" + i + "];" + NL
                        + ind + IND + IND + IND + "double cc" + coord + "2 = " + segName 
                        + "CellPoints[timeSlice][" + segName + "Unions[unionsI2][1]][" + i + "];" + NL
                        + ind + IND + IND + IND + "double cc" + coord + "3 = " + segName 
                        + "CellPoints[timeSlice][" + segName + "Unions[unionsI2][2]][" + i + "];" + NL;
            }
        }
        
        //Selection of region
        String segLSAssignment = "";
        if (pi.getInteriorRegionIds().size() <= 2) {
            for (int i = 0; i < pi.getInteriorRegionIds().size(); i++) {
                String id = pi.getInteriorRegionIds().get(i);
                if (!id.equals(lsId)) {
                    segLSAssignment = ind + IND + IND + "vectorP(ls_" + id + ", " + vectorIndex + ", " + pIndex + ") = -dist_mov;" + NL;
                }
            }
        }
        else {
            for (int i = 0; i < pi.getInteriorRegionIds().size(); i++) {
                String id = pi.getInteriorRegionIds().get(i);
                if (!id.equals(lsId)) {
                    String cond = "";
                    if (fromRemesh) {
                        cond = "vectorP_r(int_seg, sop, " + pIndex + ") == " + id + " || vectorP_r(seg, sop, " + pIndex + ") == " + id;
                    }
                    else {
                        for (int j = 0; j < pi.getInteriorRegionIds().size(); j++) {
                            String id2 = pi.getInteriorRegionIds().get(j);
                            if (!id.equals(lsId) && !id.equals(id2)) {
                                cond = cond + "acc_" + id + " > acc_" + id2 + " && ";
                            }
                        }
                        cond = cond.substring(0, cond.lastIndexOf(" &&"));
                    }
                    segLSAssignment = segLSAssignment + ind + IND + IND + "if (" + cond + ") {" + NL
                            + ind + IND + IND + IND + "vectorP(ls_" + id + ", " + vectorIndex + ", " + pIndex + ") = -dist_mov;" + NL
                            + ind + IND + IND + "}" + NL;
                }
            }
        }
        
        String coord1 = pi.getCoordinates().get(0);
        String coord2 = pi.getCoordinates().get(1);
        String coord3 = pi.getCoordinates().get(2);
        return x
                + ind + "double distax = sqrt(pow((x" + coord1 + " - a" + coord1 + "), 2) + pow((x" + coord2 + " - a" + coord2 + "), 2) + pow((x"
                    + coord3 + " - a" + coord3 + "), 2));" + NL
                + ind + "double distbx = sqrt(pow((x" + coord1 + " - b" + coord1 + "), 2) + pow((x" + coord2 + " - b" + coord2 + "), 2) + pow((x"
                    + coord3 + " - b" + coord3 + "), 2));" + NL
                + ind + "double distcx = sqrt(pow((x" + coord1 + " - c" + coord1 + "), 2) + pow((x" + coord2 + " - c" + coord2 + "), 2) + pow((x"
                    + coord3 + " - c" + coord3 + "), 2));" + NL
                + ind + "if (distbx < distax && distbx < distcx) {" + NL
                + ind + IND + "double tmp1 = ai;" + NL
                + ind + IND + "ai = bi;" + NL
                + ind + IND + "bi = ci;" + NL
                + ind + IND + "ci = tmp1;" + NL
                + ind + IND + "double tmp2 = aj;" + NL
                + ind + IND + "aj = bj;" + NL
                + ind + IND + "bj = cj;" + NL
                + ind + IND + "cj = tmp2;" + NL
                + ind + IND + "double tmp3 = ak;" + NL
                + ind + IND + "ak = bk;" + NL
                + ind + IND + "bk = ck;" + NL
                + ind + IND + "ck = tmp3;" + NL
                + ind + "}" + NL
                + ind + "if (distcx < distax && distcx < distbx) {" + NL
                + ind + IND + "double tmp1 = ai;" + NL
                + ind + IND + "ai = ci;" + NL
                + ind + IND + "ci = bi;" + NL
                + ind + IND + "bi = tmp1;" + NL
                + ind + IND + "double tmp2 = aj;" + NL
                + ind + IND + "aj = cj;" + NL
                + ind + IND + "cj = bj;" + NL
                + ind + IND + "bj = tmp2;" + NL
                + ind + IND + "double tmp3 = ak;" + NL
                + ind + IND + "ak = ck;" + NL
                + ind + IND + "ck = bk;" + NL
                + ind + IND + "bk = tmp3;" + NL
                + ind + "}" + NL
                + ind + "double modabxac = sqrt( pow(((b" + coord2 + " - a" + coord2 + ") * (c" + coord3 + " - a" + coord3 + ")  - (b" 
                + coord3 + " - a" + coord3 + ") * (c" + coord2 + " - a" + coord2 + ")), 2) + pow(((b" + coord3 + " - a" + coord3 
                + ") * (c" + coord1 + " - a" + coord1 + ")  - (b" + coord1 + " - a" + coord1 + ") * (c" + coord3 + " - a" + coord3 
                + ")), 2) + pow(((b" + coord1 + " - a" + coord1 + ") * (c" + coord2 + " - a" + coord2 + ")  - (b" + coord2 + " - a" 
                + coord2 + ") * (c" + coord1 + " - a" + coord1 + ")), 2) );" + NL
                + ind + "double d_mov = ((x" + coord1 + " - a" + coord1 + ")*((b" + coord2 + " - a" + coord2 + ") * (c" + coord3
                + " - a" + coord3 + ")  - (b" + coord3 + " - a" + coord3 + ") * (c" + coord2 + " - a" + coord2 + ")) + (x" + coord2 
                + " - a" + coord2 + ")*((b" + coord3 + " - a" + coord3 + ") * (c" + coord1 + " - a" + coord1 + ")  - (b" + coord1 
                + " - a" + coord1 + ") * (c" + coord3 + " - a" + coord3 + ")) + (x" + coord3 + " - a" + coord3 + ")*((b" + coord1 
                + " - a" + coord1 + ") * (c" + coord2 + " - a" + coord2 + ")  - (b" + coord2 + " - a" + coord2 + ") * (c" + coord1 
                + " - a" + coord1 + "))) / modabxac;" + NL
                + ind + "double modab = sqrt( (b" + coord1 + " - a" + coord1 + ")*(b" + coord1 + " - a" + coord1 + ") + (b" + coord2 
                + " - a" + coord2 + ")*(b" + coord2 + " - a" + coord2 + ") + (b" + coord3 + " - a" + coord3 + ")*(b" + coord3 + " - a"
                + coord3 + ") );" + NL
                + ind + "double modac = sqrt( (c" + coord1 + " - a" + coord1 + ")*(c" + coord1 + " - a" + coord1 + ") + (c" + coord2 
                + " - a" + coord2 + ")*(c" + coord2 + " - a" + coord2 + ") + (c" + coord3 + " - a" + coord3 + ")*(c" + coord3 + " - a" 
                + coord3 + ") );" + NL
                + ind + "double abac = (b" + coord1 + " - a" + coord1 + ") * (c" + coord1 + " - a" + coord1 + ") + (b" + coord2 
                + " - a" + coord2 + ") * (c" + coord2 + " - a" + coord2 + ") + (b" + coord3 + " - a" + coord3 + ") * (c" + coord3 
                + " - a" + coord3 + ");" + NL
                + ind + "double axab = (x" + coord1 + " - a" + coord1 + ") * (b" + coord1 + " - a" + coord1 + ") + (x" + coord2 
                + " - a" + coord2 + ") * (b" + coord2 + " - a" + coord2 + ") + (x" + coord3 + " - a" + coord3 + ") * (b" + coord3 
                + " - a" + coord3 + ");" + NL
                + ind + "double axac = (x" + coord1 + " - a" + coord1 + ") * (c" + coord1 + " - a" + coord1 + ") + (x" + coord2 
                + " - a" + coord2 + ") * (c" + coord2 + " - a" + coord2 + ") + (x" + coord3 + " - a" + coord3 + ") * (c" + coord3 
                + " - a" + coord3 + ");" + NL
                + ind + "double denom = modab * modab * modac * modac - abac * abac;" + NL
                + ind + "double lambda_mov = (axab * modac * modac - axac * abac) / denom;" + NL
                + ind + "double gamma_mov = (axac * modab * modab - axab * abac) / denom;" + NL + NL
                + ind + "//Distance calculation" + NL
                + ind + "double d_mov_p = 0;" + NL
                + ind + "if (lambda_mov >= 0 && gamma_mov >= 0 && lambda_mov + gamma_mov > 1) {" + NL
                + ind + IND + "double bcac = (c" + coord1 + " - b" + coord1 + ") * (c" + coord1 + " - a" + coord1 + ") + (c" + coord2 
                + " - b" + coord2 + ") * (c" + coord2 + " - a" + coord2 + ") + (c" + coord3 + " - b" + coord3 + ") * (c" + coord3 
                + " - a" + coord3 + ");" + NL
                + ind + IND + "double bcab = (c" + coord1 + " - b" + coord1 + ") * (b" + coord1 + " - a" + coord1 + ") + (c" + coord2 
                + " - b" + coord2 + ") * (b" + coord2 + " - a" + coord2 + ") + (c" + coord3 + " - b" + coord3 + ") * (b" + coord3 
                + " - a" + coord3 + ");" + NL
                + ind + IND + "d_mov_p = (((x" + coord1 + " - b" + coord1 + ") * (b" + coord1 + " - a" + coord1 + ") + (x" + coord2 
                + " - b" + coord2 + ") * (b" + coord2 + " - a" + coord2 + ") + (x" + coord3 + " - b" + coord3 + ") * (b" + coord3 
                + " - a" + coord3 + ")) * bcac - ((x" + coord1 + " - b" + coord1 + ") * (c" + coord1 + " - a" + coord1 + ") + (x" 
                + coord2 + " - b" + coord2 + ") * (c" + coord2 + " - a" + coord2 + ") + (x" + coord3 + " - b" + coord3 + ") * (c" 
                + coord3 + " - a" + coord3 + ")) * bcab) / fabs((b" + coord1 + " - a" + coord1 + ") * bcac + (b" + coord2 + " - a" 
                + coord2 + ") * bcac + (b" + coord3 + " - a" + coord3 + ") * bcac - ((c" + coord1 + " - a" + coord1 + ") * bcab + (c" 
                + coord2 + " - a" + coord2 + ") * bcab + (c" + coord3 + " - a" + coord3 + ") * bcab));" + NL
                + ind + "} else {" + NL
                + ind + IND + "if (lambda_mov < 0 && gamma_mov < 0) {" + NL
                + ind + IND + IND + "d_mov_p = sqrt(lambda_mov * lambda_mov * modab * modab + 2 * lambda_mov * gamma_mov * abac "
                + "+ gamma_mov * gamma_mov * modac * modac);" + NL
                + ind + IND + "} else {" + NL
                + ind + IND + IND + "if (lambda_mov >= 0 && gamma_mov < 0) {" + NL
                + ind + IND + IND + IND + "double proj_mov = axab / modab;" + NL
                + ind + IND + IND + IND + "if (proj_mov < 0) {" + NL
                + ind + IND + IND + IND + IND + "d_mov_p = sqrt(lambda_mov * lambda_mov * modab * modab + 2 * lambda_mov * gamma_mov "
                + "* abac + gamma_mov * gamma_mov * modac * modac);" + NL
                + ind + IND + IND + IND + "} else {" + NL
                + ind + IND + IND + IND + IND + "d_mov_p = (axab * abac - axac * modab * modab) / fabs((b" + coord1 + " - a" + coord1
                + ") * abac + (b" + coord2 + " - a" + coord2 + ") * abac + (b" + coord3 + " - a" + coord3 + ") * abac - ((c" + coord1
                + " - a" + coord1 + ") * modab * modab + (c" + coord2 + " - a" + coord2 + ") * modab * modab + (c" + coord3 + " - a" 
                + coord3 + ") * modab * modab));" + NL
                + ind + IND + IND + IND + "}" + NL
                + ind + IND + IND + "} else {" + NL
                + ind + IND + IND + IND + "if (lambda_mov < 0 && gamma_mov >= 0) {" + NL
                + ind + IND + IND + IND + IND + "double proj_mov = axac / modac;" + NL
                + ind + IND + IND + IND + IND + "if (proj_mov < 0) {" + NL
                + ind + IND + IND + IND + IND + IND + "d_mov_p = sqrt(lambda_mov * lambda_mov * modab * modab + 2 * lambda_mov *"
                + " gamma_mov * abac + gamma_mov * gamma_mov * modac * modac);" + NL
                + ind + IND + IND + IND + IND + "} else {" + NL
                + ind + IND + IND + IND + IND + IND + "d_mov_p = (axab * modac * modac - axac * abac) / fabs((b" + coord1 + " - a" 
                + coord1 + ") * modac * modac + (b" + coord2 + " - a" + coord2 + ") * modac * modac + (b" + coord3 + " - a" + coord3 
                + ") * modac * modac - ((c" + coord1 + " - a" + coord1 + ") * abac + (c" + coord2 + " - a" + coord2 + ") * abac + (c" 
                + coord3 + " - a" + coord3 + ") * abac));" + NL
                + ind + IND + IND + IND + IND + "}" + NL
                + ind + IND + IND + IND + "}" + NL
                + ind + IND + IND + "}" + NL
                + ind + IND + "}" + NL
                + ind + "}" + NL
                + ind + "double dist_mov = sqrt(d_mov * d_mov + d_mov_p * d_mov_p);" + NL
                + ind + "if (fabs(dist_mov) < fabs(vectorP(ls_" + lsId + ", " + vectorIndex + ", " + pIndex + "))) {" + NL
                + ind + IND + "if (!(lambda_mov < 0 || gamma_mov < 0 || lambda_mov + gamma_mov > 1)) {" + NL
                + ind + IND + IND + "dist_mov = fabs(dist_mov) * d_mov/fabs(d_mov) * " + segName + "Orientation[unionsI];" + NL
                + ind + IND + "} else {" + NL
                + ind + IND + IND + "//Find nearest point in ABC triangle and preserve union order" + NL
                + ind + IND + IND + "double distac = (pow(((x" + coord2 + " - a" + coord2 + "))*((c" + coord3 + " - a" + coord3 
                + ")) - ((x" + coord3 + " - a" + coord3 + "))*((c" + coord2 + " - a" + coord2 + ")), 2) + pow(((x" + coord3 + " - a" 
                + coord3 + "))*((c" + coord1 + " - a" + coord1 + ")) - ((x" + coord1 + " - a" + coord1 + "))*((c" + coord3 + " - a" 
                + coord3 + ")), 2) + pow(((x" + coord1 + " - a" + coord1 + "))*((c" + coord2 + " - a" + coord2 + ")) - ((x" + coord2 
                + " - a" + coord2 + "))*((c" + coord1 + " - a" + coord1 + ")), 2)) / ((c" + coord1 + " - a" + coord1 + ") * (c" 
                + coord1 + " - a" + coord1 + ") + (c" + coord2 + " - a" + coord2 + ") * (c" + coord2 + " - a" + coord2 + ") + (c" 
                + coord3 + " - a" + coord3 + ")*(c" + coord3 + " - a" + coord3 + "));" + NL
                + ind + IND + IND + "double distab = (pow(((x" + coord2 + " - a" + coord2 + "))*((b" + coord3 + " - a" + coord3
                + ")) - ((x" + coord3 + " - a" + coord3 + "))*((b" + coord2 + " - a" + coord2 + ")), 2) + pow(((x" + coord3 + " - a" 
                + coord3 + "))*((b" + coord1 + " - a" + coord1 + ")) - ((x" + coord1 + " - a" + coord1 + "))*((b" + coord3 + " - a" 
                + coord3 + ")), 2) + pow(((x" + coord1 + " - a" + coord1 + "))*((b" + coord2 + " - a" + coord2 + ")) - ((x" + coord2 
                + " - a" + coord2 + "))*((b" + coord1 + " - a" + coord1 + ")), 2)) / ((b" + coord1 + " - a" + coord1 + ") * (b" 
                + coord1 + " - a" + coord1 + ") + (b" + coord2 + " - a" + coord2 + ") * (b" + coord2 + " - a" + coord2 + ") + (b" 
                + coord3 + " - a" + coord3 + ")*(b" + coord3 + " - a" + coord3 + "));" + NL
                + ind + IND + IND + "double distbc = (pow(((x" + coord2 + " - b" + coord2 + "))*((c" + coord3 + " - b" + coord3
                + ")) - ((x" + coord3 + " - b" + coord3 + "))*((c" + coord2 + " - b" + coord2 + ")), 2) + pow(((x" + coord3 + " - b"
                + coord3 + "))*((c" + coord1 + " - b" + coord1 + ")) - ((x" + coord1 + " - b" + coord1 + "))*((c" + coord3 + " - b" 
                + coord3 + ")), 2) + pow(((x" + coord1 + " - b" + coord1 + "))*((c" + coord2 + " - b" + coord2 + ")) - ((x" + coord2 
                + " - b" + coord2 + "))*((c" + coord1 + " - b" + coord1 + ")), 2)) / ((c" + coord1 + " - b" + coord1 + ") * (c" 
                + coord1 + " - b" + coord1 + ") + (c" + coord2 + " - b" + coord2 + ") * (c" + coord2 + " - b" + coord2 + ") + (c" 
                + coord3 + " - b" + coord3 + ")*(c" + coord3 + " - b" + coord3 + "));" + NL
                + ind + IND + IND + "double np" + coord1 + "_a = orig_a" + coord1 + ";" + NL
                + ind + IND + IND + "double np" + coord2 + "_a = orig_a" + coord2 + ";" + NL
                + ind + IND + IND + "double np" + coord3 + "_a = orig_a" + coord3 + ";" + NL
                + ind + IND + IND + "double np" + coord1 + "_b = orig_b" + coord1 + ";" + NL
                + ind + IND + IND + "double np" + coord2 + "_b = orig_b" + coord2 + ";" + NL
                + ind + IND + IND + "double np" + coord3 + "_b = orig_b" + coord3 + ";" + NL
                + ind + IND + IND + "double np" + coord1 + "_c = orig_c" + coord1 + ";" + NL
                + ind + IND + IND + "double np" + coord2 + "_c = orig_c" + coord2 + ";" + NL
                + ind + IND + IND + "double np" + coord3 + "_c = orig_c" + coord3 + ";" + NL
                + ind + IND + IND + "if (distac < distab && distac < distbc) {" + NL
                + ind + IND + IND + IND + "np" + coord1 + "_b = a" + coord1 + ";" + NL
                + ind + IND + IND + IND + "np" + coord2 + "_b = a" + coord2 + ";" + NL
                + ind + IND + IND + IND + "np" + coord3 + "_b = a" + coord3 + ";" + NL
                + ind + IND + IND + IND + "np" + coord1 + "_c = b" + coord1 + ";" + NL
                + ind + IND + IND + IND + "np" + coord2 + "_c = b" + coord2 + ";" + NL
                + ind + IND + IND + IND + "np" + coord3 + "_c = b" + coord3 + ";" + NL
                + ind + IND + IND + IND + "np" + coord1 + "_a = c" + coord1 + ";" + NL
                + ind + IND + IND + IND + "np" + coord2 + "_a = c" + coord2 + ";" + NL
                + ind + IND + IND + IND + "np" + coord3 + "_a = c" + coord3 + ";" + NL
                + ind + IND + IND + "}" + NL
                + ind + IND + IND + "if (distbc < distab && distbc < distac) {" + NL
                + ind + IND + IND + IND + "np" + coord1 + "_a = b" + coord1 + ";" + NL
                + ind + IND + IND + IND + "np" + coord2 + "_a = b" + coord2 + ";" + NL
                + ind + IND + IND + IND + "np" + coord3 + "_a = b" + coord3 + ";" + NL
                + ind + IND + IND + IND + "np" + coord1 + "_b = c" + coord1 + ";" + NL
                + ind + IND + IND + IND + "np" + coord2 + "_b = c" + coord2 + ";" + NL
                + ind + IND + IND + IND + "np" + coord3 + "_b = c" + coord3 + ";" + NL
                + ind + IND + IND + IND + "np" + coord1 + "_c = a" + coord1 + ";" + NL
                + ind + IND + IND + IND + "np" + coord2 + "_c = a" + coord2 + ";" + NL
                + ind + IND + IND + IND + "np" + coord3 + "_c = a" + coord3 + ";" + NL
                + ind + IND + IND + "}" + NL
                + ind + IND + IND + "//Find adjacent triangle ABD" + NL
                + ind + IND + IND + "double e" + coord1 + ", e" + coord2 + ", e" + coord3 + ";" + NL
                + ind + IND + IND + "bool found = false;" + NL
                + ind + IND + IND + "for (int unionsI2 = 0; unionsI2 < UNIONS_" + segName + " && !found; unionsI2++) {" + NL
                + c
                + ind + IND + IND + IND + "if ((((equalsEq(cc" + coord1 + "2, np" + coord1 + "_a) && equalsEq(cc" + coord2 + "2, np" + coord2 
                + "_a)) && equalsEq(cc" + coord3 + "2, np" + coord3 + "_a) && (equalsEq(cc" + coord1 + "1, np" + coord1 + "_b) && equalsEq(cc" 
                + coord2 + "1, np" + coord2 + "_b)) && equalsEq(cc" + coord3 + "1, np" + coord3 + "_b)) || ((equalsEq(cc" + coord1 + "1, np"
                + coord1 + "_a) && equalsEq(cc" + coord2 + "1, np" + coord2 + "_a)) && equalsEq(cc" + coord3 + "1, np" + coord3 
                + "_a) && (equalsEq(cc" + coord1 + "2, np" + coord1 + "_b) && equalsEq(cc" + coord2 + "2, np" + coord2 
                + "_b)) && equalsEq(cc" + coord3 + "2, np" + coord3 + "_b))) && unionsI != unionsI2) {" + NL
                + ind + IND + IND + IND + IND + "e" + coord1 + " = cc" + coord1 + "3;" + NL
                + ind + IND + IND + IND + IND + "e" + coord2 + " = cc" + coord2 + "3;" + NL
                + ind + IND + IND + IND + IND + "e" + coord3 + " = cc" + coord3 + "3;" + NL
                + ind + IND + IND + IND + IND + "found = true;" + NL
                + ind + IND + IND + IND + "}" + NL
                + ind + IND + IND + IND + "if ((((equalsEq(cc" + coord1 + "3, np" + coord1 + "_a) && igual(cc" + coord2 + "3, np" + coord2 
                + "_a)) && equalsEq(cc" + coord3 + "3, np" + coord3 + "_a) && (equalsEq(cc" + coord1 + "1, np" + coord1 + "_b) && equalsEq(cc" 
                + coord2 + "1, np" + coord2 
                + "_b)) && equalsEq(cc" + coord3 + "1, np" + coord3 + "_b)) || ((equalsEq(cc" + coord1 + "1, np" + coord1 + "_a) && equalsEq(cc"
                + coord2 + "1, np" + coord2 
                + "_a)) && equalsEq(cc" + coord3 + "1, np" + coord3 + "_a) && (equalsEq(cc" + coord1 + "3, np" + coord1 + "_b) && equalsEq(cc" 
                + coord2 + "3, np" + coord2 
                + "_b)) && equalsEq(cc" + coord3 + "3, np" + coord3 + "_b))) && unionsI != unionsI2) {" + NL
                + ind + IND + IND + IND + IND + "e" + coord1 + " = cc" + coord1 + "2;" + NL
                + ind + IND + IND + IND + IND + "e" + coord2 + " = cc" + coord2 + "2;" + NL
                + ind + IND + IND + IND + IND + "e" + coord3 + " = cc" + coord3 + "2;" + NL
                + ind + IND + IND + IND + IND + "found = true;" + NL
                + ind + IND + IND + IND + "}" + NL
                + ind + IND + IND + IND + "if ((((equalsEq(cc" + coord1 + "3, np" + coord1 + "_a) && equalsEq(cc" + coord2 + "3, np" + coord2 
                + "_a)) && equalsEq(cc" + coord3 + "3, np" + coord3 + "_a) && (equalsEq(cc" + coord1 + "2, np" + coord1 + "_b) && equalsEq(cc" 
                + coord2 + "2, np" + coord2 
                + "_b)) && equalsEq(cc" + coord3 + "2, np" + coord3 + "_b)) || ((equalsEq(cc" + coord1 + "2, np" + coord1 + "_a) && equalsEq(cc"
                + coord2 + "2, np" + coord2 
                + "_a)) && equalsEq(cc" + coord3 + "2, np" + coord3 + "_a) && (equalsEq(cc" + coord1 + "3, np" + coord1 + "_b) && equalsEq(cc" 
                + coord2 + "3, np" + coord2 
                + "_b)) && equalsEq(cc" + coord3 + "3, np" + coord3 + "_b))) && unionsI != unionsI2) {" + NL
                + ind + IND + IND + IND + IND + "e" + coord1 + " = cc" + coord1 + "1;" + NL
                + ind + IND + IND + IND + IND + "e" + coord2 + " = cc" + coord2 + "1;" + NL
                + ind + IND + IND + IND + IND + "e" + coord3 + " = cc" + coord3 + "1;" + NL
                + ind + IND + IND + IND + IND + "found = true;" + NL
                + ind + IND + IND + IND + "}" + NL
                + ind + IND + IND + "}" + NL
                + ind + IND + IND + "if (!found) {" + NL
                + ind + IND + IND + IND + "TBOX_ERROR(\"Error Problem::getCellPoints: no adjacent triangle found\"<<  \"n\");" + NL
                + ind + IND + IND + "}" + NL
                + ind + IND + IND + "double u" + coord1 + " = (((b" + coord3 + " - a" + coord3 + ")*(c" + coord1 + " - a" + coord1 
                + ") - (b" + coord1 + " - a" + coord1 + ")*(c" + coord3 + " - a" + coord3 + ")) * (b" + coord3 + " - a" + coord3 
                + ") - ((b" + coord1 + " - a" + coord1 + ")*(c" + coord2 + " - a" + coord2 + ") - (b" + coord2 + " - a" + coord2 
                + ")*(c" + coord1 + " - a" + coord1 + ")) * (b" + coord2 + " - a" + coord2 + "));" + NL
                + ind + IND + IND + "double u" + coord2 + " = (((b" + coord1 + " - a" + coord1 + ")*(c" + coord2 + " - a" + coord2 
                + ") - (b" + coord2 + " - a" + coord2 + ")*(c" + coord1 + " - a" + coord1 + ")) * (b" + coord1 + " - a" + coord1 
                + ") - ((b" + coord2 + " - a" + coord2 + ")*(c" + coord3 + " - a" + coord3 + ") - (b" + coord3 + " - a" + coord3 
                + ")*(c" + coord2 + " - a" + coord2 + ")) * (b" + coord3 + " - a" + coord3 + "));" + NL
                + ind + IND + IND + "double u" + coord3 + " = (((b" + coord2 + " - a" + coord2 + ")*(c" + coord3 + " - a" + coord3 
                + ") - (b" + coord3 + " - a" + coord3 + ")*(c" + coord2 + " - a" + coord2 + ")) * (b" + coord2 + " - a" + coord2 
                + ") - ((b" + coord3 + " - a" + coord3 + ")*(c" + coord1 + " - a" + coord1 + ") - (b" + coord1 + " - a" + coord1 
                + ")*(c" + coord3 + " - a" + coord3 + ")) * (b" + coord1 + " - a" + coord1 + "));" + NL
                + ind + IND + IND + "double v" + coord1 + " = (((b" + coord3 + " - a" + coord3 + ")*(e" + coord1 + " - a" + coord1 
                + ") - (b" + coord1 + " - a" + coord1 + ")*(e" + coord3 + " - a" + coord3 + ")) * (b" + coord3 + " - a" + coord3 
                + ") - ((b" + coord1 + " - a" + coord1 + ")*(e" + coord2 + " - a" + coord2 + ") - (b" + coord2 + " - a" + coord2 
                + ")*(e" + coord1 + " - a" + coord1 + ")) * (b" + coord2 + " - a" + coord2 + "));" + NL
                + ind + IND + IND + "double v" + coord2 + " = (((b" + coord1 + " - a" + coord1 + ")*(e" + coord2 + " - a" + coord2 
                + ") - (b" + coord2 + " - a" + coord2 + ")*(e" + coord1 + " - a" + coord1 + ")) * (b" + coord1 + " - a" + coord1 
                + ") - ((b" + coord2 + " - a" + coord2 + ")*(e" + coord3 + " - a" + coord3 + ") - (b" + coord3 + " - a" + coord3 
                + ")*(e" + coord2 + " - a" + coord2 + ")) * (b" + coord3 + " - a" + coord3 + "));" + NL
                + ind + IND + IND + "double v" + coord3 + " = (((b" + coord2 + " - a" + coord2 + ")*(e" + coord3 + " - a" + coord3 
                + ") - (b" + coord3 + " - a" + coord3 + ")*(e" + coord2 + " - a" + coord2 + ")) * (b" + coord2 + " - a" + coord2
                + ") - ((b" + coord3 + " - a" + coord3 + ")*(e" + coord1 + " - a" + coord1 + ") - (b" + coord1 + " - a" + coord1 
                + ")*(e" + coord3 + " - a" + coord3 + ")) * (b" + coord1 + " - a" + coord1 + "));" + NL
                + ind + IND + IND + "double k_mov_" + coord1 + " = x" + coord1 + " + axab/(modab * modab) * (b" + coord1 + " - a" + coord1 + ");" + NL
                + ind + IND + IND + "double k_mov_" + coord2 + " = x" + coord2 + " + axab/(modab * modab) * (b" + coord2 + " - a" + coord2 + ");" + NL
                + ind + IND + IND + "double k_mov_" + coord3 + " = x" + coord3 + " + axab/(modab * modab) * (b" + coord3 + " - a" + coord3 + ");" + NL
                + ind + IND + IND + "double uv = x" + coord1 + " + x" + coord2 + " + x" + coord3 + " + axab/(modab * modab) * ((b"
                + coord1 + " - a" + coord1 + ") + (b" + coord2 + " - a" + coord2 + ") + (b" + coord3 + " - a" + coord3 + "));" + NL
                + ind + IND + IND + "double b1 = u" + coord2 + " * v" + coord3 + " - u" + coord3 + " * v" + coord2 + ";" + NL
                + ind + IND + IND + "double b2 = u" + coord3 + " * v" + coord1 + " - u" + coord1 + " * v" + coord3 + ";" + NL
                + ind + IND + IND + "double b3 = u" + coord1 + " * v" + coord2 + " - u" + coord2 + " * v" + coord1 + ";" + NL
                + ind + IND + IND + "if (lessThan(0, (u" + coord2 + " * (x" + coord3 + " - k_mov_" + coord3 + ") - u" + coord3 + " * (x"
                + coord2 + " - k_mov_" + coord2 + "))*b1 + (u" + coord3 + " * (x" + coord1 + " - k_mov_" + coord1 + ") - u" + coord1
                + " * (x" + coord3 + " - k_mov_" + coord3 + "))*b2 + (u" + coord1 + " * (x" + coord2 + " - k_mov_" + coord2 + ") - u"
                + coord2 + " * (x" + coord1 + " - k_mov_" + coord1 + "))*b3) && lessThan(((x" + coord2 + " - k_mov_" + coord2 + ") * v" 
                + coord3 + " - (x" + coord3 + " - k_mov_" + coord3 + ") * v" + coord2 + ")*b1 + ((x" + coord3 + " - k_mov_" + coord3 
                + ") * v" + coord1 + " - (x" + coord1 + " - k_mov_" + coord1 + ") * v" + coord3 + ")*b2 + ((x" + coord1 + " - k_mov_" 
                + coord1 + ") * v" + coord2 + " - (x" + coord2 + " - k_mov_" + coord2 + ") * v" + coord1 + ")*b3, 0)) {" + NL
                + ind + IND + IND + IND + "dist_mov = -dist_mov;" + NL
                + ind + IND + IND + "}" + NL
                + ind + IND + "}" + NL
                + ind + IND + "if (fabs(dist_mov) < fabs(vectorP(ls_" + lsId + ", " + vectorIndex + ", " + pIndex + "))) {" + NL
                + ind + IND + IND + "vectorP(ls_" + lsId + ", " + vectorIndex + ", " + pIndex + ") = dist_mov;" + NL
                + segLSAssignment
                + ind + IND + "}" + NL
                + ind + "}" + NL;
    }
    
    /**
     * Creates the code to calculate the variables related to time for x3d movement.
     * @param pi            The problem info
     * @param ind           The indent
     * @return              The code
     * @throws CGException  CG00X External error
     */
    private static String calculateTimes(ProblemInfo pi, String ind) throws CGException {

        String cubicTimeInterpolation = "";
        for (int j = 0; j < pi.getMovementInfo().getX3dMovRegions().size(); j++) {
            String x3dName = pi.getMovementInfo().getX3dMovRegions().get(j);
            
            String auxVars = "";
            String calculations = "";
            for (int i = 0; i < pi.getDimensions(); i++) {
                String coord = pi.getCoordinates().get(i);
                String contCoord = CodeGeneratorUtils.discToCont(pi.getProblem(), coord);
                auxVars = auxVars + ind + IND + "double " + contCoord + "Auxm1 = " + x3dName + "CellPoints[max(0,tsIprev_" + x3dName 
                        + "-1)][points]["  + i + "];" + NL
                        + ind + IND + "double " + contCoord + "Aux0  = " + x3dName + "CellPoints[tsIprev_" + x3dName + "][points][" + i + "];" + NL
                        + ind + IND + "double " + contCoord + "Aux1  = " + x3dName + "CellPoints[tsIpost_" + x3dName + "][points][" + i + "];" + NL
                        + ind + IND + "double " + contCoord + "Aux2  = " + x3dName + "CellPoints[min(TIMESLICES-1,tsIpost_" + x3dName 
                        + "+1)][points][" + i + "];" + NL;
                calculations = calculations + ind + IND + x3dName + "TimeCoeff[points][" + i + "][0] = " + contCoord + "Aux0;" + NL
                        + ind + IND + x3dName + "TimeCoeff[points][" + i + "][1] = (" + contCoord + "Aux1-" + contCoord + "Auxm1)/2.0;" + NL
                        + ind + IND + x3dName + "TimeCoeff[points][" + i + "][2] = " + contCoord + "Auxm1 - 2.5*" + contCoord + "Aux0 + 2.0*" 
                        + contCoord + "Aux1 - 0.5*" + contCoord + "Aux2;" + NL
                        + ind + IND + x3dName + "TimeCoeff[points][" + i + "][3] = 0.5*(-" + contCoord + "Auxm1 + 3.0*" + contCoord 
                        + "Aux0 - 3.0*" + contCoord + "Aux1 + " + contCoord + "Aux2);" + NL;
            }
            
            cubicTimeInterpolation = cubicTimeInterpolation + ind + "int tsIprev_" + x3dName + " = 0;" + NL
                + ind + "int tsIpost_" + x3dName + " = TIMESLICES - 1;" + NL
                + ind + "int tsIprev_p_" + x3dName + " = 0;" + NL
                + ind + "int tsIpost_p_" + x3dName + " = TIMESLICES - 1;" + NL
                + ind + "for (int timeSlices = 0; timeSlices < TIMESLICES; timeSlices++) {" + NL
                + ind + IND + "if (" + x3dName + "Time[timeSlices] <= time && " + x3dName + "Time[timeSlices] > " + x3dName
                + "Time[tsIprev_" + x3dName + "]) {" + NL
                + ind + IND + IND + "tsIprev_" + x3dName + " = timeSlices;" + NL
                + ind + IND + "}" + NL
                + ind + IND + "if (" + x3dName + "Time[timeSlices] > time && " + x3dName + "Time[timeSlices] < " + x3dName 
                + "Time[tsIpost_" + x3dName + "]) {" + NL
                + ind + IND + IND + "tsIpost_" + x3dName + " = timeSlices;" + NL
                + ind + IND + "}" + NL
                + ind + IND + "if (" + x3dName + "Time[timeSlices] <= time - dt && " + x3dName + "Time[timeSlices] > " + x3dName 
                + "Time[tsIprev_p_" + x3dName + "]) {" + NL
                + ind + IND + IND + "tsIprev_p_" + x3dName + " = timeSlices;" + NL
                + ind + IND + "}" + NL
                + ind + IND + "if (" + x3dName + "Time[timeSlices] > time - dt && " + x3dName + "Time[timeSlices] < " + x3dName 
                + "Time[tsIpost_p_" + x3dName + "]) {" + NL
                + ind + IND + IND + "tsIpost_p_" + x3dName + " = timeSlices;" + NL
                + ind + IND + "}" + NL
                + ind + "}" + NL + NL
                + ind + "double tfrac_" + x3dName + " = ((time) - " + x3dName + "Time[tsIprev_" + x3dName + "])/(" + x3dName 
                + "Time[tsIpost_" + x3dName + "] - " + x3dName + "Time[tsIprev_" + x3dName + "]);" + NL
                + ind + "double tfrac_p_" + x3dName + " = ((time - dt) - " + x3dName + "Time[tsIprev_p_" + x3dName + "])/(" + x3dName 
                + "Time[tsIpost_p_" + x3dName + "] - " + x3dName + "Time[tsIprev_p_" + x3dName + "]);" + NL
                + ind + "double d_tfrac_p_dt_" + x3dName + " = 1.0 / (" + x3dName + "Time[tsIpost_p_" + x3dName + "] - " + x3dName 
                + "Time[tsIprev_p_" + x3dName + "]);" + NL
                + ind + "if (Equals(" + x3dName + "Time[tsIpost_" + x3dName + "], " + x3dName + "Time[tsIprev_" + x3dName + "])) {" + NL
                + ind + IND + "tfrac_" + x3dName + " = 0;" + NL
                + ind + "}" + NL
                + ind + "if (Equals(" + x3dName + "Time[tsIpost_p_" + x3dName + "], " + x3dName + "Time[tsIprev_p_" + x3dName + "])) {" + NL
                + ind + IND + "tfrac_p_" + x3dName + " = 0;" + NL
                + ind + "}" + NL
                + ind + "double timedif_" + x3dName + " = " + x3dName + "Time[tsIpost_" + x3dName + "] - " + x3dName 
                + "Time[tsIprev_p_" + x3dName + "];" + NL
                + ind + "double " + x3dName + "TimeCoeff[POINTS_" + x3dName + "][DIMENSIONS][4];" + NL
                + ind + "for (int points = 0; points < POINTS_" + x3dName + "; points++) {" + NL
                + auxVars
                + calculations
                + ind + "}" + NL + NL;
        }
        
        return ind + "//Get timeslices previous/post" + NL
                + ind + "/*************************************************************" + NL
                + ind + "  CUBIC TIME INTERPOLATION, ASSUMING EQUALLY SPACED X3Ds" + NL
                + ind + "*************************************************************/" + NL
                + cubicTimeInterpolation;
    }
    
    /**
     * Creates the level set variable declaration.
     * @param pi    The problem info
     * @param ind   The indent
     * @return      The code
     */
    private static String lsDeclaration(ProblemInfo pi, String ind) {
        String arrayAccess = "";
        for (int i = 0; i < pi.getDimensions(); i++) {
            arrayAccess = arrayAccess + "[" + INTERPHASELENGTH + "]";
        }
        String declaration = "";
        String initialization = "";
        ArrayList<String> elements = new ArrayList<String>();
        for (int i = 0; i < INTERPHASELENGTH; i++) {
            elements.add(String.valueOf(i));
        }
        ArrayList<String> combinations = CodeGeneratorUtils.createCombinations(pi.getDimensions(), elements);
        for (int i = 0; i < pi.getInteriorRegionIds().size(); i++) {
            String segId = pi.getRegionIds().get(i);
            declaration = declaration + ind + "double* ls_" + segId + arrayAccess + ";" + NL;
            for (int j = 0; j < combinations.size(); j++) {
                String comb = combinations.get(j);
                String aIndex = "";
                for (int k = 0; k < comb.length(); k++) {
                    aIndex = aIndex + "[" + comb.substring(k, k + 1) + "]";
                }
                initialization = initialization + ind + "ls_" + segId + aIndex + " = ((pdat::CellData<double> *) "
                        + "patch->getPatchData(d_ls_" + segId + "_" + comb + "_id).get())->getPointer();" + NL;
            }
        }
        return declaration
                + initialization;
    }
    
    /**
     * Creates the cell level set and cell normal variables declaration.
     * @param pi            The problem info
     * @param ind           The indent
     * @param onlyNormal    Generate only normals
     * @param onlyLS        Generate only level sets
     * @return              The code
     */
    private static String lsNormalDeclaration(ProblemInfo pi, String ind, boolean onlyNormal, boolean onlyLS) {

        String lsDeclaration = "";
        if (!onlyNormal || onlyLS) {
            for (int i = 0; i < pi.getInteriorRegionIds().size(); i++) {
                String segId = pi.getInteriorRegionIds().get(i);
                lsDeclaration = lsDeclaration + ind + "double* LS_" + segId + " = ((pdat::CellData<double> *) patch->getPatchData("
                        + "d_LS_" + segId + "_id).get())->getPointer();" + NL;
            }
        }

        String normalDeclaration = "";
        if (onlyNormal || !onlyLS) {
            ArrayList<String> tmp = new ArrayList<String>();
            tmp.addAll(pi.getInteriorRegionIds());
            while (!tmp.isEmpty()) {
                String id = tmp.remove(0);
                for (int i = 0; i < tmp.size(); i++) {
                    String comb = id + tmp.get(i);
                    for (int j = 0; j < pi.getDimensions(); j++) {
                        String coord = pi.getCoordinates().get(j);
                        normalDeclaration = normalDeclaration + ind + "double* normal" + coord + "_" + comb + " = ((pdat::" 
                                + "CellData<double> *) patch->getPatchData(d_normal" + coord + "_" + comb + "_id).get())->getPointer();" + NL;
                    }
                }
            }
        }
        return lsDeclaration
                + normalDeclaration;
    }
    
    /**
     * Creates the declaration for the getCellPoints function.
     * @param pi            The problem info
     * @return              The declaration
     */
    private static String createGetCellPointsDeclaration(ProblemInfo pi) {
        if (pi.getMovementInfo().getX3dMovRegions().size() == 0) {
            return "";
        }
		return IND + "void getCellPoints(const std::shared_ptr< hier::PatchLevel > level, int timeSlice);" + NL;
    }
    
    /**
     * Creates the declaration for the getCellPoints function.
     * @param pi            The problem info
     * @return              The declaration
     */
    private static String createRestoreCellPointsDeclaration(ProblemInfo pi) {
        if (pi.getMovementInfo().getX3dMovRegions().size() == 0) {
            return "";
        }
		return IND + "void getTimeSlices(MainRestartData* mrd);" + NL
		        + IND + "void putTimeSlices(MainRestartData* mrd);" + NL;
    }
    
    /**
     * Creates the code to calculate the cell points from a x3d.
     * @param pi            The problem info
     * @return              The code
     * @throws CGException  CG00X External error
     */
    private static String createGetCellPoints(ProblemInfo pi) throws CGException {
        if (pi.getMovementInfo().getX3dMovRegions().size() == 0) {
            return "";
        }
        String indent = IND + IND;
        for (int i = 0; i < pi.getDimensions(); i++) {
            indent = indent + IND;
        }
        String lsCalculation = "";
        for (int i = 0; i < pi.getMovementInfo().getX3dMovRegions().size(); i++) {
            String segName = pi.getMovementInfo().getX3dMovRegions().get(i);
            lsCalculation = lsCalculation + lsCalculation(pi, IND + IND, segName, false);
        }
        
        return "void Problem::getCellPoints(const std::shared_ptr< hier::PatchLevel > level, int timeSlice) {" + NL
                + IND + "const tbox::SAMRAI_MPI& mpi(tbox::SAMRAI_MPI::getSAMRAIWorld());" + NL
                + IND + "for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {" + NL
                + IND + IND + "const std::shared_ptr< hier::Patch >& patch = *p_it;" + NL
                + IND + IND + "//Get the dimensions of the patch" + NL
                + IND + IND + "std::shared_ptr< pdat::IndexData<Interphase, pdat::CellGeometry > > interphase(patch->getPatchData("
                + "d_interphase_id), SAMRAI_SHARED_PTR_CAST_TAG);" + NL
                + IND + IND + "int* soporte = ((pdat::CellData<int> *) patch->getPatchData(d_soporte_id).get())->getPointer();" + NL
                + lsDeclaration(pi, IND + IND)
                + SAMRAIUtils.createFOVDeclaration(pi.getRegionIds(), IND + IND, "patch->")
                + IND + IND + "const hier::Index boxfirst = patch->getBox().lower();" + NL
                + IND + IND + "const hier::Index boxlast  = patch->getBox().upper();" + NL
                + IND + IND + "const hier::Index boxfirst1 = interphase->getGhostBox().lower();" + NL
                + IND + IND + "const hier::Index boxlast1  = interphase->getGhostBox().upper();" + NL
                + IND + IND + "//Get delta spaces into an array. dx, dy, dz." + NL
                + IND + IND + "const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));" + NL
                + IND + IND + "const double* dx  = patch_geom->getDx();" + NL
                + SAMRAIUtils.getLasts(pi, IND + IND, true) + NL
                + createThresholds(pi, IND + IND) + NL
                + calculateCellPoints(pi, IND + IND)
                + IND + "}" + NL
                + IND + "if (mpi.getSize() > 1) {" + NL
                + communicateCellPoints(pi, IND + IND)
                + IND + "}" + NL + NL
                + IND + "//Calculate orientations" + NL
                + IND + "for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {" + NL
                + IND + IND + "const std::shared_ptr< hier::Patch >& patch = *p_it;" + NL
                + IND + IND + "//Get the dimensions of the patch" + NL
                + IND + IND + "std::shared_ptr< pdat::IndexData<Interphase, pdat::CellGeometry > > interphase(patch->getPatchData(" 
                + "d_interphase_id), SAMRAI_SHARED_PTR_CAST_TAG);" + NL
                + IND + IND + "int* soporte = ((pdat::CellData<int> *) patch->getPatchData(d_soporte_id).get())->getPointer();" + NL
                + SAMRAIUtils.createFOVDeclaration(pi.getRegionIds(), IND + IND, "patch->")
                + IND + IND + "const hier::Index boxfirst = patch->getBox().lower();" + NL
                + IND + IND + "const hier::Index boxlast  = patch->getBox().upper();" + NL
                + IND + IND + "const hier::Index boxfirst1 = interphase->getGhostBox().lower();" + NL
                + IND + IND + "const hier::Index boxlast1  = interphase->getGhostBox().upper();" + NL
                + IND + IND + "//Get delta spaces into an array. dx, dy, dz." + NL
                + IND + IND + "const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));" + NL
                + IND + IND + "const double* dx  = patch_geom->getDx();" + NL
                + SAMRAIUtils.getLasts(pi, IND + IND, true) + NL
                + IND + IND + "int orient_acc;" + NL
                + IND + IND + "bool any_zero_value;" + NL
                + calculateOrientations(pi, IND + IND)
                + IND + "}" + NL + NL
                + IND + "if (mpi.getSize() > 1) {" + NL
                + communicateOrientations(pi, IND + IND)
                + IND + "}" + NL
                + IND + "//Calculate level set" + NL
                + IND + "for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {" + NL
                + IND + IND + "const std::shared_ptr< hier::Patch >& patch = *p_it;" + NL
                + IND + IND + "//Get the dimensions of the patch" + NL
                + IND + IND + "std::shared_ptr< pdat::IndexData<Interphase, pdat::CellGeometry > > interphase(patch->getPatchData("
                + "d_interphase_id), SAMRAI_SHARED_PTR_CAST_TAG);" + NL
                + IND + IND + "int* soporte = ((pdat::CellData<int> *) patch->getPatchData(d_soporte_id).get())->getPointer();" + NL
                + SAMRAIUtils.createFOVDeclaration(pi.getRegionIds(), IND + IND, "patch->")
                + lsDeclaration(pi, IND + IND)
                + IND + IND + "const hier::Index boxfirst = patch->getBox().lower();" + NL
                + IND + IND + "const hier::Index boxlast  = patch->getBox().upper();" + NL
                + IND + IND + "const hier::Index boxfirst1 = interphase->getGhostBox().lower();" + NL
                + IND + IND + "const hier::Index boxlast1  = interphase->getGhostBox().upper();" + NL
                + IND + IND + "//Get delta spaces into an array. dx, dy, dz." + NL
                + IND + IND + "const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));" + NL
                + IND + IND + "const double* dx  = patch_geom->getDx();" + NL
                + SAMRAIUtils.getLasts(pi, IND + IND, true) + NL
                + createThresholds(pi, IND + IND) + NL
                + IND + IND + "// Update LS mapping with fresh computed CellPoints" + NL
                + loopInitLasts(pi.getCoordinates(), IND + IND)
                + lsInitialization(pi, indent)
                + loopEnd(pi.getDimensions(), IND + IND)
                + IND + IND + "int minBlock[" + pi.getDimensions() + "], maxBlock[" + pi.getDimensions() + "];" + NL
                + lsCalculation
                + addBoundaryParticleSupport(pi, IND + IND)
                + IND + "}" + NL
                + "}" + NL;
    }
    
    /**
     * Creates the code to allow to restore the cell points from a x3d.
     * @param pi            The problem info
     * @return              The code
     * @throws CGException  CG00X External error
     */
    private static String createRestoreCellPoints(ProblemInfo pi) throws CGException {
        if (pi.getMovementInfo().getX3dMovRegions().size() == 0) {
            return "";
        }
        String store = "";
        String load = "";
        for (int i = 0; i < pi.getMovementInfo().getX3dMovRegions().size(); i++) {
            String segName = pi.getMovementInfo().getX3dMovRegions().get(i);
            store = store + IND + "double buffer_" + segName + "[TIMESLICES * POINTS_" + segName + " * DIMENSIONS];" + NL
                    + IND + "for (int ts = 0; ts < TIMESLICES; ts++) {" + NL
                    + IND + IND + "for (int point = 0; point < POINTS_" + segName + "; point++) {" + NL
                    + IND + IND + IND + "for (int coord = 0; coord < DIMENSIONS; coord++) {" + NL
                    + IND + IND + IND + IND + "buffer_" + segName + "[ts * (POINTS_" + segName 
                    + " * DIMENSIONS) + point * DIMENSIONS + coord] = " + segName + "CellPoints[ts][point][coord];" + NL
                    + IND + IND + IND + "}" + NL
                    + IND + IND + "}" + NL
                    + IND + "}" + NL
                    + IND + "mrd->add" + segName + "CellPoints(buffer_" + segName + ");" + NL;
            load = load + IND + "double* buffer_" + segName + " = mrd->get" + segName + "CellPoints();" + NL
                    + IND + "for (int ts = 0; ts < TIMESLICES; ts++) {" + NL
                    + IND + IND + "for (int point = 0; point < POINTS_" + segName + "; point++) {" + NL
                    + IND + IND + IND + "for (int coord = 0; coord < DIMENSIONS; coord++) {" + NL
                    + IND + IND + IND + IND + segName + "CellPoints[ts][point][coord] = buffer_" + segName + "[ts * (POINTS_" + segName 
                    + " * DIMENSIONS) + point * DIMENSIONS + coord];" + NL
                    + IND + IND + IND + "}" + NL
                    + IND + IND + "}" + NL
                    + IND + "}" + NL;
        }
        
        return "void Problem::putTimeSlices(MainRestartData* mrd) {" + NL
                + store
                + "}" + NL + NL
                + "void Problem::getTimeSlices(MainRestartData* mrd) {" + NL
                + load
                + "}" + NL;
    }

    /**
     * Creates the code to initialize the level set variables.
     * @param pi        The problem info
     * @param ind       The indent
     * @return          The code
     */
    private static String lsInitialization(ProblemInfo pi, String ind) {
        String ls = "";
        ArrayList<String> elements = new ArrayList<String>();
        for (int i = 0; i < INTERPHASELENGTH; i++) {
            elements.add(String.valueOf(i));
        }
        ArrayList<String> combinations = CodeGeneratorUtils.createCombinations(pi.getDimensions(), elements);
        String index = "";
        for (int i = 0; i < pi.getDimensions(); i++) {
            index = index + pi.getCoordinates().get(i) + ", ";
        }
        index = index.substring(0, index.length() - 2);
        for (int i = 0; i < pi.getInteriorRegionIds().size(); i++) {
            String id = pi.getInteriorRegionIds().get(i);
            for (int j = 0; j < combinations.size(); j++) {
                String comb = combinations.get(j);
                String pIndex = "";
                for (int k = 0; k < comb.length(); k++) {
                    pIndex = pIndex + comb.substring(k, k + 1) + ", ";
                }
                pIndex = pIndex.substring(0, pIndex.length() - 2);
                ls = ls + ind + "vectorP(ls_" + id + ", " + index + ", " + pIndex + ") = 99999999999;" + NL;
            }
        }
        return ls;
    }
    
    /**
     * Creates the code to calculate the cell points.
     * @param pi                The problem info
     * @param ind               The indent
     * @return                  The code
     * @throws CGException      CG00X External error
     */
    private static String calculateCellPoints(ProblemInfo pi, String ind) throws CGException {
        String[][] xslParams = new String [THREE][2];
        xslParams[0][0] = "condition";
        xslParams[0][1] = "true";
        xslParams[1][0] = "indent";
        xslParams[1][1] = "0";
        xslParams[2][0] =  "dimensions";
        xslParams[2][1] =  String.valueOf(pi.getCoordinates().size());
        String cuadradoCellPoints = "";
        String termCondition = "";
        String termVIndex = "";
        String termIndex = "";
        String iVectorIndex = "";
        String iVectorIndex2 = "";
        String pIndex = "";
        String normalDec = "";
        String normalCond = "";
        String indexCond = "";
        String n = "";
        String nAssign = "";
        String startCellLoop = "";
        String startFloorLoop = "";
        String startCeilLoop = "";
        String vectorIndex = "";
        String insideIndex = "";
        String indexes = "";
        String indexes2 = "";
        String indexes3 = "";
        String insideCond = "";
        String projAux = "";
        String floor = "";
        String ceil = "";
        String indent = ind + IND + IND + IND + IND;
        String indent2 = ind + IND + IND + IND + IND + IND + IND;
        for (int j = 0; j < pi.getDimensions(); j++) {
            String coord = pi.getCoordinates().get(j);
            termCondition = termCondition + coord + "term <= boxlast(" + j + ") && " + coord + "term >= boxfirst(" + j + ") && ";
            insideCond = insideCond + coord + " <= boxlast(" + j + ") && " + coord + " >= boxfirst(" + j + ") && ";
            termVIndex = termVIndex + coord + "term - boxfirst1(" + j + "), ";
            iVectorIndex = iVectorIndex + "index" + coord + "/acc - boxfirst1(" + j + "), ";
            iVectorIndex2 = iVectorIndex2 + "(int)ceil(((double)index" + coord + ")/acc) - boxfirst1(" + j + "), ";
            termIndex = termIndex + coord + "term, ";
            pIndex = pIndex + "p" + (j + 1) + ", ";
            vectorIndex = vectorIndex + coord + " - boxfirst1(" + j + "), ";
            startCellLoop = startCellLoop + indent + "for (int " + coord + " = cell" + coord + " - 1; " + coord + " <= cell" + coord + " + 1; " 
                    + coord + "++) {" + NL;
            startFloorLoop = startFloorLoop + indent2 + "for (int " + coord + " = floor(floor" + coord + "/dx[" + j + "]) - 1; " + coord 
                    + " <= floor(floor" + coord + "/dx[" + j + "]); " + coord + "++) {" + NL;
            startCeilLoop = startCeilLoop + indent2 + "for (int " + coord + " = floor(ceil" + coord + "/dx[" + j + "]) - 1; " + coord 
                    + " <= floor(ceil" + coord + "/dx[" + j + "]); " + coord + "++) {" + NL;
            indent = indent + IND;
            indent2 = indent2 + IND;
            insideIndex = insideIndex + "cell" + coord + " - min(1, max(-1,n" + coord + ")) - boxfirst1(" + j + "), ";
            projAux = projAux + "n" + coord + "*(" + coord + "-(cell" + coord + " - min(1, max(-1,n" + coord + ")))) + ";
            indexCond = indexCond + "index" + coord + " % max(1,acc) != 0 || ";
        }
        pIndex = pIndex.substring(0, pIndex.lastIndexOf(", "));
        for (int j = 0; j < pi.getDimensions(); j++) {
            String coord = pi.getCoordinates().get(j);
            normalDec = normalDec + ind + IND + IND + IND + "double normal" + coord + " = vectorP_r(normal" + coord + ", sop, " 
                    + pIndex + ");" + NL;
            normalCond = normalCond + "fabs(normal" + coord + "*ls/dx[" + j + "]) < 1.5*d_ghost_width && ";
            n = n + ind + IND + IND + IND + IND + "int n" + coord + " = 0;" + NL;
            nAssign = nAssign + indent + IND + "n" + coord + " = n" + coord + " + (" + coord + " - cell" + coord + ");" + NL;
            indexes = indexes + ind + IND + IND + IND + IND + "int index" + coord + " = 0;" + NL
                    + ind + IND + IND + IND + IND + "int index" + coord + "1 = 0;" + NL;
            indexes2 = indexes2 + indent + IND + IND + IND + "index" + coord + " = " + coord + ";" + NL
                    + indent + IND + IND + IND + "index" + coord + "1 = " + coord + ";" + NL;
            indexes3 = indexes3 + indent + IND + IND + IND + IND + "index" + coord + " = index" + coord + " + " + coord + ";" + NL;
            floor = floor + ind + IND + IND + IND + IND + IND + IND + "double floor" + coord + " = (index" + coord
                    + "/acc + 0.5 - sign*min<double>(0.5, max<double>(-0.5,n" + coord + ")))*dx[" + j 
                    + "] + d_grid_geometry->getXLower()[" + j + "];" + NL;
            ceil = ceil + ind + IND + IND + IND + IND + IND + IND + "double ceil" + coord + " = (ceil(((double)index" 
                    + coord + ")/acc) + 0.5 - sign*min<double>(0.5, max<double>(-0.5,n" + coord + ")))*dx[" + j 
                    + "] + d_grid_geometry->getXLower()[" + j + "];" + NL;
        }
        indexCond = indexCond.substring(0, indexCond.lastIndexOf(" ||"));
        termCondition = termCondition.substring(0, termCondition.lastIndexOf(" &&"));
        insideCond = insideCond.substring(0, insideCond.lastIndexOf(" &&"));
        normalCond = normalCond.substring(0, normalCond.lastIndexOf(" &&"));
        termIndex = termIndex.substring(0, termIndex.lastIndexOf(", "));
        termVIndex = termVIndex.substring(0, termVIndex.lastIndexOf(", "));
        vectorIndex = vectorIndex.substring(0, vectorIndex.lastIndexOf(", "));
        insideIndex = insideIndex.substring(0, insideIndex.lastIndexOf(", "));
        iVectorIndex = iVectorIndex.substring(0, iVectorIndex.lastIndexOf(", "));
        iVectorIndex2 = iVectorIndex2.substring(0, iVectorIndex2.lastIndexOf(", "));
        projAux = projAux.substring(0, projAux.lastIndexOf(" + "));
        for (int i = 0; i < pi.getMovementInfo().getX3dMovRegions().size(); i++) {
            String segName = pi.getMovementInfo().getX3dMovRegions().get(i);
            String segId = CodeGeneratorUtils.getRegionId(pi.getProblem(), pi.getRegions(), segName);
            String terms = "";
            String cellInit = "";
            String p = "";
            String cell = "";
            String cellPoints1 = "";
            String cellPoints2 = "";
            String cellPoints3 = "";
            String cellPoints4 = "";
            for (int j = 0; j < pi.getDimensions(); j++) {
                String coord = pi.getCoordinates().get(j);
                terms = terms + ind + IND + "int " + coord + "term = floor((" + segName + "Points[timeSlice][points][" + j 
                        + "] - d_grid_geometry->getXLower()[" + j + "]) / dx[" + j + "]);" + NL;
                cellInit = cellInit + ind + IND + IND + segName + "CellPoints[timeSlice][points][" + j + "] = " + segName 
                        + "Points[timeSlice][points][" + j + "];" + NL;
                p = p + ind + IND + IND + IND + "int p" + (j + 1) + " = min<double>(2, max<double>(0, floor(1 + (" + segName 
                        + "Points[timeSlice][points][" 
                        + j + "] - (" + coord + "term + 0.5)*dx[" + j + "]) / (dx[" + j + "]/" + INTERPHASELENGTH + "))));" + NL;
                cell = cell + ind + IND + IND + IND + IND + "int cell" + coord + " = floor((" + segName + "Points[timeSlice][points][" 
                        + j + "] + normal" + coord + "*ls - d_grid_geometry->getXLower()[" + j + "]) /dx[" + j + "]);" + NL;
                cellPoints1 = cellPoints1 + ind + IND + IND + IND + IND + segName + "CellPoints[timeSlice][points][" + j + "] = (cell" 
                        + coord + " + 0.5)*dx[" + j + "];" + NL;
                cellPoints2 = cellPoints2 + ind + IND + IND + IND + IND + IND + IND + IND + segName + "CellPoints[timeSlice][points][" 
                        + j + "] = floor" + coord + ";" + NL;
                cellPoints3 = cellPoints3 + ind + IND + IND + IND + IND + IND + IND + IND + segName + "CellPoints[timeSlice][points][" 
                        + j + "] = ceil" + coord + ";" + NL;
                cellPoints4 = cellPoints4 + ind + IND + IND + IND + IND + IND + IND + IND + segName + "CellPoints[timeSlice][points][" 
                        + j + "] = (index" + coord + "/acc + 0.5 - sign*min<double>(0.5, max<double>(-0.5,n" + coord + ")))*dx[" + j
                        + "] + d_grid_geometry->getXLower()[" + j + "];" + NL;
            }
            cuadradoCellPoints = cuadradoCellPoints + ind + "for (int points = 0; points < POINTS_" + segName + "; points++) {" + NL
                    + terms
                    + ind + IND + "if (" + termCondition + ") {" + NL
                    + cellInit
                    + ind + IND + IND + "if (vector(soporte, " + termVIndex + ") == 1) {" + NL
                    + ind + IND + IND + IND + "hier::Index idx(" + termIndex + ");" + NL
                    + ind + IND + IND + IND + "Interphase* sop = interphase->getItem(idx);" + NL
                    + p
                    + ind + IND + IND + IND + "double ls = fabs(vectorP(ls_" + segId + ", " + termVIndex + ", " + pIndex + "));" + NL
                    + normalDec + NL
                    + ind + IND + IND + IND + "if (" + normalCond + ") {" + NL
                    + cell + NL
                    + cellPoints1
                    + n
                    + startCellLoop
                    + indent + "if (vector(FOV_" + segId + ", " + vectorIndex + ") > 50) {" + NL
                    + nAssign
                    + indent + "}" + NL
                    + loopEnd(pi.getDimensions(), ind + IND + IND + IND + IND)
                    + ind + IND + IND + IND + IND + "bool inside = vector(FOV_" + segId + ", " + insideIndex + ") > 50;" + NL
                    + ind + IND + IND + IND + IND + "int proj = 9999;" + NL
                    + ind + IND + IND + IND + IND + "int acc = 0;" + NL
                    + indexes 
                    + startCellLoop
                    + indent + "if (" + insideCond + ") {" + NL
                    + indent + IND + "if ((inside && vector(FOV_" + segId + ", " + vectorIndex + ") < 50) || (!inside && vector(FOV_" + segId 
                    + ", " + vectorIndex + ") > 50)) {" + NL
                    + indent + IND + IND + "int projAux = " + projAux + ";" + NL
                    + indent + IND + IND + "if (projAux < proj) {" + NL
                    + indent + IND + IND + IND + "proj = projAux;" + NL
                    + indent + IND + IND + IND + "acc = 1;" + NL
                    + indexes2
                    + indent + IND + IND + "} else {" + NL
                    + indent + IND + IND + IND + "if (projAux == proj) {" + NL
                    + indent + IND + IND + IND + IND + "acc++;" + NL
                    + indexes3
                    + indent + IND + IND + IND + "}" + NL
                    + indent + IND + IND + "}" + NL
                    + indent + IND + "}" + NL
                    + indent + "}" + NL
                    + loopEnd(pi.getDimensions(), ind + IND + IND + IND + IND)
                    + ind + IND + IND + IND + IND + "if (acc!=0) {" + NL
                    + ind + IND + IND + IND + IND + IND + "if (" + indexCond + ") {" + NL
                    + ind + IND + IND + IND + IND + IND + IND + "int sign = 1;" + NL
                    + ind + IND + IND + IND + IND + IND + IND + "if (vector(FOV_" + segId + ", " + iVectorIndex + ") < 50) {" + NL
                    + ind + IND + IND + IND + IND + IND + IND + IND + "sign = -1;" + NL
                    + ind + IND + IND + IND + IND + IND + IND + "}" + NL
                    + floor
                    + ind + IND + IND + IND + IND + IND + IND + "sign = 1;" + NL
                    + ind + IND + IND + IND + IND + IND + IND + "if (vector(FOV_" + segId + ", " + iVectorIndex2 + ") < 50) {" + NL
                    + ind + IND + IND + IND + IND + IND + IND + IND + "sign = -1;" + NL
                    + ind + IND + IND + IND + IND + IND + IND + "}" + NL
                    + ceil
                    + ind + IND + IND + IND + IND + IND + IND + "int countFi = 0;" + NL
                    + ind + IND + IND + IND + IND + IND + IND + "int countFo = 0;" + NL
                    + startFloorLoop
                    + indent2 + "if (vector(FOV_" + segId + ", " + vectorIndex + ") < 50) {" + NL
                    + indent2 + IND + "countFo++;" + NL
                    + indent2 + "} else {" + NL
                    + indent2 + IND + "countFi++;" + NL
                    + indent2 + "}" + NL
                    + loopEnd(pi.getDimensions(), ind + IND + IND + IND + IND + IND + IND)                    
                    + ind + IND + IND + IND + IND + IND + IND + "int countCi = 0;" + NL
                    + ind + IND + IND + IND + IND + IND + IND + "int countCo = 0;" + NL
                    + startCeilLoop
                    + indent2 + "if (vector(FOV_" + segId + ", " + vectorIndex + ") < 50) {" + NL
                    + indent2 + IND + "countCo++;" + NL
                    + indent2 + "} else {" + NL
                    + indent2 + IND + "countCi++;" + NL
                    + indent2 + "}" + NL
                    + loopEnd(pi.getDimensions(), ind + IND + IND + IND + IND + IND + IND)     
                    + ind + IND + IND + IND + IND + IND + IND + "if (fabs(countFo - countFi) > fabs(countCo - countCi)) {" + NL
                    + cellPoints2
                    + ind + IND + IND + IND + IND + IND + IND + "} else {" + NL
                    + cellPoints3
                    + ind + IND + IND + IND + IND + IND + IND + "}" + NL
                    + ind + IND + IND + IND + IND + IND + "} else {" + NL
                    + ind + IND + IND + IND + IND + IND + IND + "int sign = 1;" + NL
                    + ind + IND + IND + IND + IND + IND + IND + "if (vector(FOV_" + segId + ", " + iVectorIndex + ") < 50) {" + NL
                    + ind + IND + IND + IND + IND + IND + IND + IND + "sign = -1;" + NL
                    + ind + IND + IND + IND + IND + IND + IND + "}" + NL
                    + cellPoints4
                    + ind + IND + IND + IND + IND + IND + "}" + NL
                    + ind + IND + IND + IND + IND + "}" + NL
                    + ind + IND + IND + IND + "}" + NL
                    + ind + IND + IND + "}" + NL
                    + ind + IND + "}" + NL
                    + ind + "}" + NL;
        }
        return cuadradoCellPoints;
    }
    
    /**
     * Creates the code to communicate the cell point arrays.
     * @param pi    The problem info
     * @param ind   The indent
     * @return      The code
     */
    private static String communicateCellPoints(ProblemInfo pi, String ind) {
        String comm = ind + "double* send;" + NL;
        for (int i = 0; i < pi.getMovementInfo().getX3dMovRegions().size(); i++) {
            String segName = pi.getMovementInfo().getX3dMovRegions().get(i);
            String toBuffer = "";
            String fromBuffer = "";
            for (int j = 0; j < pi.getDimensions(); j++) {
                toBuffer = toBuffer + ind + IND + "send[points * TIMESLICES + " + j + "] = " + segName 
                        + "CellPoints[timeSlice][points][" + j + "];" + NL;
                fromBuffer = fromBuffer + ind + IND + segName + "CellPoints[timeSlice][points][" + j + "] = send[points*TIMESLICES + " + j + "];"
                        + NL;
            }
            comm = comm + ind + "send = new double[POINTS_" + segName + "*TIMESLICES];" + NL
                    + ind + "for (int points = 0; points < POINTS_" + segName + "; points++) {" + NL
                    + toBuffer
                    + ind + "}" + NL
                    + ind + "mpi.AllReduce(send, POINTS_" + segName + "*TIMESLICES, MPI_MIN);" + NL
                    + ind + "for (int points = 0; points < POINTS_" + segName + "; points++) {" + NL
                    + fromBuffer
                    + ind + "}" + NL;
        }
        return comm;
    }
    
    /**
     * Creates the code to communicate the orientation arrays within the processors.
     * @param pi    The problem info
     * @param ind   The indent
     * @return      The code
     */
    private static String communicateOrientations(ProblemInfo pi, String ind) {
        String comm = ind + "double* sendO;" + NL;
        for (int i = 0; i < pi.getMovementInfo().getX3dMovRegions().size(); i++) {
            String segName = pi.getMovementInfo().getX3dMovRegions().get(i);
            comm = comm + ind + "sendO = new double[UNIONS_" + segName + "];" + NL
                    + ind + "for (int unions = 0; unions < UNIONS_" + segName + "; unions++) {" + NL
                    + ind + IND + "sendO[unions] = " + segName + "Orientation[unions];" + NL
                    + ind + "}" + NL
                    + ind + "mpi.AllReduce(sendO, UNIONS_" + segName + ", MPI_MIN);" + NL
                    + ind + "for (int unions = 0; unions < UNIONS_" + segName + "; unions++) {" + NL
                    + ind + IND + segName + "Orientation[unions] = sendO[unions];" + NL
                    + ind + "}" + NL;
        }
        return comm;
    }
    
    /**
     * Creates the code to calculate the orientations of the x3ds.
     * @param pi            The problem info
     * @param ind           The indent
     * @return              The code
     * @throws CGException  CG00X External error
     */
    private static String calculateOrientations(ProblemInfo pi, String ind) throws CGException {
        String[][] xslParams = new String [THREE][2];
        xslParams[0][0] = "condition";
        xslParams[0][1] = "true";
        xslParams[1][0] = "indent";
        xslParams[1][1] = "0";
        xslParams[2][0] =  "dimensions";
        xslParams[2][1] =  String.valueOf(pi.getCoordinates().size());
        String vIndex1p = "";
        String vIndex2p = "";
        String vIndex3p = "";
        String vIndex1m = "";
        String vIndex2m = "";
        String vIndex3m = "";
        for (int j = 0; j < pi.getDimensions(); j++) {
            String coord = pi.getCoordinates().get(j);
            vIndex1p = vIndex1p + coord + "term + frac" + coord + "_1 - boxfirst1(" + j + "), ";
            vIndex2p = vIndex2p + coord + "term + frac" + coord + "_2 - boxfirst1(" + j + "), ";
            vIndex3p = vIndex3p + coord + "term + frac" + coord + "_3 - boxfirst1(" + j + "), ";
            vIndex1m = vIndex1m + coord + "term - frac" + coord + "_1 - boxfirst1(" + j + "), ";
            vIndex2m = vIndex2m + coord + "term - frac" + coord + "_2 - boxfirst1(" + j + "), ";
            vIndex3m = vIndex3m + coord + "term - frac" + coord + "_3 - boxfirst1(" + j + "), ";
        }
        vIndex1p = vIndex1p.substring(0, vIndex1p.length() - 2);
        vIndex2p = vIndex2p.substring(0, vIndex2p.length() - 2);
        vIndex3p = vIndex3p.substring(0, vIndex3p.length() - 2);
        vIndex1m = vIndex1m.substring(0, vIndex1m.length() - 2);
        vIndex2m = vIndex2m.substring(0, vIndex2m.length() - 2);
        vIndex3m = vIndex3m.substring(0, vIndex3m.length() - 2);
        String orientation = "";
        for (int i = 0; i < pi.getMovementInfo().getX3dMovRegions().size(); i++) {
            String segName = pi.getMovementInfo().getX3dMovRegions().get(i);
            String segId = CodeGeneratorUtils.getRegionId(pi.getProblem(), pi.getRegions(), segName);
            String points = "";
            String terms = "";
            String cond = "";
            String indexes = "";
            for (int j = 0; j < pi.getDimensions(); j++) {
                String coord = pi.getCoordinates().get(j);
                String midCoord = "";
                for (int k = 0; k < pi.getDimensions(); k++) {
                    String varName = "a";
                    if (k == 1) {
                        varName = "b";
                    }
                    if (k == 2) {
                        varName = "c";
                    }
                    points = points + ind + IND + "double " + varName + coord + " = " + segName + "CellPoints[timeSlice][" + segName 
                            + "Unions[unions][" + k + "]][" + j + "];" + NL
                            + ind + IND + "double " + varName + "p" + coord + " = " + segName + "Points[timeSlice][" + segName 
                            + "Unions[unions][" + k + "]][" + j + "];" + NL;
                    midCoord = midCoord + varName + coord + " + ";
                }
                midCoord = "(" + midCoord.substring(0, midCoord.lastIndexOf(" + ")) + ")/" + pi.getDimensions();
                terms = terms + ind + IND + "int " + coord + "term = floor((" + midCoord + " - d_grid_geometry->getXLower()[" + j
                        + "]) / dx[" + j + "]);" + NL;
                cond = cond + coord + "term >= boxfirst(" + j + ") && " + coord + "term <= boxlast(" + j + ") && ";
                indexes = indexes + ind + IND + IND + "int frac" + coord + "_1 = 0;" + NL
                        + ind + IND + IND + "int frac" + coord + "_2 = 0;" + NL
                        + ind + IND + IND + "int frac" + coord + "_3 = 0;" + NL
                        + ind + IND + IND + "if (!equalsEq(v" + coord + ", 0)) {" + NL
                        + ind + IND + IND + IND + "frac" + coord + "_1 = round(1.0*v" + coord + "/fabs(v" + coord + "));" + NL
                        + ind + IND + IND + IND + "frac" + coord + "_2 = round(2.0*v" + coord + "/fabs(v" + coord + "));" + NL
                        + ind + IND + IND + IND + "frac" + coord + "_3 = round(3.0*v" + coord + "/fabs(v" + coord + "));" + NL
                        + ind + IND + IND + "}" + NL;
            }
            cond = cond.substring(0, cond.lastIndexOf(" && "));
            orientation = orientation + ind + "orient_acc = 0;" + NL
                    + ind + "any_zero_value = false;" + NL
                    + ind + "for (int unions = 0; unions < UNIONS_" + segName + "; unions++) {" + NL
                    + points
                    + terms
                    + ind + IND + "if (" + cond + ") {" + NL
                    + getVectorialProduct(pi.getCoordinates(), "v", ind + IND + IND)
                    + indexes
                    + ind + IND + IND + "//Setting orientation of each union." + NL
                    + ind + IND + IND + "//NOTE: If it can not be obtained, it will be first set to 0 and then asigned the most frequent value" + NL
                    + ind + IND + IND + "int FOV_sideA = vector(FOV_" + segId + ", " + vIndex1p + ") + vector(FOV_" + segId + ", " + vIndex2p 
                    + ") + vector(FOV_" + segId + ", " + vIndex3p + ");" + NL
                    + ind + IND + IND + "int FOV_sideB = vector(FOV_" + segId + ", " + vIndex1m + ") + vector(FOV_" + segId + ", " + vIndex2m 
                    + ") + vector(FOV_" + segId + ", " + vIndex3m + ");" + NL
                    + ind + IND + IND + segName + "Orientation[unions] = 9;" + NL
                    + ind + IND + IND + "if ((FOV_sideA <= 150) && (FOV_sideB >= 150)) {" + NL           
                    + ind + IND + IND + IND + segName + "Orientation[unions] = 1;" + NL
                    + ind + IND + IND + "}" + NL         
                    + ind + IND + IND + "if ((FOV_sideA >= 150) && (FOV_sideB <= 150)) {" + NL           
                    + ind + IND + IND + IND + segName + "Orientation[unions] = -1;" + NL
                    + ind + IND + IND + "}" + NL
                    + ind + IND + IND + "if (((FOV_sideA >= 150) && (FOV_sideB <= 150)  && (FOV_sideA <= 150) && (FOV_sideB >= 150)) || " 
                    + segName + "Orientation[unions] == 9) {" + NL           
                    + ind + IND + IND + IND + "any_zero_value = true;" + NL
                    + ind + IND + IND + "}" + NL    
                    + ind + IND + IND + "orient_acc = orient_acc + " + segName + "Orientation[unions];" + NL
                    + ind + IND + "}" + NL
                    + ind + "}" + NL
                    + ind + "//And assigning the 0-valued orientations to the most frequent orientation value" + NL
                    + ind + "if (any_zero_value) {" + NL
                    + ind + IND + "if (orient_acc != 0) {" + NL
                    + ind + IND + IND + "for (int unions = 0; unions < UNIONS_" + segName + "; unions++) {" + NL
                    + ind + IND + IND + IND + "if (" + segName + "Orientation[unions] == 9) {" + NL
                    + ind + IND + IND + IND + IND + segName + "Orientation[unions] = round(orient_acc/fabs(orient_acc));" + NL
                    + ind + IND + IND + IND + "}" + NL
                    + ind + IND + IND + "}" + NL
                    + ind + IND + "} else {" + NL
                    + ind + IND + IND + "cout<<\" ERROR assigning 0-valued orientations to the most frequent one. It seems that both "
                    + "orientations are equiprobable.\"<<endl;" + NL
                    + ind + IND + "}" + NL
                    + ind + "}" + NL;
        }
        return orientation;
        
    }
    
    /**
     * Calculate the vectorial product of two vectors.
     * @param coords    The coordinates
     * @param varName   The variable name
     * @param ind       The indent
     * @return          The code
     */
    private static String getVectorialProduct(ArrayList<String> coords, String varName, String ind) {
        if (coords.size() == 2) {
            String coord1 = coords.get(0);
            String coord2 = coords.get(1);
            return ind + "double " + varName + coord1 + " = bp" + coord2 + " - ap" + coord2 + ";" + NL
                    + ind + "double " + varName + coord2 + " = -(bp" + coord1 + " - ap" + coord1 + ");" + NL;
        }
		String coord1 = coords.get(0);
		String coord2 = coords.get(1);
		String coord3 = coords.get(2);
		return ind + "double " + varName + coord1 + " = (bp" + coord2 + " - ap" + coord2 + ") * (cp" + coord3 + " - ap" + coord3 
		        + ") - (bp" + coord3 + " - ap" + coord3 + ") * (cp" + coord2 + " - ap" + coord2 + ");" + NL
		        + ind + "double " + varName + coord2 + " = (bp" + coord3 + " - ap" + coord3 + ") * (cp" + coord1 + " - ap" + coord1 
		        + ") - (bp" + coord1 + " - ap" + coord1 + ") * (cp" + coord3 + " - ap" + coord3 + ");" + NL
		        + ind + "double " + varName + coord3 + " = (bp" + coord1 + " - ap" + coord1 + ") * (cp" + coord2 + " - ap" + coord2 
		        + ") - (bp" + coord2 + " - ap" + coord2 + ") * (cp" + coord1 + " - ap" + coord1 + ");" + NL;
    }
    
    /**
     * Allocates the memory for the redistancing temporal variables.
     * @param pi                The problem info
     * @param id                The region id
     * @param ind               The current indent
     * @return                  The code
     */
    private static String allocateRedistTmps(ProblemInfo pi, String id, String ind) {
        String code = "";
        String indent = ind;
        
        for (int i = 0; i < pi.getDimensions(); i++) {
            String coord = pi.getCoordinates().get(i);
            String pointerNumber = "";
            String arrayIndex = "";
            for (int j = 0; j < pi.getDimensions() - i; j++) {
                pointerNumber = pointerNumber + "*";
            }
            for (int j = 0; j < i; j++) {
                String coord2 = pi.getCoordinates().get(j);
                arrayIndex = arrayIndex + "[" + coord2 + "]";
            }
            String initDouble = "";
            String initBoolean = "";
            if (i == 0) {
                initDouble = "double " + pointerNumber;
                initBoolean = "bool " + pointerNumber;
            }
            code = code + indent + initDouble + "tLS_" + id + "_p" + arrayIndex + " = (double " + pointerNumber + ") malloc(" + coord 
                + "last * " + INTERPHASELENGTH + " * sizeof(double " + pointerNumber.substring(1) + "));" + NL
                + indent + initDouble + "tLS_" + id + arrayIndex + " = (double " + pointerNumber + ") malloc(" + coord 
                + "last * " + INTERPHASELENGTH + " * sizeof(double " + pointerNumber.substring(1) + "));" + NL
                + indent + initBoolean + "cons" + arrayIndex + " = (bool " + pointerNumber + ") malloc(" + coord 
                + "last * " + INTERPHASELENGTH + " * sizeof(bool " + pointerNumber.substring(1) + "));" + NL
                + indent + initBoolean + "alter" + arrayIndex + " = (bool " + pointerNumber + ") malloc(" + coord 
                + "last * " + INTERPHASELENGTH + " * sizeof(bool " + pointerNumber.substring(1) + "));" + NL;
            code = code + indent + "for(int " + coord + " = 0; " + coord + " < " + coord + "last * " + INTERPHASELENGTH + "; " 
                    + coord + "++) {" + NL;
            indent = indent + IND;
            
        }
        String arrayIndex = "";
        for (int j = 0; j < pi.getDimensions(); j++) {
            String coord = pi.getCoordinates().get(j);
            arrayIndex = arrayIndex + "[" + coord + "]";
        }
        code = code + indent + "tLS_" + id + "_p" + arrayIndex + " = 999;" + NL
                + indent + "tLS_" + id + "" + arrayIndex + " = 999;" + NL
                + indent + "cons" + arrayIndex + " = false;" + NL;
        for (int i = 0; i < pi.getDimensions(); i++) {
            indent = indent.substring(1);  
            code = code + indent + "}" + NL;

        }
        return code;
    }
    
    /**
     * Initialize the level set temporal variables.
     * @param pi                The problem info
     * @param id                The region id
     * @param ind               The current indent
     * @return                  The code
     */
    private static String lsTmpInitialization(ProblemInfo pi, String id, String ind) {
        String code = "";
        String indexInit = "";
        String index = "";
        String indent = ind;
        for (int i = 0; i < pi.getDimensions(); i++) {
            String coord = pi.getCoordinates().get(i);
            index = index + coord + ", ";
            indent = indent + IND;
        }
        for (int i = 0; i < pi.getDimensions(); i++) {
            String coord = pi.getCoordinates().get(i);
            indexInit = indexInit + indent + "int index" + i + " = (" + coord + ") * 3;" + NL;
        }
        index = index.substring(0, index.length() - 2);
        ArrayList<String> elements = new ArrayList<String>();
        for (int i = 0; i < INTERPHASELENGTH; i++) {
            elements.add(String.valueOf(i));
        }
        ArrayList<String> combinations = CodeGeneratorUtils.createCombinations(pi.getDimensions(), elements);  
        for (int i = 0; i < combinations.size(); i++) {
            String comb = combinations.get(i);
            String arrayIndex = "";
            String pIndex = "";
            for (int j = 0; j < comb.length(); j++) {
                arrayIndex = arrayIndex + "[index" + j + " + " + comb.substring(j, j + 1) + "]";
                pIndex = pIndex + comb.substring(j, j + 1) + ", ";
            }
            pIndex = pIndex.substring(0, pIndex.length() - 2);
            code = code + indent + IND + "tLS_" + id + arrayIndex + " = vectorP(ls_" + id + ", " + index + ", " + pIndex + ");" + NL
                    + indent + IND + "tLS_" + id + "_p" + arrayIndex + " = tLS_" + id + arrayIndex + ";" + NL;
        }
        return loopInitLasts(pi.getCoordinates(), ind)
                + indexInit
                + indent + "if (vector(soporte, " + index + ") == 1) {" + NL
                + code
                + indent + "}" + NL
                + loopEnd(pi.getDimensions(), ind);
    }
   
    /**
     * Creates the code that free the memory for the temporal variables created for performance reasons.
     * @param pi        The problem information
     * @param vars      The variable list
     * @param ind       The indent
     * @param bigger    If the loop is bigger
     * @return          The code
     */
    private static String tmpVarsFree(ProblemInfo pi, ArrayList<String> vars, String ind, boolean bigger) {
        String code = "";
        ArrayList<String> coords = pi.getCoordinates();
        String indent = ind;
        for (int i = coords.size() - 1; i >= 1; i--) {
            indent = indent + IND;
        }
        for (int i = coords.size() - 2; i >= 0; i--) {
            indent = indent.substring(1);
            String arrayIndex = "";
            for (int j = 0; j < i + 1; j++) {
                String coord2 = coords.get(j);
                arrayIndex = arrayIndex + "[" + coord2 + "]";
            }
            String coord = coords.get(i);
            String limit = coord + "last * " + INTERPHASELENGTH;
            if (bigger) {
                limit = "(" + coord + "last + 2) * " + INTERPHASELENGTH;
            }
            code = indent + "for(int " + coord + " = 0; " + coord + " < " + limit + "; " + coord + "++) {" + NL
                    + code;
            for (int j = 0; j < vars.size(); j++) {
                code = code + indent + IND + "free(" + vars.get(j) + arrayIndex + ");" + NL;
            }
   
            code = code + indent + "}" + NL;
        }
        for (int j = 0; j < vars.size(); j++) {
            code = code + ind + "free(" + vars.get(j) + ");" + NL;
        }
        return code;
    }
    
    /**
     * Calculates the threshold.
     * @param pi                The problem info
     * @param id                The region id
     * @param ind               The current indent
     * @return                  The code
     * @throws CGException      CGException - CG00X External error
     */
    private static String calculateThreshold(ProblemInfo pi, String id, String ind) throws CGException {
        String indent = ind;
        String indent2 = ind + IND;
        String arrayIndex = "";
        String arrayIndex2 = "";
        String cond = "";
        String indexes = "";
        String multiprocExchange = "";
        String index = "";
        String pIndex = "";
        for (int i = 0; i < pi.getDimensions(); i++) {
            String coord = pi.getCoordinates().get(i);
            arrayIndex = arrayIndex + "[" + coord + " * " + INTERPHASELENGTH + "]";
            arrayIndex2 = arrayIndex2 + "[index" + i + " + p" + (i + 1) + "]";
            index = index + coord + ", ";
            pIndex = pIndex + "p" + (i + 1) + ", ";
            String arrayp1 = "";
            String arraym1 = "";
            for (int j = 0; j < pi.getDimensions(); j++) {
                String coord2 = pi.getCoordinates().get(j);
                if (i == j) {
                    arrayp1 = arrayp1 + "[(" + coord2 + " + 1) * " + INTERPHASELENGTH + "]";
                    arraym1 = arraym1 + "[(" + coord2 + " - 1) * " + INTERPHASELENGTH + "]";
                }
                else {
                    arrayp1 = arrayp1 + "[" + coord2 + " * " + INTERPHASELENGTH + "]";
                    arraym1 = arraym1 + "[" + coord2 + " * " + INTERPHASELENGTH + "]";
                }
            }
            cond = cond + "(" + coord + " == d_ghost_width && tLS_" + id + arraym1 + " < 998) || (" + coord + " == " + coord 
                    + "last - d_ghost_width - 1 && tLS_" + id + arrayp1 + " < 998) || ";
            indent = indent + IND;
            indent2 = indent2 + IND + IND;
        }
        index = index.substring(0, index.length() - 2);
        pIndex = pIndex.substring(0, pIndex.length() - 2);
        for (int i = 0; i < pi.getDimensions(); i++) {
            String coord = pi.getCoordinates().get(i);
            indexes = indexes + indent + IND + "int index" + i + " = " + coord + " * 3;" + NL;
            String arrayp1 = "";
            String arraym1 = "";
            String arrayp12 = "";
            String arrayp22 = "";
            String arraym12 = "";
            String arraym22 = "";
            String arraym1p2 = "";
            for (int j = 0; j < pi.getDimensions(); j++) {
                String coord2 = pi.getCoordinates().get(j);
                if (i == j) {
                    arrayp1 = arrayp1 + "[(" + coord2 + " + 1) * " + INTERPHASELENGTH + "]";
                    arraym1 = arraym1 + "[(" + coord2 + " - 1) * " + INTERPHASELENGTH + "]";
                    arraym1p2 = arraym1p2 + "[(" + coord2 + " - 1) * " + INTERPHASELENGTH + " + 2]";
                    arrayp12 = arrayp12 + "[index" + j + " + p" + (j + 1) + " + 1]";
                    arrayp22 = arrayp22 + "[index" + j + " + p" + (j + 1) + " + 2]";
                    arraym12 = arraym12 + "[index" + j + " + p" + (j + 1) + " - 1]";
                    arraym22 = arraym22 + "[index" + j + " + p" + (j + 1) + " - 2]";
                }
                else {
                    arrayp1 = arrayp1 + "[index" + j + " + p" + (j + 1) + "]";
                    arraym1 = arraym1 + "[index" + j + " + p" + (j + 1) + "]";
                    arraym1p2 = arraym1p2 + "[index" + j + " + p" + (j + 1) + "]";
                    arrayp12 = arrayp12 + "[index" + j + " + p" + (j + 1) + "]";
                    arrayp22 = arrayp22 + "[index" + j + " + p" + (j + 1) + "]";
                    arraym12 = arraym12 + "[index" + j + " + p" + (j + 1) + "]";
                    arraym22 = arraym22 + "[index" + j + " + p" + (j + 1) + "]";
                }
            }
            multiprocExchange = multiprocExchange + indent2 + "if (" + coord + " == " + coord + "last - d_ghost_width - 1 && p" + (i + 1) + " == " 
                    + (INTERPHASELENGTH - 1) + " && !added && (greatherEq(fabs(tLS_" + id + arrayp1 + "), fabs(tLS_" + id + arrayIndex2 
                    + ")) || greatherEq(minThreshold, fabs(tLS_" + id + arrayIndex2 + ")))) {" + NL
                    + indent2 + IND + "if (greatherEq(minThreshold, fabs(tLS_" + id + arrayIndex2 + "))) {" + NL
                    + indent2 + IND + IND + "cons" + arrayp12 + " = true;" + NL
                    + indent2 + IND + "}" + NL
                    + indent2 + IND + "cons" + arrayIndex2 + " = true;" + NL
                    + indent2 + IND + "vectorP(ls_" + id + "," + index + ", " + pIndex + ") = tLS_" + id + arrayIndex2 + ";" + NL
                    + indent2 + "} else {" + NL
                    + indent2 + IND + "if (" + coord + " == " + coord + "last - d_ghost_width - 1 && p" + (i + 1) 
                    + " == 2 && !greatherEq(fabs(tLS_" + id + arrayp1 + "), fabs(tLS_" + id + arrayIndex2 + "))) {" + NL
                    + indent2 + IND + IND + "cons" + arrayIndex2 + " = false;" + NL
                    + indent2 + IND + IND + "cons" + arrayp12 + " = true;" + NL
                    + indent2 + IND + IND + "cons" + arrayp22 + " = true;" + NL
                    + indent2 + IND + IND + "cons" + arraym12 + " = true;" + NL
                    + indent2 + IND + IND + "if ((lessThan(fabs(tLS_" + id + arraym22 + "), fabs(tLS_" + id + arraym12 + ") && tLS_" + id + arraym22 
                    + " * tLS_" + id + arraym12 + " > 0)) || (tLS_" + id + arraym22 + " * tLS_" + id + arraym12 + " <= 0 && lessThan(fabs(tLS_" 
                    + id + arraym22 + "), fabs(tLS_" + id + arrayIndex2 + ")))) {" + NL
                    + indent2 + IND + IND + IND + "cons" + arraym22 + " = true;" + NL
                    + indent2 + IND + IND + "}" + NL
                    + indent2 + IND + IND + "candidates2.insert(particle(" + index + ", " + pIndex + ", tLS_" + id + arrayIndex2 + "));" + NL
                    + indent2 + IND + IND + "added = true;" + NL
                    + indent2 + IND + "}" + NL
                    + indent2 + "}" + NL
                    + indent2 + "if (" + coord + " == d_ghost_width && p" + (i + 1) + " == 0 && !added && (greatherEq(fabs(tLS_" + id + arraym1p2 
                    + "), fabs(tLS_" + id + arrayIndex2 + ")) || greatherEq(minThreshold, fabs(tLS_" + id + arrayIndex2 + ")))) {" + NL
                    + indent2 + IND + "if (greatherEq(minThreshold, fabs(tLS_" + id + arrayIndex2 + "))) {" + NL
                    + indent2 + IND + IND + "cons" + arraym12 + " = true;" + NL
                    + indent2 + IND + "}" + NL
                    + indent2 + IND + "cons" + arrayIndex2 + " = true;" + NL
                    + indent2 + IND + "vectorP(ls_" + id + ", " + index + ", " + pIndex + ") = tLS_" + id + arrayIndex2 + ";" + NL
                    + indent2 + "} else {" + NL
                    + indent2 + IND + "if (" + coord + " == d_ghost_width && p" + (i + 1) + " == 0 && !greatherEq(fabs(tLS_" 
                    + id + arraym1p2 + "), fabs(tLS_" + id + arrayIndex2 + "))) {" + NL
                    + indent2 + IND + IND + "cons" + arrayIndex2 + " = false;" + NL
                    + indent2 + IND + IND + "cons" + arraym12 + " = true;" + NL
                    + indent2 + IND + IND + "cons" + arraym22 + " = true;" + NL
                    + indent2 + IND + IND + "cons" + arrayp12 + " = true;" + NL
                    + indent2 + IND + IND + "if ((lessThan(fabs(tLS_" + id + arrayp22 + "), fabs(tLS_" + id + arrayp12 + ") && tLS_" + id + arrayp22 
                    + " * tLS_" + id + arrayp12 + " > 0)) || (tLS_" + id + arrayp22 + " * tLS_" + id + arrayp12 + " <= 0 && lessThan(fabs(tLS_" 
                    + id + arrayp22 + "), fabs(tLS_" + id + arrayIndex2 + ")))) {" + NL
                    + indent2 + IND + IND + IND + "cons" + arrayp22 + " = true;" + NL
                    + indent2 + IND + IND + "}" + NL
                    + indent2 + IND + IND + "candidates2.insert(particle(" + index + ", " + pIndex + ", tLS_" + id + arrayIndex2 + "));" + NL
                    + indent2 + IND + IND + "added = true;" + NL
                    + indent2 + IND + "}" + NL
                    + indent2 + "}" + NL;
        }
        cond = cond.substring(0, cond.lastIndexOf(" ||"));
        return ind + "candidates.clear();" + NL
                + ind + "candidates2.clear();" + NL
                + ind + "double threshold = 9999999;" + NL
                + ind + "if (first) {" + NL
                + ind + IND + "threshold = minThreshold;" + NL
                + ind + "}" + NL
                + loopInitNoGhosts(pi.getCoordinates(), ind)
                + indent + "if (tLS_" + id + arrayIndex + " < 998 && (" + cond + ")) {" + NL
                + indexes
                + pLoopInit(pi.getCoordinates(), indent + IND, false, false) 
                + indent2 + "bool added = false;" + NL
                + multiprocExchange
                + loopEnd(pi.getDimensions(), indent + IND)
                + indent + "}" + NL
                + loopEnd(pi.getDimensions(), ind)
                + multiprocThreshold(pi, id, ind)
                + ind + "threshold = max<double>(threshold, minThreshold);" + NL;
    }
    
    /**
     * Calculates the thershold for multiprocessor.
     * @param pi                The problem info
     * @param id                The region id
     * @param ind               The current indent
     * @return                  The code
     * @throws CGException      CGException - CG00X External error
     */
    private static String multiprocThreshold(ProblemInfo pi, String id, String ind) throws CGException {
        String eps = "";
        String indexes = "";
        String array = "";
        String epsCalc = "";
        String a1 = "";
        String a2 = "";
        String a3 = "";
        String epstmp = "";
        String change = "";
        String pIndex = "";
        String index = "";
        String ppIndex = "";
        for (int i = 0; i < pi.getDimensions(); i++) {
            String coord = pi.getCoordinates().get(i);
            String contCoord = CodeGeneratorUtils.discToCont(pi.getProblem(), coord);
            pIndex = pIndex + "p." + coord + ", ";
            ppIndex = ppIndex + "p.p" + coord + ", ";
            String var = "c";
            if (i == 1) {
                var = "d";
            }
            if (i == 2) {
                var = "e";
            }
            eps = eps + ind + IND + "double eps" + contCoord + " = 0;" + NL
                    + ind + IND + "double " + var + "3 = 0;" + NL;
            indexes = indexes + ind + IND +  "int index" + coord + " = (p." + coord + ") * 3 + p.p" + coord + ";" + NL;
            array = array + "[index" + coord + "]";
            a1 = a1 + var + "3*eps" + contCoord + "/d" + contCoord + "p2 + ";
            a2 = a2 + var + "3*" + var + "3/d" + contCoord + "p2 + ";
            a3 = a3 + "eps" + contCoord + "*eps" + contCoord + "/d" + contCoord + "p2 +";
            epstmp = epstmp + "eps" + contCoord + "t, ";
            String indexm1 = "";
            String indexm2 = "";
            String indexp1 = "";
            String indexp2 = "";
            index = "";
            for (int j = 0; j < pi.getDimensions(); j++) {
                String coordIndex = pi.getCoordinates().get(j);
                index = index + "[index" + coordIndex + "]";
                if (i == j) {
                    indexm1 = indexm1 + "[index" + coordIndex + " - 1]";
                    indexm2 = indexm2 + "[index" + coordIndex + " - 2]";
                    indexp1 = indexp1 + "[index" + coordIndex + " + 1]";
                    indexp2 = indexp2 + "[index" + coordIndex + " + 2]";
                }
                else {
                    indexm1 = indexm1 + "[index" + coordIndex + "]";
                    indexm2 = indexm2 + "[index" + coordIndex + "]";
                    indexp1 = indexp1 + "[index" + coordIndex + "]";
                    indexp2 = indexp2 + "[index" + coordIndex + "]";
                }
            }
            epsCalc = epsCalc + ind + IND + "consolm2 = false;" + NL
                    + ind + IND + "consolm1 = false;" + NL
                    + ind + IND + "consolp1 = false;" + NL
                    + ind + IND + "consolp2 = false;" + NL
                    + ind + IND + "//" + contCoord + NL
                    + ind + IND + "if (!cons" + indexm1 + ") {" + NL
                    + ind + IND + IND + "consolm1 = false;" + NL
                    + ind + IND + "} else {" + NL
                    + ind + IND + IND + "consolm1 = true;" + NL
                    + ind + IND + IND + "valuem1 = tLS_" + id + indexm1 + ";" + NL
                    + ind + IND + "}" + NL
                    + ind + IND + "if (!consolm1 || !cons" + indexm2 + ") {" + NL
                    + ind + IND + IND + "consolm2 = false;" + NL
                    + ind + IND + "} else {" + NL
                    + ind + IND + IND + "consolm2 = true;" + NL
                    + ind + IND + IND + "valuem2 = tLS_" + id + indexm2 + ";" + NL
                    + ind + IND + "}" + NL
                    + ind + IND + "if (!cons" + indexp1 + ") {" + NL
                    + ind + IND + IND + "consolp1 = false;" + NL
                    + ind + IND + "} else {" + NL
                    + ind + IND + IND + "consolp1 = true;" + NL
                    + ind + IND + IND + "valuep1 = tLS_" + id + indexp1 + ";" + NL
                    + ind + IND + "}" + NL
                    + ind + IND + "if (!consolp1 || !cons" + indexp2 + ") {" + NL
                    + ind + IND + IND + "consolp2 = false;" + NL
                    + ind + IND + "} else {" + NL
                    + ind + IND + IND + "consolp2 = true;" + NL
                    + ind + IND + IND + "valuep2 = tLS_" + id + indexp2 + ";" + NL
                    + ind + IND + "}" + NL
                    + ind + IND + "//0-11" + NL
                    + ind + IND + "if (consolp1 && consolp2 && !consolm1) {" + NL
                    + ind + IND + IND + var + "3 = -1.5;" + NL
                    + ind + IND + IND + "eps" + contCoord + " = 2 * valuep1 - 0.5 * valuep2;" + NL
                    + ind + IND + "} else" + NL
                    + ind + IND + "//11-0" + NL
                    + ind + IND + "if (consolm2 && consolm1 && !consolp1) {" + NL
                    + ind + IND + IND + var + "3 = -1.5;" + NL
                    + ind + IND + IND + "eps" + contCoord + " = -0.5 * valuem2 + 2 * valuem1;" + NL
                    + ind + IND + "} else" + NL
                    + ind + IND + "//0-10" + NL
                    + ind + IND + "if (!consolm1 && consolp1 && !consolp2) {" + NL
                    + ind + IND + IND + var + "3 = -1;" + NL
                    + ind + IND + IND + "eps" + contCoord + " = valuep1;" + NL
                    + ind + IND + "} else" + NL
                    + ind + IND + "//01-0" + NL
                    + ind + IND + "if (!consolm2 && consolm1 && !consolp1) {" + NL
                    + ind + IND + IND + var + "3 = -1;" + NL
                    + ind + IND + IND + "eps" + contCoord + " = valuem1;" + NL
                    + ind + IND + "} else" + NL
                    + ind + IND + "if (consolm1 && consolp1) {" + NL
                    + ind + IND + IND + "if (fabs(valuem1) < fabs(valuep1)) {" + NL
                    + ind + IND + IND + IND + "//01-1x" + NL
                    + ind + IND + IND + IND + "if (!consolm2) {" + NL
                    + ind + IND + IND + IND + IND + var + "3 = -1;" + NL
                    + ind + IND + IND + IND + IND + "eps" + contCoord + " = valuem1;" + NL
                    + ind + IND + IND + IND + "//11-1x" + NL
                    + ind + IND + IND + IND + "} else {" + NL
                    + ind + IND + IND + IND + IND + var + "3 = -1.5;" + NL
                    + ind + IND + IND + IND + IND + "eps" + contCoord + " = -0.5 * valuem2 + 2 * valuem1;" + NL
                    + ind + IND + IND + IND + "}" + NL
                    + ind + IND + IND + "} else {" + NL
                    + ind + IND + IND + IND + "//x1-10" + NL
                    + ind + IND + IND + IND + "if (!consolp2) {" + NL
                    + ind + IND + IND + IND + IND + var + "3 = -1;" + NL
                    + ind + IND + IND + IND + IND + "eps" + contCoord + " = valuep1;" + NL
                    + ind + IND + IND + IND + "//x1-11" + NL
                    + ind + IND + IND + IND + "} else {" + NL
                    + ind + IND + IND + IND + IND + var + "3 = -1.5;" + NL
                    + ind + IND + IND + IND + IND + "eps" + contCoord + " = -0.5 * valuep2 + 2 * valuep1;" + NL
                    + ind + IND + IND + IND + "}" + NL
                    + ind + IND + IND + "}" + NL
                    + ind + IND + "}" + NL;
            String epstmps = "";
            String a1t = "";
            String a2t = "";
            String a3t = "";
            for (int j = 0; j < pi.getDimensions(); j++) {
                String coord2 = pi.getCoordinates().get(j);
                String contCoord2 = CodeGeneratorUtils.discToCont(pi.getProblem(), coord2);
                String vart = "c";
                if (j == 1) {
                    vart = "d";
                }
                if (j == 2) {
                    vart = "e";
                }
                if (j != i) {
                    String indexm1t = "";
                    String indexm2t = "";
                    String indexp1t = "";
                    String indexp2t = "";
                    for (int k = 0; k < pi.getDimensions(); k++) {
                        String coordIndex = pi.getCoordinates().get(k);
                        if (k == j) {
                            indexm1t = indexm1t + "[index" + coordIndex + " - 1]";
                            indexm2t = indexm2t + "[index" + coordIndex + " - 2]";
                            indexp1t = indexp1t + "[index" + coordIndex + " + 1]";
                            indexp2t = indexp2t + "[index" + coordIndex + " + 2]";
                        }
                        else {
                            indexm1t = indexm1t + "[index" + coordIndex + "]";
                            indexm2t = indexm2t + "[index" + coordIndex + "]";
                            indexp1t = indexp1t + "[index" + coordIndex + "]";
                            indexp2t = indexp2t + "[index" + coordIndex + "]";
                        }
                    }
                    epstmps = epstmps + ind + IND + IND + IND + "if (fabs(tLS_" + id + indexp1t + ") < fabs(tLS_" + id + indexm1t + ")) {" + NL
                            + ind + IND + IND + IND + IND + "valuep1 = tLS_" + id + indexp1t + ";" + NL
                            + ind + IND + IND + IND + IND + "if (fabs(tLS_" + id + indexp2t + ") < fabs(tLS_" + id + indexp1t + ")) {" + NL
                            + ind + IND + IND + IND + IND + IND + "valuep2 = tLS_" + id + indexp2t + ";" + NL
                            + ind + IND + IND + IND + IND + IND + vart + "3 = -1.5;" + NL
                            + ind + IND + IND + IND + IND + IND + "eps" + contCoord2 + "t = 2 * valuep1 - 0.5 * valuep2;" + NL
                            + ind + IND + IND + IND + IND + "} else {" + NL
                            + ind + IND + IND + IND + IND + IND + vart + "3 = -1;" + NL
                            + ind + IND + IND + IND + IND + IND + "eps" + contCoord2 + "t = valuep1;" + NL
                            + ind + IND + IND + IND + IND + "}" + NL
                            + ind + IND + IND + IND + "} else {" + NL
                            + ind + IND + IND + IND + IND + "valuem1 = tLS_" + id + indexm1t + ";" + NL
                            + ind + IND + IND + IND + IND + "if (fabs(tLS_" + id + indexm2t + ") < fabs(tLS_" + id + indexm1t + ")) {" + NL
                            + ind + IND + IND + IND + IND + IND + "valuem2 = tLS_" + id + indexm2t + ";" + NL
                            + ind + IND + IND + IND + IND + IND + vart + "3 = -1.5;" + NL
                            + ind + IND + IND + IND + IND + IND + "eps" + contCoord2 + "t = 2 * valuem1 - 0.5 * valuem2;" + NL
                            + ind + IND + IND + IND + IND + "} else {" + NL
                            + ind + IND + IND + IND + IND + IND + vart + "3 = -1;" + NL
                            + ind + IND + IND + IND + IND + IND + "eps" + contCoord2 + "t = valuem1;" + NL
                            + ind + IND + IND + IND + IND + "}" + NL
                            + ind + IND + IND + IND + "}" + NL;
                    a1t = a1t + vart + "3*eps" + contCoord2 + "t/d" + contCoord2 + "p2 + ";
                    a3t = a3t + "eps" + contCoord2 + "t*eps" + contCoord2 + "t/d" + contCoord2 + "p2 + ";
                }
                else {
                    a1t = a1t + vart + "3*eps" + contCoord2 + "/d" + contCoord2 + "p2 + ";
                    a3t = a3t + "eps" + contCoord2 + "*eps" + contCoord2 + "/d" + contCoord2 + "p2 + ";
                }
                a2t = a2t + vart + "3*" + vart + "3/d" + contCoord2 + "p2 + ";
            }
            a1t = a1t.substring(0, a1t.lastIndexOf(" + "));
            a2t = a2t.substring(0, a2t.lastIndexOf(" + "));
            a3t = a3t.substring(0, a3t.lastIndexOf(" + "));
            change = change + ind + IND + IND + "if (change && p." + coord + " - 1 < d_ghost_width && p.p" + coord + " == 0) {" + NL
            		+ epstmps + NL
            		+ ind + IND + IND + IND + "double tls_" + id + " = tLS_" + id + index + ";" + NL
            		+ ind + IND + IND + IND + "double disttmp;" + NL
            		+ ind + IND + IND + IND + "double a1 = " + a1t + ";" + NL
            		+ ind + IND + IND + IND + "double a2 = " + a2t + ";" + NL
            		+ ind + IND + IND + IND + "double a3 = " + a3t + " - 1;" + NL
            		+ ind + IND + IND + IND + "double aux = sqrt(max<double>(0,a1*a1 - a2*a3));" + NL
            		+ ind + IND + IND + IND + "if ((a1 > 0 || (-a1 + aux) * tls_" + id + " < 0) && (-a1 - aux) * tls_" + id + " >= 0) {" + NL
            		+ ind + IND + IND + IND + IND + "disttmp = (-a1 - aux)/a2;" + NL
            		+ ind + IND + IND + IND + "} else {" + NL
            		+ ind + IND + IND + IND + IND + "if ((a1 <= 0 || (-a1 - aux) * tls_" + id + " < 0) && (-a1 + aux) * tls_" + id 
            		+ " >= 0) {" + NL
            		+ ind + IND + IND + IND + IND + IND + "disttmp = (-a1 + aux)/a2;" + NL
            		+ ind + IND + IND + IND + IND + "} else {" + NL
            		+ ind + IND + IND + IND + IND + IND + "disttmp = tLS_" + id + index + ";" + NL
            		+ ind + IND + IND + IND + IND + "}" + NL
            		+ ind + IND + IND + IND + "}" + NL
            		+ ind + IND + IND + IND + "if (equalsEq(disttmp, tLS_" + id + index + ")) {" + NL
            		+ ind + IND + IND + IND + IND + "change = false;" + NL
            		+ ind + IND + IND + IND + "}" + NL
            		+ ind + IND + IND + "}" + NL
            		+ ind + IND + IND + "if (change && p." + coord + " + 1 > " + coord + "last - d_ghost_width - 1 && p.p" + coord 
            		+ " == 2) {" + NL
                    + epstmps + NL
                    + ind + IND + IND + IND + "double tls_" + id + " = tLS_" + id + index + ";" + NL
                    + ind + IND + IND + IND + "double disttmp;" + NL
                    + ind + IND + IND + IND + "double a1 = " + a1t + ";" + NL
                    + ind + IND + IND + IND + "double a2 = " + a2t + ";" + NL
                    + ind + IND + IND + IND + "double a3 = " + a3t + " - 1;" + NL
                    + ind + IND + IND + IND + "double aux = sqrt(max<double>(0,a1*a1 - a2*a3));" + NL
                    + ind + IND + IND + IND + "if ((a1 > 0 || (-a1 + aux) * tls_" + id + " < 0) && (-a1 - aux) * tls_" + id + " >= 0) {" + NL
                    + ind + IND + IND + IND + IND + "disttmp = (-a1 - aux)/a2;" + NL
                    + ind + IND + IND + IND + "} else {" + NL
                    + ind + IND + IND + IND + IND + "if ((a1 <= 0 || (-a1 - aux) * tls_" + id + " < 0) && (-a1 + aux) * tls_" + id + " >= 0) {" + NL
                    + ind + IND + IND + IND + IND + IND + "disttmp = (-a1 + aux)/a2;" + NL
                    + ind + IND + IND + IND + IND + "} else {" + NL
                    + ind + IND + IND + IND + IND + IND + "disttmp = tLS_" + id + index + ";" + NL
                    + ind + IND + IND + IND + IND + "}" + NL
                    + ind + IND + IND + IND + "}" + NL
                    + ind + IND + IND + IND + "if (equalsEq(disttmp, tLS_" + id + index + ")) {" + NL
                    + ind + IND + IND + IND + IND + "change = false;" + NL
                    + ind + IND + IND + IND + "}" + NL
                    + ind + IND + IND + "}" + NL;
        }
        a1 = a1.substring(0, a1.lastIndexOf(" + "));
        a2 = a2.substring(0, a2.lastIndexOf(" + "));
        epstmp = epstmp.substring(0, epstmp.length() - 2);
        return ind + "while (!candidates2.empty()) {" + NL
                + ind + IND + "particle p = *candidates2.begin();" + NL
                + ind + IND + "candidates2.erase(candidates2.begin());" + NL + NL
                + ind + IND + "bool change = true;" + NL
                + eps
                + ind + IND + "double dist;" + NL
                + ind + IND + "bool consolm2, consolm1, consolp1, consolp2;" + NL + NL
                + indexes
                + ind + IND + "double ls_" + id + " = tLS_" + id + array + ";" + NL
                + ind + IND + "double valuem2, valuem1, valuep1, valuep2;" + NL
                + epsCalc
                + ind + IND + "double a1 = " + a1 + ";" + NL
                + ind + IND + "double a2 = " + a2 + ";" + NL
                + ind + IND + "double a3 = " + a3 + " - 1;" + NL
                + ind + IND + "double aux = sqrt(max<double>(0,a1*a1 - a2*a3));" + NL
                + ind + IND + "if ((a1 > 0 || (-a1 + aux) * ls_" + id + " < 0) && (-a1 - aux) * ls_" + id + " >= 0) {" + NL
                + ind + IND + IND + "dist = (-a1 - aux)/a2;" + NL
                + ind + IND + "} else {" + NL
                + ind + IND + IND + "if ((a1 <= 0 || (-a1 - aux) * ls_" + id + " < 0) && (-a1 + aux) * ls_" + id + " >= 0) {" + NL
                + ind + IND + IND + IND + "dist = (-a1 + aux)/a2;" + NL
                + ind + IND + IND + "} else {" + NL
                + ind + IND + IND + IND + "dist = tLS_" + id + index + ";" + NL
                + ind + IND + IND + "}" + NL
                + ind + IND + "}" + NL
                + ind + IND + "if (equalsEq(dist, tLS_" + id + index + ")) {" + NL
                + ind + IND + IND + "change = false;" + NL
                + ind + IND + "}" + NL + NL
                + ind + IND + "double " + epstmp + ";" + NL
                + ind + IND + "if (change) {" + NL
                + change
                + ind + IND + "}" + NL + NL
                + ind + IND + "if (change && ls_" + id + "*dist > 0 && lessThan(minThreshold, fabs(tLS_" + id + index + "))) {" + NL
                + ind + IND + IND + "tLS_" + id + index + " = dist;" + NL
                + ind + IND + IND + "particle ptmp2 = particle(" + pIndex + ppIndex + "dist);" + NL
                + ind + IND + IND + "candidates.insert(ptmp2);" + NL
                + ind + IND + IND + "if (fabs(dist) < threshold) {" + NL
                + ind + IND + IND + IND + "threshold = fabs(dist);" + NL
                + ind + IND + IND + "}" + NL
                + ind + IND + IND + "send = 1;" + NL
                + ind + IND + "}" + NL
                + ind + IND + "cons" + index + " = true;" + NL
                + ind + "}" + NL;
    }
    
    /**
     * Code to select candidates.
     * @param pi        The problem info
     * @param id        The region id
     * @param ind       The current indent
     * @return          The code
     */
    public static String candidateInsert(ProblemInfo pi, String id, String ind) {
        String indent = ind;
        String indent2 = ind + IND;
        String arrayIndex = "";
        String indexes = "";
        String cond = "";
        String index = "";
        String index1 = "";
        String pIndex = "";
        String thresholdeCond = "";
        String boundCond = "";
        String pLoopInit2 = "";
        String pLoopEnd2 = "";
        for (int i = 0; i < pi.getDimensions(); i++) {
            String coord = pi.getCoordinates().get(i);
            arrayIndex = arrayIndex + "[" + coord + " * 3]";
            index1 = index1 + coord + ", ";
            pIndex = pIndex + "p" + (i + 1) + ", ";
            index = index + "[index" + i + " + p" + (i + 1) + "]";
            cond = cond + "(" + coord + " >= d_ghost_width || touch" + coord + "l) && (" + coord + " < " + coord + "last - d_ghost_width || touch"
                    + coord + "u) && ";
            String indexm1 = "";
            String indexp1 = "";
            for (int k = 0; k < pi.getDimensions(); k++) {
                if (k == i) {
                    indexm1 = indexm1 + "[index" + k + " + p" + (k + 1) + " - 1]";
                    indexp1 = indexp1 + "[index" + k + " + p" + (k + 1) + " + 1]";
                }
                else {
                    indexm1 = indexm1 + "[index" + k + " + p" + (k + 1) + "]";
                    indexp1 = indexp1 + "[index" + k + " + p" + (k + 1) + "]";
                }
            }
            thresholdeCond = thresholdeCond + "(indexM(" + coord + ", p" + (i + 1) + ", 1) < " + coord 
                    + "last && lessEq(threshold, fabs(tLS_" + id + indexp1 + "))) || (indexM(" + coord + ", p" + (i + 1) 
                    + ", -1) >= 0 && lessEq(threshold, fabs(tLS_" + id + indexm1 + "))) || ";
            boundCond = boundCond + "(" + coord + " == d_ghost_width - 1) || (" + coord + " == " + coord + "last - d_ghost_width) || ";
            indent = indent + IND;
            indent2 = indent2 + IND + IND;
        }
        String indent3 = indent + IND + IND + IND;
        for (int i = 0; i < pi.getDimensions(); i++) {
            String coord = pi.getCoordinates().get(i);
            indexes = indexes + indent + IND + "int index" + i + " = " + coord + " * " + INTERPHASELENGTH + ";" + NL;
            if (i > 0) {
                pLoopInit2 = pLoopInit2 + indent3 + "for (int p" + i + " = 0; p" + i + " < " + INTERPHASELENGTH + "; p" + i + "++) {" + NL;
                pLoopEnd2 = indent3 + "}" + NL + pLoopEnd2;
                indent3 = indent3 + IND;
            }
        }
        cond = cond.substring(0, cond.lastIndexOf(" &&"));
        thresholdeCond = thresholdeCond.substring(0, thresholdeCond.lastIndexOf(" || "));
        boundCond = boundCond.substring(0, boundCond.lastIndexOf(" || "));
        String consBound = "";
        for (int i = 0; i < pi.getDimensions(); i++) {
            String coord = pi.getCoordinates().get(i);
            String indexm1 = "";
            String indexm2 = "";
            String indexp0 = "";
            String indexp1 = "";
            String indexp2 = "";
            String indexp3 = "";
            String indexp4 = "";
            String nIndexp0 = "";
            String nIndexp2 = "";
            int acc = 0;
            for (int k = 0; k < pi.getDimensions(); k++) {
                if (k == i) {
                    indexm1 = indexm1 + "[index" + k + " - 1]";
                    indexm2 = indexm2 + "[index" + k + " - 2]";
                    indexp0 = indexp0 + "[index" + k + "]";
                    indexp1 = indexp1 + "[index" + k + " + 1]";
                    indexp2 = indexp2 + "[index" + k + " + 2]";
                    indexp3 = indexp3 + "[index" + k + " + 3]";
                    indexp4 = indexp4 + "[index" + k + " + 4]";
                    nIndexp0 = nIndexp0 + "0, ";
                    nIndexp2 = nIndexp2 + "2, ";
                }
                else {
                    indexm1 = indexm1 + "[index" + k + " + p" + (acc + 1) + "]";
                    indexm2 = indexm2 + "[index" + k + " + p" + (acc + 1) + "]";
                    indexp0 = indexp0 + "[index" + k + " + p" + (acc + 1) + "]";
                    indexp1 = indexp1 + "[index" + k + " + p" + (acc + 1) + "]";
                    indexp2 = indexp2 + "[index" + k + " + p" + (acc + 1) + "]";
                    indexp3 = indexp3 + "[index" + k + " + p" + (acc + 1) + "]";
                    indexp4 = indexp4 + "[index" + k + " + p" + (acc + 1) + "]";
                    nIndexp0 = nIndexp0 + "p" + (acc + 1) + ", ";
                    nIndexp2 = nIndexp2 + "p" + (acc + 1) + ", ";
                    acc++;
                }
            }
            nIndexp2 = nIndexp2.substring(0, nIndexp2.length() - 2);
            nIndexp0 = nIndexp0.substring(0, nIndexp0.length() - 2);
            consBound = consBound + indent3 + "if (" + coord + " == d_ghost_width - 1 && lessEq(fabs(tLS_" + id + indexp2 
                    + "), minThreshold)) {" + NL
                    + indent3 + IND + "cons" + indexp2 + " = true;" + NL
                    + indent3 + IND + "candidates.insert(particle(" + index1 + nIndexp2 + ", tLS_" + id + indexp2 + "));" + NL
                    + indent3 + IND + "cons" + indexp1 + " = true;" + NL
                    + indent3 + "}" + NL
                    + indent3 + "if (" + coord + " == d_ghost_width - 1 && cons" + indexp2 + ") {" + NL
                    + indent3 + IND + "alter" + indexp3 + " = true;" + NL
                    + indent3 + IND + "alter" + indexp4 + " = true;" + NL
                    + indent3 + "}" + NL
                    + indent3 + "if (" + coord + " == " + coord + "last - d_ghost_width && lessEq(fabs(tLS_" + id + indexp0 
                    + "), minThreshold)) {" + NL
                    + indent3 + IND + "cons" + indexp0 + " = true;" + NL
                    + indent3 + IND + "candidates.insert(particle(" + index1 + nIndexp0 + ", tLS_" + id + indexp0 + "));" + NL
                    + indent3 + IND + "cons" + indexp1 + " = true;" + NL
                    + indent3 + "}" + NL
                    + indent3 + "if (" + coord + " == " + coord + "last - d_ghost_width && cons" + indexp0 + ") {" + NL
                    + indent3 + IND + "alter" + indexm1 + " = true;" + NL
                    + indent3 + IND + "alter" + indexm2 + " = true;" + NL
                    + indent3 + "}" + NL;
        }
        String consInit = "";
        ArrayList<String> elements = new ArrayList<String>();
        for (int i = 0; i < INTERPHASELENGTH; i++) {
            elements.add(String.valueOf(i));
        }
        ArrayList<String> combinations = CodeGeneratorUtils.createCombinations(pi.getDimensions(), elements);  
        for (int i = 0; i < combinations.size(); i++) {
            String comb = combinations.get(i);
            String arrayIndex2 = "";
            for (int j = 0; j < comb.length(); j++) {
                arrayIndex2 = arrayIndex2 + "[index" + j + " + " + comb.substring(j, j + 1) + "]";
            }
            consInit = consInit + indent + IND + IND + "cons" + arrayIndex2 + " = false;" + NL;
        }
        return loopInitLasts(pi.getCoordinates(), ind)
                + indent + "if (tLS_" + id + arrayIndex + " < 998) {" + NL
                + indexes
                + indent + IND + "if (" + cond + ") {" + NL
                + consInit
                + pLoopInit(pi.getCoordinates(), indent + IND + IND, false, false)
                + indent2 + IND + "if (lessEq(fabs(tLS_" + id + index + "), threshold)) {" + NL
                + indent2 + IND + IND + "cons" + index + " = true;" + NL
                + indent2 + IND + IND + "if (" + thresholdeCond + ") {" + NL
                + indent2 + IND + IND + IND + "candidates.insert(particle(" + index1 + pIndex + "tLS_" + id + index + "));" + NL
                + indent2 + IND + IND + "}" + NL
                + indent2 + IND + "}" + NL
                + loopEnd(pi.getDimensions(), indent + IND + IND)
                + indent + IND + "} else {" + NL
                + indent + IND + IND + "if (" + boundCond + ") {" + NL
                + pLoopInit2
                + consBound
                + pLoopEnd2
                + indent + IND + IND + "}" + NL
                + indent + IND + "}" + NL
                + indent + "}" + NL
                + loopEnd(pi.getDimensions(), ind);
    }
    
    /**
     * Calculates the level set from the neighbours.
     * @param pi            The problem info
     * @param id            The region id
     * @param op            The operation to use in the noeighbourhood
     * @param coordI        The base coordinate
     * @param ind           The current indent
     * @return              The code
     * @throws CGException  CG00X External error
     */
    public static String calculateLSfromNeighbours(ProblemInfo pi, String id, String op, int coordI, String ind) throws CGException {
        String coordIndex = "";
        String cond = "";
        String index = "";
        String pIndex = "";
        String ppIndex = "";
        String baseIndex = " " + op + " 1";
        for (int i = 0; i < pi.getDimensions(); i++) {
            String coord = pi.getCoordinates().get(i);
            if (i == coordI) {
                index = index + "[index" + coord + " " + op + " 1]";
                pIndex = pIndex + "indexM(p." + coord + ", p.p" + coord + ", " + op + "1), ";
                ppIndex = ppIndex + "indexPM(p.p" + coord + ", " + op + "1), ";
            }
            else {
                index = index + "[index" + coord + "]";
                pIndex = pIndex + "p." + coord + ", ";
                ppIndex = ppIndex + "p.p" + coord + ", ";
            }
        }
        String epsVars = "";
        String epsCalc = "";
        String a1 = "";
        String a2 = "";
        String a3 = "";
        String pCout = "";
        String ppCout = "";
        for (int i = 0; i < pi.getDimensions(); i++) {
            String coord = pi.getCoordinates().get(i);
            String contCoord = CodeGeneratorUtils.discToCont(pi.getProblem(), coord);
            if (i == coordI) {
                coordIndex = coordIndex + op + "1, ";
                cond = cond + "(indexM(p." + coord + ", p.p" + coord + ", " + op + "1) >= d_ghost_width || (touch" + coord 
                        + "l && indexM(p." + coord + ", p.p" + coord + ", " + op + "1) >= 0)) && (indexM(p." 
                        + coord + ", p.p" + coord + ", " + op + "1) < " + coord + "last - d_ghost_width || (touch" + coord 
                        + "u && indexM(p." + coord + ", p.p" + coord + ", " + op + "1) < " + coord + "last)) && ";
            }
            else {
                coordIndex = coordIndex + "0, ";
                cond = cond + "(p." + coord + " >= d_ghost_width || (touch" + coord + "l && p." + coord + " >= 0)) && (p." + coord + " < " + coord 
                        + "last - d_ghost_width || (touch" + coord + "u && p." + coord + " < " + coord + "last)) && ";
            }
            String var = "c";
            if (i == 1) {
                var = "d";
            }
            if (i == 2) {
                var = "e";
            }
            epsVars = epsVars + ind + IND + IND + "double " + var + "3 = 0;" + NL
                    + ind + IND + IND + "double eps" + contCoord + " = 0;" + NL;
            String base = "";
            if (i == coordI) {
                base = " " + op + " 1";
            }
            String indexm1 = "";
            String indexm2 = "";
            String indexp1 = "";
            String indexp2 = "";
            for (int j = 0; j < pi.getDimensions(); j++) {
                String coord2 = pi.getCoordinates().get(j);
                if (i == j) {
                    if (j == coordI) {
                        indexm1 = indexm1 + "[index" + coord2 + " - 1" + baseIndex + "]";
                        indexm2 = indexm2 + "[index" + coord2 + " - 2" + baseIndex + "]";
                        indexp1 = indexp1 + "[index" + coord2 + " + 1" + baseIndex + "]";
                        indexp2 = indexp2 + "[index" + coord2 + " + 2" + baseIndex + "]";
                    }
                    else {
                        indexm1 = indexm1 + "[index" + coord2 + " - 1]";
                        indexm2 = indexm2 + "[index" + coord2 + " - 2]";
                        indexp1 = indexp1 + "[index" + coord2 + " + 1]";
                        indexp2 = indexp2 + "[index" + coord2 + " + 2]";
                    }
                }
                else {
                    if (j == coordI) {
                        indexm1 = indexm1 + "[index" + coord2 + baseIndex + "]";
                        indexm2 = indexm2 + "[index" + coord2 + baseIndex + "]";
                        indexp1 = indexp1 + "[index" + coord2 + baseIndex + "]";
                        indexp2 = indexp2 + "[index" + coord2 + baseIndex + "]";
                    }
                    else {
                        indexm1 = indexm1 + "[index" + coord2 + "]";
                        indexm2 = indexm2 + "[index" + coord2 + "]";
                        indexp1 = indexp1 + "[index" + coord2 + "]";
                        indexp2 = indexp2 + "[index" + coord2 + "]";
                    }
                }
            }
            epsCalc = epsCalc + ind + IND + IND + "//" + contCoord + NL
                    + ind + IND + IND + "if (indexM(p." + coord + ", p.p" + coord + ", - 1" + base + ") < 0 || !cons" 
                    + indexm1 + ") {" + NL
                    + ind + IND + IND + IND + "consolm1 = false;" + NL
                    + ind + IND + IND + "} else {" + NL
                    + ind + IND + IND + IND + "consolm1 = true;" + NL
                    + ind + IND + IND + IND + "valuem1 = tLS_" + id + indexm1 + ";" + NL
                    + ind + IND + IND + "}" + NL
                    + ind + IND + IND + "if (!consolm1 || indexM(p." + coord + ", p.p" + coord + ", -2" + base 
                    + ") < 0 || !cons" + indexm2 + ") {" + NL
                    + ind + IND + IND + IND + "consolm2 = false;" + NL
                    + ind + IND + IND + "} else {" + NL
                    + ind + IND + IND + IND + "consolm2 = true;" + NL
                    + ind + IND + IND + IND + "valuem2 = tLS_" + id + indexm2 + ";" + NL
                    + ind + IND + IND + "}" + NL
                    + ind + IND + IND + "if (indexM(p." + coord + ", p.p" + coord + ", 1" + base + ") >= " + coord 
                    + "last || !cons" + indexp1 + ") {" + NL
                    + ind + IND + IND + IND + "consolp1 = false;" + NL
                    + ind + IND + IND + "} else {" + NL
                    + ind + IND + IND + IND + "consolp1 = true;" + NL
                    + ind + IND + IND + IND + "valuep1 = tLS_" + id + indexp1 + ";" + NL
                    + ind + IND + IND + "}" + NL
                    + ind + IND + IND + "if (!consolp1 || indexM(p." + coord + ", p.p" + coord + ", 2" + base + ") >= " 
                    + coord + "last || !cons" + indexp2 + ") {" + NL
                    + ind + IND + IND + IND + "consolp2 = false;" + NL
                    + ind + IND + IND + "} else {" + NL
                    + ind + IND + IND + IND + "consolp2 = true;" + NL
                    + ind + IND + IND + IND + "valuep2 = tLS_" + id + indexp2 + ";" + NL
                    + ind + IND + IND + "}" + NL
                    + ind + IND + IND + "//0-11" + NL
                    + ind + IND + IND + "if (consolp1 && consolp2 && !consolm1) {" + NL
                    + ind + IND + IND + IND + var + "3 = -1.5;" + NL
                    + ind + IND + IND + IND + "eps" + contCoord + " = 2 * valuep1 - 0.5 * valuep2;" + NL
                    + ind + IND + IND + "} else" + NL
                    + ind + IND + IND + "//11-0" + NL
                    + ind + IND + IND + "if (consolm2 && consolm1 && !consolp1) {" + NL
                    + ind + IND + IND + IND + var + "3 = -1.5;" + NL
                    + ind + IND + IND + IND + "eps" + contCoord + " = -0.5 * valuem2 + 2 * valuem1;" + NL
                    + ind + IND + IND + "} else" + NL
                    + ind + IND + IND + "//0-10" + NL
                    + ind + IND + IND + "if (!consolm1 && consolp1 && !consolp2) {" + NL
                    + ind + IND + IND + IND + var + "3 = -1;" + NL
                    + ind + IND + IND + IND + "eps" + contCoord + " = valuep1;" + NL
                    + ind + IND + IND + "} else" + NL
                    + ind + IND + IND + "//01-0" + NL
                    + ind + IND + IND + "if (!consolm2 && consolm1 && !consolp1) {" + NL
                    + ind + IND + IND + IND + var + "3 = -1;" + NL
                    + ind + IND + IND + IND + "eps" + contCoord + " = valuem1;" + NL
                    + ind + IND + IND + "} else" + NL
                    + ind + IND + IND + "if (consolm1 && consolp1) {" + NL
                    + ind + IND + IND + IND + "if (fabs(valuem1) < fabs(valuep1)) {" + NL
                    + ind + IND + IND + IND + IND + "//01-1x" + NL
                    + ind + IND + IND + IND + IND + "if (!consolm2) {" + NL
                    + ind + IND + IND + IND + IND + IND + var + "3 = -1;" + NL
                    + ind + IND + IND + IND + IND + IND + "eps" + contCoord + " = valuem1;" + NL
                    + ind + IND + IND + IND + IND + "//11-1x" + NL
                    + ind + IND + IND + IND + IND + "} else {" + NL
                    + ind + IND + IND + IND + IND + IND + var + "3 = -1.5;" + NL
                    + ind + IND + IND + IND + IND + IND + "eps" + contCoord + " = -0.5 * valuem2 + 2 * valuem1;" + NL
                    + ind + IND + IND + IND + IND + "}" + NL
                    + ind + IND + IND + IND + "} else {" + NL
                    + ind + IND + IND + IND + IND + "//x1-10" + NL
                    + ind + IND + IND + IND + IND + "if (!consolp2) {" + NL
                    + ind + IND + IND + IND + IND + IND + var + "3 = -1;" + NL
                    + ind + IND + IND + IND + IND + IND + "eps" + contCoord + " = valuep1;" + NL
                    + ind + IND + IND + IND + IND + "//x1-11" + NL
                    + ind + IND + IND + IND + IND + "} else {" + NL
                    + ind + IND + IND + IND + IND + IND + var + "3 = -1.5;" + NL
                    + ind + IND + IND + IND + IND + IND + "eps" + contCoord + " = -0.5 * valuep2 + 2 * valuep1;" + NL
                    + ind + IND + IND + IND + IND + "}" + NL
                    + ind + IND + IND + IND + "}" + NL
                    + ind + IND + IND + "}" + NL;
            a1 = a1 + var + "3*eps" + contCoord + "/d" + contCoord + "p2 + ";
            a2 = a2 + var + "3*" + var + "3/d" + contCoord + "p2 + ";
            a3 = a3 + "eps" + contCoord + "*eps" + contCoord + "/d" + contCoord + "p2 + ";
            pCout = pCout + "p." + coord + "<<\" \"<<";
            ppCout = ppCout + "p.p" + coord + "<<\" \"<<";
        }
        coordIndex = coordIndex.substring(0, coordIndex.length() - 2);
        a1 = a1.substring(0, a1.lastIndexOf(" + "));
        a2 = a2.substring(0, a2.lastIndexOf(" + "));
        a3 = a3.substring(0, a3.lastIndexOf(" + "));
        String coord = pi.getCoordinates().get(coordI);
        return ind + "//" + coordIndex + NL
                + ind + "if (" + cond + "!cons" + index + " && tLS_" + id + index + " < 998) {" + NL
                + ind + IND + "particle ptmp = particle(" + pIndex + ppIndex + "tLS_" + id + index + ");" +  NL
                + ind + IND + "double ls_p = tLS_" + id + index + ";" + NL
                + ind + IND + "if (alter" + index + " || fabs(tLS_" + id + index + ") > 98) {" + NL
                + ind + IND + IND + "candidates.erase(ptmp);" + NL
                + epsVars
                + ind + IND + IND + "bool consolm2, consolm1, consolp1, consolp2;" + NL
                + ind + IND + IND + "double valuem2, valuem1, valuep1, valuep2;" + NL
                + epsCalc
                + ind + IND + IND + "double a1 = " + a1 + ";" + NL
                + ind + IND + IND + "double a2 = " + a2 + ";" + NL
                + ind + IND + IND + "double a3 = " + a3 + " - 1;" + NL
                + ind + IND + IND + "double aux = sqrt(max<double>(0,a1*a1 - a2*a3));" + NL
                + ind + IND + IND + "if ((a1 > 0 || (-a1 + aux) * tLS_" + id + index + " < 0) && (-a1 - aux) * tLS_" + id + index + " >= 0) {" + NL
                + ind + IND + IND + IND + "ptmp.dist = (-a1 - aux)/a2;" + NL
                + ind + IND + IND + "} else {" + NL
                + ind + IND + IND + IND + "if ((a1 <= 0 || (-a1 - aux) * tLS_" + id + index + " < 0) && (-a1 + aux) * tLS_" 
                + id + index + " >= 0) {" + NL
                + ind + IND + IND + IND + IND + "ptmp.dist = (-a1 + aux)/a2;" + NL
                + ind + IND + IND + IND + "} else {" + NL
                + ind + IND + IND + IND + IND + "ptmp.dist = tLS_" + id + index + ";" + NL
                + ind + IND + IND + IND + IND + "cout<<\"Fast marching warning " + coord + op + "1 \"<<" + pCout + ppCout 
                + "\" + \"<<(-a1 + aux)/a2<<\" - \"<<(-a1 - aux)/a2<<\" parent \"<<ls_" + id + "<<\" value \"<<tLS_" + id + index + "<<endl;" + NL
                + ind + IND + IND + IND + "}" + NL
                + ind + IND + IND + "}" + NL
                + ind + IND + IND + "tLS_" + id + index + " = ptmp.dist;" + NL
                + ind + IND + "}" + NL
                + ind + IND + "if (fabs(ls_p) < fabs(ptmp.dist)) {" + NL
                + ind + IND + IND + "tLS_" + id + index + " = ls_p;" + NL
                + ind + IND + IND + "ptmp.dist = ls_p;" + NL
                + ind + IND + "}" + NL
                + ind + IND + "candidates.insert(ptmp);" + NL
                + ind + "}" + NL;
    }
    
    /**
     * Creates the code that calculates de level set.
     * @param pi            The problem info
     * @param id            The id of the region
     * @param ind           The current indent
     * @return              The code
     * @throws CGException  CG00X External error 
     */
    public static String calculateLSRedistancing(ProblemInfo pi, String id, String ind) throws CGException {
        String indexes = "";
        String arrayIndex = "";
        String alterInit = "";
        String neighbours = "";
        for (int i = 0; i < pi.getDimensions(); i++) {
            String coord = pi.getCoordinates().get(i);
            indexes = indexes + ind + IND + "int index" + coord + " = (p." + coord + ") * " + INTERPHASELENGTH + " + p.p" + coord + ";" + NL;
            arrayIndex = arrayIndex + "[index" + coord + "]";
            String indexm1 = "";
            String indexm2 = "";
            String indexp1 = "";
            String indexp2 = "";
            for (int j = 0; j < pi.getDimensions(); j++) {
                String coord2 = pi.getCoordinates().get(j);
                if (i == j) {
                    indexm1 = indexm1 + "[index" + coord2 + " - 1]";
                    indexm2 = indexm2 + "[index" + coord2 + " - 2]";
                    indexp1 = indexp1 + "[index" + coord2 + " + 1]";
                    indexp2 = indexp2 + "[index" + coord2 + " + 2]";
                }
                else {
                    indexm1 = indexm1 + "[index" + coord2 + "]";
                    indexm2 = indexm2 + "[index" + coord2 + "]";
                    indexp1 = indexp1 + "[index" + coord2 + "]";
                    indexp2 = indexp2 + "[index" + coord2 + "]";
                }
            }
            alterInit = alterInit + ind + IND + IND + "if (!touch" + coord + "l && index" + coord + " + 1 < " + coord + "last * " 
                + INTERPHASELENGTH + ")" + NL
                + ind + IND + IND + IND + "alter" + indexp1 + " = true;" + NL
                + ind + IND + IND + "if (!touch" + coord + "l && index" + coord + " + 2 < " + coord + "last * " + INTERPHASELENGTH + ")" + NL
                + ind + IND + IND + IND + "alter" + indexp2 + " = true;" + NL
                + ind + IND + IND + "if (!touch" + coord + "u && index" + coord + " - 1 >= 0)" + NL
                + ind + IND + IND + IND + "alter" + indexm1 + " = true;" + NL
                + ind + IND + IND + "if (!touch" + coord + "u && index" + coord + " - 2 >= 0)" + NL
                + ind + IND + IND + IND + "alter" + indexm2 + " = true;" + NL;
            neighbours = neighbours + calculateLSfromNeighbours(pi, id, "+", i, ind + IND)
                    + calculateLSfromNeighbours(pi, id, "-", i, ind + IND);
        }

        return ind + "while (!candidates.empty()) {" + NL
                + ind + IND + "particle p = *candidates.begin();" + NL
                + indexes
                + ind + IND + "candidates.erase(candidates.begin());" + NL
                + ind + IND + "double ls_" + id + " = p.dist;" + NL
                + ind + IND + "if (!first && !Equals(tLS_" + id + "_p" + arrayIndex + ", ls_" + id + ")) {" + NL
                + alterInit
                + ind + IND + "}" + NL
                + ind + IND + "double fabsls_" + id + " = fabs(ls_" + id + ");" + NL
                + ind + IND + "if (!cons" + arrayIndex + " && lessThan(fabsls_" + id + ", lastCons) && lessThan(minThreshold, fabsls_" + id + ")) {" + NL
                + ind + IND + IND + "repeat = true;" + NL
                + ind + IND + IND + "thresholdtmp = min<double>(fabsls_" + id + ", thresholdtmp);" + NL
                + ind + IND + "}" + NL
                + ind + IND + "cons" + arrayIndex + " = true;" + NL
                + ind + IND + "lastCons = fabsls_" + id + ";" + NL + NL
                + ind + IND + "//Modify neighbours" + NL
                + neighbours
                + ind + "}" + NL;
    }
    
    /**
     * Creates the code to do the redistancing.
     * @param pi            The problem info
     * @return              The code
     * @throws CGException  CG00X External error 
     */
    public static String redistancing(ProblemInfo pi) throws CGException {
        String dx2 = "";
        String dx = "";
        String index = "";
        String arrayIndex = "";
        String arrayIndex2 = "";
        String arrayIndex3 = "";
        String startLoopLast3 = "";
        String indent = IND + IND + IND + IND + IND + IND;
        String indent2 = IND + IND + IND + IND + IND + IND + IND;
        String indent3 = IND + IND + IND + IND + IND;
        String indent4 = IND + IND + IND + IND;
        for (int i = 0; i < pi.getDimensions(); i++) {
            String coord = pi.getCoordinates().get(i);
            String contCoord = CodeGeneratorUtils.discToCont(pi.getProblem(), coord);
            dx2 = dx2 + "dx[" + i + "]*dx[" + i + "] + ";
            dx = dx + IND + IND + IND + IND + "double d" + contCoord + "p2 = (dx[" + i + "]*dx[" + i + "]/" 
                    + Math.pow(INTERPHASELENGTH, pi.getDimensions()) + ");" + NL;
            arrayIndex = arrayIndex + "[" + coord + " * 3]";
            arrayIndex2 = arrayIndex2 + "[" + coord + "]";
            arrayIndex3 = arrayIndex3 + "[" + coord + " * 3 + p" + (i + 1) + "]";
            indent = indent + IND;
            indent2 = indent2 + IND + IND;
            startLoopLast3 = startLoopLast3 + indent3 + "for (int " + coord + " = 0; " + coord + " < " + coord + "last * " + INTERPHASELENGTH + "; " 
                    + coord + "++) {" + NL;
            indent3 = indent3 + IND;
            indent4 = indent4 + IND;
            index = index + coord + ", ";
        }
        dx2 = dx2.substring(0, dx2.lastIndexOf(" + "));
        index = index.substring(0, index.length() - 2);
        

        ArrayList<String> elements = new ArrayList<String>();
        for (int i = 0; i < INTERPHASELENGTH; i++) {
            elements.add(String.valueOf(i));
        }
        ArrayList<String> combinations = CodeGeneratorUtils.createCombinations(pi.getDimensions(), elements);  
        String redistancings = "";
        for (int i = 0; i < pi.getMovementInfo().getMovRegions().size(); i++) {
            String segName = pi.getMovementInfo().getMovRegions().get(i); 
            String id = CodeGeneratorUtils.getRegionId(pi.getProblem(), pi.getRegions(), segName);
            String uncons = "";
            String cons = "";
            for (int j = 0; j < combinations.size(); j++) {
                String comb = combinations.get(j);
                String combIndex = "";
                String aCombIndex = "";
                for (int k = 0; k < comb.length(); k++) {
                    String coord2 = pi.getCoordinates().get(k);
                    combIndex = combIndex + comb.substring(k, k + 1) + ", ";
                    aCombIndex = aCombIndex + "[" + coord2 + " * " + INTERPHASELENGTH + " + " + comb.substring(k, k + 1) + "]";
                }
                combIndex = combIndex.substring(0, combIndex.length() - 2);
                uncons = uncons + indent4 + IND + IND + "vectorP(ls_" + id + ", " + index + ", " + combIndex + ") = 9999999999;" + NL;
                cons = cons + indent4 + IND + IND + "vectorP(ls_" + id + ", " + index + ", " + combIndex + ") = tLS_" + id + aCombIndex 
                        + ";" + NL;
            }
            ArrayList<String> tmpVars = new ArrayList<String>();
            tmpVars.add("tLS_" + id + "_p");
            tmpVars.add("tLS_" + id);
            tmpVars.add("alter");
            tmpVars.add("cons");
            redistancings = redistancings + IND + IND + "end = false;" + NL
                    + IND + IND + "first = true;" + NL
                    + IND + IND + "while (!end) {" + NL
                    + IND + IND + IND + "int send = 0;" + NL
                    + IND + IND + IND + "end = true;" + NL
                    + IND + IND + IND + "d_mapping_fill2->createSchedule(level, level)->fillData(time, true);" + NL
                    + IND + IND + IND + "for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {" + NL
                    + IND + IND + IND + IND + "const std::shared_ptr< hier::Patch >& patch = *p_it;" + NL
                    + IND + IND + IND + IND + "double* LS_" + id + " = ((pdat::CellData<double> *) patch->getPatchData(d_LS_" 
                    + id + "_id).get())->getPointer();" + NL
                    + IND + IND + IND + IND + "int* soporte = ((pdat::CellData<int> *) patch->getPatchData(d_soporte_id).get())" 
                    + "->getPointer();" + NL
                    + lsDeclaration(pi, IND + IND + IND + IND)
                    + IND + IND + IND + IND + "const hier::Index boxfirst = patch->getBox().lower();" + NL
                    + IND + IND + IND + IND + "const hier::Index boxlast  = patch->getBox().upper();" + NL
                    + IND + IND + IND + IND + "const hier::Index boxfirst1 = ((pdat::CellData<int> *) patch->getPatchData("
                    + "d_soporte_id).get())->getGhostBox().lower();" + NL
                    + IND + IND + IND + IND + "const hier::Index boxlast1  = ((pdat::CellData<int> *) patch->getPatchData("
                    + "d_soporte_id).get())->getGhostBox().upper();" + NL + NL
                    + IND + IND + IND + IND + "//Get delta spaces into an array. dx, dy, dz." + NL
                    + IND + IND + IND + IND + "const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));" + NL
                    + IND + IND + IND + IND + "const double* dx  = patch_geom->getDx();" + NL + NL
                    + SAMRAIUtils.getLasts(pi, IND + IND + IND + IND, true) + NL
                    + getTouchVars(pi.getCoordinates(), IND + IND + IND + IND, false)
                    + IND + IND + IND + IND + "double minThreshold = sqrt(" + dx2 + ")/3.0;" + NL
                    + allocateRedistTmps(pi, id, IND + IND + IND + IND)
                    + lsTmpInitialization(pi, id, IND + IND + IND + IND)
                    + dx
                    + IND + IND + IND + IND + "//Calculate threshold" + NL
                    + calculateThreshold(pi, id, IND + IND + IND + IND) + NL
                    + IND + IND + IND + IND + "bool repeat = true;" + NL
                    + IND + IND + IND + IND + "if (threshold > 999) {" + NL
                    + IND + IND + IND + IND + IND + "repeat = false;" + NL
                    + IND + IND + IND + IND + IND + "if (end) {" + NL
                    + loopInitLasts(pi.getCoordinates(), IND + IND + IND + IND + IND + IND)
                    + indent + "if (fabs(tLS_" + id + arrayIndex + ") < 998) {" + NL
                    + pLoopInit(pi.getCoordinates(), indent + IND, false, false)
                    + indent2 + "cons" + arrayIndex3 + " = true;" + NL
                    + loopEnd(pi.getDimensions(), indent + IND)
                    + indent + "}" + NL
                    + loopEnd(pi.getDimensions(), IND + IND + IND + IND + IND + IND) 
                    + IND + IND + IND + IND + IND + "}" + NL
                    + IND + IND + IND + IND + "}" + NL + NL
                    + IND + IND + IND + IND + "while (repeat) {" + NL
                    + IND + IND + IND + IND + IND + "repeat = false;" + NL
                    + startLoopLast3
                    + indent3 + "alter" + arrayIndex2 + " = first;" + NL
                    + loopEnd(pi.getDimensions(), IND + IND + IND + IND + IND) 
                    + candidateInsert(pi, id, IND + IND + IND + IND + IND) + NL
                    + IND + IND + IND + IND + IND + "double lastCons = threshold;" + NL
                    + IND + IND + IND + IND + IND + "double thresholdtmp = 99999;" + NL
                    + calculateLSRedistancing(pi, id, IND + IND + IND + IND + IND)
                    + IND + IND + IND + IND + IND + "threshold = thresholdtmp;" + NL
                    + IND + IND + IND + IND + "}" + NL + NL
                    + loopInitLasts(pi.getCoordinates(), IND + IND + IND + IND)
                    + indent4 + "if (vector(soporte, " + index + ") == 1) {" + NL
                    + indent4 + IND + "if (!cons" + arrayIndex + ") {" + NL
                    + uncons
                    + indent4 + IND + "} else {" + NL
                    + cons
                    + indent4 + IND + "}" + NL
                    + indent4 + "}" + NL
                    + loopEnd(pi.getDimensions(), IND + IND + IND + IND)
                    + tmpVarsFree(pi, tmpVars, IND + IND + IND + IND, false) + NL
                    + IND + IND + IND + "}" + NL
                    + IND + IND + IND + "int sendArray[1];" + NL
                    + IND + IND + IND + "sendArray[0] = send;" + NL
                    + IND + IND + IND + "int tmp =mpi.AllReduce(sendArray, 1, MPI_MAX);" + NL
                    + IND + IND + IND + "if (sendArray[0] != 0 || send!=0) {" + NL
                    + IND + IND + IND + IND + "end = false;" + NL
                    + IND + IND + IND + "}" + NL
                    + IND + IND + IND + "first = false;" + NL
                    + IND + IND + "}" + NL;

        }
        return IND + IND + "//Redistancing" + NL
                + IND + IND + "std::set<particle> candidates;" + NL
                + IND + IND + "std::set<particle> candidates2;" + NL
                + IND + IND + "bool end;" + NL
                + IND + IND + "bool first;" + NL
                + redistancings
                + IND + IND + "d_mapping_fill->createSchedule(level, level)->fillData(time, true);" + NL + NL;
    }
    
    /**
     * Level set in cell mapping.
     * @param pi            The problem info
     * @return              The code
     * @throws CGException  CG00X External error 
     */
    public static String lsCellMapping(ProblemInfo pi) throws CGException {
        String indent = IND + IND + IND;
        String indent2 = IND + IND + IND;
        String indent3 = IND + IND + IND;
        String indexBox = "";
        String indexBox1 = "";
        String index = "";
        String index1 = "";
        String dists = "";
        String indexP = "";
        String dist2 = "";
        String dx = ""; 
        for (int i = 0; i < pi.getDimensions(); i++) {
            String coord = pi.getCoordinates().get(i);
            indent = indent + IND;
            indent2 = indent2 + IND + IND;
            indent3 = indent3 + IND + IND + IND;
            indexBox = indexBox + coord + " - boxfirst1(" + i + "), ";
            indexBox1 = indexBox1 + coord + "1 - boxfirst1(" + i + "), ";
            index = index + coord + ", ";
            index1 = index1 + coord + "1, ";
            indexP = indexP + "p" + (i + 1) + ", ";
            if (i > 0) {
                dx = "max(" + dx + ", dx[" + i + "])";
            }
            else {
                dx = dx + "dx[" + i + "]";
            }
        }
        indexBox = indexBox.substring(0, indexBox.lastIndexOf(","));
        index = index.substring(0, index.lastIndexOf(","));
        indexBox1 = indexBox1.substring(0, indexBox1.lastIndexOf(","));
        index1 = index1.substring(0, index1.lastIndexOf(","));
        indexP = indexP.substring(0, indexP.lastIndexOf(","));
        for (int i = 0; i < pi.getDimensions(); i++) {
            String coord = pi.getCoordinates().get(i);
            String contCoord = CodeGeneratorUtils.discToCont(pi.getProblem(), coord);
            dists = dists + indent3 + IND + IND + "double dist" + contCoord + " = (" + coord + " + 0.5) * dx[" + i 
                    + "] + d_grid_geometry->getXLower()[" + i + "] - vectorP_r(position" 
                    + coord + ", sop, " + indexP + ");" + NL;
            dist2 = dist2 + "dist" + contCoord + " * dist" + contCoord + " + ";
        }
        dist2 = dist2.substring(0, dist2.lastIndexOf(" +"));
        dists = dists + indent3 + IND + IND + "double dist = sqrt(" + dist2 + ");" + NL
                + indent3 + IND + IND + "double distNorm = dist / (" + dx + " / 3);" + NL;
        
        String lsKernel = "";
        String lsInit = "";
        String lsAssign = "";
        for (int i = 0; i < pi.getInteriorRegionIds().size(); i++) {
            String id = pi.getInteriorRegionIds().get(i);
            lsInit = lsInit + indent + IND + "vector(LS_" + id + ", " + indexBox + ") = 0;" + NL;
            lsKernel = lsKernel + indent + IND + "vector(LS_" + id + ", " + indexBox + ") = vector(LS_" + id + ", " + indexBox + ") / kernacc;" + NL;
            lsAssign = lsAssign + indent3 + IND + IND + IND + "vector(LS_" + id + ", " + indexBox + ") = vector(LS_" + id + ", " + indexBox 
                    + ") + (vectorP(ls_" + id + ", " + indexBox1 + ", " + indexP + ")) * kern;" + NL;
        }
        
        return IND + IND + "//LS in cells" + NL
                + IND + IND + "for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {" + NL
                + IND + IND + IND + "const std::shared_ptr<hier::Patch >& patch = *p_it;" + NL
                + IND + IND + IND + "std::shared_ptr< pdat::IndexData<Interphase, pdat::CellGeometry > > "
                + "interphase(patch->getPatchData(d_interphase_id), SAMRAI_SHARED_PTR_CAST_TAG);" + NL
                + SAMRAIUtils.createFOVDeclaration(pi.getRegionIds(), IND + IND + IND, "patch->")
                + lsNormalDeclaration(pi, IND + IND + IND, false, true)
                + IND + IND + IND + "int* soporte = ((pdat::CellData<int> *) patch->getPatchData(d_soporte_id).get())->getPointer();"
                + NL + NL
                + lsDeclaration(pi, IND + IND + IND)
                + IND + IND + IND + "const hier::Index boxfirst = patch->getBox().lower();" + NL
                + IND + IND + IND + "const hier::Index boxlast  = patch->getBox().upper();" + NL
                + IND + IND + IND + "const hier::Index boxfirst1 = interphase->getGhostBox().lower();" + NL
                + IND + IND + IND + "const hier::Index boxlast1  = interphase->getGhostBox().upper();" + NL + NL
                + IND + IND + IND + "//Get delta spaces into an array. dx, dy, dz." + NL
                + IND + IND + IND + "const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));" + NL
                + IND + IND + IND + "const double* dx  = patch_geom->getDx();" + NL + NL
                + SAMRAIUtils.getLasts(pi, IND + IND + IND, true) + NL + NL
                + checkPosition(pi, IND + IND + IND)
                + loopInit(pi.getCoordinates(), IND + IND + IND, true)
                + indent + "if (vector(soporte, " + indexBox + ") == 1) {" + NL
                + indent + IND + "hier::Index idx(" + index + ");" + NL
                + lsInit
                + indent + IND + "double kernacc= 0;" + NL
                + loopInitNested(pi.getCoordinates(), indent + IND, 1, "")
                + indent2 + IND + "if (vector(soporte, " + indexBox1 + ") == 1) {" + NL
                + indent2 + IND + IND + "hier::Index idx1(" + index1 + ");" + NL
                + indent2 + IND + IND + "Interphase* sop = interphase->getItem(idx1);" + NL
                + loopInitPart(pi.getCoordinates(), indent2 + IND + IND, false, false, "")
                + dists
                + indent3 + IND + IND + "if (distNorm < 2.5) {" + NL
                + indent3 + IND + IND + IND + "double kern;" + NL
                + indent3 + IND + IND + IND + "if (distNorm < 0.5) {" + NL
                + indent3 + IND + IND + IND + IND + "kern = distNorm * distNorm * distNorm * distNorm / 4.0 - 0.625 * distNorm *"
                + " distNorm + 115.0 / 192;" + NL
                + indent3 + IND + IND + IND + "} else {" + NL
                + indent3 + IND + IND + IND + IND + "if (distNorm < 1.5) {" + NL
                + indent3 + IND + IND + IND + IND + IND + "kern = - distNorm * distNorm * distNorm * distNorm / 6 + 5 * distNorm * distNorm "
                + "* distNorm / 6 - 5 * distNorm * distNorm / 4 + 5.0 * distNorm / 24.0 + 55.0 / 96;" + NL
                + indent3 + IND + IND + IND + IND + "} else {" + NL
                + indent3 + IND + IND + IND + IND + IND + "kern = (2.5 - distNorm) * (2.5 - distNorm) * (2.5 - distNorm) * (2.5 - "
                + "distNorm) / 24;" + NL
                + indent3 + IND + IND + IND + IND + "}" + NL
                + indent3 + IND + IND + IND + "}" + NL
                + indent3 + IND + IND + IND + "kernacc = kernacc + kern;" + NL
                + lsAssign          
                + indent3 + IND + IND + "}" + NL
                + loopEnd(pi.getDimensions(), indent2 + IND + IND)
                + indent2 + IND + "}" + NL
                + loopEnd(pi.getDimensions(), indent + IND)
                + lsKernel
                + indent + "}" + NL
                + loopEnd(pi.getDimensions(), IND + IND + IND)
                + IND + IND + "}" + NL
                + IND + IND + "d_mapping_fill->createSchedule(level, level)->fillData(time, true);" + NL + NL;
    }
    
    /**
     * Initialize the rm auxiliary vars.
     * @param coords    The coordinates
     * @param segIds    The region ids
     * @return          The code
     */
    public static String initializeRmVars(ArrayList<String> coords, ArrayList<String> segIds) {
        String indent = IND + IND + IND;
        String rmInit = "";
        String rmAssign = "";
        String ast = "";
        for (int i = 0; i < coords.size(); i++) {
            ast = ast + "*";
        }
        for (int i = 0; i < coords.size(); i++) {
            String coord = coords.get(i);
            rmInit = rmInit + indent + "double " + ast + "position" + coord + "_rm;" + NL
                + indent + "position" + coord + "_rm = (double " + ast + ") malloc((" + coords.get(0) + "last + 2) * 3 * sizeof(double " 
                + ast.substring(1) + "));" + NL;
        }
        for (int i = 0; i < segIds.size(); i++) {
            String id = segIds.get(i);
            rmInit = rmInit + indent + "double " + ast + "ls_" + id + "_rm;" + NL
                + indent + "ls_" + id + "_rm = (double " + ast + ") malloc((" + coords.get(0) + "last + 2) * 3 * sizeof(double " 
                + ast.substring(1) + "));" + NL;
        }
        
        String indexP = "";
        String index = "";
        String indexBox = "";
        for (int i = 0; i < coords.size(); i++) {
            String coord = coords.get(i);
            indexP = indexP + "[" + coord + "]";
            index = index + coord + ", ";
            indexBox = indexBox + coord + " + boxfirst1(" + i + "), ";
            rmInit = rmInit + indent + "for(int " + coord + " = 0; " + coord + " < (" + coord + "last + 2) * 3; " + coord + "++) {" + NL;
            rmAssign = rmAssign + indent + "for(int " + coord + " = 0; " + coord + " < " + coord + "last; " + coord + "++) {" + NL;
            indent = indent + IND;
            for (int j = 0; j < coords.size(); j++) {
                String coord2 = coords.get(j);
                if (i < coords.size() - 1) {
                    String coordNext = coords.get(i + 1);
                    rmInit = rmInit + indent + "position" + coord2 + "_rm" + indexP + " = (double " + ast.substring(i + 1) + ") malloc((" 
                            + coordNext + "last + 2) * 3 * sizeof(double " + ast.substring(i + 2) + "));" + NL;
                }
                else {
                    rmInit = rmInit + indent + "position" + coord2 + "_rm" + indexP + " = 9999;" + NL;
                }
            }
            for (int j = 0; j < segIds.size(); j++) {
                String id = segIds.get(j);
                if (i < coords.size() - 1) {
                    String coordNext = coords.get(i + 1);
                    rmInit = rmInit + indent + "ls_" + id + "_rm" + indexP + " = (double " + ast.substring(i + 1) + ") malloc((" 
                            + coordNext + "last + 2) * 3 * sizeof(double " + ast.substring(i + 2) + "));" + NL;
                }
                else {
                    rmInit = rmInit + indent + "ls_" + id + "_rm" + indexP + " = 9999;" + NL;
                }
            }
        }
        index = index.substring(0, index.lastIndexOf(","));
        indexBox = indexBox.substring(0, indexBox.lastIndexOf(","));
        
        rmAssign = rmAssign + indent + "if (vector(soporte, " + index + ") == 1) {" + NL
                + indent + IND + "hier::Index idx(" + indexBox + ");" + NL
                + indent + IND + "Interphase* sop = interphase->getItem(idx);" + NL;
        //Generate all the combinations for the particles
        ArrayList<String> elements = new ArrayList<String>();
        for (int i = 0; i < INTERPHASELENGTH; i++) {
            elements.add(String.valueOf(i));
        }
        ArrayList<String> combinations = CodeGeneratorUtils.createCombinations(coords.size(), elements);
        for (int i = 0; i < combinations.size(); i++) {
            String comb = combinations.get(i);
            String pIndex2 = "";
            String aIndex = "";
            for (int j = 0; j < comb.length(); j++) {
                pIndex2 = pIndex2 + comb.substring(j, j + 1) + ", ";
                aIndex = aIndex + "[(" + coords.get(j) + " + 1) * 3 + " + comb.substring(j, j + 1) + "]";
            }
            pIndex2 = pIndex2.substring(0, pIndex2.lastIndexOf(","));
            for (int j = 0; j < coords.size(); j++) {
                String coord = coords.get(j);
                rmAssign = rmAssign + indent + IND + "position" + coord + "_rm" + aIndex + " = vectorP_r(position" + coord + ", sop, " 
                        + pIndex2 + ");" + NL;
            }
            for (int j = 0; j < segIds.size(); j++) {
                String id = segIds.get(j);
                rmAssign = rmAssign + indent + IND + "ls_" + id + "_rm" + aIndex + " = vectorP(ls_" + id + ", " + index + ", " + pIndex2 + ");" + NL;
            }
        }
        rmAssign = rmAssign + indent + "}" + NL;
        
        for (int i = 0; i < coords.size(); i++) {
            indent = indent.substring(1);
            rmInit = rmInit + indent + "}" + NL;
            rmAssign = rmAssign + indent + "}" + NL;
        }

        return rmInit + NL + rmAssign + NL;        
    }
    
    /**
     * Generates the combinations for ls assignments.
     * @param ind       The current indent
     * @param dims      The dimensions
     * @param segId     The region id
     * @return          The code
     */
    private static String lsPartAssignment(String ind, int dims, String segId) {
        String lsVars = "";
        ArrayList<String> elements = new ArrayList<String>();
        elements.add("p0");
        for (int i = 0; i < INTERPHASELENGTH; i++) {
            elements.add("m" + String.valueOf(i + 1));
            elements.add("p" + String.valueOf(i + 1));
        }
        ArrayList<String> combinations = CodeGeneratorUtils.createCombinations(dims, elements);  
        for (int i = 0; i < combinations.size(); i++) {
            String comb = combinations.get(i);
            String index = "";
            boolean corner = true;
            boolean center = true;
            for (int k = 0; k < comb.length() / 2; k++) {
                String op = comb.substring(k * 2, k * 2 + 1);
                String val = comb.substring(k * 2 + 1, k * 2 + 2);
                if (op.equals("m")) {
                    op = " - ";
                }
                else {
                    op = " + ";
                    if (val.equals("0")) {
                        op = "";
                        val = "";
                    }
                }
                if (!val.equals(String.valueOf(INTERPHASELENGTH))) {
                    corner = false;
                }
                if (!val.equals("")) {
                    center = false;
                }
                index = index + "[index" + (k + 1) + op + val + "]";
            }
            if (!corner && !center) {
                lsVars = lsVars + ind + "lsP" + comb + " = ls_" + segId + "_rm" + index + ";" + NL;
            }
        }
        return lsVars;
    }
    
    /**
     * Creates the summation of all the terms to calculate a normal component in a particle.
     * @param dims      The dimensions
     * @param coord     The continuous coordinate
     * @param indent    The current indent
     * @return          The code
     */
    private static String createNormalSum(int dims, String coord, String indent) {
        String lsVars = "";
        ArrayList<String> elements = new ArrayList<String>();
        elements.add("p0");
        for (int i = 0; i < INTERPHASELENGTH; i++) {
            elements.add("m" + String.valueOf(i + 1));
            elements.add("p" + String.valueOf(i + 1));
        }
        ArrayList<String> combinations = CodeGeneratorUtils.createCombinations(dims, elements);  
        for (int i = 0; i < combinations.size(); i++) {
            String comb = combinations.get(i);
            String index = "";
            boolean corner = true;
            boolean center = true;
            for (int k = 0; k < comb.length() / 2; k++) {
                String op = comb.substring(k * 2, k * 2 + 1);
                String val = comb.substring(k * 2 + 1, k * 2 + 2);
                if (op.equals("m")) {
                    op = " - ";
                }
                else {
                    op = " + ";
                    if (val.equals("0")) {
                        op = "";
                        val = "";
                    }
                }
                if (!val.equals(String.valueOf(INTERPHASELENGTH))) {
                    corner = false;
                }
                if (!val.equals("")) {
                    center = false;
                }
                index = index + "[index" + (k + 1) + op + val + "]";
            }
            if (!corner && !center) {
                lsVars = lsVars + indent + "(lsP" + comb + " - ls) * dkern" + comb + " * dist" + coord + comb + " / dist" + comb + " + " + NL;
            }
        }
        return lsVars.substring(0, lsVars.lastIndexOf(" +"));
    }
    
    /**
     * Creates the fov calculation.
     * @param ind           The current indent
     * @param coords        The coordinates
     * @param ids           The list of problem ids
     * @param doc           The problem
     * @return              The code
     * @throws CGException  CGException - CG00X External error 
     */
    public static String fovCalculation(String ind, ArrayList<String> coords, ArrayList<String> ids, Document doc) throws CGException {
        String fov = "";
        String indexP = "";
        String indexBox = "";
        if (coords.size() == 2) {
            String coord1 = coords.get(0);
            String contCoord1 = CodeGeneratorUtils.discToCont(doc, coord1);
            String coord2 = coords.get(1);
            String contCoord2 = CodeGeneratorUtils.discToCont(doc, coord2);
            fov = fov + ind + "double " + contCoord1 + " = max<double>(-1e4, min<double>(1e4, (1 - par1) + max<double>(1e-10, fabs(par2)) " 
                    + "/ max<double>(1e-10, fabs(par1)) * (1 - par2)));" + NL
                    + ind + "double " + contCoord2 + " = max<double>(-1e4, min<double>(1e4, (1 - par2) + max<double>(1e-10, fabs(par1)) / " 
                    + "max<double>(1e-10, fabs(par2)) * (1 - par1)));" +  NL
                    + ind + "double fov = (((" + contCoord1 + " * " + contCoord2 + " / 2.0 * (1 - pow(1 - max<double>(0, min<double>(1, 2.0 / " 
                    + contCoord1 + ")), 2) - pow(1 - max<double>(0, min<double>(1, 2.0 / " + contCoord2 
                    + ")), 2) + pow(1 - max<double>(0, min<double>(1, 2.0 / " 
                    + contCoord1 + " + 2.0 / " + contCoord2 + ")), 2))) / 4.0));" + NL;
            indexP = "p1, p2";
            indexBox = coord1 + " - boxfirst1(0), " + coord2 + "  - boxfirst1(1)";
        }
        if (coords.size() == THREE) {
            String coord1 = coords.get(0);
            String contCoord1 = CodeGeneratorUtils.discToCont(doc, coord1);
            String coord2 = coords.get(1);
            String contCoord2 = CodeGeneratorUtils.discToCont(doc, coord2);
            String coord3 = coords.get(2);
            String contCoord3 = CodeGeneratorUtils.discToCont(doc, coord3);
            fov = fov + ind + "double " + contCoord1 + " = max<double>(-1e4, min<double>(1e4, (1 - par1) + max<double>(1e-10, fabs(par2)) " 
                    + "/ max<double>(1e-10, fabs(par1)) * (1 - par2) + max<double>(1e-10, fabs(par3)) / max<double>(1e-10, fabs(par1)) *"
                    + " (1 - par3)));" + NL
                    + ind + "double " + contCoord2 + " = max<double>(-1e4, min<double>(1e4, (1 - par2) + max<double>(1e-10, fabs(par1))"
                    + " / max<double>(1e-10, fabs(par2)) * (1 - par1) + max<double>(1e-10, fabs(par3)) / max<double>(1e-10, fabs(par2))"
                    + " * (1 - par3)));" +  NL
                    + ind + "double " + contCoord3 + " = max<double>(-1e4, min<double>(1e4, (1 - par3) + max<double>(1e-10, fabs(par1))"
                    + " / max<double>(1e-10, fabs(par3)) * (1 - par1) + max<double>(1e-10, fabs(par2)) / max<double>(1e-10, fabs(par3))"
                    + " * (1 - par2)));" +  NL
                    + ind + "double fov = (((" + contCoord1 + " * " + contCoord2 + " * " + contCoord3 + " / 6.0 * (1 - pow(1 - "
                    + "max<double>(0, min<double>(1, 2.0 / " + contCoord1 + ")), 3) - pow(1 - max<double>(0, min<double>(1, 2.0 / " + contCoord2
                    + ")), 3) -" 
                    + " pow(1 - max<double>(0, min<double>(1, 2.0 / " + contCoord3 + ")), 3) + pow(1 - max<double>(0, min<double>(1, 2.0 / " 
                    + contCoord1 + " + 2.0 / "
                    + contCoord2 + ")), 3) + pow(1 - max<double>(0, min<double>(1, 2.0 / " + contCoord1 + " + 2.0 / " + contCoord3 + ")), 3) + pow(1 "
                    + "- max<double>(0, min<double>(1, 2.0 / " + contCoord2 + " + 2.0 / " + contCoord3 + ")), 3) - pow(1 - max<double>(0, "
                    + "min<double>(1, 2.0 / " + contCoord1 + " + 2.0 / " + contCoord2 + " + 2.0 / " + contCoord3 + ")), 3))) / 8.0));" + NL;
            indexP = "p1, p2, p3";
            indexBox = coord1 + " - boxfirst1(0), " + coord2 + "  - boxfirst1(1), " + coord3 + "  - boxfirst1(2)";
        }
        for (int i = 0; i < ids.size(); i++) {
            String segId = ids.get(i);
            fov = fov + ind + "if (vectorP_r(seg, sop, " + indexP + ") == " + segId + ") {" + NL
                    + ind + IND + "vector(FOV_" + segId + ", " + indexBox + ") = vector(FOV_" + segId + ", " + indexBox + ") + round(100*fov);" + NL
                    + ind + "}" + NL
                    + ind + "if (vectorP_r(int_seg, sop, " + indexP + ") == " + segId + ") {" + NL
                    + ind + IND + "vector(FOV_" + segId + ", " + indexBox + ") = vector(FOV_" + segId + ", " + indexBox 
                    + ") + 100 - round(100*fov);" + NL
                    + ind + "}" + NL;
        }
        return fov;
    }
    
    /**
     * Creates the code that sets the seg/int_seg data, normal in particles and FOV.
     * @param pi            The problem info
     * @return              The code
     * @throws CGException  CGException - CG00X External error
     */
    public static String normalPartIntSeg(ProblemInfo pi) throws CGException {
        String indent = IND + IND + IND;
        String indent2 = IND + IND + IND;
        String indexBox = "";
        String index = "";
        String indexP = "";
        String indexP0 = "";
        for (int i = 0; i < pi.getDimensions(); i++) {
            String coord = pi.getCoordinates().get(i);
            indent = indent + IND;
            indent2 = indent2 + IND + IND;
            indexBox = indexBox + coord + " - boxfirst1(" + i + "), ";
            index = index + coord + ", ";
            indexP = indexP + "p" + (i + 1) + ", ";
            indexP0 = indexP0 + "0, ";

        }
        indexBox = indexBox.substring(0, indexBox.lastIndexOf(","));
        index = index.substring(0, index.lastIndexOf(","));
        indexP = indexP.substring(0, indexP.lastIndexOf(","));
        indexP0 = indexP0.substring(0, indexP0.lastIndexOf(","));
        
        String removeCondition = "";
        String segIntSegAssignment = "";
        String sopLS = "";
        String fovInit = "";
        String lsAssig = "";
        String fovNormalization = "";
        String fovNormalization2 = "";
        String allRegions = "";
        ArrayList<String> regionsOrdered = new ArrayList<String>();
        //The moved regions at the beginning
        regionsOrdered.addAll(pi.getMovementInfo().getMovRegions());
        //The rest at the end
        ArrayList<String> rest = new ArrayList<String>();
        rest.addAll(pi.getRegions());
        rest.removeAll(pi.getMovementInfo().getMovRegions());
        regionsOrdered.addAll(rest);
        for (int i = 0; i < regionsOrdered.size(); i++) {
            String segName = regionsOrdered.get(i); 
            String segId = CodeGeneratorUtils.getRegionId(pi.getProblem(), pi.getRegions(), segName);
            if (i == 0) {
                String nextSegName = regionsOrdered.get(1); 
                String nextSegId = CodeGeneratorUtils.getRegionId(pi.getProblem(), pi.getRegions(), nextSegName);
                if (!pi.getMovementInfo().getMovRegions().contains(nextSegName)) {
                    segIntSegAssignment = indent2 + IND + IND + "double seg_val, int_seg_val;" + NL
                            + indent2 + IND + IND + "vectorP(ls_" + nextSegId + ", " + indexBox + ", " + indexP + ") = -vectorP(ls_" + segId + ", " 
                            + indexBox + ", " + indexP + ");" + NL
                            + indent2 + IND + IND + "if (min<double>(vectorP(ls_" + segId + ", " + indexBox + ", " + indexP + "), vectorP(ls_" 
                            + nextSegId + ", "
                            + indexBox + ", " + indexP + ")) == vectorP(ls_" + segId + ", " + indexBox + ", " + indexP + ")) {" + NL 
                            + indent2 + IND + IND + IND + "vectorP_r(seg, sop, " + indexP + ") = " + segId + ";" + NL
                            + indent2 + IND + IND + IND + "vectorP_r(int_seg, sop, " + indexP + ") = " + nextSegId + ";" + NL
                            + indent2 + IND + IND + IND + "seg_val = vectorP(ls_" + segId + ", " + indexBox + ", " + indexP + ");" + NL
                            + indent2 + IND + IND + IND + "int_seg_val = vectorP(ls_" + nextSegId + ", " + indexBox + ", " + indexP + ");" + NL
                            + indent2 + IND + IND + "} else {" + NL
                            + indent2 + IND + IND + IND + "vectorP_r(seg, sop, " + indexP + ") = " + nextSegId + ";" + NL
                            + indent2 + IND + IND + IND + "vectorP_r(int_seg, sop, " + indexP + ") = " + segId + ";" + NL
                            + indent2 + IND + IND + IND + "seg_val = vectorP(ls_" + nextSegId + ", " + indexBox + ", " + indexP + ");" + NL
                            + indent2 + IND + IND + IND + "int_seg_val = vectorP(ls_" + segId + ", " + indexBox + ", " + indexP + ");" + NL
                            + indent2 + IND + IND + "}" + NL;
                }
                else {
                    segIntSegAssignment = indent2 + IND + IND + "double seg_val, int_seg_val;" + NL
                            + indent2 + IND + IND + "if (min<double>(vectorP(ls_" + segId + ", " + indexBox + ", " + indexP + "), vectorP(ls_" 
                            + nextSegId + ", " + indexBox + ", " + indexP + ")) == vectorP(ls_" + segId + ", " + indexBox + ", " + indexP + ")) {"
                            + NL 
                            + indent2 + IND + IND + IND + "vectorP_r(seg, sop, " + indexP + ") = " + segId + ";" + NL
                            + indent2 + IND + IND + IND + "vectorP_r(int_seg, sop, " + indexP + ") = " + nextSegId + ";" + NL
                            + indent2 + IND + IND + IND + "seg_val = vectorP(ls_" + segId + ", " + indexBox + ", " + indexP + ");" + NL
                            + indent2 + IND + IND + IND + "int_seg_val = vectorP(ls_" + nextSegId + ", " + indexBox + ", " + indexP + ");" + NL
                            + indent2 + IND + IND + "} else {" + NL
                            + indent2 + IND + IND + IND + "vectorP_r(seg, sop, " + indexP + ") = " + nextSegId + ";" + NL
                            + indent2 + IND + IND + IND + "vectorP_r(int_seg, sop, " + indexP + ") = " + segId + ";" + NL
                            + indent2 + IND + IND + IND + "seg_val = vectorP(ls_" + nextSegId + ", " + indexBox + ", " + indexP + ");" + NL
                            + indent2 + IND + IND + IND + "int_seg_val = vectorP(ls_" + segId + ", " + indexBox + ", " + indexP + ");" + NL
                            + indent2 + IND + IND + "}" + NL;
                }

            }
            if (i > 1) {
                if (pi.getMovementInfo().getMovRegions().contains(segName)) {
                    segIntSegAssignment = segIntSegAssignment + indent2 + IND + IND + "if (min<double>(vectorP(ls_" + segId + ", " + indexBox + ", "
                            + indexP + "), seg_val) == vectorP(ls_" + segId + ", " + indexBox + ", " + indexP + ")) {" + NL
                            + indent2 + IND + IND + IND + "vectorP_r(int_seg, sop, " + indexP + ") = vectorP_r(seg, sop, " + indexP + ");" + NL
                            + indent2 + IND + IND + IND + "vectorP_r(seg, sop, " + indexP + ") = " + segId + ";" + NL
                            + indent2 + IND + IND + IND + "int_seg_val = seg_val;" + NL
                            + indent2 + IND + IND + IND + "seg_val = vectorP(ls_" + segId + ", " + indexBox + ", " + indexP + ");" + NL
                            + indent2 + IND + IND + "} else {" + NL
                            + indent2 + IND + IND + IND + "if (min<double>(vectorP(ls_" + segId + ", " + indexBox + ", " + indexP 
                            + "), int_seg_val) == vectorP(ls_" + segId + ", " + indexBox + ", " + indexP + ")) {" + NL
                            + indent2 + IND + IND + IND + IND + "vectorP_r(int_seg, sop, " + indexP + ") = " + segId + ";" + NL
                            + indent2 + IND + IND + IND + IND + "int_seg_val = vectorP(ls_" + segId + ", " + indexBox + ", " + indexP + ");" + NL
                            + indent2 + IND + IND + IND + "}" + NL
                            + indent2 + IND + IND + "}" + NL;
                }
                else {
                    segIntSegAssignment = segIntSegAssignment 
                            + indent2 + IND + IND + "if (min<double>(vectorP(ls_" + segId + ", " + indexBox + ", " + indexP 
                            + "), int_seg_val) == vectorP(ls_" + segId + ", " + indexBox + ", " + indexP + ")) {" + NL
                            + indent2 + IND + IND + IND + "vectorP(ls_" + segId + ", " + indexBox + ", " + indexP + ") = -seg_val;" + NL
                            + indent2 + IND + IND + IND + "vectorP_r(int_seg, sop, " + indexP + ") = " + segId + ";" + NL
                            + indent2 + IND + IND + IND + "int_seg_val = vectorP(ls_" + segId + ", " + indexBox + ", " + indexP + ");" + NL
                            + indent2 + IND + IND + "}" + NL;
                }

            }
            
        }
        
        ArrayList<String> segIds = new ArrayList<String>();
        ArrayList<String> tmpVars = new ArrayList<String>();
        for (int i = 0; i < pi.getRegions().size(); i++) {
            allRegions = allRegions + "tmp" + i + " + ";
        }
        allRegions = allRegions.substring(0, allRegions.lastIndexOf(" +"));
        String points = "27";
        if (pi.getDimensions() == 2) {
            points = "9";
        }
        for (int i = 0; i < pi.getRegions().size(); i++) {
            String segName = pi.getRegions().get(i); 
            String segId = CodeGeneratorUtils.getRegionId(pi.getProblem(), pi.getRegions(), segName);
            segIds.add(segId);
            if (pi.getMovementInfo().getMovRegions().contains(segName)) {
                removeCondition = removeCondition + "vectorP(ls_" + segId + ", " + indexBox + ", " + indexP0 + ") > 9999999998 && ";
            }
            sopLS = sopLS + indent2 + IND  + IND + "if (vectorP_r(seg, sop, " + indexP + ") == " + segId + ") {" + NL
                    +  indent2 + IND + IND + IND + "ls = vectorP(ls_" + segId + ", " + indexBox + ", " + indexP + ");" + NL
                    + lsPartAssignment(indent2 + IND + IND + IND, pi.getDimensions(), segId)
                    + indent2 + IND + IND + "}"  + NL;
            fovInit = fovInit + indent + IND + "vector(FOV_" + segId + ", " + indexBox + ") = 0;" + NL;
            lsAssig = lsAssig + indent2 + IND + "if (vectorP_r(seg, sop, " + indexP + ") == " + segId + ") {" + NL
                    + indent2 + IND + IND + "ls = vectorP(ls_" + segId + ", " + indexBox + ", " + indexP + ");" + NL
                    + indent2 + IND + "}" + NL;
            fovNormalization = fovNormalization + indent + IND + "double tmp" + i + " = vector(FOV_" + segId + ", " + indexBox + ") / " + points 
                    + ".0;" + NL;
            fovNormalization2 = fovNormalization2 + indent + IND + "vector(FOV_" + segId + ", " + indexBox + ") = round(tmp" + i 
                    + ") + 100 - round(" + allRegions + ");" + NL
                    + indent + IND + "tmp" + i + " = vector(FOV_" + segId + ", " + indexBox + ");" + NL;
            tmpVars.add("ls_" + segId + "_rm");
        }
        removeCondition = removeCondition.substring(0, removeCondition.lastIndexOf(" &&"));
        
        String sopCond = "";
        ArrayList<String> elements = new ArrayList<String>();
        for (int i = 0; i < INTERPHASELENGTH; i++) {
            elements.add(String.valueOf(i));
        }
        ArrayList<String> combinations = CodeGeneratorUtils.createCombinations(pi.getDimensions(), elements);  
        for (int i = 0; i < combinations.size(); i++) {
            String comb = combinations.get(i);
            String pIndex2 = "";
            for (int k = 0; k < comb.length(); k++) {
                int val = Integer.valueOf(comb.substring(k, k + 1)) - INTERPHASELENGTH / 2;
                if (val < 0) {
                    pIndex2 = pIndex2 + pi.getCoordinates().get(k) + " " + val  + " - boxfirst1(" + k + "), ";
                }
                if (val == 0) {
                    pIndex2 = pIndex2 + pi.getCoordinates().get(k) + " - boxfirst1(" + k + "), ";
                }
                if (val > 0) {
                    pIndex2 = pIndex2 + pi.getCoordinates().get(k) + " + " + val  + " - boxfirst1(" + k + "), ";
                }
            }
            pIndex2 = pIndex2.substring(0, pIndex2.lastIndexOf(","));
            sopCond = sopCond + "vector(soporte, " + pIndex2 + ") == 0 || ";
        }
        sopCond = sopCond.substring(0, sopCond.lastIndexOf(" ||"));
        
        String distVars = "";
        String lsVars = indent2 + IND + IND + "double ls;" + NL;
        elements.clear();
        elements.add("p0");
        for (int i = 0; i < INTERPHASELENGTH; i++) {
            elements.add("m" + String.valueOf(i + 1));
            elements.add("p" + String.valueOf(i + 1));
        }
        combinations = CodeGeneratorUtils.createCombinations(pi.getDimensions(), elements);  
        for (int i = 0; i < combinations.size(); i++) {
            String comb = combinations.get(i);
            String index2 = "";
            String index3 = "";
            boolean corner = true;
            boolean center = true;
            for (int k = 0; k < comb.length() / 2; k++) {
                String op = comb.substring(k * 2, k * 2 + 1);
                String val = comb.substring(k * 2 + 1, k * 2 + 2);
                if (op.equals("m")) {
                    op = " - ";
                }
                else {
                    op = " + ";
                    if (val.equals("0")) {
                        op = "";
                        val = "";
                    }
                }
                if (!val.equals(String.valueOf(INTERPHASELENGTH))) {
                    corner = false;
                }
                if (!val.equals("")) {
                    center = false;
                }
                index2 = index2 + "[index" + (k + 1) + "]";
                index3 = index3 + "[index" + (k + 1) + op + val + "]";
            }
            if (!corner && !center) {
                String prod = "";
                for (int j = 0; j < pi.getDimensions(); j++) {
                    String coord = pi.getCoordinates().get(j);
                    String contCoord = CodeGeneratorUtils.discToCont(pi.getProblem(), coord);
                    distVars = distVars + indent2 + IND + IND + "double dist" + contCoord + comb + " = position" + coord + "_rm"
                            + index2 + " - position" + coord + "_rm" + index3 + ";" + NL;
                    prod = prod + "dist" + contCoord + comb + " * dist" + contCoord + comb + " + ";
                }
                prod = prod.substring(0, prod.lastIndexOf(" +"));
                distVars = distVars + indent2 + IND + IND + "double dist" + comb + " = sqrt(" + prod + ");" + NL
                        + indent2 + IND + IND + "double dkern" + comb + " = dkern(dist" + comb + " / (dx[0] / 3));" + NL;
                lsVars = lsVars + indent2 + IND + IND + "double lsP" + comb + ";" + NL;
            }
        }
        String normalAssig = "";
        String modNormal = "";
        String normalMod = "";
        String normalNull = "";
        String fovPars = "";
        for (int i = 0; i < pi.getDimensions(); i++) {
            String coord = pi.getCoordinates().get(i);
            String contCoord = CodeGeneratorUtils.discToCont(pi.getProblem(), coord);
            normalAssig = normalAssig + indent2 + IND + IND + "vectorP_r(normal" + coord + ", sop, " + indexP + ") = "
                    + createNormalSum(pi.getDimensions(), contCoord, indent2 + IND + IND + IND) + ";" + NL;
            modNormal = modNormal + "vectorP_r(normal" + coord + ", sop, " + indexP + ") * vectorP_r(normal" + coord + ", sop, " + indexP + ") + ";
            normalMod = normalMod + indent2 + IND + IND + "vectorP_r(normal" + coord + ", sop, " + indexP + ") = vectorP_r(normal" 
                    + coord + ", sop, " + indexP + ") / mod;" + NL;
            normalNull = normalNull + indent2 + IND + IND + "vectorP_r(normal" + coord + ", sop, " + indexP + ") = 1;" + NL;
            fovPars = fovPars + indent2 + IND + "double par" + (i + 1) + " = (2 * (ls) * fabs(vectorP_r(normal" + coord + ", sop, " 
                    + indexP + ")) / (dx[" + i + "] / 3));" + NL;
            tmpVars.add("position" + coord + "_rm");
        }
        modNormal = modNormal.substring(0, modNormal.lastIndexOf(" +"));
        
        return IND + IND + "//Normal in particles + seg / int_seg + FOV in cells calculations" + NL
                + IND + IND + "for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {" + NL
                + IND + IND + IND + "const std::shared_ptr<hier::Patch >& patch = *p_it;" + NL
                + IND + IND + IND + "std::shared_ptr< pdat::IndexData<Interphase, pdat::CellGeometry > > " 
                + "interphase(patch->getPatchData(d_interphase_id), SAMRAI_SHARED_PTR_CAST_TAG);" + NL
                + SAMRAIUtils.createFOVDeclaration(pi.getRegionIds(), IND + IND + IND, "patch->")
                + lsDeclaration(pi, IND + IND + IND)
                + IND + IND + IND + "int* soporte = ((pdat::CellData<int> *) patch->getPatchData(d_soporte_id).get())->getPointer();" + NL + NL
                + IND + IND + IND + "const hier::Index boxfirst = patch->getBox().lower();" + NL
                + IND + IND + IND + "const hier::Index boxlast  = patch->getBox().upper();" + NL
                + IND + IND + IND + "const hier::Index boxfirst1 = interphase->getGhostBox().lower();" + NL
                + IND + IND + IND + "const hier::Index boxlast1  = interphase->getGhostBox().upper();" + NL + NL
                + IND + IND + IND + "//Get delta spaces into an array. dx, dy, dz." + NL
                + IND + IND + IND + "const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));" + NL
                + IND + IND + IND + "const double* dx  = patch_geom->getDx();" + NL + NL
                + SAMRAIUtils.getLasts(pi, IND + IND + IND, true) + NL + NL
                + IND + IND + IND + "//seg/int_seg" + NL
                + loopInit(pi.getCoordinates(), IND + IND + IND, false)
                + indent + "if (vector(soporte, " + indexBox + ") == 1) {" + NL
                + indent + IND + "hier::Index idx(" + index + ");" + NL
                + indent + IND + "Interphase* sop = interphase->getItem(idx);" + NL
                + indent + IND + "if (" + removeCondition + ") {" + NL
                + indent + IND + IND + "interphase->removeItem(idx);" + NL
                + indent + IND + IND + "vector(soporte, " + indexBox + ") = 0;" + NL
                + indent + IND + "} else {" + NL
                + loopInitPart(pi.getCoordinates(), indent + IND + IND, false, false, "")
                + segIntSegAssignment
                + loopEnd(pi.getDimensions(), indent + IND + IND)
                + indent + IND + "}" + NL
                + indent + "}" + NL
                + loopEnd(pi.getDimensions(), IND + IND + IND) + NL
                + IND + IND + IND + "//Normal in particles and FOV" + NL
                + IND + IND + IND + "//Check position" + NL
                + checkPosition(pi,  IND + IND + IND)
                + IND + IND + IND + "//Normal in particles" + NL
                + initializeRmVars(pi.getCoordinates(), segIds)
                + loopInit(pi.getCoordinates(), IND + IND + IND, true)
                + indent + "if (vector(soporte, " + indexBox + ") == 1) {" + NL
                + fovInit
                + indent + IND + "hier::Index idx(" + index + ");" + NL
                + indent + IND + "Interphase* sop = interphase->getItem(idx);" + NL    
                + indent + IND + "bool null = false;" + NL
                + indent + IND + "if (" + sopCond + ") {" + NL
                + indent + IND + IND + "null = true;" + NL
                + indent + IND + "}" + NL
                + loopInitPart(pi.getCoordinates(), indent + IND, true, false, "")
                + indent2 + IND + "if (!null) {" + NL
                + distVars
                + lsVars
                + sopLS
                + normalAssig
                + indent2 + IND + IND + "double mod = sqrt(" + modNormal + ");" + NL
                + normalMod
                + indent2 + IND + "} else {" + NL
                + normalNull
                + indent2 + IND + "}" + NL
                + indent2 + IND + "//FOV" + NL
                + indent2 + IND + "double ls;" + NL
                + lsAssig
                + fovPars
                + fovCalculation(indent2 + IND, pi.getCoordinates(), segIds, pi.getProblem())
                + loopEnd(pi.getDimensions(), indent + IND) + NL
                + fovNormalization
                + fovNormalization2
                + indent + "}" + NL
                + loopEnd(pi.getDimensions(), IND + IND + IND) + NL
                + tmpVarsFree(pi, tmpVars, IND + IND + IND, true)
                + IND + IND + "}" + NL
                + IND + IND + "d_mapping_fill->createSchedule(level, level)->fillData(time, true);" + NL;
    }

    /**
     * Creates the code that sets the normals in the cells.
     * @param pi            The problem info
     * @return              The code
     * @throws CGException  CGException - CG00X External error
     */
    public static String cellNormals(ProblemInfo pi) throws CGException {
        String indent = IND + IND + IND;
        String indent2 = IND + IND + IND;
        String indent3 = IND + IND + IND;
        String indexBox = "";
        String indexBox1 = "";
        String index = "";
        String index1 = "";
        String indexP = "";
        String indexP0 = "";
        for (int i = 0; i < pi.getDimensions(); i++) {
            String coord = pi.getCoordinates().get(i);
            indent = indent + IND;
            indent2 = indent2 + IND + IND;
            indent3 = indent3 + IND + IND + IND;
            indexBox = indexBox + coord + " - boxfirst1(" + i + "), ";
            indexBox1 = indexBox1 + coord + "1 - boxfirst1(" + i + "), ";
            index = index + coord + ", ";
            index1 = index1 + coord + "1, ";
            indexP = indexP + "p" + (i + 1) + ", ";
            indexP0 = indexP0 + "0, ";
        }
        indexBox = indexBox.substring(0, indexBox.lastIndexOf(","));
        indexBox1 = indexBox1.substring(0, indexBox1.lastIndexOf(","));
        index = index.substring(0, index.lastIndexOf(","));
        index1 = index1.substring(0, index1.lastIndexOf(","));
        indexP = indexP.substring(0, indexP.lastIndexOf(","));
        indexP0 = indexP0.substring(0, indexP0.lastIndexOf(","));
                
        String normalInit = "";
        String normalCalc = "";
        String normalMod = "";
        String normalNull = "";
        for (int i = 0; i < pi.getRegions().size() - 1; i++) {
            String segName = pi.getRegions().get(i); 
            String segId = CodeGeneratorUtils.getRegionId(pi.getProblem(), pi.getRegions(), segName);

            for (int j = i + 1; j < pi.getRegions().size(); j++) {
                String segName2 = pi.getRegions().get(j); 
                String segId2 = CodeGeneratorUtils.getRegionId(pi.getProblem(), pi.getRegions(), segName2);
                String dists = "";
                String dist2 = "";
                String normalAcc = "";
                String mod = "";
                for (int k = 0; k < pi.getDimensions(); k++) {
                    String coord = pi.getCoordinates().get(k);
                    mod = mod + "vector(normal" + coord + "_" + segId + segId2 + ", " + indexBox + ")*vector(normal" + coord + "_" 
                            + segId + segId2 + ", " + indexBox + ") + ";
                }
                mod = mod.substring(0, mod.lastIndexOf(" +"));
                normalMod = normalMod + indent + IND + IND + "double mod_" + segId + segId2 + " = sqrt(" + mod + ");" + NL;
                for (int k = 0; k < pi.getDimensions(); k++) {
                    String coord = pi.getCoordinates().get(k);
                    String contCoord = CodeGeneratorUtils.discToCont(pi.getProblem(), coord);
                    normalInit = normalInit + indent + "vector(normal" + coord + "_" + segId + segId2 + ", " + indexBox + ") = 0;" + NL;
                    dists = dists + indent3 + IND + IND + IND + "double dist" + contCoord + " = (" + coord + " + 0.5) * dx[" + k 
                            + "] + d_grid_geometry->getXLower()[" + k + "] - vectorP_r(position" + coord + ", sop, " + indexP + ");" + NL;
                    dist2 = dist2 + "dist" + contCoord + " * dist" + contCoord + " + ";
                    normalAcc = normalAcc + indent3 + IND + IND + IND + IND + "vector(normal" + coord + "_" + segId + segId2 + ", " + indexBox 
                            + ") = vector(normal" + coord + "_" + segId + segId2 + ", " + indexBox + ") + (vectorP(ls_" + segId + ", " + indexBox1
                            + ", " + indexP + ") - vector(LS_" + segId + ", " + indexBox + ")) * kern * dist" + contCoord + " / dist;" + NL;
                    normalMod = normalMod + indent + IND + IND + "vector(normal" + coord + "_" + segId + segId2 + ", " + indexBox 
                            + ") = vector(normal" + coord + "_" + segId + segId2 + ", " + indexBox + ") / mod_" + segId + segId2 + ";" + NL;
                    normalNull = normalNull + indent + IND + IND + "vector(normal" + coord + "_" + segId + segId2 + ", " + indexBox + ") = 0;" + NL;
                }
                dist2 = dist2.substring(0, dist2.lastIndexOf(" +"));
                normalCalc = normalCalc + indent3 + IND + IND + "if ((vectorP_r(seg, sop, " + indexP + ") == " + segId 
                        + " && vectorP_r(int_seg, sop, " + indexP + ") == " + segId2 + ") || (vectorP_r(seg, sop, " + indexP + ") == " 
                        + segId2 + " && vectorP_r(int_seg, sop, " + indexP + ") == " + segId + ")) {" + NL
                        + dists
                        + indent3 + IND + IND + IND + "double dist = " + dist2 + ";" + NL
                        + indent3 + IND + IND + IND + "double distNorm = dist/(dx[0] / 3);" + NL
                        + indent3 + IND + IND + IND + "if (distNorm < 2.5 && distNorm > 0) {" + NL
                        + indent3 + IND + IND + IND + IND + "double kern;" + NL
                        + indent3 + IND + IND + IND + IND + "if (distNorm < 0.5) {" + NL
                        + indent3 + IND + IND + IND + IND + IND + "kern = distNorm * distNorm * distNorm - 1.25 * distNorm;" + NL
                        + indent3 + IND + IND + IND + IND + "} else {" + NL
                        + indent3 + IND + IND + IND + IND + IND + "if (distNorm < 1.5) {" + NL
                        + indent3 + IND + IND + IND + IND + IND + IND + "kern = -2 * distNorm * distNorm * distNorm / 3 + 2.5 * distNorm * distNorm "
                        + "- 2.5 * distNorm + 5.0/24.0;" + NL
                        + indent3 + IND + IND + IND + IND + IND + "} else {" + NL
                        + indent3 + IND + IND + IND + IND + IND + IND + "kern = -(2.5 - distNorm) * (2.5 - distNorm) * (2.5 - distNorm) / 6;" + NL
                        + indent3 + IND + IND + IND + IND + IND + "}" + NL
                        + indent3 + IND + IND + IND + IND + "}" + NL
                        + normalAcc
                        + indent3 + IND + IND + IND + "}" + NL
                        + indent3 + IND + IND + "}" + NL;
            }
        }
        
        return IND + IND + "//Normals in cells" + NL
                + IND + IND + "for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {" + NL
                + IND + IND + IND + "const std::shared_ptr<hier::Patch >& patch = *p_it;" + NL
                + IND + IND + IND + "std::shared_ptr< pdat::IndexData<Interphase, pdat::CellGeometry > > "
                + "interphase(patch->getPatchData(d_interphase_id), SAMRAI_SHARED_PTR_CAST_TAG);" + NL
                + SAMRAIUtils.createFOVDeclaration(pi.getRegionIds(), IND + IND + IND, "patch->")
                + lsNormalDeclaration(pi, IND + IND + IND, false, false)
                + IND + IND + IND + "int* soporte = ((pdat::CellData<int> *) patch->getPatchData(d_soporte_id).get())->getPointer();" + NL + NL
                + lsDeclaration(pi, IND + IND + IND)
                + IND + IND + IND + "const hier::Index boxfirst = patch->getBox().lower();" + NL
                + IND + IND + IND + "const hier::Index boxlast  = patch->getBox().upper();" + NL
                + IND + IND + IND + "const hier::Index boxfirst1 = interphase->getGhostBox().lower();" + NL
                + IND + IND + IND + "const hier::Index boxlast1  = interphase->getGhostBox().upper();" + NL + NL
                + IND + IND + IND + "//Get delta spaces into an array. dx, dy, dz." + NL
                + IND + IND + IND + "const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));" + NL
                + IND + IND + IND + "const double* dx  = patch_geom->getDx();" + NL + NL
                + SAMRAIUtils.getLasts(pi, IND + IND + IND, true) + NL + NL
                + IND + IND + IND + "//Check position" + NL
                + checkPosition(pi,  IND + IND + IND)
                + loopInit(pi.getCoordinates(), IND + IND + IND, true)
                + normalInit
                + indent + "if (vector(soporte, " + indexBox + ") == 1) {" + NL
                + indent + IND + "hier::Index idx(" + index + ");" + NL
                + indent + IND + "bool null = false;" + NL
                + loopInitNested(pi.getCoordinates(), IND + indent, 1, " && !null")
                + indent2 + IND + "if (vector(soporte, " + indexBox1 + ") == 1) {" + NL
                + indent2 + IND + IND + "hier::Index idx1(" + index1 + ");" + NL
                + indent2 + IND + IND + "Interphase* sop = interphase->getItem(idx1);" + NL
                + loopInitPart(pi.getCoordinates(), indent2 + IND + IND, false, false, " && !null")
                + normalCalc
                + loopEnd(pi.getDimensions(), indent2 + IND + IND) + NL
                + indent2 + IND + "} else {" + NL
                + indent2 + IND + IND + "null = true;" + NL
                + indent2 + IND + "}" + NL
                + loopEnd(pi.getDimensions(), IND + indent) + NL
                + indent + IND + "if (!null) {" + NL
                + normalMod
                + indent + IND + "} else {" + NL
                + normalNull
                + indent + IND + "}" + NL
                + indent + "}" + NL
                + loopEnd(pi.getDimensions(), IND + IND + IND) + NL
                +  IND + IND + "}" + NL
                +  IND + IND + "d_mapping_fill->createSchedule(level, level)->fillData(time, true);" + NL;
    }
    
    /**
     * Creates the code to calculate the initial level set.
     * @param problem       The problem
     * @param coords        The coordinates
     * @param regions       The regions
     * @param indent        The current indent
     * @param indexBox1     The index for boxfirst and boxlast
     * @param indexP        The index for the particles
     * @return              The code
     * @throws CGException 
     */
    private static String calculateInitialLS(Document problem, ArrayList<String> coords, ArrayList<String> regions, String indent, 
            String indexBox1, String indexP) throws CGException {
        String ls = "";
        ArrayList<String> elements = new ArrayList<String>();
        elements.add("l");
        elements.add("u");
        for (int i = 0; i < regions.size(); i++) {
            String segName = regions.get(i); 
            String segId = CodeGeneratorUtils.getRegionId(problem, regions, segName);
            for (int j = 0; j < coords.size(); j++) {
                String coord = coords.get(j);
                if (j == coords.size() - 1) {
                    ls = ls + indent + "vectorP(ls_" + segId + ", " + indexBox1 + ", " + indexP + ") = valAux_" + segId 
                            + "_l + (valAux_" + segId + "_u - valAux_"
                            + segId + "_l) / (posAux_" + coord + "_upr-posAux_" + coord + "_lwr) * (vectorP_r(position" + coord + ", inter, " + indexP
                            + ") - posAux_" + coord + "_lwr);" + NL;
                }
                else {
                    ArrayList<String> combinations = CodeGeneratorUtils.createCombinations(coords.size() - 1 - j, elements);
                    for (int k = 0; k < combinations.size(); k++) {
                        String comb = combinations.get(k);
                        ls = ls + indent + "double valAux_" + segId + "_" + comb + " = valAux_" + segId + "_l" + comb + " + (valAux_" + segId + "_u"
                                + comb + " - valAux_" + segId + "_l" + comb + ") / (posAux_" + coord + "_upr-posAux_" + coord + "_lwr) * "
                                + "(vectorP_r(position" + coord + ", inter, " + indexP + ") - posAux_" + coord + "_lwr);" + NL;
                    }
                }
            }
            ls = ls + NL;
        }
        return ls;
    }
    
    /**
     * Creates the code that will do the initial level set mapping.
     * @param pi    The problem info
     * @return      The code
     * @throws CGException 
     */
    public static String initialLSMapping(ProblemInfo pi) throws CGException {
        String indent = IND + IND + IND;
        String indent2 = IND + IND + IND;
        String indent3 = IND + IND + IND;
        String indexBox = "";
        String indexBoxP = "";
        String indexBox1 = "";
        String indexBoxP1 = "";
        String index = "";
        String index1 = "";
        String indexP = "";
        String indexPA = "";
        String notSameIndexCond = "";
        String insideCond = "";
        String insideCond2 = "";
        String boundCond = "";
        String maxDx = "";
        for (int i = 0; i < pi.getDimensions(); i++) {
            String coord = pi.getCoordinates().get(i);
            indent = indent + IND;
            indent2 = indent2 + IND + IND;
            indent3 = indent3 + IND + IND + IND;
            indexBox = indexBox + coord + " - boxfirst1(" + i + "), ";
            indexBoxP = indexBoxP + coord + " + boxfirst1(" + i + "), ";
            indexBox1 = indexBox1 + coord + "1 - boxfirst1(" + i + "), ";
            indexBoxP1 = indexBoxP1 + coord + "1 + boxfirst1(" + i + "), ";
            index = index + coord + ", ";
            index1 = index1 + coord + "1, ";
            indexP = indexP + "p" + (i + 1) + ", ";
            indexPA = indexPA + "[p" + (i + 1) + "]";
            notSameIndexCond = notSameIndexCond + coord + "1 != " + coord + " || ";
            insideCond = insideCond + "(" + coord + "1 >= d_ghost_width || (" + coord + "1 >= 0 && " 
                + "!touch" + coord + "l && " 
                + "!touchP" + coord + "l)) && "
                + "(" + coord + "1 < " + coord + "last - d_ghost_width || (" + coord + "1 < " + coord + "last && "
                + "!touch" + coord + "u && "
                + "!touchP" + coord + "u)) && ";
            insideCond2 = insideCond2 + coord + "1 >= 0 && " + coord + "1 < " + coord + "last && ";
            boundCond = boundCond + "(" + coord + " == boxfirst(" + i + ") && touch" + coord + "l) || (" + coord + " == boxlast(" 
                + i + ") && touch" + coord + "u) || ";
            if (i == 0) {
                maxDx = "dx[0]";
            }
            else {
                maxDx = "max<double>(dx[" + i + "], " + maxDx + ")";
            }
        }
        indexBox = indexBox.substring(0, indexBox.lastIndexOf(","));
        indexBoxP = indexBoxP.substring(0, indexBoxP.lastIndexOf(","));
        indexBox1 = indexBox1.substring(0, indexBox1.lastIndexOf(","));
        indexBoxP1 = indexBoxP1.substring(0, indexBoxP1.lastIndexOf(","));
        index = index.substring(0, index.lastIndexOf(","));
        index1 = index1.substring(0, index1.lastIndexOf(","));
        indexP = indexP.substring(0, indexP.lastIndexOf(","));
        notSameIndexCond = "(" + notSameIndexCond.substring(0, notSameIndexCond.lastIndexOf(" ||")) + ")";
        insideCond = insideCond.substring(0, insideCond.lastIndexOf(" &&"));
        insideCond2 = insideCond2.substring(0, insideCond2.lastIndexOf(" &&"));
        boundCond = boundCond.substring(0, boundCond.lastIndexOf(" ||"));
        
        ArrayList<String> elements = new ArrayList<String>();
        for (int i = 0; i < INTERPHASELENGTH; i++) {
            elements.add(String.valueOf(i));
        }
        ArrayList<String> combinationsReduced = CodeGeneratorUtils.createCombinations(pi.getDimensions() - 1, elements); 
        String cellDist = "";
        String shiftDec = "";
        String pDist = "";
        String pDist2 = "";
        String setPositions = "";
        String setPositions1 = "";
        String coordMinMax = "";
        String posValAuxVars = "";
        String partPos = "";
        String addInterphase = "";
        String thresholds = "";
        for (int i = 0; i < pi.getDimensions(); i++) {
            String coord = pi.getCoordinates().get(i);
            String contCoord = CodeGeneratorUtils.discToCont(pi.getProblem(), coord);
            thresholds = thresholds + indent + IND + IND + "int threshold" + contCoord + " = (int)(corridor_width * " + maxDx + " / dx[" + i 
                    + "]) - minLS / dx[" + i + "];" + NL;
            setPositions1 = setPositions1 +  indent3 + IND + IND + IND + IND + "inter->position" + coord + indexPA + " = (" + coord 
                    + "1 + boxfirst1(" + i + ") + shift" + coord + ") * dx[" + i + "] + d_grid_geometry->getXLower()[" + i + "];" + NL;
        }
        for (int i = 0; i < pi.getDimensions(); i++) {
            String coord = pi.getCoordinates().get(i);
            cellDist = cellDist + indent2 + IND + "double cellDist" + coord + " = " + coord + "1 - " + coord + ";" + NL;
            shiftDec = shiftDec + indent2 + IND + "double shift" + coord + " = 0;" + NL;
            pDist = pDist + indent3 + IND + "double pDist" + coord + " = cellDist" + coord + ";" + NL
                    + indent3 + IND + "if (cellDist" + coord + " > 0) pDist" + coord + " = cellDist" + coord + " - shift" + coord + ";" + NL
                    + indent3 + IND + "if (cellDist" + coord + " < 0) pDist" + coord + " = cellDist" + coord + " + (1 - shift" + coord + ");" + NL;
            pDist2 = pDist2 + "pDist" + coord + " * dx[" + i + "] * pDist" + coord + " * dx[" + i + "] + ";
            setPositions = setPositions +  indent3 + IND + "inter->position" + coord + indexPA + " = (" + coord + " + boxfirst1(" + i + ") + shift" 
                    + coord + ") * dx[" + i + "] + d_grid_geometry->getXLower()[" + i + "];" + NL;
            coordMinMax = coordMinMax + indent + IND + "int " + coord + "Min = " + coord + ";" + NL
                    + indent + IND + "int " + coord + "Max = " + coord + ";" + NL                     
                    + indent + IND + "if (" + coord + " == boxfirst(" + i + ")) " + coord + "Min = " + coord + " - d_ghost_width;" + NL
                    + indent + IND + "if (" + coord + " == boxlast(" + i + "))  " + coord + "Max = " + coord + " + d_ghost_width;" + NL;
            String minIndex = "";
            String maxIndex = "";
            for (int j = 0; j < pi.getDimensions(); j++) {
                minIndex = minIndex + "0, ";
                if (i == j) {
                    maxIndex = maxIndex + (INTERPHASELENGTH - 1) + ", ";
                }
                else {
                    maxIndex = maxIndex + "0, ";
                }
            }
            minIndex = minIndex.substring(0, minIndex.lastIndexOf(","));
            maxIndex = maxIndex.substring(0, maxIndex.lastIndexOf(","));
            posValAuxVars = posValAuxVars + indent + IND + "double posAux_" + coord + "_lwr = vectorP_r(position" + coord + ", sop, " + minIndex
                    + ");" + NL
                    + indent + IND + "double posAux_" + coord + "_upr = vectorP_r(position" + coord + ", sop, " + maxIndex + ");" + NL;
            partPos = partPos + indent3 + IND + IND + "vectorP_r(position" + coord + ", inter, " + indexP + ") = (" + coord + "1 + shift" + coord
                    + ") * dx[" + i + "] + d_grid_geometry->getXLower()[" + i + "];" + NL;
            String minLSL = "";
            String minLSU = "";
            String signsL = "";
            String signsU = "";
            String ls = "";
            for (int j = 0; j < pi.getRegions().size(); j++) {
                String segName = pi.getRegions().get(j); 
                String segId = CodeGeneratorUtils.getRegionId(pi.getProblem(), pi.getRegions(), segName);
                for (int k = 0; k < combinationsReduced.size(); k++) {
                    String comb = combinationsReduced.get(k);
                    String indexBoundL = "";
                    String indexBoundU = "";
                    int counter = 0;
                    for (int l = 0; l < pi.getDimensions(); l++) {
                        if (l == i) {
                            indexBoundU = indexBoundU + (INTERPHASELENGTH - 1) + ", ";
                            indexBoundL = indexBoundL + "0, ";
                        }
                        else {
                            indexBoundU = indexBoundU + comb.substring(counter, counter + 1) + ", ";
                            indexBoundL = indexBoundL + comb.substring(counter, counter + 1) + ", ";
                            counter++;
                        }
                    }
                    indexBoundU = indexBoundU.substring(0, indexBoundU.lastIndexOf(","));
                    indexBoundL = indexBoundL.substring(0, indexBoundL.lastIndexOf(","));
                    if (j == 0 && k == 0) {
                        minLSU = "fabs(vectorP(ls_" + segId + ", " + index + ", " + indexBoundU + "))";
                        minLSL = "fabs(vectorP(ls_" + segId + ", " + index + ", " + indexBoundL + "))";
                    }
                    else {
                        minLSU = "min<double>(fabs(vectorP(ls_" + segId + ", " + index + ", " + indexBoundU + ")), " + minLSU + ")";
                        minLSL = "min<double>(fabs(vectorP(ls_" + segId + ", " + index + ", " + indexBoundL + ")), " + minLSL + ")";
                    }
                }
                signsL = signsL + indent + IND + IND + "int signLS_" + segId + " = vectorP(ls_" + segId + ", " + index + ", " + maxIndex
                        + ")/fabs(vectorP(ls_" + segId + ", " + index + ", " + maxIndex + ")) > 0 ? 1 : -1;" + NL;
                signsU = signsU + indent + IND + IND + "int signLS_" + segId + " = vectorP(ls_" + segId + ", " + index + ", " + minIndex 
                        + ")/fabs(vectorP(ls_" + segId + ", " + index + ", " + minIndex + ")) > 0 ? 1 : -1;" + NL;
                ls = ls + indent3 + IND + IND + IND + IND + "vectorP(ls_" + segId + ", " + index1 + ", " + indexP + ") = signLS_" + segId 
                        + " * maxThreshold * 0.99;" + NL;
            }
            addInterphase = addInterphase + indent + IND + "if (" + coord + " < d_ghost_width && !touch" + coord + "l) {" + NL
                    + indent + IND + IND + "double minLS = " + minLSU + ";" + NL
                    + thresholds
                    + signsL
                    + loopInitThresholdBound(pi.getProblem(), pi.getCoordinates(), indent + IND + IND, i, "+")
                    + indent2 + IND + IND + "if (" + insideCond2 + ") {" + NL
                    + indent2 + IND + IND + IND + "if (vector(soporte, " + index1 + ") != 1) {" + NL
                    + indent2 + IND + IND + IND + IND + "hier::Index idx1(" + indexBoxP1 + ");" + NL
                    + indent2 + IND + IND + IND + IND + "Interphase* inter = new Interphase();" + NL
                    + indent2 + IND + IND + IND + IND + "vector(soporte, " + index1 + ") = 1;" + NL
                    + loopInitPart(pi.getCoordinates(), indent2 + IND + IND + IND + IND, false, true, "")
                    + ls
                    + setPositions1
                    + loopEnd(pi.getDimensions(), indent2 + IND + IND + IND + IND) + NL
                    + indent2 + IND + IND + IND + IND + "interphase->addItemPointer(idx1, inter);" + NL
                    + indent2 + IND + IND + IND + "}" + NL
                    + indent2 + IND + IND + "}" + NL
                    + loopEnd(pi.getDimensions(), indent + IND + IND) + NL
                    + indent + IND + "}" + NL
                    + indent + IND + "if ((" + coord + " >= " + coord + "last - d_ghost_width) && !touch" + coord + "u ) {" + NL
                    + indent + IND + IND + "double minLS = " + minLSL + ";" + NL
                    + thresholds
                    + signsU
                    + loopInitThresholdBound(pi.getProblem(), pi.getCoordinates(), indent + IND + IND, i, "-")
                    + indent2 + IND + IND + "if (" + insideCond2 + ") {" + NL
                    + indent2 + IND + IND + IND + "if (vector(soporte, " + index1 + ") != 1) {" + NL
                    + indent2 + IND + IND + IND + IND + "hier::Index idx1(" + indexBoxP1 + ");" + NL
                    + indent2 + IND + IND + IND + IND + "Interphase* inter = new Interphase();" + NL
                    + indent2 + IND + IND + IND + IND + "vector(soporte, " + index1 + ") = 1;" + NL
                    + loopInitPart(pi.getCoordinates(), indent2 + IND + IND + IND + IND, false, true, "")
                    + ls
                    + setPositions1
                    + loopEnd(pi.getDimensions(), indent2 + IND + IND + IND + IND) + NL
                    + indent2 + IND + IND + IND + IND + "interphase->addItemPointer(idx1, inter);" + NL
                    + indent2 + IND + IND + IND + "}" + NL
                    + indent2 + IND + IND + "}" + NL
                    + loopEnd(pi.getDimensions(), indent + IND + IND) + NL
                    + indent + IND + "}" + NL;
        }
        pDist2 = pDist2.substring(0, pDist2.lastIndexOf(" +"));
        
        ArrayList<String> combinations = CodeGeneratorUtils.createCombinations(pi.getDimensions(), elements); 
        elements.clear();
        elements.add("l");
        elements.add("u");
        ArrayList<String> combinationsLU = CodeGeneratorUtils.createCombinations(pi.getDimensions(), elements);  
        String lsPInit = "";
        String normalInit = "";
        String interphaseCond = "";
        String ls = "";
        for (int i = 0; i < pi.getRegions().size(); i++) {
            String segName = pi.getRegions().get(i); 
            String segId = CodeGeneratorUtils.getRegionId(pi.getProblem(), pi.getRegions(), segName);
            if (i < pi.getRegions().size() - 1) {
                for (int j = i + 1; j < pi.getRegions().size(); j++) {
                    String segName2 = pi.getRegions().get(j); 
                    String segId2 = CodeGeneratorUtils.getRegionId(pi.getProblem(), pi.getRegions(), segName2);
                    for (int k = 0; k < pi.getDimensions(); k++) {
                        String coord = pi.getCoordinates().get(k);
                        normalInit = normalInit + indent + "vector(normal" + coord + "_" + segId +  segId2 + ", " + index + ") = 0;" + NL;
                    }
                }
            }
            for (int j = 0; j < combinations.size(); j++) {
                String comb = combinations.get(j);
                String pIndex2 = "";
                for (int k = 0; k < comb.length(); k++) {
                    pIndex2 = pIndex2 + comb.substring(k, k + 1) + ", ";
                }
                pIndex2 = pIndex2.substring(0, pIndex2.lastIndexOf(","));
                lsPInit = lsPInit + indent + "vectorP(ls_" + segId + ", " + index + ", " + pIndex2 + ") = 99999999999;" + NL;
            }
            interphaseCond = interphaseCond + "(vector(FOV_" + segId + ", " + index + ") > 0 && vector(FOV_" + segId + ", " + index1 + ") == 0) || ";
            ls = ls + indent3 + IND + "if (tmpDist < fabs(vectorP(ls_" + segId + ", " + index + ", " + indexP + ")) && ((vector(FOV_" + segId + ", "
                + index1 + ") > 0 && vector(FOV_" + segId + ", " + index + ") == 0) || (vector(FOV_" + segId + ", " + index1 
                + ") == 0 && vector(FOV_" + segId + ", " + index + ") > 0))) {" + NL
                + indent3 + IND + IND + "vectorP(ls_" + segId + ", " + index + ", " + indexP + ") = tmpDist;" + NL
                + indent3 + IND + IND + "if (vector(FOV_" + segId + ", " + index + ") > 0) {" + NL
                + indent3 + IND + IND + IND + "vectorP(ls_" + segId + ", " + index + ", " + indexP + ") = - vectorP(ls_" + segId + ", " + index
                + ", " + indexP + ");" + NL
                + indent3 + IND + IND + "}" + NL
                + indent3 + IND + "}" + NL;
            for (int j = 0; j < combinationsLU.size(); j++) {
                String comb = combinationsLU.get(j);
                String indexLU = "";
                for (int k = 0; k < comb.length(); k++) {
                    if (comb.substring(k, k + 1).equals("l")) {
                        indexLU = indexLU + "0, ";
                    }
                    else {
                        indexLU = indexLU + (INTERPHASELENGTH - 1) + ", ";
                    }
                }
                indexLU = indexLU.substring(0, indexLU.lastIndexOf(","));
                posValAuxVars = posValAuxVars + indent + IND + "double valAux_" + segId + "_" + comb + " = vectorP(ls_" + segId + ", " 
                        + indexBox + ", " + indexLU + ");" + NL;
            }
        }
        interphaseCond = "(" + interphaseCond.substring(0, interphaseCond.lastIndexOf(" ||")) + ")";
        
        return IND + IND + "//Initial level set mapping" + NL
                + IND + IND + "for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {" + NL
                + IND + IND + IND + "const std::shared_ptr<hier::Patch >& patch = *p_it;" + NL
                + IND + IND + IND + "std::shared_ptr< pdat::IndexData<Interphase, pdat::CellGeometry > > " 
                + "interphase(patch->getPatchData(d_interphase_id), SAMRAI_SHARED_PTR_CAST_TAG);" + NL + NL
                + SAMRAIUtils.createFOVDeclaration(pi.getRegionIds(), IND + IND + IND, "patch->")
                + lsNormalDeclaration(pi, IND + IND + IND, true, false)
                + IND + IND + IND + "int* soporte = ((pdat::CellData<int> *) patch->getPatchData(d_soporte_id).get())->getPointer();" + NL + NL
                + lsDeclaration(pi, IND + IND + IND)
                + IND + IND + IND + "const hier::Index boxfirst = patch->getBox().lower();" + NL
                + IND + IND + IND + "const hier::Index boxlast  = patch->getBox().upper();" + NL
                + IND + IND + IND + "const hier::Index boxfirst1 = ((pdat::CellData<int> *) "
                + "patch->getPatchData(d_soporte_id).get())->getGhostBox().lower();" + NL
                + IND + IND + IND + "const hier::Index boxlast1  = ((pdat::CellData<int> *) "
                + "patch->getPatchData(d_soporte_id).get())->getGhostBox().upper();" + NL + NL
                + IND + IND + IND + "//Get delta spaces into an array. dx, dy, dz." + NL
                + IND + IND + IND + "const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));" + NL
                + IND + IND + IND + "const double* dx  = patch_geom->getDx();" + NL + NL
                + SAMRAIUtils.getLasts(pi, IND + IND + IND, true) + NL + NL
                + getTouchVars(pi.getCoordinates(), IND + IND + IND, true) + NL
                + loopInitComplete(pi.getCoordinates(), IND + IND + IND)
                + lsPInit
                + loopEnd(pi.getDimensions(), IND + IND + IND) + NL
                + loopInitNoGhosts(pi.getCoordinates(), IND + IND + IND)
                + indent + "vector(soporte, " + index + ") = 0;" + NL
                + normalInit
                + createThresholds(pi, indent)
                + loopInitThreshold(pi.getProblem(), pi.getCoordinates(), indent)
                + indent2 + "if (" + notSameIndexCond + " && " + insideCond + " && " + interphaseCond + ") {" + NL
                + cellDist
                + indent2 + IND + "hier::Index idx(" + indexBoxP + ");" + NL
                + indent2 + IND + "if (vector(soporte, " + index + ") != 1) {" + NL
                + indent2 + IND + IND + "Interphase* inter = new Interphase();" + NL
                + indent2 + IND + IND + "interphase->addItemPointer(idx, inter);" + NL
                + indent2 + IND + IND + "vector(soporte, " + index + ") = 1;" + NL
                + indent2 + IND + "}" + NL
                + shiftDec
                + indent2 + IND + "Interphase* inter = interphase->getItem(idx);" + NL
                + loopInitPart(pi.getCoordinates(), indent2 + IND, false, true, "")
                + pDist
                + indent3 + IND + "double tmpDist = sqrt(" + pDist2 + ");" + NL
                + ls
                + setPositions
                + loopEnd(pi.getDimensions(), indent2 + IND) + NL
                + indent2 + "}" + NL
                + loopEnd(pi.getDimensions(), indent) + NL
                + loopEnd(pi.getDimensions(), IND + IND + IND) + NL
                + IND + IND + IND + "/******************************************************************************************************/" + NL
                + IND + IND + IND + "//Adding particle support to (only EXTERNAL) boundaries (basically, we apply a LINEAR INTERPOLATION " 
                + "condition for LS)" + NL
                + getMaxMinThresholds(pi.getDimensions(), IND + IND + IND)
                + loopInit(pi.getCoordinates(), IND + IND + IND, true)
                + indent + "if ((" + boundCond + ") && vector(soporte, " + indexBox + ") == 1 ) {" + NL
                + indent + IND + "hier::Index idx(" + index + ");" + NL
                + indent + IND + "Interphase* sop = interphase->getItem(idx);" + NL
                + coordMinMax
                + indent + IND + "// Auxiliary data for extrapolation" + NL
                + posValAuxVars
                + indent + IND + "//Adding support" + NL
                + loopInitNestedMin(pi.getCoordinates(), indent + IND)                        
                + indent2 + IND + "if (" + notSameIndexCond + ") {" + NL
                + indent2 + IND + IND + "hier::Index idx1(" + index1 + ");" + NL
                + indent2 + IND + IND + "Interphase* inter = new Interphase();" + NL
                + indent2 + IND + IND + "interphase->addItemPointer(idx1, inter);" + NL
                + indent2 + IND + IND + "vector(soporte, " + indexBox1 + ") = 1;" + NL
                + loopInitPart(pi.getCoordinates(), indent2 + IND + IND, false, true, "")                           
                + partPos + NL
                + calculateInitialLS(pi.getProblem(), pi.getCoordinates(), pi.getRegions(), indent3 + IND + IND, indexBox1, indexP)
                + loopEnd(pi.getDimensions(), indent2 + IND + IND) + NL
                + indent2 + IND + "}" + NL
                + loopEnd(pi.getDimensions(), indent + IND) + NL
                + indent + "}" + NL
                + loopEnd(pi.getDimensions(), IND + IND + IND) + NL
                + IND + IND + IND + "/******************************************************************************************************/" + NL
                + IND + IND + "}" + NL + NL
                + IND + IND + "// Only for INTERNAL boundaries" + NL
                + IND + IND + "d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);" + NL
                + IND + IND + "for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {" + NL
                + IND + IND + IND + "const std::shared_ptr<hier::Patch >& patch = *p_it;" + NL
                + IND + IND + IND + "std::shared_ptr< pdat::IndexData<Interphase, pdat::CellGeometry > > " 
                + "interphase(patch->getPatchData(d_interphase_id), SAMRAI_SHARED_PTR_CAST_TAG);" + NL + NL
                + SAMRAIUtils.createFOVDeclaration(pi.getRegionIds(), IND + IND + IND, "patch->")
                + lsNormalDeclaration(pi, IND + IND + IND, true, false)
                + IND + IND + IND + "int* soporte = ((pdat::CellData<int> *) patch->getPatchData(d_soporte_id).get())->getPointer();" + NL + NL
                + lsDeclaration(pi, IND + IND + IND)
                + IND + IND + IND + "const hier::Index boxfirst = patch->getBox().lower();" + NL
                + IND + IND + IND + "const hier::Index boxlast  = patch->getBox().upper();" + NL
                + IND + IND + IND + "const hier::Index boxfirst1 = interphase->getGhostBox().lower();" + NL
                + IND + IND + IND + "const hier::Index boxlast1  = interphase->getGhostBox().upper();" + NL + NL
                + IND + IND + IND + "//Get delta spaces into an array. dx, dy, dz." + NL
                + IND + IND + IND + "const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));" + NL
                + getTouchVars(pi.getCoordinates(), IND + IND + IND, false) + NL
                + IND + IND + IND + "const double* dx  = patch_geom->getDx();" + NL + NL
                + SAMRAIUtils.getLasts(pi, IND + IND + IND, true) + NL + NL
                + getMaxMinThresholds(pi.getDimensions(), IND + IND + IND)
                + loopInitComplete(pi.getCoordinates(), IND + IND + IND)
                + indent + "if (vector(soporte, " + index + ") == 1) {" + NL
                + indent + IND + "hier::Index idx(" + indexBoxP + ");" + NL
                + indent + IND + "Interphase* sop = interphase->getItem(idx);" + NL
                + addInterphase
                + indent + "}" + NL
                + loopEnd(pi.getDimensions(), IND + IND + IND) + NL
                + IND + IND + "}" + NL;
    }
}
