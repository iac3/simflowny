/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.codeGeneratorPlugin;

import eu.iac3.mathMS.codeGeneratorPlugin.graph.BOOSTABM_graphCode;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.CodeGeneratorUtils;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.ExtrapolationType;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.ProblemInfo;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.SAMRAIUtils;
import eu.iac3.mathMS.codesDBPlugin.CodeResult;
import eu.iac3.mathMS.codesDBPlugin.CodesDB;
import eu.iac3.mathMS.codesDBPlugin.CodesDBImpl;
import eu.iac3.mathMS.codesDBPlugin.DCException;
import eu.iac3.mathMS.simflownyUtilsPlugin.SimflownyUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.Dictionary;
import java.util.Iterator;

import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceReference;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.service.log.LogService;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * The processes to generate the code for SimPlat platform.
 * 
 * @author bminyano
 *
 */
public class BoostCodeGenerator extends CodeGeneratorImpl {
    
    static final String NEWLINE = System.getProperty("line.separator");
    static final String INDENT = "\u0009";
    final String mtUri = "http://www.w3.org/1998/Math/MathML";
    static String spUri = "urn:simPlat";
    static String mmsUri = "urn:mathms";
    static final int THREE = 3;
    static final int INTERPHASELENGTH = 3;
    static final String IND = "\u0009";
    static final String NL = System.getProperty("line.separator");
    static BundleContext context;
    
    /**
     * Constructor to take the bundle context.
     */
    public BoostCodeGenerator() {
        super();
        context = FrameworkUtil.getBundle(getClass()).getBundleContext();
        new SAMRAIUtils();
    }
    /**
     * Generates the code for SimPlat.
     * 
     * @param document            the document to generate the code from
     * @return                  the document of the generated code
     * @throws CGException      CG001 XML document does not match xml schema
     *                           CG002 Code already exist.
     *                           CG003 Not possible to create code for the platform
     *                           CG004 External error
     */
    public String generateBoostCode(String document) throws CGException {
        CodesDB cdb = null;
        String id = null;
        try {
            String xmlDoc = SimflownyUtils.jsonToXML(document);
            
            Document doc = CodeGeneratorUtils.stringToDom(xmlDoc);
            if (!CodeGeneratorUtils.isGraphProblem(doc)) {
                throw new CGException(CGException.CG003, "Only ABM on graph can be generated with Boost library");
            }
            
            //Get configuration
            String mpich;
            String hdf5;
            String boost;
            @SuppressWarnings("rawtypes")
			ServiceReference serviceReference = context.getServiceReference(ConfigurationAdmin.class.getName());
            @SuppressWarnings("unchecked")
			ConfigurationAdmin configAdmin = (ConfigurationAdmin) context.getService(serviceReference); 
            Configuration config = configAdmin.getConfiguration("eu.iac3.mathMS.codeGeneratorPlugin"); 
            Dictionary< ? , ? > properties = config.getProperties();
            if (properties != null) {
                mpich = (String) properties.get("eu.iac3.codeGeneratorPlugin.mpichHome");
                hdf5 = (String) properties.get("eu.iac3.codeGeneratorPlugin.hdf5Home");
                boost = (String) properties.get("eu.iac3.codeGeneratorPlugin.boostHome");
            }
            else {
                mpich = System.getProperty("eu.iac3.codeGeneratorPlugin.mpichHome");
                hdf5 = System.getProperty("eu.iac3.codeGeneratorPlugin.hdf5Home");
                boost = System.getProperty("eu.iac3.codeGeneratorPlugin.boostHome");
            }
            if (logservice != null) {
                logservice.log(LogService.LOG_INFO, "Code generator configuration");
                logservice.log(LogService.LOG_INFO, "MPICH: " + mpich);
                logservice.log(LogService.LOG_INFO, "HDF5: " + hdf5);
                logservice.log(LogService.LOG_INFO, "BOOST: " + boost);
            }
          	
            //Validation of the problem received
            CodeGeneratorUtils.schemaValidation(xmlDoc, "agentBasedProblemGraph");
            //Check that the code was not generated previously. We can delete the manual work over the files.
            Date now = new Date();
            String name = doc.getDocumentElement().getFirstChild().getFirstChild().getTextContent();
            cdb = new CodesDBImpl();
            String author = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(mmsUri, "author").item(0).getTextContent();
            String version = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(mmsUri, "version").item(0).getTextContent();
            CodeResult[] crArray = cdb.getCodeList(name, author, version, now, now, PlatformType.simPlatSAMRAI.toString());
            if (crArray != null) {
                throw new CGException(CGException.CG002);
            }
            String problemIdentifier = BOOSTABM_graphCode.variableNormalize(doc.getDocumentElement().getFirstChild().getFirstChild());

            //Get the common information
            ProblemInfo pi = new ProblemInfo();
            CodeGeneratorUtils.replaceEquivalences(doc);
            pi.setHasGraphFields(true);
            pi.setFieldTimeLevels(CodeGeneratorUtils.getFieldTimeLevels(doc));
        	pi.setEdgeFields(CodeGeneratorUtils.getEdgeFields(doc));
        	pi.setDegreeDistribution(CodeGeneratorUtils.getDegreeDistribution(doc));
        	pi.setEdgeDirectionality(CodeGeneratorUtils.getEdgeDirectionality(doc));
            pi.setEvolutionStep(CodeGeneratorUtils.getEvolutionStep(doc));
            pi.setVariables(SAMRAIUtils.getVariables(doc, pi.getFieldTimeLevels(), pi.getCoordinates(), ExtrapolationType.generalPDE));
            pi.setProblem(doc);
            //Creation of the code
            if (logservice != null) {
                logservice.log(LogService.LOG_INFO, "Generating code: " + problemIdentifier);
            }
 
            id = cdb.addGeneratedCode(name, author, version, PlatformType.simPlatSAMRAI.toString());
        	//Graph code *.cpp
            String problemBoost = BOOSTABM_graphCode.create(pi);
        	//parameter.input
            cdb.addCodeFile(id, "parameter", "problem.input", problemInputGraph(pi));
            //Copy iac3_plod_generator.hpp
            cdb.addCodeFile(id, "library", "iac3_plod_generator.hpp", 
                    CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/iac3_plod_generator.hpp"), 0));
            //Copy ProblemName.cpp
            cdb.addCodeFile(id, "sources", problemIdentifier + ".cpp", problemBoost);
        
            cdb.addCodeFile(id, "makefile", "Makefile",
                    CodeGeneratorUtils.createMakefile(getCommand(mpich, hdf5, boost, problemIdentifier)));
            
            //README
            cdb.addCodeFile(id, "readme", "README",
                    CodeGeneratorUtils.convertStreamToString(getClass().getResourceAsStream("/README_BOOST"), 0));
            
            //run.sh
            cdb.addCodeFile(id, "compiler", "run.sh", 
                    CodeGeneratorUtils.runSh(problemIdentifier));
            if (logservice != null) {
                logservice.log(LogService.LOG_INFO, "run.sh created");
            }
            
            return SimflownyUtils.xmlToJSON(CodeGeneratorUtils.createCodeDocument(name, author, version, now, PlatformType.simPlatSAMRAI.toString(), id), false);
        }
        catch (CGException e) {
            e.printStackTrace();
            //If any exception occured, delete the generated code.
            if (id != null) {
                try {
                    cdb.deleteGeneratedCode(id);
                } 
                catch (DCException e1) {
                    e1.printStackTrace();
                    throw new CGException(CGException.CG00X, e.getMessage());
                }
            }
            throw e;
        }
        catch (Exception e) {
            e.printStackTrace();
            //If any exception occured, delete the generated code.
            if (id != null) {
                try {
                    cdb.deleteGeneratedCode(id);
                } 
                catch (DCException e1) {
                    e1.printStackTrace();
                    throw new CGException(CGException.CG00X, e.getMessage());
                }
            }
            throw new CGException(CGException.CG00X, e.getMessage());
        }
    }
    
    /**
     * Generates the input example file for graphs.
     * @param pi            The problem info
     * @return              The code
     * @throws CGException  CG00X External error
     */
    private String problemInputGraph(ProblemInfo pi) throws CGException {
    	String fields = "";
    	Iterator<String> fieldsIt = pi.getFieldTimeLevels().keySet().iterator();
    	while (fieldsIt.hasNext()) {
    	    fields = fields + "\"" + fieldsIt.next() + "\", ";
    	}
    	String inputGraphParams = "";
    	if (pi.getDegreeDistribution().equals("ExternalSpecification")) {
    	    inputGraphParams = "file_name = \"initial.dot\";" + NEWLINE;
    	}
    	if (pi.getDegreeDistribution().equals("Scale-free")) {
    	    inputGraphParams = "gamma_par = 2.3;       //Scale-free gamma (ref. http://en.wikipedia.org/wiki/Scale-free_network)" + NEWLINE;
    	}
    	String edgeFields = "";
    	for (int i = 0; i < pi.getEdgeFields().size(); i++) {
    	    edgeFields = edgeFields + "\"" + pi.getEdgeFields().get(i) + "\", ";
    	}
    	if (!fields.equals("")) {
    	    fields = fields.substring(0, fields.lastIndexOf(", "));
    	}
    	if (!edgeFields.equals("")) {
    	    edgeFields = edgeFields.substring(0, edgeFields.lastIndexOf(", "));
    	}
    	return "number_of_vertices = 1000;" + NEWLINE
    			+ "number_of_edges = 1000;" + NEWLINE
    			+ "seed = 0;" + NEWLINE
    			+ "output_dir_name = \"outputDir\";" + NEWLINE
    			+ "output_dump_interval = 1;" + NEWLINE
    			+ "vertex_properties = [" + fields + "];" + NEWLINE
    			+ "edge_properties = [" + edgeFields + "];" + NEWLINE
    			+ inputGraphParams
    			+ getParametersInput("", pi.getProblem());
    }
    
    /**
     * Creates the parameters lines for the input example file.
     * @param ind           The indentation
     * @param doc           The problem
     * @return              The parameters
     * @throws CGException  CG003 Not possible to create code for the platform 
     *                       CG00X External error
     */
    private String getParametersInput(String ind, Document doc) throws CGException {
        String parameters = "";
        String semiColon = ";";
        NodeList paramList = CodeGeneratorUtils.find(doc, "/*/mms:parameters/mms:parameter");
        for (int i = 0; i < paramList.getLength(); i++) {
            String name = SAMRAIUtils.variableNormalize(paramList.item(i).getFirstChild().getTextContent());
            String type = paramList.item(i).getFirstChild().getNextSibling().getTextContent();
            String defaultValue = paramList.item(i).getLastChild().getTextContent();
            parameters = parameters + ind + SAMRAIUtils.variableNormalize(name) + " = ";
          //No default value, so create a new one
            if (type.equals(defaultValue)) {
                if (type.equals("REAL")) {
                    parameters = parameters + "1.0" + semiColon + NEWLINE;
                }
                if (type.equals("INT")) {
                    parameters = parameters + "1" + semiColon + NEWLINE;
                }
                if (type.equals("STRING")) {
                    parameters = parameters + "\"\"" + semiColon + NEWLINE;
                }
                if (type.equals("BOOLEAN")) {
                    parameters = parameters + "true" + semiColon + NEWLINE;
                }
            }
            else {
                parameters = parameters + defaultValue + semiColon + NEWLINE;
            }
        }
        return parameters;
    }
    

    /**
     * Get the command to execute the compilation.
     * @param mpich         Mpich path
     * @param hdf5          Hdf5 path
     * @param boost         Boost path
     * @param exeName       Executable name
     * @return              Command to execute
     */
    private String[] getCommand(String mpich, String hdf5, String boost, String exeName) {
        ArrayList<String> commands = new ArrayList<String>();
        commands.add("g++");
        commands.add("-fPIC");
        commands.add("-fomit-frame-pointer");
        commands.add("-Os");
        commands.add("-ftree-vectorize");
        commands.add("-fgcse-after-reload");
        commands.add("-finline-functions");
        commands.add("-funswitch-loops");
        //Includes
        commands.add("-I.");
        commands.add("-I" + mpich + File.separator + "include");
        commands.add("-I" + hdf5 + File.separator + "include");
        commands.add("-I" + boost + File.separator + "include");
        //Code files
        commands.add(exeName + ".cpp");
        //Libraries
        commands.add("-L" + mpich + File.separator + "lib");
        commands.add("-L" + hdf5 + File.separator + "lib");
        commands.add("-L" + boost + File.separator + "lib");
        commands.add("-lconfig++");
        commands.add("-lboost_regex");
        commands.add("-lboost_graph");
        commands.add("-lboost_system");
        commands.add("-lboost_graph_parallel");
        commands.add("-lboost_mpi");
        commands.add("-lboost_serialization");
        commands.add("-lmpi_cxx");
        commands.add("-lmpi");
        commands.add("-lm");
        commands.add("-ldl");
        commands.add("-lpthread");
        commands.add("-lutil");
        commands.add("-lhdf5");
        commands.add("-lgsl");
        commands.add("-lgslcblas");
        commands.add("-o");
        commands.add(exeName);
        
        return commands.toArray(new String[commands.size()]);
    }
    
}
