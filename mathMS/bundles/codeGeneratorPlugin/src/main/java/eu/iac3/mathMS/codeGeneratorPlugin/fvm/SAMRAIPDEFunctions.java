/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.codeGeneratorPlugin.fvm;

import eu.iac3.mathMS.codeGeneratorPlugin.CGException;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.CodeGeneratorUtils;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.ProblemInfo;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.SAMRAIUtils;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Functions for FVM code in SAMRAI.
 * @author bminyano
 *
 */
public final class SAMRAIPDEFunctions {
	
    static final String NL = System.getProperty("line.separator");
    static final String INDENT = "\u0009";
    static final int THREE = 3;
    static final String MTURI = "http://www.w3.org/1998/Math/MathML";
    static final int MCDMAX = 10;
    
    /**
     * Default constructor.
     */
    private SAMRAIPDEFunctions() { };
    
    /**
     * Creates the external functions file for simPlat + SAMRAI.
     * @param pi                    The problem information
     * @return                      The file
     * @throws CGException          CG004 External error
     */
    public static String create(ProblemInfo pi) throws CGException {
        String result = "";
        Document doc = pi.getProblem();
        
        String[][] params = new String [THREE][2];
        params[0][0] = "condition";
        params[0][1] = "false";
        params[1][0] = "indent";
        params[1][1] = "1";
        params[2][0] =  "dimensions";
        params[2][1] =  String.valueOf(pi.getDimensions());
        
        int dimensions = pi.getSpatialDimensions();
        try {            
            String coord1 = pi.getCoordinates().get(0);
            String coord2 = pi.getCoordinates().get(1);
            if (dimensions == 2) {
                result = result + "#define vector(v, " + coord1 + ", " + coord2 + ") (v)[" + coord1 + "+" + coord1 + "last*(" 
                        + coord2 + ")]" + NL + NL;
            }
            if (dimensions == THREE) {
                String coord3 = pi.getCoordinates().get(2);
                result = result + "#define vector(v, " + coord1 + ", " + coord2 + ", " + coord3 + ") (v)[" + coord1 + "+" + coord1 + "last*("
                        + coord2 + "+" + coord2 + "last*(" + coord3 + "))]" + NL + NL;
            }
            String macros = "#functions#" + NL;
            String functions = "";
            if (dimensions == 2) {
            	macros = macros + "#define vector(v, " + coord1 + ", " + coord2 + ") (v)[" + coord1 + "+" + coord1 + "last*(" 
                        + coord2 + ")]" + NL + NL;
            }
            if (dimensions == THREE) {
                String coord3 = pi.getCoordinates().get(2);
                macros = macros + "#define vector(v, " + coord1 + ", " + coord2 + ", " + coord3 + ") (v)[" + coord1 + "+" + coord1 + "last*(" 
                        + coord2 + "+" + coord2 + "last*(" + coord3 + "))]" + NL + NL;
            }
            if (!pi.getHardRegions().isEmpty()) {
            	macros = macros + "#define POLINT_MACRO_LINEAR_1(y1, y2, i_ext, s_ext, dx, simPlat_dt, ilast, jlast) (-i_ext*y1 + i_ext*y2 + s_ext*y2)" + NL
            			+ "#define POLINT_MACRO_LINEAR_2(y1, y2, i_ext, s_ext, dx, simPlat_dt, ilast, jlast) (-i_ext*y1 + i_ext*y2 - s_ext*y1)" + NL
            			+ "#define POLINT_MACRO_QUADRATIC_1(y1,y2,y3, i_ext, s_ext, dx, simPlat_dt, ilast, jlast) (0.5*(i_ext*i_ext*y1-2*i_ext*i_ext*y2+i_ext*i_ext*y3+i_ext*s_ext*y1-4*i_ext*s_ext*y2+3*i_ext*s_ext*y3+2*s_ext*s_ext*y3)/(s_ext*s_ext))" + NL
            			+ "#define POLINT_MACRO_QUADRATIC_2(y1,y2,y3, i_ext, s_ext, dx, simPlat_dt, ilast, jlast) (0.5*(i_ext*i_ext*y1-2*i_ext*i_ext*y2+i_ext*i_ext*y3+3*i_ext*s_ext*y1-4*i_ext*s_ext*y2+i_ext*s_ext*y3+2*s_ext*s_ext*y1)/(s_ext*s_ext))" + NL
            			+ "#define POLINT_MACRO_CUBIC_1(y1,y2,y3, y4, i_ext, s_ext, dx, simPlat_dt, ilast, jlast) (-(1.0/6.0)*(i_ext*i_ext*i_ext*y1-3*i_ext*i_ext*i_ext*y2+3*i_ext*i_ext*i_ext*y3-i_ext*i_ext*i_ext*y4+3*i_ext*i_ext*s_ext*y1-12*i_ext*i_ext*s_ext*y2+15*i_ext*i_ext*s_ext*y3-6*i_ext*i_ext*s_ext*y4+2*i_ext*s_ext*s_ext*y1-9*i_ext*s_ext*s_ext*y2+18*i_ext*s_ext*s_ext*y3-11*i_ext*s_ext*s_ext*y4-6*s_ext*s_ext*s_ext*y4)/(s_ext*s_ext*s_ext))" + NL
            			+ "#define POLINT_MACRO_CUBIC_2(y1,y2,y3, y4, i_ext, s_ext, dx, simPlat_dt, ilast, jlast) ((1.0/6.0)*(i_ext*i_ext*i_ext*y1-3*i_ext*i_ext*i_ext*y2+3*i_ext*i_ext*i_ext*y3-i_ext*i_ext*i_ext*y4+6*i_ext*i_ext*s_ext*y1-15*i_ext*i_ext*s_ext*y2+12*i_ext*i_ext*s_ext*y3-3*i_ext*i_ext*s_ext*y4+11*i_ext*s_ext*s_ext*y1-18*i_ext*s_ext*s_ext*y2+9*i_ext*s_ext*s_ext*y3-2*i_ext*s_ext*s_ext*y4+6*s_ext*s_ext*s_ext*y1)/(s_ext*s_ext*s_ext))" + NL + NL;
            }

            /*if (pi.getMovementInfo().itMoves()) {
                macros = macros + "#define indexM(i, k, i1) ((i) + floor(((k) + (i1)) / 3.0))" + NEWLINE
                        + "#define indexPM(k, i1) ((3000 + (k) + (i1)) % 3)" + NEWLINE + NEWLINE;
            }*/
            
            NodeList functionList = CodeGeneratorUtils.find(doc, "/mms:discretizedProblem/mms:functions/mms:function");
            for (int i = 0; i < functionList.getLength(); i++) {
                boolean isMacro = CodeGeneratorUtils.isMacroFunction((Element) functionList.item(i));
                //Function name
                Element funcName = (Element) functionList.item(i).getFirstChild();
                boolean extrapolation = funcName.getTextContent().startsWith("extrapolate_");
                boolean notReturning = CodeGeneratorUtils.find(functionList.item(i).getLastChild(), ".//sml:return[*]").getLength() == 0;
                //Function arguments and declaration
                String arguments = "";
                ArrayList <String> argList = new ArrayList<String>();
                String argumentsMacro = "";
                String declarations = "";
                LinkedHashMap<String, ArrayList<String>> parameterDeclaration = new LinkedHashMap<String, ArrayList<String>>();  
                ArrayList<String> fieldParams = new ArrayList<String>();
                if (funcName.getNextSibling().getLocalName().equals("functionParameters")) {
                    NodeList args = funcName.getNextSibling().getChildNodes();
                    for (int j = 0; j < args.getLength(); j++) {
                        String paramName = "par" + SAMRAIUtils.variableNormalize(args.item(j).getFirstChild().getTextContent());
                        argList.add(paramName);
                        //Add "par" to all the parameter occurrences inside the function
                        NodeList paramOccurrences = CodeGeneratorUtils.find(functionList.item(i), 
                                ".//mt:ci[text() = '" + args.item(j).getFirstChild().getTextContent() + "']");
                        for (int k = 0; k < paramOccurrences.getLength(); k++) {
                            paramOccurrences.item(k).setTextContent(paramName);
                        }
                        String paramType = ((Element)args.item(j)).getElementsByTagNameNS(CodeGeneratorUtils.MMSURI, "type").item(0).getTextContent();
                        ArrayList<String> paramList;
                        if (parameterDeclaration.containsKey(paramType)) {
                            paramList = parameterDeclaration.get(paramType);
                        } 
                        else {
                            paramList = new ArrayList<String>();
                        }
                        paramList.add(paramName);
                        parameterDeclaration.put(paramType, paramList);
                        
                        boolean isDoubleArray = false;
                        if (extrapolation && (paramName.startsWith("parFOV_") || paramName.equals("parFOV") || paramName.equals("parStalled"))) {
                            isDoubleArray = true;
                        }
                        
                        if (isMacro) {
                            arguments = arguments + ", " + paramName;
                        }
                        else {
                        	String accessor = "";
                        	if (CodeGeneratorUtils.isAssigned(paramName, functionList.item(i).getLastChild())) {
                        		accessor = "&";
                        	}
                        	String functionParamParams = "";
                            if (paramType.equals("function")) {
                                NodeList paramParams = CodeGeneratorUtils.find(((Element)args.item(j)).getElementsByTagNameNS(CodeGeneratorUtils.MMSURI, "functionParameters").item(0), 
                                        "./mms:functionParameterType");
                                //Check for reference access in embedded function parameters
                                Node functionCall = CodeGeneratorUtils.find(doc, "//sml:functionCall[*[position() = 1][text() = '" + funcName.getTextContent() + "']]").item(0);
                                String embeddedFunctionName = functionCall.getChildNodes().item(j + 1).getTextContent();
                                Node embeddedFunction = CodeGeneratorUtils.find(doc, "//mms:function[mms:functionName = '" + embeddedFunctionName + "']").item(0);
                                if (embeddedFunction == null) {
                                	throw new CGException(CGException.CG004, "Embedded function " + embeddedFunctionName + " not found.");
                                }
                                NodeList embeddedFunctionPars = ((Element) embeddedFunction).getElementsByTagNameNS(CodeGeneratorUtils.MMSURI, "functionParameters").item(0).getChildNodes();
                            	for (int k = 0; k < paramParams.getLength(); k++) {
                            		String accessorFunction = "";
                                	if (CodeGeneratorUtils.isAssigned(embeddedFunctionPars.item(k).getFirstChild().getTextContent(), embeddedFunction.getLastChild())) {
                                		accessorFunction = "&";
                                	}
                            		functionParamParams = functionParamParams + SAMRAIUtils.getFVMType(paramParams.item(k).getTextContent(), false) + accessorFunction + ", ";
                            	}
                            	functionParamParams = "(" + functionParamParams + "const double*, const double, const int, const int)";
                            }
                            arguments = arguments + ", " + SAMRAIUtils.getFVMType(paramType, isDoubleArray) + accessor + " " + paramName + functionParamParams;
                        }
                        if (paramType.equals("field")) {
                            fieldParams.add(paramName);
                        }
                        else {
                            argumentsMacro = argumentsMacro + paramName + ",";
                        }
                    }
                    arguments = arguments.substring(2) + ", ";
                }
                //Declaration of variables
                declarations = declarations
                    + SAMRAIUtils.getPDEVariableDeclaration(functionList.item(i).getLastChild(), pi, 1, argList, "patch->", false, true) + NL;

                //Function instructions
                String instructions = "";
             
                if (isMacro) {
                    params[0][1] = "true";
                    params[1][1] = "0";
                }
                else {
                    params[0][1] = "false";
                    params[1][1] = "1";
                }

                ProblemInfo newPi = new ProblemInfo();
                newPi.setVariables(fieldParams);
                newPi.setCoordinates(pi.getCoordinates());
                newPi.setContCoordinates(pi.getContCoordinates());
                newPi.setExtrapolVars(new ArrayList<String>());
                newPi.setExtrapolFuncVars(new ArrayList<String>());
                Element instSet = (Element) SAMRAIUtils.preProcessPDEBlock(functionList.item(i).getLastChild().getFirstChild().cloneNode(true),
                        newPi, true, isMacro);
                //If macro then the parameters has to be inside parenthesis
                if (isMacro) {
                    introduceParenthesis(argumentsMacro, instSet);
                }
                NodeList instructionList = instSet.getChildNodes();
                for (int j = 0; j < instructionList.getLength(); j++) {
                    if (isMacro) {
                        instructions = instructions + SAMRAIUtils.xmlTransform(instructionList.item(j).getFirstChild(), params);
                    }
                    else {
                        instructions = instructions + SAMRAIUtils.xmlTransform(instructionList.item(j), params);
                    }
                }
                
                String functionName = SAMRAIUtils.variableNormalize(funcName.getTextContent());
                if (isMacro) {
                    macros = macros + "#define " + functionName + "(" + arguments 
                        + "dx, simPlat_dt, ilast, jlast) (" + instructions + ")" + NL + NL;
                }
                else {
                    String returning = "double";
                    String dt = "const double simPlat_dt, ";
                	if (extrapolation || notReturning) {
                		returning = "void";
                	}
                    if (extrapolation) {
                        dt = "";
                    }
                    functions = functions + "inline " + returning + " " + functionName + "(" + arguments 
                        + "const double* dx, " + dt + "const int ilast, const int jlast) {" + NL
                        + declarations
                        + instructions + NL
                        + "};" + NL + NL;
                }
            }
            //Unitary vector calculation for extrapolation distance variables
            if (!pi.getHardRegions().isEmpty()) {
                String args = "";
                for (int i = 0; i < pi.getDimensions(); i++) {
                    args = args + pi.getCoordinates().get(i) + ", ";
                }
                args = args.substring(0, args.lastIndexOf(","));
                macros = macros + "#define Unit_MCD(" + args + ") (" + calculateUnitMacro(pi.getCoordinates()) + ")" 
                        + NL + NL; 
            }
            return result + macros + functions;
        } 
        catch (Exception e) {
            e.printStackTrace();
            throw new CGException(CGException.CG00X, e.getMessage());
        }
    }
    
    /**
     * Calculate the Macro for the maximal common divisor of a distance vector.
     * @param coords        The spatial coordinates
     * @return              The macro
     */
    private static String calculateUnitMacro(ArrayList<String> coords) {
        String code = "";
        for (int i = MCDMAX; i > 1; i--) {
            String cond = "";
            for (int j = 0; j < coords.size(); j++) {
                String coord = coords.get(j);
                cond = cond + "((int)" + coord + ") % " + i + " == 0 && ";
            }
            cond = cond.substring(0, cond.lastIndexOf(" &&"));
            code = code + "(" + cond + ") ? " + i + " : ";
        }
        code = code + "1";
        return code;
    }
    
    /**
     * Introduce parenthesis in the macro functions to avoid precedence operation problems.
     * @param argumentsMacro    The arguments of the macros
     * @param function          The function
     * @throws CGException      CG004 External error
     */
    private static void introduceParenthesis(String argumentsMacro, Element function) throws CGException {
        Document doc = function.getOwnerDocument();
        if (argumentsMacro.length() > 0) {
            argumentsMacro = argumentsMacro.substring(0, argumentsMacro.lastIndexOf(","));
        }
        String[] argumentArray = argumentsMacro.split(",");
        for (int j = 0; j < argumentArray.length; j++) {
            NodeList paramOccurrences = CodeGeneratorUtils.find(function.getLastChild().getFirstChild(), 
                    ".//mt:ci[text() = '" + argumentArray[j] + "']");
            for (int k = paramOccurrences.getLength() - 1; k >= 0; k--) {
                Element parent = (Element) paramOccurrences.item(k).getParentNode();
                Element nothing = CodeGeneratorUtils.createElement(doc, MTURI, "ci");
                Element apply = CodeGeneratorUtils.createElement(doc, MTURI, "apply");
                apply.appendChild(nothing);
                apply.appendChild(paramOccurrences.item(k).cloneNode(true));
                parent.replaceChild(apply, paramOccurrences.item(k));
            }
        }
    }
}
