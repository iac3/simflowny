/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.codeGeneratorPlugin.fvm;

import eu.iac3.mathMS.codeGeneratorPlugin.CGException;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.CactusUtils;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.CactusVarGroupInfo;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.CodeGeneratorUtils;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.HeaderInfo;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.ProblemInfo;

import java.util.ArrayList;
import java.util.Iterator;

import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Initializations for FVM code in Cactus.
 * @author bminyano
 *
 */
public final class CactusInitializations {
    static final String NL = System.getProperty("line.separator"); 
    static final String IND = "\u0009";
    static final int THREE = 3;
    static final String MTURI = "http://www.w3.org/1998/Math/MathML";
    static final String MMSURI = "urn:mathms";
    
    /**
     * Private constructor.
     */
    private CactusInitializations() { };
    
    /**
     * Generate the content for the InitialConditions.F file.
     * @param hi                    the header information
     * @param problemInfo           The problem information
     * @return                      the content of the file
     * @throws CGException          CG003 Not possible to create code for the platform
     *                              CG004 External error
     */
    public static String initialConditionsF(HeaderInfo hi, ProblemInfo problemInfo) throws CGException {
        try {
            //Parameters for the xsl transformation. It is not a condition formula
            String[][] xslParams = new String [2][2];
            xslParams[0][0] =  "condition";
            xslParams[0][1] =  "false";
            xslParams[1][0] =  "indent";
            xslParams[1][1] =  "0";
            Document doc = problemInfo.getProblem();
            
            String localVariables = "!" + IND + IND + "Declare local variables" + NL
                + "!" + IND + IND + "-----------------" + NL + NL
                + IND + IND + "CCTK_INT dist_to_segment, status, ReflectionCounter" + NL;
            //Getting all the variables used in the initial conditions
            //Declaration of spatial coordinates, and its variables for start and end.
            for (int i = 0; i < problemInfo.getCoordinates().size(); i++) {
                String coord = CactusUtils.variableNormalize(problemInfo.getCoordinates().get(i));
                localVariables = localVariables + IND + IND + "CCTK_INT " + coord + ", " + coord + "Start, " + coord + "End, " 
                    + coord + "MapStart, " + coord + "MapEnd"  + NL;
            }
            
            //Declaration of deltas
            DocumentFragment scope = doc.createDocumentFragment();
            NodeList fc = CodeGeneratorUtils.find(doc, "//mms:initialConditions");
            for (int i = 0; i < fc.getLength(); i++) {
                scope.appendChild(fc.item(i).cloneNode(true));
            }
            localVariables = localVariables + CactusUtils.getVariableDeclaration(scope, problemInfo.getIdentifiers(), null) + NL;
            localVariables = localVariables + CactusUtils.getAllDeltas(doc, problemInfo.getGridLimits(),
                    problemInfo.getPeriodicalBoundary(), problemInfo.getSchemaStencil()) + NL;

            
            String conditions = "!" + IND + IND + "Initial conditions" + NL
                + "!" + IND + IND + "----------------" + NL;
            
            //Get the auxiliary definitions
            String query = "//mms:initialConditions/mms:auxiliarDefinitions/mms:instructionSet";
            NodeList auxDefinitions = CodeGeneratorUtils.find(doc, query);
            xslParams[1][1] =  "2";
            for (int i = 0; i < auxDefinitions.getLength(); i++) {
                Node instruction = auxDefinitions.item(i);
                conditions = conditions + CactusUtils.xmlTransform(instruction, xslParams) + NL;
            }
            
            //Initialization of the variables and setting the loop iteration over the grid
            xslParams[1][1] =  "0";
            String actualIndent = IND + IND;
            String coordIndex = "";
            for (int j = problemInfo.getCoordinates().size() - 1; j >= 0; j--) {
                //Initialization of the variables
                String coord = problemInfo.getCoordinates().get(j);
                localVariables = localVariables + IND + IND + coord + "Start = 1" + NL;
                localVariables = localVariables + IND + IND + coord + "End = cctk_lsh(" + (j + 1) + ")" + NL;
                //Loop iteration
                conditions =  conditions + actualIndent + "do " + coord + " = " + coord + "Start, " + coord + "End" + NL;
                actualIndent = actualIndent + IND;
                coordIndex =  problemInfo.getCoordinates().get(j) + ", " + coordIndex;
            }
            coordIndex = coordIndex.substring(0, coordIndex.lastIndexOf(","));
            
            //For every segment (first the lower precedents)
            for (int i = problemInfo.getRegions().size() - 1; i >= 0; i--) {
                String segmentName = problemInfo.getRegions().get(i);
                boolean hasInterior = problemInfo.getRegionIds().contains(String.valueOf((i * 2) + 1));
                boolean hasSurface = problemInfo.getRegionIds().contains(String.valueOf((i * 2) + 2));
                                
                //Segment condition before applying the conditions
                conditions = conditions + actualIndent + "if (";
                if (hasInterior) {
                    conditions = conditions + "FOV_" + ((i * 2) + 1) + "(" + coordIndex + ") .gt. 0";
                }
                if (hasInterior && hasSurface) {
                    conditions = conditions + " .or. ";
                }
                if (hasSurface) {
                    conditions = conditions + "FOV_" + ((i * 2) + 2) + "(" + coordIndex + ") .gt. 0";
                }
                conditions = conditions + ") then" + NL;
                actualIndent = actualIndent + IND;
                
                NodeList initConditions = CodeGeneratorUtils.find(doc, "//mms:initialConditions[preceding-sibling::" 
                        + "mms:segmentGroupName = '" + segmentName + "']/mms:initialCondition");
                for (int j = 0; j < initConditions.getLength(); j++) {
                    Node condition = initConditions.item(j);
                    NodeList equations = condition.getChildNodes();
                    int equationsFrom = 0;
                    
                    //If there is a condition
                    if (equations.item(0).getLocalName().equals("condition")) {
                        xslParams[0][1] =  "true";
                        equationsFrom = 1;
                        conditions = conditions + actualIndent + "if (" + CactusUtils.xmlTransform(
                                equations.item(0).getFirstChild().getFirstChild(), xslParams) + ") then" + NL;
                        actualIndent = actualIndent + IND;
                    }
                    xslParams[0][1] =  "false";
                    xslParams[1][1] = String.valueOf(actualIndent.length());
                    //Equations in the conditions
                    for (int k = equationsFrom; k < equations.getLength(); k++) {
                        NodeList maths = equations.item(k).getFirstChild().getChildNodes();
                        for (int l = 0; l < maths.getLength(); l++) {
                            conditions = conditions + CactusUtils.xmlTransform(
                                    maths.item(l), xslParams) + NL;
                        }
                    }
                    //Close if clause if exists
                    if (equationsFrom == 1) {
                        actualIndent = actualIndent.substring(0, actualIndent.lastIndexOf(IND));
                        conditions = conditions + actualIndent + "end if" + NL + NL;
                    }
                }

                //Close the segment condition
                actualIndent = actualIndent.substring(0, actualIndent.lastIndexOf(IND));
                conditions = conditions + actualIndent + "end if" + NL + NL;
            }
            
            conditions = conditions + initializeBoundaries(problemInfo, actualIndent);
            
            //close the loop iteration
            for (int j = 0; j < problemInfo.getCoordinates().size(); j++) {
                actualIndent = actualIndent.substring(0, actualIndent.lastIndexOf(IND));
                conditions =  conditions + actualIndent + "end do" + NL;
            }

            
            String content = "/*@@" + NL
                + "  @file      InitialConditions.F" + NL
                + "  @date      " + hi.getDate().toString() + NL      
                + "  @author    " + hi.getAuthor() + NL
                + "  @desc" + NL 
                + "             Initial conditions for " + hi.getProblemId() + NL
                + "  @enddesc" + NL 
                + "  @version " + hi.getVersion() + NL
                + "@@*/" + NL + NL
                + "#include \"cctk.h\"" + NL
                + "#include \"cctk_Parameters.h\"" + NL
                + "#include \"cctk_Arguments.h\"" + NL
                + "#include \"cctk_Functions.h\"" + NL + NL
                + IND + "subroutine " + hi.getProblemId() + "_initial(CCTK_ARGUMENTS)" + NL + NL
                + IND + IND + "implicit none" + NL + NL
                + IND + IND + "DECLARE_CCTK_ARGUMENTS" + NL
                + IND + IND + "DECLARE_CCTK_PARAMETERS" + NL
                + IND + IND + "DECLARE_CCTK_FUNCTIONS" + NL + NL
                + localVariables + NL + NL
                + conditions
                //Post-initialization synchronization
                + synchronization(problemInfo, hi.getProblemId())
                + IND + "end subroutine " + hi.getProblemId() + "_initial";
            
            return content;
        } 
        catch (CGException e) {
            throw e;
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new CGException(CGException.CG00X, e.getMessage());
        }
    }
    
    /**
     * Initialization of the boundaries that need to.
     * @param problemInfo       The problem info
     * @param actualIndent      The indentation for the code
     * @return                  The code
     * @throws CGException      CG00X External error
     */
    private static String initializeBoundaries(ProblemInfo problemInfo, String actualIndent) throws CGException {
        String result = "!" + actualIndent + "Boundaries Initialization" + NL;
        String ind = actualIndent + IND + IND;
        Document problem = problemInfo.getProblem();
        String[][] xslParams = new String [2][2];
        xslParams[0][0] =  "condition";
        xslParams[0][1] =  "true";
        xslParams[1][0] =  "indent";
        xslParams[1][1] =  "0";
        
        NodeList boundDefs = CodeGeneratorUtils.find(problem, "//mms:boundaryDefinition[descendant::mms:initialConditions]");
        for (int i = 0; i < boundDefs.getLength(); i++) {
            Element boundarySegmentGroups = (Element) boundDefs.item(i).getFirstChild();
            result = result + actualIndent + "if (" + CactusUtils.createBoundSegGroupCondition(problemInfo, boundarySegmentGroups) 
                    + ") then" + NL;
            NodeList boundConditions = CodeGeneratorUtils.find(boundDefs.item(i), ".//mms:boundaryCondition[descendant::" 
                    + "mms:initialConditions]");
            for (int j = 0; j < boundConditions.getLength(); j++) {
                String axis = ((Element) boundConditions.item(j)).getElementsByTagNameNS(MMSURI, "axis").item(0).getTextContent();
                String side = ((Element) boundConditions.item(j)).getElementsByTagNameNS(MMSURI, "side").item(0).getTextContent();
                result = result + actualIndent + IND + "if (" + CactusUtils.createBoundAxisSideCondition(problemInfo, axis, side) 
                        + ") then" + NL;
                //condition of the boundary
                if (((Element) boundConditions.item(j)).getElementsByTagNameNS(MMSURI, "condition").getLength() > 0) {
                    xslParams[0][1] =  "true";
                    Element condition = (Element) ((Element) boundConditions.item(j)).getElementsByTagNameNS(MMSURI, "condition").item(0);
                    result = result + actualIndent + IND + IND + "if (" + CactusUtils.xmlTransform(
                            condition.getFirstChild().getFirstChild(), xslParams) + ") then" + NL;
                }
                
                //Initial conditions
                NodeList initConditions = CodeGeneratorUtils.find(boundConditions.item(j), ".//mms:initialCondition");
                for (int k = 0; k < initConditions.getLength(); k++) {
                    Node condition = initConditions.item(k);
                    NodeList equations = condition.getChildNodes();
                    int equationsFrom = 0;
                    
                    //If there is a condition
                    if (equations.item(0).getLocalName().equals("condition")) {
                        xslParams[0][1] =  "true";
                        equationsFrom = 1;
                        result = result + ind + "if (" + CactusUtils.xmlTransform(
                                equations.item(0).getFirstChild().getFirstChild(), xslParams) + ") then" + NL;
                    }
                    xslParams[0][1] =  "false";
                    xslParams[1][1] = String.valueOf(ind.length());
                    //Equations in the conditions
                    for (int l = equationsFrom; l < equations.getLength(); l++) {
                        NodeList maths = equations.item(l).getFirstChild().getChildNodes();
                        for (int m = 0; m < maths.getLength(); m++) {
                            result = result + CactusUtils.xmlTransform(maths.item(m), xslParams) + NL;
                        }
                    }
                    //Close if clause if exists
                    if (equationsFrom == 1) {
                        result = result + ind + "end if" + NL;
                    }
                }
                
                //close condition of the boundary
                if (((Element) boundConditions.item(j)).getElementsByTagNameNS(MMSURI, "condition").getLength() > 0) {
                    result = result + actualIndent + IND + IND + "end if" + NL;
                }
                result = result + actualIndent + IND + "end if" + NL;
            }
            result = result + actualIndent + "end if" + NL;
        }
        
        return result;
    }
    
    /**
     * Creates the synchronization post initialization.
     * Needed by periodical and fixed boundary conditions.
     * @param pi                The problem information
     * @param problemId         The problem name
     * @return                  The code
     * @throws CGException      CG003 Not possible to create code for the platform
     *                          CG004 External error
     */
    private static String synchronization(ProblemInfo pi, String problemId) throws CGException {
        int dim = pi.getDimensions();
        Document doc = pi.getProblem();
        //Parameters for the xsl transformation. It is not a condition formula
        String[][] xslParams = new String [2][2];
        xslParams[0][0] =  "condition";
        xslParams[0][1] =  "true";
        xslParams[1][0] =  "indent";
        xslParams[1][1] =  "0";
        String index = "";
        String loop = "";
        String endLoop = "";
        String actualIndent = IND + IND;
        String conditions = "";
        for (int i = dim - 1; i >= 0; i--) {
            String coord = pi.getCoordinates().get(i);
            index = ", " + coord + index;
            loop = loop + actualIndent + "do " + coord + " = " + coord + "Start, " + coord + "End" + NL;
            endLoop = actualIndent + "end do" + NL + endLoop;
            actualIndent = actualIndent + IND;
        }
        index = index.substring(2);
        //Check if parabolic terms are used
        boolean parabolicTerms = CodeGeneratorUtils.find(doc, "//*[@type='parabolicTerm']").getLength() > 0;
        //Check if flat or maximal dissipation
        boolean orderBoundaries = CodeGeneratorUtils.find(doc, "//mms:boundaryCondition[mms:type='Flat' or " 
                + "mms:type='Maximal dissipation' or mms:type='Reflection']").getLength() > 0;
        //Conditions
        String extrapolationEndAssignments = "";
        for (int i = pi.getRegions().size() - 1; i >= 0; i--) {
            String segmentName = pi.getRegions().get(i);
            boolean boundPTerms = CodeGeneratorUtils.find(doc, "//mms:segmentGroup[" 
                    + "mms:segmentGroupName = '" + segmentName + "']//*[@type='parabolicTerm']").getLength() > 0;
            //Particles in reflection boundaries
            String query = "//mms:segmentGroup[mms:segmentGroupName = '" + segmentName + "']//mms:postInitialCondition";
            NodeList postInitialization = CodeGeneratorUtils.find(doc, query);
            for (int j = 0; j < postInitialization.getLength(); j++) {
                Element postInit = (Element) postInitialization.item(j).getFirstChild();
                Element child = (Element) postInit.getFirstChild();
                if (child.getLocalName().equals("interior")) {
                    conditions = conditions + CactusExecution.processBoundaries((Element) child.getFirstChild().cloneNode(true), pi, 
                                    segmentName + "I",  (i * 2) + 1, child.getLocalName(), orderBoundaries && parabolicTerms, boundPTerms);
                    extrapolationEndAssignments = extrapolationEndAssignments + extrapolationFinalization(pi, actualIndent, postInit, (i * 2) + 1);
                    if (postInit.getChildNodes().getLength() > 1) {
                        child = (Element) child.getNextSibling();
                    }
                }
                if (child.getLocalName().equals("surface")) {
                    conditions = conditions + CactusExecution.processBoundaries((Element) child.getFirstChild().cloneNode(true), pi, 
                                    segmentName + "S",  (i * 2) + 1, child.getLocalName(), orderBoundaries && parabolicTerms, boundPTerms);
                    extrapolationEndAssignments = extrapolationEndAssignments + extrapolationFinalization(pi, actualIndent, postInit, (i * 2) + 1);
                }
            }
        }
        //Field groups
        String fieldGroups = "";
        CactusVarGroupInfo cvgi = pi.getGroupInfo();
        Iterator<String> groupNames = cvgi.getVarGroups().keySet().iterator();
        boolean extrapolationSync = CodeGeneratorUtils.find(doc, "//mms:postInitialCondition//fieldExtrapolation").getLength() > 0;
        while (groupNames.hasNext()) {
            String fieldGroup = groupNames.next();
            if (fieldGroup.startsWith("fields_")) {
                fieldGroups = fieldGroups + IND + IND + "call CCTK_SyncGroup(status, cctkGH, \"" + problemId + "::" + fieldGroup + "\")" + NL;
            }
            if (extrapolationSync) {
                ArrayList<String> vars = cvgi.getVarGroups().get(fieldGroup).getVariables();
                for (int i = 0; i < vars.size() && extrapolationSync; i++) {
                    if (vars.get(i).contains("extrapolated")) {
                        fieldGroups = fieldGroups + IND + IND + "call CCTK_SyncGroup(status, cctkGH, \"" + problemId + "::" + fieldGroup + "\")" + NL;
                        extrapolationSync = false;
                    }
                }  
            }
        }
        if (conditions.equals("")) {
            return NL + "!       Post-initialization synchronization" + NL
                    + "!       ----------------" + NL
                    + fieldGroups + NL;
        }
        
        //If there are some boundaries (flat, reflection or maximal dissipation) the order of the boundary is important. 
        //The route over the domain is decomposed.
        String boundaryInitConds = "";
        if (orderBoundaries && parabolicTerms) {
            boundaryInitConds = CactusUtils.createFlatLoopStructure(pi, conditions);
        }
        else {
            boundaryInitConds = loop + conditions + endLoop;
        }
        
        String extrapolationInit = "";
        String extrapolationEnd = "";
        boolean extrapolation = CodeGeneratorUtils.find(doc, "//mms:postInitialCondition//fieldExtrapolation").getLength() > 0;
        if (extrapolation) {
            extrapolationInit = loop
                    + extrapolationInitialization(pi, IND + IND)
                    + endLoop;
            extrapolationEnd = loop
                    + extrapolationEndAssignments
                    + endLoop;
        }
        
        return NL + "!       Post-initialization synchronization" + NL
                + "!       ----------------" + NL
                + extrapolationInit
                + boundaryInitConds
                + extrapolationEnd
                + fieldGroups + NL;
    }
    
    /**
     * Create the initialization for the extrapolation block.
     * @param pi                The problem information
     * @param indent            The actual indent
     * @return                  The code
     * @throws CGException      CG00X External error
     */
    private static String extrapolationInitialization(ProblemInfo pi, String indent) throws CGException {
        String result = "";
        Document doc = pi.getProblem();
        String index = "";
        for (int i = 0; i < pi.getDimensions(); i++) {
            index = index + pi.getCoordinates().get(i) + ", ";
        }
        index = index.substring(0, index.length() - 2);
        ArrayList<String> variables = new ArrayList<String>();
        NodeList blockList = CodeGeneratorUtils.find(doc, "//mms:postInitialCondition//fieldExtrapolation");
        for (int i = 0; i < blockList.getLength(); i++) {
            Element variable = (Element) blockList.item(i);
            String field = variable.getAttribute("field");
            if (variable.getFirstChild().getNodeName().equals("auxiliaryFieldExtrapolation")) {
                variable = (Element) variable.getFirstChild();
            }
            for (int j = 0; j < pi.getDimensions(); j++) {
                String coord = pi.getCoordinates().get(j);
                if (!variables.contains("extrapolatedsub" + field + "sup" + coord)) {
                    variables.add("extrapolatedsub" + field + "sup" + coord);
                    result = result + indent + "extrapolatedsub" + CactusUtils.variableNormalize(field) + "sup" + coord + "(" 
                            + index + ") = " + CactusUtils.variableNormalize(field) + "(" + index + ")" + NL;
                }    
            }
        }
        return result;
    }
    
    /**
     * Create the finalization instructions after an extrapolation & boundaries. After a boundary calculation the extrapolation axis variables
     * must be reassigned for the boundaries that does not calculate them. 
     * @param pi                    The problem info.
     * @param indent                The actual indent  
     * @param boundary              The boundary xml code
     * @param segmentBaseNumber     The segment base number
     * @return                      The code
     * @throws CGException          CG00X External error
     */
    private static String extrapolationFinalization(ProblemInfo pi, String indent, Node boundary, int segmentBaseNumber) throws CGException {
        String result = "";
        String index = "";
        for (int i = 0; i < pi.getDimensions(); i++) {
            index = index + pi.getCoordinates().get(i) + ", ";
        }
        index = index.substring(0, index.length() - 2);
        ArrayList<String> fields = new ArrayList<String>();
        NodeList fieldList = CodeGeneratorUtils.find(pi.getProblem(), "/*/mms:field|/*/mms:auxiliarField");
        for (int i = 0; i < fieldList.getLength(); i++) {
            fields.add(fieldList.item(i).getTextContent());
        }
       
      
        for (int i = 0; i < pi.getDimensions(); i++) {
            String coordString = pi.getCoordinates().get(i);
            if (pi.getPeriodicalBoundary().get(coordString).equals("noperiodical")) {
                ArrayList<String> candidates = new ArrayList<String>();
                candidates.addAll(fields);
                NodeList extrapolatedFields = CodeGeneratorUtils.find(boundary, ".//" + coordString + "/lower//fieldExtrapolation/@field");
                for (int j = 0; j < extrapolatedFields.getLength(); j++) {
                    candidates.remove(extrapolatedFields.item(j).getTextContent());
                }
                if (!candidates.isEmpty()) {
                    String axis = CodeGeneratorUtils.discToCont(pi.getProblem(), coordString);
                    int boundId = pi.getCoordinates().indexOf(CodeGeneratorUtils.contToDisc(pi.getProblem(), axis)) * 2 + 1;
                    result = result + indent + "if ((FOV_" + CodeGeneratorUtils.getRegionName(pi.getProblem(), -boundId, 
                            pi.getCoordinates()) + "(" + index + ") .gt. 0) .and. (" + CactusUtils.getLeftCondition(false, 
                                    pi.getSchemaStencil(), segmentBaseNumber, i, pi.getCoordinates(), pi.getRegionIds()) + ")) then" + NL;
                    for (int k = 0; k < candidates.size(); k++) {
                        String field = candidates.get(k);
                        for (int l = 0; l < pi.getDimensions(); l++) {
                            String coord = pi.getCoordinates().get(l);
                            result = result + indent + IND + "extrapolatedsub" + CactusUtils.variableNormalize(field) + "sup" 
                                    + coord + "(" + index + ") = " + CactusUtils.variableNormalize(field) + "(" + index 
                                    + ")" + NL;
                        }
                    }
                    result = result + indent + "end if" + NL;
                }
                
                
                candidates = new ArrayList<String>();
                candidates.addAll(fields);
                extrapolatedFields = CodeGeneratorUtils.find(boundary, ".//" + coordString + "/upper//fieldExtrapolation/@field");
                for (int j = 0; j < extrapolatedFields.getLength(); j++) {
                    candidates.remove(extrapolatedFields.item(j).getTextContent());
                }
                if (!candidates.isEmpty()) {
                    String axis = CodeGeneratorUtils.discToCont(pi.getProblem(), coordString);
                    int boundId = pi.getCoordinates().indexOf(CodeGeneratorUtils.contToDisc(pi.getProblem(), axis)) * 2 + 2;
                    result = result + indent + "if ((FOV_" + CodeGeneratorUtils.getRegionName(pi.getProblem(), -boundId, 
                            pi.getCoordinates()) + "(" + index + ") .gt. 0) .and. (" + CactusUtils.getRightCondition(false, 
                                    pi.getSchemaStencil(), segmentBaseNumber, i, pi.getCoordinates(), pi.getRegionIds()) + ")) then" + NL;
                    for (int k = 0; k < candidates.size(); k++) {
                        String field = candidates.get(k);
                        for (int l = 0; l < pi.getDimensions(); l++) {
                            String coord = pi.getCoordinates().get(l);
                            result = result + indent + IND + "extrapolatedsub" + CactusUtils.variableNormalize(field) + "sup" 
                                    + coord + "(" + index + ") = " + CactusUtils.variableNormalize(field) + "(" + index 
                                    + ")" + NL;
                        }
                    }
                    result = result + indent + "end if" + NL;
                }
            }
        }
        
        return result;
    }
}
