/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.codeGeneratorPlugin.utils;

import java.util.Date;

/**
 * Common header information for all the files.
 * @author bminyano
 *
 */
public class HeaderInfo {

    private String problemId;
    private String author;
    private String version;
    private Date date;
    
    /**
     * Constructor.
     * @param pId           The problem id 
     * @param author        The author
     * @param version       The version
     * @param date          The date
     */
    public HeaderInfo(String pId, String author, String version, Date date) {
        problemId = pId;
        this.author = author;
        this.version = version;
        this.date = date;
    }

    public String getProblemId() {
        return problemId;
    }

    public String getAuthor() {
        return author;
    }

    public String getVersion() {
        return version;
    }

    public Date getDate() {
        return date;
    }
}
