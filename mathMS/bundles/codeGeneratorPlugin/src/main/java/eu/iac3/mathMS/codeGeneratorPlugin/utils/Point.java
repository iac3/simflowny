/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.codeGeneratorPlugin.utils;

/**
 * Point in the mesh.
 * @author bminyano
 *
 */
public class Point {

    private double[] coordinate;
    private int dimension;
    static final int THREE = 3;

    /**
     * Point constructor.
     * @param point     X3d point string
     */
    public Point(String point) {
        String[] coord = point.trim().split(" +");
        dimension = coord.length;
        coordinate = new double[coord.length];
        for (int i = 0; i < dimension; i++) {
            coordinate[i] = Float.parseFloat(coord[i]);
        }
    }

    /**
     * 3D point constructor.
     * @param a     point a
     * @param b     point b
     * @param c     point b
     */
    public Point(double a, double b, double c) {
        dimension = THREE;
        coordinate = new double[THREE];
        coordinate[0] = a;
        coordinate[1] = b;
        coordinate[2] = c;
    }
    
    public double[] getCoordinate() {
        return coordinate;
    }
    
    /**
     * Get a point in a given index.
     * @param index     The index
     * @return          The point at the index
     */
    public double getPoint(int index) {
        return coordinate[index];
    }

    public int getDimension() {
        return dimension;
    }
    
}
