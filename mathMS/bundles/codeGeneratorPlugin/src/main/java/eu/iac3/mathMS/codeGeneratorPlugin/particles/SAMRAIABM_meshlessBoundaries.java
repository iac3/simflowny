/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.codeGeneratorPlugin.particles;

import eu.iac3.mathMS.codeGeneratorPlugin.CGException;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.CodeGeneratorUtils;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.ProblemInfo;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.SAMRAIUtils;

import java.util.ArrayList;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * Creates the boundaries for the Agente based model problems.
 * @author bminano
 *
 */
public final class SAMRAIABM_meshlessBoundaries {
    static final String NL = System.getProperty("line.separator");
    static final String IND = "\u0009";
    static String mmsUri = "urn:mathms";
    static String mtUri = "http://www.w3.org/1998/Math/MathML";
    /**
     * Private constructor.
     */
    private SAMRAIABM_meshlessBoundaries() { };
    
    /**
     * Creates the boundaries for the Agente based model problems.
     * @param actualIndent      The indentation
     * @param pi                The problem information
     * @param indexCell         The index code to access cell information
     * @return                  The code
     * @throws CGException      CG00X External error
     */
    public static String createBoundaries(String actualIndent, ProblemInfo pi, String indexCell) throws CGException {
        int dimensions = pi.getDimensions();
        
        String result = "";
        //Common Loop and initialization
        result = result + createExecutionBlockLoop(actualIndent, dimensions);
        for (int j = 0; j < dimensions; j++) {
            actualIndent = IND + actualIndent;
        }
        result = result + actualIndent + "Particles* part = particleVariables->getItem(idx);" + NL;
        result = result + createParticleAccess(actualIndent, dimensions);
        actualIndent = actualIndent + IND;
        
        //TODO hacer multiregiono
        String bound = generateBoundaryCode(actualIndent, pi, 0, indexCell);
        if (bound.equals("")) {
            return "";
        }
        result = result + bound;
        
        actualIndent = actualIndent.substring(1);
        result =  result + actualIndent + "}" + NL;
        result = result + createExecutionBlockLoopEnd(actualIndent, dimensions);
        actualIndent = actualIndent.substring(dimensions);
        
        return result;
    }
    
    /**
     * Creates the boundaries for the Agente based model problems.
     * @param indent            The indentation
     * @param pi                The problem information
     * @param regionNum        The region number
     * @param indexCell         The index code to access cell information
     * @return                  The code
     * @throws CGException      CG00X External error
     */
    private static String generateBoundaryCode(String indent, ProblemInfo pi, int regionNum, String indexCell) throws CGException {
        String result = "";
        Document problem = pi.getProblem();
        
        //Only for boundary regions
        if (CodeGeneratorUtils.find(problem, "//mms:boundaryCondition").getLength() > 0) {
            ArrayList<String> coords = pi.getCoordinates();
            for (int i = 0; i < coords.size(); i++) {
                String query = "//mms:boundaryCondition[mms:axis ='" 
                    + coords.get(i) + "' or lower-case(mms:axis) ='all']";
                NodeList boundList = CodeGeneratorUtils.find(problem, query);
                //Axis iteration
                for (int k = 0; k < boundList.getLength(); k++) {
                    Element boundary = (Element) boundList.item(k);
                    String type = boundary.getElementsByTagNameNS(mmsUri, "type").item(0).getFirstChild().getLocalName();
                    if (!type.toLowerCase().equals("periodical")) {
                        String side = boundary.getElementsByTagNameNS(mmsUri, "side").item(0).getTextContent();
                        //Lower
                        if (side.toLowerCase().equals("all") || side.toLowerCase().equals("lower")) {
                            generateBoundarySide(indent, i * 2 + 1, regionNum, boundary, pi);
                        }
                        //Upper
                        if (side.toLowerCase().equals("all") || side.toLowerCase().equals("upper")) {
                            generateBoundarySide(indent, i * 2 + 2, regionNum, boundary, pi);
                        }
                    }
                }
            }
        }
        return result;
    }
    
    /**
     * Generates a side boundary.
     * @param indent            The indentation
     * @param pi                The problem information
     * @param regionNum        The region number
     * @param boundId           The boundary id
     * @param boundary          The boundary condition
     * @return                  Code
     * @throws CGException      CG00X External error
     */
    private static String generateBoundarySide(String indent, int boundId, int regionNum, Element boundary, ProblemInfo pi) throws CGException {
        String[][] xslParams = new String [5][2];
        xslParams[0][0] = "condition";
        xslParams[0][1] = "false";
        xslParams[1][0] = "indent";
        xslParams[1][1] = String.valueOf(indent.length());
        xslParams[2][0] = "dimensions";
        xslParams[2][1] = String.valueOf(pi.getDimensions());
        xslParams[3][0] = "coords";
        xslParams[3][1] = pi.getCoordinates().toString();
        xslParams[4][0] = "timeCoord";
        xslParams[4][1] = pi.getTimeCoord();
        
        String result = indent + "if (particle->region == -" + boundId + ") {" + NL;
        indent = indent + IND;
        //if there is a condition generate the if element
        boolean condition = false;
        if (CodeGeneratorUtils.find(boundary, "./mms:applyIf").getLength() > 0) {
            condition = true;
            xslParams[0][1] = "true";
            result = result + indent + SAMRAIUtils.xmlTransform(
                    CodeGeneratorUtils.find(boundary, ".//mms:applyIf").item(0), xslParams);
            indent = indent + IND;
        }
        //Process boundary
        boolean deleted = 
                CodeGeneratorUtils.find(boundary, ".//sml:algorithm//sml:deleteAgent").getLength() > 0;
        NodeList instructions = CodeGeneratorUtils.find(boundary, ".//sml:algorithm/*");
        for (int j = 0; j < instructions.getLength(); j++) {
            xslParams[0][1] = "false";
            xslParams[1][1] = String.valueOf(indent.length());
            result = result + SAMRAIUtils.xmlTransform(instructions.item(j), xslParams);
        }
        //Add newRegion value
        if (!result.equals("") && !deleted) {
            result = result + indent + "particle->newRegion = " + ((regionNum * 2) + 1) + ";" + NL;
        }
        if (condition) {
            indent = indent.substring(1);
            result = result + indent + "}" + NL;
        }
        indent = indent.substring(1);
        result = result + indent + "}" + NL;
        return result;
    }
    /**
     * Creates the loop for an execution block.
     * @param indent                The indentation
     * @param dimensions            The spatial dimensions
     * @return                      The loop
     */
    public static String createExecutionBlockLoop(String indent, int dimensions) {
        String block = "";
        String coordIndex = "";
        
        String actualIndent = indent;
        for (int i = dimensions - 1; i >= 0; i--) {
            //Loop iteration
            block =  block + actualIndent + "for (int index" + i + " = boxfirst(" + i + "); index" + i + " <= boxlast(" + i + "); index" + i 
                + "++) {" + NL;
            actualIndent = actualIndent + IND;
            coordIndex =  "index" + i + ", " + coordIndex;
        }
        coordIndex = coordIndex.substring(0, coordIndex.lastIndexOf(","));
        
        return block + actualIndent + "hier::Index idx(" + coordIndex + ");" + NL;
    }
    
    /**
     * Creates the loop end for an execution block.
     * @param actualIndent      The actual indent
     * @param dimensions        The spatial dimensions
     * @return                  The code
     */
    public static String createExecutionBlockLoopEnd(String actualIndent, int dimensions) {
        String block = "";
        for (int i = 0; i < dimensions; i++) {
            actualIndent = actualIndent.substring(1);
            block =  block + actualIndent + "}" + NL;
        }
        
        return block;
    }
    
    /**
     * Create the code to access the data inside the particles.
     * @param actualIndent      The actual indentation
     * @param dimensions        The number of spatial dimensions
     * @return                  The code
     */
    public static String createParticleAccess(String actualIndent, int dimensions) {
        String particleAccess = "";
        //Access index data and iterate over all the particles in the Particles data
        particleAccess = particleAccess + actualIndent + "for (int pit = part->getNumberOfParticles() - 1; pit >= 0; pit--) {" + NL;
        actualIndent = actualIndent + IND;
        particleAccess = particleAccess + actualIndent + "Particle* particle = part->getParticle(pit);" + NL;
        return particleAccess;
    }
}
