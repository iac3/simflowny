package eu.iac3.mathMS.codeGeneratorPlugin.SAMRAI;

import java.util.ArrayList;

/**
 * x3d regions with fixed movement info class.
 * @author bminano
 *
 */
public class X3DMovInfo {
	private String type;
    private ArrayList<String> name;
    private int timeslices;
    private ArrayList<Integer> size;
    
    /**
     * Public constructor.
     * @param type			Type
     * @param name          Name
     * @param timeslices    Timeslices
     * @param size          Size
     */
    public X3DMovInfo(String type, ArrayList<String> name, int timeslices, ArrayList<Integer> size) {
    	this.type = type;
        this.name = new ArrayList<String>();
        this.name.addAll(name);
        this.timeslices = timeslices;
        this.size = new ArrayList<Integer>();
        this.size.addAll(size);
    }
    
    /**
     * Default constructor.
     * @param object    X3DMovInfo
     */
    public X3DMovInfo(X3DMovInfo object) {
    	this.type = object.type;
        this.name = new ArrayList<String>();
        this.name.addAll(object.getName());
        this.timeslices = object.timeslices;
        this.size = new ArrayList<Integer>();
        this.size.addAll(object.getSize());
    }

    public ArrayList<String> getName() {
        return name;
    }

    public void setName(ArrayList<String> name) {
        this.name = name;
    }

    public int getTimeslices() {
        return timeslices;
    }

    public void setTimeslices(int timeslices) {
        this.timeslices = timeslices;
    }

    public ArrayList<Integer> getSize() {
        return size;
    }

    public void setSize(ArrayList<Integer> size) {
        this.size = size;
    }

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
    
}
