/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.codeGeneratorPlugin.fvm;

import eu.iac3.mathMS.codeGeneratorPlugin.CGException;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.CodeGeneratorUtils;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.ProblemInfo;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.SAMRAIUtils;

import java.util.ArrayList;

import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Initializations for FVM code in SAMRAI.
 * @author bminyano
 *
 */
public final class SAMRAIABM_meshInitializations {
    static final String NL = System.getProperty("line.separator");
    static final String IND = "\u0009";
    static final int THREE = 3;
    static final String MTURI = "http://www.w3.org/1998/Math/MathML";
    static final String MMSURI = "urn:mathms";
    
    /**
     * Private constructor.
     */
    private SAMRAIABM_meshInitializations() { };
    
    /**
     * Creates the file for the initialization.
     * @param pi                    The problem information
     * @return                      The file
     * @throws CGException          CG004 External error
     */
    public static String create(ProblemInfo pi) throws CGException {
        String result;
        Document doc = pi.getProblem();
        
        //Parameters for the xsl transformation. It is not a condition formula
        String[][] xslParams = new String [4][2];
        xslParams[0][0] =  "condition";
        xslParams[0][1] =  "false";
        xslParams[1][0] =  "indent";
        xslParams[1][1] =  "0";
        xslParams[2][0] =  "dimensions";
        xslParams[2][1] =  String.valueOf(pi.getDimensions());
        xslParams[3][0] = "timeCoord";
        xslParams[3][1] = pi.getTimeCoord();
        
        Element coordinates = CodeGeneratorUtils.createElement(pi.getProblem(), null, "coords");
        for (int i = 0; i < pi.getDimensions(); i++) {
            String coord = pi.getCoordinates().get(i);
            Element coordE = CodeGeneratorUtils.createElement(pi.getProblem(), null, coord);
            coordE.setTextContent(coord);
            coordinates.appendChild(coordE);  
        }
        String nodePar = "coords";
        
        int dimensions = pi.getSpatialDimensions();
        try {
            result = "//Get fields, auxiliary fields and local variables that are going to be used." + NL;
                
            //Declaration of variables
            DocumentFragment scope = doc.createDocumentFragment();
            NodeList fc = CodeGeneratorUtils.find(doc, "//mms:initialCondition");
            for (int i = 0; i < fc.getLength(); i++) {
                scope.appendChild(fc.item(i).cloneNode(true));
            }
            result = result + SAMRAIUtils.getPDEVariableDeclaration(scope, pi, 0, new ArrayList<String>(), 
            		"patch.", true, false) + NL;
            
            result = result + "//Get the dimensions of the patch" + NL 
                + "hier::Box pbox = patch.getBox();" + NL
                + SAMRAIUtils.createFOVDeclaration(pi.getRegionIds(), "", "patch.")
                + "const hier::Index boxfirst = patch.getBox().lower();" + NL
                + "const hier::Index boxlast  = patch.getBox().upper();" + NL + NL
                + "//Get delta spaces into an array. dx, dy, dz." + NL
                + "const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch.getPatchGeometry()));" + NL
                + "const double* dx  = patch_geom->getDx();" + NL + NL;
          
            //Do the condition
            //Initialization of the variables and setting the loop iteration
            for (int i = 0; i < dimensions; i++) {
                //Initialization of the variables
                String coordString = pi.getCoordinates().get(i);
                result =  result + "int " + coordString + "last = boxlast(" + i + ")-boxfirst(" + i + ") + 2 + 2 * d_ghost_width;" + NL;
            }
            String actualIndent = "";
            String coordIndex = "";
            for (int i = dimensions - 1; i >= 0; i--) {
                String coordString = pi.getCoordinates().get(i);
                //Loop iteration
                result =  result + actualIndent + "for(int index" + i + " = 0; index"
                        + i + " < " + coordString + "last; index" + i + "++) {" + NL;
                actualIndent = actualIndent + IND;
                coordIndex =  "index" + i + ", " + coordIndex;
            }
            coordIndex = coordIndex.substring(0, coordIndex.lastIndexOf(","));
                                
            //Region condition before applying the conditions
            result = result + actualIndent + "if (vector(FOV_1, " + coordIndex + ") > 0) {" + NL;
            actualIndent = actualIndent + IND;
            
            NodeList initConditions = CodeGeneratorUtils.find(doc, "//mms:initialConditions/mms:initialCondition");
            for (int j = 0; j < initConditions.getLength(); j++) {
                Node condition = initConditions.item(j);
                NodeList mathExpressions = condition.getChildNodes();
                boolean applyIf = false;
                //If there is a condition
                if (mathExpressions.item(0).getLocalName().equals("applyIf")) {
                    xslParams[0][1] =  "true";
                    applyIf = true;
                    result = result + actualIndent + "if (" + SAMRAIUtils.xmlTransform(
                            SAMRAIUtils.preProcessPDEBlock(mathExpressions.item(0).getFirstChild().getFirstChild(), pi, false, false), 
                            xslParams, nodePar, coordinates) + ") {" + NL;
                    actualIndent = actualIndent + IND;
                }
                xslParams[0][1] =  "false";
                xslParams[1][1] = String.valueOf(actualIndent.length());
                //Equations in the initial conditions
                mathExpressions = mathExpressions.item(mathExpressions.getLength() - 1).getChildNodes();
                for (int k = 0; k < mathExpressions.getLength(); k++) {
                    Node math = mathExpressions.item(k);
                        result = result + SAMRAIUtils.xmlTransform(SAMRAIUtils.preProcessPDEBlock(math, pi, false, false), 
                                xslParams, nodePar, coordinates);
                }
                //Close if clause if exists
                if (applyIf) {
                    actualIndent = actualIndent.substring(0, actualIndent.lastIndexOf(IND));
                    result = result + actualIndent + "}" + NL + NL;
                }
            }

            //Close the region condition
            actualIndent = actualIndent.substring(0, actualIndent.lastIndexOf(IND));
            result = result + actualIndent + "}" + NL + NL;
            
            //close the loop iteration
            for (int i = 0; i < dimensions; i++) {
                actualIndent = actualIndent.substring(0, actualIndent.lastIndexOf(IND));
                result =  result + actualIndent + "}" + NL;
            }
            result = result + NL;
            //Post-initialization synchronization
            result = result + synchronization(pi);
            return result;
        }
        catch (CGException e) {
            throw e;
        }
        catch (Exception e) {
            throw new CGException(CGException.CG00X, e.getMessage());
        } 
    }
    
    
    /**
     * Creates the synchronization post initialization.
     * Needed by periodical and fixed boundary conditions.
     * @param pi                The problem information
     * @return                  The code
     * @throws CGException      CG003 Not possible to create code for the platform
     *                          CG00X External error
     */
    private static String synchronization(ProblemInfo pi) throws CGException {
        int dim = pi.getDimensions();
        Document doc = pi.getProblem();
        //Parameters for the xsl transformation. It is not a condition formula
        String[][] xslParams = new String [THREE][2];
        xslParams[0][0] =  "condition";
        xslParams[0][1] =  "true";
        xslParams[1][0] =  "indent";
        xslParams[1][1] =  "0";
        xslParams[2][0] =  "dimensions";
        xslParams[2][1] =  String.valueOf(dim);
        String iterators = "";
        for (int i = dim - 1; i >= 0; i--) {
            String coord = pi.getCoordinates().get(i);
            iterators = IND + IND + IND + "int " + coord + "last = boxlast(" + i + ")-boxfirst(" + i + ") + 2 + 2 * d_ghost_width;" + NL + iterators;
        }
        //Conditions
        String boundaryInitConds = SAMRAIABM_meshBoundaries.createBoundaries(IND + IND + IND, pi);
        if (boundaryInitConds.equals("")) {
            return "#postInitSync#" + NL
                + IND + IND + "d_bdry_fill_init->createSchedule(level,this)->fillData(init_data_time, false);" + NL; 
        }
        //Declaration of variables
        String fields = "";
        DocumentFragment scope = doc.createDocumentFragment();
        NodeList fc = CodeGeneratorUtils.find(doc, "//mms:boundaryCondition//sml:algorithm");
        for (int i = 0; i < fc.getLength(); i++) {
            scope.appendChild(fc.item(i).cloneNode(true));
        }
        fields = fields + SAMRAIUtils.getPDEVariableDeclaration(scope, pi, 3, new ArrayList<String>(), 
        		"patch->", false, false) + NL;
        
        return "#postInitSync#" + NL
            + IND + IND + "double current_time = init_data_time;" + NL
            + IND + IND + "for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {" + NL
            + IND + IND + IND + "const std::shared_ptr< hier::Patch >& patch = *p_it;" + NL
            + SAMRAIUtils.createFOVDeclaration(pi.getRegionIds(), IND + IND + IND, "patch->")
            + fields
            + IND + IND + IND + "//Get the dimensions of the patch" + NL
            + IND + IND + IND + "const hier::Index boxfirst = patch->getBox().lower();" + NL
            + IND + IND + IND + "const hier::Index boxlast = patch->getBox().upper();" + NL + NL
            + IND + IND + IND + "//Get delta spaces into an array. dx, dy, dz." + NL
            + IND + IND + IND + "const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));" + NL
            + IND + IND + IND + "const double* dx  = patch_geom->getDx();" + NL
            + iterators
            + boundaryInitConds
            + IND + IND + "}" + NL
            + IND + IND + "d_bdry_fill_init->createSchedule(level,this)->fillData(init_data_time, false);" + NL;
    }
    
    /**
     * Create the condition for a boundary depending on its axis and side.
     * @param pi                The problem info
     * @param axis              The axis of the boundary
     * @param side              The side of the boundary
     * @return                  The condition
     * @throws CGException      CG00X External error
     */
    public static String createBoundAxisSideCondition(ProblemInfo pi, String axis, String side) throws CGException {
        String result = "";
        ArrayList<String> coordinates = pi.getCoordinates();
        String coordIndex = "";
        for (int j = coordinates.size() - 1; j >= 0; j--) {
            coordIndex = coordinates.get(j) + ", " + coordIndex;
        }
        coordIndex = coordIndex.substring(0, coordIndex.lastIndexOf(","));
        
        ArrayList<String> axisIds = new ArrayList<String>();
        if (axis.toLowerCase().equals("all")) {
            for (int i = 0; i < coordinates.size(); i++) {
                axisIds.add(CodeGeneratorUtils.discToCont(pi.getProblem(), coordinates.get(i)));
            }
        }
        else {
            axisIds.add(axis); 
        }
        ArrayList<String> boundIds = new ArrayList<String>();
        for (int i = 0; i < axisIds.size(); i++) {
            if (side.toLowerCase().equals("all") || side.toLowerCase().equals("lower")) {
                boundIds.add(axisIds.get(i) + "Lower");
            }
            if (side.toLowerCase().equals("all") || side.toLowerCase().equals("upper")) {
                boundIds.add(axisIds.get(i) + "Upper");
            }
        }
        for (int i = 0; i < boundIds.size(); i++) {
            result = result + "vector(FOV_" + boundIds.get(i) + ", " + coordIndex + ") > 0 || ";
        }
        result = result.substring(0, result.lastIndexOf(" ||"));
        
        return result;
    }
}
