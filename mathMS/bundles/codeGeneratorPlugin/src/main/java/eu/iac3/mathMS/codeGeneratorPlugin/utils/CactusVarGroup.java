/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.codeGeneratorPlugin.utils;

import java.util.ArrayList;

/**
 * This class represents a cactus group of variables.
 * @author bminyano
 *
 */
public class CactusVarGroup {

    private String name;
    private boolean fieldGroup;
    private ArrayList<String> variables;
    private int timeLevels;
    
    
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getTimeLevels() {
        return timeLevels;
    }
    public void setTimeLevels(int timeLevels) {
        this.timeLevels = timeLevels;
    }
    public boolean isFieldGroup() {
        return fieldGroup;
    }
    public void setFieldGroup(boolean fieldGroup) {
        this.fieldGroup = fieldGroup;
    }
    public ArrayList<String> getVariables() {
        return variables;
    }
    public void setVariables(ArrayList<String> fields) {
        this.variables = fields;
    }
    /**
     * Append a variable to the group.
     * @param variable  The variable to add.
     */
    public void appendVariable(String variable) {
        variables.add(variable);
    }
    
    /**
     * Compares two groups.
     * @param cvg   The group to compare to
     * @return      true if are equals
     */
    public boolean same(CactusVarGroup cvg) {
        if (!(fieldGroup == cvg.isFieldGroup())) {
            return false;
        }
        if (!(timeLevels == cvg.getTimeLevels())) {
            return false;
        }
        ArrayList<String> varsB = new ArrayList<String>();
        varsB.addAll(cvg.getVariables());
        ArrayList<String> varsA = new ArrayList<String>();
        varsA.addAll(variables);
        if (varsA.size() != varsB.size()) {
            return false;
        }
        for (int i = varsA.size() - 1; i >= 0; i--) {
            if (varsB.contains(varsA.get(i))) {
                varsB.remove(varsA.get(i));
                varsA.remove(i);
            }
            else {
                return false;
            }
        }
        return true;
    }
    
    /**
     * Tests if the groups overlaps some variable.
     * @param cvg   The group to compare to
     * @return      True if some variable is shared
     */
    public boolean overlaps(CactusVarGroup cvg) {
        ArrayList<String> varsB = new ArrayList<String>();
        varsB.addAll(cvg.getVariables());
        ArrayList<String> varsA = new ArrayList<String>();
        varsA.addAll(variables);
        for (int i = 0; i < varsA.size(); i++) {
            if (varsB.contains(varsA.get(i))) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Gets the similarity between groups. Groups are more similar when few different variables. 
     * @param cvg   The group to compare to
     * @return      Numbers near to 0 are more similars
     */
    public float getSimilarity(CactusVarGroup cvg) {
        ArrayList<String> varsB = new ArrayList<String>();
        varsB.addAll(cvg.getVariables());
        ArrayList<String> varsA = new ArrayList<String>();
        varsA.addAll(variables);
        int total = varsA.size() + varsB.size();
        for (int i = varsA.size() - 1; i >= 0; i--) {
            if (varsB.contains(varsA.get(i))) {
                varsB.remove(varsA.get(i));
                varsA.remove(i);
            }
        }
        int different = varsA.size() + varsB.size();
        
        return different / total;
    }
    
    /**
     * Check if the variable is in the group.
     * @param variable      The variable to check
     * @return              true if it is contained
     */
    public boolean hasVariable(String variable) {
        return variables.contains(variable);
    }
}
