package eu.iac3.mathMS.codeGeneratorPlugin.graph;

import eu.iac3.mathMS.codeGeneratorPlugin.CGException;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.CodeGeneratorUtils;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.ProblemInfo;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.SAMRAIUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Class to create the code for graphs in boost.
 * @author bminano
 *
 */
public final class BOOSTABM_graphCode {

    static String mtUri = "http://www.w3.org/1998/Math/MathML";
    static String smlUri = "urn:simml";
    static final String IND = "\u0009";
    static final String NL = System.getProperty("line.separator"); 
    static final String XSL = "common" + File.separator + "XSLTABM_graphToBoost" + File.separator + "instructionToBoost.xsl";
    static Transformer transformer = null;
    static TransformerFactory tFactory = null;
    
    /**
     * Private default constructor.
     */
    static {
        try {
            tFactory = new net.sf.saxon.TransformerFactoryImpl();
            transformer = tFactory.newTransformer(new StreamSource(XSL));
        } 
        catch (TransformerConfigurationException e) {
            e.printStackTrace();
        }
    }
    
    
    /**
     * Parses a xml with a xlst.
     * @param xml               The xml to parse.
     * @param params            Params to send to the xsl.
     * @return                  the result to apply the xlt to the xml.
     * @throws CGException      CG00X External error
     */
    public static String xmlTransform(Node xml, String[][] params) throws CGException {  
        try {
            ByteArrayOutputStream output = new ByteArrayOutputStream();

            // Set the parameters
            if (params != null) {
                for (int i = 0; i < params.length; i++) {
                    transformer.setParameter(params[i][0], params[i][1]);
                }   
            }
            DOMSource domSource = new DOMSource(xml);
            transformer.transform(domSource, new StreamResult(output));
            return output.toString("UTF-8");
        } 
        catch (Exception e) {
            throw new CGException(CGException.CG00X, e.getMessage());
        }
    }
    
    /**
     * Creates the code for a graph problem.
     * @param pi                The problem Info
     * @return                  The code
     * @throws CGException      CG00X External error
     */
    public static String create(ProblemInfo pi) throws CGException {
        String outputFields = "";
        String intFieldInitDec = "";
        String intFieldDec = "";
        String intFieldParam = "";
        String intFieldSer = "";
        String intFieldReadInit = "";
        String dpIn = "";
        String lastField = "";
        String dp = "";
        String properties = IND + "typedef  property_map<Graph, vertex_index_t>::type id_map;" + NL
                + IND + "id_map id = get(vertex_index, g_id);" + NL;
        String propertiesRead = IND + "property_map<Graph_read, vertex_index_t>::type id_read = get(vertex_index, g_read);" + NL;
        String propertiesDist = "";
        for (int i = 0; i < pi.getEdgeFields().size(); i++) {
            String intField = SAMRAIUtils.variableNormalize(pi.getEdgeFields().get(i));
            outputFields = outputFields + "output_" + intField + ", ";
            intFieldInitDec = intFieldInitDec + IND + IND + intField + " = 0;" + NL;
            intFieldDec = intFieldDec + IND + "double " + intField + ";" + NL;
            intFieldParam = intFieldParam + IND + IND + IND + "if(field.compare(\"" + intField + "\") == 0)" + NL
                    + IND + IND + IND + IND + "output_" + intField + " = true;" + NL;
            dpIn = dpIn + IND + "dp_in.property(\"" + intField + "\", " + intField + "_read);" + NL;
            dp = dp + IND + "if (output_" + intField + ")" + NL
                    + IND + IND + "dp.property(\"" + intField + "\", " + intField + ");" + NL;
            properties = properties + IND + "property_map<Graph, double EdgeProperties::*>::type " + intField 
                    + " = get(&EdgeProperties::" + intField + ", g_id);" + NL;
            propertiesRead = propertiesRead + IND + "property_map<Graph_read, double EdgeProperties_read::*>::type " + intField 
                    + "_read = get(&EdgeProperties_read::" + intField + ", g_read);" + NL;
            propertiesDist = propertiesDist + IND + "typedef property_map<Graph, double EdgeProperties::*>::type " + intField + "_map;" + NL
                    + IND + "typedef std::parallel::distributed_property_map<mpi_process_group,remote_key_to_global_e," + intField + "_map> Dist_"
                    + intField + "_Map;" + NL
                    + IND + "Dist_" + intField + "_Map d_" + intField + " = std::parallel::make_distributed_property_map("
                    + "pg, remote_key_to_global_e(), " + intField + ");" + NL;
            intFieldSer = intFieldSer + "& " + intField;
            intFieldReadInit = intFieldReadInit + IND + IND + intField + "[e] = " + intField + "_read[new_edge.first];" + NL;
            lastField = intField;
        }
        propertiesDist = propertiesDist + IND + "//Vertex id distributed property maps" + NL
                + IND + "typedef property_map<Graph, vertex_index_t>::const_type VertexIndexMap;" + NL  
            + IND + "typedef iterator_property_map<std::vector<size_t>::iterator, VertexIndexMap> d_index_map;" + NL
            + IND + "std::vector<size_t> index_S(num_vertices(g_id), std::numeric_limits<size_t>::max());" + NL
            + IND + "d_index_map d_index(index_S.begin(), id);" + NL
            + IND + "d_index.set_reduce(rank_accumulate_reducer<size_t>());" + NL;
        if (!intFieldParam.equals("")) {
            intFieldParam = IND + IND + "Setting &edge_properties = cfg.lookup(\"edge_properties\");" + NL
                    + IND + IND + "edge_count = edge_properties.getLength();" + NL
                    + IND + IND + "for (int n = 0; n < edge_count; n++) {" + NL
                    + IND + IND + IND + "string field = edge_properties[n];" + NL
                    + intFieldParam
                    + IND + IND + "}" + NL;
        }
        String fieldInitDec = "";
        String fieldDec = "";
        String fieldParam = "";
        String fieldSer = "";
        String vertexReadInit = "";
        Iterator<String> fieldIt = pi.getFieldTimeLevels().keySet().iterator();
        while (fieldIt.hasNext()) {
            String field = SAMRAIUtils.variableNormalize(fieldIt.next());
            outputFields = outputFields + "output_" + field + ", ";
            fieldInitDec = fieldInitDec + IND + IND + field + " = 0;" + NL;
            fieldDec = fieldDec + IND + "double " + field + ";" + NL;
            fieldParam = fieldParam + IND + IND + IND + "if(field.compare(\"" + field + "\") == 0)" + NL
                        + IND + IND + IND + IND + "output_" + field + " = true;" + NL;
            dpIn = dpIn + IND + "dp_in.property(\"" + field + "\", " + field + "_read);" + NL;
            dp = dp + IND + "if (output_" + field + ")" + NL
                    + IND + IND + "dp.property(\"" + field + "\", " + field + ");" + NL;
            properties = properties + IND + "property_map<Graph, double VertexProperties::*>::type " + field 
                    + " = get(&VertexProperties::" + field + ", g_id);" + NL;
            propertiesRead = propertiesRead + IND + "property_map<Graph_read, double VertexProperties_read::*>::type " + field 
                    + "_read = get(&VertexProperties_read::" + field + ", g_read);" + NL;
            vertexReadInit = vertexReadInit + IND + IND + field + "[v2] = " + field + "_read[v];" + NL;
            fieldSer = fieldSer + "& " + field;
            lastField = field;
        }
        if (!fieldParam.equals("")) {
            fieldParam = IND + IND + "Setting &vertex_properties = cfg.lookup(\"vertex_properties\");" + NL
                    + IND + IND + "node_count = vertex_properties.getLength();" + NL
                    + IND + IND + "for (int n = 0; n < node_count; n++) {" + NL
                    + IND + IND + IND + "string field = vertex_properties[n];" + NL
                    + fieldParam
                    + IND + IND + "}" + NL;
        }
        outputFields = outputFields.substring(0, outputFields.lastIndexOf(","));
        String params = "";
        String modelParams = "";
        NodeList paramList = CodeGeneratorUtils.find(pi.getProblem(), "/*/mms:parameters/mms:parameter");
        for (int i = 0; i < paramList.getLength(); i++) {
            String paramName = paramList.item(i).getFirstChild().getTextContent();
            String paramType = paramList.item(i).getFirstChild().getNextSibling().getTextContent();
            String paramTypeC = getParamType(paramType);
            params = params + paramTypeC + " " + paramName + ";" + NL;
            if (paramType.equals("REAL")) {
                modelParams = modelParams + IND + IND + "if (!cfg.lookupValue(\"" + paramName + "\", " + paramName + ")) {" + NL
                        + IND + IND + IND + "int aux;" + NL
                        + IND + IND + IND + "cfg.lookupValue(\"" + paramName + "\", aux);" + NL
                        + IND + IND + IND + paramName + " = aux;" + NL
                        + IND + IND + "}" + NL;
            }
            else {
                modelParams = modelParams + IND + IND + "cfg.lookupValue(\"" + paramName + "\", " + paramName + ");" + NL;
            }
        }
        String graphTopologyAndProperties = "";
        String initFileParam = "";
        String readGraphProperties = "";
        String customGraph = "";
        String connectivity = "bidirectionalS";
        if (pi.getEdgeDirectionality() != null && pi.getEdgeDirectionality().equals("Undirected")) {
            connectivity = "undirectedS";
        }
        if (pi.getDegreeDistribution().equals("File")) {
            initFileParam = initFileParam + "string file_name;" + NL;
            modelParams = modelParams + IND + IND + "cfg.lookupValue(\"file_name\", file_name);" + NL;
            graphTopologyAndProperties = IND + "typedef std::adjacency_list<vecS, vecS, " + connectivity 
                    + ", VertexProperties_read, EdgeProperties_read> Graph_read;" + NL
                    + IND + "Graph_read g_read;" + NL + NL
                    + propertiesRead + NL
                    + IND + "//Read graph" + NL
                    + IND + "ifstream ibase;" + NL
                    + IND + "ibase.open (file_name.c_str());" + NL
                    + IND + "dynamic_properties dp_in;" + NL
                    + dpIn
                    + IND + "try {" + NL
                    + IND + IND + "read_graphviz(ibase, g_read ,dp_in, \"" + lastField + "\");" + NL
                    + IND + "} catch (std::undirected_graph_error const&  ex) {" + NL
                    + IND + IND + "cout << \"Error reading input graph. \" << ex.what() <<\"Please, transform the input graph to a "
                    + "directed one.\"<< '\n';" + NL
                    + IND + "}" + NL + NL
                    + IND + "std::mpi::environment env(argc, argv);" + NL + NL
                    + IND + "Graph g_id(num_vertices(g_read));" + NL + NL
                    + IND + "mpi_process_group pg = g_id.process_group();" + NL
                    + IND + "rank = graph::distributed::process_id(pg);" + NL
                    + IND + "int processors = graph::distributed::num_processes(pg);" + NL + NL
                    + properties + NL
                    + propertiesDist + NL
                    + IND + "//Vertex id initializations" + NL
                    + IND + "BGL_FORALL_VERTICES(v, g_id, Graph) {" + NL
                    + IND + IND + "put(d_index, v, num_vertices(g_id) * rank + id[v]);" + NL
                    + IND + "}" + NL + NL
                    + IND + "//Data initialization" + NL
                    + IND + "BGL_FORALL_VERTICES(v, g_read, Graph_read) {" + NL
                    + IND + IND + "Vertex v2 = vertex(id_read[v], g_id);" + NL
                    + vertexReadInit
                    + IND + "}" + NL
                    + IND + "if (process_id(g_id.process_group()) == 0) {" + NL
                    + IND + IND + "BGL_FORALL_EDGES(e, g_read, Graph_read) {" + NL
                    + IND + IND + IND + "std::pair<std::graph_traits<Graph>::edge_descriptor, bool> new_edge = "
                    + "add_edge(vertex(id_read[source(e, g_read)], g_id), vertex(id_read[target(e, g_read)], g_id), EdgeProperties(), g_id);" + NL
                    + IND + IND + "}" + NL
                    + IND + "}" + NL
                    + IND + "BGL_FORALL_EDGES(e, g_id, Graph) {" + NL
                    + IND + IND + "int source_e = d_index[source(e, g_id)];" + NL
                    + IND + IND + "int target_e = d_index[target(e, g_id)];" + NL
                    + IND + IND + "std::pair<std::graph_traits<Graph_read>::edge_descriptor, bool> new_edge = "
                    + "edge(vertex(source_e, g_read), vertex(target_e, g_read), g_read);" + NL
                    + intFieldReadInit
                    + IND + "}" + NL
                    + IND + "synchronize(pg);" + NL
                    + IND + "synchronize(d_index);" + NL;
            readGraphProperties = "struct EdgeProperties_read {" + NL
                    + IND + "EdgeProperties_read() {" + NL 
                    + intFieldInitDec
                    + IND + "}" + NL
                    + IND + "template<typename Archiver>" + NL
                    + IND + "void serialize(Archiver& ar, const unsigned int /*version*/) {" + NL
                    + IND + IND + "ar " + intFieldSer + ";" + NL
                    + "};" + NL
                    + intFieldDec
                    + "};" + NL + NL
                    + "struct VertexProperties_read {" + NL
                    + IND + "VertexProperties_read() {" + NL
                    + fieldInitDec
                    + IND + "}" + NL
                    + IND + "template<typename Archiver>" + NL
                    + IND + "void serialize(Archiver& ar, const unsigned int /*version*/) {" + NL
                    + IND + IND + "ar " + fieldSer + ";" + NL
                    + IND + "}" + NL
                    + fieldDec
                    + "};" + NL;
        }
        else {
            if (pi.getDegreeDistribution().equals("Scale-free")) {
                graphTopologyAndProperties = IND + "std::mpi::environment env(argc, argv);" + NL
                        + IND + "//Random scale-free" + NL 
                        + IND + "typedef std::plod_iterator<Graph> SFGen;" + NL
                        + IND + "Graph g_id(SFGen(number_of_vertices, gamma_par, seed, false), SFGen(), number_of_vertices);"
                        + NL + NL;
            }
            if (pi.getDegreeDistribution().equals("Random")) {
                customGraph = customGraph + "struct edge_mine {" + NL
                        + IND + "int source;" + NL
                        + IND + "int target;" + NL
                        + IND + "edge_mine(int a, int b) {" + NL
                        + IND + IND + "source = a;" + NL
                        + IND + IND + "target = b;" + NL
                        + IND + "}" + NL
                        + IND + "bool operator<(const edge_mine& rhs) const {" + NL
                        + IND + IND + "return source < rhs.source || (source == rhs.source && target < rhs.target);" + NL
                        + IND + "}" + NL
                        + IND + "bool operator==(const edge_mine& rhs) const {" + NL
                        + IND + IND + "return (source == rhs.source && target == rhs.target) || (source == rhs.target && target == rhs.source);" + NL
                        + IND + "}" + NL
                        + "};" + NL
                        + "std::set<edge_mine> added;" + NL + NL
                        + "void generateRandomGraph (Graph &g, int n, int a, int seed) {" + NL
                        + IND + "const gsl_rng_type * T;" + NL
                        + IND + "gsl_rng_env_setup();" + NL
                        + IND + "gsl_rng *r_var = r_var;" + NL
                        + IND + "r_var = gsl_rng_alloc(gsl_rng_mt19937);" + NL 
                        + IND + "gsl_rng_set(r_var, seed);" + NL
                        + IND + "if (process_id(g.process_group()) == 0) {" + NL
                        + IND + IND + "//Add arcs" + NL
                        + IND + IND + "for (int j = 0; j < a; /* Increment in body */) {" + NL
                        + IND + IND + IND + "int a = int(gsl_rng_uniform(r_var) * n);" + NL
                        + IND + IND + IND + "int b;" + NL
                        + IND + IND + IND + "do {" + NL
                        + IND + IND + IND + IND + "b = int(gsl_rng_uniform(r_var) * n);" + NL
                        + IND + IND + IND + "} while (a == b);" + NL
                        + IND + IND + IND + "bool found = (added.find(edge_mine(a, b)) != added.end());" + NL
                        + IND + IND + IND + "if (!found) {" + NL
                        + IND + IND + IND + IND + "Vertex av = vertex(a, g);" + NL
                        + IND + IND + IND + IND + "Vertex bv = vertex(b, g);" + NL
                        + IND + IND + IND + IND + "add_edge(av, bv, g);" + NL
                        + IND + IND + IND + IND + "added.insert(edge_mine(a, b));" + NL
                        + IND + IND + IND + "}" + NL
                        + IND + IND + IND + "++j;" + NL
                        + IND + IND + "}" + NL
                        + IND + "}" + NL
                        + "}" + NL + NL;
                graphTopologyAndProperties = IND + "std::mpi::environment env(argc, argv);" + NL 
                        + IND + "//Random graph" + NL 
                        + IND + "Graph g_id(number_of_vertices);" + NL
                        + IND + "generateRandomGraph(g_id, number_of_vertices, number_of_edges, seed * (rank + 1));" + NL + NL;
            }
            if (pi.getDegreeDistribution().equals("Circular")) {
                customGraph = customGraph + "void generateCircularGraph (Graph &g) {" + NL
                        + IND + "if (process_id(g.process_group()) == 0) {" + NL
                        + IND + IND + "int num_vert = num_vertices(g);" + NL
                        + IND + IND + "for (int i = 0; i < num_vert; i++) {" + NL
                        + IND + IND + IND + "Vertex av = vertex(i, g);" + NL
                        + IND + IND + IND + "Vertex bv = vertex((i + 1) % num_vert, g);" + NL
                        + IND + IND + IND + "add_edge(av, bv, g);" + NL
                        + IND + IND + "}" + NL
                        + IND + "}" + NL
                        + "}" + NL + NL;
                graphTopologyAndProperties = IND + "std::mpi::environment env(argc, argv);" + NL 
                        + IND + "//Circular graph" + NL 
                        + IND + "Graph g_id(number_of_vertices);" + NL
                        + IND + "generateCircularGraph(g_id);" + NL + NL;
            }
            graphTopologyAndProperties = graphTopologyAndProperties + IND + "mpi_process_group pg = g_id.process_group();" + NL
                    + IND + "rank = graph::distributed::process_id(pg);" + NL
                    + IND + "int processors = graph::distributed::num_processes(pg);" + NL
                    + IND + "synchronize(g_id);" + NL + NL
                    + properties + NL
                    + propertiesDist + NL
                    + IND + "//Vertex id initializations" + NL
                    + IND + "BGL_FORALL_VERTICES(v, g_id, Graph) {" + NL
                    + IND + IND + "put(d_index, v, (number_of_vertices/processors) * rank + id[v]);" + NL
                    + IND + "}" + NL
                    + IND + "synchronize(pg);" + NL
                    + IND + "synchronize(d_index);" + NL;
        }
        
        return "#include <iostream>" + NL
            + "#include <gsl/gsl_rng.h>" + NL
            + "#include <string>" + NL
            + "#include <fcntl.h>" + NL
            + "#include <sys/stat.h>" + NL
            + "#include <queue>" + NL
            + "#include <fstream>" + NL
            + "#include <libconfig.h++>" + NL
            + "#include <boost/graph/use_mpi.hpp>" + NL
            + "#include <boost/graph/distributed/adjacency_list.hpp>" + NL
            + "#include <boost/graph/distributed/mpi_process_group.hpp>" + NL
            + "#include <boost/graph/graphviz.hpp>" + NL
            + "#include <boost/property_map/property_map.hpp>" + NL
            + "#include <boost/graph/properties.hpp>" + NL
            + "#include <boost/graph/iteration_macros.hpp>" + NL
            + "#include <iac3_plod_generator.hpp>" + NL + NL
            + "#define MAX(X,Y) ((X) > (Y) ? (X) : (Y))" + NL
            + "#define MIN(X,Y) ((X) < (Y) ? (X) : (Y))" + NL + NL
            + "using namespace std;" + NL
            + "using namespace boost;" + NL
            + "using namespace libconfig;" + NL
            + "using std::graph::distributed::mpi_process_group;" + NL + NL
            + "//Simulation parameters" + NL
            + "int number_of_vertices, number_of_edges, seed, interval;" + NL
            + "string outputDir;" + NL
            + "bool " + outputFields + " = false;" + NL
            + "double gamma_par;" + NL
            + initFileParam + NL
            + "//Model parameters" + NL
            + params + NL
            + "struct EdgeProperties {" + NL
            + IND + "EdgeProperties() {" + NL 
            + intFieldInitDec
            + IND + "}" + NL
            + IND + "template<typename Archiver>" + NL
            + IND + "void serialize(Archiver& ar, const unsigned int /*version*/) {" + NL
            + IND + IND + "ar " + intFieldSer + ";" + NL
            + "};" + NL
            + intFieldDec
            + "};" + NL + NL
            + "struct VertexProperties {" + NL
            + IND + "VertexProperties() {" + NL
            + fieldInitDec
            + IND + "}" + NL
            + IND + "template<typename Archiver>" + NL
            + IND + "void serialize(Archiver& ar, const unsigned int /*version*/) {" + NL
            + IND + IND + "ar " + fieldSer + ";" + NL
            + IND + "}" + NL
            + fieldDec
            + "};" + NL
            + readGraphProperties + NL
            + "typedef std::adjacency_list<vecS, distributedS<mpi_process_group, vecS>, " + connectivity 
            + ", VertexProperties, EdgeProperties> Graph;" + NL
            + "typedef graph_traits<Graph>::vertex_descriptor Vertex;" + NL
            + "typedef graph_traits<Graph>::edge_descriptor Edge;" + NL 
            + "struct remote_key_e {" + NL
            + IND + "remote_key_e(int p = -1, Edge l = Edge()) : processor(p){local_key = &l;  }" + NL
            + IND + "int processor;" + NL
            + IND + "Edge* local_key;" + NL
            + IND + "template<typename Archiver>" + NL
            + IND + "void serialize(Archiver& ar, const unsigned int /*version*/) {" + NL
            + IND + IND + "ar & processor & local_key;" + NL
            + IND + "}" + NL
            + "};" + NL + NL
            + "namespace boost {" + NL 
            + "template<>" + NL
            + "struct hash<remote_key_e> {" + NL
            + IND + "std::size_t operator()(const remote_key_e& key) const {" + NL
            + IND + IND + "std::size_t hash = hash_value(key.processor);" + NL
            + IND + IND + "hash_combine(hash, key.local_key);" + NL
            + IND + IND + "return hash;" + NL
            + IND + "}" + NL
            + "};" + NL
            + "}" + NL + NL
            + "inline bool operator==(const remote_key_e& x, const remote_key_e& y) {" + NL 
            + IND + "return x.processor == y.processor && (*(x.local_key)) == (*(y.local_key));" + NL 
            + "}" + NL + NL
            + "struct remote_key_to_global_e {" + NL
            + IND + "typedef readable_property_map_tag category;" + NL
            + IND + "typedef remote_key_e key_type;" + NL
            + IND + "typedef std::pair<int, Edge> value_type;" + NL
            + IND + "typedef value_type reference;" + NL
            + "};" + NL + NL
            + "inline std::pair<int, Edge>" + NL 
            + "get(remote_key_to_global_e, const remote_key_e& key) {" + NL
            + IND + "return std::make_pair(key.processor, *(key.local_key));" + NL
            + "}" + NL + NL
            + "//Synchronization reduction operation" + NL
            + "template<typename T>" + NL
            + "struct rank_accumulate_reducer {" + NL
            + IND + "static const bool non_default_resolver = false;" + NL
            + IND + "// The default rank of an unknown node" + NL
            + IND + "template<typename K>" + NL
            + IND + "T operator()(const K&) const {return T(0); }" + NL
            + IND + "template<typename K>" + NL
            + IND + "T operator()(const K&, const T& x, const T& y) const { return x; }" + NL
            + "};" + NL + NL
            + customGraph
            + "static void _mkdir(const char *dir) {" + NL
            + IND + "char tmp[256];" + NL
            + IND + "char *p = NULL;" + NL
            + IND + "size_t len;" + NL
            + IND + "snprintf(tmp, sizeof(tmp),\"%s\",dir);" + NL
            + IND + "len = strlen(tmp);" + NL
            + IND + "if(tmp[len - 1] == '/')" + NL
            + IND + IND + "tmp[len - 1] = 0;" + NL
            + IND + "for(p = tmp + 1; *p; p++)" + NL
            + IND + IND + "if(*p == '/') {" + NL
            + IND + IND + IND + "*p = 0;" + NL
            + IND + IND + IND + "mkdir(tmp, S_IRWXU);" + NL
            + IND + IND + IND + "*p = '/';" + NL
            + IND + IND + "}" + NL
            + IND + "mkdir(tmp, S_IRWXU);" + NL
            + "}" + NL + NL
            + "int main(int argc, char** argv) {" + NL
            + IND + "int rank, size;" + NL
            + IND + "int edge_count, node_count = 0;" + NL
            + IND + "int outputCounter = 0;" + NL + NL
            + IND + "//Read parameter file" + NL
            + IND + "if (argc < 2) {" + NL
            + IND + IND + "cout<<\"USAGE: ./\"<<argv[0]<<\" parameter_file\"<<endl;" + NL
            + IND + IND + "return  -1;" + NL
            + IND + "}" + NL
            + IND + "Config cfg;" + NL
            + IND + "try {" + NL
            + IND + IND + "cfg.readFile(argv[1]);" + NL + NL
            + IND + IND + "//Simulation parameters" + NL
            + IND + IND + "cfg.lookupValue(\"number_of_vertices\", number_of_vertices);" + NL
            + IND + IND + "cfg.lookupValue(\"number_of_edges\", number_of_edges);" + NL
            + IND + IND + "cfg.lookupValue(\"seed\", seed);" + NL
            + IND + IND + "cfg.lookupValue(\"output_dir_name\", outputDir);" + NL
            + IND + IND + "cfg.lookupValue(\"output_dump_interval\", interval);" + NL
            + IND + IND + "cfg.lookupValue(\"gamma_par\", gamma_par);" + NL + NL
            + fieldParam
            + intFieldParam
            + IND + IND + "//Model parameters" + NL
            + modelParams
            + IND + "}" + NL
            + IND + "catch (ParseException ex) {" + NL
            + IND + IND + "cout << \"Problem reading parameter file \" << ex.getError() << \" at line \" << ex.getLine() << endl;" + NL
            + IND + IND + "return -1;" + NL
            + IND + "}" + NL + NL
            + graphTopologyAndProperties + NL
            + IND + "const gsl_rng_type * T;" + NL
            + IND + "gsl_rng_env_setup();" + NL
            + IND + "gsl_rng *r_var = r_var;" + NL
            + IND + "r_var = gsl_rng_alloc(gsl_rng_mt19937);" + NL
            + IND + "gsl_rng_set(r_var, seed * (rank + 1));" + NL + NL
            + IND + "bool deleted_vertex = false;" + NL
            + IND + "bool deleted_edge = false;" + NL
            + IND + "dynamic_properties dp;" + NL
            + IND + "dp.property(\"node_id\", id);" + NL
            + dp + NL
            + IND + "//Creation of the dump folder recursively" + NL
            + IND + "_mkdir(outputDir.c_str());" + NL
            + IND + "//------ Initialization -------" + NL
            + createInitialization(pi) + NL
            + IND + "//Write initial output file" + NL
            + IND + "ofstream myfileI;" + NL
            + IND + "std::ostringstream outputNameI;" + NL
            + IND + "outputNameI << outputDir << \"/initial.dot\";" + NL
            + IND + "myfileI.open (outputNameI.str().c_str());" + NL
            + IND + "write_graphviz(myfileI, g_id, dp);" + NL
            + IND + "myfileI.close();" + NL + NL
            + IND + "bool end_simulation = false;" + NL + NL
            + IND + "//------ Main loop -------" + NL
            + IND + "for (int t_it = 0; ; t_it++) {" + NL
            + IND + IND + "//------ Finalization condition -------" + NL
            + createFinalization(pi) + NL
            + IND + IND + "//------ Execution flow -------" + NL
            + createExecutionFlow(pi)
            + IND + IND + "//------ Output -------" + NL
            + IND + IND + "if ((t_it + 1) % interval == 0) {" + NL                   
            + IND + IND + IND + "//Write dot file" + NL
            + IND + IND + IND + "ofstream myfile;" + NL
            + IND + IND + IND + "std::ostringstream outputName;" + NL
            + IND + IND + IND + "outputName << outputDir << \"/result\"<<outputCounter<<\".dot\";" + NL
            + IND + IND + IND + "myfile.open (outputName.str().c_str());" + NL
            + IND + IND + IND + "write_graphviz(myfile, g_id, dp);" + NL
            + IND + IND + IND + "myfile.close();" + NL + NL
            + IND + IND + IND + "outputCounter++;" + NL
            + IND + IND + "}" + NL
            + IND + "}" + NL + NL
            + IND + "return 0;" + NL
            + "}" + NL;
    }

    /**
     * Creates the code for a topological change.
     * @param pi                The problem info
     * @param instructionSet    The instruction set.
     * @return                  The code
     * @throws CGException      CG00X External error
     */
    private static String createTopologyChange(ProblemInfo pi, Element instructionSet) throws CGException {
        String code = "";
        String[][] xslParams = new String [2][2];
        xslParams[0][0] =  "condition";
        xslParams[0][1] =  "false";
        xslParams[1][0] =  "indent";
        
        code = IND + IND + "//------ Topology change -------" + NL;
        String nodeLoopInit = "";
        String nodeLoopEnd = "";
        String indent;
        if (pi.getEvolutionStep().equals("AllVertices")) {
            if (deleteVertices(instructionSet)) {
                xslParams[1][1] =  "4";
                nodeLoopInit = IND + IND + "deleted_vertex = true;" + NL
                        + IND + IND + "while(deleted_vertex) {" + NL
                        + IND + IND + IND + "deleted_vertex = false;" + NL
                        + IND + IND + IND + "BGL_FORALL_VERTICES(n_id, g_id, Graph) {" + NL;
                nodeLoopEnd = IND + IND + IND + IND + "if (deleted_vertex) {" + NL
                        + IND + IND + IND + IND + IND + "break;" + NL
                        + IND + IND + IND + IND + "}" + NL
                        + IND + IND + IND + "}" + NL
                        + IND + IND + "}" + NL;
                indent = IND + IND + IND + IND;
            }
            else {
                xslParams[1][1] =  "3";
                nodeLoopInit = IND + IND + "BGL_FORALL_VERTICES(n_id, g_id, Graph) {" + NL;
                nodeLoopEnd = IND + IND + "}" + NL;
                indent = IND + IND + IND;
            }
  
        }
        else {
            xslParams[1][1] =  "2";
            indent = IND + IND;
        }
        String instructions = "";
        NodeList children = preProcessTopology(instructionSet).getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Element block = (Element) children.item(i).cloneNode(true);
            instructions = instructions + xmlTransform(preprocessGraph(block, pi), xslParams) + NL;
        }
        code = code + nodeLoopInit + getVariableDeclaration(instructionSet, indent) + instructions + nodeLoopEnd
                + IND + IND + "synchronize(pg);" + NL;

        
        return code;
    }
    
    /**
     * Checks when there is element elimination outside an iteration.
     * @param n                 The instructions
     * @return                  True if delete vertices
     * @throws CGException      CG00X External error
     */
    private static boolean deleteVertices(Node n) throws CGException {
        return CodeGeneratorUtils.find(n, ".//sml:deleteVertex/ancestor::sml:iterateOverVertices[1]").getLength() == 0 
                && CodeGeneratorUtils.find(n, ".//sml:deleteVertex").getLength() > 0;
    }
    
    /**
     * Modifies the instructionSet for the special iteration routines in topological change.
     * @param e             The instruction set
     * @return              The modified instruction set
     * @throws CGException  CG00X External error
     */
    private static Node preProcessTopology(Node e) throws CGException {
        //Modify edge iterator to avoid invalid iterators deleting edges 
        NodeList deleteEdges = CodeGeneratorUtils.find(e, ".//sml:deleteEdge/ancestor::sml:iterateOverEdges");
        for (int i = 0; i < deleteEdges.getLength(); i++) {
            Element newElem = CodeGeneratorUtils.createElement(e.getOwnerDocument(), smlUri, "iterateOverEdgesDelete");
            String type = ((Element) deleteEdges.item(i)).getAttribute("directionAtt");
            newElem.setAttribute("directionAtt", type);
            NodeList children = deleteEdges.item(i).getChildNodes();
            for (int j = 0; j < children.getLength(); j++) {
                newElem.appendChild(children.item(j).cloneNode(true));
            }
            deleteEdges.item(i).getParentNode().replaceChild(newElem, deleteEdges.item(i));
        }
        //Modify vertex iterator to avoid invalid iterators deleting vertices
        NodeList deleteElementIts = CodeGeneratorUtils.find(e, ".//sml:deleteVertex/ancestor::sml:iterateOverVertices");
        for (int i = 0; i < deleteElementIts.getLength(); i++) {
            Element newElem = CodeGeneratorUtils.createElement(e.getOwnerDocument(), smlUri, "sml:iterateOverVerticesDelete");
            deleteElementIts.item(i).getParentNode().replaceChild(newElem, deleteElementIts.item(i));
        }
        NodeList deleteInterIts = CodeGeneratorUtils.find(e, ".//sml:deleteVertex/ancestor::sml:iterateOverEdges");
        for (int i = 0; i < deleteInterIts.getLength(); i++) {
            Element newElem = CodeGeneratorUtils.createElement(e.getOwnerDocument(), smlUri, "iterateOverEdgesDelVertex");
            String type = ((Element) deleteInterIts.item(i)).getAttribute("directionAtt");
            newElem.setAttribute("directionAtt", type);
            NodeList children = deleteInterIts.item(i).getChildNodes();
            for (int j = 0; j < children.getLength(); j++) {
                newElem.appendChild(children.item(j).cloneNode(true));
            }
            deleteInterIts.item(i).getParentNode().replaceChild(newElem, deleteInterIts.item(i));
        }
        return e;
    }
    
    /**
     * Returns the parameter type in a C style.
     * @param type              The xml type
     * @return                  The C type
     * @throws CGException      CG001 XML document is not valid
     */
    private static String getParamType(String type) throws CGException {
        if (type.equals("INT")) {
            return "int";
        }
        if (type.equals("REAL")) {
            return "double";
        }
        if (type.equals("STRING")) {
            return "string";
        }
        if (type.equals("BOOLEAN")) {
            return "bool";
        }
        throw new CGException(CGException.CG001, "Type " + type + " for parameters not allowed");
    }
    
    /**
     * Creates the initialization code.
     * @param pi            The problem type
     * @return              The code
     * @throws CGException  CG00X External error
     */
    private static String createInitialization(ProblemInfo pi) throws CGException {
        String actualIndent = IND + IND;
        String result = "";
        
        //Parameters for the xsl transformation. It is not a condition formula
        String[][] xslParams = new String [2][2];
        xslParams[0][0] =  "condition";
        xslParams[0][1] =  "false";
        xslParams[1][0] =  "indent";
        xslParams[1][1] =  "0";
        
        NodeList initConditions = CodeGeneratorUtils.find(pi.getProblem(), "//mms:initialCondition");
        String nodeInitG = "";
        String edgeInitG = "";
        for (int j = 0; j < initConditions.getLength(); j++) {
            String nodeInit = "";
            String edgeInit = "";
            String cond = "";
            String endCond = "";
            Node initCondition = initConditions.item(j);
            NodeList mathExpressions = initCondition.getLastChild().getChildNodes();
            //If there is a condition
            if (initCondition.getFirstChild().getLocalName().equals("applyIf")) {
                xslParams[0][1] =  "true";
                cond = cond + actualIndent + "if (" + xmlTransform(initCondition.getFirstChild().getFirstChild(), xslParams) + ") {" + NL;
                actualIndent = actualIndent + IND;
            }
            xslParams[0][1] =  "false";
            xslParams[1][1] = String.valueOf(actualIndent.length());
            //Equations in the initial conditions
            for (int k = 0; k < mathExpressions.getLength(); k++) {
                Node math = mathExpressions.item(k);
                if (isEdgeInit(pi, math)) {
                    edgeInit = edgeInit + xmlTransform(preprocessGraph(math, pi), xslParams);
                }
                else {
                    nodeInit = nodeInit + xmlTransform(preprocessGraph(math, pi), xslParams);
                }
            }
            //Close if clause if exists
            if (initCondition.getFirstChild().getLocalName().equals("applyIf")) {
                actualIndent = actualIndent.substring(0, actualIndent.lastIndexOf(IND));
                endCond = endCond + actualIndent + "}" + NL + NL;
            }
            if (!nodeInit.equals("")) {
                nodeInitG = nodeInitG + cond + nodeInit + endCond;
            }
            if (!edgeInit.equals("")) {
                edgeInitG = edgeInitG + cond + edgeInit + endCond;
            }
        }
        if (!nodeInitG.equals("")) {
            result = result + IND + "BGL_FORALL_VERTICES(n_id, g_id, Graph) {" + NL
                    + nodeInitG
                    + IND + "}" + NL;
        }
        if (!edgeInitG.equals("")) {
            result = result + IND + "BGL_FORALL_EDGES(e_id, g_id, Graph) {" + NL
                    + edgeInitG
                    + IND + "}" + NL;
        }
        
        return result;
    }
    
    /**
     * Checks whether the instruction is an edge initialization.
     * @param pi               The problem info
     * @param math             The mathematical instruction
     * @return                 True if is related to edges
     * @throws CGException     CG00x External error
     */
    private static boolean isEdgeInit(ProblemInfo pi, Node math) throws CGException {
        String query = ".//mt:apply/mt:eq/following-sibling::*[1]";
        NodeList leftTerms = CodeGeneratorUtils.find(math, query);
        if (leftTerms.getLength() == 0) {
            throw new CGException(CGException.CG003, "Any initial condition is not an assignment");
        }
        String variable = xmlTransform(leftTerms.item(0), null);
        if (variable.indexOf("[") != -1) {
            variable = variable.substring(0, variable.indexOf("["));
            if (pi.getEdgeFields().contains(variable)) {
                return true;
            }
			return false;
        }
        return false;
    }
    
    /**
     * Create the execution instructions block.
     * @param pi               The problem information
     * @return                 The code
     * @throws CGException     CG00x External error
     */
    private static String createExecutionFlow(ProblemInfo pi) throws CGException {
        String[][] xslParams = new String [2][2];
        xslParams[0][0] =  "condition";
        xslParams[0][1] =  "false";
        xslParams[1][0] =  "indent";
        String result = "";
        Document doc = pi.getProblem();
        
        DocumentFragment instructionAcc = doc.createDocumentFragment();
        NodeList rules = CodeGeneratorUtils.find(doc, "//mms:ruleExecutionOrder/mms:rule");
        if (pi.getEvolutionStep().equals("AllVertices")) {
            xslParams[1][1] =  "3";
            Element previousRuleOrder = null;
            Element previousAlgorithm = null;
            String previousRuleType = "";
            String nodeLoopInit = IND + IND + "BGL_FORALL_VERTICES(n_id, g_id, Graph) {" + NL;
            String nodeLoopEnd = IND + IND + "}" + NL;
            DocumentFragment variables = doc.createDocumentFragment();
            String instructions = "";
            for (int i = 0; i < rules.getLength(); i++) {
                Element rule = (Element) rules.item(i);
                String ruleName = rule.getTextContent();
                Element algorithm = CodeGeneratorUtils.getRuleCode(pi.getProblem(), ruleName);
                String ruleType = CodeGeneratorUtils.getRuleType(pi.getProblem(), ruleName); 
                boolean unifiable = false;
                if (!previousRuleType.equals("")) {
                    unifiable = CodeGeneratorUtils.checkUnifiable(pi.getEdgeFields(), rule, previousRuleOrder, pi.getVariables(), 
                            pi.getDimensions());
                }
                
                //Patch iterators
                if (!unifiable) {
                    if (i > 0) {
                        if (previousRuleType.equals("topology")) {
                            result = result + createTopologyChange(pi, previousAlgorithm);
                        }
                        else {
                            DocumentFragment syncInst = (DocumentFragment) instructionAcc.cloneNode(true);
                            NodeList blocks = CodeGeneratorUtils.unifyInstructions(instructionAcc, pi).getChildNodes();
                            for (int j = 0; j < blocks.getLength(); j++) {
                                instructions = instructions + xmlTransform(preprocessGraph(blocks.item(j), pi), xslParams);
                            }
                            result = result + nodeLoopInit + getVariableDeclaration(variables, IND + IND + IND) + instructions + nodeLoopEnd;
                            //Sync
                            result = result + createSync(syncInst, pi);
                        }
                        variables = doc.createDocumentFragment();
                        instructions = "";
                        instructionAcc = doc.createDocumentFragment();
                    }
                }
                
                //Variables
                variables.appendChild(algorithm.cloneNode(true));
                
                xslParams[1][1] = String.valueOf(3);
                NodeList instructionsList = algorithm.getChildNodes();
                for (int k = 0; k < instructionsList.getLength(); k++) {
                    Element block = (Element) instructionsList.item(k).cloneNode(true);
                    instructionAcc.appendChild(block);
                }
                
                previousRuleType = ruleType;
                previousAlgorithm = (Element) algorithm.cloneNode(true);
                previousRuleOrder = (Element) rule.cloneNode(true);
            }
            if (previousRuleType.equals("topology")) {
                result = result + createTopologyChange(pi, previousAlgorithm);
            }
            else {
                DocumentFragment syncInst = (DocumentFragment) instructionAcc.cloneNode(true);
                NodeList blocks = CodeGeneratorUtils.unifyInstructions(instructionAcc, pi).getChildNodes();
                for (int j = 0; j < blocks.getLength(); j++) {
                    instructions = instructions + xmlTransform(preprocessGraph(blocks.item(j), pi), xslParams);
                }
                result = result + nodeLoopInit + getVariableDeclaration(variables, IND + IND + IND) + instructions + nodeLoopEnd;
                //Sync
                result = result + createSync(syncInst, pi);
            }
        }
        else {
            xslParams[1][1] =  "2";
            //Get scope for the declarations
            DocumentFragment scope = doc.createDocumentFragment();
            String query = "//sml:algorithm[ancestor::mms:rules]";
            NodeList scopes = CodeGeneratorUtils.find(doc, query);
            for (int k = 0; k < scopes.getLength(); k++) {
                scope.appendChild(scopes.item(k).cloneNode(true));
                
            }
            result = result + getVariableDeclaration(scope, IND + IND) + NL;
            //Random selection
            result = result + IND + IND + "int selected = num_vertices(g_id) * gsl_rng_uniform(r_var);" + NL
                    + IND + IND + "Vertex n_id = vertex(selected, g_id);" + NL;
        
            for (int i = 0; i < rules.getLength(); i++) {
                Element ruleOrder = (Element) rules.item(i);
                String ruleName = ruleOrder.getTextContent();
                Element algorithm = CodeGeneratorUtils.getRuleCode(pi.getProblem(), ruleName);
                String ruleType = CodeGeneratorUtils.getRuleType(pi.getProblem(), ruleName); 

                //Topology change
                if (ruleType.equals("topology")) {
                    DocumentFragment syncInst = (DocumentFragment) instructionAcc.cloneNode(true);
                    NodeList blocks = CodeGeneratorUtils.unifyInstructions(instructionAcc, pi).getChildNodes();
                    for (int j = 0; j < blocks.getLength(); j++) {
                        result = result + xmlTransform(preprocessGraph(blocks.item(j), pi), xslParams);
                    }
                    //Sync
                    result = result + createSync(syncInst, pi);
                    result = result + createTopologyChange(pi, (Element) algorithm.cloneNode(true));
                    instructionAcc = doc.createDocumentFragment();
                }
                //Gather or update
                else {
                    NodeList instructionList = algorithm.getChildNodes();
                    for (int k = 0; k < instructionList.getLength(); k++) {
                        Element block = (Element) instructionList.item(k).cloneNode(true);
                        instructionAcc.appendChild(block);
                    }
                }
            }
            DocumentFragment syncInst = (DocumentFragment) instructionAcc.cloneNode(true);
            NodeList blocks = CodeGeneratorUtils.unifyInstructions(instructionAcc, pi).getChildNodes();
            for (int j = 0; j < blocks.getLength(); j++) {
                result = result + xmlTransform(preprocessGraph(blocks.item(j), pi), xslParams);
            }
            //Sync
            result = result + createSync(syncInst, pi);
        }
        return result;
    }
    
    /**
     * Creates the code for the synchronizations.
     * @param instructions  The instructions executed previously.
     * @param pi            The problem info
     * @return              The code
     * @throws CGException  CG00X External error
     */
    private static String createSync(DocumentFragment instructions, ProblemInfo pi) throws CGException {
        String syncs = IND + IND + "synchronize(pg);" + NL;
        ArrayList<String> assigned = getAssignedFields(instructions, pi);
        for (int i = 0; i < assigned.size(); i++) {
            syncs = syncs + IND + IND + "synchronize(" + assigned.get(i) + ");" + NL;
        }
        return syncs;
    }
    
    /**
     * Create the code for declaring the variables used in the code. 
     * @param scope             The block of code
     * @param indent            The actual indent
     * @return                  The code
     * @throws CGException      CG00X External error
     */
    private static String getVariableDeclaration(Node scope, String indent) throws CGException {
        String declaration = "";
        //Get the variables assigned in the code
        HashSet<String> variables = new HashSet<String>();
        String query = ".//mt:math[(not(parent::sml:if) or parent::sml:then) and (not(parent::sml:while) or parent::sml:loop) and "
            + "not(parent::sml:return) and not(ancestor::mms:applyIf) and not(ancestor::mms:finalizationCondition)]/mt:apply/mt:eq/"
            + "following-sibling::*[1]";
        NodeList leftTerms = CodeGeneratorUtils.find(scope, query);
        for (int i = 0; i < leftTerms.getLength(); i++) {
            String variable = xmlTransform(leftTerms.item(i), null);
            if (!variables.contains(variable) && variable.indexOf("[") == -1) {
                declaration = declaration + indent + "double " + variable + ";" + NL;
                variables.add(variable);
            }
        }
        return declaration;
    }
    
    /**
     * Gets the variables assigned in the code inside a iteration over interactions.
     * @param node          The instruction set
     * @param pi            The problem info
     * @return              The variables assigned
     * @throws CGException  CG00X External error
     */
    public static ArrayList<String> getAssignedFields(Node node, ProblemInfo pi) throws CGException {
        ArrayList<String> variables = new ArrayList<String>();
        String query = ".//mt:math[not(parent::sml:if) and not(parent::sml:while)]/mt:apply/mt:eq/following-sibling::*[1]"
                + "|.//mt:math[not(parent::sml:if) and not(parent::sml:while)]/sml:remoteInteraction/*[1]";
        NodeList leftTerms = CodeGeneratorUtils.find(node, query);
        for (int i = 0; i < leftTerms.getLength(); i++) {
            String variable = xmlTransform(leftTerms.item(i), null);
            if (variable.indexOf("[") > 0) {
                variable = variable.substring(0, variable.indexOf("["));
         
                if (!variables.contains(variable) 
                        && (pi.getEdgeFields().contains(variable) || pi.getFieldTimeLevels().containsKey(variable))) {
                    variables.add(variable);
                }
            }
        }
        return variables;
    }
    
    /**
     * Gets the variables assigned in the code from a set of predefined variables inside a iteration over interactions.
     * @param vars          The set of predefined variables
     * @param node          The instruction set
     * @param direction     The direction of the interaction
     * @return              The variables assigned
     * @throws CGException  CG00X External error
     */
    public static ArrayList<String> getAssignedVarsInteraction(ArrayList<String> vars, Node node, String direction) throws CGException {
        ArrayList<String> variables = new ArrayList<String>();
        String query = ".//mt:math[(not(parent::sml:if) or parent::sml:then) and (not(parent::sml:while) or parent::sml:loop) and " 
                + "(ancestor::sml:iterateOverEdgess[@directionAtt = '" + direction + "'])]/mt:apply/mt:eq/following-sibling::*[1]";
        NodeList leftTerms = CodeGeneratorUtils.find(node, query);
        for (int i = 0; i < leftTerms.getLength(); i++) {
            String variable = xmlTransform(leftTerms.item(i), null);
            if (variable.indexOf("[") > 0) {
                variable = variable.substring(0, variable.indexOf("["));
                if (!variables.contains(variable) && vars.contains(variable)) {
                    variables.add(variable);
                }
            }
        }
        return variables;
    }
    
    /**
     * Gets the variables assigned in the code inside a iteration over interactions.
     * @param node          The instruction set
     * @param direction     The direction of the interaction
     * @return              The variables assigned
     * @throws CGException  CG00X External error
     */
    public static ArrayList<String> getAssigned(Node node, String direction) throws CGException {
        ArrayList<String> variables = new ArrayList<String>();
        String query = ".//mt:math[(not(parent::sml:if) or parent::sml:then) and (not(parent::sml:while) or parent::sml:loop) and "
                + "(ancestor::sml:iterateOverEdges[@directionAtt = '" + direction + "'])]/mt:apply/mt:eq/following-sibling::*[1]";
        NodeList leftTerms = CodeGeneratorUtils.find(node, query);
        for (int i = 0; i < leftTerms.getLength(); i++) {
            String variable = xmlTransform(leftTerms.item(i), null);
            if (variable.indexOf("[") > 0) {
                variable = variable.substring(0, variable.indexOf("["));
            }
            if (!variables.contains(variable)) {
                variables.add(variable);
            }
        }
        return variables;
    }
    
    /**
     * Checks if any of a set of variables is used inside the iterateOverEdges element.
     * @param node              The element
     * @param variables         The variable set
     * @param direction         The direction of the edge
     * @return                  True if any of them used
     * @throws CGException      CG00x External error
     */
    public static boolean checkUsed(Node node, ArrayList<String> variables, String direction) throws CGException {
        //Get variables used in the right side and inside a function
        String query = ".//mt:apply[child::*[1][name() = 'mt:ci'] and (ancestor::sml:iterateOverEdges[@directionAtt = '" + direction + "'])]|.//"
                + "mt:apply[child::*[1][name() = 'mt:msup' and (ancestor::sml:iterateOverEdges[@directionAtt = '" + direction 
                + "'])]/*[1][name() = 'mt:ci']]|.//mt:ci[ancestor::sml:iterateOverEdges[@directionAtt = '" + direction 
                + "'] and not(parent::mt:msup or parent::mt:msub or parent::mt:msupsub or parent::mt:apply[child::*[1][name() = 'mt:ci']])]";
        NodeList variablesUsed = CodeGeneratorUtils.find(node, query);
        for (int i = 0; i < variablesUsed.getLength(); i++) {
            String variable = xmlTransform(variablesUsed.item(i), null);
            //Take only the identifier. The indexes will be ignored
            if (variable.indexOf("[") != -1) {
                variable = variable.substring(0, variable.indexOf("["));
            }
            if (variables.contains(variable)) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Creates the code for the finalization condition.
     * @param pi                The process information
     * @return                  The code
     * @throws CGException      CG00x External error
     */
    private static String createFinalization(ProblemInfo pi) throws CGException {
        String result = "";
        Document doc = pi.getProblem();
        String[][] xslParams = new String [2][2];
        xslParams[0][0] =  "condition";
        xslParams[0][1] =  "false";
        xslParams[1][0] =  "indent";
        xslParams[1][1] =  "2";
        NodeList finalization = CodeGeneratorUtils.find(doc, "//mms:finalizationCondition");
        for (int i = 0; i < finalization.getLength(); i++) {
            String actualIndent = IND + IND;
            NodeList mathExpressions = finalization.item(i).getLastChild().getChildNodes();
            Node condition = finalization.item(i);
            
            //If the finalization condition uses a variable the code has to loop over all the patches.
            if (CodeGeneratorUtils.usesVariable(finalization.item(i), doc)) {
                result = result + actualIndent + "BGL_FORALL_VERTICES(n_id, g_id, Graph) {" + NL;
                actualIndent = actualIndent + IND;
            }
            
            int equationsFrom = 0;
            //If there is a condition
            if (condition.getLocalName().equals("applyIf")) {
                xslParams[0][1] =  "true";
                xslParams[1][1] =  String.valueOf(actualIndent.length());
                equationsFrom = 1;
                String instruction = xmlTransform(condition.getFirstChild(), xslParams);
                result = result + actualIndent + "if (" + instruction + ") {" + NL;
                actualIndent = actualIndent + IND;
            }
            xslParams[0][1] =  "true";
            xslParams[1][1] =  String.valueOf(actualIndent.length());
            String endCondition = "";
            //Finalization conditions
            for (int k = 0; k < mathExpressions.getLength(); k++) {
                Node math = mathExpressions.item(k);
                endCondition = endCondition + " && " 
                    + xmlTransform(preprocessGraph(math, pi), xslParams);
            }
            endCondition = endCondition.substring(4);
            result = result + actualIndent + "if (" + endCondition + ") { " + NL
                + actualIndent + IND + "end_simulation = true;" + NL
                + actualIndent + "}" + NL;
            
            //Close if clause if exists
            if (equationsFrom == 1) {
                actualIndent = actualIndent.substring(0, actualIndent.lastIndexOf(IND));
                result = result + actualIndent + "}" + NL + NL;
            }
            
            if (CodeGeneratorUtils.usesVariable(finalization.item(i), doc)) {
                actualIndent = actualIndent.substring(0, actualIndent.lastIndexOf(IND));
                result = result + actualIndent + "}" + NL;
            }
            //if the condition run in a loop over nodes the break could not exit the whole simulation
            result = result + actualIndent + "if (end_simulation) {" + NL
                    + actualIndent + IND + "break;" + NL
                    + actualIndent + "}" + NL; 
        }
        
        return result;
    }
        
    
    /**
     * Preprocess the graph instructions:
     * 1 - Process in-interaction loops to update assigned interactions in multiprocessing.
     * @param element           The instruction
     * @param pi                The problem information
     * @return                  The problem processed
     * @throws CGException      CG00X External error
     */
    private static Node preprocessGraph(Node element, ProblemInfo pi) throws CGException {
        Document problem = element.getOwnerDocument();
        //1 - Process in-interaction loops to update assigned interactions in multiprocessing.
        NodeList assignments = CodeGeneratorUtils.find(element, ".//*[local-name() = 'iterateOverEdges' and @directionAtt ='in']"
                + "//mt:math[not(parent::sml:if or parent::sml:while)]|.[local-name() = 'iterateOverEdges' and @directionAtt ='in']"
                + "//mt:math[not(parent::sml:if or parent::sml:while)]");
        for (int i = assignments.getLength() - 1; i >= 0; i--) {
            Element math = (Element) assignments.item(i).getFirstChild();
            String assignedVar = xmlTransform(math.getFirstChild().getNextSibling(), null);
            if (assignedVar.indexOf("[") > 0) {
                assignedVar = assignedVar.substring(0, assignedVar.indexOf("["));
                if (pi.getEdgeFields().contains(assignedVar)) {
                    Element remoteEdge = CodeGeneratorUtils.createElement(problem, smlUri, "remoteEdge");
                    Element ci = CodeGeneratorUtils.createElement(problem, mtUri, "ci");
                    ci.setTextContent("d_" + assignedVar);
                    remoteEdge.appendChild(ci);
                    remoteEdge.appendChild(math.getLastChild());
                    math.getParentNode().replaceChild(remoteEdge, math);
                }
            }
        }
        return element;
    }
    
    /**
     * Normalize the name of a variable with C rules and according entities.xsl.
     * @param variable          The original name of the variable
     * @return                  The variable normalized.
     * @throws CGException      CG00X External error
     */
    public static String variableNormalize(Node variable) throws CGException {
        final int cVariableLimit = 255;
        try {
            //Replace the greek characters. Process with xslt
            String var = xmlTransform(variable, null);
            
            //The first character must be a letter
            while (CodeGeneratorUtils.isNumber(var.substring(0, 1))) {
            	var = var.substring(1);
            }
            //The remaining characters, if any, may be letters, digits, or underscores
            var = var.replaceAll("[^a-zA-Z0-9_]", "");
            //Check length
            if (var.length() > cVariableLimit) {
            	var = var.substring(0, cVariableLimit - 1);
            }
            
            return var;
        }
        catch (Exception e) {
            throw new CGException(CGException.CG00X, e.getMessage());
        }
    }
}
