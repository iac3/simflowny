package eu.iac3.mathMS.codeGeneratorPlugin.utils;

import java.util.ArrayList;

/**
 * Class with the important information from a x3d region.
 * @author bminyano
 *
 */
public class X3DRegion {

    private String pointAttr;
    private String coordIndexAttr;
    private String normalAttr;
    private ArrayList<ArrayList<String>> transforms;
    
    /**
     * Default constructor.
     */
    public X3DRegion() {
        
    }
    
    public String getCoordIndexAttr() {
        return coordIndexAttr;
    }

    public void setCoordIndexAttr(String coordIndexAttr) {
        this.coordIndexAttr = coordIndexAttr;
    }

    public String getPointAttr() {
        return pointAttr;
    }
    public void setPointAttr(String pointAttr) {
        this.pointAttr = pointAttr;
    }
    public ArrayList<ArrayList<String>> getTransforms() {
        return transforms;
    }
    public void setTransforms(ArrayList<ArrayList<String>> transforms) {
        this.transforms = transforms;
    }
    //TODO borrar
    public String getNormalAttr() {
        return normalAttr;
    }

    public void setNormalAttr(String normalAttr) {
        this.normalAttr = normalAttr;
    }
}
