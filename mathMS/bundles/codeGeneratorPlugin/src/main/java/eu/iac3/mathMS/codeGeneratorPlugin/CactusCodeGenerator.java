/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.codeGeneratorPlugin;

import eu.iac3.mathMS.codeGeneratorPlugin.fvm.CactusExecution;
import eu.iac3.mathMS.codeGeneratorPlugin.fvm.CactusInitializations;
import eu.iac3.mathMS.codeGeneratorPlugin.fvm.CactusMapping;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.CactusUtils;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.CactusVarGroup;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.CactusVarGroupInfo;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.CodeGeneratorUtils;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.HeaderInfo;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.ProblemInfo;
import eu.iac3.mathMS.codesDBPlugin.CodeResult;
import eu.iac3.mathMS.codesDBPlugin.CodesDB;
import eu.iac3.mathMS.codesDBPlugin.CodesDBImpl;
import eu.iac3.mathMS.codesDBPlugin.DCException;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;

import org.osgi.service.log.LogService;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * The processes to generate the code for Cactus code platform.
 * @author bminyano
 *
 */
public class CactusCodeGenerator extends CodeGeneratorImpl {
    
    static final String NL = System.getProperty("line.separator"); 
    static final String IND = "\u0009";
    static final int THREE = 3;
    static final String MTURI = "http://www.w3.org/1998/Math/MathML";
    static final String MMSURI = "urn:mathms";
    static final int MCDMAX = 10;
    
    /**
     * Constructor to take the bundle context.
     */
    public CactusCodeGenerator() {
        super();
    }
    /**
     * Generates the code for Cactus code.
     * 
     * @param author            the author of the code generated
     * @param version           the version of the code
     * @param xmlDoc            the document to generate the code from
     * @return                  The id of the generated code
     * @throws CGException      CG001 XML document does not match xml schema
     *                          CG002 Code already exist.
     *                          CG003 Not possible to create code for the platform
     *                          CG004 External error
     */
    String generateCode(String author, String version, String xmlDoc) throws CGException {
        CodesDB cdb = null;
        String id = null;
        try {
            CodeGeneratorUtils.schemaValidation(xmlDoc, "discretizedProblem");
            Document doc = CodeGeneratorUtils.stringToDom(xmlDoc);
            String name = doc.getDocumentElement().getFirstChild().getFirstChild().getTextContent();
            String problemIdentifier = CactusUtils.variableNormalize(name);
            Date now = new Date();
            //Check that the code was not generated previously. We can delete the manual work over the files.
            cdb = new CodesDBImpl();
            CodeResult[] crArray = cdb.getCodeList(name, author, version, now, now, PlatformType.cactus.toString());
            if (crArray != null) {
                throw new CGException(CGException.CG002);
            }
            
            //Check that is not a particle code
            if (CodeGeneratorUtils.find(doc, "//mms:mesh").getLength() > 0 
            		&& CodeGeneratorUtils.find(doc, "//mms:mesh").item(0).getTextContent().toLowerCase().equals("particles")) {
                throw new CGException(CGException.CG003, "Particle code is not generable for Cactus");
            }
            
            //Generation of the Cactus arrangement file structure
            String baseFolder = problemIdentifier + File.separator + problemIdentifier;
            //Get the common information
            CodeGeneratorUtils.replaceDefinitions(doc);
            HeaderInfo hi = new HeaderInfo(problemIdentifier, author, version, now);
            ProblemInfo pi = new ProblemInfo();
            pi.setCoordinates(CodeGeneratorUtils.getProblemSpatialCoordinates(doc));
            pi.setPeriodicalBoundary(CodeGeneratorUtils.getPeriodicalBoundaries(doc, pi.getCoordinates()));
            pi.setBoundariesPrecedence(CodeGeneratorUtils.getBoundariesPrecedence(doc));
            pi.setRegionPrecedence(CodeGeneratorUtils.getRegionPrecedence(doc));
            pi.setRegions(CodeGeneratorUtils.getRegions(doc, pi.getRegionPrecedence()));
            pi.setHasMeshFields(true);
            pi.setRegionIds(CodeGeneratorUtils.getRegionIds(doc, pi.getRegions(), pi.getBoundariesPrecedence(), pi));
            pi.setRegionRelations(CodeGeneratorUtils.getRegionRelations(doc, pi.getRegions()));
            pi.setGridLimits(CodeGeneratorUtils.getGridLimits(pi.getCoordinates(), doc));
            pi.setSchemaStencil(CodeGeneratorUtils.getSchemaStencil(doc));
            pi.setFieldTimeLevels(CodeGeneratorUtils.getFieldTimeLevels(doc));
            pi.setHardRegions(CodeGeneratorUtils.getHardRegions(doc, pi.getCoordinates()));
            pi.setExtrapolFuncVars(CactusUtils.extrapolationFunctionVariables(doc, pi));
            //Preprocess the problem for cactus issues
            doc = CactusUtils.preprocess(doc, pi);
            pi.setIdentifiers(CactusUtils.getIdentifiers(doc));
            pi.setVariables(CactusUtils.getVariables(doc, pi.getIdentifiers(), pi.getExtrapolFuncVars()));
            pi.setProblem(doc);
            pi.setGroupInfo(CactusUtils.getVarGroupInfo(doc, pi));
            
            //Creation of the code
            if (logservice != null) {
                logservice.log(LogService.LOG_INFO, "Generating code: " + problemIdentifier);
            }
            id = cdb.addGeneratedCode(name, author, version, PlatformType.cactus.toString());
            
            //param.ccl
            cdb.addCodeFile(id, "configuration", baseFolder + File.separator + "param.ccl", paramCCL(doc, hi));
            if (logservice != null) {
                logservice.log(LogService.LOG_INFO, "param.ccl created");
            }
            
            //schedule.ccl
            cdb.addCodeFile(id, "configuration", baseFolder + File.separator + "schedule.ccl", 
                    scheduleCCL(hi, pi));
            if (logservice != null) {
                logservice.log(LogService.LOG_INFO, "schedule.ccl created");
            }
            
            //interface.ccl
            cdb.addCodeFile(id, "configuration", baseFolder + File.separator + "interface.ccl", 
                    interfaceCCL(hi, pi));
            if (logservice != null) {
                logservice.log(LogService.LOG_INFO, "interface.ccl created");
            }
            
            //configuration.ccl
            cdb.addCodeFile(id, "configuration", baseFolder + File.separator + "configuration.ccl", configurationCCL(hi, pi.getCoordinates().size()));
            if (logservice != null) {
                logservice.log(LogService.LOG_INFO, "configuration.ccl created");
            }
            
            //example.par
            cdb.addCodeFile(id, "parameter", baseFolder + File.separator + "par" + File.separator + "example.par", 
                    examplePar(hi, pi));
            if (logservice != null) {
                logservice.log(LogService.LOG_INFO, "example.par created");
            }
            
            String sourcefiles = "";
            
            //src/Startup.F
            cdb.addCodeFile(id, "sources", baseFolder + File.separator + "src" + File.separator + "Startup.F",  startupF(hi));
            sourcefiles = sourcefiles + " Startup.F";
            if (logservice != null) {
                logservice.log(LogService.LOG_INFO, "Startup.F created");
            }
            
            //src/MappingRegions.m4
            cdb.addCodeFile(id, "sources", baseFolder + File.separator + "src" + File.separator + "MappingRegions.m4", 
                    CactusMapping.mappingRegionsM4(hi, pi));
            sourcefiles = sourcefiles + " MappingRegions.F";
            if (logservice != null) {
                logservice.log(LogService.LOG_INFO, "MappingRegions.m4 created");
            }
            
            //src/InitialConditions.F
            NodeList initConds = doc.getElementsByTagNameNS("urn:mathms", "initialCondition");
            if (initConds.getLength() > 0) {
                cdb.addCodeFile(id, "sources", baseFolder + File.separator + "src" + File.separator + "InitialConditions.F", 
                        CactusInitializations.initialConditionsF(hi, pi));
                sourcefiles = sourcefiles + " InitialConditions.F";
                if (logservice != null) {
                    logservice.log(LogService.LOG_INFO, "InitialConditions.F created");
                }
            }
            //src/ExecutionFlow.m4
            cdb.addCodeFile(id, "sources", baseFolder + File.separator + "src" + File.separator + "ExecutionFlow.m4", 
                    CactusExecution.executionFlowM4(hi, pi));
            sourcefiles = sourcefiles + " ExecutionFlow.F";
            if (logservice != null) {
                logservice.log(LogService.LOG_INFO, "ExecutionFlow.m4 created");
            }
            
            //src/Functions.F
            NodeList functions = doc.getElementsByTagNameNS("urn:mathms", "function");
            if (functions.getLength() > 0) {
                cdb.addCodeFile(id, "sources", baseFolder + File.separator + "src" + File.separator + "Functions.F", 
                        functionsF(hi, pi));
                sourcefiles = sourcefiles + " Functions.F";
            }
            if (logservice != null) {
                logservice.log(LogService.LOG_INFO, "Functions.F created");
            }
            
            //src/make.code.defn
            cdb.addCodeFile(id, "configuration", baseFolder + File.separator + "src" + File.separator + "make.code.defn", 
                    makeCodeDefn(sourcefiles, hi));

            if (logservice != null) {
                logservice.log(LogService.LOG_INFO, "Code generated finished");
            }
            return id;
        }
        catch (CGException e) {
            e.printStackTrace();
            //If any exception occured, delete the generated code.
            if (id != null) {
                try {
                    cdb.deleteGeneratedCode(id);
                } 
                catch (DCException e1) {
                    e1.printStackTrace();
                    throw new CGException(CGException.CG00X, e.getMessage());
                }
            }
            //Propagate the exceptions that could be launch in this method
            throw e;
        }
        catch (Exception e) {
            e.printStackTrace();
            //If any exception occured, delete the generated code.
            if (id != null) {
                try {
                    cdb.deleteGeneratedCode(id);
                } 
                catch (DCException e1) {
                    e1.printStackTrace();
                    throw new CGException(CGException.CG00X, e.getMessage());
                }
            }
            throw new CGException(CGException.CG00X, e.getMessage());
        }
    }
    
    /**
     * Generate the content for the param.ccl file.
     * @param doc               problem document
     * @param hi                header information
     * @return                  the content of the file
     * @throws CGException      CG003 Not possible to create code for the platform
     *                          CG004 External error
     */
    private String paramCCL(Document doc, HeaderInfo hi) throws CGException {
        String content;
        NodeList params = doc.getElementsByTagNameNS("urn:mathms", "parameter");
        String parameters = "";
        if (params.getLength() > 0) {
            //create an entry for each parameter
            for (int i = 0; i < params.getLength(); i++) {
                String type = CactusUtils.getType(((Element) params.item(i)).getElementsByTagNameNS("urn:mathms", "type")
                        .item(0).getTextContent());
                parameters = parameters 
                    + type + " " + CactusUtils.variableNormalize(params.item(i).getFirstChild().getTextContent()) + " \"\"" + NL 
                    + CactusUtils.getParameterSpecification(type);
            }
        }
        content = "# Parameter definitions for thorn " + hi.getProblemId() + NL
            + "# $Header: /cactus/" + hi.getProblemId()  + "/" + hi.getProblemId()  + "/param.ccl,v " 
            + hi.getVersion() + " " + hi.getDate().toString() + " " + hi.getAuthor() + " Exp $" + NL + NL
            + parameters;
        return content;
    }
    
    /**
     * Generate the content for the schedule.ccl file.
     * @param hi                the heather info
     * @param problemInfo       the problem info
     * @return                  the content of the file
     * @throws CGException      CG003 Not possible to create code for the platform
     *                          CG004 External error
     */
    private String scheduleCCL(HeaderInfo hi, ProblemInfo problemInfo) throws CGException {
        try {
            String content = "# Schedule definitions for thorn " + hi.getProblemId() + NL
                + "# $Header: /cactus/" + hi.getProblemId() + "/" + hi.getProblemId() + "/schedule.ccl,v " 
                + hi.getVersion() + " " + hi.getDate().toString() + " " + hi.getAuthor() + " Exp $" + NL + NL
                + "STORAGE: ";
            
            String auxGroupDouble = "";
            if (!problemInfo.getHardRegions().isEmpty()) {
                auxGroupDouble = ", AuxiliaryGroup2[1]"; 
            }
            
            String fieldAcc = "";
            String temporalVars = "";
            Hashtable<String, CactusVarGroup> varGroups = problemInfo.getGroupInfo().getVarGroups();
            Iterator<String> groupIt = varGroups.keySet().iterator();
            while (groupIt.hasNext()) {
                CactusVarGroup cvg = varGroups.get(groupIt.next());
                if (cvg.getVariables().size() > 0) {
                    if (cvg.isFieldGroup()) {
                        fieldAcc = fieldAcc + cvg.getName() + "[" + cvg.getTimeLevels() + "], ";
                    }
                    else {
                        temporalVars = temporalVars + CactusUtils.variableNormalize(cvg.getName()) + "[1], ";
                    } 
                }
            }
            fieldAcc = fieldAcc.substring(0, fieldAcc.lastIndexOf(", "));
            //If there is extrapolation, its variables must be synchronized 
            String extrapolation = "";
            boolean extrapolationSync = CodeGeneratorUtils.find(problemInfo.getProblem(), 
                    "//mms:postInitialCondition//fieldExtrapolation").getLength() > 0;
            if (extrapolationSync) {
                CactusVarGroupInfo cvgi = problemInfo.getGroupInfo();
                Iterator<String> groupNames = cvgi.getVarGroups().keySet().iterator();
                
                while (groupNames.hasNext()) {
                    String fieldGroup = groupNames.next();
                    if (extrapolationSync) {
                        ArrayList<String> vars = cvgi.getVarGroups().get(fieldGroup).getVariables();
                        for (int i = 0; i < vars.size() && extrapolationSync; i++) {
                            if (vars.get(i).contains("extrapolated")) {
                                extrapolation = extrapolation + ", " + fieldGroup + "[1]";
                                extrapolationSync = false;
                            }
                        }  
                    }
                }
            }
            
            content = content + fieldAcc + ", AuxiliaryGroup[1], MapGroup[1], NonSyncGroup[1]" + auxGroupDouble + extrapolation + NL;
           
            
            //Register banner
            content = content + NL
                + "schedule " + hi.getProblemId() + "_startup at STARTUP" + NL
                + "{" + NL
                + "  LANG: Fortran" + NL
                + "} \"Register banner\"";
            
            //Mapping
            content = content + NL
                + "schedule " + hi.getProblemId() + "_mapping at INITIAL" + NL
                + "{" + NL
                + "  LANG: Fortran" + NL
                + "  STORAGE: AuxiliaryGroup[1], MapGroup[1], NonSyncGroup[1]" + auxGroupDouble + NL
                + "} \"Mapping regions\"";
            
            //Initialization
            content = content + NL
                + "schedule " + hi.getProblemId() + "_initial at INITIAL AFTER " + hi.getProblemId() + "_mapping" + NL
                + "{" + NL
                + "  LANG: Fortran" + NL
                + "  STORAGE: " + fieldAcc + ", AuxiliaryGroup[1]" + auxGroupDouble + extrapolation + NL
                + "} \"Initial conditions\"";
            
            //flow of simulation
            content = content + NL
                + "schedule " + hi.getProblemId() + "_execution at EVOL" + NL
                + "{" + NL
                + "  LANG: Fortran" + NL;

            if (problemInfo.getVariables().size() > 0) {
                content = content + "  STORAGE: " + fieldAcc + ", " + temporalVars + "AuxiliaryGroup[1]" + auxGroupDouble + NL + "} \"Evolution\"";
            }
            else {
                content = content + "  STORAGE: " + fieldAcc + ", AuxiliaryGroup[1]" + auxGroupDouble + NL + "} \"Evolution\"";
            }
            return content;
        } 
        catch (DOMException e) {
            throw new CGException(CGException.CG00X, e.getMessage());
        }
    }
    
    /**
     * Generate the content for the interface.ccl file.
     * @param hi                The header information
     * @param problemInfo       The problem information
     * @return                  the content of the file
     * @throws CGException      CG003 Not possible to create code for the platform
     *                          CG004 External error
     */
    private String interfaceCCL(HeaderInfo hi, ProblemInfo problemInfo) 
        throws CGException {
        try {
            String content = "# Interface definition for thorn " + hi.getProblemId() + NL
                + "# $Header: /cactus/" + hi.getProblemId() + "/" + hi.getProblemId() + "/param.ccl,v " 
                + hi.getVersion() + " " + hi.getDate().toString() + " " + hi.getAuthor() + " Exp $" + NL + NL
                + "implements: " + hi.getProblemId() + NL;
            int fieldDimensions = problemInfo.getCoordinates().size();
            if (fieldDimensions == THREE) {
                content = content + "inherits: grid" + NL + NL;
            }
            content = content + "public:" + NL + NL;
            
            content = content + "##############################################" + NL
                + "################### Dummy ####################" + NL
                + "##############################################" + NL
                + "real dummy type = GF dim=" + fieldDimensions + " timelevels=1" + NL
                + "{" + NL 
                + "  dummyCactus" + NL
                + "}" + NL + NL;
            
            //Print the groups
            CactusVarGroupInfo cvgi = problemInfo.getGroupInfo();
            Hashtable<String, CactusVarGroup> varGroups = cvgi.getVarGroups();
            Iterator<String> cvgiIt = varGroups.keySet().iterator();
            while (cvgiIt.hasNext()) {
                String groupName = cvgiIt.next();
                ArrayList<String> variables = varGroups.get(groupName).getVariables();
                if (variables.size() > 0) {
                    content = content + "real " + CactusUtils.variableNormalize(groupName) + " type = GF dim=" + fieldDimensions
                        + " timelevels=" + varGroups.get(groupName).getTimeLevels() + NL
                        + "{" + NL
                        + "  ";
                    for (int i = 0; i < variables.size(); i++) {
                        content = content + CactusUtils.variableNormalize(variables.get(i)) + ", ";
                    }
                    content = content.substring(0, content.length() - 2) + NL;
                    content = content + "}" + NL + NL;
                }
            }
            String fovs = "";
            for (int j = 0; j < problemInfo.getRegionIds().size(); j++) {
                String segId = problemInfo.getRegionIds().get(j);
                fovs = fovs + "FOV_" + segId + ", ";
            }
            fovs = fovs.substring(0, fovs.length() - 2);
            
            //Auxiliar variables
            content = content + "##############################################" + NL
                + "############# Auxiliar variables #############" + NL
                + "##############################################" + NL
                + "int AuxiliaryGroup type = GF dim=" + fieldDimensions + " timelevels=1" + NL
                + "{" + NL
                + "  " + fovs + NL
                + "}" + NL
                + "int MapGroup type = GF dim=" + fieldDimensions + " timelevels=1" + NL
                + "{" + NL
                + "  interior" + NL
                + "}" + NL
                + "int NonSyncGroup type = GF dim=" + fieldDimensions + " timelevels=1" + NL
                + "{" + NL
                + "  nonSync" + NL
                + "}" + NL + NL;
            if (!problemInfo.getHardRegions().isEmpty()) {
                //Stalled vars
                String stalled = "";
                ArrayList<String> candidates = new ArrayList<String>();
                //Fill with distance variables for hard region boundaries
                ArrayList<String> segNBound = new ArrayList<String>();
                for (int j = problemInfo.getRegions().size() - 1; j >= 0; j--) {
                    String regionName = problemInfo.getRegions().get(j);
                    segNBound.add(regionName + "I");
                    segNBound.add(regionName + "S");
                }
                for (int j = problemInfo.getBoundariesPrecedence().size() - 1; j >= 0; j--) {
                    String boundName = problemInfo.getBoundariesPrecedence().get(j);
                    segNBound.add(boundName);
                }
                for (int j = 0; j < segNBound.size(); j++) {
                    String regionName = segNBound.get(j);
                    if (problemInfo.getHardRegionFields(regionName) != null) {
                        ArrayList<String> interactions = CodeGeneratorUtils.getRegionInteractions(regionName, problemInfo.getHardRegions(), 
                                problemInfo.getRegions());
                        for (int k = 0; k < interactions.size(); k++) {
                            ArrayList<String> hardFields = CodeGeneratorUtils.filterHardFields(regionName, interactions.get(k), 
                                    problemInfo.getHardRegions());
                            if (hardFields.size() > 0) {
                                String segName = interactions.get(k);
                                int segId = 2 * problemInfo.getRegions().indexOf(segName) + 1;
                                if (problemInfo.getRegionIds().contains(String.valueOf(segId))) {
                                    if (!candidates.contains("stalled_" + segId)) {
                                        candidates.add("stalled_" + segId);
                                        stalled = stalled + "stalled_" + segId + ", ";
                                    }
                                }
                                segId = 2 * problemInfo.getRegions().indexOf(segName) + 2;
                                if (problemInfo.getRegionIds().contains(String.valueOf(segId))) {
                                    if (!candidates.contains("stalled_" + segId)) {
                                        candidates.add("stalled_" + segId);
                                        stalled = stalled + "stalled_" + segId + ", ";
                                    }
                                }
                            }
                        }
                    }
                }
                content = content + "int AuxiliaryGroup2 type = GF dim=" + fieldDimensions + " timelevels=1" + NL
                        + "{" + NL
                        + IND + stalled + CactusUtils.getHardRegionIntVars(problemInfo) + NL
                        + "}" + NL;
            }
            return content;
        } 
        catch (DOMException e) {
            throw new CGException(CGException.CG00X, e.getMessage());
        }
    }
    
    /**
     * Generate the content for the configuration.ccl file.
     * @param hi                the header information
     * @param fieldDimensions   the dimensions of all the field
     * @return                  the content of the file
     */
    private String configurationCCL(HeaderInfo hi, int fieldDimensions) {
        String content = "# Configuration definition for thorn " + hi.getProblemId() + NL
            + "# $Header: /cactus/" + hi.getProblemId() + "/" + hi.getProblemId() + "/configuration.ccl,v " 
            + hi.getVersion() + " " + hi.getDate().toString() + " " + hi.getAuthor() + " Exp $" + NL + NL;
        if (fieldDimensions == THREE) {
            content = content + "REQUIRES CartGrid3D";
        }
        return content;
    }  

    /**
     * Creates a example parameter file. 
     * @param hi                    the header information
     * @param problemInfo           The problem information
     * @return                      The content of the file
     * @throws CGException          CG003 Not possible to create code for the platform
     *                              CG004 External error
     */
    private String examplePar(HeaderInfo hi, ProblemInfo problemInfo) throws CGException {
        String content =  "# Parameter example file for thorn " + hi.getProblemId() + NL
            + "# $Header: /cactus/" + hi.getProblemId() + "/" + hi.getProblemId() + "/par/example.par,v " 
            + hi.getVersion() + " " + hi.getDate().toString() + " " + hi.getAuthor() + " Exp $" + NL + NL;
            
        int dimensions = problemInfo.getCoordinates().size();
        if (dimensions == THREE) {
            content = content + "ActiveThorns = \"" + hi.getProblemId() + " time timerreport IOHDF5 pugh pughslab IOHDF5Util ioutil ioascii "
                + "iobasic CartGrid3D CoordBase SymBase boundary PUGHReduce\"";
        }
        else {
            content = content + "ActiveThorns = \"" + hi.getProblemId() + " time timerreport IOHDF5 pugh pughslab IOHDF5Util "
                + "ioutil ioascii iobasic PUGHReduce\"";
        }
        content = content + NL + NL
            + "time::timestep_method = \"given\"" + NL
            + "time::timestep        = 0.0001" + NL + NL
            + "cactus::cctk_itlast = 100" + NL + NL;
        
        Document doc = problemInfo.getProblem();
        
        //grid and ghost size
        int stencil = problemInfo.getSchemaStencil();
        if (dimensions == 1) {
            content = content + "driver::global_nx = 100" + NL
                + "# Ghost size must be stencil" + NL
                + "driver::ghost_size_x = " + stencil + NL + NL;
        }
        if (dimensions == 2) {
            content = content + "driver::global_nx = 100" + NL
                + "driver::global_ny = 100" + NL
                + "# Ghost size must be stencil" + NL
                + "driver::ghost_size_x = " + stencil + NL
                + "driver::ghost_size_y = " + stencil + NL + NL;
        }
        if (dimensions == THREE) {
            content = content + "driver::global_nx = 100" + NL
                + "driver::global_ny = 100" + NL
                + "driver::global_nz = 100" + NL
                + "# Ghost size must be stencil" + NL
                + "driver::ghost_size_x = " + stencil + NL
                + "driver::ghost_size_y = " + stencil + NL
                + "driver::ghost_size_z = " + stencil + NL + NL;
        }

        //PeriodicalBoundaries
        if (problemInfo.getPeriodicalBoundary().containsValue("periodical")) {
            content = content + "PUGH::periodic = \"yes\"" + NL;
            Iterator<String> coords = problemInfo.getPeriodicalBoundary().keySet().iterator();
            int count = 1;
            while (coords.hasNext()) {
                String coord = coords.next();
                if (problemInfo.getPeriodicalBoundary().get(coord).equals("noperiodical")) {
                    String contCoord = "z";
                    if (count == 2) {
                        contCoord = "y";
                    }
                    if (count == 1) {
                        contCoord = "x";
                    }
                    content = content + "PUGH::periodic_" + contCoord + " = \"no\"" + NL;
                }
                count++;
            }
            content = content + NL;
        }

        //IO
        Hashtable<String, CactusVarGroup> varGroups = problemInfo.getGroupInfo().getVarGroups();
        String fieldsPar = "";
        Iterator<String> groupIt = varGroups.keySet().iterator();
        while (groupIt.hasNext()) {
            CactusVarGroup cvg = varGroups.get(groupIt.next());
            if (cvg.isFieldGroup()) {
                fieldsPar = fieldsPar + hi.getProblemId() + "::" + cvg.getName() + " ";
            }
        }
        fieldsPar = fieldsPar.substring(0, fieldsPar.lastIndexOf(" "));
        if (dimensions == 1) {
            //Xgraph out
            content = content + "IOASCII::out1D_every = 10" + NL
                + "IOASCII::out1D_vars = \"" + fieldsPar + "\"" + NL
                + "IOASCII::out1D_style = \"xgraph\"" + NL + NL
                + "IO::out_dir = \"" + hi.getProblemId() + "\"" + NL + NL;
        }
        else {
            //HDF5 out
            content = content + "IOHDF5::out_every = 10" + NL
                + "IOHDF5::out_vars = \"" + fieldsPar + "\"" + NL + NL
                + "IOHDF5::out_dir = \"" + hi.getProblemId() + "\"" + NL + NL;
        }
        
        //Parameters
        NodeList parameters = CodeGeneratorUtils.find(doc, "//mms:parameter");
        for (int i = 0; i < parameters.getLength(); i++) {
            String name = parameters.item(i).getFirstChild().getTextContent();
            String type = parameters.item(i).getFirstChild().getNextSibling().getTextContent();
            String defaultValue = parameters.item(i).getLastChild().getTextContent();
            content = content + hi.getProblemId() + "::" + CactusUtils.variableNormalize(name) + " = ";
            //No default value, so create a new one
            if (type.equals(defaultValue)) {
                if (type.equals("REAL")) {
                    content = content + "1.0" + NL;
                }
                if (type.equals("INT")) {
                    content = content + "1" + NL;
                }
                if (type.equals("STRING")) {
                    content = content + "\"\"" + NL;
                }
                if (type.equals("BOOLEAN")) {
                    content = content + "true" + NL;
                }
            }
            else {
                content = content + defaultValue + NL;
            }

        }
        
        //Timer output
        content = content + NL + "timerreport::all_timers_clock = \"gettimeofday\""  + NL
            + "timerreport::out_every = 1" + NL;
        
        return content;   
    }  
    
    /**
     * Creates the start up file that registers the banner in cactus.
     * @param hi                the header information
     * @return                  the content of the file
     */
    private String startupF(HeaderInfo hi) {
        return " /*@@" + NL
            + "   @file      Startup.F90" + NL
            + "   @date" + NL   
            + "   @author    " + hi.getAuthor() + NL
            + "   @desc" + NL
            + "              Register banner" + NL
            + "   @enddesc" + NL
            + "   @version " + hi.getVersion() + NL + NL
            + " @@*/" + NL + NL
            + "#include \"cctk.h\"" + NL + NL
            + IND + "integer function " + hi.getProblemId() + "_startup ()" + NL + NL
            + IND + "implicit none" + NL + NL
            + IND + "integer ierr" + NL
            + IND + "call CCTK_RegisterBanner(ierr, \"" + hi.getProblemId() + "\")" + NL
            + IND + hi.getProblemId() + "_startup = 0" + NL + NL
            + IND + "end function " + hi.getProblemId() + "_startup";
    }
    
 
    
 
    
    /**
     * Create the Functions.F file with the functions of discretization rules that are not macros.
     * @param hi                    the header information
     * @param problemInfo           The problem information
     * @return                      The function file
     * @throws CGException          CG003 Not possible to create code for the platform
     *                              CG004 External error
     */
    private String functionsF(HeaderInfo hi, ProblemInfo problemInfo) 
        throws CGException {
        try {
            String functions = "";
            String[][] params = new String [2][2];
            params[0][0] = "condition";
            params[0][1] = "false";
            params[1][0] = "indent";
            params[1][1] = "2";
            
            //Get the dimensions
            Document doc = problemInfo.getProblem();
            String dimensions = CactusUtils.getVariableDimensions(doc);
            
            NodeList functionList = CodeGeneratorUtils.find(doc, "//mms:function");
            for (int i = 0; i < functionList.getLength(); i++) {
                boolean isMacro = CodeGeneratorUtils.isMacroFunction((Element) functionList.item(i));
                //Only write non-macro functions
                if (!isMacro) {
                    //Function name
                    Element funcName = (Element) functionList.item(i).getFirstChild();
                    String paramPurpose = "IN";
                    boolean extrapolation = funcName.getTextContent().startsWith("extrapolate_");
                    if (extrapolation) {
                        paramPurpose = "INOUT";
                    }
                    //Function arguments and declaration
                    String arguments = "CCTK_ARGUMENTS";
                    String declarations = "";
                    LinkedHashMap<String, ArrayList<String>> parameterDeclaration = new LinkedHashMap<String, ArrayList<String>>();  
                    if (funcName.getNextSibling().getLocalName().equals("functionParameters")) {
                        NodeList args = funcName.getNextSibling().getChildNodes();
                        for (int j = 0; j < args.getLength(); j++) {
                            String paramName = "param" + CactusUtils.variableNormalize(args.item(j).getFirstChild().getTextContent());
                            NodeList paramOccurrences = CodeGeneratorUtils.find(functionList.item(i), 
                                    ".//mt:ci[text() = '" + args.item(j).getFirstChild().getTextContent() + "']");
                            for (int k = 0; k < paramOccurrences.getLength(); k++) {
                                paramOccurrences.item(k).setTextContent(paramName);
                            }
                            String paramType = args.item(j).getLastChild().getTextContent();
                            //Parameter type modification for extrapolation 
                            if (extrapolation) {
                                if (paramName.startsWith("paramd_") || paramName.startsWith("paramFOV_") || paramName.equals("paramFOV")) {
                                    paramType = "int";
                                }
                            }
                            ArrayList<String> paramList;
                            if (parameterDeclaration.containsKey(paramType)) {
                                paramList = parameterDeclaration.get(paramType);
                            } 
                            else {
                                paramList = new ArrayList<String>();
                            }
                            paramList.add(paramName);
                            parameterDeclaration.put(paramType, paramList);
                            
                            arguments = arguments + ", " + paramName;
                        }
                        Iterator<String> paramTypes = parameterDeclaration.keySet().iterator();
                        while (paramTypes.hasNext()) {
                            String paramType = paramTypes.next();
                            declarations = declarations + IND + IND + CactusUtils.getType(paramType) + ", INTENT(" + paramPurpose + ") :: ";
                            ArrayList<String> paramList = parameterDeclaration.get(paramType);
                            for (int j = 0; j < paramList.size(); j++) {
                                if (paramType.equals("field") || paramList.get(j).startsWith("paramd_") 
                                        || paramList.get(j).startsWith("paramFOV_") || paramList.get(j).equals("paramFOV")) {
                                    declarations = declarations + CactusUtils.variableNormalize(paramList.get(j)) + dimensions + ", ";
                                    
                                }
                                else {
                                    declarations = declarations + CactusUtils.variableNormalize(paramList.get(j)) + ", ";
                                }
                            }
                            declarations = declarations.substring(0, declarations.lastIndexOf(",")) + NL;
                        }
                        declarations = declarations + NL;
                    }

                    //Declaration of variables
                    declarations = declarations 
                        + CactusUtils.getVariableDeclaration(functionList.item(i).getLastChild(), problemInfo.getIdentifiers(), null) + NL;
                    
                    //Declaration of functions
                    declarations = declarations + CactusUtils.getFunctionDeclaration(doc, functionList.item(i).getLastChild()) + NL;
                    
                    //Declaration and initialization of deltas if used
                    declarations = declarations + CactusUtils.getAllDeltas(doc, problemInfo.getGridLimits(), 
                            problemInfo.getPeriodicalBoundary(), problemInfo.getSchemaStencil()) + NL;
                    
                    //Function instructions
                    String instructions = "";
                    
                    NodeList instructionList = functionList.item(i).getLastChild().getFirstChild().getChildNodes();
                    for (int j = 0; j < instructionList.getLength(); j++) {
                        params[1][1] = "2";
                        instructions = instructions + CactusUtils.xmlTransform(
                                    instructionList.item(j), params) + NL;
                    }
                    
                    //Insert the function return
                    instructions = instructions.replaceAll("#function#", CactusUtils.variableNormalize(funcName.getTextContent()));
                    
                    String declaration = "CCTK_REAL function ";
                    String end = "end function ";
                    if (extrapolation) {
                        declaration = "subroutine ";
                        end = "end subroutine ";
                    }
                    
                    String function = 
                        IND + declaration + CactusUtils.variableNormalize(funcName.getTextContent()) + "(" + arguments + ")" + NL
                        + IND + IND + "implicit none" + NL + NL
                        + IND + IND + "DECLARE_CCTK_ARGUMENTS" + NL 
                        + IND + IND + "DECLARE_CCTK_PARAMETERS" + NL 
                        + IND + IND + "DECLARE_CCTK_FUNCTIONS" + NL + NL
                        + declarations
                        + instructions + NL
                        + IND + end + CactusUtils.variableNormalize(funcName.getTextContent()) + NL + NL;
                    functions = functions + function;
                }
            }
            String content = "/*@@" + NL
                + "  @file      Functions.F" + NL
                + "  @date      " + hi.getDate().toString() + NL      
                + "  @author    " + hi.getAuthor() + NL
                + "  @desc" + NL 
                + "             Functions for " + hi.getProblemId() + NL
                + "             Include flux functions and discretization functions" + NL
                + "  @enddesc" + NL 
                + "  @version " + hi.getVersion() + NL
                + "@@*/" + NL + NL
                + "#include \"cctk.h\"" + NL
                + "#include \"cctk_Parameters.h\"" + NL
                + "#include \"cctk_Arguments.h\"" + NL
                + "#include \"cctk_Functions.h\"" + NL + NL
                + functions
                + generateUnitMCD(problemInfo);
            
            return content;
        } 
        catch (Exception e) {
            throw new CGException(CGException.CG00X, e.getMessage());
        }
    }
    
    
    /**
     * Creates the function that calculates the MCD for the extrapolation variables.
     * @param pi            The problem info
     * @return              The macro
     */
    private static String generateUnitMCD(ProblemInfo pi) {
        if (pi.getHardRegions().isEmpty()) {
            return "";
        }
        String parList = "";
        ArrayList<String> coords = pi.getCoordinates();
        for (int i = 0; i < pi.getDimensions(); i++) {
            parList = parList + "par" + coords.get(i) + ", ";
        }
        String conditions = "";
        for (int i = MCDMAX; i > 1; i--) {
            String cond = "";
            for (int j = 0; j < pi.getDimensions(); j++) {
                cond = cond + "mod(par" + coords.get(j) + ", " + i + ") .eq. 0 .and. ";
            }
            cond = cond.substring(0, cond.lastIndexOf(" .and."));
            conditions = conditions + IND + IND + "if (" + cond + ") then" + NL
                    + IND + IND + IND + "Unit_MCD = " + i + NL
                    + IND + IND + IND + "return" + NL
                    + IND + IND + "end if" + NL;
        }
        parList = parList.substring(0, parList.lastIndexOf(","));
        return IND + "CCTK_INT function Unit_MCD(" + parList + ")" + NL
                + IND + IND + "implicit none" + NL + NL
                + IND + IND + "CCTK_INT, INTENT(IN) :: " + parList + NL + NL
                + conditions
                + IND + IND + "Unit_MCD = 1" + NL + NL
                + IND + "end function Unit_MCD" + NL;
    }
    
    /**
     * Generate the content for the make.code.defn file.
     * @param sourceFiles       The name of all the source files included
     * @param hi                The header information
     * @return                  the content of the file
     */
    private String makeCodeDefn(String sourceFiles, HeaderInfo hi) {
        return "# Main make.code.defn file for thorn " + hi.getProblemId() + NL
            + "# $Header: /" + hi.getProblemId() + "/" + hi.getProblemId() + "/src/make.code.defn,v " 
            + hi.getVersion() + " " + hi.getDate().toString() + " " + hi.getAuthor() + " Exp $" + NL + NL
            + "# Source files in this directory" + NL
            + "SRCS = " + sourceFiles + NL + NL
            + "# Subdirectories containing source files" + NL
            + "SUBDIRS = ";
    }
}
