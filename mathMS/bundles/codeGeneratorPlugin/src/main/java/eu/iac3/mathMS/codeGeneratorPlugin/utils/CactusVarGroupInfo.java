/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.codeGeneratorPlugin.utils;

import java.util.ArrayList;
import java.util.Hashtable;

/**
 * It has the information about the variable groups and their synchronizations for Cactus.
 * @author bminyano
 *
 */
public class CactusVarGroupInfo {

    private Hashtable<String, CactusVarGroup> varGroups;
    private Hashtable<Integer, ArrayList<String>> groupSyncs;
    
    /**
     * Constructor.
     * @param varGroups     The list of the available variable groups
     * @param groupSyncs    A pair of the groups and the block number where they have to be synchronized
     */
    CactusVarGroupInfo(Hashtable<String, CactusVarGroup> varGroups, Hashtable<Integer, ArrayList<String>> groupSyncs) {
        this.varGroups = varGroups;
        this.groupSyncs = groupSyncs;
    }
    
    public Hashtable<String, CactusVarGroup> getVarGroups() {
        return varGroups;
    }
    public void setVarGroups(Hashtable<String, CactusVarGroup> varGroups) {
        this.varGroups = varGroups;
    }
    public Hashtable<Integer, ArrayList<String>> getGroupSyncs() {
        return groupSyncs;
    }
    public void setGroupSyncs(Hashtable<Integer, ArrayList<String>> groupSyncs) {
        this.groupSyncs = groupSyncs;
    }
}
