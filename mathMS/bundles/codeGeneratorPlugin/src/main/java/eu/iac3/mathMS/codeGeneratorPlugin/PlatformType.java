/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.codeGeneratorPlugin;

/**
 * PlatformType is an enum type used in the
 * {@link CodeGenerator} interface.
 *  lists the platforms supported by the plugin
 * @author bminyano
 *
 */
public enum PlatformType {
    cactus,
    simPlatSAMRAI
}

