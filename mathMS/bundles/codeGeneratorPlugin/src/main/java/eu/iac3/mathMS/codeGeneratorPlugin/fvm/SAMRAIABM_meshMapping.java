/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.codeGeneratorPlugin.fvm;

import eu.iac3.mathMS.codeGeneratorPlugin.CGException;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.CodeGeneratorUtils;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.ProblemInfo;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.SAMRAIUtils;

import java.io.File;
import java.util.ArrayList;

import org.w3c.dom.Document;


/**
 * Mapping methods for samrai regions.
 * @author bminyano
 *
 */
public final class SAMRAIABM_meshMapping {
    static final String IND = "\u0009";
    static String mtUri = "http://www.w3.org/1998/Math/MathML";
    static final int THREE = 3;
    static final String NL = System.getProperty("line.separator"); 
    static final String XSL = "common" + File.separator + "XSLTmathmlToC" + File.separator + "instructionToC.xsl";
    
    /**
     * Private constructor.
     */
    private SAMRAIABM_meshMapping() { };
    
    /**
     * Generates the mapping file.
     * @param pi                    The problem information
     * @return                      The code
     * @throws CGException          CG004 External error
     */
    public static String create(ProblemInfo pi) throws CGException {
        try {
            //Parameters for the xsl transformation. It is not a condition formula
            String[][] xslParams = new String [THREE][2];
            xslParams[0][0] =  "condition";
            xslParams[0][1] =  "false";
            xslParams[1][0] =  "indent";
            xslParams[1][1] =  "0";
            xslParams[2][0] =  "dimensions";
            xslParams[2][1] =  String.valueOf(pi.getCoordinates());
            
            int dimensions = pi.getSpatialDimensions();
            
            String mapping = "";
            
            //Declaration of spatial coordinates, and its variables for start and end.
            for (int i = 0; i < pi.getSpatialDimensions(); i++) {
                String coord = SAMRAIUtils.variableNormalize(pi.getCoordinates().get(i));
                mapping = mapping + IND + IND + "int " + coord + ", " 
                    + coord + "MapStart, " + coord + "MapEnd, " + coord + "term, previousMap" + coord + ", " + coord + "WallAcc;" + NL
                    + IND + IND + "bool interiorMap" + coord + ";" + NL;
            }
           
            //Global variable declarations
            mapping = mapping + IND + IND + "int minBlock[" + dimensions + "], maxBlock[" 
                + dimensions + "], unionsI, facePointI, ie1, ie2, ie3, proc, pcounter, working, finished, pred;" + NL
                + IND + IND + "double maxDistance, e1, e2, e3;" + NL
                + IND + IND + "bool done, modif, workingArray[mpi.getSize()], finishedArray[mpi.getSize()], workingGlobal, finishedGlobal, "
                + "workingPatchArray[level->getLocalNumberOfPatches()], finishedPatchArray[level->getLocalNumberOfPatches()], workingPatchGlobal, "
                + "finishedPatchGlobal;" + NL
                + IND + IND + "int nodes = mpi.getSize();" + NL
                + IND + IND + "int patches = level->getLocalNumberOfPatches();" + NL;
            
            //Initialization of the variables for checking
            mapping = mapping + IND + IND + "double SQRT3INV = 1.0/sqrt(3.0);" + NL + NL;
            
            //FOV initialization
            mapping = mapping + fovInitialization(pi)
                + IND + IND + "if (ln == 0) {" + NL
                + mapMathematicalInterior(pi)
                + IND + IND + "}" + NL
                + mapBoundaries(pi);
            
            return mapping + NL + NL
                + "#mappingFunctions#" + NL
                + "#mappingDeclarations#" + NL
                + "#endMappingDeclarations#" + NL
                + "#interphaseMappingFunctions#" + NL
                + "#interphaseMappingDeclarations#" + NL
                + "#x3dArrays#" + NL;

        } 
        catch (CGException e) {
            throw e;
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new CGException(CGException.CG00X, e.getMessage());
        }
    }
      
    /**
     * Maps the mathematical region interior.
     * @param pi                The problem info
     * @return                  The mapping
     * @throws CGException      CG004 External error
     */
    private static String mapMathematicalInterior(ProblemInfo pi) throws CGException {
        ArrayList<String> coordinates = pi.getCoordinates();
                
        String loop = "";
        String endLoop = "";
        String index = "";
        String preLoop = "";
        String condition = "";
        String indent = IND + IND + IND + IND;
        for (int i = 0; i < coordinates.size(); i++) {
            String coord = coordinates.get(i);
            String minValue = "0";
            String maxValue = "d_grid_geometry->getPhysicalDomain().front().numberCells()[" + i + "] - 1";
            
            preLoop = preLoop + IND + IND + IND + IND + coord + "MapStart = " + minValue + ";" + NL;
            preLoop = preLoop + IND + IND + IND + IND + coord + "MapEnd = " + maxValue + ";" + NL;
            
            loop = loop + indent + "for (" + coordinates.get(i) + " = " + coordinates.get(i) + "MapStart; " 
                + coordinates.get(i) + " <= " + coordinates.get(i) + "MapEnd; " + coordinates.get(i) + "++) {" + NL;
            index = index + coordinates.get(i) + " - boxfirst(" + i + ") + d_ghost_width, ";
            condition = condition + coordinates.get(i) + " >= boxfirst(" + i + ") - d_ghost_width && " + coordinates.get(i) + " <= boxlast("
                    + i + ") + d_ghost_width && ";
            endLoop = indent + "}" + NL + endLoop;
            indent = indent + IND;
        }
        condition = condition.substring(0, condition.lastIndexOf(" &&"));

        index = index.substring(0, index.lastIndexOf(","));
        
        return IND + IND + IND + "//Interior" + NL
                + getPatchLoop(pi, IND + IND + IND, true)
                + SAMRAIUtils.getLasts(pi, IND + IND + IND + IND, true) + NL
                + preLoop + loop
                + indent + "if (" + condition + ") {" + NL
                + assignFOV(pi.getProblem(), 1, pi.getRegionIds(), index, pi.getCoordinates(), indent + IND)
                + indent + "}" + NL
                + endLoop
                + IND + IND + IND + "}" + NL
                + IND + IND + IND + "d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);" + NL + NL;
    }
          
    /**
     * Map the boundary conditions.
     * @param pi                The problem information
     * @return                  The code
     * @throws CGException      CG00X External error
     */
    private static String mapBoundaries(ProblemInfo pi) throws CGException {
        String result = IND + IND + "//Boundaries Mapping" + NL
                + getPatchLoop(pi, IND + IND, false)
                + SAMRAIUtils.getLasts(pi, IND + IND + IND, true) + NL;
        ArrayList<String> boundPrecedence = pi.getBoundariesPrecedence();
        ArrayList<String> coordinates = pi.getCoordinates();
        String ind = IND + IND + IND + IND;
        String index = "";
        for (int i = 0; i < coordinates.size(); i++) {
            String coord = coordinates.get(i);
            index = index + coord + ", ";
        }
        index = index.substring(0, index.lastIndexOf(","));
        
        for (int i = boundPrecedence.size() - 1; i >= 0; i--) {
            String axis = boundPrecedence.get(i).substring(0, boundPrecedence.get(i).indexOf("-"));
            String side = boundPrecedence.get(i).substring(boundPrecedence.get(i).indexOf("-") + 1);
            int boundIndex = coordinates.indexOf(CodeGeneratorUtils.contToDisc(pi.getProblem(), axis)) * 2 + 1;
            int sideIdx = 0;
            if (side.toLowerCase().equals("upper")) {
                boundIndex++;
                sideIdx = 1;
            }
            
            result = result + IND + IND + IND + "//" + boundPrecedence.get(i) + NL
                    + IND + IND + IND + "if (patch->getPatchGeometry()->getTouchesRegularBoundary(" 
                    + coordinates.indexOf(CodeGeneratorUtils.contToDisc(pi.getProblem(), axis)) + ", " + sideIdx + ")) {" + NL;
            if (side.toLowerCase().equals("lower")) {
                String loop = "";
                String endLoop = "";
                String tmpInd = ind;
                for (int j = coordinates.size() - 1; j >= 0; j--) {
                    String coord = coordinates.get(j);
                    if (j == coordinates.indexOf(CodeGeneratorUtils.contToDisc(pi.getProblem(), axis))) {
                        loop = loop + tmpInd + "for (" + coord + " = 0; " + coord + " < d_ghost_width; " + coord + "++) {" + NL;
                    }
                    else {
                        loop = loop + tmpInd + "for (" + coord + " = 0; " + coord + " < " + coord + "last; " + coord + "++) {" + NL;
                    }
                    endLoop = tmpInd + "}" + NL + endLoop;
                    tmpInd = tmpInd + IND;
                }
                result = result + loop
                    + assignFOV(pi.getProblem(), -boundIndex, pi.getRegionIds(), index, pi.getCoordinates(), tmpInd)
                    + endLoop;
            }
            else {
                String loop = "";
                String endLoop = "";
                String tmpInd = ind;
                for (int j = coordinates.size() - 1; j >= 0; j--) {
                    String coord = coordinates.get(j);
                    if (j == coordinates.indexOf(CodeGeneratorUtils.contToDisc(pi.getProblem(), axis))) {
                        loop = loop + tmpInd + "for (" + coord + " = " + coord + "last - d_ghost_width; " + coord + " < " + coord + "last; " 
                                + coord + "++) {" + NL;
                    }
                    else {
                        loop = loop + tmpInd + "for (" + coord + " = 0; " + coord + " < " + coord + "last; " + coord + "++) {" + NL;
                    }
                    endLoop = tmpInd + "}" + NL + endLoop;
                    tmpInd = tmpInd + IND;
                }
                result = result + loop
                    + assignFOV(pi.getProblem(), -boundIndex, pi.getRegionIds(), index, pi.getCoordinates(), tmpInd)
                    + endLoop;
            }
            result = result + IND + IND + IND + "}" + NL;
        }
        result = result + IND + IND + "}" + NL
                + IND + IND + "d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);" + NL + NL;
        
        return result;
    }
   
    
    /**
     * Creates the patch loop structure code.
     * @param pi                The problem info
     * @param ind               The actual indent
     * @param includeInterior   If the interior and nonSync variables must be declared
     * @return                  The code
     * @throws CGException      CG003 Not possible to create code for the platform
     *                          CG00X External error
     */
    private static String getPatchLoop(ProblemInfo pi, String ind, boolean includeInterior) throws CGException {
        String interior = "";
        if (includeInterior) {
            interior = ind + IND + "double* interior = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_regridding_value_id).get())->getPointer();" + NL
                    + ind + IND + "int* nonSync = ((pdat::NodeData<int> *) patch->getPatchData(d_nonSync_regridding_tag_id).get())->getPointer();" + NL;
        }
        return  ind + "for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {" + NL
                + ind + IND + "const std::shared_ptr< hier::Patch >& patch = *p_it;" + NL + NL
                + ind + IND + "//Get the dimensions of the patch" + NL
                + ind + IND + "hier::Box pbox = patch->getBox();" + NL
                + SAMRAIUtils.createFOVDeclaration(pi.getRegionIds(), ind + IND, "patch->")
                + interior
                + ind + IND + "const hier::Index boxfirst = patch->getBox().lower();" + NL
                + ind + IND + "const hier::Index boxlast  = patch->getBox().upper();" + NL + NL
                + ind + IND + "//Get delta spaces into an array. dx, dy, dz." + NL
                + ind + IND + "const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));" + NL
                + ind + IND + "const double* dx  = patch_geom->getDx();" + NL + NL;
    }
    
    /**
     * Assign the region to the FOVs variables.
     * @param doc           The document
     * @param regionId     The region id
     * @param regionIds    All the region ids
     * @param index         The array index
     * @param coords        The coordinates
     * @param indent        The actual indent
     * @return              The code
     * @throws CGException  CG00X External error
     */
    private static String assignFOV(Document doc, int regionId, ArrayList<String> regionIds, String index, 
            ArrayList<String> coords, String indent) throws CGException {
        String fov = "";
        String segString = CodeGeneratorUtils.getRegionName(doc, regionId, coords);
        fov = fov + indent + "vector(FOV_" + segString + ", " + index + ") = 100;" + NL;
        for (int i = 0; i < regionIds.size(); i++) {
            String segId = regionIds.get(i);
            if (!segId.equals(segString)) {
                fov = fov + indent + "vector(FOV_" + segId + ", " + index + ") = 0;" + NL;
            }
        }
        
        return fov;
    }
    
    /**
     * Initialize all FOVs.
     * @param regionIds    All the region ids
     * @param index         The array index
     * @param indent        The actual indent
     * @return              The code
     * @throws CGException  CG00X External error
     */
    private static String initFOV(ArrayList<String> regionIds, String index, String indent) throws CGException {
        String fov = "";
        for (int i = 0; i < regionIds.size(); i++) {
            String segId = regionIds.get(i);
            fov = fov + indent + "vector(FOV_" + segId + ", " + index + ") = 0;" + NL;
        }
        
        return fov;
    }
    
    /**
     * Fov initialization.
     * @param pi                The problem info
     * @return                  The mapping
     * @throws CGException      CG004 External error
     */
    private static String fovInitialization(ProblemInfo pi) throws CGException {
        ArrayList<String> coordinates = pi.getCoordinates();
        String[][] xslParams = new String [THREE][2];
        xslParams[0][0] = "condition";
        xslParams[0][1] = "true";
        xslParams[1][0] = "indent";
        xslParams[1][1] = "0";
        xslParams[2][0] =  "dimensions";
        xslParams[2][1] =  String.valueOf(coordinates.size());
        
        
        String loop = "";
        String endLoop = "";
        String index = "";
        String indent = IND + IND + IND;
        for (int i = 0; i < pi.getDimensions(); i++) {
            loop = loop + indent + "for (" + coordinates.get(i) + " = 0; " 
                + coordinates.get(i) + " < " + coordinates.get(i) + "last; " + coordinates.get(i) + "++) {" + NL;
            index = index + coordinates.get(i) + ", ";
            endLoop = indent + "}" + NL + endLoop;
            indent = indent + IND;
        }
        index = index.substring(0, index.lastIndexOf(","));

        return IND + IND + "//FOV initialization" + NL
                + getPatchLoop(pi, IND + IND, true)
                + SAMRAIUtils.getLasts(pi, IND + IND + IND, true) + NL
                + loop
                + initFOV(pi.getRegionIds(), index, indent)
                + endLoop
                + IND + IND + "}" + NL
                + IND + IND + "d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);" + NL + NL;
    }
}
