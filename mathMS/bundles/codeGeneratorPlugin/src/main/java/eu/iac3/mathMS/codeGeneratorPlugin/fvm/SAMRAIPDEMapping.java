/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.codeGeneratorPlugin.fvm;

import eu.iac3.mathMS.codeGeneratorPlugin.CGException;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.CodeGeneratorUtils;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.CoordinateInfo;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.ProblemInfo;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.SAMRAIUtils;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.MappingRegionInfo;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.X3DRegion;
import eu.iac3.mathMS.documentManagerPlugin.DMException;
import eu.iac3.mathMS.documentManagerPlugin.DocumentManager;
import eu.iac3.mathMS.documentManagerPlugin.DocumentManagerImpl;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;


/**
 * Mapping methods for samrai regions.
 * @author bminyano
 *
 */
public final class SAMRAIPDEMapping {
    static final String IND = "\u0009";
    static String mtUri = "http://www.w3.org/1998/Math/MathML";
    static final String NL = System.getProperty("line.separator"); 
    static final String XSL = "common" + File.separator + "XSLTmathmlToC" + File.separator + "instructionToC.xsl";
    
    /**
     * Private constructor.
     */
    private SAMRAIPDEMapping() { };
    
    /**
     * Generates the mapping file.
     * @param pi                    The problem information
     * @return                      The code
     * @throws CGException          CG004 External error
     */
    public static String create(ProblemInfo pi) throws CGException {
        try {
            //Parameters for the xsl transformation. It is not a condition formula
            String[][] xslParams = new String [3][2];
            xslParams[0][0] =  "condition";
            xslParams[0][1] =  "false";
            xslParams[1][0] =  "indent";
            xslParams[1][1] =  "0";
            xslParams[2][0] =  "dimensions";
            xslParams[2][1] =  String.valueOf(pi.getCoordinates());
            
            int dimensions = pi.getSpatialDimensions();
            
            String mapping = "";
            //Getting all the variables used in the initial conditions
            Document problem = pi.getProblem();
            
            //Declaration of spatial coordinates, and its variables for start and end.
            String lowersAndUppers = "";
            String particleVarInitialization = "";
            for (int i = 0; i < pi.getSpatialDimensions(); i++) {
                String coord = SAMRAIUtils.variableNormalize(pi.getCoordinates().get(i));
                String contCoorc = CodeGeneratorUtils.discToCont(pi.getProblem(), coord);

                mapping = mapping + IND + IND + "int " + coord + ", " 
                    + coord + "term, previousMap" + coord + ", " + coord + "WallAcc;" + NL
                    + IND + IND + "bool interiorMap" + coord + ";" + NL;
                mapping = mapping + IND + IND + "double " + coord + "MapStart, " + coord + "MapEnd;" + NL;
                particleVarInitialization = particleVarInitialization + IND + "int " + coord + ";" + NL
                		+ IND + "double " + coord + "MapStart, " + coord + "MapEnd;" + NL
                		+ IND + contCoorc + "Glower = d_grid_geometry->getXLower()[" + i + "];" + NL
            			+ IND + contCoorc + "Gupper = d_grid_geometry->getXUpper()[" + i + "];" + NL;
               /* if (pi.getMovementInfo().itMoves()) {
                    lowersAndUppers = lowersAndUppers + IND + IND + CodeGeneratorUtils.discToCont(pi.getProblem(), coord) 
                        + "Glower = d_grid_geometry->getXLower()[" + i + "];" + NL
                        + IND + IND + CodeGeneratorUtils.discToCont(pi.getProblem(), coord) + "Gupper = d_grid_geometry->getXUpper()[" + i + "];" + NL;
                }*/
            }
           
            //Global variable declarations
            mapping = mapping + IND + IND + "int minBlock[" + dimensions + "], maxBlock[" 
                + dimensions + "], unionsI, facePointI, ie1, ie2, ie3, proc, pcounter, working, finished, pred;" + NL
                + IND + IND + "double maxDistance, e1, e2, e3;" + NL
                + IND + IND + "bool done, modif, workingArray[mpi.getSize()], finishedArray[mpi.getSize()], workingGlobal, finishedGlobal, "
                + "workingPatchArray[level->getLocalNumberOfPatches()], finishedPatchArray[level->getLocalNumberOfPatches()], workingPatchGlobal, "
                + "finishedPatchGlobal;" + NL
                + IND + IND + "int nodes = mpi.getSize();" + NL
                + IND + IND + "int patches = level->getLocalNumberOfPatches();" + NL
                + lowersAndUppers + NL;
            particleVarInitialization = particleVarInitialization + IND + "double position[" + dimensions + "], maxPosition[" + dimensions + "], minPosition[" + dimensions + "];" + NL;
            
            //Initialization of the variables for checking
            mapping = mapping + IND + IND + "double SQRT3INV = 1.0/sqrt(3.0);" + NL + NL;
            
            //FOV initialization
            mapping = mapping + IND + IND + "if (ln == 0) {" + NL
            		+ fovInitialization(pi);
           
            //For every region (first the lower precedents)
            String particleMap = "";
            for (int i = pi.getRegionPrecedence().size() - 1; i >= 0; i--) {
                String regionName = pi.getRegionPrecedence().get(i);
                
                //Initialize the grid
                String query = "/*/mms:region[descendant::mms:name = '" + regionName + "']" 
                    + "/mms:interiorModels/mms:interiorModel|/*/mms:subregions/mms:subregion[descendant::mms:name = '" + regionName + "']" 
                    + "/mms:interiorModels/mms:interiorModel";
                boolean hasInterior = CodeGeneratorUtils.find(problem, query).getLength() > 0;
                query = "/*/mms:subregions/mms:subregion[descendant::mms:name = '" + regionName + "']/mms:surfaceModels/mms:surfaceModel";
                boolean hasSurface = CodeGeneratorUtils.find(problem, query).getLength() > 0;
                query = "/*/mms:region[mms:name = '" + regionName + "']";
                boolean isFullDomain = CodeGeneratorUtils.find(problem, query).getLength() > 0;
                int regionbaseId = pi.getRegions().indexOf(regionName);
                
                ArrayList<String> x3dIds = new ArrayList<String>();
                query = "/*/mms:subregions/mms:subregion[mms:name = '" + regionName + "']/mms:location/mms:x3d/mms:id";
                NodeList x3ds = CodeGeneratorUtils.find(problem, query);
                for (int j = 0; j < x3ds.getLength(); j++) {
                	x3dIds.add(x3ds.item(j).getTextContent());
                }
                
                ArrayList<LinkedHashMap<String, CoordinateInfo>> coordLimits = new ArrayList<LinkedHashMap<String, CoordinateInfo>>();
                query = "/*/mms:region[mms:name = '" + regionName + "']/mms:spatialDomain|/*/mms:subregions/mms:subregion[mms:name = '" + regionName + "']/mms:location/mms:spatialDomain";
                NodeList spatialDomains = CodeGeneratorUtils.find(problem, query);
                for (int j = 0; j < spatialDomains.getLength(); j++) {
                	LinkedHashMap<String, CoordinateInfo> spatialDomainLimits = new LinkedHashMap<String, CoordinateInfo>();
                    NodeList coordinates = CodeGeneratorUtils.find(spatialDomains.item(j), "./mms:coordinateLimits");
                    for (int k = 0; k < coordinates.getLength(); k++) {
                        String coordinateName = coordinates.item(k).getFirstChild().getTextContent();
                        Element min = (Element) coordinates.item(k).getFirstChild().getNextSibling();
                        Element max = (Element) min.getNextSibling();
                        CoordinateInfo ci = new CoordinateInfo();
                        ci.setName(coordinateName);
                        ci.setMin((Element) min.getFirstChild());
                        ci.setMax((Element) max.getFirstChild());
                        spatialDomainLimits.put(coordinateName, ci);
                    }
                    coordLimits.add(spatialDomainLimits);
                }
                
                MappingRegionInfo regionInfo = new MappingRegionInfo(regionName, coordLimits, 
                       (regionbaseId * 2) + 1, x3dIds, hasInterior, hasSurface, isFullDomain);
                mapping = mapping + mapRegion(regionInfo, pi);
                particleMap = particleMap + mapRegionParticles(regionInfo, pi.getGridLimits(), pi.getCoordinates());
            }
          
            //Boundary mapping
            mapping = mapping
                    + IND + IND + "}" + NL
                    + mapBoundaries(pi);
            //Initial level set mapping
          /*  if (pi.getMovementInfo().itMoves()) {
                mapping = mapping + SAMRAIFVMMovement.initialLSMapping(pi);
            }*/
            
            //if stencil less than 1 not extra stencil needed for the regions
            int stencil = Math.max(pi.getSchemaStencil(), pi.getExtrapolationStencil());
            
            String particleFunctions = "";
            String particleFunctionDeclarations = "";
            if (pi.isHasParticleFields()) {
            	for (String speciesName: pi.getParticleSpecies()) {
            		mapping = mapping + IND + IND + "mapDataOnParticles<Particle_" + speciesName + ">(level, particleDistribution_" + speciesName + ", number_of_particles_" + speciesName + ", particleSeparation_" + speciesName + ", "
        					+ "domain_offset_factor_" + speciesName + ", normal_mean_" + speciesName + ", normal_stddev_" + speciesName + ", box_min_" + speciesName + ", box_max_" + speciesName + ");" + NL;
            	}
            	particleFunctions = "/*" + NL
            			+ " * Particle mapping" + NL
            			+ " */" + NL
            			+ "template<class T>" + NL
            			+ "void Problem::mapDataOnParticles(const std::shared_ptr< hier::PatchLevel >& level, std::string particleDistribution, std::vector<double> number_of_particles, std::vector<double> particleSeparation, "
        					+ "std::vector<double> domain_offset_factor, std::vector<double> normal_mean, "
        					+ "std::vector<double> normal_stddev, std::vector<double> box_min, std::vector<double> box_max)" + NL
            			+ "{" + NL
            			+ IND + "const tbox::SAMRAI_MPI& mpi(tbox::SAMRAI_MPI::getSAMRAIWorld());" + NL
            			+ particleVarInitialization
	                    + getPatchLoop(problem, pi, IND, true)
	                    + particleSupport(pi.getProblem(), pi.getRegionIds(), pi.getCoordinates()) + NL
	                    + IND + IND + "if (particleDistribution.compare(\"RANDOM\") == 0) {" + NL
	                    + CodeGeneratorUtils.incrementIndent(generateRandomParticleMap(pi), 3)
	                    + IND + IND + "} else if (particleDistribution.compare(\"NORMAL\") == 0) {" + NL
	                    + CodeGeneratorUtils.incrementIndent(generateNormalParticleMap(pi), 3)
	                    + IND + IND + "} else if (particleDistribution.compare(\"BOX\") == 0) {" + NL
	                    + generateBoxParticleMap(pi)
	            		+ mapBoundariesParticles(pi, true)
	                    + IND + IND + "} else {" + NL
	            		+ generateRegularStaggeredParticleMap(pi) 
	            		+ mapBoundariesParticles(pi, false)
	            		+ IND + IND + "}" + NL
	            		+ IND + "}" + NL
	            		+ "}" + NL;
	            particleFunctions = particleFunctions + generateFloodFillRoutineParticles(pi.getCoordinates(), pi) + NL + NL;
            }
            if (stencil > 1) {
                return mapping + NL + NL
                    + "#mappingFunctions#" + NL
                    + generateSetStencilLimitsRoutine(stencil, pi.getCoordinates()) + NL + NL
                    + generateCheckStencilRoutine(stencil, pi.getCoordinates()) + NL + NL
                    + generateFloodFillRoutine(pi.getRegionIds(), pi.getCoordinates()) + NL + NL
                    + generateCorrectFOVRoutine(pi) + NL
                    + particleFunctions
                    + "#mappingDeclarations#" + NL
                    + generateSetStencilLimitsDeclaration(pi.getCoordinates()) + NL + NL
                    + generateCheckStencilDeclaration(pi.getCoordinates()) + NL + NL
                    + generateFloodFillDeclaration(pi) + NL + NL
                    + generateCorrectFOVDeclaration() + NL + NL
                    + particleFunctionDeclarations
                    + "#endMappingDeclarations#" + NL
                    + "#interphaseMappingFunctions#" + NL
                    + generateInterphaseMappingFunction(pi) + NL + NL
                    + generateCheckStalledFunction(pi) + NL + NL
                    + "#interphaseMappingDeclarations#" + NL
                    + generateCheckStalledDeclaration(pi.getCoordinates()) + NL + NL
                    + "#x3dArrays#" + NL
                    + SAMRAIUtils.x3dArrays(pi);
            }
			return mapping + NL + NL
			    + "#mappingFunctions#" + NL
			    + generateFloodFillRoutine(pi.getRegionIds(), pi.getCoordinates()) + NL + NL
			    + generateCorrectFOVRoutine(pi) + NL
			    + particleFunctions
			    + "#mappingDeclarations#" + NL
			    + generateFloodFillDeclaration(pi) + NL + NL
			    + generateCorrectFOVDeclaration() + NL + NL
			    + particleFunctionDeclarations
			    + "#endMappingDeclarations#" + NL
			    + "#interphaseMappingFunctions#" + NL
			    + generateInterphaseMappingFunction(pi) + NL + NL
			    + generateCheckStalledFunction(pi) + NL + NL
			    + "#interphaseMappingDeclarations#" + NL
			    + generateCheckStalledDeclaration(pi.getCoordinates()) + NL + NL
			    + "#x3dArrays#" + NL
			    + SAMRAIUtils.x3dArrays(pi);

        } 
        catch (CGException e) {
            throw e;
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new CGException(CGException.CG00X, e.getMessage());
        }
    }
    
    /**
     * Generate box particle mapping.
     * @param pi			Problem information
     * @return				Code
     * @throws CGException	CG00X External error
     */
    private static String generateBoxParticleMap(ProblemInfo pi) throws CGException {
    	String particleMap = "";
    	Document problem = pi.getProblem();
        for (int i = pi.getRegionPrecedence().size() - 1; i >= 0; i--) {
            String regionName = pi.getRegionPrecedence().get(i);
            
            //Initialize the grid
            String query = "/*/mms:region[descendant::mms:name = '" + regionName + "']" 
                + "/mms:interiorModels/mms:interiorModel|/*/mms:subregions/mms:subregion[descendant::mms:name = '" + regionName + "']" 
                + "/mms:interiorModels/mms:interiorModel";
            boolean hasInterior = CodeGeneratorUtils.find(problem, query).getLength() > 0;
            query = "/*/mms:subregions/mms:subregion[descendant::mms:name = '" + regionName + "']/mms:surfaceModels/mms:surfaceModel";
            boolean hasSurface = CodeGeneratorUtils.find(problem, query).getLength() > 0;
            query = "/*/mms:region[mms:name = '" + regionName + "']";
            boolean isFullDomain = CodeGeneratorUtils.find(problem, query).getLength() > 0;
            int regionbaseId = pi.getRegions().indexOf(regionName);
            
            ArrayList<String> x3dIds = new ArrayList<String>();
            query = "/*/mms:subregions/mms:subregion[mms:name = '" + regionName + "']/mms:location/mms:x3d/mms:id";
            NodeList x3ds = CodeGeneratorUtils.find(problem, query);
            for (int j = 0; j < x3ds.getLength(); j++) {
            	x3dIds.add(x3ds.item(j).getTextContent());
            }
            
            ArrayList<LinkedHashMap<String, CoordinateInfo>> coordLimits = new ArrayList<LinkedHashMap<String, CoordinateInfo>>();
            query = "/*/mms:region[mms:name = '" + regionName + "']/mms:spatialDomain|/*/mms:subregions/mms:subregion[mms:name = '" + regionName + "']/mms:location/mms:spatialDomain";
            NodeList spatialDomains = CodeGeneratorUtils.find(problem, query);
            for (int j = 0; j < spatialDomains.getLength(); j++) {
            	LinkedHashMap<String, CoordinateInfo> spatialDomainLimits = new LinkedHashMap<String, CoordinateInfo>();
                NodeList coordinates = CodeGeneratorUtils.find(spatialDomains.item(j), "./mms:coordinateLimits");
                for (int k = 0; k < coordinates.getLength(); k++) {
                    String coordinateName = coordinates.item(k).getFirstChild().getTextContent();
                    Element min = (Element) coordinates.item(k).getFirstChild().getNextSibling();
                    Element max = (Element) min.getNextSibling();
                    CoordinateInfo ci = new CoordinateInfo();
                    ci.setName(coordinateName);
                    ci.setMin((Element) min.getFirstChild());
                    ci.setMax((Element) max.getFirstChild());
                    spatialDomainLimits.put(coordinateName, ci);
                }
                coordLimits.add(spatialDomainLimits);
            }
            
            MappingRegionInfo regionInfo = new MappingRegionInfo(regionName, coordLimits, 
                   (regionbaseId * 2) + 1, x3dIds, hasInterior, hasSurface, isFullDomain);
            particleMap = particleMap + mapRegionParticlesBox(regionInfo, pi.getGridLimits(), pi.getCoordinates());
        }
        return particleMap;
    }

    /**
     * Generate regular and staggered particle mapping.
     * @param pi			Problem information
     * @return				Code
     * @throws CGException	CG00X External error
     */
    private static String generateRegularStaggeredParticleMap(ProblemInfo pi) throws CGException {
    	String particleMap = "";
    	Document problem = pi.getProblem();
        for (int i = pi.getRegionPrecedence().size() - 1; i >= 0; i--) {
            String regionName = pi.getRegionPrecedence().get(i);
            
            //Initialize the grid
            String query = "/*/mms:region[descendant::mms:name = '" + regionName + "']" 
                + "/mms:interiorModels/mms:interiorModel|/*/mms:subregions/mms:subregion[descendant::mms:name = '" + regionName + "']" 
                + "/mms:interiorModels/mms:interiorModel";
            boolean hasInterior = CodeGeneratorUtils.find(problem, query).getLength() > 0;
            query = "/*/mms:subregions/mms:subregion[descendant::mms:name = '" + regionName + "']/mms:surfaceModels/mms:surfaceModel";
            boolean hasSurface = CodeGeneratorUtils.find(problem, query).getLength() > 0;
            query = "/*/mms:region[mms:name = '" + regionName + "']";
            boolean isFullDomain = CodeGeneratorUtils.find(problem, query).getLength() > 0;
            int regionbaseId = pi.getRegions().indexOf(regionName);
            
            ArrayList<String> x3dIds = new ArrayList<String>();
            query = "/*/mms:subregions/mms:subregion[mms:name = '" + regionName + "']/mms:location/mms:x3d/mms:id";
            NodeList x3ds = CodeGeneratorUtils.find(problem, query);
            for (int j = 0; j < x3ds.getLength(); j++) {
            	x3dIds.add(x3ds.item(j).getTextContent());
            }
            
            ArrayList<LinkedHashMap<String, CoordinateInfo>> coordLimits = new ArrayList<LinkedHashMap<String, CoordinateInfo>>();
            query = "/*/mms:region[mms:name = '" + regionName + "']/mms:spatialDomain|/*/mms:subregions/mms:subregion[mms:name = '" + regionName + "']/mms:location/mms:spatialDomain";
            NodeList spatialDomains = CodeGeneratorUtils.find(problem, query);
            for (int j = 0; j < spatialDomains.getLength(); j++) {
            	LinkedHashMap<String, CoordinateInfo> spatialDomainLimits = new LinkedHashMap<String, CoordinateInfo>();
                NodeList coordinates = CodeGeneratorUtils.find(spatialDomains.item(j), "./mms:coordinateLimits");
                for (int k = 0; k < coordinates.getLength(); k++) {
                    String coordinateName = coordinates.item(k).getFirstChild().getTextContent();
                    Element min = (Element) coordinates.item(k).getFirstChild().getNextSibling();
                    Element max = (Element) min.getNextSibling();
                    CoordinateInfo ci = new CoordinateInfo();
                    ci.setName(coordinateName);
                    ci.setMin((Element) min.getFirstChild());
                    ci.setMax((Element) max.getFirstChild());
                    spatialDomainLimits.put(coordinateName, ci);
                }
                coordLimits.add(spatialDomainLimits);
            }
            
            MappingRegionInfo regionInfo = new MappingRegionInfo(regionName, coordLimits, 
                   (regionbaseId * 2) + 1, x3dIds, hasInterior, hasSurface, isFullDomain);
            particleMap = particleMap + mapRegionParticles(regionInfo, pi.getGridLimits(), pi.getCoordinates());
        }
        return particleMap;
    }
    
    /**
     * Generate random particle mapping.
     * @param pi			Problem information
     * @return				Code
     * @throws CGException	CG00X External error
     */
    private static String generateRandomParticleMap(ProblemInfo pi) throws CGException {
    	  String numberOfParticles = "";
    	  String positions = "";
    	  String domainCond = "";
          for (int i = 0; i < pi.getSpatialDimensions(); i++) {
              String coord = SAMRAIUtils.variableNormalize(pi.getCoordinates().get(i));

        	  numberOfParticles = numberOfParticles + "(number_of_particles[" + i + "] + 2 * d_ghost_width * (dx[" + i + "]/particleSeparation[" + i + "])) * ";
        	  positions = positions + IND + "position[" + i + "] = gsl_rng_uniform(r_map) * (d_grid_geometry->getXUpper()[" + i + "] - d_grid_geometry->getXLower()[" + i + "] + 2 * d_ghost_width * dx[" + i + "]) + d_grid_geometry->getXLower()[" + i + "] - d_ghost_width * dx[" + i + "];" + NL
        			  + IND + coord + " = floor((position[" + i + "] - d_grid_geometry->getXLower()[" + i + "])/dx[" + i + "] + 1E-10);" + NL;
        	  domainCond = domainCond + coord + " >= boxfirst(" + i + ") && " + coord + " <= boxlast(" + i + ") && ";
          }
          
          Document problem = pi.getProblem();
          String regionMapping = "";
          for (int i = pi.getRegionPrecedence().size() - 1; i >= 0; i--) {
              String regionName = pi.getRegionPrecedence().get(i);
              
              //Initialize the grid
              String query = "/*/mms:region[descendant::mms:name = '" + regionName + "']" 
                  + "/mms:interiorModels/mms:interiorModel|/*/mms:subregions/mms:subregion[descendant::mms:name = '" + regionName + "']" 
                  + "/mms:interiorModels/mms:interiorModel";
              boolean hasInterior = CodeGeneratorUtils.find(problem, query).getLength() > 0;
              query = "/*/mms:subregions/mms:subregion[descendant::mms:name = '" + regionName + "']/mms:surfaceModels/mms:surfaceModel";
              boolean hasSurface = CodeGeneratorUtils.find(problem, query).getLength() > 0;
              query = "/*/mms:region[mms:name = '" + regionName + "']";
              boolean isFullDomain = CodeGeneratorUtils.find(problem, query).getLength() > 0;
              int regionbaseId = pi.getRegions().indexOf(regionName);
              
              ArrayList<String> x3dIds = new ArrayList<String>();
              query = "/*/mms:subregions/mms:subregion[mms:name = '" + regionName + "']/mms:location/mms:x3d/mms:id";
              NodeList x3ds = CodeGeneratorUtils.find(problem, query);
              for (int j = 0; j < x3ds.getLength(); j++) {
              	x3dIds.add(x3ds.item(j).getTextContent());
              }
              
              ArrayList<LinkedHashMap<String, CoordinateInfo>> coordLimits = new ArrayList<LinkedHashMap<String, CoordinateInfo>>();
              query = "/*/mms:region[mms:name = '" + regionName + "']/mms:spatialDomain|/*/mms:subregions/mms:subregion[mms:name = '" + regionName + "']/mms:location/mms:spatialDomain";
              NodeList spatialDomains = CodeGeneratorUtils.find(problem, query);
              for (int j = 0; j < spatialDomains.getLength(); j++) {
              	LinkedHashMap<String, CoordinateInfo> spatialDomainLimits = new LinkedHashMap<String, CoordinateInfo>();
                  NodeList coordinates = CodeGeneratorUtils.find(spatialDomains.item(j), "./mms:coordinateLimits");
                  for (int k = 0; k < coordinates.getLength(); k++) {
                      String coordinateName = coordinates.item(k).getFirstChild().getTextContent();
                      Element min = (Element) coordinates.item(k).getFirstChild().getNextSibling();
                      Element max = (Element) min.getNextSibling();
                      CoordinateInfo ci = new CoordinateInfo();
                      ci.setName(coordinateName);
                      ci.setMin((Element) min.getFirstChild());
                      ci.setMax((Element) max.getFirstChild());
                      spatialDomainLimits.put(coordinateName, ci);
                  }
                  coordLimits.add(spatialDomainLimits);
              }
              
              MappingRegionInfo regionInfo = new MappingRegionInfo(regionName, coordLimits, 
                     (regionbaseId * 2) + 1, x3dIds, hasInterior, hasSurface, isFullDomain);
              regionMapping = regionMapping + mapRegionParticlesRandom(problem, regionInfo, pi.getGridLimits(), pi.getCoordinates());
          }
          
          
          numberOfParticles = numberOfParticles.substring(0, numberOfParticles.lastIndexOf(" *"));
          domainCond = domainCond.substring(0, domainCond.lastIndexOf(" &&"));
          return "//All domain mapping" + NL
				+ "int numberOfParticles = " + numberOfParticles + ";" + NL
				+ "for (int n = 0; n < numberOfParticles; n++) {" + NL
				+ positions
				+ IND + "if (" + domainCond + ") {" + NL
				+ CodeGeneratorUtils.incrementIndent(regionMapping, 2)
				+ CodeGeneratorUtils.incrementIndent(mapBoundariesParticlesPosition(pi, false), 2)
				+ IND + "}" + NL
				+ "}" + NL;
    }
    
    /**
     * Generate normal particle mapping.
     * @param pi			Problem information
     * @return				Code
     * @throws CGException	CG00X External error
     */
    private static String generateNormalParticleMap(ProblemInfo pi) throws CGException {
    	  String numberOfParticles = "";
    	  String positions = "";
    	  String domainCond = "";
    	  String normalDistributions = "";
          for (int i = 0; i < pi.getSpatialDimensions(); i++) {
              String coord = SAMRAIUtils.variableNormalize(pi.getCoordinates().get(i));
              normalDistributions = normalDistributions + "std::normal_distribution<double> distribution_" + coord + "(normal_mean[" + i + "], normal_stddev[" + i + "]);" + NL;
        	  numberOfParticles = numberOfParticles + "(number_of_particles[" + i + "] + 2 * d_ghost_width * (dx[" + i + "]/particleSeparation[" + i + "])) * ";
        	  positions = positions + IND + "position[" + i + "] = distribution_" + coord + "(generator);" + NL
        			  + IND + coord + " = floor((position[" + i + "] - d_grid_geometry->getXLower()[" + i + "])/dx[" + i + "] + 1E-10);" + NL;
        	  domainCond = domainCond + coord + " >= boxfirst(" + i + ") && " + coord + " <= boxlast(" + i + ") && ";
          }
          
          Document problem = pi.getProblem();
          String regionMapping = "";
          for (int i = pi.getRegionPrecedence().size() - 1; i >= 0; i--) {
              String regionName = pi.getRegionPrecedence().get(i);
              
              //Initialize the grid
              String query = "/*/mms:region[descendant::mms:name = '" + regionName + "']" 
                  + "/mms:interiorModels/mms:interiorModel|/*/mms:subregions/mms:subregion[descendant::mms:name = '" + regionName + "']" 
                  + "/mms:interiorModels/mms:interiorModel";
              boolean hasInterior = CodeGeneratorUtils.find(problem, query).getLength() > 0;
              query = "/*/mms:subregions/mms:subregion[descendant::mms:name = '" + regionName + "']/mms:surfaceModels/mms:surfaceModel";
              boolean hasSurface = CodeGeneratorUtils.find(problem, query).getLength() > 0;
              query = "/*/mms:region[mms:name = '" + regionName + "']";
              boolean isFullDomain = CodeGeneratorUtils.find(problem, query).getLength() > 0;
              int regionbaseId = pi.getRegions().indexOf(regionName);
              
              ArrayList<String> x3dIds = new ArrayList<String>();
              query = "/*/mms:subregions/mms:subregion[mms:name = '" + regionName + "']/mms:location/mms:x3d/mms:id";
              NodeList x3ds = CodeGeneratorUtils.find(problem, query);
              for (int j = 0; j < x3ds.getLength(); j++) {
              	x3dIds.add(x3ds.item(j).getTextContent());
              }
              
              ArrayList<LinkedHashMap<String, CoordinateInfo>> coordLimits = new ArrayList<LinkedHashMap<String, CoordinateInfo>>();
              query = "/*/mms:region[mms:name = '" + regionName + "']/mms:spatialDomain|/*/mms:subregions/mms:subregion[mms:name = '" + regionName + "']/mms:location/mms:spatialDomain";
              NodeList spatialDomains = CodeGeneratorUtils.find(problem, query);
              for (int j = 0; j < spatialDomains.getLength(); j++) {
              	LinkedHashMap<String, CoordinateInfo> spatialDomainLimits = new LinkedHashMap<String, CoordinateInfo>();
                  NodeList coordinates = CodeGeneratorUtils.find(spatialDomains.item(j), "./mms:coordinateLimits");
                  for (int k = 0; k < coordinates.getLength(); k++) {
                      String coordinateName = coordinates.item(k).getFirstChild().getTextContent();
                      Element min = (Element) coordinates.item(k).getFirstChild().getNextSibling();
                      Element max = (Element) min.getNextSibling();
                      CoordinateInfo ci = new CoordinateInfo();
                      ci.setName(coordinateName);
                      ci.setMin((Element) min.getFirstChild());
                      ci.setMax((Element) max.getFirstChild());
                      spatialDomainLimits.put(coordinateName, ci);
                  }
                  coordLimits.add(spatialDomainLimits);
              }
              
              MappingRegionInfo regionInfo = new MappingRegionInfo(regionName, coordLimits, 
                     (regionbaseId * 2) + 1, x3dIds, hasInterior, hasSurface, isFullDomain);
              regionMapping = regionMapping + mapRegionParticlesRandom(problem, regionInfo, pi.getGridLimits(), pi.getCoordinates());
          }
          
          
          numberOfParticles = numberOfParticles.substring(0, numberOfParticles.lastIndexOf(" *"));
          domainCond = domainCond.substring(0, domainCond.lastIndexOf(" &&"));
          return "//All domain mapping" + NL
        		+ normalDistributions
				+ "int numberOfParticles = " + numberOfParticles + ";" + NL
				+ "int innerParticles = 0;" + NL
				+ "while (innerParticles < numberOfParticles) {" + NL
				+ positions
				+ IND + "if (" + domainCond + ") {" + NL
				+ IND + IND + "innerParticles++;" + NL
				+ CodeGeneratorUtils.incrementIndent(regionMapping, 2)
				+ CodeGeneratorUtils.incrementIndent(mapBoundariesParticlesPosition(pi, false), 2)
				+ IND + "}" + NL
				+ "}" + NL;
    }
    
    /**
     * Map the boundary conditions.
     * @param pi                The problem information
     * @return                  The code
     * @throws CGException      CG00X External error
     */
    private static String mapBoundariesParticles(ProblemInfo pi, boolean box) throws CGException {
        String result = IND + IND + IND + "//Boundaries Mapping" + NL;
        ArrayList<String> coordinates = pi.getCoordinates();
        String ind = IND + IND + IND;
        
        String limits = "";
        String loop = "";
        String endLoop = "";
        String positions = "";
        String index = "";
        String limitCondition = "";
        for (int j = coordinates.size() - 1; j >= 0; j--) {
            String discCoord = coordinates.get(j);
            loop = loop + ind + "for (int count_" + discCoord + " = 0; " + discCoord + "MapStart + count_" + discCoord + " * particleSeparation[" + j + "]"
                + " < " + discCoord + "MapEnd; count_" + discCoord + "++) {" + NL;
            if (j == coordinates.size() - 1 && !box) {
            	loop = loop + createStaggeringControl("count_" + discCoord, ind + IND, coordinates);
            }
            endLoop = ind + "}" + NL + endLoop;
            ind = ind + IND;
        }
        for (int i = 0; i < coordinates.size(); i++) {
            String coord = coordinates.get(i);
            if (i == coordinates.size() - 1 || box) {
            	limits = limits + IND + IND + IND + coord + "MapStart = d_grid_geometry->getXLower()[" + i + "] + domain_offset_factor[" + i + "] * particleSeparation[" + i + "]" 
                        + " - (floor(0.5 + d_ghost_width * (dx[" + i + "]/particleSeparation[" + i + "])))*particleSeparation[" + i + "];" + NL;	
            }
            limits = limits + IND + IND + IND + coord + "MapEnd = d_grid_geometry->getXUpper()[" + i + "] + d_ghost_width * dx[" + i + "];" + NL;
            positions = positions + ind + "position[" + i + "] = " + coord + "MapStart + count_" + coord + " * particleSeparation[" + i + "];" + NL
                    + ind + coord + " = floor((position[" + i + "] - d_grid_geometry->getXLower()[" + i + "])/dx[" + i + "] + 1E-10);" + NL;
            index = index + coord + ", ";
            limitCondition = limitCondition + coord + " >= boxfirst(" + i + ") && " + coord + " <= boxlast(" + i + ") && ";
        }
        index = index.substring(0, index.lastIndexOf(","));
        limitCondition = limitCondition.substring(0, limitCondition.lastIndexOf(" &&"));
        String boundaries = mapBoundariesParticlesPosition(pi, box);
        if (boundaries.equals("")) {
        	return "";
        }
        result = result + limits
                + loop
                + positions
                + ind + "if (" + limitCondition + ") {" + NL
                + CodeGeneratorUtils.incrementIndent(boundaries, ind.length() + 1)
                + ind + "}" + NL
                + endLoop;
        return result;
    }
    
    /**
     * Map the boundary conditions.
     * @param pi                The problem information
     * @return                  The code
     * @throws CGException      CG00X External error
     */
    private static String mapBoundariesParticlesPosition(ProblemInfo pi, boolean box) throws CGException {
        ArrayList<String> boundPrecedence = pi.getBoundariesPrecedence();
        ArrayList<String> coordinates = pi.getCoordinates();
        
        String index = "";
        for (int i = 0; i < coordinates.size(); i++) {
            String coord = coordinates.get(i);
            index = index + coord + ", ";
        }
        index = index.substring(0, index.lastIndexOf(","));
        String boundaries = "";
        for (int i = boundPrecedence.size() - 1; i >= 0; i--) {
            String axis = boundPrecedence.get(i).substring(0, boundPrecedence.get(i).indexOf("-"));
            String side = boundPrecedence.get(i).substring(boundPrecedence.get(i).indexOf("-") + 1);
            
            //Regular boundaries (but not reflection or periodical)
            if (!CodeGeneratorUtils.isParticleReflectionCondition(pi, axis, side) && pi.getPeriodicalBoundary().get(axis).equals("noperiodical")) {
            	int boundIndex = coordinates.indexOf(CodeGeneratorUtils.contToDisc(pi.getProblem(), axis)) * 2 + 1;
                String condition = "";
                int axisIndex = coordinates.indexOf(CodeGeneratorUtils.contToDisc(pi.getProblem(), axis));
                if (side.toLowerCase().equals("upper")) {
                    boundIndex++;
                    condition = CodeGeneratorUtils.contToDisc(pi.getProblem(), axis) + " > boxlast1(" + axisIndex + ")";
                    if (box) {
                    	condition = condition + " && lessEq(position[" + axisIndex + "], box_max[" + axisIndex + "])";
                    }
                }
                else {
                    condition = CodeGeneratorUtils.contToDisc(pi.getProblem(), axis) + " < boxfirst1(" + axisIndex + ")";
                    if (box) {
                    	condition = condition + " && greaterEq(position[" + axisIndex + "], box_min[" + axisIndex + "])";
                    }
                }
                boundaries = boundaries + "//" + boundPrecedence.get(i) + NL
                        + "if (" + condition + ") {" + NL
                        + IND + "hier::Index idx(" + index + ");" + NL
                        + IND + "Particles<T>* part = particleVariables->getItem(idx);" + NL
                        + IND + "T* particle = new T(0, position, -" + boundIndex + ");" + NL
                        + IND + "T* oldParticle = part->overlaps(*particle);" + NL
                        + IND + "if (oldParticle != NULL) {" + NL
                        + IND + IND + "part->deleteParticle(*oldParticle);" + NL
                        + IND + "}" + NL
                        + IND + "part->addParticle(*particle);" + NL
                        + IND + "delete particle;" + NL
                        + "}" + NL;
            }
            //Reflection conditions does not have particles inside the boundary (and must delete the existing ones)
            if (CodeGeneratorUtils.isParticleReflectionCondition(pi, axis, side)) {
                String condition = "";
                if (side.toLowerCase().equals("upper")) {
                    condition = CodeGeneratorUtils.contToDisc(pi.getProblem(), axis) + " > boxlast1(" 
                            + coordinates.indexOf(CodeGeneratorUtils.contToDisc(pi.getProblem(), axis)) + ")";
                }
                else {
                    condition = CodeGeneratorUtils.contToDisc(pi.getProblem(), axis) + " < boxfirst1(" 
                            + coordinates.indexOf(CodeGeneratorUtils.contToDisc(pi.getProblem(), axis)) + ")";
                }
                boundaries = boundaries + "//" + boundPrecedence.get(i) + NL
                        + "if (" + condition + ") {" + NL
                        + IND + "hier::Index idx(" + index + ");" + NL
                        + IND + "Particles<T>* part = particleVariables->getItem(idx);" + NL
                        + IND + "part->clearAllParticles();" + NL
                        + "}" + NL;
            }
        }
        if (boundaries.equals("")) {
        	return "";
        }
        return boundaries;
    }
    
    /**
     * Generates the initial patch iterator and its common variables.
     * @param problem       The problem
     * @param pi            The problem information
     * @param ind           The current indent
     * @param particleMap   When the loop is for particle or cell mapping
     * @return              The instructions
     * @throws CGException 
     */
    private static String getPatchLoop(Document problem, ProblemInfo pi, String ind, boolean particleMap) throws CGException {
        String cri = "";
        String particleVars = "";
        if (particleMap) {
        	for (String speciesName : pi.getParticleSpecies()) {
        		particleVars = particleVars + ind + IND + "if (std::is_same<T, Particle_" + speciesName + ">::value) {" + NL
            			+ ind + IND + IND + "tmpVar_id = d_particleVariables_" + speciesName + "_id;" + NL
            			+ ind + IND + "}" + NL;
        	}
    		particleVars = particleVars + ind + IND + "std::shared_ptr< pdat::IndexData<Particles<T>, pdat::CellGeometry > > particleVariables(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles<T>, pdat::CellGeometry >, hier::PatchData>(patch->getPatchData(tmpVar_id)));" + NL
    				+ ind + IND + "std::shared_ptr< pdat::IndexData<NonSyncs, pdat::CellGeometry > > "
                            + "nonSyncVariable(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< NonSyncs, pdat::CellGeometry >, hier::PatchData>(patch->getPatchData(d_nonSyncP_id)));" + NL
                    + ind + IND + "const hier::Index boxfirst = particleVariables->getGhostBox().lower();" + NL
                    + ind + IND + "const hier::Index boxlast  = particleVariables->getGhostBox().upper();" + NL;
        }
        else {
            cri = ind + IND + "int* region = ((pdat::CellData<int> *) patch->getPatchData(d_region_id).get())->getPointer();" + NL;
        }
        String interior = "";
        for (int i = 0; i < pi.getDimensions(); i++) {
            //Initialization of the variables
            String coordString = pi.getCoordinates().get(i);
            interior =  interior + IND + ind + "int* interior_" + coordString + " = ((pdat::CellData<int> *) patch->getPatchData(d_interior_" 
                    + coordString + "_id).get())->getPointer();" + NL;
        }
        interior = interior + ind + IND + "int* interior = ((pdat::CellData<int> *) patch->getPatchData(d_interior_regridding_value_id).get())->getPointer();" + NL
                + ind + IND + "int* nonSync = ((pdat::CellData<int> *) patch->getPatchData(d_nonSync_regridding_tag_id).get())->getPointer();" + NL;
    
        return  ind + "for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {" + NL
                + ind + IND + "const std::shared_ptr< hier::Patch > patch = *p_it;" + NL
                + ind + IND + "int tmpVar_id;" + NL
                + cri
                + interior
                + particleVars
                + ind + IND + "const hier::Index boxfirst1 = patch->getBox().lower();" + NL
                + ind + IND + "const hier::Index boxlast1  = patch->getBox().upper();" + NL + NL
                + ind + IND + "//Get delta spaces into an array. dx, dy, dz." + NL
                + ind + IND + "const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));" + NL
                + ind + IND + "const double* dx  = patch_geom->getDx();" + NL + NL
                + SAMRAIUtils.generateLasts(pi.getCoordinates(), ind + IND);
    }
  
    /**
     * Generate the code to calculate the hard boundary region distances for the fields.
     * @param problem           The problem
     * @param stalledVars       The stalled variable declaration
     * @param pi                The problem info
     * @return                  The code
     * @throws CGException      CG004 External error
     */
    private static String generateHardRegionDistances(Document problem, String stalledVars, ProblemInfo pi) throws CGException {
        String result = "";
        String indent = IND + IND + IND;
        //Open loops
        String loop = "";
        String endLoop = "";
        String coordIndex = "";
        String ratios = "";
        String distTmpVars = "";
        ArrayList<String> coordinates = pi.getCoordinates();
        for (int i = 0; i < pi.getDimensions(); i++) {
            String coord = pi.getCoordinates().get(i);
            distTmpVars = distTmpVars + IND + IND + IND + "int dist_" + coord + "_tmp, dist_" + coord + "_tmp_p, dist_" + coord + "_tmp_m, dist_"
                    + coord + "_tmp_r;" + NL;
            ratios = ratios + IND + IND + IND + "int reGrid_" + coord + ";" + NL
                    + IND + IND + IND + "if (ratio(" + i + ") == 0) reGrid_" + coord + " = 1;" + NL
                    + IND + IND + IND + "else           reGrid_" + coord + " = ratio(" + i + ");" + NL;
            loop = loop + indent + "for (int " + coord + " = 0; " + coord + " < " + coord + "last; " + coord + "++) {" + NL;
            endLoop = indent + "}" + NL + endLoop;
            indent = indent + IND;
            coordIndex = coordIndex + coord + ", ";
        }
        distTmpVars = distTmpVars + IND + IND + IND + "int dist_r, dist_r_tmp;" + NL;
        coordIndex = coordIndex.substring(0, coordIndex.lastIndexOf(","));
        
        ArrayList<String> segNBound = new ArrayList<String>();
        ArrayList<String> segNBoundId = new ArrayList<String>();
        for (int j = pi.getRegions().size() - 1; j >= 0; j--) {
            String regionName = pi.getRegions().get(j);
            segNBound.add(regionName + "I");
            segNBoundId.add(String.valueOf(((j * 2) + 1)));
            segNBound.add(regionName + "S");
            segNBoundId.add(String.valueOf(((j * 2) + 2)));
        }
        for (int j = pi.getBoundariesPrecedence().size() - 1; j >= 0; j--) {
            String boundName = pi.getBoundariesPrecedence().get(j);
            segNBound.add(boundName);
            String axis = boundName.substring(0, boundName.lastIndexOf("-"));
            String side = boundName.substring(boundName.lastIndexOf("-") + 1);
            segNBoundId.add(axis + side);
        }

        //Initialize the distance variables
        ArrayList<String> candidates = new ArrayList<String>();
        result = result + loop;
        //Fill the stalled variables in region movement
        for (int j = 0; j < segNBound.size(); j++) {
            String regionName = segNBound.get(j);
            if (pi.getHardRegionFields(regionName) != null) {
                ArrayList<String> interactions = CodeGeneratorUtils.getRegionInteractions(regionName, pi.getHardRegions(), 
                        pi.getRegions());
                for (int k = 0; k < interactions.size(); k++) {
                    ArrayList<String> hardFields = CodeGeneratorUtils.filterHardFields(regionName, interactions.get(k), pi.getHardRegions());
                    if (hardFields.size() > 0) {
                        String segName = interactions.get(k);
                        int segId = 2 * pi.getRegions().indexOf(segName) + 1;
                        if (pi.getRegionIds().contains(String.valueOf(segId))) {
                            if (!candidates.contains("stalled_" + segId)) {
                                candidates.add("stalled_" + segId);
                                result = result + indent + "vector(stalled_" + segId + ", " + coordIndex + ") = checkStalled(patch, " 
                                        + coordIndex + ", d_FOV_" + segId + "_id);" + NL;
                            }
                        }
                        segId = 2 * pi.getRegions().indexOf(segName) + 2;
                        if (pi.getRegionIds().contains(String.valueOf(segId))) {
                            if (!candidates.contains("stalled_" + segId)) {
                                candidates.add("stalled_" + segId);
                                result = result + indent + "vector(stalled_" + segId + ", " + coordIndex + ") = checkStalled(patch, " 
                                        + coordIndex + ", d_FOV_" + segId + "_id);" + NL;
                            }
                        }
                    }
                }
            }
        }
        LinkedHashMap<String, ArrayList<String>> groups = pi.getExtrapolatedFieldGroups();
    	Iterator<String> groupsIt = groups.keySet().iterator();
    	while (groupsIt.hasNext()) {
    		String groupName = groupsIt.next();
    		String groupNameVar = SAMRAIUtils.variableNormalize(groupName);
            for (int i = 0; i < pi.getDimensions(); i++) {
                String coord = coordinates.get(i);
                result = result + indent + "vector(d_" + coord + "_" + groupNameVar + ", " + coordIndex + ") = 0;" + NL;
            }
    	}
        result = result + endLoop
                 + IND + IND + "}" + NL
                 + IND + IND + "d_mapping_fill->createSchedule(level, level)->fillData(time, true);" + NL
                 + IND + IND + "for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {" + NL
                 + IND + IND + IND + "const std::shared_ptr< hier::Patch >& patch = *p_it;" + NL + NL
                 + IND + IND + IND + "//Get the dimensions of the patch" + NL
                 + IND + IND + IND + "const hier::Index boxfirst = patch->getBox().lower();" + NL
                 + IND + IND + IND + "const hier::Index boxlast  = patch->getBox().upper();" + NL
                 + IND + IND + IND + "const hier::IntVector ratio = level->getRatioToCoarserLevel();" + NL
                 + SAMRAIUtils.createFOVDeclaration(pi.getRegionIds(), IND + IND + IND, "patch->")
                 + stalledVars
                 + IND + IND + IND + "//Hard region field distance variables" + NL
                 + SAMRAIUtils.addHardFieldDistDeclaration(pi.getCoordinates(), pi.getExtrapolatedFieldGroups(), 3) + NL
                 + ratios + NL
                 + SAMRAIUtils.getLasts(pi, IND + IND + IND, true)
                 + distTmpVars + NL;
        
        //Calculate the distances
        result = result + loop;
        for (int j = 0; j < segNBound.size(); j++) {
            String regionName = segNBound.get(j);
            result = result + generateHardDistancesRegion(problem, pi, regionName, segNBoundId.get(j));
        }
        result = result + endLoop;
        return result;
    }
    
    /**
     * Generate the code to calculate the hard boundary region distances for the fields in a region.
     * @param problem           The problem
     * @param pi                The problem info
     * @param regionName       The region name
     * @param regionId         The region id
     * @return                  The code
     * @throws CGException      CG004 External error
     */
    private static String generateHardDistancesRegion(Document problem, ProblemInfo pi, String regionName, String regionId) 
        throws CGException {
        String indent = IND + IND + IND;
        String result = "";
        String coordIndex = "";
        for (int j = 0; j < pi.getDimensions(); j++) {
            indent = indent + IND;
            String coord = pi.getCoordinates().get(j);
            coordIndex = coordIndex + coord + ", ";
        }
        coordIndex = coordIndex.substring(0, coordIndex.lastIndexOf(","));
        String distTemp = "";
        for (int j = 0; j < pi.getDimensions(); j++) {
            String coord = pi.getCoordinates().get(j);
            distTemp = distTemp + indent + IND + "dist_" + coord + "_tmp = 999;" + NL
                    + indent + IND + "dist_" + coord + "_tmp_p = 999;" + NL
                    + indent + IND + "dist_" + coord + "_tmp_m = 999;" + NL
                    + indent + IND + "dist_" + coord + "_tmp_r = 999;" + NL;
        }
        if (pi.getHardRegionFields(regionName) != null) {
            result = result + indent + "if (vector(FOV_" + regionId + ", " + coordIndex + ") > 0) {" + NL
                    + distTemp
                    + indent + IND + "dist_r = 999;" + NL;
            ArrayList<String> interactions = CodeGeneratorUtils.getRegionInteractions(regionName, pi.getHardRegions(), pi.getRegions());
            for (int k = 0; k < interactions.size(); k++) {
                ArrayList<String> hardFields = CodeGeneratorUtils.filterHardFields(regionName, interactions.get(k), pi.getHardRegions());
                
                if (hardFields.size() > 0) {
                    String segName = interactions.get(k);
                    NodeList stencils = CodeGeneratorUtils.find(problem, "/*/mms:region[" 
                            + "mms:name = '" + segName + "']/mms:discretizationInfo//mms:stencil|/*/mms:subregions/mms:subregion[" 
                            + "mms:name = '" + segName + "']/mms:discretizationInfo//mms:stencil|//mms:maximumStepStencil");
                    int stencil = 0;
                    for (int i = 0; i < stencils.getLength(); i++) {
                        stencil = Math.max(stencil, Integer.parseInt(stencils.item(i).getTextContent()));
                    }
                    //Increase stencil size in 1 unit if there is movement region. Multiple regions could share the same cell so one more 
                    //is needed to be checked.
                   /* if (pi.getMovementInfo().itMoves()) {
                        stencil++;
                    }*/
                    
                    int segId = 2 * pi.getRegions().indexOf(segName) + 1;
                    String hardFieldAssigns = "";
                    String calcDists = "";
                    String initLoops = "";
                    String endLoops = "";
                    String loopIndent = indent + IND;
                    String sideTiebreak = "";
                    String insideDomainCond = "";
                    String distsAssign = "";
                    for (int i = 0; i < pi.getDimensions(); i++) {
                        String coord = pi.getCoordinates().get(i);
                        initLoops = initLoops + loopIndent + "for (int Dist_" + coord + " = -" + stencil + " * reGrid_" + coord + "; Dist_" 
                                + coord + " <= " + stencil + " * reGrid_" + coord + "; Dist_" + coord + "++) {" + NL;
                        endLoops = loopIndent + "}" + NL + endLoops;
                        loopIndent = loopIndent + IND; 
                        //Set distances
                    	LinkedHashMap<String, ArrayList<String>> groups = pi.getExtrapolatedFieldGroups();
                    	Iterator<String> groupsIt = groups.keySet().iterator();
                    	while (groupsIt.hasNext()) {
                    		String groupName = groupsIt.next();
                    		String groupNameVar = SAMRAIUtils.variableNormalize(groupName);
                    		ArrayList<String> fields = groups.get(groupName);
                    		boolean groupAssignment = false;
                    		for (int j = 0; j < fields.size(); j++) {
                    			String field = fields.get(j);
                    			if (hardFields.contains(field)) {
                    				groupAssignment = true;
                    				break;
                    			}
                    		}
                   			if (groupAssignment) {
                                hardFieldAssigns = hardFieldAssigns + indent + IND + "if (dist_" + coord + "_tmp != 999 && ((vector(d_"
                                        + coord + "_" + groupNameVar + ", " + coordIndex + ") == 0) || (fabs(dist_" 
                                        + coord + "_tmp) < fabs(vector(d_" + coord + "_" + groupNameVar + ", " + coordIndex + "))))) {" +  NL
                                        + indent + IND + IND + "vector(d_" + coord + "_" + groupNameVar + ", " + coordIndex 
                						+ ") = dist_" + coord + "_tmp;" + NL
                                        + indent + IND + "}" +  NL;
                   			}
                    	}
                        
                        insideDomainCond = insideDomainCond + coord + " + Dist_" + coord + " >= 0 && " + coord + " + Dist_" + coord + " < " 
                                + coord + "last && ";
                    }
                    String segCond = "";
                    String distMod = "";
                    String noDistCheck = "";
                    String diagonalAssign = "";
                    for (int i = 0; i < pi.getDimensions(); i++) {
                        String distIndex = "";
                        String distIndexp = "";
                        String distIndexn = "";
                        String noOtherDist = "";
                        for (int j = 0; j < pi.getDimensions(); j++) {
                            String coord = pi.getCoordinates().get(j);
                            distIndex = distIndex + coord + " + Dist_" + coord + ", ";
                            if (i == j) {
                                distIndexp = distIndexp + coord + " - dist_" + coord + "_tmp_p + Dist_" + coord + ", ";
                                distIndexn = distIndexn + coord + " - dist_" + coord + "_tmp_m + Dist_" + coord + ", ";
                            }
                            else {
                                distIndexp = distIndexp + coord + ", ";
                                distIndexn = distIndexn + coord + ", ";
                                
                                noOtherDist = noOtherDist + "Dist_" + coord + " == 0 && ";
                            }
                        }
                        distIndex = distIndex.substring(0, distIndex.lastIndexOf(","));
                        distIndexp = distIndexp.substring(0, distIndexp.lastIndexOf(","));
                        distIndexn = distIndexn.substring(0, distIndexn.lastIndexOf(","));
                        
                        segCond = "";
                        String segCondp = "";
                        String segCondn = "";
                        boolean hasInterior = pi.getRegionIds().contains(String.valueOf(segId));
                        boolean hasSurface = pi.getRegionIds().contains(String.valueOf(segId +  1));
                        segCond = "(";
                        segCondn = " && (";
                        segCondp = " && (";
                        if (hasInterior) {
                            segCond = segCond + "!vector(stalled_" + segId + ", " + distIndex + ")";
                            segCondn = segCondn + "!vector(stalled_" + segId + ", " + distIndexn + ")";
                            segCondp = segCondp + "!vector(stalled_" + segId + ", " + distIndexp + ")";
                        }
                        if (hasInterior && hasSurface) {
                            segCond = segCond + " || ";
                            segCondp = segCondp + " || ";
                            segCondn = segCondn + " || ";
                        }
                        if (hasSurface) {
                            segCond = segCond + "!vector(stalled_" + (segId + 1) + ", " + distIndex + ")";
                            segCondp = segCondp + "!vector(stalled_" + (segId + 1) + ", " + distIndexp + ")";
                            segCondn = segCondn + "!vector(stalled_" + (segId + 1) + ", " + distIndexn + ")";
                        }
                        segCond = segCond + ")";
                        segCondp = segCondp + ")";
                        segCondn = segCondn + ")";
                        
                        String coord = pi.getCoordinates().get(i);
                        distMod = distMod + "Dist_" + coord + " * Dist_" + coord + " + ";
                        distsAssign = distsAssign + loopIndent + IND + IND + "dist_" + coord + "_tmp_r = -Dist_" + coord + ";" + NL;
                        noDistCheck = noDistCheck + "dist_" + coord + "_tmp == 999 && ";
                        diagonalAssign = diagonalAssign + indent + IND + IND + "dist_" + coord + "_tmp = dist_" + coord + "_tmp_r;" + NL;
                        calcDists = calcDists
                                + loopIndent + IND + "if (Dist_" + coord + " < 0 && " + noOtherDist + "fabs(Dist_" + coord + ") < fabs(dist_" 
                                + coord + "_tmp_m)) {" + NL
                                + loopIndent + IND + IND + "dist_" + coord + "_tmp_m = -Dist_" + coord + ";" + NL
                                + loopIndent + IND + "}" + NL
                                + loopIndent + IND + "if (Dist_" + coord + " > 0 && " + noOtherDist + "fabs(Dist_" + coord + ") < fabs(dist_" 
                                + coord + "_tmp_p)) {" + NL
                                + loopIndent + IND + IND + "dist_" + coord + "_tmp_p = -Dist_" + coord + ";" + NL
                                + loopIndent + IND + "}" + NL;
                        sideTiebreak = sideTiebreak + indent + IND + "if (fabs(dist_" + coord + "_tmp_m) == fabs(dist_" + coord + "_tmp_p) && dist_" 
                                + coord + "_tmp_m < 999) {" + NL
                                + indent + IND + IND + "dist_" + coord + "_tmp = 0;" + NL
                                + indent + IND + IND + "bool enough = true;" + NL
                                + indent + IND + IND + "for (int Dist_" + coord + " = 0; Dist_" + coord + " <= " + (stencil - 1) 
                                + " && enough; Dist_" + coord + "++) {" + NL
                                + indent + IND + IND + IND + "if (!(" + coord + " - dist_" + coord + "_tmp_p + Dist_" + coord + " < " 
                                + coord + "last" + segCondp + ")) {" + NL
                                + indent + IND + IND + IND + IND + "enough = false;" + NL
                                + indent + IND + IND + IND + "}" + NL
                                + indent + IND + IND + "}" + NL
                                + indent + IND + IND + "if (enough) {" + NL
                                + indent + IND + IND + IND + "dist_" + coord + "_tmp = dist_" + coord + "_tmp_p;" + NL
                                + indent + IND + IND + "}" + NL
                                + indent + IND + IND + "enough = true;" + NL
                                + indent + IND + IND + "for (int Dist_" + coord + " = -" + (stencil - 1) + "; Dist_" + coord 
                                + " <= 0 && enough; Dist_" + coord + "++) {" + NL
                                + indent + IND + IND + IND + "if (!(" + coord + " - dist_" + coord + "_tmp_m + Dist_" + coord 
                                + " >= 0" + segCondn + ")) {" + NL
                                + indent + IND + IND + IND + IND + "enough = false;" + NL
                                + indent + IND + IND + IND + "}" + NL
                                + indent + IND + IND + "}" + NL
                                + indent + IND + IND + "if (enough) {" + NL
                                + indent + IND + IND + IND + "dist_" + coord + "_tmp = dist_" + coord + "_tmp_m;" + NL
                                + indent + IND + IND + "}" + NL
                                + indent + IND + "} else if (fabs(dist_" + coord + "_tmp_m) < fabs(dist_" + coord + "_tmp_p)) {" + NL
                                + indent + IND + IND + "dist_" + coord + "_tmp = dist_" + coord + "_tmp_m;" + NL
                                + indent + IND + "} else {" + NL
                                + indent + IND + IND + "dist_" + coord + "_tmp = dist_" + coord + "_tmp_p;" + NL
                                + indent + IND + "}" + NL;
                    }
                    distMod = distMod.substring(0, distMod.lastIndexOf(" +"));
                    noDistCheck = noDistCheck.substring(0, noDistCheck.lastIndexOf(" &&"));
                    result = result + initLoops
                            + loopIndent + "if (" + insideDomainCond + segCond  + ") {" + NL
                            + calcDists
                            + loopIndent + IND + "dist_r_tmp = " + distMod + ";" + NL
                            + loopIndent + IND + "if (dist_r_tmp < dist_r) {" + NL
                            + loopIndent + IND + IND + "dist_r = dist_r_tmp;" + NL
                            + distsAssign
                            + loopIndent + IND + "}" + NL
                            + loopIndent + "}" + NL
                            + endLoops
                            + sideTiebreak
                            + indent + IND + "if (" + noDistCheck + ") {" + NL
                            + diagonalAssign
                            + indent + IND + "}" + NL
                            + hardFieldAssigns;
                }
            }
            result = result + indent + "}" + NL;
        }
        return result;
    }
    
    /**
     * Generates the flood-fill algorithm using a stack. 
     * The recursive algorithm sometimes fails because of the size of frame stack.
     * @param regionIds    The identifiers for the regions
     * @param coordinates   The coordinates of the problem
     * @return              The code generated
     */
    private static String generateFloodFillRoutine(ArrayList<String> regionIds, ArrayList<String> coordinates) {
        int dims = coordinates.size();
        
        String index = "";
        String pIndex = "";
        String parameters = "";
        String auxiliar = "";
        String pointAssign = "";
        String coordPush = "";
        for (int i = 0; i < dims; i++) {
            String coord = coordinates.get(i);
            index = index + coord + ", ";
            pIndex = pIndex + "p." + coord + ", ";
            parameters = parameters + "int " + coord + ", ";
            auxiliar =  auxiliar + IND + "int " + coord + "last = boxlast(" + i + ")-boxfirst(" + i + ") + 2 + 2 * d_ghost_width;" + NL;
            pointAssign = pointAssign + IND + "p." + coord + " = " + coord + ";" + NL;
            
            String negIndex = "";
            String negAssign = "";
            String posIndex = "";
            String posAssign = "";
            for (int j = 0; j < dims; j++) {
                if (i == j) {
                    negIndex = negIndex + "p." + coord + " - 1, ";
                    negAssign = negAssign + IND + IND + IND + IND + "np." + coord + " = p." + coord + "-1;" + NL;
                    posIndex = posIndex + "p." + coord + " + 1, ";
                    posAssign = posAssign + IND + IND + IND + IND + "np." + coord + " = p." + coord + "+1;" + NL;
                }
                else {
                    negIndex = negIndex + "p." + coordinates.get(j) + ", ";
                    negAssign = negAssign + IND + IND + IND + IND + "np." 
                        + coordinates.get(j) + " = p." + coordinates.get(j) + ";" + NL;
                    posIndex = posIndex + "p." + coordinates.get(j) + ", ";
                    posAssign = posAssign + IND + IND + IND + IND + "np." 
                        + coordinates.get(j) + " = p." + coordinates.get(j) + ";" + NL;
                }
            }
            negIndex = negIndex.substring(0, negIndex.length() - 2);
            posIndex = posIndex.substring(0, posIndex.length() - 2);
            
            coordPush = coordPush + IND + IND + IND + "if (p." + coord + " - 1 >= 0 && vector(nonSync, " + negIndex + ") == 0) {" + NL
                + IND + IND + IND + IND + "Point np;" + NL
                + negAssign
                + IND + IND + IND + IND + "mystack.push(np);" + NL
                + IND + IND + IND + "}" + NL
                + IND + IND + IND + "if (p." + coord + " + 1 < " + coord + "last && vector(nonSync, " + posIndex + ") == 0) {" + NL
                + IND + IND + IND + IND + "Point np;" + NL
                + posAssign
                + IND + IND + IND + IND + "mystack.push(np);" + NL
                + IND + IND + IND + "}" + NL;
        }
        index = index.substring(0, index.length() - 2);
        pIndex = pIndex.substring(0, pIndex.length() - 2);
        
        String fovDeclaration = "";
        String fovCase = IND + "switch(seg) {" + NL;
        String fovAssignment = "";
        for (int i = 0; i < regionIds.size(); i++) {
            String regionId = regionIds.get(i);
            if (CodeGeneratorUtils.isNumber(regionId)) {
                fovCase = fovCase + IND + IND + "case " + regionId + ":" + NL;
                if (i == 0) {
                    fovAssignment = fovAssignment + IND + IND + IND + IND + "vector(FOV, " + pIndex + ") = 100;" + NL;
                    fovDeclaration = fovDeclaration + IND + "double* FOV;" + NL;
                } 
                else {
                    fovAssignment = fovAssignment + IND + IND + IND + IND + "vector(FOV_" + i + ", " + pIndex + ") = 0;" + NL;
                    fovDeclaration = fovDeclaration + IND + "double* FOV_" + i + ";" + NL;
                }
                for (int j = 0; j < regionIds.size(); j++) {
                    String regionId2 = regionIds.get(j);
                    if (CodeGeneratorUtils.isNumber(regionId2)) {
                        if (i == j) {
                            fovCase = fovCase + IND + IND + IND + "FOV = ((pdat::NodeData<double> *) patch->getPatchData(d_FOV_" 
                                    + regionId2 + "_id).get())->getPointer();" + NL; 
                        } 
                        else {
                            int segId = j + 1;
                            if (j > i) {
                                segId = segId - 1;
                            }
                            fovCase = fovCase + IND + IND + IND + "FOV_" + segId + " = ((pdat::NodeData<double> *) " 
                                    + "patch->getPatchData(d_FOV_" + regionId2 + "_id).get())->getPointer();" + NL;
                        }
                    }

                }
                fovCase = fovCase + IND + IND + "break;" + NL;
            }
        }
        fovCase = fovCase + IND + "}" + NL;
        
        return "// Point class for the floodfill algorithm" + NL
            + "class Point {" + NL
            + "private:" + NL
            + "public:" + NL
            + IND + "int " + index + ";" + NL
            + "};" + NL + NL
            + "void Problem::floodfill(std::shared_ptr< hier::Patch > patch, " + parameters + "int pred, int seg) const {" + NL + NL
            + fovDeclaration
            + fovCase
            + IND + "int* nonSync = ((pdat::NodeData<int> *) patch->getPatchData(d_nonSync_regridding_tag_id).get())->getPointer();" 
            + NL
            + IND + "double* interior = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_regridding_value_id).get())->getPointer();"
            + NL
            + IND + "//Get the dimensions of the patch" + NL
            + IND + "const hier::Index boxfirst = patch->getBox().lower();" + NL
            + IND + "const hier::Index boxlast  = patch->getBox().upper();" + NL
            + IND + "//Auxiliary definitions" + NL
            + auxiliar + NL
            + IND + "stack<Point> mystack;" + NL + NL
            + IND + "Point p;" + NL
            + pointAssign + NL
            + IND + "mystack.push(p);" + NL
            + IND + "while(mystack.size() > 0) {" + NL
            + IND + IND + "p = mystack.top();" + NL
            + IND + IND + "mystack.pop();" + NL
            + IND + IND + "if (vector(nonSync, " + pIndex + ") == 0) {" + NL
            + IND + IND + IND + "vector(nonSync, " + pIndex + ") = pred;" + NL
            + IND + IND + IND + "vector(interior, " + pIndex + ") = pred;" + NL
            + IND + IND + IND + "if (pred == 2) {" + NL
            + fovAssignment
            + IND + IND + IND + "}" + NL
            + coordPush
            + IND + IND + "}" + NL
            + IND + "}" + NL
            + "}" + NL;
    }
    
    /**
     * Generates the flood-fill algorithm using a stack. 
     * The recursive algorithm sometimes fails because of the size of frame stack.
     * @param pi   			The problem information
     * @return              The code generated
     * @throws CGException 
     */
    private static String generateCorrectFOVRoutine(ProblemInfo pi) throws CGException {
        String loop = "";
        String endLoop = "";
        String index = "";
        ArrayList<String> boundPrecedence = pi.getBoundariesPrecedence();
        ArrayList<String> coordinates = pi.getCoordinates();
        String indent = IND + IND;
        for (int i = 0; i < pi.getDimensions(); i++) {
            loop = loop + indent + "for (" + coordinates.get(i) + " = 0; " 
                + coordinates.get(i) + " < " + coordinates.get(i) + "last; " + coordinates.get(i) + "++) {" + NL;
            index = index + coordinates.get(i) + ", ";
            endLoop = indent + "}" + NL + endLoop;
            indent = indent + IND;
        }
        index = index.substring(0, index.lastIndexOf(","));
        
        String correctFOVS = "";
        for (int i = 0; i < boundPrecedence.size(); i++) {
            String axis = boundPrecedence.get(i).substring(0, boundPrecedence.get(i).indexOf("-"));
            String side = boundPrecedence.get(i).substring(boundPrecedence.get(i).indexOf("-") + 1);
            int boundIndex = coordinates.indexOf(CodeGeneratorUtils.contToDisc(pi.getProblem(), axis)) * 2 + 1;
            
            if (side.toLowerCase().equals("lower")) {
                correctFOVS = correctFOVS + indent + "if (vector(FOV_" + axis + "Lower, " + index + ") > 0) {" + NL
                	+ assignFOV(pi.getProblem(), -boundIndex, pi.getRegionIds(), index, pi.getCoordinates(), indent + IND)
                    + indent + "}" + NL;
            }
            else {
                boundIndex++;
                correctFOVS = correctFOVS + indent + "if (vector(FOV_" + axis + "Upper, " + index + ") > 0) {" + NL
                    	+ assignFOV(pi.getProblem(), -boundIndex, pi.getRegionIds(), index, pi.getCoordinates(), indent + IND)
                        + indent + "}" + NL;
            }
        }
        for (int i = 0; i < pi.getRegionPrecedence().size(); i++) {
            String regionName = pi.getRegionPrecedence().get(i);
            int regionbaseId = pi.getRegions().indexOf(regionName);
            String query = "/*/mms:region[descendant::mms:name = '" + regionName + "']" 
                + "/mms:interiorModels/mms:interiorModel|/*/mms:subregions/mms:subregion[descendant::mms:name = '" + regionName + "']" 
                + "/mms:interiorModels/mms:interiorModel";
            boolean hasInterior = CodeGeneratorUtils.find(pi.getProblem(), query).getLength() > 0;
            query = "/*/mms:subregions/mms:subregion[descendant::mms:name = '" + regionName + "']/mms:surfaceModels/mms:surfaceModel";
            boolean hasSurface = CodeGeneratorUtils.find(pi.getProblem(), query).getLength() > 0;
            if (hasSurface) {
            	correctFOVS = correctFOVS + indent + "if (vector(FOV_" + (regionbaseId * 2 + 2) + ", " + index + ") > 0) {" + NL
                    	+ assignFOV(pi.getProblem(), (regionbaseId * 2 + 2), pi.getRegionIds(), index, pi.getCoordinates(), indent + IND)
                        + indent + "}" + NL;	
            }
            if (hasInterior) {
            	correctFOVS = correctFOVS + indent + "if (vector(FOV_" +  + (regionbaseId * 2 + 1) + ", " + index + ") > 0) {" + NL
                    	+ assignFOV(pi.getProblem(), (regionbaseId * 2 + 1), pi.getRegionIds(), index, pi.getCoordinates(), indent + IND)
                        + indent + "}" + NL;
            }
        }
        
        return "/*" + NL
                + " * FOV correction for AMR" + NL
                + " */" + NL
        		+ "void Problem::correctFOVS(const std::shared_ptr< hier::PatchLevel >& level) {" + NL
        		+ IND + "int i, j, k;" + NL
        		+ getPatchLoop(pi, IND, false, false)
        	    + SAMRAIUtils.getLasts(pi, IND + IND, true) + NL
        	    + loop
        	    + correctFOVS
        	    + endLoop
                + IND + "}" + NL
                + "}" + NL;
    }
    
    /**
     * Generates the interphase mapping function.
     * @param pi            The problem information
     * @return              The code
     * @throws CGException  CG00X External error
     */
    private static String generateInterphaseMappingFunction(ProblemInfo pi) throws CGException {
        String startAndEnds = "";
        String startLoop = "";
        String endLoop = "";
        String actualInd = IND + IND + IND;
        String coordIndex = "";
        for (int i = 0; i < pi.getDimensions(); i++) {
            String coord = pi.getCoordinates().get(i);
            startAndEnds = startAndEnds + IND + IND + IND + "int " + coord + "MapStart = 0;" + NL
                    + IND + IND + IND + "int " + coord + "MapEnd = boxlast(" + i + ") - boxfirst(" + i + ") + 1 + 2 * d_ghost_width;" + NL;
            startLoop = startLoop + actualInd + "for (" + coord + " = " + coord + "MapStart; " + coord + " < " + coord + "MapEnd; " + coord 
                    + "++) {" + NL;
            endLoop = actualInd + "}" + NL + endLoop;
            actualInd = actualInd + IND;
            coordIndex = coordIndex + pi.getCoordinates().get(i) + ", ";
        }
        coordIndex = coordIndex.substring(0, coordIndex.lastIndexOf(","));
        String stalledVars = "";    
        ArrayList<String> segNBound = new ArrayList<String>();
        ArrayList<String> segNBoundId = new ArrayList<String>();
        for (int j = pi.getRegions().size() - 1; j >= 0; j--) {
            String regionName = pi.getRegions().get(j);
            segNBound.add(regionName + "I");
            segNBoundId.add(String.valueOf(((j * 2) + 1)));
            segNBound.add(regionName + "S");
            segNBoundId.add(String.valueOf(((j * 2) + 2)));
        }
        for (int j = pi.getBoundariesPrecedence().size() - 1; j >= 0; j--) {
            String boundName = pi.getBoundariesPrecedence().get(j);
            segNBound.add(boundName);
            String axis = boundName.substring(0, boundName.lastIndexOf("-"));
            String side = boundName.substring(boundName.lastIndexOf("-") + 1);
            segNBoundId.add(axis + side);
        }
        //Initialize the distance variables
        ArrayList<String> candidates = new ArrayList<String>();
        ArrayList<String> added = new ArrayList<String>();
        //Fill the stalled variables in region movement
        for (int j = 0; j < segNBound.size(); j++) {
            String regionName = segNBound.get(j);
            if (pi.getHardRegionFields(regionName) != null) {
                ArrayList<String> interactions = CodeGeneratorUtils.getRegionInteractions(regionName, pi.getHardRegions(), 
                        pi.getRegions());
                for (int k = 0; k < interactions.size(); k++) {
                    ArrayList<String> hardFields = CodeGeneratorUtils.filterHardFields(regionName, interactions.get(k), pi.getHardRegions());
                    if (hardFields.size() > 0) {
                        String segName = interactions.get(k);
                        int segId = 2 * pi.getRegions().indexOf(segName) + 1;
                        if (pi.getRegionIds().contains(String.valueOf(segId))) {
                            if (!candidates.contains("stalled_" + segId)) {
                                candidates.add("stalled_" + segId);
                                stalledVars = stalledVars + IND + IND + IND + "double* stalled_" + segId 
                                        + " = ((pdat::NodeData<double> *) patch->getPatchData(d_stalled_" + segId + "_id).get())->getPointer();" + NL;
                            }
                        }
                        segId = 2 * pi.getRegions().indexOf(segName) + 2;
                        if (pi.getRegionIds().contains(String.valueOf(segId))) {
                            if (!candidates.contains("stalled_" + segId)) {
                                candidates.add("stalled_" + segId);
                                stalledVars = stalledVars + IND + IND + IND + "double* stalled_" + segId 
                                        + " = ((pdat::NodeData<double> *) patch->getPatchData(d_stalled_" + segId + "_id).get())->getPointer();" + NL;
                            }
                        }
                    }
                }
            }
        }
        
        String varInit = "";
        Iterator<String> segsHard = pi.getHardRegions().keySet().iterator();
        added = new ArrayList<String>();
        while (segsHard.hasNext()) {
            String segName = segsHard.next();
            ArrayList<String> hardFields = new ArrayList<String>();
            if (!CodeGeneratorUtils.isBoundary(pi.getRegions(), segName)) {
                hardFields.addAll(pi.getHardRegionFields(segName + "I"));
                hardFields.addAll(pi.getHardRegionFields(segName + "S"));
            }
            else {
                hardFields.addAll(pi.getHardRegionFields(segName));
            }
            for (int k = 0; k < hardFields.size(); k++) {
                String field = SAMRAIUtils.variableNormalize(hardFields.get(k));
                for (int i = 0; i < pi.getDimensions(); i++) {
                    String coord = pi.getCoordinates().get(i);
                    if (!added.contains("d_" + coord + "_" + field)) {
                        added.add("d_" + coord + "_" + field);
                        varInit = varInit + actualInd + "vector(d_" + coord + "_" + field + ", " + coordIndex + ") = 0;" + NL;
                    }
                }
            }
        }

        String distances = "";
        for (int j = 0; j < segNBound.size(); j++) {
            String regionName = segNBound.get(j);
            distances = distances + generateHardDistancesRegion(pi.getProblem(), pi, regionName, segNBoundId.get(j));
        }
        
        String movementMapping = "";
       /* if (pi.getMovementInfo().itMoves()) {
            movementMapping = IND + IND + "////////////////////////////////// MOVEMENT INTERPHASE BLOCK INIT //////////////////////////////" + NL
                    + SAMRAIFVMMovement.redistancing(pi)
                    + SAMRAIFVMMovement.normalPartIntSeg(pi)
                    + SAMRAIFVMMovement.lsCellMapping(pi)
                    + SAMRAIFVMMovement.cellNormals(pi)
                    + IND + IND + "////////////////////////////////// MOVEMENT INTERPHASE BLOCK END /////////////////////////////////" + NL;
                    
        }*/
        
        String extrapolation = "";
        if (!pi.getHardRegions().isEmpty()) {
            extrapolation = IND + IND + "//Calculation of hard region distance variables" + NL
                    + IND + IND + "for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {" + NL
                    + IND + IND + IND + "const std::shared_ptr< hier::Patch >& patch = *p_it;" + NL + NL
                    + IND + IND + IND + "//Get the dimensions of the patch" + NL
                    + IND + IND + IND + "const hier::Index boxfirst = patch->getBox().lower();" + NL
                    + IND + IND + IND + "const hier::Index boxlast  = patch->getBox().upper();" + NL
                    + IND + IND + IND + "const hier::IntVector ratio = level->getRatioToCoarserLevel();" + NL
                    + SAMRAIUtils.createFOVDeclaration(pi.getRegionIds(), IND + IND + IND, "patch->")
                    + stalledVars
                    + IND + IND + IND + "//Hard region field distance variables" + NL
                    + SAMRAIUtils.addHardFieldDistDeclaration(pi.getCoordinates(), pi.getExtrapolatedFieldGroups(), 3) + NL
                    + SAMRAIUtils.getLasts(pi, IND + IND + IND, true)
                    + generateHardRegionDistances(pi.getProblem(), stalledVars, pi)
                    + IND + IND + "}" + NL
                    + IND + IND + "d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);" + NL;
        }
        
        return "void Problem::interphaseMapping(const double time ,const bool initial_time,const int ln, "
            + "const std::shared_ptr< hier::PatchLevel >& level, const int remesh) {" + NL
            + IND + "const tbox::SAMRAI_MPI& mpi(tbox::SAMRAI_MPI::getSAMRAIWorld());" + NL
            + IND + "if (remesh == 1) {" + NL
            + movementMapping
            + IND + IND + "//Update extrapolation variables" + NL
            + extrapolation
            + IND + "}" + NL
            + "}" + NL;
    }
    
    /**
     * Generates the recursive subprogram to check the stencil width in the x3d regions.
     * @param stencil       The stencil
     * @param coordinates   The coordinates of the problem
     * @return              The code generated
     */
    private static String generateSetStencilLimitsRoutine(int stencil, ArrayList<String> coordinates) {
        String currentIndent = "";
        String parameters = "";
        String intVars = "";
        String loopStart = "";
        String loopEnd = "";
        String index = "";
        String auxiliar = "";
        String cond = "";
        String interiors = "";
        String ghostAndShiftInit = "";
        for (int i = 0; i < coordinates.size(); i++) {
            String coord = coordinates.get(i);
            auxiliar =  auxiliar + IND + "int " + coord + "last = boxlast(" + i + ")-boxfirst(" 
                    + i + ") + 2 + 2 * d_ghost_width;" + NL;
            interiors = interiors + IND + "double* interior_" + coord + " = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_" 
                    + coord + "_id).get())->getPointer();" + NL;
            parameters = parameters + "int " + coord + ", ";
            index = index + "it" + coord + ", ";
            ghostAndShiftInit = ghostAndShiftInit + IND + "currentGhost" + coord + " = d_ghost_width - 1;" + NL
                    + IND + "otherSideShift" + coord + " = 0;" + NL;
            intVars = intVars + coord + "Start, " + coord + "End, currentGhost" + coord + ", otherSideShift" + coord + ", ";
            loopStart = loopStart + IND + currentIndent + "for(int it" + coord + " = " + coord + " - " + coord + "Start; it" + coord + " <= " 
                    + coord + " + " + coord + "End; it" + coord + "++) {" + NL;
            loopEnd = currentIndent + IND + "}" + NL + loopEnd;
            cond = cond + "it" + coord + " >= 0 && it" + coord + " < " + coord + "last && ";
            currentIndent = currentIndent + IND;
             
        }
        parameters = parameters.substring(0, parameters.lastIndexOf(",")) + ", int v";
        intVars = intVars.substring(0, intVars.lastIndexOf(","));
        index = index.substring(0, index.lastIndexOf(","));

        String interiorAssign = "";
        String widthCheck = "";
        for (int i = 0; i < coordinates.size(); i++) {
            String coord = coordinates.get(i);
            interiorAssign = interiorAssign + currentIndent + IND + IND + "if (" + coord + " - it" + coord + " < 0) {" + NL
                    + currentIndent + IND + IND + IND + "vector(interior_" + coord + ", " + index + ") = - (" + coord + "Start + 1) - (" 
                    + coord + " - it" + coord + ");" + NL
                    + currentIndent + IND + IND + "} else {" + NL
                    + currentIndent + IND + IND + IND + "vector(interior_" + coord + ", " + index + ") = (" + coord + "Start + 1) - (" 
                    + coord + " - it" + coord + ");" + NL
                    + currentIndent + IND + IND + "}" + NL;
            String incIndex = "";
            String decIndex = "";
            String itIndex = "";
            for (int j = 0; j < coordinates.size(); j++) {
                String lcoord = coordinates.get(j);
                if (i == j) {
                    incIndex = incIndex + ", " + lcoord + " + 1";
                    decIndex = decIndex + ", " + lcoord + " - 1";
                    itIndex = itIndex + ", it" + lcoord;
                }
                else {
                    incIndex = incIndex + ", " + lcoord;
                    decIndex = decIndex + ", " + lcoord;
                    itIndex = itIndex + ", " + lcoord;
                }
            }
            widthCheck = widthCheck + IND + "if ((" + coord + " + 1 < " + coord + "last && vector(FOV" + incIndex + ") == 0) ||  (" 
                    + coord + " - 1 >= 0 && vector(FOV" + decIndex + ") == 0)) {" + NL
                    + IND + IND + "if (" + coord + " + 1 < " + coord + "last && vector(FOV" + incIndex + ") > 0) {" + NL
                    + IND + IND + IND + "bool stop_counting = false;" + NL
                    + IND + IND + IND + "for(int it" + coord + " = " + coord + " + 1; it" + coord + " <= " + coord 
                    + " + d_ghost_width - 1 && currentGhost" + coord + " > 0; it" + coord + "++) {" + NL
                    + IND + IND + IND + IND + "if (it" + coord + " < " + coord + "last  && vector(FOV" + itIndex 
                    + ") > 0 && stop_counting == false) {" + NL
                    + IND + IND + IND + IND + IND + "currentGhost" + coord + "--;" + NL
                    + IND + IND + IND + IND + "} else {" + NL
                    + IND + IND + IND + IND + IND + "//First not interior point found" + NL
                    + IND + IND + IND + IND + IND + "if (it" + coord + " < " + coord + "last  && vector(FOV" + itIndex + ") == 0) {" + NL
                    + IND + IND + IND + IND + IND + IND + "stop_counting = true;" + NL
                    + IND + IND + IND + IND + IND + "}" + NL
                    + IND + IND + IND + IND + IND + "//Physical boundary reach" + NL
                    + IND + IND + IND + IND + IND + "if (it" + coord + " >= " + coord + "last - " + stencil 
                    + " && patch->getPatchGeometry()->getTouchesRegularBoundary (" + i + ", 1)) {" + NL
                    + IND + IND + IND + IND + IND + IND + "stop_counting = true;" + NL
                    + IND + IND + IND + IND + IND + IND + "//Calculate the number of cells the limit cannot grow" + NL
                    + IND + IND + IND + IND + IND + IND + "if (it" + coord + " + currentGhost" + coord + "/2 >= " + coord + "last - " 
                    + stencil + ") {" + NL
                    + IND + IND + IND + IND + IND + IND + IND + "otherSideShift" + coord + " = (it" + coord + "  + currentGhost" + coord 
                    + "/2) - (" + coord + "last - " + stencil + ");" + NL
                    + IND + IND + IND + IND + IND + IND + "}" + NL
                    + IND + IND + IND + IND + IND + "}" + NL
                    + IND + IND + IND + IND + "}" + NL
                    + IND + IND + IND + "}" + NL
                    + IND + IND + "}" + NL
                    + IND + IND + "if (" + coord + " - 1 >= 0 && vector(FOV" + decIndex + ") > 0) {" + NL
                    + IND + IND + IND + "bool stop_counting = false;" + NL
                    + IND + IND + IND + "for(int it" + coord + " = " + coord + " - 1; it" + coord + " >= " + coord
                    + " - d_ghost_width + 1 && currentGhost" + coord + " > 0; it" + coord + "--) {" + NL
                    + IND + IND + IND + IND + "if (it" + coord + " >= 0  && vector(FOV" + itIndex + ") > 0) {" + NL
                    + IND + IND + IND + IND + IND + "currentGhost" + coord + "--;" + NL
                    + IND + IND + IND + IND + "} else {" + NL
                    + IND + IND + IND + IND + IND + "//First not interior point found" + NL
                    + IND + IND + IND + IND + IND + "if (it" + coord + " >= 0 && vector(FOV" + itIndex + ") == 0) {" + NL
                    + IND + IND + IND + IND + IND + IND + "stop_counting = true;" + NL
                    + IND + IND + IND + IND + IND + "}" + NL 
                    + IND + IND + IND + IND + IND + "//Physical boundary reach" + NL
                    + IND + IND + IND + IND + IND + "if (it" + coord + " < " + stencil + " && patch->getPatchGeometry()->getTouchesRegularBoundary ("
                    + i + ", 0)) {" + NL
                    + IND + IND + IND + IND + IND + IND + "stop_counting = true;" + NL
                    + IND + IND + IND + IND + IND + IND + "//calculate the number of cells the limit cannot grow" + NL
                    + IND + IND + IND + IND + IND + IND + "if (it" + coord + "  -  ((currentGhost" + coord + ") - currentGhost" + coord + "/2) < "
                    + stencil + ") {" + NL
                    + IND + IND + IND + IND + IND + IND + IND + "otherSideShift" + coord + " = " + stencil + " - (it" + coord + "  - ((currentGhost"
                    + coord + ") - currentGhost" + coord + "/2));" + NL
                    + IND + IND + IND + IND + IND + IND + "}" + NL
                    + IND + IND + IND + IND + IND + "}" + NL
                    + IND + IND + IND + IND + "}" + NL
                    + IND + IND + IND + "}" + NL
                    + IND + IND + "}" + NL
                    + IND + IND + "if (currentGhost" + coord + " > 0) {" + NL
                    + IND + IND + IND + "if (" + coord + " + 1 < " + coord + "last && vector(FOV" + incIndex + ") > 0) {" + NL
                    + IND + IND + IND + IND + "shift = 0;" + NL
                    + IND + IND + IND + IND + "if(patch->getPatchGeometry()->getTouchesRegularBoundary (" + i + ", 0)) {" + NL
                    + IND + IND + IND + IND + IND + "while(" + coord + " - ((currentGhost" + coord + ") - currentGhost" + coord + "/2) + shift < "
                    + stencil + ") {" + NL
                    + IND + IND + IND + IND + IND + IND + "shift++;" + NL
                    + IND + IND + IND + IND + IND + "}" + NL
                    + IND + IND + IND + IND + "}" + NL
                    + IND + IND + IND + IND + coord + "Start = (currentGhost" + coord + " - currentGhost" + coord + "/2) - shift - otherSideShift" 
                    + coord + ";" + NL
                    + IND + IND + IND + IND + coord + "End = 0;" + NL
                    + IND + IND + IND + "} else {" + NL
                    + IND + IND + IND + IND + "if (" + coord + " - 1 >= 0 && vector(FOV" + decIndex + ") > 0) {" + NL
                    + IND + IND + IND + IND + IND + "shift = 0;" + NL
                    + IND + IND + IND + IND + IND + "if(patch->getPatchGeometry()->getTouchesRegularBoundary (" + i + ", 1)) {" + NL
                    + IND + IND + IND + IND + IND + IND + "while(" + coord + " + currentGhost" + coord + "/2 + shift >= " + coord + "last - " 
                    + stencil + ") {" + NL
                    + IND + IND + IND + IND + IND + IND + IND + "shift--;" + NL
                    + IND + IND + IND + IND + IND + IND + "}" + NL
                    + IND + IND + IND + IND + IND + "}" + NL
                    + IND + IND + IND + IND + IND + coord + "Start = 0;" + NL
                    + IND + IND + IND + IND + IND + coord + "End = currentGhost" + coord + "/2 + shift + otherSideShift" + coord + ";" + NL
                    + IND + IND + IND + IND + "} else {" + NL
                    + IND + IND + IND + IND + IND + "shift = 0;" + NL
                    + IND + IND + IND + IND + IND + "if(patch->getPatchGeometry()->getTouchesRegularBoundary (" + i + ", 0)) {" + NL
                    + IND + IND + IND + IND + IND + IND + "while(" + coord + " - ((currentGhost" + coord + ") - currentGhost" + coord 
                    + "/2) + shift < " + stencil + ") {" + NL
                    + IND + IND + IND + IND + IND + IND + IND + "shift++;" + NL
                    + IND + IND + IND + IND + IND + IND + "}" + NL
                    + IND + IND + IND + IND + IND + "}" + NL
                    + IND + IND + IND + IND + IND + "if(patch->getPatchGeometry()->getTouchesRegularBoundary (" + i + ", 1)) {" + NL
                    + IND + IND + IND + IND + IND + IND + "while(" + coord + " + currentGhost" + coord + "/2 + shift >= " + coord + "last - "
                    + stencil + ") {" + NL
                    + IND + IND + IND + IND + IND + IND + IND + "shift--;" + NL
                    + IND + IND + IND + IND + IND + IND + "}" + NL
                    + IND + IND + IND + IND + IND + "}" + NL
                    + IND + IND + IND + IND + IND + coord + "Start = (currentGhost" + coord + " - currentGhost" + coord + "/2) - shift;" + NL
                    + IND + IND + IND + IND + IND + coord + "End = currentGhost" + coord + "/2 + shift;" + NL
                    + IND + IND + IND + IND + "}" + NL
                    + IND + IND + IND + "}" + NL
                    + IND + IND + "} else {" + NL
                    + IND + IND + IND + coord + "Start = 0;" + NL
                    + IND + IND + IND + coord + "End = 0;" + NL
                    + IND + IND + "}" + NL
                    + IND + "} else {" + NL
                    + IND + IND + coord + "Start = 0;" + NL
                    + IND + IND + coord + "End = 0;" + NL
                    + IND + "}" + NL;
        }
        
        return "/*" + NL
                + " * Sets the limit for the checkstencil routine" + NL
                + " */" + NL
                + "void Problem::setStencilLimits(std::shared_ptr< hier::Patch > patch, " + parameters + ") const {" + NL
                + IND + "double* FOV = ((pdat::NodeData<double> *) patch->getPatchData(v).get())->getPointer();" + NL
                + interiors
                + IND + "//Get the dimensions of the patch" + NL
                + IND + "const hier::Index boxfirst = patch->getBox().lower();" + NL
                + IND + "const hier::Index boxlast  = patch->getBox().upper();" + NL
                + IND + "//Auxiliary definitions" + NL
                + auxiliar + NL
                + IND + "int " + intVars + ", shift;" + NL + NL
                + ghostAndShiftInit
                + IND + "//Checking width" + NL
                + widthCheck
                + IND + "//Assigning stencil limits" + NL
                + loopStart
                + currentIndent + IND + "if(" + cond + "vector(FOV, " + index + ") == 0) {" + NL
                + interiorAssign
                + currentIndent + IND + "}" + NL
                + loopEnd
                + "}";
    }
    
    /**
     * Generates the recursive subprogram to clean the thickness in the x3d regions.
     * @param stencil       The stencil
     * @param coordinates   The coordinates of the problem
     * @return              The code generated
     */
    private static String generateCheckStencilRoutine(int stencil, ArrayList<String> coordinates) {
        String parameters = "";
        String index = "";
        String auxiliar = "";
        String interiors = "";
        String initLoop = "";
        String endLoop = "";
        String insideCond = "";
        String currentIndent = IND;
        String indexIt = "";
        for (int i = 0; i < coordinates.size(); i++) {
            String coord = coordinates.get(i);
            auxiliar =  auxiliar + IND + "int " + coord + "last = boxlast(" + i + ")-boxfirst(" 
                    + i + ") + 2 + 2 * d_ghost_width;" + NL;
            parameters = parameters + "int " + coord + ", ";
            interiors = interiors + IND + "double* interior_" + coord + " = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_" 
                    + coord + "_id).get())->getPointer();" + NL;
            index = index + coord + ", ";
            initLoop = initLoop + currentIndent + "for(int it" + coord + " = " + coord + " - " + coord + "Start; it" + coord + " <= "
                    + coord + " + " + coord + "End; it" + coord + "++) {" + NL;
            endLoop = currentIndent + "}" + NL + endLoop;
            insideCond = insideCond + "it" + coord + " >= 0 && it" + coord + " < " + coord + "last && ";
            indexIt = indexIt + "it" + coord + ", ";
            currentIndent = currentIndent + IND;
        }
        parameters = parameters.substring(0, parameters.lastIndexOf(",")) + ", int v";
        index = index.substring(0, index.lastIndexOf(","));
        indexIt = indexIt.substring(0, indexIt.lastIndexOf(","));

        String variables = "";
        String interiorAssign = "";
        for (int i = 0; i < coordinates.size(); i++) {
            String coord = coordinates.get(i);
            variables = variables + IND + "int i_" + coord + " = vector(interior_" + coord + ", " + index + ");" + NL
                    + IND + "int " + coord + "Start = MAX(0, i_" + coord + ") - 1;" + NL
                    + IND + "int " + coord + "End = MAX(0, -i_" + coord + ") - 1;" + NL;
            interiorAssign = interiorAssign + currentIndent + IND + "vector(interior_" + coord + ", " + indexIt + ") = i_"
                    + coord + "  - (" + coord + " - it" + coord + ");" + NL;
        }
        
        return "/*" + NL
                + " * Checks if the point has a stencil width" + NL
                + " */" + NL
                + "void Problem::checkStencil(std::shared_ptr< hier::Patch > patch, " + parameters + ") const {" + NL
                + IND + "double* FOV = ((pdat::NodeData<double> *) patch->getPatchData(v).get())->getPointer();" + NL
                + interiors
                + IND + "//Get the dimensions of the patch" + NL
                + IND + "const hier::Index boxfirst = patch->getBox().lower();" + NL
                + IND + "const hier::Index boxlast  = patch->getBox().upper();" + NL
                + IND + "//Auxiliary definitions" + NL
                + auxiliar + NL
                + variables + NL
                + initLoop
                + currentIndent + "if(" + insideCond  + "vector(FOV, " + indexIt + ") == 0) {" + NL
                + interiorAssign
                + currentIndent + "}" + NL
                + endLoop
                + "}" + NL;
    }
    
    /**
     * Generates the recursive subprogram to clean the thickness in the x3d regions.
     * @param pi            The problem info
     * @return              The code generated
     */
    private static String generateCheckStalledFunction(ProblemInfo pi) {
        ArrayList<String> coordinates = pi.getCoordinates();
        String parameters = "";
        String index = "";
        String stencilVars = IND + "int stencilAcc";
        String calc = "";
        String cond = "";
        for (int i = 0; i < coordinates.size(); i++) {
            String coord = coordinates.get(i);
            parameters = parameters + "int " + coord + ", ";
            index = index + coord + ", ";
            stencilVars = stencilVars + ", stencilAccMax_" + coord;
            String coordIndex = "";
            for (int j = 0; j < coordinates.size(); j++) {
                String coord2 = coordinates.get(j);
                if (i == j) {
                    coordIndex = coordIndex + coord2 + "t1, ";
                }
                else {
                    coordIndex = coordIndex + coord2 + ", ";
                } 
            }
            coordIndex = coordIndex.substring(0, coordIndex.lastIndexOf(","));
            calc = calc + IND + IND + "stencilAcc = 0;" + NL
                    + IND + IND + "stencilAccMax_" + coord + " = 0;" + NL
                    + IND + IND + "for (int " + coord + "t1 = MAX(" + coord + "-d_regionMinThickness, 0); " + coord + "t1 <= MIN(" + coord 
                    + "+d_regionMinThickness, " + coord + "last - 1); " + coord + "t1++) {" + NL
                    + IND + IND + IND + "if (vector(FOV, " + coordIndex + ") > FOV_threshold) {" + NL
                    + IND + IND + IND + IND + "stencilAcc++;" + NL
                    + IND + IND + IND + "} else {" + NL
                    + IND + IND + IND + IND + "stencilAccMax_" + coord + " = MAX(stencilAccMax_" + coord + ", stencilAcc);" + NL
                    + IND + IND + IND + IND + "stencilAcc = 0;" + NL
                    + IND + IND + IND + "}" + NL
                    + IND + IND + "}" + NL
                    + IND + IND + "stencilAccMax_" + coord + " = MAX(stencilAccMax_" + coord + ", stencilAcc);" + NL;
            cond = cond + "(stencilAccMax_" + coord + " < d_regionMinThickness) || ";
        }
        parameters = parameters.substring(0, parameters.lastIndexOf(",")) + ", int v";
        index = index.substring(0, index.lastIndexOf(","));
        stencilVars = stencilVars + ";" + NL;
        cond = cond.substring(0, cond.lastIndexOf(" ||"));
        
   
        return "/*" + NL
                + " * Checks if the point has to be stalled" + NL
                + " */" + NL
                + "bool Problem::checkStalled(std::shared_ptr< hier::Patch > patch, " + parameters + ") const {" + NL
                + IND + "double* FOV = ((pdat::NodeData<double> *) patch->getPatchData(v).get())->getPointer();" + NL
                + IND + "double* interior = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_regridding_value_id).get())->getPointer();" + NL
                + IND + "//Get the dimensions of the patch" + NL
                + IND + "const hier::Index boxfirst = patch->getBox().lower();" + NL
                + IND + "const hier::Index boxlast  = patch->getBox().upper();" + NL
                + IND + "//Auxiliary definitions" + NL
                + SAMRAIUtils.getLasts(pi, IND, true) + NL
                + stencilVars
                + IND + "bool notEnoughStencil = false;" + NL
                + IND + "int FOV_threshold = 0;" + NL    
                + IND + "if (vector(FOV, " + index + ") <= FOV_threshold) {" + NL
                + IND + IND + "notEnoughStencil = true;" + NL
                + IND + "} else {" + NL
                + calc
                + IND + IND + "if (" + cond + ") {" + NL
                + IND + IND + IND + "notEnoughStencil = true;" + NL
                + IND + IND + "}" + NL 
                + IND + "}" + NL
                + IND + "return notEnoughStencil;" + NL
                + "}" + NL;
    }
    
    /**
     * Generates the declaration of the subprogram to set the limits for the stencil width in the x3d regions.
     * @param coordinates   The coordinates of the problem
     * @return              The code generated
     */
    private static String generateSetStencilLimitsDeclaration(ArrayList<String> coordinates) {
        String parameters = "";
        for (int i = 0; i < coordinates.size(); i++) {
            parameters = parameters + "int " + coordinates.get(i) + ", ";
        }
        parameters = parameters.substring(0, parameters.lastIndexOf(",")) + ", int v";
        return IND + "/*" + NL
            + IND + " * Sets the limit for the checkstencil routine" + NL
            + IND + " */" + NL
            + IND + "void setStencilLimits(std::shared_ptr< hier::Patch > patch, " + parameters + ") const;";
    }
  
    /**
     * Generates the declaration of the subprogram to increase the thickness in the x3d regions.
     * @param coordinates   The coordinates of the problem
     * @return              The code generated
     */
    private static String generateCheckStencilDeclaration(ArrayList<String> coordinates) {
        String parameters = "";
        for (int i = 0; i < coordinates.size(); i++) {
            parameters = parameters + "int " + coordinates.get(i) + ", ";
        }
        parameters = parameters.substring(0, parameters.lastIndexOf(",")) + ", int v";
        return IND + "/*" + NL
            + IND + " * Checks if the point has a stencil width" + NL
            + IND + " */" + NL
            + IND + "void checkStencil(std::shared_ptr< hier::Patch > patch, " + parameters + ") const;";
    }
    
    /**
     * Generates the declaration of the subprogram to stall the cells.
     * @param coordinates   The coordinates of the problem
     * @return              The code generated
     */
    private static String generateCheckStalledDeclaration(ArrayList<String> coordinates) {
        String parameters = "";
        for (int i = 0; i < coordinates.size(); i++) {
            parameters = parameters + "int " + coordinates.get(i) + ", ";
        }
        parameters = parameters.substring(0, parameters.lastIndexOf(",")) + ", int v";
        return IND + "/*" + NL
            + IND + " * Checks if the point has to be stalled" + NL
            + IND + " */" + NL
            + IND + "bool checkStalled(std::shared_ptr< hier::Patch > patch, " + parameters + ") const;";
    }
    
    /**
     * Generates the declaration of the flood-fill algorithm.
     * @param particles		If there are particles in the simulation
     * @param coordinates   The coordinates of the problem
     * @return              The code generated
     */
    private static String generateFloodFillDeclaration(ProblemInfo pi) {
        String parameters = "";
        String parametersParticles = "";
        for (int i = 0; i < pi.getCoordinates().size(); i++) {
            parameters = parameters + "int " + pi.getCoordinates().get(i) + ", ";
            parametersParticles = parametersParticles + "int " + pi.getCoordinates().get(i) + ", ";

        }
        String particleDeclaration = "";
        if (pi.isHasParticleFields()) {
        	particleDeclaration = IND + "template<class T>" + NL
        			+ IND + "void floodfillParticles(const hier::Patch& patch, std::vector<double> particleSeparation, std::vector<double> domain_offset_factor, " + parametersParticles + "int pred, int seg) const;" + NL;
        }
        return IND + "/*" + NL
            + IND + " * Flood-Fill algorithm" + NL
            + IND + " */" + NL
            + IND + "void floodfill(std::shared_ptr< hier::Patch > patch, " + parameters + "int pred, int seg) const;" + NL
            + particleDeclaration;
    }
    
    /**
     * Generates the declaration of the FOV correction.
     * @return              The code generated
     */
    private static String generateCorrectFOVDeclaration() {
        return IND + "/*" + NL
            + IND + " * FOV correction for AMR" + NL
            + IND + " */" + NL
            + IND + "void correctFOVS(const std::shared_ptr< hier::PatchLevel >& level);";
    }
    
    /**
     * Generates the code to fill the faces of the region.
     * It uses geometry formulas to recognize when a point is inside the face.
     * To do this a local coordinate system for the face is used. 
     * The local coordinate points are transformed to the global coordinate system.
     * See:  Finite Element Methods. Formulation for MoL and mesh refinement.
     *                      A. Arbona
     *        IAC 3 -UIB, 07120, Palma de Mallorca
     * @param pi                The problem info
     * @param regionInfo       The region information
     * @param coordinates       The coordinates
     * @param gridLimit         The limits of the grid coordinates
     * @return                  The generated code
     * @throws CGException      CG00X External error
     */
    private static String faceFilling(ProblemInfo pi, MappingRegionInfo regionInfo, ArrayList<String> coordinates, 
            LinkedHashMap<String, CoordinateInfo> gridLimit) throws CGException {
        String[][] xslParams = new String [3][2];
        xslParams[0][0] = "condition";
        xslParams[0][1] = "true";
        xslParams[1][0] = "indent";
        xslParams[1][1] = "0";
        xslParams[2][0] =  "dimensions";
        xslParams[2][1] =  String.valueOf(coordinates.size());
        
        String regionName = regionInfo.getRegionName();
        String currentIndent = IND + IND + IND + IND;
        String maxMin = "";
        String indexComparator = "";
        String terms = "";
        String term = "";
        String timeslice = "";
       /* if (pi.getMovementInfo().getX3dMovRegions().contains(regionName)) {
            timeslice = "[timeSlice]";
        }*/
        for (int i = 0; i < coordinates.size(); i++) {
            term = term + currentIndent + IND + IND + coordinates.get(i) + "term = round((" + regionName + "Points" 
                + timeslice + "[" + regionName + "Unions[unionsI][facePointI]][" + i + "] - d_grid_geometry->getXLower()[" + i 
                + "]) / dx[" + i + "]);" + NL;
            terms = terms + currentIndent + IND + IND + IND + IND + IND + IND + coordinates.get(i) 
                + "term = round(((" + regionName + "Points" + timeslice + "[" + regionName + "Unions[unionsI][0]][" + i 
                + "] - d_grid_geometry->getXLower()[" + i + "]) / dx[" + i + "]) * e1 + ((" + regionName + "Points" + timeslice + "[" + regionName 
                + "Unions[unionsI][1]][" + i + "] - d_grid_geometry->getXLower()[" + i + "]) / dx[" + i + "]) * e2 + ((" + regionName 
                + "Points" + timeslice + "[" + regionName + "Unions[unionsI][2]][" + i + "] - d_grid_geometry->getXLower()[" + i + "]) / dx[" + i 
                + "]) * e3);" + NL;
        
            
            //Index for the initial assignment of max and min coordinates for face filling process
            maxMin = maxMin + currentIndent + IND + IND + IND + "minBlock[" + i + "] = " + coordinates.get(i) + "term;" + NL
                 + currentIndent + IND + IND + IND + "maxBlock[" + i + "] = " + coordinates.get(i) + "term;" + NL;
            //Comparations of the index
            indexComparator = indexComparator + currentIndent + IND + IND + IND + "if (" + coordinates.get(i) 
                + "term < minBlock[" + i + "]) {" + NL
                + currentIndent + IND + IND + IND + IND + "minBlock[" + i + "] = " + coordinates.get(i) + "term;" + NL
                + currentIndent + IND + IND + IND + "}" + NL
                + currentIndent + IND + IND + IND + "if (" + coordinates.get(i) + "term > maxBlock[" + i + "]) {" + NL
                + currentIndent + IND + IND + IND + IND + "maxBlock[" + i + "] = " + coordinates.get(i) + "term;" + NL
                + currentIndent + IND + IND + IND + "}" + NL;
        }
        //Fill the points in the faces
        String insidePatch = "";
        String maxDistance = "MAX(";
        String insidePatchPoint = "";
        for (int j = coordinates.size() - 1; j >= 0; j--) {
            insidePatch = "minBlock[" + j + "] <= boxlast(" + j + ") + 1 + d_ghost_width && maxBlock[" + j + "] >= boxfirst(" 
                + j + ") - d_ghost_width && " + insidePatch;
            insidePatchPoint = coordinates.get(j) + "term >= boxfirst(" + j + ") - d_ghost_width && " + coordinates.get(j) 
                + "term <= boxlast(" + j + ") + d_ghost_width && " + insidePatchPoint;
            if (j % 3 == 1 && coordinates.size() == 3) {
                maxDistance = maxDistance + "MAX(abs(minBlock[" + j + "] - maxBlock[" + j + "]),";
            }
            else {
                maxDistance = maxDistance + "abs(minBlock[" + j + "] - maxBlock[" + j + "]),";
            }
        }
        insidePatchPoint = insidePatchPoint.substring(0, insidePatchPoint.lastIndexOf(" &&"));
        maxDistance = maxDistance.substring(0, maxDistance.lastIndexOf(",")) + ")";
        if (coordinates.size() == 3) {
            maxDistance = maxDistance + ")";
        }
        insidePatch = insidePatch.substring(0, insidePatch.lastIndexOf(" &&"));

        return currentIndent + "//Fill the faces" + NL
            + currentIndent + "for (unionsI = 0; unionsI < UNIONS_" + regionName + "; unionsI++) {" + NL
            + currentIndent + IND + "for (facePointI = 0; facePointI < DIMENSIONS; facePointI++) {" + NL
            + term + currentIndent + IND + IND + "//Take maximum and minimum coordinates of the face" + NL
            + currentIndent + IND + IND + "if (facePointI == 0) {" + NL
            + maxMin + currentIndent + IND + IND + "} else {" + NL
            + indexComparator + currentIndent + IND + IND + "}" + NL
            + currentIndent + IND + "}" + NL
            + currentIndent + IND + "if (" + insidePatch + ") {" + NL
            + currentIndent + IND + IND + "maxDistance = " + maxDistance + ";" + NL
            + fillFace(regionName, pi, coordinates, terms, insidePatchPoint, regionInfo.getId())
            + currentIndent + IND + "}" + NL
            + currentIndent + "}" + NL;
    }
    
    /**
     * Generates the code to check the stencil width at the points in the triangle.
     * @param regionName           The region name
     * @param pi                    The problem information
     * @param coordinates           The coordinates of the problem
     * @param terms                 The conversion terms from the geometric algorithm
     * @param insidePatchPoint      The code that checks if the point is inside the patch
     * @param regionId             The region id
     * @return                      The code
     * @throws CGException          CG00X External error
     */
    private static String fillFace(String regionName, ProblemInfo pi, ArrayList<String> coordinates, String terms, String insidePatchPoint, 
            int regionId) throws CGException {
        String currentIndent = IND + IND + IND + IND + IND + IND;

        String termIndex = "";
        for (int j = coordinates.size() - 1; j >= 0; j--) {
            termIndex = "index" + coordinates.get(j) + ", " + termIndex;
        }
        String timeslice = "";
        /*if (pi.getMovementInfo().getX3dMovRegions().contains(regionName)) {
            timeslice = "[timeSlice]";
        }*/
        termIndex = termIndex.substring(0, termIndex.lastIndexOf(","));
        if (pi.getDimensions() == 2) {
            return currentIndent + "for (int j = minBlock[1]; j <= maxBlock[1]; j++) {" + NL
                + currentIndent + IND + "for (int i = minBlock[0]; i <= maxBlock[0]; i++) {" + NL
                + currentIndent + IND + IND + "bool s1 = (" + regionName + "Points" + timeslice + "[" 
                + regionName + "Unions[unionsI][0]][0] - d_grid_geometry->getXLower()[0] - (i - 0.5)*dx[0])*(" 
                + regionName + "Points" + timeslice + "[" + regionName + "Unions[unionsI][1]][1] - "
                + "d_grid_geometry->getXLower()[1] - (j - 0.5)*dx[1]) >= (" + regionName + "Points" + timeslice + "[" + regionName 
                + "Unions[unionsI][1]][0] - d_grid_geometry->getXLower()[0] - (i - 0.5)*dx[0])*(" 
                + regionName + "Points" + timeslice + "[" + regionName 
                + "Unions[unionsI][0]][1] - d_grid_geometry->getXLower()[1] - (j - 0.5)*dx[1]);" + NL
                + currentIndent + IND + IND + "bool s2 = (" + regionName + "Points" + timeslice + "[" + regionName 
                + "Unions[unionsI][0]][0] - d_grid_geometry->getXLower()[0] - (i + 0.5)*dx[0])*(" 
                + regionName + "Points" + timeslice + "[" + regionName + "Unions[unionsI][1]][1] - d_grid_geometry->getXLower()[1] -"
                + " (j - 0.5)*dx[1]) >= (" + regionName + "Points" + timeslice + "[" + regionName 
                + "Unions[unionsI][1]][0] - d_grid_geometry->getXLower()[0] - (i + 0.5)*dx[0])*(" + regionName + "Points" 
                + timeslice + "[" + regionName 
                + "Unions[unionsI][0]][1] - d_grid_geometry->getXLower()[1] - (j - 0.5)*dx[1]);" + NL
                + currentIndent + IND + IND + "bool s3 = (" + regionName + "Points" + timeslice + "[" 
                + regionName + "Unions[unionsI][0]][0] - d_grid_geometry->getXLower()[0] - (i - 0.5)*dx[0])*(" 
                + regionName + "Points" + timeslice + "[" + regionName + "Unions[unionsI][1]][1] - d_grid_geometry->getXLower()[1] - "
                + "(j + 0.5)*dx[1]) >= (" + regionName + "Points" + timeslice + "[" + regionName 
                + "Unions[unionsI][1]][0] - d_grid_geometry->getXLower()[0] - (i - 0.5)*dx[0])*(" + regionName + "Points" 
                + timeslice + "[" + regionName  + "Unions[unionsI][0]][1] - d_grid_geometry->getXLower()[1] - (j + 0.5)*dx[1]);" + NL
                + currentIndent + IND + IND + "bool s4 = (" + regionName + "Points" + timeslice + "[" + regionName 
                + "Unions[unionsI][0]][0] - d_grid_geometry->getXLower()[0] - (i + 0.5)*dx[0])*(" 
                + regionName + "Points" + timeslice + "[" + regionName + "Unions[unionsI][1]][1] - d_grid_geometry->getXLower()[1] - "
                + "(j + 0.5)*dx[1]) >= (" + regionName + "Points" + timeslice + "[" + regionName 
                + "Unions[unionsI][1]][0] - d_grid_geometry->getXLower()[0] - (i + 0.5)*dx[0])*(" + regionName + "Points" 
                + timeslice + "[" + regionName + "Unions[unionsI][0]][1] - d_grid_geometry->getXLower()[1] - (j + 0.5)*dx[1]);" + NL
                + currentIndent + IND + IND + "if (!((s1 && s2 && s3 && s4) || (!s1 && !s2 && !s3 && !s4))) {" + NL
                + currentIndent + IND + IND + IND + "if (i >= boxfirst(0) - d_ghost_width && i <= boxlast(0) + d_ghost_width && j >= boxfirst(1) - " 
                + "d_ghost_width && j <= boxlast(1) + d_ghost_width) {" + NL
                + x3DIndexCorrection(currentIndent + IND + IND + IND + IND, pi.getCoordinates())
                + assignFOV(pi.getProblem(), regionId, pi.getRegionIds(), termIndex, pi.getCoordinates(), 
                        currentIndent + IND + IND + IND + IND)
                + currentIndent + IND + IND + IND + "}" + NL
                + currentIndent + IND + IND + "}" + NL
                + currentIndent + IND + "}" + NL
                + currentIndent + "}" + NL;
        }
		return currentIndent + "for (ie3 = 0; ie3 < 3 * maxDistance; ie3++) {" + NL
		    + currentIndent + IND + "e3 = ie3 / (3 * maxDistance);" + NL 
		    + currentIndent + IND + "for (ie2 = 0; ie2 < 3 * maxDistance; ie2++) {" + NL
		    + currentIndent + IND + IND + "e2 = ie2 / (3 * maxDistance);" + NL
		    + currentIndent + IND + IND + "for (ie1 = 0; ie1 < 3 * maxDistance; ie1++) {" + NL
		    + currentIndent + IND + IND + IND + "e1 = ie1 / (3 * maxDistance);" + NL 
		    + currentIndent + IND + IND + IND + "if (abs(ie1 + ie2 + ie3 - 3 * maxDistance) <= SQRT3INV) {" + NL 
		    + terms 
		    + currentIndent + IND + IND + IND + IND + "if (" + insidePatchPoint + ") {" + NL
		    + x3DIndexCorrection(currentIndent + IND + IND + IND + IND + IND, pi.getCoordinates())
		    + assignFOV(pi.getProblem(), regionId, pi.getRegionIds(), termIndex, pi.getCoordinates(), 
		            currentIndent + IND + IND + IND + IND + IND)
		    + currentIndent + IND + IND + IND + IND + "}" + NL
		    + currentIndent + IND + IND + IND + "}" + NL
		    + currentIndent + IND + IND + "}" + NL
		    + currentIndent + IND + "}" + NL
		    + currentIndent + "}" + NL;
    }
    
    /**
     * Generates the x3d index correction code. It corrects the limited upper sided x3d points not to be at boundaries, but at the internal domain.
     * @param indent        The indent
     * @param coordinates   The coordinates
     * @return              The code
     */
    private static String x3DIndexCorrection(String indent, ArrayList<String> coordinates) {
        String index = "";
        String conditions = "";
        for (int i = 0; i < coordinates.size(); i++) {
            String coord = coordinates.get(i);
            if (coordinates.size() == 2) {
                index = index + indent + "int index" + coord + " = " + coord + " - boxfirst(" + i + ") + d_ghost_width;" + NL;
                conditions = conditions + indent + "if (patch->getPatchGeometry()->getTouchesRegularBoundary (" + i + ", 1) && " + coord 
                        + " - boxfirst(" + i + ") + 1 + d_ghost_width == " + coord + "last - d_ghost_width) {" + NL
                        + indent + IND + "index" + coord + "--;" + NL
                        + indent + "}" + NL;
            }
            else {
                index = index + indent + "int index" + coord + " = " + coord + "term - boxfirst(" + i + ") + d_ghost_width;" + NL;
                conditions = conditions + indent + "if (patch->getPatchGeometry()->getTouchesRegularBoundary (" + i + ", 1) && " + coord 
                        + " - boxfirst(" + i + ") + 1 + d_ghost_width == " + coord + "last - d_ghost_width) {" + NL
                        + indent + IND + "index" + coord + "--;" + NL
                        + indent + "}" + NL;
            }
        }
        return index + conditions;
    }
    
    /**
     * Creates the code to fill the interior of a x3d region.
     * It uses a flood-fill algorithm adapted to multiprocessor
     * @param regionId         The region id
     * @param coordinates       The coordinates
     * @param stencil           The problem stencil
     * @param pi                The problem info
     * @return                  The generated code
     * @throws CGException      CG003 Not possible to create code for the platform
     *                          CG004 External error
     */
    private static String fillx3dInterior(int regionId, ArrayList<String> coordinates, int stencil, ProblemInfo pi) throws CGException  {
        String currentIndent = IND;
        String initialPoint = "";
        String initialIndex = "";
        String index = "";
        for (int i = 0; i < coordinates.size(); i++) {
            initialPoint = initialPoint + "boxfirst(" + i + ") == 0 && ";
            initialIndex = initialIndex + "0, ";
            index = index + coordinates.get(i) + ", ";
            currentIndent = currentIndent + IND;
        }
        initialPoint = initialPoint.substring(0, initialPoint.lastIndexOf(" &&"));
        index = index.substring(0, index.lastIndexOf(","));
        String finishedCheck = finishedCheck(coordinates, pi);

        String assignFOV = "";
        for (int j = 0; j < pi.getRegionIds().size(); j++) {
            String currRegionId = pi.getRegionIds().get(j);
            if (CodeGeneratorUtils.isNumber(currRegionId)) {
            	int id = Integer.parseInt(currRegionId);
                if (id == regionId) {
                	assignFOV = assignFOV + currentIndent + IND + IND + IND + IND + IND + IND + IND + IND + IND + "vector(FOV_" + currRegionId + ", i, j, k) = 100;" + NL; 
                } 
                else {
                    assignFOV = assignFOV + currentIndent + IND + IND + IND + IND + IND + IND + IND + IND + IND + "vector(FOV_" + currRegionId + ", i, j, k) = 0;" + NL;
                }
            }

        }
        
        return IND + IND + IND + "//Fill the interior of the region" + NL 
            + initializeInterior(coordinates, regionId, pi)
            + IND + IND + IND + "//Initial FloodFill call" + NL
            + IND + IND + IND + "finishedGlobal = false;" + NL + NL
            + IND + IND + IND + "finishedPatchGlobal = false;" + NL + NL
            + IND + IND + IND + "pred = 1;" + NL
            + IND + IND + IND + "//First floodfill" + NL
            + IND + IND + IND + "pcounter = 0;" + NL
            + getPatchLoop(pi, IND + IND + IND, false, false)
            + IND + IND + IND + IND + "if (" + initialPoint + ") {" + NL
            + IND + IND + IND + IND + IND + "floodfill(patch, " + initialIndex + "1, " + regionId + ");" + NL
            + IND + IND + IND + IND + IND + "workingPatchArray[pcounter] = 1;" + NL
            + IND + IND + IND + IND + "}" + NL
            + IND + IND + IND + IND + "pcounter++;" + NL
            + IND + IND + IND + "}" + NL
            + IND + IND + IND + "if (nodes > 1) {" + NL
            + IND + IND + IND + IND + "d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);" + NL + NL
            + IND + IND + IND + IND + "while (!finishedGlobal) {" + NL
            + IND + IND + IND + IND + IND + "workingGlobal = true;" + NL
            + IND + IND + IND + IND + IND + "//Filling started floodfill in all the patches" + NL
            + IND + IND + IND + IND + IND + "while (workingGlobal) {" + NL
            + IND + IND + IND + IND + IND + IND + "working = 0;" + NL
            + IND + IND + IND + IND + IND + IND + "pcounter = 0;" + NL
            + getPatchLoop(pi, IND + IND + IND + IND + IND + IND, true, false)
            + SAMRAIUtils.getLasts(pi, IND + IND + IND + IND + IND + IND + IND, true)
            + IND + IND + IND + IND + IND + IND + IND + "workingPatchArray[pcounter] = 0;" + NL
            + getCellLoop(pi.getCoordinates(), IND + IND + IND + IND + IND + IND + IND)
            + currentIndent + IND + IND + IND + IND + IND + IND + "if (vector(nonSync, " + index + ") != vector(interior, " 
            + index + ") && vector(nonSync, " + index + ") == 0) {" + NL
            + currentIndent + IND + IND + IND + IND + IND + IND + IND + "floodfill(patch, " + index + ", pred, " + regionId + ");" + NL
            + currentIndent + IND + IND + IND + IND + IND + IND + IND + "workingPatchArray[pcounter] = 1;" + NL
            + currentIndent + IND + IND + IND + IND + IND + IND + IND + "working = 1;" + NL
            + currentIndent + IND + IND + IND + IND + IND + IND + "} else {" + NL
            + currentIndent + IND + IND + IND + IND + IND + IND + IND + "if (vector(nonSync, " + index + ") != vector(interior, " + index + ")) {" + NL
            + currentIndent + IND + IND + IND + IND + IND + IND + IND + IND + "vector(interior, " + index + ") = vector(nonSync, " + index + ");" + NL
            + currentIndent + IND + IND + IND + IND + IND + IND + IND + IND + "if (vector(nonSync, " + index + ") == 2) {" + NL
            + assignFOV
            + currentIndent + IND + IND + IND + IND + IND + IND + IND + IND + "}" + NL
            + currentIndent + IND + IND + IND + IND + IND + IND + IND + "}" + NL
            + currentIndent + IND + IND + IND + IND + IND + IND + "}" + NL
            + getCellLoopEnd(pi.getCoordinates(), IND + IND + IND + IND + IND + IND + IND)
            + IND + IND + IND + IND + IND + IND + IND + "pcounter++;" + NL
            + IND + IND + IND + IND + IND + IND + "}" + NL
            + IND + IND + IND + IND + IND + IND + "d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);" + NL
            + IND + IND + IND + IND + IND + IND + "//Local working variable communication" + NL
            + IND + IND + IND + IND + IND + IND + "workingGlobal = false;" + NL
            + IND + IND + IND + IND + IND + IND + "int *s = new int[1];" + NL
            + IND + IND + IND + IND + IND + IND + "s[0] = working;" + NL
            + IND + IND + IND + IND + IND + IND + "mpi.AllReduce(s, 1, MPI_MAX);" + NL
            + IND + IND + IND + IND + IND + IND + "if (s[0] == 1) {" + NL
            + IND + IND + IND + IND + IND + IND + IND + "workingGlobal = true;" + NL   
            + IND + IND + IND + IND + IND + IND + "}" + NL
            + IND + IND + IND + IND + IND + "}" + NL + NL
            + finishedCheck
            + IND + IND + IND + IND + IND + "//Finished local variables communication" + NL
            + IND + IND + IND + IND + IND + "for (proc = 0; proc < nodes; proc++) {" + NL
            + IND + IND + IND + IND + IND + IND + "if (proc == mpi.getRank()) {" + NL
            + IND + IND + IND + IND + IND + IND + IND + "for (int dproc = 0; dproc < nodes; dproc++) {" + NL
            + IND + IND + IND + IND + IND + IND + IND + IND + "if (dproc != mpi.getRank()) {" + NL
            + IND + IND + IND + IND + IND + IND + IND + IND + IND + "int *w = new int[1];" + NL
            + IND + IND + IND + IND + IND + IND + IND + IND + IND + "w[0] = finished;" + NL
            + IND + IND + IND + IND + IND + IND + IND + IND + IND + "int size = 1;" + NL
            + IND + IND + IND + IND + IND + IND + IND + IND + IND + "mpi.Send(w, size, MPI_INT, dproc, 0);" + NL
            + IND + IND + IND + IND + IND + IND + IND + IND + "}" + NL
            + IND + IND + IND + IND + IND + IND + IND + "}" + NL
            + IND + IND + IND + IND + IND + IND + IND + "finishedArray[proc] = finished;" + NL
            + IND + IND + IND + IND + IND + IND + "} else {" + NL
            + IND + IND + IND + IND + IND + IND + IND + "int *w = new int[1];" + NL
            + IND + IND + IND + IND + IND + IND + IND + "int size = 1;" + NL
            + IND + IND + IND + IND + IND + IND + IND + "tbox::SAMRAI_MPI::Status status;" + NL
            + IND + IND + IND + IND + IND + IND + IND + "mpi.Recv(w, size, MPI_INT, proc, 0, &status);" + NL
            + IND + IND + IND + IND + IND + IND + IND + "finishedArray[proc] = w[0];" + NL
            + IND + IND + IND + IND + IND + IND + "}" + NL
            + IND + IND + IND + IND + IND + "}" + NL
            + IND + IND + IND + IND + IND + "finishedGlobal = true;" + NL
            + IND + IND + IND + IND + IND + "for (proc = 0; proc < nodes & finishedGlobal; proc++) {" + NL
            + IND + IND + IND + IND + IND + IND + "if (finishedArray[proc] == 0) {" + NL
            + IND + IND + IND + IND + IND + IND + IND + "finishedGlobal = 0;" + NL
            + IND + IND + IND + IND + IND + IND + "}" + NL
            + IND + IND + IND + IND + IND + "}" + NL + NL
            + IND + IND + IND + IND + IND + "//Only one processor could start a floodfill at the same time" + NL
            + IND + IND + IND + IND + IND + "//Take the first who wants to" + NL
            + IND + IND + IND + IND + IND + "int first = -1;" + NL
            + IND + IND + IND + IND + IND + "int firstPatch = -1;" + NL
            + IND + IND + IND + IND + IND + "for (proc = 0; proc < nodes && !finishedGlobal && first < 0; proc++) {" + NL
            + IND + IND + IND + IND + IND + IND + "if (finishedArray[proc] == 0) {" + NL
            + IND + IND + IND + IND + IND + IND + IND + "first = proc;" + NL
            + IND + IND + IND + IND + IND + IND + IND + "for (int pat = 0; pat < patches && !finishedPatchGlobal && firstPatch < 0; pat++) {" + NL
            + IND + IND + IND + IND + IND + IND + IND + IND + "if (finishedPatchArray[pat] == 0) {" + NL
            + IND + IND + IND + IND + IND + IND + IND + IND + IND + "firstPatch = pat;" + NL
            + IND + IND + IND + IND + IND + IND + IND + IND + "}" + NL
            + IND + IND + IND + IND + IND + IND + IND + "}" + NL
            + IND + IND + IND + IND + IND + IND + "}" + NL
            + IND + IND + IND + IND + IND + "}" + NL
            + IND + IND + IND + IND + IND + "if (pred == 1) {" + NL
            + IND + IND + IND + IND + IND + IND + "pred = 2;" + NL
            + IND + IND + IND + IND + IND + "} else {" + NL
            + IND + IND + IND + IND + IND + IND + "pred = 1;" + NL
            + IND + IND + IND + IND + IND + "}" + NL
            + IND + IND + IND + IND + IND + "if (first == mpi.getRank() && !finishedGlobal) {" + NL
            + initFloodFill(regionId, pi, IND + IND + IND + IND + IND + IND)
            + IND + IND + IND + IND + IND + "}" + NL
            + IND + IND + IND + IND + IND + "d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);" + NL
            + IND + IND + IND + IND + "}" + NL
            + IND + IND + IND + "}" + NL
            + IND + IND + IND + "//Only one processor" + NL
            + IND + IND + IND + "else {" + NL
            + IND + IND + IND + IND + "while (!finishedPatchGlobal) {" + NL
            + IND + IND + IND + IND + IND + "d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);" + NL
            + IND + IND + IND + IND + IND + "workingPatchGlobal = true;" + NL
            + IND + IND + IND + IND + IND + "//Filling started floodfill in all the patches" + NL
            + IND + IND + IND + IND + IND + "while (workingPatchGlobal) {" + NL
            + IND + IND + IND + IND + IND + IND + "pcounter = 0;" + NL
            + getPatchLoop(pi, IND + IND + IND + IND + IND + IND, true, false)
            + SAMRAIUtils.getLasts(pi, IND + IND + IND + IND + IND + IND + IND, true) + NL
            + IND + IND + IND + IND + IND + IND + IND + "workingPatchArray[pcounter] = 0;" + NL
            + getCellLoop(pi.getCoordinates(), IND + IND + IND + IND + IND + IND + IND)
            + currentIndent + IND + IND + IND + IND + IND + IND + "if (vector(nonSync, " + index + ") != vector(interior, " 
            + index + ") && vector(nonSync, " + index + ") ==0) {" + NL
            + currentIndent + IND + IND + IND + IND + IND + IND + IND + "floodfill(patch, " + index + ", pred, " + regionId + ");" + NL
            + currentIndent + IND + IND + IND + IND + IND + IND + IND + "workingPatchArray[pcounter] = 1;" + NL
            + currentIndent + IND + IND + IND + IND + IND + IND + "} else {" + NL
            + currentIndent + IND + IND + IND + IND + IND + IND + IND + "if (vector(nonSync, " + index + ") != vector(interior, " + index + ")) {" + NL
            + currentIndent + IND + IND + IND + IND + IND + IND + IND + IND + "vector(interior, " + index + ") = vector(nonSync, " + index + ");" + NL
            + currentIndent + IND + IND + IND + IND + IND + IND + IND + IND + "if (vector(nonSync, " + index + ") == 2) {" + NL
            + assignFOV
            + currentIndent + IND + IND + IND + IND + IND + IND + IND + IND + "}" + NL
            + currentIndent + IND + IND + IND + IND + IND + IND + IND + "}" + NL
            + currentIndent + IND + IND + IND + IND + IND + IND + "}" + NL
            + getCellLoopEnd(pi.getCoordinates(), IND + IND + IND + IND + IND + IND + IND)
            + IND + IND + IND + IND + IND + IND + IND + "pcounter++;" + NL
            + IND + IND + IND + IND + IND + IND + "}" + NL
            + IND + IND + IND + IND + IND + IND + "workingPatchGlobal = false;" + NL
            + IND + IND + IND + IND + IND + IND + "for (int pat = 0; pat < patches & !workingPatchGlobal; pat++) {" + NL
            + IND + IND + IND + IND + IND + IND + IND + "if (workingPatchArray[pat] == 1) {" + NL
            + IND + IND + IND + IND + IND + IND + IND + IND + "workingPatchGlobal = true;" + NL
            + IND + IND + IND + IND + IND + IND + IND + "}" + NL
            + IND + IND + IND + IND + IND + IND + "}" + NL
            + IND + IND + IND + IND + IND + IND + "d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);" + NL
            + IND + IND + IND + IND + IND + "}" + NL
            + finishedCheck
            + IND + IND + IND + IND + IND + "//Finished local variables communication" + NL
            + IND + IND + IND + IND + IND + "finishedPatchGlobal = true;" + NL
            + IND + IND + IND + IND + IND + "for (int pat = 0; pat < patches & finishedPatchGlobal; pat++) {" + NL
            + IND + IND + IND + IND + IND + IND + "if (finishedPatchArray[pat] == 0) {" + NL
            + IND + IND + IND + IND + IND + IND + IND + "finishedPatchGlobal = 0;" + NL
            + IND + IND + IND + IND + IND + IND + "}" + NL
            + IND + IND + IND + IND + IND + "}" + NL
            + IND + IND + IND + IND + IND + "//Only one patch could start a floodfill at the same time" + NL
            + IND + IND + IND + IND + IND + "//Take the first who wants to" + NL
            + IND + IND + IND + IND + IND + "int firstPatch = -1;" + NL
            + IND + IND + IND + IND + IND + "for (int pat = 0; pat < patches && !finishedPatchGlobal && firstPatch < 0; pat++) {" + NL
            + IND + IND + IND + IND + IND + IND + "if (finishedPatchArray[pat] == 0) {" + NL
            + IND + IND + IND + IND + IND + IND + IND + "firstPatch = pat;" + NL
            + IND + IND + IND + IND + IND + IND + "}" + NL
            + IND + IND + IND + IND + IND + "}" + NL
            + IND + IND + IND + IND + IND + "if (pred == 1) {" + NL
            + IND + IND + IND + IND + IND + IND + "pred = 2;" + NL
            + IND + IND + IND + IND + IND + "} else {" + NL
            + IND + IND + IND + IND + IND + IND + "pred = 1;" + NL
            + IND + IND + IND + IND + IND + "}" + NL
            + IND + IND + IND + IND + IND + "//New floodfill" + NL
            + initFloodFill(regionId, pi, IND + IND + IND + IND + IND) 
            + IND + IND + IND + IND + IND + "d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);" + NL
            + IND + IND + IND + IND + "}" + NL
            + IND + IND + IND + "}" + NL + NL;
    }
    
    /**
     * Get the finished limits to avoid multiprocessor problems near internal boundaries.
     * @param coordinates   Problem coordinates
     * @param stencil       the stencil of the problem
     * @param indent        The actual indent
     * @return              Code
     */
    private static String getFinishLimits(ArrayList<String> coordinates, int stencil, String indent) {
        String result = indent + "//Limits for the finished checking routine to avoid internal boundary problems" + NL;
        for (int i = 0; i < coordinates.size(); i++) {
            String coord = coordinates.get(i);
            result = result + indent + "int " + coord + "l = 0;" + NL
                + indent + "if (!patch->getPatchGeometry()->getTouchesRegularBoundary (" + i + ", 0)) {" + NL
                + indent + IND + coord + "l = " + stencil + ";" + NL
                + indent + "}" + NL
                + indent + "int " + coord + "u = " + coord + "last;" + NL
                + indent + "if (!patch->getPatchGeometry()->getTouchesRegularBoundary (" + i + ", 1)) {" + NL
                + indent + IND + coord + "u = " + coord + "last - " + (2 * stencil) + ";" + NL
                + indent + "}" + NL;
        }
        return result;
    }
    
    /**
     * Generates the floodfill initial call for the algorithm.
     * @param regionId     The region identifier
     * @param pi            The problem info
     * @param indent        The actual indent
     * @return              The code
     * @throws CGException  CG003 Not possible to create code for the platform
     *                      CG004 External error
     */
    private static String initFloodFill(int regionId, ProblemInfo pi, String indent) throws CGException {
        ArrayList<String> coordinates = pi.getCoordinates();
        String actualIndent = "";
        for (int i = coordinates.size() - 1; i >= 0; i--) {
            actualIndent = actualIndent + IND;
        }
        String initialIndex = "";
        String index = "";
        for (int i = 0; i < coordinates.size(); i++) {
            initialIndex = initialIndex + "0, ";
            index = index + coordinates.get(i) + ", ";
        }
        index = index.substring(0, index.lastIndexOf(","));
        
        return indent + "pcounter = 0;" + NL  
                + getPatchLoop(pi, indent, true, false)
                + SAMRAIUtils.getLasts(pi, indent + IND, true) + NL
                + indent + IND + "if (firstPatch == pcounter && !finishedPatchGlobal) {" + NL
                + getCellLoop(pi.getCoordinates(), indent + IND + IND)
                + actualIndent + indent + IND + IND + "if (vector(nonSync, " + index + ") == 0) {" + NL
                + actualIndent + indent + IND + IND + IND + "floodfill(patch, " + index + ", pred, " + regionId + ");" + NL
                + actualIndent + indent + IND + IND + IND + "workingPatchArray[pcounter] = 1;" + NL
                + actualIndent + indent + IND + IND + "}" + NL
                + getCellLoopEnd(pi.getCoordinates(), indent + IND + IND)
                + indent + IND + "}" + NL
                + indent + IND + "pcounter++;" + NL
                + indent + "}" + NL;
    }
    
    /**
     * Get the code to check if the flood fill general algorith has finished.
     * @param coordinates       The coordinates
     * @param pi                The problem information
     * @return                  The code
     * @throws CGException      CG003 Not possible to create code for the platform
     *                          CG004 External error
     */
    private static String finishedCheck(ArrayList<String> coordinates, ProblemInfo pi) throws CGException {
        String currentIndent = IND + IND + IND + IND + IND + IND;
        String finishedLoop = "";
        String endFinishedLoop = "";
        for (int i = coordinates.size() - 1; i >= 0; i--) {
            finishedLoop = finishedLoop + currentIndent + "for (" + coordinates.get(i) + " = " + coordinates.get(i) + "l; " 
                + coordinates.get(i) + " < " + coordinates.get(i) + "u && finishedPatchArray[pcounter]; " + coordinates.get(i) + "++) {" + NL;
            endFinishedLoop = currentIndent + "}" + NL + endFinishedLoop;
            currentIndent = currentIndent + IND;
        }
        String index = "";
        for (int i = 0; i < coordinates.size(); i++) {
            index = index + coordinates.get(i) + ", ";
        }
        index = index.substring(0, index.lastIndexOf(","));
        
        return IND + IND + IND + IND + IND + "//Looking for a new floodfill" + NL
            + IND + IND + IND + IND + IND + "finished = 1;" + NL 
            + IND + IND + IND + IND + IND + "pcounter = 0;" + NL 
            + getPatchLoop(pi, IND + IND + IND + IND + IND, true, false)
            + SAMRAIUtils.getLasts(pi, IND + IND + IND + IND + IND + IND, true) + NL
            + getFinishLimits(pi.getCoordinates(), Math.max(pi.getSchemaStencil(), pi.getExtrapolationStencil()), IND + IND + IND + IND + IND + IND) + NL
            + IND + IND + IND + IND + IND + IND + "finishedPatchArray[pcounter] = 1;" + NL
            + finishedLoop
            + currentIndent + "if (vector(nonSync, " + index + ") == 0) {" + NL
            + currentIndent + IND + "finishedPatchArray[pcounter] = 0;" + NL
            + currentIndent + IND + "finished = 0;" + NL
            + currentIndent + "}" + NL
            + endFinishedLoop
            + IND + IND + IND + IND + IND + IND + "pcounter++;" + NL
            + IND + IND + IND + IND + IND + "}" + NL;
    }
    
    /**
     * Code to initialize the interior and nonSync variable.
     * @param coordinates   The problem coordinates
     * @param region       The region number
     * @param pi            The problem info
     * @return              The code
     * @throws CGException  CG003 Not possible to create code for the platform
     *                      CG004 External error
     */
    private static String initializeInterior(ArrayList<String> coordinates, int region, ProblemInfo pi) throws CGException {
        String currentIndent = IND + IND + IND;
        String startLoop = "";
        String endLoop = "";
        String index = "";
        for (int i = coordinates.size() - 1; i >= 0; i--) {
            index = coordinates.get(i) + ", " +  index;
            startLoop = startLoop + currentIndent + IND + "for (" + coordinates.get(i) + " = 0; " + coordinates.get(i) + " < " 
                + coordinates.get(i) + "last; " + coordinates.get(i) + "++) {" + NL;
            endLoop = currentIndent + IND + "}" + NL + endLoop;
            currentIndent = currentIndent + IND;
        }
        index = index.substring(0, index.lastIndexOf(","));
        
        String segCond = "";
        
        if (pi.getRegionIds().contains(String.valueOf(region))) {
            segCond = segCond + "vector(FOV_" + region + ", " + index + ") > 0";
        }
        if (pi.getRegionIds().contains(String.valueOf(region)) && pi.getRegionIds().contains(String.valueOf(region + 1))) {
            segCond = segCond + " || ";
        }
        if (pi.getRegionIds().contains(String.valueOf(region + 1))) {
            segCond = segCond + "vector(FOV_" + (region + 1) + ", " + index + ") > 0";
        }
        
        return IND + IND + IND + "//Initialize the variables and set the walls as exterior" + NL
            + getPatchLoop(pi, IND + IND + IND, true, false)
            + SAMRAIUtils.getLasts(pi, IND + IND + IND + IND, true) + NL
            + startLoop + currentIndent + IND + "//If wall, then it is considered exterior" + NL
            + currentIndent + IND + "if (" + segCond + ") {" + NL
            + currentIndent + IND + IND + "vector(interior, " + index + ") = 1;" + NL
            + currentIndent + IND + IND + "vector(nonSync, " + index + ") = 1;" + NL
            + currentIndent + IND + "} else {" + NL
            + currentIndent + IND + IND + "vector(interior, " + index + ") = 0;" + NL
            + currentIndent + IND + IND + "vector(nonSync, " + index + ") = 0;" + NL
            + currentIndent + IND + "}" + NL + endLoop
            + IND + IND + IND + "}" + NL;
    }
    
    /**
     * Maps the mathematical region interior.
     * @param regionInfo       The region information
     * @param minGrid           The minimum values of the grid
     * @param maxGrid           The maximum values of the grid
     * @param pi                The problem info
     * @return                  The mapping
     * @throws CGException      CG004 External error
     */
    private static String mapMathematicalInterior(MappingRegionInfo regionInfo, String[] minGrid, String[] maxGrid, ProblemInfo pi) throws CGException {
        ArrayList<String> coordinates = pi.getCoordinates();
        int stencil = Math.max(pi.getSchemaStencil(), pi.getExtrapolationStencil());
        String[][] xslParams = new String [3][2];
        xslParams[0][0] = "condition";
        xslParams[0][1] = "true";
        xslParams[1][0] = "indent";
        xslParams[1][1] = "0";
        xslParams[2][0] =  "dimensions";
        xslParams[2][1] =  String.valueOf(coordinates.size());
        
        String regionName = regionInfo.getRegionName();
        ArrayList<LinkedHashMap<String, CoordinateInfo>> regionLimits = regionInfo.getCoordinateLimits();
        
        String code = "";
        for (int j = 0; j < regionLimits.size(); j++) {
        	LinkedHashMap<String, CoordinateInfo> regionLimit = regionLimits.get(j);
        	String loop = "";
            String endLoop = "";
            String index = "";
            String preLoop = "";
            String condition = "";
            String indent = IND + IND + IND + IND;
            for (int i = 0; i < coordinates.size(); i++) {
                String coord = coordinates.get(i);
                String minValue = SAMRAIUtils.xmlTransform(regionLimit.get(coord).getMin(), xslParams);
                String maxValue = SAMRAIUtils.xmlTransform(regionLimit.get(coord).getMax(), xslParams);
                String minValueOriginal = minValue;
                String maxValueOriginal = maxValue;
                if (regionInfo.isFullDomain()) {
                    minValue = "0";
                    maxValue = "d_grid_geometry->getPhysicalDomain().front().numberCells()[" + i + "]";
                }
                else {
                    if (CodeGeneratorUtils.isNumber(minValue) && CodeGeneratorUtils.isNumber(minGrid[i])) {
                        minValue = "floor(" + (Double.parseDouble(minValue) - Double.parseDouble(minGrid[i])) + " / dx[" + i + "] + 1E-10)";
                    }
                    else {
                        minValue = "floor((" + minValue + " - d_grid_geometry->getXLower()[" + i + "]) / dx[" + i + "] + 1E-10)";
                    }
                    if (CodeGeneratorUtils.isNumber(maxValue) && CodeGeneratorUtils.isNumber(minGrid[i])) {
                        maxValue = "floor(" + (Double.parseDouble(maxValue) - Double.parseDouble(minGrid[i])) + " / dx[" + i + "] + 1E-10)";
                    }
                    else {
                        maxValue = "floor((" + maxValue + " - d_grid_geometry->getXLower()[" + i + "]) / dx[" + i + "] + 1E-10)";
                    }
                }
                if (regionInfo.isFullDomain() || minGrid[i].equals(minValueOriginal)) {
                    preLoop = preLoop + IND + IND + IND + IND + coord + "MapStart = " + minValue + ";" + NL;
                }
                else {
                    preLoop = preLoop + IND + IND + IND + IND + "if (" + minValue + " > d_ghost_width) {" + NL
                        + IND + IND + IND + IND + IND + coord + "MapStart = " + minValue + ";" + NL
                        + IND + IND + IND + IND + "} else {" + NL
                        + IND + IND + IND + IND + IND + coord + "MapStart = 0;" + NL
                        + IND + IND + IND + IND + "}" + NL;
                }
                if (maxGrid[i].equals(maxValueOriginal)) {
                    preLoop = preLoop + IND + IND + IND + IND + coord + "MapEnd = " + maxValue + ";" + NL;
                }
                else {
                    preLoop = preLoop + IND + IND + IND + IND + "if (" + maxValue + " < boxlast(" + i + ") + d_ghost_width) {" + NL
                        + IND + IND + IND + IND + IND + coord + "MapEnd = " + maxValue + ";" + NL
                        + IND + IND + IND + IND + "} else {" + NL
                        + IND + IND + IND + IND + IND + coord + "MapEnd = boxlast(" + i + ") + 2 * d_ghost_width;" + NL
                        + IND + IND + IND + IND + "}" + NL;
                }
                
                
                loop = loop + indent + "for (" + coordinates.get(i) + " = " + coordinates.get(i) + "MapStart; " 
                    + coordinates.get(i) + " <= " + coordinates.get(i) + "MapEnd; " + coordinates.get(i) + "++) {" + NL;
                index = index + coordinates.get(i) + " - boxfirst(" + i + ") + d_ghost_width, ";
                condition = condition + coordinates.get(i) + " >= boxfirst(" + i + ") - d_ghost_width && " + coordinates.get(i) + " <= boxlast("
                        + i + ") + d_ghost_width && ";
                endLoop = indent + "}" + NL + endLoop;
                indent = indent + IND;
            }
            condition = condition.substring(0, condition.lastIndexOf(" &&"));
            index = index.substring(0, index.lastIndexOf(","));
        	
        	code = code + preLoop + loop
                + indent + "if (" + condition + ") {" + NL
                + assignFOV(pi.getProblem(), regionInfo.getId(), pi.getRegionIds(), index, pi.getCoordinates(), indent + IND)
                + indent + "}" + NL
                + endLoop;
        }
        
        String loopsStencil = "";
        if (stencil > 1) {
            loopsStencil = loopsStencil + checkStencil(pi, regionInfo, coordinates, IND + IND + IND + IND);
        }
        
        return IND + IND + IND + "//Region: " + regionName + "I" + NL
                + getPatchLoop(pi, IND + IND + IND, true, false)
                + SAMRAIUtils.getLasts(pi, IND + IND + IND + IND, true) + NL
                + code
                + loopsStencil 
                + IND + IND + IND + "}" + NL
                + IND + IND + IND + "d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);" + NL + NL;
    }
    
    /**
     * Maps the mathematical region surface.
     * @param regionInfo       The region information
     * @param minGrid           The minimum values of the grid
     * @param maxGrid           The maximum values of the grid
     * @param pi                The problem info
     * @return                  The mapping
     * @throws CGException      CG004 External error
     */
    private static String mapMathematicalSurface(MappingRegionInfo regionInfo, String[] minGrid, String[] maxGrid, ProblemInfo pi) throws CGException {
        ArrayList<String> coordinates = pi.getCoordinates();
        LinkedHashMap<String, CoordinateInfo> gridLimit = pi.getGridLimits();
        int stencil = Math.max(pi.getSchemaStencil(), pi.getExtrapolationStencil());
        String[][] xslParams = new String [3][2];
        xslParams[0][0] = "condition";
        xslParams[0][1] = "true";
        xslParams[1][0] = "indent";
        xslParams[1][1] = "0";
        xslParams[2][0] =  "dimensions";
        xslParams[2][1] =  String.valueOf(coordinates.size());
        
        String regionName = regionInfo.getRegionName();
        String code = "";
        ArrayList<LinkedHashMap<String, CoordinateInfo>> regionLimits = regionInfo.getCoordinateLimits();
        for (LinkedHashMap<String, CoordinateInfo> regionLimit: regionLimits) {            
            String regionCondition = "";
            String[] minValue = new String[regionLimit.size()];
            String[] maxValue = new String[regionLimit.size()];
            String[] minValueInit = new String[regionLimit.size()];
            String[] maxValueInit = new String[regionLimit.size()];
            Iterator<String> coordIt = regionLimit.keySet().iterator();
            for (int i = 0; i < coordinates.size(); i++) {
                String coord = coordIt.next();
                minValueInit[i] = SAMRAIUtils.xmlTransform(regionLimit.get(coord).getMin(), xslParams);
                maxValueInit[i] = SAMRAIUtils.xmlTransform(regionLimit.get(coord).getMax(), xslParams);

                if (CodeGeneratorUtils.isNumber(minValueInit[i]) && CodeGeneratorUtils.isNumber(minGrid[i])) {
                    minValue[i] = "floor(" + (Double.parseDouble(minValueInit[i]) - Double.parseDouble(minGrid[i])) 
                        + " / dx[" + i + "] + 1E-10) - boxfirst(" + i + ") + d_ghost_width";
                }
                else {
                    minValue[i] = "floor((" + minValueInit[i] + " - d_grid_geometry->getXLower()[" + i + "]) / dx[" + i + "] + 1E-10) - boxfirst(" 
                        + i + ") + d_ghost_width";
                }
                if (maxGrid[i].equals(minValueInit[i])) {
                    regionCondition = regionCondition + minValue[i] + " <= boxlast(" + i + ") - boxfirst(" 
                        + i + ") + d_ghost_width + 1 && ";
                }
                else {
                    regionCondition = regionCondition + minValue[i] + " <= boxlast(" + i + ") - boxfirst(" + i + ") + d_ghost_width && ";
                }
                if (regionInfo.isFullDomain()) {
                    maxValue[i] = "d_grid_geometry->getPhysicalDomain().front().numberCells()[" + i + "] - boxfirst(" + i + ") + d_ghost_width";
                }
                else {
                    if (CodeGeneratorUtils.isNumber(maxValueInit[i]) && CodeGeneratorUtils.isNumber(minGrid[i])) {
                        maxValue[i] = "floor(" + (Double.parseDouble(maxValueInit[i]) - Double.parseDouble(minGrid[i])) 
                            + " / dx[" + i + "] + 1E-10) - boxfirst(" + i + ") + d_ghost_width";
                    }
                    else {
                        maxValue[i] = "floor((" + maxValueInit[i] + " - d_grid_geometry->getXLower()[" + i + "]) / dx[" + i + "] + 1E-10) - boxfirst(" 
                            + i + ") + d_ghost_width";
                    }
                }
                regionCondition = regionCondition + maxValue[i] + " >= d_ghost_width && ";
            }
            regionCondition = regionCondition.substring(0, regionCondition.lastIndexOf(" &&"));
            String loops = "";
            for (int j = 0; j < coordinates.size(); j++) {
                String loop = "";
                String endLoop = "";
                String currentIndent = IND + IND + IND + IND;
                for (int k = coordinates.size() - 1; k >= 0; k--) {
                    if (k != j) {
                        loop = loop + currentIndent + "for (" + coordinates.get(k) + " = " + coordinates.get(k) + "MapStart; " 
                            + coordinates.get(k) + " <= " + coordinates.get(k) + "MapEnd; " + coordinates.get(k) + "++) {" + NL;
                        endLoop = currentIndent + "}" + NL + endLoop;
                        currentIndent = currentIndent + IND;
                    }
                }
                loops = loops + loop + getContent(pi, minValueInit[j], maxValueInit[j], j, coordinates, currentIndent, regionInfo.getId()) 
                    + endLoop;
            }
        	code = code + getPreloopConditions(regionLimit, gridLimit, coordinates, minGrid, maxGrid) + loops;
        }
        String loopsStencil = "";
        if (stencil > 1) {
            loopsStencil = loopsStencil + checkStencil(pi, regionInfo, coordinates, IND + IND + IND + IND);
        }
        
        return  IND + IND + IND + "//Region: " + regionName + "S" + NL 
            + getPatchLoop(pi, IND + IND + IND, true, false)
            + SAMRAIUtils.getLasts(pi, IND + IND + IND + IND, true) + NL
            + code
            + loopsStencil
            + IND + IND + IND + "}" + NL
            + IND + IND + IND + "d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);" + NL + NL;
    }
    
    /**
     * Creates the code to set the surface or to check the stencil of the surface for a coordinate.
     * @param pi                The problem information
     * @param minValueInit      The minimum value of the region in a coordinate 
     * @param maxValueInit      The maximum value of the region in a coordinate
     * @param coordNumber       The coordinate number
     * @param coordinates       The coordinates of the problem
     * @param actualIndent      The actual indentation
     * @param regionId         The region identifier
     * @return                  The code
     * @throws CGException      CG00X External error
     */
    private static String getContent(ProblemInfo pi, String minValueInit, String maxValueInit, int coordNumber, ArrayList<String> coordinates, 
            String actualIndent, int regionId) throws CGException {
        String content = "";
        
        String index1 = getLimitIndex("MapStart", coordNumber, coordinates);
        String index2 = getLimitIndex("MapEnd - 1", coordNumber, coordinates);
        
        if (CodeGeneratorUtils.isNumber(minValueInit)) {
            content = content + actualIndent + "if (lessEq(patch_geom->getXLower()[" + coordNumber + "], " + Double.parseDouble(minValueInit) 
                + ") && greaterEq(patch_geom->getXUpper()[" + coordNumber + "], " + Double.parseDouble(minValueInit) + ")) {" + NL
                + assignFOV(pi.getProblem(), regionId, pi.getRegionIds(), index1, pi.getCoordinates(), actualIndent + IND)
                + actualIndent + "}" + NL; 
        }
        else {
            content = content + actualIndent + "if (lessEq(patch_geom->getXLower()[" + coordNumber + "], " 
                + minValueInit + ") && greaterEq(patch_geom->getXUpper()[" + coordNumber + "], " + minValueInit + ")) {" + NL
                + assignFOV(pi.getProblem(), regionId, pi.getRegionIds(), index1, pi.getCoordinates(), actualIndent + IND)
                + actualIndent + "}" + NL;  
        }
        if (CodeGeneratorUtils.isNumber(maxValueInit)) {
            content = content + actualIndent + "if (lessEq(patch_geom->getXLower()[" + coordNumber + "], " + Double.parseDouble(maxValueInit) 
                + ") && greaterEq(patch_geom->getXUpper()[" + coordNumber + "], " + Double.parseDouble(maxValueInit) + ")) {" + NL
                + assignFOV(pi.getProblem(), regionId, pi.getRegionIds(), index2, pi.getCoordinates(), actualIndent + IND)
                + actualIndent + "}" + NL;
        }
        else {
            content = content + actualIndent + "if (lessEq(patch_geom->getXLower()[" + coordNumber + "], " 
                + maxValueInit + ") && greaterEq(patch_geom->getXUpper()[" + coordNumber + "], " + maxValueInit + ")) {" + NL
                + assignFOV(pi.getProblem(), regionId, pi.getRegionIds(), index2, pi.getCoordinates(), actualIndent + IND)
                + actualIndent + "}" + NL;
        }
        return content;
    }
    
    /**
     * Creates the index for the coordinates at the limits of the mathematical region.
     * @param operation         The operation
     * @param coordNumber       The coordinate to generate the limits for
     * @param coordinates       The coordinates
     * @return                  The index generated
     */
    private static String getLimitIndex(String operation, int coordNumber, ArrayList<String> coordinates) {
        String index = "";
        for (int k = 0; k < coordinates.size(); k++) {
            if (coordNumber == k) {
                index = index + coordinates.get(k) + operation + ", ";
            }
            else {
                index = index + coordinates.get(k) + ", ";
            }
        }
        index = index.substring(0, index.lastIndexOf(","));
        return index;
    }
    
    /**
     * Generate the code for the pre-loop condition in map mathematical surface method.
     * @param regionLimit      The region limits
     * @param gridLimit         The grid limits
     * @param coordinates       The coordinates of the problem
     * @param minGrid           The minimum values of the grid
     * @param maxGrid           The maximum values of the grid
     * @return                  The code
     * @throws CGException      CG00X External error
     */
    private static String getPreloopConditions(LinkedHashMap<String, CoordinateInfo> regionLimit, LinkedHashMap<String, CoordinateInfo> gridLimit, 
            ArrayList<String> coordinates, String[] minGrid, String[] maxGrid) throws CGException {
        String[][] xslParams = new String [3][2];
        xslParams[0][0] = "condition";
        xslParams[0][1] = "true";
        xslParams[1][0] = "indent";
        xslParams[1][1] = "0";
        xslParams[2][0] =  "dimensions";
        xslParams[2][1] =  String.valueOf(coordinates.size());
        
        String[] minValue = new String[regionLimit.size()];
        String[] maxValue = new String[regionLimit.size()];
        String preLoop = "";
        Iterator<String> coordIt = regionLimit.keySet().iterator();
        for (int i = 0; i < coordinates.size(); i++) {
            String coord = coordIt.next();
            minValue[i] = SAMRAIUtils.xmlTransform(regionLimit.get(coord).getMin(), xslParams);
            maxValue[i] = SAMRAIUtils.xmlTransform(regionLimit.get(coord).getMax(), xslParams);
            String minValueOriginal = minValue[i];
            String maxValueOriginal = maxValue[i];
            if (minGrid[i].equals(minValue[i])) {
                minValue[i] = "0";
            }
            else {
                if (CodeGeneratorUtils.isNumber(minValue[i]) && CodeGeneratorUtils.isNumber(minGrid[i])) {
                    minValue[i] = "floor(" + (Double.parseDouble(minValue[i]) - Double.parseDouble(minGrid[i])) + " / dx[" + i + "] + 1E-10)";
                }
                else {
                    minValue[i] = "floor((" + minValue[i] + " - d_grid_geometry->getXLower()[" + i + "]) / dx[" + i + "] + 1E-10)";
                }
            }
            if (maxGrid[i].equals(maxValue[i])) {
                maxValue[i] = "boxlast(" + i + ") - boxfirst(" + i + ") + 1 + 2 * d_ghost_width";
            }
            else {
                if (CodeGeneratorUtils.isNumber(maxValue[i]) && CodeGeneratorUtils.isNumber(minGrid[i])) {
                    maxValue[i] = "floor(" + (Double.parseDouble(maxValue[i]) - Double.parseDouble(minGrid[i])) + " / dx[" + i + "] + 1E-10)";
                }
                else {
                    maxValue[i] = "floor((" + maxValue[i] + " - d_grid_geometry->getXLower()[" + i + "]) / dx[" + i + "] + 1E-10)";
                }
            }
            if (minGrid[i].equals(minValueOriginal)) {
                preLoop = preLoop + IND + IND + IND + coord + "MapStart = " + minValue[i] + " - boxfirst(" + i + ") + d_ghost_width;" + NL;
            }
            else {
                preLoop = preLoop 
                    + IND + IND + IND + IND + "if (" + minValue[i] + " >= boxfirst(" + i + ")) {" + NL
                    + IND + IND + IND + IND + IND + coord + "MapStart = " + minValue[i] + " - boxfirst(" + i + ") + d_ghost_width;" + NL
                    + IND + IND + IND + IND + "} else {" + NL
                    + IND + IND + IND + IND + IND + coord + "MapStart = 0;" + NL
                    + IND + IND + IND + IND + "}" + NL;
            }
            if (maxGrid[i].equals(maxValueOriginal)) {
                preLoop = preLoop + IND + IND + IND + IND + coord + "MapEnd = " + maxValue[i] + " - boxfirst(" + i + ") + d_ghost_width;" + NL;
            }
            else {
                preLoop = preLoop 
                    + IND + IND + IND + IND + "if (" + maxValue[i] + " <= boxlast(" + i + ")) {" + NL
                    + IND + IND + IND + IND + IND + coord + "MapEnd = " + maxValue[i] + " - boxfirst(" + i + ") + d_ghost_width;" + NL
                    + IND + IND + IND + IND + "} else {" + NL
                    + IND + IND + IND + IND + IND + coord + "MapEnd = boxlast(" 
                    + i + ") - boxfirst(" + i + ") + 1 + 2 * d_ghost_width;" + NL
                    + IND + IND + IND + IND + "}" + NL;
            }
        }
        return preLoop;
    }
    
    /**
     * Generate the code to maps a x3d region. 
     * @param x3dId				X3D id
     * @param regionInfo        The region information
     * @param pi                The problem info
     * @return                  The code generated
     * @throws CGException      CG004 External error
     */
    private static String mapX3d(String x3dId, MappingRegionInfo regionInfo, ProblemInfo pi) throws CGException {
        
        ArrayList<String> coordinates = pi.getCoordinates();
        LinkedHashMap<String, CoordinateInfo> gridLimit = pi.getGridLimits();
        int stencil = Math.max(pi.getSchemaStencil(), pi.getExtrapolationStencil());
        String regionName = regionInfo.getRegionName();
        int regionNum = regionInfo.getId();
        int originalRegionNumber = regionNum;
        
        String[][] xslParams = new String [3][2];
        xslParams[0][0] = "condition";
        xslParams[0][1] = "true";
        xslParams[1][0] = "indent";
        xslParams[1][1] = "0";
        xslParams[2][0] =  "dimensions";
        xslParams[2][1] =  String.valueOf(coordinates.size());
        try {
        	DocumentManager docMan = new DocumentManagerImpl();
            X3DRegion x3d = CodeGeneratorUtils.parseX3dRegion(docMan.getX3D(x3dId));
            
            String result = "";
            String currentIndent = IND + IND + IND;

            String coordIndex = x3d.getCoordIndexAttr();
            String[] unions;
            //The data could have commas or not...
            if (coordIndex.contains(",")) {
                unions = coordIndex.substring(0, coordIndex.lastIndexOf(" -1,")).split(" -1, ");
            }
            else {
                unions = coordIndex.substring(0, coordIndex.lastIndexOf(" -1")).split(" -1 ");
            }
            //Unions from 3D to 2D (contour)
            if (pi.getDimensions() == 2) { 
                unions = CodeGeneratorUtils.removeRepeatedUnions(unions);
            }
            
            //Surface region
            if (regionInfo.hasSurface()) {
                result = result + IND + IND + IND + "//Region: " + regionName + "S" + NL;
                regionNum++;
            }
            else {
            	result = result + currentIndent + "//Region: " + regionName + "I" + NL;
            }

            //Fill the faces
            regionInfo.setId(regionNum);
            result = result + getPatchLoop(pi, IND + IND + IND, true, false)
                    + SAMRAIUtils.getLasts(pi, IND + IND + IND + IND, true) + NL
                    + faceFilling(pi, regionInfo, coordinates, gridLimit);
            //Check the stencil in the surface and fill if necessary
            if (stencil > 1 && originalRegionNumber != regionNum) {
                result = result + checkStencil(pi, regionInfo, coordinates, IND + IND + IND + IND);
            }
            result = result + IND + IND + IND + "}" + NL
                    + IND + IND + IND + "d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);" + NL + NL;
            
            //Interior region
            if (regionInfo.hasInterior()) {
                regionInfo.setId(originalRegionNumber);
                result = result + currentIndent + "//Region: " + regionName + "I" + NL
                    + fillx3dInterior(regionInfo.getId(), coordinates, stencil, pi);
                if (stencil > 1) {
                    result = result + getPatchLoop(pi, IND + IND + IND, true, false)
                            + SAMRAIUtils.getLasts(pi, IND + IND + IND + IND, true) + NL
                            + checkStencil(pi, regionInfo, coordinates, IND + IND + IND + IND)
                            + IND + IND + IND + "}" + NL;
                }
                result = result + IND + IND + IND + "d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);" + NL + NL;
            }
            return result;
        } 
        catch (DOMException e) {
            throw new CGException(CGException.CG00X, e.getMessage());
        } 
        catch (DMException e) {
            throw new CGException(CGException.CG00X, e.getMessage());
        } 
    }
    
    /**
     * Generate the code to map a region.
     * @param regionInfo       The region info
     * @param pi                The problem info
     * @return                  The generated code
     * @throws CGException      CG004 External error
     */
    public static String mapRegion(MappingRegionInfo regionInfo, ProblemInfo pi) throws CGException {
        ArrayList<String> coordinates = pi.getCoordinates();
        LinkedHashMap<String, CoordinateInfo> gridLimit = pi.getGridLimits();
        String[][] xslParams = new String [3][2];
        xslParams[0][0] = "condition";
        xslParams[0][1] = "true";
        xslParams[1][0] = "indent";
        xslParams[1][1] = "0";
        xslParams[2][0] =  "dimensions";
        xslParams[2][1] =  String.valueOf(coordinates.size());

        String result = "";
        
        //Take the grid minimum, maximum and deltas for all the problem coordinates
        String[] minGrid = new String[coordinates.size()];
        String[] maxGrid = new String[coordinates.size()];
        for (int i = 0; i < coordinates.size(); i++) {
            minGrid[i] = SAMRAIUtils.xmlTransform(gridLimit.get(coordinates.get(i)).getMin(), xslParams);
            maxGrid[i] = SAMRAIUtils.xmlTransform(gridLimit.get(coordinates.get(i)).getMax(), xslParams);
        }
        
        //X3d
        for (String x3dId : regionInfo.getX3d()) {
            result = result + mapX3d(x3dId, regionInfo, pi);
        }
        //mathematical region
        if (regionInfo.getCoordinateLimits().size() > 0) {
	        //Map interior mathematical region
	        if (regionInfo.hasInterior()) {
	            result = result + mapMathematicalInterior(regionInfo, minGrid, maxGrid, pi);
	        }
	        //Surface mathematical region
	        if (regionInfo.hasSurface()) {
	            regionInfo.setId(regionInfo.getId() + 1);
	            result = result + mapMathematicalSurface(regionInfo, minGrid, maxGrid, pi);
	        }
        }
        return result;
    }
    
    /**
     * Map the boundary conditions.
     * @param pi                The problem information
     * @return                  The code
     * @throws CGException      CG00X External error
     */
    private static String mapBoundaries(ProblemInfo pi) throws CGException {
        String result = IND + IND + "//Boundaries Mapping" + NL
                + getPatchLoop(pi, IND + IND, false, false)
                + SAMRAIUtils.getLasts(pi, IND + IND + IND, true) + NL;
        ArrayList<String> boundPrecedence = pi.getBoundariesPrecedence();
        ArrayList<String> coordinates = pi.getCoordinates();
        String ind = IND + IND + IND + IND;
        String index = "";
        for (int i = 0; i < coordinates.size(); i++) {
            String coord = coordinates.get(i);
            index = index + coord + ", ";
        }
        index = index.substring(0, index.lastIndexOf(","));
        
        for (int i = boundPrecedence.size() - 1; i >= 0; i--) {
            String axis = boundPrecedence.get(i).substring(0, boundPrecedence.get(i).indexOf("-"));
            String side = boundPrecedence.get(i).substring(boundPrecedence.get(i).indexOf("-") + 1);
            int boundIndex = coordinates.indexOf(CodeGeneratorUtils.contToDisc(pi.getProblem(), axis)) * 2 + 1;
            int sideIdx = 0;
            if (side.toLowerCase().equals("upper")) {
                boundIndex++;
                sideIdx = 1;
            }
            
            result = result + IND + IND + IND + "//" + boundPrecedence.get(i) + NL
                    + IND + IND + IND + "if (patch->getPatchGeometry()->getTouchesRegularBoundary(" 
                    + coordinates.indexOf(CodeGeneratorUtils.contToDisc(pi.getProblem(), axis)) + ", " + sideIdx + ")) {" + NL;
            if (side.toLowerCase().equals("lower")) {
                String loop = "";
                String endLoop = "";
                String tmpInd = ind;
                for (int j = coordinates.size() - 1; j >= 0; j--) {
                    String coord = coordinates.get(j);
                    if (j == coordinates.indexOf(CodeGeneratorUtils.contToDisc(pi.getProblem(), axis))) {
                        loop = loop + tmpInd + "for (" + coord + " = 0; " + coord + " < d_ghost_width; " + coord + "++) {" + NL;
                    }
                    else {
                        loop = loop + tmpInd + "for (" + coord + " = 0; " + coord + " < " + coord + "last; " + coord + "++) {" + NL;
                    }
                    endLoop = tmpInd + "}" + NL + endLoop;
                    tmpInd = tmpInd + IND;
                }
                result = result + loop
                    + assignFOV(pi.getProblem(), -boundIndex, pi.getRegionIds(), index, pi.getCoordinates(), tmpInd)
                    + endLoop;
            }
            else {
                String loop = "";
                String endLoop = "";
                String tmpInd = ind;
                for (int j = coordinates.size() - 1; j >= 0; j--) {
                    String coord = coordinates.get(j);
                    if (j == coordinates.indexOf(CodeGeneratorUtils.contToDisc(pi.getProblem(), axis))) {
                        loop = loop + tmpInd + "for (" + coord + " = " + coord + "last - d_ghost_width; " + coord + " < " + coord + "last; " 
                                + coord + "++) {" + NL;
                    }
                    else {
                        loop = loop + tmpInd + "for (" + coord + " = 0; " + coord + " < " + coord + "last; " + coord + "++) {" + NL;
                    }
                    endLoop = tmpInd + "}" + NL + endLoop;
                    tmpInd = tmpInd + IND;
                }
                result = result + loop
                    + assignFOV(pi.getProblem(), -boundIndex, pi.getRegionIds(), index, pi.getCoordinates(), tmpInd)
                    + endLoop;
            }
            result = result + IND + IND + IND + "}" + NL;
        }
        result = result + IND + IND + "}" + NL
                + IND + IND + "d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);" + NL + NL;
        
        return result;
    }
    
    /**
     * Creates the code block that checks the thickness of a region.
     * @param pi            The problem information
     * @param regionInfo    The region 
     * @param coords        The spatial coordinates
     * @param currentIndent  The actual indent for the code
     * @return              The code
     * @throws CGException  CG00X External error
     */
    private static String checkStencil(ProblemInfo pi, MappingRegionInfo regionInfo, ArrayList<String> coords, String currentIndent) throws CGException {
        String loopStart = "";
        String loopEnd = "";
        String indent = currentIndent;
        String index = "";
        for (int i = coords.size() - 1; i >= 0; i--) {
            String coord = coords.get(i);
            index = coord + ", " + index;
            loopStart = loopStart + indent + "for (" + coord + " = 0; " + coord + " < " + coord + "last; " + coord + "++) {" + NL;
            loopEnd = indent + "}" + NL + loopEnd;
            indent = indent + IND;
        }
        index = index.substring(0, index.lastIndexOf(","));
        
        String initInterior = "";
        String incompleteStencil = "";
        String insideCond = "";
        String interiorCond = "";
        for (int i = 0; i < pi.getDimensions(); i++) {
            String coord = coords.get(i);
            initInterior = initInterior + indent + "vector(interior_" + coord + ", " + index + ") = 0;" + NL;
            incompleteStencil = incompleteStencil + "abs(vector(interior_" + coord + ", " + index + ")) > 1 || ";
            insideCond = insideCond + coord + " < d_ghost_width || " + coord + " >= " + coord + "last - d_ghost_width || ";
            interiorCond = interiorCond + "vector(interior_" + coord + ", " + index + ") != 0 || ";
        }
        incompleteStencil = incompleteStencil.substring(0, incompleteStencil.lastIndexOf(" ||"));
        insideCond = insideCond.substring(0, insideCond.lastIndexOf(" ||"));
        interiorCond = interiorCond.substring(0, interiorCond.lastIndexOf(" ||"));
        
        String patchIndent = currentIndent.substring(1);
        
        return currentIndent + "//Check stencil" + NL
            + loopStart
            + initInterior
            + loopEnd
            + loopStart
            + indent + "if (vector(FOV_" + regionInfo.getId() + ", " + index + ") > 0) {" + NL
            + indent + IND + "setStencilLimits(patch, " + index + ", d_FOV_" + regionInfo.getId() + "_id);" + NL
            + indent + "}" + NL
            + loopEnd
            + patchIndent + "}" + NL
            + patchIndent + "d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);" + NL
            + getPatchLoop(pi, patchIndent, true, false)
            + SAMRAIUtils.getLasts(pi, currentIndent, true) + NL
            + loopStart
            + indent + "if ((" + incompleteStencil + ") && (" + insideCond + ")) {" + NL
            + indent + IND + "checkStencil(patch, " + index + ", d_FOV_" + regionInfo.getId() + "_id);" + NL
            + indent + "}" + NL
            + loopEnd
            + loopStart
            + indent + "if (" + interiorCond + ") {" + NL
            + assignFOV(pi.getProblem(), regionInfo.getId(), pi.getRegionIds(), index, pi.getCoordinates(), indent + IND)
            + indent + "}" + NL
            + loopEnd;
    }
    
    /**
     * Creates the patch loop structure code.
     * @param pi                The problem info
     * @param ind               The actual indent
     * @param includeInterior   If the interior and nonSync variables must be declared
     * @param includeHard       If the hard region variables must be declared
     * @return                  The code
     * @throws CGException      CG003 Not possible to create code for the platform
     *                          CG00X External error
     */
    private static String getPatchLoop(ProblemInfo pi, String ind, boolean includeInterior, boolean includeHard) throws CGException {
        String interior = "";
        if (includeInterior) {
            for (int i = 0; i < pi.getDimensions(); i++) {
                //Initialization of the variables
                String coordString = pi.getCoordinates().get(i);
                interior =  interior + IND + ind + "double* interior_" + coordString + " = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_" 
                        + coordString + "_id).get())->getPointer();" + NL;
            }
            interior = interior + ind + IND + "double* interior = ((pdat::NodeData<double> *) patch->getPatchData(d_interior_regridding_value_id).get())->getPointer();" + NL
                    + ind + IND + "int* nonSync = ((pdat::NodeData<int> *) patch->getPatchData(d_nonSync_regridding_tag_id).get())->getPointer();" + NL;
        }
        String hard = "";
        if (includeHard) {
            hard = hard + ind + IND + "//Hard region field distance variables" + NL;
            hard = hard + SAMRAIUtils.addHardFieldDistDeclaration(pi.getCoordinates(), pi.getExtrapolatedFieldGroups(), ind.length() + 1) 
                    + NL
                    + ind + IND + "double dist_min;" + NL;
            for (int i = 0; i < pi.getDimensions(); i++) {
                //Initialization of the variables
                String coordString = pi.getCoordinates().get(i);
                hard =  hard + IND + ind + "int dist_" + coordString + "_tmp;" + NL;
            }
        }
        return  ind + "for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {" + NL
                + ind + IND + "const std::shared_ptr< hier::Patch >& patch = *p_it;" + NL + NL
                + ind + IND + "//Get the dimensions of the patch" + NL
                + ind + IND + "hier::Box pbox = patch->getBox();" + NL
                + SAMRAIUtils.createFOVDeclaration(pi.getRegionIds(), ind + IND, "patch->")
                + interior
                + hard
                + ind + IND + "const hier::Index boxfirst = patch->getBox().lower();" + NL
                + ind + IND + "const hier::Index boxlast  = patch->getBox().upper();" + NL + NL
                + ind + IND + "//Get delta spaces into an array. dx, dy, dz." + NL
                + ind + IND + "const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));" + NL
                + ind + IND + "const double* dx  = patch_geom->getDx();" + NL + NL;
    }
    
    /**
     * Generate the code for the loops over the cells.
     * @param coordinates   The coordinates
     * @param indent        The actual indent
     * @return              The code
     */
    private static String getCellLoop(ArrayList<String> coordinates, String indent) {
        String loop = "";
        String actualIndent = indent;
        
        for (int i = coordinates.size() - 1; i >= 0; i--) {
            loop = loop + actualIndent + "for (" + coordinates.get(i) + " = 0; " + coordinates.get(i) + " < " 
                + coordinates.get(i) + "last && !workingPatchArray[pcounter]; " + coordinates.get(i) + "++) {" + NL;
            actualIndent = actualIndent + IND;
        }
        return loop;
    }
    
    /**
     * Generate the code for the end of the loops over the cells.
     * @param coordinates   The coordinates
     * @param indent        The actual indent
     * @return              The code
     */
    private static String getCellLoopEnd(ArrayList<String> coordinates, String indent) {
        String loop = "";
        String actualIndent = indent;
        
        for (int i = coordinates.size() - 1; i >= 0; i--) {
            loop = actualIndent + "}" + NL + loop;
            actualIndent = actualIndent + IND;
        }
        return loop;
    }
    
    /**
     * Assign the region to the FOVs variables.
     * @param doc           The document
     * @param regionId     The region id
     * @param regionIds    All the region ids
     * @param index         The array index
     * @param coords        The coordinates
     * @param indent        The actual indent
     * @return              The code
     * @throws CGException  CG00X External error
     */
    private static String assignFOV(Document doc, int regionId, ArrayList<String> regionIds, String index, 
            ArrayList<String> coords, String indent) throws CGException {
        String fov = "";
        String segString = CodeGeneratorUtils.getRegionName(doc, regionId, coords);
        fov = fov + indent + "vector(FOV_" + segString + ", " + index + ") = 100;" + NL;
        for (int i = 0; i < regionIds.size(); i++) {
            String segId = regionIds.get(i);
            if (!segId.equals(segString)) {
                fov = fov + indent + "vector(FOV_" + segId + ", " + index + ") = 0;" + NL;
            }
        }
        
        return fov;
    }
    
    /**
     * Initialize all FOVs.
     * @param regionIds    All the region ids
     * @param index         The array index
     * @param indent        The actual indent
     * @return              The code
     * @throws CGException  CG00X External error
     */
    private static String initFOV(ArrayList<String> regionIds, String index, String indent) throws CGException {
        String fov = "";
        for (int i = 0; i < regionIds.size(); i++) {
            String segId = regionIds.get(i);
            fov = fov + indent + "vector(FOV_" + segId + ", " + index + ") = 0;" + NL;
        }
        
        return fov;
    }
    
    /**
     * Fov initialization.
     * @param pi                The problem info
     * @return                  The mapping
     * @throws CGException      CG004 External error
     */
    private static String fovInitialization(ProblemInfo pi) throws CGException {
        ArrayList<String> coordinates = pi.getCoordinates();
        String[][] xslParams = new String [3][2];
        xslParams[0][0] = "condition";
        xslParams[0][1] = "true";
        xslParams[1][0] = "indent";
        xslParams[1][1] = "0";
        xslParams[2][0] =  "dimensions";
        xslParams[2][1] =  String.valueOf(coordinates.size());
        
        
        String loop = "";
        String endLoop = "";
        String index = "";
        String indent = IND + IND + IND + IND;
        for (int i = 0; i < pi.getDimensions(); i++) {
            loop = loop + indent + "for (" + coordinates.get(i) + " = 0; " 
                + coordinates.get(i) + " < " + coordinates.get(i) + "last; " + coordinates.get(i) + "++) {" + NL;
            index = index + coordinates.get(i) + ", ";
            endLoop = indent + "}" + NL + endLoop;
            indent = indent + IND;
        }
        index = index.substring(0, index.lastIndexOf(","));

        return IND + IND + IND + "//FOV initialization" + NL
                + getPatchLoop(pi, IND + IND + IND, false, false)
                + SAMRAIUtils.getLasts(pi, IND + IND + IND + IND, true) + NL
                + loop
                + initFOV(pi.getRegionIds(), index, indent)
                + endLoop
                + IND + IND + IND + "}" + NL
                + IND + IND + IND + "d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);" + NL + NL;
    }
    
    /**
     * Gives the particle support to SAMRAI grid. Instantiates the Particles class over every cell.
     * @param doc     			Document
     * @param regionIds			Region Ids
     * @param coords            The spatial coordinates
     * @return                  The code
     * @throws CGException      CG00X External error
     */
    private static String particleSupport(Document doc, ArrayList<String> regionIds, ArrayList<String> coords) throws CGException {
        int dim = coords.size();
        String loop = "";
        String index = "";
        String currentIndent = IND + IND;
        String endLoop = "";
        String indexCell = "";
        for (int i = dim - 1; i >= 0; i--) {
            String coord = coords.get(i);
            loop = loop + currentIndent + "for (" + coord + " = boxfirst(" + i + "); " + coord + " <= boxlast(" + i + "); " + coord + "++) {" + NL;
            endLoop = currentIndent + "}" + NL + endLoop;
            currentIndent = currentIndent + IND;
            index = ", " + coord + index;
            indexCell = ", " + coord + " - boxfirst(" + i + ")" + indexCell;
        }
        index = index.substring(2);
        indexCell = indexCell.substring(2);
                
        return IND + IND + "//Give particles support" + NL
            + loop
            + currentIndent + "hier::Index idx(" + index + ");" + NL
            + currentIndent + "if (!particleVariables->isElement(idx)) {" + NL
            + currentIndent + IND + "Particles<T>* part = new Particles<T>();" + NL
            + currentIndent + IND + "particleVariables->addItemPointer(idx, part);" + NL
            + currentIndent + "}" + NL
            + currentIndent + "if (!nonSyncVariable->isElement(idx)) {" + NL
            + currentIndent + IND + "NonSyncs* nonSyncPart = new NonSyncs();" + NL
            + currentIndent + IND + "nonSyncVariable->addItemPointer(idx, nonSyncPart);" + NL
            + currentIndent + "}" + NL
            + endLoop;
    }
    
    /**
     * Generate the code to map a region.
     * @param regionInfo        The region info
     * @param gridLimit         The limits of the problem coordinates
     * @param coordinates       The coordinates of the problem
     * @return                  The generated code
     * @throws CGException      CG004 External error
     */
    public static String mapRegionParticles(MappingRegionInfo regionInfo, LinkedHashMap<String, CoordinateInfo> gridLimit, ArrayList<String> coordinates) throws CGException {
        String[][] xslParams = new String [3][2];
        xslParams[0][0] = "condition";
        xslParams[0][1] = "true";
        xslParams[1][0] = "indent";
        xslParams[1][1] = "0";
        xslParams[2][0] = "dimensions";
        xslParams[2][1] = String.valueOf(coordinates.size());
        String result = "";
        
        //Take the grid minimum, maximum and deltas for all the problem coordinates
        String[] minGrid = new String[coordinates.size()];
        String[] maxGrid = new String[coordinates.size()];
        for (int i = 0; i < coordinates.size(); i++) {
            minGrid[i] = SAMRAIUtils.xmlTransform(gridLimit.get(coordinates.get(i)).getMin(), xslParams);
            maxGrid[i] = SAMRAIUtils.xmlTransform(gridLimit.get(coordinates.get(i)).getMax(), xslParams);
        }
        
        ArrayList<String> x3d = regionInfo.getX3d();
		for (int i = 0; i < x3d.size(); i++) {
			result = result + mapX3dParticles(regionInfo, coordinates, gridLimit);
		}
        //mathematical region
        if (regionInfo.getCoordinateLimits().size() > 0) {
	        //Map interior mathematical region
	        if (regionInfo.hasInterior()) {
	            result = result + mapMathematicalInteriorParticles(regionInfo, gridLimit, coordinates, minGrid, maxGrid, false);
	        }
	        //Surface mathematical region
	        if (regionInfo.hasSurface()) {
	            regionInfo.setId(regionInfo.getId() + 1);
	            result = result + mapMathematicalSurfaceParticles(regionInfo, gridLimit, coordinates, minGrid, maxGrid);
	        }
        }
        return result;
    }
    
    /**
     * Generate the code to map a region.
     * @param regionInfo        The region info
     * @param gridLimit         The limits of the problem coordinates
     * @param coordinates       The coordinates of the problem
     * @return                  The generated code
     * @throws CGException      CG004 External error
     */
    public static String mapRegionParticlesBox(MappingRegionInfo regionInfo, LinkedHashMap<String, CoordinateInfo> gridLimit, ArrayList<String> coordinates) throws CGException {
        String[][] xslParams = new String [3][2];
        xslParams[0][0] = "condition";
        xslParams[0][1] = "true";
        xslParams[1][0] = "indent";
        xslParams[1][1] = "0";
        xslParams[2][0] = "dimensions";
        xslParams[2][1] = String.valueOf(coordinates.size());
        String result = "";
        
        //Take the grid minimum, maximum and deltas for all the problem coordinates
        String[] minGrid = new String[coordinates.size()];
        String[] maxGrid = new String[coordinates.size()];
        for (int i = 0; i < coordinates.size(); i++) {
            minGrid[i] = SAMRAIUtils.xmlTransform(gridLimit.get(coordinates.get(i)).getMin(), xslParams);
            maxGrid[i] = SAMRAIUtils.xmlTransform(gridLimit.get(coordinates.get(i)).getMax(), xslParams);
        }
        
        ArrayList<String> x3d = regionInfo.getX3d();
		for (int i = 0; i < x3d.size(); i++) {
			result = result + "TBOX_ERROR(\"Box distribution not available when using X3D regions\");" + NL;
		}
        //mathematical region
        if (regionInfo.getCoordinateLimits().size() > 0) {
	        //Map interior mathematical region
	        if (regionInfo.hasInterior()) {
	            result = result + mapMathematicalInteriorParticles(regionInfo, gridLimit, coordinates, minGrid, maxGrid, true);
	        }
	        //Surface mathematical region
	        if (regionInfo.hasSurface()) {
	        	result = result + "TBOX_ERROR(\"Box distribution not available when mapping surface regions\");" + NL;
	        }
        }
        return result;
    }
    
    /**
     * Generate the code to map a region at random position.
     * @param problem			Problem
     * @param regionInfo        The region info
     * @param gridLimit         The limits of the problem coordinates
     * @param coordinates       The coordinates of the problem
     * @return                  The generated code
     * @throws CGException      CG004 External error
     */
    public static String mapRegionParticlesRandom(Document problem, MappingRegionInfo regionInfo, LinkedHashMap<String, CoordinateInfo> gridLimit, ArrayList<String> coordinates) throws CGException {
        String[][] xslParams = new String [3][2];
        xslParams[0][0] = "condition";
        xslParams[0][1] = "true";
        xslParams[1][0] = "indent";
        xslParams[1][1] = "0";
        xslParams[2][0] = "dimensions";
        xslParams[2][1] = String.valueOf(coordinates.size());
        String result = "";
        
        //Take the grid minimum, maximum and deltas for all the problem coordinates
        String[] minGrid = new String[coordinates.size()];
        String[] maxGrid = new String[coordinates.size()];
        for (int i = 0; i < coordinates.size(); i++) {
            minGrid[i] = SAMRAIUtils.xmlTransform(gridLimit.get(coordinates.get(i)).getMin(), xslParams);
            maxGrid[i] = SAMRAIUtils.xmlTransform(gridLimit.get(coordinates.get(i)).getMax(), xslParams);
        }
        
        ArrayList<String> x3d = regionInfo.getX3d();
		for (int i = 0; i < x3d.size(); i++) {
			result = result + "TBOX_ERROR(\"Random distribution not available when using X3D regions\");" + NL;
		}
        //mathematical region
        if (regionInfo.getCoordinateLimits().size() > 0) {
	        //Map interior mathematical region
	        if (regionInfo.hasInterior()) {
	            result = result + mapMathematicalInteriorParticlesRandom(problem, regionInfo, gridLimit, coordinates, minGrid, maxGrid);
	        }
	        //Surface mathematical region
	        if (regionInfo.hasSurface()) {
	            regionInfo.setId(regionInfo.getId() + 1);
	            result = result + mapMathematicalSurfaceParticlesRandom(problem, regionInfo, gridLimit, coordinates, minGrid, maxGrid);
	        }
        }
        return result;
    }
    
    /**
     * Generate the code to maps a x3d region.
     * @param regionInfo        The region information
     * @param coordinates       The coordinates of the problem
     * @param gridLimit         The coordinate grid limits
     * @return                  The code generated
     * @throws CGException      CG004 External error
     */
    private static String mapX3dParticles(MappingRegionInfo regionInfo, ArrayList<String> coordinates, LinkedHashMap<String, CoordinateInfo> gridLimit) throws CGException {
        String regionName = regionInfo.getRegionName();
        int regionNum = regionInfo.getId();
        int originalRegionNumber = regionNum;
        String[][] xslParams = new String [3][2];
        xslParams[0][0] = "condition";
        xslParams[0][1] = "true";
        xslParams[1][0] = "indent";
        xslParams[1][1] = "0";
        xslParams[2][0] =  "dimensions";
        xslParams[2][1] =  String.valueOf(coordinates.size());
        try {
            String result = "";
            String currentIndent = IND + IND + IND;

            //Surface region
            if (regionInfo.hasSurface()) {
                result = result + currentIndent + "//Region: " + regionName + "S" + NL;
                regionNum++;
            }
            //Fill the faces
            regionInfo.setId(regionNum);
            result = result + faceFillingParticles(regionInfo, coordinates, gridLimit, false);
            //Check the stencil in the surface and fill if necessary
            result = result + faceFillingParticles(regionInfo, coordinates, gridLimit, true);
            //Interior region
            if (regionInfo.hasInterior()) {
                regionInfo.setId(originalRegionNumber);
                result = result + currentIndent + "//Region: " + regionName + "I" + NL
                    + fillx3dInteriorParticles(regionInfo.getId(), coordinates);
            }
            return result;
        } 
        catch (DOMException e) {
            throw new CGException(CGException.CG00X, e.getMessage());
        }
    }
    
    /**
     * Creates the code to fill the interior of a x3d region.
     * It uses a flood-fill algorithm adapted to multiprocessor
     * @param problem           The problem
     * @param regionId         The region id
     * @param coordinates       The coordinates
     * @return                  The generated code
     * @throws CGException      CG004 External error
     */
    private static String fillx3dInteriorParticles(int regionId, ArrayList<String> coordinates) throws CGException {
        String currentIndent = IND + IND + IND + IND + IND;
        String workingLoop = "";
        String endWorkingLoop = "";
        for (int i = coordinates.size() - 1; i >= 0; i--) {
            String coord = coordinates.get(i);
            workingLoop = workingLoop + currentIndent + IND + "for (int count_" + coord + " = 0; " + coord + "MapStart + count_" + coord 
                + " * particleSeparation[" + i + "] < " + coord + "MapEnd && !working; count_" + coord + "++) {" + NL;
            endWorkingLoop = currentIndent + IND + "}" + NL + endWorkingLoop;
            currentIndent = currentIndent + IND;
        }
        String initialPoint = "";
        String initialIndex = "";
        String index = "";
        String positions = "";
        String limitCondition = "";
        String countIndex = "";
        for (int i = 0; i < coordinates.size(); i++) {
            String coord = coordinates.get(i);
            initialPoint = initialPoint + "boxfirst1(" + i + ") == 0 && ";
            initialIndex = initialIndex + "0, domain_offset_factor[" + i + "], ";
            index = index + coordinates.get(i) + ", ";
            positions = positions + currentIndent + IND + "position[" + i + "] = " + coord + "MapStart + count_" + coord + " * particleSeparation[" + i + "];" + NL
                + currentIndent + IND + coord + " = floor((position[" + i + "] - d_grid_geometry->getXLower()[" + i + "])/dx[" + i + "] + 1E-10);" + NL;
            limitCondition = limitCondition + coord + " >= boxfirst(" + i + ") && " + coord + " <= boxlast(" + i + ") && ";
            countIndex = countIndex + "count_" + coord + ", domain_offset_factor[" + i + "], ";
        }
        limitCondition = limitCondition.substring(0, limitCondition.lastIndexOf(" &&"));
        initialPoint = initialPoint.substring(0, initialPoint.lastIndexOf(" &&"));
        index = index.substring(0, index.lastIndexOf(","));
        countIndex = countIndex.substring(0, countIndex.lastIndexOf(","));
        String finishedCheck = finishedCheck(coordinates);
        String initFloodFill = workingLoop
            + positions
            + currentIndent + IND + "if (" + limitCondition + ") {" + NL
            + currentIndent + IND + IND + "hier::Index idx(" + index + ");" + NL
            + currentIndent + IND + IND + "NonSyncs* nonSyncpart = nonSyncVariable->getItem(idx);" + NL
            + currentIndent + IND + IND + "NonSync* nonSyncparticle = new NonSync(0, position);" + NL
            + currentIndent + IND + IND + "NonSync* oldParticle = nonSyncpart->overlaps(*nonSyncparticle);" + NL
            + currentIndent + IND + IND + "if (oldParticle->getnonSync() == 0) {" + NL
            + currentIndent + IND + IND + IND + "floodfillParticles(patch, particleSeparation, domain_offset_factor, " + countIndex + ", pred, " + regionId + ");" + NL
            + currentIndent + IND + IND + IND + "working = 1;" + NL
            + currentIndent + IND + IND + "}" + NL
            + currentIndent + IND + IND + "delete nonSyncparticle;" + NL
            + currentIndent + IND + "}" + NL
            + endWorkingLoop;
        
        return IND + IND + IND + "//Fill the interior of the region" + NL 
            + initializeInterior(coordinates, regionId)
            + IND + IND + IND + "//Initial FloodFill call" + NL
            + IND + IND + IND + "finishedGlobal = false;" + NL
            + IND + IND + IND + "working = 0;" + NL
            + IND + IND + IND + "finished = 0;" + NL
            + IND + IND + IND + "nodes = mpi.getSize();" + NL
            + IND + IND + IND + "pred = 1;" + NL
            + IND + IND + IND + "//Limits for the finished checking routine to avoid internal boundary problems" + NL
            + getFinishLimits(coordinates) + NL
            + IND + IND + IND + "//First floodfill" + NL
            + IND + IND + IND + "if (" + initialPoint + ") {" + NL
            + IND + IND + IND + IND + "floodfillParticles(patch, particleSeparation, domain_offset_factor, " + initialIndex + "1, " + regionId + ");" + NL
            + IND + IND + IND + IND + "working = 1;" + NL
            + IND + IND + IND + "}" + NL
            + IND + IND + IND + "if (nodes > 1) {" + NL
            + getInteriorMultiproc(regionId, coordinates, finishedCheck, initFloodFill)
            + IND + IND + IND + "}" + NL
            + IND + IND + IND + "//Only one processor" + NL
            + IND + IND + IND + "else {" + NL
            + IND + IND + IND + IND + "while (!finished) {" + NL
            + finishedCheck
            + IND + IND + IND + IND + IND + "if (pred == 1) {" + NL
            + IND + IND + IND + IND + IND + IND + "pred = 2;" + NL
            + IND + IND + IND + IND + IND + "} else {" + NL
            + IND + IND + IND + IND + IND + IND + "pred = 1;" + NL
            + IND + IND + IND + IND + IND + "}" + NL
            + IND + IND + IND + IND + IND + "//New floodfill" + NL
            + IND + IND + IND + IND + IND + "if (!finished) {" + NL
            + IND + IND + IND + IND + IND + IND + "working = 0;" + NL
            + initFloodFill
            + IND + IND + IND + IND + IND + "}" + NL
            + IND + IND + IND + IND + "}" + NL
            + IND + IND + IND + "}" + NL + NL;
    }
    
    /**
     * Generates the code for the multiprocessor part on the fill x3d interior algorithm. 
     * @param coordinates       The spatial coordinates
     * @param regionId         The identifier of the region
     * @param finishedCheck     The code for checking the finalization of the algorithm
     * @param initFloodFill     The code for starting the floodfill algorithm
     * @return                  The code
     * @throws CGException      CG004 External error
     */
    private static String getInteriorMultiproc(int regionId, ArrayList<String> coordinates, String finishedCheck, 
            String initFloodFill) throws CGException {
        String actualIndent = IND + IND + IND + IND + IND;
        String workingLoop = "";
        String endWorkingLoop = "";
        for (int i = coordinates.size() - 1; i >= 0; i--) {
            String coord = coordinates.get(i);
            workingLoop = workingLoop + actualIndent + IND + "for (int count_" + coord + " = 0; " + coord + "MapStart + count_" + coord 
                + " * particleSeparation[" + i + "] < " + coord + "MapEnd && !working; count_" + coord + "++) {" + NL;
            endWorkingLoop = actualIndent + IND + "}" + NL + endWorkingLoop;
            actualIndent = actualIndent + IND;
        }
        String index = "";
        String positions = "";
        String limitCondition = "";
        String countIndex = "";
        for (int i = 0; i < coordinates.size(); i++) {
            String coord = coordinates.get(i);
            index = index + coordinates.get(i) + ", ";
            positions = positions + actualIndent + IND + "position[" + i + "] = " + coord + "MapStart + count_" + coord + " * particleSeparation[" + i + "];" + NL
                + actualIndent + IND + coord + " = floor((position[" + i + "] - d_grid_geometry->getXLower()[" + i + "])/dx[" + i + "] + 1E-10);" + NL;
            limitCondition = limitCondition + coord + " >= boxfirst(" + i + ") && " + coord + " <= boxlast(" + i + ") && ";
            countIndex = countIndex + "count_" + coord + ", domain_offset_factor[" + i + "], ";
        }
        limitCondition = limitCondition.substring(0, limitCondition.lastIndexOf(" &&"));
        index = index.substring(0, index.lastIndexOf(","));
        countIndex = countIndex.substring(0, countIndex.lastIndexOf(","));
        return IND + IND + IND + IND + "d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);" + NL + NL
            + IND + IND + IND + IND + "while (!finishedGlobal) {" + NL
            + IND + IND + IND + IND + IND + "workingGlobal = true;" + NL
            + IND + IND + IND + IND + IND + "//Filling started floodfill in all the patches" + NL
            + IND + IND + IND + IND + IND + "while (workingGlobal) {" + NL
            + IND + IND + IND + IND + IND + IND + "working = 0;" + NL
            + workingLoop
            + positions 
            + actualIndent + IND + "if (" + limitCondition + ") {" + NL
            + actualIndent + IND + IND + "hier::Index idx(" + index + ");" + NL
            + actualIndent + IND + IND + "Particles<T>* part = particleVariables->getItem(idx);" + NL
            + actualIndent + IND + IND + "NonSyncs* nonSyncpart = nonSyncVariable->getItem(idx);" + NL
            + actualIndent + IND + IND + "T* particle = new T(0, position, 0);" + NL
            + actualIndent + IND + IND + "T* oldParticle = part->overlaps(*particle);" + NL
            + actualIndent + IND + IND + "NonSync* nonSyncparticle = new NonSync(0, position);" + NL
            + actualIndent + IND + IND + "NonSync* nonSyncoldParticle = nonSyncpart->overlaps(*nonSyncparticle);" + NL
            + actualIndent + IND + IND + "if (nonSyncoldParticle->getnonSync() != oldParticle->interior && oldParticle->interior != 0) {"
            + NL
            + actualIndent + IND + IND + IND + "floodfillParticles(patch, particleSeparation, domain_offset_factor, " + countIndex + ", pred, " + regionId + ");" + NL
            + actualIndent + IND + IND + IND + "working = 1;" + NL
            + actualIndent + IND + IND + "}" + NL
            + actualIndent + IND + IND + "delete particle;" + NL
            + actualIndent + IND + IND + "delete nonSyncparticle;" + NL
            + actualIndent + IND + "}" + NL
            + endWorkingLoop
            + IND + IND + IND + IND + IND + IND + "d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);" + NL
            + IND + IND + IND + IND + IND + IND + "//Local working variable communication" + NL
            + IND + IND + IND + IND + IND + IND + "for (proc = 0; proc < nodes; proc++) {" + NL
            + IND + IND + IND + IND + IND + IND + IND + "if (proc == mpi.getRank()) {" + NL
            + IND + IND + IND + IND + IND + IND + IND + IND + "for (int dproc = 0; dproc < nodes; dproc++) {" + NL
            + IND + IND + IND + IND + IND + IND + IND + IND + IND + "if (dproc != mpi.getRank()) {" + NL
            + IND + IND + IND + IND + IND + IND + IND + IND + IND + IND + "int *w = new int[1];" + NL
            + IND + IND + IND + IND + IND + IND + IND + IND + IND + IND + "w[0] = working;" + NL
            + IND + IND + IND + IND + IND + IND + IND + IND + IND + IND + "int size = 1;" + NL
            + IND + IND + IND + IND + IND + IND + IND + IND + IND + IND + "mpi.Send(w, size, MPI_INT, dproc, 0);" + NL
            + IND + IND + IND + IND + IND + IND + IND + IND + IND + "}" + NL
            + IND + IND + IND + IND + IND + IND + IND + IND + "}" + NL
            + IND + IND + IND + IND + IND + IND + IND + IND + "workingArray[proc] = working;" + NL
            + IND + IND + IND + IND + IND + IND + IND + "} else {" + NL
            + IND + IND + IND + IND + IND + IND + IND + IND + "int *w = new int[1];" + NL
            + IND + IND + IND + IND + IND + IND + IND + IND + "int size = 1;" + NL
            + IND + IND + IND + IND + IND + IND + IND + IND + "tbox::SAMRAI_MPI::Status status;" + NL
            + IND + IND + IND + IND + IND + IND + IND + IND + "mpi.Recv(w, size, MPI_INT, proc, 0, &status);" + NL
            + IND + IND + IND + IND + IND + IND + IND + IND + "workingArray[proc] = w[0];" + NL
            + IND + IND + IND + IND + IND + IND + IND + "}" + NL
            + IND + IND + IND + IND + IND + IND + "}" + NL
            + IND + IND + IND + IND + IND + IND + "workingGlobal = false;" + NL
            + IND + IND + IND + IND + IND + IND + "for (proc = 0; proc < nodes & !workingGlobal; proc++) {" + NL
            + IND + IND + IND + IND + IND + IND + IND + "if (workingArray[proc] == 1) {" + NL
            + IND + IND + IND + IND + IND + IND + IND + IND + "workingGlobal = true;" + NL
            + IND + IND + IND + IND + IND + IND + IND + "}" + NL
            + IND + IND + IND + IND + IND + IND + "}" + NL
            + IND + IND + IND + IND + IND + "}" + NL + NL
            + finishedCheck
            + IND + IND + IND + IND + IND + "//Finished local variables communication" + NL
            + IND + IND + IND + IND + IND + "for (proc = 0; proc < nodes; proc++) {" + NL
            + IND + IND + IND + IND + IND + IND + "if (proc == mpi.getRank()) {" + NL
            + IND + IND + IND + IND + IND + IND + IND + "for (int dproc = 0; dproc < nodes; dproc++) {" + NL
            + IND + IND + IND + IND + IND + IND + IND + IND + "if (dproc != mpi.getRank()) {" + NL
            + IND + IND + IND + IND + IND + IND + IND + IND + IND + "int *w = new int[1];" + NL
            + IND + IND + IND + IND + IND + IND + IND + IND + IND + "w[0] = finished;" + NL
            + IND + IND + IND + IND + IND + IND + IND + IND + IND + "int size = 1;" + NL
            + IND + IND + IND + IND + IND + IND + IND + IND + IND + "mpi.Send(w, size, MPI_INT, dproc, 0);" + NL
            + IND + IND + IND + IND + IND + IND + IND + IND + "}" + NL
            + IND + IND + IND + IND + IND + IND + IND + "}" + NL
            + IND + IND + IND + IND + IND + IND + IND + "finishedArray[proc] = finished;" + NL
            + IND + IND + IND + IND + IND + IND + "} else {" + NL
            + IND + IND + IND + IND + IND + IND + IND + "int *w = new int[1];" + NL
            + IND + IND + IND + IND + IND + IND + IND + "int size = 1;" + NL
            + IND + IND + IND + IND + IND + IND + IND + "tbox::SAMRAI_MPI::Status status;" + NL
            + IND + IND + IND + IND + IND + IND + IND + "mpi.Recv(w, size, MPI_INT, proc, 0, &status);" + NL
            + IND + IND + IND + IND + IND + IND + IND + "finishedArray[proc] = w[0];" + NL
            + IND + IND + IND + IND + IND + IND + "}" + NL
            + IND + IND + IND + IND + IND + "}" + NL
            + IND + IND + IND + IND + IND + "finishedGlobal = true;" + NL
            + IND + IND + IND + IND + IND + "for (proc = 0; proc < nodes & finishedGlobal; proc++) {" + NL
            + IND + IND + IND + IND + IND + IND + "if (finishedArray[proc] == 0) {" + NL
            + IND + IND + IND + IND + IND + IND + IND + "finishedGlobal = 0;" + NL
            + IND + IND + IND + IND + IND + IND + "}" + NL
            + IND + IND + IND + IND + IND + "}" + NL + NL
            + IND + IND + IND + IND + IND + "//Only one processor could start a floodfill at the same time" + NL
            + IND + IND + IND + IND + IND + "//Take the first who wants to" + NL
            + IND + IND + IND + IND + IND + "int first = -1;" + NL
            + IND + IND + IND + IND + IND + "for (proc = 0; proc < nodes && !finishedGlobal && first < 0; proc++) {" + NL
            + IND + IND + IND + IND + IND + IND + "if (finishedArray[proc] == 0) {" + NL
            + IND + IND + IND + IND + IND + IND + IND + "first = proc;" + NL
            + IND + IND + IND + IND + IND + IND + "}" + NL
            + IND + IND + IND + IND + IND + "}" + NL
            + IND + IND + IND + IND + IND + "if (pred == 1) {" + NL
            + IND + IND + IND + IND + IND + IND + "pred = 2;" + NL
            + IND + IND + IND + IND + IND + "} else {" + NL
            + IND + IND + IND + IND + IND + IND + "pred = 1;" + NL
            + IND + IND + IND + IND + IND + "}" + NL
            + IND + IND + IND + IND + IND + "if (first == mpi.getRank() && !finishedGlobal) {" + NL
            + initFloodFill
            + IND + IND + IND + IND + IND + "}" + NL
            + IND + IND + IND + IND + IND + "d_mapping_fill->createSchedule(level, level)->fillData(initial_time, true);" + NL
            + IND + IND + IND + IND + "}" + NL;
    }
    
    /**
     * Get the finished limits to avoid multiprocessor problems near internal boundaries.
     * @param coordinates   Problem coordinates
     * @return              Code
     */
    private static String getFinishLimits(ArrayList<String> coordinates) {
        String result = "";
        for (int i = 0; i < coordinates.size(); i++) {
            String coord = coordinates.get(i);
            result = result + IND + IND + IND + coord + "l = boxfirst(" + i + ");" + NL
                + IND + IND + IND + "if (!patch.getPatchGeometry()->getTouchesRegularBoundary (" + i + ", 0)) {" + NL
                + IND + IND + IND + IND + coord + "l = boxfirst(" + i + ") + d_ghost_width;" + NL
                + IND + IND + IND + "}" + NL
                + IND + IND + IND + coord + "u = boxlast(" + i + ");" + NL
                + IND + IND + IND + "if (!patch.getPatchGeometry()->getTouchesRegularBoundary (" + i + ", 1)) {" + NL
                + IND + IND + IND + IND + coord + "u = boxlast(" + i + ") - d_ghost_width;" + NL
                + IND + IND + IND + "}" + NL;
        }
        return result;
    }
    
    /**
     * Code to initialize the interior and nonSync variable.
     * @param coordinates   The problem coordinates
     * @param region       The region number
     * @return              The code
     * @throws CGException  CG004 External error
     */
    private static String initializeInterior(ArrayList<String> coordinates, int region) throws CGException {
        String currentIndent = IND + IND + IND;
        String startLoop = "";
        String endLoop = "";
        String index = "";
        String preLoop = "";
        String positions = "";
        String limitCondition = "";
        for (int i = coordinates.size() - 1; i >= 0; i--) {
            String coord = coordinates.get(i);
            index = coord + ", " +  index;
            startLoop = startLoop + currentIndent + "for (int count_" + coord + " = 0; " + coord + "MapStart + count_" + coord 
                + " * particleSeparation[" + i + "] < " + coord + "MapEnd; count_" + coord + "++) {" + NL;
            endLoop = currentIndent + "}" + NL + endLoop;
            currentIndent = currentIndent + IND;
            preLoop = preLoop + IND + IND + IND + coord + "MapStart = d_grid_geometry->getXLower()[" + i + "] + domain_offset_factor[" + i + "] * particleSeparation[" + i + "]" 
                + " - (floor(0.5 + d_ghost_width * (dx[" + i + "]/particleSeparation[" + i + "])))*particleSeparation[" + i + "];" + NL
                + IND + IND + IND + coord + "MapEnd = d_grid_geometry->getXUpper()[" + i + "] + d_ghost_width * dx[" 
                + i + "];" + NL + preLoop;
        }
        for (int i = coordinates.size() - 1; i >= 0; i--) {
            String coord = coordinates.get(i);
            positions = currentIndent + "position[" + i + "] = " + coord + "MapStart + count_" + coord + " * particleSeparation[" + i + "];" + NL
                + currentIndent + coord + " = floor((position[" + i + "] - d_grid_geometry->getXLower()[" + i + "])/dx[" + i + "] + 1E-10);" + NL + positions;
            limitCondition = coord + " >= boxfirst(" + i + ") && " + coord + " <= boxlast(" + i + ") && " + limitCondition;
        }
        limitCondition = limitCondition.substring(0, limitCondition.lastIndexOf(" &&"));
        index = index.substring(0, index.lastIndexOf(","));
        
        return IND + IND + IND + "//Initialize the variables and set the walls as exterior" + NL
            + preLoop + startLoop
            + positions
            + currentIndent + "if (" + limitCondition + ") {" + NL
            + currentIndent + IND + "hier::Index idx(" + index + ");" + NL
            + currentIndent + IND + "Particles<T>* part = particleVariables->getItem(idx);" + NL
            + currentIndent + IND + "NonSyncs* partNonSync = nonSyncVariable->getItem(idx);" + NL
            + currentIndent + IND + "T* particle = new T(0, position, 0);" + NL
            + currentIndent + IND + "T* oldParticle = part->overlaps(*particle);" + NL
            + currentIndent + IND + "NonSync* nonSyncParticle = new NonSync(0, position);" + NL
            + currentIndent + IND + "if (oldParticle != NULL) {" + NL
            + currentIndent + IND + IND + "//If wall, then it is considered exterior" + NL
            + currentIndent + IND + IND + "if (oldParticle->region == " + region + " || oldParticle->region == " 
            + (region + 1) + ") {" + NL
            + currentIndent + IND + IND + IND + "oldParticle->interior = 1;" + NL
            + currentIndent + IND + IND + IND + "nonSyncParticle->setnonSync(1);" + NL
            + currentIndent + IND + IND + "} else {" + NL
            + currentIndent + IND + IND + IND + "oldParticle->interior = 0;" + NL
            + currentIndent + IND + IND + "}" + NL
            + currentIndent + IND + "} else {" + NL
            + currentIndent + IND + IND + "particle->interior = 0;" + NL
            + currentIndent + IND + IND + "part->addParticle(*particle);" + NL
            + currentIndent + IND + "}" + NL
            + currentIndent + IND + "partNonSync->addNonSync(*nonSyncParticle);" + NL
            + currentIndent + IND + "delete particle;" + NL
            + currentIndent + "}" + NL + endLoop;
    }
    
    /**
     * Get the code to check if the flood fill general algorith has finished.
     * @param coordinates       The coordinates
     * @return                  The code
     * @throws CGException 
     */
    private static String finishedCheck(ArrayList<String> coordinates) throws CGException {
        String currentIndent = IND + IND + IND + IND + IND;
        String finishedLoop = "";
        String endFinishedLoop = "";
        String positions = "";
        for (int i = coordinates.size() - 1; i >= 0; i--) {
            String coord = coordinates.get(i);
            endFinishedLoop = currentIndent + "}" + NL + endFinishedLoop;
            finishedLoop = finishedLoop + currentIndent + "for (int count_" + coord + " = 0; " + coord + "MapStart + count_" + coord 
                + " * particleSeparation[" + i + "] < " + coord + "MapEnd && finished; count_" + coord + "++) {" + NL;
            currentIndent = currentIndent + IND;
        }
        String index = "";
        String cond = "";
        for (int i = 0; i < coordinates.size(); i++) {
            String coord = coordinates.get(i);
            index = index + coordinates.get(i) + ", ";
            positions = positions + currentIndent + "position[" + i + "] = " + coord + "MapStart + count_" + coord + " * particleSeparation[" + i + "];" + NL
                + currentIndent + coord + " = floor((position[" + i + "] - d_grid_geometry->getXLower()[" + i + "])/dx[" + i + "] + 1E-10);" + NL;
            cond = cond + coord + " >= " + coord + "l && " + coord + " <= " + coord + "u && ";
        }
        index = index.substring(0, index.lastIndexOf(","));
        cond = cond.substring(0, cond.lastIndexOf(" &&"));
        
        return IND + IND + IND + IND + IND + "//Looking for a new floodfill" + NL
            + IND + IND + IND + IND + IND + "finished = 1;" + NL 
            + finishedLoop
            + positions
            + currentIndent + "if (" + cond + ") {" + NL
            + currentIndent + IND + "hier::Index idx(" + index + ");" + NL
            + currentIndent + IND + "NonSyncs* nonSyncpart = nonSyncVariable->getItem(idx);" + NL
            + currentIndent + IND + "NonSync* nonSyncparticle = new NonSync(0, position);" + NL
            + currentIndent + IND + "NonSync* oldParticle = nonSyncpart->overlaps(*nonSyncparticle);" + NL
            + currentIndent + IND + "if (oldParticle->getnonSync() == 0) {" + NL
            + currentIndent + IND + IND + "finished = 0;" + NL
            + currentIndent + IND + "}" + NL
            + currentIndent + IND + "delete nonSyncparticle;" + NL
            + currentIndent + "}" + NL
            + endFinishedLoop + NL;
    }
    
    /**
     * Maps the mathematical region interior.
     * @param regionInfo        The region information
     * @param gridLimit         The limits of the grid coordinates
     * @param coordinates       The coordinates
     * @param minGrid           The minimum values of the grid
     * @param maxGrid           The maximum values of the grid
     * @return                  The mapping
     * @throws CGException      CG004 External error
     */
    private static String mapMathematicalInteriorParticles(MappingRegionInfo regionInfo, LinkedHashMap<String, CoordinateInfo> gridLimit, 
            ArrayList<String> coordinates, String[] minGrid, String[] maxGrid, boolean box) throws CGException {
        String[][] xslParams = new String [3][2];
        xslParams[0][0] = "condition";
        xslParams[0][1] = "true";
        xslParams[1][0] = "indent";
        xslParams[1][1] = "0";
        xslParams[2][0] =  "dimensions";
        xslParams[2][1] =  String.valueOf(coordinates.size());
                
        String regionName = regionInfo.getRegionName();
        String code = "";
        for (LinkedHashMap<String, CoordinateInfo> regionLimit: regionInfo.getCoordinateLimits()) {
            String loop = "";
            String endLoop = "";
            String temporal = "";
            String index = "";
            String regionCondition = "";
            String limitCondition = "";
            String boxCondition = "";
            String positions = "";
            String indent = IND + IND + IND;
            for (int j = coordinates.size() - 1; j >= 0; j--) {
                String discCoord = coordinates.get(j);
                loop = loop + indent + "for (int count_" + discCoord + " = 0; " + discCoord + "MapStart + count_" + discCoord + " * particleSeparation[" + j + "]"
                    + " < " + discCoord + "MapEnd; count_" + discCoord + "++) {" + NL;
                if (j == coordinates.size() - 1 && !box) {
                	loop = loop + createStaggeringControl("count_" + discCoord, indent + IND, coordinates);
                }
                
                endLoop = indent + "}" + NL + endLoop;
                indent = indent + IND;
            }
            Iterator<String> coordIt = regionLimit.keySet().iterator();
            for (int i = 0; i < coordinates.size(); i++) {
                String regionCoord = coordIt.next();
                String discCoord = coordinates.get(i);
                index = index + discCoord + ", ";
                positions = positions + indent + "position[" + i + "] = " + discCoord + "MapStart + count_" + discCoord + " * particleSeparation[" + i + "];" + NL
                    + indent + discCoord + " = floor((position[" + i + "] - d_grid_geometry->getXLower()[" + i + "])/dx[" + i + "] + 1E-10);" + NL;
                limitCondition = limitCondition + discCoord + " >= boxfirst1(" + i + ") && " + discCoord + " <= boxlast1(" + i + ") && ";
                
                //Temporal calculation for maxGrid comparison. Particle could not be at the max value but their immediate inferior place.
                String minValue = SAMRAIUtils.xmlTransform(regionLimit.get(regionCoord).getMin(), xslParams);
                if (CodeGeneratorUtils.doubleable(maxGrid[i]) && CodeGeneratorUtils.doubleable(minValue) 
                        && Double.compare(Double.valueOf(maxGrid[i]), Double.valueOf(minValue)) == 0) {
                    temporal = indent + IND + "tmp" + discCoord + " = " + discCoord + "MapStart + (floor(d_grid_geometry->getXUpper()[" + i 
                        + "]/particleSeparation[" + i + "] - d_grid_geometry->getXLower()[" + i + "]/particleSeparation[" + i + "]" 
                        + " + 2 * d_ghost_width * (dx[" + i + "]/particleSeparation[" + i + "])) - 1) * particleSeparation[" + i + "];" + NL
                        + indent + IND + "if (Equals(tmp" + discCoord + ", " + maxGrid[i] + " + d_ghost_width * dx[" + i + "])) {" + NL
                        + indent + IND + IND + "tmp" + discCoord + " = tmp" + discCoord + " - particleSeparation[" + i + "];" + NL
                        + indent + IND + "}" + NL;
                }
                if (box) {
                	boxCondition = boxCondition + " && greaterEq(position[" + i + "], box_min[" + i + "]) && lessEq(position[" + i + "], box_max[" + i + "])";
                }
            }
            regionCondition = regionCondition + getRegionCondition(regionLimit, -1, coordinates, regionInfo, minGrid, maxGrid, gridLimit);
            
            limitCondition = limitCondition.substring(0, limitCondition.lastIndexOf(" &&"));
            index = index.substring(0, index.lastIndexOf(","));
            
        	code = code + loop
                + positions 
                + temporal
                + indent + "if (" + limitCondition + regionCondition + boxCondition + ") {" + NL
                + indent + IND + "hier::Index idx(" + index + ");" + NL
                + indent + IND + "Particles<T>* part = particleVariables->getItem(idx);" + NL
                + indent + IND + "T* particle = new T(0, position, " + regionInfo.getId() + ");"
                + NL
                + indent + IND + "T* oldParticle = part->overlaps(*particle);" + NL
                + indent + IND + "if (oldParticle != NULL) {" + NL
                + indent + IND + IND + "part->deleteParticle(*oldParticle);" + NL
                + indent + IND + "}" + NL
                + indent + IND + "part->addParticle(*particle);" + NL
                + indent + IND + "delete particle;" + NL
                + indent + "}" + NL
                + endLoop;
        }
        String preLoop = "";
        for (int i = 0; i < coordinates.size(); i++) {
            String discCoord = coordinates.get(i);
            if (i == coordinates.size() - 1 || box) {
            	preLoop = preLoop + IND + IND + IND + discCoord + "MapStart = d_grid_geometry->getXLower()[" + i + "] + domain_offset_factor[" + i + "] * particleSeparation[" + i + "]" 
                        + " - (floor(0.5 + d_ghost_width * (dx[" + i + "]/particleSeparation[" + i + "]" 
                        + ")))*particleSeparation[" + i + "];" + NL;	
            }
            preLoop = preLoop + IND + IND + IND + discCoord + "MapEnd = d_grid_geometry->getXUpper()[" + i + "] + d_ghost_width * dx[" + i + "];" + NL;
        }
        return IND + IND + IND + "//Region: " + regionName + "I" + NL
            + preLoop + code;
    }
    
    /**
     * Maps the mathematical region interior at random position.
     * @param problem			Problem
     * @param regionInfo       The region information
     * @param gridLimit         The limits of the grid coordinates
     * @param coordinates       The coordinates
     * @param minGrid           The minimum values of the grid
     * @param maxGrid           The maximum values of the grid
     * @return                  The mapping
     * @throws CGException      CG004 External error
     */
    private static String mapMathematicalInteriorParticlesRandom(Document problem, MappingRegionInfo regionInfo, LinkedHashMap<String, CoordinateInfo> gridLimit, 
            ArrayList<String> coordinates, String[] minGrid, String[] maxGrid) throws CGException {
        String[][] xslParams = new String [3][2];
        xslParams[0][0] = "condition";
        xslParams[0][1] = "true";
        xslParams[1][0] = "indent";
        xslParams[1][1] = "0";
        xslParams[2][0] =  "dimensions";
        xslParams[2][1] =  String.valueOf(coordinates.size());
                
        String regionName = regionInfo.getRegionName();
        String code = "";
        for (LinkedHashMap<String, CoordinateInfo> regionLimit: regionInfo.getCoordinateLimits()) {
            String index = "";
            String regionCondition = "";
            String limitCondition = "";
            for (int i = 0; i < coordinates.size(); i++) {
                String discCoord = coordinates.get(i);
                index = index + discCoord + ", ";
                limitCondition = limitCondition + discCoord + " >= boxfirst1(" + i + ") && " + discCoord + " <= boxlast1(" + i + ") && ";
            }
            
            regionCondition = regionCondition + getRegionCondition(regionLimit, -1, coordinates, regionInfo, minGrid, maxGrid, gridLimit);
            
            limitCondition = limitCondition.substring(0, limitCondition.lastIndexOf(" &&"));
            index = index.substring(0, index.lastIndexOf(","));
            
        	code = code
                + "if (" + limitCondition + regionCondition + ") {" + NL
                + IND + "hier::Index idx(" + index + ");" + NL
                + IND + "Particles<T>* part = particleVariables->getItem(idx);" + NL
                + IND + "T* particle = new T(0, position, " + regionInfo.getId() + ");" + NL
                + IND + "T* oldParticle = part->overlaps(*particle);" + NL
                + IND + "if (oldParticle != NULL) {" + NL
                + IND + IND + "part->deleteParticle(*oldParticle);" + NL
                + IND + "}" + NL
                + IND + "part->addParticle(*particle);" + NL
                + IND + "delete particle;" + NL
                + "}" + NL;
        }
        return "//Region: " + regionName + "I" + NL
            +  code;
    }
    
    /**
     * Maps the mathematical region surface.
     * @param regionInfo       The region information
     * @param gridLimit         The limits of the grid coordinates
     * @param coordinates       The coordinates
     * @param minGrid           The minimum values of the grid
     * @param maxGrid           The maximum values of the grid
     * @return                  The mapping
     * @throws CGException      CG004 External error
     */
    private static String mapMathematicalSurfaceParticles(MappingRegionInfo regionInfo, LinkedHashMap<String, CoordinateInfo> gridLimit, 
            ArrayList<String> coordinates, String[] minGrid, String[] maxGrid) throws CGException {
        String[][] xslParams = new String [3][2];
        xslParams[0][0] = "condition";
        xslParams[0][1] = "true";
        xslParams[1][0] = "indent";
        xslParams[1][1] = "0";
        xslParams[2][0] =  "dimensions";
        xslParams[2][1] =  String.valueOf(coordinates.size());
        
        String regionName = regionInfo.getRegionName();
        String code = "";
        for (LinkedHashMap<String, CoordinateInfo> regionLimit: regionInfo.getCoordinateLimits()) {
        	String[] minValueInit = new String[regionLimit.size()];
            String[] maxValueInit = new String[regionLimit.size()];
            Iterator<String> coordIt = regionLimit.keySet().iterator();
            int i = 0;
            while (coordIt.hasNext()) {
                String coord = coordIt.next();
                minValueInit[i] = SAMRAIUtils.xmlTransform(regionLimit.get(coord).getMin(), xslParams);
                maxValueInit[i] = SAMRAIUtils.xmlTransform(regionLimit.get(coord).getMax(), xslParams);
                i++;
            }
            String loops = "";
            String loopsStencil = "";
            for (int j = 0; j < coordinates.size(); j++) {
                String loop = "";
                String loopStencil = "";
                String endLoop = "";
                String endLoopStencil = "";
                String currentIndent = IND + IND + IND;
                for (int k = coordinates.size() - 1; k >= 0; k--) {
                    String discCoord2 = coordinates.get(k);
                    if (k != j) {
                        loop = loop + currentIndent + "for (int count_" + discCoord2 + " = 0; " + discCoord2 + "MapStart + count_" + discCoord2 
                            + " * particleSeparation[" + k + "] < " + discCoord2 + "MapEnd; count_" + discCoord2 + "++) {" + NL;
                        loopStencil = loopStencil + currentIndent + IND + "for (int count_" + discCoord2 + " = 0; " + discCoord2 + "MapStart + count_" 
                            + discCoord2 + " * particleSeparation[" + k + "] < " + discCoord2 + "MapEnd; count_" + discCoord2 + "++) {" + NL;
                        endLoop = currentIndent + "}" + NL + endLoop;
                        endLoopStencil = currentIndent + IND + "}" + NL + endLoopStencil;
                        currentIndent = currentIndent + IND;
                    }
                }
                String[] gridValues = new String[4];
                gridValues[0] = minGrid[j];
                gridValues[1] = maxGrid[j];
                gridValues[2] = minValueInit[j];
                gridValues[3] = maxValueInit[j];
                loops = loops + loop + getContent(regionLimit, gridValues, j, coordinates, regionInfo, minGrid, maxGrid, gridLimit) + endLoop;
                loopsStencil = loopsStencil + loopStencil 
                    + getContentStencil(regionLimit, gridValues, j, coordinates, regionInfo, minGrid, maxGrid, gridLimit) + endLoopStencil;
            }
        	code = code + loops
        			+ IND + IND + IND + "if (mapStencil > 1) {" + NL
                    + IND + IND + IND + IND + "//Check the stencil" + NL
                    + loopsStencil
                    + IND + IND + IND + "}" + NL;
        }
        
        String preLoop = "";
        for (int j = 0; j < coordinates.size(); j++) {
            String discCoord = coordinates.get(j);
            preLoop = preLoop + IND + IND + IND + discCoord + "MapStart = d_grid_geometry->getXLower()[" + j + "] + domain_offset_factor[" + j + "] * particleSeparation[" + j + "]" 
                + " - (floor(0.5 + d_ghost_width * (dx[" + j + "]/particleSeparation[" + j + "])))*particleSeparation[" + j + "];" + NL
                + IND + IND + IND + discCoord + "MapEnd = d_grid_geometry->getXUpper()[" + j + "] + d_ghost_width * dx[" + j + "];" + NL;
        }
        return  IND + IND + IND + "//Region: " + regionName + "S" + NL  
            + preLoop + code;
    }
    
    /**
     * Maps the mathematical region surface at random position.
     * @param problem			Problem
     * @param regionInfo       The region information
     * @param gridLimit         The limits of the grid coordinates
     * @param coordinates       The coordinates
     * @param minGrid           The minimum values of the grid
     * @param maxGrid           The maximum values of the grid
     * @return                  The mapping
     * @throws CGException      CG004 External error
     */
    private static String mapMathematicalSurfaceParticlesRandom(Document problem, MappingRegionInfo regionInfo, LinkedHashMap<String, CoordinateInfo> gridLimit, 
            ArrayList<String> coordinates, String[] minGrid, String[] maxGrid) throws CGException {
        String[][] xslParams = new String [3][2];
        xslParams[0][0] = "condition";
        xslParams[0][1] = "true";
        xslParams[1][0] = "indent";
        xslParams[1][1] = "0";
        xslParams[2][0] =  "dimensions";
        xslParams[2][1] =  String.valueOf(coordinates.size());
        
        String regionName = regionInfo.getRegionName();
        String code = "";
        for (LinkedHashMap<String, CoordinateInfo> regionLimit: regionInfo.getCoordinateLimits()) {
        	String[] minValueInit = new String[regionLimit.size()];
            String[] maxValueInit = new String[regionLimit.size()];
            Iterator<String> coordIt = regionLimit.keySet().iterator();
            int i = 0;
            while (coordIt.hasNext()) {
                String coord = coordIt.next();
                minValueInit[i] = SAMRAIUtils.xmlTransform(regionLimit.get(coord).getMin(), xslParams);
                maxValueInit[i] = SAMRAIUtils.xmlTransform(regionLimit.get(coord).getMax(), xslParams);
                i++;
            }
            String content = "";
            for (int j = 0; j < coordinates.size(); j++) {
                content = content + getContentParticles(regionLimit, j, coordinates, regionInfo, minGrid, maxGrid, gridLimit);
            }
        	code = code + content;
        }
        return  "//Region: " + regionName + "S" + NL  
            + code;
    }
    
    /**
     * Creates the code to set the surface for a coordinate.
     * @param gridValues        The grid and region limits values
     * @param coordNumber       The coordinate number
     * @param coordinates       The coordinates of the problem
     * @param regionInfo        The region information
     * @param regionLimit		Region limits
     * @param gridLimit         The limits of the grid coordinates
     * @param minGrid           The minimum values of the grid
     * @param maxGrid           The maximum values of the grid
     * @return                  The code
     * @throws CGException      CG004 External error
     */
    private static String getContent(LinkedHashMap<String, CoordinateInfo> regionLimit, String[] gridValues, int coordNumber, ArrayList<String> coordinates, MappingRegionInfo regionInfo, 
            String[] minGrid, String[] maxGrid, LinkedHashMap<String, CoordinateInfo> gridLimit) throws CGException {
        String content;
        String[][] xslParams = new String [3][2];
        xslParams[0][0] = "condition";
        xslParams[0][1] = "true";
        xslParams[1][0] = "indent";
        xslParams[1][1] = "1";
        xslParams[2][0] =  "dimensions";
        xslParams[2][1] =  String.valueOf(coordinates.size());
        String temporal = "";
        String maxGridValue = gridValues[1];
        String minSegValue = gridValues[2];
        String maxSegValue = gridValues[3];
        String indent = IND + IND + IND;
        for (int i = 0; i < coordinates.size() - 1; i++) {
            indent = indent + IND;
        }
        Iterator<String> coordIt = regionLimit.keySet().iterator();
        for (int i = 0; i < coordinates.size(); i++) {
            String regionCoord = coordIt.next();
            String coord = coordinates.get(i);
            if (i != coordNumber) {
                //Temporal calculation for maxGrid comparison. Particle could not be at the max value but their immediate inferior place.
                String minValue = SAMRAIUtils.xmlTransform(regionLimit.get(regionCoord).getMin(), xslParams);
                if (CodeGeneratorUtils.doubleable(maxGrid[i]) && CodeGeneratorUtils.doubleable(minValue) 
                        && Double.compare(Double.valueOf(maxGrid[i]), Double.valueOf(minValue)) == 0) {
                    temporal = temporal + indent + IND + "tmp" + coord + " = " + coord + "MapStart + (floor(d_grid_geometry->getXUpper()[" + i 
                        + "]/particleSeparation[" + i + "] - d_grid_geometry->getXLower()[" + i + "]/particleSeparation[" + i + "]" 
                        + " + 2 * d_ghost_width * (dx[" + i + "]/particleSeparation[" + i + "])) - 1) * particleSeparation[" + i + "]" 
                        + ";" + NL
                        + indent + IND + "if (Equals(tmp" + coord + ", " + maxGrid[i] + " + d_ghost_width * dx[" + i + "])) {" + NL
                        + indent + IND + IND + "tmp" + coord + " = tmp" + coord + " - particleSeparation[" + i + "];" + NL
                        + indent + IND + "}" + NL;
                }
            }
        }
        
        content = indent + "if (lessEq(patch_geom->getXLower()[" + coordNumber + "], " 
            + minSegValue + ") && greaterEq(patch_geom->getXUpper()[" + coordNumber + "], " + minSegValue + ")) {" + NL
            + getPositionsMin(gridValues, coordNumber, coordinates, regionInfo,  minGrid)
            + temporal
            + CodeGeneratorUtils.incrementIndent(getContentParticles(regionLimit, coordNumber, coordinates, regionInfo, minGrid, maxGrid, gridLimit), indent.length() + 1)
            + indent + "}" + NL;
        //If min and max value are equals the second part is the same as the first one
        if (!(minSegValue.equals(maxSegValue) || (CodeGeneratorUtils.doubleable(maxSegValue) && CodeGeneratorUtils.doubleable(minSegValue)
                && Double.compare(Double.valueOf(maxSegValue), Double.valueOf(minSegValue)) == 0))) {
            //Staggering
            if (coordNumber == coordinates.size() - 1) {
                for (int k = coordinates.size() - 1; k >= 0; k--) {
                    if (k != coordNumber) {
                        indent = indent.substring(1);
                        content = content + indent + "}" + NL;
                    }
                }
                //Check last particle, must be under the limit, not equal.
                String coord = coordinates.get(coordinates.size() - 1);
                if (CodeGeneratorUtils.doubleable(maxGridValue) && CodeGeneratorUtils.doubleable(maxSegValue) 
                        && Double.compare(Double.valueOf(maxGridValue), Double.valueOf(maxSegValue)) == 0) {
                    content = content + indent + "tmp = (int)(floor(d_grid_geometry->getXUpper()[" + (coordinates.size() - 1)
                        + "]/particleSeparation[" + (coordinates.size() - 1) + "] - d_grid_geometry->getXLower()[" + (coordinates.size() - 1) 
                        + "]/particleSeparation[" + (coordinates.size() - 1) + "] + 2 * d_ghost_width * (dx[" + (coordinates.size() - 1) 
                        + "]/particleSeparation[" + (coordinates.size() - 1) + "]))) - 1;" + NL
                        + indent + "position[" + (coordinates.size() - 1) + "] = " + coord + "MapStart + tmp * particleSeparation[" + (coordinates.size() - 1) + "];" + NL
                        + indent + "if (Equals(position[" + (coordinates.size() - 1) + "], " 
                        + SAMRAIUtils.xmlTransform(gridLimit.get(coordinates.get(coordinates.size() - 1)).getMax(), xslParams) 
                        + " + d_ghost_width * dx[" + (coordinates.size() - 1) + "])) {" + NL
                        + indent + IND + "tmp = tmp - 1;" + NL
                        + indent + "}" + NL;
                }
                for (int k = coordinates.size() - 1; k >= 0; k--) {
                    String discCoord2 = coordinates.get(k);
                    if (k != coordNumber) {
                        content = content + indent + "for (int count_" + discCoord2 + " = 0; " + discCoord2 + "MapStart + count_" + discCoord2 
                            + " * particleSeparation[" + k + "] < " + discCoord2 + "MapEnd; count_" + discCoord2 + "++) {" + NL;
                        indent = indent + IND;
                    }
                }
            }
            content = content 
                + indent + "if (lessEq(patch_geom->getXLower()[" + coordNumber + "], " 
                + maxSegValue + ") && greaterEq(patch_geom->getXUpper()[" + coordNumber + "], " + maxSegValue + ")) {" + NL
                + getPositionsMax(gridValues, coordNumber, coordinates, regionInfo,  minGrid)
                + temporal
                + CodeGeneratorUtils.incrementIndent(getContentParticles(regionLimit, coordNumber, coordinates, regionInfo, minGrid, maxGrid, gridLimit), indent.length() + 1)
                + indent + "}" + NL;
        }
        return content;
    }
    
    /**
     * Creates the code to set the surface for a coordinate.
     * @param coordNumber       The coordinate number
     * @param coordinates       The coordinates of the problem
     * @param regionLimit		Region limits
     * @param regionInfo        The region information
     * @param gridLimit         The limits of the grid coordinates
     * @param minGrid           The minimum values of the grid
     * @param maxGrid           The maximum values of the grid
     * @return                  The code
     * @throws CGException      CG004 External error
     */
    private static String getContentParticles(LinkedHashMap<String, CoordinateInfo> regionLimit, int coordNumber, ArrayList<String> coordinates, MappingRegionInfo regionInfo, 
            String[] minGrid, String[] maxGrid, LinkedHashMap<String, CoordinateInfo> gridLimit) throws CGException {
        String[][] xslParams = new String [3][2];
        xslParams[0][0] = "condition";
        xslParams[0][1] = "true";
        xslParams[1][0] = "indent";
        xslParams[1][1] = "1";
        xslParams[2][0] =  "dimensions";
        xslParams[2][1] =  String.valueOf(coordinates.size());
        int regionId = regionInfo.getId();
        String index = "";
        String limitCondition = "";
        String indent = IND + IND + IND;
        for (int i = 0; i < coordinates.size() - 1; i++) {
            indent = indent + IND;
        }
        for (int i = 0; i < coordinates.size(); i++) {
            String coord = coordinates.get(i);
            index = index + coord + ", ";

            limitCondition = limitCondition + coord + " >= boxfirst1(" + i + ") && " + coord + " <= boxlast1(" + i + ") && ";
        }
        index = index.substring(0, index.lastIndexOf(", "));
        limitCondition = limitCondition.substring(0, limitCondition.lastIndexOf(" &&"));
        String regionCondition = getRegionCondition(regionLimit, coordNumber, coordinates, regionInfo, minGrid, maxGrid, gridLimit);
        
        return "if (" + limitCondition + regionCondition + ") {" + NL
            + IND + "hier::Index idx(" + index + ");" + NL
            + IND + "Particles<T>* part = particleVariables->getItem(idx);" + NL
            + IND + "T* particle = new T(0, position, " + regionId 
            + ");" + NL
            + IND + "T* oldParticle = part->overlaps(*particle);" + NL
            + IND + "if (oldParticle != NULL) {" + NL
            + IND + IND + "part->deleteParticle(*oldParticle);" + NL
            + IND + "}" + NL
            + IND + "part->addParticle(*particle);" + NL
            + IND + "delete particle;" + NL
            + "}" + NL;
    }
    
    /**
     * Gets the minimum positions for the mathematical surfaces.
     * @param gridValues        The grid and region limits values
     * @param coordNumber       The coordinate number
     * @param coordinates       The coordinates of the problem
     * @param regionInfo        The region information
     * @param minGrid           The minimum values of the grid
     * @return                  The code
     * @throws CGException      CG004 External error
     */
    private static String getPositionsMin(String[] gridValues, int coordNumber, ArrayList<String> coordinates,
            MappingRegionInfo regionInfo, String[] minGrid) throws CGException {
        String positionsMin = "";
        String index = "";
        String minGridValue = gridValues[0];
        String maxGridValue = gridValues[1];
        String minSegValue = gridValues[2];
        String indent = IND + IND;
        for (int i = 0; i < coordinates.size() - 1; i++) {
            indent = indent + IND;
        }
        for (int i = 0; i < coordinates.size(); i++) {
            String coord = coordinates.get(i);
            index = index + coord + ", ";
            if (i == coordNumber) {
                if (CodeGeneratorUtils.doubleable(minGridValue) && CodeGeneratorUtils.doubleable(minSegValue) 
                        && Double.compare(Double.valueOf(minGridValue), Double.valueOf(minSegValue)) == 0) {
                    positionsMin = positionsMin + indent + IND + "position[" + i + "] = " + coord + "MapStart;" + NL;
                }
                else {
                    if (CodeGeneratorUtils.doubleable(maxGridValue) && CodeGeneratorUtils.doubleable(minSegValue) 
                            && Double.compare(Double.valueOf(maxGridValue), Double.valueOf(minSegValue)) == 0) {
                        positionsMin = positionsMin + indent + IND + "position[" + i + "] = " + coord + "MapStart + (floor(" 
                            + "d_grid_geometry->getXUpper()[" + i + "]/particleSeparation[" + i + "] - d_grid_geometry->getXLower()[" 
                            + i + "]/particleSeparation[" + i + "]" + " + 2 * d_ghost_width * (dx[" + i 
                            + "]/particleSeparation[" + i + "])) - 1) * particleSeparation[" + i + "];" + NL
                            + indent + IND + "if (Equals(position[" + i + "], " + maxGridValue + " + d_ghost_width * dx[" + i + "])) {" + NL
                            + indent + IND + IND + "position[" + i + "] = position[" + i + "] - particleSeparation[" + i + "];" + NL
                            + indent + IND + "}" + NL;
                    }
                    else {
                        if (i < coordinates.size() - 1) {
                            positionsMin = positionsMin + indent + IND + "position[" + i + "] = " + coord + "MapStart + (floor((" + minSegValue + " - " + minGrid[i] 
                                + ")/ particleSeparation[" + i + "]) + floor(0.5 + d_ghost_width*(dx[" + i 
                                + "]/particleSeparation[" + i + "]))) * particleSeparation[" + i + "];" + NL; 
                        }
                        else {
                            positionsMin = positionsMin + indent + IND + "position[" + i + "] = " + coord + "MapStart + (floor((" + minSegValue 
                                + " - " + minGrid[i] + ")/ particleSeparation[" + i + "]) + floor(0.5 + d_ghost_width*(dx[" 
                                + i + "]/particleSeparation[" + i + "]))) * particleSeparation[" + i + "];" + NL; 
                        }
                    }
                }
            }
            else {
                positionsMin = positionsMin + IND + indent + "position[" + i + "] = " + coord + "MapStart + count_" + coord 
                    + " * particleSeparation[" + i + "];" + NL;
            }
            positionsMin = positionsMin 
                + indent + IND + coord + " = floor((position[" + i + "] - d_grid_geometry->getXLower()[" + i + "])/dx[" + i + "] + 1E-10);" + NL;
        }
        return positionsMin;
    }
    /**
     * Gets the minimum positions for the mathematical surfaces.
     * @param gridValues        The grid and region limits values
     * @param coordNumber       The coordinate number
     * @param coordinates       The coordinates of the problem
     * @param regionInfo        The region information
     * @param minGrid           The minimum values of the grid
     * @return                  The code
     * @throws CGException      CG004 External error
     */
    private static String getPositionsMax(String[] gridValues, int coordNumber, ArrayList<String> coordinates,
            MappingRegionInfo regionInfo, String[] minGrid) throws CGException {
        String[][] xslParams = new String [3][2];
        xslParams[0][0] = "condition";
        xslParams[0][1] = "true";
        xslParams[1][0] = "indent";
        xslParams[1][1] = "0";
        xslParams[2][0] =  "dimensions";
        xslParams[2][1] =  String.valueOf(coordinates.size());
        String index = "";
        String positionsMax = "";
        String minGridValue = gridValues[0];
        String maxGridValue = gridValues[1];
        String maxSegValue = gridValues[3];
        String indent = IND + IND;
        for (int i = 0; i < coordinates.size() - 1; i++) {
            indent = indent + IND;
        }
        for (int i = 0; i < coordinates.size(); i++) {
            String coord = coordinates.get(i);
            index = index + coord + ", ";
            if (i == coordNumber) {
                if (CodeGeneratorUtils.doubleable(maxGridValue) && CodeGeneratorUtils.doubleable(maxSegValue) 
                        && Double.compare(Double.valueOf(maxGridValue), Double.valueOf(maxSegValue)) == 0) {
                    positionsMax = positionsMax + indent + IND + "position[" + i + "] = " + coord + "MapStart + (floor(" 
                        + "d_grid_geometry->getXUpper()[" + i + "]/particleSeparation[" + i + "] - d_grid_geometry->getXLower()[" 
                        + i + "]/particleSeparation[" + i + "] + 2 * d_ghost_width * (dx[" + i 
                        + "]/particleSeparation[" + i + "])) - 1) * particleSeparation[" + i + "];" + NL
                        + indent + IND + "if (Equals(position[" + i + "], " + maxGridValue + " + d_ghost_width * dx[" + i + "])) {" + NL
                        + indent + IND + IND + "position[" + i + "] = position[" + i + "] - particleSeparation[" + i + "];" + NL
                        + indent + IND + "}" + NL;
                }
                else {
                    if (CodeGeneratorUtils.doubleable(minGridValue) && CodeGeneratorUtils.doubleable(maxSegValue) 
                            && Double.compare(Double.valueOf(minGridValue), Double.valueOf(maxSegValue)) == 0) {
                        positionsMax = positionsMax + indent + IND + "position[" + i + "] = " + coord + "MapStart;" + NL;
                    }
                    else {
                        if (i < coordinates.size() - 1) {
                            positionsMax = positionsMax + indent + IND + "position[" + i + "] = " + coord + "MapStart + (floor((" + maxSegValue + " - " + minGrid[i] 
                                + ")/ particleSeparation[" + i + "]) + floor(0.5 + d_ghost_width*(dx[" + i 
                                + "]/particleSeparation[" + i + "]))) * particleSeparation[" + i + "];" + NL;
                        }
                        else {
                            positionsMax = positionsMax + indent + IND + "position[" + i + "] = " + coord + "MapStart + (floor((" + maxSegValue 
                                + " - " + minGrid[i] + ")/ particleSeparation[" + i + "]) + floor(0.5 + d_ghost_width*(dx[" 
                                + i + "]/particleSeparation[" + i + "]))) * particleSeparation[" + i + "];" + NL;
                        }
                    }
                }
            }
            else {
                positionsMax = positionsMax + IND + indent + "position[" + i + "] = " + coord + "MapStart + count_" + coord 
                    + " * particleSeparation[" + i + "];" + NL;
            }
            positionsMax = positionsMax 
                + indent + IND + coord + " = floor((position[" + i + "] - d_grid_geometry->getXLower()[" + i + "])/dx[" + i + "] + 1E-10);" + NL;
        }
        return positionsMax;
    }
    
    /**
     * Creates the code to check the stencil of the surface for a coordinate.
     * @param gridValues        The grid and region limits values
     * @param coordNumber       The coordinate number
     * @param coordinates       The coordinates of the problem
     * @param regionInfo       	The region information
     * @param gridLimit         The limits of the grid coordinates
     * @param minGrid           The minimum values of the grid
     * @param maxGrid           The maximum values of the grid
     * @param regionLimit		Region limits
     * @return                  The code
     * @throws CGException      CG004 External error
     */
    private static String getContentStencil(LinkedHashMap<String, CoordinateInfo> regionLimit, String[] gridValues, int coordNumber, ArrayList<String> coordinates, MappingRegionInfo regionInfo, 
            String[] minGrid, String[] maxGrid, LinkedHashMap<String, CoordinateInfo> gridLimit) throws CGException {
        String content = "";
        String[][] xslParams = new String [3][2];
        xslParams[0][0] = "condition";
        xslParams[0][1] = "true";
        xslParams[1][0] = "indent";
        xslParams[1][1] = "1";
        xslParams[2][0] =  "dimensions";
        xslParams[2][1] =  String.valueOf(coordinates.size());
        String positions = "";
        String temporalMin = "";
        String temporalMax = "";
        String temporal = "";
        String insideCheck = "";
        String minGridValue = gridValues[0];
        String maxGridValue = gridValues[1];
        String minSegValue = gridValues[2];
        String maxSegValue = gridValues[3];
        String indent = IND + IND + IND + IND;
        for (int i = 0; i < coordinates.size() - 1; i++) {
            indent = indent + IND;
        }
        int regionId = regionInfo.getId();
        Iterator<String> coordIt = regionLimit.keySet().iterator();
        for (int i = 0; i < coordinates.size(); i++) {
            String regionCoord = coordIt.next();
            String coord = coordinates.get(i);
            if (i == coordNumber) {
                if (CodeGeneratorUtils.doubleable(maxGridValue) && CodeGeneratorUtils.doubleable(minSegValue) 
                        && Double.compare(Double.valueOf(maxGridValue), Double.valueOf(minSegValue)) == 0) {
                    temporalMin = getTmpLastVariable(coord, maxGridValue, i, indent);
                }
                if (CodeGeneratorUtils.doubleable(maxGridValue) && CodeGeneratorUtils.doubleable(maxSegValue) 
                        && Double.compare(Double.valueOf(maxGridValue), Double.valueOf(maxSegValue)) == 0) {
                    temporalMax = getTmpLastVariable(coord, maxGridValue, i, indent);
                }
            }
            else {
                insideCheck = insideCheck + "floor((" + coord + "MapStart + count_" + coord + " * particleSeparation[" + i + "]" 
                    + " - d_grid_geometry->getXLower()[" + i + "])/dx[" + i + "] + 1E-10) >= boxfirst(" + i + ") && floor((" + coord + "MapStart + count_" 
                    + coord + " * particleSeparation[" + i + "] - d_grid_geometry->getXLower()[" + i + "])/dx[" + i + "] + 1E-10) <= boxlast(" + i 
                    + ") && ";
                positions = positions + indent + IND + "position[" + i + "] = " + coord + "MapStart + count_" + coord 
                    + " * particleSeparation[" + i + "];" + NL;

                //Temporal calculation for maxGrid comparison. Particle could not be at the max value but their immediate inferior place.
                String minValue = SAMRAIUtils.xmlTransform(regionLimit.get(regionCoord).getMin(), xslParams);
                if (CodeGeneratorUtils.doubleable(maxGrid[i]) && CodeGeneratorUtils.doubleable(minValue) 
                        && Double.compare(Double.valueOf(maxGrid[i]), Double.valueOf(minValue)) == 0) {
                    temporal = temporal + indent + IND + "tmp" + coord + " = " + coord + "MapStart + (floor(d_grid_geometry->getXUpper()[" + i 
                        + "]/particleSeparation[" + i + "] - d_grid_geometry->getXLower()[" + i + "]/particleSeparation[" + i + "]" 
                        + " + 2 * d_ghost_width * (dx[" + i + "]/particleSeparation[" + i + "])) - 1) * particleSeparation[" + i + "];" + NL
                        + indent + IND + "if (Equals(tmp" + coord + ", " + maxGrid[i] + " + d_ghost_width * dx[" + i + "])) {" + NL
                        + indent + IND + IND + "tmp" + coord + " = tmp" + coord + " - particleSeparation[" + i + "];" + NL
                        + indent + IND + "}" + NL;
                }
            }
        }
        insideCheck = insideCheck.substring(0, insideCheck.lastIndexOf(" &&"));
        
        String index1Stencil = getLimitIndex(getPositionsMinStencil(gridValues, coordNumber, coordinates, regionInfo, minGrid), 
                coordNumber, coordinates, true, false, true, false);
        String index3Stencil = getLimitIndex(getPositionsMaxStencil(gridValues, coordNumber, coordinates, regionInfo, minGrid), 
                coordNumber, coordinates, false, false, true, false);
        
        String regionCondition = getRegionCondition(regionLimit, coordNumber, coordinates, regionInfo, minGrid, maxGrid, gridLimit);
        String stagCheck = "";
        
        content = indent + "if (" + insideCheck + ") {" + NL
            + positions;
        
        //If min and max value are equals the second part is the same as the first one
        if (!((minSegValue.equals(maxSegValue) && minSegValue.equals(maxGridValue)) || (CodeGeneratorUtils.doubleable(maxGridValue) 
                && CodeGeneratorUtils.doubleable(maxSegValue) && CodeGeneratorUtils.doubleable(minSegValue)
                && Double.compare(Double.valueOf(maxSegValue), Double.valueOf(minSegValue)) == 0)
                && Double.compare(Double.valueOf(maxGridValue), Double.valueOf(minSegValue)) == 0)) {
            if (coordNumber < coordinates.size() - 1 && !minGridValue.equals(minSegValue)) {
                stagCheck = indent + IND + IND + IND + "checkStencil<T>(patch, " + index1Stencil + ", " + regionId + ");" + NL;
            }
            else {
                stagCheck = temporalMin + indent + IND + IND + "checkStencil<T>(patch, " + index1Stencil + ", " + regionId + ");" + NL;
            }
            
            content = content + temporal + indent + IND + "if (lessEq(patch_geom->getXLower()[" + coordNumber + "], " 
                + minSegValue + ") && greaterEq(patch_geom->getXUpper()[" + coordNumber + "], " + minSegValue + ")" + regionCondition + ") {" + NL
                + stagCheck
                + indent + IND + "}" + NL;
        }
        //If min and max value are equals the second part is the same as the first one
        if (!((minSegValue.equals(maxSegValue) && maxSegValue.equals(minGridValue)) || (CodeGeneratorUtils.doubleable(minGridValue) 
                && CodeGeneratorUtils.doubleable(maxSegValue) && CodeGeneratorUtils.doubleable(minSegValue)
                && Double.compare(Double.valueOf(maxSegValue), Double.valueOf(minSegValue)) == 0)
                && Double.compare(Double.valueOf(minGridValue), Double.valueOf(maxSegValue)) == 0)) {
            //Staggering
            if (coordNumber == coordinates.size() - 1) {
                content = content 
                        + getStaggeringControlStencil(gridValues, coordinates, regionInfo, gridLimit, insideCheck, positions);
            }
            if (coordNumber < coordinates.size() - 1 && !maxGridValue.equals(maxSegValue)) {
                stagCheck = indent + IND + IND + "checkStencil<T>(patch, " + index3Stencil + ", " + regionId + ");" + NL;
            }
            else {
                stagCheck = temporalMax + indent + IND + IND + "checkStencil<T>(patch, " + index3Stencil + ", " + regionId + ");" + NL;
            }
            content = content
                + indent + IND + "if (lessEq(patch_geom->getXLower()[" + coordNumber + "], " 
                + maxSegValue + ") && greaterEq(patch_geom->getXUpper()[" + coordNumber + "], " + maxSegValue + ")" + regionCondition + ") {" + NL
                + stagCheck
                + indent + IND + "}" + NL;
        }
        content = content + indent + "}" + NL;
        
        return content;
    }
    
    /**
     * Creates the staggering control if necessary.
     * @param gridValues        The grid and region limits values
     * @param coordinates       The coordinates of the problem
     * @param regionInfo       The region information
     * @param gridLimit         The limits of the grid coordinates
     * @param staggVar          The staggering variable
     * @param insideCheck       Code that checks if the particle is inside
     * @param positions         Code with the positions of the particles
     * @return                  The code
     * @throws CGException      CG004 External error
     */
    private static String getStaggeringControlStencil(String[] gridValues, ArrayList<String> coordinates, MappingRegionInfo regionInfo, 
            LinkedHashMap<String, CoordinateInfo> gridLimit, String insideCheck, String positions) 
        throws CGException {
        String[][] xslParams = new String [3][2];
        xslParams[0][0] = "condition";
        xslParams[0][1] = "true";
        xslParams[1][0] = "indent";
        xslParams[1][1] = "0";
        xslParams[2][0] =  "dimensions";
        xslParams[2][1] =  String.valueOf(coordinates.size());
        String maxGridValue = gridValues[1];
        String maxSegValue = gridValues[3];
        String indent = IND + IND + IND;
        for (int i = 0; i < coordinates.size() - 1; i++) {
            indent = indent + IND;
        }
        String content = "";
        for (int k = coordinates.size() - 1; k >= 0; k--) {
            if (k != coordinates.size() - 1) {
                indent = indent.substring(1);
                content = content + indent + IND + "}" + NL;
            }
        }
        content = content + indent + "}" + NL;
        //Check last particle, must be under the limit, not equal.
        String coord = coordinates.get(coordinates.size() - 1);
        if (CodeGeneratorUtils.doubleable(maxGridValue) && CodeGeneratorUtils.doubleable(maxSegValue) 
                && Double.compare(Double.valueOf(maxGridValue), Double.valueOf(maxSegValue)) == 0) {
            content = content + indent + "tmp = (int)(floor(d_grid_geometry->getXUpper()[" + (coordinates.size() - 1) + "]/particleSeparation[" + (coordinates.size() - 1) + "]" 
                + " - d_grid_geometry->getXLower()[" + (coordinates.size() - 1) + "]/particleSeparation[" + (coordinates.size() - 1) + "]" 
                + " + 2 * d_ghost_width * (dx[" + (coordinates.size() - 1) + "]/particleSeparation[" + (coordinates.size() - 1) + "]))) - 1;" + NL
                + indent + "position[" + (coordinates.size() - 1) + "] = " + coord + "MapStart + tmp * particleSeparation[" + (coordinates.size() - 1) + "];" + NL
                + indent + "if (Equals(position[" + (coordinates.size() - 1) + "], " 
                + SAMRAIUtils.xmlTransform(gridLimit.get(coordinates.get(coordinates.size() - 1)).getMax(), xslParams) 
                + " + d_ghost_width * dx[" + (coordinates.size() - 1) + "])) {" + NL
                + indent + IND + "tmp = tmp - 1;" + NL
                + indent + "}" + NL;
        }
        for (int k = coordinates.size() - 1; k >= 0; k--) {
            String discCoord2 = coordinates.get(k);
            if (k != coordinates.size() - 1) {
                content = content + indent + "for (int count_" + discCoord2 + " = 0; " + discCoord2 + "MapStart + count_" 
                    + discCoord2 + " * particleSeparation[" + k + "] < " + discCoord2 + "MapEnd; count_" + discCoord2 + "++) {" + NL;
                indent = indent + IND;
            }
        }
        return content + indent + "if (" + insideCheck + ") {" + NL
            + positions;
    }
    /**
     * Gets the temporal variable for the last particle problem.
     * @param coord             The discrete spatial coordinates
     * @param maxGridValue      The max grid value
     * @param i                 The index of the actual coordinate
     * @param indent            The actual indentation  
     * @return                  The code
     */
    private static String getTmpLastVariable(String coord, String maxGridValue, int i, String indent) {
        return indent + IND + IND + "tmp = (int)(floor(d_grid_geometry->getXUpper()[" + i + "]/particleSeparation[" + i  + "]" 
            + " - d_grid_geometry->getXLower()[" + i + "]/particleSeparation[" + i  + "]" + " + 2 * d_ghost_width * (dx[" + i  
            + "]/particleSeparation[" + i  + "]))) - 1;" + NL
            + indent + IND + IND + "position[" + i  + "] = " + coord + "MapStart + tmp * particleSeparation[" + i  + "];" + NL
            + indent + IND + IND + "if (Equals(position[" + i  + "], " + maxGridValue + " + d_ghost_width * dx[" + i  + "])) {" + NL
            + indent + IND + IND + IND + "tmp = tmp - 1;" + NL
            + indent + IND + IND + "}" + NL;
    }
    
    /**
     * Gets the minimum position for the get content stencil routine of mathematical surface.
     * @param gridValues        The grid and region limits values
     * @param coordNumber       The coordinate number
     * @param coordinates       The coordinates of the problem
     * @param regionInfo        The region information
     * @param minGrid           The minimum values of the grid
     * @return                  The code
     * @throws CGException      CG004 External error
     */
    private static String getPositionsMinStencil(String[] gridValues, int coordNumber, ArrayList<String> coordinates,
            MappingRegionInfo regionInfo, String[] minGrid) throws CGException {
        String positionsMin = "";
        String minGridValue = gridValues[0];
        String maxGridValue = gridValues[1];
        String minSegValue = gridValues[2];
        String indent = IND + IND + IND;
        for (int i = 0; i < coordinates.size() - 1; i++) {
            indent = indent + IND;
        }
        for (int i = 0; i < coordinates.size(); i++) {
            if (i == coordNumber) {
                if (CodeGeneratorUtils.doubleable(minGridValue) && CodeGeneratorUtils.doubleable(minSegValue) 
                        && Double.compare(Double.valueOf(minGridValue), Double.valueOf(minSegValue)) == 0) {
                    positionsMin = "0";
                }
                else {
                    if (CodeGeneratorUtils.doubleable(maxGridValue) && CodeGeneratorUtils.doubleable(minSegValue) 
                            && Double.compare(Double.valueOf(maxGridValue), Double.valueOf(minSegValue)) == 0) {
                        positionsMin = "tmp";
                    }
                    else {
                        positionsMin = "floor((" + minSegValue + " - " + minGrid[i] + ")/ particleSeparation[" + i + "]) + " 
                            + "floor(0.5 + d_ghost_width*(dx[" + i + "]/particleSeparation[" + i + "]))";
                    }
                }
            }
        }
        return positionsMin;
    }
    /**
     * Gets the maximum position for the get content stencil routine of mathematical surface.
     * @param gridValues        The grid and region limits values
     * @param coordNumber       The coordinate number
     * @param coordinates       The coordinates of the problem
     * @param regionInfo        The region information
     * @param minGrid           The minimum values of the grid
     * @return                  The code
     * @throws CGException      CG004 External error
     */
    private static String getPositionsMaxStencil(String[] gridValues, int coordNumber, ArrayList<String> coordinates,
            MappingRegionInfo regionInfo, String[] minGrid) throws CGException {
        String[][] xslParams = new String [3][2];
        xslParams[0][0] = "condition";
        xslParams[0][1] = "true";
        xslParams[1][0] = "indent";
        xslParams[1][1] = "0";
        xslParams[2][0] =  "dimensions";
        xslParams[2][1] =  String.valueOf(coordinates.size());
        String positionsMax = "";
        String minGridValue = gridValues[0];
        String maxGridValue = gridValues[1];
        String minSegValue = gridValues[2];
        String maxSegValue = gridValues[3];
        String indent = IND + IND + IND;
        for (int i = 0; i < coordinates.size() - 1; i++) {
            indent = indent + IND;
        }
        for (int i = 0; i < coordinates.size(); i++) {
            if (i == coordNumber) {
                if (CodeGeneratorUtils.doubleable(maxGridValue) && CodeGeneratorUtils.doubleable(maxSegValue) 
                        && Double.compare(Double.valueOf(maxGridValue), Double.valueOf(maxSegValue)) == 0) {
                    positionsMax = "tmp";
                }
                else {
                    if (CodeGeneratorUtils.doubleable(minGridValue) && CodeGeneratorUtils.doubleable(minSegValue) 
                            && Double.compare(Double.valueOf(minGridValue), Double.valueOf(minSegValue)) == 0) {
                        positionsMax = "0";
                    }
                    else {
                        positionsMax = "floor((" + maxSegValue + " - " + minGrid[i] + ")/ particleSeparation[" + i + "]) + " 
                            + "floor(0.5 + d_ghost_width*(dx[" + i + "]/particleSeparation[" + i + "]))"; 
                    }
                }
            }
        }
        return positionsMax;
    }
    /**
     * Creates the index for the coordinates at the limits of the mathematical region.
     * @param variable          The variable of the limit
     * @param coordNumber       The coordinate to generate the limits for
     * @param coordinates       The coordinates
     * @param lower             If the limit checked is the lower one
     * @param coord             If the variable depends on the coordinate
     * @param stencil           If the index is for the stencil
     * @param cell              If the index is for the cells or particles
     * @return                  The index generated
     */
    private static String getLimitIndex(String variable, int coordNumber, ArrayList<String> coordinates, boolean lower, boolean coord, 
            boolean stencil, boolean cell) {
        String index = "";
        for (int k = 0; k < coordinates.size(); k++) {
            if (coordNumber == k) {
                if (coord) {
                    index = index + coordinates.get(k) + variable + ", ";
                }
                else {
                    index = index + variable + ", ";
                }
                if (stencil) {
                    if (lower) {
                        index = index + "1, ";
                    }
                    else {
                        index = index + "2, ";
                    }
                }
            }
            else {
                if (cell) {
                    index = index + coordinates.get(k) + ", ";
                }
                else {
                    index = index + "count_" + coordinates.get(k) + ", ";
                }
                if (stencil) {
                    index = index + "1, ";
                }
            }
        }
        index = index.substring(0, index.lastIndexOf(","));
        return index;
    }
    
    /**
     * Generates the code that checks if the position is inside the region.
     * @param coordNumber       The coordinate number excluded from the condition (-1 if no coordinate have to be excluded)
     * @param coordinates       The coordinates of the problem
     * @param regionLimit		Region limits
     * @param regionInfo        The region information
     * @param gridLimit         The limits of the grid coordinates
     * @param minGrid           The minimum values of the grid
     * @param maxGrid           The maximum values of the grid
     * @return                  The code
     * @throws CGException      CG004 External error
     */
    private static String getRegionCondition(LinkedHashMap<String, CoordinateInfo> regionLimit, int coordNumber, ArrayList<String> coordinates, MappingRegionInfo regionInfo, String[] minGrid, 
            String[] maxGrid, LinkedHashMap<String, CoordinateInfo> gridLimit) throws CGException {
        String[][] xslParams = new String [3][2];
        xslParams[0][0] = "condition";
        xslParams[0][1] = "true";
        xslParams[1][0] = "indent";
        xslParams[1][1] = "0";
        xslParams[2][0] =  "dimensions";
        xslParams[2][1] =  String.valueOf(coordinates.size());
        
        String regionCondition = "";
        Iterator<String> coordIt = regionLimit.keySet().iterator();
        int i = 0;
        while (coordIt.hasNext()) {
            String coord = coordIt.next();
            String minValue = SAMRAIUtils.xmlTransform(regionLimit.get(coord).getMin(), xslParams);
            String maxValue = SAMRAIUtils.xmlTransform(regionLimit.get(coord).getMax(), xslParams);
            String discCoord = coordinates.get(i);
            if (i != coordNumber) {
             	 if (!minGrid[i].equals(minValue)) {
	                 if (maxGrid[i].equals(minValue)) {
	                     regionCondition = regionCondition + "greaterEq(position[" + i + "], tmp" + discCoord + ") && ";
	                 }
	                 else {
	                     regionCondition = regionCondition + "greaterEq(position[" + i + "], " + minValue + ") && ";
	                 }
             	 }
             	 if (!maxGrid[i].equals(maxValue)) {
	                 if (minGrid[i].equals(maxValue)) {
	                     regionCondition = regionCondition + "lessEq(position[" + i + "], " + discCoord + "MapStart) && ";
	                 }
	                 else {
	                     regionCondition = regionCondition + "lessEq(position[" + i + "], " + maxValue + ") && ";
	                 }
             	 }
            }
            i++;
        }
        if (!regionCondition.equals("")) {
            regionCondition = " && " + regionCondition.substring(0, regionCondition.lastIndexOf(" &&"));
        }
        return regionCondition;
    }
    
    /**
     * Generates the code to fill the faces of the region.
     * It uses geometry formulas to recognize when a point is inside the face.
     * To do this a local coordinate system for the face is used. 
     * The local coordinate points are transformed to the global coordinate system.
     * See:  Finite Element Methods. Formulation for MoL and mesh refinement.
     *                      A. Arbona
     *        IAC 3 -UIB, 07120, Palma de Mallorca
     * @param regionInfo        The region information
     * @param coordinates       The coordinates
     * @param gridLimit         The limits of the grid coordinates
     * @param stencilCheck      If the algorithm has to check the stencil or fill the faces
     * @return                  The generated code
     * @throws CGException      CG004 External error
     */
    private static String faceFillingParticles(MappingRegionInfo regionInfo, ArrayList<String> coordinates, LinkedHashMap<String, CoordinateInfo> gridLimit, 
            boolean stencilCheck) throws CGException {
        String[][] xslParams = new String [3][2];
        xslParams[0][0] = "condition";
        xslParams[0][1] = "true";
        xslParams[1][0] = "indent";
        xslParams[1][1] = "0";
        xslParams[2][0] =  "dimensions";
        xslParams[2][1] =  String.valueOf(coordinates.size());
        String regionName = regionInfo.getRegionName();
        String currentIndent = IND + IND + IND;
        String maxMin = "";
        String indexComparator = "";
        for (int i = 0; i < coordinates.size(); i++) {
            //Index for the initial assignment of max and min coordinates for face filling process
            maxMin = maxMin + currentIndent + IND + IND + IND + "minBlock[" + i + "] = " + coordinates.get(i) + "term;" + NL
                + currentIndent + IND + IND + IND + "maxBlock[" + i + "] = " + coordinates.get(i) + "term;" + NL;
            //Comparations of the index
            indexComparator = indexComparator + currentIndent + IND + IND + IND + "if (" + coordinates.get(i) 
                + "term < minBlock[" + i + "]) {" + NL
                + currentIndent + IND + IND + IND + IND + "minBlock[" + i + "] = " + coordinates.get(i) + "term;" + NL
                + currentIndent + IND + IND + IND + "}" + NL
                + currentIndent + IND + IND + IND + "if (" + coordinates.get(i) + "term > maxBlock[" + i + "]) {" + NL
                + currentIndent + IND + IND + IND + IND + "maxBlock[" + i + "] = " + coordinates.get(i) + "term;" + NL
                + currentIndent + IND + IND + IND + "}" + NL;
        }
        //Fill the points in the faces
        String insidePatch = "";
        String maxDistance = getMaxDistance(coordinates);
        String insidePatchPoint = "";
        for (int j = coordinates.size() - 1; j >= 0; j--) {
            insidePatch = "floor((minPosition[" + j + "] - d_grid_geometry->getXLower()[" + j + "])/dx[" + j + "] + 1E-10) <= boxlast(" + j 
                + ") && floor((maxPosition[" + j + "] - d_grid_geometry->getXLower()[" + j + "])/dx[" + j + "] + 1E-10) >= boxfirst(" + j + ") && " 
                + insidePatch;
            insidePatchPoint = "floor((position[" + j + "] - d_grid_geometry->getXLower()[" + j + "])/dx[" + j + "] + 1E-10) >= boxfirst(" + j
                + ")  && floor((position[" + j + "] - d_grid_geometry->getXLower()[" + j + "])/dx[" + j + "] + 1E-10) <= boxlast(" + j + ") && " 
                + insidePatchPoint;
        }
        insidePatchPoint = insidePatchPoint.substring(0, insidePatchPoint.lastIndexOf(" &&"));
        insidePatch = insidePatch.substring(0, insidePatch.lastIndexOf(" &&"));
        if (stencilCheck) {
            return currentIndent + "if (mapStencil) {" + NL
                + currentIndent + IND + "//Check stencil in the surface" + NL
                + currentIndent + IND + "for (unionsI = 0; unionsI < UNIONS_" + regionName + "; unionsI++) {" + NL
                + currentIndent + IND + IND + "for (facePointI = 0; facePointI < 3; facePointI++) {" + NL
                + CodeGeneratorUtils.incrementIndent(getX3dTerm(regionInfo, coordinates, gridLimit), 1)
                + currentIndent + IND + IND + IND + "//Take maximum and minimum coordinates of the face" + NL
                + currentIndent + IND + IND + IND + "if (facePointI == 0) {" + NL
                + CodeGeneratorUtils.incrementIndent(maxMin, 1)
                + currentIndent + IND + IND + IND + "} else {" + NL
                + CodeGeneratorUtils.incrementIndent(indexComparator, 1) 
                + currentIndent + IND + IND + IND + "}" + NL
                + currentIndent + IND + IND + "}" + NL
                + getMinMaxPositions(coordinates, currentIndent + IND + IND)
                + currentIndent + IND + IND + "if (" + insidePatch + ") {" + NL
                + currentIndent + IND + IND + IND + "maxDistance = " + maxDistance + ";" + NL
                + checkStencil(coordinates, CodeGeneratorUtils.incrementIndent(
                        getX3dTerms(regionInfo, coordinates, gridLimit), 1), insidePatchPoint, regionInfo.getId())
                + currentIndent + IND + IND + "}" + NL
                + currentIndent + IND + "}" + NL
                + currentIndent + "}" + NL;
        }
		return currentIndent + "//Fill the faces" + NL
		    + currentIndent + "for (unionsI = 0; unionsI < UNIONS_" + regionName + "; unionsI++) {" + NL
		    + currentIndent + IND + "for (facePointI = 0; facePointI < 3; facePointI++) {" + NL
		    + getX3dTerm(regionInfo, coordinates, gridLimit)
		    + currentIndent + IND + IND + "//Take maximum and minimum coordinates of the face" + NL
		    + currentIndent + IND + IND + "if (facePointI == 0) {" + NL
		    + maxMin 
		    + currentIndent + IND + IND + "} else {" + NL
		    + indexComparator 
		    + currentIndent + IND + IND + "}" + NL
		    + currentIndent + IND + "}" + NL
		    + getMinMaxPositions(coordinates, currentIndent + IND)
		    + currentIndent + IND + "if (" + insidePatch + ") {" + NL
		    + currentIndent + IND + IND + "maxDistance = " + maxDistance + ";" + NL
		    + fillFaceParticles(coordinates, getX3dTerms(regionInfo, coordinates, gridLimit), insidePatchPoint, regionInfo.getId())
		    + currentIndent + IND + "}" + NL
		    + currentIndent + "}" + NL;
    }
    
    /**
     * Gets the x3d term for the x3d algorithm. 
     * @param regionInfo        The region information
     * @param coordinates       The coordinates
     * @param gridLimit         The limits of the grid coordinates
     * @return                  The code
     * @throws CGException      CG004 External error
     */
    private static String getX3dTerm(MappingRegionInfo regionInfo, ArrayList<String> coordinates, LinkedHashMap<String, CoordinateInfo> gridLimit) throws CGException {
        String[][] xslParams = new String [3][2];
        xslParams[0][0] = "condition";
        xslParams[0][1] = "true";
        xslParams[1][0] = "indent";
        xslParams[1][1] = "0";
        xslParams[2][0] =  "dimensions";
        xslParams[2][1] =  String.valueOf(coordinates.size());
        String regionName = regionInfo.getRegionName();
        String currentIndent = IND + IND + IND + IND + IND;
        String term = "";
        int dim = coordinates.size();
        for (int i = 0; i < coordinates.size(); i++) {
            String discCoord = coordinates.get(i);
            //Staggering
            if (i == dim - 1) {
                String noStag = "";
                for (int j = 0; j < dim - 1; j++) {
                    String discCoord1 = coordinates.get(j);
                    noStag = currentIndent + discCoord1 + "term = floor((" + regionName + "Points[" + regionName 
                        + "Unions[unionsI][facePointI]][" + j + "] - d_grid_geometry->getXLower()[" + j + "]) / particleSeparation[" + j + "]" 
                        + ") + floor(0.5 + d_ghost_width*(dx[" + j + "]/particleSeparation[" + j + "]));" + NL + noStag;
                }
                term = currentIndent + discCoord + "term = floor((" + regionName + "Points[" + regionName 
                    + "Unions[unionsI][facePointI]][" + i + "] - d_grid_geometry->getXLower()[" + i + "])/ particleSeparation[" + i + "]" 
                    + ") + floor(0.5 + d_ghost_width*(dx[" + i + "]/particleSeparation[" + i + "]));" + NL
                    + noStag;
            }
        }
        return term;
    }
    
    /**
     * Gets the x3d terms for the x3d algorithm. 
     * @param problem			Problem
     * @param regionInfo        The region information
     * @param coordinates       The coordinates
     * @param gridLimit         The limits of the grid coordinates
     * @return                  The code
     * @throws CGException      CG004 External error
     */
    private static String getX3dTerms(MappingRegionInfo regionInfo, ArrayList<String> coordinates, LinkedHashMap<String, CoordinateInfo> gridLimit) throws CGException {
        String[][] xslParams = new String [3][2];
        xslParams[0][0] = "condition";
        xslParams[0][1] = "true";
        xslParams[1][0] = "indent";
        xslParams[1][1] = "0";
        xslParams[2][0] =  "dimensions";
        xslParams[2][1] =  String.valueOf(coordinates.size());
        String regionName = regionInfo.getRegionName();
        String indent = IND + IND + IND + IND + IND;
        String terms = "";
        int dim = coordinates.size();
        for (int i = 0; i < coordinates.size(); i++) {
            String discCoord = coordinates.get(i);
            
            //Staggering
            if (i == dim - 1) {
                String noStagTerms = "";
                for (int j = 0; j < dim - 1; j++) {
                    String discCoord1 = coordinates.get(j);
                    noStagTerms = indent + IND + IND + IND + discCoord1 + "term = floor(((" + regionName + "Points[" + regionName 
                        + "Unions[unionsI][0]][" + j + "] - d_grid_geometry->getXLower()[" + j + "])/ particleSeparation[" + j + "]" 
                        + ") * e1 + ((" + regionName
                        + "Points[" + regionName + "Unions[unionsI][1]][" + j + "] - d_grid_geometry->getXLower()[" + j 
                        + "])/ particleSeparation[" + j + "]" 
                        + ") * e2 + ((" + regionName + "Points[" + regionName + "Unions[unionsI][2]][" + j 
                        + "] - d_grid_geometry->getXLower()[" + j + "])/ particleSeparation[" + j + "]" 
                        + ") * e3) + floor(0.5 + d_ghost_width*(dx[" + j 
                        + "]/particleSeparation[" + j + "]));" + NL 
                        + indent + IND + IND + IND + IND + "position[" + j + "] = d_grid_geometry->getXLower()[" + j + "] + domain_offset_factor[" + j + "] * particleSeparation[" + j + "]"
                        + " - (floor(0.5 + d_ghost_width * (dx[" + j 
                        + "]/particleSeparation[" + j + "])))*particleSeparation[" + j + "] + (" + discCoord1
                        + "term)*particleSeparation[" + j + "];" + NL + noStagTerms;
                }
                terms = indent + IND + IND + IND + discCoord + "term = floor(((" + regionName + "Points[" + regionName 
                    + "Unions[unionsI][0]][" + i + "] - d_grid_geometry->getXLower()[" + i + "])/ particleSeparation[" + i + "]" 
                    + ") * e1 + ((" + regionName 
                    + "Points[" + regionName + "Unions[unionsI][1]][" + i + "] - d_grid_geometry->getXLower()[" + i
                    + "])/ particleSeparation[" + i + "]" 
                    + ") * e2 + ((" + regionName + "Points[" + regionName + "Unions[unionsI][2]][" + i
                    + "] - d_grid_geometry->getXLower()[" + i + "])/ particleSeparation[" + i + "]"
                    + ") * e3) + floor(0.5 + d_ghost_width*(dx[" + i + "]/particleSeparation[" + i + "]));" + NL
                    + indent + IND + IND + IND + "position[" + i + "] = d_grid_geometry->getXLower()[" + i + "] + domain_offset_factor[" + i + "] * particleSeparation[" + i + "]"
                    + " - (floor(0.5 + d_ghost_width * (dx[" + i + "]/particleSeparation[" + i + "]" 
                    + ")))*particleSeparation[" + i + "] + (" + discCoord + "term)*particleSeparation[" + i + "];" + NL
                    + noStagTerms;
            }
        }
        return terms;
    }
    
    /**
     * Gets the max. distance allowed for the filling face algorithm. 
     * @param coordinates       The spatial coordinates
     * @return                  The max distance operation
     */
    private static String getMaxDistance(ArrayList<String> coordinates) {
        String maxDistance = "MAX(";
        for (int j = coordinates.size() - 1; j >= 0; j--) {
            if (j % 3 == 1 && coordinates.size() == 3) {
                maxDistance = maxDistance + "MAX(abs(minBlock[" + j + "] - maxBlock[" + j + "]),";
            }
            else {
                maxDistance = maxDistance + "abs(minBlock[" + j + "] - maxBlock[" + j + "]),";
            }
        }
        maxDistance = maxDistance.substring(0, maxDistance.lastIndexOf(",")) + ")";
        if (coordinates.size() == 3) {
            maxDistance = maxDistance + ")";
        }
        return maxDistance;
    }
    
    /**
     * Gets minPosition and maxPosition variables to check if the x3d region is in the patch.
     * @param coordinates       The spatial coordinates
     * @param indent            The actual indentation
     * @return                  The code
     * @throws CGException      CG004 External error
     */
    private static String getMinMaxPositions(ArrayList<String> coordinates, String indent) throws CGException {
        int dim = coordinates.size();
        
        String minPosNoStag = "";
        String maxPosNoStag = "";
        for (int i = 0; i < dim; i++) {
            minPosNoStag = minPosNoStag + indent + "minPosition[" + i + "] = d_grid_geometry->getXLower()[" + i + "] + domain_offset_factor[" + i + "] * particleSeparation[" + i + "]" 
                +  " - (floor(0.5 + d_ghost_width * (dx[" + i + "]/particleSeparation[" + i + "])))*particleSeparation[" + i + "] + (minBlock[" + i + "])*particleSeparation[" + i + "];" + NL;
            maxPosNoStag = maxPosNoStag + indent + "maxPosition[" + i + "] = d_grid_geometry->getXLower()[" + i + "] + domain_offset_factor[" + i + "] * particleSeparation[" + i + "]" 
                + " - (floor(0.5 + d_ghost_width * (dx[" + i + "]/particleSeparation[" + i + "])))*particleSeparation[" + i + "] + (maxBlock[" + i + "])*particleSeparation[" + i + "];" + NL;
        }
        
        return minPosNoStag
            + maxPosNoStag;
    }
    
    /**
     * Generates the code to check the stencil width at the points in the triangle.
     * @param coordinates           The coordinates of the problem
     * @param terms                 The conversion terms from the geometric algorithm
     * @param insidePatchPoint      The code that checks if the point is inside the patch
     * @param regionId             The region id
     * @return                      The code
     * @throws CGException 
     */
    private static String fillFaceParticles(ArrayList<String> coordinates, String terms, String insidePatchPoint, int regionId) throws CGException {
        String currentIndent = IND + IND + IND + IND + IND;
        
        String index = "";
        for (int j = coordinates.size() - 1; j >= 0; j--) {
            index = "floor((position[" + j + "] - d_grid_geometry->getXLower()[" + j + "])/dx[" + j + "] + 1E-10), " + index;
        }
        index = index.substring(0, index.lastIndexOf(","));
        
        return currentIndent + "for (ie3 = 0; ie3 < 2 * maxDistance; ie3++) {" + NL
            + currentIndent + IND + "e3 = ie3 / (2 * maxDistance);" + NL 
            + currentIndent + IND + "for (ie2 = 0; ie2 < 2 * maxDistance; ie2++) {" + NL
            + currentIndent + IND + IND + "e2 = ie2 / (2 * maxDistance);" + NL
            + currentIndent + IND + IND + "for (ie1 = 0; ie1 < 2 * maxDistance; ie1++) {" + NL
            + currentIndent + IND + IND + IND + "e1 = ie1 / (2 * maxDistance);" + NL 
            + currentIndent + IND + IND + IND + "if (abs(ie1 + ie2 + ie3 - 2 * maxDistance) <= SQRT3INV) {" + NL 
            + terms 
            + currentIndent + IND + IND + IND + IND + "if (" + insidePatchPoint + ") {" + NL
            + currentIndent + IND + IND + IND + IND + IND + "hier::Index idx(" + index + ");" + NL
            + currentIndent + IND + IND + IND + IND + IND + "Particles<T>* part = particleVariables->getItem(idx);" + NL
            + currentIndent + IND + IND + IND + IND + IND + "T* particle = new T(0, position, " + regionId + ");" + NL
            + currentIndent + IND + IND + IND + IND + IND + "T* oldParticle = part->overlaps(*particle);" + NL
            + currentIndent + IND + IND + IND + IND + IND + "if (oldParticle != NULL) {" + NL
            + currentIndent + IND + IND + IND + IND + IND + IND + "part->deleteParticle(*oldParticle);" + NL
            + currentIndent + IND + IND + IND + IND + IND + "}" + NL
            + currentIndent + IND + IND + IND + IND + IND + "part->addParticle(*particle);" + NL
            + currentIndent + IND + IND + IND + IND + IND + "delete particle;" + NL
            + currentIndent + IND + IND + IND + IND + "}" + NL
            + currentIndent + IND + IND + IND + "}" + NL
            + currentIndent + IND + IND + "}" + NL
            + currentIndent + IND + "}" + NL
            + currentIndent + "}" + NL;
    }
    
    /**
     * Generates the code to check the stencil width at the points in the triangle.
     * @param coordinates           The coordinates of the problem
     * @param terms                 The conversion terms from the geometric algorithm
     * @param insidePatchPoint      The code that checks if the point is inside the patch
     * @param regionId             The region id
     * @return                      The code
     */
    private static String checkStencil(ArrayList<String> coordinates, String terms, String insidePatchPoint, int regionId) {
        String currentIndent = IND + IND + IND + IND;
        
        String checkIndex = "";
        for (int j = coordinates.size() - 1; j >= 0; j--) {
            checkIndex = coordinates.get(j) + "term, 1, " + checkIndex;
        }
        checkIndex = checkIndex.substring(0, checkIndex.lastIndexOf(","));
        
        return currentIndent + IND + IND + "for (ie3 = 0; ie3 < 2 * maxDistance; ie3++) {" + NL
            + currentIndent + IND + IND + IND + "e3 = ie3 / (2 * maxDistance);" + NL 
            + currentIndent + IND + IND + IND + "for (ie2 = 0; ie2 < 2 * maxDistance; ie2++) {" + NL
            + currentIndent + IND + IND + IND + IND + "e2 = ie2 / (2 * maxDistance);" + NL
            + currentIndent + IND + IND + IND + IND + "for (ie1 = 0; ie1 < 2 * maxDistance; ie1++) {" + NL
            + currentIndent + IND + IND + IND + IND + IND + "e1 = ie1 / (2 * maxDistance);" + NL 
            + currentIndent + IND + IND + IND + IND + IND + "if (abs(ie1 + ie2 + ie3 - 2 * maxDistance) <= SQRT3INV) {" 
            + NL + terms 
            + currentIndent + IND + IND + IND + IND + IND + IND + "if (" + insidePatchPoint + ") {" + NL
            + currentIndent + IND + IND + IND + IND + IND + IND + IND 
            + "checkStencil(patch, " + checkIndex + ", " + regionId + ");" + NL
            + currentIndent + IND + IND + IND + IND + IND + IND + "}" + NL
            + currentIndent + IND + IND + IND + IND + IND + "}" + NL
            + currentIndent + IND + IND + IND + IND + "}" + NL
            + currentIndent + IND + IND + IND + "}" + NL
            + currentIndent + IND + IND + "}" + NL;
    }
    
    /**
     * Generates the flood-fill algorithm using a stack. 
     * The recursive algorithm sometimes fails because of the size of frame stack.
     * @param coordinates   The coordinates of the problem
     * @return              The code generated
     * @throws CGException  CG004 External error
     */
    private static String generateFloodFillRoutineParticles(ArrayList<String> coordinates, ProblemInfo pi) throws CGException {
        int dims = coordinates.size();
        String index = "";
        String indexPart = "";
        String pIndex = "";
        String parameters = "";
        String pointAssign = "";
        String coordPush = "";
        String noStag = "";
        String cellIndexDeclaration = "";
        String cellIndex = "";
        String cellIndexCheck = "";
        String cellIndexNew = "";
        for (int i = 0; i < dims; i++) {
            String coord = coordinates.get(i);
            cellIndexNew = cellIndexNew + IND + IND + IND + IND + IND + "index" + coord + " = floor((newPosition[" + i 
                + "] - d_grid_geometry->getXLower()[" + i + "])/dx[" + i + "] + 1E-10);" + NL;
            indexPart = indexPart + "index" + coord + ", ";
            cellIndexCheck = cellIndexCheck + "index" + coord + " >= boxfirst(" + i + ") && index" + coord + " <= boxlast(" + i + ") && ";
        }
        cellIndexCheck = cellIndexCheck.substring(0, cellIndexCheck.lastIndexOf(" &&"));
        indexPart = indexPart.substring(0, indexPart.lastIndexOf(","));
        for (int i = 0; i < dims; i++) {
            String coord = coordinates.get(i);
            index = index + coord + ", ";
            pIndex = pIndex + "p." + coord + ", ";
            parameters = parameters + "int " + coord + ", ";
            pointAssign = pointAssign + IND + "p." + coord + " = " + coord + ";" + NL;
            String negAssign = "";
            String posAssign = "";
            for (int j = 0; j < dims; j++) {
                if (i == j) {
                    negAssign = negAssign + IND + IND + IND + IND + IND + IND + IND + "np." + coord + " = p." + coord + "-1;" + NL;
                    posAssign = posAssign + IND + IND + IND + IND + IND + IND + IND + "np." + coord + " = p." + coord + "+1;" + NL;
                }
                else {
                    negAssign = negAssign + IND + IND + IND + IND + IND + IND + IND + "np." + coordinates.get(j) + " = p." 
                        + coordinates.get(j) + ";" + NL;
                    posAssign = posAssign + IND + IND + IND + IND + IND + IND + IND + "np." + coordinates.get(j) + " = p." 
                        + coordinates.get(j) + ";" + NL;
                }
            }
            String incVar1 = "p." + coordinates.get(i) + " - 1";
            String incVar2 = "p." + coordinates.get(i) + " + 1";
            coordPush = coordPush + IND + IND + IND + IND + "if (greaterEq(position[" + i + "] - particleSeparation[" + i + "]" 
                + ", patch_geom->getXLower()[" + i + "] - dx[" + i + "]*d_ghost_width)) {" + NL
                + getNewPositions(coordinates, IND + IND + IND + IND + IND, i, incVar1)
                + cellIndexNew
                + IND + IND + IND + IND + IND + "hier::Index idx(" + indexPart + ");" + NL
                + IND + IND + IND + IND + IND + "if (" + cellIndexCheck + ") {" + NL
                + IND + IND + IND + IND + IND + IND + "NonSyncs* newnonSyncpart = nonSyncVariable->getItem(idx);" + NL
                + IND + IND + IND + IND + IND + IND + "NonSync* newnonSyncparticle = new NonSync(0, newPosition);" + NL
                + IND + IND + IND + IND + IND + IND + "NonSync* newnonSyncoldParticle = newnonSyncpart->overlaps(*newnonSyncparticle);"
                + NL
                + IND + IND + IND + IND + IND + IND + "if (newnonSyncoldParticle->getnonSync() == 0) {" + NL
                + IND + IND + IND + IND + IND + IND + IND + "Point np;" + NL
                + negAssign
                + IND + IND + IND + IND + IND + IND + IND + "mystack.push(np);" + NL
                + IND + IND + IND + IND + IND + IND + "}" + NL
                + IND + IND + IND + IND + IND + IND + "delete newnonSyncparticle;" + NL
                + IND + IND + IND + IND + IND + "}" + NL
                + IND + IND + IND + IND + "}" + NL
                + IND + IND + IND + IND + "if (position[" + i + "] + particleSeparation[" + i + "] < patch_geom->getXUpper()[" + i 
                + "] + dx[" + i + "]*d_ghost_width) {" + NL
                + getNewPositions(coordinates, IND + IND + IND + IND + IND, i, incVar2)
                + cellIndexNew
                + IND + IND + IND + IND + IND + "hier::Index idx(" + indexPart + ");" + NL
                + IND + IND + IND + IND + IND + "if (" + cellIndexCheck + ") {" + NL
                + IND + IND + IND + IND + IND + IND + "NonSyncs* newnonSyncpart = nonSyncVariable->getItem(idx);" + NL
                + IND + IND + IND + IND + IND + IND + "NonSync* newnonSyncparticle = new NonSync(0, newPosition);" + NL
                + IND + IND + IND + IND + IND + IND + "NonSync* newnonSyncoldParticle = newnonSyncpart->overlaps(*newnonSyncparticle);"
                + NL
                + IND + IND + IND + IND + IND + IND + "if (newnonSyncoldParticle->getnonSync() == 0) {" + NL
                + IND + IND + IND + IND + IND + IND + IND + "Point np;" + NL
                + posAssign
                + IND + IND + IND + IND + IND + IND + IND + "mystack.push(np);" + NL
                + IND + IND + IND + IND + IND + IND + "}" + NL
                + IND + IND + IND + IND + IND + IND + "delete newnonSyncparticle;" + NL
                + IND + IND + IND + IND + IND + "}" + NL
                + IND + IND + IND + IND + "}" + NL;
            noStag = noStag + IND + IND + "position[" + i + "] = d_grid_geometry->getXLower()[" + i + "] + domain_offset_factor[" + i + "] * particleSeparation[" + i + "]"
            		+ " - (floor(0.5 + d_ghost_width * (dx[" + i + "]/particleSeparation[" + i + "]" 
                    + ")))*particleSeparation[" + i + "] + (p." + coord + ")*particleSeparation[" + i + "];" + NL;
            cellIndexDeclaration = cellIndexDeclaration + IND + "int index" + coord + ";" + NL;
            cellIndex = cellIndex + IND + IND + "index" + coord + " = floor((position[" + i + "] - d_grid_geometry->getXLower()[" + i + "])/dx["
                + i + "] + 1E-10);" + NL;
        }
        index = index.substring(0, index.length() - 2);
        pIndex = pIndex.substring(0, pIndex.length() - 2);

        String particleVariable = IND + "int tmpVar_id;" + NL;
        for (String speciesName : pi.getParticleSpecies()) {
        	particleVariable = particleVariable + IND + "if (std::is_same<T, Particle_" + speciesName + ">::value) {" + NL
        			+ IND + IND + "tmpVar_id = d_particleVariables_" + speciesName + "_id;" + NL
        			+ IND + "}" + NL;
    	}
        particleVariable = particleVariable + IND + "std::shared_ptr< pdat::IndexData<Particles<T>, pdat::CellGeometry > > particleVariables(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles<T>, pdat::CellGeometry >, hier::PatchData>(patch.getPatchData(tmpVar_id)));" + NL;
        
        
        return "template<class T>" + NL
        	+ "void Problem::floodfillParticles(const hier::Patch& patch, std::vector<double> particleSeparation, std::vector<double> domain_offset_factor, " + parameters + "int pred, int seg) const {" + NL + NL
            + IND + "double position[" + dims + "], newPosition[" + dims + "];" + NL
            + particleVariable
            + IND + "std::shared_ptr< pdat::IndexData<NonSyncs, pdat::CellGeometry > > nonSyncVariable(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< NonSyncs, pdat::CellGeometry >, hier::PatchData>(patch.getPatchData(d_nonSyncP_id)));" + NL + NL
            + IND + "const hier::Index boxfirst = particleVariables->getGhostBox().lower();" + NL
            + IND + "const hier::Index boxlast  = particleVariables->getGhostBox().upper();" + NL + NL
            + IND + "const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch.getPatchGeometry()));" + NL
            + IND + "const double* dx  = patch_geom->getDx();" + NL
            + cellIndexDeclaration + NL
            + IND + "stack<Point> mystack;" + NL + NL
            + IND + "Point p;" + NL
            + pointAssign + NL
            + IND + "mystack.push(p);" + NL
            + IND + "while(mystack.size() > 0) {" + NL
            + IND + IND + "p = mystack.top();" + NL
            + IND + IND + "mystack.pop();" + NL
            + noStag
            + cellIndex
            + createCoordPushBlock(coordPush, dims, indexPart, cellIndexCheck)
            + IND + "}" + NL
            + "}" + NL;
    }
    /**
     * Generates the code used for getting the position of the particles during the floodfill routine. 
     * @param problem       The problem
     * @param coordinates   The spatial coordinates
     * @param indent        The actual indentation
     * @param coordNum      The coordinate number to be operated
     * @param incVar        The variable operated
     * @return              The code
     * @throws CGException  CG004 External error
     */
    private static String getNewPositions(ArrayList<String> coordinates, String indent, int coordNum, 
            String incVar) throws CGException {
        String noStag = "";
        for (int k = 0; k < coordinates.size(); k++) {
            String coord = coordinates.get(k);
            if (coordNum == k) {
                noStag = noStag + indent + "newPosition[" + k + "] = d_grid_geometry->getXLower()[" + k + "] + domain_offset_factor[" + k + "] * particleSeparation[" + k + "]" 
                    + " - (floor(0.5 + d_ghost_width * (dx[" + k + "]/particleSeparation[" + k + "]" 
                    + ")))*particleSeparation[" + k + "] + (" + incVar + ")*particleSeparation[" + k + "];" + NL;
            }
            else {
                noStag = noStag + indent + "newPosition[" + k + "] = d_grid_geometry->getXLower()[" + k + "] + domain_offset_factor[" + k + "] * particleSeparation[" + k + "]"
                    + " - (floor(0.5 + d_ghost_width * (dx[" + k + "]/particleSeparation[" + k + "]" 
                    + ")))*particleSeparation[" + k + "] + (p." + coord + ")*particleSeparation[" + k + "];" + NL;
            }
        }
        
        return noStag;
    }
    
    /**
     * Creates the coordinate push block for the floodfill routine.
     * @param coordPush         The coordinate push part of the algorithm
     * @param dims              The dimensions of the problem
     * @param indexPart         The index of the particle cell
     * @param cellIndexCheck    The check for the index of the particle cell
     * @return                  The code
     */
    private static String createCoordPushBlock(String coordPush, int dims, String indexPart, String cellIndexCheck) {
        return IND + IND + "hier::Index idx(" + indexPart + ");" + NL
            + IND + IND + "if (" + cellIndexCheck + ") {" + NL
            + IND + IND + IND + "T* particle = new T(0, position, 0);" + NL
            + IND + IND + IND + "Particles<T>* part = particleVariables->getItem(idx);" + NL
            + IND + IND + IND + "T* oldParticle = part->overlaps(*particle);" + NL
            + IND + IND + IND + "NonSyncs* nonSyncpart = nonSyncVariable->getItem(idx);" + NL
            + IND + IND + IND + "NonSync* nonSyncparticle = new NonSync(0, position);" + NL
            + IND + IND + IND + "NonSync* nonSyncoldParticle = nonSyncpart->overlaps(*nonSyncparticle);" + NL
            + IND + IND + IND + "if (nonSyncoldParticle->getnonSync() == 0) {" + NL
            + IND + IND + IND + IND + "nonSyncoldParticle->setnonSync(pred);" + NL
            + IND + IND + IND + IND + "oldParticle->interior = pred;" + NL
            + IND + IND + IND + IND + "if (pred == 2) {" + NL
            + IND + IND + IND + IND + IND + "oldParticle->region = seg;" + NL
            + IND + IND + IND + IND + "}" + NL
            + coordPush
            + IND + IND + IND + "}" + NL
            + IND + IND + IND + "delete nonSyncparticle;" + NL
            + IND + IND + IND + "delete particle;" + NL
            + IND + IND + "}" + NL;
    }
    
    /**
     * Creates the staggering control.
     * @param checkVar          The variable used to check the staggering condition
     * @param indent            The actual indent
     * @param coordinates       The spatial coordinates
     * @return                  The code
     * @throws CGException      CG004 External error
     */
    private static String createStaggeringControl(String checkVar, String indent, ArrayList<String> coordinates) throws CGException {
        String regular = "";
        String staggered = "";
        for (int i = 0; i < coordinates.size() - 1; i++) {
            String coord = coordinates.get(i);
            staggered = staggered + indent + IND + coord + "MapStart = " + coord + "MapStart + particleSeparation[" + i + "]/2;" + NL;
            regular = regular + indent + coord + "MapStart = d_grid_geometry->getXLower()[" + i + "] + domain_offset_factor[" + i + "] * "
            		+ "particleSeparation[" + i + "] - (floor(0.5 + d_ghost_width * (dx[" + i + "]/particleSeparation[" + i + "])))*"
            		+ "particleSeparation[" + i + "];" + NL;
        }
        return regular
        	+ indent + "if (particleDistribution.compare(\"STAGGERED\") == 0 && isEven(" + checkVar + ")) {" + NL
            + staggered
            + indent + "}" + NL;
    }
}
