/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.codeGeneratorPlugin.fvm;

import eu.iac3.mathMS.codeGeneratorPlugin.CGException;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.CodeGeneratorUtils;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.ProblemInfo;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.SAMRAIUtils;

import java.util.ArrayList;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * Class that generates the boundaries for Cellular automata.
 * @author bminano
 *
 */
public final class SAMRAIABM_meshBoundaries {

    static final String NL = System.getProperty("line.separator");
    static final String IND = "\u0009";
    static String mmsUri = "urn:mathms";
    static String mtUri = "http://www.w3.org/1998/Math/MathML";
    /**
     * Private constructor.
     */
    private SAMRAIABM_meshBoundaries() { };
    
    /**
     * Creates the boundaries.
     * @param actualIndent      The indentation
     * @param pi                The problem information
     * @return                  The code
     * @throws CGException      CG00X External error
     */
    public static String createBoundaries(String actualIndent, ProblemInfo pi) throws CGException {
        //TODO hacer multiregion
        
        String indent = actualIndent;
        for (int k = pi.getDimensions() - 1; k >= 0; k--) {
            indent = indent + IND;
        }
        
        String bounds = generateBoundaryCode(indent, pi, 0); 
        if (bounds.equals("")) {
            return "";
        }
        return createCALoop(actualIndent, pi, pi.getDimensions())
                + bounds
                + SAMRAIUtils.createCellLoopEnd(indent, pi.getDimensions());
    }
    
    /**
     * Creates the boundaries.
     * @param indent            The indentation
     * @param pi                The problem information
     * @param regionNum        The region number
     * @return                  The code
     * @throws CGException      CG00X External error
     */
    private static String generateBoundaryCode(String indent, ProblemInfo pi, int regionNum) throws CGException {
        String[][] xslParams = new String [4][2];
        xslParams[0][0] = "condition";
        xslParams[0][1] = "false";
        xslParams[1][0] = "indent";
        xslParams[1][1] = String.valueOf(indent.length());
        xslParams[2][0] = "dimensions";
        xslParams[2][1] = String.valueOf(pi.getDimensions());
        xslParams[3][0] = "timeCoord";
        xslParams[3][1] = pi.getTimeCoord();
        Element coordinates = CodeGeneratorUtils.createElement(pi.getProblem(), null, "coords");
        for (int i = 0; i < pi.getDimensions(); i++) {
            String coord = pi.getCoordinates().get(i);
            Element coordE = CodeGeneratorUtils.createElement(pi.getProblem(), null, coord);
            coordE.setTextContent(coord);
            coordinates.appendChild(coordE);  
        }
        String result = "";
        Document problem = pi.getProblem();
        
        String index = "";
        for (int j = 0; j < pi.getCoordinates().size(); j++) {
            index = index + "index" + j + ", ";
        }
        index = index.substring(0, index.lastIndexOf(", "));
        
        if (CodeGeneratorUtils.find(problem, "//mms:boundaryCondition").getLength() > 0) {
            ArrayList<String> coords = pi.getCoordinates();
            for (int i = 0; i < coords.size(); i++) {
                String query = "//mms:boundaryCondition[mms:axis ='" 
                    + coords.get(i) + "' or lower-case(mms:axis) ='all']";
                NodeList boundList = CodeGeneratorUtils.find(problem, query);
                //Axis iteration
                for (int k = 0; k < boundList.getLength(); k++) {
                    Element boundary = (Element) boundList.item(k).cloneNode(true);
                    SAMRAIUtils.preProcessPDEBlock(boundary, pi, false, false);
                    String type = boundary.getElementsByTagNameNS(mmsUri, "type").item(0).getFirstChild().getLocalName();
                    if (!type.toLowerCase().equals("periodical")) {
                        String side = boundary.getElementsByTagNameNS(mmsUri, "side").item(0).getTextContent();
                        //Lower
                        if (side.toLowerCase().equals("all") || side.toLowerCase().equals("lower")) {
                            int boundId = i * 2 + 1;
                            result = result + indent + "if (vector(FOV_" + CodeGeneratorUtils.getRegionName(pi.getProblem(), -boundId, 
                                    pi.getCoordinates()) + ", " + index + ") > 0) {" + NL;
                            indent = indent + IND;
                            //if there is a condition generate the if element
                            boolean condition = false;
                            if (CodeGeneratorUtils.find(boundList.item(k), "./mms:applyIf").getLength() > 0) {
                                condition = true;
                                xslParams[0][1] = "true";
                                result = result + indent + SAMRAIUtils.xmlTransform(CodeGeneratorUtils.find(boundary,
                                        ".//mms:applyIf").item(0), xslParams, "coords", coordinates);
                                indent = indent + IND;
                            }
                            //Process boundary
                            NodeList instructions = CodeGeneratorUtils.find(boundary, ".//sml:algorithm/*");
                            for (int j = 0; j < instructions.getLength(); j++) {
                                xslParams[0][1] = "false";
                                xslParams[1][1] = String.valueOf(indent.length());
                                result = result + SAMRAIUtils.xmlTransform(instructions.item(j), xslParams, "coords", coordinates);
                            }
                            if (condition) {
                                indent = indent.substring(1);
                                result = result + indent + "}" + NL;
                            }
                            indent = indent.substring(1);
                            result = result + indent + "}" + NL;
                        }
                        //Upper
                        if (side.toLowerCase().equals("all") || side.toLowerCase().equals("upper")) {
                            int boundId = i * 2 + 2;
                            result = result + indent + "if (vector(FOV_" + CodeGeneratorUtils.getRegionName(pi.getProblem(), -boundId, 
                                    pi.getCoordinates()) + ", " + index + ") > 0) {" + NL;
                            indent = indent + IND;
                            //if there is a condition generate the if element
                            boolean condition = false;
                            if (CodeGeneratorUtils.find(boundList.item(k), "./mms:applyIf").getLength() > 0) {
                                condition = true;
                                xslParams[0][1] = "true";
                                result = result + indent + SAMRAIUtils.xmlTransform(CodeGeneratorUtils.find(boundary, 
                                        ".//mms:applyIf").item(0), xslParams, "coords", coordinates);
                                indent = indent + IND;
                            }
                            //Process boundary
                            NodeList instructions = CodeGeneratorUtils.find(boundary, ".//sml:algorithm/*");
                            for (int j = 0; j < instructions.getLength(); j++) {
                                xslParams[0][1] = "false";
                                xslParams[1][1] = String.valueOf(indent.length());
                                result = result + SAMRAIUtils.xmlTransform(instructions.item(j), xslParams, "coords", coordinates);
                            }
                            if (condition) {
                                indent = indent.substring(1);
                                result = result + indent + "}" + NL;
                            }
                            indent = indent.substring(1);
                            result = result + indent + "}" + NL;
                        }
                    }
                }
            }
        }
        return result;
    }
    
    /**
     * Creates the loop for an execution block.
     * @param indent                The indentation
     * @param pi                    The problem Information
     * @param dimensions            The spatial dimensions
     * @return                      The loop
     */
    private static String createCALoop(String indent, ProblemInfo pi, int dimensions) {
        String block = "";
        
        String actualIndent = indent;
        for (int k = dimensions - 1; k >= 0; k--) {
            String coordString = pi.getCoordinates().get(k);
            //Loop iteration
            block =  block + actualIndent + "for(int index" + k + " = 0; " 
                + "index" + k + " < " + coordString + "last; index" + k + "++) {" + NL;
            actualIndent = actualIndent + IND;
        }
        
        return block;
    }
}
