/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.codeGeneratorPlugin.fvm;

import eu.iac3.mathMS.codeGeneratorPlugin.CGException;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.CodeGeneratorUtils;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.ProblemInfo;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.SAMRAIUtils;

import java.util.ArrayList;

import org.osgi.service.log.LogService;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Execution flow for FVM code in SAMRAI.
 * @author bminyano
 *
 */
public final class SAMRAIABM_meshExecution {
    static final String NL = System.getProperty("line.separator");
    static final String IND = "\u0009";
    static final int THREE = 3;
    static final String MTURI = "http://www.w3.org/1998/Math/MathML";
    static LogService logservice;
    
    /**
     * Private constructor.
     */
    private SAMRAIABM_meshExecution() { };
    
    /**
     * Create the code for the execution step.
     * @param pi                    The problem information
     * @return                      The file
     * @throws CGException          CG00X External error
     */
    public static String create(ProblemInfo pi) throws CGException {
        StringBuffer result = new StringBuffer();
        Document doc = pi.getProblem();
        
        int dimensions = pi.getSpatialDimensions();
        String actualIND = IND + IND;
        try {
            String[][] xslParams = new String [4][2];
            xslParams[0][0] = "condition";
            xslParams[0][1] = "false";
            xslParams[1][0] = "indent";
            xslParams[2][0] = "dimensions";
            xslParams[2][1] = String.valueOf(pi.getDimensions());
            xslParams[3][0] = "timeCoord";
            xslParams[3][1] = pi.getTimeCoord();
            
            Element coordinates = CodeGeneratorUtils.createElement(pi.getProblem(), null, "coords");
            for (int i = 0; i < pi.getDimensions(); i++) {
                String coord = pi.getCoordinates().get(i);
                Element coordE = CodeGeneratorUtils.createElement(pi.getProblem(), null, coord);
                coordE.setTextContent(coord);
                coordinates.appendChild(coordE);  
            }

            //Control when the synchronizations has to be launched
            boolean sync = false;
            
            String commonVariables = IND + IND + "//Get the dimensions of the patch" + NL 
                + IND + IND + "hier::Box pbox = patch->getBox();" + NL
                + IND + IND + "const hier::Index boxfirst = patch->getBox().lower();" + NL
                + IND + IND + "const hier::Index boxlast = patch->getBox().upper();" + NL + NL
                + IND + IND + "//Get delta spaces into an array. dx, dy, dz." + NL
                + IND + IND + "std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));" + NL
                + IND + IND + "const double* dx  = patch_geom->getDx();" + NL + NL
                + IND + IND + "//Auxiliary definitions" + NL;
            String endPatchWithCoarseningIteration = IND + "}" + NL
                    + IND + "if (ln > 0) {" + NL
                    + IND + IND + "d_coarsen_schedule[ln]->coarsenData();" + NL
                    + IND + "}" + NL  
                    + "}" + NL;
            String endPatchIteration = IND + "}" + NL
                    + "}" + NL;
            
            //Initialization of the variables for iteration
            String indexes = ""; 
            for (int k = 0; k < dimensions; k++) {
                //Initialization of the variables
                String coordString = pi.getCoordinates().get(k);
                commonVariables =  commonVariables + actualIND + "int " + coordString + "last = boxlast(" + k + ")-boxfirst(" 
                        + k + ") + 2 + 2 * d_ghost_width;" + NL;
                indexes = indexes + "index" + k + ", ";
            }
            indexes = indexes.substring(0, indexes.lastIndexOf(", "));
            
            NodeList rules = CodeGeneratorUtils.find(doc, "//mms:ruleExecutionOrder/mms:rule");
            DocumentFragment variables = doc.createDocumentFragment();
            String instructions = "";
            if (pi.getEvolutionStep().equals("AllAgents")) {
                for (int k = 0; k < dimensions; k++) {
                    //Initialization of the variables
                    actualIND = actualIND + IND;
                }
            	String cellInit = createExecutionBlockLoop(pi, dimensions);
            	String cellEnd = SAMRAIUtils.createCellLoopEnd(actualIND, dimensions);
            	
            	//Process every region
            	Element previousRule = null;
              	DocumentFragment instructionAcc = doc.createDocumentFragment();
                for (int i = 0; i < rules.getLength(); i++) {
                    Element ruleOrder = (Element) rules.item(i);
                    String ruleName = ruleOrder.getTextContent();
                    Element rule = (Element) CodeGeneratorUtils.find(doc, "//*[(local-name() = 'updateRule' or local-name() = 'gatherRule') " 
                    		+ "and descendant::mms:name = '" + ruleName + "']").item(0).cloneNode(true);
                    String ruleType = rule.getLocalName();
                    boolean unifiable = false;
                    if (previousRule != null) {
                        unifiable = CodeGeneratorUtils.checkUnifiable(null, ruleOrder, previousRule, pi.getVariables(), 
                	    		pi.getDimensions());
                    }
                	
                	//Patch iterators
                    if (!unifiable) {
                        if (i > 0) {	
                            NodeList blocks = CodeGeneratorUtils.unifyInstructions(instructionAcc, pi).getChildNodes();
                            for (int j = 0; j < blocks.getLength(); j++) {
                                instructions = instructions + SAMRAIUtils.xmlTransform(blocks.item(j), xslParams, "coords", 
                                        coordinates) + NL;
                            }
                			
                            result.append(SAMRAIUtils.getPDEVariableDeclaration(variables, pi, 2, new ArrayList<String>(), 
                                    "patch->", false, false) + commonVariables + cellInit + instructions
                                    + cellEnd + endPatchIteration);
                            variables = doc.createDocumentFragment();
                            instructions = "";
                            instructionAcc = doc.createDocumentFragment();
                        }
                        result.append(patchIterator(pi.getRegionIds(), sync, dimensions, i));
                        sync = false;
                    }
                	
                	//Variables
                    variables.appendChild(rule);
                	
                	//Add condition to execute only interior cells
                    if (ruleType.equals("gatherRule")) {
                        xslParams[1][1] = String.valueOf(actualIND.length() + 1);
                        instructions = instructions + actualIND + "if (vector(FOV_1, " + indexes + ") > 0) {" + NL;
                    }
                    else {
                        xslParams[1][1] = String.valueOf(actualIND.length());
                    }
                	
                    NodeList algorithm = CodeGeneratorUtils.find(doc, "//*[(local-name() = 'updateRule' or local-name() = 'gatherRule')"
                            + " and descendant::mms:name = '" + ruleName + "']//sml:algorithm/*");
                    for (int k = 0; k < algorithm.getLength(); k++) {
                        Element block = (Element) SAMRAIUtils.preProcessPDEBlock(algorithm.item(k).cloneNode(true), pi, false, false);
                        instructionAcc.appendChild(block);
                    }
                	
                    if (ruleType.equals("gatherRule")) {
                        instructions = instructions + actualIND + "}" + NL;
                    }
                	
                	//If the actual block is a gathering block then the next block has to synchronize
                    DocumentFragment blockScope = doc.createDocumentFragment();
                    Element blockList = (Element) CodeGeneratorUtils.find(doc, "//*[(local-name() = 'updateRule' or local-name() = 'gatherRule') " 
                            + "and descendant::mms:name = '" + ruleName + "']").item(0).cloneNode(true);
                    blockScope.appendChild(blockList);
                    if (ruleType.equals("gatherRule")) {
                        sync = true;
                    }
                    previousRule = (Element) ruleOrder.cloneNode(true);
                    
                }
                //Create last step
                NodeList blocks = CodeGeneratorUtils.unifyInstructions(instructionAcc, pi).getChildNodes();
                for (int j = 0; j < blocks.getLength(); j++) {
                    instructions = instructions + SAMRAIUtils.xmlTransform(blocks.item(j), xslParams, "coords", coordinates)
                            + NL;
                }
            	result.append(SAMRAIUtils.getPDEVariableDeclaration(variables, pi, 2, new ArrayList<String>(), 
            			"patch->", false, false) + commonVariables + cellInit + instructions + cellEnd + endPatchWithCoarseningIteration);
            	
            	//The boundary calculations
            	String boundaries = SAMRAIABM_meshBoundaries.createBoundaries(IND + IND, pi);
            	if (!boundaries.equals("")) {
            	    Node boundaryscope = CodeGeneratorUtils.find(pi.getProblem(), "//mms:boundaryCondition").item(0);
            	    String varDec = SAMRAIUtils.getPDEVariableDeclaration(boundaryscope, pi, 2, new ArrayList<String>(), 
            	            "patch->", false, false) + NL;
            	    result.append(patchIterator(pi.getRegionIds(), sync, dimensions, rules.getLength()) + varDec + commonVariables + boundaries
            	            + endPatchIteration);
                	//Last synchronization performed
            	    if (sync) {
            	        sync = false;
            	    }
            	}
                if (sync) {
                    result.append(emptySynchronization(dimensions, rules.getLength()));
                }
            }
            else {
            	DocumentFragment instructionAcc = doc.createDocumentFragment();
                xslParams[1][1] = "2";

            	result.append(patchIterator(pi.getRegionIds(), true, dimensions, 0));
            	//Get scope for the declarations
                DocumentFragment scope = doc.createDocumentFragment();
                String query = "//sml:algorithm[ancestor::mms:rules]";
                NodeList scopes = CodeGeneratorUtils.find(doc, query);
                for (int k = 0; k < scopes.getLength(); k++) {
                    scope.appendChild(scopes.item(k).cloneNode(true));
                    
                }
                String varDec = SAMRAIUtils.getPDEVariableDeclaration(scope, pi, 2, new ArrayList<String>(), 
                		"patch->", false, false) + NL
                        + commonVariables;
            	
                String randomSelection = createRandomSelection(pi.getCoordinates());
                
                for (int i = 0; i < rules.getLength(); i++) {
                    Element ruleOrder = (Element) rules.item(i);
                    String ruleName = ruleOrder.getTextContent();
                    NodeList algorithm = CodeGeneratorUtils.find(doc, 
                    		"//*[(local-name() = 'updateRule' or local-name() = 'gatherRule') and descendant::mms:name = '" 
                    				+ ruleName + "']//sml:algorithm/*");
                    for (int k = 0; k < algorithm.getLength(); k++) {
                        Element block = (Element) SAMRAIUtils.preProcessPDEBlock(algorithm.item(k).cloneNode(true), pi, false, false);
                        instructionAcc.appendChild(block);
                    }
                }
                NodeList blocks = CodeGeneratorUtils.unifyInstructions(instructionAcc, pi).getChildNodes();
                for (int j = 0; j < blocks.getLength(); j++) {
                    instructions = instructions + SAMRAIUtils.xmlTransform(blocks.item(j), xslParams, "coords", coordinates)
                            + NL;
                }
                result.append(varDec + randomSelection + instructions);
                //The last step is the boundary calculations
                result.append(SAMRAIABM_meshBoundaries.createBoundaries(IND + IND, pi) + NL);
            	result.append(endPatchIteration);
            }
            
            
            return result.toString();
        } 
        catch (CGException e) {
            throw e;
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new CGException(CGException.CG00X, e.getMessage());
        }
    }
    
    /**
     * Creates the code for a random cell selection.
     * @param coords    The coordinates of the problem
     * @return          The code
     */
    private static String createRandomSelection(ArrayList<String> coords) {
    	String size = "";
    	String indexes = "";
    	for (int i = 0; i < coords.size(); i++) {
    	    String coord = coords.get(i);
    	    size = size + "(boxlast(" + i + ") - boxfirst(" + i + ") + 1) * ";
    	    indexes = indexes + IND + IND + "int index" + i + " = index" + coord + "Of(selected_cell) + d_ghost_width;" + NL;
    	}
    	
    	return IND + IND + "int selected_cell = " + size + "gsl_rng_uniform(r_var);" + NL
    			+ indexes + NL;
    }
    
    /**
     * Call to an empty synchronization.
     * @param dimensions    The dimensions of the problem
     * @param blockNumber       The number of the block for the synchronization
     * @return              The call code.
     */
    private static String emptySynchronization(int dimensions, int blockNumber) {
        return "for (int ln=0; ln<=d_patch_hierarchy->getFinestLevelNumber(); ++ln ) {" + NL
            + IND + "std::shared_ptr<hier::PatchLevel > level(d_patch_hierarchy->getPatchLevel(ln));" + NL
            + IND + "//Fill ghosts and periodical boundaries, but no other boundaries" + NL
            + IND + "d_bdry_sched_advance" + blockNumber + "[ln]->fillData(current_time, false);" + NL
            + "}";
    }
    
    /**
     * Code for the patch iteration and syncs.
     * @param regions          Thte regions of the problem
     * @param sync              If there is a pending sync.
     * @param dimensions        The dimensions of the problem
     * @param blockNumber       The number of the block for the synchronization
     * @return                  The code
     */
    private static String patchIterator(ArrayList<String> regions, boolean sync, int dimensions, int blockNumber) {
        String block = "for (int ln=0; ln<=d_patch_hierarchy->getFinestLevelNumber(); ++ln ) {" + NL
            + IND + "std::shared_ptr<hier::PatchLevel > level(d_patch_hierarchy->getPatchLevel(ln));" + NL;
        if (sync) {
            block = block + IND + "//Fill ghosts and periodical boundaries, but no other boundaries" + NL
                + IND + "d_bdry_sched_advance" + blockNumber + "[ln]->fillData(current_time, false);" + NL;
        }
        return block + IND + "for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {" + NL
            + IND + IND + "const std::shared_ptr<hier::Patch >& patch = *p_it;" + NL
            + SAMRAIUtils.createFOVDeclaration(regions, IND + IND, "patch->")
            + NL;
    }

    /**
     * Creates the loop for an execution block.
     * @param pi                    The problem Information
     * @param dimensions            The spatial dimensions
     * @return                      The loop
     */
    private static String createExecutionBlockLoop(ProblemInfo pi, int dimensions) {
        String block = "";
        
        String actualIND = IND + IND;
        for (int k = dimensions - 1; k >= 0; k--) {
            String coordString = pi.getCoordinates().get(k);
            //Loop iteration
            block =  block + actualIND + "for(int index" + k + " = 0; index" + k + " < " 
            		+ coordString + "last; index" + k + "++) {" + NL;
            actualIND = actualIND + IND;
        }
        
        return block;
    }
    
    /**
     * Create the special loop structure due to the use of flat or maximal dissipation.
     * The first corner should have problems to get flat value if the rest of the cells are not assigned.
     * The general loop must start from the center of the domain and is divided in 4 (2D) or 8 (3D) loops
     * to cover all the cells.
     * @param dimensions        The number of coordinates
     * @param pi                The problem info
     * @param region           The region code
     * @param actualIND      The actual INDation
     * @return                  The code
     */
    public static String createFlatLoopStructure(int dimensions, ProblemInfo pi, String region, String actualIND) {
        if (dimensions == THREE) {
            return createExecutionBlockLoopBound(pi, true, true, true)
                + region
                + createExecutionBlockLoopEnd(actualIND, dimensions)
                + createExecutionBlockLoopBound(pi, true, true, false)
                + region
                + createExecutionBlockLoopEnd(actualIND, dimensions)
                + createExecutionBlockLoopBound(pi, true, false, false)
                + region
                + createExecutionBlockLoopEnd(actualIND, dimensions)
                + createExecutionBlockLoopBound(pi, true, false, true)
                + region
                + createExecutionBlockLoopEnd(actualIND, dimensions)
                + createExecutionBlockLoopBound(pi, false, true, true)
                + region
                + createExecutionBlockLoopEnd(actualIND, dimensions)
                + createExecutionBlockLoopBound(pi, false, true, false)
                + region
                + createExecutionBlockLoopEnd(actualIND, dimensions)
                + createExecutionBlockLoopBound(pi, false, false, false)
                + region
                + createExecutionBlockLoopEnd(actualIND, dimensions)
                + createExecutionBlockLoopBound(pi, false, false, true)
                + region
                + createExecutionBlockLoopEnd(actualIND, dimensions); 
        }
		return createExecutionBlockLoopBound(pi, true, true, false)
		    + region
		    + createExecutionBlockLoopEnd(actualIND, dimensions)
		    + createExecutionBlockLoopBound(pi, true, false, false)
		    + region
		    + createExecutionBlockLoopEnd(actualIND, dimensions)
		    + createExecutionBlockLoopBound(pi, false, true, false)
		    + region
		    + createExecutionBlockLoopEnd(actualIND, dimensions)
		    + createExecutionBlockLoopBound(pi, false, false, false)
		    + region
		    + createExecutionBlockLoopEnd(actualIND, dimensions);
    }
    
    /**
     * Creates the block loop starting at the center of the domain. This is necessary for flat and maximal
     * dissipation boundaries when parabolic terms are used.
     * @param pi            The problem information
     * @param xForward      The direction of the loop (true forward; false backward) for the 1st coordinate
     * @param yForward      The direction of the loop (true forward; false backward) for the 2nd coordinate
     * @param zForward      The direction of the loop (true forward; false backward) for the 3rd coordinate
     * @return              The loop
     */
    private static String createExecutionBlockLoopBound(ProblemInfo pi, boolean xForward, boolean yForward, boolean zForward) {
        String block = "";
        
        String actualIND = IND + IND;
        for (int k = pi.getCoordinates().size() - 1; k >= 0; k--) {
            String coordString = pi.getCoordinates().get(k);
            //Loop iteration
            if (k == 2) {
                if (zForward) {
                    block =  block + actualIND + "for(int " + coordString + " = " + coordString + "last/2; " 
                        + coordString + " < " + coordString + "last; " + coordString + "++) {" + NL;
                }
                else {
                    block =  block + actualIND + "for(int " + coordString + " = " + coordString + "last/2 - 1; " 
                        + coordString + " >= 0; " + coordString + "--) {" + NL; 
                }
            }
            if (k == 1) {
                if (yForward) {
                    block =  block + actualIND + "for(int " + coordString + " = " + coordString + "last/2; " 
                        + coordString + " < " + coordString + "last; " + coordString + "++) {" + NL;
                }
                else {
                    block =  block + actualIND + "for(int " + coordString + " = " + coordString + "last/2 - 1; " 
                        + coordString + " >= 0; " + coordString + "--) {" + NL; 
                }
            }
            if (k == 0) {
                if (xForward) {
                    block =  block + actualIND + "for(int " + coordString + " = " + coordString + "last/2; " 
                        + coordString + " < " + coordString + "last; " + coordString + "++) {" + NL;
                }
                else {
                    block =  block + actualIND + "for(int " + coordString + " = " + coordString + "last/2 - 1; " 
                        + coordString + " >= 0; " + coordString + "--) {" + NL; 
                }
            }
            actualIND = actualIND + IND;
        }
        
        return block;
    }
    
    /**
     * Creates the loop end for an execution block.
     * @param actualIND      The actual IND
     * @param dimensions        The spatial dimensions
     * @return                  The code
     */
    public static String createExecutionBlockLoopEnd(String actualIND, int dimensions) {
        String block = "";
        for (int k = 0; k < dimensions; k++) {
            actualIND = actualIND.substring(0, actualIND.lastIndexOf(IND));
            block =  block + actualIND + "}" + NL;
        }
        
        return block;
    }
    
}
