/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.codeGeneratorPlugin.particles;

import eu.iac3.mathMS.codeGeneratorPlugin.CGException;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.CodeGeneratorUtils;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.ProblemInfo;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.SAMRAIUtils;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * Functions for particles code in SAMRAI.
 * @author bminyano
 *
 */
public final class SAMRAIABM_meshlessFunctions {
    static final String NEWLINE = System.getProperty("line.separator");
    static final String INDENT = "\u0009";
    static final int THREE = 3;
    static final String MTURI = "http://www.w3.org/1998/Math/MathML";
    
    /**
     * Default constructor.
     */
    private SAMRAIABM_meshlessFunctions() { };
    
    /**
     * Creates the external functions file for simPlat + SAMRAI.
     * @param pi                    The problem information
     * @return                      The file
     * @throws CGException          CG004 External error
     */
    public static String create(ProblemInfo pi) throws CGException {
        String result = "#functions#" + NEWLINE + NEWLINE;
        Document doc = pi.getProblem();
        String[][] params = new String [THREE][2];
        params[0][0] = "condition";
        params[0][1] = "false";
        params[1][0] = "indent";
        params[1][1] = "1";
        params[2][0] =  "dimensions";
        params[2][1] =  String.valueOf(pi.getDimensions());
        int dimensions = pi.getSpatialDimensions();
        try {
            NodeList functionList = CodeGeneratorUtils.find(doc, "//mms:function");
            for (int i = 0; i < functionList.getLength(); i++) {
                boolean isMacro = CodeGeneratorUtils.isMacroFunction((Element) functionList.item(i));
                //Function name
                Element funcName = (Element) functionList.item(i).getFirstChild();
                //Function arguments and declaration
                String arguments = "";
                ArrayList <String> argList = new ArrayList<String>();
                String argumentsMacro = "";
                String declarations = "";
                LinkedHashMap<String, ArrayList<String>> parameterDeclaration = new LinkedHashMap<String, ArrayList<String>>();  
                ArrayList<String> fieldParams = new ArrayList<String>();
                if (funcName.getNextSibling().getLocalName().equals("functionParameters")) {
                    NodeList args = funcName.getNextSibling().getChildNodes();
                    for (int j = 0; j < args.getLength(); j++) {
                        String paramName = "par" + SAMRAIUtils.variableNormalize(args.item(j).getFirstChild().getTextContent());
                        argList.add(paramName);
                        //Add "par" to all the parameter occurrences inside the function
                        NodeList paramOccurrences = CodeGeneratorUtils.find(functionList.item(i), 
                                ".//mt:ci[text() = '" + args.item(j).getFirstChild().getTextContent() + "']");
                        for (int k = 0; k < paramOccurrences.getLength(); k++) {
                            paramOccurrences.item(k).setTextContent(paramName);
                        }
                        String paramType = args.item(j).getLastChild().getTextContent();
                        ArrayList<String> paramList;
                        if (parameterDeclaration.containsKey(paramType)) {
                            paramList = parameterDeclaration.get(paramType);
                        } 
                        else {
                            paramList = new ArrayList<String>();
                        }
                        paramList.add(paramName);
                        parameterDeclaration.put(paramType, paramList);
                        
                        if (isMacro) {
                            arguments = arguments + ", " + paramName;
                        }
                        else {
                           	String accessor = "";
                        	if (CodeGeneratorUtils.isAssigned(paramName, functionList.item(i).getLastChild())) {
                        		accessor = "&";
                        	}
                            arguments = arguments + ", " + SAMRAIUtils.getParticlesType(paramType, dimensions) + accessor + " " + paramName;
                        }
                        if (paramType.equals("field")) {
                            fieldParams.add(paramName);
                        }
                        else {
                            argumentsMacro = argumentsMacro + paramName + ",";
                        }
                    }
                    arguments = arguments.substring(2) + ", ";
                }
               
                //Declaration of variables
                declarations = declarations
                    + SAMRAIUtils.getPDEVariableDeclaration(functionList.item(i).getLastChild(), pi, 1, argList,
                            "patch->", false, false) + NEWLINE;
                
                //Function instructions
                String instructions = "";
             
                if (isMacro) {
                    params[0][1] = "true";
                    params[1][1] = "0";
                }
                else {
                    params[0][1] = "false";
                    params[1][1] = "1";
                }
                
                Element instructionSet;
                
                //If macro then the parameters has to be inside parenthesis
                if (isMacro) {
                    instructionSet = SAMRAIUtils.preProcessParticlesBlock(functionList.item(i).getLastChild().getFirstChild().cloneNode(true), 
                            fieldParams, pi.getCoordinates(), false, pi);
                    introduceParenthesis(argumentsMacro, instructionSet);
                } 
                else {
                    instructionSet = SAMRAIUtils.preProcessParticlesBlock(functionList.item(i).getLastChild().getFirstChild().cloneNode(true), 
                            fieldParams, pi.getCoordinates(), false, pi);
                }
                
                NodeList instructionList = instructionSet.getChildNodes();
                for (int j = 0; j < instructionList.getLength(); j++) {
                    if (isMacro) {
                        instructions = instructions + SAMRAIUtils.xmlTransform(instructionList.item(j).getFirstChild()
                                .getFirstChild(), params);
                    }
                    else {
                        instructions = instructions + SAMRAIUtils.xmlTransform(instructionList.item(j), 
                                params);
                    }
                }
                
                String functionName = SAMRAIUtils.variableNormalize(funcName.getTextContent());
                if (isMacro) {
                	result = result + "#define " + functionName + "(" + arguments 
                        + "dx, simPlat_dt) (" + instructions + ")" + NEWLINE + NEWLINE;
                }
                else {
                    String function = "double " + functionName + "(" + arguments 
                        + "const double* dx, const double simPlat_dt) {" + NEWLINE
                        + declarations
                        + instructions + NEWLINE
                        + "}" + NEWLINE + NEWLINE;
                    result = result + function;
                }
            }
            return result;
        } 
        catch (Exception e) {
        	e.printStackTrace(System.out);
            throw new CGException(CGException.CG00X, e.getMessage());
        }
    }
    
    /**
     * Introduce parenthesis in the macro functions to avoid precedence operation problems.
     * @param argumentsMacro    The arguments of the macros
     * @param function          The function
     * @throws CGException      CG004 External error
     */
    private static void introduceParenthesis(String argumentsMacro, Element function) throws CGException {
        Document doc = function.getOwnerDocument();
        if (argumentsMacro.length() > 0) {
            argumentsMacro = argumentsMacro.substring(0, argumentsMacro.lastIndexOf(","));
        }
        String[] argumentArray = argumentsMacro.split(",");
        for (int j = 0; j < argumentArray.length; j++) {
            NodeList paramOccurrences = CodeGeneratorUtils.find(function.getLastChild().getFirstChild(), 
                    ".//mt:ci[text() = '" + argumentArray[j] + "']");
            for (int k = paramOccurrences.getLength() - 1; k >= 0; k--) {
                Element parent = (Element) paramOccurrences.item(k).getParentNode();
                Element nothing = CodeGeneratorUtils.createElement(doc, MTURI, "ci");
                Element apply = CodeGeneratorUtils.createElement(doc, MTURI, "apply");
                apply.appendChild(nothing);
                apply.appendChild(paramOccurrences.item(k).cloneNode(true));
                parent.replaceChild(apply, paramOccurrences.item(k));
            }
        }
    }
}
