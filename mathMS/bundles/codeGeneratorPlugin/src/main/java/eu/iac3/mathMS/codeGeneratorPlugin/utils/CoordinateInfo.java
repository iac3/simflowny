/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.codeGeneratorPlugin.utils;

import org.w3c.dom.Element;

/**
 * Information about a spatial coordinate.
 * Its name, minimal value and maximal value in the simulation grid
 * @author bminyano
 *
 */
public class CoordinateInfo {
    private String name;
    private Element min;
    private Element max;
    
    public String getName() {
        return name;
    }
    public Element getMin() {
        return min;
    }
    public Element getMax() {
        return max;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setMin(Element min) {
        this.min = min;
    }
    public void setMax(Element max) {
        this.max = max;
    }
}
