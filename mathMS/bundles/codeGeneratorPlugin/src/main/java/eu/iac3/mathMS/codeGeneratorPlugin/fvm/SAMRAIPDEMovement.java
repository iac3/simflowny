/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.codeGeneratorPlugin.fvm;

import eu.iac3.mathMS.codeGeneratorPlugin.CGException;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.CodeGeneratorUtils;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.ProblemInfo;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.SAMRAIUtils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;

import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Class to generate the movement file.
 * @author bminyano
 *
 */
public final class SAMRAIPDEMovement {

    static final String NL = System.getProperty("line.separator");
    static final String IND = "\u0009";
    static final String MTURI = "http://www.w3.org/1998/Math/MathML";
    static final int THREE = 3;
    static final String MMSURI = "urn:mathms";
    
    /**
     * Private constructor.
     */
    private SAMRAIPDEMovement() { };
    
    /**
     * Create the movement file.
     * @param pi                The problem information
     * @return                  The code for the movement
     * @throws CGException      CG003 Not possible to create code for the platform
     *                          CG004 External error
     */
    public static String create(ProblemInfo pi) throws CGException {
        Document doc = pi.getProblem();
        int dims = pi.getCoordinates().size();

        String index = "";
        String indexPar = "";
        String newIndexPar = "";
        String newIndex = "";
        String newPosition = "";
        String checkMovement = "";
        String newPos = "";
        String lowersAndUppers = "";
        String lasts = "";
        for (int i = 0; i < dims; i++) {
            String coord = pi.getCoordinates().get(i);
            String contCoord = CodeGeneratorUtils.discToCont(doc, coord);
            newPosition = newPosition + IND + "newPosition[" + i + "] = (newPos" + coord + " - " + contCoord + "Glower)/dx[" + i 
                + "];" + NL
                + IND + "int newIndex" + i + " = floor(newPosition[" + i + "] + 1E-10);" + NL;
            checkMovement = checkMovement + IND + "if (newIndex" + i + " > boxlast(" + i + ") || newIndex" + i + " < boxfirst(" + i + ")) {" + NL
                + IND + IND + "out = true;" + NL
                + IND + "}" + NL;
            lowersAndUppers = lowersAndUppers + IND + "int " + contCoord + "lower = patch_geom->getXLower()[" + i + "];" + NL
                    + IND + "int " + contCoord + "upper = patch_geom->getXUpper()[" + i + "];" + NL;
            index = index + "index" + i + ", ";
            indexPar = indexPar + "const int index" + i + ", ";
            newIndexPar = newIndexPar + "const int newIndex" + i + ", ";
            newIndex = newIndex + "newIndex" + i + ", ";

            newPos = newPos + "const double newPos" + coord + ", ";
            lasts =  lasts + IND + "int " + coord + "last = boxlast1(" + i + ")-boxfirst1(" 
                    + i + ") + 2 + 2 * d_ghost_width;" + NL;
        }
        index = index.substring(0, index.length() - 2);
        indexPar = indexPar.substring(0, indexPar.length() - 2);
        newIndexPar = newIndexPar.substring(0, newIndexPar.length() - 2);
        newIndex = newIndex.substring(0, newIndex.length() - 2);
        newPos = newPos.substring(0, newPos.length() - 2);
        
        
        
        return "/*" + NL
            + " * Move the particles if the problem have any" + NL
            + " */" + NL
            + "template<class T>" + NL
            + "void Problem::moveParticles(const std::shared_ptr<hier::Patch > patch, std::shared_ptr< pdat::IndexData<Particles<T>, "
            + "pdat::CellGeometry > > particleVariables, Particles<T>* part, T* particle, " + indexPar 
            + ", " + newPos + ", const double simPlat_dt, const double current_time, int pit, int step, double level_influenceRadius)" + NL
            + "{" + NL
            + SAMRAIUtils.createFOVDeclaration(pi.getRegionIds(), IND, "patch->")
            + IND + "const hier::Index boxfirst1 = patch->getBox().lower();" + NL
            + IND + "const hier::Index boxlast1  = patch->getBox().upper();" + NL
            + IND + "const hier::Index boxfirst = particleVariables->getGhostBox().lower();" + NL
            + IND + "const hier::Index boxlast  = particleVariables->getGhostBox().upper();" + NL + NL
            + IND + "//Get delta spaces into an array. dx, dy, dz." + NL
            + IND + "std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));" + NL
            + IND + "const double* dx  = patch_geom->getDx();" + NL
            + lowersAndUppers
            + lasts
            + IND + "double newPosition[" + dims + "];" + NL + NL
            + IND + "hier::Index idx(" + index + ");" + NL
            + newPosition + NL
            + IND + "bool out = false;" + NL
            + checkMovement
            + IND + "if (!out) {" + NL
            + getReflectionCheck(doc, pi, IND + IND)
            + IND + IND + "hier::Index newIdx(" + newIndex + ");" + NL
            + IND + IND + "if (!(newIdx == idx)) {" + NL
            + changeCell(pi, index, newIndex)
            + IND + IND + "}" + NL
            + IND + "} else {" + NL    
            + IND + IND + "part->deleteParticle(*particle);" + NL
            + IND + "}" + NL
            + "}" + NL
            + "#moveDeclarations#" + NL
            + "template<class T>" + NL
            + "void moveParticles(const std::shared_ptr<hier::Patch > patch, std::shared_ptr< pdat::IndexData<Particles<T>, pdat::CellGeometry > > "
            + "particleVariables, Particles<T>* part, T* particle, " + indexPar 
            + ", " + newPos + ", const double simPlat_dt, const double current_time, int pit, int step, double level_influenceRadius);" + NL
            + checkRegion(pi.getProblem(), pi.getCoordinates(), pi.getRegions(), newIndexPar, indexPar);
    }
    
    /**
     * Checks if there is any boundary that implies a region change when a particle comes in or goes out.
     * @param doc               The problem
     * @return                  True if a region checking should be done after a movement 
     * @throws CGException      CG004 External error
     */
    private static boolean containsRegionChangingBoundary(Document doc) throws CGException {
        String query = "/*/mms:boundaryConditions/mms:boundaryPolicy/mms:boundaryCondition[descendant::mms:type/mms:algebraic or "
                + "descendant::mms:type/mms:static or descendant::mms:type/mms:maximalDissipation]";
        return CodeGeneratorUtils.find(doc, query).getLength() > 0;
    }
    
    /**
     * Create the code for the checkRegion routine.
     * This routine has to check if a particle inside a boundary has gone out inside the interior and assigns the interior region.
     * @param doc               The problem
     * @param coords            The spatial coordinates
     * @param regionGroup      The list of regionGroup names
     * @param newIndexPar       new index parameters
     * @param indexPar          index parameters
     * @return                  The code
     * @throws CGException      CG004 External error
     */
    private static String checkRegion(Document doc, ArrayList<String> coords, ArrayList<String> regionGroup, String newIndexPar, 
            String indexPar) throws CGException {
        String result = "";
        String[][] xslParams = new String [THREE][2];
        xslParams[0][0] =  "condition";
        xslParams[0][1] =  "true";
        xslParams[1][0] =  "indent";
        xslParams[1][1] =  "0";
        xslParams[2][0] =  "dimensions";
        xslParams[2][1] =  String.valueOf(coords.size());
        
        for (int i = regionGroup.size() - 1; i >= 0; i--) {
            String regionName = regionGroup.get(i);
            NodeList boundaries = CodeGeneratorUtils.find(doc, "/*/mms:boundaryConditions/mms:boundaryPolicy/mms:boundaryCondition[ancestor::" 
                    + "mms:boundaryPolicy[descendant::mms:regionName = '" + regionName + "']]");
            for (int j = 0; j < boundaries.getLength(); j++) {
                String type = boundaries.item(j).getFirstChild().getFirstChild().getLocalName();
                if (type.equals("algebraic") || type.equals("static")
                        || type.equals("maximalDissipation") || type.equals("non-periodical")) {
                    String axis = boundaries.item(j).getFirstChild().getNextSibling().getTextContent();
                    String side = boundaries.item(j).getFirstChild().getNextSibling().getNextSibling().getTextContent();
                    String startCondition = "";
                    String endCondition = "";
                    String indentCondition = IND + IND;
                    //condition of the boundary
                    if (((Element) boundaries.item(j)).getElementsByTagNameNS(MMSURI, "applyIf").getLength() > 0) {
                        Element condition = (Element) ((Element) boundaries.item(j)).getElementsByTagNameNS(MMSURI, "applyIf").item(0);
                        startCondition = IND + IND + "if (" + SAMRAIUtils.xmlTransform(
                                condition.getFirstChild(), xslParams) + ") {" + NL;
                        endCondition = IND + IND + "}" + NL;
                        indentCondition = IND + IND + IND;
                    }
                    for (int k = 0; k < coords.size(); k++) {
                        String coord = coords.get(k);
                        if (axis.toLowerCase().equals("all") || coord.equals(CodeGeneratorUtils.contToDisc(doc, axis))) {
                            if (side.toLowerCase().equals("all") || side.toLowerCase().equals("lower")) {
                                result = result + IND + "if (index" + k + " < boxfirst1(" + k + ") && newIndex" + k + " >= boxfirst1(" + k 
                                        + ") && particle->region == -" + (2 * k + 1) + ") {" + NL
                                        + startCondition
                                        + indentCondition + "particle->region = particle->newRegion;" + NL
                                        + endCondition
                                        + IND + "}" + NL;
                                if (type.equals("maximalDissipation")) {
                                    result = result + IND + "if (index" + k + " >= boxfirst1(" + k + ") && newIndex" + k + " < boxfirst1(" + k 
                                            + ") && (particle->region == " + (2 * i + 1) + " || particle->region == " + (2 * i + 2) + ")) {" + NL
                                            + startCondition
                                            + indentCondition + "particle->region = -" + (2 * k + 1) + ";" + NL
                                            + endCondition
                                            + IND + "}" + NL;
                                }
                            }
                            if (side.toLowerCase().equals("all") || side.toLowerCase().equals("upper")) {
                                result = result + IND + "if (index" + k + " > boxlast1(" + k + ") && newIndex" + k + " <= boxlast1(" + k 
                                        + ") && particle->region == -" + (2 * k + 2) + ") {" + NL
                                        + startCondition
                                        + indentCondition + "particle->region = particle->newRegion;" + NL
                                        + endCondition
                                        + IND + "}" + NL;
                                if (type.equals("maximalDissipation")) {
                                    result = result + IND + "if (index" + k + " <= boxlast1(" + k + ") && newIndex" + k + " > boxlast1(" + k 
                                            + ") && (particle->region == " + (2 * i + 1) + " || particle->region == " + (2 * i + 2) + ")) {" + NL
                                            + startCondition
                                            + indentCondition + "particle->region = -" + (2 * k + 2) + ";" + NL
                                            + endCondition
                                            + IND + "}" + NL;
                                }
                            }
                        }
                    }
                }
            }
        }
        if (!result.equals("")) {
            result = "#checkRegion#" + NL
            	+ "template<class T>" + NL
                + "void Problem::checkRegion(T* particle, " + newIndexPar + ", " + indexPar 
                + ", const hier::Index boxfirst1, const hier::Index boxlast1) {" + NL
                + result
                + "}";
        }
        return result;
    }
    
    /**
     * Creates the code to change the cell in particle movement.
     * If an internal boundary enters a static or fixed boundary it must disappear.
     * If an static, maximal dissipation or static boundary [INFLOW condition] enters the problem a new particle must
     * be created in the boundary.
     * @param pi                The problem information
     * @param index             The index string
     * @param newIndex          The new index string
     * @return                  The code
     * @throws CGException      CG004 External error
     */
    private static String changeCell(ProblemInfo pi, String index, String newIndex) throws CGException {
        Document doc = pi.getProblem();
        
        String checkRegionCall = "";
        if (containsRegionChangingBoundary(doc)) {
            checkRegionCall = checkRegionCall + IND + IND + IND + IND 
                + "//Check the region for particle moving outside or inside the boundary" + NL
                + IND + IND + IND + IND + "checkRegion(particle, " + newIndex + ", " + index + ", boxfirst1, boxlast1);" + NL;
        }
        
        return IND + IND + IND + "bool deleted = false;" + NL
            + createExitInflowBoundaryCond(pi)
            + IND + IND + IND + "if (!deleted) {" + NL
            + checkRegionCall
            + IND + IND + IND + IND + "//Need to create copy of the particle. Erase an item from a vector calls the destructor " 
            + "of the object" + NL
            + IND + IND + IND + IND + "T* newParticle = new T(*particle);" + NL
            + IND + IND + IND + IND + "part->deleteParticle(*particle);" + NL
            + IND + IND + IND + IND + "Particles<T>* destPart = particleVariables->getItem(newIdx);" + NL
            + IND + IND + IND + IND + "destPart->addParticle(*newParticle);" + NL
            + IND + IND + IND + IND + "delete newParticle;" + NL
            + IND + IND + IND + "}" + NL;
    }
    
    /**
     * If there is any reflection condition the particle entering this condition must store their sign
     * and change it.
     * @param doc               The problem
     * @param pi                The problem information
     * @param actualInd			The actual indentation
     * @return                  The code
     * @throws CGException      CG004 External error
     */
    private static String getReflectionCheck(Document doc, ProblemInfo pi, String actualInd) throws CGException {
        String result = "";
        ArrayList<String> coords = pi.getCoordinates();
        int dims = pi.getDimensions();
        
        //Parameters for the xsl transformation. It is not a condition formula
        String[][] xslParams = new String [THREE][2];
        xslParams[0][0] =  "condition";
        xslParams[0][1] =  "false";
        xslParams[1][0] =  "indent";
        xslParams[1][1] =  "0";
        xslParams[2][0] =  "dimensions";
        xslParams[2][1] =  String.valueOf(pi.getDimensions());
        
        String index = "";
        for (int i = 0; i < dims; i++) {
            index = index + "index" + i + " - boxfirst(" + i + "), ";
        }
        index = index.substring(0, index.length() - 2);
        
        String boundaryCrossing = "";
        ArrayList<String> regionIds = pi.getRegionIds();
        for (int i = 0; i < regionIds.size(); i++) {
            String regId = regionIds.get(i);
            String newIndex = "";
            if (regId.contains("Lower") || regId.contains("Upper")) {
                for (int k = 0; k < coords.size(); k++) {
                    String coord = pi.getContCoordinates().get(k);
                    if (regId.substring(0,  1).equals(coord) && regId.contains("Upper")) {
                        newIndex = newIndex + "newIndex" + k + " - boxfirst(" + k + ") + 1, ";
                    }
                    else {
                    	newIndex = newIndex + "newIndex" + k + " - boxfirst(" + k + "), ";
                    }
                }
                newIndex = newIndex.substring(0, newIndex.length() - 2);
            	boundaryCrossing = boundaryCrossing + "vector(FOV_" + regId + ", " + newIndex + ") > 0 || ";
            }
        }
        boundaryCrossing = boundaryCrossing.substring(0, boundaryCrossing.lastIndexOf("||"));
        
        for (int i = pi.getRegions().size() - 1; i >= 0; i--) {
            String regionName = pi.getRegions().get(i);
            if (CodeGeneratorUtils.find(doc, "/*/mms:boundaryConditions//mms:boundaryPolicy[descendant::mms:regionName = '" 
                    + regionName + "']/mms:boundaryCondition[mms:type/mms:reflection]").getLength() > 0) {
                for (String speciesName: pi.getParticleSpecies()) {
                    String enteringReflection = "";
                	//Condition in the reflection
                    NodeList reflectionBoundaries = CodeGeneratorUtils.find(doc, "/*/mms:boundaryConditions//mms:boundaryPolicy[descendant::" 
                            + "mms:regionName = '" + regionName + "']/mms:boundaryCondition[mms:type/mms:reflection]");
                    for (int j = 0; j < reflectionBoundaries.getLength(); j++) {
                        Element reflection = (Element) reflectionBoundaries.item(j);
                        String axis = reflection.getElementsByTagNameNS(MMSURI, "axis").item(0).getTextContent();
                        String side = reflection.getElementsByTagNameNS(MMSURI, "side").item(0).getTextContent();
                        //Iterate over the negative field reflections, apply sign changes and store the initial sign.
                        for (int k = 0; k < coords.size(); k++) {
                            String coord = coords.get(k);
                            if (axis.toLowerCase().equals("all") || coord.equals(CodeGeneratorUtils.contToDisc(doc, axis))) {
                                if (side.toLowerCase().equals("all") || side.toLowerCase().equals("lower")) {
                                    String signChanges = "";
                                    NodeList boundaries = CodeGeneratorUtils.find(doc, "//sml:boundary[ancestor::mms:region[mms:name = '" + regionName 
                                            + "'] or ancestor::mms:subregion[mms:name = '" + regionName 
                                            + "']]//sml:axis[@axisAtt = '" + CodeGeneratorUtils.contToDisc(doc, axis) + "']" 
                                            + "/sml:side[@sideAtt = 'lower']");
                                    for (int m = 0; m < boundaries.getLength(); m++) {
                                    	Element boundary = (Element) SAMRAIUtils.preProcessPDEBlock(boundaries.item(m).cloneNode(true), pi, false, false);
                                        NodeList insts = CodeGeneratorUtils.find(boundary, "./sml:iterateOverParticles[@speciesNameAtt = '" + speciesName + "']/mt:math[@sml:type = 'reflection']");
                                        xslParams[1][1] = String.valueOf(actualInd.length() + 3);
                                        if (insts.getLength() > 0) {
                                	        Element tmp = (Element) boundaries.item(m).getParentNode();
                                	        while (!tmp.getLocalName().equals("boundary")) {
                                	        	tmp = (Element) tmp.getParentNode();
                                	        }
                                	        int counter = 0;
                                	        while (tmp.getPreviousSibling() != null) {
                                	        	tmp = (Element) tmp.getPreviousSibling();
                                	        	if (tmp.getLocalName().equals("iterateOverCells") && CodeGeneratorUtils.find(tmp, ".//mt:ci[starts-with(text(), 'moveParticles')]").getLength() > 0) {
                                	        		counter++;
                                	        	}
                                	        }
                                        	signChanges = signChanges + actualInd + IND + IND + "if (step == " + counter + ") {" + NL;
                                        }
                                        for (int l = 0; l < insts.getLength(); l++) {
                                            signChanges = signChanges + SAMRAIUtils.xmlTransform(insts.item(l), xslParams);
                                        }
                                        if (insts.getLength() > 0) {
                                        	signChanges = signChanges + actualInd + IND + IND + "}" + NL;
                                        }
                                    }

                                    enteringReflection = enteringReflection + actualInd + IND + "if (newIndex" + k + " < boxfirst1(" + k 
                                            + ") && patch->getPatchGeometry()->getTouchesRegularBoundary(" + k + ", 0)) {" + NL
                                        + signChanges
                                        + actualInd + IND + "}" + NL;
                                }
                                if (side.toLowerCase().equals("all") || side.toLowerCase().equals("upper")) {
                                    String signChanges = "";
                                    NodeList boundaries = CodeGeneratorUtils.find(doc, "//sml:boundary[ancestor::mms:region[mms:name = '" + regionName 
                                            + "'] or ancestor::mms:subregion[mms:name = '" + regionName 
                                            + "']]//sml:axis[@axisAtt = '" + CodeGeneratorUtils.contToDisc(doc, axis) + "']" 
                                            + "/sml:side[@sideAtt = 'upper']");
                                    for (int m = 0; m < boundaries.getLength(); m++) {
                                    	Element boundary = (Element) SAMRAIUtils.preProcessPDEBlock(boundaries.item(m).cloneNode(true), pi, false, false);
                                        NodeList insts = CodeGeneratorUtils.find(boundary, "./sml:iterateOverParticles[@speciesNameAtt = '" + speciesName + "']/mt:math[@sml:type = 'reflection']");
                                        xslParams[1][1] = String.valueOf(actualInd.length() + 3);
                                        if (insts.getLength() > 0) {
                                	        Element tmp = (Element) boundaries.item(m).getParentNode();
                                	        while (!tmp.getLocalName().equals("boundary")) {
                                	        	tmp = (Element) tmp.getParentNode();
                                	        }
                                	        int counter = 0;
                                	        
                                	        while (tmp.getPreviousSibling() != null) {
                                	        	tmp = (Element) tmp.getPreviousSibling();
                                   	        	if (tmp.getLocalName().equals("iterateOverCells") && CodeGeneratorUtils.find(tmp, ".//mt:ci[starts-with(text(), 'moveParticles')]").getLength() > 0) {
                                	        		counter++;
                                	        	}
                                	        }
                                        	signChanges = signChanges + actualInd + IND + IND + "if (step == " + counter + ") {" + NL;
                                        }
                                        for (int l = 0; l < insts.getLength(); l++) {
                                            signChanges = signChanges + SAMRAIUtils.xmlTransform(insts.item(l), xslParams);
                                        }
                                        if (insts.getLength() > 0) {
                                        	signChanges = signChanges + actualInd + IND + IND + "}" + NL;
                                        }
                                    }
                                    
                                    enteringReflection = enteringReflection + actualInd + IND + "if (newIndex" + k + " > boxlast1(" + k 
                                            + ") - 1 && patch->getPatchGeometry()->getTouchesRegularBoundary(" + k + ", 1)) {" + NL
                                            
                                        + signChanges
                                        + actualInd + IND + "}" + NL;
                                }
                            }
                        }
                    }
                    
                    if (!enteringReflection.equals("")) {
                        result = result + actualInd + "if (std::is_same<T, Particle_" + speciesName + ">::value) {" + NL
                        	+ actualInd + IND + "std::shared_ptr< pdat::IndexData<Particles<Particle_" +  speciesName + ">, pdat::CellGeometry > > particleVariables_" +  speciesName + "(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles<Particle_" +  speciesName + ">, pdat::CellGeometry >, hier::PatchData>(patch->getPatchData(d_particleVariables_" +  speciesName + "_id)));" + NL
                        	+ actualInd + IND + "Particle_" +  speciesName + "* particleTmp = (Particle_" +  speciesName + "*) particle;" + NL
                        	+ actualInd + IND + "if ((particleTmp->region == " + (2 * i + 1) 
                                    + " || particleTmp->region == " + (2 * i + 2) + ") && (" + boundaryCrossing + ")) {" + NL
                            + CodeGeneratorUtils.incrementIndent(enteringReflection.replaceAll("particle->", "particleTmp->"), 1)
                            + actualInd + IND + "}" + NL
                            + actualInd + "}" + NL;
                    }
                }
            }
        }
        if (!result.equals("")) {
            result = actualInd + "//Reflection checkings. The first time a particle came into the reflection" + NL +  result;
        }
        return result;
    }
    
    /**
     * Creates the common code to create inflow particles. 
     * @param doc                   The problem
     * @param pi                    The problem information
     * @param regionName      The region group name
     * @param boundary              The boundary
     * @return                      The code
     * @throws CGException          CG004 External error
     */
    private static String createCommonCreate(Document doc, ProblemInfo pi, String regionName, 
            Node boundary, String speciesName) throws CGException {
        int dims = pi.getDimensions();
        String indexNP = "";
        String cond = "";
        for (int j = 0; j < dims; j++) {
            cond = cond + " && indexNP" + j + " >= boxfirst(" + j + ") && indexNP" + j + " <= boxlast(" + j + ")";
            indexNP = indexNP + ", indexNP" + j;
        }
        indexNP = indexNP.substring(2);
        cond = cond.substring(" && ".length());
        //New particle initializations
        DocumentFragment scope = doc.createDocumentFragment();
        String query = "/*/mms:boundaryConditions/mms:boundaryPolicy/mms:boundaryCondition[(mms:type/mms:static or mms:type/mms:algebraic) and ancestor::" 
                    + "mms:boundaryPolicy[descendant::mms:regionName = '" + regionName + "']]";
        NodeList scopes = CodeGeneratorUtils.find(doc, query);
        for (int k = 0; k < scopes.getLength(); k++) {
            scope.appendChild(scopes.item(k).cloneNode(true));
        }
        String declarations = SAMRAIUtils.getPDELocalVariableDeclaration(scope, pi, 5, new ArrayList<String>()) + NL;
        String initializations = IND + IND + IND + IND + IND + "//particleNP initializations" + NL;
        for (int j = 0; j < dims; j++) {
            String staticInit = "";
            if (CodeGeneratorUtils.find(doc, "/*/mms:boundaryConditions/mms:boundaryPolicy/mms:boundaryCondition[mms:type/mms:static and ancestor::" 
                    + "mms:boundaryPolicy[descendant::mms:regionName = '" + regionName + "']]").getLength() > 0) {
                staticInit = getStaticInitialization(doc, regionName, pi, speciesName);
            }
            String algebraicInit = "";
            if (CodeGeneratorUtils.find(doc, "/*/mms:boundaryConditions/mms:boundaryPolicy/mms:boundaryCondition[mms:type/mms:algebraic and ancestor::" 
                    + "mms:boundaryPolicy[descendant::mms:regionName = '" + regionName + "']]").getLength() > 0) {
                algebraicInit = getAlgebraicInitialization(boundary, regionName, pi, speciesName);
            }
      
            initializations = initializations + IND + IND + IND + IND + IND + "if (indexNP" + j + " < boxfirst1(" + j 
                + ") || indexNP" + j + " > boxlast1(" + j + ")) {" + NL
                + staticInit
                + algebraicInit
                + IND + IND + IND + IND + IND + "}" + NL;
        }
        return IND + IND + IND + IND + "if (" + cond + ") {" + NL
        	+ declarations
            + initializations
            + IND + IND + IND + IND + IND + "hier::Index idxNP(" + indexNP + ");" + NL
            + IND + IND + IND + IND + IND + "Particles<Particle_" + speciesName + ">* destPart = particleVariables_" +  speciesName + "->getItem(idxNP);" + NL
            + IND + IND + IND + IND + IND + "destPart->addParticle(*particleNP);" + NL
            + IND + IND + IND + IND + IND + "pit++;" + NL
            + IND + IND + IND + IND + "}" + NL
            + IND + IND + IND + IND + "delete particleNP;" + NL;
    }
    
    /**
     * Creates the code to check if a particle has trespassed the cell in one dimension.
     * @param doc                   The problem
     * @param coords                The spatial coordinates
     * @param commonCreate          A common code used in all the trespassed checkings
     * @param axis                  The boundary axis
     * @param side                  The boundary side
     * @param timeCoord             The time coordinate
     * @param pi					Problem information
     * @return                      The code
     * @throws CGException          CG004 External error
     */
    private static String create1DimBoundTrespassing(Document doc, ArrayList<String> coords, String timeCoord, String commonCreate, 
    		String axis, String side, ProblemInfo pi, String speciesName) throws CGException {
        String createParticle = "";
        int dims = coords.size();
        String positions = "";
        String varDeclaration = "";
        for (int j = 0; j < dims; j++) {
            if (axis.toLowerCase().equals("all") || CodeGeneratorUtils.contToDisc(doc, axis).equals(coords.get(j))) {
                String posNP = "";
                //Take the actual position
                varDeclaration = "";
                for (int k = 0; k < dims; k++) {
                    String cont = CodeGeneratorUtils.discToCont(doc, coords.get(k));
                    ArrayList<String> posVars = pi.getVariableInfo().getPositionVariables(speciesName);
                    String posVarCode = "";
                    String negVarCode = "";
                    String varCode = "";
                    for (int l = 0; l < posVars.size(); l++) {
                        String position = posVars.get(l);
                        if (position.contains("position" + cont)) {
	                        posVarCode = posVarCode + IND + IND + IND + IND + IND + IND + "particleNP->" + position 
	                            + " = particle->" + position + " + d_ghost_width * particleSeparation_" + cont + ";" + NL;
	                        negVarCode = negVarCode + IND + IND + IND + IND + IND + IND + "particleNP->" + position 
	                            + " = particle->" + position + " - d_ghost_width * particleSeparation_" + cont + ";" + NL;
	                        varCode = varCode + IND + IND + IND + IND + IND + "particleNP->" + position 
	                            + " = particle->" + position + ";" + NL;
                        }
                    }
                    if (j == k) {
                        posNP = posNP + IND + IND + IND + IND + IND + "if (index" + k + " > boxlast1(" + k + ") && newIndex" + k 
                            + " <= boxlast1(" + k + ")) {" + NL
                            + IND + IND + IND + IND + IND + IND + "posNP[" + k + "] = newPosition[" + k + "] + d_ghost_width;" + NL
                            + posVarCode
                            + IND + IND + IND + IND + IND + "} else {" + NL
                            + IND + IND + IND + IND + IND + IND + "posNP[" + k + "] = newPosition[" + k + "] - d_ghost_width;" + NL
                            + negVarCode
                            + IND + IND + IND + IND + IND + "}" + NL;
                    }
                    else {
                        posNP = posNP + IND + IND + IND + IND + IND + "posNP[" + k + "] = newPosition[" + k + "];" + NL
                            + varCode;
                    }
                    posNP = posNP + IND + IND + IND + IND + IND + "indexNP" + k + " = floor(posNP[" + k + "] + 1E-10);" + NL
                        + IND + IND + IND + IND + IND + "posNP[" + k + "] = posNP[" + k + "] * dx[" + k + "] + " + cont + "lower;" + NL;
                    varDeclaration = varDeclaration + IND + IND + IND + IND + "int indexNP" + k + ";" + NL;
                }
                
                String condition = "";
                int id = 2 * j + 1;
                if (side.toLowerCase().equals("lower") || side.toLowerCase().equals("all")) {
                    condition = condition + "(particle->region == -" + id + " && index" + j + " < boxfirst1(" + j + ") && newIndex" + j 
                            + " >= boxfirst1(" + j + ")) || ";
                }
                if (side.toLowerCase().equals("upper") || side.toLowerCase().equals("all")) {
                    condition = condition + "(particle->region == -" + (id + 1) + " && index" + j + " > boxlast1(" + j + ") && newIndex" 
                            + j + " <= boxlast1(" + j + ")) || ";  
                }
                condition = condition.substring(0, condition.lastIndexOf(" ||"));
                
                positions = positions + IND + IND + IND + IND + "//" + CodeGeneratorUtils.discToCont(doc, coords.get(j)) 
                    + " boundary exceeded" + NL
                    + IND + IND + IND + IND + "if (" + condition + ") {" + NL
                    + posNP
                    + IND + IND + IND + IND + "}" + NL;
            }
        }
        createParticle = createParticle
                + IND + IND + IND + IND + "Particle_" +  speciesName + "* particleNP = new Particle_" +  speciesName + "(particle->region);" + NL
                + IND + IND + IND + IND + "std::shared_ptr< pdat::IndexData<Particles<Particle_" +  speciesName + ">, pdat::CellGeometry > > particleVariables_" +  speciesName + "(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles<Particle_" +  speciesName + ">, pdat::CellGeometry >, hier::PatchData>(patch->getPatchData(d_particleVariables_" +  speciesName + "_id)));" + NL
                + varDeclaration
                + positions
                + commonCreate;
        return createParticle;
    }
    
    /**
     * Generate the code to initialize the new particles within static boundaries.
     * @param doc               The problem
     * @param regionName       The region group name
     * @param pi                The process information
     * @return                  The initializations
     * @throws CGException      CG004 External error
     */
    private static String getStaticInitialization(Document doc, String regionName, ProblemInfo pi, String speciesName) throws CGException {
        String currIndent = IND + IND + IND + IND + IND + IND;
        String result = "";
        //Parameters for the xsl transformation. It is not a condition formula
        String[][] xslParams = new String [THREE][2];
        xslParams[0][0] = "condition";
        xslParams[0][1] = "false";
        xslParams[1][0] = "indent";
        xslParams[1][1] = "0";
        xslParams[2][0] = "dimensions";
        xslParams[2][1] = String.valueOf(pi.getDimensions());
        
        //Static new particles have to take their values from boundary initialization.
        NodeList initConditions = CodeGeneratorUtils.find(doc, "/*/mms:boundaryConditions/mms:boundaryPolicy/mms:boundaryCondition[mms:type/mms:static and ancestor::" 
                    + "mms:boundaryPolicy[descendant::mms:regionName = '" + regionName + "']]//mms:initialCondition//sml:iterateOverParticles[@speciesNameAtt = '" + speciesName + "']/*");
        for (int j = 0; j < initConditions.getLength(); j++) {
        	Node condition = initConditions.item(j).cloneNode(true);
	        xslParams[0][1] = "false";
	        xslParams[1][1] = String.valueOf(currIndent.length());
            result = result + SAMRAIUtils.xmlTransform(SAMRAIUtils.preProcessParticlesBlock(setNPPosition(
            		decrementTimeStep(condition.cloneNode(true), pi), pi.getCoordinates()), pi.getVariables(), pi.getCoordinates(), false,
                pi), xslParams).
                replaceAll("particle->", "particleNP->");
        }
        //Static assignment for all the auxiliary variables
        Iterator<String> fieldEnum = pi.getFieldTimeLevels().keySet().iterator();
        ArrayList<String> candidates = new ArrayList<String>();
        while (fieldEnum.hasNext()) {
            String field = fieldEnum.next();
            NodeList leftTerms = CodeGeneratorUtils.find(doc, 
                ".//mt:math[(not(parent::sml:if) or parent::sml:then) and (not(parent::sml:while) or parent::sml:loop)" 
                + " and not(parent::sml:return) and (ancestor::sml:boundary) and (ancestor::mms:region[mms:name = '" 
                + regionName + "'] or ancestor::mms:subregion[mms:name = '" + regionName 
                + "'])]/mt:apply/mt:eq[not(following-sibling::*[1][name()='mt:apply'] = "
                + "preceding-sibling::mt:math/mt:apply/mt:eq/following-sibling::*[1][name()='mt:apply' or name()='sml:sharedVariable'])]"
                + "/following-sibling::*[1][(name()='mt:apply' or name()='sml:sharedVariable') and (descendant::mt:ci[text() = '" 
                + field + "' or ends-with(text(), '" + field 
                + "') or (matches(text(), '" + field + "_.*') and starts-with(text(), '" + field + "'))])]");
            for (int i = 0; i < leftTerms.getLength(); i++) {
                String variable;
                if (leftTerms.item(i).getLocalName().equals("sharedVariable")) {
                    variable = new String(SAMRAIUtils.xmlTransform(leftTerms.item(i).getFirstChild().getFirstChild(), null));
                }
                else {
                    variable = new String(SAMRAIUtils.xmlTransform(leftTerms.item(i).getFirstChild(), null));
                }
                if (pi.getVariableInfo().getParticleVariables().get(speciesName).contains(variable) && !candidates.contains(variable)) {
                    candidates.add(variable);
    	            if (!CodeGeneratorUtils.isAuxiliaryField(pi, SAMRAIUtils.variableNormalize(field))) {
	                    result = result + currIndent + "particleNP->" + variable + " = particleNP->" 
	                            + SAMRAIUtils.variableNormalize(field) + "_p;" + NL;
    	            }
                }   
            }
        }
        result = result + currIndent + "particleNP->newRegion = particle->newRegion;" + NL;
        return result;
    }
    
    /**
     * Generate the code to initialize the new particles within a algebraic boundary.
     * @param boundary          The boundary of the region group
     * @param regionName       The region group name
     * @param pi                The process information
     * @return                  The initializations
     * @throws CGException      CG004 External error
     */
    private static String getAlgebraicInitialization(Node boundary, String regionName, ProblemInfo pi, String speciesName) throws CGException {
        Document doc = pi.getProblem();
        String currIndent = IND + IND + IND + IND + IND + IND;
        String result = "";
        //Parameters for the xsl transformation. It is not a condition formula
        String[][] xslParams = new String [THREE][2];
        xslParams[0][0] = "condition";
        xslParams[0][1] = "false";
        xslParams[1][0] = "indent";
        xslParams[1][1] = String.valueOf(currIndent.length());
        xslParams[2][0] = "dimensions";
        xslParams[2][1] = String.valueOf(pi.getDimensions());
   
        //fixed assignment variables
        Iterator<String> fieldEnum = pi.getFieldTimeLevels().keySet().iterator();
        LinkedHashMap<String, ArrayList<String>> candidates = new LinkedHashMap<String, ArrayList<String>>();
        while (fieldEnum.hasNext()) {
            String field = fieldEnum.next();
            if (pi.getVariableInfo().getParticleVariables().get(speciesName).contains(field)) {
	            String fieldNorm = SAMRAIUtils.variableNormalize(field);
	            ArrayList<String> vars;
	            if (candidates.containsKey(fieldNorm)) {
	                vars = candidates.get(fieldNorm);
	            }
	            else {
	                vars = new ArrayList<String>();
	            }
	            NodeList leftTerms = CodeGeneratorUtils.find(doc, 
	                ".//mt:math[(not(parent::sml:if) or parent::sml:then) and (not(parent::sml:while) or parent::sml:loop)"
	                + " and not(parent::sml:return) and (ancestor::sml:boundary) and (ancestor::mms:region[mms:name = '" 
	                + regionName + "'] or ancestor::mms:subregion[mms:name = '" + regionName 
	                + "'])]/mt:apply/mt:eq[not(following-sibling::*[1][name()='mt:apply'] = "
	                + "preceding-sibling::mt:math/mt:apply/mt:eq/following-sibling::*[1][name()='mt:apply' or name()='sml:sharedVariable'])]"
	                + "/following-sibling::*[1][(name()='mt:apply' or name()='sml:sharedVariable') and (descendant::mt:ci[text() = '" 
	                + field + "' or (matches(text(), '" + field + "._*') and starts-with(text(), '" + field + "'))])]");
	            for (int i = 0; i < leftTerms.getLength(); i++) {
	                String variable;
	                if (leftTerms.item(i).getLocalName().equals("sharedVariable")) {
	                    variable = new String(SAMRAIUtils.xmlTransform(leftTerms.item(i).getFirstChild().getFirstChild(), null));
	                }
	                else {
	                    variable = new String(SAMRAIUtils.xmlTransform(leftTerms.item(i).getFirstChild(), null));
	                }
	                if (!vars.contains(variable)) {
	                    vars.add(variable);
	                }   
	            }
	            String variable = fieldNorm;
	            if (!CodeGeneratorUtils.isAuxiliaryField(pi, SAMRAIUtils.variableNormalize(field))) {
		            for (int i = 1; i < pi.getFieldTimeLevels().get(field); i++) {
		                variable = variable + "_p";
		                if (!vars.contains(variable)) {
		                    vars.add(variable);
		                }  
		            }
	            }
	            candidates.put(fieldNorm, vars);
            }
        }
        //Initial variables not actualized during time steps (mass, etc.)
        NodeList initConditions = CodeGeneratorUtils.find(doc, ".//mms:initialCondition[ancestor::mms:region[" 
            + "mms:name = '" + regionName + "'] or ancestor::mms:subregion[mms:name = '" + regionName 
            + "']]//sml:iterateOverParticles[@speciesNameAtt = '" + speciesName + "']//mt:math/mt:apply/*[preceding-sibling::sml:setFieldValue and position() = 2]");
        String initVars = "";
        for (int j = 0; j < initConditions.getLength(); j++) {
            String variable = new String(SAMRAIUtils.xmlTransform(initConditions.item(j), null));
            if (pi.getVariableInfo().getParticleVariables().get(speciesName).contains(variable) && !candidates.containsKey(variable)) {
                initVars = initVars + SAMRAIUtils.xmlTransform(SAMRAIUtils.preProcessParticlesBlock(setNPPosition(
                		decrementTimeStep(initConditions.item(j).getParentNode().getParentNode().cloneNode(true), pi)
                		, pi.getCoordinates()), pi.getVariables(), pi.getCoordinates(), false, pi), xslParams
                        ).replaceAll("particle->", "particleNP->");
            }
        }
        
        //Fixed new particles have to take their values from boundary definition.
        String type = boundary.getFirstChild().getFirstChild().getLocalName();
        if (type.toLowerCase().equals("algebraic")) {
            //condition
            boolean cond = false;
            if (CodeGeneratorUtils.find(boundary, "./mms:applyIf").getLength() == 1) {
                Node condition = setNPPosition(CodeGeneratorUtils.find(boundary, "./mms:applyIf").item(0).getFirstChild(), pi.getCoordinates());
                xslParams[0][1] = "true";
                result = result + currIndent + "if (" + SAMRAIUtils.xmlTransform(SAMRAIUtils.preProcessParticlesBlock(
                		CodeGeneratorUtils.replaceFields(doc, setNPPosition(condition.cloneNode(true), pi.getCoordinates()), 
                		pi.getCoordinates()), pi.getVariables(), pi.getCoordinates(), false, pi), xslParams
                		).replaceAll("particle->", "particleNP->") + ") {" + NL;
                currIndent = currIndent + IND;
                cond = true;
            }
            xslParams[0][1] = "false";
            xslParams[1][1] = "0";
            //algebraic values
            NodeList algebraicValues = CodeGeneratorUtils.find(boundary, "./mms:type/mms:algebraic/mms:expression");
            for (int k = 0; k < algebraicValues.getLength(); k++) {
                String field = SAMRAIUtils.variableNormalize(algebraicValues.item(k).getFirstChild().getTextContent());
                ArrayList<String> vars = candidates.get(field);
                for (int l = 0; l < vars.size(); l++) {
                    result = result + currIndent + "particleNP->" + vars.get(l) + " = " + SAMRAIUtils.xmlTransform(
                        SAMRAIUtils.preProcessParticlesBlock(setNPPosition(CodeGeneratorUtils.replaceFields(doc, 
                        		algebraicValues.item(k).getLastChild().cloneNode(true), pi.getCoordinates()), 
                        		pi.getCoordinates()), pi.getVariables(), pi.getCoordinates(), false, pi), xslParams
                        		).replaceAll("particle->", "particleNP->");
                }
            }
            result = result + currIndent + "particleNP->newRegion = particle->newRegion;" + NL;
            //Close if clause if exists
            if (cond) {
                currIndent = currIndent.substring(0, currIndent.lastIndexOf(IND));
                result = result + currIndent + "}" + NL;
            }
        }
        return result + initVars;
    }
    
    /**
     * Decrement the time step of the variables.
     * @param element               The element where to look for time variables
     * @param pi					ProblemInfo
     * @return                      The modified element
     * @throws CGException          CG004 External error
     */
    private static Element decrementTimeStep(Node element, ProblemInfo pi) throws CGException {
        String time = CodeGeneratorUtils.find(element.getOwnerDocument(), "/*/mms:coordinates/" 
            + "mms:timeCoordinate").item(0).getTextContent();
        NodeList timeSteps = CodeGeneratorUtils.find(element, ".//mt:ci[text() = '(" + time + ")' and (ancestor::mt:msup or ancestor::mt:msubsup)]");
        for (int i = timeSteps.getLength() - 1; i >= 0; i--) {
        	Element parent = (Element) timeSteps.item(i).getParentNode();
        	if (parent.getLocalName().equals("apply")) {
        		parent = (Element) parent.getParentNode();
        	}
        	String field = parent.getFirstChild().getTextContent();
            if (!CodeGeneratorUtils.isAuxiliaryField(pi, SAMRAIUtils.variableNormalize(field))) {
	            if (timeSteps.item(i).getParentNode().getLocalName().equals("apply")) {
	                //n+1 -> n
	                if (timeSteps.item(i).getParentNode().getFirstChild().getLocalName().equals("plus")) {
	                    timeSteps.item(i).getParentNode().getParentNode().replaceChild(timeSteps.item(i), timeSteps.item(i).getParentNode()); 
	                }
	                //n-x -> n-(x-1)
	                else {
	                    timeSteps.item(i).getNextSibling().setTextContent(
	                            String.valueOf(Integer.valueOf(timeSteps.item(i).getNextSibling().getTextContent()) - 1)); 
	                }
	            }
	            //n -> n-1
	            else {
	                Element apply = CodeGeneratorUtils.createElement(element.getOwnerDocument(), MTURI, "apply");
	                Element minus = CodeGeneratorUtils.createElement(element.getOwnerDocument(), MTURI, "minus");
	                Element cn = CodeGeneratorUtils.createElement(element.getOwnerDocument(), MTURI, "cn");
	                cn.setTextContent("1");
	                apply.appendChild(minus);
	                apply.appendChild(timeSteps.item(i).cloneNode(false));
	                apply.appendChild(cn);
	                timeSteps.item(i).getParentNode().replaceChild(apply.cloneNode(true), timeSteps.item(i));
	            }
            }
        }
        
        return (Element) element;
    }
    
    /**
     * Modify the element to use the position of the new particle.
     * @param instruction       The instruction to modify
     * @param coordinates       The spatial coordinates
     * @return                  The instruction modified
     * @throws CGException      CG004 External error
     */
    private static Element setNPPosition(Node instruction, ArrayList<String> coordinates) throws CGException {
        for (int i = 0; i < coordinates.size(); i++) {
            String coord = coordinates.get(i);
            NodeList positions = CodeGeneratorUtils.find(instruction, ".//mt:ci[not(preceding-sibling::mt:eq and position() != 2) and " 
                    + "child::*[name() = 'mt:msup' or name() = 'mt:msub' or name() = 'mt:msubsup']/*[position() = 1 and name() = 'mt:ci'" 
                    + " and text() = '" + coord + "']]");
            for (int j = positions.getLength() - 1; j >= 0; j--) {
                Element ci = CodeGeneratorUtils.createElement(instruction.getOwnerDocument(), MTURI, "ci");
                ci.setTextContent("newPosition[" + i + "]");
                positions.item(j).getParentNode().replaceChild(ci, positions.item(j));
            }
        }
        return (Element) instruction;
    }
    
    /**
     * Creates the condition to check if a particle has left the inflow boundary.
     * @param pi            The problem info
     * @return              The condition
     * @throws CGException  CG00X External error
     */
    private static String createExitInflowBoundaryCond(ProblemInfo pi) throws CGException {
        Document doc = pi.getProblem();
        ArrayList<String> coords = pi.getCoordinates();
        ArrayList<String> regionGroup = pi.getRegions();
        int dims = coords.size();
        String conditions = "";
        String[][] xslParams = new String [THREE][2];
        xslParams[0][0] =  "condition";
        xslParams[0][1] =  "true";
        xslParams[1][0] =  "indent";
        xslParams[1][1] =  "0";
        xslParams[2][0] =  "dimensions";
        xslParams[2][1] =  String.valueOf(pi.getDimensions());
        
        //if an internal boundary enters a static or fixed boundary it must disappear.
        for (int i = regionGroup.size() - 1; i >= 0; i--) {
            String regionName = regionGroup.get(i);
            NodeList boundaries = CodeGeneratorUtils.find(doc, "/*/mms:boundaryConditions/mms:boundaryPolicy[descendant::mms:regionName = '" + regionName + "']/mms:boundaryCondition[(" 
                + "mms:type/mms:static or mms:type/mms:algebraic)]");
            //Inflow information initialization
            for (int j = 0; j < boundaries.getLength(); j++) {
                String axis = boundaries.item(j).getFirstChild().getNextSibling().getTextContent();
                String side = boundaries.item(j).getFirstChild().getNextSibling().getNextSibling().getTextContent();
                String regionCondition = "";
                
                for (int k = 0; k < coords.size(); k++) {
                    if (axis.toLowerCase().equals("all") || CodeGeneratorUtils.contToDisc(doc, axis).equals(coords.get(k))) {
                        int id = 2 * k + 1;
                        if (side.toLowerCase().equals("lower") || side.toLowerCase().equals("all")) {
                            regionCondition = regionCondition + "(particle->region == -" + id + " && index" + k + " < boxfirst1(" 
                                    + k + ") && newIndex" + k + " >= boxfirst1(" + k + ")) || ";
                        }
                        if (side.toLowerCase().equals("upper") || side.toLowerCase().equals("all")) {
                            regionCondition = regionCondition + "(particle->region == -" + (id + 1) + " && index" + k + " > boxlast1(" 
                                    + k + ") && newIndex" + k + " <= boxlast1(" + k + ")) || ";  
                        }
                    }
                }

                regionCondition = regionCondition.substring(0, regionCondition.lastIndexOf(" ||"));
                
                NodeList conditionBound = ((Element) boundaries.item(j)).getElementsByTagNameNS(MMSURI, "applyIf");
                String enterCondition = "";
                //If there is a condition for the boundary
                if (conditionBound.getLength() == 1) {
                    enterCondition = "(" + regionCondition + ") && (" + SAMRAIUtils.xmlTransform(
                        SAMRAIUtils.preProcessParticlesBlock(
                                setNPPosition(conditionBound.item(0).getFirstChild().cloneNode(true), coords), 
                                pi.getVariables(), pi.getCoordinates(), false, pi), xslParams) + ")";
                }
                else {
                    enterCondition = regionCondition; 
                }
                                
                //Common create particle code
                String createParticle = "";
	            for (String speciesName: pi.getParticleSpecies()) {
	                String commonCreate = createCommonCreate(doc, pi, regionName, boundaries.item(j), speciesName);
	                createParticle = createParticle + IND + IND + IND + IND + "if (std::is_same<T, Particle_" + speciesName + ">::value) {" + NL
	                	+ CodeGeneratorUtils.incrementIndent(create1DimBoundTrespassing(doc, coords, pi.getTimeCoord(), commonCreate, axis, side, pi, speciesName), 1)
	                	+ IND + IND + IND + IND + "}" + NL;
	            }
                
                conditions = conditions + IND + IND + IND + "if (" + enterCondition + ") {" + NL
                        + IND + IND + IND + IND + "particle->region = particle->newRegion;" + NL
                        + IND + IND + IND + IND + "double posNP[" + dims + "];" + NL
                        + createParticle
                        + IND + IND + IND + "}" + NL;
            }
        }

        
        if (conditions.equals("")) {
            return "";
        }
        return IND + IND + IND + "//Particles that exits an inflow boundary must create a replacing one" + NL
                + conditions;
    }
}
