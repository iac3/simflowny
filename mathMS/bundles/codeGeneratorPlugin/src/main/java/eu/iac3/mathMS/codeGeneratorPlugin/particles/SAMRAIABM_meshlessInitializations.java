/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.codeGeneratorPlugin.particles;

import eu.iac3.mathMS.codeGeneratorPlugin.CGException;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.CodeGeneratorUtils;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.ProblemInfo;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.SAMRAIUtils;

import java.util.ArrayList;

import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Initializations for particles code in SAMRAI.
 * @author bminyano
 *
 */
public final class SAMRAIABM_meshlessInitializations {
    static final String NL = System.getProperty("line.separator");
    static final String IND = "\u0009";
    static final int THREE = 3;
    static final String MTURI = "http://www.w3.org/1998/Math/MathML";
    static final String MMSURI = "urn:mathms";
    
    /**
     * Private constructor.
     */
    private SAMRAIABM_meshlessInitializations() { };
    
    /**
     * Creates the file for the initialization.
     * @param pi                    The problem information
     * @return                      The file
     * @throws CGException          CG004 External error
     */
    public static String create(ProblemInfo pi) throws CGException {
        String result;
        Document doc = pi.getProblem();
        //Parameters for the xsl transformation. It is not a condition formula
        String[][] xslParams = new String [THREE][2];
        xslParams[0][0] =  "condition";
        xslParams[0][1] =  "false";
        xslParams[1][0] =  "indent";
        xslParams[1][1] =  "0";
        xslParams[2][0] =  "dimensions";
        xslParams[2][1] =  String.valueOf(pi.getDimensions());
        int dimensions = pi.getSpatialDimensions();
        
        try {
            result = "//Get fields, auxiliary fields and local variables that are going to be used." + NL;
                
            //Declaration of variables
            DocumentFragment scope = doc.createDocumentFragment();
            NodeList fc = CodeGeneratorUtils.find(doc, "//mms:initialConditions");
            for (int i = 0; i < fc.getLength(); i++) {
                scope.appendChild(fc.item(i).cloneNode(true));
            }
            
            result = result + "std::shared_ptr< pdat::IndexData<Particles, pdat::CellGeometry > > particleVariables("
            	+ "SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles, pdat::CellGeometry >, hier::PatchData>(patch.getPatchData(d_particleVariables_id)));" + NL
            	+ "int* region = ((pdat::CellData<int> *) patch.getPatchData(d_region_id).get())->getPointer();" + NL
                + "const hier::Index boxfirst = particleVariables->getGhostBox().lower();" + NL
                + "const hier::Index boxlast  = particleVariables->getGhostBox().upper();" + NL + NL
                + "const hier::Index boxfirst1 = patch.getBox().lower();" + NL
                + "const hier::Index boxlast1 = patch.getBox().upper();" + NL + NL
                + SAMRAIUtils.generateLasts(pi.getCoordinates(), "") + NL
                + SAMRAIUtils.getParticlesVariableDeclaration(scope, 0, pi) + NL;
            
            //Do the condition
            String actualIndent = "";
            String coordIndex = "";
            String loopBegin = "";
            for (int i = dimensions - 1; i >= 0; i--) {
                //Loop iteration
                loopBegin =  loopBegin + actualIndent + "for(int index" + i + " = boxfirst(" + i + "); index" 
                    + i + " <= boxlast(" + i + "); index" + i + "++) {" + NL;
                actualIndent = actualIndent + IND;
                coordIndex =  "index" + i + ", " + coordIndex;
            }
            result = result + loopBegin;
            coordIndex = coordIndex.substring(0, coordIndex.lastIndexOf(","));
            
            //Create Index for indexData, access index data and iterate over all the particles in the Particles data
            result = result + actualIndent + "hier::Index idx(" + coordIndex + ");" + NL
                + actualIndent + "Particles* part = particleVariables->getItem(idx);" + NL
                + actualIndent + "for (int pit = 0; pit < part->getNumberOfParticles(); pit++) {" + NL;
            actualIndent = actualIndent + IND;
            result = result + actualIndent + "Particle* particle = part->getParticle(pit);" + NL;
            
            NodeList initConditions = CodeGeneratorUtils.find(doc, "//mms:initialConditions/mms:initialCondition");
            for (int j = 0; j < initConditions.getLength(); j++) {
                Node condition = initConditions.item(j);
                NodeList mathExpressions = condition.getChildNodes();
                boolean applyIf = false;

                //If there is a condition
                if (mathExpressions.item(0).getLocalName().equals("applyIf")) {
                    xslParams[0][1] = "true";
                    applyIf = true;
                    result = result + actualIndent + "if (" + SAMRAIUtils.xmlTransform(SAMRAIUtils.preProcessParticlesBlock(
                        mathExpressions.item(0).getFirstChild().getFirstChild(), pi.getVariables(), pi.getCoordinates(), false, pi), xslParams) + ") {" + NL;
                    actualIndent = actualIndent + IND;
                }
                xslParams[0][1] = "false";
                xslParams[1][1] = String.valueOf(actualIndent.length());
                //Equations in the conditions
                mathExpressions = mathExpressions.item(mathExpressions.getLength() - 1).getChildNodes();
                for (int k = 0; k < mathExpressions.getLength(); k++) {
                    Node math = mathExpressions.item(k);
                    result = result + SAMRAIUtils.xmlTransform(SAMRAIUtils.preProcessParticlesBlock(math, 
                        pi.getVariables(), pi.getCoordinates(), false, pi), xslParams);
                }
                //Close if clause if exists
                if (applyIf) {
                    actualIndent = actualIndent.substring(0, actualIndent.lastIndexOf(IND));
                    result = result + actualIndent + "}" + NL;
                }
            }
               
            //Close the iteration over the particles of a Particle data
            actualIndent = actualIndent.substring(0, actualIndent.lastIndexOf(IND));
            result = result + actualIndent + "}" + NL;
            
            //close the loop iteration
            String loopEnd = "";
            for (int i = 0; i < dimensions; i++) {
                actualIndent = actualIndent.substring(0, actualIndent.lastIndexOf(IND));
                loopEnd =  loopEnd + actualIndent + "}" + NL;
            }
            result = result + loopEnd + NL;
            
            //Post-initialization synchronization
            result = result + synchronization(pi);
            
            return result;
        }
        catch (CGException e) {
            throw e;
        }
        catch (Exception e) {
            throw new CGException(CGException.CG00X, e.getMessage());
        } 
    }
    
    /**
     * Creates the synchronization for the agent based models.
     * @param pi            The problem information
     * @return              The code
     * @throws CGException  CG00X External error
     */
    private static String synchronization(ProblemInfo pi) throws CGException {
        String indexCell = "";
        String actualIndent = IND + IND;
        Document doc = pi.getProblem();
        for (int i = 0; i < pi.getDimensions(); i++) {
            actualIndent = actualIndent + IND;
            indexCell = indexCell + "index" + i + " - boxfirst(" + i + "), ";
        }
        indexCell = indexCell.substring(0, indexCell.lastIndexOf(","));
        String bound = SAMRAIABM_meshlessBoundaries.createBoundaries(IND + IND + IND, pi, indexCell);
        String checkPosition = "";
        if (CodeGeneratorUtils.find(doc, "//mms:boundaryCondition//mms:periodical").getLength() > 0) {
            checkPosition = checkPositions(pi.getDimensions());
        }
        if (bound.equals("") && !checkPosition.equals("")) {
            return "#postInitSync#" + NL
                + IND + IND + "d_bdry_fill_init->createSchedule(level,this)->fillData(init_data_time, false);" + NL
                + IND + IND + "for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {" + NL
                + IND + IND + IND + "const std::shared_ptr< hier::Patch > patch = *p_it;" + NL
                + IND + IND + IND + "std::shared_ptr< pdat::IndexData<Particles, pdat::CellGeometry > > particleVariables("
                + "SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles, pdat::CellGeometry >, hier::PatchData>(patch->getPatchData(d_particleVariables_id)));" + NL
                + IND + IND + IND + "const hier::Index boxfirst = particleVariables->getGhostBox().lower();" + NL
                + IND + IND + IND + "const hier::Index boxlast  = particleVariables->getGhostBox().upper();" + NL + NL
                + checkPosition
                + IND + IND + "}" + NL; 
        }
		return "#postInitSync#" + NL
		        + IND + IND + "double current_time = init_data_time;" + NL
		        + IND + IND + "for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {" + NL
		        + IND + IND + IND + "const std::shared_ptr< hier::Patch > patch = *p_it;" + NL
		        + IND + IND + IND + "std::shared_ptr< pdat::IndexData<Particles, pdat::CellGeometry > > particleVariables("
		        + "SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles, pdat::CellGeometry >, hier::PatchData>(patch->getPatchData(d_particleVariables_id)));" + NL
		        + IND + IND + IND + "const hier::Index boxfirst1 = patch->getBox().lower();" + NL
		        + IND + IND + IND + "const hier::Index boxlast1  = patch->getBox().upper();" + NL
		        + IND + IND + IND + "const hier::Index boxfirst = particleVariables->getGhostBox().lower();" + NL
		        + IND + IND + IND + "const hier::Index boxlast  = particleVariables->getGhostBox().upper();" + NL + NL
		        + SAMRAIUtils.generateLasts(pi.getCoordinates(), IND + IND + IND) + NL
		        + IND + IND + IND + "//Get the dimensions of the patch" + NL
		        + IND + IND + IND + "const std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry, hier::PatchGeometry>(patch->getPatchGeometry()));" + NL
		        + IND + IND + IND + "const double* dx  = patch_geom->getDx();" + NL + NL
		        + bound
		        + IND + IND + "}" + NL
		        + IND + IND + "d_bdry_fill_init->createSchedule(level,this)->fillData(init_data_time, false);" + NL
		        + IND + IND + "for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {" + NL
		        + IND + IND + IND + "const std::shared_ptr< hier::Patch > patch = *p_it;" + NL
		        + IND + IND + IND + "const hier::Index boxfirst = particleVariables->getGhostBox().lower();" + NL
		        + IND + IND + IND + "const hier::Index boxlast  = particleVariables->getGhostBox().upper();" + NL + NL
		        + checkPosition
		        + IND + IND + "}" + NL;
    }
   
    /**
     * Create the calls to checkPosition to correct the position of the particles at the boundaries when there are periodical boundaries.
     * @param dimensions        The dimensions of the problem
     * @return                  The code
     */
    private static String checkPositions(int dimensions) {
        String block = "";
        String coordIndex = "";
        String currentIndent = IND + IND + IND;
        
        //Loop iteration
        for (int i = dimensions - 1; i >= 0; i--) {
            block =  block + currentIndent + "for (int index" + i + " = boxfirst(" + i + "); index" + i + " <= boxlast(" + i + "); index" + i 
                + "++) {" + NL;
            currentIndent = currentIndent + IND;
            coordIndex =  "index" + i + ", " + coordIndex;
        }
        coordIndex = coordIndex.substring(0, coordIndex.lastIndexOf(","));
        
        //Create Index for indexData
        block = block + currentIndent + "hier::Index idx(" + coordIndex + ");" + NL
            + currentIndent + "//Correct the position if there is periodical boundary" + NL;
        
        //call checkposition
        block = block + currentIndent + "Particles* part = particleVariables->getItem(idx);" + NL
            + currentIndent + "checkPosition(patch, " + coordIndex + ", part);" + NL;
        
        //Close loop
        for (int i = 0; i < dimensions; i++) {
            currentIndent = currentIndent.substring(1);
            block =  block + currentIndent + "}" + NL;
        }
        
        return block;
    }
    
    /**
     * Creates the condition for the boundary near to its region groups.
     * @param pi                    The problem info
     * @param boundRegionGroups    The region groups of the boundary
     * @return                      The condition
     * @throws CGException          CG00X External error
     */
    public static String createBoundSegGroupCondition(ProblemInfo pi, Element boundRegionGroups) throws CGException {
        ArrayList<String> coordinates = pi.getCoordinates();
        ArrayList<String> regions = pi.getRegions();
        int stencil = pi.getSchemaStencil();
        String condition = "";
        
        NodeList boundRegions = boundRegionGroups.getChildNodes();
        for (int l = 0; l < boundRegions.getLength(); l++) {
            String regionName = boundRegions.item(l).getTextContent();
            String index = "";
            String positiveCompatible = "";
            String negativeCompatible = "";
            for (int i = 0; i < coordinates.size(); i++) {
                for (int j = 0; j < stencil; j++) {
                    String positiveStencilIndex = "";
                    String negativeStencilIndex = "";
                    String positiveIndexCheck = "";
                    String negativeIndexCheck = "";
                    for (int k = 0; k < coordinates.size(); k++) {
                        if (i == k) {
                            positiveStencilIndex = 
                                positiveStencilIndex + "index" + k + " - boxfirst(" + k + ") + " + (j + 1) + ", ";
                            negativeStencilIndex = 
                                negativeStencilIndex + "index" + k + " - boxfirst(" + k + ") - " + (j + 1) + ", ";
                            positiveIndexCheck = "index" + k + " - boxfirst(" + k + ") + " + (j + 1) + " < " + coordinates.get(k) + "last";
                            negativeIndexCheck = "index" + k + " - boxfirst(" + k + ") - " + (j + 1) + " >= 0";
                        }
                        else {
                            positiveStencilIndex = positiveStencilIndex + "index" + k + " - boxfirst(" + k + "), ";
                            negativeStencilIndex = negativeStencilIndex + "index" + k + " - boxfirst(" + k + "), ";
                        }
                    }
                    positiveStencilIndex = positiveStencilIndex.substring(0, positiveStencilIndex.lastIndexOf(","));
                    negativeStencilIndex = negativeStencilIndex.substring(0, negativeStencilIndex.lastIndexOf(","));
                    positiveCompatible = positiveCompatible + "(" + positiveIndexCheck + " && vector(seg"
                        + (regions.indexOf(regionName) * 2 + 1) + ", " + positiveStencilIndex + ") == 1) || ";
                    negativeCompatible = negativeCompatible + "(" + negativeIndexCheck + " && vector(seg" 
                        + (regions.indexOf(regionName) * 2 + 1) + ", " + negativeStencilIndex + ") == 1) || ";
                }
                index = index + coordinates.get(i) + ", ";
            }
            positiveCompatible = positiveCompatible.substring(0, positiveCompatible.lastIndexOf(" ||"));
            negativeCompatible = negativeCompatible.substring(0, negativeCompatible.lastIndexOf(" ||"));
            index = index.substring(0, index.lastIndexOf(","));
            
    
            condition = condition + "(" + positiveCompatible + ") || (" + negativeCompatible + ") || ";
        }
        
        return condition.substring(0, condition.lastIndexOf(" ||"));
    }
}
