/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.codeGeneratorPlugin;

import eu.iac3.mathMS.codeGeneratorPlugin.utils.CodeGeneratorUtils;

import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.log.LogService;
import org.osgi.util.tracker.ServiceTracker;

/** 
 * CodesGeneratorImpl implements the 
 * {@link CodeGenerator} interface.
 * It has the functions of create the code
 * in the specified simulation platform.
 * 
 * @author  ----
 * @version ----
 */
@Component //
(//
    immediate = true, //
    name = "CodeGenerator", //
    property = //
    { //
      "service.exported.interfaces=*", //
      "service.exported.configs=org.apache.cxf.ws", //
      "org.apache.cxf.ws.address=/CodeGenerator" //
    } //
)
public class CodeGeneratorImpl implements CodeGenerator {
    
    static LogService logservice = null;
    
    /**
     * Constructor to take the bundle context.
     */
    public CodeGeneratorImpl() {
        super();
        new CodeGeneratorUtils();
        BundleContext context = FrameworkUtil.getBundle(getClass()).getBundleContext();
        if (context != null) {
            @SuppressWarnings({ "rawtypes", "unchecked" })
			ServiceTracker logServiceTracker = new ServiceTracker(context, org.osgi.service.log.LogService.class.getName(), null);
            logServiceTracker.open();
            logservice = (LogService) logServiceTracker.getService();
        }
    }
    
    /** 
     * Generates the code for SAMRAI framework.
     *
     * @param xmlDoc         discretized problem to generate the code
     * @return               the document of the generated code
     * @throws CGException   CG001 XML document does not match xml schema
     *                        CG002 Code already exist.
     *                        CG003 Not possible to create code for the platform
     *                        CG004 External error
     */
    public String generateSAMRAICode(String xmlDoc) throws CGException {
        try {
            return new SimPlatSAMRAICodeGenerator().generateSAMRAICode(xmlDoc);
        }
        catch (CGException e) {
            throw e;
        }
    }
    
    /**
     * Generates the code with Boost library.
     *
     * @param xmlDoc         discretized problem to generate the code
     * @return               the document of the generated code
     * @throws CGException   CG001 XML document does not match xml schema
     *                        CG002 Code already exist.
     *                        CG003 Not possible to create code for the platform
     *                        CG004 External error
     */
    public String generateBoostCode(String xmlDoc) throws CGException {
        try {
            return new BoostCodeGenerator().generateBoostCode(xmlDoc);
        }
        catch (CGException e) {
            throw e;
        }
    }
}
