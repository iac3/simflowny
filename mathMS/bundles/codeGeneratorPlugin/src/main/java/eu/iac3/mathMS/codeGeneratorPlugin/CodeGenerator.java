/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.codeGeneratorPlugin;

import javax.jws.WebService;

/** 
 * CodeGenerator is an interface that provides the functionality 
 * to create the files for a selected simulation platform.
 * 
 * @author      ----
 * @version     ----
 */
@WebService
public interface CodeGenerator {

    /** 
     * Generates the code for SAMRAI framework.
     *
     * @param xmlDoc         discretized problem to generate the code
     * @return               the document of the generated code
     * @throws CGException   CG001 XML document does not match xml schema
     *                        CG002 Code already exist.
     *                        CG003 Not possible to create code for the platform
     *                        CG004 External error
     */
    String generateSAMRAICode(String xmlDoc) throws CGException;
    
   /**
    * Generates the code with Boost library.
    *
    * @param xmlDoc         discretized problem to generate the code
    * @return               the document of the generated code
    * @throws CGException   CG001 XML document does not match xml schema
    *                        CG002 Code already exist.
    *                        CG003 Not possible to create code for the platform
    *                        CG004 External error
    */
    String generateBoostCode(String xmlDoc) throws CGException;

}


