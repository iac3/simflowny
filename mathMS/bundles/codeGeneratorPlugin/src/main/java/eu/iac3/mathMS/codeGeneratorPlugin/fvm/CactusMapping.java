/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.codeGeneratorPlugin.fvm;

import eu.iac3.mathMS.codeGeneratorPlugin.CGException;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.HeaderInfo;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.ProblemInfo;
import eu.iac3.mathMS.documentManagerPlugin.DocumentManager;

/**
 * Mapping methods for cactus regions.
 * @author bminyano
 *
 */
public final class CactusMapping {
    static final String IND = "\u0009";
    static String mtUri = "http://www.w3.org/1998/Math/MathML";
    static final int THREE = 3;
    static final String NL = System.getProperty("line.separator"); 
    static final double ABSERROR = 1e-10;
    static DocumentManager docMan;
    static final int MCDMAX = 10;
    
    /**
     * Private constructor.
     */
    private CactusMapping() { };
    
    /**
     * Generates the mapping file.
     * @param hi                    The header information
     * @param problemInfo           The problem information
     * @return                      The code
     * @throws CGException          CG004 External error
     */
    public static String mappingRegionsM4(HeaderInfo hi, ProblemInfo problemInfo) throws CGException {
        /*try {
        	docMan = new DocumentManagerImpl();
            String localVariables = "!" + IND + IND + "Declare local variables" + NL
                + "!" + IND + IND + "-----------------" + NL + NL;
            //Getting all the variables used in the initial conditions
            Document problem = problemInfo.getProblem();
            
            //Declaration of spatial coordinates, and its variables for start and end.
            for (int i = 0; i < problemInfo.getCoordinates().size(); i++) {
                String coord = CactusUtils.variableNormalize(problemInfo.getCoordinates().get(i));
                localVariables = localVariables + IND + IND + "CCTK_INT " + coord + ", " + coord + "MapStart, " + coord + "MapEnd, " 
                    + coord + "term, previousMap" + coord + ", " + coord + "WallAcc, " + coord + "u, " + coord + "l, index" + coord +  NL
                    + IND + IND + "logical interiorMap" + coord + NL;
            }
           
            //Global variable declarations
            localVariables = localVariables + IND + IND + "CCTK_INT minBlock(" + problemInfo.getCoordinates().size() + "), maxBlock(" 
                + problemInfo.getCoordinates().size() + "), unionsI, facePointI, status, ie1, ie2, ie3, proc, reduction_handle" + NL
                + IND + IND + "CCTK_REAL maxDistance, e1, e2, e3, SQRT3INV" + NL
                + IND + IND + "logical done, modif, checkStencil, s1, s2, s3, s4, checkStalled" + NL;
            
            String mapping = IND + IND + "SQRT3INV = 1.0/SQRT(3.0)" + NL
                + "!" + IND + IND + "Region mapping" + NL
                + "!" + IND + IND + "----------------" + NL;
            
            //For every region group (first the lower precedents)
            for (int i = problemInfo.getRegionPrecedence().size() - 1; i >= 0; i--) {
                String regionName = problemInfo.getRegionPrecedence().get(i);
                
                //Initialize the grid
                String query = "//mms:regionGroup[descendant::mms:regionName = '" + regionName + "']" 
                    + "//mms:interior";
                boolean hasInterior = CodeGeneratorUtils.find(problem, query).getLength() > 0;
                query = "//mms:regionGroup[descendant::mms:regionName = '" + regionName + "']//mms:surface";
                boolean hasSurface = CodeGeneratorUtils.find(problem, query).getLength() > 0;
                query = "//mms:regionGroup[descendant::mms:regionName = '" + regionName + "']//" 
                        + "mms:problemDomain";
                boolean isFullDomain = CodeGeneratorUtils.find(problem, query).getLength() > 0;
                query = "//mms:regionGroup[descendant::mms:regionName = '" + regionName + "']/" 
                    + "mms:regionGroupName";
                String regionGroupName = CodeGeneratorUtils.find(problem, query).item(0).getTextContent();
                int regionbaseId = problemInfo.getRegions().indexOf(regionGroupName);
                query = "//mms:regionGroup[descendant::mms:regionName = '" + regionName + "']";
                CodeGeneratorUtils.find(problem, query).item(0);
                problemInfo.getRegionLimits().get(regionName);
                MappingRegionInfo si = new MappingRegionInfo((Element) CodeGeneratorUtils.find(problem, query).item(0), 
                        problemInfo.getRegionLimits().get(regionName), (regionbaseId * 2) + 1, hasInterior, hasSurface, isFullDomain);
                mapping = mapping + mapRegion(si, problemInfo, hi.getProblemId());
                //Region variable declarations
                localVariables = localVariables + getRegionVariables((Element) CodeGeneratorUtils.find(problem, query).item(0), problemInfo);
            }
            //Boundary mapping
            mapping = mapping + mapBoundaries(problemInfo);
            
            //Mapping variables
            localVariables = localVariables + "!" + IND + IND + "Mapping variables" + NL
                + IND + IND + "logical workingGlobal, finishedGlobal, cleanStencil" + NL
                + IND + IND + "CCTK_INT working, finished, nodes, tmp1, tmp2, pred, first" + NL
                + IND + IND + "CCTK_INT, allocatable :: workingArray(:), finishedArray(:)" + NL;
            if (!problemInfo.getHardRegions().isEmpty()) {
                String vars = "";
                for (int j = 0; j < problemInfo.getDimensions(); j++) {
                    String coord = problemInfo.getCoordinates().get(j);
                    vars = vars + "dist_" + coord + "_tmp, dist_" + coord + "_tmp_p, dist_" + coord + "_tmp_m, Dist_" + coord + ", ";
                }
                vars = vars.substring(0, vars.lastIndexOf(", "));
                localVariables = localVariables + IND + IND + "CCTK_INT " + vars + NL;
            }
            //Delta declarations
            localVariables = localVariables + "!" + IND + IND + "Deltas" + NL
                + CactusUtils.getAllDeltas(problem, problemInfo.getGridLimits(), problemInfo.getPeriodicalBoundary(), problemInfo.getSchemaStencil());
            
            //Floating point comparisons
            String epsilon = "";
            double maxValue = getMaxDomainDoubleValue(problemInfo.getGridLimits());
            String fpInit = "";
            String fpVar = "";
            //All the domains are numbers
            if (maxValue > 0) {
                epsilon = String.valueOf(maxValue * ABSERROR);
            }
            //Some domain values are not numbers, so the max value should be done during execution
            else {
                epsilon = "FPC_epsilon_Cactus";
                fpVar = IND + IND + "CCTK_REAL FPC_epsilon_Cactus" + NL;
                String maximumValue = getMaxDomainStringValue(problemInfo.getGridLimits());
                
                fpInit = IND + IND + "FPC_epsilon_Cactus = " + maximumValue + " * " + ABSERROR + NL;
            }
            String macros = "define(leF,((abs(($1) - ($2))/" + epsilon + " .lt. 10 .and. FLOOR(abs(($1) - ($2))/" + epsilon 
                + ") .lt. 1) .or. ($1) < ($2)))dnl" + NL
                + "define(geF,((abs(($1) - ($2))/" + epsilon + " .lt. 10 .and. FLOOR(abs(($1) - ($2))/" + epsilon 
                + ") .lt. 1) .or. ($2) < ($1)))dnl" + NL;
            //Unitary vector calculation for extrapolation distance variables

            String content = "/*@@" + NL
                + "  @file      MappingRegions.F" + NL
                + "  @date      " + hi.getDate().toString() + NL      
                + "  @author    " + hi.getAuthor() + NL
                + "  @desc" + NL 
                + "             Mapping regions for " + hi.getProblemId() + NL
                + "  @enddesc" + NL 
                + "  @version " + hi.getVersion() + NL
                + "@@* /" + NL + NL
                + macros + NL
                + "#include \"cctk.h\"" + NL
                + "#include \"cctk_Parameters.h\"" + NL
                + "#include \"cctk_Arguments.h\"" + NL
                + "#include \"cctk_Functions.h\"" + NL + NL
                + IND + "subroutine " + hi.getProblemId() + "_mapping(CCTK_ARGUMENTS)" + NL + NL
                + IND + IND + "implicit none" + NL + NL
                + IND + IND + "DECLARE_CCTK_ARGUMENTS" + NL
                + IND + IND + "DECLARE_CCTK_PARAMETERS" + NL
                + IND + IND + "DECLARE_CCTK_FUNCTIONS" + NL + NL
                + fpVar + NL
                + localVariables + NL
                + IND + IND + "allocate(workingArray(cctk_nProcs(cctkGH)))" + NL
                + IND + IND + "allocate(finishedArray(cctk_nProcs(cctkGH)))" + NL + NL
                + fpInit + NL
                + mapping + NL
                //Calculation of the hard region distances for the extrapolations
                + generateHardRegionDistances(problem, problemInfo) + NL
                + IND + "end subroutine " + hi.getProblemId() + "_mapping" + NL
                + createFloodFillFunction(problemInfo.getRegionIds(), problemInfo.getCoordinates())
                + generateCheckStalledFunction(problemInfo);
            
            //if stencil less than 2 not extra stencil needed for the regions
            if (problemInfo.getSchemaStencil() > 1) {
                content = content + CactusUtils.generateSetStencilLimitRoutine(problemInfo.getSchemaStencil(), problemInfo.getCoordinates(), 
                        problemInfo.getRegionIds())
                        + CactusUtils.generateCheckStencilRoutine(problemInfo.getSchemaStencil(), problemInfo.getCoordinates(), 
                                problemInfo.getRegionIds());
            }
            return content;
        } 
        catch (CGException e) {
            throw e;
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new CGException(CGException.CG00X, e.getMessage());
        }*/
    	return "";
    }
//
//    /**
//     * Generate the code to calculate the hard boundary region distances for the fields.
//     * @param problem           The problem
//     * @param pi                The problem info
//     * @return                  The code
//     * @throws CGException      CG004 External error
//     */
//    private static String generateHardRegionDistances(Document problem, ProblemInfo pi) throws CGException {
//        String result = "";
//        if (!pi.getHardRegions().isEmpty()) {
//            String indent = IND + IND;
//            result = result + "!" + IND + IND + "Calculation of the hard region distances for the extrapolations" + NL;
//            
//            //Open loops
//            String loop = "";
//            String endLoop = "";
//            String coordIndex = "";
//            ArrayList<String> coordinates = pi.getCoordinates();
//            for (int i = 0; i < pi.getDimensions(); i++) {
//                loop = loop + indent + "do " + coordinates.get(i) + " = " + coordinates.get(i) + "MapStart, " + coordinates.get(i) 
//                        + "MapEnd" + NL;
//                endLoop = indent + "end do" + NL + endLoop;
//                indent = indent + IND;
//                coordIndex = coordIndex + pi.getCoordinates().get(i) + ", ";
//                result = result + IND + IND + coordinates.get(i) + "MapStart = 1" + NL 
//                        + IND + IND + coordinates.get(i) + "MapEnd = cctk_lsh(" + (i + 1) + ")" + NL;
//            }
//            coordIndex = coordIndex.substring(0, coordIndex.lastIndexOf(","));
//            
//            ArrayList<String> segNBound = new ArrayList<String>();
//            ArrayList<String> segNBoundId = new ArrayList<String>();
//            for (int j = pi.getRegions().size() - 1; j >= 0; j--) {
//                String regionName = pi.getRegions().get(j);
//                segNBound.add(regionName + "I");
//                segNBoundId.add(String.valueOf((j * 2) + 1));
//                segNBound.add(regionName + "S");
//                segNBoundId.add(String.valueOf((j * 2) + 2));
//            }
//            for (int j = pi.getBoundariesPrecedence().size() - 1; j >= 0; j--) {
//                String boundName = pi.getBoundariesPrecedence().get(j);
//                segNBound.add(boundName);
//                String axis = boundName.substring(0, boundName.lastIndexOf("-"));
//                String side = boundName.substring(boundName.lastIndexOf("-") + 1);
//                segNBoundId.add(axis + side);
//            }
//            
//            //Initialize the distance variables
//            ArrayList<String> candidates = new ArrayList<String>();
//            ArrayList<String> added = new ArrayList<String>();
//            result = result + loop;
//            for (int j = 0; j < segNBound.size(); j++) {
//                String regionName = segNBound.get(j);
//                if (pi.getHardRegionFields(regionName) != null) {
//                    ArrayList<String> interactions = CodeGeneratorUtils.getRegionInteractions(regionName, pi.getHardRegions(), 
//                            pi.getRegions());
//                    for (int k = 0; k < interactions.size(); k++) {
//                        ArrayList<String> hardFields = CodeGeneratorUtils.filterHardFields(regionName, interactions.get(k), pi.getHardRegions());
//                        if (hardFields.size() > 0) {
//                            String segName = interactions.get(k);
//                            int segId = 2 * pi.getRegions().indexOf(segName) + 1;
//                            if (pi.getRegionIds().contains(String.valueOf(segId))) {
//                                if (!candidates.contains("stalled_" + segId)) {
//                                    candidates.add("stalled_" + segId);
//                                    result = result + indent + "stalled_" + segId + "(" + coordIndex + ") = checkStalled(CCTK_PASS_FTOF, " 
//                                            + coordIndex + ", " + segId + ")" + NL;
//                                }
//                            }
//                            segId = 2 * pi.getRegions().indexOf(segName) + 2;
//                            if (pi.getRegionIds().contains(String.valueOf(segId))) {
//                                if (!candidates.contains("stalled_" + segId)) {
//                                    candidates.add("stalled_" + segId);
//                                    result = result + indent + "stalled_" + segId + "(" + coordIndex + ") = checkStalled(CCTK_PASS_FTOF, " 
//                                            + coordIndex + ", " + segId + ")" + NL;
//                                }
//                            }
//                        }
//                    }
//                }
//              
//            }
//            Iterator<String> segsHard = pi.getHardRegions().keySet().iterator();
//            while (segsHard.hasNext()) {
//                String segName = segsHard.next();
//                ArrayList<String> hardFields = new ArrayList<String>();
//                if (!CodeGeneratorUtils.isBoundary(pi.getRegions(), segName)) {
//                    hardFields.addAll(pi.getHardRegionFields(segName + "I"));
//                    hardFields.addAll(pi.getHardRegionFields(segName + "S"));
//                }
//                else {
//                    hardFields.addAll(pi.getHardRegionFields(segName));
//                }
//                for (int k = 0; k < hardFields.size(); k++) {
//                    String field = CactusUtils.variableNormalize(hardFields.get(k));
//                    for (int i = 0; i < pi.getDimensions(); i++) {
//                        String coord = coordinates.get(i);
//                        if (!added.contains("d_" + coord + "_" + field)) {
//                            added.add("d_" + coord + "_" + field);
//                            result = result + indent + "d_" + coord + "_" + field + "(" + coordIndex + ") = 0" + NL;
//                        }
//                    }
//                }
//            }
//            result = result + endLoop;
//            
//            //Calculate the distances
//            result = result + loop;
//            for (int j = 0; j < segNBound.size(); j++) {
//                String regionName = segNBound.get(j);
//                result = result + generateHardDistancesRegion(problem, pi, regionName, segNBoundId.get(j));
//            }
//            result = result + endLoop;
//        }
//        return result;
//    }
//    
//    
//    /**
//     * Generate the code to calculate the hard boundary region distances for the fields in a region.
//     * @param problem           The problem
//     * @param pi                The problem info
//     * @param regionName       The region name
//     * @param regionId         The region id
//     * @return                  The code
//     * @throws CGException      CG004 External error
//     */
//    private static String generateHardDistancesRegion(Document problem, ProblemInfo pi, String regionName, String regionId) 
//        throws CGException {
//        String indent = IND + IND;
//        String result = "";
//        String coordIndex = "";
//        for (int j = 0; j < pi.getDimensions(); j++) {
//            indent = indent + IND;
//            String coord = pi.getCoordinates().get(j);
//            coordIndex = coordIndex + coord + ", ";
//        }
//        coordIndex = coordIndex.substring(0, coordIndex.lastIndexOf(","));
//        String distTemp = "";
//        for (int j = 0; j < pi.getDimensions(); j++) {
//            String coord = pi.getCoordinates().get(j);
//            distTemp = distTemp + indent + IND + "dist_" + coord + "_tmp = 999" + NL
//                    + indent + IND + "dist_" + coord + "_tmp_p = 999" + NL
//                    + indent + IND + "dist_" + coord + "_tmp_m = 999" + NL;
//        }
//        if (pi.getHardRegionFields(regionName) != null) {
//            result = result + indent + "if (FOV_" + regionId + "(" + coordIndex + ") .gt. 0) then" + NL
//                    + distTemp;
//            ArrayList<String> interactions = CodeGeneratorUtils.getRegionInteractions(regionName, pi.getHardRegions(), pi.getRegions());
//            for (int k = 0; k < interactions.size(); k++) {
//                ArrayList<String> hardFields = CodeGeneratorUtils.filterHardFields(regionName, interactions.get(k), pi.getHardRegions());
//                
//                if (hardFields.size() > 0) {
//                    String segName = interactions.get(k);
//                    int stencil = Integer.parseInt(CodeGeneratorUtils.find(problem, "//mms:regionGroup[" 
//                            + "mms:regionGroupName = '" + segName + "']//mms:discretizationInfo/" 
//                            + "mms:stencil").item(0).getTextContent());
//                    int segId = 2 * pi.getRegions().indexOf(segName) + 1;
//
//                    
//                    for (int i = 0; i < pi.getDimensions(); i++) {
//
//                        String distIndex = "";
//                        for (int j = 0; j < pi.getDimensions(); j++) {
//                            String coord = pi.getCoordinates().get(j);
//                            if (i == j) {
//                                distIndex = distIndex + coord + " + Dist_" + coord + ", ";
//                            }
//                            else {
//                                distIndex = distIndex + coord + ", ";
//                            }
//                        }
//                        distIndex = distIndex.substring(0, distIndex.lastIndexOf(","));
//                        
//                        String segCond = "";
//                        boolean hasInterior = pi.getRegionIds().contains(String.valueOf(segId));
//                        boolean hasSurface = pi.getRegionIds().contains(String.valueOf(segId +  1));
//                        segCond = " .and. (";
//                        if (hasInterior) {
//                            segCond = segCond + "stalled_" + segId + "(" + distIndex + ") .eq. 0";
//                        }
//                        if (hasInterior && hasSurface) {
//                            segCond = segCond + " .or. ";
//                        }
//                        if (hasSurface) {
//                            segCond = segCond + "stalled_" + (segId + 1) + "(" + distIndex + ") .eq. 0";
//                        }
//                        segCond = segCond + ")";
//                        
//                        String coord = pi.getCoordinates().get(i);
//                        String nonZero = coord + " + Dist_" + coord + " .ge. 1 .and. " + coord + " + Dist_" + coord + " .le. " + coord + "MapEnd";
//                        result = result + indent + IND + "do Dist_" + coord + " = -" + stencil + ", 0" + NL
//                                + indent + IND + IND + "if (" + nonZero + " .and. ABS(Dist_" + coord + ") .lt. ABS(dist_" + coord + "_tmp_m)"
//                                + segCond + ") then" + NL
//                                + indent + IND + IND + IND + "dist_" + coord + "_tmp_m = -Dist_" + coord + NL
//                                + indent + IND + IND + "end if" + NL
//                                + indent + IND + "end do" + NL
//                                + indent + IND + "do Dist_" + coord + " = 1, " + stencil + NL
//                                + indent + IND + IND + "if (" + nonZero + " .and. ABS(Dist_" + coord + ") .lt. ABS(dist_" + coord + "_tmp_p)"
//                                + segCond + ") then" + NL
//                                + indent + IND + IND + IND + "dist_" + coord + "_tmp_p = -Dist_" + coord + NL
//                                + indent + IND + IND + "end if" + NL
//                                + indent + IND + "end do" + NL
//                                + indent + IND + "if (ABS(dist_" + coord + "_tmp_m) .eq. ABS(dist_" + coord + "_tmp_p)) then" + NL
//                                + indent + IND + IND + "dist_" + coord + "_tmp = dist_" + coord + "_tmp_m" + NL
//                                + indent + IND + "else" + NL
//                                + indent + IND + IND + "if (ABS(dist_" + coord + "_tmp_m) .lt. ABS(dist_" + coord + "_tmp_p)) then" + NL
//                                + indent + IND + IND + IND + "dist_" + coord + "_tmp = dist_" + coord + "_tmp_m" + NL
//                                + indent + IND + IND + "else" + NL
//                                + indent + IND + IND + IND + "dist_" + coord + "_tmp = dist_" + coord + "_tmp_p" + NL
//                                + indent + IND + IND + "end if" + NL
//                                + indent + IND + "end if" + NL;
//                        //Set distances
//                        for (int l = 0; l < hardFields.size(); l++) {
//                            String field = CactusUtils.variableNormalize(hardFields.get(l));
//                            String assignment = indent + IND + IND + "d_" + coord + "_" + field + "(" 
//                                    + coordIndex + ") = dist_" + coord + "_tmp" + NL;
//                            String zeroCond = "d_" + coord + "_" + field + "(" + coordIndex + ") .eq. 0";
//                            String distCond = "ABS(d_" + coord + "_" + field + "(" + coordIndex + "))";
//                            result = result + indent + IND + "if (dist_" + coord + "_tmp .ne. 999 .and. ((" + zeroCond + ") .or. (ABS(dist_" 
//                                    + coord + "_tmp) .lt. " + distCond + "))) then" +  NL
//                                    + assignment
//                                    + indent + IND + "end if" +  NL;
//                        }
//                    }
//
//                }
//            }
//            result = result + indent + "end if" + NL;
//        }
//        return result;
//    }
//    
//    /**
//     * Generates the recursive subprogram to clean the thickness in the x3d regions.
//     * @param pi            The problem info
//     * @return              The code generated
//     */
//    private static String generateCheckStalledFunction(ProblemInfo pi) {
//        ArrayList<String> coordinates = pi.getCoordinates();
//        String parameters = "";
//        String index = "";
//        String stencilVars = IND + IND + "CCTK_INT stencilAcc";
//        String calc = "";
//        String cond = "";
//        for (int i = 0; i < coordinates.size(); i++) {
//            String coord = coordinates.get(i);
//            parameters = parameters + coord + ", ";
//            index = index + coord + ", ";
//            stencilVars = stencilVars + ", stencilAccMax_" + coord + ", " + coord + "t1";
//            String coordIndex = "";
//            for (int j = 0; j < coordinates.size(); j++) {
//                String coord2 = coordinates.get(j);
//                if (i == j) {
//                    coordIndex = coordIndex + coord2 + "t1, ";
//                }
//                else {
//                    coordIndex = coordIndex + coord2 + ", ";
//                } 
//            }
//            coordIndex = coordIndex.substring(0, coordIndex.lastIndexOf(","));
//            calc = calc + IND + IND + IND + "stencilAcc = 0" + NL
//                    + IND + IND + IND + "stencilAccMax_" + coord + " = 0" + NL
//                    + IND + IND + IND + "do " + coord + "t1 = MAX(" + coord + " - " + pi.getSchemaStencil() + ", 0), MIN(" + coord 
//                    + " + " + pi.getSchemaStencil() + ", cctk_lsh(" + (i + 1) + "))" + NL
//                    + IND + IND + IND + IND + "if (FOV(" + coordIndex + ") .gt. FOV_threshold) then" + NL
//                    + IND + IND + IND + IND + IND + "stencilAcc = stencilAcc + 1" + NL
//                    + IND + IND + IND + IND + "else" + NL
//                    + IND + IND + IND + IND + IND + "stencilAccMax_" + coord + " = MAX(stencilAccMax_" + coord + ", stencilAcc)" + NL
//                    + IND + IND + IND + IND + IND + "stencilAcc = 0" + NL
//                    + IND + IND + IND + IND + "end if" + NL
//                    + IND + IND + IND + "end do" + NL
//                    + IND + IND + IND + "stencilAccMax_" + coord + " = MAX(stencilAccMax_" + coord + ", stencilAcc)" + NL;
//            cond = cond + "(stencilAccMax_" + coord + " .lt. " + pi.getSchemaStencil() + ") .or. ";
//        }
//        parameters = parameters.substring(0, parameters.lastIndexOf(",")) + ", seg";
//        index = index.substring(0, index.lastIndexOf(","));
//        stencilVars = stencilVars + ";" + NL;
//        cond = cond.substring(0, cond.lastIndexOf(" .or."));
//        
//        String fovCase = IND + IND + "select case(seg)" + NL;
//        String fovDeclaration = "";
//        String x2 = "";
//        if (coordinates.size() == 2) {
//            x2 = ",X1AuxiliaryGroup";
//        }
//        if (coordinates.size() == THREE) {
//            x2 = ",X1AuxiliaryGroup,X2AuxiliaryGroup";
//        }
//        for (int i = 0; i < pi.getInteriorRegionIds().size(); i++) {
//            String regionId = pi.getInteriorRegionIds().get(i);
//            fovCase = fovCase + IND + IND + IND + "case (" + regionId + ")" + NL;
//            if (i == 0) {
//                fovDeclaration = fovDeclaration + IND + IND + "CCTK_INT FOV(X0AuxiliaryGroup" + x2 + ")" + NL;
//            }
//            for (int j = 0; j < pi.getInteriorRegionIds().size(); j++) {
//                String regionId2 = pi.getInteriorRegionIds().get(j);
//                if (i == j) {
//                    fovCase = fovCase + IND + IND + IND + IND + "FOV = FOV_" + regionId2 + NL;
//                }
//            }
//        }
//        fovCase = fovCase + IND + IND + "end select" + NL;
//        
//        return IND + "logical function checkStalled(CCTK_ARGUMENTS, " + parameters + ")" + NL
//                + IND + IND + "implicit none" + NL + NL
//                + IND + IND + "DECLARE_CCTK_ARGUMENTS" + NL
//                + IND + IND + "DECLARE_CCTK_PARAMETERS" + NL
//                + IND + IND + "DECLARE_CCTK_FUNCTIONS" + NL + NL
//                + IND + IND + "CCTK_INT, INTENT(IN) :: " + parameters + NL
//                + IND + IND + "logical notEnoughStencil" + NL
//                + IND + IND + "CCTK_INT FOV_threshold" + NL
//                + stencilVars + NL
//                + fovDeclaration
//                + fovCase
//                + IND + IND + "notEnoughStencil = .false." + NL
//                + IND + IND + "FOV_threshold = 0" + NL    
//                + IND + IND + "if (FOV(" + index + ") .le. FOV_threshold) then" + NL
//                + IND + IND + IND + "notEnoughStencil = .true." + NL
//                + IND + IND + "else" + NL
//                + calc
//                + IND + IND + IND + "if (" + cond + ") then" + NL
//                + IND + IND + IND + IND + "notEnoughStencil = .true." + NL
//                + IND + IND + IND + "end if" + NL 
//                + IND + IND + "end if" + NL
//                + IND + IND + "checkStalled = notEnoughStencil" + NL
//                + IND + "end function checkStalled" + NL;
//    }
//    
//    /**
//     * Get the declaration of the variables of the region.
//     * @param region           The region
//     * @param pi                The problem information
//     * @return                  The variable declaration
//     * @throws CGException      CG004 External error
//     */
//    private static String getRegionVariables(Element region, ProblemInfo pi) throws CGException {
//        String result = "";
//        try {
//            NodeList x3dregions = CodeGeneratorUtils.find(region, ".//mms:region//mms:x3dRegionId");
//            for (int i = 0; i < x3dregions.getLength(); i++) {
//                String regionName = x3dregions.item(i).getPreviousSibling().getTextContent();
//                //Get the region
//                X3DRegion x3d = CodeGeneratorUtils.parseX3dRegion(docMan.getDocument(x3dregions.item(i).getTextContent()));    
//                
//                //Fill the point data
//                //X3D is always a 3D point file. If the problem is 2D the last coordinate (Z) will not be generated
//                String pointAttr = x3d.getPointAttr();
//                double[] cactusPoints = null;
//                if (pointAttr.indexOf(",") == -1) {
//                    String[] pointStrings = pointAttr.substring(0, pointAttr.lastIndexOf(" ")).split(" ");
//                    
//                    //X3D is always a 3D point file. If the problem is 2D the last coordinate (Z) will not be generated
//                    cactusPoints = new double[(pointStrings.length)];
//                    for (int j = 0; j < (pointStrings.length) / THREE; j++) {
//                        cactusPoints[j] = Double.valueOf(pointStrings[j * THREE]);
//                        cactusPoints[j + 1 * pointStrings.length / THREE] = Double.valueOf(pointStrings[j * THREE + 1]);
//                        cactusPoints[j + 2 * pointStrings.length / THREE] = Double.valueOf(pointStrings[j * THREE + 2]);
//                    }
//                    
//                } 
//                else {
//                    String[] pointStrings = pointAttr.substring(0, pointAttr.lastIndexOf(",")).split(", ");
//                    Point[] points = new Point[pointStrings.length];
//                    //X3D is always a 3D point file. If the problem is 2D the last coordinate (Z) will not be generated
//                    cactusPoints = new double[THREE * pointStrings.length];
//                    for (int j = 0; j < pointStrings.length; j++) {
//                        points[j] = new Point(pointStrings[j]);
//                        double[] coords = points[j].getCoordinate();
//                        for (int k = 0; k < coords.length; k++) {
//                            cactusPoints[j + k * pointStrings.length] = coords[k];
//                        }
//                    }
//                }
//                
//                //Apply transformations to points
//             //   applyTransformations(x3d, cactusPoints);
//                
//                //Fill the face data (point unions)
//                String coordIndex = x3d.getCoordIndexAttr();
//                String[] unions;
//                //The data could have commas or not...
//                if (coordIndex.indexOf(",") > -1) {
//                    unions = coordIndex.substring(0, coordIndex.lastIndexOf(" -1,")).split(" -1, ");
//                }
//                else {
//                    unions = coordIndex.substring(0, coordIndex.lastIndexOf(" -1")).split(" -1 ");
//                }
//                //2D
//                if (pi.getDimensions() == 2) {
//                    //Unions from 3D to 2D (contour)
//                    unions = CodeGeneratorUtils.removeRepeatedUnions(unions);
//                }
//                
//                //Points
//                //If 2D, remove the interior points
//                if (pi.getDimensions() == 2) {
//                    cactusPoints = CodeGeneratorUtils.removeInteriorPoints(cactusPoints, unions);
//                }
//                
//                int pointSize = cactusPoints.length / pi.getDimensions();
//                int unionSize = unions.length;
//                
//                result = result + IND + IND + "CCTK_REAL " + regionName + "Points(" + pointSize + "," + pi.getDimensions() + ")" 
//                    + NL + IND + IND + "CCTK_INT " + regionName + "Unions(" + unionSize + "," + pi.getDimensions() + ")" + NL;
//            }
//            return result;
//        } 
//        catch (DMException e) {
//            throw new CGException(CGException.CG00X, e.getMessage());
//        }
//    }
//
//    /**
//     * Gets the max domain string value of the problem. Uses MAX to establish the maximum value during execution.
//     * @param gridLimits        The grid limits of the problem
//     * @return                  The string
//     * @throws CGException      CG004 External error
//     */
//    private static String getMaxDomainStringValue(LinkedHashMap<String, CoordinateInfo>  gridLimits) throws CGException {
//        String[][] xslParams = new String [2][2];
//        xslParams[0][0] =  "condition";
//        xslParams[0][1] =  "true";
//        xslParams[1][0] =  "indent";
//        xslParams[1][1] =  "0";
//        
//        String maximumValue = "";
//        int i = 0;
//        String coord = "";
//        Iterator<String> coordIt = gridLimits.keySet().iterator();
//        coordIt = gridLimits.keySet().iterator();
//        while (coordIt.hasNext()) {
//            i++;
//            coord = coordIt.next();
//            maximumValue = maximumValue + "max(" + CactusUtils.xmlTransform(
//                    gridLimits.get(coord).getMin(), xslParams) + ", ";
//            if (i < gridLimits.size()) {
//                maximumValue = maximumValue + "max(" + CactusUtils.xmlTransform(
//                        gridLimits.get(coord).getMax(), xslParams) + ", ";
//            }
//        }
//        maximumValue = maximumValue + CactusUtils.xmlTransform(gridLimits.get(coord).getMax(), xslParams);
//        i = 0;
//        coordIt = gridLimits.keySet().iterator();
//        while (coordIt.hasNext()) {
//            i++;
//            coord = coordIt.next();
//            maximumValue = maximumValue + ")";
//            if (i < gridLimits.size()) {
//                maximumValue = maximumValue + ")";
//            }
//        }
//        return maximumValue;
//    }
//    
//    /**
//     * Gets the max domain floating point value of the problem or -1 if there is any non-float value.
//     * @param gridLimits        The grid limits of the problem
//     * @return                  The value
//     * @throws CGException      CG004 External error
//     */
//    private static double getMaxDomainDoubleValue(LinkedHashMap<String, CoordinateInfo>  gridLimits) throws CGException {
//        String[][] xslParams = new String [2][2];
//        xslParams[0][0] =  "condition";
//        xslParams[0][1] =  "true";
//        xslParams[1][0] =  "indent";
//        xslParams[1][1] =  "0";
//        
//        double maxValue = 0;
//        Iterator<String> coordIt = gridLimits.keySet().iterator();
//        while (coordIt.hasNext()) {
//            String coord = coordIt.next();
//            try {
//                double tmp = Math.abs(CodeGeneratorUtils.toNumber(CactusUtils.xmlTransform(gridLimits.get(coord).getMin(), xslParams)));
//                if (tmp > maxValue) {
//                    maxValue = tmp;
//                }
//                tmp = Math.abs(CodeGeneratorUtils.toNumber(CactusUtils.xmlTransform(gridLimits.get(coord).getMax(), xslParams)));
//                if (tmp > maxValue) {
//                    maxValue = tmp;
//                }
//            }
//            catch (NumberFormatException e) {
//                return -1; 
//            }
//        }
//        return maxValue;
//    }
//    
//    /**
//     * create the floodfill algorithm.
//     * @param regionIds    The identifiers for the regions
//     * @param coordinates   The problem coordinates
//     * @return              The floodfill function
//     */
//    private static String createFloodFillFunction(ArrayList<String> regionIds, ArrayList<String> coordinates) {
//        String cctks = "";
//        String cctks2 = "";
//        String hInit = "";
//        String tassign = "";
//        for (int j = 0; j < coordinates.size(); j++) {
//            String coord = coordinates.get(j);
//            cctks = cctks + "*cctk_lsh(" + (j + 1) + ")";
//            cctks2 = cctks2 + ", cctk_lsh(" + (j + 1) + ")";
//            hInit = hInit + IND + IND + "h" + coord + "(1) = " + coord + NL;
//            tassign = tassign + IND + IND + IND + "t" + coord + " = h" + coord + "(index)" + NL;
//        }
//        cctks = cctks.substring(1);
//        cctks2 = cctks2.substring(2);
//        String index = "";
//        String tindex = "";
//        String arrays = "";
//        String actualIndent = IND + IND;
//        String loop = "";
//        String endLoop = "";
//        String coordFloodFill = "";
//        for (int i = coordinates.size() - 1; i >= 0; i--) {
//            String coord = coordinates.get(i);
//            index = coord + ", " + index;
//            tindex = "t" + coord + ", " + tindex;
//            arrays = "h" + coord + "(" + cctks + "), " + arrays;
//            loop = loop + actualIndent + "do t" + coord + " = 1 , cctk_lsh(" + (i + 1) + ")" + NL;
//            endLoop = actualIndent + "end do" + NL + endLoop;
//            actualIndent = actualIndent + IND;
//            String negIndex = "";
//            String negAssign = "";
//            String posIndex = "";
//            String posAssign = "";
//            for (int j = 0; j < coordinates.size(); j++) {
//                if (i == j) {
//                    negIndex = negIndex + "t" + coord + " - 1, ";
//                    negAssign = negAssign + IND + IND + IND + IND + IND + IND + "h" + coord + "(index) = t" + coord + " - 1" + NL;
//                    posIndex = posIndex + "t" + coord + " + 1, ";
//                    posAssign = posAssign + IND + IND + IND + IND + IND + IND + "h" + coord + "(index) = t" + coord + " + 1" + NL;
//                }
//                else {
//                    negIndex = negIndex + "t" + coordinates.get(j) + ", ";
//                    negAssign = negAssign + IND + IND + IND + IND + IND + IND + "h" + coordinates.get(j) + "(index) = t" 
//                        + coordinates.get(j) + "" + NL;
//                    posIndex = posIndex + "t" + coordinates.get(j) + ", ";
//                    posAssign = posAssign + IND + IND + IND + IND + IND + IND + "h" + coordinates.get(j) + "(index) = t" 
//                        + coordinates.get(j) + "" + NL;
//                }
//            }
//            negIndex = negIndex.substring(0, negIndex.lastIndexOf(", "));
//            posIndex = posIndex.substring(0, posIndex.lastIndexOf(", "));
//            coordFloodFill = coordFloodFill + IND + IND + IND + IND + "if (t" + coord + " - 1 .ge. 1 .and. nonSync(" + negIndex 
//                + ") .eq. 0) then" + NL
//                + IND + IND + IND + IND + IND + "if (.not. control(" + negIndex + ")) then" + NL
//                + IND + IND + IND + IND + IND + IND + "index = index + 1" + NL
//                + negAssign
//                + IND + IND + IND + IND + IND + IND + "control(" + negIndex + ") = .true." + NL
//                + IND + IND + IND + IND + IND + "end if" + NL
//                + IND + IND + IND + IND + "end if" + NL
//                + IND + IND + IND + IND + "if (t" + coord + " + 1 .le. cctk_lsh(" + (i + 1) + ") .and. nonSync(" + posIndex 
//                + ") .eq. 0) then" + NL
//                + IND + IND + IND + IND + IND + "if (.not. control(" + posIndex + ")) then" + NL
//                + IND + IND + IND + IND + IND + IND + "index = index + 1" + NL
//                + posAssign
//                + IND + IND + IND + IND + IND + IND + "control(" + posIndex + ") = .true." + NL
//                + IND + IND + IND + IND + IND + "end if" + NL
//                + IND + IND + IND + IND + "end if" + NL;
//        }
//        tindex = tindex.substring(0, tindex.lastIndexOf(", "));
//        arrays = IND + IND + "CCTK_INT " + arrays.substring(0, arrays.lastIndexOf(",")) + NL;
//        
//        String fovCase = IND + IND + "select case(seg)" + NL;
//        String fovFinalCase = IND + IND + "select case(seg)" + NL;
//        String fovDeclaration = "";
//        String fovAssignment = "";
//        String x2 = "";
//        if (coordinates.size() == 2) {
//            x2 = ",X1AuxiliaryGroup";
//        }
//        if (coordinates.size() == THREE) {
//            x2 = ",X1AuxiliaryGroup,X2AuxiliaryGroup";
//        }
//        for (int i = 0; i < regionIds.size(); i++) {
//            String regionId = regionIds.get(i);
//            if (CodeGeneratorUtils.isNumber(regionId)) {
//                fovCase = fovCase + IND + IND + IND + "case (" + regionId + ")" + NL;
//                fovFinalCase = fovFinalCase + IND + IND + IND + "case (" + regionId + ")" + NL;
//                if (i == 0) {
//                    fovAssignment = fovAssignment + IND + IND + IND + IND + "FOV(" + tindex + ") = 100" + NL;
//                    fovDeclaration = fovDeclaration + IND + IND + "CCTK_INT FOV(X0AuxiliaryGroup" + x2 + ")" + NL;
//                } 
//                else {
//                    fovAssignment = fovAssignment + IND + IND + IND + IND + "FOV" + i + "(" + tindex + ") = 0" + NL;
//                    fovDeclaration = fovDeclaration + IND + IND + "CCTK_INT FOV" + i + "(X0AuxiliaryGroup" + x2 + ")" + NL;
//                }
//                for (int j = 0; j < regionIds.size(); j++) {
//                    String regionId2 = regionIds.get(j);
//                    if (CodeGeneratorUtils.isNumber(regionId2)) {
//                        if (i == j) {
//                            fovCase = fovCase + IND + IND + IND + IND + "FOV = FOV_" + regionId2 + NL;
//                            fovFinalCase = fovFinalCase + IND + IND + IND + IND + "FOV_" + regionId2 + " = FOV" + NL; 
//                        } 
//                        else {
//                            int segId = j + 1;
//                            if (j > i) {
//                                segId = segId - 1;
//                            }
//                            fovCase = fovCase + IND + IND + IND + IND + "FOV" + segId + " = FOV_" + regionId2 + NL;
//                            fovFinalCase = fovFinalCase + IND + IND + IND + IND + "FOV_" + regionId2 + " = FOV" + segId + NL;
//                        }
//                    }
//
//                }
//            }
//        }
//        fovCase = fovCase + IND + IND + "end select" + NL;
//        fovFinalCase = fovFinalCase + IND + IND + "end select" + NL;
//        
//        return IND + "subroutine floodfill(CCTK_ARGUMENTS, " + index + "pred, seg)" + NL
//            + IND + IND + "implicit none" + NL + NL
//            + IND + IND + "DECLARE_CCTK_ARGUMENTS" + NL
//            + IND + IND + "DECLARE_CCTK_PARAMETERS" + NL
//            + IND + IND + "DECLARE_CCTK_FUNCTIONS" + NL
//            + IND + IND + "CCTK_INT, INTENT(IN) :: " + index + "pred, seg" + NL
//            + arrays
//            + IND + IND + "CCTK_INT " + tindex + ", index, loop" + NL
//            + IND + IND + "logical control(" + cctks2 + ")" + NL + NL
//            + fovDeclaration
//            + fovCase
//            + loop
//            + actualIndent + "control(" + tindex + ") = .false." + NL
//            + endLoop + NL
//            + hInit
//            + IND + IND + "index = 1" + NL
//            + IND + IND + "do while (index .ne. 0)" + NL
//            + tassign
//            + IND + IND + IND + "index = index - 1" + NL
//            + IND + IND + IND + "if (nonSync(" + tindex + ") .eq. 0) then" + NL
//            + IND + IND + IND + IND + "nonSync(" + tindex + ") = pred" + NL
//            + IND + IND + IND + IND + "interior(" + tindex + ") = pred" + NL
//            + IND + IND + IND + IND + "if (pred .eq. 2) then" + NL
//            + fovAssignment
//            + IND + IND + IND + IND + "end if" + NL
//            + coordFloodFill
//            + IND + IND + IND + "end if" + NL
//            + IND + IND + "end do" + NL
//            + fovFinalCase
//            + IND + "end subroutine floodfill" + NL;
//    }
//    
//    /**
//     * Generates the code to fill the faces of the region.
//     * It uses geometry formulas to recognize when a point is inside the face.
//     * To do this a local coordinate system for the face is used. 
//     * The local coordinate points are transformed to the global coordinate system.
//     * See:  Finite Element Methods. Formulation for MoL and mesh refinement.
//     *                      A. Arbona
//     *        IAC 3 -UIB, 07120, Palma de Mallorca
//     * @param pi                Problem info
//     * @param regionInfo       The region information
//     * @param gridLimit         The limits of the grid coordinates
//     * @param minGrid           The minimum values of the grid
//     * @param delta             The delta values
//     * @param unionNumber       The number of unions
//     * @return                  The generated code
//     * @throws CGException      CG004 External error
//     */
//    private static String faceFilling(ProblemInfo pi, MappingRegionInfo regionInfo, 
//            LinkedHashMap<String, CoordinateInfo> gridLimit, String[] minGrid, String[] delta, int unionNumber) throws CGException {
//        String[][] xslParams = new String [2][2];
//        xslParams[0][0] = "condition";
//        xslParams[0][1] = "true";
//        xslParams[1][0] = "indent";
//        xslParams[1][1] = "0";
//        ArrayList<String> coordinates = pi.getCoordinates();
//        String regionName = regionInfo.getElement().getFirstChild().getTextContent();
//        String actualIndent = IND + IND;
//        String maxMin = "";
//        String indexComparator = "";
//        String terms = "";
//        String term = "";
//        String operator = "FLOOR";
//        if (pi.getDimensions() == 2) {
//            operator = "IDNINT";
//        }
//        Iterator<String> coordIt = regionInfo.getCoordinateLimits().keySet().iterator();
//        int i = 0;
//        while (coordIt.hasNext()) {
//            term = term + actualIndent + IND + IND + coordinates.get(i) + "term = " + operator + "(1 + (" + regionName + "Points(" 
//                + regionName + "Unions(unionsI, facePointI), " + (i + 1) + ")" + "-(" + minGrid[i] + ")) / "  + delta[i] + ")" + NL;
//            terms = terms + actualIndent + IND + IND + IND + IND + IND + IND + coordinates.get(i) 
//                + "term = " + operator + "((1 + ((" + regionName + "Points(" + regionName + "Unions(unionsI, 1), " + (i + 1) + ")) - (" 
//                + minGrid[i] + ")) / " + delta[i] + ") * e1 + (1 + ((" + regionName + "Points(" + regionName + "Unions(unionsI, 2), " 
//                + (i + 1) + ")) - (" + minGrid[i] + ")) / " + delta[i] + ") * e2 + (1 + ((" + regionName + "Points(" + regionName 
//                + "Unions(unionsI, 3), " + (i + 1) + ")) - (" + minGrid[i] + ")) / " + delta[i] + ") * e3)" + NL;
//        
//            
//            //Index for the initial assignment of max and min coordinates for face filling process
//            maxMin = maxMin + actualIndent + IND + IND + IND + "minBlock(" + (i + 1) + ") = " + coordinates.get(i) + "term" + NL
//                 + actualIndent + IND + IND + IND + "maxBlock(" + (i + 1) + ") = " + coordinates.get(i) + "term" + NL;
//            //Comparations of the index
//            indexComparator = indexComparator + actualIndent + IND + IND + IND + "if (" + coordinates.get(i) 
//                + "term .lt. minBlock(" + (i + 1) + ")) then" + NL
//                + actualIndent + IND + IND + IND + IND + "minBlock(" + (i + 1) + ") = " + coordinates.get(i) + "term" + NL
//                + actualIndent + IND + IND + IND + "end if" + NL
//                + actualIndent + IND + IND + IND + "if (" + coordinates.get(i) + "term .gt. maxBlock(" + (i + 1) + ")) then" + NL
//                + actualIndent + IND + IND + IND + IND + "maxBlock(" + (i + 1) + ") = " + coordinates.get(i) + "term" + NL
//                + actualIndent + IND + IND + IND + "end if" + NL;
//            i++;
//        }
//        //Fill the points in the faces
//        String insidePatch = "";
//        String maxDistance = "";
//        String insidePatchPoint = "";
//        for (int j = coordinates.size() - 1; j >= 0; j--) {
//            insidePatch = "minBlock(" + (j + 1) + ") .le. cctk_ubnd(" + (j + 1) + ") + 1 .and. maxBlock(" + (j + 1) + ") .ge. cctk_lbnd(" 
//                + (j + 1) + ") + 1 .and. " + insidePatch;
//            insidePatchPoint = coordinates.get(j) + "term .ge. cctk_lbnd(" + (j + 1) + ") + 1 .and. " + coordinates.get(j) 
//                + "term .le. cctk_ubnd(" + (j + 1) + ") + 1 .and." + insidePatchPoint;
//            maxDistance = maxDistance + "ABS(minBlock(" + (j + 1) + ") - maxBlock(" + (j + 1) + ")),";
//        }
//        insidePatchPoint = insidePatchPoint.substring(0, insidePatchPoint.lastIndexOf(" .and."));
//        maxDistance = maxDistance.substring(0, maxDistance.lastIndexOf(","));
//        insidePatch = insidePatch.substring(0, insidePatch.lastIndexOf(" .and."));
//        return "!" + IND + IND + "Fill the faces" + NL
//            + actualIndent + "do unionsI = 1, " + unionNumber + NL
//            + actualIndent + IND + "do facePointI = 1, " + pi.getDimensions() + NL
//            + term + "!" + actualIndent + IND + IND + "Take maximum and minimum coordinates of the face" + NL
//            + actualIndent + IND + IND + "if (facePointI .eq. 1) then" + NL
//            + maxMin + actualIndent + IND + IND + "else" + NL
//            + indexComparator + actualIndent + IND + IND + "end if" + NL
//            + actualIndent + IND + "end do" + NL
//            + IND + IND + IND + "if (" + insidePatch + ") then" + NL
//            + IND + IND + IND + IND + "maxDistance = MAX(" + maxDistance + ")" + NL
//            + fillFace(regionName, pi, coordinates, terms, insidePatchPoint, regionInfo.getId(), delta)
//            + IND + IND + IND + "end if" + NL
//            + actualIndent + "end do" + NL;
//    }
//    
//    /**
//     * Generates the code to check the stencil width at the points in the triangle.
//     * @param regionName           The name of the region
//     * @param pi                    Problem information
//     * @param coordinates           The coordinates of the problem
//     * @param terms                 The conversion terms from the geometric algorithm
//     * @param insidePatchPoint      The code that checks if the point is inside the patch
//     * @param regionId             The region id
//     * @param delta                 The delta values
//     * @return                      The code
//     * @throws CGException          CG00X External error
//     */
//    private static String fillFace(String regionName, ProblemInfo pi, ArrayList<String> coordinates, String terms, String insidePatchPoint, 
//            int regionId, String[] delta) throws CGException {
//        String actualIndent = IND + IND + IND + IND;
//        
//        String termIndex = "";
//        for (int j = coordinates.size() - 1; j >= 0; j--) {
//            termIndex = "index" + coordinates.get(j) + ", " + termIndex;
//        }
//        termIndex = termIndex.substring(0, termIndex.lastIndexOf(","));
//        if (pi.getDimensions() == 2) {
//            return actualIndent + "do j = minBlock(2), maxBlock(2)" + NL
//                + actualIndent + IND + "do i = minBlock(1), maxBlock(1)" + NL
//                + actualIndent + IND + IND + "s1 = (" + regionName + "Points(" + regionName + "Unions(unionsI, 1), 1) - (i - 1 - 0.5)*" + delta[0] 
//                + ")*(" + regionName + "Points(" + regionName + "Unions(unionsI, 2),2) - (j - 1 - 0.5)*" + delta[1] + ") .ge. (" + regionName 
//                + "Points(" + regionName + "Unions(unionsI, 2), 1) - (i - 1 - 0.5)*" + delta[0] + ")*(" + regionName + "Points(" + regionName 
//                + "Unions(unionsI, 1), 2) - (j - 1 - 0.5)*" + delta[1] + ")" + NL
//                + actualIndent + IND + IND + "s2 = (" + regionName + "Points(" + regionName + "Unions(unionsI, 1), 1) - (i - 1 + 0.5)*" + delta[0] 
//                + ")*(" + regionName + "Points(" + regionName + "Unions(unionsI, 2), 2) - (j - 1 - 0.5)*" + delta[1] + ") .ge. (" + regionName 
//                + "Points(" + regionName + "Unions(unionsI, 2), 1) - (i - 1 + 0.5)*" + delta[0] + ")*(" + regionName + "Points(" + regionName 
//                + "Unions(unionsI, 1), 2) - (j - 1 - 0.5)*" + delta[1] + ")" + NL
//                + actualIndent + IND + IND + "s3 = (" + regionName + "Points(" + regionName + "Unions(unionsI, 1), 1) - (i - 1 - 0.5)*" + delta[0] 
//                + ")*(" + regionName + "Points(" + regionName + "Unions(unionsI, 2), 2) - (j - 1 + 0.5)*" + delta[1] + ") .ge. (" + regionName 
//                + "Points(" + regionName + "Unions(unionsI, 2), 1) - (i - 1 - 0.5)*" + delta[0] + ")*(" + regionName + "Points(" + regionName 
//                + "Unions(unionsI, 1), 2) - (j - 1 + 0.5)*" + delta[1] + ")" + NL
//                + actualIndent + IND + IND + "s4 = (" + regionName + "Points(" + regionName + "Unions(unionsI, 1), 1) - (i - 1 + 0.5)*" + delta[0] 
//                + ")*(" + regionName + "Points(" + regionName + "Unions(unionsI, 2), 2) - (j - 1 + 0.5)*" + delta[1] + ") .ge. (" + regionName 
//                + "Points(" + regionName + "Unions(unionsI, 2), 1) - (i - 1 + 0.5)*" + delta[0] + ")*(" + regionName + "Points(" + regionName 
//                + "Unions(unionsI, 1), 1) - (j - 1 + 0.5)*" + delta[1] + ")" + NL
//                + actualIndent + IND + IND + "if (.not.((s1 .and. s2 .and. s3 .and. s4) .or. ((.not. s1) .and. (.not. s2) .and. (.not. s3) " 
//                + ".and. (.not. s4)))) then" + NL
//                + actualIndent + IND + IND + IND + "if (i .ge. 1 .and. i .le. cctk_lsh(1) .and. j .ge. 1 .and. j .le. cctk_lsh(2)) then" + NL
//                + x3DIndexCorrection(actualIndent + IND + IND + IND, pi.getCoordinates(), pi.getSchemaStencil(), true)
//                + assignFOV(pi.getProblem(), regionId, pi.getRegionIds(), termIndex, pi.getCoordinates(), 
//                        actualIndent + IND + IND + IND + IND)
//                + actualIndent + IND + IND + IND + "end if" + NL
//                + actualIndent + IND + IND + "end if" + NL
//                + actualIndent + IND + "end do" + NL
//                + actualIndent + "end do" + NL;
//        }
//        else {
//            return actualIndent + "do ie3 = 0, 2 * maxDistance" + NL
//                + actualIndent + IND + "e3 = ie3 / (2 * maxDistance)" + NL 
//                + actualIndent + IND + "do ie2 = 0, 2 * maxDistance" + NL
//                + actualIndent + IND + IND + "e2 = ie2 / (2 * maxDistance)" + NL
//                + actualIndent + IND + IND + "do ie1 = 0, 2 * maxDistance" + NL
//                + actualIndent + IND + IND + IND + "e1 = ie1 / (2 * maxDistance)" + NL 
//                + actualIndent + IND + IND + IND + "if (ABS(ie1 + ie2 + ie3 - 2 * maxDistance) .le. SQRT3INV) then" 
//                + NL + terms 
//                + actualIndent + IND + IND + IND + IND + "if (" + insidePatchPoint + ") then" + NL
//                + x3DIndexCorrection(actualIndent + IND + IND + IND + IND, pi.getCoordinates(), pi.getSchemaStencil(), false)
//                + assignFOV(pi.getProblem(), regionId, pi.getRegionIds(), termIndex, pi.getCoordinates(), 
//                        actualIndent + IND + IND + IND + IND)
//                + actualIndent + IND + IND + IND + IND + "end if" + NL
//                + actualIndent + IND + IND + IND + "end if" + NL
//                + actualIndent + IND + IND + "end do" + NL
//                + actualIndent + IND + "end do" + NL
//                + actualIndent + "end do" + NL;
//        }
//    }
//    
//    /**
//     * Generates the x3d index correction code. It corrects the limited upper sided x3d points not to be at boundaries, but at the internal domain.
//     * @param indent        The indent
//     * @param coordinates   The coordinates
//     * @param stencil       The stencil of the problem
//     * @param twoD          if the code is for two dimensions
//     * @return              The code
//     */
//    private static String x3DIndexCorrection(String indent, ArrayList<String> coordinates, int stencil, boolean twoD) {
//        String index = "";
//        String conditions = "";
//        for (int i = 0; i < coordinates.size(); i++) {
//            String coord = coordinates.get(i);
//            if (twoD) {
//                index = index + indent + "index" + coord + " = " + coord + NL;
//                conditions = conditions + indent + "if ((cctk_bbox(" + (2 * i + 2) + ") .eq. 1) .and. (index" + coord 
//                        + " .eq. cctk_ubnd(" + (i + 1) + ") - " + stencil + ")) then" + NL
//                        + indent + "index" + coord + " = index" + coord + " - 1" + NL
//                        + indent + "end if" + NL;
//            }
//            else {
//                index = index + indent + "index" + coord + " = " + coord + "term - cctk_lbnd(" + (i + 1) + ")" + NL; 
//                conditions = conditions + "!" + indent + "To use when the domain redefinition ticket was solved" + NL
//                        + "!" + indent + "if ((cctk_bbox(" + (2 * i + 2) + ") .eq. 1) .and. (index" + coord 
//                        + " .eq. cctk_ubnd(" + (i + 1) + ") - " + stencil + ")) then" + NL
//                        + "!" + indent + "index" + coord + " = index" + coord + " - 1" + NL
//                        + "!" + indent + "end if" + NL;
//            }
//
//        }
//        return index + conditions;
//    }
//    
//    /**
//     * Creates the code to fill the interior of a x3d region.
//     * It surrounds the region with a square and goes through all the coordinates 
//     * line by line recognizing when the point is inside or outside the region.
//     * To do this it checks when a limit of the region is trespassed.
//     * @param pi                The problem information
//     * @param regionId         The region identifier
//     * @param problemIdentifier The name of the problem
//     * @param stencil           The problem stencil
//     * @return                  The generated code
//     */
//    private static String fillx3dInterior(ProblemInfo pi, int regionId, String problemIdentifier, int stencil) {
//        String mapLimits = "";
//        String index = "";
//        String init5ILoop = "";
//        String end5ILoop = "";
//        String end5ILoop2 = "";
//        String init4ILoop = "";
//        String end4ILoop = "";
//        String end4ILoop2 = "";
//        ArrayList<String> coordinates = pi.getCoordinates();
//        for (int i = 0; i < coordinates.size(); i++) {
//            String coord = coordinates.get(i);
//            mapLimits = mapLimits + IND + IND + coord + "MapStart = 1" + NL
//                + IND + IND + coord + "MapEnd = cctk_lsh(" + (i + 1) + ")" + NL;
//            init5ILoop = IND + IND + IND + IND + IND + "do " + coord + " = " + coord + "MapStart, " + coord + "MapEnd" + NL 
//                + init5ILoop;
//            init4ILoop = IND + IND + IND + IND + "do " + coord + " = " + coord + "l, " + coord + "u" + NL + init4ILoop;
//            index = index + coord + ", ";
//            end4ILoop = end4ILoop + IND + IND + IND + IND + "end do" + NL;
//            end5ILoop = end5ILoop + IND + IND + IND + IND + IND + "end do" + NL;
//            end4ILoop2 = end4ILoop2 + IND + IND + IND + IND + "if (finished .eq. 0) then" + NL
//                    + IND + IND + IND + IND + IND + "exit" + NL
//                    + IND + IND + IND + IND + "end if" + NL
//                    + IND + IND + IND + IND + "end do" + NL;
//            end5ILoop2 = end5ILoop2 + IND + IND + IND + IND + IND + "if (working .eq. 1) then" + NL
//                    + IND + IND + IND + IND + IND + IND + "exit" + NL
//                    + IND + IND + IND + IND + IND + "end if" + NL
//                    + IND + IND + IND + IND + IND + "end do" + NL;
//        }
//        index = index.substring(0, index.lastIndexOf(", "));
//        
//        return "!" + IND + IND + "Fill the interior of the region" + NL
//            + IND + IND + "call CCTK_ReductionArrayHandle(reduction_handle,\"sum\")" + NL
//            + mapLimits + NL
//            + initializationFloodfill(pi, regionId, problemIdentifier, coordinates, stencil)
//            + IND + IND + IND + "do while (.not. finishedGlobal)" + NL
//            + IND + IND + IND + IND + "workingGlobal = .true." + NL
//            + "!" + IND + IND + IND + IND + "Filling started floodfill in all the patches" + NL
//            + IND + IND + IND + IND + "do while (workingGlobal)" + NL
//            + IND + IND + IND + IND + IND + "working = 0" + NL
//            + init5ILoop
//            + IND + IND + IND + IND + IND + IND + "if (working .eq. 0) then" + NL
//            + IND + IND + IND + IND + IND + IND + IND + "if (nonSync(" + index + ") .ne. interior(" +  index 
//            + ") .and. nonSync(" + index + ") .eq. 0) then" + NL
//            + IND + IND + IND + IND + IND + IND + IND + IND + "call floodfill(CCTK_PASS_FTOF, " + index + ", pred, " 
//            + regionId + ")" + NL
//            + IND + IND + IND + IND + IND + IND + IND + IND + "working = 1" + NL
//            + IND + IND + IND + IND + IND + IND + IND + "end if" + NL
//            + IND + IND + IND + IND + IND + IND + "end if" + NL
//            + end5ILoop 
//            + IND + IND + IND + IND + IND + "call CCTK_SyncGroup(status, cctkGH, \"" + problemIdentifier + "::MapGroup\")" + NL
//            + "!" + IND + IND + IND + IND + IND + "Local working variable communication" + NL
//            + IND + IND + IND + IND + IND + "call CCTK_ReduceLocalScalar(status, cctkGH, -1, reduction_handle,working, tmp1, " 
//            + "CCTK_VARIABLE_INT)" + NL
//            + IND + IND + IND + IND + IND + "if (tmp1 .gt. 0) then" + NL
//            + IND + IND + IND + IND + IND + IND + "workingGlobal = .true." + NL
//            + IND + IND + IND + IND + IND + "else" + NL
//            + IND + IND + IND + IND + IND + IND + "workingGlobal = .false." + NL
//            + IND + IND + IND + IND + IND + "end if" + NL
//            + IND + IND + IND + IND + "end do" + NL + NL
//            + IND + IND + IND + IND + "finished = 1" + NL
//            + init4ILoop
//            + IND + IND + IND + IND + IND + "if (nonSync(" + index + ") .eq. 0) then" + NL
//            + IND + IND + IND + IND + IND + IND + "finished = 0" + NL
//            + IND + IND + IND + IND + IND + "end if" + NL
//            + end4ILoop
//            + "!" + IND + IND + IND + IND + "Finished local variables communication" + NL
//            + IND + IND + IND + IND + "finishedGlobal = .true." + NL
//            + IND + IND + IND + IND + "first = -1" + NL
//            + IND + IND + IND + IND + "do proc = 1, nodes" + NL
//            + IND + IND + IND + IND + IND + "if (proc .eq. cctk_myProc(cctkGH) + 1) then" + NL
//            + IND + IND + IND + IND + IND + IND + "tmp1 = finished" + NL
//            + IND + IND + IND + IND + IND + "else" + NL
//            + IND + IND + IND + IND + IND + IND + "tmp1 = 0" + NL
//            + IND + IND + IND + IND + IND + "end if" + NL
//            + IND + IND + IND + IND + IND + "call CCTK_ReduceLocalScalar(status, cctkGH, -1, reduction_handle, tmp1, " 
//            + "tmp2, CCTK_VARIABLE_INT)" + NL
//            + IND + IND + IND + IND + IND + "if (tmp2 .eq. 0) then" + NL
//            + IND + IND + IND + IND + IND + IND + "finishedGlobal = .false." + NL
//            + IND + IND + IND + IND + IND + IND + "if (first .lt. 0) then" + NL
//            + IND + IND + IND + IND + IND + IND + IND + "first = proc" + NL
//            + IND + IND + IND + IND + IND + IND + "end if" + NL
//            + IND + IND + IND + IND + IND + "end if" + NL
//            + IND + IND + IND + IND + "end do" + NL + NL
//            + IND + IND + IND + IND + "if (pred .eq. 1) then" + NL
//            + IND + IND + IND + IND + IND + "pred = 2" + NL
//            + IND + IND + IND + IND + "else" + NL
//            + IND + IND + IND + IND + IND + "pred = 1" + NL
//            + IND + IND + IND + IND + "end if" + NL + NL
//            + IND + IND + IND + IND +  "if (first .eq. cctk_myProc(cctkGH) .and. .not. finishedGlobal) then" + NL
//            + init5ILoop
//            + IND + IND + IND + IND + IND + IND + "if (nonSync(" + index + ") .eq. 0 .and. working .eq. 0) then" + NL
//            + IND + IND + IND + IND + IND + IND + IND + "call floodfill(CCTK_PASS_FTOF, " + index + ", pred, " 
//            + regionId + ")" + NL
//            + IND + IND + IND + IND + IND + IND + IND + "working = 1" + NL
//            + IND + IND + IND + IND + IND + IND + "end if" + NL
//            + end5ILoop
//            + IND + IND + IND + IND + "end if" + NL + NL
//            + IND + IND + IND + IND + "call CCTK_SyncGroup(status, cctkGH, \"" + problemIdentifier + "::MapGroup\")" + NL
//            + IND + IND + IND + "end do" + NL
//            + "!" + IND + IND + "Only one processor" + NL
//            + IND + IND + "else" + NL
//            + IND + IND + IND + "do while (finished .eq. 0)" + NL
//            + IND + IND + IND + IND + "finished = 1" + NL
//            + init4ILoop 
//            + IND + IND + IND + IND + IND + "if (nonSync(" + index + ") .eq. 0 .and. finished .eq. 1) then" + NL
//            + IND + IND + IND + IND + IND + IND + "finished = 0" + NL
//            + IND + IND + IND + IND + IND + IND + "exit" + NL
//            + IND + IND + IND + IND + IND + "end if" + NL
//            + end4ILoop2 + NL
//            + IND + IND + IND + IND + "if (finished .eq. 0) then" + NL
//            + IND + IND + IND + IND + IND + "working = 0" + NL
//            + init5ILoop
//            + IND + IND + IND + IND + IND + IND + "if (nonSync(" + index + ") .eq. 0 .and. working .eq. 0) then" + NL
//            + IND + IND + IND + IND + IND + IND + IND + "if (pred .eq. 1) then" + NL
//            + IND + IND + IND + IND + IND + IND + IND + IND + "pred = 2" + NL
//            + IND + IND + IND + IND + IND + IND + IND + "else" + NL
//            + IND + IND + IND + IND + IND + IND + IND + IND + "pred = 1" + NL
//            + IND + IND + IND + IND + IND + IND + IND + "end if" + NL
//            + IND + IND + IND + IND + IND + IND + IND + "call floodfill(CCTK_PASS_FTOF, " + index + ", pred, " 
//            + regionId + ")" + NL
//            + IND + IND + IND + IND + IND + IND + IND + "working = 1" + NL
//            + IND + IND + IND + IND + IND + IND + IND + "exit" + NL
//            + IND + IND + IND + IND + IND + IND + "end if" + NL
//            + end5ILoop2
//            + IND + IND + IND + IND + "end if" + NL
//            + IND + IND + IND + "end do" + NL
//            + IND + IND + "end if" + NL + NL;
//    }
// 
//    /**
//     * Create the code for the floodfill algorith initialization. Including the initial floodfill call.
//     * @param pi                    The problem information
//     * @param regionId             The region identifier
//     * @param problemIdentifier     The name of the problem
//     * @param coordinates           The coordinates of the problem
//     * @param stencil               The problem stencil
//     * @return                      The code
//     */
//    private static String initializationFloodfill(ProblemInfo pi, int regionId, String problemIdentifier, ArrayList<String> coordinates, 
//            int stencil) {
//        String initLoop = "";
//        String endLoop = "";
//        String index = "";
//        String initIndex = "";
//        String condition = "";
//        String finishLimits = "";
//        for (int i = 0; i < coordinates.size(); i++) {
//            String coord = coordinates.get(i);
//            initLoop = IND + IND + "do " + coord + " = " + coord + "MapStart, " + coord + "MapEnd" + NL + initLoop;
//            index = index + coord + ", ";
//            endLoop = endLoop + IND + IND + "end do" + NL;
//            initIndex = initIndex + "1, ";
//            condition = condition + "cctk_lbnd(" + (i + 1) + ") .eq. 0 .and. ";
//            finishLimits = finishLimits + IND + IND + coord + "l = " +  coord + "MapStart" + NL
//                + IND + IND + "if (cctk_bbox(" + (2 * i + 1) + ") .eq. 0) then" + NL
//                + IND + IND + IND + coord + "l = " + coord + "MapStart + " + stencil + NL
//                + IND + IND + "end if" + NL
//                + IND + IND + coord + "u = " + coord + "MapEnd" + NL
//                + IND + IND + "if (cctk_bbox(" + (2 * i + 2) + ") .eq. 0) then" + NL
//                + IND + IND + IND + coord + "u = " + coord + "MapEnd - " + (2 * stencil) + NL
//                + IND + IND + "end if" + NL;
//        }
//        index = index.substring(0, index.lastIndexOf(", "));
//        condition = condition.substring(0, condition.lastIndexOf(" .and."));
//        
//        String segCond = "";
//        
//        if (pi.getRegionIds().contains(String.valueOf(regionId))) {
//            segCond = segCond + "FOV_" + regionId + "(" + index + ") .gt. 0";
//        }
//        if (pi.getRegionIds().contains(String.valueOf(regionId)) && pi.getRegionIds().contains(String.valueOf(regionId + 1))) {
//            segCond = segCond + " .or. ";
//        }
//        if (pi.getRegionIds().contains(String.valueOf(regionId + 1))) {
//            segCond = segCond + "FOV_" + (regionId + 1) + "(" + index + ") .gt. 0";
//        }
//        
//        return "!" + IND + IND + "Initialize the variables and set the walls as exterior" + NL
//            + initLoop
//            + "!" + IND + IND + IND + "If wall, then it is considered exterior" + NL
//            + IND + IND + IND + "if (" + segCond + ") then" + NL
//            + IND + IND + IND + IND + "interior(" + index + ") = 1" + NL
//            + IND + IND + IND + IND + "nonSync(" + index + ") = 1" + NL
//            + IND + IND + IND + "else" + NL
//            + IND + IND + IND + IND + "interior(" + index + ") = 0" + NL
//            + IND + IND + IND + IND + "nonSync(" + index + ") = 0" + NL
//            + IND + IND + IND + "end if" + NL
//            + endLoop + NL
//            + "!" + IND + IND + "Initial floodfill" + NL
//            + IND + IND + "finishedGlobal = .false." + NL
//            + IND + IND + "finished = 0" + NL
//            + IND + IND + "pred = 1" + NL
//            + "!" + IND + IND + "Limits for the finished checking routine to avoid internal boundary problems" + NL
//            + finishLimits + NL
//            + IND + IND + "nodes = CCTK_nProcs(cctkGH)" + NL
//            + IND + IND + "if (" + condition + ") then" + NL
//            + IND + IND + IND + "call floodfill(CCTK_PASS_FTOF, " + initIndex + "1, " + regionId + ")" + NL
//            + IND + IND + IND + "working = 1" + NL
//            + IND + IND + "end if" + NL + NL
//            + "!" + IND + IND + "Multiprocessor" + NL
//            + IND + IND + "if (nodes .gt. 1) then" + NL
//            + IND + IND + IND + "call CCTK_SyncGroup(status, cctkGH, \"" + problemIdentifier + "::MapGroup\")" + NL;
//    }
//    
//    /**
//     * Maps the mathematical region interior.
//     * @param pi                The problem information
//     * @param regionInfo       The region information
//     * @param gridLimit         The limits of the grid coordinates
//     * @param coordinates       The coordinates
//     * @param minGrid           The minimum values of the grid
//     * @param maxGrid           The maximum values of the grid
//     * @param delta             The delta values
//     * @return                  The mapping
//     * @throws CGException      CG004 External error
//     */
//    private static String mapMathematicalInterior(ProblemInfo pi, MappingRegionInfo regionInfo, LinkedHashMap<String, CoordinateInfo> gridLimit, 
//            ArrayList<String> coordinates, String[] minGrid, String[] maxGrid, String[] delta) throws CGException {
//        String[][] xslParams = new String [2][2];
//        xslParams[0][0] = "condition";
//        xslParams[0][1] = "false";
//        xslParams[1][0] = "indent";
//        xslParams[1][1] = "0";
//        
//        String regionName = regionInfo.getElement().getFirstChild().getTextContent();
//        LinkedHashMap<String, CoordinateInfo> regionLimit = regionInfo.getCoordinateLimits();
//        
//        String loop = "";
//        String endLoop = "";
//        String index = "";
//        String preLoop = "";
//        String regionCondition = "";
//        String coordinateTransform = "";
//        String indent = IND + IND + IND;
//        Iterator<String> coordIt = regionLimit.keySet().iterator();
//        int i = 0;
//        while (coordIt.hasNext()) {
//            String coord = coordIt.next();
//            String minValue = CactusUtils.xmlTransform(regionLimit.get(coord).getMin(), xslParams);
//            String maxValue = CactusUtils.xmlTransform(regionLimit.get(coord).getMax(), xslParams);
//            String minValueOriginal = minValue;
//            String maxValueOriginal = maxValue;
//            //Minimum value
//            if (minGrid[i].equals(minValue)) {
//                minValue = "1";
//            }
//            else {
//                if (CodeGeneratorUtils.isNumber(minValue) && CodeGeneratorUtils.isNumber(minGrid[i])) {
//                    minValue = "FLOOR(1 + " + (CodeGeneratorUtils.toNumber(minValue) - CodeGeneratorUtils.toNumber(minGrid[i])) + " / " 
//                        + delta[i] + ") - cctk_lbnd(" + (i + 1) + ")";
//                }
//                else {
//                    minValue = "FLOOR(1 + (" + minValue + " - (" + minGrid[i] + ")) / " + delta[i] + ") - cctk_lbnd(" + (i + 1) + ")";
//                }
//            }
//            regionCondition = regionCondition + minValue + " .le. cctk_ubnd(" + (i + 1) + ") - cctk_lbnd(" + (i + 1) + ") + 1 .and. ";
//            //Maximum value
//            if (maxGrid[i].equals(maxValue)) {
//                maxValue = "cctk_lsh(" + (i + 1) + ")";
//            }
//            else {
//                if (CodeGeneratorUtils.isNumber(maxValue) && CodeGeneratorUtils.isNumber(minGrid[i])) {
//                    maxValue = "FLOOR(1 + " + (CodeGeneratorUtils.toNumber(maxValue) - CodeGeneratorUtils.toNumber(minGrid[i])) + " / " 
//                        + delta[i] + ") - cctk_lbnd(" + (i + 1) + ")";
//                }
//                else {
//                    maxValue = "FLOOR(1 + (" + maxValue + " - (" + minGrid[i] + ")) / " + delta[i] + ") - cctk_lbnd(" + (i + 1) + ")";
//                }
//                regionCondition = regionCondition + maxValue + " .ge. 1 .and. ";
//            }
//
//            if (minGrid[i].equals(minValueOriginal)) {
//                preLoop = preLoop + IND + IND + IND + coord + "MapStart = " + minValue + NL;
//            }
//            else {
//                preLoop = preLoop + IND + IND + IND + "if (" + minValue + " .gt. 1) then" + NL
//                    + IND + IND + IND + IND + coord + "MapStart = " + minValue + NL
//                    + IND + IND + IND + "else" + NL
//                    + IND + IND + IND + IND + coord + "MapStart = 1" + NL
//                    + IND + IND + IND + "end if" + NL;
//            }
//            if (maxGrid[i].equals(maxValueOriginal)) {
//                preLoop = preLoop + IND + IND + IND + coord + "MapEnd = " + maxValue + NL;
//            }
//            else {
//                preLoop = preLoop + IND + IND + IND + "if (" + maxValue + " .lt. cctk_ubnd(" + (i + 1) + ") - cctk_lbnd(" 
//                    + (i + 1) + ") + 1) then" + NL
//                    + IND + IND + IND + IND + coord + "MapEnd = " + maxValue + NL
//                    + IND + IND + IND + "else" + NL
//                    + IND + IND + IND + IND + coord + "MapEnd = cctk_lsh(" + (i + 1) + ")" + NL
//                    + IND + IND + IND + "end if" + NL;
//            }
//            
//            loop = loop + indent + "do " + coordinates.get(i) + " = " + coordinates.get(i) + "MapStart, " 
//                + coordinates.get(i) + "MapEnd" + NL;
//            index = index + coordinates.get(i) + ", ";
//            endLoop = indent + "end do" + NL + endLoop;
//            indent = indent + IND;
//            i++;
//        }
//        String loopsStencil = "";
//        if (pi.getSchemaStencil() > 1) {
//            loopsStencil = loopsStencil + checkStencil(pi, regionInfo, IND + IND + IND);
//        }
//        regionCondition = regionCondition.substring(0, regionCondition.lastIndexOf(" .and."));
//        index = index.substring(0, index.lastIndexOf(","));
//        return "!" + IND + IND + "Region: " + regionName + "I" + NL 
//            + coordinateTransform
//            + IND + IND + "if (" + regionCondition + ") then" + NL 
//            + preLoop + loop
//            + assignFOV(pi.getProblem(), regionInfo.getId(), pi.getRegionIds(), index, pi.getCoordinates(), indent + IND)
//            + endLoop
//            + loopsStencil
//            + IND + IND + "end if" + NL;
//    }
//    
//    /**
//     * Maps the mathematical region surface.
//     * @param pi                The problem information
//     * @param regionInfo       The region information
//     * @param gridLimit         The limits of the grid coordinates
//     * @param minGrid           The minimum values of the grid
//     * @param maxGrid           The maximum values of the grid
//     * @param delta             The delta values
//     * @return                  The mapping
//     * @throws CGException      CG004 External error
//     */
//    private static String mapMathematicalSurface(ProblemInfo pi, MappingRegionInfo regionInfo, LinkedHashMap<String, CoordinateInfo> gridLimit, 
//            String[] minGrid, String[] maxGrid, String[] delta) throws CGException {
//        String[][] xslParams = new String [2][2];
//        xslParams[0][0] = "condition";
//        xslParams[0][1] = "false";
//        xslParams[1][0] = "indent";
//        xslParams[1][1] = "0";
//        
//        ArrayList<String> coordinates = pi.getCoordinates();
//        String regionName = regionInfo.getElement().getFirstChild().getTextContent();
//        LinkedHashMap<String, CoordinateInfo> regionLimit = regionInfo.getCoordinateLimits();
//        String regionCondition = "";
//        String coordinateTransform = "";
//        String[] minValue = new String[regionLimit.size()];
//        String[] maxValue = new String[regionLimit.size()];
//        String[] minValueInit = new String[regionLimit.size()];
//        String[] maxValueInit = new String[regionLimit.size()];
//        Iterator<String> coordIt = regionLimit.keySet().iterator();
//        int i = 0;
//        while (coordIt.hasNext()) {
//            String coord = coordIt.next();
//            minValueInit[i] = CactusUtils.xmlTransform(regionLimit.get(coord).getMin(), xslParams);
//            maxValueInit[i] = CactusUtils.xmlTransform(regionLimit.get(coord).getMax(), xslParams);
//            //Minimum value
//            if (minGrid[i].equals(minValueInit[i])) {
//                minValue[i] = "1";
//            }
//            else {
//                if (CodeGeneratorUtils.isNumber(minValueInit[i]) && CodeGeneratorUtils.isNumber(minGrid[i])) {
//                    minValue[i] = "FLOOR(1 + " + (CodeGeneratorUtils.toNumber(minValueInit[i]) - CodeGeneratorUtils.toNumber(minGrid[i])) + " / " 
//                        + delta[i] + ") - cctk_lbnd(" + (i + 1) + ")";
//                }
//                else {
//                    minValue[i] = "FLOOR(1 + (" + minValueInit[i] + " - (" + minGrid[i] + ")) / " + delta[i] + ") - cctk_lbnd(" + (i + 1) + ")";
//                }
//            }
//            regionCondition = regionCondition + minValue[i] + " .le. cctk_ubnd(" + (i + 1) + ") - cctk_lbnd(" + (i + 1) + ") + 1 .and. ";
//            //Maximum value
//            if (maxGrid[i].equals(maxValueInit[i])) {
//                maxValue[i] = "cctk_lsh(" + (i + 1) + ")";
//            }
//            else {
//                if (CodeGeneratorUtils.isNumber(maxValueInit[i]) && CodeGeneratorUtils.isNumber(minGrid[i])) {
//                    maxValue[i] = "FLOOR(1 + " + (CodeGeneratorUtils.toNumber(maxValueInit[i]) - CodeGeneratorUtils.toNumber(minGrid[i])) + " / " 
//                        + delta[i] + ") - cctk_lbnd(" + (i + 1) + ")";
//                }
//                else {
//                    maxValue[i] = "FLOOR(1 + (" + maxValueInit[i] + " - (" + minGrid[i] + ")) / " + delta[i] + ") - cctk_lbnd(" + (i + 1) + ")";
//                }
//                regionCondition = regionCondition + maxValue[i] + " .ge. 1 .and. ";
//            }
//            
//            i++;
//        }
//        regionCondition = regionCondition.substring(0, regionCondition.lastIndexOf(" .and."));
//        String loops = "";
//        String loopsStencil = "";
//        for (int j = 0; j < coordinates.size(); j++) {
//            String loop = "";
//            String endLoop = "";
//            String actualIndent = IND + IND + IND;
//            for (int k = coordinates.size() - 1; k >= 0; k--) {
//                if (j != k) {
//                    loop = loop + actualIndent + "do " + coordinates.get(k) + " = " + coordinates.get(k) + "MapStart, " 
//                        + coordinates.get(k) + "MapEnd" + NL;
//                    endLoop = actualIndent + "end do" + NL + endLoop;
//                    actualIndent = actualIndent + IND;
//                }
//            }
//
//            loops = loops + loop + getContent(minValueInit[j], maxValueInit[j], j, regionInfo.getId(), delta[j], minGrid[j], pi) 
//                + endLoop;
//        }
//        if (pi.getSchemaStencil() > 1) {
//            loopsStencil = loopsStencil + checkStencil(pi, regionInfo, IND + IND + IND);
//        }
//        
//        return "!" + IND + IND + "Region: " + regionName + "S" + NL 
//            + coordinateTransform + IND + IND + "if (" + regionCondition + ") then" + NL 
//            + getPreloopConditions(regionLimit, gridLimit, coordinates, minGrid, maxGrid, delta) + loops 
//            + loopsStencil + IND + IND + "end if" + NL;
//    }
//    
//    /**
//     * Creates the code to set the surface or to check the stencil of the surface for a coordinate.
//     * @param minValueInit      The minimum value of the region for the coordinate 
//     * @param maxValueInit      The maximum value of the region for the coordinate
//     * @param coordNumber       The coordinate number
//     * @param regionId         The region identifier
//     * @param delta             The delta variable for the coordinate
//     * @param minGrid           The minimum value of the problem for the coordinate
//     * @param pi                Problem info
//     * @return                  The code
//     * @throws CGException      CG00X External error
//     */
//    private static String getContent(String minValueInit, String maxValueInit, int coordNumber, int regionId, String delta, String minGrid, 
//            ProblemInfo pi) throws CGException {
//        String content = "";
//        ArrayList<String> coordinates = pi.getCoordinates();
//        String index1 = getLimitIndex("MapStart", coordNumber, coordinates, false);
//        String index2 = getLimitIndex("MapEnd", coordNumber, coordinates, false);
//        
//        String actualIndent = IND + IND + IND;
//        for (int i = 0; i < coordinates.size() - 1; i++) {
//            actualIndent = actualIndent + IND;
//        }
//        
//        if (CodeGeneratorUtils.isNumber(minValueInit)) {
//            content = content + actualIndent + "if(leF(" + minGrid + " + (cctk_lbnd(" + (coordNumber + 1) + ")) * " + delta + ", " 
//                + CodeGeneratorUtils.toNumber(minValueInit) + ") .and. geF(" + minGrid + " + (cctk_ubnd(" + (coordNumber + 1) + ")) * " 
//                + delta + ", " 
//                + CodeGeneratorUtils.toNumber(minValueInit) + ")) then" + NL
//                + assignFOV(pi.getProblem(), regionId, pi.getRegionIds(), index1, pi.getCoordinates(), actualIndent + IND)
//                + actualIndent + "end if" + NL;
//        }
//        else {
//            content = content + actualIndent + "if(leF(" + minGrid + " + (cctk_lbnd(" + (coordNumber + 1) + ")) * " + delta + ", " 
//                + minValueInit + ") .and. geF(" + minGrid + " + (cctk_ubnd(" + (coordNumber + 1) + ")) * " + delta + ", " 
//                + minValueInit + ")) then" + NL
//                + assignFOV(pi.getProblem(), regionId, pi.getRegionIds(), index1, pi.getCoordinates(), actualIndent + IND)
//                + actualIndent + "end if" + NL;
//        }
//        if (CodeGeneratorUtils.isNumber(maxValueInit)) {
//            content = content + actualIndent + "if(leF(" + minGrid + " + (cctk_lbnd(" + (coordNumber + 1) + ")) * " + delta + ", " 
//                + CodeGeneratorUtils.toNumber(maxValueInit) + ") .and. geF(" + minGrid + " + (cctk_ubnd(" + (coordNumber + 1) + ")) * "
//                + delta + ", " 
//                + CodeGeneratorUtils.toNumber(maxValueInit) + ")) then" + NL
//                + assignFOV(pi.getProblem(), regionId, pi.getRegionIds(), index2, pi.getCoordinates(), actualIndent + IND)
//                + actualIndent + "end if" + NL;
//        }
//        else {
//            content = content + actualIndent + "if(leF(" + minGrid + " + (cctk_lbnd(" + (coordNumber + 1) + ")) * " + delta + ", " 
//                + maxValueInit + ") .and. geF(" + minGrid + " + (cctk_ubnd(" + (coordNumber + 1) + ")) * " + delta + ", " 
//                + maxValueInit + ")) then" + NL
//                + assignFOV(pi.getProblem(), regionId, pi.getRegionIds(), index2, pi.getCoordinates(), actualIndent + IND)
//                + actualIndent + "end if" + NL;
//        }
//        return content;
//    }
//    
//    /**
//     * Creates the index for the coordinates at the limits of the mathematical region.
//     * @param operation         The operation to make
//     * @param coordNumber       The coordinate to generate the limits for
//     * @param coordinates       The coordinates
//     * @param stencil           If the stencil is checked in the function that uses this index
//     * @return                  The index generated
//     */
//    private static String getLimitIndex(String operation, int coordNumber, ArrayList<String> coordinates, boolean stencil) {
//        String index = "";
//        for (int k = 0; k < coordinates.size(); k++) {
//            if (coordNumber == k) {
//                index = index + coordinates.get(coordNumber) + operation + ", ";
//                if (stencil) {
//                    if (operation.equals("MapStart")) {
//                        index = index + "1, ";
//                    }
//                    else {
//                        index = index + "2, ";
//                    }
//                }
//            }
//            else {
//                index = index + coordinates.get(k) + ", ";
//                if (stencil) {
//                    index = index + "1, ";
//                }
//            }
//        }
//        index = index.substring(0, index.lastIndexOf(","));
//        return index;
//    }
//    
//    /**
//     * Generate the code for the pre-loop condition in map mathematical surface method.
//     * @param regionLimit      The region limits
//     * @param gridLimit         The grid limits
//     * @param coordinates       The coordinates of the problem
//     * @param minGrid           The minimum values of the grid
//     * @param maxGrid           The maximum values of the grid
//     * @param delta             The delta values
//     * @return                  The code
//     * @throws CGException      CG004 External error
//     */
//    private static String getPreloopConditions(LinkedHashMap<String, CoordinateInfo> regionLimit, LinkedHashMap<String, CoordinateInfo> gridLimit, 
//            ArrayList<String> coordinates, String[] minGrid, String[] maxGrid, String[] delta) throws CGException {
//        String[][] xslParams = new String [2][2];
//        xslParams[0][0] = "condition";
//        xslParams[0][1] = "false";
//        xslParams[1][0] = "indent";
//        xslParams[1][1] = "0";
//        
//        String[] minValue = new String[regionLimit.size()];
//        String[] maxValue = new String[regionLimit.size()];
//        String preLoop = "";
//        int i = 0;
//        Iterator<String> coordIt = regionLimit.keySet().iterator();
//        while (coordIt.hasNext()) {
//            String coord = coordIt.next();
//            minValue[i] = CactusUtils.xmlTransform(regionLimit.get(coord).getMin(), xslParams);
//            maxValue[i] = CactusUtils.xmlTransform(regionLimit.get(coord).getMax(), xslParams);
//            String minValueOriginal = minValue[i];
//            String maxValueOriginal = maxValue[i];
//            if (minGrid[i].equals(minValue[i])) {
//                minValue[i] = "1";
//            }
//            else {
//                if (CodeGeneratorUtils.isNumber(minValue[i]) && CodeGeneratorUtils.isNumber(minGrid[i])) {
//                    minValue[i] = "FLOOR(1 + " + (CodeGeneratorUtils.toNumber(minValue[i]) - CodeGeneratorUtils.toNumber(minGrid[i])) + " / " 
//                        + delta[i] + ") - cctk_lbnd(" + (i + 1) + ")";
//                }
//                else {
//                    minValue[i] = "FLOOR(1 + (" + minValue[i] + " - (" + minGrid[i] + ")) / " + delta[i] + ") - cctk_lbnd(" + (i + 1) + ")";
//                }
//            }
//            if (maxGrid[i].equals(maxValue[i])) {
//                maxValue[i] = "cctk_lsh(" + (i + 1) + ")";
//            }
//            else {
//                if (CodeGeneratorUtils.isNumber(maxValue[i]) && CodeGeneratorUtils.isNumber(minGrid[i])) {
//                    maxValue[i] = "FLOOR(1 + " + (CodeGeneratorUtils.toNumber(maxValue[i]) - CodeGeneratorUtils.toNumber(minGrid[i])) + " / " 
//                        + delta[i] + ") - cctk_lbnd(" + (i + 1) + ")";
//                }
//                else {
//                    maxValue[i] = "FLOOR(1 + (" + maxValue[i] + " - (" + minGrid[i] + ")) / " + delta[i] + ") - cctk_lbnd(" + (i + 1) + ")";
//                }
//            }
//            if (minGrid[i].equals(minValueOriginal)) {
//                preLoop = preLoop + IND + IND + IND + coord + "MapStart = " + minValue[i] + NL;
//            }
//            else {
//                preLoop = preLoop 
//                    + IND + IND + IND + "if (" + minValue[i] + " .gt. 1) then" + NL
//                    + IND + IND + IND + IND + coord + "MapStart = " + minValue[i] + NL
//                    + IND + IND + IND + "else" + NL
//                    + IND + IND + IND + IND + coord + "MapStart = 1" + NL
//                    + IND + IND + IND + "end if" + NL;
//            }
//            if (maxGrid[i].equals(maxValueOriginal)) {
//                preLoop = preLoop + IND + IND + IND + coord + "MapEnd = " + maxValue[i] + NL;
//            }
//            else {
//                preLoop = preLoop 
//                    + IND + IND + IND + "if (" + maxValue[i] + " .lt. cctk_ubnd(" + (i + 1) + ") - cctk_lbnd(" 
//                    + (i + 1) + ") + 1) then" + NL
//                    + IND + IND + IND + IND + coord + "MapEnd = " + maxValue[i] + NL
//                    + IND + IND + IND + "else" + NL
//                    + IND + IND + IND + IND + coord + "MapEnd = cctk_lsh(" + (i + 1) + ")" + NL
//                    + IND + IND + IND + "end if" + NL;
//            }
//            i++;
//        }
//        return preLoop;
//    }
//    
//    /**
//     * Generate the code to maps a x3d region. 
//     * @param regionInfo       The region information
//     * @param pi                Information of the problem
//     * @param minGrid           The minimum values of the grid
//     * @param delta             The delta values
//     * @param problemIdentifier The name of the problem
//     * @return                  The code generated
//     * @throws CGException      CG004 External error
//     */
//    private static String mapX3d(MappingRegionInfo regionInfo, ProblemInfo pi, String[] minGrid, String[] delta, String problemIdentifier) 
//        throws CGException {
//        final int oneHundred = 100;
//        final int ninetyNine = 99;
//        LinkedHashMap<String, CoordinateInfo> gridLimit = pi.getGridLimits();
//        int stencil = pi.getSchemaStencil();
//        String regionName = regionInfo.getElement().getFirstChild().getTextContent();
//        int regionNum = regionInfo.getId();
//        int originalRegionNumber = regionNum;
//        
//        String[][] xslParams = new String [2][2];
//        xslParams[0][0] = "condition";
//        xslParams[0][1] = "true";
//        xslParams[1][0] = "indent";
//        xslParams[1][1] = "0";
//        
//        try {
//            X3DRegion x3d = CodeGeneratorUtils.parseX3dRegion(docMan.getDocument(CodeGeneratorUtils.find(
//                    regionInfo.getElement(), ".//mms:region//mms:x3dRegionId").item(0).getTextContent()));
//
//            String result = "";
//            String actualIndent = IND + IND;
//            //Common information
//            result = result + "!" + actualIndent + "Common information for region: " + regionName + NL;
//            //Fill the point data
//            //X3D is always a 3D point file. If the problem is 2D the last coordinate (Z) will not be generated
//            String pointAttr = x3d.getPointAttr();
//            double[] cactusPoints = null;
//            if (pointAttr.indexOf(",") == -1) {
//                String[] pointStrings = pointAttr.substring(0, pointAttr.lastIndexOf(" ")).split(" ");
//                //X3D is always a 3D point file. If the problem is 2D the last coordinate (Z) will not be generated
//                cactusPoints = new double[(pointStrings.length)];
//                for (int j = 0; j < (pointStrings.length) / THREE; j++) {
//                    cactusPoints[j] = Double.valueOf(pointStrings[j * THREE]);
//                    cactusPoints[j + 1 * pointStrings.length / THREE] = Double.valueOf(pointStrings[j * THREE + 1]);
//                    cactusPoints[j + 2 * pointStrings.length / THREE] = Double.valueOf(pointStrings[j * THREE + 2]);
//                }
//            } 
//            else {
//                String[] pointStrings = pointAttr.substring(0, pointAttr.lastIndexOf(",")).split(", ");
//                Point[] points = new Point[pointStrings.length];
//                //X3D is always a 3D point file. If the problem is 2D the last coordinate (Z) will not be generated
//                cactusPoints = new double[THREE * pointStrings.length];
//                for (int j = 0; j < pointStrings.length; j++) {
//                    points[j] = new Point(pointStrings[j]);
//                    double[] coords = points[j].getCoordinate();
//                    for (int k = 0; k < coords.length; k++) {
//                        cactusPoints[j + k * pointStrings.length] = coords[k];
//                    }
//                }
//            }
//            //Apply transformations to points
//            applyTransformations(x3d, cactusPoints);
//            //Fill the face data (point unions)
//            String coordIndex = x3d.getCoordIndexAttr();
//            String[] unions;
//            //The data could have commas or not...
//            if (coordIndex.indexOf(",") > -1) {
//                unions = coordIndex.substring(0, coordIndex.lastIndexOf(" -1,")).split(" -1, ");
//            }
//            else {
//                unions = coordIndex.substring(0, coordIndex.lastIndexOf(" -1")).split(" -1 ");
//            }
//            //2D
//            if (pi.getDimensions() == 2) {
//                //Unions from 3D to 2D (contour)
//                unions = CodeGeneratorUtils.removeRepeatedUnions(unions);
//            }
//            
//            //Points
//            //If 2D, remove the interior points
//            if (pi.getDimensions() == 2) {
//                cactusPoints = CodeGeneratorUtils.removeInteriorPoints(cactusPoints, unions);
//            }
//            
//            //StringBuffer needed for performance when x3d is big
//            StringBuffer pointString = new StringBuffer(actualIndent + "data " + regionName + "Points/");
//            for (int i = 0; i < cactusPoints.length; i++) {
//                if (i % oneHundred == ninetyNine) {
//                    pointString.append("\\" + NL + actualIndent + cactusPoints[i] + ", ");
//                }
//                else {
//                    pointString.append(cactusPoints[i] + ", ");
//                }
//            }
//            result = result + pointString.toString().substring(0, pointString.toString().length() - 2) + "/" + NL;
//
//            int facePointsNumber = unions[0].trim().split(" +").length;
//            //The last point in the face is the first point, not to be stored
//            int[] cactusUnions = new int[unions.length * facePointsNumber];
//            try {
//                for (int i = 0; i < unions.length; i++) {
//                    if (!unions[i].equals("")) {
//                        String[] union = unions[i].trim().split(" +");
//                        for (int j = 0; j < union.length; j++) {
//                            cactusUnions[i + j * unions.length] = Integer.parseInt(union[j]) + 1;
//                        }
//                    }
//                }
//            } 
//            catch (Exception e) {
//                throw new CGException(CGException.CG003, "The x3d is malformed, non integer found in unions");
//            }
//            //StringBuffer needed for performance when x3d is big
//            StringBuffer unionString = new StringBuffer(actualIndent + "data " + regionName + "Unions/");
//            for (int i = 0; i < cactusUnions.length; i++) {
//                if (i % oneHundred == ninetyNine) {
//                    unionString.append("\\" + NL + actualIndent + cactusUnions[i] + ", ");
//                }
//                else {
//                    unionString.append(cactusUnions[i] + ", ");
//                }
//            }
//            result = result + unionString.toString().substring(0, unionString.toString().length() - 2) + "/" + NL;
//            
//            //Surface region
//            if (regionInfo.hasSurface()) {
//                result = result + "!" + IND + IND + "Region: " + regionName + "S" + NL;
//                regionNum++;
//            }
//
//            //Fill the faces
//            regionInfo.setId(regionNum);
//            result = result + faceFilling(pi, regionInfo, gridLimit, minGrid, delta, unions.length);
//            //Check the stencil in the surface and fill if necessary
//            if (stencil > 1 && originalRegionNumber != regionNum) {
//                result = result + checkStencil(pi, regionInfo, IND + IND);
//
//            }
//            result = result + IND + IND + "call CCTK_SyncGroup(status, cctkGH, \"" + problemIdentifier + "::AuxiliarGroup\")" + NL;
//            
//            //Interior region
//            if (regionInfo.hasInterior()) {
//                regionInfo.setId(originalRegionNumber);
//                result = result + "!" + actualIndent + "Region: " + regionName + "I" + NL
//                    + fillx3dInterior(pi, regionInfo.getId(), problemIdentifier, stencil);
//                if (stencil > 1) {
//                    result = result + checkStencil(pi, regionInfo, IND + IND);
//                }
//            }
//            return result;
//        } 
//        catch (DOMException e) {
//            throw new CGException(CGException.CG00X, e.getMessage());
//        } 
//        catch (DMException e) {
//            throw new CGException(CGException.CG00X, e.getMessage());
//        } 
//    }
//    
//    /**
//     * Apply the transformations of the x3d document to the points.
//     * @param x3d                   The original x3d
//     * @param cactusPoints          The points of the x3d without process
//     */
//    private static void applyTransformations(X3DRegion x3d, double[] cactusPoints) {
//        //Although no transformation is defined the x3d document have all the identity transformations
//        ArrayList<ArrayList<String>> transforms = x3d.getTransforms();
//        for (int i = transforms.size() - 1; i >= 0; i--) {
//            //The order of transformations for x3d is rotation scale translation
//            //rotation
//            for (int j =  0; j < transforms.get(i).size(); j++) {
//                String attribute = transforms.get(i).get(j);
//                if (attribute.startsWith("r")) {
//                    String[] rotationString = attribute.substring(1).split(" +");
//                    double[] rotation = new double[rotationString.length];
//                    for (int k = 0; k < rotationString.length; k++) {
//                        rotation[k] = Double.parseDouble(rotationString[k]);
//                    }
//                    CodeGeneratorUtils.applyRotation(rotation, cactusPoints);
//                }
//            }
//            //scale
//            for (int j =  0; j < transforms.get(i).size(); j++) {
//                String attribute = transforms.get(i).get(j);
//                if (attribute.startsWith("s")) {
//                    String[] scaleString = attribute.substring(1).split(" +");
//                    double[] scale = new double[scaleString.length];
//                    for (int k = 0; k < scaleString.length; k++) {
//                        scale[k] = Double.parseDouble(scaleString[k]);
//                    }
//                    CodeGeneratorUtils.applyScale(scale, cactusPoints);
//                }
//            }
//            //translation
//            for (int j =  0; j < transforms.get(i).size(); j++) {
//                String attribute = transforms.get(i).get(j);
//                if (attribute.startsWith("t")) {
//                    String[] translationString = attribute.substring(1).split(" +");
//                    double[] translation = new double[translationString.length];
//                    for (int k = 0; k < translationString.length; k++) {
//                        translation[k] = Double.parseDouble(translationString[k]);
//                    }
//                    CodeGeneratorUtils.applyTranslation(translation, cactusPoints);
//                }   
//            }
//        }
//    }
//    
//    /**
//     * Generate the code to map a region.
//     * @param regionInfo       The region info
//     * @param pi                The information of the problem
//     * @param problemIdentifier The name of the problem
//     * @return                  The generated code
//     * @throws CGException      CG004 External error
//     */
//    private static String mapRegion(MappingRegionInfo regionInfo, ProblemInfo pi, String problemIdentifier) 
//        throws CGException {
//        String[][] xslParams = new String [2][2];
//        xslParams[0][0] = "condition";
//        xslParams[0][1] = "false";
//        xslParams[1][0] = "indent";
//        xslParams[1][1] = "0";
//        
//        LinkedHashMap<String, CoordinateInfo> gridLimit = pi.getGridLimits();
//        ArrayList<String> coordinates = pi.getCoordinates();
//        Document problem = regionInfo.getElement().getOwnerDocument();
//        String result = "";
//        
//        //Take the grid minimum, maximum and deltas for all the problem coordinates
//        String[] minGrid = new String[coordinates.size()];
//        String[] maxGrid = new String[coordinates.size()];
//        String[] delta = new String[coordinates.size()];
//        for (int i = 0; i < coordinates.size(); i++) {
//            minGrid[i] = CactusUtils.xmlTransform(gridLimit.get(coordinates.get(i)).getMin(), xslParams);
//            maxGrid[i] = CactusUtils.xmlTransform(gridLimit.get(coordinates.get(i)).getMax(), xslParams);
//            String query =
//                "//mms:implicitParameter[mms:coordinate = '" + coordinates.get(i) + "']/mms:value";
//            delta[i] = CactusUtils.variableNormalize(CodeGeneratorUtils.find(problem, query).item(0).getTextContent());
//        }
//        
//        //X3d
//        if (CodeGeneratorUtils.find(regionInfo.getElement(), ".//mms:region//mms:x3dRegionId").getLength() > 0) {
//            result = result + mapX3d(regionInfo, pi, minGrid, delta, problemIdentifier);
//        }
//        //mathematical region
//        else {
//            //Map interior mathematical region
//            if (regionInfo.hasInterior()) {
//                result = result + mapMathematicalInterior(pi, regionInfo, gridLimit, coordinates, minGrid, maxGrid, delta);
//            }
//            //Surface mathematical region
//            if (regionInfo.hasSurface()) {
//                regionInfo.setId(regionInfo.getId() + 1);
//                result = result + mapMathematicalSurface(pi, regionInfo, gridLimit, minGrid, maxGrid, delta);
//            }
//            
//        }
//        return result;
//    }
//    
//    /**
//     * Map the boundary conditions.
//     * @param problemInfo       The problem information
//     * @return                  The code
//     * @throws CGException      CG00X External error
//     */
//    private static String mapBoundaries(ProblemInfo problemInfo) throws CGException {
//        String result = "!" + IND + IND + "Boundaries Mapping" + NL;
//        ArrayList<String> boundPrecedence = problemInfo.getBoundariesPrecedence();
//        ArrayList<String> coordinates = problemInfo.getCoordinates();
//        String ind = IND + IND + IND;
//        String index = "";
//        for (int i = 0; i < coordinates.size(); i++) {
//            String coord = coordinates.get(i);
//            index = index + coord + ", ";
//        }
//        index = index.substring(0, index.lastIndexOf(","));
//        
//        for (int i = boundPrecedence.size() - 1; i >= 0; i--) {
//            String axis = boundPrecedence.get(i).substring(0, boundPrecedence.get(i).indexOf("-"));
//            String side = boundPrecedence.get(i).substring(boundPrecedence.get(i).indexOf("-") + 1);
//            int boundIndex = coordinates.indexOf(CodeGeneratorUtils.contToDisc(problemInfo.getProblem(), axis)) * 2 + 1;
//            if (side.toLowerCase().equals("upper")) {
//                boundIndex++;
//            }
//            result = result + "!" + IND + IND + boundPrecedence.get(i) + NL
//                    + IND + IND + "if (cctk_bbox(" + boundIndex + ") .eq. 1) then" + NL;
//            if (side.toLowerCase().equals("lower")) {
//                String loop = "";
//                String endLoop = "";
//                String tmpInd = ind;
//                for (int j = coordinates.size() - 1; j >= 0; j--) {
//                    String coord = coordinates.get(j);
//                    if (j == coordinates.indexOf(CodeGeneratorUtils.contToDisc(problemInfo.getProblem(), axis))) {
//                        loop = loop + tmpInd + "do " + coord + " = 1, " + problemInfo.getSchemaStencil() + NL;
//                    }
//                    else {
//                        loop = loop + tmpInd + "do " + coord + " = 1, cctk_lsh(" + (j + 1) + ")" + NL;
//                    }
//                    endLoop = tmpInd + "end do" + NL + endLoop;
//                    tmpInd = tmpInd + IND;
//                }
//                result = result + loop
//                    + assignFOV(problemInfo.getProblem(), -boundIndex, problemInfo.getRegionIds(), index, problemInfo.getCoordinates(), tmpInd)
//                    + endLoop;
//            }
//            else {
//                String loop = "";
//                String endLoop = "";
//                String tmpInd = ind;
//                for (int j = coordinates.size() - 1; j >= 0; j--) {
//                    String coord = coordinates.get(j);
//                    if (j == coordinates.indexOf(CodeGeneratorUtils.contToDisc(problemInfo.getProblem(), axis))) {
//                        loop = loop + tmpInd + "do " + coord + " = cctk_lsh(" + (j + 1) + ") - " + (problemInfo.getSchemaStencil() - 1) + ", cctk_lsh(" 
//                                + (j + 1) + ")" + NL;
//                    }
//                    else {
//                        loop = loop + tmpInd + "do " + coord + " = 1, cctk_lsh(" + (j + 1) + ")" + NL;
//                    }
//                    endLoop = tmpInd + "end do" + NL + endLoop;
//                    tmpInd = tmpInd + IND;
//                }
//                result = result + loop
//                    + assignFOV(problemInfo.getProblem(), -boundIndex, problemInfo.getRegionIds(), index, problemInfo.getCoordinates(), tmpInd)
//                    + endLoop;
//            }
//            result = result + IND + IND + "end if" + NL;
//        }
//        
//        return result;
//    }
//    
//    /**
//     * Creates the code block that checks the thickness of a region.
//     * @param pi            The problem information
//     * @param regionInfo   The region 
//     * @param actualIndent  The actual indent for the code
//     * @return              The code
//     * @throws CGException  CG00X External error
//     */
//    private static String checkStencil(ProblemInfo pi, MappingRegionInfo regionInfo, String actualIndent) throws CGException {
//        String loopStart = "";
//        String loopStart2 = "";
//        String loopEnd = "";
//        String loopEnd2 = "";
//        String indent = actualIndent;
//        String index = "";
//        ArrayList<String> coordinates = pi.getCoordinates();
//        for (int i = coordinates.size() - 1; i >= 0; i--) {
//            String coord = coordinates.get(i);
//            index = coord + ", " + index;
//            loopStart = loopStart + indent + "do " + coord + " = 1, cctk_lsh(" + (i + 1) + ")" + NL;
//            loopStart2 = loopStart2 + indent + IND + "do " + coord + " = 1, cctk_lsh(" + (i + 1) + ")" + NL;
//            loopEnd = indent + "end do" + NL + loopEnd;
//            loopEnd2 = indent + IND + "end do" + NL + loopEnd2;
//            indent = indent + IND;
//        }
//        index = index.substring(0, index.lastIndexOf(","));
//        
//        return "!" + actualIndent + "Check stencil" + NL
//            + loopStart
//            + "!" + indent + "If wall, then it is considered exterior" + NL
//            + indent + "if (FOV_" + regionInfo.getId() + "(" + index + ") .gt. 0) then" + NL
//            + indent + IND + "interior(" + index + ") = 1" + NL
//            + indent + "else" + NL
//            + indent + IND + "interior(" + index + ") = -1" + NL
//            + indent + "end if" + NL
//            + loopEnd
//            + loopStart
//            + indent + "if (FOV_" + regionInfo.getId() + "(" + index + ") .gt. 0) then" + NL
//            + indent + IND + "call setStencilLimits(CCTK_PASS_FTOF, " + index + ", " + regionInfo.getId() + ")" + NL
//            + indent + "end if" + NL
//            + loopEnd
//            + actualIndent + "modif = .true." + NL
//            + actualIndent + "do while (modif)" + NL
//            + actualIndent + IND + "modif = .false." + NL
//            + loopStart2
//            + indent + IND + "if (interior(" + index + ") .eq. 1 .and. FOV_" + regionInfo.getId() + "(" + index + ") .eq. 0) then" + NL
//            + indent + IND + "if (checkStencil(CCTK_PASS_FTOF, " + index + ", " + regionInfo.getId() + ")) then" + NL
//            + indent + IND + "modif = .true." + NL
//            + indent + IND + "end if" + NL
//            + indent + IND + "end if" + NL
//            + loopEnd2
//            + loopStart2
//            + indent + "if (interior(" + index + ") .eq. 2) then" + NL
//            + assignFOV(pi.getProblem(), regionInfo.getId(), pi.getRegionIds(), index, pi.getCoordinates(), actualIndent + IND)
//            + indent + "end if" + NL
//            + loopEnd2
//            + actualIndent + "end do" + NL;
//    }
//    
//    /**
//     * Assign the region to the FOVs variables.
//     * @param doc           The document
//     * @param regionId     The region id
//     * @param regionIds    All the region ids
//     * @param index         The array index
//     * @param coords        The coordinates
//     * @param indent        The actual indent
//     * @return              The code
//     * @throws CGException  CG00X External error
//     */
//    private static String assignFOV(Document doc, int regionId, ArrayList<String> regionIds, String index, 
//            ArrayList<String> coords, String indent) throws CGException {
//        String fov = "";
//        String segString = CodeGeneratorUtils.getRegionName(doc, regionId, coords);
//        fov = fov + indent + "FOV_" + segString + "(" + index + ") = 100" + NL;
//        for (int i = 0; i < regionIds.size(); i++) {
//            String segId = regionIds.get(i);
//            if (!segId.equals(segString)) {
//                fov = fov + indent + "FOV_" + segId + "(" + index + ") = 0" + NL;
//            }
//        }
//        
//        return fov;
//    }
}
