package eu.iac3.mathMS.codeGeneratorPlugin.utils;

import java.util.ArrayList;

/**
 * Common information of the movement.
 * @author bminyano
 *
 */
public class MovementInfo {

    private boolean moves;
    private ArrayList<String> x3dMovRegions;
    private ArrayList<String> movRegions;
    
    /**
     * Constructor.
     * @param moves             if the regions have movement
     * @param x3dMovRegions    The list of regions that moves with x3d
     * @param movRegions       The list of regions that moves
     */
    public MovementInfo(boolean moves, ArrayList<String> x3dMovRegions, ArrayList<String> movRegions) {
        this.moves = moves;
        this.movRegions = new ArrayList<String>();
        this.x3dMovRegions = new ArrayList<String>();
        this.x3dMovRegions.addAll(x3dMovRegions);
        this.movRegions.addAll(movRegions);
    }

    /**
     * To known when at least one region moves.
     * @return  True if some region moves
     */
    public boolean itMoves() {
        return moves;
    }

    public void setMoves(boolean moves) {
        this.moves = moves;
    }

    public ArrayList<String> getX3dMovRegions() {
        return x3dMovRegions;
    }

    public void setX3dMovRegions(ArrayList<String> x3dMovRegions) {
        this.x3dMovRegions = x3dMovRegions;
    }

    public ArrayList<String> getMovRegions() {
        return movRegions;
    }

    public void setMovRegions(ArrayList<String> movRegions) {
        this.movRegions = movRegions;
    }
    
    
}
