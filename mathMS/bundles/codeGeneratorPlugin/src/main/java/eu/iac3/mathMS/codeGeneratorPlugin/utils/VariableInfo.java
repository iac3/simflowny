/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.codeGeneratorPlugin.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

import eu.iac3.mathMS.codeGeneratorPlugin.CGException;


/**
 * Class with the relevant information of the variables taken from the configuration.
 * @author bminyano
 *
 */
public class VariableInfo {

    private ArrayList<String> meshVariables;
    private HashMap<String, String> parameters;
    private ArrayList<String> outputVars;
    private ArrayList<String> outputVarsAnalysis;
    private HashMap<String, String> meshFields;
    private HashMap<String, String> vectors;
    private HashMap<String, ArrayList<String>> particleVariables;
    private ArrayList<String> allParticleVariables;
    private ArrayList<String> allParticleFields;
    private HashMap<String, ArrayList<String>> particleFields;
    private HashMap<String, ArrayList<String>> positionVariables;
    
    /**
     * Constructor.
     * @param meshVariables           	The variable tag in the configuration
     * @param particleFields			Particle fields
     * @param particleVariables			The particle variables
     * @throws CGException              CG003 Not possible to create code for the platform
     */
    public VariableInfo(Element meshVariables, HashMap<String, ArrayList<String>> particleFields, HashMap<String, ArrayList<String>> particleVariables, HashMap<String, ArrayList<String>> positionVariables) throws CGException {
        this.meshVariables = getVariables(meshVariables);
        vectors = getVectors(meshVariables);
        parameters = getParameters(meshVariables);
        outputVars = getOutputVariables(meshVariables);
        outputVarsAnalysis = getOutputVariablesAnalysis(meshVariables);
        meshFields = getFields(meshVariables);
        this.particleVariables = particleVariables;
        this.particleFields = particleFields;
        this.positionVariables = positionVariables;
        allParticleVariables = new ArrayList<String>();
        allParticleFields = new ArrayList<String>();
        calculateAllParticleVariables();
        calculateAllParticleFields();
        checkIncompatibilities();
    }
    
	public Set<String> getAllPositionVariables() {
		Set<String> allPositions = new HashSet<String>();
		for(ArrayList<String> positions: positionVariables.values()) {
			allPositions.addAll(positions);
		}
		return allPositions;
	}

    
	public HashMap<String, ArrayList<String>> getPositionVariables() {
		return positionVariables;
	}

	public ArrayList<String> getPositionVariables(String speciesName) {
		return positionVariables.get(speciesName);
	}

	public HashMap<String, ArrayList<String>> getParticleFields() {
		return particleFields;
	}

	public HashMap<String, ArrayList<String>> getParticleVariables() {
		return particleVariables;
	}
	public void setParticleVariables(HashMap<String, ArrayList<String>> particleVariables) {
		this.particleVariables = particleVariables;
	}
	
    public ArrayList<String> getMeshVariables() {
        return meshVariables;
    }

    public HashMap<String, String> getParameters() {
        return parameters;
    }

    public ArrayList<String> getOutputVars() {
        return outputVars;
    }

    public HashMap<String, String> getVectors() {
        return vectors;
    }

    public HashMap<String, String> getMeshFields() {
        return meshFields;
    }

    public ArrayList<String> getOutputVarsAnalysis() {
        return outputVarsAnalysis;
    }

	public ArrayList<String> getAllParticleVariables() {
		return allParticleVariables;
	}

	public void calculateAllParticleVariables() {
		for (ArrayList<String> vars : particleVariables.values()) {
			allParticleVariables.addAll(new ArrayList<String>(vars));
		}
	}
    
	public ArrayList<String> getAllParticleFields() {
		return allParticleFields;
	}

	public void calculateAllParticleFields() {
		for (ArrayList<String> vars : particleFields.values()) {
			allParticleFields.addAll(new ArrayList<String>(vars));
		}
	}
    
	
    /**
     * Gets the variables used in the model.
     * @param variablesE    The variable element in configuration.xml
     * @return              The list of the variables
     */
    private ArrayList<String> getVariables(Element variablesE) {
        ArrayList<String> result = new ArrayList<String>();
        ArrayList<String> previousSteps = new ArrayList<String>();
        Node variable = variablesE.getFirstChild();
        while (variable != null) {
            if (variable.getLocalName().equals("field")) {
                String name = variable.getFirstChild().getTextContent();
                int timeSteps = Integer.parseInt(variable.getLastChild().getTextContent());
                result.add(name);
                for (int i = 1; i < timeSteps; i++) {
                    name = name + "_p";
                    previousSteps.add(name);
                }
            } 
            else {  
                //Vector is going to 
                if (variable.getLocalName().equals("vector")) {
                    String name = variable.getFirstChild().getTextContent();
                    int timeSteps = Integer.parseInt(variable.getLastChild().getTextContent());
                    result.add(name);
                    for (int i = 1; i < timeSteps; i++) {
                        name = name + "_p";
                        previousSteps.add(name);
                    }
                }
                else {
                    //The remaining variables must be added as they are except the parameters
                    if (!variable.getLocalName().equals("parameter")) {
                        result.add(variable.getTextContent());   
                    }
                }
            }
            variable = variable.getNextSibling();
        }
        result.addAll(previousSteps);
        return result;
    }
        
    /**
     * Get the variables to generate output.
     * @param variablesE    The variable element in configuration.xml 
     * @return              The variables selected
     */
    private ArrayList<String> getOutputVariables(Element variablesE) {
        ArrayList<String> result = new ArrayList<String>();
        Node variable = variablesE.getFirstChild();
        while (variable != null) {
            if (variable.getLocalName().equals("field")) {
                result.add(variable.getFirstChild().getTextContent());
            }
            variable = variable.getNextSibling();
        }
        
        return result;
    }
    
    /**
     * Get the variables to generate output.
     * @param variablesE    The variable element in configuration.xml 
     * @return              The variables selected
     */
    private ArrayList<String> getOutputVariablesAnalysis(Element variablesE) {
        ArrayList<String> result = new ArrayList<String>();
        Node variable = variablesE.getFirstChild();
        while (variable != null) {
            if (variable.getLocalName().equals("analysisField")) {
                result.add(variable.getFirstChild().getTextContent());
            } 
            variable = variable.getNextSibling();
        }
        
        return result;
    }
    
    /**
     * Get the variables to generate output.
     * @param variablesE    The variable element in configuration.xml 
     * @return              The variables selected
     */
    private HashMap<String, String> getVectors(Element variablesE) {
        HashMap<String, String>  result = new HashMap<String, String>();
        Node variable = variablesE.getFirstChild();
        while (variable != null) {
            if (variable.getLocalName().equals("vector")) {
                result.put(variable.getFirstChild().getTextContent(), variable.getLastChild().getTextContent());   
            }
            variable = variable.getNextSibling();
        }
        
        return result;
    }
    
    /**
     * Gets the fields used in the model.
     * @param variablesE    The variable element in configuration.xml
     * @return              The list of the fields
     */
    private HashMap<String, String> getFields(Element variablesE) {
        HashMap<String, String>  result = new HashMap<String, String>();
        Node variable = variablesE.getFirstChild();
        while (variable != null) {
            if (variable.getLocalName().equals("field")) {
                result.put(variable.getFirstChild().getTextContent(), variable.getLastChild().getTextContent());   
            }
            variable = variable.getNextSibling();
        }
        
        return result;
    }
        
    /**
     * Gets the parameters used in the model.
     * @param variablesE    The variable element in configuration.xml
     * @return              The list of the parameters
     */
    private HashMap<String, String> getParameters(Element variablesE) {
        HashMap<String, String>  result = new HashMap<String, String>();
        Node variable = variablesE.getFirstChild();
        while (variable != null) {
            if (variable.getLocalName().equals("parameter")) {
                result.put(variable.getFirstChild().getTextContent(), variable.getLastChild().getTextContent());   
            }
            variable = variable.getNextSibling();
        }
        
        return result;
    }
    
    /**
     * Check incompatibilities between variables and fields.
     * @throws CGException	CG003 Not possible to create code for the platform
     */
    private void checkIncompatibilities() throws CGException {
    	if (!Collections.disjoint(meshVariables, allParticleFields)) {
    		throw new CGException(CGException.CG003, "Mesh assigning particle fields is not allowed. Check initial conditions.");
    	}
    	if (!Collections.disjoint(meshFields.keySet(), allParticleVariables)) {
    		throw new CGException(CGException.CG003, "Particles assigning mesh fields is not allowed. Check initial conditions.");
    	}
    }
}
