/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.codeGeneratorPlugin.utils;

import java.util.ArrayList;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import eu.iac3.mathMS.codeGeneratorPlugin.CGException;

public class SpatialCondition {
    ArrayList<String> axes = null;
    ArrayList<String> sides = null;
    ArrayList<Integer> distances;
    boolean mainCondition;
    
    private Document docRef;
    
    /**
     * Constructs the spatial condition from the spatialDiscretization or boundarySchema tags.
     * @param conditions    The input tag
     * @param problem       The discretized problem
     * @param isMainSchema  True if the tag is a main schema discretization
     * @throws CGException  
     */
    public SpatialCondition(Element conditions, Document problem, boolean isMainSchema) throws CGException {
        axes = new ArrayList<String>();
        sides = new ArrayList<String>();
        distances = new ArrayList<Integer>();
        NodeList conditionList = conditions.getChildNodes();
        for (int i = 0; i < conditionList.getLength(); i++) {
            Element condition = (Element) conditionList.item(i);
            axes.add(CodeGeneratorUtils.contToDisc(problem, condition.getAttribute("axisAtt")));
            sides.add(condition.getAttribute("sideAtt"));
            distances.add(new Integer(condition.getAttribute("distanceToBoundaryAtt")));
        }
        mainCondition = isMainSchema;
    }
    
    /**
     * Creates a condition simml element.
     * @param doc           A document reference to create the elements.
     * @param coordinates   The spatial coordinates
     * @param regionId      The current region id
     * @return              The simml element
     */
    public Element createCondition(Document doc, ArrayList<String> coordinates, int regionId) {
        docRef = doc;
        Element ifE = CodeGeneratorUtils.createElement(doc, CodeGeneratorUtils.SMLURI, "if");
        Element cond = CodeGeneratorUtils.createElement(doc, CodeGeneratorUtils.MTURI, "math");
        Element then = CodeGeneratorUtils.createElement(doc, CodeGeneratorUtils.SMLURI, "then");
        //Condition depending on typology of spatial discretization
        Element conditionApply = null;
        if (mainCondition) {
            conditionApply = createMainCondition(doc, coordinates, regionId);
            //If no boundary schemas defined, there is no condition to use 
            if (conditionApply == null) {
                return null;
            }
        }
        else {
            conditionApply = createBoundaryCondition(doc, coordinates, regionId);
        }
        cond.appendChild(conditionApply);
        ifE.appendChild(cond);
        ifE.appendChild(then);
        return ifE;
    }
    
    /**
     * Creates the condition for main schema spatial discretizations.
     * @param doc           A document reference to create the elements.
     * @param coordinates   The spatial coordinates
     * @param regionId      The current region id
     * @return              The simml element
     */
    public Element createMainCondition(Document doc, ArrayList<String> coordinates, int regionId) {
        Element conditionApply = null;
        for (int j = 0; j < coordinates.size(); j++) {
            String coord = coordinates.get(j);
            for (int i = 0; i < distances.size(); i++) {
                int distance = distances.get(i);
                Element distanceElement = null;
                if (axes.get(i).equals(coord)) {
                    //Lower
                    Element lower = null;
                    if (sides.get(i).equals("Lower")) {
                        //Lower condition
                        lower = CodeGeneratorUtils.createElement(doc, CodeGeneratorUtils.MTURI, "apply");
                        Element and = CodeGeneratorUtils.createElement(doc, CodeGeneratorUtils.MTURI, "and");
                        //Coordinate limits condition
                        Element applyG = CodeGeneratorUtils.createElement(doc, CodeGeneratorUtils.MTURI, "apply");
                        Element geq = CodeGeneratorUtils.createElement(doc, CodeGeneratorUtils.MTURI, "geq");
                        Element ci = CodeGeneratorUtils.createElement(doc, CodeGeneratorUtils.MTURI, "ci");
                        ci.setTextContent(coord);
                        Element applyMinus = CodeGeneratorUtils.createElement(doc, CodeGeneratorUtils.MTURI, "apply");
                        Element minus = CodeGeneratorUtils.createElement(doc, CodeGeneratorUtils.MTURI, "minus");
                        Element zero = CodeGeneratorUtils.createElement(doc, CodeGeneratorUtils.MTURI, "cn");
                        zero.setTextContent("0");
                        Element dist = CodeGeneratorUtils.createElement(doc, CodeGeneratorUtils.MTURI, "cn");
                        dist.setTextContent(String.valueOf(distance));
                        lower.appendChild(and);
                        lower.appendChild(applyG);
                        lower.appendChild(getMainSchemaFovCondition(distance, coord, coordinates, regionId, "lower"));
                        applyG.appendChild(geq);
                        applyG.appendChild(applyMinus);
                        applyG.appendChild(zero);
                        applyMinus.appendChild(minus);
                        applyMinus.appendChild(ci);
                        applyMinus.appendChild(dist);
                    }
                    //Upper
                    Element upper = null;
                    if (sides.get(i).equals("Upper")) {
                        //Upper condition
                        upper = CodeGeneratorUtils.createElement(doc, CodeGeneratorUtils.MTURI, "apply");
                        Element and = CodeGeneratorUtils.createElement(doc, CodeGeneratorUtils.MTURI, "and");
                        //Coordinate limits condition
                        Element applyL = CodeGeneratorUtils.createElement(doc, CodeGeneratorUtils.MTURI, "apply");
                        Element lt = CodeGeneratorUtils.createElement(doc, CodeGeneratorUtils.MTURI, "lt");
                        Element ci = CodeGeneratorUtils.createElement(doc, CodeGeneratorUtils.MTURI, "ci");
                        ci.setTextContent(coord);
                        Element last = CodeGeneratorUtils.createElement(doc, CodeGeneratorUtils.MTURI, "ci");
                        last.setTextContent(coord + "last");
                        Element applyMinus = CodeGeneratorUtils.createElement(doc, CodeGeneratorUtils.MTURI, "apply");
                        Element plus = CodeGeneratorUtils.createElement(doc, CodeGeneratorUtils.MTURI, "plus");
                        Element dist = CodeGeneratorUtils.createElement(doc, CodeGeneratorUtils.MTURI, "cn");
                        dist.setTextContent(String.valueOf(distance));
                        upper.appendChild(and);
                        upper.appendChild(applyL);
                        upper.appendChild(getMainSchemaFovCondition(distance, coord, coordinates, regionId, "upper"));
                        applyL.appendChild(lt);
                        applyL.appendChild(applyMinus);
                        applyL.appendChild(last);
                        applyMinus.appendChild(plus);
                        applyMinus.appendChild(ci);
                        applyMinus.appendChild(dist);
                    }
                    //If both sides are checked, create an and condition
                    if (lower != null && upper != null) {
                        distanceElement = CodeGeneratorUtils.createElement(doc, CodeGeneratorUtils.MTURI, "apply");
                        Element and = CodeGeneratorUtils.createElement(doc, CodeGeneratorUtils.MTURI, "and");
                        distanceElement.appendChild(and);
                        distanceElement.appendChild(lower);
                        distanceElement.appendChild(upper);
                    }
                    else {
                        if (lower != null) {
                            distanceElement = lower;
                        }
                        else {
                            distanceElement = upper;
                        }
                    }
                    //Assing final condition
                    if (i == 0) {
                        conditionApply = distanceElement;
                    }
                    else {
                        //Append the new distance condition to the previous ones
                        Element tmp = CodeGeneratorUtils.createElement(doc, CodeGeneratorUtils.MTURI, "apply");
                        Element and = CodeGeneratorUtils.createElement(doc, CodeGeneratorUtils.MTURI, "and");
                        tmp.appendChild(and);
                        tmp.appendChild(conditionApply.cloneNode(true));
                        tmp.appendChild(distanceElement);
                        
                        conditionApply = tmp;
                    }
                }
            }
        }
        return conditionApply;
    }
    
    /**
     * Creates the condition for boundary spatial discretizations.
     * @param doc           A document reference to create the elements.
     * @param coordinates   The spatial coordinates
     * @param regionId      The current region id
     * @return              The simml element
     */
    public Element createBoundaryCondition(Document doc, ArrayList<String> coordinates, int regionId) {
        Element conditionApply = null;
        for (int j = 0; j < coordinates.size(); j++) {
            String coord = coordinates.get(j);
            for (int i = 0; i < distances.size(); i++) {
                int distance = distances.get(i);
                Element distanceElement = null;
                if (axes.get(i).equals(coord)) {
                    //Lower
                    Element lower = null;
                    if (sides.get(i).equals("Lower")) {
                        //Lower condition
                        lower = CodeGeneratorUtils.createElement(doc, CodeGeneratorUtils.MTURI, "apply");
                        Element and = CodeGeneratorUtils.createElement(doc, CodeGeneratorUtils.MTURI, "and");
                        //Coordinate limits condition
                        Element applyG = CodeGeneratorUtils.createElement(doc, CodeGeneratorUtils.MTURI, "apply");
                        Element geq = CodeGeneratorUtils.createElement(doc, CodeGeneratorUtils.MTURI, "geq");
                        Element ci = CodeGeneratorUtils.createElement(doc, CodeGeneratorUtils.MTURI, "ci");
                        ci.setTextContent(coord);
                        Element applyMinus = CodeGeneratorUtils.createElement(doc, CodeGeneratorUtils.MTURI, "apply");
                        Element minus = CodeGeneratorUtils.createElement(doc, CodeGeneratorUtils.MTURI, "minus");
                        Element zero = CodeGeneratorUtils.createElement(doc, CodeGeneratorUtils.MTURI, "cn");
                        zero.setTextContent("0");
                        Element dist = CodeGeneratorUtils.createElement(doc, CodeGeneratorUtils.MTURI, "cn");
                        dist.setTextContent(String.valueOf(distance));
                        lower.appendChild(and);
                        lower.appendChild(applyG);
                        lower.appendChild(getBoundaryFovCondition(distance, coord, coordinates, regionId, "lower"));
                        applyG.appendChild(geq);
                        applyG.appendChild(applyMinus);
                        applyG.appendChild(zero);
                        applyMinus.appendChild(minus);
                        applyMinus.appendChild(ci);
                        applyMinus.appendChild(dist);
                    }
                    //Upper
                    Element upper = null;
                    if (sides.get(i).equals("Upper")) {
                        //Upper condition
                        upper = CodeGeneratorUtils.createElement(doc, CodeGeneratorUtils.MTURI, "apply");
                        Element and = CodeGeneratorUtils.createElement(doc, CodeGeneratorUtils.MTURI, "and");
                        //Coordinate limits condition
                        Element applyL = CodeGeneratorUtils.createElement(doc, CodeGeneratorUtils.MTURI, "apply");
                        Element lt = CodeGeneratorUtils.createElement(doc, CodeGeneratorUtils.MTURI, "lt");
                        Element ci = CodeGeneratorUtils.createElement(doc, CodeGeneratorUtils.MTURI, "ci");
                        ci.setTextContent(coord);
                        Element last = CodeGeneratorUtils.createElement(doc, CodeGeneratorUtils.MTURI, "ci");
                        last.setTextContent(coord + "last");
                        Element applyMinus = CodeGeneratorUtils.createElement(doc, CodeGeneratorUtils.MTURI, "apply");
                        Element plus = CodeGeneratorUtils.createElement(doc, CodeGeneratorUtils.MTURI, "plus");
                        Element dist = CodeGeneratorUtils.createElement(doc, CodeGeneratorUtils.MTURI, "cn");
                        dist.setTextContent(String.valueOf(distance));
                        upper.appendChild(and);
                        upper.appendChild(applyL);
                        upper.appendChild(getBoundaryFovCondition(distance, coord, coordinates, regionId, "upper"));
                        applyL.appendChild(lt);
                        applyL.appendChild(applyMinus);
                        applyL.appendChild(last);
                        applyMinus.appendChild(plus);
                        applyMinus.appendChild(ci);
                        applyMinus.appendChild(dist);
                    }
                    //If both sides are checked, create an and condition
                    if (lower != null && upper != null) {
                        distanceElement = CodeGeneratorUtils.createElement(doc, CodeGeneratorUtils.MTURI, "apply");
                        Element and = CodeGeneratorUtils.createElement(doc, CodeGeneratorUtils.MTURI, "and");
                        distanceElement.appendChild(and);
                        distanceElement.appendChild(lower);
                        distanceElement.appendChild(upper);
                    }
                    else {
                        if (lower != null) {
                            distanceElement = lower;
                        }
                        else {
                            distanceElement = upper;
                        }
                    }
                    //Assing final condition
                    if (i == 0) {
                        conditionApply = distanceElement;
                    }
                    else {
                        //Append the new distance condition to the previous ones
                        Element tmp = CodeGeneratorUtils.createElement(doc, CodeGeneratorUtils.MTURI, "apply");
                        Element and = CodeGeneratorUtils.createElement(doc, CodeGeneratorUtils.MTURI, "and");
                        tmp.appendChild(and);
                        tmp.appendChild(conditionApply.cloneNode(true));
                        tmp.appendChild(distanceElement);
                        
                        conditionApply = tmp;
                    }
                }
            }
        }
        return conditionApply;
    }
    
    /**
     * Creates the region checking condition.
     * @param distance      The distance to check.
     * @param coordinate    The current coordinate
     * @param coordinates   The spatial coordinates
     * @param regionId      The region id
     * @param direction     The direction to check
     * @return              The condition
     */
    private Element getBoundaryFovCondition(int distance, String coordinate, ArrayList<String> coordinates, int regionId, String direction) {
        String operation = "minus";
        if (direction.equals("upper")) {
            operation = "plus";
        }
        //Create the condition to check the inner points that has to belong to the region
        Element innerCondition = null;
        for (int i = 1; i < distance; i++) {
            Element nearPoint = CodeGeneratorUtils.createElement(docRef, CodeGeneratorUtils.MTURI, "apply");
            Element gt = CodeGeneratorUtils.createElement(docRef, CodeGeneratorUtils.MTURI, "gt");
            Element fovApply = CodeGeneratorUtils.createElement(docRef, CodeGeneratorUtils.MTURI, "apply");
            Element fov = CodeGeneratorUtils.createElement(docRef, CodeGeneratorUtils.MTURI, "ci");
            fov.setTextContent("FOV_" + regionId);
            fovApply.appendChild(fov);
            fovApply.appendChild(CodeGeneratorUtils.deployOperatedCoordinates(docRef, coordinates, coordinate, operation, i));
            Element zero = CodeGeneratorUtils.createElement(docRef, CodeGeneratorUtils.MTURI, "cn");
            zero.setTextContent("0");
            nearPoint.appendChild(gt);
            nearPoint.appendChild(fovApply);
            nearPoint.appendChild(zero);
            if (i == 1) {
                innerCondition = nearPoint;
            }
            else {
                //Append the new distance condition to the previous ones
                Element tmp = CodeGeneratorUtils.createElement(docRef, CodeGeneratorUtils.MTURI, "apply");
                Element and = CodeGeneratorUtils.createElement(docRef, CodeGeneratorUtils.MTURI, "and");
                tmp.appendChild(and);
                tmp.appendChild(innerCondition.cloneNode(true));
                tmp.appendChild(nearPoint);
                
                innerCondition = tmp;
            }
        }
        Element outerPoint = CodeGeneratorUtils.createElement(docRef, CodeGeneratorUtils.MTURI, "apply");
        Element eq = CodeGeneratorUtils.createElement(docRef, CodeGeneratorUtils.MTURI, "eq");
        Element fovApply = CodeGeneratorUtils.createElement(docRef, CodeGeneratorUtils.MTURI, "apply");
        Element fov = CodeGeneratorUtils.createElement(docRef, CodeGeneratorUtils.MTURI, "ci");
        fov.setTextContent("FOV_" + regionId);
        fovApply.appendChild(fov);
        fovApply.appendChild(CodeGeneratorUtils.deployOperatedCoordinates(docRef, coordinates, coordinate, operation, distance));
        Element zero = CodeGeneratorUtils.createElement(docRef, CodeGeneratorUtils.MTURI, "cn");
        zero.setTextContent("0");
        outerPoint.appendChild(eq);
        outerPoint.appendChild(fovApply);
        outerPoint.appendChild(zero);
        if (innerCondition != null) {
            //Add the condition to check the point outside the region (boundary)
            Element result = CodeGeneratorUtils.createElement(docRef, CodeGeneratorUtils.MTURI, "apply");
            Element and = CodeGeneratorUtils.createElement(docRef, CodeGeneratorUtils.MTURI, "and");
            result.appendChild(and);
            result.appendChild(innerCondition.cloneNode(true));
            result.appendChild(outerPoint);

            return result;
        }
		return outerPoint;
       
    }
    
    /**
     * Creates the region checking condition.
     * @param distance      The distance to check.
     * @param coordinate    The current coordinate
     * @param coordinates   The spatial coordinates
     * @param regionId      The region id
     * @param direction     The direction to check
     * @return              The condition
     */
    private Element getMainSchemaFovCondition(int distance, String coordinate, ArrayList<String> coordinates, int regionId, String direction) {
        Element fov = CodeGeneratorUtils.createElement(docRef, CodeGeneratorUtils.MTURI, "ci");
        fov.setTextContent("FOV_" + regionId);
        Element gt = CodeGeneratorUtils.createElement(docRef, CodeGeneratorUtils.MTURI, "gt");
        Element zero = CodeGeneratorUtils.createElement(docRef, CodeGeneratorUtils.MTURI, "cn");
        zero.setTextContent("0");
        if (direction.equals("upper")) {
            //upper side check
            Element upperPoint = CodeGeneratorUtils.createElement(docRef, CodeGeneratorUtils.MTURI, "apply");
            Element fovApplyUpper = CodeGeneratorUtils.createElement(docRef, CodeGeneratorUtils.MTURI, "apply");
            fovApplyUpper.appendChild(fov);
            fovApplyUpper.appendChild(CodeGeneratorUtils.deployOperatedCoordinates(docRef, coordinates, coordinate, "plus", distance));
            upperPoint.appendChild(gt);
            upperPoint.appendChild(fovApplyUpper);
            upperPoint.appendChild(zero);
            return upperPoint;
        }
		//lower side check
		Element lowerPoint = CodeGeneratorUtils.createElement(docRef, CodeGeneratorUtils.MTURI, "apply");
		Element fovApplyLower = CodeGeneratorUtils.createElement(docRef, CodeGeneratorUtils.MTURI, "apply");
		fovApplyLower.appendChild(fov);
		fovApplyLower.appendChild(CodeGeneratorUtils.deployOperatedCoordinates(docRef, coordinates, coordinate, "minus", distance));
		lowerPoint.appendChild(gt);
		lowerPoint.appendChild(fovApplyLower);
		lowerPoint.appendChild(zero);
		return lowerPoint;
    }
}
