/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.codeGeneratorPlugin.utils;

import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * The region common information.
 * @author bminyano
 *
 */
public class MappingRegionInfo {

    private String regionName;
    private ArrayList<LinkedHashMap<String, CoordinateInfo>> coordinateLimits;
    private int id;
    private ArrayList<String> x3d;
    private boolean surface;
    private boolean interior;
    private boolean fullDomain;
    
    /**
     * Constructor.
     * @param regionName            Region name
     * @param coordinateLimits      The coordinate limits
     * @param x3dId					Ids of x3d
     * @param id                    The id of the region
     * @param hasInterior           If the region has interior
     * @param hasSurface            If the region has surface
     * @param fullDomain            True if the region is defined in the full domain
     */
    public MappingRegionInfo(String regionName, ArrayList<LinkedHashMap<String, CoordinateInfo>> coordinateLimits, int id, ArrayList<String> x3dId, boolean hasInterior, 
            boolean hasSurface, boolean fullDomain) {
        this.regionName = regionName;
        this.coordinateLimits = coordinateLimits;
        this.id = id;
        x3d = x3dId;
        surface = hasSurface;
        interior = hasInterior;
        this.fullDomain = fullDomain;
    }
    
    public String getRegionName() {
        return regionName;
    }
    public ArrayList<LinkedHashMap<String, CoordinateInfo>> getCoordinateLimits() {
        return coordinateLimits;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public boolean isFullDomain() {
        return fullDomain;
    }
    public void setFullDomain(boolean fullDomain) {
        this.fullDomain = fullDomain;
    }

    public ArrayList<String> getX3d() {
		return x3d;
	}

	public void setX3d(ArrayList<String> x3d) {
		this.x3d = x3d;
	}

	/**
     * True if the region has surface.
     * @return  True if the region has surface
     */
    public boolean hasSurface() {
        return surface;
    }
    /**
     * True if the region has interior.
     * @return  True if the region has interior
     */
    public boolean hasInterior() {
        return interior;
    }
}
