/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.codeGeneratorPlugin.fvm;

import eu.iac3.mathMS.codeGeneratorPlugin.CGException;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.CactusUtils;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.CodeGeneratorUtils;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.HeaderInfo;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.ProblemInfo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Execution flow for FVM code in Cactus.
 * @author bminyano
 *
 */
public final class CactusExecution {
    static final String NL = System.getProperty("line.separator"); 
    static final String IND = "\u0009";
    static final String MTURI = "http://www.w3.org/1998/Math/MathML";
    static final String MMSURI = "urn:mathms";
    static final int THREE = 3;
    static final int FOUR = 4;
    
    /**
     * Private constructor.
     */
    private CactusExecution() { };
    
    /**
     * Generate the content for the ExecutionFlow.m4 file.
     * @param hi                    the header information
     * @param problemInfo           The problem info
     * @return                      the content of the file
     * @throws CGException          CG003 Not possible to create code for the platform
     *                              CG004 External error
     */
    public static String executionFlowM4(HeaderInfo hi, ProblemInfo problemInfo) throws CGException {
        //Parameters for the xsl transformation. It is a condition
        String[][] xslParams = new String [2][2];
        xslParams[0][0] =  "condition";
        xslParams[0][1] =  "false";
        xslParams[1][0] =  "indent";
        xslParams[1][1] =  "2";
        Document doc = problemInfo.getProblem();
        try {
            //Check if parabolic terms are used
            boolean parabolicTerms = CodeGeneratorUtils.find(doc, "//*[@type='parabolicTerm']").getLength() > 0;
            //Check if flat or maximal dissipation
            boolean orderBoundaries = CodeGeneratorUtils.find(doc, "//mms:boundaryCondition[mms:type='Flat' or " 
                    + "mms:type='Maximal dissipation' or mms:type='Reflection']").getLength() > 0;
            String localVariables = "!" + IND + IND + "Declare local variables" + NL
                + "!" + IND + IND + "-----------------" + NL + NL;
            //Declaration of spatial coordinates, and its variables for start and end.
            String coordIndex = "";
            for (int i = 0; i < problemInfo.getCoordinates().size(); i++) {
                String coord = problemInfo.getCoordinates().get(i);
                localVariables = localVariables + IND + IND + "INTEGER " + coord + ", " + coord + "Start, " + coord + "End" + NL;
                coordIndex =  coordIndex + problemInfo.getCoordinates().get(i) + ", ";
            }
            coordIndex = coordIndex.substring(0, coordIndex.lastIndexOf(","));
            
            //Declaration of deltas
            //The scope is the finalization condition and the execution flow
            DocumentFragment varScope = doc.createDocumentFragment();
            NodeList fc = CodeGeneratorUtils.find(doc, "//mms:finalizationConditions/mms:auxiliarDefinitions");
            for (int i = 0; i < fc.getLength(); i++) {
                varScope.appendChild(fc.item(i).cloneNode(true));
            }
            NodeList scopes = CodeGeneratorUtils.find(doc, "//mms:interiorExecutionFlow|//mms:surfaceExecutionFlow");
            for (int i = 0; i < scopes.getLength(); i++) {
                varScope.appendChild(scopes.item(i).cloneNode(true));
            }
            //Variable for the status of the filling internal ghost points
            localVariables = localVariables + IND + IND + "INTEGER istat, status, ReflectionCounter" + NL;
            //Declaration of variables and functions
            localVariables = localVariables 
                + CactusUtils.getVariableDeclaration(varScope, problemInfo.getIdentifiers(), problemInfo.getVariables()) + NL;
            localVariables = localVariables + CactusUtils.getFunctionDeclaration(doc, varScope) + NL;
            
            String code = "";
            //Check finalization conditions at the beginning
            //Get the finalization condition
            NodeList finalization = CodeGeneratorUtils.find(doc, "//mms:finalizationCondition");
            //If there are finalization conditions we have to declare an additional variable for the CCTK_exit function
            if (finalization.getLength() > 0) {
                localVariables = localVariables + IND + IND + "CCTK_INT exitCond" + NL;
                code = code + "!" + IND + IND + "Finalization condition" + NL;
                code = code + getFinalizationConditions(doc);
            }
            //Get all deltas
            localVariables = localVariables + CactusUtils.getAllDeltas(doc, problemInfo.getGridLimits(), 
                    problemInfo.getPeriodicalBoundary(), problemInfo.getSchemaStencil());
            
            code = code + NL + "!" + IND + IND + "Evolution" + NL;
            for (int i = 0; i < problemInfo.getCoordinates().size(); i++) {
                //Initialization of the variables
                String coordString = problemInfo.getCoordinates().get(i);
                code = code + IND + IND + coordString + "Start = 1" + NL;
                code = code + IND + IND + coordString + "End = cctk_lsh(" + (i + 1) + ")" + NL;
            }
            //Iterate over all the blocks
            int blocks = CodeGeneratorUtils.getBlocksNum(doc, "//mms:interiorExecutionFlow/sml:simml|//mms:surfaceExecutionFlow/sml:simml");
            for (int i = 0; i < blocks; i++) {
                boolean boundary = CodeGeneratorUtils.find(doc, 
                        "//mms:instructionSet/*[position() = " + (i + 1) + " and local-name() = 'boundary']/*").getLength() > 0;
                //Put the parameter "condition" to false for the xsl parameters
                xslParams[0][1] =  "false";
                
                //For boundaries and parabolicTerms the loops are individual for each segment (diagonal fluxes)
                String block = "";
                String actualIndent = IND + IND;
                for (int j = 0; j < problemInfo.getSpatialDimensions(); j++) {
                    actualIndent = IND + actualIndent;
                }
                boolean extrapolation = CodeGeneratorUtils.find(doc, "//mms:instructionSet/*[position() = " + (i + 1) 
                        + " and local-name() = 'boundary']//fieldExtrapolation").getLength() > 0;
                boolean fluxes = CodeGeneratorUtils.find(doc, "//mms:instructionSet/*[position() = " + (i + 1) 
                        + " and local-name() = 'boundary' and @type='flux']//fieldExtrapolation").getLength() > 0;
                if (extrapolation && !fluxes) {
                    block = block + createExecutionBlockLoop(problemInfo)
                            + extrapolationInitialization(problemInfo, i, actualIndent)
                            + CactusUtils.createExecutionBlockLoopEnd(problemInfo);
                }
                
                if (!(boundary && parabolicTerms && orderBoundaries)) {
                    block = block + createExecutionBlockLoop(problemInfo);
                }

                //Create a blockScope with the instructions of the block to synchronize in cactus only for the variables assigned in this scope
                DocumentFragment blockScope = doc.createDocumentFragment();
                NodeList blockList = CodeGeneratorUtils.find(doc, "//mms:interiorExecutionFlow/" 
                        + "mms:instructionSet/*[position() = " + (i + 1) + " and (@stencil > 0  or descendant::interiorDomainContext)]|//"
                        + "mms:surfaceExecutionFlow/" 
                        + "mms:instructionSet/*[position() = " + (i + 1) + " and (@stencil > 0  or descendant::interiorDomainContext)]");
                for (int j = 0; j < blockList.getLength(); j++) {
                    blockScope.appendChild(blockList.item(j).cloneNode(true));
                }
                
                String segment = "";
                String extrapolationEndAssignments = "";
                //For every segment (first the lower precedents)
                for (int j = problemInfo.getRegions().size() - 1; j >= 0; j--) {
                    String segmentName = problemInfo.getRegions().get(j);
                    segment = segment + segmentExecutionFlow(problemInfo, segmentName, actualIndent, i, j, "interior",
                            boundary && parabolicTerms && orderBoundaries);
                    segment = segment + segmentExecutionFlow(problemInfo, segmentName, actualIndent, i, j, "surface",
                            boundary && parabolicTerms && orderBoundaries);
                    if (extrapolation && !fluxes) {
                        NodeList boundaryList = CodeGeneratorUtils.find(doc, "//mms:interiorExecutionFlow[preceding-sibling::" 
                                + "mms:segmentGroupName = '" + segmentName + "']/mms:instructionSet/*[position() = " + (i + 1) 
                                + " and child::*]");
                        for (int k = 0; k < boundaryList.getLength(); k++) {
                            Element postInit = (Element) boundaryList.item(k);
                            extrapolationEndAssignments = extrapolationEndAssignments + extrapolationFinalization(problemInfo, 
                                    actualIndent, postInit, (j * 2) + 1);
                        }
                        boundaryList = CodeGeneratorUtils.find(doc, "//mms:surfaceExecutionFlow[preceding-sibling::" 
                                + "mms:segmentGroupName = '" + segmentName + "']/mms:instructionSet/*[position() = " + (i + 1) 
                                + " and child::*]");
                        for (int k = 0; k < boundaryList.getLength(); k++) {
                            Element postInit = (Element) boundaryList.item(k);
                            extrapolationEndAssignments = extrapolationEndAssignments + extrapolationFinalization(problemInfo, actualIndent, 
                                    postInit, (j * 2) + 1);
                        }
                    }
                }
                //Check if the structure must be special for the type of boundaries (flat, reflection and maximal dissipation)
                if (boundary && parabolicTerms && orderBoundaries) {
                    block = block + CactusUtils.createFlatLoopStructure(problemInfo, segment);
                }
                else {
                    block = block + segment + CactusUtils.createExecutionBlockLoopEnd(problemInfo);
                }
                
                //Extrapolation finalization
                if (extrapolation && !extrapolationEndAssignments.equals("")) {
                    block = block + createExecutionBlockLoop(problemInfo)
                            + extrapolationEndAssignments
                            + CactusUtils.createExecutionBlockLoopEnd(problemInfo);
                }
                
                //Check if the block is not empty and add it to the code
                if (!segment.equals("")) {
                    //Call ghost routines
                    block = block + callGhostRoutines(hi.getProblemId(), problemInfo.getGroupInfo().getGroupSyncs().get(i));
                    code = code + block;
                }
            }
            return "/*@@" + NL
                + "  @file      ExecutionFlow.F" + NL
                + "  @date      " + hi.getDate().toString() + NL      
                + "  @author    " + hi.getAuthor() + NL
                + "  @desc" + NL 
                + "             Execution flow for " + hi.getProblemId() + NL
                + "  @enddesc" + NL 
                + "  @version " + hi.getVersion() + NL
                + "@@*/" + NL + NL
                + getMacroFunctions(problemInfo) + NL
                + "#include \"cctk.h\"" + NL
                + "#include \"cctk_Parameters.h\"" + NL
                + "#include \"cctk_Arguments.h\"" + NL
                + "#include \"cctk_Functions.h\"" + NL + NL
                + IND + "subroutine " + hi.getProblemId() + "_execution(CCTK_ARGUMENTS)" + NL + NL
                + IND + IND + "implicit none" + NL + NL
                + IND + IND + "DECLARE_CCTK_ARGUMENTS" + NL
                + IND + IND + "DECLARE_CCTK_PARAMETERS" + NL 
                + IND + IND + "DECLARE_CCTK_FUNCTIONS" + NL + NL
                + localVariables + NL
                + code + NL
                + IND + "end subroutine " + hi.getProblemId() + "_execution";
        } 
        catch (CGException e) {
            throw e;
        }
        catch (Exception e) {
            throw new CGException(CGException.CG00X, e.getMessage());
        }
    }
    
    /**
     * Creates the loop for an execution block.
     * @param problemInfo   The problem information
     * @return              The code
     */
    private static String createExecutionBlockLoop(ProblemInfo problemInfo) {
        String actualIndent = IND + IND;
        //Call the filling ghost routines
        String block = "";
        //Initialization of the variables and setting the loop iteration
        for (int j = problemInfo.getCoordinates().size() - 1; j >= 0; j--) {
            String coordString = problemInfo.getCoordinates().get(j);
            //Loop iteration
            block =  block + actualIndent + "do " + coordString + " = " + coordString + "Start, " + coordString + "End" + NL;
            actualIndent = actualIndent + IND;
        }
        return block;
    }
    
    /**
     * Get the finalization condition of the problem.
     * @param doc               The problem information
     * @return                  A string with the fortran code.
     * @throws CGException      CG004 External error
     */
    private static String getFinalizationConditions(Document doc) throws CGException {
        
        String[][] xslParams = new String [2][2];
        xslParams[0][0] =  "condition";
        xslParams[0][1] =  "false";
        xslParams[1][0] =  "indent";
        xslParams[1][1] =  "2";
        String code = "";
        final int andLength = 7;
        try {
            //Get the auxiliary definitions
            String query = "//mms:finalizationConditions/mms:auxiliarDefinitions/mms:instructionSet";
            NodeList auxDefinitions = CodeGeneratorUtils.find(doc, query);
            for (int j = 0; j < auxDefinitions.getLength(); j++) {
                code = code + CactusUtils.xmlTransform(auxDefinitions.item(j), xslParams) + NL;
            }
            xslParams[0][1] =  "true";
            xslParams[1][1] =  "0";
            NodeList finalization = CodeGeneratorUtils.find(doc, "//mms:finalizationCondition");
            for (int i = 0; i < finalization.getLength(); i++) {
                String actualIndent = IND + IND;
                NodeList equations = finalization.item(i).getChildNodes();
                int equationsFrom = 0;
                //If there is a condition
                if (finalization.item(i).getLocalName().equals("condition")) {
                    equationsFrom = 1;
                    String instruction = CactusUtils.xmlTransform(equations.item(0).getFirstChild().getFirstChild(), xslParams);
                    code = code + actualIndent + "if (" + instruction + ") then" + NL;
                    actualIndent = actualIndent + IND;
                }
                
                String endCondition = "";
                //Finalization conditions
                for (int k = equationsFrom; k < equations.getLength(); k++) {
                    endCondition = endCondition + " .AND. " + CactusUtils.xmlTransform(
                                equations.item(k).getLastChild().getFirstChild(), xslParams);
                }
                endCondition = endCondition.substring(andLength);
                code = code + actualIndent + "if (" + endCondition + ") then " + NL + actualIndent + IND 
                    + "call CCTK_Exit(exitCond , cctkGH, 0)" + NL + actualIndent + "end if" + NL;
                
                //Close if clause if exists
                if (equationsFrom == 1) {
                    actualIndent = actualIndent.substring(0, actualIndent.lastIndexOf(IND));
                    code = code + actualIndent + "end if" + NL + NL;
                }
            }
            return code;
        } 
        catch (Exception e) {
            throw new CGException(CGException.CG00X, e.getMessage());
        }
    }
    
    /**
     * Write code to call ghost point copy routines.
     * @param problemIdentifier     name of the thorn
     * @param groupSyncVars         The variable groups to synchronize
     * @return                      the code with the calls
     * @throws CGException          CG003 Not possible to create code for the platform
     *                              CG004 External error
     */
    private static String callGhostRoutines(String problemIdentifier, ArrayList<String> groupSyncVars) throws CGException {
        String result = "";
        if (groupSyncVars != null) {
            for (int i = 0; i < groupSyncVars.size(); i++) {
                result = result + IND + IND + "call CCTK_SyncGroup(status, cctkGH, \"" 
                    + problemIdentifier + "::" + CactusUtils.variableNormalize(groupSyncVars.get(i)) + "\")" + NL;
            }
        }
        return result;
    }
    
    /**
     * Gets the functions that could be used as a macro.
     * @param problemInfo       The problem information
     * @return                  The code of the macros
     * @throws CGException      CG004 External error
     */
    private static String getMacroFunctions(ProblemInfo problemInfo) 
        throws CGException {
        try {
            String macros = "";
            String[][] params = new String [2][2];
            params[0][0] = "condition";
            params[0][1] = "false";
            params[1][0] = "indent";
            params[1][1] = "0";
            
            //Get the dimensions
            Document doc = problemInfo.getProblem();
            
            NodeList functionList = CodeGeneratorUtils.find(doc, "//mms:function");
            for (int i = 0; i < functionList.getLength(); i++) {
                boolean isMacro = CodeGeneratorUtils.isMacroFunction((Element) functionList.item(i));
                //Only write macro functions
                if (isMacro) {
                    //Function name
                    Element funcName = (Element) functionList.item(i).getFirstChild();
                    //Function arguments and declaration
                    if (funcName.getNextSibling().getLocalName().equals("functionParameters")) {
                        NodeList args = funcName.getNextSibling().getChildNodes();
                        for (int j = 0; j < args.getLength(); j++) {
                            NodeList paramOccurrences = CodeGeneratorUtils.find(functionList.item(i), 
                                    ".//mt:ci[text() = '" + args.item(j).getFirstChild().getTextContent() + "']");
                            String paramType = args.item(j).getLastChild().getTextContent();
                            for (int k = 0; k < paramOccurrences.getLength(); k++) {
                                paramOccurrences.item(k).setTextContent("$" + String.valueOf(j + 2));
                                //Insert parenthesis if the parameter type is not field
                                if (!paramType.equals("field")) {
                                    Element parent = (Element) paramOccurrences.item(k).getParentNode();
                                    Element nothing = CodeGeneratorUtils.createElement(doc, MTURI, "ci");
                                    Element apply = CodeGeneratorUtils.createElement(doc, MTURI, "apply");
                                    apply.appendChild(nothing);
                                    apply.appendChild(paramOccurrences.item(k).cloneNode(true));
                                    parent.replaceChild(apply, paramOccurrences.item(k));
                                }
                        
                            }
                        }
                    }
                    //Function instructions
                    String instructions = "";
                    NodeList instructionList = functionList.item(i).getLastChild().getFirstChild().getChildNodes();
                    for (int j = 0; j < instructionList.getLength(); j++) {
                        instructions = instructions + CactusUtils.xmlTransform(instructionList.item(j), params);
                    }
                    //Delete the return asignation
                    instructions = instructions.replaceAll("#function# = ", "");
                   
                    macros = macros 
                        + "define(" + CactusUtils.variableNormalize(funcName.getTextContent()) + "," + instructions + ")dnl" + NL;
                }
            }
            return macros;
        } 
        catch (Exception e) {
            throw new CGException(CGException.CG00X, e.getMessage());
        }
    }
    
    /**
     * Generates the execution flow code for a segment.
     * @param problemInfo       The problem common information
     * @param segmentName       The segment name
     * @param actualIndent      The actual indent 
     * @param blockNum          The block number
     * @param segmentNum        The segment number
     * @param segmentType       The segment type (interior or surface)
     * @param orderBoundaries   true if order boundaries are used
     * @return                  The code
     * @throws CGException      CG004 External error
     */
    private static String segmentExecutionFlow(ProblemInfo problemInfo, String segmentName, String actualIndent, int blockNum, 
            int segmentNum, String segmentType, boolean orderBoundaries) throws CGException {
        //Parameters for the xsl transformation. It is a condition
        String[][] xslParams = new String [2][2];
        xslParams[0][0] =  "condition";
        xslParams[0][1] =  "false";
        xslParams[1][0] =  "indent";
        Document doc = problemInfo.getProblem();
        String code = "";
        //Generate the index for the coordinates
        String index = "";
        for (int i = 0; i < problemInfo.getCoordinates().size(); i++) {
            index =  index + problemInfo.getCoordinates().get(i) + ", ";
        }
        index = index.substring(0, index.lastIndexOf(","));
        boolean boundPTerms = CodeGeneratorUtils.find(doc, "//mms:" + segmentType + "ExecutionFlow[preceding-sibling::"  
                + "mms:segmentGroupName = '" + segmentName + "']//*[@type='parabolicTerm']").getLength() > 0;
                
        NodeList blockList = CodeGeneratorUtils.find(doc, "//mms:" + segmentType + "ExecutionFlow[preceding-sibling::" 
                + "mms:segmentGroupName = '" + segmentName + "']/mms:instructionSet/*[" + (blockNum + 1) + "]");
        for (int i = 0; i < blockList.getLength(); i++) {
            Element block = (Element) CactusUtils.preProcessBlock(blockList.item(i).cloneNode(true), problemInfo);
            
            if (block.hasChildNodes()) {
                //Time step
                if (block.getLocalName().equals("timeStep")) {
                    int stencil = Integer.parseInt(block.getAttribute("stencil"));
                    int append = Integer.parseInt(block.getAttribute("append"));
                    //Add condition to calculate only inside of the array limits when there are stencil
                    String insideLimitsCondition = "";
                    if (stencil > 0) {
                        insideLimitsCondition = " .and. " + getLimitCondition(problemInfo.getCoordinates(), stencil);
                    }

                    //Condition for the boundaries
                    int segmentId = (segmentNum * 2) + 1;
                    if (segmentType.equals("surface")) {
                        segmentId++;
                    }
                    String segmentInteractionCondition = "";
                    if (append > 0) {
                        boolean parabolicTerms = CodeGeneratorUtils.find(block, ".//*[@type='parabolicTerm']").getLength() > 0;
                        segmentInteractionCondition = getCondition(problemInfo.getRegionRelations().get(segmentName), problemInfo.getRegions(), 
                                problemInfo.getCoordinates(), append, segmentId, parabolicTerms);
                    }
                    code = code + actualIndent + "if ((FOV_" + segmentId + "(" + index + ") .gt. 0"  
                        + segmentInteractionCondition + ")" + insideLimitsCondition + ") then" + NL;
                    actualIndent = actualIndent + IND;
                    
                    //Execution flow instruction set
                    xslParams[1][1] = String.valueOf(actualIndent.length());
                    NodeList executionInstruction = block.getChildNodes();
                    for (int k = 0; k < executionInstruction.getLength(); k++) {
                        Node instruction = executionInstruction.item(k);
                        code = code + CactusUtils.xmlTransform(instruction, xslParams) + NL;
                    }
                    
                    //Close segment
                    actualIndent = actualIndent.substring(0, actualIndent.lastIndexOf(IND));
                    code = code + actualIndent + "end if" + NL;
                }
                //Boundary
                if (problemInfo.getPeriodicalBoundary().containsValue("noperiodical") && block.getLocalName().equals("boundary")) {
                    code = code + "!" + actualIndent + "Boundaries" + NL;
                    String completeSegmentName = segmentName;
                    if (segmentType.equals("interior")) {
                        completeSegmentName = completeSegmentName + "I";
                    }
                    else {
                        completeSegmentName = completeSegmentName + "S";
                    }
                    code = code + processBoundaries(block, problemInfo, completeSegmentName, (segmentNum * 2) + 1, segmentType,
                        orderBoundaries, boundPTerms);
                }
            }
        }
        return code;
    }
    
    /**
     * Process a boundary condition block.
     * @param boundary              The boundary block
     * @param pi                    The problem info
     * @param segmentName           The segment name
     * @param segBaseNumber         The number of the segment (Interior)
     * @param segmentType           The segment type
     * @param orderBoundaries       true if order boundaries are used
     * @param boundPTerms           true if the boundaries must be deployed for parabolic terms
     * @return                      The fortran code to process the boundary
     * @throws CGException          CG004 External error
     */
    public static String processBoundaries(Element boundary, ProblemInfo pi, String segmentName, 
            int segBaseNumber, String segmentType, boolean orderBoundaries, boolean boundPTerms) throws CGException {
        Document doc = pi.getProblem();
        String[][] xslParams = new String [2][2];
        xslParams[0][0] =  "condition";
        xslParams[0][1] =  "false";
        xslParams[1][0] =  "indent";
        String code = "";
        String indent = IND + IND;
        HashMap<String, String> periodicalBoundary = pi.getPeriodicalBoundary();
        ArrayList<String> coordinates = pi.getCoordinates();
        
        boolean fluxes = false;
        if (boundary.getAttribute("type").equals("flux")) {
            fluxes = true;
        }
        
        try {
            String index = "";
            for (int i = 0; i < coordinates.size(); i++) {
                index = index + coordinates.get(i) + ", ";
            }
            index = index.substring(0, index.lastIndexOf(", "));
            String stencil = String.valueOf(pi.getSchemaStencil());
            for (int i = 0; i < coordinates.size(); i++) {
                String coordString = coordinates.get(i);
                //Only write non periodical boundaries
                if (periodicalBoundary.get(coordString).equals("noperiodical")) {
                    NodeList boundaries = CodeGeneratorUtils.find(boundary, ".//" + coordString);
                    for (int j = 0; j < boundaries.getLength(); j++) {
                        Element coordBoundary = (Element) boundaries.item(j);
                        String axis = CodeGeneratorUtils.discToCont(pi.getProblem(), coordString);
                        //Left boundaries
                        if (CodeGeneratorUtils.find(coordBoundary, ".//lower").getLength() > 0) {
                            int boundId = pi.getCoordinates().indexOf(CodeGeneratorUtils.contToDisc(pi.getProblem(), axis)) * 2 + 1;
                            indent = indent + IND;
                            
                            //Switch the order of execution of the lower boundaries
                            Element newIndex = CodeGeneratorUtils.createElement(doc, MTURI, "apply");
                            Element operation = CodeGeneratorUtils.createElement(doc, MTURI, "minus");
                            Element cn = CodeGeneratorUtils.createElement(doc, MTURI, "cn");
                            cn.setTextContent(String.valueOf(Integer.valueOf(stencil) + 1));
                            Element ci = CodeGeneratorUtils.createElement(doc, MTURI, "ci");
                            ci.setTextContent(coordString);
                            newIndex.appendChild(operation);
                            newIndex.appendChild(cn);
                            newIndex.appendChild(ci);
                            xslParams[1][1] =  String.valueOf(FOUR);
                            String instructions = "";
                            String extrapolations = "";
                            NodeList lower = CodeGeneratorUtils.find(coordBoundary, ".//lower");
                            for (int k = 0; k < lower.getLength(); k++) {
                                NodeList children = lower.item(k).getChildNodes();
                                for (int l = 0; l < children.getLength(); l++) {
                                    //Boundary
                                    if (!children.item(l).getLocalName().equals("fieldExtrapolation")) {
                                        Element replaced = (Element) children.item(l).cloneNode(true);
                                        if (!orderBoundaries) {
                                            NodeList coords = CodeGeneratorUtils.find(replaced, ".//mt:ci[text() = '" + coordString + "']");
                                            for (int n = coords.getLength() - 1; n >= 0; n--) {
                                                coords.item(n).getParentNode().replaceChild(newIndex.cloneNode(true), coords.item(n));
                                            }
                                        }
                                        instructions = instructions + CactusUtils.xmlTransform(replaced, xslParams) + NL;
                                    }
                                    //Boundary extrapolation
                                    else {
                                        String field = ((Element) children.item(l)).getAttribute("field");
                                        String fieldTransf = CactusUtils.variableNormalize(field);
                                        extrapolations = extrapolations + "!" + indent + "Segment field extrapolations" + NL
                                            + indent + "if (" + CactusUtils.hardRegionCondition(index, pi.getCoordinates(), fieldTransf) 
                                            + ") then" + NL;
                                        indent = indent + IND;
                                        if (CodeGeneratorUtils.isAuxiliaryField(pi, field)) {
                                            String auxSegmentName = ((Element) children.item(l).getFirstChild()).getAttribute("segment");
                                            extrapolations = extrapolations + indent + "if (" 
                                                    + createExtrapolSegmentConditionBound(pi, auxSegmentName, fieldTransf) + ") then" + NL;
                                            indent = indent + IND;
                                            NodeList lowerInstructions = children.item(l).getFirstChild().getChildNodes();
                                            xslParams[1][1] =  String.valueOf(indent.length());
                                            for (int m = 0; m < lowerInstructions.getLength(); m++) {
                                                extrapolations = extrapolations + CactusUtils.xmlTransform(lowerInstructions.item(m), xslParams) + NL;
                                            }
                                            indent = indent.substring(1);
                                            extrapolations = extrapolations + indent + "end if" +  NL;
                                        }
                                        else {
                                            NodeList lowerInstructions = children.item(l).getChildNodes();
                                            xslParams[1][1] =  String.valueOf(indent.length());
                                            for (int m = 0; m < lowerInstructions.getLength(); m++) {
                                                extrapolations = extrapolations + CactusUtils.xmlTransform(lowerInstructions.item(m), xslParams) + NL;
                                            }
                                        }
                                        indent = indent.substring(1);
                                        extrapolations = extrapolations + indent + "end if" +  NL;
                                    }
                                }
                            }
                            indent = indent.substring(0, indent.lastIndexOf(IND));
                            if (!(extrapolations.equals("") && instructions.equals(""))) {
                                code = code + indent + "if ((FOV_" + CodeGeneratorUtils.getRegionName(pi.getProblem(), -boundId, pi.getCoordinates()) 
                                        + "(" + index + ") .gt. 0) .and. (" 
                                        + CactusUtils.getLeftCondition(boundPTerms, Integer.parseInt(stencil), segBaseNumber, i, coordinates, 
                                                pi.getRegionIds()) 
                                        + ")) then" + NL;
                                code = code + extrapolations + instructions;
                                code = code + indent + "end if" + NL;
                            }
                        }
                        //Right boundaries
                        if (CodeGeneratorUtils.find(coordBoundary, ".//upper").getLength() > 0) {
                            int boundId = pi.getCoordinates().indexOf(CodeGeneratorUtils.contToDisc(pi.getProblem(), axis)) * 2 + 2;
                            indent = indent + IND;
                            xslParams[1][1] = String.valueOf(indent.length());
                            String instructions = "";
                            String extrapolations = "";
                            NodeList upper = CodeGeneratorUtils.find(coordBoundary, ".//upper");
                            for (int k = 0; k < upper.getLength(); k++) {
                                NodeList children = upper.item(k).getChildNodes();
                                for (int n = 0; n < children.getLength(); n++) {
                                    //Boundary
                                    if (!children.item(n).getLocalName().equals("fieldExtrapolation")) {
                                        instructions = instructions + CactusUtils.xmlTransform(children.item(n), xslParams) + NL;
                                    }
                                    //Boundary extrapolation
                                    else {
                                        String field = ((Element) children.item(n)).getAttribute("field");
                                        String fieldTransf = CactusUtils.variableNormalize(field);
                                        extrapolations = extrapolations + "!" + indent + "Segment field extrapolations" + NL
                                            + indent + "if (" + CactusUtils.hardRegionCondition(index, pi.getCoordinates(), fieldTransf) 
                                            + ") then" + NL;
                                        indent = indent + IND;
                                        if (CodeGeneratorUtils.isAuxiliaryField(pi, field)) {
                                            String auxSegmentName = ((Element) children.item(n).getFirstChild()).getAttribute("segment");
                                            extrapolations = extrapolations + indent + "if (" 
                                                    + createExtrapolSegmentConditionBound(pi, auxSegmentName, fieldTransf) + ") then" + NL;
                                            indent = indent + IND;
                                            NodeList upperInstructions = children.item(n).getFirstChild().getChildNodes();
                                            xslParams[1][1] =  String.valueOf(indent.length());
                                            for (int m = 0; m < upperInstructions.getLength(); m++) {
                                                extrapolations = extrapolations + CactusUtils.xmlTransform(upperInstructions.item(m), xslParams) + NL;
                                            }
                                            indent = indent.substring(1);
                                            extrapolations = extrapolations + indent + "end if" +  NL;
                                        }
                                        else {
                                            NodeList upperInstructions = children.item(n).getChildNodes();
                                            xslParams[1][1] =  String.valueOf(indent.length());
                                            for (int m = 0; m < upperInstructions.getLength(); m++) {
                                                extrapolations = extrapolations + CactusUtils.xmlTransform(upperInstructions.item(m), xslParams) + NL;
                                            }
                                        }
                                        indent = indent.substring(1);
                                        extrapolations = extrapolations + indent + "end if" +  NL;
                                    }
                                }
                            }
                            indent = indent.substring(0, indent.lastIndexOf(IND));
                            if (!(extrapolations.equals("") && instructions.equals(""))) {
                                code = code + indent + "if ((FOV_" + CodeGeneratorUtils.getRegionName(pi.getProblem(), -boundId, pi.getCoordinates()) 
                                        + "(" + index + ") .gt. 0) .and. (" 
                                        + CactusUtils.getRightCondition(boundPTerms, Integer.parseInt(stencil), segBaseNumber, i, coordinates, 
                                                pi.getRegionIds()) 
                                        + ")) then" + NL;
                                code = code + extrapolations + instructions;
                                code = code + indent + "end if" + NL;
                            }
                        }
                    }
                }
            }
            //Extrapolation
            if (!pi.getHardRegions().isEmpty() && CodeGeneratorUtils.find(boundary, "./fieldExtrapolation").getLength() > 0) {
                if (segmentType.equals("interior")) {
                    code = code + indent + "if (FOV_" + segBaseNumber + "(" + index + ") .gt. 0) then" + NL;
                }
                else {
                    code = code + indent + "if (FOV_" + (segBaseNumber + 1) + "(" + index + ") .gt. 0) then" + NL;
                }
                indent = indent + IND;
                String coordIndex = "";
                for (int k = coordinates.size() - 1; k >= 0; k--) {
                    coordIndex =  coordinates.get(k) + ", " + coordIndex;
                }
                coordIndex = coordIndex.substring(0, coordIndex.lastIndexOf(","));
                if (pi.getHardRegionFields(segmentName) != null) {
                    ArrayList<String> fields = pi.getHardRegionFields(segmentName);
                    for (int l = 0; l < fields.size(); l++) {
                        String fieldTransformed = CactusUtils.variableNormalize(fields.get(l));
                        if (CodeGeneratorUtils.isAuxiliaryField(pi, fields.get(l))) {
                            if (!fluxes) {
                                code = code + indent + "if (" 
                                    + CactusUtils.hardRegionCondition(coordIndex, coordinates, fieldTransformed) + ") then" + NL;
                                indent = indent + IND;
                                NodeList segments = CodeGeneratorUtils.find(boundary, ".//fieldExtrapolation[@field ='" 
                                        + fields.get(l) + "']").item(0).getChildNodes();
                                for (int m = 0; m < segments.getLength(); m++) {
                                    code = code + indent + "if (" 
                                            + createExtrapolSegmentCondition(pi, segmentName, fields.get(l)) + ") then" + NL;
                                    indent = indent + IND;
                                    NodeList instructions = segments.item(m).getChildNodes();
                                    xslParams[1][1] =  String.valueOf(indent.length());
                                    for (int n = 0; n < instructions.getLength(); n++) {
                                        code = code + CactusUtils.xmlTransform(instructions.item(n), xslParams) + NL;
                                    }
                                    indent = indent.substring(1);
                                    code = code + indent + "end if" +  NL;
                                }
                                indent = indent.substring(1);
                                code = code + indent + "end if" +  NL;
                            }
                        }
                        else {
                            code = code + indent + "if (" 
                                + CactusUtils.hardRegionCondition(coordIndex, coordinates, fieldTransformed) + ") then" + NL;
                            indent = indent + IND;
                            NodeList instructions = CodeGeneratorUtils.find(boundary, ".//fieldExtrapolation[@field ='" 
                                    + fields.get(l) + "']").item(0).getChildNodes();
                            xslParams[1][1] =  String.valueOf(indent.length());
                            for (int m = 0; m < instructions.getLength(); m++) {
                                code = code + CactusUtils.xmlTransform(instructions.item(m), xslParams) + NL;
                            }
                            indent = indent.substring(1);
                            code = code + indent + "end if" +  NL;
                        }
                    }
                }
                //Close segment
                indent = indent.substring(0, indent.lastIndexOf(IND));
                code = code + indent + "end if" + NL;
            }
            return code;
        } 
        catch (Exception e) {
            e.printStackTrace();
            throw new CGException(CGException.CG00X, e.getMessage());
        }
    }
    
    /**
     * Create the condition to check if the point has to be calculated 
     * depending on the segment relations.
     * @param relations         The segment relations
     * @param segments          The segments
     * @param coordinates       The coordinates
     * @param stencil           The stencil of the problem
     * @param segmentId         The segment id of the initial segment
     * @param parabolicTerms    If parabolic terms are used
     * @return                  The condition
     */
    private static String getCondition(LinkedHashMap<String, String> relations, ArrayList<String> segments, 
            ArrayList<String> coordinates, int stencil, int segmentId, boolean parabolicTerms) {
        String condition = "";
        
        String index = "";
        String positiveCompatible = "";
        String negativeCompatible = "";
        for (int i = 0; i < coordinates.size(); i++) {
            for (int j = 0; j < stencil; j++) {
                String positiveStencilIndex = "";
                String negativeStencilIndex = "";
                String positiveIndexCheck = "";
                String negativeIndexCheck = "";
                for (int k = 0; k < coordinates.size(); k++) {
                    if (i == k) {
                        positiveStencilIndex = 
                            positiveStencilIndex + coordinates.get(k) + " + " + (j + 1) + ", ";
                        negativeStencilIndex = 
                            negativeStencilIndex + coordinates.get(k) + " - " + (j + 1) + ", ";
                        positiveIndexCheck = coordinates.get(k) + " + " + (j + 1) + " .le. cctk_lsh(" + (k + 1) + ")";
                        negativeIndexCheck = coordinates.get(k) + " - " + (j + 1) + " .ge. 1";
                    }
                    else {
                        positiveStencilIndex = positiveStencilIndex + coordinates.get(k) + ", ";
                        negativeStencilIndex = negativeStencilIndex + coordinates.get(k) + ", ";
                    }
                }
                positiveStencilIndex = positiveStencilIndex.substring(0, positiveStencilIndex.lastIndexOf(","));
                negativeStencilIndex = negativeStencilIndex.substring(0, negativeStencilIndex.lastIndexOf(","));
                
                positiveCompatible = positiveCompatible + "(" + positiveIndexCheck + " .and. FOV_" + segmentId + "(" 
                        + positiveStencilIndex + ") .gt. 0) .or. ";
                negativeCompatible = negativeCompatible + "(" + negativeIndexCheck + " .and. FOV_" + segmentId + "(" 
                        + negativeStencilIndex + ") .gt. 0) .or. ";
            }
            index = index + coordinates.get(i) + ", ";
        }
        if (stencil > 0) {
            positiveCompatible = positiveCompatible.substring(0, positiveCompatible.lastIndexOf(" .or."));
            negativeCompatible = negativeCompatible.substring(0, negativeCompatible.lastIndexOf(" .or."));
            index = index.substring(0, index.lastIndexOf(","));
            
            condition = condition + " .or. ((" + positiveCompatible + ") .or. (" + negativeCompatible + ")" 
                    + getDiagonal(parabolicTerms, coordinates, stencil, segmentId, segments) + ")";
        }
        
        
        return condition;
    }
    
    /**
     * Generate the condition to check diagonals of a point in the mesh. Only if parabolic terms fluxes are used.
     * @param parabolicTerms    If parabolic terms are used
     * @param segments          The segments
     * @param coordinates       The coordinates
     * @param stencil           The stencil of the problem
     * @param segmentId         The segment id of the original condition
     * @return                  The condition
     */     
    public static String getDiagonal(boolean parabolicTerms, ArrayList<String> coordinates, int stencil, int segmentId, 
            ArrayList<String> segments) {
        final int four = 4;
        final int three = 3;
        if (parabolicTerms) {
            String diagonal = "";
            for (int i = 0; i < coordinates.size(); i++) {
                for (int j = 0; j < stencil; j++) {
                    for (int k = i + 1; k < coordinates.size(); k++) {
                        for (int l = 0; l < stencil; l++) {
                            for (int s2 = 0; s2 < four; s2++) {
                                String stencilIndex = "";
                                String checkIndex = "";
                                for (int m = 0; m < coordinates.size(); m++) {
                                    if (i == m) {
                                        if (s2 == 0 || s2 == 1) {
                                            stencilIndex = stencilIndex + "int(" + coordinates.get(m) + " + " + (j + 1) + "), ";
                                            checkIndex = checkIndex + coordinates.get(m) + " + " + (j + 1) + " .le. cctk_lsh(" + (m + 1) + ") .and. ";
                                        }
                                        else {
                                            stencilIndex = stencilIndex + "int(" + coordinates.get(m) + " - " + (j + 1) + "), ";
                                            checkIndex = checkIndex + coordinates.get(m) + " - " + (j + 1) + " .ge. 1 .and. ";
                                        }
                                    }
                                    else {
                                        if (m == k) {
                                            if (s2 == 1 || s2 == three) {
                                                stencilIndex = stencilIndex + "int(" + coordinates.get(m) + " + " + (l + 1) + "), ";
                                                checkIndex = checkIndex + coordinates.get(m) + " + " + (l + 1) + " .le. cctk_lsh(" + (m + 1) 
                                                    + ") .and. ";
                                            }
                                            else {
                                                stencilIndex = 
                                                    stencilIndex + "int(" + coordinates.get(m) + " - " + (l + 1) + "), ";
                                                checkIndex = checkIndex + coordinates.get(m) + " - " + (l + 1) + " .ge. 1 .and. ";
                                            }
                                        }
                                        else {
                                            stencilIndex = stencilIndex + coordinates.get(m) + ", ";
                                        }
                                    }
                                }
                                stencilIndex = stencilIndex.substring(0, stencilIndex.lastIndexOf(","));
                                checkIndex = checkIndex.substring(0, checkIndex.lastIndexOf(" .and. "));
                                
                                diagonal = diagonal + "((" + checkIndex + ") .and. FOV_" + segmentId + "(" + stencilIndex + ") .gt. 0) .or. ";
                            }
                        }
                    }
                }
            }
            return " .or. (" + diagonal.substring(0, diagonal.lastIndexOf(" .or. ")) + ")";
        }
		return "";
    }
    
    /**
     * Generate the condition to check diagonals of a point in the mesh. Only if parabolic terms fluxes are used.
     * @param parabolicTerms    If parabolic terms are used
     * @param coordinates       The coordinates
     * @param stencil           The stencil of the problem
     * @param segId             The segment id
     * @return                  The condition
     */     
    public static String getBoundDiagonal(boolean parabolicTerms, ArrayList<String> coordinates, int stencil, int segId) {
        final int four = 4;
        final int three = 3;
        if (parabolicTerms) {
            String diagonal = "";
            for (int i = 0; i < coordinates.size(); i++) {
                for (int j = 0; j < stencil; j++) {
                    for (int k = i + 1; k < coordinates.size(); k++) {
                        for (int l = 0; l < stencil; l++) {
                            for (int s2 = 0; s2 < four; s2++) {
                                String stencilIndex = "";
                                String checkIndex = "";
                                for (int m = 0; m < coordinates.size(); m++) {
                                    if (i == m) {
                                        if (s2 == 0 || s2 == 1) {
                                            stencilIndex = stencilIndex + "int(" + coordinates.get(m) + " + " + (j + 1) + "), ";
                                            checkIndex = checkIndex + coordinates.get(m) + " + " + (j + 1) + " .le. cctk_lsh(" + (m + 1) + ") .and. ";
                                        }
                                        else {
                                            stencilIndex = stencilIndex + "int(" + coordinates.get(m) + " - " + (j + 1) + "), ";
                                            checkIndex = checkIndex + coordinates.get(m) + " - " + (j + 1) + " .ge. 1 .and. ";
                                        }
                                    }
                                    else {
                                        if (m == k) {
                                            if (s2 == 1 || s2 == three) {
                                                stencilIndex = stencilIndex + "int(" + coordinates.get(m) + " + " + (l + 1) + "), ";
                                                checkIndex = checkIndex + coordinates.get(m) + " + " + (l + 1) + " .le. cctk_lsh(" + (m + 1) 
                                                    + ") .and. ";
                                            }
                                            else {
                                                stencilIndex = 
                                                    stencilIndex + "int(" + coordinates.get(m) + " - " + (l + 1) + "), ";
                                                checkIndex = checkIndex + coordinates.get(m) + " - " + (l + 1) + " .ge. 1 .and. ";
                                            }
                                        }
                                        else {
                                            stencilIndex = stencilIndex + coordinates.get(m) + ", ";
                                        }
                                    }
                                }
                                stencilIndex = stencilIndex.substring(0, stencilIndex.lastIndexOf(","));
                                checkIndex = checkIndex.substring(0, checkIndex.lastIndexOf(" .and. "));
                                
                                diagonal = diagonal + "(" + checkIndex + " .and. FOV_" + segId + "(" + stencilIndex + ") .gt. 0) .or. ";
                            }
                        }
                    }
                }
            }
            return " .or. (" + diagonal.substring(0, diagonal.lastIndexOf(" .or. ")) + ")";
        }
		return "";
    }
    
    
    /**
     * Create the condition to check if the point has to be calculated 
     * depending on the stencil and the array limits.
     * @param coordinates   The coordinates
     * @param stencil       The stencil of the problem
     * @return              The condition
     */
    private static String getLimitCondition(ArrayList<String> coordinates, int stencil) {
        String condition = "(";
        
        for (int i = 0; i < coordinates.size(); i++) {
            condition = condition + coordinates.get(i) + " + " + stencil + " .le. cctk_lsh(" + (i + 1) + ") .and. " 
                + coordinates.get(i) + " - " + stencil + " .ge. 1 .and. ";
        }
        condition = condition.substring(0, condition.lastIndexOf(" .and. "));
              
        return condition + ")";
    }
    
    /**
     * Create the condition for the extrapolation that depends on a segment.
     * @param pi                The problem information
     * @param segmentName       The segment name
     * @param field             The field being extrapolated
     * @return                  The condition
     */
    private static String createExtrapolSegmentConditionBound(ProblemInfo pi, String segmentName, String field) {
        ArrayList<String> segments = pi.getRegionIds();
        String index = "";
        for (int i = 0; i < pi.getCoordinates().size(); i++) {
            index = index + pi.getCoordinates().get(i) + ", ";
        }
        index = index.substring(0, index.lastIndexOf(","));
        String distIndex = "";
        for (int i = 0; i < pi.getCoordinates().size(); i++) {
            String coord = pi.getCoordinates().get(i);
            distIndex = distIndex + "int(" + coord + " - d_" + coord + "_" + field + "(" + index + ")), ";
        }
        distIndex = distIndex.substring(0, distIndex.lastIndexOf(","));
        int segmentIndex = pi.getRegions().indexOf(segmentName);
        String condition = "";
        
        if (segments.contains(String.valueOf(((segmentIndex * 2) + 1)))) {
            condition = condition + "FOV_" + ((segmentIndex * 2) + 1) + "(" + distIndex + ") .gt. 0";
        }
        if (segments.contains(String.valueOf(((segmentIndex * 2) + 1))) && segments.contains(String.valueOf(((segmentIndex * 2) + 2)))) {
            condition = condition + " .or. ";
        }
        if (segments.contains(String.valueOf(((segmentIndex * 2) + 2)))) {
            condition = condition + "FOV_" + ((segmentIndex * 2) + 2) + "(" + distIndex + ") .gt. 0";
        }
        
        return condition;
    }
    
    /**
     * Create the condition for the extrapolation that depends on a segment.
     * @param pi                The problem information
     * @param segmentName       The segment name
     * @param field             The field being extrapolated
     * @return                  The condition
     * @throws CGException      CG003 Not possible to create code for the platform
     *                          CG00X External error
     */
    private static String createExtrapolSegmentCondition(ProblemInfo pi, String segmentName, String field) throws CGException {
        String fieldTransf = CactusUtils.variableNormalize(field);
        String index = "";
        for (int i = 0; i < pi.getCoordinates().size(); i++) {
            index = index + pi.getCoordinates().get(i) + ", ";
        }
        index = index.substring(0, index.lastIndexOf(","));
        String distIndex = "";
        for (int i = 0; i < pi.getCoordinates().size(); i++) {
            String coord = pi.getCoordinates().get(i);
            distIndex = distIndex + "int(" + coord + " - d_" + coord + "_" + fieldTransf + "(" + index + ")), ";
        }
        distIndex = distIndex.substring(0, distIndex.lastIndexOf(","));
        String condition = "";
        
        ArrayList<String> interactions = CodeGeneratorUtils.getRegionInteractions(segmentName , pi.getHardRegions(), 
                pi.getRegions());
        for (int k = 0; k < interactions.size(); k++) {
            ArrayList<String> hardFields = CodeGeneratorUtils.filterHardFields(segmentName, interactions.get(k), 
                    pi.getHardRegions());
            String segName = interactions.get(k);
            if (hardFields.contains(field) 
                    && pi.getRegionIds().contains(String.valueOf(2 * pi.getRegions().indexOf(segName) + 1))) {
                int segId = 2 * pi.getRegions().indexOf(segName) + 1;
                condition = condition + "FOV_" + String.valueOf(segId) + "(" + distIndex + ") + ";
            }
            if (hardFields.contains(field) 
                    && pi.getRegionIds().contains(String.valueOf(2 * pi.getRegions().indexOf(segName) + 2))) {
                int segId = 2 * pi.getRegions().indexOf(segName) + 2;
                condition = condition + "FOV_" + String.valueOf(segId) + "(" + distIndex + ") + ";
            }
        }
        condition = condition.substring(0, condition.lastIndexOf(" +")) + " .gt. 0";
        
        return condition;
    }
    
    /**
     * Create the initialization for the extrapolation block.
     * @param pi                The problem information
     * @param blockNumber       The number of the block
     * @param indent            The actual indent
     * @return                  The code
     * @throws CGException      CG00X External error
     */
    private static String extrapolationInitialization(ProblemInfo pi, int blockNumber, String indent) throws CGException {
        String result = "";
        Document doc = pi.getProblem();
        final int numParam = 4;
        String index = "";
        for (int i = 0; i < pi.getDimensions(); i++) {
            index = index + pi.getCoordinates().get(i) + ", ";
        }
        index = index.substring(0, index.length() - 2);
        ArrayList<String> variables = new ArrayList<String>();
        NodeList blockList = CodeGeneratorUtils.find(doc, "//mms:interiorExecutionFlow/mms:instructionSet/*[" 
                + (blockNumber + 1) + "]//fieldExtrapolation|//mms:surfaceExecutionFlow/mms:instructionSet/*[" 
                + (blockNumber + 1) + "]//fieldExtrapolation");
        for (int i = 0; i < blockList.getLength(); i++) {
            Element variable = (Element) blockList.item(i);
            String field = variable.getAttribute("field");
            //Fields
            NodeList functions = CodeGeneratorUtils.find(variable, ".//mt:math/mt:apply[@type = 'function']");
            for (int j = 0; j < functions.getLength(); j++) {
                Node function = functions.item(j);
                Node origin = function.getChildNodes().item(pi.getDimensions() * numParam + 2);
                for (int k = 0; k < pi.getDimensions(); k++) {
                    String variableName = CactusUtils.xmlTransform(function.getChildNodes().item(k * numParam + FOUR), null);
                    if (!variables.contains(variableName)) {
                        variables.add(variableName);
                        result = result + indent + variableName + "(" + index + ") = " 
                                + CactusUtils.xmlTransform(origin, null) + "(" + index + ")" + NL;
                    }
                }
            }
            
            if (variable.getFirstChild().getNodeName().equals("auxiliaryFieldExtrapolation")) {
                variable = (Element) variable.getFirstChild();
            }
            NodeList leftTerms = CodeGeneratorUtils.find(variable, ".//mt:math[(not(parent::if) or parent::then) and (not(parent::while) or "
                    + "parent::loop) and not(parent::return) and not(ancestor::mms:condition) and not(ancestor::" 
                    + "mms:finalizationCondition)]/mt:apply/mt:eq[not(following-sibling::*[1][name()='mt:apply'] = "
                    + "preceding-sibling::mt:math/mt:apply/mt:eq/following-sibling::*[1][name()='mt:apply'])]/following-sibling::*[1]" 
                    + "[name()='mt:apply' and (descendant::mt:ci = 'extrapolated') and (descendant::mt:ci = '" + field 
                    + "' or descendant::mt:ci[ends-with(text(), '" + field + "')])]");
            for (int j = 0; j < leftTerms.getLength(); j++) {
                String variableName = new String(CactusUtils.xmlTransform(leftTerms.item(j).getFirstChild(), null));
                if (!variables.contains(variableName)) {
                    variables.add(variableName);
                    NodeList originalField = CodeGeneratorUtils.find(variable, ".//mt:math[(not(parent::if) or parent::then) "
                            + "and (not(parent::while) or "
                            + "parent::loop) and not(parent::return) and not(ancestor::mms:condition) and not(ancestor::" 
                            + "mms:finalizationCondition)]/mt:apply/mt:eq[not(following-sibling::*[1][name()='mt:apply'] = "
                            + "preceding-sibling::mt:math/mt:apply/mt:eq/following-sibling::*[1][name()='mt:apply'])]/following-sibling::*[1]" 
                            + "[name()='mt:apply' and not(descendant::mt:ci = 'extrapolated')  and (descendant::mt:ci = '" + field 
                            + "' or descendant::mt:ci[ends-with(text(), '" + field + "')])]");
                    result = result + indent + variableName + "(" + index + ") = " 
                            + CactusUtils.xmlTransform(originalField.item(0).getFirstChild(), null) + "(" + index + ")" + NL;
                }    
            }
        }
        return result;
    }
    
    /**
     * Create the finalization instructions after an extrapolation & boundaries. After a boundary calculation the extrapolation axis variables
     * must be reassigned for the boundaries that does not calculate them. 
     * @param pi                    The problem info.
     * @param indent                The actual indent  
     * @param boundary              The boundary xml code
     * @param segmentBaseNumber     The segment base number
     * @return                      The code
     * @throws CGException          CG00X External error
     */
    private static String extrapolationFinalization(ProblemInfo pi, String indent, Node boundary, int segmentBaseNumber) throws CGException {
        String result = "";
        String index = "";
        for (int i = 0; i < pi.getDimensions(); i++) {
            index = index + pi.getCoordinates().get(i) + ", ";
        }
        index = index.substring(0, index.length() - 2);
        ArrayList<String> fields = new ArrayList<String>();
        NodeList fieldList = CodeGeneratorUtils.find(pi.getProblem(), "/*/mms:field|/*/mms:auxiliarField");
        for (int i = 0; i < fieldList.getLength(); i++) {
            fields.add(fieldList.item(i).getTextContent());
        }
       
        for (int i = 0; i < pi.getDimensions(); i++) {
            String coordString = pi.getCoordinates().get(i);
            if (pi.getPeriodicalBoundary().get(coordString).equals("noperiodical")) {
                ArrayList<String> candidates = new ArrayList<String>();
                candidates.addAll(fields);
                NodeList extrapolatedFields = CodeGeneratorUtils.find(boundary, ".//" + coordString + "/lower//fieldExtrapolation/@field");
                for (int j = 0; j < extrapolatedFields.getLength(); j++) {
                    candidates.remove(extrapolatedFields.item(j).getTextContent());
                }
                if (!candidates.isEmpty()) {
                    String axis = CodeGeneratorUtils.discToCont(pi.getProblem(), coordString);
                    int boundId = pi.getCoordinates().indexOf(CodeGeneratorUtils.contToDisc(pi.getProblem(), axis)) * 2 + 1;
                    result = result + indent + "if ((FOV_" + CodeGeneratorUtils.getRegionName(pi.getProblem(), -boundId, 
                            pi.getCoordinates()) + "(" + index + ") .gt. 0) .and. (" + CactusUtils.getLeftCondition(false, 
                                    pi.getSchemaStencil(), segmentBaseNumber, i, pi.getCoordinates(), pi.getRegionIds()) + ")) then" + NL;
                    for (int k = 0; k < candidates.size(); k++) {
                        String field = candidates.get(k);
                        Element originalField = CodeGeneratorUtils.getOriginalField(boundary, fields, field, pi.getExtrapolationType(), pi.getDimensions());
                        for (int l = 0; l < pi.getDimensions(); l++) {
                            String coord = pi.getCoordinates().get(l);
                            result = result + indent + IND + "extrapolatedsub" + CactusUtils.variableNormalize(field) + "sup" 
                                    + coord + "(" + index + ") = " + CactusUtils.xmlTransform(originalField, null) 
                                    + "(" + index + ")" + NL;
                        }
                    }
                    result = result + indent + "end if" + NL;
                }
                
                
                candidates = new ArrayList<String>();
                candidates.addAll(fields);
                extrapolatedFields = CodeGeneratorUtils.find(boundary, ".//" + coordString + "/upper//fieldExtrapolation/@field");
                for (int j = 0; j < extrapolatedFields.getLength(); j++) {
                    candidates.remove(extrapolatedFields.item(j).getTextContent());
                }
                if (!candidates.isEmpty()) {
                    String axis = CodeGeneratorUtils.discToCont(pi.getProblem(), coordString);
                    int boundId = pi.getCoordinates().indexOf(CodeGeneratorUtils.contToDisc(pi.getProblem(), axis)) * 2 + 2;
                    result = result + indent + "if ((FOV_" + CodeGeneratorUtils.getRegionName(pi.getProblem(), -boundId, 
                            pi.getCoordinates()) + "(" + index + ") .gt. 0) .and. (" + CactusUtils.getRightCondition(false, 
                                    pi.getSchemaStencil(), segmentBaseNumber, i, pi.getCoordinates(), pi.getRegionIds()) + ")) then" + NL;
                    for (int k = 0; k < candidates.size(); k++) {
                        String field = candidates.get(k);
                        Element originalField = CodeGeneratorUtils.getOriginalField(boundary, fields, field, pi.getExtrapolationType(), pi.getDimensions());
                        for (int l = 0; l < pi.getDimensions(); l++) {
                            String coord = pi.getCoordinates().get(l);
                            result = result + indent + IND + "extrapolatedsub" + CactusUtils.variableNormalize(field) + "sup" 
                                    + coord + "(" + index + ") = " + CactusUtils.xmlTransform(originalField, null) 
                                    + "(" + index + ")" + NL;
                        }
                    }
                    result = result + indent + "end if" + NL;
                }
            }
        }
        return result;
    }
}
