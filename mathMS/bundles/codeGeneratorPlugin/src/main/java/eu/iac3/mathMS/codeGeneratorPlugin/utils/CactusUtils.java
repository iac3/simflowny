/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.codeGeneratorPlugin.utils;

import eu.iac3.mathMS.codeGeneratorPlugin.CGException;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Utility class for Cactus code generation.
 * @author bminyano
 *
 */
public final class CactusUtils {
    static final String XSL = "common" + File.separator + "XSLTmathml" + File.separator + "instructionToFortran.xsl";
    static String mtUri = "http://www.w3.org/1998/Math/MathML";
    static final String NEWLINE = System.getProperty("line.separator"); 
    static final int THREE = 3;
    static final String INDENT = "\u0009";
    static Transformer transformer = null;
    static TransformerFactory tFactory = null;
    
    //XSL initialization
    static {
        try {
            tFactory = new net.sf.saxon.TransformerFactoryImpl();
            transformer = tFactory.newTransformer(new StreamSource(XSL));
        } 
        catch (TransformerConfigurationException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Private constructor.
     */
    private CactusUtils() { };
    
    /**
     * Parses a xml with a xlst.
     * @param xml                           The xml to parse.
     * @param params                        Params to send to the xsl.
     * @return                              the result to apply the xlt to the xml.
     * @throws CGException                  CG004 External error
     */
    public static String xmlTransform(String xml, String[][] params) throws CGException {  
        try {
            ByteArrayInputStream input = new ByteArrayInputStream(xml.getBytes("UTF-8"));
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            
            // Set the parameters
            if (params != null) {
                for (int i = 0; i < params.length; i++) {
                    transformer.setParameter(params[i][0], params[i][1]);
                }   
            }
            transformer.transform(new StreamSource(input), new StreamResult(output));
            return output.toString("UTF-8");
        } 
        catch (Exception e) {
            throw new CGException(CGException.CG00X, e.getMessage());
        }
    }
    
    /**
     * Parses a xml with a xlst.
     * @param xml                           The xml to parse.
     * @param params                        Params to send to the xsl.
     * @return                              the result to apply the xlt to the xml.
     * @throws CGException                  CG004 External error
     */
    public static String xmlTransform(Node xml, String[][] params) throws CGException {  
        try {
            ByteArrayOutputStream output = new ByteArrayOutputStream();

            // Set the parameters
            if (params != null) {
                for (int i = 0; i < params.length; i++) {
                    transformer.setParameter(params[i][0], params[i][1]);
                }   
            }
            DOMSource domSource = new DOMSource(xml);
            transformer.transform(domSource, new StreamResult(output));
            return output.toString("UTF-8");
        } 
        catch (Exception e) {
            throw new CGException(CGException.CG00X, e.getMessage());
        }
    }
    
    /**
     * Normalize the name of a variable with Fortran rules and according entities.xsl.
     * @param variable          The original name of the variable
     * @return                  The variable normalized.
     * @throws CGException      CG003 Not possible to create code for the platform
     *                          CG004 External error
     */
    public static String variableNormalize(String variable) throws CGException {
        final int fortranVariableLimit = 27;
        try {
            //Replace the greek characters. Process with xslt
            variable = "<mt:math xmlns:mt=\"http://www.w3.org/1998/Math/MathML\"><mt:ci>" + variable + "</mt:ci></mt:math>";
            variable = xmlTransform(variable, null);
            
            //The first character must be a letter
            while (CodeGeneratorUtils.isNumber(variable.substring(0, 1))) {
                variable = variable.substring(1);
            }
            //The remaining characters, if any, may be letters, digits, or underscores
            variable = variable.replaceAll("[^a-zA-Z0-9_]", "");
            //It has no more than 27 characters
            if (variable.length() > fortranVariableLimit) {
                variable = variable.substring(0, fortranVariableLimit - 1);
            }
            
            return variable;
        }
        catch (IndexOutOfBoundsException e) {
            String msg = "The variable name " + variable + " is not valid. See Fortran documentation. Should have at least one letter";
            throw new CGException(CGException.CG003, msg);
        } 
        catch (Exception e) {
            throw new CGException(CGException.CG00X, e.getMessage());
        }
    }
    
    /**
     * Find in the document the auxiliary variables that have to be registered in the system. 
     * @param doc               The problem document
     * @param identifiers       The fields, auxiliary fields, constraints, vectors and parameters
     * @param exFuncVars        The variables used in extrapolation calls
     * @return                  All the variables in an array list
     * @throws CGException      CG003 Not possible to create code for the platform
     *                          CG004 External error
     */
    public static ArrayList<String> getVariables(Document doc, HashSet<String> identifiers, ArrayList<String> exFuncVars) throws CGException {
        ArrayList<String> candidates = new ArrayList<String>();
        //Take all the variables that are assigned inside the code. They will need to be auxiliar.
        NodeList leftTerms = CodeGeneratorUtils.find(doc, ".//mt:math[(not(parent::if) or parent::then) and (not(parent::while) or parent::loop)" 
                + " and not(parent::return) and not(ancestor::mms:condition) and not(ancestor::" 
                + "mms:finalizationCondition) and not(ancestor::mms:function)]/mt:apply/mt:eq[not(following-sibling::*[1]" 
                + "[name()='mt:apply'] = preceding-sibling::mt:math/mt:apply/mt:eq/following-sibling::*[1][name()='mt:apply' or "
                + "name()='sml:sharedVariables'])]/following-sibling::*[1][name()='mt:apply' or name()='sml:sharedVariables']");
        for (int i = 0; i < leftTerms.getLength(); i++) {
            String variable = 
                new String(xmlTransform(leftTerms.item(i).getFirstChild(), null));
            if (!identifiers.contains(variable) && !candidates.contains(variable)) {
                candidates.add(variable);
            }   
        }
        //Fill with extrapolation variables used in function calls.
        for (int i = 0; i < exFuncVars.size(); i++) {
            String variable = exFuncVars.get(i);
            if (!candidates.contains(variable) && !identifiers.contains(variable)) {
                candidates.add(variable);
            }
        }
        
        return candidates;
    }

    /**
     * Create the initialization for the extrapolation block.
     * @param pi                The problem information
     * @param problem           The problem
     * @return                  The code
     * @throws CGException      CG00X External error
     */
    public static ArrayList<String> extrapolationFunctionVariables(Document problem, ProblemInfo pi) throws CGException {
        final int numParam = 4;
        ArrayList<String> result = new ArrayList<String>();
        NodeList extrapolations = CodeGeneratorUtils.find(problem, "//fieldExtrapolation/mt:math/mt:apply[@type = 'function']");
        for (int j = 0; j < extrapolations.getLength(); j++) {
            NodeList parameters = CodeGeneratorUtils.find(extrapolations.item(j), "./*");
            for (int i = 0; i < pi.getDimensions(); i++) {
                String from = new String(CactusUtils.xmlTransform(parameters.item(i * numParam + 2), null));
                if (!result.contains(from)) {
                    result.add(from);
                }
                String to = new String(CactusUtils.xmlTransform(parameters.item(i * numParam + THREE), null));
                if (!result.contains(to)) {
                    result.add(to);
                }
            }
            if (numParam * pi.getDimensions() + 2 < parameters.getLength()) {
                String finalTo = new String(CactusUtils.xmlTransform(parameters.item(pi.getDimensions() * numParam + 1), null));
                if (!result.contains(finalTo)) {
                    result.add(finalTo);
                }
            } 
        }
        return result;
    }
    
    /**
     * Get the identifiers of the problem. (field and parameters)
     * @param doc               The problem document
     * @return                  The identifiers in a hash set
     * @throws CGException      CG003 Not possible to create code for the platform
     *                          CG004 External error
     */
    public static HashSet<String> getIdentifiers(Document doc) throws CGException {
        
        HashSet<String> result = new HashSet<String>();
        
        //Fields and parameters
        String query = "/*/mms:field|/*/mms:auxiliarField|/*/" 
            + "mms:parameter/mms:name|/*/mms:implicitParameter/mms:value";
        
        NodeList identifiers = CodeGeneratorUtils.find(doc, query);
        for (int i = 0; i < identifiers.getLength(); i++) {
            result.add(CactusUtils.variableNormalize(identifiers.item(i).getTextContent()));
        }
        //Vectors
        ArrayList<String> vectors = getVectors(doc);
        for (int i = 0; i < vectors.size(); i++) {
            result.add(vectors.get(i));
        }
        return result;
    }
    
    /**
     * Get the vectors used in the problem.
     * @param doc               The model
     * @return                  An array with the vectors used
     * @throws CGException      CG004 External error
     */
    public static ArrayList<String> getVectors(Document doc) throws CGException {
        ArrayList<String> result = new ArrayList<String>();
        NodeList constraints = CodeGeneratorUtils.find(doc, ".//mms:vectorName");
        for (int i = 0; i < constraints.getLength(); i++) {
            if (CodeGeneratorUtils.find(doc, ".//mt:ci[text() = '" + constraints.item(i).getTextContent() + "']").getLength() > 0) {
                result.add(constraints.item(i).getTextContent());
            }
        }
        return result;
    }
    
    /**
     * Gets a list of hard region variables used as auxiliary variables.
     * @param pi            The problem info
     * @return              The variables
     * @throws CGException  CG003 Not possible to create code for the platform
     *                      CG004 External error
     */
    public static String getHardRegionIntVars(ProblemInfo pi) throws CGException {
        String vars = "";
        HashMap<String, ArrayList<String>> hardRegions = pi.getHardRegions();
        ArrayList<String> coords = pi.getCoordinates();
        //Fill with distance variables for hard region boundaries
        Iterator<String> hardIt = hardRegions.keySet().iterator();
        ArrayList<String> added = new ArrayList<String>();
        while (hardIt.hasNext()) {
            String regionId = hardIt.next();
            ArrayList<String> hardFields = hardRegions.get(regionId);
            for (int i = 0; i < hardFields.size(); i++) {
                if (!added.contains(hardFields.get(i))) {
                    added.add(hardFields.get(i));
                    String field = CactusUtils.variableNormalize(hardFields.get(i));
                    for (int j = 0; j < coords.size(); j++) {
                        vars = vars + ", d_" + coords.get(j) + "_" + field;
                    }
                }
            }
        }
        return vars.substring(1);
    }
    
    /**
     * Get the variable group information for Cactus. It includes the following:
     * 1 - Group information for interface.ccl
     * 2 - Groups to be synchronized in every block
     * @param doc               The problem document
     * @param pi                The problem info
     * @return                  All the variables in an array list
     * @throws CGException      CG004 External error
     */
    public static CactusVarGroupInfo getVarGroupInfo(Document doc, ProblemInfo pi) 
        throws CGException {
        HashSet<String> identifiers = pi.getIdentifiers(); 
        ArrayList<String> variables = pi.getVariables();
        ArrayList<String> exFuncVars = pi.getExtrapolFuncVars();
        LinkedHashMap<String, Integer> fieldTimeLevels = pi.getFieldTimeLevels();
        Hashtable<String, CactusVarGroup> varGroups = new Hashtable<String, CactusVarGroup>();
        Hashtable<Integer, ArrayList<String>> groupSyncs = new Hashtable<Integer, ArrayList<String>>();
        
        //Filling a common variable to control which variables are used and which are not. 
        ArrayList<String> vars = new ArrayList<String>();
        for (int i = 0; i < variables.size();i++) {
            vars.add(variables.get(i));
        }
        ArrayList<String> vectors = CactusUtils.getVectors(doc);
        if (vectors.size() >  0) {
            for (int i = 0; i < vectors.size(); i++) {
                if (!vars.contains(CactusUtils.variableNormalize(vectors.get(i)))) {
                    vars.add(CactusUtils.variableNormalize(vectors.get(i)));
                }
            }
        }
        //1 - Group information for interface.ccl
        //Fields have to be apart
        NodeList fieldNodeList = CodeGeneratorUtils.find(doc, "/*/mms:field|/*/mms:auxiliarField");
        Hashtable<String, Integer> fields = new Hashtable<String, Integer>();
        for (int i = 0; i < fieldNodeList.getLength(); i++) {
            String field = ((Element) fieldNodeList.item(i)).getTextContent();
            Integer ftl = fieldTimeLevels.get(field);
            fields.put(variableNormalize(field), ftl);
            String groupName = "fields_tl" + ftl.toString();
            if (varGroups.containsKey(groupName)) {
                CactusVarGroup cvg = varGroups.get(groupName);
                cvg.appendVariable(field);
                varGroups.put(groupName, cvg);
            }
            else {
                CactusVarGroup cvg = new CactusVarGroup();
                cvg.setName(groupName);
                cvg.setFieldGroup(true);
                cvg.setTimeLevels(ftl);
                ArrayList<String> fieldList = new ArrayList<String>();
                fieldList.add(field);
                cvg.setVariables(fieldList);
                varGroups.put(groupName, cvg);
            }
        }
        //Iterate over all the blocks
        //Check if there are periodical boundaries
        int blocks = CodeGeneratorUtils.getBlocksNum(doc, "//mms:interiorExecutionFlow/sml:simml|//mms:surfaceExecutionFlow/sml:simml");
        for (int i = 0; i < blocks; i++) {
            //Create a blockScope with the instructions of the block to synchronize in cactus only for the variables assigned in this scope
            DocumentFragment blockScope = doc.createDocumentFragment();
            NodeList blockList = CodeGeneratorUtils.find(doc, "//mms:interiorExecutionFlow/" 
                    + "mms:instructionSet/*[position() = " + (i + 1) + " and @stencil > 0]|//mms:surfaceExecutionFlow/" 
                    + "mms:instructionSet/*[position() = " + (i + 1) + " and @stencil > 0]");
            for (int j = 0; j < blockList.getLength(); j++) {
                blockScope.appendChild(blockList.item(j).cloneNode(true));
            }
            
            //Get the variables to be synchronized
            ArrayList<String> syncvariables = new ArrayList<String>();
            //Get the variables assigned in the code
            String query = ".//mt:math[(not(parent::if) or parent::then) and (not(parent::while) or parent::loop) and not(parent::return) " 
                + "and not(ancestor::mms:condition) and not(ancestor::mms:finalizationCondition)]"
                + "/mt:apply/mt:eq[not(following-sibling::*[1] = preceding-sibling::mt:math/mt:apply/mt:eq/following-sibling::*[1])]" 
                + "/following-sibling::*[1]";
            NodeList leftTerms = CodeGeneratorUtils.find(blockScope, query);
            ArrayList<String> fieldGroupNames = new ArrayList<String>();
            for (int j = 0; j < leftTerms.getLength(); j++) {
                String variable = CactusUtils.xmlTransform(leftTerms.item(j), null);
                //Take only the identifier. The indexes will be ignored
                if (variable.indexOf("(") != -1) {
                    variable = variable.substring(0, variable.indexOf("("));
                    while (variable.endsWith("_p")) {
                        variable = variable.substring(0, variable.lastIndexOf("_p"));
                    }
                    if (!syncvariables.contains(variable) && (identifiers.contains(variable) || vectors.contains(variable) 
                            || variables.contains(variable))) {
                        if (fields.containsKey(variable)) {
                            Integer ftl = fields.get(variable);
                            if (!fieldGroupNames.contains("fields_tl" + ftl.toString())) {
                                fieldGroupNames.add("fields_tl" + ftl.toString());
                            }
                        }
                        else {
                            syncvariables.add(variable);
                            vars.remove(variable);
                        }
                    }
                }
            }
            //Get the variables used in extrapolation calls
            query = ".//fieldExtrapolation//mt:apply[@type = 'function' and (mt:ci = 'call extrapolate_field' "
                    + "or mt:ci = 'call extrapolate_flux') ]/*";
            NodeList arguments = CodeGeneratorUtils.find(blockScope, query);
            for (int j = 0; j < arguments.getLength(); j++) {
                String variable = CactusUtils.xmlTransform(arguments.item(j), null);
                if (!syncvariables.contains(variable) && !identifiers.contains(variable) 
                        && (variables.contains(variable) || exFuncVars.contains(variable))) {
                    syncvariables.add(variable);
                    vars.remove(variable);
                }
            }
            
            //2 - Groups to be synchronized in every block
            if (!syncvariables.isEmpty() || (fieldGroupNames.size() > 0)) {
                ArrayList<String> syncGroupNames = new ArrayList<String>();
                //Add temporal variables
                if (!syncvariables.isEmpty()) {
                    CactusVarGroup cvg = new CactusVarGroup();
                    cvg.setFieldGroup(false);
                    cvg.setName("block" + i);
                    cvg.setTimeLevels(1);
                    cvg.setVariables(syncvariables);
                    varGroups.put("block" + i, cvg);
                    syncGroupNames.add("block" + i);
                }
                //Add fields
                for (int j = 0; j < fieldGroupNames.size(); j++) {
                    syncGroupNames.add(fieldGroupNames.get(j));
                }
                groupSyncs.put(i, syncGroupNames);
            }
        }
        //Simplifications
        simplification(varGroups, groupSyncs);
        //The non-synchronized variables might be in one group
        CactusVarGroup cvg = new CactusVarGroup();
        cvg.setFieldGroup(false);
        cvg.setName("temporalGroup");
        cvg.setTimeLevels(1);
        cvg.setVariables(vars);
        varGroups.put("temporalGroup", cvg);
        
        return new CactusVarGroupInfo(varGroups, groupSyncs);
    }
    
    /**
     * Simplify the variable group information. The following simplifications are made:
     * a - Join groups that are similars. Same variables
     * b - Separate the repeated variables in individual groups
     * @param varGroups         The variable groups
     * @param groupSyncs        The group synchronizations
     */
    private static void simplification(Hashtable<String, CactusVarGroup> varGroups, Hashtable<Integer, ArrayList<String>> groupSyncs) {
        //a - same group name for group sharing same variables
        mergeGroups(varGroups, groupSyncs);
        //b - Divide groups taking count similarities until there is no overlap between groups
        boolean isOverlap = false;
        //Check if there is overlap, get the most similarity between groups
        ArrayList<String> groupNames = new ArrayList<String>();
        groupNames.addAll(varGroups.keySet());
        String nameA = "";
        String nameB = "";
        float similarity = 1;
        for (int i = 0; i < groupNames.size() - 1; i++) {
            CactusVarGroup cvgA = varGroups.get(groupNames.get(i));
            for (int j = i + 1; j < groupNames.size(); j++) {
                CactusVarGroup cvgB = varGroups.get(groupNames.get(j));
                if (cvgA.overlaps(cvgB)) {
                    isOverlap = true;
                    if (cvgA.getSimilarity(cvgB) < similarity) {
                        similarity = cvgA.getSimilarity(cvgB);
                        nameA = cvgA.getName();
                        nameB = cvgB.getName();
                    }
                }
            }
        }
        while (isOverlap) {
            //get the different variables and remove them from the groups
            CactusVarGroup cvgA = varGroups.get(nameA);
            CactusVarGroup cvgB = varGroups.get(nameB);
            ArrayList<String> varsA = new ArrayList<String>();
            varsA.addAll(cvgA.getVariables());
            ArrayList<String> varsB = new ArrayList<String>();
            varsB.addAll(cvgB.getVariables());
            //Get the variables that are different
            for (int i = varsA.size() - 1; i >= 0; i--) {
                if (varsB.contains(varsA.get(i))) {
                    varsB.remove(varsA.get(i));
                    varsA.remove(i);
                }
            }
            if (varsA.size() > 0) {
                CactusVarGroup newCvg = new CactusVarGroup();
                newCvg.setName(varsA.get(0) + "Group"); //Setting the name to a variable avoid repeat group names
                newCvg.setFieldGroup(false);
                newCvg.setTimeLevels(1);
                newCvg.setVariables(varsA);
                //Add the new group to the group list
                varGroups.put(varsA.get(0) + "Group", newCvg);
                //Change the actual group to remove the variables that has been moved
                ArrayList<String> newVars = cvgA.getVariables();
                newVars.removeAll(varsA);
                cvgA.setVariables(newVars);
                varGroups.put(nameA, cvgA);
                //Add the new group to the group synchronizations where actual group appears.
                Iterator<Integer> syncGroupIt = groupSyncs.keySet().iterator();
                while (syncGroupIt.hasNext()) {
                    Integer key = syncGroupIt.next();
                    ArrayList<String> groupSyncNames = groupSyncs.get(key);
                    if (groupSyncNames.contains(nameA)) {
                        groupSyncNames.add(varsA.get(0) + "Group");
                        groupSyncs.put(key, groupSyncNames);
                    }
                }
            }
            if (varsB.size() > 0) {
                CactusVarGroup newCvg = new CactusVarGroup();
                newCvg.setName(varsB.get(0) + "Group"); //Setting the name to a variable avoid repeat group names
                newCvg.setFieldGroup(false);
                newCvg.setTimeLevels(1);
                newCvg.setVariables(varsB);
                //Add the new group to the group list
                varGroups.put(varsB.get(0) + "Group", newCvg);
                //Change the actual group to remove the variables that has been moved
                ArrayList<String> newVars = cvgB.getVariables();
                newVars.removeAll(varsB);
                cvgB.setVariables(newVars);
                varGroups.put(nameB, cvgB);
                //Add the new group to the group synchronizations where actual group appears.
                Iterator<Integer> syncGroupIt = groupSyncs.keySet().iterator();
                while (syncGroupIt.hasNext()) {
                    Integer key = syncGroupIt.next();
                    ArrayList<String> groupSyncNames = groupSyncs.get(key);
                    if (groupSyncNames.contains(nameB)) {
                        groupSyncNames.add(varsB.get(0) + "Group");
                        groupSyncs.put(key, groupSyncNames);
                    }
                }
            }
            
            //After separate variables from groups try to reduce groups 
            mergeGroups(varGroups, groupSyncs);
            
            //Check if there is overlap
            isOverlap = false;
            groupNames.clear();
            groupNames.addAll(varGroups.keySet());
            similarity = 1;
            for (int i = 0; i < groupNames.size() - 1; i++) {
                cvgA = varGroups.get(groupNames.get(i));
                for (int j = i + 1; j < groupNames.size(); j++) {
                    cvgB = varGroups.get(groupNames.get(j));
                    if (cvgA.overlaps(cvgB)) {
                        isOverlap = true;
                        if (cvgA.getSimilarity(cvgB) < similarity) {
                            similarity = cvgA.getSimilarity(cvgB);
                            nameA = cvgA.getName();
                            nameB = cvgB.getName();
                        }
                    }
                }
            }
        } 
    }
    
    /**
     * Merge similar groups.
     * @param varGroups     The variable groups
     * @param groupSyncs    The synchronization groups
     */
    private static void mergeGroups(Hashtable<String, CactusVarGroup> varGroups, Hashtable<Integer, ArrayList<String>> groupSyncs) {
        ArrayList<String> groupNames = new ArrayList<String>();
        groupNames.addAll(varGroups.keySet());
        for (int i = groupNames.size() - 1; i >= 1; i--) {
            //Check to avoid out of bounds when some groups are reduced
            if (i < groupNames.size()) {
                CactusVarGroup cvgA = varGroups.get(groupNames.get(i));
                for (int j = i - 1; j >= 0; j--) {
                    if (j < groupNames.size()) {
                        CactusVarGroup cvgB = varGroups.get(groupNames.get(j));
                        if (cvgA.same(cvgB)) {
                            //Use groupA instead of groupB
                            Iterator<Integer> it = groupSyncs.keySet().iterator();
                            while (it.hasNext()) {
                                Integer blockNumber = it.next();
                                ArrayList<String> syncNames = groupSyncs.get(blockNumber);
                                if (syncNames.contains(cvgB.getName())) {
                                    syncNames.remove(cvgB.getName());
                                    syncNames.add(cvgA.getName());
                                    groupSyncs.put(blockNumber, syncNames);
                                }
                            }
                            //Remove groupB
                            varGroups.remove(cvgB.getName());
                            groupNames.remove(cvgB.getName());
                        }
                    }
                }
            }
        }
    }
    
    /**
     * Changes a variable name with a safe name.
     * @param problemDoc        The problem 
     * @param varName           The variable to change
     * @throws CGException      CG004 External error
     */
    private static void changeVar(Document problemDoc, String varName) throws CGException {
        NodeList reserved = CodeGeneratorUtils.find(problemDoc, "//mt:ci[text() = '" + varName + "']");
        if (reserved.getLength() > 0) {
            for (int i = 0; i < reserved.getLength(); i++) {
                reserved.item(i).setTextContent(varName + "variable");
            }
            //Could be a field or parameter
            String query = "/*/mms:field[text() = '" + varName + "']|/*/mms:auxiliarField[text() = '" + varName + "']|/*/" 
                + "mms:parameter/mms:name[text() = '" + varName + "']";
            reserved = CodeGeneratorUtils.find(problemDoc, query);
            for (int i = 0; i < reserved.getLength(); i++) {
                reserved.item(i).setTextContent(varName + "variable");
            }
            //Could be a function name or parameter
            query = "//mms:functionName[text() = '" + varName + "']|//" 
                + "mms:functionParameter/mms:name[text() = '" + varName + "']";
            reserved = CodeGeneratorUtils.find(problemDoc, query);
            for (int i = 0; i < reserved.getLength(); i++) {
                reserved.item(i).setTextContent(varName + "variable");
            }
        }
    }
    
    /**
     * Preprocess the problem to adapt to special cactus issues.
     * 1 - CCTK_PASS_FTOF in the calling routine
     * 2 - current iteration number
     * 3 - convert int to real
     * 4 - reserved grid thorn variables
     * 5 - Numbers to real
     * 6 - grid values replacement
     * 7 - tag currentTimeStep to cctk_time
     * 9 - Replace unnecessary namespaces in ci tags
     * 10 - continuous coordinate utilization
     * 11 - add region to fluxes, speeds and parabolic terms variables names 
     * 12 - Replace regionId tags 
     * 13 - Replace iterationNumber tags
     * 14 - Replace stencil tags
     * 15 - FORTRAN SIGN correction 
     * 16 - Cast double to integers in indexes with operations
     * 17 - Add "call" to extrapolate subroutines
     * 18 - Extrapolation FOV processing
     * @param problemDoc        The problem
     * @param problemInfo       The problem information
     * @return                  The problem processed
     * @throws CGException      CG004 External error
     */
    public static Document preprocess(Document problemDoc, ProblemInfo problemInfo) throws CGException {
        //1 - To pass arguments to another routine in the same thorn use CCTK_PASS_FTOF in the calling routine. 
        //See Cactus user guide chapter B7.2.1
        Element macroVariable = CodeGeneratorUtils.createElement(problemDoc, mtUri, "ci");
        macroVariable.setTextContent("CCTK_PASS_FTOF");
        //Insert the macro in the function callings
        NodeList functionCalls = CodeGeneratorUtils.find(problemDoc, "//mt:apply[@type='function']|//mt:ci[@type = 'flux']");
        for (int i = 0; i < functionCalls.getLength(); i++) {
            Element call = (Element) functionCalls.item(i);
            if (call.getLocalName().equals("apply")) {
                call = (Element) call.getFirstChild();
            }
            if (call.getNextSibling() != null) {
                call.getParentNode().insertBefore(macroVariable.cloneNode(true), call.getNextSibling());
            }
            else {
                call.getParentNode().appendChild(macroVariable.cloneNode(true));
            }
        }
        //2 - Replace the value of the time coordinate with the current iteration number.
        String timeCoordinate = CodeGeneratorUtils.find(problemDoc, "//mms:timeCoordinate").item(0).getTextContent();
        NodeList found = CodeGeneratorUtils.find(problemDoc, "//mt:ci[text() = '(" + timeCoordinate + ")' and not(ancestor::mt:msup)]");
        for (int i = 0; i < found.getLength(); i++) {
            found.item(i).setTextContent("cctk_iteration");
        }
        //3 - Convert integer to reals for math functions
        String query = "//mt:cn[contains(text(), '.')]";
        NodeList constants = CodeGeneratorUtils.find(problemDoc, query);
        for (int i = 0; i < constants.getLength(); i++) {
            constants.item(i).setTextContent(constants.item(i).getTextContent() + "d0");
        }
        query = "//mt:apply[child::mt:max or child::mt:min or child::mt:abs or child::mt:cos or child::mt:sin or child::mt:tan " 
            + "or child::mt:root or child::mt:floor or child::mt:ceiling or child::mt:sinh or child::mt:cosh or child::mt:tanh " 
            + "or child::mt:arctan or child::mt:arccos or child::mt:arcsin or child::mt:ln or child::mt:log or child::mt:exp " 
            + "or child::mt:exponentiale]/mt:cn[not(contains(text(), '.'))]";
        constants = CodeGeneratorUtils.find(problemDoc, query);
        for (int i = 0; i < constants.getLength(); i++) {
            constants.item(i).setTextContent(constants.item(i).getTextContent() + ".0d0");
        }
        //4 - Check for reserved variables x, y, z, r if grid is inherited (3 dimensions)
        if (problemInfo.getCoordinates().size() == THREE) {
            changeVar(problemDoc, "x");
            changeVar(problemDoc, "y");
            changeVar(problemDoc, "z");
            changeVar(problemDoc, "r");
        }
        //5 - Force numbers used to be double (not field indexes)
        NodeList numbers = CodeGeneratorUtils.find(problemDoc, 
                "//mt:cn[not(contains(text(), '.')) and not(ancestor::mt:apply/*[1][name() = 'mt:ci']) " 
                + "and not(contains(text(), '\"')) and not (contains(text(), \"'\")) and not(ancestor::mt:apply/mt:ci = 'ReflectionCounter')"
                + "and not(ancestor::mt:msubsup or ancestor::mt:msub or ancestor::mt:msup)]");
        for (int i = 0; i < numbers.getLength(); i++) {
            numbers.item(i).setTextContent(numbers.item(i).getTextContent() + ".0d0");
        }
        //6 - Region value tags replacement
        query = "//*[local-name() = 'minSegmentValue']";
        NodeList minGridValues = CodeGeneratorUtils.find(problemDoc, query);
        for (int i = minGridValues.getLength() - 1; i >= 0; i--) {
            String coord = minGridValues.item(i).getTextContent();
            minGridValues.item(i).getParentNode().replaceChild(CodeGeneratorUtils.getMinCoordinate(problemDoc, coord)
                    .cloneNode(true), minGridValues.item(i));
        }
        query = "//*[local-name() = 'maxSegmentValue']";
        NodeList maxRegionValue = CodeGeneratorUtils.find(problemDoc, query);
        for (int i = maxRegionValue.getLength() - 1; i >= 0; i--) {
            String coord = maxRegionValue.item(i).getTextContent();
            maxRegionValue.item(i).getParentNode().replaceChild(CodeGeneratorUtils.getMaxCoordinate(problemDoc, coord)
                    .cloneNode(true), maxRegionValue.item(i));
        }
        //14 - Replace stencil tags
        NodeList stencil = CodeGeneratorUtils.find(problemDoc, "//stencil");
        for (int i = stencil.getLength() - 1; i >= 0; i--) {
            Element cn = CodeGeneratorUtils.createElement(problemDoc, mtUri, "cn");
            cn.setTextContent(String.valueOf(problemInfo.getSchemaStencil()));
            stencil.item(i).getParentNode().replaceChild(cn, stencil.item(i));
        }  
        
        //7 - Change tag currentTime to cctk_time
        NodeList currentTimes = CodeGeneratorUtils.find(problemDoc, "//sml:currentTime");
        for (int i = currentTimes.getLength() - 1; i >= 0; i--) {
            Element ci = CodeGeneratorUtils.createElement(problemDoc, mtUri, "ci");
            ci.setTextContent("cctk_time");
            currentTimes.item(i).getParentNode().replaceChild(ci, currentTimes.item(i));
        }

        //9 - Replace unnecessary namespaces in ci tags
        String problemDocString = CodeGeneratorUtils.domToString(problemDoc);
        problemDocString = problemDocString.replaceAll("<mt:ci xmlns:mt=\"http://www.w3.org/1998/Math/MathML\">", "<mt:ci>");
        
        Document newDoc = CodeGeneratorUtils.stringToDom(problemDocString);
        //10 - Decrement the index in the discrete coordinate when represent a utilization of continuous coordinate
        NodeList deltas = CodeGeneratorUtils.find(newDoc, "//mms:implicitParameter[mms:type ='delta_space']");
        for (int i = 0; i < deltas.getLength(); i++) {
            //Node to search
            Element apply = CodeGeneratorUtils.createElement(newDoc, mtUri, "apply");
            Element times = CodeGeneratorUtils.createElement(newDoc, mtUri, "times");
            Element coord = CodeGeneratorUtils.createElement(newDoc, mtUri, "ci");
            coord.setTextContent(deltas.item(i).getLastChild().getTextContent());
            Element delta = CodeGeneratorUtils.createElement(newDoc, mtUri, "ci");
            delta.setTextContent(deltas.item(i).getFirstChild().getNextSibling().getTextContent());
            apply.appendChild(times);
            apply.appendChild(coord);
            apply.appendChild(delta);
            //Node to put
            Element newApply = createDecrementElement(newDoc, (Element) deltas.item(i), i);
            //Search and replace in the problem
            CodeGeneratorUtils.nodeReplace(newDoc.getDocumentElement(), apply, newApply);
        }
        //18 - Extrapolation FOV processing
        CodeGeneratorUtils.exFOVProcessing(newDoc, problemInfo);
        //11 - add region to fluxes, speeds and parabolic terms variables names
        CodeGeneratorUtils.changeVariableNames(newDoc, problemInfo, false);

        //12 - Replace regionId tags
        CodeGeneratorUtils.replaceRegionIdTags(newDoc, problemInfo);
        
        //13 - Replace iterationNumber tags
        NodeList iterationNumber = CodeGeneratorUtils.find(newDoc, ".//iterationNumber");
        Element iterationElement = CodeGeneratorUtils.createElement(newDoc, mtUri, "ci");
        iterationElement.setTextContent("cctk_iteration");
        for (int j = 0; j < iterationNumber.getLength(); j++) {
            iterationNumber.item(j).getParentNode().replaceChild(iterationElement.cloneNode(true), iterationNumber.item(j));
        }
       
        //15 - FORTRAN SIGN correction
        NodeList signs = CodeGeneratorUtils.find(newDoc, ".//mt:apply[mt:ci = 'SIGN']");
        Element one = CodeGeneratorUtils.createElement(newDoc, mtUri, "cn");
        one.setTextContent("1");
        for (int j = 0; j < signs.getLength(); j++) {
            signs.item(j).insertBefore(one.cloneNode(true), signs.item(j).getLastChild());
        }
        
        //16 - Cast double to integers in indexes with operations
        NodeList reflectionCounters = CodeGeneratorUtils.find(newDoc, 
            ".//mt:ci[text() = 'ReflectionCounter' and not(ancestor::mt:apply[mt:eq]/mt:ci[text() = 'ReflectionCounter'])]");
        for (int j = reflectionCounters.getLength() - 1; j >= 0; j--) {
            Element cast = CodeGeneratorUtils.createElement(newDoc, mtUri, "apply");
            Element op = CodeGeneratorUtils.createElement(newDoc, mtUri, "int");
            cast.appendChild(op);
            cast.appendChild(reflectionCounters.item(j).cloneNode(true));
            reflectionCounters.item(j).getParentNode().replaceChild(cast, reflectionCounters.item(j));
        }
        NodeList operations = CodeGeneratorUtils.find(newDoc, 
                "//fieldExtrapolation//mt:apply[parent::mt:apply/*[1][name() = 'mt:ci' and not(text() = 'SIGN')] " 
                + "and not(descendant::mt:ci = 'ReflectionCounter') and not(ancestor::mt:apply/mt:int) "
                + "and not(ancestor::mt:msubsup or ancestor::mt:msub or ancestor::mt:msup)]");
        for (int j = operations.getLength() - 1; j >= 0; j--) {
            Element cast = CodeGeneratorUtils.createElement(newDoc, mtUri, "apply");
            Element op = CodeGeneratorUtils.createElement(newDoc, mtUri, "int");
            cast.appendChild(op);
            cast.appendChild(operations.item(j).cloneNode(true));
            operations.item(j).getParentNode().replaceChild(cast, operations.item(j));
        }
        operations = CodeGeneratorUtils.find(newDoc, 
                "//mms:function[not(descendant::return)]//mt:apply[parent::mt:apply/*[1][name() = 'mt:ci' and not(text() = 'SIGN')] " 
                + "and not(descendant::mt:ci = 'ReflectionCounter') and not(ancestor::mt:apply/mt:int) "
                + "and not(ancestor::mt:msubsup or ancestor::mt:msub or ancestor::mt:msup)]");
        for (int j = operations.getLength() - 1; j >= 0; j--) {
            Element cast = CodeGeneratorUtils.createElement(newDoc, mtUri, "apply");
            Element op = CodeGeneratorUtils.createElement(newDoc, mtUri, "int");
            cast.appendChild(op);
            cast.appendChild(operations.item(j).cloneNode(true));
            operations.item(j).getParentNode().replaceChild(cast, operations.item(j));
        }
        operations = CodeGeneratorUtils.find(newDoc, 
                "//mt:apply[parent::mt:apply/*[1][name() = 'mt:ci' and starts-with(text(), 'FOV')]  and not(ancestor::mt:apply/mt:int) "
                + "and not(mt:int)]");
        for (int j = operations.getLength() - 1; j >= 0; j--) {
            Element cast = CodeGeneratorUtils.createElement(newDoc, mtUri, "apply");
            Element op = CodeGeneratorUtils.createElement(newDoc, mtUri, "int");
            cast.appendChild(op);
            cast.appendChild(operations.item(j).cloneNode(true));
            operations.item(j).getParentNode().replaceChild(cast, operations.item(j));
        }
        //17 - Add "call" to extrapolate subroutines
        NodeList extrapolations = CodeGeneratorUtils.find(newDoc, 
                "//mt:apply[@type = 'function']/mt:ci[starts-with(text(), 'extrapolate_')]");
        for (int j = extrapolations.getLength() - 1; j >= 0; j--) {
            extrapolations.item(j).setTextContent("call " + extrapolations.item(j).getTextContent());
        }        
        
        return newDoc;
    }
   
    /**
     * Create the decrement index element when using deltas.
     * @param doc       The document
     * @param delta     The delta
     * @param index     The index of the delta in the delta array
     * @return          The element to use
     */
    private static Element createDecrementElement(Document doc, Element delta, int index) {
        Element newApply = CodeGeneratorUtils.createElement(doc, mtUri, "apply");
        Element newTimes = CodeGeneratorUtils.createElement(doc, mtUri, "times");
        Element newApplyCoord = CodeGeneratorUtils.createElement(doc, mtUri, "apply");
        Element newMinus = CodeGeneratorUtils.createElement(doc, mtUri, "minus");
        Element newPlusApply = CodeGeneratorUtils.createElement(doc, mtUri, "apply");
        Element newPlus = CodeGeneratorUtils.createElement(doc, mtUri, "minus");
        Element newFunc = CodeGeneratorUtils.createElement(doc, mtUri, "apply");
        Element newCi = CodeGeneratorUtils.createElement(doc, mtUri, "ci");
        newCi.setTextContent("cctk_lbnd");
        Element newCn = CodeGeneratorUtils.createElement(doc, mtUri, "cn");
        newCn.setTextContent(String.valueOf(index + 1));
        Element newCoord = CodeGeneratorUtils.createElement(doc, mtUri, "ci");
        newCoord.setTextContent(delta.getLastChild().getTextContent());
        Element newUnit = CodeGeneratorUtils.createElement(doc, mtUri, "cn");
        newUnit.setTextContent("1");
        Element newDelta = CodeGeneratorUtils.createElement(doc, mtUri, "ci");
        newFunc.appendChild(newCi);
        newFunc.appendChild(newCn);
        newPlusApply.appendChild(newPlus);
        newPlusApply.appendChild(newUnit);
        newPlusApply.appendChild(newFunc);
        newDelta.setTextContent(delta.getFirstChild().getNextSibling().getTextContent());
        newApply.appendChild(newTimes);
        newApply.appendChild(newApplyCoord);
        newApply.appendChild(newDelta);
        newApplyCoord.appendChild(newMinus);
        newApplyCoord.appendChild(newCoord);
        newApplyCoord.appendChild(newPlusApply);
        
        return newApply;
    }
    
    /**
     * Get the Fortran type from the function parameter type.
     * @param type              The function parameter type
     * @return                  The fortran type
     * @throws CGException      CG003 Not possible to create code for the platform
     */
    public static String getType(String type) throws CGException {
        if (type.toLowerCase().equals("int")) {
            return "CCTK_INT";
        }
        if (type.toLowerCase().equals("real") || type.toLowerCase().equals("field")) {
            return "CCTK_REAL";
        }
        if (type.toLowerCase().equals("string")) {
            throw new CGException(CGException.CG003, "The type " + type.toLowerCase() + " is not permitted due to low time performance");
        }
        if (type.toLowerCase().equals("boolean")) {
            return "CCTK_BOOLEAN";
        }
        throw new CGException(CGException.CG003, "The type " + type.toLowerCase() + " is not permitted");
    }
    
    /**
     * Get a string that simbolizes the fortran dimesions of an aray for the fields.
     * The field dimensions is get from the region limits
     * @param doc               The document with the information
     * @return                  The dimensions in fortran format 
     * @throws CGException      CG004 External error
     */
    public static String getVariableDimensions(Document doc) throws CGException {
        String[][] xslParams = new String [2][2];
        xslParams[0][0] =  "condition";
        xslParams[0][1] =  "false";
        xslParams[1][0] =  "indent";
        xslParams[1][1] =  "0";
        String dimensions = "(";
        NodeList coords = CodeGeneratorUtils.find(doc, "//mms:coordinates//mms:spatialCoordinate");
        for (int j = 0; j < coords.getLength(); j++) {
            dimensions = dimensions + "1:cctk_lsh(" + (j + 1) + "), ";
        }
        return dimensions.substring(0, dimensions.lastIndexOf(",")) + ")";
    }
    
    /**
     * Gets the parameter values and default values for a parameter depending on the type.
     * @param cactusType        The cactus type
     * @return                  The specification for the type
     * @throws CGException      CG003 Not possible to create code for the platform
     */
    public static String getParameterSpecification(String cactusType) throws CGException {
        if (cactusType.equals("CCTK_INT")) {
            return "{" + NEWLINE
                + " *" + NEWLINE
                + "} 0" + NEWLINE;
        }
        if (cactusType.equals("CCTK_REAL")) {
            return "{" + NEWLINE
                + " *" + NEWLINE
                + "} 0.0" + NEWLINE;
        }
        if (cactusType.equals("CCTK_STRING")) {
            return "{" + NEWLINE
                + " \"\"" + NEWLINE
                + "} \"\"" + NEWLINE;
        }
        if (cactusType.equals("CCTK_BOOLEAN")) {
            return "{" + NEWLINE
                + "" + NEWLINE
                + "} true" + NEWLINE;
        }
        throw new CGException(CGException.CG003, "The type " + cactusType + " is not permitted for parameters");
    }
    
    /**
     * Generates the recursive subprogram to check the stencil width in the x3d regions.
     * @param stencil       The stencil
     * @param coordinates   The coordinates of the problem
     * @param regionIds    The region identifiers
     * @return              The code generated
     */
    public static String generateSetStencilLimitRoutine(int stencil, ArrayList<String> coordinates, ArrayList<String> regionIds) {
        String actualIndent = INDENT;
        String parameters = "";
        String intVars = "";
        String limitCalculation = "";
        String loopStart = "";
        String loopEnd = "";
        String index = "";
        String cond = "";
        for (int i = 0; i < coordinates.size(); i++) {
            String coord = coordinates.get(i);
            parameters = parameters + coord + ", ";
            index = index + "it" + coord + ", ";
            intVars = intVars + coord + "Start, " + coord + "End, it" + coord + ", ";
            limitCalculation = limitCalculation  + INDENT + INDENT + "shift = 0" + NEWLINE
                    + INDENT + INDENT + "if(cctk_bbox(" + ((i * 2) + 1) + ") .eq. 1) then" + NEWLINE
                    + INDENT + INDENT + INDENT + "do while(" + coord + " - ((" + stencil + " - 1) - (" + stencil 
                    + " - 1)/2) + shift .lt. " + (1 + stencil) + ")" + NEWLINE
                    + INDENT + INDENT + INDENT + INDENT + "shift = shift + 1" + NEWLINE
                    + INDENT + INDENT + INDENT + "end do" + NEWLINE
                    + INDENT + INDENT + "end if" + NEWLINE
                    + INDENT + INDENT + "if(cctk_bbox(" + ((i * 2) + 2) + ") .eq. 1) then" + NEWLINE
                    + INDENT + INDENT + INDENT + "do while(" + coord + " + (" + stencil + " - 1)/2 + shift .gt. cctk_lsh(" + (i + 1) + ") - " 
                    + stencil + ")" + NEWLINE
                    + INDENT + INDENT + INDENT + INDENT + "shift = shift - 1" + NEWLINE
                    + INDENT + INDENT + INDENT + "end do" + NEWLINE
                    + INDENT + INDENT + "end if" + NEWLINE
                    + INDENT + INDENT + coord + "Start = ((" + stencil + " - 1) - (" + stencil + " - 1)/2) - shift" + NEWLINE
                    + INDENT + INDENT + coord + "End = (" + stencil + " - 1)/2 + shift" + NEWLINE + NEWLINE;
            loopStart = loopStart + INDENT + actualIndent + "do it" + coord + " = " + coord + " - " + coord + "Start, " 
                    + coord + " + " + coord + "End" + NEWLINE;
            loopEnd = actualIndent + INDENT + "end do" + NEWLINE + loopEnd;
            cond = cond + "it" + coord + " .ge. 1 .and. it" + coord + " .le. cctk_lsh(" + (i + 1) + ") .and. ";
            actualIndent = actualIndent + INDENT;
             
        }
        parameters = parameters.substring(0, parameters.lastIndexOf(",")) + ", v";
        intVars = intVars.substring(0, intVars.lastIndexOf(","));
        index = index.substring(0, index.lastIndexOf(","));      
        
        String fovCase = INDENT + INDENT + "select case(v)" + NEWLINE;
        String fovFinalCase = INDENT + INDENT + "select case(v)" + NEWLINE;
        for (int i = 0; i < regionIds.size(); i++) {
            String regionId = regionIds.get(i);
            if (CodeGeneratorUtils.isNumber(regionId)) {
                fovCase = fovCase + INDENT + INDENT + INDENT + "case (" + regionId + ")" + NEWLINE;
                fovFinalCase = fovFinalCase + INDENT + INDENT + INDENT + "case (" + regionId + ")" + NEWLINE;
                for (int j = 0; j < regionIds.size(); j++) {
                    String regionId2 = regionIds.get(j);
                    if (CodeGeneratorUtils.isNumber(regionId2)) {
                        if (i == j) {
                            fovCase = fovCase + INDENT + INDENT + INDENT + INDENT + "FOV = FOV_" + regionId2 + NEWLINE;
                            fovFinalCase = fovFinalCase + INDENT + INDENT + INDENT + INDENT + "FOV_" + regionId2 + " = FOV" + NEWLINE; 
                        }
                    }

                }
            }
        }
        fovCase = fovCase + INDENT + INDENT + "end select" + NEWLINE;
        fovFinalCase = fovFinalCase + INDENT + INDENT + "end select" + NEWLINE;
        String x2 = "";
        if (coordinates.size() == 2) {
            x2 = ",X1AuxiliaryGroup";
        }
        if (coordinates.size() == THREE) {
            x2 = ",X1AuxiliaryGroup,X2AuxiliaryGroup";
        }
        
        return INDENT + "subroutine setStencilLimits(CCTK_ARGUMENTS, " + parameters + ")" + NEWLINE
                + INDENT + INDENT + "implicit none" + NEWLINE + NEWLINE
                + INDENT + INDENT + "DECLARE_CCTK_ARGUMENTS" + NEWLINE
                + INDENT + INDENT + "DECLARE_CCTK_PARAMETERS" + NEWLINE
                + INDENT + INDENT + "DECLARE_CCTK_FUNCTIONS" + NEWLINE + NEWLINE
                + INDENT + INDENT + "CCTK_INT, INTENT(IN) :: " + parameters + NEWLINE
                + INDENT + INDENT + "CCTK_INT " + intVars + NEWLINE
                + INDENT + INDENT + "CCTK_INT shift" + NEWLINE
                + INDENT + INDENT + "CCTK_INT FOV(X0AuxiliaryGroup" + x2 + ")" + NEWLINE + NEWLINE
                + fovCase
                + limitCalculation
                + loopStart
                + actualIndent + INDENT + "if(" + cond + "FOV(" + index + ") .eq. 0) then" + NEWLINE
                + actualIndent + INDENT + INDENT + "interior(" + index + ") = 1" + NEWLINE
                + actualIndent + INDENT + "end if" + NEWLINE
                + loopEnd
                + fovFinalCase
                + INDENT + "end subroutine setStencilLimits" + NEWLINE + NEWLINE;
    }
    
    /**
     * Generates the subprogram to clean the thickness of the regions.
     * @param stencil       The stencil
     * @param coordinates   The coordinates of the problem
     * @param regionIds    The region identifiers
     * @return              The code generated
     */
    public static String generateCheckStencilRoutine(int stencil, ArrayList<String> coordinates, ArrayList<String> regionIds) {
        String parameters = "";
        String index = "";
        String integers = "";
        for (int i = 0; i < coordinates.size(); i++) {
            String coord = coordinates.get(i);
            integers = integers + "it" + coord + ", ";
            parameters = parameters + coord + ", ";
            index = index + coord + ", ";
        }
        parameters = parameters.substring(0, parameters.lastIndexOf(",")) + ", v";
        index = index.substring(0, index.lastIndexOf(","));
        integers = integers.substring(0, integers.lastIndexOf(","));

        //Combinations
        String toDelete = "";
        ArrayList<String> combinations = new ArrayList<String>();
        for (int i = 0; i < coordinates.size(); i++) {
            if (combinations.isEmpty()) {
                combinations.add("0");
                combinations.add("+");
                combinations.add("-");
            }
            else {
                ArrayList<String> combTmp = new ArrayList<String>();
                for (int j = 0; j < combinations.size();j++) {
                    String actualComb = combinations.get(j);
                    combTmp.add("0" + actualComb);
                    combTmp.add("+" + actualComb);
                    combTmp.add("-" + actualComb);
                }
                combinations = combTmp;
            }
        }
        String nullComb = "";
        for (int i = 0; i < coordinates.size(); i++) {
            nullComb = nullComb + "0";
        }
        combinations.remove(nullComb);
        for (int i = 0; i < combinations.size(); i++) {
            String comb = combinations.get(i);
            String cond1 = "";
            String andCond = "";
            String orCond = "";
            String loop = "";
            String endLoop = "";
            String itIndex = "";
            String actualIndent = INDENT + INDENT;
            for (int j = 0; j < comb.length(); j++) {
                String coord = coordinates.get(j);
                String pIndex = "";
                String mIndex = "";
                for (int k = 0; k < coordinates.size(); k++) {
                    String coord2 = coordinates.get(k);
                    if (k == j) {
                        pIndex = pIndex + coord2 + " + 1, ";
                        mIndex = mIndex + coord2 + " - 1, ";
                    }
                    else {
                        pIndex = pIndex + coord2 + ", ";
                        mIndex = mIndex + coord2 + ", ";
                    }
                }
                pIndex = pIndex.substring(0, pIndex.lastIndexOf(","));
                mIndex = mIndex.substring(0, mIndex.lastIndexOf(","));
                
                String action = comb.substring(j, j + 1);
                if (action.equals("0")) {
                    itIndex = itIndex + coord + ", ";
                }
                if (action.equals("+")) {
                    cond1 = cond1 + coord + " + 1 .le. cctk_lsh(" + (j + 1) + ") .and. FOV(" + pIndex + ") .gt. 0 .and. ";
                    loop = loop + actualIndent + "do it" + coord + " = " + coord + " + 1, " + coord + " + " + stencil + NEWLINE;
                    endLoop = actualIndent + "end do" + NEWLINE + endLoop;
                    actualIndent = actualIndent + INDENT;
                    andCond = andCond + "it" + coord + " .le. cctk_lsh(" + (j + 1) + ") .and. ";
                    orCond = orCond + "(it" + coord + " .gt. cctk_lsh(" + (j + 1) + ") .and. cctk_bbox(" + (2 * j + 2) + ") .eq. 1) .or. ";
                    itIndex = itIndex + "it" + coord + ", ";
                }
                if (action.equals("-")) {
                    cond1 = cond1 + coord + " - 1 .ge. 1 .and. FOV(" + mIndex + ") .gt. 0 .and. ";
                    loop = loop + actualIndent + "do it" + coord + " = " + coord + " - " + stencil + ", " + coord + " - 1" + NEWLINE;
                    endLoop = actualIndent + "end do" + NEWLINE + endLoop;
                    actualIndent = actualIndent + INDENT;
                    andCond = andCond + "it" + coord + " .ge. 1 .and. ";
                    orCond = orCond + "(it" + coord + " .lt. 1 .and. cctk_bbox(" + (2 * j + 1) + ") .eq. 1) .or. ";
                    itIndex = itIndex + "it" + coord + ", ";
                }
            }
            itIndex = itIndex.substring(0, itIndex.lastIndexOf(","));
            andCond = andCond.substring(0, andCond.lastIndexOf(" .and. "));
            orCond = orCond.substring(0, orCond.lastIndexOf(" .or. "));
            
            toDelete = toDelete + INDENT + "if (" + cond1 + " .not. toGrow) then" + NEWLINE
                    + loop
                    + actualIndent + "if ((" + andCond + "  .and. (interior(" + itIndex + ") .lt. 1 .or. ((.not. interior(" 
                    + itIndex + ") .eq. 2) .and. FOV(" + itIndex + ") .eq. 0))) .or. " + orCond + ") then" + NEWLINE
                    + actualIndent + INDENT + "toGrow = .true." + NEWLINE
                    + actualIndent + "end if" + NEWLINE
                    + endLoop
                    + INDENT + "end if" + NEWLINE;
        }
        
        String fovCase = INDENT + INDENT + "select case(v)" + NEWLINE;
        String fovFinalCase = INDENT + INDENT + "select case(v)" + NEWLINE;
        for (int i = 0; i < regionIds.size(); i++) {
            String regionId = regionIds.get(i);
            if (CodeGeneratorUtils.isNumber(regionId)) {
                fovCase = fovCase + INDENT + INDENT + INDENT + "case (" + regionId + ")" + NEWLINE;
                fovFinalCase = fovFinalCase + INDENT + INDENT + INDENT + "case (" + regionId + ")" + NEWLINE;
                for (int j = 0; j < regionIds.size(); j++) {
                    String regionId2 = regionIds.get(j);
                    if (CodeGeneratorUtils.isNumber(regionId2)) {
                        if (i == j) {
                            fovCase = fovCase + INDENT + INDENT + INDENT + INDENT + "FOV = FOV_" + regionId2 + NEWLINE;
                            fovFinalCase = fovFinalCase + INDENT + INDENT + INDENT + INDENT + "FOV_" + regionId2 + " = FOV" + NEWLINE; 
                        }
                    }

                }
            }
        }
        fovCase = fovCase + INDENT + INDENT + "end select" + NEWLINE;
        fovFinalCase = fovFinalCase + INDENT + INDENT + "end select" + NEWLINE;
        String x2 = "";
        if (coordinates.size() == 2) {
            x2 = ",X1AuxiliaryGroup";
        }
        if (coordinates.size() == THREE) {
            x2 = ",X1AuxiliaryGroup,X2AuxiliaryGroup";
        }
        
        return INDENT + "logical function checkStencil(CCTK_ARGUMENTS,  " + parameters + ")" + NEWLINE
                + INDENT + INDENT + "implicit none" + NEWLINE + NEWLINE
                + INDENT + INDENT + "DECLARE_CCTK_ARGUMENTS" + NEWLINE
                + INDENT + INDENT + "DECLARE_CCTK_PARAMETERS" + NEWLINE
                + INDENT + INDENT + "DECLARE_CCTK_FUNCTIONS" + NEWLINE + NEWLINE
                + INDENT + INDENT + "CCTK_INT, INTENT(IN) :: " + parameters + NEWLINE
                + INDENT + INDENT + "logical toGrow" + NEWLINE
                + INDENT + INDENT + "CCTK_INT " + integers + NEWLINE
                + INDENT + INDENT + "CCTK_INT FOV(X0AuxiliaryGroup" + x2 + ")" + NEWLINE + NEWLINE
                + fovCase
                + INDENT + INDENT + "toGrow = .false." + NEWLINE
                + toDelete
                + INDENT + INDENT + "if (toGrow) then" + NEWLINE
                + INDENT + INDENT + INDENT + "interior(" + index + ") = 2" + NEWLINE
                + INDENT + INDENT + "end if" + NEWLINE
                + INDENT + INDENT + "checkStencil = toGrow" + NEWLINE
                + fovFinalCase
                + INDENT + "end function checkStencil" + NEWLINE;
    }
    
    /**
     * Get a variable declaration for the variables used in the scope that are not declared.
     * @param scope             The scope of the variables
     * @param identifiers       The identifiers declared in cactus
     * @param tempVariables     the auxiliary variables
     * @return                  The declaration
     * @throws CGException      CG004 External error
     */
    public static String getVariableDeclaration(Node scope, HashSet<String> identifiers, ArrayList<String> tempVariables) throws CGException {
        //Get the array limits from the coordinate limits
        String dimensions = CactusUtils.getVariableDimensions(scope.getOwnerDocument());
        
        //Get variable declaration
        String declaration = "";
        ArrayList<String> variables = new ArrayList<String>();
        String query = ".//mt:math[(not(parent::if) or parent::then) and (not(parent::while) or parent::loop) and not(parent::return) " 
            + "and not(ancestor::mms:condition) and not(ancestor::mms:finalizationCondition)]"
            + "/mt:apply/mt:eq[not(following-sibling::*[1] = preceding-sibling::mt:math/mt:apply/mt:eq/following-sibling::*[1])]"
            + "/following-sibling::*[1]";
        NodeList leftTerms = CodeGeneratorUtils.find(scope, query);
        for (int i = 0; i < leftTerms.getLength(); i++) {
            String variable = new String(xmlTransform(leftTerms.item(i), null));
            //Take only the identifier. The indexes, if it is a field, will be ignored
            if (variable.indexOf("(") != -1) {
                variable = variable.substring(0, variable.indexOf("("));
                String varTmp = variable;
                while (variable.endsWith("_p")) {
                    varTmp = varTmp.substring(0, varTmp.lastIndexOf("_p"));
                }
                if (identifiers.contains(varTmp)) {
                    variable = varTmp;
                }
                if (!identifiers.contains(variable) && !variables.contains(variable + dimensions) 
                        && tempVariables != null && !tempVariables.contains(variable)) {
                    variables.add(variable + dimensions);
                }
            }
            else {
                String varTmp = variable;
                while (variable.endsWith("_p")) {
                    varTmp = varTmp.substring(0, varTmp.lastIndexOf("_p"));
                }
                if (identifiers.contains(varTmp)) {
                    variable = varTmp;
                }
                if (!identifiers.contains(variable) && !variables.contains(variable) && !variable.equals("ReflectionCounter")) {
                    variables.add(variable);
                }
            }
        }
        if (variables.size() > 0) {
            declaration = declaration + INDENT + INDENT + "CCTK_REAL ";
            for (int i = 0; i < variables.size(); i++) {
                declaration = declaration + variables.get(i) + ", ";
            }
            declaration = declaration.substring(0, declaration.lastIndexOf(",")) + NEWLINE;
        }
        return declaration;
    }
    
    /**
     * Gets the delta declaration and initialization for the mapping.
     * @param problem           The problem
     * @param gridLimits        The grid limits
     * @param boundaries        The type of the boundaries of the spatial coordinates
     * @param stencil           The stencil of the problem
     * @return                  The deltas for all the coordinates (base and auxiliary)
     * @throws CGException      CG004 External error
     */
    public static String getAllDeltas(Document problem, HashMap<String, CoordinateInfo> gridLimits, HashMap<String, String> boundaries, 
            int stencil) throws CGException {
        String result = "";
        String[][] xslParams = new String [2][2];
        xslParams[0][0] =  "condition";
        xslParams[0][1] =  "false";
        xslParams[1][0] =  "indent";
        xslParams[1][1] =  "0";
        //Declarations
        NodeList deltas = CodeGeneratorUtils.find(problem, 
                "//mms:implicitParameter[starts-with(mms:type,\"delta\")]/mms:value");
        for (int i = 0; i < deltas.getLength(); i++) {
            result = result + INDENT + INDENT + "CCTK_REAL " + CactusUtils.variableNormalize(deltas.item(i).getTextContent()) + NEWLINE;
        }
        //Initializations
        result = result + NEWLINE;
        for (int i = 0; i < deltas.getLength(); i++) {
            Node type = deltas.item(i).getParentNode().getFirstChild();
            if (type.getTextContent().equals("delta_time")) {
                result = result + INDENT + INDENT + CactusUtils.variableNormalize(deltas.item(i).getTextContent()) + " = CCTK_DELTA_TIME" + NEWLINE;
            }
            else {
                int position = 0;
                String minFormula = "";
                String maxFormula = "";
                int j = 0;
                String deltaCoord = deltas.item(i).getNextSibling().getTextContent();
                //Take the delta coordinate in the base coordinate system if necessary
                if (!gridLimits.containsKey(deltaCoord)) {
                    String query = "//mms:toAuxiliar[descendant::mt:ci = '" + deltaCoord + "']/mms:coordinate";
                    deltaCoord = CodeGeneratorUtils.find(problem, query).item(0).getTextContent();
                }
                Iterator<String> coordIt = gridLimits.keySet().iterator();
                while (coordIt.hasNext()) {
                    String coord = coordIt.next();
                    if (coord.equals(deltaCoord)) {
                        position = j;
                        Element minValue = gridLimits.get(coord).getMin();
                        minFormula = CactusUtils.xmlTransform(minValue, xslParams);
                        Element maxValue = gridLimits.get(coord).getMax();
                        maxFormula = CactusUtils.xmlTransform(maxValue, xslParams);
                    }
                    j++;
                }
                int minusDelta = 1;
                if (boundaries.get(deltaCoord).equals("periodical")) {
                    minusDelta = 2 * stencil;
                }
                if (CodeGeneratorUtils.isNumber(minFormula) && CodeGeneratorUtils.isNumber(maxFormula)) {
                    result = result + INDENT + INDENT + CactusUtils.variableNormalize(deltas.item(i).getTextContent()) + " = " 
                        + (CodeGeneratorUtils.toNumber(maxFormula) - CodeGeneratorUtils.toNumber(minFormula)) 
                        + "d0/(cctk_gsh(" + (position + 1) + ") - " + minusDelta + ")" + NEWLINE;
                }
                else {
                    result = result + INDENT + INDENT + CactusUtils.variableNormalize(deltas.item(i).getTextContent()) + " = (" 
                        + maxFormula + " - (" + minFormula + "))/(cctk_gsh(" + (position + 1) + ") - " + minusDelta + ")" + NEWLINE;
                }
            }
        }
        return result;
    }
    
    /**
     * Create a hard region condition.
     * @param coordIndex    The coordinates indexes
     * @param coords        The coordinates
     * @param field         The field for the condition
     * @return              The code
     */
    public static String hardRegionCondition(String coordIndex, ArrayList<String> coords, String field) {
        String condition = "";
        for (int i = 0; i < coords.size(); i++) {
            condition = condition + "d_" + coords.get(i) + "_" + field + "(" + coordIndex + ") .ne. 0 .or. ";
        }
        return "(" + condition.substring(0, condition.lastIndexOf(" .or.")) + ")";
    }
    
    /**
     * Create the special loop structure due to the use of flat or maximal dissipation.
     * The first corner should have problems to get flat value if the rest of the cells are not assigned.
     * The general loop must start from the center of the domain and is divided in 4 (2D) or 8 (3D) loops
     * to cover all the cells.
     * @param problemInfo       The problem info
     * @param region           The region code
     * @return                  The code
     */
    public static String createFlatLoopStructure(ProblemInfo problemInfo, String region) {
        if (problemInfo.getCoordinates().size() == THREE) {
            return createExecutionBlockLoopBound(problemInfo, true, true, true)
                + region
                + createExecutionBlockLoopEnd(problemInfo)
                + createExecutionBlockLoopBound(problemInfo, true, true, false)
                + region
                + createExecutionBlockLoopEnd(problemInfo)
                + createExecutionBlockLoopBound(problemInfo, true, false, false)
                + region
                + createExecutionBlockLoopEnd(problemInfo)
                + createExecutionBlockLoopBound(problemInfo, true, false, true)
                + region
                + createExecutionBlockLoopEnd(problemInfo)
                + createExecutionBlockLoopBound(problemInfo, false, true, true)
                + region
                + createExecutionBlockLoopEnd(problemInfo)
                + createExecutionBlockLoopBound(problemInfo, false, true, false)
                + region
                + createExecutionBlockLoopEnd(problemInfo)
                + createExecutionBlockLoopBound(problemInfo, false, false, false)
                + region
                + createExecutionBlockLoopEnd(problemInfo)
                + createExecutionBlockLoopBound(problemInfo, false, false, true)
                + region
                + createExecutionBlockLoopEnd(problemInfo); 
        }
		if (problemInfo.getCoordinates().size() == 2) {
		    return createExecutionBlockLoopBound(problemInfo, true, true, false)
		        + region
		        + createExecutionBlockLoopEnd(problemInfo)
		        + createExecutionBlockLoopBound(problemInfo, true, false, false)
		        + region
		        + createExecutionBlockLoopEnd(problemInfo)
		        + createExecutionBlockLoopBound(problemInfo, false, true, false)
		        + region
		        + createExecutionBlockLoopEnd(problemInfo)
		        + createExecutionBlockLoopBound(problemInfo, false, false, false)
		        + region
		        + createExecutionBlockLoopEnd(problemInfo);
		}
		return createExecutionBlockLoopBound(problemInfo, true, true, false)
		    + region
		    + createExecutionBlockLoopEnd(problemInfo)
		    + createExecutionBlockLoopBound(problemInfo, true, false, false)
		    + region
		    + createExecutionBlockLoopEnd(problemInfo);
    }
    
    /**
     * Creates the block loop starting at the center of the domain. This is necessary for flat and maximal
     * dissipation boundaries when parabolic terms are used.
     * @param pi            The problem information
     * @param xForward      The direction of the loop (true forward; false backward) for the 1st coordinate
     * @param yForward      The direction of the loop (true forward; false backward) for the 2nd coordinate
     * @param zForward      The direction of the loop (true forward; false backward) for the 3rd coordinate
     * @return              The loop
     */
    private static String createExecutionBlockLoopBound(ProblemInfo pi, boolean xForward, boolean yForward, boolean zForward) {
        String block = "";
        
        String actualIndent = INDENT + INDENT;
        for (int k = pi.getCoordinates().size() - 1; k >= 0; k--) {
            String coordString = pi.getCoordinates().get(k);
            //Loop iteration
            if (k == 2) {
                if (zForward) {
                    block =  block + actualIndent + "do " + coordString + " = " + coordString + "End/2, " + coordString + "End" + NEWLINE;
                }
                else {
                    block =  block + actualIndent + "do " + coordString + " = " + coordString + "End/2 - 1, " + coordString + "Start, -1" + NEWLINE; 
                }
            }
            if (k == 1) {
                if (yForward) {
                    block =  block + actualIndent + "do " + coordString + " = " + coordString + "End/2, " + coordString + "End" + NEWLINE;
                }
                else {
                    block =  block + actualIndent + "do " + coordString + " = " + coordString + "End/2 - 1, " + coordString + "Start, -1" + NEWLINE; 
                }
            }
            if (k == 0) {
                if (xForward) {
                    block =  block + actualIndent + "do " + coordString + " = " + coordString + "End/2, " + coordString + "End" + NEWLINE;
                }
                else {
                    block =  block + actualIndent + "do " + coordString + " = " + coordString + "End/2 - 1, " + coordString + "Start, -1" + NEWLINE; 
                }
            }
            actualIndent = actualIndent + INDENT;
        }
        
        return block;
    }
    
    /**
     * Creates the loop end for an execution block.
     * @param problemInfo   The problem information
     * @return              The code
     */
    public static String createExecutionBlockLoopEnd(ProblemInfo problemInfo) {
        String block = "";
        //close the loop iteration
        
        String actualIndent = INDENT + INDENT;
        for (int j = 0; j < problemInfo.getCoordinates().size(); j++) {
            actualIndent = actualIndent + INDENT;
        }
        
        for (int j = 0; j < problemInfo.getCoordinates().size(); j++) {
            actualIndent = actualIndent.substring(0, actualIndent.lastIndexOf(INDENT));
            block =  block + actualIndent + "end do" + NEWLINE;
        }
        return block;
    }
    
    /**
     * Gets the right condition in a boundary.
     * @param parabolicTerms        If parabolic terms are used
     * @param stencil               The stencil
     * @param regionBaseNumber     The number of the equivalent internal region
     * @param coordinateIndex       The coordinate index
     * @param coordinates           The spatial coordinates
     * @param regions              The region identifiers
     * @return                      The code
     */
    public static String getRightCondition(boolean parabolicTerms, int stencil, int regionBaseNumber, int coordinateIndex, 
            ArrayList<String> coordinates, ArrayList<String> regions) {
        String condition = "";
        for (int l = 0; l < stencil; l++) {
            String stencilIndex = "";
            for (int m = 0; m < coordinates.size(); m++) {
                if (coordinateIndex == m) {
                    stencilIndex =  stencilIndex + "int(" + coordinates.get(m) + " - " + (l + 1) + "), ";
                }
                else {
                    stencilIndex =  stencilIndex + coordinates.get(m) + ", ";
                }
            }
            stencilIndex = stencilIndex.substring(0, stencilIndex.lastIndexOf(","));
            condition = condition + "(" +  coordinates.get(coordinateIndex) + " - " + (l + 1) + " .ge. 1 .and. (";
            if (regions.contains(String.valueOf(regionBaseNumber))) {
                condition = condition + "FOV_" + regionBaseNumber + "(" + stencilIndex + ") .gt. 0";
            }
            if (regions.contains(String.valueOf(regionBaseNumber)) && regions.contains(String.valueOf(regionBaseNumber + 1))) {
                condition = condition + " .or. ";
            }
            if (regions.contains(String.valueOf(regionBaseNumber + 1))) {
                condition = condition + "FOV_" + (regionBaseNumber + 1) + "(" + stencilIndex + ") .gt. 0";
            }
            condition = condition + ")) .or. ";
        }
        return condition.substring(0, condition.lastIndexOf(" .or."))
                + getRightConditionDiagonal(parabolicTerms, coordinateIndex, coordinates, stencil, regionBaseNumber, regions);
    }
    
    /**
     * Generate the condition to check diagonals of a point in the mesh. Only if parabolic terms fluxes are used.
     * @param parabolicTerms    If parabolic terms are used
     * @param coordinates       The coordinates
     * @param coord             Coordinate of the condition
     * @param stencil           The stencil of the problem
     * @param regionBaseNumber The region id
     * @param regions              The region identifiers
     * @return                  The condition
     */     
    private static String getRightConditionDiagonal(boolean parabolicTerms, int coord, ArrayList<String> coordinates, int stencil, 
            int regionBaseNumber, ArrayList<String> regions) {
        if (parabolicTerms) {
            String diagonal = "";
            for (int j = 0; j < stencil; j++) {
                for (int k = 0; k < coordinates.size(); k++) {
                    if (k != coord) {
                        for (int l = 0; l < stencil; l++) {
                            for (int s2 = 0; s2 < 2; s2++) {
                                String stencilIndex = "";
                                String checkIndex = "";
                                for (int m = 0; m < coordinates.size(); m++) {
                                    if (coord == m) {
                                        stencilIndex =  stencilIndex + "int(" + coordinates.get(m) + " - " + (j + 1) + "), ";
                                        checkIndex = checkIndex + coordinates.get(m) + " - " + (j + 1) + " .ge. 1 .and. ";
                                    }
                                    else {
                                        if (m == k) {
                                            if (s2 == 0) {
                                                stencilIndex = stencilIndex + "int(" + coordinates.get(m) + " + " + (l + 1) + "), ";
                                                checkIndex = checkIndex 
                                                        + coordinates.get(m) + " + " + (l + 1) + " .le. cctk_lsh(" + (m + 1) + ") .and. ";
                                            }
                                            else {
                                                stencilIndex = stencilIndex + "int(" + coordinates.get(m) + " - " + (l + 1) + "), ";
                                                checkIndex = checkIndex + coordinates.get(m) + " - " + (l + 1) + " .ge. 1 .and. ";
                                            }
                                        }
                                        else {
                                            stencilIndex = stencilIndex + coordinates.get(m) + ", ";
                                        }
                                    }
                                }
                                stencilIndex = stencilIndex.substring(0, stencilIndex.lastIndexOf(","));
                                checkIndex = checkIndex.substring(0, checkIndex.lastIndexOf(" .and. "));
                                
                                diagonal = diagonal + "(" + checkIndex + " .and. (";
                                if (regions.contains(String.valueOf(regionBaseNumber))) {
                                    diagonal = diagonal + "FOV_" + regionBaseNumber + "(" + stencilIndex + ") .gt. 0";
                                }
                                if (regions.contains(String.valueOf(regionBaseNumber)) 
                                        && regions.contains(String.valueOf(regionBaseNumber + 1))) {
                                    diagonal = diagonal + " .or. ";
                                }
                                if (regions.contains(String.valueOf(regionBaseNumber + 1))) {
                                    diagonal = diagonal + "FOV_" + (regionBaseNumber + 1) + "(" + stencilIndex + ") .gt. 0";
                                }
                                diagonal = diagonal + ")) .or. ";
                            }
                        }
                    }
                }
            }
            return " .or. (" + diagonal.substring(0, diagonal.lastIndexOf(" .or. ")) + ")";
        }
		return "";
    }
    
    /**
     * Gets the left condition in a boundary.
     * @param parabolicTerms        If parabolic terms are used
     * @param stencil               The stencil
     * @param regionBaseNumber     The number of the equivalent internal region
     * @param coordinateIndex       The index coordinate
     * @param coordinates           The spatial coordinates
     * @param regions              The region identifiers
     * @return                      The code
     */
    public static String getLeftCondition(boolean parabolicTerms, int stencil, int regionBaseNumber, int coordinateIndex, 
            ArrayList<String> coordinates, ArrayList<String> regions) {
        String condition = "";
        for (int l = 0; l < stencil; l++) {
            String stencilIndex = "";
            for (int m = 0; m < coordinates.size(); m++) {
                if (coordinateIndex == m) {
                    stencilIndex =  stencilIndex + "int(" + coordinates.get(m) + " + " + (l + 1) + "), ";
                }
                else {
                    stencilIndex =  stencilIndex + coordinates.get(m) + ", ";
                }
            }
            stencilIndex = stencilIndex.substring(0, stencilIndex.lastIndexOf(","));
            
            condition = condition + "(" +  coordinates.get(coordinateIndex) + " + " + (l + 1) + " .le. cctk_lsh(" + (coordinateIndex + 1) 
                    + ") .and. (";
            if (regions.contains(String.valueOf(regionBaseNumber))) {
                condition = condition + "FOV_" + regionBaseNumber + "(" + stencilIndex + ") .gt. 0";
            }
            if (regions.contains(String.valueOf(regionBaseNumber)) && regions.contains(String.valueOf(regionBaseNumber + 1))) {
                condition = condition + " .or. ";
            }
            if (regions.contains(String.valueOf(regionBaseNumber + 1))) {
                condition = condition + "FOV_" + (regionBaseNumber + 1) + "(" + stencilIndex + ") .gt. 0";
            }
            condition = condition + ")) .or. ";
        }
        return condition.substring(0, condition.lastIndexOf(" .or."))
                + getLeftConditionDiagonal(parabolicTerms, coordinateIndex, coordinates, stencil, regionBaseNumber, regions);
    }
    
    /**
     * Generate the condition to check diagonals of a point in the mesh. Only if parabolic terms fluxes are used.
     * @param parabolicTerms    If parabolic terms are used
     * @param coordinates       The coordinates
     * @param coord             Coordinate of the condition
     * @param stencil           The stencil of the problem
     * @param regionBaseNumber The region id
     * @param regions          The region identifiers
     * @return                  The condition
     */     
    private static String getLeftConditionDiagonal(boolean parabolicTerms, int coord, ArrayList<String> coordinates, int stencil, 
            int regionBaseNumber, ArrayList<String> regions) {
        if (parabolicTerms) {
            String diagonal = "";
            for (int j = 0; j < stencil; j++) {
                for (int k = 0; k < coordinates.size(); k++) {
                    if (k != coord) {
                        for (int l = 0; l < stencil; l++) {
                            for (int s2 = 0; s2 < 2; s2++) {
                                String stencilIndex = "";
                                String checkIndex = "";
                                for (int m = 0; m < coordinates.size(); m++) {
                                    if (coord == m) {
                                        stencilIndex = stencilIndex + "int(" + coordinates.get(m) + " + " + (j + 1) + "), ";
                                        checkIndex = checkIndex + coordinates.get(m) + " + " + (j + 1) + " .le. cctk_lsh(" + (m + 1) + ") .and. ";
                                    }
                                    else {
                                        if (m == k) {
                                            if (s2 == 0) {
                                                stencilIndex = stencilIndex + "int(" + coordinates.get(m) + " + " + (l + 1) + "), ";
                                                checkIndex = checkIndex 
                                                        + coordinates.get(m) + " + " + (l + 1) + " .le. cctk_lsh(" + (m + 1) + ") .and. ";
                                            }
                                            else {
                                                stencilIndex = stencilIndex + "int(" + coordinates.get(m) + " - " + (l + 1) + "), ";
                                                checkIndex = checkIndex + coordinates.get(m) + " - " + (l + 1) + " .ge. 1 .and. ";
                                            }
                                        }
                                        else {
                                            stencilIndex = stencilIndex + coordinates.get(m) + ", ";
                                        }
                                    }
                                }
                                stencilIndex = stencilIndex.substring(0, stencilIndex.lastIndexOf(","));
                                checkIndex = checkIndex.substring(0, checkIndex.lastIndexOf(" .and. "));
                                
                                diagonal = diagonal + "(" + checkIndex + " .and. (";
                                if (regions.contains(String.valueOf(regionBaseNumber))) {
                                    diagonal = diagonal + "FOV_" + regionBaseNumber + "(" + stencilIndex + ") .gt. 0";
                                }
                                if (regions.contains(String.valueOf(regionBaseNumber)) 
                                        && regions.contains(String.valueOf(regionBaseNumber + 1))) {
                                    diagonal = diagonal + " .or. ";
                                }
                                if (regions.contains(String.valueOf(regionBaseNumber + 1))) {
                                    diagonal = diagonal + "FOV_" + (regionBaseNumber + 1) + "(" + stencilIndex + ") .gt. 0";
                                }
                                diagonal = diagonal + ")) .or. ";
                            }
                        }
                    }
                }
            }
            return " .or. (" + diagonal.substring(0, diagonal.lastIndexOf(" .or. ")) + ")";
        }
		return "";
    }
    
    /**
     * Get a function declaration for the functions used in the scope.
     * @param doc               The problem document
     * @param scope             The scope of the variables
     * @return                  The declaration
     * @throws CGException      CG003 Not possible to create code for the platform
     *                          CG004 External error
     */
    public static String getFunctionDeclaration(Document doc, Node scope) throws CGException {
        
        String result = INDENT + INDENT + "CCTK_REAL ";
        NodeList functions = CodeGeneratorUtils.find(doc, "//mms:functionName");
        for (int i = 0; i < functions.getLength(); i++) {
            boolean isMacro = CodeGeneratorUtils.isMacroFunction((Element) functions.item(i).getParentNode());
            if (!isMacro) {
                if (CodeGeneratorUtils.find(scope, ".//mt:ci[text() = '" + functions.item(i).getTextContent() + "']").getLength() > 0) {
                    result = result + CactusUtils.variableNormalize(functions.item(i).getTextContent()) + ", ";
                }
            }
        }
        if (result.indexOf(",") != -1) {
            return result.substring(0, result.lastIndexOf(",")); 
        }
		return "";
    }
    
    /**
     * Create the condition for a boundary depending on its axis and side.
     * @param pi                The problem info
     * @param axis              The axis of the boundary
     * @param side              The side of the boundary
     * @return                  The condition
     * @throws CGException      CG00X External error
     */
    public static String createBoundAxisSideCondition(ProblemInfo pi, String axis, String side) throws CGException {
        String result = "";
        ArrayList<String> coordinates = pi.getCoordinates();
        String coordIndex = "";
        for (int j = coordinates.size() - 1; j >= 0; j--) {
            coordIndex = coordinates.get(j) + ", " + coordIndex;
        }
        coordIndex = coordIndex.substring(0, coordIndex.lastIndexOf(","));
        
        ArrayList<Integer> axisIds = new ArrayList<Integer>();
        if (axis.toLowerCase().equals("all")) {
            for (int i = 0; i < coordinates.size(); i++) {
                axisIds.add(1 + i * 2);
            }
        }
        else {
            axisIds.add(1 + coordinates.indexOf(CodeGeneratorUtils.contToDisc(pi.getProblem(), axis)) * 2); 
        }
        ArrayList<Integer> boundIds = new ArrayList<Integer>();
        for (int i = 0; i < axisIds.size(); i++) {
            if (side.toLowerCase().equals("all") || side.toLowerCase().equals("lower")) {
                boundIds.add(axisIds.get(i));
            }
            if (side.toLowerCase().equals("all") || side.toLowerCase().equals("upper")) {
                boundIds.add(axisIds.get(i) + 1);
            }
        }
        for (int i = 0; i < boundIds.size(); i++) {
            result = result + "FOV_" + CodeGeneratorUtils.getRegionName(pi.getProblem(), -boundIds.get(i), pi.getCoordinates()) + "(" 
                    + coordIndex + ") .gt. 0 .or. ";
        }
        result = result.substring(0, result.lastIndexOf(" .or."));
        
        return result;
    }
    
    /**
     * Creates the condition for the boundary near to its region groups.
     * @param pi                    The problem info
     * @param boundRegionGroups    The region groups of the boundary
     * @return                      The condition
     * @throws CGException          CG00X External error
     */
    public static String createBoundSegGroupCondition(ProblemInfo pi, Element boundRegionGroups) throws CGException {
        ArrayList<String> coordinates = pi.getCoordinates();
        ArrayList<String> regions = pi.getRegions();
        int stencil = pi.getSchemaStencil();
        String condition = "";
        String index = "";
        String positiveCompatible = "";
        String negativeCompatible = "";
        for (int i = 0; i < coordinates.size(); i++) {
            for (int j = 0; j < stencil; j++) {
                String positiveStencilIndex = "";
                String negativeStencilIndex = "";
                String positiveIndexCheck = "";
                String negativeIndexCheck = "";
                for (int k = 0; k < coordinates.size(); k++) {
                    if (i == k) {
                        positiveStencilIndex = 
                            positiveStencilIndex + coordinates.get(k) + " + " + (j + 1) + ", ";
                        negativeStencilIndex = 
                            negativeStencilIndex + coordinates.get(k) + " - " + (j + 1) + ", ";
                        positiveIndexCheck = coordinates.get(k) + " + " + (j + 1) + " .le. cctk_lsh(" + (k + 1) + ")";
                        negativeIndexCheck = coordinates.get(k) + " - " + (j + 1) + " .ge. 1";
                    }
                    else {
                        positiveStencilIndex = positiveStencilIndex + coordinates.get(k) + ", ";
                        negativeStencilIndex = negativeStencilIndex + coordinates.get(k) + ", ";
                    }
                }
                positiveStencilIndex = positiveStencilIndex.substring(0, positiveStencilIndex.lastIndexOf(","));
                negativeStencilIndex = negativeStencilIndex.substring(0, negativeStencilIndex.lastIndexOf(","));
                
                positiveCompatible = positiveCompatible + "(" + positiveIndexCheck + " .and. (";
                negativeCompatible = negativeCompatible + "(" + negativeIndexCheck + " .and. (";
                
                NodeList boundRegions = boundRegionGroups.getChildNodes();
                for (int l = 0; l < boundRegions.getLength(); l++) {
                    String regionName = boundRegions.item(l).getTextContent();
                
                    boolean hasInterior = pi.getRegionIds().contains(String.valueOf((regions.indexOf(regionName) * 2) + 1));
                    boolean hasSurface = pi.getRegionIds().contains(String.valueOf((regions.indexOf(regionName) * 2) + 2));
                    if (hasInterior) {
                        positiveCompatible = positiveCompatible + "FOV_" + (regions.indexOf(regionName) * 2 + 1) + "(" 
                                + positiveStencilIndex + ") .gt. 0 .or. ";
                        negativeCompatible = negativeCompatible + "FOV_" + (regions.indexOf(regionName) * 2 + 1) + "(" 
                                + negativeStencilIndex + ") .gt. 0 .or. ";
                    }
                    if (hasSurface) {
                        positiveCompatible = positiveCompatible + "FOV_" + (regions.indexOf(regionName) * 2 + 2) + "(" 
                                + positiveStencilIndex + ") .gt. 0 .or. ";
                        negativeCompatible = negativeCompatible + "FOV_" + (regions.indexOf(regionName) * 2 + 2) + "(" 
                                + negativeStencilIndex + ") .gt. 0 .or. ";
                    }
                }
                positiveCompatible = positiveCompatible.substring(0, positiveCompatible.lastIndexOf(" .or.")) + ")) .or. ";
                negativeCompatible = negativeCompatible.substring(0, negativeCompatible.lastIndexOf(" .or.")) + ")) .or. ";
            }
            index = index + coordinates.get(i) + ", ";
        }
        positiveCompatible = positiveCompatible.substring(0, positiveCompatible.lastIndexOf(" .or."));
        negativeCompatible = negativeCompatible.substring(0, negativeCompatible.lastIndexOf(" .or."));
        index = index.substring(0, index.lastIndexOf(","));
        
        condition = condition + "(" + positiveCompatible + ") .or. (" + negativeCompatible + ")"
            + getDiagonal(pi, coordinates, stencil, regions, boundRegionGroups) + " .or. ";
        
        return condition.substring(0, condition.lastIndexOf(" .or."));
    }
    
    /**
     * Generate the condition to check diagonals of a point in the mesh. Only if parabolic terms fluxes are used.
     * @param pi                    The problem info
     * @param boundRegionGroups    The region groups of the boundary
     * @param regions              The regions
     * @param coordinates           The coordinates
     * @param stencil               The stencil of the problem
     * @return                      The condition
     * @throws CGException          CG00X External error
     */     
    private static String getDiagonal(ProblemInfo pi, ArrayList<String> coordinates, int stencil, 
            ArrayList<String> regions, Element boundRegionGroups) throws CGException {
        ArrayList<String> regionIds = pi.getRegionIds();
        final int four = 4;
        final int three = 3;
        boolean generalParabolicTerms = CodeGeneratorUtils.find(pi.getProblem(), "//*[@type='parabolicTerm']").getLength() > 0;
        if (generalParabolicTerms) {
            String diagonal = "";
            for (int i = 0; i < coordinates.size(); i++) {
                for (int j = 0; j < stencil; j++) {
                    for (int k = i + 1; k < coordinates.size(); k++) {
                        for (int l = 0; l < stencil; l++) {
                            for (int s2 = 0; s2 < four; s2++) {
                                String stencilIndex = "";
                                String checkIndex = "";
                                for (int m = 0; m < coordinates.size(); m++) {
                                    if (i == m) {
                                        if (s2 == 0 || s2 == 1) {
                                            stencilIndex = stencilIndex + coordinates.get(m) + " + " + (j + 1) + ", ";
                                            checkIndex = checkIndex + coordinates.get(m) + " + " + (j + 1) + " .le. cctk_lsh(" + (m + 1) + ") .and. ";
                                        }
                                        else {
                                            stencilIndex = stencilIndex + coordinates.get(m) + " - " + (j + 1) + ", ";
                                            checkIndex = checkIndex + coordinates.get(m) + " - " + (j + 1) + " .ge. 1 .and. ";
                                        }
                                    }
                                    else {
                                        if (m == k) {
                                            if (s2 == 1 || s2 == three) {
                                                stencilIndex = stencilIndex + coordinates.get(m) + " + " + (l + 1) + ", ";
                                                checkIndex = checkIndex + coordinates.get(m) + " + " + (l + 1) + " .le. cctk_lsh(" + (m + 1) 
                                                    + ") .and. ";
                                            }
                                            else {
                                                stencilIndex = 
                                                    stencilIndex + coordinates.get(m) + " - " + (l + 1) + ", ";
                                                checkIndex = checkIndex + coordinates.get(m) + " - " + (l + 1) + " .ge. 1 .and. ";
                                            }
                                        }
                                        else {
                                            stencilIndex = stencilIndex + coordinates.get(m) + ", ";
                                        }
                                    }
                                }
                                stencilIndex = stencilIndex.substring(0, stencilIndex.lastIndexOf(","));
                                checkIndex = checkIndex.substring(0, checkIndex.lastIndexOf(" .and. "));
                                diagonal = diagonal + "((" + checkIndex + ") .and. (";
                                
                                NodeList boundRegions = boundRegionGroups.getChildNodes();
                                for (int m = 0; m < boundRegions.getLength(); m++) {
                                    String regionName = boundRegions.item(m).getTextContent();
                                    boolean parabolicTerms = CodeGeneratorUtils.find(pi.getProblem(), "//mms:regionGroup[" 
                                        + "mms:regionGroupName = '" + regionName + "']//*[@type='parabolicTerm']").getLength() > 0;
                                
                                    if (parabolicTerms) {
                                        boolean hasInterior = regionIds.contains(String.valueOf((regions.indexOf(regionName) * 2) + 1));
                                        boolean hasSurface = regionIds.contains(String.valueOf((regions.indexOf(regionName) * 2) + 2));
                                   
                                        if (hasInterior) {
                                            diagonal = diagonal + "FOV_" + (regions.indexOf(regionName) * 2 + 1) 
                                                    + "(" + stencilIndex + ") .gt. 0 .or. ";
                                        }
                                        if (hasSurface) {
                                            diagonal = diagonal + "FOV_" + (regions.indexOf(regionName) * 2 + 2) 
                                                    + "(" + stencilIndex + ") .gt. 0 .or. ";
                                        }        
                                    }
                                }
                                diagonal = diagonal.substring(0, diagonal.lastIndexOf(" .or.")) + ")) .or. ";
                            }
                        }
                    }
                }
            }
            return " .or. (" + diagonal.substring(0, diagonal.lastIndexOf(" .or. ")) + ")";
        }
		return "";
    }
    
    /**
     * Preprocess an instruction block to do the following changes:
     * 1 - Replace interiorDomainContext tags.
     * @param instruction       The instruction in xml
     * @param pi                The problem information
     * @return                  The instruction ready to be processed by the XSL
     * @throws CGException      CG004 External error
     */
    public static Node preProcessBlock(Node instruction, ProblemInfo pi) throws CGException {
        
        //1 - Replace interiorDomainContext tags
        replaceInteriorDomainContextTags(instruction, pi.getCoordinates(), pi.getSchemaStencil());
        
        return instruction;
    }
    
    /**
     * Replaces the InteriorDomainContext tag with a if clause.
     * @param instruction   The instruction
     * @param coords        The coordinates
     * @param stencil       The stencil of the problem
     * @throws CGException  CG00X External error
     */
    private static void replaceInteriorDomainContextTags(Node instruction, ArrayList<String> coords, int stencil) throws CGException {
        Document doc = instruction.getOwnerDocument();
        Element ifE = CodeGeneratorUtils.createElement(doc, null, "if");
        Element cond = CodeGeneratorUtils.createElement(doc, mtUri, "math");
        Element then = CodeGeneratorUtils.createElement(doc, null, "then");
        ifE.appendChild(cond);
        ifE.appendChild(then);
        
        Element applyTmp = null;
        for (int i = 0; i < coords.size(); i++) {
            Element apply = CodeGeneratorUtils.createElement(doc, mtUri, "apply");
            Element and = CodeGeneratorUtils.createElement(doc, mtUri, "and");
            Element applyG = CodeGeneratorUtils.createElement(doc, mtUri, "apply");
            Element applyL = CodeGeneratorUtils.createElement(doc, mtUri, "apply");
            Element lt = CodeGeneratorUtils.createElement(doc, mtUri, "leq");
            Element gt = CodeGeneratorUtils.createElement(doc, mtUri, "geq");
            Element coord = CodeGeneratorUtils.createElement(doc, mtUri, "ci");
            coord.setTextContent(coords.get(i));
            Element stencilE = CodeGeneratorUtils.createElement(doc, mtUri, "cn");
            stencilE.setTextContent(String.valueOf(stencil));
            Element last = CodeGeneratorUtils.createElement(doc, mtUri, "ci");
            last.setTextContent("cctk_lsh(" + (i + 1) + ")");
            Element applyMinus = CodeGeneratorUtils.createElement(doc, mtUri, "apply");
            Element minus = CodeGeneratorUtils.createElement(doc, mtUri, "minus");
            
            apply.appendChild(and);
            apply.appendChild(applyG);
            apply.appendChild(applyL);
            applyG.appendChild(gt);
            applyG.appendChild(coord);
            applyG.appendChild(stencilE);
            applyL.appendChild(lt);
            applyL.appendChild(coord.cloneNode(true));
            applyL.appendChild(applyMinus);
            applyMinus.appendChild(minus);
            applyMinus.appendChild(last);
            applyMinus.appendChild(stencilE.cloneNode(true));
            
            if (i == 0) {
                applyTmp = apply;
            }
            else {
                Element applyTmp2 = CodeGeneratorUtils.createElement(doc, mtUri, "apply");
                Element and2 = CodeGeneratorUtils.createElement(doc, mtUri, "and");
                applyTmp2.appendChild(and2);
                applyTmp2.appendChild(applyTmp);
                applyTmp2.appendChild(apply);
                applyTmp = (Element) applyTmp2.cloneNode(true);
            }
        }
        cond.appendChild(applyTmp);
        
        NodeList interior = CodeGeneratorUtils.find(instruction, ".//interiorDomainContext");
        for (int i = interior.getLength() - 1; i >= 0; i--) {
            Element newIf = (Element) ifE.cloneNode(true);
            NodeList children = interior.item(i).getChildNodes();
            for (int j = 0; j < children.getLength(); j++) {
                newIf.getLastChild().appendChild(children.item(j).cloneNode(true));
            }
            interior.item(i).getParentNode().replaceChild(newIf, interior.item(i));
        }
    }
}

