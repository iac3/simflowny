/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
@XmlSchema(
	    attributeFormDefault = javax.xml.bind.annotation.XmlNsForm.UNQUALIFIED,
	    elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED)
package eu.iac3.mathMS.codeGeneratorPlugin;

import javax.xml.bind.annotation.XmlSchema;
