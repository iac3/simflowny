/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.codeGeneratorPlugin.fvm;

import eu.iac3.mathMS.codeGeneratorPlugin.CGException;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.CodeGeneratorUtils;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.ExtrapolationType;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.ProblemInfo;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.SAMRAIUtils;
import eu.iac3.mathMS.codeGeneratorPlugin.utils.TimeInterpolationInfo.TimeInterpolator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Set;

import org.osgi.service.log.LogService;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Execution flow for FVM code in SAMRAI.
 * @author bminyano
 *
 */
public final class SAMRAIPDEExecution {
    static final String NL = System.getProperty("line.separator");
    static final String IND = "\u0009";
    static final int THREE = 3;
    static final String MTURI = "http://www.w3.org/1998/Math/MathML";
    static LogService logservice;
    
    /**
     * Private constructor.
     */
    private SAMRAIPDEExecution() { };
    
    /**
     * Create the code for the execution step.
     * @param pi                    The problem information
     * @return                      The file
     * @throws CGException          CG004 External error
     */
    public static String create(ProblemInfo pi) throws CGException {
        StringBuffer result = new StringBuffer();
        Document doc = pi.getProblem();
        
        int longestTimeInterpIndex = 0;
        ArrayList<Integer> timeInterpIndices = new ArrayList<Integer>();
        for (int i = 0; i < pi.getTimeInterpolationInfo().getTimeInterpolators().size(); i++) {
        	timeInterpIndices.add(0);
        }
        String[][] xslParams = new String [THREE][2];
        xslParams[0][0] =  "condition";
        xslParams[0][1] =  "true";
        xslParams[1][0] =  "indent";
        xslParams[1][1] =  "0";
        xslParams[2][0] =  "dimensions";
        xslParams[2][1] =  String.valueOf(pi.getDimensions());
        int dimensions = pi.getSpatialDimensions();
        String currIndent = IND;
        try {
            //Check if there are periodical boundaries
            boolean periodical = CodeGeneratorUtils.find(doc, "/mms:discretizedProblem/mms:boundaryConditions/mms:boundaryPolicy/mms:boundaryCondition/" 
                    + "mms:type/mms:periodical").getLength() > 0;
            //Check if parabolic terms are used
            boolean parabolicTerms = CodeGeneratorUtils.find(doc, "//*[@sml:type='parabolicTerm' or @type='parabolicTerm' or @cornerCalculationAtt='true']").getLength() > 0;
            //Check if flat or maximal dissipation
            boolean orderedBoundaries = CodeGeneratorUtils.find(doc, "/mms:discretizedProblem/mms:boundaryConditions/mms:boundaryPolicy/mms:boundaryCondition[mms:type/mms:flat or " 
                    + "mms:type/mms:maximalDissipation or mms:type/mms:reflection]").getLength() > 0 && pi.isHasMeshFields();
            //Iterate over all the blocks
            int blocks = CodeGeneratorUtils.getBlocksNum(doc, "//mms:interiorExecutionFlow/sml:simml|//mms:surfaceExecutionFlow/sml:simml");
            
            String commonVariables = IND + "//Get the dimensions of the patch" + NL 
                + IND + "const hier::Index boxfirst = patch->getBox().lower();" + NL
                + IND + "const hier::Index boxlast = patch->getBox().upper();" + NL + NL;
            if (pi.isHasParticleFields()) {
            	commonVariables = commonVariables + IND + "const hier::Index boxfirstP = particleVariables_" + pi.getParticleSpecies().toArray()[0] + "->getGhostBox().lower();" + NL
            			+ IND + "const hier::Index boxlastP = particleVariables_" + pi.getParticleSpecies().toArray()[0] + "->getGhostBox().upper();" + NL + NL;
            }
            commonVariables = commonVariables + IND + "//Get delta spaces into an array. dx, dy, dz." + NL
                + IND + "std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry,"
                		+ " hier::PatchGeometry>(patch->getPatchGeometry()));" + NL
                + IND + "const double* dx  = patch_geom->getDx();" + NL + NL
                + IND + "//Auxiliary definitions" + NL;
            String executionBlockLoop = createCellLoop(pi);
            String endPatchIteration = "}" + NL;
            
            //Initialization of the variables for iteration
            for (int k = 0; k < dimensions; k++) {
                //Initialization of the variables
                String coordString = pi.getCoordinates().get(k);
                commonVariables =  commonVariables + currIndent + "int " + coordString + "last = boxlast(" + k + ")-boxfirst(" 
                        + k + ") + 2 + 2 * d_ghost_width;" + NL;
            }
            
            boolean skipedSync = false;
            boolean previousRhsLoop = false;
            int syncNumber = 0;
            ArrayList<String> previousSyncVars = new ArrayList<String>();
            //Iterate over the blocks
            for (int i = 0; i < blocks; i++) {
                String block = "";
                //patch loop and synchronizations
                boolean boundary = CodeGeneratorUtils.find(doc, "//sml:simml/*[position() = " + (i + 1) 
                    + " and local-name() = 'boundary' and not(ancestor::mms:postInitialCondition or ancestor::mms:analysis)]/*").getLength() > 0;
                block = patchIterator(pi.getRegionIds());
                //Extrapolation
                boolean extrapolation = CodeGeneratorUtils.find(doc, "//sml:simml/*[position() = " + (i + 1) 
                    + " and local-name() = 'boundary' and not(ancestor::mms:postInitialCondition or ancestor::mms:analysis)]"
                    + "//sml:fieldExtrapolation").getLength() > 0;
                boolean rhsLoop = CodeGeneratorUtils.find(doc, "//sml:simml/*[position() = " + (i + 1)
                    + " and local-name() = 'iterateOverCells' and @subStepSyncAtt]/*").getLength() > 0 || previousRhsLoop;                    
                //synchronization required after this block
                boolean sync = CodeGeneratorUtils.find(doc, "//sml:simml/*[position() = " + (i + 1) 
                    + " and local-name() = 'iterateOverCells' and (@stencilAtt > 0 or @subStepSyncAtt = 'true') and not(ancestor::mms:postInitialCondition "
                    + "or ancestor::mms:analysis)]/*").getLength() > 0;
                //move particles before any sync
                boolean nextMoveParticles = CodeGeneratorUtils.find(doc, "//sml:simml/*[position() = " + (i + 2) 
                    + " and local-name() = 'iterateOverCells' and (descendant::mt:apply/mt:ci[position() = 1][starts-with(text(), 'moveParticles')]) and not(ancestor::mms:postInitialCondition "
                    + "or ancestor::mms:analysis)]/*").getLength() > 0;                        
                //Get scope for the declarations
                DocumentFragment scope = doc.createDocumentFragment();
                String query = "//mms:interiorExecutionFlow/sml:simml/*[" 
                        + (i + 1) + "]|//mms:surfaceExecutionFlow/sml:simml/*[" + (i + 1) + "]";
                NodeList scopes = CodeGeneratorUtils.find(doc, query);
                
                for (int k = 0; k < scopes.getLength(); k++) {
                    scope.appendChild(scopes.item(k).cloneNode(true));
                }
                if (!pi.getHardRegions().isEmpty() && boundary) {
                    block = block + IND + "//Hard region field distance variables" + NL;
                    block = block + SAMRAIUtils.addHardFieldDistDeclaration(pi.getCoordinates(), pi.getExtrapolatedFieldGroups(), 1) + NL;
                }
                /*if (extrapolation && pi.getMovementInfo().itMoves()) {
                    block = block + addStalled(pi, IND + IND);
                }*/
                if (pi.isHasParticleFields()) {
                	for (String speciesName : pi.getParticleSpecies()) {
	                	block = block + IND + "std::shared_ptr< pdat::IndexData<Particles<Particle_" + speciesName + ">, pdat::CellGeometry > > particleVariables_" + speciesName + "(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles<Particle_" + speciesName + ">, pdat::CellGeometry >, hier::PatchData>(patch->getPatchData(d_particleVariables_" + speciesName + "_id)));" + NL;
                	}
                }
                block = block + SAMRAIUtils.getPDEVariableDeclaration(scope, pi, 1, new ArrayList<String>(), 
                		"patch->", false, false) + NL
                    + commonVariables;
                if (CodeGeneratorUtils.find(doc, "//sml:simml/*[position() = " + (i + 1) + "]//sml:iterateOverParticlesFromCell").getLength() > 0) {
                	block = block + IND + "double* position = new double[" + pi.getDimensions() + "];" + NL;
                }
                currIndent = IND;
                for (int j = 0; j < dimensions; j++) {
                    currIndent = IND + currIndent;
                }
                boolean fluxes = CodeGeneratorUtils.find(doc, "//sml:simml/*[position() = " + (i + 1) 
                        + " and local-name() = 'boundary' and @typeAtt='flux' and not(ancestor::mms:postInitialCondition)]"
                        + "//sml:fieldExtrapolation").getLength() > 0;
                if (extrapolation && !fluxes && pi.getExtrapolationType() != ExtrapolationType.generalPDE) {
                    block = block + executionBlockLoop
                            + extrapolationInitialization(pi, i, currIndent)
                            + SAMRAIUtils.createCellLoopEnd(currIndent, dimensions);
                }
                        
                //Common Loop and initialization
                if (!(boundary && parabolicTerms && orderedBoundaries) && !pi.isHasABMMeshFields()) {
                    block = block + executionBlockLoop;
                }

                String region = "";
                //Add previous accumulators (region Movement)
                /*if (pi.getMovementInfo().itMoves()) {
                    region = region + addAccumulators(pi, i, currIndent);
                }*/
                //For every region (first the lower precedents)
                String extrapolationEndAssignments = "";
               // String auxNorm = "";
                for (int j = pi.getRegions().size() - 1; j >= 0; j--) {
                    String regionName = pi.getRegions().get(j);
                    boolean boundPTerms = CodeGeneratorUtils.find(doc, "//*[mms:name = '" + regionName + "']//*[@sml:type='parabolicTerm' or @type='parabolicTerm' or @cornerCalculationAtt='true']").getLength() > 0;
                    //Interior execution flow
                    region = region + regionExecutionFlow(regionName, currIndent, i, j, "interior", pi, 
                    		"//mms:interiorExecutionFlow[preceding-sibling::mms:name = '" + regionName + "']",
                            boundary && parabolicTerms && orderedBoundaries, boundPTerms);
                    //Surface execution flow
                    region = region + regionExecutionFlow(regionName, currIndent, i, j, "surface", pi,
                    		"//mms:surfaceExecutionFlow[preceding-sibling::mms:name = '" + regionName + "']",
                            boundary && parabolicTerms && orderedBoundaries, boundPTerms);
                    if (extrapolation && !fluxes && pi.getExtrapolationType() != ExtrapolationType.generalPDE) {
                        NodeList boundaryList = CodeGeneratorUtils.find(doc, "//mms:interiorExecutionFlow[preceding-sibling::" 
                            + "mms:name = '" + regionName + "']/sml:simml/*[position() = " + (i + 1) + " and child::*]");
                        for (int k = 0; k < boundaryList.getLength(); k++) {
                            Element postInit = (Element) boundaryList.item(k);
                            extrapolationEndAssignments = extrapolationEndAssignments + extrapolationFinalization(pi, currIndent, postInit, 
                                    (j * 2) + 1);
                        }
                        boundaryList = CodeGeneratorUtils.find(doc, "//mms:surfaceExecutionFlow[preceding-sibling::" 
                                + "mms:name = '" + regionName + "']/sml:simml/*[position() = " + (i + 1) + " and child::*]");
                        for (int k = 0; k < boundaryList.getLength(); k++) {
                            Element postInit = (Element) boundaryList.item(k);
                            extrapolationEndAssignments = extrapolationEndAssignments + extrapolationFinalization(pi, currIndent, postInit, 
                                    (j * 2) + 1);
                        }
                    }
                    /*if (pi.getMovementInfo().itMoves()) {
                        auxNorm = auxNorm + addNormalizationBlock(regionName, currIndent, i, j, "interior", pi)
                             + addNormalizationBlock(regionName, currIndent, i, j, "surface", pi);
                    }*/
                }
                //Add variable normalization (region Movement)
                /*if (pi.getMovementInfo().itMoves()) {
                    region = region + addNormalizations(pi, i, currIndent);
                }*/
                //Check if the structure must be special for the type of boundaries (flat, reflection and maximal dissipation)
                if (boundary && parabolicTerms && orderedBoundaries) {
                    block = block + SAMRAIUtils.createFlatLoopStructure(dimensions, pi, region, currIndent);
                }
                else {
                    block = block + region; 
                }
                //close the loop iteration
                if (!(boundary && parabolicTerms && orderedBoundaries)) {
                    block = block + SAMRAIUtils.createCellLoopEnd(currIndent, dimensions);
                }
                //Extrapolation finalization
                if (extrapolation && !extrapolationEndAssignments.equals("")) {
                    block = block + executionBlockLoop
                            + extrapolationEndAssignments
                            + SAMRAIUtils.createCellLoopEnd(currIndent, dimensions);
                }
                //Auxiliary normalization
                /*if (pi.getMovementInfo().itMoves() && !auxNorm.equals("")) {
                    block = block + executionBlockLoop
                            + auxNorm
                            + SAMRAIUtils.createExecutionBlockLoopEnd(currIndent, dimensions);
                }*/
                
                currIndent = currIndent.substring(dimensions);
                //Close loop 
                if (CodeGeneratorUtils.find(doc, "//sml:simml/*[position() = " + (i + 1) + "]//sml:iterateOverParticlesFromCell").getLength() > 0) {
                	block = block + IND + "delete[] position;" + NL;
                }
                block = block + endPatchIteration;
                //Synchronizations
                DocumentFragment blockScope = doc.createDocumentFragment();
                NodeList blockList = CodeGeneratorUtils.find(doc, "//sml:simml/*[position() = " + (i + 1) 
                		+ " and (local-name() = 'iterateOverCells' or local-name() = 'boundary') and "
                		+ "not(ancestor::mms:postInitialConditions or ancestor::mms:analysis)]/*");
                for (int j = 0; j < blockList.getLength(); j++) {
                    blockScope.appendChild(blockList.item(j).cloneNode(true));
                }
                ArrayList<String> syncVars = 
            			SAMRAIUtils.getSyncVariables(blockScope, pi.getVariables(), pi.getDimensions(), pi.getExtrapolationType());
                if (!skipedSync && !nextMoveParticles) {
                	syncNumber = i;
                }
                if ((sync || skipedSync) && !nextMoveParticles) {
                    //Use previous time interpolation index if boundaries
                	if (longestTimeInterpIndex > 0 && boundary) {
                		longestTimeInterpIndex--;
                	}
                }
                if (longestTimeInterpIndex < pi.getTimeInterpolationInfo().getLongestTimestepFactors().size() && rhsLoop && !skipedSync) {
                	longestTimeInterpIndex++;
                }
                //Obtain the current step number (needed if time interpolation in Berger-Oliger)
                String timeInterpolateSettings = "";
                if ((rhsLoop && !nextMoveParticles) || skipedSync) {
                    if (longestTimeInterpIndex <= pi.getTimeInterpolationInfo().getLongestTimestepFactors().size() && rhsLoop) {
                    	float factor = pi.getTimeInterpolationInfo().getLongestTimestepFactors().get(longestTimeInterpIndex - 1);
             
                		ArrayList<TimeInterpolator> timeInterpolators = new ArrayList<TimeInterpolator>(pi.getTimeInterpolationInfo().getTimeInterpolators().values());
                    	for (int j = 0; j < timeInterpolators.size(); j++) {
                    		TimeInterpolator ti = timeInterpolators.get(j);
                    		
                    		if (timeInterpIndices.get(j) < ti.getTimestepFactor().size() && factor == ti.getTimestepFactor().get(timeInterpIndices.get(j))) {
                        		int schemaStep = timeInterpIndices.get(j);
                        		schemaStep++;
                        		timeInterpIndices.set(j, schemaStep);
                        		if (sync || skipedSync) {
	                        		if (pi.isHasMeshFields()) {    		
	                        			timeInterpolateSettings = timeInterpolateSettings + "time_interpolate_operator_mesh" + ti.getId() + "->setStep(" + schemaStep + ");" + NL;
	                        		}
	                        		if (pi.isHasParticleFields()) {
	                        		  for (String speciesName: pi.getParticleSpecies()) {
	        	                		  timeInterpolateSettings = timeInterpolateSettings + "time_interpolate_operator_particles_" + speciesName + "->setStep(" + schemaStep + ");" + NL;
	        	                		  timeInterpolateSettings = timeInterpolateSettings + "refine_particle_operator_" + speciesName + "->setStep(" + schemaStep + ");" + NL;
	                        		  }
	                        	  }
                        		}
                        	}
                    	}
                    }
                }
                if ((sync || skipedSync) && !nextMoveParticles) {
                    //Obtain the sincronization time (needed if time interpolation in Berger-Oliger)
                    String sincronizationTime = "current_time";
                    if (longestTimeInterpIndex <= pi.getTimeInterpolationInfo().getLongestTimestepFactors().size() && rhsLoop) {
                    	float factor = pi.getTimeInterpolationInfo().getLongestTimestepFactors().get(longestTimeInterpIndex - 1);
                    	String timeIncrement = "";
                    	if (factor != 0 && factor != 1) {
                    		timeIncrement = " + simPlat_dt * " + factor;
                    	}
                    	if (factor == 1) {
                    		timeIncrement = " + simPlat_dt";
                    	}
                    	sincronizationTime = "current_time" + timeIncrement;
                    }
                    block = block + "//Fill ghosts and periodical boundaries" + NL
                    		+ timeInterpolateSettings;
                    //Mesh synchronization
                    if (syncVars.size() > 0 || previousSyncVars.size() > 0) {
                    	block = block + "d_bdry_sched_advance" + syncNumber + "[ln]->fillData(" + sincronizationTime + ", false);" + NL;
                    }
                    //Particle synchronization                    	
                    if (pi.isHasParticleFields()) {
                    	block = block + "d_bdry_sched_advance[ln]->fillData(" + sincronizationTime + ", false);" + NL;
                        if (periodical) {
                        	block = block + patchIterator(pi.getRegionIds())
                                    + SAMRAIUtils.checkPositions(pi, dimensions, IND, true)
                                    + "}" + NL;
                        }
                    }
                }
                if (sync && nextMoveParticles) {
                	previousRhsLoop = rhsLoop;
                	previousSyncVars = new ArrayList<String>(syncVars);
                	syncNumber = i;
                }
                else {
                	previousRhsLoop = false;
                	previousSyncVars = new ArrayList<String>();
                }
                //Mark skipped sync
                skipedSync = sync && nextMoveParticles;
                if (!region.equals("")) {
                    result.append(block);
                }
            }
            //Coarsen
            result.append("if (d_refinedTimeStepping) {" + NL
        			+ IND + "if (!hierarchy->finerLevelExists(ln) && last_step) {" + NL
					+ IND + IND + "int currentLevelNumber = ln;" + NL
					+ IND + IND + "while (currentLevelNumber > 0 && current_iteration[currentLevelNumber] % hierarchy->getRatioToCoarserLevel(currentLevelNumber).max() == 0) {" + NL
					+ IND + IND + IND + "d_coarsen_schedule[currentLevelNumber]->coarsenData();" + NL
					+ IND + IND + IND + "d_bdry_sched_postCoarsen[currentLevelNumber - 1]->fillData(current_time, false);" + NL
					+ IND + IND + IND + "currentLevelNumber--;" + NL
					+ IND + IND + "}" + NL
					+ IND + "}" + NL
					+ "} else {" + NL
					+ IND + "if (ln > 0) {" + NL
					+ IND + IND + "d_coarsen_schedule[ln]->coarsenData();" + NL
					+ IND + IND + "d_bdry_sched_postCoarsen[ln - 1]->fillData(current_time, false);" + NL
					+ IND + "}" + NL
					+ "}" + NL);
   
            return result.toString();
        } 
        catch (CGException e) {
            e.printStackTrace(System.out);
            throw e;
        }
        catch (Exception e) {
        	e.printStackTrace(System.out);
            throw new CGException(CGException.CG00X, e.getMessage());
        }
    }
    
    /**
     * Creates the stalled variable declaration.
     * @param pi        The problem info
     * @param indent    The current indent
     * @return          The code
     */
    public static String addStalled(ProblemInfo pi, String indent) {
        String stalledVars = "";
        for (int i = 0; i < pi.getInteriorRegionIds().size(); i++) {
            String segId = pi.getInteriorRegionIds().get(i);
            stalledVars = stalledVars + indent + "double* stalled_" + segId + " = ((pdat::NodeData<int> *) patch->getPatchData(d_stalled_" 
                    + segId + "_id).get())->getPointer();" + NL;
        }
        return stalledVars;
    }
    
    /**
     * Creates the call to the interphaseMovement function.
     * @param pi            The problem info
     * @return              The code
     * @throws CGException  CG00X External error
     */
   /* private static String addMovement(ProblemInfo pi) throws CGException {
        ArrayList<String> fields = CodeGeneratorUtils.getFields(pi.getProblem());
        String parameters = "";
        for (int i = 0; i < fields.size(); i++) {
            parameters = parameters + "d_" + SAMRAIUtils.variableNormalize(fields.get(i)) + "_id, ";
        }
        return "// Move regions" + NL
                + "interphaseMovement(current_time, " + parameters + "simPlat_dt, remesh);" + NL;
    }*/
    
    /**
     * Add the variable normalization instructions. 
     * @param pi            The problem info
     * @param blockNumber   The block of instructions index
     * @param indent        The current indent
     * @return              The code
     * @throws CGException  CG00X External error
     */
  /*  private static String addNormalizations(ProblemInfo pi, int blockNumber, String indent) throws CGException {
        Document doc = pi.getProblem();
        String result = "";
        ArrayList<String> added = new ArrayList<String>();
        NodeList vars = CodeGeneratorUtils.find(doc, "//mms:interiorExecutionFlow/sml:simml/*[position() = " 
                + (blockNumber + 1) + "]//mt:math[not(ancestor::sml:boundary)]/mt:apply/mt:eq/following-sibling::*[1]" 
                + "[name()='sml:sharedVariable' and mt:apply]");
        result = result + addNormalization(pi, vars, indent, added);
        vars = CodeGeneratorUtils.find(doc, "//mms:surfaceExecutionFlow/sml:simml/*[position() = " 
                + (blockNumber + 1) + "]//mt:math[not(ancestor::sml:boundary)]/mt:apply/mt:eq/following-sibling::*[1]" 
                + "[name()='sml:sharedVariable' and mt:apply]");
        result = result + addNormalization(pi, vars, indent, added);
        
        Iterator<String> fields = pi.getFieldTimeLevels().keySet().iterator();
        while (fields.hasNext()) {
            String field = fields.next();
            vars = CodeGeneratorUtils.find(doc, "//mms:interiorExecutionFlow/sml:simml/*[position() = " 
                    + (blockNumber + 1) + "]//mt:math[not(ancestor::sml:boundary)]/mt:apply/mt:eq/following-sibling::*[1]" 
                    + "[name()='mt:apply' and mt:ci/mt:msup[mt:ci = '" + field + "' and mt:apply[mt:ci = '(" 
                    + pi.getTimeCoord() + ")' and mt:plus and mt:cn[text() = '1']]]]");
            result = result + addNormalization(pi, vars, indent, added);
            vars = CodeGeneratorUtils.find(doc, "//mms:surfaceExecutionFlow/sml:simml/*[position() = " 
                    + (blockNumber + 1) + "]//mt:math[not(ancestor::sml:boundary)]/mt:apply/mt:eq/following-sibling::*[1]" 
                    + "[name()='mt:apply' and mt:ci/mt:msup[mt:ci = '" + field + "' and mt:apply[mt:ci = '(" 
                    + pi.getTimeCoord() + ")' and mt:plus and mt:cn[text() = '1']]]]");
            result = result + addNormalization(pi, vars, indent, added);
        }
        return result;
    }*/
    
    /**
     * Add the variable normalization instructions. 
     * @param pi            The problem info
     * @param vars          The variable list
     * @param indent        The current indent
     * @param added         The list of already added variables
     * @return              The code
     * @throws CGException  CG00X External error
     */
   /* private static String addNormalization(ProblemInfo pi, NodeList vars, String indent, ArrayList<String> added) throws CGException {
        String[][] xslParams = new String [THREE][2];
        xslParams[0][0] =  "condition";
        xslParams[0][1] =  "true";
        xslParams[1][0] =  "indent";
        xslParams[1][1] =  "0";
        xslParams[2][0] =  "dimensions";
        xslParams[2][1] =  String.valueOf(pi.getDimensions());
        String result = "";
        for (int i = 0; i < vars.getLength(); i++) {
            String variable = SAMRAIUtils.xmlTransform(vars.item(i), null);
            //Take only the identifier. The indexes will be ignored
            if (variable.indexOf("(") != -1) {
                variable = variable.substring(0, variable.indexOf("("));
            }
            if (!added.contains(variable) && pi.getVarFields().contains(variable)) {
                Element parent = CodeGeneratorUtils.createElement(pi.getProblem(), MTURI, "math");
                parent.appendChild(vars.item(i).cloneNode(true));
                String var = SAMRAIUtils.xmlTransform(
                        SAMRAIUtils.preProcessFVMBlock(parent, pi, false, false).getFirstChild(), xslParams);
                result = result + indent + "if (FOVAcc_" + variable + " > 0) " + var + " = " + var + " / FOVAcc_" + variable + ";" + NL; 
                added.add(variable);
            }
        }
        return result;
    }*/
    
    /**
     * Add the FOV and variable accumulator instructions. 
     * @param pi            The problem info
     * @param blockNumber   The block of instructions index
     * @param indent        The current indent
     * @return              The code
     * @throws CGException  CG00X External error
     */
  /*  private static String addAccumulators(ProblemInfo pi, int blockNumber, String indent) throws CGException {
        Document doc = pi.getProblem();
        String result = "";
        ArrayList<String> added = new ArrayList<String>();
        NodeList vars = CodeGeneratorUtils.find(doc, "//mms:interiorExecutionFlow/sml:simml/*[position() = " 
                + (blockNumber + 1) + "]//mt:math[not(ancestor::sml:boundary)]/mt:apply/mt:eq/following-sibling::*[1]" 
                + "[name()='sml:sharedVariable' and mt:apply]");
        result = result + addAccumulator(pi, vars, indent, added);
        vars = CodeGeneratorUtils.find(doc, "//mms:surfaceExecutionFlow/sml:simml/*[position() = " 
                + (blockNumber + 1) + "]//mt:math[not(ancestor::sml:boundary)]/mt:apply/mt:eq/following-sibling::*[1]" 
                + "[name()='sml:sharedVariable' and mt:apply]");
        result = result + addAccumulator(pi, vars, indent, added);
        
        Iterator<String> fields = pi.getFieldTimeLevels().keySet().iterator();
        while (fields.hasNext()) {
            String field = fields.next();
            vars = CodeGeneratorUtils.find(doc, "//mms:interiorExecutionFlow/sml:simml/*[position() = " 
                    + (blockNumber + 1) + "]//mt:math[not(ancestor::sml:boundary)]/mt:apply/mt:eq/following-sibling::*[1]" 
                    + "[name()='mt:apply' and mt:ci/mt:msup[mt:ci = '" + field + "' and mt:apply[mt:ci = '(" + pi.getTimeCoord() 
                    + ")' and mt:plus and mt:cn[text() = '1']]]]");
            result = result + addAccumulator(pi, vars, indent, added);
            vars = CodeGeneratorUtils.find(doc, "//mms:surfaceExecutionFlow/sml:simml/*[position() = " 
                    + (blockNumber + 1) + "]//mt:math[not(ancestor::sml:boundary)]/mt:apply/mt:eq/following-sibling::*[1]" 
                    + "[name()='mt:apply' and mt:ci/mt:msup[mt:ci = '" + field + "' and mt:apply[mt:ci = '(" + pi.getTimeCoord() 
                    + ")' and mt:plus and mt:cn[text() = '1']]]]");
            result = result + addAccumulator(pi, vars, indent, added);
        }
        return result;
    }*/
    
    /**
     * Add the FOV and variable accumulator instructions. 
     * @param pi            The problem info
     * @param vars          The variable list
     * @param indent        The current indent
     * @param added         The list of already added variables
     * @return              The code
     * @throws CGException  CG00X External error
     */
   /* private static String addAccumulator(ProblemInfo pi, NodeList vars, String indent, ArrayList<String> added) throws CGException {
        String[][] xslParams = new String [THREE][2];
        xslParams[0][0] =  "condition";
        xslParams[0][1] =  "true";
        xslParams[1][0] =  "indent";
        xslParams[1][1] =  "0";
        xslParams[2][0] =  "dimensions";
        xslParams[2][1] =  String.valueOf(pi.getDimensions());
        String result = "";
        for (int i = 0; i < vars.getLength(); i++) {
            String variable = SAMRAIUtils.xmlTransform(vars.item(i), null);
            //Take only the identifier. The indexes will be ignored
            if (variable.indexOf("(") != -1) {
                variable = variable.substring(0, variable.indexOf("("));
            }
            if (!added.contains(variable) && pi.getVarFields().contains(variable)) {
                Element parent = CodeGeneratorUtils.createElement(pi.getProblem(), MTURI, "math");
                parent.appendChild(vars.item(i).cloneNode(true));
                result = result + indent + SAMRAIUtils.xmlTransform(
                        SAMRAIUtils.preProcessFVMBlock(parent, pi, false, false).getFirstChild(), xslParams) + " = 0;" + NL
                        + indent + "FOVAcc_" + variable + " = 0;" + NL; 
                added.add(variable);
            }
        }
        return result;
    }*/
    
    /**
     * Creates the normalization for the auxiliary field variables after the execution block.
     * @param pi                The problem common information
     * @param regionName       The region name
     * @param actualIndent      The actual indent 
     * @param blockNum          The block number
     * @param regionNum        The region number
     * @param regionType       The region type (interior or surface)
     * @return                  The code
     * @throws CGException      CG00X External error
     */
   /* private static String addNormalizationBlock(String regionName, String actualIndent, int blockNum, 
            int regionNum, String regionType, ProblemInfo pi) throws CGException {
        String[][] xslParams = new String [THREE][2];
        xslParams[0][0] =  "condition";
        xslParams[0][1] =  "false";
        xslParams[1][0] =  "indent";
        xslParams[1][1] =  String.valueOf(actualIndent.length() + 1);
        xslParams[2][0] =  "dimensions";
        xslParams[2][1] =  String.valueOf(pi.getDimensions());
        
        Document doc = pi.getProblem();
        String result = "";
        int dimensions = pi.getSpatialDimensions();
        //Generate the index for the coordinates
        String index = "";
        for (int i = 0; i < dimensions; i++) {
            index =  index + pi.getCoordinates().get(i) + ", ";
        }
        index = index.substring(0, index.lastIndexOf(","));
        //Region execution flow
        NodeList blockList = CodeGeneratorUtils.find(doc, "//mms:" + regionType + "ExecutionFlow[preceding-sibling::" 
                + "mms:name = '" + regionName + "']/sml:simml/*[" + (blockNum + 1) + "]");
        for (int i = 0; i < blockList.getLength(); i++) {
            Element block = (Element) blockList.item(i);
            if (block.hasChildNodes()) {
                //Process a time step
                if (block.getLocalName().equals("iterateOverCells") && block.hasChildNodes()) {
                    if (logservice != null) {
                        logservice.log(LogService.LOG_INFO, "IterateOverCells");
                    }
                    
                    //Check that the stencil points needed by the method are available
                    int stencil = Integer.parseInt(block.getAttribute("stencilAtt"));
                    int append = Integer.parseInt(block.getAttribute("appendAtt"));
          
                    if (logservice != null) {
                        logservice.log(LogService.LOG_INFO, "Processing conditions");
                    }
                    //Add condition to calculate only inside of the array limits when there are stencil
                    String insideLimitsCondition = "";
                    if (stencil > 0) {
                        insideLimitsCondition = " && " + getLimitCondition(pi.getCoordinates(), stencil);
                    }

                    //Condition to calculate region interactions
                    int regionId = (regionNum * 2) + 1;
                    if (regionType.equals("surface")) {
                        regionId++;
                    }
                    String regionInteractionCondition = "";
                    if (append > 0) {
                        boolean parabolicTerms = block.getAttribute("cornerCalculationAtt").equals("true") || CodeGeneratorUtils.find(block, 
                                ".//*[@sml:type='parabolicTerm' or @type='parabolicTerm']").getLength() > 0;
                        regionInteractionCondition = getCondition(pi.getRegionRelation().get(regionName), pi.getRegionGroups(), 
                                pi.getCoordinates(), append, regionId, parabolicTerms);
                    }
                    
                    NodeList vars = CodeGeneratorUtils.find(block, ".//mt:math[mt:apply/mt:eq/following-sibling::*[1]" 
                            + "[name()='sml:sharedVariable' and mt:apply]]|.//mt:math[mt:apply/mt:eq/following-sibling::*[1]" 
                            + "[name()='mt:apply' and mt:ci/mt:msup[mt:apply[mt:ci = '(" 
                            + pi.getTimeCoord() + ")' and mt:plus and mt:cn[text() = '1']]]]]");
                    for (int j = 0; j < vars.getLength(); j++) {
                        String variable = new String(SAMRAIUtils.xmlTransform(vars.item(j).getFirstChild().getFirstChild().getNextSibling()
                                .getFirstChild(), null));
                        //Take only the identifier. The indexes will be ignored
                        if (variable.indexOf("(") != -1) {
                            variable = variable.substring(0, variable.indexOf("("));
                        }
                        if (!pi.getVarFields().contains(variable)) {
                            result = result + SAMRAIUtils.xmlTransform(SAMRAIUtils.preProcessFVMBlock(vars.item(j).cloneNode(true), pi, false,
                                    false), xslParams) + NL; 
                        }
                    }
                    if (!result.equals("")) {
                        result = actualIndent + "if ((vector(FOV_" + regionId + ", " + index + ") > 0" 
                                + regionInteractionCondition + ")" + insideLimitsCondition + ") {" + NL
                                + result
                                + actualIndent + "}" + NL;
                                
                    }
                }
            }
        }
        return result;
    }*/
    
    /**
     * Code for the patch iteration and syncs.
     * @param regions          	The regions of the problem
     * @return                  The code
     */
    static String patchIterator(ArrayList<String> regions) {
        return "for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {" + NL
            + IND + "const std::shared_ptr<hier::Patch >& patch = *p_it;" + NL
            + SAMRAIUtils.createFOVDeclaration(regions, IND, "patch->") + NL;
    }
      
    /**
     * Generates the execution flow code for a region.
     * @param pi                 	The problem common information
     * @param regionName         	The region name
     * @param currentIndent       	The actual indent 
     * @param blockNum           	The block number
     * @param regionNum          	The region number
     * @param regionType         	The region type (interior or surface)
     * @param orderBoundaries    	true if order boundaries are used
     * @return                   	The code
     * @throws CGException       	CG00X External error
     */
    public static String regionExecutionFlow(String regionName, String currentIndent, int blockNum, 
            int regionNum, String regionType, ProblemInfo pi, String baseQuery, boolean orderBoundaries, boolean boundPTerms) throws CGException {
        Document doc = pi.getProblem();
        String result = "";
        String[][] xslParams = new String [4][2];
        xslParams[0][0] =  "condition";
        xslParams[0][1] =  "false";
        xslParams[1][0] =  "indent";
        xslParams[2][0] =  "dimensions";
        xslParams[2][1] =  String.valueOf(pi.getDimensions());
        xslParams[3][0] = "region";
        xslParams[3][1] = String.valueOf(regionNum * 2 + 1);
        
        //Get current id
        int regionId = (regionNum * 2) + 1;
        if (regionType.equals("surface")) {
            regionId++;
        }
        pi.setCurrentRegionId(regionId);
        
        int dimensions = pi.getSpatialDimensions();
        //Generate the index for the coordinates
        String index = "";
        String particleIndex = "";
        String particleCellIndexCondition = "";
        for (int i = 0; i < dimensions; i++) {
        	String coord = pi.getCoordinates().get(i);
            index =  index + pi.getCoordinates().get(i) + ", ";
            particleIndex = particleIndex + coord + " + boxfirstP(" + i + "), ";
            particleCellIndexCondition = particleCellIndexCondition + coord + " + 1 < " + coord + "last && ";
        }
        index = index.substring(0, index.lastIndexOf(","));
        particleIndex = particleIndex.substring(0, particleIndex.lastIndexOf(","));
        particleCellIndexCondition = particleCellIndexCondition.substring(0,  particleCellIndexCondition.lastIndexOf(" &&"));
        //Region execution flow
        NodeList blockList = CodeGeneratorUtils.find(doc, baseQuery + "/sml:simml/*[" + (blockNum + 1) + "]");
        for (int i = 0; i < blockList.getLength(); i++) {
            Element block = (Element) blockList.item(i).cloneNode(true);
            /*if (pi.getMovementInfo().itMoves()) {
                block = (Element) SAMRAIUtils.preProcessFVMBlockMove(block, pi);  
            }*/
            block = (Element) SAMRAIUtils.preProcessPDEBlock(block, pi, false, false);
            if (block.hasChildNodes()) {
                //Process a time step
                if (block.getLocalName().equals("iterateOverCells") && block.hasChildNodes()) {
                	//Mesh
                    NodeList instructions = CodeGeneratorUtils.find(block, "./*[not(local-name() = 'iterateOverParticles')]");
                	if (pi.isHasMeshFields() && instructions.getLength() > 0) {
                		//Check that the stencil points needed by the method are available
                        String appendAtt = block.getAttribute("appendAtt");
                        int append = 0;
                        if (!appendAtt.equals("")) {
                            append = Integer.parseInt(appendAtt);
                        }
              
                        //Add condition to calculate only inside of the array limits when there are stencil
                        String insideLimitsCondition = "";
                        int stencil = Integer.parseInt(block.getAttribute("stencilAtt"));
                        if (stencil > 0) {
                            insideLimitsCondition = " && " + getLimitCondition(pi.getCoordinates(), stencil);
                        }
                        if (block.getAttribute("excludePhysicalBoundariesAtt").equals("true")) {
                        	insideLimitsCondition = insideLimitsCondition + " && " + getExPhysBoundCondition(pi.getCoordinates(), pi.getSchemaStencil());
                        }

                        //Condition to calculate region interactions
                        String regionInteractionCondition = "";
                        if (append > 0) {
                            boolean calculateCorners = block.getAttribute("cornerCalculationAtt").equals("true");
                            regionInteractionCondition = getCondition(pi.getRegionRelations().get(regionName), 
                                    pi.getCoordinates(), append, regionId, calculateCorners);
                        }
                        
                        if (pi.getRegions().size() > 1) {
                        	result = result + currentIndent + "if ((vector(FOV_" + regionId + ", " + index + ") > 0" 
                                    + regionInteractionCondition + ")" + insideLimitsCondition + ") {" + NL;
                        	currentIndent = currentIndent + IND;
                        }
                        else {
                        	if (!insideLimitsCondition.equals("")) {
                        		result = result + currentIndent + "if (" + insideLimitsCondition.substring(insideLimitsCondition.indexOf("&& ") + 3) + ") {" + NL;
                        		currentIndent = currentIndent + IND;
                        	}
                        }
                        
                        if (logservice != null) {
                            logservice.log(LogService.LOG_INFO, "Processing instructions");
                        }
                        //Instruction to code
                        xslParams[1][1] = String.valueOf(currentIndent.length());
                        for (int j = 0; j < instructions.getLength(); j++) {
                        	result = result + SAMRAIUtils.xmlTransform(instructions.item(j), xslParams);
                        }
                        
                     	if (pi.getRegions().size() > 1 || !insideLimitsCondition.equals("")) {
                     		currentIndent = currentIndent.substring(1);
                     		result = result + currentIndent + "}" + NL;
                     	}
                	}
                	//Particles
                	NodeList iterateOverParticles = CodeGeneratorUtils.find(block, "./sml:iterateOverParticles");
                	if (pi.isHasParticleFields() && iterateOverParticles.getLength() > 0) {
              			result = result + currentIndent + "if (" + particleCellIndexCondition + ") {" + NL  
                				+ currentIndent + IND + "hier::Index idx(" + particleIndex + ");" + NL;
                		for (int j = 0; j < iterateOverParticles.getLength(); j++) {
                			String speciesName = ((Element) iterateOverParticles.item(j)).getAttribute("speciesNameAtt");
                			instructions = iterateOverParticles.item(j).getChildNodes();
                			result = result + currentIndent + IND + "Particles<Particle_" + speciesName + ">* part_" + speciesName + " = particleVariables_" + speciesName + "->getItem(idx);" + NL
                    				+ currentIndent + IND + "for (int pit = part_" + speciesName + "->getNumberOfParticles() - 1; pit >= 0; pit--) {" + NL
                    				+ currentIndent + IND + IND + "Particle_" + speciesName + "* particle = part_" + speciesName + "->getParticle(pit);" + NL;
                    		currentIndent = currentIndent + IND + IND;
                    		
                            boolean insideCondition = false;
                            ArrayList<String> fields = SAMRAIUtils.getFieldVariables(pi.getFieldTimeLevels());
                            for (int k = 0; k < instructions.getLength(); k++) {
                                //Only movement, field, shared and position assignments should be differenced by region
                                boolean movement = isMovement(instructions.item(k));
                                boolean fieldAssignment = fieldAssignment(instructions.item(k), fields);
                                boolean positionAssignment = positionAssignment(instructions.item(k), pi.getVariableInfo().getAllPositionVariables());
                                boolean sharedAssignment = sharedAssignment(instructions.item(k), pi.getVariables());
                                if ((movement || positionAssignment || fieldAssignment || sharedAssignment) && !insideCondition) {
                                    result = result + currentIndent + "if (particle->region == " + regionId + " || particle->newRegion == " + regionId + ") {" + NL;
                                    currentIndent = currentIndent + IND;
                                    insideCondition = true;
                                }
                                //Close condition
                                if ((!movement && !positionAssignment && !fieldAssignment && !sharedAssignment) && insideCondition) {
                                	currentIndent = currentIndent.substring(1);
                                    result = result + currentIndent + "}" + NL; 
                                    insideCondition = false;
                                }
                                
                                //Process instruction
                                xslParams[1][1] = String.valueOf(currentIndent.length());
                                result = result + SAMRAIUtils.xmlTransform(instructions.item(k), xslParams);
                            }
                            //Close condition
                            if (insideCondition) {
                            	currentIndent = currentIndent.substring(1);
                                result = result + currentIndent + "}" + NL; 
                                insideCondition = false;
                            }
                            
                            currentIndent = currentIndent.substring(2);
                    		result = result + currentIndent + IND + "}" + NL;
                		}
                		result = result + currentIndent + "}" + NL;
                	}
                }
                //Process a boundary block if any axis has non periodical boundary
                if (block.getLocalName().equals("boundary")) {
                    if (block.hasChildNodes() && pi.getPeriodicalBoundary().containsValue("noperiodical")) {
                        if (logservice != null) {
                            logservice.log(LogService.LOG_INFO, "Boundary");
                        }
                        String completeRegionName = regionName;
                        if (regionType.equals("interior")) {
                            completeRegionName = completeRegionName + "I";
                        }
                        else {
                            completeRegionName = completeRegionName + "S";
                        }
                        result = result + processBoundaries(block, pi, currentIndent, completeRegionName, (regionNum * 2) + 1, 
                                regionType, orderBoundaries, boundPTerms);
                    }
                }
            }
        }
        return result;
    }
    
    /**
     * Create the condition to check if the point has to be calculated 
     * depending on the region relations.
     * @param relations         	The region relations
     * @param coordinates       	The coordinates
     * @param stencil           	The stencil of the problem
     * @param regionId         		The region id of the initial region
     * @param calculateCorners    	If corners have to be checked
     * @return                  	The condition
     */
    static String getCondition(HashMap<String, String> relations,
            ArrayList<String> coordinates, int stencil, int regionId, boolean calculateCorners) {
        String condition = "";
        
        String index = "";
        String indexLast = "";
        String positiveCompatible = "";
        String negativeCompatible = "";
        for (int i = 0; i < coordinates.size(); i++) {
            for (int j = 0; j < stencil; j++) {
                String positiveStencilIndex = "";
                String negativeStencilIndex = "";
                String positiveIndexCheck = "";
                String negativeIndexCheck = "";
                for (int k = 0; k < coordinates.size(); k++) {
                    if (i == k) {
                        positiveStencilIndex = 
                            positiveStencilIndex + coordinates.get(k) + " + " + (j + 1) + ", ";
                        negativeStencilIndex = 
                            negativeStencilIndex + coordinates.get(k) + " - " + (j + 1) + ", ";
                        positiveIndexCheck = coordinates.get(k) + " + " + (j + 1) + " < " + coordinates.get(k) + "last";
                        negativeIndexCheck = coordinates.get(k) + " - " + (j + 1) + " >= 0";
                    }
                    else {
                        positiveStencilIndex = positiveStencilIndex + coordinates.get(k) + ", ";
                        negativeStencilIndex = negativeStencilIndex + coordinates.get(k) + ", ";
                    }
                }
                positiveStencilIndex = positiveStencilIndex.substring(0, positiveStencilIndex.lastIndexOf(","));
                negativeStencilIndex = negativeStencilIndex.substring(0, negativeStencilIndex.lastIndexOf(","));
                
                positiveCompatible = positiveCompatible + "(" + positiveIndexCheck + " && vector(FOV_" + regionId + ", " 
                        + positiveStencilIndex + ") > 0) || ";
                negativeCompatible = negativeCompatible + "(" + negativeIndexCheck + " && vector(FOV_" + regionId + ", " 
                        + negativeStencilIndex + ") > 0) || ";
            }
            index = index + coordinates.get(i) + ", ";
            indexLast = indexLast + coordinates.get(i) + "last, ";
        }
        if (stencil > 0) {
            index = index.substring(0, index.lastIndexOf(","));
            indexLast = indexLast.substring(0, indexLast.lastIndexOf(","));
            positiveCompatible = positiveCompatible.substring(0, positiveCompatible.lastIndexOf(" ||"));
            negativeCompatible = negativeCompatible.substring(0, negativeCompatible.lastIndexOf(" ||"));
            
            condition = condition + " || ((" + positiveCompatible + ") || (" + negativeCompatible + ")" 
                    + getDiagonal(calculateCorners, coordinates, stencil, regionId) + ") ";
        }

        return condition;
    }
    
    /**
     * Generate the condition to check diagonals of a point in the mesh. 
     * @param calculateCorners  If corners have to be checked
     * @param coordinates       The coordinates
     * @param stencil           The stencil of the problem
     * @param regionId         	The region id of the original condition
     * @return                  The condition
     */     
    private static String getDiagonal(boolean calculateCorners, ArrayList<String> coordinates, int stencil, int regionId) {
        if (calculateCorners) {
            String diagonal = "";
            
            ArrayList<Integer> elements = new ArrayList<Integer>();
            elements.add(0);
            for (int j = 1; j <= stencil; j++) {
            	elements.add(j);
            	elements.add(-j);
            }
            ArrayList<ArrayList<Integer>> combinations = CodeGeneratorUtils.createCombinationsInt(coordinates.size(), elements);
            for (int i = 0; i < combinations.size(); i++) {
            	ArrayList<Integer> combination = combinations.get(i);
            	int zeros = 0; 
                String stencilIndex = "";
                String checkIndex = "";
            	for (int k = 0; k < combination.size(); k++) {
            		Integer op = combination.get(k);
            		if (op > 0) {
            			stencilIndex = stencilIndex + coordinates.get(k) + " + " + op + ", ";
                        checkIndex = checkIndex + coordinates.get(k) + " + " + op + " < " + coordinates.get(k) + "last && ";
            		}
            		if (op < 0) {
                        stencilIndex = stencilIndex + coordinates.get(k) + " - " + Math.abs(op) + ", ";
                        checkIndex = checkIndex + coordinates.get(k) + " - " + Math.abs(op) + " >= 0 && ";
            		}
            		if (op == 0) {
                        stencilIndex = stencilIndex + coordinates.get(k) + ", ";
                        zeros++;
            		}
            	}
                //Do not use linear combinations (00, 0+, 0-, ...) At least two increment/decrement
            	if (zeros < coordinates.size() - 1) {
                    stencilIndex = stencilIndex.substring(0, stencilIndex.lastIndexOf(","));
                    checkIndex = checkIndex.substring(0, checkIndex.lastIndexOf(" && "));
                    
                    diagonal = diagonal + "((" + checkIndex + ") && vector(FOV_" + regionId + ", " + stencilIndex + ") > 0) || ";
            	}

            }
            return " || (" + diagonal.substring(0, diagonal.lastIndexOf(" || ")) + ")";
        }
		return "";
    }
    
    /**
     * Create the condition to check if the point has to be calculated 
     * depending on the stencil and the array limits.
     * @param coordinates   The coordinates
     * @param stencil       The stencil of the problem
     * @return              The condition
     */
    static String getLimitCondition(ArrayList<String> coordinates, int stencil) {
        String condition = "(";
        
        for (int i = 0; i < coordinates.size(); i++) {
            condition = condition + coordinates.get(i) + " + " + stencil + " < " + coordinates.get(i) + "last && " 
                + coordinates.get(i) + " - " + stencil + " >= 0 && ";
        }
        condition = condition.substring(0, condition.lastIndexOf(" && "));
              
        return condition + ")";
    }
    
    /**
     * Create the condition to check if the point is in the interior domain (excluding physical boundaries).
     * @param coordinates   The coordinates
     * @param stencil       The stencil of the problem
     * @return              The condition
     */
    static String getExPhysBoundCondition(ArrayList<String> coordinates, int stencil) {
        String condition = "(";
        
        for (int i = 0; i < coordinates.size(); i++) {
        	String coord = coordinates.get(i);
            condition = condition + "(" + coord + " + " + stencil + " < " + coord + "last || "
            		+ "!patch->getPatchGeometry()->getTouchesRegularBoundary(" + i + ", 1)) && (" + coord + " - " + stencil + " >= 0 "
            		+ "|| !patch->getPatchGeometry()->getTouchesRegularBoundary(" + i + ", 0)) && ";
        }
        condition = condition.substring(0, condition.lastIndexOf(" && "));
              
        return condition + ")";
    }
    
    /**
     * Create the condition to check if the point has to be calculated 
     * depending on the stencil and the array limits.
     * @param coordinates   The coordinates
     * @param stencil       The stencil of the problem
     * @return              The condition
     */
    static String getOuterLimitCondition(ArrayList<String> coordinates, int stencil) {
        String condition = "(";
        
        for (int i = 0; i < coordinates.size(); i++) {
            condition = condition + coordinates.get(i) + " + " + stencil + " >= " + coordinates.get(i) + "last || " 
                + coordinates.get(i) + " - " + stencil + " < 0 || ";
        }
        condition = condition.substring(0, condition.lastIndexOf(" || "));
              
        return condition + ")";
    }

    /**
     * Process a boundary condition block.
     * @param boundary              The boundary block
     * @param pi                    The problem information
     * @param currentIndent          The actual indent to apply in the declarations 
     * @param regionName           The region name
     * @param regionBaseNumber     The number of the interior region equivalent to this region
     * @param regionType           The region type
     * @param orderBoundaries       true if order boundaries are used
     * @param boundPTerms           true if the boundaries must be deployed for parabolic terms
     * @return                      The fortran code to process the boundary
     * @throws CGException          CG004 External error
     */
    public static String processBoundaries(Element boundary, ProblemInfo pi, String currentIndent, String regionName, int regionBaseNumber, 
            String regionType, boolean orderBoundaries, boolean boundPTerms) throws CGException {
        String[][] xslParams = new String [THREE][2];
        xslParams[0][0] =  "condition";
        xslParams[0][1] =  "false";
        xslParams[1][0] =  "indent";
        xslParams[1][1] =  "0";
        xslParams[2][0] =  "dimensions";
        xslParams[2][1] =  String.valueOf(pi.getDimensions());
        String result = "";
        
        String meshBoundaries = "";
        HashMap<String, String> particleBoundaries = new HashMap<String, String>();
        for (String speciesName: pi.getParticleSpecies()) {
        	particleBoundaries.put(speciesName, "");
        }

        boolean fluxes = false;
        if (boundary.getAttribute("typeAtt").equals("flux")) {
            fluxes = true;
        }
        
        int dimensions = pi.getSpatialDimensions();
        String index = "";
        String particleIndex = "";
        String particleCellIndexCondition = "";
        for (int i = 0; i < dimensions; i++) {
        	String coord = pi.getCoordinates().get(i);
            index =  index + pi.getCoordinates().get(i) + ", ";
            particleIndex = particleIndex + coord + " + boxfirstP(" + i + "), ";
            particleCellIndexCondition = particleCellIndexCondition + coord + " + 1 < " + coord + "last && ";
        }
        index = index.substring(0, index.lastIndexOf(","));
        particleIndex = particleIndex.substring(0, particleIndex.lastIndexOf(","));
        particleCellIndexCondition = particleCellIndexCondition.substring(0,  particleCellIndexCondition.lastIndexOf(" &&"));
        try {
            String stencil = String.valueOf(pi.getSchemaStencil());
            //Start the evolution for the region.
            //Condition to apply the boundaries depending on the axis and the side.
            for (int i = 0; i < dimensions; i++) {
                String coordString = pi.getCoordinates().get(i);
                String contCoordString = pi.getContCoordinates().get(i);
                if (pi.getPeriodicalBoundary().get(contCoordString).equals("noperiodical")) {
                    NodeList boundaries = CodeGeneratorUtils.find(boundary, ".//sml:axis[@axisAtt = '" + coordString + "']");
                    for (int j = 0; j < boundaries.getLength(); j++) {
                        Element coordBoundary = (Element) boundaries.item(j).cloneNode(true);
                        //lower boundary
                        if (CodeGeneratorUtils.find(coordBoundary, "./sml:side[@sideAtt = 'lower' and *[not(local-name() = 'iterateOverParticles')]]").getLength() > 0) {
                        	meshBoundaries = meshBoundaries + processBoundary(boundPTerms, currentIndent, stencil, i, 
                                    pi, regionBaseNumber, coordBoundary, "lower");
                        }
                        //upper boundary
                        if (CodeGeneratorUtils.find(coordBoundary, "./sml:side[@sideAtt = 'upper' and *[not(local-name() = 'iterateOverParticles')]]").getLength() > 0) {
                        	meshBoundaries = meshBoundaries + processBoundary(boundPTerms, currentIndent, stencil, i, 
                                    pi, regionBaseNumber, coordBoundary, "upper");
                        }
                        //Particle boundaries
                        for (String speciesName: pi.getParticleSpecies()) {
                        	String tmp = particleBoundaries.get(speciesName);
                            if (CodeGeneratorUtils.find(coordBoundary, "./sml:side[@sideAtt = 'lower' and sml:iterateOverParticles[@speciesNameAtt = '" + speciesName + "']]").getLength() > 0) {
                            	tmp = tmp + processBoundaryParticles(currentIndent + IND + IND, index, 
                                        pi, regionBaseNumber, coordBoundary, coordString, "lower", speciesName);
                            }
                            if (CodeGeneratorUtils.find(coordBoundary, "./sml:side[@sideAtt = 'upper' and sml:iterateOverParticles[@speciesNameAtt = '" + speciesName + "']]").getLength() > 0) {
                            	tmp = tmp + processBoundaryParticles(currentIndent + IND + IND, index, 
                                        pi, regionBaseNumber, coordBoundary, coordString, "upper", speciesName);
                            }
                            particleBoundaries.put(speciesName, tmp);
                        }
                    }
                }
            }
            //Region extrapolation
            if (!pi.getHardRegions().isEmpty() && CodeGeneratorUtils.find(boundary, "./sml:fieldExtrapolation").getLength() > 0) {
                if (regionType.equals("interior")) {
                	meshBoundaries = meshBoundaries + currentIndent + "if (vector(FOV_" + regionBaseNumber + ", " + index + ") > 0) {" + NL;
                }
                else {
                	meshBoundaries = meshBoundaries + currentIndent + "if (vector(FOV_" + (regionBaseNumber + 1) + ", " + index + ") > 0) {" + NL;
                }
                currentIndent = currentIndent + IND;
                String coordIndex = "";
                for (int i = dimensions - 1; i >= 0; i--) {
                    coordIndex =  pi.getCoordinates().get(i) + ", " + coordIndex;
                }
                coordIndex = coordIndex.substring(0, coordIndex.lastIndexOf(","));
                if (pi.getHardRegionFields(regionName) != null) {
                	//Group of extrapolated fields to reduce simulation variables (performance)
                    ArrayList<String> fields = pi.getHardRegionFields(regionName);
                	LinkedHashMap<String, ArrayList<String>> extrapolatedFieldGroups = pi.getExtrapolatedFieldGroups();
                	Iterator<String> extrapolatedFieldGroupsIterator = extrapolatedFieldGroups.keySet().iterator();
                	String fieldExtrapolation = "";
                	while (extrapolatedFieldGroupsIterator.hasNext()) {
                		String fieldGroup = extrapolatedFieldGroupsIterator.next();
                		String fieldGroupTransf = SAMRAIUtils.variableNormalize(fieldGroup);
                		ArrayList<String> groupFields = extrapolatedFieldGroups.get(fieldGroup);
                		String tmpFieldExtrapolation = "";
                        currentIndent = currentIndent + IND;
                		for (int i = 0; i < groupFields.size(); i++) {
                			if (fields.contains(groupFields.get(i))) {
                                if (!fluxes || !CodeGeneratorUtils.isAuxiliaryField(pi, SAMRAIUtils.variableNormalize(fields.get(i)))) {
                                    NodeList instructions = CodeGeneratorUtils.find(boundary, "./sml:fieldExtrapolation[@fieldAtt ='"
                                            + fields.get(i) + "']").item(0).getChildNodes();
                                    xslParams[1][1] =  String.valueOf(currentIndent.length());
                                    for (int m = 0; m < instructions.getLength(); m++) {
                                    	tmpFieldExtrapolation = tmpFieldExtrapolation + SAMRAIUtils.xmlTransform(instructions.item(m), xslParams);
                                    }
                                }
                			}
                		}
                        currentIndent = currentIndent.substring(1);
                        if (!tmpFieldExtrapolation.equals("")) {
                        	fieldExtrapolation = fieldExtrapolation + currentIndent + "//Region field extrapolations" + NL
	                                + currentIndent + "if (" + SAMRAIUtils.hardRegionCondition(coordIndex, pi.getCoordinates(), fieldGroupTransf)
	                                + ") {" + NL
	                                + tmpFieldExtrapolation
	                                + currentIndent + "}" +  NL;
                        }
                	}
                	meshBoundaries = meshBoundaries + fieldExtrapolation;
                }
                currentIndent = currentIndent.substring(0, currentIndent.lastIndexOf(IND));
                currentIndent = currentIndent.substring(0, currentIndent.lastIndexOf(IND));
                
                meshBoundaries = meshBoundaries + currentIndent + IND + "}" + NL;
            }
            result = result + meshBoundaries;
            String particleResult = "";
            for (String speciesName: pi.getParticleSpecies()) {
            	if (!particleBoundaries.get(speciesName).equals("")) {
	            	particleResult = particleResult + currentIndent + IND + "Particles<Particle_" + speciesName + ">* part_" + speciesName + " = particleVariables_" + speciesName + "->getItem(idx);" + NL
	        				+ currentIndent + IND + "for (int pit = part_" + speciesName + "->getNumberOfParticles() - 1; pit >= 0; pit--) {" + NL
	        				+ currentIndent + IND + IND + "Particle_" + speciesName + "* particle = part_" + speciesName + "->getParticle(pit);" + NL
	        				+ particleBoundaries.get(speciesName)
	        				+ currentIndent + IND + "}" + NL;
            	}
            }
            if (!particleResult.equals("")) {
            	result = result + currentIndent + "if (" + particleCellIndexCondition + ") {" + NL
            			+ currentIndent + IND + "hier::Index idx(" + particleIndex + ");" + NL
            			+ particleResult
            			+ currentIndent + "}" + NL;
            }
            return result;
        } 
        catch (Exception e) {
            e.printStackTrace();
            throw new CGException(CGException.CG00X, e.getMessage());
        }
    }
    
    /**
     * Process the right boundary.
     * @param boundPTerms                       true if the boundaries must be deployed for parabolic terms
     * @param currentIndent                     The actual indent
     * @param stencil                           The stencil of the problem
     * @param i                                 The index of the coordinate
     * @param pi                                The process information
     * @param regionBaseNumber                  The region base number
     * @param coordBoundary                     The element with the boundary
     * @param side								Boundary side
     * @return                                  The code of the boundary
     * @throws CGException                      CG004 External error
     */
    private static String processBoundary(boolean boundPTerms, String currentIndent, String stencil, int i, ProblemInfo pi, 
            int regionBaseNumber, Element coordBoundary, String side) throws CGException {
        String[][] xslParams = new String [THREE][2];
        xslParams[0][0] =  "condition";
        xslParams[0][1] =  "false";
        xslParams[1][0] =  "indent";
        xslParams[1][1] =  "0";
        xslParams[2][0] =  "dimensions";
        xslParams[2][1] =  String.valueOf(pi.getDimensions());
        
        String condition = "";
        if (side.equals("lower")) {
        	condition = SAMRAIUtils.getLeftCondition(boundPTerms, Integer.parseInt(stencil), regionBaseNumber, i, 
                    pi.getCoordinates(), pi.getRegionIds());
        }
        else {
        	condition = SAMRAIUtils.getRightCondition(boundPTerms, Integer.parseInt(stencil), regionBaseNumber, i, 
                    pi.getCoordinates(), pi.getRegionIds());
        }
        
        try {
            String index = "";
            for (int j = 0; j < pi.getCoordinates().size(); j++) {
                index = index + pi.getCoordinates().get(j) + ", ";
            }
            index = index.substring(0, index.lastIndexOf(", "));
            
            String coord = pi.getCoordinates().get(i);
            String axis = CodeGeneratorUtils.discToCont(pi.getProblem(), coord);
            int boundId = pi.getCoordinates().indexOf(coord) * 2 + 2;
            if (side.equals("lower")) {
            	boundId--;
            }
            String result = "";
            if (pi.getRegions().size() > 1) {
            	result = currentIndent + "if ((vector(FOV_" + CodeGeneratorUtils.getRegionName(pi.getProblem(), -boundId, pi.getCoordinates()) 
                	+ ", " + index + ") > 0) && (" + condition + ")) {" + NL;
            }
            else {
             	result = currentIndent + "if ((vector(FOV_" + CodeGeneratorUtils.getRegionName(pi.getProblem(), -boundId, pi.getCoordinates()) 
            	+ ", " + index + ") > 0)) {" + NL;
            }
            currentIndent = currentIndent + IND;
            xslParams[1][1] =  String.valueOf(currentIndent.length());
            String instructions = "";
            String fieldExtrapolations = "";
            NodeList sides = CodeGeneratorUtils.find(coordBoundary, ".//sml:side[@sideAtt = '" + side + "' and *[not(local-name() = 'iterateOverParticles')]]");
            for (int k = 0; k < sides.getLength(); k++) {
                NodeList children = CodeGeneratorUtils.find(sides.item(k), "./*[not(local-name() = 'iterateOverParticles') and not(local-name() = 'fieldExtrapolation')]");
                for (int l = 0; l < children.getLength(); l++) {
                    Element child = (Element) children.item(l).cloneNode(true);
                    instructions = instructions + SAMRAIUtils.xmlTransform(child.cloneNode(true), xslParams);
                }
            	//Boundary extrapolation
                if (pi.getHardRegionFields(axis + "-" + side.substring(0, 1).toUpperCase() + side.substring(1)) != null) {
                	LinkedHashMap<String, ArrayList<String>> extrapolatedFieldGroups = pi.getExtrapolatedFieldGroups();
                	Iterator<String> extrapolatedFieldGroupsIterator = extrapolatedFieldGroups.keySet().iterator();
                	while (extrapolatedFieldGroupsIterator.hasNext()) {
                		String tmpFieldExtrapolation = "";
                		String fieldGroup = extrapolatedFieldGroupsIterator.next();
                		String fieldGroupTransf = SAMRAIUtils.variableNormalize(fieldGroup);
                		ArrayList<String> groupFields = extrapolatedFieldGroups.get(fieldGroup);
                        currentIndent = currentIndent + IND;
                		for (int j = 0; j < groupFields.size(); j++) {
                			NodeList fieldExtrapolation = CodeGeneratorUtils.find(sides.item(k), 
                					".//sml:fieldExtrapolation[@fieldAtt = '" + groupFields.get(j) + "']");
                			if (fieldExtrapolation.getLength() > 0) {
                                xslParams[1][1] =  String.valueOf(currentIndent.length());
                                tmpFieldExtrapolation = tmpFieldExtrapolation + SAMRAIUtils.xmlTransform(fieldExtrapolation.item(0).getFirstChild(), xslParams);
                            }
                		}
                        currentIndent = currentIndent.substring(1);
                        if (!tmpFieldExtrapolation.equals("")) {
                        	fieldExtrapolations = fieldExtrapolations + currentIndent + "//Region field extrapolations" + NL
	                                + currentIndent + "if (" + SAMRAIUtils.hardRegionCondition(index, pi.getCoordinates(), fieldGroupTransf)
	                                + ") {" + NL
	                                + tmpFieldExtrapolation
	                                + currentIndent + "}" +  NL;
                        }
                	}
                }                
            }
            if (fieldExtrapolations.equals("") && instructions.equals("")) {
                return "";
            }
            result = result + fieldExtrapolations + instructions;
            
            currentIndent = currentIndent.substring(0, currentIndent.lastIndexOf(IND));
            result = result + currentIndent + "}" + NL;
            return result;
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new CGException(CGException.CG00X, e.getMessage());
        }
    }
    
    /**
     * Creates the loop for an execution block.
     * @param pi                    The problem Information
     * @return                      The loop
     */
    static String createCellLoop(ProblemInfo pi) {
        String block = "";
        
        String actualIndent = IND;
        for (int k = pi.getDimensions() - 1; k >= 0; k--) {
            String coordString = pi.getCoordinates().get(k);
            //Loop iteration
            block =  block + actualIndent + "for(int " + coordString + " = 0; " 
                + coordString + " < " + coordString + "last; " + coordString + "++) {" + NL;
            actualIndent = actualIndent + IND;
        }
        
        return block;
    }
    
    /**
     * Create the initialization for the extrapolation block.
     * @param pi                The problem information
     * @param blockNumber       The number of the block
     * @param indent            The actual indent
     * @return                  The code
     * @throws CGException      CG00X External error
     */
    private static String extrapolationInitialization(ProblemInfo pi, int blockNumber, String indent) throws CGException {        
        String result = "";
        Document doc = pi.getProblem();
        String index = "";
        for (int i = 0; i < pi.getDimensions(); i++) {
            index = index + pi.getCoordinates().get(i) + ", ";
        }
        index = index.substring(0, index.length() - 2);
        
        
        ArrayList<String> fields = new ArrayList<String>();
        NodeList fieldList = CodeGeneratorUtils.find(pi.getProblem(), "/*/mms:fields/mms:field|/*/mms:auxiliaryFields/mms:auxiliaryField");
        for (int i = 0; i < fieldList.getLength(); i++) {
            fields.add(fieldList.item(i).getTextContent());
        }
        DocumentFragment blockScope = doc.createDocumentFragment();
        NodeList blockList = CodeGeneratorUtils.find(doc, "//mms:interiorExecutionFlow/sml:simml/*[" 
                + (blockNumber + 1) + "]|//mms:surfaceExecutionFlow/sml:simml/*[" 
                + (blockNumber + 1) + "]");
        for (int j = 0; j < blockList.getLength(); j++) {
            blockScope.appendChild(blockList.item(j).cloneNode(true));
        }
        ArrayList<String> variables = new ArrayList<String>();
        for (int i = 0; i < pi.getDimensions(); i++) {
            String contCoordString = pi.getContCoordinates().get(i);
            if (pi.getPeriodicalBoundary().get(contCoordString).equals("noperiodical")) {
                ArrayList<String> candidates = new ArrayList<String>();
                candidates.addAll(fields);
                for (int k = 0; k < candidates.size(); k++) {
                    String field = candidates.get(k);
                    if (!variables.contains(field)) {
                    	Element originalField;
                    	if (CodeGeneratorUtils.isAuxiliaryField(pi, SAMRAIUtils.variableNormalize(field))) {
                    		originalField = CodeGeneratorUtils.createElement(pi.getProblem(), MTURI, "ci");
                    		originalField.setTextContent(field);
                    	}
                    	else {
                    		originalField = CodeGeneratorUtils.getOriginalField(blockScope, fields, field, 
                    				pi.getExtrapolationType(), pi.getDimensions());	
                    	}
                        variables.add(field);
                        for (int l = 0; l < pi.getDimensions(); l++) {
                            String coord = pi.getCoordinates().get(l);
                            result = result + indent + IND + "vector(extrapolatedSB" 
                                    + SAMRAIUtils.variableNormalize(field) + "SP" + coord + ", " + index + ") = vector(" 
                                    + CodeGeneratorUtils.xmlVarToString(originalField, false, false) + ", " + index + ");" + NL;
                        }    
                    }
                }
            }
        }
        
        return result;
    }
    
    /**
     * Create the finalization instructions after an extrapolation & boundaries. After a boundary calculation the extrapolation axis variables
     * must be reassigned for the boundaries that does not calculate them. 
     * @param pi                    The problem info.
     * @param indent                The actual indent  
     * @param boundary              The boundary xml code
     * @param regionBaseNumber     The region base number
     * @return                      The code
     * @throws CGException          CG00X External error
     */
    private static String extrapolationFinalization(ProblemInfo pi, String indent, Node boundary, int regionBaseNumber) throws CGException {
        String result = "";
        String index = "";
        for (int i = 0; i < pi.getDimensions(); i++) {
            index = index + pi.getCoordinates().get(i) + ", ";
        }
        index = index.substring(0, index.length() - 2);
        
        ArrayList<String> fields = new ArrayList<String>();
        NodeList fieldList = CodeGeneratorUtils.find(pi.getProblem(), "/*/mms:fields/mms:field|/*/mms:auxiliaryFields/mms:auxiliaryField");
        for (int i = 0; i < fieldList.getLength(); i++) {
            fields.add(fieldList.item(i).getTextContent());
        }
       
        for (int i = 0; i < pi.getDimensions(); i++) {
            String coordString = pi.getCoordinates().get(i);
            String contCoordString = pi.getContCoordinates().get(i);
            if (pi.getPeriodicalBoundary().get(contCoordString).equals("noperiodical")) {
                ArrayList<String> candidates = new ArrayList<String>();
                candidates.addAll(fields);
                NodeList extrapolatedFields = CodeGeneratorUtils.find(boundary, 
                        ".//sml:axis[@axisAtt = '" + coordString + "']/sml:side[@sideAtt = 'lower']//sml:fieldExtrapolation/@fieldAtt");
                for (int j = 0; j < extrapolatedFields.getLength(); j++) {
                    candidates.remove(extrapolatedFields.item(j).getTextContent());
                }
                if (!candidates.isEmpty()) {
                    String axis = CodeGeneratorUtils.discToCont(pi.getProblem(), coordString);
                    int boundId = pi.getCoordinates().indexOf(CodeGeneratorUtils.contToDisc(pi.getProblem(), axis)) * 2 + 1;
                    result = result + indent + "if ((vector(FOV_" + CodeGeneratorUtils.getRegionName(pi.getProblem(), -boundId, 
                            pi.getCoordinates()) + ", " + index + ") > 0) && (" + SAMRAIUtils.getLeftCondition(false, 
                                    pi.getSchemaStencil(), regionBaseNumber, i, pi.getCoordinates(), pi.getRegionIds()) + ")) {" + NL;
                    for (int k = 0; k < candidates.size(); k++) {
                        String field = candidates.get(k);
                       	Element originalField;
                    	if (CodeGeneratorUtils.isAuxiliaryField(pi, SAMRAIUtils.variableNormalize(field))) {
                    		originalField = CodeGeneratorUtils.createElement(pi.getProblem(), MTURI, "ci");
                    		originalField.setTextContent(field);
                    	}
                    	else {
                    		originalField = CodeGeneratorUtils.getOriginalField(boundary, fields, field, 
                    				pi.getExtrapolationType(), pi.getDimensions());
                    	}
                        for (int l = 0; l < pi.getDimensions(); l++) {
                            String coord = pi.getCoordinates().get(l);
                            result = result + indent + IND + "vector(extrapolatedSB" 
                                    + SAMRAIUtils.variableNormalize(field) + "SP" + coord + ", " + index + ") = vector(" 
                                    + CodeGeneratorUtils.xmlVarToString(originalField, false, false) + ", " + index + ");" + NL;
                        }
                    }
                    result = result + indent + "}" + NL;
                }
                
                
                candidates = new ArrayList<String>();
                candidates.addAll(fields);
                extrapolatedFields = CodeGeneratorUtils.find(boundary, 
                        ".//sml:axis[@axisAtt = '" + coordString + "']/sml:side[@sideAtt = 'upper']//sml:fieldExtrapolation/@fieldAtt");
                for (int j = 0; j < extrapolatedFields.getLength(); j++) {
                    candidates.remove(extrapolatedFields.item(j).getTextContent());
                }
                if (!candidates.isEmpty()) {
                    String axis = CodeGeneratorUtils.discToCont(pi.getProblem(), coordString);
                    int boundId = pi.getCoordinates().indexOf(CodeGeneratorUtils.contToDisc(pi.getProblem(), axis)) * 2 + 2;
                    result = result + indent + "if ((vector(FOV_" + CodeGeneratorUtils.getRegionName(pi.getProblem(), -boundId, 
                            pi.getCoordinates()) + ", " + index + ") > 0) && (" + SAMRAIUtils.getRightCondition(false, 
                                    pi.getSchemaStencil(), regionBaseNumber, i, pi.getCoordinates(), pi.getRegionIds()) + ")) {" + NL;
                    for (int k = 0; k < candidates.size(); k++) {
                        String field = candidates.get(k);
                       	Element originalField;
                    	if (CodeGeneratorUtils.isAuxiliaryField(pi, SAMRAIUtils.variableNormalize(field))) {
                    		originalField = CodeGeneratorUtils.createElement(pi.getProblem(), MTURI, "ci");
                    		originalField.setTextContent(field);
                    	}
                    	else {
                    		originalField = CodeGeneratorUtils.getOriginalField(boundary, fields, field, 
                    				pi.getExtrapolationType(), pi.getDimensions());
                    	}
                        for (int l = 0; l < pi.getDimensions(); l++) {
                            String coord = pi.getCoordinates().get(l);
                            result = result + indent + IND + "vector(extrapolatedSB" 
                                    + SAMRAIUtils.variableNormalize(field) + "SP" + coord + ", " + index + ") = vector(" 
                                    + CodeGeneratorUtils.xmlVarToString(originalField, false, false) + ", " + index + ");" + NL;
                        }
                    }
                    result = result + indent + "}" + NL;
                }
            }
        }
        return result;
    }   
    
    /**
     * Creates the post refinement code.
     * @param pi			The problem information
     * @return				The code
     * @throws CGException	CG00X External error
     */
    public static String createPostNewLevelAlgorithm(ProblemInfo pi) throws CGException {
        String result = "";
        Document doc = pi.getProblem();
        
        int dimensions = pi.getSpatialDimensions();
        String currIndent = IND + IND;
        try {
            //Iterate over all the blocks
            int blocks = CodeGeneratorUtils.getBlocksNum(doc, "//mms:postRefinementAlgorithm//sml:simml");
            
            //Get scope for the declarations
            DocumentFragment scope = doc.createDocumentFragment();
            String query = "//mms:postRefinementAlgorithm//sml:simml/*";
            NodeList scopes = CodeGeneratorUtils.find(doc, query);
            for (int k = 0; k < scopes.getLength(); k++) {
                scope.appendChild(scopes.item(k).cloneNode(true));
            }   
            
            if (pi.isHasParticleFields()) {
            	result = IND + "const int ln = level->getLevelNumber();" + NL
            			+ IND + "const double level_ratio = level->getRatioToCoarserLevel().max();" + NL
            			+ IND + "double level_influenceRadius = influenceRadius/MAX(1,level_ratio);" + NL
            			+ IND + "double volume_level_factor = 1.0 / MAX(1, level->getRatioToLevelZero().getProduct());" + NL;
            }
            
            String commonVariables = SAMRAIUtils.createFOVDeclaration(pi.getRegionIds(), IND + IND, "patch->")
            	+ SAMRAIUtils.getPDEVariableDeclaration(scope, pi, 2, new ArrayList<String>(), "patch->", false, false) + NL;
	        for (String speciesName : pi.getParticleSpecies()) {
	        	commonVariables = commonVariables + IND + IND  + "std::shared_ptr< pdat::IndexData<Particles<Particle_" + speciesName + ">, pdat::CellGeometry > > particleVariables_" + speciesName + "(SAMRAI_SHARED_PTR_CAST<pdat::IndexData< Particles<Particle_" + speciesName + ">, pdat::CellGeometry >, hier::PatchData>(patch->getPatchData(d_particleVariables_" + speciesName + "_id)));" + NL;
	    	}
            commonVariables = commonVariables + IND + IND + "//Get the dimensions of the patch" + NL
                + IND + IND + "const hier::Index boxfirst = patch->getBox().lower();" + NL
                + IND + IND + "const hier::Index boxlast = patch->getBox().upper();" + NL
                + IND + IND + "//Get delta spaces into an array. dx, dy, dz." + NL
                + IND + IND + "std::shared_ptr<geom::CartesianPatchGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianPatchGeometry,"
                		+ " hier::PatchGeometry>(patch->getPatchGeometry()));" + NL
                + IND + IND + "const double* dx  = patch_geom->getDx();" + NL
                + IND + IND + "const double simPlat_dt = patch->getPatchData(d_FOV_" + pi.getRegionIds().get(0) + "_id)->getTime();" + NL + NL
                + IND + IND + "//Auxiliary definitions" + NL;
            String executionBlockLoop = CodeGeneratorUtils.incrementIndent(createCellLoop(pi), 1);
            
            //Initialization of the variables for iteration
            for (int k = 0; k < dimensions; k++) {
                //Initialization of the variables
                String coordString = pi.getCoordinates().get(k);
                commonVariables =  commonVariables + currIndent + "int " + coordString + "last = boxlast(" + k + ")-boxfirst(" 
                        + k + ") + 2 + 2 * d_ghost_width;" + NL;
            }
            if (pi.isHasParticleFields()) {
            	commonVariables = commonVariables + currIndent + "const hier::Index boxfirstP = particleVariables_Prova->getGhostBox().lower();" + NL
            			+ currIndent + "const hier::Index boxlastP = particleVariables_Prova->getGhostBox().upper();" + NL
            			+ currIndent + "double* position = new double[3];" + NL;
            }
            
            //Iterate over the blocks
            for (int i = 0; i < blocks; i++) {
                String block = "";
                block = IND + "for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {" + NL
                		+ IND + IND + "const std::shared_ptr< hier::Patch >& patch = *p_it;" + NL
                		+ commonVariables;
                currIndent = IND + IND;
                for (int j = 0; j < dimensions; j++) {
                    currIndent = IND + currIndent;
                }                        
                block = block + executionBlockLoop;

                String region = "";
                for (int j = pi.getRegions().size() - 1; j >= 0; j--) {
                    String regionName = pi.getRegions().get(j);
                    //Interior execution flow
                    region = region + regionPostRefinementAlgorithm(regionName, currIndent, i, j, "interior", pi, false);
                    //Surface execution flow
                    region = region + regionPostRefinementAlgorithm(regionName, currIndent, i, j, "surface", pi, false);
                }
                
                block = block + region;
                
                //close the loop iteration
                block = block + SAMRAIUtils.createCellLoopEnd(currIndent, dimensions)
                	+ IND + "}" + NL;
                
                for (int j = 0; j < dimensions; j++) {
                    currIndent = currIndent.substring(0, currIndent.lastIndexOf(IND));
                }
                //Close loop 
                if (!region.equals("")) {
                    result = result + block;
                }
            }
   
            return "/* " + NL
            		+ " * Calculation of auxiliary terms after a new level is regridded." + NL
            		+ " */" + NL
            		+ "void Problem::postNewLevel(const std::shared_ptr< hier::PatchLevel >& level) {" + NL
            		+ result
            		+ "}";
        } 
        catch (CGException e) {
            e.printStackTrace(System.out);
            throw e;
        }
        catch (Exception e) {
        	e.printStackTrace(System.out);
            throw new CGException(CGException.CG00X, e.getMessage());
        }
    }
    
    /**
     * Generates the execution flow code for a region.
     * @param pi                 	The problem common information
     * @param regionName         	The region name
     * @param actualIndent       	The actual indent 
     * @param blockNum           	The block number
     * @param regionNum          	The region number
     * @param regionType         	The region type (interior or surface)
     * @param orderBoundaries    	true if order boundaries are used
     * @return                   	The code
     * @throws CGException       	CG00X External error
     */
    private static String regionPostRefinementAlgorithm(String regionName, String actualIndent, int blockNum, 
            int regionNum, String regionType, ProblemInfo pi, boolean orderBoundaries) throws CGException {
        Document doc = pi.getProblem();
        String result = "";
        String[][] xslParams = new String [THREE][2];
        xslParams[0][0] =  "condition";
        xslParams[0][1] =  "false";
        xslParams[1][0] =  "indent";
        xslParams[2][0] =  "dimensions";
        xslParams[2][1] =  String.valueOf(pi.getDimensions());
        
        //Get current id
        int regionId = (regionNum * 2) + 1;
        if (regionType.equals("surface")) {
            regionId++;
        }
        pi.setCurrentRegionId(regionId);
        
        int dimensions = pi.getSpatialDimensions();
        //Generate the index for the coordinates
        String index = "";
        for (int i = 0; i < dimensions; i++) {
            index =  index + pi.getCoordinates().get(i) + ", ";
        }
        index = index.substring(0, index.lastIndexOf(","));
        //Region execution flow
        NodeList blockList = CodeGeneratorUtils.find(doc, "//mms:postRefinementAlgorithm[preceding-sibling::" 
                + "mms:name = '" + regionName + "']/mms:" + regionType + "Algorithm/sml:simml/*[" + (blockNum + 1) + "]");
        for (int i = 0; i < blockList.getLength(); i++) {
            Element block = (Element) blockList.item(i).cloneNode(true);
            /*if (pi.getMovementInfo().itMoves()) {
                block = (Element) SAMRAIUtils.preProcessFVMBlockMove(block, pi);  
            }*/
            block = (Element) SAMRAIUtils.preProcessPDEBlock(block, pi, false, false);
            if (block.hasChildNodes()) {
                //Process a time step
                if (block.getLocalName().equals("iterateOverCells") && block.hasChildNodes()) {
                    
                    //Check that the stencil points needed by the method are available
                    String appendAtt = block.getAttribute("appendAtt");
                    int append = 0;
                    if (!appendAtt.equals("")) {
                        append = Integer.parseInt(appendAtt);
                    }
          
                    //Add condition to calculate only inside of the array limits when there are stencil
                    String insideLimitsCondition = "";
                    int stencil = Integer.parseInt(block.getAttribute("stencilAtt"));
                    if (stencil > 0) {
                        insideLimitsCondition = " && " + getLimitCondition(pi.getCoordinates(), stencil);
                    }

                    //Condition to calculate region interactions
                    String regionInteractionCondition = "";
                    if (append > 0) {
                        boolean calculateCorners = block.getAttribute("cornerCalculationAtt").equals("true") || CodeGeneratorUtils.find(block, 
                                ".//*[@sml:type='parabolicTerm' or @type='parabolicTerm']").getLength() > 0;
                        regionInteractionCondition = getCondition(pi.getRegionRelations().get(regionName), 
                                pi.getCoordinates(), append, regionId, calculateCorners);
                    }
                    result = result + actualIndent + "if ((vector(FOV_" + regionId + ", " + index + ") > 0" 
                        + regionInteractionCondition + ")" + insideLimitsCondition + ") {" + NL;
                    actualIndent = actualIndent + IND;
                    if (logservice != null) {
                        logservice.log(LogService.LOG_INFO, "Processing instructions");
                    }
                    //Instruction to code
                    xslParams[1][1] = String.valueOf(actualIndent.length());
                    result = result + SAMRAIUtils.xmlTransform(block, xslParams);
                    actualIndent = actualIndent.substring(1);
                    result = result + actualIndent + "}" + NL;
                }
            }
        }
        return result;
    }
   
    /**
     * Checks if the instruction is a particle movement.
     * @param instructionSet        The instruction set
     * @return                      True if contains an assignment
     * @throws CGException          CG004 External error
     */
    private static boolean isMovement(Node instructionSet) throws CGException {
        NodeList leftTerms = CodeGeneratorUtils.find(instructionSet, "./descendant-or-self::mt:apply/*[1][local-name() = 'ci' and starts-with(text(), 'moveParticles')]");
        return leftTerms.getLength() > 0;
    }
    
    /**
     * Checks if the instruction set has any field assignment.
     * @param instructionSet        The instruction set
     * @param fields                The fields of the problem
     * @return                      True if contains an assignment
     * @throws CGException          CG004 External error
     */
    private static boolean fieldAssignment(Node instructionSet, ArrayList<String> fields) throws CGException {
        NodeList leftTerms = CodeGeneratorUtils.find(instructionSet, 
                ".//mt:math[(not(parent::sml:if) or parent::sml:then) and (not(parent::sml:while) "
                + "or parent::sml:loop)]/mt:apply/mt:eq/following-sibling::*[1][local-name() = 'apply']/sml:particleAccess/following-sibling::*[1]|./mt:apply/mt:eq/following-sibling::*[1][local-name() = 'apply']/sml:particleAccess/following-sibling::*[1]");
        for (int i = 0; i < leftTerms.getLength(); i++) {
            String variable = new String(SAMRAIUtils.xmlTransform(leftTerms.item(i), null));
            if (fields.contains(variable)) {
                return true;
            }   
        }
        return false;
    }
    
    /**
     * Checks if the instruction set has any position assignment.
     * @param instructionSet        The instruction set
     * @param positions             The position variables of the problem
     * @return                      True if contains an assignment
     * @throws CGException          CG004 External error
     */
    private static boolean positionAssignment(Node instructionSet, Set<String> positions) throws CGException {
        NodeList leftTerms = CodeGeneratorUtils.find(instructionSet, 
                ".//mt:math[(not(parent::sml:if) or parent::sml:then) and (not(parent::sml:while) "
                + "or parent::sml:loop)]/mt:apply/mt:eq/following-sibling::*[1][local-name() = 'apply']/sml:particleAccess/following-sibling::*[1]|./mt:apply/mt:eq/following-sibling::*[1][local-name() = 'apply']/sml:particleAccess/following-sibling::*[1]");
        for (int i = 0; i < leftTerms.getLength(); i++) {
            String variable = new String(SAMRAIUtils.xmlTransform(leftTerms.item(i), null));
            if (positions.contains(variable)) {
            	return true;
            }
        }
        return false;
    }
    
    /**
     * Checks if the instruction set has any shared assignment.
     * @param instructionSet        The instruction set
     * @param variables             The variables of the problem
     * @return                      True if contains an assignment
     * @throws CGException          CG004 External error
     */
    private static boolean sharedAssignment(Node instructionSet, ArrayList<String> variables) throws CGException {
        NodeList leftTerms = CodeGeneratorUtils.find(instructionSet, 
                ".//mt:math[(not(parent::sml:if) or parent::sml:then) and (not(parent::sml:while) "
                + "or parent::sml:loop)]/mt:apply/mt:eq/following-sibling::*[1][local-name() = 'sharedVariable']/mt:apply/sml:particleAccess/following-sibling::*[1]|" 
                + "./mt:apply/mt:eq/following-sibling::*[1][local-name() = 'sharedVariable']/mt:apply/sml:particleAccess/following-sibling::*[1]");
        for (int i = 0; i < leftTerms.getLength(); i++) {
            String variable = new String(SAMRAIUtils.xmlTransform(leftTerms.item(i), null));
            if (variables.contains(variable)) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Process the boundary for particles.
     * @param currentIndent                     The current indent
     * @param indexCell                         The index cell
     * @param pi                                The process information
     * @param regionBaseNumber                  The region base number
     * @param coordBoundary                     The element with the boundary
     * @param coord                   			Boundary coordinate
     * @param side								Boundary side
     * @return                                  The code of the boundary
     * @throws CGException                      CG004 External error
     */
    private static String processBoundaryParticles(String currentIndent, String indexCell, ProblemInfo pi, 
            int regionBaseNumber, Element coordBoundary, String coord, String side, String speciesName) throws CGException {
        String[][] xslParams = new String [THREE][2];
        xslParams[0][0] =  "condition";
        xslParams[0][1] =  "false";
        xslParams[1][0] =  "indent";
        xslParams[1][1] =  "0";
        xslParams[2][0] =  "dimensions";
        xslParams[2][1] =  String.valueOf(pi.getDimensions());
        String boundariesResult = "";
        try {
        	String bound = "";
            String axis = CodeGeneratorUtils.discToCont(pi.getProblem(), coord);
            int boundId = pi.getCoordinates().indexOf(CodeGeneratorUtils.contToDisc(pi.getProblem(), axis)) * 2 + 1;
            if (side.equals("upper")) {
            	boundId++;
            }
            String condition = currentIndent + "if (particle->region == -" + boundId + ") {" + NL;
            NodeList boundList = CodeGeneratorUtils.find(coordBoundary, "./sml:side[@sideAtt = '" + side + "']/sml:iterateOverParticles[@speciesNameAtt = '" + speciesName + "']");
            for (int k = 0; k < boundList.getLength(); k++) {
                Element sideBoundary = (Element) boundList.item(k).cloneNode(true);
                boolean reflection = CodeGeneratorUtils.find(sideBoundary, ".//mt:math[@sml:type = 'reflection']").getLength() > 0;
                if (!reflection) {
                    if (sideBoundary.hasChildNodes()) {
                        xslParams[1][1] =  String.valueOf(currentIndent.length() + 1);
                        
                        NodeList instructions = sideBoundary.getChildNodes();
                        for (int l = 0; l < instructions.getLength(); l++) {
                            Element instruction = (Element) instructions.item(l);
                            bound = bound + SAMRAIUtils.xmlTransform(instruction, xslParams);
                        }
                    }
                }
                if (!bound.equals("")) {
                    bound = condition + bound + currentIndent + "}" + NL;
                } 
            }
            if (!bound.equals("")) {
                boundariesResult = boundariesResult + bound;
            }

            return boundariesResult;
        } 
        catch (Exception e) {
            e.printStackTrace();
            throw new CGException(CGException.CG00X, e.getMessage());
        }
    }
}
