/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.codeGeneratorPlugin.utils;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class FileReadInformation {
	//Table name <name, dimensions>
	private HashMap<String, Integer> fileNameInfo;
	//Number of quintic reads <number of table dimensions, list of number of variables to output>
	private HashMap<Integer, Set<Integer>> numberQuinticReads;
	//Number of linear reads <list of number of variables to output>
	private Set<Integer> linearRead1D;
	private Set<Integer> linearRead2D;
	private Set<Integer> linearRead3D;
	//Number of linear reads <list of number of variables to output>
	private Set<Integer> linearReadWithDerivatives1D;
	private Set<Integer> linearReadWithDerivatives2D;
	private Set<Integer> linearReadWithDerivatives3D;
	//Number of reads coordinate from data <number of dimension output>
	private Set<Integer> coordReads1D;
	private Set<Integer> coordReads2D;
	private Set<Integer> coordReads3D;
	
	public FileReadInformation() {
		
		fileNameInfo = new HashMap<String, Integer>();
		
		numberQuinticReads = new HashMap<Integer, Set<Integer>>();
		
		linearRead1D = new HashSet<Integer>();
		linearRead2D = new HashSet<Integer>();
		linearRead3D = new HashSet<Integer>();
		
		linearReadWithDerivatives1D = new HashSet<Integer>();
		linearReadWithDerivatives2D = new HashSet<Integer>();
		linearReadWithDerivatives3D = new HashSet<Integer>();
		
		coordReads1D = new HashSet<Integer>();
		coordReads2D = new HashSet<Integer>();
		coordReads3D = new HashSet<Integer>();
	}
	
	public void addInstruction(String instructionType, String fileName, int tableDimensions, int nOutputVars, int ndimRead) {
		fileNameInfo.put(fileName, tableDimensions);
		if (instructionType.equals("readFromFileQuintic")) {
			if (numberQuinticReads.containsKey(tableDimensions)) {
				Set<Integer> nOutputList = numberQuinticReads.get(tableDimensions);
				nOutputList.add(nOutputVars);
				numberQuinticReads.put(tableDimensions, nOutputList);	
			}
			else {
				Set<Integer> nOutputList = new HashSet<Integer>();
				nOutputList.add(nOutputVars);
				numberQuinticReads.put(tableDimensions, nOutputList);
			}
		}
		else if (instructionType.equals("readFromFileLinear1D")) {
			linearRead1D.add(nOutputVars);
		}
		else if (instructionType.equals("readFromFileLinear2D")) {
			linearRead2D.add(nOutputVars);
		}
		else if (instructionType.equals("readFromFileLinear3D")) {
			linearRead3D.add(nOutputVars);
		}
		else if (instructionType.equals("readFromFileLinearWithDerivatives1D")) {
			linearReadWithDerivatives1D.add(nOutputVars);
		}
		else if (instructionType.equals("readFromFileLinearWithDerivatives2D")) {
			linearReadWithDerivatives2D.add(nOutputVars);
		}
		else if (instructionType.equals("readFromFileLinearWithDerivatives3D")) {
			linearReadWithDerivatives3D.add(nOutputVars);
		}
		else if (instructionType.equals("findCoordFromFile1D")) {
			linearRead1D.add(1);
			coordReads1D.add(ndimRead);
		}
		else if (instructionType.equals("findCoordFromFile2D")) {
			linearRead2D.add(1);
			coordReads2D.add(ndimRead);
		}
		else if (instructionType.equals("findCoordFromFile3D")) {
			linearRead3D.add(1);
			coordReads3D.add(ndimRead);
		}		
	}

	public HashMap<String, Integer> getFileNameInfo() {
		return fileNameInfo;
	}

	public void setFileNameInfo(HashMap<String, Integer> fileNameInfo) {
		this.fileNameInfo = fileNameInfo;
	}

	public HashMap<Integer, Set<Integer>> getNumberQuinticReads() {
		return numberQuinticReads;
	}

	public void setNumberQuinticReads(HashMap<Integer, Set<Integer>> numberQuinticReads) {
		this.numberQuinticReads = numberQuinticReads;
	}

	public Set<Integer> getLinearRead1D() {
		return linearRead1D;
	}

	public void setLinearRead1D(Set<Integer> linearRead1D) {
		this.linearRead1D = linearRead1D;
	}

	public Set<Integer> getLinearRead2D() {
		return linearRead2D;
	}

	public void setLinearRead2D(Set<Integer> linearRead2D) {
		this.linearRead2D = linearRead2D;
	}

	public Set<Integer> getLinearRead3D() {
		return linearRead3D;
	}

	public void setLinearRead3D(Set<Integer> linearRead3D) {
		this.linearRead3D = linearRead3D;
	}

	public Set<Integer> getCoordReads1D() {
		return coordReads1D;
	}

	public void setCoordReads1D(Set<Integer> coordReads1D) {
		this.coordReads1D = coordReads1D;
	}

	public Set<Integer> getCoordReads2D() {
		return coordReads2D;
	}

	public void setCoordReads2D(Set<Integer> coordReads2D) {
		this.coordReads2D = coordReads2D;
	}

	public Set<Integer> getCoordReads3D() {
		return coordReads3D;
	}

	public void setCoordReads3D(Set<Integer> coordReads3D) {
		this.coordReads3D = coordReads3D;
	}

	public Set<Integer> getLinearReadWithDerivatives1D() {
		return linearReadWithDerivatives1D;
	}

	public void setLinearReadWithDerivatives1D(Set<Integer> linearReadWithDerivatives1D) {
		this.linearReadWithDerivatives1D = linearReadWithDerivatives1D;
	}

	public Set<Integer> getLinearReadWithDerivatives2D() {
		return linearReadWithDerivatives2D;
	}

	public void setLinearReadWithDerivatives2D(Set<Integer> linearReadWithDerivatives2D) {
		this.linearReadWithDerivatives2D = linearReadWithDerivatives2D;
	}

	public Set<Integer> getLinearReadWithDerivatives3D() {
		return linearReadWithDerivatives3D;
	}

	public void setLinearReadWithDerivatives3D(Set<Integer> linearReadWithDerivatives3D) {
		this.linearReadWithDerivatives3D = linearReadWithDerivatives3D;
	}

	@Override
	public String toString() {
		return "FileReadInformation [fileNameInfo=" + fileNameInfo + ", numberQuinticReads=" + numberQuinticReads
				+ ", linearRead1D=" + linearRead1D + ", linearRead2D=" + linearRead2D + ", linearRead3D=" + linearRead3D
				+ ", linearReadWithDerivatives1D=" + linearReadWithDerivatives1D + ", linearReadWithDerivatives2D="
				+ linearReadWithDerivatives2D + ", linearReadWithDerivatives3D=" + linearReadWithDerivatives3D
				+ ", coordReads1D=" + coordReads1D + ", coordReads2D=" + coordReads2D + ", coordReads3D=" + coordReads3D
				+ "]";
	}
	
	
}
