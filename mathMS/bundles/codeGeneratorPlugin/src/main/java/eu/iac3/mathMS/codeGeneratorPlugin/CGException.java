/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.codeGeneratorPlugin;

/** 
 * CGException is the class Exception launched by the 
 * {@link CodeGenerator} interface.
 * The exceptions that this class return are:
 * CG001 XML document is not valid
 * CG002 Code already exist
 * CG003 Not possible to create code for the platform
 * CG00X External error
 * @author      ----
 * @version     ----
 */
@SuppressWarnings("serial")
public class CGException extends Exception {

    public static final String CG001 = "XML document is not valid";
    public static final String CG002 = "Code with the same name, author and version already exists. "
            + "Change any of those fields to generate a new copy or delete the existing one.";
    public static final String CG003 = "Not possible to create code for the platform";
    public static final String CG004 = "Incorrect source code";
    public static final String CG00X = "External error";
	
    //External error message
    private String extMsg;
	  /** 
	     * Constructor for known messages.
	     *
	     * @param code  the message	     
	     */
    CGException(String code) {
    	super(code);
    }
	  
	  /** 
	     * Constructor.
	     *
	     * @param code  the internal error message	    
	     * @param msg  the external error message
	     */
    public CGException(String code, String msg) {
		super(code);
        this.extMsg = msg;
    }
	  /** 
     * Returns the error code.
     *
     * @return the error code	     
     */
    public String getErrorCode() {
    	return super.getMessage();
    }
	  /** 
	     * Returns the error message as a string.
	     *
	     * @return the error message	     
	     */
    public String getMessage() {
    	if (extMsg == null) {
            return "CGException: " + super.getMessage();
    	}
		return "CGException: " + super.getMessage() + " (" + extMsg + ")";
    }  
}
