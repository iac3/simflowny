#include "Problem.h"
#include "Functions.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <stack>
#include "hdf5.h"
#includes#

#include "SAMRAI/tbox/Array.h"
#include "SAMRAI/hier/BoundaryBox.h"
#include "SAMRAI/hier/BoxContainer.h"
#include "SAMRAI/geom/CartesianPatchGeometry.h"
#include "SAMRAI/hier/VariableDatabase.h"
#include "SAMRAI/hier/PatchDataRestartManager.h"
#include "SAMRAI/hier/Index.h"
#include "SAMRAI/tbox/PIO.h"
#include "SAMRAI/tbox/Utilities.h"
#include "SAMRAI/tbox/Timer.h"
#include "SAMRAI/tbox/TimerManager.h"

#define MIN(X,Y) ((X) < (Y) ? (X) : (Y))
#define MAX(X,Y) ((X) > (Y) ? (X) : (Y))
#define SIGN(X) (((X) > 0) - ((X) < 0))
#define isEven(a) ((a) % 2 == 0 ? true : false)
#define greaterEq(a,b) ((fabs((a) - (b))/1.0E-15 > 10 ? false : (floor(fabs((a) - (b))/1.0E-15) < 1)) || (b)<(a))
#define lessEq(a,b) ((fabs((a) - (b))/1.0E-15 > 10 ? false: (floor(fabs((a) - (b))/1.0E-15) < 1)) || (a)<(b))
#define equalsEq(a,b) ((fabs((a) - (b))/1.0E-15 > 10 ? false: (floor(fabs((a) - (b))/1.0E-15) < 1)))
#define reducePrecision(x, p) (floor(((x) * pow(10, (p)) + 0.5)) / pow(10, (p)))

using namespace external;

#global variables#
//Timers
std::shared_ptr<tbox::Timer> t_step;
std::shared_ptr<tbox::Timer> t_moveParticles;
std::shared_ptr<tbox::Timer> t_output;

inline int Problem::GetExpoBase2(double d)
{
	int i = 0;
	((short *)(&i))[0] = (((short *)(&d))[3] & (short)32752); // _123456789ab____ & 0111111111110000
	return (i >> 4) - 1023;
}

bool	Problem::Equals(double d1, double d2)
{
	if (d1 == d2)
		return true;
	int e1 = GetExpoBase2(d1);
	int e2 = GetExpoBase2(d2);
	int e3 = GetExpoBase2(d1 - d2);
	if ((e3 - e2 < -48) && (e3 - e1 < -48))
		return true;
	return false;
}

/*
 * Constructor of the problem.
 */
Problem::Problem(const string& object_name, const tbox::Dimension& dim, std::shared_ptr<tbox::Database>& database, std::shared_ptr<geom::CartesianGridGeometry >& grid_geom, std::shared_ptr<hier::PatchHierarchy >& patch_hierarchy, MainRestartData& mrd, const double dt, const bool init_from_restart, const int console_output, const int timer_output#writerVariablesParam##sliceVariablesParam##integrationVariablesParam#): 
d_dim(dim), xfer::RefinePatchStrategy(), xfer::CoarsenPatchStrategy()#DataWriterVariable#, d_output_interval(console_output), d_timer_output_interval(timer_output)#sliceVariablesInitializationInline##integrationVariablesInitializationInline#
{
	//Setup the timers
	t_step = tbox::TimerManager::getManager()->getTimer("Step");
	t_moveParticles = tbox::TimerManager::getManager()->getTimer("Move particles");
	t_output = tbox::TimerManager::getManager()->getTimer("OutputGeneration");

	//Output configuration
	next_console_output = d_output_interval;
	next_timer_output = d_timer_output_interval;

	//Get the object name, the grid geometry and the patch hierarchy
  	d_grid_geometry = grid_geom;
	d_patch_hierarchy = patch_hierarchy;
	d_object_name = object_name;
	d_init_from_restart = init_from_restart;
	initial_dt = dt;

#sliceVariablesInitialization#
#integrationVariablesInitialization#

	//Get parameters
    cout<<"Reading parameters"<<endl;
#parameter declaration#

	//External eos parameters
#ifdef EXTERNAL_EOS

	std::shared_ptr<tbox::Database> external_eos_db = database->getDatabase("external_EOS");
	Commons::ExternalEos::reprimand_eos_type = external_eos_db->getInteger("eos_type");
	Commons::ExternalEos::reprimand_atmo_Ye = external_eos_db->getDouble("atmo_Ye");
	Commons::ExternalEos::reprimand_max_z = external_eos_db->getDouble("max_z");
	Commons::ExternalEos::reprimand_max_b = external_eos_db->getDouble("max_b");
	Commons::ExternalEos::reprimand_c2p_acc = external_eos_db->getDouble("c2p_acc");
	Commons::ExternalEos::reprimand_atmo_rho = external_eos_db->getDouble("atmo_rho");
	Commons::ExternalEos::reprimand_rho_strict = external_eos_db->getDouble("rho_strict");
	Commons::ExternalEos::reprimand_max_rho = external_eos_db->getDouble("max_rho");
	Commons::ExternalEos::reprimand_max_eps = external_eos_db->getDouble("max_eps");
	Commons::ExternalEos::reprimand_gamma_th = external_eos_db->getDouble("gamma_th");
#endif

    	//Subcycling
#subcycling#

#regridding#
	//Stencil of the discretization method
#d_ghost_width#
	
	//Register Fields and temporal fields into the variable database
	hier::VariableDatabase *vdb = hier::VariableDatabase::getDatabase();
  	std::shared_ptr<hier::VariableContext> d_cont_curr(vdb->getContext("Current"));
#variableDeclaration and register#

	//Refine and coarse algorithms
#fill variables#
	d_mapping_fill = std::shared_ptr< xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());
    d_tagging_fill = std::shared_ptr< xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());
	d_fill_new_level    = std::shared_ptr< xfer::RefineAlgorithm >(new xfer::RefineAlgorithm());
#fillNewLevelAux#

	//mapping communication
#refineOperators#
#mappingCommunication#

    d_tagging_fill->registerRefine(d_nonSync_regridding_tag_id,d_nonSync_regridding_tag_id,d_nonSync_regridding_tag_id, d_grid_geometry->lookupRefineOperator(nonSync, "NO_REFINE"));

	//refine and coarsen operators
#refine coarsen operators#

	//Register variables to the refineAlgorithm for boundaries
#refine boundaries register#

	//Register variables to the refineAlgorithm for filling new levels on regridding
#refine new levels register#

	//Register variables to the coarsenAlgorithm
#coarsen boundaries register#
#readTables#

    Commons::initialization();
}

/*
 * Destructor.
 */
Problem::~Problem() 
{
} 

/*
 * Initialize the data from a given level.
 */
void Problem::initializeLevelData (
   const std::shared_ptr<hier::PatchHierarchy >& hierarchy , 
   const int level_number ,
   const double init_data_time ,
   const bool can_be_refined ,
   const bool initial_time ,
   const std::shared_ptr<hier::PatchLevel >& old_level ,
   const bool allocate_data )
{
    cout<<"Initializing level "<<level_number<<endl;
    tbox::MemoryUtilities::printMemoryInfo(cout);   
	std::shared_ptr< hier::PatchLevel > level(hierarchy->getPatchLevel(level_number));

	// Allocate storage needed to initialize level and fill data from coarser levels in AMR hierarchy.  
	level->allocatePatchData(d_interior_regridding_value_id, init_data_time);
	level->allocatePatchData(d_nonSync_regridding_tag_id, init_data_time);
#allocate variables#

	//Mapping the current data for new level.
#mapDataCall#
#finerLevelMapping#

#fillBoundaries#

#interphaseMappingUse#

	//Initialize current data for new level.
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr< hier::Patch > patch = *p_it;
		if (initial_time) {
  		    initializeDataOnPatch(*patch, init_data_time, initial_time);
		}

	}
	//Post-initialization Sync.
    	if (initial_time || level_number == 0) {
#postInitSync#
    	}
#deallocate variables#
    cout<<"Level "<<level_number<<" initialized"<<endl;
    tbox::MemoryUtilities::printMemoryInfo(cout);
}

#analysisInitialization#

void Problem::initializeLevelIntegrator(
   const std::shared_ptr<mesh::GriddingAlgorithmStrategy>& gridding_alg)
{
}

double Problem::getLevelDt(
   const std::shared_ptr<hier::PatchLevel>& level,
   const double dt_time,
   const bool initial_time)
{
  
   TBOX_ASSERT(level);

   if (level->getLevelNumber() == 0) return initial_dt;

    double dt = initial_dt;
    const hier::IntVector ratio = level->getRatioToLevelZero();
    double local_dt = dt;
    for (int i = 0; i < 2; i++) {
        if (local_dt > dt / ratio[i]) {
            local_dt = dt / ratio[i];
        }
    }
    return local_dt;
}

double Problem::getMaxFinerLevelDt(
   const int finer_level_number,
   const double coarse_dt,
   const hier::IntVector& ratio)
{
   NULL_USE(finer_level_number);

   TBOX_ASSERT(ratio.min() > 0);

   return coarse_dt / double(ratio.max());
}

void Problem::standardLevelSynchronization(
   const std::shared_ptr<hier::PatchHierarchy>& hierarchy,
   const int coarsest_level,
   const int finest_level,
   const double sync_time,
   const std::vector<double>& old_times)
{

}

void Problem::synchronizeNewLevels(
   const std::shared_ptr<hier::PatchHierarchy>& hierarchy,
   const int coarsest_level,
   const int finest_level,
   const double sync_time,
   const bool initial_time)
{
#derivativeAuxFieldSyncrhonization#
    //Not needed, but not absolutely sure
    /*for (int fine_ln = finest_level; fine_ln > coarsest_level; --fine_ln) {
        const int coarse_ln = fine_ln - 1;
        std::shared_ptr<hier::PatchLevel> fine_level(hierarchy->getPatchLevel(fine_ln));
        d_bdry_fill_init->createSchedule(fine_level, coarse_ln, hierarchy, this)->fillData(sync_time, true);
    }*/
}

void Problem::resetTimeDependentData(
   const std::shared_ptr<hier::PatchLevel>& level,
   const double new_time,
   const bool can_be_refined)
{
   TBOX_ASSERT(level);
   level->setTime(new_time);
}

void Problem::resetDataToPreadvanceState(
   const std::shared_ptr<hier::PatchLevel>& level)
{
    //cout<<"resetDataToPreadvanceState"<<endl;
}

/*
 * Map data on a patch. This mapping is done only at the begining of the simulation.
 */
void Problem::mapDataOnPatch(#mapDataArgs#)
{
	(void) time;
	const tbox::SAMRAI_MPI& mpi(tbox::SAMRAI_MPI::getSAMRAIWorld());
   	if (initial_time || ln == 0) {

		// Mapping		
#mapping#
   	}
}

#mappingFunctions#

#interphaseMappingFunction#

#interphaseMovementFunction#

/*
 * Initialize data on a patch. This initialization is done only at the begining of the simulation.
 */
void Problem::initializeDataOnPatch(hier::Patch& patch, 
                                    const double time,
                                    const bool initial_time)
{
	(void) time;
   	if (initial_time) {
		// Initial conditions		
#initial conditions#
   	}
}



/*
 * Gets the coarser patch that contains the box
 */
const std::shared_ptr<hier::Patch >& Problem::getCoarserPatch(
	const std::shared_ptr< hier::PatchLevel >& level,
	const hier::Box interior, 
	const hier::IntVector ratio)
{
	const hier::Box& coarsenBox = hier::Box::coarsen(interior, ratio);
	for (hier::PatchLevel::iterator p_it(level->begin()); p_it != level->end(); ++p_it) {
		const std::shared_ptr< hier::Patch >& patch = *p_it;
		const hier::Box& interior_C = patch->getBox();
		if (interior_C.intersects(coarsenBox)) {
			return patch;		
		}
	}
    return NULL;
}

/*
 * Reset the hierarchy-dependent internal information.
 */
void Problem::resetHierarchyConfiguration (
   const std::shared_ptr<hier::PatchHierarchy >& new_hierarchy ,
   int coarsest_level ,
   int finest_level )
{
	int finest_hiera_level = new_hierarchy->getFinestLevelNumber();

   	//  If we have added or removed a level, resize the schedule arrays
#resetHierarchyConfiguration#
}

#postNewLevelFunction#

/*
 * This method sets the physical boundary conditions.
 */
void Problem::setPhysicalBoundaryConditions(
   hier::Patch& patch,
   const double fill_time,
   const hier::IntVector& ghost_width_to_fill)
{
	//Boundary must not be implemented in this method
}

#setupSimflownyPlotters#

/*
 * Perform a single step from the discretization schema algorithm   
 */
double Problem::advanceLevel(
   const std::shared_ptr<hier::PatchLevel>& level,
   const std::shared_ptr<hier::PatchHierarchy>& hierarchy,
   const double current_time,
   const double new_time,
   const bool first_step,
   const bool last_step,
   const bool regrid_advance)
{
	const tbox::SAMRAI_MPI& mpi(tbox::SAMRAI_MPI::getSAMRAIWorld());

	const int ln = level->getLevelNumber();
	const double simPlat_dt = new_time - current_time;
	const double level_ratio = level->getRatioToCoarserLevel().max();
	if (first_step) {
		bo_substep_iteration[ln] = 0;
	}
	else {
		bo_substep_iteration[ln] = bo_substep_iteration[ln] + 1;
	}
#level_influenceRadius#
	if (d_refinedTimeStepping && first_step && ln > 0) {
		current_iteration[ln] = (current_iteration[ln - 1] - 1) * hierarchy->getRatioToCoarserLevel(ln).max() + 1;
	} else {
		current_iteration[ln] = current_iteration[ln] + 1;
	}
	int previous_iteration = current_iteration[ln] - 1;
	int outputCycle = current_iteration[ln];
	int maxLevels = hierarchy->getMaxNumberOfLevels();
	if (maxLevels > ln + 1) {
		int currentLevelNumber = ln;
		while (currentLevelNumber < maxLevels - 1) {
			int ratio = hierarchy->getRatioToCoarserLevel(currentLevelNumber + 1).max();
			outputCycle = outputCycle * ratio;
			previous_iteration = previous_iteration * ratio;
			currentLevelNumber++;
		}
	}
#remeshing#
	t_step->start();
  	// Shifting time
#shift variables#
  	// Evolution
#evolution step#
	t_step->stop();
#analysis#

	//Output
	t_output->start();
#output#
	t_output->stop();

	if (mpi.getRank() == 0 && d_output_interval > 0 ) {
		if (previous_iteration < next_console_output && outputCycle >= next_console_output) {
			int currentLevelNumber = ln;
			while (currentLevelNumber > 0) {
				currentLevelNumber--;
				cout <<"  ";
			}

			cout << "Level "<<ln<<". Iteration " << current_iteration[ln]<<". Time "<<current_time<<"."<< endl;
			if (ln == hierarchy->getFinestLevelNumber()) {
				next_console_output = next_console_output + d_output_interval;
				while (outputCycle >= next_console_output) {
					next_console_output = next_console_output + d_output_interval;
				}
			
			}
		}
	}

	if (d_timer_output_interval > 0 ) {
		if (previous_iteration < next_timer_output && outputCycle >= next_timer_output) {
			if (ln == hierarchy->getFinestLevelNumber()) {
				//Print timers
				if (mpi.getRank() == 0) {
					tbox::TimerManager::getManager()->print(cout);
				}
				else {
					if (ln == hierarchy->getFinestLevelNumber()) {
						//Dispose other processor timers
						//SAMRAI needs all processors run tbox::TimerManager::getManager()->print, otherwise it hungs
						std::ofstream ofs;
						ofs.setstate(std::ios_base::badbit);
						tbox::TimerManager::getManager()->print(ofs);
					}
				}
				next_timer_output = next_timer_output + d_timer_output_interval;
				while (outputCycle >= next_timer_output) {
					next_timer_output = next_timer_output + d_timer_output_interval;
				}
			
			}
		}
	}

	return simPlat_dt;
}

/*
 * Checks the finalization conditions              
 */
bool Problem::checkFinalization(const double current_time, const double simPlat_dt)
{
#finalization condition#
}

void Problem::putToRestart(MainRestartData& mrd) {
#putToRestart#
	mrd.setCurrentIteration(current_iteration);
	mrd.setNextConsoleOutputIteration(next_console_output);
	mrd.setNextTimerOutputIteration(next_timer_output);
}

void Problem::getFromRestart(MainRestartData& mrd) {
#getFromRestart#
	current_iteration = mrd.getCurrentIteration();
	next_console_output = mrd.getNextConsoleOutputIteration();
	next_timer_output = mrd.getNextTimerOutputIteration();
}

void Problem::allocateAfterRestart() {
#allocateAfterRestart#
}

/*
 *  Cell tagging routine - tag cells that require refinement based on a provided condition. 
 */
void Problem::applyGradientDetector(
   const std::shared_ptr< hier::PatchHierarchy >& hierarchy, 
   const int level_number,
   const double time, 
   const int tag_index,
   const bool initial_time,
   const bool uses_richardson_extrapolation_too) 
{
#applyGradientDetector#
}

#particlesFunctions#

