#ifndef included_Particle_#SpeciesName#
#define included_Particle_#SpeciesName#

#include "SAMRAI/tbox/MessageStream.h"
#include "SAMRAI/hier/IntVector.h"
#include "SAMRAI/tbox/Database.h"
#include <vector>

/**
 * The SampleClass struct holds some dummy data and methods.  It's intent
 * is to indicate how a user could construct their own index data double.
 */
using namespace std;
using namespace SAMRAI;

class Particle_#SpeciesName#
{
public:

	inline int GetExpoBase2(double d);

	bool	Equal(double d1, double d2);

	/**
	* The default constructor.
	*/
	Particle_#SpeciesName#();

	/**
	* Constructor.
	*/
	Particle_#SpeciesName#(const int region);

	/**
	* The complete constructor.
	*/
	Particle_#SpeciesName#(const double fieldValue, const double* pos, const int region = 0);

	/**
	* Copy constructor.
	*/
	Particle_#SpeciesName#(const Particle_#SpeciesName#& data);
	Particle_#SpeciesName#(const Particle_#SpeciesName#& data, bool newId);

	~Particle_#SpeciesName#();

	//Setters 
	void setId(const int id);
	void setProcId(const int procId);
	void setFieldValue(string varName, double value);

	bool hasField(string varName);

	//Getters
	double getFieldValue(string varName);
	int getId();
	int getProcId();

	//Distance between particles
#distances#

	inline bool same(const Particle_#SpeciesName#& data)
	{
		return !(#overlaps# || region != data.region);
	}
	Particle_#SpeciesName#* overlaps(const Particle_#SpeciesName#& data);

	Particle_#SpeciesName#& operator=(const Particle_#SpeciesName#& data);

	inline bool operator==(const Particle_#SpeciesName#& data)
	{
		return !((this != &data) || (id != data.id) || (procId != data.procId));
	}

	void packStream(tbox::MessageStream& stream);
	void unpackStream(tbox::MessageStream& stream, const hier::IntVector& offset);
	void getFromDatabase(std::shared_ptr<tbox::Database>& database, int particle);
	void putToDatabase(std::shared_ptr<tbox::Database>& database, int particle);

	static size_t size() {
		size_t bytes = tbox::MessageStream::getSizeof<double>(#positionsNumber# + #fieldsNumber#) + tbox::MessageStream::getSizeof<int>(5);
		return(bytes);
	};

   	//Field value of the particle
#fields#

   	//Positions of the particle (Global index position)
#positions#

	//Region information
	int region;
	int interior;
	int newRegion; //The region to become when entering the domain (boundaries)

	//Particle identifier
	int id;
	//Creator processor identifier
	int procId;

private:

	//Particle counter
	static int counter;


};

#endif
