#ifndef included_NonSync
#define included_NonSync

#include "SAMRAI/tbox/MessageStream.h"
#include "SAMRAI/hier/IntVector.h"
#include "SAMRAI/tbox/Database.h"
#include <vector>
#include <typeinfo>

/**
 * The SampleClass struct holds some dummy data and methods.  It's intent
 * is to indicate how a user could construct their own index data type.
 */
using namespace std;
using namespace SAMRAI;

class NonSync
{
public:
	/**
	* The default constructor.
	*/
	NonSync();

	/**
	* The complete constructor.
	*/
	NonSync(const int nonSyncValue, const double* pos);

	/**
	* Copy constructor.
	*/
	NonSync(const NonSync& data);

	~NonSync();

	inline int GetExpoBase2(double d);

	bool	equals(double d1, double d2);

	//Setters 
#setPositions#

	void setnonSync(const int nonSync);

	//Getters
#getPositions#

	int getnonSync();

	NonSync* overlaps(const NonSync& data);
	double getDistance();

	NonSync& operator=(const NonSync& data);

	bool operator==(const NonSync& data);

	void packStream(tbox::MessageStream& stream);
	void unpackStream(tbox::MessageStream& stream, const hier::IntVector& offset);
	void getFromDatabase(std::shared_ptr<tbox::Database>& database, int nonSync);
	void putToDatabase(std::shared_ptr<tbox::Database>& database, int nonSync);

	static size_t size() {
		size_t bytes = tbox::MessageStream::getSizeof<double>(#DIM#) + tbox::MessageStream::getSizeof<int>(1);
		return(bytes);
	};

private:

   	//Positions of the nonSync
#positions#

	//Region information
	int nonSyncValue;

};

#endif
