****************************************************************************
*  Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at       *
*  http://www.iac3.eu/simflowny-copyright-and-license                      *
****************************************************************************
****************************************************************************
*                                                                          *
*                      Code generated for SAMRAI                           *
*     Structured Adaptive Mesh Refinement Applications Infrastructure      *
*                   http://www.llnl.gov/CASC/SAMRAI                        *
*                                                                          *
****************************************************************************

Overview
--------

This document describes the Simflowny's generated code for SAMRAI framework as well as the parameters for the simulation.

The downloaded code is already compiled in the machine running Simflowny server.
The user may want to manually modify the code and compile it again using the MAKEFILE providen.

As a C++ code, files may have .cpp, .C and .h extension, both refering to the same class. The .h files declares the functions and variables used within the class, and the .cpp and .C implement the functions. 

Simulation parameters
---------------------

SAMRAI provides a system to run executions with different parameters providing a parameter file.

Simflowny's code includes a file, problem.input to be used as a reference by the user to set up the parameters desired for the simulations.

The parameters are grouped in different sections.

Problem
*******

The first subsection particles sets the particles in the domain.

    - number_of_particles_x, number_of_particles_y, number_of_particles_z. Indicates the number of particles for each dimension. The total number of particles is the product of these parameters.
    - print_average. When true, the underlying cells calculates the averages from the particle fields and export them in the output data.
    - smoothingLength. The typical separation between particles.
    - particle_distribution. The distribution of the particles in the domain. Two values possible. REGULAR, where all the particles are placed aligned for all the dimensions. STAGGERED, where the particles are shifted half a particle distance each row.

After the particle section there are the parameters related with the simulation problem defined in Simflowny.

Main
****

This section allows the configuration for the simulation time evolution.

    - output_interval. Interval of screen output of the simulation time.
    - start_from_restart. To indicate when the simulation starts from a restart snapshot.
    - restart_interval. If greater than 0, indicates the interval to store restart snapshots. It stores the current state of the system to allow simulation recovery.
    - restart_iteration. When start_from_restart is true, this parameter indicates the step number to start from.
    - restart_dirname. The directory name for the restart files.
    - init_from_files. When true, the field of the problem can be read form external hdf5 files. The files should have the same dimension size of the simulation (configured in the next section).
    - dt. Indicates the time increment between each step in the simulation.

FileWriter
**********

This is the section to set the configuration for the output to disk.

    - hdf5_dump_interval. To configure the interval for the output files with data.
    - hdf5_dump_dirname. The directory name for the files.
    - variables. The variables to write to disk

CartesianGeometry
*****************

In this section the simulation domain is set. Simflowny automatically sets the physical domain with the data set in the problem.
SAMRAI is a framework that run simulations over meshes, however it also allows implementing a system of particles on top of a mesh. Consequently, there is necessary to set a mesh size even in the particle problems.

    - domain_boxes. Sets the mesh size, from the initial values to the final ones. The ghost cells needed for calculations are automatically added to the mesh afterwards.
    - x_lo, x_up. Set by simflowny. Anyway, it can be modified by the user if needed.
    - periodic_dimension. Already set by Simflowny. Indicates the dimensions that have periodical boundary. It should not be changed by a user since not periodical boundaries are implemented within the code.


Note: StandardTagAndInitialize, GriddingAlgorithm, LoadBalancer and TimerManager are SAMRAI internal configurations and should not be changed by the user.


Compilation
-----------

If the user modifies the source code or wants to run the code in a different machine from Simflowny server, the Makefile provided eases the task.
First of all, open it with a text editor and check that the library paths are the same of your system configuration. 
Once the Makefile is properly configured, open a terminal and execute:
    $ make


Execution
---------

To run a simulation in a single processor run in a terminal:
    $ ./binFile parameterFile.input
where binName is the name of the binary created in the compilation, and parameter.input is the name of the parameter file.

To run multiprocessor run:
    $ mpirun -np X ./binFile parameterFile.input

where X is the number of processors in which the simulation will run.

File Description
----------------

* Functions - This class may contain any function from the discretization schema which is not a MACRO function and the extrapolation functions if needed. One-line functions are automatically translated to MACRO functions by Simflowny for performance purposes and are located in the Problem class.

* MainRestartData - This class implements the checkpointing feature. A regular user should not be interested in changing its behaviour.

* Makefile - Makefile is a special format file that together with the make utility will help you to automatically compile this code. It contains the references to the required libraries and should be adapted if any library is installed in a different location from Simflowny server.

* NonSync and NonSyncs - Internal classes to implement particle mapping. The user does not need to modify them.

* Particle - Class representing a single particle. It stores the particle variables and fields and some basic behaviour. 

* ParticleDataWriter - The particle data writer implementation. The user does not need to modify it.

* Particles - Cell support for particles. The user only need to modify it if added or removed variables to the Particle class.

* Problem - This class implements the problem simulation routines, from the initialization to finalization checks. It is described in more detail in the following section.

* SAMRAIConnector - Is the main class for the simulation. It joins and manages all SAMRAI components. The user could be interested in modify the behaviour of the output that could not be managed through the simulation parameters.

Problem modification
--------------------

The following sections describe the most significative functions for an user wanting to manually modify part of the code.

    - Problem::initializeLevelData. In the end of this function, there are the post-initialization calculations.
    - Problem::mapDataOnPatch. This section maps the problem regions in the mesh and particles.
    - Problem::initializeDataOnPatch. Initialization of the fields from the problem equations.
    - Problem::initializeDataFromFile. Initialization of the fields from external hdf5 files.
    - Problem::setupPlotter. This function sets the fields and variables that are going to be included in the output data.
    - Problem::step. This section includes the whole algorithm (discretized problem) that is executed at one time step.
    - Problem::checkFinalization. Where the finalization condition is implemented.
    - Problem::moveParticles. Implementation of the particle movement.
