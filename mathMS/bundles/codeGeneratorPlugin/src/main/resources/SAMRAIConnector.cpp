#include <stdio.h>
#include <signal.h>
#include <errno.h>
#include <execinfo.h>
#include "string.h"

#include "Problem.h"

#include "SAMRAI/tbox/SAMRAIManager.h"
#include "SAMRAI/tbox/Database.h"
#include "SAMRAI/tbox/InputDatabase.h"
#include "SAMRAI/tbox/InputManager.h"
#include "SAMRAI/tbox/RestartManager.h"
#include "SAMRAI/tbox/SAMRAI_MPI.h"
#include "SAMRAI/tbox/PIO.h"
#include "SAMRAI/tbox/Utilities.h"
#include "SAMRAI/tbox/Timer.h"
#include "SAMRAI/tbox/TimerManager.h"
#include "SAMRAI/mesh/BergerRigoutsos.h"
#include "SAMRAI/geom/CartesianGridGeometry.h"
#include "SAMRAI/tbox/BalancedDepthFirstTree.h"
#include "SAMRAI/mesh/TileClustering.h"
#include "SAMRAI/mesh/GriddingAlgorithm.h"
#include "SAMRAI/mesh/TreeLoadBalancer.h"
#include "SAMRAI/mesh/ChopAndPackLoadBalancer.h"
#include "SAMRAI/mesh/CascadePartitioner.h"
#include "SAMRAI/hier/PatchHierarchy.h"
#include "MainRestartData.h"
#include "SAMRAI/mesh/StandardTagAndInitialize.h"
#include "TimeRefinementIntegrator.h"
#include "SAMRAI/algs/TimeRefinementLevelStrategy.h"
#DataWriterInclude#

const int MPI_MASTER = 0;

// SAMRAI namespaces
using namespace std;
using namespace SAMRAI;

//Global variables
Problem* problem;

int n_at_commands;
int iteration_num;
double loop_time;
double dt;
int output_interval;
int timer_output_interval;
int restore_num = 0;
bool is_from_restart = false;
#FullMeshDeclaration#
#IntegrationDeclaration#
#ParticleAverageDeclaration#
#SlicerDeclarations#

ostringstream output_information;

int gcd(int a, int b) {
    return b == 0 ? a : gcd(b, a % b);
}

struct sigcontext ctx;

void bt_sighandler(int sig) {

  void *trace[16];
  char **messages = (char **)NULL;
  int i, trace_size = 0;

  if (sig == SIGSEGV)
    printf("Got signal %d, faulty address is %p, "
           "from %p\n", sig, (void *)ctx.cr2, (void *)ctx.rip);
  else
    printf("Got signal %d\n", sig);

  trace_size = backtrace(trace, 16);
  /* overwrite sigaction with caller's address */
  trace[1] = (void *)ctx.rip;
  messages = backtrace_symbols(trace, trace_size);
  /* skip first stack frame (points here) */
  printf("[bt] Execution path:\n");
  for (i=1; i<trace_size; ++i)
  {
    printf("[bt] #%d %s\n", i, messages[i]);

    /* find first occurence of '(' or ' ' in message[i] and assume
     * everything before that is the file name. (Don't go beyond 0 though
     * (string terminator)*/
    size_t p = 0;
    while(messages[i][p] != '(' && messages[i][p] != ' '
            && messages[i][p] != 0)
        ++p;

    char syscom[256];
    sprintf(syscom,"addr2line %p -e %.*s", trace[i], (int) p, messages[i]);
        //last parameter is the file name of the symbol
    int err = system(syscom);
  }

  exit(0);
}

int main( int argc, char* argv[] )
{
	tbox::SAMRAI_MPI::init(&argc, &argv);
	tbox::SAMRAIManager::initialize();
	tbox::SAMRAIManager::startup();
	const tbox::SAMRAI_MPI& mpi(tbox::SAMRAI_MPI::getSAMRAIWorld());
	tbox::SAMRAIManager::setMaxNumberPatchDataEntries(#NUMVARS#);
	int my_proc = mpi.getRank();

	if (argc != 2) {
		TBOX_ERROR("Usage: "<<argv[0]<<" <parameter file>");
		return -1;
	}

	//Signal handling
  	struct sigaction sa;
  	sa.sa_handler = (void (*)(int))bt_sighandler;
  	sigemptyset(&sa.sa_mask);
  	sa.sa_flags = SA_RESTART;
  	sigaction(SIGSEGV, &sa, NULL);
	sigaction(SIGINT, &sa, NULL);
	sigaction(SIGQUIT, &sa, NULL);
	sigaction(SIGILL, &sa, NULL);
	sigaction(SIGABRT, &sa, NULL);
	sigaction(SIGTERM, &sa, NULL);

	std::shared_ptr<tbox::InputDatabase> input_db(new tbox::InputDatabase("input_db"));
	std::string input_file(argv[1]);
	tbox::InputManager::getManager()->parseInputFile(input_file, input_db);
	if (!input_db->keyExists("Main")) return -1;
	std::shared_ptr<tbox::Database> main_db(input_db->getDatabase("Main"));
	dt = main_db->getDouble("dt");
	bool rebalance_mesh = main_db->getBoolWithDefault("rebalance_processors", false);

	const tbox::Dimension dim(static_cast<unsigned short>(#DIM#));

	// Get the restart manager and root restart database.If run is from restart, open the restart file.
	is_from_restart = main_db->getBool("start_from_restart");
	int restart_interval = main_db->getInteger("restart_interval");
	string restart_write_dirname = main_db->getStringWithDefault("restart_dirname", ".");
	const bool write_restart = (restart_interval > 0) && !(restart_write_dirname.empty());

	tbox::RestartManager* restart_manager = tbox::RestartManager::getManager();

	if (is_from_restart) {
		restore_num = main_db->getInteger("restart_iteration");
		restart_manager->openRestartFile(restart_write_dirname, restore_num, mpi.getSize());
	}

	MainRestartData* main_restart_data = new MainRestartData("MainRestartData",main_db);

	loop_time = main_restart_data->getLoopTime();
	int loop_cycle = main_restart_data->getIterationNumber();
	iteration_num = main_restart_data->getIterationNumber();

	//Setting the timers
	tbox::TimerManager::createManager(input_db->getDatabase("TimerManager"));
	std::shared_ptr<tbox::Timer> t_regrid(tbox::TimerManager::getManager()->getTimer("Regridding"));
	
	//Setup the hdf5 outputs
	int visit_number_procs_per_file = 1;
	std::shared_ptr<tbox::Database> writer_db(input_db->getDatabase("FileWriter"));
#MeshInputParameters#
#ParticleInputParameters#
#IntegrationInputParameters#
#SlicerInputParameters#
	
#force one patch#

#ParticleAverage#

#DataWriterDeclaration#

	//Setup the output
	output_interval = 0;
	if (main_db->keyExists("output_interval")) {
		output_interval = main_db->getInteger("output_interval");
	}
	timer_output_interval = 0;
	if (main_db->keyExists("timer_output_interval")) {
		timer_output_interval = main_db->getInteger("timer_output_interval");
	}

	//Mesh creation
	std::shared_ptr<geom::CartesianGridGeometry > grid_geometry(new geom::CartesianGridGeometry(dim,"CartesianGeometry", input_db->getDatabase("CartesianGeometry")));
	std::shared_ptr<hier::PatchHierarchy > patch_hierarchy(new hier::PatchHierarchy("PatchHierarchy", grid_geometry, input_db->getDatabase("PatchHierarchy")));
#refinementConversion#
	std::string problem_name("Problem");
	std::shared_ptr<tbox::Database> problem_db(input_db->getDatabase("Problem"));
	problem = new Problem(problem_name, dim, problem_db, grid_geometry, patch_hierarchy, *main_restart_data, dt, is_from_restart, output_interval, timer_output_interval#DataWriterParameters##slicerParameter##IntegrationParameters#);
	std::shared_ptr<mesh::StandardTagAndInitialize > sti(new mesh::StandardTagAndInitialize("StandardTagAndInitialize", problem, sti_db));
	// Set up the clustering.
    const std::string clustering_type = main_db->getStringWithDefault("clustering_type", "BergerRigoutsos");
    std::shared_ptr<mesh::BoxGeneratorStrategy> box_generator;
	if (clustering_type == "BergerRigoutsos") {
        std::shared_ptr<tbox::Database> abr_db(input_db->getDatabase("BergerRigoutsos"));
        std::shared_ptr<mesh::BoxGeneratorStrategy> berger_rigoutsos(new mesh::BergerRigoutsos(dim, abr_db));
        box_generator = berger_rigoutsos;
    } else if (clustering_type == "TileClustering") {
    	std::shared_ptr<tbox::Database> tc_db(input_db->getDatabase("TileClustering"));
        std::shared_ptr<mesh::BoxGeneratorStrategy> tile_clustering(new mesh::TileClustering(dim, tc_db));
        box_generator = tile_clustering;
    }
    // Set up the load balancer.
    std::shared_ptr<mesh::LoadBalanceStrategy> load_balancer;
    std::shared_ptr<mesh::LoadBalanceStrategy> load_balancer0;
    const std::string partitioner_type = main_db->getStringWithDefault("partitioner_type", "TreeLoadBalancer");
    if (partitioner_type == "TreeLoadBalancer") {
        std::shared_ptr<mesh::TreeLoadBalancer> tree_load_balancer(new mesh::TreeLoadBalancer(dim, "mesh::TreeLoadBalancer", input_db->getDatabase("TreeLoadBalancer"), std::shared_ptr<tbox::RankTreeStrategy>(new tbox::BalancedDepthFirstTree)));
        tree_load_balancer->setSAMRAI_MPI(tbox::SAMRAI_MPI::getSAMRAIWorld());

        std::shared_ptr<mesh::TreeLoadBalancer> tree_load_balancer0(new mesh::TreeLoadBalancer(dim, "mesh::TreeLoadBalancer0", input_db->getDatabase("TreeLoadBalancer"), std::shared_ptr<tbox::RankTreeStrategy>(new tbox::BalancedDepthFirstTree)));
        tree_load_balancer0->setSAMRAI_MPI(tbox::SAMRAI_MPI::getSAMRAIWorld());

        load_balancer = tree_load_balancer;
        load_balancer0 = tree_load_balancer0;
    } else if (partitioner_type == "CascadePartitioner") {
        std::shared_ptr<mesh::CascadePartitioner> cascade_partitioner(new mesh::CascadePartitioner(dim, "mesh::CascadePartitioner", input_db->getDatabase("CascadePartitioner")));
        cascade_partitioner->setSAMRAI_MPI(tbox::SAMRAI_MPI::getSAMRAIWorld());

        std::shared_ptr<mesh::CascadePartitioner> cascade_partitioner0(new mesh::CascadePartitioner(dim, "mesh::CascadePartitioner0", input_db->getDatabase("CascadePartitioner")));
        cascade_partitioner0->setSAMRAI_MPI(tbox::SAMRAI_MPI::getSAMRAIWorld());

        load_balancer = cascade_partitioner;
        load_balancer0 = cascade_partitioner0;
    } else if (partitioner_type == "ChopAndPackLoadBalancer") {

        std::shared_ptr<mesh::ChopAndPackLoadBalancer> cap_load_balancer(new mesh::ChopAndPackLoadBalancer(dim, "mesh::ChopAndPackLoadBalancer", input_db->getDatabase("ChopAndPackLoadBalancer")));

        load_balancer = cap_load_balancer;
        std::shared_ptr<mesh::CascadePartitioner> cascade_partitioner0(new mesh::CascadePartitioner(dim, "mesh::CascadePartitioner0", input_db->getDatabase("CascadePartitioner")));
        cascade_partitioner0->setSAMRAI_MPI(tbox::SAMRAI_MPI::getSAMRAIWorld());
        load_balancer0 = cascade_partitioner0;
    }

	std::shared_ptr< mesh::GriddingAlgorithm > gridding_algorithm(new mesh::GriddingAlgorithm(patch_hierarchy, "GriddingAlgorithm", input_db->getDatabase("GriddingAlgorithm"), sti, box_generator, load_balancer, load_balancer0));
	if (!gridding_algorithm) return -1;

   // std::shared_ptr< algs::TimeRefinementLevelStrategy >  timeRefinementStrategy(SAMRAI_SHARED_PTR_CAST<algs::TimeRefinementLevelStrategy, Problem*>(problem));
    std::shared_ptr<algs::TimeRefinementLevelStrategy> timeRefinementStrategy(problem);
    std::shared_ptr<algs::TimeRefinementIntegrator> time_integrator(new algs::TimeRefinementIntegrator("TimeRefinementIntegrator", input_db->getDatabase("TimeRefinementIntegrator"), patch_hierarchy, timeRefinementStrategy, gridding_algorithm));



	//Prints the banner of the simulation
	if (my_proc == MPI_MASTER) {
		cout << "|-----------------------SIMPLAT--------------------------|" << endl;
		cout << "    Number of processors " << mpi.getSize()  << endl;
		cout << "|--------------------------------------------------------|" << endl;
		cout << output_information.str();
	}

#PlotterSetup#

	//Hierarchy initialization
	double dt_now = time_integrator->initializeHierarchy();
	if (tbox::RestartManager::getManager()->isFromRestart() ) {
		//Allocates temporal mesh variables after a restart
		problem->allocateAfterRestart();
		//Rebalancing of mesh (in case more processors are needed)
		if (rebalance_mesh) {
			gridding_algorithm->makeCoarsestLevel(time_integrator->getIntegratorTime());
		}
#ParticleCommonInit#
	}
	tbox::RestartManager::getManager()->closeRestartFile();

#PostInit#
	//Print the initial data.

#InitialPlot#
#IntegrationFirstOutput#
#slicerFirstOutput#

	//Initial level information
	if (my_proc == MPI_MASTER) {
		cout << "|--------------------------------------------------------|" << endl;
		cout << "    Number of levels " << patch_hierarchy->getNumberOfLevels() << endl;
	}
	for (int ln = 0; ln < patch_hierarchy->getNumberOfLevels(); ln++) {
		std::shared_ptr< hier::PatchLevel > level(patch_hierarchy->getPatchLevel(ln));
		const std::shared_ptr<geom::CartesianGridGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianGridGeometry, hier::BaseGridGeometry>(level->getGridGeometry()));
		const double* deltas = patch_geom->getDx();
		const hier::IntVector ratio = level->getRatioToLevelZero();
		int numPatches = level->getGlobalNumberOfPatches();
		int numCells = level->getGlobalNumberOfCells();
		if (my_proc == MPI_MASTER) {
			cout<<"    Level: "<<ln<<". Number of patches: "<<numPatches<<". Number of cells: "<<numCells<<". Dx: "<<#deltas#<<endl;
		}
	}
	if (my_proc == MPI_MASTER) {
		cout << "|--------------------------------------------------------|" << endl;
	}
	//Print memory info
	mpi.Barrier();
	tbox::MemoryUtilities::printMemoryInfo(cout);

	//Simulation steps
	double loop_time = time_integrator->getIntegratorTime();
	int iteration_num = time_integrator->getIntegratorStep();
	while (!problem->checkFinalization(loop_time, dt_now)) {
		//Do a step
		iteration_num = time_integrator->getIntegratorStep() + 1;

		problem->registerIteration(iteration_num);
		double dt_new = time_integrator->advanceHierarchy(dt_now);

		loop_time += dt_now;
		dt_now = dt_new;
		
		//Level information after hierarchy change
		if (time_integrator->atRegridPoint(0)) {
			for (int ln = 0; ln < patch_hierarchy->getNumberOfLevels(); ln++) {
				std::shared_ptr< hier::PatchLevel > level(patch_hierarchy->getPatchLevel(ln));
				const std::shared_ptr<geom::CartesianGridGeometry > patch_geom(SAMRAI_SHARED_PTR_CAST<geom::CartesianGridGeometry, hier::BaseGridGeometry>(level->getGridGeometry()));
				const double* deltas = patch_geom->getDx();
				const hier::IntVector ratio = level->getRatioToLevelZero();
				int numPatches = level->getGlobalNumberOfPatches();
				int numCells = level->getGlobalNumberOfCells();
				if (my_proc == MPI_MASTER) {
					cout<<"    Level: "<<ln<<". Number of patches: "<<numPatches<<". Number of cells: "<<numCells<<". Dx: "<<#deltas#<<endl;
				}
			}
			if (my_proc == MPI_MASTER) {
				cout << "|--------------------------------------------------------|" << endl;
			}
			//Print memory info
			mpi.Barrier();
			tbox::MemoryUtilities::printMemoryInfo(cout);
		}


		//Output restart data
		if ( write_restart && (0 == iteration_num % restart_interval) ) {
			problem->putToRestart(*main_restart_data);
			tbox::RestartManager::getManager()->writeRestartFile(restart_write_dirname, iteration_num);
		}
	}
	//Last restart at the end of the simulation
	if ( write_restart) {
		problem->putToRestart(*main_restart_data);
		tbox::RestartManager::getManager()->writeRestartFile(restart_write_dirname, iteration_num);
	}

	//Print the end output
	if (my_proc == MPI_MASTER) {
		cout << "    Simulation finished:" << endl;
		cout << "           Iterations: " << iteration_num << endl;
		cout << "           Time: " << loop_time  << endl;	
		cout << "|--------------------------------------------------------|" << endl;

		tbox::TimerManager::getManager()->print(cout);
	}
	else {
		//Dispose other processor timers
		//SAMRAI needs all processors run tbox::TimerManager::getManager()->print, otherwise it hungs
		std::ofstream ofs;
		ofs.setstate(std::ios_base::badbit);
		tbox::TimerManager::getManager()->print(ofs);	
	}

	//Close all the objects
	time_integrator.reset();
	gridding_algorithm.reset();
	load_balancer.reset();
	box_generator.reset(); 
	sti.reset();
	if (main_restart_data) delete main_restart_data;

	patch_hierarchy.reset();
	grid_geometry.reset();
	input_db.reset();
	main_db.reset();
	tbox::SAMRAIManager::shutdown();
	tbox::SAMRAIManager::finalize();
	//Should be here, but an error is thrown if not commented.
	//tbox::SAMRAI_MPI::finalize();
	return 0;
}
