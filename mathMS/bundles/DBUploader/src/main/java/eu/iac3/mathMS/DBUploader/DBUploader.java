package eu.iac3.mathMS.DBUploader;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.io.FileUtils;
import org.exist.xmldb.DatabaseImpl;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xmldb.api.DatabaseManager;
import org.xmldb.api.base.Collection;
import org.xmldb.api.base.Database;
import org.xmldb.api.base.XMLDBException;
import org.xmldb.api.modules.BinaryResource;
import org.xmldb.api.modules.CollectionManagementService;
import org.xmldb.api.modules.XMLResource;

public class DBUploader implements BundleActivator {
	

	public void start(BundleContext context) throws Exception {
		Collection col = null;
        try {
        	@SuppressWarnings("rawtypes")
			ServiceReference serviceReference = context.getServiceReference(DatabaseImpl.class.getName());
        	@SuppressWarnings("unchecked")
        	Database database = (Database) context.getService(serviceReference); 
            DatabaseManager.registerDatabase(database);
            
            File dir = new File("db_uploader");  
            String[] collections = dir.list();
            for (int i = 0; i < collections.length; i++) {
                createDB("db/" + collections[i], context);
            }
            System.out.println("Upload completed!!");
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            try { 
                if (col != null) {
                    col.close();
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
	}
	
    /** 
     * Return the node element for the actual collection following the SchemaStructure XSD. 
     *
     * @param coll          The base collection
     * @param doc           The DOM document base for the xml
     * @return              It returns the root node for the collection following the
     *                      SchemaStructure XSD 
     */
    static void createDB(String coll, BundleContext context) {
        try {
        	@SuppressWarnings("rawtypes")
			ServiceReference serviceReference = context.getServiceReference(DatabaseImpl.class.getName());
        	@SuppressWarnings("unchecked")
        	Database database = (Database) context.getService(serviceReference); 
            DatabaseManager.registerDatabase(database);
            System.out.println("Entro en createDB");
            //Element name
            File dir = new File(coll.replaceFirst("db/", "db_uploader/"));
            //Create collection if it does not exist
            createCollection(coll, context);
            
            //List files and directories
            File[] listOfFiles = dir.listFiles();
            for (int i = 0; i < listOfFiles.length; i++) {
            	System.out.println(listOfFiles[i].toString() + " " + listOfFiles[i].isFile() + " " + listOfFiles[i].isDirectory());
                //Files
            	if (listOfFiles[i].isFile()) {
            		String name = listOfFiles[i].toString();
            		System.out.println("ends " + name.endsWith(".xml"));
            		if (name.endsWith(".xml")) {
            			addFile(fileToString(name), listOfFiles[i].toString(), context);
            		}
            		else {
            			if (!name.endsWith("~")) {
            				addX3D(name.substring(name.lastIndexOf("/") + 1), fileToString(name), context);
            			}
        			}
            	}
	            //Collections
	            else if (listOfFiles[i].isDirectory()) {
	            	createDB(listOfFiles[i].toString(), context);
	            }
            }
           
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }

    
    static String addFile(String xmlDoc, String baseCollection, BundleContext context) {
        Collection col = null;
        try {
            System.out.println("Voy a añadir file ");
        	
            //Getting the id and collection info
            Document doc = stringToDom(xmlDoc);
            Element root = doc.getDocumentElement();
            removeWhitespaceNodes(root);
        
            String id = ((Element) root.getFirstChild()).getElementsByTagName("mms:id").item(0).getTextContent();
            
            System.out.println(id);
            String collection = baseCollection.substring(baseCollection.indexOf("/") + 1, baseCollection.lastIndexOf("/"));
  
            //Configuring connection to DB
        	@SuppressWarnings("rawtypes")
			ServiceReference serviceReference = context.getServiceReference(DatabaseImpl.class.getName());
        	@SuppressWarnings("unchecked")
        	Database database = (Database) context.getService(serviceReference); 
            DatabaseManager.registerDatabase(database);

            col = DatabaseManager.getCollection("xmldb:exist:///db/" + collection, "admin", "");
System.out.println(collection + " " + col);
            //Setting the xml resource
            XMLResource model = (XMLResource) col.createResource(id, XMLResource.RESOURCE_TYPE);
                        
            model.setContent(xmlDoc);
            col.storeResource(model);
            
            return id;
        }
        catch (Exception e) {
            e.printStackTrace();
            return "";
        }
        finally {
            try {
                if (col != null) {
                    col.close();
                }
            }
            catch (XMLDBException e) {
                e.printStackTrace();
                return "";
            }
        }
    }
    
    static String addX3D(String fileName, String xmlDoc, BundleContext context) {
        Collection col = null;
        try {
            //Getting the id and collection info
            Document doc = stringToDom(xmlDoc);
            Element root = doc.getDocumentElement();
            removeWhitespaceNodes(root);
        
            //Configuring connection to DB
        	@SuppressWarnings("rawtypes")
			ServiceReference serviceReference = context.getServiceReference(DatabaseImpl.class.getName());
        	@SuppressWarnings("unchecked")
        	Database database = (Database) context.getService(serviceReference); 
            DatabaseManager.registerDatabase(database);

            col = DatabaseManager.getCollection("xmldb:exist:///db/x3d", "admin", "");

            //Setting the binary resource
            BinaryResource model = (BinaryResource) col.createResource(fileName, "BinaryResource");
            model.setContent(xmlDoc.getBytes());
            col.storeResource(model);
            
            return fileName;
        }
        catch (Exception e) {
            e.printStackTrace();
            return "";
        }
        finally {
            try {
                if (col != null) {
                    col.close();
                }
            }
            catch (XMLDBException e) {
                e.printStackTrace();
                return "";
            }
        }
    }
    
    static void createCollection(String collection, BundleContext context) {
        try {
        	@SuppressWarnings("rawtypes")
			ServiceReference serviceReference = context.getServiceReference(DatabaseImpl.class.getName());
        	@SuppressWarnings("unchecked")
        	Database database = (Database) context.getService(serviceReference); 
            DatabaseManager.registerDatabase(database);
            //Remove the previous db
            String colString = collection.substring(collection.indexOf("/") + 1);
            String uri = "xmldb:exist:///db/";
            
            System.out.println("Voy a crear " + collection);
            
            
            String[] collSplit = colString.split("/");
            Collection col = DatabaseManager.getCollection(uri, "admin", "");
            //Creation of the complete branch of the collection
            //It checks all parent collections, if they do not exist it creates them
            for (int i = 0; i < collSplit.length; i++) {
                System.out.println("uri " + uri);
                col = DatabaseManager.getCollection(uri, "admin", "");
                //Checking if the database already exists
                if (col.getChildCollection(collSplit[i]) == null) {
                	CollectionManagementService service = (CollectionManagementService) col.getService("CollectionManagementService", "1.0");
            
                    System.out.println("creo colección " + collSplit[i] + " en uri " + uri);
                    service.createCollection(collSplit[i]);
                }
                col.close();
                uri = uri + collSplit[i] + "/";
            }
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * A method for the format of the output.
     * 
     * @param document          The document to be formatted
     * @return                  The document formatted
     * @throws Exception 
     */
    public static String indent(String document) throws Exception  {
        DocumentBuilder docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        Document doc = docBuilder.parse(new ByteArrayInputStream(document.getBytes("UTF-8")));

        // Setup pretty print options
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

        // Return pretty print xml string
        StringWriter stringWriter = new StringWriter();
        transformer.transform(new DOMSource(doc), new StreamResult(stringWriter));
        return stringWriter.toString();
    }
    
    /**
     * Read a file as String.
     * 
     * @param is    The input stream of the file
     * @return      The resource as string
     */
    public static String convertStreamToString(InputStream is) {
        StringBuilder sb = null;
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } 
        catch (IOException e) {
            e.printStackTrace();
        } 
        finally {
            try {
                is.close();
            } 
            catch (IOException e) { 
                e.printStackTrace();
            }
        }
        return sb.toString();
    }
    
    /**
     * Converts a string to Dom.
     * 
     * @param str               The string
     * @return                  The Dom document
     */
    static Document stringToDom(String str) {
        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            docFactory.setNamespaceAware(true);
            docFactory.setIgnoringElementContentWhitespace(true);
            docFactory.setIgnoringComments(true);
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            InputStream is = new ByteArrayInputStream(str.getBytes("UTF-8"));
            return docBuilder.parse(is);
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    /**
     * Remove the whitespaced nodes from a Dom element.
     * 
     * @param e The element to delete the whitespaces from
     */
    public static void removeWhitespaceNodes(Element e) {
        NodeList children = e.getChildNodes();
        for (int i = children.getLength() - 1; i >= 0; i--) {
            Node child = children.item(i);
            if (child instanceof Text && ((Text) child).getData().trim().length() == 0) {
                e.removeChild(child);
            }
            else if (child instanceof Element) {
                removeWhitespaceNodes((Element) child);
            }
        }
    }
    
    /**
     * Converts a Dom to string.
     * 
     * @param doc               The dom document
     * @return                  The String
     */
    static String domToString(Document doc) {
        try {
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            //StreamResult result = new StreamResult(new StringWriter());
            StreamResult result = new StreamResult(new ByteArrayOutputStream());
            DOMSource source = new DOMSource(doc);
            transformer.transform(source, result);
            return new String(((ByteArrayOutputStream) result.getOutputStream()).toByteArray(), "UTF-8");
        }
        catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }
    
    static String fileToString(String path) {
        try{
        	return FileUtils.readFileToString(new File(path), "UTF-8");
        }
        catch(Exception e) {
            e.printStackTrace();
            return "";
        }
    }
    static String fileToString2(String path) {
        try{
            BufferedReader in = new BufferedReader(new FileReader(path));
            String result = "";
            String str;
            while ((str = in.readLine()) != null) {
                result = result + str + System.getProperty("line.separator");
            }
            in.close();
            return result;
        }
        catch(Exception e) {
            e.printStackTrace();
            return "";
        }
    }

	public void stop(BundleContext arg0) throws Exception {
	}
}
