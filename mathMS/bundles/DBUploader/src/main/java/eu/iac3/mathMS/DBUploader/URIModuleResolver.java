package eu.iac3.mathMS.DBUploader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import javax.xml.transform.stream.StreamSource;

import net.sf.saxon.lib.ModuleURIResolver;
import net.sf.saxon.lib.StandardModuleURIResolver;
import net.sf.saxon.trans.XPathException;

public class URIModuleResolver implements ModuleURIResolver {
    private StandardModuleURIResolver res;
    
    /**
     * Constructor.
     */
    public URIModuleResolver() {
        res = StandardModuleURIResolver.getInstance();
    }
    
    /**
     * Locate a query module, or a set of query modules, given the identifying URI and a set of associated location hints.
     * @param moduleURI         The module URI of the module to be imported; or null when loading a non-library module.
     * @param baseURI           The base URI of the module containing the "import module" declaration; null if no base URI is known
     * @param locations         The set of URIs specified in the "at" clause of "import module", which serve as location hints for the module
     * @return                  An array of StreamSource objects each identifying the contents of a query module to be imported
     * @throws XPathException   If the module cannot be located, and if delegation to the default module resolver is not required.
     */
    public StreamSource[] resolve(String  moduleURI, String baseURI, String[] locations)
        throws XPathException {
        StreamSource[] resp = null;
        
        if ("http://ds.iac3.eu/".equals(moduleURI)) {
            try {
                FileInputStream fi = new FileInputStream(new File("common" + File.separator + "xquery" + File.separator + "libreria.xquery"));
                StreamSource ss = new StreamSource();
                ss.setInputStream(fi);
                resp = new StreamSource[1];
                resp[0] = ss;
            } 
            catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            
        }
        else {
            resp = res.resolve(moduleURI, baseURI, locations);
        }
        
        return resp;
    }
}
