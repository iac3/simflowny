/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.codesDBPluginTest;

import eu.iac3.mathMS.codesDBPlugin.CodeResult;
import eu.iac3.mathMS.codesDBPlugin.CodesDB;
import eu.iac3.mathMS.codesDBPlugin.CodesDBImpl;
import eu.iac3.mathMS.codesDBPlugin.DCException;

import static org.ops4j.pax.exam.CoreOptions.bootDelegationPackages;
import static org.ops4j.pax.exam.CoreOptions.junitBundles;
import static org.ops4j.pax.exam.CoreOptions.mavenBundle;
import static org.ops4j.pax.exam.CoreOptions.options;
import static org.ops4j.pax.exam.CoreOptions.systemProperty;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;


import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;
import org.junit.runner.RunWith;
import org.ops4j.pax.exam.Configuration;
import org.ops4j.pax.exam.Option;
import org.ops4j.pax.exam.junit.PaxExam;
import org.ops4j.pax.exam.spi.reactors.ExamReactorStrategy;
import org.ops4j.pax.exam.spi.reactors.PerSuite;
import static org.junit.Assert.*;

/** 
 * PhysicalModelsTest class is a junit testcase to
 * test the {@link eu.iac3.mathMS.codesDBPlugin.CodesDB}.
 * 
 * @author      iac3Team
 * @version     1.0
 */
@RunWith(PaxExam.class)
@ExamReactorStrategy(PerSuite.class)
public class CodesDBTest {

    long startTime;
    
    @Rule 
    public TestName name = new TestName();
        
    @Configuration
    public Option[] config() {
        return options(        		
    		bootDelegationPackages(
    				 "com.sun.*"
    				),
        	systemProperty("org.osgi.service.http.port").value("8080"),
        	systemProperty("file.encoding").value("UTF-8"),
        	systemProperty("derby.system.home").value("target/Derby"),
        	mavenBundle("org.apache.felix", "org.osgi.compendium", "1.4.0"),
        	mavenBundle("org.apache.felix", "org.apache.felix.configadmin", "1.9.0"),
        	mavenBundle("org.ops4j.pax.confman", "pax-confman-propsloader", "0.2.3"),
        	mavenBundle("eu.iac3.thirdParty.bundles", "xml-apis", "1.0"),
        	mavenBundle("org.apache.servicemix.bundles", "org.apache.servicemix.bundles.relaxngDatatype", "2011.1_1"),
            mavenBundle("org.apache.servicemix.bundles", "org.apache.servicemix.bundles.xmlresolver", "1.2_5"),
            mavenBundle("org.apache.servicemix.bundles", "org.apache.servicemix.bundles.saxon", "9.8.0-10_1"),
        	mavenBundle("org.apache.servicemix.bundles", "org.apache.servicemix.bundles.derby", "10.12.1.1_1"),
        	mavenBundle("org.osgi", "org.osgi.enterprise", "5.0.0"),
        	mavenBundle("org.osgi", "org.osgi.service.cm", "1.6.0"),
        	mavenBundle("org.ops4j.pax.jdbc", "pax-jdbc-derby", "1.3.0"), 
            junitBundles()
            );
    }
    
    /**
     * Initial setup for the tests.
     * @throws DCException 
     */
    @Before
    public void onSetUp() throws DCException {
    	new CodesDBImpl().createDB();
        startTime = System.currentTimeMillis();
    }
    
    /**
     * Close browser when last test.
     */
    @After
    public void onTearDown() {
        System.out.println(name.getMethodName() + ": " + (((float)(System.currentTimeMillis() - startTime)) / 60000));
    }
    
    /**
     * Tests {@link eu.iac3.mathMS.codesDBPlugin.CodesDB #getCodeList(String, String, String, java.util.Date, java.util.Date, String, String)}.
     * There are the following tests:
     * 1- query all the parameters with result
     * 2- query some parameters with result
     * 3- query any parameter with result
     * 4- query with no result
     */
    @Test
    public void testGetCodeList() {
        CodesDB cdb;
        //Test 1- query all the parameters with result
        try {
            //Creation of the dates to compare
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            cdb = new CodesDBImpl();
            String id = cdb.addGeneratedCode("testGetCodeList", "test1", "Version", "cactus");
            cdb.addCodeFile(id, "boundary", "Path" + File.separator + "seconfpath1" + File.separator + "finalfile.txt", "FileContent");
            cdb.addCodeFile(id, "boundary2", "Path" + File.separator + "seconfpath2" + File.separator + "finalfile.txt", "FileContent");
            cdb.addCodeFile(id, "boundary3", "Path" + File.separator + "seconfpath3" + File.separator + "finalfile.txt", "FileContent");
            
            CodeResult[] crArray = cdb.getCodeList("testGetCodeList", "test1", "Version", formatter.parse("2002-01-03T00:00:00"), null, "cactus");
            assertEquals("test1", crArray[0].getAuthor());
            assertEquals("testGetCodeList", crArray[0].getName());
        }
        catch (ParseException e) {
            fail("Test 2 - Error getting the code list: " + e.getMessage());
        }
        catch (DCException e) {
            e.printStackTrace();
            fail("Test 2 - Error getting the code list: " + e.getMessage());
        }
        //Test 2- query some parameters with result
        try {
            cdb = new CodesDBImpl();
            String id = cdb.addGeneratedCode("testGetCodeList", "test2", "Version", "cactus");
            cdb.addCodeFile(id, "boundary's", "Path", "FileContent");
            CodeResult[] crArray = cdb.getCodeList("testGetCodeList", "test2", null, null, null, null);
            assertEquals("test2", crArray[0].getAuthor());
            assertEquals("testGetCodeList", crArray[0].getName());
        }
        catch (DCException e) {
            fail("Test 2 - Error getting the code list: " + e.getMessage());
        }
        //Test 3- query with no result
        try {
            cdb = new CodesDBImpl();
            String id = cdb.addGeneratedCode("testGetCodeList", "test3", "Version", "cactus");
            cdb.addCodeFile(id, "boundary's", "Path", "FileContent");
            assertNotNull(cdb.getCodeList(null, null, null, null, null, null));
        }
        catch (DCException e) {
            fail("Test 3 - Error getting the code list: " + e.getMessage());
        }
        //Test 4- query with no result
        try {
            cdb = new CodesDBImpl();
            assertNull(cdb.getCodeList("testGetCodeList", "test4", "Version", null, null, "cactus"));
        }
        catch (DCException e) {
            fail("Test 4 - Error getting the code list: " + e.getMessage());
        }
    }
   
    /**
     * Tests {@link eu.iac3.mathMS.codesDBPlugin.CodesDB #getGeneratedCode(String)}.
     * There are the following tests:
     * 1- get from an existing code
     * 2- get from a not existing code.
     */
    @Test
    public void testGetGeneratedCode() {
        CodesDB cdb;
        //Test 1- get from an existing file
        try {
            cdb = new CodesDBImpl();
            String id = cdb.addGeneratedCode("testGetGeneratedCode", "Test", "Version", "Platrm");
            cdb.addCodeFile(id, "Module1", "Path1", "File\nIn two lines");
            cdb.addCodeFile(id, "Module2", "Path2", "File\nIn two lines");
            cdb.addCodeFile(id, "Module3", "Path3", "File\nIn two lines");
            String[] result = cdb.getGeneratedCode(id);
            assertEquals("codeFiles" + File.separator + "testGetGeneratedCode" + File.separator + "Test" + File.separator + "Version" + File.separator
                    + "Platrm" + File.separator + "Path1", result[0].replaceAll("\\/[0-9]{14}", ""));
            assertEquals("codeFiles" + File.separator + "testGetGeneratedCode" + File.separator + "Test" + File.separator + "Version" + File.separator
                    + "Platrm" + File.separator + "Path2", result[1].replaceAll("\\/[0-9]{14}", ""));
            assertEquals("codeFiles" + File.separator + "testGetGeneratedCode" + File.separator + "Test" + File.separator + "Version" + File.separator
                    + "Platrm" + File.separator + "Path3", result[2].replaceAll("\\/[0-9]{14}", ""));
        }
        catch (DCException e) {
            fail("Test 1 - Error getting the code file: " + e.getMessage());
        }
      //Test 2- get from a not existing file
        try {
            cdb = new CodesDBImpl();
            assertNull(cdb.getGeneratedCode("8764576565879"));
        }
        catch (DCException e) {
            fail("Test 2 - Error getting the code file: " + e.getMessage());
        }
    }
    
    /**
     * Tests {@link eu.iac3.mathMS.codesDBPlugin.CodesDB #getCodeFile(String)}.
     * There are the following tests:
     * 1- get from an existing file
     * 2- get from a not existing file.
     */
    @Test
    public void testGetCodeFile() {
        CodesDB cdb;
        //Test 1- get from an existing file
        try {
            cdb = new CodesDBImpl();
            String id = cdb.addGeneratedCode("testGetCodeFile", "Test", "Version", "Platrm");
            String path = cdb.addCodeFile(id, "Module", "Path", "File\nIn two lines");
            assertEquals("File\nIn two lines", new String(cdb.getCodeFile(path), "UTF8"));
        }
        catch (Exception e) {
            fail("Test 1 - Error getting the code file: " + e.getMessage());
        }
      //Test 2- get from a not existing file
        try {
            cdb = new CodesDBImpl();
            assertNull(cdb.getCodeFile("Test" + File.separator + "Test" + File.separator + "Version" + File.separator 
                    + "NonExisting" + File.separator + "Platrm" + File.separator + "Path"));
        }
        catch (DCException e) {
            fail("Test 2 - Error getting the code file: " + e.getMessage());
        }
    }
    
    /**
     * Tests {@link eu.iac3.mathMS.codesDBPlugin.CodesDB #addCodeFile(String, String, String, Date, String, String, String, String)}.
     * There are the following tests:
     * 1- Insert test
     * 2- Duplicate insert test. Must throw {@link eu.iac3.mathMS.codesDBPlugin.DCException #DC001}
     * 
     */
    @Test
    public void testAddCodeFile() {
        CodesDB cdb;
        String id = null;
        //Test 1- Insert test
        try {
            cdb = new CodesDBImpl();
            id = cdb.addGeneratedCode("testAddCodeFile", "Author", "Version", "Platrm");
            String path = cdb.addCodeFile(id, "Module", "Path" + File.separator + "Path" , "File\u0394");
            assertEquals("File\u0394", new String(cdb.getCodeFile(path), "UTF8"));
        }
        catch (Exception e) {
            fail("Test 1 - Error adding code file: " + e.getMessage());
        }
    }
    
    /**
     * Tests {@link eu.iac3.mathMS.codesDBPlugin.CodesDB #addGeneratedCode(String, String, String, String)}.
     * There are the following tests:
     * 1- Insert test
     * 2- Duplicate insert test
     * 
     */
    @Test
    public void testAddGeneratedCode() {    	
        CodesDB cdb;
        String id = null;
        //Test 1- Insert test
        try {
            cdb = new CodesDBImpl();
            id = cdb.addGeneratedCode("testAddGeneratedCode", "Author", "Version", "Platrm");
            assertNotNull(id);
        }
        catch (DCException e) {
        	e.printStackTrace();
            fail("Test 1 - Error adding generated code: " + e.getMessage());
        }
        //Test 2- Duplicate insert test
        try {
            cdb = new CodesDBImpl();
            id = cdb.addGeneratedCode("testAddGeneratedCode", "Author", "Version", "Platrm");
            assertNotNull(id);
        }
        catch (DCException e) {
            fail("Test 2 - Error adding generated code: " + e.getMessage());
        }
    }
    
    /**
     * Tests {@link eu.iac3.mathMS.codesDBPlugin.CodesDB #modifyCodeFile(String, String)}.
     * There are the following tests:
     * 1- Modify existing file
     * 2- Modify non existing file. Must throw {@link eu.iac3.mathMS.codesDBPlugin.DCException #DC003}
     */
    @Test
    public void testModifyCodeFile() {
        CodesDB cdb;
        //Test 1- Modify existing file
        try {
            cdb = new CodesDBImpl();
            String id = cdb.addGeneratedCode("testModifyCodeFile", "test1", "Version", "Platrm");
            String path = cdb.addCodeFile(id, "Module", "Path", "File");
            cdb.modifyCodeFile(path, "File modified");
            assertEquals("File modified", new String(cdb.getCodeFile(path), "UTF8"));
        }
        catch (Exception e) {
            fail("Test 1 - Error modifying code file: " + e.getMessage());
        }
        //Test 2- Modify non existing file
        try {
            cdb = new CodesDBImpl();
            cdb.modifyCodeFile("NonExistingPath", "File modified");
            fail("Test 2 - Non existing file modified!!!");
        }
        catch (DCException e) {
            assertEquals(e.getErrorCode(), DCException.DC003);
        }
    }
    
    /**
     * Tests {@link eu.iac3.mathMS.codesDBPlugin.CodesDB #deleteGeneratedCode(String)}.
     * There are the following tests:
     * 1- Delete existing file
     * 2- Delete non existing file. Must throw {@link eu.iac3.mathMS.codesDBPlugin.DCException #DC006}
     */
    @Test
    public void testDeleteGeneratedCode() {
        CodesDB cdb;
        String id = null;
        String path = null;
        //Test 1- Delete existing file
        try {
            cdb = new CodesDBImpl();
            id = cdb.addGeneratedCode("testDeleteGeneratedCode", "test1", "Version", "Platrm");
            path = cdb.addCodeFile(id, "Module1", "Path", "File");
            cdb.addCodeFile(id, "Module2", "Path1", "File");
            cdb.addCodeFile(id, "Module3", "Path2", "File");
            cdb.deleteGeneratedCode(id);
            assertNull(cdb.getCodeFile(path));
            assertNull(cdb.getGeneratedCode(id));
        }
        catch (DCException e) {
            fail("Test 1 - Error deleting generated code: " + e.getMessage());
        }
        //Test 2- Delete non existing file
        try {
            cdb = new CodesDBImpl();
            cdb.deleteGeneratedCode("23465456436456435");
        }
        catch (DCException e) {
            fail("Test 2 - Non existing file cannot be deleted: " + e.getMessage());
        }
    }
    
    /**
     * Tests {@link eu.iac3.mathMS.codesDBPlugin.CodesDB #deleteCodeFile(String)}.
     * There are the following tests:
     * 1- Delete existing file
     * 2- Delete non existing file. Must throw {@link eu.iac3.mathMS.codesDBPlugin.DCException #DC003}
     */
    @Test
    public void testDeleteCodeFile() {
        CodesDB cdb;
        //Test 1- Delete existing file
        try {
            cdb = new CodesDBImpl();
            String id = cdb.addGeneratedCode("testDeleteCodeFile", "test1", "Version", "Platrm");
            String path = cdb.addCodeFile(id, "Module", "Path", "File");
            cdb.deleteCodeFile(path);
            assertNull(cdb.getCodeFile(path));
        }
        catch (DCException e) {
            fail("Test 1 - Error deleting code file: " + e.getMessage());
        }
        //Test 2- Delete non existing file
        try {
            cdb = new CodesDBImpl();
            cdb.deleteCodeFile("NonExistingPath");
        }
        catch (DCException e) {
        	fail("Test 2 - Non existing file cannot be deleted: " + e.getMessage());
        }
    }
    
    /**
     * Tests {@link eu.iac3.mathMS.codesDBPlugin.CodesDB #getCodeMeta(String)}.
     * There are the following tests:
     * 1- Getting meta from existing code
     * 2- Getting meta from non existing code
     */
    @Test
    public void testgetCodeMeta() {
        CodesDB cdb;
        //Test 1- Getting meta from existing code
        try {
            cdb = new CodesDBImpl();
            String id = cdb.addGeneratedCode("testGetCodeMeta", "test1", "Version", "Platrm");
            CodeResult cr = cdb.getCodeMeta(id);
            assertEquals("testGetCodeMeta", cr.getName());
            assertEquals("codeFiles" + File.separator + "testGetCodeMeta" + File.separator + "test1" + File.separator 
                    + "Version" + File.separator + "Platrm", cr.getPath().replaceAll("\\/[0-9]{14}", ""));
        }
        catch (DCException e) {
            fail("Test 1 - Error getting code meta: " + e.getMessage());
        }
        //Test 2- Getting meta from non existing code
        try {
            cdb = new CodesDBImpl();
            assertNull(cdb.getCodeMeta("987345868"));
        }
        catch (DCException e) {
            fail("Test 2 - Error getting code meta: " + e.getMessage());
        }
    }
    
    /**
     * Tests {@link eu.iac3.mathMS.codesDBPlugin.CodesDB #getCodeList(String, String, String, java.util.Date, java.util.Date, String, String)}.
     * There are the following tests:
     * 1- query all the parameters with result
     * 2- query some parameters with result
     * 3- query any parameter with result
     * 4- query with no result
     */
    @Test
    public void testGetCodeStructure() {
        CodesDB cdb;
        //Test 1- query all the parameters with result
        try {
            //Creation of the dates to compare
            cdb = new CodesDBImpl();
            String id = cdb.addGeneratedCode("testGetCodeStructure", "test1", "Version", "cactus");
            cdb.addCodeFile(id, "boundary", "Path" + File.separator + "seconfpath1" + File.separator + "finalfile.txt", "FileContent");
            cdb.addCodeFile(id, "boundary2", "Path" + File.separator + "seconfpath2" + File.separator + "finalfile.txt", "FileContent");
            cdb.addCodeFile(id, "boundary3", "Path" + File.separator + "seconfpath3" + File.separator + "finalfile.txt", "FileContent");
            
            String structure = cdb.getCodeFolderStructure(id);
            assertTrue(structure.indexOf("<mms:folderPath>Path" + File.separator + "seconfpath3</mms:folderPath>") > 0);
            assertTrue(structure.indexOf("<mms:folderPath>Path" + File.separator + "seconfpath2</mms:folderPath>") > 0);
            assertTrue(structure.indexOf("<mms:folderPath>Path" + File.separator + "seconfpath1</mms:folderPath>") > 0);
            
        }
        catch (DCException e) {
            fail("Test 2 - Error getting the code list: " + e.getMessage());
        }        
    }
}
