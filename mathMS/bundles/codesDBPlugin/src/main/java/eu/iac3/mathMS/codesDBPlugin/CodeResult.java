/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.codesDBPlugin;

import java.util.Date;

/** 
 * CodeResult is a JavaBean that represents the response file 
 * when querying the database a list of codes.
 * 
 * @author      ----
 * @version     ----
 */
public class CodeResult {
    //Id of the generated code
    private String id;
    //discretized problem name of the code
    private String name;
    //author of the code
    private String author;
    //version of the code
    private String version;
    //date creation of the code
    private Date date;
    //platformId of the code
    private String platformId;
    //path of the code
    private String path;
    
    /**
     * Constructor.
     * 
     * @param id            The id of the generated code
     * @param name          Name of code
     * @param author        Author
     * @param version       Version
     * @param date          Date
     * @param platformId    Platform id
     * @param path          Path of the file
     */
    public CodeResult(String id, String name, String author, String version, Date date, String platformId, String path) {
        this.id = id;
        this.name = name;
        this.author = author;
        this.version = version;
        this.date = date;
        this.platformId = platformId;
        this.path = path;
    }
   
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    //getters and setters of the bean
    public void setName(String codeName) {
        name = codeName;
    }

    public String getName() {
        return name;
    }

    public void setAuthor(String codeAuthor) {
        author = codeAuthor;
    }

    public String getAuthor() {
        return author;
    }


    public void setVersion(String codeVersion) {
        version = codeVersion;
    }

    public String getVersion() {
        return version;
    }

    public void setDate(Date codeDate) {
        date = codeDate;
    }

    public Date getDate() {
        return date;
    }

    public void setPlatformId(String codePlatformId) {
        platformId = codePlatformId;
    }

    public String getPlatformId() {
        return platformId;
    }

    public void setPath(String codePath) {
        path = codePath;
    }

    public String getPath() {
        return path;
    }
}

