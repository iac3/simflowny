/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.codesDBPlugin;

/** 
 * DCOException is the class Exception launched by the 
 * {@link CodesDB} interface.
 * The exceptions that this class return are:
 * DC001 Code already exists
 * DC002 Permission denied
 * DC003 File not found
 * DC004 Database permission denied
 * DC005 External error
 * 
 * @author      ----
 * @version     ----
 */
@SuppressWarnings("serial")
public class DCException extends Exception {

    public static final String DC001 = "Code already exists";
    public static final String DC002 = "Permission denied";
    public static final String DC003 = "File not found";
    public static final String DC004 = "Database permission denied"; //When the user control will be implemented
    public static final String DC005 = "External error";
    public static final String DC006 = "Generated code not found";
	
    //External error message
    private String extMsg;
	  /** 
	     * Constructor for known messages.
	     *
	     * @param code  the message	     
	     */
    DCException(String code) {
    	super(code);
    }
	  
	  /** 
	     * Constructor.
	     *
	     * @param code  the internal error message	    
	     * @param msg  the external error message
	     */
    DCException(String code, String msg) {
		super(code);
        this.extMsg = msg;
    }
	  /** 
     * Returns the error code.
     *
     * @return the error code	     
     */
    public String getErrorCode() {
    	return super.getMessage();
    }
	  /** 
	     * Returns the error message as a string.
	     *
	     * @return the error message	     
	     */
    public String getMessage() {
    	if (extMsg == null) {
            return "DCException: " + super.getMessage();
    	}
    	else {
            return "DCException: " + super.getMessage() + " (" + extMsg + ")";
    	}
    }  
}
