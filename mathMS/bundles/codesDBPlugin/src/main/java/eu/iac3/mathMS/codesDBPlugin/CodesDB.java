/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/

package eu.iac3.mathMS.codesDBPlugin;

import java.util.Date;

import javax.jws.WebService;

/** 
 * CodesDB is an interface that provides the functionality 
 * to manage the storage of the files with the code of the discretized problems.
 * <p>
 * The interface have the functions of adding, retrieving, and removing the files 
 * on the file system and manages its information in a relational database table.
 * 
 * @author      ----
 * @version     ----
 */
@WebService
public interface CodesDB {

    /** 
     * It returns a {@link CodeResultSet} with the generated codes
     * matching the query on the database of codes.
     * The empty parameters will be considered as no filtering data.
     *
     * @param name          discrete model name of the code
     * @param author        author name of the code
     * @param version       Version of the code
     * @param dateFrom      include codes created after this date
     * @param dateTo        include codes created before this date
     * @param platformId    identification of the platform of the code    
     * 
     * @return              It returns the result of querying as a {@link CodeResultSet}. 
     *                      Null if no result.
     * @throws DCException  DC005 - External error
     */
    CodeResult[] getCodeList(String name, String author, String version, Date dateFrom, Date dateTo, String platformId)
        throws DCException;
     
    /**
     * Get the files for a generated code.
     * @param id                The generated code id
     * @return                  An array of file paths
     * @throws DCException      DC005 - External error
     */
    String[] getGeneratedCode(String id) throws DCException;
    
    /** 
     * It returns the code file of the path.
     *
     * @param filePath         path of the file to be returned
     *   
     * @return                 It returns the content of the file requested or null if it does not exist
     * @throws DCException     DC005 - External error
     */
    byte[] getCodeFile(String filePath) throws DCException;
    
    /**
     * Add the header for a code that will be generated.
     * @param name              The name of the problem
     * @param author            The author
     * @param version           The version
     * @param platform          The simulation platform
     * @return                  The id assigned
     * @throws DCException      DC001 - code already exists
     *                          DC002 - permission denied
     *                          DC005 - external exception
     */
    String addGeneratedCode(String name, String author, String version, String platform) throws DCException;
    
    /** 
     * It inserts a new code file in the folder for the generated code id. 
     * It checks whether the code already exists and 
     * returns an exception otherwise.
     * If the folder does not exist the function creates it.
     *
     * @param codeId        The generated code id
     * @param module        The module of the code
     * @param fileName      The name of the file
     * @param codeFile      the code file to be inserted    
     * @return              the path where the file is stored
     * @throws DCException  DC001 - code already exists
     *                      DC002 - permission denied
     *                      DC005 - external exception
     */        
    String addCodeFile(String codeId, String module, String fileName, String codeFile) throws DCException;
    
    /** 
     * It inserts a new code file in the folder for the generated code id. 
     * It checks whether the code already exists and 
     * returns an exception otherwise.
     * If the folder does not exist the function creates it.
     *
     * @param codeId        The generated code id
     * @param module        The module of the code
     * @param fileName      The name of the file
     * @param codeFile      the code file to be inserted    
     * @return              the path where the file is stored
     * @throws DCException  DC001 - code already exists
     *                      DC002 - permission denied
     *                      DC005 - external exception
     */        
    String addCodeFile(String codeId, String module, String fileName, byte[] codeFile) throws DCException;
    
    /** 
     * It replaces the code file in the folder.     
     *
     * @param filePath          path of the file to be modified
     * @param codeFile          the code file to be inserted     
     *   
     * @throws DCException      DC003 - file not found
     *                          DC002 - permission denied
     */
    void modifyCodeFile(String filePath, String codeFile) throws DCException;
      
    /**
     * Delete the generated code and all its files.
     * @param codeId            The generated code to delete
     * @throws DCException      DC005 - external exception
     *                          DC006 - Generated code not found
     */
    void deleteGeneratedCode(String codeId) throws DCException;
    
    /** 
     * It deletes a code file from the specidied folder  .  
     *
     * @param filePath        path of the file to be deleted   
     *   
     * @throws DCException    DC003 - file not found
     *                        DC002 - permission denied
     */
    void deleteCodeFile(String filePath) throws DCException;   
    
    /**
     * Get the metadata of a generated code.
     * @param id                The id of the code
     * @return                  The metadata in a CodeResult class
     * @throws DCException      DC005 - External error
     */
    CodeResult getCodeMeta(String id) throws DCException;
    
    /**
     * Get the Folder structure of a generated code.
     * @param id                The generated code id
     * @return                  the folder structure
     * @throws DCException      DC005 - External error
     */
    String getCodeFolderStructure(String id) throws DCException;
}
