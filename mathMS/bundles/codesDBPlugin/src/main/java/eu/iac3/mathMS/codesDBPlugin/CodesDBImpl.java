/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.codesDBPlugin;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.file.Files;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;

import javax.sql.DataSource;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceReference;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.jdbc.DataSourceFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

/** 
 * CodesDBImpl implements the 
 * {@link CodesDB CodesDB} interface.
 * It has the functions of adding, retrieving, and removing the files 
 * on the file system and manages its information in a relational database table.
 *  
 * @author  ----
 * @version ----
 */
@Component //
(//
    immediate = true, //
    name = "CodesDB", //
    property = //
    { //
      "service.exported.interfaces=*", //
      "service.exported.configs=org.apache.cxf.ws", //
      "org.apache.cxf.ws.address=/CodesDB" //
    } //
)
public class CodesDBImpl implements CodesDB {
    static final String DB_NAME = "CodesDB";
    static final String DATASOURCE_NAME = "SIMFLOWNY_DATASOURCE";
    static final String MMSURI = "urn:mathms";
    static final String codeBaseFolder = "codeFiles";
    static BundleContext context;
    static DataSource dataSource;

    //Derby error codes
    static final int CREATION_PERMISION_DENIED = 40000;
    static final int INSERT_ERROR = 30000;
    static final String DUPLICATED_KEY = "23505";
    /**
     * Constructor for {@link CodesDBImpl}. It creates the database table for the plugin.
     * 
     * @throws DCException  DC002 - Permission denied to create the database
     *                      DC005 - External error
     */
    public CodesDBImpl() throws DCException {
    	context = FrameworkUtil.getBundle(getClass()).getBundleContext();
    }
    
   /** 
     * It returns a {@link CodeResultSet} with the result info
     * of querying the database of codes.
     * The empty parameters will be considered as no filtering data.
     *
     * @param name          discrete model name of the code
     * @param author        author name of the code
     * @param version       Version of the code
     * @param dateFrom      include codes created after this date
     * @param dateTo        include codes created before this date
     * @param platformId    identification of the platform of the code    
     * 
     * @return              It returns the result of querying as a {@link CodeResultSet}. 
     *                      Null if no result.
     * @throws DCException  DC005 - External error
     */
    public CodeResult[] getCodeList(String name, String author, String version, Date dateFrom, Date dateTo, String platformId)
        throws DCException {
        Connection conn = null;
        CodeResult[] crArray = null;
        final int andLength = 4;
        final int emptyWhereLength = 5;
        try {
            conn = getConnection();
            String whereClause = "WHERE";
            if (name != null) {
                whereClause = whereClause + " Name='" + removeSpecialChars(name) + "' AND";
            }
            if (author != null) {
                whereClause = whereClause + " Author='" + removeSpecialChars(author) + "' AND";
            }
            if (version != null) {
                whereClause = whereClause + " Version='" + removeSpecialChars(version) + "' AND";
            }
            if (dateFrom != null) {
                whereClause = whereClause + " Date>='" + (new java.sql.Timestamp(dateFrom.getTime())).toString() + "' AND";
            }
            if (dateTo != null) {
                whereClause = whereClause + " Date<='" + (new java.sql.Timestamp(dateTo.getTime())).toString() + "' AND";
            }
            if (platformId != null) {
                whereClause = whereClause + " Platform='" + platformId + "' AND";
            }
            //If no parameter the whereClause has to be empty
            if (whereClause.length() == emptyWhereLength) {
                whereClause = "";
            }
            else {
                //Delete the last AND.;
                whereClause = whereClause.substring(0, whereClause.length() - andLength);
            }
            
            //Querying the database with a scrollable statement to get the row count with one query
            Statement s = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = s.executeQuery("SELECT * FROM generatedCode " + whereClause);
            rs.last();
            //There is some result
            if (rs.getRow() > 0) {
                crArray = new CodeResult[rs.getRow()];
                rs.first();
                rs.previous();
                int i = 0;
                while (rs.next()) {
                    //Fill the CodeResult class with the values
                    CodeResult cr = new CodeResult(rs.getString("code_id"), rs.getString("Name"), rs.getString("Author"), rs.getString("Version"), 
                            new java.util.Date(rs.getDate("Date").getTime()), rs.getString("Platform"), rs.getString("Path"));
                    crArray[i] = cr;
                    i++;
                }
            }
            return crArray;
        }
        catch (SQLException e) {
            throw new DCException(DCException.DC005, e.getMessage());
        }
        catch (DCException e) {
            throw e;
        }
        finally {
            try {
                conn.close();
            }
            catch (Exception e) {
                throw new DCException(DCException.DC005, e.getMessage());
            }
        }
    }
     
    /**
     * Get the files for a generated code.
     * @param id                The generated code id
     * @return                  An array of file paths
     * @throws DCException      DC005 - External error
     */
    public String[] getGeneratedCode(String id) throws DCException {
        Connection conn = null;
        ArrayList<String> result = new ArrayList<String>();
        try {
            conn = getConnection();
            Statement s = conn.createStatement();
            ResultSet rs = s.executeQuery("SELECT path FROM file WHERE code_id=" + id + "");
            while (rs.next()) {
                result.add(rs.getString("path"));
            }
            if (result.size() == 0) {
                return null;
            }
            return result.toArray(new String[result.size()]);
        }
        catch (DCException e) {
            throw e;
        }
        catch (Exception e) {
            throw new DCException(DCException.DC005, e.getMessage());
        } 
        finally {
            try {
                conn.close();
            }
            catch (Exception e) {
                throw new DCException(DCException.DC005, e.getMessage());
            }
        }
    }
    
    /** 
     * It returns the code file of the path.
     *
     * @param filePath         path of the file to be returned
     *   
     * @return                 It returns the content of the file requested or null if it does not exist
     * @throws DCException     DC005 - External error
     */
    public byte[] getCodeFile(String filePath) throws DCException {
        Connection conn = null;
        try {
            conn = getConnection();
            Statement s = conn.createStatement();
            ResultSet rs = s.executeQuery("SELECT path FROM file WHERE Path='" + filePath + "'");
            //The file is in the database
            if (rs.next()) {
                File file = new File(filePath);
                if (file.exists()) {
                	return Files.readAllBytes(file.toPath());
                }
                //File not found on file system
                else {
                    return null;
                }
            }
            //File not found on DB
            else {
                return null;
            }
        }
        catch (DCException e) {
            throw e;
        }
        catch (Exception e) {
            throw new DCException(DCException.DC005, e.getMessage());
        }
        finally {
            try {
                conn.close();
            }
            catch (Exception e) {
                throw new DCException(DCException.DC005, e.getMessage());
            }
        }
    }
    
    /**
     * Add the header for a code that will be generated.
     * @param name              The name of the problem
     * @param author            The author
     * @param version           The version
     * @param platform          The simulation platform
     * @return                  The id assigned
     * @throws DCException      DC001 - code already exists
     *                          DC002 - permission denied
     *                          DC005 - external exception
     */
    public String addGeneratedCode(String name, String author, String version, String platform) throws DCException {
        Connection conn = null;
        Statement s = null;
        try {
        	Date javaDate = new Date();
            java.sql.Timestamp date = new java.sql.Timestamp(javaDate.getTime());
            
            //Create the path
            DateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
            String stringPath = codeBaseFolder + File.separator + removeSpecialChars(name) + File.separator + removeSpecialChars(author) 
                + File.separator + removeSpecialChars(version) + File.separator + removeSpecialChars(platform) + File.separator + formatter.format(javaDate); 
            //The stringPath needs to remove SQL characters because its value will be introduced in the database
            stringPath = removeSpecialCharsSQL(stringPath, 300);
            String nameSQL = removeSpecialCharsSQL(name, 50);
            String authorSQL = removeSpecialCharsSQL(author, 20);
            String versionSQL = removeSpecialCharsSQL(version, 20);
            String platformSQL = removeSpecialCharsSQL(platform, 20);
            //Creation of the folder path
            File path = new File(stringPath);
            path.mkdirs();
            
            //Registering to database
            conn = getConnection();
            s = conn.createStatement();
            
            //Check that the code does not exist
            ResultSet rs = s.executeQuery("SELECT code_id FROM generatedCode WHERE name='" + nameSQL + "' and author='" 
                    + authorSQL + "' and version='" + versionSQL + "' and platform='" + platformSQL + "' and date='" + date.toString() + "'");
            if (rs.next()) {
                throw new DCException(DCException.DC001);
            }
            //The data stored need to replace the sql special characters              
            s.executeUpdate("INSERT INTO generatedCode(Name, Author, Version, Date, Platform, Path) values("
                    + "'" + nameSQL + "',"
                    + "'" + authorSQL + "',"
                    + "'" + versionSQL + "',"
                    + "'" + date.toString() + "',"
                    + "'" + platformSQL + "',"
                    + "'" + stringPath + "')");
            rs = s.executeQuery("SELECT code_id FROM generatedCode WHERE name='" + nameSQL + "' and author='" 
                    + authorSQL + "' and version='" + versionSQL + "' and platform='" + platformSQL + "' and date='" + date.toString() + "'");
            rs.next();
            return rs.getString("code_id");
        }
        catch (SQLException e) {
            if (e.getErrorCode() == INSERT_ERROR && e.getSQLState().equals(DUPLICATED_KEY)) {
                throw new DCException(DCException.DC001, e.getMessage());
            }
            throw new DCException(DCException.DC005, e.getMessage());
        }
        catch (DCException e) {
            throw e;
        }
        finally {
            try {
                if (s != null) {
                    s.close();  
                }
                if (conn != null) {
                    conn.close();
                }
            }
            catch (Exception e) {
                throw new DCException(DCException.DC005, e.getMessage());
            }
        }
    }  
    
    /** 
     * It inserts a new code file in the folder for the generated code id. 
     * It checks whether the code already exists and 
     * returns an exception otherwise.
     * If the folder does not exist the function creates it.
     *
     * @param codeId        The generated code id
     * @param module        The module of the code
     * @param fileName      The name of the file
     * @param codeFile      the code file to be inserted    
     * @return              the path where the file is stored
     * @throws DCException  DC001 - code already exists
     *                      DC002 - permission denied
     *                      DC005 - external exception
     */       
    public String addCodeFile(String codeId, String module, String fileName, String codeFile) throws DCException {
        Connection conn = null;
        Statement s = null;
        try {
            java.sql.Timestamp date = new java.sql.Timestamp(new Date().getTime());
            
            //Registering to database
            conn = getConnection();
            s = conn.createStatement();
            //Query for the existence of generated code.
            String stringPath;
            ResultSet rs = s.executeQuery("SELECT path FROM generatedCode WHERE code_id=" + codeId);
            if (!rs.next()) {
                throw new DCException(DCException.DC006, "Id: " + codeId);
            }
            else {
                stringPath = rs.getString("path");
            }
            
            //The stringPath needs to remove SQL characters because its value will be introduced in the database
            stringPath = removeSpecialCharsSQL(stringPath, 300);
            //Creation of the folder path
            if (fileName.indexOf(File.separator) > 0) {
                File path = new File(stringPath + File.separator + removeSpecialCharsSQL(removeSpecialChars(
                        fileName.substring(0, fileName.lastIndexOf(File.separator))), 300));
                path.mkdirs();
            }
            else {
                File path = new File(stringPath);
                path.mkdirs();
            }
            //Creation of the file in the folder path
            stringPath = stringPath + File.separator + removeSpecialCharsSQL(removeSpecialChars(fileName), 300);
            File file = new File(stringPath);
            if (!file.exists()) {
                file.createNewFile();
            }
            //Overwrite the file
            BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(stringPath), "UTF8"));
            out.write(codeFile);
            out.close();
            
            //The data stored need to replace the sql special characters
            s.executeUpdate("INSERT INTO file(Date, Module, Path, Code_id) values("
        		+ "'" + date.toString() + "',"
        		+ "'" + removeSpecialCharsSQL(module, 20) + "',"
        		+ "'" + removeSpecialCharsSQL(stringPath, 300) + "',"
        		+ "" + codeId + ")");
            s.close();
            
            return stringPath;
        }
        catch (IOException e) {
            throw new DCException(DCException.DC002, e.getMessage());
        }
        catch (SQLException e) {
            if (e.getErrorCode() == INSERT_ERROR && e.getSQLState().equals(DUPLICATED_KEY)) {
                throw new DCException(DCException.DC001, e.getMessage());
            }
            throw new DCException(DCException.DC005, e.getMessage());
        }
        catch (DCException e) {
            throw e;
        }
        finally {
            try {
                conn.close();
            }
            catch (Exception e) {
                throw new DCException(DCException.DC005, e.getMessage());
            }
        }
    }  
    
    /** 
     * It inserts a new code file in the folder for the generated code id. 
     * It checks whether the code already exists and 
     * returns an exception otherwise.
     * If the folder does not exist the function creates it.
     *
     * @param codeId        The generated code id
     * @param module        The module of the code
     * @param fileName      The name of the file
     * @param codeFile      the code file to be inserted    
     * @return              the path where the file is stored
     * @throws DCException  DC001 - code already exists
     *                      DC002 - permission denied
     *                      DC005 - external exception
     */       
    public String addCodeFile(String codeId, String module, String fileName, byte[] codeFile) throws DCException {
        Connection conn = null;
        Statement s = null;
        try {
            java.sql.Timestamp date = new java.sql.Timestamp(new Date().getTime());
            
            //Registering to database
            conn = getConnection();
            s = conn.createStatement();
            //Query for the existence of generated code.
            String stringPath;
            ResultSet rs = s.executeQuery("SELECT path FROM generatedCode WHERE code_id=" + codeId);
            if (!rs.next()) {
                throw new DCException(DCException.DC006, "Id: " + codeId);
            }
            else {
                stringPath = rs.getString("path");
            }
            
            //The stringPath needs to remove SQL characters because its value will be introduced in the database
            stringPath = removeSpecialCharsSQL(stringPath, 300);
            //Creation of the folder path
            if (fileName.indexOf(File.separator) > 0) {
                File path = new File(stringPath + File.separator + removeSpecialCharsSQL(removeSpecialChars(
                        fileName.substring(0, fileName.lastIndexOf(File.separator))), 300));
                path.mkdirs();
            }
            else {
                File path = new File(stringPath);
                path.mkdirs();
            }
            //Creation of the file in the folder path
            stringPath = stringPath + File.separator + removeSpecialCharsSQL(removeSpecialChars(fileName), 300);
            File file = new File(stringPath);
            if (!file.exists()) {
                file.createNewFile();
            }
            //Overwrite the file
            try (FileOutputStream fos = new FileOutputStream(stringPath)) {
            	   fos.write(codeFile);
            	   fos.close();
            }
            
            //The data stored need to replace the sql special characters
            s.executeUpdate("INSERT INTO file(Date, Module, Path, Code_id) values("
        		+ "'" + date.toString() + "',"
        		+ "'" + removeSpecialCharsSQL(module, 20) + "',"
        		+ "'" + removeSpecialCharsSQL(stringPath, 300) + "',"
        		+ "" + codeId + ")");
            s.close();
            
            return stringPath;
        }
        catch (IOException e) {
            throw new DCException(DCException.DC002, e.getMessage());
        }
        catch (SQLException e) {
            if (e.getErrorCode() == INSERT_ERROR && e.getSQLState().equals(DUPLICATED_KEY)) {
                throw new DCException(DCException.DC001, e.getMessage());
            }
            throw new DCException(DCException.DC005, e.getMessage());
        }
        catch (DCException e) {
            throw e;
        }
        finally {
            try {
                conn.close();
            }
            catch (Exception e) {
                throw new DCException(DCException.DC005, e.getMessage());
            }
        }
    }  
    
    /** 
     * It replaces the code file in the folder.     
     *
     * @param filePath          path of the file to be modified
     * @param codeFile          the code file to be inserted     
     *   
     * @throws DCException      DC003 - file not found
     *                          DC002 - permission denied
     */
    public void modifyCodeFile(String filePath, String codeFile) throws DCException {
        Connection conn = null;
        try {
            conn = getConnection();
            Statement s = conn.createStatement();
            ResultSet rs = s.executeQuery("SELECT Path FROM file WHERE Path='" + filePath + "'");
            if (rs.next()) {
                //Overwrite the file
                BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filePath), "UTF8"));
                out.write(codeFile);
                out.close();
            }
            //File not found on DB
            else {
                throw new DCException(DCException.DC003, filePath + " not found on database");
            }
        }
        catch (IOException e) {
            throw new DCException(DCException.DC002, e.getMessage());
        }
        catch (SQLException e) {
            throw new DCException(DCException.DC005, e.getMessage());
        }
        catch (DCException e) {
            throw e;
        }
        finally {
            try {
                conn.close();
            }
            catch (Exception e) {
                throw new DCException(DCException.DC005, e.getMessage());
            }
        }
    }
   
    /**
     * Delete the generated code and all its files.
     * @param codeId            The generated code to delete
     * @throws DCException      DC005 - external exception
     *                          DC006 - Generated code not found
     */
    public void deleteGeneratedCode(String codeId) throws DCException {
        Connection conn = null;
        try {
            conn = getConnection();
            
            Statement s = conn.createStatement();
            //Delete all related code files
            ResultSet rs = s.executeQuery("SELECT path FROM file WHERE code_Id=" + codeId);
            while (rs.next()) {
                deleteCodeFile(rs.getString("path"), conn);
            }
            rs = s.executeQuery("SELECT code_Id FROM generatedCode WHERE code_Id=" + codeId);
            if (rs.next()) {
                //Delete code reference
                s.executeUpdate("DELETE FROM generatedCode WHERE code_Id=" + codeId);
            }
            
        }
        catch (SQLException e) {
            throw new DCException(DCException.DC005, e.getMessage());
        }
        finally {
            try {
                conn.close();
            }
            catch (Exception e) {
                throw new DCException(DCException.DC005, e.getMessage());
            }
        }
    }
    
    /** 
     * It deletes a code file from the specidied folder  .  
     *
     * @param filePath        path of the file to be deleted   
     *   
     * @throws DCException    DC003 - file not found
     *                        DC002 - permission denied
     */
    public void deleteCodeFile(String filePath) throws DCException {
        Connection conn = null;
        try {
            conn = getConnection();
            
            Statement s = conn.createStatement();
            ResultSet rs = s.executeQuery("SELECT Path FROM file WHERE Path='" + filePath + "'");
            if (rs.next()) {
                //Overwrite the file
                File file = new File(filePath);
                if (!file.delete()) {
                    throw new DCException(DCException.DC002, filePath);
                }
                s.executeUpdate("DELETE FROM file WHERE Path='" + filePath + "'");
                //Delete the parent empty folders
                deleteEmptyFolders(filePath.substring(0, filePath.lastIndexOf(File.separator)));
            }
        }
        catch (SQLException e) {
            throw new DCException(DCException.DC005, e.getMessage());
        }
        catch (DCException e) {
            throw e;
        }
        finally {
            try {
                conn.close();
            }
            catch (Exception e) {
                throw new DCException(DCException.DC005, e.getMessage());
            }
        }
    }
    
    /** 
     * It deletes a code file from the specidied folder  .  
     *
     * @param filePath        path of the file to be deleted   
     * @param conn			  connection to database   
     * @throws DCException    DC003 - file not found
     *                        DC002 - permission denied
     */
    private void deleteCodeFile(String filePath, Connection conn) throws DCException {
        try {
            Statement s = conn.createStatement();
            ResultSet rs = s.executeQuery("SELECT Path FROM file WHERE Path='" + filePath + "'");
            if (rs.next()) {
                //Overwrite the file
                File file = new File(filePath);
                file.delete();
                s.executeUpdate("DELETE FROM file WHERE Path='" + filePath + "'");
                //Delete the parent empty folders
                deleteEmptyFolders(filePath.substring(0, filePath.lastIndexOf(File.separator)));
            }
        }
        catch (SQLException e) {
            throw new DCException(DCException.DC005, e.getMessage());
        }
    }
    
    /**
     * Establish the connection with the DB.
     * @throws DCException      DC002 - Permission denied to create the database
     *                          DC005 - External error
     */
    public void createDB() throws DCException {
        try {        	
        	@SuppressWarnings("rawtypes")
        	//Get jdbc embedded driver instead of default one
        	String filter = "(osgi.jdbc.driver.class=org.apache.derby.jdbc.EmbeddedDriver)";
			ServiceReference[] serviceReference = context.getServiceReferences(DataSourceFactory.class.getName(), filter);
        	@SuppressWarnings("unchecked")
			DataSourceFactory factory = (DataSourceFactory) context.getService(serviceReference[0]);
        	
        	Properties props = new Properties(); 
        	props.setProperty(DataSourceFactory.JDBC_DATABASE_NAME, DB_NAME);
        	//props.setProperty(DataSourceFactory.JDBC_DATASOURCE_NAME, DATASOURCE_NAME);
        	
        	dataSource = factory.createDataSource(props);
        	System.out.println(dataSource.toString());
        	Driver dr = factory.createDriver(props);
        	Connection con = dr.connect("jdbc:derby:" + DB_NAME + ";create=true;", null);
        	try {
                Statement s = con.createStatement();
                //Check if the table already exists
                ResultSet rs = s.executeQuery("SELECT TABLENAME FROM sys.systables where TABLENAME='GENERATEDCODE'");
                //If it does not exist then create it 
                if (!rs.next()) {
                    s.executeUpdate("CREATE TABLE generatedCode(Code_id INT GENERATED ALWAYS AS IDENTITY PRIMARY KEY, Name VARCHAR(50) NOT NULL, "
                            + "Author VARCHAR(20) NOT NULL, Version VARCHAR(20) NOT NULL, Date TIMESTAMP NOT NULL, Platform VARCHAR(15) NOT NULL, " 
                            + "Path VARCHAR(300) NOT NULL)");
                }
                //Check if the table already exists
                rs = s.executeQuery("SELECT TABLENAME FROM sys.systables where TABLENAME='FILE'");
                //If it does not exist then create it 
                if (!rs.next()) {
                    s.executeUpdate("CREATE TABLE file(Date TIMESTAMP NOT NULL, Module VARCHAR(20) NOT NULL, Path VARCHAR(300) NOT NULL, "
                            + "Code_id INT NOT NULL, PRIMARY KEY (Path), Foreign Key (Code_id) references generatedCode(Code_id))");
                }

                s.close();
            }  
            catch (Exception e)  {
            	e.printStackTrace();
                throw new DCException(DCException.DC005, e.getMessage());
            }
        }
        catch (SQLException e)  {
            if (e.getErrorCode() == CREATION_PERMISION_DENIED && e.getSQLState().equals("XJ041")) {
                throw new DCException(DCException.DC002, DB_NAME);
            }
        	e.printStackTrace();
            throw new DCException(DCException.DC005, e.getMessage());
        } 
        catch (Exception e) {
        	e.printStackTrace();
            throw new DCException(DCException.DC005, e.getMessage());
        }
    }
    
    /**
     * Establish the connection with the DB.
     * @return                  The connection
     * @throws DCException      DC002 - Permission denied to create the database
     *                          DC005 - External error
     */
    Connection getConnection() throws DCException {
        try {
            return dataSource.getConnection();
        }
        catch (SQLException e)  {
            if (e.getErrorCode() == CREATION_PERMISION_DENIED && e.getSQLState().equals("XJ041")) {
                throw new DCException(DCException.DC002, DB_NAME);
            }
            throw new DCException(DCException.DC005, e.getMessage());
        } 
        catch (Exception e) {
        	e.printStackTrace();
            throw new DCException(DCException.DC005, e.getMessage());
        }
    }
    
    /**
     * Remove reserved characters for file systems (Unix, Windows, Mac).
     * Also protect sql from special characters.
     * 
     * @param in    The string to remove characters
     * @return      The resulting string
     */
    String removeSpecialChars(String in) {
        //File system special characters
        in = in.replaceAll(":", "");
        in = in.replaceAll("\\*", "");
        in = in.replaceAll("\\?", "");
        in = in.replaceAll("<", "");
        in = in.replaceAll(">", "");
        in = in.replaceAll("\\|", "");
        in = in.replaceAll("\\(", "");
        in = in.replaceAll("\\)", "");
        return in;
    }
    
    /**
     * Protect sql from special characters.
     * 
     * @param in    The string to remove characters
     * @param limit Character limit in database
     * @return      The resulting string
     */
    String removeSpecialCharsSQL(String in, int limit) {
        //SQL special characters
        in = in.replaceAll("/\\*", "");
        in = in.replaceAll("\\*/", "");
        in = in.replaceAll("'", "''");
        in = in.replaceAll("--", "");
        in = in.replaceAll("\\|", "");
        in = in.replaceAll("\\(", "");
        in = in.replaceAll("\\)", "");
        if (in.length() >= limit) {
            in = in.substring(0, limit);
        }
        return in;
    }
    
    /**
     * Deletes if the parent folders if empty after a file deletion.
     * @param path  The path to clean
     */
    void deleteEmptyFolders(String path) {
        if (path != null) {
            File folder = new File(path);
            if (folder.list().length == 0) {
                folder.delete();
                if (path.lastIndexOf(File.separator) > 0) {
                    deleteEmptyFolders(path.substring(0, path.lastIndexOf(File.separator)));
                }
            }
        }
    }
    
    /**
     * Get the metadata of a generated code.
     * @param id                The id of the code
     * @return                  The metadata in a CodeResult class
     * @throws DCException      DC005 - External error
     */
    public CodeResult getCodeMeta(String id) throws DCException {
        Connection conn = null;
        try {
            conn = getConnection();
            Statement s = conn.createStatement();
            ResultSet rs = s.executeQuery("SELECT Name, Author, Version, Date, Platform, Path FROM generatedCode WHERE code_id=" + id + "");
            if (!rs.next()) {
                return null;
            }
            else {
                return new CodeResult(id, rs.getString("Name"), rs.getString("Author"), rs.getString("Version"), 
                        new java.util.Date(rs.getDate("Date").getTime()), rs.getString("Platform"), rs.getString("Path"));
            }
        }
        catch (Exception e) {
            throw new DCException(DCException.DC005, e.getMessage());
        } 
        finally {
            try {
                conn.close();
            }
            catch (Exception e) {
                throw new DCException(DCException.DC005, e.getMessage());
            }
        }
    }

    /**
     * Get the Folder structure of a generated code.
     * @param id                The generated code id
     * @return                  the folder structure
     * @throws DCException      DC005 - External error
     */
    public String getCodeFolderStructure(String id) throws DCException {
		
        DocumentBuilder builder;
        Document doc = null;
        try {
    		//Creation of the root element
            builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            doc = builder.newDocument();
            Node root = doc.createElementNS(MMSURI, "folderStructure");
            root.setPrefix("mms");
            doc.appendChild(root);
            
            //Obtain code metadata
            CodeResult cResult = getCodeMeta(id);
            if (cResult != null) {
            	IsFolder isFolder = new IsFolder();
            	String basePath = cResult.getPath();
            	File folder = new File(basePath);
            	if (folder.exists()) {
                    File[] folders = folder.listFiles(isFolder);
                    for (int i = 0; i < folders.length; i++) {
                        Node folderXml = getCodeFolderStructureNode(folders[i], doc, basePath);
                        root.appendChild(folderXml);
                    }
            	}
            }
            
        } 
        catch (ParserConfigurationException e) {
            throw new DCException(DCException.DC005, e.getMessage());
        }
        return domToString(doc);
    }
    
    /**
     * Get the Folder subStructure of a given folder.
     * @param folder    the folder
     * @param doc       The main xml document
     * @param basePath  The base path
     * @return          the subFolder structure
     */
    private Node getCodeFolderStructureNode(File folder, Document doc, String basePath) {
    	//new folder
    	Node root = doc.createElementNS(MMSURI, "folder");
        root.setPrefix("mms");
	    //folder name
        Node name = doc.createElementNS(MMSURI, "folderName");
        name.setPrefix("mms");
        name.setTextContent(folder.getName());
        root.appendChild(name);
        //folder path
        Node path = doc.createElementNS(MMSURI, "folderPath");
        path.setPrefix("mms");
        //creates the folder path
        String folderPath = folder.getPath();
        //folderPath = folderPath.replaceFirst(basePath, "");
        folderPath = folderPath.substring(basePath.length() + 1);
        path.setTextContent(folderPath);
        root.appendChild(path);
	    
	    //subfolders
        IsFolder isFolder = new IsFolder();
        File[] folders = folder.listFiles(isFolder);
        if (folders.length > 0) {
            Node subFolders = doc.createElementNS(MMSURI, "subFolders");
    	    subFolders.setPrefix("mms");    	    	    	
    	    for (int i = 0; i < folders.length; i++) {
                Node folderXml = getCodeFolderStructureNode(folders[i], doc, basePath);
                subFolders.appendChild(folderXml);
            }
            root.appendChild(subFolders);
        }
        return root;
    }
	
    /**
     * Converts a Dom to string.
     * 
     * @param doc 				The dom document
     * @return					The String
     * @throws DCException 	  DC00X - External error
     */
    String domToString(Document doc) throws DCException {
    	try {
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            //StreamResult result = new StreamResult(new StringWriter());
            StreamResult result = new StreamResult(new ByteArrayOutputStream());
            DOMSource source = new DOMSource(doc);
            transformer.transform(source, result);
            return new String(((ByteArrayOutputStream) result.getOutputStream()).toByteArray(), "UTF-8");
    	}
    	catch (Exception e) {
    	    throw new DCException(DCException.DC005, e.getMessage());
    	}
    }
    
    /**
     * FileFilter, return only the directory files.
     */
    class IsFolder implements FileFilter {
        /**
         * Returns true if the given file is a directory.
         * @param arg0 the file to check
         * @return true if the file is a directory
         */
        public boolean accept(File arg0) {			
            return arg0.isDirectory();
        }
    	
    }
}
