/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.documentManagerPluginTest;

import eu.iac3.mathMS.codesDBPlugin.CodesDB;
import eu.iac3.mathMS.codesDBPlugin.CodesDBImpl;
import eu.iac3.mathMS.documentManagerPlugin.DMException;
import eu.iac3.mathMS.documentManagerPlugin.DocumentManagerImpl;
import eu.iac3.mathMS.documentManagerPlugin.utils.DocumentManagerUtils;
import eu.iac3.mathMS.simflownyUtilsPlugin.SUException;
import eu.iac3.mathMS.simflownyUtilsPlugin.SimflownyUtils;
import eu.iac3.mathMS.simflownyUtilsPlugin.SimflownyUtilsImpl;

import static org.ops4j.pax.exam.CoreOptions.bootDelegationPackages;
import static org.ops4j.pax.exam.CoreOptions.junitBundles;
import static org.ops4j.pax.exam.CoreOptions.mavenBundle;
import static org.ops4j.pax.exam.CoreOptions.options;
import static org.ops4j.pax.exam.CoreOptions.systemProperty;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;
import org.junit.runner.RunWith;
import org.ops4j.pax.exam.Configuration;
import org.ops4j.pax.exam.Option;
import org.ops4j.pax.exam.junit.PaxExam;
import org.ops4j.pax.exam.spi.reactors.ExamReactorStrategy;
import org.ops4j.pax.exam.spi.reactors.PerSuite;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/** 
 * PhysicalModelsTest class is a junit testcase to
 * test the {@link eu.iac3.mathMS.documentManagerPlugin.DocumentManager}.
 * 
 * @author      iac3Team
 * @version     1.0
 */
@RunWith(PaxExam.class)
@ExamReactorStrategy(PerSuite.class)
public class DocumentManagerTest {
    
    long startTime;
    
    @Rule 
    public TestName name = new TestName();
    
    /**
     * Configuration for Pax-Exam.
     * @return options
     */
    @Configuration
    public Option[] config() {
        return options(
    		bootDelegationPackages(
    				 "com.sun.*",
    				 "javax.management",
    				 "javax.naming"
    				),
        	systemProperty("file.encoding").value("UTF-8"),
        	systemProperty("derby.system.home").value("target/Derby"),
        	systemProperty("exist.initdb").value("true"),
        	systemProperty("exist.home").value("."),
        	systemProperty("dataSourceName").value("SIMFLOWNY_DATASOURCE"),
        	systemProperty("ANTLR_USE_DIRECT_CLASS_LOADING").value("true"),
        	mavenBundle("org.apache.felix", "org.osgi.compendium", "1.4.0"),
        	mavenBundle("org.apache.felix", "org.apache.felix.log", "1.0.1"),
            mavenBundle("org.apache.commons", "commons-lang3", "3.7"),
            mavenBundle("org.apache.commons", "commons-text", "1.3"),
            mavenBundle("org.osgi", "org.osgi.enterprise", "5.0.0"), 
            mavenBundle("org.apache.felix", "org.apache.felix.scr.annotations", "1.12.0"),
            mavenBundle("org.apache.felix", "org.apache.felix.scr", "2.1.0"),
            mavenBundle("org.osgi", "org.osgi.dto", "1.1.0"),
            mavenBundle("org.osgi", "org.osgi.core", "6.0.0"),
            mavenBundle("eu.iac3.thirdParty.bundles", "xml-apis", "1.0"),
            mavenBundle("org.apache.servicemix.bundles", "org.apache.servicemix.bundles.relaxngDatatype", "2011.1_1"),
            mavenBundle("org.apache.servicemix.bundles", "org.apache.servicemix.bundles.xmlresolver", "1.2_5"),
            mavenBundle("org.apache.servicemix.bundles", "org.apache.servicemix.bundles.saxon", "9.8.0-10_1"),
            mavenBundle("org.apache.servicemix.bundles", "org.apache.servicemix.bundles.derby", "10.12.1.1_1"),
            mavenBundle("org.ops4j.pax.jdbc", "pax-jdbc-derby", "1.3.0"), 
            mavenBundle("eu.iac3.thirdParty.bundles.xsom", "xsom", "20140925"),
            mavenBundle("com.google.code.gson", "gson", "2.8.4"),
            mavenBundle("org.json", "json", "20180130"),
            mavenBundle("eu.iac3.thirdParty.bundles.snuggletex", "snuggletex-core-upconversion", "1.2.2"),
            mavenBundle("com.fasterxml.jackson.core", "jackson-core", "2.10.3"),
            mavenBundle("com.fasterxml.jackson.core", "jackson-annotations", "2.10.3"),
            mavenBundle("com.fasterxml.jackson.core", "jackson-databind", "2.10.3"),
            mavenBundle("eu.iac3.thirdParty.bundles.underscore", "underscore", "1.60"),
            mavenBundle("org.yaml", "snakeyaml", "1.26"),
            mavenBundle("com.fasterxml.jackson.dataformat", "jackson-dataformat-yaml", "2.10.3"),
            mavenBundle("org.mozilla", "rhino", "1.7.12"),
            mavenBundle("eu.iac3.mathMS", "simflownyUtilsPlugin", "3.1"),
        	mavenBundle("org.apache.felix", "org.apache.felix.configadmin", "1.9.0"),
        	mavenBundle("org.ops4j.pax.confman", "pax-confman-propsloader", "0.2.3"),
            mavenBundle("eu.iac3.mathMS", "codesDBPlugin", "3.1"),
            mavenBundle("org.ops4j.pax.logging", "pax-logging-api", "1.10.1"),
            mavenBundle("org.ops4j.pax.logging", "pax-logging-service", "1.10.1"),
            mavenBundle("org.ops4j.pax.logging", "pax-logging-log4j2", "1.10.1"),
            mavenBundle("com.mchange.c3p0", "com.springsource.com.mchange.v2.c3p0", "0.9.1.2"),
            mavenBundle("com.zaxxer", "HikariCP-java6", "2.3.13"),
            mavenBundle("org.quartz-scheduler", "quartz", "2.3.0"),
            mavenBundle("org.apache.servicemix.bundles", "org.apache.servicemix.bundles.regexp", "1.3_3"),
            mavenBundle("org.apache.servicemix.bundles", "org.apache.servicemix.bundles.lucene", "4.10.4_1"),
            mavenBundle("org.apache.servicemix.bundles", "org.apache.servicemix.bundles.lucene-sandbox", "4.10.4_1"),
            mavenBundle("org.apache.servicemix.bundles", "org.apache.servicemix.bundles.lucene-queries", "4.10.4_1"),
            mavenBundle("org.apache.servicemix.bundles", "org.apache.servicemix.bundles.lucene-queryparser", "4.10.4_1"),
            mavenBundle("org.apache.servicemix.bundles", "org.apache.servicemix.bundles.lucene-analyzers-common", "4.10.4_1"),
            mavenBundle("eu.iac3.thirdParty.bundles.exist", "exist", "4.1"),
            mavenBundle("eu.iac3.mathMS", "eXistDBWrapper", "3.1"),
            junitBundles()
            );
    }


        
    /**
     * An auxiliary function to read a file as String.
     * 
     * @param path  The path of the file
     * @return      The resource as string
     * @throws UnsupportedEncodingException Error
     */
    public String convertStreamToString(String path) throws UnsupportedEncodingException {
        InputStream is = getClass().getResourceAsStream(path);
        BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } 
        catch (IOException e) {
            e.printStackTrace();
        } 
        finally {
            try {
                is.close();
            } 
            catch (IOException e) { 
                e.printStackTrace();
            }
        }
        return sb.toString();
    }
    
    /**
     * Initial setup for the tests.
     */
    @Before
    public void onSetUp() {
        startTime = System.currentTimeMillis();
        DocumentManagerImpl pmi = null;
        try {
        	pmi = new DocumentManagerImpl();
			pmi.createBasicStructure();
		} 
        catch (DMException e) {
		}
    }
    
    /**
     * Close browser when last test.
     */
    @After
    public void onTearDown() {
        System.out.println(name.getMethodName() + ": " + (((float)(System.currentTimeMillis() - startTime)) / 60000));
    }
    
    /**
     * Includes 4 tests:
     * 1 - Basic collection
     * 2 - Complex collection
     * 3 - Complex collection without his base collection. Must create it too.
     * 4 - Create a colletion that exists. Must throw {@link eu.iac3.mathMS.documentManagerPlugin.DMException #DDS006}
     */
    @Test
    public void testAddCollection() {
        DocumentManagerImpl pmi = null;
        try {
            pmi = new DocumentManagerImpl();
       
            //Test 1: Basic collection
            pmi.addCollection("testAddCollection", "");
            
            //Test 2: Complex collection
            pmi.addCollection("test11", "testAddCollection");
            pmi.addCollection("test12", "testAddCollection");
            pmi.addCollection("test121", "testAddCollection/test12");
            
            //Test 3: Complex collection without his base collection. Must create it too.
            pmi.addCollection("test2/test21", "");
        }
        catch (Exception e) {
        	e.printStackTrace(System.out);
            fail("Error creating collections: " + e.toString());
        }
        try {
            pmi = new DocumentManagerImpl();
            //Test 4: Create a colletion that exists
            pmi.addCollection("test21", "test2");
            fail("Error creating collections: An existing collection has been created again!!!");
        }
        catch (DMException e) {
            assertEquals(DMException.DM006, e.getErrorCode());
        }
        finally {
            try {
	            pmi.deleteCollection("test2");
			}
            catch (DMException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
    }
    
    /**
     * Includes 1 test:
     * 1 - Test structure.
     * 
     */
    @Test
    public void testGetCollectionTree() {
        DocumentManagerImpl pmi = null;
        try {
            pmi = new DocumentManagerImpl();
            
            //Test 1: Test structure
            String structure = pmi.getCollectionTree();
            assertNotNull(structure);
        }
        catch (Exception e) {
            fail("Error getting the collection tree: " + e.toString());
        }
    }
    
    /**
     * Test of {@link eu.iac3.mathMS.documentManagerPlugin.DocumentManager #deleteCollection(String)}.
     * Includes 2 tests:
     * 1 - Delete a collection
     * 2 - Try to delete a collection that does not exist
     */
    @Test
    public void testDeleteCollection() {
        DocumentManagerImpl pmi = null;
        try {
            //Test 1: Delete a collection
            pmi = new DocumentManagerImpl();
            pmi.addCollection("testdelete", "");
            pmi.addCollection("newCol", "testdelete");
            pmi.deleteCollection("testdelete/newCol");
            //If the collection has been deleted then the following collection does not exist and could be created without throwing an exception.
            pmi.addCollection("newCol", "testdelete");
        }
        catch (Exception e) {
            fail("Error deleting collection: " + e.toString());
        }
        try {
            //Test 2: Try to move a collection to another that already exists
            pmi = new DocumentManagerImpl();
            pmi.deleteCollection("fdnkfs");
            fail("Error deleting collection: deleted a collection that not exists!!!!!!!!!!!!!!1");
        }
        catch (DMException e) {
            assertEquals(DMException.DM007, e.getErrorCode());
        }
    }
    
    /**
     * Includes 3 tests:
     * 1 - A new document
     * 2 - A new document with not valid XML content
     * 3 - A new document that already exist.
     * @throws DMException 
     */
    @Test
    public void testAddDocument() throws DMException {
        DocumentManagerImpl pmi = null;
        String id = "";
        try {
            //Test configuration
            pmi = new DocumentManagerImpl();
            pmi.addCollection("AddDiscretizationSchema", "");
            
            //Test 1: Adding a new document
            String doc = convertStreamToString("/document.json");
            String newDoc = pmi.addDocument(doc, "AddDiscretizationSchema");
            assertNotNull(newDoc);
            Document result = DocumentManagerUtils.stringToDom(SimflownyUtils.jsonToXML(newDoc));
            id = ((Element) result.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
        }
        catch (Exception e) {
            fail("Error adding document: " + e.toString());
        }
        try {
            pmi = new DocumentManagerImpl();
            //Test 2: Adding a new document with not valid syntax
            String xmlDoc = convertStreamToString("/document_wrong.json");
            pmi.addDocument(xmlDoc, "AddDiscretizationSchema");
            fail("DM002 should have been thrown");
        }
        catch (DMException e) {
            assertEquals(DMException.DM002, e.getErrorCode());
        }
        catch (Exception e) {
            fail("Error adding document: " + e.toString());
        }
        try {
            pmi = new DocumentManagerImpl();
            //Test 3: A new document that already exist
            String xmlDoc = convertStreamToString("/document.json");
            pmi.addDocument(xmlDoc, "AddDiscretizationSchema");
            fail("DM005 should have been thrown");
        }
        catch (DMException e) {
            assertEquals(DMException.DM005, e.getErrorCode());
        }
        catch (Exception e) {
            fail("Error adding document: " + e.toString());
        }
        pmi.deleteDocument(id);
    }
    
    /**
     * Includes 2 tests:
     * 1 - List documents
     * 2 - List with filtering
     * 3 - List from non existing path
     * 4 - List with wrong filter expression.
     * @throws DMException 
     * 
     */
    @Test
    public void testDocumentList() throws DMException {
        DocumentManagerImpl pmi = null;
        String id = "";
        String id2 = "";
        String id3 = "";
        try {
            //Test configuration
            pmi = new DocumentManagerImpl();
            pmi.addCollection("docList", "");
            pmi.addCollection("docList2", "docList");
            
            //Test 1: List documents
            String doc = convertStreamToString("/document.json");
            String newDoc = pmi.addDocument(doc, "docList");
            Document result = DocumentManagerUtils.stringToDom(SimflownyUtils.jsonToXML(newDoc));
            id = ((Element) result.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
            doc = SimflownyUtils.xmlToJSON(convertStreamToString("/dependency_model.xml"), false);
            newDoc = pmi.addDocument(doc, "docList/docList2");
            result = DocumentManagerUtils.stringToDom(SimflownyUtils.jsonToXML(newDoc));
            id2 = ((Element) result.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
            doc = convertStreamToString("/document2.json");
            newDoc = pmi.addDocument(doc, "docList");
            result = DocumentManagerUtils.stringToDom(SimflownyUtils.jsonToXML(newDoc));
            id3 = ((Element) result.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();

            String list = pmi.getDocumentList("docList");
            
            Pattern pattern = Pattern.compile("\"id\"");
            Matcher matcher = pattern.matcher(list);

            int count = 0;
            while (matcher.find()) {
                count++;
            }
            assertEquals(count, 2);
        }
        catch (Exception e) {
        	e.printStackTrace(System.out);
            fail("Error getting document list: " + e.toString());
        }
        try {
            pmi = new DocumentManagerImpl();
            //Test 3 - List from non existing path
            pmi.getDocumentList("nonExistent");
            fail("DM007 should have been thrown");
        }
        catch (DMException e) {
            assertEquals(DMException.DM007, e.getErrorCode());
        }
        pmi.deleteDocument(id);
        pmi.deleteDocument(id2);
        pmi.deleteDocument(id3);
    }
    
    /**
     * Tests:
     * 1 - Import x3d.
     * @throws SUException 
     * @throws DMException 
     */
    @Test
    public void testImportX3D() throws SUException, DMException {
        DocumentManagerImpl pmi = null;
        String id = "";
        try {
            //Test 1: Import x3d
            pmi = new DocumentManagerImpl();
            pmi.addCollection("importx3d", "");
            
            String metaInfo = SimflownyUtils.xmlToJSON(convertStreamToString("/metaSegment.xml"), false);
            String x3d = convertStreamToString("/x3d.x3d");
            String newDoc = pmi.importX3D(metaInfo, x3d, "importx3d");
            Document result = DocumentManagerUtils.stringToDom(SimflownyUtils.jsonToXML(newDoc));
            id = ((Element) result.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
            
            //Check meta data
            assertEquals(newDoc, pmi.getDocument(id));
        }
        catch (Exception e) {
            fail("Error importing x3d: " + e.toString());
        }
        pmi.deleteDocument(id);
    }
    
    /**
     * Tests:
     * 1 - Read x3d.
     * 2 - Try to read non-existing x3d
     * @throws SUException 
     * @throws DMException 
     */
    @Test
    public void testReadX3D() throws SUException, DMException {
        DocumentManagerImpl pmi = null;
        String id = "";
        try {
            //Test 1: Read x3d
            pmi = new DocumentManagerImpl();
            pmi.addCollection("readx3d", "");
            
            String metaInfo = SimflownyUtils.xmlToJSON(convertStreamToString("/metaSegment.xml"), false);
            String x3d = convertStreamToString("/x3d.x3d");
            String newDoc = pmi.importX3D(metaInfo, x3d, "readx3d");
            Document result = DocumentManagerUtils.stringToDom(SimflownyUtils.jsonToXML(newDoc));
            id = ((Element) result.getDocumentElement()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
            
            //Check meta data
            assertEquals(x3d, pmi.getX3D(id));
        }
        catch (Exception e) {
            fail("Error getting x3d: " + e.toString());
        }
        try {
            //Test 2: Try to read non-existing x3d
            pmi = new DocumentManagerImpl();
            pmi.getX3D("nonexist");
            fail("DM004 should have been thrown");
        }
        catch (DMException e) {
            assertEquals(DMException.DM004, e.getErrorCode());
        }
        catch (Exception e) {
            fail("Error getting x3d: " + e.toString());
        }
        pmi.deleteDocument(id);
    }
    
    /**
     * Tests the duplication of documents.
     * 1 - Duplicate a document
     * 2 - Duplicate a X3D document
     * @throws SUException  
     * @throws DMException 
     */
    @Test
    public void testDuplicateDocument() throws SUException, DMException {
        DocumentManagerImpl pmi = null;
        String originalId = "";
        String newId = "";
        try {
            //Test  1 - Duplicate a document
            pmi = new DocumentManagerImpl();
            pmi.addCollection("DuplicateDocument", "");
            String doc = convertStreamToString("/document.json");
            String newDoc = pmi.addDocument(doc, "DuplicateDocument");
            Document result = DocumentManagerUtils.stringToDom(SimflownyUtils.jsonToXML(newDoc));
            originalId = ((Element) result.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
            //Test 1: Delete an existing document
            Document duplicatedDocument = DocumentManagerUtils.stringToDom(SimflownyUtils.jsonToXML(pmi.duplicateDocument(originalId)));
            newId = ((Element) duplicatedDocument.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
            
            
            assertFalse(originalId.equals(newId));
            assertTrue(DocumentManagerUtils.domToString(result).equals(
                    DocumentManagerUtils.domToString(duplicatedDocument).replaceFirst(newId, originalId).replace("version>2<", "version>1<")));
        }
        catch (Exception e) {
            fail("Error duplicating document: " + e.toString());
        }
        pmi.deleteDocument(originalId);
        pmi.deleteDocument(newId);
        try {
            //Test  2 - Duplicate a X3D document
            String metaInfo = SimflownyUtils.xmlToJSON(convertStreamToString("/metaSegment.xml"), false);
            String x3d = convertStreamToString("/x3d.x3d");
            String newDoc = pmi.importX3D(metaInfo, x3d, "DuplicateDocument");
            Document result = DocumentManagerUtils.stringToDom(SimflownyUtils.jsonToXML(newDoc));
            originalId = ((Element) result.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
            Document duplicatedDocument = DocumentManagerUtils.stringToDom(SimflownyUtils.jsonToXML(pmi.duplicateDocument(originalId)));
            newId = ((Element) duplicatedDocument.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
                        
            //This time, documents must be different since the internal x3d must be duplicated and be provided a new id.
            assertFalse(DocumentManagerUtils.domToString(result).equals(
                    DocumentManagerUtils.domToString(duplicatedDocument).replaceFirst(newId, originalId)));
        }
        catch (Exception e) {
            fail("Error duplicating a X3D document: " + e.toString());
        }
        pmi.deleteDocument(originalId);
        pmi.deleteDocument(newId);
    }
    
    /**
     * Includes 2 tests:
     * 1 - Delete an existing document
     * 2 - Delete a not existing document
     * 3 - Try to delete a document which is on a dependence.
     * 4 - Delete a x3d segment.
     * 5 - Try to delete a x3d segment which is on a dependence.
     * 6 - Delete a code document.
     * @throws SUException 
     * @throws DMException 
     */
    @Test
    public void testDeleteDocument() throws SUException, DMException {
        DocumentManagerImpl pmi = null;
        try {
            //Test configuration
            pmi = new DocumentManagerImpl();
            pmi.addCollection("DeleteDiscretizationSchema", "");
            String doc = convertStreamToString("/document.json");
            String newDoc = pmi.addDocument(doc, "DeleteDiscretizationSchema");
            Document result = DocumentManagerUtils.stringToDom(SimflownyUtils.jsonToXML(newDoc));
            String id2 = ((Element) result.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
            //Test 1: Delete an existing document
            pmi.deleteDocument(id2);
            
            String list = pmi.getDocumentList("DeleteDiscretizationSchema");
            assertEquals("{\"success\":true}", list);
        }
        catch (Exception e) {
            fail("Error deleting document: " + e.toString());
        }
        try {
            //Test 2: Delete a not existing document
            pmi.deleteDocument("NotExist");
            fail("DDS004 should have been thrown");
        }
        catch (DMException e) {
            assertEquals(DMException.DM004, e.getErrorCode());
        }
        try {
            //Test 4 - Delete a x3d segment.
            pmi.addCollection("deletex3d", "");
            
            String metaInfo = SimflownyUtils.xmlToJSON(convertStreamToString("/metaSegment.xml"), false);
            String x3d = convertStreamToString("/x3d.x3d");
            String newDoc = pmi.importX3D(metaInfo, x3d, "deletex3d");
            Document result = DocumentManagerUtils.stringToDom(SimflownyUtils.jsonToXML(newDoc));
            String id2 = ((Element) result.getDocumentElement()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
            
            pmi.deleteDocument(id2);
            String list = pmi.getDocumentList("deletex3d");
            assertEquals("{\"success\":true}", list);
        }
        catch (Exception e) {
            fail("Error deleting x3d: " + e.toString());
        }
        try {
            //Test 6 - Delete a code document.
            
            CodesDB cdb = new CodesDBImpl();
            String codeId = cdb.addGeneratedCode("testDeleteGeneratedCode", "test1", "Version", "Platrm");
            String path = cdb.addCodeFile(codeId, "Module1", "Path", "File");
            cdb.addCodeFile(codeId, "Module2", "Path1", "File");
            cdb.addCodeFile(codeId, "Module3", "Path2", "File");
            
            String metaInfo = SimflownyUtils.xmlToJSON(convertStreamToString("/generatedCode.xml"), false);
            metaInfo = metaInfo.replace("CODEID", codeId);
            String newDoc = pmi.addDocument(metaInfo, "");
            Document result = DocumentManagerUtils.stringToDom(SimflownyUtils.jsonToXML(newDoc));
            String id2 = ((Element) result.getDocumentElement()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
            
            pmi.deleteDocument(id2);
            assertNull(cdb.getCodeFile(path));
            assertNull(cdb.getGeneratedCode(codeId));
        }
        catch (Exception e) {
        	e.printStackTrace(System.out);
            fail("Error deleting code document: " + e.toString());
        }
    }
    
    /**
     * Tests the download of a document with dependences.
     * 1 - Problem
     * 2 - x3d
     * @throws SUException 
     * @throws DMException 
     */
    @Test
    public void testDownloadDocument() throws SUException, DMException {
        DocumentManagerImpl pmi = null;
        String id = "";
        String id2 = "";
        String modelId = "";
        try {
            //Test 1 - Problem
            pmi = new DocumentManagerImpl();
            pmi.addCollection("download", "");
            String metaInfo = SimflownyUtils.xmlToJSON(convertStreamToString("/metaSegment.xml"), false);
            String x3d = convertStreamToString("/x3d.x3d");
            String newDoc = pmi.importX3D(metaInfo, x3d, "download");
            Document result = DocumentManagerUtils.stringToDom(SimflownyUtils.jsonToXML(newDoc));
            id = ((Element) result.getDocumentElement()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
            
            String doc = SimflownyUtils.xmlToJSON(convertStreamToString("/dependency_model.xml"), false);
            newDoc = pmi.addDocument(doc, "download");
            result = DocumentManagerUtils.stringToDom(SimflownyUtils.jsonToXML(newDoc));
            modelId = ((Element) result.getDocumentElement()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
    
            doc = SimflownyUtils.xmlToJSON(convertStreamToString("/dependency_Segment.xml"), false);
            doc = doc.replace("SEGMENTID", id).replace("MaxwellCE", modelId);
            
            newDoc = pmi.addDocument(doc, "download");
            result = DocumentManagerUtils.stringToDom(SimflownyUtils.jsonToXML(newDoc));
            id2 = ((Element) result.getDocumentElement()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
            pmi = new DocumentManagerImpl();
            byte[] output = pmi.downloadDocument(id2);
            FileOutputStream fos = new FileOutputStream("testDownloadDocument.zip");
            fos.write(output);
            fos.close();
            
            ZipFile zipFile = new ZipFile("testDownloadDocument.zip");

            Enumeration< ? extends ZipEntry> entries = zipFile.entries();
                        
            List<String> expectedFiles = 
                    Arrays.asList("Sample meta.x3d", "Sample meta_1_Borja Miñano.xml", "Maxwell classical equations_1_Carles Bona.xml",
                            "Antenna problem.xml");
            
            while (entries.hasMoreElements()) {
                String fileName = entries.nextElement().getName();
                assertTrue(expectedFiles.contains(fileName));
            }
            
            zipFile.close();
            
            File file = new File("testDownloadDocument.zip");
            file.delete();
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Error downloading document: " + e.toString());
        }
        pmi.deleteDocument(id2);
        pmi.deleteDocument(modelId);
        pmi.deleteDocument(id);
        try {
            //Test 2 - x3d
            pmi = new DocumentManagerImpl();
            String metaInfo = SimflownyUtils.xmlToJSON(convertStreamToString("/metaSegment.xml"), false);
            String x3d = convertStreamToString("/x3d.x3d");
            String newDoc = pmi.importX3D(metaInfo, x3d, "download");
            Document result = DocumentManagerUtils.stringToDom(SimflownyUtils.jsonToXML(newDoc));
            id = ((Element) result.getDocumentElement()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
            
            pmi = new DocumentManagerImpl();
            byte[] output = pmi.downloadDocument(id);
            FileOutputStream fos = new FileOutputStream("testDownloadX3D.zip");
            fos.write(output);
            fos.close();
            
            ZipFile zipFile = new ZipFile("testDownloadX3D.zip");

            Enumeration< ? extends ZipEntry> entries = zipFile.entries();
            
            List<String> expectedFiles = Arrays.asList("Sample meta.xml", "Sample meta.x3d");
            while (entries.hasMoreElements()) {
                String fileName = entries.nextElement().getName();
                assertTrue(expectedFiles.contains(fileName));
            }
            zipFile.close();
            
            File file = new File("testDownloadX3D.zip");
            file.delete();
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Error downloading document: " + e.toString());
        }
        pmi.deleteDocument(id);
    }
    
    /**
     * Includes 2 tests:
     * 1 - Getting an existing document.
     * 2 - Trying to get a non existing document.
     * @throws SUException 
     * @throws DMException 
     * 
     */
    @Test
    public void testGetDocument() throws SUException, DMException {
        
        DocumentManagerImpl pmi = new DocumentManagerImpl();
        String id = "";
        try {
            //Test configuration
            pmi.addCollection("GetDocument", "");
            String doc = convertStreamToString("/document.json");
            String newDoc = pmi.addDocument(doc, "GetDocument");
            Document result = DocumentManagerUtils.stringToDom(SimflownyUtils.jsonToXML(newDoc));
            id = ((Element) result.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
            
            //Test 1: Getting an existing document
            assertEquals(newDoc, pmi.getDocument(id));
        }
        catch (Exception e) {
            fail("Error getting document: " + e.toString());
        }
        try {
            //Test 2: Trying to get a not existing document
            pmi.getDocument("NonExisting");
            fail("DM004 should have been thrown");
        }
        catch (DMException e) {
            assertEquals(DMException.DM004, e.getErrorCode());
        }
        pmi.deleteDocument(id);
    }
    
    /**
     * Includes 1 test:
     * 1 - Save a document with an id.
     * 2 - Save a document without id.
     * 3 - Save a document with id but wrong path.
     * 4 - Save a document with different header information.
     * 5 - Save a document with different header information and dependencies.
     * @throws SUException 
     * @throws DMException 
     * 
     */
    @Test
    public void testSaveDocument() throws SUException, DMException {
        
        DocumentManagerImpl pmi = new DocumentManagerImpl();
        String id = "";
        String newId = "";
        String idDep = "";
        String idDep2 = "";
        try {
            //Test configuration
            pmi.addCollection("SaveDocument", "");
            String doc = SimflownyUtils.xmlToJSON(convertStreamToString("/Collective_motion_model.xml"), false);
            String newDoc = pmi.addDocument(doc, "SaveDocument");
            Document result = DocumentManagerUtils.stringToDom(SimflownyUtils.jsonToXML(newDoc));
            id = ((Element) result.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();

            newDoc = newDoc.replace("neighbours", "neighboursmodified");
            System.out.println(newDoc);
            assertFalse(newDoc.equals(pmi.getDocument(id)));
            
            //Test 1: Save a document with an id.
            String savedDoc = pmi.saveDocument(newDoc, "SaveDocument");
            
            assertEquals(newDoc, savedDoc);
        }
        catch (Exception e) {
            fail("Error saving document: " + e.toString());
        }
        try {
            //Test configuration
            String newDoc = pmi.getDocument(id);
            String oldDoc = newDoc;
            pmi.deleteDocument(id);
            //Remove the id
            newDoc = newDoc.replace(id, "");
            //Test 2 - Save a document without id.
            String savedDoc = pmi.saveDocument(newDoc, "SaveDocument");
            
            assertTrue(oldDoc.equals(savedDoc));
        }
        catch (Exception e) {
            fail("Error saving document: " + e.toString());
        }
        try {
            //Test configuration
            String newDoc = pmi.getDocument(id);

            //Test 3 - Save a document with id but wrong path.
            pmi.saveDocument(newDoc, "anotherPath");
            
            fail("Error saving document: duplicated document");
        }
        catch (DMException e) {
            assertEquals(DMException.DM004, e.getErrorCode());
        }
        try {
            //Test configuration
            String newDoc = pmi.getDocument(id);
            newDoc = newDoc.replaceAll("Collective motion 2D", "New document name");
            //Test 4 - Save a document with different header information.
            String savedDoc = pmi.saveDocument(newDoc, "SaveDocument");
            Document result = DocumentManagerUtils.stringToDom(SimflownyUtils.jsonToXML(savedDoc));
            newId = ((Element) result.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
        }
        catch (Exception e) {
            fail("Error saving document: " + e.toString());
        }
        try {
            //Test configuration
            String doc = SimflownyUtils.xmlToJSON(convertStreamToString("/dependency_model.xml"), false);
            String newDoc = pmi.addDocument(doc, "SaveDocument");
            Document result = DocumentManagerUtils.stringToDom(SimflownyUtils.jsonToXML(newDoc));
            String oldIdDep = ((Element) result.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
            //Test 5 - Save a document with different header information and dependencies.
            doc = SimflownyUtils.xmlToJSON(convertStreamToString("/dependency_problem.xml").replaceAll("MODELIDIMPORT", oldIdDep), false);
            String dependent = pmi.addDocument(doc, "SaveDocument");
            result = DocumentManagerUtils.stringToDom(SimflownyUtils.jsonToXML(dependent));
            idDep2 = ((Element) result.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
            newDoc = newDoc.replaceAll("Maxwell classical equations", "New document name");
            newDoc = pmi.saveDocument(newDoc, "SaveDocument");
            result = DocumentManagerUtils.stringToDom(SimflownyUtils.jsonToXML(newDoc));
            idDep = ((Element) result.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();

            newDoc = pmi.getDocument(idDep2);
            result = DocumentManagerUtils.stringToDom(SimflownyUtils.jsonToXML(newDoc));
            String modelId = DocumentManagerUtils.find(result, "//mms:model/mms:id").item(0).getTextContent();
            
            assertFalse(oldIdDep.equals(idDep));
            assertEquals(idDep, modelId);
        }
        catch (Exception e) {
        	e.printStackTrace();
            fail("Error saving document: " + e.toString());
        }
        pmi.deleteDocument(newId);
        pmi.deleteDocument(idDep2);
        pmi.deleteDocument(idDep);
    }
    
    /**
     * Includes 1 test:
     * 1 - Moves a document.
     * @throws SUException 
     * @throws DMException 
     * 
     */
    @Test
    public void testMoveDocument() throws SUException, DMException {
        
        DocumentManagerImpl pmi = new DocumentManagerImpl();
        String id = "";
        try {
            //Test configuration
            pmi.addCollection("Move1", "");
            pmi.addCollection("Move2", "");
            String doc = convertStreamToString("/document.json");
            String newDoc = pmi.addDocument(doc, "Move1");
            Document result = DocumentManagerUtils.stringToDom(SimflownyUtils.jsonToXML(newDoc));
            id = ((Element) result.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
            
            //Test 1: Moving document
            pmi.moveDocument(id, "Move2");
            //Must contain the new child
            assertTrue(pmi.getDocumentList("Move2").contains("\"id\":"));
        }
        catch (Exception e) {
            fail("Error moving document: " + e.toString());
        }
        try {
            pmi.getDocument(id);
            assertFalse(pmi.getDocumentList("Move1").contains("\"id\":"));
        }
        catch (DMException e) {
            fail("Error moving document: " + e.toString());
        }
        pmi.deleteDocument(id);
    }

    /**
     * Includes 3 tests:
     * 1 - Moves a collection
     * 2 - Try to move a collection on the same parent collection
     * 3 - Try to move a collection that does not exist
     * 4 - Try to move a collection to a child collection.
     * @throws DMException 
     */
    @Test
    public void testMoveCollection() throws DMException {
        DocumentManagerImpl pmi = new DocumentManagerImpl();
        String id = "";
        String id2 = "";
        try {
            
            pmi.addCollection("Level11", "");
            pmi.addCollection("Level21", "Level11");
            pmi.addCollection("Level31", "Level11/Level21");
            pmi.addCollection("Level12", "");
            //Test 1: Moves a collection
            String doc = convertStreamToString("/document.json");
            String newDoc = pmi.addDocument(doc, "Level11");
            Document result = DocumentManagerUtils.stringToDom(SimflownyUtils.jsonToXML(newDoc));
            id = ((Element) result.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
            
            doc = convertStreamToString("/document2.json");
            newDoc = pmi.addDocument(doc, "Level11/Level21/Level31");
            result = DocumentManagerUtils.stringToDom(SimflownyUtils.jsonToXML(newDoc));
            id2 = ((Element) result.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
            assertFalse(pmi.getDocumentList("Level12").contains("\"id\":"));
            
            pmi.moveCollection("Level11", "", "Level12");
            assertTrue(pmi.getDocumentList("Level12/Level11").contains("\"id\":"));
            assertTrue(pmi.getDocumentList("Level12/Level11/Level21/Level31").contains("\"id\":"));
        }
        catch (Exception e) {
        	e.printStackTrace();
            fail("Error moving collection: " + e.toString());
        }
        try {
            //Test 2:  Try to move a collection on the same parent collection
            pmi.moveCollection("Level11", "Level12", "Level12");
            fail("Error moving collection: collection moved to the same parent collection");
        }
        catch (DMException e) {
            assertEquals(DMException.DM006, e.getErrorCode());
        }
        try {
            //Test 3: Try to move a collection that does not exist
            pmi.moveCollection("frwfer", "", "Level12");
            fail("Error moving collection: moved a collection that not exists!!!!!!!!!!!!!!1");
        }
        catch (DMException e) {
            assertEquals(DMException.DM007, e.getErrorCode());
        }
        try {
            //Test 4: Try to move a collection to a child collection.
            pmi.moveCollection("Level21", "Level12/Level11", "Level12/Level11/Level21/Level31");
            fail("Error moving collection: moved a collection to its own children!!!!!!!!!!!!!!1");
        }
        catch (DMException e) {
            assertEquals(DMException.DM008, e.getErrorCode());
        }
        pmi.deleteDocument(id);
        pmi.deleteDocument(id2);
    }
    
    /**
     * Includes 3 tests:
     * 1 - Renamet a collection.
     * 2 - Try to rename a collection with the name of an existing one.
     * 3 - Try to rename a collection that does not exist.
     * @throws DMException 
     */
    @Test
    public void testRenameCollection() throws DMException {
        DocumentManagerImpl pmi = new DocumentManagerImpl();
        try {
            pmi.addCollection("NameA", "");
            //Test 1: Moves a collection
            assertTrue(pmi.getCollectionTree().contains("NameA"));
            
            pmi.renameCollection("NameA", "NameB", "");
            assertFalse(pmi.getCollectionTree().contains("NameA"));
        }
        catch (Exception e) {
            fail("Error renaming collection: " + e.toString());
        }
        try {
            //Test 2:  Try to rename a collection with the name of an existing one
            pmi.addCollection("NameA", "");
            pmi.renameCollection("NameB", "NameA", "");
            fail("Error renaming collection: collection renamed to the a name of existing collection");
        }
        catch (DMException e) {
            assertEquals(DMException.DM006, e.getErrorCode());
        }
        try {
            //Test 3: Try to rename a collection that does not exist
            pmi = new DocumentManagerImpl();
            pmi.moveCollection("frwfer", "dsff", "");
            fail("Error renaming collection: renamed a collection that not exists!!!!!!!!!!!!!!1");
        }
        catch (DMException e) {
            assertEquals(DMException.DM007, e.getErrorCode());
        }
    }
    
    /**
     * Includes 2 tests:
     * 1 - Existing Document
     * 2 - Non-Existing Document.
     */
    @Test
    public void testGetDocumentCollection() {
        DocumentManagerImpl pmi = null;
        try { 
            
            pmi = new DocumentManagerImpl();
            pmi.addCollection("testGetCollection", "test1/test12/test121");

            //Test 1: Existing Document
            String doc = convertStreamToString("/document.json");
            String newDoc = pmi.addDocument(doc, "test1/test12/test121/testGetCollection");
            Document result = DocumentManagerUtils.stringToDom(SimflownyUtils.jsonToXML(newDoc));
            String id = ((Element) result.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
            
            assertEquals("test1/test12/test121/testGetCollection", pmi.getDocumentCollection(id));
            
            pmi.deleteDocument(id);
        }
        catch (Exception e) {
            fail("Error getting the document collection: " + e.toString());
        }
        try { 
            //Test 1: Non-Existing Document
            pmi.getDocumentCollection("NonExistingId");
            fail("DM004 should have been thrown");
        }
        catch (DMException e) {
            assertEquals(DMException.DM004, e.getErrorCode());
        }
    }

    /**
     * Test to check the validation.
     * Test 1: Validate document
     * Test 2: Wrong schema
     * Test 3: Schematron violation
     * Test 4: Inter document validation
     */
    @Test
    public void testValidateDocument() {
        DocumentManagerImpl pmi = null;
        SimflownyUtils su = null;
        try { 
            //Test configuration
            pmi = new DocumentManagerImpl();
            su = new SimflownyUtilsImpl();
                        
            //Test 1: Valid document
            String doc = SimflownyUtils.xmlToJSON(convertStreamToString("/Collective_motion_model.xml"), false);
            String newDoc = pmi.addDocument(doc, "");
            Document result = DocumentManagerUtils.stringToDom(SimflownyUtils.jsonToXML(newDoc));
            String id = ((Element) result.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
            
            
            doc = SimflownyUtils.xmlToJSON(convertStreamToString("/Collective_motion_problem.xml"), false).replaceAll("MODELCOLLECTIONID", id);
            pmi.validateDocument(doc, true);
            pmi.deleteDocument(id);
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Error validating the document: " + e.toString());
        }
        try {
            //Test 2: Wrong schema
            String doc = convertStreamToString("/Collective_motion_problem_wrong_schema.json");
            pmi.validateDocument(doc, true);
            fail("Error validating the document: Wrong schema validated correctly!!!!!!");
        }
        catch (DMException e) {
            assertEquals(DMException.DM003, e.getErrorCode());
        }
        catch (Exception e) {
            fail("Error validating the document: " + e.toString());
        }
        try {
            //Test 3: Schematron violation
            String doc = SimflownyUtils.xmlToJSON(convertStreamToString("/Collective_motion_schematron_violation.xml"), false);
            pmi.validateDocument(doc, true);
            fail("Error validating the document: Schematron violation validated correctly!!!!!!");
        }
        catch (DMException e) {
            assertEquals(DMException.DM009, e.getErrorCode());
        }
        catch (Exception e) {
            fail("Error validating the document: " + e.toString());
        }
        try {
        	//Test 4: Inter document validation
        	//Adding the model
            String model = pmi.addDocument(SimflownyUtils.xmlToJSON(convertStreamToString(getClass().
                    getResourceAsStream("/modelGenericPDETest.xml")), false), "");
            Document doc = DocumentManagerUtils.stringToDom(SimflownyUtils.jsonToXML(model));
            model = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
            
            //Spatial operator schemas
            String loadDB = SimflownyUtils.xmlToJSON(convertStreamToString(getClass().
                    getResourceAsStream("/CenteredDifference4_Carlos.xml")), false);
            String added = pmi.addDocument(loadDB, "");
            doc = DocumentManagerUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            String cd4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
           
            loadDB = SimflownyUtils.xmlToJSON(convertStreamToString(getClass().
            	       getResourceAsStream("/CenteredDifference.xml")), false);
            added = pmi.addDocument(loadDB, "");
            doc = DocumentManagerUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            String cd = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
           
            loadDB = SimflownyUtils.xmlToJSON(convertStreamToString(getClass().
                    getResourceAsStream("/BackwardDifferenceLie.xml")), false);
            added = pmi.addDocument(loadDB, "");
            doc = DocumentManagerUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            String blie = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            loadDB = SimflownyUtils.xmlToJSON(convertStreamToString(getClass().
                    getResourceAsStream("/ForwardDifferenceLie.xml")), false);
            added = pmi.addDocument(loadDB, "");
            doc = DocumentManagerUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            String flie = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            loadDB = SimflownyUtils.xmlToJSON(convertStreamToString(getClass().
                    getResourceAsStream("/Diffusion4_Carlos.xml")), false);
            added = pmi.addDocument(loadDB, "");
            doc = DocumentManagerUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            String difid = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            loadDB = SimflownyUtils.xmlToJSON(convertStreamToString(getClass().
                    getResourceAsStream("/operatorGenericPDETest1.xml")), false)
                    .replaceAll("CDID", cd).replaceAll("CD4ID", cd4);
            added = pmi.addDocument(loadDB, "");
            doc = DocumentManagerUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            String operator = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            loadDB = SimflownyUtils.xmlToJSON(convertStreamToString(getClass().
                        getResourceAsStream("/operatorGenericPDETest2.xml")), false)
                        .replaceAll("LIEBID", blie);
            added = pmi.addDocument(loadDB, "");
            doc = DocumentManagerUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            String blieOp = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
        
        
            loadDB = SimflownyUtils.xmlToJSON(convertStreamToString(getClass().
                        getResourceAsStream("/operatorGenericPDETest3.xml")), false)
                        .replaceAll("LIEFID", flie);
            added = pmi.addDocument(loadDB, "");
            doc = DocumentManagerUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            String flieOp = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
    
            //Schema
            loadDB = SimflownyUtils.xmlToJSON(convertStreamToString(getClass().
                    getResourceAsStream("/RK3P1.xml")), false);
            added = pmi.addDocument(loadDB, "");
            doc = DocumentManagerUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            String rk3p1 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(convertStreamToString(getClass().
                    getResourceAsStream("/RK3P2.xml")), false);
            added = pmi.addDocument(loadDB, "");
            doc = DocumentManagerUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            String rk3p2 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(convertStreamToString(getClass().
                    getResourceAsStream("/RK3P3.xml")), false);
            added = pmi.addDocument(loadDB, "");
            doc = DocumentManagerUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            String rk3p3 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(convertStreamToString(getClass().
                    getResourceAsStream("/schemaGenericPDETest.xml")), false)
                    .replaceAll("RK3P1ID", rk3p1).replaceAll("RK3P2ID", rk3p2).replaceAll("RK3P3ID", rk3p3).replaceAll("DIFFID", difid);
            added = pmi.addDocument(loadDB, "");
            doc = DocumentManagerUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            String schema = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            //Loading the problem
            String problem = SimflownyUtils.xmlToJSON(convertStreamToString(getClass().
                    getResourceAsStream("/problemGenericPDETest.xml")), false);
            problem = problem.replaceAll("MODELID", model);
            added = pmi.addDocument(problem, "");
            doc = DocumentManagerUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            String problemID = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();

            //Loading the discretization Policy
            String policy = SimflownyUtils.xmlToJSON(convertStreamToString(getClass().
                    getResourceAsStream("/policyGenericPDETest.xml")), false);
            policy = policy.replaceAll("PROBLEMID", problemID).replaceAll("OPERATORID", operator).replaceAll("SCHEMAID", schema)
                    .replaceAll("LIEFID", flieOp).replaceAll("LIEBID", blieOp);
            pmi.validateDocument(policy, true);
            
            pmi.deleteDocument(problemID);
            pmi.deleteDocument(model);
            pmi.deleteDocument(schema);
            pmi.deleteDocument(operator);
            pmi.deleteDocument(flieOp);
            pmi.deleteDocument(blieOp);
            pmi.deleteDocument(flie);
            pmi.deleteDocument(blie);
            pmi.deleteDocument(rk3p1);
            pmi.deleteDocument(rk3p2);
            pmi.deleteDocument(rk3p3);
            pmi.deleteDocument(difid);
            pmi.deleteDocument(cd);
            pmi.deleteDocument(cd4);
        }
        catch (Exception e) {
        	e.printStackTrace(System.out);
            fail("Error validating the document: " + e.toString());
        }
    }
   
    /**
     * Test to check dependencies.
     */
    @Test
    public void testGetDependencies() {
        DocumentManagerImpl pmi = null;
        try { 
            //Test configuration
            pmi = new DocumentManagerImpl();
            
            pmi.addCollection("dependencies", "");

            String doc = convertStreamToString("/delete_dependent.json");
            String newDoc = pmi.addDocument(doc, "dependencies");
            Document result = DocumentManagerUtils.stringToDom(SimflownyUtils.jsonToXML(newDoc));
            String id = ((Element) result.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
            doc = SimflownyUtils.xmlToJSON(convertStreamToString("/dependency_problem.xml").replaceAll("MODELIDIMPORT", id), false);
            
            newDoc = pmi.addDocument(doc, "dependencies");
            
            String dependencies = pmi.getParentDependencies(id);
            
            //Get children id
            result = DocumentManagerUtils.stringToDom(SimflownyUtils.jsonToXML(newDoc));
            String depId = ((Element) result.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();

            assertTrue(dependencies.contains(depId));
            
            pmi.deleteDocument(depId);
            pmi.deleteDocument(id);
        }
        catch (Exception e) {
            fail("Error getting dependencies: " + e.toString());
        }
    }
    
    /**
     * Test to check dependencies.
     */
    @Test
    public void testImportDataFromModels() {
        DocumentManagerImpl pmi = null;
        try { 
            //Test configuration
            pmi = new DocumentManagerImpl();
            
            pmi.addCollection("importData", "");

            String doc = SimflownyUtils.xmlToJSON(convertStreamToString("/modelImportTest.xml"), false);
            String newDoc = pmi.addDocument(doc, "importData");
            Document result = DocumentManagerUtils.stringToDom(SimflownyUtils.jsonToXML(newDoc));
            String id = ((Element) result.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
            doc = SimflownyUtils.xmlToJSON(convertStreamToString("/problemImportTest.xml").replaceAll("MODELID", id), false);
            
            newDoc = pmi.addDocument(doc, "importData");
            
            //Get children id
            result = DocumentManagerUtils.stringToDom(SimflownyUtils.jsonToXML(newDoc));
            String depId = ((Element) result.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
            
            pmi.importDataFromModels(newDoc);
            
            assertTrue(SimflownyUtils.jsonToXML(pmi.getDocument(depId)).replaceAll("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}", "")
            		.equals(convertStreamToString("/problemImportTestResult.xml").replaceAll("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}", "")));
                        
            pmi.deleteDocument(depId);
            pmi.deleteDocument(id);
        }
        catch (Exception e) {
        	e.printStackTrace(System.out);
            fail("Error importing data: " + e.toString());
        }
    }
    
    /**
     * Read a file as String.
     * 
     * @param is    The input stream of the file
     * @return      The resource as string
     */
    public static String convertStreamToString(InputStream is) {
        StringBuilder sb = null;
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } 
        catch (IOException e) {
            e.printStackTrace();
        } 
        finally {
            try {
                is.close();
            } 
            catch (IOException e) { 
                e.printStackTrace();
            }
        }
        return sb.toString();
    }
}
