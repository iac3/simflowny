/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.documentManagerPlugin;

import javax.jws.WebService;

/** 
 * DiscretizationSchemas is an interface that provides the functionality 
 * To permit querying and updating of the database of discretization 
 * schemas.
 * 
 * @author      ----
 * @version     ----
 */

@WebService
public interface DocumentManager {
 
    /** 
     * It returns the XML document that matches with the provided id.
     *
     * @param id          	 the Identification of the document
     * @return            	 It returns the XML document or null if it does not exist 
     * @throws DMException  DM004 - XML document not found
     *                       DM007 - Collection not found
     *                       DM00X - External error
     */
    String getDocument(String id) throws DMException;

    /** 
     * It returns the document collection.
     *
     * @param id             the Identification of the document
     * @return               It returns the document collection
     * @throws DMException  DM004 - XML document not found
     *                       DM007 - Collection not found
     *                       DM00X - External error
     */
    String getDocumentCollection(String id) throws DMException;

    
    /** 
     * It returns a zip file with the document id and its dependencies.
     *
     * @param id             the Identification of the document
     * @return               byte array with documents or null if it does not exist 
     * @throws DMException   DM004 - XML document not found
     *                        DM007 - Collection not found
     *                        DM00X - External error
     */
    byte[] downloadDocument(String id) throws DMException;
    
    /**
     * Creates an empty document from the XSD in the path selected. 
     * It is not added to the database, a collection is added, but not an ID.
     * 
     * @param docXSD        The document schema
     * @param path          The collection path 
     * @return              The document
     * @throws DMException  DM007 - Collection not found
     *                       DM00X - External error
     */
    String createDocument(String docXSD, String path) throws DMException;
    
    /** 
     * It inserts a new document in the database. So it creates a new ID.
     *
     * @param doc         	the document to insert in the bbdd
     * @param path         the path for the document 
     * @return            	It returns the document
     * @throws DMException	DM002 - Not a valid XML Document
     *  					DM003 - XML document does not match XML Schema
     *                      DM007 - Collection not found
     *  					DM00X - External error
     */
    String addDocument(String doc, String path) throws DMException;
    
    /** 
     * It saves an existing document in the database. The id is not modified, unless there is no id and then it is created.
     *
     * @param doc         	the model modified 
     * @param path         the path to save the document 
     * @return             The document saved
     * @throws DMException	DM002 - Not a valid XML Document
     * 						DM003 - XML document does not match XML Schema
     * 						DM00X - External error
     */
    String saveDocument(String doc, String path) throws DMException;

    /**
     * Duplicates a document in the database.
     * @param docId         The document id from the document to duplicate
     * @return              The new document
     * @throws DMException  DM002 - Not a valid XML Document
     *                      DM004 - XML document not found
     *                      DM00X - External error
     */
    String duplicateDocument(String docId) throws DMException;
    
    /** 
     * It deletes a document from the database.
     *
     * @param id          	the document identification  
     * @throws DMException	DM004 - XML document not found
     *                      DM007 - Collection not found
     * 						DM00X - External error
     */
    void deleteDocument(String id) throws DMException;

    /**
     * Moves a document from a collection to another.
     * @param id                The document id
     * @param destinationPath   The destination collection
     * @throws DMException      DM004 - XML document not found
     *                           DM007 - Collection not found
     *                           DM00X - External error
     */
    void moveDocument(String id, String destinationPath) throws DMException;
    
    /**
     * Gets a list with the documents of a collection.
     * @param path              The collection
     * @return                  The list of models
     * @throws DMException      DM007 - Collection not found
     *                           DM00X - External error
     */
    String getDocumentList(String path) throws DMException;
        
    /** 
     * It returns the full collection structure. 
     *
     * @return            	The collection tree
     * @throws DMException	DM00X - External error
     */
    String getCollectionTree() throws DMException;
     
    /**
     * Adds the collection to the path specified.
     * 
     * @param colName			the collection name
     * @param path             the parent collection for the new one
     * @throws DMException     DM006  Collection already exists
     *                          DM007  Collection not found
     *                          DM00X  External error
     */
    void addCollection(String colName, String path) throws DMException;

    /**
     * It move the collection and all its schemas and collections to another branch of the tree.
     * @param colName           The collection to move
     * @param destinationPath   The new place of the collection
     * @param sourcePath        The old place of the collection
     * @throws DMException      DM006  Collection already exists
     *                           DM007  Collection not found
     *                           DM008  Collection could not be moved to an own child
     *                           DM00X  External error
     */
    void moveCollection(String colName, String sourcePath, String destinationPath) throws DMException;

    /**
     * Deletes a collection and its contents.
     * @param path             The collection
     * @throws DMException     DM007  Collection not found
     *                          DM00X  External error
     */
    void deleteCollection(String path) throws DMException;
    
    /** 
     * It renames a collection.
     *
     * @param oldName           the old collection name
     * @param newName           the new collection name
     * @param path              the parent collection
     * @throws DMException  DM002 - Not a valid XML Document
     *                       DM003 - XML document does not match XML Schema
     *                       DM00X - External error
     */
    void renameCollection(String oldName, String newName, String path) throws DMException;
    
    /** 
     * It validates the document with the corresponding XSD.
     *
     * @param doc      	             the document to validate
     * @param validateDependencies  validate internal document dependencies (models, schematron) when true
     * @throws DMException	         DM002 - Not a valid XML Document
     *  					         DM003 - XML document does not match XML Schema
     *                               DM009    XML document validation error
     *  					         DM00X - External error
     */
    void validateDocument(String doc, boolean validateDependencies) throws DMException;
    
    /** 
     * It imports data (fields, coordinates, etc.) from the used models.
     *
     * @param docId    	    the problem from whose models do the data import 
     * @throws DMException  DM011 	Operation not valid for this document type
     *                      DM00X - External error
     */
    void importDataFromModels(String docId) throws DMException;
     
    /** 
     * It imports a new X3D in the database.
     *
     * @param metaInfo      the x3d meta-information
     * @param x3d           the x3d data
     * @param path          the path for the document 
     * @return              It returns the document
     * @throws DMException  DM002 - Not a valid XML Document
     *                       DM003 - XML document does not match XML Schema
     *                       DM007 - Collection not found
     *                       DM00X - External error
     */
    String importX3D(String metaInfo, String x3d, String path) throws DMException;
    
    /** 
     * It gets the X3D data.
     *
     * @param id            the document id (meta data)
     * @return              It returns the x3d data
     * @throws DMException  DM004 - XML document not found
     *                       DM00X - External error
     */
    String getX3D(String id) throws DMException;
    
    /** 
     * Gets the documents that imports the document provided.
     *
     * @param id            the document id (meta data)
     * @return              It returns a list with the parent documents (JSON)
     * @throws DMException  DM004 - XML document not found
     *                       DM00X - External error
     */
    String getParentDependencies(String id) throws DMException;
}

