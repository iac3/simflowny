/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.documentManagerPlugin.utils;

import eu.iac3.mathMS.documentManagerPlugin.DMException;
import eu.iac3.mathMS.documentManagerPlugin.DocumentManagerImpl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Random;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.xml.namespace.NamespaceContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.exist.xmldb.DatabaseImpl;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceReference;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;
import org.xmldb.api.DatabaseManager;
import org.xmldb.api.base.Collection;
import org.xmldb.api.base.Database;
import org.xmldb.api.base.ResourceSet;
import org.xmldb.api.base.XMLDBException;
import org.xmldb.api.modules.XPathQueryService;

/**
 * Utility class.
 * @author bminano
 *
 */
public class DocumentManagerUtils {
    static XPath xpath;
    static XPathFactory factory = XPathFactory.newInstance();
    public static BundleContext context;
    /**
     * Default constructor.
     */
    public DocumentManagerUtils() {
    	context = FrameworkUtil.getBundle(getClass()).getBundleContext();
        factory = new net.sf.saxon.xpath.XPathFactoryImpl();
        xpath = factory.newXPath();
        NamespaceContext nsctx = new NamespaceContext() {
            public String getNamespaceURI(String prefix) {
                String uri;
                if (prefix.equals("sml")) {
                    uri = "urn:simml";
                }
                else {
                    if (prefix.equals("mms")) {
                        uri = "urn:mathms";  
                    }    
                    else {
                        if (prefix.equals("mt")) {
                            uri = "http://www.w3.org/1998/Math/MathML";
                        }
                        else {
                            if (prefix.equals("scm")) {
                                uri = "http://purl.oclc.org/dsdl/schematron";
                            }
                            else {
                                uri = null;
                            }
                        }
                    }
                }
                return uri;
            }
            // Dummy implementation - not used!
            public Iterator < ? > getPrefixes(String val) {
                return null;
            }
            // Dummy implemenation - not used!
            public String getPrefix(String uri) {
                return null;
            }
        };
    
        xpath.setNamespaceContext(nsctx);
    }
    
    /**
     * Create an element in the Uri namespace.
     * @param doc           The document where to create the element
     * @param uri           The namespace uri
     * @param tagName       The tag name
     * @return              The element
     */
    public static Element createElement(Document doc, String uri, String tagName) {
        Element element;
        if (uri != null) {
            element = doc.createElementNS(uri, tagName);
            if (uri.equals("http://www.w3.org/1998/Math/MathML")) {
                element.setPrefix("mt");
            }
            if (uri.equals("urn:mathms")) {
                element.setPrefix("mms");
            }
            if (uri.equals("urn:simml")) {
                element.setPrefix("sml");
            }
        }
        else {
            element = doc.createElement(tagName); 
        }
        return element;
    }
    
    /**
     * Executes an xpath query in the document.
     * @param node          The document to find in
     * @param xpathQuery    The query
     * @return              A nodelist with the results
     * @throws DMException  DM00X External error
     */
    public static NodeList find(Node node, String xpathQuery) throws DMException {
        try {
            XPathExpression expr = xpath.compile(xpathQuery);

            return (NodeList) expr.evaluate(node, XPathConstants.NODESET);
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            throw new DMException(DMException.DM00X, e.toString());
        }
    }
    
    /**
     * Evaluate an xpath query as a boolean.
     * @param node          The node for the query
     * @param xpathQuery    The query
     * @return              True or false
     */
    public static boolean evaluate(Node node, String xpathQuery)  {
        try {
            XPathExpression expr = xpath.compile(xpathQuery);

            return (Boolean) expr.evaluate(node, XPathConstants.BOOLEAN);
        }
        catch (Exception e) {
            return false;
        }
    }
    
    /**
     * Reads a file.
     * @param path          The path of the file
     * @return              The string
     * @throws DMException  DM00X - External error
     */
    public static String readFile(String path) throws DMException {
        try {
            FileInputStream file = new FileInputStream(path);
            byte[] b = new byte[file.available()];
            file.read(b);
            file.close();
            return new String(b);
        }
        catch (Exception e) {
            throw new DMException(DMException.DM00X, e.toString());
        }
    }
    
    /**
     * Adds the Id in the document.
     * @param xmlDoc            The document
     * @param id                The id.
     * @return                  The modified document
     * @throws DMException      DM002 - Not a valid XML Document
     *                           DM00X - External error
     */
    public static String addId(String xmlDoc, String id) throws DMException {
        //Check if the document has an id or collection
        Document dom = DocumentManagerUtils.stringToDom(xmlDoc);
        Element root = dom.getDocumentElement();
        DocumentManagerUtils.removeWhitespaceNodes(root);
        Element head = (Element) root.getFirstChild();
        NodeList idEls = head.getElementsByTagNameNS(DocumentManagerImpl.MMSURI, "id");
        if (idEls.getLength() > 0) {
            idEls.item(0).setTextContent(id);
        }
        else {
            throw new DMException(DMException.DM00X, "Document must have an \"id\" tag");
        }
        return domToString(dom);
    }
    
    /**
     * Creates a UUID from an XML document head. 
     * @param xmlDoc		The XML document
     * @return				The UUID
     * @throws DMException	DM002 - Not a valid XML Document
     *                      DM00X - External error
     */
    public static String calculateId(String xmlDoc) throws DMException {
        //Check if the document has an id or collection
        Document dom = DocumentManagerUtils.stringToDom(xmlDoc);
        Element root = dom.getDocumentElement();
        DocumentManagerUtils.removeWhitespaceNodes(root);
        Element head = (Element) root.getFirstChild();
        String name = head.getElementsByTagNameNS(DocumentManagerImpl.MMSURI, "name").item(0).getTextContent();
        String author = head.getElementsByTagNameNS(DocumentManagerImpl.MMSURI, "author").item(0).getTextContent();
        String version = head.getElementsByTagNameNS(DocumentManagerImpl.MMSURI, "version").item(0).getTextContent();
        String date = head.getElementsByTagNameNS(DocumentManagerImpl.MMSURI, "date").item(0).getTextContent();
        
        return UUID.nameUUIDFromBytes((name + author + version + date).getBytes()).toString();
    }
    
    /**
     * A method for the format of the output.
     * 
     * @param document          The document to be formatted
     * @return                  The document formatted
     * @throws DMException     DM00X - External error
     */
    public static String indent(Document document) throws DMException {
        try {
            // Setup pretty print options
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

            // Return pretty print xml string
            StringWriter stringWriter = new StringWriter();
            transformer.transform(new DOMSource(document), new StreamResult(stringWriter));
            return stringWriter.toString();            
        }
        catch (Throwable e) {
            throw new DMException(DMException.DM00X, e.getMessage());
        }
    }
    
    /**
     * Converts a Dom to string.
     * 
     * @param doc               The dom document
     * @return                  The String
     * @throws DMException  DM00X - External error
     */
    public static String domToString(Node doc) throws DMException {
        try {
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            //StreamResult result = new StreamResult(new StringWriter());
            StreamResult result = new StreamResult(new ByteArrayOutputStream());
            DOMSource source = new DOMSource(doc);
            transformer.transform(source, result);
            return new String(((ByteArrayOutputStream) result.getOutputStream()).toByteArray(), "UTF-8");
        }
        catch (Exception e) {
            throw new DMException(DMException.DM00X, e.toString());
        }
    }
    
    /**
     * Converts a string to Dom.
     * 
     * @param str               The string
     * @return                  The Dom document
     * @throws DMException  	DM002 - Not a valid XML Document
     *                          DM00X - External error
     */
    public static Document stringToDom(String str) throws DMException {
        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            docFactory.setNamespaceAware(true);
            docFactory.setIgnoringElementContentWhitespace(true);
            docFactory.setIgnoringComments(true);
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            InputStream is = new ByteArrayInputStream(str.getBytes("UTF-8"));
            Document result = docBuilder.parse(is);
            removeWhitespaceNodes(result.getDocumentElement());
            return result;
        }
        catch (SAXException e) {
            throw new DMException(DMException.DM002, e.toString());
        }
        catch (Exception e) {
            throw new DMException(DMException.DM00X, e.toString());
        }
    }
    
    /**
     * Remove the white spaced nodes from a Dom element.
     * 
     * @param e The element to delete the whitespaces from
     */
    public static void removeWhitespaceNodes(Element e) {
        NodeList children = e.getChildNodes();
        for (int i = children.getLength() - 1; i >= 0; i--) {
            Node child = children.item(i);
            if (child instanceof Text && ((Text) child).getData().trim().length() == 0) {
                e.removeChild(child);
            }
            else if (child instanceof Element) {
                removeWhitespaceNodes((Element) child);
            }
        }
    }
    
    /**
     * Process a string with dom transformations.
     * 
     * @param str               The input string
     * @return                  The output String
     * @throws DMException      DM00X - External error
     */
    public static String processString(String str) throws DMException {
        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            docFactory.setNamespaceAware(true);
            docFactory.setIgnoringElementContentWhitespace(true);
            docFactory.setIgnoringComments(true);
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            InputStream is = new ByteArrayInputStream(str.getBytes("UTF-8"));
            Document doc = docBuilder.parse(is);
            removeWhitespaceNodes(doc.getDocumentElement());
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            StreamResult result = new StreamResult(new ByteArrayOutputStream());
            DOMSource source = new DOMSource(doc);
            transformer.transform(source, result);
            return new String(((ByteArrayOutputStream) result.getOutputStream()).toByteArray(), "UTF-8");
        }
        catch (Exception e) {
            throw new DMException(DMException.DM00X, e.toString());
        }
    }
    

    /**
     * Gets the collection from the path.
     * @param path              The path
     * @return                  The collection object
     * @throws DMException      DM007   Collection not found
     *                           DM00X   External error
     */
    public static Collection getCollection(String path) throws DMException {
        Collection col = null;
        try {
            String uri = "/" + path;
            if (path.equals("") || path == null) {
                uri = "";
            }
            
        	@SuppressWarnings("rawtypes")
			ServiceReference serviceReference = context.getServiceReference("org.exist.xmldb.DatabaseImpl");
        	@SuppressWarnings("unchecked")
        	Database database = ((DatabaseImpl) context.getService(serviceReference)); 

            DatabaseManager.registerDatabase(database);
            col = DatabaseManager.getCollection(DocumentManagerImpl.uri
                    + DocumentManagerImpl.COLLECTION + uri, "admin", "");

            //Collection not found
            if (col == null) {
                throw new DMException(DMException.DM007, path);
            }
            return col;
        }
        catch (DMException e) {
            throw e;
        }
        catch (XMLDBException e) {
            throw new DMException(DMException.DM00X, e.toString());
        }
    }
    
    /**
     * Gets the root collection.
     * @return                  The collection object
     * @throws DMException      DM00X   External error
     */
    public static Collection getRootCollection() throws DMException {
        Collection col = null;
        try {
        	
        	@SuppressWarnings("rawtypes")
			ServiceReference serviceReference = context.getServiceReference(DatabaseImpl.class.getName());
        	@SuppressWarnings("unchecked")
        	Database database = (Database) context.getService(serviceReference); 

            DatabaseManager.registerDatabase(database);
            
            col = DatabaseManager.getCollection(DocumentManagerImpl.uri + DocumentManagerImpl.COLLECTION, "admin", "");
            return col;
        }
        catch (Exception e) {
        	e.printStackTrace();
            throw new DMException(DMException.DM00X, e.toString());
        }
    }
    
    /**
     * Gets the root collection.
     * @return                  The collection object
     * @throws DMException      DM00X   External error
     */
    public static Collection getX3DCollection() throws DMException {
        Collection col = null;
        try {
            String driver = "org.exist.xmldb.DatabaseImpl";
            Class < ? > c = Class.forName(driver);

            Database database = (Database) c.newInstance();
            DatabaseManager.registerDatabase(database);
            col = DatabaseManager.getCollection(DocumentManagerImpl.uri + DocumentManagerImpl.X3DCOL, "admin", "");
            return col;
        }
        catch (Exception e) {
            throw new DMException(DMException.DM00X, e.toString());
        }
    }
    
    /**
     * Get parent dependencies.
     * @param id                The document id
     * @param col               The collection to search in
     * @return                  Parent documents
     * @throws DMException      DM00X External error
     */
    public static ResourceSet getParentDependencies(String id, Collection col) throws DMException {
        XPathQueryService service;
        try {
            service = (XPathQueryService) col.getService("XPathQueryService", "1.0");
            service.setNamespace("sml", "urn:simml");
            service.setNamespace("mms", "urn:mathms");
            
            String filter = "//mms:head[parent::*//mms:id[text() = '" + id
                    + "' and not(parent::mms:head)]]|//mms:head[parent::*//sml:import/sml:ruleId[text() = '" + id + "']]";

            return service.query(filter);
        } 
        catch (Exception e) {
            throw new DMException(DMException.DM00X, e.toString());
        }
    }
    
    /**
     * Get parent dependencies.
     * @param id                The document id
     * @param col               The collection to search in
     * @return                  Parent documents
     * @throws DMException      DM00X External error
     */
    public static ResourceSet getDependentDocuments(String id, Collection col) throws DMException {
        XPathQueryService service;
        try {
            service = (XPathQueryService) col.getService("XPathQueryService", "1.0");
            service.setNamespace("sml", "urn:simml");
            service.setNamespace("mms", "urn:mathms");
            
            String filter = "/*[descendant::mms:id[text() = '" + id 
                    + "' and not(parent::mms:head)]]|/*[descendant::sml:import/sml:ruleId[text() = '" + id + "']]";

            return service.query(filter);
        } 
        catch (Exception e) {
            throw new DMException(DMException.DM00X, e.toString());
        }
    }
    
    /**
     * Returns the parent collection of a document by its ID.
     * @param docID         The document ID
     * @param coll          The collection to search into
     * @return              The collection if the document was found
     * @throws DMException  DM00X External error
     */
    public static Collection getCollection(String docID, String coll) throws DMException {
        Collection col = null;
        try {
        	@SuppressWarnings("rawtypes")
			ServiceReference serviceReference = context.getServiceReference("org.exist.xmldb.DatabaseImpl");
        	@SuppressWarnings("unchecked")
        	Database database = ((DatabaseImpl) context.getService(serviceReference)); 
            DatabaseManager.registerDatabase(database);

            col = DatabaseManager.getCollection(DocumentManagerImpl.uri
                    + DocumentManagerImpl.COLLECTION + coll, "admin", "");
            
            //Query the actual collection
            if (col.getResource(docID) != null) {
                return col;
            } 
            //Recursive call to query child collections
            String[] children = col.listChildCollections();
            col.close();
            for (int j = 0; j < children.length; j++) {
                Collection child = getCollection(docID, coll + "/" + children[j]);
                if (child != null) {
                    return child;
                }
            }
            return null;
        } 
        catch (XMLDBException e) {
            throw new DMException(DMException.DM00X, e.toString());
        }
        catch (Exception e) {
            throw new DMException(DMException.DM00X, e.toString());
        }
    }
    
    /**
     * Returns the collection of a document by its ID.
     * @param docID         The document ID
     * @param coll          The collection to search into
     * @return              The collection path if the document was found
     * @throws DMException  DM00X External error
     */
    public static String getCollectionPath(String docID, String coll) throws DMException {
        Collection col = null;
        try {
            String driver = "org.exist.xmldb.DatabaseImpl";
            Class < ? > c = Class.forName(driver);

            Database database = (Database) c.newInstance();
            DatabaseManager.registerDatabase(database);

            col = DatabaseManager.getCollection(DocumentManagerImpl.uri
                    + DocumentManagerImpl.COLLECTION + coll, "admin", "");
            
            //Query the actual collection
            if (col.getResource(docID) != null) {
                return coll;
            } 
            //Recursive call to query child collections
            String[] children = col.listChildCollections();
            col.close();
            for (int j = 0; j < children.length; j++) {
                String child = getCollectionPath(docID, coll + "/" + children[j]);
                if (child != null) {
                    return child;
                }
            }
            return null;
        } 
        catch (XMLDBException e) {
            throw new DMException(DMException.DM00X, e.toString());
        }
        catch (Exception e) {
            throw new DMException(DMException.DM00X, e.toString());
        }
    }
    
    
    /**
     * Creates the temporal directory structure.
     * @param inputFiles    The input files to zip hashed by name
     * @return              The temporal folder path
     * @throws DMException  DM00X External error
     */
    public static String createDirStructure(HashMap<String, byte[]> inputFiles) throws DMException {
        try {
            Random generator = new Random(new GregorianCalendar().getTimeInMillis());
            int randomIndex = generator.nextInt() + 1;
            String name = String.valueOf(System.currentTimeMillis()) + String.valueOf(randomIndex);
            String tmpDir = "tmp" + File.separator + name;
            while (new File(tmpDir).exists()) {
                randomIndex = generator.nextInt() + 1;
                name = String.valueOf(System.currentTimeMillis()) + String.valueOf(randomIndex);
                tmpDir = "tmp" + File.separator + name;
            }
            File tmp = new File(tmpDir);
            tmp.mkdirs();
            String[] files = new String[inputFiles.size()];
            Iterator<String> fileNames = inputFiles.keySet().iterator();
            int i = 0;
            while (fileNames.hasNext()) {
                String fileName = fileNames.next();
                byteArrayToFile(tmpDir + File.separator + fileName, inputFiles.get(fileName));
                files[i] = fileName;
                i++;
            }
            String os = System.getProperty("os.name");
            String exeZip = tmpDir + File.separator + "download.zip";
            if (os.toLowerCase().indexOf("windows") >= 0) {
                //Executable zip
                zip(exeZip, files);
            }
            else {
                //Executable zip
                String[] commands = new String[2 + files.length];
                commands[0] = "zip";
                commands[1] = "download.zip";
                for (int j = 0; j < files.length; j++) {
                    commands[2 + j] = files[j];
                }
                Process process = Runtime.getRuntime().exec(commands, null, new File(tmpDir));
                process.waitFor();
            }
            return exeZip;
        } 
        catch (Exception e) {
        	e.printStackTrace();
            throw new DMException(DMException.DM00X, e.getMessage());
        }
    }
    
    /**
     * Creates a zip file from the input files.
     * @param zipName           The name of the zip file
     * @param fileNames         The file names inside the zip
     * @throws DMException      DM00X External error
     */
    public static void zip(String zipName, String[] fileNames) throws DMException {
        final int kbyte = 1024;
        // Create a buffer for reading the files
        byte[] buf = new byte[kbyte];
        
        try {
            // Create the ZIP file
            ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipName));
        
            // Compress the files
            for (int i = 0; i < fileNames.length; i++) {
                FileInputStream in = new FileInputStream(new File(fileNames[i]).getAbsolutePath());

                // Add ZIP entry to output stream.
                out.putNextEntry(new ZipEntry(fileNames[i].substring(fileNames[i].lastIndexOf(File.separator) + 1)));
        
                // Transfer bytes from the file to the ZIP file
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
        
                // Complete the entry
                out.closeEntry();
                in.close();
            }
            // Complete the ZIP file
            out.close();
        } 
        catch (IOException e) {
            throw new DMException(DMException.DM00X, e.getMessage());
        }
    }
    
    /**
     * Creates a file with the string as the content.
     * @param fileName          The file name
     * @param content           The content of the file
     * @throws DMException      DM00X External error
     */
    public static void byteArrayToFile(String fileName, byte[] content) throws DMException {
        try {
            FileOutputStream fos = new FileOutputStream(fileName);
     	    fos.write(content);
     	    fos.close();
        } 
        catch (IOException e) {
            throw new DMException(DMException.DM00X, e.getMessage());
        }
    }
    
    /**
     * Gets the content of a file as a String.
     * @param path              The file path
     * @return                  The content of the file
     * @throws DMException      DM00X External error
     */
    public static byte[] fileToByteArray(String path) throws DMException {
        try {
            FileInputStream file = new FileInputStream(path);
            byte[] b = new byte[file.available()];
            file.read(b);
            file.close();
            return b;
        } 
        catch (Exception e) {
            throw new DMException(DMException.DM00X, e.toString());
        }
    }
    
    /**
     * Formats a xpath expression for readability.
     * @param xpathValue    The expression
     * @return          	A more friendly expression
     */
    public static String formatPath(String xpathValue) {
        String[] elements = xpathValue.split("\\/[\\w]+:");
        String path = "";
        for (int i = 1; i < elements.length; i++) {
            path = path + " -> " + formatString(elements[i]);
        }
        return path.substring(path.indexOf(">") + 1).trim();
    }
    
    /**
     * Formats a java string into a capitalized string.
     * @param javaString    The java string
     * @return              Capitalized string
     */
    public static String formatString(String javaString) {
        String[] words = javaString.split("(?=[A-Z][a-z])");
        String string = "";
        for (int i = 0; i < words.length; i++) {
            string = string + " " + words[i].substring(0, 1).toUpperCase() + words[i].substring(1);
        }
        return string.trim();
    }
    
    /**
     * Preprocess a schematron assertion to allow inter-document links.
     * @param ruleCtxt      The rule context for the assert
     * @param xpathValue    The assertion
     * @return              The new assertion
     * @throws DMException  DM00X External error
     */
    public static String preprocessAssertion(Node ruleCtxt, String xpathValue) throws DMException {
        //Inter-document link
        if (xpathValue.contains("//")) {
            String completeExpression = ""; 
            //Split OR expressions
            String orPatternString = "( or )|(\\|)";   
            String[] expressions = xpathValue.split(orPatternString);
            Pattern orPattern = Pattern.compile(orPatternString);
            Matcher orMatcher = orPattern.matcher(xpathValue);
            for (int i = 0; i < expressions.length; i++) {
                String minXpath = expressions[i];
                //Iterate all the references
                //Skip the first double dash, if exists
                while (minXpath.substring(1).indexOf("//") > 0) {
	                String uuidQuery = minXpath.substring(0, minXpath.substring(1).indexOf("//") + 1);
	                String assertQuery = minXpath.substring(minXpath.substring(1).indexOf("//") + 1);
	                
	                //Search UUID and filter all the results
	                NodeList uid = find(ruleCtxt, uuidQuery);
	                String uuidsExpression = "";
	                if (uid.getLength() > 0) {
	                    for (int j = 0; j < uid.getLength(); j++) {
	                        uuidsExpression = uuidsExpression + "or mms:head/mms:id = '" + uid.item(j).getTextContent() + "'";
	                    }
	                    uuidsExpression = "[" + uuidsExpression.substring(3) + "]";
	                }
	                //Find first node
	                String patternString = "\\/\\/[\\w]*:[\\w]*";                
	                Pattern pattern = Pattern.compile(patternString);
	                Matcher matcher = pattern.matcher(assertQuery);
	                StringBuffer sb = new StringBuffer();
	                if (matcher.find()) {
	                    //Add id filter to the node
	                    matcher.appendReplacement(sb, matcher.group(0) + uuidsExpression);
	                }
	                matcher.appendTail(sb);
	                minXpath = sb.toString();
                }
                if (i > 0) {
                	orMatcher.find();
                	completeExpression = completeExpression + orMatcher.group(0) + minXpath;
                }
                else {
                	completeExpression = minXpath;
                }
            }
            return completeExpression;
        }
        else {
            return xpathValue;
        }
    }
    
    /**
     * Obtains the model data specified by given parameters.
     * @param model				Model
     * @param importedModel		Imported model equivalences
     * @param modelXPath		Model data xpath 
     * @param equivalence		Equivalence for that data
     * @return					Data
     * @throws DMException		DM00X External error
     */
    public static ArrayList<String> getModelData(Document model, Node importedModel, String modelXPath, String equivalence) throws DMException {
    	ArrayList<String> result = new ArrayList<String>();
    	//Load model data
    	NodeList data = find(model, modelXPath);
    	for (int i = 0; i < data.getLength(); i++) {
    		result.add(data.item(i).getTextContent());
    	}
    	//Check mappings
    	NodeList mappings = ((Element) importedModel).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, equivalence);
    	if (mappings.getLength() > 0) {
    		NodeList equivalences = mappings.item(0).getChildNodes();
    		for (int j = 0; j < equivalences.getLength(); j++) {
    			String modelData = equivalences.item(j).getLastChild().getTextContent();
    			String problemData = equivalences.item(j).getFirstChild().getTextContent();
    			result.remove(modelData);
    			result.add(problemData);
    		}
    	}
    	return result;
    }
    
    /**
     * Obtains the model parameters.
     * @param model				Model
     * @param importedModel		Imported model equivalences
     * @return					Parameters
     * @throws DMException		DM00X External error
     */
    public static ArrayList<Element> getModelParameters(Document model, Node importedModel) throws DMException {
    	ArrayList<Element> result = new ArrayList<Element>();
    	//Check mappings
    	HashMap<String, String> replacements = new HashMap<String, String>();
    	NodeList mappings = ((Element) importedModel).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, "parameterEquivalence");
    	if (mappings.getLength() > 0) {
    		NodeList equivalences = mappings.item(0).getChildNodes();
    		for (int j = 0; j < equivalences.getLength(); j++) {
    			String modelData = equivalences.item(j).getLastChild().getTextContent();
    			String problemData = equivalences.item(j).getFirstChild().getTextContent();
    			replacements.put(modelData, problemData);
    		}
    	}
    	//Load model data
    	NodeList parameters = find(model, "/*/mms:parameters/mms:parameter");
    	for (int i = 0; i < parameters.getLength(); i++) {
    		Element parameter = (Element) parameters.item(i).cloneNode(true);
    		Node parName = parameter.getFirstChild();
    		if (replacements.containsKey(parName.getTextContent())) {
    			parName.setTextContent(replacements.get(parName.getTextContent()));
    		}
    		result.add(parameter);
    	}
    	return result;
    }
    
    /**
     * Obtains the model tensors.
     * @param model				Model
     * @param importedModel		Imported model equivalences
     * @return					Tensors
     * @throws DMException		DM00X External error
     */
    public static ArrayList<Element> getModelTensors(Document model, Node importedModel) throws DMException {
    	ArrayList<Element> result = new ArrayList<Element>();
    	//Check mappings
    	HashMap<String, String> replacements = new HashMap<String, String>();
    	NodeList mappings = ((Element) importedModel).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, "tensorEquivalence");
    	if (mappings.getLength() > 0) {
    		NodeList equivalences = mappings.item(0).getChildNodes();
    		for (int j = 0; j < equivalences.getLength(); j++) {
    			String modelData = equivalences.item(j).getLastChild().getTextContent();
    			String problemData = equivalences.item(j).getFirstChild().getTextContent();
    			replacements.put(modelData, problemData);
    		}
    	}
    	//Load model data
    	NodeList tensors = find(model, "/*/mms:tensors/mms:tensor");
    	for (int i = 0; i < tensors.getLength(); i++) {
    		Element tensor = (Element) tensors.item(i).cloneNode(true);
    		Node parName = tensor.getFirstChild();
    		if (replacements.containsKey(parName.getTextContent())) {
    			parName.setTextContent(replacements.get(parName.getTextContent()));
    		}
    		result.add(tensor);
    	}
    	return result;
    }
    
    /**
     * Import data into the problem.
     * @param problem		Problem document
     * @param data			Data to import
     * @param xPath			XPath of the data
     * @param parent		Parent node for data
     * @param tag			Data tag
     * @param reference		Insertion reference in case of non existing parent
     * @throws DMException	DM00X External error
     */
    public static void importData(Document problem, ArrayList<String> data, String xPath, String parent, String tag, Node reference) throws DMException {
    	//If any element already present, remove it from the list 
    	NodeList problemData = find(problem, xPath);
    	for (int i = 0; i < problemData.getLength(); i++) {
    		String text = problemData.item(i).getTextContent();
    		data.remove(text);
    	}
    	if (data.size() > 0) {
    		//Create parent if does not exist
    		NodeList parentElem = problem.getDocumentElement().getElementsByTagNameNS(DocumentManagerImpl.MMSURI, parent);
    		Element insertionElement = null;
    		if (parentElem.getLength() == 0) {
    			insertionElement = createElement(problem, DocumentManagerImpl.MMSURI, parent);
    			problem.getDocumentElement().insertBefore(insertionElement, reference);
    		}
    		else {
    			insertionElement = (Element) parentElem.item(0);
    		}
        	for (int i = 0; i < data.size(); i++) {
    			Element newElement = createElement(problem, DocumentManagerImpl.MMSURI, tag);
    			newElement.setTextContent(data.get(i));
        		insertionElement.appendChild(newElement);
        	}
    	}
    }
    
    /**
     * Import parameters into the problem.
     * @param problem		Problem document
     * @param data			Data to import
     * @throws DMException	DM00X External error
     */
    public static void importParameters(Document problem, ArrayList<Element> data) throws DMException {
    	//If any element already present, remove it from the list 
    	NodeList problemData = find(problem, "/*/mms:parameters/mms:parameter");
    	for (int i = 0; i < problemData.getLength(); i++) {
    		String paramName = problemData.item(i).getFirstChild().getTextContent();
    		for (int j = data.size() - 1; j >= 0; j--) {
    			String listParam = data.get(j).getFirstChild().getTextContent();
    			if (listParam.equals(paramName)) {
    				data.remove(j);    				
    			}
    		}
    	}
    	if (data.size() > 0) {
    		//Create parent if does not exist
    		NodeList parentElem = problem.getDocumentElement().getElementsByTagNameNS(DocumentManagerImpl.MMSURI, "parameters");
    		Element insertionElement = null;
    		if (parentElem.getLength() == 0) {
    			insertionElement = createElement(problem, DocumentManagerImpl.MMSURI, "parameters");
    			problem.getDocumentElement().insertBefore(insertionElement, problem.getDocumentElement().getElementsByTagNameNS(DocumentManagerImpl.MMSURI, "models").item(0));
    		}
    		else {
    			insertionElement = (Element) parentElem.item(0);
    		}
        	for (int i = 0; i < data.size(); i++) {
        		insertionElement.appendChild(problem.importNode(data.get(i).cloneNode(true), true));
        	}
    	}
    }

    /**
     * Import tensors into the problem.
     * @param problem		Problem document
     * @param data			Data to import
     * @throws DMException	DM00X External error
     */
    public static void importTensors(Document problem, ArrayList<Element> data) throws DMException {
    	//If any element already present, remove it from the list 
    	NodeList problemData = find(problem, "/*/mms:tensors/mms:tensor");
    	for (int i = 0; i < problemData.getLength(); i++) {
    		String paramName = problemData.item(i).getFirstChild().getTextContent();
    		for (int j = data.size() - 1; j >= 0; j--) {
    			String listParam = data.get(j).getFirstChild().getTextContent();
    			if (listParam.equals(paramName)) {
    				data.remove(j);    				
    			}
    		}
    	}
    	if (data.size() > 0) {
    		//Create parent if does not exist
    		NodeList parentElem = problem.getDocumentElement().getElementsByTagNameNS(DocumentManagerImpl.MMSURI, "tensors");
    		Element insertionElement = null;
    		if (parentElem.getLength() == 0) {
    			insertionElement = createElement(problem, DocumentManagerImpl.MMSURI, "tensors");
    			Node reference = problem.getDocumentElement().getElementsByTagNameNS(DocumentManagerImpl.MMSURI, "models").item(0);
    			if (reference.getPreviousSibling().getLocalName().equals("parameters")) {
    				reference = reference.getPreviousSibling();
    			}
    			problem.getDocumentElement().insertBefore(insertionElement, reference);
    		}
    		else {
    			insertionElement = (Element) parentElem.item(0);
    		}
        	for (int i = 0; i < data.size(); i++) {
        		insertionElement.appendChild(problem.importNode(data.get(i).cloneNode(true), true));
        	}
    	}
    }
}
