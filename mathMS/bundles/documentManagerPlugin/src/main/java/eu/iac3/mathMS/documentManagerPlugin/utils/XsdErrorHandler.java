/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.documentManagerPlugin.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

/**
 * Error handler for XSD validation.
 * @author bminano
 *
 */
public class XsdErrorHandler implements ErrorHandler {

    /**
     * Override of warning method.
     * @param exception         Exception
     * @throws SAXException     Exception
     */
    public void warning(SAXParseException exception) throws SAXException {
        handleMessage("Warning", exception);
    }

    /**
     * Override of error method.
     * @param exception         Exception
     * @throws SAXException     Exception
     */
    public void error(SAXParseException exception) throws SAXException {
        handleMessage("Error", exception);
    }

    /**
     * Override of fatalError method.
     * @param exception         Exception
     * @throws SAXException     Exception
     */
    public void fatalError(SAXParseException exception) throws SAXException {
        handleMessage("Fatal", exception);
    }

    /**
     * Handles the message with the proper format.
     * @param level             Error level
     * @param exception         Exception
     * @return                  null
     * @throws SAXException     Exception
     */
    private String handleMessage(String level, SAXParseException exception) throws SAXException {
        String message = exception.getMessage();
        if (message.endsWith("is expected.")) {
            String patternString = "[A-Za-z0-9.-]*: [\\w ]*'[\\w]*:([\\w]*)'.[\\w ]*'\\{((\"[\\w\\/.:]*\":[\\w]*(, )?)*)\\}'[\\w ]*.";
                                    
            Pattern pattern = Pattern.compile(patternString);
            Matcher matcher = pattern.matcher(message);
            message = "Element ";
            while (matcher.find()) {
                message = message + "'" + DocumentManagerUtils.formatString(matcher.group(1)) + "' is not valid. One of ";
                String[] expected = matcher.group(2).split(", ");
                for (int i = 0; i < expected.length; i++) {
                    message = message + "'" + DocumentManagerUtils.formatString(expected[i].substring(expected[i].lastIndexOf(":") + 1)) + "', ";
                }
                message = message.substring(0, message.lastIndexOf(",")) + " is expected.";
            }
        }
        else {
            if (message.contains("is not a valid value for")) {
                String patternString = "[A-Za-z0-9.-]*: ('[^']*') is not a valid value for ('[\\w]*').";
                Pattern pattern = Pattern.compile(patternString);
                Matcher matcher = pattern.matcher(message);
                message = "Value ";
                while (matcher.find()) {
                    message = message + matcher.group(1) + " is not valid for type " + matcher.group(2) + ".";
                }
            }
            else {
                if (message.contains("is not facet-valid with respect to enumeration")) {
                    String patternString = "[A-Za-z0-9.-]*: Value ('[^']*') is not facet-valid with respect to enumeration '\\[([^\\\\]*)\\]'. "
                            + "It must be a value from the enumeration.";
                    Pattern pattern = Pattern.compile(patternString);
                    Matcher matcher = pattern.matcher(message);
                    message = "Value ";
                    while (matcher.find()) {
                        message = message + matcher.group(1) + " must be one of the following: ";
                        String[] expected = matcher.group(2).split(", ");
                        for (int i = 0; i < expected.length; i++) {
                            message = message + "'" + expected[i] + "', ";
                        }
                        message = message.substring(0, message.lastIndexOf(",")) + ".";
                    }
                }
                else {
                    if (message.contains("is not facet-valid with respect to pattern")) {
                        String patternString = "[A-Za-z0-9.-]*: Value ('[^']*') is not facet-valid with respect to pattern ('[^']*') for type '[^']*'.";
                        
                        Pattern pattern = Pattern.compile(patternString);
                        Matcher matcher = pattern.matcher(message);
                        message = "Value ";
                        while (matcher.find()) {
                            message = message + matcher.group(1) + " must match the following format: " + matcher.group(2) + ".";
                        }
                    }
                }
            }
        }
        
        throw new SAXException(message);
    }
}