/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.documentManagerPlugin;

/** 
 * DMException is the class Exception launched by the 
 * {@link DocumentManager} interface.
 * The exceptions that this class return are:
 * DM001	XpathQuery not well formed
 * DM002	Not a valid XML Document
 * DM003	XML document does not match XML Schema
 * DM004	XML document not found
 * DM005    XML document already exists
 * DM006    Collection already exists
 * DM007    Collection not found
 * DM008    Collection could not be moved to an own child
 * DM009    XML document validation error
 * DM010    Integrity problem
 * DM011 	Operation not valid for this document type
 * DM00X    External error
 * @author      ----
 * @version     ----
 */
@SuppressWarnings("serial")
public class DMException extends Exception {

    public static final String DM001 = "XpathQuery not well formed";
    public static final String DM002 = "Not a valid XML document";
    public static final String DM003 = "XML document does not match XML Schema";
    public static final String DM004 = "XML document not found";
    public static final String DM005 = "XML document already exists";
    public static final String DM006 = "Collection already exists";
    public static final String DM007 = "Collection not found";
    public static final String DM008 = "Collections cannot be moved to their own childs";
    public static final String DM009 = "XML document validation error";
    public static final String DM010 = "Integrity problem";
    public static final String DM011 = "Operation not valid for this document type";
    public static final String DM00X = "External error";
	
    //External error message
    private String extMsg;
	  /** 
	     * Constructor for known messages.
	     *
	     * @param code  the message	     
	     */
    public DMException(String code) {
    	super(code);
    }
	  
	  /** 
	     * Constructor.
	     *
	     * @param code  the internal error message	    
	     * @param msg  the external error message
	     */
    public DMException(String code, String msg) {
		super(code);
        this.extMsg = msg;
    }
	  /** 
     * Returns the error code.
     *
     * @return the error code	     
     */
    public String getErrorCode() {
    	return super.getMessage();
    }
	  /** 
	     * Returns the error message as a string.
	     *
	     * @return the error message	     
	     */
    public String getMessage() {
    	if (extMsg == null) {
            return "DMException: " + super.getMessage();
    	}
    	else {
            return "DMException: " + super.getMessage() + " (" + extMsg + ")";
    	}
    }  
}
