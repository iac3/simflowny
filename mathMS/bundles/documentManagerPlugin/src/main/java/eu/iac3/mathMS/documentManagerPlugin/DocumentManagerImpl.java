/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.documentManagerPlugin;

import eu.iac3.mathMS.codesDBPlugin.CodesDB;
import eu.iac3.mathMS.codesDBPlugin.CodesDBImpl;
import eu.iac3.mathMS.documentManagerPlugin.utils.DocumentManagerUtils;
import eu.iac3.mathMS.documentManagerPlugin.utils.XsdErrorHandler;
import eu.iac3.mathMS.simflownyUtilsPlugin.SUException;
import eu.iac3.mathMS.simflownyUtilsPlugin.SimflownyUtils;
import eu.iac3.mathMS.simflownyUtilsPlugin.SimflownyUtilsImpl;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.exist.xmldb.DatabaseImpl;
import org.exist.xmldb.DatabaseInstanceManager;
import org.json.JSONArray;
import org.json.JSONObject;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceReference;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.log.LogService;
import org.osgi.util.tracker.ServiceTracker;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import org.xmldb.api.DatabaseManager;
import org.xmldb.api.base.Collection;
import org.xmldb.api.base.Database;
import org.xmldb.api.base.ErrorCodes;
import org.xmldb.api.base.Resource;
import org.xmldb.api.base.ResourceIterator;
import org.xmldb.api.base.ResourceSet;
import org.xmldb.api.base.XMLDBException;
import org.xmldb.api.modules.BinaryResource;
import org.xmldb.api.modules.CollectionManagementService;
import org.xmldb.api.modules.XMLResource;
import org.xmldb.api.modules.XPathQueryService;


 
/** 
 * DiscretizationSchemasImpl implements the 
 * {@link DocumentManager} interface.
 * It provides the functionality To permit querying and updating 
 * of the database of discretization schemas. 
 * @author      ----
 * @version     ----
 */
@Component //
(//
    immediate = true, //
    name = "DocumentManager", //
    property = //
    { //
      "service.exported.interfaces=*", //
      "service.exported.configs=org.apache.cxf.ws", //
      "org.apache.cxf.ws.address=/DocumentManager" //
    } //
)
public class DocumentManagerImpl implements DocumentManager {
    static final int XPATH_NOT_WELL_FORMED = 640;
    public static String uri;
    static LogService logservice = null;
    public static final String MMSURI = "urn:mathms";
    CodesDB cdb;
    public static final String COLLECTION = "docManager";
    public static final String X3DCOL = "x3d";
    DocumentManagerUtils dmu;
    public static BundleContext context;
    
    /**
     * Shutdown hook class.
     * @author bminano
     *
     */
    class ShutdownDatabase extends Thread {
    	/**
    	 * Shutdown xindice database.
    	 */
	    public void run() {
	    	try {
	        	@SuppressWarnings("rawtypes")
				ServiceReference serviceReference = context.getServiceReference(DatabaseImpl.class.getName());
	        	@SuppressWarnings("unchecked")
	        	Database database = (Database) context.getService(serviceReference); 
	    		
	            DatabaseManager.registerDatabase(database);
	            Collection col = DatabaseManager.getCollection(uri, "admin", "");
	            
	         // shut down the database
	            DatabaseInstanceManager manager = (DatabaseInstanceManager) 
	                col.getService("DatabaseInstanceManager", "1.0"); 
	            manager.shutdown();
	    	} 
	    	catch (Exception e) {
	    		e.printStackTrace();
	    	}
	    }
    }
    
    
    /**
     * Constructor that initialice the logger.
     * 
     * @throws DMException 
     */
    public DocumentManagerImpl() throws DMException {
    	super();
    	context = FrameworkUtil.getBundle(getClass()).getBundleContext();
    	// Install shutdown hook
    	ShutdownDatabase sh = new ShutdownDatabase();
    	Runtime.getRuntime().addShutdownHook(sh);
        try {
            cdb = new CodesDBImpl();
        } 
        catch (Exception e) {
        	e.printStackTrace();
            throw new DMException(DMException.DM00X, e.toString());
        }
        
        dmu = new DocumentManagerUtils();
        uri = "xmldb:exist:///db/";
        @SuppressWarnings({ "rawtypes", "unchecked" })
		ServiceTracker logServiceTracker = new ServiceTracker(context, org.osgi.service.log.LogService.class.getName(), null);
        logServiceTracker.open();
        logservice = (LogService) logServiceTracker.getService();
    }

    /** 
     * It returns the JSON document that matches with the provided id and path.
     *
     * @param id             the Identification of the document
     * @return               It returns the JSON document or null if it does not exist 
     * @throws DMException   DM004  XML document not found
     *                        DM00X - External error
     */
    public String getDocument(String id) throws DMException {
    	Collection col = DocumentManagerUtils.getCollection(id, "");
        if (col == null) {
            throw new DMException(DMException.DM004, "Document " + id + " not found.");
        }
    	try {
            XMLResource document = (XMLResource) col.getResource(id);
            if (document != null) {
                if (logservice != null) {
                    logservice.log(LogService.LOG_INFO, "Document " + id);
                    logservice.log(LogService.LOG_DEBUG, (String) document.getContent());
                }
                return SimflownyUtils.xmlToJSON(DocumentManagerUtils.processString(
                        new String(document.getContent().toString().getBytes("UTF-8"), "UTF-8")), false);
            }
            if (logservice != null) {
                logservice.log(LogService.LOG_WARNING, "Document " + id + " not found");
            }
            throw new DMException(DMException.DM004, "Document " + id + " not found");
        }
    	catch (DMException e) {
    	    throw e;
    	}
    	catch (Exception e) {
            throw new DMException(DMException.DM00X, e.toString());
    	}
    	finally {
            try {
            	if (col != null) {
                    col.close();
            	}
            }
            catch (XMLDBException e) {
            	throw new DMException(DMException.DM00X, e.toString());
            }
    	}
    }
    

    /** 
     * It returns the document collection.
     *
     * @param id             the Identification of the document
     * @return               It returns the document collection
     * @throws DMException  DM004 - XML document not found
     *                       DM007 - Collection not found
     *                       DM00X - External error
     */
    public String getDocumentCollection(String id) throws DMException {
        String col = DocumentManagerUtils.getCollectionPath(id, "");
        if (col == null) {
            throw new DMException(DMException.DM004, "Document " + id + " not found.");
        }
        if (col.equals("")) {
            return col;
        }
        //Remove the first slash when the collection is not the root
        return col.substring(1);
    }
    
    /** 
     * It returns a zip file with the document id and its dependencies.
     *
     * @param id             the Identification of the document
     * @return               byte array with documents or null if it does not exist 
     * @throws DMException   DM004 - XML document not found
     *                        DM007 - Collection not found
     *                        DM00X - External error
     */
    public byte[] downloadDocument(String id) throws DMException {
        Collection col = DocumentManagerUtils.getCollection(id, "");
        if (col == null) {
            throw new DMException(DMException.DM004, "Document " + id + " not found.");
        }
        try {
            String document = SimflownyUtils.jsonToXML(getDocument(id));
            if (document != null) {
                //Make the zips in a tmp directory
            	Set<String> docsAdded = new HashSet<String>();
                HashMap<String, byte[]> files = new HashMap<String, byte[]>();
                Document doc = DocumentManagerUtils.stringToDom(document);
                String name = DocumentManagerUtils.find(doc, "//mms:head/mms:name").item(0).getTextContent();
                files.put(name + ".xml", DocumentManagerUtils.indent(doc).getBytes(StandardCharsets.UTF_8));
                docsAdded.add(id);
                files.putAll(getDependentFiles(document, docsAdded));
                return DocumentManagerUtils.fileToByteArray(DocumentManagerUtils.createDirStructure(files));
            }
            if (logservice != null) {
                logservice.log(LogService.LOG_WARNING, "Document not found");
            }
            throw new DMException(DMException.DM004, "Document " + id + " not found");
        }
        catch (DMException e) {
        	e.printStackTrace();
            throw e;
        }
        catch (Exception e) {
        	e.printStackTrace();
            throw new DMException(DMException.DM00X, e.toString());
        }
        finally {
            try {
                if (col != null) {
                    col.close();
                }
            }
            catch (XMLDBException e) {
            	e.printStackTrace();
                throw new DMException(DMException.DM00X, e.toString());
            }
        }
    }
    
    /** 
     * It inserts a new document in the database.
     *
     * @param doc           the document to insert in the bbdd
     * @param path          the path of the document collection  
     * @return              It returns the document
     * @throws DMException  DM002 - Not a valid XML Document
     *                       DM003 - XML document does not match XML Schema
     *                       DM00X - External error
     */
    public String addDocument(String doc, String path) throws DMException {
        Collection col = null;
        String xmlDoc;
        try {
            //Transform json to XML
            xmlDoc = SimflownyUtils.jsonToXML(doc);
        }
        catch (SUException e) {
            if (e.getErrorCode() == SUException.SU001) {
                throw new DMException(DMException.DM002, e.toString());
            }
            else {
                throw new DMException(DMException.DM00X, e.toString());
            }
        }
        try {
            //Get parent collection
            col = DocumentManagerUtils.getCollection(path);
                        
            //Add id to the document
            String id = DocumentManagerUtils.calculateId(xmlDoc);
            xmlDoc = DocumentManagerUtils.addId(xmlDoc, id);
            //Check if the same id already exists
            String existingDocPath = DocumentManagerUtils.getCollectionPath(id, "");
           	if (existingDocPath != null) {
           	    throw new DMException(DMException.DM005, "A document with the same information header already exist in " + existingDocPath);
           	}
            
            //Setting the xml resource
            XMLResource model = (XMLResource) col.createResource(id, XMLResource.RESOURCE_TYPE);
            model.setContent(xmlDoc);
            col.storeResource(model);
            if (logservice != null) {
                logservice.log(LogService.LOG_INFO, "Document " + id + " inserted in " + path);
                logservice.log(LogService.LOG_DEBUG, doc);
            }
            return SimflownyUtils.xmlToJSON(xmlDoc, false);
        }
        catch (XMLDBException e) {
            if (logservice != null) {
                logservice.log(LogService.LOG_ERROR, "XML:DB Exception occured " + e.vendorErrorCode + " " + e.errorCode);
                logservice.log(LogService.LOG_ERROR, "XML:DB Exception message: " + e.toString());
            }
            throw new DMException(DMException.DM002);
        }
        catch (DMException e) {
            throw e;
        }
        catch (Exception e) {
            throw new DMException(DMException.DM00X, e.toString());
        }
        finally {
            try {
                if (col != null) {
                    col.close();
                }
            }
            catch (XMLDBException e) {
                throw new DMException(DMException.DM00X, e.toString());
            }
        }
    }
    
    /** 
     * It saves a document in the database.
     * If the document is new (does not have id or has been deleted and modified posteriorly in an editor) 
     * add it in the provided collection path.
     * Otherwise, ignore the provided path since the document could have been moved in the manager.
     *
     * @param doc           the model modified 
     * @param path          the path to store deleted or new documents
     * @return				the saved document
     * @throws DMException  DM002 - Not a valid XML Document
     *                       DM003 - XML document does not match XML Schema
     *                       DM00X - External error
     */
    public String saveDocument(String doc, String path) throws DMException {
        Collection col = null;
    	try {
            //Transform json to XML
            String xmlDoc = SimflownyUtils.jsonToXML(doc);
    	    
            //Get the id and collection to save the document
            Document dom = DocumentManagerUtils.stringToDom(xmlDoc);
            Element root = dom.getDocumentElement();
            DocumentManagerUtils.removeWhitespaceNodes(root);
            Element head = (Element) root.getFirstChild();
            String previousId = head.getElementsByTagNameNS(MMSURI, "id").item(0).getTextContent();
            String currentId = DocumentManagerUtils.calculateId(xmlDoc);
            //Empty ID, the document is a new document
            if (previousId.equals("")) {
                return addDocument(doc, path);
            }
            String realPath = getDocumentCollection(previousId);
            if (!realPath.equals(path)) {
                throw new DMException(DMException.DM004, realPath + " : " + previousId);
            }
            //Get document collection.
            col = DocumentManagerUtils.getCollection(realPath);
            //Check if header information has been modified
            if (!previousId.equals(currentId)) {
            	//Change id 
            	xmlDoc = DocumentManagerUtils.addId(xmlDoc, currentId);
            	//Change dependencies ids
            	changeDependencies(previousId, currentId, DocumentManagerUtils.getRootCollection(), "");
                //Setting the xml resource
                XMLResource model = (XMLResource) col.createResource(currentId, XMLResource.RESOURCE_TYPE);
                model.setContent(xmlDoc);
                col.storeResource(model);
            	//Delete old document
            	deleteDocument(previousId);
            }
            else {
                //Setting the xml resource
                XMLResource model = (XMLResource) col.getResource(previousId);
                if (model == null) {
                    throw new DMException(DMException.DM004, realPath + " : " + previousId);
                }
                model.setContent(xmlDoc);
                col.storeResource(model);
            }
            
            if (logservice != null) {
                logservice.log(LogService.LOG_INFO, "Document " + currentId + " saved in " + realPath);
                logservice.log(LogService.LOG_DEBUG, doc);
            }
            return SimflownyUtils.xmlToJSON(xmlDoc, false);
    	}
    	catch (DMException e) {
    	    e.printStackTrace();
    	    throw e;
    	}
    	catch (Exception e) {
    	    e.printStackTrace();
    	    throw new DMException(DMException.DM00X, e.getLocalizedMessage());
    	}
    }

    /**
     * Duplicates a document in the database.
     * @param docId         The document id from the document to duplicate
     * @return              The new document
     * @throws DMException  DM002 - Not a valid XML Document
     *                      DM004 - XML document not found
     *                      DM00X - External error
     */
    public String duplicateDocument(String docId) throws DMException {
        String path = DocumentManagerUtils.getCollectionPath(docId, "");
        String originalDocument = getDocument(docId);
        Document doc;
        try {
            doc = DocumentManagerUtils.stringToDom(SimflownyUtils.jsonToXML(originalDocument));
            //Change version number
            String version = doc.getElementsByTagNameNS(MMSURI, "version").item(0).getTextContent();
            try {
            	int verInt = Integer.parseInt(version);
            	verInt++;
            	doc.getElementsByTagNameNS(MMSURI, "version").item(0).setTextContent(String.valueOf(verInt));
            }
            catch (NumberFormatException e) {
            	doc.getElementsByTagNameNS(MMSURI, "version").item(0).setTextContent(version + " copy");
            }
            originalDocument = SimflownyUtils.xmlToJSON(DocumentManagerUtils.domToString(doc), false);
            //Check for X3D regions, which have a special behavior
            if (doc.getDocumentElement().getNodeName().equals("mms:x3dRegion")) {
                //Get the internal X3D that has to be duplicated too
                String x3d = getX3D(docId);
                //Save the x3d and its duplicated meta information file
                return importX3D(originalDocument, x3d, path);
            }
            else {
            	return addDocument(originalDocument, path);
            }
        }
        catch (SUException e) {
            if (e.getErrorCode() == SUException.SU001) {
                throw new DMException(DMException.DM002, e.toString());
            }
            else {
                throw new DMException(DMException.DM00X, e.toString());
            }
        }
    }
    
    /** 
     * It deletes a document from the database.
     *
     * @param id            the document identification 
     * @throws DMException  DM004 - XML document not found
     *                       DM00X - External error
     */
    public void deleteDocument(String id) throws DMException {
        deleteDocument(id, false);
    }
    
    /** 
     * It deletes a document from the database.
     *
     * @param id                the document identification
     * @param fromMovement      if the deletion comes from a document movement
     * @throws DMException      DM004 - XML document not found
     *                           DM00X - External error
     */
    private void deleteDocument(String id, boolean fromMovement) throws DMException {
        Collection col = null;
        try {
            //Get parent collection
            col = DocumentManagerUtils.getCollection(id, "");
            if (col == null) {
                throw new DMException(DMException.DM004, "Document " + id + " not found.");
            }
            Document document = null;
            XMLResource resource = (XMLResource) col.getResource(id);
            if (resource != null) {
                document = DocumentManagerUtils.stringToDom(new String(resource.getContent().toString().getBytes("UTF-8"), "UTF-8"));
            }
            //Only delete hidden related documents if not a movement
            if (!fromMovement) {
                //if it is a x3d region, remove the region and then the meta-information
                if (document.getDocumentElement().getLocalName().equals("x3dRegion")) {
                    String regId = document.getDocumentElement().getElementsByTagName("mms:fileId").item(0).getTextContent();
                    if (!regId.equals("")) {
                        deleteRegion(regId);
                    }
                }
                //if it is a code document, remove the code and then the meta-information
                if (document.getDocumentElement().getLocalName().equals("generatedCode")) {
                    String codeId = document.getDocumentElement().getElementsByTagName("mms:codeId").item(0).getTextContent();
                    if (!codeId.equals("")) {
                        cdb.deleteGeneratedCode(codeId);
                    }
                }
            }
            col.removeResource(col.getResource(id));
            
            if (logservice != null) {
                logservice.log(LogService.LOG_INFO, "Document " + id + " removed from " + col.getName());
            }
        }
        catch (XMLDBException e) {
            e.printStackTrace(System.out);
            if (logservice != null) {
                logservice.log(LogService.LOG_ERROR, "XML:DB Exception occured " + e.vendorErrorCode + " " + e.errorCode);
                logservice.log(LogService.LOG_ERROR, "XML:DB Exception message: " + e.getMessage());
            }
            if (e.errorCode == ErrorCodes.NO_SUCH_RESOURCE || e.errorCode == ErrorCodes.INVALID_RESOURCE
                    || e.errorCode == ErrorCodes.UNKNOWN_RESOURCE_TYPE) {
                throw new DMException(DMException.DM004);
            }
            throw new DMException(DMException.DM00X, e.toString());
        }
        catch (DMException e) {
        	e.printStackTrace(System.out);
            throw e;
        }
        catch (Exception e) {
        	e.printStackTrace(System.out);
            throw new DMException(DMException.DM00X, e.toString());
        }
        finally {
            try { 
                if (col != null) {
                    col.close();
                }
            }
            catch (Exception e) {
                throw new DMException(DMException.DM00X, e.toString());
            }
        }
    }
    
    /** 
     * It deletes a document from a collection.
     *
     * @param id                the document identification
     * @param col      			The collection
     * @throws DMException      DM004 - XML document not found
     *                           DM00X - External error
     */
    private void deleteDocument(String id, Collection col) throws DMException {
        try {
            col.removeResource(col.getResource(id));
            
            if (logservice != null) {
                logservice.log(LogService.LOG_INFO, "Document " + id + " removed from " + col.getName());
            }
        }
        catch (XMLDBException e) {
            e.printStackTrace(System.out);
            if (logservice != null) {
                logservice.log(LogService.LOG_ERROR, "XML:DB Exception occured " + e.vendorErrorCode + " " + e.errorCode);
                logservice.log(LogService.LOG_ERROR, "XML:DB Exception message: " + e.getMessage());
            }
            if (e.errorCode == ErrorCodes.NO_SUCH_RESOURCE || e.errorCode == ErrorCodes.INVALID_RESOURCE
                    || e.errorCode == ErrorCodes.UNKNOWN_RESOURCE_TYPE) {
                throw new DMException(DMException.DM004);
            }
            throw new DMException(DMException.DM00X, e.toString());
        }
        finally {
            try { 
                if (col != null) {
                    col.close();
                }
            }
            catch (Exception e) {
                throw new DMException(DMException.DM00X, e.toString());
            }
        }
    }
    
    /**
     * Deletes a x3d region.
     * @param id                The region id
     * @throws DMException      DM00X   External error
     */
    private void deleteRegion(String id) throws DMException {
        Collection col = null;
        try {
            //Get parent collection
            col = DocumentManagerUtils.getX3DCollection();
            col.removeResource(col.getResource(id));
        }
        catch (DMException e) {
            throw e;
        }
        catch (XMLDBException e) {
            throw new DMException(DMException.DM00X, e.toString());
        }
        finally {
            try { 
                if (col != null) {
                    col.close();
                }
            }
            catch (XMLDBException e) {
                throw new DMException(DMException.DM00X, e.toString());
            }
        }
    }
    
    /** 
     * It returns the full collection structure. 
     *
     * @return              The collection tree
     * @throws DMException  DM00X - External error
     */
    public String getCollectionTree() throws DMException {
    	Collection col = null;
    	try {

    	    col = DocumentManagerUtils.getRootCollection();
    	    
    	    //Creation of the root element
    	    DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
    	    Document doc = builder.newDocument();
    	    Node root = doc.createElementNS(MMSURI, "docManagerTree");
    	    root.setPrefix("mms");
    	    doc.appendChild(root);
    	    
    	    //Getting the child collections
    	    String[] child = col.listChildCollections();
    	    col.close();
    	    Node collection = null;
    	    for (int i = 0; i < child.length; i++) {
    	        if (logservice != null) {
    	            logservice.log(LogService.LOG_DEBUG, "Hijo de " + COLLECTION + ": " + child[i]);
    	        }
    	        collection = getCollectionTree(child[i], doc);
    	        root.appendChild(collection);
    	    }
    	    return SimflownyUtilsImpl.docManagerTreeToJSON(root);
    	}
        catch (Exception e) {
            throw new DMException(DMException.DM00X, e.toString());
        }
        finally {
            try { 
                if (col != null) {
                    col.close();
                }
            }
            catch (Exception e) {
                throw new DMException(DMException.DM00X, e.toString());
            }
        }
    }
    /** 
     * Return the node element for the actual collection following the SchemaStructure XSD. 
     *
     * @param coll			The base collection
     * @param doc           The DOM document base for the xml
     * @return            	It returns the root node for the collection following the
     * 						SchemaStructure XSD 
     * @throws DMException	DM00X - External error
     */
    Node getCollectionTree(String coll, Document doc) throws DMException {
    	Collection col = null;
    	try {

    	    col = DocumentManagerUtils.getCollection(coll);
    	    String nameS = coll.substring(coll.lastIndexOf("/") + 1);
    	            
    	    Node root = doc.createElementNS(MMSURI, "collection");
    	    root.setPrefix("mms");
    	    //Element name
    	    Node name = doc.createElementNS(MMSURI, "name");
    	    name.setPrefix("mms");
    	    name.setTextContent(nameS);
    	    root.appendChild(name);
    	    //Element collections
    	    Node collections = doc.createElementNS(MMSURI, "collections");
    	    collections.setPrefix("mms");
    	    String[] children = col.listChildCollections();
    	    Node collection = null;
            if (logservice != null) {
                logservice.log(LogService.LOG_DEBUG, "Hijos de " + coll + ": " + children.length);
            }
    	    for (int i = 0; i < children.length; i++) {
                if (logservice != null) {
                    logservice.log(LogService.LOG_DEBUG, "Hijo de " + coll + ": " + children[i]);
                }
                collection = getCollectionTree(coll + "/" + children[i], doc);
                collections.appendChild(collection);
    	    }
    	    if (collections.getChildNodes().getLength() > 0) {
    	        root.appendChild(collections);
    	    }
    	    
    	    return root;
    	}
        catch (Exception e) {
            throw new DMException(DMException.DM00X, e.toString());
        }
        finally {
            try { 
                if (col != null) {
                    col.close();
                }
            }
            catch (Exception e) {
                throw new DMException(DMException.DM00X, e.toString());
            }
        }

    }
    
    /**
     * Adds the collection to the path specified.
     * 
     * @param colName           the collection name
     * @param path             the parent collection for the new one
     * @throws DMException     DM006  Collection already exists
     *                          DM007  Collection not found
     *                          DM00X  External error
     */

    public void addCollection(String colName, String path) throws DMException {
    	Collection col = null;
    	try {
            String colString = path + "/" + colName;
            //If path is empty the parent collection is the root collection
            if (path.equals("") || path == null) {
                colString = colName;
            }
            
            
            String[] collSplit = colString.split("/");
            col = DocumentManagerUtils.getRootCollection();
            //Creation of the complete branch of the collection
            //It checks all parent collections, if they do not exist it creates them
            String colPath = "";
            for (int i = 0; i < collSplit.length; i++) {
                //Checking if the database already exists
                if (col.getChildCollection(collSplit[i]) == null) {
                    createCollection(col, collSplit[i], false);
                } 
                else {
                    //If the collection already exist throws an error
                    if (collSplit.length == i + 1) {
                        if (logservice != null) {
                            logservice.log(LogService.LOG_ERROR, "Collection " + collSplit[i] + " already exists in " + path);
                        }
                        throw new DMException(DMException.DM006, path + "/" + colName);
                    }
                }
                col.close();
                if (colPath.equals("")) {
                    colPath = collSplit[i];
                }
                else {
                    colPath = colPath + "/" + collSplit[i];
                }
                col = DocumentManagerUtils.getCollection(colPath);
            }
            col.close();
        }
    	catch (XMLDBException e) {
            if (logservice != null) {
                logservice.log(LogService.LOG_ERROR, "XML:DB Exception occured " + e.vendorErrorCode + " " + e.errorCode);
                logservice.log(LogService.LOG_ERROR, "XML:DB Exception message: " + e.getMessage());
            }
            throw new DMException(DMException.DM00X, e.toString());
    	}
        catch (DMException e) {
            if (logservice != null) {
                logservice.log(LogService.LOG_ERROR, "Exception:  " + e.toString());
            }
            throw e;
        }
        finally {
            try { 
                if (col != null) {
                    col.close();
                }
            }
            catch (Exception e) {
                throw new DMException(DMException.DM00X, e.toString());
            }
        }
    }
    
    /**
     * It move the collection and all its schemas and collections to another branch of the tree.
     * @param colName           The collection to move
     * @param destinationPath   The new place of the collection
     * @param sourcePath        The old place of the collection
     * @throws DMException      DM006  Collection already exists
     *                           DM007  Collection not found
     *                           DM008  Collection could not be moved to an own child
     *                           DM00X  External error
     */
    public void moveCollection(String colName, String sourcePath, String destinationPath) throws DMException {
        try {
            moveRenameCollection(colName, colName, sourcePath, destinationPath);
        }
        catch (DMException e) {
            throw e;
        }
    }
    
    /**
     * It move the collection and all its schemas and collections to another branch of the tree.
     * @param oldColName        The previous collection to move
     * @param newColName        The new name for the collection moved
     * @param destinationPath   The new place of the collection
     * @param sourcePath        The old place of the collection
     * @throws DMException      DM006  Collection already exists
     *                           DM007  Collection not found
     *                           DM008  Collection could not be moved to an own child
     *                           DM00X  External error
     */
    private void moveRenameCollection(String oldColName, String newColName, String sourcePath, String destinationPath) throws DMException {
        Collection col = null;
        Collection parent = null;
        try {
            //Check for recursive collection moving
            if (destinationPath.startsWith(sourcePath + "/" + oldColName)) {
                throw new DMException(DMException.DM008, "Child collection: " + destinationPath);
            }
            //Create the new collection
            addCollection(newColName, destinationPath);
 
            //Get old collection
            col = DocumentManagerUtils.getCollection(sourcePath + "/" + oldColName);
            //Move the resources
            String[] resources = col.listResources();
            for (int i = 0; i < resources.length; i++) {
                moveDocument(resources[i], destinationPath + "/" + newColName);
            }
            //Move the sub-collection recursively
            String[] collections = col.listChildCollections();
            for (int i = 0; i < collections.length; i++) {
                moveCollection(collections[i], sourcePath + "/" + oldColName, destinationPath + "/" + newColName);
            }
            
            //Delete the old collection
            parent = col.getParentCollection();
            CollectionManagementService service = (CollectionManagementService) parent.getService("CollectionManagementService", "1.0"); 
            service.removeCollection(oldColName); 
        }
        catch (DMException e) {
            e.printStackTrace(System.out);
            throw e;
        } 
        catch (XMLDBException e) {
            if (logservice != null) {
                logservice.log(LogService.LOG_ERROR, "XML:DB Exception occured " + e.vendorErrorCode + " " + e.errorCode);
                logservice.log(LogService.LOG_ERROR, "XML:DB Exception message: " + e.getMessage());
            }
            throw new DMException(DMException.DM00X, e.toString());
        }
        catch (Exception e) {
            throw new DMException(DMException.DM00X, e.toString());
        }
        finally {
            try { 
                if (col != null) {
                    col.close();
                }
                if (parent != null) {
                    parent.close();
                }
            }
            catch (Exception e) {
                throw new DMException(DMException.DM00X, e.toString());
            }
        }
    }

    /**
     * Deletes a collection.
     * @param path             The collection
     * @throws DMException     DM007  Collection not found
     *                          DM00X  External error
     */
    public void deleteCollection(String path) throws DMException {
        Collection oldCol = null;
        try {
            oldCol = DocumentManagerUtils.getCollection(path);
            
            //Delete the resources
            String[] resources = oldCol.listResources();
            for (int i = 0; i < resources.length; i++) {
                //Deletes the schema to delete the metadata
                deleteDocument(resources[i]);
            }
            
            //Delete the sub-collection recursively
            String[] collections = oldCol.listChildCollections();
            for (int i = 0; i < collections.length; i++) {
                deleteCollection(path + "/" + collections[i]);
            }
            
            //Delete the old collection
            Collection parent = oldCol.getParentCollection();
            CollectionManagementService service = (CollectionManagementService) parent.getService("CollectionManagementService", "1.0"); 
            service.removeCollection(path.substring(path.lastIndexOf("/") + 1));
        } 

        catch (DMException e) {
            throw e;
        }
        catch (Exception e) {
            throw new DMException(DMException.DM00X, e.toString());
        } 
        finally {
            try { 
                if (oldCol != null) {
                    oldCol.close();
                }
            }
            catch (Exception e) {
                throw new DMException(DMException.DM00X, e.toString());
            }
        }
    }
    
    /** 
     * It validates the document with the corresponding XSD.
     *
     * @param doc                   the document to validate
     * @param validateDependencies  validate internal document dependencies (models, schematron) when true
     * @throws DMException          DM002 - Not a valid XML Document
     *                               DM003 - XML document does not match XML Schema
     *                               DM009   XML document validation error
     *                               DM00X - External error
     */
    public void validateDocument(String doc, boolean validateDependencies) throws DMException {
        Document xmlDoc;
        String document;
        String documentName;
        try {
            document = SimflownyUtils.jsonToXML(doc);
            xmlDoc = DocumentManagerUtils.stringToDom(document);
            documentName = xmlDoc.getDocumentElement().getLocalName();
            //Validation against XSD
            SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            String filePath = "common" + File.separator + "XSD" + File.separator + documentName + ".xsd";
            File schemaLocation = new File(filePath);
            factory.setFeature("http://apache.org/xml/features/validation/schema-full-checking", false);
            Schema schema = factory.newSchema(schemaLocation);
            Validator validator = schema.newValidator();
            validator.setErrorHandler(new XsdErrorHandler());
            InputStream is = new ByteArrayInputStream(document.getBytes("UTF-8"));
            Source source = new StreamSource(is);
            validator.validate(source);
            //Validate content
            validateVariables(xmlDoc);
        }
        catch (SAXException e) {
            e.printStackTrace(System.out);
            if (logservice != null) {
                logservice.log(LogService.LOG_ERROR, "SAXException " + e.getLocalizedMessage());
            }
            throw new DMException(DMException.DM003, e.getLocalizedMessage());
        }
        catch (IOException e) {
            e.printStackTrace(System.out);
            throw new DMException(DMException.DM00X, e.toString());
        }
        catch (SUException e) {
            e.printStackTrace(System.out);
            throw new DMException(DMException.DM00X, e.toString());
        }
        if (validateDependencies) {
            try {
            	//Load external documents 
            	validateExternalDocuments(xmlDoc);
                //Schematron validation
                String schematronPath = "common" + File.separator + "schematron" + File.separator + documentName + ".sch";
                Document schematron = DocumentManagerUtils.stringToDom(DocumentManagerUtils.readFile(schematronPath));
                ArrayList<String> errorsXpath = new ArrayList<String>();
                StringWriter w = new StringWriter();
                NodeList rules = DocumentManagerUtils.find(schematron, "//scm:rule");
                for (int i = 0; i < rules.getLength(); i++) {
                    String rule = rules.item(i).getAttributes().getNamedItem("context").getTextContent();
                    NodeList asserts = DocumentManagerUtils.find(rules.item(i), "./scm:assert");
                    for (int j = 0; j < asserts.getLength(); j++) {
                        String assertion = asserts.item(j).getAttributes().getNamedItem("test").getTextContent();
                        NodeList contexts = DocumentManagerUtils.find(xmlDoc, rule);
                        for (int k = 0; k < contexts.getLength(); k++) {
                            if (!DocumentManagerUtils.evaluate(contexts.item(k), 
                                    DocumentManagerUtils.preprocessAssertion(contexts.item(k), assertion)) && !errorsXpath.contains(rule + k)) {
                                w.append("Invalid content in element '" + DocumentManagerUtils.formatPath(rule) + "' (item " + (k + 1) + "): " 
                                        + asserts.item(j).getTextContent().trim() + "<br/>");
                                //Only one error per node.
                                errorsXpath.add(rule + k);
                            }
                        }
                    }
                }
                if (!w.toString().equals("")) {
                    throw new DMException(DMException.DM009, w.toString());
                }
            }
            catch (DMException e) {
                e.printStackTrace(System.out);
                throw e;
            }
            catch (Exception e) {
                e.printStackTrace(System.out);
                throw new DMException(DMException.DM00X, e.toString());
            }
        }
        StringWriter w = new StringWriter();
        
        //Print all errors and throw the exception.
        if (!w.toString().equals("")) {
            throw new DMException(DMException.DM002, w.toString());
        }
    }
    
    /**
     * Tries to create the basic structure if the discSchema collection does not exist.
     * If the collection already exist, the user could have modified the collection structure.
     * @throws DMException     DM00X - External error
     */
    public void createBasicStructure() throws DMException {
        Collection col = null;
        try {        	
        	@SuppressWarnings("rawtypes")
			ServiceReference serviceReference = context.getServiceReference(DatabaseImpl.class.getName());
        	@SuppressWarnings("unchecked")
        	Database database = (Database) context.getService(serviceReference); 
        	
            database.setProperty("create-database", "true");
            DatabaseManager.registerDatabase(database);
            col = DatabaseManager.getCollection(uri, "admin", "");
            //Checking if the collection already exists
            if (col.getChildCollection(COLLECTION) == null) {
                createCollection(col, COLLECTION, false);
            }
            if (col.getChildCollection(X3DCOL) == null) {
                createCollection(col, X3DCOL, true);
            }            
        }
        catch (Exception e) {
        	e.printStackTrace(System.out);
            throw new DMException(DMException.DM00X, e.toString());
        }
    }
    
    /**
     * Creates a collection on the parent collection.
     * @param parent            The parent collection
     * @param inline            When the collection must have inline metadata
     * @param name              The name for the new collection
     * @throws DMException      DM00X - External error
     */
    private void createCollection(Collection parent, String name, boolean inline) throws DMException {
        //Creation of the metadata collection to store the collection and schemas relation.
    	CollectionManagementService service;
        try {
            service = (CollectionManagementService) parent.getService("CollectionManagementService", "1.0");
            service.createCollection(name);
            if (logservice != null) {
                logservice.log(LogService.LOG_INFO, "Collection " + name + " created.");
            }
        }
        catch (Exception e) {
            throw new DMException(DMException.DM00X, e.toString());
        }
    }

    /**
     * Creates an empty document from the XSD in the path selected. 
     * It is not added to the database, a collection is added, but not an ID.
     * 
     * @param docXSD        The document schema
     * @param path          The collection path 
     * @return              The document
     * @throws DMException  DM007 - Collection not found
     *                       DM00X - External error
     */
    public String createDocument(String docXSD, String path) throws DMException {
        try {
            String newDoc = SimflownyUtils.xmlToJSON(docXSD, true);
            String xmlDoc = SimflownyUtils.jsonToXML(newDoc);
            //Add an empty id to the new document
            DocumentManagerUtils.addId(xmlDoc, "");
            
            return SimflownyUtils.xmlToJSON(xmlDoc, false);
        } 
        catch (SUException e) {
            throw new DMException(DMException.DM00X, e.toString());
        }
    }

    /**
     * Moves a document from a collection to another.
     * It makes a duplicate of the document in the destination path with a different id before deleting the original one
     * for safety reasons (it could happen the old document was deleted and the new one not created).
     * Once the copy is ready, delete the old document, and change id on the new one.
     * @param id                The document id
     * @param destinationPath   The destination collection
     * @throws DMException      DM004 - XML document not found
     *                           DM007 - Collection not found
     *                           DM00X - External error
     */
    public void moveDocument(String id, String destinationPath) throws DMException {
        Collection col = DocumentManagerUtils.getCollection(id, "");
        //Get old data
        String originalDocument = getDocument(id);
        String x3d  = "";
        Document doc;
		try {
			doc = DocumentManagerUtils.stringToDom(SimflownyUtils.jsonToXML(originalDocument));
		} 
		catch (SUException e) {
			throw new DMException(DMException.DM00X, e.toString());
		}
        if (doc.getDocumentElement().getNodeName().equals("mms:x3dRegion")) {
            x3d = getX3D(id);
        }
        //Delete old document
        deleteDocument(id, col);
        
        //Create new documents
        if (doc.getDocumentElement().getNodeName().equals("mms:x3dRegion")) {
            //Save the x3d and its duplicated meta information file
            importX3D(originalDocument, x3d, destinationPath);
        }
        else {
        	addDocument(originalDocument, destinationPath);
        }
    }

    /**
     * Gets a list with the documents of a collection.
     * @param path              The collection
     * @return                  The list of models
     * @throws DMException      DM007 - Collection not found
     *                           DM00X - External error
     */
    public String getDocumentList(String path) throws DMException {
        Collection col = null;
        try {
            //Creation of list document
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document doc = builder.newDocument();
            Node root = doc.createElementNS(MMSURI, "docManagerList");
            doc.appendChild(root);
            
            //Get query service
            col = DocumentManagerUtils.getCollection(path);
            XPathQueryService service = (XPathQueryService) col.getService("XPathQueryService", "1.0");
            service.setNamespace("sml", "urn:simml");
            service.setNamespace("mms", MMSURI);
  
            ResourceSet resultSet = service.query("xmldb:get-child-resources('" + col.getName() +  "')");
            
            //Query the actual collection
            if (resultSet.getSize() > 0) {
                ResourceIterator results = resultSet.getIterator();

                while (results.hasMoreResources()) {
                    String docId = results.nextResource().getContent().toString();
                    XMLResource resource = (XMLResource) col.getResource(docId);
                    Document document = DocumentManagerUtils.stringToDom(DocumentManagerUtils.processString(
                            new String(resource.getContent().toString().getBytes("UTF-8"), "UTF-8")));
                    Element head = (Element) document.getDocumentElement().getElementsByTagNameNS(MMSURI, "head").item(0);
                    Node documentElement = doc.createElementNS(MMSURI, "document");
                    Node documentType = doc.createElementNS(MMSURI, "documentType");
                    documentType.setTextContent(document.getDocumentElement().getLocalName());
                    documentElement.appendChild(documentType);
                    Node name = doc.createElementNS(MMSURI, "name");
                    name.setTextContent(head.getElementsByTagNameNS(MMSURI, "name").item(0).getTextContent());
                    documentElement.appendChild(name);
                    Node author = doc.createElementNS(MMSURI, "author");
                    author.setTextContent(head.getElementsByTagNameNS(MMSURI, "author").item(0).getTextContent());
                    documentElement.appendChild(author);
                    Node id = doc.createElementNS(MMSURI, "id");
                    id.setTextContent(head.getElementsByTagNameNS(MMSURI, "id").item(0).getTextContent());
                    documentElement.appendChild(id);
                    Node version = doc.createElementNS(MMSURI, "version");
                    version.setTextContent(head.getElementsByTagNameNS(MMSURI, "version").item(0).getTextContent());
                    documentElement.appendChild(version);
                    Node date = doc.createElementNS(MMSURI, "date");
                    date.setTextContent(head.getElementsByTagNameNS(MMSURI, "date").item(0).getTextContent());
                    documentElement.appendChild(date);
                    Node description = doc.createElementNS(MMSURI, "description");
                    if (head.getElementsByTagNameNS(MMSURI, "description").getLength() > 0) {
                        description.setTextContent(head.getElementsByTagNameNS(MMSURI, "description").item(0).getTextContent());
                    }
                    else {
                        description.setTextContent("");
                    }
                    documentElement.appendChild(description);
                    root.appendChild(documentElement);
                }

            } 
            return SimflownyUtilsImpl.docManagerListToJSON(root);
        } 
        catch (XMLDBException e) {
        	e.printStackTrace();
            if (logservice != null) {
                logservice.log(LogService.LOG_ERROR, "XML:DB Exception occured " + e.vendorErrorCode + " " + e.errorCode);
            }
            if (e.getMessage().contains("XPST0003")) {
                throw new DMException(DMException.DM001);
            }
            throw new DMException(DMException.DM00X, e.toString());
        }
        catch (ParserConfigurationException e) {
        	e.printStackTrace();
            throw new DMException(DMException.DM00X, e.toString());
        }
        catch (UnsupportedEncodingException e) {
        	e.printStackTrace();
            throw new DMException(DMException.DM00X, e.toString());
        }
        finally {
            try {
                if (col != null) {
                    col.close();
                }
            }
            catch (XMLDBException e) {
                throw new DMException(DMException.DM00X, e.toString());
            }
        }
    }

    /** 
     * It renames a collection.
     *
     * @param oldName           the old collection name
     * @param newName           the new collection name
     * @param path              the parent collection
     * @throws DMException  DM002 - Not a valid XML Document
     *                       DM003 - XML document does not match XML Schema
     *                       DM00X - External error
     */
    public void renameCollection(String oldName, String newName, String path) throws DMException {
        try {
            moveRenameCollection(oldName, newName, path, path);
        }
        catch (DMException e) {
            throw e;
        }
    }
    
    /** 
     * It imports data (fields, coordinates, etc.) from the used models.
     *
     * @param docId         the problem from whose models do the data import 
     * @throws DMException  DM011 	Operation not valid for this document type
     *                      DM00X - External error
     */
    public void importDataFromModels(String problem) throws DMException {
        Document xmlDoc;
        String document;
        String documentName;
        try {
            document = SimflownyUtils.jsonToXML(problem);
            xmlDoc = DocumentManagerUtils.stringToDom(document);
            NodeList idEls = xmlDoc.getElementsByTagNameNS(DocumentManagerImpl.MMSURI, "id");
            String docId = "";
            if (idEls.getLength() > 0) {
            	docId = idEls.item(0).getTextContent();
            }
            documentName = xmlDoc.getDocumentElement().getLocalName();
            if (!documentName.equals("PDEProblem")) {
            	throw new DMException(DMException.DM011);
            }
            NodeList models = DocumentManagerUtils.find(xmlDoc, "/*/mms:models/mms:model");
            for (int i = 0; i < models.getLength(); i++) {
            	String id = ((Element) models.item(i)).getElementsByTagNameNS(MMSURI, "id").item(0).getTextContent();
            	Document model = DocumentManagerUtils.stringToDom(SimflownyUtils.jsonToXML(getDocument(id)));
            	//Get data and insert it
            	ArrayList<String> fields = DocumentManagerUtils.getModelData(model, models.item(i), "/*/mms:fields/mms:field", "fieldEquivalence");
    			Node reference = xmlDoc.getDocumentElement().getElementsByTagNameNS(DocumentManagerImpl.MMSURI, "models").item(0);
    			while (!reference.getPreviousSibling().getLocalName().equals("fields")) {
    				reference = reference.getPreviousSibling();
    			}
            	DocumentManagerUtils.importData(xmlDoc, fields, "/*/mms:fields/mms:field", "fields", "field", reference);
            	ArrayList<String> auxiliaryFields = DocumentManagerUtils.getModelData(model, models.item(i), "/*/mms:auxiliaryFields/mms:auxiliaryField", "auxiliaryFieldEquivalence");
            	reference = xmlDoc.getDocumentElement().getElementsByTagNameNS(DocumentManagerImpl.MMSURI, "models").item(0);
    			while (!reference.getPreviousSibling().getLocalName().equals("fields") && !reference.getPreviousSibling().getLocalName().equals("auxiliaryFields")) {
    				reference = reference.getPreviousSibling();
    			}
            	DocumentManagerUtils.importData(xmlDoc, auxiliaryFields, "/*/mms:auxiliaryFields/mms:auxiliaryField", "auxiliaryFields", "auxiliaryField", reference);
            	ArrayList<String> auxiliaryVariables = DocumentManagerUtils.getModelData(model, models.item(i), "/*/mms:auxiliaryVariables/mms:auxiliaryVariable", "auxiliaryVariableEquivalence");
            	reference = xmlDoc.getDocumentElement().getElementsByTagNameNS(DocumentManagerImpl.MMSURI, "models").item(0);
    			while (!reference.getPreviousSibling().getLocalName().equals("fields") && !reference.getPreviousSibling().getLocalName().equals("auxiliaryFields") && !reference.getPreviousSibling().getLocalName().equals("auxiliaryVariables")) {
    				reference = reference.getPreviousSibling();
    			}
            	DocumentManagerUtils.importData(xmlDoc, auxiliaryVariables, "/*/mms:auxiliaryVariables/mms:auxiliaryVariable", "auxiliaryVariables", "auxiliaryVariable", reference);
            	ArrayList<String> eigenVectors = DocumentManagerUtils.getModelData(model, models.item(i), "/*/mms:eigenVectors/mms:eigenVector", "eigenVectorEquivalence");
            	reference = xmlDoc.getDocumentElement().getElementsByTagNameNS(DocumentManagerImpl.MMSURI, "models").item(0);
    			while (!reference.getPreviousSibling().getLocalName().equals("fields") && !reference.getPreviousSibling().getLocalName().equals("auxiliaryFields") && !reference.getPreviousSibling().getLocalName().equals("auxiliaryVariables") && !reference.getPreviousSibling().getLocalName().equals("eigenVectors")) {
    				reference = reference.getPreviousSibling();
    			}
            	DocumentManagerUtils.importData(xmlDoc, eigenVectors, "/*/mms:eigenVectors/mms:eigenVector", "eigenVectors", "eigenVector", reference);
            	ArrayList<Element> parameters = DocumentManagerUtils.getModelParameters(model, models.item(i));
            	DocumentManagerUtils.importParameters(xmlDoc, parameters);
            	ArrayList<Element> tensors = DocumentManagerUtils.getModelTensors(model, models.item(i));
            	DocumentManagerUtils.importTensors(xmlDoc, tensors);
            }

            //Save the modified document
            saveDocument(SimflownyUtils.xmlToJSON(DocumentManagerUtils.domToString(xmlDoc), false), getDocumentCollection(docId));
        }
        catch (SUException e) {
            throw new DMException(DMException.DM00X, e.toString());
        }
    }
    
    /** 
     * It imports a new X3D in the database.
     *
     * @param metaInfo      the x3d meta-information
     * @param x3d           the x3d data
     * @param path          the path for the document 
     * @return              It returns the document
     * @throws DMException  DM002 - Not a valid XML Document
     *                       DM003 - XML document does not match XML Schema
     *                       DM007 - Collection not found
     *                       DM00X - External error
     */
    public String importX3D(String metaInfo, String x3d, String path) throws DMException {
        Collection x3dcol = null;
        //Create a new Id general for all the collections
        String x3dID = UUID.randomUUID().toString();
        try {
            //Validate metadata
            validateDocument(metaInfo, false);
            
            //Get parent collection
            x3dcol = DocumentManagerUtils.getX3DCollection();
            
            //Adding the X3D segment to the database as binary
            BinaryResource model = (BinaryResource) x3dcol.createResource(x3dID, "BinaryResource");
            model.setContent(x3d.getBytes());
            x3dcol.storeResource(model);
            
            //Modify the meta-info to store the x3d ID.
            String xmlDoc = SimflownyUtils.jsonToXML(metaInfo);
            //Add x3d ID
            Document dom = DocumentManagerUtils.stringToDom(xmlDoc);
            DocumentManagerUtils.find(dom, "//mms:fileId").item(0).setTextContent(x3dID);
            String jsonDoc = SimflownyUtils.xmlToJSON(DocumentManagerUtils.domToString(dom), false);
            return addDocument(jsonDoc, path);
        }
        catch (XMLDBException e) {
            if (logservice != null) {
                logservice.log(LogService.LOG_ERROR, "XML:DB Exception occured " + e.vendorErrorCode + " " + e.errorCode);
                logservice.log(LogService.LOG_ERROR, "XML:DB Exception message: " + e.toString());
            }
            throw new DMException(DMException.DM002);
        }
        catch (DMException e) {
            //If an error occurred adding the meta-information, remove the x3d
            try {
                x3dcol.removeResource(x3dcol.getResource(x3dID));
            } 
            catch (XMLDBException e1) {
                throw new DMException(DMException.DM00X, e.toString());
            }
            throw e;
        }
        catch (Exception e) {
            throw new DMException(DMException.DM00X, e.toString());
        }
        finally {
            try {
                if (x3dcol != null) {
                    x3dcol.close();
                }
            }
            catch (XMLDBException e) {
                throw new DMException(DMException.DM00X, e.toString());
            }
        }
    }
    
    /** 
     * It gets the X3D data.
     *
     * @param id            the document id (meta data)
     * @return              It returns the x3d data
     * @throws DMException  DM004 - XML document not found
     *                       DM007 - Collection not found
     *                       DM00X - External error
     */
    public String getX3D(String id) throws DMException {
        //Get meta info document
        Collection col = DocumentManagerUtils.getCollection(id, "");
        if (col == null) {
            throw new DMException(DMException.DM004, "Document " + id + " not found.");
        }
        try {
            XMLResource resource = (XMLResource) col.getResource(id);
            if (resource != null) {
                if (logservice != null) {
                    logservice.log(LogService.LOG_INFO, "Document " + id);
                    logservice.log(LogService.LOG_DEBUG, (String) resource.getContent());
                }
                Document document = DocumentManagerUtils.stringToDom(new String(resource.getContent().toString().getBytes("UTF-8"), "UTF-8"));
                String x3dID = document.getDocumentElement().getElementsByTagName("mms:fileId").item(0).getTextContent();
                //Get x3d document
                Collection x3dCol = DocumentManagerUtils.getX3DCollection();
                BinaryResource resBin = (BinaryResource) x3dCol.getResource(x3dID);
                if (resBin != null) {
                    return new String((byte[]) resBin.getContent(), "UTF-8");
                }
                throw new DMException(DMException.DM004, "X3D Document " + x3dID 
                        + " not found. Inconsistency found (Existing meta-info, non-existing x3d).");
            }
            if (logservice != null) {
                logservice.log(LogService.LOG_WARNING, "Document " + id + " not found");
            }
            throw new DMException(DMException.DM004, "Document " + id + " not found");
        }
        catch (DMException e) {
            throw e;
        }
        catch (Exception e) {
            throw new DMException(DMException.DM00X, e.toString());
        }
        finally {
            try {
                if (col != null) {
                    col.close();
                }
            }
            catch (XMLDBException e) {
                throw new DMException(DMException.DM00X, e.toString());
            }
        }
    }
    
    /**
     * Gets the dependent files from a document.
     * @param document      Document
     * @return              The structure with the information (name, content)
     * @throws DMException  DM00X External error
     */
    private HashMap<String, byte[]> getDependentFiles(String document, Set<String> docsAdded) throws DMException {
        try {
            HashMap<String,  byte[]> files = new HashMap<String,  byte[]>();
            Document doc = DocumentManagerUtils.stringToDom(document);
            //Get documents
            if (!doc.getDocumentElement().getNodeName().equals("mms:x3dRegion")) {
	            NodeList documents = DocumentManagerUtils.find(doc, "//*[not(*) and not(parent::mms:head) and matches(text(), "
	                        + "'^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$')]");
	            for (int i = 0; i < documents.getLength(); i++) {
	            	String id = documents.item(i).getTextContent();
	            	if (!docsAdded.contains(id)) {
		                String model = SimflownyUtils.jsonToXML(getDocument(id));
		                Document modelDoc = DocumentManagerUtils.stringToDom(model);
		                String name = DocumentManagerUtils.find(modelDoc, "//mms:head/mms:name").item(0).getTextContent();
		                String version = DocumentManagerUtils.find(modelDoc, "//mms:head/mms:version").item(0).getTextContent();
		                String author = DocumentManagerUtils.find(modelDoc, "//mms:head/mms:author").item(0).getTextContent();
		                files.put(name + "_" + version + "_" + author + ".xml", DocumentManagerUtils.indent(modelDoc).getBytes(StandardCharsets.UTF_8));
		                docsAdded.add(id);
		                files.putAll(getDependentFiles(model, docsAdded));
	            	}
	            }
            }
            //Get code files
            NodeList codeId = DocumentManagerUtils.find(doc, "//mms:codeId");
            for (int i = 0; i < codeId.getLength(); i++) {
                String id = codeId.item(i).getTextContent();
                String[] codePaths = cdb.getGeneratedCode(id);
                for (int j = 0; j < codePaths.length; j++) {
                    files.put(codePaths[j].substring(codePaths[j].lastIndexOf(File.separator) + 1), cdb.getCodeFile(codePaths[j]));
                }
                docsAdded.add(id);
            }
            //X3D file
            if (doc.getDocumentElement().getNodeName().equals("mms:x3dRegion")) {
                String id = DocumentManagerUtils.find(doc, "//mms:head/mms:id").item(0).getTextContent();
                String x3d = getX3D(id);
                Document x3dDoc = DocumentManagerUtils.stringToDom(x3d);
                String name = DocumentManagerUtils.find(doc, "//mms:head/mms:name").item(0).getTextContent();
                files.put(name + ".x3d", DocumentManagerUtils.indent(x3dDoc).getBytes(StandardCharsets.UTF_8));
                docsAdded.add(id);
            }
            
            return files;
        } 
        catch (DMException e) {
            throw e;
        }
        catch (Exception e) {
            throw new DMException(DMException.DM00X, e.getMessage());
        }
    }
    
    /** 
     * Gets the documents that imports the document provided.
     *
     * @param id            the document id (meta data)
     * @return              It returns a list with the parent documents
     * @throws DMException  DM004 - XML document not found
     *                       DM00X - External error
     */
    public String getParentDependencies(String id) throws DMException {
        JSONArray jsonList = getParentDependencies(id, DocumentManagerUtils.getRootCollection(), "");
        return jsonList.toString();
    }
    
    /**
     * Gets the parent documents from a collection.
     * @param id            The document id whose parents must be seek
     * @param col           The collection to seek on
     * @param path          The current path
     * @return              The array of parents
     * @throws DMException  DM00X - External error
     */
    JSONArray getParentDependencies(String id, Collection col, String path) throws DMException {
        JSONArray parents = new JSONArray();
        Collection collection = null;
        try {
            String[] child = col.listChildCollections();
            col.close();
            for (int i = 0; i < child.length; i++) {
                collection = DocumentManagerUtils.getCollection(path + "/" + child[i]);
                ResourceSet result = DocumentManagerUtils.getParentDependencies(id, collection);
                ResourceIterator results = result.getIterator();
                while (results.hasMoreResources()) {
                    Resource res = results.nextResource();
                    Node header = DocumentManagerUtils.stringToDom(new String(res.getContent().toString().getBytes("UTF-8"), "UTF-8"));
                    JSONObject dependency = new JSONObject();
                    String name = ((Element) header.getFirstChild()).getElementsByTagNameNS(MMSURI, "name").item(0).getTextContent();
                    String parentId = ((Element) header.getFirstChild()).getElementsByTagNameNS(MMSURI, "id").item(0).getTextContent();
                    dependency.put("id", parentId);
                    dependency.put("name", name);
                    dependency.put("collection", path + "/" + child[i]);
                    parents.put(dependency);
                }
                JSONArray childrenCollectionResults = getParentDependencies(id, collection, path + "/" + child[i]);
                for (int j = 0; j < childrenCollectionResults.length(); j++) {
                    parents.put(childrenCollectionResults.get(j));
                }
                collection.close();
            }
            return parents;
        }
        catch (DMException e) {
            throw e;
        }
        catch (Exception e) {
            throw new DMException(DMException.DM00X, e.toString());
        }
        finally {
            try { 
                if (col != null) {
                    col.close();
                }
                if (collection != null) {
                    collection.close();
                }
            }
            catch (Exception e) {
                throw new DMException(DMException.DM00X, e.toString());
            }
        }
    }
    
    /**
     * Load external documents and validate them recursively.
     * @param doc           The current document to get external documents from
     * @throws SUException  
     * @throws DMException  DM00X - External error
     */
    private void validateExternalDocuments(Document doc) throws SUException, DMException {
    	ArrayList<String> ids = new ArrayList<String>();
        ArrayList<Document> imported = new ArrayList<Document>();
        NodeList externalReferences = DocumentManagerUtils.find(doc, "//*[not(*) and not(parent::mms:head) and matches(text(), "
                + "'^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$')]");
        for (int i = 0; i < externalReferences.getLength(); i++) {
            String docId = externalReferences.item(i).getTextContent();
            //Check if id not imported and validated yet
            if (!ids.contains(docId)) {
                ids.add(docId);
                try {
                    //Insert the external document into the current one
                    String xmlStringDoc = SimflownyUtils.jsonToXML(getDocument(docId));
                    Document externalDoc = DocumentManagerUtils.stringToDom(xmlStringDoc);
                    validateExternalDocuments(externalDoc);
                    imported.add(externalDoc);
                    externalReferences.item(i).getParentNode().appendChild(doc.importNode(externalDoc.getDocumentElement(), true));
                    //Validate the external document
                    validateDocument(SimflownyUtils.xmlToJSON(xmlStringDoc, false), true);
                } 
                catch (DMException e) {
                    e.printStackTrace(System.out);
                    throw e;
                }
            } 
            else {
            	//If already imported does not validate it
            	externalReferences.item(i).getParentNode().appendChild(doc.importNode(imported.get(ids.indexOf(docId)).getDocumentElement(), true));
            }
        }
    }
    
    /**
     * Changes the id in documents that has dependencies from old id.
     * @param oldId         The document old Id whose parents must be seek
     * @param newId         The document new Id to use
     * @param col           The collection to seek on
     * @param path          The current path
     * @throws DMException  DM00X - External error
     */
    private static void changeDependencies(String oldId, String newId, Collection col, String path) throws DMException {
        Collection collection = null;
        try {
            String[] child = col.listChildCollections();
            col.close();
            for (int i = 0; i < child.length; i++) {
                collection = DocumentManagerUtils.getCollection(path + "/" + child[i]);
                ResourceSet result = DocumentManagerUtils.getDependentDocuments(oldId, collection);
                ResourceIterator results = result.getIterator();
                while (results.hasMoreResources()) {
                	XMLResource res = (XMLResource) results.nextResource();
                    Node document = DocumentManagerUtils.stringToDom(new String(res.getContent().toString().getBytes("UTF-8"), "UTF-8"));
                    NodeList ids = DocumentManagerUtils.find(document, "//mms:id[text() = '" + oldId + "']|"
                    		+ "//sml:import/sml:ruleId[text() = '" + oldId + "']");
                    for (int j = 0; j < ids.getLength(); j++) {
                    	ids.item(j).setTextContent(newId);
                    }
                    //Setting the xml resource
                    XMLResource model = (XMLResource) col.createResource(res.getDocumentId(), XMLResource.RESOURCE_TYPE);
                    model.setContent(DocumentManagerUtils.domToString(document));
                    collection.storeResource(model);
                }
                changeDependencies(oldId, newId, collection, path + "/" + child[i]);
                collection.close();
            }
        }
        catch (DMException e) {
            throw e;
        }
        catch (Exception e) {
        	e.printStackTrace();
            throw new DMException(DMException.DM00X, e.toString());
        }
        finally {
            try { 
                if (col != null) {
                    col.close();
                }
                if (collection != null) {
                    collection.close();
                }
            }
            catch (Exception e) {
                throw new DMException(DMException.DM00X, e.toString());
            }
        }
    }
    
    /**
     * Validate variables used in the documents.
     * @param doc			Document to validate
     * @throws DMException	DM009    XML document validation error
     */
    private void validateVariables(Document doc) throws DMException {
    	String docType = doc.getDocumentElement().getLocalName();
    	if (docType.equals("PDEModel") || docType.equals("PDEProblem")) {
    		HashSet<String> declaredVariables = new HashSet<String>();
    		//Get all declared variables
    		NodeList vars = DocumentManagerUtils.find(doc, "//mms:spatialCoordinate|//mms:timeCoordinate|//mms:fields/mms:field|"
    				+ "//mms:auxiliaryFields/mms:auxiliaryField|//mms:auxiliaryVariables/mms:auxiliaryVariable|//mms:parameter/mms:name|"
    				+ "//mms:tensors/mms:tensor|//mms:eigenName|//mms:eigenVectors/mms:eigenVector|//mms:analysisFields/mms:analysisField|"
    				+ "//mms:auxiliaryAnalysisVariables/mms:auxiliaryAnalysisVariable|//mms:equivalence|/mms:problemElement");
    		for (int i = 0; i < vars.getLength(); i++) {
    			declaredVariables.add(vars.item(i).getTextContent());
    		}
    		vars = DocumentManagerUtils.find(doc, "//mms:spatialCoordinate|//mms:timeCoordinate");
    		for (int i = 0; i < vars.getLength(); i++) {
    			declaredVariables.add("Δ" + vars.item(i).getTextContent());
    			declaredVariables.add("\u0094" + vars.item(i).getTextContent());
    		}
    		HashSet<String> undeclaredVariables = new HashSet<String>();
    		NodeList usedVars = DocumentManagerUtils.find(doc, "//mt:ci[not(ancestor::sml:simml) and not(ancestor::mms:initialCondition)]");
    		for (int i = 0; i < usedVars.getLength(); i++) {
    			if (!declaredVariables.contains(usedVars.item(i).getTextContent())) {
    				undeclaredVariables.add(usedVars.item(i).getTextContent());
    			}
    		}    
    		if (undeclaredVariables.size() > 0) {
    	         throw new DMException(DMException.DM009, "The following variables have not been properly declared: " + undeclaredVariables);
    		}
    	}
    }
}
