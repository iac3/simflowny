package eu.iac3.mathMS.DBDownloader;
/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny/copyright_and_license.html
*/
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.exist.xmldb.DatabaseImpl;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xmldb.api.DatabaseManager;
import org.xmldb.api.base.Collection;
import org.xmldb.api.base.Database;
import org.xmldb.api.modules.BinaryResource;
import org.xmldb.api.modules.XMLResource;

public class DBDownloader implements BundleActivator {

 
	@Override
	public void start(BundleContext context) throws Exception {
		Collection col = null;
        try { 
        	@SuppressWarnings("rawtypes")
        	ServiceReference serviceReference = context.getServiceReference(DatabaseImpl.class.getName());
        	@SuppressWarnings("unchecked")
        	Database database = (Database) ((DatabaseImpl) context.getService(serviceReference)); 
            DatabaseManager.registerDatabase(database);

            col = DatabaseManager.getCollection("xmldb:exist:///db/");
     
            File dir = new File("db_downloaded");  
            dir.mkdir();
            
            //Getting the child collections
            String[] child = col.listChildCollections();
            col.close();
            for (int i = 0; i < child.length; i++) {
                //The metadata collection should not be shown
                if (!child[i].equals("Metadata") && !child[i].equals("system") && !child[i].equals("meta")) {
                    getSchemaStructuredNode("db/" + child[i], context);
                }
            }
            System.out.println("Download completed!!");
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            try { 
                if (col != null) {
                    col.close();
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
	}
    
    /** 
     * Return the node element for the actual collection following the SchemaStructure XSD. 
     *
     * @param coll          The base collection
     * @param doc           The DOM document base for the xml
     * @return              It returns the root node for the collection following the
     *                      SchemaStructure XSD 
     */
    static String getSchemaStructuredNode(String coll, BundleContext context) {
        Collection col = null;
        try {
        	@SuppressWarnings("rawtypes")
        	ServiceReference serviceReference = context.getServiceReference(DatabaseImpl.class.getName());
        	@SuppressWarnings("unchecked")
        	Database database = (Database) ((DatabaseImpl) context.getService(serviceReference)); 
            DatabaseManager.registerDatabase(database);
            
            col = DatabaseManager.getCollection("xmldb:exist:///" + coll);
            //Element name
            File dir = new File(coll.replaceFirst("db", "db_downloaded"));  
            dir.mkdir();
            
            //Element collections
            String[] child = col.listChildCollections();
            for (int i = 0; i < child.length; i++) {
                if (!child[i].equals("Metadata")) {
                    getSchemaStructuredNode(coll + "/" + child[i], context);
                }
            }
            //Element documents
            String[] resource = col.listResources();
            for (int i = 0; i < resource.length; i++) {
                //Save document to file
                String doc = "";
                if (col.getResource(resource[i]).getResourceType().equals("BinaryResource")) {
                    BinaryResource document = (BinaryResource) col.getResource(resource[i]);
                    if (document != null) {
                        doc = new String((byte[]) document.getContent(), "UTF-8");
                        stringToFile(coll.replaceFirst("db", "db_downloaded") + File.separator + resource[i].toString(), doc);
                    }
                }
                else {
                    XMLResource document = (XMLResource) col.getResource(resource[i]);
                    if (document != null) {
                    	System.out.println(coll + " " + resource[i]);
                        try {
                        	doc = indent(new String(document.getContent().toString().getBytes("UTF-8"), "UTF-8"));
                        }
                        catch (Exception e) {
                        	e.printStackTrace();
                        	System.out.println("Fallo al cargar resource " + resource[i] + " de " + coll);
                        }
                    }
                }
                if (!doc.equals("") && !col.getResource(resource[i]).getResourceType().equals("BinaryResource")) {
                    stringToFile(coll.replaceFirst("db", "db_downloaded") + File.separator + resource[i].toString() + ".xml", doc);
                }
            }
            
            return "";
        }
        catch (Exception e) {
            e.printStackTrace();
            return "";
        }

    }

    /**
     * A method for the format of the output.
     * 
     * @param document          The document to be formatted
     * @return                  The document formatted
     * @throws Exception 
     */
    public static String indent(String document) throws Exception  {
        DocumentBuilder docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        Document doc = docBuilder.parse(new ByteArrayInputStream(document.getBytes("UTF-8")));

        // Setup pretty print options
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

        // Return pretty print xml string
        StringWriter stringWriter = new StringWriter();
        transformer.transform(new DOMSource(doc), new StreamResult(stringWriter));
        return stringWriter.toString();
    }
    
    static String stringToFile(String path, String doc) {
        try{
            BufferedWriter out = new BufferedWriter(new FileWriter(path));
            String result = "";
            out.write(doc);
            out.close();
            return result;
        }
        catch(Exception e) {
            e.printStackTrace();
            return "";
        }
    }
    
    /**
     * Converts a string to Dom.
     * 
     * @param str               The string
     * @return                  The Dom document
     * @throws CGException      CG001 Not a valid XML Document
     *                          CG004 External error
     */
    public static Document stringToDom(String str) {
        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            docFactory.setNamespaceAware(true);
            docFactory.setIgnoringElementContentWhitespace(true);
            docFactory.setIgnoringComments(true);
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            InputStream is = new ByteArrayInputStream(str.getBytes("UTF-8"));
            Document doc = docBuilder.parse(is);
            removeWhitespaceNodes(doc.getDocumentElement());
            return doc;
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    /**
     * Converts a node to string.
     * 
     * @param node              The node
     * @return                  The String
     */
    public static String domToString(Node node) {
        try {
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            StreamResult result = new StreamResult(new ByteArrayOutputStream());
            DOMSource source = new DOMSource(node);
            transformer.transform(source, result);
            return new String(((ByteArrayOutputStream) result.getOutputStream()).toString().getBytes("UTF-8"), "UTF-8");
        }
        catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }
    
    /**
     * Remove the whitespaced nodes from a Dom element.
     * 
     * @param e The element to delete the whitespaces from
     */
    public static void removeWhitespaceNodes(Element e) {
        NodeList children = e.getChildNodes();
        for (int i = children.getLength() - 1; i >= 0; i--) {
            Node child = children.item(i);
            if (child instanceof Text && ((Text) child).getData().trim().length() == 0) {
                e.removeChild(child);
            }
            else if (child instanceof Element) {
                removeWhitespaceNodes((Element) child);
            }
        }
    }


	@Override
	public void stop(BundleContext arg0) throws Exception {
	}

}
