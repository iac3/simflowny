/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.AGDM.utils.PDE;

/**
 * Operator discretization pair. The operator name and its spatial discretization.
 * @author bminyano
 *
 */
public class OperatorDiscretization {
    private String operator;
    private String schema;
    
    public String getSchema() {
        return schema;
    }
    public String getOperator() {
        return operator;
    }
    public void setSchema(String schema) {
        this.schema = schema;
    }
    public void setOperator(String operator) {
        this.operator = operator;
    }
}
