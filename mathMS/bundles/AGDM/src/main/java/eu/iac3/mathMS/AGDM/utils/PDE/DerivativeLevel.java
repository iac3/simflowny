/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.AGDM.utils.PDE;

import java.util.ArrayList;

/**
 * The information needed to process an equation.
 * @author bminyano
 *
 */
public class DerivativeLevel {
    private ArrayList<Derivative> derivatives;
    
    /**
     * Constructor.
     */
    public DerivativeLevel() {
        derivatives = new ArrayList<Derivative>();
    }
    
    /**
     * Copy constructor.
     * @param dl    The object to copy
     */
    public DerivativeLevel(ArrayList<Derivative> dl) {
        derivatives = new ArrayList<Derivative>();
        for (Derivative d : dl) {
        	derivatives.add(new Derivative(d));
        }
    }
    
    /**
     * Copy constructor.
     * @param dl    The object to copy
     */
    public DerivativeLevel(DerivativeLevel dl) {
        derivatives = new ArrayList<Derivative>();
        for (Derivative d : dl.getDerivatives()) {
        	derivatives.add(new Derivative(d));
        }
    }
   
    /**
     * Add a derivative to the list.
     * @param d The new derivative
     */
    public void add(Derivative d) {
        derivatives.add(d);
    }
    
    /**
     * Add all derivatives to the current derivative level.
     * @param dl    The new derivatives to add
     */
    public void addAll(DerivativeLevel dl) {
        derivatives.addAll(dl.getDerivatives());
    }

    public ArrayList<Derivative> getDerivatives() {
        return derivatives;
    }
    
    /**
     * Gets the derivative sources for a field.
     * @param field	The field to filter terms
     * @return		Sources
     */
    public ArrayList<Derivative> getFilteredDerivativesSources(String field) {
        ArrayList<Derivative> filteredDerivatives = new ArrayList<Derivative>();
        for (int i = 0; i < derivatives.size(); i++) {
            if (!derivatives.get(i).getDerivativeTerm().isPresent() && derivatives.get(i).getField().equals(field)) {
                filteredDerivatives.add(derivatives.get(i));
            }
        }
        return filteredDerivatives;
    }

    /**
     * Gets the derivative sources with given ids.
     * @param field	The field to filter terms
     * @return		Sources
     */
    public ArrayList<Derivative> getFilteredDerivativesSources(ArrayList<String> ids) {
        ArrayList<Derivative> filteredDerivatives = new ArrayList<Derivative>();
        for (int i = 0; i < derivatives.size(); i++) {
            if (!derivatives.get(i).getDerivativeTerm().isPresent() && ids.contains(derivatives.get(i).getId())) {
                filteredDerivatives.add(derivatives.get(i));
            }
        }
        return filteredDerivatives;
    }
    
    /**
     * Gets the derivative with given id.
     * @param field	The field to filter terms
     * @return		Derivative
     */
    public Derivative getFilteredDerivatives(String id) {
        for (int i = 0; i < derivatives.size(); i++) {
            if (id.equals(derivatives.get(i).getId())) {
                return derivatives.get(i);
            }
        }
        return null;
    }
    
    @Override
    public String toString() {
        return "DerivativeLevel [derivatives=" + derivatives + "]";
    }
    
}
