package eu.iac3.mathMS.AGDM.processors;

import eu.iac3.mathMS.AGDM.AGDMException;
import eu.iac3.mathMS.AGDM.ExtrapolationType;
import eu.iac3.mathMS.AGDM.utils.AGDMUtils;
import eu.iac3.mathMS.AGDM.utils.CoordinateInfo;
import eu.iac3.mathMS.AGDM.utils.IncompatibilityFields;
import eu.iac3.mathMS.AGDM.utils.PDE.ProcessInfo;

import java.util.ArrayList;
import java.util.Iterator;

import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

/**
 * The class that implements all the extrapolation methods.
 * @author bminano
 *
 */
public class Extrapolation {

    static String mmsUri = "urn:mathms";
    static String smlUri = "urn:simml";
    static String mtUri = "http://www.w3.org/1998/Math/MathML";
    static final int THREE = 3;
    static final int FOUR = 4;
    static final int FIVE = 5;
    static final int SIX = 6;
    static final int SEVEN = 7;
    static final int EIGHT = 8;
    
    /**
     * Constructor.
     * @throws AGDMException 
     */
    Extrapolation() throws AGDMException {
    };
    
    /**
     * Creates the instructions for the extrapolation of incompatible fields in hard region boundaries.
     * @param result                The problem being discretized
     * @param problem               The document with the info
     * @param timeSlice             The time slice
     * @param pi                    The process information
     * @return                      The extrapolation instructions
     * @throws AGDMException        AGDM007 External error
     */
    public static DocumentFragment processExtrapolation(Document result, Node problem, Element timeSlice, ProcessInfo pi) 
            throws AGDMException {
            //Result is stored in document fragment
            DocumentFragment fragmentResult = result.createDocumentFragment();
            IncompatibilityFields incompFields = pi.getIncomFields();
            incompFields.toString();
            //Other region's incompatible fields
            Iterator<String> incompIt = incompFields.getRegionFields().keySet().iterator();
            while (incompIt.hasNext()) {
                String field = incompIt.next();
                ProcessInfo newPi = new ProcessInfo(pi);
                newPi.setField(field);
                Element incompElem = AGDMUtils.createElement(result, smlUri, "fieldExtrapolation");
                incompElem.setAttribute("fieldAtt", field);
            	incompElem.appendChild(createExtrapolationCallPDE(result, newPi.getBaseSpatialCoordinates(), field, timeSlice, null, newPi, false));
                if (incompElem.hasChildNodes()) {
                    fragmentResult.appendChild(incompElem);
                }
            }
            //Boundaries incompatible fields
            incompIt = incompFields.getBoundaryFields().keySet().iterator();
            while (incompIt.hasNext()) {
                String boundary = incompIt.next();
                String axis = boundary.substring(0, boundary.indexOf("-"));
                String side = boundary.substring(boundary.indexOf("-") + 1);
                Element axisE = AGDMUtils.createElement(result, smlUri, "axis");
                axisE.setAttribute("axisAtt", axis);
                Element sideE = AGDMUtils.createElement(result, smlUri, "side");
                sideE.setAttribute("sideAtt", side.toLowerCase());
                axisE.appendChild(sideE);
                ArrayList<String> fields = incompFields.getBoundaryFields().get(boundary);
          
                for (int i = 0; i < fields.size(); i++) {
                    String field = fields.get(i);
                    String segWithField = pi.getRegionName().substring(0, pi.getRegionName().length() - 1);
                    ProcessInfo newPi = new ProcessInfo(pi);
                    newPi.setField(field);
                    Element incompElem = AGDMUtils.createElement(result, smlUri, "fieldExtrapolation");
                    incompElem.setAttribute("fieldAtt", field);
             		incompElem.appendChild(createExtrapolationCallPDE(result, newPi.getBaseSpatialCoordinates(), field, timeSlice, segWithField, newPi, true)); 
                    if (incompElem.hasChildNodes()) {
                        sideE.appendChild(incompElem);
                    }
                }
                if (sideE.hasChildNodes()) {
                    fragmentResult.appendChild(axisE);
                }
            }
            
            return fragmentResult;
        }
    

    /**
     * Creates the calls to the extrapolation functions.
     * @param result                The result problem
     * @param coordinates           The coordinates
     * @param field                 The field to extrapolate
     * @param timeSlice             The timeslice of the variable to extrapolate
     * @param regionName            The region name from the interaction
     * @param pi                    The process information
     * @param boundaries            If the call is from the boundaries
     * @return                      The calling
     * @throws AGDMException        AGDM00X  External error
     */
    public static Element createExtrapolationCallPDE(Document result, ArrayList<String> coordinates, String field, 
            Element timeSlice, String regionName, ProcessInfo pi, boolean boundaries) throws AGDMException {
    	//Create the target variable
        Element target = (Element) timeSlice.cloneNode(false);
        target.appendChild(AGDMUtils.getExtrapolationVariable(timeSlice.getOwnerDocument(), true, false));
        //Create the FOV variable
        Element fov = createFovVariable(result, regionName, boundaries);

        Element math = AGDMUtils.createElement(result, mtUri, "math");
        Element function = AGDMUtils.createElement(result, smlUri, "functionCall");
        Element name = AGDMUtils.createElement(result, mtUri, "ci");
        name.setTextContent("extrapolate_field");
        function.appendChild(name);
        String origCCoord = pi.getSpatialCoord1();
        for (int i = 0; i < coordinates.size(); i++) {
            String coord = coordinates.get(i);
            String discCoord = pi.getCoordinateRelation().getDiscCoords().get(i);
            pi.setSpatialCoord1(coord);
            
            //Distance
            Element distance = AGDMUtils.createElement(result, mtUri, "ci");
            distance.setTextContent("d_" + discCoord + "_" + field);
            function.appendChild(distance);
            if (pi.hasFluxes() && ProcessInfo.getNumberOfRegions() > 1) {
	            //Target axis field
	            Element targetAxisE = (Element) result.importNode(ExecutionFlow.replaceTags(target.getFirstChild().cloneNode(true), pi), true);
	            function.appendChild(targetAxisE.getFirstChild());
            }
            //Coordinates
            Element coordE = AGDMUtils.createElement(result, mtUri, "ci");
            coordE.setTextContent(discCoord);
            function.appendChild(coordE);
        }
        pi.setSpatialCoord1(origCCoord);
        //Target field
        if (pi.getRegionInfo().isAuxiliaryField(pi.getField()) && pi.isStepVariable(timeSlice.getFirstChild(), false)) {
        	Node fieldTime = AGDMUtils.createTimeSlice(result);            		
        	fieldTime = ExecutionFlow.replaceTags(fieldTime, pi).getFirstChild();
        	if (timeSlice.getFirstChild().getLocalName().equals("sharedVariable")) {
        		Node tmp = result.importNode(timeSlice.getFirstChild().cloneNode(false), true);
        		tmp.appendChild(fieldTime.cloneNode(true));
        		fieldTime = tmp;
        	}
        	function.appendChild(fieldTime);
        }
        else {
	        if (timeSlice.getFirstChild().getLocalName().equals("sharedVariable")) {
	            Node tsVar = timeSlice.getFirstChild().cloneNode(false);
	            tsVar.appendChild(timeSlice.getFirstChild().getFirstChild().getFirstChild().cloneNode(true));
	            function.appendChild(result.importNode(ExecutionFlow.replaceTags(tsVar, pi), true));
	        }
	        else {
	            Element targetField = (Element) timeSlice.cloneNode(true);
	            targetField = ExecutionFlow.replaceTags(targetField, pi);
	            function.appendChild(result.importNode(targetField.getFirstChild().getFirstChild(), true));
	        }
	    }
        //FOV
        function.appendChild(fov);
        math.appendChild(function);
        return math;
    }
  
    /**
     * Creates the extrapolation functions.
     * @param result            The problem
     * @param coordinates       The spatial coordinates
     * @param method            The extrapolation method
     * @param ci                The coordinate information
     * @param hasFluxes			If the equations has fluxes
     * @return                  The code
     * @throws AGDMException    AGDM00X External error
     */
    public static DocumentFragment createExtrapolationFunctions(Document result, ArrayList<String> coordinates, ExtrapolationType method, 
    		CoordinateInfo ci, boolean hasFluxes) throws AGDMException {
        DocumentFragment extrapolFuncs = result.createDocumentFragment();
        
        DocumentFragment extrapol = result.createDocumentFragment();
        extrapol.appendChild(createSignVars(result, coordinates));
        extrapol.appendChild(createDisplacement(result, coordinates, null, null));
        for (int j = 0; j < coordinates.size(); j++) {
            String coord = coordinates.get(j);
            Element sourceVar = getExtrapolationVariable(result, "to_f", coordinates);;
            Element targetVar = null;
            if (hasFluxes && ProcessInfo.getNumberOfRegions() > 1) {
                targetVar = getExtrapolationVariable(result, "to_" + coord + "_f", coordinates);
            } 
            else {
                targetVar = AGDMUtils.createElement(result, mtUri, "ci");
                targetVar.setTextContent("to_" + coord + "_f");
            }
            if (method.equals(ExtrapolationType.Linear_LagrangianExtrapolation)) {
                extrapol.appendChild(createLinearLagrangianExtrapolation(result, targetVar, sourceVar, coordinates, coord));
            }
            if (method.equals(ExtrapolationType.Quadratic_LagrangianExtrapolation)) {
                extrapol.appendChild(createQuadraticLagrangianExtrapolation(result, targetVar, sourceVar, coordinates, coord));
            }
            if (method.equals(ExtrapolationType.Cubic_LagrangianExtrapolation)) {
                extrapol.appendChild(createCubicLagrangianExtrapolation(result, targetVar, sourceVar, coordinates, coord));
            }
        }
        
        extrapolFuncs.appendChild(createExtrapolationFunction(result, coordinates, (DocumentFragment) extrapol.cloneNode(true), ci, true, true, hasFluxes));

        return extrapolFuncs;
    }
    
    /**
     * Gets the extrapolation variable.
     * @param doc               The base document for the xml
     * @param varName           The variable name
     * @param coords            The spatial coordinates
     * @return                  The xml variable structure
     */
    private static Element getExtrapolationVariable(Document doc, String varName, ArrayList<String> coords) {
        Element extrapolVar = AGDMUtils.createElement(doc, mtUri, "ci");
        extrapolVar.setTextContent(varName);
        Element apply = AGDMUtils.createElement(doc, mtUri, "apply");
        apply.appendChild(extrapolVar);
        apply.appendChild(AGDMUtils.currentCell(doc, coords));
        return apply;
    }
    
    /**
     * Create the extrapolation function.
     * @param result                The result problem   
     * @param coordinates           The coordinates
     * @param extrapolation         The extrapolation code
     * @param ci                    The coordinate information
     * @param fieldExtrapolation    If the code is intended for field extrapolation or only for flux extrapolation
     * @param localAxisExtrapolation			If extrapolation is for a local axis
     * @param hasFluxes				If the equations has fluxes
     * @return                      The code
     * @throws AGDMException        AGDM00X  External error
     */
    public static Element createExtrapolationFunction(Document result, ArrayList<String> coordinates, DocumentFragment extrapolation, 
    		CoordinateInfo ci, boolean fieldExtrapolation, boolean localAxisExtrapolation, boolean hasFluxes) throws AGDMException {
        String functionName = "extrapolate_field";
        if (!fieldExtrapolation) {
            functionName = "extrapolate_flux";
        }
        //Function
        Element function = AGDMUtils.createElement(result, mmsUri, "function");
        //Function name
        Element funcName = AGDMUtils.createElement(result, mmsUri, "functionName");
        funcName.setTextContent(functionName);
        function.appendChild(funcName);
        //Function parameters
        //The parameters are the spatial coordinates of the field
        Element funcParams = AGDMUtils.createElement(result, mmsUri, "functionParameters");
        for (int i = 0; i < coordinates.size(); i++) {
            String coord = coordinates.get(i);
            //Distance
            Element funcParam = AGDMUtils.createElement(result, mmsUri, "functionParameter");
            Element paramName = AGDMUtils.createElement(result, mmsUri, "name");
            paramName.setTextContent("d_" + coord + "_f");
            Element paramType = AGDMUtils.createElement(result, mmsUri, "type");
            paramType.setTextContent("field");
            funcParam.appendChild(paramName);
            funcParam.appendChild(paramType);
            funcParams.appendChild(funcParam);
            if (!localAxisExtrapolation) {
	            //Source field
	            Element funcParam1 = AGDMUtils.createElement(result, mmsUri, "functionParameter");
	            Element paramName1 = AGDMUtils.createElement(result, mmsUri, "name");
	            paramName1.setTextContent("from_" + coord + "_f");
	            Element paramType1 = AGDMUtils.createElement(result, mmsUri, "type");
	            paramType1.setTextContent("field");
	            funcParam1.appendChild(paramName1);
	            funcParam1.appendChild(paramType1);
	            funcParams.appendChild(funcParam1);
            }
            if (hasFluxes && ProcessInfo.getNumberOfRegions() > 1) {
	            //Target axis field
	            Element funcParam2 = AGDMUtils.createElement(result, mmsUri, "functionParameter");
	            Element paramName2 = AGDMUtils.createElement(result, mmsUri, "name");
	            paramName2.setTextContent("to_" + coord + "_f");
	            Element paramType2 = AGDMUtils.createElement(result, mmsUri, "type");
	            paramType2.setTextContent("field");
	            funcParam2.appendChild(paramName2);
	            funcParam2.appendChild(paramType2);
	            funcParams.appendChild(funcParam2);
            }
            //Coordinates
            Element funcParam4 = AGDMUtils.createElement(result, mmsUri, "functionParameter");
            Element paramName4 = AGDMUtils.createElement(result, mmsUri, "name");
            paramName4.setTextContent(coord);
            Element paramType4 = AGDMUtils.createElement(result, mmsUri, "type");
            paramType4.setTextContent("int");
            funcParam4.appendChild(paramName4);
            funcParam4.appendChild(paramType4);
            funcParams.appendChild(funcParam4);
        }
        //Target field
        if (fieldExtrapolation) {
            Element funcParam = AGDMUtils.createElement(result, mmsUri, "functionParameter");
            Element paramName = AGDMUtils.createElement(result, mmsUri, "name");
            paramName.setTextContent("to_f");
            Element paramType = AGDMUtils.createElement(result, mmsUri, "type");
            paramType.setTextContent("field");
            funcParam.appendChild(paramName);
            funcParam.appendChild(paramType);
            funcParams.appendChild(funcParam);
        }
        //FOV
        Element funcParam3 = AGDMUtils.createElement(result, mmsUri, "functionParameter");
        Element paramName3 = AGDMUtils.createElement(result, mmsUri, "name");
        paramName3.setTextContent("FOV");
        Element paramType3 = AGDMUtils.createElement(result, mmsUri, "type");
        paramType3.setTextContent("field");
        funcParam3.appendChild(paramName3);
        funcParam3.appendChild(paramType3);
        funcParams.appendChild(funcParam3);
        
        function.appendChild(funcParams);
        //Function instructions
        Element funcInstructions = AGDMUtils.createElement(result, mmsUri, "functionInstructions");
        Element sml = AGDMUtils.createElement(result, smlUri, "simml");
        sml.appendChild(extrapolation);
        if (fieldExtrapolation) {
            sml.appendChild(unifyAxisExtrapolations(result, coordinates, ci, localAxisExtrapolation, hasFluxes));
        }
        funcInstructions.appendChild(sml);
        function.appendChild(funcInstructions);
        
        return function;
    }
        
    /**
     * Create the linear Lagrangian extrapolation.
     * Right extrapolation: -dist*y1+dist*y2+sign*y2
     * Left extrapolation: -dist*y1+dist*y2-sign*y1
     * @param result            The document
     * @param targetVar         The extrapolation variable
     * @param sourceVar         The time slice of the source variable
     * @param coordinates       The spatial coordinates
     * @param coord             The coordinate
     * @return                  The instructions
     * @throws AGDMException    AGDM00X External error
     */
    private static DocumentFragment createLinearLagrangianExtrapolation(Document result, Element targetVar, Element sourceVar, ArrayList<String> coordinates, 
            String coord) throws AGDMException {
        DocumentFragment res = result.createDocumentFragment();
        
        //Greater than 0
        Element ifElem = AGDMUtils.createElement(result, smlUri, "if");
        Element condition = AGDMUtils.createElement(result, mtUri, "math");
        condition.appendChild(axisExtrapolationCondition(result, coord, coordinates, null, "gt"));
        ifElem.appendChild(condition);
        Element then = AGDMUtils.createElement(result, smlUri, "then");
        ifElem.appendChild(then);
        res.appendChild(ifElem);
        //y1 = vector(parfrom_i_f, int(pari - (vector(pard_i_f, pari, parj, park) + 1 * sign_i_ext)), int(parj - disp_j_i), int(park - disp_k_i));
        Element math = AGDMUtils.createElement(result, mtUri, "math");
        Element apply = AGDMUtils.createElement(result, mtUri, "apply");
        Element eq = AGDMUtils.createElement(result, mtUri, "eq");
        Element ex = AGDMUtils.createElement(result, mtUri, "ci");
        ex.setTextContent("y1");
        math.appendChild(apply);
        apply.appendChild(eq);
        apply.appendChild(ex);
        apply.appendChild(generateFieldValue(result, coordinates, coord, sourceVar, 1));
        then.appendChild(math);
        //y2 = vector(parfrom_i_f, int(pari - (vector(pard_i_f, pari, parj, park))), int(parj - disp_j_i), int(park - disp_k_i));
        math = AGDMUtils.createElement(result, mtUri, "math");
        apply = AGDMUtils.createElement(result, mtUri, "apply");
        eq = AGDMUtils.createElement(result, mtUri, "eq");
        Element ey = AGDMUtils.createElement(result, mtUri, "ci");
        ey.setTextContent("y2");
        math.appendChild(apply);
        apply.appendChild(eq);
        apply.appendChild(ey);
        apply.appendChild(generateFieldValue(result, coordinates, coord, sourceVar, 0));
        then.appendChild(math);
        //vector(parto_i_f, pari, parj, park) = POLINT_MACRO_LINEAR_1(y1, y2, vector(pard_i_f, pari, parj, park), sign_i_ext);
        math = AGDMUtils.createElement(result, mtUri, "math");
        apply = AGDMUtils.createElement(result, mtUri, "apply");
        eq = AGDMUtils.createElement(result, mtUri, "eq");
        math.appendChild(apply);
        apply.appendChild(eq);
        apply.appendChild(targetVar.cloneNode(true));
        Element functionCall = AGDMUtils.createElement(result, smlUri, "functionCall");
        Element functionName = AGDMUtils.createElement(result, mtUri, "ci");
        functionName.setTextContent("POLINT_MACRO_LINEAR_1");
        Element paramy1 = AGDMUtils.createElement(result, mtUri, "ci");
        paramy1.setTextContent("y1");
        Element paramy2 = AGDMUtils.createElement(result, mtUri, "ci");
        paramy2.setTextContent("y2");
        Element paramDist = AGDMUtils.createElement(result, mtUri, "apply");
        Element dist = AGDMUtils.createElement(result, mtUri, "ci");
        dist.setTextContent("d_" + coord + "_f");
        paramDist.appendChild(dist);
        paramDist.appendChild(AGDMUtils.currentCell(result, coordinates));
        Element paramSign = AGDMUtils.createElement(result, mtUri, "ci");
        paramSign.setTextContent("sign_" + coord + "_ext");
        functionCall.appendChild(functionName);
        functionCall.appendChild(paramy1);
        functionCall.appendChild(paramy2);
        functionCall.appendChild(paramDist);
        functionCall.appendChild(paramSign);
        apply.appendChild(functionCall);
        then.appendChild(math);
        
        //Less than 0
        ifElem = AGDMUtils.createElement(result, smlUri, "if");
        condition = AGDMUtils.createElement(result, mtUri, "math");
        condition.appendChild(axisExtrapolationCondition(result, coord, coordinates, null, "lt"));
        ifElem.appendChild(condition);
        then = AGDMUtils.createElement(result, smlUri, "then");
        ifElem.appendChild(then);
        res.appendChild(ifElem);
        //y2 = vector(parfrom_i_f, int(pari - (vector(pard_i_f, pari, parj, park) + 1 * sign_i_ext)), int(parj - disp_j_i), int(park - disp_k_i));
        math = AGDMUtils.createElement(result, mtUri, "math");
        apply = AGDMUtils.createElement(result, mtUri, "apply");
        eq = AGDMUtils.createElement(result, mtUri, "eq");
        ex = AGDMUtils.createElement(result, mtUri, "ci");
        ex.setTextContent("y2");
        math.appendChild(apply);
        apply.appendChild(eq);
        apply.appendChild(ex);
        apply.appendChild(generateFieldValue(result, coordinates, coord, sourceVar, 1));
        then.appendChild(math);
        //y1 = vector(parfrom_i_f, int(pari - (vector(pard_i_f, pari, parj, park))), int(parj - disp_j_i), int(park - disp_k_i));
        math = AGDMUtils.createElement(result, mtUri, "math");
        apply = AGDMUtils.createElement(result, mtUri, "apply");
        eq = AGDMUtils.createElement(result, mtUri, "eq");
        ey = AGDMUtils.createElement(result, mtUri, "ci");
        ey.setTextContent("y1");
        math.appendChild(apply);
        apply.appendChild(eq);
        apply.appendChild(ey);
        apply.appendChild(generateFieldValue(result, coordinates, coord, sourceVar, 0));
        then.appendChild(math);
        //vector(parto_i_f, pari, parj, park) = POLINT_MACRO_LINEAR_2(y1, y2, vector(pard_i_f, pari, parj, park), sign_i_ext);
        math = AGDMUtils.createElement(result, mtUri, "math");
        apply = AGDMUtils.createElement(result, mtUri, "apply");
        eq = AGDMUtils.createElement(result, mtUri, "eq");
        math.appendChild(apply);
        apply.appendChild(eq);
        apply.appendChild(targetVar.cloneNode(true));
        functionCall = AGDMUtils.createElement(result, smlUri, "functionCall");
        functionName = AGDMUtils.createElement(result, mtUri, "ci");
        functionName.setTextContent("POLINT_MACRO_LINEAR_2");
        paramy1 = AGDMUtils.createElement(result, mtUri, "ci");
        paramy1.setTextContent("y1");
        paramy2 = AGDMUtils.createElement(result, mtUri, "ci");
        paramy2.setTextContent("y2");
        paramDist = AGDMUtils.createElement(result, mtUri, "apply");
        dist = AGDMUtils.createElement(result, mtUri, "ci");
        dist.setTextContent("d_" + coord + "_f");
        paramDist.appendChild(dist);
        paramDist.appendChild(AGDMUtils.currentCell(result, coordinates));
        paramSign = AGDMUtils.createElement(result, mtUri, "ci");
        paramSign.setTextContent("sign_" + coord + "_ext");
        functionCall.appendChild(functionName);
        functionCall.appendChild(paramy1);
        functionCall.appendChild(paramy2);
        functionCall.appendChild(paramDist);
        functionCall.appendChild(paramSign);
        apply.appendChild(functionCall);
        then.appendChild(math);
        
        return res;
    }
    
    /**
     * Create the quadratic Lagrangian extrapolation.
     * Right extrapolation: (1/2)*(dist^2*y1-2*dist^2*y2+dist^2*y3+dist*sign*y1-4*dist*sign*y2+3*dist*sign*y3+2*sign^2*y3)/sign^2
     * Left extrapolation: (1/2)*(dist^2*y1-2*dist^2*y2+dist^2*y3+3*dist*sign*y1-4*dist*sign*y2+dist*sign*y3+2*sign^2*y1)/sign^2
     * @param result            The document
     * @param targetVar         The extrapolation variable
     * @param sourceVar         The time slice of the source variable
     * @param coordinates       The spatial coordinates
     * @param coord             The coordinate
     * @return                  The instructions
     * @throws AGDMException    AGDM00X External error
     */
    private static DocumentFragment createQuadraticLagrangianExtrapolation(Document result, Element targetVar, Element sourceVar, ArrayList<String> coordinates, 
            String coord) throws AGDMException {
        DocumentFragment res = result.createDocumentFragment();
        
        //Greater than 0
        Element ifElem = AGDMUtils.createElement(result, smlUri, "if");
        Element condition = AGDMUtils.createElement(result, mtUri, "math");
        condition.appendChild(axisExtrapolationCondition(result, coord, coordinates, null, "gt"));
        ifElem.appendChild(condition);
        Element then = AGDMUtils.createElement(result, smlUri, "then");
        ifElem.appendChild(then);
        res.appendChild(ifElem);
        //y1 = vector(parfrom_i_f, int(pari - (vector(pard_i_f, pari, parj, park) + 2 * sign_i_ext)), int(parj - disp_j_i), int(park - disp_k_i));
        Element math = AGDMUtils.createElement(result, mtUri, "math");
        Element apply = AGDMUtils.createElement(result, mtUri, "apply");
        Element eq = AGDMUtils.createElement(result, mtUri, "eq");
        Element ex = AGDMUtils.createElement(result, mtUri, "ci");
        ex.setTextContent("y1");
        math.appendChild(apply);
        apply.appendChild(eq);
        apply.appendChild(ex);
        apply.appendChild(generateFieldValue(result, coordinates, coord, sourceVar, 2));
        then.appendChild(math);
        //y2 = vector(parfrom_i_f, int(pari - (vector(pard_i_f, pari, parj, park) + 1 * sign_i_ext)), int(parj - disp_j_i), int(park - disp_k_i));
        math = AGDMUtils.createElement(result, mtUri, "math");
        apply = AGDMUtils.createElement(result, mtUri, "apply");
        eq = AGDMUtils.createElement(result, mtUri, "eq");
        ex = AGDMUtils.createElement(result, mtUri, "ci");
        ex.setTextContent("y2");
        math.appendChild(apply);
        apply.appendChild(eq);
        apply.appendChild(ex);
        apply.appendChild(generateFieldValue(result, coordinates, coord, sourceVar, 1));
        then.appendChild(math);
        //y3 = vector(parfrom_i_f, int(pari - (vector(pard_i_f, pari, parj, park))), int(parj - disp_j_i), int(park - disp_k_i));
        math = AGDMUtils.createElement(result, mtUri, "math");
        apply = AGDMUtils.createElement(result, mtUri, "apply");
        eq = AGDMUtils.createElement(result, mtUri, "eq");
        Element ey = AGDMUtils.createElement(result, mtUri, "ci");
        ey.setTextContent("y3");
        math.appendChild(apply);
        apply.appendChild(eq);
        apply.appendChild(ey);
        apply.appendChild(generateFieldValue(result, coordinates, coord, sourceVar, 0));
        then.appendChild(math);
        //vector(parto_i_f, pari, parj, park) = POLINT_MACRO_QUADRATIC_1(y1, y2, y3, vector(pard_i_f, pari, parj, park), sign_i_ext);
        math = AGDMUtils.createElement(result, mtUri, "math");
        apply = AGDMUtils.createElement(result, mtUri, "apply");
        eq = AGDMUtils.createElement(result, mtUri, "eq");
        math.appendChild(apply);
        apply.appendChild(eq);
        apply.appendChild(targetVar.cloneNode(true));
        Element functionCall = AGDMUtils.createElement(result, smlUri, "functionCall");
        Element functionName = AGDMUtils.createElement(result, mtUri, "ci");
        functionName.setTextContent("POLINT_MACRO_QUADRATIC_1");
        Element paramy1 = AGDMUtils.createElement(result, mtUri, "ci");
        paramy1.setTextContent("y1");
        Element paramy2 = AGDMUtils.createElement(result, mtUri, "ci");
        paramy2.setTextContent("y2");
        Element paramy3 = AGDMUtils.createElement(result, mtUri, "ci");
        paramy3.setTextContent("y3");
        Element paramDist = AGDMUtils.createElement(result, mtUri, "apply");
        Element dist = AGDMUtils.createElement(result, mtUri, "ci");
        dist.setTextContent("d_" + coord + "_f");
        paramDist.appendChild(dist);
        paramDist.appendChild(AGDMUtils.currentCell(result, coordinates));
        Element paramSign = AGDMUtils.createElement(result, mtUri, "ci");
        paramSign.setTextContent("sign_" + coord + "_ext");
        functionCall.appendChild(functionName);
        functionCall.appendChild(paramy1);
        functionCall.appendChild(paramy2);
        functionCall.appendChild(paramy3);
        functionCall.appendChild(paramDist);
        functionCall.appendChild(paramSign);
        apply.appendChild(functionCall);
        then.appendChild(math);
        
        //Less than 0
        ifElem = AGDMUtils.createElement(result, smlUri, "if");
        condition = AGDMUtils.createElement(result, mtUri, "math");
        condition.appendChild(axisExtrapolationCondition(result, coord, coordinates, null, "lt"));
        ifElem.appendChild(condition);
        then = AGDMUtils.createElement(result, smlUri, "then");
        ifElem.appendChild(then);
        res.appendChild(ifElem);
        //y3 = vector(parfrom_i_f, int(pari - (vector(pard_i_f, pari, parj, park) + 2 * sign_i_ext)), int(parj - disp_j_i), int(park - disp_k_i));
        math = AGDMUtils.createElement(result, mtUri, "math");
        apply = AGDMUtils.createElement(result, mtUri, "apply");
        eq = AGDMUtils.createElement(result, mtUri, "eq");
        ex = AGDMUtils.createElement(result, mtUri, "ci");
        ex.setTextContent("y3");
        math.appendChild(apply);
        apply.appendChild(eq);
        apply.appendChild(ex);
        apply.appendChild(generateFieldValue(result, coordinates, coord, sourceVar, 2));
        then.appendChild(math);
        //y2 = vector(parfrom_i_f, int(pari - (vector(pard_i_f, pari, parj, park) + 1 * sign_i_ext)), int(parj - disp_j_i), int(park - disp_k_i));
        math = AGDMUtils.createElement(result, mtUri, "math");
        apply = AGDMUtils.createElement(result, mtUri, "apply");
        eq = AGDMUtils.createElement(result, mtUri, "eq");
        ex = AGDMUtils.createElement(result, mtUri, "ci");
        ex.setTextContent("y2");
        math.appendChild(apply);
        apply.appendChild(eq);
        apply.appendChild(ex);
        apply.appendChild(generateFieldValue(result, coordinates, coord, sourceVar, 1));
        then.appendChild(math);
        //y1 = vector(parfrom_i_f, int(pari - (vector(pard_i_f, pari, parj, park))), int(parj - disp_j_i), int(park - disp_k_i));
        math = AGDMUtils.createElement(result, mtUri, "math");
        apply = AGDMUtils.createElement(result, mtUri, "apply");
        eq = AGDMUtils.createElement(result, mtUri, "eq");
        ey = AGDMUtils.createElement(result, mtUri, "ci");
        ey.setTextContent("y1");
        math.appendChild(apply);
        apply.appendChild(eq);
        apply.appendChild(ey);
        apply.appendChild(generateFieldValue(result, coordinates, coord, sourceVar, 0));
        then.appendChild(math);
        //vector(parto_i_f, pari, parj, park) = POLINT_MACRO_QUADRATIC_1(y1, y2, y3, vector(pard_i_f, pari, parj, park), sign_i_ext);
        math = AGDMUtils.createElement(result, mtUri, "math");
        apply = AGDMUtils.createElement(result, mtUri, "apply");
        eq = AGDMUtils.createElement(result, mtUri, "eq");
        math.appendChild(apply);
        apply.appendChild(eq);
        apply.appendChild(targetVar.cloneNode(true));
        functionCall = AGDMUtils.createElement(result, smlUri, "functionCall");
        functionName = AGDMUtils.createElement(result, mtUri, "ci");
        functionName.setTextContent("POLINT_MACRO_QUADRATIC_2");
        paramy1 = AGDMUtils.createElement(result, mtUri, "ci");
        paramy1.setTextContent("y1");
        paramy2 = AGDMUtils.createElement(result, mtUri, "ci");
        paramy2.setTextContent("y2");
        paramy3 = AGDMUtils.createElement(result, mtUri, "ci");
        paramy3.setTextContent("y3");
        paramDist = AGDMUtils.createElement(result, mtUri, "apply");
        dist = AGDMUtils.createElement(result, mtUri, "ci");
        dist.setTextContent("d_" + coord + "_f");
        paramDist.appendChild(dist);
        paramDist.appendChild(AGDMUtils.currentCell(result, coordinates));
        paramSign = AGDMUtils.createElement(result, mtUri, "ci");
        paramSign.setTextContent("sign_" + coord + "_ext");
        functionCall.appendChild(functionName);
        functionCall.appendChild(paramy1);
        functionCall.appendChild(paramy2);
        functionCall.appendChild(paramy3);
        functionCall.appendChild(paramDist);
        functionCall.appendChild(paramSign);
        apply.appendChild(functionCall);
        then.appendChild(math);
        return res;
    }

    /**
     * Create the cubic Lagrangian extrapolation.
     * Right extrapolation: -(1/6)*(dist^3*y1-3*dist^3*y2+3*dist^3*y3-dist^3*y4+3*dist^2*sign*y1-12*dist^2*sign*y2+15*dist^2*sign*y3-6*dist^2*sign*y4+2*dist*sign^2*y1-9*dist*sign^2*y2+18*dist*sign^2*y3-11*dist*sign^2*y4-6*sign^3*y4)/sign^3
     * Left extrapolation: (1/6)*(dist^3*y1-3*dist^3*y2+3*dist^3*y3-dist^3*y4+6*dist^2*sign*y1-15*dist^2*sign*y2+12*dist^2*sign*y3-3*dist^2*sign*y4+11*dist*sign^2*y1-18*dist*sign^2*y2+9*dist*sign^2*y3-2*dist*sign^2*y4+6*sign^3*y1)/sign^3
     * @param result            The document
     * @param targetVar         The extrapolation variable
     * @param sourceVar         The time slice of the source variable
     * @param coordinates       The spatial coordinates
     * @param coord             The coordinate
     * @return                  The instructions
     * @throws AGDMException    AGDM00X External error
     */
    private static DocumentFragment createCubicLagrangianExtrapolation(Document result, Element targetVar, Element sourceVar, ArrayList<String> coordinates, 
            String coord) throws AGDMException {
        DocumentFragment res = result.createDocumentFragment();
        
        //Greater than 0
        Element ifElem = AGDMUtils.createElement(result, smlUri, "if");
        Element condition = AGDMUtils.createElement(result, mtUri, "math");
        condition.appendChild(axisExtrapolationCondition(result, coord, coordinates, null, "gt"));
        ifElem.appendChild(condition);
        Element then = AGDMUtils.createElement(result, smlUri, "then");
        ifElem.appendChild(then);
        res.appendChild(ifElem);
        //y1 = vector(parfrom_i_f, int(pari - (vector(pard_i_f, pari, parj, park) + 3 * sign_i_ext)), int(parj - disp_j_i), int(park - disp_k_i));
        Element math = AGDMUtils.createElement(result, mtUri, "math");
        Element apply = AGDMUtils.createElement(result, mtUri, "apply");
        Element eq = AGDMUtils.createElement(result, mtUri, "eq");
        Element ex = AGDMUtils.createElement(result, mtUri, "ci");
        ex.setTextContent("y1");
        math.appendChild(apply);
        apply.appendChild(eq);
        apply.appendChild(ex);
        apply.appendChild(generateFieldValue(result, coordinates, coord, sourceVar, 3));
        then.appendChild(math);
        //y2 = vector(parfrom_i_f, int(pari - (vector(pard_i_f, pari, parj, park) + 2 * sign_i_ext)), int(parj - disp_j_i), int(park - disp_k_i));
        math = AGDMUtils.createElement(result, mtUri, "math");
        apply = AGDMUtils.createElement(result, mtUri, "apply");
        eq = AGDMUtils.createElement(result, mtUri, "eq");
        ex = AGDMUtils.createElement(result, mtUri, "ci");
        ex.setTextContent("y2");
        math.appendChild(apply);
        apply.appendChild(eq);
        apply.appendChild(ex);
        apply.appendChild(generateFieldValue(result, coordinates, coord, sourceVar, 2));
        then.appendChild(math);
        //y3 = vector(parfrom_i_f, int(pari - (vector(pard_i_f, pari, parj, park) + 1 * sign_i_ext)), int(parj - disp_j_i), int(park - disp_k_i));
        math = AGDMUtils.createElement(result, mtUri, "math");
        apply = AGDMUtils.createElement(result, mtUri, "apply");
        eq = AGDMUtils.createElement(result, mtUri, "eq");
        ex = AGDMUtils.createElement(result, mtUri, "ci");
        ex.setTextContent("y3");
        math.appendChild(apply);
        apply.appendChild(eq);
        apply.appendChild(ex);
        apply.appendChild(generateFieldValue(result, coordinates, coord, sourceVar, 1));
        then.appendChild(math);
        //y4 = vector(parfrom_i_f, int(pari - (vector(pard_i_f, pari, parj, park))), int(parj - disp_j_i), int(park - disp_k_i));
        math = AGDMUtils.createElement(result, mtUri, "math");
        apply = AGDMUtils.createElement(result, mtUri, "apply");
        eq = AGDMUtils.createElement(result, mtUri, "eq");
        Element ey = AGDMUtils.createElement(result, mtUri, "ci");
        ey.setTextContent("y4");
        math.appendChild(apply);
        apply.appendChild(eq);
        apply.appendChild(ey);
        apply.appendChild(generateFieldValue(result, coordinates, coord, sourceVar, 0));
        then.appendChild(math);
        //vector(parto_i_f, pari, parj, park) = POLINT_MACRO_QUADRATIC_1(y1, y2, y3, vector(pard_i_f, pari, parj, park), sign_i_ext);
        math = AGDMUtils.createElement(result, mtUri, "math");
        apply = AGDMUtils.createElement(result, mtUri, "apply");
        eq = AGDMUtils.createElement(result, mtUri, "eq");
        math.appendChild(apply);
        apply.appendChild(eq);
        apply.appendChild(targetVar.cloneNode(true));
        Element functionCall = AGDMUtils.createElement(result, smlUri, "functionCall");
        Element functionName = AGDMUtils.createElement(result, mtUri, "ci");
        functionName.setTextContent("POLINT_MACRO_CUBIC_1");
        Element paramy1 = AGDMUtils.createElement(result, mtUri, "ci");
        paramy1.setTextContent("y1");
        Element paramy2 = AGDMUtils.createElement(result, mtUri, "ci");
        paramy2.setTextContent("y2");
        Element paramy3 = AGDMUtils.createElement(result, mtUri, "ci");
        paramy3.setTextContent("y3");
        Element paramy4 = AGDMUtils.createElement(result, mtUri, "ci");
        paramy4.setTextContent("y4");
        Element paramDist = AGDMUtils.createElement(result, mtUri, "apply");
        Element dist = AGDMUtils.createElement(result, mtUri, "ci");
        dist.setTextContent("d_" + coord + "_f");
        paramDist.appendChild(dist);
        paramDist.appendChild(AGDMUtils.currentCell(result, coordinates));
        Element paramSign = AGDMUtils.createElement(result, mtUri, "ci");
        paramSign.setTextContent("sign_" + coord + "_ext");
        functionCall.appendChild(functionName);
        functionCall.appendChild(paramy1);
        functionCall.appendChild(paramy2);
        functionCall.appendChild(paramy3);
        functionCall.appendChild(paramy4);
        functionCall.appendChild(paramDist);
        functionCall.appendChild(paramSign);
        apply.appendChild(functionCall);
        then.appendChild(math);
        
        //Less than 0
        ifElem = AGDMUtils.createElement(result, smlUri, "if");
        condition = AGDMUtils.createElement(result, mtUri, "math");
        condition.appendChild(axisExtrapolationCondition(result, coord, coordinates, null, "lt"));
        ifElem.appendChild(condition);
        then = AGDMUtils.createElement(result, smlUri, "then");
        ifElem.appendChild(then);
        res.appendChild(ifElem);
        //y4 = vector(parfrom_i_f, int(pari - (vector(pard_i_f, pari, parj, park) + 3 * sign_i_ext)), int(parj - disp_j_i), int(park - disp_k_i));
        math = AGDMUtils.createElement(result, mtUri, "math");
        apply = AGDMUtils.createElement(result, mtUri, "apply");
        eq = AGDMUtils.createElement(result, mtUri, "eq");
        ex = AGDMUtils.createElement(result, mtUri, "ci");
        ex.setTextContent("y4");
        math.appendChild(apply);
        apply.appendChild(eq);
        apply.appendChild(ex);
        apply.appendChild(generateFieldValue(result, coordinates, coord, sourceVar, 3));
        then.appendChild(math);
        //y3 = vector(parfrom_i_f, int(pari - (vector(pard_i_f, pari, parj, park) + 2 * sign_i_ext)), int(parj - disp_j_i), int(park - disp_k_i));
        math = AGDMUtils.createElement(result, mtUri, "math");
        apply = AGDMUtils.createElement(result, mtUri, "apply");
        eq = AGDMUtils.createElement(result, mtUri, "eq");
        ex = AGDMUtils.createElement(result, mtUri, "ci");
        ex.setTextContent("y3");
        math.appendChild(apply);
        apply.appendChild(eq);
        apply.appendChild(ex);
        apply.appendChild(generateFieldValue(result, coordinates, coord, sourceVar, 2));
        then.appendChild(math);
        //y2 = vector(parfrom_i_f, int(pari - (vector(pard_i_f, pari, parj, park) + 1 * sign_i_ext)), int(parj - disp_j_i), int(park - disp_k_i));
        math = AGDMUtils.createElement(result, mtUri, "math");
        apply = AGDMUtils.createElement(result, mtUri, "apply");
        eq = AGDMUtils.createElement(result, mtUri, "eq");
        ex = AGDMUtils.createElement(result, mtUri, "ci");
        ex.setTextContent("y2");
        math.appendChild(apply);
        apply.appendChild(eq);
        apply.appendChild(ex);
        apply.appendChild(generateFieldValue(result, coordinates, coord, sourceVar, 1));
        then.appendChild(math);
        //y1 = vector(parfrom_i_f, int(pari - (vector(pard_i_f, pari, parj, park))), int(parj - disp_j_i), int(park - disp_k_i));
        math = AGDMUtils.createElement(result, mtUri, "math");
        apply = AGDMUtils.createElement(result, mtUri, "apply");
        eq = AGDMUtils.createElement(result, mtUri, "eq");
        ey = AGDMUtils.createElement(result, mtUri, "ci");
        ey.setTextContent("y1");
        math.appendChild(apply);
        apply.appendChild(eq);
        apply.appendChild(ey);
        apply.appendChild(generateFieldValue(result, coordinates, coord, sourceVar, 0));
        then.appendChild(math);
        //vector(parto_i_f, pari, parj, park) = POLINT_MACRO_QUADRATIC_1(y1, y2, y3, vector(pard_i_f, pari, parj, park), sign_i_ext);
        math = AGDMUtils.createElement(result, mtUri, "math");
        apply = AGDMUtils.createElement(result, mtUri, "apply");
        eq = AGDMUtils.createElement(result, mtUri, "eq");
        math.appendChild(apply);
        apply.appendChild(eq);
        apply.appendChild(targetVar.cloneNode(true));
        functionCall = AGDMUtils.createElement(result, smlUri, "functionCall");
        functionName = AGDMUtils.createElement(result, mtUri, "ci");
        functionName.setTextContent("POLINT_MACRO_CUBIC_2");
        paramy1 = AGDMUtils.createElement(result, mtUri, "ci");
        paramy1.setTextContent("y1");
        paramy2 = AGDMUtils.createElement(result, mtUri, "ci");
        paramy2.setTextContent("y2");
        paramy3 = AGDMUtils.createElement(result, mtUri, "ci");
        paramy3.setTextContent("y3");
        paramy4 = AGDMUtils.createElement(result, mtUri, "ci");
        paramy4.setTextContent("y4");
        paramDist = AGDMUtils.createElement(result, mtUri, "apply");
        dist = AGDMUtils.createElement(result, mtUri, "ci");
        dist.setTextContent("d_" + coord + "_f");
        paramDist.appendChild(dist);
        paramDist.appendChild(AGDMUtils.currentCell(result, coordinates));
        paramSign = AGDMUtils.createElement(result, mtUri, "ci");
        paramSign.setTextContent("sign_" + coord + "_ext");
        functionCall.appendChild(functionName);
        functionCall.appendChild(paramy1);
        functionCall.appendChild(paramy2);
        functionCall.appendChild(paramy3);
        functionCall.appendChild(paramy4);
        functionCall.appendChild(paramDist);
        functionCall.appendChild(paramSign);
        apply.appendChild(functionCall);
        then.appendChild(math);
        return res;
    }
    
    /**
     * Deploys the coordinates for an extrapolation field.
     * @param problem       The problem
     * @param spatialCoords The spatial coordinates
     * @param coord         The coordinate
     * @param index         The variable of the index to use
     * @param displaced     If the coordinates must be displaced from the extrapolation
     * @param field         If there is a field, the distance variables used are for this field
     * @return              The coordinates
     */
    private static DocumentFragment exCoordDeploy(Document problem, ArrayList<String> spatialCoords, String coord, Node index,
            boolean displaced, String field) {
        String variable = "f";
        if (field != null) {
            variable = field;
        }
        DocumentFragment deploy = problem.createDocumentFragment();
        for (int i = 0; i < spatialCoords.size(); i++) {
            Element apply = AGDMUtils.createElement(problem, mtUri, "apply");
            Element minus = AGDMUtils.createElement(problem, mtUri, "minus");
            Element ci = AGDMUtils.createElement(problem, mtUri, "ci");
            ci.setTextContent(spatialCoords.get(i));
            Element apply5 = AGDMUtils.createElement(problem, mtUri, "apply");
            Element dist = AGDMUtils.createElement(problem, mtUri, "ci");
            dist.setTextContent("d_" + spatialCoords.get(i) + "_" + variable);
            apply5.appendChild(dist);
            apply5.appendChild(AGDMUtils.currentCell(problem, spatialCoords));
            if (spatialCoords.get(i).equals(coord)) {
                Element apply2 = AGDMUtils.createElement(problem, mtUri, "apply");
                Element apply3 = AGDMUtils.createElement(problem, mtUri, "apply");
                Element plus = AGDMUtils.createElement(problem, mtUri, "plus");
                Element times = AGDMUtils.createElement(problem, mtUri, "times");
                Element sign = AGDMUtils.createElement(problem, mtUri, "ci");
                sign.setTextContent("sign_" + spatialCoords.get(i) + "_ext");
                if (index != null && !index.getTextContent().equals("0")) {
                    apply.appendChild(minus);
                    apply.appendChild(ci);
                    apply.appendChild(apply3);
                    apply3.appendChild(plus);
                    apply3.appendChild(apply5);
                    if (index.getTextContent().equals("1")) {
                    	apply3.appendChild(sign);
                    } 
                    else {
                        apply3.appendChild(apply2);
                        apply2.appendChild(times);
                        apply2.appendChild(index.cloneNode(true));
                        apply2.appendChild(sign);
                    }
                    deploy.appendChild(apply);
                }
                else {
                    apply.appendChild(minus);
                    apply.appendChild(ci);
                    apply.appendChild(apply5);
                    deploy.appendChild(apply);
                }
            }
            else {
                if (displaced) {
                    Element disp = AGDMUtils.createElement(problem, mtUri, "ci");
                    disp.setTextContent("disp_" + spatialCoords.get(i) + "_" + coord);
                    apply.appendChild(minus);
                    apply.appendChild(ci);
                    apply.appendChild(disp);
                    deploy.appendChild(apply);
                }
                else {
                    deploy.appendChild(ci);
                }
            }
        }
        return deploy;
    }
    
    /**
     * Generate the Regression Summatory.
     * @param doc               The current document
     * @param coordinates       The spatial coordinates
     * @param coord             The current coordinate
     * @param var               The variable template to use
     * @param order             The order of the summatory
     * @return                  The mathml
     * @throws AGDMException    AGDM00X  External error
     */
    private static Element generateFieldValue(Document doc, ArrayList<String> coordinates, String coord, Node var, int order) throws AGDMException {
        Element orderElem = AGDMUtils.createElement(doc, mtUri, "cn");
        orderElem.setTextContent(String.valueOf(order));
        Element y = AGDMUtils.createElement(doc, mtUri, "apply");
        y.appendChild(var.getFirstChild().cloneNode(true));
        y.appendChild(exCoordDeploy(doc, coordinates, coord, orderElem.cloneNode(true), true, null));
        
        return y;
    }
   
    /**
     * Calculate the extrapolation variable from the axis-extrapolated ones.
     * @param doc                   The base document
     * @param problem               The problem
     * @param pi                    The process info
     * @param timeSlice             The time slice
     * @return                      The code
     * @throws AGDMException        AGDM00X  External error
     */
    public static DocumentFragment unifyAxisExtrapolationsBalanceLaw(Document doc, Document problem, ProcessInfo pi, Element timeSlice) throws AGDMException {
        DocumentFragment result = problem.createDocumentFragment();
        ArrayList<String> spatialCoords = AGDMUtils.getBaseDiscSpatialCoordinate(doc);
        boolean isShared = ((Element) timeSlice.getFirstChild()).getLocalName().equals("sharedVariable");
        
        DocumentFragment index = problem.createDocumentFragment();
        DocumentFragment indexDistance = problem.createDocumentFragment();
        for (int i = 0; i < spatialCoords.size(); i++) {
            String coord = spatialCoords.get(i);
            Element coordinate = AGDMUtils.createElement(problem, mtUri, "ci");
            coordinate.setTextContent(coord);
            index.appendChild(coordinate);
        }
        
        Element d2 = AGDMUtils.createElement(problem, mtUri, "apply");
        Element plus = AGDMUtils.createElement(problem, mtUri, "plus");
        d2.appendChild(plus);
        Element dDelta2 = AGDMUtils.createElement(problem, mtUri, "apply");
        plus = AGDMUtils.createElement(problem, mtUri, "plus");
        dDelta2.appendChild(plus);
        for (int i = 0; i < spatialCoords.size(); i++) {
            //d2
            String coord = spatialCoords.get(i);
            Element powerApply = AGDMUtils.createElement(problem, mtUri, "apply");
            Element power = AGDMUtils.createElement(problem, mtUri, "power");
            Element exp = AGDMUtils.createElement(problem, mtUri, "cn");
            exp.setTextContent("2");
            Element d = AGDMUtils.createElement(problem, mtUri, "apply");
            Element name = AGDMUtils.createElement(problem, mtUri, "ci");
            name.setTextContent("d_" + coord + "_" + pi.getField());
            d.appendChild(name);
            d.appendChild(index.cloneNode(true));
            powerApply.appendChild(power);
            powerApply.appendChild(d);
            powerApply.appendChild(exp);
            d2.appendChild(powerApply);
            
            //dDelta2
            powerApply = AGDMUtils.createElement(problem, mtUri, "apply");
            power = AGDMUtils.createElement(problem, mtUri, "power");
            exp = AGDMUtils.createElement(problem, mtUri, "cn");
            exp.setTextContent("2");
            Element timesApply = AGDMUtils.createElement(problem, mtUri, "apply");
            Element times = AGDMUtils.createElement(problem, mtUri, "times");
            Element dx = AGDMUtils.createElement(problem, mtUri, "ci");
            dx.setTextContent("\u0394" + pi.getCoordinateRelation().getContCoords().get(i));
            timesApply.appendChild(times);
            timesApply.appendChild(d.cloneNode(true));
            timesApply.appendChild(dx);
            powerApply.appendChild(power);
            powerApply.appendChild(timesApply);
            powerApply.appendChild(exp);
            dDelta2.appendChild(powerApply);
            
            Element apply = AGDMUtils.createElement(problem, mtUri, "apply");
            Element minus = AGDMUtils.createElement(problem, mtUri, "minus");
            Element coordinate = AGDMUtils.createElement(problem, mtUri, "ci");
            coordinate.setTextContent(coord);
            apply.appendChild(minus);
            apply.appendChild(coordinate);
            apply.appendChild(d.cloneNode(true));
            indexDistance.appendChild(apply);
        }
        
        //Mod distance
        Element math = AGDMUtils.createElement(problem, mtUri, "math");
        Element apply = AGDMUtils.createElement(problem, mtUri, "apply");
        Element eq = AGDMUtils.createElement(problem, mtUri, "eq");
        Element mod_d = AGDMUtils.createElement(problem, mtUri, "ci");
        mod_d.setTextContent("mod_d");
        Element rootApply = AGDMUtils.createElement(problem, mtUri, "apply");
        Element root = AGDMUtils.createElement(problem, mtUri, "root");
        Element degree = AGDMUtils.createElement(problem, mtUri, "degree");
        Element degreeValue = AGDMUtils.createElement(problem, mtUri, "cn");
        degreeValue.setTextContent("2");
        math.appendChild(apply);
        apply.appendChild(eq);
        apply.appendChild(mod_d);
        apply.appendChild(rootApply);
        rootApply.appendChild(root);
        rootApply.appendChild(degree);
        degree.appendChild(degreeValue);
        rootApply.appendChild(d2);
        result.appendChild(math);
        
        //Term_x
        Element termSummation = AGDMUtils.createElement(problem, mtUri, "apply");
        plus = AGDMUtils.createElement(problem, mtUri, "plus");
        termSummation.appendChild(plus);
        for (int i = 0; i < spatialCoords.size(); i++) {
            //Term initialization
            String coord = spatialCoords.get(i);
            String contCoord = pi.getCoordinateRelation().getContCoords().get(i); 
            math = AGDMUtils.createElement(problem, mtUri, "math");
            apply = AGDMUtils.createElement(problem, mtUri, "apply");
            eq = AGDMUtils.createElement(problem, mtUri, "eq");
            Element term = AGDMUtils.createElement(problem, mtUri, "ci");
            term.setTextContent("term_" + coord);
            Element zero = AGDMUtils.createElement(problem, mtUri, "cn");
            zero.setTextContent("0");
            math.appendChild(apply);
            apply.appendChild(eq);
            apply.appendChild(term);
            apply.appendChild(zero);
            result.appendChild(math);
            
            termSummation.appendChild(term.cloneNode(true));
            
            //Term condition
            Element ifTerm = AGDMUtils.createElement(problem, smlUri, "if");
            Element mathCond = AGDMUtils.createElement(problem, mtUri, "math");
            mathCond.appendChild(axisExtrapolationCondition(problem, coord, spatialCoords, pi.getField(), "neq"));
            ifTerm.appendChild(mathCond);
            Element then = AGDMUtils.createElement(problem, smlUri, "then");
            ifTerm.appendChild(then);
            //Then 
            math = AGDMUtils.createElement(problem, mtUri, "math");
            apply = AGDMUtils.createElement(problem, mtUri, "apply");
            eq = AGDMUtils.createElement(problem, mtUri, "eq");
            term = AGDMUtils.createElement(problem, mtUri, "ci");
            term.setTextContent("term_" + coord);
            Element rhsApply = AGDMUtils.createElement(problem, mtUri, "apply");
            Element divide = AGDMUtils.createElement(problem, mtUri, "divide");
            math.appendChild(apply);
            apply.appendChild(eq);
            apply.appendChild(term);
            apply.appendChild(rhsApply);
            rhsApply.appendChild(divide);
            Element minusApply = AGDMUtils.createElement(problem, mtUri, "apply");
            Element minus = AGDMUtils.createElement(problem, mtUri, "minus");
            minusApply.appendChild(minus);
            //Extrapolated point
            Element extrapolated = AGDMUtils.createElement(problem, mtUri, "apply");
            pi.setSpatialCoord1(contCoord);
            Element extrapolVar = AGDMUtils.getExtrapolationVariable(timeSlice.getOwnerDocument(), true, false);
            Element variable = ExecutionFlow.replaceTags(extrapolVar.getFirstChild(), pi);
            extrapolated.appendChild(problem.importNode(variable, true));
            extrapolated.appendChild(index.cloneNode(true));
            minusApply.appendChild(extrapolated);
            //Base point
            Element base = AGDMUtils.createElement(problem, mtUri, "apply");
            base.appendChild(problem.importNode(variable.cloneNode(true), true));
            base.appendChild(exCoordDeploy(problem, spatialCoords, coord, null, true, pi.getField()));
            minusApply.appendChild(base);
            rhsApply.appendChild(minusApply);
            Element dx = AGDMUtils.createElement(problem, mtUri, "ci");
            dx.setTextContent("\u0394" + contCoord);
            rhsApply.appendChild(dx);
            then.appendChild(math);
            result.appendChild(ifTerm);
        }
        //Extrapolation field unification
        math = AGDMUtils.createElement(problem, mtUri, "math");
        apply = AGDMUtils.createElement(problem, mtUri, "apply");
        eq = AGDMUtils.createElement(problem, mtUri, "eq");
        apply.appendChild(eq);
        Element leftSide = AGDMUtils.createElement(problem, mtUri, "apply");
        Element tsVar = (Element) timeSlice.getFirstChild().getFirstChild().cloneNode(true);
        if (isShared) {
            tsVar = (Element) tsVar.getFirstChild();
        }
        leftSide.appendChild(problem.importNode(ExecutionFlow.replaceTags(tsVar, pi), true));
        leftSide.appendChild(AGDMUtils.currentCell(problem, pi));
        if (isShared) {
            Element shared = AGDMUtils.createElement(problem, smlUri, "sharedVariable");
            shared.appendChild(leftSide.cloneNode(true));
            apply.appendChild(shared);
        }
        else {
            apply.appendChild(leftSide.cloneNode(true));
        }        
        Element applyRHS = AGDMUtils.createElement(problem, mtUri, "apply");
        plus = AGDMUtils.createElement(problem, mtUri, "plus");
        //Base point
        Element base = AGDMUtils.createElement(problem, mtUri, "apply");
        pi.setSpatialCoord1(pi.getCoordinateRelation().getContCoords().get(0));
        Element extrapolVar = AGDMUtils.getExtrapolationVariable(timeSlice.getOwnerDocument(), true, false);
        Element extrapol = ExecutionFlow.replaceTags(extrapolVar.getFirstChild(), pi);
        base.appendChild(problem.importNode(extrapol, true));
        base.appendChild(indexDistance.cloneNode(true));
        Element termApply = AGDMUtils.createElement(problem, mtUri, "apply");
        Element times = AGDMUtils.createElement(problem, mtUri, "times");
        Element distanceModules = AGDMUtils.createElement(problem, mtUri, "apply");
        Element divide = AGDMUtils.createElement(problem, mtUri, "divide");
        rootApply = AGDMUtils.createElement(problem, mtUri, "apply");
        root = AGDMUtils.createElement(problem, mtUri, "root");
        degree = AGDMUtils.createElement(problem, mtUri, "degree");
        degreeValue = AGDMUtils.createElement(problem, mtUri, "cn");
        degreeValue.setTextContent("2");
        rootApply.appendChild(root);
        rootApply.appendChild(degree);
        degree.appendChild(degreeValue);
        rootApply.appendChild(dDelta2);
        math.appendChild(apply);
        apply.appendChild(applyRHS);
        applyRHS.appendChild(plus);
        applyRHS.appendChild(base);
        applyRHS.appendChild(termApply);
        termApply.appendChild(times);
        termApply.appendChild(termSummation);
        termApply.appendChild(distanceModules);
        distanceModules.appendChild(divide);
        distanceModules.appendChild(rootApply);
        distanceModules.appendChild(mod_d.cloneNode(true));
        result.appendChild(math);

        return result;
    }
    
    /**
     * Calculate the extrapolation variable from the axis-extrapolated ones.
     * @param doc                   The base document
     * @param problem               The problem
     * @param pi                    The process info
     * @param timeSlice             The time slice
     * @param hasFluxes				If the equations has fluxes
     * @return                      The code
     * @throws AGDMException        AGDM00X  External error
     */
    public static DocumentFragment unifyAxisExtrapolationsPDE(Document doc, Document problem, ProcessInfo pi, Element timeSlice, boolean hasFluxes) throws AGDMException {
        DocumentFragment result = problem.createDocumentFragment();
        ArrayList<String> spatialCoords = AGDMUtils.getBaseDiscSpatialCoordinate(doc);
        boolean isShared = ((Element) timeSlice.getFirstChild()).getLocalName().equals("sharedVariable");
        
        DocumentFragment index = problem.createDocumentFragment();
        DocumentFragment indexDistance = problem.createDocumentFragment();
        for (int i = 0; i < spatialCoords.size(); i++) {
            String coord = spatialCoords.get(i);
            Element coordinate = AGDMUtils.createElement(problem, mtUri, "ci");
            coordinate.setTextContent(coord);
            index.appendChild(coordinate);
        }
        
        Element d2 = AGDMUtils.createElement(problem, mtUri, "apply");
        Element plus = AGDMUtils.createElement(problem, mtUri, "plus");
        d2.appendChild(plus);
        Element dDelta2 = AGDMUtils.createElement(problem, mtUri, "apply");
        plus = AGDMUtils.createElement(problem, mtUri, "plus");
        dDelta2.appendChild(plus);
        for (int i = 0; i < spatialCoords.size(); i++) {
            //d2
            String coord = spatialCoords.get(i);
            Element powerApply = AGDMUtils.createElement(problem, mtUri, "apply");
            Element power = AGDMUtils.createElement(problem, mtUri, "power");
            Element exp = AGDMUtils.createElement(problem, mtUri, "cn");
            exp.setTextContent("2");
            Element d = AGDMUtils.createElement(problem, mtUri, "apply");
            Element name = AGDMUtils.createElement(problem, mtUri, "ci");
            name.setTextContent("d_" + coord + "_" + pi.getField());
            d.appendChild(name);
            d.appendChild(index.cloneNode(true));
            powerApply.appendChild(power);
            powerApply.appendChild(d);
            powerApply.appendChild(exp);
            d2.appendChild(powerApply);
            
            //dDelta2
            powerApply = AGDMUtils.createElement(problem, mtUri, "apply");
            power = AGDMUtils.createElement(problem, mtUri, "power");
            exp = AGDMUtils.createElement(problem, mtUri, "cn");
            exp.setTextContent("2");
            Element timesApply = AGDMUtils.createElement(problem, mtUri, "apply");
            Element times = AGDMUtils.createElement(problem, mtUri, "times");
            Element dx = AGDMUtils.createElement(problem, mtUri, "ci");
            dx.setTextContent("\u0394" + pi.getCoordinateRelation().getContCoords().get(i));
            timesApply.appendChild(times);
            timesApply.appendChild(d.cloneNode(true));
            timesApply.appendChild(dx);
            powerApply.appendChild(power);
            powerApply.appendChild(timesApply);
            powerApply.appendChild(exp);
            dDelta2.appendChild(powerApply);
            
            Element apply = AGDMUtils.createElement(problem, mtUri, "apply");
            Element minus = AGDMUtils.createElement(problem, mtUri, "minus");
            Element coordinate = AGDMUtils.createElement(problem, mtUri, "ci");
            coordinate.setTextContent(coord);
            apply.appendChild(minus);
            apply.appendChild(coordinate);
            apply.appendChild(d.cloneNode(true));
            indexDistance.appendChild(apply);
        }
        
        //Mod distance
        Element math = AGDMUtils.createElement(problem, mtUri, "math");
        Element apply = AGDMUtils.createElement(problem, mtUri, "apply");
        Element eq = AGDMUtils.createElement(problem, mtUri, "eq");
        Element mod_d = AGDMUtils.createElement(problem, mtUri, "ci");
        mod_d.setTextContent("mod_d");
        Element rootApply = AGDMUtils.createElement(problem, mtUri, "apply");
        Element root = AGDMUtils.createElement(problem, mtUri, "root");
        Element degree = AGDMUtils.createElement(problem, mtUri, "degree");
        Element degreeValue = AGDMUtils.createElement(problem, mtUri, "cn");
        degreeValue.setTextContent("2");
        math.appendChild(apply);
        apply.appendChild(eq);
        apply.appendChild(mod_d);
        apply.appendChild(rootApply);
        rootApply.appendChild(root);
        rootApply.appendChild(degree);
        degree.appendChild(degreeValue);
        rootApply.appendChild(d2);
        result.appendChild(math);
        
        //Term_x
        Element termSummation = AGDMUtils.createElement(problem, mtUri, "apply");
        plus = AGDMUtils.createElement(problem, mtUri, "plus");
        termSummation.appendChild(plus);
        for (int i = 0; i < spatialCoords.size(); i++) {
            //Term initialization
            String coord = spatialCoords.get(i);
            String contCoord = pi.getCoordinateRelation().getContCoords().get(i); 
            math = AGDMUtils.createElement(problem, mtUri, "math");
            apply = AGDMUtils.createElement(problem, mtUri, "apply");
            eq = AGDMUtils.createElement(problem, mtUri, "eq");
            Element term = AGDMUtils.createElement(problem, mtUri, "ci");
            term.setTextContent("term_" + coord);
            Element zero = AGDMUtils.createElement(problem, mtUri, "cn");
            zero.setTextContent("0");
            math.appendChild(apply);
            apply.appendChild(eq);
            apply.appendChild(term);
            apply.appendChild(zero);
            result.appendChild(math);
            
            termSummation.appendChild(term.cloneNode(true));
            
            //Term condition
            Element ifTerm = AGDMUtils.createElement(problem, smlUri, "if");
            Element mathCond = AGDMUtils.createElement(problem, mtUri, "math");
            mathCond.appendChild(axisExtrapolationCondition(problem, coord, spatialCoords, pi.getField(), "neq"));
            ifTerm.appendChild(mathCond);
            Element then = AGDMUtils.createElement(problem, smlUri, "then");
            ifTerm.appendChild(then);
            //Then 
            math = AGDMUtils.createElement(problem, mtUri, "math");
            apply = AGDMUtils.createElement(problem, mtUri, "apply");
            eq = AGDMUtils.createElement(problem, mtUri, "eq");
            term = AGDMUtils.createElement(problem, mtUri, "ci");
            term.setTextContent("term_" + coord);
            Element rhsApply = AGDMUtils.createElement(problem, mtUri, "apply");
            Element divide = AGDMUtils.createElement(problem, mtUri, "divide");
            math.appendChild(apply);
            apply.appendChild(eq);
            apply.appendChild(term);
            apply.appendChild(rhsApply);
            rhsApply.appendChild(divide);
            Element minusApply = AGDMUtils.createElement(problem, mtUri, "apply");
            Element minus = AGDMUtils.createElement(problem, mtUri, "minus");
            minusApply.appendChild(minus);
            //Extrapolated point
            Element extrapolated = null;
            pi.setSpatialCoord1(contCoord);
            if (hasFluxes && ProcessInfo.getNumberOfRegions() > 1) {
            	extrapolated = AGDMUtils.createElement(problem, mtUri, "apply");
                Element extrapolVar = AGDMUtils.getExtrapolationVariable(timeSlice.getOwnerDocument(), true, false);
                Element variable = ExecutionFlow.replaceTags(extrapolVar.getFirstChild(), pi);
                extrapolated.appendChild(problem.importNode(variable, true));
                extrapolated.appendChild(index.cloneNode(true));
            }
            else {
            	if (isShared) {
                	extrapolated = AGDMUtils.createElement(problem, smlUri, "sharedVariable");
                	Element sharedApply = AGDMUtils.createElement(problem, mtUri, "apply");
                	Element variable = (Element) problem.importNode(ExecutionFlow.replaceTags(timeSlice.getFirstChild().cloneNode(true), pi), true).getFirstChild();
                	sharedApply.appendChild(variable.getFirstChild().cloneNode(true));
                	sharedApply.appendChild(index.cloneNode(true));
                	extrapolated.appendChild(sharedApply);
                }
                else {
                	extrapolated = AGDMUtils.createElement(problem, mtUri, "apply");
                	Element variable = (Element) problem.importNode(ExecutionFlow.replaceTags(timeSlice.cloneNode(true), pi), true).getFirstChild();
                    extrapolated.appendChild(variable.getFirstChild().cloneNode(true));
                    extrapolated.appendChild(index.cloneNode(true));
                }
            }
            minusApply.appendChild(extrapolated);
            //Base point
            Element base = null;
            if (hasFluxes && ProcessInfo.getNumberOfRegions() > 1) {
                base = AGDMUtils.createElement(problem, mtUri, "apply");
                Element extrapolVar = AGDMUtils.getExtrapolationVariable(timeSlice.getOwnerDocument(), true, false);
                Element variable = ExecutionFlow.replaceTags(extrapolVar.getFirstChild(), pi);
                base.appendChild(problem.importNode(variable, true));
                base.appendChild(exCoordDeploy(problem, spatialCoords, coord, null, true, pi.getField()));
            }
            else {
	            if (isShared) {
	            	base = AGDMUtils.createElement(problem, smlUri, "sharedVariable");
	            	Element sharedApply = AGDMUtils.createElement(problem, mtUri, "apply");
	            	Element variable = (Element) problem.importNode(ExecutionFlow.replaceTags(timeSlice.getFirstChild().cloneNode(true), pi), true).getFirstChild();
	            	sharedApply.appendChild(variable.getFirstChild().cloneNode(true));
	            	sharedApply.appendChild(exCoordDeploy(problem, spatialCoords, coord, null, true, pi.getField()));
	            	base.appendChild(sharedApply);
	            }
	            else {
	            	base = AGDMUtils.createElement(problem, mtUri, "apply");
	            	Element variable = (Element) problem.importNode(ExecutionFlow.replaceTags(timeSlice.cloneNode(true), pi), true).getFirstChild();
	            	base.appendChild(variable.getFirstChild().cloneNode(true));
	            	base.appendChild(exCoordDeploy(problem, spatialCoords, coord, null, true, pi.getField()));
	            }
            }
            minusApply.appendChild(base);
            rhsApply.appendChild(minusApply);
            Element dx = AGDMUtils.createElement(problem, mtUri, "ci");
            dx.setTextContent("\u0394" + contCoord);
            rhsApply.appendChild(dx);
            then.appendChild(math);
            result.appendChild(ifTerm);
        }
        //Extrapolation field unification
        math = AGDMUtils.createElement(problem, mtUri, "math");
        apply = AGDMUtils.createElement(problem, mtUri, "apply");
        eq = AGDMUtils.createElement(problem, mtUri, "eq");
        apply.appendChild(eq);
        Element leftSide = ExecutionFlow.replaceTags(AGDMUtils.createTimeSlice(problem), pi);
        apply.appendChild(problem.importNode(leftSide.cloneNode(true), true));      
        Element applyRHS = AGDMUtils.createElement(problem, mtUri, "apply");
        plus = AGDMUtils.createElement(problem, mtUri, "plus");
        //Base point
        Element base = AGDMUtils.createElement(problem, mtUri, "apply");
        pi.setSpatialCoord1(pi.getCoordinateRelation().getContCoords().get(0));
        if (hasFluxes && ProcessInfo.getNumberOfRegions() > 1) {
            Element extrapolVar = AGDMUtils.getExtrapolationVariable(timeSlice.getOwnerDocument(), true, false);
            Element extrapol = ExecutionFlow.replaceTags(extrapolVar.getFirstChild(), pi);
            base.appendChild(problem.importNode(extrapol, true));
            base.appendChild(indexDistance.cloneNode(true));
        }
        else {
	        if (isShared) {
	            base = AGDMUtils.createElement(problem, smlUri, "sharedVariable");
	            Element applyShared = AGDMUtils.createElement(problem, mtUri, "apply");
	            applyShared.appendChild(problem.importNode(leftSide.getFirstChild().cloneNode(true), true));
	            applyShared.appendChild(indexDistance.cloneNode(true));
	            base.appendChild(applyShared);
	        }
	        else {
	        	base.appendChild(problem.importNode(leftSide.getFirstChild().cloneNode(true), true));
	            base.appendChild(indexDistance.cloneNode(true));
	        }
        }
        Element termApply = AGDMUtils.createElement(problem, mtUri, "apply");
        Element times = AGDMUtils.createElement(problem, mtUri, "times");
        Element distanceModules = AGDMUtils.createElement(problem, mtUri, "apply");
        Element divide = AGDMUtils.createElement(problem, mtUri, "divide");
        rootApply = AGDMUtils.createElement(problem, mtUri, "apply");
        root = AGDMUtils.createElement(problem, mtUri, "root");
        degree = AGDMUtils.createElement(problem, mtUri, "degree");
        degreeValue = AGDMUtils.createElement(problem, mtUri, "cn");
        degreeValue.setTextContent("2");
        rootApply.appendChild(root);
        rootApply.appendChild(degree);
        degree.appendChild(degreeValue);
        rootApply.appendChild(dDelta2);
        math.appendChild(apply);
        apply.appendChild(applyRHS);
        applyRHS.appendChild(plus);
        applyRHS.appendChild(base);
        applyRHS.appendChild(termApply);
        termApply.appendChild(times);
        termApply.appendChild(termSummation);
        termApply.appendChild(distanceModules);
        distanceModules.appendChild(divide);
        distanceModules.appendChild(rootApply);
        distanceModules.appendChild(mod_d.cloneNode(true));
        result.appendChild(math);

        return result;
    }
    
    /**
     * Calculate the extrapolation variable from the axis-extrapolated ones.
     * @param problem               The problem
     * @param spatialCoords         The spatial coordinates
     * @param ci                    The coordinate information
     * @param hasFluxes				If the equations has fluxes
     * @param localAxisExtrapolation			If extrapolation is for a local axis
     * @return                      The code
     * @throws AGDMException        AGDM00X  External error
     */
    public static DocumentFragment unifyAxisExtrapolations(Document problem, ArrayList<String> spatialCoords, 
    		CoordinateInfo ci, boolean localAxisExtrapolation, boolean hasFluxes) throws AGDMException {
        DocumentFragment result = problem.createDocumentFragment();
        
        DocumentFragment index = problem.createDocumentFragment();
        DocumentFragment indexDistance = problem.createDocumentFragment();
        for (int i = 0; i < spatialCoords.size(); i++) {
            String coord = spatialCoords.get(i);
            Element coordinate = AGDMUtils.createElement(problem, mtUri, "ci");
            coordinate.setTextContent(coord);
            index.appendChild(coordinate);
        }
        
        Element d2 = AGDMUtils.createElement(problem, mtUri, "apply");
        Element plus = AGDMUtils.createElement(problem, mtUri, "plus");
        d2.appendChild(plus);
        Element dDelta2 = AGDMUtils.createElement(problem, mtUri, "apply");
        plus = AGDMUtils.createElement(problem, mtUri, "plus");
        dDelta2.appendChild(plus);
        for (int i = 0; i < spatialCoords.size(); i++) {
            //d2
            String coord = spatialCoords.get(i);
            Element powerApply = AGDMUtils.createElement(problem, mtUri, "apply");
            Element power = AGDMUtils.createElement(problem, mtUri, "power");
            Element exp = AGDMUtils.createElement(problem, mtUri, "cn");
            exp.setTextContent("2");
            Element d = AGDMUtils.createElement(problem, mtUri, "apply");
            Element name = AGDMUtils.createElement(problem, mtUri, "ci");
            name.setTextContent("d_" + coord + "_f");
            d.appendChild(name);
            d.appendChild(index.cloneNode(true));
            powerApply.appendChild(power);
            powerApply.appendChild(d);
            powerApply.appendChild(exp);
            d2.appendChild(powerApply);
            
            //dDelta2
            powerApply = AGDMUtils.createElement(problem, mtUri, "apply");
            power = AGDMUtils.createElement(problem, mtUri, "power");
            exp = AGDMUtils.createElement(problem, mtUri, "cn");
            exp.setTextContent("2");
            Element timesApply = AGDMUtils.createElement(problem, mtUri, "apply");
            Element times = AGDMUtils.createElement(problem, mtUri, "times");
            Element dx = AGDMUtils.createElement(problem, mtUri, "ci");
            dx.setTextContent("\u0394" + ci.getContCoords().get(i));
            timesApply.appendChild(times);
            timesApply.appendChild(d.cloneNode(true));
            timesApply.appendChild(dx);
            powerApply.appendChild(power);
            powerApply.appendChild(timesApply);
            powerApply.appendChild(exp);
            dDelta2.appendChild(powerApply);
            
            Element apply = AGDMUtils.createElement(problem, mtUri, "apply");
            Element minus = AGDMUtils.createElement(problem, mtUri, "minus");
            Element coordinate = AGDMUtils.createElement(problem, mtUri, "ci");
            coordinate.setTextContent(coord);
            apply.appendChild(minus);
            apply.appendChild(coordinate);
            apply.appendChild(d.cloneNode(true));
            indexDistance.appendChild(apply);
        }
        
        //Mod distance
        Element math = AGDMUtils.createElement(problem, mtUri, "math");
        Element apply = AGDMUtils.createElement(problem, mtUri, "apply");
        Element eq = AGDMUtils.createElement(problem, mtUri, "eq");
        Element mod_d = AGDMUtils.createElement(problem, mtUri, "ci");
        mod_d.setTextContent("mod_d");
        Element rootApply = AGDMUtils.createElement(problem, mtUri, "apply");
        Element root = AGDMUtils.createElement(problem, mtUri, "root");
        Element degree = AGDMUtils.createElement(problem, mtUri, "degree");
        Element degreeValue = AGDMUtils.createElement(problem, mtUri, "cn");
        degreeValue.setTextContent("2");
        math.appendChild(apply);
        apply.appendChild(eq);
        apply.appendChild(mod_d);
        apply.appendChild(rootApply);
        rootApply.appendChild(root);
        rootApply.appendChild(degree);
        degree.appendChild(degreeValue);
        rootApply.appendChild(d2);
        result.appendChild(math);
        
        //Term_x
        Element termSummation = AGDMUtils.createElement(problem, mtUri, "apply");
        plus = AGDMUtils.createElement(problem, mtUri, "plus");
        termSummation.appendChild(plus);
        for (int i = 0; i < spatialCoords.size(); i++) {
            //Term initialization
            String coord = spatialCoords.get(i);
            math = AGDMUtils.createElement(problem, mtUri, "math");
            apply = AGDMUtils.createElement(problem, mtUri, "apply");
            eq = AGDMUtils.createElement(problem, mtUri, "eq");
            Element term = AGDMUtils.createElement(problem, mtUri, "ci");
            term.setTextContent("term_" + coord);
            Element zero = AGDMUtils.createElement(problem, mtUri, "cn");
            zero.setTextContent("0");
            math.appendChild(apply);
            apply.appendChild(eq);
            apply.appendChild(term);
            apply.appendChild(zero);
            result.appendChild(math);
            
            termSummation.appendChild(term.cloneNode(true));
            
            //Term condition
            Element ifTerm = AGDMUtils.createElement(problem, smlUri, "if");
            Element mathCond = AGDMUtils.createElement(problem, mtUri, "math");
            mathCond.appendChild(axisExtrapolationCondition(problem, coord, spatialCoords, null, "neq"));
            ifTerm.appendChild(mathCond);
            Element then = AGDMUtils.createElement(problem, smlUri, "then");
            ifTerm.appendChild(then);
            //Then 
            math = AGDMUtils.createElement(problem, mtUri, "math");
            apply = AGDMUtils.createElement(problem, mtUri, "apply");
            eq = AGDMUtils.createElement(problem, mtUri, "eq");
            term = AGDMUtils.createElement(problem, mtUri, "ci");
            term.setTextContent("term_" + coord);
            Element rhsApply = AGDMUtils.createElement(problem, mtUri, "apply");
            Element divide = AGDMUtils.createElement(problem, mtUri, "divide");
            math.appendChild(apply);
            apply.appendChild(eq);
            apply.appendChild(term);
            apply.appendChild(rhsApply);
            rhsApply.appendChild(divide);
            Element minusApply = AGDMUtils.createElement(problem, mtUri, "apply");
            Element minus = AGDMUtils.createElement(problem, mtUri, "minus");
            //Extrapolated point
            Element extrapolated;
            if (hasFluxes && ProcessInfo.getNumberOfRegions() > 1) {
	            extrapolated = AGDMUtils.createElement(problem, mtUri, "apply");
	            Element name = AGDMUtils.createElement(problem, mtUri, "ci");
	            name.setTextContent("to_" + coord + "_f");
	            extrapolated.appendChild(name);
	            extrapolated.appendChild(index.cloneNode(true));
            }
            else {
            	extrapolated = AGDMUtils.createElement(problem, mtUri, "ci");
            	extrapolated.setTextContent("to_" + coord + "_f");
            }
            //Base point
            Element base = AGDMUtils.createElement(problem, mtUri, "apply");
            Element baseName = AGDMUtils.createElement(problem, mtUri, "ci");
            if (localAxisExtrapolation) {
            	baseName.setTextContent("to_f");
            }
            else {
            	baseName.setTextContent("from_" + coord + "_f");
            }
            base.appendChild(baseName);
            base.appendChild(exCoordDeploy(problem, spatialCoords, coord, null, true, null));
            rhsApply.appendChild(minusApply);
            minusApply.appendChild(minus);
            minusApply.appendChild(extrapolated);
            minusApply.appendChild(base);
            Element dx = AGDMUtils.createElement(problem, mtUri, "ci");
            dx.setTextContent("\u0394" + ci.getContCoords().get(i));
            rhsApply.appendChild(dx);
            then.appendChild(math);
            result.appendChild(ifTerm);
        }
        //Extrapolation field unification
        math = AGDMUtils.createElement(problem, mtUri, "math");
        apply = AGDMUtils.createElement(problem, mtUri, "apply");
        eq = AGDMUtils.createElement(problem, mtUri, "eq");
        Element field = AGDMUtils.createElement(problem, mtUri, "apply");
        Element name = AGDMUtils.createElement(problem, mtUri, "ci");
        name.setTextContent("to_f");
        field.appendChild(name);
        field.appendChild(index.cloneNode(true));
        Element applyRHS = AGDMUtils.createElement(problem, mtUri, "apply");
        plus = AGDMUtils.createElement(problem, mtUri, "plus");
        //Base point
        Element base = AGDMUtils.createElement(problem, mtUri, "apply");
        Element baseName = AGDMUtils.createElement(problem, mtUri, "ci");
        baseName.setTextContent("to_f");
        base.appendChild(baseName);
        base.appendChild(indexDistance.cloneNode(true));
        Element termApply = AGDMUtils.createElement(problem, mtUri, "apply");
        Element times = AGDMUtils.createElement(problem, mtUri, "times");
        Element distanceModules = AGDMUtils.createElement(problem, mtUri, "apply");
        Element divide = AGDMUtils.createElement(problem, mtUri, "divide");
        rootApply = AGDMUtils.createElement(problem, mtUri, "apply");
        root = AGDMUtils.createElement(problem, mtUri, "root");
        degree = AGDMUtils.createElement(problem, mtUri, "degree");
        degreeValue = AGDMUtils.createElement(problem, mtUri, "cn");
        degreeValue.setTextContent("2");
        rootApply.appendChild(root);
        rootApply.appendChild(degree);
        degree.appendChild(degreeValue);
        rootApply.appendChild(dDelta2);
        math.appendChild(apply);
        apply.appendChild(eq);
        apply.appendChild(field);
        apply.appendChild(applyRHS);
        applyRHS.appendChild(plus);
        applyRHS.appendChild(base);
        applyRHS.appendChild(termApply);
        termApply.appendChild(times);
        termApply.appendChild(termSummation);
        termApply.appendChild(distanceModules);
        distanceModules.appendChild(divide);
        distanceModules.appendChild(rootApply);
        distanceModules.appendChild(mod_d.cloneNode(true));
        result.appendChild(math);
            
        return result;
    }
    
    /**
     * Create the instructions to have the signs of the distance variables.
     * @param problem       The problem
     * @param spatialCoords The spatial coordinates
     * @return              The instructions
     */
    private static DocumentFragment createSignVars(Document problem, ArrayList<String> spatialCoords) {
        DocumentFragment result = problem.createDocumentFragment();
        for (int i = 0; i < spatialCoords.size(); i++) {
            String coord = spatialCoords.get(i);
            Element math = AGDMUtils.createElement(problem, mtUri, "math");
            Element apply = AGDMUtils.createElement(problem, mtUri, "apply");
            Element eq = AGDMUtils.createElement(problem, mtUri, "eq");
            Element ci = AGDMUtils.createElement(problem, mtUri, "ci");
            ci.setTextContent("sign_" + coord + "_ext");
            Element apply2 = AGDMUtils.createElement(problem, mtUri, "apply");
            Element sign = AGDMUtils.createElement(problem, mtUri, "ci");
            sign.setTextContent("SIGN");
            Element applyDist = AGDMUtils.createElement(problem, mtUri, "apply");
            Element dist = AGDMUtils.createElement(problem, mtUri, "ci");
            dist.setTextContent("d_" + coord + "_f");
            applyDist.appendChild(dist);
            applyDist.appendChild(AGDMUtils.currentCell(problem, spatialCoords));
            math.appendChild(apply);
            apply.appendChild(eq);
            apply.appendChild(ci);
            apply.appendChild(apply2);
            apply2.appendChild(sign);
            apply2.appendChild(applyDist);
            result.appendChild(math);
        }
        return result;
    }
    
    /**
     * Create the instructions to check displacement.
     * @param problem           The problem
     * @param spatialCoords     The spatial coordinates
     * @param field             If a field is given, use distance of this field
     * @param region            The region reference if provided
     * @return                  The instructions
     * @throws AGDMException    AGDM00X  External error
     */
    private static DocumentFragment createDisplacement(Document problem, ArrayList<String> spatialCoords, String field, String region) 
            throws AGDMException {
        String variable = "f";
        if (field != null) {
            variable = field;
        }
        DocumentFragment result = problem.createDocumentFragment();
        for (int i = 0; i < spatialCoords.size(); i++) {
            String coord = spatialCoords.get(i);
            for (int j = 0; j < spatialCoords.size(); j++) {
                if (i != j) {
                    String coord2 = spatialCoords.get(j);
                    Element dispEq = AGDMUtils.createElement(problem, mtUri, "math");
                    Element apply = AGDMUtils.createElement(problem, mtUri, "apply");
                    Element eq = AGDMUtils.createElement(problem, mtUri, "eq");
                    Element ci = AGDMUtils.createElement(problem, mtUri, "ci");
                    ci.setTextContent("disp_" + coord + "_" + coord2);
                    Element zero = AGDMUtils.createElement(problem, mtUri, "cn");
                    zero.setTextContent("0");
                    dispEq.appendChild(apply);
                    apply.appendChild(eq);
                    apply.appendChild(ci);
                    apply.appendChild(zero);
                    result.appendChild(dispEq);
                }
            }
        }
        for (int i = 0; i < spatialCoords.size(); i++) {
            String coord = spatialCoords.get(i);
            Element condition = AGDMUtils.createElement(problem, smlUri, "if");
            Element math = AGDMUtils.createElement(problem, mtUri, "math");
            Element applyAnd = AGDMUtils.createElement(problem, mtUri, "apply");
            Element and = AGDMUtils.createElement(problem, mtUri, "and");
            Element applyNe = AGDMUtils.createElement(problem, mtUri, "apply");
            Element neq = AGDMUtils.createElement(problem, mtUri, "neq");
            Element applyDist = AGDMUtils.createElement(problem, mtUri, "apply");
            Element dist = AGDMUtils.createElement(problem, mtUri, "ci");
            dist.setTextContent("d_" + coord + "_" + variable);
            applyDist.appendChild(dist);
            applyDist.appendChild(AGDMUtils.currentCell(problem, spatialCoords));
            
            Element applyFOV = AGDMUtils.createElement(problem, mtUri, "apply");
            Element eq = AGDMUtils.createElement(problem, mtUri, "eq");
            Element FOV;
            if (region != null) {
                Element interior = null;
                if (AGDMUtils.isInteriorRegion(problem, region)) {
                    interior = AGDMUtils.createElement(problem, mtUri, "apply");
                    Element regId = AGDMUtils.createElement(problem, smlUri, "regionInteriorId");
                    regId.setTextContent(region);//FOV
                    interior.appendChild(regId);
                    interior.appendChild(exCoordDeploy(problem, spatialCoords, coord, null, false, variable));
                }
                Element surface = null;
                if (AGDMUtils.isSurfaceRegion(problem, region)) {
                    surface = AGDMUtils.createElement(problem, mtUri, "apply");
                    Element regId = AGDMUtils.createElement(problem, smlUri, "regionSurfaceId");
                    regId.setTextContent(region);//FOV
                    surface.appendChild(regId);
                    surface.appendChild(exCoordDeploy(problem, spatialCoords, coord, null, false, variable));
                }
                if (interior != null && surface != null) {
                    FOV = AGDMUtils.createElement(problem, mtUri, "apply");
                    Element plus = AGDMUtils.createElement(problem, mtUri, "plus");
                    FOV.appendChild(plus);
                    FOV.appendChild(interior);
                    FOV.appendChild(surface);
                }
                else {
                    if (interior != null) {
                        FOV = interior;
                    }
                    else {
                        FOV = surface;
                    }
                }
            }
            else {
                FOV = AGDMUtils.createElement(problem, mtUri, "apply");
                Element ci = AGDMUtils.createElement(problem, mtUri, "ci");
                ci.setTextContent("FOV");
                FOV.appendChild(ci);
                FOV.appendChild(exCoordDeploy(problem, spatialCoords, coord, null, false, variable));
            }
            Element zero = AGDMUtils.createElement(problem, mtUri, "cn");
            zero.setTextContent("0");
            math.appendChild(applyAnd);
            applyAnd.appendChild(and);
            applyAnd.appendChild(applyNe);
            applyAnd.appendChild(applyFOV);
            applyNe.appendChild(neq);
            applyNe.appendChild(applyDist);
            applyNe.appendChild(zero);
            applyFOV.appendChild(eq);
            applyFOV.appendChild(FOV);
            applyFOV.appendChild(zero.cloneNode(true));
            condition.appendChild(math);
            Element then = AGDMUtils.createElement(problem, smlUri, "then");
            //Assign the displacement
            for (int j = 0; j < spatialCoords.size(); j++) {
                String coord2 = spatialCoords.get(j);
                if (i != j) {
                    Element dispEq = AGDMUtils.createElement(problem, mtUri, "math");
                    Element apply = AGDMUtils.createElement(problem, mtUri, "apply");
                    eq = AGDMUtils.createElement(problem, mtUri, "eq");
                    Element ci = AGDMUtils.createElement(problem, mtUri, "ci");
                    ci.setTextContent("disp_" + coord2 + "_" + coord);
                    applyDist = AGDMUtils.createElement(problem, mtUri, "apply");
                    dist = AGDMUtils.createElement(problem, mtUri, "ci");
                    dist.setTextContent("d_" + coord2 + "_" + variable);
                    applyDist.appendChild(dist);
                    applyDist.appendChild(AGDMUtils.currentCell(problem, spatialCoords));
                    dispEq.appendChild(apply);
                    apply.appendChild(eq);
                    apply.appendChild(ci);
                    apply.appendChild(applyDist);
                    then.appendChild(dispEq);
                }
                
            }
            condition.appendChild(then);
            result.appendChild(condition);
        }
        
        return result;
    }
    
    /**
     * Creates the condition for axis extrapolation.
     * @param problem           The problem document
     * @param coord             The coordinate for the axis extrapolation
     * @param spatialCoords     All the spatial coordinates
     * @param field             If a field is given, use distance of this fieldç
     * @param operation			The operation to perform
     * @return                  The mathematical condition
     */
    private static Element axisExtrapolationCondition(Document problem, String coord, ArrayList<String> spatialCoords, String field, String operation) {
        String variable = "f";
        if (field != null) {
            variable = field;
        }
        Element applyCond = AGDMUtils.createElement(problem, mtUri, "apply");
        Element op = AGDMUtils.createElement(problem, mtUri, operation);
        Element applyDist = AGDMUtils.createElement(problem, mtUri, "apply");
        Element dist = AGDMUtils.createElement(problem, mtUri, "ci");
        dist.setTextContent("d_" + coord + "_" + variable);
        applyDist.appendChild(dist);
        applyDist.appendChild(AGDMUtils.currentCell(problem, spatialCoords));
        applyCond.appendChild(op);
        applyCond.appendChild(applyDist.cloneNode(true));
        Element zero = AGDMUtils.createElement(problem, mtUri, "cn");
        zero.setTextContent("0");
        applyCond.appendChild(zero);
        return applyCond;
    }
    
    /**
     * Creates the FOV variable for interaction.
     * @param problem           The reference problem
     * @param regionName        The region name to generate the variable
     * @param boundaries        If the variable is for the boundaries
     * @return                  The fov element
     * @throws AGDMException    AGDM00X  External error
     */
    private static Element createFovVariable(Document problem, String regionName, boolean boundaries) throws AGDMException {
        Element fov = null;
        if (boundaries) {
            Element interior = null;
            if (AGDMUtils.isInteriorRegion(problem, regionName)) {
                interior = AGDMUtils.createElement(problem, smlUri, "regionInteriorId");
                interior.setTextContent(regionName);//FOV
            }
            Element surface = null;
            if (AGDMUtils.isSurfaceRegion(problem, regionName)) {
                surface = AGDMUtils.createElement(problem, smlUri, "regionSurfaceId");
                surface.setTextContent(regionName);//FOV
            }
            
            if (interior != null && surface != null) {
                fov = AGDMUtils.createElement(problem, mtUri, "apply");
                Element plus = AGDMUtils.createElement(problem, mtUri, "plus");
                fov.appendChild(plus);
                fov.appendChild(interior);
                fov.appendChild(surface);
            }
            else {
                if (interior != null) {
                    fov = interior;
                }
                else {
                    fov = surface;
                }
            }
        }
        else {
            fov = AGDMUtils.createElement(problem, smlUri, "interactionRegionId");
        }
        return fov;
    }
}
