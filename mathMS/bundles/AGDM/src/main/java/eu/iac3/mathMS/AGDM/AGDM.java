/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.AGDM;

import javax.jws.WebService;

/** 
 * Automated Generator of Discretized Models is an interface 
 * that provides the functionality that converts a physical 
 * problem into a discretized problem with a discretization 
 * schema and its parameters.
 * The interface has functions which accept XML as input and 
 * returns xml as the discretization result.
 * 
 * @author      ----
 * @version     ----
 */
@WebService
public interface AGDM {

    /** 
     * It returns the result of discretize the physical problem. 
     * It checks the document against its schema and applies a preprocess to prepare the problem for discretization schemas.
     * 
     * @param policy                the discretization policy for the problem
     * @return                      the resulting xml file as a String
     * @throws AGDMException        AGDM001  XML document does not match XML schema
     *                              AGDM002  XML discretization schema not valid
     *                              AGDM003  Import discretization schema not exist
     *                              AGDM004  Received parameters do not match schema parameters
     *                              AGDM005  Not a valid XML document
     *                              AGDM006  Schema not applicable to this problem
     *                              AGDM007  External error
     */
    String discretizeProblem(String policy) throws AGDMException;
    
    /**
     * Creates a discretization policy document from the schema.
     * It reads all the parameters needed to be set for a discretization.
     * @param schema                The discretization schema
     * @return                      The discretization policy
     * @throws AGDMException        AGDM001  XML document does not match XML schema
     *                               AGDM002  XML discretization schema not valid
     *                               AGDM007  External error
     */
    String createPDEDiscretizationPolicy(String schema) throws AGDMException;
}
