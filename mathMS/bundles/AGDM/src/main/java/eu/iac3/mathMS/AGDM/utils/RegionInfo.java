/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.AGDM.utils;

import eu.iac3.mathMS.AGDM.AGDMException;
import eu.iac3.mathMS.AGDM.utils.PDE.FieldGroupMap;
import eu.iac3.mathMS.AGDM.utils.PDE.PDERegionDiscPolicy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * Relation of the fields and coordinates of a region.
 * @author bminyano
 *
 */
public class RegionInfo {

	
    private ArrayList<String> fields;
    private ArrayList<String> allFields;
    private ArrayList<String> fieldGroupsSchema;
    private ArrayList<String> fieldGroupsRegion;
    private boolean filtered = false;
    private HashMap<String, Boolean> isAuxiliary;
    
    /**
     * Constructor.
     * @param fields    	fields of the region
     * @param isAuxiliary	Auxiliary field checks
     * @param coords    	coords of the region
     */
    public RegionInfo() {
        this.fields = new ArrayList<String>();
        this.allFields = new ArrayList<String>();
        fieldGroupsSchema = new ArrayList<String>();
        fieldGroupsRegion = new ArrayList<String>();
        this.isAuxiliary = new HashMap<String, Boolean>();
    }
    
    /**
     * Constructor.
     * @param fields    	fields of the region
     * @param isAuxiliary	Auxiliary field checks
     * @param coords    	coords of the region
     */
    public RegionInfo(ArrayList<String> fields, HashMap<String, Boolean> isAuxiliary) {
        this.fields = new ArrayList<String>(fields);
        this.allFields = new ArrayList<String>(fields);
        fieldGroupsSchema = new ArrayList<String>();
        fieldGroupsRegion = new ArrayList<String>();
        this.isAuxiliary = new HashMap<String, Boolean>(isAuxiliary);
    }
    
    /**
     * Copy constructor.
     * @param sr    The object to copy
     */
    public RegionInfo(RegionInfo sr) {
        this.fields = new ArrayList<String>(sr.fields);
        this.allFields = new ArrayList<String>(sr.allFields);
        this.fieldGroupsSchema = new ArrayList<String>(sr.fieldGroupsSchema);
        this.fieldGroupsRegion = new ArrayList<String>(sr.fieldGroupsRegion);
        this.isAuxiliary = new HashMap<String, Boolean>(sr.isAuxiliary);
        this.filtered = sr.filtered;
    }
    
    public HashMap<String, Boolean> getIsAuxiliary() {
		return isAuxiliary;
	}

	public void setIsAuxiliary(HashMap<String, Boolean> isAuxiliary) {
		this.isAuxiliary = isAuxiliary;
	}
	
	/**
	 * Checks if a field is auxiliary.
	 * @param field		The field to check
	 * @return			True if auxiliary
	 */
	public boolean isAuxiliaryField(String field) {
		return isAuxiliary.getOrDefault(field, false);
	}

	public ArrayList<String> getFields() {
        return fields;
    }
    public void setFields(ArrayList<String> fields) {
        this.fields = new ArrayList<String>(fields);
    }

    public void setGroup(ArrayList<String> group) {
        this.fieldGroupsSchema = new ArrayList<String>(group);
    }

    public boolean isFiltered() {
		return filtered;
	}

	public void setFiltered(boolean filtered) {
		this.filtered = filtered;
	}

	public ArrayList<String> getAllFields() {
		return allFields;
	}

	public void setAllFields(ArrayList<String> allFields) {
		this.allFields = allFields;
	}
	
	/**
     * Get the group of the field in the region that it belongs to.
     * @param field     The name of the field
     * @return          the name of the group or null if no group specified.
     */
    public String getGroup(String field) {
        if (fieldGroupsSchema.size() == 0) {
            return null;
        }
        return fieldGroupsSchema.get(fields.indexOf(field));
    }
    
    /**
     * Fill the groups of the field with the groups mapped in the region policy.
     * @param regionPolicy         The discretization policy
     * @param region               The region element
     * @throws AGDMException        AGDM004 Received parameters do not match schema parameters
     */
    public void fillGroup(PDERegionDiscPolicy regionPolicy, Element region) throws AGDMException {
        for (int i = 0; i < fields.size(); i++) {
            NodeList fieldGroup = AGDMUtils.find(region, "//mms:fieldGroups/mms:fieldGroup[mms:field = '" + fields.get(i) + "']/mms:name");
            //All the fields must have a group
            if (fieldGroup.getLength() > 0) {
	            String group = fieldGroup.item(0).getTextContent();
	            String schemaGroup = null;
	            //Field group maps
	            ArrayList<FieldGroupMap> fgm = new ArrayList<FieldGroupMap>(Arrays.asList(regionPolicy.getFieldGroupMaps()));
	            for (int j = 0; j < fgm.size(); j++) {
	                if (fgm.get(j).getRegion().equals(group)) {
	                    schemaGroup = fgm.get(j).getSchema();
	                }
	            }
	            // Field group discretization
	           /* ArrayList<String> discFieldGroups = regionPolicy.getFieldGroups();
	            if (discFieldGroups.contains(group)) {
	            	schemaGroup = group;
	            }*/
	            if (schemaGroup != null) {
	                fieldGroupsSchema.add(schemaGroup);
	            }
	            else {
	            	fieldGroupsSchema.add("");
	            }
	            fieldGroupsRegion.add(group);
            } 
            else {
                fieldGroupsSchema.add("");
                fieldGroupsRegion.add("");
            }
        }
    }
    
    /**
     * Removes the fields that do not correspond to the schema group provided.
     * @param schemaGroup       The schema group to use
     * @throws AGDMException    AGDM006  Schema not applicable to this problem
     */
    public void filterGroup(String schemaGroup) throws AGDMException {
        if (fieldGroupsSchema.size() == 0) {
            throw new AGDMException(AGDMException.AGDM006, "Discretizing: The group " + schemaGroup + " does not exist in the region");
        }
        for (int i = fieldGroupsSchema.size() - 1; i >= 0; i--)  {
            if (!fieldGroupsSchema.get(i).equals(schemaGroup)) {
                fieldGroupsSchema.remove(i);
                fieldGroupsRegion.remove(i);
                fields.remove(i);
            }
        }
        filtered = true;
    }
  
    /**
     * Removes the fields that do not correspond to the schema group provided.
     * @param schemaGroup       The schema group to use
     * @throws AGDMException    AGDM006  Schema not applicable to this problem
     */
    public void applyGroupDiscretizationFilter(ArrayList<String> schemaGroups) throws AGDMException {
    	if (!schemaGroups.isEmpty()) {
	        for (int i = fieldGroupsRegion.size() - 1; i >= 0; i--)  {
	            if (!schemaGroups.contains(fieldGroupsRegion.get(i))) {
	            	fieldGroupsRegion.remove(i);
	            	fieldGroupsSchema.remove(i);
	                fields.remove(i);
	            }
	        }
    	}
    }
        
    /**
     * Removes all field but provided.
     * @param field				Field to filter
     * @throws AGDMException    AGDM006  Schema not applicable to this problem
     */
    public void filterField(String field) throws AGDMException {
        for (int i = fields.size() - 1; i >= 0; i--)  {
            if (!fields.get(i).equals(field)) {
                if (fieldGroupsSchema.size() > 0) {
                    fieldGroupsSchema.remove(i);
                }
                fields.remove(i);
            }
        }
    }
    
	@Override
	public String toString() {
		return "RegionInfo [fields=" + fields + ", fieldGroups=" + fieldGroupsSchema + ", filtered="
				+ filtered + ", isAuxiliary=" + isAuxiliary + "]";
	}
    
}
