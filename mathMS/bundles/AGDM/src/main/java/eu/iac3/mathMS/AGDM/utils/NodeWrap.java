/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.AGDM.utils;

import org.w3c.dom.Node;

/**
 * Node wrapper.
 * @author bminano
 *
 */
public class NodeWrap {
    Node node;
    
    /**
     * Constructor.
     * @param n The node to wrap
     */
    public NodeWrap(Node n) {
        node = n;
    }
    
    public Node getNode() {
		return node;
	}

	@Override
    public boolean equals(Object v) {
        NodeWrap ptr = (NodeWrap) v;
        return this.node.isEqualNode(ptr.node);
    }

    @Override
    public int hashCode() {
        return 1;
    }
}
