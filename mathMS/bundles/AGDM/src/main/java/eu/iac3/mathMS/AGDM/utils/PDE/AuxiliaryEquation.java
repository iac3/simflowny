/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.AGDM.utils.PDE;

import java.util.ArrayList;

import org.w3c.dom.Element;

public class AuxiliaryEquation {
	ArrayList<String> fields;
    ArrayList<String> terms;
    EquationType auxFieldType;
    Element algorithm;
    
    /**
     * Constructor.
     * @param field     The variable name
     * @param eqTerms     The terms for the equation
     * @param eqAlgorithm Auxiliary algorithm
     * @param opType    The operator type
     */
    public AuxiliaryEquation(ArrayList<String> field, ArrayList<String> eqTerms, Element eqAlgorithm, EquationType opType) {
        this.fields = field;
        this.terms = eqTerms;
        this.algorithm = eqAlgorithm;
        this.auxFieldType = opType;
    }
    
    /**
     * Copy constructor.
     * @param et    The object to copy
     */
    public AuxiliaryEquation(AuxiliaryEquation et) {
        this.fields = new ArrayList<String>();
        for (String f : et.fields) {
    		this.fields.add(f);
    	}
        this.terms = new ArrayList<String>();
        for (String t : et.terms) {
    		this.terms.add(t);
    	}
        this.auxFieldType = et.auxFieldType;
        if (et.algorithm != null) {
        	this.algorithm = (Element) et.algorithm.cloneNode(true);
        }
    }

    public ArrayList<String> getFields() {
        return fields;
    }

    public ArrayList<String> getTerms() {
        return terms;
    }

    public EquationType getOperatorType() {
        return auxFieldType;
    }
    
    public Element getAlgorithm() {
		return algorithm;
	}

	@Override
    public String toString() {
        return "EquationTerms [fields=" + fields + ", terms=" + terms + ", auxFieldType=" + auxFieldType + "]" + "\n";
    }  
    
}
