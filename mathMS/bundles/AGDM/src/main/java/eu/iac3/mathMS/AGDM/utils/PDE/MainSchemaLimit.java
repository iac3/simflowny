package eu.iac3.mathMS.AGDM.utils.PDE;

/**
 * This class stands for a limit in the main spatial operator schema.
 * The limits are deduced from the boundary schemas. 
 * If a boundary schema is not specified, there will be no limit in that axis and side.
 * @author bminano
 *
 */
public class MainSchemaLimit extends BoundarySchemaCondition {
    
    /**
     * Constructor from copy.
     * @param limit     The copied class
     */
    public MainSchemaLimit(MainSchemaLimit limit) {
        super();
        distanceToBoundary = limit.distanceToBoundary;
        axis = limit.getAxis();
        side = limit.getSide();
    }
    
    /**
     * Constructor from copy.
     * @param condition     The copied class
     */
    public MainSchemaLimit(BoundarySchemaCondition condition) {
        super(condition);
    }
    
    @Override
    public boolean equals(Object object) {
        if (object instanceof MainSchemaLimit) {
            MainSchemaLimit comp = (MainSchemaLimit) object;
            return this.axis.equals(comp.axis) && this.side.equals(comp.side) && this.distanceToBoundary == comp.distanceToBoundary;
        }
        return false;
    }
    
    @Override
    public String toString() {
        return "Limit [axis=" + axis + ", side=" + side
                + ", distanceToBoundary=" + distanceToBoundary + "]";
    }
}
