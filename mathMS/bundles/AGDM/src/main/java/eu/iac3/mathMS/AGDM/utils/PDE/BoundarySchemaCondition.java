package eu.iac3.mathMS.AGDM.utils.PDE;

import org.w3c.dom.Element;

import eu.iac3.mathMS.AGDM.utils.AGDMUtils;

/**
 * This class stands for a single condition in spatial operator discretization boundary schemas.
 * @author bminano
 *
 */
public class BoundarySchemaCondition {
    String axis;
    String side;
    int distanceToBoundary;
    
    /**
     * Default constructor.
     */
    BoundarySchemaCondition() {
    }
    
    /**
     * Constructor from copy.
     * @param condition The object to copy
     */
    public BoundarySchemaCondition(BoundarySchemaCondition condition) {
        distanceToBoundary = condition.distanceToBoundary;
        axis = condition.getAxis();
        side = condition.getSide();
    }
    
    /**
     * Constructs a boundary schema condition from the document element.
     * @param condition             The condition element
     */
    public BoundarySchemaCondition(Element condition) {
        distanceToBoundary = Integer.valueOf(condition.getElementsByTagNameNS(AGDMUtils.mmsUri, "distanceToBoundary").item(0).getTextContent());
        axis = condition.getElementsByTagNameNS(AGDMUtils.mmsUri, "axis").item(0).getTextContent();
        side = condition.getElementsByTagNameNS(AGDMUtils.mmsUri, "side").item(0).getTextContent();
    }
    
    public void setAxis(String axis) {
        this.axis = axis;
    }

    public String getAxis() {
        return axis;
    }

    public String getSide() {
        return side;
    }

    public int getDistanceToBoundary() {
        return distanceToBoundary;
    }
    
    @Override
    public String toString() {
        return "Condition [axis=" + axis + ", side=" + side
                + ", distanceToBoundary=" + distanceToBoundary + "]";
    }
    /**
     * Equals method.
     * @param bsc	object to check
     * @return		True if equals
     */
    public boolean equals(BoundarySchemaCondition bsc) {
        return bsc.getAxis().equals(this.axis) && bsc.getDistanceToBoundary() == this.distanceToBoundary && bsc.getSide().equals(this.side);
    }
}
