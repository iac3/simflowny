/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.AGDM.utils;

import java.util.ArrayList;

/**
 * Relation of the discrete and continuos coordinates, and its type (spatial or time).
 * @author bminyano
 *
 */
public class CoordinateInfo {

    private ArrayList<String> discCoords;
    private ArrayList<String> contCoords;
    private String discTimeCoord;
    private String contTimeCoord;
    
    /**
     * Constructor.
     * 
     * @param disc  Discrete coordinate
     * @param cont  Continuous coordinate
     * @param type  Type of the coordinate, spatial or time.
     */
    public CoordinateInfo(ArrayList<String> discCoords, ArrayList<String> contCoords, String discTimeCoord, String contTimeCoord) {
    	this.discCoords = new ArrayList<String>(discCoords);
        this.contCoords = new ArrayList<String>(contCoords);
        this.discTimeCoord = discTimeCoord;
        this.contTimeCoord = contTimeCoord;
    }

	public ArrayList<String> getDiscCoords() {
		return discCoords;
	}

	public ArrayList<String> getContCoords() {
		return contCoords;
	}

	public String getDiscTimeCoord() {
		return discTimeCoord;
	}

	public String getContTimeCoord() {
		return contTimeCoord;
	}
    
	/**
	 * Get continuous spatial coordinate equivalent to given discrete one
	 * @param discCoord		given discrete coordinate
	 * @return				continuous coordinate
	 */
	public String getDiscToCont(String discCoord) {
		return contCoords.get(discCoords.indexOf(discCoord));
	}
	
	/**
	 * Get discrete spatial coordinate equivalent to given continuous one
	 * @param contCoord		given continuous coordinate
	 * @return				discrete coordinate
	 */
	public String getContToDisc(String contCoord) {
		return discCoords.get(contCoords.indexOf(contCoord));
	}
    
}
