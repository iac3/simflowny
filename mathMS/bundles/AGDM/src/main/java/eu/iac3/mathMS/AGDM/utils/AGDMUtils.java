/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.AGDM.utils;

import eu.iac3.mathMS.AGDM.AGDMException;
import eu.iac3.mathMS.AGDM.processors.ExecutionFlow;
import eu.iac3.mathMS.AGDM.processors.SchemaCreator;
import eu.iac3.mathMS.AGDM.utils.PDE.EquationType;
import eu.iac3.mathMS.AGDM.utils.PDE.Derivative;
import eu.iac3.mathMS.AGDM.utils.PDE.DerivativeLevel;
import eu.iac3.mathMS.AGDM.utils.PDE.EquationTermsLevel;
import eu.iac3.mathMS.AGDM.utils.PDE.MultiplicationTerm;
import eu.iac3.mathMS.AGDM.utils.PDE.MultiplicationTermsLevel;
import eu.iac3.mathMS.AGDM.utils.PDE.OperatorInfoType;
import eu.iac3.mathMS.AGDM.utils.PDE.OperatorsInfo;
import eu.iac3.mathMS.AGDM.utils.PDE.PDERegionDiscPolicy;
import eu.iac3.mathMS.AGDM.utils.PDE.ProcessInfo;
import eu.iac3.mathMS.AGDM.utils.PDE.Tensor;
import eu.iac3.mathMS.documentManagerPlugin.DMException;
import eu.iac3.mathMS.documentManagerPlugin.DocumentManager;
import eu.iac3.mathMS.documentManagerPlugin.DocumentManagerImpl;
import eu.iac3.mathMS.simflownyUtilsPlugin.SUException;
import eu.iac3.mathMS.simflownyUtilsPlugin.SimflownyUtils;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.stream.Collectors;

import javax.xml.XMLConstants;
import javax.xml.namespace.NamespaceContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;

import static eu.iac3.mathMS.AGDM.utils.PDE.EquationType.*;

/**
 * Utility class.
 * 
 * @author bminyano
 *
 */
public final class AGDMUtils {
    static final int THREE = 3;
    static final int FOUR = 4;
    static final int FIVE = 5;
    static final int SIX = 6;
    public static String mtUri = "http://www.w3.org/1998/Math/MathML";
    public static String mmsUri = "urn:mathms";
    public static String simmlUri = "urn:simml";
    private static XPathFactory factory;
    static XPath xpath;
    public static int schemaNameCounter = 0;
    static Transformer transformerSelected;
    static final String XSL_PDE = "common" + File.separator + "XSLTmathmlToC" + File.separator + "instructionToC.xsl";
    
    /**
     * Constructor.
     * @throws AGDMException  
     */
    public AGDMUtils() throws AGDMException {
        try {
	    	TransformerFactory tFactory = new net.sf.saxon.TransformerFactoryImpl();
	    	transformerSelected = tFactory.newTransformer(new StreamSource(XSL_PDE));
	        factory = new net.sf.saxon.xpath.XPathFactoryImpl();
	        xpath = factory.newXPath();
        } 
        catch (Exception e) {
            throw new AGDMException(AGDMException.AGDM00X, e.toString());
        }
    }
    
    /**
     * Converts a string to Dom.
     * 
     * @param str               The string
     * @return                  The Dom document
     * @throws AGDMException    AGDM005 - Not a valid XML Document
     *                          AGDM007 - External error
     */
    public static Document stringToDom(String str) throws AGDMException {
        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            docFactory.setNamespaceAware(true);
            docFactory.setIgnoringElementContentWhitespace(true);
            docFactory.setIgnoringComments(true);
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            InputStream is = new ByteArrayInputStream(str.getBytes("UTF-8"));
            Document result = docBuilder.parse(is);
            removeWhitespaceNodes(result.getDocumentElement());
            return result;
        }
        catch (SAXException e) {
            throw new AGDMException(AGDMException.AGDM005, e.toString());
        }
        catch (Exception e) {
            throw new AGDMException(AGDMException.AGDM00X, e.toString());
        }
    }
    
    /**
     * Converts a Dom node to string.
     * 
     * @param node              The dom node
     * @return                  The String
     * @throws AGDMException    AGDM007 - External error
     */
    public static String domToString(Node node) throws AGDMException {
        try {
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            StringWriter writer = new StringWriter();
            StreamResult result = new StreamResult(writer);
            DOMSource source = new DOMSource(node);
            transformer.transform(source, result);
            
            
            return writer.toString();
        }
        catch (Exception e) {
            throw new AGDMException(AGDMException.AGDM00X, e.toString());
        }
    }
    
    /**
     * Executes an xpath query in the document. Using Saxon 9.1 implementation (xpath 2.0).
     * @param node              The document to find in
     * @param xpathQuery        The query
     * @return                  A node list with the results
     * @throws AGDMException    AGDM00X - External error
     */
    public static NodeList find(Node node, String xpathQuery) throws AGDMException {
        try {
            
            NamespaceContext ctx = new NamespaceContext() {
                public String getNamespaceURI(String prefix) {
                    String uri;
                    if (prefix.equals("mms")) {
                        uri = "urn:mathms";  
                    }    
                    else {
                        if (prefix.equals("mt")) {
                            uri = "http://www.w3.org/1998/Math/MathML";
                        }
                        else {
                            if (prefix.equals("sml")) {
                                uri = "urn:simml";
                            }
                            else {
                                uri = null;
                            }
                        }
                    }
                    return uri;
                }
                // Dummy implementation - not used!
                public Iterator < ? > getPrefixes(String val) {
                    return null;
                }
                // Dummy implemenation - not used!
                public String getPrefix(String uri) {
                    return null;
                }
            };

            xpath.setNamespaceContext(ctx);
            XPathExpression expr = xpath.compile(xpathQuery);

            return (NodeList) expr.evaluate(node, XPathConstants.NODESET);
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            throw new AGDMException(AGDMException.AGDM00X, e.getMessage());
        } 
    }
    
    /**
     * Remove namespaces from xml element.
     * @param elem  The element
     * @return      The element without namespace
     */
    public static Element removeNamespaces(Element elem) {
        NamedNodeMap nnm = elem.getAttributes();
        for (int i = 0; i < nnm.getLength(); i++) {
            if (nnm.item(i).getNodeName().startsWith("xmlns:")) {
                elem.removeAttribute(nnm.item(i).getNodeName());
            }
        }
        return elem;
    }
    
    /**
     * Remove the white spaced nodes from a Dom element.
     * 
     * @param e The element to delete the whitespaces from
     */
    public static void removeWhitespaceNodes(Element e) {
        NodeList children = e.getChildNodes();
        for (int i = children.getLength() - 1; i >= 0; i--) {
            Node child = children.item(i);
            if (child instanceof Text && ((Text) child).getData().trim().length() == 0) {
                e.removeChild(child);
            }
            else if (child instanceof Element) {
                removeWhitespaceNodes((Element) child);
            }
        }
    }
        
    /**
     * Gets the process info classes that have any flux.
     * @param pis   An array of Process info
     * @return      An array of Process info with fluxes
     */
    public static ArrayList<ProcessInfo> getOnlyWithFlux(ArrayList<ProcessInfo> pis) {
        ArrayList<ProcessInfo> result = new ArrayList<ProcessInfo>();
        for (int i = 0; i < pis.size(); i++) {
            if (pis.get(i).getFluxesCoords().size() > 0) {
                result.add(pis.get(i));
            }
        }
        return result;
    }
    
    /**
     * Generate the processInfos with the information of the equations.
     * @param problem               Problem with the equation data
     * @param pi                The processInfo base with coordinate and field relations
     *                              and parameter info
     * @param vectorPI              Generate the PI for the eigen vectors
     * @return                      An array list with all the processInfo generated.
     * @throws AGDMException        AGDM00X External error
     */
    public static ArrayList<ProcessInfo> generateProcessInfo(Node problem, ProcessInfo pi, boolean vectorPI) 
        throws AGDMException {
        ArrayList<ProcessInfo> pis = new ArrayList<ProcessInfo>();

        if (vectorPI) {
            //Search for the canonical PDE evolution equations
            NodeList equationSet = find(problem, ".//mms:equationSet");
            for (int i = 0; i < equationSet.getLength(); i++) {
                ProcessInfo tmpPi = new ProcessInfo(pi);
                NodeList charDecomps = ((Element) equationSet.item(i)).getElementsByTagName("mms:characteristicDecompositions");
                if (charDecomps.getLength() > 0) {
                    tmpPi.setCharDecomposition((Element) charDecomps.item(0));
                }
                ArrayList<String> vectors = tmpPi.getEigenVectors();
                for (int k = 0; k < vectors.size(); k++) {
                    String vector = vectors.get(k);
                    ProcessInfo newPi = new ProcessInfo(tmpPi);
                    newPi.setVector(vector);
                    pis.add(newPi);
                }
            }
            if (equationSet.getLength() == 0) {
                pis.add(pi);
            } 
        }
        else {
            //Search for the canonical PDE evolution equations
            NodeList equation = find(problem, "//mms:evolutionEquation");
            for (int i = 0; i < equation.getLength(); i++) {
                String field = ((Element) equation.item(i)).getElementsByTagName("mms:field").item(0).getTextContent();
                if (pi.getRegionInfo().getFields().contains(field) && 
                		((pi.getDiscretizationType() == DiscretizationType.mesh && pi.getMeshFields().contains(field))
                		|| (pi.getDiscretizationType() == DiscretizationType.particles && pi.getParticleFields().contains(field)))) {
                    ProcessInfo newPi = new ProcessInfo(pi);
                    //Get the equation
                    newPi.setEquation((Element) equation.item(i));
                    //get field
                    newPi.setField(field);
                    //Get the characteristic decomposition if exists
                    NodeList modelId = ((Element) equation.item(i)).getElementsByTagName("mms:modelId");
                    //If there is modelId, get the characteristic decomposition
                    String charQuery;
                    if (modelId.getLength() > 0) {
                        charQuery = "//mms:characteristicDecompositions[mms:modelId= '" + modelId.item(0).getTextContent() + "']";
                    }
                    //If there is not modelId there is a model and have to get the characteristic decomposition without modelId
                    else {
                        charQuery = "//mms:characteristicDecompositions";
                    }
                    NodeList charDecomps = find(problem, charQuery);
                    if (charDecomps.getLength() > 0) {
                        newPi.setCharDecomposition((Element) charDecomps.item(0));
                    }
                    pis.add(newPi);
                }
            }
            //If there is a field filter, it is posible to use auxiliary fields
            if (pi.getRegionInfo().isFiltered()) {
            	NodeList auxEquation = find(problem, "//mms:auxiliaryFieldEquation");
                for (int i = 0; i < auxEquation.getLength(); i++) {
                	NodeList fieldList = ((Element) auxEquation.item(i)).getElementsByTagName("mms:auxiliaryField");
                	for (int j = 0; j < fieldList.getLength(); j++) {
                        String field = fieldList.item(j).getTextContent();
                        if (pi.getRegionInfo().getFields().contains(field) && 
                        		((pi.getDiscretizationType() == DiscretizationType.mesh && pi.getMeshFields().contains(field))
                                		|| (pi.getDiscretizationType() == DiscretizationType.particles && pi.getParticleFields().contains(field)))) {
                            ProcessInfo newPi = new ProcessInfo(pi);
                            //Get the equation
                            newPi.setEquation((Element) auxEquation.item(i));
                            //get field
                            newPi.setField(field);
                            pis.add(newPi);
                        }
                	}
                }
            }
            
            if (equation.getLength() == 0) {
                pis.add(pi);
            } 
        }
        return pis;
    }
    
    /**
     * Generate the processInfos with the information of the boundary.
     * @param problem               Problem with the equation data
     * @param boundary              Boundary with the equation data
     * @param basePi                The processInfo base with coordinate and field relations
     *                              and parameter info
     * @param allowedFields			Allowed fields for the process info
     * @param withAux               true if the boundary information for the auxiliary fields have to be added 
     * @param withFields			true if the boundary information for the fields have to be added
     * @return                      An array list with all the processInfo generated.
     * @throws AGDMException        AGDM006 External error
     */
    public static ArrayList<ProcessInfo> generateProcessInfoForBoundary(Node problem, Node boundary, ProcessInfo basePi, 
    		ArrayList<String> allowedFields, boolean withAux, boolean withFields) throws AGDMException {
        
        ArrayList<ProcessInfo> pis = new ArrayList<ProcessInfo>();
      
        //Search for the canonical PDE evolution equations
        String query = ".//mms:evolutionEquation//mms:field|.//" 
                + "mms:auxiliaryFieldEquation//mms:auxiliaryField";
        if (!withAux) {
            query = ".//mms:evolutionEquation//mms:field";
        }
        if (!withFields) {
            query = ".//mms:auxiliaryFieldEquation//mms:auxiliaryField";
        }
        NodeList equationFields = find(problem, query);
        for (int j = 0; j < equationFields.getLength(); j++) {
            String field = equationFields.item(j).getTextContent();
            NodeList fields = find(boundary, ".//mms:field[text() = '" + field + "' and parent::mms:fields]");
            if (fields.getLength() > 0 && allowedFields.contains(field)) {
                ProcessInfo newPi = new ProcessInfo(basePi);
                //get field
                newPi.setField(fields.item(0).getTextContent());
                //Get the characteristic decomposition if exists
                NodeList modelId = find(problem, ".//mms:model/mms:id");
                //If there is modelId, get the characteristic decomposition
                String charQuery;
                if (modelId.getLength() > 0) {
                    charQuery = ".//mms:characteristicDecompositions[mms:modelId= '" + modelId.item(0).getTextContent() + "']";
                    newPi.setModelId(modelId.item(0).getTextContent());
                }
                //If there is not modelId there is a model and have to get the characteristic decomposition without modelId
                else {
                    charQuery = ".//mms:characteristicDecompositions";
                    newPi.setModelId("");
                }
                NodeList charDecomps = find(problem, charQuery);
                if (charDecomps.getLength() > 0) {
                    newPi.setCharDecomposition((Element) charDecomps.item(0));
                }
                pis.add(newPi);   
            }
        }
        return pis;
    }
    
    /**
     * The preprocess method prepares the problem for the discretization.
     * It has to do the following jobs:
     * 1- For each model replace the local values with the equivalence values
     * 2- Import the equation set of each model
     * 3- Replace the usage of auxiliary fields by their equations in the characteristic decompositions
     * 4- Remove invalid namespaces
     * 
     * @param simProblem        The simulation problem to transform
     * @param constantInfo		Constant information
     * @return                  The case with the transformation
     * @throws AGDMException    AGDM007 - External error
     */
    public static Document preprocess(String simProblem, ConstantInformation constants) throws AGDMException {
        try {
            //Prefix of the problems
            Document docProblem = stringToDom(simProblem);
            DocumentManager docMan = new DocumentManagerImpl();
           
            NodeList models = docProblem.getElementsByTagName("mms:model");
            for (int i = models.getLength() - 1; i >= 0; i--) {
                Element model = (Element) models.item(i);
                String modelId = model.getElementsByTagNameNS(mmsUri, "id").item(0).getTextContent();

                //Getting all the equivalences
                NodeList fieldEquivalences = model.getElementsByTagName("mms:fieldEquivalence");
                NodeList variableEquivalences = model.getElementsByTagName("mms:variableEquivalence");
                NodeList coordinateEquivalences = model.getElementsByTagName("mms:coordinateEquivalence");
                NodeList parameterEquivalences = model.getElementsByTagName("mms:parameterEquivalence");
                NodeList tensorEquivalences = model.getElementsByTagName("mms:tensorEquivalence");
                NodeList eigenVectorEquivalences = model.getElementsByTagName("mms:eigenVectorEquivalence");
                
                //Get the model by the id in the import.
                Document docModel = stringToDom(SimflownyUtils.jsonToXML(docMan.getDocument(modelId)));
                //Get constant information from model
                AGDMUtils.getConstants(docModel, constants);
                // 1-Replacing the model values of field, parameters and coordinates with the case values
                // There could be crossed equivalences, so a temporal intermediate name has to be used 
                //Change to temporal names
                equivalenceReplace(docProblem, docModel, modelId, fieldEquivalences, true);
                equivalenceReplace(docProblem, docModel, modelId, variableEquivalences, true);
                equivalenceReplace(docProblem, docModel, modelId, coordinateEquivalences, true);
                equivalenceReplace(docProblem, docModel, modelId, parameterEquivalences, true);
                equivalenceReplace(docProblem, docModel, modelId, eigenVectorEquivalences, true);
                tensorReplace(docProblem, docModel, modelId, tensorEquivalences, true);
                //Change to new names
                equivalenceReplace(docProblem, docModel, modelId, fieldEquivalences, false);
                equivalenceReplace(docProblem, docModel, modelId, variableEquivalences, false);
                equivalenceReplace(docProblem, docModel, modelId, coordinateEquivalences, false);
                equivalenceReplace(docProblem, docModel, modelId, parameterEquivalences, false);
                equivalenceReplace(docProblem, docModel, modelId, eigenVectorEquivalences, false);
                tensorReplace(docProblem, docModel, modelId, tensorEquivalences, false);

                // 2 - Adding the equation set to the model import
                //Getting the equations.
                Element equationSet = createElement(docModel, mmsUri, "equationSet");
                equationSet.appendChild(docModel.getDocumentElement().getElementsByTagName("mms:evolutionEquations").item(0));
                NodeList auxEquations = docModel.getDocumentElement().getElementsByTagName("mms:auxiliaryEquations");
                if (auxEquations.getLength() > 0) {
                    equationSet.appendChild(auxEquations.item(0));
                }
                auxEquations = docModel.getDocumentElement().getElementsByTagName("mms:auxiliaryFieldEquations");
                if (auxEquations.getLength() > 0) {
                    equationSet.appendChild(auxEquations.item(0));
                }
                auxEquations = docModel.getDocumentElement().getElementsByTagName("mms:fieldRecoveries");
                if (auxEquations.getLength() > 0) {
                    equationSet.appendChild(auxEquations.item(0));
                }
                auxEquations = docModel.getDocumentElement().getElementsByTagName("mms:auxiliaryVariableEquations");
                if (auxEquations.getLength() > 0) {
                    equationSet.appendChild(auxEquations.item(0));
                }
                NodeList stiffSolver = docModel.getDocumentElement().getElementsByTagName("mms:stiffSolver");
                if (stiffSolver.getLength() > 0) {
                    equationSet.appendChild(stiffSolver.item(0));
                }
                NodeList charDecomposition = docModel.getDocumentElement().getElementsByTagName("mms:characteristicDecompositions");
                if (charDecomposition.getLength() > 0) {
                    equationSet.appendChild(charDecomposition.item(0));
                }
                Element clon = (Element) model.cloneNode(true);
                clon.appendChild(docProblem.importNode(equationSet, true));
                //Adding the model to the problem document
                model.getParentNode().replaceChild(clon, model);
            }
            
            //3- Replace the utilization of auxiliary fields by their equations in the characteristic decompositions
            replaceAuxiliaryFieldsInCharDecomp(docProblem);
            
            //4- Remove invalid namespaces
            String doc = domToString(docProblem);
            doc = doc.replaceAll("xmlns=?(\"|\').*?(\"|\')", "");
            
            //Return the case pre-processed
            return stringToDom(doc);
        }
        catch (Exception e) {
            throw new AGDMException(AGDMException.AGDM00X, e.toString());
        }
    }
   
    /**
     * Replaces all the occurrences of the equivalences in a model.
     * 
     * @param docProblem            The problem document
     * @param docModel              The model document where to replace
     * @param modelId               The model id for the equivalences
     * @param equivalences          The equivalences to be used
     * @param toTemporal            If true the name would be temporal, else the name would be the final name
     *                                  Depending of the equality type the parameters could be:
     *                                      a - field (field, auxiliary field)
     *                                      b - coordinate (coordinates)
     *                                      c - null (parameters)
     * @throws AGDMException        AGDM00X - External error
     */
    public static void equivalenceReplace(Document docProblem, Document docModel, String modelId, NodeList equivalences, 
            boolean toTemporal) throws AGDMException {
        //Replace all the equalities
        if (equivalences.getLength() > 0) {
            Element equality = (Element) equivalences.item(0);
            NodeList replacements = equality.getElementsByTagName("mms:equivalence");
            for (int i = 0; i < replacements.getLength(); i++) {
                Element replacement = (Element) replacements.item(i);
                String oldName;
                String newName;
                if (toTemporal) {
                    oldName = replacement.getLastChild().getTextContent();
                    newName = replacement.getFirstChild().getTextContent() + "#TMP";
                }
                else {
                    oldName = replacement.getFirstChild().getTextContent() + "#TMP";
                    newName = replacement.getFirstChild().getTextContent(); 
                }
                //Replace the old values.
                NodeList data = find(docModel, 
                        "//mms:evolutionEquations|//mms:auxiliaryFieldEquations|//mms:auxiliaryVariableEquations"
                        + "|//mms:characteristicDecompositions");
                for (int j = 0; j < data.getLength(); j++) {
                	replaceText(data.item(j), oldName, newName);
                }
                
            }
        }
    }
   
    /**
     * Replaces all the occurrences of a tensor in a model.
     * @param docProblem            The problem document
     * @param docModel              The model document where to replace
     * @param modelId               The model id for the equivalences
     * @param equivalences          The equivalences to be used
     * @param toTemporal            If true the name would be temporal, else the name would be the final name
     * @throws AGDMException        AGDM00X - External error
     */
    public static void tensorReplace(Document docProblem, Document docModel, String modelId, NodeList equivalences, boolean toTemporal) 
            throws AGDMException {
            if (docModel.getElementsByTagName("mms:characteristicDecompositions").getLength() > 0 && equivalences.getLength() > 0) {
                //Replace all the equalities
            	Element equality = (Element) equivalences.item(0);
                NodeList replacements = equality.getElementsByTagName("mms:equivalence");
                for (int j = 0; j < replacements.getLength(); j++) {
                    Element replacement = (Element) replacements.item(j);
                    String oldName;
                    String newName;
                    if (toTemporal) {
                        oldName = replacement.getLastChild().getTextContent() + "_n";
                        newName = replacement.getFirstChild().getTextContent() + "_n" + "#TMP";
                    }
                    else {
                        oldName = replacement.getFirstChild().getTextContent() + "_n" + "#TMP";
                        newName = replacement.getFirstChild().getTextContent() + "_n"; 
                    }
                    replaceText(docModel.getElementsByTagName("mms:characteristicDecompositions").item(0), oldName, newName);
                    
                    NodeList modelCoords = find(docModel, "//mms:spatialCoordinates/mms:spatialCoordinate");
                    NodeList problemCoords = find(docProblem, "//mms:spatialCoordinates/mms:spatialCoordinate");
                    for (int k = 0; k < modelCoords.getLength(); k++) {
                        String modelCoord = modelCoords.item(k).getTextContent();
                        String problemCoord = problemCoords.item(k).getTextContent();
                        if (toTemporal) {
                            oldName = replacement.getLastChild().getTextContent() + "_t" + modelCoord;
                            newName = replacement.getFirstChild().getTextContent() + "_t" + problemCoord + "#TMP";
                        }
                        else {
                            oldName = replacement.getFirstChild().getTextContent() + "_t" + problemCoord + "#TMP";
                            newName = replacement.getFirstChild().getTextContent() + "_t" + problemCoord; 
                        }
                        replaceText(docModel.getElementsByTagName("mms:characteristicDecompositions").item(0), oldName, newName);
                    }
                }
            }
    }
  
    /**
     * Creates the normal variables for a given axis.
     * @param doc               The document
     * @param pi                The process information
     * @param coord             The spatial coordinate of the axis
     * @param dir               The direction of the normal
     * @return                  The instructions
     * @throws AGDMException    AGDM00X  External error
     */
    public static DocumentFragment createNormalsToAxis(Document doc, ProcessInfo pi, String coord, String dir) throws AGDMException {
        DocumentFragment newInstructions = doc.createDocumentFragment();
        ArrayList<String> coords = pi.getBaseSpatialCoordinates();
        for (int i = 0; i < coords.size(); i++) {
            if (pi.getCoordinateRelation().getDiscToCont(coord).equals(coords.get(i))) {
                Element normal = AGDMUtils.createElement(doc, mtUri, "math");
                Element apply2 = AGDMUtils.createElement(doc, mtUri, "apply");
                Element eq2 = AGDMUtils.createElement(doc, mtUri, "eq");
                Element ci2 = AGDMUtils.createElement(doc, mtUri, "ci");
                ci2.setTextContent("n_" + coords.get(i));
                Element unit = AGDMUtils.createElement(doc, mtUri, "cn");
                unit.setTextContent("1");
                normal.appendChild(apply2);
                apply2.appendChild(eq2);
                apply2.appendChild(ci2);
                apply2.appendChild(unit);
                if (dir.equals("lower")) {
                    Element apply3 = AGDMUtils.createElement(doc, mtUri, "apply");
                    Element minus = AGDMUtils.createElement(doc, mtUri, "minus");
                    apply3.appendChild(minus);
                    apply3.appendChild(unit.cloneNode(true));
                    apply2.replaceChild(apply3, unit);
                    newInstructions.appendChild(normal.cloneNode(true));
                }
                else {
                    newInstructions.appendChild(normal.cloneNode(true));
                }
            }
            else {
                Element normal = AGDMUtils.createElement(doc, mtUri, "math");
                Element apply = AGDMUtils.createElement(doc, mtUri, "apply");
                Element eq = AGDMUtils.createElement(doc, mtUri, "eq");
                Element ci = AGDMUtils.createElement(doc, mtUri, "ci");
                ci.setTextContent("n_" + coords.get(i));
                Element cn = AGDMUtils.createElement(doc, mtUri, "cn");
                cn.setTextContent("0");
                normal.appendChild(apply);
                apply.appendChild(eq);
                apply.appendChild(ci);
                apply.appendChild(cn);
                newInstructions.appendChild(normal);
            }
        }
        return newInstructions;
    }
    
    /**
     * Replaces the occurrences of old node with a new node in a element.
     * @param scope             Scope for the replacement
     * @param oldNode           The old node
     * @param newNode           The new node
     * @throws AGDMException    AGDM00X - External error
     */
    public static void nodeReplace(Node scope, Node oldNode, Node newNode) throws AGDMException {
        NodeList candidates = find(scope, printTree(oldNode, 0));
        for (int i = candidates.getLength() - 1; i >= 0; i--) {
            candidates.item(i).getParentNode().replaceChild(newNode.cloneNode(true), candidates.item(i));
        }
    }
    
    
    /**
     * Creates a Xpath with the information of the node. Recursive.
     * @param doc           The node to create.
     * @param position      The position in the code of the node.
     * @return              A Xpath for the node
     */
    public static String printTree(Node doc, int position) {
        try {
            String acc;
            if (doc.getNodeName().equals("#text")) {
                return "text() = \"" + doc.getNodeValue() + "\"";
            }
          
            if (position == 0) {
                acc = ".//" + doc.getNodeName();
            }
            else {
                acc = "name(*[" + position + "]";
            }
          
            NodeList nl = doc.getChildNodes();
            acc = acc + "[";
            NamedNodeMap nnm = doc.getAttributes();
            for (int i = 0; i < nnm.getLength(); i++) {
                if (!nnm.item(i).getNodeName().startsWith("xmlns:")) {
                    acc = acc + "@" + nnm.item(i).getNodeName() + " = '" + nnm.item(i).getNodeValue() + "' and ";
                }
            }
            if (nnm.getLength() == 0) {
                acc = acc + "not(attribute::*) and ";
            }
            
            int counter = 1;
            for (int i = 0; i < nl.getLength(); i++) {
                Node node = nl.item(i);
                acc = acc + printTree(node, counter) + " and ";
                if (!node.getNodeName().equals("#text")) {
                    counter++;
                }
            }
            if (counter - 1 > 0) {
            	acc = acc + "count(*) = " + (counter - 1) + " and ";
            }
            
            acc = acc.substring(0, acc.lastIndexOf(" and"));
            acc = acc + "]";
          
            if (position > 0) {
                acc = acc + ") = '" + doc.getNodeName() + "'";
            }
            return acc;
        } 
        catch (Throwable e) {
            return null;
        }
    }
    
    /**
     * Replaces the auxiliary fields in the vectors and undiagonalizations in the characteristic decompositions.
     * That avouid future problems at discretization time.
     * @param problem               The problem
     * @throws AGDMException        AGDM005 - Not a valid XML document
     *                              AGDM00X - External error
     */
    private static void replaceAuxiliaryFieldsInCharDecomp(Document problem) throws AGDMException {
        NodeList models = problem.getElementsByTagName("mms:modelImport");
        for (int i = 0; i < models.getLength(); i++) {
            String modelName = models.item(i).getFirstChild().getTextContent();
            //Get the auxiliary equations
            HashMap<String, Element> auxiliaryEquations = new HashMap<String, Element>();
            NodeList auxs = ((Document) models.item(i)).getElementsByTagName("mms:auxiliaryEquationData");
            for (int j = 0; j < auxs.getLength(); j++) {
                auxiliaryEquations.put(auxs.item(j).getFirstChild().getTextContent(), 
                        (Element) auxs.item(j).getLastChild().getFirstChild().getFirstChild().getFirstChild().cloneNode(true));
            }
            //Replace possible auxiliary field within equations
            boolean finish = false;
            while (!finish) {
                finish = true;    
                Iterator<String> auxFieldIt = auxiliaryEquations.keySet().iterator();
                while (auxFieldIt.hasNext()) {
                    String auxField = auxFieldIt.next();
                    Element equation = auxiliaryEquations.get(auxField);
                    Iterator<String> auxFieldEqIt = auxiliaryEquations.keySet().iterator();
                    while (auxFieldEqIt.hasNext()) {
                        String auxFieldEq = auxFieldEqIt.next();
                        NodeList auxFieldOccurrences = 
                            find(equation, ".//mt:ci[text() = '" + auxFieldEq + "']|.[self::mt:ci[text() = '" + auxFieldEq + "']]");
                        if (auxFieldEq.equals(auxField) && auxFieldOccurrences.getLength() > 0) {
                            throw new AGDMException(AGDMException.AGDM005, "Model: " + modelName + " has recursive auxiliary equations");
                        }
                        for (int j = auxFieldOccurrences.getLength() - 1; j >= 0; j--) {
                            finish = false;
                            auxFieldOccurrences.item(j).getParentNode().replaceChild(
                                    auxiliaryEquations.get(auxFieldEq).cloneNode(true), auxFieldOccurrences.item(j));
                        }
                    }
                }
            }
            Iterator<String> auxFieldIt = auxiliaryEquations.keySet().iterator();
            while (auxFieldIt.hasNext()) {
                String auxField = auxFieldIt.next();
                Element equation = auxiliaryEquations.get(auxField);
                NodeList auxFields = find(models.item(i), ".//mt:ci[(ancestor::mms:eigenVector or ancestor::" 
                        + "mms:undiagonalization or ancestor::mms:eigenValue) and text() = '" + auxField + "']");
                for (int j = auxFields.getLength() - 1; j >= 0; j--) {
                    auxFields.item(j).getParentNode().replaceChild(equation.cloneNode(true), auxFields.item(j));
                }
            }
        }
    }
    
    /**
     * Create an element in the Uri namespace.
     * @param doc           The document where to create the element
     * @param uri           The namespace uri
     * @param tagName       The tag name
     * @return              The element
     */
    public static Element createElement(Document doc, String uri, String tagName) {
        Element element;
        if (uri != null) {
            element = doc.createElementNS(uri, tagName);
            if (uri.equals("http://www.w3.org/1998/Math/MathML")) {
                element.setPrefix("mt");
            }
            if (uri.equals("urn:mathms")) {
                element.setPrefix("mms");
            }
            if (uri.equals("urn:simml")) {
                element.setPrefix("sml");
            }
        }
        else {
            element = doc.createElement(tagName); 
        }
        return element;
    }
    
    /**
     * Method that validates the xml document against a XSD.
     * @param xmlDoc            The document to be validated
     * @param xsd               The schema to be checked against
     * @throws AGDMException    AGDM001 - XML document does not match XML Schema
     *                          AGDM007 - External error
     */   
    public static void schemaValidation(String xmlDoc, String xsd) throws AGDMException {
        try {
            // 1. Lookup a factory for the W3C XML Schema language
            SchemaFactory sFactory = 
                SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            sFactory.setFeature("http://apache.org/xml/features/validation/schema-full-checking", false);
            // 2. Compile the schema. 
            // Here the schema is loaded from a java.io.File.
            String filePath = "common" + File.separator + "XSD" + File.separator + xsd;
            File schemaLocation = new File(filePath);
            Schema schema = sFactory.newSchema(schemaLocation);
            // 3. Get a validator from the schema.
            Validator validator = schema.newValidator();
            // 4. Parse the document you want to check.
            InputStream is = new ByteArrayInputStream(xmlDoc.getBytes("UTF-8"));
            Source source = new StreamSource(is);
            // 5. Check the document
            validator.validate(source);
        }
        catch (SAXException e) {
            e.printStackTrace();
            throw new AGDMException(AGDMException.AGDM001, "Document: " + xmlDoc + "\n" + e.toString());
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new AGDMException(AGDMException.AGDM00X, e.toString());
        }
    }
    
    /**
     * Validation of the simflowny problem correctness:
     * 1 - Periodical boundaries must be defined in both sides of the axis and must be alone in the axis.
     * 2 - All the boundaries must be defined in all the sides of the domain
     * 3 - The name of the regions must not be in conflict with the boundaries ones.
     * @param problem           The problem
     * @throws AGDMException    AGDM008  Invalid simulation problem
     *                          AGDM00X  External error
     */
    public static void simflownyValidation(Document problem) throws AGDMException {
        try {
            //1
            NodeList periodical = find(problem, "//mms:boundaryCondition/mms:type[mms:periodical]");
            for (int i = 0; i < periodical.getLength(); i++) {
                String axis = periodical.item(i).getNextSibling().getTextContent();
                String condition = "";
                if (!axis.equals("All")) {
                    condition = "[mms:axis = '" + axis + "' or mms:axis = 'All']";
                }
                if (find(problem, "//mms:boundaryCondition" + condition + "/mms:type[not(mms:periodical)]").getLength() > 0) {
                    throw new AGDMException(AGDMException.AGDM008, "Periodical boundaries must be defined in the whole axis."
                        + "Any other boundary type can share axis with a periodical boundary.");
                }
            }
            //2
            NodeList coords = find(problem, "//mms:spatialCoordinates/mms:spatialCoordinate");
            for (int i = 0; i < coords.getLength(); i++) {
                String coord = coords.item(i).getTextContent();
                NodeList axisBounds = find(problem, "//mms:boundaryCondition[mms:axis = '" + coord + "' or mms:axis = 'All']/mms:side");
                if (axisBounds.getLength() == 0) {
                    throw new AGDMException(AGDMException.AGDM008, "Axis " + coord + " not defined in boundaries.");
                }
                boolean lower = false;
                boolean upper = false;
                for (int j = 0; j < axisBounds.getLength(); j++) {
                    String side = axisBounds.item(j).getTextContent();
                    if (side.toLowerCase().equals("all") || side.toLowerCase().equals("lower")) {
                        lower = true;
                    }
                    if (side.toLowerCase().equals("all") || side.toLowerCase().equals("upper")) {
                        upper = true;
                    }
                }
                if (!lower) {
                    throw new AGDMException(AGDMException.AGDM008, "Axis " + coord + " not defined in the lower side.");
                }
                if (!upper) {
                    throw new AGDMException(AGDMException.AGDM008, "Axis " + coord + " not defined in the upper side.");
                }
            }
            //3
            for (int i = 0; i < coords.getLength(); i++) {
                String coord = coords.item(i).getTextContent();
                if (find(problem, "//mms:name[(parent::mms:region or parent::mms:subregion) and text() = '" + coord + "-Lower']").getLength() > 0) {
                    throw new AGDMException(AGDMException.AGDM008, "Region cannot have the same name like a boundary identifier. " 
                            + "Please, change " + coord + "-Lower region name.");
                }
                if (find(problem, "//mms:name[(parent::mms:region or parent::mms:subregion) and text() = '" + coord + "-Upper']").getLength() > 0) {
                    throw new AGDMException(AGDMException.AGDM008, "Region cannot have the same name like a boundary identifier. " 
                            + "Please, change " + coord + "-Upper region name.");
                }
            }
        }
        catch (Exception e) {
            throw new AGDMException(AGDMException.AGDM00X, e.toString());
        }
    }
   
    /**
     * Read a file as String.
     * 
     * @param is    The input stream of the file
     * @return      The resource as string
     */
    public static String convertStreamToString(InputStream is) {
        StringBuilder sb = null;
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } 
        catch (IOException e) {
            e.printStackTrace();
        } 
        finally {
            try {
                is.close();
            } 
            catch (IOException e) { 
                e.printStackTrace();
            }
        }
        return sb.toString();
    }
    /**
     * Read xml resource documents removing whitespaces.
     * 
     * @param path              The file name path
     * @return                  The resource as string
     * @throws AGDMException    AGDM007 - External error
     */
    public String readAsString(String path) throws AGDMException {
        try {
            DocumentBuilderFactory dFactory = DocumentBuilderFactory.newInstance();
            dFactory.setNamespaceAware(true);
            dFactory.setIgnoringElementContentWhitespace(true);
            dFactory.setIgnoringComments(true);
            DocumentBuilder dBuilder = dFactory.newDocumentBuilder();
            Document document = dBuilder.parse(getClass().getResourceAsStream(path)); 
            //Getting the root element
            Element root = document.getDocumentElement();
            //Remove the whitespaces
            removeWhitespaceNodes(root);
                      
            // Setup pretty print options
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

            // Return pretty print xml string
            StringWriter stringWriter = new StringWriter();
            transformer.transform(new DOMSource(document), new StreamResult(stringWriter));
            return stringWriter.toString();
        }
        catch (Exception e) {
            throw new AGDMException(AGDMException.AGDM00X, e.getMessage());
        }
    }
    
    /**
     * Checks if a string value is a number.
     * @param value     The string value
     * @return          True if number
     *                  False if not
     */
    public static boolean isNumber(String value) {
        try {
            Integer.parseInt(value);
            return true;
        }
        catch (NumberFormatException e) {
            return false;
        }
    }
    
    /**
     * Creates the mathML representing a field in a time slice with the spatial coordinates.
     * @param scope                 The fragment where to create the time slice
     * @param field                 The field to create it to
     * @param timeSlice             The time slice
     * @param sr                    The region relation with the fields and coordinates
     * @param coordRel              The coordinate relation
     * @throws AGDMException        AGDM007  External error
     */
    public static void createTimeSlice(Node scope, String field, Element timeSlice, RegionInfo sr, 
    		CoordinateInfo coordRel) throws AGDMException {
        Document doc = scope.getOwnerDocument();
        Element newSlice = createElement(doc, mtUri, "apply");
        Element var = newSlice; 
        if (timeSlice.getLocalName().equals("sharedVariable")) {
            newSlice = createElement(doc, simmlUri, "sharedVariable");
            var = createElement(doc, mtUri, "apply");
            var.appendChild(doc.importNode(timeSlice.getFirstChild().getFirstChild(), true));
            newSlice.appendChild(var);
        }
        else {
            var.appendChild(doc.importNode(timeSlice.getFirstChild(), true));   
        }
 
        ArrayList <String> fieldCoords = coordRel.getDiscCoords();
        for (int k = 0; k < fieldCoords.size(); k++) {
            Element ciCoord = createElement(doc, mtUri, "ci");
            ciCoord.setTextContent(fieldCoords.get(k));
            var.appendChild(ciCoord);
        }
        
        NodeList found = find(scope, ".//mt:ci[text() = '" + field + "']");
        for (int i = found.getLength() - 1; i >= 0; i--) {
            found.item(i).getParentNode().replaceChild(doc.importNode(newSlice, true), found.item(i));
        }
    }
    
    /**
     * Creates the mathML representing a field in a time slice with the spatial coordinates operated.
     * @param scope                 The fragment where to create the time slice
     * @param pi                    The process information
     * @param timeSlice             The time slice
     * @param coord                 The spatial coordinate to be decremented
     * @param value                 The value to be decremented
     * @param op                    The operation to be applied
     * @throws AGDMException        AGDM007  External error
     */
    public static void createTimeSliceOperated(Node scope, ProcessInfo pi, Element timeSlice, String coord, String op, int value) 
        throws AGDMException {
        Document doc = scope.getOwnerDocument();
        Element newSlice = createElement(doc, mtUri, "apply");
        Element var = newSlice; 
        if (timeSlice.getLocalName().equals("sharedVariable")) {
            newSlice = createElement(doc, simmlUri, "sharedVariable");
            var = createElement(doc, mtUri, "apply");
            var.appendChild(doc.importNode(timeSlice.getFirstChild().getFirstChild(), true));
            newSlice.appendChild(var);
        }
        else {
            var.appendChild(doc.importNode(timeSlice.getFirstChild(), true));   
        }
        ArrayList <String> fieldCoords = pi.getCoordinateRelation().getDiscCoords();
        for (int k = 0; k < fieldCoords.size(); k++) {
            Element ci = createElement(doc, mtUri, "ci");
            ci.setTextContent(fieldCoords.get(k));
            //Operate coordinate
            if (coord.equals(fieldCoords.get(k))) {
                Element apply2 = createElement(doc, mtUri, "apply");
                Element operationElem = createElement(doc, mtUri, op);
                apply2.appendChild(operationElem);
                apply2.appendChild(ci);
                //Adding component
                Element opValue = createElement(doc, mtUri, "cn");
                opValue.setTextContent(String.valueOf(value));
                apply2.appendChild(opValue);
                var.appendChild(apply2);
            }
            //Not operate the coordinate
            else {
                var.appendChild(ci.cloneNode(true));
            }
        }
        NodeList found = find(scope, ".//mt:ci[text() = '" + pi.getField() + "']");
        for (int i = 0; i < found.getLength(); i++) {
            found.item(i).getParentNode().replaceChild(doc.importNode(newSlice, true), found.item(i));
        }
    }
    
    /**
     * Creates the mathML representing a field in a time slice with the spatial coordinates operated.
     * @param pi                    The process information
     * @param timeSlice             The time slice
     * @param coord                 The spatial coordinate to be decremented
     * @param value                 The value to be decremented
     * @param op                    The operation to be applied
     * @throws AGDMException        AGDM007  External error
 * @return                      The element
 */
    public static Element createTimeSliceOperated(ProcessInfo pi, Node timeSlice, String coord, String op, int value) 
        throws AGDMException {
        Document doc = timeSlice.getOwnerDocument();
        Element newSlice = createElement(doc, mtUri, "apply");
        Element var = newSlice; 
        if (timeSlice.getLocalName().equals("sharedVariable")) {
            newSlice = createElement(doc, simmlUri, "sharedVariable");
            var = createElement(doc, mtUri, "apply");
            var.appendChild(doc.importNode(timeSlice.getFirstChild().getFirstChild(), true));
            newSlice.appendChild(var);
        }
        else {
            var.appendChild(doc.importNode(timeSlice.getFirstChild(), true));   
        }
        ArrayList <String> fieldCoords = pi.getCoordinateRelation().getDiscCoords();
        for (int k = 0; k < fieldCoords.size(); k++) {
            Element ci = createElement(doc, mtUri, "ci");
            ci.setTextContent(fieldCoords.get(k));
            //Operate coordinate
            if (coord.equals(fieldCoords.get(k))) {
                Element apply2 = createElement(doc, mtUri, "apply");
                Element operationElem = createElement(doc, mtUri, op);
                apply2.appendChild(operationElem);
                apply2.appendChild(ci);
                //Adding component
                Element opValue = createElement(doc, mtUri, "cn");
                opValue.setTextContent(String.valueOf(value));
                apply2.appendChild(opValue);
                var.appendChild(apply2);
            }
            //Not operate the coordinate
            else {
                var.appendChild(ci.cloneNode(true));
            }
        }
        return newSlice;
    }
    
    /**
     * Check if a given variable has coordinate access.
     * @param variable			The variable to check
     * @return					True if the variable is a step variable
     * @throws AGDMException	AGDM007 External error
     */
    public static boolean hasCoordinates(Node variable) throws AGDMException {
    	Element compareVariable = (Element) variable.cloneNode(true);
    	while (compareVariable.getLocalName().equals("neighbourParticle") || compareVariable.getLocalName().equals("sharedVariable")) {
    		compareVariable = (Element) compareVariable.getFirstChild();
    	}
    	if (compareVariable.getLocalName().equals("apply")) {
    		return true;
    	}
    	return false;
    }
    
    /**
     * Replaces all field occurrences in an expression by the equivalent variable.
     * @param pi                    The process information
     * @param variable              The variable template
     * @param scope                 The expression to check
     * @throws AGDMException        AGDM00X  External error
     */
    public static void replaceFieldVariables(ProcessInfo pi, Element variable, Element scope) throws AGDMException {
    	replaceFieldVariables(pi, variable, scope, variable);
    }
   
    /**
     * Replaces all field occurrences in an expression by the equivalent variables.
     * @param pi                    The process information
     * @param variable              The variable template for fields
     * @param scope                 The expression to check
     * @param currentVarTemplate    The variable template for auxiliary variables
     * @throws AGDMException        AGDM00X  External error
     */
    public static void replaceFieldVariables(ProcessInfo pi, Element variable, Element scope, Node currentVarTemplate) throws AGDMException {
    	boolean isStepVariable = pi.isStepVariable(variable, false);
 
        ArrayList<String> fields = new ArrayList<String>(pi.getRegionInfo().getAllFields());
        HashSet<String> interpFields = new HashSet<String>(pi.getOperatorsInfo().getVariablesToInterpolate());
        interpFields.addAll(pi.getOperatorsInfo().getAuxiliaryVariablesToInterpolate());
        fields.removeAll(interpFields);
    	Iterator<String> itVars = interpFields.iterator();
    	while (itVars.hasNext()) {
    		String toAdd = itVars.next();
    		if (pi.getDiscretizationType() == DiscretizationType.mesh) {
    			fields.add(toAdd + "_mesh");
    		}
    		if (pi.getDiscretizationType() == DiscretizationType.particles) {
    			fields.add(toAdd + "_particles");
    		}
    	}
    	Set<String> variables = pi.getAuxiliaryVariables();
        
    	NodeList cis = scope.getElementsByTagNameNS(mtUri, "ci");
    	HashMap<String, ArrayList<Element>> fieldOccurrences = new HashMap<String, ArrayList<Element>>();
    	HashMap<String, ArrayList<Element>> varOccurrences = new HashMap<String, ArrayList<Element>>();
    	for (int i = 0; i < cis.getLength(); i++) {
    		Element varElem = (Element) cis.item(i);
    		String var = varElem.getTextContent();
    		if (fields.contains(var)) {
    			ArrayList<Element> tmp = fieldOccurrences.getOrDefault(var, new ArrayList<Element>());
    			tmp.add(varElem);
    			fieldOccurrences.put(var, tmp);
    		}
    		if (variables.contains(var)) {
    			ArrayList<Element> tmp = varOccurrences.getOrDefault(var, new ArrayList<Element>());
    			tmp.add(varElem);
    			varOccurrences.put(var, tmp);
    		}
    	}
    	
        String  origField = pi.getField();
        boolean hasCoordinates = hasCoordinates(variable);
        boolean neighbourTag = variable.getLocalName().equals("neighbourParticle") || variable.getElementsByTagNameNS(simmlUri, "neighbourParticle").getLength() > 0;
        for (String field: fieldOccurrences.keySet()) {
            pi.setField(field);
            Element fieldTime;
            if (pi.getRegionInfo().isAuxiliaryField(field) && isStepVariable) {
            	if (hasCoordinates) {
                	fieldTime = ExecutionFlow.replaceTags(AGDMUtils.createTimeSliceWithExternalIndex(variable.getOwnerDocument(), variable.cloneNode(true)), pi);            		
            	}
            	else {
            		fieldTime = (Element) ExecutionFlow.replaceTags(variable.cloneNode(true), pi);
            	}
            	if (neighbourTag) {
            		Element neighbourParticle = createElement(fieldTime.getOwnerDocument(), simmlUri, "neighbourParticle");
            		neighbourParticle.appendChild(fieldTime);
            		fieldTime = neighbourParticle;
            	}
            }
            else {
            	Node template = variable.cloneNode(true);
            	//Check if the field is discretized with another schema and get the proper template
            	if (isStepVariable && !pi.getRegionInfo().getFields().contains(field)) {
            		Element tmpTemplate = pi.getCurrentFieldTemplate(field);
            		if (tmpTemplate != null) {
            			template = tmpTemplate.getFirstChild().cloneNode(true);
            			if (!hasCoordinates) {
            				if (template.getLocalName().equals("sharedVariable")) {
            					tmpTemplate = (Element) template.cloneNode(false);
            					template = template.getFirstChild().getFirstChild();
            					tmpTemplate.appendChild(template);
            					template = tmpTemplate;
            				}
            				else {
            					template = template.getFirstChild();
            				}
            			}
            		}
            	}
            	fieldTime = (Element) ExecutionFlow.replaceTags(template, pi);
            }
            for (Element e: fieldOccurrences.get(field)) {
            	AGDMUtils.replaceVariable(e, field, fieldTime);
            }   
        }
        
        if (varOccurrences.size() > 0) {
	        //Remove index access if any
	        Element template = (Element) currentVarTemplate;
	        boolean neighbour = false;
	        while (template.getLocalName().equals("neighbourParticle") || 
	        		template.getLocalName().equals("sharedVariable") || 
	        		template.getLocalName().equals("apply")) {
	        	if (template.getLocalName().equals("neighbourParticle")) {
	        		neighbour = true;
	        	}
	        	template = (Element) template.getFirstChild();
	        }
	        for (String var: varOccurrences.keySet()) {
	        	 pi.setField(var);
	        	if (neighbour) {
	        		 pi.setField(var + SchemaCreator.NEIGHBOURSUFFIX);
	        	}
	            Element fieldTime = (Element) ExecutionFlow.replaceTags(template.cloneNode(true), pi);
	            for (Element e: varOccurrences.get(var)) {
	            	AGDMUtils.replaceVariable(e, var, fieldTime);
	            }
	        }
        }
        pi.setField(origField);
    }
   
    
    /**
     * Removes the index in the variable.
     * @param var	The variable
     * @return		Variable without index
     */
    public static Node removeIndex(Node var) {
        Element template = (Element) var.cloneNode(true);
        if (template.getLocalName().equals("sharedVariable")) {
        	template = (Element) template.getFirstChild();
        }
        if (template.getLocalName().equals("apply")) {
        	template = (Element) template.getFirstChild();
        }
        return template;
    }
    
    /**
     * Replaces the variables with new ones.
     * @param scope                 The fragment where to replace the variable
     * @param var                   The variable to be replaced
     * @param variable              The variable to be used
     * @throws AGDMException        AGDM00X  External error
     */
    public static void replaceVariable(Node scope, String var, Element variable) 
        throws AGDMException {
        if (scope.getNodeType() == Node.DOCUMENT_FRAGMENT_NODE) {
        	NodeList nodes = scope.getChildNodes();
        	for (int i = 0; i < nodes.getLength(); i++) {
        		replaceVariable((Element) nodes.item(i), var, variable);
        	}
        }
        else {
        	replaceVariable((Element) scope, var, variable);
        }
    }
    
    /**
     * Replaces the variables with new ones.
     * @param scope                 The element where to replace the variable
     * @param var                   The variable to be replaced
     * @param variable              The variable to be used
     * @throws AGDMException        AGDM00X  External error
     */
    public static void replaceVariable(Element scope, String var, Element variable) {
    	Document doc = scope.getOwnerDocument();
    	Element importedVar = (Element) doc.importNode(variable, true);
        if (scope.getLocalName().equals("ci") && scope.getTextContent().equals(var)) {
            Node parent = scope.getParentNode();
            parent.replaceChild(importedVar.cloneNode(true), scope);
        }
        else {
	        NodeList found = scope.getElementsByTagNameNS(mtUri, "ci");
	        for (int i = found.getLength() - 1; i >= 0; i--) {
	        	Node item =found.item(i);
	        	if (item.getTextContent().equals(var)) {
	        		Node parent = item.getParentNode();
	        		parent.replaceChild(importedVar.cloneNode(true), item);
	        	}
	        }
        }
    }
    
    /**
     * Replaces text in a node.
     * @param scope                 The fragment where to create the time slice
     * @param oldText               The text to be removed
     * @param newText               The text to be used
     * @throws AGDMException        AGDM007  External error
     */
    public static void replaceText(Node scope, String oldText, String newText) 
        throws AGDMException {
        NodeList found = find(scope, ".//*[text() = '" + oldText + "']|.[self::*[text() = '" + oldText + "']]");
        for (int i = found.getLength() - 1; i >= 0; i--) {
            found.item(i).setTextContent(newText);
        }
    }
    
    /**
     * Return the mathml representing the coordinates deployed of a field.
     * 
     * @param doc       The document where the information is
     * @param pi        The process information for the equation
     * @return          The deployed coordinates
     */
    public static DocumentFragment currentCell(Document doc, ProcessInfo pi) {
        ArrayList<String> spatialCoords =  pi.getCoordinateRelation().getDiscCoords();
        DocumentFragment deploy = doc.createDocumentFragment();
        for (int i = 0; i < spatialCoords.size(); i++) {
            Element ci = createElement(doc, mtUri, "ci");
            ci.setTextContent(spatialCoords.get(i));
            deploy.appendChild(ci.cloneNode(true));
        }
        return deploy;
    }
    
    /**
     * Return the mathml representing the coordinates deployed of a field.
     * 
     * @param doc           The document where the information is
     * @param spatialCoords The spatial coordinates
     * @return              The deployed coordinates
     */
    public static DocumentFragment currentCell(Document doc, ArrayList<String> spatialCoords) {
        DocumentFragment deploy = doc.createDocumentFragment();
        for (int i = 0; i < spatialCoords.size(); i++) {
            Element ci = createElement(doc, mtUri, "ci");
            ci.setTextContent(spatialCoords.get(i));
            deploy.appendChild(ci.cloneNode(true));
        }
        return deploy;
    }
    
    /**
     * Return the mathml representing the coordinates deployed for a combination of interactions.
     * 
     * @param doc       The document where the information is
     * @param pi        The process information for the equation
     * @param comb      The combination
     * @return          The deployed coordinates
     */
    public static DocumentFragment deployCoordinateCombination(Document doc, ProcessInfo pi, String comb) {
        ArrayList<String> spatialCoords =pi.getCoordinateRelation().getDiscCoords();
        DocumentFragment deploy = doc.createDocumentFragment();
        for (int i = 0; i < spatialCoords.size(); i++) {
            if (comb.substring(i).startsWith("+")) {
                Element apply = createElement(doc, mtUri, "apply");
                Element op = createElement(doc, mtUri, "plus");
                Element ci = createElement(doc, mtUri, "ci");
                Element cn = createElement(doc, mtUri, "cn");
                ci.setTextContent(spatialCoords.get(i));
                cn.setTextContent("1");
                apply.appendChild(op);
                apply.appendChild(ci);
                apply.appendChild(cn);
                deploy.appendChild(apply.cloneNode(true));
            }
            if (comb.substring(i).startsWith("-")) {
                Element apply = createElement(doc, mtUri, "apply");
                Element op = createElement(doc, mtUri, "minus");
                Element ci = createElement(doc, mtUri, "ci");
                Element cn = createElement(doc, mtUri, "cn");
                ci.setTextContent(spatialCoords.get(i));
                cn.setTextContent("1");
                apply.appendChild(op);
                apply.appendChild(ci);
                apply.appendChild(cn);
                deploy.appendChild(apply.cloneNode(true));
            }
            if (comb.substring(i).startsWith("0")) {
                Element ci = createElement(doc, mtUri, "ci");
                ci.setTextContent(spatialCoords.get(i));
                deploy.appendChild(ci.cloneNode(true));
            }
        }
        return deploy;
    }
    
    /**
     * Combine two elements into a conditional operation.
     * @param elem1         Element 1
     * @param elem2         Element 2
     * @param operation     The operation "and" "or"
     * @return              The element operated
     */
    public static Element combineConditions(Element elem1, Element elem2, String operation) {
        Document doc = elem1.getOwnerDocument();
        Element math = createElement(doc, mtUri, "math");
        Element apply = createElement(doc, mtUri, "apply");
        Element op = createElement(doc, mtUri, operation);
        apply.appendChild(op);
        apply.appendChild(doc.importNode(elem1.getFirstChild(), true));
        apply.appendChild(doc.importNode(elem2.getFirstChild(), true));
        math.appendChild(apply);
        return math;
    }
    
    /**
     * Return the mathml representing the coordinates deployed of a field with one of them are operated by a value.
     * The two possibilities are plus or minus.
     * 
     * @param doc                   The document where the information is
     * @param pi                    The process information for the equation
     * @param spatialCoordinate     The spatial coordinate to be decremented
     * @param value                 The value to be decremented
     * @param operation             The operation to be applied
     * @return                      The deployed coordinates
     */
    public static DocumentFragment operateCoordinate(Document doc, ProcessInfo pi, String spatialCoordinate, String value, String operation) {
        //The result are stored in a document fragment
        DocumentFragment operate = doc.createDocumentFragment();
        
        //Get the spatial coordinates
        ArrayList<String> spatialCoords = pi.getCoordinateRelation().getDiscCoords();
        for (int j = 0; j < spatialCoords.size(); j++) {
            Element ci = createElement(doc, mtUri, "ci");
            ci.setTextContent(spatialCoords.get(j));
            //Operate coordinate
            if (spatialCoordinate.equals(spatialCoords.get(j))) {
                Element apply = createElement(doc, mtUri, "apply");
                Element operationElem = createElement(doc, mtUri, operation);
                apply.appendChild(operationElem);
                apply.appendChild(ci);
                //Adding component
                Element opValue;
                if (isNumber(value)) {
                    opValue = createElement(doc, mtUri, "cn");
                }
                else {
                    opValue = createElement(doc, mtUri, "ci");
                }
                opValue.setTextContent(value);
                apply.appendChild(opValue);
                operate.appendChild(apply);
            }
            //Not operate the coordinate
            else {
                operate.appendChild(ci.cloneNode(true));
            }
        }
        return operate;
    }
    
    /**
     * Return the mathml representing the coordinates deployed of a field with both of them are operated by a value.
     * The two possibilities are plus or minus.
     * 
     * @param doc                   The document where the information is
     * @param pi                    The process information for the equation
     * @param spatialCoordinate     The spatial coordinate to be decremented
     * @param spatialCoordinate2    The second spatial coordinate (Only in parabolic terms)
     * @param value                 The value to be decremented
     * @param operation             The operation to be applied
     * @param operation2            The second operation to be applied
     * @return                      The deployed coordinates
     */
    public static DocumentFragment operateCoordinate(Document doc, ProcessInfo pi, 
            String spatialCoordinate, String spatialCoordinate2,  String value, String operation, String operation2) {
        //The result are stored in a document fragmente
        DocumentFragment operate = doc.createDocumentFragment();
        
        String[] values = value.split(",");
        
        //Get the spatial coordinates
        ArrayList<String> spatialCoords = pi.getCoordinateRelation().getDiscCoords();
        for (int j = 0; j < spatialCoords.size(); j++) {
            Element ci = createElement(doc, mtUri, "ci");
            ci.setTextContent(spatialCoords.get(j));
            //Operate coordinate
            if (spatialCoordinate.equals(spatialCoords.get(j))) {
                Element apply = createElement(doc, mtUri, "apply");
                Element operationElem = createElement(doc, mtUri, operation);
                apply.appendChild(operationElem);
                apply.appendChild(ci);
                //Adding component
                Element opValue;
                if (isNumber(values[0])) {
                    opValue = createElement(doc, mtUri, "cn");
                }
                else {
                    opValue = createElement(doc, mtUri, "ci");
                }
                opValue.setTextContent(values[0]);
                apply.appendChild(opValue);
                operate.appendChild(apply);
            }
            else {
                //Operate coordinate 2
                if (spatialCoordinate2.equals(spatialCoords.get(j))) {
                    Element apply = createElement(doc, mtUri, "apply");
                    Element operationElem = createElement(doc, mtUri, operation2);
                    apply.appendChild(operationElem);
                    apply.appendChild(ci);
                    //Adding component
                    Element opValue;
                    if (isNumber(values[1])) {
                        opValue = createElement(doc, mtUri, "cn");
                    }
                    else {
                        opValue = createElement(doc, mtUri, "ci");
                    }
                    opValue.setTextContent(values[1]);
                    apply.appendChild(opValue);
                    operate.appendChild(apply);
                }
                //Not operate the coordinate
                else {
                    operate.appendChild(ci.cloneNode(true));
                }
            }
        }
        return operate;
    }
    
    /**
     * Gets the base spatial coordinates of the problem.
     * @param problem           The problem
     * @return                  The list of the coordinates
     * @throws AGDMException    AGDM007  External error
     */
    public static ArrayList<String> getBaseDiscSpatialCoordinate(Document problem)
        throws AGDMException {
        ArrayList<String> coords = new ArrayList<String>();
        NodeList coordList = find(problem, "//mms:spatialCoordinates/mms:spatialCoordinate");
        for (int i = 0; i < coordList.getLength(); i++) {
            coords.add(coordList.item(i).getTextContent());
        }
        
        return coords;
    }
    
    /**
     * Previous validations to check the correction of the schema.
     * @param schema            Discretization schema
     * @throws AGDMException    AGDM002 XML discretization schema not valid
     */
    public static void previousValidations(Document schema) throws AGDMException {
        if (find(schema, 
                "//sml:neighbourParticle[not(ancestor::sml:iterateOverInteractions) and not(ancestor::sml:regionInteraction)]").getLength() > 0) {
            throw new AGDMException(AGDMException.AGDM002, 
                    "The tag \"neighbourParticle\" must be placed within a \"sml:iterateOverInteractions\" or \"regionInteraction\" tag");
        }
        if (find(schema, 
                "//sml:particleDistance[not(ancestor::sml:iterateOverInteractions) and not(ancestor::sml:regionInteraction)]").getLength() > 0) {
            throw new AGDMException(AGDMException.AGDM002, 
                    "The tag \"particleDistance\" must be placed within a \"sml:iterateOverInteractions\" or \"regionInteraction\" tag");
        }
    }
    
    /**
     * Previous processes for the schema. 
     * 0 - Import schema presteps
     * 1 - Replace field variables with field variables at the axis when the left term is evaluated at the axis (Extrapolation purposes).
     * 2 - Add schema sufix to the variables in post initialization. 
     * @param schema            Discretization schema
     * @param sufix				Schema sufix
     * @param extrapolation     True if the problem have extrapolation
     * @throws AGDMException    AGDM007  External error
     */
    public static void schemaPreprocess(Document schema, String sufix, boolean extrapolation) 
            throws AGDMException {
    	//0 - Import schema presteps
    	NodeList presteps = AGDMUtils.find(schema, ".//mms:meshPreStep|.//mms:particlePreStep");
    	for (int i = presteps.getLength() - 1; i >= 0; i--) {
    		//Read prestep variables
    		Element prestepElement = (Element) presteps.item(i);
    		String schemaId = prestepElement.getFirstChild().getLastChild().getTextContent();
            NodeList inputVariables = prestepElement.getElementsByTagNameNS(AGDMUtils.mmsUri, "inputVariable");
            NodeList outputVariables = prestepElement.getElementsByTagNameNS(AGDMUtils.mmsUri, "outputVariable");
    		//Load prestep
            String prestepRule = "";
            
            try {
                DocumentManager docMan = new DocumentManagerImpl();
                prestepRule = SimflownyUtils.jsonToXML(docMan.getDocument(schemaId));
            } 
            catch (SUException e) {
                throw new AGDMException(AGDMException.AGDM002, "Spatial operator discretization with id " + schemaId + " not valid.");
            }
            catch (DMException e) {
                throw new AGDMException(AGDMException.AGDM003, "Spatial operator discretization with id " + schemaId + " does not exist.");
            }
            Element importSchema = AGDMUtils.stringToDom(prestepRule).getDocumentElement();
            Element code = (Element) importSchema.getElementsByTagNameNS(AGDMUtils.simmlUri, "simml").item(0).cloneNode(true);
            NodeList ruleInputVariables = importSchema.getElementsByTagNameNS(AGDMUtils.mmsUri, "inputVariable");
            NodeList ruleOutputVariables = importSchema.getElementsByTagNameNS(AGDMUtils.mmsUri, "outputVariable");
            //Check parameters
            if (inputVariables.getLength() != ruleInputVariables.getLength() 
            		|| outputVariables.getLength() != ruleOutputVariables.getLength()) {
                throw new AGDMException(AGDMException.AGDM002, "The number of prestep schema variables must fit prestep transformation rule variable number.");
            }
    		//Replace variables
            for (int j = 0; j < inputVariables.getLength(); j++) {
            	AGDMUtils.replaceTransformationRuleExternalVariable(code, inputVariables.item(j).getFirstChild(), ruleInputVariables.item(j).getFirstChild());
            }
            for (int j = 0; j < outputVariables.getLength(); j++) {
            	AGDMUtils.replaceTransformationRuleExternalVariable(code, outputVariables.item(j).getFirstChild(), ruleOutputVariables.item(j).getFirstChild());
            }
    		
    		Element newPrestep = (Element) prestepElement.cloneNode(false);
    		newPrestep.appendChild(schema.importNode(code, true));
    		Element step = (Element) prestepElement.getParentNode();
    		step.replaceChild(newPrestep, prestepElement);
    	}
    	
        //1 - Replace field variables with field variables at the axis when the left term is evaluated at the axis (Extrapolation purposes).
        if (extrapolation && ProcessInfo.getNumberOfRegions() > 1) {
            Element extrapolVar = getExtrapolationVariable(schema, false, false);
            NodeList boundaryVariables = find(schema, "//mms:timeDiscretization/mms:outputVariable");
            for (int i = 0; i < boundaryVariables.getLength(); i++) {
                Element variable = (Element) boundaryVariables.item(i).getFirstChild().getFirstChild().getFirstChild();
                Element newVar = (Element) extrapolVar.cloneNode(true);
                boolean isSharedVar = ((Element) boundaryVariables.item(i).getFirstChild().getFirstChild()).getLocalName().equals("sharedVariable");
                if (isSharedVar) {
                    newVar = (Element) variable.cloneNode(true);
                    newVar.replaceChild(extrapolVar.cloneNode(true), newVar.getFirstChild());
                    variable = (Element) variable.getParentNode();
                }
                NodeList maths = find(schema, "//sml:iterateOverCells//mt:math[descendant::sml:spatialCoordinate]|//mms:conservativeTermDiscretization//mms:inputVariable//mt:math");
                for (int j = maths.getLength() - 1; j >= 0; j--) {
                    nodeReplace(maths.item(j), variable, newVar);
                    if (isSharedVar) {
                        Element variable2 = createElement(schema, simmlUri, "sharedVariable");
                        variable2.appendChild(
                                boundaryVariables.item(i).getFirstChild().getFirstChild().getFirstChild().getFirstChild().cloneNode(true));
                        nodeReplace(maths.item(j), variable2, extrapolVar.cloneNode(true));
                    }
                }
            }
            //Replace first timestep _p variables too
            Element timeSlice = (Element) AGDMUtils.createPreviousTimeSlice(schema).getFirstChild();
            NodeList maths = find(schema, "//sml:iterateOverCells//mt:math[descendant::sml:spatialCoordinate]|//mms:conservativeTermDiscretization//mms:inputVariable//mt:math");
            for (int j = maths.getLength() - 1; j >= 0; j--) {
                nodeReplace(maths.item(j), timeSlice, extrapolVar.cloneNode(true));
            }
        }
        //2 - Add schema sufix to the variables in post initialization. 
        String query = "//mms:postInitialization//mt:math[(not(parent::sml:if) or parent::sml:then) and (not(parent::sml:while) or parent::sml:loop)"
            + " and not(parent::sml:return)]/mt:apply/mt:eq[not(following-sibling::*[1] = preceding-sibling::mt:math/mt:apply/mt:eq/" 
            + "following-sibling::*[1])]/following-sibling::*[1]";
        NodeList terms = find(schema, query);
        for (int i = terms.getLength() - 1; i >= 0; i--) {
            Element originalCi;
            if (terms.item(i).getLocalName().equals("apply")) {
                originalCi = (Element) terms.item(i).getFirstChild().cloneNode(true);
            }
            else {
                originalCi = (Element) terms.item(i).cloneNode(true);
            }
            
            //Create the new variable adding the region number at the end of the variable name
            Element newCi = (Element) originalCi.cloneNode(true);
            NodeList cis = find(newCi, "self::*[self::mt:ci[not(sml:timeCoordinate)]]|.//mt:ci[not(sml:timeCoordinate)]");
            boolean added = false;
            for (int k = cis.getLength() - 1; k >= 0 && !added; k--) {
                if (!cis.item(k).getTextContent().startsWith("(")) {
                    cis.item(k).setTextContent(cis.item(k).getTextContent() + "_" + sufix);
                    added = true;
                }
            }
            if (added) {
                //Replace all the occurrences of the variable by the new name in the interior execution flow
                nodeReplace(schema.getDocumentElement(), originalCi, newCi);
            }
        }
    }
    
    /**
     * Gets the extrapolation variable.
     * @param doc               The base document for the xml
     * @param withCurrentCell  Add the currentCell if true
     * @param flux              True for fluxes, false for fields
     * @return                  The xml variable structure
     */
    public static Element getExtrapolationVariable(Document doc, boolean withCurrentCell, boolean flux) {
        Element extrapolVar = createElement(doc, mtUri, "ci");
        Element msubsup = createElement(doc, mtUri, "msubsup");
        Element ext = createElement(doc, mtUri, "ci");
        Element fieldCi = createElement(doc, mtUri, "ci");
        Element coordCi = createElement(doc, mtUri, "ci");
        Element field = createElement(doc, simmlUri, "field");
        Element coord = createElement(doc, simmlUri, "spatialCoordinate");
        if (flux) {
            ext.setTextContent("Flux_ext");
        }
        else {
            ext.setTextContent("extrapolated");
        }
        extrapolVar.appendChild(msubsup);
        msubsup.appendChild(ext);
        msubsup.appendChild(fieldCi);
        msubsup.appendChild(coordCi);
        fieldCi.appendChild(field);
        coordCi.appendChild(coord);
        if (!withCurrentCell) {
            return extrapolVar;
        } 
        else {
            Element apply = createElement(doc, mtUri, "apply");
            Element coords = createElement(doc, simmlUri, "currentCell");
            apply.appendChild(extrapolVar);
            apply.appendChild(coords);
            return apply;
        }
    }
    
    /**
     * Checks if an instruction is invalid.
     * @param rule                  The instruction to check
     * @throws AGDMException        AGDM002 XML discretization schema not valid
     *                              AGDM00X  External error
     */
    public static void checkIncorrectInstruction(Element rule) throws AGDMException {
        //The result are stored in a document fragment
        String query = "./mt:apply[(parent::mt:math[(not(parent::sml:if) or parent::sml:then) and (not(parent::sml:while) or parent::sml:loop) " 
            + "and not(parent::sml:return) and not(ancestor::mms:applyIf) and not(ancestor::" 
            + "mms:finalizationCondition)])]/mt:eq/following-sibling::*[1][(name()='mt:apply' or name()='mt:ci' or name()='sml:sharedVariable') and " 
            + "(descendant::sml:field) and (descendant::sml:eigenVector)]";
        if (AGDMUtils.find(rule, query).getLength() > 0) {
            throw new AGDMException(AGDMException.AGDM002, 
                    "There is an instruction in the schema that contains a field and eigenVector tag at the same time.");
        }
    }
    
    /**
     * Check if an variable is a field.
     * @param doc                   The problem
     * @param var                   The variable
     * @return                      True if it is a field
     * @throws AGDMException        AGDM00X  External error
     */
    public static boolean isField(Document doc, String var) throws AGDMException {
        String query = "/*/mms:fields/mms:field[text() = '" + var + "']|/*/mms:auxiliarFields/mms:auxiliarField[text() = '" + var + "']";
        if (AGDMUtils.find(doc, query).getLength() > 0) {
            return true;
        }
        return false;
    }
    
    /**
     * Check if an instruction uses eigenVector tag.
     * @param rule                  The instruction to check
     * @return                      True if the rule uses it
     * @throws AGDMException        AGDM00X  External error
     */
    public static boolean isVectorInstruction(Element rule) throws AGDMException {
        String query = "./mt:apply[(parent::mt:math[(not(parent::sml:if) or parent::sml:then) and (not(parent::sml:while) or parent::sml:loop) " 
                + "and not(parent::sml:return) and not(ancestor::mms:applyIf) and not(ancestor::"
                + "mms:finalizationCondition)])]/mt:eq/following-sibling::*[1][(name()='mt:apply' or name()='mt:ci' or name()='sml:sharedVariable')"
                + " and (descendant::sml:eigenVector)]";
        if (AGDMUtils.find(rule, query).getLength() > 0) {
            return true;
        }
        return false;
    }
    
    /**
     * Create the instruction for the eigen vector from its coefficients.
     * @param node                  The eigen vector or undiagonalization info
     * @param variable              The field variable
     * @param timeSlice             The field timeSlice
     * @param pi                    The process info
     * @param deployCoords          True if the coordinates must be deployed
     * @param useTmp                Use temporal variable
     * @param undiag                If the coefficient is for an undiagonalization
     * @param tensorVar             If the variable is a tensor
     * @return                      The mathml
     * @throws AGDMException        AGDM006 Schema not applicable to this problem
     *                              AGDM007 External error
     */
    public static Element createCoefficientInstruction(Element node, Node variable, Node timeSlice, ProcessInfo pi, boolean deployCoords, 
            boolean useTmp, boolean undiag, boolean tensorVar) throws AGDMException {
        Document doc = variable.getOwnerDocument();
        Element result = null;
        ArrayList <String> fieldCoords = pi.getCoordinateRelation().getDiscCoords();
        //The first children is the vector name
        NodeList coefficients;
        if (undiag) {
            coefficients = node.getElementsByTagName("mms:undiagonalizationCoefficients").item(0).getChildNodes();
        }
        else {
        	
        	if (node.getElementsByTagName("mms:diagonalizationCoefficients").getLength() > 0) {
        		coefficients = node.getElementsByTagName("mms:diagonalizationCoefficients").item(0).getChildNodes();
        	}
        	//Tems instead of coefficients if coming from field interaction
        	else {
        		coefficients = node.getElementsByTagName("mms:terms").item(0).getChildNodes();
        	}
        }
        for (int i = 0; i < coefficients.getLength(); i++) {
            Element coefficient = (Element) coefficients.item(i);
            
            Element term = createElement(doc, mtUri, "apply");
            Element times = createElement(doc, mtUri, "times");
            if (coefficient.getChildNodes().getLength() > 1) {
                String fieldName = coefficient.getFirstChild().getTextContent();
                Element field;
                if (deployCoords && (undiag || AGDMUtils.isField(pi.getCompleteProblem(), fieldName))) {
                    field = createElement(doc, mtUri, "apply");
                    ProcessInfo newPi = new ProcessInfo(pi);
                    newPi.setField(fieldName);
                    
                    Element slice = null;
                    if (useTmp) {
                        slice = (Element) replaceTmp(ExecutionFlow.replaceTags(variable.cloneNode(true), newPi));
                    }
                    else {
                        slice = ExecutionFlow.replaceTags(variable.cloneNode(true), newPi);
                    }
                    field.appendChild(doc.importNode(slice.getFirstChild(), true));
                    for (int k = 0; k < fieldCoords.size(); k++) {
                        Element ciCoord = createElement(doc, mtUri, "ci");
                        ciCoord.setTextContent(fieldCoords.get(k));
                        field.appendChild(ciCoord);
                    }
                }
                else {
                    ProcessInfo newPi = new ProcessInfo(pi);
                    newPi.setField(fieldName);
                    if (undiag || AGDMUtils.isField(pi.getCompleteProblem(), fieldName)) {
                        if (useTmp) {
                            field = (Element) replaceTmp(
                                    ExecutionFlow.replaceTags(variable.cloneNode(true), newPi)).getFirstChild();
                        }
                        else {
                            field = (Element) 
                                    ExecutionFlow.replaceTags(variable.cloneNode(true), newPi).getFirstChild();
                        }
                    }
                    //tensor variables do not use variable
                    else {
                        field = createElement(doc, mtUri, "ci");
                        field.setTextContent(fieldName + "_var");
                    }
                }
                
                //Apply TimeSlice  to the coefficient
                Element coefficientE = (Element) coefficient.getLastChild().cloneNode(true);
                replaceFieldVariables(pi, (Element) timeSlice.getFirstChild(), coefficientE);
                if (tensorVar) {
                    ArrayList<Tensor> tensors = new ArrayList<Tensor>(pi.getTensors());
                    for (int k = 0; k < tensors.size(); k++) {
                        String name = tensors.get(k).getTensorName();
                        Element tVar = createElement(doc, mtUri, "ci");
                        tVar.setTextContent(name + "_n_var");
                        AGDMUtils.replaceVariable(coefficientE, name + "_n", tVar);
                        for (int l = 0; l < pi.getBaseSpatialCoordinates().size(); l++) {
                            tVar = createElement(doc, mtUri, "ci");
                            tVar.setTextContent(name + "_t" + pi.getBaseSpatialCoordinates().get(l) + "_var");
                            AGDMUtils.replaceVariable(coefficientE, name + "_t" + pi.getBaseSpatialCoordinates().get(l), tVar);
                            
                        }
                    }
                }
 
                term.appendChild(times);
                term.appendChild(doc.importNode(field, true));
                term.appendChild(doc.importNode(coefficientE.getFirstChild(), true));
                //Simplification if coefficient is 1.
                if (coefficient.getLastChild().getFirstChild().getLocalName().equals("cn") 
                        && coefficient.getLastChild().getFirstChild().getTextContent().equals("1")) {
                    term = (Element) doc.importNode(field, true);
                }
                //Simplification if coefficient is 1.
                Element simp = createElement(doc, mtUri, "apply");
                Element minus = createElement(doc, mtUri, "minus");
                Element one = createElement(doc, mtUri, "cn");
                one.setTextContent("1");
                simp.appendChild(minus);
                simp.appendChild(one);
                if (coefficient.getLastChild().getFirstChild().isEqualNode(simp)) {
                    simp.replaceChild(doc.importNode(field, true), one);
                    term = simp;
                }
                if (i == 0) {
                    result = term;
                }
                else {
                    Element apply = createElement(doc, mtUri, "apply");
                    Element plus = createElement(doc, mtUri, "plus");
                    apply.appendChild(plus);
                    apply.appendChild(doc.importNode(result, true));
                    apply.appendChild(doc.importNode(term, true));
                    result = apply;
                }
            }
            //Free coeficient
            else {
                //Apply TimeSlice  to the coefficient
                Element coefficientE = (Element) coefficient.getFirstChild().cloneNode(true);
                replaceFieldVariables(pi, (Element) timeSlice.getFirstChild(), coefficientE);
                
                term = (Element) doc.importNode(coefficientE.getFirstChild(), true);
                if (i == 0) {
                    result = term;
                }
                else {
                    Element apply = createElement(doc, mtUri, "apply");
                    Element plus = createElement(doc, mtUri, "plus");
                    apply.appendChild(plus);
                    apply.appendChild(doc.importNode(result, true));
                    apply.appendChild(doc.importNode(term, true));
                    result = apply;
                }
            }
        }
        return result;
    }
    
    /**
     * Create the instruction for the eigen vector from its coefficients with operated coordinates.
     * @param vectorTag             The eigen vector info
     * @param timeSlice             The time slice
     * @param pi                    The process info
     * @param coord                 The coordinate to operate
     * @param op                    The operation to perform
     * @param value                 The value to operate
     * @param tensorVar             If the variable is a tensor
     * @return                      The mathml
     * @throws AGDMException        AGDM006 Schema not applicable to this problem
     *                              AGDM007 External error
     */
    public static Element createVectorInstructionOperated(Element vectorTag, Node timeSlice, ProcessInfo pi, String coord, String op, 
            int value, boolean tensorVar) throws AGDMException {
        Document doc = timeSlice.getOwnerDocument();
        Element result = null;
        ArrayList <String> fieldCoords = pi.getCoordinateRelation().getDiscCoords();
        //The first children is the vector name
        NodeList coefficients = vectorTag.getElementsByTagName("mms:diagonalizationCoefficients").item(0).getChildNodes();
        for (int i = 0; i < coefficients.getLength(); i++) {
            Element coefficient = (Element) coefficients.item(i);
            Element term = createElement(doc, mtUri, "apply");
            Element times = createElement(doc, mtUri, "times");
            Element field = createElement(doc, mtUri, "apply");
            ProcessInfo newPi = new ProcessInfo(pi);
            newPi.setField(coefficient.getFirstChild().getTextContent());
            if (AGDMUtils.isField(pi.getCompleteProblem(), coefficient.getFirstChild().getTextContent())) {
                Element slice = ExecutionFlow.replaceTags(timeSlice.cloneNode(true), newPi);
                field.appendChild(doc.importNode(slice.getFirstChild(), true));
                for (int k = 0; k < fieldCoords.size(); k++) {
                    Element ci = createElement(doc, mtUri, "ci");
                    ci.setTextContent(fieldCoords.get(k));
                    //Operate coordinate
                    if (coord.equals(fieldCoords.get(k))) {
                        Element apply2 = createElement(doc, mtUri, "apply");
                        Element operationElem = createElement(doc, mtUri, op);
                        apply2.appendChild(operationElem);
                        apply2.appendChild(ci);
                        //Adding component
                        Element opValue = createElement(doc, mtUri, "cn");
                        opValue.setTextContent(String.valueOf(value));
                        apply2.appendChild(opValue);
                        field.appendChild(apply2);
                    }
                    //Not operate the coordinate
                    else {
                        field.appendChild(ci.cloneNode(true));
                    }
                }
            }
            else {
                field = createElement(doc, mtUri, "ci");
                field.setTextContent(coefficient.getFirstChild().getTextContent() + "_var");
            }
            
            //ApplyTimeSlice to the coefficient
            Element coefficientE = (Element) coefficient.getLastChild().cloneNode(true);
            ArrayList<String> vars = new ArrayList<String>(pi.getRegionInfo().getFields());
            for (int k = 0; k < vars.size(); k++) {
                newPi = new ProcessInfo(pi);
                newPi.setField(vars.get(k));
                Element slice = ExecutionFlow.replaceTags(timeSlice.cloneNode(true), newPi);
                Element fieldTime = createTimeSliceOperated(newPi, slice, coord, op, value);
                AGDMUtils.replaceVariable(coefficientE, vars.get(k), fieldTime);
            }
            if (tensorVar) {
                ArrayList<Tensor> tensors = new ArrayList<Tensor>(pi.getTensors());
                for (int k = 0; k < tensors.size(); k++) {
                    String name = tensors.get(k).getTensorName();
                    Element tVar = createElement(doc, mtUri, "ci");
                    tVar.setTextContent(name + "_n_var");
                    AGDMUtils.replaceVariable(coefficientE, name + "_n", tVar);
                    for (int l = 0; l < pi.getBaseSpatialCoordinates().size(); l++) {
                        tVar = createElement(doc, mtUri, "ci");
                        tVar.setTextContent(name + "_t" + pi.getBaseSpatialCoordinates().get(l) + "_var");
                        AGDMUtils.replaceVariable(coefficientE, name + "_t" + pi.getBaseSpatialCoordinates().get(l), tVar);
                        
                    }
                }
            }
            
            term.appendChild(times);
            term.appendChild(field);
            term.appendChild(doc.importNode(coefficientE.getFirstChild(), true));
            //Simplification if coefficient is 1.
            if (coefficient.getLastChild().getFirstChild().getLocalName().equals("cn") 
                    && coefficient.getLastChild().getFirstChild().getTextContent().equals("1")) {
                term = field;
            }
            //Simplification if coefficient is -1.
            Element simp = createElement(doc, mtUri, "apply");
            Element minus = createElement(doc, mtUri, "minus");
            Element one = createElement(doc, mtUri, "cn");
            one.setTextContent("1");
            simp.appendChild(minus);
            simp.appendChild(one);
            if (coefficient.getLastChild().getFirstChild().isEqualNode(simp)) {
                simp.replaceChild(field, one);
                term = simp;
            }
            
            if (i == 0) {
                result = term;
            }
            else {
                Element apply = createElement(doc, mtUri, "apply");
                Element plus = createElement(doc, mtUri, "plus");
                apply.appendChild(plus);
                apply.appendChild(result);
                apply.appendChild(term);
                result = apply;
            }
        }
        return result;
    }
    
    /**
     * Deploys the auxiliary definitions for the characteristic information depending the tag in PDE.
     * @param charDecomposition        The characteristic information
     * @param tagId                     The id tag (0 = diagonalization, 
     *                                              1 = undiagonalization, 
     *                                              2 = boundaries, 
     *                                              3 = regionInteraction, 
     *                                              4 = maxCharSpeed
     *                                              5 = maxCharSpeedCoord)
     * @param pi                        The process information
     * @param timeSlice                 The timeSlice to be used on the fields
     * @param tensorVar             	If the variable is a tensor
     * @return                          The instructions
     * @throws AGDMException            AGDM006 Schema not applicable to this problem
     *                                  AGDM007 External error
     */
    public static DocumentFragment deployAuxiliaryDefinitions(Document discProblem, Node problem, Node charDecomposition, int tagId, ProcessInfo pi, Node timeSlice, 
            boolean tensorVar)
        throws AGDMException {
        DocumentFragment result = charDecomposition.getOwnerDocument().createDocumentFragment();
                
        //Get used variables
        String query = "";
        if (tagId == 0) {
            query = ".//mms:eigenSpace[ancestor::mms:coordinateCharacteristicDecomposition]//mms:eigenVector";
        }
        if (tagId == 1) {
            query = ".//mms:undiagonalization[ancestor::mms:coordinateCharacteristicDecomposition]";
        }
        if (tagId == 2) {
            query = ".//mms:eigenSpace|.//mms:undiagonalization";
        }
        if (tagId == THREE) {
            query = ".//mms:eigenSpace[ancestor::mms:generalCharacteristicDecomposition]|.//mms:undiagonalization[ancestor::mms:generalCharacteristicDecomposition]";
        }
        if (tagId == FOUR || tagId == FIVE) {
            if (((Element) charDecomposition).getElementsByTagName("mms:maxCharacteristicSpeed").getLength() > 0 && tagId == FOUR) {
                query = ".//mms:maxCharacteristicSpeed";    
            }
            else {
                if (find(charDecomposition, ".//mms:eigenValue[ancestor::mms:coordinateCharacteristicDecomposition]").getLength() > 0) {
                    query = ".//mms:eigenValue[ancestor::mms:coordinateCharacteristicDecomposition]";    
                }
                else {
                    query = ".//mms:eigenValue[ancestor::mms:generalCharacteristicDecomposition]";   
                }  
            }
        }
        HashSet<String> usedVariables = getUsedVars(charDecomposition, query);
        //Filter auxiliary variables needed
    	OperatorsInfo opInfoCharDecomp = new OperatorsInfo(pi.getOperatorsInfo());
    	opInfoCharDecomp.onlyDependent(usedVariables, true, null);
    	if (opInfoCharDecomp.hasDerivatives(OperatorInfoType.auxiliaryVariable)) {
            throw new AGDMException(AGDMException.AGDM005, "Auxiliary variables with derivatives cannot be used in characteristic decomposition.");
    	}
    	//Create equations
    	DocumentFragment auxEquation = SchemaCreator.createAuxiliaryVariableEquations(discProblem, problem, opInfoCharDecomp, pi.getOperatorsInfo().getDerivativeLevels(), pi.getOperatorsInfo().getMultiplicationLevels(), opInfoCharDecomp.getDerivativeLevels(), opInfoCharDecomp.getMultiplicationLevels(), pi, (Element) timeSlice.cloneNode(true), 0, false);
    	//Replace tensors
        if (tensorVar) {
            ArrayList<Tensor> tensors = new ArrayList<Tensor>(pi.getTensors());
            for (int k = 0; k < tensors.size(); k++) {
                String name = tensors.get(k).getTensorName();
                Element tVar = createElement(charDecomposition.getOwnerDocument(), mtUri, "ci");
                tVar.setTextContent(name + "_n_var");
                AGDMUtils.replaceVariable(auxEquation, name + "_n", tVar);
                for (int l = 0; l < pi.getBaseSpatialCoordinates().size(); l++) {
                    tVar = createElement(charDecomposition.getOwnerDocument(), mtUri, "ci");
                    tVar.setTextContent(name + "_t" + pi.getBaseSpatialCoordinates().get(l) + "_var");
                    AGDMUtils.replaceVariable(auxEquation, name + "_t" + pi.getBaseSpatialCoordinates().get(l), tVar);       
                }
            }
        }
    	result.appendChild(auxEquation);
        
        return result;
    }
    
    /**
     * Create the instructions to project vectors before doing a diagonalization.
     * @param charDecomposition         The characteristic decomposition
     * @param variable                  The field variable
     * @param timeSlice                 The field timeslice
     * @param pi                        The process information
     * @param boundary                  If the projection is within a boundary
     * @return                          The instructions
     * @throws AGDMException            AGDM006 Schema not applicable to this problem
     *                                  AGDM00X External error
     */
    public static DocumentFragment projectVectors(Node charDecomposition, Node variable, Node timeSlice, ProcessInfo pi, boolean boundary) 
            throws AGDMException {
        Document doc = charDecomposition.getOwnerDocument();
        DocumentFragment result = doc.createDocumentFragment();
        ArrayList<String> coords = pi.getBaseSpatialCoordinates();
        ArrayList<Tensor> tensors = pi.getTensors();
        for (int i = 0; i < tensors.size(); i++) {
            Tensor tensor = tensors.get(i);
            String tensorName = tensor.getTensorName();
            if (tensor.getOrder() == 1) {
                if (coords.size() != tensor.getFields().size()) {
                    throw new AGDMException(AGDMException.AGDM005, 
                            "The problem is incorrect. The vector " + tensorName + " must have " + coords.size() + " components");
                }
                if (find(charDecomposition, ".//mt:ci[text() = '" + tensorName + "_n']|.//mms:field[text() = '" + tensorName 
                        + "_n']").getLength() > 0) {
                    //With variable
                    //Normal
                    Element normalVar = createElement(doc, mtUri, "math");
                    Element apply = createElement(doc, mtUri, "apply");
                    Element eq = createElement(doc, mtUri, "eq");
                    ProcessInfo newPi = new ProcessInfo(pi);
                    Element ci = createElement(doc, mtUri, "ci");
                    ci.setTextContent(tensorName + "_n_var");
                    Element applyPlus = createElement(doc, mtUri, "apply");
                    Element plus = createElement(doc, mtUri, "plus");
                    applyPlus.appendChild(plus);
                    ArrayList<String> fields = tensor.getFields();
                    for (int j = 0; j < fields.size(); j++) {
                        Element applyTimes = createElement(doc, mtUri, "apply");
                        Element times = createElement(doc, mtUri, "times");
                        newPi.setField(fields.get(j));
                        Element fieldTime = ExecutionFlow.replaceTags(variable.cloneNode(true), newPi);
                        Element normal = createElement(doc, mtUri, "ci");
                        normal.setTextContent("n_" + coords.get(j));
                        applyTimes.appendChild(times);
                        applyTimes.appendChild(doc.importNode(fieldTime, true));
                        applyTimes.appendChild(normal);
                        applyPlus.appendChild(applyTimes);
                    }
                    normalVar.appendChild(apply);
                    apply.appendChild(eq);
                    apply.appendChild(doc.importNode(ci, true));
                    apply.appendChild(applyPlus);
                    result.appendChild(normalVar);
                    //Transversal
                    for (int j = 0; j < coords.size(); j++) {
                        String coord = coords.get(j);
                        Element transversal = createElement(doc, mtUri, "math");
                        apply = createElement(doc, mtUri, "apply");
                        Element applyMinus = createElement(doc, mtUri, "apply");
                        Element minus = createElement(doc, mtUri, "minus");
                        Element applyTimes = createElement(doc, mtUri, "apply");
                        Element times = createElement(doc, mtUri, "times");
                        newPi.setField(fields.get(j));
                        Element fieldTime = ExecutionFlow.replaceTags(variable.cloneNode(true), newPi);
                        Element normal2 = createElement(doc, mtUri, "ci");
                        normal2.setTextContent("n_" + coords.get(j));
                        applyMinus.appendChild(minus);
                        applyMinus.appendChild(doc.importNode(fieldTime, true));
                        applyMinus.appendChild(applyTimes);
                        applyTimes.appendChild(times);
                        applyTimes.appendChild(applyPlus.cloneNode(true));
                        applyTimes.appendChild(normal2);
                        ci = createElement(doc, mtUri, "ci");
                        ci.setTextContent(tensorName + "_t" + coord + "_var");
                        transversal.appendChild(apply);
                        apply.appendChild(eq.cloneNode(true));
                        apply.appendChild(doc.importNode(ci, true));
                        apply.appendChild(applyMinus);
                        result.appendChild(transversal);
                    }
                    if (!boundary) {
                        //With timeSlice
                        //Normal
                        Element normalVar2 = createElement(doc, mtUri, "math");
                        Element apply2 = createElement(doc, mtUri, "apply");
                        Element eq2 = createElement(doc, mtUri, "eq");
                        Element ci3 = createElement(doc, mtUri, "ci");
                        ci3.setTextContent(tensorName + "_n");
                        Element applyPlus2 = createElement(doc, mtUri, "apply");
                        Element plus2 = createElement(doc, mtUri, "plus");
                        applyPlus2.appendChild(plus2);
                        for (int j = 0; j < fields.size(); j++) {
                            Element applyTimes = createElement(doc, mtUri, "apply");
                            Element times = createElement(doc, mtUri, "times");
                            newPi.setField(fields.get(j));
                            Element fieldTime = ExecutionFlow.replaceTags(timeSlice.cloneNode(true), newPi);
                            Element normal = createElement(doc, mtUri, "ci");
                            normal.setTextContent("n_" + coords.get(j));
                            applyTimes.appendChild(times);
                            applyTimes.appendChild(doc.importNode(fieldTime, true));
                            applyTimes.appendChild(normal);
                            applyPlus2.appendChild(applyTimes);
                        }
                        normalVar2.appendChild(apply2);
                        apply2.appendChild(eq2);
                        apply2.appendChild(ci3);
                        apply2.appendChild(applyPlus2);
                        result.appendChild(normalVar2);
                        //Transversal
                        for (int j = 0; j < coords.size(); j++) {
                            String coord = coords.get(j);
                            Element transversal = createElement(doc, mtUri, "math");
                            apply = createElement(doc, mtUri, "apply");
                            Element applyMinus = createElement(doc, mtUri, "apply");
                            Element minus = createElement(doc, mtUri, "minus");
                            Element applyTimes = createElement(doc, mtUri, "apply");
                            Element times = createElement(doc, mtUri, "times");
                            newPi.setField(fields.get(j));
                            Element fieldTime = ExecutionFlow.replaceTags(timeSlice.cloneNode(true), newPi);
                            Element normal2 = createElement(doc, mtUri, "ci");
                            normal2.setTextContent("n_" + coords.get(j));
                            applyMinus.appendChild(minus);
                            applyMinus.appendChild(doc.importNode(fieldTime, true));
                            applyMinus.appendChild(applyTimes);
                            applyTimes.appendChild(times);
                            applyTimes.appendChild(applyPlus2.cloneNode(true));
                            applyTimes.appendChild(normal2);
                            ci = createElement(doc, mtUri, "ci");
                            ci.setTextContent(tensorName + "_t" + coord);
                            transversal.appendChild(apply);
                            apply.appendChild(eq2.cloneNode(true));
                            apply.appendChild(ci);
                            apply.appendChild(applyMinus);
                            result.appendChild(transversal);
                        }
                    }
                }
            }
        }
        return result;
    }
    
    /**
     * Create the instructions to project vectors in an specific coordinate.
     * @param destCoord                 The coordinate
     * @param charDecomposition         The characteristic decomposition
     * @param timeSlice                 The timeslice
     * @param pi                        The process information
     * @return                          The instructions
     * @throws AGDMException            AGDM006 Schema not applicable to this problem
     *                                  AGDM007 External error
     */
    public static DocumentFragment projectCoordVectors(String destCoord, Node charDecomposition, Node timeSlice, ProcessInfo pi) 
        throws AGDMException {
        Document doc = charDecomposition.getOwnerDocument();
        DocumentFragment result = doc.createDocumentFragment();
        ArrayList<String> coords = pi.getBaseSpatialCoordinates();
        ArrayList<Tensor> tensors = pi.getTensors();
        for (int i = 0; i < tensors.size(); i++) {
            Tensor tensor = tensors.get(i);
            String tensorName = tensor.getTensorName();
            if (tensor.getOrder() == 1) {
                if (coords.size() != tensor.getFields().size()) {
                    throw new AGDMException(AGDMException.AGDM005, 
                            "The problem is incorrect. The vector " + tensorName + " must have " + coords.size() + " components");
                }
                if (find(charDecomposition, ".//mt:ci[text() = '" + tensorName + "_n']|.//mms:field[text() = '" + tensorName 
                        + "_n']").getLength() > 0) {
                    //With timeslice
                    //Normal
                    Element normalVar = createElement(doc, mtUri, "math");
                    Element apply = createElement(doc, mtUri, "apply");
                    Element eq = createElement(doc, mtUri, "eq");
      
                    Element ci = createElement(doc, mtUri, "ci");
                    ci.setTextContent(tensorName + "_n");
                    Element coordElem = null;
                    ArrayList<String> fields = tensor.getFields();
                    for (int j = 0; j < fields.size(); j++) {
                        if (coords.get(j).equals(destCoord)) {
                            ProcessInfo newPi = new ProcessInfo(pi);
                            newPi.setField(fields.get(j));
                            coordElem = ExecutionFlow.replaceTags(timeSlice.cloneNode(true), newPi);
                        }
                    }
                    normalVar.appendChild(apply);
                    apply.appendChild(eq);
                    apply.appendChild(doc.importNode(ci, true));
                    apply.appendChild(doc.importNode(coordElem, true));
                    result.appendChild(normalVar);
                    //Transversal
                    for (int j = 0; j < coords.size(); j++) {
                        String coord = coords.get(j);
                        Element transversal = createElement(doc, mtUri, "math");
                        apply = createElement(doc, mtUri, "apply");
                        ProcessInfo newPi = new ProcessInfo(pi);
                        newPi.setField(fields.get(j));
                        Element fieldTime = ExecutionFlow.replaceTags(timeSlice.cloneNode(true), newPi);
                        ci = createElement(doc, mtUri, "ci");
                        ci.setTextContent(tensorName + "_t" + coord);
                        transversal.appendChild(apply);
                        apply.appendChild(eq.cloneNode(true));
                        apply.appendChild(ci);
                        if (coord.equals(destCoord)) {
                            Element zero = createElement(doc, mtUri, "cn");
                            zero.setTextContent("0");
                            apply.appendChild(zero);
                        }
                        else {
                            apply.appendChild(doc.importNode(fieldTime, true));
                        }
                        result.appendChild(transversal);
                    }
                }
            }
        }
        return result;
    }
    
    /**
     * Create the instructions to unproject vectors after doing a undiagonalization.
     * @param charDecomposition         The characteristic decomposition
     * @param variable                  The field variable
     * @param pi                        The process information
     * @return                          The instructions
     * @throws AGDMException            AGDM006 Schema not applicable to this problem
     *                                  AGDM007 External error
     */
    public static DocumentFragment unprojectVectors(Node charDecomposition, Node variable, ProcessInfo pi) throws AGDMException {
        Document doc = charDecomposition.getOwnerDocument();
        DocumentFragment result = doc.createDocumentFragment();
        ArrayList<String> coords = pi.getBaseSpatialCoordinates();
        ArrayList<Tensor> tensors = pi.getTensors();
        for (int i = 0; i < tensors.size(); i++) {
            Tensor tensor = tensors.get(i);
            String tensorName = tensor.getTensorName();
            if (tensor.getOrder() == 1) {
                if (coords.size() != tensor.getFields().size()) {
                    throw new AGDMException(AGDMException.AGDM005, 
                            "The problem is incorrect. The vector " + tensorName + " must have " + coords.size() + " components");
                }
                if (find(charDecomposition, ".//mt:ci[text() = '" + tensorName + "_n']|.//mms:field[text() = '" + tensorName 
                        + "_n']").getLength() > 0) {
                    //With timeslice
                    for (int j = 0; j < coords.size(); j++) {
                        Element field = AGDMUtils.createElement(doc, mtUri, "math");
                        Element apply = AGDMUtils.createElement(doc, mtUri, "apply");
                        Element eq = AGDMUtils.createElement(doc, mtUri, "eq");
                        ProcessInfo newPi = new ProcessInfo(pi);
                        newPi.setField(tensor.getFields().get(j));
                        Element fieldTime = ExecutionFlow.replaceTags(variable.cloneNode(true), newPi);
                        Element applyPlus = AGDMUtils.createElement(doc, mtUri, "apply");
                        Element plus = AGDMUtils.createElement(doc, mtUri, "plus");
                        Element trans = AGDMUtils.createElement(doc, mtUri, "ci");
                        trans.setTextContent(tensorName + "_t" + coords.get(j) + "_var");
                        Element applyTimes = AGDMUtils.createElement(doc, mtUri, "apply");
                        Element times = AGDMUtils.createElement(doc, mtUri, "times");
                        Element normalVar = AGDMUtils.createElement(doc, mtUri, "ci");
                        normalVar.setTextContent(tensorName + "_n_var");
                        Element normal = AGDMUtils.createElement(doc, mtUri, "ci");
                        normal.setTextContent("n_" + coords.get(j));
                        field.appendChild(apply);
                        apply.appendChild(eq);
                        apply.appendChild(doc.importNode(fieldTime, true));
                        apply.appendChild(applyPlus);
                        applyPlus.appendChild(plus);
                        applyPlus.appendChild(doc.importNode(trans, true));
                        applyPlus.appendChild(applyTimes);
                        applyTimes.appendChild(times);
                        applyTimes.appendChild(doc.importNode(normalVar, true));
                        applyTimes.appendChild(normal);
                        result.appendChild(field);
                    }
                }
            }
        }
        return result;
    }
    
    /**
     * Returns the tagId number for the function deployAuxiliaryDefinitions.
     * @param rule              The rule.
     * @return                  The id
     * @throws AGDMException    AGDM00X - External error
     */
    public static int checkTagId(Node rule) throws AGDMException {
        if (find(rule, ".//sml:maxCharSpeedCoordinate|.//sml:positiveCharSpeedCoordinate|.//sml:negativeCharSpeedCoordinate").getLength() > 0) {
            return FIVE;
        }
        if (find(rule, ".//sml:maxCharSpeed|.//sml:positiveCharSpeed|.//sml:negativeCharSpeed").getLength() > 0) {
            return FOUR;
        }
        return 0;
    }
    
    /**
     * Create a base timeslice at time n+1.
     * @param doc   The problem
     * @return      The element
     */
    public static Element createBaseTimeSlice(Document doc) {
        Element math = createElement(doc, mtUri, "math");
        Element apply = createElement(doc, mtUri, "apply");
        Element ci1 = createElement(doc, mtUri, "ci");
        Element msup = createElement(doc, mtUri, "msup");
        Element ci2 = createElement(doc, mtUri, "ci");
        Element field = createElement(doc, simmlUri, "field");
        Element apply2 = createElement(doc, mtUri, "apply");
        Element plus = createElement(doc, mtUri, "plus");
        Element ci3 = createElement(doc, mtUri, "ci");
        Element timeCoordinate = createElement(doc, simmlUri, "timeCoordinate");
        Element cn = createElement(doc, mtUri, "cn");
        cn.setTextContent("1");
        Element deploy = createElement(doc, simmlUri, "currentCell");
        apply.appendChild(ci1);
        apply.appendChild(deploy);
        ci1.appendChild(msup);
        msup.appendChild(ci2);
        msup.appendChild(apply2);
        ci2.appendChild(field);
        ci3.appendChild(timeCoordinate);
        apply2.appendChild(plus);
        apply2.appendChild(ci3);
        apply2.appendChild(cn);
        math.appendChild(apply);
        
        return math;
    }
    
    /**
     * Create a base timeslice at time n.
     * @param doc   The problem
     * @return      The element
     */
    public static Element createPreviousTimeSlice(Document doc) {
        Element apply = createElement(doc, mtUri, "apply");
        Element ci1 = createElement(doc, mtUri, "ci");
        Element msup = createElement(doc, mtUri, "msup");
        Element ci2 = createElement(doc, mtUri, "ci");
        Element field = createElement(doc, simmlUri, "field");
        Element ci3 = createElement(doc, mtUri, "ci");
        Element timeCoordinate = createElement(doc, simmlUri, "timeCoordinate");
        Element deploy = createElement(doc, simmlUri, "currentCell");
        apply.appendChild(ci1);
        apply.appendChild(deploy);
        ci1.appendChild(msup);
        msup.appendChild(ci2);
        msup.appendChild(ci3);
        ci2.appendChild(field);
        ci3.appendChild(timeCoordinate);
        
        return apply;
    }
    
    /**
     * Create a base position at time n.
     * @param doc   The problem
     * @return      The element
     */
    public static Element createPreviousPosition(Document doc) {
    	Element ci = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
		Element msup = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "msup");
		Element ci2 = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
		Element particlePosition = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "particlePosition");
		Element ci3 = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
		Element timeCoordinate = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "timeCoordinate");
		
		ci.appendChild(msup);
		msup.appendChild(ci2);
		ci2.appendChild(particlePosition);
		msup.appendChild(ci3);
		ci3.appendChild(timeCoordinate);
        
        return ci;
    }
    
    /**
     * Adds current cell to a variable checking if it is a shared variable.
     * @param doc		The document
     * @param variable	The variable to use
     * @return			Output variable
     */
    public static Element addCurrentCell(Document doc, Element variable) {
        Element currentCell = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "currentCell");
        Element var = (Element) variable.cloneNode(true);
        boolean shared = false;
        if (var.getLocalName().equals("sharedVariable")) {
        	var = (Element) var.getFirstChild();
        	shared = true;
        }
        boolean neighbour = false;
        if (var.getLocalName().equals("neighbourParticle")) {
        	var = (Element) var.getFirstChild();
        	neighbour = true;
        }
    	Element applyVar = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
    	applyVar.appendChild(doc.importNode(var, true));
    	applyVar.appendChild(currentCell);
        if (shared) {
        	Element sharedVar = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "sharedVariable");
        	sharedVar.appendChild(applyVar.cloneNode(true));
        	applyVar = sharedVar;
        }
        if (neighbour) {
        	Element neighbourParticle = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "neighbourParticle");
        	neighbourParticle.appendChild(applyVar.cloneNode(true));
        	applyVar = neighbourParticle;
        }
        return applyVar;
    }
    
    /**
     * Create a base timeslice at time n+1.
     * @param doc   The problem
     * @return      The element
     */
    public static Element createTimeSlice(Document doc) {
        Element apply = createElement(doc, mtUri, "apply");
        Element ci1 = createElement(doc, mtUri, "ci");
        Element msup = createElement(doc, mtUri, "msup");
        Element ci2 = createElement(doc, mtUri, "ci");
        Element field = createElement(doc, simmlUri, "field");
        Element ci3 = createElement(doc, mtUri, "ci");
        Element timeCoordinate = createElement(doc, simmlUri, "timeCoordinate");
        Element deploy = createElement(doc, simmlUri, "currentCell");
        
        Element apply2 = createElement(doc, mtUri, "apply");
        Element plus = createElement(doc, mtUri, "plus");
        Element one = createElement(doc, mtUri, "cn");
        one.setTextContent("1");
        apply2.appendChild(plus);
        apply2.appendChild(ci3);
        apply2.appendChild(one);
        ci3.appendChild(timeCoordinate);
        
        apply.appendChild(ci1);
        apply.appendChild(deploy);
        ci1.appendChild(msup);
        msup.appendChild(ci2);
        msup.appendChild(apply2);
        ci2.appendChild(field);
        
        return apply;
    }
    
    /**
     * Create a base timeslice at time n+1 with index from an external variable.
     * @param doc   	The problem
     * @param node	The variable to take the index from
     * @return      	The element
     * @throws AGDMException 
     */
    public static Element createTimeSliceWithExternalIndex(Document doc, Node node) throws AGDMException {
        Element apply = createElement(doc, mtUri, "apply");
        Element ci1 = createElement(doc, mtUri, "ci");
        Element msup = createElement(doc, mtUri, "msup");
        Element ci2 = createElement(doc, mtUri, "ci");
        Element field = createElement(doc, simmlUri, "field");
        Element ci3 = createElement(doc, mtUri, "ci");
        Element timeCoordinate = createElement(doc, simmlUri, "timeCoordinate");
        
        Element apply2 = createElement(doc, mtUri, "apply");
        Element plus = createElement(doc, mtUri, "plus");
        Element one = createElement(doc, mtUri, "cn");
        one.setTextContent("1");
        apply2.appendChild(plus);
        apply2.appendChild(ci3);
        apply2.appendChild(one);
        ci3.appendChild(timeCoordinate);
        
        apply.appendChild(ci1);
        
        ci1.appendChild(msup);
        msup.appendChild(ci2);
        msup.appendChild(apply2);
        ci2.appendChild(field);
        
        //Extract cell access from the variable (First child is the variable name, the rest are indexes or simml index tag)
        Node cellIndex = node;
        while (!cellIndex.getLocalName().equals("apply")) {
        	cellIndex = cellIndex.getFirstChild();
        }
        cellIndex = cellIndex.getFirstChild();
        while (cellIndex.getNextSibling() != null) {
        	cellIndex = cellIndex.getNextSibling();
        	apply.appendChild(doc.importNode(cellIndex.cloneNode(true), true));
        }
        
        return apply;
    }
    
    /**
     * Create a base timeslice at time n+1 with index from an external index.
     * @param doc   	The problem
     * @param index		The index
     * @return      	The element
     * @throws AGDMException 
     */
    public static Element createTimeSliceWithExternalIndex(Document doc, DocumentFragment index) throws AGDMException {
        Element apply = createElement(doc, mtUri, "apply");
        Element ci1 = createElement(doc, mtUri, "ci");
        Element msup = createElement(doc, mtUri, "msup");
        Element ci2 = createElement(doc, mtUri, "ci");
        Element field = createElement(doc, simmlUri, "field");
        Element ci3 = createElement(doc, mtUri, "ci");
        Element timeCoordinate = createElement(doc, simmlUri, "timeCoordinate");
        
        Element apply2 = createElement(doc, mtUri, "apply");
        Element plus = createElement(doc, mtUri, "plus");
        Element one = createElement(doc, mtUri, "cn");
        one.setTextContent("1");
        apply2.appendChild(plus);
        apply2.appendChild(ci3);
        apply2.appendChild(one);
        ci3.appendChild(timeCoordinate);
        
        apply.appendChild(ci1);
        
        ci1.appendChild(msup);
        msup.appendChild(ci2);
        msup.appendChild(apply2);
        ci2.appendChild(field);
        
        apply.appendChild(doc.importNode(index.cloneNode(true), true));
        
        return apply;
    }
    
    /**
     * Gets the maximum boundary stencil of the schema.
     * @param schema            The schema
     * @return                  The stencil
     * @throws AGDMException    AGDM00X - External error
     */
    public static int getMaxBoundaryStencil(Document schema) throws AGDMException {
        int maxStencil = 0;
        NodeList stencils = find(schema, "//sml:boundary/sml:stencil");
        for (int i = 0; i < stencils.getLength(); i++) {
            int stencil = Integer.parseInt(stencils.item(i).getTextContent());
            if (stencil > maxStencil) {
                maxStencil = stencil;
            }
        }
        return maxStencil;
    }
    
    /**
     * Remove invalid characters from a variable.
     * @param variable  The variable
     * @return          The new variable
     */
    public static String removeInvalidVariableCharacters(String variable) {
        String newVar = new String(variable);
        newVar = newVar.replaceAll(" ", "");
        return newVar;
    }
    
    /**
     * Replaces the element by the same element adding it a tmp sufix.
     * @param element               The element
     * @return                      The element
     * @throws AGDMException        AGDM00X  External error
     */
    public static Node replaceTmp(Node element) throws AGDMException {
        Element originalCi;
        if (element.getFirstChild().getLocalName().equals("apply")) {
            originalCi = (Element) element.getFirstChild().getFirstChild().cloneNode(true);
        }
        else {
            originalCi = (Element) element.getFirstChild().cloneNode(true);
        }

        //Create the new variable adding the region number at the end of the variable name
        Element newCi = (Element) originalCi.cloneNode(true);
        NodeList cis = AGDMUtils.find(newCi, "self::*[self::mt:ci]|.//mt:ci");
        boolean added = false;
        for (int k = cis.getLength() - 1; k >= 0 && !added; k--) {
            if (!cis.item(k).getTextContent().startsWith("(")) {
                cis.item(k).setTextContent(cis.item(k).getTextContent() + "_Tmp");
                added = true;
            }
        }
        if (added) {
            //Replace all the occurrences of the variable by the new name in the interior execution flow
            AGDMUtils.nodeReplace(element.getParentNode(), originalCi, newCi);
        }
        
        return element;
    }
    
    /**
     * Replace the normal vector components by the coordinate orientation.
     * @param element               The element with normals
     * @param coords                The spatial coordinates
     * @param coord                 The orientation coordinate
     * @return                      The resulting element
     * @throws AGDMException        AGDM007  External error
     */
    public static Node replaceNormals(Node element, ArrayList<String> coords, String coord) throws AGDMException {
        for (int i = 0; i < coords.size(); i++) {
            Element value = createElement(element.getOwnerDocument(), mtUri, "cn");
            if (coords.get(i).equals(coord)) {
                value.setTextContent("1");
            }
            else {
                value.setTextContent("0");
            }
            replaceVariable(element, "n_" + coords.get(i), value);
        }
        return element;
    }
    
    /**
     * Checks when an element uses normal vector components.
     * @param element           The element
     * @param coords            The spatial coordinates
     * @return                  True if uses normal vector components
     * @throws AGDMException    AGDM007  External error
     */
    public static boolean useNormals(Node element, ArrayList<String> coords) throws AGDMException {
        for (int i = 0; i < coords.size(); i++) {
            if (find(element, ".//mt:ci[text() = 'n_" + coords.get(i) + "']").getLength() > 0) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Checks when the interior region.
     * @param discProblem       The discretization problem
     * @param regionName        The region to check
     * @return                  True if the interior region exists
     * @throws AGDMException    AGDM00X  External error
     */
    public static boolean isInteriorRegion(Document discProblem, String regionName) throws AGDMException {
        return AGDMUtils.find(discProblem, "//mms:region[mms:name[text() = '" 
                + regionName + "']]//mms:interiorModel|//mms:subregion[mms:name[text() = '" 
                + regionName + "']]//mms:interiorModel").getLength() > 0;
    }
    
    /**
     * Checks when the surface region.
     * @param discProblem       The discretization problem
     * @param regionName        The region to check
     * @return                  True if the surface region exists
     * @throws AGDMException    AGDM00X  External error
     */
    public static boolean isSurfaceRegion(Document discProblem, String regionName) throws AGDMException {
        return AGDMUtils.find(discProblem, "//mms:region[mms:name[text() = '" 
                + regionName + "']]//mms:surfaceModel|//mms:subregion[mms:name[text() = '" 
                + regionName + "']]//mms:surfaceModel").getLength() > 0;
    }
    
    /**
     * Get the regions that have a defined field.
     * @param problem           The problem
     * @param field             The field
     * @return                  The region list
     * @throws AGDMException    AGDM00X - External error
     */
    public static ArrayList<String> regionsWithField(Document problem, String field) throws AGDMException {
        ArrayList<String> fieldRegions = new ArrayList<String>();
        NodeList regions = AGDMUtils.find(problem, "//mms:region|//mms:subregion");
        for (int i = 0; i < regions.getLength(); i++) {
            String regionName = regions.item(i).getFirstChild().getTextContent();
            NodeList models = AGDMUtils.find(problem, "//mms:region[mms:name = '" + regionName + "']//" 
                    + "mms:interiorModel|//mms:subregion[mms:name = '" + regionName + "']//" 
                    + "mms:interiorModel|//mms:subregion[mms:name = '" + regionName + "']//" 
                    + "mms:surfaceModel");
            for (int j = 0; j < models.getLength(); j++) {
                if (AGDMUtils.find(problem, "//mms:model[mms:name = '" 
                        + models.item(j).getTextContent() + "']//mms:field[ancestor::" 
                        + "mms:evolutionEquation and text() = '" + field + "']").getLength() > 0) {
                    fieldRegions.add(regionName);
                }
            }
        }
        return fieldRegions;
    }
    
    /**
     * Do some post-processing on the problem.
     * 1 - Join boundary coordinate and side tags
     * 2 - Add empty IterateOverCells to fit all region schemas.
     * 3 - Remove duplicated expressions if possible.
     * 4 - Use local variables for mesh accesses
     * @param problem           The problem
     * @param coords            The spatial coordinates
     * @throws AGDMException    AGDM00X  External error
     */
    public static void postProcess(Document problem, DiscretizationPolicy policy ,ArrayList<String> coords,HashMap<String, Integer> derivativeLevels, HashMap<String, Integer> auxDerivativeLevels) throws AGDMException {
        //1 - Join boundary coordinate and side tags
        NodeList boundaries = problem.getElementsByTagName("sml:boundary");
        for (int i = 0; i < boundaries.getLength(); i++) {
            Element boundary = (Element) boundaries.item(i);
            for (int j = coords.size() - 1; j >= 0; j--) {
                Element coord = AGDMUtils.createElement(problem, simmlUri, "axis");
                coord.setAttribute("axisAtt", coords.get(j));
                //Lower
                Element lower = AGDMUtils.createElement(problem, simmlUri, "side");
                lower.setAttribute("sideAtt", "lower");
                NodeList lowers = find(boundary, "./sml:axis[@axisAtt='" + coords.get(j) + "']/sml:side[@sideAtt='lower']");
                for (int k = 0; k < lowers.getLength(); k++) {
                    NodeList children = lowers.item(k).getChildNodes();
                    for (int l = 0; l < children.getLength(); l++) {
                        lower.appendChild(children.item(l).cloneNode(true));
                    }
                }
                //Upper
                Element upper = AGDMUtils.createElement(problem, simmlUri, "side");
                upper.setAttribute("sideAtt", "upper");
                NodeList uppers = find(boundary, "./sml:axis[@axisAtt='" + coords.get(j) + "']/sml:side[@sideAtt='upper']");
                for (int k = 0; k < uppers.getLength(); k++) {
                    NodeList children = uppers.item(k).getChildNodes();
                    for (int l = 0; l < children.getLength(); l++) {
                        upper.appendChild(children.item(l).cloneNode(true));
                    }
                }
                if (lowers.getLength() > 0) {
                    coord.appendChild(lower);
                }
                if (uppers.getLength() > 0) {
                    coord.appendChild(upper);
                }
                //Clean
                NodeList coordElems = find(boundary, "./sml:axis[@axisAtt='" + coords.get(j) + "'][not(descendant::sml:periodicalBoundary)]");
                for (int k = coordElems.getLength() - 1; k >= 0; k--) {
                    coordElems.item(k).getParentNode().removeChild(coordElems.item(k));
                }
                //Add joined elements
                if (lowers.getLength() > 0 || uppers.getLength() > 0) {
                    boundary.insertBefore(coord, boundary.getFirstChild());
                }
            }
        }
        //2 - Add empty IterateOverCells to fit all region schemas. 
        //Some of them may have less derivative levels, so iterations and boundaries does not match unless this correction is applied.
        int maxDerivativeLevels = Collections.max(derivativeLevels.values());
        int maxAuxDerivativeLevels = Collections.max(auxDerivativeLevels.values());
        Element iterOverCells = AGDMUtils.createElement(problem, AGDMUtils.simmlUri, "iterateOverCells");
        iterOverCells.setAttribute("stencilAtt", "0");
        
        for (PDERegionDiscPolicy rdp: policy.getRegionDiscPolicies()) {
            String groupSuffix = rdp.getFieldGroupSuffix();
            
            String name = rdp.getRegionName();
        	Element region = (Element) AGDMUtils.find(problem, "//mms:region[mms:name = '" + name + "']|//mms:subregion[mms:name = '" + name + "']").item(0);
        
            String keyI = name + "I" + groupSuffix;
            if ((derivativeLevels.containsKey(keyI) && derivativeLevels.get(keyI) < maxDerivativeLevels) ||
            		(auxDerivativeLevels.containsKey(keyI) && auxDerivativeLevels.get(keyI) < maxAuxDerivativeLevels)) {
            	Element executionFlow = (Element) AGDMUtils.find(problem, "//mms:interiorExecutionFlow[@groupSuffix = '" + groupSuffix + "']").item(0);
	            //Search a time substep
            	NodeList boundary = executionFlow.getElementsByTagName("sml:boundary");
            	for (int k = 0; k < boundary.getLength(); k++) {
            		//Odd boundaries limit field blocks
            		if (k % 2 == 1 && derivativeLevels.containsKey(keyI) && derivativeLevels.get(keyI) < maxDerivativeLevels) {
	            		//Search for the insertion point. The first derivative (or fluxes) loop
	            		Element insertionPoint = (Element) boundary.item(k);
	            		for (int l = 0; l < derivativeLevels.get(keyI); l++) {
	            			insertionPoint = (Element) insertionPoint.getPreviousSibling();	
	            		}
	            		//Insert as many empty loops as needed
	            		for (int l = 0; l < maxDerivativeLevels - derivativeLevels.get(keyI); l++) {
	            			insertionPoint.getParentNode().insertBefore(iterOverCells.cloneNode(true), insertionPoint);	
	            		}
            		}
            		//Pair boundaries limit auxiliary field blocks
            		if (k % 2 == 0 && auxDerivativeLevels.containsKey(keyI) && auxDerivativeLevels.get(keyI) < maxAuxDerivativeLevels) {
	            		//Search for the insertion point. The first derivative (or fluxes) loop
	            		Element insertionPoint = (Element) boundary.item(k);
	            		for (int l = 0; l < auxDerivativeLevels.get(keyI); l++) {
	            			insertionPoint = (Element) insertionPoint.getPreviousSibling();	
	            		}
	            		//Insert as many empty loops as needed
	            		for (int l = 0; l < maxAuxDerivativeLevels - auxDerivativeLevels.get(keyI); l++) {
	            			insertionPoint.getParentNode().insertBefore(iterOverCells.cloneNode(true), insertionPoint);	
	            		}
    	            }
	            }
            }
            String keyS = name + "S" + groupSuffix;
            if ((derivativeLevels.containsKey(keyS) && derivativeLevels.get(keyS) < maxDerivativeLevels) ||
            		(auxDerivativeLevels.containsKey(keyS) && auxDerivativeLevels.get(keyS) < maxAuxDerivativeLevels)) {
            	Element executionFlow = (Element) AGDMUtils.find(problem, "//mms:surfaceExecutionFlow[@groupSuffix = '" + groupSuffix + "']").item(0);
            	//Search a time substep
            	NodeList boundary = executionFlow.getElementsByTagName("sml:boundary");
            	for (int k = 0; k < boundary.getLength(); k++) {
            		//Odd boundaries limit field blocks
            		if (k % 2 == 1 && derivativeLevels.containsKey(keyS) && derivativeLevels.get(keyS) < maxDerivativeLevels) {
	            		//Search for the insertion point. The first derivative (or fluxes) loop
	            		Element insertionPoint = (Element) boundary.item(k);
	            		for (int l = 0; l < derivativeLevels.get(keyS); l++) {
	            			insertionPoint = (Element) insertionPoint.getPreviousSibling();	
	            		}
	            		//Insert as many empty loops as needed
	            		for (int l = 0; l < maxDerivativeLevels - derivativeLevels.get(keyS); l++) {
	            			insertionPoint.getParentNode().insertBefore(iterOverCells.cloneNode(true), insertionPoint);	
	            		}
            		}
            		//Pair boundaries limit auxiliary field blocks
            		if (k % 2 == 0 && auxDerivativeLevels.containsKey(keyS) && auxDerivativeLevels.get(keyS) < maxAuxDerivativeLevels) {
	            		//Search for the insertion point. The first derivative (or fluxes) loop
	            		Element insertionPoint = (Element) boundary.item(k);
	            		for (int l = 0; l < auxDerivativeLevels.get(keyS); l++) {
	            			insertionPoint = (Element) insertionPoint.getPreviousSibling();	
	            		}
	            		//Insert as many empty loops as needed
	            		for (int l = 0; l < maxAuxDerivativeLevels - auxDerivativeLevels.get(keyS); l++) {
	            			insertionPoint.getParentNode().insertBefore(iterOverCells.cloneNode(true), insertionPoint);	
	            		}
            		}
	            }
            }
            //Post initial condition
            if (auxDerivativeLevels.containsKey(keyI) && auxDerivativeLevels.get(keyI) < maxAuxDerivativeLevels) {
	            NodeList executionFlow = region.getElementsByTagName("mms:postInitialCondition");
	            for (int j = 0; j < executionFlow.getLength(); j++) {
	            	//Search a time substep
	            	NodeList boundary = ((Element) executionFlow.item(j)).getElementsByTagName("sml:boundary");
	            	for (int k = 0; k < boundary.getLength(); k++) {
	            		if (auxDerivativeLevels.containsKey(keyI) && auxDerivativeLevels.get(keyI) < maxAuxDerivativeLevels) {
		            		//Search for the insertion point. The first derivative (or fluxes) loop
		            		Element insertionPoint = (Element) boundary.item(k);
		            		for (int l = 0; l < auxDerivativeLevels.get(keyI); l++) {
		            			insertionPoint = (Element) insertionPoint.getPreviousSibling();	
		            		}
		            		//Insert as many empty loops as needed
		            		for (int l = 0; l < maxAuxDerivativeLevels - auxDerivativeLevels.get(keyI); l++) {
		            			insertionPoint.getParentNode().insertBefore(iterOverCells.cloneNode(true), insertionPoint);	
		            		}
	            		}
	            	}
	            }
            }
            if (auxDerivativeLevels.containsKey(keyS) && auxDerivativeLevels.get(keyS) < maxAuxDerivativeLevels) {
	            NodeList executionFlow = region.getElementsByTagName("mms:postInitialCondition");
	            for (int j = 0; j < executionFlow.getLength(); j++) {
	            	//Search a time substep
	            	NodeList boundary = ((Element) executionFlow.item(j)).getElementsByTagName("sml:boundary");
	            	for (int k = 0; k < boundary.getLength(); k++) {
	            		if (auxDerivativeLevels.containsKey(keyS) && auxDerivativeLevels.get(keyS) < maxAuxDerivativeLevels) {
		            		//Search for the insertion point. The first derivative (or fluxes) loop
		            		Element insertionPoint = (Element) boundary.item(k);
		            		for (int l = 0; l < auxDerivativeLevels.get(keyS); l++) {
		            			insertionPoint = (Element) insertionPoint.getPreviousSibling();	
		            		}
		            		//Insert as many empty loops as needed
		            		for (int l = 0; l < maxAuxDerivativeLevels - auxDerivativeLevels.get(keyS); l++) {
		            			insertionPoint.getParentNode().insertBefore(iterOverCells.cloneNode(true), insertionPoint);	
		            		}
	            		}
	            	}
	            }
            }
        }
        //3 - Remove duplicated expressions if possible.
        NodeList iterations = find(problem, ".//sml:iterateOverCells|.//sml:iterateOverAgents|.//sml:iterateOverVertices");
        for (int i = 0; i < iterations.getLength(); i++) {
        	removeDuplicatedExpressions(iterations.item(i));
        }


        
        //4 - Add empty substeps where needed if different time discretization schemas are used. Also remove substep tags
        NodeList executionFlows = find(problem, ".//mms:interiorExecutionFlow/sml:simml|.//mms:surfaceExecutionFlow/sml:simml");
        ArrayList<Float> globalTimestepFactors = policy.getLongestTimestepFactors();
        DocumentFragment emptySubstep = createEmptySubstep(executionFlows.item(0).getFirstChild());
        for (int i = executionFlows.getLength() - 1; i >= 0; i--) {
        	Element executionFlow = (Element) executionFlows.item(i).cloneNode(false);
        	NodeList substeps = executionFlows.item(i).getChildNodes();
        	
    		int globalIndex = 0;
    		for (int j = 0; j < substeps.getLength(); j++) {
    			Element substep = (Element) substeps.item(j).cloneNode(true);
    			//Mismatch in substep length, add missing substeps
            	if (substeps.getLength() < globalTimestepFactors.size()) {
        			Float factor = Float.parseFloat(substep.getAttribute("factor"));
        			while (globalTimestepFactors.get(globalIndex) < factor) {
        				globalIndex++;
        				executionFlow.appendChild(emptySubstep);
        			}
            	}
    			//Add current substep without substep tag
    			NodeList content = substep.getChildNodes();
    			for (int k = 0; k < content.getLength(); k++) {
    				executionFlow.appendChild(content.item(k).cloneNode(true));
    			}
    			if (substeps.getLength() < globalTimestepFactors.size()) {
    				globalIndex++;
    			}
    		}
    		//Mismatch in substep length, add missing substeps
        	if (substeps.getLength() < globalTimestepFactors.size()) {
        		//Check if more substeps must be added in the end
        		if (globalIndex < globalTimestepFactors.size()) {
            		for (int j = globalIndex; j < globalTimestepFactors.size(); j++) {
            			executionFlow.appendChild(emptySubstep);
            		}
        		}
        	}
    		executionFlows.item(i).getParentNode().replaceChild(executionFlow, executionFlows.item(i));

        	
        }
        
        //4 - Use local variables for mesh accesses
        /*NodeList iterations = find(problem, ".//sml:iterateOverCells|.//sml:iterateOverAgents|.//sml:iterateOverVertices");
        for (int i = 0; i < iterations.getLength(); i++) {
        	makeLocalArrayAccess(iterations.item(i));
        }*/
    }
    
    /**
     * Creates an empty substep with all its iterateOverCells and Boundary tags empty.
     * @param substep	A given substep structure
     * @return			Empty substep content
     */
    private static DocumentFragment createEmptySubstep(Node substep) {
    	DocumentFragment emptySubstep = substep.getOwnerDocument().createDocumentFragment();
    	NodeList content = substep.getChildNodes();
    	for (int i = 0; i < content.getLength(); i++) {
    		emptySubstep.appendChild(content.item(i).cloneNode(false));
    	}
    	
    	return emptySubstep;
    }
    
    /**
     * Make local variables from array accesses.
     * @param iteration			The context
     * @throws AGDMException	
     */
   /* private static void makeLocalArrayAccess(Node iteration) throws AGDMException {
    	ArrayList<String> assignedVars =  getAssignedVarsString(iteration, false);
    	ArrayList<String> assigned =  new ArrayList<String>();
    	Document owner = iteration.getOwnerDocument();
    	DocumentFragment added = owner.createDocumentFragment();
    	NodeList nodes = find(iteration, "descendant-or-self::mt:math/mt:apply/mt:eq/following-sibling::*[2]/descendant-or-self::mt:apply[*[1][local-name() = 'ci']]");
        for (int i = 0; i < nodes.getLength(); i++) {
        	Node var = nodes.item(i);
        	//Get variable name
        	String varName = xmlVarToString(var, false, false);
        	//Get index
	        String indexName = getIndexName(var);
	        if (!indexName.equals("")) {
	        	varName = varName + indexName;
        		Element ci = createElement(owner, mtUri, "ci");
        		ci.setTextContent(varName);
	        	//Add variable access assignment if not already added
	        	if (!assigned.contains(varName)) {
	        		assigned.add(varName);
	        		//Insert variable at begining
	        		Element math = createElement(owner, mtUri, "math");
	        		Element apply = createElement(owner, mtUri, "apply");
	        		Element eq = createElement(owner, mtUri, "eq");
	        		math.appendChild(apply);
	        		apply.appendChild(eq);
	        		apply.appendChild(ci);
	        		apply.appendChild(var.cloneNode(true));
	        		added.appendChild(math);
	        	}
	        	//Replace variable
	        	nodes.item(i).getParentNode().replaceChild(ci.cloneNode(true), nodes.item(i));
	        }
        }
        iteration.insertBefore(added, iteration.getFirstChild());
    }*/
    
    /**
     * Get cell index of variable as a string.
     * @param variable	The variable
     * @return			Index string
     */
  /*  private static String getIndexName(Node variable) {
    	String indexName = "";
    	NodeList coords = variable.getChildNodes();
    	for (int i = 1; i < coords.getLength(); i++) {
    		Node coord = coords.item(i);
    		if (coord.getLocalName().equals("ci")) {
    			indexName += coord.getTextContent();
    		} 
    		else {
	    		if (coord.getLocalName().equals("apply")) {
	    			NodeList applyChildren = coord.getChildNodes();
	    			String op;
	    			if (applyChildren.item(0).getLocalName().equals("plus")) {
	    				op = "p";
	    			}
	    			else {
	    				if (applyChildren.item(0).getLocalName().equals("minus")) {
		    				op = "m";
		    			}
	    				else {
	    					//Not a simple index
	    	    			return "";
	    				}
	    			}
	    			indexName += applyChildren.item(1).getTextContent() + op + Integer.parseInt(applyChildren.item(2).getTextContent());
	    		}
	    		else {
	    			//Not a simple index
	    			return "";
	    		}
    		}
    	}
		return indexName;
    }*/
    
    /**
     * Removes duplicated expressions in a instruction context.
     * @param context			The instruction context
     * @throws AGDMException	AGDM00X  External error
     */
    private static void removeDuplicatedExpressions(Node context) throws AGDMException {
    	DocumentFragment newChildren = context.getOwnerDocument().createDocumentFragment();
    	HashMap<String, ArrayList<String>> map = new HashMap<String, ArrayList<String>>();
    	NodeList instructions = context.getChildNodes();
    	for (int i = 0; i < instructions.getLength(); i++) {
    		Node instruction = instructions.item(i);
    		ArrayList<String> varAssigned = getAllAssignedVarsString(instruction);
    		if (instruction.getLocalName().equals("math")) {
    			String instructionString = xmlTransform(domToString(instruction));
    			if (!map.containsKey(instructionString)) {
    				removeAssignedVars(map, varAssigned);
    				newChildren.appendChild(instruction.cloneNode(true));
    	    		ArrayList<String> usedVars = getUsedVarsString(instruction, false);
    	    		if (usedVars.size() > 0) {
	    	    		usedVars.add(varAssigned.get(0));
	    				map.put(instructionString, usedVars);
    	    		}
    			}
    		}
    		else {
        		removeAssignedVars(map, varAssigned);
    			if (instruction.getLocalName().equals("if")) {
    				Element insElem = (Element) instruction.cloneNode(true);
    				removeDuplicatedExpressions(insElem.getElementsByTagName("sml:then").item(0));
    				NodeList elseElem = insElem.getElementsByTagName("sml:else");
    				if (elseElem.getLength() > 0) {
    					removeDuplicatedExpressions(elseElem.item(0));	
    				}
    				newChildren.appendChild(insElem);
    			}
    			else {
        			if (instruction.getLocalName().equals("while")) {
        				Element insElem = (Element) instruction.cloneNode(true);
        				removeDuplicatedExpressions(insElem.getElementsByTagName("sml:loop").item(0));
        				newChildren.appendChild(insElem);
        			} 
        			else {
            			if (instruction.getLocalName().equals("iterateOverEdges") || instruction.getLocalName().equals("iterateOverInteractions")
            					|| instruction.getLocalName().equals("interiorDomainContext") || instruction.getLocalName().equals("omnidirectionalContext")) {
            				Element insElem = (Element) instruction.cloneNode(true);
                			removeDuplicatedExpressions(insElem);
            				newChildren.appendChild(insElem);
            			}
            			else {
                			newChildren.appendChild(instruction.cloneNode(true));
            			}
        			}
    			}
    		}
    	}
    	//Remove old children
    	for (int i = instructions.getLength() - 1; i >= 0; i--) {
        	context.removeChild(instructions.item(i));
    	}
    	//Add new children
    	context.appendChild(newChildren);
    }
    
    /**
     * Remove equations who use any assigned variable given.
     * @param map	The equation map
     * @param vars	The assigned variables
     */
    private static void removeAssignedVars(HashMap<String, ArrayList<String>> map, ArrayList<String> vars) {
    	Iterator<String> it = map.keySet().iterator();
    	ArrayList<String> keysToRemove = new ArrayList<String>();
    	//Get keys to remove
    	while (it.hasNext()) {
    		String equation = it.next();
    		ArrayList<String> varsUsed = map.get(equation);
    		ArrayList<String> intersection = new ArrayList<String>(varsUsed);
    		intersection.retainAll(vars);
    		if (!intersection.isEmpty()) {
    			keysToRemove.add(equation);
    		}
    	}
    	//Remove equations
    	for (int i = 0; i < keysToRemove.size(); i++) {
    		map.remove(keysToRemove.get(i));
    	}
    }
    
    /**
     * Checks when a timeslice contains a spatial coordinate tag.
     * @param timeslice         The timeslice
     * @return                  True if contains one
     * @throws AGDMException    AGDM00X  External error
     */
    public static boolean hasSpatialCoordinate(Node timeslice) throws AGDMException {
        return ((Element) timeslice).getElementsByTagName("sml:spatialCoordinate").getLength() > 0;
    }
    
    /**
     * Add a spatial suffix to the timeslice variable.
     * @param pi                    The process info
     * @param timeslice             The timeslice
     * @return                      The new time slice
     * @throws AGDMException        AGDM00X  External error
     */
    public static Element addSpatialCoordinate(ProcessInfo pi, Element timeslice) throws AGDMException {
        Element timesliceNew = (Element) timeslice.cloneNode(true);
        Document doc = timesliceNew.getOwnerDocument();
        Element ci = createElement(doc, mtUri, "ci");
        Element msub = createElement(doc, mtUri, "msub");
        Element coord = createElement(doc, mtUri, "ci");
        Element spatialCoordinate = createElement(doc, simmlUri, "spatialCoordinate");
        coord.appendChild(spatialCoordinate);
        ci.appendChild(msub);
        
        if (timesliceNew.getElementsByTagName("sml:currentCell").getLength() > 0) {
            Node father = timesliceNew.getElementsByTagName("sml:currentCell").item(0).getParentNode();
            Element variable = (Element) father.getFirstChild().cloneNode(true);
            msub.appendChild(variable);
            msub.appendChild(coord);
            father.replaceChild(ci, father.getFirstChild());
        }
        else {
            Node father = timesliceNew;
            Element variable = (Element) father.getFirstChild().cloneNode(true);
            msub.appendChild(variable);
            msub.appendChild(coord);
            father.replaceChild(ci, father.getFirstChild());
        }
        return timesliceNew;
    }
    
    /**
     * Assign an expression to a variable.
     * @param variable          The variable to be assigned
     * @param variableTemplate  The variable template for fields
     * @param rhs               The right hand side of the expression
     * @param pi                The process information
     * @return                  The complete assignment
     * @throws AGDMException    AGDM00X  External error
     */
    public static Element assignVariable(String variable, Element variableTemplate, Element rhs, ProcessInfo pi) throws AGDMException {
        Document doc = rhs.getOwnerDocument();
        Element math = AGDMUtils.createElement(doc, mtUri, "math");
        Element apply = AGDMUtils.createElement(doc, mtUri, "apply");
        Element equal = AGDMUtils.createElement(doc, mtUri, "eq");
        math.appendChild(apply);
        apply.appendChild(equal);
        Element variableElem = AGDMUtils.createElement(doc, mtUri, "apply");
        Element varName = AGDMUtils.createElement(doc, mtUri, "ci");
        varName.setTextContent(variable);
        Element currCell = AGDMUtils.createElement(doc, simmlUri, "currentCell");
        variableElem.appendChild(varName);
        variableElem.appendChild(currCell);
        apply.appendChild(doc.importNode(ExecutionFlow.replaceTags(variableElem, pi), true));
        AGDMUtils.replaceFieldVariables(pi, variableTemplate, rhs);
        apply.appendChild(rhs.getFirstChild());
        return math;
    }
    
    /**
     * Assign an expression to a local variable (not in memory).
     * @param variable          The variable to be assigned
     * @param variableTemplate  The variable template for fields
     * @param rhs               The right hand side of the expression
     * @param pi                The process information
     * @return                  The complete assignment
     * @throws AGDMException    AGDM00X  External error
     */
    public static Element assignLocalVariable(String variable, Element variableTemplate, Element rhs, ProcessInfo pi) throws AGDMException {
        Document doc = rhs.getOwnerDocument();
        Element math = AGDMUtils.createElement(doc, mtUri, "math");
        Element apply = AGDMUtils.createElement(doc, mtUri, "apply");
        Element equal = AGDMUtils.createElement(doc, mtUri, "eq");
        math.appendChild(apply);
        apply.appendChild(equal);
        Element varName = AGDMUtils.createElement(doc, mtUri, "ci");
        varName.setTextContent(variable);
        apply.appendChild(doc.importNode(ExecutionFlow.replaceTags(varName, pi), true));
        AGDMUtils.replaceFieldVariables(pi, (Element) variableTemplate.cloneNode(true), rhs);
        apply.appendChild(rhs.getFirstChild());        
        return math;
    }
    
    /**
     * Assign an expression to a local variable (not in memory).
     * @param variable          The variable to be assigned
     * @param variableTemplate  The variable template for fields
     * @param rhs               The right hand side of the expression
     * @param pi                The process information
     * @return                  The complete assignment
     * @throws AGDMException    AGDM00X  External error
     */
    public static Element assignLocalVariableTemplate(String variable, Element variableTemplate, Element rhs, ProcessInfo pi) throws AGDMException {
        Document doc = rhs.getOwnerDocument();
        Element math = AGDMUtils.createElement(doc, mtUri, "math");
        Element apply = AGDMUtils.createElement(doc, mtUri, "apply");
        Element equal = AGDMUtils.createElement(doc, mtUri, "eq");
        math.appendChild(apply);
        apply.appendChild(equal);
        ProcessInfo newPi = new ProcessInfo(pi);
        newPi.setField(variable);
        Element varName = (Element) ExecutionFlow.replaceTags(AGDMUtils.removeIndex(variableTemplate), newPi);
        apply.appendChild(doc.importNode(ExecutionFlow.replaceTags(varName, pi), true));
        AGDMUtils.replaceFieldVariables(pi, (Element) variableTemplate.cloneNode(true), rhs);
        apply.appendChild(rhs.getFirstChild());
        return math;
    }
    
    /**
     * Assign an expression to a variable.
     * @param variable          The variable to be assigned
     * @param rhs               The right hand side of the expression
     * @return                  The complete assignment
     * @throws AGDMException    AGDM00X  External error
     */
    public static Element assignVariable(String variable, Node rhs) throws AGDMException {
        Document doc = rhs.getOwnerDocument();
        Element math = AGDMUtils.createElement(doc, mtUri, "math");
        Element apply = AGDMUtils.createElement(doc, mtUri, "apply");
        Element equal = AGDMUtils.createElement(doc, mtUri, "eq");
        math.appendChild(apply);
        apply.appendChild(equal);
        Element varName = AGDMUtils.createElement(doc, mtUri, "ci");
        varName.setTextContent(variable);
        apply.appendChild(varName);
        apply.appendChild(rhs);
        return math;
    }
    
    /**
     * Returns a counter and increments it.
     * @return  The counter
     */
    public static int getCounter() {
        schemaNameCounter++;
        return schemaNameCounter;
    }
    
    /**
     * Improves the performance of the resulting code by applying previous simplifications.
     * @param problem           The raw complete problem with models included
     * @throws AGDMException    AGDM00X  External error
     */
    public static void improvePerformance(Document problem) throws AGDMException {
        //1 - Group all source terms in equations(PDE problems)
        NodeList operators = problem.getElementsByTagName("mms:operator");
        for (int j = 0; j < operators.getLength(); j++) {
            Element operator = (Element) operators.item(j);
            //Process every term
            NodeList sources = find(operator, ".//mms:term[not(mms:partialDerivatives)]");
            if (sources.getLength() > 1) {
                //Common source term summation
                Element commonSource = createElement(problem, mmsUri, "term");
                Element math = createElement(problem, mtUri, "math");
                Element apply = createElement(problem, mtUri, "apply");
                Element plus = createElement(problem, mtUri, "plus");
                commonSource.appendChild(math);
                math.appendChild(apply);
                apply.appendChild(plus);
                //Append every term to the summation and remove it
                for (int k = sources.getLength() - 1; k >= 0; k--) {
                    Node term = ((Element) sources.item(k)).getElementsByTagNameNS(mtUri, "math").item(0).getFirstChild();
                    apply.appendChild(term);
                    sources.item(k).getParentNode().removeChild(sources.item(k));
                }
                //Add summation term
                operator.appendChild(commonSource);
            }
        }
    }
    
    /**
     * Calculates the maximum between the values of two hasMaps, key by key.
     * It adds the keys if do not exists in both elements.
     * @param a		Element a
     * @param b		Element b
     * @return		max(a,b)
     */
    public static HashMap<String, Integer> maxHashMapStencil(HashMap<String, Integer> a, HashMap<String, Integer> b) {
    	HashMap<String, Integer> c = new HashMap<String, Integer>();
    	Iterator<String> aIt = a.keySet().iterator();
    	while (aIt.hasNext()) {
    		String keyA = aIt.next();
    		if (b.containsKey(keyA)) {
    			c.put(keyA, Math.max(a.get(keyA), b.get(keyA)));
    		}
    		else {
    			c.put(keyA, a.get(keyA));
    		}
    	}
    	//Add remaining elements in B
    	Iterator<String> bIt = b.keySet().iterator();
    	while (bIt.hasNext()) {
    		String keyB = bIt.next();
    		if (!a.containsKey(keyB)) {
    			c.put(keyB, b.get(keyB));
    		}
    	}
    	return c;
    }
    
    /**
     * Checks if there are any flux in any model of the problem.
     * @param problem				The problem
     * @return						True if there is at least one flux.
     * @throws AGDMException		AGDM00X - External error
     */
    public static boolean checkIfHasFluxes(Document problem) throws AGDMException {
    	return problem.getElementsByTagName("mms:flux").getLength() > 0;
    }
    
    /**
     * Adds a suffix to the parameters used in the schema.
     * @param schema			The schema
     * @param code				The code where to replace
     * @param suffix			The suffix
     * @throws AGDMException	AGDM00X - External error
     */
    public static void addParameterSuffixNames(Document schema, Node code, String suffix) throws AGDMException {
        NodeList schemaParams = schema.getDocumentElement().getElementsByTagNameNS(AGDMUtils.mmsUri, "parameter");
        for (int j = 0; j < schemaParams.getLength(); j++) {
        	String param = schemaParams.item(j).cloneNode(true).getFirstChild().getTextContent();
        	Element oldNode = AGDMUtils.createElement(code.getOwnerDocument(), AGDMUtils.mtUri, "ci");
        	oldNode.setTextContent(param);
        	String newParam = param + "_" + suffix;
        	Element newNode = AGDMUtils.createElement(code.getOwnerDocument(), AGDMUtils.mtUri, "ci");
        	newNode.setTextContent(newParam);
        	AGDMUtils.nodeReplace(code, oldNode, newNode);
        }
    }
    
    /**
     * Adds a suffix to a variable.
     * @param variable			Variable to add  suffix to
     * @param suffix			The suffix
     * @throws AGDMException	AGDM00X - External error
     */
    public static void addSuffix(Element variable, String suffix) throws AGDMException {
        NodeList varName = find(variable, "descendant-or-self::mt:ci");
        if (varName.getLength() > 0) {
        	String name = varName.item(varName.getLength() - 1).getTextContent();
        	varName.item(varName.getLength() - 1).setTextContent(name + suffix);
        }
    }
    
    /**
     * Adds a prefix to a variable.
     * @param variable			Variable to add  prefix to
     * @param prefix			The prefix
     * @throws AGDMException	AGDM00X - External error
     */
    public static void addPrefix(Element variable, String prefix) throws AGDMException {
        NodeList varName = find(variable, "descendant-or-self::mt:ci[normalize-space(text())]");
        if (varName.getLength() > 0) {
        	String name = varName.item(0).getTextContent();
        	varName.item(0).setTextContent(prefix + name);
        }
    }
    
    /**
     * Get all the variables (stored in mesh) that are assigned inside the code.
     * @param block				The block to check
     * @param withIndex			If the variables must be returned with index
     * @return					The variables
     * @throws AGDMException 	AGDM00X - External error
     */
    public static ArrayList<NodeWrap> getAssignedVars(Node block, boolean withIndex) throws AGDMException {
    	NodeList nodes = find(block, "descendant-or-self::mt:math[(not(parent::sml:if) or parent::sml:then) and "
            + "(not(parent::sml:while) or parent::sml:loop)]/mt:apply/mt:eq/following-sibling::*[1][name()='mt:apply' "
            + "or name()='sml:sharedVariable']");
    	ArrayList<NodeWrap> assignedVars = new ArrayList<NodeWrap>();
        for (int i = 0; i < nodes.getLength(); ++i) {
        	Node var = nodes.item(i);
        	if (!withIndex) {
	        	//Remove the index
	        	if (var.getLocalName().equals("sharedVariable")) {
	        		var = var.getFirstChild().getFirstChild();
	        	}
	        	else {
	        		var = var.getFirstChild();
	        	}
        	}
            assignedVars.add(new NodeWrap(var));
        }
        return assignedVars;
    }
    
    /**
     * Get all the variables (stored in mesh) that are assigned inside the code.
     * @param block				The block to check
     * @param withIndex			If the variables must be returned with index
     * @return					The variables
     * @throws AGDMException 	AGDM00X - External error
     */
    public static ArrayList<String> getAssignedVarsString(Node block, boolean withIndex) throws AGDMException {
    	NodeList nodes = find(block, "descendant-or-self::mt:math[(not(parent::sml:if) or parent::sml:then) and "
            + "(not(parent::sml:while) or parent::sml:loop)]/mt:apply/mt:eq/following-sibling::*[1][name()='mt:apply' "
            + "or name()='sml:sharedVariable']");
    	ArrayList<String> assignedVars = new ArrayList<String>();
        for (int i = 0; i < nodes.getLength(); ++i) {
        	Node var = nodes.item(i);
        	if (!withIndex) {
	        	//Remove the index
	        	if (var.getLocalName().equals("sharedVariable")) {
	        		var = var.getFirstChild().getFirstChild();
	        	}
	        	else {
	        		var = var.getFirstChild();
	        	}
        	}
            assignedVars.add(xmlVarToString(var, false, true));
        }
        return assignedVars;
    }
    /**
     * Get all the variables (stored in mesh) that are assigned inside the code filtered.
     * @param block				The block to check
     * @param outputVars		Filter to apply
     * @return					The variables
     * @throws AGDMException 	AGDM00X - External error
     */
    public static ArrayList<NodeWrap> getAssignedNonOutputVars(Node block, ArrayList<NodeWrap> outputVars) throws AGDMException {
    	NodeList nodes = find(block, "descendant-or-self::mt:math[(not(parent::sml:if) or parent::sml:then) and "
                + "(not(parent::sml:while) or parent::sml:loop)]/mt:apply/mt:eq/following-sibling::*[1][name()='mt:apply' or name()='sml:sharedVariable']");
    	ArrayList<NodeWrap> assignedVars = new ArrayList<NodeWrap>();
        for (int i = 0; i < nodes.getLength(); ++i) {
        	Node var = nodes.item(i);
        	//Remove the index
        	if (var.getLocalName().equals("sharedVariable")) {
        		var = var.getFirstChild().getFirstChild();
        	}
        	else {
        		var = var.getFirstChild();
        	}
        	if (!outputVars.contains(new NodeWrap(var))) {
        		assignedVars.add(new NodeWrap(var));
        	}
        }
        return assignedVars;
    }
    /**
     * Get all the variables (stored in mesh) that are assigned inside the code filtered.
     * @param block				The block to check
     * @param outputVars		Filter to apply
     * @return					The variables
     * @throws AGDMException 	AGDM00X - External error
     */
    public static ArrayList<String> getAssignedNonOutputVarsString(Node block, ArrayList<NodeWrap> outputVars) throws AGDMException {
    	NodeList nodes = find(block, "descendant-or-self::mt:math[(not(parent::sml:if) or parent::sml:then) and "
                + "(not(parent::sml:while) or parent::sml:loop)]/mt:apply/mt:eq/following-sibling::*[1][name()='mt:apply' or name()='sml:sharedVariable']");
    	ArrayList<String> assignedVars = new ArrayList<String>();
        for (int i = 0; i < nodes.getLength(); ++i) {
        	Node var = nodes.item(i);
        	//Remove the index
        	if (var.getLocalName().equals("sharedVariable")) {
        		var = var.getFirstChild().getFirstChild();
        	}
        	else {
        		var = var.getFirstChild();
        	}
         	if (!outputVars.contains(new NodeWrap(var))) {
         		 assignedVars.add(xmlVarToString(var, false, true));
        	}
        }
        return assignedVars;
    }
    
    /**
     * Get all assigned variables in a block without index.
     * @param block				The block to check
     * @return					The variables
     * @throws AGDMException 	AGDM00X - External error
     */
    public static ArrayList<NodeWrap> getAllAssignedVars(Node block) throws AGDMException {
    	NodeList nodes = find(block, "descendant-or-self::mt:math[(not(parent::sml:if) or parent::sml:then) and "
                + "(not(parent::sml:while) or parent::sml:loop)]/mt:apply/mt:eq/following-sibling::*[1]");
        	ArrayList<NodeWrap> assignedVars = new ArrayList<NodeWrap>();
            for (int i = 0; i < nodes.getLength(); ++i) {
            	Node var = nodes.item(i);
	        	//Remove the index
	        	if (var.getLocalName().equals("sharedVariable") || var.getLocalName().equals("apply")) {
	        		var = var.getFirstChild();
	        	}
                assignedVars.add(new NodeWrap(var));
            }
            return assignedVars;
    }
    /**
     * Get all assigned variables in a block without index.
     * @param block				The block to check
     * @return					The variables
     * @throws AGDMException 	AGDM00X - External error
     */
    public static ArrayList<String> getAllAssignedVarsString(Node block) throws AGDMException {
    	NodeList nodes = find(block, "descendant-or-self::mt:math[(not(parent::sml:if) or parent::sml:then) and "
                + "(not(parent::sml:while) or parent::sml:loop) and not(ancestor::sml:positionReference)]/mt:apply/mt:eq/following-sibling::*[1]");
    	ArrayList<String> assignedVars = new ArrayList<String>();
        for (int i = 0; i < nodes.getLength(); ++i) {
        	Node var = nodes.item(i);
        	//Remove the index
        	if (var.getLocalName().equals("sharedVariable") || var.getLocalName().equals("apply")) {
        		var = var.getFirstChild();
        	}
            assignedVars.add(xmlVarToString(var, false, true));
        }
        return assignedVars;
    }
    
    /**
     * Conversion of a xml node variable in a string variable.
     * @param xmlVar            The xml variable
     * @param onlyMeshVars      Only convert mesh variables
     * @param removeTimeSteps   Remove the previous time steps suffixes from the variable name
     * @return                  The variable string
     */
    public static String xmlVarToString(Node xmlVar, boolean onlyMeshVars, boolean removeTimeSteps) {
        Node tmp = xmlVar.cloneNode(true);
        //Remove shared variable tag
        if (tmp.getLocalName().equals("sharedVariable")) {
            tmp = tmp.getFirstChild();
        }
        //Remove coordinates
        if (tmp.getLocalName().equals("apply")) {
            tmp = tmp.getFirstChild();
        }
        else {
            if (onlyMeshVars) {
                return null;
            }
        }
        //Process recursively the variable
        return mathToString((Element) tmp, removeTimeSteps);
    }
    
    /**
     * Converts recursively a node variable into a string variable.
     * @param xml               The node variable element
     * @param removeTimeSteps   Remove the previous time steps suffixes from the variable name
     * @return                  The current variable name
     */
    public static String mathToString(Element xml, boolean removeTimeSteps) {
        //ci or cn terminal element
        
        if ((xml.getLocalName().equals("ci") || xml.getLocalName().equals("cn")) && xml.getFirstChild().getNodeType() == Node.TEXT_NODE) {
            return xml.getTextContent().replaceAll("'", "prime").replaceAll("\\*", "ast").replaceAll("\\+", "pos")
                    .replaceAll("-", "neg").replaceAll("#", "sharp").replaceAll("%", "percent").replaceAll("\u0025B", "epsilon")
                    .replaceAll("\u00AC", "negative").replaceAll("\u00B5", "mu").replaceAll("\u00D7", "times")
                    .replaceAll("\u00E6", "ae").replaceAll("\u0391", "Alpha").replaceAll("\u0392", "Beta")
                    .replaceAll("\u0393", "Gamma").replaceAll("\u0394", "delta").replaceAll("\u0395", "Epsilon")
                    .replaceAll("\u0396", "Zeta").replaceAll("\u0397", "Eta").replaceAll("\u0398", "Theta")
                    .replaceAll("\u0399", "Iota").replaceAll("\u039A", "Kappa").replaceAll("\u039B", "Lambda")
                    .replaceAll("\u039C", "Mu").replaceAll("\u039D", "Nu").replaceAll("\u039E", "Xi")
                    .replaceAll("\u039F", "Omicron").replaceAll("\u03A0", "Pi").replaceAll("\u03A1", "Rho")
                    .replaceAll("\u03A3", "Sigma").replaceAll("\u03A4", "Tau").replaceAll("\u03A5", "Upsilon")
                    .replaceAll("\u03A6", "Phi").replaceAll("\u03A7", "Chi").replaceAll("\u03A8", "Psi")
                    .replaceAll("\u03A9", "Omega").replaceAll("\u03B1", "alpha").replaceAll("\u03B2", "beta")
                    .replaceAll("\u03B3", "gamma").replaceAll("\u03B4", "deltaSmall").replaceAll("\u03B5", "epsilon")
                    .replaceAll("\u03B6", "zeta").replaceAll("\u03B7", "eta").replaceAll("\u03B8", "theta")
                    .replaceAll("\u03B9", "iota").replaceAll("\u03BA", "kappa").replaceAll("\u03BB", "lambda")
                    .replaceAll("\u03BC", "mu").replaceAll("\u03BD", "nu").replaceAll("\u03BE", "xi")
                    .replaceAll("\u03BF", "o").replaceAll("\u03C0", "pi").replaceAll("\u03C1", "rho")
                    .replaceAll("\u03C2", "stigma").replaceAll("\u03C3", "sigma").replaceAll("\u03C4", "tau")
                    .replaceAll("\u03C5", "upsilon").replaceAll("\u03C6", "phi").replaceAll("\u03C7", "chi")
                    .replaceAll("\u03C8", "psi").replaceAll("\u03C9", "omega").replaceAll("\u03D1", "theta")
                    .replaceAll("\u03D2", "upsilon").replaceAll("\u03D5", "phi").replaceAll("\u03D6", "pi")
                    .replaceAll("\u03F0", "kappa").replaceAll("\u03F1", "rho").replaceAll("\"", "quot")
                    .replaceAll("\u03B7", "eta").replaceAll("\u03B7", "eta").replaceAll("\u03B7", "eta");
        }
        //msubsup
        if (xml.getLocalName().equals("msubsup")) {
            NodeList subtags = xml.getChildNodes();
            return mathToString((Element) subtags.item(0), removeTimeSteps) + "SB" + mathToString((Element) subtags.item(1), removeTimeSteps) 
                + "SP" + mathToString((Element) subtags.item(2), removeTimeSteps);
        }
        //msub
        if (xml.getLocalName().equals("msub")) {
            NodeList subtags = xml.getChildNodes();
            return mathToString((Element) subtags.item(0), removeTimeSteps) + "SB" + mathToString((Element) subtags.item(1), removeTimeSteps);
        }
        //msup (special attention to time steps)
        if (xml.getLocalName().equals("msup")) {
            NodeList subtags = xml.getChildNodes();
            String value = mathToString((Element) subtags.item(0), removeTimeSteps);
            if (subtags.item(1).getFirstChild().getNodeType() == Node.ELEMENT_NODE) {
                if (subtags.item(1).getChildNodes().item(1).getTextContent().startsWith("(")) {
                    if (!removeTimeSteps) {
                        if (subtags.item(1).getChildNodes().item(0).getLocalName().equals("minus")) {
                           int steps = Integer.parseInt(subtags.item(1).getChildNodes().item(2).getTextContent()) + 1;
                           for (int i = 0; i < steps; i++) {
                               value = value + "_p";
                           }
                        }
                    }
                }
                else {
                    value = value + "SP" +  mathToString((Element) subtags.item(1), removeTimeSteps);
                }
            }
            else {
                if (subtags.item(1).getTextContent().startsWith("(")) {
                    if (!removeTimeSteps) {
                        value = value + "_p";
                    }
                }
                else {
                    value = value + "SP" +  mathToString((Element) subtags.item(1), removeTimeSteps);
                }
            }
            return value;
        }
        //Others
        String value = "";
        NodeList subtags = xml.getChildNodes();
        for (int i = 0; i < subtags.getLength(); i++) {
            if (subtags.item(i).getNodeType() == Node.ELEMENT_NODE) {
                value = value + mathToString((Element) subtags.item(i), removeTimeSteps);
            }
        }
        return value;
    }
    
    /**
     * Parses a xml with a xlst.
     * @param xml                   The xml to parse.
     * @return                      the result to apply the xlt to the xml.
     * @throws AGDMException        AGDM00X External error
     */
    public static String xmlTransform(String xml) throws AGDMException {  
        try {
            ByteArrayInputStream input = new ByteArrayInputStream(xml.getBytes("UTF-8"));
            ByteArrayOutputStream output = new ByteArrayOutputStream();
                    
            transformerSelected.transform(new StreamSource(input), new StreamResult(output));
            return output.toString("UTF-8");
        } 
        catch (Exception e) {
        	e.printStackTrace();
            throw new AGDMException(AGDMException.AGDM00X, e.getMessage());
        }
    }
    
    /**
     * Get all mesh variables that are used inside the code.
     * @param block				The block to check
     * @param withIndex			If the index must be included
     * @return					The variables
     * @throws AGDMException 	AGDM00X - External error
     */
    public static ArrayList<NodeWrap> getUsedVars(Node block, boolean withIndex) throws AGDMException {
    	NodeList nodes = find(block, "descendant-or-self::mt:math[(not(parent::sml:if) or parent::sml:then) and (not(parent::sml:while) or parent::sml:loop)]/mt:apply/mt:eq/following-sibling::*[2]/descendant-or-self::*[(name() = 'mt:apply' and .[child::*[1][name() = 'mt:ci' or name() = 'mt:msup']]) or name() = 'sml:sharedVariable' or (name() = 'mt:ci' and .[parent::sml:functionCall])]");
    	ArrayList<NodeWrap> usedVars = new ArrayList<NodeWrap>();
        for (int i = 0; i < nodes.getLength(); ++i) {
        	Node var = nodes.item(i);
        	//Remove shared variable tag
        	if (var.getLocalName().equals("sharedVariable")) {
        		var = var.getFirstChild();
        	}
        	//Remove index if necessary
        	if (var.getLocalName().equals("apply") && !withIndex) {
        		var = var.getFirstChild();
        	}
        	if (!usedVars.contains(new NodeWrap(var))) {
        		usedVars.add(new NodeWrap(var));
        	}
        }
        return usedVars;
    }
    
    /**
     * Get all  variables that are used inside the code.
     * @param block				The block to check
     * @param withIndex			If the index must be included
     * @return					The variables
     * @throws AGDMException 	AGDM00X - External error
     */
    public static ArrayList<NodeWrap> getAllUsedVars(Node block, boolean withIndex) throws AGDMException {
    	NodeList nodes = find(block, "descendant-or-self::*[name() = 'mt:msup' or name() = 'mt:msub' or (name() = 'mt:ci' and not(ancestor::mt:msub) and not(ancestor::mt:msup))]");
    	ArrayList<NodeWrap> usedVars = new ArrayList<NodeWrap>();
        for (int i = 0; i < nodes.getLength(); ++i) {
        	Node var = nodes.item(i);
        	//Remove shared variable tag
        	if (var.getLocalName().equals("sharedVariable")) {
        		var = var.getFirstChild();
        	}
        	//Remove index if necessary
        	if (var.getLocalName().equals("apply") && !withIndex) {
        		var = var.getFirstChild();
        	}
        	if (!usedVars.contains(new NodeWrap(var))) {
        		usedVars.add(new NodeWrap(var));
        	}
        }
        return usedVars;
    }
    
    /**
     * Get all  variables that are used inside the code.
     * @param block				The block to check
     * @param withIndex			If the index must be included
     * @return					The variables
     * @throws AGDMException 	AGDM00X - External error
     */
    public static ArrayList<String> getAllUsedVarsString(Node block, boolean withIndex) throws AGDMException {
    	NodeList nodes = find(block, "descendant-or-self::*[name() = 'mt:msup' or name() = 'mt:msub' or (name() = 'mt:ci' and not(ancestor::mt:msub) and not(ancestor::mt:msup))]");
    	ArrayList<String> usedVars = new ArrayList<String>();
        for (int i = 0; i < nodes.getLength(); ++i) {
        	Node var = nodes.item(i);
        	//Remove shared variable tag
        	if (var.getLocalName().equals("sharedVariable")) {
        		var = var.getFirstChild();
        	}
        	//Remove index if necessary
        	if (var.getLocalName().equals("apply") && !withIndex) {
        		var = var.getFirstChild();
        	}
        	String variable = xmlVarToString(var, false, false);
        	if (!usedVars.contains(variable)) {
        		usedVars.add(variable);
        	}
        }
        return usedVars;
    }
    
    /**
     * Get all the variables that are used inside the code.
     * @param block				The block to check
     * @return					The variables
     * @throws AGDMException 	AGDM00X - External error
     */
    public static ArrayList<NodeWrap> getDisplacedVars(Node block) throws AGDMException {
    	NodeList nodes = find(block, "descendant-or-self::mt:math[(not(parent::sml:if) or parent::sml:then) and (not(parent::sml:while) or parent::sml:loop)]/mt:apply/mt:eq/following-sibling::*[2]/descendant-or-self::*[(name() = 'mt:apply' and .[child::*[1][name() = 'mt:ci' or name() = 'mt:msup']]) or name() = 'sml:sharedVariable' or (name() = 'mt:ci' and .[parent::sml:functionCall])]");
    	ArrayList<NodeWrap> usedVars = new ArrayList<NodeWrap>();
        for (int i = 0; i < nodes.getLength(); ++i) {
        	Node var = nodes.item(i);
        	//Remove shared variable tag
        	if (var.getLocalName().equals("sharedVariable")) {
        		var = var.getFirstChild();
        	}
        	//Remove index if necessary
        	if (var.getLocalName().equals("apply")) {
        		if (find(var, "./*[position() > 1][self::mt:apply]").getLength() > 0) {
            		var = var.getFirstChild();
            		usedVars.add(new NodeWrap(var));	
        		}
        	}
        }
        return usedVars;
    }
    
    /**
     * Get all the variables that are used inside the code.
     * @param block				The block to check
     * @param withIndex			If the index must be included
     * @return					The variables
     * @throws AGDMException 	AGDM00X - External error
     */
    public static ArrayList<String> getUsedVarsString(Node block, boolean withIndex) throws AGDMException {
    	NodeList nodes = find(block, "descendant-or-self::mt:math/mt:apply/mt:eq/following-sibling::*[2]/descendant-or-self::mt:ci");
    	ArrayList<String> usedVars = new ArrayList<String>();
        for (int i = 0; i < nodes.getLength(); ++i) {
        	Node var = nodes.item(i);
        	//Remove shared variable tag
        	if (var.getLocalName().equals("sharedVariable")) {
        		var = var.getFirstChild();
        	}
        	//Remove index if necessary
        	if (var.getLocalName().equals("apply") && !withIndex) {
        		var = var.getFirstChild();
        	}
        	String varName = xmlVarToString(var, false, false);
        	if (!usedVars.contains(varName)) {
        		usedVars.add(varName);
        	}
        }
        return usedVars;
    }
    
    /**
     * Get all variable names that are used in the xml indicated by the query.
     * @param block				The query limits
     * @param query				Query
     * @return					Variable names
     * @throws AGDMException	AGDM00X - External error
     */
    public static HashSet<String> getUsedVars(Node block, String query) throws AGDMException {
    	HashSet<String> usedVars = new LinkedHashSet<String>();
    	NodeList nodes = find(block, query);
        for (int i = 0; i < nodes.getLength(); ++i) {
    		NodeList vars = ((Element) nodes.item(i)).getElementsByTagName("mt:ci");
			for (int j = 0; j < vars.getLength(); j++) {
				usedVars.add(vars.item(j).getTextContent());
			}
        }
        return usedVars;
    }
    
    /**
     * Checks if the variables assigned in a block is read in the same loop with a different index.
     * @param block				The block to check
     * @return					True if there is at least one self-dependecy
     * @throws AGDMException 	AGDM00X - External error
     */
    public static boolean hasSelfDependencies(Element block) throws AGDMException {
    	ArrayList<NodeWrap> assignedVars = getAssignedVars(block, false);
    	ArrayList<NodeWrap> usedVars = getUsedVars(block, true);
    	//Check only used variables with index incremented or decremented
    	for (int i = 0; i < usedVars.size(); i++) {
    		boolean modifiedIndex = false;
    		Node usedVar = usedVars.get(i).getNode();
    		if (usedVar.hasChildNodes()) {
	    		Node indexCoord = usedVar.getFirstChild().getNextSibling();
	    		while (indexCoord != null && !modifiedIndex) {
	    			if (!indexCoord.getLocalName().equals("ci")) {
	    				modifiedIndex = true;
	    			}
	    			indexCoord = indexCoord.getNextSibling();
	    		}
	    		//Check if the variable with modified index is assigned in the same loop
	    		if (modifiedIndex && assignedVars.contains(new NodeWrap(usedVar.getFirstChild()))) {
	    			return true;
	    		}
    		}
    	}
        return false;
    }
    
    /**
     * Orders the equation term according to dependencies.
     * @param derivativeLevels		The derivative levels
     * @param multiplicationLevels	Multiplication term levels
     * @param equationTerms			Equations
     * @return						Ordered equations
     * @throws AGDMException		AGDM008 Invalid simulation problem
     * 								AGDM00X External error
     */
    public static EquationTermsLevel orderEquationTermDependencies(ArrayList<DerivativeLevel> derivativeLevels, ArrayList<MultiplicationTermsLevel> multiplicationLevels, EquationTermsLevel equationTerms) throws AGDMException {
    	EquationTermsLevel newEquationTerms = new  EquationTermsLevel();
    	//Get equation dependencies and definitions
    	HashMap<String, Set<String>> dependencies = new LinkedHashMap<String, Set<String>>();
    	HashMap<String, Set<String>> definitions = new HashMap<String, Set<String>>();
    	Set<String> globalDefinitions = new HashSet<String>();
    	for (int i = 0; i < equationTerms.getEquations().size(); i++) {
    		ArrayList<String> fields = equationTerms.getEquations().get(i).getFields();
    		Set<String> eqDependencies = new LinkedHashSet<String>();
    		Set<String> eqDefinitions = new LinkedHashSet<String>(); 
        
        	String eqID = fields.stream().map(Object::toString).collect(Collectors.joining("_"));;
        	
        	EquationType auxiliaryFieldType = equationTerms.getEquations().get(i).getEquationType();
        	//Take into account the algorithmic definitions
        	if (auxiliaryFieldType == algorithmic) {
        		NodeList vars = equationTerms.getEquations().get(i).getAlgorithm().getElementsByTagName("mt:ci");
				for (int j = 0; j < vars.getLength(); j++) {
					String var = vars.item(j).getTextContent();
					eqDependencies.add(var);
				}
        	}
    		//Get dependencies and definitions of derivative levels from current equation
    		for (int l = 0; l < derivativeLevels.size(); l++) {
    			ArrayList<Derivative> derivatives = derivativeLevels.get(l).getDerivatives();
	            for (int j = 0; j < derivatives.size(); j++) {
	            	if (fields.contains(derivatives.get(j).getField())) {
	    				if (derivatives.get(j).getAlgebraicExpression() != null) {
			    			NodeList vars = ((Element) derivatives.get(j).getAlgebraicExpression()).getElementsByTagName("mt:ci");
			    			for (int k = 0; k < vars.getLength(); k++) {
			    				eqDependencies.add(vars.item(k).getTextContent());
			    			}
	    				}
	    				if (derivatives.get(j).getDerivativeTerm().isPresent()) {
		    				NodeList vars = ((Element) derivatives.get(j).getDerivativeTerm().get()).getElementsByTagName("mt:ci");
			    			for (int k = 0; k < vars.getLength(); k++) {
			    				eqDependencies.add(vars.item(k).getTextContent());
			    			}
	    				}
	    			    eqDefinitions.add(derivatives.get(j).getId());
	            	}
	            }
    		}
    		//Get dependencies and definitions of multiplication levels from current equation
    		for (int l = 0; l < multiplicationLevels.size(); l++) {
    			ArrayList<MultiplicationTerm> multTerms = multiplicationLevels.get(l).getMultiplicationTerms();
	            for (int j = 0; j < multTerms.size(); j++) {
	              	if (fields.contains(multTerms.get(j).getField())) {
		        		NodeList vars = ((Element) multTerms.get(j).getAlgebraicExpression()).getElementsByTagName("mt:ci");
		    			for (int k = 0; k < vars.getLength(); k++) {
		    				eqDependencies.add(vars.item(k).getTextContent());
		    			}
		    			eqDependencies.addAll(multTerms.get(j).getVariables());
					    eqDefinitions.add(multTerms.get(j).getId());
	              	}
	            }
    		}
		    eqDefinitions.addAll(fields);
    		//Remove self dependencies
    		eqDependencies.removeAll(eqDefinitions);
    		//Get dependencies from current equation
    		eqDependencies.addAll(equationTerms.getEquations().get(i).getTerms());
    		dependencies.put(eqID, eqDependencies);
    		definitions.put(eqID, eqDefinitions);
    		//Add global definitions
    		globalDefinitions.addAll(fields);
    		globalDefinitions.addAll(eqDefinitions);
    	}
    	//Remove external dependencies
    	Iterator<String> fields = dependencies.keySet().iterator();
    	HashMap<String, Integer> marks = new HashMap<String, Integer>();
    	while (fields.hasNext()) {
    		String field = fields.next();
    		Set<String> fieldDeps = dependencies.get(field);
    		fieldDeps.retainAll(globalDefinitions);
    		dependencies.put(field, fieldDeps);
    		marks.put(field, 0);
    	}
    	
    	//Do the ordering (Depth-first search)
    	String node = getUnmarkedNode(marks);
    	while (node != null) {
    	    visit(node, marks, newEquationTerms, equationTerms, dependencies, definitions);
    	    node = getUnmarkedNode(marks);
    	}
    	return newEquationTerms;
    }
    
    /**
     * Part of depth-first topological ordering algorithm. 
     * @param marks		The marks of the nodes
     * @return			An unmarked node
     */
    private static String getUnmarkedNode(HashMap<String, Integer> marks) {
    	Iterator<String> fields = marks.keySet().iterator();
    	while (fields.hasNext()) {
    		String field = fields.next();
    		if (marks.get(field) == 0) {
    			return field;
    		}
    	}
    	return null;
    }
    
    /**
     * Checks if m depends on n.
     * @param n				Node n
     * @param m				Node m
     * @param dependencies	Dependencies from all nodes
     * @param definitions	Definitions of all nodes
     * @return				True if m depends on n
     */
    private static boolean depends(String n, String m, HashMap<String, Set<String>> dependencies, HashMap<String, Set<String>> definitions) {
    	Set<String> mDependencies = new HashSet<String>(dependencies.get(m));
    	Set<String> nDefinitions = new HashSet<String>(definitions.get(n));
    	//Intersect dependencies from m with definitions of n
    	mDependencies.retainAll(nDefinitions);
    	return !mDependencies.isEmpty();
    }
    
    /**
     * Depth-first search algorithm recursive method.
     * @param n					Current node to visit
     * @param marks				The marks of the nodes
     * @param orderedEqs		The list of ordered equations
     * @param unorderedEqs		The whole list of equations
     * @param dependencies		Dependencies from all nodes
     * @param definitions		Definitions of all nodes
     * @throws AGDMException 	AGDM008 Invalid simulation problem
     */
    private static void visit(String n, HashMap<String, Integer> marks, EquationTermsLevel orderedEqs, 
    		EquationTermsLevel unorderedEqs, HashMap<String, Set<String>> dependencies, HashMap<String, Set<String>> definitions) throws AGDMException {
	    //if n has a permanent mark then return
    	if (marks.get(n) == 2) {
    		return;
    	}
	    //if n has a temporary mark then stop (not a DAG)
    	if (marks.get(n) == 1) {
    		throw new AGDMException(AGDMException.AGDM008, 
        			"Auxiliary variables have cyclical dependencies.");
    	}
    	//mark n temporarily
    	marks.put(n, 1);
	    //for each node m with an edge from n to m do visit(m)
    	Iterator<String> fields = marks.keySet().iterator();
    	while (fields.hasNext()) {
    		String m = fields.next();
    		if (!n.equals(m)) {
    			if (depends(n, m, dependencies, definitions)) {
    				visit(m, marks, orderedEqs, unorderedEqs, dependencies, definitions);
    			}
    		}
    	}
	    //mark n permanently
    	marks.put(n, 2);
	    //add n to head of L
    	orderedEqs.add(unorderedEqs.getEquationsForId(n), 0);
    }
        
    /**
     * Creates all the combinations in n-dimensions for a list of elements.
     * @param dimensions    The dimensions
     * @param elements      The elements
     * @return              The combinations
     */
    public static ArrayList<String> createCombinations(int dimensions, ArrayList<String> elements) {
        ArrayList<String> combinations = new ArrayList<String>();
        for (int i = 0; i < dimensions; i++) {
            if (i == 0) {
                for (int j = 0; j < elements.size(); j++) {
                    combinations.add(elements.get(j));
                }
            }
            else {
                ArrayList<String> combinationsTmp = new ArrayList<String>();
                for (int j = 0; j < combinations.size(); j++) {
                    for (int k = 0; k < elements.size(); k++) {
                        String combination = combinations.get(j) + elements.get(k);
                        combinationsTmp.add(combination);
                    }
                }
                combinations.clear();
                combinations.addAll(combinationsTmp);
            }
        }
        return combinations;
    }
    
    /**
     * Replaces the external variable in transformation rules with given one.
     * @param scope				The transformation rule
     * @param newVar			The new variable
     * @param oldVar			The external variable
     * @throws AGDMException	AGDM00X External error
     */
    public static void replaceTransformationRuleExternalVariable(Node scope, Node newVar, Node oldVar) throws AGDMException {
    	Document owner = scope.getOwnerDocument();
    	boolean isShared = newVar.getFirstChild().getLocalName().equals("sharedVariable");
    	NodeList candidates = find(scope, printTree(oldVar.getFirstChild(), 0));
    	for (int i = candidates.getLength() - 1; i >= 0; i--) {
    		Element candidate = (Element) candidates.item(i);
    		Element parent = (Element) candidates.item(i).getParentNode();
    		if (isShared) {
    			if (parent.getLocalName().equals("apply") && parent.getFirstChild().isSameNode(candidate)) {
    				Element parentClone = (Element) parent.cloneNode(true);
    				parentClone.replaceChild(parentClone.getOwnerDocument().importNode(newVar.getFirstChild().getFirstChild().cloneNode(true), true), parentClone.getFirstChild());
        			Element sharedVar = (Element) newVar.getFirstChild().cloneNode(false);
        			sharedVar.appendChild(sharedVar.getOwnerDocument().importNode(parentClone, true));
        			Element parentsParent = (Element) parent.getParentNode();
        			parentsParent.replaceChild(parent.getOwnerDocument().importNode(sharedVar, true), parent);
        		} 
    			else {
    				parent.replaceChild(owner.importNode(newVar.getFirstChild().cloneNode(true), true), candidate);
        		}
    		}
    		else {
    			parent.replaceChild(owner.importNode(newVar.getFirstChild().cloneNode(true), true), candidate);
    		}
    	}
    }
    
    /**
     * Removes the special characters from a input string.
     * @param input		The input string
     * @return			Clean output
     */
    public static String removeSpecialCharacters(String input) {
        final int variableLimit = 255;
        //The first character must be a letter
        while (AGDMUtils.isNumber(input.substring(0, 1))) {
        	input = input.substring(1);
        }
        //The remaining characters, if any, may be letters, digits, or underscores
        input = input.replaceAll("[^a-zA-Z0-9_]", "");
        //Check length
        if (input.length() > variableLimit) {
        	input = input.substring(0, variableLimit - 1);
        }
        return input;
    }
    
    /**
     * Unifies two fragments with code (Iterate over iterations). 
     * In case of different length, the codes are aligned to last.
     * @param a		fragment a
     * @param b		fragment b
     * @return		aligned fragment codes
     */
    public static DocumentFragment unifyCodeFragments(Node a, Node b) {
    	DocumentFragment result = a.getOwnerDocument().createDocumentFragment();
    	int maxA = a.getChildNodes().getLength();
    	int maxB = b.getChildNodes().getLength();
    	int maxSize = Math.max(maxA, maxB);
    	int startA = Math.max(0, maxB - maxA);
    	int startB = Math.max(0, maxA - maxB);
    	NodeList childrenA = a.getChildNodes();
    	NodeList childrenB = b.getChildNodes();
    	for (int i = 0; i < maxSize; i++) {
    		Node node = null;
    		if (i >= startA) {
    			node = childrenA.item(i - startA).cloneNode(true);
    			if (i >= startB) {
    				NodeList children = childrenB.item(i - startB).getChildNodes();
    				for (int j = 0; j < children.getLength(); j++) {
    					node.appendChild(children.item(j));
    				}
    			}
    		}
    		else {
    			node = childrenB.item(i - startB).cloneNode(true);
    		}
    		result.appendChild(node);
    	}
    	return result;
    }
    
    /**
     * Get numerical constants from a document.
     * @param document			Document to extract constants from
     * @param constantInfo		Constant information
     * @throws AGDMException	AGDM00X External error
     */
    public static void getConstants(Node document, ConstantInformation constantInfo) throws AGDMException {
    	NodeList found = find(document, "//mms:constant");
    	for (int i = 0; i < found.getLength(); i++) {
    		String name = found.item(i).getFirstChild().getTextContent();
    		double value = Double.parseDouble(found.item(i).getLastChild().getTextContent());
    		String constantName = constantInfo.addConstant(name, value);
    		//If the constant has been renamed, apply in the document.
    		if (!constantName.equals(name)) {
    			NodeList vars = find(document, "//mt:ci[text() = '" + name + "']");
    			for (int j = 0; j < vars.getLength(); j++) {
    				vars.item(j).setTextContent(constantName);
    			}
    		}
    	}
    }
}
