/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.AGDM.utils.PDE;

import java.util.ArrayList;

import org.w3c.dom.Node;

/**
 * The information needed to process an equation.
 * @author bminyano
 *
 */
public class MultiplicationTermsLevel {

    private ArrayList<MultiplicationTerm> multiplicationTerms;
    
    /**
     * Constructor.
     */
    public MultiplicationTermsLevel() {
        multiplicationTerms = new ArrayList<MultiplicationTerm>();
    }
    
    /**
     * Copy constructor.
     * @param mtl   The object to copy
     */
    public MultiplicationTermsLevel(MultiplicationTermsLevel mtl) {
        multiplicationTerms = new ArrayList<MultiplicationTerm>();
    	for (MultiplicationTerm multTermLevel : mtl.getMultiplicationTerms()) {
    		multiplicationTerms.add(new MultiplicationTerm(multTermLevel));
    	}
    }
    
    /**
     * Copy constructor.
     * @param mtl   The object to copy
     */
    public MultiplicationTermsLevel(ArrayList<MultiplicationTerm> mtl) {
        multiplicationTerms = new ArrayList<MultiplicationTerm>();
    	for (MultiplicationTerm multTermLevel : mtl) {
    		multiplicationTerms.add(new MultiplicationTerm(multTermLevel));
    	}
    }
   
    /**
     * Adds a new multiplication term to the list.
     * @param field         The variable for the multiplication
     * @param id            The identifier of the multilplication term
     * @param terms         The derivative terms in the multiplication
     * @param expression    The algebraic expression in the multiplication
     * @param opType        The operator type
     */
    public void add(String field, String id, ArrayList<String> terms, Node expression, String opType, Node conditional) {
        multiplicationTerms.add(new MultiplicationTerm(field, id, terms, expression, opType, conditional));
    }
    
    /**
     * Adds a new multiplication term to the list.
     * @param multTerm	The new multiplication term
     */
    public void add(MultiplicationTerm multTerm) {
        multiplicationTerms.add(new MultiplicationTerm(multTerm));
    }
    
    /**
     * Adds a new multiplication term to the list.
     * @param multTerm	The new multiplication term
     * @param position	the position
     */
    public void add(MultiplicationTerm multTerm, int position) {
        multiplicationTerms.add(position, new MultiplicationTerm(multTerm));
    }

    public ArrayList<MultiplicationTerm> getMultiplicationTerms() {
        return multiplicationTerms;
    }

    /**
     * Gets the multiplication terms for a given field.
     * @param field     The field
     * @return          The multiplication terms of the field
     */
    public ArrayList<MultiplicationTerm> getFilteredMultiplicationTerm(String field) {
        ArrayList<MultiplicationTerm> filteredMultiplicationTerm = new ArrayList<MultiplicationTerm>();
        for (int i = 0; i < multiplicationTerms.size(); i++) {
            if (multiplicationTerms.get(i).getField().equals(field)) {
                filteredMultiplicationTerm.add(multiplicationTerms.get(i));
            }
        }
        return filteredMultiplicationTerm;
    }
    
    @Override
    public String toString() {
        return "MultiplicationTermsLevel [multiplicationTerms="
                + multiplicationTerms + "]";
    }
    
}
