/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.AGDM;

import eu.iac3.mathMS.AGDM.processors.Boundary;
import eu.iac3.mathMS.AGDM.processors.ExecutionFlow;
import eu.iac3.mathMS.AGDM.processors.Extrapolation;
import eu.iac3.mathMS.AGDM.processors.Fluxes;
import eu.iac3.mathMS.AGDM.processors.Interpolation;
import eu.iac3.mathMS.AGDM.processors.SchemaCreator;
import eu.iac3.mathMS.AGDM.utils.AGDMUtils;
import eu.iac3.mathMS.AGDM.utils.ConstantInformation;
import eu.iac3.mathMS.AGDM.utils.CoordinateInfo;
import eu.iac3.mathMS.AGDM.utils.DiscretizationPolicy;
import eu.iac3.mathMS.AGDM.utils.DiscretizationType;
import eu.iac3.mathMS.AGDM.utils.ImportCall;
import eu.iac3.mathMS.AGDM.utils.IncompatibilityFields;
import eu.iac3.mathMS.AGDM.utils.ReferenceFrameType;
import eu.iac3.mathMS.AGDM.utils.RuleInfo;
import eu.iac3.mathMS.AGDM.utils.PDE.PDEAnalysisDiscPolicy;
import eu.iac3.mathMS.AGDM.utils.PDE.OperatorDiscretization;
import eu.iac3.mathMS.AGDM.utils.PDE.OperatorPolicy;
import eu.iac3.mathMS.AGDM.utils.PDE.OperatorsInfo;
import eu.iac3.mathMS.AGDM.utils.PDE.PDERegionDiscPolicy;
import eu.iac3.mathMS.AGDM.utils.PDE.ProcessInfo;
import eu.iac3.mathMS.AGDM.utils.PDE.SpatialOperatorDiscretization;
import eu.iac3.mathMS.AGDM.utils.PDE.Species;
import eu.iac3.mathMS.AGDM.utils.RegionInfo;
import eu.iac3.mathMS.documentManagerPlugin.DMException;
import eu.iac3.mathMS.documentManagerPlugin.DocumentManager;
import eu.iac3.mathMS.documentManagerPlugin.DocumentManagerImpl;
import eu.iac3.mathMS.simflownyUtilsPlugin.SUException;
import eu.iac3.mathMS.simflownyUtilsPlugin.SimflownyUtils;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map.Entry;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.log.LogService;
import org.osgi.util.tracker.ServiceTracker;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**  
 * Automated Generator of Discretized Models is an interface that provides the functionality that converts a physical 
 * problem into a discretized problem with a discretization schema and its parameters.
 * The interface has functions which accept XML as input and returns xml as the discretization result.
 * 
 * @author      ----
 * @version     ----
 */
@Component //
(//
    immediate = true, //
    name = "AGDM", //
    property = //
    { //
      "service.exported.interfaces=*", //
      "service.exported.configs=org.apache.cxf.ws", //
      "org.apache.cxf.ws.address=/AGDM" //
    } //
)
public class AGDMImpl implements AGDM {
    static LogService logservice = null;
    public static BundleContext context;
    static String mmsUri = "urn:mathms";
    static String smlUri = "urn:simml";
    static String mtUri = "http://www.w3.org/1998/Math/MathML";
    static HashMap<String, Integer> derivativeLevels;
    static HashMap<String, Integer> auxDerivativeLevels;
    public static ArrayList<String> normalizationSchemaId;
    public static HashMap<String, String> normalizationEquivalences;
    public static boolean hasMeshInterpolation;
    public static boolean hasParticleInterpolation;
    
    /**
     * Constructor.
     * 
     * @throws AGDMException 
     */
	public AGDMImpl() throws AGDMException {
        super();
        context = FrameworkUtil.getBundle(getClass()).getBundleContext();
        new AGDMUtils();
        
        //Getting the log service
        @SuppressWarnings("rawtypes")
		ServiceTracker logServiceTracker = new ServiceTracker(context, org.osgi.service.log.LogService.class.getName(), null);
        logServiceTracker.open();
        logservice = (LogService) logServiceTracker.getService();
        
    }
    /** 
     * It returns the result of discretize the physical problem. 
     * It checks the document against its schema and applies a preprocess to prepare the problem for discretization schemas.
     * 
     * @param discPolicy            the discretization policy for the problem
     * @return                      the resulting json file as a String
     * @throws AGDMException        AGDM001  XML document does not match XML schema
     *                              AGDM002  XML discretization schema not valid
     *                              AGDM003  Import discretization schema not exist
     *                              AGDM004  Received parameters do not match schema parameters
     *                              AGDM005  Not a valid XML document
     *                              AGDM006  Schema not applicable to this problem
     *                              AGDM007  External error
     */
    public String discretizeProblem(String discPolicy)throws AGDMException {
        if (logservice != null) {
            logservice.log(LogService.LOG_INFO, "Discretizing problem");
        }
        try {
            hasMeshInterpolation = false;
            hasParticleInterpolation = false;
        	derivativeLevels = new HashMap<String, Integer>();
        	auxDerivativeLevels = new HashMap<String, Integer>();
        	normalizationSchemaId = new ArrayList<String>();
        	normalizationEquivalences = new HashMap<String, String>();
        	//Discretization auxiliary inicialization
        	AGDMUtils.schemaNameCounter = 0;
        	SchemaCreator.maxStencil = 0;
        	
        	ConstantInformation constants = new ConstantInformation();
        	
            //Conversion and validation
            String policyString = SimflownyUtils.jsonToXML(discPolicy);
            Document policyDocument = AGDMUtils.stringToDom(policyString);
            
            AGDMUtils.schemaValidation(policyString, policyDocument.getDocumentElement().getLocalName() + ".xsd");
            //Get constant information from policy
            AGDMUtils.getConstants(policyDocument, constants);
            DiscretizationPolicy policy = new DiscretizationPolicy(policyDocument);
            //Getting problem
            DocumentManager docMan = new DocumentManagerImpl();
            String simulationProblem;
            try {
                simulationProblem = SimflownyUtils.jsonToXML(docMan.getDocument(policy.getProblem()));
            }
            catch (DMException e1) {
                  throw new AGDMException(AGDMException.AGDM00X, e1.getMessage());
            }
            //Problem validations
            //XML
            Document problemDocument = AGDMUtils.stringToDom(simulationProblem);
            //Get constant information from problem
            AGDMUtils.getConstants(problemDocument, constants);
            AGDMUtils.schemaValidation(simulationProblem, problemDocument.getDocumentElement().getLocalName() + ".xsd");
            
            //Simflowny
            AGDMUtils.simflownyValidation(AGDMUtils.stringToDom(simulationProblem));

            //Creation of the problem result skeleton
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document result = builder.newDocument();
            
            //ModelCharDecompositions, previous to import equationset
            Element mcd = createModelCharDecomposition(result, problemDocument);
            
            //The preprocess method merge the problem with the models used in the same document
            //It also replaces the utilization of auxiliary fields by their equations in the characteristic decompositions
            Document problem = AGDMUtils.preprocess(simulationProblem, constants);

            //Get reference frame for fields
            HashMap<String, ReferenceFrameType> referenceFrames = getFieldReferenceFrame(problem);
            //Improves the performance of the problem
            AGDMUtils.improvePerformance(problem);
            //Check the policy
            NodeList fieldList = AGDMUtils.find(problem, "/*/mms:fields/mms:field|/*/mms:auxiliaryFields/mms:auxiliaryField");
            ArrayList<String> problemFields = new ArrayList<String>();
            for (int j = 0; j < fieldList.getLength(); j++) {
            	problemFields.add(fieldList.item(j).getTextContent());
            }
            NodeList coordList = AGDMUtils.find(problem, "/mms:coordinates//mms:spatialCoordinate");
            ArrayList<String> spatialCoords = new ArrayList<String>();
            for (int j = 0; j < coordList.getLength(); j++) {
            	spatialCoords.add(coordList.item(j).getTextContent());
            }
            HashMap<String, ArrayList<String>> regionsAndFieldGroups = new HashMap<String, ArrayList<String>>(); 
            NodeList regions = AGDMUtils.find(problem, "//mms:region|//mms:subregion");
            for (int j = 0; j < regions.getLength(); j++) {
            	Element region = (Element) regions.item(j);
            	String regionName = region.getElementsByTagNameNS(mmsUri, "name").item(0).getTextContent();
            	NodeList fieldGroups = region.getElementsByTagNameNS(mmsUri, "fieldGroup");
            	ArrayList<String> groups = new ArrayList<String>();
            	for (int k = 0; k < fieldGroups.getLength(); k++) {	
            		groups.add(fieldGroups.item(k).getFirstChild().getTextContent());
            	}
            	regionsAndFieldGroups.put(regionName, groups);
            }
            policy.createRegionInformations(problem);
            policy.createStepFieldTemplate();
            policy.checkPolicy(spatialCoords, problemFields, referenceFrames, regionsAndFieldGroups);
            
            //Root
            Element root = AGDMUtils.createElement(result, mmsUri, "discretizedProblem");
            result.appendChild(root);
            //Header
            makeHeader(result, problem);
            
            //Field relation with continuous coordinates 
            //Copy fields, parameters, discretized coordinates and deploy discretized coordinates for the fields
            CoordinateInfo ci = processFieldsAndCoordinates(result, problem, policy);
            //Process initial conditions
            ProcessInfo pi = createVerySimpleProcessInfo(problem, result, ci, policy, referenceFrames, constants);
            processInitialConditions(problem, policy, pi);
            
            //Deploy coordinates for fields on initial conditions
            deployCoordsOnProblem(problem, ci);
            
            //Discretize coordinates on mathml
            coordinateTransform(problem, ci, policy);
            
            //Process discretization policy
            processDiscretizationPolicy(problem, policy, ci);
     
            //Process boundary conditions
            processBoundaryConditions(result, problem, ci, policy, referenceFrames, constants);
            
            //modelCharDecompositions, previously generated
            if (mcd.hasChildNodes()) {
                root.appendChild(mcd);
            }
            
            //Regions
            DocumentFragment regionFragment = getRegions((Document) problem.cloneNode(true));
            root.appendChild(result.importNode(regionFragment, true));
            
            //Region precedence
            NodeList precedence = AGDMUtils.find(problem, "//mms:subregionPrecedence");
            if (precedence.getLength() > 0) {
                root.appendChild(result.importNode(precedence.item(0), true));
            }
                        
            //sml:boundary conditions
            Element boundaries = (Element) (AGDMUtils.find(problem, "//mms:boundaryConditions")).item(0);
            root.appendChild(result.importNode(boundaries.cloneNode(true), true));
            
            //Boundaries precedence
            NodeList boundPrec = AGDMUtils.find(problem, "//mms:boundaryPrecedence");
            if (boundPrec.getLength() > 0) {
                root.appendChild(result.importNode(boundPrec.item(0), true));
            }
            
            //Finalization conditions
            Element conditions = (Element) (AGDMUtils.find(problem, "//mms:finalizationConditions")).item(0);
            root.appendChild(result.importNode(conditions, true));
            
            //Functions used commonly
            Element functions = AGDMUtils.createElement(result, mmsUri, "functions");
            root.appendChild(functions);
           
            //Get incompatible region information
            HashMap<String, IncompatibilityFields> incompFields = getIncompatibleFields(result, problem, ci, policy.getMeshFields());
           
            //Check if the problem has fluxes
            boolean hasFluxes = AGDMUtils.checkIfHasFluxes(problem);
            //Process every discretization policy
            ProcessInfo.setNumberOfRegions(regions.getLength());
            SchemaCreator sc = new SchemaCreator();
            for (int i = 0; i < policy.getRegionDiscPolicies().size(); i++) {
            	PDERegionDiscPolicy rdp = policy.getRegionDiscPolicies().get(i);
            	String name = rdp.getRegionName();
                processRegionDiscretizationPDE(result, problem, rdp, functions, policy, ci, incompFields.get(name), sc, hasFluxes, referenceFrames, constants);
            }
            
            //Analysis
            processAnalysis(problem, result, ci, policy, sc, referenceFrames, constants);
            
            //Add extrapolation functions to functions tag
            if (hasExtrapolation(policy, incompFields)) {
                ArrayList<String> coords = AGDMUtils.getBaseDiscSpatialCoordinate(result);
                functions.appendChild(Extrapolation.createExtrapolationFunctions(result, coords, policy.getExtrapolationMethod(), 
                        ci, hasFluxes));
            }
            
            //Add interpolation functions to functions tag
            if (hasMeshInterpolation) {
                ArrayList<String> coords = AGDMUtils.getBaseDiscSpatialCoordinate(result);
                functions.appendChild(Interpolation.createMeshInterpolationFunction(result, coords, policy.getMeshInterpolationMethod()));
            }
            
            //Extrapolation method
            if (hasExtrapolation(policy, incompFields)) {
                Element extrapolationMethod = AGDMUtils.createElement(result, mmsUri, "extrapolationMethod");
                Element name = AGDMUtils.createElement(result, mmsUri, "name");
                name.setTextContent(policy.getExtrapolationMethod().toString());
                Element stencil = AGDMUtils.createElement(result, mmsUri, "stencil");
                int param = policy.getExtrapolationStencil();
                stencil.setTextContent(String.valueOf(param));
                extrapolationMethod.appendChild(name);
                extrapolationMethod.appendChild(stencil);
                root.appendChild(extrapolationMethod); 
            }
            
            //Region relation
            if (regions.getLength() > 1) {
             	root.appendChild(result.importNode(generateRegionRelation(regions, problem, policy), true));
            }
            
            //Post processing
            AGDMUtils.postProcess(result, policy, AGDMUtils.getBaseDiscSpatialCoordinate(result), derivativeLevels, auxDerivativeLevels);
            
            //Field discretization types
            Element fieldDiscretizationTypes = AGDMUtils.createElement(result, mmsUri, "fieldDiscretizationTypes");
            if (policy.getMeshFields().size() > 0) {
            	Element fieldDiscretizationType = AGDMUtils.createElement(result, mmsUri, "fieldDiscretizationType");
            	Element discretizationType = AGDMUtils.createElement(result, mmsUri, "discretizationType");
            	Element fvm = AGDMUtils.createElement(result, mmsUri, "FVM");
            	fieldDiscretizationType.appendChild(discretizationType);
            	discretizationType.appendChild(fvm);
            	Element fields = AGDMUtils.createElement(result, mmsUri, "fields");
            	fieldDiscretizationType.appendChild(fields);
            	for (String f : policy.getMeshFields()) {
            		Element field = AGDMUtils.createElement(result, mmsUri, "field");
            		field.setTextContent(f);
            		fields.appendChild(field);
            	}
            	fieldDiscretizationTypes.appendChild(fieldDiscretizationType);
            }
            for (Entry<String, Species> entry : policy.getParticleSpecies().entrySet()) {
            	Element fieldDiscretizationType = AGDMUtils.createElement(result, mmsUri, "fieldDiscretizationType");
            	Element discretizationType = AGDMUtils.createElement(result, mmsUri, "discretizationType");
            	Element particles = AGDMUtils.createElement(result, mmsUri, "particles");
            	particles.setTextContent(entry.getKey());
            	fieldDiscretizationType.appendChild(discretizationType);
            	discretizationType.appendChild(particles);
            	Element fields = AGDMUtils.createElement(result, mmsUri, "fields");
            	fieldDiscretizationType.appendChild(fields);
            	for (String f : entry.getValue().getParticleFields()) {
            		Element field = AGDMUtils.createElement(result, mmsUri, "field");
            		field.setTextContent(f);
            		fields.appendChild(field);
            	}
            	fieldDiscretizationTypes.appendChild(fieldDiscretizationType);
            }
            root.appendChild(fieldDiscretizationTypes);
            //Discretization Policy Id
            Element discretizationPolicyId = AGDMUtils.createElement(result, mmsUri, "discretizationPolicyId");
            discretizationPolicyId.setTextContent(((Element) policyDocument.getFirstChild()).getElementsByTagNameNS(mmsUri, "id").item(0).getTextContent());
            root.appendChild(discretizationPolicyId);
            
            //Adding continuous problem id
            Element continuousProblemId = AGDMUtils.createElement(result, mmsUri, "continuousProblemId");
            continuousProblemId.setTextContent(problemDocument.getElementsByTagNameNS(mmsUri, "id").item(0).getTextContent());
            root.appendChild(continuousProblemId);
            
            //Adding all constant variables
            if (!constants.getConstants().isEmpty()) {
            	HashMap<String, Double> constantInfo = constants.getConstants(); 
                Element constantsEl = AGDMUtils.createElement(result, mmsUri, "constants");
                Iterator<String> constantNameIt = constantInfo.keySet().iterator();
                while (constantNameIt.hasNext()) {
                	String name = constantNameIt.next();
                	double value = constantInfo.get(name);
                    Element constantEl = AGDMUtils.createElement(result, mmsUri, "constant");
                    Element nameEl = AGDMUtils.createElement(result, mmsUri, "name");
                    nameEl.setTextContent(name);
                    Element valueEl = AGDMUtils.createElement(result, mmsUri, "value");
                    valueEl.setTextContent(String.valueOf(value));
                    constantsEl.appendChild(constantEl);
                    constantEl.appendChild(nameEl);
                    constantEl.appendChild(valueEl);
                }
                root.appendChild(constantsEl);
            }
            
            
            AGDMUtils.stringToDom(AGDMUtils.domToString(result));
            return SimflownyUtils.xmlToJSON(AGDMUtils.domToString(result), false);
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            throw new AGDMException(AGDMException.AGDM00X, e.getMessage());
        }
    }
 
    /**
     * Process the interior and surface of the region.
     * @param result                The result of the discretization
     * @param problem               The problem data
     * @param region                The region to process
     * @param functions             The functions element tag
     * @param sc					Schema creator
     * @param hasFluxes				If the model has fluxes
     * @param policy                the discretization policy for the problem
     * @param relations             The coordinate and region relations
     * @param incompFields          The incompatible fields for the region
     * @param referenceFrames		Reference frame for fields
     * @param constantInfo			Constant information
     * @throws AGDMException        AGDM003  Import discretization schema not exist
     *                              AGDM006  Schema not applicable to this problem
     */
    private void processRegionDiscretizationPDE(Document result, Document problem, PDERegionDiscPolicy regionPolicy, Element functions, DiscretizationPolicy policy, 
            CoordinateInfo ci, IncompatibilityFields incompFields, SchemaCreator sc, boolean hasFluxes,
            HashMap<String, ReferenceFrameType> referenceFrames, ConstantInformation constants) throws AGDMException {
        SchemaCreator.maxStepStencil = 0;
        String regionName = regionPolicy.getRegionName();
        //Take schema and parameters for the region
        Element region = (Element) AGDMUtils.find(result, "//mms:region[mms:name = '" + regionName + "']|//mms:subregion[mms:name = '" + regionName + "']").item(0);
        
        RegionInfo regionInfo = regionPolicy.getRegionInfo();
        String groupSuffix = regionPolicy.getFieldGroupSuffix();
        
        //Creation of the schema
        boolean extrapolation = hasExtrapolation(policy, incompFields);

        //Process interior
        DocumentFragment regionProblem = null;
        Element interiorPI = AGDMUtils.createElement(result, smlUri, "simml");
        Element surfacePI = AGDMUtils.createElement(result, smlUri, "simml");
        Element interiorAlgorithm = AGDMUtils.createElement(result, mmsUri, "interiorAlgorithm");;
        Element surfaceAlgorithm = AGDMUtils.createElement(result, mmsUri, "surfaceAlgorithm");;
        //Load time schema
        Document schema = null;
        try {
            DocumentManager docMan = new DocumentManagerImpl();
            String schemaString = SimflownyUtils.jsonToXML(docMan.getDocument(regionPolicy.getTimeSchema()));
            schema = AGDMUtils.stringToDom(schemaString);
            AGDMUtils.schemaValidation(schemaString, schema.getDocumentElement().getLocalName() + ".xsd");
            //Get constant information from schema
            AGDMUtils.getConstants(schema, constants);
            AGDMUtils.schemaPreprocess(schema, String.valueOf(regionPolicy.getSchemaId()), extrapolation);
        } 
        catch (SUException e) {
            throw new AGDMException(AGDMException.AGDM002, "Time schema with id " + regionPolicy.getTimeSchema() + " not valid.");
        }
        catch (DMException e) {
            throw new AGDMException(AGDMException.AGDM003, "Time schema with id " + regionPolicy.getTimeSchema() + " does not exist.");
        }
        //Add discretization information Element
        NodeList discretizationInfos = region.getElementsByTagNameNS(mmsUri, "discretizationInfo");
        Element discretizationInfo = null;
        if (discretizationInfos.getLength() == 0) {
        	discretizationInfo = AGDMUtils.createElement(result, mmsUri, "discretizationInfo");
        	region.appendChild(discretizationInfo);
        }
        else {
        	discretizationInfo = (Element) discretizationInfos.item(0);
        }
        // Process Interior 
        if (region.getElementsByTagNameNS(mmsUri, "interiorModel").getLength() > 0) {
            //Create a document with the equations and info from the region
            Element interior = (Element) region.getElementsByTagNameNS(mmsUri, "interiorModel").item(0);
            regionProblem = createRegionProblem(problem, interior, regionName);
            //Infer operators information
            OperatorsInfo opInfoMesh = new OperatorsInfo(problem, regionProblem, policy, regionPolicy, policy.getCommonMeshFields(regionInfo.getFields()), DiscretizationType.mesh);
            HashMap<String, OperatorsInfo> opInfoParticles = new HashMap<String, OperatorsInfo>();
            for (Entry<String, Species> entry : policy.getCommonParticleSpecies(regionInfo.getFields()).entrySet()) {
            	String name = entry.getKey();
            	opInfoParticles.put(name, new OperatorsInfo(problem, regionProblem, policy, regionPolicy, entry.getValue().getParticleFields(), DiscretizationType.particles));
            }
            ProcessInfo pi = createProcessInfo(problem, result, schema, regionInfo, ci, regionName + "I", policy, 
                    incompFields, extrapolation, hasFluxes, opInfoMesh, opInfoParticles, sc, referenceFrames, constants);
            int maxDerLevel = Math.max(1, opInfoMesh.getDerivativeLevels().size());
            for (OperatorsInfo opInfo : opInfoParticles.values()) {
            	maxDerLevel = Math.max(maxDerLevel, opInfo.getDerivativeLevels().size());
            }
            int maxAuxDerLevel = opInfoMesh.getAuxiliaryDerivativeLevels().size();
            for (OperatorsInfo opInfo : opInfoParticles.values()) {
            	maxAuxDerLevel = Math.max(maxAuxDerLevel, opInfo.getAuxiliaryDerivativeLevels().size());
            }
            derivativeLevels.put(regionName + "I" + groupSuffix, maxDerLevel);
            auxDerivativeLevels.put(regionName + "I" + groupSuffix, maxAuxDerLevel);
            //Execution flow
            NodeList executionFlows = region.getElementsByTagNameNS(mmsUri, "interiorExecutionFlow");
            Element executionFlow = AGDMUtils.createElement(result, mmsUri, "interiorExecutionFlow");
            if (executionFlows.getLength() > 0) {
            	region.insertBefore(executionFlow, executionFlows.item(0));
            }
            else {
            	Element nodeReference = (Element) region.getLastChild();
            	while (nodeReference.getPreviousSibling().getLocalName().equals("surfaceExecutionFlow")) {
            		nodeReference = (Element) nodeReference.getPreviousSibling();
            	}
            	region.insertBefore(executionFlow, nodeReference);
            }
            
            executionFlow.appendChild(result.importNode(
            		sc.createPDESchema(result, schema, regionProblem, pi, regionName), true));
            //Temporal attribute to identify executionFlown when a groupSuffix is applied
            executionFlow.setAttribute("groupSuffix", groupSuffix);
            //Flux functions
            DocumentFragment fluxFunction = Fluxes.fluxGeneration(result, problem, region, ci, regionName + "I", pi);
            functions.appendChild(fluxFunction);
            //Post-initial conditions (Boundaries and extrapolations)
            interiorPI.appendChild(sc.createPostInitialCondition(result, regionProblem, schema, pi, opInfoMesh, opInfoParticles));
            //Auxiliary equations algorithm
            Element simml = AGDMUtils.createElement(result, smlUri, "simml");
            interiorAlgorithm.appendChild(simml);
            simml.appendChild(result.importNode(sc.createPostRefinementAlgorithm(schema, result, regionProblem, opInfoMesh, opInfoParticles, pi), true));
        }
        //Process surface
        if (region.getElementsByTagNameNS(mmsUri, "surfaceModel").getLength() > 0) {
            //Mount the base problem with the equations of the region
            Element surface = (Element) region.getElementsByTagNameNS(mmsUri, "surfaceModel").item(0);
            regionProblem = createRegionProblem(problem, surface, regionName);
            //Infer operators information
            OperatorsInfo opInfoMesh = new OperatorsInfo(problem, regionProblem, policy, regionPolicy, policy.getCommonMeshFields(regionInfo.getFields()), DiscretizationType.mesh);
            HashMap<String, OperatorsInfo> opInfoParticles = new HashMap<String, OperatorsInfo>();
            for (Entry<String, Species> entry : policy.getCommonParticleSpecies(regionInfo.getFields()).entrySet()) {
            	String name = entry.getKey();
            	opInfoParticles.put(name, new OperatorsInfo(problem, regionProblem, policy, regionPolicy, entry.getValue().getParticleFields(), DiscretizationType.particles));
            }
            ProcessInfo pi = createProcessInfo(problem, result, schema, regionInfo, ci, regionName + "S", policy, 
                    incompFields, extrapolation, hasFluxes, opInfoMesh, opInfoParticles, sc, referenceFrames, constants);
            int maxDerLevel = Math.max(1, opInfoMesh.getDerivativeLevels().size());
            for (OperatorsInfo opInfo : opInfoParticles.values()) {
            	maxDerLevel = Math.max(maxDerLevel, opInfo.getDerivativeLevels().size());
            }
            int maxAuxDerLevel = opInfoMesh.getAuxiliaryDerivativeLevels().size();
            for (OperatorsInfo opInfo : opInfoParticles.values()) {
            	maxAuxDerLevel = Math.max(maxAuxDerLevel, opInfo.getAuxiliaryDerivativeLevels().size());
            }
            derivativeLevels.put(regionName + "S" + groupSuffix, maxDerLevel);
            auxDerivativeLevels.put(regionName + "S" + groupSuffix, maxAuxDerLevel);

            //Execution flow
            Element executionFlow = AGDMUtils.createElement(result, mmsUri, "surfaceExecutionFlow");
            region.insertBefore(region.getLastChild(), executionFlow);
            executionFlow.appendChild(result.importNode(
            		sc.createPDESchema(result, schema, regionProblem, pi, regionName), true));
            //Temporal attribute to identify executionFlown when a groupSuffix is applied
            executionFlow.setAttribute("groupSuffix", groupSuffix);
            //Flux functions
            DocumentFragment fluxFunction = Fluxes.fluxGeneration(result, problem, region, ci, regionName + "S", pi);
            functions.appendChild(fluxFunction);
            //Post-initial conditions (Boundaries and extrapolations)
            surfacePI.appendChild(sc.createPostInitialCondition(result, regionProblem, schema, pi, opInfoMesh, opInfoParticles));
            //Auxiliary equations algorithm
            Element simml = AGDMUtils.createElement(result, smlUri, "simml");
            surfaceAlgorithm.appendChild(simml);
            simml.appendChild(result.importNode(sc.createPostRefinementAlgorithm(schema, result, regionProblem, opInfoMesh, opInfoParticles, pi), true));
        }
        //Insert Post Initial conditions
        Element postInitialConditions = AGDMUtils.createElement(result, mmsUri, "postInitialConditions");
        //Insert Post-initialization boundaries
        if (interiorPI.hasChildNodes()) {
            Element postInitialCondition = AGDMUtils.createElement(result, mmsUri, "postInitialCondition");
            Element regionType = AGDMUtils.createElement(result, mmsUri, "regionType");
            regionType.setTextContent("interior");
            postInitialCondition.appendChild(regionType);
            postInitialCondition.appendChild(interiorPI);
            postInitialConditions.appendChild(postInitialCondition);
        }
        if (surfacePI.hasChildNodes()) {
            Element postInitialCondition = AGDMUtils.createElement(result, mmsUri, "postInitialCondition");
            Element regionType = AGDMUtils.createElement(result, mmsUri, "regionType");
            regionType.setTextContent("surface");
            postInitialCondition.appendChild(regionType);
            postInitialCondition.appendChild(surfacePI);
            postInitialConditions.appendChild(postInitialCondition);
        }
        //Process post-initialization schema rules
        ArrayList<Document> docs = new ArrayList<Document>();
        docs.add(result);
        docs.add(schema);
        docs.add(problem);
        if (region.getElementsByTagNameNS(mmsUri, "interiorModel").getLength() > 0) {
            //Mount the base problem with the equations of the region
            Element interior = (Element) region.getElementsByTagNameNS(mmsUri, "interiorModel").item(0);
            regionProblem = createRegionProblem(problem, interior, regionName);
        } 
        if (region.getElementsByTagNameNS(mmsUri, "surfaceModel").getLength() > 0) {
            Element surface = (Element) region.getElementsByTagNameNS(mmsUri, "surfaceModel").item(0);
            regionProblem = createRegionProblem(problem, surface, regionName); 
        }
        //Policy post initialization
        Element resultInit = AGDMUtils.createElement(result, smlUri, "simml");
        if (regionProblem != null && policy.getPostInitialization() != null) {
        	Element iterOverCells = AGDMUtils.createElement(result, smlUri, "iterateOverCells");
        	iterOverCells.setAttribute("stencilAtt", String.valueOf(0));
            ProcessInfo pi = createProcessInfoGeneric(problem, result, regionInfo, ci, policy, referenceFrames, constants);
            Node postInitSchemaInst = processPostInit(result, regionProblem, policy.getPostInitialization(), pi);
            resultInit.appendChild(iterOverCells);
            iterOverCells.appendChild(result.importNode(postInitSchemaInst, true));
        }
        if (resultInit.hasChildNodes()) {
            Element postInitialCondition = AGDMUtils.createElement(result, mmsUri, "initialCondition");
            postInitialCondition.appendChild(resultInit);
            Element initialConditions = (Element) ((Element) region).getElementsByTagName("mms:initialConditions").item(0);
            initialConditions.appendChild(postInitialCondition);
        }

        if (postInitialConditions.hasChildNodes()) {
            Element initialConditions = (Element) ((Element) region).getElementsByTagName("mms:initialConditions").item(0);
            initialConditions.getParentNode().insertBefore(postInitialConditions, initialConditions.getNextSibling());
        }
        //Add auxiliary equations algorithm
        Element postRefinementAlgorithm = AGDMUtils.createElement(result, mmsUri, "postRefinementAlgorithm");
        if (interiorAlgorithm.hasChildNodes()) {
        	if (interiorAlgorithm.getFirstChild().hasChildNodes()) {
        		postRefinementAlgorithm.appendChild(interiorAlgorithm);
        	}
        }
        if (surfaceAlgorithm.hasChildNodes()) {
        	if (surfaceAlgorithm.getFirstChild().hasChildNodes()) {
        		postRefinementAlgorithm.appendChild(surfaceAlgorithm);
        	}
        }
        if (postRefinementAlgorithm.hasChildNodes()) {
            Element entryPoint = (Element) AGDMUtils.find(region, ".//mms:interactions|.//mms:interiorExecutionFlow|.//mms:surfaceExecutionFlow").item(0);
            entryPoint.getParentNode().insertBefore(postRefinementAlgorithm, entryPoint);
        }
        ProcessInfo piDisc = createProcessInfo(problem, result, schema, regionInfo, ci, regionName, policy, 
                incompFields, extrapolation, hasFluxes, null, null, sc, referenceFrames, constants);
        
        createPDEDiscretizationInfo(discretizationInfo, region, problem, result, regionPolicy, piDisc);
    }
    
    /**
     * Creates the initial process info structure for PDE.
     * @param completeProblem   The whole problem
     * @param result            The problem
     * @param ri                The region information
     * @param ci                The coordinate information
     * @param schema            The discretization schema
     * @param regionName        The region name
     * @param policy            The discretization policy
     * @param incompFields      The incompatible fields
     * @param extrapolation     If there is extrapolation
     * @param hasFluxes			If the model has fluxes
     * @param opInfoMesh		The operators information
     * @param opInfoParticles	The operators information
     * @param sc				Schema creator
     * @param referenceFrames	Reference frame for fields
     * @return                  The process info
     * @throws AGDMException    AGDM007  External error
     */
    private ProcessInfo createProcessInfo(Document completeProblem, Document result, Document schema, RegionInfo ri, 
    		CoordinateInfo ci, String regionName, DiscretizationPolicy policy, 
            IncompatibilityFields incompFields, boolean extrapolation, boolean hasFluxes, OperatorsInfo opInfoMesh, HashMap<String, OperatorsInfo> opInfoParticles, SchemaCreator sc,
            HashMap<String, ReferenceFrameType> referenceFrames, ConstantInformation constants) throws AGDMException {
        ProcessInfo pi = new ProcessInfo();
        pi.setCoordinateRelation(ci);
        pi.setRegionInfo(ri);
        pi.setRegionName(regionName);
        pi.setBaseSpatialCoordinates(ci.getContCoords());
        pi.setTensor(AGDMUtils.find(completeProblem, "//mms:tensor"));
        pi.setIncomFields(incompFields);
        pi.setExtrapolationMethod(policy.getExtrapolationMethod());
        pi.setStepFieldTemplates(policy.getStepFieldTemplates());
        pi.setCompleteProblem(completeProblem);
        pi.setExtrapolation(extrapolation);
        pi.setHasFluxes(hasFluxes);
        pi.calculateStepVariables(schema);
        pi.calculateStiffVariables(schema);
        pi.setOperatorsInfoMesh(opInfoMesh);
        pi.setOperatorsInfoParticles(opInfoParticles);
        pi.setParticleSpecies(policy.getCommonParticleSpecies(ri.getFields()));
        pi.setMeshFields(policy.getCommonMeshFields(ri.getFields()));
        pi.setConstantInformation(constants);
        Set<String> auxiliaryVariables = new LinkedHashSet<String>();
        NodeList auxVars = AGDMUtils.find(completeProblem, "//mms:auxiliaryVariables/mms:auxiliaryVariable|//mms:auxiliaryAnalysisVariables/mms:auxiliaryAnalysisVariable");
        for (int i = 0; i < auxVars.getLength(); i++) {
        	auxiliaryVariables.add(auxVars.item(i).getTextContent());
        }
        pi.setAuxiliaryVariables(auxiliaryVariables);
        pi.setSchemaCreator(sc);
        int compactSupportRatio = 1;
        NodeList csr = schema.getElementsByTagName("mms:compactSupportRatio");
        if (csr.getLength() > 0) {
        	compactSupportRatio = Integer.parseInt(csr.item(0).getTextContent());
        }
        pi.setCompactSupportRatio(compactSupportRatio);
        pi.setReferenceFrame(referenceFrames);
        if (policy.getPostInitialization() != null) {
        	pi.setPostInitialization(policy.getPostInitialization().cloneNode(true));
        }
        
        return pi;
    }
    
    
    /**
     * Creates the initial process info structure for PDE.
     * @param completeProblem   The whole problem
     * @param result            The problem
     * @param ri                The region information
     * @param ci                The coordinate information
     * @param policy            The discretization policy
     * @param referenceFrames	Reference frame for fields
     * @return                  The process info
     * @throws AGDMException    AGDM007  External error
     */
    private ProcessInfo createProcessInfoGeneric(Document completeProblem, Document result, RegionInfo ri, 
    		CoordinateInfo ci, DiscretizationPolicy policy, HashMap<String, ReferenceFrameType> referenceFrames, ConstantInformation constants) throws AGDMException {
        ProcessInfo pi = new ProcessInfo();
        pi.setCoordinateRelation(ci);
        pi.setRegionInfo(ri);
        pi.setBaseSpatialCoordinates(ci.getContCoords());
        pi.setTensor(AGDMUtils.find(completeProblem, "//mms:tensor"));
        pi.setExtrapolationMethod(policy.getExtrapolationMethod());
        pi.setCompleteProblem(completeProblem);
        pi.setParticleSpecies(policy.getCommonParticleSpecies(ri.getFields()));
        pi.setConstantInformation(constants);
        pi.setMeshFields(policy.getCommonMeshFields(ri.getFields()));
        Set<String> auxiliaryVariables = new LinkedHashSet<String>();
        NodeList auxVars = AGDMUtils.find(completeProblem, "//mms:auxiliaryVariables/mms:auxiliaryVariable|//mms:auxiliaryAnalysisVariables/mms:auxiliaryAnalysisVariable");
        for (int i = 0; i < auxVars.getLength(); i++) {
        	auxiliaryVariables.add(auxVars.item(i).getTextContent());
        }
        pi.setAuxiliaryVariables(auxiliaryVariables);
        if (policy.getPostInitialization() != null) {
        	pi.setPostInitialization(policy.getPostInitialization().cloneNode(true));
        }
        pi.setReferenceFrame(referenceFrames);
        
        return pi;
    }
    
    
    /**
     * Creates the initial process info structure for PDE.
     * @param completeProblem   The whole problem
     * @param result            The problem
     * @param ci                The coordinate information
     * @param policy            The discretization policy
     * @param referenceFrames	Reference frame for fields
     * @return                  The process info
     * @throws AGDMException    AGDM007  External error
     */
    private ProcessInfo createVerySimpleProcessInfo(Document completeProblem, Document result, 
    		CoordinateInfo ci, DiscretizationPolicy policy, HashMap<String, ReferenceFrameType> referenceFrames, ConstantInformation constants) throws AGDMException {
        ProcessInfo pi = new ProcessInfo();
        pi.setCoordinateRelation(ci);
        pi.setBaseSpatialCoordinates(ci.getContCoords());
        pi.setTensor(AGDMUtils.find(completeProblem, "//mms:tensor"));
        pi.setExtrapolationMethod(policy.getExtrapolationMethod());
        pi.setCompleteProblem(completeProblem);
        pi.setParticleSpecies(policy.getParticleSpecies());
        pi.setConstantInformation(constants);
        pi.setMeshFields(policy.getMeshFields());
        Set<String> auxiliaryVariables = new LinkedHashSet<String>();
        NodeList auxVars = AGDMUtils.find(completeProblem, "//mms:auxiliaryVariable|//mms:auxiliaryAnalysisVariable");
        for (int i = 0; i < auxVars.getLength(); i++) {
        	auxiliaryVariables.add(auxVars.item(i).getTextContent());
        }
        pi.setAuxiliaryVariables(auxiliaryVariables);
        if (policy.getPostInitialization() != null) {
        	pi.setPostInitialization(policy.getPostInitialization().cloneNode(true));
        }
        pi.setReferenceFrame(referenceFrames);
        
        return pi;
    }
    
    /**
     * Creates the initial process info structure for PDE.
     * @param completeProblem   The whole problem
     * @param result            The problem
     * @param ci                The coordinate information
     * @param opInfoMesh		Operators info
     * @param policy            The discretization policy
     * @param sc				Schema creator
     * @param referenceFrames	Reference frame for fields 
     * @return                  The process info
     * @throws AGDMException    AGDM007  External error
     */
    private ProcessInfo createAnalysisProcessInfo(Document completeProblem, Document result,
    		CoordinateInfo ci, DiscretizationPolicy policy, OperatorsInfo opInfoMesh, SchemaCreator sc, HashMap<String, ReferenceFrameType> referenceFrames, ConstantInformation constants) throws AGDMException {
        ProcessInfo pi = new ProcessInfo();
        pi.setCoordinateRelation(ci);
        pi.setBaseSpatialCoordinates(ci.getContCoords());
        pi.setExtrapolationMethod(policy.getExtrapolationMethod());
        pi.setCompleteProblem((Document) completeProblem.cloneNode(true));
        pi.setExtrapolation(false);
        pi.setConstantInformation(constants);
        pi.addStepVariable((Element) AGDMUtils.createTimeSlice(completeProblem).getFirstChild());
        //Creation of the region information with the coordinates and fields for analysis
        ArrayList<String> fields = new ArrayList<String>();
        HashMap<String, Boolean> isAuxiliary = new HashMap<String, Boolean>();
        NodeList fieldList = AGDMUtils.find(completeProblem, "/*/mms:fields/mms:field|/*/mms:auxiliaryFields/mms:auxiliaryField"
                + "|/*/mms:analysisFields/mms:analysisField");
        for (int j = 0; j < fieldList.getLength(); j++) {
            fields.add(fieldList.item(j).getTextContent());
            isAuxiliary.put(fieldList.item(j).getTextContent(), fieldList.item(j).getLocalName().equals("auxiliaryField"));
        }

        RegionInfo ri = new RegionInfo(fields, isAuxiliary);
        pi.setRegionInfo(ri);
        Set<String> auxiliaryVariables = new LinkedHashSet<String>();
        NodeList auxVars = AGDMUtils.find(completeProblem, "//mms:auxiliaryVariables/mms:auxiliaryVariable|//mms:auxiliaryAnalysisVariables/mms:auxiliaryAnalysisVariable");
        for (int i = 0; i < auxVars.getLength(); i++) {
        	auxiliaryVariables.add(auxVars.item(i).getTextContent());
        }
        pi.setAuxiliaryVariables(auxiliaryVariables);
        pi.setSchemaCreator(sc);
        pi.setDiscretizationType(DiscretizationType.mesh);
        pi.setReferenceFrame(referenceFrames);
        pi.setMeshFields(policy.getCommonMeshFields(ri.getFields()));
        pi.setOperatorsInfoMesh(opInfoMesh);
        return pi;
    }
    
    /**
     * Process post initial instructions from the schema.
     * @param discProblem      	The discretized problem
     * @param problem           The document with the info
     * @param instructions		Instructions to process
     * @param pi				ProcessInfo
     * @return                  The document fragment resulting
     * @throws AGDMException    AGDM002 XML discretization schema not valid
     *                          AGDM006 External error
     */
    private DocumentFragment processPostInit(Document discProblem, Node problem, 
            Node instructions, ProcessInfo pi) throws AGDMException {
        
        //The result are stored in a document fragment
        DocumentFragment result = problem.getOwnerDocument().createDocumentFragment();
        //Process every child
        NodeList flowChildren = instructions.getChildNodes();
        for (int i = 0; i < flowChildren.getLength(); i++) {
            Element rule = (Element) flowChildren.item(i);
            pi.setDiscretizationType(DiscretizationType.mesh);
            RuleInfo ri = new RuleInfo();
            ri.setIndividualField(false);
            ri.setIndividualCoord(false);
            ri.setFunction(false);
            ri.setProcessInfo(pi);
            ri.setAuxiliaryCDAdded(false);
            ri.setAuxiliaryFluxesAdded(false);
            result.appendChild(problem.getOwnerDocument().importNode(ExecutionFlow.processExecutionFlowChild(discProblem, problem, rule, ri), true));
        }
        return result;
    }
    
    /**
     * Gets the incompatible fields for all the regions.
     * @param result            The result problem
     * @param problem           The problem with the information
     * @param ci                The coordinate information
     * @param meshFields		Only mesh fields can have incompatibilities 
     * @return                  The incompatibilities
     * @throws AGDMException    AGDM007 - External error
     */
    private HashMap<String, IncompatibilityFields> getIncompatibleFields(Document result, Document problem, 
    		CoordinateInfo ci, ArrayList<String> meshFields) throws AGDMException {
        NodeList regions = AGDMUtils.find(result, "//mms:region|//mms:subregion");
        HashMap<String, IncompatibilityFields> incompatibilities = new HashMap<String, IncompatibilityFields>();
        //Obtain the region information relevant to infer the incompatibilities
        HashMap<String, ArrayList<String>> regionFields = new HashMap<String, ArrayList<String>>();
        for (int i = 0; i < regions.getLength(); i++) {
            String regionName = regions.item(i).getFirstChild().getTextContent();
            ArrayList<String> fields = new ArrayList<String>();
            NodeList models = AGDMUtils.find(result, "//*[mms:name = '" + regionName + "']//mms:interiorModel|//*[mms:name = '" 
                    + regionName + "']//mms:surfaceModel");
            for (int j = 0; j < models.getLength(); j++) {
                NodeList fieldList = AGDMUtils.find(problem, "//mms:models/mms:model[mms:name = '" 
                        + models.item(j).getTextContent() + "']//mms:field[ancestor::mms:evolutionEquation]|//" 
                        + "mms:models/mms:model[mms:name = '" + models.item(j).getTextContent() + "']//" 
                        + "mms:auxiliaryField[ancestor::mms:auxiliaryEquation or ancestor::mms:auxiliaryFieldEquation]");
                for (int k = 0; k < fieldList.getLength(); k++) {
                    if (meshFields.contains(fieldList.item(k).getTextContent()) && !fields.contains(fieldList.item(k).getTextContent())) {
                        fields.add(fieldList.item(k).getTextContent());
                    }
                }
            }
            if (models.getLength() > 0) {
	            regionFields.put(regionName, fields);
            }
        }
        for (int i = 0; i < regions.getLength(); i++) {
            String regionAName = regions.item(i).getFirstChild().getTextContent();
            if (regionFields.containsKey(regionAName)) {
	            ArrayList<String> fieldsA = regionFields.get(regionAName);
	            LinkedHashMap<String, ArrayList<String>> incompatibleFields = new LinkedHashMap<String, ArrayList<String>>();
	            LinkedHashMap<String, ArrayList<String>> incompatibleBounds = new LinkedHashMap<String, ArrayList<String>>();
	            //Incompatibilities from other regions
	            for (int j = 0; j < regions.getLength(); j++) {
	                if (j != i) {
	                    String regionBName = regions.item(j).getFirstChild().getTextContent();
	                    if (regionFields.containsKey(regionBName)) {
		                    ArrayList<String> fieldsB = regionFields.get(regionBName);
		                    ArrayList<String> incompatible = new ArrayList<String>();
		                    for (int k = 0; k < fieldsB.size(); k++) {
		                        if (!fieldsA.contains(fieldsB.get(k))) {
		                            incompatible.add(fieldsB.get(k));
		                        }
		                        if (!incompatible.isEmpty()) {
		                            for (int l = 0; l < incompatible.size(); l++) {
		                                ArrayList<String> fields;
		                                if (incompatibleFields.containsKey(regionAName)) {
		                                    fields = incompatibleFields.get(regionAName);
		                                }
		                                else {
		                                    fields = new ArrayList<String>();
		                                }
		                                ArrayList<String> destSeg;
		                                if (fields.contains(incompatible.get(l))) {
		                                    destSeg = incompatibleFields.get(incompatible.get(l));
		                                }
		                                else {
		                                    destSeg = new ArrayList<String>();
		                                }
		                                destSeg.add(regionBName);
		                                incompatibleFields.put(incompatible.get(l), destSeg);
		                            }
		                        }
		                    }
	                    }
	                }
	            }
	            //Incompatibilities for the boundaries
	            NodeList coords = AGDMUtils.find(result, "//mms:spatialCoordinates/mms:spatialCoordinate");
	            for (int j = 0; j < coords.getLength(); j++) {
	                String coord = ci.getContCoords().get(j);
	                NodeList boundaryFields = AGDMUtils.find(result, "//mms:boundaryPolicy[descendant::" 
	                    + "mms:regionName = '" + regionAName + "']//mms:boundaryCondition[not(mms:type/mms:extrapolation) and (mms:axis ='" 
	                    + coord + "' " + "or lower-case(mms:axis) ='all') and (mms:side ='Lower' " 
	                    + "or lower-case(mms:side) ='all')]//mms:field");
	                ArrayList<String> fieldsBound = new ArrayList<String>();
	                for (int k = 0; k < boundaryFields.getLength(); k++) {
	                    if (meshFields.contains(boundaryFields.item(k).getTextContent()) && !fieldsBound.contains(boundaryFields.item(k).getTextContent())) {
	                        fieldsBound.add(boundaryFields.item(k).getTextContent());
	                    }
	                }
	                ArrayList<String> incompatibleBound = new ArrayList<String>();
	                for (int k = 0; k < fieldsA.size(); k++) {
	                    if (!fieldsBound.contains(fieldsA.get(k)) && !incompatibleBound.contains(fieldsA.get(k))) {
	                        incompatibleBound.add(fieldsA.get(k));
	                    }
	                }
	                incompatibleBounds.put(coords.item(j).getTextContent() + "-Lower", incompatibleBound);
	                boundaryFields = AGDMUtils.find(result, "//mms:boundaryPolicy[descendant::mms:regionName = '" 
	                    + regionAName + "']//mms:boundaryCondition[not(mms:type/mms:extrapolation) and (mms:axis ='" + coord + "' " 
	                    + "or lower-case(mms:axis) ='all') and (mms:side ='Upper' " 
	                    + "or lower-case(mms:side) ='all')]//mms:field");
	                fieldsBound = new ArrayList<String>();
	                for (int k = 0; k < boundaryFields.getLength(); k++) {
	                    if (meshFields.contains(boundaryFields.item(k).getTextContent()) && !fieldsBound.contains(boundaryFields.item(k).getTextContent())) {
	                        fieldsBound.add(boundaryFields.item(k).getTextContent());
	                    }
	                }
	                incompatibleBound = new ArrayList<String>();
	                for (int k = 0; k < fieldsA.size(); k++) {
	                    if (!fieldsBound.contains(fieldsA.get(k)) && !incompatibleBound.contains(fieldsA.get(k))) {
	                        incompatibleBound.add(fieldsA.get(k));
	                    }
	                }
	                incompatibleBounds.put(coords.item(j).getTextContent() + "-Upper", incompatibleBound);
	            }
	            incompatibilities.put(regionAName, new IncompatibilityFields(incompatibleFields, incompatibleBounds));
            }
        }
        return incompatibilities;
    } 
    
    /**
     * Generates the region relations depending on the fields and schema used for a region.
     * @param regions     		The regions
     * @param problem           The problem with the information
     * @param policy            The discretization policy
     * @return                  The xml element with the relation
     * @throws AGDMException    AGDM00X - External error
     */
    private Element generateRegionRelation(NodeList regions, Document problem, DiscretizationPolicy policy) throws AGDMException {
        Document result = regions.item(0).getOwnerDocument();
        ArrayList<String> auxiliarFields = new ArrayList<String>();
        NodeList auxFields = result.getElementsByTagName("mms:auxiliaryField");
        for (int i = 0; i < auxFields.getLength(); i++) {
            auxiliarFields.add(auxFields.item(i).getTextContent());
        }
        Element regionRelations = AGDMUtils.createElement(result, mmsUri, "regionRelations");
        //Obtain the region information relevant to infer the relations
        ArrayList<String> regionSchemas = new ArrayList<String>();
        HashMap<String, ArrayList<String>> regionFields = new HashMap<String, ArrayList<String>>();
        for (int i = 0; i < regions.getLength(); i++) {
            String regionName = regions.item(i).getFirstChild().getTextContent();
            regionSchemas.add(regionName);
            ArrayList<String> fields = new ArrayList<String>();
            NodeList models = AGDMUtils.find(result, "//mms:*[mms:name = '" + regionName + "']//" 
                    + "mms:interiorModel|//mms:*[mms:name = '" + regionName + "']//" 
                    + "mms:surfaceModel");
            for (int j = 0; j < models.getLength(); j++) {
                NodeList fieldList = AGDMUtils.find(problem, "//mms:model[mms:name = '" 
                        + models.item(j).getTextContent() + "']//mms:field[ancestor::mms:evolutionEquation]|//" 
                        + "mms:model[mms:name = '" + models.item(j).getTextContent() + "']//" 
                        + "mms:auxiliaryField[ancestor::mms:auxiliaryEquation or ancestor::mms:auxiliaryFieldEquation]");
                for (int k = 0; k < fieldList.getLength(); k++) {
                    if (!fields.contains(fieldList.item(k).getTextContent())) {
                        fields.add(fieldList.item(k).getTextContent());
                    }
                }
            }
            NodeList boundaryFields = AGDMUtils.find(result, "//mms:*[mms:name = '" 
                    + regionName + "']//mms:boundaryCondition//mms:field");
            for (int k = 0; k < boundaryFields.getLength(); k++) {
                if (!fields.contains(boundaryFields.item(k).getTextContent())) {
                    fields.add(boundaryFields.item(k).getTextContent());
                }
            }
            regionFields.put(regionName, fields);
        }
        for (int i = 0; i < regions.getLength() - 1; i++) {
            String regionAName = regions.item(i).getFirstChild().getTextContent();
            Element regionA = AGDMUtils.createElement(result, mmsUri, "regionA");
            regionA.setTextContent(regionAName);
            ArrayList<String> fieldsA = regionFields.remove(regionAName);
            Collections.sort(fieldsA);
            for (int j = i + 1; j < regions.getLength(); j++) {
                Element regionRelation = AGDMUtils.createElement(result, mmsUri, "regionRelation");
                String regionBName = regions.item(j).getFirstChild().getTextContent();
                Element regionB = AGDMUtils.createElement(result, mmsUri, "regionB");
                regionB.setTextContent(regionBName);
                Element relation = AGDMUtils.createElement(result, mmsUri, "relation");
                ArrayList<String> fieldsB = regionFields.get(regionBName);
                Collections.sort(fieldsB);
                if (!fieldsA.equals(fieldsB)) {
                    relation.setTextContent("HARD");
                }
                else {
                    relation.setTextContent("COMPATIBLE");
                }
                regionRelation.appendChild(regionA.cloneNode(true));
                regionRelation.appendChild(regionB);
                regionRelation.appendChild(relation);
                regionRelations.appendChild(regionRelation).cloneNode(true);
            }
        }
        
        return regionRelations;
    }
    
    /**
     * Generates the discretization information for every region.
     * @param region			Region being discretized
     * @param problem			Original problem
     * @param result            The result document
     * @param policy            The discretization policy of the region
     * @param pi				Problem information
     * @return                  The discretization information element
     * @throws AGDMException    AGDM007 - External error
     */
    private void createPDEDiscretizationInfo(Element discretizationInfo, Element region, Document problem, Document result, PDERegionDiscPolicy policy, ProcessInfo pi) throws AGDMException {
       
        Element pdeDiscretization = AGDMUtils.createElement(result, mmsUri, "PDEDiscretization");
        
        try {
            DocumentManager docMan = new DocumentManagerImpl();
            String schema = docMan.getDocument(policy.getTimeSchema());
            Document timeSchema = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(schema));
            //Get maximum stencil from pre/post-step
            int prepostStencil = 0;
            NodeList idList = AGDMUtils.find(timeSchema, ".//mms:meshPreStep/mms:transformationRule/mms:id[not(text() = preceding::mms:id/text())]|.//mms:particlePreStep/mms:transformationRule/mms:id[not(text() = preceding::mms:id/text())]");
            for (int i = 0; i < idList.getLength(); i++) {
	            String schemaString = docMan.getDocument(idList.item(i).getTextContent());
	            Document prestepCode = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(schemaString));
	            NodeList iterations = AGDMUtils.find(prestepCode, "//sml:simml//sml:iterateOverCells");
	            for (int j = 0; j < iterations.getLength(); j++) {
	            	int stencil = Integer.valueOf(((Element) iterations.item(i)).getAttribute("stencilAtt"));
	            	prepostStencil = Math.max(prepostStencil, stencil);
	            }
            }
            //Spatial discretization
            Element spatialDiscs = AGDMUtils.createElement(result, mmsUri, "spatialOperatorDiscretizations");
            //Arbitrary derivative operators
            HashMap<String, OperatorPolicy> operatorPolicies = policy.getOperatorPolicies();
            ArrayList<OperatorDiscretization> operators = policy.getOperatorDiscretizations();
            for (int i = 0; i < operators.size(); i++) {
                Element spatialDisc = AGDMUtils.createElement(result, mmsUri, "spatialOperatorDiscretization");
                String spatialSchemaID = docMan.getDocument(operators.get(i).getSchema());
                Document spatialSchema = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(spatialSchemaID));
                Element name = AGDMUtils.createElement(result, mmsUri, "name");
                name.setTextContent(spatialSchema.getElementsByTagNameNS(mmsUri, "name").item(0).getTextContent());
                Element id = AGDMUtils.createElement(result, mmsUri, "id");
                id.setTextContent(spatialSchema.getElementsByTagNameNS(mmsUri, "id").item(0).getTextContent());
                Element stencil = AGDMUtils.createElement(result, mmsUri, "stencil");
                
                //Calculate max stencil from all the individual spatial discretizations in the operator policy
                int maxStencil = 0;
                OperatorPolicy op = operatorPolicies.get(spatialSchema.getElementsByTagNameNS(mmsUri, "id").item(0).getTextContent());
                ArrayList<SpatialOperatorDiscretization> sod = op.getDiscretization();
                for (int j = 0; j < sod.size(); j++) {
                    Document singleSchema = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(docMan.getDocument(sod.get(j).getSchemaId())));
                    int localStencil = SchemaCreator.getSchemaStencil(singleSchema);
                    maxStencil = Math.max(maxStencil, localStencil);
                }
                //Check if max stencil is overloaded by the pre/post-step
                maxStencil = Math.max(prepostStencil, maxStencil);
                
                stencil.setTextContent(String.valueOf(maxStencil));
                spatialDisc.appendChild(name);
                spatialDisc.appendChild(id);
                spatialDisc.appendChild(stencil);
                
                spatialDiscs.appendChild(spatialDisc);
            }
            //Flux operator
            NodeList fluxOperator = AGDMUtils.find(timeSchema, ".//mms:conservativeTermDiscretization//mms:meshDiscretization/mms:transformationRule/mms:id[not(text() = preceding::mms:id/text())]|.//mms:conservativeTermDiscretization//mms:particleDiscretization/mms:transformationRule/mms:id[not(text() = preceding::mms:id/text())]");
            for (int i = 0; i < fluxOperator.getLength(); i++) {
	            Element spatialDisc = AGDMUtils.createElement(result, mmsUri, "spatialOperatorDiscretization");
	            String spatialSchemaString = docMan.getDocument(fluxOperator.item(i).getTextContent());
	            Document spatialSchema = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(spatialSchemaString));
	            Element name = AGDMUtils.createElement(result, mmsUri, "name");
	            name.setTextContent(spatialSchema.getElementsByTagNameNS(mmsUri, "name").item(0).getTextContent());
	            Element id = AGDMUtils.createElement(result, mmsUri, "id");
	            id.setTextContent(fluxOperator.item(i).getLastChild().getTextContent());
	            Element stencil = AGDMUtils.createElement(result, mmsUri, "stencil");
	            stencil.setTextContent(String.valueOf(SchemaCreator.getSchemaStencil(spatialSchema)));
	            spatialDisc.appendChild(name);
                spatialDisc.appendChild(id);
                spatialDisc.appendChild(stencil);
	            spatialDiscs.appendChild(spatialDisc);
            }
            pdeDiscretization.appendChild(spatialDiscs);
                        
            //Dissipation discretization            
            NodeList dissipations = timeSchema.getElementsByTagNameNS(mmsUri, "dissipation");
            if (dissipations.getLength() > 0) {
                Element dissipationDiscretizations = AGDMUtils.createElement(result, mmsUri, "dissipationDiscretizations");
                for (int i = 0; i < dissipations.getLength(); i++) {
                    Element dissipation = (Element) dissipations.item(i);
                    Element dissipationDiscretization = AGDMUtils.createElement(result, mmsUri, "dissipationDiscretization");
                    Element name = AGDMUtils.createElement(result, mmsUri, "name");
                    name.setTextContent(dissipation.getElementsByTagNameNS(mmsUri, "name").item(0).getTextContent());
                    dissipationDiscretization.appendChild(name);
                    Element id = AGDMUtils.createElement(result, mmsUri, "id");
                    id.setTextContent(dissipation.getElementsByTagNameNS(mmsUri, "id").item(0).getTextContent());
                    dissipationDiscretization.appendChild(id);
                    Element stencil = AGDMUtils.createElement(result, mmsUri, "stencil");
                    stencil.setTextContent(String.valueOf(SchemaCreator.getSchemaStencil(AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(
                            docMan.getDocument(id.getTextContent()))))));
                    dissipationDiscretization.appendChild(stencil);
                    dissipationDiscretizations.appendChild(dissipationDiscretization);
                }
                pdeDiscretization.appendChild(dissipationDiscretizations);
            }
            
            //Time discretization
            Element timeDisc = AGDMUtils.createElement(result, mmsUri, "timeDiscretization");
            Element name = AGDMUtils.createElement(result, mmsUri, "name");
            name.setTextContent(timeSchema.getElementsByTagNameNS(mmsUri, "name").item(0).getTextContent());
            Element id = AGDMUtils.createElement(result, mmsUri, "id");
            id.setTextContent(timeSchema.getElementsByTagNameNS(mmsUri, "id").item(0).getTextContent());
            timeDisc.appendChild(name);
            timeDisc.appendChild(id);
            pdeDiscretization.appendChild(timeDisc);
            
            //Maximum step stencil
            Element maxStepStencil = AGDMUtils.createElement(result, mmsUri, "maximumStepStencil");
            maxStepStencil.setTextContent(String.valueOf(SchemaCreator.maxStepStencil));
            pdeDiscretization.appendChild(maxStepStencil);
            
            //Time interpolation
            Element timeInterpolation = AGDMUtils.createElement(result, mmsUri, "timeInterpolation");
            Element steps = AGDMUtils.createElement(result, mmsUri, "steps");
            for (int i = 0; i < policy.getStepsNumber(); i++) {
            	Element stepInformation = AGDMUtils.createElement(result, mmsUri, "stepInformation");
            	Element outputVariable = AGDMUtils.createElement(result, mmsUri, "outputVariable");
            	outputVariable.appendChild(result.importNode(policy.getTimestepOutputVariableTemplates().get(i).cloneNode(true), true));
            	stepInformation.appendChild(outputVariable);
            	Element timestepFactor = AGDMUtils.createElement(result, mmsUri, "timestepFactor");
            	timestepFactor.setTextContent(policy.getTimestepFactors().get(i).toString());
            	stepInformation.appendChild(timestepFactor);
            	steps.appendChild(stepInformation);
            }
            timeInterpolation.appendChild(steps);
            //Check if there is a custom time interpolation
            NodeList customTimeInterpolation = timeSchema.getElementsByTagNameNS(mmsUri, "timeInterpolation");
            if (customTimeInterpolation.getLength() > 0) {
            	Element algorithm = AGDMUtils.createElement(result, mmsUri, "timeInterpolator");
                //Get the schema document to import
                schema = SimflownyUtils.jsonToXML(docMan.getDocument(customTimeInterpolation.item(0).getFirstChild().getLastChild().getTextContent()));
                if (schema == null) {
                    throw new AGDMException(AGDMException.AGDM002, "The schema " + customTimeInterpolation.item(0).getFirstChild().getLastChild().getTextContent() + " does not exist");
                }
                Document importDoc = AGDMUtils.stringToDom(schema);
            	ImportCall ic = new ImportCall(importDoc, "");
            	pi.setTimeInterpolator(true);
            	algorithm.appendChild(result.importNode(ExecutionFlow.processImport(result, problem, ic, pi, null, false), true));
            	//Copy the input parameters
            	algorithm.appendChild(result.importNode(customTimeInterpolation.item(0).getLastChild().cloneNode(true), true));
            	timeInterpolation.appendChild(algorithm);
            }
            pdeDiscretization.appendChild(timeInterpolation);
            
            //Add fields discretized with this schema
        	Element fields = AGDMUtils.createElement(result, mmsUri, "fields");
        	for (String f: policy.getRegionInfo().getFields()) {
        		Element field = AGDMUtils.createElement(result, mmsUri, "field");
        		field.setTextContent(f);
        		fields.appendChild(field);
        	}
        	pdeDiscretization.appendChild(fields);
            
            discretizationInfo.appendChild(pdeDiscretization);
        }
        catch (Exception e) {
        	e.printStackTrace(System.out);
            throw new AGDMException(AGDMException.AGDM002, e.getMessage());
        }
        
    }
    
    /**
     * Generates the discretization information for every region.
     * @param result            The result document
     * @param policy            The discretization policy of the region
     * @return                  The discretization information element
     * @throws AGDMException    AGDM007 - External error
     */
    private Element createPDEAnalysisDiscretizationInfo(Document result, PDEAnalysisDiscPolicy policy) throws AGDMException {
        Element PDEDiscretization = AGDMUtils.createElement(result, mmsUri, "PDEDiscretization");
        try {
            DocumentManager docMan = new DocumentManagerImpl();
           
            //Spatial discretization
            Element spatialDiscs = AGDMUtils.createElement(result, mmsUri, "spatialOperatorDiscretizations");
            HashMap<String, OperatorPolicy> operatorPolicies = policy.getOperatorPolicies();
            ArrayList<OperatorDiscretization> operators = policy.getOperatorDiscretizations();
            for (int i = 0; i < operators.size(); i++) {
                Element spatialDisc = AGDMUtils.createElement(result, mmsUri, "spatialOperatorDiscretization");
                String schema = docMan.getDocument(operators.get(i).getSchema());
                Document spatialSchema = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(schema));
                Element name = AGDMUtils.createElement(result, mmsUri, "name");
                name.setTextContent(spatialSchema.getElementsByTagNameNS(mmsUri, "name").item(0).getTextContent());
                Element id = AGDMUtils.createElement(result, mmsUri, "id");
                id.setTextContent(spatialSchema.getElementsByTagNameNS(mmsUri, "id").item(0).getTextContent());
                Element stencil = AGDMUtils.createElement(result, mmsUri, "stencil");
                
                //Calculate max stencil from all the individual spatial discretizations in the operator policy
                int maxStencil = 0;
                OperatorPolicy op = operatorPolicies.get(spatialSchema.getElementsByTagNameNS(mmsUri, "id").item(0).getTextContent());
                ArrayList<SpatialOperatorDiscretization> sod = op.getDiscretization();
                for (int j = 0; j < sod.size(); j++) {
                    Document singleSchema = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(docMan.getDocument(sod.get(j).getSchemaId())));
                    int localStencil = SchemaCreator.getSchemaStencil(singleSchema);
                    maxStencil = Math.max(maxStencil, localStencil);
                }
                
                stencil.setTextContent(String.valueOf(maxStencil));
                spatialDisc.appendChild(name);
                spatialDisc.appendChild(id);
                spatialDisc.appendChild(stencil);
                
                spatialDiscs.appendChild(spatialDisc);
            }
            PDEDiscretization.appendChild(spatialDiscs);
            
            //Maximum step stencil
            Element maxStepStencil = AGDMUtils.createElement(result, mmsUri, "maximumStepStencil");
            maxStepStencil.setTextContent(String.valueOf(SchemaCreator.maxStepStencil));
            PDEDiscretization.appendChild(maxStepStencil);
            
            return PDEDiscretization;
        }
        catch (Exception e) {
            throw new AGDMException(AGDMException.AGDM002, e.getMessage());
        }
        
    }
    
    /**
     * Creates the region with the information to process it:
     * - Equation set of every model.
     * - Interactions
     * - Boundary conditions for the region
     * - Finalization condition of the problem.
     * @param problem                   The problem
     * @param region                    The region to use
     * @param regName                   The region name
     * @return                          A document fragment with the equation sets of the region
     * @throws AGDMException            AGDM007 External error
     */
    public DocumentFragment createRegionProblem(Document problem, Element region, String regName) 
            throws AGDMException {
        
        DocumentFragment result = problem.createDocumentFragment();
        result.appendChild(problem.importNode(region.cloneNode(true), true));
        //Copy the equationSet of the problem
        NodeList modelIdList = AGDMUtils.find(result, "//mms:interiorModel|//mms:surfaceModel");
        for (int i = modelIdList.getLength() - 1; i >= 0; i--) {
            String modelName = modelIdList.item(i).getTextContent();
            Element equationSet = (Element) AGDMUtils.find(problem, "//mms:model[mms:name[text() = '" + modelName + "']]//"
                    + "mms:equationSet").item(0);
            modelIdList.item(i).getParentNode().replaceChild(equationSet.cloneNode(true), modelIdList.item(i));
        }
        
        //Copy the regionInteraction
        if (AGDMUtils.find(region, "./ancestor::mms:region/mms:interactions|./ancestor::mms:subregion/mms:interactions").getLength() > 0) {
            Element regionInteraction = (Element) AGDMUtils.find(region, 
                    "./ancestor::mms:region/mms:interactions|./ancestor::mms:subregion/mms:interactions").item(0);
            result.appendChild(problem.importNode(regionInteraction.cloneNode(true), true));
        }
        
        //Copy the boundary conditions
        NodeList boundaries = AGDMUtils.find(problem, ".//mms:boundaryPolicy[*/mms:regionName = '" + regName + "']");
        if (boundaries.getLength() > 0) {
            result.appendChild(boundaries.item(0).cloneNode(true));
        }
        else {
            throw new AGDMException(AGDMException.AGDM008, "The region \"" + regName + "\" does not have defined boundaries."); 
        }
        
        //Copy the finalization conditions
        Element finalizationConditions = (Element) AGDMUtils.find(problem, "//mms:finalizationConditions").item(0);
        result.appendChild(finalizationConditions.cloneNode(true));
        return result;
    }
 
    
    /**
     * Create the header tag for the problem document.
     * 
     * @param result        The document to construct the header
     * @param problem       The problem that the header must be constructed from
     */
    private void makeHeader(Document result, Document problem) {
        //The header are the first child of the root element.
        Element originalHeader = (Element) problem.getDocumentElement().getFirstChild();
        Element root = result.getDocumentElement();
        Element header = AGDMUtils.createElement(result, mmsUri, "head");
        root.appendChild(header);
        //Name
        Element name = AGDMUtils.createElement(result, mmsUri, "name");
        Element problemName = (Element) originalHeader.getFirstChild();
        name.setTextContent(problemName.getTextContent());
        header.appendChild(name);
        //Empty id
        Element id = AGDMUtils.createElement(result, mmsUri, "id");
        header.appendChild(id);
        //Author
        Element author = AGDMUtils.createElement(result, mmsUri, "author");
        author.setTextContent((originalHeader.getElementsByTagName("mms:author").item(0)).getTextContent());
        header.appendChild(author);
        //Version
        Element problemVersion = (Element) (originalHeader.getElementsByTagName("mms:version").item(0));
        Element version = AGDMUtils.createElement(result, mmsUri, "version");
        version.setTextContent(problemVersion.getTextContent());
        header.appendChild(version);
        //Date
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Date actual = new Date();
        Element date = AGDMUtils.createElement(result, mmsUri, "date");
        date.setTextContent(formatter.format(actual));
        header.appendChild(date);
        //Description
        NodeList problemDescription = originalHeader.getElementsByTagName("mms:description");
        if (problemDescription.getLength() > 0) {
            Element description = AGDMUtils.createElement(result, mmsUri, "description");
            description.setTextContent(problemDescription.item(0).getTextContent());
            header.appendChild(description);
        }
    }
    /**
     * Generates the name for discrete coordinates.
     * Copy all the fields and parameters to the discretized problem.
     * Also deploy the coordinates in the problem for every field.
     * 
     * @param discProblem            The discretized problem
     * @param problem           The problem from the header must be constructed
     * @param policy			Discretization policy
     * @return                  A hashMap with the relation of the continuous and discrete coordinates, 
     *                          and a hashMap with the coordinates and fields of each region.
     * @throws AGDMException    AGDM007 - External error
     */
    private CoordinateInfo processFieldsAndCoordinates(Document discProblem, Document problem, DiscretizationPolicy policy) 
    		throws AGDMException {
        //Discretized coordinate candidates. At the begining the spatial candidates, at the end the time candidates.
        ArrayList < String > candidateCoords = getDiscCoordinateCandidates(problem);
        //Creation of the coordinate node with the discretized coordinates in the discretized document
        Element root = discProblem.getDocumentElement();
        Element coordinates = (Element) discProblem.importNode((AGDMUtils.find(problem, "//mms:coordinates").item(0)).cloneNode(true), true);
        ArrayList<String> discCoords = new ArrayList<String>();
        ArrayList<String> contCoords = new ArrayList<String>();
        String discTimeCoord = "";
        String contTimeCoord = "";
        NodeList coordList =  AGDMUtils.find(coordinates, ".//mms:spatialCoordinate");
        for (int i = 0; i < coordList.getLength(); i++) {
            Element coord = (Element) coordList.item(i);
            String discCoordName = candidateCoords.get(i); 
            discCoords.add(discCoordName);
            contCoords.add(coord.getTextContent());
            //Change the continuous coordinate by a discretized one
            coord.setTextContent(discCoordName);
            
        }
        coordList =  AGDMUtils.find(coordinates, ".//mms:timeCoordinate");
        for (int i = 0; i < coordList.getLength(); i++) {
            Element coord = (Element) coordList.item(i);
            String discCoordName = candidateCoords.get(candidateCoords.size() - 1);
        	discTimeCoord = discCoordName;
        	contTimeCoord = coord.getTextContent();
            //Change the continuous coordinate by a discretized one
            coord.setTextContent(discCoordName);
            
        }
        root.appendChild(coordinates);
        CoordinateInfo ci = new CoordinateInfo(discCoords, contCoords, discTimeCoord, contTimeCoord);
        
        //Get the base Coordinates
        NodeList base = AGDMUtils.find(problem, "//mms:spatialCoordinates/mms:spatialCoordinate|//mms:coordinates//mms:timeCoordinate");
        ArrayList<String> coords = new ArrayList<String>();
        for (int i = 0; i < base.getLength(); i++) {
            coords.add(base.item(i).getTextContent());
        }
        
        //Creation of the fields and parameters tags in the discretized result document
        Element fieldsE = AGDMUtils.createElement(discProblem, mmsUri, "fields");
        root.appendChild(fieldsE);
        NodeList nodes =  AGDMUtils.find(problem, "/*/mms:fields/mms:field");
        for (int i = 0; i < nodes.getLength(); i++) {
            fieldsE.appendChild(discProblem.adoptNode(nodes.item(i).cloneNode(true)));
        }
        Element auxiliaryFields = AGDMUtils.createElement(discProblem, mmsUri, "auxiliaryFields");
        nodes =  AGDMUtils.find(problem, "/*/mms:auxiliaryFields/mms:auxiliaryField");
        for (int i = 0; i < nodes.getLength(); i++) {
            auxiliaryFields.appendChild(discProblem.adoptNode(nodes.item(i).cloneNode(true)));
        }
        if (auxiliaryFields.hasChildNodes()) {
            root.appendChild(auxiliaryFields);
        }
        Element auxiliaryVariables = AGDMUtils.createElement(discProblem, mmsUri, "auxiliaryVariables");
        nodes =  AGDMUtils.find(problem, "/*/mms:auxiliaryVariables/mms:auxiliaryVariable");
        for (int i = 0; i < nodes.getLength(); i++) {
            auxiliaryVariables.appendChild(discProblem.adoptNode(nodes.item(i).cloneNode(true)));
        }
        if (auxiliaryVariables.hasChildNodes()) {
            root.appendChild(auxiliaryVariables);
        }
        Element analysisFields = AGDMUtils.createElement(discProblem, mmsUri, "analysisFields");
        nodes =  AGDMUtils.find(problem, "/*/mms:analysisFields/mms:analysisField");
        for (int i = 0; i < nodes.getLength(); i++) {
            analysisFields.appendChild(discProblem.adoptNode(nodes.item(i).cloneNode(true)));
        }
        if (analysisFields.hasChildNodes()) {
            root.appendChild(analysisFields);
        }
        Element auxiliaryAnalysisVariables = AGDMUtils.createElement(discProblem, mmsUri, "auxiliaryAnalysisVariables");
        nodes =  AGDMUtils.find(problem, "/*/mms:auxiliaryAnalysisVariables/mms:auxiliaryAnalysisVariable");
        for (int i = 0; i < nodes.getLength(); i++) {
            auxiliaryAnalysisVariables.appendChild(discProblem.adoptNode(nodes.item(i).cloneNode(true)));
        }
        if (auxiliaryAnalysisVariables.hasChildNodes()) {
            root.appendChild(auxiliaryAnalysisVariables);
        }
        //Creation of the tensors in the discretized result document
        Element tensors = AGDMUtils.createElement(discProblem, mmsUri, "tensors");
        nodes =  AGDMUtils.find(problem, "//mms:tensor");
        for (int i = 0; i < nodes.getLength(); i++) {
            tensors.appendChild(discProblem.adoptNode(nodes.item(i).cloneNode(true)));
        }
        if (tensors.hasChildNodes()) {
            root.appendChild(tensors);
        }
        //Creation of the fields and parameters tags in the discretized result document.
        Element parameters = AGDMUtils.createElement(discProblem, mmsUri, "parameters");
        nodes = AGDMUtils.find(problem, "/*/mms:parameters/mms:parameter");
        for (int i = 0; i < nodes.getLength(); i++) {
            parameters.appendChild(discProblem.adoptNode(nodes.item(i).cloneNode(true)));
        }
        //Add parameters from schemas
        ArrayList<Node> schemaParams = policy.getSchemaParameters();
        for (int i = 0; i < schemaParams.size(); i++) {
            parameters.appendChild(discProblem.adoptNode(schemaParams.get(i).cloneNode(true)));
        }
        if (parameters.hasChildNodes()) {
            root.appendChild(parameters);
        }
        //Creation of the deltas (implicit parameters) in the discretized result document
        Element implicitParameters = AGDMUtils.createElement(discProblem, mmsUri, "implicitParameters");
        coordList =  AGDMUtils.find(problem, ".//mms:coordinates//mms:spatialCoordinate");
        for (int i = 0; i < coordList.getLength(); i++) {
            Node implicitParameter = AGDMUtils.createElement(discProblem, mmsUri, "implicitParameter");
            implicitParameters.appendChild(implicitParameter);
            //Type
            Node ipType = AGDMUtils.createElement(discProblem, mmsUri, "type");
            ipType.setTextContent("delta_space");
            implicitParameter.appendChild(ipType.cloneNode(true));
            //Name
            Node ipValue = AGDMUtils.createElement(discProblem, mmsUri, "value");
            ipValue.setTextContent("\u0394" + ci.getContCoords().get(i));
            implicitParameter.appendChild(ipValue.cloneNode(true));
            //Coordinate
            Node ipCoord = AGDMUtils.createElement(discProblem, mmsUri, "coordinate");
            ipCoord.setTextContent(ci.getDiscCoords().get(i));
            implicitParameter.appendChild(ipCoord.cloneNode(true));
        }
        coordList =  AGDMUtils.find(problem, ".//mms:coordinates//mms:timeCoordinate");
        for (int i = 0; i < coordList.getLength(); i++) {
            Node implicitParameter = AGDMUtils.createElement(discProblem, mmsUri, "implicitParameter");
            implicitParameters.appendChild(implicitParameter);
            //Type
            Node ipType = AGDMUtils.createElement(discProblem, mmsUri, "type");
            ipType.setTextContent("delta_time");
            implicitParameter.appendChild(ipType.cloneNode(true));
            //Name
            Node ipValue = AGDMUtils.createElement(discProblem, mmsUri, "value");
            ipValue.setTextContent("\u0394" + ci.getContTimeCoord());
            implicitParameter.appendChild(ipValue.cloneNode(true));
            //Coordinate
            Node ipCoord = AGDMUtils.createElement(discProblem, mmsUri, "coordinate");
            ipCoord.setTextContent(ci.getDiscTimeCoord());
            implicitParameter.appendChild(ipCoord.cloneNode(true));
        }
        root.appendChild(implicitParameters);
        //Creation of vectorName tags
        Element vectorNames = AGDMUtils.createElement(discProblem, mmsUri, "vectorNames");
        NodeList vectors = AGDMUtils.find(problem, "/*/mms:eigenVectors/mms:eigenVector");
        for (int i = 0; i < vectors.getLength(); i++) {
            Element vectorName = AGDMUtils.createElement(discProblem, mmsUri, "vectorName");
            vectorName.setTextContent(vectors.item(i).getTextContent());
        }
        if (vectorNames.hasChildNodes()) {
            root.appendChild(vectorNames);
        }
        //Return the array list with the relations hashMaps
        return ci;
    }
    
    /**
     * Create a candidate list for the name of the discretized coordinates.
     * @param problem           The problem
     * @return                  The list
     * @throws AGDMException    AGDM007 External error
     */
    private ArrayList<String> getDiscCoordinateCandidates(Document problem) throws AGDMException {
        String[] abc = {"i", "j", "k", "x", "y", "z", "m", "o", "p", "q", "r", "s", "u", "v",
            "w", "a", "b", "c", "d", "e", "f", "g", "h", "t", "l", "n"};
        ArrayList < String > candidateCoords = new ArrayList< String >(Arrays.asList(abc));
            
        //Discard candidates that are already an identified inside the problem
        String identifiersQuery = "//mms:coordinates//mms:timeCoordinate|//mms:coordinates//" 
            + "mms:spatialCoordinate|/*/mms:fields/mms:field|/*/mms:auxiliaryFields/mms:auxiliaryField|"
            + "/*/mms:parameters/mms:parameter/mms:name|//mms:vectorName";
        NodeList identifiers =  AGDMUtils.find(problem, identifiersQuery);
        for (int i = 0; i < identifiers.getLength(); i++) {
            Element contCoord = (Element) identifiers.item(i);
            if (candidateCoords.contains(contCoord.getTextContent())) {
                candidateCoords.remove(contCoord.getTextContent());
            }
        }
        return candidateCoords;
    }
    
    /**
     * Process initial conditions. It classifies mesh and particle initial conditions. 
     * It also checks if particle equations need any mesh field and adds interpolation.
     * @param problem			The problem
     * @param policy			The policy
     * @throws AGDMException	AGDM00X External error
     */
    private void processInitialConditions(Document problem, DiscretizationPolicy policy, ProcessInfo pi) throws AGDMException {
        ArrayList<String> meshFields = policy.getMeshFields();
    	//Process Initial Conditions (Even in boundary usage)
    	HashSet<String> dependentVariablesMesh = new HashSet<String>();
    	HashMap<String, HashSet<String>> dependentVariablesParticles = new HashMap<String, HashSet<String>>();
    	for (String speciesName : policy.getParticleSpecies().keySet()) {
    		dependentVariablesParticles.put(speciesName, new HashSet<String>());
    	}
        NodeList initialConditions =  AGDMUtils.find(problem, ".//mms:initialConditions");
        for (int i = 0; i < initialConditions.getLength(); i++) {
            Element initialConditionsNew = AGDMUtils.createElement(problem, mmsUri, "mms:initialConditions");
    		//Special case (external initial data)
        	if (initialConditions.item(i).getFirstChild().getLocalName().equals("externalInitialCondition")) {
        		initialConditionsNew.appendChild(initialConditions.item(i).getFirstChild().cloneNode(true));
        	}
        	if (((Element) initialConditions.item(i)).getElementsByTagNameNS(mmsUri, "initialCondition").getLength() > 0) {
	            Element initialConditionNew = AGDMUtils.createElement(problem, mmsUri, "mms:initialCondition");
	            initialConditionsNew.appendChild(initialConditionNew);
	            HashMap<String, DocumentFragment> initialConditionParticles = new HashMap<String, DocumentFragment>();
	        	for (String speciesName : policy.getParticleSpecies().keySet()) {
	        		initialConditionParticles.put(speciesName, problem.createDocumentFragment());
	        	}
	            DocumentFragment initialConditionMesh = problem.createDocumentFragment();
	            NodeList initialConditionList = ((Element) initialConditions.item(i)).getElementsByTagNameNS(mmsUri, "initialCondition");
    			HashSet<String> usedVars = new HashSet<String>();
	            for (int j = initialConditionList.getLength() - 1; j >= 0; j--) {
	            	HashMap<String, DocumentFragment> localInitialConditionParticles = new HashMap<String, DocumentFragment>();
	            	for (String speciesName : policy.getParticleSpecies().keySet()) {
	            		localInitialConditionParticles.put(speciesName, problem.createDocumentFragment());
		        	}
	                DocumentFragment localInitialConditionMesh = problem.createDocumentFragment();
	            	Element initialCondition = (Element) initialConditionList.item(j);
	            	//Get applyIf, if it exists
	            	NodeList condition =  AGDMUtils.find(initialCondition, ".//mms:applyIf/*");
	            	if (condition.getLength() > 0) {
		    	    	NodeList vars = AGDMUtils.find(condition.item(0), "descendant-or-self::mt:ci");
		    	    	for (int k = 0; k < vars.getLength(); k++) {
		    	    		Node var = vars.item(k);
		    	    		usedVars.add(var.getTextContent());
		    	    	}
	            	}
            	
	            	//Classify expressions
	            	classifyInitialConditionExpressions(initialCondition, localInitialConditionMesh, localInitialConditionParticles, meshFields, policy.getParticleSpecies(), dependentVariablesMesh, dependentVariablesParticles, usedVars);
	            	
	            	//Check if there is an ApplyIf for the condition
	            	if (condition.getLength() > 0) {
	                	Element ifElem = AGDMUtils.createElement(problem, smlUri, "if");
	                	ifElem.appendChild(condition.item(0).cloneNode(true));
	                	
	                	if (localInitialConditionMesh.hasChildNodes()) {
	                    	Element thenElem = AGDMUtils.createElement(problem, smlUri, "then");
	                       	Element ifElemMesh = (Element) ifElem.cloneNode(true);
	                       	ifElemMesh.appendChild(thenElem);
	                       	thenElem.appendChild(localInitialConditionMesh);
	                       	localInitialConditionMesh = problem.createDocumentFragment();
	                       	localInitialConditionMesh.appendChild(ifElemMesh);
	                	}
	                  	for (Entry<String, DocumentFragment> entry : localInitialConditionParticles.entrySet()) {
		    				String speciesName = entry.getKey();
		                	if (entry.getValue().hasChildNodes()) {
		                    	Element thenElem = AGDMUtils.createElement(problem, smlUri, "then");
		                       	Element ifElemParticles = (Element) ifElem.cloneNode(true);
		                       	ifElemParticles.appendChild(thenElem);
		                       	thenElem.appendChild(entry.getValue());
		                       	DocumentFragment tmp = problem.createDocumentFragment();
		                       	tmp.appendChild(ifElemParticles);
		                       	localInitialConditionParticles.put(speciesName, tmp);
		                	}
	                  	}
	            	}
	            	if (localInitialConditionMesh.hasChildNodes()) {
	            		if (initialConditionMesh.hasChildNodes()) {
	            			initialConditionMesh.insertBefore(localInitialConditionMesh, initialConditionMesh.getFirstChild());
	            		}
	            		else {
	            			initialConditionMesh.appendChild(localInitialConditionMesh);
	            		}
	            	}
		        	for (Entry<String, DocumentFragment> entry : localInitialConditionParticles.entrySet()) {
	    				String speciesName = entry.getKey();
		            	if (entry.getValue().hasChildNodes()) {
		            		DocumentFragment tmp = initialConditionParticles.get(speciesName);
		            		if (tmp.hasChildNodes()) {
		            			tmp.insertBefore(entry.getValue(), tmp.getFirstChild());
		            		}
		            		else {
		            			tmp.appendChild(entry.getValue());
		            		}
		            	}
		        	}
	            }
	            //Check if interpolation is needed 
	            HashMap<String, ArrayList<String>> variablesToInterpolate = new HashMap<String, ArrayList<String>>();
	        	Element simml = AGDMUtils.createElement(problem, smlUri, "simml");
	        	boolean hasDependencies = false;
	        	if (initialConditionMesh.hasChildNodes()) {
	        		ArrayList<String> meshVariables = policy.getMeshFields();
	        		for (Entry<String, DocumentFragment> entry : initialConditionParticles.entrySet()) {
	    				String speciesName = entry.getKey();
	    				ArrayList<String> varsToInterpolate = new ArrayList<String>();
	    		       	if (entry.getValue().hasChildNodes()) {
	    		       		for (int j = 0; j < meshVariables.size(); j++) {
	    		       			String field = meshVariables.get(j);
		    		        	if (AGDMUtils.find(entry.getValue(), ".//mt:ci[text() = '" + field + "']").getLength() > 0) {
		    		        		varsToInterpolate.add(field);
		    		        		hasDependencies = true;
		    		        	}
	    		       		}
	    	        	}
	    		       	variablesToInterpolate.put(speciesName, varsToInterpolate);
		        	}
	        	}
	        	
	        	//If mesh and particle IC and particle equations depend on mesh fields do interpolation
	        	if (hasDependencies) {
	        		//Mesh iteration
	        		Element iterateOverCellsMesh = AGDMUtils.createElement(problem, AGDMUtils.simmlUri, "iterateOverCells");
	        		iterateOverCellsMesh.setAttribute("stencilAtt", String.valueOf(0));
	        		iterateOverCellsMesh.appendChild(initialConditionMesh);
	        		//Interpolation and particle
	        		Element iterateOverCellsParticles = AGDMUtils.createElement(problem, AGDMUtils.simmlUri, "iterateOverCells");
	        		iterateOverCellsParticles.setAttribute("stencilAtt", String.valueOf(0));
	              	for (Entry<String, DocumentFragment> entry : initialConditionParticles.entrySet()) {
	    				String speciesName = entry.getKey();
	    		       	if (entry.getValue().hasChildNodes()) {
	    	            	Element iterateOverParticles = AGDMUtils.createElement(problem, smlUri, "iterateOverParticles");
	    	            	iterateOverParticles.setAttribute("stencilAtt", String.valueOf(0));
	    	            	iterateOverParticles.setAttribute("speciesNameAtt", speciesName);
	    	            	//Adding interpolation
	    	            	ArrayList<String> varsToInterpolate = variablesToInterpolate.get(speciesName);
	    	            	if (varsToInterpolate.size() > 0) {
	    	            		//Adding common interpolation variables
	    	            		Element timeOutputVariable = AGDMUtils.createTimeSlice(problem);
	    	            		Element positionTemplate = (Element) SchemaCreator.convertToPosition(timeOutputVariable);
	    	            		iterateOverParticles.appendChild(SchemaCreator.createMeshInterpolationCalls(problem, varsToInterpolate, pi, timeOutputVariable, positionTemplate, entry.getKey()));
	    	            	}
	    	            	iterateOverParticles.appendChild(entry.getValue());
	    	            	iterateOverCellsParticles.appendChild(iterateOverParticles);
	    	        	}
		        	}
	         		simml.appendChild(iterateOverCellsMesh);
	         		simml.appendChild(iterateOverCellsParticles);
	        	} else {
	        		Element iterateOverCells = AGDMUtils.createElement(problem, AGDMUtils.simmlUri, "iterateOverCells");
	        		iterateOverCells.setAttribute("stencilAtt", String.valueOf(0));
	             	if (initialConditionMesh.hasChildNodes()) {
	             		iterateOverCells.appendChild(initialConditionMesh);
		        	}
	              	for (Entry<String, DocumentFragment> entry : initialConditionParticles.entrySet()) {
	    				String speciesName = entry.getKey();
	    		       	if (entry.getValue().hasChildNodes()) {
	    	            	Element iterateOverParticles = AGDMUtils.createElement(problem, smlUri, "iterateOverParticles");
	    	            	iterateOverParticles.setAttribute("speciesNameAtt", speciesName);
	    	            	iterateOverParticles.appendChild(entry.getValue());
	    	            	iterateOverCells.appendChild(iterateOverParticles);
	    	        	}
		        	}
	        		simml.appendChild(iterateOverCells);
	        	}
	        	
	        	
	        	initialConditionNew.appendChild(simml);
	        	
	        	
	        	
        	}
        	
            //Replace initial conditions with the new ones
            initialConditions.item(i).getParentNode().replaceChild(initialConditionsNew, initialConditions.item(i));
        }
        
    }
    
    /**
     * Process boundary conditions. It classifies mesh and particle boundaries. 
     * @param result			The result
     * @param problem			The problem
     * @param information		coordinate and region relations
     * @param policy			The policy
     * @param referenceFrames	Reference frame for fields 
     * @throws AGDMException	AGDM00X External error
     */
    private void processBoundaryConditions(Document result, Document problem, CoordinateInfo ci, 
    		DiscretizationPolicy policy, HashMap<String, ReferenceFrameType> referenceFrames, ConstantInformation constants) throws AGDMException {
        ArrayList<String> meshFields = policy.getMeshFields();
        //Create discretized spatial coordinates
        HashMap<String, Element> discCoordParticles = new HashMap<String, Element>();

        NodeList coordinates =  AGDMUtils.find(problem, ".//mms:coordinates//mms:spatialCoordinate");
        for (int i = 0; i < coordinates.getLength(); i++) {
            String coord = coordinates.item(i).getTextContent();
            String discCoord = ci.getDiscCoords().get(i);
            //Mesh (current cell index)
            //Not conversion needed
            //Particles (particle position)
            Element newNode = AGDMUtils.createElement(problem, mtUri, "ci");
            Element coordinate = AGDMUtils.createElement(problem, mtUri, "ci");
            Element time = AGDMUtils.createElement(problem, mtUri, "ci");
            Element apply = AGDMUtils.createElement(problem, mtUri, "apply");
            Element msup = AGDMUtils.createElement(problem, mtUri, "msup");
            Element plus = AGDMUtils.createElement(problem, mtUri, "plus");
            Element one = AGDMUtils.createElement(problem, mtUri, "cn");
            coordinate.setTextContent(discCoord);
            time.setTextContent("(" + ci.getDiscTimeCoord() + ")");
            one.setTextContent("1");
            newNode.appendChild(msup);
            msup.appendChild(coordinate);
            msup.appendChild(apply);
            apply.appendChild(plus);
            apply.appendChild(time);
            apply.appendChild(one);
            discCoordParticles.put(coord, newNode);
        }
    	//Process Boundary Conditions
        String coordCondition = "";
        for (int i = 0; i < coordinates.getLength(); i++) {
            String coord = coordinates.item(i).getTextContent();
	        NodeList contCoords = AGDMUtils.find(problem, "//mms:boundaryCondition//mms:expression//mt:ci[text() = '" + coord + "']");
	        for (int j = 0; j < contCoords.getLength(); j++) {
	        	String variable = AGDMUtils.find(contCoords.item(j), ".//ancestor::mms:expression/mms:field").item(0).getTextContent();
	        	//Expression of a particle field
	    		for (Species s : policy.getParticleSpecies().values()) {
		       		if (s.getParticleFields().contains(variable)) {
		    			contCoords.item(j).getParentNode().replaceChild(discCoordParticles.get(coord), contCoords.item(j));
		    		}
	    		}
	        }
	        coordCondition = coordCondition + "text() = '" + coord + "' or ";
        }
        coordCondition = coordCondition.substring(0, coordCondition.lastIndexOf(" or"));
        //Boundaries with applyIf must be separated
        NodeList applyIfBoundaries = AGDMUtils.find(problem, "//mms:boundaryCondition[descendant::mms:applyIf//mt:ci[" + coordCondition + "]]");
        for (int i = applyIfBoundaries.getLength() - 1; i >= 0; i--) {
        	Element boundary = (Element) applyIfBoundaries.item(i);
            Node type = boundary.getElementsByTagNameNS(mmsUri, "type").item(0);
            Node axis = boundary.getElementsByTagNameNS(mmsUri, "axis").item(0);
            Node side = boundary.getElementsByTagNameNS(mmsUri, "side").item(0);
            NodeList fields = boundary.getElementsByTagNameNS(mmsUri, "fields").item(0).getChildNodes();
            Node applyIf = boundary.getElementsByTagNameNS(mmsUri, "applyIf").item(0);
        	DocumentFragment boundaryFragment = problem.createDocumentFragment();

        	Node typeMesh = AGDMUtils.createElement(problem, mmsUri, "mms:type");
        	Node typeParticles = AGDMUtils.createElement(problem, mmsUri, "mms:type");
        	if (type.getFirstChild().getLocalName().equals("reflection") || type.getFirstChild().getLocalName().equals("algebraic")) {
            	Node reflectionMesh = type.getFirstChild().cloneNode(false);
            	Node reflectionParticles = type.getFirstChild().cloneNode(false);
        		typeMesh.appendChild(reflectionMesh);
        		typeParticles.appendChild(reflectionParticles);
        		NodeList signs = type.getFirstChild().getChildNodes();
        		for (int j = 0; j < signs.getLength(); j++) {
        			String field = signs.item(j).getFirstChild().getTextContent();
        			for (Species s : policy.getParticleSpecies().values()) {
    		       		if (s.getParticleFields().contains(field)) {
	            			reflectionParticles.appendChild(signs.item(j).cloneNode(true));
	            		}
        			}
            		if (meshFields.contains(field)) {
            			reflectionMesh.appendChild(signs.item(j).cloneNode(true));
            		}
        		}
        	} 
        	else {
        		typeMesh = type.cloneNode(true);
        		typeParticles = type.cloneNode(true);
        	}
        	
            //Create particle initial conditions structure
        	Node boundaryConditionParticles = AGDMUtils.createElement(problem, mmsUri, "mms:boundaryCondition");
        	boundaryConditionParticles.appendChild(typeParticles);
        	boundaryConditionParticles.appendChild(axis.cloneNode(true));
        	boundaryConditionParticles.appendChild(side.cloneNode(true));
        	Node fieldsParticles = AGDMUtils.createElement(problem, mmsUri, "mms:fields");
        	boundaryConditionParticles.appendChild(fieldsParticles);
        	Node applyIfParticles = applyIf.cloneNode(true);
        	boundaryConditionParticles.appendChild(applyIfParticles);
        	//Create mesh initial conditions structure
        	Node boundaryConditionMesh = AGDMUtils.createElement(problem, mmsUri, "mms:boundaryCondition");
        	boundaryConditionMesh.appendChild(typeMesh);
        	boundaryConditionMesh.appendChild(axis.cloneNode(true));
        	boundaryConditionMesh.appendChild(side.cloneNode(true));
        	Node fieldsMesh = AGDMUtils.createElement(problem, mmsUri, "mms:fields");
        	boundaryConditionMesh.appendChild(fieldsMesh);
        	Node applyIfMesh = applyIf.cloneNode(true);
        	boundaryConditionMesh.appendChild(applyIfMesh);
        	for (int j = 0; j < fields.getLength(); j++) {
        		for (Species s : policy.getParticleSpecies().values()) {
		       		if (s.getParticleFields().contains(fields.item(j).getTextContent())) {
	        			fieldsParticles.appendChild(fields.item(j).cloneNode(true));
	        		}
		       	}
        		if (meshFields.contains(fields.item(j).getTextContent())) {
        			fieldsMesh.appendChild(fields.item(j).cloneNode(true));
        		}
        	}
            if (fieldsMesh.getChildNodes().getLength() > 0) {
            	boundaryFragment.appendChild(boundaryConditionMesh);
            }
            if (fieldsParticles.getChildNodes().getLength() > 0) {
            	boundaryFragment.appendChild(boundaryConditionParticles);
            }
        	//Now, the boundary conditions are classified, so the coordinates can be replaced
            for (int j = 0; j < coordinates.getLength(); j++) {
                String coord = coordinates.item(j).getTextContent();
                //Replace the coordinate occurrences
                Node oldNode = AGDMUtils.createElement(problem, mtUri, "ci");
                oldNode.setTextContent(coord);
                AGDMUtils.nodeReplace(applyIfParticles, oldNode, discCoordParticles.get(coord));
            }
            if (fieldsMesh.getChildNodes().getLength() > 0) {
            	boundaryFragment.appendChild(boundaryConditionMesh);
            }
            if (fieldsParticles.getChildNodes().getLength() > 0) {
            	boundaryFragment.appendChild(boundaryConditionParticles);
            }
            boundary.getParentNode().replaceChild(boundaryFragment, boundary);
        }
        //Policy post initialization
        Element resultInit = AGDMUtils.createElement(problem, smlUri, "simml");
        if (policy.getPostInitialization() != null) {
            Element iterOverCells = AGDMUtils.createElement(problem, smlUri, "iterateOverCells");
            iterOverCells.setAttribute("stencilAtt", String.valueOf(0));
            //Add a region info with all fields
            ArrayList<String> fields = new ArrayList<String>();
            HashMap<String, Boolean> isAuxiliary = new HashMap<String, Boolean>();
            NodeList fieldList = AGDMUtils.find(problem, "/*/mms:fields/mms:field|/*/mms:auxiliaryFields/mms:auxiliaryField"
                    + "|/*/mms:analysisFields/mms:analysisField");
            for (int j = 0; j < fieldList.getLength(); j++) {
                fields.add(fieldList.item(j).getTextContent());
                isAuxiliary.put(fieldList.item(j).getTextContent(), fieldList.item(j).getLocalName().equals("auxiliaryField"));
            }

            RegionInfo ri = new RegionInfo(fields, isAuxiliary);
            ProcessInfo pi = createProcessInfoGeneric(problem, result, ri, ci, policy, referenceFrames, constants);
            Node postInitSchemaInst = processPostInit(result, problem.getDocumentElement(), policy.getPostInitialization(), pi);
            iterOverCells.appendChild(problem.importNode(postInitSchemaInst, true));
            resultInit.appendChild(iterOverCells);
        }
        if (resultInit.hasChildNodes()) {
            Element postInitialCondition = AGDMUtils.createElement(problem, mmsUri, "initialCondition");
            postInitialCondition.appendChild(resultInit);
            NodeList initialConditions = AGDMUtils.find(problem, ".//mms:boundaryCondition//mms:initialConditions");
            for (int i = 0; i < initialConditions.getLength(); i++) {
            	initialConditions.item(i).appendChild(postInitialCondition.cloneNode(true));
            }
        }
    }
    
    /**
     * Make the transformations for the coordinate scoped transformation rule.
     * 
     * @param problem           document problem
     * @param coordRelation     relations of the discrete and continuous coordinates
     * @param policy			discretization policy
     * @throws AGDMException    AGDM007 External error
     */
    private void coordinateTransform(Document problem, CoordinateInfo coordRelation, DiscretizationPolicy policy)  throws AGDMException {
        //Replace in the rule the tags with the discretized values
    	//Time coordinate
        String timeCoord =  AGDMUtils.find(problem, ".//mms:coordinates//mms:timeCoordinate").item(0).getTextContent();
        //Replace the coordinate occurrences
        Element oldNode = AGDMUtils.createElement(problem, mtUri, "ci");
        oldNode.setTextContent(timeCoord);
    	Element newNode = AGDMUtils.createElement(problem, smlUri, "currentTime");
        AGDMUtils.nodeReplace(problem, oldNode, newNode);
        
        //Create discretized spatial coordinates
        HashMap<String, Element> discCoordParticles = new HashMap<String, Element>();
        NodeList coordinates =  AGDMUtils.find(problem, ".//mms:coordinates//mms:spatialCoordinate");
        for (int i = 0; i < coordinates.getLength(); i++) {
            String coord = coordinates.item(i).getTextContent();
            //Mesh (current cell index)
            //No transformation needed
            //Particles (particle position)
            newNode = AGDMUtils.createElement(problem, mtUri, "ci");
            Element particlePosition = AGDMUtils.createElement(problem, smlUri, "particlePosition");
            particlePosition.setTextContent(coord);
            Element coordinate = AGDMUtils.createElement(problem, mtUri, "ci");
            Element time = AGDMUtils.createElement(problem, mtUri, "ci");
            Element apply = AGDMUtils.createElement(problem, mtUri, "apply");
            Element msup = AGDMUtils.createElement(problem, mtUri, "msup");
            Element plus = AGDMUtils.createElement(problem, mtUri, "plus");
            Element one = AGDMUtils.createElement(problem, mtUri, "cn");
            coordinate.appendChild(particlePosition);
            time.setTextContent("(" + coordRelation.getDiscTimeCoord() + ")");
            one.setTextContent("1");
            newNode.appendChild(msup);
            msup.appendChild(coordinate);
            msup.appendChild(apply);
            apply.appendChild(plus);
            apply.appendChild(time);
            apply.appendChild(one);
            discCoordParticles.put(coord, newNode);
        }
        
    	//Process Initial Conditions (Even in boundary usage)
        NodeList particleConditions =  AGDMUtils.find(problem, ".//mms:initialConditions/mms:initialCondition//mt:math[ancestor::sml:iterateOverParticles]");
        for (int i = 0; i < particleConditions.getLength(); i++) {
        	Element initialCondition = (Element) particleConditions.item(i);
            for (int j = 0; j < coordinates.getLength(); j++) {
                String coord = coordinates.item(j).getTextContent();
                //Replace the coordinate occurrences
                oldNode = AGDMUtils.createElement(problem, mtUri, "ci");
                oldNode.setTextContent(coord);
                AGDMUtils.nodeReplace(initialCondition, oldNode, discCoordParticles.get(coord));
            }
        }
    }
    
    /**
     * Classifies the initial condition mathematical expressions in mesh and particles separately.
     * @param initialCondition				The original condition
     * @param mathExpressionsMesh			The mesh expressions
     * @param mathExpressionsParticles		The particle expressions
     * @param meshFields					Mesh fields
     * @param dependentVariablesMesh		Mesh dependent variables
     * @param dependentVariablesParticles	Particle dependent variables
     * @param dependentCondVars				Conditional dependent variables
     * @param particleInfo					Particle variable information
     * @throws AGDMException				AGDM008  Invalid simulation problem
     * 										AGDM00X  External error
     */
    private void classifyInitialConditionExpressions(Element initialCondition, DocumentFragment mathExpressionsMesh, HashMap<String, DocumentFragment> mathExpressionsParticles, 
    		ArrayList<String> meshFields, HashMap<String, Species> particleInfo, HashSet<String> dependentVariablesMesh, HashMap<String, HashSet<String>> dependentVariablesParticles, HashSet<String> dependentCondVars) throws AGDMException {
    	NodeList expressions = initialCondition.getLastChild().getChildNodes();
    	for (int j = expressions.getLength() - 1; j >= 0; j--) {
    		Node expression = expressions.item(j);
    		try {
    			String assignedVar = AGDMUtils.find(expression, "./mt:apply/mt:eq/following-sibling::*[1]").item(0).getTextContent();
    			HashSet<String> usedVars = new HashSet<String>();
    	    	NodeList vars = AGDMUtils.find(expression, "descendant-or-self::mt:math/mt:apply/mt:eq/following-sibling::*[2]/descendant-or-self::mt:ci");
    	    	for (int k = 0; k < vars.getLength(); k++) {
    	    		Node var = vars.item(k);
    	    		usedVars.add(var.getTextContent());
    	    	}
    			//Check if is a mesh variable or dependent variable
    			if (meshFields.contains(assignedVar) || dependentVariablesMesh.contains(assignedVar) || dependentCondVars.contains(assignedVar)) {
    				if (j == expressions.getLength() - 1) {
    					mathExpressionsMesh.appendChild(initialCondition.getOwnerDocument().importNode(expression.cloneNode(true), true));
    				}
    				else {
    					mathExpressionsMesh.insertBefore(initialCondition.getOwnerDocument().importNode(expression.cloneNode(true), true), mathExpressionsMesh.getFirstChild());
    				}
    				dependentVariablesMesh.addAll(usedVars);
    			}
    			//Check if is a particle variable or dependent variable
    			for (Entry<String, Species> entry : particleInfo.entrySet()) {
    				String speciesName = entry.getKey();
    				ArrayList<String> particleFields = entry.getValue().getParticleFields();
    				HashSet<String> dependentVars = dependentVariablesParticles.get(speciesName);
        			if (particleFields.contains(assignedVar) || dependentVars.contains(assignedVar) || dependentCondVars.contains(assignedVar)) {
        				DocumentFragment tmp = mathExpressionsParticles.get(speciesName);
        				if (j == expressions.getLength() - 1) {
        					tmp.appendChild(initialCondition.getOwnerDocument().importNode(expression.cloneNode(true), true));
        				}
        				else {
        					tmp.insertBefore(initialCondition.getOwnerDocument().importNode(expression.cloneNode(true), true), tmp.getFirstChild());
        				}
        				mathExpressionsParticles.put(speciesName, tmp);
        				dependentVars.addAll(usedVars);
        				dependentVariablesParticles.put(speciesName, dependentVars);
        			}
    			}
    		}
    		catch (Exception e) {
    			//Not assigning instruction (check then used variables inside functions that can be assigned inside)
    			HashSet<String> usedVars = new HashSet<String>();
    	    	NodeList vars = AGDMUtils.find(expression, "//mt:ci[ancestor::sml:readFromFileQuintic || ancestor::sml:readFromFileLinearWithDerivatives1D || ancestor::sml:readFromFileLinearWithDerivatives2D || ancestor::sml:readFromFileLinearWithDerivatives3D || ancestor::sml:readFromFileLinear1D || ancestor::sml:readFromFileLinear2D || ancestor::sml:readFromFileLinear3D || ancestor::sml:functionCall]");
    	    	for (int k = 0; k < vars.getLength(); k++) {
    	    		Node var = vars.item(k);
    	    		usedVars.add(var.getTextContent());
    	    	}
    	    	//Check if is a mesh variable or dependent variable
    			if (!Collections.disjoint(meshFields, usedVars) || !Collections.disjoint(dependentVariablesMesh, usedVars) || !Collections.disjoint(dependentCondVars, usedVars)) {
    				if (j == expressions.getLength() - 1) {
    					mathExpressionsMesh.appendChild(initialCondition.getOwnerDocument().importNode(expression.cloneNode(true), true));
    				}
    				else {
    					mathExpressionsMesh.insertBefore(initialCondition.getOwnerDocument().importNode(expression.cloneNode(true), true), mathExpressionsMesh.getFirstChild());
    				}
    				dependentVariablesMesh.addAll(usedVars);
    			}
    			//Check if is a particle variable or dependent variable
    			for (Entry<String, Species> entry : particleInfo.entrySet()) {
    				String speciesName = entry.getKey();
    				ArrayList<String> particleFields = entry.getValue().getParticleFields();
    				HashSet<String> dependentVars = dependentVariablesParticles.get(speciesName);
        			if (!Collections.disjoint(particleFields, usedVars) || !Collections.disjoint(dependentVars, usedVars) || !Collections.disjoint(dependentCondVars, usedVars)) {
        				DocumentFragment tmp = mathExpressionsParticles.get(speciesName);
        				if (j == expressions.getLength() - 1) {
        					tmp.appendChild(initialCondition.getOwnerDocument().importNode(expression.cloneNode(true), true));
        				}
        				else {
        					tmp.insertBefore(initialCondition.getOwnerDocument().importNode(expression.cloneNode(true), true), tmp.getFirstChild());
        				}
        				mathExpressionsParticles.put(speciesName, tmp);
        				dependentVars.addAll(usedVars);
        				dependentVariablesParticles.put(speciesName, dependentVars);
        			}
    			}
    	    	if (usedVars.size() == 0) {
    	    		throw new AGDMException(AGDMException.AGDM008, "Initial condition math expressions must be variable assignments.");
    	    	}
    		}
    	}
    }
      
    /**
     * Adds the execution flow structure to the regions.
     * 
     * @param problem           The document with the region information
     * @return                  The regions processed
     * @throws AGDMException    AGDM00X - External error
     */
    private DocumentFragment getRegions(Document problem)  throws AGDMException {
        //The result are stored in a document fragment
        DocumentFragment fragmentResult = problem.createDocumentFragment();
        
        Element region = (Element) (AGDMUtils.find(problem, "//mms:region")).item(0);
        fragmentResult.appendChild(region);
        Element subregions = (Element) (AGDMUtils.find(problem, "//mms:subregions")).item(0);
        if (subregions != null) {
            fragmentResult.appendChild(subregions);
        }
        return fragmentResult;
    }
    
    /**
     * Deploy the coordinates for a field in the problem.
     * @param problem               The problem document
     * @param coordMap              The coordinate relation
     * @throws AGDMException        AGDM007 - External error
     */
    private void deployCoordsOnProblem(Document problem, CoordinateInfo coordMap) throws AGDMException {
        //Time coordinate
        String timeCoordinate = coordMap.getDiscTimeCoord();
        
        //Base deployment
        Element applyBase = AGDMUtils.createElement(problem, mtUri, "apply");
        Element parentCi = AGDMUtils.createElement(problem, mtUri, "ci");
        applyBase.appendChild(parentCi);
        Element msup = AGDMUtils.createElement(problem, mtUri, "msup");
        parentCi.appendChild(msup);
        Element fieldCi = AGDMUtils.createElement(problem, mtUri, "ci");
        msup.appendChild(fieldCi);
        Element timeCi = AGDMUtils.createElement(problem, mtUri, "ci");
        timeCi.setTextContent("(" + timeCoordinate + ")");
        msup.appendChild(timeCi);
        Element timeComponent = AGDMUtils.createElement(problem, mtUri, "apply");
        Element plus = AGDMUtils.createElement(problem, mtUri, "plus");
        Element cn = AGDMUtils.createElement(problem, mtUri, "cn");
        cn.setTextContent("1");
        timeComponent.appendChild(plus);
        timeComponent.appendChild(timeCi);
        timeComponent.appendChild(cn);
        msup.appendChild(timeComponent);
        //Deploy for all the fields
        NodeList fieldList = AGDMUtils.find(problem, "/*/mms:fields/mms:field|/*/mms:auxiliaryFields/mms:auxiliaryField");
        for (int i = 0; i < fieldList.getLength(); i++) {
            String fieldName = fieldList.item(i).getTextContent();
            //Put the current field
            fieldCi.setTextContent(fieldName);
            
            //Creation of the field element with the problem coordinates deployed for time n + 1
            Element applyRegion = (Element) applyBase.cloneNode(true);
            for (String discCoord: coordMap.getDiscCoords()) {
                Node coordCi = AGDMUtils.createElement(problem, mtUri, "ci");
                coordCi.setTextContent(discCoord);
                applyRegion.appendChild(coordCi.cloneNode(true));
            }
            
            //Replace the original field elements with the new ones in initialConditions
            String query = ".//mt:ci[text() = '" + fieldName + "' and (ancestor::mms:initialConditions or ancestor::mms:finalizationCondition) and not(parent::sml:readFromFile/*[1] = .) "
                    + "and not(ancestor::mms:externalInitialCondition)]";
            NodeList ciFields =  AGDMUtils.find(problem, query);
            for (int j = ciFields.getLength() - 1; j >= 0; j--) {
                Element parent = (Element) ciFields.item(j).getParentNode();
                parent.replaceChild(applyRegion.cloneNode(true), ciFields.item(j));
            }
        }
    }
    
    /**
     * Creates the model characteristic decomposition elements if the models have them.
     * @param result                The problem being discretized
     * @param problem               the xml to be discretized
     * @return                      The characteristic decompositions of the models
     * @throws AGDMException        AGDM005 - Not a valid XML Document 
     *                              AGDM00X - External error
     */
    private Element createModelCharDecomposition(Document result, Document problem) throws AGDMException {
        Element modelCharDecompositions = AGDMUtils.createElement(result, mmsUri, "modelCharacteristicDecompositionsSet");
        NodeList tensorEquivalences = problem.getElementsByTagName("mms:tensorEquivalence");
        NodeList pmi = AGDMUtils.find(problem, "//mms:model");
        for (int i = 0; i < pmi.getLength(); i++) {
            Element mcd = AGDMUtils.createElement(result, mmsUri, "modelCharacteristicDecompositions");
            Element modelName = AGDMUtils.createElement(result, mmsUri, "modelName");
            modelName.setTextContent(pmi.item(i).getFirstChild().getTextContent());
            mcd.appendChild(modelName);
            Element modelId = AGDMUtils.createElement(result, mmsUri, "modelId");
            modelId.setTextContent(pmi.item(i).getFirstChild().getNextSibling().getTextContent());
            mcd.appendChild(modelId);
            //Obtaining the characteristic decomposition
            Document docModel;
            try {
                DocumentManager docMan = new DocumentManagerImpl();
                docModel = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(docMan.getDocument(pmi.item(i).getFirstChild().getNextSibling().getTextContent())));
            } 
            catch (DMException e) {
                throw new AGDMException(AGDMException.AGDM00X, e.toString());
            }
            catch (SUException e) {
                throw new AGDMException(AGDMException.AGDM00X, e.toString());
            }
            AGDMUtils.tensorReplace(problem, docModel, pmi.item(i).getLastChild().getTextContent(), tensorEquivalences, true);
            AGDMUtils.tensorReplace(problem, docModel, pmi.item(i).getLastChild().getTextContent(), tensorEquivalences, false);
            NodeList charDecompositions = AGDMUtils.find(docModel, "//mms:characteristicDecompositions");
            if (charDecompositions.getLength() > 0) {
                //Adding sufix to vector names
                Element charDecomp = (Element) charDecompositions.item(0).cloneNode(true);
                NodeList vectors = AGDMUtils.find(charDecomp, "//mms:eigenVector/mms:name");
                for (int j = 0; j < vectors.getLength(); j++) {
                    String name = vectors.item(j).getTextContent();
                    vectors.item(j).setTextContent(name + "_" + (i + 1));
                    NodeList uses = AGDMUtils.find(charDecomp, ".//mt:ci[text() = '" + name + "']|.//" 
                            + "mms:eigenVector/mms:name[text() = '" + name + "']");
                    for (int k = uses.getLength() - 1; k >= 0; k--) {
                        uses.item(k).setTextContent(uses.item(k).getTextContent() + "_" + (i + 1));
                    }
                }
                vectors = AGDMUtils.find(charDecomp, ".//mms:undiagonalizationCoefficient/mms:eigenVector");
                for (int j = 0; j < vectors.getLength(); j++) {
                    vectors.item(j).setTextContent(vectors.item(j).getTextContent() + "_" + (i + 1));
                }
                mcd.appendChild(result.importNode(charDecomp, true));
            }
            modelCharDecompositions.appendChild(mcd);
        }
        return modelCharDecompositions;
    }
    
    /**
     * Check when the region has to do extrapolation.
     * @param policy    The discretization policy
     * @param incomp    The incompatible regions
     * @return          If there should be extrapolation
     */
    private static boolean hasExtrapolation(DiscretizationPolicy policy, IncompatibilityFields incomp) {
    	if (incomp == null) {
    		return false;
    	}
        boolean compatible = true;
        Iterator<String> it = incomp.getBoundaryFields().keySet().iterator();
        while (it.hasNext() && compatible) {
            String bound = it.next();
            if (!incomp.getBoundaryFields().get(bound).isEmpty()) {
                compatible = false;
            }
        }
        it = incomp.getRegionFields().keySet().iterator();
        while (it.hasNext() && compatible) {
            String seg = it.next();
            if (!incomp.getRegionFields().get(seg).isEmpty()) {
                compatible = false;
            }
        }
        
        return !compatible;
    }
    
    /**
     * Check when the problem has to do extrapolation.
     * @param policy            The discretization policy
     * @param incompRegions    The incompatible regions
     * @return                  If there should be extrapolation
     */
    private static boolean hasExtrapolation(DiscretizationPolicy policy, HashMap<String, IncompatibilityFields> incompRegions) {
        boolean compatible = true;
        Iterator<String> iterator =  incompRegions.keySet().iterator();
        while (iterator.hasNext() && compatible) {
            IncompatibilityFields incomp = incompRegions.get(iterator.next());
            Iterator<String> it = incomp.getBoundaryFields().keySet().iterator();
            while (it.hasNext() && compatible) {
                String bound = it.next();
                if (!incomp.getBoundaryFields().get(bound).isEmpty()) {
                    compatible = false;
                }
            }
            it = incomp.getRegionFields().keySet().iterator();
            while (it.hasNext() && compatible) {
                String seg = it.next();
                if (!incomp.getRegionFields().get(seg).isEmpty()) {
                    compatible = false;
                }
            }
        }
        return !compatible;
    }
  
    /**
    * Creates a PDE discretization policy document from the problem.
    * It reads all the parameters needed to be set for a discretization.
    * @param problem               The problem
    * @return                      The discretization policy
    * @throws AGDMException        AGDM00X  External error
    */
    public String createPDEDiscretizationPolicy(String problem) throws AGDMException {
        String problemString;
        try {
            problemString = SimflownyUtils.jsonToXML(problem);
            Document doc = AGDMUtils.stringToDom(problemString);
            AGDMUtils.schemaValidation(problemString, "PDEProblem.xsd");

            Element root = doc.getDocumentElement();
            AGDMUtils.removeWhitespaceNodes(root);
            
            String problemName = root.getElementsByTagName("mms:name").item(0).getTextContent();
            String problemId = root.getElementsByTagName("mms:id").item(0).getTextContent();

            //Creation of the result document
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document result = builder.newDocument();
   
            //Header
            Element discretizationPolicy = AGDMUtils.createElement(result, mmsUri, "PDEDiscretizationPolicy");
            result.appendChild(discretizationPolicy);
            
            //Header
            Element header = AGDMUtils.createElement(result, mmsUri, "head");
            discretizationPolicy.appendChild(header);
            //Name
            Element name = AGDMUtils.createElement(result, mmsUri, "name");
            header.appendChild(name);
            //Empty id
            Element id = AGDMUtils.createElement(result, mmsUri, "id");
            header.appendChild(id);
            //Author
            Element author = AGDMUtils.createElement(result, mmsUri, "author");
            header.appendChild(author);
            //Version
            Element version = AGDMUtils.createElement(result, mmsUri, "version");
            header.appendChild(version);
            //Date
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            Date actual = new Date();
            Element date = AGDMUtils.createElement(result, mmsUri, "date");
            date.setTextContent(formatter.format(actual));
            header.appendChild(date);
            
            //SimProblem
            Element simProblem = AGDMUtils.createElement(result, mmsUri, "problem");
            discretizationPolicy.appendChild(simProblem);
            Element problemNameE = AGDMUtils.createElement(result, mmsUri, "name");
            problemNameE.setTextContent(problemName);
            simProblem.appendChild(problemNameE);
            Element problemIdE = AGDMUtils.createElement(result, mmsUri, "id");
            problemIdE.setTextContent(problemId);
            simProblem.appendChild(problemIdE);
            
            //Variables
            boolean particles = AGDMUtils.find(doc, "//mms:referenceFrame[text() = 'Lagrangian']").getLength() > 0;
            NodeList fields = AGDMUtils.find(doc, "/*/mms:fields/mms:field|/*/mms:auxiliaryFields/mms:auxiliaryField");
            DocumentFragment vars = result.createDocumentFragment();
            for (int i = 0; i < fields.getLength(); i++) {
                if (particles) {
                    Element var = AGDMUtils.createElement(result, mmsUri, "particleVariable");
                    var.setTextContent(fields.item(i).getTextContent());
                    vars.appendChild(simProblem);
                }
                else {
                    Element var = AGDMUtils.createElement(result, mmsUri, "meshVariable");
                    var.setTextContent(fields.item(i).getTextContent());
                    vars.appendChild(var);
                }
            }
            if (particles) {
                Element parts = AGDMUtils.createElement(result, mmsUri, "particleVariables");
                discretizationPolicy.appendChild(parts);
                Element species = AGDMUtils.createElement(result, mmsUri, "species");
                parts.appendChild(species);
                Element speciesName = AGDMUtils.createElement(result, mmsUri, "name");
                species.appendChild(speciesName);
                species.appendChild(vars);
                Element volume = AGDMUtils.createElement(result, mmsUri, "volume");
                species.appendChild(volume);
            }
            else {
                Element mesh = AGDMUtils.createElement(result, mmsUri, "meshVariables");
                mesh.appendChild(vars);
                discretizationPolicy.appendChild(mesh);
            }
            
            //Discretizations
            Element regionDiscretizationsE = AGDMUtils.createElement(result, mmsUri, "regionDiscretizations");
            discretizationPolicy.appendChild(regionDiscretizationsE);
            NodeList regions = AGDMUtils.find(doc, "//mms:region/mms:name|//mms:subregion/mms:name");
            for (int i = 0; i < regions.getLength(); i++) {
                Element region = (Element) regions.item(i);
                Element regionDiscretization = AGDMUtils.createElement(result, mmsUri, "regionDiscretization");
                regionDiscretizationsE.appendChild(regionDiscretization);
                Element regionE = AGDMUtils.createElement(result, mmsUri, "region");
                regionE.setTextContent(region.getTextContent());
                regionDiscretization.appendChild(regionE);
                //Operators
                Element operatorDiscretizations = AGDMUtils.createElement(result, mmsUri, "operatorDiscretizations");
                regionDiscretization.appendChild(operatorDiscretizations);
                ArrayList<String> operators = getRegionOperators(doc, region.getTextContent());
                for (int j = 0; j < operators.size(); j++) {
                    Element operatorDiscretization = AGDMUtils.createElement(result, mmsUri, "operatorDiscretization");
                    operatorDiscretizations.appendChild(operatorDiscretization);
                    Element operatorName = AGDMUtils.createElement(result, mmsUri, "operatorName");
                    operatorName.setTextContent(operators.get(j));
                    operatorDiscretization.appendChild(operatorName);
                    Element operatorPolicy = AGDMUtils.createElement(result, mmsUri, "operatorPolicy");
                    operatorDiscretization.appendChild(operatorPolicy);
                    Element description = AGDMUtils.createElement(result, mmsUri, "description");
                    operatorPolicy.appendChild(description);
                    Element operatorPolicyId = AGDMUtils.createElement(result, mmsUri, "id");
                    operatorPolicy.appendChild(operatorPolicyId);
                }
                //Time integration
                Element timeIntegrationSchema = AGDMUtils.createElement(result, mmsUri, "timeIntegrationSchema");
                regionDiscretization.appendChild(timeIntegrationSchema);
                Element timeIntegrationSchemaName = AGDMUtils.createElement(result, mmsUri, "name");
                timeIntegrationSchema.appendChild(timeIntegrationSchemaName);
                Element operatorPolicyId = AGDMUtils.createElement(result, mmsUri, "id");
                timeIntegrationSchema.appendChild(operatorPolicyId);
            }
            //Analysis
            NodeList analysis = AGDMUtils.find(doc, "//mms:analysisFieldEquations");
            if (analysis.getLength() > 0) {
            	Element analysisInfo = AGDMUtils.createElement(result, mmsUri, "analysisDiscretization");
            	NodeList operators = AGDMUtils.find(doc, "//mms:analysisFieldEquation/mms:operator/mms:name|//mms:auxiliaryAnalysisVariableEquation/mms:operator/mms:name");
            	Set<String> added = new HashSet<String>();
                for (int j = 0; j < operators.getLength(); j++) {
                	if (!added.contains(operators.item(j).getTextContent())) {
                		added.add(operators.item(j).getTextContent());
                        Element operatorDiscretization = AGDMUtils.createElement(result, mmsUri, "operatorDiscretization");
                        analysisInfo.appendChild(operatorDiscretization);
                        Element operatorName = AGDMUtils.createElement(result, mmsUri, "operatorName");
                        operatorName.setTextContent(operators.item(j).getTextContent());
                        operatorDiscretization.appendChild(operatorName);
                        Element operatorPolicy = AGDMUtils.createElement(result, mmsUri, "operatorPolicy");
                        operatorDiscretization.appendChild(operatorPolicy);
                        Element description = AGDMUtils.createElement(result, mmsUri, "description");
                        operatorPolicy.appendChild(description);
                        Element operatorPolicyId = AGDMUtils.createElement(result, mmsUri, "id");
                        operatorPolicy.appendChild(operatorPolicyId);
                	}
                }
            	Element inputVariables = AGDMUtils.createElement(result, mmsUri, "inputVariables");
            	Element inputVariable = AGDMUtils.createElement(result, mmsUri, "inputVariable");
            	Element math = AGDMUtils.createElement(result, mtUri, "math");
                inputVariables.appendChild(inputVariable);
                inputVariable.appendChild(math);
                analysisInfo.appendChild(inputVariables);
            }
            
            
            return SimflownyUtils.xmlToJSON(AGDMUtils.domToString(result), false);
        } 
        catch (Exception e) {
            e.printStackTrace(System.out);
            throw new AGDMException(AGDMException.AGDM00X, e.getMessage());
        }
    }

    /**
     * Returns the operators from problem region's models.
     * @param problem           The problem
     * @param regionName        The region name
     * @return                  The operators
     * @throws AGDMException    AGDM00X  External error
     */
    private ArrayList<String> getRegionOperators(Document problem, String regionName) throws AGDMException {
        ArrayList<String> operatorNames = new ArrayList<String>();
        ArrayList<String> modelsLoaded = new ArrayList<String>();
        try {
            NodeList models = AGDMUtils.find(problem, 
                    "//mms:region[mms:name = '" + regionName + "']//mms:interiorModel|//mms:subregion[mms:name = '" + regionName + "']"
                  + "//mms:interiorModel|//mms:subregion[mms:name = '" + regionName + "']//mms:surfaceModel");
            for (int i = 0; i < models.getLength(); i++) {
                String modelName = models.item(i).getTextContent();
                if (!modelsLoaded.contains(modelName)) {
                    //Load model
                    DocumentManager docMan = new DocumentManagerImpl();
                    Document model = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(docMan.getDocument(AGDMUtils.find(problem, 
                            "//mms:model[mms:name = '" + modelName + "']/mms:id").item(0).getTextContent())));
                    NodeList operators = AGDMUtils.find(model, "//mms:operator/mms:name");
                    for (int j = 0; j < operators.getLength(); j++) {
                        if (!operatorNames.contains(operators.item(j).getTextContent())) {
                            operatorNames.add(operators.item(j).getTextContent());
                        }
                    }
                }
            }
        }
        catch (Exception e) {
            throw new AGDMException(AGDMException.AGDM00X, e.getMessage());
        }
        return operatorNames;
    }
    
    /**
     * Generates the analysis instructions.
     * @param problem       	The continuous problem
     * @param result   			The destination problem
     * @param ci            	The coordinate information
     * @param policy        	The discretization policy
     * @param sc				Schema creator
     * @param referenceFrames	Reference frames for fields
     * @throws AGDMException 	AGDM007  Invalid discretization policy
     * 							AGDM00X  External error
     */
    private void processAnalysis(Document problem, Document result, CoordinateInfo ci, DiscretizationPolicy policy
    		, SchemaCreator sc, HashMap<String, ReferenceFrameType> referenceFrames, ConstantInformation constants) throws AGDMException {
        //Analysis
        NodeList analysisEquations = AGDMUtils.find(problem, "//mms:analysisFieldEquations|//mms:auxiliaryAnalysisVariableEquations");
        if (analysisEquations.getLength() > 0) {
            Node scope = AGDMUtils.createElement(problem, mmsUri, "analysis");
            for (int i = 0; i < analysisEquations.getLength(); i++) {
                scope.appendChild(analysisEquations.item(i).cloneNode(true));
            }
            if (policy.getAnalysisDiscretization() == null) {
            	throw new AGDMException(AGDMException.AGDM007, 
            			"The problem has analysis fields, but the discretization policy does not has a discretization for them. "
            			+ "Please, add an Analysis Discretization to the discretization policy.");
            }
            //Get the operators information
            OperatorsInfo opInfo = new OperatorsInfo(problem, scope, policy, policy.getAnalysisDiscretization(), policy.getMeshFields(), DiscretizationType.mesh);
            //Get the process information
            ProcessInfo pi = createAnalysisProcessInfo(problem, result, ci, policy, opInfo, sc, referenceFrames, constants);
            //Create the instructions
            Element analysis = sc.createAnalysis(result, opInfo, pi, policy.getAnalysisInputVariables());
            if (analysis != null) {
                Element analysisElement = AGDMUtils.createElement(result, mmsUri, "analysis");
                analysisElement.appendChild(result.importNode(analysis, true));
                
                //Add discretization information
                analysisElement.appendChild(createPDEAnalysisDiscretizationInfo(result, policy.getAnalysisDiscretization()));
                
                Element boundaries = (Element) result.getElementsByTagName("mms:boundaryConditions").item(0);
                boundaries.getParentNode().insertBefore(analysisElement, boundaries);
            }
        }
    }
    
    /**
     * Process discretization policy.
     * @param problem			Problem
     * @param policy			Policy
     * @param coordMap			Coordinate relation
     * @throws AGDMException	AGDM00X  External error
     */
    public void processDiscretizationPolicy(Document problem, DiscretizationPolicy policy, CoordinateInfo coordMap) throws AGDMException {
    	if (policy.getPostInitialization() != null) {
    		Document owner = policy.getPostInitialization().getOwnerDocument();
    		ArrayList<String> fields = new ArrayList<String>();
        	//1 - Add currentCell to fields in volume and initial conditions
	    	//Time coordinate
	        String timeCoordinate = coordMap.getDiscTimeCoord();
	        //Base deployment
	        Element newNode = AGDMUtils.createElement(owner, mtUri, "apply");
	        Element parentCi = AGDMUtils.createElement(owner, mtUri, "ci");
	        newNode.appendChild(parentCi);
	        Element msup = AGDMUtils.createElement(owner, mtUri, "msup");
	        parentCi.appendChild(msup);
	        Element fieldCi = AGDMUtils.createElement(owner, mtUri, "ci");
	        msup.appendChild(fieldCi);
	        Element timeCi = AGDMUtils.createElement(owner, mtUri, "ci");
	        timeCi.setTextContent("(" + timeCoordinate + ")");
	        msup.appendChild(timeCi);
	        Element timeComponent = AGDMUtils.createElement(owner, mtUri, "apply");
	        Element plus = AGDMUtils.createElement(owner, mtUri, "plus");
	        Element cn = AGDMUtils.createElement(owner, mtUri, "cn");
	        cn.setTextContent("1");
	        timeComponent.appendChild(plus);
	        timeComponent.appendChild(timeCi);
	        timeComponent.appendChild(cn);
	        msup.appendChild(timeComponent);
	        Element currentCell = AGDMUtils.createElement(owner, smlUri, "currentCell");
	        newNode.appendChild(currentCell);
	        //Deploy for all the fields
	        NodeList fieldList = AGDMUtils.find(problem, "/*/mms:fields/mms:field|/*/mms:auxiliaryFields/mms:auxiliaryField");
	        for (int i = 0; i < fieldList.getLength(); i++) {
	            String fieldName = fieldList.item(i).getTextContent();
	            fields.add(fieldName);
	            //Set the current field
	            fieldCi.setTextContent(fieldName);
	    		Element oldNode = AGDMUtils.createElement(owner, mtUri, "ci");
	    		oldNode.setTextContent(fieldName);
	            //Replace the original field elements with the new ones in initialConditions
				AGDMUtils.nodeReplace(policy.getPostInitialization(), oldNode, newNode.cloneNode(true));
	        }
	    	//2 - Add currentCell to auxiliary initial conditions variables used outside initial conditions
	        newNode = AGDMUtils.createElement(owner, mtUri, "apply");
	        fieldCi = AGDMUtils.createElement(owner, mtUri, "ci");
	        newNode.appendChild(fieldCi);
	        currentCell = AGDMUtils.createElement(owner, smlUri, "currentCell");
	        newNode.appendChild(currentCell);
	    	Set<String> vars = new HashSet<String>(AGDMUtils.getAllAssignedVarsString(policy.getPostInitialization()));
	    	for (String var: vars) {
	    		Element oldNode = AGDMUtils.createElement(owner, mtUri, "ci");
	    		oldNode.setTextContent(var);
	            //Set the current variable
	            fieldCi.setTextContent(var);
	    		if (!fields.contains(var)) {
	    			AGDMUtils.nodeReplace(policy.getPostInitialization(), oldNode, newNode.cloneNode(true));
	    			for (Entry<String, Species> entry : policy.getParticleSpecies().entrySet()) {
		    			if (entry.getValue().getParticleVolume() != null) {
		    				AGDMUtils.nodeReplace(entry.getValue().getParticleVolume(), oldNode, newNode.cloneNode(true));
		    			}
		    			for (Element v : entry.getValue().getParticleVelocities().values()) {
		    				AGDMUtils.nodeReplace(v, oldNode, newNode.cloneNode(true));
		    			}
	    			}
	    		}
	    	}
    	}    	
    }
    
    /**
     * Gets the reference frame for all the evolution fields in the problem.
     * @param problem			Problem
     * @return					Reference frame
     * @throws AGDMException	AGDM008  Invalid simulation problem
     * 							AGDM00X  External error
     */
    static HashMap<String, ReferenceFrameType> getFieldReferenceFrame(Document problem) throws AGDMException {
    	HashMap<String, ReferenceFrameType> referenceFrames = new HashMap<String, ReferenceFrameType>();
    	NodeList equationFields = AGDMUtils.find(problem, "//mms:evolutionEquation");
    	for (int i = 0; i < equationFields.getLength(); i++) {
    		String field = ((Element) equationFields.item(i)).getElementsByTagNameNS(mmsUri, "field").item(0).getTextContent();
    		String referenceFrame = ((Element) equationFields.item(i)).getElementsByTagNameNS(mmsUri, "referenceFrame").item(0).getTextContent().toLowerCase();
    		if (referenceFrames.containsKey(field) && ReferenceFrameType.valueOf(referenceFrame) != referenceFrames.get(field)) {
    			throw new AGDMException(AGDMException.AGDM008, "Field " + field + " from two different models have different reference frame.");
    		}
    		try {
    			referenceFrames.put(field, ReferenceFrameType.valueOf(referenceFrame));
    		}
    		catch (IllegalArgumentException e) {
    			throw new AGDMException(AGDMException.AGDM008, "Field " + field + " does not hava a valid reference frame.");
    		}
    	}
    	return referenceFrames;
    }
}
