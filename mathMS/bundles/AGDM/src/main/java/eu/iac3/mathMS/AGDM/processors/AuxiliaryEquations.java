/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.AGDM.processors;

import eu.iac3.mathMS.AGDM.AGDMException;
import eu.iac3.mathMS.AGDM.utils.AGDMUtils;
import eu.iac3.mathMS.AGDM.utils.RuleInfo;
import eu.iac3.mathMS.AGDM.utils.PDE.OperatorsInfo;
import eu.iac3.mathMS.AGDM.utils.PDE.ProcessInfo;

import java.util.ArrayList;
import java.util.HashSet;

import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * This class have all the necessary methods to process the auxiliar equations.
 * @author bminyano
 */
public class AuxiliaryEquations {
    
    static String mtUri = "http://www.w3.org/1998/Math/MathML";
    
    /**
     * Constructor.
     */
    public AuxiliaryEquations() {
    }
    
    /**
     * Process the tags auxiliaryEquations.
     * @param discProblem       The problem being discretized
     * @param problem           The original problem
     * @param variable          The element variable
     * @param pi                The process information
     * @return                  The equations
     * @throws AGDMException    AGDM007 External error
     */
    public static DocumentFragment processAuxiliaryEquations(Document discProblem, Node problem, Element variable, ProcessInfo pi) 
        throws AGDMException {
        //The result are stored in a document fragment
        DocumentFragment result = discProblem.createDocumentFragment();

        //If there is an active filter
        ArrayList<String> fieldIt = new ArrayList<String>();
        String filteredFieldsQuery = "";
        if (pi.getRegionInfo().isFiltered() && pi.getRegionInfo().getFields().size() > 0) {
        	fieldIt = pi.getRegionInfo().getFields();
        	filteredFieldsQuery = "[";
            for (int k = 0; k < fieldIt.size(); k++) {
            	filteredFieldsQuery = filteredFieldsQuery + "mms:auxiliaryField = '" + fieldIt.get(k) + "' or ";
            }
        	filteredFieldsQuery = filteredFieldsQuery.substring(0, filteredFieldsQuery.lastIndexOf(" or")) + "]";
        }

        //Apply definitions
        if (!pi.getRegionInfo().isFiltered() || pi.getRegionInfo().getFields().size() > 0) {
        	boolean isStepVariable = pi.isStepVariable(variable.getFirstChild(), false);
            NodeList spatialIteration = AGDMUtils.find(variable, ExecutionFlow.SPATIAL_IT_TAGS);
            ProcessInfo newPi = new ProcessInfo(pi);
            if (spatialIteration.getLength() > 0) {
                ArrayList<String> coords = pi.getBaseSpatialCoordinates();
                for (int j = 0; j < coords.size(); j++) {
                	newPi.setSpatialCoord1(coords.get(j));
                    //Auxiliary variables
                 	OperatorsInfo opInfoFluxes = new OperatorsInfo(pi.getOperatorsInfo());
                 	opInfoFluxes.onlyDependent(getAuxiliaryVariables(problem, "//mms:auxiliaryFieldEquation" + filteredFieldsQuery), true, opInfoFluxes.getAuxiliaryFieldAlgorithmCommonConditional(fieldIt));
                 	result.appendChild(discProblem.importNode(SchemaCreator.createAuxiliaryVariableEquations(discProblem, problem, opInfoFluxes, pi.getOperatorsInfo().getDerivativeLevels(), pi.getOperatorsInfo().getMultiplicationLevels(), opInfoFluxes.getDerivativeLevels(), opInfoFluxes.getMultiplicationLevels(), newPi, (Element) variable.getFirstChild().cloneNode(true), 0, false), true));
                 	
                	//Auxiliary equations
        	        NodeList equation = AGDMUtils.find(problem, "//mms:auxiliaryFieldEquation" + filteredFieldsQuery);
        	        for (int i = 0; i < equation.getLength(); i++) {
        	            NodeList algorithmEl = ((Element) equation.item(i)).getElementsByTagName("sml:simml");
        	            NodeList algebraicEl = ((Element) equation.item(i)).getElementsByTagName("mms:algebraic");
        	            NodeList conditionalEl = ((Element) equation.item(i)).getElementsByTagName("mms:evolutionCondition");
        	            Node conditional = null;
        	            if (conditionalEl.getLength() > 0) {
        	            	conditional = conditionalEl.item(0).getFirstChild().cloneNode(true);
        	            }
        	            if (algorithmEl.getLength() > 0 && algebraicEl.getLength() > 0) {
        	            	throw new AGDMException(AGDMException.AGDM005, 
        	            			"Auxiliary field equations cannot be defined with both algorithm and algebraic terms.");
        	            }
        	            Element definition = AGDMUtils.createElement(discProblem, AGDMUtils.simmlUri, "simml");
        	            //Algorithm
        	            if (algorithmEl.getLength() > 0) {
        	            	definition.appendChild(ExecutionFlow.processSimml((Element) algorithmEl.item(0).cloneNode(true), discProblem, problem, pi, true));
        	            	//definition = (Element) algorithmEl.item(0).cloneNode(true);
        	            }
        	            if (algebraicEl.getLength() > 0) {
        		            NodeList operators = ((Element) algebraicEl.item(0)).getElementsByTagName("mms:operator");
        		            Node math = algebraicEl.item(0).getFirstChild();
        		            if (operators.getLength() > 0) {
        	                	throw new AGDMException(AGDMException.AGDM006, 
        	                			"Prestep cannot discretize auxiliary equations with derivative terms.");
        		            }
        		            //Mathematical expression
        		            if (math.getLocalName().equals("math")) {
        		            	String field = ((Element) equation.item(i)).getElementsByTagName("mms:auxiliaryField").item(0).getTextContent();
        		               	Element Elem = AGDMUtils.assignVariable(field, math.getFirstChild().cloneNode(true).cloneNode(true));
        		               	definition.appendChild(discProblem.importNode(Elem, true));
        		            }
        	            }
        	            result.appendChild(processAuxiliaryEquation(discProblem, definition, variable, newPi, isStepVariable, conditional));
        	        }
                }
            }
            else {
                //Auxiliary variables
            	OperatorsInfo opInfoFluxes = new OperatorsInfo(pi.getOperatorsInfo());
            	opInfoFluxes.onlyDependent(getAuxiliaryVariables(problem, "//mms:auxiliaryFieldEquation" + filteredFieldsQuery), true, opInfoFluxes.getAuxiliaryFieldAlgorithmCommonConditional(fieldIt));
            	result.appendChild(discProblem.importNode(SchemaCreator.createAuxiliaryVariableEquations(discProblem, problem, opInfoFluxes, pi.getOperatorsInfo().getDerivativeLevels(), pi.getOperatorsInfo().getMultiplicationLevels(), opInfoFluxes.getDerivativeLevels(), opInfoFluxes.getMultiplicationLevels(), newPi, (Element) variable.getFirstChild().cloneNode(true), 0, false), true));
            	
            	//Auxiliary equations
    	        NodeList equation = AGDMUtils.find(problem, "//mms:auxiliaryFieldEquation" + filteredFieldsQuery);
    	        for (int i = 0; i < equation.getLength(); i++) {
    	            NodeList algorithmEl = ((Element) equation.item(i)).getElementsByTagName("sml:simml");
    	            NodeList algebraicEl = ((Element) equation.item(i)).getElementsByTagName("mms:algebraic");
    	            NodeList conditionalEl = ((Element) equation.item(i)).getElementsByTagName("mms:evolutionCondition");
    	            Node conditional = null;
    	            if (conditionalEl.getLength() > 0) {
    	            	conditional = conditionalEl.item(0).getFirstChild().cloneNode(true);
    	            }
    	            if (algorithmEl.getLength() > 0 && algebraicEl.getLength() > 0) {
    	            	throw new AGDMException(AGDMException.AGDM005, 
    	            			"Auxiliary field equations cannot be defined with both algorithm and algebraic terms.");
    	            }
    	            Element definition = AGDMUtils.createElement(discProblem, AGDMUtils.simmlUri, "simml");
    	            //Algorithm
    	            if (algorithmEl.getLength() > 0) {
    	            	//definition = (Element) algorithmEl.item(0).cloneNode(true);
    	            	definition.appendChild(ExecutionFlow.processSimml((Element) algorithmEl.item(0).cloneNode(true), discProblem, problem, pi, true));
    	            }
    	            if (algebraicEl.getLength() > 0) {
    		            NodeList operators = ((Element) algebraicEl.item(0)).getElementsByTagName("mms:operator");
    		            Node math = algebraicEl.item(0).getFirstChild();
    		            if (operators.getLength() > 0) {
    	                	throw new AGDMException(AGDMException.AGDM006, 
    	                			"Prestep cannot discretize auxiliary equations with derivative terms.");
    		            }
    		            //Mathematical expression
    		            if (math.getLocalName().equals("math")) {
    		            	String field = ((Element) equation.item(i)).getElementsByTagName("mms:auxiliaryField").item(0).getTextContent();
    		               	Element Elem = AGDMUtils.assignVariable(field, math.getFirstChild().cloneNode(true).cloneNode(true));
    		               	definition.appendChild(discProblem.importNode(Elem, true));
    		            }
    	            }
    	            result.appendChild(processAuxiliaryEquation(discProblem, definition, variable, newPi, isStepVariable, conditional));
    	        }
	        }
        }
        return result;
    }
    
    /**
     * Process the tags processFieldFromAuxiliaryEquations.
     * @param discProblem       The problem being discretized
     * @param problem           The original problem
     * @param variable          The element variable
     * @param ri                The rule information
     * @return                  The equations
     * @throws AGDMException    AGDM00X External error
     */
    public static DocumentFragment processFieldRecoveries(Document discProblem, Node problem, Element variable, RuleInfo ri) 
        throws AGDMException {
        //The result are stored in a document fragment
        DocumentFragment result = discProblem.createDocumentFragment();

    	boolean isStepVariable = ri.getProcessInfo().isStepVariable(variable.getFirstChild(), false);
        NodeList spatialIteration = AGDMUtils.find(variable, ExecutionFlow.SPATIAL_IT_TAGS);
        ProcessInfo newPi = new ProcessInfo(ri.getProcessInfo());
        if (spatialIteration.getLength() > 0) {
        	if (ri.getFunction() || ri.getIndividualCoord()) {
            	OperatorsInfo opInfoFluxes = new OperatorsInfo(ri.getProcessInfo().getOperatorsInfo());
            	opInfoFluxes.onlyDependent(getFieldRecoveryVariables(problem), true, null);
            	result.appendChild(discProblem.importNode(SchemaCreator.createAuxiliaryVariableEquations(discProblem, problem, opInfoFluxes, ri.getProcessInfo().getOperatorsInfo().getDerivativeLevels(), ri.getProcessInfo().getOperatorsInfo().getMultiplicationLevels(), opInfoFluxes.getDerivativeLevels(), opInfoFluxes.getMultiplicationLevels(), ri.getProcessInfo(), (Element) variable.getFirstChild().cloneNode(true), 0, false), true));
            	//Field recovery
                NodeList equation = AGDMUtils.find(problem, "//mms:fieldRecovery");
                for (int i = 0; i < equation.getLength(); i++) {
                	Element definition = (Element) ((Element) equation.item(i)).getElementsByTagName("sml:simml").item(0).cloneNode(true);
                	result.appendChild(processFieldRecovery(discProblem, definition, variable, ri.getProcessInfo(), isStepVariable));
                }
        	}
        	else {
                ArrayList<String> coords = ri.getProcessInfo().getBaseSpatialCoordinates();
                for (int j = 0; j < coords.size(); j++) {
                    newPi.setSpatialCoord1(coords.get(j));
                    //Auxiliary variables
                	OperatorsInfo opInfoFluxes = new OperatorsInfo(ri.getProcessInfo().getOperatorsInfo());
                	opInfoFluxes.onlyDependent(getFieldRecoveryVariables(problem), true, null);
                	result.appendChild(discProblem.importNode(SchemaCreator.createAuxiliaryVariableEquations(discProblem, problem, opInfoFluxes, ri.getProcessInfo().getOperatorsInfo().getDerivativeLevels(), ri.getProcessInfo().getOperatorsInfo().getMultiplicationLevels(), opInfoFluxes.getDerivativeLevels(), opInfoFluxes.getMultiplicationLevels(), newPi, (Element) variable.getFirstChild().cloneNode(true), 0, false), true));

                	//Field recovery
                    NodeList equation = AGDMUtils.find(problem, "//mms:fieldRecovery");
                    for (int i = 0; i < equation.getLength(); i++) {
                    	Element definition = (Element) ((Element) equation.item(i)).getElementsByTagName("sml:simml").item(0).cloneNode(true);
                    	result.appendChild(processFieldRecovery(discProblem, definition, variable, newPi, isStepVariable));
                    }
                }
        	}
        }
        else {
            //Auxiliary variables
        	OperatorsInfo opInfoFluxes = new OperatorsInfo(ri.getProcessInfo().getOperatorsInfo());
        	opInfoFluxes.onlyDependent(getFieldRecoveryVariables(problem), true, null);
        	result.appendChild(discProblem.importNode(SchemaCreator.createAuxiliaryVariableEquations(discProblem, problem, opInfoFluxes, ri.getProcessInfo().getOperatorsInfo().getDerivativeLevels(), ri.getProcessInfo().getOperatorsInfo().getMultiplicationLevels(), opInfoFluxes.getDerivativeLevels(), opInfoFluxes.getMultiplicationLevels(), newPi, (Element) variable.getFirstChild().cloneNode(true), 0, false), true));
        	
        	//Field recovery
            NodeList equation = AGDMUtils.find(problem, "//mms:fieldRecovery");
            for (int i = 0; i < equation.getLength(); i++) {
            	Element definition = (Element) ((Element) equation.item(i)).getElementsByTagName("sml:simml").item(0).cloneNode(true);
            	result.appendChild(processFieldRecovery(discProblem, definition, variable, newPi, isStepVariable));
            }
        }
        
        return result;
    }
    
    /**
     * Process a field recovery.
     * @param discProblem		The document
     * @param definition		The field recovery definition
     * @param variable			Variable template
     * @param pi				Process info
     * @param isStepVariable	If the template is a substep variable
     * @return					The code
     * @throws AGDMException	AGDM00X External error
     */
    public static DocumentFragment processFieldRecovery(Document discProblem, Node definition, Element variable, ProcessInfo pi, boolean isStepVariable) throws AGDMException  {
        DocumentFragment result = discProblem.createDocumentFragment();
        Element simml = (Element) definition.cloneNode(true);  
        AGDMUtils.replaceFieldVariables(pi, (Element) variable.getFirstChild(), simml);
        NodeList instructions = simml.getChildNodes();
        for (int k = 0; k < instructions.getLength(); k++) {
        	result.appendChild(discProblem.importNode(instructions.item(k), true));
        }
        return result;
    }

    /**
     * Process an auxiliary equation.
     * @param discProblem		The document
     * @param definition		The field recovery definition
     * @param variable			Variable template
     * @param pi				Process info
     * @param isStepVariable	If the template is a substep variable
     * @return					The code
     * @throws AGDMException	AGDM00X External error
     */
    public static DocumentFragment processAuxiliaryEquation(Document discProblem, Node definition, Element variable, ProcessInfo pi, boolean isStepVariable, Node conditional) throws AGDMException  {
        DocumentFragment result = discProblem.createDocumentFragment();
	    Element simml = (Element) definition.cloneNode(true);     
	    
        //Create the equation with the time slice
        DocumentFragment simmlInstr = simml.getOwnerDocument().createDocumentFragment();
        for (int l = 0; l < simml.getChildNodes().getLength(); l++) {
        	simmlInstr.appendChild(simml.getChildNodes().item(l).cloneNode(true));
        }
        Node tmp = SchemaCreator.applyConditionalIfExists(simmlInstr, conditional);
        simml = (Element)definition.cloneNode(false);
        simml.appendChild(tmp);
        AGDMUtils.replaceFieldVariables(pi, (Element) variable.getFirstChild(), simml);
	    NodeList instructions = simml.getChildNodes();
	    for (int k = 0; k < instructions.getLength(); k++) {
	    	result.appendChild(discProblem.importNode(instructions.item(k), true));
	    } 
	    return result;
    }
    
    /**
     * Get variables used in field recovery.
     * @param problem			The problem with recovery information
     * @return					The variables used.
     * @throws AGDMException	AGDM00X  External error
     */
    public static HashSet<String> getFieldRecoveryVariables(Node problem) throws AGDMException {
    	HashSet<String> variables = new HashSet<String>();
        NodeList vars = AGDMUtils.find(problem, "//mms:fieldRecovery//mt:ci");
        for (int i = 0; i < vars.getLength(); i++) {
        	variables.add(vars.item(i).getTextContent());
        }
        return variables;
    }
    
    /**
     * Get variables used in auxiliary equations.
     * @param problem			The problem with recovery information
     * @param query				The auxiliaryField query
     * @return					The variables used.
     * @throws AGDMException	AGDM00X  External error
     */
    public static HashSet<String> getAuxiliaryVariables(Node problem, String query) throws AGDMException {
    	HashSet<String> variables = new HashSet<String>();
        NodeList vars = AGDMUtils.find(problem, query + "//mt:ci");
        for (int i = 0; i < vars.getLength(); i++) {
        	variables.add(vars.item(i).getTextContent());
        }
        return variables;
    }
}
