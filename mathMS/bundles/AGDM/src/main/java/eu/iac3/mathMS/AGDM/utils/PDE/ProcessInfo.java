/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.AGDM.utils.PDE;

import eu.iac3.mathMS.AGDM.AGDMException;
import eu.iac3.mathMS.AGDM.ExtrapolationType;
import eu.iac3.mathMS.AGDM.processors.ExecutionFlow;
import eu.iac3.mathMS.AGDM.processors.SchemaCreator;
import eu.iac3.mathMS.AGDM.utils.AGDMUtils;
import eu.iac3.mathMS.AGDM.utils.ConstantInformation;
import eu.iac3.mathMS.AGDM.utils.CoordinateInfo;
import eu.iac3.mathMS.AGDM.utils.DiscretizationType;
import eu.iac3.mathMS.AGDM.utils.IncompatibilityFields;
import eu.iac3.mathMS.AGDM.utils.ReferenceFrameType;
import eu.iac3.mathMS.AGDM.utils.RegionInfo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


/**
 * The information needed to process an equation.
 * @author bminyano
 *
 */
public class ProcessInfo {

    private CoordinateInfo coordinateRelation;
    private RegionInfo regionInfo;
    private ArrayList<String> baseSpatialCoordinates;
    private Element equation;
    private Element charDecomposition;
    private ArrayList<String> eigenVectors;
    private String currentField;
    private String currentSpecies;
    private String vector;
    private String regionName;
    private ExtrapolationType extrapolationMethod;
    private String spatialCoord1;
    private String spatialCoord2;
    private ArrayList<Tensor> tensors;
    private IncompatibilityFields incomFields;
    private Document completeProblem;
    private boolean extrapolation;
    private String modelId;
    private boolean isTimeInterpolator;
    private boolean hasFluxes;
    private static int numberOfRegions;
    private ArrayList<Element> stepVariables = new ArrayList<Element>();
    private ArrayList<Element> stiffVariables = new ArrayList<Element>();
    private Set<String> auxiliaryVariables;
    private OperatorsInfo operatorsInfoMesh = null;
    private HashMap<String, OperatorsInfo> operatorsInfoParticles;
    private SchemaCreator schemaCreator;
    private DiscretizationType discretizationType;
    private ArrayList<String> meshFields;
    private HashMap<String, Species> particleSpecies;
    private Node currentTimeStep;
    private int currentTimeStepIndex;
    private int compactSupportRatio = 1;
    private Element kernel = null;
    private Element kernelGradient = null;
    private HashMap<String, ReferenceFrameType> referenceFrame;
    private Node postInitialization;
    private ConstantInformation constantInformation;
    private HashMap<Integer, HashMap<String, Element>> stepFieldTemplates;
    
    /**
     * Constructor.
     */
    public ProcessInfo() {
    }
   
    /**
     * Copy constructor.
     * @param pi    The ProcessInfo to copy
     */
    public ProcessInfo(ProcessInfo pi) {
        this.coordinateRelation = pi.coordinateRelation;
        this.regionInfo = new RegionInfo(pi.regionInfo);
        if (pi.equation != null) {
            this.equation = (Element) pi.equation.cloneNode(true);
        }
        if (pi.charDecomposition != null) {
            this.charDecomposition = (Element) pi.charDecomposition.cloneNode(true);
            this.eigenVectors = calculateVectors();
        }
        this.currentField = pi.currentField;
        this.vector = pi.vector;
        this.regionName = pi.regionName;
        this.baseSpatialCoordinates = new ArrayList<String>(pi.baseSpatialCoordinates);
        this.spatialCoord1 = pi.getSpatialCoord1();
        this.spatialCoord2 = pi.getSpatialCoord2();
        tensors = new ArrayList<Tensor>();
        if (pi.tensors != null) {
        	tensors.addAll(pi.tensors);
        }
        this.incomFields = pi.incomFields;
        this.extrapolationMethod = pi.extrapolationMethod;
        this.completeProblem = pi.completeProblem;
        this.extrapolation = pi.extrapolation;
        this.modelId = pi.modelId;
        this.isTimeInterpolator = pi.isTimeInterpolator;
        this.hasFluxes = pi.hasFluxes;
        this.stepVariables = new ArrayList<Element>();
        if (pi.stepVariables != null) {
        	this.stepVariables.addAll(pi.stepVariables);
        }
        this.stiffVariables = new ArrayList<Element>();
        if (pi.stiffVariables != null) {
        	this.stiffVariables.addAll(pi.stiffVariables);
        }
        this.auxiliaryVariables = new LinkedHashSet<String>();
        if (pi.auxiliaryVariables != null) {
        	this.auxiliaryVariables.addAll(pi.auxiliaryVariables);
        }
        if (pi.operatorsInfoMesh != null) {
        	this.operatorsInfoMesh = new OperatorsInfo(pi.operatorsInfoMesh);
        }
        if (pi.operatorsInfoParticles != null) {
        	this.operatorsInfoParticles = new HashMap<String, OperatorsInfo>(pi.operatorsInfoParticles);
        }
        if (pi.particleSpecies != null) {
        	this.particleSpecies = (HashMap<String, Species>) pi.particleSpecies.clone();
        }
        this.schemaCreator = pi.schemaCreator;
        this.discretizationType = pi.discretizationType;
        
        this.meshFields = new ArrayList<String>();
        if (pi.meshFields != null) {
        	this.meshFields.addAll(pi.meshFields);
        }
        if (pi.currentTimeStep != null) {
        	this.currentTimeStep = pi.currentTimeStep.cloneNode(true);
        }
        this.currentTimeStepIndex = pi.currentTimeStepIndex;
        this.compactSupportRatio = pi.compactSupportRatio;
        if (pi.kernel != null) {
        	this.kernel = (Element) pi.kernel.cloneNode(true);
        }
        if (pi.kernelGradient != null) {
        	this.kernelGradient = (Element) pi.kernelGradient.cloneNode(true);
        }
        this.referenceFrame = new HashMap<String, ReferenceFrameType>(pi.referenceFrame);
        if (pi.postInitialization != null) {
        	this.postInitialization = pi.postInitialization.cloneNode(true);
        }
        this.currentSpecies = pi.currentSpecies;
        this.constantInformation = pi.constantInformation;
        
        if (pi.stepFieldTemplates != null) {
        	this.stepFieldTemplates = new HashMap<Integer, HashMap<String, Element>>(pi.stepFieldTemplates);
        }
    }
    
	public void setParticleSpecies(HashMap<String, Species> particleSpecies) {
		this.particleSpecies = particleSpecies;
	}

	public String getCurrentSpecies() {
		return currentSpecies;
	}

	public void setCurrentSpecies(String currentSpecies) {
		this.currentSpecies = currentSpecies;
	}

	public HashMap<String, Species> getParticleSpecies() {
		return particleSpecies;
	}

	public Node getPostInitialization() {
		return postInitialization;
	}

	public void setPostInitialization(Node postInitialization) {
		this.postInitialization = postInitialization;
	}

	public HashMap<String, ReferenceFrameType> getReferenceFrame() {
		return referenceFrame;
	}

	public void setReferenceFrame(HashMap<String, ReferenceFrameType> referenceFrame) {
		this.referenceFrame = referenceFrame;
	}

	public Element getParticleVolume() {
		return particleSpecies.get(currentSpecies).getParticleVolume();
	}

	public Node getCurrentTimeStep() {
		return currentTimeStep;
	}

	public void setCurrentTimeStep(Node currentStep) {
		this.currentTimeStep = currentStep;
	}
	
	public void setCurrentTimeStepIndex(int currentStepIndex) {
		this.currentTimeStepIndex = currentStepIndex;
	}

	public ArrayList<String> getMeshFields() {
		return meshFields;
	}

	public void setMeshFields(List<String> meshFields) {
		this.meshFields = new ArrayList<String>(meshFields);
	}

	public ArrayList<String> getParticleFields() {
		return particleSpecies.get(currentSpecies).getParticleFields();
	}

	public static int getNumberOfRegions() {
		return numberOfRegions;
	}

	public static void setNumberOfRegions(int numberOfRegions) {
		ProcessInfo.numberOfRegions = numberOfRegions;
	}

	public String getModelId() {
        return modelId;
    }

    public void setModelId(String modelId) {
        this.modelId = modelId;
    }

    public boolean isTimeInterpolator() {
		return isTimeInterpolator;
	}

	public void setTimeInterpolator(boolean iti) {
		this.isTimeInterpolator = iti;
	}

    public boolean isExtrapolation() {
        return extrapolation;
    }

    public void setExtrapolation(boolean extrapolation) {
        this.extrapolation = extrapolation;
    }

    public Document getCompleteProblem() {
        return completeProblem;
    }

    public void setCompleteProblem(Document completeProblem) {
        this.completeProblem = completeProblem;
    }

    public ExtrapolationType getExtrapolationMethod() {
        return extrapolationMethod;
    }

    public Element getKernel() {
		return kernel;
	}

	public void setKernel(Element kernel) {
		this.kernel = kernel;
	}

	public Element getKernelGradient() {
		return kernelGradient;
	}

	public void setKernelGradient(Element kernelGradient) {
		this.kernelGradient = kernelGradient;
	}

	public void setExtrapolationMethod(ExtrapolationType extrapolationMethod) {
        this.extrapolationMethod = extrapolationMethod;
    }

    public IncompatibilityFields getIncomFields() {
        return incomFields;
    }

    public void setIncomFields(IncompatibilityFields incomFields) {
        this.incomFields = incomFields;
    }

    public String getSpatialCoord1() {
        return spatialCoord1;
    }

    public void setSpatialCoord1(String spatialCoord1) {
        this.spatialCoord1 = spatialCoord1;
    }

    public String getSpatialCoord2() {
        return spatialCoord2;
    }

    public void setSpatialCoord2(String spatialCoord2) {
        this.spatialCoord2 = spatialCoord2;
    }

    public ArrayList<Tensor> getTensors() {
        return tensors;
    }

    public ArrayList<String> getBaseSpatialCoordinates() {
        return baseSpatialCoordinates;
    }
    public void setBaseSpatialCoordinates(ArrayList<String> baseSpatialCoordinates) {
        this.baseSpatialCoordinates = baseSpatialCoordinates;
    }
    public String getRegionName() {
        return regionName;
    }
    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }
    public CoordinateInfo getCoordinateRelation() {
        return coordinateRelation;
    }
    public RegionInfo getRegionInfo() {
        return regionInfo;
    }
    public Element getEquation() {
        return equation;
    }
    public Element getCharDecomposition() {
        return charDecomposition;
    }

    public void setCoordinateRelation(CoordinateInfo coordinateRelation) {
        this.coordinateRelation = coordinateRelation;
    }
    public void setRegionInfo(RegionInfo fieldRelation) {
        this.regionInfo = new RegionInfo(fieldRelation);
    }
    public void setEquation(Element equation) {
        this.equation = equation;
    }

	public SchemaCreator getSchemaCreator() {
		return schemaCreator;
	}

	public void setSchemaCreator(SchemaCreator schemaCreator) {
		this.schemaCreator = schemaCreator;
	}

	public DiscretizationType getDiscretizationType() {
		return discretizationType;
	}

	public void setDiscretizationType(DiscretizationType discretizationType) {
		this.discretizationType = discretizationType;
	}

	public HashMap<Integer, HashMap<String, Element>> getStepFieldTemplates() {
		return stepFieldTemplates;
	}

	public void setStepFieldTemplates(HashMap<Integer, HashMap<String, Element>> stepFieldTemplates) {
		this.stepFieldTemplates = stepFieldTemplates;
	}
	
	/**
	 * Get the proper variable template in the current step for a given field.
	 * @param field		Field to get template
	 * @return			Template
	 */
	public Element getCurrentFieldTemplate(String field) {
		return stepFieldTemplates.get(currentTimeStepIndex).get(field);
	}

	/**
	 * If the process has fluxes.
	 * @return	True if has fluxes
	 */
	public boolean hasFluxes() {
		return hasFluxes;
	}

	public void setHasFluxes(boolean hasFluxes) {
		this.hasFluxes = hasFluxes;
	}

	public HashMap<String, Element> getParticleVelocities() {
		return particleSpecies.get(currentSpecies).getParticleVelocities();
	}

	public int getCompactSupportRatio() {
		return compactSupportRatio;
	}

	public void setCompactSupportRatio(int compactSupportRatio) {
		this.compactSupportRatio = compactSupportRatio;
	}

	/**
	 * Gets the operators info depending on the discretization type.
	 * @return	Ops info
	 */
	public OperatorsInfo getOperatorsInfo() {
		if (discretizationType == DiscretizationType.mesh) {
			return operatorsInfoMesh;
		}
		else {
			return operatorsInfoParticles.get(currentSpecies);
		}
	}
	
	public OperatorsInfo getOperatorsInfoMesh() {
		return operatorsInfoMesh;
	}

	public void setOperatorsInfoMesh(OperatorsInfo operatorsInfoMesh) {
		this.operatorsInfoMesh = operatorsInfoMesh;
	}

	public HashMap<String, OperatorsInfo> getOperatorsInfoParticles() {
		return operatorsInfoParticles;
	}

	public void setOperatorsInfoParticles(HashMap<String, OperatorsInfo> operatorsInfoParticles) {
		this.operatorsInfoParticles = operatorsInfoParticles;
	}

	public Set<String> getAuxiliaryVariables() {
		return auxiliaryVariables;
	}

	public void setAuxiliaryVariables(Set<String> auxiliaryVariables) {
		this.auxiliaryVariables = auxiliaryVariables;
	}

	/**
     * Sets the characteristic decomposition and calculate the vectors.
     * @param charDecomposition         The characteristic decomposition
     */
    public void setCharDecomposition(Element charDecomposition) {
        this.charDecomposition = charDecomposition;
        this.eigenVectors = calculateVectors();
    }
    public String getField() {
        return currentField;
    }
    public void setField(String field) {
        this.currentField = field;
    }
    public String getVector() {
        return vector;
    }
    public void setVector(String vector) {
        this.vector = vector;
    }
    public ArrayList<String> getEigenVectors() {
        return eigenVectors;
    }
    
    /**
     * Adds a step variable to the list.
     * @param stepVariable	The variable to add
     */
    public void addStepVariable(Element stepVariable) {
		this.stepVariables.add(stepVariable);
	}

	public ArrayList<Element> getStepVariables() {
		return stepVariables;
	}

    public void addStiffVariable(Element stiffVariable) {
		this.stiffVariables.add(stiffVariable);
	}
	
	public ArrayList<Element> getStiffVariables() {
		return stiffVariables;
	}
	
	/**
     * Get the coordinates used in fluxes.
     * @return  The coordinates used in an array list
     */
    public ArrayList<String> getFluxesCoords() {
        try {
            ArrayList<String> coordList = new ArrayList<String>();
            NodeList coords = AGDMUtils.find(equation, ".//mms:flux/mms:coordinate");
            for (int i = 0; i < coords.getLength(); i++) {
                coordList.add(coords.item(i).getTextContent());
            }
            return coordList;
        } 
        catch (Exception e) {
            return null;
        }
    }
    
    /**
     * Get the source component of the equation.
     * @return  The source component.
     */
    public Element getSource() {
        try {
            return (Element) equation.getElementsByTagName("mms:source").item(0).getFirstChild();
        } 
        catch (Exception e) {
            return null;
        }
    }
    
    /**
     * Store the vector names of the characteristic decomposition. 
     * @return      The vector list
     */
    public ArrayList<String> calculateVectors() {
        ArrayList<String> vectors = new ArrayList<String>();
        try {
            NodeList vectorList = AGDMUtils.find(charDecomposition, ".//mms:eigenVector/mms:name");
            for (int i = 0; i < vectorList.getLength(); i++) {
                vectors.add(vectorList.item(i).getTextContent());
            }
        } 
        catch (Exception e) {
            return null;
        }
        return vectors;
    }
    
    /**
     * Sets the tensor information.
     * @param tensorList    The tensor XML DOM elements
     */
    public void setTensor(NodeList tensorList) {
        tensors = new ArrayList<Tensor>();
        for (int i = 0; i < tensorList.getLength(); i++) {
            tensors.add(new Tensor(tensorList.item(i)));
        }
    }
    
    /**
     * Calculates the schema step variables and store them in the proper variable without index.
     * @param schema			The schema
     * @throws AGDMException	AGDM007 External error
     */
    public void calculateStepVariables(Document schema) throws AGDMException {
    	this.stepVariables = new ArrayList<Element>();
    	NodeList stepVariableList = AGDMUtils.find(schema, "//mms:step/mms:timeDiscretization/mms:outputVariable/mt:math/*");
    	for (int i = 0; i < stepVariableList.getLength(); i++) {
    		Element stepVariable = (Element) stepVariableList.item(i).cloneNode(true);
        	if (stepVariable.getLocalName().equals("sharedVariable")) {
        		stepVariable = (Element) stepVariable.getFirstChild().getFirstChild();
        	}
        	else {
        		stepVariable = (Element) stepVariable.getFirstChild();
        	}
    		this.stepVariables.add(stepVariable);
    	}
    	//Add variable at step n
    	this.stepVariables.add((Element) AGDMUtils.createPreviousTimeSlice(schema).getFirstChild());
    }
    
    public void calculateStiffVariables(Document schema) throws AGDMException {
    	this.stiffVariables = new ArrayList<Element>();
    	NodeList stiffVariableList = AGDMUtils.find(schema, "/mms:PDEDiscretizationSchema/mms:schema/mms:step/mms:timeDiscretization/mms:implicit/mms:stiffRHSVariable/mt:math/*");
    	for (int i = 0; i < stiffVariableList.getLength(); i++) {
    		Element stiffVariable = (Element) stiffVariableList.item(i).cloneNode(true);
        	if (stiffVariable.getLocalName().equals("sharedVariable")) {
        		stiffVariable = (Element) stiffVariable.getFirstChild().getFirstChild();
        	}
        	else {
        		stiffVariable = (Element) stiffVariable.getFirstChild();
        	}
    		this.stiffVariables.add(stiffVariable);
    	}
    }
    
    /**
     * Check if a given variables is a step variable.
     * @param variable			The variable to check
     * @param replacedFieldTag	If the input variable has its fieldTag replaced
     * @return					True if the variable is a step variable
     * @throws AGDMException	AGDM007 External error
     */
    public boolean isStepVariable(Node variable, boolean replacedFieldTag) throws AGDMException {
    	if (stepVariables != null && variable.getChildNodes().getLength() > 0) {
	    	Element compareVariable = (Element) variable.cloneNode(true);
	    	if (compareVariable.getLocalName().equals("neighbourParticle")) {
	    		compareVariable = (Element) compareVariable.getFirstChild();
	    	}
	    	if (compareVariable.getLocalName().equals("sharedVariable")) {
	    		compareVariable = (Element) compareVariable.getFirstChild();
	    	}
	    	if (compareVariable.getLocalName().equals("apply")) {
	    		compareVariable = (Element) compareVariable.getFirstChild();
	    	}
	    	for (int i = 0; i < stepVariables.size(); i++) {
	    		Element stepVariable = (Element) stepVariables.get(i).cloneNode(true);
	        	if (replacedFieldTag) {
	        		stepVariable = ExecutionFlow.replaceTags(stepVariable.cloneNode(true), this);
	        	}
	    		if (compareVariable.isEqualNode(stepVariable)) {
	    			return true;
	    		}
	    	}
    	}
    	return false;
    }
    
    /**
     * Checks if any field is Eulerian.
     * @return			True if any Eulerian
     */
    public boolean isAnyEulerianParticle() {
    	Collection<Species> species = particleSpecies.values();
    	for (Species s : species) {
    		for (String field : s.getParticleFields()) {
        		if (referenceFrame.get(field) == ReferenceFrameType.eulerian) {
        			return true;
        		}
        	}	
    	}
    	return false;
    }
    
    /**
     * Checks if a field is Eulerian.
     * @param field		field to check
     * @return			True if Eulerian
     */
    public boolean isEulerianField(String field) {
    	return referenceFrame.get(field) == ReferenceFrameType.eulerian;
    }
    
    /**
     * Checks if a field is Lagrangian.
     * @param field		field to check
     * @return			True if Lagrangian
     */
    public boolean isLagrangianField(String field) {
    	return referenceFrame.get(field) == ReferenceFrameType.lagrangian;
    }
    
    /**
     * Gets the velocity component.
     * @param coord		The coordinate from the velocity vector to extract component.
     * @return			velocity component.
     */
    public Element getVelocityComponent(String coord) {
    	return particleSpecies.get(currentSpecies).getParticleVelocities().get(coord);
    }


	public ConstantInformation getConstantInformation() {
		return constantInformation;
	}

	public void setConstantInformation(ConstantInformation constantInformation) {
		this.constantInformation = constantInformation;
	}
    
	@Override
	public String toString() {
		return "ProcessInfo [coordinateRelation=" + coordinateRelation + ", regionInfo="
				+ regionInfo + ", baseSpatialCoordinates=" + baseSpatialCoordinates + ", equation=" + equation
				+ ", charDecomposition=" + charDecomposition + ", eigenVectors=" + eigenVectors + ", field=" + currentField
				+ ", vector=" + vector + ", regionName=" + regionName + ", extrapolationMethod=" + extrapolationMethod
				+ ", spatialCoord1=" + spatialCoord1 + ", spatialCoord2=" + spatialCoord2 + ", tensors=" + tensors
				+ ", incomFields=" + incomFields + ", completeProblem=" + completeProblem + ", extrapolation="
				+ extrapolation + ", modelId=" + modelId
				+ ", isTimeInterpolator=" + isTimeInterpolator + ", hasFluxes=" + hasFluxes + ", stepVariables="
				+ stepVariables + ", auxiliaryVariables=" + auxiliaryVariables + ", operatorsInfoMesh="
				+ operatorsInfoMesh + ", operatorsInfoParticles=" + operatorsInfoParticles + ", schemaCreator="
				+ schemaCreator + ", discretizationType=" + discretizationType + ", meshFields=" + meshFields
				+ ", particleFields=" + particleSpecies + "]";
	}
}
