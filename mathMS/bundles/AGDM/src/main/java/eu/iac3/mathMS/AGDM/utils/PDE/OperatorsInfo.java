/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.AGDM.utils.PDE;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Optional;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import eu.iac3.mathMS.AGDM.AGDMException;
import eu.iac3.mathMS.AGDM.AGDMImpl;
import eu.iac3.mathMS.AGDM.utils.AGDMUtils;
import eu.iac3.mathMS.AGDM.utils.DiscretizationPolicy;
import eu.iac3.mathMS.AGDM.utils.DiscretizationType;

/**
 * The information needed to process an equation.
 * There are three kinds of variables in Models:
 * 		Fields
 * 		Auxiliary fields
 * 		Auxiliary variables
 * 
 * The first two are stored in memory, and the last one calculated when needed.
 * So, there is need an algorithm to detect which auxiliary variables are used by fields or auxiliary fields depending on the context.
 * 
 * Moreover, fields and auxiliary fields may have derivatives of themselves, and/or auxiliary variables. But never they will contain
 * derivatives of auxiliary variables.
 * 
 * 
 * Auxiliary variable structures (derivative levels, multiplication levels, and equation terms level) are created
 * in the same structures as fields. Then, derivative levels and multiplication levels for auxiliary variables are copied
 * to auxiliary field structures. So both derivative levels and multiplication levels for auxiliary variables could be duplicated (for fields and auxiliary fields).
 * Equation terms level for auxiliary variables only remain in field structures (Need to change that).
 * 
 * After the duplication of structures for auxiliary variables, there is a process to purge the auxiliary variables that are not
 * used in both structures independently.
 * 
 * 
 * @author bminyano
 *
 */
public class OperatorsInfo {
    private ArrayList<DerivativeLevel> derivativeLevels;
    private ArrayList<DerivativeLevel> auxiliaryDerivativeLevels;
    private ArrayList<NormalizationLevel> normalizationLevels;
    private ArrayList<NormalizationLevel> auxiliaryNormalizationLevels;
    
    private ArrayList<MultiplicationTermsLevel> multiplicationLevels;
    private ArrayList<MultiplicationTermsLevel> auxiliaryMultiplicationLevels;
    private EquationTermsLevel equationTerms;
    private EquationTermsLevel auxiliaryFieldEquations;
    private FluxTerms fluxTerms;
    private SourceTerms sourceTerms;
    private StiffSourceTerms stiffSourceTerms;
    private ArrayList<String> evolutionFields;
    private ArrayList<String> auxiliaryFields;
    private ArrayList<String> analysisFields;
    private ArrayList<String> variablesToInterpolate;
    private ArrayList<String> auxiliaryVariablesToInterpolate;
    private Node stiffSolver;
    
    //To avoid repetition of derivatives processing when ascending the tree
    private ArrayList<String> derivativesProcessed;
    
    private int stencil;
    private int accumulatedStencil;
    
    private DiscretizationType type;
      
    /**
     * Default constructor.
     */
    public OperatorsInfo() {
    	derivativeLevels = new ArrayList<DerivativeLevel>();
    	auxiliaryDerivativeLevels = new ArrayList<DerivativeLevel>();
    	multiplicationLevels = new ArrayList<MultiplicationTermsLevel>();
    	auxiliaryMultiplicationLevels = new ArrayList<MultiplicationTermsLevel>();
    	equationTerms = new EquationTermsLevel();
    	auxiliaryFieldEquations = new EquationTermsLevel();
    	fluxTerms = new FluxTerms();
    	sourceTerms = new SourceTerms();
    	stiffSourceTerms = new StiffSourceTerms();
    	evolutionFields = new ArrayList<String>();
    	auxiliaryFields = new ArrayList<String>();
    	variablesToInterpolate = new ArrayList<String>();
    }
    /**
     * Copy constructor.
     * @param opInfo	original object
     */
	public OperatorsInfo(OperatorsInfo opInfo) {
    	this.derivativeLevels = new ArrayList<DerivativeLevel>();
    	for (int i = 0; i < opInfo.derivativeLevels.size(); i++) {
    		derivativeLevels.add(new DerivativeLevel(opInfo.derivativeLevels.get(i)));
    	}
    	this.auxiliaryDerivativeLevels = new ArrayList<DerivativeLevel>();
    	for (int i = 0; i < opInfo.auxiliaryDerivativeLevels.size(); i++) {
    		auxiliaryDerivativeLevels.add(new DerivativeLevel(opInfo.auxiliaryDerivativeLevels.get(i)));
    	}
    	this.multiplicationLevels = new ArrayList<MultiplicationTermsLevel>();
    	for (int i = 0; i < opInfo.multiplicationLevels.size(); i++) {
    		multiplicationLevels.add(new MultiplicationTermsLevel(opInfo.multiplicationLevels.get(i)));
    	}
    	this.auxiliaryMultiplicationLevels = new ArrayList<MultiplicationTermsLevel>();
    	for (int i = 0; i < opInfo.auxiliaryMultiplicationLevels.size(); i++) {
    		auxiliaryMultiplicationLevels.add(new MultiplicationTermsLevel(opInfo.auxiliaryMultiplicationLevels.get(i)));
    	}
    	this.equationTerms = new EquationTermsLevel(opInfo.equationTerms);
    	this.auxiliaryFieldEquations = new EquationTermsLevel(opInfo.auxiliaryFieldEquations);
    	this.fluxTerms = new FluxTerms(opInfo.fluxTerms);
    	this.sourceTerms = new SourceTerms(opInfo.sourceTerms);
    	this.stiffSourceTerms = new StiffSourceTerms(opInfo.stiffSourceTerms);
    	this.evolutionFields = new ArrayList<String>();
    	for (String f : opInfo.evolutionFields) {
    		this.evolutionFields.add(f);
    	}
    	this.auxiliaryFields = new ArrayList<String>();
    	for (String f : opInfo.auxiliaryFields) {
    		this.auxiliaryFields.add(f);
    	}
    	this.variablesToInterpolate = new ArrayList<String>();
    	for (String f : opInfo.variablesToInterpolate) {
    		this.variablesToInterpolate.add(f);
    	}
    	this.auxiliaryVariablesToInterpolate = new ArrayList<String>();
    	for (String f : opInfo.auxiliaryVariablesToInterpolate) {
    		this.auxiliaryVariablesToInterpolate.add(f);
    	}
    	if (opInfo.normalizationLevels != null) {
	    	this.normalizationLevels = new ArrayList<NormalizationLevel>();
	    	for (NormalizationLevel f : opInfo.normalizationLevels) {
	    		this.normalizationLevels.add(f);
	    	}
    	}
    	if (opInfo.auxiliaryNormalizationLevels != null) {
	    	this.auxiliaryNormalizationLevels = new ArrayList<NormalizationLevel>();
	    	for (NormalizationLevel f : opInfo.auxiliaryNormalizationLevels) {
	    		this.auxiliaryNormalizationLevels.add(f);
	    	}
    	}
    	this.type = opInfo.type;
    	if (opInfo.stiffSolver != null) {
    		this.stiffSolver = opInfo.stiffSolver.cloneNode(true);
    	}
    }
    
    /**
     * Constructor. Extract the spatial operator information for discretization from the models and region interactions.
     * @param problem           The problem where the interactions are defined
     * @param models            The models used in the problem to discretize
     * @param policy            The discretization policy
     * @param includedFields	The list of fields to get operators for
     * @param regionPolicy		The region discretization policy
     * @param type				Discretization type
     * @throws AGDMException    AGDM008  Invalid simulation problem
     *                          AGDM00X  External error
     */
    public OperatorsInfo(Document problem, Node models, DiscretizationPolicy policy, AbstractPDEDiscPolicy regionPolicy, List<String> includedFields, DiscretizationType type) throws AGDMException {
        derivativeLevels = new ArrayList<DerivativeLevel>();
        auxiliaryDerivativeLevels = new ArrayList<DerivativeLevel>();
        multiplicationLevels = new ArrayList<MultiplicationTermsLevel>();
        auxiliaryMultiplicationLevels = new ArrayList<MultiplicationTermsLevel>();
        equationTerms = new EquationTermsLevel();
        auxiliaryFieldEquations = new EquationTermsLevel();
        fluxTerms = new FluxTerms();
        sourceTerms = new SourceTerms();
        stiffSourceTerms = new StiffSourceTerms();
        evolutionFields = new ArrayList<String>();
        auxiliaryFields = new ArrayList<String>();
        analysisFields = new ArrayList<String>();
        this.type = type;
        derivativesProcessed = new ArrayList<String>();
        stencil = 0;
    	variablesToInterpolate = new ArrayList<String>();
    	auxiliaryVariablesToInterpolate = new ArrayList<String>();
        if (includedFields.size() > 0) {
	        //Evolution equations, analysis variables and auxiliary variables
	        NodeList equations = AGDMUtils.find(models, ".//mms:evolutionEquation|.//mms:auxiliaryVariableEquation|.//mms:projection"
	                + "|.//mms:analysisFieldEquation|.//mms:auxiliaryAnalysisVariableEquation");
	        for (int i = 0; i < equations.getLength(); i++) {
	            ArrayList<DerivativeLevel> equationDerivativeLevels = new ArrayList<DerivativeLevel>();
	            Element equation = (Element) equations.item(i);
	            String field = AGDMUtils.find(equation, "./mms:field|./mms:auxiliaryVariable|./mms:variable|./mms:analysisField"
	                    + "|./mms:auxiliaryAnalysisVariable").item(0).getTextContent();
	            if (equation.getLocalName().equals("auxiliaryVariableEquation") || includedFields.contains(field)
	            		|| type == DiscretizationType.mesh && (
	            	equation.getLocalName().equals("projection")
	            	|| equation.getLocalName().equals("analysisFieldEquation")
	            	|| equation.getLocalName().equals("auxiliaryAnalysisVariableEquation"))) {
	            
		            String opTypePrefix = "";
		            if (equation.getLocalName().equals("projection")) {
		                opTypePrefix = "i_";
		            }
		            if (equation.getLocalName().equals("evolutionEquation")) {
		                evolutionFields.add(field);
		            }
		            if (equation.getLocalName().equals("analysisFieldEquation")) {
		                analysisFields.add(field);
		            }
		            String opType = equation.getLocalName();
		            
		            Node conditional = null;
		            NodeList cond = AGDMUtils.find(equation, "./mms:evolutionCondition/mt:math");
		            if (cond.getLength() > 0) {
		            	conditional = cond.item(0);
		            }
		            
		            EquationType eqType = EquationType.algebraic;
		            Element algorithm = null;
		            ArrayList<String> fields = new ArrayList<String>();
		            NodeList fieldEls = AGDMUtils.find(equation, "./mms:field|./mms:auxiliaryVariable|./mms:variable|./mms:analysisField"
	                    + "|./mms:auxiliaryAnalysisVariable");
		            for (int j = 0; j < fieldEls.getLength(); j++) {
		                fields.add(fieldEls.item(j).getTextContent());
		            }
		            NodeList algorithmEl = equation.getElementsByTagName("sml:simml");
		            NodeList algebraicEl = equation.getElementsByTagName("mms:algebraic");
		            if (algorithmEl.getLength() > 0 && algebraicEl.getLength() > 0) {
		            	throw new AGDMException(AGDMException.AGDM005, 
		            			"Auxiliary field equations cannot be defined with both algorithm and algebraic terms.");
		            }
		            //Algorithmic definition
		            if (algorithmEl.getLength() > 0) {
		            	algorithm = (Element) algorithmEl.item(0).cloneNode(true);
		            	eqType = EquationType.algorithmic;
		            }
		            //Process every operator from Non-Conservative terms
		            ArrayList<String> eqTerms = new ArrayList<String>();
		            if (algebraicEl.getLength() > 0 || equation.getElementsByTagName("mms:operator").getLength() > 0) {
		            	eqType = EquationType.algebraic;
		                if (fields.size() > 1) {
		                	throw new AGDMException(AGDMException.AGDM005, 
		                			"Auxiliary variable equations defined with algebraic terms can only define one auxiliary variable.");
		                }
			            NodeList operators = equation.getElementsByTagName("mms:operator");
			            if (algebraicEl.getLength() > 0) {
				            Node definition = algebraicEl.item(0).getFirstChild();
				            if (operators.getLength() > 0 && definition.getLocalName().equals("math")) {
			                	throw new AGDMException(AGDMException.AGDM005, 
			                			"Auxiliary variable equations defined with algebraic terms must use mathematics or operators, but not both.");
				            }
				            //Simple mathematic expression. 
				            if (definition.getLocalName().equals("math")) {
				            	algorithm = AGDMUtils.createElement(models.getOwnerDocument(), AGDMUtils.simmlUri, "simml");
				            	Element math = AGDMUtils.assignVariable(fields.get(0), definition.getFirstChild().cloneNode(true));
				            	algorithm.appendChild(math);
				            	eqType = EquationType.algorithmic;
				            }
			            }
			            //Operators
			            if (operators.getLength() > 0 ) {
				            for (int j = 0; j < operators.getLength(); j++) {
				                Element operator = (Element) operators.item(j);
				                String operatorName = operator.getElementsByTagName("mms:name").item(0).getTextContent();
				                //Process every term
				                NodeList terms = operator.getElementsByTagName("mms:term");
				                for (int k = 0; k < terms.getLength(); k++) {
				                    ArrayList<DerivativeLevel> termDerivativeLevels = new ArrayList<DerivativeLevel>();
				                    Element term = (Element) terms.item(k);
				                    int level = 0;
				                    //Get the lower level derivatives in the term
				                    
				                    NodeList lowerLevelDerivatives = AGDMUtils.find(term, ".//mms:partialDerivatives[not(descendant::mms:partialDerivatives)]");
				                    //If there is no derivatives, the term is a source term
				                    if (lowerLevelDerivatives.getLength() == 0) {
				                        processSource(term, eqTerms, field, j, k, termDerivativeLevels, opType, opTypePrefix, conditional);
				                    }
				                    //Process a term with derivatives
				                    else {
				                        for (int l = 0; l < lowerLevelDerivatives.getLength(); l++) {
				                        	accumulatedStencil = 0;
				                        	ArrayList<MultiplicationTermsLevel> localMultiplicationLevels = new ArrayList<MultiplicationTermsLevel>();
				                            level = processDerivativesTerm((Element) lowerLevelDerivatives.item(l), eqTerms, regionPolicy, operatorName, field, j, k, 
				                                    level, Optional.empty(), termDerivativeLevels, opType, opTypePrefix, 0, localMultiplicationLevels, conditional);
				                            addReverseLocalMultiplicationTermsLevel(multiplicationLevels, localMultiplicationLevels);
				                            stencil = Math.max(stencil, accumulatedStencil);
				                        }
				                    }
				                    appendEquationDerivativeLevels(equationDerivativeLevels, termDerivativeLevels);
				                }
				            }
			            }
		            }
		            
		            //Process fluxes
		            NodeList fluxes = equation.getElementsByTagName("mms:flux");
		            for (int j = 0; j < fluxes.getLength(); j++) {
		            	String coord = fluxes.item(j).getFirstChild().getTextContent();
		            	fluxTerms.add(field, coord, (Element) fluxes.item(j).getLastChild().cloneNode(true), conditional);
		            }
		            //Process sources
		            NodeList sources = equation.getElementsByTagName("mms:source");
		            for (int j = 0; j < sources.getLength(); j++) {
		            	sourceTerms.add(field, (Element) sources.item(j).getFirstChild(), conditional);
		            }
		            //Process stiff sources
		            NodeList stiffSources = equation.getElementsByTagName("mms:stiffSource");
		            for (int j = 0; j < stiffSources.getLength(); j++) {
		            	stiffSourceTerms.add(field, (Element) stiffSources.item(j).getFirstChild(), conditional);
		            }
		            
		            //Set the equation terms summation
		            if (eqTerms.size() > 0 || algorithm != null) {
		            	equationTerms.add(fields, eqTerms, algorithm, opType, eqType, conditional, false);
		            }
		            //Add equation derivative levels to the current object. 
		            appendEquationDerivativeLevels(derivativeLevels, equationDerivativeLevels);
	            }
	        }
	        
	        //Auxiliary fields
	        equations = AGDMUtils.find(models, ".//mms:auxiliaryFieldEquation");
	        derivativesProcessed = new ArrayList<String>();
	        for (int i = 0; i < equations.getLength(); i++) {
	            ArrayList<DerivativeLevel> equationDerivativeLevels = new ArrayList<DerivativeLevel>();
	            Element equation = (Element) equations.item(i);
	            ArrayList<String> auxFields = new ArrayList<String>();
	            NodeList fields = equation.getElementsByTagName("mms:auxiliaryField");
	            boolean added = false;
	            boolean maximumPriority = equation.getElementsByTagName("mms:maximumPriority").getLength() > 0;
	            if (includedFields.contains(fields.item(0).getTextContent())) {
	            	auxFields.add(fields.item(0).getTextContent());
	            	added = true;
	            }
	            for (int j = 1; j < fields.getLength(); j++) {
	            	if ((!added && includedFields.contains(fields.item(j).getTextContent()))
	            		|| (added && !includedFields.contains(fields.item(j).getTextContent()))) {
	            		throw new AGDMException(AGDMException.AGDM005, "Auxiliary algorithmic definitions cannot be mixed (defined for particles and mesh) (Field " + fields.item(j) + ")");  
	            	}
	                if (added && includedFields.contains(fields.item(j).getTextContent())) {
	                	auxFields.add(fields.item(j).getTextContent());
	                }
	            }
	            
	            Node conditional = null;
	            NodeList cond = AGDMUtils.find(equation, "./mms:evolutionCondition/mt:math");
	            if (cond.getLength() > 0) {
	            	conditional = cond.item(0);
	            }
	            
	            NodeList algorithmEl = equation.getElementsByTagName("sml:simml");
	            NodeList algebraicEl = equation.getElementsByTagName("mms:algebraic");
	            if (algorithmEl.getLength() > 0 && algebraicEl.getLength() > 0) {
	            	throw new AGDMException(AGDMException.AGDM005, 
	            			"Auxiliary field equations cannot be defined with both algorithm and algebraic terms.");
	            }
	            //Algorithmic definition
	            Element algorithm = null;
	            EquationType opType = null;
	            if (algorithmEl.getLength() > 0 && !auxFields.isEmpty()) {
	                if (includedFields.contains(fields.item(0).getTextContent())) {
		            	algorithm = (Element) algorithmEl.item(0).cloneNode(true);
		            	opType = EquationType.algorithmic;
	                }
	            }

	            //Algebraic definition
	            ArrayList<String> eqTerms = new ArrayList<String>();
	            if (algebraicEl.getLength() > 0 && !auxFields.isEmpty()) {
	               	opType = EquationType.algebraic;
	               	auxiliaryFields.add(auxFields.get(0));
	                if (auxFields.size() > 1) {
	                	throw new AGDMException(AGDMException.AGDM005, 
	                			"Auxiliary field equations defined with algebraic terms can only define one auxiliary field.");
	                }
		            NodeList operators = ((Element) algebraicEl.item(0)).getElementsByTagName("mms:operator");
		            Node definition = algebraicEl.item(0).getFirstChild();
		            if (operators.getLength() > 0 && definition.getLocalName().equals("math")) {
	                	throw new AGDMException(AGDMException.AGDM005, 
	                			"Auxiliary field equations defined with algebraic terms must use mathematics or operators, but not both.");
		            }
		            if (operators.getLength() > 0 && maximumPriority) {
		            	throw new AGDMException(AGDMException.AGDM005, 
	                			"Auxiliary field equations with maximum priorities cannot be defined using derivatives.");
		            }
		            //Simple mathematic expression. 
		            if (definition.getLocalName().equals("math")) {
		            	algorithm = AGDMUtils.createElement(models.getOwnerDocument(), AGDMUtils.simmlUri, "simml");
		            	Element math = AGDMUtils.assignVariable(auxFields.get(0), definition.getFirstChild().cloneNode(true));
		            	algorithm.appendChild(math);
		               	opType = EquationType.algorithmic;
		            }
		            //Operators
		            if (operators.getLength() > 0 ) {
			            for (int j = 0; j < operators.getLength(); j++) {
			                Element operator = (Element) operators.item(j);
			                String operatorName = operator.getElementsByTagName("mms:name").item(0).getTextContent();
			                //Process every term
			                NodeList terms = operator.getElementsByTagName("mms:term");
			                for (int k = 0; k < terms.getLength(); k++) {
			                    ArrayList<DerivativeLevel> termDerivativeLevels = new ArrayList<DerivativeLevel>();
			                    Element term = (Element) terms.item(k);
			                    int level = 0;
			                    //Get the lower level derivatives in the term
			                    NodeList lowerLevelDerivatives = AGDMUtils.find(term, ".//mms:partialDerivatives[not(descendant::mms:partialDerivatives)]");
			                    //If there is no derivatives, the term is a source term
			                    if (lowerLevelDerivatives.getLength() == 0) {
			                        processSource(term, eqTerms, auxFields.get(0), j, k, termDerivativeLevels, "auxiliaryField", "", conditional);
			                    }
			                    //Process a term with derivatives
			                    else {
			                        for (int l = 0; l < lowerLevelDerivatives.getLength(); l++) {
			                        	ArrayList<MultiplicationTermsLevel> localMultiplicationLevels = new ArrayList<MultiplicationTermsLevel>();
			                            level = processDerivativesTerm((Element) lowerLevelDerivatives.item(l), eqTerms, regionPolicy, operatorName, auxFields.get(0), j, k, 
			                                    level, Optional.empty(), termDerivativeLevels, "auxiliaryField", "", 0, localMultiplicationLevels, conditional);
			                            addReverseLocalMultiplicationTermsLevel(auxiliaryMultiplicationLevels, localMultiplicationLevels);
			                        }
			                    }
			                    appendEquationDerivativeLevels(equationDerivativeLevels, termDerivativeLevels);
			
			                }
			            }
		            }
	            }
	            if (!auxFields.isEmpty()) {
		            //Set the equation terms summation
		            auxiliaryFieldEquations.add(auxFields, eqTerms, algorithm, "auxiliaryField", opType, conditional, maximumPriority);
		            //Add equation derivative levels to the current object. 
		            appendEquationDerivativeLevels(auxiliaryDerivativeLevels, equationDerivativeLevels);
	            }
	        }
	      
	        //Add auxiliary variable equations to auxiliary field structures
	        if (equations.getLength() > 0) {
	        	HashSet<String> dependentVariables = getAuxFieldsVariables(problem, policy, type);
	        	addAuxVarsToAuxFields();
		        onlyDependentAuxFields(dependentVariables, false);
	        }
	
	        NodeList stiff = AGDMUtils.find(problem, "//mms:stiffSolver/sml:simml/*");
	        Element simml = AGDMUtils.createElement(problem, AGDMUtils.simmlUri, "simml");
	        stiffSolver = null;
	        for (int i = 0; i < stiff.getLength(); i++) {
	        	simml.appendChild(stiff.item(i).cloneNode(true));
	        }
	        if (stiff.getLength() > 0) {
	        	stiffSolver = simml;
	        }

	        //Auxiliary variable filter. Only auxiliary equations variables used in equations and velocities (if particles).
	        onlyDependent(getEquationVariables(problem, policy, type, analysisFields.size() > 0), false, null);
	        this.simplify();
	        this.orderDerivatives(AGDMUtils.getBaseDiscSpatialCoordinate(problem));

	        //Mesh-Particle interaction.
	        getVariablesToInterpolate(policy);
	        //Normalization
	        if (type == DiscretizationType.particles) {
	        	normalizationLevels = calculateNormalization(derivativeLevels, multiplicationLevels);
	        	auxiliaryNormalizationLevels = calculateNormalization(auxiliaryDerivativeLevels, auxiliaryMultiplicationLevels);
	        }
        }
    }
    
    /**
     * Process a source term, without derivatives.
     * @param term                  The term to process
     * @param eqTerms               The equation terms to update
     * @param field                 The field of the equation terms
     * @param operatorId            The operator identifier
     * @param termId                The term identifier
     * @param termDerivativeLevels  The term derivative levels
     * @param opType                The operator type
     * @param opTypePrefix          A prefix for the identifiers
     * @throws AGDMException        AGDM008  Invalid simulation problem
     *                              AGDM00X  External error
     */
    private void processSource(Element term, ArrayList<String> eqTerms, String field, int operatorId, int termId, 
            ArrayList<DerivativeLevel> termDerivativeLevels, String opType, String opTypePrefix, Node conditional) throws AGDMException {
        //Add derivative(sources actually) structure
        DerivativeLevel dl = getDerivativeLevel(termDerivativeLevels, 0);
        Derivative sources = new Derivative();
        sources.setId(opTypePrefix + "d_" + field + "_o" + operatorId + "_t" + termId + "_m0_l0");
        sources.setField(field);
        NodeList algebraicExpression = term.getElementsByTagName("mt:math");
        if (algebraicExpression.getLength() == 0) {
            throw new AGDMException(AGDMException.AGDM008, 
                    "Equation for field " + field + " has a term without algebraic expression nor derivative terms.");
        }
        sources.setAlgebraicExpression(algebraicExpression.item(0).cloneNode(true));
        sources.setOperatorType(opType);
        sources.setConditional(conditional);
        dl.add(sources);
        termDerivativeLevels.set(0, dl);
        //Add term variable to the equation terms
        eqTerms.add(opTypePrefix + "d_" + field + "_o" + operatorId + "_t" + termId + "_m0_l0");
    }
    
    /**
     * Process a term with derivatives.
     * @param derivatives           		The term to process
     * @param eqTerms               		The equation terms to update
     * @param policy                		The region discretization policy
     * @param operatorName          		The name of the term operator
     * @param field                 		The field of the equation terms
     * @param operatorId            		The operator identifier
     * @param termId                		The term identifier
     * @param level                 		The current level of the term
     * @param lastVar               		The last variable used
     * @param termDerivativeLevels  		The term derivative levels
     * @param opType                		The operator type
     * @param opTypePrefix          		A prefix for the identifiers
     * @param depht							Current derivative depth
     * @param localMultiplicationTermsLevel	Current level of multiplication terms
     * @return                      		The maximum level reached for the term
     * @throws AGDMException        		AGDM00X  External error
     */
    private int processDerivativesTerm(Element derivatives, ArrayList<String> eqTerms, AbstractPDEDiscPolicy policy, String operatorName, 
            String field, int operatorId, int termId, int level, Optional<String> lastVar, ArrayList<DerivativeLevel> termDerivativeLevels, 
            String opType, String opTypePrefix, int depht, ArrayList<MultiplicationTermsLevel> localMultiplicationTermsLevel, Node conditional) throws AGDMException {
    	MultiplicationTermsLevel mtl = getMultiplicationLevel(localMultiplicationTermsLevel, depht);
        if (!derivativesProcessed.contains(opTypePrefix + "m_" + field + "_o" + operatorId + "_t" + termId + "_l" + level)) {
            derivativesProcessed.add(opTypePrefix + "m_" + field + "_o" + operatorId + "_t" + termId + "_l" + level);
        
            Optional<String> termVariable = Optional.empty();
            //if only one derivative term without algebraic expression the try to get maximum solvable schema
            if (!hasAlgebraicExpression(derivatives) && !isDerivativeProduct(derivatives)) {
                //Get a compatible schema for the current term
            	SpatialOperatorDiscretization lastValidSchema = null;
            	Element lastValidDerivative = null;
            	ArrayList<String> lastValidCoords = null;
                int order = 1;
                String coordinates = "i";
                //derivative coordinates array for comparisons
                ArrayList<String> derivCoordsCmp = new ArrayList<String>();
                //consolidated derivative coordinate
                ArrayList<String> derivCoords = new ArrayList<String>();
                derivCoordsCmp.add(derivatives.getChildNodes().item(0).getFirstChild().getTextContent());
                derivCoords.add(derivatives.getChildNodes().item(0).getFirstChild().getTextContent());
                Optional<SpatialOperatorDiscretization> schema = policy.getCompatiblePolicy(order, coordinates, operatorName);
                if (schema.isPresent()) {
                	lastValidSchema = schema.get();
                	lastValidCoords = new ArrayList<String>(derivCoords);
                	lastValidDerivative = derivatives;
                }
                Element derivative = (Element) derivatives.getChildNodes().item(0);
                //get parent derivative
                Element parentDerivative = (Element) derivatives.getParentNode();
                if (parentDerivative.getLocalName().equals("partialDerivative")) {
                    //Refresh order and coordinates to search for a operator policy
                    order++;
                    String newCoord = parentDerivative.getFirstChild().getTextContent();
                    coordinates += appendCoordinate(derivCoordsCmp, newCoord);
                }
                //ascent maximum order of derivatives that can be solved
                while (parentDerivative.getLocalName().equals("partialDerivative") && !hasAlgebraicExpression(derivatives) 
                        && !isDerivativeProduct(derivatives)) {
                    //Add coordinate only when confirmed
                    derivCoords.add(parentDerivative.getFirstChild().getTextContent());
                    schema = policy.getCompatiblePolicy(order, coordinates, operatorName);
                    //get parent derivative
                    derivatives = (Element) derivatives.getParentNode().getParentNode();
                    parentDerivative = (Element) derivatives.getParentNode();
                    if (parentDerivative.getLocalName().equals("partialDerivative")) {
                        //Refresh order and coordinates to search for a operator policy
                        order++;
                        String newCoord = parentDerivative.getFirstChild().getTextContent();
                        coordinates += appendCoordinate(derivCoordsCmp, newCoord);
                    }
                    if (schema.isPresent()) {
                        lastValidSchema = schema.get();
                        lastValidCoords = new ArrayList<String>(derivCoords);
                        lastValidDerivative = derivatives;
                    }
                }
                if (lastValidSchema == null) {
            		throw new AGDMException(AGDMException.AGDM006, "The operator " + operatorName + " does not have compatible spatial discretization.");
                }
                accumulatedStencil = accumulatedStencil + lastValidSchema.getStencil();
                termVariable = Optional.of(processDerivative(derivative, lastValidSchema, field, operatorId, operatorName, termId, 0, level, lastVar, lastValidCoords, 
                        termDerivativeLevels, opType, opTypePrefix, conditional));
                //Check if there is an algebraic expression that multiplies
                NodeList algebraic = AGDMUtils.find(lastValidDerivative, "./preceding-sibling::mt:math");
                Node algebraicExpression = null;
                if (algebraic.getLength() > 0) {
                    //Add the multiplication expression
                    algebraicExpression = algebraic.item(0).cloneNode(true);
                    ArrayList<String> multTerms = new ArrayList<String>();
                    multTerms.add(termVariable.get());
                    depht = depht + order - 1;
                    mtl = getMultiplicationLevel(localMultiplicationTermsLevel, depht);
                    mtl.add(field, opTypePrefix + "m_" + field + "_o" + operatorId + "_t" + termId + "_l" + level, 
                            multTerms, algebraicExpression, opType, conditional);
                    termVariable = Optional.of(opTypePrefix + "m_" + field + "_o" + operatorId + "_t" + termId + "_l" + level);
                }
                //Only add the last term to the equation summation
                if (!lastValidDerivative.getParentNode().getLocalName().equals("partialDerivative")) {
                    eqTerms.add(termVariable.get());
                }
                derivatives = lastValidDerivative;
            }
            //one order derivative (multiple derivatives or algebraic expression)
            else {
                ArrayList<String> multTerms = new ArrayList<String>();
                //Process all the derivatives
                NodeList derivativeList = derivatives.getChildNodes();
                int maxStencil = 0;
                for (int l = 0; l < derivativeList.getLength(); l++) {
                    Element derivative = (Element) derivativeList.item(l);
                    ArrayList<String> derivCoords = new ArrayList<String>();
                    derivCoords.add(derivative.getFirstChild().getTextContent());
                    Optional<SpatialOperatorDiscretization> schema = policy.getCompatiblePolicy(1, "i", operatorName);
                	if (!schema.isPresent()) {
                		throw new AGDMException(AGDMException.AGDM006, "The operator " + operatorName + " does not have compatible spatial discretization.");
                	}
                    multTerms.add(processDerivative(derivative, schema.get(), 
                            field, operatorId, operatorName, termId, l, level, lastVar, derivCoords, termDerivativeLevels, opType, opTypePrefix, conditional));
                    maxStencil = Math.max(maxStencil, schema.get().getStencil());
                }
                accumulatedStencil = accumulatedStencil + maxStencil;
                //if more than one derivative or has an algebraic expression a multiplication is needed
                NodeList algebraic = AGDMUtils.find(derivatives, "./preceding-sibling::mt:math");
                Node algebraicExpression = null;
                if (algebraic.getLength() > 0) {
                    algebraicExpression = algebraic.item(0).cloneNode(true);
                }
                mtl.add(field, opTypePrefix + "m_" + field + "_o" + operatorId + "_t" + termId + "_l" + level, 
                        multTerms, algebraicExpression, opType, conditional);
                termVariable = Optional.of(opTypePrefix + "m_" + field + "_o" + operatorId + "_t" + termId + "_l" + level);
                //Only add the last term to the equation summation
                if (!derivatives.getParentNode().getLocalName().equals("partialDerivative")) {
                    eqTerms.add(termVariable.get());
                }
            }
            //check if there are derivatives ancestors and calculate the term from that point.
            if (derivatives.getParentNode().getLocalName().equals("partialDerivative")) {
                return processDerivativesTerm((Element) derivatives.getParentNode().getParentNode(), eqTerms, policy, operatorName, 
                	field, operatorId, termId, level + 1, termVariable, termDerivativeLevels, opType, opTypePrefix, depht + 1, localMultiplicationTermsLevel, conditional);
            }
        }
        return level;
    }
    
    /**
     * Process a single derivative term. Includes derivative ascension to fit high order discretizations.
     * @param derivative            The derivative to process
     * @param schema                The spatial operator schema
     * @param field                 The field of the equation terms
     * @param operatorId            The operator identifier
     * @param operatorName			The operator name
     * @param termId                The term identifier
     * @param multId                The multiplication identifier
     * @param level                 The current level of the term
     * @param lastVar               The last variable used
     * @param coordinates           The spatial coordinates
     * @param termDerivativeLevels  The term derivative levels
     * @param opType                The operator type
     * @param opTypePrefix          A prefix for the identifiers
     * @return                      The derivative variable
     * @throws AGDMException        AGDM00X  External error
     */
    private String processDerivative(Element derivative, SpatialOperatorDiscretization schema, String field, int operatorId, String operatorName, int termId, int multId, 
            int level, Optional<String> lastVar, ArrayList<String> coordinates, ArrayList<DerivativeLevel> termDerivativeLevels, String opType, 
            String opTypePrefix, Node conditional) throws AGDMException {
        //Create derivative operator and add it to the corresponding derivative level
        Derivative deriv = new Derivative();
        deriv.setId(opTypePrefix + "d_" + field + "_o" + operatorId + "_t" + termId + "_m" + multId + "_l" + level);
        deriv.setField(field);
        //If no variable coming from recursivity then it is a 
        if (!lastVar.isPresent()) {
        	Node derivativeTerm = derivative.getElementsByTagNameNS(AGDMUtils.mtUri, "math").item(0);
            deriv.setDerivativeTerm(derivativeTerm.cloneNode(true));
            if (!derivativeTerm.getFirstChild().getLocalName().equals("apply")) {
            	deriv.setSimple(true);
            }
        }
        else {
            Element math = AGDMUtils.createElement(derivative.getOwnerDocument(), AGDMUtils.mtUri, "math");
            Element ci = AGDMUtils.createElement(derivative.getOwnerDocument(), AGDMUtils.mtUri, "ci");
            ci.setTextContent(lastVar.get());
            math.appendChild(ci);
            deriv.setDerivativeTerm(math);
        }
        deriv.setSpatialOperatorDiscretization(schema);
        deriv.setCoordinates(coordinates);
        deriv.mapCoordinatesToIndices();
        deriv.setInner(level > 0);
        deriv.setOperatorType(opType);
        deriv.setConditional(conditional);
        DerivativeLevel dl = getDerivativeLevel(termDerivativeLevels, level);
        dl.add(deriv);
        termDerivativeLevels.set(level, dl);
        return opTypePrefix + "d_" + field + "_o" + operatorId + "_t" + termId + "_m" + multId + "_l" + level;
    }
    
    /**
     * Returns the coordinate for a coordinate combination depending on the coordinates of the derivatives.
     * @param derivCoords   The derivative coordinates
     * @param newCoord      The last derivative coordinates
     * @return              The new coordinate for the coordinate combination
     */
    private String appendCoordinate(ArrayList<String> derivCoords, String newCoord) {
        String[] coordinates = {"i", "j", "k"};
        for (int i = 0; i < derivCoords.size(); i++) {
            if (derivCoords.get(i).equals(newCoord)) {
                return coordinates[i];
            }
        }
        derivCoords.add(newCoord);
        return coordinates[derivCoords.size() - 1];
    }
    
    /**
     * Checks if a term has an algebraic expression.
     * @param derivative        The derivative
     * @return                  True if the algebraic expression exists.
     */
    private boolean hasAlgebraicExpression(Element derivative) {
        Node algebraic = derivative.getPreviousSibling();
        return algebraic != null && algebraic.getLocalName().equals("math");
    }
    
    /**
     * Check if there is a product of derivatives.
     * @param derivative    The derivative
     * @return              True if there is a product of derivatives
     */
    private boolean isDerivativeProduct(Element derivative) {
        return derivative.getChildNodes().getLength() > 1;
    }
    
    public ArrayList<DerivativeLevel> getDerivativeLevels() {
        return derivativeLevels;
    }

    public ArrayList<MultiplicationTermsLevel> getMultiplicationLevels() {
        return multiplicationLevels;
    }

    public ArrayList<NormalizationLevel> getNormalizationLevels() {
		return normalizationLevels;
	}
	public ArrayList<NormalizationLevel> getAuxiliaryNormalizationLevels() {
		return auxiliaryNormalizationLevels;
	}
	public EquationTermsLevel getEquationTermsLevel() {
        return equationTerms;
    }
   
    public ArrayList<String> getEvolutionFields() {
        return evolutionFields;
    }
    
    public FluxTerms getFluxTerms() {
    	return fluxTerms;
    }
    
    public SourceTerms getSourceTerms() {
    	return sourceTerms;
    }
    
    public StiffSourceTerms getStiffSourceTerms() {
    	return stiffSourceTerms;
    }
    
    public ArrayList<DerivativeLevel> getAuxiliaryDerivativeLevels() {
		return auxiliaryDerivativeLevels;
	}

	public ArrayList<MultiplicationTermsLevel> getAuxiliaryMultiplicationLevels() {
		return auxiliaryMultiplicationLevels;
	}

	public EquationTermsLevel getAuxEquations() {
		return auxiliaryFieldEquations;
	}

	public Node getStiffSolver() {
		return stiffSolver;
	}
	/**
     * Get the derivative information at the specified level.
     * @param dl        The derivative levels to search into
     * @param level     The level
     * @return          The derivative information
     */
    private DerivativeLevel getDerivativeLevel(ArrayList<DerivativeLevel> dl, int level) {
        while (dl.size() <= level) {
            dl.add(new DerivativeLevel());
        }
        return dl.get(level);
    }
        
    /**
     * Get the multiplication term information at the specified level.
     * @param termsLevel	The multiplications term level
     * @param level     	The level
     * @return          	The multiplication term information
     */
    private MultiplicationTermsLevel getMultiplicationLevel(ArrayList<MultiplicationTermsLevel> termsLevel, int level) {
        while (termsLevel.size() <= level) {
        	termsLevel.add(new MultiplicationTermsLevel());
        }
        return termsLevel.get(level);
    }
    
    /**
     * Add equation derivative levels to the current object. 
     * If the new derivatives have a higher level, the existing ones must be shifted to be aligned with the new ones to the right.
     * @param previousDerivativeLevels      the previous derivative levels
     * @param currentDerivativeLevels       the new derivative levels to append
     */
    private void appendEquationDerivativeLevels(ArrayList<DerivativeLevel> previousDerivativeLevels, 
            ArrayList<DerivativeLevel> currentDerivativeLevels) {
        int currentMaxLevel = previousDerivativeLevels.size();
        int newMaxLevel = currentDerivativeLevels.size();
        
        for (int i = newMaxLevel - 1; i >= 0; i--) {
            //Index for the current derivative levels
            int j = currentMaxLevel - (newMaxLevel - i);
            
            //Append the new derivative level terms to the previous ones
            if (j >= 0) {
                DerivativeLevel dl = getDerivativeLevel(previousDerivativeLevels, j);
                dl.addAll(currentDerivativeLevels.get(i));
                previousDerivativeLevels.set(j, dl);
            }
            // If the j index is negative, there are more levels in the new derivative level structure and 
            // the lower ones must be inserted at the beginning, so the previous ones are aligned right
            else {
                previousDerivativeLevels.add(0, currentDerivativeLevels.get(i));
            }
        }
    }

  @Override
	public String toString() {
		return "OperatorsInfo [derivativeLevels=" + derivativeLevels + ", auxiliaryDerivativeLevels="
				+ auxiliaryDerivativeLevels + ", multiplicationLevels=" + multiplicationLevels
				+ ", auxiliaryMultiplicationLevels=" + auxiliaryMultiplicationLevels + ", equationTerms="
				+ equationTerms + ", auxiliaryEquations=" + auxiliaryFieldEquations + ", fluxTerms=" + fluxTerms
				+ ", sourceTerms=" + sourceTerms + ", evolutionFields=" + evolutionFields + ", derivativesProcessed="
				+ derivativesProcessed + ", stencil=" + stencil + ", accumulatedStencil=" + accumulatedStencil
				+ ", variablesToInterpolate=" + variablesToInterpolate + ", type=" + type + "]";
	}
    /**
     * Simplifies the operators information removing duplicate derivatives or multiplication terms.
     * @throws AGDMException 
     */
    private void simplify() throws AGDMException {
        //Order the derivatives and multiplication terms previous simplification
        this.order();
    	//Clean empty multiplicationTerms at the beginning
    	this.cleanMultiplicationTerms(multiplicationLevels, derivativeLevels);
    	this.cleanMultiplicationTerms(auxiliaryMultiplicationLevels, auxiliaryDerivativeLevels);
        //Simplificate while there are changes
        boolean change = true;
        while (change) {
            change = false;
            ArrayList<String> oldVars = new ArrayList<String>();
            ArrayList<String> newVars = new ArrayList<String>();
            //Reuse similar derivatives
            calculateReusability(oldVars, newVars, derivativeLevels, multiplicationLevels);
            if (oldVars.size() > 0) {
            	change = true;
            }
            //Replace variable usage
            replaceVariable(oldVars, newVars);
        }
        change = true;
        while (change) {
            change = false;
            ArrayList<String> oldVars = new ArrayList<String>();
            ArrayList<String> newVars = new ArrayList<String>();
            //Reuse similar derivatives 
            calculateReusability(oldVars, newVars, auxiliaryDerivativeLevels, auxiliaryMultiplicationLevels);
            if (oldVars.size() > 0) {
            	change = true;
            }
            //Replace variable usage
            replaceAuxiliaryVariable(oldVars, newVars);
         }
    }
    
    public void calculateReusability(ArrayList<String> oldVars, ArrayList<String> newVars, ArrayList<DerivativeLevel> ders, ArrayList<MultiplicationTermsLevel> mults) {
    	//Reuse similar derivatives
        for (int l = 0; l < ders.size(); l++) {
            DerivativeLevel dl = ders.get(l);
            ArrayList<Derivative> derivatives = dl.getDerivatives();
            for (int i = 0; i < derivatives.size(); i++) {
                Derivative d = derivatives.get(i);
               //Check for similar derivative usage in all levels
               for (int cl = l; cl < ders.size(); cl++) {
                   DerivativeLevel cdl = ders.get(cl);
                   ArrayList<Derivative> cderivatives = cdl.getDerivatives();
                   int limit = 0;
                   if (l == cl) {
                       limit = i + 1;
                   }
                   for (int j = cderivatives.size() - 1; j >= limit; j--) {
                       Derivative cd = cderivatives.get(j);
                       //Check different levels of equivalence
                       if (cd.getEquivalence(d) != Derivative.DIFFERENT) {
                    	   //If only one have a conditional keep the derivative unconditioned 
                           if (cd.getEquivalence(d) == Derivative.CONDITIONAL) {
                        	   d.setConditional(null);
                           }
                           //Remove derivative
                           cderivatives.remove(j);
                           oldVars.add(cd.getId());
                           newVars.add(d.getId()); 
                       }
                   }
               }
            }
        }
        for (int l = 0; l < mults.size(); l++) {
        	MultiplicationTermsLevel mtl = mults.get(l);
        	//Reuse similar multiplication terms
            ArrayList<MultiplicationTerm> mterms = mtl.getMultiplicationTerms();
            //Sort to have the auxiliary calculations at first
            Collections.sort(mterms);
            for (int i = 0; i < mterms.size(); i++) {
                MultiplicationTerm mt = mterms.get(i);
                int limit = i + 1;
                for (int j = mterms.size() - 1; j >= limit; j--) {
                    MultiplicationTerm cmt = mterms.get(j);
                    //When multiplication terms are equivalent
                    if (cmt.getEquivalence(mt) != MultiplicationTerm.DIFFERENT) {
                   	   //If only one have a conditional keep the derivative unconditioned 
                        if (cmt.getEquivalence(mt) == MultiplicationTerm.CONDITIONAL) {
                     	   mt.setConditional(null);
                        }
                        //Remove multiplication term
                        mterms.remove(j);
                        oldVars.add(cmt.getId());
                        newVars.add(mt.getId());
                    }
                }
            }
        }
    }
    
    /**
     * Replaces variables with a new ones where they are used.
     * @param oldVar    The variables to remove
     * @param newVar    The variables to set
     * @throws AGDMException 
     */
    public void replaceVariable(ArrayList<String> oldVar, ArrayList<String> newVar) throws AGDMException {
        //Replace in derivatives
        for (int l = 0; l < derivativeLevels.size(); l++) {
            DerivativeLevel dl = derivativeLevels.get(l);
            ArrayList<Derivative> derivatives = dl.getDerivatives();
            for (int i = 0; i < derivatives.size(); i++) {
                Derivative d = derivatives.get(i);
                if (d.getDerivativeTerm().isPresent()) {
                    NodeList occurrences = ((Element) d.getDerivativeTerm().get()).getElementsByTagName("mt:ci");
                    for (int j = 0; j < occurrences.getLength(); j++) {
                        int index = oldVar.indexOf(occurrences.item(j).getTextContent());
                        if (index >= 0) {
                            occurrences.item(j).setTextContent(newVar.get(index));
                        }
                    }
                }
                if (d.getAlgebraicExpression() != null) {
                    NodeList occurrences = ((Element) d.getAlgebraicExpression()).getElementsByTagName("mt:ci");
                    for (int j = 0; j < occurrences.getLength(); j++) {
                        int index = oldVar.indexOf(occurrences.item(j).getTextContent());
                        if (index >= 0) {
                            occurrences.item(j).setTextContent(newVar.get(index));
                        }
                    }
                }
            }
        }
        //Replace in multiplication terms
        for (int l = 0; l < multiplicationLevels.size(); l++) {
        	MultiplicationTermsLevel mtl = multiplicationLevels.get(l);
            ArrayList<MultiplicationTerm> mts = mtl.getMultiplicationTerms();
            for (int i = 0; i < mts.size(); i++) {
                ArrayList<String> variables = mts.get(i).getVariables();
                for (int j = 0; j < variables.size(); j++) {
                    int index = oldVar.indexOf(variables.get(j));
                    if (index >= 0) {
                        variables.set(j, newVar.get(index));
                    }
                }
                if (mts.get(i).getAlgebraicExpression() != null) {
                    NodeList occurrences = ((Element) mts.get(i).getAlgebraicExpression()).getElementsByTagName("mt:ci");
                    for (int j = 0; j < occurrences.getLength(); j++) {
                        int index = oldVar.indexOf(occurrences.item(j).getTextContent());
                        if (index >= 0) {
                            occurrences.item(j).setTextContent(newVar.get(index));
                        }
                    }
                }
            }
        }

        //Replace in equation terms
        ArrayList<EquationTerms> et = equationTerms.getEquations();
        for (int i = 0; i < et.size(); i++) {
            ArrayList<String> variables = et.get(i).getTerms();
            for (int j = 0; j < variables.size(); j++) {
                int index = oldVar.indexOf(variables.get(j));
                if (index >= 0) {
                    variables.set(j, newVar.get(index));
                }
            }
        }
    }
    
    /**
     * Replaces variables with a new ones where they are used.
     * @param oldVar    The variables to remove
     * @param newVar    The variables to set
     * @throws AGDMException 
     */
    public void replaceAuxiliaryVariable(ArrayList<String> oldVar, ArrayList<String> newVar) throws AGDMException {
        //Replace in derivatives
        for (int l = 0; l < auxiliaryDerivativeLevels.size(); l++) {
            DerivativeLevel dl = auxiliaryDerivativeLevels.get(l);
            ArrayList<Derivative> derivatives = dl.getDerivatives();
            for (int i = 0; i < derivatives.size(); i++) {
                Derivative d = derivatives.get(i);
                if (d.getDerivativeTerm().isPresent()) {
                    NodeList occurrences = ((Element) d.getDerivativeTerm().get()).getElementsByTagName("mt:ci");
                    for (int j = 0; j < occurrences.getLength(); j++) {
                        int index = oldVar.indexOf(occurrences.item(j).getTextContent());
                        if (index >= 0) {
                            occurrences.item(j).setTextContent(newVar.get(index));
                        }
                    }
                }
                if (d.getAlgebraicExpression() != null) {
                    NodeList occurrences = ((Element) d.getAlgebraicExpression()).getElementsByTagName("mt:ci");
                    for (int j = 0; j < occurrences.getLength(); j++) {
                        int index = oldVar.indexOf(occurrences.item(j).getTextContent());
                        if (index >= 0) {
                            occurrences.item(j).setTextContent(newVar.get(index));
                        }
                    }
                }
            }
        }
        //Replace in multiplication terms
        for (int l = 0; l < auxiliaryMultiplicationLevels.size(); l++) {
        	MultiplicationTermsLevel mtl = auxiliaryMultiplicationLevels.get(l);
            ArrayList<MultiplicationTerm> mts = mtl.getMultiplicationTerms();
            for (int i = 0; i < mts.size(); i++) {
                ArrayList<String> variables = mts.get(i).getVariables();
                for (int j = 0; j < variables.size(); j++) {
                    int index = oldVar.indexOf(variables.get(j));
                    if (index >= 0) {
                        variables.set(j, newVar.get(index));
                    }
                }
                if (mts.get(i).getAlgebraicExpression() != null) {
                    NodeList occurrences = ((Element) mts.get(i).getAlgebraicExpression()).getElementsByTagName("mt:ci");
                    for (int j = 0; j < occurrences.getLength(); j++) {
                        int index = oldVar.indexOf(occurrences.item(j).getTextContent());
                        if (index >= 0) {
                            occurrences.item(j).setTextContent(newVar.get(index));
                        }
                    }
                }
            }
        }

        //Replace in equation terms
        ArrayList<EquationTerms>  et = auxiliaryFieldEquations.getEquations();
        for (int i = 0; i < et.size(); i++) {
            ArrayList<String> variables = et.get(i).getTerms();
            for (int j = 0; j < variables.size(); j++) {
                int index = oldVar.indexOf(variables.get(j));
                if (index >= 0) {
                    variables.set(j, newVar.get(index));
                }
            }
        }
    }
    
    /**
     * Order derivatives and multiplication terms for a proper simplification.
     * Auxiliary terms must prevail since they are executed first.
     */
    private void order() {
        ArrayList<DerivativeLevel> newDerivativeLevels = new ArrayList<DerivativeLevel>();
        ArrayList<MultiplicationTermsLevel> newMultiplicationLevels = new ArrayList<MultiplicationTermsLevel>();
        //Derivative ordering
        for (int l = 0; l < derivativeLevels.size(); l++) {
            DerivativeLevel dl = derivativeLevels.get(l);
            ArrayList<Derivative> derivatives = dl.getDerivatives();
            ArrayList<Derivative> newDerivatives = new ArrayList<Derivative>();
            //First auxiliary
            for (int i = 0; i < derivatives.size(); i++) {
                Derivative d = derivatives.get(i);
                if (d.getOperatorType() == OperatorInfoType.auxiliaryVariable) {
                    newDerivatives.add(d);   
                }
            }
            //Second fields
            for (int i = 0; i < derivatives.size(); i++) {
                Derivative d = derivatives.get(i);
                if (d.getOperatorType() == OperatorInfoType.field || d.getOperatorType() == OperatorInfoType.analysisField) {
                    newDerivatives.add(d);   
                }
            }
            //Third interactions
            for (int i = 0; i < derivatives.size(); i++) {
                Derivative d = derivatives.get(i);
                if (d.getOperatorType() == OperatorInfoType.interaction) {
                    newDerivatives.add(d);   
                }
            }
            newDerivativeLevels.add(new DerivativeLevel(newDerivatives));
        }
        //Multiplication terms ordering
        for (int l = multiplicationLevels.size() - 1; l >= 0; l--) {
        	MultiplicationTermsLevel mtl = multiplicationLevels.get(l);
        	ArrayList<MultiplicationTerm> mts = mtl.getMultiplicationTerms();
            ArrayList<MultiplicationTerm> newMts = new ArrayList<MultiplicationTerm>();
            //First auxiliary
            for (int i = 0; i < mts.size(); i++) {
                if (mts.get(i).getOperatorType() == OperatorInfoType.auxiliaryVariable) {
                    newMts.add(mts.get(i));
                }
            }
            //Second fields
            for (int i = 0; i < mts.size(); i++) {
                if (mts.get(i).getOperatorType() == OperatorInfoType.field || mts.get(i).getOperatorType() == OperatorInfoType.analysisField) {
                    newMts.add(mts.get(i));
                }
            }
            //Third interactions
            for (int i = 0; i < mts.size(); i++) {
                if (mts.get(i).getOperatorType() == OperatorInfoType.interaction) {
                    newMts.add(mts.get(i));
                }
            }
            newMultiplicationLevels.add(new MultiplicationTermsLevel(newMts));
        }
        derivativeLevels = newDerivativeLevels;
        multiplicationLevels = newMultiplicationLevels;

        //Auxiliary Multiplication terms ordering
        newMultiplicationLevels = new ArrayList<MultiplicationTermsLevel>();
        for (int l = auxiliaryMultiplicationLevels.size() - 1; l >= 0; l--) {
        	MultiplicationTermsLevel mtl = auxiliaryMultiplicationLevels.get(l);
        	ArrayList<MultiplicationTerm> mts = mtl.getMultiplicationTerms();
            ArrayList<MultiplicationTerm> newMts = new ArrayList<MultiplicationTerm>();
            //First auxiliary
            for (int i = 0; i < mts.size(); i++) {
                    newMts.add(mts.get(i));
            }
            newMultiplicationLevels.add(new MultiplicationTermsLevel(newMts));
        }
        auxiliaryMultiplicationLevels = newMultiplicationLevels;
    }
    
    public int getStencil() {
    	return stencil;
    }
   
    /**
     * Clean empty derivative levels.
     */
    public void cleanDerivativeLevels( ArrayList<DerivativeLevel> der) {
    	for (int i = der.size() - 1; i >= 0; i--) {
    		if (der.get(i).getDerivatives().size() == 0) {
    			der.remove(i);
    		}
    	}
    }
    
    /**
     * Clean multiplication terms.
     */
    public void cleanMultiplicationTerms(ArrayList<MultiplicationTermsLevel> mult, ArrayList<DerivativeLevel> der) {
    	if (mult.size() > der.size()) {
    		int multSize = mult.size();
    		for (int i = 0; i < multSize - der.size(); i++) {
    			mult.remove(0);
    		}
    	}
    }
    
    /**
     * Adds the local multiplication terms level in an reverse order to the global multiplication terms level.
     * @param globalMTL							multiplication terms levels
     * @param localMultiplicationTermsLevels	local multiplication terms levels
     */
    public void addReverseLocalMultiplicationTermsLevel(ArrayList<MultiplicationTermsLevel> globalMTL, ArrayList<MultiplicationTermsLevel> localMultiplicationTermsLevels) {
    	int multiplicationLevelsIndex = 0;
    	for (int i = localMultiplicationTermsLevels.size() - 1; i >= 0; i--) {
    		if (globalMTL.size() <= multiplicationLevelsIndex) {
    			globalMTL.add(new MultiplicationTermsLevel());
    			
    		}
    		globalMTL.get(multiplicationLevelsIndex).getMultiplicationTerms().addAll(localMultiplicationTermsLevels.get(i).getMultiplicationTerms());
    		multiplicationLevelsIndex++;
    	}
    }
    
    /**
     * Remove auxiliary variables not appearing as dependencies from the input variables.
     * @param dependentVariables		The dependent variables
     * @param onlyAuxiliaryVariables	Keep only auxiliary variables
     * @throws AGDMException			AGDM00X - External error
     */
    public void onlyDependent(HashSet<String> dependentVariables, boolean onlyAuxiliaryVariables, Node conditional) throws AGDMException {
    	   	
    	HashSet<String> variables = new LinkedHashSet<String>(dependentVariables);
    	HashSet<String> alreadyAdded = new LinkedHashSet<String>();
    	ArrayList<DerivativeLevel> newDerivativeLevels = new ArrayList<DerivativeLevel>();
    	for (int l = 0; l < derivativeLevels.size(); l++) {
    		newDerivativeLevels.add(new DerivativeLevel());
    	}
    	ArrayList<MultiplicationTermsLevel> newMultiplicationLevels = new ArrayList<MultiplicationTermsLevel>();
    	for (int l = 0; l < multiplicationLevels.size(); l++) {
    		newMultiplicationLevels.add(new MultiplicationTermsLevel());
    	}
    	EquationTermsLevel newEquationTerms = new  EquationTermsLevel();
    	    	
    	int oldEquationTerms = -1;
    	while (newEquationTerms.getEquations().size() != oldEquationTerms) {
    		oldEquationTerms = newEquationTerms.getEquations().size();
	    	//Remove auxiliary variable equation terms not appearing in the dependent terms
	    	ArrayList<EquationTerms> eqs = equationTerms.getEquations();
	    	for (int i = 0; i < eqs.size(); i++) {
    			ArrayList<String> auxiliaryVariables = eqs.get(i).getFields();
    			for (String auxiliaryVariable : auxiliaryVariables) {
    	    		if (eqs.get(i).getOperatorType() == OperatorInfoType.auxiliaryVariable) {
    	    			//Add dependent equation term
    	    			if (!alreadyAdded.contains(auxiliaryVariable + "_" + eqs.get(i).getOperatorType()) && variables.contains(auxiliaryVariable)) {
    	    				EquationTerms copy = new EquationTerms(eqs.get(i));
    	    				if (conditional != null) {
    	    					copy.setConditional(conditional);
    	    				}
    	    				newEquationTerms.add(copy);
    	    				variables.addAll(eqs.get(i).getTerms());
    	    				if (eqs.get(i).getAlgorithm() != null) {
				    			NodeList vars = ((Element) eqs.get(i).getAlgorithm()).getElementsByTagName("mt:ci");
				    			for (int j = 0; j < vars.getLength(); j++) {
				    				variables.add(vars.item(j).getTextContent());
				    			}
		    				}
    	    				alreadyAdded.add(auxiliaryVariable + "_" + eqs.get(i).getOperatorType());
    	    			}
    	    		}
    	    		else {
    	    			if (!onlyAuxiliaryVariables && !alreadyAdded.contains(auxiliaryVariable + "_" + eqs.get(i).getOperatorType())) {
    						newEquationTerms.add(eqs.get(i));
    	    				variables.addAll(eqs.get(i).getTerms());
    	    				alreadyAdded.add(auxiliaryVariable + "_" + eqs.get(i).getOperatorType());
    	    				
    	    			}
    	    		}
    			}
	    	}	    	
	    	//Remove multiplication terms not appearing in the dependent terms.
	    	for (int l = 0; l < multiplicationLevels.size(); l++) {
		    	ArrayList<MultiplicationTerm> multTerms = multiplicationLevels.get(l).getMultiplicationTerms();
		    	MultiplicationTermsLevel newMultTermsLevel = newMultiplicationLevels.get(l);
		    	for (int i = 0; i < multTerms.size(); i++) {
		    		String auxiliaryVariable = multTerms.get(i).getId();
		    		if (multTerms.get(i).getOperatorType() == OperatorInfoType.auxiliaryVariable) {
		    			//Remove equation terms whose variable does not appears in dependent terms
		    			if (!alreadyAdded.contains(auxiliaryVariable) && variables.contains(auxiliaryVariable)) {
		    				variables.addAll(multTerms.get(i).getVariables());
		    				MultiplicationTerm copy = new MultiplicationTerm(multTerms.get(i));
		    				if (conditional != null) {
    	    					copy.setConditional(conditional);
    	    				}
		    				newMultTermsLevel.add(copy);
		    				alreadyAdded.add(auxiliaryVariable);
			    			NodeList vars = ((Element) multTerms.get(i).getAlgebraicExpression()).getElementsByTagName("mt:ci");
			    			for (int j = 0; j < vars.getLength(); j++) {
			    				variables.add(vars.item(j).getTextContent());
			    			}
		    			}
		    		}
		    		else {
		    			if (!onlyAuxiliaryVariables && !alreadyAdded.contains(auxiliaryVariable)) {
			 				variables.addAll(multTerms.get(i).getVariables());
		    				newMultTermsLevel.add(multTerms.get(i));
		    				alreadyAdded.add(auxiliaryVariable);
		    			}
		    		}
		    	}
	    	}
	    	//Remove derivative terms not appearing in the dependent terms.
	    	for (int l = 0; l < derivativeLevels.size(); l++) {
		    	ArrayList<Derivative> derivatives = derivativeLevels.get(l).getDerivatives();
		    	DerivativeLevel newDerLevel = newDerivativeLevels.get(l);
		    	for (int i = 0; i < derivatives.size(); i++) {
		    		String auxiliaryVariable = derivatives.get(i).getId();
		    		if (derivatives.get(i).getOperatorType() == OperatorInfoType.auxiliaryVariable) {
		    			//Remove equation terms whose variable does not appears in dependent terms
		    			if (!alreadyAdded.contains(auxiliaryVariable) && variables.contains(auxiliaryVariable)) {
		    				Derivative copy = new Derivative(derivatives.get(i));
		    				if (conditional != null) {
    	    					copy.setConditional(conditional);
    	    				}
		    				newDerLevel.add(copy);
		    				alreadyAdded.add(auxiliaryVariable);
		    				if (derivatives.get(i).getAlgebraicExpression() != null) {
				    			NodeList vars = ((Element) derivatives.get(i).getAlgebraicExpression()).getElementsByTagName("mt:ci");
				    			for (int j = 0; j < vars.getLength(); j++) {
				    				variables.add(vars.item(j).getTextContent());
				    			}
		    				}
		    				if (derivatives.get(i).getDerivativeTerm().isPresent()) {
			    				NodeList vars = ((Element) derivatives.get(i).getDerivativeTerm().get()).getElementsByTagName("mt:ci");
				    			for (int j = 0; j < vars.getLength(); j++) {
				    				variables.add(vars.item(j).getTextContent());
				    			}
		    				}
		    			}
		    		}
		    		else {
	    				if (!onlyAuxiliaryVariables && !alreadyAdded.contains(auxiliaryVariable)) {
		    				newDerLevel.add(derivatives.get(i));
		    				alreadyAdded.add(auxiliaryVariable);
	    				}
	    			}
		    	}
	    	}
    	}
    	if (onlyAuxiliaryVariables) {
    		newEquationTerms = AGDMUtils.orderEquationTermDependencies(newDerivativeLevels, newMultiplicationLevels, newEquationTerms);
    	}
    	this.derivativeLevels = newDerivativeLevels;
    	this.multiplicationLevels = newMultiplicationLevels;
    	this.equationTerms = newEquationTerms;
    }
    
    /**
     * Remove auxiliary variables not appearing as dependencies from the input variables.
     * @param dependentVariables	The dependent variables
     * @throws AGDMException		AGDM00X - External error
     */
    public void onlyDependentAuxFields(HashSet<String> dependentVariables, boolean onlyAuxiliaries) throws AGDMException {
    	HashSet<String> variables = new LinkedHashSet<String>(dependentVariables);
    	HashSet<String> alreadyAdded = new LinkedHashSet<String>();
    	ArrayList<DerivativeLevel> newDerivativeLevels = new ArrayList<DerivativeLevel>();
    	for (int l = 0; l < auxiliaryDerivativeLevels.size(); l++) {
    		newDerivativeLevels.add(new DerivativeLevel());
    	}
    	ArrayList<MultiplicationTermsLevel> newMultiplicationLevels = new ArrayList<MultiplicationTermsLevel>();
    	for (int l = 0; l < auxiliaryMultiplicationLevels.size(); l++) {
    		newMultiplicationLevels.add(new MultiplicationTermsLevel());
    	}
    	EquationTermsLevel newEquationTerms = new  EquationTermsLevel();
    	
    	int oldEquationTerms = -1;
    	while (newEquationTerms.getEquations().size() != oldEquationTerms) {
    		oldEquationTerms = newEquationTerms.getEquations().size();
	    	//Remove auxiliary variable equation terms not appearing in the dependent terms
	    	ArrayList<EquationTerms> eqs = equationTerms.getEquations();
	    	for (int i = 0; i < eqs.size(); i++) {
	    		ArrayList<String> auxiliaryVariables = eqs.get(i).getFields();
    			for (String auxiliaryVariable : auxiliaryVariables) {
		    		if (eqs.get(i).getOperatorType() == OperatorInfoType.auxiliaryVariable) {
		    			//Add dependent equation term
		    			if (!alreadyAdded.contains(auxiliaryVariable + "_" + eqs.get(i).getOperatorType()) && variables.contains(auxiliaryVariable)) {
		    				newEquationTerms.add(eqs.get(i));
		    				variables.addAll(eqs.get(i).getTerms());
		    				if (eqs.get(i).getAlgorithm() != null) {
				    			NodeList vars = ((Element) eqs.get(i).getAlgorithm()).getElementsByTagName("mt:ci");
				    			for (int j = 0; j < vars.getLength(); j++) {
				    				variables.add(vars.item(j).getTextContent());
				    			}
		    				}
		    				alreadyAdded.add(auxiliaryVariable + "_" + eqs.get(i).getOperatorType());
		    			}
		    		}
		    		else {
		    			if (!onlyAuxiliaries && !alreadyAdded.contains(auxiliaryVariable + "_" + eqs.get(i).getOperatorType())) {
							newEquationTerms.add(eqs.get(i));
		    				variables.addAll(eqs.get(i).getTerms());
		    				alreadyAdded.add(auxiliaryVariable + "_" + eqs.get(i).getOperatorType());
		    				
		    			}
		    		}
    			}
	    	}
	    	//Remove multiplication terms not appearing in the dependent terms.
	    	for (int l = 0; l < auxiliaryMultiplicationLevels.size(); l++) {
		    	ArrayList<MultiplicationTerm> multTerms = auxiliaryMultiplicationLevels.get(l).getMultiplicationTerms();
		    	MultiplicationTermsLevel newMultTermsLevel = newMultiplicationLevels.get(l);
		    	for (int i = 0; i < multTerms.size(); i++) {
		    		String auxiliaryVariable = multTerms.get(i).getId();
		    		if (multTerms.get(i).getOperatorType() == OperatorInfoType.auxiliaryVariable) {
		    			//Remove equation terms whose variable does not appears in dependent terms
		    			if (!alreadyAdded.contains(auxiliaryVariable) && variables.contains(auxiliaryVariable)) {
		    				variables.addAll(multTerms.get(i).getVariables());
		    				newMultTermsLevel.add(multTerms.get(i));
		    				alreadyAdded.add(auxiliaryVariable);
			    			NodeList vars = ((Element) multTerms.get(i).getAlgebraicExpression()).getElementsByTagName("mt:ci");
			    			for (int j = 0; j < vars.getLength(); j++) {
			    				variables.add(vars.item(j).getTextContent());
			    			}
		    			}
		    		}
		    		else {
		    			if (!onlyAuxiliaries && !alreadyAdded.contains(auxiliaryVariable)) {
			 				variables.addAll(multTerms.get(i).getVariables());
		    				newMultTermsLevel.add(multTerms.get(i));
		    				alreadyAdded.add(auxiliaryVariable);
		    			}
		    		}
		    	}
	    	}
	    	//Remove derivative terms not appearing in the dependent terms.
	    	for (int l = 0; l < auxiliaryDerivativeLevels.size(); l++) {
		    	ArrayList<Derivative> derivatives = auxiliaryDerivativeLevels.get(l).getDerivatives();
		    	DerivativeLevel newDerLevel = newDerivativeLevels.get(l);
		    	for (int i = 0; i < derivatives.size(); i++) {
		    		String auxiliaryVariable = derivatives.get(i).getId();
		    		if (derivatives.get(i).getOperatorType() == OperatorInfoType.auxiliaryVariable) {
		    			//Remove equation terms whose variable does not appears in dependent terms
		    			if (!alreadyAdded.contains(auxiliaryVariable) && variables.contains(auxiliaryVariable)) {
		    				newDerLevel.add(derivatives.get(i));
		    				alreadyAdded.add(auxiliaryVariable);
		    				if (derivatives.get(i).getAlgebraicExpression() != null) {
				    			NodeList vars = ((Element) derivatives.get(i).getAlgebraicExpression()).getElementsByTagName("mt:ci");
				    			for (int j = 0; j < vars.getLength(); j++) {
				    				variables.add(vars.item(j).getTextContent());
				    			}
		    				}
		    				if (derivatives.get(i).getDerivativeTerm().isPresent()) {
			    				NodeList vars = ((Element) derivatives.get(i).getDerivativeTerm().get()).getElementsByTagName("mt:ci");
				    			for (int j = 0; j < vars.getLength(); j++) {
				    				variables.add(vars.item(j).getTextContent());
				    			}
		    				}
		    			}
		    		}
		    		else {
	    				if (!onlyAuxiliaries && !alreadyAdded.contains(auxiliaryVariable)) {
		    				newDerLevel.add(derivatives.get(i));
		    				alreadyAdded.add(auxiliaryVariable);
	    				}
	    			}
		    	}
	    	}
    	}
    	
    	if (onlyAuxiliaries) {
    		newEquationTerms = AGDMUtils.orderEquationTermDependencies(newDerivativeLevels, newMultiplicationLevels, newEquationTerms);
        	this.equationTerms = newEquationTerms;
    	}
    	this.auxiliaryDerivativeLevels = newDerivativeLevels;
    	this.auxiliaryMultiplicationLevels = newMultiplicationLevels;
    }
    
    /**
     * Get variables used in fluxes.
     * @return					Variables
     * @throws AGDMException	AGDM00X - External error
     */
    public HashSet<String> getFluxVariables() throws AGDMException {
    	HashSet<String> variables = new LinkedHashSet<String>();
    	ArrayList<Flux> fluxes = fluxTerms.getFluxes();
    	for (int i = 0; i < fluxes.size(); i++) {
    		NodeList vars = ((Element) fluxes.get(i).getMath()).getElementsByTagName("mt:ci");
    		for (int j = 0; j < vars.getLength(); j++) {
    			variables.add(vars.item(j).getTextContent());
    		}
    	}
    	return variables;
    }
    
    
    /**
     * Get common conditional (if any) from variables used in fluxes.
     * @return					Common conditional or null
     */
    public Node getFluxCommonConditional() throws AGDMException {
    	Node conditional = null;
    	boolean first = true;
    	ArrayList<Flux> fluxes = fluxTerms.getFluxes();
    	for (int i = 0; i < fluxes.size(); i++) {
    		if (first) {
				conditional = fluxes.get(i).getConditional();
				first = false;
			}
    		else {
    			if((fluxes.get(i).getConditional() == null && conditional != null)
    					|| (fluxes.get(i).getConditional() != null && conditional == null)
    					|| (fluxes.get(i).getConditional() != null && conditional != null && !fluxes.get(i).getConditional().isEqualNode(conditional))
    					) {
					//One conditional is different, so no common conditional exists
					return null;
				}
    		}
    	}
    	return conditional;
    }
    
    /**
     * Get common conditional (if any) from variables used in sources and non conservative terms.
     * @return					Common conditional or null
     */
    public Node getNonConservativeSourceCommonConditional() {
    	Node conditional = null;
    	boolean first = true;
    	ArrayList<EquationTerms> eqs = equationTerms.getEquations();
    	for (int i = 0; i < eqs.size(); i++) {
    		if (first) {
				conditional = eqs.get(i).getConditional();
				first = false;
			}
    		else {
    			if((eqs.get(i).getConditional() == null && conditional != null)
    					|| (eqs.get(i).getConditional() != null && conditional == null)
    					|| (eqs.get(i).getConditional() != null && conditional != null && !eqs.get(i).getConditional().isEqualNode(conditional))
    					) {
					//One conditional is different, so no common conditional exists
					return null;
				}
    		}
    	}
    	return conditional;
    }
    
    /**
     * Get common conditional (if any) from variables used in sources and non conservative terms.
     * @return					Common conditional or null
     */
    public Node getConservativeSourceCommonConditional() {
    	Node conditional = null;
    	boolean first = true;
    	
    	if (derivativeLevels.size() > 0) {
	        ArrayList<Derivative> derivatives = derivativeLevels.get(derivativeLevels.size() - 1).getDerivatives();
	        for (int i = 0; i < derivatives.size(); i++) {
	            if (derivatives.get(i).getOperatorType() == OperatorInfoType.field 
	            		|| derivatives.get(i).getOperatorType() == OperatorInfoType.analysisField
	    				|| derivatives.get(i).getOperatorType() == OperatorInfoType.interaction) {
	                Optional<Node> derivative = derivatives.get(i).getDerivativeTerm();
	                //Sources term
	                if (!derivative.isPresent()) {
	              		if (first) {
	    					conditional = derivatives.get(i).getConditional();
	    					first = false;
	    				}
	    	    		else {
	    	    			if((derivatives.get(i).getConditional() == null && conditional != null)
	    	    					|| (derivatives.get(i).getConditional() != null && conditional == null)
	    	    					|| (derivatives.get(i).getConditional() != null && conditional != null && !derivatives.get(i).getConditional().isEqualNode(conditional))
	    	    					) {
	    						//One conditional is different, so no common conditional exists
	    						return null;
	    					}
	    	    		}
	                }
	            }
	        }
    	}
    	return conditional;
    }
    
    /**
     * Get variables used in derivative levels.
     * @return					Variables
     * @throws AGDMException	AGDM00X - External error
     */
    public HashSet<String> getDerivativeVariables(boolean onlyFields) throws AGDMException {
    	HashSet<String> variables = new LinkedHashSet<String>();
    	for (int l = 0; l < derivativeLevels.size(); l++) {
	    	ArrayList<Derivative> derivatives = derivativeLevels.get(l).getDerivatives();
	    	for (int i = 0; i < derivatives.size(); i++) {
	    		Derivative d = derivatives.get(i);
	    		if (!onlyFields || derivatives.get(i).getOperatorType() == OperatorInfoType.field || derivatives.get(i).getOperatorType() == OperatorInfoType.analysisField) {
		    		if (d.getAlgebraicExpression() != null) {
			    		NodeList vars = ((Element) d.getAlgebraicExpression()).getElementsByTagName("mt:ci");
			    		for (int j = 0; j < vars.getLength(); j++) {
			    			variables.add(vars.item(j).getTextContent());
			    		}
		    		}
	    		}
	    	}
    	}
    	return variables;
    }
    
    /**
     * Get variables used in multipliation terms.
     * @return					Variables
     * @throws AGDMException	AGDM00X - External error
     */
    public HashSet<String> getMultiplicationVariables(boolean onlyFields) throws AGDMException {
    	HashSet<String> variables = new LinkedHashSet<String>();
    	for (int l = multiplicationLevels.size() - 1; l >= 0; l--) {
	    	ArrayList<MultiplicationTerm> multTerms = multiplicationLevels.get(l).getMultiplicationTerms();
	    	for (int i = 0; i < multTerms.size(); i++) {
	    		if (!onlyFields || multTerms.get(i).getOperatorType() == OperatorInfoType.field || multTerms.get(i).getOperatorType() == OperatorInfoType.analysisField) {
		    		if (multTerms.get(i).getAlgebraicExpression() != null) {
		    			NodeList vars = ((Element) multTerms.get(i).getAlgebraicExpression()).getElementsByTagName("mt:ci");
			    		for (int j = 0; j < vars.getLength(); j++) {
			    			variables.add(vars.item(j).getTextContent());
			    		}
		    		}
	    		}
	    	}
    	}
    	return variables;
    }
    
    /**
     * Get variables used in multipliation terms.
     * @return					Variables
     * @throws AGDMException	AGDM00X - External error
     */
    public HashSet<String> getMaxCharSpeedVariables(Node problem) throws AGDMException {
    	HashSet<String> variables = new LinkedHashSet<String>();
		NodeList vars = AGDMUtils.find(problem, "//mms:characteristicDecomposition//mt:ci");
		for (int j = 0; j < vars.getLength(); j++) {
			variables.add(vars.item(j).getTextContent());
		}
    	return variables;
    }
    
    /**
     * Get variables used in all species velocities.
     * @return					Variables
     * @throws AGDMException	AGDM00X - External error
     */
    public HashSet<String> getVelocityVariables(DiscretizationPolicy policy) throws AGDMException {
    	HashSet<String> variables = new LinkedHashSet<String>();
    	for (Species s: policy.getParticleSpecies().values()) {
    		for (Element vel: s.getParticleVelocities().values()) {
    			NodeList vars = vel.getElementsByTagName("mt:ci");
	    		for (int j = 0; j < vars.getLength(); j++) {
	    			variables.add(vars.item(j).getTextContent());
	    		}
    		}
    	}
    	return variables;
    }
    
    /**
     * Get variables used in particle velocities given.
     * @return					Variables
     * @throws AGDMException	AGDM00X - External error
     */
    public HashSet<String> getVelocityVariables(Collection<Element> velocities) throws AGDMException {
    	HashSet<String> variables = new LinkedHashSet<String>();
		for (Element vel: velocities) {
			NodeList vars = vel.getElementsByTagName("mt:ci");
    		for (int j = 0; j < vars.getLength(); j++) {
    			variables.add(vars.item(j).getTextContent());
    		}
		}
    	return variables;
    }
    
    
    /**
     * Get variables used in fluxes from a given field.
     * @param field				The field to filter
     * @return					Variables
     * @throws AGDMException	AGDM00X - External error
     */
    public HashSet<String> getFluxVariables(String field) throws AGDMException {
    	HashSet<String> variables = new LinkedHashSet<String>();
    	ArrayList<Flux> fluxes = fluxTerms.getFluxes();
    	for (int i = 0; i < fluxes.size(); i++) {
    		if (fluxes.get(i).getField().equals(field)) {
	    		NodeList vars = fluxes.get(i).getMath().getElementsByTagName("mt:ci");
	    		for (int j = 0; j < vars.getLength(); j++) {
	    			variables.add(vars.item(j).getTextContent());
	    		}
    		}
    	}
    	return variables;
    }
    
    /**
     * Get conditional used in fluxes from a given field.
     * @param field				The field to filter
     * @return					Conditional, if any
     * @throws AGDMException	AGDM00X - External error
     */
    public Node getFluxConditional(String field) throws AGDMException {
    	ArrayList<Flux> fluxes = fluxTerms.getFluxes();
    	for (int i = 0; i < fluxes.size(); i++) {
    		if (fluxes.get(i).getField().equals(field)) {
	    		return fluxes.get(i).getConditional();
    		}
    	}
    	return null;
    }
    
    /**
     * Get variables used in conservative sources.
     * @return					Variables
     * @throws AGDMException	AGDM00X - External error
     */
    public HashSet<String> getConservativeSourceVariables() throws AGDMException {
    	HashSet<String> variables = new LinkedHashSet<String>();
    	ArrayList<Source> sources = sourceTerms.getSources();
    	for (int i = 0; i < sources.size(); i++) {
    		NodeList vars = sources.get(i).getFormula().getElementsByTagName("mt:ci");
    		for (int j = 0; j < vars.getLength(); j++) {
    			variables.add(vars.item(j).getTextContent());
    		}
    	}
    	return variables;
    }
    
    /**
     * Get variables used in stiff sources.
     * @return					Variables
     * @throws AGDMException	AGDM00X - External error
     */
    public HashSet<String> getStiffSourceVariables() throws AGDMException {
    	HashSet<String> variables = new LinkedHashSet<String>();
    	ArrayList<StiffSource> stiffSources = stiffSourceTerms.getStiffSources();
    	for (int i = 0; i < stiffSources.size(); i++) {
    		NodeList vars = stiffSources.get(i).getFormula().getElementsByTagName("mt:ci");
    		for (int j = 0; j < vars.getLength(); j++) {
    			variables.add(vars.item(j).getTextContent());
    		}
    	}
    	return variables;
    }
    
    /**
     * Get variables used in non conservative sources.
     * @return					Variables
     * @throws AGDMException	AGDM00X - External error
     */
    public HashSet<String> getNonConservativeSourceVariables() throws AGDMException {
    	HashSet<String> variables = new LinkedHashSet<String>();
    	if (derivativeLevels.size() > 0) {
	        ArrayList<Derivative> derivatives = derivativeLevels.get(derivativeLevels.size() - 1).getDerivatives();
	        for (int i = 0; i < derivatives.size(); i++) {
	            if (derivatives.get(i).getOperatorType() == OperatorInfoType.field 
	            		|| derivatives.get(i).getOperatorType() == OperatorInfoType.analysisField
	    				|| derivatives.get(i).getOperatorType() == OperatorInfoType.interaction) {
	                Node algebraic = derivatives.get(i).getAlgebraicExpression();
	                Optional<Node> derivative = derivatives.get(i).getDerivativeTerm();
	                //Sources term
	                if (!derivative.isPresent()) {
	            		NodeList vars = ((Element) algebraic).getElementsByTagName("mt:ci");
		    			for (int j = 0; j < vars.getLength(); j++) {
		    				variables.add(vars.item(j).getTextContent());
		    			}
	                }
	            }
	        }
    	}
    	return variables;
    }
    
    /**
     * Get variables used in non conservative terms.
     * @return					Variables
     * @throws AGDMException	AGDM00X - External error
     */
    public HashSet<String> getNonConservativeFields() throws AGDMException {
    	HashSet<String> variables = new LinkedHashSet<String>();
    	if (derivativeLevels.size() > 0) {
	        ArrayList<Derivative> derivatives = derivativeLevels.get(derivativeLevels.size() - 1).getDerivatives();
	        for (int i = 0; i < derivatives.size(); i++) {
	            if (derivatives.get(i).getOperatorType() == OperatorInfoType.field 
	            		|| derivatives.get(i).getOperatorType() == OperatorInfoType.analysisField
	    				|| derivatives.get(i).getOperatorType() == OperatorInfoType.interaction) {
	                Node algebraic = derivatives.get(i).getAlgebraicExpression();
	                Optional<Node> derivative = derivatives.get(i).getDerivativeTerm();
	                //Sources term
	                if (!derivative.isPresent()) {
	            		NodeList vars = ((Element) algebraic).getElementsByTagName("mt:ci");
		    			for (int j = 0; j < vars.getLength(); j++) {
		    				variables.add(vars.item(j).getTextContent());
		    			}
	                }
	            }
	        }
    	}
    	for (int l = multiplicationLevels.size() - 1; l >= 0; l--) {
	    	ArrayList<MultiplicationTerm> multTerms = multiplicationLevels.get(l).getMultiplicationTerms();
	    	for (int i = 0; i < multTerms.size(); i++) {
	    		if (multTerms.get(i).getOperatorType() == OperatorInfoType.field 
	    				|| multTerms.get(i).getOperatorType() == OperatorInfoType.analysisField
	    				|| multTerms.get(i).getOperatorType() == OperatorInfoType.interaction) {
	    			variables.addAll(multTerms.get(i).getVariables());
	    			if (multTerms.get(i).getAlgebraicExpression() != null) {
		    			NodeList vars = ((Element) multTerms.get(i).getAlgebraicExpression()).getElementsByTagName("mt:ci");
		    			for (int j = 0; j < vars.getLength(); j++) {
		    				variables.add(vars.item(j).getTextContent());
		    			}
	    			}
	    		}
	    	}
    	}
    	ArrayList<EquationTerms> eqs = equationTerms.getEquations();
    	for (int i = 0; i < eqs.size(); i++) {
    		if (eqs.get(i).getOperatorType() == OperatorInfoType.field || eqs.get(i).getOperatorType() == OperatorInfoType.analysisField || eqs.get(i).getOperatorType() == OperatorInfoType.interaction) {
    			variables.addAll(eqs.get(i).getTerms());
    		}
    	}
    	return variables;
    }
    
    /**
     * Get common conditional (if any) from variables used in non conservative terms.
     * @return					Common conditional or null
     */
    public Node getNonConservativeCommonConditional() {
    	Node conditional = null;
    	boolean first = true;
    	ArrayList<EquationTerms> eqs = equationTerms.getEquations();
    	for (int i = 0; i < eqs.size(); i++) {
    		if (eqs.get(i).getOperatorType() == OperatorInfoType.field || eqs.get(i).getOperatorType() == OperatorInfoType.analysisField || eqs.get(i).getOperatorType() == OperatorInfoType.interaction) {
    			if (first) {
    				conditional = eqs.get(i).getConditional();
    				first = false;
    			}
    			else {
    				if((eqs.get(i).getConditional() == null && conditional != null)
        					|| (eqs.get(i).getConditional() != null && conditional == null)
        					|| (eqs.get(i).getConditional() != null && conditional != null && !eqs.get(i).getConditional().isEqualNode(conditional))
        					) {
    					//One conditional is different, so no common conditional exists
    					return null;
    				}
    			}
    		}
    	}
    	return conditional;
    }
    
    /**
     * Get variables used in auxiliary equations.
     * @return					Variables
     * @throws AGDMException	AGDM00X - External error
     */
    public HashSet<String> getAuxiliaryFieldAlgorithmVariables() throws AGDMException {
    	HashSet<String> variables = new LinkedHashSet<String>();
    	ArrayList<EquationTerms> eqs = auxiliaryFieldEquations.getEquations();
    	for (int i = 0; i < eqs.size(); i++) {
    		if (eqs.get(i).getAlgorithm() != null) {
	    		NodeList vars = ((Element) eqs.get(i).getAlgorithm()).getElementsByTagName("mt:ci");
				for (int j = 0; j < vars.getLength(); j++) {
					variables.add(vars.item(j).getTextContent());
				}
    		}
    	}
    	return variables;
    }
    
    
    /**
     * Get common conditional (if any) from variables used in non derivative dependent auxiliary fields.
     * @return					Common conditional or null
     */
    public Node getNonDerivativeDependentAuxiliaryFieldCommonConditional() throws AGDMException {
    	Node conditional = null;
    	boolean first = true;
    	HashSet<String> derDependentVars = getAuxFieldDerivativeDependent();
    	ArrayList<EquationTerms> eqs = auxiliaryFieldEquations.getEquations();
    	for (int i = 0; i < eqs.size(); i++) {
    		if (eqs.get(i).getEquationType() == EquationType.algorithmic && !derDependentVars.contains(eqs.get(i).getFields())) {
        		if (first) {
    				conditional = eqs.get(i).getConditional();
    				first = false;
    			}
        		else {
        			if((eqs.get(i).getConditional() == null && conditional != null)
        					|| (eqs.get(i).getConditional() != null && conditional == null)
        					|| (eqs.get(i).getConditional() != null && conditional != null && !eqs.get(i).getConditional().isEqualNode(conditional))
        					) {
    					//One conditional is different, so no common conditional exists
    					return null;
    				}
        		}
    		}
    	}
    	return conditional;
    }
    
    /**
     * Get common conditional (if any) from variables used in non derivative dependent auxiliary fields.
     * PRECONDITION				Current operationsInfo has removed non derivative dependent auxiliary fields 
     * @return					Common conditional or null
     */
    public Node getDerivativeDependentAuxiliaryFieldCommonConditional() throws AGDMException {
    	Node conditional = null;
    	boolean first = true;
    	ArrayList<EquationTerms> eqs = auxiliaryFieldEquations.getEquations();
    	for (int i = 0; i < eqs.size(); i++) {
    		if (first) {
				conditional = eqs.get(i).getConditional();
				first = false;
			}
    		else {
    			if((eqs.get(i).getConditional() == null && conditional != null)
    					|| (eqs.get(i).getConditional() != null && conditional == null)
    					|| (eqs.get(i).getConditional() != null && conditional != null && !eqs.get(i).getConditional().isEqualNode(conditional))
    					) {
					//One conditional is different, so no common conditional exists
					return null;
				}
    		}
    	}
    	return conditional;
    }
    
    /**
     * Get common conditional (if any) from variables used in algorithmic auxiliary fields.
     * @return					Common conditional or null
     */
    public Node getAuxiliaryFieldAlgorithmCommonConditional(ArrayList<String> fields) throws AGDMException {
    	Node conditional = null;
    	boolean first = true;
    	ArrayList<EquationTerms> eqs = auxiliaryFieldEquations.getEquations();
    	for (int i = 0; i < eqs.size(); i++) {
    		if (eqs.get(i).getAlgorithm() != null && (fields.isEmpty() || fields.contains(eqs.get(i).getFields()))) {
        		if (first) {
    				conditional = eqs.get(i).getConditional();
    				first = false;
    			}
        		else {
        			if((eqs.get(i).getConditional() == null && conditional != null)
        					|| (eqs.get(i).getConditional() != null && conditional == null)
        					|| (eqs.get(i).getConditional() != null && conditional != null && !eqs.get(i).getConditional().isEqualNode(conditional))
        					) {
    					//One conditional is different, so no common conditional exists
    					return null;
    				}
        		}
    		}
    	}
    	return conditional;
    }
    
    public DiscretizationType getType() {
		return type;
	}
    
    /**
     * Checks if a term is a source term.
     * @param variable	The variable term
     * @return			True if sources
     */
    public boolean isSourceTerm(String variable) {
    	if (derivativeLevels.size() > 0) {
    		DerivativeLevel lastLevel = derivativeLevels.get(derivativeLevels.size() - 1);
    		ArrayList<Derivative> ders = lastLevel.getDerivatives();
    		for (Derivative d : ders) {
    			if (d.getId().equals(variable)) {
    				if (!d.getDerivativeTerm().isPresent()) {
    					return true;
    				}
    				else {
    					return false;
    				}
    			}
    		}
    	}
    	return false;
    }
    
    /**
     * Removes the algorithmic auxiliary fields that depend from derivative auxiliary fields.
     * @throws AGDMException	AGDM00X - External error
     */
    public void removeAlgorithmicAuxFieldDerivativeDependent() throws AGDMException {
    	HashSet<String> varsToRemove = getAuxFieldDerivativeDependent();
        if (auxiliaryFieldEquations != null) {
            ArrayList<EquationTerms> equations = auxiliaryFieldEquations.getEquations();
            for (int i = equations.size() - 1; i >= 0; i--) {
            	EquationType auxiliaryFieldType = equations.get(i).getEquationType();
            	if (auxiliaryFieldType == EquationType.algorithmic && !equations.get(i).isMaximumPriority()) {
					//Remove equations marked for removal
					if (varsToRemove.containsAll(equations.get(i).getFields())) {
						equations.remove(i);
					}
            	}
            }
            EquationTermsLevel tmpAuxiliaryEquations = new EquationTermsLevel(auxiliaryFieldEquations);
            tmpAuxiliaryEquations = AGDMUtils.orderEquationTermDependencies(new ArrayList<DerivativeLevel>(), new ArrayList<MultiplicationTermsLevel>(), tmpAuxiliaryEquations);
            this.auxiliaryFieldEquations = tmpAuxiliaryEquations;
        }
    }
    
    /**
     * Removes the algorithmic auxiliary fields that do not depend from derivative auxiliary fields.
     * @throws AGDMException	AGDM00X - External error
     */
    public void removeAuxFieldDerivativeIndependent() throws AGDMException {
    	HashSet<String> varsToKeep = getAuxFieldDerivativeDependent();
        if (auxiliaryFieldEquations != null) {
            ArrayList<EquationTerms> equations = auxiliaryFieldEquations.getEquations();
            for (int i = equations.size() - 1; i >= 0; i--) {
            	HashSet<String> usedVars = new HashSet<String>();
            	if (equations.get(i).getAlgorithm() != null) {
	    			NodeList vars = equations.get(i).getAlgorithm().getElementsByTagName("mt:ci");
	    			for (int j = 0; j < vars.getLength(); j++) {
	    				usedVars.add(vars.item(j).getTextContent());
	    			}
				}
				//Remove equations marked to keep
				if (!varsToKeep.containsAll(equations.get(i).getFields()) && (Collections.disjoint(varsToKeep, usedVars) || equations.get(i).maximumPriority)) {
					equations.remove(i);
				}
            }
            
            EquationTermsLevel tmpAuxiliaryEquations = new EquationTermsLevel(auxiliaryFieldEquations);
            tmpAuxiliaryEquations = AGDMUtils.orderEquationTermDependencies(auxiliaryDerivativeLevels, auxiliaryMultiplicationLevels, tmpAuxiliaryEquations);
            this.auxiliaryFieldEquations = tmpAuxiliaryEquations;      
        }
    }
    
    /**
     * Gets the algorithmic auxiliary fields that depend on derivative auxiliary fields.
     * @return					List of fields
     * @throws AGDMException	AGDM00X - External error
     */
    public HashSet<String> getAuxFieldDerivativeDependent() throws AGDMException {
    	HashSet<String> dependencies = getDerivativeAuxiliaryFields();
      	dependencies.addAll(getDerivativeAuxiliaryVariables());
        if (auxiliaryFieldEquations != null) {
            ArrayList<EquationTerms> equations = auxiliaryFieldEquations.getEquations();
            for (int i = equations.size() - 1; i >= 0; i--) {
            	EquationType auxiliaryFieldType = equations.get(i).getEquationType();
            	if (auxiliaryFieldType == EquationType.algorithmic && !equations.get(i).isMaximumPriority()) {
            		NodeList vars = ((Element) equations.get(i).getAlgorithm()).getElementsByTagName("mt:ci");
    				for (int j = 0; j < vars.getLength(); j++) {
    					String var = vars.item(j).getTextContent();
    					//Found equation containing dependencies
    					if (dependencies.contains(var)) {
    						//Add removed equation to dependencies
    						dependencies.addAll(equations.get(i).getFields());
    					}
    				}
            	}
            }
        }
        
        return dependencies;
    }
    
    /**
     * Get auxiliary fields using derivatives.
     * @return		List of auxiliary fields
     */
    public HashSet<String> getDerivativeAuxiliaryFields() {
    	HashSet<String> variables = new LinkedHashSet<String>();
        if (auxiliaryFieldEquations != null) {
            ArrayList<EquationTerms> equations = auxiliaryFieldEquations.getEquations();
            for (int i = 0; i < equations.size(); i++) {
            	EquationType auxiliaryFieldType = equations.get(i).getEquationType();
            	if (auxiliaryFieldType == EquationType.algebraic) {
            		String field = equations.get(i).getFields().get(0);
            		variables.add(field);
            	}
            }
        }
        return variables;
    }
    
    /**
     * Get auxiliary fields using derivatives.
     * @return		List of auxiliary fields
     * @throws AGDMException 
     */
    public HashSet<String> getDerivativeAuxiliaryVariables() throws AGDMException {
    	HashSet<String> variables = new LinkedHashSet<String>();
    	for (int l = 0; l < derivativeLevels.size(); l++) {
	    	ArrayList<Derivative> derivatives = derivativeLevels.get(l).getDerivatives();
	    	for (int i = 0; i < derivatives.size(); i++) {
	    		Derivative d = derivatives.get(i);
	    		if (derivatives.get(i).getOperatorType() == OperatorInfoType.auxiliaryVariable) {
		    		if (d.getDerivativeTerm().isPresent()) {
		    			variables.add(d.getField());
		    			variables.add(d.getId());
		    		}
	    		}
	    	}
    	}
    	//Get indirect derivative auxiliary variables
    	int size = 0;
    	while (size != variables.size()) {
    		size = variables.size();
    		ArrayList<EquationTerms> eqs = equationTerms.getEquations();
	    	for (int i = 0; i < eqs.size(); i++) {
	    		if (eqs.get(i).getOperatorType() == OperatorInfoType.auxiliaryVariable) {
	    			ArrayList<String> terms = eqs.get(i).getTerms();
	    			//Add dependent equation term
	    			if (!Collections.disjoint(variables, terms)) {
	    				variables.addAll(eqs.get(i).getFields());
	    			}
	    		}
	    	}
	    	//Remove multiplication terms not appearing in the dependent terms.
	    	for (int l = 0; l < multiplicationLevels.size(); l++) {
		    	ArrayList<MultiplicationTerm> multTerms = multiplicationLevels.get(l).getMultiplicationTerms();
		    	for (int i = 0; i < multTerms.size(); i++) {
		    		if (multTerms.get(i).getOperatorType() == OperatorInfoType.auxiliaryVariable) {
		    			ArrayList<String> terms = multTerms.get(i).getVariables();
		    			//Add dependent equation term
		    			if (!Collections.disjoint(variables, terms)) {
		    				variables.add(multTerms.get(i).getField());
		    				variables.add(multTerms.get(i).getId());
		    			}
		    		}
		    	}
	    	}
	    	//Remove derivative terms not appearing in the dependent terms.
	    	for (int l = 0; l < derivativeLevels.size(); l++) {
		    	ArrayList<Derivative> derivatives = derivativeLevels.get(l).getDerivatives();
		    	for (int i = 0; i < derivatives.size(); i++) {
		    		if (derivatives.get(i).getOperatorType() == OperatorInfoType.auxiliaryVariable) {
		    			if (derivatives.get(i).getAlgebraicExpression() != null) {
				    		NodeList vars = ((Element) derivatives.get(i).getAlgebraicExpression()).getElementsByTagName("mt:ci");
			    			for (int j = 0; j < vars.getLength(); j++) {
				    			//Add dependent equation term
				    			if (variables.contains(vars.item(j).getTextContent())) {
				    				variables.add(derivatives.get(i).getField());
				    				variables.add(derivatives.get(i).getId());
				    			}
			    			}
		    			}
		    		}
		    	}
	    	}
    	}
        return variables;
    }
    
	/**
     * Get variables used in auxiliary equations.
     * @return					Variables
     * @throws AGDMException	AGDM00X - External error
     */
    public HashSet<String> getAuxiliaryFieldVariables() throws AGDMException {
    	HashSet<String> variables = new LinkedHashSet<String>();
    	for (int l = 0; l < auxiliaryDerivativeLevels.size(); l++) {
	    	ArrayList<Derivative> derivatives = auxiliaryDerivativeLevels.get(l).getDerivatives();
	    	for (int i = 0; i < derivatives.size(); i++) {
	    		Derivative d = derivatives.get(i);
	    		//Eliminado para que funcione el test 18, cuando solo hay auxiliaryVariables
	    		//if (d.getOperatorType() == OperatorInfoType.auxiliaryField) {
		    	    Optional<Node> derTerm = d.getDerivativeTerm();
		    	    if (derTerm.isPresent()) {
		    			NodeList vars = ((Element) derTerm.get()).getElementsByTagName("mt:ci");
		    			for (int j = 0; j < vars.getLength(); j++) {
		    				variables.add(vars.item(j).getTextContent());
		    			}
			    	}
		    	    if (d.getAlgebraicExpression() != null) {
			    		NodeList vars = ((Element) d.getAlgebraicExpression()).getElementsByTagName("mt:ci");
		    			for (int j = 0; j < vars.getLength(); j++) {
		    				variables.add(vars.item(j).getTextContent());
		    			}
		    	    }
	    		//}
	    	}
    	}
    	for (int l = auxiliaryMultiplicationLevels.size() - 1; l >= 0; l--) {
	    	ArrayList<MultiplicationTerm> multTerms = auxiliaryMultiplicationLevels.get(l).getMultiplicationTerms();
	    	for (int i = 0; i < multTerms.size(); i++) {
	    		//if (multTerms.get(i).getOperatorType() == OperatorInfoType.auxiliaryField) {
	    			variables.addAll(multTerms.get(i).getVariables());
	    			if (multTerms.get(i).getAlgebraicExpression() != null) {
		    			NodeList vars = ((Element) multTerms.get(i).getAlgebraicExpression()).getElementsByTagName("mt:ci");
		    			for (int j = 0; j < vars.getLength(); j++) {
		    				variables.add(vars.item(j).getTextContent());
		    			}
	    			}
	    		//}
	    	}
    	}
    	ArrayList<EquationTerms> eqs = auxiliaryFieldEquations.getEquations();
    	for (int i = 0; i < eqs.size(); i++) {
    		variables.addAll(eqs.get(i).getTerms());
    	}
    	return variables;
    }
    
    /**
     * Check if the operators info derivatives has effectively any derivative term for a specific type.
     * @param opType	The type to check
     * @return			true if any derivative term
     */
    public boolean hasDerivatives(OperatorInfoType opType) {
    	for (int l = 0; l < derivativeLevels.size(); l++) {
	    	ArrayList<Derivative> derivatives = derivativeLevels.get(l).getDerivatives();
	    	for (int i = 0; i < derivatives.size(); i++) {
	    		Derivative d = derivatives.get(i);
	            Optional<Node> derTerm = d.getDerivativeTerm();
	    		if (derTerm.isPresent() && d.getOperatorType() == opType) {
	    			return true;
	    		}
	    	}
    	}
    	return false;
    }
    
    /**
     * Check if the operators info derivatives has effectively any derivative term for auxiliary fields.
     * @return					true if any derivative term
     * @throws AGDMException 
     */
    public boolean hasAuxiliaryFieldDerivatives() throws AGDMException {
        if (auxiliaryFieldEquations != null) {
            ArrayList<EquationTerms> equations = auxiliaryFieldEquations.getEquations();
            for (int i = 0; i < equations.size(); i++) {
            	EquationType auxiliaryFieldType = equations.get(i).getEquationType();
            	if (auxiliaryFieldType == EquationType.algebraic) {
            		return true;
            	}
            }
            //Also check auxiliary variable dependencies with derivatives
            OperatorsInfo opInfoAuxFields = new OperatorsInfo(this);
            opInfoAuxFields.onlyDependent(opInfoAuxFields.getAuxiliaryFieldAlgorithmVariables(), true, null);
            if (opInfoAuxFields.hasDerivatives(OperatorInfoType.auxiliaryVariable)) {
            	return true;
            }
        }
    	return false;
    }
    
    /**
     * Orders the derivatives for performance.
     * @param coords			Spatial coordinates.
     * @throws AGDMException	AGDM00X - External error
     */
    private void orderDerivatives(ArrayList<String> coords) throws AGDMException {
    	for (int i = 0; i < derivativeLevels.size(); i++) {
    		ArrayList<Derivative> derivatives = derivativeLevels.get(i).getDerivatives();
    		ArrayList<Derivative> newDerivatives = new ArrayList<Derivative>();
    		HashMap<String, ArrayList<Derivative>> map = new LinkedHashMap<String, ArrayList<Derivative>>();
			//Classify derivatives by derivative term
    		for (int j = 0; j < derivatives.size(); j++) {
    			Derivative der = derivatives.get(j);
    			String derivativeTerm = "";
    			if (der.getDerivativeTerm().isPresent()) {
    				derivativeTerm = AGDMUtils.xmlTransform(AGDMUtils.domToString(der.getDerivativeTerm().get()));
    			}
    			//Add derivative to the correspondent key
    			ArrayList<Derivative> mapDer = map.getOrDefault(derivativeTerm, new ArrayList<Derivative>());
				mapDer.add(der);
				map.put(derivativeTerm, mapDer);
    		}
    		//Iterate over derivative terms
    		Iterator<String> keys = map.keySet().iterator();
    	    while (keys.hasNext()) {
    	        String derivativeTerm = keys.next();
    	        ArrayList<Derivative> mapDers = map.get(derivativeTerm);
    	        //Iterate over coordinates
    	        //Group same coordinates
    	        for (int j = 0; j < coords.size(); j++) {
    	        	for (int n = 0; n < coords.size(); n++) {
    	        		ArrayList<String> coordCombinations = AGDMUtils.createCombinations(n + 1, coords);
    	        		if (coordCombinations.get(0).substring(0, 1).equals(coords.get(j))) {
    	    	        	for (int l = 0; l < coordCombinations.size(); l++) {
    	        	        	String coordComb = coordCombinations.get(l);
    	        	        	for (int k = 0; k < mapDers.size(); k++) {
    	        	        		Derivative der = mapDers.get(k);
    	        	        		if (der.getCoordinates() != null) {
    		        	        		ArrayList<String> derCoords = der.getCoordinates();
    		        	        		String derCoordString = "";
    		        	        		for (int m = 0; m < derCoords.size(); m++) {
    		        	        			derCoordString = derCoordString + derCoords.get(m);
    		        	        		}
    		        	        		if (coordComb.equals(derCoordString)) {
    		        	        			newDerivatives.add(der);
    		        	        		}
    	        	        		}
    	        	        	}
    	    	        	}
    	        		}
    	        	}
    	        }
    	        //add derivatives without coordinates
    	       	for (int k = 0; k < mapDers.size(); k++) {
	        		Derivative der = mapDers.get(k);
   	        		if (der.getCoordinates() == null) {
 	        			newDerivatives.add(der);
   	        		}
    	       	}
    	    }
    		derivativeLevels.set(i, new DerivativeLevel(newDerivatives));
    	}
    }
    
    /**
     * Obtains the variables to interpolate from mesh or from particles, depending on the operator information discretization type.
     * @param policy			Discretization policy
     * @throws AGDMException 	AGDM00X - External error
     */
    private void getVariablesToInterpolate(DiscretizationPolicy policy) throws AGDMException {
    	ArrayList<String> candidateVariables = new ArrayList<String>();
    	if (type == DiscretizationType.mesh) {
    		for (Species s : policy.getParticleSpecies().values()) {
    			candidateVariables.addAll(s.getParticleFields());
    		}
    	}
    	if (type == DiscretizationType.particles) {
    		candidateVariables = policy.getMeshFields();	
    	}
    	for (int i = 0; i < derivativeLevels.size(); i++) {
    		ArrayList<Derivative> derivatives = derivativeLevels.get(i).getDerivatives();
    		for (int j = 0; j < derivatives.size(); j++) {
    			Derivative der = derivatives.get(j);
    			if (der.getAlgebraicExpression() != null) {
    				NodeList fields = ((Element) der.getAlgebraicExpression()).getElementsByTagName("mt:ci");
	    			for (int k = fields.getLength() - 1; k >= 0; k--) {
	    				String field = fields.item(k).getTextContent();
	    				if (candidateVariables.contains(field)) {
	    					fields.item(k).setTextContent(field + "_" + type);
		    				if (!variablesToInterpolate.contains(field)) {
		        				variablesToInterpolate.add(field);
		        			}
	    				}
	    			}
    			}
    			if (der.getDerivativeTerm().isPresent()) {
	    			NodeList fields = ((Element) der.getDerivativeTerm().get()).getElementsByTagName("mt:ci");
	    			for (int k = fields.getLength() - 1; k >= 0; k--) {
	    				String field = fields.item(k).getTextContent();
	    				if (candidateVariables.contains(field)) {
	    					fields.item(k).setTextContent(field + "_" + type);
		    				if (!variablesToInterpolate.contains(field)) {
		        				variablesToInterpolate.add(field);
		        			}
	    				}
	    			}
    			}
    		}
    	}
    	for (int l = 0; l < multiplicationLevels.size(); l++) {
        	MultiplicationTermsLevel mtl = multiplicationLevels.get(l);
        	//Reuse similar multiplication terms
            ArrayList<MultiplicationTerm> mterms = mtl.getMultiplicationTerms();
            for (int i = 0; i < mterms.size(); i++) {
                MultiplicationTerm mt = mterms.get(i);
                if (mt.getAlgebraicExpression() != null) {
	    			NodeList fields = ((Element) mt.getAlgebraicExpression()).getElementsByTagName("mt:ci");
	    			for (int k = fields.getLength() - 1; k >= 0; k--) {
	    				String field = fields.item(k).getTextContent();
	    				if (candidateVariables.contains(field)) {
	    					fields.item(k).setTextContent(field + "_" + type);
		    				if (!variablesToInterpolate.contains(field)) {
		        				variablesToInterpolate.add(field);
		        			}
	    				}
	    			}
                }
            }
        }
    	ArrayList<Flux> fluxes = fluxTerms.getFluxes();
    	for (int i = 0; i < fluxes.size(); i++) {
			NodeList fields = ((Element) fluxes.get(i).getMath()).getElementsByTagName("mt:ci");
			for (int k = fields.getLength() - 1; k >= 0; k--) {
				String field = fields.item(k).getTextContent();
				if (candidateVariables.contains(field)) {
					fields.item(k).setTextContent(field + "_" + type);
    				if (!variablesToInterpolate.contains(field)) {
        				variablesToInterpolate.add(field);
        			}
				}
			}
    	}
    	
    	ArrayList<Source> sources = sourceTerms.getSources();
    	for (int i = 0; i < sources.size(); i++) {
			NodeList fields = ((Element) sources.get(i).getFormula()).getElementsByTagName("mt:ci");
			for (int k = fields.getLength() - 1; k >= 0; k--) {
				String field = fields.item(k).getTextContent();
				if (candidateVariables.contains(field)) {
					fields.item(k).setTextContent(field + "_" + type);
    				if (!variablesToInterpolate.contains(field)) {
        				variablesToInterpolate.add(field);
        			}
				}
			}
    	}
    	
    	//Auxiliary equations
    	ArrayList<EquationTerms> auxEq = auxiliaryFieldEquations.getEquations();
    	for (int i = 0; i < auxEq.size(); i++) {
    		Element algorithm = auxEq.get(i).getAlgorithm();
    		if (algorithm != null) {
    	    	NodeList vars = AGDMUtils.find(algorithm, "descendant-or-self::mt:math/mt:apply/mt:eq/following-sibling::*[2]/descendant-or-self::mt:ci");
    	    	for (int k = vars.getLength() - 1; k >= 0; k--) {
					String var = vars.item(k).getTextContent();
					if (candidateVariables.contains(var)) {
						vars.item(k).setTextContent(var + "_" + type);
	    				if (!auxiliaryVariablesToInterpolate.contains(var)) {
	    					auxiliaryVariablesToInterpolate.add(var);
	        			}
					}
				}
    		}
    	}
    	for (int i = 0; i < auxiliaryDerivativeLevels.size(); i++) {
    		ArrayList<Derivative> derivatives = auxiliaryDerivativeLevels.get(i).getDerivatives();
    		for (int j = 0; j < derivatives.size(); j++) {
    			Derivative der = derivatives.get(j);
    			if (der.getAlgebraicExpression() != null) {
    				NodeList fields = ((Element) der.getAlgebraicExpression()).getElementsByTagName("mt:ci");
	    			for (int k = fields.getLength() - 1; k >= 0; k--) {
	    				String field = fields.item(k).getTextContent();
	    				if (candidateVariables.contains(field)) {
	    					fields.item(k).setTextContent(field + "_" + type);
		    				if (!auxiliaryVariablesToInterpolate.contains(field)) {
		    					auxiliaryVariablesToInterpolate.add(field);
		        			}
	    				}
	    			}
    			}
    			if (der.getDerivativeTerm().isPresent()) {
	    			NodeList fields = ((Element) der.getDerivativeTerm().get()).getElementsByTagName("mt:ci");
	    			for (int k = fields.getLength() - 1; k >= 0; k--) {
	    				String field = fields.item(k).getTextContent();
	    				if (candidateVariables.contains(field)) {
	    					fields.item(k).setTextContent(field + "_" + type);
		    				if (!auxiliaryVariablesToInterpolate.contains(field)) {
		    					auxiliaryVariablesToInterpolate.add(field);
		        			}
	    				}
	    			}
    			}
    		}
    	}
    	for (int l = 0; l < auxiliaryMultiplicationLevels.size(); l++) {
        	MultiplicationTermsLevel mtl = auxiliaryMultiplicationLevels.get(l);
        	//Reuse similar multiplication terms
            ArrayList<MultiplicationTerm> mterms = mtl.getMultiplicationTerms();
            for (int i = 0; i < mterms.size(); i++) {
                MultiplicationTerm mt = mterms.get(i);
                if (mt.getAlgebraicExpression() != null) {
	    			NodeList fields = ((Element) mt.getAlgebraicExpression()).getElementsByTagName("mt:ci");
	    			for (int k = fields.getLength() - 1; k >= 0; k--) {
	    				String field = fields.item(k).getTextContent();
	    				if (candidateVariables.contains(field)) {
	    					fields.item(k).setTextContent(field + "_" + type);
		    				if (!auxiliaryVariablesToInterpolate.contains(field)) {
		    					auxiliaryVariablesToInterpolate.add(field);
		        			}
	    				}
	    			}
                }
            }
        }
    	
    	//Velocity and volume
    	if (type == DiscretizationType.particles) {
    		for (Species s : policy.getParticleSpecies().values()) {
    			for (Element e : s.getParticleVelocities().values()) {
    				NodeList fields = e.getElementsByTagName("mt:ci");
	    			for (int k = fields.getLength() - 1; k >= 0; k--) {
	    				String field = fields.item(k).getTextContent();
	    				if (candidateVariables.contains(field)) {
	    					fields.item(k).setTextContent(field + "_" + type);
		    				if (!variablesToInterpolate.contains(field)) {
		    					variablesToInterpolate.add(field);
		        			}
	    				}
	    			}
    			}
				NodeList fields = s.getParticleVolume().getElementsByTagName("mt:ci");
    			for (int k = fields.getLength() - 1; k >= 0; k--) {
    				String field = fields.item(k).getTextContent();
    				if (candidateVariables.contains(field)) {
    					fields.item(k).setTextContent(field + "_" + type);
	    				if (!variablesToInterpolate.contains(field)) {
	    					variablesToInterpolate.add(field);
	        			}
    				}
    			}
    		}
    		
    	}
    	
    	if ((variablesToInterpolate.size() > 0 || auxiliaryVariablesToInterpolate.size() > 0) && type == DiscretizationType.particles) {
    		AGDMImpl.hasMeshInterpolation = true;
    	}
    	if ((variablesToInterpolate.size() > 0 || auxiliaryVariablesToInterpolate.size() > 0) && type == DiscretizationType.mesh) {
    		AGDMImpl.hasParticleInterpolation = true;
    	}
    }
	public ArrayList<String> getVariablesToInterpolate() {
		return variablesToInterpolate;
	}
	
	public ArrayList<String> getAuxiliaryVariablesToInterpolate() {
		return auxiliaryVariablesToInterpolate;
	}
	
	/**
	 * Calculate normalization information.
	 * @param ders	Derivatives
	 * @param mul	Multiplication levels
	 * @return		Information
	 */
	public ArrayList<NormalizationLevel> calculateNormalization(ArrayList<DerivativeLevel> ders, ArrayList<MultiplicationTermsLevel> mul) {
		ArrayList<NormalizationLevel> norm = new ArrayList<NormalizationLevel>();
		int levels = ders.size();
		for (int i = 0; i < levels; i++) {
		    HashMap<String, ArrayList<String>> coordVariablesMap = new HashMap<String, ArrayList<String>>();
		    HashMap<String, ArrayList<String>> coordSchemaIdMap = new HashMap<String, ArrayList<String>>();
		    HashMap<String, ArrayList<String>> coordSchemaNameMap = new HashMap<String, ArrayList<String>>();
			for (Derivative d: ders.get(i).getDerivatives()) {
				if (d.getDerivativeTerm().isPresent()) {
					String varName = d.getId();
					String coordComb = String.join("", d.getCoordinates());
					coordComb = coordComb + AGDMImpl.normalizationSchemaId.indexOf(d.getSpatialOperatorDiscretization().getNormalizationId());
					//coordinates-variables map
					ArrayList<String> variables = new ArrayList<String>();
					variables.add(varName);
					ArrayList<String> key = coordVariablesMap.get(coordComb);
					if (key != null) {
						variables.addAll(key);
					}
					coordVariablesMap.put(coordComb, variables);
					//coordinates-schema maps
					ArrayList<String> schemaId = new ArrayList<String>();
					schemaId.add(d.getSpatialOperatorDiscretization().getNormalizationId());
					ArrayList<String> schemaName = new ArrayList<String>();
					schemaName.add(d.getSpatialOperatorDiscretization().getNormalizationName());
					coordSchemaIdMap.put(coordComb, schemaId);
					coordSchemaNameMap.put(coordComb, schemaName);
				}
			}
			
			norm.add(new NormalizationLevel(coordVariablesMap, coordSchemaIdMap, coordSchemaNameMap));
		}
		return norm;
	}
	
	private HashSet<String> getEquationVariables(Node problem, DiscretizationPolicy policy, DiscretizationType type, boolean analysis) throws AGDMException {
		HashSet<String> dependentVariables = new HashSet<String>();
		if (!analysis) {
			//Sources
			dependentVariables.addAll(getConservativeSourceVariables());
			//Stiff sources
			dependentVariables.addAll(getStiffSourceVariables());
			//Fluxes
			dependentVariables.addAll(getFluxVariables());
			//Fields
			dependentVariables.addAll(evolutionFields);
			if (type == DiscretizationType.mesh) {
				//Characteristic information
				dependentVariables.addAll(getMaxCharSpeedVariables(problem));
			}	
		}		
		else {
			//Fields
			dependentVariables.addAll(analysisFields);	
		}
		//Derivative levels
		dependentVariables.addAll(getDerivativeVariables(true));
		//Multiplication levels
		dependentVariables.addAll(getMultiplicationVariables(true));
		//Auxiliary field variables
		dependentVariables.addAll(auxiliaryFields);
		dependentVariables.addAll(getAuxiliaryFieldAlgorithmVariables());
		dependentVariables.addAll(getAuxiliaryFieldVariables());
		
		if (type == DiscretizationType.particles) {
			//Velocities
			dependentVariables.addAll(getVelocityVariables(policy));
		}
		return dependentVariables;
	}
	
	private HashSet<String> getAuxFieldsVariables(Node problem, DiscretizationPolicy policy, DiscretizationType type) throws AGDMException {
		HashSet<String> dependentVariables = new HashSet<String>();
		//Fields
		dependentVariables.addAll(auxiliaryFields);
		dependentVariables.addAll(getAuxiliaryFieldAlgorithmVariables());
		dependentVariables.addAll(getAuxiliaryFieldVariables());
		return dependentVariables;
	}
	
	/**
	 * Add auxiliary variable structures to Auxiliary field derivative and multiplication levels.
	 */
	private void addAuxVarsToAuxFields() {
		//Equiparate levels
		int shiftDers = 0;
		if (derivativeLevels.size() > auxiliaryDerivativeLevels.size()) {
			int originalSize = auxiliaryDerivativeLevels.size();
			for (int i = 0; i < derivativeLevels.size() - originalSize; i++) {
				auxiliaryDerivativeLevels.add(new DerivativeLevel());
			}
		}
		if (derivativeLevels.size() < auxiliaryDerivativeLevels.size()) {
			shiftDers = auxiliaryDerivativeLevels.size() - derivativeLevels.size();
		}
		int shiftMults = 0;
		if (multiplicationLevels.size() > auxiliaryMultiplicationLevels.size()) {
			int originalSize = auxiliaryMultiplicationLevels.size();
			for (int i = 0; i < multiplicationLevels.size() - originalSize; i++) {
				auxiliaryMultiplicationLevels.add(new MultiplicationTermsLevel());
			}
		}

		if (multiplicationLevels.size() < auxiliaryMultiplicationLevels.size()) {
			shiftMults = auxiliaryMultiplicationLevels.size() - multiplicationLevels.size();
		}
		
		//Derivative levels
        for (int l = 0; l < derivativeLevels.size(); l++) {
            DerivativeLevel dl = derivativeLevels.get(l);
            DerivativeLevel auxDl = auxiliaryDerivativeLevels.get(l + shiftDers);
            ArrayList<Derivative> derivatives = dl.getDerivatives();
            for (int i = 0; i < derivatives.size(); i++) {
                Derivative d = derivatives.get(i);
                if (d.getOperatorType() == OperatorInfoType.auxiliaryVariable) {
                	auxDl.add(new Derivative(d));   
                }
            }
            auxiliaryDerivativeLevels.set(l + shiftDers, auxDl);            
        }
        //Multiplicative terms
    	for (int l = 0; l < multiplicationLevels.size(); l++) {
	    	ArrayList<MultiplicationTerm> multTerms = multiplicationLevels.get(l).getMultiplicationTerms();
	    	MultiplicationTermsLevel auxMl = auxiliaryMultiplicationLevels.get(l + shiftMults);
	    	for (int i = 0; i < multTerms.size(); i++) {
	    		if (multTerms.get(i).getOperatorType() == OperatorInfoType.auxiliaryVariable) {
	    			auxMl.add(new MultiplicationTerm(multTerms.get(i))); 
	    		}
	    	}
	    	auxiliaryMultiplicationLevels.set(l + shiftMults, auxMl);  
    	}
	}
	
	/**
	 * Checks if auxiliary variables has derivatives in a given level.
	 * @param level			Level to check
	 * @return				True if there are derivatives in auxiliary variables
	 */
    public boolean hasAuxiliaryVarDerivative(int level) {
        ArrayList<Derivative> derivatives = derivativeLevels.get(level).getDerivatives();
        for (int i = 0; i < derivatives.size(); i++) {
        	if (derivatives.get(i).getOperatorType() == OperatorInfoType.auxiliaryVariable && derivatives.get(i).getDerivativeTerm().isPresent()) {
        		return true;
        	}
        }
        return false;
    }
}
