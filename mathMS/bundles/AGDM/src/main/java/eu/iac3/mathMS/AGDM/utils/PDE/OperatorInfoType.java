package eu.iac3.mathMS.AGDM.utils.PDE;

public enum OperatorInfoType {
    field,
    auxiliaryField,
    analysisField,
    auxiliaryVariable,
    interaction
}
