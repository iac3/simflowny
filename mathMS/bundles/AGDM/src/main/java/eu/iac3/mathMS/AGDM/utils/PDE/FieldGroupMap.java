/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.AGDM.utils.PDE;

/**
 * Mapping of a field group from the schema and the region.
 * @author bminyano
 *
 */
public class FieldGroupMap {
    private String schema;
    private String region;
    
    public String getSchema() {
        return schema;
    }
    public String getRegion() {
        return region;
    }
    public void setSchema(String schema) {
        this.schema = schema;
    }
    public void setRegion(String region) {
        this.region = region;
    }
}
