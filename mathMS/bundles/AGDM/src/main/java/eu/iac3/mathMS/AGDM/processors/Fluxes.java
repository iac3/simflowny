/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.AGDM.processors;

import eu.iac3.mathMS.AGDM.AGDMException;
import eu.iac3.mathMS.AGDM.utils.AGDMUtils;
import eu.iac3.mathMS.AGDM.utils.CoordinateInfo;
import eu.iac3.mathMS.AGDM.utils.PDE.FluxInfo;
import eu.iac3.mathMS.AGDM.utils.PDE.ProcessInfo;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * This class have all the necessary methods to generate the flux information.
 * @author bminyano
 *
 */
public final class Fluxes {
    static String mmsUri = "urn:mathms";
    static String smlUri = "urn:simml";
    static String mtUri = "http://www.w3.org/1998/Math/MathML";
    
    /**
     * Constructor.
     */
    private Fluxes() { };
    /**
     * Generates the flux functions from the PDEs.
     * Adapts the equations that uses fluxes to set all the parameters in the flux, not only the equation's field.
     * 
     * @param result                    The document where to generate the results.
     * @param problem                   The problem document with the fluxes of the models
     * @param region                    The region to generate the fluxes.
     * @param coordRel                  The coordinate relations
     * @param regionName                The name of the region
     * @param pi						Problem information
     * @return                          The document fragment with the flux functions.
     * @throws AGDMException            AGDM002 XML discretization schema not valid 
     *                                  AGDM008 - External error
     */
    public static DocumentFragment fluxGeneration(Document result, Document problem, Element region, 
    		CoordinateInfo coordRel, String regionName, ProcessInfo pi) throws AGDMException {
        //The result is stored in a document fragment
        DocumentFragment resultFragment = result.createDocumentFragment();
        //The flux function information
        LinkedHashMap < String, FluxInfo > fluxRel = new LinkedHashMap < String, FluxInfo >();
        
        NodeList problemFields = AGDMUtils.find(result, "/*/mms:fields/mms:field|/*/mms:auxiliaryFields/mms:auxiliaryField");
        NodeList problemVariables = AGDMUtils.find(result, "/*/mms:auxiliaryVariables/mms:auxiliaryVariable");
        
        String query;
        if (regionName.substring(regionName.length() - 1, regionName.length()).equals("S")) {
            query = "mms:surfaceModel";  
        } 
        else {
            query = "mms:interiorModel";   
        }
        NodeList models = region.getElementsByTagName(query);
        for (int i = 0; i < models.getLength(); i++) {
            NodeList fluxes = AGDMUtils.find(problem, "//mms:model[mms:name = '" 
                    + models.item(i).getTextContent() + "']//mms:flux");
            for (int j = 0; j < fluxes.getLength(); j++) {
                Element flux = (Element) fluxes.item(j);
                //Get the name of the field
                String field = ((Element) flux.getParentNode().getParentNode()).getElementsByTagNameNS(mmsUri, "field").item(0).getTextContent();
                //Get the spatial coordinate of the flux
                String spatialCoord = coordRel.getDiscCoords().get(coordRel.getContCoords().indexOf(flux.getFirstChild().getTextContent()));
                //Creation of the function
                Element fluxFunction = AGDMUtils.createElement(result, mmsUri, "function");
                //Function name
                Element fluxName = AGDMUtils.createElement(result, mmsUri, "functionName");
                fluxName.setTextContent("F" + spatialCoord + field + "_" + AGDMUtils.removeInvalidVariableCharacters(regionName));
                fluxFunction.appendChild(fluxName);
                //Search for the fields and parameters used in the flux and make it parameters
                LinkedHashMap < String, String > parameters = new LinkedHashMap < String, String >();
                for (int k = 0; k < problemFields.getLength(); k++) {
                    String keyField = problemFields.item(k).getTextContent();
                    NodeList found =  AGDMUtils.find(flux, ".//mt:ci[text() = '" + keyField + "']");
                    //Adding it to the parameter list
                    if (found.getLength() > 0) {
                        parameters.put(keyField, "field");
                    }
                }
                for (int k = 0; k < problemVariables.getLength(); k++) {
                    String keyVar = problemVariables.item(k).getTextContent();
                    NodeList found =  AGDMUtils.find(flux, ".//mt:ci[text() = '" + keyVar + "']");
                    //Adding it to the parameter list
                    if (found.getLength() > 0) {
                        parameters.put(keyVar, "variable");
                    }
                }
                NodeList problemParameters = AGDMUtils.find(region, "//mms:parameters/mms:parameter/mms:name");
                for (int k = 0; k < problemParameters.getLength(); k++) {
                    String keyParameter = problemParameters.item(k).getTextContent();
                    String parameterType = problemParameters.item(k).getParentNode().getFirstChild().getNextSibling().getTextContent();
                    NodeList found =  AGDMUtils.find(flux, ".//mt:ci[text() = '" + keyParameter + "']");
                    //Adding it to the parameter list
                    if (found.getLength() > 0) {
                        parameters.put(keyParameter, parameterType);
                    }
                }
                
                if (parameters.size() > 0) {
                    //Function parameters
                    Element fluxFields = AGDMUtils.createElement(result, mmsUri, "functionParameters");
                    Iterator <String> paramIt = parameters.keySet().iterator();
                    while (paramIt.hasNext()) {
                        String param = paramIt.next();
                        String type = parameters.get(param);
                        if (type.equals("field") || type.equals("variable")) {
                            type = "real";
                        }
                        Element fluxField = AGDMUtils.createElement(result, mmsUri, "functionParameter");
                        Element paramName = AGDMUtils.createElement(result, mmsUri, "name");
                        paramName.setTextContent(param);
                        Element paramType = AGDMUtils.createElement(result, mmsUri, "type");
                        paramType.setTextContent(type.toLowerCase());
                        fluxField.appendChild(paramName);
                        fluxField.appendChild(paramType);
                        fluxFields.appendChild(fluxField.cloneNode(true));
                    }
                    fluxFunction.appendChild(fluxFields);
                }
                //Function instructions
                Element fluxInstructions = AGDMUtils.createElement(result, mmsUri, "functionInstructions");
                Element simml = AGDMUtils.createElement(result, smlUri, "simml");
                Element returnMath = AGDMUtils.createElement(result, smlUri, "return");
                returnMath.appendChild(result.importNode(flux.getLastChild().cloneNode(true), true));
                simml.appendChild(returnMath);
                fluxInstructions.appendChild(simml);
                fluxFunction.appendChild(fluxInstructions);
                resultFragment.appendChild(fluxFunction);
                
                //Add to the flux info
                FluxInfo fi = new FluxInfo(field, parameters);
                fluxRel.put("F" + spatialCoord + field, fi);
            }
        }
        
        //Adapt discretized equations to flux information
        //Take the suitable execution flow depending on the region
        Element executionFlow;
        String originalName = regionName.substring(0, regionName.length() - 1);
        if (regionName.substring(regionName.length() - 1, regionName.length()).equals("S")) {
            executionFlow = (Element) AGDMUtils.find(result, "//mms:surfaceExecutionFlow[preceding-sibling::mms:name[text() = '"
                    + originalName + "']]").item(0);
        }
        else {
            executionFlow = (Element) AGDMUtils.find(result, "//mms:interiorExecutionFlow[preceding-sibling::mms:name[text() = '"
                    + originalName + "']]").item(0);
        }
        adaptToFluxes(executionFlow, fluxRel, result, regionName, pi);
        return resultFragment;
    }
  
    
    /**
     * Adapts the equations that uses fluxes to the fields that are used in the flux.
     * 1 - Take the fluxes and its original parameters in the equations. Replace the flux calling name with one that contains the region
     * 2 - If the paramenter is a field replace the parameters with the real parameters of the flux
     * 3 - If the parameter is a variable then replace with the new variables
     * 4 - If the parameter is a function call then replace with the new functions for the fields.
     * @param fragment          Where to adapt fluxes
     * @param result            The document with the equations
     * @param fluxRel           Fluxes and their info.
     * @param regionName        The region name
     * @param pi				Problem information
     * @throws AGDMException    AGDM002 XML discretization schema not valid    
     *                          AGDM007 External error
     */
    public static void adaptToFluxes(Node fragment, LinkedHashMap < String, FluxInfo > fluxRel, Document result, String regionName,
    		ProcessInfo pi) throws AGDMException {
        //Search all the fluxes
        NodeList fluxes = ((Element) fragment).getElementsByTagNameNS(smlUri, "flux");
        HashMap<Node, Node> fluxReplacement = new HashMap<Node, Node>(); 
        int fluxesLength = fluxes.getLength();
        for (int i = 0; i < fluxesLength; i++) {
            Element flux = (Element) fluxes.item(i).cloneNode(true);
            //Replace original flux variable with the model ones
            if (fluxRel.containsKey(flux.getFirstChild().getTextContent())) {
                //Take all the fields used in the flux
                FluxInfo fi = fluxRel.get(flux.getFirstChild().getTextContent());
                LinkedHashMap < String, String > params = fi.getParams();
                
                //1 - Take the original parameter field and its parent element.Replace the flux calling name with one that contains the region
                Element originalParam = (Element) flux.getLastChild();
                flux.getFirstChild().setTextContent(flux.getFirstChild().getTextContent() + "_" 
                        + AGDMUtils.removeInvalidVariableCharacters(regionName));
                //Remove the original field
                flux.removeChild(originalParam);
                //Check the type of the original parameter.
                //Could be a field, a variable or a function call. 
                
                //field, variable or parameter
                String query = ".//ancestor::mt:ci[ends-with(text()[1],'" + fi.getField() + "') and not(ancestor::sml:functionCall)]|"
                    + ".//ancestor::mt:ci[starts-with(text()[1],'" + fi.getField() + "') and not(ancestor::sml:functionCall)]";
                NodeList cis = AGDMUtils.find(originalParam, query);
                if (cis.getLength() > 0) {
                    Element ci = (Element) cis.item(0);
                    //2 - field
                    if (ci.getTextContent().equals(fi.getField()) &&  ci.getParentNode().getParentNode().getParentNode() != null 
                            && ci.getParentNode().getParentNode().getParentNode().equals(originalParam)) {
                        adaptField(fi, params, originalParam, flux, result, pi);
                    }
                    //3 - variable 
                    else {
                        adaptVariable(fi, params, originalParam, flux, result, ci, pi);
                    }
                }
               
                //4 - function call. The function called cannot contain another flux, so the function is general.
                query = ".//mt:ci[starts-with(text(),'" + fi.getField() + "') and not(text() = '" + fi.getField() + "')]" 
                    + "/ancestor::sml:functionCall";
                NodeList functionCalls = AGDMUtils.find(originalParam, query);
                if (functionCalls.getLength() > 0) {
                    Iterator <String> paramIt = params.keySet().iterator();
                    while (paramIt.hasNext()) {
                        String param = paramIt.next();
                        if (params.get(param).equals("field")) {
                            Element newFunctionCall = (Element) functionCalls.item(0).cloneNode(true);
                            cis = AGDMUtils.find(newFunctionCall, ".//mt:ci[starts-with(text(),'" + fi.getField() + "')]");
                            int cisLength = cis.getLength();
                            for (int j = 0; j < cisLength; j++) {
                                String newCi = 
                                    param + cis.item(j).getTextContent().substring(fi.getField().length());
                                cis.item(j).setTextContent(newCi);
                            }
                            flux.appendChild(newFunctionCall);
                        }
                        else {
                            Element ciParam = AGDMUtils.createElement(result, mtUri, "ci");
                            ciParam.setTextContent(param);
                            flux.appendChild(ciParam);
                        }
                    }
                }
                
                query = ".//mt:ci[ends-with(text(),'" + fi.getField() + "')]/ancestor::sml:functionCall";
                functionCalls = AGDMUtils.find(originalParam, query);
                if (functionCalls.getLength() > 0) {
                    Iterator <String> paramIt = params.keySet().iterator();
                    while (paramIt.hasNext()) {
                        String param = paramIt.next();
                        if (params.get(param).equals("field")) {
                            Element newFunctionCall = (Element) functionCalls.item(0).cloneNode(true);
                            cis = AGDMUtils.find(newFunctionCall, ".//mt:ci[ends-with(text(),'" + fi.getField() + "')]");
                            int cisLength = cis.getLength();
                            for (int j = 0; j < cisLength; j++) {
                                String newCi = cis.item(j).getTextContent()
                                        .substring(0, cis.item(j).getTextContent().lastIndexOf(fi.getField())) + param;
                                cis.item(j).setTextContent(newCi);
                            }
                            flux.appendChild(newFunctionCall);
                        }
                        else {
                            Element ciParam = AGDMUtils.createElement(result, mtUri, "ci");
                            ciParam.setTextContent(param);
                            flux.appendChild(ciParam);
                        }
                    }
                }
            }
            //Only particles generates fluxes 0
            else {
                Element zero = AGDMUtils.createElement(result, mtUri, "cn");
                zero.setTextContent("0");
                flux.getParentNode().replaceChild(zero, flux);
            }
            fluxReplacement.put(fluxes.item(i), flux);
        }
        for (Entry<Node, Node> e: fluxReplacement.entrySet()) {
        	Node oldNode = e.getKey();
        	Node newNode = e.getValue();
        	oldNode.getParentNode().replaceChild(newNode, oldNode);
        }
    }
        
    /**
     * Adapt fluxes: adapt a field.
     * @param fi                The flux information
     * @param params            The flux parameters
     * @param originalParam     The original flux parameter
     * @param originalParent    The original parent of the element
     * @param result            The result document
     * @param pi				Problem information
     * @throws AGDMException    AGDM007 External error
     */
    private static void adaptField(FluxInfo fi, LinkedHashMap < String, String > params, Element originalParam, 
            Element originalParent, Document result, ProcessInfo pi) throws AGDMException {
    	pi.setField(fi.getField());
    	boolean isStepVariable = pi.isStepVariable(originalParam, true);
        Iterator <String> paramIt = params.keySet().iterator();
        while (paramIt.hasNext()) {
            String param = paramIt.next();
            if (params.get(param).equals("field") || params.get(param).equals("variable")) {
            	pi.setField(param);
                //Create the new field based on the original one
            	Element newField;
            	//Auxiliary variable
            	if (pi.getAuxiliaryVariables().contains(param)) {
            		newField = (Element) AGDMUtils.removeIndex(originalParam.cloneNode(true));
                    Element fieldCi = (Element) AGDMUtils.find(newField, ".//mt:ci[text()='" + fi.getField() + "']").item(0);
                    fieldCi.setTextContent(param);
            	}
            	else {
                	//Auxiliary field
	            	if (pi.getRegionInfo().isAuxiliaryField(param) && isStepVariable) {
	            		newField = ExecutionFlow.replaceTags(AGDMUtils.createTimeSliceWithExternalIndex(result, originalParam.cloneNode(true)), pi);
	            	}
                	//Evolution field
	            	else {
	                    newField = (Element) originalParam.cloneNode(true);
	                    Element fieldCi = (Element) AGDMUtils.find(newField, ".//mt:ci[text()='" + fi.getField() + "']").item(0);
	                    fieldCi.setTextContent(param);
	            	}
            	}

                //Add the new field as a parameter for the flux
                originalParent.appendChild(result.importNode(newField, true));
            }
            else {
                Element ciParam = AGDMUtils.createElement(result, mtUri, "ci");
                ciParam.setTextContent(param);
                originalParent.appendChild(ciParam);
            }
        }
    }
    
    /**
     * Adapt fluxes: adapt a variable.
     * @param fi                The flux information
     * @param params            The flux parameters
     * @param originalParam     The original flux parameter
     * @param originalParent    The original parent of the element
     * @param result            The result document
     * @param ci                The variable element
     * @param pi				Problem information
     * @throws AGDMException    AGDM002 XML discretization schema not valid    
     *                          AGDM007 External error
     */
    private static void adaptVariable(FluxInfo fi, LinkedHashMap < String, String > params, Element originalParam, 
            Element originalParent, Document result, Element ci, ProcessInfo pi) throws AGDMException {
    	pi.setField(fi.getField());
    	boolean isStepVariable = pi.isStepVariable(originalParam, true);
        Iterator <String> paramIt = params.keySet().iterator();
        while (paramIt.hasNext()) {
            String param = paramIt.next();
            if (params.get(param).equals("field") || params.get(param).equals("variable")) {
                try {
                	Element newParam;
                  	//Auxiliary variable
                	if (pi.getAuxiliaryVariables().contains(param)) {
                		newParam = (Element) AGDMUtils.removeIndex(originalParam.cloneNode(true));
	                    Element newCi;
	                    String newVarName;
	                    NodeList ends = AGDMUtils.find(newParam, ".//ancestor::mt:ci[ends-with(text()[1],'" + fi.getField() + "')]");
	                    if (ends.getLength() > 0) {
	                        newCi = (Element) ends.item(0);
	                        if (newCi.getTextContent().lastIndexOf(fi.getField()) == 0) {
	                            newVarName = param;
	                        }
	                        else {
	                            newVarName = 
	                                newCi.getTextContent().substring(0, newCi.getTextContent().lastIndexOf(fi.getField())) + param;
	                        }
	                    } 
	                    else {
	                        newCi = (Element) 
	                            AGDMUtils.find(newParam, ".//ancestor::mt:ci[starts-with(text()[1],'" + fi.getField() + "')]").item(0);
	                        if (newCi.getTextContent().equals(fi.getField())) {
	                            newVarName = param;
	                        }
	                        else {
	                            newVarName = 
	                                param + newCi.getTextContent().substring(fi.getField().length());
	                        }
	                    }
	                    newCi.setTextContent(newVarName);
                	}
                	else {
	                	if (pi.getRegionInfo().isAuxiliaryField(param) && isStepVariable) {
	                		pi.setField(param);
	                		newParam = ExecutionFlow.replaceTags(AGDMUtils.createTimeSliceWithExternalIndex(result, originalParam.cloneNode(true)), pi);
	                	}
	                	else {
		                    newParam = (Element) originalParam.cloneNode(true);
		                    Element newCi;
		                    String newVarName;
		                    NodeList ends = AGDMUtils.find(newParam, ".//ancestor::mt:ci[ends-with(text()[1],'" + fi.getField() + "')]");
		                    if (ends.getLength() > 0) {
		                        newCi = (Element) ends.item(0);
		                        if (newCi.getTextContent().lastIndexOf(fi.getField()) == 0) {
		                            newVarName = param;
		                        }
		                        else {
		                            newVarName = 
		                                newCi.getTextContent().substring(0, newCi.getTextContent().lastIndexOf(fi.getField())) + param;
		                        }
		                    } 
		                    else {
		                        newCi = (Element) 
		                            AGDMUtils.find(newParam, ".//ancestor::mt:ci[starts-with(text()[1],'" + fi.getField() + "')]").item(0);
		                        if (newCi.getTextContent().equals(fi.getField())) {
		                            newVarName = param;
		                        }
		                        else {
		                            newVarName = 
		                                param + newCi.getTextContent().substring(fi.getField().length());
		                        }
		                    }
		                    newCi.setTextContent(newVarName);
	                	}
                	}
                    originalParent.appendChild(result.importNode(newParam, true));
                }
                catch (IndexOutOfBoundsException e) {
                    throw new AGDMException(AGDMException.AGDM002, 
                            "Variables for fields must end or start with the field: " + ci.getTextContent());
                }
            }
            else {
                Element ciParam = AGDMUtils.createElement(result, mtUri, "ci");
                ciParam.setTextContent(param);
                originalParent.appendChild(ciParam);
            }
        }
    }
    
}
