/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.AGDM.utils.PDE;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Optional;
import java.util.stream.Collectors;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import eu.iac3.mathMS.AGDM.AGDMException;
import eu.iac3.mathMS.AGDM.utils.AGDMUtils;
import eu.iac3.mathMS.AGDM.utils.RegionInfo;
import eu.iac3.mathMS.documentManagerPlugin.DMException;
import eu.iac3.mathMS.documentManagerPlugin.DocumentManager;
import eu.iac3.mathMS.documentManagerPlugin.DocumentManagerImpl;
import eu.iac3.mathMS.simflownyUtilsPlugin.SUException;
import eu.iac3.mathMS.simflownyUtilsPlugin.SimflownyUtils;


/**
 * Discretization policy of a region.
 * @author bminyano
 *
 */
public class PDERegionDiscPolicy extends AbstractPDEDiscPolicy {

    private String timeSchema;
    private String regionName;
    private FieldGroupMap[] fieldGroupMaps;
    private ArrayList<String> fieldGroups;
    private HashMap<String, OperatorPolicy> operatorPolicies;
    private int schemaId;
    private String regionId;
    private int stepsNumber;
    private ArrayList<Float> timestepFactors;
    private ArrayList<Element> timestepOutputVariableTemplates;
    private RegionInfo regionInfo;
    
    private Document schemaDoc;
    
    /**
     * Constructor.
     */
    public PDERegionDiscPolicy() {
    }
    
    /**
     * Constructor.
     * @param rdp  The region discretization policy xml
     * @throws AGDMException 
     * @throws DMException 
     * @throws SUException 
     */
    public PDERegionDiscPolicy(Node rdp) throws AGDMException, DMException, SUException {
        regionName =  ((Element) rdp).getElementsByTagNameNS(AGDMUtils.mmsUri, "region").item(0).getTextContent();
        timeSchema = ((Element) rdp).getElementsByTagNameNS(AGDMUtils.mmsUri, "timeIntegrationSchema").item(0).getLastChild().getTextContent();
        //Operator discretizations
        NodeList opDiscs = ((Element) rdp).getElementsByTagNameNS(AGDMUtils.mmsUri, "operatorDiscretization");
        operatorDiscretizations = new ArrayList<OperatorDiscretization>();
        for (int i = 0; i < opDiscs.getLength(); i++) {
            OperatorDiscretization od = new OperatorDiscretization();
            od.setOperator(opDiscs.item(i).getFirstChild().getTextContent());
            od.setSchema(opDiscs.item(i).getLastChild().getLastChild().getTextContent());
            operatorDiscretizations.add(od);
        }
        //Load all the operator policies
        operatorPolicies = super.loadOperatorPolicies();
        //Field group maps
        NodeList fieldGroupMapsLocal = ((Element) rdp).getElementsByTagName("mms:fieldGroupMaps");
        if (fieldGroupMapsLocal.getLength() > 0) {
            fieldGroupMapsLocal = fieldGroupMapsLocal.item(0).getChildNodes();
            this.fieldGroupMaps = new FieldGroupMap[fieldGroupMapsLocal.getLength()];
            for (int i = 0; i < fieldGroupMapsLocal.getLength(); i++) {
                FieldGroupMap fgm = new FieldGroupMap();
                fgm.setSchema(fieldGroupMapsLocal.item(i).getFirstChild().getTextContent());
                fgm.setRegion(fieldGroupMapsLocal.item(i).getLastChild().getTextContent());
                this.fieldGroupMaps[i] = fgm;
            }
        }
        else {
            this.fieldGroupMaps = new FieldGroupMap[0];
        }
        
        //Field groups
        this.fieldGroups = new ArrayList<String>();
        NodeList fieldGroupsLocal = ((Element) rdp).getElementsByTagName("mms:fieldGroupName");
        if (fieldGroupsLocal.getLength() > 0) {
            for (int i = 0; i < fieldGroupsLocal.getLength(); i++) {
                this.fieldGroups.add(fieldGroupsLocal.item(i).getTextContent());
            }
        }
        
        this.regionId = regionName + this.fieldGroups.stream().collect(Collectors.joining("_"));
        
        timestepOutputVariableTemplates = new ArrayList<Element>();
        timestepFactors = new ArrayList<Float>();
        DocumentManager docMan = new DocumentManagerImpl();
        String schema = docMan.getDocument(timeSchema);
        schemaDoc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(schema));
        NodeList schemaSteps = schemaDoc.getElementsByTagNameNS(AGDMUtils.mmsUri, "step");
        stepsNumber = schemaSteps.getLength();
        for (int i = 0; i < schemaSteps.getLength(); i++) {
        	Element step = (Element) schemaSteps.item(i);
        	timestepOutputVariableTemplates.add((Element) ((Element) step.getElementsByTagNameNS(AGDMUtils.mmsUri, "timeDiscretization").item(0))
        			.getElementsByTagNameNS(AGDMUtils.mmsUri, "outputVariable").item(0).getFirstChild().cloneNode(true));

        	timestepFactors.add(Float.parseFloat(step.getElementsByTagNameNS(AGDMUtils.mmsUri, "timestepFactor").item(0).getTextContent()));
        }
        
    }
    
    /**
     * Creates Region information structure.
     * @param problem			Problem data
     * @throws AGDMException
     */
    public void createRegionInformation(Node problem) throws AGDMException {
        ArrayList<String> fields;
        //Creation of the region information with the coordinates and fields. Deploy the coordinates in the initial conditions
        Node region = AGDMUtils.find(problem, "//mms:region[mms:name = '" + regionName + "']|//mms:subregion[mms:name = '" + regionName + "']").item(0);

        fields = new ArrayList<String>();
        
        NodeList models = AGDMUtils.find(region, ".//mms:interiorModel|.//mms:surfaceModel");
        for (int j = 0; j < models.getLength(); j++) {
            String modelName = models.item(j).getTextContent();
            
            //Fill fields and auxiliary fields of the models
            NodeList fieldList = AGDMUtils.find(problem, 
                    "//mms:model[mms:modelName[text() = '" + modelName + "']]/mms:fieldEquivalence//mms:problemElement");
            for (int k = 0; k < fieldList.getLength(); k++) {
                if (!fields.contains(fieldList.item(k).getTextContent())) {
                    fields.add(fieldList.item(k).getTextContent());
                }
            }
        }
        //Fill fields from surface boundaries
        NodeList fieldList = AGDMUtils.find(problem, 
                "/*/mms:fields/mms:field|/*/mms:auxiliaryFields/mms:auxiliaryField");
        HashMap<String, Boolean> isAuxiliary = new HashMap<String, Boolean>();
        for (int j = 0; j < fieldList.getLength(); j++) {
            if (!fields.contains(fieldList.item(j).getTextContent())) {
                fields.add(fieldList.item(j).getTextContent());
                isAuxiliary.put(fieldList.item(j).getTextContent(), fieldList.item(j).getLocalName().equals("auxiliaryField"));
            }
        }
        RegionInfo ri = new RegionInfo(fields, isAuxiliary);
        ri.fillGroup(this, (Element) region);
        ri.applyGroupDiscretizationFilter(fieldGroups);
        
    	this.regionInfo = ri;

    }
    
    public Document getSchemaDoc() {
		return schemaDoc;
	}

	public RegionInfo getRegionInfo() {
		return regionInfo;
	}

	public int getSchemaId() {
		return schemaId;
	}

	public void setSchemaId(int schemaId) {
		this.schemaId = schemaId;
	}

	public void setOperatorDiscretization(ArrayList<OperatorDiscretization> opDiscs) {
        operatorDiscretizations = opDiscs;
    }

    public String getTimeSchema() {
        return timeSchema;
    }

    public void setTimeSchema(String ts) {
        timeSchema = ts;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String r) {
        regionName = r;
    }
    
    public HashMap<String, OperatorPolicy> getOperatorPolicies() {
        return operatorPolicies;
    }
       
    public FieldGroupMap[] getFieldGroupMaps() {
		return fieldGroupMaps;
	}

	public ArrayList<String> getFieldGroups() {
		return fieldGroups;
	}

	public String getRegionId() {
		return regionId;
	}
	
	public int getStepsNumber() {
		return stepsNumber;
	}

	public ArrayList<Float> getTimestepFactors() {
		return timestepFactors;
	}

	public ArrayList<Element> getTimestepOutputVariableTemplates() {
		return timestepOutputVariableTemplates;
	}

	/**
	 * Gets a suffix for the current field groups.
	 * @return	Suffix joining with underscores
	 */
	public String getFieldGroupSuffix() {
		return fieldGroups.stream().collect(Collectors.joining("_"));
	}
	
	/**
     * Returns the spatial operator discretization compatible with the parameters.
     * @param order             The order of the solver
     * @param coordinates       The coordinate combination
     * @param operatorName      The name of the operator to solve
     * @return                  The discretization if exists, otherwise returns null
	 * @throws AGDMException    AGDM007 Invalid discretization policy
     */
    @Override
    public Optional<SpatialOperatorDiscretization> getCompatiblePolicy(int order, String coordinates, String operatorName) throws AGDMException {
        for (int i = 0; i < operatorDiscretizations.size(); i++) {
            OperatorDiscretization operatorDiscretization = operatorDiscretizations.get(i);
            if (operatorDiscretization.getOperator().equals(operatorName)) {
                String opDiscId = operatorDiscretization.getSchema();
                ArrayList<SpatialOperatorDiscretization> spatialDiscs = operatorPolicies.get(opDiscId).getDiscretization();
                for (int j = 0; j < spatialDiscs.size(); j++) {
                    SpatialOperatorDiscretization spatialDisc = spatialDiscs.get(j);
                    if (order == spatialDisc.getOrder() && coordinates.equals(spatialDisc.getCoordCombination())) {
                        return Optional.of(spatialDisc);
                    }
                }
            }
        }
        return Optional.empty();
    }
    
	@Override
	public String toString() {
		return "PDERegionDiscPolicy [timeSchema=" + timeSchema + ", region=" + regionName + ", fieldGroupMaps="
				+ Arrays.toString(fieldGroupMaps) + ", operatorPolicies=" + operatorPolicies + ", schemaId=" + schemaId
				+ "]";
	}
    
}
