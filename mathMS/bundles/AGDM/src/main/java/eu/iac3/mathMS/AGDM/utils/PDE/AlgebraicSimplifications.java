package eu.iac3.mathMS.AGDM.utils.PDE;

import java.util.ArrayList;

public class AlgebraicSimplifications {

    private ArrayList<String> formerVariables;
    private ArrayList<String> replacementVariables;
    
    /**
     * Default constructor.
     */
    public AlgebraicSimplifications() {
        formerVariables = new ArrayList<String>();
        replacementVariables = new ArrayList<String>();
    }
    
    /**
     * Add simplification.
     * @param oldVar    The old variable
     * @param newVar    The new variable
     */
    public void addSimplification(String oldVar, String newVar) {
        formerVariables.add(oldVar);
        replacementVariables.add(newVar);
    }
    
    /**
     * Get the variable to be used instead of the one provided.
     * @param var   The variable to be replaced
     * @return      The variable to use
     */
    public String getSimplifiedVariable(String var) {
        if (formerVariables.contains(var)) {
            return replacementVariables.get(formerVariables.indexOf(var));
        }
        return null;
    }
}
