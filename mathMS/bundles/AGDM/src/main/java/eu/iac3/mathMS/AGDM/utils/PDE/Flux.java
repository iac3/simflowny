/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.AGDM.utils.PDE;

import java.util.Optional;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

import eu.iac3.mathMS.AGDM.AGDMException;
import eu.iac3.mathMS.AGDM.utils.AGDMUtils;

/**
 * Flux class.
 * @author bminano
 *
 */
public class Flux {
	private String field;
	private String coordinate;
	private boolean simple;
	private Element math;
	private Node conditional;
	
	/**
	 * Copy constructor.
	 * @param f	copy object
	 */
	public Flux(Flux f) {
		this.field = f.field;
		this.coordinate = f.coordinate;
		this.math = (Element) f.math.cloneNode(true);
		if (math.getFirstChild().getLocalName().equals("apply")) {
			simple = false;
		}
		else {
			simple = true;
		}
        if (f.conditional != null) {
        	conditional = f.conditional.cloneNode(true);
        }
	}
	
	/**
	 * Constructor.
	 * @param field	field
	 * @param coord	coordinate
	 * @param math	math element
	 */
	public Flux(String field, String coord, Element math, Node condition) {
		this.field = field;
		this.coordinate = coord;
		this.math = (Element) math.cloneNode(true);
		if (math.getFirstChild().getLocalName().equals("apply")) {
			simple = false;
		}
		else {
			simple = true;
		}
        if (condition != null) {
        	conditional = condition.cloneNode(true);
        }
	}
	public String getField() {
		return field;
	}
	public void setField(String field) {
		this.field = field;
	}
	public String getCoordinate() {
		return coordinate;
	}
	public void setCoordinate(String coord) {
		this.coordinate = coord;
	}
	public boolean isSimple() {
		return simple;
	}
	public void setSimple(boolean simple) {
		this.simple = simple;
	}
	public Element getMath() {
		return math;
	}
	public void setMath(Element math) {
		this.math = math;
	}
	

	public Node getConditional() {
		if (conditional == null) {
			return null;
		}
		return conditional.cloneNode(true);
	}

	public void setConditional(Node conditional) {
		this.conditional = conditional;
	}

	@Override
	public String toString() {
        String formulaString = "empty";
        if (math != null) {
            try {
            	formulaString = AGDMUtils.domToString(math);
            } 
            catch (AGDMException e) {
            }
        }
        Optional<Node> op = Optional.ofNullable(conditional);
        String condition = op.map( a -> {
			try {
				return AGDMUtils.domToString(a);
			} 
			catch (AGDMException e) {
				return "empty";
			}
		}).orElse("empty");
		return "Flux [field=" + field + ", coordinate=" + coordinate + ", simple=" + simple + ", math=" + formulaString + ", condition=" + condition + "]";
	}
	
}
