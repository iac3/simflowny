/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.AGDM.processors;

import eu.iac3.mathMS.AGDM.AGDMException;
import eu.iac3.mathMS.AGDM.utils.AGDMUtils;
import eu.iac3.mathMS.AGDM.utils.CoordinateInfo;
import eu.iac3.mathMS.AGDM.utils.DiscretizationType;
import eu.iac3.mathMS.AGDM.utils.RuleInfo;
import eu.iac3.mathMS.AGDM.utils.PDE.EquationTerms;
import eu.iac3.mathMS.AGDM.utils.PDE.EquationType;
import eu.iac3.mathMS.AGDM.utils.PDE.OperatorsInfo;
import eu.iac3.mathMS.AGDM.utils.PDE.ProcessInfo;
import eu.iac3.mathMS.AGDM.utils.RegionInfo;

import java.util.ArrayList;
import java.util.Map.Entry;

import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * This class have all the necessary methods to process the boundaries.
 * @author bminyano
 *
 */
public final class Boundary {
    static String mmsUri = "urn:mathms";
    static String smlUri = "urn:simml";
    static String mtUri = "http://www.w3.org/1998/Math/MathML";
    
    /**
     * Constructor.
     * @throws AGDMException 
     */
    public Boundary() throws AGDMException {
    };
    
    /**
     * Creates a boundary depending the type specified in the case and the data in the schema.
     * @param result                The problem being discretized
     * @param problem               The document with the info
     * @param rule                  The boundary rule in the schema
     * @param pi                    The process information
     * @param postInit              True if the boundaries are invoked from post initialization
     * @param lastBoundary			True if last boundary
     * @param withAuxiliaries		If boundaries have to be generated with auxiliaries
     * @param withFields			If boundaries have to be generated with fields
     * @return                      The math in the boundary
     * @throws AGDMException        AGDM007 External error
     */
    public static DocumentFragment processBoundary(Document result, Node problem, Element rule, ProcessInfo pi, boolean lastBoundary, 
            boolean postInit, boolean withAuxiliaries, boolean withFields) throws AGDMException {
        //Result is stored in document fragment
        DocumentFragment fragmentResult = result.createDocumentFragment();
        Document ruleDoc = rule.getOwnerDocument();
        Element boundaryRule = (Element) rule.cloneNode(false);
        Element timeSlice = (Element) rule.getElementsByTagName("sml:variable").item(0).getFirstChild().cloneNode(true);
        Element previousTimeSlice = (Element) rule.getElementsByTagName("sml:previousVariable").item(0).getFirstChild().cloneNode(true);
        int stencil = Integer.parseInt(rule.getElementsByTagName("sml:stencil").item(0).getTextContent()); 
        boundaryRule.setAttribute("stencilAtt", Integer.toString(stencil));
        
        //Only for boundary regions
        if (AGDMUtils.find(problem, "//mms:boundaryPolicy").getLength() > 0) {
            ArrayList<String> coords = pi.getCoordinateRelation().getDiscCoords();
            for (int i = 0; i < coords.size(); i++) {
            	String contCoord = pi.getCoordinateRelation().getContCoords().get(i);
                String query = "//mms:boundaryCondition[mms:axis ='" + contCoord + "' or lower-case(mms:axis) ='all']";
                NodeList boundList = AGDMUtils.find(problem, query);
                //Axis iteration
                for (int k = 0; k < boundList.getLength(); k++) {
                    Element boundary = (Element) boundList.item(k);
                    Element coordTag = AGDMUtils.createElement(problem.getOwnerDocument(), smlUri, "axis");
                    coordTag.setAttribute("axisAtt", coords.get(i));
                    //Process boundary
                    String boundaryType = boundary.getElementsByTagNameNS(mmsUri, "type").item(0).getFirstChild().getLocalName();
                    //PERIODICAL
                    if (boundaryType.toLowerCase().equals("periodical")) {
                        Node periodical = getPeriodicalBoundary(timeSlice);
                        coordTag.appendChild(problem.getOwnerDocument().importNode(periodical.cloneNode(true), true));
                    }
                    else {
                    	String side = boundary.getElementsByTagNameNS(mmsUri, "side").item(0).getTextContent();
                        Element lower = AGDMUtils.createElement(problem.getOwnerDocument(), smlUri, "side");
                        lower.setAttribute("sideAtt", "lower");
                        Element upper = AGDMUtils.createElement(problem.getOwnerDocument(), smlUri, "side");
                        upper.setAttribute("sideAtt", "upper");
                        //if there is a condition generate the if element
                        Element condition = null;
                        if (((Element) boundList.item(k)).getElementsByTagName("mms:applyIf").getLength() > 0) {
                            condition = generateLimitCondition((((Element) boundList.item(k)).getElementsByTagName("mms:applyIf")
                                        .item(0)).cloneNode(true));
                        }
                        //Mesh
                        ArrayList<ProcessInfo> pis = AGDMUtils.generateProcessInfoForBoundary(problem, boundList.item(k), pi, pi.getMeshFields(), withAuxiliaries, withFields);
                        if (pis.size() > 0) {
                        	pi.setDiscretizationType(DiscretizationType.mesh);
                            DocumentFragment limit = problem.getOwnerDocument().createDocumentFragment();
                        	//STATIC
                        	if (boundaryType.toLowerCase().equals("static")) {
                                for (int l = 0; l < pis.size(); l++) {
                                    limit.appendChild(problem.getOwnerDocument().importNode(
                                        getStaticBoundary(timeSlice, previousTimeSlice, pis.get(l), coords.get(i)), true));
                                }
                                limit.appendChild(problem.getOwnerDocument().importNode(getStaticBoundaryAuxiliaries(result, problem, timeSlice, pi, boundary, pi.getMeshFields()), true));

    	                        if (side.toLowerCase().equals("lower") || side.toLowerCase().equals("all")) {
    	                            //insert the boundary in the condition if exist
    	                            if (condition == null) {
    	                                lower.appendChild(limit.cloneNode(true));
    	                            }
    	                            else {
    	                                Element conditionClone = (Element) condition.cloneNode(true);
    	                                for (int m = 0; m < limit.getChildNodes().getLength(); m++) {
    	                                    conditionClone.getLastChild().appendChild(limit.getChildNodes().item(m).cloneNode(true));
    	                                }
    	                                lower.appendChild(conditionClone);
    	                            }
    	                        }
    	                        if (side.toLowerCase().equals("upper") || side.toLowerCase().equals("all")) {
    	                            //insert the boundary in the condition if exist
    	                            if (condition == null) {
    	                                upper.appendChild(limit.cloneNode(true));
    	                            }
    	                            else {
    	                                Element conditionClone = (Element) condition.cloneNode(true);
    	                                for (int m = 0; m < limit.getChildNodes().getLength(); m++) {
    	                                    conditionClone.getLastChild().appendChild(limit.getChildNodes().item(m).cloneNode(true));
    	                                }
    	                                upper.appendChild(conditionClone);
    	                            }
    	                        }
    	                    }
    	                    //FLAT
    	                    if (boundaryType.toLowerCase().equals("flat")) {
    	                        if (side.toLowerCase().equals("lower") || side.toLowerCase().equals("all")) {
    	                            //insert the boundary in the condition if exist
    	                            if (condition == null) {
    	                                //Create boundaries for every field except if the boundary depends on vectors
    	                                for (int l = 0; l < pis.size(); l++) {
    	                                	lower.appendChild(problem.getOwnerDocument().importNode(getFlatBoundary(timeSlice, pis.get(l), 
    	                                			coords.get(i), "lower"), true));
    	                                }
    	                            }
    	                            else {
    	                                Element conditionClone = (Element) condition.cloneNode(true);
    	                                //Create boundaries for every field except if the boundary depends on vectors
    	                                for (int l = 0; l < pis.size(); l++) {
                                            conditionClone.getLastChild().appendChild(problem.getOwnerDocument().importNode(getFlatBoundary(
                                                    timeSlice, pis.get(l), coords.get(i), "lower").getFirstChild(), true));
    	                    
    	                                }
    	                                lower.appendChild(conditionClone);
    	                            }
    	                        }
    	                        if (side.toLowerCase().equals("upper") || side.toLowerCase().equals("all")) {
    	                            //insert the boundary in the condition if exist
    	                            if (condition == null) {
    	                                //Create boundaries for every field except if the boundary depends on vectors
    	                                for (int l = 0; l < pis.size(); l++) {
                                            upper.appendChild(problem.getOwnerDocument().importNode(getFlatBoundary(timeSlice, pis.get(l), 
                                                    coords.get(i), "upper"), true));
    	                                }
    	                            }
    	                            else {
    	                                Element conditionClone = (Element) condition.cloneNode(true);
    	                                //Create boundaries for every field except if the boundary depends on vectors
    	                                for (int l = 0; l < pis.size(); l++) {
                                            conditionClone.getLastChild().appendChild(problem.getOwnerDocument().importNode(getFlatBoundary(
                                                    timeSlice, pis.get(l), coords.get(i), "upper").getFirstChild(), true));
    	                                }
    	                                upper.appendChild(conditionClone);
    	                            }
    	                        }
    	                    }
    	                    //REFLECTION
    	                    if (boundaryType.toLowerCase().equals("reflection")) {
    	                    	if (side.toLowerCase().equals("lower") || side.toLowerCase().equals("all")) {
    	                    		lower = processReflectionMesh(boundList.item(k), condition, 
    	                                (Element) timeSlice, coords, i, pis, "lower");
    	                    	}
    	                    	if (side.toLowerCase().equals("upper") || side.toLowerCase().equals("all")) {
    	                    		upper = processReflectionMesh(boundList.item(k), condition, 
    	                                (Element) timeSlice, coords, i, pis, "upper");
    	                    	}
    	                    }
    	                    //ALGEBRAIC
    	                    if (boundaryType.toLowerCase().equals("algebraic")) {
    	                        for (int l = 0; l < pis.size(); l++) {
                                    limit.appendChild(problem.getOwnerDocument().importNode(
                                        getAlgebraicBoundary(pi.getCompleteProblem(), boundList.item(k), timeSlice, pis.get(l), coords.get(i)), true));
    	                        }
    	                        if (side.toLowerCase().equals("lower") || side.toLowerCase().equals("all")) {
    	                            //insert the boundary in the condition if exist
    	                            if (condition == null) {
    	                                lower.appendChild(limit.cloneNode(true));
    	                            }
    	                            else {
    	                                Element conditionClone = (Element) condition.cloneNode(true);
    	                                for (int m = 0; m < limit.getChildNodes().getLength(); m++) {
    	                                    conditionClone.getLastChild().appendChild(limit.getChildNodes().item(m).cloneNode(true));
    	                                }
    	                                lower.appendChild(conditionClone);
    	                            }
    	                        }
    	                        if (side.toLowerCase().equals("upper") || side.toLowerCase().equals("all")) {
    	                            //insert the boundary in the condition if exist
    	                            if (condition == null) {
    	                                upper.appendChild(limit.cloneNode(true));
    	                            }
    	                            else {
    	                                Element conditionClone = (Element) condition.cloneNode(true);
    	                                for (int m = 0; m < limit.getChildNodes().getLength(); m++) {
    	                                    conditionClone.getLastChild().appendChild(limit.getChildNodes().item(m).cloneNode(true));
    	                                }
    	                                upper.appendChild(conditionClone);
    	                            }
    	                        }
    	                    }
    	                    //MAXIMAL DISSIPATION
    	                    if (boundaryType.toLowerCase().equals("maximaldissipation")) {
                                if (pis.size() > 0) {
        	                    	if (side.toLowerCase().equals("lower") || side.toLowerCase().equals("all")) {
        	                    		lower = processMaximalDissipation(result, problem, condition, timeSlice, previousTimeSlice, pis, coords.get(i), 
        	                    				"lower", postInit);
        	                    	}
        	                    	if (side.toLowerCase().equals("upper") || side.toLowerCase().equals("all")) {
        	                    		upper = processMaximalDissipation(result, problem, condition, timeSlice, previousTimeSlice, pis, coords.get(i), 
        	                    				"upper", postInit);
        	                    	}
                                    
                                }
    	                    }
                        }
                        //Particles
                        for (Entry<String, OperatorsInfo> entry: pi.getOperatorsInfoParticles().entrySet()) {
                        	pi.setCurrentSpecies(entry.getKey());
                            pis = AGDMUtils.generateProcessInfoForBoundary(problem, boundList.item(k), pi, pi.getParticleFields(), withAuxiliaries, withFields);
                        	if (pis.size() > 0) {
                            	pi.setDiscretizationType(DiscretizationType.particles);
                                Element limit = AGDMUtils.createElement(problem.getOwnerDocument(), smlUri, "iterateOverParticles");
                                limit.setAttribute("speciesNameAtt", entry.getKey());
        	                    //STATIC and MAXIMAL DISSIPATION
        	                    if (boundaryType.toLowerCase().equals("static") || boundaryType.toLowerCase().equals("maximaldissipation")) {
                                    limit.appendChild(problem.getOwnerDocument().importNode(
                                        getStaticParticleBoundary(result, problem, boundList.item(k), timeSlice, previousTimeSlice, pi, coords.get(i), 
                                        		lastBoundary, postInit, rule, withAuxiliaries, withFields), true));
        	                        if (side.toLowerCase().equals("lower") || side.toLowerCase().equals("all")) {
        	                            //insert the boundary in the condition if exist
        	                            if (condition == null) {
        	                                lower.appendChild(limit.cloneNode(true));
        	                            }
        	                            else {
        	                                Element conditionClone = (Element) condition.cloneNode(true);
        	                                for (int m = 0; m < limit.getChildNodes().getLength(); m++) {
        	                                    conditionClone.getLastChild().appendChild(limit.getChildNodes().item(m).cloneNode(true));
        	                                }
        	                                lower.appendChild(conditionClone);
        	                            }
        	                        }
        	                        if (side.toLowerCase().equals("upper") || side.toLowerCase().equals("all")) {
        	                            //insert the boundary in the condition if exist
        	                            if (condition == null) {
        	                                upper.appendChild(limit.cloneNode(true));
        	                            }
        	                            else {
        	                                Element conditionClone = (Element) condition.cloneNode(true);
        	                                for (int m = 0; m < limit.getChildNodes().getLength(); m++) {
        	                                    conditionClone.getLastChild().appendChild(limit.getChildNodes().item(m).cloneNode(true));
        	                                }
        	                                upper.appendChild(conditionClone);
        	                            }
        	                        }
        	                    }
                            	//FLAT boundary not valid for particles
        	                    if (boundaryType.toLowerCase().equals("flat")) {
        	                    	throw new AGDMException(AGDMException.AGDM006, 
        	                    	        "Flat boundaries not applicable on particles. Use Static or Maximal Dissipation instead.");
        	                    }
        	                    //REFLECTION
        	                    if (boundaryType.toLowerCase().equals("reflection")) {
        	                    	if (side.toLowerCase().equals("lower") || side.toLowerCase().equals("all")) {
        	                    		lower.appendChild(processReflectionParticles(boundList.item(k), condition, 
            	                                (Element) timeSlice, coords, i, pis, postInit, "lower"));
        	                    	}
        	                    	if (side.toLowerCase().equals("upper") || side.toLowerCase().equals("all")) {
        	                    		upper.appendChild(processReflectionParticles(boundList.item(k), condition, 
            	                                (Element) timeSlice, coords, i, pis, postInit, "upper"));
        	                    	}
        	                    }
        	                    //ALGEBRAIC
        	                    if (boundaryType.toLowerCase().equals("algebraic")) {
        	                        for (int l = 0; l < pis.size(); l++) {
                                        limit.appendChild(problem.getOwnerDocument().importNode(
                                            getAlgebraicBoundary(pi.getCompleteProblem(), boundList.item(k), timeSlice, pis.get(l), coords.get(i)), true));
        	                        }
        	                        if (side.toLowerCase().equals("lower") || side.toLowerCase().equals("all")) {
        	                            //insert the boundary in the condition if exist
        	                            if (condition == null) {
                                            DocumentFragment inst = processPostInit(result, problem, pi, timeSlice, rule.getOwnerDocument());
                                            if (inst.hasChildNodes()) {
                                            	limit.appendChild(limit.getOwnerDocument().importNode(inst, true));
                                            }
                                            if (lastBoundary || postInit) {
                                            	limit.appendChild(createNewRegionAssignment(pi));
                                            }
                                            lower.appendChild(limit.cloneNode(true));
        	                            }
        	                            else {
        	                            	Element iterOverParticles = (Element) limit.cloneNode(false);
        	                                Element conditionClone = (Element) condition.cloneNode(true);
        	                                for (int m = 0; m < limit.getFirstChild().getChildNodes().getLength(); m++) {
        	                                    conditionClone.getLastChild().appendChild(limit.getFirstChild().getChildNodes().item(m).cloneNode(true));
        	                                }
                                            DocumentFragment inst = processPostInit(result, problem, pi, timeSlice, rule.getOwnerDocument());
                                            if (inst.hasChildNodes()) {
                                                conditionClone.getLastChild().appendChild(conditionClone.getOwnerDocument().importNode(inst, true));
                                            }
                                            if (lastBoundary || postInit) {
                                                conditionClone.getLastChild().appendChild(createNewRegionAssignment(pi));
                                            }
                                            iterOverParticles.appendChild(conditionClone);
        	                                lower.appendChild(iterOverParticles);
        	                            }
        	                        }
        	                        if (side.toLowerCase().equals("upper") || side.toLowerCase().equals("all")) {
        	                            //insert the boundary in the condition if exist
        	                            if (condition == null) {
                                            DocumentFragment inst = processPostInit(result, problem, pi, timeSlice, rule.getOwnerDocument());
                                            if (inst.hasChildNodes()) {
                                            	limit.appendChild(upper.getOwnerDocument().importNode(inst, true));
                                            }
                                            if (lastBoundary || postInit) {
                                            	limit.appendChild(createNewRegionAssignment(pi));
                                            }
                                            upper.appendChild(limit.cloneNode(true));
        	                            }
        	                            else {
        	                            	Element iterOverParticles = (Element) limit.cloneNode(false);
        	                                Element conditionClone = (Element) condition.cloneNode(true);
        	                                for (int m = 0; m < limit.getFirstChild().getChildNodes().getLength(); m++) {
        	                                    conditionClone.getLastChild().appendChild(limit.getFirstChild().getChildNodes().item(m).cloneNode(true));
        	                                }
                                            DocumentFragment inst = processPostInit(result, problem, pi, timeSlice, rule.getOwnerDocument());
                                            if (inst.hasChildNodes()) {
                                                conditionClone.getLastChild().appendChild(conditionClone.getOwnerDocument().importNode(inst, true));
                                            }
                                            if (lastBoundary || postInit) {
                                                conditionClone.getLastChild().appendChild(createNewRegionAssignment(pi)); 
                                            }
                                            iterOverParticles.appendChild(conditionClone);
                                            upper.appendChild(iterOverParticles);
        	                            }
        	                        }
        	                    }
                            }
                        }
                        if (lower != null && (side.toLowerCase().equals("lower") || side.toLowerCase().equals("all"))) {
                        	coordTag.appendChild(lower);
                        }
                        if (upper != null && (side.toLowerCase().equals("upper") || side.toLowerCase().equals("all"))) {
                        	coordTag.appendChild(upper);
                        }
                    }
                    
                    //Add boundary if there is code inside
                    if (coordTag.getChildNodes().getLength() > 0) {
                        boundaryRule.appendChild(ruleDoc.importNode(coordTag, true));
                    }
                    
                }
            }
        }
        //Field extrapolation for Hard region boundaries
        if (pi.isExtrapolation()) {
            boundaryRule.appendChild(ruleDoc.importNode(Extrapolation.processExtrapolation(result, problem, timeSlice, pi), true));
        }
        fragmentResult.appendChild(result.importNode(boundaryRule, true));
        
        return fragmentResult;
    }

    /**
     * Process the maximal dissipation boundary type.
     * @param boundary          The boundary
     * @param condition         A condition for the boundary (if provided)
     * @param timeSlice         The time slice
     * @param coords            Coordinates of the problem
     * @param coodIndex         coordinate index
     * @param pis               The processes information
     * @param side              The side of the boundary
     * @return					The boundary
     * @throws AGDMException    AGDM007 External error
     */
    private static Element processReflectionMesh(Node boundary, Element condition, Element timeSlice, ArrayList<String> coords, 
            int coodIndex, ArrayList<ProcessInfo> pis, String side) throws AGDMException {
        Document boundDoc = boundary.getOwnerDocument();
        Element result = AGDMUtils.createElement(boundDoc, smlUri, "side");
        result.setAttribute("sideAtt", side);
        DocumentFragment reflectionCounter = createReflectionCounter(boundDoc, coords, coords.get(coodIndex), side, 
                pis.get(0).getCoordinateRelation());
        result.appendChild(reflectionCounter);
        Element conditionClone = null;
        if (condition != null) {
            conditionClone = (Element) condition.cloneNode(true);
        }
        //Create boundaries for every field except if the boundary depends on vectors
        for (int l = 0; l < pis.size(); l++) {
            ProcessInfo pi = pis.get(l);
            Element newTimeSlice = (Element) timeSlice.cloneNode(true);
            boolean signChange = false;
            DocumentFragment reflectionElement = 
                    getReflectionBoundaryMesh(boundary, newTimeSlice, pi, coords.get(coodIndex), side, signChange);
            if (reflectionElement.hasChildNodes()) {
                //insert the boundary in the condition if exist
                if (condition == null) {
                    result.appendChild(boundDoc.importNode(reflectionElement, true));
                }
                else {
                    conditionClone.getLastChild().appendChild(boundDoc.importNode(reflectionElement.getFirstChild(), true));
                }
            }
        }
        //insert the boundary in the condition if exist
        if (condition != null) {
            result.appendChild(conditionClone);
        }
        return result;
    }
    
    /**
     * Process the maximal dissipation boundary type.
     * @param boundary          The boundary
     * @param condition         A condition for the boundary (if provided)
     * @param timeSlice         The time slice
     * @param coords            Coordinates of the problem
     * @param coodIndex         coordinate index
     * @param pis               The processes information
     * @param postInit          True if the boundaries are invoked from post initialization
     * @param side              The side of the boundary
     * @return 					Boundary
     * @throws AGDMException    AGDM007 External error
     */
    private static DocumentFragment processReflectionParticles(Node boundary, Element condition, Element timeSlice, ArrayList<String> coords, 
            int coodIndex, ArrayList<ProcessInfo> pis, boolean postInit, String side) throws AGDMException {
        Document boundDoc = boundary.getOwnerDocument();
        DocumentFragment fragment = boundDoc.createDocumentFragment();
        
        if (!postInit) {
            Element limit = AGDMUtils.createElement(boundDoc, smlUri, "iterateOverParticles");
            limit.setAttribute("speciesNameAtt", pis.get(0).getCurrentSpecies());
            //Create boundaries for every field except if the boundary depends on vectors
            for (int l = 0; l < pis.size(); l++) {
                ProcessInfo pi = pis.get(l);
                DocumentFragment reflectionElement = 
                        getReflectionBoundaryParticles(boundary, timeSlice, pi, coords.get(coodIndex), side);
                if (reflectionElement.hasChildNodes()) {
                    //insert the boundary in the condition if exist
                    if (condition == null) {
                    	limit.appendChild(boundDoc.importNode(reflectionElement.cloneNode(true), true));
                    }
                    else {
                        Element conditionClone = (Element) condition.cloneNode(true);
                        conditionClone.getLastChild().appendChild(boundDoc.importNode(reflectionElement.getFirstChild(), true));
                        limit.appendChild(conditionClone);
                    }
                }
            }
            fragment.appendChild(limit);
        }
        return fragment;
    }
    
    /**
     * Process the maximal dissipation boundary type.
     * @param problem               The document with the info
     * @param condition             The condition element for the boundary
     * @param timeSlice             The timeSlice
     * @param previousTimeSlice     The previous timeSlice
     * @param pis                   The process information
     * @param coordinate            The coordinate to generate the boundary to
     * @param side                  The side of the boundary
     * @param postInit              True if the boundaries are invoked from post initialization
     * @return						Boundary
     * @throws AGDMException        AGDM007 External error
     */
    private static Element processMaximalDissipation(Document discProblem, Node problem, Element condition, Element timeSlice, Element previousTimeSlice,
            ArrayList<ProcessInfo> pis, String coordinate, String side, boolean postInit) throws AGDMException {
        
        Element result = AGDMUtils.createElement(problem.getOwnerDocument(), smlUri, "side");
        result.setAttribute("sideAtt", side);
        //insert the boundary in the condition if exist
        if (condition == null) {
            result.appendChild(problem.getOwnerDocument().importNode(getMaxDissipBoundary(discProblem, problem, timeSlice, 
                    previousTimeSlice, pis, coordinate, side, postInit), true));
        }
        else {
            Element conditionClone = (Element) condition.cloneNode(true);
            conditionClone.getLastChild().appendChild(problem.getOwnerDocument().importNode(getMaxDissipBoundary(discProblem, problem, 
                    timeSlice, previousTimeSlice, pis, coordinate, side, postInit)
                    .getFirstChild(), true));
            result.appendChild(conditionClone);
        }
        return result;
    }
    
    /**
     * Create the variable to count the distance between the point to do the reflection to the limit of its region.
     * @param doc       The document
     * @param coords    The coordinates
     * @param coord     The coordinate for the boundary
     * @param direction The direction of the boundary
     * @param coordRel  The coordinate relation
     * @return          The code with the while
     */
    private static DocumentFragment createReflectionCounter(Document doc, ArrayList<String> coords, String coord, String direction, 
    		CoordinateInfo coordRel) {
        DocumentFragment result = doc.createDocumentFragment();
        
        Element initialization = AGDMUtils.createElement(doc, mtUri, "math");
        Element applyInit = AGDMUtils.createElement(doc, mtUri, "apply");
        Element eqInit = AGDMUtils.createElement(doc, mtUri, "eq");
        Element rc = AGDMUtils.createElement(doc, mtUri, "ci");
        rc.setTextContent("ReflectionCounter");
        Element cnInit = AGDMUtils.createElement(doc, mtUri, "cn");
        cnInit.setTextContent("0");
        applyInit.appendChild(eqInit);
        applyInit.appendChild(rc.cloneNode(true));
        applyInit.appendChild(cnInit);
        initialization.appendChild(applyInit);
        result.appendChild(initialization);
       
        Element whileTag = AGDMUtils.createElement(doc, smlUri, "while");
        Element conditionWhile = AGDMUtils.createElement(doc, mtUri, "math");
        Element conditionApply1 = AGDMUtils.createElement(doc, mtUri, "apply");
        Element conditionGt = AGDMUtils.createElement(doc, mtUri, "gt");
        Element regionApply = AGDMUtils.createElement(doc, mtUri, "apply");
        
        String boundId = coordRel.getDiscToCont(coord);
        if (direction.equals("upper")) {
            boundId = boundId + "-Upper"; 
        }
        else {
            boundId = boundId + "-Lower"; 
        }
        Element regionInteriorId = AGDMUtils.createElement(doc, smlUri, "regionInteriorId");
        regionInteriorId.setTextContent(boundId);
        
        //Create the index
        DocumentFragment index = doc.createDocumentFragment();
        for (int i = 0; i < coords.size(); i++) {
            if (coords.get(i).equals(coord)) {
                Element plusApply1 = AGDMUtils.createElement(doc, mtUri, "apply");
                Element operation1;
                if (direction.equals("lower")) {
                    operation1 = AGDMUtils.createElement(doc, mtUri, "plus");
                    
                }
                else {
                    operation1 = AGDMUtils.createElement(doc, mtUri, "minus");
                }
                Element coordCi = AGDMUtils.createElement(doc, mtUri, "ci");
                coordCi.setTextContent(coords.get(i));
                plusApply1.appendChild(operation1);
                plusApply1.appendChild(coordCi);
                plusApply1.appendChild(rc.cloneNode(true));
                index.appendChild(plusApply1);
            }
            else {
                Element coordCi = AGDMUtils.createElement(doc, mtUri, "ci");
                coordCi.setTextContent(coords.get(i));
                index.appendChild(coordCi);
            }
        }
        Element zero = AGDMUtils.createElement(doc, mtUri, "cn");
        zero.setTextContent("0");
        regionApply.appendChild(regionInteriorId);
        regionApply.appendChild(index);
        conditionApply1.appendChild(conditionGt);
        conditionApply1.appendChild(regionApply);
        conditionApply1.appendChild(zero);
        conditionWhile.appendChild(conditionApply1);
        whileTag.appendChild(conditionWhile);
        Element loop = AGDMUtils.createElement(doc, smlUri, "loop");
        Element math = AGDMUtils.createElement(doc, mtUri, "math");
        Element apply1 = AGDMUtils.createElement(doc, mtUri, "apply");
        Element apply2 = AGDMUtils.createElement(doc, mtUri, "apply");
        Element eq = AGDMUtils.createElement(doc, mtUri, "eq");
        Element plus = AGDMUtils.createElement(doc, mtUri, "plus");
        Element cn = AGDMUtils.createElement(doc, mtUri, "cn");
        cn.setTextContent("1");
        apply1.appendChild(eq);
        apply1.appendChild(rc.cloneNode(true));
        apply2.appendChild(plus);
        apply2.appendChild(rc.cloneNode(true));
        apply2.appendChild(cn);
        apply1.appendChild(apply2);
        math.appendChild(apply1);
        loop.appendChild(math);
        whileTag.appendChild(loop);
        result.appendChild(whileTag);
        
        return result;
    }
    
    /**
     * Creates the static boundary for FVM problems.
     * @param timeSlice             The time slice when to calculate the boundary
     * @param previousTimeSlice     The previous time slice to get the boundary value
     * @param pi                    The process information
     * @param coord                 The coordinate to create the boundary
     * @return                      The static boundary math
     * @throws AGDMException        AGDM007 External error
     */
    private static DocumentFragment getStaticBoundary(Element timeSlice, Element previousTimeSlice, ProcessInfo pi, String coord) 
        throws AGDMException {
        //Result is stored in document fragment
        Document doc = timeSlice.getOwnerDocument();
        DocumentFragment fragmentResult = doc.createDocumentFragment();
        //Auxiliaries does not have timeSlices
        if (!pi.getRegionInfo().isAuxiliaryField(pi.getField())) {
	        boolean isShared = ((Element)timeSlice.getFirstChild()).getLocalName().equals("sharedVariable");
	        boolean isSharedP = ((Element)previousTimeSlice.getFirstChild()).getLocalName().equals("sharedVariable");
	        Element tsVar = (Element) timeSlice.getFirstChild().getFirstChild().cloneNode(true);
	        if (isShared) {
	            tsVar = (Element) tsVar.getFirstChild();
	        }
	        Element ptsVar = (Element) previousTimeSlice.getFirstChild().getFirstChild().cloneNode(true);
	        if (isSharedP) {
	            ptsVar = (Element) ptsVar.getFirstChild();
	        }
	        
	        String contCoord = pi.getCoordinateRelation().getDiscToCont(coord);
	        pi.setSpatialCoord1(contCoord);
	        pi.setSpatialCoord2(null);
	        Element math = AGDMUtils.createElement(doc, mtUri, "math");
	        Element apply = AGDMUtils.createElement(doc, mtUri, "apply");
	        Element eq = AGDMUtils.createElement(doc, mtUri, "eq");
	        Element leftSide = AGDMUtils.createElement(doc, mtUri, "apply");
	        leftSide.appendChild(doc.importNode(ExecutionFlow.replaceTags(tsVar, pi), true));
	        leftSide.appendChild(getBoundaryCoordinates(doc, pi.getRegionInfo(), pi.getCoordinateRelation(), coord, null, null));
	        if (isShared) {
	            Element shared = AGDMUtils.createElement(doc, smlUri, "sharedVariable");
	            shared.appendChild(leftSide);
	            leftSide = shared;
	        }
	        Element rightSide = AGDMUtils.createElement(doc, mtUri, "apply");
        	rightSide.appendChild(doc.importNode(ExecutionFlow.replaceTags(ptsVar, pi), true));
        	rightSide.appendChild(getBoundaryCoordinates(doc, pi.getRegionInfo(), pi.getCoordinateRelation(), coord, null, null));
	        if (isSharedP) {
	            Element shared = AGDMUtils.createElement(doc, smlUri, "sharedVariable");
	            shared.appendChild(rightSide);
	            rightSide = shared;
	        }
	        apply.appendChild(eq);
	        apply.appendChild(doc.importNode(leftSide, true));
	        apply.appendChild(doc.importNode(rightSide, true));
	        math.appendChild(apply);
	        fragmentResult.appendChild(math);
        }
       
        return fragmentResult;
    }
    
    /**
     * Creates the static boundary for FVM problems.
     * @param problem				CompleteProblem
     * @param result				Discrete problem
     * @param timeSlice             The time slice when to calculate the boundary
     * @param pi                    The process information
     * @param boundary				Boundary
     * @param allowedFields			Allowed fields for the process info
     * @return                      The static boundary math
     * @throws AGDMException        AGDM007 External error
     */
    private static DocumentFragment getStaticBoundaryAuxiliaries(Document result, Node problem, Element timeSlice, ProcessInfo pi, Node boundary, ArrayList<String> allowedFields) 
        throws AGDMException {
        //Result is stored in document fragment
        Document doc = timeSlice.getOwnerDocument();
        DocumentFragment fragmentResult = doc.createDocumentFragment();
        ArrayList<String> alreadyCreated = new ArrayList<String>();
        //Get fields from boundary
        NodeList fields = AGDMUtils.find(boundary, ".//mms:fields/mms:field");
        for (int i = 0; i < fields.getLength(); i++) {
        	String field = fields.item(i).getTextContent();
	        if (pi.getRegionInfo().isAuxiliaryField(field) && allowedFields.contains(field) && !alreadyCreated.contains(field)) {
	        	EquationTerms auxEquations = pi.getOperatorsInfo().getAuxEquations().getEquationsForField(field);
            	EquationType auxiliaryFieldType = auxEquations.getEquationType();
            	if (auxiliaryFieldType == EquationType.algorithmic) {
	                Element simml = (Element) auxEquations.getAlgorithm().cloneNode(true);
	                //Create the equation with the time slice
	                AGDMUtils.replaceFieldVariables(pi, (Element) timeSlice.getFirstChild(), simml);
	                fragmentResult.appendChild(doc.importNode(ExecutionFlow.processSimml(simml, result, problem, pi, true), true));
	                
            	}
	            alreadyCreated.addAll(auxEquations.getFields());
	        }
        }

        return fragmentResult;
    }
    
    /**
     * Creates the static boundary for particles.
     * @param problem				Complete Problem
     * @param result				Discrete problem
     * @param boundary              The boundary
     * @param timeSlice             The time slice when to calculate the boundary
     * @param previousTimeSlice     The previous time slice when to calculate the boundary
     * @param pi                    The process information
     * @param coord                 The coordinate to create the boundary
     * @param isLastBoundary        True if is the last boundary in the schema
     * @param postInit              True if the boundary is for the post initialization synchronization
     * @param rule                  The boundary
     * @param withAuxiliaries		If boundaries have to be generated with auxiliaries
     * @param withFields			If boundaries have to be generated with fields
     * @return                      The static boundary math
     * @throws AGDMException        AGDM007 External error
     */
    private static DocumentFragment getStaticParticleBoundary(Document result, Node problem, Node boundary, Element timeSlice, Element previousTimeSlice, 
            ProcessInfo pi, String coord, boolean isLastBoundary, boolean postInit, Element rule, boolean withAuxiliaries, boolean withFields) throws AGDMException {
        //Result is stored in document fragment
        Document doc = timeSlice.getOwnerDocument();
        DocumentFragment fragmentResult = doc.createDocumentFragment();
        Element condition = null;
        if (((Element) boundary).getElementsByTagName("mms:applyIf").getLength() > 0) {
            condition = (Element) ((Element) boundary).getElementsByTagName("mms:applyIf").item(0).cloneNode(true);
            condition = generateLimitCondition(condition);
        }
        
        DocumentFragment staticPart = problem.getOwnerDocument().createDocumentFragment();
        //Create the static part
        ArrayList<ProcessInfo> pis = AGDMUtils.generateProcessInfoForBoundary(problem, boundary, pi, pi.getParticleFields(), withAuxiliaries, withFields);
        for (int l = 0; l < pis.size(); l++) {
            staticPart.appendChild(problem.getOwnerDocument().importNode(
                getStaticBoundary(timeSlice, previousTimeSlice, pis.get(l), coord), true));
        }
        //If static, keep same value in auxiliaries
        /*OperatorsInfo opInfoAuxFields = new OperatorsInfo(pi.getOperatorsInfo());
    	opInfoAuxFields.removeAlgorithmicAuxFieldDerivativeDependent();
    	opInfoAuxFields.onlyDependent(opInfoAuxFields.getAuxiliaryFieldAlgorithmVariables(), true, opInfoAuxFields.getNonDerivativeDependentAuxiliaryFieldCommonConditional());
    	staticPart.appendChild(problem.getOwnerDocument().importNode(SchemaCreator.createAuxiliaryVariableEquations(result, problem, opInfoAuxFields, pi.getOperatorsInfo().getAuxiliaryDerivativeLevels(), pi.getOperatorsInfo().getAuxiliaryMultiplicationLevels(), opInfoAuxFields.getAuxiliaryDerivativeLevels(), opInfoAuxFields.getAuxiliaryMultiplicationLevels(), pi, (Element) previousTimeSlice.getFirstChild().cloneNode(true), 0), true));
        staticPart.appendChild(problem.getOwnerDocument().importNode(getStaticBoundaryAuxiliaries(result, problem, previousTimeSlice, pi, boundary, pi.getParticleFields()), true));*/
        //If not the last boundary use the static part
        if (!isLastBoundary || postInit) {
            DocumentFragment inst = processPostInit(doc, problem, pi, timeSlice, rule.getOwnerDocument());

            if (inst.hasChildNodes() && postInit) {
                staticPart.appendChild(staticPart.getOwnerDocument().importNode(inst, true));
            }
            if (postInit) {
                staticPart.appendChild(createNewRegionAssignment(pi));
            }
            if (condition != null) {
                condition.getLastChild().appendChild(staticPart);
            }
            else {
                fragmentResult.appendChild(doc.importNode(staticPart, true));
            }
        }
        //Create static if not new region, else use initial conditions
        else {
        	boolean isStepVariable = pi.isStepVariable(timeSlice.getFirstChild(), false);
        	
            Element ifElem = AGDMUtils.createElement(doc, smlUri, "if");
            Element then = AGDMUtils.createElement(doc, smlUri, "then");
            Element elseElem = AGDMUtils.createElement(doc, smlUri, "else");
            //Initialization of the interaction indicator
            Element ifCond = AGDMUtils.createElement(doc, mtUri, "math");
            Element apply = AGDMUtils.createElement(doc, mtUri, "apply");
            Element eq = AGDMUtils.createElement(doc, mtUri, "eq");
            Element newRegion = AGDMUtils.createElement(doc, mtUri, "apply");
            Element ci = AGDMUtils.createElement(doc, mtUri, "ci");
            ci.setTextContent("newRegion");
            newRegion.appendChild(ci);
            for (int i = 0; i < pi.getBaseSpatialCoordinates().size(); i++) {
                Element coordElem = AGDMUtils.createElement(doc, mtUri, "ci");
                coordElem.setTextContent(pi.getCoordinateRelation().getDiscCoords().get(i));
                newRegion.appendChild(coordElem);
            }
            String regionName = pi.getRegionName();
            Element regionId = null;
            if (regionName.substring(regionName.length() - 1).equals("I")) {
                regionId = AGDMUtils.createElement(doc, smlUri, "regionInteriorId");
            } 
            else {
                regionId = AGDMUtils.createElement(doc, smlUri, "regionSurfaceId");
            }
            regionId.setTextContent(regionName.substring(0, regionName.length() - 1));
            ifElem.appendChild(ifCond);
            ifElem.appendChild(then);
            ifElem.appendChild(elseElem);
            ifCond.appendChild(apply);
            apply.appendChild(eq);
            apply.appendChild(newRegion);
            apply.appendChild(regionId);
            //Add the static part to the then element
            then.appendChild(doc.importNode(staticPart, true));
            //Add the initialization part if the boundary particle is being used by first time
            NodeList initialConditions = AGDMUtils.find(boundary, ".//mms:initialConditions/mms:initialCondition/sml:iterateOverParticles/*");
            for (int i = 0; i < initialConditions.getLength(); i++) {
                Element initialCondition = (Element) initialConditions.item(i).cloneNode(true);
                
                ArrayList<String> fieldIt = pi.getRegionInfo().getFields();
                for (int j = 0; j < fieldIt.size(); j++) {
                    String field = fieldIt.get(j);
                    ProcessInfo newPi = new ProcessInfo(pi);
                    newPi.setField(field);
                    newPi.setSpatialCoord1(pi.getCoordinateRelation().getDiscToCont(coord));
                    newPi.setSpatialCoord2(null);
                    Element fieldTime;
                    if (pi.getRegionInfo().isAuxiliaryField(field) && isStepVariable) {
                    	fieldTime = ExecutionFlow.replaceTags(AGDMUtils.createTimeSliceWithExternalIndex(doc, (Element) timeSlice.getFirstChild()), newPi);
                    }
                    else {
                    	fieldTime = ExecutionFlow.replaceTags(timeSlice.getFirstChild().cloneNode(true), newPi);	
                    }
                    NodeList fieldVars = AGDMUtils.find(initialCondition, ".//mt:apply[mt:ci/mt:msup[mt:ci = '" + field + "']//mt:ci = '(" 
                            + pi.getCoordinateRelation().getDiscTimeCoord() + ")']");
                    for (int k = fieldVars.getLength() - 1; k >= 0; k--) {
                        fieldVars.item(k).getParentNode().replaceChild(boundary.getOwnerDocument().importNode(
                                fieldTime, true), fieldVars.item(k));
                    }
                }
                elseElem.appendChild(doc.importNode(initialCondition, true));
            }
           
            DocumentFragment inst = processPostInit(doc, problem, pi, timeSlice, rule.getOwnerDocument());
            if (inst.hasChildNodes()) {
                elseElem.appendChild(elseElem.getOwnerDocument().importNode(inst, true));
            }
            elseElem.appendChild(doc.importNode(createNewRegionAssignment(pi), true));
            
            if (condition != null) {
                condition.getLastChild().appendChild(ifElem);
            }
            else {
                fragmentResult.appendChild(doc.importNode(ifElem, true));
            }
        }
        if (condition != null) {
            fragmentResult.appendChild(condition);
        }

        return fragmentResult;
    }
    /**
     * Creates the periodical boundary.
     * @param timeSlice             The time slice when to calculate the boundary
     * @return                      The periodical boundary math
     */
    private static DocumentFragment getPeriodicalBoundary(Element timeSlice) {
        //Result is stored in document fragment
        Document doc = timeSlice.getOwnerDocument();
        DocumentFragment fragmentResult = doc.createDocumentFragment();
        
        //lower boundary
        fragmentResult.appendChild(AGDMUtils.createElement(doc, smlUri, "periodicalBoundary"));
        return fragmentResult;
    }
    /**
     * Creates the flat boundary.
     * @param timeSlice             The time slice when to calculate the boundary
     * @param pi                    The process information
     * @param coord                 The coordinate to create the boundary
     * @param limit                 The boundary limit ("lower" or "upper")
     * @return                      The flat boundary math
     * @throws AGDMException        AGDM007 External error
     */
    private static DocumentFragment getFlatBoundary(Element timeSlice, ProcessInfo pi, String coord, String limit) 
        throws AGDMException {
        //Result is stored in document fragment
        Document doc = timeSlice.getOwnerDocument();
        DocumentFragment fragmentResult = doc.createDocumentFragment();
        boolean isShared = ((Element)timeSlice.getFirstChild()).getLocalName().equals("sharedVariable");
        
        //Distance for flat
        Element distance = AGDMUtils.createElement(doc, mtUri, "cn");
        distance.setTextContent("1");
        
        String contCoord = pi.getCoordinateRelation().getDiscToCont(coord);
        pi.setSpatialCoord1(contCoord);
        pi.setSpatialCoord2(null);
        String side = "left";
        if (limit.equals("lower")) {
            side = "right";
        }
        Element math = AGDMUtils.createElement(doc, mtUri, "math");
        Element apply = AGDMUtils.createElement(doc, mtUri, "apply");
        Element eq = AGDMUtils.createElement(doc, mtUri, "eq");
        Element tsVar = (Element) timeSlice.getFirstChild().getFirstChild().cloneNode(true);
        if (isShared) {
            tsVar = (Element) tsVar.getFirstChild();
        }
        Element leftSide;
        if (pi.getRegionInfo().isAuxiliaryField(pi.getField())) {
        	leftSide = ExecutionFlow.replaceTags(AGDMUtils.createTimeSlice(doc), pi);
        }
        else {
	        leftSide = AGDMUtils.createElement(doc, mtUri, "apply");
	        leftSide.appendChild(doc.importNode(ExecutionFlow.replaceTags(tsVar, pi), true));
	        leftSide.appendChild(
                getBoundaryCoordinates(doc, pi.getRegionInfo(), pi.getCoordinateRelation(), coord, null, null));
	        if (isShared) {
	            Element shared = AGDMUtils.createElement(doc, smlUri, "sharedVariable");
	            shared.appendChild(leftSide);
	            leftSide = shared;
	        }
        }
        Element rightSide;
        DocumentFragment rightIndex;
        if (limit.equals("lower")) {
        	rightIndex = getGhostCoordinates(doc, pi.getRegionInfo(), pi.getCoordinateRelation(), coord);
        }
        else {
        	rightIndex = getBoundaryCoordinates(doc, pi.getRegionInfo(), pi.getCoordinateRelation(), coord, distance, side);
        }
        if (pi.getRegionInfo().isAuxiliaryField(pi.getField())) {
        	rightSide = ExecutionFlow.replaceTags(AGDMUtils.createTimeSliceWithExternalIndex(doc, rightIndex), pi);
        }
        else {
	        rightSide = AGDMUtils.createElement(doc, mtUri, "apply");
	        rightSide.appendChild(doc.importNode(ExecutionFlow.replaceTags(tsVar.cloneNode(true), pi), true));
	        rightSide.appendChild(rightIndex);
	        if (isShared) {
           	 Element shared = AGDMUtils.createElement(doc, smlUri, "sharedVariable");
                shared.appendChild(rightSide);
                rightSide = shared;
           }
        }
        apply.appendChild(eq);
        apply.appendChild(doc.importNode(leftSide, true));
        apply.appendChild(doc.importNode(rightSide, true));
        math.appendChild(apply);
        fragmentResult.appendChild(math);
                
        return fragmentResult;
    }
    /**
     * Creates the reflection boundary.
     * @param boundary              The boundary condition
     * @param timeSlice             The time slice when to calculate the boundary
     * @param pi                    The process information
     * @param coord                 The coordinate to create the boundary
     * @param limit                 The boundary limit ("lower" or "upper")
     * @param signChange            If there is a change in the sign of the field.
     * @return                      The reflection boundary math
     * @throws AGDMException        AGDM007 External error
     */
    private static DocumentFragment getReflectionBoundaryMesh(Node boundary, Element timeSlice, ProcessInfo pi, String coord, String limit, 
            boolean signChange) throws AGDMException {
        //Result is stored in document fragment
        Document doc = timeSlice.getOwnerDocument();
        DocumentFragment fragmentResult = doc.createDocumentFragment();
        boolean isShared = ((Element)timeSlice.getFirstChild()).getLocalName().equals("sharedVariable");
        Element tsVar = (Element) timeSlice.getFirstChild().getFirstChild().cloneNode(true);
        if (isShared) {
            tsVar = (Element) tsVar.getFirstChild();
        }
        
        NodeList signs = AGDMUtils.find(boundary, ".//mms:sign[mms:field = '" + pi.getField() + "']");
        if (signs.getLength() == 0) {
            throw new AGDMException(AGDMException.AGDM006, "Reflection boundary requires reflection signs for all the fields");
        }
        String sign = signs.item(0).getLastChild().getTextContent(); 
        String contCoord = pi.getCoordinateRelation().getDiscToCont(coord);
        pi.setSpatialCoord1(contCoord);
        pi.setSpatialCoord2(null);
        
        Element math = AGDMUtils.createElement(doc, mtUri, "math");
        Element apply = AGDMUtils.createElement(doc, mtUri, "apply");
        Element eq = AGDMUtils.createElement(doc, mtUri, "eq");
        Element leftSide;
        if (pi.getRegionInfo().isAuxiliaryField(pi.getField())) {
        	leftSide = ExecutionFlow.replaceTags(AGDMUtils.createTimeSlice(doc), pi);
        }
        else {
            leftSide = AGDMUtils.createElement(doc, mtUri, "apply");
            leftSide.appendChild(doc.importNode(ExecutionFlow.replaceTags(tsVar, pi), true));
            leftSide.appendChild(getBoundaryCoordinates(doc, pi.getRegionInfo(), pi.getCoordinateRelation(), coord, null, null));
            if (isShared) {
            	 Element shared = AGDMUtils.createElement(doc, smlUri, "sharedVariable");
                 shared.appendChild(leftSide);
                 leftSide = shared;
            }
        }
        Element rightSide;
        DocumentFragment rightIndex = getReflectionCoordinates(doc, pi.getRegionInfo(), pi.getCoordinateRelation(), coord, limit);
        if (pi.getRegionInfo().isAuxiliaryField(pi.getField())) {
        	rightSide = (Element) doc.importNode(ExecutionFlow.replaceTags(AGDMUtils.createTimeSliceWithExternalIndex(doc, rightIndex), pi), true);
        }
        else {
            rightSide = AGDMUtils.createElement(doc, mtUri, "apply");
            rightSide.appendChild(doc.importNode(ExecutionFlow.replaceTags(tsVar, pi), true));
            rightSide.appendChild(rightIndex);
            if (isShared) {
            	 Element shared = AGDMUtils.createElement(doc, smlUri, "sharedVariable");
                 shared.appendChild(rightSide);
                 rightSide = shared;
            }
        }
        apply.appendChild(eq);
        apply.appendChild(doc.importNode(leftSide, true));
        //If the reflection is negative add a minus sign preceding the formula
        if ((sign.equals("Negative") && !signChange) || (sign.equals("Positive") && signChange)) {
            Element rightNegative = AGDMUtils.createElement(doc, mtUri, "apply");
            Element minus = AGDMUtils.createElement(doc, mtUri, "minus");
            rightNegative.appendChild(minus);
            rightNegative.appendChild(rightSide);
            apply.appendChild(rightNegative);
        }
        else {
            apply.appendChild(rightSide);
        }
        
        math.appendChild(apply);
        fragmentResult.appendChild(math);
        
        return fragmentResult;
    }
    
    /**
     * Creates the reflection boundary.
     * @param boundary              The boundary condition
     * @param timeSlice             The time slice when to calculate the boundary
     * @param pi                    The process information
     * @param coord                 The coordinate to create the boundary
     * @param limit                 The boundary limit ("lower" or "upper")
     * @return                      The reflection boundary math
     * @throws AGDMException        AGDM007 External error
     */
    private static DocumentFragment getReflectionBoundaryParticles(Node boundary, Element timeSlice, ProcessInfo pi, 
    		String coord, String limit) throws AGDMException {
        //Result is stored in document fragment
        Document doc = timeSlice.getOwnerDocument();
        DocumentFragment fragmentResult = doc.createDocumentFragment();
        boolean isShared = ((Element)timeSlice.getFirstChild()).getLocalName().equals("sharedVariable");
        Element tsVar = (Element) timeSlice.getFirstChild().getFirstChild().cloneNode(true);
        if (isShared) {
            tsVar = (Element) tsVar.getFirstChild();
        }
        
        NodeList signs = AGDMUtils.find(boundary, ".//mms:sign[mms:field = '" + pi.getField() + "']");
        if (signs.getLength() == 0) {
            throw new AGDMException(AGDMException.AGDM006, "Reflection boundary requires reflection signs for all the fields");
        }
        String sign = signs.item(0).getLastChild().getTextContent(); 
        //Only the negative fields need to be set
        if (sign.equals("Negative")) {
            String contCoord = pi.getCoordinateRelation().getDiscToCont(coord);
            pi.setSpatialCoord1(contCoord);
            pi.setSpatialCoord2(null);
            
            Element math = AGDMUtils.createElement(doc, mtUri, "math");
            math.setAttributeNS(smlUri, "sml:type", "reflection");
            Element apply = AGDMUtils.createElement(doc, mtUri, "apply");
            Element eq = AGDMUtils.createElement(doc, mtUri, "eq");
            Element leftSide = AGDMUtils.createElement(doc, mtUri, "apply");
            leftSide.appendChild(doc.importNode(ExecutionFlow.replaceTags(tsVar, pi), true));
            leftSide.appendChild(getBoundaryCoordinates(doc, pi.getRegionInfo(), pi.getCoordinateRelation(), coord, null, null));
            Element rightSide = AGDMUtils.createElement(doc, mtUri, "apply");
            rightSide.appendChild(doc.importNode(ExecutionFlow.replaceTags(tsVar, pi), true));
            rightSide.appendChild(getBoundaryCoordinates(doc, pi.getRegionInfo(), pi.getCoordinateRelation(), coord, null, null));
            apply.appendChild(eq);
            if (isShared) {
                Element shared = AGDMUtils.createElement(doc, smlUri, "sharedVariable");
                shared.appendChild(leftSide);
                apply.appendChild(shared);
                Element tmp = AGDMUtils.createElement(doc, smlUri, "sharedVariable");
                tmp.appendChild(rightSide);
                rightSide = tmp;
            }
            else {
                apply.appendChild(leftSide);
            }
            //Absolute value
            Element rightAbs = AGDMUtils.createElement(doc, mtUri, "apply");
            Element abs = AGDMUtils.createElement(doc, mtUri, "abs");
            rightAbs.appendChild(abs);
            rightAbs.appendChild(rightSide);

            Element right;
            if (limit.equals("lower")) {
                right = rightAbs;
            }
            else {
                right = AGDMUtils.createElement(doc, mtUri, "apply");
                Element minus = AGDMUtils.createElement(doc, mtUri, "minus");
                right.appendChild(minus);
                right.appendChild(rightAbs);
            }
            apply.appendChild(right);
            math.appendChild(apply);
            fragmentResult.appendChild(math);
        }
        
        return fragmentResult;
    }
    
    /**
     * Creates the fixed boundary.
     * @param completeProblem       The complete simulation problem
     * @param boundary              The boundary condition
     * @param timeSlice             The time slice when to calculate the boundary
     * @param pi                    The process information
     * @param coord                 The coordinate to create the boundary
     * @return                      The flat boundary math
     * @throws AGDMException        AGDM007 External error
     */
    private static DocumentFragment getAlgebraicBoundary(Node completeProblem, Node boundary, Element timeSlice, ProcessInfo pi, String coord) 
        throws AGDMException {
        //Result is stored in document fragment
        Document doc = timeSlice.getOwnerDocument();
        DocumentFragment fragmentResult = doc.createDocumentFragment();
        boolean isShared = ((Element) timeSlice.getFirstChild()).getLocalName().equals("sharedVariable");
        
        //Get the fixed value for the field
        String contCoord = pi.getCoordinateRelation().getDiscToCont(coord);
        pi.setSpatialCoord1(contCoord);
        pi.setSpatialCoord2(null);
        NodeList expressions = AGDMUtils.find(boundary, ".//mms:expression[mms:field = '" + pi.getField() + "']");
        if (expressions.getLength() == 0) {
            throw new AGDMException(AGDMException.AGDM006, "Algebraic boundary requires mathematical expressions values for every field");
        }
        
        Element rightSide = null;
        //Fixed value from formulae
        if (expressions.item(0).getLastChild().getLocalName().equals("math")) {
            Element mathExpression = (Element) expressions.item(0).getLastChild().cloneNode(true); 
            //Modify the fixedValue if there is a field used inside
            ArrayList<String> fieldIt = pi.getRegionInfo().getFields();
            for (int i = 0; i < fieldIt.size(); i++) {
                String field = fieldIt.get(i);
                ProcessInfo newPi = new ProcessInfo(pi);
                newPi.setField(field);
                newPi.setSpatialCoord1(contCoord);
                newPi.setSpatialCoord2(null);
                Element fieldTime;
                if (pi.getRegionInfo().isAuxiliaryField(field)) {
                	fieldTime = ExecutionFlow.replaceTags(AGDMUtils.createTimeSliceWithExternalIndex(doc, (Element) timeSlice.getFirstChild()), newPi);
                }
                else {
                	fieldTime = ExecutionFlow.replaceTags(timeSlice.getFirstChild().cloneNode(true), newPi);
                }
                AGDMUtils.createTimeSlice(mathExpression, field, fieldTime, 
                        pi.getRegionInfo(), pi.getCoordinateRelation());
            }
            rightSide = (Element) mathExpression.getFirstChild();
        }
        //Fixed value from model
        else {
            String modelId = expressions.item(0).getLastChild().getTextContent(); 
            Element rhs = (Element) AGDMUtils.find(completeProblem, "//mms:model[mms:id = '" + modelId 
                    + "']//mms:auxiliaryEquation[descendant::mms:auxiliaryField = '" + pi.getField() + "']//" 
                    + "mt:math").item(0).cloneNode(true);
            ArrayList<String> fieldIt = pi.getRegionInfo().getFields();
            for (int j = 0; j < fieldIt.size(); j++) {
                String field = fieldIt.get(j);
                ProcessInfo newPi = new ProcessInfo(pi);
                newPi.setField(field);
                newPi.setSpatialCoord1(null);
                newPi.setSpatialCoord2(null);
                Element fieldVar;
                if (pi.getRegionInfo().isAuxiliaryField(field)) {
                	fieldVar = ExecutionFlow.replaceTags(AGDMUtils.createTimeSliceWithExternalIndex(doc, (Element) timeSlice.getFirstChild()), newPi);
                }
                else {
                	fieldVar = ExecutionFlow.replaceTags(timeSlice.getFirstChild().cloneNode(true), newPi);
                }
                AGDMUtils.replaceVariable(rhs, field, fieldVar);
            }
            rightSide = (Element) rhs.getFirstChild();
        }
        
        Element math = AGDMUtils.createElement(doc, mtUri, "math");
        Element apply = AGDMUtils.createElement(doc, mtUri, "apply");
        Element eq = AGDMUtils.createElement(doc, mtUri, "eq");

        Element leftSide;
        if (pi.getRegionInfo().isAuxiliaryField(pi.getField())) {
        	leftSide = ExecutionFlow.replaceTags(AGDMUtils.createTimeSlice(doc), pi);
        }
        else {
	        leftSide = AGDMUtils.createElement(doc, mtUri, "apply");
	        Element tsVar = (Element) timeSlice.getFirstChild().getFirstChild().cloneNode(true);
	        if (isShared) {
	            tsVar = (Element) tsVar.getFirstChild();
	        }
	        leftSide.appendChild(doc.importNode(ExecutionFlow.replaceTags(tsVar, pi), true));
	        leftSide.appendChild(getBoundaryCoordinates(doc, pi.getRegionInfo(), pi.getCoordinateRelation(), coord, null, null));
	        if (isShared) {
	          	 Element shared = AGDMUtils.createElement(doc, smlUri, "sharedVariable");
	             shared.appendChild(leftSide);
	             leftSide = shared;
	        }
        }
        apply.appendChild(eq);
        apply.appendChild(doc.importNode(leftSide, true));
        apply.appendChild(doc.importNode(rightSide, true));
        math.appendChild(apply);
        fragmentResult.appendChild(math);

        return fragmentResult;
    }
    
    /**
     * Creates the maximal dissipation boundary.
     * @param problem                 The document with the info
     * @param timeSlice             The time slice when to calculate the boundary
     * @param previousTimeSlice     The previous time slice to get the boundary value
     * @param pis                   The process information
     * @param coord                 The coordinate to create the boundary
     * @param limit                 The boundary limit ("lower" or "upper")
     * @param postInit              True if the boundaries are invoked from post initialization
     * @return                      The maximal dissipation boundary math
     * @throws AGDMException        AGDM007 External error
     */
    private static DocumentFragment getMaxDissipBoundary(Document discProblem, Node problem, Element timeSlice, Element previousTimeSlice, 
            ArrayList<ProcessInfo> pis, String coord, String limit, boolean postInit) throws AGDMException {
        ProcessInfo pi = pis.get(0);
        //Result is stored in document fragment
        Document doc = timeSlice.getOwnerDocument();
        DocumentFragment fragmentResult = doc.createDocumentFragment();

        boolean isStepVariable = pi.isStepVariable(timeSlice.getFirstChild(), false);
        String op = "";
        if (limit.equals("lower")) {
            op = "plus";
        }
        else {
            op = "minus";
        }
        
        if (pi.getCharDecomposition() == null) {
            throw new AGDMException(AGDMException.AGDM006, "This schema uses eigenSpace and some model have not been defined");
        }
        //Normals to axis
        fragmentResult.appendChild(AGDMUtils.createNormalsToAxis(doc, pi, coord, limit));
        //Projection of vectors
        Element neigh = AGDMUtils.createTimeSliceOperated(pi, 
                timeSlice.getFirstChild().cloneNode(true), coord, op, 1);
        fragmentResult.appendChild(doc.importNode(AGDMUtils.projectVectors(pi.getCharDecomposition(), neigh.cloneNode(true),
                timeSlice.getFirstChild().cloneNode(true), pi, true), true));
        
        //Auxiliary definitions
        fragmentResult.appendChild(doc.importNode(
                AGDMUtils.deployAuxiliaryDefinitions(discProblem, problem, pi.getCharDecomposition(), 2, pi, neigh.cloneNode(true), true), true));
        //Diagonalization
        fragmentResult.appendChild(diagonalization(problem, (Element) timeSlice.getFirstChild().cloneNode(true), pi, coord, limit, false));
        if (postInit) {
            //Projection of vectors
            fragmentResult.appendChild(doc.importNode(AGDMUtils.projectVectors(pi.getCharDecomposition(), timeSlice.getFirstChild().cloneNode(true),
                    timeSlice.getFirstChild().cloneNode(true), pi, true), true));
    
            //Auxiliary definitions
            fragmentResult.appendChild(doc.importNode(
                    AGDMUtils.deployAuxiliaryDefinitions(discProblem, problem, pi.getCharDecomposition(), 2, pi, 
                            timeSlice.getFirstChild().cloneNode(true), true), true));
            //Diagonalization
            fragmentResult.appendChild(diagonalization(problem, (Element) timeSlice.getFirstChild().cloneNode(true), pi, coord, limit, true));
        }
        
        //Boundaries on vectors
        String contCoord = pi.getCoordinateRelation().getDiscToCont(coord);
        NodeList eigenSpaces = AGDMUtils.find(pi.getCharDecomposition(), ".//mms:eigenSpace[ancestor::mms:coordinateCharacteristicDecomposition[" 
                + "mms:eigenCoordinate = '" + contCoord + "']]");
        if (eigenSpaces.getLength() == 0) {
            eigenSpaces = AGDMUtils.find(problem, ".//mms:eigenSpace[ancestor::mms:generalCharacteristicDecomposition]");
        }
        for (int i = 0; i < eigenSpaces.getLength(); i++) {
            //condition
            Element condition = AGDMUtils.createElement(doc, smlUri, "if");
            Element condMath = AGDMUtils.createElement(doc, mtUri, "math");
            Element condApply = AGDMUtils.createElement(doc, mtUri, "apply");
            Element condOperation = AGDMUtils.createElement(doc, mtUri, "gt");
            Element eigenValue = (Element) ((Element) eigenSpaces.item(i)).getElementsByTagNameNS(mmsUri, "eigenValue").item(0);
            eigenValue = (Element) eigenValue.getFirstChild().getFirstChild().cloneNode(true);
                        
            //Time slice for fields in eigenvalue
            ArrayList<String> fieldIt = pi.getRegionInfo().getFields();
            for (int k = 0; k < fieldIt.size(); k++) {
                String field = fieldIt.get(k);
                ProcessInfo newPi1 = new ProcessInfo(pi);
                newPi1.setField(field);
                Element fieldSlice;
                if (pi.getRegionInfo().isAuxiliaryField(field) && isStepVariable) {
                	fieldSlice = ExecutionFlow.replaceTags(AGDMUtils.createTimeSliceWithExternalIndex(doc, (Element) timeSlice.getFirstChild()), newPi1);
                }
                else {
                	fieldSlice = ExecutionFlow.replaceTags(timeSlice.getFirstChild().cloneNode(true), newPi1);
                }
                AGDMUtils.createTimeSliceOperated(eigenValue, newPi1, fieldSlice, coord, op, 1);
            }
            Element condCn = AGDMUtils.createElement(doc, mtUri, "cn");
            condCn.setTextContent("0");
            condApply.appendChild(condOperation);
            condApply.appendChild(doc.importNode(eigenValue.cloneNode(true), true));
            condApply.appendChild(condCn);
            condMath.appendChild(condApply);
            condition.appendChild(condMath);
            NodeList eigenVectors = ((Element) eigenSpaces.item(i)).getElementsByTagNameNS(mmsUri, "eigenVector");
            
            for (int j = 0; j < eigenVectors.getLength(); j++) {
                ProcessInfo newPi = new ProcessInfo(pi);
                newPi.setField(eigenVectors.item(j).getFirstChild().getTextContent());
                //greater than 0
                fragmentResult.appendChild(maximalDissipationGT((Element) condition.cloneNode(true), newPi, coord, limit));
                //less than 0
                fragmentResult.appendChild(maximalDissipationLT((Element) condition.cloneNode(true), newPi, coord, limit));
            }
        }
        //Undiagonalization
        fragmentResult.appendChild(undiagonalization(problem, (Element) timeSlice.getFirstChild().cloneNode(true), pi, coord, limit));
        //Unprojection of vectors
        fragmentResult.appendChild(doc.importNode(AGDMUtils.unprojectVectors(pi.getCharDecomposition(), 
                timeSlice.getFirstChild().cloneNode(true), pi), true));
    
        //Auxiliary variables
        for (int l = 0; l < pis.size(); l++) {
            if (pi.getRegionInfo().isAuxiliaryField(pis.get(l).getField())) {
                fragmentResult.appendChild(createAuxiliaries((Element) timeSlice.getFirstChild().cloneNode(true), pis.get(l), coord));
            }
        }

       
        return fragmentResult;
    }
    
    /**
     * Creates the auxiliary equation.
     * @param timeSlice             The time slice when to calculate the boundary
     * @param pi                    The process information
     * @param coord                 The coordinate to create the boundary
     * @return                      The flat boundary math
     * @throws AGDMException        AGDM007 External error
     */
    private static DocumentFragment createAuxiliaries(Element timeSlice, ProcessInfo pi, String coord) 
        throws AGDMException {
        //Result is stored in document fragment
        Document doc = timeSlice.getOwnerDocument();
        DocumentFragment fragmentResult = doc.createDocumentFragment();
        
        boolean isStepVariable = pi.isStepVariable(timeSlice.getFirstChild(), false);
        //Get the fixed value for the field
        String contCoord = pi.getCoordinateRelation().getDiscToCont(coord);
        pi.setSpatialCoord1(contCoord);
        pi.setSpatialCoord2(null);
        
        Document completeProblem = pi.getCompleteProblem();
        String query;
        if (pi.getModelId().equals("")) {
            query = "//mms:model//mms:auxiliaryFieldEquation[descendant::mms:auxiliaryField = '" + pi.getField() 
                    + "']"; 
        }
        else {
            query = "//mms:model[mms:id = '" + pi.getModelId() + "']//mms:auxiliaryFieldEquation[descendant::mms:auxiliaryField = '" 
                    + pi.getField() + "']";
        }
        NodeList equation = AGDMUtils.find(completeProblem, query);
        for (int i = 0; i < equation.getLength(); i++) {
        	NodeList algorithmEl = ((Element) equation.item(i)).getElementsByTagName("sml:simml");
        	NodeList algebraicEl = ((Element) equation.item(i)).getElementsByTagName("mms:algebraic");
        	if (algorithmEl.getLength() > 0 && algebraicEl.getLength() > 0) {
            	throw new AGDMException(AGDMException.AGDM005, 
            			"Auxiliary field equations cannot be defined with both algorithm and algebraic terms.");
            }

            //Algorithm
            if (algorithmEl.getLength() > 0) {
            	throw new AGDMException(AGDMException.AGDM006, 
            			"Maximal dissipation cannot be used with algorithmic auxiliary equations.");
            }
            if (algebraicEl.getLength() > 0) {
	            NodeList operators = ((Element) algebraicEl.item(0)).getElementsByTagName("mms:operator");
	            Node math = algebraicEl.item(0).getFirstChild();
	            if (operators.getLength() > 0) {
                	throw new AGDMException(AGDMException.AGDM006, 
                			"Maximal dissipation cannot be used with auxiliary equations with derivative terms.");
	            }
	            //Mathematic expression
	            if (math.getLocalName().equals("math")) {
	               	Element rightSide = null;
	                Element rhs = (Element) AGDMUtils.find(completeProblem, query).item(0).getFirstChild().cloneNode(true);
	                ArrayList<String> fieldIt = pi.getRegionInfo().getFields();
	                for (int j = 0; j < fieldIt.size(); j++) {
	                    String field = fieldIt.get(j);
	                    ProcessInfo newPi = new ProcessInfo(pi);
	                    newPi.setField(field);
	                    newPi.setSpatialCoord1(null);
	                    newPi.setSpatialCoord2(null);
	                    Element fieldVar;
	                    if (pi.getRegionInfo().isAuxiliaryField(field) && isStepVariable) {
	                        fieldVar = ExecutionFlow.replaceTags(AGDMUtils.createTimeSliceWithExternalIndex(completeProblem, (Element) timeSlice.getFirstChild()), newPi);
	                    }
	                    else {
	                    	fieldVar = ExecutionFlow.replaceTags(timeSlice.getFirstChild().cloneNode(true), newPi);
	                    }
	                    AGDMUtils.replaceVariable(rhs, field, fieldVar);
	                }
	                rightSide = rhs;
	                
	                Element mathRes = AGDMUtils.createElement(doc, mtUri, "math");
	                Element apply = AGDMUtils.createElement(doc, mtUri, "apply");
	                Element eq = AGDMUtils.createElement(doc, mtUri, "eq");
	                Element leftSide = AGDMUtils.createElement(doc, mtUri, "apply");
	                //Take only the variable slice in time, but not the coordinates 
	                leftSide.appendChild(doc.importNode(ExecutionFlow.replaceTags(timeSlice.getFirstChild().cloneNode(true), pi), true));
	                leftSide.appendChild(getBoundaryCoordinates(doc, pi.getRegionInfo(), pi.getCoordinateRelation(), coord, null, null));

	                apply.appendChild(eq);
	                apply.appendChild(leftSide);
	                apply.appendChild(doc.importNode(rightSide, true));
	                mathRes.appendChild(apply);
	                fragmentResult.appendChild(mathRes);
	               	
	            }
            }
        	
        }
        
        
        return fragmentResult;
    }
    
    /**
     * Creates the diagonalization mathML.
     * @param problem               The document with the info
     * @param timeSlice             The time slice when to calculate the boundary
     * @param pi                    The rule information
     * @param coord                 The coordinate to create the boundary
     * @param limit                 The boundary limit ("lower" or "upper")
     * @param noNeighbour           True if the instructions are for the same cell.
     * @return                      The diagonalization content.
     * @throws AGDMException        AGDM007 External error
     */
    private static DocumentFragment diagonalization(Node problem, Element timeSlice, ProcessInfo pi, String coord, String limit, boolean noNeighbour) 
        throws AGDMException {
        String prefix = "mms";
        Document doc = timeSlice.getOwnerDocument();
        DocumentFragment fragmentResult = doc.createDocumentFragment();
        
        String contCoord = pi.getCoordinateRelation().getDiscToCont(coord);
        String op = "";
        if (limit.equals("lower")) {
            op = "plus";
        }
        else {
            op = "minus";
        }
        //Get information from coordinate char decomposition or angle char decomposition
        NodeList vectors = AGDMUtils.find(problem, "//mms:eigenSpace[ancestor::" 
                + prefix + ":coordinateCharacteristicDecomposition[mms:eigenCoordinate = '" + contCoord + "']]//mms:eigenVector");
        if (vectors.getLength() == 0) {
            vectors = AGDMUtils.find(problem, "//mms:eigenSpace[ancestor::mms:generalCharacteriscticDecomposition]//mms:eigenVector");
        }
        //Iterate over the vectors, neighbor needed to have vectors too.
        for (int j = 0; j < vectors.getLength(); j++) {
            Element vectorTag = (Element) vectors.item(j).cloneNode(true);
            Element equation = AGDMUtils.createElement(doc, mtUri, "math");
            Element apply = AGDMUtils.createElement(doc, mtUri, "apply");
            Element eq = AGDMUtils.createElement(doc, mtUri, "eq");
            ProcessInfo newPi = new ProcessInfo(pi);
            newPi.setField(vectorTag.getFirstChild().getTextContent());
            newPi.setSpatialCoord1(contCoord);
            newPi.setSpatialCoord2(null);
            Element vectorTime = ExecutionFlow.replaceTags(AGDMUtils.createTimeSlice(doc), newPi);
            
            Element ci = AGDMUtils.createElement(doc, mtUri, "ci");
            ci.setTextContent(vectorTag.getFirstChild().getTextContent());
            apply.appendChild(eq);
            apply.appendChild(ci);
            if (noNeighbour) {
                apply.appendChild(doc.importNode(
                        AGDMUtils.createVectorInstructionOperated(vectorTag, timeSlice.cloneNode(true), newPi, "", "", 0, true), true));
            }
            else {
                apply.appendChild(doc.importNode(
                        AGDMUtils.createVectorInstructionOperated(vectorTag, timeSlice.cloneNode(true), newPi, coord, op, 1, true), true));
            }
            equation.appendChild(apply);
            if (noNeighbour) {
                AGDMUtils.createTimeSliceOperated(equation, newPi, vectorTime, "", "", 0);
            }
            else {
                AGDMUtils.createTimeSliceOperated(equation, newPi, vectorTime, coord, op, 1);
            }
            
            fragmentResult.appendChild(equation);
        }
        return fragmentResult;
    }
    
    /**
     * Creates the undiagonalization mathML.
     * @param problem               The document with the info
     * @param timeSlice             The time slice when to calculate the boundary
     * @param pi                    The process information
     * @param coord                 The coordinate to create the boundary
     * @param limit                 The boundary limit ("lower" or "upper")
     * @return                      The undiagonalization content.
     * @throws AGDMException        AGDM007 External error
     */
    private static DocumentFragment undiagonalization(Node problem, Element timeSlice, ProcessInfo pi, String coord, String limit) 
        throws AGDMException {
        Document doc = timeSlice.getOwnerDocument();
        DocumentFragment fragmentResult = doc.createDocumentFragment();
        
        String contCoord = pi.getCoordinateRelation().getDiscToCont(coord);
        String direction;
        if (limit.equals("lower")) {
            direction = "right";
        }
        else {
            direction = "left";
        }
        NodeList undiagonalzations = AGDMUtils.find(problem, "//mms:coordinateCharacteristicDecomposition[mms:eigenCoordinate = '" 
                + contCoord + "']//mms:undiagonalization");
        if (undiagonalzations.getLength() == 0) {
            undiagonalzations = AGDMUtils.find(problem, "//mms:undiagonalization[ancestor::mms:generalCharacteristicDecomposition]");
        }
        for (int j = 0; j < undiagonalzations.getLength(); j++) {
            Element undiagonalizeTag = (Element) undiagonalzations.item(j).cloneNode(true);
            Element equation = AGDMUtils.createElement(doc, mtUri, "math");
            Element apply = AGDMUtils.createElement(doc, mtUri, "apply");
            Element eq = AGDMUtils.createElement(doc, mtUri, "eq");
            ProcessInfo newPi = new ProcessInfo(pi);
            newPi.setField(undiagonalizeTag.getFirstChild().getTextContent());
            newPi.setSpatialCoord1(contCoord);
            newPi.setSpatialCoord2(null);
            Element fieldTime = null;
            if (AGDMUtils.isField(pi.getCompleteProblem(), undiagonalizeTag.getFirstChild().getTextContent())) {
                fieldTime = AGDMUtils.createElement(doc, mtUri, "apply");
                Element ci = (Element) doc.importNode(ExecutionFlow.replaceTags(timeSlice.getFirstChild().cloneNode(true), newPi), true);
                fieldTime.appendChild(ci);
                fieldTime.appendChild(getBoundaryCoordinates(doc, pi.getRegionInfo(), pi.getCoordinateRelation(), coord, null, direction));
            }
            else {
                fieldTime =  AGDMUtils.createElement(doc, mtUri, "ci");
                fieldTime.setTextContent(undiagonalizeTag.getFirstChild().getTextContent() + "_var");
            }
            apply.appendChild(eq);
            apply.appendChild(fieldTime);
            apply.appendChild(doc.importNode(AGDMUtils.createCoefficientInstruction(
                undiagonalizeTag, AGDMUtils.createTimeSlice(doc), AGDMUtils.createTimeSlice(doc), 
                newPi, true, false, true, true), true));
            equation.appendChild(apply);
            fragmentResult.appendChild(equation);
        }
        return fragmentResult;
    }
    
    /**
     * Creates the maximal dissipation boundary for the greater than condition for a spatial coordinate.
     * @param condition             The condition structure
     * @param pi                    The process information
     * @param coord                 The coordinate to create the boundary
     * @param limit                 The boundary limit ("lower" or "upper")
     * @return                      The boundaries for the spatial coordinate
     * @throws AGDMException        AGDM007 External error
     */
    private static DocumentFragment maximalDissipationGT(Element condition, ProcessInfo pi, String coord, String limit)
        throws AGDMException {
        Document doc = condition.getOwnerDocument();
        DocumentFragment fragmentResult = doc.createDocumentFragment();
        
        String contCoord = pi.getCoordinateRelation().getDiscToCont(coord);
        pi.setSpatialCoord1(contCoord);
        pi.setSpatialCoord2(null);
        if (limit.equals("upper")) {
            Element newOperation = AGDMUtils.createElement(doc, mtUri, "geq");
            condition.getFirstChild().getFirstChild().replaceChild(newOperation, condition.getFirstChild().getFirstChild().getFirstChild());
            Element gtRightCond = (Element) condition.cloneNode(true);
            Element gtRightThen = AGDMUtils.createElement(doc, smlUri, "then");
            gtRightCond.appendChild(gtRightThen);
            Element distance = AGDMUtils.createElement(doc, mtUri, "cn");
            distance.setTextContent("1");
            //upper boundary
            Element math = AGDMUtils.createElement(doc, mtUri, "math");
            Element apply = AGDMUtils.createElement(doc, mtUri, "apply");
            Element eq = AGDMUtils.createElement(doc, mtUri, "eq");
            Element leftSide = AGDMUtils.createElement(doc, mtUri, "apply");
            leftSide.appendChild(doc.importNode(ExecutionFlow.replaceTags(AGDMUtils.createTimeSlice(doc).getFirstChild().cloneNode(true), pi), true));
            leftSide.appendChild(
                    getBoundaryCoordinates(doc, pi.getRegionInfo(), pi.getCoordinateRelation(), coord, null, "left"));
            Element rightSide = AGDMUtils.createElement(doc, mtUri, "apply");
            rightSide.appendChild(doc.importNode(
                    ExecutionFlow.replaceTags(AGDMUtils.createTimeSlice(doc).getFirstChild().cloneNode(true), pi), true));
            rightSide.appendChild(
                    getBoundaryCoordinates(doc, pi.getRegionInfo(), pi.getCoordinateRelation(), coord, distance, "left"));
            apply.appendChild(eq);
            apply.appendChild(leftSide);
            apply.appendChild(rightSide);
            math.appendChild(apply);
            gtRightThen.appendChild(math);
            fragmentResult.appendChild(gtRightCond);
        }
        return fragmentResult;
    }
    
    /**
     * Creates the maximal dissipation boundary for the lower than condition for a spatial coordinate.
     * @param condition             The condition structure
     * @param pi                    The process information
     * @param coord                 The coordinate to create the boundary
     * @param limit                 The boundary limit ("lower" or "upper")
     * @return                      The boundaries for the spatial coordinate
     * @throws AGDMException        AGDM007 External error
     */
    private static DocumentFragment maximalDissipationLT(Element condition, ProcessInfo pi, String coord, String limit) 
        throws AGDMException {
        Document doc = condition.getOwnerDocument();
        DocumentFragment fragmentResult = doc.createDocumentFragment();
        
        String contCoord = pi.getCoordinateRelation().getDiscToCont(coord);
        pi.setSpatialCoord1(contCoord);
        pi.setSpatialCoord2(null);
        if (limit.equals("lower")) {
            Element newOperation = AGDMUtils.createElement(doc, mtUri, "geq");
            condition.getFirstChild().getFirstChild().replaceChild(newOperation, condition.getFirstChild().getFirstChild().getFirstChild());
            Element ltRightCond = (Element) condition.cloneNode(true);
            Element ltRightThen = AGDMUtils.createElement(doc, smlUri, "then");
            ltRightCond.appendChild(ltRightThen);
            Element distance = AGDMUtils.createElement(doc, mtUri, "cn");
            distance.setTextContent("1");
            //left boundary
            Element math = AGDMUtils.createElement(doc, mtUri, "math");
            Element apply = AGDMUtils.createElement(doc, mtUri, "apply");
            Element eq = AGDMUtils.createElement(doc, mtUri, "eq");
            Element leftSide = AGDMUtils.createElement(doc, mtUri, "apply");
            leftSide.appendChild(doc.importNode(ExecutionFlow.replaceTags(AGDMUtils.createTimeSlice(doc).getFirstChild().cloneNode(true), pi), true));
            leftSide.appendChild(
                    getBoundaryCoordinates(doc, pi.getRegionInfo(), pi.getCoordinateRelation(), coord, null, "right"));
            Element rightSide = AGDMUtils.createElement(doc, mtUri, "apply");
            rightSide.appendChild(doc.importNode(ExecutionFlow.replaceTags(
                    AGDMUtils.createTimeSlice(doc).getFirstChild().cloneNode(true), pi), true));
            rightSide.appendChild(
                    getBoundaryCoordinates(doc, pi.getRegionInfo(), pi.getCoordinateRelation(), coord, distance, "right"));
            apply.appendChild(eq);
            apply.appendChild(leftSide);
            apply.appendChild(rightSide);
            math.appendChild(apply);
            ltRightThen.appendChild(math);
            fragmentResult.appendChild(ltRightCond);
            

        }
        //Static, copiamos paso anterior, no hace falta hacer nada
       
        return fragmentResult;
    }
    
    /**
     * Deploy the coordinates for the boundaries depending on the direction and the distance from the value.
     * @param doc               The document with the info
     * @param segRel            The region relation
     * @param coordRel          Relation between coordinates
     * @param spatialCoord      The spatial coordinate to create the boundary
     * @param distance          The increment or decrement distance from the limit
     * @param direction         The direction to take for the operations ("left" or "right")
     * @return                  A document fragment with the coordinate indexes for the boundary
     */
    private static DocumentFragment getBoundaryCoordinates(Document doc, RegionInfo segRel, CoordinateInfo coordRel, 
            String spatialCoord, Element distance, String direction) {
       
        ArrayList<String> spatialCoords = coordRel.getDiscCoords();
        DocumentFragment operate = doc.createDocumentFragment();
        for (int j = 0; j < spatialCoords.size(); j++) {
            //Operate coordinate
            if (spatialCoord.equals(spatialCoords.get(j))) {
                if (distance != null) {
                    Element apply = AGDMUtils.createElement(doc, mtUri, "apply");
                    Element operation;
                    //Select operation depending on the side
                    if (direction.equals("left")) {
                        operation = AGDMUtils.createElement(doc, mtUri, "minus");
                    }
                    else {
                        operation = AGDMUtils.createElement(doc, mtUri, "plus");
                    }
                    apply.appendChild(operation);
                    Element ci = AGDMUtils.createElement(doc, mtUri, "ci");
                    ci.setTextContent(spatialCoord);
                    apply.appendChild(ci.cloneNode(true)); 
                    apply.appendChild(distance);
                    operate.appendChild(apply);
                }
                else {
                    Element ci = AGDMUtils.createElement(doc, mtUri, "ci");
                    ci.setTextContent(spatialCoords.get(j));
                    operate.appendChild(ci.cloneNode(true));
                }
            }
            //Not operate coordinate
            else {
                Element ci = AGDMUtils.createElement(doc, mtUri, "ci");
                ci.setTextContent(spatialCoords.get(j));
                operate.appendChild(ci.cloneNode(true));
            }
        }
        return operate;
    }
    
    /**
     * Deploy the coordinates for the boundaries depending on the direction and the distance from the value.
     * @param doc               The document with the info
     * @param segRel            The region relation
     * @param coordRel          Relation between coordinates
     * @param spatialCoord      The spatial coordinate to create the boundary
     * @return                  A document fragment with the coordinate indexes for the boundary
     */
    private static DocumentFragment getGhostCoordinates(Document doc, RegionInfo segRel, CoordinateInfo coordRel, 
            String spatialCoord) {
       
        ArrayList<String> spatialCoords = coordRel.getDiscCoords();
        DocumentFragment operate = doc.createDocumentFragment();
        for (int j = 0; j < spatialCoords.size(); j++) {
            //Operate coordinate
            if (spatialCoord.equals(spatialCoords.get(j))) {
                Element mdv = AGDMUtils.createElement(doc, smlUri, "minDomainValue");
                Element ci = AGDMUtils.createElement(doc, mtUri, "ci");
                ci.setTextContent(spatialCoords.get(j));
                mdv.appendChild(ci);
                operate.appendChild(mdv);
            }
            //Not operate coordinate
            else {
                Element ci = AGDMUtils.createElement(doc, mtUri, "ci");
                ci.setTextContent(spatialCoords.get(j));
                operate.appendChild(ci);
            }
        }
        return operate;
    }
    
    /**
     * Deploy the coordinates for the boundaries depending on the direction and the distance from the value on reflection.
     * @param doc               The document with the info
     * @param segRel            The region relation
     * @param coordRel          Relation between coordinates
     * @param spatialCoord      The spatial coordinate to create the boundary
     * @param direction         The direction to take for the operations ("left" or "right")
     * @return                  A document fragment with the coordinate indexes for the boundary
     */
    private static DocumentFragment getReflectionCoordinates(Document doc, RegionInfo segRel, 
            CoordinateInfo coordRel, String spatialCoord, String direction) {
       
        ArrayList<String> spatialCoords = coordRel.getDiscCoords();
        DocumentFragment operate = doc.createDocumentFragment();
        for (int j = 0; j < spatialCoords.size(); j++) {
            //Operate coordinate
            if (spatialCoord.equals(spatialCoords.get(j))) {
                Element applyOp = AGDMUtils.createElement(doc, mtUri, "apply");
                Element applyTimes = AGDMUtils.createElement(doc, mtUri, "apply");
                Element mathOperation;
                //Select operation depending on the side
                if (direction.equals("lower")) {
                    mathOperation = AGDMUtils.createElement(doc, mtUri, "plus");
                }
                else {
                    mathOperation = AGDMUtils.createElement(doc, mtUri, "minus");
                }
                
                Element times = AGDMUtils.createElement(doc, mtUri, "times");
                Element coordinate = AGDMUtils.createElement(doc, mtUri, "ci");
                coordinate.setTextContent(spatialCoords.get(j));  
                Element rc = AGDMUtils.createElement(doc, mtUri, "ci");
                rc.setTextContent("ReflectionCounter");
                Element one = AGDMUtils.createElement(doc, mtUri, "cn");
                one.setTextContent("1");
                Element two = AGDMUtils.createElement(doc, mtUri, "cn");
                two.setTextContent("2");
                
                applyTimes.appendChild(times);
                applyTimes.appendChild(two);
                applyTimes.appendChild(rc);
                
                applyOp.appendChild(mathOperation);
                applyOp.appendChild(coordinate);
                applyOp.appendChild(applyTimes);
                
                operate.appendChild(applyOp);
            }
            //Not operate coordinate
            else {
                Element ci = AGDMUtils.createElement(doc, mtUri, "ci");
                ci.setTextContent(spatialCoords.get(j));
                operate.appendChild(ci.cloneNode(true));
            }
        }
        return operate;
    }
    
    /**
     * Generates the if element with the condition.
     * @param condition     The condition of the boundary
     * @return              The if element
     */
    private static Element generateLimitCondition(Node condition) {
        Document doc = condition.getOwnerDocument();
        Element ifTag = AGDMUtils.createElement(doc, smlUri, "if");
        ifTag.appendChild(((Element) condition).getElementsByTagName("mt:math").item(0));
        Element then = AGDMUtils.createElement(doc, smlUri, "then");
        ifTag.appendChild(then);
        return ifTag;
    }
    
    /**
     * Creates the assignment of the region that is going to become a particle if it enters the interior domain.
     * @param pi        The problem information
     * @return          The equation
     */
    private static Element createNewRegionAssignment(ProcessInfo pi) {
        Document base = pi.getCompleteProblem();
        Element math = AGDMUtils.createElement(base, mtUri, "math");
        Element apply = AGDMUtils.createElement(base, mtUri, "apply");
        Element eq = AGDMUtils.createElement(base, mtUri, "eq");
        Element newRegion = AGDMUtils.createElement(base, mtUri, "apply");
        Element ci = AGDMUtils.createElement(base, mtUri, "ci");
        ci.setTextContent("newRegion");
        newRegion.appendChild(ci);
        for (int i = 0; i < pi.getBaseSpatialCoordinates().size(); i++) {
            Element coord = AGDMUtils.createElement(base, mtUri, "ci");
            coord.setTextContent(pi.getCoordinateRelation().getDiscCoords().get(i));
            newRegion.appendChild(coord);
        }
        
        Element regionId = null;
        String regionName = pi.getRegionName();
        if (regionName.substring(regionName.length() - 1).equals("I")) {
            regionId = AGDMUtils.createElement(base, smlUri, "regionInteriorId");
        } 
        else {
            regionId = AGDMUtils.createElement(base, smlUri, "regionSurfaceId");
        }
        regionId.setTextContent(regionName.substring(0, regionName.length() - 1));
        
        
        math.appendChild(apply);
        apply.appendChild(eq);
        apply.appendChild(newRegion);
        apply.appendChild(regionId);
        
        return math;
    }
    
    /**
     * Process the post initialization rules from the schema.
     * @param discProblem       The discrete problem
     * @param problem           The region problem
     * @param pi                The process info
     * @param timeSlice         The time slice
     * @param schema            The schema
     * @return                  The instructions
     * @throws AGDMException    AGDM00X  External error
     */
    private static DocumentFragment processPostInit(Document discProblem, Node problem, ProcessInfo pi, Element timeSlice, 
            Document schema) throws AGDMException {
        DocumentFragment result = discProblem.createDocumentFragment();
        
        if (pi.getPostInitialization() != null) { 
	        Node postInit = pi.getPostInitialization().cloneNode(true);
	        NodeList fieldVars = AGDMUtils.find(postInit, ".//mt:apply[mt:ci/mt:msup//sml:timeCoordinate]");
	        for (int i = fieldVars.getLength() - 1; i >= 0; i--) {
	            Element newTimeSlice = (Element) timeSlice.cloneNode(true);
	            fieldVars.item(i).getParentNode().replaceChild(postInit.getOwnerDocument().importNode(newTimeSlice.getFirstChild(), true), fieldVars.item(i));
	        }
	        String query = "";
	        if (pi.getDiscretizationType() == DiscretizationType.mesh) {
	        	query = "./descendant-or-self::sml:simml/*[not(local-name() = 'iterateOverParticles')]";
	        }
	        else {
	           	query = "./descendant-or-self::sml:iterateOverParticles[@speciesNameAtt = '" + pi.getCurrentSpecies() + "' or not(@speciesNameAtt)]/*";	
	        }
	        NodeList flowChildren = AGDMUtils.find(postInit, query);
	        for (int i = 0; i < flowChildren.getLength(); i++) {
	            Element rule = (Element) flowChildren.item(i);
	            RuleInfo ri = new RuleInfo();
	            ri.setIndividualField(false);
	            ri.setIndividualCoord(false);
	            ri.setFunction(false);
	            ri.setProcessInfo(pi);
	            ri.setAuxiliaryCDAdded(false);
	            ri.setAuxiliaryFluxesAdded(false);
	            result.appendChild(discProblem.importNode(ExecutionFlow.processExecutionFlowChild(discProblem, problem, rule, ri), true));
	        }
        }
        return result;
    }
}
