/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.AGDM.utils;

import eu.iac3.mathMS.AGDM.AGDMException;
import eu.iac3.mathMS.AGDM.ExtrapolationType;
import eu.iac3.mathMS.AGDM.MeshInterpolationType;
import eu.iac3.mathMS.AGDM.ParticleInterpolationType;
import eu.iac3.mathMS.AGDM.utils.PDE.PDEAnalysisDiscPolicy;
import eu.iac3.mathMS.AGDM.utils.PDE.PDERegionDiscPolicy;
import eu.iac3.mathMS.AGDM.utils.PDE.Species;
import eu.iac3.mathMS.documentManagerPlugin.DMException;
import eu.iac3.mathMS.documentManagerPlugin.DocumentManager;
import eu.iac3.mathMS.documentManagerPlugin.DocumentManagerImpl;
import eu.iac3.mathMS.simflownyUtilsPlugin.SUException;
import eu.iac3.mathMS.simflownyUtilsPlugin.SimflownyUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Discretization policy for a problem.
 * Every region has its discretization schema an parameters.
 * All the problem shares coordinate discretization schema.
 * This class will be used in a web service, so its made of pojos.
 * @author bminyano
 *
 */
public class DiscretizationPolicy {
    private String problem;
    private ExtrapolationType extrapolationMethod;
    private ArrayList<PDERegionDiscPolicy> regionDiscPolicies;
    private PDEAnalysisDiscPolicy analysisDiscretization;
    private ArrayList<String> meshFields;
    private MeshInterpolationType meshInterpolationMethod;
    private ParticleInterpolationType particleInterpolationMethod;
    private HashMap<String, Species> particleSpecies;
    private Element postInitialization;
    private ArrayList<Float> longestTimestepFactors;
    private HashMap<Integer, HashMap<String, Element>> stepFieldTemplates;
   
    private ArrayList<Element> analysisInputVariables;

    
    /**
     * Initializes the discretization policy from the xml document.
     * @param xml               The document with the data
     * @throws AGDMException 
     * @throws SUException 
     * @throws DMException 
     */
    public DiscretizationPolicy(Document xml) throws AGDMException, DMException, SUException {
        
        Node problemLocal = xml.getFirstChild().getFirstChild().getNextSibling();
        this.problem = problemLocal.getLastChild().getTextContent();

        //Process every region discretization
        NodeList regionDiscretizations = xml.getDocumentElement().getElementsByTagName("mms:regionDiscretization");
        ArrayList<PDERegionDiscPolicy> rdp = new ArrayList<PDERegionDiscPolicy>();
        for (int i = 0; i < regionDiscretizations.getLength(); i++) {
        	PDERegionDiscPolicy tmpRDP = new PDERegionDiscPolicy(regionDiscretizations.item(i));
            rdp.add(tmpRDP);
        	//Also get the longest schema timestep factors (and check schemas compatibility)
            if (i == 0) {
            	longestTimestepFactors = new ArrayList<Float>(tmpRDP.getTimestepFactors());
            }
            else {
            	ArrayList<Float> tmpFactors = tmpRDP.getTimestepFactors();
            	if (tmpFactors.size() > longestTimestepFactors.size()) {
                  	longestTimestepFactors = new ArrayList<Float>(tmpFactors);
            	}
            }
        }
        this.regionDiscPolicies = rdp;
        
        //Analysis discretization is optional
        NodeList analysisOperator = ((Element) xml.getFirstChild()).getElementsByTagName("mms:analysisDiscretization");
        if (analysisOperator.getLength() > 0) {
            analysisDiscretization = new PDEAnalysisDiscPolicy(analysisOperator.item(0));
        }
        
        //Particle species
        particleSpecies = new HashMap<String, Species>();
        NodeList species = xml.getDocumentElement().getElementsByTagName("mms:species");
        for (int i = 0; i < species.getLength(); i++) {
        	Element speciesElement = (Element) species.item(i); 
        	String name = speciesElement.getElementsByTagName("mms:name").item(0).getTextContent();
        	ArrayList<String> variables = new ArrayList<String>();
            NodeList particleVariables = speciesElement.getElementsByTagName("mms:particleVariable");
            for (int j = 0; j < particleVariables.getLength(); j++) {
            	variables.add(particleVariables.item(j).getTextContent());
            }
            NodeList velocities = speciesElement.getElementsByTagName("mms:velocity");
            HashMap<String, Element> particleVelocities = new HashMap<String, Element>();
            for (int j = 0; j < velocities.getLength(); j++) {
            	particleVelocities.put(velocities.item(j).getFirstChild().getTextContent(), (Element) velocities.item(j).getLastChild().getFirstChild());
            }
            NodeList volume = AGDMUtils.find(speciesElement, "./mms:volume/mt:math");
            Element particleVolume = null;
            if (volume.getLength() > 0) {
            	particleVolume = (Element) volume.item(0).cloneNode(true);
            }
        	particleSpecies.put(name, new Species(name, variables, particleVolume, particleVelocities));
        }
        
        meshFields = new ArrayList<String>();
        NodeList meshVariables = xml.getDocumentElement().getElementsByTagName("mms:meshVariable");
        for (int i = 0; i < meshVariables.getLength(); i++) {
        	meshFields.add(meshVariables.item(i).getTextContent());
        }

        //Extrapolation method is optional
        NodeList extrapolationMethodLocal = ((Element) xml.getFirstChild()).getElementsByTagName("mms:extrapolationMethod");
        if (extrapolationMethodLocal.getLength() > 0) {
            this.extrapolationMethod = ExtrapolationType.valueOf(extrapolationMethodLocal.item(0).getTextContent());
        }
        else {
            //Default values
            this.extrapolationMethod = ExtrapolationType.Linear_LagrangianExtrapolation;
        }
        
        //Interpolation methods
        NodeList meshInterpolationMethodLocal = ((Element) xml.getFirstChild()).getElementsByTagName("mms:meshInterpolationMethod");
        if (meshInterpolationMethodLocal.getLength() > 0) {
            this.meshInterpolationMethod = MeshInterpolationType.valueOf(meshInterpolationMethodLocal.item(0).getTextContent());
        }
        else {
            //Default values
            this.meshInterpolationMethod = MeshInterpolationType.Linear_LagrangianInterpolation;
        }
        NodeList particleInterpolationMethodLocal = ((Element) xml.getFirstChild()).getElementsByTagName("mms:particleInterpolationMethod");
        if (particleInterpolationMethodLocal.getLength() > 0) {
            this.particleInterpolationMethod = ParticleInterpolationType.valueOf(particleInterpolationMethodLocal.item(0).getTextContent());
        }
        else {
            //Default values
            this.particleInterpolationMethod = ParticleInterpolationType.WendlandInterpolation;
        }
        
        analysisInputVariables = new ArrayList<Element>();
    	NodeList variables = AGDMUtils.find(xml, "//mms:analysisDiscretization//mms:inputVariable/mt:math");
    	for (int i = 0; i < variables.getLength(); i++) {
    		analysisInputVariables.add((Element) variables.item(i).cloneNode(true));
    	}
    	
        NodeList postInitializationList = AGDMUtils.find(xml, "//mms:postInitialization/sml:simml");
        if (postInitializationList.getLength() > 0) {
        	this.postInitialization = (Element) postInitializationList.item(0).cloneNode(true);
        }
        else {
        	this.postInitialization = null;
        }
    }

	public MeshInterpolationType getMeshInterpolationMethod() {
		return meshInterpolationMethod;
	}

	public void setMeshInterpolationMethod(MeshInterpolationType meshInterpolationMethod) {
		this.meshInterpolationMethod = meshInterpolationMethod;
	}

	public ParticleInterpolationType getParticleInterpolationMethod() {
		return particleInterpolationMethod;
	}

	public void setParticleInterpolationMethod(ParticleInterpolationType particleInterpolationMethod) {
		this.particleInterpolationMethod = particleInterpolationMethod;
	}

	public String getProblem() {
        return problem;
    }

    public void setProblem(String problem) {
        this.problem = problem;
    }

    public ExtrapolationType getExtrapolationMethod() {
        return extrapolationMethod;
    }
    public void setExtrapolationMethod(ExtrapolationType extrapolationMethod) {
        this.extrapolationMethod = extrapolationMethod;
    }
    
    public HashMap<Integer, HashMap<String, Element>> getStepFieldTemplates() {
		return stepFieldTemplates;
	}

	public ArrayList<PDERegionDiscPolicy> getRegionDiscPolicies() {
        return regionDiscPolicies;
    }

    public void setRegionDiscPolicies(ArrayList<PDERegionDiscPolicy> grdp) {
        this.regionDiscPolicies = grdp;
    }
    
    public PDEAnalysisDiscPolicy getAnalysisDiscretization() {
        return analysisDiscretization;
    }

    public ArrayList<String> getMeshFields() {
		return meshFields;
	}

	public ArrayList<Element> getAnalysisInputVariables() {
		return analysisInputVariables;
	}

	public Element getPostInitialization() {
		return postInitialization;
	}

	public void setPostInitialization(Element postInitialization) {
		this.postInitialization = postInitialization;
	}

	public HashMap<String, Species> getParticleSpecies() {
		return particleSpecies;
	}

    public ArrayList<Float> getLongestTimestepFactors() {
		return longestTimestepFactors;
	}

	/**
     * Return common fields from given ones
     * @param globalFields		Given fields
     * @return					Common fields 
     */
    public List<String> getCommonMeshFields(ArrayList<String> globalFields) {
    	List<String> result = meshFields.stream()
    			  .distinct()
    			  .filter(globalFields::contains)
    			  .collect(Collectors.toList());
		return result;
	}
	
	/**
	 * Get particle species information that has fields in common with the given by parameter.
	 * @param fields	Fields to check 
	 * @return			Species with common fields
	 */
	public HashMap<String, Species> getCommonParticleSpecies(ArrayList<String> fields) {
		HashMap<String, Species> commonParticleSpecies = new HashMap<String, Species>();
		
		for (Entry<String, Species> entry : particleSpecies.entrySet()) {
    		String speciesName = entry.getKey();
    		Species sp = new Species(entry.getValue());
    		List<String> spFields = sp.getParticleFields()
    				.stream()
    				.distinct()
    				.filter(fields::contains)
      			  .collect(Collectors.toList());
    		if (!spFields.isEmpty()) {
    			commonParticleSpecies.put(speciesName, sp);
    		}
    	}
		return commonParticleSpecies;
	}

	
	/**
     * Check the discretization policy.
     * Checks that there are one region group policies for every region group.
     * @param coordinates				Spatial coordinates
     * @param problemFields				Problem fields
     * @param referenceFrames			Field reference frames
     * @throws AGDMException AGDM007    Invalid discretization policy
     */
    public void checkPolicy(ArrayList<String> coordinates, ArrayList<String> problemFields, HashMap<String, ReferenceFrameType> referenceFrames, HashMap<String, ArrayList<String>> regionsAndFieldGroups) throws AGDMException {
    	//No lagrangian field can be discretized with mesh
    	for (String meshField : meshFields) {
    		if (referenceFrames.get(meshField) == ReferenceFrameType.lagrangian) {
    			throw new AGDMException(AGDMException.AGDM006, "Lagrangian equation cannot be discretized with Finite Diferences (" + meshField + ").");
    		}
    	}
    	//All fields
    	if (meshFields.size() == 1 && meshFields.get(0).equals("All")) {
    		meshFields.remove(0);
    		meshFields.addAll(problemFields);
    	}
    	for (Species species : particleSpecies.values()) {
    		ArrayList<String> fields = species.getParticleFields();
	    	if (fields.size() == 1 && fields.get(0).equals("All")) {
	    		fields.remove(0);
	    		fields.addAll(problemFields);
	    	}
	    	species.setParticleFields(fields);
    	}
    	//All problem fields must be covered and no field overlap
    	for (String field : meshFields) {
    		if (!problemFields.remove(field)) {
    			throw new AGDMException(AGDMException.AGDM006, "A field can only be discretized with Finite Differences or a particle species (" + field + ").");
    		}
    	}
    	for (Species species : particleSpecies.values()) {
    		for (String field : species.getParticleFields()) {
        		if (!problemFields.remove(field)) {
        			throw new AGDMException(AGDMException.AGDM006, "A field can only be discretized with Finite Differences or a particle species (" + field + ").");
        		}
    		}
    		Set<String> vels = species.getParticleVelocities().keySet();
    		if (vels.size() > 0) {
    			if (!vels.containsAll(coordinates)) {
        			throw new AGDMException(AGDMException.AGDM006, "Particle velocities must be defined for all spatial coordinates for species (" + species.getName() + ").");

    			}
    		}
    	}
    	if (!problemFields.isEmpty()) {
			throw new AGDMException(AGDMException.AGDM006, "All problem fields must be discretized (" + problemFields + ").");
    	}
    	
    	//Checks that all regions are covered completely. If a region is discretized partially (using field groups) make sure all groups are discretized
    	//Also make sure a region are not discretized more than once (even partially)
    	ArrayList<String> partiallyDiscretizedRegions = new ArrayList<String>();
    	for (int i = 0; i < regionDiscPolicies.size(); i++) {
    		PDERegionDiscPolicy regionDiscPolicy = regionDiscPolicies.get(i);
    		String regionName = regionDiscPolicy.getRegionName();
    		ArrayList<String> groups = regionDiscPolicy.getFieldGroups();
    		if (regionsAndFieldGroups.containsKey(regionName)) {
    			ArrayList<String> remainGroups = regionsAndFieldGroups.get(regionName);
    			//Both empty groups means the region has no discretization by groups  
    			if (groups.isEmpty() && remainGroups.isEmpty()) {
    				regionsAndFieldGroups.remove(regionName);
    			}
    			else {
    				// A discretization overlap occurs
    				if (groups.isEmpty() && !remainGroups.isEmpty() && partiallyDiscretizedRegions.contains(regionName)) {
    					throw new AGDMException(AGDMException.AGDM006, "Region " + regionName + " is trying to be discretized by two policies");		
    				}
    				// A region in the problem has variable groups, but the discretization of this region is for all groups. 
    				if (groups.isEmpty() && !remainGroups.isEmpty() && !partiallyDiscretizedRegions.contains(regionName)) {
    					regionsAndFieldGroups.remove(regionName);
    				}
    				// Trying to discretize by group a region with no variable groups.
    				if (!groups.isEmpty() && remainGroups.isEmpty()) {
    					throw new AGDMException(AGDMException.AGDM006, "Region " + regionName + " does not have variable groups " + groups.toString());
    				}
    				//Partially discretization for a region
    				if (!groups.isEmpty() && !remainGroups.isEmpty()) {
    					partiallyDiscretizedRegions.add(regionName);
    					if (remainGroups.containsAll(groups)) {
    						remainGroups.removeAll(groups);
    						if (remainGroups.isEmpty()) {
    							regionsAndFieldGroups.remove(regionName);
    						}
    					}
    					else {
    						throw new AGDMException(AGDMException.AGDM006, "Trying to discretize a variable group twice, or non existing for region " + regionName + ".");	
    					}
    					
    				}
    			}
    		}
    		// Same region with different discretization policies
    		else {
    			throw new AGDMException(AGDMException.AGDM006, "Region " + regionName + " is trying to be discretized by two policies");
    		}	
    	}
        assignIdsToSchemas();
        checkMultipleSchemaCompatibility();
    }
    
    /**
     * Assign an unique identifier to the schema Xindice ID.
     */
    public void assignIdsToSchemas() {
        Hashtable<String, Integer> ids = new Hashtable<String, Integer>();
        int counter = 0;
        for (int i = 0; i < regionDiscPolicies.size(); i++) {
            String schemaId = regionDiscPolicies.get(i).getTimeSchema();
            if (!ids.containsKey(schemaId)) {
                ids.put(schemaId, counter);
                counter++;
            }
            regionDiscPolicies.get(i).setSchemaId(ids.get(schemaId));
        }
    }
    
    /**
     * Check multiple schema compatibility. In case there are multiple time discretization schemas, all of them must have
     * the same time step factors and those factors must be ordered.
     * @throws AGDMException
     */
    public void checkMultipleSchemaCompatibility() throws AGDMException {
    	boolean differentSchemas = false;
    	for (int i = 0; i < regionDiscPolicies.size(); i++) {
    		ArrayList<Float> timestepFactors = regionDiscPolicies.get(i).getTimestepFactors();
    		//When different schemas are used, the timestep factors must be compatible
    		if (!timestepFactors.equals(longestTimestepFactors)) {
    			differentSchemas = true;
    			List<Float> differences = longestTimestepFactors.stream().distinct()
    		            .filter(element -> !timestepFactors.contains(element))
    		            .collect(Collectors.toList());
    			//All factors must be at both schemas
    		 	if (!differences.isEmpty()) {
    		 		throw new AGDMException(AGDMException.AGDM002, "Incompatible region discretizations for region " + regionDiscPolicies.get(i).getRegionName());
    		 	}
    			//Factors must be ordered
    		 	if (!timestepFactors.stream().sorted().collect(Collectors.toList()).equals(timestepFactors)) {
    		 		throw new AGDMException(AGDMException.AGDM002, "Using different schemas require all of their timestep factors be in order. Region " + regionDiscPolicies.get(i).getRegionName() + " is trying to be discretized with an unordered time step schema");
    		 	}
    		}
    	}
    	if (differentSchemas) {
    		//Also check longest schema order
    		if (!longestTimestepFactors.stream().sorted().collect(Collectors.toList()).equals(longestTimestepFactors)) {
		 		throw new AGDMException(AGDMException.AGDM002, "Using different schemas require all of their timestep factors be in order.");
		 	}
    	}
    	//Check that any field is trying to be discretized with different schemas 
    	HashMap<String, String> fieldSchemaMap = new HashMap<String, String>();
    	for (PDERegionDiscPolicy rdp: regionDiscPolicies) {
    		ArrayList<String> fields = rdp.getRegionInfo().getFields();
    		String schema = rdp.getTimeSchema();
    		for (String f: fields) {
    			String previousSchema = fieldSchemaMap.get(f);
    			if (previousSchema != null && !schema.equals(previousSchema)) {
    		 		throw new AGDMException(AGDMException.AGDM002, "Fields are not allowed to be discretized with different schemas (" + f + ").");
    			}
    			fieldSchemaMap.putIfAbsent(f, schema);
    		}
    	}
    }
    
    /**
     * Returns the extrapolation stencil if any.
     * @return	extrapolation stencil
     */
    public int getExtrapolationStencil() {
        if (extrapolationMethod != null) {
            if (extrapolationMethod.equals(ExtrapolationType.Linear_LagrangianExtrapolation)) {
                return 2;
            }
            if (extrapolationMethod.equals(ExtrapolationType.Quadratic_LagrangianExtrapolation)) {
                return 3;
            }
            if (extrapolationMethod.equals(ExtrapolationType.Cubic_LagrangianExtrapolation)) {
                return 4;
            }
        }
        return 0;
    }
    
    /**
     * Returns the schema parameters.
     * @return a list of the parameters
     * @throws AGDMException 
     */
    public ArrayList<Node> getSchemaParameters() throws AGDMException {
    	ArrayList<Node> params = new ArrayList<Node>();
		for (int i = 0; i < regionDiscPolicies.size(); i++) {
			String schemaID = regionDiscPolicies.get(i).getTimeSchema();
	        //Load time schema
	        Document schema = null;
	        try {
	            DocumentManager docMan = new DocumentManagerImpl();
	            schema = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(docMan.getDocument(schemaID)));
	        } 
	        catch (SUException e) {
	            throw new AGDMException(AGDMException.AGDM002, "Time schema with id " + schemaID + " not valid.");
	        }
	        catch (DMException e) {
	            throw new AGDMException(AGDMException.AGDM003, "Time schema with id " + schemaID + " does not exist.");
	        }
	        NodeList schemaParams = schema.getDocumentElement().getElementsByTagNameNS(AGDMUtils.mmsUri, "parameter");
	        for (int j = 0; j < schemaParams.getLength(); j++) {
	        	Element param = (Element) schemaParams.item(j).cloneNode(true);
	        	param.getFirstChild().setTextContent(param.getFirstChild().getTextContent() + "_" + regionDiscPolicies.get(i).getRegionId());
	        	params.add(param);
	        }
		}
    	return params;
    }
    
    /**
     * Create the region information for fields using the problem.
     * @param problem	Problem data
     * @throws AGDMException 
     */
    public void createRegionInformations(Node problem) throws AGDMException {
      	for (int i = 0; i < regionDiscPolicies.size(); i++) {
    		regionDiscPolicies.get(i).createRegionInformation(problem);
      	}
    }
    
    /**
     * Creates the structure to hold information of which variable template corresponds for a field in each step.
     */
    public void createStepFieldTemplate() {
    	stepFieldTemplates = new HashMap<Integer, HashMap<String, Element>>();
    	
		for (PDERegionDiscPolicy rdp: regionDiscPolicies) {
			ArrayList<Float> factors = rdp.getTimestepFactors();
			ArrayList<String> fields = rdp.getRegionInfo().getFields();
			int globalIndex = 0;
			for (int i = 0; i < factors.size(); i++) {
				Float factor = factors.get(i);
				Element template = null;
    			//The initial template is the previous time common template, as first RHS is evaluated for variable at t = 0 
    			if (i == 0) {
    				Element math = AGDMUtils.createElement(rdp.getSchemaDoc(), AGDMUtils.mtUri, "math");
    				math.appendChild(AGDMUtils.createPreviousTimeSlice(rdp.getSchemaDoc()));
    				template = math;
    			}
    			else {
    				template = rdp.getTimestepOutputVariableTemplates().get(i - 1);
    			}
				//Check if there will be empty steps and add previous variable template
    			if (factors.size() < longestTimestepFactors.size()) {
        			while (longestTimestepFactors.get(globalIndex) < factor) {
        				//Add one template for every field
        				HashMap<String, Element> fieldTemplates = stepFieldTemplates.get(globalIndex);
        				if (fieldTemplates == null) {
            				fieldTemplates = new HashMap<String, Element>();
            			}
        				for (String field: fields) {
        					fieldTemplates.putIfAbsent(field, template);
        				}
        				stepFieldTemplates.put(globalIndex, fieldTemplates);
        				globalIndex++;
        			}
            	}
    			//Add one template for every field
    			HashMap<String, Element> fieldTemplates = stepFieldTemplates.get(globalIndex);
    			if (fieldTemplates == null) {
    				fieldTemplates = new HashMap<String, Element>();
    			}
				for (String field: fields) {
					fieldTemplates.putIfAbsent(field, template);
					stepFieldTemplates.put(globalIndex, fieldTemplates);
				}
    			globalIndex++;
			}
    		//Check if more substeps must be added in the end
			if (factors.size() < longestTimestepFactors.size()) {
        		if (globalIndex < longestTimestepFactors.size()) {
        			Element template = rdp.getTimestepOutputVariableTemplates().get(factors.size() - 1);
            		for (int j = globalIndex; j < longestTimestepFactors.size(); j++) {
            			HashMap<String, Element> fieldTemplates = stepFieldTemplates.get(j);
            			if (fieldTemplates == null) {
            				fieldTemplates = new HashMap<String, Element>();
            			}
        				for (String field: fields) {
        					fieldTemplates.putIfAbsent(field, template);
        				}
        				stepFieldTemplates.put(j, fieldTemplates);
            		}
        		}
        	}
    	}
    }
}
