/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.AGDM.utils.PDE;

import java.util.ArrayList;
import java.util.Optional;

import org.w3c.dom.Node;

import eu.iac3.mathMS.AGDM.AGDMException;
import eu.iac3.mathMS.AGDM.utils.AGDMUtils;

/**
 * The information needed to process an equation.
 * @author bminyano
 *
 */
public class Derivative {
    private String id;
    private String field;
    private ArrayList<String> coordinates;
    private Node algebraicExpression;
    private Node derivativeTerm; 
    private SpatialOperatorDiscretization spatialOperatorDiscretization;
    private boolean inner;	//If the derivative is a inner derivative
    private boolean simple;		//If the derivative term is a simple field
    private Node conditional;
    private OperatorInfoType operatorType;
    
    static short DIFFERENT = -1;
    static short EQUIVALENT = 0;
    static short CONDITIONAL = 1;
    
    /**
     * Constructor.
     */
    public Derivative() {
        algebraicExpression = null;
        derivativeTerm = null;
        spatialOperatorDiscretization = null;
        inner = false;
        simple = false;
        conditional = null;
    }

    /**
     * Copy constructor.
     * @param d     Object to copy
     */
    public Derivative(Derivative d) {
        id = d.id;
        field = d.field;
        if (d.coordinates != null) {
            coordinates = new ArrayList<String>();
            for (String c: d.coordinates) {
            	coordinates.add(c);
            }
        }
        if (d.algebraicExpression != null) {
            algebraicExpression = d.algebraicExpression.cloneNode(true);
        }
        if (d.derivativeTerm != null) {
            derivativeTerm = d.derivativeTerm.cloneNode(true);
        }
        if (d.conditional != null) {
        	conditional = d.conditional.cloneNode(true);
        }
        if (d.spatialOperatorDiscretization != null) {
            spatialOperatorDiscretization = new SpatialOperatorDiscretization(d.getSpatialOperatorDiscretization());
        }
        inner = d.inner;
        operatorType = d.operatorType;
        simple = d.simple;
    }
    
    public OperatorInfoType getOperatorType() {
        return operatorType;
    }

    /**
     * Sets the operator type.
     * @param opType    The operator type
     */
    public void setOperatorType(String opType) {
        if (opType.equals("evolutionEquation")) {
            this.operatorType = OperatorInfoType.field;
        }
        if (opType.equals("auxiliaryField")) {
            this.operatorType = OperatorInfoType.auxiliaryField;
        }
        if (opType.equals("analysisFieldEquation")) {
            this.operatorType = OperatorInfoType.analysisField;
        }
        if (opType.equals("auxiliaryVariableEquation") || opType.equals("auxiliaryAnalysisVariableEquation")) {
            this.operatorType = OperatorInfoType.auxiliaryVariable;
        }
        if (opType.equals("projection")) {
            this.operatorType = OperatorInfoType.interaction;
        }
    }


    public boolean isInner() {
        return inner;
    }

    public void setInner(boolean inner) {
        this.inner = inner;
    }

    public ArrayList<String> getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(ArrayList<String> coordinates) {
        this.coordinates = coordinates;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public Node getAlgebraicExpression() {
        return algebraicExpression;
    }

    public void setAlgebraicExpression(Node algebraicTerm) {
        this.algebraicExpression = algebraicTerm;
    }

    public Optional<Node> getDerivativeTerm() {
        return Optional.ofNullable(derivativeTerm);
    }

    public void setDerivativeTerm(Node derivativeTerm) {
        this.derivativeTerm = derivativeTerm;
    }

    public SpatialOperatorDiscretization getSpatialOperatorDiscretization() {
        return spatialOperatorDiscretization;
    }

    public void setSpatialOperatorDiscretization(SpatialOperatorDiscretization spatialOperatorDiscretization) {
        this.spatialOperatorDiscretization = new SpatialOperatorDiscretization(spatialOperatorDiscretization);
    }
    

	public Node getConditional() {
		if (conditional == null) {
			return null;
		}
		return conditional.cloneNode(true);
	}

	public void setConditional(Node conditional) {
		this.conditional = conditional;
	}

	public boolean isSimple() {
		return simple;
	}

	public void setSimple(boolean simple) {
		this.simple = simple;
	}

	@Override
    public String toString() {
        Optional<Node> op = Optional.ofNullable(algebraicExpression);
        String algebraic = op.map( a -> {
			try {
				return AGDMUtils.domToString(a);
			} 
			catch (AGDMException e) {
				return "empty";
			}
		}).orElse("empty");
        op = Optional.ofNullable(derivativeTerm);
        String derivative = op.map( a -> {
			try {
				return AGDMUtils.domToString(a);
			} 
			catch (AGDMException e) {
				return "empty";
			}
		}).orElse("empty");
        op = Optional.ofNullable(conditional);
        String condition = op.map( a -> {
			try {
				return AGDMUtils.domToString(a);
			} 
			catch (AGDMException e) {
				return "empty";
			}
		}).orElse("empty");
        return "Derivative [id=" + id + ", field=" + field + ", coordinates="
                + coordinates + ", algebraicExpression=" + algebraic
                + ", derivativeTerm=" + derivative
                + ", operatorType=" + operatorType
                + ", conditional=" + condition
                + ", spatialOperatorDiscretization="
                + spatialOperatorDiscretization + ", composed=" + inner + ", simple=" + simple
                + "]\n";
    }
    
    /**
     * Maps problem coordinates to indices.
     */
    public void mapCoordinatesToIndices() {
        if (spatialOperatorDiscretization != null) {
            spatialOperatorDiscretization.mapCoordinatesToIndices(coordinates);
            spatialOperatorDiscretization.inferSchemaLimits();
        }
    }
    
    /**
     * Checks if two objects are equivalent taking care of conditional.
     * @param d     The object to compare to
     * @return      DIFFERENT are not equivalent at all
     * 				EQUIVALENT are completely equivalent
     * 				CONDITIONAL are equivalent, but only one of them has a conditional
     */
    public int getEquivalence(Derivative d) {
        if ((!d.getDerivativeTerm().isPresent() && this.derivativeTerm != null) || (d.getDerivativeTerm().isPresent() && this.derivativeTerm == null)) {
            return DIFFERENT;
        }
        if ((d.getAlgebraicExpression() == null && this.algebraicExpression != null) 
                || (d.getAlgebraicExpression() != null && this.algebraicExpression == null)) {
            return DIFFERENT;
        }
        if ((d.getSpatialOperatorDiscretization() == null && this.spatialOperatorDiscretization != null) 
                || (d.getSpatialOperatorDiscretization() != null && this.spatialOperatorDiscretization == null)) {
            return DIFFERENT;
        }
        if ((d.getCoordinates() == null && this.coordinates != null) || (d.getCoordinates() != null && this.coordinates == null)) {
            return DIFFERENT;
        }
        if (((d.getCoordinates() == null && this.coordinates == null) || d.getCoordinates().equals(this.coordinates)) 
            && ((!d.getDerivativeTerm().isPresent() && this.derivativeTerm == null) || d.derivativeTerm.isEqualNode(this.derivativeTerm)) 
            && ((d.getAlgebraicExpression() == null && this.algebraicExpression == null) 
                    || d.getAlgebraicExpression().isEqualNode(this.algebraicExpression))
            && ((d.getSpatialOperatorDiscretization() == null && this.spatialOperatorDiscretization == null) 
                    || d.getSpatialOperatorDiscretization().equals(this.spatialOperatorDiscretization))) {
        	if ((this.conditional != null && d.getConditional() == null) || (this.conditional == null && d.getConditional() != null)) {
        		return CONDITIONAL;
        	}
        	return EQUIVALENT;
        }
        return DIFFERENT;
    }
   
}
