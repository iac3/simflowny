/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.AGDM.utils.PDE;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Optional;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import eu.iac3.mathMS.AGDM.AGDMException;

public class PDEAnalysisDiscPolicy extends AbstractPDEDiscPolicy {

    private HashMap<String, OperatorPolicy> operatorPolicies;
    
    /**
     * Constructor.
     * @param adp  The analysis discretization policy xml
     * @throws AGDMException 
     */
    public PDEAnalysisDiscPolicy(Node adp) throws AGDMException {
        //Operator discretizations
        NodeList opDiscs = adp.getChildNodes();
        operatorDiscretizations = new ArrayList<OperatorDiscretization>();
        for (int i = 0; i < opDiscs.getLength() - 1; i++) {
            OperatorDiscretization od = new OperatorDiscretization();
            od.setOperator(opDiscs.item(i).getFirstChild().getTextContent());
            od.setSchema(opDiscs.item(i).getLastChild().getLastChild().getTextContent());
            operatorDiscretizations.add(od);
        }

        //Load all the operator policies
        operatorPolicies = super.loadOperatorPolicies();
    }
    
    /**
     * Returns the spatial operator discretization compatible with the operator.
     * @param order             The order of the solver
     * @param coordinates       The coordinate combination
     * @param operatorName      The name of the operator to solve
     * @return                  The discretization if exists, otherwise returns null
     */
    @Override
    Optional<SpatialOperatorDiscretization> getCompatiblePolicy(int order, String coordinates, String operatorName) {
        for (int i = 0; i < operatorDiscretizations.size(); i++) {
            OperatorDiscretization operatorDiscretization = operatorDiscretizations.get(i);
            if (operatorDiscretization.getOperator().equals(operatorName)) {
                String opDiscId = operatorDiscretization.getSchema();
                ArrayList<SpatialOperatorDiscretization> spatialDiscs = operatorPolicies.get(opDiscId).getDiscretization();
                for (int j = 0; j < spatialDiscs.size(); j++) {
                    SpatialOperatorDiscretization spatialDisc = spatialDiscs.get(j);
                    if (order == spatialDisc.getOrder() && coordinates.equals(spatialDisc.getCoordCombination())) {
                        return Optional.of(spatialDisc);
                    }
                }
            }
        }
        return Optional.empty();
    }

    
    public HashMap<String, OperatorPolicy> getOperatorPolicies() {
        return operatorPolicies;
    }
}
