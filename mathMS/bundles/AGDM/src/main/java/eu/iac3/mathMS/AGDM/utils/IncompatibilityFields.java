/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.AGDM.utils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;


/**
 * Structure to store the incompatibilities of a region.
 * @author bminano
 */
public class IncompatibilityFields {
    static final String NL = System.getProperty("line.separator");
    static final String IND = "\u0009";
    //Fields that the region must interpolate
    private LinkedHashMap<String, ArrayList<String>> regionFields;
    //Fields that the boundary for the region must interpolate
    private LinkedHashMap<String, ArrayList<String>> boundaryFields;
    
    /**
     * Constructor.
     * @param segFields     The incompatible fields from other regions
     * @param boundFields   The incompatible fields for the boundaries of the region
     */
    public IncompatibilityFields(LinkedHashMap<String, ArrayList<String>> segFields, LinkedHashMap<String, ArrayList<String>> boundFields) {
        regionFields = new LinkedHashMap<String, ArrayList<String>>();
        regionFields.putAll(segFields);
        boundaryFields = new LinkedHashMap<String, ArrayList<String>>();
        boundaryFields.putAll(boundFields);
    }
    
    public LinkedHashMap<String, ArrayList<String>> getRegionFields() {
        return regionFields;
    }
    public void setRegionFields(LinkedHashMap<String, ArrayList<String>> regionFields) {
        this.regionFields = regionFields;
    }

    public LinkedHashMap<String, ArrayList<String>> getBoundaryFields() {
        return boundaryFields;
    }

    public void setBoundaryFields(LinkedHashMap<String, ArrayList<String>> boundaryFields) {
        this.boundaryFields = boundaryFields;
    }
    
    /**
     * Returns the information of the class.
     * @return  The class in a string format.
     */
    public String toString() {
        String result = "Region incompatible fields: " + NL;
        Iterator<String> regionFieldsIt = regionFields.keySet().iterator();
        if (!regionFieldsIt.hasNext()) {
            result = result + IND + "No field incompatibility." + NL;
        }
        while (regionFieldsIt.hasNext()) {
            String field = regionFieldsIt.next();
            result = result + IND + field + ": ";
            for (int i = 0; i < regionFields.get(field).size(); i++) {
                result = result + regionFields.get(field).get(i) + ", ";
            }
            if (regionFields.get(field).size() > 0) {
                result = result.substring(0, result.lastIndexOf(",")) + NL;
            }
            else {
                result = result + "None." + NL;
            }
        }

        result = result + "Boundary incompatible fields:" + NL;
        Iterator<String> boundaryFieldsIt = boundaryFields.keySet().iterator();
        if (!boundaryFieldsIt.hasNext()) {
            result = result + IND + "No boundary incompatibility." + NL;
        }
        while (boundaryFieldsIt.hasNext()) {
            String bound = boundaryFieldsIt.next();
            result = result + IND + bound + ": ";
            for (int i = 0; i < boundaryFields.get(bound).size(); i++) {
                result = result + boundaryFields.get(bound).get(i) + ", ";
            }
            if (boundaryFields.get(bound).size() > 0) {
                result = result.substring(0, result.lastIndexOf(",")) + NL;
            }
            else {
                result = result + "None." + NL;
            }
        }
        
        return result;
    }
}
