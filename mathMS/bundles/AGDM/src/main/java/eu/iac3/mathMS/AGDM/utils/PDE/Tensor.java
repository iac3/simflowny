package eu.iac3.mathMS.AGDM.utils.PDE;

import java.util.ArrayList;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Tensor class.
 * @author bminano
 *
 */
public class Tensor {
    private String tensorName;
    private int order;
    private ArrayList<String> fields;
    
    /**
     * Constructor.
     * @param tensorElement     The tensor XML DOM element.
     */
    public Tensor(Node tensorElement) {
        fields = new ArrayList<String>();
        tensorName = tensorElement.getFirstChild().getTextContent();
        order = Integer.parseInt(tensorElement.getFirstChild().getNextSibling().getTextContent());
        NodeList fieldList = tensorElement.getLastChild().getChildNodes();
        for (int i = 0; i < fieldList.getLength(); i++) {
            fields.add(fieldList.item(i).getTextContent());
        }
    }
    
    /**
     * Create a tensor from another.
     * @param t     The tensor
     */
    public Tensor(Tensor t) {
        tensorName = t.tensorName;
        order = t.order;
        fields = new ArrayList<String>();
        fields.addAll(t.fields);
    }
    
    public String getTensorName() {
        return tensorName;
    }
    public int getOrder() {
        return order;
    }
    public ArrayList<String> getFields() {
        return fields;
    }
}
