/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.AGDM.utils;

import java.util.ArrayList;
import java.util.HashMap;

import org.w3c.dom.Element;

import eu.iac3.mathMS.AGDM.AGDMException;
import eu.iac3.mathMS.AGDM.processors.ExecutionFlow;

public class ParticleNormalization {
	private String schemaId;
	private String schemaName;
	private boolean spatialDependent;
	private ArrayList<NodeWrap> inputVariables;
	private HashMap<String, String> suffixMap;
	
	/**
	 * Constructor.
	 * @param id				Schema id
	 * @param inputVars			Schema input variables
	 * @throws AGDMException	AGDM00X - External error
	 */
	public ParticleNormalization(String id, String name, ArrayList<Element> inputVars) throws AGDMException {
		schemaId = id;
		schemaName = name;
		inputVariables = new ArrayList<NodeWrap>();
		for (Element i : inputVars) {
			inputVariables.add(new NodeWrap(i));
		}
		spatialDependent = isSpatialDependentCalculation();
		suffixMap = new HashMap<String, String>();
	}
	
	/**
	 * Constructor.
	 * @param id				Schema id
	 * @param inputVars			Schema input variables
	 * @throws AGDMException	AGDM00X - External error
	 */
	public ParticleNormalization(String id, ArrayList<Element> inputVars) throws AGDMException {
		schemaId = id;
		inputVariables = new ArrayList<NodeWrap>();
		for (Element i : inputVars) {
			inputVariables.add(new NodeWrap(i));
		}
		spatialDependent = isSpatialDependentCalculation();
		suffixMap = new HashMap<String, String>();
	}
	
	/**
	 * Calculates from the input variables if the normalization is spatial dependent.
	 * @return						If is spatial dependent
	 * @throws AGDMException		AGDM00X - External error
	 */
	private boolean isSpatialDependentCalculation() throws AGDMException {
		for (NodeWrap n : inputVariables) {
			if (AGDMUtils.find(n.getNode(), ExecutionFlow.SPATIAL_IT_TAGS + "|" 
					+ ExecutionFlow.FIRST_SPATIAL_ORDER_TAGS).getLength() > 0) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Gets the suffix from a given one.
	 * @param suffix	Suffix given.
	 * @return			Suffix from map.
	 */
	public String getSuffixValue(String suffix) {
		return suffixMap.get(suffix);
	}

	public String getSchemaId() {
		return schemaId;
	}

	public String getSchemaName() {
		return schemaName;
	}

	public HashMap<String, String> getSuffixMap() {
		return suffixMap;
	}

	/**
	 * Adds a suffix to the map.
	 * @param suffix	Suffix
	 * @param value		Value
	 */
	public void addSuffix(String suffix, String value) {
		this.suffixMap.put(suffix, value);
	}

	public boolean isSpatialDependent() {
		return spatialDependent;
	}

	public ArrayList<NodeWrap> getInputVariables() {
		return inputVariables;
	}
	
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((inputVariables == null) ? 0 : inputVariables.hashCode());
		result = prime * result + ((schemaId == null) ? 0 : schemaId.hashCode());
		result = prime * result + (spatialDependent ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ParticleNormalization other = (ParticleNormalization) obj;
		if (inputVariables == null) {
			if (other.inputVariables != null)
				return false;
		} 
		else 
			if (!inputVariables.equals(other.inputVariables))
			return false;
		if (schemaId == null) {
			if (other.schemaId != null)
				return false;
		} 
		else 
			if (!schemaId.equals(other.schemaId))
			return false;	
		if (spatialDependent != other.spatialDependent)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ParticleNormalization [schemaId=" + schemaId + ", schemaName=" + schemaName + ", spatialDependent=" + spatialDependent
				+ ", suffixMap=" + suffixMap + "]";
	}
	
}
