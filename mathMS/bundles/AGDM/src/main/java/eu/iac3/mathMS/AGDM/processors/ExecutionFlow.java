/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.AGDM.processors;

import eu.iac3.mathMS.AGDM.AGDMException;
import eu.iac3.mathMS.AGDM.utils.AGDMUtils;
import eu.iac3.mathMS.AGDM.utils.DiscretizationType;
import eu.iac3.mathMS.AGDM.utils.ImportCall;
import eu.iac3.mathMS.AGDM.utils.NodeWrap;
import eu.iac3.mathMS.AGDM.utils.RuleInfo;
import eu.iac3.mathMS.AGDM.utils.PDE.OperatorInfoType;
import eu.iac3.mathMS.AGDM.utils.PDE.OperatorsInfo;
import eu.iac3.mathMS.AGDM.utils.PDE.ProcessInfo;
import eu.iac3.mathMS.AGDM.utils.RegionInfo;
import eu.iac3.mathMS.documentManagerPlugin.DMException;
import eu.iac3.mathMS.documentManagerPlugin.DocumentManager;
import eu.iac3.mathMS.documentManagerPlugin.DocumentManagerImpl;
import eu.iac3.mathMS.simflownyUtilsPlugin.SUException;
import eu.iac3.mathMS.simflownyUtilsPlugin.SimflownyUtils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
/**
 * This class have all the necessary methods to process the executionFlow of a discretization schema.
 * 
 * @author bminyano
 *
 */
public class ExecutionFlow {
    static SimflownyUtils su;
    static String mmsUri = "urn:mathms";
    static String smlUri = "urn:simml";
    static String mtUri = "http://www.w3.org/1998/Math/MathML";
    //Tags that requires spatial iteration
    public static final String SPATIAL_IT_TAGS = ".//sml:spatialCoordinate|.//sml:incrementCoordinate|.//sml:decrementCoordinate" 
        + "|.//sml:contSpatialCoordinate|.//sml:maxCharSpeedCoordinate|.//sml:positiveCharSpeedCoordinate|.//sml:negativeCharSpeedCoordinate|.//sml:particlePosition|.//sml:particleVelocity";
    public static final String CHARSPEEDS_TAGS = ".//sml:maxCharSpeed|.//sml:maxCharSpeedCoordinate|.//sml:positiveCharSpeed|.//sml:positiveCharSpeedCoordinate|.//sml:negativeCharSpeed|.//sml:negativeCharSpeedCoordinate";
    //Tags that refers to fields
    static final String FIELD_TAGS = ".//sml:field|.//sml:eigenVector";
    //Tags that second order spatial iteration
    public static final String FIRST_SPATIAL_ORDER_TAGS = ".//sml:spatialCoordinate|.//sml:incrementCoordinate|.//sml:decrementCoordinate" 
        + "|.//sml:contSpatialCoordinate|.//sml:particlePosition|.//sml:particleVelocity";
    static final String SECOND_SPATIAL_ORDER_TAGS = ".//sml:secondSpatialCoordinate|.//sml:incrementCoordinate1IncrementCoordinate2"
        + "|.//sml:decrementCoordinate1IncrementCoordinate2|.//sml:incrementCoordinate1DecrementCoordinate2"
        + "|.//sml:decrementCoordinate1DecrementCoordinate2|.//sml:secondContSpatialCoordinate|.//sml:incrementCoordinate2|.//sml:decrementCoordinate2";
    
    static {
    	
    }
    
    /**
     * The constructor.
     * @throws AGDMException 
     */
    ExecutionFlow() throws AGDMException {
    }
    /**
     * Process a execution flow to resolve all the possible tags and generate the flow of the problem.
     * 
     * @param originalDocs      The problem and discretization schema
     * @param problem           The document with the info
     * @param fieldRel          Relation between coordinates
     * @param pi                The process information
     * @return                  The document fragment resulting
     * @throws AGDMException    AGDM002 XML discretization schema not valid
     *                          AGDM00X External error
     */
    public static DocumentFragment processExecutionFlow(ArrayList<Document> originalDocs, Node problem, RegionInfo fieldRel, ProcessInfo pi) 
        throws AGDMException {
        Document originalProblem = originalDocs.get(0);
        Document schema = originalDocs.get(1);
        
        //The result are stored in a document fragment
        DocumentFragment result = problem.getOwnerDocument().createDocumentFragment();
        
        boolean extrapolation = false;
        if (pi.getExtrapolationMethod() != null && pi.getIncomFields() != null) {
            extrapolation = true;
        }
        //Process every child
        NodeList flowChildren = schema.getElementsByTagNameNS(smlUri, "simml").item(0).getChildNodes();
        for (int i = 0; i < flowChildren.getLength(); i++) {
            Element rule = (Element) flowChildren.item(i);
            RuleInfo ri = new RuleInfo();
            ri.setIndividualField(false);
            ri.setIndividualCoord(false);
            ri.setFunction(false);
            ri.setProcessInfo(pi);        
            ri.setAuxiliaryCDAdded(false);
            ri.setAuxiliaryFluxesAdded(false);
            ri.setUseBaseCoordinates(false);
            ri.setExtrapolation(extrapolation);
            ruleConsistencyCheck(rule);
            result.appendChild(problem.getOwnerDocument().importNode(processExecutionFlowChild(originalProblem, problem, rule, ri), true));
        }
        return result;
    }
    
    /**
     * Process Simml to resolve all the possible tags and generate the instructions of the problem.
     * @param schema			Discretization schema
     * @param originalProblem	The original problem
     * @param problem			The problem in construction
     * @param pi				The process information
     * @param individualDisc	When the discretization is made for only one field
     * @return					Instructions
     * @throws AGDMException	AGDM002 XML discretization schema not valid
     *                          AGDM00X External error
     */
    public static DocumentFragment processSimml(Element schema, Document originalProblem, Node problem, ProcessInfo pi, boolean individualDisc) 
            throws AGDMException {
    	
            //The result are stored in a document fragment
            DocumentFragment result = problem.getOwnerDocument().createDocumentFragment();
            
            boolean extrapolation = false;
            if (pi.getExtrapolationMethod() != null && pi.getIncomFields() != null) {
                extrapolation = true;
            }
            //Process every child
            NodeList flowChildren;
            if (schema.getLocalName().equals("simml")) {
            	flowChildren = schema.getChildNodes();
            }
            else {
            	flowChildren = schema.getElementsByTagNameNS(smlUri, "simml").item(0).getChildNodes();
            }
            for (int i = 0; i < flowChildren.getLength(); i++) {
                Element rule = (Element) flowChildren.item(i);
                RuleInfo ri = new RuleInfo();
                ri.setIndividualField(individualDisc);
                ri.setIndividualCoord(individualDisc);
                ri.setFunction(false);
                ri.setProcessInfo(pi);
                ri.setAuxiliaryCDAdded(false);
                ri.setAuxiliaryFluxesAdded(false);
                ri.setUseBaseCoordinates(false);
                ri.setExtrapolation(extrapolation);
                result.appendChild(problem.getOwnerDocument().importNode(processExecutionFlowChild(originalProblem, problem, rule, ri), true));
            }
            return result;
        }
    
    /**
     * Process an element of a transformation rule.
     * @param originalProblem       The problem with the original information
     * @param problem               The problem with the discretization
     * @param rule                  The element to process
     * @param ri                    The rule information
     * @return                      The element and sub-elements processed
     * @throws AGDMException        AGDM002 XML discretization schema not valid
     *                              AGDM006  Schema not applicable to this problem
     *                              AGDM007 External error
     */
    public static DocumentFragment processExecutionFlowChild(Document originalProblem, Node problem, Element rule, RuleInfo ri) throws AGDMException {
        Document doc = rule.getOwnerDocument();
        DocumentFragment fragmentResult = doc.createDocumentFragment();
        //if
        if (rule.getLocalName().equals("if")) {
        	//Check Auxiliary definitions in condition
        	checkCDAuxDefs(fragmentResult, originalProblem, problem, rule.getFirstChild(), ri);
            fragmentResult.appendChild(processIf(originalProblem, problem, (Element) rule.cloneNode(true), ri));
            return fragmentResult;
        }
        //while
        if (rule.getLocalName().equals("while")) {
        	//Check Auxiliary definitions in condition
        	checkCDAuxDefs(fragmentResult, originalProblem, problem, rule.getFirstChild(), ri);
            fragmentResult.appendChild(processWhile(originalProblem, problem, (Element) rule.cloneNode(true), ri));
            return fragmentResult;
        }
        //Import
        if (rule.getLocalName().equals("import")) {
            fragmentResult.appendChild(processImportTag(originalProblem, problem, rule, ri));
            return fragmentResult;
        }
        //Math content
        if (rule.getLocalName().equals("math")) {
            fragmentResult.appendChild(processMath(originalProblem, problem, rule, ri));
            return fragmentResult;
        }
        //Return
        if (rule.getLocalName().equals("return")) {
        	if (rule.hasChildNodes()) {
        		rule.replaceChild(processMath(originalProblem, problem, (Element) rule.getFirstChild(), ri), rule.getFirstChild());
        	}
            fragmentResult.appendChild(rule);
            return fragmentResult;
        }
        //regionInteraction
        if (rule.getLocalName().equals("regionInteraction")) {
            fragmentResult.appendChild(RegionInteraction.processRegionInteraction(originalProblem, problem, rule, ri, 0, null, null, null));
            return fragmentResult;
        }
        //diagonalization
        if (rule.getLocalName().equals("diagonalize")) {
            fragmentResult.appendChild(processDiagonalization(originalProblem, problem, rule, ri));
            return fragmentResult;
        }
        //undiagonalization
        if (rule.getLocalName().equals("undiagonalize")) {
            fragmentResult.appendChild(processUndiagonalization(originalProblem, problem, rule, ri));
            return fragmentResult;
        }
        //auxiliaryEquations tag
        if (rule.getLocalName().equals("auxiliaryEquations")) {
            fragmentResult.appendChild(doc.importNode(AuxiliaryEquations
                .processAuxiliaryEquations(originalProblem, problem, (Element) rule.getFirstChild(), ri.getProcessInfo()), true));
            return fragmentResult;
        }
        //fieldFromAuxiliaryEquations tag
        if (rule.getLocalName().equals("fieldFromAuxiliaryEquations")) {
            fragmentResult.appendChild(doc.importNode(AuxiliaryEquations
                .processFieldRecoveries(originalProblem, problem, (Element) rule.getFirstChild(), ri), true));
            return fragmentResult;
        }
        //checkFinalization tag
        if (rule.getLocalName().equals("checkFinalization")) {
            fragmentResult.appendChild(doc.importNode(
                    processFinalizationCheck(originalProblem, problem, (Element) rule.getFirstChild(), ri.getProcessInfo()), true));
            return fragmentResult;
        }
        //timeStep tag
        if (rule.getLocalName().equals("iterateOverCells")) {
        	fragmentResult.appendChild(processIterateOverCells(originalProblem, problem, rule, ri));
        	if (ri.getProcessInfo().getDiscretizationType() == DiscretizationType.particles) {
        		Element iterOverParticles = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "iterateOverParticles");
        		iterOverParticles.appendChild(fragmentResult);
        		fragmentResult = doc.createDocumentFragment();
        		fragmentResult.appendChild(iterOverParticles);
        	}
            return fragmentResult;
        }
        //fieldGroup tag
        if (rule.getLocalName().equals("fieldGroup")) {
            //Create a new region relation only with the fields of the group
            String groupName = rule.getAttribute("groupNameAtt");
            RuleInfo newRi = new RuleInfo(ri);
            ProcessInfo pi = new ProcessInfo(newRi.getProcessInfo());
            RegionInfo newSr = new RegionInfo(pi.getRegionInfo());
            newSr.filterGroup(groupName);
            pi.setRegionInfo(newSr);
            newRi.setProcessInfo(pi);
            fragmentResult.appendChild(processContext(originalProblem, problem, rule, newRi));
            return fragmentResult;
        }
        //omnidirectionalContext tag
        if (rule.getLocalName().equals("omnidirectionalContext")) {
            //Create a new region relation with the fields but the special field
            RuleInfo newRi = new RuleInfo(ri);
            newRi.setUseBaseCoordinates(true);
            fragmentResult.appendChild(processContext(originalProblem, problem, rule, newRi));
            return fragmentResult;
        }
        //fieldLoopContext tag
        if (rule.getLocalName().equals("fieldLoopContext")) {
        	ArrayList<String> fields = ri.getProcessInfo().getRegionInfo().getFields();
        	for (int i = 0; i < fields.size(); i++) {
        		ArrayList<String> fieldFiltered = new ArrayList<String>();
        		fieldFiltered.add(fields.get(i));
                //Create a new region relation only with the fields of the group
                RuleInfo newRi = new RuleInfo(ri);
                ProcessInfo pi = new ProcessInfo(newRi.getProcessInfo());
                RegionInfo newSr = new RegionInfo(pi.getRegionInfo());
                newSr.filterField(fields.get(i));
                pi.setRegionInfo(newSr);
                newRi.setProcessInfo(pi);
                fragmentResult.appendChild(processContext(originalProblem, problem, rule, newRi));
        	}

            return fragmentResult;
        }
        //coordinateLoopContext tag
        if (rule.getLocalName().equals("coordinateLoopContext")) {
        	ArrayList<String> coordinates = ri.getProcessInfo().getBaseSpatialCoordinates();
        	for (int i = 0; i < coordinates.size(); i++) {
                //Create a new region relation only with the fields of the group
                RuleInfo newRi = new RuleInfo(ri);
                ProcessInfo pi = new ProcessInfo(newRi.getProcessInfo());
                pi.setSpatialCoord1(coordinates.get(i));
                newRi.setIndividualCoord(true);
                newRi.setProcessInfo(pi);
                fragmentResult.appendChild(processContext(originalProblem, problem, rule, newRi));
        	}

            return fragmentResult;
        }
        //iterateOverParticles tag
        if (rule.getLocalName().equals("iterateOverParticles")) {
            RuleInfo newRi = new RuleInfo(ri);
            ProcessInfo pi = new ProcessInfo(newRi.getProcessInfo());
            pi.setDiscretizationType(DiscretizationType.particles);
            //Specific species
            if (rule.hasAttribute("speciesNameAtt")) {
            	String species = rule.getAttribute("speciesNameAtt");
                Element iterateOverParticles = (Element) rule.cloneNode(false);
                pi.setCurrentSpecies(species);
                newRi.setProcessInfo(pi);
                iterateOverParticles.setAttribute("speciesNameAtt", species);
                iterateOverParticles.appendChild(processContext(originalProblem, problem, rule, newRi));
                fragmentResult.appendChild(iterateOverParticles);
            }
            //All species
            else {
	            for (String speciesName: pi.getParticleSpecies().keySet()) {
	                Element iterateOverParticles = (Element) rule.cloneNode(false);
	                pi.setCurrentSpecies(speciesName);
	                newRi.setProcessInfo(pi);
	                iterateOverParticles.setAttribute("speciesNameAtt", speciesName);
	                iterateOverParticles.appendChild(processContext(originalProblem, problem, rule, newRi));
	                fragmentResult.appendChild(iterateOverParticles);
	            }
            }
            return fragmentResult;
        }
        //iterateOverInteractions tag
        if (rule.getLocalName().equals("iterateOverInteractions")) {
            fragmentResult.appendChild(processIterateOverInteractions(originalProblem, problem, rule, ri));
            return fragmentResult;
        }
        //boundary tag
        if (rule.getLocalName().equals("boundary")) {
            fragmentResult.appendChild(doc.importNode(Boundary.processBoundary(originalProblem, problem, 
                    rule, ri.getProcessInfo(), isLastBoundary(rule), false, true, true), true));
            return fragmentResult;
        }
        //move particles declaration
        if (rule.getLocalName().equals("moveParticles")) {
            fragmentResult.appendChild(doc.importNode(processMoveParticles(originalProblem, rule, ri), true));
            return fragmentResult;
        }
        //throw error declaration
        if (rule.getLocalName().equals("throwError")) {
            fragmentResult.appendChild(doc.importNode(rule.cloneNode(true), true));
            return fragmentResult;
        }
        //comment declaration
        if (rule.getLocalName().equals("comment")) {
            fragmentResult.appendChild(doc.importNode(rule.cloneNode(true), true));
            return fragmentResult;
        }
        //interiorDomainContext
        if (rule.getLocalName().equals("interiorDomainContext")) {
            Element result = (Element) rule.cloneNode(false);
            NodeList children = rule.getChildNodes();
            for (int i = 0; i < children.getLength(); i++) {
                Element newRule = (Element) children.item(i).cloneNode(true);
                result.appendChild(processExecutionFlowChild(originalProblem, problem, newRule, ri));
            }
            if (result.hasChildNodes()) {
                fragmentResult.appendChild(result);
            }
            return fragmentResult;
        }
        //Other tags are not permitted
        throw new AGDMException(AGDMException.AGDM002, "Invalid tag " + rule.getLocalName());
    }
    
    /**
     * Checks when the characteristic decomposition auxiliary definitions are required.
     * @param fragmentResult        The fragment result.
     * @param problem               The region problem
     * @param rule                  The rule
     * @param ri                    The Rule info
     * @throws AGDMException        AGDM00X External error
     */
    private static void checkCDAuxDefs(DocumentFragment fragmentResult, Document discProblem, Node problem, Node rule, RuleInfo ri) 
        throws AGDMException {
        //Add characteristic decomposition auxiliaries if necessary
        if (!ri.getAuxiliaryCDAdded() && AGDMUtils.find(rule, CHARSPEEDS_TAGS).getLength() > 0
                && !rule.getLocalName().equals("timeStep") && !rule.getLocalName().equals("boundary")) {
            addCDAuxiliaryEquations(fragmentResult, discProblem, problem, rule, ri);
        }
    }
    
    /**
     * Checks when the flux auxiliary definitions are required.
     * @param fragmentResult        The fragment result.
     * @param problem               The region problem
     * @param rule                  The rule
     * @param ri                    The Rule info
     * @throws AGDMException        AGDM00X External error
     */
    private static void checkFluxAuxDefs(DocumentFragment fragmentResult, Document discProblem, Node problem, Element rule, RuleInfo ri) 
            throws AGDMException {
    	if (!ri.getAuxiliaryFluxesAdded()) {
            //Add characteristic decomposition auxiliaries if necessary
        	NodeList fluxes = rule.getElementsByTagName("sml:flux");
        	if (fluxes.getLength() > 0 ) {
            	//Auxiliary variables dependant from fluxes (Only the ones containing sources)
        		OperatorsInfo opInfoFluxes = new OperatorsInfo(ri.getProcessInfo().getOperatorsInfo());
        		HashSet<String> fluxVariables = new HashSet<String>();
        		for (String field: ri.getProcessInfo().getRegionInfo().getFields()) {
        			fluxVariables.addAll(opInfoFluxes.getFluxVariables(field));
        		}
            	
            	opInfoFluxes.onlyDependent(fluxVariables, true, opInfoFluxes.getFluxCommonConditional());
            	if (opInfoFluxes.hasDerivatives(OperatorInfoType.auxiliaryVariable)) {
                    throw new AGDMException(AGDMException.AGDM005, "Auxiliary variables used in fluxes cannot be defined using derivatives.");
            	}
            	ArrayList<NodeWrap> inputs = new ArrayList<NodeWrap>();
    	        for (int i = 0 ; i < fluxes.getLength(); i++) {
    	        	Element inputVariable = (Element) fluxes.item(i).getLastChild().cloneNode(true);
    	        	if (!inputs.contains(new NodeWrap(inputVariable))) {
    	        		fragmentResult.appendChild(fragmentResult.getOwnerDocument().importNode(SchemaCreator.createAuxiliaryVariableEquations(discProblem, problem, opInfoFluxes, ri.getProcessInfo().getOperatorsInfo().getDerivativeLevels(), ri.getProcessInfo().getOperatorsInfo().getMultiplicationLevels(), opInfoFluxes.getDerivativeLevels(), opInfoFluxes.getMultiplicationLevels(), ri.getProcessInfo(), inputVariable, 0, false), true));
    	        		inputs.add(new NodeWrap(inputVariable));
    	        	}
    	        }
        	}
        	ri.setAuxiliaryFluxesAdded(true);
        }
    }
    /*
    for (int i = 0; i < equation.getLength(); i++) {
        String field = ((Element) equation.item(i)).getElementsByTagName("mms:field").item(0).getTextContent();
        if (ri.getProcessInfo().getRegionInfo().getFields().contains(field)) {
    */
    /**
     * Process all the children of a context.
     * @param originalProblem       The problem with the original information
     * @param problem               The problem with the discretization
     * @param rule                  The element to process
     * @param ri                    The rule information
     * @return                      The element and sub-elements processed
     * @throws AGDMException        AGDM002 XML discretization schema not valid
     *                              AGDM006  Schema not applicable to this problem
     *                              AGDM00X External error
     */
    private static DocumentFragment processContext(Document originalProblem, Node problem, Element rule, RuleInfo ri) throws AGDMException {
        Document doc = rule.getOwnerDocument();
        DocumentFragment fragmentResult = doc.createDocumentFragment();
        NodeList children = rule.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            fragmentResult.appendChild(processExecutionFlowChild(originalProblem, problem, (Element) children.item(i), ri));
        }
        return fragmentResult;
    }
    /**
     * Process the import tag.
     * @param discProblem           The problem with the discretization
     * @param problem               The problem with the original information
     * @param rule                  The element to process
     * @param ri                    The rule information
     * @return                      The content selected processed.
     * @throws AGDMException        AGDM002 XML discretization schema not valid
     *                              AGDM007 External error
     */
    public static DocumentFragment processImportTag(Document discProblem, Node problem, Element rule, RuleInfo ri) throws AGDMException {
        Document parentSchema = rule.getOwnerDocument();
        DocumentFragment fragmentResult = parentSchema.createDocumentFragment();
        try {
            //Get the schema document to import
            DocumentManager docMan = new DocumentManagerImpl();
            String schema = SimflownyUtils.jsonToXML(docMan.getDocument(rule.getFirstChild().getTextContent()));
            if (schema == null) {
                throw new AGDMException(AGDMException.AGDM002, "The schema " + rule.getFirstChild().getTextContent() + " does not exist");
            }
            Document importDoc = AGDMUtils.stringToDom(schema);
            //Get constant information from transformation rule
            AGDMUtils.getConstants(importDoc, ri.getProcessInfo().getConstantInformation());
            //Get the name of the calling
            String functionName = null;
            //If it is a function that uses only one spatial coordinate tag there have to be created one function for each spatial coordinate
            NodeList sufix = rule.getElementsByTagName("sml:sufix");
            if (sufix.getLength() > 0 && ((Element) sufix.item(0)).getElementsByTagName("sml:spatialCoordinate").getLength() > 0
                    && ((Element) sufix.item(0)).getElementsByTagName("sml:secondSpatialCoordinate").getLength() == 0) {
                ArrayList<String> coords = ri.getProcessInfo().getCoordinateRelation().getContCoords();
                for (int i = 0; i < coords.size(); i++) {
                    ProcessInfo pi = new ProcessInfo(ri.getProcessInfo());
           
                    pi.setSpatialCoord1(coords.get(i));
                    pi.setSpatialCoord2(null);
                    functionName = rule.getElementsByTagName("sml:name").item(0).getTextContent()
                            + replaceTags(rule.getElementsByTagName("sml:sufix").item(0).cloneNode(true), pi).getTextContent();
                    //Create a new instance to not overwrite the parameter information inside the processImport function
                    ProcessInfo newPi = new ProcessInfo(ri.getProcessInfo());
                    ImportCall ic = new ImportCall(importDoc, functionName);
                    newPi.setSpatialCoord1(coords.get(i));
                    newPi.setSpatialCoord2(null);
                    processImport(discProblem, problem, ic, newPi, parentSchema, true);
                }
            }
            else {
                //TODO generalizar a orden N
                if (sufix.getLength() > 0 &&  ((Element) sufix.item(0)).getElementsByTagName("sml:secondSpatialCoordinate").getLength() > 0) {
                    ArrayList<String> coords = ri.getProcessInfo().getCoordinateRelation().getContCoords();
                    for (int i = 0; i < coords.size(); i++) {
                        for (int j = 0; j < coords.size(); j++) {
                            ProcessInfo pi = new ProcessInfo(ri.getProcessInfo());
                            pi.setSpatialCoord1(coords.get(i));
                            pi.setSpatialCoord2(coords.get(j));
                            functionName = rule.getElementsByTagName("sml:name").item(0).getTextContent()
                                    + replaceTags(rule.getElementsByTagName("sml:sufix").item(0).cloneNode(true), pi).getTextContent();

                            //Create a new instance to not overwrite the parameter information inside the processImport function
                            ProcessInfo newPi = new ProcessInfo(ri.getProcessInfo());
                            ImportCall ic = new ImportCall(importDoc, functionName);
                            newPi.setSpatialCoord1(coords.get(i));
                            newPi.setSpatialCoord2(coords.get(j));
                            processImport(discProblem, problem, ic, newPi, parentSchema, true);
                        }
                    }
                }
                //Only one function has to be created.
                else {
                    functionName = rule.getElementsByTagName("sml:name").item(0).getTextContent();

                    //Create a new instance for not overwrite the parameter information inside the processImport function
                    ProcessInfo newPi = new ProcessInfo(ri.getProcessInfo());
                    ImportCall ic = new ImportCall(importDoc, functionName);
                    processImport(discProblem, problem, ic, newPi, parentSchema, true);
                }
            }
            //Return an empty document. Nothing to add to the execution flow. Already added to the function tag in the discretized problem
            return fragmentResult;
        } 
        catch (DMException e) {
            throw new AGDMException(AGDMException.AGDM00X, e.getMessage());
        }
        catch (SUException e) {
            throw new AGDMException(AGDMException.AGDM00X, e.getMessage());
        }
    }
    /**
     * Process the tag time step.
     * @param discProblem           The problem with the discretization
     * @param problem               The problem with the original information
     * @param rule                  The element to process
     * @param ri                    The rule information
     * @return                      The content selected processed.
     * @throws AGDMException        AGDM007 External error
     */
    private static DocumentFragment processIterateOverCells(Document discProblem, Node problem, Element rule, RuleInfo ri) throws AGDMException {
        //The result is stored in a document fragment
        Document doc = rule.getOwnerDocument();
        DocumentFragment result = doc.createDocumentFragment();
        Element timeStep = (Element) rule.cloneNode(false);
        result.appendChild(timeStep);
        if (AGDMUtils.find(problem, "//mms:equationSet").getLength() > 0) {
            NodeList children = rule.getChildNodes();
            for (int i = 0; i < children.getLength(); i++) {
                ri.setAuxiliaryCDAdded(false);
                ri.setAuxiliaryFluxesAdded(false);
                timeStep.appendChild(doc.importNode(processExecutionFlowChild(discProblem, problem, (Element) children.item(i), ri), true));
            }
        }
        return result;
    }
    
    /**
     * Process the summatory over the influence radius in particle schemas.
     * @param discProblem      The problem with the discretization
     * @param problem               The problem with the original information
     * @param rule                  The element to process
     * @param ri                    The rule information
     * @return                      The content selected processed.
     * @throws AGDMException        AGDM007 External error
     */
    private static DocumentFragment processIterateOverInteractions(Document discProblem, Node problem, Element rule, RuleInfo ri) 
            throws AGDMException {
        Document doc = rule.getOwnerDocument();
        DocumentFragment fragmentResult = doc.createDocumentFragment();

        Element iterateOverInteractions = (Element) rule.cloneNode(false);
        fragmentResult.appendChild(iterateOverInteractions);
        iterateOverInteractions.appendChild(rule.getFirstChild().cloneNode(false));
        NodeList children = rule.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            iterateOverInteractions.appendChild(processExecutionFlowChild(discProblem, problem, (Element) children.item(i), ri));
        }
        return fragmentResult;
    }
    /**
     * Process the tag moveParticles.
     * @param ri	                The rule information
     * @param discProblem           The problem with the discretization
     * @param rule                  The element to process
     * @return                      The content selected processed.
     * @throws AGDMException        AGDM002 XML discretization schema not valid
     */
    public static DocumentFragment processMoveParticles(Document discProblem, Element rule, RuleInfo ri) 
        throws AGDMException {
    	
        DocumentFragment result = discProblem.createDocumentFragment();
        result.appendChild(discProblem.importNode(rule.cloneNode(false), true));
              
        for (int i = 0; i < ri.getProcessInfo().getBaseSpatialCoordinates().size(); i++) {
            ProcessInfo pi = ri.getProcessInfo();
            pi.setSpatialCoord1(pi.getBaseSpatialCoordinates().get(i));
            pi.setSpatialCoord2(null);
            result.getFirstChild().appendChild(discProblem.importNode(replaceTags(
                    rule.getFirstChild().cloneNode(true), pi), true));
        }

        return result;
    }
    
    /**
     * Process a if rule.
     * @param discProblem           The problem with the discretization
     * @param problem               The problem with the original information
     * @param rule                  The element to process
     * @param ri                    The rule information
     * @return                      The content selected processed.
     * @throws AGDMException        AGDM002 XML discretization schema not valid
     *                              AGDM007 External error
     */
    private static DocumentFragment processIf(Document discProblem, Node problem, Element rule, RuleInfo ri) throws AGDMException {
        //Take the attributes of the if
        String fieldIteration = rule.getAttribute("fieldAtt");
        String coordIteration = rule.getAttribute("coordinateAtt");
        
        //Get the inherited attributes for individual iteration
        boolean individualCoord = ri.getIndividualCoord();
        boolean individualField = ri.getIndividualField();
        
        //The if has to be processed individually for every spatial coordinate and field
        if ((individualCoord && individualField) || (fieldIteration.equals("") && coordIteration.equals(""))
                || (individualCoord && fieldIteration.equals("")) || (individualField && coordIteration.equals(""))) {
            return processIfIndividually(discProblem, problem, rule, ri);
        }
        //The if has to be processed individually only for every field
        if (individualField || fieldIteration.equals("")) {
            return processIfIndividuallyField(discProblem, problem, rule, ri);
        }
        //The if has to be processed individually only for every spatial coordinate
        if (individualCoord || coordIteration.equals("")) {
            return processIfIndividuallyCoord(discProblem, problem, rule, ri);
        }
        //The if has to be processed together for every spatial coordinate and field
        return processIfForAll(discProblem, problem, rule, ri);
    }
    /**
     * Process the if when it has to be generated for every spatial coordinate and field.
     * @param discProblem           The problem with the discretization
     * @param problem               The problem with the original information
     * @param rule                  The element to process
     * @param ri                    The rule information
     * @return                      The rule processed.
     * @throws AGDMException        AGDM002 XML discretization schema not valid
     *                              AGDM007 External error
     */
    private static DocumentFragment processIfIndividually(Document discProblem, Node problem, Element rule, RuleInfo ri) throws AGDMException {
        Document doc = rule.getOwnerDocument();
        DocumentFragment result = doc.createDocumentFragment();
        ProcessInfo pi = ri.getProcessInfo();
        Element ruleIt = null;
        
        //Prepare the iterations depending on the inherited attributes and the tag ones
        String actualSpatialCoord;
        ProcessInfo actualPi;
        int coordIterations = 1;
        int fieldIterations = 1;
        ArrayList<String> coords = new ArrayList<String>();
        ArrayList<ProcessInfo> pis = new ArrayList<ProcessInfo>();
        //Look for spatial iterable tags to do the iteration
        if (!ri.getIndividualField()) {
            pis = AGDMUtils.generateProcessInfo(problem, pi, AGDMUtils.isVectorInstruction(rule));
            NodeList spatialTags = AGDMUtils.find(rule.getFirstChild(), SPATIAL_IT_TAGS);
            if (spatialTags.getLength() > 0 && !AGDMUtils.isVectorInstruction(rule)
                    && !ri.getUseBaseCoordinates()) {
                pis = AGDMUtils.getOnlyWithFlux(pis);
            }
            fieldIterations = pis.size();
        }
        //Field iteration
        for (int i = 0; i < fieldIterations; i++) {
            //Take the process info from the pi array if not inherited
            if (!ri.getIndividualField()) {
                actualPi = pis.get(i);
            }
            //Take the inherited process info
            else {
                actualPi = pi;
            }
            if (!ri.getIndividualCoord()) {
                if (AGDMUtils.isVectorInstruction(rule) || ri.getUseBaseCoordinates()) {
                    coords = actualPi.getBaseSpatialCoordinates();
                    coordIterations = coords.size();
                }
                else {
                    coords = actualPi.getFluxesCoords();
                    coordIterations = coords.size();
                }
            }
            //Spatial coordinate iteration
            for (int j = 0; j < coordIterations; j++) {
                ruleIt = (Element) rule.cloneNode(true);
                //Take the spatial coordinate from the coordinates array if not inherited
                if (!ri.getIndividualCoord()) {
                    actualSpatialCoord = coords.get(j);
                }
                //Take the inherited spatial coordinate
                else {
                    actualSpatialCoord = ri.getProcessInfo().getSpatialCoord1();
                }
                //Process condition
                Element condition = (Element) ruleIt.getFirstChild();
                if (!condition.getLocalName().equals("math")) {
                    throw new AGDMException(AGDMException.AGDM002, "Only a mathml sentence could be used as condition.");
                }
                Element then = (Element) condition.getNextSibling();
                actualPi.setSpatialCoord1(actualSpatialCoord);
                actualPi.setSpatialCoord2(ri.getProcessInfo().getSpatialCoord2());
                ruleIt.replaceChild(doc.importNode(replaceTags(condition.cloneNode(true), actualPi), true), condition);
                
                //The children of then and else have to be processed individually for fields and spatial coordinates
                RuleInfo newRi = new RuleInfo();
                newRi.setIndividualField(true);
                newRi.setIndividualCoord(true);
                newRi.setFunction(ri.getFunction());
                newRi.setProcessInfo(actualPi);
                newRi.setAuxiliaryCDAdded(ri.getAuxiliaryCDAdded());
                newRi.setAuxiliaryFluxesAdded(ri.getAuxiliaryFluxesAdded());
                newRi.setUseBaseCoordinates(ri.getUseBaseCoordinates());
                //Process then
                if (!then.getLocalName().equals("then")) {
                    String msg = "Only one conditional sencence is allowed. To use various conditions use mathml \"or\" and \"and\".";
                    throw new AGDMException(AGDMException.AGDM002, msg);
                }
                Element newThen = AGDMUtils.createElement(doc, smlUri, "then");
                NodeList thenChildren = then.getChildNodes();
                for (int k = 0; k < thenChildren.getLength(); k++) {
                    Element thenChild = (Element) thenChildren.item(k);
                    newThen.appendChild(doc.importNode(processExecutionFlowChild
                        (discProblem, problem, (Element) thenChild.cloneNode(true), newRi), true));
                }
                Element elseTag = (Element) then.getNextSibling();
                ruleIt.replaceChild(newThen, then);
                //Process else
                if (elseTag != null) {
                    Element newElse = AGDMUtils.createElement(doc, smlUri, "else");
                    NodeList elseChildren = elseTag.getChildNodes();
                    for (int k = 0; k < elseChildren.getLength(); k++) {
                        Element elseChild = (Element) elseChildren.item(k);
                        newElse.appendChild(doc.importNode(processExecutionFlowChild
                            (discProblem, problem, (Element) elseChild.cloneNode(true), newRi), true));
                    }
                    ruleIt.replaceChild(newElse, elseTag);
                }
                result.appendChild(ruleIt);
            }
        }
        return result;
    }
    /**
     * Process the if when it has to be generated for every field.
     * @param discProblem           The problem with the discretization
     * @param problem               The problem with the original information
     * @param rule                  The element to process
     * @param ri                    The rule information
     * @return                      The rule processed.
     * @throws AGDMException        AGDM002 XML discretization schema not valid
     *                              AGDM007 External error
     */
    private static DocumentFragment processIfIndividuallyField(Document discProblem, Node problem, Element rule, RuleInfo ri) throws AGDMException {
        //The result is stored in a document fragment
        Document doc = rule.getOwnerDocument();
        DocumentFragment result = doc.createDocumentFragment();
        //Take the coordinate attribute of the if and get the operation
        String coordIteration = rule.getAttribute("coordinateAtt");
        String coordOperation;
        if (coordIteration.equals("forAll")) {
            coordOperation = "and";
        }
        else {
            coordOperation = "or";
        }
        //Get the possible inherited attributes for individual iteration
        boolean function = ri.getFunction();
        boolean individualField = ri.getIndividualField();
        String spatialCoord = ri.getProcessInfo().getSpatialCoord1();
        String spatialCoord2 = ri.getProcessInfo().getSpatialCoord2();
        ProcessInfo pi = ri.getProcessInfo();
        
        //The original rule element cannot be modified, so use this one 
        Element ruleIt;
    
        //Prepare the iterations depending on the inherited attributes and the tag ones
        ProcessInfo actualPi;
        int fieldIterations = 1;
        ArrayList<ProcessInfo> pis = new ArrayList<ProcessInfo>();
        if (!individualField) {
            pis = AGDMUtils.generateProcessInfo(problem, pi, AGDMUtils.isVectorInstruction(rule));
            NodeList spatialTags = AGDMUtils.find(rule.getFirstChild(), SPATIAL_IT_TAGS);
            if (spatialTags.getLength() > 0 && !AGDMUtils.isVectorInstruction(rule) 
                    && !ri.getUseBaseCoordinates()) {
                pis = AGDMUtils.getOnlyWithFlux(pis);
            }
            fieldIterations = pis.size();
        }
        //Field iteration
        for (int i = 0; i < fieldIterations; i++) {
            ruleIt = (Element) rule.cloneNode(true);
            //Take the process info from the pi array if not inherited
            if (!individualField) {
                actualPi = pis.get(i);
            }
            //Take the inherited process info
            else {
                actualPi = pi;
            }
        
            //Process condition
            Element condition = (Element) ruleIt.getFirstChild();
            Element then = (Element) condition.getNextSibling();
            actualPi.setSpatialCoord1(spatialCoord);
            actualPi.setSpatialCoord2(spatialCoord2);
            ruleIt.replaceChild(doc.importNode(processForAllCoordinates(condition, actualPi, function, coordOperation, 
                    AGDMUtils.isVectorInstruction(rule)), true), condition);
            //The children of then and else have to be processed individually for fields
            RuleInfo newRi = new RuleInfo();
            newRi.setIndividualField(true);
            newRi.setIndividualCoord(false);
            newRi.setFunction(function);
            newRi.setProcessInfo(actualPi);
            newRi.setAuxiliaryCDAdded(ri.getAuxiliaryCDAdded());
            newRi.setAuxiliaryFluxesAdded(ri.getAuxiliaryFluxesAdded());
            newRi.setUseBaseCoordinates(ri.getUseBaseCoordinates());
            //Process then
            if (!then.getLocalName().equals("then")) {
                String msg = "Only one conditional sencence is allowed. To use various conditions use mathml \"or\" and \"and\".";
                throw new AGDMException(AGDMException.AGDM002, msg);
            }
            Element newThen = AGDMUtils.createElement(doc, smlUri, "then");
            NodeList thenChildren = then.getChildNodes();
            for (int j = 0; j < thenChildren.getLength(); j++) {
                Element thenChild = (Element) thenChildren.item(j);
                newThen.appendChild(doc.importNode(processExecutionFlowChild
                    (discProblem, problem, (Element) thenChild.cloneNode(true), newRi), true));
            }
            Element elseTag = (Element) then.getNextSibling();
            ruleIt.replaceChild(newThen, then);
            //Process else
            if (elseTag != null) {
                Element newElse = AGDMUtils.createElement(doc, smlUri, "else");
                NodeList elseChildren = elseTag.getChildNodes();
                for (int j = 0; j < elseChildren.getLength(); j++) {
                    Element elseChild = (Element) elseChildren.item(j);
                    newElse.appendChild(doc.importNode(processExecutionFlowChild
                        (discProblem, problem, (Element) elseChild.cloneNode(true), newRi), true));
                }
                ruleIt.replaceChild(newElse, elseTag);
            }
            result.appendChild(ruleIt);
        }
        return result;
    }
    /**
     * Process the if when it has to be generated for every spatial coordinate.
     * @param discProblem           The problem with the discretization
     * @param problem               The problem with the original information
     * @param rule                  The element to process
     * @param ri                    The rule information
     * @return                      The rule processed.
     * @throws AGDMException        AGDM002 XML discretization schema not valid
     *                              AGDM007 External error
     */
    private static DocumentFragment processIfIndividuallyCoord(Document discProblem, Node problem, Element rule, RuleInfo ri) throws AGDMException {
        //The result is stored in a document fragment
        Document doc = rule.getOwnerDocument();
        DocumentFragment result = doc.createDocumentFragment();
        
        //Take the field attribute of the if and get the operation
        String fieldIteration = rule.getAttribute("fieldAtt");
        String fieldOperation;
        if (fieldIteration.equals("forAll")) {
            fieldOperation = "and";
        }
        else {
            fieldOperation = "or";
        }
        
        //Get the possible inherited attributes for individual iteration
        boolean function = ri.getFunction();
        boolean individualCoord = ri.getIndividualCoord();
        String spatialCoord = ri.getProcessInfo().getSpatialCoord1();
        ProcessInfo pi = ri.getProcessInfo();
        
        //The original rule element cannot be modified, so use this one 
        Element ruleIt = null;

        //Prepare the iterations depending on the inherited attributes and the tag ones
        String actualSpatialCoord;
        int coordIterations = 1;
        ArrayList<String> coords = new ArrayList<String>();
        if (!individualCoord) {
            if (AGDMUtils.isVectorInstruction(rule)) {
                coords = pi.getBaseSpatialCoordinates();
                coordIterations = coords.size();
            }
            else {
                NodeList fieldTags = AGDMUtils.find(rule.cloneNode(true).getFirstChild(), FIELD_TAGS);
                //Any field tag exist in the condition
                if (fieldTags.getLength() > 0) {
                    coords = pi.getFluxesCoords();
                    coordIterations = coords.size();
                }
                else {
                    coords = pi.getBaseSpatialCoordinates();
                    coordIterations = coords.size(); 
                }
            }
        }
        //Coord iteration
        for (int i = 0; i < coordIterations; i++) {
            ruleIt = (Element) rule.cloneNode(true);
            //Take the spatial coordinate from the coordinates array if not inherited
            if (!individualCoord) {
                actualSpatialCoord = coords.get(i);
            }
            //Take the inherited spatial coordinate
            else {
                actualSpatialCoord = spatialCoord;
            }
        
            //Process condition
            Element condition = (Element) ruleIt.getFirstChild();
            Element then = (Element) condition.getNextSibling();
            if (!condition.getLocalName().equals("math")) {
                throw new AGDMException(AGDMException.AGDM002, "Only a mathml sentence could be used as condition.");
            }
            //Look for field tags to know if it is necessary to iterate over the fields
            NodeList fieldTags = AGDMUtils.find(condition, FIELD_TAGS);
            //Any field tag exist in the condition
            if (fieldTags.getLength() > 0) {
                //Iterate the conditions for every field
                ArrayList<ProcessInfo> pis = AGDMUtils.generateProcessInfo(problem, pi, AGDMUtils.isVectorInstruction(rule));
                //There is only one field to iterate
                if (pis.size() == 1) {
                    ProcessInfo newPi = pis.get(0);
                    newPi.setSpatialCoord1(actualSpatialCoord);
                    ruleIt.replaceChild(doc.importNode(replaceTags(condition.cloneNode(true), newPi), true), condition);
                }
                //There is more than one field to iterate
                else {
                    ProcessInfo newPi = pis.get(0);
                    newPi.setSpatialCoord1(actualSpatialCoord);
                    Element cond = (Element) doc.importNode(
                            replaceTags(condition.cloneNode(true), newPi), true);
                    for (int j = 1; j < pis.size(); j++) {
                        newPi = pis.get(j);
                        newPi.setSpatialCoord1(actualSpatialCoord);
                        Element fieldCond = (Element) doc.importNode(replaceTags(condition.cloneNode(true), newPi), true);
                        cond = AGDMUtils.combineConditions(cond, fieldCond, fieldOperation);
                    }
                    ruleIt.replaceChild(cond, condition);
                }
            }
            //No field exists in the conditions. So it is not necessary to iterate over fields  
            else {
                ProcessInfo newPi = new ProcessInfo(pi);
                newPi.setSpatialCoord1(actualSpatialCoord);
                ruleIt.replaceChild(doc.importNode(replaceTags(condition.cloneNode(true), newPi), true), condition);
            }
            //The children of then and else have to be processed individually for spatial coordinates
            RuleInfo newRi = new RuleInfo();
            newRi.setIndividualField(false);
            newRi.setIndividualCoord(true);
            newRi.setFunction(function);
            pi.setSpatialCoord1(actualSpatialCoord);
            newRi.setProcessInfo(pi);
            newRi.setAuxiliaryCDAdded(ri.getAuxiliaryCDAdded());
            newRi.setAuxiliaryFluxesAdded(ri.getAuxiliaryFluxesAdded());
            newRi.setUseBaseCoordinates(ri.getUseBaseCoordinates());
            //Process then
            if (!then.getLocalName().equals("then")) {
                String msg = "Only one conditional sencence is allowed. To use various conditions use mathml \"or\" and \"and\".";
                throw new AGDMException(AGDMException.AGDM002, msg);
            }
            Element newThen = AGDMUtils.createElement(doc, smlUri, "then");
            NodeList thenChildren = then.getChildNodes();
            for (int j = 0; j < thenChildren.getLength(); j++) {
                Element thenChild = (Element) thenChildren.item(j);
                newThen.appendChild(doc.importNode(processExecutionFlowChild
                    (discProblem, problem, (Element) thenChild.cloneNode(true), newRi), true));
            }
            Element elseTag = (Element) then.getNextSibling();
            ruleIt.replaceChild(newThen, then);
            //Process else
            if (elseTag != null) {
                Element newElse = AGDMUtils.createElement(doc, smlUri, "else");
                NodeList elseChildren = elseTag.getChildNodes();
                for (int j = 0; j < elseChildren.getLength(); j++) {
                    Element elseChild = (Element) elseChildren.item(j);
                    newElse.appendChild(doc.importNode(processExecutionFlowChild
                        (discProblem, problem, (Element) elseChild.cloneNode(true), newRi), true));
                }
                ruleIt.replaceChild(newElse, elseTag);
            }
            result.appendChild(ruleIt);
        }
        return result;
    }
    /**
     * Process the if when it has to be generated together for every spatial coordinate and field.
     * @param discProblem             The problem with the discretization
     * @param problem                 The problem with the original information
     * @param rule                  The element to process
     * @param ri                    The rule information
     * @return                      The rule processed.
     * @throws AGDMException        AGDM002 XML discretization schema not valid
     *                              AGDM007 External error
     */
    private static DocumentFragment processIfForAll(Document discProblem, Node problem, Element rule, RuleInfo ri) throws AGDMException {
        //The result is stored in a document fragment
        Document doc = rule.getOwnerDocument();
        DocumentFragment result = doc.createDocumentFragment();
        //Take the field and coordinate attributes of the if and get the operations
        String fieldIteration = rule.getAttribute("fieldAtt");
        String coordIteration = rule.getAttribute("coordinateAtt");
        String fieldOperation;
        if (fieldIteration.equals("forAll")) {
            fieldOperation = "and";
        }
        else {
            fieldOperation = "or";
        }
        String coordOperation;
        if (coordIteration.equals("forAll")) {
            coordOperation = "and";
        }
        else {
            coordOperation = "or";
        }
        
        //Get the possible inherited attributes for individual iteration
        boolean function = ri.getFunction();
        ProcessInfo pi = ri.getProcessInfo();
        
        //Process condition
        Element condition = (Element) rule.getFirstChild();
        if (!condition.getLocalName().equals("math")) {
            throw new AGDMException(AGDMException.AGDM002, "Only a mathml sentence could be used as condition.");
        }
        Element then = (Element) condition.getNextSibling();
        //Look for field tags to know if it is necessary to iterate over the fields
        NodeList fieldTags = AGDMUtils.find(condition, FIELD_TAGS);
        //Look for spatial iterable tags to do the iteration
        //Any field tag exist in the condition
        if (fieldTags.getLength() > 0) {
            NodeList spatialTags = AGDMUtils.find(rule.getFirstChild(), SPATIAL_IT_TAGS);
            
            //Iterate the conditions for every field
            ArrayList<ProcessInfo> pis = AGDMUtils.generateProcessInfo(problem, pi, AGDMUtils.isVectorInstruction(rule));
            if (spatialTags.getLength() > 0 && !AGDMUtils.isVectorInstruction(rule)) {
                pis = AGDMUtils.getOnlyWithFlux(pis);
            }
            //There is only one field to iterate
            if (pis.size() == 1) {
                rule.replaceChild(doc.importNode(processForAllCoordinates(condition, pis.get(0), function, coordOperation, 
                        AGDMUtils.isVectorInstruction(rule)), true), condition);
            }
            //There is more than one field to iterate
            else {
                Element cond = (Element) doc.importNode(processForAllCoordinates(condition, pis.get(0), function, coordOperation, 
                        AGDMUtils.isVectorInstruction(rule)), true);
                for (int j = 1; j < pis.size(); j++) {
                    Element fieldCond = (Element) doc.importNode(processForAllCoordinates(condition, pis.get(j), function, coordOperation, 
                            AGDMUtils.isVectorInstruction(rule)), true);
                    cond = AGDMUtils.combineConditions(cond, fieldCond, fieldOperation);
                }
                rule.replaceChild(cond, condition);
            }
        }
        //No field exists in the conditions. So it is not necessary to iterate over fields  
        else {
            rule.replaceChild(doc.importNode(processForAllCoordinates(condition, pi, function, coordOperation, 
                    AGDMUtils.isVectorInstruction(rule)), true), condition);
        }
        //The children of then and else have to be processed together for spatial coordinates and fields
        RuleInfo newRi = new RuleInfo();
        newRi.setIndividualField(false);
        newRi.setIndividualCoord(false);
        newRi.setFunction(function);
        newRi.setProcessInfo(pi);
        newRi.setAuxiliaryCDAdded(ri.getAuxiliaryCDAdded());
        newRi.setAuxiliaryFluxesAdded(ri.getAuxiliaryFluxesAdded());
        newRi.setUseBaseCoordinates(ri.getUseBaseCoordinates());
        //Process then
        if (!then.getLocalName().equals("then")) {
            String msg = "Only one conditional sencence is allowed. To use various conditions use mathml \"or\" and \"and\".";
            throw new AGDMException(AGDMException.AGDM002, msg);
        }
        Element newThen = AGDMUtils.createElement(doc, smlUri, "then");
        NodeList thenChildren = then.getChildNodes();
        for (int j = 0; j < thenChildren.getLength(); j++) {
            Element thenChild = (Element) thenChildren.item(j);
            newThen.appendChild(doc.importNode(processExecutionFlowChild
                (discProblem, problem, (Element) thenChild.cloneNode(true), newRi), true));
        }
        Element elseTag = (Element) then.getNextSibling();
        rule.replaceChild(newThen, then);
        //Process else
        if (elseTag != null) {
            Element newElse = AGDMUtils.createElement(doc, smlUri, "else");
            NodeList elseChildren = elseTag.getChildNodes();
            for (int j = 0; j < elseChildren.getLength(); j++) {
                Element elseChild = (Element) elseChildren.item(j);
                newElse.appendChild(doc.importNode(processExecutionFlowChild
                    (discProblem, problem, (Element) elseChild.cloneNode(true), newRi), true));
            }
            rule.replaceChild(newElse, elseTag);
        }
        result.appendChild(rule);
        return result;
    }
    /**
     * Process a while rule.
     * @param discProblem           The problem with the discretization
     * @param problem               The problem with the original information
     * @param rule                  The element to process
     * @param ri                    The rule information
     * @return                      The rule processed.
     * @throws AGDMException        AGDM002 XML discretization schema not valid
     *                              AGDM007 External error
     */
    private static DocumentFragment processWhile(Document discProblem, Node problem, Element rule, RuleInfo ri) throws AGDMException {
        //Take the attributes of the if
        String fieldIteration = rule.getAttribute("fieldAtt");
        String coordIteration = rule.getAttribute("coordinateAtt");
        
        //Get the inherited attributes for individual iteration
        boolean individualCoord = ri.getIndividualCoord();
        boolean individualField = ri.getIndividualField();
        
        //The if has to be processed individually for every spatial coordinate and field
        if ((individualCoord && individualField) || (fieldIteration.equals("") && coordIteration.equals(""))
                || (individualCoord && fieldIteration.equals("")) || (individualField && coordIteration.equals(""))) {
            return processWhileIndividually(discProblem, problem, rule, ri);
        }
        
        //The if has to be processed individually only for every field
        if (individualField || fieldIteration.equals("")) {
            return processWhileIndividuallyField(discProblem, problem, rule, ri);
        }
        
        //The if has to be processed individually only for every spatial coordinate
        if (individualCoord || coordIteration.equals("")) {
            return processWhileIndividuallyCoord(discProblem, problem, rule, ri);
        }
        
        //The if has to be processed together for every spatial coordinate and field
        return processWhileForAll(discProblem, problem, rule, ri);
    }
    /**
     * Process a while when it has to be generated for every spatial coordinate and field.
     * @param discProblem           The problem with the discretization
     * @param problem               The problem with the original information
     * @param rule                  The element to process
     * @param ri                    The rule information
     * @return                      The content selected processed.
     * @throws AGDMException        AGDM002 XML discretization schema not valid
     *                              AGDM007 External error
     */
    private static DocumentFragment processWhileIndividually(Document discProblem, Node problem, Element rule, RuleInfo ri) throws AGDMException {
        //The result is stored in a document fragment
        Document doc = rule.getOwnerDocument();
        DocumentFragment result = doc.createDocumentFragment();
        
        //Get the possible inherited attributes for individual iteration
        boolean function = ri.getFunction();
        boolean individualCoord = ri.getIndividualCoord();
        boolean individualField = ri.getIndividualField();
        String spatialCoord = ri.getProcessInfo().getSpatialCoord1();
        ProcessInfo pi = ri.getProcessInfo();
       
        //The original rule element cannot be modified, so use this one 
        Element ruleIt = null;
        
        //Prepare the iterations depending on the inherited attributes and the tag ones
        String actualSpatialCoord;
        ProcessInfo actualPi;
        int coordIterations = 1;
        int fieldIterations = 1;
        ArrayList<String> coords = new ArrayList<String>();
        ArrayList<ProcessInfo> pis = new ArrayList<ProcessInfo>();
        if (!individualCoord) {
            if (AGDMUtils.isVectorInstruction(rule) || ri.getUseBaseCoordinates()) {
                coords = pi.getBaseSpatialCoordinates();
                coordIterations = coords.size();
            }
            else {
                coords = pi.getFluxesCoords();
                coordIterations = coords.size();
            }
        }
        if (!individualField) {
            pis = AGDMUtils.generateProcessInfo(problem, pi, AGDMUtils.isVectorInstruction(rule));
            fieldIterations = pis.size();
        }
        //Field iteration
        for (int i = 0; i < fieldIterations; i++) {
            ruleIt = (Element) rule.cloneNode(true);
            //Take the process info from the pi array if not inherited
            if (!individualField) {
                actualPi = pis.get(i);
            }
            //Take the inherited process info
            else {
                actualPi = pi;
            }
            //Spatial coordinate iteration
            for (int j = 0; j < coordIterations; j++) {
                //Take the spatial coordinate from the coordinates array if not inherited
                if (!individualCoord) {
                    actualSpatialCoord = coords.get(j);
                }
                //Take the inherited spatial coordinate
                else {
                    actualSpatialCoord = spatialCoord;
                }
                actualPi.setSpatialCoord1(actualSpatialCoord);
                
                //Process condition
                Element condition = (Element) ruleIt.getFirstChild();
                if (!condition.getLocalName().equals("math")) {
                    throw new AGDMException(AGDMException.AGDM002, "Only a mathml sentence could be used as condition.");
                }
                Element loop = (Element) condition.getNextSibling();

                ruleIt.replaceChild(doc.importNode(replaceTags(condition.cloneNode(true), actualPi), true), condition);
                //Process loop
                if (!loop.getLocalName().equals("loop")) {
                    String msg = "Only one conditional sencence is allowed. To use various conditions use mathml \"or\" and \"and\".";
                    throw new AGDMException(AGDMException.AGDM002, msg);
                }
                //The children of loop have to be processed individually for fields and spatial coordinates
                RuleInfo newRi = new RuleInfo();
                newRi.setIndividualField(true);
                newRi.setIndividualCoord(true);
                newRi.setFunction(function);
                newRi.setProcessInfo(actualPi);
                newRi.setAuxiliaryCDAdded(ri.getAuxiliaryCDAdded());
                newRi.setAuxiliaryFluxesAdded(ri.getAuxiliaryFluxesAdded());
                newRi.setUseBaseCoordinates(ri.getUseBaseCoordinates());
                Element newLoop = AGDMUtils.createElement(doc, smlUri, "loop");
                NodeList loopChildren = loop.getChildNodes();
                for (int k = 0; k < loopChildren.getLength(); k++) {
                    Element loopChild = (Element) loopChildren.item(k);
                    newLoop.appendChild(doc.importNode(
                            processExecutionFlowChild(discProblem, problem, (Element) loopChild.cloneNode(true), newRi), true));
                }
                ruleIt.replaceChild(newLoop, loop);
                result.appendChild(ruleIt);
            }
        }
        return result;
    } 
    /**
     * Process a while when it has to be generated for every field.
     * @param discProblem           The problem with the discretization
     * @param problem               The problem with the original information
     * @param rule                  The element to process
     * @param ri                    The rule information
     * @return                      The rule processed.
     * @throws AGDMException        AGDM002 XML discretization schema not valid
     *                              AGDM007 External error
     */
    private static DocumentFragment processWhileIndividuallyField(Document discProblem, Node problem, Element rule, RuleInfo ri) 
            throws AGDMException {
        //The result is stored in a document fragment
        Document doc = rule.getOwnerDocument();
        DocumentFragment result = doc.createDocumentFragment();
        
        //Take the coordinate attribute of the if
        String coordIteration = rule.getAttribute("coordinateAtt");
        String coordOperation;
        if (coordIteration.equals("forAll")) {
            coordOperation = "and";
        }
        else {
            coordOperation = "or";
        }
        
        //Get the possible inherited attributes for individual iteration
        boolean function = ri.getFunction();
        boolean individualField = ri.getIndividualField();
        String spatialCoord = ri.getProcessInfo().getSpatialCoord1();
        ProcessInfo pi = ri.getProcessInfo();
        
        //The original rule element cannot be modified, so use this one 
        Element ruleIt = null;
    
        //Prepare the iterations depending on the inherited attributes and the tag ones
        ProcessInfo actualPi;
        int fieldIterations = 1;
        ArrayList<ProcessInfo> pis = new ArrayList<ProcessInfo>();
        if (!individualField) {
            pis = AGDMUtils.generateProcessInfo(problem, pi, AGDMUtils.isVectorInstruction(rule));
            NodeList spatialTags = AGDMUtils.find(rule.getFirstChild(), SPATIAL_IT_TAGS);
            if (spatialTags.getLength() > 0 && !AGDMUtils.isVectorInstruction(rule) 
                    && !ri.getUseBaseCoordinates()) {
                pis = AGDMUtils.getOnlyWithFlux(pis);
            }
            fieldIterations = pis.size();
        }
        //Field iteration
        for (int i = 0; i < fieldIterations; i++) {
            ruleIt = (Element) rule.cloneNode(true);
            //Take the process info from the pi array if not inherited
            if (!individualField) {
                actualPi = pis.get(i);
            }
            //Take the inherited process info
            else {
                actualPi = pi;
            }
            actualPi.setSpatialCoord1(spatialCoord);
            
            //Process condition
            Element condition = (Element) ruleIt.getFirstChild();
            Element loop = (Element) condition.getNextSibling();
            ruleIt.replaceChild(doc.importNode(processForAllCoordinates(condition, actualPi, function, coordOperation, 
                    AGDMUtils.isVectorInstruction(rule)), true), condition);
            //Process loop
            if (!loop.getLocalName().equals("loop")) {
                String msg = "Only one conditional sencence is allowed. To use various conditions use mathml \"or\" and \"and\".";
                throw new AGDMException(AGDMException.AGDM002, msg);
            }
            //The children of loop have to be processed individually for fields
            RuleInfo newRi = new RuleInfo();
            newRi.setIndividualField(true);
            newRi.setIndividualCoord(false);
            newRi.setFunction(function);
            newRi.setProcessInfo(actualPi);
            newRi.setAuxiliaryCDAdded(ri.getAuxiliaryCDAdded());
            newRi.setAuxiliaryFluxesAdded(ri.getAuxiliaryFluxesAdded());
            newRi.setUseBaseCoordinates(ri.getUseBaseCoordinates());
            Element newLoop = AGDMUtils.createElement(doc, smlUri, "loop");
            NodeList loopChildren = loop.getChildNodes();
            for (int k = 0; k < loopChildren.getLength(); k++) {
                Element loopChild = (Element) loopChildren.item(k);
                newLoop.appendChild(doc.importNode(
                        processExecutionFlowChild(discProblem, problem, (Element) loopChild.cloneNode(true), newRi), true));
            }
            ruleIt.replaceChild(newLoop, loop);
            result.appendChild(ruleIt);
        }
        return result;
    }
    /**
     * Process a while when it has to be generated for every spatial coordinate.
     * @param discProblem           The problem with the discretization
     * @param problem               The problem with the original information
     * @param rule                  The element to process
     * @param ri                    The rule information
     * @return                      The rule processed.
     * @throws AGDMException        AGDM002 XML discretization schema not valid
     *                              AGDM007 External error
     */
    private static DocumentFragment processWhileIndividuallyCoord(Document discProblem, Node problem, Element rule, RuleInfo ri) 
            throws AGDMException {
        //The result is stored in a document fragment
        Document doc = rule.getOwnerDocument();
        DocumentFragment result = doc.createDocumentFragment();
        
        //Take the field attribute of the if
        String fieldIteration = rule.getAttribute("fieldAtt");
        String fieldOperation;
        if (fieldIteration.equals("forAll")) {
            fieldOperation = "and";
        }
        else {
            fieldOperation = "or";
        }
        
        //Get the possible inherited attributes for individual iteration
        boolean function = ri.getFunction();
        boolean individualCoord = ri.getIndividualCoord();
        String spatialCoord = ri.getProcessInfo().getSpatialCoord1();
        ProcessInfo pi = ri.getProcessInfo();
        
        //Prepare the iterations depending on the inherited attributes and the tag ones
        String actualSpatialCoord;
        int coordIterations = 1;
        ArrayList<String> coords = new ArrayList<String>();
        if (!individualCoord) {
            if (AGDMUtils.isVectorInstruction(rule)) {
                coords = pi.getBaseSpatialCoordinates();
                coordIterations = coords.size();
            }
            else {
                coords = pi.getFluxesCoords();
                coordIterations = coords.size();
            }
        }
        
        //The original rule element cannot be modified, so use this one 
        Element ruleIt = null;
        //Field iteration
        for (int i = 0; i < coordIterations; i++) {
            ruleIt = (Element) rule.cloneNode(true);
            //Take the spatial coordinate from the coordinates array if not inherited
            if (!individualCoord) {
                actualSpatialCoord = coords.get(i);
            }
            //Take the inherited spatial coordinate
            else {
                actualSpatialCoord = spatialCoord;
            }
        
            //Process condition
            Element condition = (Element) ruleIt.getFirstChild();
            Element loop = (Element) condition.getNextSibling();
            if (!condition.getLocalName().equals("math")) {
                throw new AGDMException(AGDMException.AGDM002, "Only a mathml sentence could be used as condition.");
            }
            //Look for field tags to know if it is necessary to iterate over the fields
            NodeList fieldTags = AGDMUtils.find(condition, FIELD_TAGS);
            //Any field tag exist in the condition
            if (fieldTags.getLength() > 0) {
                //Iterate the conditions for every field
                ArrayList<ProcessInfo> pis = AGDMUtils.generateProcessInfo(problem, pi, AGDMUtils.isVectorInstruction(rule));
                //There is only one field to iterate
                if (pis.size() == 1) {
                    ProcessInfo newPi = pis.get(0);
                    newPi.setSpatialCoord1(actualSpatialCoord);
                    ruleIt.replaceChild(doc.importNode(replaceTags(condition.cloneNode(true), newPi), true), condition);
                }
                //There is more than one field to iterate
                else {
                    ProcessInfo newPi = pis.get(0);
                    newPi.setSpatialCoord1(actualSpatialCoord);
                    Element cond = (Element) doc.importNode(replaceTags(condition.cloneNode(true), newPi), true);
                    for (int j = 1; j < pis.size(); j++) {
                        newPi = pis.get(j);
                        newPi.setSpatialCoord1(actualSpatialCoord);
                        Element fieldCond = (Element) doc.importNode(replaceTags(condition.cloneNode(true), newPi), true);
                        cond = AGDMUtils.combineConditions(cond, fieldCond, fieldOperation);
                    }
                    ruleIt.replaceChild(cond, condition);
                }
            }
            //No field exists in the conditions. So it is not necessary to iterate over fields  
            else {
                pi.setSpatialCoord1(actualSpatialCoord);
                ruleIt.replaceChild(doc.importNode(replaceTags(condition.cloneNode(true), pi), true), condition);
            }
            //Process loop
            if (!loop.getLocalName().equals("loop")) {
                String msg = "Only one conditional sencence is allowed. To use various conditions use mathml \"or\" and \"and\".";
                throw new AGDMException(AGDMException.AGDM002, msg);
            }
            //The children of loop have to be processed individually for spatial coordinates
            RuleInfo newRi = new RuleInfo();
            newRi.setIndividualField(false);
            newRi.setIndividualCoord(true);
            newRi.setFunction(function);
            pi.setSpatialCoord1(actualSpatialCoord);
            newRi.setProcessInfo(pi);
            newRi.setAuxiliaryCDAdded(ri.getAuxiliaryCDAdded());
            newRi.setAuxiliaryFluxesAdded(ri.getAuxiliaryFluxesAdded());
            newRi.setUseBaseCoordinates(ri.getUseBaseCoordinates());
            Element newLoop = AGDMUtils.createElement(doc, smlUri, "loop");
            NodeList loopChildren = loop.getChildNodes();
            for (int k = 0; k < loopChildren.getLength(); k++) {
                Element loopChild = (Element) loopChildren.item(k);
                newLoop.appendChild(doc.importNode(
                        processExecutionFlowChild(discProblem, problem, (Element) loopChild.cloneNode(true), newRi), true));
            }
            ruleIt.replaceChild(newLoop, loop);
            result.appendChild(ruleIt);
        }
        return result;
    }
    /**
     * Process a while when it has to be generated together for every spatial coordinate and field.
     * @param discProblem           The problem with the discretization
     * @param problem               The problem with the original information
     * @param rule                  The element to process
     * @param ri                    The rule information
     * @return                      The rule processed.
     * @throws AGDMException        AGDM002 XML discretization schema not valid
     *                              AGDM007 External error
     */
    private static DocumentFragment processWhileForAll(Document discProblem, Node problem, Element rule, RuleInfo ri) throws AGDMException {
        //The result is stored in a document fragment
        Document doc = rule.getOwnerDocument();
        DocumentFragment result = doc.createDocumentFragment();
        
        //Take the attributes of the if
        String fieldIteration = rule.getAttribute("fieldAtt");
        String coordIteration = rule.getAttribute("coordinateAtt");
        String fieldOperation;
        if (fieldIteration.equals("forAll")) {
            fieldOperation = "and";
        }
        else {
            fieldOperation = "or";
        }
        String coordOperation;
        if (coordIteration.equals("forAll")) {
            coordOperation = "and";
        }
        else {
            coordOperation = "or";
        }
        
        //Get the inherited attributes for individual iteration
        boolean function = ri.getFunction();
        ProcessInfo pi = ri.getProcessInfo();
        
        //Process condition
        Element condition = (Element) rule.getFirstChild();
        if (!condition.getLocalName().equals("math")) {
            throw new AGDMException(AGDMException.AGDM002, "Only a mathml sentence could be used as condition.");
        }
        Element loop = (Element) condition.getNextSibling();
        //Look for field tags to know if it is necessary to iterate over the fields 
        NodeList fieldTags = AGDMUtils.find(condition, FIELD_TAGS);
        //Any field tag exist in the condition
        if (fieldTags.getLength() > 0) {
            //Iterate the conditions for every field
            ArrayList<ProcessInfo> pis = AGDMUtils.generateProcessInfo(problem, pi, AGDMUtils.isVectorInstruction(rule));
            //There is only one field to iterate
            if (pis.size() == 1) {
                rule.replaceChild(doc.importNode(processForAllCoordinates(condition, pis.get(0), function, coordOperation, 
                        AGDMUtils.isVectorInstruction(rule)), true), condition);
            }
            //There is more than one field to iterate
            else {
                Element cond = (Element) doc.importNode(processForAllCoordinates(condition, pis.get(0), function, coordOperation, 
                        AGDMUtils.isVectorInstruction(rule)), true);
                for (int j = 1; j < pis.size(); j++) {
                    Element fieldCond = (Element) doc.importNode(processForAllCoordinates(condition, pis.get(j), 
                            function, coordOperation, AGDMUtils.isVectorInstruction(rule)), true);
                    cond = AGDMUtils.combineConditions(cond, fieldCond, fieldOperation);
                }
                rule.replaceChild(cond, condition);
            }
        }
        //No field exists in the conditions. So it is not necessary to iterate over fields  
        else {
            rule.replaceChild(doc.importNode(processForAllCoordinates(condition, pi, function, coordOperation, 
                    AGDMUtils.isVectorInstruction(rule)), true), condition);
        }
        //Process loop
        if (!loop.getLocalName().equals("loop")) {
            String msg = "Only one conditional sencence is allowed. To use various conditions use mathml \"or\" and \"and\".";
            throw new AGDMException(AGDMException.AGDM002, msg);
        }
        //The children of loop have to be processed together for spatial coordinates and fields
        RuleInfo newRi = new RuleInfo();
        newRi.setIndividualField(false);
        newRi.setIndividualCoord(false);
        newRi.setFunction(function);
        newRi.setProcessInfo(pi);
        newRi.setAuxiliaryCDAdded(ri.getAuxiliaryCDAdded());
        newRi.setAuxiliaryFluxesAdded(ri.getAuxiliaryFluxesAdded());
        newRi.setUseBaseCoordinates(ri.getUseBaseCoordinates());
        Element newLoop = AGDMUtils.createElement(doc, smlUri, "loop");
        NodeList loopChildren = loop.getChildNodes();
        for (int k = 0; k < loopChildren.getLength(); k++) {
            Element loopChild = (Element) loopChildren.item(k);
            newLoop.appendChild(doc.importNode(
                    processExecutionFlowChild(discProblem, problem, (Element) loopChild.cloneNode(true), newRi), true));
        }
        rule.replaceChild(newLoop, loop);
        result.appendChild(rule);
        return result;
    } 
    /**
     * Process a condition mathml for a procesInfo.
     * Combine all the possible spatial tags.
     * @param condition         The condition original element    
     * @param pi                The process information
     * @param function          If true the condition is inside a function
     * @param operation         The operation to perform ("and" or "or")
     * @param isVector          If the instruction includes the eigenVector tag
     * @return                  The condition processed
     * @throws AGDMException    AGDM002 XML discretization schema not valid
     *                          AGDM007 External error
     */
    private static Element processForAllCoordinates(Element condition, ProcessInfo pi, Boolean function, String operation, boolean isVector) 
        throws AGDMException {
        if (!condition.getLocalName().equals("math")) {
            throw new AGDMException(AGDMException.AGDM002, "Only a mathml sentence could be used as condition.");
        }
        //Look for spatial iterable tags to do the iteration
        NodeList spatialIteration = AGDMUtils.find(condition, SPATIAL_IT_TAGS);
        if (spatialIteration.getLength() > 0) {
            ArrayList<String> coords;
            //If there is a function only one coordinate has to be used
            if (function) {
                coords = new ArrayList<String>();
                coords.add(pi.getSpatialCoord1());
            }
            else {
                //If fvm then the fluxes coordinates have to be used, if particles the base spatial coordinates
                if (pi.getDiscretizationType() == DiscretizationType.mesh && !isVector) {
                    coords = pi.getFluxesCoords();
                }
                else {
                    coords = pi.getBaseSpatialCoordinates();
                }
            }
            if (coords.size() == 1) {
                return replaceTags(condition.cloneNode(true), pi);
            }
            else {
                //If there are more than one coordinate operate them with the operation
                ProcessInfo newPi = new ProcessInfo(pi);
                newPi.setSpatialCoord1(coords.get(0));
                Element cond = replaceTags(condition.cloneNode(true), newPi);
                for (int j = 1; j < coords.size(); j++) {
                    newPi.setSpatialCoord1(coords.get(j));
                    cond = AGDMUtils.combineConditions(cond, replaceTags(condition.cloneNode(true), newPi), operation);
                }
                return cond;
            }
        }
        else {
            return replaceTags(condition.cloneNode(true), pi);
        }
    }
    /**
     * Process a mathML instruction.
     * @param problem                   The problem with the equation information
     * @param rule                      The rule
     * @param ri                        The rule information
     * @return                          A document fragment with maths processed
     * @throws AGDMException            AGDM002 XML discretization schema not valid
     *                                  AGDM00X External error
     */
    public static DocumentFragment processMath(Document discProblem, Node problem, Element rule, RuleInfo ri) throws AGDMException {
        //The result are stored in a document fragment
        Document doc = rule.getOwnerDocument();
        DocumentFragment result = doc.createDocumentFragment();

        //Process it for only one field
        if (ri.getIndividualField()) {
            //Look for spatial iterable tags to do the iteration
            NodeList spatialIteration = AGDMUtils.find(rule, SPATIAL_IT_TAGS);
            if (spatialIteration.getLength() > 0) {
                if (ri.getFunction() || ri.getIndividualCoord()) {
                	checkFluxAuxDefs(result, discProblem, problem, rule, ri);
                	checkCDAuxDefs(result, discProblem, problem, rule, ri);
                    Node ruleTags = rule.cloneNode(true);
                    if (ri.getProcessInfo().getRegionInfo().isAuxiliaryField(ri.getProcessInfo().getField())) {
                		replaceStepVariables(ruleTags, ri.getProcessInfo().getStepVariables());
                    }
                    result.appendChild(doc.importNode(replaceTags(ruleTags, ri.getProcessInfo()), true));
                }
                else {
                    ArrayList<String> coords;
                    //If fvm then the fluxes coordinates have to be used, if particles the base spatial coordinates
                    if (ri.getProcessInfo().getDiscretizationType() == DiscretizationType.mesh && !AGDMUtils.isVectorInstruction(rule)
                            && !ri.getUseBaseCoordinates()) {
                        coords = ri.getProcessInfo().getFluxesCoords();
                    }
                    else {
                        coords = ri.getProcessInfo().getBaseSpatialCoordinates();
                    }

                    for (int j = 0; j < coords.size(); j++) {
                        ProcessInfo pi = new ProcessInfo(ri.getProcessInfo());
                        pi.setSpatialCoord1(coords.get(j));
                        pi.setSpatialCoord2(null);
                        RuleInfo newRi = new RuleInfo(ri);
                        newRi.setProcessInfo(pi);
                        checkFluxAuxDefs(result, discProblem, problem, rule, newRi);
                        checkCDAuxDefs(result, discProblem, problem, rule, newRi);
                        Node ruleTags = rule.cloneNode(true);
                        if (pi.getRegionInfo().isAuxiliaryField(pi.getField())) {
                    		replaceStepVariables(ruleTags, pi.getStepVariables());
                    	}
                        result.appendChild(doc.importNode(replaceTags(ruleTags, pi), true));
                    }
                }
            }
            else {
            	checkCDAuxDefs(result, discProblem, problem, rule, ri);
                Node ruleTags = rule.cloneNode(true);
                if (ri.getProcessInfo().getRegionInfo().isAuxiliaryField(ri.getProcessInfo().getField())) {
            		replaceStepVariables(ruleTags, ri.getProcessInfo().getStepVariables());
            	}
                result.appendChild(doc.importNode(replaceTags(ruleTags, ri.getProcessInfo()), true));
            }
        }
        else {
            //If the left hand side does not contains a field or vector tag
            String query = "./mt:apply[(parent::mt:math[(not(parent::sml:if) or parent::sml:then) and (not(parent::sml:while) "
                + "or parent::sml:loop) and not(parent::sml:return) and not(ancestor::mms:applyIf) and " 
                + "not(ancestor::mms:finalizationCondition)])]/mt:eq/following-sibling::*[1][(name()='mt:apply' or name()='mt:ci' "
                + "or name()='sml:sharedVariable') and not(descendant::sml:field) and not(descendant::sml:eigenVector)]";
            //Or is a function call with no assignment
            query = query + "|./sml:functionCall";
            if (AGDMUtils.find(rule, query).getLength() > 0) {
                //Generate only one time (for the first field). Used for generic or known repetitive instructions 
                //like max characteristic speeds.
                ArrayList<ProcessInfo> pis = AGDMUtils.generateProcessInfo(problem, ri.getProcessInfo(), 
                        AGDMUtils.isVectorInstruction(rule));
                //Look for spatial iterable tags to do the iteration
                NodeList spatialIteration = AGDMUtils.find(rule, SPATIAL_IT_TAGS);
                if (spatialIteration.getLength() > 0) {
                    ArrayList<String> coords = ri.getProcessInfo().getBaseSpatialCoordinates();
                    for (int j = 0; j < coords.size(); j++) {
                        if (!ri.getIndividualCoord() 
                                || (ri.getIndividualCoord() && coords.get(j).equals(ri.getProcessInfo().getSpatialCoord1()))) {
                            ProcessInfo pi = pis.get(0);
                            pi.setSpatialCoord1(coords.get(j));
                            pi.setSpatialCoord2(null);
                            RuleInfo newRi = new RuleInfo(ri);
                            newRi.setProcessInfo(pi);
                            checkFluxAuxDefs(result, discProblem, problem, rule, newRi);
                            checkCDAuxDefs(result, discProblem, problem, rule, newRi);
                            
                            Node ruleTags = rule.cloneNode(true);
                            if (pi.getRegionInfo().isAuxiliaryField(pi.getField())) {
                        		replaceStepVariables(ruleTags, pi.getStepVariables());
                            }
                            result.appendChild(doc.importNode(replaceTags(ruleTags, pi), true));
                        }
                    }
                }
                else {
                	checkCDAuxDefs(result, discProblem, problem, rule, ri);
                	 Node ruleTags = rule.cloneNode(true);
                     if (pis.get(0).getRegionInfo().isAuxiliaryField(pis.get(0).getField())) {
                 		replaceStepVariables(ruleTags, pis.get(0).getStepVariables());
                     }
                    result.appendChild(doc.importNode(replaceTags(ruleTags, pis.get(0)), true));
                }
            }
            else {
                AGDMUtils.checkIncorrectInstruction(rule);
                ArrayList<ProcessInfo> pis = AGDMUtils.generateProcessInfo(problem, ri.getProcessInfo(), 
                        AGDMUtils.isVectorInstruction(rule));
                if (pis.size() > 0) {
	                //Look for spatial iterable tags to do the iteration
	                NodeList spatialIteration = AGDMUtils.find(rule, SPATIAL_IT_TAGS);
	                if (spatialIteration.getLength() > 0) {
	                    ArrayList<String> coords = ri.getProcessInfo().getBaseSpatialCoordinates();
	                    for (int j = 0; j < coords.size(); j++) {
	                        if (!ri.getIndividualCoord() 
	                                || (ri.getIndividualCoord() && coords.get(j).equals(ri.getProcessInfo().getSpatialCoord1()))) {
	                            RuleInfo newRi = new RuleInfo(ri);
	                        	for (int i = 0; i < pis.size(); i++) {
	    	                    	ArrayList<String> coordsComp;
	    	                        //If fvm then the fluxes coordinates have to be used, if particles the base spatial coordinates
	    		                    if (pis.get(i).getDiscretizationType() == DiscretizationType.mesh && !AGDMUtils.isVectorInstruction(rule) 
	    		                            && !ri.getUseBaseCoordinates()) {
	    		                    	coordsComp = pis.get(i).getFluxesCoords();
	    		                    }
	    		                    else {
	    		                    	coordsComp = pis.get(i).getBaseSpatialCoordinates();
	    		                    }
	    		                    if (coordsComp.contains(coords.get(j))) {
		                        		ProcessInfo pi = pis.get(i);
		                                pi.setSpatialCoord1(coords.get(j));
		                                pi.setSpatialCoord2(null);
		                        		newRi.setProcessInfo(pi);
		                                checkFluxAuxDefs(result, discProblem, problem, rule, newRi);
		                                checkCDAuxDefs(result, discProblem, problem, rule, newRi);
		                                
		                            	Node ruleTags = rule.cloneNode(true);
		                                if (pi.getRegionInfo().isAuxiliaryField(pi.getField())) {
		                                	replaceStepVariables(ruleTags, pi.getStepVariables());
		                                }
		                                result.appendChild(doc.importNode(replaceTags(ruleTags, pi), true));
	    		                    }
	                        	}
	                        }
	                    }
	                }
	                else {
	                	for (int i = 0; i < pis.size(); i++) {
	                		checkCDAuxDefs(result, discProblem, problem, rule, ri);
	                		Node ruleTags = rule.cloneNode(true);
	                		if (pis.get(i).getRegionInfo().isAuxiliaryField(pis.get(i).getField())) {
	                			replaceStepVariables(ruleTags, pis.get(i).getStepVariables());
	                		}
	                		result.appendChild(doc.importNode(replaceTags(ruleTags, pis.get(i)), true));
	                	}
	                }
                }
            }
        }
        return result;
    }
    /**
     * Creates the mathml for a diagonalization.
     * @param problem           The problem with the original information
     * @param rule              The rule with the diagonalization tag
     * @param ri                The rule information
     * @return                  A document fragment with the math equations
     * @throws AGDMException    AGDM006 Schema not applicable to this problem
     *                          AGDM007 External error
     */
    private static DocumentFragment processDiagonalization(Document discProblem, Node problem, Element rule, RuleInfo ri) throws AGDMException {
        //The result are stored in a document fragment
        Document elemDoc = rule.getOwnerDocument();
        DocumentFragment fragmentResult = rule.getOwnerDocument().createDocumentFragment();
        
        Element fieldVariable = (Element) rule.getFirstChild().getFirstChild().cloneNode(true);
        Element timeSlice = (Element) rule.getLastChild().getFirstChild().cloneNode(true);
                
        //The diagonalization block uses all the fields each time is being used
        ArrayList<ProcessInfo> pis = null;
        ProcessInfo actualPi;
        if (ri.getIndividualField()) {
            actualPi = ri.getProcessInfo();
        }
        else {
            pis = AGDMUtils.generateProcessInfo(problem, ri.getProcessInfo(), false);
            actualPi = pis.get(0);
        }
            
        //Check that the characteristic decomposition information exists and get it
        if (actualPi.getCharDecomposition() == null) {
            throw new AGDMException(AGDMException.AGDM006, "The schema uses characteristic decomposition but the model does not have anyone");
        }
        if (AGDMUtils.isVectorInstruction(rule)) {
            throw new AGDMException(AGDMException.AGDM002, "The diagonalization tag can not use eigenVector tag");
        }
        Element charDecompositions = (Element) actualPi.getCharDecomposition().cloneNode(true);
        
        //Projection of vectors
        fragmentResult.appendChild(elemDoc.importNode(AGDMUtils.projectVectors(charDecompositions, fieldVariable.getFirstChild().cloneNode(true), 
                timeSlice.getFirstChild().cloneNode(true), ri.getProcessInfo(), false), true));
        //Auxiliary definitions
        fragmentResult.appendChild(elemDoc.importNode(AGDMUtils.deployAuxiliaryDefinitions(discProblem, problem, charDecompositions, 0, ri.getProcessInfo(), 
                timeSlice.getFirstChild().cloneNode(true), false), true));
        //Diagonalize
        ArrayList<String> coords = ri.getProcessInfo().getBaseSpatialCoordinates();
        for (int i = 0; i < coords.size(); i++) {
            NodeList vectors = AGDMUtils.find(charDecompositions, ".//mms:eigenSpace//mms:eigenVector[ancestor::" 
                    + "mms:coordinateCharacteristicDecomposition[mms:eigenCoordinate = '" + coords.get(i) + "']]");
            if (vectors.getLength() == 0) {
                throw new AGDMException(AGDMException.AGDM006, "The diagonalize tag requires a coordinate characteristic decomposition");
            }
            for (int j = 0; j < vectors.getLength(); j++) {
                Element vectorTag = (Element) vectors.item(j).cloneNode(true);
                //Create the diagonalization equation for this vector
                Element equation = AGDMUtils.createElement(elemDoc, mtUri, "math");
                Element apply = AGDMUtils.createElement(elemDoc, mtUri, "apply");
                Element eq = AGDMUtils.createElement(elemDoc, mtUri, "eq");
                ProcessInfo newPi = new ProcessInfo(actualPi);
                newPi.setField(vectorTag.getFirstChild().getTextContent());
                newPi.setSpatialCoord1(coords.get(i));
                newPi.setSpatialCoord2(null);
                Element vectorTime = replaceTags(fieldVariable.cloneNode(true), newPi);
                Element ci = AGDMUtils.createElement(elemDoc, mtUri, "ci");
                ci.setTextContent(vectorTag.getFirstChild().getTextContent());
                apply.appendChild(eq);
                apply.appendChild(ci);
                apply.appendChild(elemDoc.importNode(AGDMUtils.createCoefficientInstruction(vectorTag, fieldVariable.cloneNode(true), 
                        timeSlice.cloneNode(true), newPi, true, false, false, false), true));
                equation.appendChild(apply);
                AGDMUtils.createTimeSlice(equation, vectorTag.getFirstChild().getTextContent(), vectorTime, 
                       ri.getProcessInfo().getRegionInfo(), ri.getProcessInfo().getCoordinateRelation());
                fragmentResult.appendChild(equation);
            }
        }
        return fragmentResult;
    }
    /**
     * Creates the mathml for a undiagonalization.
     * @param problem           The problem with the original information
     * @param rule               The element with the undiagonalization tag
     * @param ri                The rule information
     * @return                  A document fragment with the math equations
     * @throws AGDMException    AGDM006 Schema not applicable to this problem
     *                          AGDM007 External error
     */
    private static DocumentFragment processUndiagonalization(Document discProblem, Node problem, Element rule, RuleInfo ri) throws AGDMException {
        //The result are stored in a document fragment
        Document elemDoc = rule.getOwnerDocument();
        DocumentFragment fragmentResult = rule.getOwnerDocument().createDocumentFragment();
        
        Element fieldVariable = (Element) rule.getFirstChild().getFirstChild().cloneNode(true);
        Element timeSlice = (Element) rule.getLastChild().getFirstChild().cloneNode(true);
                
        //The diagonalization block uses all the fields each time is being used
        ProcessInfo actualPi;
        if (ri.getIndividualField()) {
            actualPi = ri.getProcessInfo();
        }
        else {
            actualPi = AGDMUtils.generateProcessInfo(problem, ri.getProcessInfo(), false).get(0);
        }
            
        //Check that the characteristic decomposition information exists and get it
        if (actualPi.getCharDecomposition() == null) {
            throw new AGDMException(AGDMException.AGDM006, "The schema uses characteristic decomposition but the model does not have anyone");
        }
        if (AGDMUtils.isVectorInstruction(rule)) {
            throw new AGDMException(AGDMException.AGDM002, "The diagonalization tag can not use eigenVector tag");
        }
        Element charDecompositions = (Element) actualPi.getCharDecomposition().cloneNode(true);
        
        //Auxiliary definitions
        fragmentResult.appendChild(AGDMUtils.deployAuxiliaryDefinitions(discProblem, problem, charDecompositions, 1, ri.getProcessInfo(), 
                timeSlice.cloneNode(true), false));   
        //Undiagonalize
        ArrayList<String> coords = ri.getProcessInfo().getBaseSpatialCoordinates();
        for (int i = 0; i < coords.size(); i++) {
            NodeList undiagonalizeList = AGDMUtils.find(charDecompositions, ".//mms:undiagonalization[ancestor::" 
                    + "mms:coordinateCharacteristicDecomposition[mms:eigenCoordinate = '" + coords.get(i) + "']]");
            if (undiagonalizeList.getLength() == 0) {
                throw new AGDMException(AGDMException.AGDM006, "The undiagonalize tag requires a coordinate characteristic decomposition");
            }
            for (int j = 0; j < undiagonalizeList.getLength(); j++) {
                Element undiagonalizeTag = (Element) undiagonalizeList.item(j).cloneNode(true);
                //Create the diagonalization equation for this vector
                Element equation = AGDMUtils.createElement(elemDoc, mtUri, "math");
                Element apply = AGDMUtils.createElement(elemDoc, mtUri, "apply");
                Element eq = AGDMUtils.createElement(elemDoc, mtUri, "eq");
                ProcessInfo newPi = new ProcessInfo(actualPi);
                newPi.setField(undiagonalizeTag.getFirstChild().getTextContent());
                newPi.setSpatialCoord1(coords.get(i));
                newPi.setSpatialCoord2(null);
                Element fieldTime = null;
                if (AGDMUtils.isField(actualPi.getCompleteProblem(), undiagonalizeTag.getFirstChild().getTextContent())) {
                    fieldTime = replaceTags(fieldVariable.cloneNode(true), newPi);
                }
                else {
                    fieldTime =  AGDMUtils.createElement(elemDoc, mtUri, "ci");
                    fieldTime.setTextContent(undiagonalizeTag.getFirstChild().getTextContent() + "_var");
                }
                Element ci = AGDMUtils.createElement(elemDoc, mtUri, "ci");
                ci.setTextContent(undiagonalizeTag.getFirstChild().getTextContent());
                apply.appendChild(eq);
                apply.appendChild(ci);
                apply.appendChild(elemDoc.importNode(
                        AGDMUtils.createCoefficientInstruction(undiagonalizeTag, fieldVariable.cloneNode(true), 
                                timeSlice.cloneNode(true), newPi, true, false, true, false), true));
                equation.appendChild(apply);
                AGDMUtils.createTimeSlice(equation, undiagonalizeTag.getFirstChild().getTextContent(), 
                        fieldTime, ri.getProcessInfo().getRegionInfo(), 
                        ri.getProcessInfo().getCoordinateRelation());
                fragmentResult.appendChild(equation);
            }
        }
        //Unprojection of vectors
        fragmentResult.appendChild(elemDoc.importNode(
                AGDMUtils.unprojectVectors(charDecompositions, fieldVariable.cloneNode(true), ri.getProcessInfo()), true));
        return fragmentResult;
    }
 
    /**
     * Do the replacement of the tags used in the schemas with the actual values.
     * There are two kinds of replacements: DOM and String replacements. 
     * 
     * @param child                 The node with the element to replace
     * @param pi                    The process information for the equation
     * @return                      The element with the actual values
     * @throws AGDMException        AGDM006 Schema not applicable to this problem
     *                              AGDM00X External error
     */
    public static Element replaceTags(Node child, ProcessInfo pi) 
        throws AGDMException {
    	
        String discSpatialCoord = null;
        if (pi.getSpatialCoord1() != null) {
            discSpatialCoord = pi.getCoordinateRelation().getContToDisc(pi.getSpatialCoord1());
        }
        String discSpatialCoord2 = null;
        if (pi.getSpatialCoord2() != null) {
            discSpatialCoord2 = pi.getCoordinateRelation().getContToDisc(pi.getSpatialCoord2());
        }
        
        //Node with nodes replacement
        Element elementNode = (Element) child;
        Document elemDoc = elementNode.getOwnerDocument();

        /*
         * DOM replacement area
         */
        //maxCharSpeed
        NodeList maxCharSpeeds = elementNode.getElementsByTagName("sml:maxCharSpeed");
        if (maxCharSpeeds.getLength() > 0) {
            if (pi.getCharDecomposition() == null) {
                throw new AGDMException(AGDMException.AGDM006, "Two possible causes:" + System.getProperty("line.separator")
                    + "The schema uses characteristic speed but the model does not have characteristic decomposition." 
                    + System.getProperty("line.separator")
                    + "There is a maxCharSpeed tag inside a function. This is not avoided");
            }
            maxCharSpeedDeploy(elemDoc, maxCharSpeeds, pi);
        }
        //positiveCharSpeed
        NodeList positiveCharSpeeds = elementNode.getElementsByTagName("sml:positiveCharSpeed");
        if (positiveCharSpeeds.getLength() > 0) {
            if (pi.getCharDecomposition() == null) {
                throw new AGDMException(AGDMException.AGDM006, "Two possible causes:" + System.getProperty("line.separator")
                    + "The schema uses characteristic speed but the model does not have characteristic decomposition." 
                    + System.getProperty("line.separator")
                    + "There is a positiveCharSpeed tag inside a function. This is not avoided");
            }
            charSpeedDeploy(elemDoc, positiveCharSpeeds, pi, "Positive");
        }
        //positiveCharSpeed
        NodeList negativeCharSpeeds = elementNode.getElementsByTagName("sml:negativeCharSpeed");
        if (negativeCharSpeeds.getLength() > 0) {
            if (pi.getCharDecomposition() == null) {
                throw new AGDMException(AGDMException.AGDM006, "Two possible causes:" + System.getProperty("line.separator")
                    + "The schema uses characteristic speed but the model does not have characteristic decomposition." 
                    + System.getProperty("line.separator")
                    + "There is a negativeCharSpeed tag inside a function. This is not avoided");
            }
            charSpeedDeploy(elemDoc, negativeCharSpeeds, pi, "Negative");
        }
        
        //maxCharSpeedCoordinate
        NodeList maxCharSpeedsCoordinate = elementNode.getElementsByTagName("sml:maxCharSpeedCoordinate");
        if (maxCharSpeedsCoordinate.getLength() > 0) {
            if (pi.getCharDecomposition() == null) {
                throw new AGDMException(AGDMException.AGDM006, "Two possible causes:" + System.getProperty("line.separator")
                    + "The schema uses characteristic speed but the model does not have coordinate characteristic decomposition." 
                    + System.getProperty("line.separator")
                    + "There is a maxCharSpeed tag inside a function. This is not avoided");
            }
            maxCharSpeedCoordinateDeploy(elemDoc, maxCharSpeedsCoordinate, pi);
        }
        
        //maxCharSpeedCoordinate
        NodeList positiveCharSpeedsCoordinate = elementNode.getElementsByTagName("sml:positiveCharSpeedCoordinate");
        if (positiveCharSpeedsCoordinate.getLength() > 0) {
            if (pi.getCharDecomposition() == null) {
                throw new AGDMException(AGDMException.AGDM006, "Two possible causes:" + System.getProperty("line.separator")
                    + "The schema uses characteristic speed but the model does not have coordinate characteristic decomposition." 
                    + System.getProperty("line.separator")
                    + "There is a positiveCharSpeed tag inside a function. This is not avoided");
            }
            charSpeedCoordinateDeploy(elemDoc, positiveCharSpeedsCoordinate, pi, "Positive");
        }
        
        //maxCharSpeedCoordinate
        NodeList negativeCharSpeedsCoordinate = elementNode.getElementsByTagName("sml:negativeCharSpeedCoordinate");
        if (negativeCharSpeedsCoordinate.getLength() > 0) {
            if (pi.getCharDecomposition() == null) {
                throw new AGDMException(AGDMException.AGDM006, "Two possible causes:" + System.getProperty("line.separator")
                    + "The schema uses characteristic speed but the model does not have coordinate characteristic decomposition." 
                    + System.getProperty("line.separator")
                    + "There is a negativeCharSpeed tag inside a function. This is not avoided");
            }
            charSpeedCoordinateDeploy(elemDoc, negativeCharSpeedsCoordinate, pi, "Negative");
        }
        
        //Replaces the deployment of coordinate tags.
        replaceCoordinateDeploymentTags(elemDoc, elementNode, pi, discSpatialCoord, discSpatialCoord2);
       
        //Cell position
        if (pi.getSpatialCoord1() != null) {
	        NodeList position = elementNode.getElementsByTagNameNS(AGDMUtils.simmlUri, "cellPosition");
	        for (int i = 0; i < position.getLength(); i++) {
	            position.item(i).setTextContent(discSpatialCoord);
	        }
        }
        
        //Cell deltaSpacing
        if (pi.getSpatialCoord1() != null) {
	        NodeList dx = elementNode.getElementsByTagNameNS(AGDMUtils.simmlUri, "cellDeltaSpacing");
	        for (int i = 0; i < dx.getLength(); i++) {
	        	dx.item(i).setTextContent(pi.getSpatialCoord1());
	        }
        }
        
        //sources
        NodeList sources = elementNode.getElementsByTagName("sml:sources");
        for (int i = 0; i < sources.getLength(); i++) {
            Element sourceTime = (Element) sources.item(i).getFirstChild();
            sources.item(i).getParentNode().replaceChild(elemDoc.importNode(processSources(sourceTime, pi), true), sources.item(i));
        }
        
        //Particle position
        if (pi.getSpatialCoord1() != null) {
	        NodeList position = elementNode.getElementsByTagNameNS(AGDMUtils.simmlUri, "particlePosition");
	        for (int i = 0; i < position.getLength(); i++) {
	            position.item(i).setTextContent(pi.getSpatialCoord1());
	        }
        }
        
        //Particle deltaSpacing
        if (pi.getSpatialCoord1() != null) {
	        NodeList dx = elementNode.getElementsByTagNameNS(AGDMUtils.simmlUri, "particleDeltaSpacing");
	        for (int i = 0; i < dx.getLength(); i++) {
	        	dx.item(i).setTextContent(pi.getSpatialCoord1());
	        }
        }
        
        //Particle velocity
        NodeList velocity = elementNode.getElementsByTagName("sml:particleVelocity");
        for (int i = velocity.getLength() - 1; i >= 0; i--) {
        	velocity.item(i).getParentNode().replaceChild(elemDoc.importNode(pi.getVelocityComponent(pi.getSpatialCoord1()).getFirstChild().cloneNode(true), true), velocity.item(i));
        }
        
        //Particle Volume
        NodeList volume = elementNode.getElementsByTagName("sml:particleVolume");
        for (int i = volume.getLength() - 1; i >= 0; i--) {
            Element volumeVar = (Element) volume.item(i).getFirstChild().cloneNode(true);
            //Apply volume level factor for AMR in particles
            Element apply = AGDMUtils.createElement(elemDoc, mtUri, "apply");
            Element times = AGDMUtils.createElement(elemDoc, mtUri, "times");
            Element ci = AGDMUtils.createElement(elemDoc, mtUri, "ci");
            ci.setTextContent("volume_level_factor");
            apply.appendChild(times);
            apply.appendChild(elemDoc.importNode(processParticleVolume(volumeVar, pi), true));
            apply.appendChild(ci);
            volume.item(i).getParentNode().replaceChild(apply, volume.item(i));
        }
        
        /*
         * String replacement area
         */
        ArrayList<Node> replacements = new ArrayList<Node>();
        NodeList tag = elementNode.getElementsByTagName("sml:field");
        Text text = elementNode.getOwnerDocument().createTextNode(pi.getField());
        for (int i = tag.getLength() - 1; i >= 0; i--) {
            Element field = (Element) tag.item(i);
            replacements.add(field.getParentNode());
            field.getParentNode().replaceChild(text.cloneNode(true), field);
        }
        
        tag = elementNode.getElementsByTagName("sml:eigenVector");
        text = elementNode.getOwnerDocument().createTextNode(pi.getVector());
        for (int i = tag.getLength() - 1; i >= 0; i--) {
            Element field = (Element) tag.item(i);
            replacements.add(field.getParentNode());
            field.getParentNode().replaceChild(text.cloneNode(true), field);
        }
                
        if (pi.getSpatialCoord1() != null) {
            tag = elementNode.getElementsByTagName("sml:spatialCoordinate");
            text = elementNode.getOwnerDocument().createTextNode(discSpatialCoord);
            for (int i = tag.getLength() - 1; i >= 0; i--) {
                Element field = (Element) tag.item(i);
                replacements.add(field.getParentNode());
                field.getParentNode().replaceChild(text.cloneNode(true), field);
             
            }
            tag = elementNode.getElementsByTagName("sml:contSpatialCoordinate");
            text = elementNode.getOwnerDocument().createTextNode(pi.getSpatialCoord1());
            for (int i = tag.getLength() - 1; i >= 0; i--) {
                Element field = (Element) tag.item(i);
                replacements.add(field.getParentNode());
                field.getParentNode().replaceChild(text.cloneNode(true), field);
            }
        }
        
        if (pi.getSpatialCoord2() != null) {
            tag = elementNode.getElementsByTagName("sml:secondSpatialCoordinate");
            text = elementNode.getOwnerDocument().createTextNode(discSpatialCoord2);
            for (int i = tag.getLength() - 1; i >= 0; i--) {
                Element field = (Element) tag.item(i);
                replacements.add(field.getParentNode());
                field.getParentNode().replaceChild(text.cloneNode(true), field);
            }
            tag = elementNode.getElementsByTagName("sml:secondContSpatialCoordinate");
            text = elementNode.getOwnerDocument().createTextNode(pi.getSpatialCoord2());
            for (int i = tag.getLength() - 1; i >= 0; i--) {
                Element field = (Element) tag.item(i);
                replacements.add(field.getParentNode());
                field.getParentNode().replaceChild(text.cloneNode(true), field);
            }
        }
        
        //Replaces time coordinate only if not creating time interpolation instructions
        tag = elementNode.getElementsByTagName("sml:timeCoordinate");
        if (!pi.isTimeInterpolator()) {
            text = elementNode.getOwnerDocument().createTextNode("(" + pi.getCoordinateRelation().getDiscTimeCoord() + ")");
            for (int i = tag.getLength() - 1; i >= 0; i--) {
                Element field = (Element) tag.item(i);
                replacements.add(field.getParentNode());
                field.getParentNode().replaceChild(text.cloneNode(true), field);
            }
        }

        tag = elementNode.getElementsByTagName("sml:kernel");
        if (tag.getLength() > 0 && pi.getKernel() == null) {
            throw new AGDMException(AGDMException.AGDM006, "Kernel tag used, but not provided in discretization schema.");
        }
        for (int i = tag.getLength() - 1; i >= 0; i--) {
            Element field = (Element) tag.item(i);
            field.getParentNode().replaceChild(elemDoc.importNode(pi.getKernel().cloneNode(true), true), field);
        }
        
        tag = elementNode.getElementsByTagName("sml:kernelGradient");
        if (tag.getLength() > 0 && pi.getKernelGradient() == null) {
            throw new AGDMException(AGDMException.AGDM006, "KernelGradient tag used, but not provided in discretization schema.");
        }
        for (int i = tag.getLength() - 1; i >= 0; i--) {
            Element field = (Element) tag.item(i);
            field.getParentNode().replaceChild(elemDoc.importNode(pi.getKernelGradient().cloneNode(true), true), field);
        }
        
        tag = elementNode.getElementsByTagName("sml:contTimeCoordinate");
        text = elementNode.getOwnerDocument().createTextNode(pi.getCoordinateRelation().getContTimeCoord());
        for (int i = tag.getLength() - 1; i >= 0; i--) {
            Element field = (Element) tag.item(i);
            replacements.add(field.getParentNode());
            field.getParentNode().replaceChild(text.cloneNode(true), field);
        }
   
        //Remove text nodes
        for (int i = 0; i < replacements.size(); i++) {
        	Node replacement = replacements.get(i);
        	replacement.setTextContent(replacement.getTextContent());
        }
        return elementNode;
    }
    /**
     * Deploys the coordinates depending on the tag.
     * @param elemDoc               The document with the information
     * @param elementNode           The element to discretize.
     * @param pi                    The process information for the equation
     * @param discSpatialCoord      The actual spatial coordinate
     * @param discSpatialCoord2     The second derivative spatial coordinate
     * @throws AGDMException 
     */
    private static void replaceCoordinateDeploymentTags(Document elemDoc, Element elementNode, ProcessInfo pi, 
            String discSpatialCoord, String discSpatialCoord2) throws AGDMException {
        //currentCell
        DocumentFragment deploy = AGDMUtils.currentCell(elemDoc, pi);
        NodeList currentCell = elementNode.getElementsByTagName("sml:currentCell");
        for (int i = currentCell.getLength() - 1; i >= 0; i--) {
            Element parent = (Element) currentCell.item(i).getParentNode();
            parent.replaceChild(deploy.cloneNode(true), currentCell.item(i));
        }

        //incrementCoordinate2
        NodeList increments = elementNode.getElementsByTagName("sml:incrementCoordinate2");
        for (int i = increments.getLength() - 1; i >= 0; i--) {
            if (pi.getDiscretizationType() == DiscretizationType.particles) {
                throw new AGDMException(AGDMException.AGDM006, "The schema uses incrementCoordinate2, not valid for particles schemas");
            }
            DocumentFragment increment = AGDMUtils.operateCoordinate(
                    elemDoc, pi, discSpatialCoord2, increments.item(i).getTextContent(), "plus");
            Element parent = (Element) increments.item(i).getParentNode();
            parent.replaceChild(increment, increments.item(i));
        }
        //decrementCoordinate2
        NodeList decrements = elementNode.getElementsByTagName("sml:decrementCoordinate2");
        for (int i = decrements.getLength() - 1; i >= 0; i--) {
            if (pi.getDiscretizationType() == DiscretizationType.particles) {
                throw new AGDMException(AGDMException.AGDM006, "The schema uses decrementCoordinate2, not valid for particles schemas");
            }
            DocumentFragment decrement = 
                AGDMUtils.operateCoordinate(elemDoc, pi, discSpatialCoord2, decrements.item(i).getTextContent(), "minus");
            Element parent = (Element) decrements.item(i).getParentNode();
            parent.replaceChild(decrement, decrements.item(i));
        }
        //incrementCoordinate
        increments = elementNode.getElementsByTagName("sml:incrementCoordinate");
        for (int i = increments.getLength() - 1; i >= 0; i--) {
            if (pi.getDiscretizationType() == DiscretizationType.particles) {
                throw new AGDMException(AGDMException.AGDM006, "The schema uses incrementCoordinate, not valid for particles schemas");
            }
            DocumentFragment increment = 
                AGDMUtils.operateCoordinate(elemDoc, pi, discSpatialCoord, increments.item(i).getTextContent(), "plus");
            Element parent = (Element) increments.item(i).getParentNode();
            parent.replaceChild(increment, increments.item(i));
        }
        //incrementCoordinate1IncrementCoordinate2
        increments = elementNode.getElementsByTagName("sml:incrementCoordinate1IncrementCoordinate2");
        for (int i = increments.getLength() - 1; i >= 0; i--) {
            if (pi.getDiscretizationType() == DiscretizationType.particles) {
                throw new AGDMException(AGDMException.AGDM006, 
                        "The schema uses incrementCoordinate1IncrementCoordinate2, not valid for particles schemas");
            }
            DocumentFragment increment = AGDMUtils.operateCoordinate(
                    elemDoc, pi, discSpatialCoord, discSpatialCoord2, increments.item(i).getTextContent(), "plus", "plus");
            Element parent = (Element) increments.item(i).getParentNode();
            parent.replaceChild(increment, increments.item(i));
        }
        //incrementCoordinate1DecrementCoordinate2
        increments = elementNode.getElementsByTagName("sml:incrementCoordinate1DecrementCoordinate2");
        for (int i = increments.getLength() - 1; i >= 0; i--) {
            if (pi.getDiscretizationType() == DiscretizationType.particles) {
                throw new AGDMException(AGDMException.AGDM006, 
                        "The schema uses incrementCoordinate1DecrementCoordinate2, not valid for particles schemas");
            }
            DocumentFragment increment = AGDMUtils.operateCoordinate(
                    elemDoc, pi, discSpatialCoord, discSpatialCoord2, increments.item(i).getTextContent(), "plus", "minus");
            Element parent = (Element) increments.item(i).getParentNode();
            parent.replaceChild(increment, increments.item(i));
        }
        //decrementCoordinate
        decrements = elementNode.getElementsByTagName("sml:decrementCoordinate");
        for (int i = decrements.getLength() - 1; i >= 0; i--) {
            if (pi.getDiscretizationType() == DiscretizationType.particles) {
                throw new AGDMException(AGDMException.AGDM006, "The schema uses decrementCoordinate, not valid for particles schemas");
            }
            DocumentFragment decrement = 
                AGDMUtils.operateCoordinate(elemDoc, pi, discSpatialCoord, decrements.item(i).getTextContent(), "minus");
            Element parent = (Element) decrements.item(i).getParentNode();
            parent.replaceChild(decrement, decrements.item(i));
        }
        //decrementCoordinate1IncrementCoordinate2
        increments = elementNode.getElementsByTagName("sml:decrementCoordinate1IncrementCoordinate2");
        for (int i = increments.getLength() - 1; i >= 0; i--) {
            if (pi.getDiscretizationType() == DiscretizationType.particles) {
                throw new AGDMException(AGDMException.AGDM006, 
                        "The schema uses decrementCoordinate1IncrementCoordinate2, not valid for particles schemas");
            }
            DocumentFragment increment = AGDMUtils.operateCoordinate(
                    elemDoc, pi, discSpatialCoord, discSpatialCoord2, increments.item(i).getTextContent(), "minus", "plus");
            Element parent = (Element) increments.item(i).getParentNode();
            parent.replaceChild(increment, increments.item(i));
        }
        //decrementCoordinate1DecrementCoordinate2
        increments = elementNode.getElementsByTagName("sml:decrementCoordinate1DecrementCoordinate2");
        for (int i = increments.getLength() - 1; i >= 0; i--) {
            if (pi.getDiscretizationType() == DiscretizationType.particles) {
                throw new AGDMException(AGDMException.AGDM006, 
                        "The schema uses decrementCoordinate1DecrementCoordinate2, not valid for particles schemas");
            }
            DocumentFragment increment = AGDMUtils.operateCoordinate(
                    elemDoc, pi, discSpatialCoord, discSpatialCoord2, increments.item(i).getTextContent(), "minus", "minus");
            Element parent = (Element) increments.item(i).getParentNode();
            parent.replaceChild(increment, increments.item(i));
        }
    }
    /**
     * Deploy the max characteristic speed depending on the tags from the characteristic decomposition.
     * 1 - Max characteristic speed
     * 2 - Maximum of eigenvalues of coordinate characteristic speed
     * 3 - Maximum of eigenvalues of coordinate characteristic speed converted from general characteristic speed 
     * 4 - Maximum of eigenvalues of coordinate characteristic speed (Positive and Negative)
     * 5 - Maximum of eigenvalues of coordinate characteristic speed converted from general characteristic speed (Positive and Negative)
     * @param speeds                The occurrences of the maxCharSpeed tag
     * @param pi                    The process information for the equation
     * @param doc                   The document where the information is
     * @throws AGDMException        AGDM006  Schema not applicable to this problem
     *                              AGDM007  External error
     */
    private static void maxCharSpeedDeploy(Document doc, NodeList speeds, ProcessInfo pi) throws AGDMException {
        //MaxCharacteristicSpeed tag
        boolean generalCharacteristicDecomposition = false;
        //Select specific characteristic information if available
        Element charDecomp = pi.getCharDecomposition();
        NodeList specificCharDecomp = AGDMUtils.find(charDecomp, ".//mms:characteristicDecomposition[mms:applyingFields/mms:field = '" + pi.getField() + "']");
        if (specificCharDecomp.getLength() > 0) {
        	charDecomp = (Element) specificCharDecomp.item(0);
        } else {
        	NodeList defaultCharDecomp = AGDMUtils.find(charDecomp, ".//mms:characteristicDecomposition[not(mms:applyingFields)]");
            if (defaultCharDecomp.getLength() == 0) {
                throw new AGDMException(AGDMException.AGDM006, 
                        "The schema uses characteristic speed, but field " + pi.getField() + " does not have a valid characteristic information.");
            }
        	charDecomp = (Element) defaultCharDecomp.item(0);
        }
        NodeList maxCharSpeed = charDecomp.getElementsByTagName("mms:maxCharacteristicSpeed");
        if (maxCharSpeed.getLength() == 0) {
            //EigenvaluesTag from coordinateCharacteristicDecomposition
            maxCharSpeed = AGDMUtils.find(charDecomp, ".//mms:eigenSpace[mms:type = 'Max']/mms:eigenValue[ancestor::mms:coordinateCharacteristicDecomposition]");
            if (maxCharSpeed.getLength() == 0) {
                maxCharSpeed = AGDMUtils.find(charDecomp, ".//mms:eigenSpace[mms:type = 'Max']/mms:eigenValue[ancestor::mms:generalCharacteristicDecomposition]");
                if (maxCharSpeed.getLength() == 0) {
                    maxCharSpeed = AGDMUtils.find(charDecomp, ".//mms:eigenValue[ancestor::mms:coordinateCharacteristicDecomposition]");
                    if (maxCharSpeed.getLength() == 0) {
                        maxCharSpeed = AGDMUtils.find(charDecomp, ".//mms:eigenValue[ancestor::mms:generalCharacteristicDecomposition]");
		                generalCharacteristicDecomposition = true;
		                if (maxCharSpeed.getLength() == 0) {
		                    throw new AGDMException(AGDMException.AGDM006, 
		                            "The schema uses characteristic speed, the model must have max Characteristic speed or some eigen values");
		                }
                    }
                }
                else {
	                generalCharacteristicDecomposition = true;
                }
            }
        }
        
        //The absolute value for the speeds
        Element absoluteValue = AGDMUtils.createElement(doc, mtUri, "apply");
        Element abs = AGDMUtils.createElement(doc, mtUri, "abs");
        absoluteValue.appendChild(abs);
        
        //Make the replacements
        for (int i = 0; i < speeds.getLength(); i++) {
            //Store a copy of the speed equations to not overwrite the original ones.
            DocumentFragment speedMaths = doc.createDocumentFragment();
            Element variable = (Element) speeds.item(i).getFirstChild();
            //Check for possible use of fields in the charSpeeds and replace their index with the parameter ones.
            for (int j = 0; j < maxCharSpeed.getLength(); j++) {
                Element speedMath = (Element) doc.importNode(maxCharSpeed.item(j).getFirstChild().cloneNode(true), true);
                //Create the equation with the time slice
                AGDMUtils.replaceFieldVariables(pi, (Element) variable, speedMath);
                if (generalCharacteristicDecomposition || AGDMUtils.useNormals(speedMath, pi.getBaseSpatialCoordinates())) {
                    for (int k = 0; k < pi.getBaseSpatialCoordinates().size(); k++) {
                        ProcessInfo newPi = new ProcessInfo(pi);
                        newPi.setSpatialCoord1(pi.getBaseSpatialCoordinates().get(k));
                        speedMaths.appendChild(AGDMUtils.replaceNormals(speedMath.cloneNode(true), pi.getBaseSpatialCoordinates(), pi.getBaseSpatialCoordinates().get(k)));
                    }
                }
                else {
                    speedMaths.appendChild(speedMath);
                }
            }
            NodeList speedMathsChildren = speedMaths.getChildNodes();
            //Only one equation implies that there is the max characteristic speed.
            if (speedMathsChildren.getLength() == 1) {
                Element speed = (Element) absoluteValue.cloneNode(true);
                speed.appendChild(speedMathsChildren.item(0).getFirstChild());
                speeds.item(i).getParentNode().replaceChild(speed, speeds.item(i));
            }
            else {
                Element apply = AGDMUtils.createElement(doc, mtUri, "apply");
                Element max = AGDMUtils.createElement(doc, mtUri, "max");
                apply.appendChild(max);
                Element applyBase = apply;
                for (int j = 0; j < speedMathsChildren.getLength(); j++) {
                    Element speed = (Element) absoluteValue.cloneNode(true);
                    speed.appendChild(speedMathsChildren.item(j).getFirstChild());
                    apply.appendChild(speed);
                }
                speeds.item(i).getParentNode().replaceChild(applyBase, speeds.item(i));
            }
        }
    }
    
    /**
     * Deploy the positive or negative characteristic speed depending on the tags from the characteristic decomposition.
     * 1 - Maximum of eigenvalues of coordinate characteristic speed
     * 2 - Maximum of eigenvalues of coordinate characteristic speed converted from general characteristic speed 
     * @param speeds                The occurrences of the maxCharSpeed tag
     * @param pi                    The process information for the equation
     * @param doc                   The document where the information is
     * @param speedType				Positive or Negative
     * @throws AGDMException        AGDM006  Schema not applicable to this problem
     *                              AGDM007  External error
     */
    private static void charSpeedDeploy(Document doc, NodeList speeds, ProcessInfo pi, String speedType) throws AGDMException {
        //MaxCharacteristicSpeed tag
        boolean generalCharacteristicDecomposition = false;
        //Select specific characteristic information if available
        Element charDecomp = pi.getCharDecomposition();
        NodeList specificCharDecomp = AGDMUtils.find(charDecomp, ".//mms:characteristicDecomposition[mms:applyingFields/mms:field = '" + pi.getField() + "']");
        if (specificCharDecomp.getLength() > 0) {
        	charDecomp = (Element) specificCharDecomp.item(0);
        } else {
        	NodeList defaultCharDecomp = AGDMUtils.find(charDecomp, ".//mms:characteristicDecomposition[not(mms:applyingFields)]");
            if (defaultCharDecomp.getLength() == 0) {
                throw new AGDMException(AGDMException.AGDM006, 
                        "The schema uses characteristic speed, but field " + pi.getField() + " does not have a valid characteristic information.");
            }
        	charDecomp = (Element) defaultCharDecomp.item(0);
        }
        //EigenvaluesTag from coordinateCharacteristicDecomposition
        NodeList maxCharSpeed = AGDMUtils.find(charDecomp, ".//mms:eigenSpace[mms:type = '" + speedType + "']/mms:eigenValue[ancestor::mms:coordinateCharacteristicDecomposition]");
        if (maxCharSpeed.getLength() == 0) {
            maxCharSpeed = AGDMUtils.find(charDecomp, ".//mms:eigenSpace[mms:type = '" + speedType + "']/mms:eigenValue[ancestor::mms:generalCharacteristicDecomposition]");
            generalCharacteristicDecomposition = true;
            if (maxCharSpeed.getLength() == 0) {
                throw new AGDMException(AGDMException.AGDM006, 
                        "The schema uses positive characteristic speed, the model must have Eigen values");
            }
        }
                
        //Make the replacements
        for (int i = 0; i < speeds.getLength(); i++) {
            //Store a copy of the speed equations to not overwrite the original ones.
            DocumentFragment speedMaths = doc.createDocumentFragment();
            Element variable = (Element) speeds.item(i).getFirstChild();
            //Check for possible use of fields in the charSpeeds and replace their index with the parameter ones.
            for (int j = 0; j < maxCharSpeed.getLength(); j++) {
                Element speedMath = (Element) doc.importNode(maxCharSpeed.item(j).getFirstChild().cloneNode(true), true);
                //Create the equation with the time slice
                AGDMUtils.replaceFieldVariables(pi, (Element) variable, speedMath);
                if (generalCharacteristicDecomposition || AGDMUtils.useNormals(speedMath, pi.getBaseSpatialCoordinates())) {
                    for (int k = 0; k < pi.getBaseSpatialCoordinates().size(); k++) {
                        ProcessInfo newPi = new ProcessInfo(pi);
                        newPi.setSpatialCoord1(pi.getBaseSpatialCoordinates().get(k));
                        speedMaths.appendChild(AGDMUtils.replaceNormals(speedMath.cloneNode(true), pi.getBaseSpatialCoordinates(), pi.getBaseSpatialCoordinates().get(k)));
                    }
                }
                else {
                    speedMaths.appendChild(speedMath);
                }
            }
            NodeList speedMathsChildren = speedMaths.getChildNodes();
            //Only one equation implies that there is the max characteristic speed.
            if (speedMathsChildren.getLength() == 1) {
                Element speed = (Element) speedMathsChildren.item(0).getFirstChild().cloneNode(true);
                speeds.item(i).getParentNode().replaceChild(speed, speeds.item(i));
            }
            else {
                Element apply = AGDMUtils.createElement(doc, mtUri, "apply");
                Element max = AGDMUtils.createElement(doc, mtUri, "max");
                apply.appendChild(max);
                Element applyBase = apply;
                for (int j = 0; j < speedMathsChildren.getLength(); j++) {
                    Element speed = (Element) speedMathsChildren.item(j).getFirstChild().cloneNode(true);
                    apply.appendChild(speed);
                }
                speeds.item(i).getParentNode().replaceChild(applyBase, speeds.item(i));
            }
        }
    }
    
    /**
     * Deploy the max characteristic speed depending on the tags from the characteristic decomposition.
     * 
     * @param speeds                The occurrences of the maxCharSpeed tag
     * @param pi                    The process information for the equation
     * @param doc                   The document where the information is
     * @throws AGDMException        AGDM006 Schema not applicable to this problem
     *                              AGDM007 External error
     *                              
     */
    private static void maxCharSpeedCoordinateDeploy(Document doc, NodeList speeds, ProcessInfo pi) throws AGDMException {
        //Select specific characteristic information if available
        Element charDecomp = pi.getCharDecomposition();
        NodeList specificCharDecomp = AGDMUtils.find(charDecomp, ".//mms:characteristicDecomposition[mms:applyingFields/mms:field = '" + pi.getField() + "']");
        if (specificCharDecomp.getLength() > 0) {
        	charDecomp = (Element) specificCharDecomp.item(0);
        } else {
        	NodeList defaultCharDecomp = AGDMUtils.find(charDecomp, ".//mms:characteristicDecomposition[not(mms:applyingFields)]");
            if (defaultCharDecomp.getLength() == 0) {
                throw new AGDMException(AGDMException.AGDM006, 
                        "The schema uses characteristic speed, but field " + pi.getField() + " does not have a valid characteristic information.");
            }
        	charDecomp = (Element) defaultCharDecomp.item(0);
        }
        //Coordinate char decomposition
        String query = ".//mms:eigenSpace[mms:type = 'Max' and ancestor::" 
                + "mms:coordinateCharacteristicDecomposition[mms:eigenCoordinate = '" + pi.getSpatialCoord1() + "']]/mms:eigenValue";
        NodeList maxCharSpeed = AGDMUtils.find(charDecomp, query);
        if (maxCharSpeed.getLength() == 0) {
            //Angle char decomposition
            query = ".//mms:eigenSpace[mms:type = 'Max' and ancestor::mms:generalCharacteristicDecomposition]/mms:eigenValue";
            maxCharSpeed = AGDMUtils.find(charDecomp, query);
            if (maxCharSpeed.getLength() == 0) {
                query = ".//mms:eigenSpace[ancestor::" 
                        + "mms:coordinateCharacteristicDecomposition[mms:eigenCoordinate = '" + pi.getSpatialCoord1() + "']]/mms:eigenValue"; 
                maxCharSpeed = AGDMUtils.find(charDecomp, query);
                if (maxCharSpeed.getLength() == 0) {
                    query = ".//mms:eigenSpace[ancestor::mms:generalCharacteristicDecomposition]/mms:eigenValue";
                    maxCharSpeed = AGDMUtils.find(charDecomp, query);
                    if (maxCharSpeed.getLength() == 0) {
                        throw new AGDMException(AGDMException.AGDM006, 
                            "The schema uses characteristic speed, the model must have coordinate or general Characteristic decomposition");
                    }
                }
            }
        }

        //The absolute value for the speeds
        Element absoluteValue = AGDMUtils.createElement(doc, mtUri, "apply");
        Element abs = AGDMUtils.createElement(doc, mtUri, "abs");
        absoluteValue.appendChild(abs);
        
        //Make the replacements
        for (int i = 0; i < speeds.getLength(); i++) {
            //Store a copy of the speed equations to not overwrite the original ones.
            DocumentFragment speedMaths = doc.createDocumentFragment();
            Element variable = (Element) speeds.item(i).getFirstChild().cloneNode(true);
            //Check for possible use of fields in the charSpeeds and replace their index with the parameter ones.
            for (int j = 0; j < maxCharSpeed.getLength(); j++) {
                Element speedMath = (Element) doc.importNode(maxCharSpeed.item(j).getFirstChild().cloneNode(true), true);
                //Create the equation with the time slice
                AGDMUtils.replaceFieldVariables(pi, (Element) variable, speedMath);
                speedMaths.appendChild(AGDMUtils.replaceNormals(speedMath, pi.getBaseSpatialCoordinates(), pi.getSpatialCoord1()));
            }
            NodeList speedMathsChildren = speedMaths.getChildNodes();
            if (speedMathsChildren.getLength() == 1) {
                Element speed = (Element) absoluteValue.cloneNode(true);
                speed.appendChild(speedMathsChildren.item(0).getFirstChild());
                speeds.item(i).getParentNode().replaceChild(speed, speeds.item(i));
            }
            else {
                Element apply = AGDMUtils.createElement(doc, mtUri, "apply");
                Element max = AGDMUtils.createElement(doc, mtUri, "max");
                apply.appendChild(max);
                for (int j = 0; j < speedMathsChildren.getLength(); j++) {
                    Element speed = (Element) absoluteValue.cloneNode(true);
                    speed.appendChild(speedMathsChildren.item(j).getFirstChild());
                    apply.appendChild(speed);
                }
                speeds.item(i).getParentNode().replaceChild(apply, speeds.item(i));
            }
        }
    }
    
    /**
     * Deploy the positive or negative characteristic speed depending on the tags from the characteristic decomposition.
     * 
     * @param speeds                The occurrences of the maxCharSpeed tag
     * @param pi                    The process information for the equation
     * @param doc                   The document where the information is
     * @param speedType				Positive or Negative
     * @throws AGDMException        AGDM006 Schema not applicable to this problem
     *                              AGDM007 External error
     *                              
     */
    private static void charSpeedCoordinateDeploy(Document doc, NodeList speeds, ProcessInfo pi, String speedType) throws AGDMException {
        //Select specific characteristic information if available
        Element charDecomp = pi.getCharDecomposition();
        NodeList specificCharDecomp = AGDMUtils.find(charDecomp, ".//mms:characteristicDecomposition[mms:applyingFields/mms:field = '" + pi.getField() + "']");
        if (specificCharDecomp.getLength() > 0) {
        	charDecomp = (Element) specificCharDecomp.item(0);
        } else {
        	NodeList defaultCharDecomp = AGDMUtils.find(charDecomp, ".//mms:characteristicDecomposition[not(mms:applyingFields)]");
            if (defaultCharDecomp.getLength() == 0) {
                throw new AGDMException(AGDMException.AGDM006, 
                        "The schema uses characteristic speed, but field " + pi.getField() + " does not have a valid characteristic information.");
            }
        	charDecomp = (Element) defaultCharDecomp.item(0);
        }
        //Coordinate char decomposition
        String query = ".//mms:eigenSpace[mms:type = '" + speedType + "' and ancestor::" 
                + "mms:coordinateCharacteristicDecomposition[mms:eigenCoordinate = '" + pi.getSpatialCoord1() + "']]/mms:eigenValue";
        NodeList maxCharSpeed = AGDMUtils.find(charDecomp, query);
        if (maxCharSpeed.getLength() == 0) {
            //Angle char decomposition
            query = ".//mms:eigenSpace[mms:type = '" + speedType + "' and ancestor::mms:generalCharacteristicDecomposition]/mms:eigenValue";
            maxCharSpeed = AGDMUtils.find(charDecomp, query);
            if (maxCharSpeed.getLength() == 0) {
                throw new AGDMException(AGDMException.AGDM006, 
                    "The schema uses characteristic speed, the model must have coordinate or general Characteristic decomposition");
            }
        }
        
        //Make the replacements
        for (int i = 0; i < speeds.getLength(); i++) {
            //Store a copy of the speed equations to not overwrite the original ones.
            DocumentFragment speedMaths = doc.createDocumentFragment();
            Element variable = (Element) speeds.item(i).getFirstChild().cloneNode(true);
            //Check for possible use of fields in the charSpeeds and replace their index with the parameter ones.
            for (int j = 0; j < maxCharSpeed.getLength(); j++) {
                Element speedMath = (Element) doc.importNode(maxCharSpeed.item(j).getFirstChild().cloneNode(true), true);
                //Create the equation with the time slice
                AGDMUtils.replaceFieldVariables(pi, (Element) variable, speedMath);
                speedMaths.appendChild(AGDMUtils.replaceNormals(speedMath, pi.getBaseSpatialCoordinates(), pi.getSpatialCoord1()));
            }
            NodeList speedMathsChildren = speedMaths.getChildNodes();
            if (speedMathsChildren.getLength() == 1) {
                Element speed = (Element) speedMathsChildren.item(0).getFirstChild().cloneNode(true);
                speeds.item(i).getParentNode().replaceChild(speed, speeds.item(i));
            }
            else {
                Element apply = AGDMUtils.createElement(doc, mtUri, "apply");
                Element max = AGDMUtils.createElement(doc, mtUri, "max");
                apply.appendChild(max);
                for (int j = 0; j < speedMathsChildren.getLength(); j++) {
                    Element speed = (Element) speedMathsChildren.item(j).getFirstChild().cloneNode(true);
                    apply.appendChild(speed);
                }
                speeds.item(i).getParentNode().replaceChild(apply, speeds.item(i));
            }
        }
    }
    
    /**
     * Process an import element.
     * @param discProblem       The document to generate the document fragment
     * @param problem           The original problem
     * @param impCall           The parameters for the import calling
     * @param pi                The process information for the equation
     * @param parentSchema      The schema that calls the import
     * @param saveFunction		if the function has to be saved in the problem
     * @return					The imported function
     * @throws AGDMException    AGDM007 External error
     */
    public static Element processImport(Document discProblem, Node problem, ImportCall impCall, ProcessInfo pi, Document parentSchema, boolean saveFunction)
        throws AGDMException {
        DocumentFragment fragmentResult = discProblem.createDocumentFragment();
        //Getting the schema for the import
        Document schema = (Document) impCall.getImportDoc().cloneNode(true);
        if (!schema.getDocumentElement().getLocalName().equals("transformationRule")) {
            throw new AGDMException(AGDMException.AGDM006, 
                    "The imported documents must be transformation rules");
        }
        
        RuleInfo newRi = new RuleInfo();
        newRi.setIndividualField(true);
        newRi.setIndividualCoord(true);
        newRi.setFunction(true);
        newRi.setProcessInfo(pi);
        newRi.setAuxiliaryCDAdded(false);
        newRi.setAuxiliaryFluxesAdded(false);
        newRi.setUseBaseCoordinates(false);
        //Process the content of the schema imported and store the result
        Element rule = (Element) schema.getElementsByTagName("sml:simml").item(0);
        NodeList ruleChildren = rule.getChildNodes();
        for (int i = 0; i < ruleChildren.getLength(); i++) {
            fragmentResult.appendChild(discProblem.adoptNode(
                    processExecutionFlowChild(discProblem, problem, (Element) ruleChildren.item(i), newRi)));
        }
        //Function
        Element function = AGDMUtils.createElement(discProblem, mmsUri, "function");
        //Function name
        Element funcName = AGDMUtils.createElement(discProblem, mmsUri, "functionName");
        funcName.setTextContent(impCall.getFunctionName());
        function.appendChild(funcName);
        //Function parameters
        //The parameters are the spatial coordinates of the field
        Element funcParams = AGDMUtils.createElement(discProblem, mmsUri, "functionParameters");
        NodeList funcParamList = schema.getElementsByTagName("mms:functionParameter");
        for (int i = 0; i < funcParamList.getLength(); i++) {
            Element firstChild = (Element) funcParamList.item(i).getFirstChild();
            if (firstChild.getLocalName().equals("name")) {
                funcParams.appendChild(discProblem.importNode(funcParamList.item(i), true));
            }
            else {
                if (firstChild.getTextContent().equals("coordinates")) {
                    ArrayList<String> coords = pi.getCoordinateRelation().getDiscCoords();
                    for (int j = 0; j < coords.size(); j++) {
                        Element funcParam = AGDMUtils.createElement(discProblem, mmsUri, "functionParameter");
                        Element paramName = AGDMUtils.createElement(discProblem, mmsUri, "name");
                        paramName.setTextContent(coords.get(j));
                        Element paramType = AGDMUtils.createElement(discProblem, mmsUri, "type");
                        if (pi.getDiscretizationType() == DiscretizationType.particles) {
                            paramType.setTextContent("real");
                        }
                        else {
                            paramType.setTextContent("int");
                        }
                        funcParam.appendChild(paramName);
                        funcParam.appendChild(paramType);
                        funcParams.appendChild(funcParam);
                    }
                }
            }
        }
        function.appendChild(funcParams);
        //Function instructions
        Element funcInstructions = AGDMUtils.createElement(discProblem, mmsUri, "functionInstructions");
        Element simml = AGDMUtils.createElement(discProblem, smlUri, "simml");
        simml.appendChild(fragmentResult);
        funcInstructions.appendChild(simml);
        function.appendChild(funcInstructions);
        
        if (saveFunction) {
        	//If the function does not exist yet then add it 
            if (AGDMUtils.find(discProblem, "//mms:functionName[text() = '" + impCall.getFunctionName() + "']").getLength() == 0) {
            	discProblem.getElementsByTagName("mms:functions").item(0).appendChild(function);
            }
            //If the function already exists some checking must be considered
            else {
                boolean alreadyExists = false;
                NodeList functionList = AGDMUtils.find(discProblem, 
                        "//mms:function[mms:functionName[starts-with(text(), '" + impCall.getFunctionName() + "')]]");
                for (int i = 0; i < functionList.getLength() && !alreadyExists; i++) {
                    String functionName = functionList.item(i).getFirstChild().getTextContent();
                    Element previousFunction = (Element) functionList.item(i).cloneNode(true);
                    
                    //Change the name before the comparison
                    previousFunction.getFirstChild().setTextContent(impCall.getFunctionName());
                    
                    //A previous function does the same, none additional function is needed
                    if (AGDMUtils.domToString(previousFunction).equals(AGDMUtils.domToString(function))) {
                        //If the name of the existing function is not the import name change the calling function names over the schema. 
                        if (!functionName.equals(impCall.getFunctionName())) {
                            //Create the new element name
                            Element newNameElem = AGDMUtils.createElement(parentSchema, mtUri, "mt:ci");
                            newNameElem.setTextContent(functionName);
                            
                            //Change the name in the function calls
                            NodeList functionCallings = parentSchema.getElementsByTagName("sml:functionCall");
                            for (int j = 0; j < functionCallings.getLength(); j++) {
                                Node name = functionCallings.item(j).getFirstChild();
                                String originalName = AGDMUtils.domToString(name);
                                originalName = originalName.substring(originalName.indexOf("\">") + 2, originalName.indexOf("</mt:ci"));
                                String baseName = impCall.getFunctionName();
                                if (originalName.equals(baseName)) {
                                    functionCallings.item(j).replaceChild(newNameElem.cloneNode(true), name);
                                }
                            }
                        }
                        alreadyExists = true;
                    }
                }
                //The function is a new function
                if (!alreadyExists) {
                    //Use a new name
                    String newName = impCall.getFunctionName() + "_";
                    int lastInt = 0;
                    for (int i = 0; i < functionList.getLength() && !alreadyExists; i++) {
                        String functionName = functionList.item(i).getFirstChild().getTextContent();
                        if (functionName.indexOf("_") > 0) {
                            try {
                                int index = Integer.parseInt(functionName.substring(functionName.lastIndexOf("_") + 1));
                                if (index > lastInt)  {
                                    lastInt = index;
                                }
                            }
                            catch (NumberFormatException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    function.getFirstChild().setTextContent(newName + (lastInt + 1));
                    //Create the new element name
                    Element newNameElem = AGDMUtils.createElement(parentSchema, mtUri, "mt:ci");
                    newNameElem.setTextContent(impCall.getFunctionName() + "_" + (lastInt + 1));
                    
                    //Change the name in the function calls
                    NodeList functionCallings = parentSchema.getElementsByTagName("sml:functionCall");
                    for (int j = 0; j < functionCallings.getLength(); j++) {
                        Node name = functionCallings.item(j).getFirstChild();
                        String originalName = AGDMUtils.domToString(name);
                        originalName = originalName.substring(originalName.indexOf("\">") + 2, originalName.indexOf("</mt:ci"));
                        String baseName = impCall.getFunctionName();
                        if (originalName.equals(baseName)) {
                            functionCallings.item(j).replaceChild(newNameElem.cloneNode(true), name);
                        }
                    }
                    //Add the new function
                    Element functions = (Element) discProblem.getElementsByTagName("mms:functions").item(0);
                    functions.appendChild(function);
                }
            }
        }
        return function;
    }
    /**
     * Process sources tag. Replace the fields used in the sources with the time slice in the rule.
     * @param variable          The variable
     * @param pi                The process information for the equation
     * @return                  The sources processed
     * @throws AGDMException    AGDM007 External error
     */
    private static DocumentFragment processSources(Element variable, ProcessInfo pi) 
        throws AGDMException {
        //The result are stored in a document fragment
        DocumentFragment result = pi.getSource().getOwnerDocument().createDocumentFragment();
        
        boolean isStepVariable = pi.isStepVariable(variable, false);
        //Search for the equations
        Element source = (Element) pi.getSource().cloneNode(true);
        //Create the equation with the time slice
        ArrayList<String> fieldIt = pi.getRegionInfo().getFields();
        for (int i = 0; i < fieldIt.size(); i++) {
            String field = fieldIt.get(i);
            ProcessInfo newPi = new ProcessInfo(pi);
            newPi.setField(field);
            newPi.setSpatialCoord1(null);
            newPi.setSpatialCoord2(null);
            Element fieldVar;
            if (pi.getRegionInfo().isAuxiliaryField(field) && isStepVariable) {
            	fieldVar = replaceTags(AGDMUtils.createTimeSliceWithExternalIndex(pi.getSource().getOwnerDocument(), variable), newPi);
            }
            else {
            	fieldVar = replaceTags(variable.cloneNode(true), newPi);
            }
            AGDMUtils.replaceVariable(source, field, fieldVar);
        }
        result.appendChild(source.getFirstChild());
        return result;
    }
    /**
     * Process particle volume tag. Replace the fields used in the particle volume with the variable template in the rule.
     * @param variable          The variable
     * @param pi                The process information for the equation
     * @return                  The sources processed
     * @throws AGDMException    AGDM007 External error
     */
    private static DocumentFragment processParticleVolume(Element variable, ProcessInfo pi) 
        throws AGDMException {
    	if (pi.getParticleVolume() == null) {
            throw new AGDMException(AGDMException.AGDM006, 
                    "Particle volume must be set in the discretization policy for this problem.");
    	}
        //The result are stored in a document fragment
        DocumentFragment result = pi.getParticleVolume().getOwnerDocument().createDocumentFragment();
        boolean isStepVariable = pi.isStepVariable(variable, false);
        //Search for the equations
        Element volume = (Element) pi.getParticleVolume().cloneNode(true);
        //Create the equation with the time slice
        ArrayList<String> fieldIt = pi.getRegionInfo().getFields();
        ProcessInfo newPi = new ProcessInfo(pi);
        for (int i = 0; i < fieldIt.size(); i++) {
            String field = fieldIt.get(i);
            newPi.setField(field);
            newPi.setSpatialCoord1(null);
            newPi.setSpatialCoord2(null);
            Element fieldVar;
            if (pi.getRegionInfo().isAuxiliaryField(field) && isStepVariable) {
            	fieldVar = ExecutionFlow.replaceTags(AGDMUtils.createTimeSliceWithExternalIndex(pi.getParticleVolume().getOwnerDocument(), variable.cloneNode(true)), newPi);
            }
            else {
            	fieldVar = replaceTags(variable.cloneNode(true), newPi);
            }
            AGDMUtils.replaceVariable(volume, field, fieldVar);
        }
        Iterator<String> variables = pi.getAuxiliaryVariables().iterator();
        while (variables.hasNext()) {
        	String var = variables.next();
            newPi.setField(var);
            //Remove index access if any
            Element template = (Element) variable.cloneNode(true);
            if (template.getLocalName().equals("sharedVariable")) {
            	template = (Element) template.getFirstChild();
            }
            if (template.getLocalName().equals("apply")) {
            	template = (Element) template.getFirstChild();
            }
            Element fieldTime = (Element) ExecutionFlow.replaceTags(template, newPi);
            AGDMUtils.replaceVariable(volume, var, fieldTime);
        }
        volume = replaceTags(volume, pi);
        result.appendChild(volume.getFirstChild());
        return result;
    }
    
    /**
     * Generates the pseudo code to check the finalization conditions.
     * @param discProblem       The problem being discretized
     * @param problem           The original problem
     * @param timeSlice         The element with the slice of time to deploy the equation
     * @param pi                The process information for the equation
     * @return                  The pseudo code to check the finalization conditions
     * @throws AGDMException    AGDM007 External error
     */
    private static DocumentFragment processFinalizationCheck(Document discProblem, Node problem, Element timeSlice, ProcessInfo pi) 
        throws AGDMException {
        //The result are stored in a document fragment
        DocumentFragment result = discProblem.createDocumentFragment();
        
        boolean isStepVariable = pi.isStepVariable(timeSlice, false);
        Element ifTag = AGDMUtils.createElement(discProblem, smlUri, "if");
        result.appendChild(ifTag);
        //Search for the finalization conditions
        NodeList finalizations = AGDMUtils.find(problem, "//mms:finalizationCondition//mt:math");
        Element cond = (Element) finalizations.item(0);
        for (int i = 1; i < finalizations.getLength(); i++) {
            Element aux = (Element) finalizations.item(i).getFirstChild();
            cond = AGDMUtils.combineConditions(aux, cond, "and");
        }
        //Create the conditions with the time slice
        ArrayList<String> fieldIt = pi.getRegionInfo().getFields();
        for (int i = 0; i < fieldIt.size(); i++) {
            String field = fieldIt.get(i);
            ProcessInfo newPi = new ProcessInfo(pi);
            newPi.setField(field);
            newPi.setSpatialCoord1(null);
            newPi.setSpatialCoord2(null);
            Element fieldTime;
            if (pi.getRegionInfo().isAuxiliaryField(field) && isStepVariable) {
            	fieldTime = ExecutionFlow.replaceTags(AGDMUtils.createTimeSliceWithExternalIndex(discProblem, timeSlice), newPi);
            }
            else {
            	fieldTime = replaceTags(timeSlice.cloneNode(true), newPi);
            }
            AGDMUtils.createTimeSlice(cond, field, fieldTime, pi.getRegionInfo(), pi.getCoordinateRelation());
        }
        ifTag.appendChild(discProblem.importNode(cond, true));
        Element then = AGDMUtils.createElement(discProblem, smlUri, "then");
        Element exit = AGDMUtils.createElement(discProblem, smlUri, "exit");
        then.appendChild(exit);
        ifTag.appendChild(then);
        
        return result;
    }
    
   /**
    * Add the characteristic decomposition auxiliary equations if necessary.
    * @param fragmentResult        The fragment to add the equations to
    * @param problem               The problem with the discretization
    * @param rule                  The element to process
    * @param ri                    The rule information
    * @throws AGDMException        AGDM006 Schema not applicable to this problem
    *                              AGDM007 External error
    */
   private static void addCDAuxiliaryEquations(DocumentFragment fragmentResult, Document discProblem, Node problem, Node rule, 
           RuleInfo ri) throws AGDMException {
       Document doc = rule.getOwnerDocument();
       NodeList equation = AGDMUtils.find(problem, "//mms:evolutionEquation");
       HashSet<String> added = new HashSet<String>();
       ProcessInfo pi = ri.getProcessInfo();
       Element origEq = pi.getEquation();
       String origField = pi.getField();
       String origCoord = pi.getSpatialCoord1();
       for (int i = 0; i < equation.getLength(); i++) {
           String field = ((Element) equation.item(i)).getElementsByTagName("mms:field").item(0).getTextContent();
           if (ri.getProcessInfo().getRegionInfo().getFields().contains(field)) {
               //Get the equation
               pi.setEquation((Element) equation.item(i));
               //get field
               pi.setField(field);
               //Get the characteristic decomposition if exists
               NodeList modelId = ((Element) equation.item(i)).getElementsByTagName("mms:modelId");
               //If there is modelId, get the characteristic decomposition
               String charQuery;
               if (modelId.getLength() > 0) {
                   charQuery = "//mms:characteristicDecomposition[mms:modelId= '" + modelId.item(0).getTextContent() + "']";
               }
               //If there is not modelId there is a model and have to get the characteristic decomposition without modelId
               else {
                   charQuery = "//mms:characteristicDecomposition";
               }
               //Select specific characteristic information if available
               Element charDecomp = null;
               String charDecompAdded;
               NodeList specificCharDecomp = AGDMUtils.find(problem, charQuery + "[mms:applyingFields/mms:field = '" + field + "']");
               if (specificCharDecomp.getLength() > 0) {
               		charDecomp = (Element) specificCharDecomp.item(0);
               		charDecompAdded = AGDMUtils.find(charDecomp, "./mms:applyingFields/mms:field").item(0).getTextContent();
               } else {
               		NodeList defaultCharDecomp = AGDMUtils.find(problem, charQuery + "[not(mms:applyingFields)]");
               		if (defaultCharDecomp.getLength() > 0) {
                	 	charDecomp = (Element) defaultCharDecomp.item(0);
              	 	}
               		charDecompAdded = "default";
               }
               if (!added.contains(charDecompAdded) && charDecomp != null) {
                   if (AGDMUtils.find(rule, CHARSPEEDS_TAGS).getLength() > 1) {
                       throw new AGDMException(AGDMException.AGDM002, 
                               "Only one maxCharSpeed and maxCharSpeedCoordinate is allowed within an instruction.");
                   }
                   Element timeSlice = (Element) AGDMUtils.find(rule, CHARSPEEDS_TAGS).item(0).getFirstChild();
                   //General case
                   boolean hasMaxCharSpeed = charDecomp.getElementsByTagName("mms:maxCharacteristicSpeed").getLength() > 0;
                   boolean hasCoordCharDecomp = charDecomp.getElementsByTagName("mms:coordinateCharacteristicDecomposition").getLength() > 0;
                   if ((AGDMUtils.find(rule, ".//sml:maxCharSpeed|.//sml:positiveCharSpeed|.//sml:negativeCharSpeed").getLength() == 1 && !hasMaxCharSpeed && !hasCoordCharDecomp) 
                           || (AGDMUtils.find(rule, ".//sml:maxCharSpeedCoordinate|.//sml:negativeCharSpeedCoordinate|.//sml:positiveCharSpeedCoordinate").getLength() == 1 && !hasCoordCharDecomp)
                           || AGDMUtils.find(timeSlice, SPATIAL_IT_TAGS).getLength() >= 1) {
                   	String coord = pi.getSpatialCoord1();
                   pi.setSpatialCoord1(coord);
                   fragmentResult.appendChild(doc.importNode(AGDMUtils.replaceNormals(AGDMUtils.projectCoordVectors(coord, 
                           charDecomp, timeSlice, pi), pi.getBaseSpatialCoordinates(), coord), true));
                   fragmentResult.appendChild(doc.importNode(AGDMUtils.replaceNormals(AGDMUtils.deployAuxiliaryDefinitions(
                		   discProblem, problem, charDecomp, AGDMUtils.checkTagId(rule), pi, timeSlice, false), 
                           pi.getBaseSpatialCoordinates(), coord), true));
                   }
                   else {
                       fragmentResult.appendChild(doc.importNode(AGDMUtils.deployAuxiliaryDefinitions(discProblem, problem, charDecomp, 
                               AGDMUtils.checkTagId(rule), pi, timeSlice, false), true));
                   }
                   added.add(charDecompAdded);
               }
           }
       }
       pi.setEquation(origEq);
       pi.setField(origField);
       pi.setSpatialCoord1(origCoord);
       ri.setAuxiliaryCDAdded(true);
   }
    
    /**
     * Replace schema step variables by unique variable.
     * @param math				The math where to replace
     * @param stepVariables		The step variables to find
     * @throws AGDMException	AGDM00X - External error
     */
    private static void replaceStepVariables(Node math, ArrayList<Element> stepVariables) throws AGDMException {
    	Node replacement = AGDMUtils.createTimeSlice(math.getOwnerDocument()).getFirstChild();
    	for (int i = 0; i < stepVariables.size(); i++) {
    		Element stepVariable = stepVariables.get(i);
    		AGDMUtils.nodeReplace(math, stepVariable, replacement.cloneNode(true));
    	}
    }
    
    /**
     * Checks if the current boundary tag is the last one.
     * @param rule		Current boundary	
     * @return			True if last
     */
    private static boolean isLastBoundary(Element rule) {
    	Node sibling = rule.getNextSibling();
    	while (sibling != null) {
    		if (sibling.getLocalName().equals("boundary")) {
    			return false;
    		}
    		sibling = rule.getNextSibling();
    	}
    	return true;
    }
    
    /**
     * Checks the consistency of the schema.
     * 1- particleDistance has to be used inside a iterateOverInteractions
     * 2- neighbourParticle has to be used inside a iterateOverInteractions
     * @param schema            The schema
     * @throws AGDMException    AGDM002  XML discretization schema not valid
     *                          AGDM00X  External error
     */
    //TODO reemplazar con schematron
    static void ruleConsistencyCheck(Element schema) throws AGDMException {
        //1- particleDistance has to be used inside a iterateOverInteractions
        if (AGDMUtils.find(schema, 
                ".//sml:particleDistance[not(ancestor::sml:iterateOverInteractions) and not(ancestor::sml:regionInteraction)]").getLength() > 0) {
            throw new AGDMException(AGDMException.AGDM002, 
                    "The tag particleDistance cannot be used outside a iterateOverInteractions or regionInteraction");
        }
        //2- neighbourParticle has to be used inside a iterateOverInteractions
        if (AGDMUtils.find(schema, 
                ".//sml:neighbourParticle[not(ancestor::sml:iterateOverInteractions) and not(ancestor::sml:regionInteraction)]").getLength() > 0) {
            throw new AGDMException(AGDMException.AGDM002, 
                    "The tag neighbourParticle cannot be used outside a iterateOverInteractions or regionInteraction");
        }
    }
}
