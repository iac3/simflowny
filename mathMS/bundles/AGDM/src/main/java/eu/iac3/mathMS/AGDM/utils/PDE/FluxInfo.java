/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.AGDM.utils.PDE;

import java.util.LinkedHashMap;

/**
 * Information of a flux function.
 * @author bminyano
 *
 */
public class FluxInfo {

    String field;
    LinkedHashMap < String, String > params;
    
    /**
     * Constructor.
     * @param field     Original field that generated the flux
     * @param params    The fields used in the flux
     */
    public FluxInfo(String field, LinkedHashMap < String, String > params) {
        this.field = field;
        this.params = params;
    }
    
    public String getField() {
        return field;
    }
    public void setField(String field) {
        this.field = field;
    }
    public LinkedHashMap < String, String > getParams() {
        return params;
    }
    public void setParams(LinkedHashMap < String, String > params) {
        this.params = params;
    }
    
}
