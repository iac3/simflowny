/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.AGDM.utils;

import java.util.HashMap;

public class ConstantInformation {
	HashMap<String, Double> constants;
	
	/**
	 * Constructor.
	 */
	public ConstantInformation() {
		constants = new HashMap<String, Double>(); 
	}
	
	/**
	 * Add a constant checking for name collision.
	 * @param name		Prefered name for the constant.
	 * @param value		Constant value.
	 * @return			The final constant name.
	 */
	public String addConstant(String name, Double value) {
		//Check if constant exists
		if (constants.containsKey(name)) {
			//if there is a name collision, assign another name
			if (Double.compare(constants.get(name), value) != 0) {				
				int counter = 1;
				String newName = name + "_" + counter;
				while (true) {
					if (constants.containsKey(newName)) {
						if (Double.compare(constants.get(newName), value) == 0) {
							return newName;
						}
					}
					else {
						constants.put(newName, value);
						return newName;
					}
					counter++;
					newName = name + "_" + counter;
				}
			}
		}
		else {
			constants.put(name, value);
		}
		return name;
	}

	public HashMap<String, Double> getConstants() {
		return constants;
	}
	
}
