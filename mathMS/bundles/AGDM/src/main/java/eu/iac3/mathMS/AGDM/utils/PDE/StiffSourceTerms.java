/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.AGDM.utils.PDE;

import java.util.ArrayList;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

/**
 * Source terms.
 * @author bminyano
 *
 */
public class StiffSourceTerms {
    private ArrayList<StiffSource> stiffSources;

    /**
     * Constructor.
     */
    public StiffSourceTerms() {
    	stiffSources = new ArrayList<StiffSource>();
    }
    
    /**
     * Copy constructor.
     * @param sourceTerms   The object to copy
     */
    public StiffSourceTerms(StiffSourceTerms sourceTerms) {
    	stiffSources = new ArrayList<StiffSource>();
    	for (StiffSource s : sourceTerms.stiffSources) {
    		stiffSources.add(new StiffSource(s));
    	}
    }
    
    /**
     * Copy constructor.
     * @param sourcesList   The object to copy
     */
    public StiffSourceTerms(ArrayList<StiffSource> sourcesList) {
    	stiffSources = new ArrayList<StiffSource>();
    	for (StiffSource s : sourcesList) {
    		stiffSources.add(new StiffSource(s));
    	}
    }
    
    /**
     * Adds a new source term to the list.
     * @param field         The variable for the source
     * @param formula       The formula of the source term
     */
    public void add(String field, Element formula, Node conditional) {
    	stiffSources.add(new StiffSource(field, formula, conditional));
    }
    
	public ArrayList<StiffSource> getStiffSources() {
		return stiffSources;
	}

    /**
     * Checks if there is a term for a specific field.
     * @param field     The field
     * @return          True if there is a term for the field
     */
    public boolean hasStiffSourcesForField(String field) {
        for (int i = 0; i < stiffSources.size(); i++) {
            if (stiffSources.get(i).getField().equals(field)) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Checks if there is a term for a specific field.
     * @param field     The field
     * @return          True if there is a term for the field
     */
    public StiffSource getStiffSourcesForField(String field) {
        for (int i = 0; i < stiffSources.size(); i++) {
            if (stiffSources.get(i).getField().equals(field)) {
                return stiffSources.get(i);
            }
        }
        return null;
    }
	
	public void setStiffSources(ArrayList<StiffSource> fluxes) {
		this.stiffSources = fluxes;
	}

	@Override
	public String toString() {
		return "StiffSourceTerms [Stiff sources=" + stiffSources + "]";
	}
}
