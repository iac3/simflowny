/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.AGDM.utils.PDE;

import java.util.ArrayList;
import java.util.HashMap;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import eu.iac3.mathMS.AGDM.AGDMException;
import eu.iac3.mathMS.AGDM.AGDMImpl;
import eu.iac3.mathMS.AGDM.processors.SchemaCreator;
import eu.iac3.mathMS.AGDM.utils.AGDMUtils;
import eu.iac3.mathMS.documentManagerPlugin.DMException;
import eu.iac3.mathMS.documentManagerPlugin.DocumentManager;
import eu.iac3.mathMS.documentManagerPlugin.DocumentManagerImpl;
import eu.iac3.mathMS.simflownyUtilsPlugin.SUException;
import eu.iac3.mathMS.simflownyUtilsPlugin.SimflownyUtils;

public class SpatialOperatorDiscretization {
    private int order;
    private String coordCombination;
    private String schemaId;
    private String normalizationId = null;
    private String schemaName;
    private String normalizationName = null;
    private ArrayList<BoundarySchema> boundarySchemas;
    private ArrayList<MainSchemaLimit> mainSchemaLimits;
    private int stencil;
    
    /**
     * Constructor.
     * @param discretization    From an existent discretization
     */
    public SpatialOperatorDiscretization(SpatialOperatorDiscretization discretization) {
        order = discretization.getOrder();
        coordCombination = discretization.getCoordCombination();
        schemaId = discretization.getSchemaId();
        schemaName = discretization.schemaName;
        if (discretization.normalizationId != null) {
        	normalizationId = discretization.normalizationId;
        }
        if (discretization.normalizationName != null) {
        	normalizationName = discretization.normalizationName;
        }
        boundarySchemas = new ArrayList<BoundarySchema>();
        for (int i = 0; i < discretization.getBoundarySchemas().size(); i++) {
            boundarySchemas.add(new BoundarySchema(discretization.getBoundarySchemas().get(i)));
        }
        mainSchemaLimits = new ArrayList<MainSchemaLimit>();
        for (int i = 0; i < discretization.getMainSchemaLimits().size(); i++) {
            mainSchemaLimits.add(new MainSchemaLimit(discretization.getMainSchemaLimits().get(i)));
        }
        stencil = discretization.stencil;
    }
    
    /**
     * Constructor.
     * @param discretization    From an element.
     * @throws AGDMException 	AGDM002 XML discretization schema not valid
     * 							AGDM003  Import discretization schema not exist
     * 							AGDM00X  External error
     */
    public SpatialOperatorDiscretization(Element discretization) throws AGDMException {
        boundarySchemas = new ArrayList<BoundarySchema>();
        order = Integer.valueOf(discretization.getElementsByTagNameNS(AGDMUtils.mmsUri, "derivativeOrder").item(0).getTextContent());
        coordCombination = discretization.getElementsByTagNameNS(AGDMUtils.mmsUri, "coordinateCombination").item(0).getTextContent();
        schemaId = discretization.getElementsByTagNameNS(AGDMUtils.mmsUri, "mainSchema").item(0).getLastChild().getTextContent();        
        schemaName = discretization.getElementsByTagNameNS(AGDMUtils.mmsUri, "mainSchema").item(0).getFirstChild().getTextContent();
        NodeList normalization = discretization.getElementsByTagNameNS(AGDMUtils.mmsUri, "normalizationSchema");
        if (normalization.getLength() > 0) {
        	normalizationId = normalization.item(0).getLastChild().getTextContent();
        	normalizationName = normalization.item(0).getFirstChild().getTextContent();
            if (!AGDMImpl.normalizationSchemaId.contains(normalizationId)) {
            	AGDMImpl.normalizationSchemaId.add(normalizationId);
            }
        }
        NodeList boundSchemas = discretization.getElementsByTagNameNS(AGDMUtils.mmsUri, "boundarySchema");
        for (int i = 0; i < boundSchemas.getLength(); i++) {
            boundarySchemas.add(new BoundarySchema((Element) boundSchemas.item(i)));
        }
        mainSchemaLimits = new ArrayList<MainSchemaLimit>();
        //Getting the operator stencil
        Document schemaDocument;
        try {
            DocumentManager docMan = new DocumentManagerImpl();
            schemaDocument = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(docMan.getDocument(schemaId)));
        } 
        catch (SUException e) {
            throw new AGDMException(AGDMException.AGDM002, "Spatial discretization rule with id " + schemaId + " not valid.");
        }
        catch (DMException e) {
            throw new AGDMException(AGDMException.AGDM003, "Spatial discretization rule  with id " + schemaId + " does not exist.");
        }
        stencil = Integer.valueOf(SchemaCreator.getSchemaStencil(schemaDocument));
    }

    public int getOrder() {
        return order;
    }

    public String getCoordCombination() {
        return coordCombination;
    }

    public String getSchemaId() {
        return schemaId;
    }

    public String getSchemaName() {
        return schemaName;
    }
    
    public ArrayList<BoundarySchema> getBoundarySchemas() {
        return boundarySchemas;
    }

    public ArrayList<MainSchemaLimit> getMainSchemaLimits() {
        return mainSchemaLimits;
    }
    
    public String getNormalizationId() {
		return normalizationId;
	}

	public int getStencil() {
        return stencil;
    }

    public String getNormalizationName() {
		return normalizationName;
	}

	@Override
    public String toString() {
        return "SpatialOperatorDiscretization [order=" + order
                + ", coordCombination=" + coordCombination + ", schemaId="
                + schemaId + ", schemaName="
                + schemaName + ", normalizationId="
                + normalizationId + ", normalizationName="
                + normalizationName + ", boundarySchemas=" + boundarySchemas + "]";
    }
    
    /**
     * Maps the axis indices from the boundary conditions to problem coordinates.
     * @param coordinates   The problem coordinates
     */
    public void mapCoordinatesToIndices(ArrayList<String> coordinates) {
        HashMap<String, String> indexCoordMap = new HashMap<String, String>();
        int coordIndex = 0;
        String newCoordComb = "";
        for (int i = 0; i < coordCombination.length(); i++) {
            String index = coordCombination.substring(i, i + 1);
            if (!indexCoordMap.containsKey(index)) {
                indexCoordMap.put(index, coordinates.get(coordIndex));
                coordIndex++;
            }
            newCoordComb = newCoordComb + indexCoordMap.get(index);
        }
        
        coordCombination = newCoordComb;
        
        for (int i = 0; i < boundarySchemas.size(); i++) {
            ArrayList<BoundarySchemaCondition> conditions = boundarySchemas.get(i).getConditions();
            for (int j = 0; j < conditions.size(); j++) {
                conditions.get(j).setAxis(indexCoordMap.get(conditions.get(j).getAxis()));
            }
        }
    }
    
    /**
     * Infers the schema limits from the boundary schema conditions.
     */
    public void inferSchemaLimits() {
        mainSchemaLimits = new ArrayList<MainSchemaLimit>();
        for (int i = 0; i < boundarySchemas.size(); i++) {
            ArrayList<BoundarySchemaCondition> bsc = boundarySchemas.get(i).getConditions();
            for (int j = 0; j < bsc.size(); j++) {
                MainSchemaLimit msl = new MainSchemaLimit(bsc.get(j));
                if (!mainSchemaLimits.contains(msl)) {
                    mainSchemaLimits.add(msl);
                }
                else {
                    int schemaIndex = mainSchemaLimits.indexOf(msl);
                    MainSchemaLimit existent = mainSchemaLimits.get(schemaIndex);
                    if (existent.getDistanceToBoundary() < msl.getDistanceToBoundary()) {
                        mainSchemaLimits.set(schemaIndex, msl);
                    }
                }
            }
        }
    }
    
    /**
     * Checks equality.
     * @param so	object to check to
     * @return		true if equals
     */
    public boolean equals(SpatialOperatorDiscretization so) {
        return so.getCoordCombination().equals(this.coordCombination) && so.getOrder() == this.order && so.getSchemaId().equals(this.schemaId)
                && so.getStencil() == this.stencil
                && so.getBoundarySchemas().equals(this.boundarySchemas)
                && so.getMainSchemaLimits().equals(this.mainSchemaLimits);
        
           }
}