/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.AGDM.utils.PDE;

import java.util.ArrayList;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class FluxTerms {
    private ArrayList<Flux> fluxes;

    /**
     * Constructor.
     */
    public FluxTerms() {
    	fluxes = new ArrayList<Flux>();
    }
    
    /**
     * Copy constructor.
     * @param ft   The object to copy
     */
    public FluxTerms(FluxTerms ft) {
    	fluxes = new ArrayList<Flux>();
    	for (Flux f : ft.fluxes) {
    		fluxes.add(new Flux(f));
    	}
    }
    
    /**
     * Copy constructor.
     * @param fluxList   The object to copy
     */
    public FluxTerms(ArrayList<Flux> fluxList) {
    	fluxes = new ArrayList<Flux>();
    	for (Flux f : fluxList) {
    		fluxes.add(new Flux(f));
    	}
    }
    
    /**
     * Adds a new fluxes to the list.
     * @param field         The variable for the flux
     * @param coord         The coordinate of the flux term
     * @param math			Flux mathematics 
     */
    public void add(String field, String coord, Element math, Node conditional) {
    	fluxes.add(new Flux(field, coord, math, conditional));
    }
    
	public ArrayList<Flux> getFluxes() {
		return fluxes;
	}
   
    /**
     * Gets the fluxes for a given field.
     * @param field     The field
     * @return          The fluxes of the field
     */
    public ArrayList<Flux> getFilteredFluxTerms(String field) {
        ArrayList<Flux> filteredFluxes = new ArrayList<Flux>();
        for (int i = 0; i < fluxes.size(); i++) {
            if (fluxes.get(i).getField().equals(field)) {
            	filteredFluxes.add(new Flux(fluxes.get(i)));
            }
        }
        return filteredFluxes;
    }

	@Override
	public String toString() {
		return "FluxTerms [fluxes=" + fluxes + "]";
	}
    
}
