/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.AGDM.utils.PDE;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Normalization information.
 * @author bminyano
 *
 */
public class NormalizationLevel {
    private HashMap<String, ArrayList<String>> coordVariablesMap;
    private HashMap<String, ArrayList<String>> coordSchemaIdMap;
    private HashMap<String, ArrayList<String>> coordSchemaNameMap;
    
    /**
     * Constructor.
     */
    public NormalizationLevel() {
    	coordVariablesMap = new HashMap<String, ArrayList<String>>();
    	coordSchemaIdMap = new HashMap<String, ArrayList<String>>();
    	coordSchemaNameMap = new HashMap<String, ArrayList<String>>();
    }
    
    /**
     * Constructor with fields.
     * @param coordVariablesMap		input
     * @param coordSchemaIdMap 		input
     * @param coordSchemaNameMap 	input
     */
	public NormalizationLevel(HashMap<String, ArrayList<String>> coordVariablesMap, HashMap<String, ArrayList<String>> coordSchemaIdMap,
			HashMap<String, ArrayList<String>> coordSchemaNameMap) {
		super();
		this.coordVariablesMap = coordVariablesMap;
		this.coordSchemaIdMap = coordSchemaIdMap;
		this.coordSchemaNameMap = coordSchemaNameMap;
	}

	public HashMap<String, ArrayList<String>> getCoordVariablesMap() {
		return coordVariablesMap;
	}

	public void setCoordVariablesMap(HashMap<String, ArrayList<String>> coordVariablesMap) {
		this.coordVariablesMap = coordVariablesMap;
	}

	public HashMap<String, ArrayList<String>> getCoordSchemaIdMap() {
		return coordSchemaIdMap;
	}

	public void setCoordSchemaIdMap(HashMap<String, ArrayList<String>> coordSchemaIdMap) {
		this.coordSchemaIdMap = coordSchemaIdMap;
	}

	public HashMap<String, ArrayList<String>> getCoordSchemaNameMap() {
		return coordSchemaNameMap;
	}

	public void setCoordSchemaNameMap(HashMap<String, ArrayList<String>> coordSchemaNameMap) {
		this.coordSchemaNameMap = coordSchemaNameMap;
	}

	@Override
	public String toString() {
		return "NormalizationLevel [coordVariablesMap=" + coordVariablesMap
				+ ", coordSchemaIdMap=" + coordSchemaIdMap + ", coordSchemaNameMap=" + coordSchemaNameMap + "]";
	}

}
