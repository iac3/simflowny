/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.AGDM.utils.PDE;

import java.util.ArrayList;
import java.util.Optional;

import org.w3c.dom.Node;

import eu.iac3.mathMS.AGDM.AGDMException;
import eu.iac3.mathMS.AGDM.utils.AGDMUtils;

public class MultiplicationTerm implements Comparable<Object> {
    String field;
    String id;
    ArrayList<String> variables;
    Node algebraicExpression;
    OperatorInfoType operatorType;
    Node conditional;
    
    static short DIFFERENT = -1;
    static short EQUIVALENT = 0;
    static short CONDITIONAL = 1;
    
    /**
     * Constructor.
     * @param field         The field which the multiplication term belongs to
     * @param id            The multiplication id
     * @param variables     The variables used in the multiplication
     * @param expression    The algebraic expression that multiplies the variables
     * @param opType        The operator type
     */
    MultiplicationTerm(String field, String id, ArrayList<String> variables, Node expression, String opType, Node conditional) {
        this.field = field;
        this.id = id;
        this.variables = variables;
        algebraicExpression = expression;
        switch(opType) {
        	case "evolutionEquation":
        		this.operatorType = OperatorInfoType.field;
        		break;
        	case "auxiliaryField":
        		this.operatorType = OperatorInfoType.auxiliaryField;
        		break;
        	case "analysisFieldEquation":
        		this.operatorType = OperatorInfoType.analysisField;
        		break;
        	case "auxiliaryAnalysisVariableEquation":
        	case "auxiliaryVariableEquation":
        		this.operatorType = OperatorInfoType.auxiliaryVariable;
        		break;
        	case "projection":
        		this.operatorType = OperatorInfoType.interaction;
        		break;
        }
        if (conditional != null) {
        	this.conditional = conditional.cloneNode(true);
        }
        else {
        	this.conditional = null;
        }
    }

    /**
     * Copy constructor.
     * @param mt    The object to copy
     */
    MultiplicationTerm(MultiplicationTerm mt) {
        this.field = mt.field;
        this.id = mt.id;
        this.variables = new ArrayList<String>(mt.variables);
    	if (mt.algebraicExpression != null) {
    		this.algebraicExpression = mt.algebraicExpression.cloneNode(true);
    	}
    	if (mt.conditional != null) {
    		this.conditional = mt.conditional.cloneNode(true);
    	}
        this.operatorType = mt.operatorType;
    }
    
    public String getId() {
        return id;
    }

    public ArrayList<String> getVariables() {
        return variables;
    }

    public Node getAlgebraicExpression() {
        return algebraicExpression;
    }
    
    public OperatorInfoType getOperatorType() {
        return operatorType;
    }

    public String getField() {
        return field;
    }
    
    public Node getConditional() {
		if (conditional == null) {
			return null;
		}
		return conditional.cloneNode(true);
	}

	public void setConditional(Node conditional) {
		this.conditional = conditional;
	}

	@Override
    public String toString() {
        Optional<Node> op = Optional.ofNullable(algebraicExpression);
        String result = op.map( a -> {
			try {
				return AGDMUtils.domToString(a);
			} 
			catch (AGDMException e) {
				return "empty";
			}
		}).orElse("empty");
        op = Optional.ofNullable(conditional);
        String condition = op.map( a -> {
			try {
				return AGDMUtils.domToString(a);
			} 
			catch (AGDMException e) {
				return "empty";
			}
		}).orElse("empty");
        return "MultiplicationTerm [field=" + field + ", id=" + id + ", variables=" + variables
                + ", algebraicExpression=" + result + ", conditionalExpression=" + condition + ", operatorType=" + operatorType + "]\n";
    }

    /**
     * Compare multiplication terms. The only relevant fields is when the object is from auxiliary equation or not.
     * Used for sorting the multilplication terms. The auxiliary terms must be the first, field seconds and interaction lasts.
     * @param o     The term to compare to.
     * @return      The comparison result
     */
    public int compareTo(Object o) {
        if (this.getOperatorType().equals(OperatorInfoType.auxiliaryVariable)) {
            return -1;
        }
        else {
            if (((MultiplicationTerm) o).getOperatorType().equals(OperatorInfoType.auxiliaryVariable)) {
                return 1;
            }
            else {
                if (this.getOperatorType().equals(OperatorInfoType.field)) {
                    return -1;
                }
                else {
                    if (((MultiplicationTerm) o).getOperatorType().equals(OperatorInfoType.field)) {
                        return 1;
                    }
                }
            }
        }
        if (this.getOperatorType().equals(OperatorInfoType.auxiliaryVariable)) {
            return -1;
        }
        else {
            if (((MultiplicationTerm) o).getOperatorType().equals(OperatorInfoType.auxiliaryVariable)) {
                return 1;
            }
            else {
                if (this.getOperatorType().equals(OperatorInfoType.analysisField)) {
                    return -1;
                }
                else {
                    if (((MultiplicationTerm) o).getOperatorType().equals(OperatorInfoType.analysisField)) {
                        return 1;
                    }
                }
            }
        }
        return 0;
    }
    
    /**
     * Checks if two objects are equivalent taking care of conditional.
     * @param mt    The object to compare to
     * @return      DIFFERENT are not equivalent at all
     * 				EQUIVALENT are completely equivalent
     * 				CONDITIONAL are equivalent, but only one of them has a conditional
     */
    public int getEquivalence(MultiplicationTerm mt) {
        if ((this.algebraicExpression != null && mt.getAlgebraicExpression() == null) 
                || (this.algebraicExpression == null && mt.getAlgebraicExpression() != null)) {
            return DIFFERENT;
        }
        if (this.variables.equals(mt.getVariables()) && ((this.algebraicExpression == null && mt.getAlgebraicExpression() == null) 
                || this.algebraicExpression.isEqualNode(mt.getAlgebraicExpression()))) {
        	if ((this.conditional != null && mt.getConditional() == null) || (this.conditional == null && mt.getConditional() != null)) {
        		return CONDITIONAL;
        	}
        	return EQUIVALENT;
        }
        return DIFFERENT;
    }
}
