/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.AGDM.utils;

import org.w3c.dom.Document;

/**
 * Class for a import call. Contains the document and the parameter values.
 * @author bminyano
 *
 */
public class ImportCall {
    Document importDoc;
    String functionName;
    
    /**
     * Constructor.
     * @param doc               Initial importDocument
     * @param functionName      The name of the function if the import is an auxiliary
     */
    public ImportCall(Document doc, String functionName) {
        importDoc = doc;
        this.functionName = functionName;
    }
    public Document getImportDoc() {
        return importDoc;
    }

    public void setImportDoc(Document doc) {
        importDoc = doc;
    }

    public String getFunctionName() {
        return functionName;
    }
    public void setFunctionName(String functionName) {
        this.functionName = functionName;
    }
    
}
