/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.AGDM.processors;

import eu.iac3.mathMS.AGDM.AGDMException;
import eu.iac3.mathMS.AGDM.utils.AGDMUtils;
import eu.iac3.mathMS.AGDM.utils.DiscretizationType;
import eu.iac3.mathMS.AGDM.utils.RuleInfo;
import eu.iac3.mathMS.AGDM.utils.PDE.AlgebraicSimplifications;
import eu.iac3.mathMS.AGDM.utils.PDE.OperatorInfoType;
import eu.iac3.mathMS.AGDM.utils.PDE.OperatorsInfo;
import eu.iac3.mathMS.AGDM.utils.PDE.ProcessInfo;

import java.util.ArrayList;

import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Class that provides the methods controlling region interactions.
 * @author bminano
 *
 */
public final class RegionInteraction {
    static final int THREE = 3;
    static final int SIX = 6;
    static String mtUri = "http://www.w3.org/1998/Math/MathML";
    static String smlUri = "urn:simml";
    
    /**
     * Constructor.
     */
    public RegionInteraction() {
    }
    
    /**
     * Process the regionInteractionTag.
     * @param discProblem       The discrete problem
     * @param problem           The problem with the original information 
     * @param rule              The element to process
     * @param ri                The rule information
     * @return                  The rule processed
     * @throws AGDMException    AGDM007 External error
     */
    public static DocumentFragment processRegionInteraction(Document discProblem, Node problem, Element rule, RuleInfo ri,
    		int level, OperatorsInfo opInfo, AlgebraicSimplifications simplifications, Element positionElement) throws AGDMException {
        Document elemDoc = rule.getOwnerDocument();
        DocumentFragment fragmentResult = elemDoc.createDocumentFragment();
        
        
        ArrayList<String> combinations = RegionInteraction.createInteractionCombinations(ri.getProcessInfo().getBaseSpatialCoordinates());
        NodeList interactions = AGDMUtils.find(problem, ".//mms:eigenVectorInteraction|.//mms:fieldInteraction|.//mms:interaction");
        if (interactions.getLength() > 0) {
	        //Calculate the normal
	        fragmentResult.appendChild(calculateNormal(discProblem, elemDoc, interactions, ri.getProcessInfo()));
	        for (int i = 0; i < interactions.getLength(); i++) {
	            String type = interactions.item(i).getLocalName();
	            //Enter if the interaction has to be performed
	            Element ifElem = AGDMUtils.createElement(elemDoc, smlUri, "if");
	            Element then = AGDMUtils.createElement(elemDoc, smlUri, "then");
	            //Initialization of the interaction indicator
	            Element indicator = AGDMUtils.createElement(elemDoc, mtUri, "math");
	            Element apply = AGDMUtils.createElement(elemDoc, mtUri, "apply");
	            Element eq = AGDMUtils.createElement(elemDoc, mtUri, "eq");
	            Element ci = AGDMUtils.createElement(elemDoc, mtUri, "ci");
	            ci.setTextContent("interaction_index");
	            Element cnIndicator = AGDMUtils.createElement(elemDoc, mtUri, "cn");
	            cnIndicator.setTextContent(String.valueOf(i + 1));
	            indicator.appendChild(apply);
	            apply.appendChild(eq);
	            apply.appendChild(ci);
	            apply.appendChild(cnIndicator);
	            ifElem.appendChild(indicator);
	            ifElem.appendChild(then);
	            if (type.equals("interaction")) {
	            	then.appendChild(elemDoc.importNode(
	                    processPDEInteraction(discProblem, problem, rule, ri.getProcessInfo(), opInfo, level, simplifications), true));
	            }
	            if (type.equals("fieldInteraction")) {
	                then.appendChild(
	                        processFieldInteraction(discProblem, problem, rule, ri.getProcessInfo(), combinations, interactions.item(i)));
	            }
	            if (type.equals("eigenVectorInteraction")) {
	                then.appendChild(
	                        processEigenVectorInteraction(discProblem, problem, rule, ri.getProcessInfo(), combinations, interactions.item(i)));
	            }
	            fragmentResult.appendChild(ifElem);
	        }
        }
        return fragmentResult;
    }
        
    /**
     * Calculates the normal components of the cell for FVM.
     * @param problem           The problem
     * @param rule              The rule
     * @param interactions      The interactions
     * @param pi                The process information
     * @return                  The instructions
     * @throws AGDMException    AGDM00X  External error
     */
    private static DocumentFragment calculateNormal(Document problem, Document rule, NodeList interactions, ProcessInfo pi) throws AGDMException {
        ArrayList<String> combinations = RegionInteraction.createInteractionCombinations(pi.getBaseSpatialCoordinates());
        DocumentFragment result = rule.createDocumentFragment();
        ArrayList<String> coords = pi.getBaseSpatialCoordinates();
        Element zeroCondition = null;
        Element notZeroCondition = null;
        //Initialization of normal variables
        for (int i = 0; i < coords.size(); i++) {
            Element normal = AGDMUtils.createElement(rule, mtUri, "math");
            Element apply = AGDMUtils.createElement(rule, mtUri, "apply");
            Element eq = AGDMUtils.createElement(rule, mtUri, "eq");
            Element ci = AGDMUtils.createElement(rule, mtUri, "ci");
            ci.setTextContent("n_" + coords.get(i));
            Element cn = AGDMUtils.createElement(rule, mtUri, "cn");
            cn.setTextContent("0");
            apply.appendChild(eq);
            apply.appendChild(ci);
            apply.appendChild(cn);
            normal.appendChild(apply.cloneNode(true));
            result.appendChild(normal);
            //Creation of the condition for normal to be zero
            if (interactions.getLength() > 1) {
                if (i == 0) {
                    zeroCondition = (Element) apply.cloneNode(true);
                }
                else {
                    Element and = AGDMUtils.createElement(rule, mtUri, "and");
                    Element apply2 = AGDMUtils.createElement(rule, mtUri, "apply");
                    Element tmp = zeroCondition;
                    apply2.appendChild(and);
                    apply2.appendChild(tmp);
                    apply2.appendChild(apply.cloneNode(true));
                    zeroCondition = (Element) apply2.cloneNode(true);
                }
            }
            if (i == 0) {
                Element neq = AGDMUtils.createElement(rule, mtUri, "neq");
                apply.replaceChild(neq, eq);
                notZeroCondition = (Element) apply.cloneNode(true);
            }
            else {
                Element neq = AGDMUtils.createElement(rule, mtUri, "neq");
                apply.replaceChild(neq, eq);
                Element or = AGDMUtils.createElement(rule, mtUri, "or");
                Element apply2 = AGDMUtils.createElement(rule, mtUri, "apply");
                Element tmp = notZeroCondition;
                apply2.appendChild(or);
                apply2.appendChild(tmp);
                apply2.appendChild(apply.cloneNode(true));
                notZeroCondition = apply2;
            }
        }
        //Initialization of the interaction indicator
        Element indicator = AGDMUtils.createElement(rule, mtUri, "math");
        Element apply = AGDMUtils.createElement(rule, mtUri, "apply");
        Element eq = AGDMUtils.createElement(rule, mtUri, "eq");
        Element ci = AGDMUtils.createElement(rule, mtUri, "ci");
        ci.setTextContent("interaction_index");
        Element cnIndicator = AGDMUtils.createElement(rule, mtUri, "cn");
        cnIndicator.setTextContent("0");
        indicator.appendChild(apply);
        apply.appendChild(eq);
        apply.appendChild(ci);
        apply.appendChild(cnIndicator);
        result.appendChild(indicator.cloneNode(true));
        //Create normal depending on the neighbours for all the interactions
        for (int i = 0; i < interactions.getLength(); i++) {
            //Set indicator
            cnIndicator.setTextContent(String.valueOf(i + 1));
            //Simple detection, only perpendicular
            Element then = null;
            if (interactions.getLength() > 1) {
                Element ifElem = AGDMUtils.createElement(rule, smlUri, "if");
                Element math = AGDMUtils.createElement(rule, mtUri, "math");
                then = AGDMUtils.createElement(rule, smlUri, "then");
                ifElem.appendChild(math);
                ifElem.appendChild(then);
                math.appendChild(zeroCondition.cloneNode(true));
                result.appendChild(ifElem);
            }
            NodeList targetRegions = interactions.item(i).getFirstChild().getChildNodes();
            for (int j = 0; j < coords.size(); j++) {
                Element ifPos = AGDMUtils.createElement(rule, smlUri, "if");
                Element mathPos = AGDMUtils.createElement(rule, mtUri, "math");
                Element thenPos = AGDMUtils.createElement(rule, smlUri, "then");
                ifPos.appendChild(mathPos);
                ifPos.appendChild(thenPos);
                mathPos.appendChild(createSimpleCondition(problem, rule, pi, targetRegions, coords.get(j), "plus"));
                thenPos.appendChild(indicator.cloneNode(true));
                //Set normal component
                Element normal = AGDMUtils.createElement(rule, mtUri, "math");
                Element apply2 = AGDMUtils.createElement(rule, mtUri, "apply");
                Element eq2 = AGDMUtils.createElement(rule, mtUri, "eq");
                Element ci2 = AGDMUtils.createElement(rule, mtUri, "ci");
                ci2.setTextContent("n_" + coords.get(j));
                Element apply3 = AGDMUtils.createElement(rule, mtUri, "apply");
                Element plus = AGDMUtils.createElement(rule, mtUri, "plus");
                Element applyDelta = AGDMUtils.createElement(rule, mtUri, "apply");
                Element divide = AGDMUtils.createElement(rule, mtUri, "divide");
                Element one = AGDMUtils.createElement(rule, mtUri, "cn");
                one.setTextContent("1");
                Element delta = AGDMUtils.createElement(rule, mtUri, "ci");
                delta.setTextContent("\u0394" + coords.get(j));
                applyDelta.appendChild(divide);
                applyDelta.appendChild(one);
                applyDelta.appendChild(delta);
                normal.appendChild(apply2);
                apply2.appendChild(eq2);
                apply2.appendChild(ci2);
                apply2.appendChild(apply3);
                apply3.appendChild(plus);
                apply3.appendChild(ci2.cloneNode(true));
                apply3.appendChild(applyDelta);
                thenPos.appendChild(normal.cloneNode(true));
                if (interactions.getLength() > 1) {
                    then.appendChild(ifPos);
                }
                else {
                    result.appendChild(ifPos);
                }
                //Negative
                Element ifNeg = AGDMUtils.createElement(rule, smlUri, "if");
                Element mathNeg = AGDMUtils.createElement(rule, mtUri, "math");
                Element thenNeg = AGDMUtils.createElement(rule, smlUri, "then");
                ifNeg.appendChild(mathNeg);
                ifNeg.appendChild(thenNeg);
                mathNeg.appendChild(createSimpleCondition(problem, rule, pi, targetRegions, coords.get(j), "minus"));
                thenNeg.appendChild(indicator.cloneNode(true));
                //Set normal component
                Element minus = AGDMUtils.createElement(rule, mtUri, "minus");
                apply3.replaceChild(minus, plus);
                thenNeg.appendChild(normal.cloneNode(true));
                if (interactions.getLength() > 1) {
                    then.appendChild(ifNeg);
                }
                else {
                    result.appendChild(ifNeg);
                }   
            }
            //Complex detection, with diagonal
            then = null;
            if (interactions.getLength() > 1) {
                Element ifElem = AGDMUtils.createElement(rule, smlUri, "if");
                Element math = AGDMUtils.createElement(rule, mtUri, "math");
                then = AGDMUtils.createElement(rule, smlUri, "then");
                ifElem.appendChild(math);
                ifElem.appendChild(then);
                math.appendChild(zeroCondition.cloneNode(true));
                result.appendChild(ifElem);
            }
            targetRegions = interactions.item(i).getFirstChild().getChildNodes();
            for (int j = 0; j < coords.size(); j++) {
                //Positive
                Element ifPos = AGDMUtils.createElement(rule, smlUri, "if");
                Element mathPos = AGDMUtils.createElement(rule, mtUri, "math");
                Element thenPos = AGDMUtils.createElement(rule, smlUri, "then");
                ifPos.appendChild(mathPos);
                ifPos.appendChild(thenPos);
                mathPos.appendChild(createNeighbourCondition(problem, rule, pi, targetRegions, filterCombs(combinations, j, "+")));
                thenPos.appendChild(indicator.cloneNode(true));
                //Set normal component
                Element normal = AGDMUtils.createElement(rule, mtUri, "math");
                Element apply2 = AGDMUtils.createElement(rule, mtUri, "apply");
                Element eq2 = AGDMUtils.createElement(rule, mtUri, "eq");
                Element ci2 = AGDMUtils.createElement(rule, mtUri, "ci");
                ci2.setTextContent("n_" + coords.get(j));
                Element apply3 = AGDMUtils.createElement(rule, mtUri, "apply");
                Element plus = AGDMUtils.createElement(rule, mtUri, "plus");                
                Element applyDelta = AGDMUtils.createElement(rule, mtUri, "apply");
                Element divide = AGDMUtils.createElement(rule, mtUri, "divide");
                Element one = AGDMUtils.createElement(rule, mtUri, "cn");
                one.setTextContent("1");
                Element delta = AGDMUtils.createElement(rule, mtUri, "ci");
                delta.setTextContent("\u0394" + coords.get(j));
                applyDelta.appendChild(divide);
                applyDelta.appendChild(one);
                applyDelta.appendChild(delta);
                normal.appendChild(apply2);
                apply2.appendChild(eq2);
                apply2.appendChild(ci2);
                apply2.appendChild(apply3);
                apply3.appendChild(plus);
                apply3.appendChild(ci2.cloneNode(true));
                apply3.appendChild(applyDelta);
                thenPos.appendChild(normal.cloneNode(true));
                if (interactions.getLength() > 1) {
                    then.appendChild(ifPos);
                }
                else {
                    result.appendChild(ifPos);
                }
                //Negative
                Element ifNeg = AGDMUtils.createElement(rule, smlUri, "if");
                Element mathNeg = AGDMUtils.createElement(rule, mtUri, "math");
                Element thenNeg = AGDMUtils.createElement(rule, smlUri, "then");
                ifNeg.appendChild(mathNeg);
                ifNeg.appendChild(thenNeg);
                mathNeg.appendChild(createNeighbourCondition(problem, rule, pi, targetRegions, filterCombs(combinations, j, "-")));
                thenNeg.appendChild(indicator.cloneNode(true));
                //Set normal component
                Element minus = AGDMUtils.createElement(rule, mtUri, "minus");
                apply3.replaceChild(minus, plus);
                thenNeg.appendChild(normal.cloneNode(true));
                if (interactions.getLength() > 1) {
                    then.appendChild(ifNeg);
                }
                else {
                    result.appendChild(ifNeg);
                }
            }
        }
        //Normal normalization
        //Condition
        Element ifElem = AGDMUtils.createElement(rule, smlUri, "if");
        Element math = AGDMUtils.createElement(rule, mtUri, "math");
        Element then = AGDMUtils.createElement(rule, smlUri, "then");
        ifElem.appendChild(math);
        ifElem.appendChild(then);
        math.appendChild(notZeroCondition.cloneNode(true));
        //Modulus calculation
        math = AGDMUtils.createElement(rule, mtUri, "math");
        Element eQapply = AGDMUtils.createElement(rule, mtUri, "apply");
        math.appendChild(eQapply);
        eq = AGDMUtils.createElement(rule, mtUri, "eq");
        ci = AGDMUtils.createElement(rule, mtUri, "ci");
        ci.setTextContent("mod_normal");
        eQapply.appendChild(eq);
        eQapply.appendChild(ci);
        apply = AGDMUtils.createElement(rule, mtUri, "apply");
        Element sqrt = AGDMUtils.createElement(rule, mtUri, "root");
        apply.appendChild(sqrt);
        eQapply.appendChild(apply);
        Element mod = null;
        for (int i = 0; i < coords.size(); i++) {
            Element apply2 = AGDMUtils.createElement(rule, mtUri, "apply");
            Element power = AGDMUtils.createElement(rule, mtUri, "power");
            ci = AGDMUtils.createElement(rule, mtUri, "ci");
            ci.setTextContent("n_" + coords.get(i));
            Element cn = AGDMUtils.createElement(rule, mtUri, "cn");
            cn.setTextContent("2");
            apply2.appendChild(power);
            apply2.appendChild(ci);
            apply2.appendChild(cn);
            if (i == 0) {
                mod = (Element) apply2.cloneNode(true);
            }
            else {
                Element plus = AGDMUtils.createElement(rule, mtUri, "plus");
                Element apply3 = AGDMUtils.createElement(rule, mtUri, "apply");
                Element tmp = mod;
                apply3.appendChild(plus);
                apply3.appendChild(tmp);
                apply3.appendChild(apply2.cloneNode(true));
                mod = (Element) apply3.cloneNode(true);
            }
        }
        apply.appendChild(mod);
        then.appendChild(math);
        //Normal normalization
        for (int i = 0; i < coords.size(); i++) {
            math = AGDMUtils.createElement(rule, mtUri, "math");
            apply = AGDMUtils.createElement(rule, mtUri, "apply");
            eq = AGDMUtils.createElement(rule, mtUri, "eq");
            ci = AGDMUtils.createElement(rule, mtUri, "ci");
            ci.setTextContent("n_" + coords.get(i));
            math.appendChild(apply);
            apply.appendChild(eq);
            apply.appendChild(ci);
            Element apply2 = AGDMUtils.createElement(rule, mtUri, "apply");
            Element divide = AGDMUtils.createElement(rule, mtUri, "divide");
            apply2.appendChild(divide);
            apply2.appendChild(ci.cloneNode(true));
            Element ci2 = AGDMUtils.createElement(rule, mtUri, "ci");
            ci2.setTextContent("mod_normal");
            apply2.appendChild(ci2);
            apply.appendChild(apply2);
            then.appendChild(math.cloneNode(true));
        }
        result.appendChild(ifElem);
        
        return result;
    }
  
    /**
     * Process the field interaction between regions.
     * @param discProblem           The problem with the discretization
     * @param problem               The problem
     * @param rule                  The rule
     * @param pi                    The problem information
     * @param combinations          The combinations of the interactions
     * @param interaction           The interaction base information
     * @return                      The instructions
     * @throws AGDMException        AGDM002 XML discretization schema not valid
     *                              AGDM00X  External error
     */
    private static DocumentFragment processFieldInteraction(Document discProblem, Node problem, Element rule, ProcessInfo pi, 
            ArrayList<String> combinations, Node interaction) throws AGDMException {
        NodeList spatialIteration = AGDMUtils.find(rule.getLastChild().getFirstChild(), ExecutionFlow.SPATIAL_IT_TAGS);
        //Time slice with spatial tags
        if (spatialIteration.getLength() > 0) {
            throw new AGDMException(AGDMException.AGDM002, "The region interaction time slices must not contain spatial tags");
        }
        else {
            //Particles
            if (pi.getDiscretizationType() == DiscretizationType.particles) {
                return createFieldInteraction(problem, interaction, rule.getLastChild().getPreviousSibling(), rule.getLastChild(), 
                		pi);
            }
            //FVM
            else {
                return createFieldInteraction(problem, interaction, rule.getFirstChild(), rule.getLastChild(), pi);
            }
        }
    }
    
    /**
     * Creates the interaction for PDE.
     * @param discProblem       The discrete problem being generated
     * @param problem           The problem
     * @param rule              The interaction rule
     * @param pi                The process information
     * @param opInfo            The operators information
     * @param level             The current level for the operators info
     * @param simplifications	Algebraic simplifications
     * @return                  The code
     * @throws AGDMException    AGDM00X External error
     */
    private static DocumentFragment processPDEInteraction(Document discProblem, Node problem, Element rule, ProcessInfo pi, 
    		OperatorsInfo opInfo, int level, AlgebraicSimplifications simplifications) throws AGDMException {
        Element variable = (Element) rule.getFirstChild();
        Element timeslice = (Element) rule.getLastChild();
                
        //Result is stored in document fragment
        DocumentFragment fragmentResult = discProblem.createDocumentFragment();

        //Process terms
        //Process last level derivatives
        Element derivatives = pi.getSchemaCreator().createInteractionDerivativeCalculation(problem.getOwnerDocument(), opInfo, discProblem, problem, pi, level, simplifications);
        if (derivatives != null) {
        	fragmentResult.appendChild(discProblem.importNode(derivatives, true));
        }
       
        //Sources (they will only be in the last level)
        fragmentResult.appendChild(discProblem.importNode(
        		pi.getSchemaCreator().createSourcesMesh(problem.getOwnerDocument(), opInfo.getDerivativeLevels().get(level), pi, (Element) timeslice.getFirstChild().getFirstChild(), 
                        OperatorInfoType.interaction), true));
        //Multiplication of derivatives and algebraic terms
        fragmentResult.appendChild(discProblem.importNode(
        		pi.getSchemaCreator().createDerivativeMultiplication(problem.getOwnerDocument(), opInfo, opInfo.getMultiplicationLevels(), opInfo.getDerivativeLevels(), pi, (Element) timeslice.getFirstChild().getFirstChild(), 
                        OperatorInfoType.interaction, level, true), true));
        //Equation term summation
        fragmentResult.appendChild(discProblem.importNode(
        		pi.getSchemaCreator().createEquationSummationMesh(problem.getOwnerDocument(), opInfo, pi, (Element) variable.getFirstChild().getFirstChild(), 
                        OperatorInfoType.interaction, true), true));
        
        return fragmentResult;
    }
    
    /**
     * Process the eigen vector interaction between regions.
     * @param discProblem           The problem with the discretization
     * @param problem               The problem
     * @param rule                  The rule
     * @param pi                    The problem information
     * @param combinations          The combinations of the interactions
     * @param interaction           The interaction base information
     * @return                      The instructions
     * @throws AGDMException        AGDM00X  External error
     */
    private static DocumentFragment processEigenVectorInteraction(Document discProblem, Node problem, Element rule, ProcessInfo pi, 
            ArrayList<String> combinations, Node interaction) throws AGDMException {
        Document elemDoc = rule.getOwnerDocument();
        DocumentFragment fragmentResult = elemDoc.createDocumentFragment();
        //Particles
        if (pi.getDiscretizationType() == DiscretizationType.particles) {
          //Generate every condition combination
            NodeList spatialIteration = AGDMUtils.find(rule.getLastChild().getPreviousSibling().getFirstChild(), ExecutionFlow.SPATIAL_IT_TAGS);
            NodeList charInformationList = AGDMUtils.find(problem, "//mms:characteristicDecomposition");
            for (int i = 0; i < charInformationList.getLength(); i++) {
                Element charDecomposition = (Element) charInformationList.item(i);
                if (charDecomposition.getElementsByTagName("mms:generalCharacteristicDecomposition").getLength() ==  0) {
                    throw new AGDMException(AGDMException.AGDM006, "The region interaction requires a general characteristic decomposition");
                }
                //Time slice with spatial tags
                if (spatialIteration.getLength() > 0) {
                    throw new AGDMException(AGDMException.AGDM002, "The region interaction variables must not contain spatial tags");
                }
                else {
                    ProcessInfo newPi = new ProcessInfo(pi);
                    fragmentResult.appendChild(createEigenVectorInteraction(discProblem, problem, charDecomposition, interaction, 
                            rule.getLastChild().getPreviousSibling(), rule.getLastChild(), newPi));
                }
            }
        }
        //FVM
        else {
            //Process it for every characteristic decomposition of the region
            NodeList charInformationList = AGDMUtils.find(problem, "//mms:characteristicDecomposition");
            for (int i = 0; i < charInformationList.getLength(); i++) {
                Element charDecomposition = (Element) charInformationList.item(i);
                if (charDecomposition.getElementsByTagName("mms:generalCharacteristicDecomposition").getLength() ==  0) {
                    throw new AGDMException(AGDMException.AGDM006, "The region interaction requires a general characteristic decomposition");
                }
                NodeList spatialIteration = AGDMUtils.find(rule, ExecutionFlow.SPATIAL_IT_TAGS);
                //Time slice with spatial tags
                if (spatialIteration.getLength() > 0) {
                    throw new AGDMException(AGDMException.AGDM002, "The region interaction time slices must not contain spatial coordinate tags");
                }
                //Time slice without spatial tags
                else {
                    ProcessInfo newPi = new ProcessInfo(pi);
                    newPi.setSpatialCoord1(getAxis(combinations.get(0), pi.getBaseSpatialCoordinates()));
                    fragmentResult.appendChild(createEigenVectorInteraction(discProblem, problem, charDecomposition, interaction, 
                            rule.getFirstChild(), rule.getLastChild(), newPi));
                }
            }
        }
        return fragmentResult;
    }
    
    /**
     * Creates the instruction of a eigen vector interaction.
     * @param problem               The problem
     * @param charDecomposition     The characteristic decomposition of the model
     * @param vectorInteraction      The field Interaction
     * @param variable              The field variable
     * @param timeSlice             The field time slice
     * @param pi                    The process information
     * @return                      The mathml with the interactions
     * @throws AGDMException        AGDM006 Schema not applicable to this problem
     *                              AGDM007 External error
     */
    private static DocumentFragment createEigenVectorInteraction(Document discProblem, Node problem, Node charDecomposition, Node vectorInteraction, 
            Node variable, Node timeSlice, ProcessInfo pi) throws AGDMException {
        //Result is stored in document fragment
        Document doc = timeSlice.getOwnerDocument();
        DocumentFragment fragmentResult = doc.createDocumentFragment();
        //Projection of vectors
        fragmentResult.appendChild(doc.importNode(AGDMUtils.projectVectors(charDecomposition, 
                variable.getFirstChild().getFirstChild().cloneNode(true), 
                timeSlice.getFirstChild().getFirstChild().cloneNode(true), pi, false), true));
        //Auxiliary definitions
        fragmentResult.appendChild(doc.importNode(
            AGDMUtils.deployAuxiliaryDefinitions(discProblem, problem, charDecomposition, THREE, pi, 
                    timeSlice.getFirstChild().getFirstChild().cloneNode(true), false), true));
        //Diagonalization
        String query = ".//mms:generalCharacteristicDecomposition//mms:eigenSpace";
        NodeList eigenSpaces = AGDMUtils.find(charDecomposition, query);
        for (int i = 0; i < eigenSpaces.getLength(); i++) {
            //Creation of the base common if for the eigen values
            Element eigenValue = (Element) eigenSpaces.item(i).getChildNodes().item(2).cloneNode(true);
            Element ifElem = AGDMUtils.createElement(doc, smlUri, "if");
            Element then = AGDMUtils.createElement(doc, smlUri, "then");
            Element elseElem = AGDMUtils.createElement(doc, smlUri, "else");
            Element math = AGDMUtils.createElement(doc, mtUri, "math");
            Element apply = AGDMUtils.createElement(doc, mtUri, "apply");
            Element operator = AGDMUtils.createElement(doc, mtUri, "gt");
            Element zero = AGDMUtils.createElement(doc, mtUri, "cn");
            zero.setTextContent("0");
            //Modify the eigenValue if there is a field used inside using timeslice
            ArrayList<String> fieldIt = pi.getRegionInfo().getFields();
            for (int j = 0; j < fieldIt.size(); j++) {
                String field = fieldIt.get(j);
                //Field replacement with deployed coordinates
                Element timeSliceApply = AGDMUtils.createElement(doc, mtUri, "apply");
                Element fieldTimeSlice = AGDMUtils.createElement(doc, mtUri, "ci");
                Element msup = AGDMUtils.createElement(doc, mtUri, "msup");
                Element fieldE = AGDMUtils.createElement(doc, mtUri, "ci");
                Element time = AGDMUtils.createElement(doc, mtUri, "ci");
                fieldE.setTextContent(field);
                time.setTextContent("(" + pi.getCoordinateRelation().getDiscTimeCoord() + ")");
                fieldTimeSlice.appendChild(msup);
                timeSliceApply.appendChild(fieldTimeSlice);
                msup.appendChild(fieldE);
                msup.appendChild(time);
                AGDMUtils.createTimeSlice(eigenValue, field, timeSliceApply, pi.getRegionInfo(), pi.getCoordinateRelation());
            }
            //Creation of the content of the then and else elements
            //Then
            NodeList eigenVectors = ((Element) eigenSpaces.item(i)).getElementsByTagName("mms:eigenVector");
            for (int j = 0; j < eigenVectors.getLength(); j++) {
                String vectorName = eigenVectors.item(j).getFirstChild().getTextContent();
                Element thenMath = AGDMUtils.createElement(doc, mtUri, "math");
                Element thenApply = AGDMUtils.createElement(doc, mtUri, "apply");
                Element eq = AGDMUtils.createElement(doc, mtUri, "eq");
                Element ci = AGDMUtils.createElement(doc, mtUri, "ci");
                ci.setTextContent("fluxAcc" + vectorName);
                thenApply.appendChild(eq);
                thenApply.appendChild(doc.importNode(ci, true));
                thenApply.appendChild(doc.importNode(AGDMUtils.createCoefficientInstruction(
                        (Element) eigenVectors.item(j), variable.getFirstChild().cloneNode(true), 
                        timeSlice.getFirstChild().cloneNode(true), pi, false, false, false, false), true));
                thenMath.appendChild(thenApply);
                then.appendChild(thenMath);
            }
            //Else
            for (int j = 0; j < eigenVectors.getLength(); j++) {
                String vectorName = eigenVectors.item(j).getFirstChild().getTextContent();
                NodeList fieldInteractionValue = AGDMUtils.find(vectorInteraction, 
                        ".//mms:projection[mms:variable = '" + vectorName + "']");
                if (fieldInteractionValue.getLength() != 1) {
                    throw new AGDMException(AGDMException.AGDM006, 
                            "All characteristic region interactions must have the value for vector " + vectorName + " set.");
                }
                
                Element elseMath = AGDMUtils.createElement(doc, mtUri, "math");
                Element elseApply = AGDMUtils.createElement(doc, mtUri, "apply");
                Element eq = AGDMUtils.createElement(doc, mtUri, "eq");
                Element ci = AGDMUtils.createElement(doc, mtUri, "ci");
                ci.setTextContent("fluxAcc" + vectorName);
                elseApply.appendChild(eq);
                elseApply.appendChild(doc.importNode(ci, true));
                elseApply.appendChild(doc.importNode(AGDMUtils.createCoefficientInstruction(
                        (Element) fieldInteractionValue.item(0), variable.getFirstChild().cloneNode(true), 
                        timeSlice.getFirstChild().cloneNode(true), pi, false, false, false, false), true));
                elseMath.appendChild(elseApply);
                elseElem.appendChild(elseMath);
            }
            
            apply.appendChild(operator);
            Element cond = (Element) eigenValue.cloneNode(true);
            AGDMUtils.replaceFieldVariables(pi, (Element) timeSlice.getFirstChild().getFirstChild().cloneNode(true), cond);
            apply.appendChild(doc.importNode(cond.getFirstChild().getFirstChild(), true));
            apply.appendChild(zero);
            math.appendChild(apply);
            ifElem.appendChild(math);
            ifElem.appendChild(then);
            ifElem.appendChild(elseElem);
            fragmentResult.appendChild(ifElem);
        }
        //Undiagonalization
        query = ".//mms:generalCharacteristicDecomposition//mms:undiagonalization";
        NodeList undiagonalizations = AGDMUtils.find(charDecomposition, query);
        for (int i = 0; i < undiagonalizations.getLength(); i++) {
            Element fieldElem = (Element) undiagonalizations.item(i).getFirstChild().cloneNode(true);
            ProcessInfo actualPi = new ProcessInfo(pi);
            actualPi.setField(fieldElem.getTextContent());
            Element fieldTime = null;
            if (AGDMUtils.isField(pi.getCompleteProblem(), fieldElem.getTextContent())) {
                fieldTime = ExecutionFlow.replaceTags(variable.getFirstChild().getFirstChild().cloneNode(true), actualPi);
            }
            else {
                fieldTime =  AGDMUtils.createElement(doc, mtUri, "ci");
                fieldTime.setTextContent(fieldElem.getTextContent() + "_var");
            }

            Element math = AGDMUtils.createElement(doc, mtUri, "math");
            Element undiagApply = AGDMUtils.createElement(doc, mtUri, "apply");
            Element eq = AGDMUtils.createElement(doc, mtUri, "eq");
            undiagApply.appendChild(eq);
            undiagApply.appendChild(doc.importNode(fieldTime, true));
            undiagApply.appendChild(doc.importNode(
                    createUndiagonalizationInstruction(undiagonalizations.item(i).cloneNode(true), 
                            timeSlice.getFirstChild().getFirstChild().cloneNode(true), actualPi), true));
            math.appendChild(undiagApply);
            fragmentResult.appendChild(math);
        }
        //Unprojection of vectors
        fragmentResult.appendChild(doc.importNode(AGDMUtils.unprojectVectors(charDecomposition, 
                variable.getFirstChild().getFirstChild().cloneNode(true), pi), true));
        return fragmentResult;
    }
    
    /**
     * Creates the instruction of a field interaction.
     * @param problem               The problem
     * @param fieldInteraction      The field Interaction
     * @param timeSlice             The time slice for the field
     * @param variable              The variable for the field
     * @param pi                    The process information
     * @return                      The mathm with the interactions
     * @throws AGDMException        AGDM006 Schema not applicable to this problem
     *                              AGDM007 External error
     */
    private static DocumentFragment createFieldInteraction(Node problem, Node fieldInteraction, Node variable, Node timeSlice, 
            ProcessInfo pi) throws AGDMException {
        //Result is stored in document fragment
        Document doc = timeSlice.getOwnerDocument();
        DocumentFragment fragmentResult = doc.createDocumentFragment();
        //Get the fixed value for the field
        ArrayList<ProcessInfo> pis = AGDMUtils.generateProcessInfo(problem, pi, false);
        for (int j = 0; j < pis.size(); j++) {
            Element math = AGDMUtils.createElement(doc, mtUri, "math");
            Element apply = AGDMUtils.createElement(doc, mtUri, "apply");
            Element eq = AGDMUtils.createElement(doc, mtUri, "eq");
            apply.appendChild(eq);
            if (variable.getFirstChild().getLastChild().getLocalName().equals("currentCell")) {
                apply.appendChild(doc.importNode(AGDMUtils.replaceTmp(
                    ExecutionFlow.replaceTags(variable.cloneNode(true).getFirstChild(), pis.get(j))), true).getFirstChild().getFirstChild());
            }
            else {
                apply.appendChild(doc.importNode(AGDMUtils.replaceTmp(
                    ExecutionFlow.replaceTags(variable.cloneNode(true).getFirstChild(), pis.get(j))), true).getFirstChild());
            }
            apply.appendChild(doc.importNode(
                    ExecutionFlow.replaceTags(variable.getFirstChild().cloneNode(true), pis.get(j)).getFirstChild(), true));
            math.appendChild(apply);
            fragmentResult.appendChild(math);
        }
       
        //Get the fixed value for the field
        NodeList projection = ((Element) fieldInteraction).getElementsByTagName("mms:projection");
        for (int j = 0; j < projection.getLength(); j++) {
            ProcessInfo actualPi = new ProcessInfo(pi);
            actualPi.setField(projection.item(j).getFirstChild().getTextContent());
            Element math = AGDMUtils.createElement(doc, mtUri, "math");
            Element apply = AGDMUtils.createElement(doc, mtUri, "apply");
            Element eq = AGDMUtils.createElement(doc, mtUri, "eq");
            
            Element rightSide = (Element) doc.importNode(AGDMUtils.createCoefficientInstruction(
                    (Element) projection.item(j), variable.getFirstChild().cloneNode(true), 
                    timeSlice.getFirstChild().cloneNode(true), pi, false, true, false, false), true);
            apply.appendChild(eq);
            apply.appendChild(doc.importNode(
                    ExecutionFlow.replaceTags(variable.getFirstChild().cloneNode(true).getFirstChild(), actualPi), true));
            apply.appendChild(doc.importNode(rightSide, true));
            math.appendChild(apply);
            fragmentResult.appendChild(math);
        }
        return fragmentResult;
    }
    
    /**
     * Creates an undiagonalization instruction.
     * @param node              The instruction XML
     * @param timeSlice         The time slice
     * @param pi                The process info
     * @return                  The instruction
     * @throws AGDMException    AGDM007  External error 
     */
    private static Element createUndiagonalizationInstruction(Node node, Node timeSlice, ProcessInfo pi) throws AGDMException {
        Document doc = timeSlice.getOwnerDocument();
        Element result = null;
        //The first children is the vector name
        NodeList coefficients = node.getLastChild().getChildNodes(); 
        for (int i = 0; i < coefficients.getLength(); i++) {
            Element coefficient = (Element) coefficients.item(i);
            
            Element term = AGDMUtils.createElement(doc, mtUri, "apply");
            Element times = AGDMUtils.createElement(doc, mtUri, "times");
            Element field = AGDMUtils.createElement(doc, mtUri, "ci");
            field.setTextContent("fluxAcc" + coefficient.getFirstChild().getTextContent());
            
            //Apply TimeSlice to the coefficient
            Element coefficientE = (Element) coefficient.getLastChild().getFirstChild().cloneNode(true);
            AGDMUtils.replaceFieldVariables(pi, (Element) timeSlice, coefficientE);

            
            term.appendChild(times);
            term.appendChild(field);
            term.appendChild(doc.importNode(coefficientE, true));
            
            //Simplification if coefficient is 1.
            if (coefficient.getLastChild().getFirstChild().getLocalName().equals("cn") 
                    && coefficient.getLastChild().getFirstChild().getTextContent().equals("1")) {
                term = field;
            }
            //Simplification if coefficient is 1.
            Element simp = AGDMUtils.createElement(doc, mtUri, "apply");
            Element minus = AGDMUtils.createElement(doc, mtUri, "minus");
            Element one = AGDMUtils.createElement(doc, mtUri, "cn");
            one.setTextContent("1");
            simp.appendChild(minus);
            simp.appendChild(one);
            if (coefficient.getLastChild().getFirstChild().isEqualNode(simp)) {
                simp.replaceChild(field, one);
                term = simp;
            }
            
            if (i == 0) {
                result = term;
            }
            else {
                Element apply = AGDMUtils.createElement(doc, mtUri, "apply");
                Element plus = AGDMUtils.createElement(doc, mtUri, "plus");
                apply.appendChild(plus);
                apply.appendChild(result);
                apply.appendChild(term);
                result = apply;
            }
        }
        return result;
    }
    
    /**
     * Creates all the combinations of interactions.
     * @param coordinates       The coordinates
     * @return                  The interaction list
     */
    private static ArrayList<String> createInteractionCombinations(ArrayList<String> coordinates) {
        ArrayList<String> combinations = new ArrayList<String>();
        
        combinations.add("+");
        combinations.add("-");
        combinations.add("0");
        
        for (int j = 0; j < coordinates.size() - 1; j++) {
            ArrayList<String> combinationsTMP = new ArrayList<String>();
            combinationsTMP.addAll(combinations);
            combinations.clear();
            for (int i = 0; i < combinationsTMP.size(); i++) {
                String newComb = combinationsTMP.get(i);
                combinations.add(newComb + "+");
                combinations.add(newComb + "-");
                combinations.add(newComb + "0");
            }            
        }
        if (coordinates.size() == 1) {
            combinations.remove("0");
        }
        if (coordinates.size() == 2) {
            combinations.remove("00");
        }
        if (coordinates.size() == THREE) {
            combinations.remove("000");
        }
        
        return combinations;
    }
    
    /**
     * Returns the first index that has an increment or decrement in the combination.
     * @param combination       The combination.
     * @param coords            The coordinates
     * @return                  The coordinate
     */
    private static String getAxis(String combination, ArrayList<String> coords) {
        for (int i = 0; i < combination.length(); i++) {
            if (!combination.substring(i).startsWith("0")) {
                return coords.get(i);
            }
        }
        return null;
    }
 
    
    /**
     * Create the condition to check neighbours for the combinations and target regions.
     * @param problem           The document
     * @param rule              The rule
     * @param pi                The process information
     * @param targetRegions    The target regions
     * @param combinations      The combinations
     * @return                  The condition
     * @throws AGDMException    AGDM00X  External error
     */
    private static Element createNeighbourCondition(Document problem, Document rule, ProcessInfo pi, NodeList targetRegions, 
            ArrayList<String> combinations) throws AGDMException {
        Element condition = null;
        for (int i = 0; i < combinations.size(); i++) {
            for (int j = 0; j < targetRegions.getLength(); j++) {
                
                boolean hasInterior = AGDMUtils.isInteriorRegion(problem, targetRegions.item(j).getTextContent());
                boolean hasSurface = AGDMUtils.isSurfaceRegion(problem, targetRegions.item(j).getTextContent());
                Element targetInteriorCondition = AGDMUtils.createElement(rule, mtUri, "apply");
                Element targetSurfaceCondition = AGDMUtils.createElement(rule, mtUri, "apply");
                Element gt = AGDMUtils.createElement(rule, mtUri, "gt");
                Element applyI = AGDMUtils.createElement(rule, mtUri, "apply");
                Element applyS = AGDMUtils.createElement(rule, mtUri, "apply");
                Element regionInteriorId = AGDMUtils.createElement(rule, smlUri, "regionInteriorId");
                regionInteriorId.setTextContent(targetRegions.item(j).getTextContent());
                Element regionSurfaceId = AGDMUtils.createElement(rule, smlUri, "regionSurfaceId");
                regionSurfaceId.setTextContent(targetRegions.item(j).getTextContent());
                
                Element zero = AGDMUtils.createElement(rule, mtUri, "cn");
                zero.setTextContent("0");
                
                targetInteriorCondition.appendChild(gt);
                targetInteriorCondition.appendChild(applyI);
                targetInteriorCondition.appendChild(zero);
                
                targetSurfaceCondition.appendChild(gt.cloneNode(true));
                targetSurfaceCondition.appendChild(applyS);
                targetSurfaceCondition.appendChild(zero.cloneNode(true));
                
                applyI.appendChild(regionInteriorId);
                applyI.appendChild(AGDMUtils.deployCoordinateCombination(rule, pi, combinations.get(i)));
                
                applyS.appendChild(regionSurfaceId);
                applyS.appendChild(AGDMUtils.deployCoordinateCombination(rule, pi, combinations.get(i)));
                
                Element apply = AGDMUtils.createElement(rule, mtUri, "apply");
                
                //If the interaction is with a region then could be with the interior or surface
                if (isRegion(problem, targetRegions.item(j).getTextContent())) {
                    if (hasInterior && hasSurface) {
                        Element or = AGDMUtils.createElement(rule, mtUri, "or");
                        apply.appendChild(or);
                        apply.appendChild(targetInteriorCondition);
                        apply.appendChild(targetSurfaceCondition);
                    }
                    else {
                        if (hasInterior) {
                            apply = targetInteriorCondition;
                        }
                        else {
                            apply = targetSurfaceCondition;
                        }
                    }
                }
                //If the interaction is with a boundary
                else {
                    apply = targetInteriorCondition;
                }
                

                if (j == 0 && i == 0) {
                    condition = (Element) apply.cloneNode(true);
                }
                else {
                    Element apply3 = AGDMUtils.createElement(rule, mtUri, "apply");
                    Element or2 = AGDMUtils.createElement(rule, mtUri, "or");
                    Element tmp = condition;
                    apply3.appendChild(or2);
                    apply3.appendChild(tmp);
                    apply3.appendChild(apply.cloneNode(true));
                    condition = apply3;
                }
            }
        }
        return condition;
    }
    
    /**
     * Create the condition to check neighbours for the combinations and target regions.
     * @param problem           The discrete problem
     * @param rule              The rule
     * @param pi                The process information
     * @param targetRegions    The target regions
     * @param coord             The coordinate for the condition
     * @param op                The operation
     * @return                  The condition
     * @throws AGDMException    AGDM00X  External error
     */
    private static Element createSimpleCondition(Document problem, Document rule, ProcessInfo pi, NodeList targetRegions, String coord, String op)
        throws AGDMException {
        Element condition = null;
        for (int j = 0; j < targetRegions.getLength(); j++) {
            
            boolean hasInterior = AGDMUtils.isInteriorRegion(problem, targetRegions.item(j).getTextContent());
            boolean hasSurface = AGDMUtils.isSurfaceRegion(problem, targetRegions.item(j).getTextContent());
            Element targetInteriorCondition = AGDMUtils.createElement(rule, mtUri, "apply");
            Element targetSurfaceCondition = AGDMUtils.createElement(rule, mtUri, "apply");
            Element gt = AGDMUtils.createElement(rule, mtUri, "gt");
            Element applyI = AGDMUtils.createElement(rule, mtUri, "apply");
            Element applyS = AGDMUtils.createElement(rule, mtUri, "apply");
            Element zero = AGDMUtils.createElement(rule, mtUri, "cn");
            zero.setTextContent("0");
            Element regionInteriorId = AGDMUtils.createElement(rule, smlUri, "regionInteriorId");
            regionInteriorId.setTextContent(targetRegions.item(j).getTextContent());
            targetInteriorCondition.appendChild(gt);
            targetInteriorCondition.appendChild(applyI);
            targetInteriorCondition.appendChild(zero);
            
            Element regionSurfaceId = AGDMUtils.createElement(rule, smlUri, "regionSurfaceId");
            regionSurfaceId.setTextContent(targetRegions.item(j).getTextContent());
            targetSurfaceCondition.appendChild(gt.cloneNode(true));
            targetSurfaceCondition.appendChild(applyS);
            targetSurfaceCondition.appendChild(zero.cloneNode(true));
            
            applyI.appendChild(regionInteriorId);
            applyI.appendChild(deploySimpleCoordinate(rule, pi, coord, op));
            
            applyS.appendChild(regionSurfaceId);
            applyS.appendChild(deploySimpleCoordinate(rule, pi, coord, op));
            
            Element apply = AGDMUtils.createElement(rule, mtUri, "apply");
            
            //If the interaction is with a region then could be with the interior or surface
            if (isRegion(problem, targetRegions.item(j).getTextContent())) {
                if (hasInterior && hasSurface) {
                    Element or = AGDMUtils.createElement(rule, mtUri, "or");
                    apply.appendChild(or);
                    apply.appendChild(targetInteriorCondition);
                    apply.appendChild(targetSurfaceCondition);
                }
                else {
                    if (hasInterior) {
                        apply = targetInteriorCondition;
                    }
                    else {
                        apply = targetSurfaceCondition;
                    }
                }
            }
            //If the interaction is with a boundary
            else {
                apply = targetInteriorCondition;
            }

            if (j == 0) {
                condition = (Element) apply.cloneNode(true);
            }
            else {
                Element apply3 = AGDMUtils.createElement(rule, mtUri, "apply");
                Element or2 = AGDMUtils.createElement(rule, mtUri, "or");
                Element tmp = condition;
                apply3.appendChild(or2);
                apply3.appendChild(tmp);
                apply3.appendChild(apply.cloneNode(true));
                condition = apply3;
            }
        }
        return condition;
    }
    
    /**
     * Deploy the coordinates with an incremen or decrement on the given coordinate.
     * @param doc       The problem
     * @param pi        The problem information
     * @param coord     The coordinate
     * @param op        The operation
     * @return          The coordinates operated
     */
    private static DocumentFragment deploySimpleCoordinate(Document doc, ProcessInfo pi, String coord, String op) {
        DocumentFragment coords = doc.createDocumentFragment();
        for (int i = 0; i < pi.getBaseSpatialCoordinates().size(); i++) {
            String currentCoord = pi.getBaseSpatialCoordinates().get(i);
            String discCoord = pi.getCoordinateRelation().getDiscCoords().get(i);
            if (currentCoord.equals(coord)) {
                Element apply = AGDMUtils.createElement(doc, mtUri, "apply");
                Element opElem = AGDMUtils.createElement(doc, mtUri, op);
                Element ci = AGDMUtils.createElement(doc, mtUri, "ci");
                ci.setTextContent(discCoord);
                Element cn = AGDMUtils.createElement(doc, mtUri, "cn");
                cn.setTextContent("1");
                apply.appendChild(opElem);
                apply.appendChild(ci);
                apply.appendChild(cn);
                coords.appendChild(apply);
            }
            else {
                Element ci = AGDMUtils.createElement(doc, mtUri, "ci");
                ci.setTextContent(discCoord);
                coords.appendChild(ci);
            }
        }
        return coords;
    }
    
    /**
     * Filter the combinations that have a certain operation in a coordinate.
     * @param combinations      All the combinations
     * @param j                 The index of the coordinate to check
     * @param op                The operation
     * @return                  The filtered combinations
     */
    private static ArrayList<String> filterCombs(ArrayList<String> combinations, int j, String op) {
        ArrayList<String> candidates = new ArrayList<String>();
        for (int i = 0; i < combinations.size(); i++) {
            if (combinations.get(i).substring(j).startsWith(op)) {
                candidates.add(combinations.get(i));
            }
        }
        return candidates;
    }
    
 
    /**
     * Checks when the region name exist.
     * @param discProblem       The discretization problem
     * @param regionName        The region to check
     * @return                  True if the regions exists
     * @throws AGDMException    AGDM00X  External error
     */
    public static boolean isRegion(Document discProblem, String regionName) throws AGDMException {
        return AGDMUtils.find(discProblem, "//mms:region/mms:name[text() = '" 
                + regionName + "']|//mms:subregion/mms:name[text() = '" 
                + regionName + "']").getLength() > 0;
    }
    
}
