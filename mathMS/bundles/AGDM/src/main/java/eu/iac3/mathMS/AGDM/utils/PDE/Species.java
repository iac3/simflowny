/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.AGDM.utils.PDE;

import java.util.ArrayList;
import java.util.HashMap;

import org.w3c.dom.Element;

public class Species {
	private String name;
	private ArrayList<String> particleFields;
    private Element particleVolume;
    private HashMap<String, Element> particleVelocities;
    
    /**
     * Constructor.
     * @param name					Name
     * @param particleFields		Fields
     * @param particleVolume		Volume
     * @param particleVelocities	Velocities
     */
	public Species(String name, ArrayList<String> particleFields, Element particleVolume,
			HashMap<String, Element> particleVelocities) {
		super();
		this.name = name;
		this.particleFields = particleFields;
		this.particleVolume = particleVolume;
		this.particleVelocities = particleVelocities;
	}
	
    /**
     * Constructor.
     * @param name					Name
     * @param particleFields		Fields
     * @param particleVolume		Volume
     * @param particleVelocities	Velocities
     */
	public Species(Species sp) {
		super();
		this.name = sp.name;
		this.particleFields = new ArrayList<String>(sp.particleFields);
		this.particleVolume = sp.particleVolume;
		this.particleVelocities = new HashMap<String, Element>(sp.particleVelocities);
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public ArrayList<String> getParticleFields() {
		return particleFields;
	}
	public void setParticleFields(ArrayList<String> particleFields) {
		this.particleFields = particleFields;
	}
	public Element getParticleVolume() {
		return particleVolume;
	}
	public void setParticleVolume(Element particleVolume) {
		this.particleVolume = particleVolume;
	}
	public HashMap<String, Element> getParticleVelocities() {
		return particleVelocities;
	}
	public void setParticleVelocities(HashMap<String, Element> particleVelocities) {
		this.particleVelocities = particleVelocities;
	}


	@Override
	public String toString() {
		return "Species [name=" + name + ", particleFields=" + particleFields + ", particleVolume=" + particleVolume
				+ ", particleVelocities=" + particleVelocities + "]";
	}
	
	
}
