/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.AGDM.utils.PDE;

import java.util.ArrayList;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import eu.iac3.mathMS.AGDM.AGDMException;
import eu.iac3.mathMS.AGDM.utils.AGDMUtils;

public class OperatorPolicy {
    private ArrayList<SpatialOperatorDiscretization> discretizations;

    /**
     * Constructor.
     * @param policyDocument	Document fragment to read from.
     * @throws AGDMException	AGDM002 XML discretization schema not valid
     * 							AGDM003  Import discretization schema not exist
     * 							AGDM00X  External error
     */
    public OperatorPolicy(Document policyDocument) throws AGDMException {
        discretizations = new ArrayList<SpatialOperatorDiscretization>();
        NodeList discs = policyDocument.getDocumentElement().getElementsByTagNameNS(AGDMUtils.mmsUri, "discretization");
        for (int i = 0; i < discs.getLength(); i++) {
            discretizations.add(new SpatialOperatorDiscretization((Element) discs.item(i)));
        }
    }
    
    public ArrayList<SpatialOperatorDiscretization> getDiscretization() {
        return discretizations;
    }
}
