/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.AGDM.utils.PDE;

import java.util.ArrayList;
import java.util.stream.Collectors;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

import eu.iac3.mathMS.AGDM.AGDMException;

/**
 * The information needed to process an equation.
 * @author bminyano
 *
 */
public class EquationTermsLevel {

    private ArrayList<EquationTerms> equations;
    
    /**
     * Constructor.
     */
    public EquationTermsLevel() {
        equations = new ArrayList<EquationTerms>();
    }
    
    /**
     * Copy constructor.
     * @param etl   The original object   
     */
    public EquationTermsLevel(EquationTermsLevel etl) {
    	equations = new ArrayList<EquationTerms>();
    	for (EquationTerms et : etl.equations) {
    		equations.add(new EquationTerms(et));
    	}
    }
    
    /**
     * Copy constructor.
     * @param etl   The original object   
     */
    public EquationTermsLevel(ArrayList<EquationTerms> etl) {
        equations = new ArrayList<EquationTerms>();
    	for (EquationTerms et : etl) {
    		equations.add(new EquationTerms(et));
    	}
    }
    
    /**
     * Adds a new Equation term.
     * @param fields     	The variable names
     * @param terms     	The terms for the summation equation
     * @param algorithm		Algorithm
     * @param opType    	The operator type
     * @param equationType	Equation type
     */
    public void add(ArrayList<String> fields, ArrayList<String> terms, Element algorithm, String opType, EquationType equationType, Node condition, boolean maximumPriority) {
        equations.add(new EquationTerms(fields, terms, algorithm, opType, equationType, condition, maximumPriority));
    }
    
    /**
     * Adds a new Equation term.
     * @param et	the equation term
     */
    public void add(EquationTerms et) {
        equations.add(new EquationTerms(et));
    }
    
    /**
     * Adds a new Equation term.
     * @param et		the equation term
     * @param position	the position
     */
    public void add(EquationTerms et, int position) {
        equations.add(position, new EquationTerms(et));
    }
    
    public ArrayList<EquationTerms> getEquations() {
        return equations;
    }

    /**
     * Gets equation terms from a given field.
     * @param field		The field to get equation terms
     * @return			The equation terms
     */
    public EquationTerms getEquationsForField(String field) {
        for (int i = 0; i < equations.size(); i++) {
            if (equations.get(i).getFields().contains(field)) {
                return equations.get(i);
            }
        }
        return null;
    }
    
    /**
     * Get equations for given field id.
     * @param id		The field id to search.
     * @return			The equation
     */
    public EquationTerms getEquationsForId(String id) {
        for (int i = 0; i < equations.size(); i++) {
        	String eqID = equations.get(i).getFields().stream().map(Object::toString).collect(Collectors.joining("_"));;
            if (eqID.equals(id)) {
                return equations.get(i);
            }
        }
        return null;
    }
    
    @Override
    public String toString() {
        return "EquationTermsLevel [equations=" + equations + "]";
    }
    
    /**
     * Checks if there is a term for a specific field.
     * @param field     The field
     * @return          True if there is a term for the field
     */
    public boolean hasTermForField(String field) {
        for (int i = 0; i < equations.size(); i++) {
            if (equations.get(i).getFields().contains(field)) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Find the equation which uses a given derivative term.
     * @param derivativeTermVar		The derivative term
     * @return						Equation field
     * @throws AGDMException		AGDM009  Derivative variable not found
     */
    public String findEquationForDerivativeTerm(String derivativeTermVar) throws AGDMException {
        for (int i = 0; i < equations.size(); i++) {
            if (equations.get(i).getTerms().contains(derivativeTermVar)) {
                return equations.get(i).getFields().get(0);
            }
        }
        throw new AGDMException(AGDMException.AGDM009, derivativeTermVar);
    }
}
