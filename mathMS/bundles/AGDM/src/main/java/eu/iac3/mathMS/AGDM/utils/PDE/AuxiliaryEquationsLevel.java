/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.AGDM.utils.PDE;

import java.util.ArrayList;
import java.util.stream.Collectors;

import org.w3c.dom.Element;

/**
 * The information needed to process an equation.
 * @author bminyano
 *
 */
public class AuxiliaryEquationsLevel {

    private ArrayList<AuxiliaryEquation> equations;
    
    /**
     * Constructor.
     */
    public AuxiliaryEquationsLevel() {
        equations = new ArrayList<AuxiliaryEquation>();
    }
  
    /**
     * Copy constructor.
     * @param etl   The original object   
     */
    public AuxiliaryEquationsLevel(AuxiliaryEquationsLevel etl) {
        equations = new ArrayList<AuxiliaryEquation>();
    	for (AuxiliaryEquation ae : etl.equations) {
    		equations.add(new AuxiliaryEquation(ae));
    	}
    }
    
    /**
     * Copy constructor.
     * @param etl   The original object   
     */
    public AuxiliaryEquationsLevel(ArrayList<AuxiliaryEquation> etl) {
        equations = new ArrayList<AuxiliaryEquation>();
    	for (AuxiliaryEquation ae : etl) {
    		equations.add(new AuxiliaryEquation(ae));
    	}
    }
    
    /**
     * Adds a new Equation term.
     * @param fields    The auxiliary fields
     * @param terms     The terms for the summation equation
     * @param algorithm Auxiliary algorithm
     * @param opType    The operator type
     */
    public void add(ArrayList<String> fields, ArrayList<String> terms, Element algorithm, EquationType opType) {
        equations.add(new AuxiliaryEquation(fields, terms, algorithm, opType));
    }
    
    /**
     * Adds a new equation.
     * @param eq	The equation to add
     * @param position	the position
     */
    public void add(AuxiliaryEquation eq, int position) {
        equations.add(position, eq);
    }
    
    /**
     * Get equations for given field.
     * @param field		The field to search.
     * @return			The equation
     */
    public AuxiliaryEquation getEquationsForField(String field) {
        for (int i = 0; i < equations.size(); i++) {
            if (equations.get(i).getFields().contains(field)) {
                return equations.get(i);
            }
        }
        return null;
    }
    
    
    /**
     * Get equations for given field id.
     * @param id		The field id to search.
     * @return			The equation
     */
    public AuxiliaryEquation getEquationsForId(String id) {
        for (int i = 0; i < equations.size(); i++) {
        	String eqID = equations.get(i).getFields().stream().map(Object::toString).collect(Collectors.joining("_"));;
            if (eqID.equals(id)) {
                return equations.get(i);
            }
        }
        return null;
    }
    
    public ArrayList<AuxiliaryEquation> getEquations() {
        return equations;
    }

    @Override
    public String toString() {
        return "AuxiliaryEquationLevel [equations=" + equations + "]";
    }
    
}
