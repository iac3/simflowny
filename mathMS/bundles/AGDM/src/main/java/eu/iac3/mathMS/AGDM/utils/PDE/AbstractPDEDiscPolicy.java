/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.AGDM.utils.PDE;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Optional;

import org.w3c.dom.Document;

import eu.iac3.mathMS.AGDM.AGDMException;
import eu.iac3.mathMS.AGDM.utils.AGDMUtils;
import eu.iac3.mathMS.documentManagerPlugin.DMException;
import eu.iac3.mathMS.documentManagerPlugin.DocumentManager;
import eu.iac3.mathMS.documentManagerPlugin.DocumentManagerImpl;
import eu.iac3.mathMS.simflownyUtilsPlugin.SUException;
import eu.iac3.mathMS.simflownyUtilsPlugin.SimflownyUtils;

/**
 * Base discretization policy for regions and analysis.
 * @author bminano
 *
 */
public abstract class AbstractPDEDiscPolicy {

    ArrayList<OperatorDiscretization> operatorDiscretizations;
    
    public ArrayList<OperatorDiscretization> getOperatorDiscretizations() {
        return operatorDiscretizations;
    }
    
    /**
     * Returns the spatial operator discretization compatible with the parameters.
     * @param order             The order of the solver
     * @param coordinates       The coordinate combination
     * @param operatorName      The name of the operator to solve
     * @return                  The discretization if exists, otherwise returns null
     * @throws AGDMException    AGDM007 Invalid discretization policy
     */
    abstract Optional<SpatialOperatorDiscretization> getCompatiblePolicy(int order, String coordinates, String operatorName) throws AGDMException;
    
    /**
     * Loads the operator discretization policies.
     * @return  A map for operator policies by the operator policy document id.
     * @throws AGDMException 
     */
    HashMap<String, OperatorPolicy> loadOperatorPolicies() throws AGDMException {
        HashMap<String, OperatorPolicy> map = new HashMap<String, OperatorPolicy>();
        
        //For every operator policy in every region
        for (int j = 0; j < operatorDiscretizations.size(); j++) {
            String opDiscId = operatorDiscretizations.get(j).getSchema();
            if (!map.containsKey(opDiscId)) {
                //Getting the operator policy document
                Document operatorDiscretization;
                try {
                    DocumentManager docMan = new DocumentManagerImpl();
                    operatorDiscretization = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(docMan.getDocument(opDiscId)));
                } 
                catch (SUException e) {
                    throw new AGDMException(AGDMException.AGDM002, "Spatial operator discretization with id " + opDiscId + " not valid.");
                }
                catch (DMException e) {
                    throw new AGDMException(AGDMException.AGDM003, "Spatial operator discretization with id " + opDiscId + " does not exist.");
                }
                map.put(opDiscId, new OperatorPolicy(operatorDiscretization));
            }
        }
        return map;
    }
}
