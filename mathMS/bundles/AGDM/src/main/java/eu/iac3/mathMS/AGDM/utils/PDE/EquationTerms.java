/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.AGDM.utils.PDE;

import java.util.ArrayList;
import java.util.Optional;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

import eu.iac3.mathMS.AGDM.AGDMException;
import eu.iac3.mathMS.AGDM.utils.AGDMUtils;

public class EquationTerms {
	ArrayList<String> fields;
    ArrayList<String> terms;
    OperatorInfoType operatorType;
    EquationType equationType;
    Element algorithm;
    Node conditional;
    boolean maximumPriority;
    
    /**
     * Constructor.
     * @param field     The variable name
     * @param terms     The terms for the equation
     * @param opType    The operator type
     */
    public EquationTerms(ArrayList<String>  fields, ArrayList<String> terms, Element eqAlgorithm, String opType, EquationType equationType, Node condition, boolean maximumPriority) {
        this.fields = fields;
        this.terms = new ArrayList<String>();
       	for (String term : terms) {
       		this.terms.add(term);
       	}
        if (opType.equals("evolutionEquation")) {
            this.operatorType = OperatorInfoType.field;
        }
        if (opType.equals("analysisFieldEquation")) {
            this.operatorType = OperatorInfoType.analysisField;
        }
        if (opType.equals("auxiliaryVariableEquation") || opType.equals("auxiliaryAnalysisVariableEquation")) {
            this.operatorType = OperatorInfoType.auxiliaryVariable;
        }
        if (opType.equals("projection")) {
            this.operatorType = OperatorInfoType.interaction;
        }
        if (opType.equals("auxiliaryField")) {
            this.operatorType = OperatorInfoType.auxiliaryField;
        }
        this.equationType = equationType;
        this.algorithm = eqAlgorithm;
        this.conditional = condition;
        this.maximumPriority = maximumPriority;
    }
    
    /**
     * Copy constructor.
     * @param et    The object to copy
     */
    public EquationTerms(EquationTerms et) {
        this.fields = new ArrayList<String>();
       	for (String f : et.getFields()) {
       		fields.add(f);
       	}
        this.terms = new ArrayList<String>();
       	for (String term : et.getTerms()) {
       		terms.add(term);
       	}
        if (et.algorithm != null) {
        	this.algorithm = (Element) et.algorithm.cloneNode(true);
        }
        if (et.conditional != null) {
        	this.conditional = (Element) et.conditional.cloneNode(true);
        }
        this.operatorType = et.operatorType;
        this.equationType = et.equationType;
        this.maximumPriority = et.maximumPriority;
    }

    public ArrayList<String> getFields() {
        return fields;
    }

    public ArrayList<String> getTerms() {
        return terms;
    }

    public Node getConditional() {
		if (conditional == null) {
			return null;
		}
		return conditional.cloneNode(true);
	}

	public OperatorInfoType getOperatorType() {
        return operatorType;
    }

    public EquationType getEquationType() {
		return equationType;
	}

	public Element getAlgorithm() {
		return algorithm;
	}
	

	public boolean isMaximumPriority() {
		return maximumPriority;
	}

	public void setConditional(Node conditional) {
		this.conditional = conditional;
	}

	@Override
    public String toString() {
		Optional<Node> op = Optional.ofNullable(conditional);
        String condition = op.map( a -> {
			try {
				return AGDMUtils.domToString(a);
			} 
			catch (AGDMException e) {
				return "empty";
			}
		}).orElse("empty");
    	if (equationType == EquationType.algebraic) {
    		return "EquationTerms [field=" + fields.get(0) + ", terms=" + terms + ", operatorType=" + operatorType + ", equationType=" + equationType + ", condition=" + condition + "]" + "\n";
    	}
    	if (equationType == EquationType.algorithmic) {
    		try {
				return "EquationTerms [fields=" + fields + ", algorithm=" + AGDMUtils.domToString(algorithm) + ", operatorType=" + operatorType + ", equationType=" + equationType + ", condition=" + condition + "]" + "\n";
			} catch (AGDMException e) {
				e.printStackTrace();
			}
    	}
    	return "";
    }  
    
}
