/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.AGDM;

/**
 * Extrapolation types allowed in Simflowny.
 * @author bminano
 *
 */
public enum ExtrapolationType {
    Linear_LagrangianExtrapolation,
    Quadratic_LagrangianExtrapolation,
    Cubic_LagrangianExtrapolation
}
