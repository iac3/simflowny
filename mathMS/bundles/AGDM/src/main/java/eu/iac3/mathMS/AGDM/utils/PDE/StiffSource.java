/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.AGDM.utils.PDE;

import java.util.Optional;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

import eu.iac3.mathMS.AGDM.AGDMException;
import eu.iac3.mathMS.AGDM.utils.AGDMUtils;

/**
 * Source class.
 * @author bminano
 *
 */
public class StiffSource {
	private String field;
	private Element formula;
	private Node conditional;
	
	/**
	 * Copy constructor.
	 * @param s	object to copy
	 */
	public StiffSource(StiffSource s) {
		this.field = s.field;
		this.formula = (Element) s.formula.cloneNode(true);
		if (conditional != null) {
			this.conditional = s.conditional.cloneNode(true);
		}
		else {
			this.conditional = null;
		}
	}
	
	/**
	 * Constructor.
	 * @param field		field
	 * @param formula	formula
	 */
	public StiffSource(String field, Element formula, Node conditional) {
		this.field = field;
		this.formula = (Element) formula.cloneNode(true);
		if (conditional != null) {
			this.conditional = conditional.cloneNode(true);
		}
		else {
			this.conditional = null;
		}
	}
	public String getField() {
		return field;
	}
	public void setField(String field) {
		this.field = field;
	}
	public Element getFormula() {
		return formula;
	}
	public void setFormula(Element formula) {
		this.formula = formula;
	}

	public Node getConditional() {
		if (conditional == null) {
			return null;
		}
		return conditional.cloneNode(true);
	}

	public void setConditional(Node conditional) {
		this.conditional = conditional;
	}

	@Override
	public String toString() {
        String formulaString = "empty";
        if (formula != null) {
            try {
            	formulaString = AGDMUtils.domToString(formula);
            } 
            catch (AGDMException e) {
            }
        }
        Optional<Node> op = Optional.ofNullable(conditional);
        String condition = op.map( a -> {
			try {
				return AGDMUtils.domToString(a);
			} 
			catch (AGDMException e) {
				return "empty";
			}
		}).orElse("empty");
		return "Stiff source [field=" + field + ", formula=" + formulaString + ", condition=" + condition + "]";
	}
	
}
