/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.AGDM;

/** 
 * DPMException is the class Exception launched by the 
 * {@link AGDM PhysicalModels} interface.
 * The exceptions that this class return are:
 * AGDM001	XML document does not match XML schema
 * AGDM002	XML discretization schema not valid
 * AGDM003  Import discretization schema not exist
 * AGDM004	Received parameters do not match schema parameters
 * AGDM005  Not a valid XML document
 * AGDM006  Schema not applicable to this problem
 * AGDM007  Invalid discretization policy
 * AGDM008  Invalid simulation problem
 * AGDM009  Derivative variable not found
 * AGDM00X  External error
 * @author      ----
 * @version     ----
 */
@SuppressWarnings("serial")
public class AGDMException extends Exception {

    public static final String AGDM001 = "XML document does not match XML schema";
    public static final String AGDM002 = "XML discretization schema is not valid";
    public static final String AGDM003 = "Import discretization schema does not exist";
    public static final String AGDM004 = "Received parameters do not match schema parameters";
    public static final String AGDM005 = "Not a valid XML document";
    public static final String AGDM006 = "Schema not applicable to this problem";
    public static final String AGDM007 = "Invalid discretization policy";
    public static final String AGDM008 = "Invalid simulation problem";
    public static final String AGDM009 = "Derivative variable not found";
    public static final String AGDM00X = "External error";
    
    //External error message
    private String extMsg;
	  /** 
	     * Constructor for known messages.
	     *
	     * @param code  the message	     
	     */
    public AGDMException(String code) {
    	super(code);
    }
	  
	  /** 
	     * Constructor.
	     *
	     * @param code  the internal error message	    
	     * @param msg  the external error message
	     */
    public AGDMException(String code, String msg) {
		super(code);
        this.extMsg = msg;
    }
	  /** 
     * Returns the error code.
     *
     * @return the error code	     
     */
    public String getErrorCode() {
    	return super.getMessage();
    }
	  /** 
	     * Returns the error message as a string.
	     *
	     * @return the error message	     
	     */
    public String getMessage() {
    	if (extMsg == null) {
            return "AGDMException: " + super.getMessage();
    	}
    	else {
            return "AGDMException: " + super.getMessage() + " (" + extMsg + ")";
    	}
    }  
}
