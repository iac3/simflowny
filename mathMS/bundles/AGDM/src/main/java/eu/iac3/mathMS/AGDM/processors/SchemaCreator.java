/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.AGDM.processors;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;

import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import eu.iac3.mathMS.AGDM.AGDMException;
import eu.iac3.mathMS.AGDM.utils.AGDMUtils;
import eu.iac3.mathMS.AGDM.utils.CoordinateInfo;
import eu.iac3.mathMS.AGDM.utils.DiscretizationType;
import eu.iac3.mathMS.AGDM.utils.NodeWrap;
import eu.iac3.mathMS.AGDM.utils.ParticleNormalization;
import eu.iac3.mathMS.AGDM.utils.RuleInfo;
import eu.iac3.mathMS.AGDM.utils.PDE.AlgebraicSimplifications;
import eu.iac3.mathMS.AGDM.utils.PDE.EquationType;
import eu.iac3.mathMS.AGDM.utils.PDE.BoundarySchema;
import eu.iac3.mathMS.AGDM.utils.PDE.BoundarySchemaCondition;
import eu.iac3.mathMS.AGDM.utils.PDE.Derivative;
import eu.iac3.mathMS.AGDM.utils.PDE.DerivativeLevel;
import eu.iac3.mathMS.AGDM.utils.PDE.EquationTerms;
import eu.iac3.mathMS.AGDM.utils.PDE.EquationTermsLevel;
import eu.iac3.mathMS.AGDM.utils.PDE.Flux;
import eu.iac3.mathMS.AGDM.utils.PDE.MainSchemaLimit;
import eu.iac3.mathMS.AGDM.utils.PDE.MultiplicationTerm;
import eu.iac3.mathMS.AGDM.utils.PDE.MultiplicationTermsLevel;
import eu.iac3.mathMS.AGDM.utils.PDE.NormalizationLevel;
import eu.iac3.mathMS.AGDM.utils.PDE.OperatorInfoType;
import eu.iac3.mathMS.AGDM.utils.PDE.OperatorsInfo;
import eu.iac3.mathMS.AGDM.utils.PDE.ProcessInfo;
import eu.iac3.mathMS.AGDM.utils.PDE.Source;
import eu.iac3.mathMS.AGDM.utils.PDE.SpatialOperatorDiscretization;
import eu.iac3.mathMS.AGDM.utils.PDE.Species;
import eu.iac3.mathMS.AGDM.utils.PDE.StiffSource;
import eu.iac3.mathMS.documentManagerPlugin.DMException;
import eu.iac3.mathMS.documentManagerPlugin.DocumentManager;
import eu.iac3.mathMS.documentManagerPlugin.DocumentManagerImpl;
import eu.iac3.mathMS.simflownyUtilsPlugin.SUException;
import eu.iac3.mathMS.simflownyUtilsPlugin.SimflownyUtils;

public class SchemaCreator {
    public static final String NEIGHBOURSUFFIX = "_neighbour";
    
    HashMap<String, String> functionsLoaded;
    static HashMap<String, Integer> functionOrder;
    Element spatialDiscretizations;
    Element boundarySpatialDiscretizations;
    //Maximum stencil (updated when loading schemas)
    public static int maxStencil = 0;
    //Accumulate stencil in case derivatives on the same coordinate are recursive
    ArrayList<Integer> accumulatedStencil;
    //In case of crossed derivatives, the mathematical content of the derivative has to be computed at the corners
    boolean crossedDerivatives;
    //boolean particleSources;
    public static int maxStepStencil;
    boolean hasConservativeTermsMesh;
    boolean hasConservativeTermsParticles;
    boolean hasNonConservativeTermsMesh;
    boolean hasNonConservativeTermsParticles;
    int consStencilMesh;
    boolean hasDerivativeAuxiliaresMesh;
    boolean hasDerivativeAuxiliaresParticles;
    static ArrayList<String> auxTmpVarsWithTemplateVarMesh;
    static Element auxTmpVarTemplateMesh;
    boolean hasSpatialDiscretization;
    Element timeOutputVariable;
    
    /**
     * Constructor. Functions must be common on the whole problem.
     */
    public SchemaCreator() {
        functionsLoaded = new HashMap<String, String>();
        functionOrder = new HashMap<String, Integer>();
    }
    
    /**
     * Creates the PDE schema for the current region.
     * @param discProblem       The problem being discretized
     * @param problem           The continuous problem
     * @param schema            The discretization schema
     * @param pi                The process information
     * @param regionName		Region name
     * @return                  The schema
     * @throws AGDMException    AGDM00X - External error
     */
    public Node createPDESchema(Document discProblem, Document schema, Node problem, 
            ProcessInfo pi, String regionName) throws AGDMException {
        Document doc = problem.getOwnerDocument();
        OperatorsInfo opInfoMesh = pi.getOperatorsInfoMesh();
        HashMap<String, OperatorsInfo> opInfoParticleSpecies = pi.getOperatorsInfoParticles();
        maxStepStencil = 0;
        hasConservativeTermsMesh = pi.getOperatorsInfoMesh().getFluxTerms().getFluxes().size() > 0;
        hasConservativeTermsParticles = false;
        for (OperatorsInfo op: opInfoParticleSpecies.values()) {
        	hasConservativeTermsParticles = hasConservativeTermsParticles || op.getFluxTerms().getFluxes().size() > 0;
        }
        crossedDerivatives = false;
        hasDerivativeAuxiliaresMesh = opInfoMesh.hasAuxiliaryFieldDerivatives();
        hasDerivativeAuxiliaresParticles = false;
        for (OperatorsInfo op: opInfoParticleSpecies.values()) {
        	hasDerivativeAuxiliaresParticles = hasDerivativeAuxiliaresParticles || op.hasAuxiliaryFieldDerivatives();
        }
        consStencilMesh = 0;
        
        Element simml = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "simml");
        //Process every time step
        NodeList steps = schema.getDocumentElement().getElementsByTagNameNS(AGDMUtils.mmsUri, "step");        
        for (int i = 0; i < steps.getLength(); i++) {
        //for (int i = 0; i < 1; i++) {
        	Element step = (Element) steps.item(i);
        	Element substep = AGDMUtils.createElement(doc, AGDMUtils.mmsUri, "substep");
        	System.out.println("Step " + i);
        	AGDMUtils.addParameterSuffixNames(schema, step, regionName);
        	pi.setCurrentTimeStep(step);
        	pi.setCurrentTimeStepIndex(i);
        	substep.appendChild(processTimeStep(discProblem, problem, (Element) step, opInfoMesh, opInfoParticleSpecies, pi, regionName, i + 1 == steps.getLength()));
        	substep.setAttribute("factor", step.getElementsByTagNameNS(AGDMUtils.mmsUri, "timestepFactor").item(0).getTextContent());
            simml.appendChild(substep);
        }
        //Append dissipation parameters
        addDissipationParameters(discProblem, schema, opInfoMesh.getEvolutionFields(), "meshDissipation");
        for (OperatorsInfo op: opInfoParticleSpecies.values()) {
        	addDissipationParameters(discProblem, schema, op.getEvolutionFields(), "particleDissipation");
        }
        //Append velocity parameters
        addVelocityParameter(discProblem, pi);
                
        return simml;
    }
    
    /**
     * Process an individual time step.
     * @param discProblem       	The problem being discretized
     * @param problem           	The continuous problem
     * @param step              	The current time step
     * @param opInfoMesh        	The operator information for mesh fields
     * @param opInfoParticleSpecies The operator information for particle fields
     * @param pi                	The process information
     * @param regionName			Region name
     * @param isLastStep			Check if is the last substep	
     * @return                  	The discretized region
     * @throws AGDMException    	AGDM00X - External error
     */
    private DocumentFragment processTimeStep(Document discProblem, Node problem, Element step, OperatorsInfo opInfoMesh, HashMap<String, OperatorsInfo> opInfoParticleSpecies, 
    		ProcessInfo pi, String regionName, boolean isLastStep) throws AGDMException {
        Document doc = problem.getOwnerDocument();
        DocumentFragment result = problem.getOwnerDocument().createDocumentFragment();
        hasNonConservativeTermsMesh = opInfoMesh.getDerivativeLevels().size() > 0;
        hasNonConservativeTermsParticles = false;
        for (OperatorsInfo op: opInfoParticleSpecies.values()) {
        	hasNonConservativeTermsParticles = hasNonConservativeTermsParticles || op.getDerivativeLevels().size() > 0;	
        }
        hasSpatialDiscretization = hasSpatialDiscretization(step);
        
        //Calculate accumulated stencil
        int maxParticleDerLevels = 0;
        for (OperatorsInfo op: opInfoParticleSpecies.values()) {
        	maxParticleDerLevels = Math.max(maxParticleDerLevels, op.getDerivativeLevels().size());
        }
        calculateAccumulatedStencilAndCrossedDerivatives(opInfoMesh.getDerivativeLevels(), opInfoMesh.getMultiplicationLevels(), maxParticleDerLevels, step);
        
        //TODO finalization condition
        
        timeOutputVariable = getTimeOutputVariable(step);
        Element spatialOutputVariable = getSpatialOutputVariable(step);
        Element nonConsInputVariable = getNonConsInputVariable(doc, step);
        Element previousVariable = getPreviousVariable(step.getOwnerDocument(), step);
        
        int opInfoStencil = opInfoMesh.getStencil();
        int dissipationStencil = getMeshDissipationStencil(step);
        //Structures for optimization on algebraic terms
        Hashtable<NodeWrap, String> usedTermsMesh = new Hashtable<NodeWrap, String>();
        Hashtable<NodeWrap, String> usedTermsParticles = new Hashtable<NodeWrap, String>();
        AlgebraicSimplifications simplificationsMesh = new AlgebraicSimplifications();
        AlgebraicSimplifications simplificationsParticles = new AlgebraicSimplifications();
        
        //Load particle kernel
        loadKernel(problem, discProblem, step, pi);
        
        //Particle-mesh interaction interpolations
        Element interpolation = createParticleMeshInteraction(step, discProblem, problem, opInfoMesh, opInfoParticleSpecies, pi, previousVariable, getPreviousPositionVariable(step.getOwnerDocument(), step), OperatorInfoType.field);
    	if (interpolation.hasChildNodes()) {
    		result.appendChild(doc.importNode(interpolation, true));
    	}
        //Prestep
        DocumentFragment particlePrestep = null;
        DocumentFragment meshPrestep = null;
        Element iterOverCells = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "iterateOverCells");
    	if (hasSpatialDiscretization) {
	        //Mesh
	        Element prestep = getPrestep(step, DiscretizationType.mesh);
	        if (!pi.getMeshFields().isEmpty() && prestep != null) {
	        	AGDMUtils.addParameterSuffixNames(step.getOwnerDocument(), prestep, regionName);
	        	pi.setDiscretizationType(DiscretizationType.mesh);
	        	meshPrestep = ExecutionFlow.processSimml(prestep, discProblem, problem, pi, false);		
	        }
	        //Particles
	        prestep = getPrestep(step, DiscretizationType.particles);
	        if (!pi.getParticleSpecies().isEmpty() && prestep != null) {
	        	AGDMUtils.addParameterSuffixNames(step.getOwnerDocument(), prestep, regionName);
	        	pi.setDiscretizationType(DiscretizationType.particles);
	        	particlePrestep = ExecutionFlow.processSimml(prestep, discProblem, problem, pi, false);
	        }
	        //Unify presteps
	        if (meshPrestep != null && particlePrestep != null) {
	        	result.appendChild(AGDMUtils.unifyCodeFragments(meshPrestep.cloneNode(true), particlePrestep));
	        }
	        else {
	        	if (meshPrestep != null) {
	        		result.appendChild(meshPrestep.cloneNode(true));
	        	}
	        	else {
	            	if (particlePrestep != null) {
	            		result.appendChild(particlePrestep.cloneNode(true));
	            	}
	        	}
	        }
	        
	        //First block
	        iterOverCells.setAttribute("stencilAtt", String.valueOf(0));
	        iterOverCells.setAttribute("appendAtt", String.valueOf(accumulatedStencil.get(0)));
	        //Mesh
	        //First order algebraic terms
	        if (hasNonConservativeTermsMesh) {
	        	pi.setDiscretizationType(DiscretizationType.mesh);
	        	iterOverCells.appendChild(createAlgebraicTerms(doc, opInfoMesh.getDerivativeLevels().get(0), pi, nonConsInputVariable, usedTermsMesh, simplificationsMesh));
	        }
	        insertCornerCalculation(iterOverCells);
	        //Fluxes. If stencil is 0, the fluxes will be defined in the RHS loop (last derivative loop).
	        if (consStencilMesh > 0) {
	         	pi.setDiscretizationType(DiscretizationType.mesh);
	        	iterOverCells.appendChild(createFluxes(discProblem, problem, opInfoMesh, pi, step, false));
	        }
	        //Particles
	        for (Entry<String, OperatorsInfo> entry : opInfoParticleSpecies.entrySet()) {
	        	String speciesName = entry.getKey();
	        	OperatorsInfo opInfoParticles = entry.getValue();
	            Element iterOverParticles = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "iterateOverParticles");
	            iterOverParticles.setAttribute("speciesNameAtt", speciesName);
	    		ProcessInfo newPi = new ProcessInfo(pi);
	    		newPi.setCurrentSpecies(entry.getKey());
	            //First order algebraic terms
	            if (hasNonConservativeTermsParticles) {
	            	newPi.setDiscretizationType(DiscretizationType.particles);
	             	iterOverParticles.appendChild(createAlgebraicTerms(doc, opInfoParticles.getDerivativeLevels().get(0), newPi, nonConsInputVariable, usedTermsParticles, simplificationsParticles));
	            }
	            insertCornerCalculation(iterOverCells);
	            newPi.setDiscretizationType(DiscretizationType.particles);
	         	iterOverParticles.appendChild(createFluxes(discProblem, problem, opInfoParticles, newPi, step, false));
	            if (iterOverParticles.hasChildNodes()) {
	                iterOverCells.appendChild(iterOverParticles);
	            }
	        }
	        result.appendChild(iterOverCells);
    	}
    	
    	

        //Second block (repetitive)
        Element iterOverCells2 = null;
        //The operators Info may need different number of loops. All must be aligned at the end.
        //Then calculate the starting loop for mesh and particle species.
    	int maxMesh = Math.max(Math.max(opInfoMesh.getDerivativeLevels().size(), opInfoMesh.getFluxVariables().size() > 0 ? 1 : 0), opInfoMesh.getSourceTerms().getSources().size() > 0 ? 1 : 0);
    	int discretizationBlocks = Math.max(maxMesh, 1);
    	HashMap<String, Integer> maxParticles = new HashMap<String, Integer>();
    	for (Entry<String, OperatorsInfo> entry : opInfoParticleSpecies.entrySet()) {
    		String speciesName = entry.getKey();
        	OperatorsInfo opInfoParticles = entry.getValue();
        	int maxParticleSpecies = Math.max(Math.max(opInfoParticles.getDerivativeLevels().size(), opInfoParticles.getFluxVariables().size() > 0 ? 1 : 0), opInfoParticles.getSourceTerms().getSources().size() > 0 ? 1 : 0); 
        	maxParticles.put(speciesName, maxParticleSpecies);
        	discretizationBlocks = Math.max(discretizationBlocks, maxParticleSpecies);
    	}
    	int startMesh = discretizationBlocks - maxMesh;
    	HashMap<String, Integer> startParticlesSpecies = new HashMap<String, Integer>();
    	for (String speciesName : opInfoParticleSpecies.keySet()) {
    		startParticlesSpecies.put(speciesName, discretizationBlocks - maxParticles.get(speciesName));
    	}
        for (int i = 0; i < discretizationBlocks; i++) {
        	boolean lastLoop = (i + 1 == discretizationBlocks);
            usedTermsMesh = new Hashtable<NodeWrap, String>();
            usedTermsParticles = new Hashtable<NodeWrap, String>();
            iterOverCells2 = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "iterateOverCells");
            //rule info
            RuleInfo ri = new RuleInfo();
            ri.setProcessInfo(pi);
            ri.setIndividualField(false);
            ri.setIndividualCoord(false);
            ri.setFunction(false);
            ri.setAuxiliaryCDAdded(false);
            ri.setAuxiliaryFluxesAdded(false);
            ri.setUseBaseCoordinates(false);
            ri.setExtrapolation(pi.getExtrapolationMethod() != null && pi.getIncomFields() != null);

            //Mesh
    		if (i >= startMesh && opInfoMesh.getEvolutionFields().size() > 0) {
    			pi.setDiscretizationType(DiscretizationType.mesh);
                ri.setProcessInfo(pi);    
                //Nth order derivative calculation
	        	if (hasNonConservativeTermsMesh && hasSpatialDiscretization) {
	    			ArrayList<Element> nonConsInputVariables = getNonConsFunctionInputVariables((Element) pi.getCurrentTimeStep(), DiscretizationType.mesh);
                    OperatorsInfo opInfoVariables = new OperatorsInfo(opInfoMesh);
                    HashSet<String> sourceVariables = new HashSet<String>();
                    sourceVariables.addAll(opInfoVariables.getEvolutionFields());
                    sourceVariables.addAll(opInfoVariables.getNonConservativeFields());
                    sourceVariables.addAll(opInfoVariables.getConservativeSourceVariables());
                    opInfoVariables.onlyDependent(sourceVariables, false, null);
	        		iterOverCells2.appendChild(createDerivativeCalculation(doc, opInfoMesh, opInfoVariables.getDerivativeLevels().get(i - startMesh), discProblem, problem, pi, nonConsInputVariables, i - startMesh, 
	        				lastLoop, simplificationsMesh, lastLoop, null));
	        	}
	            //No last block
	            if (!lastLoop) {
	            	if (hasSpatialDiscretization) {
		                //Reset simplification terms for the next level
		                simplificationsMesh = new AlgebraicSimplifications();
		                //Multiplication of derivatives and algebraic terms
		                iterOverCells2.appendChild(createDerivativeMultiplication(doc, opInfoMesh, opInfoMesh.getMultiplicationLevels(), opInfoMesh.getDerivativeLevels(), pi, nonConsInputVariable, OperatorInfoType.field, i - startMesh, lastLoop));
		                //Nth order order algebraic terms
		                if (hasNonConservativeTermsMesh) {
		                	iterOverCells2.appendChild(createAlgebraicTerms(doc, opInfoMesh.getDerivativeLevels().get(i + 1 - startMesh), pi, nonConsInputVariable, usedTermsMesh, simplificationsMesh));
		                }
		                insertCornerCalculation(iterOverCells2);
	            	}
	            }
	            //Last block
	            else {
	            	//Fluxes if stencil is 0
	            	if (hasSpatialDiscretization) {
		                if (consStencilMesh == 0 && hasConservativeTermsMesh) {
		                	iterOverCells2.appendChild(createFluxes(discProblem, problem, opInfoMesh, pi, step, true));
		                }
		                //Auxiliary equations
		            	OperatorsInfo opInfoVariables = new OperatorsInfo(opInfoMesh);
		            	HashSet<String> usedVariables = opInfoVariables.getNonConservativeFields();
		            	usedVariables.addAll(opInfoVariables.getConservativeSourceVariables());
		            	usedVariables.addAll(opInfoVariables.getStiffSourceVariables());
		            	opInfoVariables.onlyDependent(usedVariables, true, opInfoVariables.getNonConservativeSourceCommonConditional());
		            	
		                iterOverCells2.appendChild(createAuxiliaryVariableEquations(discProblem, problem, opInfoVariables, opInfoMesh.getDerivativeLevels(), opInfoMesh.getMultiplicationLevels(), opInfoVariables.getDerivativeLevels(), opInfoVariables.getMultiplicationLevels(), pi, nonConsInputVariable, i - startMesh, false));
		                //Sources (they will only be in the last level)
		                if (hasNonConservativeTermsMesh) {
		                	iterOverCells2.appendChild(createSourcesMesh(doc, opInfoMesh.getDerivativeLevels().get(i - startMesh), pi, nonConsInputVariable, OperatorInfoType.field));
		                }
		                //Conservative sources
		                iterOverCells2.appendChild(createConservativeSourcesMesh(doc, opInfoMesh, pi, nonConsInputVariable, step));
		                //Conservative discretization
		                iterOverCells2.appendChild(createConservativeDiscretization(doc, opInfoMesh, pi, discProblem, problem, step, i - startMesh, consStencilMesh == 0));
		                        
		                //Multiplication of derivatives, algebraic terms and auxiliary definitions
		                iterOverCells2.appendChild(createDerivativeMultiplication(doc, opInfoMesh, opInfoMesh.getMultiplicationLevels(), opInfoMesh.getDerivativeLevels(), pi, nonConsInputVariable, OperatorInfoType.field, i - startMesh, lastLoop));
		                //Equation term summation
		                iterOverCells2.appendChild(createEquationSummationMesh(doc, opInfoMesh, pi, spatialOutputVariable, OperatorInfoType.field, false));
		                	          
		                //Stiffness RHS
		                iterOverCells2.appendChild(stiffRHS(doc, opInfoMesh, pi, step, previousVariable));
		                
		                //Interactions
		                Element interaction = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "regionInteraction");
		                Element variableBoundary = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "variable");
		                Element variableMath = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "math");
		                variableMath.appendChild(doc.importNode(spatialOutputVariable.cloneNode(true), true));
		                variableBoundary.appendChild(variableMath);
		                Element timeslice = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "timeSlice");
		                Element preVariableMath = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "math");
		                preVariableMath.appendChild(doc.importNode(nonConsInputVariable.cloneNode(true), true));
		                timeslice.appendChild(preVariableMath);
		                interaction.appendChild(variableBoundary);
		                interaction.appendChild(timeslice);
		                iterOverCells2.appendChild(doc.importNode(
		                        RegionInteraction.processRegionInteraction(discProblem, problem, interaction, ri, i - startMesh, opInfoMesh, simplificationsMesh, null), true));
		                
		                //Dissipative terms
		                iterOverCells2.appendChild(
		                        createMeshDissipation(doc, opInfoMesh.getEvolutionFields(), pi, spatialOutputVariable, discProblem, problem, step));
		                
	            	}
	                //Time solving
	                iterOverCells2.appendChild(solveTime(doc, opInfoMesh, pi, discProblem, problem, step));
	                
	                //Stiffness
	                iterOverCells2.appendChild(solveStiff(doc, opInfoMesh, pi, step, timeOutputVariable));
	                
	                if (hasSpatialDiscretization) {
	                	iterOverCells2.setAttribute("subStepSyncAtt", "true");	
	                }
	                else {
	                	iterOverCells2.setAttribute("subStepSyncAtt", "false");
	                }	                
	            }
    		}
            //Particles
            for (Entry<String, OperatorsInfo> entry : opInfoParticleSpecies.entrySet()) {
            	String speciesName = entry.getKey();
        		ProcessInfo newPi = new ProcessInfo(pi);
        		newPi.setCurrentSpecies(speciesName);
        		newPi.setDiscretizationType(DiscretizationType.particles);
            	OperatorsInfo opInfoParticles = entry.getValue();
            	int startParticles = startParticlesSpecies.get(speciesName);
            	boolean hasAuxiliaryVarDerivativeParticles = opInfoParticles.hasAuxiliaryVarDerivative(i - startParticles);
            	if (i >= startParticles && opInfoParticles.getEvolutionFields().size() > 0) {
                    Element iterateOverInteractionsDers = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "iterateOverInteractions");
                    iterateOverInteractionsDers.setAttribute("compactSupportRatioAtt", String.valueOf(newPi.getCompactSupportRatio()));
                    iterateOverInteractionsDers.setAttribute("speciesNameAtt", speciesName);
                    Element iterOverParticles = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "iterateOverParticles");
                    iterOverParticles.setAttribute("speciesNameAtt", speciesName);
                	boolean hasDissipation = step.getElementsByTagNameNS(AGDMUtils.mmsUri, "particleDissipation").getLength() > 0;
        			ArrayList<ParticleNormalization> normalizationInfo = generateNormalizationInfo(doc, step, newPi, opInfoParticles, i - startParticles, lastLoop, hasDissipation);
                    ri.setProcessInfo(newPi);
                    DocumentFragment tmpDers1 = doc.createDocumentFragment();
            		if (hasSpatialDiscretization) {
	                    //Initialize normalizations
	                	iterOverParticles.appendChild(initializeNormalizationVars(doc, step, newPi, spatialOutputVariable, opInfoParticles, i - startParticles, OperatorInfoType.field, lastLoop, hasDissipation));
	                	//Calculate normalization
	                	if (!hasAuxiliaryVarDerivativeParticles) {
	                		tmpDers1.appendChild(calculateNormalizationFunctions(doc, discProblem, problem, step, newPi, opInfoParticles, i - startParticles, normalizationInfo, lastLoop));
	                	}
	                    //Nth order derivative calculation
	                	iterOverParticles.appendChild(initializeDerivativeVariables(doc, opInfoParticles, opInfoParticles.getDerivativeLevels().get(i - startParticles), newPi, i - startParticles));
	    	        	if (hasNonConservativeTermsParticles) {
	    	    			ArrayList<Element> nonConsInputVariables = getNonConsFunctionInputVariables((Element) newPi.getCurrentTimeStep(), DiscretizationType.particles);
	                        OperatorsInfo opInfoVariables = new OperatorsInfo(opInfoParticles);
	                        HashSet<String> sourceVariables = new HashSet<String>();
	                        sourceVariables.addAll(opInfoVariables.getEvolutionFields());
	                        sourceVariables.addAll(opInfoVariables.getNonConservativeFields());
	                        sourceVariables.addAll(opInfoVariables.getVelocityVariables(newPi.getParticleVelocities().values()));
	                        opInfoVariables.onlyDependent(sourceVariables, false, null);
	    	        		iterateOverInteractionsDers.appendChild(createDerivativeCalculation(doc, opInfoParticles, opInfoVariables.getDerivativeLevels().get(i - startParticles), discProblem, problem, newPi, nonConsInputVariables, i - startParticles, 
	    	        				lastLoop, simplificationsParticles, lastLoop, null));
	    	        	}
            		}
    	            //No last block
    	            if (!lastLoop) {
    	            	if (hasSpatialDiscretization) {
	    	                //Reset simplification terms for the next level
	    	                simplificationsParticles = new AlgebraicSimplifications();
	
	    	                //Nth order order algebraic terms
	    	                if (hasNonConservativeTermsParticles) {
	    	                	iterateOverInteractionsDers.appendChild(createAlgebraicTerms(doc, opInfoParticles.getDerivativeLevels().get(i + 1 - startParticles), newPi, nonConsInputVariable, usedTermsParticles, simplificationsParticles));
	    	                }
	    	                if (iterateOverInteractionsDers.hasChildNodes()) {
	    	                	//Add particle position
	    	                    Element particlePosition = createPositionReference(doc, getPreviousPositionVariable(doc, step), newPi);
	    	                	iterateOverInteractionsDers.insertBefore(particlePosition, iterateOverInteractionsDers.getFirstChild());
	    	                	iterOverParticles.appendChild(iterateOverInteractionsDers);
	    	                }
	    	                //Normalization
	    	                iterOverParticles.appendChild(securizeNormalizations(doc, step, newPi, opInfoParticles, i - startParticles, lastLoop, false));
	    	                iterOverParticles.appendChild(createNormalizeRHSFunctions(doc, step, newPi, spatialOutputVariable, opInfoParticles, i - startParticles, normalizationInfo, opInfoParticles.getNormalizationLevels(), false, false, true));
	    	                //Multiplication of derivatives and algebraic terms
	    	                iterOverParticles.appendChild(createDerivativeMultiplication(doc, opInfoParticles, opInfoParticles.getMultiplicationLevels(), opInfoParticles.getDerivativeLevels(), newPi, nonConsInputVariable, OperatorInfoType.field, i - startParticles, lastLoop));
    	            	}
    	            }
    	            //Last block
    	            else {
    	            	//Cumulative vars initialization
    	            	iterOverParticles.appendChild(initializeRHS(doc, newPi, spatialOutputVariable, opInfoParticles, i - startParticles, OperatorInfoType.field, hasDissipation));
    	             	if (newPi.getParticleVelocities().size() > 0) {
    	             		iterOverParticles.appendChild(initializePositionCorrection(doc, newPi, problem, discProblem));
    	             	}
    	             	
    	            	//Auxiliary variables without derivatives
      	            	OperatorsInfo opInfoVariables = new OperatorsInfo(opInfoParticles);
    	            	HashSet<String> auxVariables = opInfoVariables.getConservativeSourceVariables();
    	            	opInfoVariables.onlyDependent(auxVariables, true, opInfoVariables.getConservativeSourceCommonConditional());
    	            	Element neighbourVariable = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "neighbourParticle");
    	            	neighbourVariable.appendChild(nonConsInputVariable);
    	            	iterateOverInteractionsDers.appendChild(createAuxiliaryVariableEquations(discProblem, problem, opInfoVariables, opInfoParticles.getDerivativeLevels(), opInfoParticles.getMultiplicationLevels(), opInfoVariables.getDerivativeLevels(), opInfoVariables.getMultiplicationLevels(), newPi, neighbourVariable, i - startParticles, true));

    	            	//Derivatives (loop neighbours) (previously added)

    	            	//Conservative fluxes (loop neighbours)
    	                iterateOverInteractionsDers.appendChild(createConservativeDiscretization(doc, opInfoParticles, newPi, discProblem, problem, step, i - startParticles, false));
    	                
    	            	//Conservative sources (loop neighbours)
		                iterateOverInteractionsDers.appendChild(createConservativeSourcesParticles(doc, opInfoParticles, newPi, discProblem, problem, step));

    	            	//Advective terms (loop neighbours)
		                iterateOverInteractionsDers.appendChild(createAdvectiveTerms(doc, discProblem, problem, step, newPi));
    	                
    	            	//position correction (loop neighbours)
		                DocumentFragment tmpDers2 = doc.createDocumentFragment();
		                tmpDers2.appendChild(createParticleCorrection(doc, newPi, problem, step));
    	                
    	            	//Dissipation terms (loop neighbours)
		                tmpDers2.appendChild(createParticleDissipation(doc, opInfoParticles.getEvolutionFields(), newPi, spatialOutputVariable, discProblem, problem, step));
    	            	
    	                //Adding conditional iteration over interactions if necessary (when there is only dissipation terms or position correction)
    	                Element particlePosition = createPositionReference(doc, getPreviousPositionVariable(doc, step), newPi);
    	                if (iterateOverInteractionsDers.hasChildNodes()) {
    	                	iterateOverInteractionsDers.insertBefore(tmpDers1, iterateOverInteractionsDers.getFirstChild());
    	                	iterateOverInteractionsDers.appendChild(tmpDers2);
    	                	iterateOverInteractionsDers.insertBefore(particlePosition, iterateOverInteractionsDers.getFirstChild());
    	                	iterOverParticles.appendChild(iterateOverInteractionsDers);	
    	                }
    	                else if (tmpDers2.hasChildNodes()) {
    	                	Element condition = createConditionOnInteractions(doc, newPi, step, opInfoParticles.getEvolutionFields());
    	                	Element then = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "then");
    	                	condition.appendChild(then);
    	                	iterateOverInteractionsDers.insertBefore(tmpDers1, iterateOverInteractionsDers.getFirstChild());
    	                	iterateOverInteractionsDers.appendChild(tmpDers2);
    	                	iterateOverInteractionsDers.insertBefore(particlePosition, iterateOverInteractionsDers.getFirstChild());
    	                	then.appendChild(iterateOverInteractionsDers);
    	                	iterOverParticles.appendChild(condition);	
    	                }
    	                
    	                //Normalizations
    	               	if (hasNonConservativeTermsParticles || hasConservativeTermsParticles || hasDissipation || hasAuxiliaryVarDerivativeParticles) {
	            			iterOverParticles.appendChild(securizeNormalizations(doc, step, newPi, opInfoParticles, i - startParticles, lastLoop, hasDissipation));
    	            	}
    	            	if (hasNonConservativeTermsParticles || hasConservativeTermsParticles || hasDissipation) {
    		                iterOverParticles.appendChild(createNormalizeRHSFunctions(doc, step, newPi, spatialOutputVariable, opInfoParticles, i - startParticles, normalizationInfo, opInfoParticles.getNormalizationLevels(), true, hasDissipation, !hasAuxiliaryVarDerivativeParticles));
    	            	}
		                if (hasAuxiliaryVarDerivativeParticles) {
        		            iterOverParticles.appendChild(createNormalizeAuxiliaryVarRHSFunctions(doc, step, opInfoParticles, i - startParticles, normalizationInfo));
		                }
		                
    	            	//Auxiliary variables with derivatives
	  	            	opInfoVariables = new OperatorsInfo(opInfoParticles);
    	            	auxVariables = opInfoVariables.getNonConservativeSourceVariables();
    	            	auxVariables.addAll(opInfoVariables.getVelocityVariables(newPi.getParticleVelocities().values()));
    	            	auxVariables.addAll(opInfoVariables.getNonConservativeFields());
    	            	opInfoVariables.onlyDependent(auxVariables, true, opInfoVariables.getNonConservativeSourceCommonConditional());
    	            	iterOverParticles.appendChild(createAuxiliaryVariableEquations(discProblem, problem, opInfoVariables, opInfoParticles.getDerivativeLevels(), opInfoParticles.getMultiplicationLevels(), opInfoVariables.getDerivativeLevels(), opInfoVariables.getMultiplicationLevels(), newPi, nonConsInputVariable, i - startParticles, false));

    	            	//Non-conservative terms
		                iterOverParticles.appendChild(createNonConsSourcesParticles(doc, opInfoParticles.getDerivativeLevels().get(i - startParticles), newPi, previousVariable.cloneNode(true), nonConsInputVariable, OperatorInfoType.field));
		                
    	            	//Couple equations
		                iterOverParticles.appendChild(createDerivativeMultiplication(doc, opInfoParticles, opInfoParticles.getMultiplicationLevels(), opInfoParticles.getDerivativeLevels(), newPi, nonConsInputVariable, OperatorInfoType.field, i - startParticles, lastLoop));
		                iterOverParticles.appendChild(createEquationSummationParticles(doc, opInfoParticles, newPi, spatialOutputVariable, OperatorInfoType.field, hasDissipation));
		                
		                //Stiffness RHS
		                iterOverParticles.appendChild(stiffRHS(doc, opInfoParticles, pi, step, previousVariable));
		                
		                //Stiffness
		                iterOverParticles.appendChild(solveStiff(doc, opInfoParticles, pi, step, timeOutputVariable));
		                
    	                //Normalization for position correction
    	            	//Check if there are sources in the equation
    	                boolean hasSources = opInfoParticles.getSourceTerms().getSources().size() > 0;
    	                if (opInfoParticles.getDerivativeLevels().size() > i - startParticles) {
    	                    ArrayList<Derivative> derivatives = opInfoParticles.getDerivativeLevels().get(i - startParticles).getDerivatives();
    	                    for (int j = 0; j < derivatives.size() && !hasSources; j++) {
    	                        if (!derivatives.get(j).getDerivativeTerm().isPresent()) {
    	                        	hasSources = true;
    	                        }
    	                    }
    	                }
    	             	if (newPi.getParticleVelocities().size() > 0) {
    	             		iterOverParticles.appendChild(createPositionCorrectionNormalization(doc, newPi, problem, discProblem, step, hasSources));
    	             	}
		                
    	            	//Time solving
    	                iterOverParticles.appendChild(solveTime(doc, opInfoParticles, newPi, discProblem, problem, step));
    	                    	                
    	            	//New positions
    	                iterOverParticles.appendChild(calculateNewPosition(doc, newPi, problem, discProblem, step));
    	            	
		                
    	            }
    	            if (iterOverParticles.hasChildNodes()) {
    	            	iterOverCells2.appendChild(iterOverParticles);
    	            	if (lastLoop) {
    	                    if (hasSpatialDiscretization) {
    		                	iterOverCells2.setAttribute("subStepSyncAtt", "true");	
    		                }
    		                else {
    		                	iterOverCells2.setAttribute("subStepSyncAtt", "false");
    		                }
    	            	}
    	            }
        		}
            }
            
            // Stencil attribute
        	int stencil = 0;
        	if (hasSpatialDiscretization && (hasNonConservativeTermsParticles || hasConservativeTermsParticles)) {
        		stencil = 1;
        	}
        	if (hasSpatialDiscretization && hasNonConservativeTermsMesh) {
        		stencil = getLevelStencil(opInfoMesh.getDerivativeLevels().get(i - startMesh).getDerivatives()); 
        	}
        	if (hasSpatialDiscretization && hasConservativeTermsMesh) {
        		stencil = Math.max(stencil, consStencilMesh);
        	}
            if (i + 1 - startMesh < discretizationBlocks || !hasSpatialDiscretization) {
                iterOverCells2.setAttribute("stencilAtt", String.valueOf(stencil));
            }
            else {
                iterOverCells2.setAttribute("stencilAtt", String.valueOf(Math.max(dissipationStencil, stencil)));
            }
            //Append attribute
            if (hasSpatialDiscretization) {
	            if (i + 2 - startMesh == discretizationBlocks) {
	                iterOverCells2.setAttribute("appendAtt", String.valueOf(Math.max(getMeshDissipationStencil(step), accumulatedStencil.get(i + 1))));    
	            } 
	            else {
	                if (i >= startMesh && i + 1 - startMesh < discretizationBlocks && opInfoMesh.getDerivativeLevels().get(i + 1 - startMesh) != null) {
	                    iterOverCells2.setAttribute("appendAtt", String.valueOf(accumulatedStencil.get(i + 1)));    
	                }
	            }
	            insertCornerCalculation(iterOverCells2);
            }
            result.appendChild(iterOverCells2);
        }
    	if (hasNonConservativeTermsParticles || hasConservativeTermsParticles) {
    		maxStepStencil = Math.max(maxStepStencil, 1);
    	}
        maxStepStencil = Math.max(Math.max(Math.max(Math.max(opInfoStencil, dissipationStencil), maxStepStencil), consStencilMesh), Math.max(getPreStepMaxStencil(particlePrestep), getPreStepMaxStencil(meshPrestep)));

        //Particle movement if needed
        Element iterOverCellsMovement = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "iterateOverCells");
        for (String speciesName: opInfoParticleSpecies.keySet()) {
        	ProcessInfo newPi = new ProcessInfo(pi);
        	newPi.setCurrentSpecies(speciesName);
        	newPi.setDiscretizationType(DiscretizationType.particles);
        	if (newPi.getParticleVelocities().size() > 0) {
        		iterOverCellsMovement.appendChild(createParticleMovement(problem, step, newPi));
        	}
        }
        iterOverCellsMovement.setAttribute("stencilAtt", "0");
	    if (iterOverCellsMovement.hasChildNodes()) {
	        result.appendChild(iterOverCellsMovement);
        }
        
        //If there are auxiliaryFields with derivative terms, the boundaries for the fields must be calculated
        if (hasDerivativeAuxiliaresMesh || hasDerivativeAuxiliaresParticles) {
        	//Boundaries and extrapolations
            Element boundary = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "boundary");
            Element variableBoundary = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "variable");
            Element variableMath = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "math");
            variableMath.appendChild(doc.importNode(timeOutputVariable.cloneNode(true), true));
            variableBoundary.appendChild(variableMath);
            Element previousVariableBoundary = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "previousVariable");
            Element preVariableMath = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "math");
            preVariableMath.appendChild(doc.importNode(nonConsInputVariable.cloneNode(true), true));
            previousVariableBoundary.appendChild(preVariableMath);
            Element stencil = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "stencil");
            stencil.setTextContent(String.valueOf(maxStencil));
            boundary.appendChild(variableBoundary);
            boundary.appendChild(previousVariableBoundary);
            boundary.appendChild(stencil);
            //Add the processed boundary
            result.appendChild(doc.importNode(Boundary.processBoundary(discProblem, problem, boundary, pi, isLastStep, false, false, true), true));
        } 
        else {
        	Element boundary = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "boundary");
        	result.appendChild(boundary);
        }
        
        //Auxiliary field blocks
        //Calculate accumulated stencil
        maxParticleDerLevels = 0;
        for (OperatorsInfo op: opInfoParticleSpecies.values()) {
        	maxParticleDerLevels = Math.max(maxParticleDerLevels, op.getAuxiliaryDerivativeLevels().size());
        }
        calculateAccumulatedStencilAndCrossedDerivatives(opInfoMesh.getAuxiliaryDerivativeLevels(), opInfoMesh.getAuxiliaryMultiplicationLevels(), maxParticleDerLevels, null);
        hasNonConservativeTermsMesh = opInfoMesh.getAuxiliaryDerivativeLevels().size() > 0;
        hasNonConservativeTermsParticles = false;
        for (OperatorsInfo op: opInfoParticleSpecies.values()) {
        	hasNonConservativeTermsParticles = hasNonConservativeTermsParticles || op.getAuxiliaryDerivativeLevels().size() > 0;	
        }
        Element iterOverCells3 = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "iterateOverCells");
        iterOverCells3.setAttribute("stencilAtt", String.valueOf(0));
        if (accumulatedStencil.size() > 0) {
        	iterOverCells3.setAttribute("appendAtt", String.valueOf(accumulatedStencil.get(0)));
        }
        //If there are no boundary loop previous to that one, calculate auxiliaries only in internal domain (including internal boundaries)
        if (!hasDerivativeAuxiliaresMesh && !hasDerivativeAuxiliaresParticles) {
        	iterOverCells3.setAttribute("excludePhysicalBoundariesAtt", "true");
        }
        //Particle-mesh interaction interpolations
		NodeList instructions = createParticleMeshInteraction(step, discProblem, problem, opInfoMesh, opInfoParticleSpecies, pi, timeOutputVariable, (Element) convertToPosition(timeOutputVariable), OperatorInfoType.auxiliaryField).getChildNodes();
		for (int i = 0; i < instructions.getLength(); i++) {
			iterOverCells3.appendChild(doc.importNode(instructions.item(i).cloneNode(true), true));
		}
               
        //Auxiliary variables (Only the ones containing sources)
        usedTermsMesh = new Hashtable<NodeWrap, String>();
        simplificationsMesh = new AlgebraicSimplifications();
        //Mesh
    	pi.setDiscretizationType(DiscretizationType.mesh);
    	OperatorsInfo opInfoAuxFields = new OperatorsInfo(opInfoMesh);
    	opInfoAuxFields.removeAlgorithmicAuxFieldDerivativeDependent();
    	opInfoAuxFields.onlyDependent(opInfoAuxFields.getAuxiliaryFieldAlgorithmVariables(), true, opInfoAuxFields.getNonDerivativeDependentAuxiliaryFieldCommonConditional());
    	if (opInfoAuxFields.hasDerivatives(OperatorInfoType.auxiliaryVariable)) {
            throw new AGDMException(AGDMException.AGDM005, "Auxiliary variables used in Auxiliary fields cannot be defined using derivatives.");
    	}
        iterOverCells3.appendChild(createAuxiliaryVariableEquations(discProblem, problem, opInfoAuxFields, opInfoMesh.getAuxiliaryDerivativeLevels(), opInfoMesh.getAuxiliaryMultiplicationLevels(), opInfoAuxFields.getAuxiliaryDerivativeLevels(), opInfoAuxFields.getAuxiliaryMultiplicationLevels(), pi, (Element) timeOutputVariable.cloneNode(true), 0, false));
        //Non derivative auxiliary equations
       	OperatorsInfo opInfoVariables = new OperatorsInfo(opInfoMesh);
       	opInfoVariables.removeAlgorithmicAuxFieldDerivativeDependent();
        iterOverCells3.appendChild(createNonDerivativeAuxiliaryEquation(discProblem, problem, opInfoVariables, pi));
        if (hasNonConservativeTermsMesh) {
        	iterOverCells3.appendChild(createAlgebraicTerms(doc, opInfoMesh.getAuxiliaryDerivativeLevels().get(0), pi, timeOutputVariable, usedTermsMesh, simplificationsMesh));
        }
        insertCornerCalculation(iterOverCells3);
        
        //Particles
        for (Entry<String, OperatorsInfo> entry : opInfoParticleSpecies.entrySet()) {
        	String speciesName = entry.getKey();
        	OperatorsInfo opInfoParticles = entry.getValue();
            Element iterOverParticles = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "iterateOverParticles");
            iterOverParticles.setAttribute("speciesNameAtt", speciesName);
            ProcessInfo newPi = new ProcessInfo(pi);
            newPi.setDiscretizationType(DiscretizationType.particles);
            newPi.setCurrentSpecies(speciesName);
        	opInfoAuxFields = new OperatorsInfo(opInfoParticles);
        	opInfoAuxFields.removeAlgorithmicAuxFieldDerivativeDependent();
        	opInfoAuxFields.onlyDependent(opInfoAuxFields.getAuxiliaryFieldAlgorithmVariables(), true, opInfoAuxFields.getNonDerivativeDependentAuxiliaryFieldCommonConditional());
        	iterOverParticles.appendChild(createAuxiliaryVariableEquations(discProblem, problem, opInfoAuxFields, opInfoParticles.getAuxiliaryDerivativeLevels(), opInfoParticles.getMultiplicationLevels(), opInfoAuxFields.getDerivativeLevels(), opInfoAuxFields.getAuxiliaryMultiplicationLevels(), newPi, (Element) timeOutputVariable.cloneNode(true), 0, false));
            //Non derivative auxiliary equations
           	opInfoVariables = new OperatorsInfo(opInfoParticles);
           	opInfoVariables.removeAlgorithmicAuxFieldDerivativeDependent();
            iterOverParticles.appendChild(createNonDerivativeAuxiliaryEquation(discProblem, problem, opInfoVariables, newPi));
            if (hasNonConservativeTermsParticles) {
            	iterOverParticles.appendChild(createAlgebraicTerms(doc, opInfoParticles.getAuxiliaryDerivativeLevels().get(0), newPi, timeOutputVariable, usedTermsMesh, simplificationsMesh));
            }
            if (iterOverParticles.hasChildNodes()) {
            	iterOverCells3.appendChild(iterOverParticles);
            }
        }
		unifyParticleIteration(pi, iterOverCells3);
        result.appendChild(iterOverCells3);
        
        //Second block field auxiliaries (iterative)
        //Remove derivative level variables non derivative dependent
        OperatorsInfo opInfoAuxMesh = new OperatorsInfo(opInfoMesh);
        opInfoAuxMesh.removeAuxFieldDerivativeIndependent();
    	HashSet<String> variablesToCheck = opInfoAuxMesh.getAuxiliaryFieldVariables();
    	variablesToCheck.addAll(opInfoAuxMesh.getAuxiliaryFieldAlgorithmVariables());
    	opInfoAuxMesh.onlyDependent(variablesToCheck, true, opInfoAuxMesh.getDerivativeDependentAuxiliaryFieldCommonConditional());
    	maxMesh = opInfoAuxMesh.getAuxiliaryDerivativeLevels().size();
    	discretizationBlocks = maxMesh;
    	maxParticles = new HashMap<String, Integer>();
    	HashMap<String, OperatorsInfo> opInfoAuxParticles = new HashMap<String, OperatorsInfo>();
    	for (Entry<String, OperatorsInfo> entry : opInfoParticleSpecies.entrySet()) {
    		String speciesName = entry.getKey();
    		 //Remove derivative level variables non derivative dependent
            OperatorsInfo opInfoParticles = new OperatorsInfo(entry.getValue());
            opInfoParticles.removeAuxFieldDerivativeIndependent();
        	variablesToCheck = opInfoParticles.getAuxiliaryFieldVariables();
        	variablesToCheck.addAll(opInfoParticles.getAuxiliaryFieldAlgorithmVariables());
        	opInfoParticles.onlyDependent(variablesToCheck, true, opInfoParticles.getDerivativeDependentAuxiliaryFieldCommonConditional());
        	opInfoAuxParticles.put(speciesName, opInfoParticles);
        	int maxParticleSpecies = opInfoParticles.getAuxiliaryDerivativeLevels().size();
        	maxParticles.put(speciesName, maxParticleSpecies);
        	discretizationBlocks = Math.max(discretizationBlocks, maxParticleSpecies);
    	}
    	startMesh = discretizationBlocks - maxMesh;
    	startParticlesSpecies = new HashMap<String, Integer>();
    	for (String speciesName : opInfoParticleSpecies.keySet()) {
    		startParticlesSpecies.put(speciesName, discretizationBlocks - maxParticles.get(speciesName));
    	}
        for (int i = 0; i < discretizationBlocks; i++) {     
        	boolean lastLoop = i + 1 == discretizationBlocks;
            usedTermsMesh = new Hashtable<NodeWrap, String>();
            usedTermsParticles = new Hashtable<NodeWrap, String>();
            Element iterOverCells4 = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "iterateOverCells");
            //rule info
            RuleInfo ri = new RuleInfo();
            ri.setProcessInfo(pi);
            ri.setIndividualField(false);
            ri.setIndividualCoord(false);
            ri.setFunction(false);
            ri.setAuxiliaryCDAdded(false);
            ri.setAuxiliaryFluxesAdded(false);
            ri.setUseBaseCoordinates(false);
            ri.setExtrapolation(pi.getExtrapolationMethod() != null && pi.getIncomFields() != null);
            //Mesh
    		if (i >= startMesh && (opInfoAuxMesh.getAuxEquations().getEquations().size() > 0)) {
    			pi.setDiscretizationType(DiscretizationType.mesh);
                ri.setProcessInfo(pi);    
                
	            //Nth order derivative calculation
	            if (hasNonConservativeTermsMesh) {
	    			ArrayList<Element> nonConsInputVariables = getNonConsFunctionInputVariables((Element) pi.getCurrentTimeStep(), DiscretizationType.mesh);
	    			nonConsInputVariables = replaceVariables(nonConsInputVariables, nonConsInputVariable, timeOutputVariable); 
		        	iterOverCells4.appendChild(createDerivativeCalculation(doc, opInfoAuxMesh, opInfoAuxMesh.getAuxiliaryDerivativeLevels().get(i - startMesh), discProblem, problem, pi, nonConsInputVariables, i - startMesh, 
		        			lastLoop, simplificationsMesh, lastLoop, null));
	            }
	            //No last block
	            if (!lastLoop) {
	                //Reset simplification terms for the next level
	                simplificationsMesh = new AlgebraicSimplifications();
	                //Multiplication of derivatives and algebraic terms
	                iterOverCells4.appendChild(createDerivativeMultiplication(doc, opInfoAuxMesh, opInfoAuxMesh.getAuxiliaryMultiplicationLevels(), opInfoAuxMesh.getAuxiliaryDerivativeLevels(), pi, timeOutputVariable, OperatorInfoType.auxiliaryField, i - startMesh, lastLoop));
	                //Nth order order algebraic terms
	                if (hasNonConservativeTermsMesh) {
	                	iterOverCells4.appendChild(createAlgebraicTerms(doc, opInfoAuxMesh.getAuxiliaryDerivativeLevels().get(i + 1 - startMesh), pi, timeOutputVariable, usedTermsMesh, simplificationsMesh));
	                }
	                insertCornerCalculation(iterOverCells4);
	            }
	            //Last block
	            else {
	                //Auxiliary variables (Only the ones containing sources)
	                if (hasNonConservativeTermsMesh) {
	                    iterOverCells4.appendChild(createAuxiliaryVariableEquations(discProblem, problem, opInfoAuxMesh, opInfoMesh.getAuxiliaryDerivativeLevels(), opInfoMesh.getAuxiliaryMultiplicationLevels(), opInfoAuxMesh.getAuxiliaryDerivativeLevels(), opInfoAuxMesh.getAuxiliaryMultiplicationLevels(), pi, (Element) timeOutputVariable.cloneNode(true), i - startMesh, false));
	                }
	                //Sources (they will only be in the last level)
	            	if (hasNonConservativeTermsMesh) {
	            		iterOverCells4.appendChild(createAuxFieldSourcesMesh(doc, opInfoAuxMesh.getAuxiliaryDerivativeLevels().get(i - startMesh), pi, OperatorInfoType.auxiliaryField));
	            	}
	                //Multiplication of derivatives, algebraic terms and auxiliary definitions
	                iterOverCells4.appendChild(createDerivativeMultiplication(doc, opInfoAuxMesh, opInfoAuxMesh.getAuxiliaryMultiplicationLevels(), opInfoAuxMesh.getAuxiliaryDerivativeLevels(), pi, timeOutputVariable, OperatorInfoType.auxiliaryField, i - startMesh, lastLoop));
	                //Equation term summation
	                iterOverCells4.appendChild(createDerivativeAuxiliaryEquation(doc, opInfoAuxMesh, pi, timeOutputVariable));
	                //Non derivative auxiliary equations dependent from derivative ones.
	                iterOverCells4.appendChild(createNonDerivativeAuxiliaryEquation(discProblem, problem, opInfoAuxMesh, pi));
	            }
    		}
    		//Particles
    		for (Entry<String, OperatorsInfo> entry : opInfoParticleSpecies.entrySet()) {
            	String speciesName = entry.getKey();
            	OperatorsInfo opInfoParticles = opInfoAuxParticles.get(speciesName);
            	int startParticles = startParticlesSpecies.get(speciesName);
            	ProcessInfo newPi = new ProcessInfo(pi);
                newPi.setDiscretizationType(DiscretizationType.particles);
                newPi.setCurrentSpecies(speciesName);
                //Check if any auxiliary field to calculate
	    		if (i >= startParticles && opInfoParticles.getAuxiliaryFieldVariables().size() > 0) {
	    			ArrayList<ParticleNormalization> normalizationInfo = generateNormalizationInfo(doc, step, newPi, opInfoParticles, i - startParticles, lastLoop, false);
	                Element iterOverParticles = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "iterateOverParticles");
	                iterOverParticles.setAttribute("speciesNameAtt", speciesName);
	                Element iterateOverInteractions = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "iterateOverInteractions");
	                iterateOverInteractions.setAttribute("compactSupportRatioAtt", String.valueOf(newPi.getCompactSupportRatio()));
	                iterateOverInteractions.setAttribute("speciesNameAtt", speciesName);
	                ri.setProcessInfo(newPi);
	                if (opInfoParticles.getAuxiliaryDerivativeLevels().get(i - startParticles).getDerivatives().size() > 0) {
		    			ArrayList<Element> nonConsInputVariables = getNonConsFunctionInputVariables((Element) newPi.getCurrentTimeStep(), DiscretizationType.particles);
		    			nonConsInputVariables = replaceVariables(nonConsInputVariables, nonConsInputVariable, timeOutputVariable);
	                    //Initialize normalizations
		            	iterOverParticles.appendChild(initializeNormalizationVars(doc, step, newPi, spatialOutputVariable, opInfoParticles, i - startParticles, OperatorInfoType.auxiliaryField, lastLoop, false));
		            	//Calculate normalization
		            	iterateOverInteractions.appendChild(calculateNormalizationFunctions(doc, discProblem, problem, step, newPi, opInfoParticles, i - startParticles, normalizationInfo, lastLoop));
			            //Nth order derivative calculation
		            	iterateOverInteractions.appendChild(createDerivativeCalculation(doc, opInfoParticles, opInfoParticles.getAuxiliaryDerivativeLevels().get(i), discProblem, problem, newPi, nonConsInputVariables, i - startParticles, 
			        			lastLoop, simplificationsParticles, lastLoop, null));
		            }
		            //No last block
		            if (!lastLoop) {
		             	iterOverParticles.appendChild(initializeDerivativeVariables(doc, opInfoParticles, opInfoParticles.getAuxiliaryDerivativeLevels().get(i - startParticles), newPi, i - startParticles));
		                //Reset simplification terms for the next level
		                simplificationsParticles = new AlgebraicSimplifications();
	
		                //Nth order order algebraic terms
		                if (opInfoParticles.getAuxiliaryDerivativeLevels().get(i - startParticles).getDerivatives().size() > 0) {
		                	iterateOverInteractions.appendChild(createAlgebraicTerms(doc, opInfoParticles.getAuxiliaryDerivativeLevels().get(i + 1 - startParticles), newPi, timeOutputVariable, usedTermsParticles, simplificationsParticles));
		                }
		                if (iterateOverInteractions.hasChildNodes()) {
		                	//Add particle position
		                    Element particlePosition = createPositionReference(doc, getPreviousPositionVariable(doc, step), newPi);
		                	iterateOverInteractions.insertBefore(particlePosition, iterateOverInteractions.getFirstChild());
		                	iterOverParticles.appendChild(iterateOverInteractions);
		                }
		                //Normalization
		                iterOverParticles.appendChild(securizeNormalizations(doc, step, newPi, opInfoParticles, i - startParticles, lastLoop, false));
		                iterOverParticles.appendChild(createNormalizeRHSFunctions(doc, step, newPi, spatialOutputVariable, opInfoParticles, i - startParticles, normalizationInfo, opInfoParticles.getAuxiliaryNormalizationLevels(), false, false, true));
		                //Multiplication of derivatives and algebraic terms
		                iterOverParticles.appendChild(createDerivativeMultiplication(doc, opInfoParticles, opInfoParticles.getAuxiliaryMultiplicationLevels(), opInfoParticles.getAuxiliaryDerivativeLevels(), newPi, timeOutputVariable, OperatorInfoType.auxiliaryField, i - startParticles, lastLoop));
		            }
		            //Last block
		            else {
		                if (iterateOverInteractions.hasChildNodes()) {
		                	//Add particle position
		                    Element particlePosition = createPositionReference(doc, getPreviousPositionVariable(doc, step), newPi);
		                	iterateOverInteractions.insertBefore(particlePosition, iterateOverInteractions.getFirstChild());
		                	iterOverParticles.appendChild(iterateOverInteractions);
		                }
		            	
		                //Auxiliary variables (Only the ones containing sources)
		                if (opInfoParticles.getAuxiliaryDerivativeLevels().get(i - startParticles).getDerivatives().size() > 0) {
		                	if (opInfoParticles.hasDerivatives(OperatorInfoType.auxiliaryVariable)) {
		                        throw new AGDMException(AGDMException.AGDM005, "Auxiliary variables used in Auxiliary fields cannot be defined using derivatives.");
		                	}
		                	iterOverParticles.appendChild(createAuxiliaryVariableEquations(discProblem, problem, opInfoParticles, opInfoParticles.getDerivativeLevels(), opInfoParticles.getMultiplicationLevels(), opInfoParticles.getDerivativeLevels(), opInfoParticles.getMultiplicationLevels(), newPi, (Element) timeOutputVariable.cloneNode(true), i - startParticles, false));
		                }
		                //Sources (they will only be in the last level)
		            	if (opInfoParticles.getAuxiliaryDerivativeLevels().get(i - startParticles).getDerivatives().size() > 0) {
		                	iterOverParticles.appendChild(createNonConsSourcesParticles(doc, opInfoParticles.getAuxiliaryDerivativeLevels().get(i - startParticles), newPi, previousVariable, timeOutputVariable, OperatorInfoType.auxiliaryField));		                	
		            	}

		            	//Normalize variables
		                if (opInfoParticles.getAuxiliaryDerivativeLevels().get(i - startParticles).getDerivatives().size() > 0) {
			                iterOverParticles.appendChild(securizeNormalizations(doc, step, newPi, opInfoParticles, i - startParticles, lastLoop, false));
		                	iterOverParticles.appendChild(createNormalizeRHSFunctions(doc, step, newPi, spatialOutputVariable, opInfoParticles, i - startParticles, normalizationInfo, opInfoParticles.getAuxiliaryNormalizationLevels(), false, false, true));
		                }
		                //Multiplication of derivatives, algebraic terms and auxiliary definitions
		                iterOverParticles.appendChild(createDerivativeMultiplication(doc, opInfoParticles, opInfoParticles.getAuxiliaryMultiplicationLevels(), opInfoMesh.getAuxiliaryDerivativeLevels(), newPi, timeOutputVariable, OperatorInfoType.auxiliaryField, i - startParticles, lastLoop));
		                //Equation term summation
		                iterOverParticles.appendChild(createDerivativeAuxiliaryEquation(doc, opInfoParticles, newPi, timeOutputVariable));
		                //Non derivative auxiliary equations dependent from derivative ones.
		               	iterOverParticles.appendChild(createNonDerivativeAuxiliaryEquation(discProblem, problem, opInfoParticles, newPi));
		            }
		            if (iterOverParticles.hasChildNodes()) {
		    			iterOverCells4.appendChild(iterOverParticles);
		    		
		    		}
	    		}
    		}
            // Stencil attribute
        	int stencil = 0;
    		for (Entry<String, OperatorsInfo> entry : opInfoParticleSpecies.entrySet()) {
            	String speciesName = entry.getKey();
            	OperatorsInfo opInfoParticles = opInfoAuxParticles.get(speciesName);
            	int startParticles = startParticlesSpecies.get(speciesName);
            	if (opInfoParticles.getAuxiliaryDerivativeLevels().get(i - startParticles).getDerivatives().size() > 0) {
            		stencil = 1;
            	}
    		}

        	if (hasNonConservativeTermsMesh) {
        		stencil = getLevelStencil(opInfoMesh.getAuxiliaryDerivativeLevels().get(i - startMesh).getDerivatives()); 
        	}
            iterOverCells4.setAttribute("stencilAtt", String.valueOf(stencil));
            //Append attribute
            if ((i + 2 == opInfoMesh.getAuxiliaryDerivativeLevels().size()) || (i >= startMesh && i + 1 < opInfoMesh.getAuxiliaryDerivativeLevels().size() && opInfoMesh.getAuxiliaryDerivativeLevels().get(i + 1 - startMesh) != null)) {
            	iterOverCells4.setAttribute("appendAtt", String.valueOf(accumulatedStencil.get(i + 1)));    
            }
    		if (lastLoop) {
				iterOverCells4.setAttribute("excludePhysicalBoundariesAtt", "true");
			}
            insertCornerCalculation(iterOverCells4);
            result.appendChild(iterOverCells4);
        }
        
        //Poststep
        //Mesh
        Element poststep = getPoststep(step, DiscretizationType.mesh);
        DocumentFragment meshPoststep = null;
        if (poststep != null) {
        	AGDMUtils.addParameterSuffixNames(step.getOwnerDocument(), poststep, regionName);
        	pi.setDiscretizationType(DiscretizationType.mesh);
        	meshPoststep = ExecutionFlow.processSimml(poststep, discProblem, problem, pi, false);
        }
        //Particles
        poststep = getPoststep(step, DiscretizationType.particles);
        DocumentFragment particlePoststep = null;
        if (poststep != null) {
        	AGDMUtils.addParameterSuffixNames(step.getOwnerDocument(), poststep, regionName);
        	pi.setDiscretizationType(DiscretizationType.particles);
        	particlePoststep = ExecutionFlow.processSimml(poststep, discProblem, problem, pi, false);
        }
        //Unify poststep
        if (meshPoststep != null && particlePoststep != null) {
        	result.appendChild(AGDMUtils.unifyCodeFragments(meshPoststep, particlePoststep));
        }
        else {
        	if (meshPoststep != null) {
        		result.appendChild(meshPoststep);
        	}
        	else {
        		if (particlePoststep != null) {
        			result.appendChild(particlePoststep);
        		}
        	}
        }
        
        //Third block
        if (hasSpatialDiscretization) {
	        //Boundaries and extrapolations
	        Element boundary = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "boundary");
	        Element variableBoundary = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "variable");
	        Element variableMath = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "math");
	        variableMath.appendChild(doc.importNode(timeOutputVariable.cloneNode(true), true));
	        variableBoundary.appendChild(variableMath);
	        Element previousVariableBoundary = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "previousVariable");
	        Element preVariableMath = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "math");
	        preVariableMath.appendChild(doc.importNode(nonConsInputVariable.cloneNode(true), true));
	        previousVariableBoundary.appendChild(preVariableMath);
	        Element stencil = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "stencil");
	        stencil.setTextContent(String.valueOf(maxStencil));
	        boundary.appendChild(variableBoundary);
	        boundary.appendChild(previousVariableBoundary);
	        boundary.appendChild(stencil);
	        //Add the processed boundary
	        result.appendChild(doc.importNode(Boundary.processBoundary(discProblem, problem, boundary, pi, isLastStep, false, true, !(hasDerivativeAuxiliaresMesh || hasDerivativeAuxiliaresParticles)), true));
        }
        loopUnification(doc, pi, iterOverCells, iterOverCells2, interpolation, step, opInfoStencil, dissipationStencil);
        checkSynchronizationVariables(doc, step, opInfoMesh, pi, result);
        replaceParticlePositions(pi, step, result);
        conditionalUnification(result);
        
        return result;
    }
 
    /**
     * Creates the post refinement algorithm. Only algorithmic auxiliary field equations.
     * @param discProblem       The problem being discretized
     * @param problem           The continuous problem
     * @param opInfoMesh        The operator discretization info for mesh
     * @param opInfoParticles   The operator discretization info for particles
     * @param pi                The process information
     * @return					The algorithm
     * @throws AGDMException	AGDM00X - External error
     */
    public DocumentFragment createPostRefinementAlgorithm(Document schema, Document discProblem, Node problem, OperatorsInfo opInfoMesh, HashMap<String, OperatorsInfo> opInfoParticles, 
    		ProcessInfo pi) throws AGDMException {
    	Document doc = problem.getOwnerDocument();
        DocumentFragment result = problem.getOwnerDocument().createDocumentFragment();
        
        pi.setDiscretizationType(DiscretizationType.mesh);
        
        timeOutputVariable = AGDMUtils.createTimeSlice(doc);
        
    	//Auxiliary field blocks
        //Calculate accumulated stencil
        int maxParticleDerLevels = 0;
        for (OperatorsInfo op: opInfoParticles.values()) {
        	maxParticleDerLevels = Math.max(maxParticleDerLevels, op.getAuxiliaryDerivativeLevels().size());
        }
        calculateAccumulatedStencilAndCrossedDerivatives(opInfoMesh.getAuxiliaryDerivativeLevels(), opInfoMesh.getAuxiliaryMultiplicationLevels(), maxParticleDerLevels, null);
        hasNonConservativeTermsMesh = opInfoMesh.getAuxiliaryDerivativeLevels().size() > 0;
        //First order algebraic terms
        Element iterOverCells3 = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "iterateOverCells");
        iterOverCells3.setAttribute("stencilAtt", String.valueOf(0));
        if (accumulatedStencil.size() > 0) {
        	iterOverCells3.setAttribute("appendAtt", String.valueOf(accumulatedStencil.get(0)));
        }
        
        //Particle-mesh interaction interpolations
        Element firstStep = (Element) schema.getDocumentElement().getElementsByTagNameNS(AGDMUtils.mmsUri, "step").item(0);
		NodeList instructions = createParticleMeshInteraction(firstStep, discProblem, problem, opInfoMesh, opInfoParticles, pi, timeOutputVariable, (Element) convertToPosition(timeOutputVariable), OperatorInfoType.auxiliaryField).getChildNodes();
		for (int i = 0; i < instructions.getLength(); i++) {
			iterOverCells3.appendChild(doc.importNode(instructions.item(i).cloneNode(true), true));
		}
        
        //Auxiliary variables (Only the ones containing sources)
    	OperatorsInfo opInfoAuxFields = new OperatorsInfo(opInfoMesh);
    	opInfoAuxFields.removeAlgorithmicAuxFieldDerivativeDependent();
    	opInfoAuxFields.onlyDependent(opInfoAuxFields.getAuxiliaryFieldAlgorithmVariables(), true, opInfoAuxFields.getNonDerivativeDependentAuxiliaryFieldCommonConditional());
    	if (opInfoAuxFields.hasDerivatives(OperatorInfoType.auxiliaryVariable)) {
            throw new AGDMException(AGDMException.AGDM005, "Auxiliary variables used in Auxiliary fields cannot be defined using derivatives.");
    	}
        iterOverCells3.appendChild(createAuxiliaryVariableEquations(discProblem, problem, opInfoAuxFields, opInfoMesh.getAuxiliaryDerivativeLevels(), opInfoMesh.getAuxiliaryMultiplicationLevels(), opInfoAuxFields.getAuxiliaryDerivativeLevels(), opInfoAuxFields.getAuxiliaryMultiplicationLevels(), pi, (Element) timeOutputVariable.cloneNode(true), 0, false));

        //Non derivative auxiliary equations
        opInfoAuxFields = new OperatorsInfo(opInfoMesh);
        opInfoAuxFields.removeAlgorithmicAuxFieldDerivativeDependent();
        iterOverCells3.appendChild(createNonDerivativeAuxiliaryEquation(discProblem, problem, opInfoAuxFields, pi));
        insertCornerCalculation(iterOverCells3);
        if (iterOverCells3.hasChildNodes()) {
        	result.appendChild(iterOverCells3);
        }
        
        return result;
    }
    
    /**
     * Creates the post refinement algorithm. Only algorithmic auxiliary field equations.
     * @param schema				The discretization schema
     * @param discProblem       	The problem being discretized
     * @param problem           	The continuous problem
     * @param opInfoMesh        	The operator discretization info for mesh
     * @param opInfoParticleSpecies The operator discretization info for particles
     * @param pi                	The process information
     * @return						The algorithm
     * @throws AGDMException		AGDM00X - External error
     */
    public DocumentFragment createPostInitAuxiliaries(Document schema, Document discProblem, Node problem, OperatorsInfo opInfoMesh, HashMap<String, OperatorsInfo> opInfoParticleSpecies, 
    		ProcessInfo pi) throws AGDMException {
    	Document doc = problem.getOwnerDocument();
        DocumentFragment result = problem.getOwnerDocument().createDocumentFragment();
                
        timeOutputVariable = AGDMUtils.createTimeSlice(doc);
        AlgebraicSimplifications simplificationsMesh = new AlgebraicSimplifications();
        AlgebraicSimplifications simplificationsParticles = new AlgebraicSimplifications();
        Hashtable<NodeWrap, String> usedTermsMesh = new Hashtable<NodeWrap, String>();
        Hashtable<NodeWrap, String> usedTermsParticles = new Hashtable<NodeWrap, String>();
        
        //Get first step and replace previous variable occurrences with final time variable
        Element step = (Element) schema.getDocumentElement().getElementsByTagNameNS(AGDMUtils.mmsUri, "step").item(0).cloneNode(true);
        //"_p" variable
        Node previousTime = AGDMUtils.createPreviousTimeSlice(doc).getFirstChild();
        AGDMUtils.nodeReplace(step, step.getOwnerDocument().importNode(previousTime, true), step.getOwnerDocument().importNode(timeOutputVariable.getFirstChild(), true));
        
        
        int maxParticleDerLevels = 0;
        for (OperatorsInfo op: opInfoParticleSpecies.values()) {
        	maxParticleDerLevels = Math.max(maxParticleDerLevels, op.getAuxiliaryDerivativeLevels().size());
        }
        calculateAccumulatedStencilAndCrossedDerivatives(opInfoMesh.getAuxiliaryDerivativeLevels(), opInfoMesh.getAuxiliaryMultiplicationLevels(), maxParticleDerLevels, null);
        hasNonConservativeTermsMesh = opInfoMesh.getAuxiliaryDerivativeLevels().size() > 0;
        hasNonConservativeTermsParticles = false;
        for (OperatorsInfo op: opInfoParticleSpecies.values()) {
        	hasNonConservativeTermsParticles = hasNonConservativeTermsParticles || op.getAuxiliaryDerivativeLevels().size() > 0;	
        }
        Element iterOverCells3 = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "iterateOverCells");
        iterOverCells3.setAttribute("stencilAtt", String.valueOf(0));
        if (accumulatedStencil.size() > 0) {
        	iterOverCells3.setAttribute("appendAtt", String.valueOf(accumulatedStencil.get(0)));
        }
        //If there are no boundary loop previous to that one, calculate auxiliaries only in internal domain (including internal boundaries)
        if (!hasDerivativeAuxiliaresMesh && !hasDerivativeAuxiliaresParticles) {
        	iterOverCells3.setAttribute("excludePhysicalBoundariesAtt", "true");
        }
        //Particle-mesh interaction interpolations
		NodeList instructions = createParticleMeshInteraction(step, discProblem, problem, opInfoMesh, opInfoParticleSpecies, pi, timeOutputVariable, (Element) convertToPosition(timeOutputVariable), OperatorInfoType.auxiliaryField).getChildNodes();
		for (int i = 0; i < instructions.getLength(); i++) {
			iterOverCells3.appendChild(doc.importNode(instructions.item(i).cloneNode(true), true));
		}
        //Auxiliary variables (Only the ones containing sources)
        usedTermsMesh = new Hashtable<NodeWrap, String>();
        simplificationsMesh = new AlgebraicSimplifications();
        //Mesh
    	pi.setDiscretizationType(DiscretizationType.mesh);
    	OperatorsInfo opInfoAuxFields = new OperatorsInfo(opInfoMesh);
    	opInfoAuxFields.removeAlgorithmicAuxFieldDerivativeDependent();
    	opInfoAuxFields.onlyDependent(opInfoAuxFields.getAuxiliaryFieldAlgorithmVariables(), true, opInfoAuxFields.getNonDerivativeDependentAuxiliaryFieldCommonConditional());
    	if (opInfoAuxFields.hasDerivatives(OperatorInfoType.auxiliaryVariable)) {
            throw new AGDMException(AGDMException.AGDM005, "Auxiliary variables used in Auxiliary fields cannot be defined using derivatives.");
    	}
        iterOverCells3.appendChild(createAuxiliaryVariableEquations(discProblem, problem, opInfoAuxFields, opInfoMesh.getAuxiliaryDerivativeLevels(), opInfoMesh.getAuxiliaryMultiplicationLevels(), opInfoAuxFields.getAuxiliaryDerivativeLevels(), opInfoAuxFields.getAuxiliaryMultiplicationLevels(), pi, (Element) timeOutputVariable.cloneNode(true), 0, false));
        //Non derivative auxiliary equations
       	OperatorsInfo opInfoVariables = new OperatorsInfo(opInfoMesh);
       	opInfoVariables.removeAlgorithmicAuxFieldDerivativeDependent();
        iterOverCells3.appendChild(createNonDerivativeAuxiliaryEquation(discProblem, problem, opInfoVariables, pi));
        if (hasNonConservativeTermsMesh) {
        	iterOverCells3.appendChild(createAlgebraicTerms(doc, opInfoMesh.getAuxiliaryDerivativeLevels().get(0), pi, timeOutputVariable, usedTermsMesh, simplificationsMesh));
        }
        insertCornerCalculation(iterOverCells3);
        //Particles
        for (Entry<String, OperatorsInfo> entry : opInfoParticleSpecies.entrySet()) {
        	String speciesName = entry.getKey();
        	OperatorsInfo opInfoParticles = entry.getValue();
            Element iterOverParticles = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "iterateOverParticles");
            iterOverParticles.setAttribute("speciesNameAtt", speciesName);
            ProcessInfo newPi = new ProcessInfo(pi);
            newPi.setDiscretizationType(DiscretizationType.particles);
            newPi.setCurrentSpecies(speciesName);
        	opInfoAuxFields = new OperatorsInfo(opInfoParticles);
        	opInfoAuxFields.removeAlgorithmicAuxFieldDerivativeDependent();
        	opInfoAuxFields.onlyDependent(opInfoAuxFields.getAuxiliaryFieldAlgorithmVariables(), true, opInfoAuxFields.getNonDerivativeDependentAuxiliaryFieldCommonConditional());
        	iterOverParticles.appendChild(createAuxiliaryVariableEquations(discProblem, problem, opInfoAuxFields, opInfoParticles.getAuxiliaryDerivativeLevels(), opInfoParticles.getMultiplicationLevels(), opInfoAuxFields.getDerivativeLevels(), opInfoAuxFields.getAuxiliaryMultiplicationLevels(), newPi, (Element) timeOutputVariable.cloneNode(true), 0, false));
            //Non derivative auxiliary equations
           	opInfoVariables = new OperatorsInfo(opInfoParticles);
           	opInfoVariables.removeAlgorithmicAuxFieldDerivativeDependent();
            iterOverParticles.appendChild(createNonDerivativeAuxiliaryEquation(discProblem, problem, opInfoVariables, newPi));
            if (hasNonConservativeTermsParticles) {
            	iterOverParticles.appendChild(createAlgebraicTerms(doc, opInfoParticles.getAuxiliaryDerivativeLevels().get(0), newPi, timeOutputVariable, usedTermsMesh, simplificationsMesh));
            }
            if (iterOverParticles.hasChildNodes()) {
            	iterOverCells3.appendChild(iterOverParticles);
            }
        }
		unifyParticleIteration(pi, iterOverCells3);
		result.appendChild(iterOverCells3);
		
        //Second block field auxiliaries (iterative)
        //Remove derivative level variables non derivative dependent
        OperatorsInfo opInfoAuxMesh = new OperatorsInfo(opInfoMesh);
        opInfoAuxMesh.removeAuxFieldDerivativeIndependent();
    	HashSet<String> variablesToCheck = opInfoAuxMesh.getAuxiliaryFieldVariables();
    	variablesToCheck.addAll(opInfoAuxMesh.getAuxiliaryFieldAlgorithmVariables());
    	opInfoAuxMesh.onlyDependent(variablesToCheck, true, opInfoAuxMesh.getDerivativeDependentAuxiliaryFieldCommonConditional());
    	int maxMesh = opInfoAuxMesh.getAuxiliaryDerivativeLevels().size();
    	int discretizationBlocks = maxMesh;
    	HashMap<String, Integer> maxParticles = new HashMap<String, Integer>();
    	HashMap<String, OperatorsInfo> opInfoAuxParticles = new HashMap<String, OperatorsInfo>();
    	for (Entry<String, OperatorsInfo> entry : opInfoParticleSpecies.entrySet()) {
    		String speciesName = entry.getKey();
    		 //Remove derivative level variables non derivative dependent
            OperatorsInfo opInfoParticles = new OperatorsInfo(entry.getValue());
            opInfoParticles.removeAuxFieldDerivativeIndependent();
        	variablesToCheck = opInfoParticles.getAuxiliaryFieldVariables();
        	variablesToCheck.addAll(opInfoParticles.getAuxiliaryFieldAlgorithmVariables());
        	opInfoParticles.onlyDependent(variablesToCheck, true, opInfoParticles.getDerivativeDependentAuxiliaryFieldCommonConditional());
        	opInfoAuxParticles.put(speciesName, opInfoParticles);
        	int maxParticleSpecies = opInfoParticles.getAuxiliaryDerivativeLevels().size();
        	maxParticles.put(speciesName, maxParticleSpecies);
        	discretizationBlocks = Math.max(discretizationBlocks, maxParticleSpecies);
    	}
    	int startMesh = discretizationBlocks - maxMesh;
    	HashMap<String, Integer> startParticlesSpecies = new HashMap<String, Integer>();
    	for (String speciesName : opInfoParticleSpecies.keySet()) {
    		startParticlesSpecies.put(speciesName, discretizationBlocks - maxParticles.get(speciesName));
    	}
        for (int i = 0; i < discretizationBlocks; i++) {      
        	boolean lastLoop = i + 1 == discretizationBlocks;
        	usedTermsMesh = new Hashtable<NodeWrap, String>();
        	usedTermsParticles = new Hashtable<NodeWrap, String>();
            Element iterOverCells4 = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "iterateOverCells");
            //rule info
            RuleInfo ri = new RuleInfo();
            ri.setProcessInfo(pi);
            ri.setIndividualField(false);
            ri.setIndividualCoord(false);
            ri.setFunction(false);
            ri.setAuxiliaryCDAdded(false);
            ri.setAuxiliaryFluxesAdded(false);
            ri.setUseBaseCoordinates(false);
            ri.setExtrapolation(pi.getExtrapolationMethod() != null && pi.getIncomFields() != null);
            //Mesh
    		if (i >= startMesh && opInfoAuxMesh.getAuxEquations().getEquations().size() > 0) {
    			pi.setDiscretizationType(DiscretizationType.mesh);
                ri.setProcessInfo(pi);    
	            //Nth order derivative calculation
	            if (hasNonConservativeTermsMesh) {
	    			ArrayList<Element> nonConsInputVariables = getNonConsFunctionInputVariables(step, DiscretizationType.mesh);
		        	iterOverCells4.appendChild(createDerivativeCalculation(doc, opInfoAuxMesh, opInfoAuxMesh.getAuxiliaryDerivativeLevels().get(i - startMesh), discProblem, problem, pi, nonConsInputVariables, i - startMesh, 
		        			lastLoop, simplificationsMesh, lastLoop, null));
	            }
	            //No last block
	            if (!lastLoop) {
	                //Reset simplification terms for the next level
	            	simplificationsMesh = new AlgebraicSimplifications();
	                //Multiplication of derivatives and algebraic terms
	                iterOverCells4.appendChild(createDerivativeMultiplication(doc, opInfoAuxMesh, opInfoAuxMesh.getAuxiliaryMultiplicationLevels(), opInfoAuxMesh.getAuxiliaryDerivativeLevels(), pi, timeOutputVariable, OperatorInfoType.auxiliaryField, i - startMesh, lastLoop));
	                //Nth order order algebraic terms
	                if (hasNonConservativeTermsMesh) {
	                	iterOverCells4.appendChild(createAlgebraicTerms(doc, opInfoAuxMesh.getAuxiliaryDerivativeLevels().get(i + 1 - startMesh), pi, timeOutputVariable, usedTermsMesh, simplificationsMesh));
	                }
	                insertCornerCalculation(iterOverCells4);
	            }
	            //Last block
	            else {
	                //Auxiliary variables (Only the ones containing sources)
	                if (hasNonConservativeTermsMesh) {
	                    iterOverCells4.appendChild(createAuxiliaryVariableEquations(discProblem, problem, opInfoAuxMesh, opInfoMesh.getAuxiliaryDerivativeLevels(), opInfoMesh.getAuxiliaryMultiplicationLevels(), opInfoAuxMesh.getAuxiliaryDerivativeLevels(), opInfoAuxMesh.getAuxiliaryMultiplicationLevels(), pi, (Element) timeOutputVariable.cloneNode(true), i - startMesh, false));
	                }
	                //Sources (they will only be in the last level)
	            	if (hasNonConservativeTermsMesh) {
	            		iterOverCells4.appendChild(createAuxFieldSourcesMesh(doc, opInfoAuxMesh.getAuxiliaryDerivativeLevels().get(i - startMesh), pi, OperatorInfoType.auxiliaryField));
	            	}
	                //Multiplication of derivatives, algebraic terms and auxiliary definitions
	                iterOverCells4.appendChild(createDerivativeMultiplication(doc, opInfoAuxMesh, opInfoAuxMesh.getAuxiliaryMultiplicationLevels(), opInfoAuxMesh.getAuxiliaryDerivativeLevels(), pi, timeOutputVariable, OperatorInfoType.auxiliaryField, i - startMesh, lastLoop));
	                //Equation term summation
	                iterOverCells4.appendChild(createDerivativeAuxiliaryEquation(doc, opInfoAuxMesh, pi, timeOutputVariable));
	                //Non derivative auxiliary equations dependent from derivative ones.
	                iterOverCells4.appendChild(createNonDerivativeAuxiliaryEquation(discProblem, problem, opInfoAuxMesh, pi));
	            }
    		}
    		//Particles
    		for (Entry<String, OperatorsInfo> entry : opInfoParticleSpecies.entrySet()) {
            	String speciesName = entry.getKey();
            	OperatorsInfo opInfoParticles = opInfoAuxParticles.get(speciesName);
            	int startParticles = startParticlesSpecies.get(speciesName);
            	ProcessInfo newPi = new ProcessInfo(pi);
                newPi.setDiscretizationType(DiscretizationType.particles);
                newPi.setCurrentSpecies(speciesName);
                //Check if any auxiliary field to calculate
	    		if (i >= startParticles && opInfoParticles.getAuxiliaryFieldVariables().size() > 0) {
	    			ArrayList<ParticleNormalization> normalizationInfo = generateNormalizationInfo(doc, step, newPi, opInfoParticles, i - startParticles, lastLoop, false);
	                Element iterOverParticles = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "iterateOverParticles");
	                iterOverParticles.setAttribute("speciesNameAtt", speciesName);
	                Element iterateOverInteractions = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "iterateOverInteractions");
	                iterateOverInteractions.setAttribute("compactSupportRatioAtt", String.valueOf(newPi.getCompactSupportRatio()));
	                iterateOverInteractions.setAttribute("speciesNameAtt", speciesName);
	                ri.setProcessInfo(newPi);
	                if (opInfoParticles.getAuxiliaryDerivativeLevels().get(i - startParticles).getDerivatives().size() > 0) {
		    			ArrayList<Element> nonConsInputVariables = getNonConsFunctionInputVariables(step, DiscretizationType.particles);
	                    //Initialize normalizations
		            	iterOverParticles.appendChild(initializeNormalizationVars(doc, step, newPi, null, opInfoParticles, i - startParticles, OperatorInfoType.auxiliaryField, lastLoop, false));
		            	//Calculate normalization
		            	iterateOverInteractions.appendChild(calculateNormalizationFunctions(doc, discProblem, problem, step, newPi, opInfoParticles, i - startParticles, normalizationInfo, lastLoop));
			            //Nth order derivative calculation
		            	iterateOverInteractions.appendChild(createDerivativeCalculation(doc, opInfoParticles, opInfoParticles.getAuxiliaryDerivativeLevels().get(i), discProblem, problem, newPi, nonConsInputVariables, i - startParticles, 
			        			lastLoop, simplificationsParticles, lastLoop, null));
		            }
		            //No last block
		            if (!lastLoop) {
		             	iterOverParticles.appendChild(initializeDerivativeVariables(doc, opInfoParticles, opInfoParticles.getAuxiliaryDerivativeLevels().get(i - startParticles), newPi, i - startParticles));
		                //Reset simplification terms for the next level
		                simplificationsParticles = new AlgebraicSimplifications();
	
		                //Nth order order algebraic terms
		                if (opInfoParticles.getAuxiliaryDerivativeLevels().get(i - startParticles).getDerivatives().size() > 0) {
		                	iterateOverInteractions.appendChild(createAlgebraicTerms(doc, opInfoParticles.getAuxiliaryDerivativeLevels().get(i + 1 - startParticles), newPi, timeOutputVariable, usedTermsParticles, simplificationsParticles));
		                }
		                if (iterateOverInteractions.hasChildNodes()) {
		                	//Add particle position
		                    Element particlePosition = createPositionReference(doc, (Element) convertToPosition(timeOutputVariable.cloneNode(true)), newPi);
		                	iterateOverInteractions.insertBefore(particlePosition, iterateOverInteractions.getFirstChild());
		                	iterOverParticles.appendChild(iterateOverInteractions);
		                }
		                //Normalization
		                iterOverParticles.appendChild(securizeNormalizations(doc, step, newPi, opInfoParticles, i - startParticles, lastLoop, false));
		                iterOverParticles.appendChild(createNormalizeRHSFunctions(doc, step, newPi, null, opInfoParticles, i - startParticles, normalizationInfo, opInfoParticles.getAuxiliaryNormalizationLevels(), false, false, true));
		                //Multiplication of derivatives and algebraic terms
		                iterOverParticles.appendChild(createDerivativeMultiplication(doc, opInfoParticles, opInfoParticles.getAuxiliaryMultiplicationLevels(), opInfoParticles.getAuxiliaryDerivativeLevels(), newPi, timeOutputVariable, OperatorInfoType.auxiliaryField, i - startParticles, lastLoop));
		            }
		            //Last block
		            else {
		                if (iterateOverInteractions.hasChildNodes()) {
		                	//Add particle position
		                    Element particlePosition = createPositionReference(doc, (Element) convertToPosition(timeOutputVariable.cloneNode(true)), newPi);
		                	iterateOverInteractions.insertBefore(particlePosition, iterateOverInteractions.getFirstChild());
		                	iterOverParticles.appendChild(iterateOverInteractions);
		                }
		            	
		                //Auxiliary variables (Only the ones containing sources)
		                if (opInfoParticles.getAuxiliaryDerivativeLevels().get(i - startParticles).getDerivatives().size() > 0) {
		                	if (opInfoParticles.hasDerivatives(OperatorInfoType.auxiliaryVariable)) {
		                        throw new AGDMException(AGDMException.AGDM005, "Auxiliary variables used in Auxiliary fields cannot be defined using derivatives.");
		                	}
		                	iterOverParticles.appendChild(createAuxiliaryVariableEquations(discProblem, problem, opInfoParticles, opInfoParticles.getDerivativeLevels(), opInfoParticles.getMultiplicationLevels(), opInfoParticles.getDerivativeLevels(), opInfoParticles.getMultiplicationLevels(), newPi, (Element) timeOutputVariable.cloneNode(true), i - startParticles, false));
		                }
		                //Sources (they will only be in the last level)
		            	if (opInfoParticles.getAuxiliaryDerivativeLevels().get(i - startParticles).getDerivatives().size() > 0) {
		                	iterOverParticles.appendChild(createNonConsSourcesParticles(doc, opInfoParticles.getAuxiliaryDerivativeLevels().get(i - startParticles), newPi, timeOutputVariable, timeOutputVariable, OperatorInfoType.auxiliaryField));		                	
		            	}

		            	//Normalize variables
		                if (opInfoParticles.getAuxiliaryDerivativeLevels().get(i - startParticles).getDerivatives().size() > 0) {
			                iterOverParticles.appendChild(securizeNormalizations(doc, step, newPi, opInfoParticles, i - startParticles, lastLoop, false));
		                	iterOverParticles.appendChild(createNormalizeRHSFunctions(doc, step, newPi, null, opInfoParticles, i - startParticles, normalizationInfo, opInfoParticles.getAuxiliaryNormalizationLevels(), false, false, true));
		                }
		                //Multiplication of derivatives, algebraic terms and auxiliary definitions
		                iterOverParticles.appendChild(createDerivativeMultiplication(doc, opInfoParticles, opInfoParticles.getAuxiliaryMultiplicationLevels(), opInfoMesh.getAuxiliaryDerivativeLevels(), newPi, timeOutputVariable, OperatorInfoType.auxiliaryField, i - startParticles, lastLoop));
		                //Equation term summation
		                iterOverParticles.appendChild(createDerivativeAuxiliaryEquation(doc, opInfoParticles, newPi, timeOutputVariable));
		                //Non derivative auxiliary equations dependent from derivative ones.
		               	iterOverParticles.appendChild(createNonDerivativeAuxiliaryEquation(discProblem, problem, opInfoParticles, newPi));
		            }
		            if (iterOverParticles.hasChildNodes()) {
		    			iterOverCells4.appendChild(iterOverParticles);
		    		
		    		}
	    		}
    		}
            // Stencil attribute
        	int stencil = 0;
    		for (Entry<String, OperatorsInfo> entry : opInfoParticleSpecies.entrySet()) {
            	String speciesName = entry.getKey();
            	OperatorsInfo opInfoParticles = opInfoAuxParticles.get(speciesName);
            	int startParticles = startParticlesSpecies.get(speciesName);
            	if (opInfoParticles.getAuxiliaryDerivativeLevels().get(i - startParticles).getDerivatives().size() > 0) {
            		stencil = 1;
            	}
    		}

        	if (hasNonConservativeTermsMesh) {
        		stencil = getLevelStencil(opInfoMesh.getAuxiliaryDerivativeLevels().get(i - startMesh).getDerivatives()); 
        	}
            iterOverCells4.setAttribute("stencilAtt", String.valueOf(stencil));
            //Append attribute
            if ((i + 2 == opInfoMesh.getAuxiliaryDerivativeLevels().size()) || (i >= startMesh && i + 1 < opInfoMesh.getAuxiliaryDerivativeLevels().size() && opInfoMesh.getAuxiliaryDerivativeLevels().get(i + 1 - startMesh) != null)) {
            	iterOverCells4.setAttribute("appendAtt", String.valueOf(accumulatedStencil.get(i + 1)));    
            }
    		if (lastLoop) {
				iterOverCells4.setAttribute("excludePhysicalBoundariesAtt", "true");
			}
            insertCornerCalculation(iterOverCells4);
            result.appendChild(iterOverCells4);
        }
        conditionalUnification(result);
        
        return result;
    }
    
    /**
     * Get the time input variables from a given schema step.
     * @param step              The step
     * @return                  The input variables
     * @throws AGDMException    AGDM00X - External error
     */
    private static ArrayList<Element> getExplicitTimeInputVariables(Element step) throws AGDMException {
        ArrayList<Element> variables = new ArrayList<Element>();
        NodeList maths = AGDMUtils.find(step, ".//mms:timeDiscretization/mms:explicit//mms:inputVariable/mt:math");
        for (int i = 0; i < maths.getLength(); i++) { 
            variables.add((Element) maths.item(i).getFirstChild());
        }
        return variables;
    }
    
    /**
     * Get the implicit time input variables from a given schema step.
     * @param step              The step
     * @return                  The input variables
     * @throws AGDMException    AGDM00X - External error
     */
    private static ArrayList<Element> getImplicitTimeInputVariables(Element step) throws AGDMException {
        ArrayList<Element> variables = new ArrayList<Element>();
        NodeList maths = AGDMUtils.find(step, ".//mms:timeDiscretization/mms:implicit//mms:inputVariable/mt:math");
        for (int i = 0; i < maths.getLength(); i++) { 
            variables.add((Element) maths.item(i).getFirstChild());
        }
        return variables;
    }
    
    /**
     * Get the time input variables from a given schema step.
     * @param step              The step
     * @return                  The input variables
     * @throws AGDMException    AGDM00X - External error
     */
    private static ArrayList<Element> getTimePositionInputVariables(Element step) throws AGDMException {
        ArrayList<Element> variables = new ArrayList<Element>();
        NodeList maths = AGDMUtils.find(step, ".//mms:timeDiscretization//mms:positionInputVariable/mt:math");
        for (int i = 0; i < maths.getLength(); i++) { 
            variables.add((Element) maths.item(i));
        }
        return variables;
    }
        
    /**
     * Get the conservative discretization input variables from a given schema step.
     * @param step              The step
     * @param dt				Discretization type
     * @return                  The input variables
     * @throws AGDMException    AGDM00X - External error
     */
    private static ArrayList<Element> getConservativeDiscretizationInputVariables(Element step, DiscretizationType dt) throws AGDMException {
    	String query = "";
    	if (dt == DiscretizationType.mesh) {
    		query = ".//mms:meshDiscretization//mms:inputVariable/mt:math";
    	}
    	else {
    		query = ".//mms:particleDiscretization//mms:derivativeFunction//mms:inputVariable/mt:math";
    	}
        ArrayList<Element> variables = new ArrayList<Element>();
        NodeList maths = AGDMUtils.find(step, query);
        for (int i = 0; i < maths.getLength(); i++) {
            variables.add((Element) maths.item(i));
        }
        return variables;
    }
    
    /**
     * Get the particle advective discretization input variables from a given schema step.
     * @param step              The step
     * @return                  The input variables
     * @throws AGDMException    AGDM00X - External error
     */
    private static ArrayList<Element> getAdvectiveDerivativeFunctionInputVariables(Element step) throws AGDMException {
    	String query = ".//mms:particleAdvectiveTerm//mms:particleDerivativeFunction//mms:inputVariable/mt:math";
        ArrayList<Element> variables = new ArrayList<Element>();
        NodeList maths = AGDMUtils.find(step, query);
        for (int i = 0; i < maths.getLength(); i++) {
            variables.add((Element) maths.item(i));
        }
        return variables;
    }
    
    /**
     * Get the particle advective normalization input variables from a given schema step.
     * @param step              The step
     * @return                  The input variables
     * @throws AGDMException    AGDM00X - External error
     */
    private static ArrayList<Element> getAdvectiveNormalizationFunctionInputVariables(Element step) throws AGDMException {
    	String query = ".//mms:particleAdvectiveTerm//mms:particleNormalizationFunction//mms:inputVariable/mt:math";
        ArrayList<Element> variables = new ArrayList<Element>();
        NodeList maths = AGDMUtils.find(step, query);
        for (int i = 0; i < maths.getLength(); i++) {
            variables.add((Element) maths.item(i));
        }
        return variables;
    }
    
    /**
     * Get the interpolation function input variables from a given schema step.
     * @param step              The step
     * @return                  The input variables
     * @throws AGDMException    AGDM00X - External error
     */
    private static ArrayList<Element> getParticleInterpolationFunctionInputVariables(Element step) throws AGDMException {
    	String query = ".//mms:particleInterpolationMethods/mms:particleInterpolationFunction//mms:inputVariable/mt:math";
        ArrayList<Element> variables = new ArrayList<Element>();
        NodeList maths = AGDMUtils.find(step, query);
        for (int i = 0; i < maths.getLength(); i++) {
            variables.add((Element) maths.item(i));
        }
        return variables;
    }
    
    /**
     * Get particle interpolation normalization input variables from a given schema step.
     * @param step              The step
     * @return                  The input variables
     * @throws AGDMException    AGDM00X - External error
     */
    private static ArrayList<Element> getParticleInterpolationNormalizationInputVariables(Element step) throws AGDMException {
    	String query = ".//mms:particleInterpolationMethods/mms:particleNormalizationFunction//mms:inputVariable/mt:math";
        ArrayList<Element> variables = new ArrayList<Element>();
        NodeList maths = AGDMUtils.find(step, query);
        for (int i = 0; i < maths.getLength(); i++) {
            variables.add((Element) maths.item(i));
        }
        return variables;
    }
    
    /**
     * Get the conservative discretization normalization input variables from a given schema step.
     * @param step              The step
     * @return                  The input variables
     * @throws AGDMException    AGDM00X - External error
     */
    private static ArrayList<Element> getParticleConservativeDerivativeNormalizationInputVariables(Element step) throws AGDMException {
    	String query = ".//mms:conservativeTermDiscretization//mms:normalizationFunction//mms:inputVariable/mt:math";
        ArrayList<Element> variables = new ArrayList<Element>();
        NodeList maths = AGDMUtils.find(step, query);
        for (int i = 0; i < maths.getLength(); i++) {
            variables.add((Element) maths.item(i));
        }
        return variables;
    }
    
    /**
     * Get the conservative discretization normalization input variables from a given schema step.
     * @param step              The step
     * @return                  The input variables
     * @throws AGDMException    AGDM00X - External error
     */
    private static ArrayList<Element> getParticleNonConservativeDerivativeNormalizationInputVariables(Element step) throws AGDMException {
    	String query = ".//mms:particleNormalizationInputVariables//mt:math";
        ArrayList<Element> variables = new ArrayList<Element>();
        NodeList maths = AGDMUtils.find(step, query);
        for (int i = 0; i < maths.getLength(); i++) {
            variables.add((Element) maths.item(i));
        }
        return variables;
    }
    
    /**
     * Get the mesh dissipation input variables from a given schema step.
     * @param step              The step
     * @return                  The input variables
     * @throws AGDMException    AGDM00X - External error
     */
    private static ArrayList<Element> getMeshDissipationInputVariables(Element step) throws AGDMException {
        ArrayList<Element> variables = new ArrayList<Element>();
        NodeList maths = AGDMUtils.find(step, ".//mms:dissipation/mms:meshDissipation//mms:inputVariable/mt:math");
        for (int i = 0; i < maths.getLength(); i++) { 
            variables.add((Element) maths.item(i).getFirstChild());
        }
        return variables;
    }
    
    /**
     * Get the particle dissipation input variables from a given schema step.
     * @param step              The step
     * @return                  The input variables
     * @throws AGDMException    AGDM00X - External error
     */
    private static ArrayList<Element> getParticleDissipationInputVariables(Element step) throws AGDMException {
        ArrayList<Element> variables = new ArrayList<Element>();
        NodeList maths = AGDMUtils.find(step, ".//mms:dissipation/mms:particleDissipation/mms:inputVariables/mms:inputVariable/mt:math");
        for (int i = 0; i < maths.getLength(); i++) { 
            variables.add((Element) maths.item(i).getFirstChild());
        }
        return variables;
    }
    
    /**
     * Get the particle dissipation input variables from a given schema step.
     * @param step              The step
     * @return                  The input variables
     * @throws AGDMException    AGDM00X - External error
     */
    private static ArrayList<Element> getParticleDissipationNormalizationInputVariables(Element step) throws AGDMException {
        ArrayList<Element> variables = new ArrayList<Element>();
        NodeList maths = AGDMUtils.find(step, ".//mms:particleDissipation/mms:normalizationFunction//mms:inputVariable/mt:math");
        for (int i = 0; i < maths.getLength(); i++) { 
            variables.add((Element) maths.item(i));
        }
        return variables;
    }
    
    /**
     * Get the time output variable from a given schema step.
     * @param step              The step
     * @return                  The output variable
     * @throws AGDMException    AGDM00X - External error
     */
    private static Element getStiffRHSVariable(Element step) throws AGDMException {
    	NodeList stiff = AGDMUtils.find(step, ".//mms:timeDiscretization//mms:stiffRHSVariable/mt:math");
    	if (stiff.getLength() == 0) {
    		return null;
    	}
        return (Element) stiff.item(0).getFirstChild().cloneNode(true);
    }
    
    /**
     * Get the time output variable from a given schema step.
     * @param step              The step
     * @return                  The output variable
     * @throws AGDMException    AGDM00X - External error
     */
    private static Element getTimeOutputVariable(Element step) throws AGDMException {
        return (Element) AGDMUtils.find(step, ".//mms:timeDiscretization/mms:outputVariable/mt:math").item(0).getFirstChild().cloneNode(true);
    }
    
    /**
     * Get the spatial output variable from a given schema step.
     * @param step              The step
     * @return                  The output variable
     * @throws AGDMException    AGDM00X - External error
     */
    private static Element getSpatialOutputVariable(Element step) throws AGDMException {
    	NodeList spatialOutput = AGDMUtils.find(step, ".//mms:spatialDiscretization/mms:outputVariable/mt:math");
    	if (spatialOutput.getLength() == 0) {
    		return null;
    	}
        return (Element) spatialOutput.item(0).getFirstChild().cloneNode(true);
    }
    
    /**
     * Get the flux input variable from a given schema step.
     * @param step              The step
     * @param dt				Discretization type
     * @return                  The input variable
     * @throws AGDMException    AGDM00X - External error
     */
    private static ArrayList<Element> getFluxInputVariables(Element step, DiscretizationType dt) throws AGDMException {
    	String query = "";
    	if (dt == DiscretizationType.mesh) {
    		query = ".//mms:meshFluxVariables//mms:inputVariable/mt:math";
    	}
    	else {
    		query = ".//mms:particleFluxVariables//mms:inputVariable/mt:math";
    	}
    	ArrayList<Element> inputVars = new ArrayList<Element>();
    	NodeList variables = AGDMUtils.find(step, query);
    	for (int i = 0; i < variables.getLength(); i++) {
    		inputVars.add((Element) variables.item(i).cloneNode(true));
    	}
    	return inputVars;
    }
    
    /**
     * Get the flux output variable from a given schema step.
     * @param step              The step
     * @param dt				Discretization type
     * @return                  The output variable
     * @throws AGDMException    AGDM00X - External error
     */
    private static ArrayList<Element> getFluxOutputVariables(Element step, DiscretizationType dt) throws AGDMException {
    	String query = "";
    	if (dt == DiscretizationType.mesh) {
    		query = ".//mms:meshFluxVariables//mms:outputVariable/mt:math";
    	}
    	else {
    		query = ".//mms:particleFluxVariables//mms:outputVariable/mt:math";
    	}
    	ArrayList<Element> inputVars = new ArrayList<Element>();
    	NodeList variables = AGDMUtils.find(step, query);
    	for (int i = 0; i < variables.getLength(); i++) {
    		inputVars.add((Element) variables.item(i).cloneNode(true));
    	}
    	return inputVars;
    }
    
    /**
     * Get the non-conservative input variable from a given schema step.
     * @param doc				Current document
     * @param step              The step
     * @return                  The input variable
     * @throws AGDMException    AGDM00X - External error
     */
    private Element getNonConsInputVariable(Document doc, Element step) throws AGDMException {
    	try {
        	NodeList spatialOutput = AGDMUtils.find(step, ".//mms:nonConservativeTermDiscretization/mms:inputVariable/mt:math");
        	if (spatialOutput.getLength() == 0) {
        		return null;
        	}
			return AGDMUtils.addCurrentCell(doc, (Element) spatialOutput.item(0).getFirstChild().cloneNode(true));
		} 
    	catch (AGDMException e) {
			if (hasNonConservativeTermsMesh || hasNonConservativeTermsParticles) {
				throw new AGDMException(AGDMException.AGDM006, 
						"The model has non-conservative terms, but the schema does not include discretization for them.");
			}
			else {
				return null;
			}
		}
    }
    
    /**
     * Get non-conservative input variables.
     * @param step              The step
     * @param dt				Discretization type
     * @return                  The input variable
     * @throws AGDMException    AGDM00X - External error
     */
    private static ArrayList<Element> getNonConsFunctionInputVariables(Element step, DiscretizationType dt) throws AGDMException {
    	String query = "";
    	if (dt == DiscretizationType.mesh) {
    		query = ".//mms:nonConservativeTermDiscretization//mms:meshDerivativeInputVariable/mt:math";
    	}
    	else {
    		query = ".//mms:nonConservativeTermDiscretization//mms:particleFunctionInputVariable/mt:math";
    	}
	  	ArrayList<Element> inputVars = new ArrayList<Element>();
    	NodeList variables = AGDMUtils.find(step, query);
    	for (int i = 0; i < variables.getLength(); i++) {
    		inputVars.add((Element) variables.item(i).cloneNode(true));
    	}
    	return inputVars;
    }
        
    /**
     * Replaces variable given in a list.
     * @param variables			Variable list
     * @param oldVar			Variable to remove
     * @param newVar			Variable to add
     * @return					New list
     * @throws AGDMException 
     */
    private static ArrayList<Element> replaceVariables(ArrayList<Element> variables, Element oldVar, Element newVar) throws AGDMException {
    	ArrayList<Element> outputVars = new ArrayList<Element>();
    	Document doc = newVar.getOwnerDocument();
    	for (int i = 0; i < variables.size(); i++) {
    		Element e = variables.get(i);
    		boolean neighbourParticle = false;
    		//Skip math tag
    		Node etmp = e.getFirstChild();
    		//Also skip sharedVariable and neighbourParticle if exist
    		while (etmp.getLocalName().equals("sharedVariable") || etmp.getLocalName().equals("neighbourParticle")) {
    			if (etmp.getLocalName().equals("neighbourParticle")) {
    				neighbourParticle = true;
    			}
    			etmp = etmp.getFirstChild();
    		}
    		String inputTag = etmp.getLocalName();
    		//Find first tag with same local name
    		Node oldVartmp = oldVar.cloneNode(true);	
    		while (!inputTag.equals(oldVartmp.getLocalName())) {
    			oldVartmp = oldVartmp.getFirstChild();
    			if (oldVartmp == null) {
    				break;
    			}
    		}
    		//If the local name is the same
    		if (oldVartmp!= null && etmp.isEqualNode(oldVartmp)) {
    			//Find first tag with same local name in the new variable
    			Node newVartmp = newVar.cloneNode(true);
    			boolean sharedVar = false;
        		while (!newVartmp.getLocalName().equals(inputTag)) {
        			if (newVartmp.getLocalName().equals("sharedVariable")) {
        				sharedVar = true;
        			}
        			newVartmp = newVartmp.getFirstChild();
        		}
        		//Add shared variable tag
        		if (sharedVar) {
        			Node tmp = newVartmp;
        			newVartmp = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "sharedVariable");
        			newVartmp.appendChild(tmp);
        		}
        		//Add neighbour particle tag
        		if (neighbourParticle) {
        			Node tmp = newVartmp;
        			newVartmp = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "neighbourParticle");
        			newVartmp.appendChild(tmp);
        		}
        		
        		//Create math
        		Node tmp = newVartmp;
    			newVartmp = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "math");
    			newVartmp.appendChild(tmp);
    			outputVars.add((Element) newVartmp);
    		}
    		else {
    			outputVars.add((Element) e.cloneNode(true));
    		}
    	}
    	return outputVars;
    }
    
    /**
     * Get kernel input variables if provided.
     * @param step              The step
     * @return                  The input variable
     * @throws AGDMException    AGDM00X - External error
    ) */
    private static ArrayList<Element> getKernelInputVariables( Element step) throws AGDMException {
	  	ArrayList<Element> inputVars = new ArrayList<Element>();
    	NodeList variables = AGDMUtils.find(step, ".//mms:particleKernelDiscretization/mms:kernel//mms:inputVariable/mt:math");
    	for (int i = 0; i < variables.getLength(); i++) {
    		inputVars.add((Element) variables.item(i).cloneNode(true));
    	}
    	return inputVars;
    }
    
    /**
     * Get kernel gradient input variables if provided.
     * @param step              The step
     * @return                  The input variable
     * @throws AGDMException    AGDM00X - External error
     */
    private static ArrayList<Element> getKernelGradientInputVariables(Element step) throws AGDMException {
	  	ArrayList<Element> inputVars = new ArrayList<Element>();
    	NodeList variables = AGDMUtils.find(step, ".//mms:particleKernelDiscretization/mms:kernelGradient//mms:inputVariable/mt:math");
    	for (int i = 0; i < variables.getLength(); i++) {
    		inputVars.add((Element) variables.item(i).cloneNode(true));
    	}
    	return inputVars;
    }
    
    private static Node getStiffCoefficient(Element step) throws AGDMException {
    	return AGDMUtils.find(step, ".//mms:stiffCoefficient/mt:math").item(0).getFirstChild();
    }
    
    /**
     * Creates the algebraic expressions for the level.
     * @param doc				Current document
     * @param derLevel          The derivative level
     * @param pi                The process information
     * @param previousVariable  The previous variable template
     * @param usedTerms         A list of already used terms
     * @param simplifications   A list of simplifications for algebraic terms
     * @return                  The algebraic expressions
     * @throws AGDMException    AGDM00X - External error
     */
    private DocumentFragment createAlgebraicTerms(Document doc, DerivativeLevel derLevel, ProcessInfo pi, 
            Element previousVariable, Hashtable<NodeWrap, String> usedTerms, 
            AlgebraicSimplifications simplifications) throws AGDMException {
        DocumentFragment result = doc.createDocumentFragment();
        
        //Process algebraic expressions inside derivatives
        ArrayList<Derivative> derivatives = derLevel.getDerivatives();
        for (int i = 0; i < derivatives.size(); i++) {
            Optional<Node> derivativeOp = derivatives.get(i).getDerivativeTerm();
            //Derivative term only in level 0
            if (derivativeOp.isPresent() && !derivatives.get(i).isInner() && !derivatives.get(i).isSimple()) {
                //Check if the algebraic term previously exist
                if (!usedTerms.containsKey(new NodeWrap(derivativeOp.get()))) {
                    Node derivative = derivativeOp.get().cloneNode(true);
                    Element math = AGDMUtils.assignVariable("a" + derivatives.get(i).getId(), previousVariable, 
                            (Element) derivativeOp.get().cloneNode(true), pi);
                    math = (Element) applyConditionalIfExists(math, derivatives.get(i).getConditional());
                    result.appendChild(doc.importNode(math, true));
                    usedTerms.put(new NodeWrap(derivative), "a" + derivatives.get(i).getId());
                }
                else {
                    //If it exists then add the equivalent term
                    simplifications.addSimplification("a" + derivatives.get(i).getId(), usedTerms.get(new NodeWrap(derivativeOp.get())));
                }
            }
        }
        return result;
    }
    
    /**
     * Creates the flux macro deploy.
     * @param doc				Current document
     * @param opInfo			The operators information
     * @param pi				The process information
     * @param step				Current schema step
     * @param fluxesInRHS		If the fluxes will be created in the RHS loop
     * @return					The fluxes
     * @throws AGDMException	AGDM00X - External error
     */
    private DocumentFragment createFluxes(Document discProblem, Node problem, OperatorsInfo opInfo, ProcessInfo pi, Element step, boolean fluxesInRHS) throws AGDMException {
    	Document doc = problem.getOwnerDocument();
        DocumentFragment result = doc.createDocumentFragment();
        
        ArrayList<Element> inputVariables = getFluxInputVariables(step, pi.getDiscretizationType());
        ArrayList<Element> outputVariables = getFluxOutputVariables(step, pi.getDiscretizationType());
        
    	//Auxiliary variables dependant from fluxes (Only the ones containing sources)
    	OperatorsInfo opInfoFluxes = new OperatorsInfo(opInfo);
    	opInfoFluxes.onlyDependent(opInfoFluxes.getFluxVariables(), true, opInfoFluxes.getFluxCommonConditional());
    	if (opInfoFluxes.hasDerivatives(OperatorInfoType.auxiliaryVariable)) {
            throw new AGDMException(AGDMException.AGDM005, "Auxiliary variables used in fluxes cannot be defined using derivatives.");
    	}
        
        //Process all fluxes listed
        for (int i = 0; i < inputVariables.size(); i++) {
            
        	Element inputVariable = (Element) inputVariables.get(i).getFirstChild();
        	Element outputVariable = (Element) outputVariables.get(i).getFirstChild();
        	
        	//Create generic flux call
            Element math = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "math");
            Element apply = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
            Element eq = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "eq");
            
            Element lhs;
            if (fluxesInRHS) {
            	lhs = (Element) doc.importNode(outputVariable.cloneNode(true), true);
            }
            else {
            	lhs = AGDMUtils.addCurrentCell(doc, (Element) outputVariable.cloneNode(true));
            }
            //flux
            Element flux = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "flux");
            Element fluxName = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
            Element rhs;
            if (pi.isExtrapolation() && ProcessInfo.getNumberOfRegions() > 1) {
            	rhs = AGDMUtils.getExtrapolationVariable(doc, true, false);
            }
            else {
            	rhs = AGDMUtils.addCurrentCell(doc, (Element) inputVariable.cloneNode(true));
            }
            flux.appendChild(fluxName);
            flux.appendChild(doc.importNode(rhs, true));
            math.appendChild(apply);
            apply.appendChild(eq);
            apply.appendChild(lhs);
            apply.appendChild(flux);
            
            //Process flux (only fluxes with complex algebraic terms)
            ArrayList<Flux> fluxes = opInfo.getFluxTerms().getFluxes();
            ArrayList<String> coordsUsed = new ArrayList<String>();
            for (int k = 0; k < pi.getBaseSpatialCoordinates().size(); k++) {
	            for (int j = 0; j < fluxes.size(); j++) {
	            	if (!fluxes.get(j).isSimple()) {
		                String coord = fluxes.get(j).getCoordinate();
		                if (coord.equals(pi.getBaseSpatialCoordinates().get(k))) {
			                String field = fluxes.get(j).getField();
			                pi.setField(field);
			                pi.setSpatialCoord1(coord);
			                //Add auxiliary variables before fluxes (only once by coordinate)
			                DocumentFragment tmp = doc.createDocumentFragment();
			                if (!coordsUsed.contains(coord)) {
			                	tmp.appendChild(createAuxiliaryVariableEquations(discProblem, problem, opInfoFluxes, opInfo.getDerivativeLevels(), opInfo.getMultiplicationLevels(), opInfoFluxes.getDerivativeLevels(), opInfoFluxes.getMultiplicationLevels(), pi, (Element) rhs.cloneNode(true), 0, false));
			                	coordsUsed.add(coord);
			                }
		                	//Add fluxes
			                fluxName.setTextContent("F" + pi.getCoordinateRelation().getContToDisc(coord) + field);
			                tmp.appendChild(doc.importNode(ExecutionFlow.replaceTags(math.cloneNode(true), pi), true));
			                result.appendChild(applyConditionalIfExists(tmp, fluxes.get(j).getConditional()));
		                }
	            	}
	            }
            }
        }

        return result;
    }
    
    /**
     * Creates the source expressions for the level.
     * @param doc				Current document
     * @param derLevel          Derivative level
     * @param pi                The process information
     * @param previousVariable  The previous variable template
     * @param opType            The operator type from which generate the terms
     * @return                  The algebraic expressions
     * @throws AGDMException    AGDM00X - External error
     */
    public DocumentFragment createSourcesMesh(Document doc, DerivativeLevel derLevel, ProcessInfo pi, 
            Element previousVariable, OperatorInfoType opType) throws AGDMException {
        DocumentFragment result = doc.createDocumentFragment();
        
        //Process algebraic expressions inside sources
        ArrayList<Derivative> derivatives = derLevel.getDerivatives();
        for (int i = 0; i < derivatives.size(); i++) {
            if (derivatives.get(i).getOperatorType().equals(opType)) {
                Node algebraic = derivatives.get(i).getAlgebraicExpression();
                Optional<Node> derivative = derivatives.get(i).getDerivativeTerm();
                //Sources term
                if (!derivative.isPresent() && derivatives.get(i).getOperatorType() != OperatorInfoType.auxiliaryField) {
                    algebraic = algebraic.cloneNode(true);
                    Element math = AGDMUtils.assignLocalVariable(derivatives.get(i).getId(), previousVariable, 
                            (Element) algebraic.cloneNode(true), pi);
                    math = (Element) applyConditionalIfExists(math, derivatives.get(i).getConditional());
                    result.appendChild(doc.importNode(math, true));
                }
            }
        }
        return result;
    }
    
    /**
     * Creates the source expressions for the level.
     * @param doc				Current document
     * @param derLevel          Derivative level
     * @param pi                The process information
     * @param opType            The operator type from which generate the terms
     * @return                  The algebraic expressions
     * @throws AGDMException    AGDM00X - External error
     */
    public DocumentFragment createAuxFieldSourcesMesh(Document doc, DerivativeLevel derLevel, ProcessInfo pi, 
            OperatorInfoType opType) throws AGDMException {
        DocumentFragment result = doc.createDocumentFragment();
        
        //Process algebraic expressions inside sources
        ArrayList<Derivative> derivatives = derLevel.getDerivatives();
        for (int i = 0; i < derivatives.size(); i++) {
            if (derivatives.get(i).getOperatorType().equals(opType)) {
                Node algebraic = derivatives.get(i).getAlgebraicExpression();
                Optional<Node> derivative = derivatives.get(i).getDerivativeTerm();
                //Sources term
                if (!derivative.isPresent() && derivatives.get(i).getOperatorType() == OperatorInfoType.auxiliaryField) {
                    algebraic = algebraic.cloneNode(true);
                    Element math = AGDMUtils.assignLocalVariable(derivatives.get(i).getId(), timeOutputVariable, 
                            (Element) algebraic.cloneNode(true), pi);
                    math = (Element) applyConditionalIfExists(math, derivatives.get(i).getConditional());

                    result.appendChild(doc.importNode(math, true));
                }
            }
        }
        return result;
    }
    
    /**
     * Create derivative calculation calling the proper spatial operator functions.
     * @param doc					Current document
     * @param derLevel				The derivative level
     * @param discProblem       	The problem being discretized
     * @param problem           	The continuous problem
     * @param opInfo            	The operator information
     * @param pi                	The process information
     * @param inputs				Input variables
     * @param level             	The current level
     * @param lastLoop          	If within the last loop
     * @param simplifications   	A list of simplifications for algebraic terms
     * @param lastDerivativeLevel	When this derivative is in the last level
     * @return                  	The calculation
     * @throws AGDMException    	AGDM00X - External error
     */
    private DocumentFragment createDerivativeCalculation(Document doc, OperatorsInfo opInfo, DerivativeLevel derLevel, Document discProblem, 
            Node problem, ProcessInfo pi, ArrayList<Element> inputs, int level, boolean lastLoop, AlgebraicSimplifications simplifications, boolean lastDerivativeLevel, OperatorInfoType filter) throws AGDMException {
    	DocumentFragment result = doc.createDocumentFragment();
        spatialDiscretizations = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "spatialDiscretizations");
        boundarySpatialDiscretizations = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "boundarySpatialDiscretizations");
        spatialDiscretizations.appendChild(boundarySpatialDiscretizations);
        ArrayList<Derivative> derivatives = derLevel.getDerivatives();
        for (int i = 0; i < derivatives.size(); i++) {
        	if ((!lastDerivativeLevel || derivatives.get(i).getOperatorType() != OperatorInfoType.interaction) 
        			&& (filter == null || (filter == derivatives.get(i).getOperatorType()))) {
        		SpatialOperatorDiscretization disc = derivatives.get(i).getSpatialOperatorDiscretization();
                if (disc != null) {
                    Element spatialDiscretization = findSpatialDiscretization(doc, 
                            derivatives.get(i).getSpatialOperatorDiscretization().getMainSchemaLimits(), pi.getCoordinateRelation());
                    String schemaId = disc.getSchemaId();
                    //Imports the document (if not already imported) and names it for the call
                    String functionNamePrefix = loadSchema(doc, schemaId, discProblem, problem, pi, disc.getSchemaName());
                    spatialDiscretization.appendChild(
                            createSpatialDiscretizationCalls(doc, opInfo, functionNamePrefix, derivatives.get(i), pi, level, lastLoop, simplifications, inputs));
                    //Process boundary schemas
                    ArrayList<BoundarySchema> bs = disc.getBoundarySchemas();
                    if (bs != null) {
                        for (int j = 0; j < bs.size(); j++) {
                            //Search for a boundary with similar conditions for performance reasons
                            Element boundaryDiscretization = findBoundarySpatialDiscretization(doc, bs.get(j).getConditions(), pi.getCoordinateRelation());
                            //Import the document
                            functionNamePrefix = loadSchema(doc, bs.get(j).getSchemaId(), discProblem, problem, pi, bs.get(j).getSchemaName());
                            //Create the function calls
                            boundaryDiscretization.appendChild(
                                    createSpatialDiscretizationCalls(doc, opInfo, functionNamePrefix, derivatives.get(i), pi, level, lastLoop, simplifications, inputs));
                        }
                    }
                    spatialDiscretizations.insertBefore(spatialDiscretization, boundarySpatialDiscretizations);
                }
        	}
        }
        if (spatialDiscretizations.getChildNodes().getLength() > 1 || boundarySpatialDiscretizations.getChildNodes().getLength() > 0) {
        	result.appendChild(spatialDiscretizations);
        }
    	return result;
    }
    
    /**
     * Create derivative calculation calling the proper spatial operator functions.
     * @param doc					Current document
     * @param derLevel				The derivative level
     * @param opInfo            	The operator information
     * @param pi                	The process information
     * @param level             	The current level
     * @return                  	The calculation
     * @throws AGDMException    	AGDM00X - External error
     */
    private DocumentFragment initializeDerivativeVariables(Document doc, OperatorsInfo opInfo, DerivativeLevel derLevel,  
            ProcessInfo pi, int level) throws AGDMException {
    	DocumentFragment result = doc.createDocumentFragment();
        ArrayList<Derivative> derivatives = derLevel.getDerivatives();
        for (int i = 0; i < derivatives.size(); i++) {
        	if (derivatives.get(i).getOperatorType() != OperatorInfoType.interaction) {
        		SpatialOperatorDiscretization disc = derivatives.get(i).getSpatialOperatorDiscretization();
        		if (disc != null) {
	                Derivative derivative = derivatives.get(i);
	                boolean localVariable = checkLocalDerivativeTerm(doc, opInfo.getMultiplicationLevels(),  opInfo.getDerivativeLevels(), derivative.getId(), level);
	                Element math = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "math");
	                Element apply = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
	                Element eq = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "eq");
	                Element zero = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "cn");
	                zero.setTextContent("0");
	                apply.appendChild(eq);
	                //LHS
	                Element lhs = null;
	                if (!localVariable) {
	                    lhs = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
	                    Element ci = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
	                    ci.setTextContent(derivative.getId());
	                    lhs.appendChild(ci);
	                    Element cells = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "currentCell");
	                    lhs.appendChild(cells);
	                    lhs = (Element) doc.importNode(ExecutionFlow.replaceTags(lhs, pi), true);
	                }
	                else {
	                    lhs = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
	                    lhs.setTextContent(derivative.getId());
	                }
	                apply.appendChild(lhs);
	                apply.appendChild(zero);
	                math.appendChild(apply);
	                result.appendChild(math);
        		}
        	}
        }
    	return result;
    }
    
    /**
     * Initializes the RHS variable accumulators.
     * @param doc						Problem
     * @param pi						Process information
     * @param spatialOutputVariable		variable template
     * @param opInfo					Operators info
     * @param currentLevel				Current level
     * @param opType					Operator type
     * @param hasDissipation			If the step has dissipation
     * @return							Code
     * @throws AGDMException			AGDM00X - External error
     */
    private DocumentFragment initializeRHS(Document doc, ProcessInfo pi, Element spatialOutputVariable, OperatorsInfo opInfo, int currentLevel,
    		OperatorInfoType opType, boolean hasDissipation) throws AGDMException {
    	
    	DocumentFragment result = doc.createDocumentFragment();
        ArrayList<String> fields = pi.getParticleFields();
        for (int j = 0; j < fields.size(); j++) {
        	String field = fields.get(j);
            if (!pi.getRegionInfo().isAuxiliaryField(field)) {
                ProcessInfo newPi = new ProcessInfo(pi);
                newPi.setField(field);
          
                //Dissipative terms
                if (hasDissipation) {
    		        for (int k = 0; k < pi.getBaseSpatialCoordinates().size(); k++) {
			        	String coord = pi.getBaseSpatialCoordinates().get(k);
			        	Element math = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "math");
			            Element apply = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
			            Element eq = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "eq");
			            apply.appendChild(eq);
			            //LHS
			            Element lhs = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
			            lhs.setTextContent("RHS_dissipative_term_" + field + "_" + coord);
	                	Element cn = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "cn");
	                	cn.setTextContent("0");
	                	math.appendChild(apply);
	                	apply.appendChild(eq);
	                	apply.appendChild(doc.importNode(lhs, true));
	                	apply.appendChild(cn);
	                	result.appendChild(math);
    		        }
                }
                
                //RHS advective terms
                if (pi.isEulerianField(field)) {
			        for (int k = 0; k < pi.getBaseSpatialCoordinates().size(); k++) {
			        	String coord = pi.getBaseSpatialCoordinates().get(k);
			        	Element math = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "math");
			            Element apply = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
			            Element eq = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "eq");
			            apply.appendChild(eq);
			            //LHS
			            Element lhs = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
			            lhs.setTextContent("RHS_particle_advective_term_" + field + "_" + coord);
	                	Element cn = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "cn");
	                	cn.setTextContent("0");
	                	math.appendChild(apply);
	                	apply.appendChild(eq);
	                	apply.appendChild(doc.importNode(lhs, true));
	                	apply.appendChild(cn);
	                	result.appendChild(math);
			        }
                }
                
                //RHS conservative
                ArrayList<Flux> fluxes = opInfo.getFluxTerms().getFilteredFluxTerms(field);
    	        for (int i = 0; i < fluxes.size(); i++) {
    	        	String coord = fluxes.get(i).getCoordinate();
    	            newPi.setSpatialCoord1(coord);
	        		Element math = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "math");
	        		Element apply = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
	        		Element eq = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "eq");
                	Element variableName = ExecutionFlow.replaceTags(spatialOutputVariable.cloneNode(true), newPi);
                	AGDMUtils.addSuffix(variableName, "_Cons_" + coord);
                	Element cn = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "cn");
                	cn.setTextContent("0");
                	math.appendChild(apply);
                	apply.appendChild(eq);
                	apply.appendChild(doc.importNode(variableName, true));
                	apply.appendChild(cn);
                	result.appendChild(math);
    	        }
  
                //RHS sources    	
    	        if (opInfo.getDerivativeLevels().size() > currentLevel) {
	                if (!opInfo.getDerivativeLevels().get(currentLevel).getFilteredDerivativesSources(field).isEmpty() || opInfo.getSourceTerms().hasSourcesForField(field)) {
		        		Element math = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "math");
		        		Element apply = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
		        		Element eq = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "eq");
	                	Element variableName = null;
	                	if (opInfo.getDerivativeLevels().get(currentLevel).getFilteredDerivativesSources(field).isEmpty()) {
	                		variableName = ExecutionFlow.replaceTags(spatialOutputVariable.cloneNode(true), newPi);
		                	AGDMUtils.addSuffix(variableName, "_sources");
	                	}
	                	else {
	                		variableName = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
	                		variableName.setTextContent(opInfo.getDerivativeLevels().get(currentLevel).getFilteredDerivativesSources(field).get(0).getId());
	                	}
	                 	Element cn = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "cn");
	                	cn.setTextContent("0");
	                	math.appendChild(apply);
	                	apply.appendChild(eq);
	                	apply.appendChild(doc.importNode(variableName, true));
	                	apply.appendChild(cn);
	                	result.appendChild(math);
	                }
    	        }
            }
        }
        return result;
    }
    
    /**
     * Create derivative calculation calling the proper spatial operator functions.
     * @param doc				Current document
     * @param discProblem       The problem being discretized
     * @param problem           The continuous problem
     * @param opInfo            The operator information
     * @param pi                The process information
     * @param level             The current level
     * @param simplifications   A list of simplifications for algebraic terms
     * @return                  The calculation
     * @throws AGDMException    AGDM00X - External error
     */
    public Element createInteractionDerivativeCalculation(Document doc, OperatorsInfo opInfo, Document discProblem, 
            Node problem, ProcessInfo pi, int level, AlgebraicSimplifications simplifications) throws AGDMException {
        spatialDiscretizations = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "spatialDiscretizations");
        boundarySpatialDiscretizations = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "boundarySpatialDiscretizations");
        spatialDiscretizations.appendChild(boundarySpatialDiscretizations);
		ArrayList<Element> nonConsInputVariables = getNonConsFunctionInputVariables((Element) pi.getCurrentTimeStep(), DiscretizationType.mesh);
        ArrayList<Derivative> derivatives = opInfo.getDerivativeLevels().get(level).getDerivatives();
        for (int i = 0; i < derivatives.size(); i++) {
        	if (derivatives.get(i).getOperatorType() == OperatorInfoType.interaction) {
        		SpatialOperatorDiscretization disc = derivatives.get(i).getSpatialOperatorDiscretization();
                if (disc != null) {
                    Element spatialDiscretization = findSpatialDiscretization(doc, 
                            derivatives.get(i).getSpatialOperatorDiscretization().getMainSchemaLimits(), pi.getCoordinateRelation());
                    String schemaId = disc.getSchemaId();
                    //Imports the document (if not already imported) and names it for the call
                    String functionNamePrefix = loadSchema(doc, schemaId, discProblem, problem, pi, disc.getSchemaName());
                    spatialDiscretization.appendChild(
                            createSpatialDiscretizationCalls(doc, opInfo, functionNamePrefix, derivatives.get(i), pi, level, true, simplifications, nonConsInputVariables));
                    spatialDiscretizations.insertBefore(spatialDiscretization, boundarySpatialDiscretizations);
                }
        	}
        }
        //No derivative in interaction (they may be simplified)
  		if (spatialDiscretizations.getChildNodes().getLength() == 1) {
  			return null;
  		}
        return spatialDiscretizations;
    }
    
    /**
     * Find or create a spatial discretization element that fits with the stencil.
     * @param doc						Current document
     * @param mainSchemaLimits          The limits of the main schema to find
     * @param coordInfo                 The coordinate information
     * @return                          The spatial discretization element
     * @throws AGDMException            AGDM00X - External error
     */
    private Element findSpatialDiscretization(Document doc, ArrayList<MainSchemaLimit> mainSchemaLimits, 
            CoordinateInfo coordInfo) throws AGDMException {
        String query = ".//sml:spatialDiscretization[sml:limits[";
        if (mainSchemaLimits.size() > 0) {
            for (int i = 0; i < mainSchemaLimits.size(); i++) {
                MainSchemaLimit limit = mainSchemaLimits.get(i);
                query = query + "sml:limit[@axisAtt = '" + coordInfo.getContToDisc(limit.getAxis()) + "' and @sideAtt = '" 
                        + limit.getSide() + "' and @distanceToBoundaryAtt = '" + limit.getDistanceToBoundary() + "'] and ";
            }
            query = query.substring(0, query.lastIndexOf(" and")) + "]]";
        }
        //No limits in the schema (no boundary schemas)
        else {
            query = ".//sml:spatialDiscretization[not(descendant::sml:limit)]";
        }
        //Check if the spatialDiscretization exists
        NodeList spatialDiscretizationList = AGDMUtils.find(spatialDiscretizations, query);
        if (spatialDiscretizationList.getLength() > 0) {
            return (Element) spatialDiscretizationList.item(0);
        }
        //Create a new spatialDiscretization
        Element spatialDiscretization = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "spatialDiscretization");
        Element spatialDiscretizationLimits = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "limits");
        for (int i = 0; i < mainSchemaLimits.size(); i++) {
            Element limit = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "limit");
            limit.setAttribute("axisAtt", mainSchemaLimits.get(i).getAxis());
            limit.setAttribute("sideAtt", mainSchemaLimits.get(i).getSide());
            limit.setAttribute("distanceToBoundaryAtt", String.valueOf(mainSchemaLimits.get(i).getDistanceToBoundary()));
            spatialDiscretizationLimits.appendChild(limit);
        }
        spatialDiscretization.appendChild(spatialDiscretizationLimits);
        spatialDiscretizations.appendChild(spatialDiscretization);

        return spatialDiscretization;
    }
    
    /**
     * Find or create a boundary spatial discretization that fits with the input.
     * @param doc						Current document
     * @param conditions                The conditions of the boundary to find
     * @param coordInfo                 The coordinate information
     * @return                          The boundary discretization element
     * @throws AGDMException            AGDM00X - External error
     */
    private Element findBoundarySpatialDiscretization(Document doc, ArrayList<BoundarySchemaCondition> conditions, 
            CoordinateInfo coordInfo) throws AGDMException {
        String query = ".//sml:boundarySpatialDiscretization[sml:conditions[";
        for (int i = 0; i < conditions.size(); i++) {
            BoundarySchemaCondition condition = conditions.get(i);
            query = query + "sml:condition[@axisAtt = '" + coordInfo.getContToDisc(condition.getAxis()) + "' and @sideAtt = '" 
                    + condition.getSide() + "' and @distanceToBoundaryAtt = '" + condition.getDistanceToBoundary() + "'] and";
        }
        query = query.substring(0, query.lastIndexOf(" and")) + "]]";
        //Check if the boundarySpatialDiscretization exists
        NodeList boundaryDiscretization = AGDMUtils.find(boundarySpatialDiscretizations, query);
        if (boundaryDiscretization.getLength() > 0) {
            return (Element) boundaryDiscretization.item(0);
        }
        //Create a new boundarySpatialDiscretization
        Element boundarySpatialDiscretization = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "boundarySpatialDiscretization");
        Element boundaryConditions = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "conditions");
        for (int i = 0; i < conditions.size(); i++) {
            Element condition = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "condition");
            condition.setAttribute("axisAtt", conditions.get(i).getAxis());
            condition.setAttribute("sideAtt", conditions.get(i).getSide());
            condition.setAttribute("distanceToBoundaryAtt", String.valueOf(conditions.get(i).getDistanceToBoundary()));
            boundaryConditions.appendChild(condition);
        }
        boundarySpatialDiscretization.appendChild(boundaryConditions);
        boundarySpatialDiscretizations.appendChild(boundarySpatialDiscretization);
        
        return boundarySpatialDiscretization;
    }
    
    /**
     * Loads an schema if not already imported and returns the name for the calling.
     * @param doc				Current document
     * @param discProblem       The problem being discretized
     * @param problem           The continuous problem
     * @param schemaId          The schema id
     * @param pi                The process information
     * @param schemaNamePrefix  The name prefix for the schema to load
     * @return                  The name for the schema call
     * @throws AGDMException    AGDM00X - External error
     */
    private String loadSchema(Document doc, String schemaId, Document discProblem, Node problem, 
            ProcessInfo pi, String schemaNamePrefix) throws AGDMException {
        if (functionsLoaded.containsKey(schemaId)) {
            //Function already loaded
            return functionsLoaded.get(schemaId);
        }
        else {
            //Function not loaded
            //Get the schema document to import
            String schema = "";
            try {
                DocumentManager docMan = new DocumentManagerImpl();
                schema = SimflownyUtils.jsonToXML(docMan.getDocument(schemaId));
            } 
            catch (SUException e) {
                throw new AGDMException(AGDMException.AGDM002, "Spatial operator discretization with id " + schemaId + " not valid.");
            }
            catch (DMException e) {
                throw new AGDMException(AGDMException.AGDM003, "Spatial operator discretization with id " + schemaId + " does not exist.");
            }
            Document importSchema = AGDMUtils.stringToDom(schema);
            //Get schema prefix
            String functionPrefix = AGDMUtils.removeSpecialCharacters(schemaNamePrefix) + "_";
            if (functionsLoaded.containsValue(functionPrefix)) {
                functionPrefix = functionPrefix + AGDMUtils.getCounter() + "_";	
            }
            
            functionsLoaded.put(schemaId, functionPrefix);

            //Get schema order
            int order = getSchemaOrder(importSchema);
            functionOrder.put(functionPrefix, order);
            //Get schema stencil
            int stencil = getSchemaStencil(importSchema);
            maxStencil = Math.max(maxStencil, stencil);
            
            //Call Import tag to load the schemas
            RuleInfo ri = new RuleInfo();
            ri.setProcessInfo(pi);
            ri.setIndividualField(true);
            ri.setIndividualCoord(true);
            ri.setFunction(true);
            ri.setProcessInfo(pi);
            ri.setAuxiliaryCDAdded(false);
            ri.setAuxiliaryFluxesAdded(false);
            ri.setUseBaseCoordinates(false);
            Element rule = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "import");
            Element id = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "rule");
            id.setTextContent(schemaId);
            Element name = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "name");
            name.setTextContent(functionPrefix);
            rule.appendChild(id);
            rule.appendChild(name);
            if (order > 0) {
                Element sufix = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "sufix");
                sufix.appendChild(appendHigherOrderSuffix(doc, order));
                rule.appendChild(sufix);
            }
            ExecutionFlow.processImportTag(discProblem, problem, rule, ri);
            return functionPrefix;
        }         
    }
   
    /**
     * Gets the order of a spatial discretization schema.
     * @param schema            The schema
     * @return                  The order that solves
     * @throws AGDMException    AGDM00X - External error
     */
    //TODO generalizar a orden N
    private static int getSchemaOrder(Node schema) throws AGDMException {
        if (AGDMUtils.find(schema, ExecutionFlow.SECOND_SPATIAL_ORDER_TAGS).getLength() > 0) {
            return 2;
        }
        else {
            if (AGDMUtils.find(schema, ExecutionFlow.FIRST_SPATIAL_ORDER_TAGS).getLength() > 0) {
                return 1;
            }
            else {
                return 0;
            }
        }
    }
    
    /**
     * Gets the stencil of a schema.
     * @param schema            The schema
     * @return                  The stencil needed
     * @throws AGDMException    AGDM00X - External error
     */
    public static int getSchemaStencil(Node schema) throws AGDMException {
        int stencil = 0;
        NodeList instructions = AGDMUtils.find(schema, ".//sml:*[contains(local-name(), 'increment') or contains(local-name(), 'decrement')]");
        for (int i = 0; i < instructions.getLength(); i++) {
            String[] values = instructions.item(i).getTextContent().split(",");
            for (int j = 0; j < values.length; j++) {
                int value = Integer.parseInt(values[j]);
                if (stencil < value) {
                    stencil = value;
                }
            }
        }
        return stencil;
    }
    
    /**
     * Gets the stencil of a schema.
     * @param schemaId          The schema id
     * @return                  The stencil needed
     * @throws AGDMException    AGDM00X - External error
     */
    public static int getSchemaStencil(String schemaId) throws AGDMException {
        //Get the schema document to import
        String schema = "";
        try {
            DocumentManager docMan = new DocumentManagerImpl();
            schema = SimflownyUtils.jsonToXML(docMan.getDocument(schemaId));
        } 
        catch (SUException e) {
            throw new AGDMException(AGDMException.AGDM002, "Spatial operator discretization with id " + schemaId + " not valid.");
        }
        catch (DMException e) {
            throw new AGDMException(AGDMException.AGDM003, "Spatial operator discretization with id " + schemaId + " does not exist.");
        }
        Document importSchema = AGDMUtils.stringToDom(schema);
        return getSchemaStencil(importSchema);
    }
    
    /**
     * Appends the elements necessary to load a high order spatialSchema.
     * @param doc		Current document
     * @param order     The order
     * @return          The suffix for higher order 
     */
    //TODO generalizar a orden N
    private static Node appendHigherOrderSuffix(Document doc, int order) {
        DocumentFragment sufix = doc.createDocumentFragment();
        Element spatialCoordinate = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "spatialCoordinate");
        sufix.appendChild(spatialCoordinate);
        if (order == 2) {
            spatialCoordinate = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "secondSpatialCoordinate");
            sufix.appendChild(spatialCoordinate);
        }
        return sufix;
    }
    
    /**
     * Creates the function call to the spatial operator discretization function of a derivative.
     * @param doc						Current document
     * @param opInfo            		The operator information
     * @param functionNamePrefix        The function name prefix
     * @param derivative                The derivative information
     * @param pi                        The process iformation
     * @param level                     The current level
     * @param lastLoop                  If within the last loop
     * @param simplifications           A list of simplifications for algebraic terms
     * @param inputs					Input variables
     * @return                          The mathml call
     * @throws AGDMException            AGDM00X - External error
     */
    private static Node createSpatialDiscretizationCalls(Document doc, OperatorsInfo opInfo, String functionNamePrefix, 
            Derivative derivative, ProcessInfo pi, int level, boolean lastLoop, AlgebraicSimplifications simplifications, ArrayList<Element> inputs) throws AGDMException {
        //Create mathml call
        Element math = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "math");
        Element apply = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
        Element eq = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "eq");
        apply.appendChild(eq);
        //LHS
        Element lhs = null;
        boolean localVariable = checkLocalDerivativeTerm(doc, opInfo.getMultiplicationLevels(),  opInfo.getDerivativeLevels(), derivative.getId(), level);
        if (lastLoop || localVariable) {
        	lhs = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
            lhs.setTextContent(derivative.getId());

        }
        else {
        	lhs = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
            Element ci = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
            ci.setTextContent(derivative.getId());
            lhs.appendChild(ci);
            Element cells = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "currentCell");
            lhs.appendChild(cells);
            lhs = (Element) doc.importNode(ExecutionFlow.replaceTags(lhs, pi), true);
        }
        apply.appendChild(lhs);
        //RHS
        Element functionCall = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "functionCall");
        Element functionName = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
        Integer order = functionOrder.get(functionNamePrefix);
        if (order > 0) {
        	functionName.setTextContent(functionNamePrefix + getCoordinateSuffix(derivative.getCoordinates(), pi.getCoordinateRelation()));	
        }
        else {
            functionName.setTextContent(functionNamePrefix);	
        }
        functionCall.appendChild(functionName);
       	//Input parameters
    	pi.setSpatialCoord1(derivative.getCoordinates().get(0));
    	for (int i = 0; i < inputs.size(); i++) {
            DocumentFragment varName = doc.createDocumentFragment();
            Element input = (Element) inputs.get(i).cloneNode(true);
    		boolean isStepVariable = pi.isStepVariable(input.getFirstChild(), false);
    		if (isStepVariable) {
    			if (!derivative.isInner()) {
    	        	//If the derivative terms is a simple field, use it directly
    	        	if (derivative.isSimple()) {
    	        		Element tmp = (Element) derivative.getDerivativeTerm().get().cloneNode(true);
    	        		//If variable is a field, use the previousVariable template
    	         	  	if (!(pi.getRegionInfo().isAuxiliaryField(tmp.getTextContent()))) {
    		                AGDMUtils.replaceFieldVariables(pi, (Element) input.getFirstChild(), tmp);
    		                tmp = (Element) tmp.getFirstChild();
    		                if (tmp.getLocalName().equals("sharedVariable")) {
    		                	tmp = (Element) tmp.getFirstChild();
    		                }
    	                }
    	         	  	else {
    	         	  		if ((pi.getRegionInfo().isAuxiliaryField(tmp.getTextContent())) && input.getElementsByTagNameNS(AGDMUtils.simmlUri, "currentCell").getLength() > 0) {
    		                    ProcessInfo newPi = new ProcessInfo(pi);
    		                    newPi.setField(tmp.getTextContent());
    		                    newPi.setSpatialCoord1(null);
    		                    newPi.setSpatialCoord2(null);
    		                    tmp = ExecutionFlow.replaceTags(AGDMUtils.createTimeSlice(doc), newPi); 
	        		            if (input.getFirstChild().getLocalName().equals("neighbourParticle")) {
	        		                Element neighbourParticle = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "neighbourParticle");
	               		            neighbourParticle.appendChild(tmp);
	               		            tmp = neighbourParticle;
	        		            }
    	         	  		} 
    	         	  		else {
    	         	  			tmp = (Element) tmp.getFirstChild();
    	         	  		}
    	         	  	}
    	                varName.appendChild(doc.importNode(tmp, true));
    	        	}
    	        	else {
    		            //Get the simplified variable if exists
    		            String variable = simplifications.getSimplifiedVariable("a" + derivative.getId());
    		            if (variable == null) {
    		                variable = "a" + derivative.getId();
    		            }
    		            Element tmp = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
    		            tmp.setTextContent(variable);
    		            if (pi.getDiscretizationType() == DiscretizationType.particles) {
    		            	tmp = AGDMUtils.addCurrentCell(doc, tmp);
    		            }
    		            if (input.getFirstChild().getLocalName().equals("neighbourParticle")) {
           		            Element neighbourParticle = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "neighbourParticle");
           		            neighbourParticle.appendChild(tmp);
           		            tmp = neighbourParticle;
    		            }
    		            varName.appendChild(doc.importNode(tmp, true));
    	        	}
    	        }
    	        else {
    	            Element tmp = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
    	            tmp.setTextContent(((Element) derivative.getDerivativeTerm().get()).getElementsByTagNameNS(AGDMUtils.mtUri, "ci").item(0).getTextContent());
		            if (pi.getDiscretizationType() == DiscretizationType.particles) {
		            	tmp = AGDMUtils.addCurrentCell(doc, tmp);
		            }
		            if (input.getFirstChild().getLocalName().equals("neighbourParticle")) {
       		            Element neighbourParticle = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "neighbourParticle");
       		            neighbourParticle.appendChild(tmp);
       		            tmp = neighbourParticle;
		            }
   		            varName.appendChild(doc.importNode(tmp, true));
    	        }
    		}
    		else {
    			Element tmp = ExecutionFlow.replaceTags(input, pi);
    			NodeList children = tmp.getChildNodes();
    			for (int j = 0; j < children.getLength(); j++) {
    				varName.appendChild(doc.importNode(children.item(j), true));
    			}
    		}
    		functionCall.appendChild(varName);
    	}
        if (pi.getDiscretizationType() == DiscretizationType.mesh) {
            apply.appendChild(doc.importNode(ExecutionFlow.replaceTags(functionCall, pi), true));
        }
        else {
        	//If particles, accumulate derivatives from neighbours
        	Element applyAcc = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
        	Element plus = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "plus");
        	applyAcc.appendChild(plus);
        	applyAcc.appendChild(lhs.cloneNode(true));
        	applyAcc.appendChild(doc.importNode(ExecutionFlow.replaceTags(functionCall, pi), true));
            apply.appendChild(applyAcc);
        }
        math.appendChild(apply);
        math = (Element) applyConditionalIfExists(math, derivative.getConditional());
        return math;
    }
    
    /**
     * Creates the multiplication of each term from equations.
     * @param doc					Current document
     * @param opInfo                The operator information
     * @param multLevels			The multiplication levels
     * @param derLevels				The derivative levels
     * @param pi                    The process information
     * @param variable              The variable to use as reference
     * @param opType                The operator type from which generate the terms
     * @param level					Derivative level number
     * @param lastLoop				When this is the last loop in the schema step
     * @return                      The terms
     * @throws AGDMException        AGDM00X - External error
     */
    public DocumentFragment createDerivativeMultiplication(Document doc, OperatorsInfo opInfo, ArrayList<MultiplicationTermsLevel> multLevels, ArrayList<DerivativeLevel> derLevels, ProcessInfo pi, 
            Element variable, OperatorInfoType opType, int level, boolean lastLoop) throws AGDMException {
        ArrayList<String> spatialCoords = pi.getCoordinateRelation().getDiscCoords();
        DocumentFragment result = doc.createDocumentFragment();
        if (level < multLevels.size()) {
	        MultiplicationTermsLevel multLevel = multLevels.get(level);
	        if (multLevel != null) {
	            ArrayList<MultiplicationTerm> multiplication = multLevel.getMultiplicationTerms();
	            for (int i = 0; i < multiplication.size(); i++) {
	                if (multiplication.get(i).getOperatorType().equals(opType)) {
	                    Node algebraic = multiplication.get(i).getAlgebraicExpression();
	                    ArrayList<String> variables = multiplication.get(i).getVariables();
	                    //Create multiplication expression
	                    int initialVariable = 0;
	                    Element parent;
	                    //Adding all variables and algebraic expression
	                    if (algebraic != null) {
	                        parent = (Element) algebraic.cloneNode(true);
	                        AGDMUtils.replaceFieldVariables(pi, variable, parent);
	                        parent = (Element) parent.getFirstChild();
	                    }
	                    else {
	                        initialVariable = 1;
	                        boolean localVariable = checkLocalDerivativeTerm(doc, opInfo.getMultiplicationLevels(),  opInfo.getDerivativeLevels(), variables.get(0), level);
	                        parent = getVariableNode(doc, variables.get(0), derLevels, spatialCoords, localVariable);
	                    }
	                    for (int j = initialVariable; j < variables.size(); j++) {
	                    	boolean localVariable = checkLocalDerivativeTerm(doc, opInfo.getMultiplicationLevels(),  opInfo.getDerivativeLevels(), variables.get(j), level);
	                        Element apply = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
	                        Element times = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "times");
	                        Element var = getVariableNode(doc, variables.get(j), derLevels, spatialCoords, localVariable);
	                        
	                        apply.appendChild(times);
	                        apply.appendChild(doc.importNode(parent.cloneNode(true), true));
	                        apply.appendChild(var);
	                        
	                        parent = apply;
	                    }
	      
	                    //Set equality
	                    Element math = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "math");
	                    Element apply = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
	                    Element eq = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "eq");
	                    Element var = null;
	                    if (lastLoop) {
	                    	var = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
	                        var.setTextContent(multiplication.get(i).getId());
	                    }
	                    else {
	                    	var = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
	                        Element ci = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
	                        ci.setTextContent(multiplication.get(i).getId());
	                        var.appendChild(ci);
	                        Element cells = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "currentCell");
	                        var.appendChild(cells);
	                    }
	                    apply.appendChild(eq);
	                    apply.appendChild(var);
		                apply.appendChild(parent);

	                    math.appendChild(apply);
	                    //Add expression to the return list
	                    math = ExecutionFlow.replaceTags(math, pi);
	                    math = (Element) applyConditionalIfExists(math, multiplication.get(i).getConditional());
	                    result.appendChild(doc.importNode(math, true));
	                }
	            }
	        }
        }
        
        return result;
    }
    
    
    /**
     * Creates the summation of terms in an evolution or auxiliary equation.
     * @param doc					Current document
     * @param opInfo                The operator information
     * @param pi                    The process information
     * @param variable              The variable to use as reference
     * @param opType                The operator type from which generate the terms
     * @return                      The evolution equation summations
     * @throws AGDMException        AGDM00X - External error
     */
    public DocumentFragment createEquationSummationMesh(Document doc, OperatorsInfo opInfo, ProcessInfo pi, Element variable, OperatorInfoType opType, boolean fromInteraction) 
            throws AGDMException {
        ArrayList<String> spatialCoords = pi.getCoordinateRelation().getDiscCoords();
        DocumentFragment result = doc.createDocumentFragment();
        EquationTermsLevel termsLevel = opInfo.getEquationTermsLevel();
        if (termsLevel != null) {
            ArrayList<EquationTerms> equations = termsLevel.getEquations();
            for (int i = 0; i < equations.size(); i++) {
                if (equations.get(i).getOperatorType().equals(opType)) {
                    String field = equations.get(i).getFields().get(0);
                    ArrayList<String> variables = equations.get(i).getTerms();
                    boolean localVariable = checkLocalMultiplicationTerm(opInfo, variables.get(0));
                    //Create summation expression
                    Element parent = null;
                    for (int j = 0; j < variables.size(); j++) {
                        localVariable = checkLocalMultiplicationTerm(opInfo, variables.get(j));
                        Element var = null;
                        if (auxTmpVarsWithTemplateVarMesh.contains(variables.get(j))) {
                        	var = getVariableNodeTemplate(doc, variables.get(j), auxTmpVarTemplateMesh, opInfo.getDerivativeLevels(), spatialCoords, pi, localVariable);
                        }
                        else {
                        	var = getVariableNode(doc, variables.get(j), opInfo.getDerivativeLevels(), spatialCoords, localVariable);	
                        }
                        if (parent != null) {
	                        Element apply = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
	                        Element plus = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "plus");
	                        apply.appendChild(plus);
	                        apply.appendChild(parent.cloneNode(true));
	                        apply.appendChild(doc.importNode(var, true));
	                        parent = apply;
                        }
                        else {
                        	parent = var;
                        }
                    }
                    
                    //Set equality
                    ProcessInfo newPi = new ProcessInfo(pi);
                    newPi.setField(field);
               
                    Element math = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "math");
                    Element apply = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
                    Element eq = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "eq");
                    Element lhs = ExecutionFlow.replaceTags(variable.cloneNode(true), newPi);
                    apply.appendChild(eq);
                 	apply.appendChild(doc.importNode(lhs, true));
                    if (fromInteraction || (!opInfo.getSourceTerms().hasSourcesForField(field) && opInfo.getFluxTerms().getFilteredFluxTerms(field).size() == 0)) {
                        apply.appendChild(parent);
                    }
                    else {
    	            	Element applyAcc = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
    	            	Element plus = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "plus");
    	            	applyAcc.appendChild(plus);
    	            	applyAcc.appendChild(doc.importNode(lhs.cloneNode(true), true));
    	            	applyAcc.appendChild(parent);
    	            	apply.appendChild(applyAcc);
                    }
                    math.appendChild(apply);
               
                    math = ExecutionFlow.replaceTags(math, pi);
                    math = (Element) applyConditionalIfExists(math, equations.get(i).getConditional());
                    result.appendChild(doc.importNode(math, true));
                }
            }
        }
    
        return result;
    }
    
    /**
     * Creates the summation of terms in an evolution or auxiliary equation.
     * @param doc					Current document
     * @param opInfo                The operator information
     * @param pi                    The process information
     * @param variable              The variable to use as reference
     * @param opType                The operator type from which generate the terms
     * @param hasDissipation		If the step has dissipation
     * @return                      The evolution equation summations
     * @throws AGDMException        AGDM00X - External error
     */
    public DocumentFragment createEquationSummationParticles(Document doc, OperatorsInfo opInfo, ProcessInfo pi, Element variable, OperatorInfoType opType, boolean hasDissipation) 
            throws AGDMException {
        ArrayList<String> spatialCoords = pi.getCoordinateRelation().getDiscCoords();
        DocumentFragment result = doc.createDocumentFragment();
        EquationTermsLevel termsLevel = opInfo.getEquationTermsLevel();
        ArrayList<String> evolFields = opInfo.getEvolutionFields();
        for (int i = 0; i < evolFields.size(); i++) {
        	//Create summation expression
            Element parent = null;
            String field = evolFields.get(i);
            EquationTerms equations = termsLevel.getEquationsForField(field);
	        if (equations != null) {
                if (equations.getOperatorType().equals(opType)) {
                    ArrayList<String> variables = equations.getTerms();
                    boolean localVariable = checkLocalMultiplicationTerm(opInfo, variables.get(0));
                  
                    //Non conservative terms
                    for (int j = 0; j < variables.size(); j++) {
                        localVariable = checkLocalMultiplicationTerm(opInfo, variables.get(j));
                        Element var = null;
                        if (auxTmpVarsWithTemplateVarMesh.contains(variables.get(j))) {
                        	var = getVariableNodeTemplate(doc, variables.get(j), auxTmpVarTemplateMesh, opInfo.getDerivativeLevels(), spatialCoords, pi, localVariable);
                        }
                        else {
                        	var = getVariableNode(doc, variables.get(j), opInfo.getDerivativeLevels(), spatialCoords, localVariable);	
                        }
                        if (parent != null) {
	                        Element apply = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
	                        Element plus = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "plus");
	                        apply.appendChild(plus);
	                        apply.appendChild(parent.cloneNode(true));
	                        apply.appendChild(doc.importNode(var, true));
	                        parent = apply;
                        }
                        else {
                        	parent = var;
                        }
                    }
                    
                }
	        }
                    
            ProcessInfo newPi = new ProcessInfo(pi);
            newPi.setField(field);
            //Sources
            if (opInfo.getSourceTerms().hasSourcesForField(field)) {
            	Element variableName = ExecutionFlow.replaceTags(variable.cloneNode(true), newPi);
            	AGDMUtils.addSuffix(variableName, "_sources");
                if (parent != null) {
                    Element apply = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
                    Element plus = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "plus");
                    apply.appendChild(plus);
                    apply.appendChild(parent.cloneNode(true));
                    apply.appendChild(doc.importNode(variableName, true));
                    parent = apply;
                }
                else {
                	parent = (Element) doc.importNode(variableName, true);
                }
	        }
            //Conservative
            ArrayList<Flux> fluxes = opInfo.getFluxTerms().getFilteredFluxTerms(field);
	        for (int j = 0; j < fluxes.size(); j++) {
	        	String coord = fluxes.get(j).getCoordinate();
	            newPi.setSpatialCoord1(coord);
	            newPi.setSpatialCoord2(null);
            	Element variableName = ExecutionFlow.replaceTags(variable.cloneNode(true), newPi);
            	AGDMUtils.addSuffix(variableName, "_Cons_" + coord);
       
                if (parent != null) {
                    Element apply = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
                    Element plus = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "plus");
                    apply.appendChild(plus);
                    apply.appendChild(parent.cloneNode(true));
                    apply.appendChild(doc.importNode(variableName, true));
                    parent = apply;
                }
                else {
                	parent = variableName;
                }
	        }
            //Advective terms
	        if (pi.isEulerianField(field)) {
		        for (int k = 0; k < pi.getBaseSpatialCoordinates().size(); k++) {
		        	String coord = pi.getBaseSpatialCoordinates().get(k);
		        	newPi.setSpatialCoord1(coord);
		        	newPi.setSpatialCoord2(null);
		            //LHS
		            Element variableName = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
		            variableName.setTextContent("RHS_particle_advective_term_" + field + "_" + coord);
                    if (parent != null) {
                        Element apply = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
                        Element plus = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "plus");
                        apply.appendChild(plus);
                        apply.appendChild(parent.cloneNode(true));
                        apply.appendChild(doc.importNode(variableName, true));
                        parent = apply;
                    }
                    else {
                    	parent = variableName;
                    }
		        }  
	        }
	        //Dissipative terms
	        if (hasDissipation) {
	        	Element apply = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
                Element times = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "times");
                Element influenceRadius = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "influenceRadius");
                Element dissipation_factor = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
                dissipation_factor.setTextContent("dissipation_factor_" + field);
                apply.appendChild(times);
                apply.appendChild(dissipation_factor);
                apply.appendChild(influenceRadius);
                Element vars = null;
   		        for (int k = 0; k < pi.getBaseSpatialCoordinates().size(); k++) {
		        	String coord = pi.getBaseSpatialCoordinates().get(k);
                    
		        	Element variableName = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
		            variableName.setTextContent("RHS_dissipative_term_" + field + "_" + coord);
                 	if (vars != null) {
                        Element applyPlus = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
                        Element plus = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "plus");
                        applyPlus.appendChild(plus);
                        applyPlus.appendChild(vars.cloneNode(true));
                        applyPlus.appendChild(doc.importNode(variableName, true));
                        vars = applyPlus;
                    }
                    else {
                    	vars = variableName;
                    }
   		        }
   		        apply.appendChild(vars);
             	if (parent != null) {
                    Element applyPlus = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
                    Element plus = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "plus");
                    applyPlus.appendChild(plus);
                    applyPlus.appendChild(parent.cloneNode(true));
                    applyPlus.appendChild(apply);
                    parent = applyPlus;
                }
                else {
                	parent = apply;
                }
            }
	       
            //Set equality               
            Element math = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "math");
            Element apply = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
            Element eq = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "eq");
            Element lhs = ExecutionFlow.replaceTags(variable.cloneNode(true), newPi);
            apply.appendChild(eq);
         	apply.appendChild(doc.importNode(lhs, true));
            apply.appendChild(parent);
            math.appendChild(apply);
       
            math = ExecutionFlow.replaceTags(math, pi);
            if (equations != null) {
            	math = (Element) applyConditionalIfExists(math, equations.getConditional());
            }
            result.appendChild(doc.importNode(math, true));
        }
        return result;
    }
    
    
    
    /**
     * Creates the summation of terms in auxiliary equations.
     * @param doc					Current document
     * @param opInfo                The operator information
     * @param pi                    The process information
     * @param variable              The variable to use as reference
     * @return                      The evolution equation summations
     * @throws AGDMException        AGDM00X - External error
     */
    public static DocumentFragment createDerivativeAuxiliaryEquation(Document doc, OperatorsInfo opInfo, ProcessInfo pi, Element variable) 
            throws AGDMException {
        ArrayList<String> spatialCoords = pi.getCoordinateRelation().getDiscCoords();
        DocumentFragment result = doc.createDocumentFragment();
        EquationTermsLevel termsLevel = opInfo.getAuxEquations();
        if (termsLevel != null) {
            ArrayList<EquationTerms> equations = termsLevel.getEquations();
            for (int i = 0; i < equations.size(); i++) {
            	EquationType auxiliaryFieldType = equations.get(i).getEquationType();
            	if (auxiliaryFieldType == EquationType.algebraic) {
            		String field = equations.get(i).getFields().get(0);
            		ArrayList<String> variables = equations.get(i).getTerms();
                    boolean localVariable = checkLocalAuxMultiplicationTerm(opInfo, variables.get(0));
                    //Create summation expression
                    Element parent = getVariableNode(doc, variables.get(0), opInfo.getAuxiliaryDerivativeLevels(), spatialCoords, localVariable);
                    parent.setTextContent(variables.get(0));
                    for (int j = 1; j < variables.size(); j++) {
                        Element apply = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
                        Element plus = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "plus");
                        localVariable = checkLocalAuxMultiplicationTerm(opInfo, variables.get(j));
                        Element var = getVariableNode(doc, variables.get(j), opInfo.getAuxiliaryDerivativeLevels(), spatialCoords, localVariable);
                        
                        apply.appendChild(plus);
                        apply.appendChild(parent.cloneNode(true));
                        apply.appendChild(var);
                        
                        parent = apply;
                    }
                    
                    //Set equality
                    ProcessInfo newPi = new ProcessInfo(pi);
                    newPi.setField(field);
               
                    Element math = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "math");
                    Element apply = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
                    Element eq = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "eq");
                    apply.appendChild(eq);
                    apply.appendChild(doc.importNode(ExecutionFlow.replaceTags(AGDMUtils.createTimeSlice(doc), newPi), true));
                    apply.appendChild(parent);
                    math.appendChild(apply);
                    math = (Element) applyConditionalIfExists(math, equations.get(i).getConditional());
                    math = ExecutionFlow.replaceTags(math, pi);
                    result.appendChild(doc.importNode(math, true));
            	}
            }
        }
    
        return result;
    }
    
    /**
     * Creates the algorithm for auxiliary equations.
     * @param discProblem			The problem being discretized
     * @param problem           	The continuous problem
     * @param opInfo                The operator information
     * @param pi                    The process information
     * @param variable              The variable to use as reference
     * @return                      The evolution equation summations
     * @throws AGDMException        AGDM00X - External error
     */
    public DocumentFragment createNonDerivativeAuxiliaryEquation(Document discProblem, Node problem, OperatorsInfo opInfo, ProcessInfo pi) 
            throws AGDMException {
    	Document doc = problem.getOwnerDocument();
        DocumentFragment result = doc.createDocumentFragment();
        EquationTermsLevel termsLevel = opInfo.getAuxEquations();
        if (termsLevel != null) {
            ArrayList<EquationTerms> equations = termsLevel.getEquations();
            for (int i = 0; i < equations.size(); i++) {
            	EquationType auxiliaryFieldType = equations.get(i).getEquationType();
            	if (auxiliaryFieldType == EquationType.algorithmic) {
	                Element simml = (Element) equations.get(i).getAlgorithm().cloneNode(true);
	                //Create the equation with the time slice
	                DocumentFragment simmlInstr = simml.getOwnerDocument().createDocumentFragment();
	                for (int l = 0; l < simml.getChildNodes().getLength(); l++) {
	                	simmlInstr.appendChild(simml.getChildNodes().item(l).cloneNode(true));
	                }
	                Node tmp = applyConditionalIfExists(simmlInstr, equations.get(i).getConditional());
	                simml = (Element) equations.get(i).getAlgorithm().cloneNode(false);
	                simml.appendChild(tmp);
	                AGDMUtils.replaceFieldVariables(pi, timeOutputVariable, simml);
	            	result.appendChild(ExecutionFlow.processSimml(simml, discProblem, problem, pi, true));
            	}
            }
        }
        return result;
    }
    
    /**
     * Check when a multiplication term is a local variable.
     * @param opInfo		The operators Information
     * @param multTerm		The multiplication term to check
     * @return				True if local
     */
    public static boolean checkLocalMultiplicationTerm(OperatorsInfo opInfo, String multTerm) {
    	//Check only multiplication terms, otherwise is a local variable
    	if (!multTerm.startsWith("m")) {
    		return true;
    	}
    	//Only last level multiplication terms are local
    	
    	if (opInfo.getMultiplicationLevels().size() > 0) {
	    	ArrayList<MultiplicationTerm> lastLoopMultTerms = opInfo.getMultiplicationLevels().get(opInfo.getMultiplicationLevels().size() - 1).getMultiplicationTerms();
	    	for (int i = 0; i < lastLoopMultTerms.size(); i++) {
	    		String termId = lastLoopMultTerms.get(i).getId();
	    		if (termId.equals(multTerm)) {
	    			return true;
	    		}
	    	}
    	}
    	return false;
    }
    
    /**
     * Check when a multiplication term is a local variable.
     * @param opInfo		The operators Information
     * @param multTerm		The multiplication term to check
     * @return				True if local
     */
    public static boolean checkLocalAuxMultiplicationTerm(OperatorsInfo opInfo, String multTerm) {
    	//Check only multiplication terms, otherwise is a local variable
    	if (!multTerm.startsWith("m")) {
    		return true;
    	}
    	//Only last level multiplication terms are local
    	
    	if (opInfo.getAuxiliaryMultiplicationLevels().size() > 0) {
	    	ArrayList<MultiplicationTerm> lastLoopMultTerms = opInfo.getAuxiliaryMultiplicationLevels().get(opInfo.getAuxiliaryMultiplicationLevels().size() - 1).getMultiplicationTerms();
	    	for (int i = 0; i < lastLoopMultTerms.size(); i++) {
	    		String termId = lastLoopMultTerms.get(i).getId();
	    		if (termId.equals(multTerm)) {
	    			return true;
	    		}
	    	}
    	}
    	return false;
    }
    
    /**
     * Check when a derivative term is a local variable.
     * @param doc				Current document
     * @param opInfo			The operators Information
     * @param derTerm			The derivative term to check
     * @param level				Current derivative term level
     * @return					True if local
     * @throws AGDMException	AGDM00X - External error
     */
    public static boolean checkLocalDerivativeTerm(Document doc, ArrayList<MultiplicationTermsLevel> mult, ArrayList<DerivativeLevel> ders, String derTerm, int level) throws AGDMException {
    	//A derivative term is local if it is only used in the same level multiplicative terms or derivative term
    	for (int i = 0; i < ders.size(); i++) {
    		//Check if the derivative term is used in next levels...
    		if (i > level) {
        		//... in a multiplication term ...
	    		ArrayList<MultiplicationTerm> multTerms = mult.get(i).getMultiplicationTerms();
	        	for (int j = 0; j < multTerms.size(); j++) {
	        		if (multTerms.get(j).getVariables().contains(derTerm)) {
	        			return false;
	        		}
	        	}
        		//... or in a derivative term ...
	        	ArrayList<Derivative> derTerms = ders.get(i).getDerivatives();
	        	for (int j = 0; j < derTerms.size(); j++) {
	        		if (AGDMUtils.find(derTerms.get(j).getDerivativeTerm().get(), ".//mt:ci[text() = '" + derTerm + "']").getLength() > 0) {
	        			return false;
	        		}
	        	}
	    	}
    		//Check if the derivative term is defined in a previous level 
    		if (i < level) {
	        	ArrayList<Derivative> derTerms = ders.get(i).getDerivatives();
	        	for (int j = 0; j < derTerms.size(); j++) {
	        		if (derTerms.get(j).getId().equals(derTerm)) {
	        			return false;
	        		}
	        	}
	    	}
    	}
    	return true;
    }
    
    /**
     * Deploy the auxiliary equations in strict order. Auxiliary equations may have interdependencies.
     * @param doc					Current document
     * @param filteredOpInfo        The operator information
     * @param derLevels				All derivative levels
     * @param multLevels				All multiplicative levels
     * @param fDerLevels		    Filtered derivative levels
     * @param fMultLevels				Filtered multiplicative levels
     * @param pi                    The process information
     * @param variable              The variable to use as reference
     * @param level                 The level to create the instructions from
     * @param neighbourCalculated	Equation evaluated in neighbour particles
     * @return                      The evolution equation summations
     * @throws AGDMException        AGDM00X - External error
     */
    public static DocumentFragment createAuxiliaryVariableEquations(Document discProblem, Node problem, OperatorsInfo filteredOpInfo, ArrayList<DerivativeLevel> derLevels, ArrayList<MultiplicationTermsLevel> multLevels, ArrayList<DerivativeLevel> fDerLevels, ArrayList<MultiplicationTermsLevel> fMultLevels, ProcessInfo pi, Element variable, int level, boolean neighbourCalculated) 
            throws AGDMException {
    	Document doc = problem.getOwnerDocument();
        ArrayList<String> spatialCoords = pi.getCoordinateRelation().getDiscCoords();
        HashMap<String, String> removedVars = new LinkedHashMap<String, String>();
        auxTmpVarsWithTemplateVarMesh = new ArrayList<String>();
        auxTmpVarTemplateMesh = (Element) variable.cloneNode(true);
        DocumentFragment result = doc.createDocumentFragment();
        EquationTermsLevel termsLevel = filteredOpInfo.getEquationTermsLevel();
        if (termsLevel != null) {
            ArrayList<EquationTerms> equations = termsLevel.getEquations();
            for (int i = 0; i < equations.size(); i++) {
                if (equations.get(i).getOperatorType().equals(OperatorInfoType.auxiliaryVariable)) {
                	if (equations.get(i).getEquationType() == EquationType.algebraic) {
                		String field = equations.get(i).getFields().get(0);
                        ArrayList<String> variables = equations.get(i).getTerms();
                        ArrayList<Derivative> derivativeSources = new ArrayList<Derivative>();
                        if (fDerLevels.size() > level) {
                        	derivativeSources = fDerLevels.get(level).getFilteredDerivativesSources(variables);
                        }
                        else {
                        	if (fDerLevels.size() > 0) {
                        		derivativeSources = fDerLevels.get(fDerLevels.size() - 1).getFilteredDerivativesSources(variables);
                        	}
                        }
                        //Check for algebraic auxiliary variables
                        boolean noMultiplicationLevel = true;
	                    ArrayList<MultiplicationTerm> multiplication = new ArrayList<MultiplicationTerm>();
                        if (fMultLevels.size() > level) {
                        	multiplication = fMultLevels.get(level).getFilteredMultiplicationTerm(field);
                        }
                        else {
                        	if (fMultLevels.size() > 0) {
                        		multiplication = fMultLevels.get(fMultLevels.size() - 1).getFilteredMultiplicationTerm(field);
                        	}
                        }
                     	if (multiplication.size() > 0) {
                    		noMultiplicationLevel = false;
                    	}
                        boolean simpleAuxiliaryVariable = noMultiplicationLevel && variables.size() == 1 && derivativeSources.size() == 1 && derivativeSources.get(0).getSpatialOperatorDiscretization() == null;
                        if (!simpleAuxiliaryVariable) {
    	                    //Deploy sources
    	                    for (int j = 0; j < derivativeSources.size(); j++) {
    	                        Element algebraic = (Element) derivativeSources.get(j).getAlgebraicExpression().cloneNode(true);
    	                        Element math = AGDMUtils.assignLocalVariableTemplate(derivativeSources.get(j).getId(), variable, algebraic, pi);
    	                        math = (Element) applyConditionalIfExists(math, derivativeSources.get(j).getConditional());
    	                        result.appendChild(doc.importNode(math, true));
    	                        auxTmpVarsWithTemplateVarMesh.add(derivativeSources.get(j).getId());
    	                    }
    	                    //Deploy multiplication terms
		                    for (int j = 0; j < multiplication.size(); j++) {
		                        Node algebraic = multiplication.get(j).getAlgebraicExpression();
		                        ArrayList<String> multVariables = multiplication.get(j).getVariables();
		                        //Create multiplication expression
		                        int initialVariable = 0;
		                        Element parent;
		                        //Adding all variables and algebraic expression
		                        if (algebraic != null) {
		                            parent = (Element) algebraic.cloneNode(true);
		                            AGDMUtils.replaceFieldVariables(pi, variable, parent);
		                            parent = (Element) parent.getFirstChild();
		                        }
		                        else {
		                            initialVariable = 1;
		                            boolean localVariable = checkLocalDerivativeTerm(doc, multLevels, derLevels, multVariables.get(0), level);
		                            String varTerm = multVariables.get(0);
		    	                    if (removedVars.containsKey(multVariables.get(0))) {
		    	                    	varTerm = removedVars.get(multVariables.get(0));
		    	                    }
		    	                    
		    	                    if (!auxTmpVarsWithTemplateVarMesh.contains(varTerm)) {
		    	                    	parent = getVariableNode(doc, varTerm, fDerLevels, spatialCoords, localVariable);	
		    	                    }
		    	                    else {
		    	                    	parent = getVariableNodeTemplate(doc, varTerm, variable, fDerLevels, spatialCoords, pi, localVariable);	
		    	                    }
		                        }
		                        for (int k = initialVariable; k < multVariables.size(); k++) {
		                        	boolean localVariable = checkLocalDerivativeTerm(doc, multLevels, derLevels, multVariables.get(k), level);
		                        	String varTerm = multVariables.get(k);
		    	                    if (removedVars.containsKey(multVariables.get(0))) {
		    	                    	varTerm = removedVars.get(multVariables.get(0));
		    	                    }
		                            Element apply = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
		                            Element times = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "times");
		                            Element var = null;
		                            if (!auxTmpVarsWithTemplateVarMesh.contains(varTerm)) {
		                            	var = getVariableNode(doc, varTerm, fDerLevels, spatialCoords, localVariable);
		                            }
		                            else {
		                            	var = getVariableNodeTemplate(doc, varTerm, variable, fDerLevels, spatialCoords, pi, localVariable);	
		                            }
		                            
		                            apply.appendChild(times);
		                            apply.appendChild(doc.importNode(parent.cloneNode(true), true));
		                            apply.appendChild(var);
		                            
		                            parent = apply;
		                        }
		                        //Set equality
		                        Element math = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "math");
		                        Element apply = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
		                        Element eq = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "eq");
		                        ProcessInfo newPi = new ProcessInfo(pi);
		                        newPi.setField(multiplication.get(j).getId());
		                        Element var = (Element) ExecutionFlow.replaceTags(AGDMUtils.removeIndex(variable), newPi);
		                        apply.appendChild(eq);
		                        apply.appendChild(doc.importNode(var, true));
		                        apply.appendChild(parent);
		                        math.appendChild(apply);
		                        //Add expression to the return list
		                        math = ExecutionFlow.replaceTags(math, pi);
		                        math = (Element) applyConditionalIfExists(math, multiplication.get(j).getConditional());
		                        
		                        result.appendChild(doc.importNode(math, true));
		                        auxTmpVarsWithTemplateVarMesh.add(multiplication.get(j).getId());
		                    }
    	                  //Deploy equation summation
    	                    boolean localVariable = checkLocalMultiplicationTerm(filteredOpInfo, variables.get(0));
    	                    String varTerm = variables.get(0);
    	                    if (removedVars.containsKey(variables.get(0))) {
    	                    	varTerm = removedVars.get(variables.get(0));
    	                    }
    	                    Element parent = null;
    	                    if (!auxTmpVarsWithTemplateVarMesh.contains(varTerm)) {
    	                    	parent = getVariableNode(doc, varTerm, fDerLevels, spatialCoords, localVariable);
    	                    }
    	                    else {
    	                    	parent = getVariableNodeTemplate(doc, varTerm, variable, fDerLevels, spatialCoords, pi, localVariable);
    	                    }
    	                    
    	                    for (int j = 1; j < variables.size(); j++) {
    	                        varTerm = variables.get(j);
    		                    if (removedVars.containsKey(variables.get(0))) {
    		                    	varTerm = removedVars.get(variables.get(0));
    		                    }
    	                        Element apply = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
    	                        Element plus = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "plus");
    	                        localVariable = checkLocalMultiplicationTerm(filteredOpInfo, variables.get(j));
    	                        Element var = null;
    	                        if (!auxTmpVarsWithTemplateVarMesh.contains(varTerm)) {
                                	var = getVariableNode(doc, varTerm, fDerLevels, spatialCoords, localVariable);
                                }
                                else {
                                	var = getVariableNodeTemplate(doc, varTerm, variable, fDerLevels, spatialCoords, pi, localVariable);
                                }
    	                        apply.appendChild(plus);
    	                        apply.appendChild(doc.importNode(parent.cloneNode(true), true));
    	                        apply.appendChild(doc.importNode(var, true));
    	                        
    	                        parent = apply;
    	                    }
    	                    //Set equality
    	                    Element math = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "math");
    	                    Element apply = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
    	                    Element eq = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "eq");
    	                    Element varElement = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
    	                    varElement.setTextContent(field);
    	                    apply.appendChild(eq);
    	                    apply.appendChild(varElement);
    	                    apply.appendChild(doc.importNode(parent, true));
    	                    math.appendChild(apply);
    	                    
    	                    math = ExecutionFlow.replaceTags(math, pi);
    	                    math = (Element) applyConditionalIfExistsDef0(math, equations.get(i).getConditional(), equations.get(i).getFields(), variable);
    	                    AGDMUtils.replaceFieldVariables(pi, (Element) variable.cloneNode(true), math);
    	                    result.appendChild(doc.importNode(math, true));
    	          
                        }
                        //Only one term in equation. Simplify
                        else {
                        	Element equation = null;
                    		equation = (Element) derivativeSources.get(0).getAlgebraicExpression().cloneNode(true);
                    		equation = (Element) equation.getFirstChild();

                        	//Deploy equation summation
                            Element math = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "math");
                            Element apply = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
                            Element eq = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "eq");
                            Element varElement = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
                            varElement.setTextContent(field);
                            apply.appendChild(eq);
                            apply.appendChild(varElement);
                            apply.appendChild(doc.importNode(equation, true));
                            math.appendChild(apply);
                            
                            math = ExecutionFlow.replaceTags(math, pi);
    	                    math = (Element) applyConditionalIfExistsDef0(math, equations.get(i).getConditional(), equations.get(i).getFields(), variable);
    	                    AGDMUtils.replaceFieldVariables(pi, (Element) variable.cloneNode(true), math);
                            result.appendChild(doc.importNode(math, true));
                            removedVars.put(derivativeSources.get(0).getId(), field);
                        }
                	}
                	if (equations.get(i).getEquationType() == EquationType.algorithmic) {
                        Element simml = (Element) equations.get(i).getAlgorithm().cloneNode(true);
                        
    	                DocumentFragment simmlInstr = simml.getOwnerDocument().createDocumentFragment();
    	                for (int l = 0; l < simml.getChildNodes().getLength(); l++) {
    	                	simmlInstr.appendChild(simml.getChildNodes().item(l).cloneNode(true));
    	                }
    	                Node tmp = applyConditionalIfExistsDef0(simmlInstr, equations.get(i).getConditional(), equations.get(i).getFields(), variable);
    	                simml = (Element) equations.get(i).getAlgorithm().cloneNode(false);
    	                simml.appendChild(tmp);
    	                //Create the equation with the time slice
    	                AGDMUtils.replaceFieldVariables(pi, (Element) variable.cloneNode(true), simml);
    	            	result.appendChild(ExecutionFlow.processSimml(simml, discProblem, problem, pi, true));
                	}
                }
            }
        }
        return result;
    }    
    
    /**
     * Create the instructions to solve the temporal derivative.
     * @param doc					Current document
     * @param opInfo                The operator information
     * @param pi                    The process information
     * @param discProblem           The problem being discretized
     * @param problem               The continuous problem
     * @param step                  The current time step
     * @return                      The instructions
     * @throws AGDMException        AGDM00X - External error
     */
    private DocumentFragment solveTime(Document doc, OperatorsInfo opInfo, ProcessInfo pi, Document discProblem, 
            Node problem, Element step) throws AGDMException {
        DocumentFragment result = doc.createDocumentFragment();
        
        //Load schema
        String schemaId = step.getElementsByTagNameNS(AGDMUtils.mmsUri, "explicit").item(0).getFirstChild().getLastChild().getTextContent();
        String functionNamePrefix = loadSchema(doc, schemaId, discProblem, problem, pi, step.getElementsByTagNameNS(AGDMUtils.mmsUri, "explicit").item(0).getFirstChild().getFirstChild().getTextContent());
        String functionNamePrefixExplicit = null;
        ArrayList<Element> inputVariableListImplicit = null;
        if (opInfo.getStiffSourceTerms().getStiffSources().size() > 0) {
        	NodeList implicitCall = step.getElementsByTagNameNS(AGDMUtils.mmsUri, "implicit");
        	if (implicitCall.item(0).getFirstChild().getLocalName().equals("transformationRule")) {
        		String schemaIdImplicit = implicitCall.item(0).getFirstChild().getLastChild().getTextContent();
        		functionNamePrefixExplicit = loadSchema(doc, schemaIdImplicit, discProblem, problem, pi, implicitCall.item(0).getFirstChild().getFirstChild().getTextContent());
        		if (!implicitCall.item(0).getFirstChild().getNextSibling().getLocalName().equals("inputVariables")) {
        			throw new AGDMException(AGDMException.AGDM002, "Invalid time discretization. Implicit transformation rule must have input variables.");
        		}
            	inputVariableListImplicit = getImplicitTimeInputVariables(step);
        	}
        }
        ArrayList<Element> inputVariableListExplicit = getExplicitTimeInputVariables(step);        
        
        //Create time discretization calls
        ArrayList<String> equations = opInfo.getEvolutionFields();
        for (int i = 0; i < equations.size(); i++) {
            String field = equations.get(i);
            ProcessInfo newPi = new ProcessInfo(pi);
            newPi.setField(field);
            
            Element outputVariable = (Element) timeOutputVariable.cloneNode(true);
            outputVariable = ExecutionFlow.replaceTags(outputVariable, newPi);
            if (opInfo.getStiffSourceTerms().hasStiffSourcesForField(field)) {
	            if (outputVariable.getLocalName().equals("sharedVariable")) {
	            	outputVariable = (Element) outputVariable.getFirstChild();
	            }
	        	AGDMUtils.addPrefix(outputVariable, "aux_");
            }
            //Create mathml call
            Element math = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "math");
            Element apply = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
            Element eq = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "eq");
            apply.appendChild(eq);
            //LHS
            apply.appendChild(doc.importNode(outputVariable, true));
            
            //RHS
            Element functionCall = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "functionCall");
            Element functionName = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
            functionName.setTextContent(functionNamePrefix);
            functionCall.appendChild(functionName);
            for (int j = 0; j < inputVariableListExplicit.size(); j++) {
                functionCall.appendChild(doc.importNode(inputVariableListExplicit.get(j).cloneNode(true), true));
            }
            //Implicit and explicit
            if (opInfo.getStiffSourceTerms().hasStiffSourcesForField(field) && functionNamePrefixExplicit != null) {
                Element functionCallImplicit = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "functionCall");
                Element functionNameImplicit = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
                functionNameImplicit.setTextContent(functionNamePrefixExplicit);
                functionCallImplicit.appendChild(functionNameImplicit);
                for (int j = 0; j < inputVariableListImplicit.size(); j++) {
                	functionCallImplicit.appendChild(doc.importNode(inputVariableListImplicit.get(j).cloneNode(true), true));
                }
            	
            	Element applyImEx = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
            	Element plus = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "plus");
            	applyImEx.appendChild(plus);
            	applyImEx.appendChild(doc.importNode(ExecutionFlow.replaceTags(functionCall, newPi), true));
            	applyImEx.appendChild(doc.importNode(ExecutionFlow.replaceTags(functionCallImplicit, newPi), true));
            	apply.appendChild(applyImEx);
            }
            //Only explicit
            else {
            	apply.appendChild(doc.importNode(ExecutionFlow.replaceTags(functionCall, newPi), true));	
            }
            math.appendChild(apply);
            Node conditional = null;
            if (opInfo.getEquationTermsLevel().getEquationsForField(field) != null) {
            	conditional = opInfo.getEquationTermsLevel().getEquationsForField(field).getConditional();
            }
            else {
            	if (!opInfo.getFluxTerms().getFilteredFluxTerms(field).isEmpty()) {
            		conditional = opInfo.getFluxTerms().getFilteredFluxTerms(field).get(0).getConditional();
            	}
            	else {
            		if (opInfo.getSourceTerms().getSources(field) != null) {
            			conditional = opInfo.getSourceTerms().getSources(field).getConditional();
            		}
            	}
            }
            math = (Element) applyConditionalIfExistsWithElse(math, conditional, outputVariable.cloneNode(true), step);
            math = ExecutionFlow.replaceTags(math, newPi);
            result.appendChild(math);
        }
        
        return result;   
    }
    

    /**
     * Calculate RHS for stiffness terms
     * @param doc					Current document
     * @param opInfo                The operator information
     * @param pi                    The process information
     * @param step                  The current time step
     * @return                      The instructions
     * @throws AGDMException        AGDM00X - External error
     */
    private DocumentFragment stiffRHS(Document doc, OperatorsInfo opInfo, ProcessInfo pi, Element step, Element variableTemplate) throws AGDMException {
        DocumentFragment result = doc.createDocumentFragment();
        
    	Element timeOutputVar = (Element) variableTemplate.cloneNode(true);
    
        //Create store stiff rhs
        Node stiffrhsVariable = getStiffRHSVariable(step);
        if (stiffrhsVariable != null) {
	        ArrayList<StiffSource> stiffSources = opInfo.getStiffSourceTerms().getStiffSources();
	        for (int i = 0; i < stiffSources.size(); i++) {
	            String field = stiffSources.get(i).getField();
	            ProcessInfo newPi = new ProcessInfo(pi);
	            newPi.setField(field);
	            Node outputVariable = stiffrhsVariable.cloneNode(true);
	            //Create mathml call
	            Element math = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "math");
	            Element apply = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
	            Element eq = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "eq");
	            apply.appendChild(eq);
	            //LHS
	            Element lhs = (Element) doc.importNode(ExecutionFlow.replaceTags(outputVariable, newPi), true);
	            apply.appendChild(lhs);
	            
	            //RHS
	            Element stiffSource = (Element) stiffSources.get(i).getFormula().cloneNode(true);
	            AGDMUtils.replaceFieldVariables(pi, (Element) timeOutputVar.cloneNode(true), stiffSource);
	            apply.appendChild(doc.importNode(stiffSource.getFirstChild(), true));
	            math.appendChild(apply);
	            math = (Element) applyConditionalIfExists(math, stiffSources.get(i).getConditional());
	            result.appendChild(math);
	        }
        }
        
        return result;   
    }
    
    /**
     * Calculate RHS for stiffness terms
     * @param doc					Current document
     * @param opInfo                The operator information
     * @param pi                    The process information
     * @param step                  The current time step
     * @return                      The instructions
     * @throws AGDMException        AGDM00X - External error
     */
    private DocumentFragment solveStiff(Document doc, OperatorsInfo opInfo, ProcessInfo pi, Element step, Element variableTemplate) throws AGDMException {
        DocumentFragment result = doc.createDocumentFragment();
        
        //Create stiff solver
        Element stiffSolver = (Element) opInfo.getStiffSolver();
        if (stiffSolver != null) {
        	stiffSolver = (Element) stiffSolver.cloneNode(true);
        	Node coefficient = getStiffCoefficient(step);
        	replaceStiffExplicitVariable(pi, timeOutputVariable, stiffSolver);
        	replaceStiffCoefficient(pi, coefficient, stiffSolver);
            AGDMUtils.replaceFieldVariables(pi, (Element) timeOutputVariable.cloneNode(true), stiffSolver);
            stiffSolver = ExecutionFlow.replaceTags(stiffSolver, pi);
            NodeList stiffSolverInstructions = stiffSolver.getChildNodes();
            for (int i = 0; i < stiffSolverInstructions.getLength(); i++) {
            	result.appendChild(stiffSolverInstructions.item(i));
            }
        }
        
        return result;   
    }
    
    
    /**
     * Creates the conservative discretization calls.
     * @param doc					Current document
     * @param opInfo                The operator information
     * @param pi                    The process information
     * @param discProblem           The problem being discretized
     * @param problem               The continuous problem
     * @param step                  The current time step
     * @param level					Current level number
     * @param fluxesInRHS			If the fluxes are places in the RHS loop
     * @return                      The instructions
     * @throws AGDMException        AGDM00X - External error
     */
    public DocumentFragment createConservativeDiscretization(Document doc, OperatorsInfo opInfo, ProcessInfo pi, Document discProblem, 
            Node problem, Element step, int level, boolean fluxesInRHS) throws AGDMException {
        DocumentFragment result = doc.createDocumentFragment();
        
        String query;
        if (pi.getDiscretizationType() == DiscretizationType.mesh) {
        	query = ".//mms:conservativeTermDiscretization/mms:meshDiscretization";
        }
        else {
        	query = ".//mms:conservativeTermDiscretization/mms:particleDiscretization/mms:derivativeFunction";
        }
        
        //Load schema
        NodeList discretization = AGDMUtils.find(step, query);
        if (discretization.getLength() > 0) {
	        String schemaId = discretization.item(0).getFirstChild().getLastChild().getTextContent();
	        String functionNamePrefix = loadSchema(doc, schemaId, discProblem, problem, pi, discretization.item(0).getFirstChild().getFirstChild().getTextContent());
	        
	        ArrayList<Element> inputVariableList = getConservativeDiscretizationInputVariables(step, pi.getDiscretizationType());
	        Node outputVariable = getSpatialOutputVariable(step);
	        ArrayList<Flux> fluxes = opInfo.getFluxTerms().getFluxes();
	        ArrayList<String> rhsCreated = new ArrayList<String>();
	        
	        for (int i = 0; i < fluxes.size(); i++) {
	        	String field = fluxes.get(i).getField();
	        	String coord = fluxes.get(i).getCoordinate();
	            ProcessInfo newPi = new ProcessInfo(pi);
	            newPi.setField(field);
	            newPi.setSpatialCoord1(coord);
	            newPi.setSpatialCoord2(null);
	            //Create mathml call
	            Element math = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "math");
	            Element apply = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
	            Element eq = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "eq");
	            apply.appendChild(eq);
	            //LHS
	            Element lhs = ExecutionFlow.replaceTags(outputVariable.cloneNode(true), newPi);
	            if (opInfo.getType() == DiscretizationType.particles) {
	            	AGDMUtils.addSuffix(lhs, "_Cons_" + coord);
	            }
	            apply.appendChild(doc.importNode(lhs, true));
	            //RHS
	            Element functionCall = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "functionCall");
	            Element functionName = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
	            Integer order = functionOrder.get(functionNamePrefix);
	            if (order > 0) {
	            	functionName.setTextContent(functionNamePrefix + pi.getCoordinateRelation().getContToDisc(coord));
	            }
	            else {
	            	functionName.setTextContent(functionNamePrefix);
	            }
	            functionCall.appendChild(functionName);
	            for (int j = 0; j < inputVariableList.size(); j++) {
	            	//If the flux is simple, use it directly instead of its output variable
	            	Node inputVar = getFluxInputVariable(inputVariableList.get(j).getFirstChild().cloneNode(true), step, pi.getDiscretizationType());
	            	if (inputVar != null && (fluxes.get(i).isSimple())) {
	             	  	boolean isStepVariable = pi.isStepVariable(inputVar, false);
	             	  	String varToUse = fluxes.get(i).getMath().getFirstChild().getTextContent();
	             	  	//Field
	             	  	if (!AGDMUtils.isNumber(varToUse)) {
		             	  	if (pi.getRegionInfo().isAuxiliaryField(varToUse) && isStepVariable) {
		             	  		Element tmp = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
		             	  		tmp.setTextContent(varToUse);
			             		if (pi.getDiscretizationType() == DiscretizationType.particles) {
			             			tmp = AGDMUtils.addCurrentCell(doc, tmp);
			             		}
			             		if (inputVar.getLocalName().equals("neighbourParticle")) {
				             		Element neighbourParticle = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "neighbourParticle");
				             		neighbourParticle.appendChild(tmp);
				             		tmp = neighbourParticle;
			             		}
		             	  		functionCall.appendChild(doc.importNode(ExecutionFlow.replaceTags(tmp.cloneNode(true), newPi), true));
		             	  	}
		             	  	else {
			             	  	newPi.setField(varToUse);
			               		Element tmp = (Element) inputVar.cloneNode(true);
			               		if (inputVar.getLocalName().equals("neighbourParticle")) {
			               			tmp = (Element) tmp.getFirstChild();
			               		}
			             		if (pi.getDiscretizationType() == DiscretizationType.particles) {
			             			tmp = AGDMUtils.addCurrentCell(doc, tmp);
			             		}
			             		if (inputVar.getLocalName().equals("neighbourParticle")) {
				             		Element neighbourParticle = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "neighbourParticle");
				             		neighbourParticle.appendChild(tmp);
				             		tmp = neighbourParticle;
			             		}
		             	  		functionCall.appendChild(doc.importNode(ExecutionFlow.replaceTags(tmp.cloneNode(true), newPi), true));
		             	  	}
	             	  	}
	             	  	//Number
	             	  	else {
	             	  		Element number = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "cn");
	             	  		number.setTextContent(varToUse);
	             	  		functionCall.appendChild(number);
	             	  	}
	             	}
	             	//If the flux is in the same loop and the input variables for conservative discretization is a output variable in the fluxes
	             	//use flux output variable as local
	             	else if (inputVar != null && fluxesInRHS) {
	             		newPi.setField(field);
	             		Element input = (Element) inputVariableList.get(j).cloneNode(true).getFirstChild();
	             		if (input.getLocalName().equals("apply")) {
	             			input = (Element) input.getFirstChild();
	             		}
	             		
	             		functionCall.appendChild(doc.importNode(ExecutionFlow.replaceTags(input, newPi), true));
             		}
	             	//Otherwise use flux output variables stored in mesh
	             	else {
	             		newPi.setField(field);	             	  	
	             		NodeList params = ExecutionFlow.replaceTags(inputVariableList.get(j).cloneNode(true), newPi).getChildNodes();
	             		for (int k = 0; k < params.getLength(); k++) {
	             			functionCall.appendChild(doc.importNode(params.item(k).cloneNode(true), true));
	             		}
	             	}
	            }
	            //If there is not non-conservative discretization or sources, the output variable must be initialized instead of accumulated
	            //In particles, accumulation is needed. So initialization mus be done always previously.
	            if (pi.getDiscretizationType() == DiscretizationType.mesh && !rhsCreated.contains(field) && !opInfo.getSourceTerms().hasSourcesForField(field)) {
	            	Element applyAcc = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
	            	Element minus = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "minus");
	             	applyAcc.appendChild(minus);
	             	applyAcc.appendChild(functionCall);
	             	apply.appendChild(applyAcc);
	            }
	            //Otherwise accumulate the discretizations
	            else {
	            	Element applyAcc = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
	            	Element minus = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "minus");
	            	applyAcc.appendChild(minus);
	            	applyAcc.appendChild(doc.importNode(lhs.cloneNode(true), true));
	            	applyAcc.appendChild(functionCall);
	            	apply.appendChild(applyAcc);
	            }
	            math.appendChild(apply);
	            math = (Element) applyConditionalIfExists(math, fluxes.get(i).getConditional());
	            result.appendChild(math);
	            rhsCreated.add(field);
	        }
        }
        return result;
    }
    
    /**
     * Creates the non-conservative source terms for particles. There must be used the particle interpolation function.
     * @param doc				Document
     * @param opInfo			Operators info
     * @param pi				Problem information
     * @param discProblem		Discrete problem
     * @param problem			Continuous problem
     * @param step				Current step
     * @return					Source calls
     * @throws AGDMException	AGDM00X - External error
     */
    private DocumentFragment createConservativeSourcesParticles(Document doc, OperatorsInfo opInfo, ProcessInfo pi, Document discProblem, 
            Node problem, Element step) throws AGDMException {
        DocumentFragment result = doc.createDocumentFragment();
    	Node outputVariable = getSpatialOutputVariable(step);  
  
        //Load schema
        NodeList discretization = step.getElementsByTagName("mms:particleInterpolationFunction");
        if (discretization.getLength() > 0) {
	        String schemaId = discretization.item(0).getFirstChild().getLastChild().getTextContent();
	        String functionNamePrefix = loadSchema(doc, schemaId, discProblem, problem, pi, discretization.item(0).getFirstChild().getFirstChild().getTextContent());
	        ArrayList<Element> inputVariableList = getParticleInterpolationFunctionInputVariables(step);

	        ArrayList<Source> sources = opInfo.getSourceTerms().getSources();
	        for (int i = 0; i < sources.size(); i++) {
	        	Element sourcesFormula = sources.get(i).getFormula();
	        	if (!sourcesFormula.getFirstChild().getLocalName().equals("cn") || !sourcesFormula.getFirstChild().getTextContent().equals("0")) {
		        	String field = sources.get(i).getField();
		            ProcessInfo newPi = new ProcessInfo(pi);
		            newPi.setField(field);
		            
		            //Create mathml call
		            Element math = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "math");
		            Element apply = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
		            Element eq = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "eq");
		            apply.appendChild(eq);
		            //LHS
		            Element lhs = ExecutionFlow.replaceTags(outputVariable.cloneNode(true), newPi);
                	AGDMUtils.addSuffix(lhs, "_sources");
		            apply.appendChild(doc.importNode(lhs, true));
		            
		            //RHS
		            Element functionCall = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "functionCall");
		            Element functionName = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
		            functionName.setTextContent(functionNamePrefix);
		            functionCall.appendChild(functionName);
		            for (int j = 0; j < inputVariableList.size(); j++) {
		            	//Replace field by the source formula
		            	if (pi.isStepVariable(inputVariableList.get(j).getFirstChild(), false)) {
		            		Element input = (Element) sources.get(i).getFormula().cloneNode(true);
		            		AGDMUtils.replaceFieldVariables(pi, (Element) inputVariableList.get(j).getFirstChild().cloneNode(true), input);
	             			functionCall.appendChild(doc.importNode(input.getFirstChild(), true));
		            	}
		            	else {
		             		NodeList params = ExecutionFlow.replaceTags(inputVariableList.get(j).cloneNode(true), pi).getChildNodes();
		             		for (int k = 0; k < params.getLength(); k++) {
		             			functionCall.appendChild(doc.importNode(params.item(k).cloneNode(true), true));
		             		}
		            	}
		            }
	            	Element applyAcc = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
	            	Element plus = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "plus");
	            	applyAcc.appendChild(plus);
	            	applyAcc.appendChild(doc.importNode(lhs.cloneNode(true), true));
	            	applyAcc.appendChild(functionCall);
	            	apply.appendChild(applyAcc);
		            math.appendChild(apply);
		            math = (Element) applyConditionalIfExists(math, sources.get(i).getConditional());
		            result.appendChild(math);
	        	}
	        }
        }
        else {
			throw new AGDMException(AGDMException.AGDM006, "Particle interpolation function must be defined in the schema if the model have source terms.");
        }
        return result;
    }
    
    /**
     * Creates the conservative source terms for particles. There must be used the particle interpolation function.
     * @param doc				Document
     * @param derLevel			Derivatives
     * @param pi				Problem information
     * @param discProblem		Discrete problem
     * @param problem			Continuous problem
     * @param inputVariable		Variable template
     * @param opType			Operator type
     * @return					Source calls
     * @throws AGDMException	AGDM00X - External error
     */
    private DocumentFragment createNonConsSourcesParticles(Document doc, DerivativeLevel derLevel, ProcessInfo pi, Node inputVariable, Node timeOutputVariable, OperatorInfoType opType) throws AGDMException {
        DocumentFragment result = doc.createDocumentFragment();
    	
        //Process algebraic expressions inside sources
        ArrayList<Derivative> derivatives = derLevel.getDerivatives();
        for (int i = 0; i < derivatives.size(); i++) {
            if (derivatives.get(i).getOperatorType().equals(opType)) {
            	pi.setField(derivatives.get(i).getField());
                Node algebraic = derivatives.get(i).getAlgebraicExpression();
                Optional<Node> derivative = derivatives.get(i).getDerivativeTerm();
                //Sources term
                if(!derivative.isPresent()) {
                //if (!derivative.isPresent() && derivatives.get(i).getOperatorType() != OperatorInfoType.auxiliaryField) {
                  //Create mathml call
    	            Element math = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "math");
    	            Element apply = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
    	            Element eq = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "eq");
    	            apply.appendChild(eq);
    	            //LHS
    	            Element lhs = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
    	            lhs.setTextContent(derivatives.get(i).getId());
		            apply.appendChild(doc.importNode(lhs, true));
    	            //RHS
            		Element input = (Element) algebraic.cloneNode(true);
            		AGDMUtils.replaceFieldVariables(pi, (Element) inputVariable.cloneNode(true), input, timeOutputVariable.cloneNode(true));
	            	apply.appendChild(input.getFirstChild());
    	            math.appendChild(apply);
    	            math = (Element) applyConditionalIfExists(math, derivatives.get(i).getConditional());
    	            result.appendChild(math);
                }
            }
        }
        return result;
    }
        
    /**
     * Creates the sources from conservative formula.
     * @param doc					Current document
     * @param opInfo                The operator information
     * @param pi                    The process information
     * @param inputVariable     	Input template variable
     * @param step                  The current time step
     * @return                      The instructions
     * @throws AGDMException        AGDM00X - External error
     */
    public static DocumentFragment createConservativeSourcesMesh(Document doc, OperatorsInfo opInfo, ProcessInfo pi, 
            Node inputVariable, Element step) throws AGDMException {
    	DocumentFragment result = doc.createDocumentFragment();
    	Node outputVariable = getSpatialOutputVariable(step);  
      
        ArrayList<Source> sources = opInfo.getSourceTerms().getSources();
        for (int i = 0; i < sources.size(); i++) {
        	String field = sources.get(i).getField();
            ProcessInfo newPi = new ProcessInfo(pi);
            newPi.setField(field);
            
            //Create mathml call
            Element math = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "math");
            Element apply = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
            Element eq = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "eq");
            apply.appendChild(eq);
            //LHS
            Element lhs = ExecutionFlow.replaceTags(outputVariable.cloneNode(true), newPi);
            apply.appendChild(doc.importNode(lhs, true));
            //RHS
            Element rhs = (Element) sources.get(i).getFormula().cloneNode(true);
            AGDMUtils.replaceFieldVariables(pi, (Element) inputVariable.cloneNode(true), rhs);
            rhs = (Element) rhs.getFirstChild();
            apply.appendChild(rhs.cloneNode(true));
            
            math.appendChild(apply);
            math = (Element) applyConditionalIfExists(math, sources.get(i).getConditional());
            result.appendChild(math);
        }    
        return result;
    }
    
    /**
     * Creates the suffix for function calling depending on the derivative coordinates.
     * Avoids repetition of coordinates.
     * @param coords    The coordinates
     * @param coordInfo The coordinate information
     * @return          The elements concatenated
     */
    public static String getCoordinateSuffix(ArrayList<String> coords, CoordinateInfo coordInfo) {
        String suffix = "";
        String added = "";
        for (String s : coords) {
            if (!added.contains(s)) {
                suffix += coordInfo.getContToDisc(s);
                added += s;
            }
        }
        return suffix;
    }
    
    /**
     * Get the stencil for the derivative level spatial discretization.
     * @param derivatives   The derivatives in the level
     * @return              The maximum stencil
     */
    private static int getLevelStencil(ArrayList<Derivative> derivatives) {
        int maxLevelStencil = 0;
        for (int i = 0; i < derivatives.size(); i++) {
            if (derivatives.get(i).getSpatialOperatorDiscretization() != null) {
                int stencil = derivatives.get(i).getSpatialOperatorDiscretization().getStencil();
                maxLevelStencil = Math.max(maxLevelStencil, stencil);
            }
        }
        return maxLevelStencil;
    }
    
    /**
     * Return the mathML node for the requested variable. 
     * If the variable is a derivative term that comes from previous levels, the variable is a mesh variable and must be accessed with a vector.
     * @param doc						Current document
     * @param var                       The variable to generate from
     * @param derivativeLevels          The derivative terms information
     * @param spatialCoords             The spatial coordinates
     * @param localVar					If the variable is local
     * @return                          The mathML element
     */
    static private Element getVariableNode(Document doc, String var, ArrayList<DerivativeLevel> derivativeLevels, ArrayList<String> spatialCoords, 
    		boolean localVar) {
        //Check only derivative variables
        if (var.startsWith("i_d")) {
            //Check all levels but last one
            for (int l = 0; l < derivativeLevels.size() - 1; l++) {
                ArrayList<Derivative> derivatives = derivativeLevels.get(l).getDerivatives();
                for (int i = 0; i < derivatives.size(); i++) {
                    if (derivatives.get(i).getId().equals(var)) {
                        Element varElement = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
                        Element ci = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
                        ci.setTextContent(var);
                        varElement.appendChild(ci);
                        Element cells = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "currentCell");
                        varElement.appendChild(cells);
                        return varElement;
                    }
                }
            }
        }
        Element varElement = null;
        if (localVar) {
            varElement = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
            varElement.setTextContent(var);
        }
        else {
        	varElement = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
            Element ci = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
            ci.setTextContent(var);
            varElement.appendChild(ci);
            Element cells = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "currentCell");
            varElement.appendChild(cells);
        }
   
        return varElement;
    }
    
    /**
     * Return the mathML node for the requested variable. 
     * If the variable is a derivative term that comes from previous levels, the variable is a mesh variable and must be accessed with a vector.
     * @param doc						Current document
     * @param var                       The variable to generate from
     * @param variableTemplate			The variable template
     * @param derivativeLevels          The derivative terms information
     * @param spatialCoords             The spatial coordinates
     * @param pi						The process information
     * @param localVar					If the variable is local
     * @return                          The mathML element
     * @throws AGDMException 
     */
    static private Element getVariableNodeTemplate(Document doc, String var, Element variableTemplate, ArrayList<DerivativeLevel> derivativeLevels, ArrayList<String> spatialCoords, 
    		ProcessInfo pi, boolean localVar) throws AGDMException {
        //Check only derivative variables
        if (var.startsWith("i_d")) {
            //Check all levels but last one
            for (int l = 0; l < derivativeLevels.size() - 1; l++) {
                ArrayList<Derivative> derivatives = derivativeLevels.get(l).getDerivatives();
                for (int i = 0; i < derivatives.size(); i++) {
                    if (derivatives.get(i).getId().equals(var)) {
                        Element varElement = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
                        ProcessInfo newPi = new ProcessInfo(pi);
                        newPi.setField(var);
                        Element ci = (Element) ExecutionFlow.replaceTags(variableTemplate.cloneNode(true), newPi);
                        varElement.appendChild(ci);
                        Element cells = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "currentCell");
                        varElement.appendChild(cells);
                        return varElement;
                    }
                }
            }
        }
        Element varElement = null;
        if (localVar) {
            ProcessInfo newPi = new ProcessInfo(pi);
            newPi.setField(var);
            varElement = (Element) ExecutionFlow.replaceTags(AGDMUtils.removeIndex(variableTemplate), newPi);
        }
        else {
        	varElement = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
            ProcessInfo newPi = new ProcessInfo(pi);
            newPi.setField(var);
            Element ci = (Element) ExecutionFlow.replaceTags(AGDMUtils.removeIndex(variableTemplate), newPi);
            varElement.appendChild(ci);
            varElement.appendChild(ci);
            Element cells = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "currentCell");
            varElement.appendChild(cells);
        }
   
        return varElement;
    }
    
    /**
     * Creates the dissipation terms if needed.
     * @param doc					Current document
     * @param evolutionEquations	The evolution equation fields
     * @param pi                	The process information
     * @param variable          	The output variable in which to add the dissipation
     * @param discProblem       	The discrete problem
     * @param problem           	The continuous problem
     * @param step              	The current time step
     * @return                  	Mathematical dissipation summations
     * @throws AGDMException    	AGDM00X - External error
     */
    private DocumentFragment createMeshDissipation(Document doc, ArrayList<String> evolutionEquations, ProcessInfo pi, 
            Element variable, Document discProblem, Node problem, Element step) throws AGDMException {
        DocumentFragment result = doc.createDocumentFragment();
        ArrayList<String> spatialCoords = pi.getCoordinateRelation().getDiscCoords();        
        ArrayList<Element> inputVars = getMeshDissipationInputVariables(step);
        
        //Check if dissipation is set
        NodeList dissipation = step.getElementsByTagNameNS(AGDMUtils.mmsUri, "meshDissipation");
        if (dissipation.getLength() > 0) {
            String dissId = dissipation.item(0).getFirstChild().getLastChild().getTextContent();
            String functionNamePrefix = loadSchema(doc, dissId, discProblem, problem, pi, "meshDissipation");
                        
            //Apply dissipation over every evolution equation
            for (int i = 0; i < evolutionEquations.size(); i++) {
                String field = evolutionEquations.get(i);
                ProcessInfo newPi = new ProcessInfo(pi);
                newPi.setField(field);
                
                //Calculate summatory of dissipation terms
                Element dissipationSumm = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
                Element plus = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "plus");
                dissipationSumm.appendChild(plus);
                
                //Create input variables
                DocumentFragment inputVariables = doc.createDocumentFragment();
                for (int k = 0; k < inputVars.size(); k++) {
                	 Node inputVar = doc.importNode(ExecutionFlow.replaceTags(inputVars.get(k).cloneNode(true), newPi), true);
                	 inputVariables.appendChild(inputVar);
                }
                
                for (int j = 0; j < spatialCoords.size(); j++) {
                    //Function call
                    Element functionCall = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "functionCall");
                    Element functionName = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
                    functionName.setTextContent(functionNamePrefix + spatialCoords.get(j));
                    functionCall.appendChild(functionName);
                    functionCall.appendChild(inputVariables.cloneNode(true));
                    //Adding coordinates
                    Element cells = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "currentCell");
                    functionCall.appendChild(cells);
                    dissipationSumm.appendChild(functionCall);
                }
                
                //Add dissipation to the RHS
                Element ifElem = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "if");
                Element cond = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "math");
                Element applyCond = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
                Element gtCond = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "gt");
                Element factor = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
                factor.setTextContent("dissipation_factor_" + field);
                Element zero = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "cn");
                zero.setTextContent("0");
                cond.appendChild(applyCond);
                applyCond.appendChild(gtCond);
                applyCond.appendChild(factor);
                applyCond.appendChild(zero);
                Element then = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "then");
                ifElem.appendChild(cond);
                ifElem.appendChild(then);
                
                Element math = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "math");
                Element apply = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
                Element eq = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "eq");
                Element sumApply = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
                plus = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "plus");
                sumApply.appendChild(plus);
                Node rhs = doc.importNode(ExecutionFlow.replaceTags(variable.cloneNode(true), newPi), true);
                sumApply.appendChild(rhs.cloneNode(true));
                Element factorApply = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
                Element times = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "times");
                factorApply.appendChild(times);
                factorApply.appendChild(factor.cloneNode(true));
                factorApply.appendChild(dissipationSumm);
                sumApply.appendChild(factorApply);
                apply.appendChild(eq);
                apply.appendChild(rhs.cloneNode(true));
                apply.appendChild(sumApply);
                math.appendChild(apply);
                math = ExecutionFlow.replaceTags(math, pi);
                then.appendChild(math);
                result.appendChild(ifElem);
            }
        }
        return result;
    }
    
    /**
     * Creates the dissipation terms if needed.
     * @param doc					Current document
     * @param evolutionEquations	The evolution equation fields
     * @param pi                	The process information
     * @param variable          	The output variable in which to add the dissipation
     * @param discProblem       	The discrete problem
     * @param problem           	The continuous problem
     * @param step              	The current time step
     * @return                  	Mathematical dissipation summations
     * @throws AGDMException    	AGDM00X - External error
     */
    private DocumentFragment createParticleDissipation(Document doc, ArrayList<String> evolutionEquations, ProcessInfo pi, 
            Element variable, Document discProblem, Node problem, Element step) throws AGDMException {
        DocumentFragment result = doc.createDocumentFragment();
        ArrayList<String> spatialCoords = pi.getCoordinateRelation().getDiscCoords();        
        ArrayList<Element> inputVars = getParticleDissipationInputVariables(step);
        
        //Check if dissipation is set
        NodeList dissipation = step.getElementsByTagNameNS(AGDMUtils.mmsUri, "particleDissipation");
        if (dissipation.getLength() > 0) {
            String dissId = dissipation.item(0).getFirstChild().getLastChild().getTextContent();
            String functionNamePrefix = loadSchema(doc, dissId, discProblem, problem, pi, "particleDissipation");
                        
            //Apply dissipation over every evolution equation
            for (int i = 0; i < evolutionEquations.size(); i++) {
                String field = evolutionEquations.get(i);
                ProcessInfo newPi = new ProcessInfo(pi);
                newPi.setField(field);
                
                //Calculate summatory of dissipation terms
                Element dissipationSumm = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
                Element plus = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "plus");
                dissipationSumm.appendChild(plus);
                                
                //Add dissipation to the RHS
                Element ifElem = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "if");
                Element cond = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "math");
                Element applyCond = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
                Element gtCond = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "gt");
                Element factor = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
                factor.setTextContent("dissipation_factor_" + field);
                Element zero = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "cn");
                zero.setTextContent("0");
                cond.appendChild(applyCond);
                applyCond.appendChild(gtCond);
                applyCond.appendChild(factor);
                applyCond.appendChild(zero);
                Element then = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "then");
                ifElem.appendChild(cond);
                ifElem.appendChild(then);
                for (int j = 0; j < spatialCoords.size(); j++) {
                    //Function call
                    Element functionCall = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "functionCall");
                    Element functionName = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
                    functionName.setTextContent(functionNamePrefix);
                    functionCall.appendChild(functionName);
                    //Create input variables
                    newPi.setSpatialCoord1(pi.getBaseSpatialCoordinates().get(j));
                    newPi.setSpatialCoord2(null);
                    for (int k = 0; k < inputVars.size(); k++) {
                    	Node inputVar = doc.importNode(ExecutionFlow.replaceTags(inputVars.get(k).cloneNode(true), newPi), true);
                    	functionCall.appendChild(inputVar);
                    }
                    dissipationSumm.appendChild(functionCall);
                    
                    Element math = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "math");
                    Element apply = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
                    Element eq = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "eq");
                    Element applyAcc = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
                    plus = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "plus");
                    Element rhs = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
                    rhs.setTextContent("RHS_dissipative_term_" + field + "_" + pi.getBaseSpatialCoordinates().get(j));                
                    Element factorApply = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
                    Element times = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "times");
                    factorApply.appendChild(times);
                    factorApply.appendChild(factor.cloneNode(true));
                    factorApply.appendChild(dissipationSumm);
                    applyAcc.appendChild(plus);
                    applyAcc.appendChild(rhs.cloneNode(true));
                    applyAcc.appendChild(functionCall);
                    apply.appendChild(eq);
                    apply.appendChild(rhs.cloneNode(true));
                    apply.appendChild(applyAcc);
                    math.appendChild(apply);
                    math = ExecutionFlow.replaceTags(math, newPi);
                    then.appendChild(math);
                }     
                result.appendChild(ifElem);
            }
        }
        return result;
    }
    
    /**
     * Calculates the stencil from dissipation terms.
     * @param step              The step in which the dissipation is placed
     * @return                  The stencil
     * @throws AGDMException    AGDM00X - External error
     */
    public static int getMeshDissipationStencil(Element step) throws AGDMException {
        //Check if dissipation is set
        NodeList dissipation = step.getElementsByTagNameNS(AGDMUtils.mmsUri, "meshDissipation");
        if (dissipation.getLength() > 0) {
            String dissipationId = dissipation.item(0).getFirstChild().getLastChild().getTextContent();
            String schema;
            try {
                DocumentManager docMan = new DocumentManagerImpl();
                schema = SimflownyUtils.jsonToXML(docMan.getDocument(dissipationId));
            } 
            catch (SUException e) {
                throw new AGDMException(AGDMException.AGDM002, "Mesh dissipation discretization with id " + dissipationId + " not valid.");
            }
            catch (DMException e) {
                throw new AGDMException(AGDMException.AGDM003, "Mesh dissipation discretization with id " + dissipationId + " does not exist.");
            }
            return getSchemaStencil(AGDMUtils.stringToDom(schema));
        }
        return 0;
    }
    
    /**
     * If there are crossed derivatives for the derivative level, insert a tag for corner calculation.
     * @param iterateOverCells  The element in which the tag has to be inserted.
     * @throws AGDMException    AGDM00X - External error
     */
    private void insertCornerCalculation(Element iterateOverCells) throws AGDMException {
        if (crossedDerivatives) {
        	iterateOverCells.setAttribute("cornerCalculationAtt", "true");
        } 
    }
    
    /**
     * Calculates the maximum recursive stencil for a variable.
     * @param derivativeLevels		The derivative levels
     * @param multiplicationLevels	The multiplication levels
     * @param variableId			The variable to check
     * @param level					The current level
     * @param stencils				The stencils
     * @return						Maximum recursive stencil
     * @throws AGDMException 
     */
    private HashMap<String, Integer> calculateRecursiveStencil(ArrayList<DerivativeLevel> derivativeLevels,
    		ArrayList<MultiplicationTermsLevel> multiplicationLevels, String variableId, int level, HashMap<String, Integer> stencils) throws AGDMException {
    	HashMap<String, Integer> maxAccumulatedStencil = new HashMap<String, Integer>(stencils);
    	if (level < derivativeLevels.size()) {
	        ArrayList<Derivative> derivatives = derivativeLevels.get(level).getDerivatives();
	        for (int j = 0; j < derivatives.size(); j++) {
	            Derivative d = derivatives.get(j);
	            if (d.getDerivativeTerm().isPresent()) {
	                //Get variables inside the derivative (both, derivative ...
	                if (AGDMUtils.find(d.getDerivativeTerm().get(), ".//mt:ci[text() = '" + variableId + "']").getLength() > 0) {
	                	int localStencil = derivatives.get(j).getSpatialOperatorDiscretization().getStencil();
	                	String derId = derivatives.get(j).getId();
	                	HashSet<String> coords = new HashSet<String>(derivatives.get(j).getCoordinates());
	                    HashMap<String, Integer> localAccumulatedStencil = addStencil(maxAccumulatedStencil, coords, localStencil);
	                    maxAccumulatedStencil = AGDMUtils.maxHashMapStencil(maxAccumulatedStencil, calculateRecursiveStencil(derivativeLevels, multiplicationLevels, derId, level + 1, localAccumulatedStencil));
	                    //Get recursive stencil for the multiplication terms that use the current derivative variable
	                    for (int k = level; k < multiplicationLevels.size(); k++) {
	                    	ArrayList<MultiplicationTerm> multTerms = multiplicationLevels.get(k).getMultiplicationTerms();
	                    	for (int l = 0; l < multTerms.size(); l++) {
	                    		if (multTerms.get(l).getVariables().contains(derId)) {
	                    			maxAccumulatedStencil = AGDMUtils.maxHashMapStencil(maxAccumulatedStencil, calculateRecursiveStencil(derivativeLevels, multiplicationLevels, multTerms.get(l).getId(), level + 1, localAccumulatedStencil));
	                    		}
	                    	}
	                    }
	                    
	                }
	            }
	        }
    	}
        return maxAccumulatedStencil;
    }
    
    /**
     * Add the stencil to the coordinates that coincide in the coordinate stencil map.
     * @param coordStencil		The stencil to coordinate mapping
     * @param coords			The coordinates to which accumulate stencil
     * @param stencil			The stencil to accumulate
     * @return					The new coordinate mapping
     */
    private HashMap<String, Integer> addStencil(HashMap<String, Integer> coordStencil, HashSet<String> coords, int stencil) {
    	HashMap<String, Integer> result = new HashMap<String, Integer>(coordStencil);
    	Iterator<String> coordIt = coords.iterator();
    	while (coordIt.hasNext()) {
    		String coord = coordIt.next();
    		//The current stencil mapping contains the coordinate
    		if (result.containsKey(coord)) {
    			result.put(coord, result.get(coord) + stencil);
    		}
    		//Otherwise the coordinate must be added
    		else {
    			result.put(coord, stencil);
    		}
    	}
    	return result;
    }
    
    /**
     * Calculates the accumulated stencil for every derivative level.
     * It has to check for recursions in the derivatives. For instance, a Di(Di(u)) must generate accumulated stencil in the internal level. 
     * Di(Dj(u)) does not need extra stencil.
     * Di²(u) does not need extra stencil.
     * It also gets if there are crossed derivatives.
     * @param derivativeLevels  		The derivative level information
     * @param multiplicationLevels		Multiplication level information
     * @param step						Schema step
     * @param particleDerivativeLevels	Number of particle derivative levels
     * @throws AGDMException    		AGDM00X - External error
     */
    private void calculateAccumulatedStencilAndCrossedDerivatives(ArrayList<DerivativeLevel> derivativeLevels,
    		ArrayList<MultiplicationTermsLevel> multiplicationLevels, int particleDerivativeLevels, Element step) throws AGDMException {
        accumulatedStencil = new ArrayList<Integer>();
        crossedDerivatives = false;
        for (int i = 0; i < derivativeLevels.size(); i++) {
        	ArrayList<Derivative> derivatives = derivativeLevels.get(i).getDerivatives();
        	HashMap<String, Integer> maxAccumulatedStencil = new HashMap<String, Integer>();
        	//Calculate for every derivative at current level
            for (int j = 0; j < derivatives.size(); j++) {
            	//Get recursive stencil for the current derivative variable
                String derId = derivatives.get(j).getId();
                if (derivatives.get(j).getSpatialOperatorDiscretization() != null) {
	                int stencil = derivatives.get(j).getSpatialOperatorDiscretization().getStencil();
	                HashSet<String> coords = new HashSet<String>(derivatives.get(j).getCoordinates());
	                HashMap<String, Integer> localAccumulatedStencil = addStencil(new HashMap<String, Integer>(), coords, stencil);
	                //Check in the following levels
	                if (i + 1 < derivativeLevels.size()) {
	                	HashMap<String, Integer> tmpLocalAccumulatedStencil = calculateRecursiveStencil(derivativeLevels, multiplicationLevels, derId, i + 1, localAccumulatedStencil);
		                //Get recursive stencil for the multiplication terms that use the current derivative variable
		                for (int k = i; k < multiplicationLevels.size(); k++) {
		                	ArrayList<MultiplicationTerm> multTerms = multiplicationLevels.get(k).getMultiplicationTerms();
		                	for (int l = 0; l < multTerms.size(); l++) {
		                		if (multTerms.get(l).getVariables().contains(derId)) {
		                			tmpLocalAccumulatedStencil = AGDMUtils.maxHashMapStencil(tmpLocalAccumulatedStencil, calculateRecursiveStencil(derivativeLevels, multiplicationLevels, multTerms.get(l).getId(), i + 1, localAccumulatedStencil));
		                		}
		                	}
		                }
		                localAccumulatedStencil = tmpLocalAccumulatedStencil;
	                }
	                maxAccumulatedStencil = AGDMUtils.maxHashMapStencil(maxAccumulatedStencil, localAccumulatedStencil);
                }
            }
            //Get maximum from current stencil and coordinate accumulated stencil
            if (maxAccumulatedStencil.isEmpty()) {
            	accumulatedStencil.add(0);
            }
            else {
            	accumulatedStencil.add(Collections.max(maxAccumulatedStencil.values()));
            }
        }
        
        //Check if there are crossed derivatives
        for (int i = 0; i < derivativeLevels.size() && !crossedDerivatives; i++) {
            //For every derivative in the level
            ArrayList<Derivative> derivatives = derivativeLevels.get(i).getDerivatives();
            //Calculate for every derivative at current level
            for (int j = 0; j < derivatives.size(); j++) {
            	//Check if a derivative variable was used inside a previous level derivative
                String derId = derivatives.get(j).getId();
                ArrayList<String> coords = derivatives.get(j).getCoordinates();
                if (derivatives.get(j).getSpatialOperatorDiscretization() != null) {
                    //Get multiplication terms using this derivative id. Add the terms also the search for usage.
                    ArrayList<String> multIds = new ArrayList<String>();
                    for (int k = i; k < multiplicationLevels.size(); k++) {
                    	ArrayList<MultiplicationTerm> multTerms = multiplicationLevels.get(k).getMultiplicationTerms();
                    	for (int l = 0; l < multTerms.size(); l++) {
                    		if (multTerms.get(l).getVariables().contains(derId)) {
                    			multIds.add(multTerms.get(l).getId());
                    		}
                    	}
                    }
                    //Search in the following derivative level
                    if (derivativeLevels.size() > i + 1) {
	                    ArrayList<Derivative> nextLevelDerivatives = derivativeLevels.get(i + 1).getDerivatives();
	                    for (int l = 0; l < nextLevelDerivatives.size() && !crossedDerivatives; l++) {
	                        Derivative d = nextLevelDerivatives.get(l);
	                        if (d.getDerivativeTerm().isPresent()) {
	                            //Get variables inside the derivative (both, derivative ...
	                            boolean used = AGDMUtils.find(d.getDerivativeTerm().get(), ".//mt:ci[text() = '" + derId + "']").getLength() > 0;
	                            //... and multiplication terms)
	                            for (int m = 0; m < multIds.size(); m++) {
	                            	used = used || AGDMUtils.find(d.getDerivativeTerm().get(), ".//mt:ci[text() = '" + multIds.get(m) + "']").getLength() > 0;
	                            }
	                            if (used) {
	                                ArrayList<String> derCoords = d.getCoordinates();
	                                for (int m = 0; m < coords.size(); m++) {
	                                    //A crossed derivative is found
	                                    if (!derCoords.contains(coords.get(m))) {
	                                    	crossedDerivatives = true;
	                                    }
	                                }
	                            }
	                        }
	                    }
                    }
                    if (coords.size() > 1) {
                    	String coord = coords.get(0);
                        for (int m = 1; m < coords.size(); m++) {
                        	if (!coord.equals(coords.get(m))) {
                        		crossedDerivatives = true;
                        	}
                        }
                    }
                }
            }
        }
        if (step != null) {
        	crossedDerivatives = crossedDerivatives || step.getElementsByTagNameNS(AGDMUtils.mmsUri, "meshDissipation").getLength() > 0;
        }
        //Accumulated stencil in Conservative discretization
        //Load schema
        if (step != null) {
            NodeList discretization = AGDMUtils.find(step, ".//mms:conservativeTermDiscretization/mms:meshDiscretization");
            if (discretization.getLength() > 0) {
		        String schemaId = discretization.item(0).getFirstChild().getLastChild().getTextContent();
		        consStencilMesh = getSchemaStencil(schemaId);
		        //Set maximum stencil
		        if (accumulatedStencil.size() > 0) {
		        	accumulatedStencil.set(accumulatedStencil.size() - 1,
		        			Math.max(accumulatedStencil.get(accumulatedStencil.size() - 1), consStencilMesh));
		        }
		        else {
		        	accumulatedStencil.add(consStencilMesh);
		        }
            }
        }
        
        //Particles does not have stencil, but they need accumulated stencil information.
        //If no mesh derivatives, add accumulated stencil for particle first algebraic terms 
        if (accumulatedStencil.size() == 0) {
         	accumulatedStencil.add(0);
        }
        //Check that there are enough accumulatedStencil for every particle operator level.
        for (int i = 0; i < particleDerivativeLevels - accumulatedStencil.size() + 1; i++) {
        	accumulatedStencil.add(1, 0);
        }

    }
    
    /**
     * Creates the analysis instructions.
     * @param discProblem       		The problem being discretized
     * @param problem           		The continuous problem
     * @param opInfo            		The operator information
     * @param pi                		The process information
     * @param inputs					Analysis discretization input variables
     * @return                  		The schema
     * @throws AGDMException    		AGDM00X - External error
     */
    public Element createAnalysis(Document discProblem, OperatorsInfo opInfo, ProcessInfo pi, ArrayList<Element> inputs) throws AGDMException {
        Document doc = discProblem;
        crossedDerivatives = false;
        maxStepStencil = opInfo.getStencil();
        hasNonConservativeTermsMesh = opInfo.getDerivativeLevels().size() > 0;
        
        calculateAccumulatedStencilAndCrossedDerivatives(opInfo.getDerivativeLevels(), opInfo.getMultiplicationLevels(), 0, null);
        //If no children, there is no analysis terms.
        if (accumulatedStencil.size() == 0) {
            return null;
        }
        Element simml = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "simml");
        
        //
        Element timeSlice = AGDMUtils.createTimeSlice(doc);
        
        //Structures for optimization on algebraic terms
        Hashtable<NodeWrap, String> usedTerms = new Hashtable<NodeWrap, String>();
        AlgebraicSimplifications simplifications = new AlgebraicSimplifications();
        
        //First block
        Element iterOverCells = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "iterateOverCells");
        iterOverCells.setAttribute("stencilAtt", String.valueOf(0));
        iterOverCells.setAttribute("appendAtt", String.valueOf(accumulatedStencil.get(0)));
        //First order algebraic terms
        if (hasNonConservativeTermsMesh) {
        	iterOverCells.appendChild(createAlgebraicTerms(doc, opInfo.getDerivativeLevels().get(0), pi, timeSlice, usedTerms, simplifications));
        }
        insertCornerCalculation(iterOverCells);
        simml.appendChild(iterOverCells);
        //Second block (repetitive)
        for (int i = 0; i < opInfo.getDerivativeLevels().size(); i++) {
            Element iterOverCells2 = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "iterateOverCells");
            //Nth order derivative calculation
            if (hasNonConservativeTermsMesh) {
	            iterOverCells2.appendChild(createDerivativeCalculation(doc, opInfo, opInfo.getDerivativeLevels().get(i), discProblem, discProblem.getDocumentElement(), pi, inputs, i, 
	                    i + 1 == opInfo.getDerivativeLevels().size(), simplifications, false, null));
            }
            //No last block
            if (i + 1 < opInfo.getDerivativeLevels().size()) {
            	//Multiplication of derivatives and algebraic terms
                iterOverCells2.appendChild(createDerivativeMultiplication(doc, opInfo, opInfo.getMultiplicationLevels(), opInfo.getDerivativeLevels(), pi, timeSlice, OperatorInfoType.analysisField, i, i + 1 == opInfo.getDerivativeLevels().size()));
                //Nth order order algebraic terms
                if (hasNonConservativeTermsMesh) {
                	iterOverCells2.appendChild(createAlgebraicTerms(doc, opInfo.getDerivativeLevels().get(i + 1), pi, timeSlice, usedTerms, simplifications));
                }
                insertCornerCalculation(iterOverCells2);
            }
            //Last block
            else {
                //Auxiliary variable equations for derivative equations
            	OperatorsInfo opInfoVariables = new OperatorsInfo(opInfo);
            	HashSet<String> sourceVariables = opInfoVariables.getNonConservativeFields();
            	opInfoVariables.onlyDependent(sourceVariables, true, opInfoVariables.getNonConservativeCommonConditional());            	
                iterOverCells2.appendChild(createAuxiliaryVariableEquations(discProblem, discProblem.getDocumentElement(), opInfoVariables, opInfo.getDerivativeLevels(), opInfo.getMultiplicationLevels(), opInfoVariables.getDerivativeLevels(), opInfoVariables.getMultiplicationLevels(), pi, timeSlice, i, false));
                //Sources (they will only be in the last level)
                if (hasNonConservativeTermsMesh) {
                	iterOverCells2.appendChild(createSourcesMesh(doc, opInfo.getDerivativeLevels().get(i), pi, timeSlice, OperatorInfoType.analysisField));
                }
                //Multiplication of derivatives and algebraic terms
                iterOverCells2.appendChild(createDerivativeMultiplication(doc, opInfo, opInfo.getMultiplicationLevels(), opInfo.getDerivativeLevels(), pi, timeSlice, OperatorInfoType.analysisField, i, i + 1 == opInfo.getDerivativeLevels().size()));
                //Equation term summation
                iterOverCells2.appendChild(createEquationSummationMesh(doc, opInfo, pi, timeSlice, OperatorInfoType.analysisField, false));
            }
            // Stencil attribute
            if (i + 1 < opInfo.getDerivativeLevels().size()) {
            	int stencil = getLevelStencil(opInfo.getDerivativeLevels().get(i).getDerivatives());
                iterOverCells2.setAttribute("stencilAtt", 
                        String.valueOf(stencil));
            }
            else {
            	int stencil = getLevelStencil(opInfo.getDerivativeLevels().get(i).getDerivatives());
                iterOverCells2.setAttribute("stencilAtt", String.valueOf(stencil));
            }
            if (i + 2 == opInfo.getDerivativeLevels().size()) {
                iterOverCells2.setAttribute("appendAtt", String.valueOf(accumulatedStencil.get(i + 1)));    
            } 
            else {
                if (i + 1 < opInfo.getDerivativeLevels().size() && opInfo.getDerivativeLevels().get(i + 1) != null) {
                    iterOverCells2.setAttribute("appendAtt", String.valueOf(accumulatedStencil.get(i + 1)));    
                }
            }
            simml.appendChild(iterOverCells2);
        }

        return simml;
    }
    
    /**
     * Get prestep from current step.
     * @param step				Current step
     * @param dt				Discretization type
     * @return					Pre-step if it exists
     * @throws AGDMException 	AGDM00X - External error
     */
    private Element getPrestep(Element step, DiscretizationType dt) throws AGDMException {
    	String query = "";
    	if (dt == DiscretizationType.mesh) {
    		query = "mms:meshPreStep";
    	}
     	if (dt == DiscretizationType.particles) {
    		query = "mms:particlePreStep";
    	}
    	NodeList prestep = step.getElementsByTagName(query);
    	if (prestep.getLength() > 0) {
    		return (Element) prestep.item(0).cloneNode(true);
    	}
    	return null;
    }
    
    /**
     * Get poststep from current step.
     * @param step				Current step
     * @param dt				Discretization type
     * @return					Post-step if it exists
     * @throws AGDMException 	AGDM00X - External error
     */
    private Element getPoststep(Element step, DiscretizationType dt) throws AGDMException {
    	String query = "";
    	if (dt == DiscretizationType.mesh) {
    		query = "mms:meshPostStep";
    	}
     	if (dt == DiscretizationType.particles) {
    		query = "mms:particlePostStep";
    	}
    	NodeList poststep = step.getElementsByTagName(query);
    	if (poststep.getLength() > 0) {
    		return (Element) poststep.item(0).cloneNode(true);
    	}
    	return null;
    }
    
    /**
     * Get prestep maximum stencil from prestep.
     * @param prestep			Current prestep
     * @return					Maximum stencil
     * @throws AGDMException 	AGDM00X - External error
     */
    private int getPreStepMaxStencil(DocumentFragment prestep) throws AGDMException {
    	int stencil = 0;
	    if (prestep != null) {
	    	NodeList children = prestep.getChildNodes();
	    	for (int i = 0; i < children.getLength(); i++) {
	    		stencil = Math.max(stencil, Integer.parseInt(((Element) children.item(i)).getAttribute("stencilAtt")));
	    	}
    	}
    	return stencil;
    }
    
    /**
     * Adds the dissipation parameters for every field to the discrete problem.
     * @param problem				The discrete problem
     * @param schema				The current schema
     * @param evolutionEquations	The evolution equations
     * @param dissipation			Dissipation tag
     * @throws AGDMException		AGDM00X - External error
     */
    private void addDissipationParameters(Document problem, Document schema, ArrayList<String> evolutionEquations, String dissipation) throws AGDMException {
    	if (schema.getDocumentElement().getElementsByTagNameNS(AGDMUtils.mmsUri, dissipation).getLength() > 0) {
    		//Fragment to store dissipation parameters
            DocumentFragment paramsFragment = problem.createDocumentFragment();
            //Create dissipation parameter for current field
            for (int i = 0; i < evolutionEquations.size(); i++) {
                String field = evolutionEquations.get(i);
                Element param = AGDMUtils.createElement(problem, AGDMUtils.mmsUri, "parameter");
                Element name = AGDMUtils.createElement(problem, AGDMUtils.mmsUri, "name");
                Element type = AGDMUtils.createElement(problem, AGDMUtils.mmsUri, "type");
                Element defaultValue = AGDMUtils.createElement(problem, AGDMUtils.mmsUri, "defaultValue");
                name.setTextContent("dissipation_factor_" + field);
                type.setTextContent("REAL");
                defaultValue.setTextContent("0");
                param.appendChild(name);
                param.appendChild(type);
                param.appendChild(defaultValue);
                if (AGDMUtils.find(problem, "//mms:parameters/mms:parameter/mms:name[text() = '" + name.getTextContent() + "']").getLength() == 0) {
                	paramsFragment.appendChild(param);	
                }
            }
            //Adding dissipation parameters to the problem.
            NodeList params = AGDMUtils.find(problem, "//mms:parameters");
            Element parameters = null;
            if (params.getLength() > 0) {
            	parameters = (Element) params.item(0);
            }
            //There are no parameters currently in the problem. Create the section
            else {
            	parameters = AGDMUtils.createElement(problem, AGDMUtils.mmsUri, "parameters");
            	Element models = (Element) AGDMUtils.find(problem, "//mms:models").item(0);
            	models.getParentNode().insertBefore(parameters, models);
            }
            parameters.appendChild(paramsFragment);
    	}
    }
    
    /**
     * Adds velocity parameter.
     * @param problem				The discrete problem
     * @param pi					Problem information
     * @throws AGDMException		AGDM00X - External error
     */
    private static void addVelocityParameter(Document problem, ProcessInfo pi) throws AGDMException {
    	boolean velocityParams = false;
        for (String speciesName: pi.getParticleSpecies().keySet()) {
        	pi.setCurrentSpecies(speciesName);
        	if (pi.getParticleSpecies().size() > 0 && pi.getParticleVelocities().size() > 0) {
        		velocityParams = true;
        	}
        }
    	if (velocityParams && AGDMUtils.find(problem, "//mms:parameters/mms:parameter/mms:name[text() = 'particle_velocity_corrector_factor']").getLength() == 0) {
    		Element param = AGDMUtils.createElement(problem, AGDMUtils.mmsUri, "parameter");
    		Element name = AGDMUtils.createElement(problem, AGDMUtils.mmsUri, "name");
    		Element type = AGDMUtils.createElement(problem, AGDMUtils.mmsUri, "type");
    		Element defaultValue = AGDMUtils.createElement(problem, AGDMUtils.mmsUri, "defaultValue");
    		name.setTextContent("particle_velocity_corrector_factor");
    		type.setTextContent("REAL");
    		defaultValue.setTextContent("0.0");
    		param.appendChild(name);
    		param.appendChild(type);
    		param.appendChild(defaultValue);

    		//Adding dissipation parameters to the problem.
    		NodeList params = AGDMUtils.find(problem, "//mms:parameters");
    		Element parameters = null;
    		if (params.getLength() > 0) {
    			parameters = (Element) params.item(0);
    		}
    		//There are no parameters currently in the problem. Create the section
    		else {
    			parameters = AGDMUtils.createElement(problem, AGDMUtils.mmsUri, "parameters");
    			Element models = (Element) AGDMUtils.find(problem, "//mms:models").item(0);
    			models.getParentNode().insertBefore(parameters, models);
    		}
    		parameters.appendChild(param);
    	}
    }
    
    /**
     * Returns the flux input variable corresponding to the flux call input variable.
     * @param fluxCallInputVariable		Flux call input variable
     * @param step						Schema step
     * @param dt						Discretization type
     * @return							Flux input variable with output variable equals to fluxCallInputVariable
     * @throws AGDMException			AGDM00X - External error
     */
    private Node getFluxInputVariable(Node fluxCallInputVariable, Element step, DiscretizationType dt) throws AGDMException {
        ArrayList<Element> inputVariables = getFluxInputVariables(step, dt);
        ArrayList<Element> outputVariables = getFluxOutputVariables(step, dt);
        Node fieldVar;
        
        //If there is a neighbourParticle tag, remove it before the comparison...
        boolean neighbourParticle = false;
        if (fluxCallInputVariable.getLocalName().equals("neighbourParticle")) {
        	neighbourParticle = true;
        	fluxCallInputVariable = fluxCallInputVariable.getFirstChild();
        }
        //Compare only variable without index if exists
		if (fluxCallInputVariable.getLocalName().equals("apply")) {
			fieldVar = fluxCallInputVariable.getFirstChild();
		}
		else {
			fieldVar = fluxCallInputVariable;
		}
        for (int i = 0; i < outputVariables.size(); i++) {
        	if (outputVariables.get(i).getFirstChild().isEqualNode(fieldVar)) {
        		Node output;
        		//Copy index of original variable if exists
        		if (fluxCallInputVariable.getLocalName().equals("apply")) {
        			output = fluxCallInputVariable.cloneNode(true);
        			if (inputVariables.get(i).getFirstChild().getLocalName().equals("sharedVariable")) {
        				Node shared = inputVariables.get(i).getFirstChild().cloneNode(false);
        				output.replaceChild(inputVariables.get(i).getFirstChild().getFirstChild().cloneNode(true), output.getFirstChild());
        				shared.appendChild(output);
        				output = shared;
        			}
        			else {
        				output.replaceChild(inputVariables.get(i).getFirstChild().cloneNode(true), output.getFirstChild());
        			}
        		}
        		else {
        			output = inputVariables.get(i).getFirstChild().cloneNode(true);
        		}

        		//If there was a neighbourParticle tag, restore it
        		if (neighbourParticle) {
    	        	Element neighbourParticleE = AGDMUtils.createElement(fluxCallInputVariable.getOwnerDocument(), AGDMUtils.simmlUri, "neighbourParticle");
    	        	neighbourParticleE.appendChild(output);
    	        	output = neighbourParticleE;
        		}

        		return output;
        	}
        }
        return null;
    }
    
    /**
     * Creates the particle movement.
     * @param problem			Problem
     * @param step				Current time step
     * @param pi				Process information
     * @return					movement
     * @throws AGDMException	AGDM00X - External error
     */
    private DocumentFragment createParticleMovement(Node problem, Element step, ProcessInfo pi) throws AGDMException {
    	Document doc = problem.getOwnerDocument();
    	DocumentFragment result = doc.createDocumentFragment();
    	
        //rule info
        RuleInfo ri = new RuleInfo();
        ri.setProcessInfo(pi);
        ri.setIndividualField(false);
        ri.setIndividualCoord(false);
        ri.setFunction(false);
        ri.setAuxiliaryCDAdded(false);
        ri.setAuxiliaryFluxesAdded(false);
        ri.setUseBaseCoordinates(false);
        ri.setExtrapolation(pi.getExtrapolationMethod() != null && pi.getIncomFields() != null);
		    		
		Element newPositionVariable = (Element) convertToPosition(timeOutputVariable);
		Element iterOverParticles = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "iterateOverParticles");
		iterOverParticles.setAttribute("speciesNameAtt", pi.getCurrentSpecies());
		//Create moveParticles tag
		Element movement = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "moveParticles");
		Element mathNew = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "math");
		mathNew.appendChild(doc.importNode(newPositionVariable.cloneNode(true), true));
		movement.appendChild(mathNew);
		iterOverParticles.appendChild(ExecutionFlow.processMoveParticles(doc, movement, ri));
		result.appendChild(iterOverParticles);
    	
    	return result;
    }
    
    /**
     * Converts a field template into a particle position template.
     * @param varTemplate		The field variable
     * @return					Particle position template
     * @throws AGDMException	AGDM00X  External error
     */
    static public Node convertToPosition(Node varTemplate) throws AGDMException {
    	Node result = varTemplate.cloneNode(true);
    	//Remove cell access if any
    	NodeList currentCell = ((Element) result).getElementsByTagName("sml:currentCell");
    	if (currentCell.getLength() > 0) {
    		result = currentCell.item(0).getPreviousSibling();
    	}
    	//Replace field usage with particlePosition
    	NodeList fields = ((Element) result).getElementsByTagName("sml:field");
    	for (int i = 0; i < fields.getLength(); i++) {
    		varTemplate.getOwnerDocument().renameNode(fields.item(i), AGDMUtils.simmlUri, "sml:particlePosition");
    	}    
    	return result;
    }
    
    /**
     * Gets the previous position variable from current step.
     * @param doc				Document
     * @param step				Current step
     * @return					Previous position template
     * @throws AGDMException	AGDM00X - External error
     */
    private Element getPreviousPositionVariable(Document doc, Element step) throws AGDMException {
    	Node previousStep = step.getPreviousSibling();
    	if (previousStep != null) {
    		return (Element) convertToPosition(getTimeOutputVariable((Element) previousStep));
    	}
    	else {
    		return AGDMUtils.createPreviousPosition(doc);
    	}
    }
    
    /**
     * Gets the previous variable from current step.
     * @param doc				Document
     * @param step				Current step
     * @return					Previous position template
     * @throws AGDMException	AGDM00X - External error
     */
    static private Element getPreviousVariable(Document doc, Element step) throws AGDMException {
    	Node previousStep = step.getPreviousSibling();
    	if (previousStep != null) {
    		return getTimeOutputVariable((Element) previousStep);
    	}
    	else {
    		return AGDMUtils.createPreviousTimeSlice(doc);
    	}
    }
    
    /**
     * Unificate loops when possible.
     * @param doc					Current document
     * @param pi					The processInfo
     * @param fluxLoop				The flux loop reference
     * @param discretizationLoop	The discretization loop reference
     * @param step					Current step schema
     * @param nonConsStencil		Stencil of non conservative discretization
     * @param dissipStencil			Stencil of dissipation
     * @throws AGDMException		AGDM00X - External error
     */
    private void loopUnification(Document doc, ProcessInfo pi, Element fluxLoop, Element discretizationLoop, Element interpolation, Element step, int nonConsStencil, int dissipStencil) throws AGDMException {
    	//Unification of loops from flux loop to prestep 
    	Element loop2 = fluxLoop;
    	Element loop1 = (Element) fluxLoop.getPreviousSibling();
    	boolean isFluxLoop = true;
    	
    	while (loop1 != null && (interpolation == null || !loop1.isEqualNode(interpolation))) {
    		int a1 = Integer.parseInt(loop1.getAttribute("appendAtt"));
    		int a2 = Integer.parseInt(loop2.getAttribute("appendAtt"));
    		int s1 = Integer.parseInt(loop1.getAttribute("stencilAtt"));
    		int s2 = Integer.parseInt(loop2.getAttribute("stencilAtt"));
    		NodeList children = loop2.getChildNodes();
    		//Check unification conditions
    		if (children.getLength() > 0 && (maxStepStencil - s1 >= a2 || a2 <= a1) && s1 >= 0 && s2 == 0 
    				&& !AGDMUtils.hasSelfDependencies(loop2)) {
    			loop1.setAttribute("appendAtt", String.valueOf(Math.max(a1, a2)));
    			if (loop2.hasAttribute("cornerCalculationAtt")) {
    				loop1.setAttribute("cornerCalculationAtt", loop2.getAttribute("cornerCalculationAtt"));
    			}
    			//Set no Synchronization to loop2 math assignments
    			ArrayList<NodeWrap> assignedVars = AGDMUtils.getAssignedVars(loop2, false);
    			for (int i = 0; i < assignedVars.size(); i++) {
    				((Element) assignedVars.get(i).getNode().getParentNode().getParentNode()).setAttributeNS(AGDMUtils.simmlUri, "sml:type", "noSync");
    			}
    			//Copy children from back to forward
    			Element refNode = (Element) children.item(children.getLength() - 1);
    			loop1.appendChild(refNode);
    			for (int i = children.getLength() - 1; i >= 0; i--) {
    				Element nodeToCopy = (Element) children.item(i);
    				loop1.insertBefore(nodeToCopy, refNode);
    				refNode = nodeToCopy;
    			}
    			unifyParticleIteration(pi, loop1);
    			makeLocalVariables(pi, loop1);
    			if (isFluxLoop) {
    				fluxLoop = loop1;
    			}
    		}
    		else {
    			isFluxLoop = false;
    		}
        	loop2 = loop1;
        	loop1 = (Element) loop2.getPreviousSibling();
    	}
    	
    	//Unification of discretization loop with previous loops
    	//Search for last prestep loop with code
    	Element prestep = (Element) fluxLoop.getPreviousSibling();
    	while (prestep != null && prestep.getChildNodes().getLength() == 0) {
    		prestep = (Element) prestep.getPreviousSibling();
    	}
    	if (prestep != null && (interpolation == null || !prestep.isEqualNode(interpolation))) {
	    	//Get step input variables
	    	Element nonConsInput = getNonConsInputVariable(doc, step);
	    	ArrayList<NodeWrap> nonConsInputs = new ArrayList<NodeWrap>();
	    	ArrayList<Element> nonConsInputsElem = new ArrayList<Element>();
	    	nonConsInputsElem.add(nonConsInput);
	    	if (!pi.getMeshFields().isEmpty()) {
	    		pi.setDiscretizationType(DiscretizationType.mesh);
	    		nonConsInputs.addAll(replaceTags(pi, nonConsInputsElem, false));
	    		nonConsInputs.addAll(replaceTags(pi, getNonConsFunctionInputVariables((Element) pi.getCurrentTimeStep(), DiscretizationType.mesh), false));
	    	}
	    	if (pi.getParticleSpecies().size() > 0) {
	    		pi.setDiscretizationType(DiscretizationType.particles);
		    	for (String speciesName: pi.getParticleSpecies().keySet()) {
		    		pi.setCurrentSpecies(speciesName);
		    		nonConsInputs.addAll(replaceTags(pi, nonConsInputsElem, false));
		    		nonConsInputs.addAll(replaceTags(pi, getNonConsFunctionInputVariables((Element) pi.getCurrentTimeStep(), DiscretizationType.particles), false));
		    	}
	    	}
	    	
	    	//Conservative input could be currentCell, that is a list of elements (the child of math cannot be used directly)
	    	ArrayList<Element> tmp = new ArrayList<Element>();
	     	if (!pi.getMeshFields().isEmpty()) {
	     		tmp.addAll(getConservativeDiscretizationInputVariables(step, DiscretizationType.mesh));
	     	}
	    	if (pi.getParticleSpecies().size() > 0) {
	    		tmp.addAll(getConservativeDiscretizationInputVariables(step, DiscretizationType.particles));
	    	}
	    	ArrayList<Element> consInput = new ArrayList<Element>();
	    	for (int i = 0; i < tmp.size(); i++) {
	    		consInput.add((Element) tmp.get(i).getFirstChild());
	    	}
	    	ArrayList<Element> dissipInput = getMeshDissipationInputVariables(step);
	    	dissipInput.addAll(getParticleDissipationInputVariables(step));
	    	//Replace templates and get field variables

	    	ArrayList<NodeWrap> consInputs = replaceTags(pi, consInput, false);
	    	ArrayList<NodeWrap> dissipInputs = replaceTags(pi, dissipInput, false);
	    	//For every variable assigned in the prestep to unify (with and without index)...
	    	ArrayList<NodeWrap> assignedVars = AGDMUtils.getAssignedVars(prestep, true);
	    	assignedVars.addAll(AGDMUtils.getAssignedVars(prestep, false));
	    	Iterator<NodeWrap> assignedVarsIt = assignedVars.iterator();
	    	boolean unifiable = true;
	    	
	    	while (assignedVarsIt.hasNext() && unifiable) {
	    		NodeWrap assignedVar = assignedVarsIt.next();
	    		// ... check if is used as dissipation input
	    		if (dissipStencil > 0 && dissipInputs.contains(assignedVar)) {
	    			unifiable = false;
	    		}
	    		// ... check if is used as conservative input
	    		if (consStencilMesh > 0 && consInputs.contains(assignedVar)) {
	    			unifiable = false;
	    		}
	    		// ... check if is used as nonConservative input
	    		if (nonConsStencil > 0 && nonConsInputs.contains(assignedVar)) {
	    			unifiable = false;
	    		}
	    	}
	    	//If the loops are unifiable
	    	if (unifiable) {
				//Copy children from back to forward at the beginning of the loop
	    		NodeList children = prestep.getChildNodes();
				for (int i = children.getLength() - 1; i >= 0; i--) {
					discretizationLoop.insertBefore(children.item(i), discretizationLoop.getFirstChild());
				}
				unifyParticleIteration(pi, discretizationLoop);
				makeLocalVariables(pi, discretizationLoop);
				//Get maximum stencil
	    		int s1 = Integer.parseInt(prestep.getAttribute("stencilAtt"));
	    		int s2 = Integer.parseInt(discretizationLoop.getAttribute("stencilAtt"));

	    		discretizationLoop.setAttribute("stencilAtt", String.valueOf(Math.max(s1, s2)));
    			if (prestep.hasAttribute("cornerCalculationAtt")) {
    				discretizationLoop.setAttribute("cornerCalculationAtt", prestep.getAttribute("cornerCalculationAtt"));
    			}
	    	}
    	}
    	//Unify iterateOverInteractions in the same iterateOverParticles when possible
    	if (fluxLoop.getParentNode() != null) {
	    	NodeList blocks = AGDMUtils.find(fluxLoop.getParentNode(), ".//sml:iterateOverParticles[count(sml:iterateOverInteractions) > 1]");
	    	for (int i = 0; i < blocks.getLength(); i++) {
	    		Element iop = (Element) blocks.item(i);
	    		NodeList ioi = iop.getElementsByTagNameNS(AGDMUtils.simmlUri, "iterateOverInteractions");
	    		//From back to front
	    		for (int j = ioi.getLength() - 1; j > 0; j--) {
	    			//1 - Get assigned vars in first iterateOverInteractions loop
	    			ArrayList<NodeWrap> ioiVars = AGDMUtils.getAllAssignedVars(ioi.item(j - 1));
	    			//2 - Get instructions and assigned vars between iterateOverInteractions using variables assigned in 1
	    			ArrayList<Node> instructions = new ArrayList<Node>();
	    			Element instruction = (Element) ioi.item(j - 1).getNextSibling();
	    			while (!instruction.getLocalName().equals("iterateOverInteractions")) {
	    				//Check if any variable is used in the instruction    			
	    				if (!Collections.disjoint(ioiVars, AGDMUtils.getAllUsedVars(instruction, false))) {
	    					//Add instruction
	    					instructions.add(instruction);
	    					ioiVars.addAll(AGDMUtils.getAllAssignedVars(instruction));
	    				}
	    				instruction = (Element) instruction.getNextSibling();
	    			}
	    			//3 - Check that those variables are not used inside the following iterateOverInteractions loop
	    			if (Collections.disjoint(ioiVars, AGDMUtils.getAllUsedVars(ioi.item(j), false)) && 
	    					ioi.item(j).getFirstChild().isEqualNode(ioi.item(j - 1).getFirstChild())) {
	    				//4 - Unify if 3
	    				//Insert instructions at the beginning of their respective scope
	    				//External iterateOverInteractions instructions
	    				for (int k = instructions.size() - 1; k >= 0; k--) {
	    					iop.insertBefore(instructions.get(k).cloneNode(true), ioi.item(j).getNextSibling());
	    					iop.removeChild(instructions.get(k));
	    				}
	    				//Inner iterateOverInteractions instructions
	    				NodeList ioiInstructions = ioi.item(j - 1).getChildNodes();
	    				//Skip positionReference
	    				for (int k = 1; k < ioiInstructions.getLength(); k++) {
	    					ioi.item(j).insertBefore(ioiInstructions.item(k).cloneNode(true), ioi.item(j).getFirstChild().getNextSibling());
	    				}
	    				iop.removeChild(ioi.item(j - 1));
	    			}    			
	    		}
	    	}
    	}
    }
    
    /**
     * Unifies particle iterations.
     * @param pi				Problem information
     * @param loop				Parent loop
     * @throws AGDMException	AGDM00X - External error
     */
    private void unifyParticleIteration(ProcessInfo pi, Element loop) throws AGDMException {
    	for (String speciesName: pi.getParticleSpecies().keySet()) {
    		NodeList iterations = AGDMUtils.find(loop, "./sml:iterateOverParticles[@speciesNameAtt = '" + speciesName + "']");
    		for (int i = iterations.getLength() - 2; i >= 0; i--) {
    			Element lastIteration = (Element) iterations.item(iterations.getLength() - 1);
    			NodeList children = iterations.item(i).getChildNodes();
    			for (int j = children.getLength() - 1; j >= 0; j--) {
    				lastIteration.insertBefore(children.item(j), lastIteration.getFirstChild());
    			}
    			iterations.item(i).getParentNode().removeChild(iterations.item(i));
    		}
    	}
    }
    
    /**
     * Check which mesh variables does not need to be synchronized.
     * @param doc				Current document
     * @param schemaStep		Schema step
     * @param opInfo			Operators info
     * @param pi                The process information
     * @param stepCode			Current step code
     * @throws AGDMException 	AGDM00X - External error
     */
    private void checkSynchronizationVariables(Document doc, Element schemaStep, OperatorsInfo opInfo, ProcessInfo pi, Node stepCode) throws AGDMException {
    	//Create step output variables
    	ArrayList<NodeWrap> timeOutputVars = new ArrayList<NodeWrap>();
        ArrayList<String> equations = pi.getRegionInfo().getFields();
        for (int i = 0; i < equations.size(); i++) {
            String field = equations.get(i);
            ProcessInfo newPi = new ProcessInfo(pi);
            newPi.setField(field);
            Node outputVar;
            if (pi.getRegionInfo().isAuxiliaryField(field)) {                
                outputVar = ExecutionFlow.replaceTags(AGDMUtils.createTimeSlice(doc), newPi);
                if (outputVar.getLocalName().equals("sharedVariable")) {
                	outputVar = outputVar.getFirstChild();
                }
                if (outputVar.getLocalName().equals("apply")) {
                	outputVar = outputVar.getFirstChild();
                }
            }
            else {
            	outputVar = ExecutionFlow.replaceTags(timeOutputVariable.cloneNode(true), newPi);
                if (outputVar.getLocalName().equals("sharedVariable")) {
                	outputVar = outputVar.getFirstChild();
                }
                if (outputVar.getLocalName().equals("apply")) {
                	outputVar = outputVar.getFirstChild();
                }
            }
            timeOutputVars.add(new NodeWrap(outputVar));
        }
    	
    	NodeList children = stepCode.getChildNodes();
    	//Check which variable assigned in a block is read before a new assignment in the following blocks
    	for (int i = 0; i < children.getLength() - 1; i++) {
    		Node block = children.item(i);
    		ArrayList<NodeWrap> assignedVars = AGDMUtils.getAssignedNonOutputVars(block, timeOutputVars);
    		ArrayList<String> assignedVarString = AGDMUtils.getAssignedNonOutputVarsString(block, timeOutputVars);
    		for (int j = i + 1; j < children.getLength() && assignedVarString.size() > 0; j++) {
    	    	NodeList instructions = children.item(j).getChildNodes();
    	    	for (int k = 0; k < instructions.getLength() && assignedVarString.size() > 0; k++) {
    	    		checkInstructionSynchronization(instructions.item(k), assignedVarString, assignedVars, true);
    	    	}
    		}
    		//If any variable has not be found, it does not need to be synchronized
    		for (int j = 0; j < assignedVarString.size(); j++) {
    			((Element) AGDMUtils.find(assignedVars.get(j).getNode(), "./ancestor-or-self::mt:math").item(0)).setAttributeNS(AGDMUtils.simmlUri, "sml:type", "noSync");
    		}
    	}
    }
    
    /**
     * Check which mesh variables does not need to be synchronized in the context of a given instruction.
     * @param instruction			Instruction to check
     * @param assignedVarString		The name of the variables to check
     * @param assignedVars          The dom of the variables to check
     * @param addNonSyncAtt			If the variable can be marked as non-synchronizable variable
     * @return						A variable that does not need to synchronize or empty.
     * @throws AGDMException 		AGDM00X - External error
     */
    private String checkInstructionSynchronization(Node instruction, ArrayList<String> assignedVarString, ArrayList<NodeWrap> assignedVars, boolean addNonSyncAtt) throws AGDMException {
    	String nonSyncFound = "";

		if (instruction.getLocalName().equals("math")) {
			ArrayList<String> usedVars = AGDMUtils.getUsedVarsString(instruction, false);
			ArrayList<String> assignedList = AGDMUtils.getAssignedVarsString(instruction, false);
			if (assignedList.size() > 0 && assignedVarString.contains(assignedList.get(0))) {
				String assigned = assignedList.get(0);
				int index = assignedVarString.indexOf(assigned);
				//assigned variable, no need to synchronize
				if (!usedVars.contains(assigned)) {
					if (addNonSyncAtt) {
	        			((Element) AGDMUtils.find(assignedVars.get(index).getNode(), "./ancestor-or-self::mt:math").item(0)).setAttributeNS(AGDMUtils.simmlUri, "sml:type", "noSync");
					}
					nonSyncFound = assigned;
				}
				assignedVarString.remove(assigned);
				assignedVars.remove(index);
			}
			else {
				//Used variable, need to synchronize
				removeUsedVars(assignedVarString, assignedVars, usedVars);
			}
		}
		else {
			if (instruction.getLocalName().equals("if")) {
				Node condition = instruction.getFirstChild();
				ArrayList<String> usedVars = AGDMUtils.getUsedVarsString(condition, false);
				removeUsedVars(assignedVarString, assignedVars, usedVars);
				
				if (instruction.getLastChild().getLocalName().equals("else")) {
		    		ArrayList<NodeWrap> assignedVarsThen = new ArrayList<NodeWrap>();
		    		ArrayList<String> assignedVarThenString = new ArrayList<String>();
		    		assignedVarsThen.addAll(assignedVars);
		    		assignedVarThenString.addAll(assignedVarString);
		    		
		    		ArrayList<NodeWrap> assignedVarsElse = new ArrayList<NodeWrap>();
		    		ArrayList<String> assignedVarElseString = new ArrayList<String>();
		    		assignedVarsElse.addAll(assignedVars);
		    		assignedVarElseString.addAll(assignedVarString);
		    		
		    		NodeList thenChildren = condition.getNextSibling().getChildNodes();
		    		ArrayList<String> nonSyncDeletedThen = new ArrayList<String>();
					for (int i =  0; i < thenChildren.getLength() && assignedVarThenString.size() > 0; i++) {
						String nonSync = checkInstructionSynchronization(thenChildren.item(i), assignedVarThenString, assignedVarsThen, false);
						if (!nonSync.equals("")) {
							nonSyncDeletedThen.add(nonSync);
						}
					}
					NodeList elseChildren = instruction.getLastChild().getChildNodes();
					ArrayList<String> nonSyncDeletedElse = new ArrayList<String>();
					for (int i =  0; i < elseChildren.getLength() && assignedVarString.size() > 0; i++) {
						String nonSync = checkInstructionSynchronization(elseChildren.item(i), assignedVarElseString, assignedVarsElse, false);
						if (!nonSync.equals("")) {
							nonSyncDeletedElse.add(nonSync);
						}
					}
					//Mark as nonSync vars if the variable has been assigned in both conditions
					nonSyncDeletedElse.retainAll(nonSyncDeletedThen);
					for (int i = 0; i < nonSyncDeletedElse.size(); i++) {
						String var = nonSyncDeletedElse.get(i);
						int index = assignedVarString.indexOf(var);
	        			((Element) AGDMUtils.find(assignedVars.get(index).getNode(), "./ancestor-or-self::mt:math").item(0)).setAttributeNS(AGDMUtils.simmlUri, "sml:type", "noSync");
	    				assignedVarString.remove(index);
	    				assignedVars.remove(index);
					}
					//Remove from checking the used vars in any loops
					assignedVars.retainAll(assignedVarsThen);
					assignedVars.retainAll(assignedVarsElse);
					assignedVarString.retainAll(assignedVarThenString);
					assignedVarString.retainAll(assignedVarElseString);
				}
				else {
					NodeList thenChildren = condition.getNextSibling().getChildNodes();
					for (int i =  0; i < thenChildren.getLength() && assignedVarString.size() > 0; i++) {
						checkInstructionSynchronization(thenChildren.item(i), assignedVarString, assignedVars, addNonSyncAtt);
					}
				}
			}
			else {
				if (instruction.getLocalName().equals("while")) {
					Node condition = instruction.getFirstChild();
					ArrayList<String> usedVars = AGDMUtils.getUsedVarsString(condition, false);
					removeUsedVars(assignedVarString, assignedVars, usedVars);
					NodeList loopChildren = condition.getNextSibling().getChildNodes();
					for (int i =  0; i < loopChildren.getLength() && assignedVarString.size() > 0; i++) {
						checkInstructionSynchronization(loopChildren.item(i), assignedVarString, assignedVars, addNonSyncAtt);
					}
				}
				else {
					NodeList children = instruction.getChildNodes();
					for (int i =  0; i < children.getLength() && assignedVarString.size() > 0; i++) {
						checkInstructionSynchronization(children.item(i), assignedVarString, assignedVars, addNonSyncAtt);
					}
				}
			}
		}
		return nonSyncFound;
    }
    
    /**
     * Remove variables that are used.
     * @param stringVars	Original variable names
     * @param vars			Original variable dom
     * @param usedVars		Used vars to remove
     */
    private static void removeUsedVars(ArrayList<String> stringVars, ArrayList<NodeWrap> vars, ArrayList<String> usedVars) {
		usedVars.retainAll(stringVars);
		for (int l = 0; l < usedVars.size(); l++) {
			int index = stringVars.indexOf(usedVars.get(l));
			stringVars.remove(index);
			vars.remove(index);
		}
    }
    
    /**
     * Make variables assigned in the loop local when possible.
     * @param pi				The process information
     * @param loop				The loop
     * @throws AGDMException	AGDM00X - External error
     */
    private void makeLocalVariables(ProcessInfo pi, Element loop) throws AGDMException {
    	//Check if the assigned vars are used in any of the following loops in the step
    	ArrayList<NodeWrap> assignedVars = AGDMUtils.getAssignedVars(loop, true);
    	Iterator<NodeWrap> assignedVarsIt = assignedVars.iterator();
    	ArrayList<NodeWrap> stepVars = new ArrayList<NodeWrap>();
    	if (!pi.getMeshFields().isEmpty()) {
    		pi.setDiscretizationType(DiscretizationType.mesh);
    		stepVars.addAll(replaceTags(pi, pi.getStepVariables(), true));
    	}
    	if (pi.getParticleSpecies().size() > 0) {
    		pi.setDiscretizationType(DiscretizationType.particles);
    	   	for (String speciesName: pi.getParticleSpecies().keySet()) {
    	   		pi.setCurrentSpecies(speciesName);
    	   		stepVars.addAll(replaceTags(pi, pi.getStepVariables(), true));
    	   	}
    	}
    	//Add stiff variables to be excluded in replacements
    	stepVars.addAll(replaceTags(pi, pi.getStiffVariables(), true));
    	
    	ArrayList<NodeWrap> otherLoopVars = new ArrayList<NodeWrap>();
    	//Check forward loops

     	Element nextLoop = (Element) loop.getNextSibling();
     	while (nextLoop != null && !nextLoop.getLocalName().equals("boundary")) {
     		otherLoopVars.addAll(AGDMUtils.getUsedVars(nextLoop, false));
     		otherLoopVars.addAll(AGDMUtils.getAssignedVars(nextLoop, false));
    		nextLoop = (Element) nextLoop.getNextSibling();
     	}
     	//Check variables assigned in the code with a neighbour index access
		otherLoopVars.addAll(AGDMUtils.getDisplacedVars(loop.cloneNode(true)));
    	//Check back loops
    	Element previousLoop = (Element) loop.getPreviousSibling();
       	while (previousLoop != null) {
     		otherLoopVars.addAll(AGDMUtils.getUsedVars(previousLoop, false));
     		otherLoopVars.addAll(AGDMUtils.getAssignedVars(previousLoop, false));
    		previousLoop = (Element) previousLoop.getPreviousSibling();
    	}
       	
    	while (assignedVarsIt.hasNext()) {
    		Node var = assignedVarsIt.next().getNode();
    		//Do not check substep variables
    		if (!stepVars.contains(new NodeWrap(var.getFirstChild())) && !var.getLocalName().equals("sharedVariable")) {
	        	Node varWithoutIndex = var.getFirstChild();
	        	//If the variable is not used outside the current loop, then it can be set as local
	        	if (!otherLoopVars.contains(new NodeWrap(varWithoutIndex))) {
	        		AGDMUtils.nodeReplace(loop, var, var.getFirstChild().cloneNode(true));
	        	}
    		}
    	}
    }
    
    /**
     * Replaces the templates.
     * @param pi				The process information
     * @param varTemplates		The templates to replace
     * @return					The real variables
     * @throws AGDMException	AGDM00X - External error
     */
    private ArrayList<NodeWrap> replaceTags(ProcessInfo pi, ArrayList<Element> varTemplates, boolean withInterpolatedFields) throws AGDMException {
    	ArrayList<String> coords = pi.getBaseSpatialCoordinates();
    	ArrayList<NodeWrap> result = new ArrayList<NodeWrap>();
        Set<String> fields = new HashSet<String>();
        for (int i = 0; i < pi.getRegionInfo().getFields().size(); i++) {
        	fields.add(pi.getRegionInfo().getFields().get(i));
        }
        if (withInterpolatedFields) {
        	ArrayList<String> interpFields = pi.getOperatorsInfo().getVariablesToInterpolate();
        	Iterator<String> iter = interpFields.iterator();
	    	while(iter.hasNext()) {
	    		String toAdd = iter.next();
	    		fields.remove(toAdd);
	    		if (pi.getDiscretizationType() == DiscretizationType.mesh) {
	    			fields.add(toAdd + "_mesh");
	    		}
	    		if (pi.getDiscretizationType() == DiscretizationType.particles) {
	    			fields.add(toAdd + "_particles");
	    		}
	    	}
        }
    	
        Iterator<String> fieldsIt = fields.iterator();
        while (fieldsIt.hasNext()) {
        	String field = fieldsIt.next();
    		pi.setField(field);
    		for (int j = 0; j < varTemplates.size(); j++) {
    			NodeList spatialIteration = AGDMUtils.find(varTemplates.get(j), ExecutionFlow.SPATIAL_IT_TAGS);
    			if (spatialIteration.getLength() > 0) {
		           for (int k = 0; k < coords.size(); k++) {
		        	   pi.setSpatialCoord1(coords.get(k));
		        	   pi.setSpatialCoord2(null);
					result.add(new NodeWrap(ExecutionFlow.replaceTags(varTemplates.get(j).cloneNode(true), pi)));
		           }
    			}
    			else {
    				result.add(new NodeWrap(ExecutionFlow.replaceTags(varTemplates.get(j).cloneNode(true), pi)));
    			}
    		}
    	}
    	return result;
    }
    
    /**
     * Replaces the stiffExplicit tag with the proper variable.
     * @param pi				Process info
     * @param variable			Variable to use
     * @param scope				Place for the replacement
     * @throws AGDMException	AGDM00X - External error
     */
    private void replaceStiffExplicitVariable(ProcessInfo pi, Element variable, Node scope) throws AGDMException {
        NodeList stiff = ((Element) scope).getElementsByTagName("sml:stiffExplicit");
        for (int i = stiff.getLength() - 1; i >= 0; i--) {
        	String field = stiff.item(i).getFirstChild().getTextContent();
        	ProcessInfo newPi = new ProcessInfo(pi);
        	newPi.setField(field);
        	Element var = ExecutionFlow.replaceTags(variable.cloneNode(true), newPi);
            if (var.getLocalName().equals("sharedVariable")) {
            	var = (Element) var.getFirstChild();
            }
         	AGDMUtils.addPrefix(var, "aux_");
        	
        	stiff.item(i).getParentNode().replaceChild(scope.getOwnerDocument().importNode(var, true), stiff.item(i));
        }
    }
    
    /**
     * Replaces the stiffCoefficient tag with the proper value.
     * @param pi				Process info
     * @param variable			Variable to use
     * @param scope				Place for the replacement
     * @throws AGDMException	AGDM00X - External error
     */
    private void replaceStiffCoefficient(ProcessInfo pi, Node variable, Node scope) throws AGDMException {
        NodeList stiff = ((Element) scope).getElementsByTagName("sml:stiffCoefficient");
        for (int i = stiff.getLength() - 1; i >= 0; i--) {
        	stiff.item(i).getParentNode().replaceChild(scope.getOwnerDocument().importNode(variable.cloneNode(true), true), stiff.item(i));
        }	
    }
    
    /**
     * Creates the interactions between particles and mesh if needed.
     * @param step				Current step
     * @param discProblem		Discrete problem
     * @param problem			problem
     * @param opMesh			Operators information for mesh
     * @param opParticles		Operators information for particles
     * @param pi				Process information
     * @param variableTemplate	Variable template
     * @param positionTemplate	Position variable template
     * @param type				OperatorInfoType
     * @return					Code
     * @throws AGDMException	AGDM00X - External error
     */
    private Element createParticleMeshInteraction(Element step, Document discProblem, 
            Node problem, OperatorsInfo opMesh, HashMap<String, OperatorsInfo> opParticles, ProcessInfo pi, Element variableTemplate, Element positionTemplate, OperatorInfoType type) throws AGDMException {
        Element iterOverCells = AGDMUtils.createElement(step.getOwnerDocument(), AGDMUtils.simmlUri, "iterateOverCells");
        DocumentFragment interpolations = step.getOwnerDocument().createDocumentFragment();
        int stencilAtt = 0;
        ArrayList<String> meshVars = new ArrayList<String>();
        if (type == OperatorInfoType.field) {
        	meshVars.addAll(opMesh.getVariablesToInterpolate());
        }
        if (type == OperatorInfoType.auxiliaryField) {
        	meshVars.addAll(opMesh.getAuxiliaryVariablesToInterpolate());
        }
    	if (meshVars.size() > 0) {
    		//Separate Specie interpolations
    	   	for (Entry<String, Species> entry: pi.getParticleSpecies().entrySet()) {
                ArrayList<String> meshSpeciesVars = new ArrayList<String>();
                for (String var: entry.getValue().getParticleFields()) {
                	if (meshVars.contains(var)) {
                		meshSpeciesVars.add(var);
                	}
                }
        		ProcessInfo newPi = new ProcessInfo(pi);
        		newPi.setCurrentSpecies(entry.getKey());
                interpolations.appendChild(createParticleInterpolationCalls(interpolations.getOwnerDocument(), step, discProblem, problem, meshSpeciesVars, newPi, variableTemplate, positionTemplate, entry.getKey()));
    	   	}
    		stencilAtt = 1;
    	}
    	for (Entry<String, OperatorsInfo> entry: opParticles.entrySet()) {
            ArrayList<String> particleVars = new ArrayList<String>();
            if (type == OperatorInfoType.field) {
            	particleVars = entry.getValue().getVariablesToInterpolate();
            }
            if (type == OperatorInfoType.auxiliaryField) {
            	particleVars = entry.getValue().getAuxiliaryVariablesToInterpolate();
            }
        	if (particleVars.size() > 0) {
        		ProcessInfo newPi = new ProcessInfo(pi);
        		newPi.setCurrentSpecies(entry.getKey());
        		interpolations.appendChild(createMeshInterpolationCalls(interpolations.getOwnerDocument(), particleVars, newPi, variableTemplate, positionTemplate, entry.getKey()));
        		stencilAtt = 1;
        	}
    	}
        iterOverCells.setAttribute("appendAtt", String.valueOf(accumulatedStencil.get(0)));
        iterOverCells.setAttribute("stencilAtt", String.valueOf(stencilAtt));
        iterOverCells.appendChild(interpolations);
    	return iterOverCells;
    }
    
    /**
     * Creates the mesh interpolation calls from particles.
     * @param doc				Document
     * @param variables			Variables to interpolate
     * @param pi				Process information
     * @param variableTemplate	Variable template
     * @param positionTemplate	Position variable template
     * @param species			Species name
     * @return					Code
     * @throws AGDMException	AGDM00X - External error
     */
    static public Element createMeshInterpolationCalls(Document doc, ArrayList<String> variables, ProcessInfo pi, Element variableTemplate, Element positionTemplate, String species) throws AGDMException {
    	Element iterOverParticles = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "iterateOverParticles");
    	iterOverParticles.setAttribute("speciesNameAtt", species);
    	
    	//Common factors and variables
    	iterOverParticles.appendChild(Interpolation.createLinearLagrangianInterpolationFactors(doc, pi, positionTemplate));
    	for (int i = 0; i < variables.size(); i++) {
    		String variable = variables.get(i);
    		pi.setField(variable + "_particles");
    	  	Element math = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "math");
    		Element apply = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
      		Element eq = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "eq");
      		Element ci = ExecutionFlow.replaceTags(variableTemplate.cloneNode(true), pi);
            Element functionCall = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "functionCall");
            Element functionName = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
            functionName.setTextContent("interpolate_from_mesh");
            pi.setField(variable);
            Element field = null;
     	  	if (!(pi.getRegionInfo().isAuxiliaryField(variable))) {
     	  		field = ExecutionFlow.replaceTags(AGDMUtils.removeIndex(variableTemplate.cloneNode(true)), pi);
     	  	}
     	  	else {
     	  		field = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
     	  		field.setTextContent(variable);
     	  	}
            functionCall.appendChild(functionName);
            functionCall.appendChild(doc.importNode(field, true));
            //Factors
          	for (int j = 0; j < Math.pow(2, pi.getBaseSpatialCoordinates().size()); j++) {
          		Element factor = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
          		factor.setTextContent("factor" + j);
          		functionCall.appendChild(factor);
          	}
            //indices
            for (int j = 0; j < pi.getCoordinateRelation().getDiscCoords().size(); j++) {
                String discCoord = pi.getCoordinateRelation().getDiscCoords().get(j);
                Element index = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
                index.setTextContent(discCoord);
                functionCall.appendChild(index);
            }
      		apply.appendChild(eq);
      		apply.appendChild(doc.importNode(ci, true));
      		apply.appendChild(functionCall);
      		math.appendChild(apply);
      		iterOverParticles.appendChild(math);
    	}
    	return iterOverParticles;
    }
    
    /**
     * Creates the particle interpolation calls from mesh.
     * @param doc				Document
     * @param step				Current step
     * @param discProblem		Discrete problem
     * @param problem			problem
     * @param variables			Variables to interpolate
     * @param pi				Process information
     * @param variableTemplate	Variable template
     * @param positionTemplate	Position variable template
     * @param speciesName		Current particle species
     * @return					Code
     * @throws AGDMException	AGDM00X - External error
     */
    private DocumentFragment createParticleInterpolationCalls(Document doc, Element step, Document discProblem, 
            Node problem, ArrayList<String> variables, ProcessInfo pi, Element variableTemplate, Element positionTemplate, String speciesName) throws AGDMException {
    	Element iterOverParticles = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "iterateOverParticlesFromCell");
    	iterOverParticles.setAttribute("speciesNameAtt", speciesName);
    	iterOverParticles.setAttribute("compactSupportRatioAtt", String.valueOf(pi.getCompactSupportRatio()));
     	//Add particle position
        Element particlePosition = createPositionReference(doc, getPreviousPositionVariable(doc, step), pi);
    	iterOverParticles.appendChild(particlePosition);
    	
    	DocumentFragment result = doc.createDocumentFragment();
    	DocumentFragment variableInitialization = doc.createDocumentFragment();
    	DocumentFragment variableNormalization = doc.createDocumentFragment();
    	
    	String normalizationPrefix = "";
    	NodeList discretization = AGDMUtils.find(step, ".//mms:particleInterpolationMethods/mms:particleNormalizationFunction");
        if (discretization.getLength() > 0) {
	        String schemaId = discretization.item(0).getFirstChild().getLastChild().getTextContent();
	        normalizationPrefix = loadSchema(doc, schemaId, discProblem, problem, pi, discretization.item(0).getFirstChild().getFirstChild().getTextContent());
        }
        else {
			throw new AGDMException(AGDMException.AGDM006, "Particle interpolation normalization must be defined in the schema if the model have source terms.");
        }
    	
        discretization = step.getElementsByTagName("mms:particleInterpolationFunction");
        if (discretization.getLength() > 0) {
	        String schemaId = discretization.item(0).getFirstChild().getLastChild().getTextContent();
	        String functionNamePrefix = loadSchema(doc, schemaId, discProblem, problem, pi, discretization.item(0).getFirstChild().getFirstChild().getTextContent());

            //Normalization accumulation
    		Element math = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "math");
    		Element apply = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
    		Element eq = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "eq");
    		Element accInterp = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
        	accInterp.setTextContent("interpolation_normalization");
      		Element applyPlus = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
      		Element plus = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "plus");
      		ArrayList<Element> inputVariableList = getParticleInterpolationNormalizationInputVariables(step);
        	//RHS
        	Element functionCall = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "functionCall");
        	Element functionName = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
            functionName.setTextContent(normalizationPrefix);
            functionCall.appendChild(functionName);
            for (int j = 0; j < inputVariableList.size(); j++) {
         		NodeList params = ExecutionFlow.replaceTags(inputVariableList.get(j).cloneNode(true), pi).getChildNodes();
         		for (int k = 0; k < params.getLength(); k++) {
         			functionCall.appendChild(doc.importNode(params.item(k).cloneNode(true), true));
         		}
            }
           	math.appendChild(apply);
        	apply.appendChild(eq);
        	apply.appendChild(doc.importNode(accInterp, true));
        	apply.appendChild(applyPlus);
        	applyPlus.appendChild(plus);
        	applyPlus.appendChild(doc.importNode(accInterp.cloneNode(true), true));
        	applyPlus.appendChild(functionCall);
        	iterOverParticles.appendChild(math);
	        
    		//Normalization
    		math = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "math");
    		apply = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
    		eq = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "eq");
        	accInterp = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
        	accInterp.setTextContent("interpolation_normalization");
        	Element cn = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "cn");
        	cn.setTextContent("0");
           	math.appendChild(apply);
        	apply.appendChild(eq);
        	apply.appendChild(doc.importNode(accInterp, true));
        	apply.appendChild(cn);
        	variableInitialization.appendChild(math);
	        inputVariableList = getParticleInterpolationFunctionInputVariables(step);
	    	for (int i = 0; i < variables.size(); i++) {
	    		String variable = variables.get(i);
	    		pi.setField(variable + "_mesh");
	    		//Variable accumulator
	    		math = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "math");
	    		apply = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
	    		eq = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "eq");
	      		Element variableElement = ExecutionFlow.replaceTags(variableTemplate.cloneNode(true), pi);
	        	cn = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "cn");
	        	cn.setTextContent("0");
	           	math.appendChild(apply);
	        	apply.appendChild(eq);
	        	apply.appendChild(doc.importNode(variableElement, true));
	        	apply.appendChild(cn);
	        	variableInitialization.appendChild(math);

	        	//Interpolate
	            math = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "math");
	            apply = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
	            eq = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "eq");
	            apply.appendChild(eq);
	            //LHS
	            apply.appendChild(doc.importNode(variableElement.cloneNode(true), true));
	            //RHS
	            functionCall = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "functionCall");
	            functionName = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
	            functionName.setTextContent(functionNamePrefix);
	            functionCall.appendChild(functionName);
	            pi.setField(variable);
	            for (int j = 0; j < inputVariableList.size(); j++) {
             		NodeList params = ExecutionFlow.replaceTags(inputVariableList.get(j).cloneNode(true), pi).getChildNodes();
             		for (int k = 0; k < params.getLength(); k++) {
             			functionCall.appendChild(doc.importNode(params.item(k).cloneNode(true), true));
             		}
	            }
	        	Element applyAcc = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
	        	plus = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "plus");
	        	applyAcc.appendChild(plus);
	        	applyAcc.appendChild(doc.importNode(variableElement.cloneNode(true), true));
	        	applyAcc.appendChild(functionCall);
	        	apply.appendChild(applyAcc);
	            math.appendChild(apply);
	            iterOverParticles.appendChild(math);
	      		
	      		//Normalization
	      		math = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "math");
	            apply = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
	            eq = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "eq");
	      		Element nonConsNormalization = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
	        	Element divide = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "divide");
	           	Element normVar = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
	           	normVar.setTextContent("interpolation_normalization");
	        	nonConsNormalization.appendChild(divide);
	        	nonConsNormalization.appendChild(doc.importNode(variableElement.cloneNode(true), true));
	        	nonConsNormalization.appendChild(normVar);
	        	math.appendChild(apply);
	        	apply.appendChild(eq);
	        	apply.appendChild(doc.importNode(variableElement.cloneNode(true), true));
	        	apply.appendChild(nonConsNormalization);
	        	variableNormalization.appendChild(math);
	    	}
	    	
	    	result.appendChild(variableInitialization);
	    	result.appendChild(iterOverParticles);
	    	//Securization of interpolation_normalization
	    	Element ifE = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "if");
    		Element then = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "then");
    		Element mathCond = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "math");
    		Element applyCond = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
    		Element lt = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "lt");
    		Element abs = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "abs");
           	Element varName = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
        	varName.setTextContent("interpolation_normalization");
        	cn = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "cn");
        	cn.setTextContent("0.000000000000001");
        	Element applyAbs = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
        	ifE.appendChild(then);
        	ifE.appendChild(mathCond);
        	ifE.appendChild(then);
        	mathCond.appendChild(applyCond);
        	applyCond.appendChild(lt);
        	applyCond.appendChild(applyAbs);
        	applyCond.appendChild(cn);
        	applyAbs.appendChild(abs);
        	applyAbs.appendChild(varName);
    		math = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "math");
    		apply = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
    		eq = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "eq");
           	math.appendChild(apply);
        	apply.appendChild(eq);
        	apply.appendChild(varName.cloneNode(true));
        	apply.appendChild(cn.cloneNode(true));
        	then.appendChild(math);
        	result.appendChild(ifE);
	    	result.appendChild(variableNormalization);
	    	
	    	return result;
        }
        else {
        	throw new AGDMException(AGDMException.AGDM006, "Particle interpolation function must be defined in the schema if particle fields are in the RHS of mesh fields.");
        }
    }
    
    /**
     * Loads the kernel and its gradient in the problem.
     * @param problem			Problem
     * @param discProblem		Discrete problem
     * @param step				Current discretization step
     * @param pi				Problem information
     * @throws AGDMException	AGDM00X - External error
     */
    private void loadKernel(Node problem, Document discProblem, Element step, ProcessInfo pi) throws AGDMException {
    	Document doc = problem.getOwnerDocument();
    	NodeList kernels = step.getElementsByTagNameNS(AGDMUtils.mmsUri, "particleKernelDiscretization");
    	if (kernels.getLength() > 0) {
    		//Load kernel
    		String kernelId = kernels.item(0).getFirstChild().getFirstChild().getLastChild().getTextContent();
    		String kernelName = kernels.item(0).getFirstChild().getFirstChild().getFirstChild().getTextContent();
    		kernelName = loadSchema(doc, kernelId, discProblem, problem, pi, kernelName);
    		
    		Element functionCall = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "functionCall");
            Element functionName = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
            functionName.setTextContent(kernelName);
            functionCall.appendChild(functionName);
            
        	ArrayList<Element> addVars = getKernelInputVariables((Element) pi.getCurrentTimeStep());
        	for (int i = 0; i < addVars.size(); i++) {
          		Element input = (Element) addVars.get(i).cloneNode(true);
        		functionCall.appendChild(doc.importNode(ExecutionFlow.replaceTags(input.getFirstChild(), pi), true));
        	}
        	pi.setKernel(functionCall);
                		
    		//Load kernel gradient
    		String kernelGradientId = kernels.item(0).getLastChild().getFirstChild().getLastChild().getTextContent();
    		String kernelGradientName = kernels.item(0).getLastChild().getFirstChild().getFirstChild().getTextContent();
    		kernelGradientName = loadSchema(doc, kernelGradientId, discProblem, problem, pi, kernelGradientName);
    		
    		functionCall = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "functionCall");
            functionName = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
            functionName.setTextContent(kernelGradientName);
            functionCall.appendChild(functionName);
            
        	addVars = getKernelGradientInputVariables((Element) pi.getCurrentTimeStep());
        	for (int i = 0; i < addVars.size(); i++) {
        		Element input = (Element) addVars.get(i).cloneNode(true);
        		functionCall.appendChild(doc.importNode(ExecutionFlow.replaceTags(input.getFirstChild(), pi), true));
        	}
        	pi.setKernelGradient(functionCall);
    	}
    }
    
    /**
     * Creates instructions to avoid nans in normalizations.
     * @param doc						Document
     * @param step						Current step
     * @param pi						Problem information
     * @param opInfo					Operators info
     * @param currentLevel				Current derivative level
     * @param lastLevel					If this is the last derivative level
     * @param hasDissipation			If the step has dissipation
     * @return							Code
     * @throws AGDMException			AGDM00X - External error
     */
    private DocumentFragment securizeNormalizations(Document doc, Element step, ProcessInfo pi, OperatorsInfo opInfo, int currentLevel,
    		boolean lastLevel, boolean hasDissipation) throws AGDMException {
    	DocumentFragment result = doc.createDocumentFragment();
    	ArrayList<ParticleNormalization> normalizationInfo = new ArrayList<ParticleNormalization>();
        HashMap<String, Integer> suffixes = new HashMap<String, Integer>();

        //RHS advective terms
        if (lastLevel && pi.isAnyEulerianParticle()) {
		    NodeList discretization = AGDMUtils.find(step, ".//mms:particleAdvectiveTerm//mms:particleNormalizationFunction");
		    if (discretization.getLength() > 0) {
		    	String schemaId = discretization.item(0).getFirstChild().getLastChild().getTextContent();
		    	ArrayList<Element> inputVariableList = getAdvectiveNormalizationFunctionInputVariables(step);
		    	ParticleNormalization normalization = new ParticleNormalization(schemaId, inputVariableList);
		    	if (!normalizationInfo.contains(normalization)) {
		    		normalizationInfo.add(normalization);
		     		//Function name suffix
		    		String suffix = "";
		     		if (normalization.isSpatialDependent()) {
		     	        for (int i = 0; i < pi.getBaseSpatialCoordinates().size(); i++) {
		    	        	String coord = pi.getBaseSpatialCoordinates().get(i);
		    	        	pi.setSpatialCoord1(coord);
		    	        	pi.setSpatialCoord2(null);
		    	        	suffix = "_" + coord;
		            		//Check not existing name
		            		if (!suffixes.containsKey(suffix)) {
		            			suffixes.put(suffix, 0);
		            		}
		            		else {
		            			suffixes.put(suffix, suffixes.get(suffix) + 1);
		            			suffix = suffix + (suffixes.get(suffix) + 1);
		            		}
				        	result.appendChild(createNormalizationSecurization(doc, "derivative_normalization" + suffix));
		     	        }
		     		}
		     		else {
		     			//Check not existing name
		        		if (!suffixes.containsKey(suffix)) {
		        			suffixes.put(suffix, 0);
		        		}
		        		else {
		        			suffixes.put(suffix, suffixes.get(suffix) + 1);
		        			suffix = suffix + (suffixes.get(suffix) + 1);
		        		}
			        	result.appendChild(createNormalizationSecurization(doc, "derivative_normalization" + suffix));
		     		}
		    	}
		    }
		}
        
        
        //RHS conservative
        if (lastLevel) {
            NodeList discretization = AGDMUtils.find(step, ".//mms:conservativeTermDiscretization//mms:normalizationFunction");
            if (discretization.getLength() > 0) {
            	String schemaId = discretization.item(0).getFirstChild().getLastChild().getTextContent();
            	ArrayList<Element> inputVariableList = getParticleConservativeDerivativeNormalizationInputVariables(step);
            	ParticleNormalization normalization = new ParticleNormalization(schemaId, inputVariableList);
            	if (!normalizationInfo.contains(normalization)) {
            		normalizationInfo.add(normalization);
            		//Function name suffix
            		String suffix = "";
            		if (normalization.isSpatialDependent()) {
            			ArrayList<Flux> fluxes = opInfo.getFluxTerms().getFluxes();
            		    for (int i = 0; i < fluxes.size(); i++) {
            		    	String coord = fluxes.get(i).getCoordinate();
	        	        	pi.setSpatialCoord1(coord);
	        	        	pi.setSpatialCoord2(null);
	        	    		suffix = "_" + coord;
	                		//Check not existing name
	                		if (!suffixes.containsKey(suffix)) {
	                			suffixes.put(suffix, 0);
	                		}
	                		else {
	                			suffixes.put(suffix, suffixes.get(suffix) + 1);
	                			suffix = suffix + (suffixes.get(suffix) + 1);
	                		}
				        	result.appendChild(createNormalizationSecurization(doc, "derivative_normalization" + suffix));
            		    }
            		}
            		else {
                		//Check not existing name
                		if (!suffixes.containsKey(suffix)) {
                			suffixes.put(suffix, 0);
                		}
                		else {
                			suffixes.put(suffix, suffixes.get(suffix) + 1);
                			suffix = suffix + (suffixes.get(suffix) + 1);
                		}
			        	result.appendChild(createNormalizationSecurization(doc, "derivative_normalization" + suffix));
    			    }
            	}
            }
        }
        
	    //RHS non conservative
        if (opInfo.getNormalizationLevels().size() > currentLevel) {
        	//Group same derivative coordinate variables
	    	HashMap<String, ArrayList<String>> norm = opInfo.getNormalizationLevels().get(currentLevel).getCoordVariablesMap();
	    	Iterator<String> it = norm.keySet().iterator();
	    	while (it.hasNext()) {
	    		String key = it.next();
	    		//Only simple coordinate derivatives
	    		if (!key.contains("_")) {
	    			String coords = key.replaceAll("[-0-9]*", "");
		    		pi.setSpatialCoord1(coords.substring(0,  1));
		    		if (coords.length() > 1) {
			    		pi.setSpatialCoord2(coords.substring(1,  2));
		    		}
	    	        String schemaId = opInfo.getNormalizationLevels().get(currentLevel).getCoordSchemaIdMap().get(key).get(0);
	            	ArrayList<Element> inputVariableList = getParticleNonConservativeDerivativeNormalizationInputVariables(step);
	            	ParticleNormalization normalization = new ParticleNormalization(schemaId, inputVariableList);
	            	if (!normalizationInfo.contains(normalization)) {
	            		normalizationInfo.add(normalization);
	            		//Function name suffix
	            		String suffix = "";
	            		if (normalization.isSpatialDependent()) {
	            			suffix = "_" + key;
	            		}
	            		//Check not existing name
	            		if (!suffixes.containsKey(suffix)) {
	            			suffixes.put(suffix, 0);
	            		}
	            		else {
	            			suffixes.put(suffix, suffixes.get(suffix) + 1);
	            			suffix = suffix + (suffixes.get(suffix) + 1);
	            		}
	            		//Formula
			        	result.appendChild(createNormalizationSecurization(doc, "derivative_normalization" + suffix));
			        }
	    		}
	    	}
        }
        
        //Dissipation terms
        if (hasDissipation) {
        	NodeList discretization = AGDMUtils.find(step, ".//mms:particleDissipation//mms:normalizationFunction");
            if (discretization.getLength() > 0) {
            	String schemaId = discretization.item(0).getFirstChild().getLastChild().getTextContent();
            	ArrayList<Element> inputVariableList = getParticleDissipationNormalizationInputVariables(step);
            	ParticleNormalization normalization = new ParticleNormalization(schemaId, inputVariableList);
            	if (!normalizationInfo.contains(normalization)) {
            		normalizationInfo.add(normalization);
            		//Function name suffix
            		String suffix = "";
            		if (normalization.isSpatialDependent()) {
            			ArrayList<Flux> fluxes = opInfo.getFluxTerms().getFluxes();
            		    for (int i = 0; i < fluxes.size(); i++) {
            		    	String coord = fluxes.get(i).getCoordinate();
	        	        	pi.setSpatialCoord1(coord);
	        	    		suffix = "_" + coord;
	                		//Check not existing name
	                		if (!suffixes.containsKey(suffix)) {
	                			suffixes.put(suffix, 0);
	                		}
	                		else {
	                			suffixes.put(suffix, suffixes.get(suffix) + 1);
	                			suffix = suffix + (suffixes.get(suffix) + 1);
	                		}
				        	result.appendChild(createNormalizationSecurization(doc, "derivative_normalization" + suffix));
            		    }
            		}
            		else {
                		//Check not existing name
                		if (!suffixes.containsKey(suffix)) {
                			suffixes.put(suffix, 0);
                		}
                		else {
                			suffixes.put(suffix, suffixes.get(suffix) + 1);
                			suffix = suffix + (suffixes.get(suffix) + 1);
                		}
			        	result.appendChild(createNormalizationSecurization(doc, "derivative_normalization" + suffix));
    			    }
            	}
            }
        }
    
        //RHS sources
        boolean nonConsSources = false;
        if (opInfo.getDerivativeLevels().size() > currentLevel) {
            ArrayList<Derivative> derivatives = opInfo.getDerivativeLevels().get(currentLevel).getDerivatives();
            for (int i = 0; i < derivatives.size() && !nonConsSources; i++) {
                if (!derivatives.get(i).getDerivativeTerm().isPresent()) {
                	nonConsSources = true;
                }
            }
        }
        if (nonConsSources || (lastLevel && opInfo.getSourceTerms().getSources().size() > 0)) {        	
        	Element ifE = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "if");
    		Element then = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "then");
    		Element mathCond = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "math");
    		Element applyCond = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
    		Element lt = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "lt");
    		Element abs = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "abs");
           	Element varName = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
        	varName.setTextContent("interpolation_normalization");
        	Element cn = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "cn");
        	cn.setTextContent("0.000000000000001");
        	Element applyAbs = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
        	ifE.appendChild(then);
        	ifE.appendChild(mathCond);
        	ifE.appendChild(then);
        	mathCond.appendChild(applyCond);
        	applyCond.appendChild(lt);
        	applyCond.appendChild(applyAbs);
        	applyCond.appendChild(cn);
        	applyAbs.appendChild(abs);
        	applyAbs.appendChild(varName);
    		Element math = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "math");
    		Element apply = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
    		Element eq = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "eq");
           	math.appendChild(apply);
        	apply.appendChild(eq);
        	apply.appendChild(varName.cloneNode(true));
        	apply.appendChild(cn.cloneNode(true));
        	then.appendChild(math);
        	result.appendChild(ifE);
        }
        return result;
    }
    
    /**
     * Create the normalization securization code.
     * @param doc				Document
     * @param normVarName		Variable to securize
     * @return					Code.
     */
    private Element createNormalizationSecurization(Document doc, String normVarName) {
    	Element ifE = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "if");
		Element then = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "then");
		Element mathCond = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "math");
		Element applyCond = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
		Element lt = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "lt");
		Element abs = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "abs");
       	Element varName = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
    	varName.setTextContent(normVarName);
    	Element cn = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "cn");
    	cn.setTextContent("0.000000000000001");
    	Element applyAbs = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
    	ifE.appendChild(then);
    	ifE.appendChild(mathCond);
    	ifE.appendChild(then);
    	mathCond.appendChild(applyCond);
    	applyCond.appendChild(lt);
    	applyCond.appendChild(applyAbs);
    	applyCond.appendChild(cn);
    	applyAbs.appendChild(abs);
    	applyAbs.appendChild(varName);
		Element math = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "math");
		Element apply = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
		Element eq = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "eq");
       	math.appendChild(apply);
    	apply.appendChild(eq);
    	apply.appendChild(varName.cloneNode(true));
    	apply.appendChild(cn.cloneNode(true));
    	then.appendChild(math);
    	return ifE;
    }
    
    /**
     * Normalization of RHS functions.
     * @param doc						Problem
     * @param step						Current schema step
     * @param pi						Process information
     * @param spatialOutputVariable		variable template
     * @param opInfo					Operators info
     * @param currentLevel				Current derivative level
     * @param normalizationInfo			Normalization information
     * @param lastLoop					If this calculates in the derivative last loop
     * @param hasDissipation			If the step has dissipation
     * @param withAuxVarNorm			If the auxiliary variable derivatives must be normalized
     * @return							Code
     * @throws AGDMException			AGDM00X - External error
     */
    private DocumentFragment createNormalizeRHSFunctions(Document doc, Element step, ProcessInfo pi, Element spatialOutputVariable, OperatorsInfo opInfo, int currentLevel,
    		 ArrayList<ParticleNormalization> normalizationInfo, ArrayList<NormalizationLevel> normalizationLevel, boolean lastLoop, boolean hasDissipation, boolean withAuxVarNorm) throws AGDMException {
    	DocumentFragment result = doc.createDocumentFragment();
    	if (lastLoop) {
    	    ArrayList<String> fields = pi.getParticleFields();
            for (int j = 0; j < fields.size(); j++) {
            	String field = fields.get(j);
                if (!pi.getRegionInfo().isAuxiliaryField(field)) {
                    ProcessInfo newPi = new ProcessInfo(pi);
                    newPi.setField(field);
                    if (pi.isEulerianField(field)) {
	                    //RHS advective terms
			    		for (int i = 0; i < pi.getBaseSpatialCoordinates().size(); i++) {
			            	String coord = pi.getBaseSpatialCoordinates().get(i);
			                newPi.setSpatialCoord1(coord);
			                Element consNormalization = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
			               	Element divide = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "divide");
			                Element variableName = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
			                variableName.setTextContent("RHS_particle_advective_term_" + field + "_" + coord);

			    		    NodeList discretization = AGDMUtils.find(step, ".//mms:particleAdvectiveTerm//mms:particleNormalizationFunction");
				        	String schemaId = discretization.item(0).getFirstChild().getLastChild().getTextContent();
			            	ArrayList<Element> inputVariableList = getAdvectiveNormalizationFunctionInputVariables(step);
				        	ParticleNormalization normalization = new ParticleNormalization(schemaId, inputVariableList);
				        	int normIndex = normalizationInfo.indexOf(normalization);
				        	String suffix = "";
				        	if (normalization.isSpatialDependent()) {
				        		suffix = "_" + coord;
				        	}
			               	Element normVar = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
			               	normVar.setTextContent("derivative_normalization" + normalizationInfo.get(normIndex).getSuffixValue(suffix));
			                
			            	consNormalization.appendChild(divide);
			            	consNormalization.appendChild(doc.importNode(variableName, true));
			            	consNormalization.appendChild(normVar);
			            	
			                Element math = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "math");
		                    Element apply = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
		                    Element eq = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "eq");
		                    apply.appendChild(eq);
		                    apply.appendChild(variableName.cloneNode(true));
		                    apply.appendChild(consNormalization);
		                    math.appendChild(apply);
		                    result.appendChild(math);
			            }
                    }
		    		//RHS conservative
		            ArrayList<Flux> fluxes = opInfo.getFluxTerms().getFilteredFluxTerms(field);
		            for (int i = 0; i < fluxes.size(); i++) {
		            	String coord = fluxes.get(i).getCoordinate();
		                newPi.setSpatialCoord1(coord);
		                Element consNormalization = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
		               	Element divide = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "divide");
		            	Element variableName = ExecutionFlow.replaceTags(spatialOutputVariable.cloneNode(true), newPi);
		            	AGDMUtils.addSuffix(variableName, "_Cons_" + coord);

		                NodeList discretization = AGDMUtils.find(step, ".//mms:conservativeTermDiscretization//mms:normalizationFunction");
	                	String schemaId = discretization.item(0).getFirstChild().getLastChild().getTextContent();
	                	ArrayList<Element> inputVariableList = getParticleConservativeDerivativeNormalizationInputVariables(step);
	                	ParticleNormalization normalization = new ParticleNormalization(schemaId, inputVariableList);
	    	        	int normIndex = normalizationInfo.indexOf(normalization);
			        	String suffix = "";
			        	if (normalization.isSpatialDependent()) {
			        		suffix = "_" + coord;
			        	}
		               	Element normVar = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
		               	normVar.setTextContent("derivative_normalization" + normalizationInfo.get(normIndex).getSuffixValue(suffix));
		                
		            	consNormalization.appendChild(divide);
		            	consNormalization.appendChild(doc.importNode(variableName, true));
		            	consNormalization.appendChild(normVar);
		     
		                Element math = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "math");
	                    Element apply = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
	                    Element eq = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "eq");
	                    apply.appendChild(eq);
	                    apply.appendChild(doc.importNode(variableName.cloneNode(true), true));
	                    apply.appendChild(consNormalization);
	                    math.appendChild(apply);
	                    result.appendChild(math);
		            }
		            //DissipativeTerms
		            if (hasDissipation) {
		            	for (int i = 0; i < pi.getBaseSpatialCoordinates().size(); i++) {
			            	String coord = pi.getBaseSpatialCoordinates().get(i);
			                newPi.setSpatialCoord1(coord);
			                Element consNormalization = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
			               	Element divide = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "divide");
			                Element variableName = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
			                variableName.setTextContent("RHS_dissipative_term_" + field + "_" + coord);

			    		    NodeList discretization = AGDMUtils.find(step, ".//mms:particleAdvectiveTerm//mms:particleNormalizationFunction");
				        	String schemaId = discretization.item(0).getFirstChild().getLastChild().getTextContent();
			            	ArrayList<Element> inputVariableList = getParticleDissipationNormalizationInputVariables(step);
				        	ParticleNormalization normalization = new ParticleNormalization(schemaId, inputVariableList);
				        	int normIndex = normalizationInfo.indexOf(normalization);
				        	String suffix = "";
				        	if (normalization.isSpatialDependent()) {
				        		suffix = "_" + coord;
				        	}
			               	Element normVar = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
			               	normVar.setTextContent("derivative_normalization" + normalizationInfo.get(normIndex).getSuffixValue(suffix));
			                
			            	consNormalization.appendChild(divide);
			            	consNormalization.appendChild(doc.importNode(variableName, true));
			            	consNormalization.appendChild(normVar);
			            	
			                Element math = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "math");
		                    Element apply = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
		                    Element eq = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "eq");
		                    apply.appendChild(eq);
		                    apply.appendChild(variableName.cloneNode(true));
		                    apply.appendChild(consNormalization);
		                    math.appendChild(apply);
		                    result.appendChild(math);
			            }
		            }
                }
            }
            //RHS sources
            ArrayList<Source> sources = opInfo.getSourceTerms().getSources();
	        for (int i = 0; i < sources.size(); i++) {
	        	Element sourcesFormula = sources.get(i).getFormula();
	        	if (!sourcesFormula.getFirstChild().getLocalName().equals("cn") || !sourcesFormula.getFirstChild().getTextContent().equals("0")) {
		        	String field = sources.get(i).getField();
		            ProcessInfo newPi = new ProcessInfo(pi);
		            newPi.setField(field);
		            
		            //Create mathml call
		            Element math = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "math");
		            Element apply = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
		            Element eq = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "eq");
		            //LHS
		            Element lhs = ExecutionFlow.replaceTags(spatialOutputVariable.cloneNode(true), newPi);
                	AGDMUtils.addSuffix(lhs, "_sources");
		            //RHS
	               	Element normVar = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
	               	normVar.setTextContent("interpolation_normalization");           
	               	Element sourcesNorm = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
	            	Element divide = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "divide");
	            	sourcesNorm.appendChild(divide);
	            	sourcesNorm.appendChild(doc.importNode(lhs.cloneNode(true), true));
	            	sourcesNorm.appendChild(normVar);
                    apply.appendChild(eq);
                    apply.appendChild(doc.importNode(doc.importNode(lhs, true), true));
                    apply.appendChild(sourcesNorm);
                    math.appendChild(apply);
                    result.appendChild(math);
	        	}
	        }
    	}
        //RHS non conservative
        if (normalizationLevel.size() > 0) {
        	//Group same derivative coordinate variables
        	HashMap<String, ArrayList<String>> norm = normalizationLevel.get(currentLevel).getCoordVariablesMap();
        	Iterator<String> it = norm.keySet().iterator();
        	while (it.hasNext()) {
        		String key = it.next();
        		ArrayList<String> variableFilter = norm.get(key);
                for (int i = 0; i < variableFilter.size(); i++) {
                	if (withAuxVarNorm || opInfo.getDerivativeLevels().get(currentLevel).getFilteredDerivatives(variableFilter.get(i)).getOperatorType() != OperatorInfoType.auxiliaryVariable) {
		        		Element nonConsNormalization = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
	                	Element divide = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "divide");
	                	Element variable = null;
	                    boolean localVariable = checkLocalDerivativeTerm(doc, opInfo.getMultiplicationLevels(), opInfo.getDerivativeLevels(), variableFilter.get(i), currentLevel);
	                    if (lastLoop || localVariable) {
	                    	variable = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
	                    	variable.setTextContent(variableFilter.get(i));
	                    }
	                    else {
	                    	variable = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
	                        Element ci = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
	                        ci.setTextContent(variableFilter.get(i));
	                        variable.appendChild(ci);
	                        Element cells = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "currentCell");
	                        variable.appendChild(cells);
	                    }
	                                        
		    	        String schemaId = normalizationLevel.get(currentLevel).getCoordSchemaIdMap().get(key).get(0);
		            	ArrayList<Element> inputVariableList = getParticleNonConservativeDerivativeNormalizationInputVariables(step);
		            	ParticleNormalization normalization = new ParticleNormalization(schemaId, inputVariableList);
	    	        	int normIndex = normalizationInfo.indexOf(normalization);
			        	String suffix = "";
			        	if (normalization.isSpatialDependent()) {
			        		suffix = "_" + key;
			        	}
	                   	Element normVar = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
	                   	normVar.setTextContent("derivative_normalization" + normalizationInfo.get(normIndex).getSuffixValue(suffix));
	                	
	                	nonConsNormalization.appendChild(divide);
	                	nonConsNormalization.appendChild(doc.importNode(variable.cloneNode(true), true));
	                	nonConsNormalization.appendChild(normVar);
	                	
	                    Element math = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "math");
	                    Element apply = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
	                    Element eq = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "eq");
	                    apply.appendChild(eq);
	                    apply.appendChild(variable.cloneNode(true));
	                    apply.appendChild(nonConsNormalization);
	                    math.appendChild(apply);
	                    result.appendChild(math);
	                }
	        	}
	        }

        }
        return result;
    }
    
    
    /**
     * Normalization of Auxiliary variable RHS functions.
     * @param doc						Problem
     * @param step						Current schema step
     * @param opInfo					Operators info
     * @param currentLevel				Current derivative level
     * @param normalizationInfo			Normalization information
     * @return							Code
     * @throws AGDMException			AGDM00X - External error
     */
    private DocumentFragment createNormalizeAuxiliaryVarRHSFunctions(Document doc, Element step, OperatorsInfo opInfo, int currentLevel,
    		 ArrayList<ParticleNormalization> normalizationInfo) throws AGDMException {
    	DocumentFragment result = doc.createDocumentFragment();

        //RHS non conservative
        if (opInfo.getNormalizationLevels().size() > 0) {
        	//Group same derivative coordinate variables
        	HashMap<String, ArrayList<String>> norm = opInfo.getNormalizationLevels().get(currentLevel).getCoordVariablesMap();
        	Iterator<String> it = norm.keySet().iterator();
        	while (it.hasNext()) {
        		String key = it.next();
        		ArrayList<String> variableFilter = norm.get(key);
                for (int i = 0; i < variableFilter.size(); i++) {
                	if (opInfo.getDerivativeLevels().get(currentLevel).getFilteredDerivatives(variableFilter.get(i)).getOperatorType() == OperatorInfoType.auxiliaryVariable) {
		        		Element nonConsNormalization = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
	                	Element divide = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "divide");
	                	Element variable = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
	                    variable.setTextContent(variableFilter.get(i));
		    	        String schemaId = opInfo.getNormalizationLevels().get(currentLevel).getCoordSchemaIdMap().get(key).get(0);
		            	ArrayList<Element> inputVariableList = getParticleNonConservativeDerivativeNormalizationInputVariables(step);
		            	ParticleNormalization normalization = new ParticleNormalization(schemaId, inputVariableList);
	    	        	int normIndex = normalizationInfo.indexOf(normalization);
			        	String suffix = "";
			        	if (normalization.isSpatialDependent()) {
			        		suffix = "_" + key;
			        	}
	                   	Element normVar = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
	                   	normVar.setTextContent("derivative_normalization" + normalizationInfo.get(normIndex).getSuffixValue(suffix));
	                	
	                	nonConsNormalization.appendChild(divide);
	                	nonConsNormalization.appendChild(doc.importNode(variable.cloneNode(true), true));
	                	nonConsNormalization.appendChild(normVar);
	                	
	                    Element math = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "math");
	                    Element apply = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
	                    Element eq = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "eq");
	                    apply.appendChild(eq);
	                    apply.appendChild(variable.cloneNode(true));
	                    apply.appendChild(nonConsNormalization);
	                    math.appendChild(apply);
	                    result.appendChild(math);
	                }
	        	}
	        }
        }
        return result;
    }
    
    /**
     * Initialization of normalization variables for current level.
     * @param doc						Problem
     * @param step						Current schema step
     * @param pi						Process information
     * @param spatialOutputVariable		variable template
     * @param opInfo					Operators info
     * @param currentLevel				Current level
     * @param opType					Operator type
     * @param lastLevel					if current level is the last one
     * @param hasDissipation			If the step has dissipation
     * @return							Code
     * @throws AGDMException			AGDM00X - External error
     */
    private DocumentFragment initializeNormalizationVars(Document doc, Element step, ProcessInfo pi, Element spatialOutputVariable, OperatorsInfo opInfo, int currentLevel,
    		OperatorInfoType opType, boolean lastLevel, boolean hasDissipation) throws AGDMException {
    	DocumentFragment result = doc.createDocumentFragment();
    	ArrayList<ParticleNormalization> normalizationInfo = new ArrayList<ParticleNormalization>();
        HashMap<String, Integer> suffixes = new HashMap<String, Integer>();
        //RHS advective terms
		if (lastLevel && pi.isAnyEulerianParticle()) {
		    NodeList discretization = AGDMUtils.find(step, ".//mms:particleAdvectiveTerm//mms:particleNormalizationFunction");
		    if (discretization.getLength() > 0) {
		    	String schemaId = discretization.item(0).getFirstChild().getLastChild().getTextContent();
		    	ArrayList<Element> inputVariableList = getAdvectiveNormalizationFunctionInputVariables(step);
		    	ParticleNormalization normalization = new ParticleNormalization(schemaId, inputVariableList);
		    	if (!normalizationInfo.contains(normalization)) {
		    		normalizationInfo.add(normalization);
		     		//Function name suffix
		    		String suffix = "";
		     		if (normalization.isSpatialDependent()) {
		     	        for (int i = 0; i < pi.getBaseSpatialCoordinates().size(); i++) {
		    	        	String coord = pi.getBaseSpatialCoordinates().get(i);
		    	        	pi.setSpatialCoord1(coord);
		    	        	suffix = "_" + coord;
		            		//Check not existing name
		            		if (!suffixes.containsKey(suffix)) {
		            			suffixes.put(suffix, 0);
		            		}
		            		else {
		            			suffixes.put(suffix, suffixes.get(suffix) + 1);
		            			suffix = suffix + (suffixes.get(suffix) + 1);
		            		}
		               		Element math = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "math");
				    		Element apply = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
				    		Element eq = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "eq");
				           	Element accDer = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
				        	accDer.setTextContent("derivative_normalization" + suffix);
				        	Element cn = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "cn");
				        	cn.setTextContent("0");
				           	math.appendChild(apply);
				        	apply.appendChild(eq);
				        	apply.appendChild(doc.importNode(accDer, true));
				        	apply.appendChild(cn);
				        	result.appendChild(math);
		     	        }
		     		}
		     		else {
		     			//Check not existing name
		        		if (!suffixes.containsKey(suffix)) {
		        			suffixes.put(suffix, 0);
		        		}
		        		else {
		        			suffixes.put(suffix, suffixes.get(suffix) + 1);
		        			suffix = suffix + (suffixes.get(suffix) + 1);
		        		}
		        		Element math = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "math");
			    		Element apply = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
			    		Element eq = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "eq");
			           	Element accDer = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
			        	accDer.setTextContent("derivative_normalization" + suffix);
			        	Element cn = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "cn");
			        	cn.setTextContent("0");
			           	math.appendChild(apply);
			        	apply.appendChild(eq);
			        	apply.appendChild(doc.importNode(accDer, true));
			        	apply.appendChild(cn);
			        	result.appendChild(math);
		     		}
		    	}
		    }
		}
		//Dissipative terms
		if (lastLevel && hasDissipation) {
		    NodeList discretization = AGDMUtils.find(step, ".//mms:particleDissipation//mms:normalizationFunction");
		    if (discretization.getLength() > 0) {
		    	String schemaId = discretization.item(0).getFirstChild().getLastChild().getTextContent();
		    	ArrayList<Element> inputVariableList = getParticleDissipationNormalizationInputVariables(step);
		    	ParticleNormalization normalization = new ParticleNormalization(schemaId, inputVariableList);
		    	if (!normalizationInfo.contains(normalization)) {
		    		normalizationInfo.add(normalization);
		     		//Function name suffix
		    		String suffix = "";
		     		if (normalization.isSpatialDependent()) {
		     	        for (int i = 0; i < pi.getBaseSpatialCoordinates().size(); i++) {
		    	        	String coord = pi.getBaseSpatialCoordinates().get(i);
		    	        	pi.setSpatialCoord1(coord);
		    	        	suffix = "_" + coord;
		            		//Check not existing name
		            		if (!suffixes.containsKey(suffix)) {
		            			suffixes.put(suffix, 0);
		            		}
		            		else {
		            			suffixes.put(suffix, suffixes.get(suffix) + 1);
		            			suffix = suffix + (suffixes.get(suffix) + 1);
		            		}
		               		Element math = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "math");
				    		Element apply = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
				    		Element eq = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "eq");
				           	Element accDer = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
				        	accDer.setTextContent("derivative_normalization" + suffix);
				        	Element cn = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "cn");
				        	cn.setTextContent("0");
				           	math.appendChild(apply);
				        	apply.appendChild(eq);
				        	apply.appendChild(doc.importNode(accDer, true));
				        	apply.appendChild(cn);
				        	result.appendChild(math);
		     	        }
		     		}
		     		else {
		     			//Check not existing name
		        		if (!suffixes.containsKey(suffix)) {
		        			suffixes.put(suffix, 0);
		        		}
		        		else {
		        			suffixes.put(suffix, suffixes.get(suffix) + 1);
		        			suffix = suffix + (suffixes.get(suffix) + 1);
		        		}
		        		Element math = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "math");
			    		Element apply = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
			    		Element eq = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "eq");
			           	Element accDer = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
			        	accDer.setTextContent("derivative_normalization" + suffix);
			        	Element cn = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "cn");
			        	cn.setTextContent("0");
			           	math.appendChild(apply);
			        	apply.appendChild(eq);
			        	apply.appendChild(doc.importNode(accDer, true));
			        	apply.appendChild(cn);
			        	result.appendChild(math);
		     		}
		    	}
		    }
		}
        
        //RHS conservative
		if (lastLevel) {
            NodeList discretization = AGDMUtils.find(step, ".//mms:conservativeTermDiscretization//mms:normalizationFunction");
            if (discretization.getLength() > 0) {
            	String schemaId = discretization.item(0).getFirstChild().getLastChild().getTextContent();
            	ArrayList<Element> inputVariableList = getParticleConservativeDerivativeNormalizationInputVariables(step);
            	ParticleNormalization normalization = new ParticleNormalization(schemaId, inputVariableList);
            	if (!normalizationInfo.contains(normalization)) {
            		normalizationInfo.add(normalization);
            		//Function name suffix
            		String suffix = "";
            		if (normalization.isSpatialDependent()) {
            			ArrayList<Flux> fluxes = opInfo.getFluxTerms().getFluxes();
            		    for (int i = 0; i < fluxes.size(); i++) {
            		    	String coord = fluxes.get(i).getCoordinate();
	        	        	pi.setSpatialCoord1(coord);
	        	    		suffix = "_" + coord;
	                		//Check not existing name
	                		if (!suffixes.containsKey(suffix)) {
	                			suffixes.put(suffix, 0);
	                		}
	                		else {
	                			suffixes.put(suffix, suffixes.get(suffix) + 1);
	                			suffix = suffix + (suffixes.get(suffix) + 1);
	                		}
	    	        		Element math = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "math");
				    		Element apply = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
				    		Element eq = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "eq");
				           	Element accDer = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
				        	accDer.setTextContent("derivative_normalization" + suffix);
				        	Element cn = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "cn");
				        	cn.setTextContent("0");
				           	math.appendChild(apply);
				        	apply.appendChild(eq);
				        	apply.appendChild(doc.importNode(accDer, true));
				        	apply.appendChild(cn);
				        	result.appendChild(math);
				        	}
            		}
            		else {
                		//Check not existing name
                		if (!suffixes.containsKey(suffix)) {
                			suffixes.put(suffix, 0);
                		}
                		else {
                			suffixes.put(suffix, suffixes.get(suffix) + 1);
                			suffix = suffix + (suffixes.get(suffix) + 1);
                		}
    	        		Element math = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "math");
			    		Element apply = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
			    		Element eq = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "eq");
			           	Element accDer = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
			        	accDer.setTextContent("derivative_normalization" + suffix);
			        	Element cn = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "cn");
			        	cn.setTextContent("0");
			           	math.appendChild(apply);
			        	apply.appendChild(eq);
			        	apply.appendChild(doc.importNode(accDer, true));
			        	apply.appendChild(cn);
			        	result.appendChild(math);
    			    }
            	}
            }
        }
        
	    //RHS non conservative
		if (opInfo.getNormalizationLevels().size() > currentLevel) {
        	//Group same derivative coordinate variables
	    	HashMap<String, ArrayList<String>> norm = opInfo.getNormalizationLevels().get(currentLevel).getCoordVariablesMap();
	    	Iterator<String> it = norm.keySet().iterator();
	    	while (it.hasNext()) {
	    		String key = it.next();
	    		//Only simple coordinate derivatives
	    		if (!key.contains("_")) {
	    			String coords = key.replaceAll("[-0-9]*", "");
		    		pi.setSpatialCoord1(coords.substring(0,  1));
		    		if (coords.length() > 1) {
			    		pi.setSpatialCoord2(coords.substring(1,  2));
		    		}
	    	        String schemaId = opInfo.getNormalizationLevels().get(currentLevel).getCoordSchemaIdMap().get(key).get(0);
	            	ArrayList<Element> inputVariableList = getParticleNonConservativeDerivativeNormalizationInputVariables(step);
	            	ParticleNormalization normalization = new ParticleNormalization(schemaId, inputVariableList);
	            	if (!normalizationInfo.contains(normalization)) {
	            		normalizationInfo.add(normalization);
	            		//Function name suffix
	            		String suffix = "";
	            		if (normalization.isSpatialDependent()) {
	            			suffix = "_" + key;
	            		}
	            		//Check not existing name
	            		if (!suffixes.containsKey(suffix)) {
	            			suffixes.put(suffix, 0);
	            		}
	            		else {
	            			suffixes.put(suffix, suffixes.get(suffix) + 1);
	            			suffix = suffix + (suffixes.get(suffix) + 1);
	            		}
	            		
	            		//Formula
	             		Element math = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "math");
			    		Element apply = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
			    		Element eq = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "eq");
			           	Element accDer = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
			        	accDer.setTextContent("derivative_normalization" + suffix);
			        	Element cn = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "cn");
			        	cn.setTextContent("0");
			           	math.appendChild(apply);
			        	apply.appendChild(eq);
			        	apply.appendChild(doc.importNode(accDer, true));
			        	apply.appendChild(cn);
			        	result.appendChild(math);
			        }
	    		}
	    	}
        }
    
        //RHS sources or velocity correction
        boolean nonConsSources = false;
        if (opInfo.getDerivativeLevels().size() > currentLevel) {
            ArrayList<Derivative> derivatives = opInfo.getDerivativeLevels().get(currentLevel).getDerivatives();
            for (int i = 0; i < derivatives.size() && !nonConsSources; i++) {
                if (!derivatives.get(i).getDerivativeTerm().isPresent()) {
                	nonConsSources = true;
                }
            }
        }
        if (nonConsSources || (lastLevel && opInfo.getSourceTerms().getSources().size() > 0) || (lastLevel && pi.getParticleVelocities().size() > 0)) {
    		Element math = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "math");
    		Element apply = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
    		Element eq = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "eq");
        	Element accInterp = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
        	accInterp.setTextContent("interpolation_normalization");
        	Element cn = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "cn");
        	cn.setTextContent("0");
           	math.appendChild(apply);
        	apply.appendChild(eq);
        	apply.appendChild(doc.importNode(accInterp, true));
        	apply.appendChild(cn);
        	result.appendChild(math);
        }
        return result;
    }
    
    /**
     * Generates normalization function information.
     * @param doc						Document
     * @param step						Current schema step
     * @param pi						Process information
     * @param opInfo					Operators info
     * @param currentLevel				Current level
     * @param lastLoop					if current level is the last one
     * @param hasDissipation			If the step has dissipation
     * @return							Code
     * @throws AGDMException			AGDM006  Schema not applicable to this problem
     * 									AGDM00X - External error
     */
    private ArrayList<ParticleNormalization> generateNormalizationInfo(Document doc, Element step, ProcessInfo pi, OperatorsInfo opInfo, int currentLevel, boolean lastLoop, boolean hasDissipation) throws AGDMException {
    	ArrayList<ParticleNormalization> normalizationInfo = new ArrayList<ParticleNormalization>();
        HashMap<String, Integer> suffixes = new HashMap<String, Integer>();
    	//RHS advective term
        if (lastLoop && pi.isAnyEulerianParticle()) {
            NodeList discretization = AGDMUtils.find(step, ".//mms:particleAdvectiveTerm//mms:particleNormalizationFunction");
            if (discretization.getLength() > 0) {
	        	String schemaId = discretization.item(0).getFirstChild().getLastChild().getTextContent();
            	ArrayList<Element> inputVariableList = getAdvectiveNormalizationFunctionInputVariables(step);
	        	ParticleNormalization normalization = new ParticleNormalization(schemaId, discretization.item(0).getFirstChild().getFirstChild().getTextContent(), inputVariableList);
	        	if (!normalizationInfo.contains(normalization)) {
	         		//Function name suffix
            		String suffix = "";
             		if (normalization.isSpatialDependent()) {
             	        for (int i = 0; i < pi.getBaseSpatialCoordinates().size(); i++) {
            	        	String coord = pi.getBaseSpatialCoordinates().get(i);
            	        	pi.setSpatialCoord1(coord);
            	        	suffix = "_" + coord;
	                		//Check not existing name
	                		if (!suffixes.containsKey(suffix)) {
	                			suffixes.put(suffix, 0);
	                		}
	                		else {
	                			suffixes.put(suffix, suffixes.get(suffix) + 1);
	                			suffix = suffix + (suffixes.get(suffix) + 1);
	                		}
	                		normalization.addSuffix("_" + coord, suffix);
             	        }
             		}
             		else {
             			//Check not existing name
                		if (!suffixes.containsKey(suffix)) {
                			suffixes.put(suffix, 0);
                		}
                		else {
                			suffixes.put(suffix, suffixes.get(suffix) + 1);
                			suffix = suffix + (suffixes.get(suffix) + 1);
                		}
                		normalization.addSuffix("", suffix);
             		}
	        		normalizationInfo.add(normalization);
	        	}
            }
        }
        
        //RHS conservative
        if (lastLoop) {
            NodeList discretization = AGDMUtils.find(step, ".//mms:conservativeTermDiscretization//mms:normalizationFunction");
            if (discretization.getLength() > 0) {
            	String schemaId = discretization.item(0).getFirstChild().getLastChild().getTextContent();
            	ArrayList<Element> inputVariableList = getParticleConservativeDerivativeNormalizationInputVariables(step);
            	ParticleNormalization normalization = new ParticleNormalization(schemaId, discretization.item(0).getFirstChild().getFirstChild().getTextContent(), inputVariableList);
            	if (!normalizationInfo.contains(normalization)) {
            		//Function name suffix
            		String suffix = "";
            		if (normalization.isSpatialDependent()) {
            			ArrayList<Flux> fluxes = opInfo.getFluxTerms().getFluxes();
            		    for (int i = 0; i < fluxes.size(); i++) {
            		    	String coord = fluxes.get(i).getCoordinate();
	        	        	pi.setSpatialCoord1(coord);
	        	    		suffix = "_" + coord;
	                		//Check not existing name
	                		if (!suffixes.containsKey(suffix)) {
	                			suffixes.put(suffix, 0);
	                		}
	                		else {
	                			suffixes.put(suffix, suffixes.get(suffix) + 1);
	                			suffix = suffix + (suffixes.get(suffix) + 1);
	                		}
	                		normalization.addSuffix("_" + coord, suffix);
            		    }
            		}
            		else {
                		//Check not existing name
                		if (!suffixes.containsKey(suffix)) {
                			suffixes.put(suffix, 0);
                		}
                		else {
                			suffixes.put(suffix, suffixes.get(suffix) + 1);
                			suffix = suffix + (suffixes.get(suffix) + 1);
                		}
                		normalization.addSuffix("", suffix);
            		}
            		normalizationInfo.add(normalization);
            	}
            }
        }
        
	    //RHS non conservative
        if (opInfo.getNormalizationLevels().size() > currentLevel) {
        	//Group same derivative coordinate variables
	    	HashMap<String, ArrayList<String>> norm = opInfo.getNormalizationLevels().get(currentLevel).getCoordVariablesMap();
	    	Iterator<String> it = norm.keySet().iterator();
	    	while (it.hasNext()) {
	    		String key = it.next();
	    		//Only simple coordinate derivatives
	    		if (!key.contains("_")) {
	    			String coords = key.replaceAll("[-0-9]*", "");
		    		pi.setSpatialCoord1(coords.substring(0,  1));
		    		if (coords.length() > 1) {
			    		pi.setSpatialCoord2(coords.substring(1,  2));
		    		}
	    	        String schemaId = opInfo.getNormalizationLevels().get(currentLevel).getCoordSchemaIdMap().get(key).get(0);
	    	    	String schemaName = opInfo.getNormalizationLevels().get(currentLevel).getCoordSchemaNameMap().get(key).get(0);
	            	ArrayList<Element> inputVariableList = getParticleNonConservativeDerivativeNormalizationInputVariables(step);
	            	ParticleNormalization normalization = new ParticleNormalization(schemaId, schemaName, inputVariableList);
	            	if (!normalizationInfo.contains(normalization)) {
	            		//Function name suffix
	            		String suffix = "";
	            		if (normalization.isSpatialDependent()) {
	            			suffix = "_" + key;
	            		}
	            		String keySuffix = suffix;
	            		//Check not existing name
	            		if (!suffixes.containsKey(suffix)) {
	            			suffixes.put(suffix, 0);
	            		}
	            		else {
	            			suffixes.put(suffix, suffixes.get(suffix) + 1);
	            			suffix = suffix + (suffixes.get(suffix) + 1);
	            		}
                		normalization.addSuffix(keySuffix, suffix);
	            		//Formula
	            		normalizationInfo.add(normalization);
	            	}
	    		}
	    	}
        }
        
      //RHS Dissipative terms
        if (lastLoop && hasDissipation) {
            NodeList discretization = AGDMUtils.find(step, ".//mms:particleDissipation//mms:normalizationFunction");
            if (discretization.getLength() > 0) {
            	String schemaId = discretization.item(0).getFirstChild().getLastChild().getTextContent();
            	ArrayList<Element> inputVariableList = getParticleDissipationNormalizationInputVariables(step);
            	ParticleNormalization normalization = new ParticleNormalization(schemaId, discretization.item(0).getFirstChild().getFirstChild().getTextContent(), inputVariableList);
            	if (!normalizationInfo.contains(normalization)) {
            		//Function name suffix
            		String suffix = "";
            		if (normalization.isSpatialDependent()) {
            			ArrayList<Flux> fluxes = opInfo.getFluxTerms().getFluxes();
            		    for (int i = 0; i < fluxes.size(); i++) {
            		    	String coord = fluxes.get(i).getCoordinate();
	        	        	pi.setSpatialCoord1(coord);
	        	    		suffix = "_" + coord;
	                		//Check not existing name
	                		if (!suffixes.containsKey(suffix)) {
	                			suffixes.put(suffix, 0);
	                		}
	                		else {
	                			suffixes.put(suffix, suffixes.get(suffix) + 1);
	                			suffix = suffix + (suffixes.get(suffix) + 1);
	                		}
	                		normalization.addSuffix("_" + coord, suffix);
            		    }
            		}
            		else {
                		//Check not existing name
                		if (!suffixes.containsKey(suffix)) {
                			suffixes.put(suffix, 0);
                		}
                		else {
                			suffixes.put(suffix, suffixes.get(suffix) + 1);
                			suffix = suffix + (suffixes.get(suffix) + 1);
                		}
                		normalization.addSuffix("", suffix);
            		}
            		normalizationInfo.add(normalization);
            	}
            }
        }
        
        return normalizationInfo;
    }
    
    /**
     * Initialization of normalization variables for current level.
     * @param doc						Document
     * @param discProblem				Discretized problem
     * @param step						Current schema step
     * @param problem					Region problem
     * @param pi						Process information
     * @param opInfo					Operators info
     * @param currentLevel				Current level
     * @param normalizationInfo			Normalization info
     * @param lastLoop					if current level is the last one
     * @param hasDissipation			If the step has dissipation
     * @return							Code
     * @throws AGDMException			AGDM006  Schema not applicable to this problem
     * 									AGDM00X - External error
     */
    private DocumentFragment calculateNormalizationFunctions(Document doc, Document discProblem, 
            Node problem, Element step, ProcessInfo pi, OperatorsInfo opInfo, int currentLevel,
    		ArrayList<ParticleNormalization> normalizationInfo, boolean lastLoop) throws AGDMException {
    	DocumentFragment result = doc.createDocumentFragment();

        for (ParticleNormalization normalization: normalizationInfo) {
    		Iterator<String> suffixIt = normalization.getSuffixMap().keySet().iterator();
    		while (suffixIt.hasNext()) {
    			String suffix = normalization.getSuffixMap().get(suffixIt.next());
            	result.appendChild(createNormalizationFunctionCall(doc, normalization.getSchemaId(), normalization.getSchemaName(), suffix, discProblem, problem, pi, normalization.getInputVariables()));
 	        }
        }
	   
        //RHS sources || position correction
        boolean nonConsSources = false;
	    if (opInfo.getNormalizationLevels().size() > currentLevel) {
	        ArrayList<Derivative> derivatives = opInfo.getDerivativeLevels().get(currentLevel).getDerivatives();
	        for (int i = 0; i < derivatives.size() && !nonConsSources; i++) {
	            if (!derivatives.get(i).getDerivativeTerm().isPresent()) {
	            	nonConsSources = true;
	            }
	        }
	    }
        if (nonConsSources || (lastLoop && opInfo.getSourceTerms().getSources().size() > 0) || (lastLoop && pi.getParticleVelocities().size() > 0)) {
            //Load schema
            NodeList discretization = AGDMUtils.find(step, ".//mms:particleInterpolationMethods/mms:particleNormalizationFunction");
            if (discretization.getLength() > 0) {
    	        String schemaId = discretization.item(0).getFirstChild().getLastChild().getTextContent();
    	        String functionNamePrefix = loadSchema(doc, schemaId, discProblem, problem, pi, discretization.item(0).getFirstChild().getFirstChild().getTextContent());
        		Element math = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "math");
        		Element apply = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
        		Element eq = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "eq");
            	Element accInterp = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
            	accInterp.setTextContent("interpolation_normalization");
          		Element applyPlus = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
        		Element plus = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "plus");
            	ArrayList<Element> inputVariableList = getParticleInterpolationNormalizationInputVariables(step);
            	//RHS
	            Element functionCall = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "functionCall");
	            Element functionName = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
	            functionName.setTextContent(functionNamePrefix);
	            functionCall.appendChild(functionName);
	            for (int j = 0; j < inputVariableList.size(); j++) {
             		NodeList params = ExecutionFlow.replaceTags(inputVariableList.get(j).cloneNode(true), pi).getChildNodes();
             		for (int k = 0; k < params.getLength(); k++) {
             			functionCall.appendChild(doc.importNode(params.item(k).cloneNode(true), true));
             		}
	            }
               	math.appendChild(apply);
            	apply.appendChild(eq);
            	apply.appendChild(doc.importNode(accInterp, true));
            	apply.appendChild(applyPlus);
            	applyPlus.appendChild(plus);
            	applyPlus.appendChild(doc.importNode(accInterp.cloneNode(true), true));
            	applyPlus.appendChild(functionCall);
            	result.appendChild(math);
            }
            else {
    			throw new AGDMException(AGDMException.AGDM006, "Particle interpolation normalization must be defined in the schema if the model have source terms.");
            }
        }
        return result;
    }
    
    /**
     * Creates the normalization function call.
     * @param doc						Document
     * @param schemaId					function id
     * @param schemaName				function name
     * @param suffix					variable suffix
     * @param discProblem				Discretized problem
     * @param problem					Region problem
     * @param pi						Process information
     * @param inputVariableList			Input function variables
     * @return							Call
     * @throws AGDMException			AGDM00X - External error
     */
    private Element createNormalizationFunctionCall(Document doc, String schemaId, String schemaName, String suffix, Document discProblem, 
            Node problem, ProcessInfo pi, ArrayList<NodeWrap> inputVariableList) throws AGDMException {
    	String functionNamePrefix = loadSchema(doc, schemaId, discProblem, problem, pi, schemaName);
		Element math = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "math");
		Element apply = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
		Element eq = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "eq");
    	Element accDer = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
    	accDer.setTextContent("derivative_normalization" + suffix);
  		Element applyPlus = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
		Element plus = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "plus");
    	//RHS
        Element functionCall = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "functionCall");
        Element functionName = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
        functionName.setTextContent(functionNamePrefix);
        functionCall.appendChild(functionName);
        for (int j = 0; j < inputVariableList.size(); j++) {
     		NodeList params = ExecutionFlow.replaceTags(inputVariableList.get(j).getNode().cloneNode(true), pi).getChildNodes();
     		for (int k = 0; k < params.getLength(); k++) {
     			functionCall.appendChild(doc.importNode(params.item(k).cloneNode(true), true));
     		}
        }
       	math.appendChild(apply);
    	apply.appendChild(eq);
    	apply.appendChild(doc.importNode(accDer, true));
    	apply.appendChild(applyPlus);
    	applyPlus.appendChild(plus);
    	applyPlus.appendChild(doc.importNode(accDer.cloneNode(true), true));
    	applyPlus.appendChild(functionCall);
    	return math;
    }
    
    /**
     * Creates particle advective term calculation.
     * @param doc					Document
     * @param discProblem			Discrete problem
     * @param problem				Problem
     * @param step					Current step
     * @param pi					Process information
     * @return						Code
     * @throws AGDMException		AGDM006  Schema not applicable to this problem
     * 								AGDM00X - External error
     */
    private DocumentFragment createAdvectiveTerms(Document doc, Document discProblem, 
            Node problem, Element step, ProcessInfo pi) throws AGDMException {
    	DocumentFragment result = doc.createDocumentFragment();
    	
    	Element template = getPreviousVariable(doc, step);
        ArrayList<Element> inputVariableList = getAdvectiveDerivativeFunctionInputVariables(step);
    	
    	//Call function
        String query = ".//mms:particleAdvectiveTerm/mms:particleDerivativeFunction";
        
        //Load schema
        NodeList discretization = AGDMUtils.find(step, query);
        if (discretization.getLength() > 0) {
	        String schemaId = discretization.item(0).getFirstChild().getLastChild().getTextContent();
	        String functionNamePrefix = loadSchema(doc, schemaId, discProblem, problem, pi, discretization.item(0).getFirstChild().getFirstChild().getTextContent());
	        //Create advective term
	        for (int i = 0; i < pi.getParticleFields().size(); i++) {
	        	String field = pi.getParticleFields().get(i);
	            if (pi.isEulerianField(field) && !pi.getRegionInfo().isAuxiliaryField(field)) {
		        	ProcessInfo newPi = new ProcessInfo(pi);
			        newPi.setField(field);
			    
			        for (int j = 0; j < pi.getBaseSpatialCoordinates().size(); j++) {
			        	String coord = pi.getBaseSpatialCoordinates().get(j);
			        	newPi.setSpatialCoord1(coord);
			        	Element math = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "math");
			            Element apply = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
			            Element eq = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "eq");
			            apply.appendChild(eq);
			            //LHS
			            Element lhs = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
			            lhs.setTextContent("RHS_particle_advective_term_" + field + "_" + coord);
			            apply.appendChild(doc.importNode(lhs, true));
			            //RHS
			            Element functionCall = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "functionCall");
			            Element functionName = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
			            Integer order = functionOrder.get(functionNamePrefix);
			            if (order > 0) {
			            	functionName.setTextContent(functionNamePrefix + pi.getCoordinateRelation().getDiscCoords().get(j));
			            }
			            else {
			            	functionName.setTextContent(functionNamePrefix);
			            }
			            functionCall.appendChild(functionName);
			            for (int l = 0; l < inputVariableList.size(); l++) {
		             		NodeList params = ExecutionFlow.replaceTags(inputVariableList.get(l).cloneNode(true), newPi).getChildNodes();
		             		for (int k = 0; k < params.getLength(); k++) {
		             			functionCall.appendChild(doc.importNode(params.item(k).cloneNode(true), true));
		             		}
			            }
		            	Element applyAcc = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
		            	Element plus = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "plus");
		            	applyAcc.appendChild(plus);
		            	applyAcc.appendChild(doc.importNode(lhs.cloneNode(true), true));
		            	Element applyVel = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
		            	Element times = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "times");
		            	applyVel.appendChild(times);
		            	Element velocity = (Element) newPi.getVelocityComponent(newPi.getSpatialCoord1()).cloneNode(true);
		            	AGDMUtils.replaceFieldVariables(newPi, template, velocity);
		            	applyVel.appendChild(doc.importNode(velocity.getFirstChild(), true));
		            	applyVel.appendChild(functionCall);
		             	applyAcc.appendChild(applyVel);
		            	apply.appendChild(applyAcc);
			            math.appendChild(apply);
			            result.appendChild(math);
			        }
	            }
	        }
        }
        else {
			throw new AGDMException(AGDMException.AGDM006, "Particle evolution equations need particle discretization functions.");
        }
    	return result;
    }
    
    /**
     * Creates position reference sub-tag for iterationOverInteractions.
     * - Position without coordinates (used to get distances)
     * - Actual positions
     * @param doc				Document
     * @param position			Position template
     * @param pi				Process info
     * @return					Position reference
     * @throws AGDMException	AGDM00X - External error
     */
    private Element createPositionReference(Document doc, Element position, ProcessInfo pi) throws AGDMException {
    	Element particlePosition = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "positionReference");
        Element math = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "math");
        pi.setSpatialCoord1(null);
		Element positionElement = ExecutionFlow.replaceTags(position.cloneNode(true), pi);
		math.appendChild(doc.importNode(positionElement.cloneNode(true), true));
    	particlePosition.appendChild(math);
    	
    	for (int i = 0; i < pi.getBaseSpatialCoordinates().size(); i++) {
    	        pi.setSpatialCoord1(pi.getBaseSpatialCoordinates().get(i));
    	        math = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "math");
    			positionElement = ExecutionFlow.replaceTags(position.cloneNode(true), pi);
    			math.appendChild(doc.importNode(positionElement.cloneNode(true), true));
    	    	particlePosition.appendChild(math);
    	}
		return particlePosition;
    }
    
    /**
     * Creates the position calculation.
     * @param doc				Document
     * @param pi				Process information
     * @param problem			Current problem
     * @param discProblem		Discretized problem
     * @param step				Current step
     * @return					Position calculation
     * @throws AGDMException	AGDM00X - External error
     */
    private DocumentFragment calculateNewPosition(Document doc, ProcessInfo pi, Node problem, Document discProblem, Element step) throws AGDMException {
    	
		//Load schema
        String schemaId = step.getElementsByTagNameNS(AGDMUtils.mmsUri, "explicit").item(0).getFirstChild().getLastChild().getTextContent();
        String functionNamePrefix = loadSchema(doc, schemaId, discProblem, problem, pi, step.getElementsByTagNameNS(AGDMUtils.mmsUri, "explicit").item(0).getFirstChild().getFirstChild().getTextContent());
      
        ArrayList<Element> inputVariableList = getTimePositionInputVariables(step);
		
		pi.setDiscretizationType(DiscretizationType.particles);
		pi.setSpatialCoord1(null);
        RuleInfo ri = new RuleInfo();
        ri.setProcessInfo(pi);
        ri.setIndividualField(true);
        ri.setIndividualCoord(false);
        ri.setFunction(false);
        ri.setAuxiliaryCDAdded(false);
        ri.setAuxiliaryFluxesAdded(false);
        ri.setUseBaseCoordinates(false);
        ri.setExtrapolation(pi.getExtrapolationMethod() != null && pi.getIncomFields() != null);
		
		Element math = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "math");
		Element apply = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
		Element eq = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "eq");
		Element newPositionVariable = (Element) convertToPosition(timeOutputVariable);
		math.appendChild(apply);
		apply.appendChild(eq);
		apply.appendChild(doc.importNode(newPositionVariable, true));
		if (pi.getParticleVelocities().size() > 0) {
			//RHS
            Element functionCall = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "functionCall");
            Element functionName = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
            functionName.setTextContent(functionNamePrefix);
            functionCall.appendChild(functionName);
            for (int j = 0; j < inputVariableList.size(); j++) {
            	Element input = (Element) inputVariableList.get(j).cloneNode(true);
            	//Replace occurrences of particleVelocity adding the position correction.
    			NodeList velocities = input.getElementsByTagNameNS(AGDMUtils.simmlUri, "particleVelocity");
    			for (int i = 0; i < velocities.getLength(); i++) {
    				Element applyPlus = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
    				Element plus = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "plus");
    	    		Element applytimes = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
    	    		Element times = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "times");
    	     		Element epsilon = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
    	    		epsilon.setTextContent("particle_velocity_corrector_factor");
    	    		Element correctionVar = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "msub");
    	    		Element ci = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
    	    		ci.setTextContent("velocity_correction");
    	      		Element coord = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
    	      		Element spatialCoordinate = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "spatialCoordinate");
    	      		coord.appendChild(spatialCoordinate);
    	      		correctionVar.appendChild(ci);
    	      		correctionVar.appendChild(coord);
    	    		applytimes.appendChild(times);
    	    		applytimes.appendChild(epsilon);
    	    		applytimes.appendChild(correctionVar);
    	    		applyPlus.appendChild(plus);
    	    		applyPlus.appendChild(doc.importNode(velocities.item(i).cloneNode(true), true));
    	    		applyPlus.appendChild(applytimes);
    				velocities.item(i).getParentNode().replaceChild(input.getOwnerDocument().importNode(applyPlus, true), velocities.item(i));
    			}
                functionCall.appendChild(doc.importNode(input.getFirstChild(), true));
            }
            apply.appendChild(doc.importNode(functionCall, true));
			
    	}
    	else {
    		Element previousPos = getPreviousPositionVariable(discProblem, step);
    	     apply.appendChild(doc.importNode(previousPos, true));
    	}
		DocumentFragment maths = ExecutionFlow.processMath(discProblem, problem, math, ri);
		//Replace fields by their timeslice
		NodeList children = maths.getChildNodes();
		for (int i = 0; i < children.getLength(); i++) {
			AGDMUtils.replaceFieldVariables(pi, getPreviousVariable(doc, step), (Element) children.item(i));
		}
		return maths;
    }
    
    /**
     * Normalizes the position corrections.
     * @param doc				Document
     * @param pi				Process information
     * @param problem			Current problem
     * @param step				Current step
     * @param hasSources		If the problem has sources
     * @return					Code
     * @throws AGDMException	AGDM00X - External error
     */
    private DocumentFragment createPositionCorrectionNormalization(Document doc, ProcessInfo pi, Node problem, Document discProblem, Element step, boolean hasSources) throws AGDMException {
		DocumentFragment result = doc.createDocumentFragment();
		pi.setDiscretizationType(DiscretizationType.particles);
        RuleInfo ri = new RuleInfo();
        ri.setProcessInfo(pi);
        ri.setIndividualField(true);
        ri.setIndividualCoord(false);
        ri.setFunction(false);
        ri.setAuxiliaryCDAdded(false);
        ri.setAuxiliaryFluxesAdded(false);
        ri.setUseBaseCoordinates(false);
        ri.setExtrapolation(pi.getExtrapolationMethod() != null && pi.getIncomFields() != null);
        
     	Element accInterp = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
    	accInterp.setTextContent("interpolation_normalization");
        if (!hasSources) {
    		//Normalization correction
    	   	Element ifE = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "if");
    	   	Element then = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "then");
    	   	Element mathCond = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "math");
    	   	Element applyCond = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
    		Element lt = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "lt");
    		Element abs = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "abs");
    		Element cn = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "cn");
        	cn.setTextContent("0.000000000000001");
        	Element applyAbs = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
        	ifE.appendChild(then);
        	ifE.appendChild(mathCond);
        	ifE.appendChild(then);
        	mathCond.appendChild(applyCond);
        	applyCond.appendChild(lt);
        	applyCond.appendChild(applyAbs);
        	applyCond.appendChild(cn);
        	applyAbs.appendChild(abs);
        	applyAbs.appendChild(accInterp.cloneNode(true));
        	Element math = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "math");
        	Element apply = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
        	Element eq = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "eq");
           	math.appendChild(apply);
        	apply.appendChild(eq);
        	apply.appendChild(accInterp.cloneNode(true));
        	apply.appendChild(cn.cloneNode(true));
        	then.appendChild(math);
        	result.appendChild(ifE);
        }
		
    	//Variable normalization
		Element correctionVar = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "msub");
		Element ci = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
		ci.setTextContent("velocity_correction");
  		Element coord = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
  		Element spatialCoordinate = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "spatialCoordinate");
  		coord.appendChild(spatialCoordinate);
  		correctionVar.appendChild(ci);
  		correctionVar.appendChild(coord);
 		Element math = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "math");
 		Element apply = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
 		Element eq = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "eq");
  		Element nonConsNormalization = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
    	Element divide = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "divide");   	
    	nonConsNormalization.appendChild(divide);
    	nonConsNormalization.appendChild(correctionVar);
    	nonConsNormalization.appendChild(accInterp.cloneNode(true));
    	math.appendChild(apply);
    	apply.appendChild(eq);
    	apply.appendChild(correctionVar.cloneNode(true));
    	apply.appendChild(nonConsNormalization);
    	result.appendChild(ExecutionFlow.processMath(discProblem, problem, math, ri));
    	return result;
    }
    
    /**
     * Initializes the position correction variables.
     * @param doc				Document
     * @param pi				Process information
     * @param problem			Current problem
     * @return					Position calculation
     * @throws AGDMException	AGDM00X - External error
     */
    private DocumentFragment initializePositionCorrection(Document doc, ProcessInfo pi, Node problem, Document discProblem) throws AGDMException {
		pi.setDiscretizationType(DiscretizationType.particles);
        RuleInfo ri = new RuleInfo();
        ri.setProcessInfo(pi);
        ri.setIndividualField(true);
        ri.setIndividualCoord(false);
        ri.setFunction(false);
        ri.setAuxiliaryCDAdded(false);
        ri.setAuxiliaryFluxesAdded(false);
        ri.setUseBaseCoordinates(false);
        ri.setExtrapolation(pi.getExtrapolationMethod() != null && pi.getIncomFields() != null);
        //Calculate velocity correction (XSPH is a variant, proposed by Monaghan (1994), corrects particle velocity
        //assuring more ordered flow and prevents penetration between continua when
        //high speed or impact occur)
        Element math = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "math");
		Element apply = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
		Element eq = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "eq");
		Element correctionVar = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "msub");
		Element ci = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
		ci.setTextContent("velocity_correction");
  		Element coord = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
  		Element spatialCoordinate = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "spatialCoordinate");
  		coord.appendChild(spatialCoordinate);
  		correctionVar.appendChild(ci);
  		correctionVar.appendChild(coord);
  		Element cn = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "cn");
  		cn.setTextContent("0");
  		math.appendChild(apply);
  		apply.appendChild(eq);
  		apply.appendChild(correctionVar.cloneNode(true));
  		apply.appendChild(cn.cloneNode(true));
  		return ExecutionFlow.processMath(discProblem, problem, math, ri);
    }
    
    /**
     * Creates the position correction calculation.
     * @param doc				Document
     * @param pi				Process information
     * @param problem			Current problem
     * @param step				Current step
     * @return					Position calculation
     * @throws AGDMException	AGDM00X - External error
     */
    private DocumentFragment createParticleCorrection(Document doc, ProcessInfo pi, Node problem, Element step) throws AGDMException {
    	if (pi.getParticleVelocities().size() > 0) {
    		DocumentFragment result = doc.createDocumentFragment();
    		pi.setDiscretizationType(DiscretizationType.particles);
            RuleInfo ri = new RuleInfo();
            ri.setProcessInfo(pi);
            ri.setIndividualField(true);
            ri.setIndividualCoord(false);
            ri.setFunction(false);
            ri.setAuxiliaryCDAdded(false);
            ri.setAuxiliaryFluxesAdded(false);
            ri.setUseBaseCoordinates(false);
            ri.setExtrapolation(pi.getExtrapolationMethod() != null && pi.getIncomFields() != null);
	    	//Only if particle_velocity_corrector_factor > 0
	        Element ifEl = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "if");
	        Element then = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "then");
	        Element mathCond = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "math");
			Element applyCond = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
			Element gt = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "gt");
			Element epsilon = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
			epsilon.setTextContent("particle_velocity_corrector_factor");
			Element cn = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "cn");
			cn.setTextContent("0");
			mathCond.appendChild(applyCond);
			applyCond.appendChild(gt);
			applyCond.appendChild(epsilon);
			applyCond.appendChild(cn);
			ifEl.appendChild(mathCond);
			ifEl.appendChild(then);
			result.appendChild(ifEl);
			
			Element correctionVar = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "msub");
			Element ci = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
			ci.setTextContent("velocity_correction");
	  		Element coord = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
	  		Element spatialCoordinate = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "spatialCoordinate");
	  		coord.appendChild(spatialCoordinate);
	  		correctionVar.appendChild(ci);
	  		correctionVar.appendChild(coord);
	    	//velocity correction calculation
	    	for (int i = 0; i < pi.getBaseSpatialCoordinates().size(); i++) {
	    		pi.setSpatialCoord1(pi.getBaseSpatialCoordinates().get(i));
	    		Element math = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "math");
	    		Element apply = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
	    		Element eq = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "eq");
	     		math.appendChild(apply);
	     		apply.appendChild(eq);
	     		apply.appendChild(correctionVar.cloneNode(true));
	     		Element applyPlus = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
	     		apply.appendChild(applyPlus);
	     		Element plus = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "plus");
	     		applyPlus.appendChild(plus);
	     		applyPlus.appendChild(correctionVar.cloneNode(true));
	     		Element applyTimes = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
	     		Element times = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "times");
	        	Element neighbourParticle = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "neighbourParticle");
	        	Element particleVolume = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "particleVolume");
	        	particleVolume.appendChild(doc.importNode(getPreviousVariable(doc, step), true));
	        	neighbourParticle.appendChild(particleVolume);
	        	applyPlus.appendChild(applyTimes);
	        	applyTimes.appendChild(times);
	        	applyTimes.appendChild(neighbourParticle);
	     		Element applyMinus = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
	     		Element minus = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "minus");
	        	neighbourParticle = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "neighbourParticle");
	        	Element particleVelocity = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "particleVelocity");
	        	neighbourParticle.appendChild(particleVelocity.cloneNode(true));
	     		applyMinus.appendChild(minus);
	     		applyMinus.appendChild(neighbourParticle);
	     		applyMinus.appendChild(particleVelocity.cloneNode(true));
	     		applyTimes.appendChild(applyMinus);
	        	Element kernelGradient = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "kernel");
	     		applyTimes.appendChild(kernelGradient);
	     		then.appendChild(ExecutionFlow.replaceTags(math, pi));
	    		AGDMUtils.replaceFieldVariables(pi, getPreviousVariable(doc, step), applyMinus);
	    	}
	    	return result;
	    }
		else {
	  		return doc.createDocumentFragment();
		}
    }
    
    /**
     * Create condition for iteration on neighbour particles if there is only dissipative terms or velocity correction inside the loop.
     * @param doc					Document
     * @param pi					Process information
     * @param step					Current step
     * @param evolutionEquations	Evoluton fields
     * @return						Conditional
     */
    Element createConditionOnInteractions(Document doc, ProcessInfo pi, Element step, ArrayList<String> evolutionEquations) {
        Element ifEl = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "if");
        Element mathCond = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "math");
		Element applyCond = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
		Element or = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "or");
		Element gt = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "gt");
		
		Element cn = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "cn");
		cn.setTextContent("0");
		
		applyCond.appendChild(or);

		ifEl.appendChild(mathCond);
		//Particle velocity
		if (pi.getParticleVelocities().size() > 0) {
			Element epsilon = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
			epsilon.setTextContent("particle_velocity_corrector_factor");
			Element apply = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
			apply.appendChild(gt.cloneNode(true));
			apply.appendChild(epsilon);
			apply.appendChild(cn.cloneNode(true));
			applyCond.appendChild(apply);
		}
        //Dissipation
        NodeList dissipation = step.getElementsByTagNameNS(AGDMUtils.mmsUri, "particleDissipation");
        if (dissipation.getLength() > 0) {
            //Apply dissipation over every evolution equation
            for (int i = 0; i < evolutionEquations.size(); i++) {
                String field = evolutionEquations.get(i);
                Element factor = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
                factor.setTextContent("dissipation_factor_" + field);
            	Element apply = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
        		apply.appendChild(gt.cloneNode(true));
        		apply.appendChild(factor);
        		apply.appendChild(cn.cloneNode(true));
        		applyCond.appendChild(apply);
            }
        }
        
        //Remove "or" if unnecessary
        if (applyCond.getChildNodes().getLength() == 2) {
        	applyCond = (Element) applyCond.getLastChild();
        }
        
        mathCond.appendChild(applyCond);
		
		return ifEl;
    }
    
    
    /**
     * Replaces the position (spatial coordinates) occurrences inside particle code by particle positions.
     * @param pi				The processInfo
     * @param step				Current step
     * @param result			Code into where replace
     * @throws AGDMException	AGDM006 Schema not applicable to this problem
     *                          AGDM00X External error
     */
    void replaceParticlePositions(ProcessInfo pi, Element step, DocumentFragment result) throws AGDMException {
    	 for (String contCoord :  pi.getBaseSpatialCoordinates()) {
	    	NodeList contCoordinates = AGDMUtils.find(result, "//mt:math[ancestor::sml:iterateOverParticles]//mt:ci[text() = '" + contCoord + "']");
	    	Element position = getPreviousPositionVariable(result.getOwnerDocument(), step);
	        pi.setSpatialCoord1(contCoord);
	        position = ExecutionFlow.replaceTags(position, pi);
	        for (int j = contCoordinates.getLength() - 1; j >= 0; j--) {
	        	contCoordinates.item(j).getParentNode().replaceChild(contCoordinates.item(j).getOwnerDocument().importNode(position.cloneNode(true), true), contCoordinates.item(j));
	        }
        }
    }
    
    /**
     * Checks if given step has spatial discretization.
     * @param step				current step
     * @return					true if any spatial discretization tag
     * @throws AGDMException	AGDM00X External error
     */
    boolean hasSpatialDiscretization(Element step) throws AGDMException {
    	return AGDMUtils.find(step, "./mms:meshPreStep|./mms:particlePreStep|./mms:spatialDiscretization|./mms:dissipation").getLength() > 0;
    }
    
    /**
     * Applies conditional expression if condition provided
     * @param expression	Expression to wrap
     * @param condition		Condition to use
     * @return				New expression
     */
    static Node applyConditionalIfExists(Node expression, Node condition) {
    	//Bypass if no condition
    	if (condition == null) {
    		return expression;
    	}
    	//Add condition
    	Document doc = expression.getOwnerDocument();
    	Element ifElem = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "if");
    	Element then = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "then");
    	ifElem.appendChild(condition.cloneNode(true));
    	ifElem.appendChild(then);
    	then.appendChild(expression);
    	return ifElem;
    }
    
    /**
     * Applies conditional expression if condition provided, and assign previous time variable to current variable.
     * @param expression		Expression to wrap
     * @param condition			Condition to use
     * @param currentVar		Current variable
     * @param step				Current discretization step
     * @return					New expression
     * @throws AGDMException	AGDM00X - External error
     */
    static Node applyConditionalIfExistsWithElse(Node expression, Node condition, Node currentVar, Element step) throws AGDMException {
    	//Bypass if no condition
    	if (condition == null) {
    		return expression;
    	}
    	//Add condition
    	Document doc = expression.getOwnerDocument();
    	Element ifElem = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "if");
    	Element then = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "then");
    	Element elseElem = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "else");
    	ifElem.appendChild(condition.cloneNode(true));
    	ifElem.appendChild(then);
    	ifElem.appendChild(elseElem);
    	then.appendChild(expression);
    	//Create assignment to previous time variable
    	Element math = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "math");
    	Element apply = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
    	Element eq = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "eq");
    	math.appendChild(apply);
    	apply.appendChild(eq);
    	apply.appendChild(doc.importNode(currentVar, true));
    	apply.appendChild(doc.importNode(getPreviousVariable(step.getOwnerDocument(), step), true));
    	elseElem.appendChild(math);
    	return ifElem;	
    }
    
    /**
     * Applies conditional expression if condition provided, and assign 0 to given variables
     * @param expression		Expression to wrap
     * @param condition			Condition to use
     * @param variables			Variables to assign
     * @param currentVariable	Current variable name
     * @return					New expression
     * @throws AGDMException	AGDM00X - External error
     */
    static Node applyConditionalIfExistsDef0(Node expression, Node condition, ArrayList<String> variables, Element currentVariable) throws AGDMException {
    	//Bypass if no condition
    	if (condition == null) {
    		return expression;
    	}
    	//Add condition
    	Document doc = expression.getOwnerDocument();
    	Element ifElem = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "if");
    	Element then = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "then");
    	Element elseElem = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "else");
    	ifElem.appendChild(condition.cloneNode(true));
    	ifElem.appendChild(then);
    	ifElem.appendChild(elseElem);
    	then.appendChild(expression);
    	//Create assignment
    	for (int i = 0; i < variables.size(); i++) {
    		String variable = variables.get(i);
	    	Element math = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "math");
	    	Element apply = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "apply");
	    	Element eq = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "eq");
            Element varElement = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "ci");
            varElement.setTextContent(variable);
            Element zero = AGDMUtils.createElement(doc, AGDMUtils.mtUri, "cn");
            zero.setTextContent("0");
	    	math.appendChild(apply);
	    	apply.appendChild(eq);
	    	apply.appendChild(varElement);
	    	apply.appendChild(zero);
	    	elseElem.appendChild(math);
    	}
    	return ifElem;	
    }
    
    /**
     * Unify conditional contents
     * @param step	current step
     * @throws AGDMException 
     */
    private void conditionalUnification(DocumentFragment step) throws AGDMException {
    	//First unify spatial discretization contents
    	NodeList iters = AGDMUtils.find(step, ".//sml:spatialDiscretization");
    	for (int i = 0; i < iters.getLength(); i++) {
    		Element iter = (Element) iters.item(i);
    		if (iter.getChildNodes().getLength() > 1) {
    			Element currentElement = (Element) iter.getFirstChild();
    			//Iterate over elements
    			while (currentElement != null) {
    				if (currentElement.getLocalName().equals("if")) {
    					//Iterate over siblings
    					while (currentElement.getNextSibling() != null) {
    						Element sibling = (Element) currentElement.getNextSibling();
        					if (sibling.getLocalName().equals("if")) {
        						//If condition is the same, unify
        						if (currentElement.getFirstChild().isEqualNode(sibling.getFirstChild())) {
        							Element then = (Element) currentElement.getFirstChild().getNextSibling();
        							Element thenSibling = (Element) sibling.getFirstChild().getNextSibling();
        							for (int j = 0; j < thenSibling.getChildNodes().getLength(); j++) {
        								then.appendChild(thenSibling.getChildNodes().item(j).cloneNode(true));
        							}
        							iter.removeChild(sibling);
        						}
        						else {
            						//When there is no equivalent conditional, go to next current element 
            						break;
            					}
        					}
        					else {
        						//When there is no conditional, go to next current element 
        						break;
        					}
    					}
    				}
    				currentElement = (Element) currentElement.getNextSibling();
    			}
    		}
    	}
    	//If the content of spatiaDiscretizations is conditional, move the conditional outside
    	iters = AGDMUtils.find(step, ".//sml:spatialDiscretizations");
    	for (int i = iters.getLength() - 1; i >= 0; i--) {
    		NodeList spatialDisc = iters.item(i).getChildNodes();
    		Node conditional = null;
    		boolean first = true;
    		boolean common = true;
    		for (int j = 0; j < spatialDisc.getLength(); j++) {
    			NodeList children = spatialDisc.item(j).getChildNodes();
    			if (children.getLength() > 0) {
	    			//Limit element
	    			if (children.item(0).getChildNodes().getLength() > 0) {
	    				common = false;
	    				break;
	    			}
	    			if (children.getLength() == 2) {
	    				if (children.item(1).getLocalName().equals("if")) {
	    					if (first) {
	    						first = false;
	    						conditional = children.item(1).getFirstChild().cloneNode(true);
	    					}
	    					else {
	    						if (!children.item(1).getFirstChild().isEqualNode(conditional)) {
	    							common = false;
	    							break;
	    						}
	    					}
	    				}
	    				else {
	    					common = false;
	    					break;
	    				}
	    			}
	    			else {
						common = false;
						break;
					}
    			}
    		}
    		if (common) {
    			Element ifElem = AGDMUtils.createElement(step.getOwnerDocument(), AGDMUtils.simmlUri, "if");
    			Element thenElem = AGDMUtils.createElement(step.getOwnerDocument(), AGDMUtils.simmlUri, "then");
    			ifElem.appendChild(conditional);
    			ifElem.appendChild(thenElem);
    			thenElem.appendChild(iters.item(i).cloneNode(true));
    	 		spatialDisc = thenElem.getFirstChild().getChildNodes();
        		for (int j = 0; j < spatialDisc.getLength(); j++) {
        			NodeList children = spatialDisc.item(j).getChildNodes();
        			if (children.getLength() > 0) {
        				Node copy = children.item(1).cloneNode(true);
        				spatialDisc.item(j).removeChild(children.item(1));
        				NodeList childrenInThen = copy.getLastChild().getChildNodes();
        				for (int k = 0; k < childrenInThen.getLength(); k++) {
        					spatialDisc.item(j).appendChild(childrenInThen.item(k).cloneNode(true));
        				}
        			}
        		}
    			iters.item(i).getParentNode().replaceChild(ifElem, iters.item(i));
    		}
    	}
    	//Unify iterationOverCells
    	iters = AGDMUtils.find(step, "sml:iterateOverCells");
    	for (int i = 0; i < iters.getLength(); i++) {
    		Element iter = (Element) iters.item(i);
    		if (iter.getChildNodes().getLength() > 1) {
    			Element currentElement = (Element) iter.getFirstChild();
    			//Iterate over elements
    			while (currentElement != null) {
    				if (currentElement.getLocalName().equals("if")) {
    					//Iterate over siblings
    					while (currentElement.getNextSibling() != null) {
    						Element sibling = (Element) currentElement.getNextSibling();
        					if (sibling.getLocalName().equals("if")) {
        						//If condition is the same, unify (only if all conditional variable is not modified in the condition code)
        						Collection<String> assignedVars = AGDMUtils.getAllAssignedVarsString(currentElement.getFirstChild().getNextSibling());
        						Collection<String> conditionalVars = AGDMUtils.getAllUsedVarsString(currentElement.getFirstChild(), false);
        						
        						assignedVars.retainAll(conditionalVars);
        						boolean interdependency = !assignedVars.isEmpty();

        						if (currentElement.getFirstChild().isEqualNode(sibling.getFirstChild()) && !interdependency) {
        							Element then = (Element) currentElement.getFirstChild().getNextSibling();
        							Element thenSibling = (Element) sibling.getFirstChild().getNextSibling();
        							for (int j = 0; j < thenSibling.getChildNodes().getLength(); j++) {
        								then.appendChild(thenSibling.getChildNodes().item(j).cloneNode(true));
        							}
        							Element elseCurr = (Element) then.getNextSibling();
        							Element elseSibling = (Element) thenSibling.getNextSibling();
        							if (elseSibling != null) {
        								if (elseCurr == null) {
        									currentElement.appendChild(elseSibling.cloneNode(true));
        								}
        								else {
        									for (int j = 0; j < elseSibling.getChildNodes().getLength(); j++) {
        										elseCurr.appendChild(elseSibling.getChildNodes().item(j).cloneNode(true));
        	    							}
        								}
        							}
        							iter.removeChild(sibling);
        						}
        						else {
            						//When there is no equivalent conditional, go to next current element 
            						break;
            					}
        					}
        					else {
        						//When there is no conditional, go to next current element 
        						break;
        					}
    					}
    				}
    				currentElement = (Element) currentElement.getNextSibling();
    			}
    		}
    	}         
    }

    /**
     * Creates post initial condition. This includes boundaries and auxiliary fields.
     * @param doc				Current document.
     * @param regionProblem		Region document
     * @param schema			Schema
     * @param pi				Process Info
     * @param opInfoMesh		Mesh operators
     * @param opInfoParticles	Particle operators
     * @return					Post initial condition
     * @throws AGDMException	AGDM00X  External error	
     */
    public DocumentFragment createPostInitialCondition(Document doc, DocumentFragment regionProblem, Document schema, ProcessInfo pi, OperatorsInfo opInfoMesh, HashMap<String,OperatorsInfo> opInfoParticles) throws AGDMException {
    	DocumentFragment result = doc.createDocumentFragment();

        //If there are auxiliaryFields with derivative terms, the boundaries for the fields must be calculated
        if (hasDerivativeAuxiliaresMesh || hasDerivativeAuxiliaresParticles) {
        	//Boundaries and extrapolations
            Element boundary = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "boundary");
            Element variable = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "variable");
            Element timeSlice = AGDMUtils.createBaseTimeSlice(doc);
            Element pVariable = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "previousVariable");
            Element stencil = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "stencil");
            stencil.setTextContent(String.valueOf(SchemaCreator.maxStencil));
            boundary.appendChild(variable);
            variable.appendChild(timeSlice);
            boundary.appendChild(pVariable);
            pVariable.appendChild(timeSlice.cloneNode(true));
            boundary.appendChild(stencil);
            //Add the processed boundary
            result.appendChild(doc.importNode(Boundary.processBoundary(doc, regionProblem, boundary, pi, false, true, false, true), true));
        } 
        //Auxiliaries
        result.appendChild(doc.importNode(createPostInitAuxiliaries(schema, doc, regionProblem, opInfoMesh, opInfoParticles, pi), true));
        Element boundary = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "boundary");
        Element variable = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "variable");
        Element pVariable = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "previousVariable");
        Element stencil = AGDMUtils.createElement(doc, AGDMUtils.simmlUri, "stencil");
        Element timeSlice = AGDMUtils.createBaseTimeSlice(doc);
        boundary.appendChild(variable);
        variable.appendChild(timeSlice);
        boundary.appendChild(pVariable);
        pVariable.appendChild(timeSlice.cloneNode(true));
        stencil.setTextContent(String.valueOf(SchemaCreator.maxStencil));
        boundary.appendChild(stencil);
        //Auxiliary boundaries
        result.appendChild(Boundary.processBoundary(doc, regionProblem, boundary, pi, false, true, true, !(hasDerivativeAuxiliaresMesh || hasDerivativeAuxiliaresParticles)));
        return result;
    }
    
}
