package eu.iac3.mathMS.AGDM.processors;

import eu.iac3.mathMS.AGDM.AGDMException;
import eu.iac3.mathMS.AGDM.MeshInterpolationType;
import eu.iac3.mathMS.AGDM.utils.AGDMUtils;
import eu.iac3.mathMS.AGDM.utils.PDE.ProcessInfo;

import java.util.ArrayList;
import java.util.Arrays;

import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.Element;

/**
 * The class that implements all the interpolation methods.
 * @author bminano
 *
 */
public class Interpolation {

    static String mmsUri = "urn:mathms";
    static String smlUri = "urn:simml";
    static String mtUri = "http://www.w3.org/1998/Math/MathML";
    
    /**
     * Constructor.
     * @throws AGDMException 
     */
    Interpolation() throws AGDMException {
    };
  
    /**
     * Creates mesh interpolation function.
     * @param result            The problem
     * @param coordinates       The spatial coordinates
     * @param method            The interpolation method
     * @return                  The code
     * @throws AGDMException    AGDM00X External error
     */
    public static Element createMeshInterpolationFunction(Document result, ArrayList<String> coordinates, MeshInterpolationType method) throws AGDMException {
        DocumentFragment interpolation = result.createDocumentFragment();
        if (method.equals(MeshInterpolationType.Linear_LagrangianInterpolation)) {
        	interpolation.appendChild(createLinearLagrangianInterpolation(result, coordinates));
        }
        
        //Function
        Element function = AGDMUtils.createElement(result, mmsUri, "function");
        //Function name
        Element funcName = AGDMUtils.createElement(result, mmsUri, "functionName");
        funcName.setTextContent("interpolate_from_mesh");
        function.appendChild(funcName);
        //Function parameters
        //The parameters are the spatial coordinates of the field
        Element funcParams = AGDMUtils.createElement(result, mmsUri, "functionParameters");
        //field
        Element funcParam1 = AGDMUtils.createElement(result, mmsUri, "functionParameter");
        Element paramName1 = AGDMUtils.createElement(result, mmsUri, "name");
        paramName1.setTextContent("u");
        Element paramType1 = AGDMUtils.createElement(result, mmsUri, "type");
        paramType1.setTextContent("field");
        funcParam1.appendChild(paramName1);
        funcParam1.appendChild(paramType1);
        funcParams.appendChild(funcParam1);
        //Factors
      	for (int j = 0; j < Math.pow(2, coordinates.size()); j++) {      		
            Element funcParam = AGDMUtils.createElement(result, mmsUri, "functionParameter");
            Element paramName = AGDMUtils.createElement(result, mmsUri, "name");
            paramName.setTextContent("factor" + j);
            Element paramType = AGDMUtils.createElement(result, mmsUri, "type");
            paramType.setTextContent("real");
            funcParam.appendChild(paramName);
            funcParam.appendChild(paramType);
            funcParams.appendChild(funcParam);
      		
      	}
        //coordinates
        for (int i = 0; i < coordinates.size(); i++) {
            String coord = coordinates.get(i);
	        Element funcParam = AGDMUtils.createElement(result, mmsUri, "functionParameter");
            Element paramName = AGDMUtils.createElement(result, mmsUri, "name");
            paramName.setTextContent(coord);
	        Element paramType = AGDMUtils.createElement(result, mmsUri, "type");
	        paramType.setTextContent("int");
	        funcParam.appendChild(paramName);
	        funcParam.appendChild(paramType);
	        funcParams.appendChild(funcParam);
	        function.appendChild(funcParams);
        }
        //Function instructions
        Element funcInstructions = AGDMUtils.createElement(result, mmsUri, "functionInstructions");
        Element sml = AGDMUtils.createElement(result, smlUri, "simml");
        sml.appendChild(interpolation);

        funcInstructions.appendChild(sml);
        function.appendChild(funcInstructions);
        
        return function;
    }
        
    /**
     * Create the linear Lagrangian interpolation.
     * @param result            The document
     * @param coordinates       The spatial coordinates
     * @param coordRel			Coordinate information
     * @return                  The instructions
     * @throws AGDMException    AGDM00X External error
     */
    private static DocumentFragment createLinearLagrangianInterpolation(Document result, ArrayList<String> coordinates) throws AGDMException {
        DocumentFragment res = result.createDocumentFragment();

      	//Return combination
      	/*
      	 * 		factor1 * vector(dat_ptr, i, j)
         *    + factor2 * vector(dat_ptr, i, j + 1)
         *    + factor3 * vector(dat_ptr, i + 1, j)
         *    + factor4 * vector(dat_ptr, i + 1, j + 1)
      	 */
      	Element returnElement = AGDMUtils.createElement(result, smlUri, "return");
     	Element math = AGDMUtils.createElement(result, mtUri, "math");
     	Element appendElement = null;
      	ArrayList<String> elements = new ArrayList<String>(Arrays.asList("+", "0"));
      	ArrayList<String> combinations = AGDMUtils.createCombinations(coordinates.size(), elements);
      	for (int i = 0; i < combinations.size(); i++) {
      		String combination = combinations.get(i);
      		Element summationElement = AGDMUtils.createElement(result, mtUri, "apply");
      		Element times = AGDMUtils.createElement(result, mtUri, "times");
      		summationElement.appendChild(times);
      	   	Element factor =  AGDMUtils.createElement(result, mtUri, "ci");
        	factor.setTextContent("factor" + i);
      		//factors and indices
      		DocumentFragment indices = result.createDocumentFragment();
            for (int j = 0; j < coordinates.size(); j++) {
          		String coord = coordinates.get(j);
            	Element coordinate;
                if (combination.substring(j).startsWith("+")) {
              		//coordinate
              		coordinate = AGDMUtils.createElement(result, mtUri, "apply");
             		Element plus = AGDMUtils.createElement(result, mtUri, "plus");
             		Element cn = AGDMUtils.createElement(result, mtUri, "cn");
             		cn.setTextContent("1");
             		Element coordE = AGDMUtils.createElement(result, mtUri, "ci");
             		coordE.setTextContent(coord);
              		coordinate.appendChild(plus);
              		coordinate.appendChild(coordE);
              		coordinate.appendChild(cn);
                }
                else {
              		//coordinate
              		coordinate = AGDMUtils.createElement(result, mtUri, "ci");
              		coordinate.setTextContent(coord);
                }
                
                indices.appendChild(coordinate);
            }
            //data
            Element data = AGDMUtils.createElement(result, mtUri, "apply");
            Element ciData = AGDMUtils.createElement(result, mtUri, "ci");
            ciData.setTextContent("u");
            data.appendChild(ciData);
            data.appendChild(indices);
            summationElement.appendChild(factor);
            summationElement.appendChild(data);
            
            //complete term
      		if (i == 0) {
      			appendElement = summationElement;
      		}
      		else {
      			//Append summation element
    	    	Element applyPlus = AGDMUtils.createElement(result, mtUri, "apply");
    	    	Element plus = AGDMUtils.createElement(result, mtUri, "plus");
    	    	applyPlus.appendChild(plus);
    	    	applyPlus.appendChild(summationElement);
    	    	applyPlus.appendChild(appendElement);
    	    	appendElement = applyPlus;
      		}
      	}
      	math.appendChild(appendElement);
      	returnElement.appendChild(math);
      	res.appendChild(returnElement);
        
        return res;
    }
    
    /**
     * Create the linear Lagrangian interpolation factors.
     * @param result            The document
     * @param coordinates       The spatial coordinates
     * @param coordRel			Coordinate information
     * @return                  The instructions
     * @throws AGDMException    AGDM00X External error
     */
    public static DocumentFragment createLinearLagrangianInterpolationFactors(Document result, ProcessInfo pi, Element positionTemplate) throws AGDMException {
        DocumentFragment res = result.createDocumentFragment();
        
        ArrayList<String> coordinates = pi.getBaseSpatialCoordinates();
      	for (int i = 0 ; i < coordinates.size(); i++) {
      		String contCoord = coordinates.get(i);
      		String coord = pi.getCoordinateRelation().getDiscCoords().get(i);
	      	//fraction of distance
	      	Element fracMath = AGDMUtils.createElement(result, mtUri, "math");
	      	Element apply = AGDMUtils.createElement(result, mtUri, "apply");
	      	Element eq = AGDMUtils.createElement(result, mtUri, "eq");
	      	Element fraction = AGDMUtils.createElement(result, mtUri, "ci");
	      	fraction.setTextContent(contCoord + "frac");
	    	Element applyDivide = AGDMUtils.createElement(result, mtUri, "apply");
	    	Element divide = AGDMUtils.createElement(result, mtUri, "divide");
	     	Element coordBase = AGDMUtils.createElement(result, mtUri, "ci");
	      	coordBase.setTextContent(coordinates.get(i));
	    	Element delta = AGDMUtils.createElement(result, smlUri, "cellDeltaSpacing");
	    	delta.setTextContent(contCoord);
	        Element posCell = AGDMUtils.createElement(result, AGDMUtils.simmlUri, "cellPosition");
	        posCell.setTextContent(coord);
	    	Element applyMinus = AGDMUtils.createElement(result, mtUri, "apply");
	    	Element minus = AGDMUtils.createElement(result, mtUri, "minus");
	    	pi.setSpatialCoord1(contCoord);
            pi.setSpatialCoord2(null);                
            Element pos = ExecutionFlow.replaceTags(positionTemplate.cloneNode(true), pi);
	    	applyMinus.appendChild(minus);
	    	applyMinus.appendChild(result.importNode(pos, true));
	    	applyMinus.appendChild(posCell);
	      	fracMath.appendChild(apply);
	      	apply.appendChild(eq);
	      	apply.appendChild(fraction);
	      	apply.appendChild(applyDivide);
	      	applyDivide.appendChild(divide.cloneNode(false));
	      	applyDivide.appendChild(applyMinus);
	      	applyDivide.appendChild(delta.cloneNode(true));
	    	
	      	res.appendChild(fracMath);
      	}
      	//Return combination
      	/*
      	 * 		(1 - xfrac) * (1-yfrac)
         *      (1 - xfrac) *    yfrac 
         *           xfrac  * (1-yfrac)
         *           xfrac  *    yfrac
         *            
      	 */
      	ArrayList<String> elements = new ArrayList<String>(Arrays.asList("+", "0"));
      	ArrayList<String> combinations = AGDMUtils.createCombinations(coordinates.size(), elements);
      	for (int i = 0; i < combinations.size(); i++) {
      		String combination = combinations.get(i);
      		Element math = AGDMUtils.createElement(result, mtUri, "math");
      		Element apply = AGDMUtils.createElement(result, mtUri, "apply");
      		Element eq = AGDMUtils.createElement(result, mtUri, "eq");
      		math.appendChild(apply);
      		apply.appendChild(eq);
      		Element fac = AGDMUtils.createElement(result, mtUri, "ci");
      		fac.setTextContent("factor" + i);
      		apply.appendChild(fac);
      		//factors      		
      		Element applyTimes = null;      		
            for (int j = 0; j < coordinates.size(); j++) {
          		String contCoord = coordinates.get(j);
          		String coord = pi.getCoordinateRelation().getDiscCoords().get(j);
            	Element fracApply;
            	Element coordinate;
                if (combination.substring(j).startsWith("+")) {
              		fracApply = AGDMUtils.createElement(result, mtUri, "ci");
              		fracApply.setTextContent(contCoord + "frac");
              		//coordinate
              		coordinate = AGDMUtils.createElement(result, mtUri, "apply");
             		Element plus = AGDMUtils.createElement(result, mtUri, "plus");
             		Element cn = AGDMUtils.createElement(result, mtUri, "cn");
             		cn.setTextContent("1");
             		Element coordE = AGDMUtils.createElement(result, mtUri, "ci");
             		coordE.setTextContent(coord);
              		coordinate.appendChild(plus);
              		coordinate.appendChild(coordE);
              		coordinate.appendChild(cn);
                }
                else {
                	fracApply = AGDMUtils.createElement(result, mtUri, "apply");
             		Element minus = AGDMUtils.createElement(result, mtUri, "minus");
             		Element cn = AGDMUtils.createElement(result, mtUri, "cn");
             		cn.setTextContent("1");
             		Element frac = AGDMUtils.createElement(result, mtUri, "ci");
              		frac.setTextContent(contCoord + "frac");
              		fracApply.appendChild(minus);
              		fracApply.appendChild(cn);
              		fracApply.appendChild(frac);
              		//coordinate
              		coordinate = AGDMUtils.createElement(result, mtUri, "ci");
              		coordinate.setTextContent(coord);
                }
                if (j == 0) {
                	applyTimes = fracApply;
                }
                else {
                	Element applyTimesTmp = AGDMUtils.createElement(result, mtUri, "apply");
                	Element times = AGDMUtils.createElement(result, mtUri, "times");
                	applyTimesTmp.appendChild(times);
                	applyTimesTmp.appendChild(applyTimes);
                	applyTimesTmp.appendChild(fracApply);
                	applyTimes = applyTimesTmp;
                }
            }
            apply.appendChild(applyTimes);
            
         	res.appendChild(math);
         	
      	}
        return res;
    }
    
}
