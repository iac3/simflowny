/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.AGDM.utils;

import eu.iac3.mathMS.AGDM.utils.PDE.ProcessInfo;

/**
 * Auxiliary class that groups all the information needed in the rule processing. 
 * @author bminyano
 *
 */
public class RuleInfo {
    Boolean individualField; 
    Boolean individualCoord;
    Boolean function;
    ProcessInfo processInfo;
    Boolean auxiliaryCDAdded;
    Boolean auxiliaryFluxesAdded;
    Boolean useBaseCoordinates;
    boolean extrapolation;
  
    /**
     * Constructor.
     */
    public RuleInfo() {
    }
    
    /**
     * Copy constructor.
     * @param ri    The instance to copy
     */
    public RuleInfo(RuleInfo ri) {
        this.individualField = ri.individualField;
        this.individualCoord = ri.individualCoord;
        this.function = ri.function;
        this.processInfo = new ProcessInfo(ri.processInfo);
        this.auxiliaryCDAdded = ri.auxiliaryCDAdded;
        this.auxiliaryFluxesAdded = ri.auxiliaryFluxesAdded;
        this.useBaseCoordinates = ri.useBaseCoordinates;
        this.extrapolation = ri.extrapolation;
    }
    
    public Boolean getUseBaseCoordinates() {
        return useBaseCoordinates;
    }
    public void setUseBaseCoordinates(Boolean useBaseCoordinates) {
        this.useBaseCoordinates = useBaseCoordinates;
    }
    public Boolean getIndividualField() {
        return individualField;
    }
    public Boolean getIndividualCoord() {
        return individualCoord;
    }
    public Boolean getFunction() {
        return function;
    }
    public void setIndividualField(Boolean individualField) {
        this.individualField = individualField;
    }
    public void setIndividualCoord(Boolean individualCoord) {
        this.individualCoord = individualCoord;
    }
    public void setFunction(Boolean function) {
        this.function = function;
    }
    public ProcessInfo getProcessInfo() {
        return processInfo;
    }
    public void setProcessInfo(ProcessInfo processInfo) {
        this.processInfo = processInfo;
    }

    public Boolean getAuxiliaryCDAdded() {
		return auxiliaryCDAdded;
	}

	public void setAuxiliaryCDAdded(Boolean auxiliaryCDAdded) {
		this.auxiliaryCDAdded = auxiliaryCDAdded;
	}

	public Boolean getAuxiliaryFluxesAdded() {
		return auxiliaryFluxesAdded;
	}

	public void setAuxiliaryFluxesAdded(Boolean auxiliaryFluxesAdded) {
		this.auxiliaryFluxesAdded = auxiliaryFluxesAdded;
	}

	public boolean isExtrapolation() {
        return extrapolation;
    }

    public void setExtrapolation(boolean extrapolation) {
        this.extrapolation = extrapolation;
    }
}
