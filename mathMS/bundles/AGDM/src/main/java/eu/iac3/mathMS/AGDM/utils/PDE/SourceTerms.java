/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.AGDM.utils.PDE;

import java.util.ArrayList;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

/**
 * Source terms.
 * @author bminyano
 *
 */
public class SourceTerms {
    private ArrayList<Source> sources;

    /**
     * Constructor.
     */
    public SourceTerms() {
    	sources = new ArrayList<Source>();
    }
    
    /**
     * Copy constructor.
     * @param sourceTerms   The object to copy
     */
    public SourceTerms(SourceTerms sourceTerms) {
    	sources = new ArrayList<Source>();
    	for (Source s : sourceTerms.sources) {
    		sources.add(new Source(s));
    	}
    }
    
    /**
     * Copy constructor.
     * @param sourcesList   The object to copy
     */
    public SourceTerms(ArrayList<Source> sourcesList) {
    	sources = new ArrayList<Source>();
    	for (Source s : sourcesList) {
    		sources.add(new Source(s));
    	}
    }
    
    /**
     * Adds a new source term to the list.
     * @param field         The variable for the source
     * @param formula       The formula of the source term
     */
    public void add(String field, Element formula, Node conditional) {
    	sources.add(new Source(field, formula, conditional));
    }
    
	public ArrayList<Source> getSources() {
		return sources;
	}

    /**
     * Checks if there is a term for a specific field.
     * @param field     The field
     * @return          True if there is a term for the field
     */
    public boolean hasSourcesForField(String field) {
        for (int i = 0; i < sources.size(); i++) {
            if (sources.get(i).getField().equals(field)) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Checks if there is a term for a specific field.
     * @param field     The field
     * @return          True if there is a term for the field
     */
    public Source getSources(String field) {
        for (int i = 0; i < sources.size(); i++) {
            if (sources.get(i).getField().equals(field)) {
                return sources.get(i);
            }
        }
        return null;
    }
	
	public void setSources(ArrayList<Source> fluxes) {
		this.sources = fluxes;
	}

	@Override
	public String toString() {
		return "SourceTerms [sources=" + sources + "]";
	}
}
