/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.AGDM.utils.PDE;

import java.util.ArrayList;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import eu.iac3.mathMS.AGDM.utils.AGDMUtils;

/**
 * The information needed to process an equation.
 * @author bminyano
 *
 */
public class BoundarySchema {
    
    private String schemaId;
    private String schemaName;
    private ArrayList<BoundarySchemaCondition> conditions;
    
    /**
     * Copy constructor.
     * @param boundarySchema    The object to copy
     */
    public BoundarySchema(BoundarySchema boundarySchema) {
        schemaId = boundarySchema.getSchemaId();
        schemaName = boundarySchema.getSchemaName();
        conditions = new ArrayList<BoundarySchemaCondition>();
        for (int i = 0; i < boundarySchema.getConditions().size(); i++) {
            conditions.add(new BoundarySchemaCondition(boundarySchema.getConditions().get(i)));
        }
    }
    
    /**
     * Parses a boundary schema from xml.
     * @param boundarySchema        The xml element.
     */
    public BoundarySchema(Element boundarySchema) {
        conditions = new ArrayList<BoundarySchemaCondition>();
        schemaId = boundarySchema.getElementsByTagNameNS(AGDMUtils.mmsUri, "id").item(0).getTextContent();
        schemaName = boundarySchema.getElementsByTagNameNS(AGDMUtils.mmsUri, "name").item(0).getTextContent();
        NodeList conds = boundarySchema.getElementsByTagNameNS(AGDMUtils.mmsUri, "condition");
        for (int i = 0; i < conds.getLength(); i++) {
            conditions.add(new BoundarySchemaCondition((Element) conds.item(i)));
        }
    }

    public String getSchemaId() {
        return schemaId;
    }

    public void setSchemaId(String sch) {
        this.schemaId = sch;
    }

    public String getSchemaName() {
        return schemaName;
    }

    public void setSchemaName(String sch) {
        this.schemaName = sch;
    }
    
    public ArrayList<BoundarySchemaCondition> getConditions() {
        return conditions;
    }

    public void setConditions(ArrayList<BoundarySchemaCondition> conds) {
        this.conditions = conds;
    }

    @Override
    public String toString() {
        return "BoundarySchema [schemaId=" + schemaId + ", schemaName=" + schemaName + ", conditions=" + conditions
                + "]";
    }
    
    /**
     * Check equality.
     * @param bs	input boundary schema
     * @return		true if equals
     */
    public boolean equals(BoundarySchema bs) {
        return bs.getSchemaId().equals(this.schemaId) && bs.getConditions().equals(this.conditions);
    }
}
