/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.AGDMTest;

import eu.iac3.mathMS.AGDM.AGDMImpl;
import eu.iac3.mathMS.AGDM.utils.AGDMUtils;
import eu.iac3.mathMS.documentManagerPlugin.DMException;
import eu.iac3.mathMS.documentManagerPlugin.DocumentManager;
import eu.iac3.mathMS.documentManagerPlugin.DocumentManagerImpl;
import eu.iac3.mathMS.simflownyUtilsPlugin.SimflownyUtils;

import static org.ops4j.pax.exam.CoreOptions.bootDelegationPackages;
import static org.ops4j.pax.exam.CoreOptions.junitBundles;
import static org.ops4j.pax.exam.CoreOptions.mavenBundle;
import static org.ops4j.pax.exam.CoreOptions.options;
import static org.ops4j.pax.exam.CoreOptions.systemProperty;

import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.PrintWriter;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;
import org.junit.runner.RunWith;
import org.ops4j.pax.exam.Configuration;
import org.ops4j.pax.exam.Option;
import org.ops4j.pax.exam.junit.PaxExam;
import org.ops4j.pax.exam.spi.reactors.ExamReactorStrategy;
import org.ops4j.pax.exam.spi.reactors.PerSuite;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

/** 
 * AGDMTest class is a junit testcase to
 * test the {@link eu.iac3.mathMS.AGDM.AGDM} interface.
 *  
 * @author      ----
 * @version     ----
 */
@RunWith(PaxExam.class)
@ExamReactorStrategy(PerSuite.class)
public class AGDMTest {

    long startTime;
    
    @Rule 
    public TestName name = new TestName();
    	
    /**
     * Configuration for Pax-Exam.
     * @return options
     */
    @Configuration
    public Option[] config() {
        return options(
    		bootDelegationPackages(
    				 "com.sun.*",
    				 "javax.management"
    				),
        	systemProperty("file.encoding").value("UTF-8"),
        	systemProperty("derby.system.home").value("target/Derby"),
        	systemProperty("exist.initdb").value("true"),
        	systemProperty("exist.home").value("."),
        	systemProperty("dataSourceName").value("SIMFLOWNY_DATASOURCE"),
        	systemProperty("ANTLR_USE_DIRECT_CLASS_LOADING").value("true"),
        	systemProperty("org.ops4j.pax.logging.DefaultServiceLog.level").value("WARN"),
        	        	
        	mavenBundle("org.apache.felix", "org.osgi.compendium", "1.4.0"),
        	mavenBundle("org.apache.felix", "org.apache.felix.log", "1.0.1"),
            mavenBundle("org.apache.commons", "commons-lang3", "3.7"),
            mavenBundle("org.apache.commons", "commons-text", "1.3"),
            mavenBundle("org.osgi", "org.osgi.enterprise", "5.0.0"), 
            mavenBundle("org.apache.felix", "org.apache.felix.scr.annotations", "1.12.0"),
            mavenBundle("org.apache.felix", "org.apache.felix.scr", "2.1.0"),
            mavenBundle("org.osgi", "org.osgi.dto", "1.1.0"),
            mavenBundle("org.osgi", "org.osgi.core", "6.0.0"),
            mavenBundle("eu.iac3.thirdParty.bundles", "xml-apis", "1.0"),
            mavenBundle("org.apache.servicemix.bundles", "org.apache.servicemix.bundles.relaxngDatatype", "2011.1_1"),
            mavenBundle("org.apache.servicemix.bundles", "org.apache.servicemix.bundles.xmlresolver", "1.2_5"),
            mavenBundle("org.apache.servicemix.bundles", "org.apache.servicemix.bundles.saxon", "9.8.0-10_1"),
            mavenBundle("org.apache.servicemix.bundles", "org.apache.servicemix.bundles.derby", "10.12.1.1_1"),
            mavenBundle("org.ops4j.pax.jdbc", "pax-jdbc-derby", "1.3.0"), 
            mavenBundle("eu.iac3.thirdParty.bundles.xsom", "xsom", "20140925"),
            mavenBundle("com.google.code.gson", "gson", "2.8.4"),
            mavenBundle("org.json", "json", "20180130"),
            mavenBundle("com.fasterxml.jackson.core", "jackson-core", "2.10.3"),
            mavenBundle("com.fasterxml.jackson.core", "jackson-annotations", "2.10.3"),
            mavenBundle("com.fasterxml.jackson.core", "jackson-databind", "2.10.3"),
            mavenBundle("eu.iac3.thirdParty.bundles.underscore", "underscore", "1.60"),
            mavenBundle("org.yaml", "snakeyaml", "1.26"),
            mavenBundle("com.fasterxml.jackson.dataformat", "jackson-dataformat-yaml", "2.10.3"),
            mavenBundle("eu.iac3.thirdParty.bundles.snuggletex", "snuggletex-core-upconversion", "1.2.2"),
            mavenBundle("org.mozilla", "rhino", "1.7.12"),
            mavenBundle("eu.iac3.mathMS", "simflownyUtilsPlugin", "3.1"),
        	mavenBundle("org.apache.felix", "org.apache.felix.configadmin", "1.9.0"),
        	mavenBundle("org.ops4j.pax.confman", "pax-confman-propsloader", "0.2.3"),
            mavenBundle("eu.iac3.mathMS", "codesDBPlugin", "3.1"),
            mavenBundle("org.ops4j.pax.logging", "pax-logging-api", "1.10.1"),
            mavenBundle("org.ops4j.pax.logging", "pax-logging-service", "1.10.1"),
            mavenBundle("org.ops4j.pax.logging", "pax-logging-log4j2", "1.10.1"),
            mavenBundle("com.mchange.c3p0", "com.springsource.com.mchange.v2.c3p0", "0.9.1.2"),
            mavenBundle("com.zaxxer", "HikariCP-java6", "2.3.13"),
            mavenBundle("org.quartz-scheduler", "quartz", "2.3.0"),
            mavenBundle("org.apache.servicemix.bundles", "org.apache.servicemix.bundles.regexp", "1.3_3"),
            mavenBundle("org.apache.servicemix.bundles", "org.apache.servicemix.bundles.lucene", "4.10.4_1"),
            mavenBundle("org.apache.servicemix.bundles", "org.apache.servicemix.bundles.lucene-sandbox", "4.10.4_1"),
            mavenBundle("org.apache.servicemix.bundles", "org.apache.servicemix.bundles.lucene-queries", "4.10.4_1"),
            mavenBundle("org.apache.servicemix.bundles", "org.apache.servicemix.bundles.lucene-queryparser", "4.10.4_1"),
            mavenBundle("org.apache.servicemix.bundles", "org.apache.servicemix.bundles.lucene-analyzers-common", "4.10.4_1"),
            mavenBundle("eu.iac3.thirdParty.bundles.exist", "exist", "4.1"),
            mavenBundle("eu.iac3.mathMS", "eXistDBWrapper", "3.1"),
            mavenBundle("eu.iac3.mathMS", "documentManagerPlugin", "3.1"),
            junitBundles()
            );
    }
   
    /**
     * Initial setup for the tests.
     * @throws DMException 
     */
    @Before
    public void onSetUp() throws DMException {
        startTime = System.currentTimeMillis();
        DocumentManager docMan = new DocumentManagerImpl();
        try {
        	docMan.addCollection("physModel", "");
        }
        catch (DMException e) {
        	//Collection already exists
        }
    }
    
    /**
     * Close browser when last test.
     */
    @After
    public void onTearDown() {
        System.out.println(name.getMethodName() + ": " + (((float)(System.currentTimeMillis() - startTime)) / 60000));
    }
   
    //@Test
    public void testCreatePDEDiscretizationPolicy() throws DMException {
   	 	DocumentManager docMan = new DocumentManagerImpl();
   	 	String modelId = "";
   	 	String problemId = "";
    	try {
           
            //Loading the problem
    	    String model = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                     getResourceAsStream("/IRO_validation/ccz4_mhd_model_old.xml")), false);
    	    model = docMan.addDocument(model, "");
            Document doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(model));
            modelId = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
            String problem = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/IRO_validation/problem_ccz4_mhd_old.xml")), false);
            problem = docMan.addDocument(problem, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(problem));
            problemId = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
     
            String result = new AGDMImpl().createPDEDiscretizationPolicy(problem);
            result = result.replaceAll("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}", "");
            String comp = AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/resultCreatePDEDiscretizationPolicy.xml"));
            FileWriter fichero = new FileWriter("testCreatePDEDiscretizationPolicy.xml");
            PrintWriter pw = new PrintWriter(fichero);
            pw.write(SimflownyUtils.jsonToXML(result));
            fichero.close();
            String jsonComp = SimflownyUtils.xmlToJSON(comp, false);
            jsonComp = jsonComp.replaceAll("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}", "");

            assertEquals(jsonComp, result);
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Error in test createPDEDiscretizationPolicy: " + e.getMessage());
        }
        finally {
	        docMan.deleteDocument(problemId);
	        docMan.deleteDocument(modelId);
        }
    }
    
   
    @Test
    public void testIntegration1() throws DMException {
    	 DocumentManager docMan = new DocumentManagerImpl();
         String model = "";
         String operator = "";
         String cd = "";
         String cd4 = "";
         String problemID = "";
         String EulerCD4PT = "";
         String Euler = "";
         String cdc4 = "";
         
        try {
            String basePath = "../../../integration tests/1 - Basic";
            
                        
            //Adding the model
            model = docMan.addDocument(SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/models/Advection Equation 2D_1_Carles Bona.xml")), false), "");
            Document doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(model));
            model = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
            
            //Adding schemas
            String loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/CenteredDifference4.xml")), false);
            String added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            cd = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/Euler.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            Euler = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/EulerCD4PT.xml")), false)
                    .replaceAll("08f7b1c4-125b-4287-a0b9-63023dc933f0", cd)
                    .replaceAll("08f7b1c4-125b-4287-a0b9-63023dc933f1", Euler);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            EulerCD4PT = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            //Loading the problem
            String problem = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/problem/Advection with Sinus 2D.xml")), false);
            problem = problem.replaceAll("08f7b1c4-125b-4287-a0b9-63023dc933f0", model);
            added = docMan.addDocument(problem, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            problemID = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();

            //Operator  
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/CenteredDifference4D1.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            cd4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/CenteredDifference4D1Crossed.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        cdc4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
	                 DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
	        
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
            		new FileInputStream(basePath + "/disc_schemes/operator.xml")), false)
                   .replaceAll("CDID", cd).replaceAll("CD4ID", cd4).replaceAll("CD4CID", cdc4);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        operator = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            
            //Loading the discretization Policy
            String policy = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_policy/policy.xml")), false);
            policy = policy.replaceAll("ProblemID", problemID).replaceAll("OPERATORID", operator).replaceAll("schemaID", EulerCD4PT); 
                        
            String result = new AGDMImpl().discretizeProblem(policy);
            result = result.replaceAll("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}", "");
            String comp = AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/discProblem/Advection with Sinus 2D.xml"));
            
            comp = comp.replaceAll("MODELID", model);
            comp = comp.replaceAll("operatorID", operator);
            comp = comp.replaceAll("schemaID", EulerCD4PT);
            comp = comp.replaceAll("CDID", cd);
            String jsonComp = SimflownyUtils.xmlToJSON(comp, false);
            jsonComp = jsonComp.replaceAll("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}", "");
            FileWriter fichero = new FileWriter("testIntegration1.xml");
            PrintWriter pw = new PrintWriter(fichero);
            pw.write(SimflownyUtils.jsonToXML(result));
            fichero.close();
            assertEquals(jsonComp, result);
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Error in Integration test 1: " + e.getMessage());
        }
        finally {
            docMan.deleteDocument(operator);
            docMan.deleteDocument(cd);
            docMan.deleteDocument(cd4);
            docMan.deleteDocument(cdc4);
            docMan.deleteDocument(problemID);
            docMan.deleteDocument(EulerCD4PT);
            docMan.deleteDocument(model);
            docMan.deleteDocument(Euler);
        }
    }
    
    @Test
    public void testIntegration2() throws DMException {
    	 DocumentManager docMan = new DocumentManagerImpl();
         String model = "";
         String operator = "";
         String cd = "";
         String cd4 = "";
         String cdc4 = "";
         String problemID = "";
         String RK3FDOC3 = "";
         String FDOC3 = "";
         String RK3P1 = "";
         String RK3P2 = "";
         String RK3P3 = "";
         String FDOC_prestep = "";
         
        try {
            String basePath = "../../../integration tests/2 - FVM";
            
                        
            //Adding the model
            model = docMan.addDocument(SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/models/Advection Equation 2D_1_Carles Bona.xml")), false), "");
            Document doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(model));
            model = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
            
            //Adding schemas
            String loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/FDOC3.xml")), false);
            String added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            FDOC3 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/RK3P1.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            RK3P1 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/RK3P2.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            RK3P2 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/RK3P3.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            RK3P3 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
                    
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/FDOC_prestep.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            FDOC_prestep = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
                    
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/RK3FDOC3.xml")), false)
                    .replaceAll("PRESTEPID", FDOC_prestep)
                    .replaceAll("FDOCID", FDOC3)
                    .replaceAll("RK3P1ID", RK3P1)
                    .replaceAll("RK3P2ID", RK3P2)
                    .replaceAll("RK3P3ID", RK3P3);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            RK3FDOC3 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            //Loading the problem
            String problem = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/problem/Advection with Sinus 2D.xml")), false);
            problem = problem.replaceAll("2900a7c07f662495000001252fa3de0b", model);
            added = docMan.addDocument(problem, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            problemID = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();

            //Operator  
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/CenteredDifference4.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            cd = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/CenteredDifference4D1.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            cd4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/CenteredDifference4D1Crossed.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        cdc4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
	                 DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
	        
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
            		new FileInputStream(basePath + "/disc_schemes/operator.xml")), false)
                   .replaceAll("CDID", cd).replaceAll("CD4ID", cd4).replaceAll("CD4CID", cdc4);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        operator = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            //Loading the discretization Policy
            String policy = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_policy/policy.xml")), false);
            policy = policy.replaceAll("schemaID", RK3FDOC3).replaceAll("OPERATORID", operator).replaceAll("ProblemID", problemID);  
            
            String result = new AGDMImpl().discretizeProblem(policy);
            result = result.replaceAll("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}", "");
            String comp = AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/discProblem/Advection with Sinus 2D.xml"));
            
            comp = comp.replaceAll("modelID", model);
            comp = comp.replaceAll("operatorID", operator);
            comp = comp.replaceAll("schemaID", RK3FDOC3);
            comp = comp.replaceAll("FDOCID", FDOC3);
            String jsonComp = SimflownyUtils.xmlToJSON(comp, false);
            jsonComp = jsonComp.replaceAll("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}", "");
            FileWriter fichero = new FileWriter("testIntegration2.xml");
            PrintWriter pw = new PrintWriter(fichero);
            pw.write(SimflownyUtils.jsonToXML(result));
            fichero.close();
            assertEquals(jsonComp, result);
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Error in Integration test 2: " + e.getMessage());
        }
        finally {
            docMan.deleteDocument(operator);
            docMan.deleteDocument(cd);
            docMan.deleteDocument(cd4);
            docMan.deleteDocument(cdc4);
            docMan.deleteDocument(problemID);
            docMan.deleteDocument(RK3FDOC3);
            docMan.deleteDocument(model);
            docMan.deleteDocument(FDOC3);
            docMan.deleteDocument(RK3P1);
            docMan.deleteDocument(RK3P2);
            docMan.deleteDocument(RK3P3);
            docMan.deleteDocument(FDOC_prestep);
        }
    }
    
    @Test
    public void testIntegration3() throws DMException {
    	 DocumentManager docMan = new DocumentManagerImpl();
         String model = "";
         String operator = "";
         String cd = "";
         String cd4 = "";
         String cdc4 = "";
         String problemID = "";
         String WENO7RK3 = "";
         String LLFRusanovVRID = "";
         String RK3P1 = "";
         String RK3P2 = "";
         String RK3P3 = "";
         String weno7ID = "";
         String smoothnessIndicator70 = "";
         String smoothnessIndicator71 = "";
         String smoothnessIndicator72 = "";
         String smoothnessIndicator73 = "";
         String nonLinearWeights70 = "";
         String nonLinearWeights71 = "";
         String nonLinearWeights72 = "";
         String nonLinearWeights73 = "";
         String lowOrderReconstruction70 = "";
         String lowOrderReconstruction71 = "";
         String lowOrderReconstruction72 = "";
         String lowOrderReconstruction73 = "";         
         String WENO7_prestep = "";
        try {
            String basePath = "../../../integration tests/3 - Advanced Schema 1D";
            
                        
            //Adding the model
            model = docMan.addDocument(SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/models/Advection Equation 2D_1_Carles Bona.xml")), false), "");
            Document doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(model));
            model = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
            
            //Adding schemas
            String loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/weno7.xml")), false);
            String added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            weno7ID = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/RK3P1.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            RK3P1 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/RK3P2.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            RK3P2 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/RK3P3.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            RK3P3 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/FVM-LLFRusanovVR.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            LLFRusanovVRID = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();         
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/smoothnessIndicator7-0.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            smoothnessIndicator70 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();  
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/smoothnessIndicator7-1.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            smoothnessIndicator71 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();  
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/smoothnessIndicator7-2.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            smoothnessIndicator72 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();  
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/smoothnessIndicator7-3.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            smoothnessIndicator73 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();  
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/nonLinearWeights7-0.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            nonLinearWeights70 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();  
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/nonLinearWeights7-1.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            nonLinearWeights71 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();  
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/nonLinearWeights7-2.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            nonLinearWeights72 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();  
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/nonLinearWeights7-3.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            nonLinearWeights73 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/lowOrderReconstruction7-0.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            lowOrderReconstruction70 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();  
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/lowOrderReconstruction7-1.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            lowOrderReconstruction71 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();  
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/lowOrderReconstruction7-2.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            lowOrderReconstruction72 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();  
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/lowOrderReconstruction7-3.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            lowOrderReconstruction73 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();  
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/weno7_prestep.xml")), false)
            		.replaceAll("WENOID", weno7ID)
                    .replaceAll("smooth1ID", smoothnessIndicator70)
                    .replaceAll("smooth2ID", smoothnessIndicator71)
                    .replaceAll("smooth3ID", smoothnessIndicator72)
                    .replaceAll("smooth4ID", smoothnessIndicator73)
                    .replaceAll("nlw1ID", nonLinearWeights70)
                    .replaceAll("nlw2ID", nonLinearWeights71)
                    .replaceAll("nlw3ID", nonLinearWeights72)
                    .replaceAll("nlw4ID", nonLinearWeights73)
                    .replaceAll("lor1ID", lowOrderReconstruction70)
                    .replaceAll("lor2ID", lowOrderReconstruction71)
                    .replaceAll("lor3ID", lowOrderReconstruction72)
                    .replaceAll("lor4ID", lowOrderReconstruction73);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            WENO7_prestep = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/WENO7RK3.xml")), false)
            	     .replaceAll("PRESTEPID", WENO7_prestep)
                    .replaceAll("RK3P1ID", RK3P1)
                    .replaceAll("RK3P2ID", RK3P2)
                    .replaceAll("RK3P3ID", RK3P3)
                    .replaceAll("LLFID", LLFRusanovVRID);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            WENO7RK3 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            //Loading the problem
            String problem = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/problem/Advection with Balsara-Shu 2D.xml")), false);
            problem = problem.replaceAll("2900a7c07f662495000001252fa3de0b", model);
            added = docMan.addDocument(problem, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            problemID = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();

            //Operator  
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/CenteredDifference4.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            cd = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/CenteredDifference4D1.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            cd4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/CenteredDifference4D1Crossed.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        cdc4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
	                 DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
	        
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
            		new FileInputStream(basePath + "/disc_schemes/operator.xml")), false)
                   .replaceAll("CDID", cd).replaceAll("CD4ID", cd4).replaceAll("CD4CID", cdc4);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        operator = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            //Loading the discretization Policy
            String policy = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_policy/policy.xml")), false);
            policy = policy.replaceAll("schemaID", WENO7RK3).replaceAll("OPERATORID", operator).replaceAll("ProblemID", problemID);  
            
            String result = new AGDMImpl().discretizeProblem(policy);
            result = result.replaceAll("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}", "");
            String comp = AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/discProblem/Advection with Balsara-Shu 2D.xml"));
            
            comp = comp.replaceAll("modelID", model);
            comp = comp.replaceAll("operatorID", operator);
            comp = comp.replaceAll("schemaID", WENO7RK3);
            comp = comp.replaceAll("LLFID", LLFRusanovVRID);
            String jsonComp = SimflownyUtils.xmlToJSON(comp, false);
            jsonComp = jsonComp.replaceAll("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}", "");
            FileWriter fichero = new FileWriter("testIntegration3.xml");
            PrintWriter pw = new PrintWriter(fichero);
            pw.write(SimflownyUtils.jsonToXML(result));
            fichero.close();
            assertEquals(jsonComp, result);
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Error in Integration test 3: " + e.getMessage());
        }
        finally {
            docMan.deleteDocument(operator);
            docMan.deleteDocument(cd);
            docMan.deleteDocument(cd4);
            docMan.deleteDocument(cdc4);
            docMan.deleteDocument(problemID);
            docMan.deleteDocument(WENO7RK3);
            docMan.deleteDocument(model);
            docMan.deleteDocument(LLFRusanovVRID);
            docMan.deleteDocument(RK3P1);
            docMan.deleteDocument(RK3P2);
            docMan.deleteDocument(RK3P3);
            docMan.deleteDocument(weno7ID);
            docMan.deleteDocument(smoothnessIndicator70);
            docMan.deleteDocument(smoothnessIndicator71);
            docMan.deleteDocument(smoothnessIndicator72);
            docMan.deleteDocument(smoothnessIndicator73);
            docMan.deleteDocument(nonLinearWeights70);
            docMan.deleteDocument(nonLinearWeights71);
            docMan.deleteDocument(nonLinearWeights72);
            docMan.deleteDocument(nonLinearWeights73);
            docMan.deleteDocument(lowOrderReconstruction70);
            docMan.deleteDocument(lowOrderReconstruction71);
            docMan.deleteDocument(lowOrderReconstruction72);
            docMan.deleteDocument(lowOrderReconstruction73);
            docMan.deleteDocument(WENO7_prestep);
        }
    }
    
    @Test
    public void testIntegration4() throws DMException {
    	 DocumentManager docMan = new DocumentManagerImpl();
         String model = "";
         String operator = "";
         String cd = "";
         String cd4 = "";
         String cdc4 = "";
         String problemID = "";
         String RK3FDOC3 = "";
         String FDOC3 = "";
         String RK3P1 = "";
         String RK3P2 = "";
         String RK3P3 = "";
         String FDOC_prestep = "";
        try {
            String basePath = "../../../integration tests/4 - Parabolic Terms";
 
            //Adding the model
            model = docMan.addDocument(SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/models/Heat Equation 2D_1_Toni Arbona.xml")), false), "");
            Document doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(model));
            model = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
            
            //Adding schemas
            String loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/FDOC3.xml")), false);
            String added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            FDOC3 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/RK3P1.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            RK3P1 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/RK3P2.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            RK3P2 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/RK3P3.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            RK3P3 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/FDOC_prestep.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            FDOC_prestep = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/RK3FDOC3.xml")), false)
            		.replaceAll("PRESTEPID", FDOC_prestep)
                    .replaceAll("FDOCID", FDOC3)
                    .replaceAll("RK3P1ID", RK3P1)
                    .replaceAll("RK3P2ID", RK3P2)
                    .replaceAll("RK3P3ID", RK3P3);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            RK3FDOC3 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            //Loading the problem
            String problem = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/problem/Heat Problem 2D.xml")), false);
            problem = problem.replaceAll("2800a7c07f6624950000012679f6e134", model);
            added = docMan.addDocument(problem, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            problemID = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();

            //Operator  
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/CenteredDifference4.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            cd4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/CenteredDifference4D1.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            cd = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/CenteredDifference4D1Crossed.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        cdc4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
	                 DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
	        
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
            		new FileInputStream(basePath + "/disc_schemes/operator.xml")), false)
                   .replaceAll("CDID", cd).replaceAll("CD4ID", cd4).replaceAll("CD4CID", cdc4);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        operator = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
	        
            //Loading the discretization Policy
            String policy = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_policy/policy.xml")), false);
            policy = policy.replaceAll("schemaID", RK3FDOC3).replaceAll("OPERATORID", operator).replaceAll("ProblemID", problemID);  
            
            String result = new AGDMImpl().discretizeProblem(policy);
            result = result.replaceAll("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}", "");
            String comp = AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/discProblem/Heat Problem 2D.xml"));
            
            comp = comp.replaceAll("modelID", model);
            comp = comp.replaceAll("operatorID", operator);
            comp = comp.replaceAll("schemaID", RK3FDOC3);
            comp = comp.replaceAll("FDOCID", FDOC3);
            String jsonComp = SimflownyUtils.xmlToJSON(comp, false);
            jsonComp = jsonComp.replaceAll("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}", "");
            FileWriter fichero = new FileWriter("testIntegration4.xml");
            PrintWriter pw = new PrintWriter(fichero);
            pw.write(SimflownyUtils.jsonToXML(result));
            fichero.close();
            assertEquals(jsonComp, result);
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Error in Integration test 4: " + e.getMessage());
        }
        finally {
            docMan.deleteDocument(operator);
            docMan.deleteDocument(cd);
            docMan.deleteDocument(cd4);
            docMan.deleteDocument(cdc4);
            docMan.deleteDocument(problemID);
            docMan.deleteDocument(RK3FDOC3);
            docMan.deleteDocument(model);
            docMan.deleteDocument(FDOC3);
            docMan.deleteDocument(RK3P1);
            docMan.deleteDocument(RK3P2);
            docMan.deleteDocument(RK3P3);
            docMan.deleteDocument(FDOC_prestep);
        }
    }
    
    @Test
    public void testIntegration5() throws DMException {
    	 DocumentManager docMan = new DocumentManagerImpl();
         String model = "";
         String operator = "";
         String cd = "";
         String cd4 = "";
         String cdc4 = "";
         String problemID = "";
         String RK3FDOC3 = "";
         String FDOC3 = "";
         String RK3P1 = "";
         String RK3P2 = "";
         String RK3P3 = "";
         String FDOC_prestep = "";
        try {
            String basePath = "../../../integration tests/5 - 2D and Outflow Boundaries";
            
                        
            //Adding the model
            model = docMan.addDocument(SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/models/Wave Equation 2D_1_Toni Arbona.xml")), false), "");
            Document doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(model));
            model = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
            
            //Adding schemas
            String loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/FDOC3.xml")), false);
            String added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            FDOC3 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/RK3P1.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            RK3P1 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/RK3P2.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            RK3P2 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/RK3P3.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            RK3P3 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/FDOC_prestep.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            FDOC_prestep = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/RK3FDOC3.xml")), false)
            		.replaceAll("PRESTEPID", FDOC_prestep)
                    .replaceAll("FDOCID", FDOC3)
                    .replaceAll("RK3P1ID", RK3P1)
                    .replaceAll("RK3P2ID", RK3P2)
                    .replaceAll("RK3P3ID", RK3P3);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            RK3FDOC3 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            //Loading the problem
            String problem = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/problem/Wave Equation 2D central Gaussian profile.xml")), false);
            problem = problem.replaceAll("2900a7c07f66249500000124956c50c1", model);
            added = docMan.addDocument(problem, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            problemID = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();

            //Operator  
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/CenteredDifference4.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            cd4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/CenteredDifference4D1.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            cd = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/CenteredDifference4D1Crossed.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        cdc4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
	                 DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
	        
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
            		new FileInputStream(basePath + "/disc_schemes/operator.xml")), false)
                   .replaceAll("CDID", cd).replaceAll("CD4ID", cd4).replaceAll("CD4CID", cdc4);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        operator = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            //Loading the discretization Policy
            String policy = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_policy/policy.xml")), false);
            policy = policy.replaceAll("schemaID", RK3FDOC3).replaceAll("OPERATORID", operator).replaceAll("ProblemID", problemID);  
            
            String result = new AGDMImpl().discretizeProblem(policy);
            result = result.replaceAll("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}", "");
            String comp = AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/discProblem/Wave Equation 2D central Gaussian profile.xml"));
            
            comp = comp.replaceAll("modelID", model);
            comp = comp.replaceAll("operatorID", operator);
            comp = comp.replaceAll("schemaID", RK3FDOC3);
            comp = comp.replaceAll("FDOCID", FDOC3);
            String jsonComp = SimflownyUtils.xmlToJSON(comp, false);
            jsonComp = jsonComp.replaceAll("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}", "");
            FileWriter fichero = new FileWriter("testIntegration5.xml");
            PrintWriter pw = new PrintWriter(fichero);
            pw.write(SimflownyUtils.jsonToXML(result));
            fichero.close();
            assertEquals(jsonComp, result);
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Error in Integration test 5: " + e.getMessage());
        }
        finally {
            docMan.deleteDocument(operator);
            docMan.deleteDocument(cd);
            docMan.deleteDocument(cd4);
            docMan.deleteDocument(cdc4);
            docMan.deleteDocument(problemID);
            docMan.deleteDocument(RK3FDOC3);
            docMan.deleteDocument(model);
            docMan.deleteDocument(FDOC3);
            docMan.deleteDocument(RK3P1);
            docMan.deleteDocument(RK3P2);
            docMan.deleteDocument(RK3P3);
            docMan.deleteDocument(FDOC_prestep);
        }
    }
    
    @Test
    public void testIntegration6() throws DMException {
    	 DocumentManager docMan = new DocumentManagerImpl();
         String model = "";
         String operator = "";
         String cd = "";
         String cd4 = "";
         String cdc4 = "";
         String problemID = "";
         String RK3FDOC3 = "";
         String FDOC3 = "";
         String RK3P1 = "";
         String RK3P2 = "";
         String RK3P3 = "";
         String FDOC_prestep = "";
        try {
            String basePath = "../../../integration tests/6 - Advanced Boundaries and Shock Management";
            
                        
            //Adding the model
            model = docMan.addDocument(SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/models/Euler 2D_1_Carles Bona.xml")), false), "");
            Document doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(model));
            model = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
            
            //Adding schemas
            String loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/FDOC3.xml")), false);
            String added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            FDOC3 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/RK3P1.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            RK3P1 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/RK3P2.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            RK3P2 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/RK3P3.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            RK3P3 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/FDOC_prestep.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            FDOC_prestep = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/RK3FDOC3.xml")), false)
            		.replaceAll("PRESTEPID", FDOC_prestep)
                    .replaceAll("FDOCID", FDOC3)
                    .replaceAll("RK3P1ID", RK3P1)
                    .replaceAll("RK3P2ID", RK3P2)
                    .replaceAll("RK3P3ID", RK3P3);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            RK3FDOC3 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            //Loading the problem
            String problem = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/problem/Euler with double mach.xml")), false);
            problem = problem.replaceAll("00000000-0000-4000-8888-000000000000", model);
            added = docMan.addDocument(problem, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            problemID = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();

          //Operator  
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/CenteredDifference4.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            cd4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/CenteredDifference4D1.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            cd = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/CenteredDifference4D1Crossed.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        cdc4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
	                 DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
	        
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
            		new FileInputStream(basePath + "/disc_schemes/operator.xml")), false)
                   .replaceAll("CDID", cd).replaceAll("CD4ID", cd4).replaceAll("CD4CID", cdc4);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        operator = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            //Loading the discretization Policy
            String policy = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_policy/policy.xml")), false);
            policy = policy.replaceAll("schemaID", RK3FDOC3).replaceAll("OPERATORID", operator).replaceAll("ProblemID", problemID);  
            
            String result = new AGDMImpl().discretizeProblem(policy);
            result = result.replaceAll("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}", "");
            String comp = AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/discProblem/Euler with double mach.xml"));
            
            comp = comp.replaceAll("modelID", model);
            comp = comp.replaceAll("operatorID", operator);
            comp = comp.replaceAll("schemaID", RK3FDOC3);
            comp = comp.replaceAll("FDOCID", FDOC3);
            String jsonComp = SimflownyUtils.xmlToJSON(comp, false);
            jsonComp = jsonComp.replaceAll("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}", "");
            FileWriter fichero = new FileWriter("testIntegration6.xml");
            PrintWriter pw = new PrintWriter(fichero);
            pw.write(SimflownyUtils.jsonToXML(result));
            fichero.close();
            assertEquals(jsonComp, result);
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Error in Integration test 6: " + e.getMessage());
        }
        finally {
            docMan.deleteDocument(operator);
            docMan.deleteDocument(cd);
            docMan.deleteDocument(cd4);
            docMan.deleteDocument(cdc4);
            docMan.deleteDocument(problemID);
            docMan.deleteDocument(RK3FDOC3);
            docMan.deleteDocument(model);
            docMan.deleteDocument(FDOC3);
            docMan.deleteDocument(RK3P1);
            docMan.deleteDocument(RK3P2);
            docMan.deleteDocument(RK3P3);
            docMan.deleteDocument(FDOC_prestep);
        }
    }
    
    @Test
    public void testIntegration7() throws DMException {
    	 DocumentManager docMan = new DocumentManagerImpl();
         String model = "";
         String operator = "";
         String cd = "";
         String cd4 = "";
         String cdc4 = "";
         String problemID = "";
         String WENO7RK3 = "";
         String LLFRusanovVRID = "";
         String RK3P1 = "";
         String RK3P2 = "";
         String RK3P3 = "";
         String weno7ID = "";
         String smoothnessIndicator70 = "";
         String smoothnessIndicator71 = "";
         String smoothnessIndicator72 = "";
         String smoothnessIndicator73 = "";
         String nonLinearWeights70 = "";
         String nonLinearWeights71 = "";
         String nonLinearWeights72 = "";
         String nonLinearWeights73 = "";
         String lowOrderReconstruction70 = "";
         String lowOrderReconstruction71 = "";
         String lowOrderReconstruction72 = "";
         String lowOrderReconstruction73 = ""; 
         String WENO7_prestep = "";
        try {
            String basePath = "../../../integration tests/7 - Advanced Schema 2D";
            
                        
            //Adding the model
            model = docMan.addDocument(SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/models/Euler 2D_1_Carles Bona.xml")), false), "");
            Document doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(model));
            model = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
            
            //Adding schemas
            String loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/weno7.xml")), false);
            String added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            weno7ID = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/RK3P1.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            RK3P1 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/RK3P2.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            RK3P2 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/RK3P3.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            RK3P3 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/FVM-LLFRusanovVR.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            LLFRusanovVRID = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();         
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/smoothnessIndicator7-0.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            smoothnessIndicator70 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();  
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/smoothnessIndicator7-1.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            smoothnessIndicator71 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();  
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/smoothnessIndicator7-2.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            smoothnessIndicator72 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();  
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/smoothnessIndicator7-3.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            smoothnessIndicator73 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();  
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/nonLinearWeights7-0.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            nonLinearWeights70 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();  
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/nonLinearWeights7-1.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            nonLinearWeights71 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();  
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/nonLinearWeights7-2.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            nonLinearWeights72 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();  
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/nonLinearWeights7-3.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            nonLinearWeights73 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/lowOrderReconstruction7-0.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            lowOrderReconstruction70 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();  
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/lowOrderReconstruction7-1.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            lowOrderReconstruction71 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();  
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/lowOrderReconstruction7-2.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            lowOrderReconstruction72 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();  
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/lowOrderReconstruction7-3.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            lowOrderReconstruction73 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();  
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/weno7_prestep.xml")), false)
            		.replaceAll("WENOID", weno7ID)
                    .replaceAll("smooth1ID", smoothnessIndicator70)
                    .replaceAll("smooth2ID", smoothnessIndicator71)
                    .replaceAll("smooth3ID", smoothnessIndicator72)
                    .replaceAll("smooth4ID", smoothnessIndicator73)
                    .replaceAll("nlw1ID", nonLinearWeights70)
                    .replaceAll("nlw2ID", nonLinearWeights71)
                    .replaceAll("nlw3ID", nonLinearWeights72)
                    .replaceAll("nlw4ID", nonLinearWeights73)
                    .replaceAll("lor1ID", lowOrderReconstruction70)
                    .replaceAll("lor2ID", lowOrderReconstruction71)
                    .replaceAll("lor3ID", lowOrderReconstruction72)
                    .replaceAll("lor4ID", lowOrderReconstruction73);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            WENO7_prestep = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/WENO7RK3.xml")), false)
           	     	.replaceAll("PRESTEPID", WENO7_prestep)
                    .replaceAll("RK3P1ID", RK3P1)
                    .replaceAll("RK3P2ID", RK3P2)
                    .replaceAll("RK3P3ID", RK3P3)
                    .replaceAll("LLFID", LLFRusanovVRID);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            WENO7RK3 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            //Loading the problem
            String problem = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/problem/Euler Periodic Vortex.xml")), false);
            problem = problem.replaceAll("2900a7c07f6624950000012452ec79f3", model);
            added = docMan.addDocument(problem, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            problemID = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();

            //Operator  
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/CenteredDifference4.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            cd4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/CenteredDifference4D1.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            cd = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/CenteredDifference4D1Crossed.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        cdc4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
	                 DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
	        
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
            		new FileInputStream(basePath + "/disc_schemes/operator.xml")), false)
                   .replaceAll("CDID", cd).replaceAll("CD4ID", cd4).replaceAll("CD4CID", cdc4);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        operator = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            //Loading the discretization Policy
            String policy = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_policy/policy.xml")), false);
            policy = policy.replaceAll("schemaID", WENO7RK3).replaceAll("OPERATORID", operator).replaceAll("ProblemID", problemID);  
            
            String result = new AGDMImpl().discretizeProblem(policy);
            result = result.replaceAll("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}", "");
            String comp = AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/discProblem/Euler Periodic Vortex.xml"));
            
            comp = comp.replaceAll("modelID", model);
            comp = comp.replaceAll("operatorID", operator);
            comp = comp.replaceAll("schemaID", WENO7RK3);
            comp = comp.replaceAll("LLFID", LLFRusanovVRID);
            String jsonComp = SimflownyUtils.xmlToJSON(comp, false);
            jsonComp = jsonComp.replaceAll("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}", "");
            FileWriter fichero = new FileWriter("testIntegration7.xml");
            PrintWriter pw = new PrintWriter(fichero);
            pw.write(SimflownyUtils.jsonToXML(result));
            fichero.close();
            assertEquals(jsonComp, result);
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Error in Integration test 7: " + e.getMessage());
        }
        finally {
            docMan.deleteDocument(operator);
            docMan.deleteDocument(cd);
            docMan.deleteDocument(cd4);
            docMan.deleteDocument(cdc4);
            docMan.deleteDocument(problemID);
            docMan.deleteDocument(WENO7RK3);
            docMan.deleteDocument(model);
            docMan.deleteDocument(LLFRusanovVRID);
            docMan.deleteDocument(RK3P1);
            docMan.deleteDocument(RK3P2);
            docMan.deleteDocument(RK3P3);
            docMan.deleteDocument(weno7ID);
            docMan.deleteDocument(smoothnessIndicator70);
            docMan.deleteDocument(smoothnessIndicator71);
            docMan.deleteDocument(smoothnessIndicator72);
            docMan.deleteDocument(smoothnessIndicator73);
            docMan.deleteDocument(nonLinearWeights70);
            docMan.deleteDocument(nonLinearWeights71);
            docMan.deleteDocument(nonLinearWeights72);
            docMan.deleteDocument(nonLinearWeights73);
            docMan.deleteDocument(lowOrderReconstruction70);
            docMan.deleteDocument(lowOrderReconstruction71);
            docMan.deleteDocument(lowOrderReconstruction72);
            docMan.deleteDocument(lowOrderReconstruction73);
            docMan.deleteDocument(WENO7_prestep);
        }
    }
    
    @Test
    public void testIntegration8() throws DMException {
    	DocumentManager docMan = new DocumentManagerImpl();
        String model = "";
        String operator = "";
        String cd = "";
        String cd4 = "";
        String cdc4 = "";
        String problemID = "";
        String RK3FDOC3 = "";
        String FDOC3 = "";
        String RK3P1 = "";
        String RK3P2 = "";
        String RK3P3 = "";
        String FDOC_prestep = "";
        try {
            String basePath = "../../../integration tests/8 - Non-reflecting Boundaries and Hyperbolic-Parabolic";
            
                        
            //Adding the model
            model = docMan.addDocument(SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/models/Navier-Stokes liquids 2D_1_Toni Arbona.xml")), false), "");
            Document doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(model));
            model = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
            
            //Adding schemas
            String loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/FDOC3.xml")), false);
            String added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            FDOC3 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/RK3P1.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            RK3P1 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/RK3P2.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            RK3P2 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/RK3P3.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            RK3P3 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/FDOC_prestep.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            FDOC_prestep = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/RK3FDOC3.xml")), false)
            		.replaceAll("PRESTEPID", FDOC_prestep)
                    .replaceAll("FDOCID", FDOC3)
                    .replaceAll("RK3P1ID", RK3P1)
                    .replaceAll("RK3P2ID", RK3P2)
                    .replaceAll("RK3P3ID", RK3P3);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            RK3FDOC3 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            //Loading the problem
            String problem = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/problem/Compressible Counterflow 2D.xml")), false);
            problem = problem.replaceAll("2900a7c07f662495000001301c0e1a2e", model);
            added = docMan.addDocument(problem, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            problemID = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
    
            //Operator  
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/CenteredDifference4.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            cd4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/CenteredDifference4D1.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            cd = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/CenteredDifference4D1Crossed.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        cdc4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
	                 DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
	        
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
            		new FileInputStream(basePath + "/disc_schemes/operator.xml")), false)
                   .replaceAll("CDID", cd).replaceAll("CD4ID", cd4).replaceAll("CD4CID", cdc4);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        operator = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            //Loading the discretization Policy
            String policy = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_policy/policy.xml")), false);
            policy = policy.replaceAll("schemaID", RK3FDOC3).replaceAll("OPERATORID", operator).replaceAll("ProblemID", problemID);
            
            String result = new AGDMImpl().discretizeProblem(policy);
            result = result.replaceAll("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}", "");
            String comp = AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/discProblem/Compressible Counterflow 2D.xml"));
            
            comp = comp.replaceAll("modelID", model);
            comp = comp.replaceAll("operatorID", operator);
            comp = comp.replaceAll("schemaID", RK3FDOC3);
            comp = comp.replaceAll("FDOCID", FDOC3);
            String jsonComp = SimflownyUtils.xmlToJSON(comp, false);
            jsonComp = jsonComp.replaceAll("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}", "");
            FileWriter fichero = new FileWriter("testIntegration8.xml");
            PrintWriter pw = new PrintWriter(fichero);
            pw.write(SimflownyUtils.jsonToXML(result));
            fichero.close();
            assertEquals(jsonComp, result);
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Error in Integration test 8: " + e.getMessage());
        }
        finally {
            docMan.deleteDocument(operator);
            docMan.deleteDocument(cd);
            docMan.deleteDocument(cd4);
            docMan.deleteDocument(cdc4);
            docMan.deleteDocument(problemID);
            docMan.deleteDocument(RK3FDOC3);
            docMan.deleteDocument(model);
            docMan.deleteDocument(FDOC3);
            docMan.deleteDocument(RK3P1);
            docMan.deleteDocument(RK3P2);
            docMan.deleteDocument(RK3P3);
            docMan.deleteDocument(FDOC_prestep);
        }
    }
   
    @Test
    public void testIntegration9() throws DMException {
    	 DocumentManager docMan = new DocumentManagerImpl();
         String model = "";
         String operator = "";
         String cd = "";
         String cd4 = "";
         String problemID = "";
         String EulerCD4PT = "";
         String Euler = "";
         String cdc4 = "";
         
        try {
            String basePath = "../../../integration tests/9 - AuxiliaryVariables";
            
                        
            //Adding the model
            model = docMan.addDocument(SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/models/model.xml")), false), "");
            Document doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(model));
            model = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
            
            //Adding schemas
            String loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/CenteredDifference4.xml")), false);
            String added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            cd = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/Euler.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            Euler = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/EulerCD4PT.xml")), false)
                    .replaceAll("08f7b1c4-125b-4287-a0b9-63023dc933f0", cd)
                    .replaceAll("08f7b1c4-125b-4287-a0b9-63023dc933f1", Euler);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            EulerCD4PT = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            //Loading the problem
            String problem = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/problem/problem.xml")), false);
            problem = problem.replaceAll("MODELID", model);
            added = docMan.addDocument(problem, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            problemID = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();

            //Operator  
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/CenteredDifference4D1.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            cd4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/CenteredDifference4D1Crossed.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        cdc4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
	                 DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
	        
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
            		new FileInputStream(basePath + "/disc_schemes/operator.xml")), false)
                   .replaceAll("CDID", cd).replaceAll("CD4ID", cd4).replaceAll("CD4CID", cdc4);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        operator = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            
            //Loading the discretization Policy
            String policy = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_policy/policy.xml")), false);
            policy = policy.replaceAll("ProblemID", problemID).replaceAll("OPERATORID", operator).replaceAll("schemaID", EulerCD4PT); 
                        
            String result = new AGDMImpl().discretizeProblem(policy);
            result = result.replaceAll("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}", "");
            String comp = AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/discProblem/discProblem.xml"));
            
            comp = comp.replaceAll("MODELID", model);
            comp = comp.replaceAll("operatorID", operator);
            comp = comp.replaceAll("schemaID", EulerCD4PT);
            comp = comp.replaceAll("CDID", cd);
            String jsonComp = SimflownyUtils.xmlToJSON(comp, false);
            jsonComp = jsonComp.replaceAll("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}", "");
            FileWriter fichero = new FileWriter("testIntegration9.xml");
            PrintWriter pw = new PrintWriter(fichero);
            pw.write(SimflownyUtils.jsonToXML(result));
            fichero.close();
            assertEquals(jsonComp, result);
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Error in Integration test 9: " + e.getMessage());
        }
        finally {
            docMan.deleteDocument(operator);
            docMan.deleteDocument(cd);
            docMan.deleteDocument(cd4);
            docMan.deleteDocument(cdc4);
            docMan.deleteDocument(problemID);
            docMan.deleteDocument(EulerCD4PT);
            docMan.deleteDocument(model);
            docMan.deleteDocument(Euler);
        }
    }
    
    @Test
    public void testIntegration10() throws DMException {
        DocumentManager docMan = new DocumentManagerImpl();
        String model = "";
        String operator = "";
        String cd = "";
        String cd4 = "";
        String cdc4 = "";
        String problemID = "";
        String RK3FDOC3 = "";
        String FDOC3 = "";
        String RK3P1 = "";
        String RK3P2 = "";
        String RK3P3 = "";
        String FDOC_prestep = "";
        try {
            String basePath = "../../../integration tests/10 - 3D Extrapolations and Segment Interaction";
            

            //Adding the model
            model = docMan.addDocument(SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                     new FileInputStream(basePath + "/models/Navier-Stokes liquids 3D_1_Toni Arbona.xml")), false), "");
            Document doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(model));
            model = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                     "id").item(0).getTextContent();
            String model2 = docMan.addDocument(SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                     new FileInputStream(basePath + "/models/Null 3D_1_Miquel Trias.xml")), false), "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(model2));
            model2 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                     "id").item(0).getTextContent();
             
          //Adding schemas
            String loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/FDOC3.xml")), false);
            String added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            FDOC3 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/RK3P1.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            RK3P1 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/RK3P2.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            RK3P2 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/RK3P3.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            RK3P3 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/FDOC_prestep.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            FDOC_prestep = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/RK3FDOC3.xml")), false)
            		.replaceAll("PRESTEPID", FDOC_prestep)
                    .replaceAll("FDOCID", FDOC3)
                    .replaceAll("RK3P1ID", RK3P1)
                    .replaceAll("RK3P2ID", RK3P2)
                    .replaceAll("RK3P3ID", RK3P3);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            RK3FDOC3 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
             
            //Region
            String ptSoil = docMan.importX3D(SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/regions/planex3d.xml")), false), AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/regions/Plane.x3d")), "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(ptSoil));
            ptSoil = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
            
            //Loading the problem
            String problem = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                     new FileInputStream(basePath + "/problem/FDAsNozzle 3D.xml")), false);
            problem = problem.replaceAll("2900a7c07f662495000001301c4c2667", model).replaceAll("2b00a7c07f66249500000136634f5fee", model2).replaceAll("SEGMENTID", ptSoil);
            added = docMan.addDocument(problem, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            problemID = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                     "id").item(0).getTextContent();
     
            //Operator  
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/CenteredDifference4.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            cd = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/CenteredDifference4D2.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            cd4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/CenteredDifference4D2Crossed.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        cdc4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
	                 DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
	        
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
            		new FileInputStream(basePath + "/disc_schemes/operator.xml")), false)
                   .replaceAll("CDID", cd).replaceAll("CD4ID", cd4).replaceAll("CD4CID", cdc4);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        operator = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            //Loading the discretization Policy
            String policy = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                     new FileInputStream(basePath + "/disc_policy/policy.xml")), false);
            policy = policy.replaceAll("schemaID", RK3FDOC3).replaceAll("OPERATORID", operator).replaceAll("ProblemID", problemID);
             
            String result = new AGDMImpl().discretizeProblem(policy);
            
            result = result.replaceAll("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}", "");
            String comp = AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/discProblem/FDAsNozzle 3D.xml"));
            
            comp = comp.replaceAll("modelID", model);
            comp = comp.replaceAll("model2ID", model2);
            comp = comp.replaceAll("operatorID", operator);
            comp = comp.replaceAll("SEGMENTID", ptSoil);
            comp = comp.replaceAll("schemaID", RK3FDOC3);
            comp = comp.replaceAll("FDOCID", FDOC3);
            String jsonComp = SimflownyUtils.xmlToJSON(comp, false);
            jsonComp = jsonComp.replaceAll("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}", "");
            FileWriter fichero = new FileWriter("testIntegration10.xml");
            PrintWriter pw = new PrintWriter(fichero);
            pw.write(SimflownyUtils.jsonToXML(result));
            fichero.close();
            assertEquals(jsonComp, result);
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Error in Integration test 10: " + e.getMessage());
        }
        finally {
            docMan.deleteDocument(operator);
            docMan.deleteDocument(cd);
            docMan.deleteDocument(cd4);
            docMan.deleteDocument(cdc4);
            docMan.deleteDocument(problemID);
            docMan.deleteDocument(RK3FDOC3);
            docMan.deleteDocument(model);
            docMan.deleteDocument(FDOC3);
            docMan.deleteDocument(RK3P1);
            docMan.deleteDocument(RK3P2);
            docMan.deleteDocument(RK3P3);
            docMan.deleteDocument(FDOC_prestep);
        }
    }
    
    @Test
    public void testIntegration11() throws DMException {
    	DocumentManager docMan = new DocumentManagerImpl();
        String corrector = "";
        String ko = "";
        String sphdiffusion = "";
        String problemID = "";
        String model = "";
        String gradWendland = "";
        String sphSym = "";
        String sphAsym = "";
        String sphAsym2 = "";
        String interp = "";
        String interpNorm = "";
        String wedland = "";
        String sphNorm = "";
        String operatorSym = "";
        String operatorAsym = "";
        String predictor = "";
        try {
            String basePath = "../../../integration tests/11 - SPH";
            
                         
            //Adding the model
            model = docMan.addDocument(SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                     new FileInputStream(basePath + "/models/Navier-Stokes liquids 2D_1_Toni Arbona.xml")), false), "");
            Document doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(model));
            model = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                     "id").item(0).getTextContent();
            
            //Adding schemas
            String loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                   new FileInputStream(basePath + "/disc_schemes/Corrector step.xml")), false);
            String added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            corrector = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                   new FileInputStream(basePath + "/disc_schemes/Gradient Wendland.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            gradWendland = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                   new FileInputStream(basePath + "/disc_schemes/KO-dissipation-order3.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            ko = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                   new FileInputStream(basePath + "/disc_schemes/Predictor step.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            predictor = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                   new FileInputStream(basePath + "/disc_schemes/SPH-1st Symmetric.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            sphSym = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/SPH-1st Antisymmetric.xml")), false);
             added = docMan.addDocument(loadDB, "");
             doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
             sphAsym = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
             loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                     new FileInputStream(basePath + "/disc_schemes/SPH-2nd Antisymmetric.xml")), false);
              added = docMan.addDocument(loadDB, "");
              doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
              sphAsym2 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                     DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/SPH-1st Normalization.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            sphNorm = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                     new FileInputStream(basePath + "/disc_schemes/SPH interpolation function.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            interp = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                     DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                      new FileInputStream(basePath + "/disc_schemes/SPH normalization function.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            interpNorm = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                      DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                       new FileInputStream(basePath + "/disc_schemes/Wendland.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            wedland = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                       DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                   new FileInputStream(basePath + "/disc_schemes/Predictor Corrector SPH.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            sphdiffusion = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
    
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/OpSPHSymmetric.xml")), false);
             added = docMan.addDocument(loadDB, "");
             doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
             operatorSym = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
             
             loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                     new FileInputStream(basePath + "/disc_schemes/OpSPHAntisymmetric.xml")), false);
              added = docMan.addDocument(loadDB, "");
              doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
              operatorAsym = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                     DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
             
            //Loading the problem
            String problem = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/problem/Dam break 2D.xml")), false);
            problem = problem.replaceAll("2900a7c07f662495000001301c0e1a2e", model);
            added = docMan.addDocument(problem, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            problemID = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
    
            //Loading the discretization Policy
            String policy = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_policy/policy.xml")), false);
            policy = policy.replaceAll("schemaID", sphdiffusion).replaceAll("ProblemID", problemID);  
            
            String result = new AGDMImpl().discretizeProblem(policy);
            result = result.replaceAll("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}", "");
            String comp = AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/discProblem/Dam break 2D.xml"));
            
            comp = comp.replaceAll("modelID", model);
            comp = comp.replaceAll("schemaID", sphdiffusion);
            String jsonComp = SimflownyUtils.xmlToJSON(comp, false);
            jsonComp = jsonComp.replaceAll("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}", "");
            FileWriter fichero = new FileWriter("testIntegration11.xml");
            PrintWriter pw = new PrintWriter(fichero);
            pw.write(SimflownyUtils.jsonToXML(result));
            fichero.close();
            assertEquals(jsonComp, result);
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Error in Integration test 11: " + e.getMessage());
        }
        finally {
        	docMan.deleteDocument(operatorSym);
        	docMan.deleteDocument(operatorAsym);
        	docMan.deleteDocument(interp);
        	docMan.deleteDocument(interpNorm);
        	docMan.deleteDocument(wedland);
        	docMan.deleteDocument(sphNorm);
            docMan.deleteDocument(sphSym);
            docMan.deleteDocument(sphAsym);
            docMan.deleteDocument(sphAsym2);
            docMan.deleteDocument(predictor);
            docMan.deleteDocument(ko);
            docMan.deleteDocument(problemID);
            docMan.deleteDocument(sphdiffusion);
            docMan.deleteDocument(model);
            docMan.deleteDocument(gradWendland);
            docMan.deleteDocument(corrector);
        }
    }
   
    @Test
    public void testIntegration14() throws DMException {
    	DocumentManager docMan = new DocumentManagerImpl();
        String model = "";
        String operator = "";
        String schema = "";
        String cd1st = "";
        String cd2nd = "";
        String problemID = "";
        String RK3P1 = "";
        String RK3P2 = "";
        String RK3P3 = "";
        
        try {
            String basePath = "../../../integration tests/14 - genericPDE";
            
                         
            //Adding the model
            model = docMan.addDocument(SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                     new FileInputStream(basePath + "/models/modelWaveSecondOrderGauge.xml")), false), "");
            Document doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(model));
            model = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                     "id").item(0).getTextContent();
             
             //Adding schemas
            String loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/CD4-1stOrder.xml")), false);
            String added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            cd1st = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/CD4-2ndOrder.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            cd2nd = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/RK3P1.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            RK3P1 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/RK3P2.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            RK3P2 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/RK3P3.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            RK3P3 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/schema.xml")), false)
                    .replaceAll("RK3P1ID", RK3P1)
                    .replaceAll("RK3P2ID", RK3P2)
                    .replaceAll("RK3P3ID", RK3P3);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            schema = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            //Loading the operator
            operator = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                     new FileInputStream(basePath + "/disc_policy/operator.xml")), false);
            operator = operator.replaceAll("CDID", cd1st);
            operator = operator.replaceAll("CD4ID", cd2nd);
            added = docMan.addDocument(operator, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            operator = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                     "id").item(0).getTextContent();
            
            //Loading the problem
            String problem = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                     new FileInputStream(basePath + "/problem/problemWaveSecondOrderGauge.xml")), false);
            problem = problem.replaceAll("WAVE2DID", model);
            added = docMan.addDocument(problem, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            problemID = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                     "id").item(0).getTextContent();
     
            //Loading the discretization Policy
            String policy = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                     new FileInputStream(basePath + "/disc_policy/policy.xml")), false);
            policy = policy.replaceAll("SCHEMAID", schema).replaceAll("PROBLEMID", problemID).replaceAll("OPERATORID", operator);  
             
            String result = new AGDMImpl().discretizeProblem(policy);
            result = result.replaceAll("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}", "");
            String comp = AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/discProblem/WaveEquation2D2ndOrder.xml"));
            
            comp = comp.replaceAll("MODELID", model);
            comp = comp.replaceAll("OPERATORID", operator);
            comp = comp.replaceAll("SCHEMAID", schema);
            String jsonComp = SimflownyUtils.xmlToJSON(comp, false);
            jsonComp = jsonComp.replaceAll("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}", "");
            FileWriter fichero = new FileWriter("testIntegration14.xml");
            PrintWriter pw = new PrintWriter(fichero);
            pw.write(SimflownyUtils.jsonToXML(result));
            fichero.close();
            assertEquals(jsonComp, result);
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Error in Integration test 14: " + e.getMessage());
        }
        finally {
            docMan.deleteDocument(operator);
            docMan.deleteDocument(model);
            docMan.deleteDocument(schema);
            docMan.deleteDocument(problemID);
            docMan.deleteDocument(cd1st);
            docMan.deleteDocument(cd2nd);
            docMan.deleteDocument(RK3P1);
            docMan.deleteDocument(RK3P2);
            docMan.deleteDocument(RK3P3);
        }
    }
    
    @Test
    public void testIntegration15() throws DMException {
        DocumentManager docMan = new DocumentManagerImpl();
        String model = "";
        String operator = "";
        String schema = "";
        String problemID = "";
        String cd1st = "";
        String cd2nd = "";
        String RK3P1 = "";
        String RK3P2 = "";
        String RK3P3 = "";
        String flieOp = "";
        String blieOp = "";
        String flie = "";
        String blie = "";
        String TIMEINTERP3ID = "";
        
        try {
            String basePath = "../../../integration tests/15 - ComplexGenericPDE";
            
                         
            //Adding the model
            model = docMan.addDocument(SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                     new FileInputStream(basePath + "/models/WaveEquation.xml")), false), "");
            Document doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(model));
            model = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                     "id").item(0).getTextContent();
             
             //Adding schemas
            String loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/CD4-1stOrder.xml")), false);
            String added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            cd1st = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/CD4-2ndOrder.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            cd2nd = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/Diffusion4.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            String difid = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/RK3P1.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            RK3P1 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/RK3P2.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            RK3P2 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/RK3P3.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            RK3P3 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
            		new FileInputStream(basePath + "/disc_schemes/timeInterpRK3.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            TIMEINTERP3ID = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/schema.xml")), false)
                    .replaceAll("RK3P1ID", RK3P1)
                    .replaceAll("RK3P2ID", RK3P2)
                    .replaceAll("RK3P3ID", RK3P3)
                    .replaceAll("TIMEINTERP3ID", TIMEINTERP3ID)
                    .replaceAll("DIFFID", difid);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            schema = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/BackwardDifferenceLie.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            blie = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/ForwardDifferenceLie.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            flie = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
                        
            //Loading the operators
            operator = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                     new FileInputStream(basePath + "/disc_policy/operator.xml")), false);
            operator = operator.replaceAll("CDID", cd1st);
            operator = operator.replaceAll("CD4ID", cd2nd);
            
            added = docMan.addDocument(operator, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            operator = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                     "id").item(0).getTextContent();
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_policy/operatorBackward.xml")), false)
                    .replaceAll("LIEBID", blie);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            blieOp = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
        
        
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_policy/operatorForward.xml")), false)
                        .replaceAll("LIEFID", flie);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            flieOp = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            //Loading the problem
            String problem = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                     new FileInputStream(basePath + "/problem/problemWaveSecondOrder.xml")), false);
            problem = problem.replaceAll("MODELID", model);
            added = docMan.addDocument(problem, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            problemID = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                     "id").item(0).getTextContent();
     
            //Loading the discretization Policy
            String policy = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                     new FileInputStream(basePath + "/disc_policy/policy.xml")), false);
            policy = policy.replaceAll("SCHEMAID", schema).replaceAll("PROBLEMID", problemID).replaceAll("OPERATORID", operator)
                    .replaceAll("LIEFID", flieOp).replaceAll("LIEBID", blieOp);  
             
            String result = new AGDMImpl().discretizeProblem(policy);
            result = result.replaceAll("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}", "");
            String comp = AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/discProblem/WaveTest.xml"));
            comp = comp.replaceAll("MODELID", model);
            comp = comp.replaceAll("OPERATORID", operator);
            comp = comp.replaceAll("LIEFID", flieOp);
            comp = comp.replaceAll("LIEBID", blieOp);
            comp = comp.replaceAll("SCHEMAID", schema);
            comp = comp.replaceAll("DIFID", difid);
            
            String jsonComp = SimflownyUtils.xmlToJSON(comp, false);
            jsonComp = jsonComp.replaceAll("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}", "");
            FileWriter fichero = new FileWriter("testIntegration15.xml");
            PrintWriter pw = new PrintWriter(fichero);
            pw.write(SimflownyUtils.jsonToXML(result));
            fichero.close();
            assertEquals(jsonComp, result);
            
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Error in Integration test 15: " + e.getMessage());
        }
        finally {
            docMan.deleteDocument(operator);
            docMan.deleteDocument(model);
            docMan.deleteDocument(schema);
            docMan.deleteDocument(problemID);
            docMan.deleteDocument(cd1st);
            docMan.deleteDocument(cd2nd);
            docMan.deleteDocument(RK3P1);
            docMan.deleteDocument(RK3P2);
            docMan.deleteDocument(RK3P3);
            docMan.deleteDocument(flieOp);
            docMan.deleteDocument(blieOp);
            docMan.deleteDocument(flie);
            docMan.deleteDocument(blie);
            docMan.deleteDocument(TIMEINTERP3ID);
        }
    }
   
    @Test
    public void testIntegration16() throws DMException {
        DocumentManager docMan = new DocumentManagerImpl();
        String model = "";
        String operator = "";
        String schema = "";
        String problemID = "";
        String operatorBoundaries = "";
        String cd_stencil1 = "";
        String cd4_stencil1 = "";
        String cdc4 = "";
        String cd = "";
        String cd4 = "";
        String flieOp = "";
        String blieOp = "";
        String flie = "";
        String blie = "";
        String interp4 = "";
        String rk4p1 = "";
        String rk4p2 = "";
        String rk4p3 = "";
        String rk4p4 = "";
        String difid = "";
    	try {
    		String basePath = "../../../integration tests/16 - BlackHole3D";
    		
            //Adding the model
            model = docMan.addDocument(SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/models/ccz4_sf2_model.xml")), false), "");
            Document doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(model));
            model = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
            
            //Loading the problem
            String problem = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/problem/einstein_problem_ccz4_sf2_blackhole.xml")), false);
            problem = problem.replaceAll("WAVE2DID", model).replace("ADV2DID", model)
            		.replace("EULER2DID", model).replace("MODELID", model);
            problem = docMan.addDocument(problem, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(problem));
            problemID = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();

            //RK4
            String loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/disc_schemes/RK4P1.xml")), false);
            String added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            rk4p1 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/disc_schemes/RK4P2.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            rk4p2 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/disc_schemes/RK4P3.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            rk4p3 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();       
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/disc_schemes/RK4P4.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            rk4p4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/disc_schemes/timeInterpRK4.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            interp4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            //Dissipation
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/disc_schemes/Diffusion4.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            difid = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            //RK4 - CD only
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/disc_schemes/schemaRK4.xml")), false)
            		.replaceAll("RK4P1ID", rk4p1).replaceAll("RK4P2ID", rk4p2).replaceAll("RK4P3ID", rk4p3).replaceAll("RK4P4ID", rk4p4)
            		.replaceAll("DIFFID", difid).replaceAll("TIMEINTERP4ID", interp4);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            schema = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            //Operator
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/disc_schemes/CenteredDifference4.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            cd4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
           
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/disc_schemes/CenteredDifference4D1.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            cd = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/disc_schemes/CenteredDifference4D1Crossed.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        cdc4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
	                 DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
	        
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/disc_policy/operatorEinsteinCrossed.xml")), false)
                   .replaceAll("CDID", cd).replaceAll("CD4ID", cd4).replaceAll("CD4CID", cdc4);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        operator = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/disc_schemes/CenteredDifference.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            cd_stencil1 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                 DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
           
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/disc_schemes/CenteredDifference2ndOrder.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            cd4_stencil1 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                  DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
	        
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/disc_policy/operatorWave.xml")), false)
                    .replaceAll("CDID", cd_stencil1).replaceAll("CD4ID", cd4_stencil1);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        operatorBoundaries = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
	                DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
	        
	        loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/disc_schemes/BackwardDifferenceLie.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            blie = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/disc_schemes/ForwardDifferenceLie.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            flie = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
	        
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/disc_policy/operatorWaveLieBackward.xml")), false)
                    .replaceAll("LIEBID", blie);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            blieOp = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
        
        
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/disc_policy/operatorWaveLieForward.xml")), false)
                        .replaceAll("LIEFID", flie);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            flieOp = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
	        
            //Loading the discretization Policy
            String policy = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/disc_policy/policyEinsteinAdvection.xml")), false);
            policy = policy.replaceAll("PROBLEMID", problemID).replaceAll("OPERATORID", operator).replaceAll("SCHEMAID", schema)
                 .replaceAll("LIEFID", flieOp).replaceAll("LIEBID", blieOp).replaceAll("OPERATORBOUND", operatorBoundaries);  
            
	        String result = new AGDMImpl().discretizeProblem(policy);
	        result = result.replaceAll("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}", "");
	        String comp = AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/discProblem/blackhole.xml"));
	        
	        comp = comp.replaceAll("MODELID", model);
	        comp = comp.replaceAll("OPERATORID", operator);
	        comp = comp.replaceAll("OPERATOR2ID", operatorBoundaries);
	        comp = comp.replaceAll("LIEFID", flieOp);
	        comp = comp.replaceAll("LIEBID", blieOp);
	        comp = comp.replaceAll("SCHEMAID", schema);
	        comp = comp.replaceAll("DIFID", difid);
	        String jsonComp = SimflownyUtils.xmlToJSON(comp, false);
	        jsonComp = jsonComp.replaceAll("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}", "");
	        FileWriter fichero = new FileWriter("testIntegration16.xml");
	        PrintWriter pw = new PrintWriter(fichero);
	        pw.write(SimflownyUtils.jsonToXML(result));
	        fichero.close();
	        assertEquals(jsonComp, result);
    	}
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Error in Integration test 16: " + e.getMessage());
        }
    	finally {
	        docMan.deleteDocument(operator);
            docMan.deleteDocument(model);
            docMan.deleteDocument(schema);
            docMan.deleteDocument(problemID);
            docMan.deleteDocument(operatorBoundaries);
            docMan.deleteDocument(cd_stencil1);
            docMan.deleteDocument(cd4_stencil1);
            docMan.deleteDocument(cdc4);
            docMan.deleteDocument(cd);
            docMan.deleteDocument(cd4);
            docMan.deleteDocument(flieOp);
            docMan.deleteDocument(blieOp);
            docMan.deleteDocument(flie);
            docMan.deleteDocument(blie);
            docMan.deleteDocument(interp4);
            docMan.deleteDocument(rk4p1);
            docMan.deleteDocument(rk4p2);
            docMan.deleteDocument(rk4p3);
            docMan.deleteDocument(rk4p4);
            docMan.deleteDocument(difid);
    	}
    }
   
    @Test
    public void testIntegration17() throws DMException {
        DocumentManager docMan = new DocumentManagerImpl();
        String model = "";
        String operator = "";
        String schema = "";
        String problemID = "";
        String operatorBoundaries = "";
        String cd_stencil1 = "";
        String cd4_stencil1 = "";
        String cdc4 = "";
        String cd = "";
        String cd4 = "";
        String flieOp = "";
        String blieOp = "";
        String flie = "";
        String blie = "";
        String interp4 = "";
        String rk4p1 = "";
        String rk4p2 = "";
        String rk4p3 = "";
        String rk4p4 = "";
        String difid = "";
        String fvmid = "";
        String lor1zid = "";
        String lor2zid = "";
        String lor3zid = "";
        String nlw1zid = "";
        String nlw2zid = "";
        String nlw3zid = "";
        String smooth1zid = "";
        String smooth2zid = "";
        String smooth3zid = "";
        String onlyLLFid = "";
        String wenoid = "";
        String quinticID = "";
        String WENO5_prestep = "";
    	try {
    		String basePath = "../../../integration tests/17 - NeutronStars";
    		
            //Adding the model
            model = docMan.addDocument(SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/models/ccz4_sf2_mhd_model_rtsafe.xml")), false), "");
            Document doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(model));
            model = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
            
            //Loading the problem
            String problem = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/problem/einstein_problem_ccz4_sf2_mhd_neutron_stars.xml")), false);
            problem = problem.replace("MODELID", model);
            problem = docMan.addDocument(problem, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(problem));
            problemID = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();

            //RK4
            String loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/disc_schemes/RK4P1.xml")), false);
            String added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            rk4p1 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/disc_schemes/RK4P2.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            rk4p2 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/disc_schemes/RK4P3.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            rk4p3 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();       
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/disc_schemes/RK4P4.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            rk4p4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/disc_schemes/timeInterpRK4.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            interp4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            //Dissipation
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/disc_schemes/Diffusion4.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            difid = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            //WENO5
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
            		new FileInputStream(basePath + "/disc_schemes/LowOrderReconstruction-0.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            lor1zid = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
            		new FileInputStream(basePath + "/disc_schemes/LowOrderReconstruction-1.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            lor2zid = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
            		new FileInputStream(basePath + "/disc_schemes/LowOrderReconstruction-2.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            lor3zid = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
            		new FileInputStream(basePath + "/disc_schemes/Non-linearWeights-0.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            nlw1zid = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
            		new FileInputStream(basePath + "/disc_schemes/Non-linearWeights-1.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            nlw2zid = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
            		new FileInputStream(basePath + "/disc_schemes/Non-linearWeights-2.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            nlw3zid = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
            		new FileInputStream(basePath + "/disc_schemes/SmoothnessIndicator-0.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            smooth1zid = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
            		new FileInputStream(basePath + "/disc_schemes/SmoothnessIndicator-1.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            smooth2zid = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
            		new FileInputStream(basePath + "/disc_schemes/SmoothnessIndicator-2.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            smooth3zid = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
            		new FileInputStream(basePath + "/disc_schemes/LLFRusanovVR.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            onlyLLFid = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
            		new FileInputStream(basePath + "/disc_schemes/FVM.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            fvmid = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
            		new FileInputStream(basePath + "/disc_schemes/WENO5.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            wenoid = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            //Quintic interpolation
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
            		new FileInputStream(basePath + "/disc_schemes/QuinticInterpolation.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            quinticID = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent(); 
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/Weno5ZHalf.xml")), false)
            		.replaceAll("lor1ID", lor1zid).replaceAll("lor2ID", lor2zid).replaceAll("lor3ID", lor3zid)
            		.replaceAll("smooth1ID", smooth1zid).replaceAll("smooth2ID", smooth2zid).replaceAll("smooth3ID", smooth3zid)
            		.replaceAll("nlw1ID", nlw1zid).replaceAll("nlw2ID", nlw2zid).replaceAll("nlw3ID", nlw3zid).replaceAll("WENOID", wenoid).replace("LLFID", onlyLLFid)
            		.replaceAll("quinticInterpID", quinticID);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            WENO5_prestep = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            //RK4 - WENO5Z HALF
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
            		new FileInputStream(basePath + "/disc_schemes/RK4WENO5ZHalf.xml")), false)
            		.replaceAll("RK4P1ID", rk4p1).replaceAll("RK4P2ID", rk4p2).replaceAll("RK4P3ID", rk4p3).replaceAll("RK4P4ID", rk4p4)
            		.replaceAll("DIFFID", difid).replaceAll("TIMEINTERP4ID", interp4)
            		.replaceAll("PRESTEPID", WENO5_prestep).replaceAll("FVMID", fvmid);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            schema = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            //Operator
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/disc_schemes/CenteredDifference4.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            cd4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
           
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/disc_schemes/CenteredDifference4D1.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            cd = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/disc_schemes/CenteredDifference4D1Crossed.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        cdc4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
	                 DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
	        
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/disc_policy/operatorEinsteinCrossed.xml")), false)
                   .replaceAll("CDID", cd).replaceAll("CD4ID", cd4).replaceAll("CD4CID", cdc4);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        operator = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/disc_schemes/CenteredDifference.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            cd_stencil1 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                 DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
           
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/disc_schemes/CenteredDifference2ndOrder.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            cd4_stencil1 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                  DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
	        
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/disc_policy/operatorWave.xml")), false)
                    .replaceAll("CDID", cd_stencil1).replaceAll("CD4ID", cd4_stencil1);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        operatorBoundaries = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
	                DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
	        
	        loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/disc_schemes/BackwardDifferenceLie.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            blie = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/disc_schemes/ForwardDifferenceLie.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            flie = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
	        
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/disc_policy/operatorWaveLieBackward.xml")), false)
                    .replaceAll("LIEBID", blie);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            blieOp = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
        
        
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/disc_policy/operatorWaveLieForward.xml")), false)
                        .replaceAll("LIEFID", flie);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            flieOp = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
	        
            //Loading the discretization Policy
            String policy = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/disc_policy/policyEinsteinMHD.xml")), false);
            policy = policy.replaceAll("PROBLEMID", problemID).replaceAll("OPERATORID", operator).replaceAll("SCHEMAID", schema)
                 .replaceAll("LIEFID", flieOp).replaceAll("LIEBID", blieOp).replaceAll("OPERATORBOUND", operatorBoundaries);  
            
	        String result = new AGDMImpl().discretizeProblem(policy);
	        result = result.replaceAll("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}", "");
	        String comp = AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/discProblem/neutronStar.xml"));
	        
	        comp = comp.replaceAll("MODELID", model);
	        comp = comp.replaceAll("OPERATORID", operator);
	        comp = comp.replaceAll("OPERATOR2ID", operatorBoundaries);
	        comp = comp.replaceAll("FVMID", fvmid);
	        comp = comp.replaceAll("LIEFID", flieOp);
	        comp = comp.replaceAll("LIEBID", blieOp);
	        comp = comp.replaceAll("SCHEMAID", schema);
	        comp = comp.replaceAll("DIFID", difid);
	        String jsonComp = SimflownyUtils.xmlToJSON(comp, false);
	        jsonComp = jsonComp.replaceAll("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}", "");
	        FileWriter fichero = new FileWriter("testIntegration17.xml");
	        PrintWriter pw = new PrintWriter(fichero);
	        pw.write(SimflownyUtils.jsonToXML(result));
	        fichero.close();
	        assertEquals(jsonComp, result);
    	}
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Error in Integration test 17: " + e.getMessage());
        }
    	finally {
	        docMan.deleteDocument(operator);
            docMan.deleteDocument(model);
            docMan.deleteDocument(schema);
            docMan.deleteDocument(problemID);
            docMan.deleteDocument(operatorBoundaries);
            docMan.deleteDocument(cd_stencil1);
            docMan.deleteDocument(cd4_stencil1);
            docMan.deleteDocument(cdc4);
            docMan.deleteDocument(cd);
            docMan.deleteDocument(cd4);
            docMan.deleteDocument(flieOp);
            docMan.deleteDocument(blieOp);
            docMan.deleteDocument(flie);
            docMan.deleteDocument(blie);
            docMan.deleteDocument(interp4);
            docMan.deleteDocument(rk4p1);
            docMan.deleteDocument(rk4p2);
            docMan.deleteDocument(rk4p3);
            docMan.deleteDocument(rk4p4);
            docMan.deleteDocument(difid);
            
            docMan.deleteDocument(fvmid);
            docMan.deleteDocument(lor1zid);
            docMan.deleteDocument(lor2zid);
            docMan.deleteDocument(lor3zid);
            docMan.deleteDocument(nlw1zid);
            docMan.deleteDocument(nlw2zid);
            docMan.deleteDocument(nlw3zid);
            docMan.deleteDocument(smooth1zid);
            docMan.deleteDocument(smooth2zid);
            docMan.deleteDocument(smooth3zid);
            docMan.deleteDocument(onlyLLFid);
            docMan.deleteDocument(wenoid);
            docMan.deleteDocument(quinticID);
            docMan.deleteDocument(WENO5_prestep);
    	}
    	
    }
   
    @Test
    public void testIntegration18() throws DMException {
    	DocumentManager docMan = new DocumentManagerImpl();
        String model = "";
        String operator = "";
        String schema = "";
        String cd1st = "";
        String cd2nd = "";
        String problemID = "";
        String RK3P1 = "";
        String RK3P2 = "";
        String RK3P3 = "";
        String interp = "";
        String dissip = "";
        String FDOC3p = "";
        String FDOC3 = "";
        
        try {
            String basePath = "../../../integration tests/18 - conditionalEvolution";
            
                         
            //Adding the model
            model = docMan.addDocument(SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                     new FileInputStream(basePath + "/models/modelWaveSecondOrderGauge.xml")), false), "");
            Document doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(model));
            model = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                     "id").item(0).getTextContent();
             
             //Adding schemas
            String loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/CD4-1stOrder.xml")), false);
            String added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            cd1st = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/CD4-2ndOrder.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            cd2nd = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/RK3P1.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            RK3P1 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/RK3P2.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            RK3P2 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/RK3P3.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            RK3P3 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/FDOC3 prestep.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            FDOC3p = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/FDOC 3.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            FDOC3 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/KO-dissipation-order5.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            dissip = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/Time interpolation 3.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            interp = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/schema.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            schema = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            //Loading the operator
            operator = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                     new FileInputStream(basePath + "/disc_policy/operator.xml")), false);
            operator = operator.replaceAll("CDID", cd1st);
            operator = operator.replaceAll("CD4ID", cd2nd);
            added = docMan.addDocument(operator, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            operator = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                     "id").item(0).getTextContent();
            
            //Loading the problem
            String problem = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                     new FileInputStream(basePath + "/problem/problemWaveSecondOrderGauge.xml")), false);
            problem = problem.replaceAll("WAVE2DID", model);
            added = docMan.addDocument(problem, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            problemID = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                     "id").item(0).getTextContent();
     
            //Loading the discretization Policy
            String policy = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                     new FileInputStream(basePath + "/disc_policy/policy.xml")), false);
            policy = policy.replaceAll("SCHEMAID", schema).replaceAll("PROBLEMID", problemID).replaceAll("OPERATORID", operator);  
             
            String result = new AGDMImpl().discretizeProblem(policy);

            result = result.replaceAll("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}", "");
            String comp = AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/discProblem/WaveEquation2D2ndOrder.xml"));
            
            comp = comp.replaceAll("MODELID", model);
            comp = comp.replaceAll("OPERATORID", operator);
            comp = comp.replaceAll("SCHEMAID", schema);
            String jsonComp = SimflownyUtils.xmlToJSON(comp, false);
            jsonComp = jsonComp.replaceAll("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}", "");
            FileWriter fichero = new FileWriter("testIntegration18.xml");
            PrintWriter pw = new PrintWriter(fichero);
            pw.write(SimflownyUtils.jsonToXML(result));
            fichero.close();
            assertEquals(jsonComp, result);
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Error in Integration test 18: " + e.getMessage());
        }
        finally {
            docMan.deleteDocument(operator);
            docMan.deleteDocument(model);
            docMan.deleteDocument(schema);
            docMan.deleteDocument(problemID);
            docMan.deleteDocument(cd1st);
            docMan.deleteDocument(cd2nd);
            docMan.deleteDocument(RK3P1);
            docMan.deleteDocument(RK3P2);
            docMan.deleteDocument(RK3P3);
            docMan.deleteDocument(FDOC3);
            docMan.deleteDocument(FDOC3p);
            docMan.deleteDocument(interp);
            docMan.deleteDocument(dissip);
        }
    }
    
    @Test
    public void testIntegration19() throws DMException {
    	DocumentManager docMan = new DocumentManagerImpl();
        String corrector = "";
        String ko = "";
        String sphdiffusion = "";
        String problemID = "";
        String model = "";
        String gradWendland = "";
        String sphSym = "";
        String sphAsym = "";
        String sphAsym2 = "";
        String interp = "";
        String interpNorm = "";
        String wedland = "";
        String sphNorm = "";
        String operatorSym = "";
        String operatorAsym = "";
        String predictor = "";
        try {
            String basePath = "../../../integration tests/19 - Particles";
            
                         
            //Adding the model
            model = docMan.addDocument(SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                     new FileInputStream(basePath + "/models/modelParticles.xml")), false), "");
            Document doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(model));
            model = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                     "id").item(0).getTextContent();
            
            //Adding schemas
            String loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                   new FileInputStream(basePath + "/disc_schemes/Corrector step.xml")), false);
            String added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            corrector = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                   new FileInputStream(basePath + "/disc_schemes/Gradient Wendland.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            gradWendland = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                   new FileInputStream(basePath + "/disc_schemes/KO-dissipation-order3.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            ko = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                   new FileInputStream(basePath + "/disc_schemes/Predictor step.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            predictor = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                   new FileInputStream(basePath + "/disc_schemes/SPH-1st Symmetric.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            sphSym = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/SPH-1st Antisymmetric.xml")), false);
             added = docMan.addDocument(loadDB, "");
             doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
             sphAsym = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
             loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                     new FileInputStream(basePath + "/disc_schemes/SPH-2nd Antisymmetric.xml")), false);
              added = docMan.addDocument(loadDB, "");
              doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
              sphAsym2 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                     DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/SPH-1st Normalization.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            sphNorm = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                     new FileInputStream(basePath + "/disc_schemes/SPH interpolation function.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            interp = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                     DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                      new FileInputStream(basePath + "/disc_schemes/SPH normalization function.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            interpNorm = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                      DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                       new FileInputStream(basePath + "/disc_schemes/Wendland.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            wedland = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                       DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                   new FileInputStream(basePath + "/disc_schemes/Predictor Corrector SPH.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            sphdiffusion = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
    
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/OpSPHSymmetric.xml")), false);
             added = docMan.addDocument(loadDB, "");
             doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
             operatorSym = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
             
             loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                     new FileInputStream(basePath + "/disc_schemes/OpSPHAntisymmetric.xml")), false);
              added = docMan.addDocument(loadDB, "");
              doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
              operatorAsym = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                     DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
             
            //Loading the problem
            String problem = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/problem/problemParticles.xml")), false);
            problem = problem.replaceAll("PARTICLEID", model);
            added = docMan.addDocument(problem, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            problemID = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
    
            //Loading the discretization Policy
            String policy = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_policy/policy.xml")), false);
            policy = policy.replaceAll("schemaID", sphdiffusion).replaceAll("PROBLEMID", problemID);  
            
            String result = new AGDMImpl().discretizeProblem(policy);
            result = result.replaceAll("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}", "");
            String comp = AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/discProblem/ParticleCode.xml"));
            
            comp = comp.replaceAll("modelID", model);
            comp = comp.replaceAll("schemaID", sphdiffusion);
            String jsonComp = SimflownyUtils.xmlToJSON(comp, false);
            jsonComp = jsonComp.replaceAll("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}", "");
            FileWriter fichero = new FileWriter("testIntegration19.xml");
            PrintWriter pw = new PrintWriter(fichero);
            pw.write(SimflownyUtils.jsonToXML(result));
            fichero.close();
            assertEquals(jsonComp, result);
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Error in Integration test 19: " + e.getMessage());
        }
        finally {
        	docMan.deleteDocument(operatorSym);
        	docMan.deleteDocument(operatorAsym);
        	docMan.deleteDocument(interp);
        	docMan.deleteDocument(interpNorm);
        	docMan.deleteDocument(wedland);
        	docMan.deleteDocument(sphNorm);
            docMan.deleteDocument(sphSym);
            docMan.deleteDocument(sphAsym);
            docMan.deleteDocument(sphAsym2);
            docMan.deleteDocument(predictor);
            docMan.deleteDocument(ko);
            docMan.deleteDocument(problemID);
            docMan.deleteDocument(sphdiffusion);
            docMan.deleteDocument(model);
            docMan.deleteDocument(gradWendland);
            docMan.deleteDocument(corrector);
        }
    }
    
    @Test
    public void testIntegration20() throws DMException {
    	 DocumentManager docMan = new DocumentManagerImpl();
         String model = "";
         String operator = "";
         String cd = "";
         String cd4 = "";
         String cdc4 = "";
         String problemID = "";
         String RK4FDOC3 = "";
         String FDOC3 = "";
         String RK4P1 = "";
         String RK4P2 = "";
         String RK4P3 = "";
         String RK4P4 = "";
         String FDOC_prestep = "";
         String interp4 = "";
         String difid = "";
         String RK2P1 = "";
         String RK2P2 = "";
         String RK2FDOC3 = "";
        try {
            String basePath = "../../../integration tests/20 - Two Schemas";
 
            //Adding the model
            model = docMan.addDocument(SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/models/TwoSchemas_1_Toni Arbona.xml")), false), "");
            Document doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(model));
            model = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
            
            //Adding schemas
            String loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/FDOC3.xml")), false);
            String added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            FDOC3 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/RK4 Delta1.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            RK4P1 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/RK4 Delta2.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            RK4P2 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/RK4 Delta3.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            RK4P3 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/RK4 Delta4.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            RK4P4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/RK2P1.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            RK2P1 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/RK2P2.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            RK2P2 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/FDOC_prestep.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            FDOC_prestep = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
            		new FileInputStream(basePath + "/disc_schemes/timeInterpRK4.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            interp4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            //Dissipation
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
            		new FileInputStream(basePath + "/disc_schemes/KO-dissipation-order5.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            difid = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/RK4 FDOC3.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            RK4FDOC3 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/RK2FDOC3.xml"))
            		.replaceAll("RK2P1ID", RK2P1)
            		.replaceAll("RK2P2ID", RK2P2), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            RK2FDOC3 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            //Loading the problem
            String problem = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/problem/TwoSchemas.xml")), false);
            problem = problem.replaceAll("MODELID", model);
            added = docMan.addDocument(problem, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            problemID = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
            
            //Operator  
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/CenteredDifference4.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            cd4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/CenteredDifference4D1.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            cd = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_schemes/CenteredDifference4D1Crossed.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        cdc4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
	                 DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
	        
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
            		new FileInputStream(basePath + "/disc_schemes/operator.xml")), false)
                   .replaceAll("CDID", cd).replaceAll("CD4ID", cd4).replaceAll("CD4CID", cdc4);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        operator = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
	        
            //Loading the discretization Policy
            String policy = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(
                    new FileInputStream(basePath + "/disc_policy/policy.xml")), false);
            policy = policy.replaceAll("schemaID", RK4FDOC3).replaceAll("schema2ID", RK2FDOC3).replaceAll("OPERATORID", operator).replaceAll("ProblemID", problemID);  
            
            String result = new AGDMImpl().discretizeProblem(policy);
            result = result.replaceAll("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}", "");
            String comp = AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/discProblem/TwoSchemas.xml"));
            
            comp = comp.replaceAll("modelID", model);
            comp = comp.replaceAll("operatorID", operator);
            comp = comp.replaceAll("schemaID", RK4FDOC3);
            comp = comp.replaceAll("FDOCID", FDOC3);
            String jsonComp = SimflownyUtils.xmlToJSON(comp, false);
            jsonComp = jsonComp.replaceAll("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}", "");
            FileWriter fichero = new FileWriter("testIntegration20.xml");
            PrintWriter pw = new PrintWriter(fichero);
            pw.write(SimflownyUtils.jsonToXML(result));
            fichero.close();
            assertEquals(jsonComp, result);
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Error in Integration test 20: " + e.getMessage());
        }
        finally {
            docMan.deleteDocument(operator);
            docMan.deleteDocument(cd);
            docMan.deleteDocument(cd4);
            docMan.deleteDocument(cdc4);
            docMan.deleteDocument(problemID);
            docMan.deleteDocument(RK4FDOC3);
            docMan.deleteDocument(model);
            docMan.deleteDocument(FDOC3);
            docMan.deleteDocument(RK4P1);
            docMan.deleteDocument(RK4P2);
            docMan.deleteDocument(RK4P3);
            docMan.deleteDocument(RK4P4);
            docMan.deleteDocument(FDOC_prestep);
            docMan.deleteDocument(difid);
            docMan.deleteDocument(interp4);
            docMan.deleteDocument(RK2P1);
            docMan.deleteDocument(RK2P2);
            docMan.deleteDocument(RK2FDOC3);
        }
    }
    
    @Test
    public void testSchemasRK4CD() throws DMException {
    	String schema = "";
    	String model = "";
    	String problemID = "";
    	String rk4p1 = "";
    	String rk4p2 = "";
    	String rk4p3 = "";
    	String rk4p4 = "";
    	String interp4 = "";
    	String difid = "";
    	String cdc4 = "";
    	String cd4 = "";
    	String cd42 = "";
    	String operator = "";
    	DocumentManager docMan = new DocumentManagerImpl();
    	try {
	       
    		String basePath = "../../../schema validation tests/RK4 CD4";
    		
            //Adding the model
            model = docMan.addDocument(SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/model_problem/modelWave.xml")), false), "");
            Document doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(model));
            model = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
            
            //Loading the problem
            String problem = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/model_problem/problemWave.xml")), false);
            problem = problem.replace("MODELID", model);
            problem = docMan.addDocument(problem, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(problem));
            problemID = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();

            //RK4
            String loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/RK4 Delta1.xml")), false);
            String added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            rk4p1 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/RK4 Delta2.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            rk4p2 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/RK4 Delta3.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            rk4p3 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();       
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/RK4 Delta4.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            rk4p4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/Time interpolation 4.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            interp4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            //Dissipation
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/KO-dissipation-order5.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            difid = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            //Operator
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/D1-centered-order4.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            cd4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
           
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/D1-centered-order4-crossed.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            cdc4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/D2-centered-order4.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        cd42 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
	                 DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
	        
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/Operator.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        operator = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();            
            
            //RK4 - CD only
			loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/RK4 CD.xml")), false);
			added = docMan.addDocument(loadDB, "");
			doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
			schema = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
		  
			//Loading the discretization Policy
			String policy = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/policyWave.xml")), false);
			policy = policy.replaceAll("PROBLEMID", problemID);  
          
  	        String result = new AGDMImpl().discretizeProblem(policy);
  	        result = result.replaceAll("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}", "");
  	        String comp = AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/discProblem/wave.xml"));
  	        
  	        String jsonComp = SimflownyUtils.xmlToJSON(comp, false);
  	        jsonComp = jsonComp.replaceAll("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}", "");
  	        FileWriter fichero = new FileWriter("testSchemasRK4CD.xml");
  	        PrintWriter pw = new PrintWriter(fichero);
  	        pw.write(SimflownyUtils.jsonToXML(result));
  	        fichero.close();
  	        assertEquals(jsonComp, result);
    	}
    	catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Error in Schema Validation Test RK4CD: " + e.getMessage());
        }
    	finally {
	        docMan.deleteDocument(operator);
            docMan.deleteDocument(model);
            docMan.deleteDocument(schema);
            docMan.deleteDocument(problemID);
            docMan.deleteDocument(cd42);
            docMan.deleteDocument(cdc4);
            docMan.deleteDocument(cd4);
            docMan.deleteDocument(interp4);
            docMan.deleteDocument(rk4p1);
            docMan.deleteDocument(rk4p2);
            docMan.deleteDocument(rk4p3);
            docMan.deleteDocument(rk4p4);
            docMan.deleteDocument(difid);
    	}
    }
    
    @Test
    public void testSchemasRK4MP5() throws DMException {
    	String schema = "";
    	String model = "";
    	String problemID = "";
    	String rk4p1 = "";
    	String rk4p2 = "";
    	String rk4p3 = "";
    	String rk4p4 = "";
    	String interp4 = "";
    	String difid = "";
    	String cdc4 = "";
    	String cd4 = "";
    	String cd42 = "";
    	String operator = "";
    	String fvm = "";
    	String prestep = "";
    	String mm = "";
    	String mm4 = "";
    	String mp5 = "";
    	DocumentManager docMan = new DocumentManagerImpl();
    	try {
	       
    		String basePath = "../../../schema validation tests/RK4 MP5";
    		
            //Adding the model
            model = docMan.addDocument(SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/model_problem/modelWave.xml")), false), "");
            Document doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(model));
            model = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
            
            //Loading the problem
            String problem = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/model_problem/problemWave.xml")), false);
            problem = problem.replace("MODELID", model);
            problem = docMan.addDocument(problem, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(problem));
            problemID = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();

            //RK4
            String loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/RK4 Delta1.xml")), false);
            String added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            rk4p1 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/RK4 Delta2.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            rk4p2 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/RK4 Delta3.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            rk4p3 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();       
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/RK4 Delta4.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            rk4p4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/Time interpolation 4.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            interp4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            //Dissipation
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/KO-dissipation-order5.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            difid = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            //Operator
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/D1-centered-order4.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            cd4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
           
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/D1-centered-order4-crossed.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            cdc4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/D2-centered-order4.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        cd42 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
	                 DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
	        
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/Operator.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        operator = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();            
            
            //RK4 - MP5
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/MP5.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        mp5 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();   
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/Minmod limiter.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        mm = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();   
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/Minmod limiter 4.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        mm4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();   
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/FVM.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        fvm = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();   
	        loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/MP5 prestep.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        prestep = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();   
			loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/RK4 MP5.xml")), false);
			added = docMan.addDocument(loadDB, "");
			doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
			schema = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
		  
			//Loading the discretization Policy
			String policy = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/policyWave.xml")), false);
			policy = policy.replaceAll("PROBLEMID", problemID);  
          
  	        String result = new AGDMImpl().discretizeProblem(policy);
  	        result = result.replaceAll("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}", "");
  	        String comp = AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/discProblem/wave.xml"));
  	        
  	        String jsonComp = SimflownyUtils.xmlToJSON(comp, false);
  	        jsonComp = jsonComp.replaceAll("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}", "");
  	        FileWriter fichero = new FileWriter("testSchemasRK4MP5.xml");
  	        PrintWriter pw = new PrintWriter(fichero);
  	        pw.write(SimflownyUtils.jsonToXML(result));
  	        fichero.close();
  	        assertEquals(jsonComp, result);
    	}
    	catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Error in Schema Validation Test RK4MP5: " + e.getMessage());
        }
    	finally {
	        docMan.deleteDocument(operator);
            docMan.deleteDocument(model);
            docMan.deleteDocument(schema);
            docMan.deleteDocument(problemID);
            docMan.deleteDocument(cd42);
            docMan.deleteDocument(cdc4);
            docMan.deleteDocument(cd4);
            docMan.deleteDocument(interp4);
            docMan.deleteDocument(rk4p1);
            docMan.deleteDocument(rk4p2);
            docMan.deleteDocument(rk4p3);
            docMan.deleteDocument(rk4p4);
            docMan.deleteDocument(difid);
            docMan.deleteDocument(prestep);
            docMan.deleteDocument(fvm);
            docMan.deleteDocument(mp5);
            docMan.deleteDocument(mm4);
            docMan.deleteDocument(mm);
    	}
    }
    
    @Test
    public void testSchemasRK4WENO5Z() throws DMException {
    	String schema = "";
    	String model = "";
    	String problemID = "";
    	String rk4p1 = "";
    	String rk4p2 = "";
    	String rk4p3 = "";
    	String rk4p4 = "";
    	String interp4 = "";
    	String difid = "";
    	String cdc4 = "";
    	String cd4 = "";
    	String cd42 = "";
    	String operator = "";
    	String fvm = "";
    	String prestep = "";
    	String weno5 = "";
    	String lor0 = "";
    	String nlw0 = "";
    	String si0 = "";
    	String lor1 = "";
    	String nlw1 = "";
    	String si1 = "";
    	String lor2 = "";
    	String nlw2 = "";
    	String si2 = "";
    	DocumentManager docMan = new DocumentManagerImpl();
    	try {
	       
    		String basePath = "../../../schema validation tests/RK4 WENO5Z";
    		
            //Adding the model
            model = docMan.addDocument(SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/model_problem/modelWave.xml")), false), "");
            Document doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(model));
            model = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
            
            //Loading the problem
            String problem = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/model_problem/problemWave.xml")), false);
            problem = problem.replace("MODELID", model);
            problem = docMan.addDocument(problem, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(problem));
            problemID = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();

            //RK4
            String loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/RK4 Delta1.xml")), false);
            String added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            rk4p1 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/RK4 Delta2.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            rk4p2 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/RK4 Delta3.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            rk4p3 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();       
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/RK4 Delta4.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            rk4p4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/Time interpolation 4.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            interp4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            //Dissipation
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/KO-dissipation-order5.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            difid = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            //Operator
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/D1-centered-order4.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            cd4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
           
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/D1-centered-order4-crossed.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            cdc4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/D2-centered-order4.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        cd42 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
	                 DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
	        
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/Operator.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        operator = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();            
            
            //RK4 - WENO5Z
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/WENO 5.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        weno5 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/FVM.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        fvm = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();   
	        loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/WENO5Z prestep.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        prestep = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();   
			loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/RK4 WENO5Z.xml")), false);
			added = docMan.addDocument(loadDB, "");
			doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
			schema = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
		  
			loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO5Z/Low order reconstruction-0.xml")), false);
            added = docMan.addDocument(loadDB, "");
			doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
			lor0 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO5Z/Low order reconstruction-1.xml")), false);
            added = docMan.addDocument(loadDB, "");
    		doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
			lor1 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO5Z/Low order reconstruction-2.xml")), false);
            added = docMan.addDocument(loadDB, "");
    		doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
			lor2 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO5Z/Non-linear weights-0.xml")), false);
            added = docMan.addDocument(loadDB, "");
    		doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
			nlw0 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO5Z/Non-linear weights-1.xml")), false);
            added = docMan.addDocument(loadDB, "");
    		doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
			nlw1 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO5Z/Non-linear weights-2.xml")), false);
            added = docMan.addDocument(loadDB, "");
    		doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
			nlw2 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO5Z/Smoothness Indicator-0.xml")), false);
            added = docMan.addDocument(loadDB, "");
    		doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
			si0 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO5Z/Smoothness Indicator-1.xml")), false);
            added = docMan.addDocument(loadDB, "");
    		doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
			si1 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO5Z/Smoothness Indicator-2.xml")), false);
            added = docMan.addDocument(loadDB, "");
    		doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
			si2 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
			
			//Loading the discretization Policy
			String policy = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/policyWave.xml")), false);
			policy = policy.replaceAll("PROBLEMID", problemID);  
          
  	        String result = new AGDMImpl().discretizeProblem(policy);
  	        result = result.replaceAll("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}", "");
  	        String comp = AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/discProblem/wave.xml"));
  	        
  	        String jsonComp = SimflownyUtils.xmlToJSON(comp, false);
  	        jsonComp = jsonComp.replaceAll("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}", "");
  	        FileWriter fichero = new FileWriter("testSchemasRK4WENO5Z.xml");
  	        PrintWriter pw = new PrintWriter(fichero);
  	        pw.write(SimflownyUtils.jsonToXML(result));
  	        fichero.close();
  	        assertEquals(jsonComp, result);
    	}
    	catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Error in Schema Validation Test RK4WENO5Z: " + e.getMessage());
        }
    	finally {
	        docMan.deleteDocument(operator);
            docMan.deleteDocument(model);
            docMan.deleteDocument(schema);
            docMan.deleteDocument(problemID);
            docMan.deleteDocument(cd42);
            docMan.deleteDocument(cdc4);
            docMan.deleteDocument(cd4);
            docMan.deleteDocument(interp4);
            docMan.deleteDocument(rk4p1);
            docMan.deleteDocument(rk4p2);
            docMan.deleteDocument(rk4p3);
            docMan.deleteDocument(rk4p4);
            docMan.deleteDocument(difid);
            docMan.deleteDocument(prestep);
            docMan.deleteDocument(fvm);
            docMan.deleteDocument(lor0);
            docMan.deleteDocument(nlw0);
            docMan.deleteDocument(si0);
            docMan.deleteDocument(lor1);
            docMan.deleteDocument(nlw1);
            docMan.deleteDocument(si1);
            docMan.deleteDocument(lor2);
            docMan.deleteDocument(nlw2);
            docMan.deleteDocument(si2);
            docMan.deleteDocument(weno5);
    	}
    }
    
    @Test
    public void testSchemasRK4WENO5() throws DMException {
    	String schema = "";
    	String model = "";
    	String problemID = "";
    	String rk4p1 = "";
    	String rk4p2 = "";
    	String rk4p3 = "";
    	String rk4p4 = "";
    	String interp4 = "";
    	String difid = "";
    	String cdc4 = "";
    	String cd4 = "";
    	String cd42 = "";
    	String operator = "";
    	String fvm = "";
    	String prestep = "";
    	String weno5 = "";
    	String lor0 = "";
    	String nlw0 = "";
    	String si0 = "";
    	String lor1 = "";
    	String nlw1 = "";
    	String si1 = "";
    	String lor2 = "";
    	String nlw2 = "";
    	String si2 = "";
    	DocumentManager docMan = new DocumentManagerImpl();
    	try {
	       
    		String basePath = "../../../schema validation tests/RK4 WENO5";
    		
            //Adding the model
            model = docMan.addDocument(SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/model_problem/modelWave.xml")), false), "");
            Document doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(model));
            model = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
            
            //Loading the problem
            String problem = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/model_problem/problemWave.xml")), false);
            problem = problem.replace("MODELID", model);
            problem = docMan.addDocument(problem, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(problem));
            problemID = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();

            //RK4
            String loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/RK4 Delta1.xml")), false);
            String added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            rk4p1 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/RK4 Delta2.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            rk4p2 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/RK4 Delta3.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            rk4p3 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();       
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/RK4 Delta4.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            rk4p4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/Time interpolation 4.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            interp4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            //Dissipation
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/KO-dissipation-order5.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            difid = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            //Operator
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/D1-centered-order4.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            cd4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
           
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/D1-centered-order4-crossed.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            cdc4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/D2-centered-order4.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        cd42 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
	                 DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
	        
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/Operator.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        operator = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();            
            
            //RK4 - WENO5
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/WENO 5.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        weno5 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/FVM.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        fvm = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();   
	        loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/WENO5 prestep.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        prestep = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();   
			loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/RK4 WENO5.xml")), false);
			added = docMan.addDocument(loadDB, "");
			doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
			schema = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
		  
			loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO5/Low order reconstruction-0.xml")), false);
            added = docMan.addDocument(loadDB, "");
			doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
			lor0 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO5/Low order reconstruction-1.xml")), false);
            added = docMan.addDocument(loadDB, "");
    		doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
			lor1 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO5/Low order reconstruction-2.xml")), false);
            added = docMan.addDocument(loadDB, "");
    		doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
			lor2 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO5/Non-linear weights-0.xml")), false);
            added = docMan.addDocument(loadDB, "");
    		doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
			nlw0 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO5/Non-linear weights-1.xml")), false);
            added = docMan.addDocument(loadDB, "");
    		doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
			nlw1 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO5/Non-linear weights-2.xml")), false);
            added = docMan.addDocument(loadDB, "");
    		doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
			nlw2 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO5/Smoothness Indicator-0.xml")), false);
            added = docMan.addDocument(loadDB, "");
    		doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
			si0 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO5/Smoothness Indicator-1.xml")), false);
            added = docMan.addDocument(loadDB, "");
    		doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
			si1 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO5/Smoothness Indicator-2.xml")), false);
            added = docMan.addDocument(loadDB, "");
    		doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
			si2 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
			
			//Loading the discretization Policy
			String policy = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/policyWave.xml")), false);
			policy = policy.replaceAll("PROBLEMID", problemID);  
          
  	        String result = new AGDMImpl().discretizeProblem(policy);
  	        result = result.replaceAll("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}", "");
  	        String comp = AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/discProblem/wave.xml"));
  	        
  	        String jsonComp = SimflownyUtils.xmlToJSON(comp, false);
  	        jsonComp = jsonComp.replaceAll("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}", "");
  	        FileWriter fichero = new FileWriter("testSchemasRK4WENO5.xml");
  	        PrintWriter pw = new PrintWriter(fichero);
  	        pw.write(SimflownyUtils.jsonToXML(result));
  	        fichero.close();
  	        assertEquals(jsonComp, result);
    	}
    	catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Error in Schema Validation Test RK4WENO5: " + e.getMessage());
        }
    	finally {
	        docMan.deleteDocument(operator);
            docMan.deleteDocument(model);
            docMan.deleteDocument(schema);
            docMan.deleteDocument(problemID);
            docMan.deleteDocument(cd42);
            docMan.deleteDocument(cdc4);
            docMan.deleteDocument(cd4);
            docMan.deleteDocument(interp4);
            docMan.deleteDocument(rk4p1);
            docMan.deleteDocument(rk4p2);
            docMan.deleteDocument(rk4p3);
            docMan.deleteDocument(rk4p4);
            docMan.deleteDocument(difid);
            docMan.deleteDocument(prestep);
            docMan.deleteDocument(fvm);
            docMan.deleteDocument(lor0);
            docMan.deleteDocument(nlw0);
            docMan.deleteDocument(si0);
            docMan.deleteDocument(lor1);
            docMan.deleteDocument(nlw1);
            docMan.deleteDocument(si1);
            docMan.deleteDocument(lor2);
            docMan.deleteDocument(nlw2);
            docMan.deleteDocument(si2);
            docMan.deleteDocument(weno5);
    	}
    }
    
    @Test
    public void testSchemasRK4FDOC5() throws DMException {
    	String schema = "";
    	String model = "";
    	String problemID = "";
    	String rk4p1 = "";
    	String rk4p2 = "";
    	String rk4p3 = "";
    	String rk4p4 = "";
    	String interp4 = "";
    	String difid = "";
    	String cdc4 = "";
    	String cd4 = "";
    	String cd42 = "";
    	String operator = "";
    	String prestep = "";
    	String fdoc5 = "";
    	DocumentManager docMan = new DocumentManagerImpl();
    	try {
	       
    		String basePath = "../../../schema validation tests/RK4 FDOC5";
    		
            //Adding the model
            model = docMan.addDocument(SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/model_problem/modelWave.xml")), false), "");
            Document doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(model));
            model = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
            
            //Loading the problem
            String problem = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/model_problem/problemWave.xml")), false);
            problem = problem.replace("MODELID", model);
            problem = docMan.addDocument(problem, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(problem));
            problemID = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();

            //RK4
            String loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/RK4 Delta1.xml")), false);
            String added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            rk4p1 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/RK4 Delta2.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            rk4p2 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/RK4 Delta3.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            rk4p3 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();       
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/RK4 Delta4.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            rk4p4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/Time interpolation 4.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            interp4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            //Dissipation
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/KO-dissipation-order5.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            difid = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            //Operator
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/D1-centered-order4.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            cd4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
           
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/D1-centered-order4-crossed.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            cdc4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/D2-centered-order4.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        cd42 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
	                 DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
	        
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/Operator.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        operator = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();            
            
            //RK4 - FDOC5
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/FDOC 5.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        fdoc5 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
	        loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/FDOC5 prestep.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        prestep = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();   
			loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/RK4 FDOC5.xml")), false);
			added = docMan.addDocument(loadDB, "");
			doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
			schema = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
		  
			//Loading the discretization Policy
			String policy = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/policyWave.xml")), false);
			policy = policy.replaceAll("PROBLEMID", problemID);  
          
  	        String result = new AGDMImpl().discretizeProblem(policy);
  	        result = result.replaceAll("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}", "");
  	        String comp = AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/discProblem/wave.xml"));
  	        
  	        String jsonComp = SimflownyUtils.xmlToJSON(comp, false);
  	        jsonComp = jsonComp.replaceAll("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}", "");
  	        FileWriter fichero = new FileWriter("testSchemasRK4FDOC5.xml");
  	        PrintWriter pw = new PrintWriter(fichero);
  	        pw.write(SimflownyUtils.jsonToXML(result));
  	        fichero.close();
  	        assertEquals(jsonComp, result);
    	}
    	catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Error in Schema Validation Test RK4FDOC5: " + e.getMessage());
        }
    	finally {
	        docMan.deleteDocument(operator);
            docMan.deleteDocument(model);
            docMan.deleteDocument(schema);
            docMan.deleteDocument(problemID);
            docMan.deleteDocument(cd42);
            docMan.deleteDocument(cdc4);
            docMan.deleteDocument(cd4);
            docMan.deleteDocument(interp4);
            docMan.deleteDocument(rk4p1);
            docMan.deleteDocument(rk4p2);
            docMan.deleteDocument(rk4p3);
            docMan.deleteDocument(rk4p4);
            docMan.deleteDocument(difid);
            docMan.deleteDocument(fdoc5);
            docMan.deleteDocument(prestep);
    	}
    }
    
    @Test
    public void testSchemasRK3WENO5Z() throws DMException {
    	String schema = "";
    	String model = "";
    	String problemID = "";
    	String rk3p1 = "";
    	String rk3p2 = "";
    	String rk3p3 = "";
    	String interp3 = "";
    	String difid = "";
    	String cdc4 = "";
    	String cd4 = "";
    	String cd42 = "";
    	String operator = "";
    	String fvm = "";
    	String prestep = "";
    	String weno5 = "";
    	String lor0 = "";
    	String nlw0 = "";
    	String si0 = "";
    	String lor1 = "";
    	String nlw1 = "";
    	String si1 = "";
    	String lor2 = "";
    	String nlw2 = "";
    	String si2 = "";
    	DocumentManager docMan = new DocumentManagerImpl();
    	try {
	       
    		String basePath = "../../../schema validation tests/RK3 WENO5Z";
    		
            //Adding the model
            model = docMan.addDocument(SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/model_problem/modelWave.xml")), false), "");
            Document doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(model));
            model = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
            
            //Loading the problem
            String problem = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/model_problem/problemWave.xml")), false);
            problem = problem.replace("MODELID", model);
            problem = docMan.addDocument(problem, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(problem));
            problemID = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();

            //RK4
            String loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/RK3P1.xml")), false);
            String added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            rk3p1 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/RK3P2.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            rk3p2 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/RK3P3.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            rk3p3 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/Time interpolation 3.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            interp3 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            //Dissipation
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/KO-dissipation-order5.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            difid = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            //Operator
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/D1-centered-order4.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            cd4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
           
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/D1-centered-order4-crossed.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            cdc4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/D2-centered-order4.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        cd42 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
	                 DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
	        
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/Operator.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        operator = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();            
            
            //RK4 - WENO5Z
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/WENO 5.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        weno5 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/FVM.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        fvm = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();   
	        loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/WENO5Z prestep.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        prestep = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();   
			loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/RK3 WENO5Z.xml")), false);
			added = docMan.addDocument(loadDB, "");
			doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
			schema = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
		  
			loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO5Z/Low order reconstruction-0.xml")), false);
            added = docMan.addDocument(loadDB, "");
			doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
			lor0 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO5Z/Low order reconstruction-1.xml")), false);
            added = docMan.addDocument(loadDB, "");
    		doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
			lor1 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO5Z/Low order reconstruction-2.xml")), false);
            added = docMan.addDocument(loadDB, "");
    		doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
			lor2 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO5Z/Non-linear weights-0.xml")), false);
            added = docMan.addDocument(loadDB, "");
    		doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
			nlw0 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO5Z/Non-linear weights-1.xml")), false);
            added = docMan.addDocument(loadDB, "");
    		doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
			nlw1 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO5Z/Non-linear weights-2.xml")), false);
            added = docMan.addDocument(loadDB, "");
    		doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
			nlw2 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO5Z/Smoothness Indicator-0.xml")), false);
            added = docMan.addDocument(loadDB, "");
    		doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
			si0 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO5Z/Smoothness Indicator-1.xml")), false);
            added = docMan.addDocument(loadDB, "");
    		doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
			si1 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO5Z/Smoothness Indicator-2.xml")), false);
            added = docMan.addDocument(loadDB, "");
    		doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
			si2 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
			
			//Loading the discretization Policy
			String policy = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/policyWave.xml")), false);
			policy = policy.replaceAll("PROBLEMID", problemID);  
          
  	        String result = new AGDMImpl().discretizeProblem(policy);
  	        result = result.replaceAll("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}", "");
  	        String comp = AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/discProblem/wave.xml"));
  	        
  	        String jsonComp = SimflownyUtils.xmlToJSON(comp, false);
  	        jsonComp = jsonComp.replaceAll("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}", "");
  	        FileWriter fichero = new FileWriter("testSchemasRK3WENO5Z.xml");
  	        PrintWriter pw = new PrintWriter(fichero);
  	        pw.write(SimflownyUtils.jsonToXML(result));
  	        fichero.close();
  	        assertEquals(jsonComp, result);
    	}
    	catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Error in Schema Validation Test RK3WENO5Z: " + e.getMessage());
        }
    	finally {
	        docMan.deleteDocument(operator);
            docMan.deleteDocument(model);
            docMan.deleteDocument(schema);
            docMan.deleteDocument(problemID);
            docMan.deleteDocument(cd42);
            docMan.deleteDocument(cdc4);
            docMan.deleteDocument(cd4);
            docMan.deleteDocument(interp3);
            docMan.deleteDocument(rk3p1);
            docMan.deleteDocument(rk3p2);
            docMan.deleteDocument(rk3p3);
            docMan.deleteDocument(difid);
            docMan.deleteDocument(prestep);
            docMan.deleteDocument(fvm);
            docMan.deleteDocument(lor0);
            docMan.deleteDocument(nlw0);
            docMan.deleteDocument(si0);
            docMan.deleteDocument(lor1);
            docMan.deleteDocument(nlw1);
            docMan.deleteDocument(si1);
            docMan.deleteDocument(lor2);
            docMan.deleteDocument(nlw2);
            docMan.deleteDocument(si2);
            docMan.deleteDocument(weno5);
    	}
    }
    
    @Test
    public void testSchemasRK3MP5() throws DMException {
    	String schema = "";
    	String model = "";
    	String problemID = "";
    	String rk3p1 = "";
    	String rk3p2 = "";
    	String rk3p3 = "";
    	String interp3 = "";
    	String difid = "";
    	String cdc4 = "";
    	String cd4 = "";
    	String cd42 = "";
    	String operator = "";
    	String fvm = "";
    	String prestep = "";
    	String mm = "";
    	String mm4 = "";
    	String mp5 = "";
    	DocumentManager docMan = new DocumentManagerImpl();
    	try {
	       
    		String basePath = "../../../schema validation tests/RK3 MP5";
    		
            //Adding the model
            model = docMan.addDocument(SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/model_problem/modelWave.xml")), false), "");
            Document doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(model));
            model = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
            
            //Loading the problem
            String problem = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/model_problem/problemWave.xml")), false);
            problem = problem.replace("MODELID", model);
            problem = docMan.addDocument(problem, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(problem));
            problemID = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();

            //RK4
            String loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/RK3P1.xml")), false);
            String added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            rk3p1 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/RK3P2.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            rk3p2 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/RK3P3.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            rk3p3 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/Time interpolation 3.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            interp3 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            //Dissipation
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/KO-dissipation-order5.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            difid = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            //Operator
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/D1-centered-order4.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            cd4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
           
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/D1-centered-order4-crossed.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            cdc4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/D2-centered-order4.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        cd42 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
	                 DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
	        
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/Operator.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        operator = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();            
            
            //RK4 - MP5
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/MP5.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        mp5 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();   
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/Minmod limiter.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        mm = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();   
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/Minmod limiter 4.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        mm4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();   
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/FVM.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        fvm = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();   
	        loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/MP5 prestep.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        prestep = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();   
			loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/RK3 MP5.xml")), false);
			added = docMan.addDocument(loadDB, "");
			doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
			schema = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
		  
			//Loading the discretization Policy
			String policy = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/policyWave.xml")), false);
			policy = policy.replaceAll("PROBLEMID", problemID);  
          
  	        String result = new AGDMImpl().discretizeProblem(policy);
  	        result = result.replaceAll("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}", "");
  	        String comp = AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/discProblem/wave.xml"));
  	        
  	        String jsonComp = SimflownyUtils.xmlToJSON(comp, false);
  	        jsonComp = jsonComp.replaceAll("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}", "");
  	        FileWriter fichero = new FileWriter("testSchemasRK3MP5.xml");
  	        PrintWriter pw = new PrintWriter(fichero);
  	        pw.write(SimflownyUtils.jsonToXML(result));
  	        fichero.close();
  	        assertEquals(jsonComp, result);
    	}
    	catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Error in Schema Validation Test RK3MP5: " + e.getMessage());
        }
    	finally {
	        docMan.deleteDocument(operator);
            docMan.deleteDocument(model);
            docMan.deleteDocument(schema);
            docMan.deleteDocument(problemID);
            docMan.deleteDocument(cd42);
            docMan.deleteDocument(cdc4);
            docMan.deleteDocument(cd4);
            docMan.deleteDocument(interp3);
            docMan.deleteDocument(rk3p1);
            docMan.deleteDocument(rk3p2);
            docMan.deleteDocument(rk3p3);
            docMan.deleteDocument(difid);
            docMan.deleteDocument(prestep);
            docMan.deleteDocument(fvm);
            docMan.deleteDocument(mp5);
            docMan.deleteDocument(mm4);
            docMan.deleteDocument(mm);
    	}
    }
    
    @Test
    public void testSchemasRK3WENO3() throws DMException {
    	String schema = "";
    	String model = "";
    	String problemID = "";
    	String rk3p1 = "";
    	String rk3p2 = "";
    	String rk3p3 = "";
    	String interp3 = "";
    	String difid = "";
    	String cdc4 = "";
    	String cd4 = "";
    	String cd42 = "";
    	String operator = "";
    	String fvm = "";
    	String prestep = "";
    	String weno3 = "";
    	String lor0 = "";
    	String nlw0 = "";
    	String si0 = "";
    	String lor1 = "";
    	String nlw1 = "";
    	String si1 = "";
    	DocumentManager docMan = new DocumentManagerImpl();
    	try {
	       
    		String basePath = "../../../schema validation tests/RK3 WENO3";
    		
            //Adding the model
            model = docMan.addDocument(SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/model_problem/modelWave.xml")), false), "");
            Document doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(model));
            model = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
            
            //Loading the problem
            String problem = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/model_problem/problemWave.xml")), false);
            problem = problem.replace("MODELID", model);
            problem = docMan.addDocument(problem, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(problem));
            problemID = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();

            //RK4
            String loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/RK3P1.xml")), false);
            String added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            rk3p1 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/RK3P2.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            rk3p2 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/RK3P3.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            rk3p3 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/Time interpolation 3.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            interp3 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            //Dissipation
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/KO-dissipation-order5.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            difid = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            //Operator
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/D1-centered-order4.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            cd4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
           
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/D1-centered-order4-crossed.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            cdc4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/D2-centered-order4.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        cd42 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
	                 DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
	        
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/Operator.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        operator = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();            
            
            //RK4 - WENO5
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/WENO 3.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        weno3 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/FVM.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        fvm = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();   
	        loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/WENO3 prestep.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        prestep = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();   
			loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/RK3 WENO3.xml")), false);
			added = docMan.addDocument(loadDB, "");
			doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
			schema = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
		  
			loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO3/Low order reconstruction-0.xml")), false);
            added = docMan.addDocument(loadDB, "");
			doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
			lor0 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO3/Low order reconstruction-1.xml")), false);
            added = docMan.addDocument(loadDB, "");
    		doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
			lor1 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO3/Non-linear Weights-0.xml")), false);
            added = docMan.addDocument(loadDB, "");
    		doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
			nlw0 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO3/Non-linear Weights-1.xml")), false);
            added = docMan.addDocument(loadDB, "");
    		doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
			nlw1 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO3/Smoothness indicator-0.xml")), false);
            added = docMan.addDocument(loadDB, "");
    		doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
			si0 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO3/Smoothness indicator-1.xml")), false);
            added = docMan.addDocument(loadDB, "");
    		doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
			si1 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
			
			//Loading the discretization Policy
			String policy = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/policyWave.xml")), false);
			policy = policy.replaceAll("PROBLEMID", problemID);  
          
  	        String result = new AGDMImpl().discretizeProblem(policy);
  	        result = result.replaceAll("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}", "");
  	        String comp = AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/discProblem/wave.xml"));
  	        
  	        String jsonComp = SimflownyUtils.xmlToJSON(comp, false);
  	        jsonComp = jsonComp.replaceAll("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}", "");
  	        FileWriter fichero = new FileWriter("testSchemasRK3WENO3.xml");
  	        PrintWriter pw = new PrintWriter(fichero);
  	        pw.write(SimflownyUtils.jsonToXML(result));
  	        fichero.close();
  	        assertEquals(jsonComp, result);
    	}
    	catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Error in Schema Validation Test RK3WENO3: " + e.getMessage());
        }
    	finally {
	        docMan.deleteDocument(operator);
            docMan.deleteDocument(model);
            docMan.deleteDocument(schema);
            docMan.deleteDocument(problemID);
            docMan.deleteDocument(cd42);
            docMan.deleteDocument(cdc4);
            docMan.deleteDocument(cd4);
            docMan.deleteDocument(interp3);
            docMan.deleteDocument(rk3p1);
            docMan.deleteDocument(rk3p2);
            docMan.deleteDocument(rk3p3);
            docMan.deleteDocument(difid);
            docMan.deleteDocument(prestep);
            docMan.deleteDocument(fvm);
            docMan.deleteDocument(lor0);
            docMan.deleteDocument(nlw0);
            docMan.deleteDocument(si0);
            docMan.deleteDocument(lor1);
            docMan.deleteDocument(nlw1);
            docMan.deleteDocument(si1);
            docMan.deleteDocument(weno3);
    	}
    }
    
    @Test
    public void testSchemasRK3WENO3YC() throws DMException {
    	String schema = "";
    	String model = "";
    	String problemID = "";
    	String rk3p1 = "";
    	String rk3p2 = "";
    	String rk3p3 = "";
    	String interp3 = "";
    	String difid = "";
    	String cdc4 = "";
    	String cd4 = "";
    	String cd42 = "";
    	String operator = "";
    	String fvm = "";
    	String prestep = "";
    	String weno3 = "";
    	String lor0 = "";
    	String nlw0 = "";
    	String si0 = "";
    	String lor1 = "";
    	String nlw1 = "";
    	String si1 = "";
    	DocumentManager docMan = new DocumentManagerImpl();
    	try {
	       
    		String basePath = "../../../schema validation tests/RK3 WENO3YC";
    		
            //Adding the model
            model = docMan.addDocument(SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/model_problem/modelWave.xml")), false), "");
            Document doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(model));
            model = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
            
            //Loading the problem
            String problem = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/model_problem/problemWave.xml")), false);
            problem = problem.replace("MODELID", model);
            problem = docMan.addDocument(problem, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(problem));
            problemID = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();

            //RK4
            String loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/RK3P1.xml")), false);
            String added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            rk3p1 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/RK3P2.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            rk3p2 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/RK3P3.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            rk3p3 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/Time interpolation 3.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            interp3 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            //Dissipation
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/KO-dissipation-order5.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            difid = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            //Operator
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/D1-centered-order4.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            cd4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
           
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/D1-centered-order4-crossed.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            cdc4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/D2-centered-order4.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        cd42 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
	                 DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
	        
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/Operator.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        operator = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();            
            
            //RK4 - WENO5
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/WENO 3.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        weno3 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/FVM.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        fvm = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();   
	        loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/WENO3YC prestep.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        prestep = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();   
			loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/RK3 WENO3YC.xml")), false);
			added = docMan.addDocument(loadDB, "");
			doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
			schema = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
		  
			loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO3YC/Low order reconstruction-0.xml")), false);
            added = docMan.addDocument(loadDB, "");
			doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
			lor0 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO3YC/Low order reconstruction-1.xml")), false);
            added = docMan.addDocument(loadDB, "");
    		doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
			lor1 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO3YC/Non-linear Weights-0.xml")), false);
            added = docMan.addDocument(loadDB, "");
    		doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
			nlw0 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO3YC/Non-linear Weights-1.xml")), false);
            added = docMan.addDocument(loadDB, "");
    		doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
			nlw1 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO3YC/Smoothness indicator-0.xml")), false);
            added = docMan.addDocument(loadDB, "");
    		doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
			si0 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO3YC/Smoothness indicator-1.xml")), false);
            added = docMan.addDocument(loadDB, "");
    		doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
			si1 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
			
			//Loading the discretization Policy
			String policy = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/policyWave.xml")), false);
			policy = policy.replaceAll("PROBLEMID", problemID);  
          
  	        String result = new AGDMImpl().discretizeProblem(policy);
  	        result = result.replaceAll("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}", "");
  	        String comp = AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/discProblem/wave.xml"));
  	        
  	        String jsonComp = SimflownyUtils.xmlToJSON(comp, false);
  	        jsonComp = jsonComp.replaceAll("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}", "");
  	        FileWriter fichero = new FileWriter("testSchemasRK3WENO3YC.xml");
  	        PrintWriter pw = new PrintWriter(fichero);
  	        pw.write(SimflownyUtils.jsonToXML(result));
  	        fichero.close();
  	        assertEquals(jsonComp, result);
    	}
    	catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Error in Schema Validation Test RK3WENO3YC: " + e.getMessage());
        }
    	finally {
	        docMan.deleteDocument(operator);
            docMan.deleteDocument(model);
            docMan.deleteDocument(schema);
            docMan.deleteDocument(problemID);
            docMan.deleteDocument(cd42);
            docMan.deleteDocument(cdc4);
            docMan.deleteDocument(cd4);
            docMan.deleteDocument(interp3);
            docMan.deleteDocument(rk3p1);
            docMan.deleteDocument(rk3p2);
            docMan.deleteDocument(rk3p3);
            docMan.deleteDocument(difid);
            docMan.deleteDocument(prestep);
            docMan.deleteDocument(fvm);
            docMan.deleteDocument(lor0);
            docMan.deleteDocument(nlw0);
            docMan.deleteDocument(si0);
            docMan.deleteDocument(lor1);
            docMan.deleteDocument(nlw1);
            docMan.deleteDocument(si1);
            docMan.deleteDocument(weno3);
    	}
    }
    
    @Test
    public void testSchemasRK3CD() throws DMException {
    	String schema = "";
    	String model = "";
    	String problemID = "";
    	String rk3p1 = "";
    	String rk3p2 = "";
    	String rk3p3 = "";
    	String interp3 = "";
    	String difid = "";
    	String cdc4 = "";
    	String cd4 = "";
    	String cd42 = "";
    	String operator = "";
    	DocumentManager docMan = new DocumentManagerImpl();
    	try {
	       
    		String basePath = "../../../schema validation tests/RK3 CD4";
    		
            //Adding the model
            model = docMan.addDocument(SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/model_problem/modelWave.xml")), false), "");
            Document doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(model));
            model = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
            
            //Loading the problem
            String problem = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/model_problem/problemWave.xml")), false);
            problem = problem.replace("MODELID", model);
            problem = docMan.addDocument(problem, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(problem));
            problemID = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();

            //RK4
            String loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/RK3P1.xml")), false);
            String added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            rk3p1 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/RK3P2.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            rk3p2 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/RK3P3.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            rk3p3 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/Time interpolation 3.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            interp3 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            //Dissipation
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/KO-dissipation-order5.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            difid = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            //Operator
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/D1-centered-order4.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            cd4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
           
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/D1-centered-order4-crossed.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            cdc4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/D2-centered-order4.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        cd42 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
	                 DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
	        
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/Operator.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        operator = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();            
            
            //RK4 - CD only
			loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/RK3 CD.xml")), false);
			added = docMan.addDocument(loadDB, "");
			doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
			schema = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
		  
			//Loading the discretization Policy
			String policy = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/policyWave.xml")), false);
			policy = policy.replaceAll("PROBLEMID", problemID);  
          
  	        String result = new AGDMImpl().discretizeProblem(policy);
  	        result = result.replaceAll("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}", "");
  	        String comp = AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/discProblem/wave.xml"));
  	        
  	        String jsonComp = SimflownyUtils.xmlToJSON(comp, false);
  	        jsonComp = jsonComp.replaceAll("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}", "");
  	        FileWriter fichero = new FileWriter("testSchemasRK3CD.xml");
  	        PrintWriter pw = new PrintWriter(fichero);
  	        pw.write(SimflownyUtils.jsonToXML(result));
  	        fichero.close();
  	        assertEquals(jsonComp, result);
    	}
    	catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Error in Schema Validation Test RK3CD: " + e.getMessage());
        }
    	finally {
	        docMan.deleteDocument(operator);
            docMan.deleteDocument(model);
            docMan.deleteDocument(schema);
            docMan.deleteDocument(problemID);
            docMan.deleteDocument(cd42);
            docMan.deleteDocument(cdc4);
            docMan.deleteDocument(cd4);
            docMan.deleteDocument(interp3);
            docMan.deleteDocument(rk3p1);
            docMan.deleteDocument(rk3p2);
            docMan.deleteDocument(rk3p3);
            docMan.deleteDocument(difid);
    	}
    }
    
    @Test
    public void testSchemasRK3FDOC3() throws DMException {
    	String schema = "";
    	String model = "";
    	String problemID = "";
    	String rk3p1 = "";
    	String rk3p2 = "";
    	String rk3p3 = "";
    	String interp3 = "";
    	String difid = "";
    	String cdc4 = "";
    	String cd4 = "";
    	String cd42 = "";
    	String operator = "";
    	String prestep = "";
    	String fdoc3 = "";
    	DocumentManager docMan = new DocumentManagerImpl();
    	try {
	       
    		String basePath = "../../../schema validation tests/RK3 FDOC3";
    		
            //Adding the model
            model = docMan.addDocument(SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/model_problem/modelWave.xml")), false), "");
            Document doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(model));
            model = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
            
            //Loading the problem
            String problem = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/model_problem/problemWave.xml")), false);
            problem = problem.replace("MODELID", model);
            problem = docMan.addDocument(problem, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(problem));
            problemID = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();

            //RK4
            String loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/RK3P1.xml")), false);
            String added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            rk3p1 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/RK3P2.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            rk3p2 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/RK3P3.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            rk3p3 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/Time interpolation 3.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            interp3 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            //FDOC
	        loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/FDOC3 prestep.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        prestep = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();   
			loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/FDOC 3.xml")), false);
			added = docMan.addDocument(loadDB, "");
			doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
			fdoc3 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            //Dissipation
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/KO-dissipation-order5.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            difid = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            //Operator
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/D1-centered-order4.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            cd4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
           
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/D1-centered-order4-crossed.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            cdc4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/D2-centered-order4.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        cd42 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
	                 DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
	        
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/Operator.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        operator = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();            
            
            //RK4 - CD only
			loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/RK3 FDOC3.xml")), false);
			added = docMan.addDocument(loadDB, "");
			doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
			schema = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
		  
			//Loading the discretization Policy
			String policy = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/policyWave.xml")), false);
			policy = policy.replaceAll("PROBLEMID", problemID);  
          
  	        String result = new AGDMImpl().discretizeProblem(policy);
  	        result = result.replaceAll("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}", "");
  	        String comp = AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/discProblem/wave.xml"));
  	        
  	        String jsonComp = SimflownyUtils.xmlToJSON(comp, false);
  	        jsonComp = jsonComp.replaceAll("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}", "");
  	        FileWriter fichero = new FileWriter("testSchemasRK3FDOC3.xml");
  	        PrintWriter pw = new PrintWriter(fichero);
  	        pw.write(SimflownyUtils.jsonToXML(result));
  	        fichero.close();
  	        assertEquals(jsonComp, result);
    	}
    	catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Error in Schema Validation Test RK3FDOC3: " + e.getMessage());
        }
    	finally {
	        docMan.deleteDocument(operator);
            docMan.deleteDocument(model);
            docMan.deleteDocument(schema);
            docMan.deleteDocument(problemID);
            docMan.deleteDocument(cd42);
            docMan.deleteDocument(cdc4);
            docMan.deleteDocument(cd4);
            docMan.deleteDocument(interp3);
            docMan.deleteDocument(rk3p1);
            docMan.deleteDocument(rk3p2);
            docMan.deleteDocument(rk3p3);
            docMan.deleteDocument(difid);
            docMan.deleteDocument(prestep);
            docMan.deleteDocument(fdoc3);
    	}
    }
   
    @Test
    public void testSchemasIMEX3CD() throws DMException {
    	String schema = "";
    	String model = "";
    	String problemID = "";
    	String imex3p1 = "";
    	String imex3p2 = "";
    	String imex3p3 = "";
    	String imex3p4 = "";
    	String imex3p5 = "";
    	String imex3p2s = "";
    	String imex3p3s = "";
    	String imex3p4s = "";
    	String imex3p5s = "";
    	String interp3 = "";
    	String difid = "";
    	String cdc4 = "";
    	String cd4 = "";
    	String cd42 = "";
    	String operator = "";
    	DocumentManager docMan = new DocumentManagerImpl();
    	try {
	       
    		String basePath = "../../../schema validation tests/IMEX3 CD4";
    		
            //Adding the model
            model = docMan.addDocument(SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/model_problem/modelWave.xml")), false), "");
            Document doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(model));
            model = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
            
            //Loading the problem
            String problem = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/model_problem/problemWave.xml")), false);
            problem = problem.replace("MODELID", model);
            problem = docMan.addDocument(problem, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(problem));
            problemID = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();

            //RK4
            String loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/IMEX3P1.xml")), false);
            String added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            imex3p1 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/IMEX3P2.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            imex3p2 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/IMEX3P3.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            imex3p3 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/IMEX3P4.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            imex3p4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/IMEX3P5.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            imex3p5 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/IMEX3P2S.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            imex3p2s = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/IMEX3P3S.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            imex3p3s = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/IMEX3P4S.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            imex3p4s = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/IMEX3P5S.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            imex3p5s = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/Time interpolation IMEX3.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            interp3 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            //Dissipation
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/KO-dissipation-order5.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            difid = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            //Operator
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/D1-centered-order4.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            cd4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
           
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/D1-centered-order4-crossed.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            cdc4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/D2-centered-order4.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        cd42 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
	                 DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
	        
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/Operator.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        operator = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();            
            
			loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/IMEX3-433 CD.xml")), false);
			added = docMan.addDocument(loadDB, "");
			doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
			schema = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
		  
			//Loading the discretization Policy
			String policy = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/policyWave.xml")), false);
			policy = policy.replaceAll("PROBLEMID", problemID);  
          
  	        String result = new AGDMImpl().discretizeProblem(policy);
  	        result = result.replaceAll("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}", "");
  	        String comp = AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/discProblem/wave.xml"));
  	        
  	        String jsonComp = SimflownyUtils.xmlToJSON(comp, false);
  	        jsonComp = jsonComp.replaceAll("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}", "");
  	        FileWriter fichero = new FileWriter("testSchemasIMEX3CD.xml");
  	        PrintWriter pw = new PrintWriter(fichero);
  	        pw.write(SimflownyUtils.jsonToXML(result));
  	        fichero.close();
  	        assertEquals(jsonComp, result);
    	}
    	catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Error in Schema Validation Test IMEX3CD: " + e.getMessage());
        }
    	finally {
	        docMan.deleteDocument(operator);
            docMan.deleteDocument(model);
            docMan.deleteDocument(schema);
            docMan.deleteDocument(problemID);
            docMan.deleteDocument(cd42);
            docMan.deleteDocument(cdc4);
            docMan.deleteDocument(cd4);
            docMan.deleteDocument(interp3);
            docMan.deleteDocument(imex3p1);
            docMan.deleteDocument(imex3p2);
            docMan.deleteDocument(imex3p3);
            docMan.deleteDocument(imex3p4);
            docMan.deleteDocument(imex3p5);
            docMan.deleteDocument(imex3p2s);
            docMan.deleteDocument(imex3p3s);
            docMan.deleteDocument(imex3p4s);
            docMan.deleteDocument(imex3p5s);
            docMan.deleteDocument(difid);
    	}
    }
    
   // @Test
    public void testSchemasIMEX3CD2() throws DMException {
    	String schema = "";
    	String model = "";
    	String problemID = "";
    	String imex3p2 = "";
    	String imex3p3 = "";
    	String rk3p1 = "";
    	String rk3p2 = "";
    	String rk3p3 = "";
    	String interp3 = "";
    	String difid = "";
    	String cdc4 = "";
    	String cd4 = "";
    	String cd42 = "";
    	String operator = "";
    	DocumentManager docMan = new DocumentManagerImpl();
    	try {
	       
    		String basePath = "../../../schema validation tests/IMEX3_va CD4";
    		
            //Adding the model
            model = docMan.addDocument(SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/model_problem/modelWave.xml")), false), "");
            Document doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(model));
            model = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
            
            //Loading the problem
            String problem = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/model_problem/problemWave.xml")), false);
            problem = problem.replace("MODELID", model);
            problem = docMan.addDocument(problem, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(problem));
            problemID = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();

            //RK4
            String loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/RK3P1.xml")), false);
            String added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            rk3p1 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/RK3P2.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            rk3p2 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/RK3P3.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            rk3p3 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/IMEX3P2.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            imex3p2 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/IMEX3P3.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            imex3p3 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/Time interpolation 3.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            interp3 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            //Dissipation
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/KO-dissipation-order5.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            difid = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            //Operator
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/D1-centered-order4.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            cd4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
           
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/D1-centered-order4-crossed.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            cdc4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/D2-centered-order4.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        cd42 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
	                 DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
	        
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/Operator.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        operator = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();            
            
			loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/IMEX3-433 CD.xml")), false);
			added = docMan.addDocument(loadDB, "");
			doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
			schema = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
		  
			//Loading the discretization Policy
			String policy = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/policyWave.xml")), false);
//			policy = policy.replaceAll("PROBLEMID", problemID);  
          
  	        String result = new AGDMImpl().discretizeProblem(policy);
  	        result = result.replaceAll("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}", "");
  	        String comp = AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/discProblem/wave.xml"));
  	        
  	        String jsonComp = SimflownyUtils.xmlToJSON(comp, false);
  	        jsonComp = jsonComp.replaceAll("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}", "");
  	        FileWriter fichero = new FileWriter("testSchemasIMEX3CD.xml");
  	        PrintWriter pw = new PrintWriter(fichero);
  	        pw.write(SimflownyUtils.jsonToXML(result));
  	        fichero.close();
  	        assertEquals(jsonComp, result);
    	}
    	catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Error in Schema Validation Test IMEX3CD: " + e.getMessage());
        }
    	finally {
	        docMan.deleteDocument(operator);
            docMan.deleteDocument(model);
            docMan.deleteDocument(schema);
            docMan.deleteDocument(problemID);
            docMan.deleteDocument(cd42);
            docMan.deleteDocument(cdc4);
            docMan.deleteDocument(cd4);
            docMan.deleteDocument(interp3);
            docMan.deleteDocument(imex3p2);
            docMan.deleteDocument(imex3p3);
            docMan.deleteDocument(rk3p1);
            docMan.deleteDocument(rk3p2);
            docMan.deleteDocument(rk3p3);
            docMan.deleteDocument(difid);
    	}
    }
    
    @Test
    public void testSchemasIMEX3MP5() throws DMException {
    	String schema = "";
    	String model = "";
    	String problemID = "";
    	String imex3p1 = "";
    	String imex3p2 = "";
    	String imex3p3 = "";
    	String imex3p4 = "";
    	String imex3p5 = "";
    	String imex3p2s = "";
    	String imex3p3s = "";
    	String imex3p4s = "";
    	String imex3p5s = "";
    	String interp3 = "";
    	String difid = "";
    	String cdc4 = "";
    	String cd4 = "";
    	String cd42 = "";
    	String operator = "";
    	String mp5 = "";
    	String mm = "";
    	String mm4 = "";
    	String prestep = "";
    	String fvm = "";
    	DocumentManager docMan = new DocumentManagerImpl();
    	try {
	       
    		String basePath = "../../../schema validation tests/IMEX3 MP5";
    		
            //Adding the model
            model = docMan.addDocument(SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/model_problem/modelWave.xml")), false), "");
            Document doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(model));
            model = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
            
            //Loading the problem
            String problem = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/model_problem/problemWave.xml")), false);
            problem = problem.replace("MODELID", model);
            problem = docMan.addDocument(problem, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(problem));
            problemID = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();

            //RK4
            String loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/IMEX3P1.xml")), false);
            String added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            imex3p1 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/IMEX3P2.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            imex3p2 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/IMEX3P3.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            imex3p3 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/IMEX3P4.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            imex3p4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/IMEX3P5.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            imex3p5 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/IMEX3P2S.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            imex3p2s = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/IMEX3P3S.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            imex3p3s = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/IMEX3P4S.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            imex3p4s = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/IMEX3P5S.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            imex3p5s = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/Time interpolation IMEX3.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            interp3 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/MP5.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        mp5 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();   
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/Minmod limiter.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        mm = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();   
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/Minmod limiter 4.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        mm4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();   
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/FVM.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        fvm = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();   
	        loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/MP5 prestep.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        prestep = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent(); 
            
            //Dissipation
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/KO-dissipation-order5.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            difid = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            //Operator
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/D1-centered-order4.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            cd4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
           
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/D1-centered-order4-crossed.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            cdc4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/D2-centered-order4.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        cd42 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
	                 DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
	        
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/Operator.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        operator = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();            
            
			loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/IMEX3-433 MP5.xml")), false);
			added = docMan.addDocument(loadDB, "");
			doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
			schema = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
		  
			//Loading the discretization Policy
			String policy = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/policyWave.xml")), false);
			policy = policy.replaceAll("PROBLEMID", problemID);  
          
  	        String result = new AGDMImpl().discretizeProblem(policy);
  	        result = result.replaceAll("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}", "");
  	        String comp = AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/discProblem/wave.xml"));
  	        
  	        String jsonComp = SimflownyUtils.xmlToJSON(comp, false);
  	        jsonComp = jsonComp.replaceAll("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}", "");
  	        FileWriter fichero = new FileWriter("testSchemasIMEX3MP5.xml");
  	        PrintWriter pw = new PrintWriter(fichero);
  	        pw.write(SimflownyUtils.jsonToXML(result));
  	        fichero.close();
  	        assertEquals(jsonComp, result);
    	}
    	catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Error in Schema Validation Test IMEX3MP5: " + e.getMessage());
        }
    	finally {
	        docMan.deleteDocument(operator);
            docMan.deleteDocument(model);
            docMan.deleteDocument(schema);
            docMan.deleteDocument(problemID);
            docMan.deleteDocument(cd42);
            docMan.deleteDocument(cdc4);
            docMan.deleteDocument(cd4);
            docMan.deleteDocument(interp3);
            docMan.deleteDocument(imex3p1);
            docMan.deleteDocument(imex3p2);
            docMan.deleteDocument(imex3p3);
            docMan.deleteDocument(imex3p4);
            docMan.deleteDocument(imex3p5);
            docMan.deleteDocument(imex3p2s);
            docMan.deleteDocument(imex3p3s);
            docMan.deleteDocument(imex3p4s);
            docMan.deleteDocument(imex3p5s);
            docMan.deleteDocument(difid);
            docMan.deleteDocument(fvm);
            docMan.deleteDocument(prestep);
            docMan.deleteDocument(mm4);
            docMan.deleteDocument(mm);
            docMan.deleteDocument(mp5);
    	}
    }
    
    
    @Test
    public void testSchemasIMEX3WENO5Z() throws DMException {
    	String schema = "";
    	String model = "";
    	String problemID = "";
    	String imex3p1 = "";
    	String imex3p2 = "";
    	String imex3p3 = "";
    	String imex3p4 = "";
    	String imex3p5 = "";
    	String imex3p2s = "";
    	String imex3p3s = "";
    	String imex3p4s = "";
    	String imex3p5s = "";
    	String interp3 = "";
    	String difid = "";
    	String cdc4 = "";
    	String cd4 = "";
    	String cd42 = "";
    	String operator = "";
    	String prestep = "";
    	String fvm = "";
    	String weno5 = "";
    	String lor0 = "";
    	String nlw0 = "";
    	String si0 = "";
    	String lor1 = "";
    	String nlw1 = "";
    	String si1 = "";
    	String lor2 = "";
    	String nlw2 = "";
    	String si2 = "";
    	DocumentManager docMan = new DocumentManagerImpl();
    	try {
	       
    		String basePath = "../../../schema validation tests/IMEX3 WENO5Z";
    		
            //Adding the model
            model = docMan.addDocument(SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/model_problem/modelWave.xml")), false), "");
            Document doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(model));
            model = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
            
            //Loading the problem
            String problem = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/model_problem/problemWave.xml")), false);
            problem = problem.replace("MODELID", model);
            problem = docMan.addDocument(problem, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(problem));
            problemID = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
            System.out.println(problemID);
            //RK4
            String loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/IMEX3P1.xml")), false);
            String added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            imex3p1 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/IMEX3P2.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            imex3p2 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/IMEX3P3.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            imex3p3 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/IMEX3P4.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            imex3p4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/IMEX3P5.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            imex3p5 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/IMEX3P2S.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            imex3p2s = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/IMEX3P3S.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            imex3p3s = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/IMEX3P4S.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            imex3p4s = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/IMEX3P5S.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            imex3p5s = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/Time interpolation IMEX3.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            interp3 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/WENO 5.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        weno5 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/FVM.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        fvm = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();   
	        loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/WENO5Z prestep.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        prestep = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
		  
			loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO5Z/Low order reconstruction-0.xml")), false);
            added = docMan.addDocument(loadDB, "");
			doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
			lor0 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO5Z/Low order reconstruction-1.xml")), false);
            added = docMan.addDocument(loadDB, "");
    		doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
			lor1 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO5Z/Low order reconstruction-2.xml")), false);
            added = docMan.addDocument(loadDB, "");
    		doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
			lor2 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO5Z/Non-linear weights-0.xml")), false);
            added = docMan.addDocument(loadDB, "");
    		doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
			nlw0 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO5Z/Non-linear weights-1.xml")), false);
            added = docMan.addDocument(loadDB, "");
    		doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
			nlw1 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO5Z/Non-linear weights-2.xml")), false);
            added = docMan.addDocument(loadDB, "");
    		doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
			nlw2 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO5Z/Smoothness Indicator-0.xml")), false);
            added = docMan.addDocument(loadDB, "");
    		doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
			si0 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO5Z/Smoothness Indicator-1.xml")), false);
            added = docMan.addDocument(loadDB, "");
    		doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
			si1 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO5Z/Smoothness Indicator-2.xml")), false);
            added = docMan.addDocument(loadDB, "");
    		doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
			si2 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            //Dissipation
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/KO-dissipation-order5.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            difid = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            //Operator
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/D1-centered-order4.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            cd4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
           
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/D1-centered-order4-crossed.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            cdc4 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/D2-centered-order4.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        cd42 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
	                 DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
	        
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/Operator.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        operator = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();            
            
			loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/IMEX3-433 WENO5Z.xml")), false);
			added = docMan.addDocument(loadDB, "");
			doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
			schema = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
		  
			//Loading the discretization Policy
			String policy = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/schema/policyWave.xml")), false);
			policy = policy.replaceAll("PROBLEMID", problemID);  
          
  	        String result = new AGDMImpl().discretizeProblem(policy);
  	        result = result.replaceAll("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}", "");
  	        String comp = AGDMUtils.convertStreamToString(new FileInputStream(basePath + "/discProblem/wave.xml"));
  	        
  	        String jsonComp = SimflownyUtils.xmlToJSON(comp, false);
  	        jsonComp = jsonComp.replaceAll("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}", "");
  	        FileWriter fichero = new FileWriter("testSchemasIMEX3WENO5Z.xml");
  	        PrintWriter pw = new PrintWriter(fichero);
  	        pw.write(SimflownyUtils.jsonToXML(result));
  	        fichero.close();
  	        assertEquals(jsonComp, result);
    	}
    	catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Error in Schema Validation Test IMEX3WENO5Z: " + e.getMessage());
        }
    	finally {
	        docMan.deleteDocument(operator);
            docMan.deleteDocument(model);
            docMan.deleteDocument(schema);
            docMan.deleteDocument(problemID);
            docMan.deleteDocument(cd42);
            docMan.deleteDocument(cdc4);
            docMan.deleteDocument(cd4);
            docMan.deleteDocument(interp3);
            docMan.deleteDocument(imex3p1);
            docMan.deleteDocument(imex3p2);
            docMan.deleteDocument(imex3p3);
            docMan.deleteDocument(imex3p4);
            docMan.deleteDocument(imex3p5);
            docMan.deleteDocument(imex3p2s);
            docMan.deleteDocument(imex3p3s);
            docMan.deleteDocument(imex3p4s);
            docMan.deleteDocument(imex3p5s);
            docMan.deleteDocument(difid);
            docMan.deleteDocument(fvm);
            docMan.deleteDocument(prestep);
            docMan.deleteDocument(weno5);
            docMan.deleteDocument(lor0);
            docMan.deleteDocument(nlw0);
            docMan.deleteDocument(si0);
            docMan.deleteDocument(lor1);
            docMan.deleteDocument(nlw1);
            docMan.deleteDocument(si1);
            docMan.deleteDocument(lor2);
            docMan.deleteDocument(nlw2);
            docMan.deleteDocument(si2);
    	}
    }
    
    //@Test
    public void testManual() {
    	String problemID = "";
    	String flieOp = "";
    	String blieOp = "";
    	String model = "";
    	String schema = "";
    	String operator = "";
    	String d1cd2 = "";
    	try {
            DocumentManager docMan = new DocumentManagerImpl();
            
            String loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/modelFunctions/eos_cold.xml")), false);
            String added = docMan.addDocument(loadDB, "");
            Document doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/modelFunctions/eos_recovery.xml")), false);
            added = docMan.addDocument(loadDB, "");
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/modelFunctions/con2primEOS.xml")), false);
            added = docMan.addDocument(loadDB, "");
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/modelFunctions/con2primEOS_piecewise_les.xml")), false);
            added = docMan.addDocument(loadDB, "");
            
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/modelFunctions/EoS recovery LES.xml")), false);
            added = docMan.addDocument(loadDB, "");
            
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/modelFunctions/Fermi.xml")), false);
            added = docMan.addDocument(loadDB, "");
            
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/modelFunctions/con2primEOS_generic_tab.xml")), false);
            added = docMan.addDocument(loadDB, "");
            
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/modelFunctions/con2primEOS_generic.xml")), false);
            added = docMan.addDocument(loadDB, "");
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/modelFunctions/function_piecewiseEos.xml")), false);
            added = docMan.addDocument(loadDB, "");
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/modelFunctions/function_piecewiseEos_poly.xml")), false);
            added = docMan.addDocument(loadDB, "");
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/modelFunctions/derivative_function_piecewiseEos.xml")), false);
            added = docMan.addDocument(loadDB, "");
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/modelFunctions/derivative_function_piecewiseEos_poly.xml")), false);
            added = docMan.addDocument(loadDB, "");
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/modelFunctions/newtonRaphson.xml")), false);
            added = docMan.addDocument(loadDB, "");
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/modelFunctions/newtonRaphson_Tabulated1D.xml")), false);
            added = docMan.addDocument(loadDB, "");
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/modelFunctions/function_TabulatedEos.xml")), false);
            added = docMan.addDocument(loadDB, "");
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/modelFunctions/dEdrho_dEdt_dPdrho_dPdt.xml")), false);
            added = docMan.addDocument(loadDB, "");
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/modelFunctions/zbrent.xml")), false);
            added = docMan.addDocument(loadDB, "");
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/modelFunctions/magnetic_recovery.xml")), false);
            added = docMan.addDocument(loadDB, "");
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/modelFunctions/Eikonal/eikonal.xml")), false);
            added = docMan.addDocument(loadDB, "");
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/modelFunctions/Eikonal/eikonal_2D.xml")), false);
            added = docMan.addDocument(loadDB, "");
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/modelFunctions/Eikonal/eikonal_3D.xml")), false);
            added = docMan.addDocument(loadDB, "");
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/modelFunctions/Eikonal/operator_eikonal.xml")), false);
            added = docMan.addDocument(loadDB, "");
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/modelFunctions/leakage.xml")), false);
            added = docMan.addDocument(loadDB, "");
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/modelFunctions/Smoothstep.xml")), false);
            added = docMan.addDocument(loadDB, "");
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/Carlos/les+eikonal/xml/NR tabulated_EOS tab + LES_Carlos Palenzuela.xml")), false);
            added = docMan.addDocument(loadDB, "");
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/Carlos/M1/Function minerbo_1_Carlos Palenzuela.xml")), false);
            added = docMan.addDocument(loadDB, "");
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/Carlos/M1/Zbrent_Minerbo_Carlos Palenzuela.xml")), false);
            added = docMan.addDocument(loadDB, "");
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/Carlos/M1/Minerbo_1_Carlos Palenzuela.xml")), false);
            added = docMan.addDocument(loadDB, "");
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/Carlos/M1/Leakage_M1_Carlos Palenzuela.xml")), false);
            added = docMan.addDocument(loadDB, "");
       
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/modelFunctions/Fermi over Fermi.xml")), false);
            added = docMan.addDocument(loadDB, "");
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/SpaceDiscretization/FDM/Average-Stencil3.xml")), false);
            added = docMan.addDocument(loadDB, "");
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/policies/OpAvg.xml")), false);
            added = docMan.addDocument(loadDB, "");
            
            //Adding the model
            model = docMan.addDocument(SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
         //           getResourceAsStream("/modelWaveAux.xml")), false), "");
      //   		getResourceAsStream("/ccz4_sf2_model.xml")), false), "");
            //getResourceAsStream("/ccz4_sf2_mhd_model_rtsafe.xml")), false), "");
            //getResourceAsStream("/ccz4_mhd_model_piecewiseEoS_Carlos.xml")), false), "");
      //      getResourceAsStream("/CCZ4gauge+MHD+tabEoS.xml")), false), "");            
        //    		getResourceAsStream("/IRO_validation/modelWaveEinstein.xml")), false), "");
        //    		getResourceAsStream("/IRO_validation/modelWaveShift.xml")), false), "");
      //  	getResourceAsStream("/IRO_validation/ccz4_sf2_model.xml")), false), "");
      //      	  	getResourceAsStream("/IRO_validation/ccz4_mhd_model.xml")), false), "");
      //              getResourceAsStream("/Carlos/CCZ4gauge+MHD+tabEoS.xml")), false), "");
     //               getResourceAsStream("/Carlos/reprimand/PDEModel.xml")), false), "");
       //             getResourceAsStream("/Carlos/tabeos/CCZ4gauge+MHD+tabEoS_2.0 _Carlos Palenzuela.xml")), false), "");
        //    		getResourceAsStream("/Ricard/particles/sin_Lorene/PDEModel.xml")), false), "");
              		//getResourceAsStream("/Ricard/particles/con_Lorene/Tab_nonzero/non_static_metric/PDEModel.xml")), false), "");
         //   		getResourceAsStream("/Ricard/particles/con_Lorene/Tab_nonzero/non_static_metric/massive_particles/PDEModel.xml")), false), "");
        //           getResourceAsStream("/Carlos/eikonal/CCZ4gauge+MHD+tabEoS_eikonal _Carlos Palenzuela.xml")), false), "");
         //           getResourceAsStream("/Carlos/les/CCZ4gauge+MHD+LES.xml")), false), "");
          //          getResourceAsStream("/Carlos/les+eikonal/xml/CCZ4gauge+MHD+tabEoS+leakage+LES_1_Carlos Palenzuela.xml")), false), "");
                    getResourceAsStream("/Carlos/M1/CCZ4+MHD+M1_1_Carlos Palenzuela.xml")), false), "");
          //  		getResourceAsStream("/Daniele/Planet_average/PlanetAtmosphere2Dpert.xml")), false), "");
      //              getResourceAsStream("/Manuel/00_Hyperbolic_Boson_Stars_ccz4 SF2_Manuel Izquierdo.xml")), false), "");
      //              getResourceAsStream("/Carlos/Poisson/modelPoisson_CarlosID.xml")), false), "");
     //       		getResourceAsStream("/Carlos/boson/Einstein_relax_simple + 2 complex scalar fields_ccz4 SF2_Carlos Palenzuela.xml")), false), "");
    //        	       getResourceAsStream("/CCZ4gauge+MHD+Particles_1.0_Carlos Palenzuela.xml")), false), "");
   //      	       getResourceAsStream("/Ricard/Error/CCZ4gauge+Tab0+metrica_estacionaria+Scwarzschild+Geodesics_1_Ricard Aguilera Miret.xml")), false), "");
//           	       getResourceAsStream("/Daniele/Pulsar v5.xml")), false), "");
  //          getResourceAsStream("/modelMHDEquations2D.xml")), false), "");

            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(model));
            model = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
            System.out.println("Model ID: " + model);
            
            //Loading the problem
            String problem = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
        //     		getResourceAsStream("/problemWaveAux.xml")), false);
       //     		getResourceAsStream("/einstein_problem_ccz4_sf2_boosted_boson_star.xml")), false);
       //     		getResourceAsStream("/einstein_problem_ccz4_mhd_neutron_stars_lorene.xml")), false);
      //       		getResourceAsStream("/einstein_problem_ccz4_mhd_tabEoS_neutron_stars_lorene.xml")), false);
          //  		getResourceAsStream("/Carlos/reprimand/PDEProblem.xml")), false);
//            		getResourceAsStream("/Carlos/tabeos/Einstein Neutron Stars_v2.xml")), false);
            		//getResourceAsStream("/Carlos/eikonal/Einstein Neutron Stars_eikonal.xml")), false);
       //     		getResourceAsStream("/Ricard/particles/sin_Lorene/PDEProblem.xml")), false);
            		//getResourceAsStream("/Ricard/particles/con_Lorene/Tab_nonzero/non_static_metric/PDEProblem.xml")), false);
      //      		getResourceAsStream("/Ricard/particles/con_Lorene/Tab_nonzero/non_static_metric/massive_particles/PDEProblem.xml")), false);
      //      		getResourceAsStream("/Carlos/les+eikonal/xml/Einstein Neutron Stars_GRMHD tabEoS eikonal LES_Carlos Palenzuela.xml")), false);
        //    		getResourceAsStream("/Carlos/les/Neutron Star (CCZ4gauge+MHD+LES).xml")), false);
            		getResourceAsStream("/Carlos/M1/Einstein Neutron Stars_M1_Carlos Palenzuela.xml")), false);
          //     		getResourceAsStream("/Daniele/Planet_average/Atmospheric_turbulence_2D_pert_3_Daniele Vigano.xml")), false);
            //   		getResourceAsStream("/Manuel/0_Einstein_Boson_Stars_ccz4 sf2_Manuel Izquierdo.xml")), false);
   //         		getResourceAsStream("/Carlos/Poisson/problemRelaxation_CarlosID.xml")), false);
     //       		getResourceAsStream("/Carlos/boson/Einstein Boson Stars_relax_simple_ccz4 sf2_Carlos Palenzuela.xml")), false);
     //         		getResourceAsStream("/Neutron Star (CCZ4gauge+MHD+Particles).xml")), false);
     //       		getResourceAsStream("/Ricard/Error/CCZ4+particules+Schwarzschild+Geodesics_1_Ricard Aguilera Miret.xml")), false);
       //     		getResourceAsStream("/Daniele/Induction 3D FF v5_5_DV.xml")), false);
    	//	getResourceAsStream("/CircularPolarizedAlfvenWaveTestConservadas.xml")), false);
            problem = problem.replace("MODELID", model);
            problem = docMan.addDocument(problem, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(problem));
            problemID = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(DocumentManagerImpl.MMSURI, 
                    "id").item(0).getTextContent();
            //PREDICTOR CORRECTOR
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/ODESolvers/PredictorCorrector/Predictor step.xml")), false);
            added = docMan.addDocument(loadDB, "");
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/ODESolvers/PredictorCorrector/Corrector step.xml")), false);
            added = docMan.addDocument(loadDB, "");
            
            //RK3
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/ODESolvers/RK3/RK3P1.xml")), false);
            added = docMan.addDocument(loadDB, "");
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/ODESolvers/RK3/RK3P2.xml")), false);
            added = docMan.addDocument(loadDB, "");
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/ODESolvers/RK3/RK3P3.xml")), false);
            added = docMan.addDocument(loadDB, "");

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
            		getResourceAsStream("/schemas/TimeInterpolation/Time interpolation 3.xml")), false);
            added = docMan.addDocument(loadDB, "");
           
            
            //RK4
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/ODESolvers/RK4/RK4 Delta1.xml")), false);
            added = docMan.addDocument(loadDB, "");
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/ODESolvers/RK4/RK4 Delta2.xml")), false);
            added = docMan.addDocument(loadDB, "");

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/ODESolvers/RK4/RK4 Delta3.xml")), false);
            added = docMan.addDocument(loadDB, "");
       
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/ODESolvers/RK4/RK4 Delta4.xml")), false);
            added = docMan.addDocument(loadDB, "");

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/TimeInterpolation/Time interpolation 4.xml")), false);
            added = docMan.addDocument(loadDB, "");
            
            //IMEX3-433
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/ODESolvers/IMEX3-433/IMEX3P2.xml")), false);
            added = docMan.addDocument(loadDB, "");

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/ODESolvers/IMEX3-433/IMEX3P3.xml")), false);
            added = docMan.addDocument(loadDB, "");
       
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
            		getResourceAsStream("/schemas/TimeInterpolation/Time interpolation IMEX3.xml")), false);
            added = docMan.addDocument(loadDB, "");
            
            //Dissipation
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/SpaceDiscretization/FDM/KO-dissipation-order5.xml")), false);
            added = docMan.addDocument(loadDB, "");
            
            //FDOC3
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/SpaceDiscretization/FDM/FDOC 3.xml")), false);
            added = docMan.addDocument(loadDB, "");
            
            //FDOC5
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/SpaceDiscretization/FDM/FDOC 5.xml")), false);
            added = docMan.addDocument(loadDB, "");
            
            //MC
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/Limiters/MC limiter.xml")), false);
            added = docMan.addDocument(loadDB, "");
     
            //Minmod
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/Limiters/Minmod limiter.xml")), false);
            added = docMan.addDocument(loadDB, ""); 
            
            //Minmod4
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/Limiters/Minmod limiter 4.xml")), false);
            added = docMan.addDocument(loadDB, ""); 
            
            //Quintic interpolation
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/VariableReconstruction/Quintic Interpolation.xml")), false);
            added = docMan.addDocument(loadDB, "");
            
            //MP5
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/VariableReconstruction/MP5.xml")), false);
            added = docMan.addDocument(loadDB, "");
            
            
            //WENO3
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/VariableReconstruction/WENO 3.xml")), false);
            added = docMan.addDocument(loadDB, "");

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO3/Low order reconstruction-0.xml")), false);
            added = docMan.addDocument(loadDB, "");

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO3/Low order reconstruction-1.xml")), false);
            added = docMan.addDocument(loadDB, "");

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO3/Non-linear Weights-0.xml")), false);
            added = docMan.addDocument(loadDB, "");

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO3/Non-linear Weights-1.xml")), false);
            added = docMan.addDocument(loadDB, "");

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO3/Smoothness indicator-0.xml")), false);
            added = docMan.addDocument(loadDB, "");

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO3/Smoothness indicator-1.xml")), false);
            added = docMan.addDocument(loadDB, "");
            
            //WENO3YC
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO3YC/Low order reconstruction-0.xml")), false);
            added = docMan.addDocument(loadDB, "");

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO3YC/Low order reconstruction-1.xml")), false);
            added = docMan.addDocument(loadDB, "");

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO3YC/Non-linear Weights-0.xml")), false);
            added = docMan.addDocument(loadDB, "");

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO3YC/Non-linear Weights-1.xml")), false);
            added = docMan.addDocument(loadDB, "");

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO3YC/Smoothness indicator-0.xml")), false);
            added = docMan.addDocument(loadDB, "");

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO3YC/Smoothness indicator-1.xml")), false);
            added = docMan.addDocument(loadDB, "");
            
          //WENO5
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/VariableReconstruction/WENO 5.xml")), false);
            added = docMan.addDocument(loadDB, "");

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO5/Low order reconstruction-0.xml")), false);
            added = docMan.addDocument(loadDB, "");

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO5/Low order reconstruction-1.xml")), false);
            added = docMan.addDocument(loadDB, "");

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO5/Low order reconstruction-2.xml")), false);
            added = docMan.addDocument(loadDB, "");
      
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO5/Non-linear weights-0.xml")), false);
            added = docMan.addDocument(loadDB, "");

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO5/Non-linear weights-1.xml")), false);
            added = docMan.addDocument(loadDB, "");

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO5/Non-linear weights-2.xml")), false);
            added = docMan.addDocument(loadDB, "");

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO5/Smoothness Indicator-0.xml")), false);
            added = docMan.addDocument(loadDB, "");

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO5/Smoothness Indicator-1.xml")), false);
            added = docMan.addDocument(loadDB, "");

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO5/Smoothness Indicator-2.xml")), false);
            added = docMan.addDocument(loadDB, "");
            
            //WENO5Z
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO5Z/Low order reconstruction-0.xml")), false);
            added = docMan.addDocument(loadDB, "");

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO5Z/Low order reconstruction-1.xml")), false);
            added = docMan.addDocument(loadDB, "");

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO5Z/Low order reconstruction-2.xml")), false);
            added = docMan.addDocument(loadDB, "");
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO5Z/Non-linear weights-0.xml")), false);
            added = docMan.addDocument(loadDB, "");

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO5Z/Non-linear weights-1.xml")), false);
            added = docMan.addDocument(loadDB, "");

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO5Z/Non-linear weights-2.xml")), false);
            added = docMan.addDocument(loadDB, "");

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO5Z/Smoothness Indicator-0.xml")), false);
            added = docMan.addDocument(loadDB, "");

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO5Z/Smoothness Indicator-1.xml")), false);
            added = docMan.addDocument(loadDB, "");

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO5Z/Smoothness Indicator-2.xml")), false);
            added = docMan.addDocument(loadDB, "");
            
            //WENO5ZA
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO5ZA/Low order reconstruction-0.xml")), false);
            added = docMan.addDocument(loadDB, "");

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO5ZA/Low order reconstruction-1.xml")), false);
            added = docMan.addDocument(loadDB, "");

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO5ZA/Low order reconstruction-2.xml")), false);
            added = docMan.addDocument(loadDB, "");
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO5ZA/Non-linear weights-0.xml")), false);
            added = docMan.addDocument(loadDB, "");

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO5ZA/Non-linear weights-1.xml")), false);
            added = docMan.addDocument(loadDB, "");

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO5ZA/Non-linear weights-2.xml")), false);
            added = docMan.addDocument(loadDB, "");

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO5ZA/Smoothness Indicator-0.xml")), false);
            added = docMan.addDocument(loadDB, "");

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO5ZA/Smoothness Indicator-1.xml")), false);
            added = docMan.addDocument(loadDB, "");

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/ReconstructionMethods/AuxiliaryReconstructionFunctions/WENO5ZA/Smoothness Indicator-2.xml")), false);
            added = docMan.addDocument(loadDB, "");
                        
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/SpaceDiscretization/FVM/FVM.xml")), false);
            added = docMan.addDocument(loadDB, "");
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/FVMMethods/FluxMethods/FVM with LLF Rusanov.xml")), false);
            added = docMan.addDocument(loadDB, "");
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/ParticlesMethods/Reciprocity_SPH/SPH interpolation function.xml")), false);
            added = docMan.addDocument(loadDB, "");
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/ParticlesMethods/Reciprocity_SPH/SPH normalization function.xml")), false);
            added = docMan.addDocument(loadDB, "");
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/ParticlesMethods/Kernels/Wendland.xml")), false);
            added = docMan.addDocument(loadDB, "");
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/ParticlesMethods/Kernels/Gradient Wendland.xml")), false);
            added = docMan.addDocument(loadDB, "");
            
            //WENO5Z prestep
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/PresSteps/WENO5Z prestep.xml")), false);
            added = docMan.addDocument(loadDB, "");
                                    
            //WENO5 prestep
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/PresSteps/WENO5 prestep.xml")), false);
            added = docMan.addDocument(loadDB, "");
            
            //WENO3 prestep
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/PresSteps/WENO3 prestep.xml")), false);
            added = docMan.addDocument(loadDB, "");
           
            //WENO3YC prestep
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/PresSteps/WENO3YC prestep.xml")), false);
            added = docMan.addDocument(loadDB, "");
            
            //WENO5ZA prestep
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/PresSteps/WENO5ZA prestep.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
                       
            //FDOC3 prestep
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/PresSteps/FDOC3 prestep.xml")), false);
            added = docMan.addDocument(loadDB, "");
            
            //FDOC5 prestep
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/PresSteps/FDOC5 prestep.xml")), false);
            added = docMan.addDocument(loadDB, "");
            
            //MP5 prestep
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/PresSteps/MP5 prestep.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
         
            //MP5_HLL prestep
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/PresSteps/MP5_HLL prestep.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));

            //PPM prestep
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/PresSteps/PPM prestep.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
  
            
            //MP5 prestep
            /*loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/PresSteps/MP5.xml")), false);
            added = docMan.addDocument(loadDB, "");*/
            
            //RK3 - FDOC
           /* loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
	    		getResourceAsStream("/schemaMarriage.xml")), false);
		    added = docMan.addDocument(loadDB, "");
		    doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
		    schema = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
            DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();*/
            //RK3 - WENO
            /*loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
            		getResourceAsStream("/schemas/HyperbolicSolvers/MoL/RK3 WENO3.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            schema = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();*/
            
            //RK4 - FDOC3
           /* loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
            		getResourceAsStream("/schemas/HyperbolicSolvers/MoL/RK4 FDOC3.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            schema = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();*/
            
            //RK4 - FDOC3
            /* loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
             		getResourceAsStream("/schemas/HyperbolicSolvers/MoL/RK4 FDOC3_ms.xml")), false)
            		 .replaceAll("FDOCID", fdocid).replaceAll("FDOCprestep", fdoc3pre);
             added = docMan.addDocument(loadDB, "");
             doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
             schema = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                     DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();*/
            
            //RK4 - FDOC5
            /*loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
            		getResourceAsStream("/schemas/HyperbolicSolvers/MoL/RK4 FDOC5.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            schema = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();*/
            //RK4 - WENO3
           /* loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
            		getResourceAsStream("/schemas/HyperbolicSolvers/MoL/RK4 WENO3.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            schema = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();*/
    
            //RK3 - PPM
            /*loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
            		getResourceAsStream("/schemas/HyperbolicSolvers/MoL/RK3PPMConservadas.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            schema = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();*/
            
            //RK3 - FDOC3 - SPH
          /*  loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
            		getResourceAsStream("/schemas/HyperbolicSolvers/MoL/RK3 FDOC3 SPH.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            schema = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();*/
            
            //Predictor Corrector - FDOC3 - SPH
            /*loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
            		getResourceAsStream("/schemas/HyperbolicSolvers/MoL/Predictor Corrector FDOC3 SPH.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            schema = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();*/
                    

            //RK4 - FDOC3 - SPH
           /* loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
            		getResourceAsStream("/schemas/HyperbolicSolvers/MoL/RK4 FDOC3 SPH.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            schema = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();*/
            
            //IMEX3-433 MP5
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
            		getResourceAsStream("/schemas/HyperbolicSolvers/MoL/IMEX3-433 MP5.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            schema = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            //IMEX3-433 CD
            /*loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
            		getResourceAsStream("/schemas/HyperbolicSolvers/MoL/IMEX3-433 CD.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            schema = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();*/
            
            //IMEX3-433 WENO3
           /* loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
            		getResourceAsStream("/schemas/HyperbolicSolvers/MoL/IMEX3-433 WENO3.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            schema = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();*/
            
            //RK4 - CD - SPH
           /*loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
            		getResourceAsStream("/schemas/HyperbolicSolvers/MoL/RK4 CD SPH.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            schema = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();*/
            
            //RK4 - PPM
           /* loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
            		getResourceAsStream("/schemas/HyperbolicSolvers/MoL/RK4 PPM.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            schema = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();*/
            
            
            //RK4 - MP5
           /*loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
           		getResourceAsStream("/schemas/HyperbolicSolvers/MoL/RK4 MP5.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            schema = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();*/
            
          //RK3 - MP5
        /*       loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
              		getResourceAsStream("/schemas/HyperbolicSolvers/MoL/RK3 MP5.xml")), false);
               added = docMan.addDocument(loadDB, "");
               doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
               schema = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                      DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();*/
          //RK4 - MP5 - SPH
           /* loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
               		getResourceAsStream("/schemas/HyperbolicSolvers/MoL/RK4 MP5 SPH.xml")), false);
                added = docMan.addDocument(loadDB, "");
                doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
                schema = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                       DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();*/

            //RK4 - MP5_HLL
           /* loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
           		getResourceAsStream("/schemas/HyperbolicSolvers/MoL/RK4 MP5_HLL.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            schema = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();*/

            
            //RK4 - WENO5
           /* loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
            		getResourceAsStream("/schemas/HyperbolicSolvers/MoL/RK4 WENO5.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            schema = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();*/
            
            //RK4 - WENO5Z
           /* loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
            		getResourceAsStream("/schemas/HyperbolicSolvers/MoL/RK4 WENO5Z.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            schema = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
             */
             
            //RK4 - WENO5Z + SPH
           /* loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
            		getResourceAsStream("/schemas/HyperbolicSolvers/MoL/RK4 WENO5Z SPH.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            schema = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();*/
 
            //RK4 - WENO5ZA
            /*loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
            		getResourceAsStream("/schemas/HyperbolicSolvers/MoL/RK4 WENO5ZA.xml")), false)
            		.replace("WENO5ZAPREID", w5zpre);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            schema = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();*/
            
            //RK4 - WENO3YC
            /*loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
            		getResourceAsStream("/schemas/HyperbolicSolvers/MoL/RK4 WENO3YC.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            schema = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();*/
            
            //RK4 - CD only
        /*    loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
            		getResourceAsStream("/schemas/HyperbolicSolvers/MoL/RK4 CD.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            schema = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
        */
            //RK3 - CD only
            /*loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
            		getResourceAsStream("/schemas/HyperbolicSolvers/MoL/RK3 CD.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            schema = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();*/
            
            //Operator
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/SpaceDiscretization/FDM/D2-centered-order4.xml")), false);
            added = docMan.addDocument(loadDB, "");
           
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                       getResourceAsStream("/schemas/SpaceDiscretization/FDM/D1-centered-order4.xml")), false);
            added = docMan.addDocument(loadDB, "");

            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/SpaceDiscretization/FDM/D1-centered-order2.xml")), false);
	         added = docMan.addDocument(loadDB, "");
	         doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	         d1cd2 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
	                 DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();

            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/SpaceDiscretization/FDM/D1-centered-order4-crossed.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
           		getResourceAsStream("/operatorEinsteinCrossed.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        operator = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                   DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
                       
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                     getResourceAsStream("/schemas/SpaceDiscretization/FDM/D2-centered-order2.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            String d2cd2 = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                  DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
	        
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/operatorWave.xml")), false)
                    .replaceAll("CDID", d1cd2).replaceAll("CD4ID", d2cd2);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        String operatorBoundaries = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
	                DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
	        
	        loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/SpaceDiscretization/FDM/D1-backward-order4.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            String blie = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/SpaceDiscretization/FDM/D1-forward-order4.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            String flie = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
	        
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/SpaceDiscretization/FDM/Rotor derivative.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            String rotorID = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
        
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/SpaceDiscretization/SPH/SPH-1st Normalization.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/SpaceDiscretization/SPH/SPH-1st Symmetric.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/SpaceDiscretization/SPH/SPH-1st Antisymmetric.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/schemas/SpaceDiscretization/SPH/SPH-2nd Antisymmetric.xml")), false);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/operatorWaveLieBackward.xml")), false)
                    .replaceAll("LIEBID", blie);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            blieOp = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
        
        
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                        getResourceAsStream("/operatorWaveLieForward.xml")), false)
                        .replaceAll("LIEFID", flie);
            added = docMan.addDocument(loadDB, "");
            doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            flieOp = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
                    DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
	        
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/operatorRotor.xml")), false)
                    .replaceAll("ROTORID", rotorID);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        String operatorRotor = ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagNameNS(
	                DocumentManagerImpl.MMSURI, "id").item(0).getTextContent();
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/policies/OpSPHSymmetric.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
            
            loadDB = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
                    getResourceAsStream("/policies/OpSPHAntisymmetric.xml")), false);
	        added = docMan.addDocument(loadDB, "");
	        doc = AGDMUtils.stringToDom(SimflownyUtils.jsonToXML(added));
	        
            //Loading the discretization Policy
            String policy = SimflownyUtils.xmlToJSON(AGDMUtils.convertStreamToString(getClass().
            /*        getResourceAsStream("/policyMarriage.xml")), false);
            policy = policy.replaceAll("PROBLEMID", problemID).replaceAll("OPERATORID", operator).replaceAll("SCHEMAID", schema); */
                  //  getResourceAsStream("/policyMallaParticulasAdvection.xml")), false); //SPH + FDM
           //  		getResourceAsStream("/policyPic_species3D.xml")), false);
           // 		getResourceAsStream("/policyCCZ4MHDTab.xml")), false);
           // 		getResourceAsStream("/Carlos/reprimand/PDEDiscretizationPolicy.xml")), false);
            		//getResourceAsStream("/Carlos/tabeos/PDEDiscretizationPolicy.xml")), false);
            	//	getResourceAsStream("/Carlos/eikonal/PDEDiscretizationPolicy_eikonal.xml")), false);
           // 		getResourceAsStream("/Ricard/particles/sin_Lorene/PDEDiscretizationPolicy.xml")), false);
           // 		getResourceAsStream("/Ricard/particles/con_Lorene/Tab_nonzero/non_static_metric/PDEDiscretizationPolicy.xml")), false);
            // 		getResourceAsStream("/Ricard/particles/con_Lorene/Tab_nonzero/non_static_metric/massive_particles/PDEDiscretizationPolicy.xml")), false);
           //  		getResourceAsStream("/Carlos/les+eikonal/xml/Policy CCZ4+MHD+tabeos+leakage + les (MP5).xml")), false);
            //		getResourceAsStream("/Carlos/les/PDEDiscretizationPolicy.xml")), false);
            		getResourceAsStream("/Carlos/M1/Policy CCZ4+MHD+M1 (MP5).xml")), false);
            //		getResourceAsStream("/Daniele/Planet_average/Planet2D_policy.xml")), false);
             //		getResourceAsStream("/Manuel/0_Manuel_Policy CCZ4gauge+SF2.xml")), false);
           //  		getResourceAsStream("/Carlos/Poisson/PDEDiscretizationPolicy.xml")), false);
            //		getResourceAsStream("/Carlos/boson/Policy CCZ4gauge+SF2.xml")), false);
            // 		getResourceAsStream("/Ricard/Error/CCZ4+particules+Schwarzschild+Geodesics.xml")), false);
           // 		getResourceAsStream("/Daniele/Pulsar.xml")), false);
           // 		getResourceAsStream("/policyEinsteinAdvection.xml")), false);
           	//	getResourceAsStream("/policyAlfven.xml")), false);
            policy = policy.replaceAll("PROBLEMID", problemID).replaceAll("OPERATORID", operator).replaceAll("SCHEMAID", schema)
                 .replaceAll("LIEFID", flieOp).replaceAll("LIEBID", blieOp).replaceAll("OPERATORBOUND", operatorBoundaries)
                 .replaceAll("OPROTORID", operatorRotor);  
            
            String result = new AGDMImpl().discretizeProblem(policy);
            System.out.println("Fin manual");
            FileWriter fichero = new FileWriter("testMarriage.xml");
            PrintWriter pw = new PrintWriter(fichero);
            pw.write(SimflownyUtils.jsonToXML(result));
            fichero.close();
    	}
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Error in Marriage test: " + e.getMessage());
        }
    }
    
    /**
     * Remove the whitespaced nodes from a Dom element.
     * 
     * @param e The element to delete the whitespaces from
     */
    public  void removeWhitespaceNodes(Element e) {
        NodeList children = e.getChildNodes();
        for (int i = children.getLength() - 1; i >= 0; i--) {
            Node child = children.item(i);
            if (child instanceof Text && ((Text) child).getData().trim().length() == 0) {
                e.removeChild(child);
            }
            else if (child instanceof Element) {
                removeWhitespaceNodes((Element) child);
            }
        }
    }
}
