/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/

package eu.iac3.mathMS.latexPdfGeneratorPlugin;

import javax.jws.WebService;

/** 
 * LatexPDFGenerator is an interface that provides the functionality 
 * to transform XML into Latex or pdf, 
 * assuming the mathematical expressions are in MathML.
 * <p>
 * The interface has functions which accept XML as input and 
 * return either Latex or PDF files as output.
 * 
 * @author      ----
 * @version     ----
 */
@WebService
public interface LatexPDFGenerator {
	
	
    /** 
     * It returns the Latex file as a result to 
     * transform the document to latex.
     *
     * @param jsonDoc         the document to be transformed.
     * @return               the Latex file in an array of bytes.
     * @throws LPGException 
     *                       LPG001   Not a valid XML Document
     *                       LPG002   XML document does not match XML Schema
     *                       LPG003   XML transform error
     */
    byte[] xmlToLatex(String jsonDoc) throws LPGException;
     
     /** 
      * It returns the Pdf file as a result to 
      * transform the document to pdf.
      *
      * @param jsonDoc          the document to be transformed
      * @return                 the pdf file in an array of bytes.
      * @throws LPGException   
      *                          LPG001 Not a valid XML Document
      *                          LPG002   XML document does not match XML Schema
      *                          LPG003   XML transform error
      */
    byte[] xmlToPdf(String jsonDoc) throws LPGException;

}
