/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/

package eu.iac3.mathMS.latexPdfGeneratorPlugin;


import java.io.File;
import java.io.UnsupportedEncodingException;

import javax.xml.transform.TransformerException;

import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.log.LogService;
import org.osgi.util.tracker.ServiceTracker;
import org.w3c.dom.Document;

import eu.iac3.mathMS.simflownyUtilsPlugin.SUException;
import eu.iac3.mathMS.simflownyUtilsPlugin.SimflownyUtils;
import eu.iac3.mathMS.simflownyUtilsPlugin.SimflownyUtilsImpl;

/** 
 * LatexPDFGeneratorServ
 * This class offers the LatexPDF generator functionality as a
 * WSDL service.
 * This class transforms XML into Latex or pdf, 
 * assuming the mathematical expressions are in MathML.
 * <p>
 * Xml accepted by the generator are: physicalModel, simulationProblem, 
 * discretizationSchema y discreteModel. 
 * It has functions which accept XML as input and 
 * return either Latex or PDF files as output.
 * 
 * class needs to have the system installed latex 
 * @author      ----
 * @version     ----
 */
@Component //
(//
    immediate = true, //
    name = "LatexPDFGenerator", //
    property = //
    { //
      "service.exported.interfaces=*", //
      "service.exported.configs=org.apache.cxf.ws", //
      "org.apache.cxf.ws.address=/LatexPDFGenerator" //
    } //
)
public class LatexPDFGeneratorImpl implements LatexPDFGenerator {	
	
    private BundleContext context;
    private LatexPDFGeneratorUtils utils;
    static LogService logservice = null;
	
    /**
     * Constructor.
     * @throws LPGException 
     */
    public LatexPDFGeneratorImpl() throws LPGException {
    	super();
    	utils = new LatexPDFGeneratorUtils();
    	context = FrameworkUtil.getBundle(getClass()).getBundleContext();
        if (context !=  null) {
            @SuppressWarnings({ "unchecked", "rawtypes" })
			ServiceTracker logServiceTracker = new ServiceTracker(context, org.osgi.service.log.LogService.class.getName(), null);
            logServiceTracker.open();
            logservice = (LogService) logServiceTracker.getService();
        }
    }
    
	/** 
     * It returns the Latex file as a result to 
     * transform the document to latex.
     *
     * @param jsonDoc    the document to be transformed
     * @return          the Latex file in an array of bytes.
     * @throws LPGException 
     * 						LPG001   Not a valid XML Document
     *                      LPG002   XML document does not match XML Schema
     *                      LPG003   XML transform error
     */
    public byte[] xmlToLatex(String jsonDoc)throws LPGException {
        String resultAux = utils.getLatexHeader();
        byte[] result;
        try {
            if (logservice != null) {
                logservice.log(LogService.LOG_INFO, "LPG: xmlToLatex complex called");
            }
            //JSON to XML
            String xmlDoc;
            try {
                xmlDoc = SimflownyUtils.jsonToXML(jsonDoc);
            } 
            catch (SUException e) {
                throw new LPGException(LPGException.LPG00X, e.getMessage());
            }
            
            Document doc = LatexPDFGeneratorUtils.stringToDom(xmlDoc);
            String schemaType = doc.getDocumentElement().getLocalName();
            result = utils.parseXSL(xmlDoc, "common" + File.separator + "xsl" + File.separator + schemaType + ".xsl", logservice);
            resultAux = resultAux + new String(result, "UTF-8");
            resultAux = resultAux + utils.getLatexFooter();
            resultAux = utils.removeLineSpaces(resultAux);
            result = resultAux.getBytes("UTF-8");
        } 
        catch (TransformerException e) {
            e.printStackTrace();
            throw new LPGException(LPGException.LPG003, e.getMessage());
        } 
        catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            throw new LPGException(LPGException.LPG003, e.getMessage());
        }
        if (logservice != null) {
            logservice.log(LogService.LOG_INFO, "LPG: xmlToLatex simProblem completed");                    
        }
        return result;
    }
	
	/** 
     * It returns the Pdf file as a result to 
     * transform the document to pdf.
     *
     * @param jsonDoc   the document to be transformed
     * @return          the pdf file in an array of bytes.
     * @throws LPGException 
     * 						LPG001   Not a valid XML Document
     *                      LPG002   XML document does not match XML Schema
     *                      LPG003   XML transform error
     */
    public byte[] xmlToPdf(String jsonDoc)throws LPGException {        
    	return utils.latexToPdf(this.xmlToLatex(jsonDoc), logservice);
    }
    
}
