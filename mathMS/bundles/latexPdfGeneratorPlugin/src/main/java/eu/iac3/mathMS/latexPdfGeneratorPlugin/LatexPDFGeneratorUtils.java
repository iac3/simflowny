/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.latexPdfGeneratorPlugin;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.Random;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.osgi.service.log.LogService;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;

/**
 * Common utilites of the latexPdfGenerator.
 */
public class LatexPDFGeneratorUtils {
	
    private TransformerFactory tFactory;
	
    /**
     * Constructor.
     */
    public LatexPDFGeneratorUtils() {
    	//System.setProperty("javax.xml.transform.TransformerFactory", "org.apache.xalan.processor.TransformerFactoryImpl");
    	tFactory = new net.sf.saxon.TransformerFactoryImpl();
    	//tFactory = TransformerFactory.newInstance();
    }
	
    /**
     * Returns the Latex standard header.
     * @return returns the latex header as a String.
     */    
    public String getLatexHeader() {
        String eol = System.getProperty("line.separator");
    	return "\\documentclass[11pt]{article}" + eol
				+ "\\usepackage{amssymb}" + eol
				//Packages to break the equations in lines automatically
				+ "\\usepackage[fleqn]{amsmath}" + eol
				+ "\\usepackage{enumitem}" + eol
				+ "\\usepackage{color}" + eol
				+ "\\usepackage[dvipsnames]{xcolor}" + eol
				+ "\\renewcommand{\\baselinestretch}{0.8}" + eol
				+ "\\usepackage{array,tabularx,colortbl}" + eol
				+ "\\usepackage[breakable, skins]{tcolorbox}" + eol
				+ "\\usepackage{breqn}" + eol
				+ "\\eqnumsep=4em" + eol
				//Some mathematical symbols do not shows correctly with breqn
				+ "\\DeclareFlexSymbol{\\Gamma}  {Var}{latin}{00}" + eol
                + "\\DeclareFlexSymbol{\\Delta}  {Var}{latin}{01}" + eol
                + "\\DeclareFlexSymbol{\\Theta}  {Var}{latin}{02}" + eol
                + "\\DeclareFlexSymbol{\\Lambda} {Var}{latin}{03}" + eol
                + "\\DeclareFlexSymbol{\\Xi}     {Var}{latin}{04}" + eol
                + "\\DeclareFlexSymbol{\\Pi}     {Var}{latin}{05}" + eol
                + "\\DeclareFlexSymbol{\\Sigma}  {Var}{latin}{06}" + eol
                + "\\DeclareFlexSymbol{\\Upsilon}{Var}{latin}{07}" + eol
                + "\\DeclareFlexSymbol{\\Phi}    {Var}{latin}{08}" + eol
                + "\\DeclareFlexSymbol{\\Psi}    {Var}{latin}{09}" + eol
                + "\\DeclareFlexSymbol{\\Omega}  {Var}{latin}{0A}" + eol
                + "\\DeclareFlexSymbol{0}{Var}{latin}{30}" + eol
                + "\\DeclareFlexSymbol{1}{Var}{latin}{31}" + eol
                + "\\DeclareFlexSymbol{2}{Var}{latin}{32}" + eol
                + "\\DeclareFlexSymbol{3}{Var}{latin}{33}" + eol
                + "\\DeclareFlexSymbol{4}{Var}{latin}{34}" + eol
                + "\\DeclareFlexSymbol{5}{Var}{latin}{35}" + eol
                + "\\DeclareFlexSymbol{6}{Var}{latin}{36}" + eol
                + "\\DeclareFlexSymbol{7}{Var}{latin}{37}" + eol
                + "\\DeclareFlexSymbol{8}{Var}{latin}{38}" + eol
                + "\\DeclareFlexSymbol{9}{Var}{latin}{39}" + eol
                + "\\usepackage{float}" + eol + eol
                + "\\oddsidemargin 9pt" + eol
                + "\\textwidth 430pt" + eol
                + "\\definecolor{almostwhite}{HTML}{F6F6F6}" + eol
                + "\\setlength{\\parskip}{0em}" + eol + eol
                + "\\newtcolorbox{mygrouping}{enhanced, breakable, after skip=1em, boxrule=0.5pt, colframe=lightgray!75!black, colback=almostwhite}" + eol + eol
                + "\\newcommand{\\breakingcomma}{%" + eol
                + "    \\begingroup\\lccode`~=`," + eol
                + "    \\lowercase{\\endgroup\\expandafter\\def\\expandafter~\\expandafter{~\\penalty0 }}}" + eol + eol		
                + "\\newtcolorbox{mytable}[3][]{tabular*={\\renewcommand{\\arraystretch}{1.7}}{#2}, before={\\begin{table}[H]\\refstepcounter{table}\\hspace*{#3}}, after={\\end{table}}}" + eol + eol
                + "\\makeatletter" + eol
                + "\\tcbset{" + eol
                + "    tabular*/.style 2 args={%" + eol
                + "    boxsep=0pt,top=0pt,bottom=0pt,leftupper=0pt,rightupper=0pt,boxrule=0.3mm,hbox,colframe=lightgray!75!black, colback=almostwhite," + eol
                + "    before upper={\\arrayrulecolor{tcbcol@frame}\\def\\arraystretch{1.1}#1%" + eol
                + "    \\tcb@hack@currenvir\\tabular{#2}}," + eol
                + "    after upper=\\endtabular\\arrayrulecolor{black}}," + eol
                + "}" + eol
                + "\\makeatother" + eol + eol
                + "\\makeatletter" + eol
                + "\\newcommand{\\hlinegray}{%" + eol
                + "    \\arrayrulecolor{lightgray} \\hline" + eol
                + "}" + eol + eol
                + "\\newcolumntype{|}{@{\\hskip\\tabcolsep\\color{lightgray}\\vrule width 0.5pt\\hskip\\tabcolsep}}" + eol
                + "\\makeatother" + eol + eol
				+ "\\begin{document}" + eol;
    }
	
    /**
     * Returns the Latex standard footer.
     * @return The latex footer.
     */
    public String getLatexFooter() {
        String eol = System.getProperty("line.separator");
    	return "\\end{document}" + eol;
    }
    
    /**
     * Removes the line spaces from a text.
     * @param text      The text
     * @return          The clean test
     */
    public String removeLineSpaces(String text) {
    	String result = "";
    	
    	String eol = System.getProperty("line.separator");
    	
    	String[] resAux = text.split(eol);
    	for (int i = 0; i < resAux.length; i++) {
    	    String line = resAux[i];
    	    line = line.trim();
    	    result = result + line + eol;
    	}
    	
    	return result;
    }
	
    /**
     * Parses a xml with a xlst.
     * @param xml       The xml to parse.
     * @param xslPath   The path of the XSL.
     * @param logservice the logservice class utility.
     * @return the result to apply the xlt to the xml.
     * @throws TransformerException transformer exception.
     * @throws UnsupportedEncodingException exception.
     */
    public byte[] parseXSL(String xml, String xslPath, LogService logservice) throws TransformerException, UnsupportedEncodingException {		
    	ByteArrayInputStream input = new ByteArrayInputStream(xml.getBytes("UTF-8"));
    	ByteArrayOutputStream output = new ByteArrayOutputStream();
    	Transformer transformer = null;
        try {
            transformer = tFactory.newTransformer(new StreamSource(xslPath));
            transformer.transform(new StreamSource(input), new StreamResult(output));
        } 
        catch (Exception e) {
            e.printStackTrace();
        }		
    	return output.toByteArray();    	
    }
       
    /**
     * Pass a latex data into a pdf data.
     * @param latex      The latex data to convert.
     * @param logservice Osgi framework log utility.
     * @return the pdf obtained as an array of bytes.
     * @throws LPGException return LPG003:Xml transformation error.
     */
    public byte[] latexToPdf(byte[] latex, LogService logservice) throws LPGException {
    	byte[] pdf = null;
    	final int seed = 19580427;
    	final int rang = 100;
    	try {						
			//obtan the temporary name file
    	    Random generator = new Random(seed);
    	    int randomIndex = generator.nextInt(rang) + 1;
    	    String name = String.valueOf(System.currentTimeMillis()) + String.valueOf(randomIndex);
    	    String dir = "tmp" + File.separator + name;
			
			//set the temporary folder
    	    File folder =  new File(dir);
    	    if (!folder.exists()) {
    	    	folder.mkdirs();
    	    }
			//saves the latex file into the temporary folder			
    	    String filePath = dir + File.separator + name;
    	    File latexFile = new File(filePath + ".tex");
    	    copy(latex, latexFile);
			
			//command line
    	    String cmd = "pdflatex -interaction nonstopmode " + name + ".tex";
			// Get runtime instance
    	    Runtime r = Runtime.getRuntime();
			//execute the pdflatex command
    	    Process p = r.exec(cmd, null, folder);
			//output readers
    	    BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
    	    BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));
    	    OutputStream stdin = p.getOutputStream();
    	    String s;
    	    boolean error = true;
    	    while (error) {
    	    	boolean errorAux = false;
    	    	s = stdInput.readLine();
    	    	while (s != null && !errorAux) {
    	    	    if (s.startsWith("?")) {
    	    	    	errorAux = true;
    	    	    	stdin.write("\n".getBytes());
    	    	    	stdin.flush();
    	    	    }
    	            s = stdInput.readLine();
    	    	}
                s = stdError.readLine();
    	    	while (s != null && !errorAux) {
    	    	    if (s.startsWith("?")) {
    	    	    	errorAux = true;
    	    	    	stdin.write("\n".getBytes());
    	    	    	stdin.flush();
    	    	    }
    	            s = stdError.readLine();
    	    	}
    	    	if (!errorAux) {        	    
    	    	    error = false;
    	    	}        	    
    	    }
    	    
    	    p.waitFor();

		    //Loads the generated pdf file
    	    pdf = getFile(filePath + ".pdf");
		    
		    //deletes all the generated files.
    	    deleteFolder(folder);

    	} 
    	catch (IOException e1) {
    	    if (logservice != null) {
                logservice.log(LogService.LOG_ERROR, "latexToPdf IOException " + e1.toString());
            }
            throw new LPGException(LPGException.LPG003, e1.getMessage());
    	} 
    	catch (InterruptedException e) {
    	    if (logservice != null) {
                logservice.log(LogService.LOG_ERROR, "latexToPdf InterruptedException " + e.toString());
            }
            throw new LPGException(LPGException.LPG003, e.getMessage());
    	}
    	return pdf;
    }
    
    /**
     * copy an array of bytes into a file.
     * @param data The array of bytes to copy.
     * @param target The tarjet file.
     * @throws IOException io exception.
     */
    private  void copy(byte[] data, File target) throws IOException {
    	OutputStream fout = new FileOutputStream(target);
        OutputStream bout = new BufferedOutputStream(fout);
        OutputStreamWriter out = new OutputStreamWriter(bout, "UTF-8");
        out.write(new String(data, "UTF-8"));
        out.flush();
        out.close();
        fout.close();
    }
    
    /**
     * loads the content of a file into a byte array.
     * @param filePath The path of the file to read.
     * @return the content of the file.
     * @throws IOException io excepcion.
     */
    private byte[] getFile(String filePath) throws IOException {
    	ByteArrayOutputStream baos = new ByteArrayOutputStream();
    	InputStream is;
    	byte[] buf = null;
    	File file = new File(filePath);
    	if (file.exists()) {
    	    is = new FileInputStream(filePath);
    	    buf = new byte[is.available()];
            int len = is.read(buf);
            while (len != -1) {
                baos.write(buf, 0, len);
                len = is.read(buf);
            }			            
    	    baos.flush();
    	    baos.close();
    	    buf = baos.toByteArray();
    	}
    	return buf;
    }
    
    /**
     * Deletes a folder and his content.
     * @param folder The folder to delete.
     */
    private static void deleteFolder(File folder) {
    	File[] listOfFiles = folder.listFiles();
    	for (int i = 0; i < listOfFiles.length; i++) {
    	    if (listOfFiles[i].isFile()) {
    	    	listOfFiles[i].delete();
    	    } 
    	}
    	folder.delete();
    }
    
    /**
     * Converts a string to Dom.
     * 
     * @param str               The string
     * @return                  The Dom document
     * @throws LPGException    
     */
    public static Document stringToDom(String str) throws LPGException {
        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            docFactory.setNamespaceAware(true);
            docFactory.setIgnoringElementContentWhitespace(true);
            docFactory.setIgnoringComments(true);
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            InputStream is = new ByteArrayInputStream(str.getBytes("UTF-8"));
            Document result = docBuilder.parse(is);
            removeWhitespaceNodes(result.getDocumentElement());
            return result;
        }
        catch (SAXException e) {
            throw new LPGException(LPGException.LPG001, e.toString());
        }
        catch (Exception e) {
            throw new LPGException(LPGException.LPG001, e.toString());
        }
    }
    
    /**
     * Remove the white spaced nodes from a Dom element.
     * 
     * @param e The element to delete the whitespaces from
     */
    public static void removeWhitespaceNodes(Element e) {
        NodeList children = e.getChildNodes();
        for (int i = children.getLength() - 1; i >= 0; i--) {
            Node child = children.item(i);
            if (child instanceof Text && ((Text) child).getData().trim().length() == 0) {
                e.removeChild(child);
            }
            else if (child instanceof Element) {
                removeWhitespaceNodes((Element) child);
            }
        }
    }
}
