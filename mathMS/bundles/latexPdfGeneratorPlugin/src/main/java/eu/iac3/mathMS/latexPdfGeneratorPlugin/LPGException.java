/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.latexPdfGeneratorPlugin;

/** 
 * LPGException is the class Exception launched by the 
 * {@link LatexPDFGenerator LatexPDFGenerator} interface.
 * The exceptions that this class return are:
 * LPG001	Not a valid XML Document
 * LPG002	XML document does not match XML Schema
 * LPG003	XML transform error
 * LPG00X   External error
 * class needs to have the system installed latex 
 * @author      ----
 * @version     ----
 */
@SuppressWarnings("serial")
public class LPGException extends Exception {

    static final String LPG001 = "Not a valid XML document";
    static final String LPG002 = "XML document does not match XML Schema";
    static final String LPG003 = "XML transformation error";
    static final String LPG00X = "External error";
	
    //External error message
    private String extError;
	  /** 
	     * Constructor for known messages.
	     *
	     * @param msg  the message	     
	     */
    LPGException(String msg) {
    	super(msg);
    }
	  
	  /** 
	     * Constructor.
	     *
	     * @param intMsg  the internal error message	    
	     * @param extMsg  the external error message
	     */
    LPGException(String intMsg, String extMsg) {
		super(intMsg);
        extError = extMsg;
    }
	  /** 
     * Returns the error code.
     *
     * @return the error code	     
     */
    public String getErrorCode() {
    	return super.getMessage();
    }
	  /** 
	     * Returns the error message as a string.
	     *
	     * @return the error message	     
	     */
    public String getMessage() {
    	if (extError == null) {
            return "LPGException: " + super.getMessage();
    	}
    	else {
            return "LPGException: " + super.getMessage() + " (" + extError + ")";
    	}
    }  
}
