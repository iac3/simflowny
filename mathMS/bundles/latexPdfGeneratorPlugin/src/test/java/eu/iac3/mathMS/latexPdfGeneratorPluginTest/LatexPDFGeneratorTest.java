/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.latexPdfGeneratorPluginTest;

import eu.iac3.mathMS.latexPdfGeneratorPlugin.LatexPDFGenerator;
import eu.iac3.mathMS.latexPdfGeneratorPlugin.LatexPDFGeneratorImpl;
import eu.iac3.mathMS.simflownyUtilsPlugin.SimflownyUtils;

import static org.junit.Assert.*;
import static org.ops4j.pax.exam.CoreOptions.bootDelegationPackages;
import static org.ops4j.pax.exam.CoreOptions.junitBundles;
import static org.ops4j.pax.exam.CoreOptions.mavenBundle;
import static org.ops4j.pax.exam.CoreOptions.options;
import static org.ops4j.pax.exam.CoreOptions.systemProperty;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.junit.runner.RunWith;
import org.ops4j.pax.exam.Configuration;
import org.ops4j.pax.exam.Option;
import org.ops4j.pax.exam.junit.PaxExam;
import org.ops4j.pax.exam.spi.reactors.ExamReactorStrategy;
import org.ops4j.pax.exam.spi.reactors.PerSuite;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;


/** 
 * LatexPDFGeneratorTest class is a junit testcase to
 * test the {@link LatexPDFGenerator LatexPDFGenerator}.
 * It has eight tests:
 * Four tests to test the LaTeX generation, one 
 * for each schema type. 
 * Four tests to test the PDF generation, one
 * for each schema type.
 * @author      ----
 * @version     ----
 */
@RunWith(PaxExam.class)
@ExamReactorStrategy(PerSuite.class)
public class LatexPDFGeneratorTest {
        
    long startTime;
    
    @Rule 
    public TestName name = new TestName();

    /**
     * Configuration for Pax-Exam.
     * @return options
     * @throws IOException 
     * @throws FileNotFoundException 
     */
    @Configuration
    public Option[] config() {
    	
        return options(
    		bootDelegationPackages(
    				 "com.sun.*"
    				),
        	systemProperty("file.encoding").value("UTF-8"),
        	
        	mavenBundle("org.apache.felix", "org.osgi.compendium", "1.4.0"),
        	mavenBundle("org.apache.felix", "org.apache.felix.log", "1.0.1"),
            mavenBundle("org.apache.commons", "commons-lang3", "3.7"),
            mavenBundle("org.apache.commons", "commons-text", "1.3"),
            mavenBundle("org.osgi", "org.osgi.enterprise", "5.0.0"), 
            mavenBundle("org.apache.felix", "org.apache.felix.scr.annotations", "1.12.0"),
            mavenBundle("org.apache.felix", "org.apache.felix.scr", "2.1.0"),
            mavenBundle("org.osgi", "org.osgi.dto", "1.1.0"),
            mavenBundle("org.osgi", "org.osgi.core", "6.0.0"),
            mavenBundle("eu.iac3.thirdParty.bundles", "xml-apis", "1.0"),
            mavenBundle("org.apache.servicemix.bundles", "org.apache.servicemix.bundles.relaxngDatatype", "2011.1_1"),
            mavenBundle("org.apache.servicemix.bundles", "org.apache.servicemix.bundles.xmlresolver", "1.2_5"),
            mavenBundle("org.apache.servicemix.bundles", "org.apache.servicemix.bundles.saxon", "9.8.0-10_1"),
            mavenBundle("org.apache.servicemix.bundles", "org.apache.servicemix.bundles.derby", "10.12.1.1_1"),
            mavenBundle("org.ops4j.pax.jdbc", "pax-jdbc-derby", "1.3.0"), 
            mavenBundle("eu.iac3.thirdParty.bundles.xsom", "xsom", "20140925"),
            mavenBundle("com.google.code.gson", "gson", "2.8.4"),
            mavenBundle("org.json", "json", "20180130"),
            mavenBundle("org.ops4j.pax.logging", "pax-logging-api", "1.10.1"),
            mavenBundle("org.ops4j.pax.logging", "pax-logging-service", "1.10.1"),
            mavenBundle("org.ops4j.pax.logging", "pax-logging-log4j2", "1.10.1"),
            mavenBundle("com.fasterxml.jackson.core", "jackson-core", "2.10.3"),
            mavenBundle("com.fasterxml.jackson.core", "jackson-annotations", "2.10.3"),
            mavenBundle("com.fasterxml.jackson.core", "jackson-databind", "2.10.3"),
            mavenBundle("eu.iac3.thirdParty.bundles.underscore", "underscore", "1.60"),
            mavenBundle("org.yaml", "snakeyaml", "1.26"),
            mavenBundle("com.fasterxml.jackson.dataformat", "jackson-dataformat-yaml", "2.10.3"),
            mavenBundle("eu.iac3.thirdParty.bundles.snuggletex", "snuggletex-core-upconversion", "1.2.2"),
            mavenBundle("org.mozilla", "rhino", "1.7.12"),
            mavenBundle("eu.iac3.mathMS", "simflownyUtilsPlugin", "3.1"),
            junitBundles()
            );
    }
    
    /**
     * Initial setup for the tests.
     */
    @Before
    public void onSetUp() {
        startTime = System.currentTimeMillis();
    }
    
    /**
     * Close browser when last test.
     */
    @After
    public void onTearDown() {
        System.out.println(name.getMethodName() + ": " + (((float)(System.currentTimeMillis() - startTime)) / 60000));
    }
    
    /**            
     * Checks the {@link xmlToLatex xmlToLatex()} method with a
        * CanonicalPDE schema.	      
        * The CanonicalPDE xml used is: 
        * /mathms/common/testfiles/canonicalPDE.xml
        * To validate the result it compares the obtained latex file 
        * with this file:
        *  /mathms/common/testfiles/canonicalPDE.tex
     */
    @Test
    public void testLatex() {
    	try {
    		LatexPDFGenerator generator =  new LatexPDFGeneratorImpl();
    	    //get the complex document
            String json = SimflownyUtils.xmlToJSON(getStringFromInputStream("/PDEProblem.xml"), false);
    	    String result = new String(generator.xmlToLatex(json), "UTF-8");
            System.out.println(result);
	    	//get the complex latex result
    	    String latex = getStringFromInputStream("/PDEProblem.tex");
    	    assertEquals(latex, result);
    	}
    	catch (Exception e) {
            fail("Error testLatex: " + e.toString());
        }
    }

    /**            
     * Checks the {@link xmlToLatex xmlToLatex()} method with a
        * CanonicalPDE schema.	      
        * The CanonicalPDE xml used is: 
        * src/test/resources/canonicalPDE.xml
        * To validate the result it compares the obtained latex file 
        * with this file:
        *  src/test/resources/canonicalPDE.pdf
     */
    @Test
    public void testPdf() {
        LatexPDFGenerator generator;
    	try {
            generator =  new LatexPDFGeneratorImpl();
    		//get the complex xml file
    	    String json = SimflownyUtils.xmlToJSON(getStringFromInputStream("/PDEProblem.xml"), false);
    	    byte[] resp = generator.xmlToPdf(json);
    	    boolean equals = true;
    	    if (resp == null || resp.length < 50000) {
    	    	equals = false;
    	    }
    	    assertTrue(equals);
    	} 
        catch (Exception e) {
            fail("Error testPdf: " + e.toString());
        } 
    }
	    
    /**
     * Reads a file.
     * @param path  The file path
     * @return      The file as string
     */
    private String getStringFromInputStream(String path) {
        InputStream is = getClass().getResourceAsStream(path);
 
        BufferedReader br = null;
        StringBuilder sb = new StringBuilder();
 
        String line;
        try {
 
            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line + "\n");
            }
 
        } 
        catch (IOException e) {
            e.printStackTrace();
        } 
        finally {
            if (br != null) {
                try {
                    br.close();
                } 
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
 
        return sb.toString();
 
    }

}
