/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.guiTesting;

import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.sun.xml.xsom.XSAnnotation;
import com.sun.xml.xsom.XSAttributeUse;
import com.sun.xml.xsom.XSComplexType;
import com.sun.xml.xsom.XSElementDecl;
import com.sun.xml.xsom.XSModelGroup;
import com.sun.xml.xsom.XSModelGroupDecl;
import com.sun.xml.xsom.XSParticle;
import com.sun.xml.xsom.XSTerm;
import com.sun.xml.xsom.parser.XSOMParser;


import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.sikuli.script.Key;
import org.sikuli.script.Screen;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Class that parses the XSD and XML into JSON.
 * @author bminano
 *
 */
public class SikuliTransformer {
    static final String NEWLINE = System.getProperty("line.separator");
    
    private String xml;
    private Document doc;
    private XSOMParser parser;
    private String rootElement;
    JsonArray struct;
    private boolean emptyComplex;
    private XSElementDecl simml;
    private ArrayList<String> uuids;
    private static Screen screen;
    private ArrayList<String> seqList;
    
    /**
     * Constructor.
     * @param s             Sikuli screen
     * @param input         XML document to transform
     * @throws Exception    Exception
     */
    public SikuliTransformer(Screen s, String input) throws Exception {
        xml = Utils.getStringFromInputStream(new FileInputStream(input));
        String schemaName = Utils.stringToDom(xml).getDocumentElement().getLocalName();
        String schemaPath = "common" + File.separator + "XSD" + File.separator + schemaName + ".xsd";
        String schematronPath = "common" + File.separator + "schematron" + File.separator + schemaName + ".sch";
        
        new Utils();
        seqList = new ArrayList<String>();
        
        parser = new XSOMParser();
        parser.setAnnotationParser(new XSOMAnnotationParser());
        parser.parse(schemaPath);
        
        rootElement = schemaName;
        XSDTagStructureGenerator tagStruct = new XSDTagStructureGenerator();
        tagStruct.addStruct(schemaPath, schematronPath, rootElement);
        struct = new JsonParser().parse(tagStruct.getStruct()).getAsJsonArray();
    
        uuids = new ArrayList<String>();
        simml = null;
        screen = s;
    }
    
    /**
     * Parse the particle element. 
     * @param particle      The particle
     * @param attributes    The attributes of the parent element
     * @param absPath       The xpath of the element
     * @param order         The element order
     * @param siblings      Siblings
     * @param choice        If the element is in a choice
     * @throws Exception    Exception
     */
    private void printParticle(XSParticle particle, Collection<? extends XSAttributeUse> attributes, 
            String absPath, int order, ArrayList<String> siblings, boolean choice) throws Exception {
        int min = particle.getMinOccurs();
        int max = particle.getMaxOccurs();
        XSTerm term = particle.getTerm();
        if (term.isModelGroup()) {
            printGroup(term.asModelGroup(), attributes, absPath);
        }
        else if (term.isModelGroupDecl()) {
            printGroupDecl(term.asModelGroupDecl(), attributes, absPath);
        }
        else if (term.isElementDecl()) {
            printElements(term.asElementDecl(), absPath, min, max, order, siblings, choice);
        }
    }
    
    /**
     * Parse the model group. 
     * @param modelGroup    The model group
     * @param attributes    The attributes of the parent element
     * @param absPath       The xpath of the element
     * @throws Exception    Exception
     */
    private void printGroup(XSModelGroup modelGroup, Collection<? extends XSAttributeUse> attributes, 
            String absPath) throws Exception {
        boolean recursive = false;
        if (seqList.contains(absPath)) {
            recursive = true;
        }
        else {
            seqList.add(absPath);
        }
        boolean choice = modelGroup.getCompositor().toString().equals("choice");
        /*//Only set children info if there is a sequence. When it is a choice just print all the elements without any metadata
        if ((modelGroup.getCompositor().toString().equals("sequence") || modelGroup.getCompositor().toString().equals("choice")) && !rec) {
            w.write(", \"children\":[\n");
        }*/
        //Print attributes
       /* String result = printAttributes(attributes, absPath).toString();
        w.write(result);*/
        //Print children
        XSParticle[] list = modelGroup.getChildren();
        ArrayList<String> siblings = getSiblings(modelGroup);
        emptyComplex = isEmtyComplex(list);
        for (int i = 0; i < list.length; i++) {
            XSParticle particle = list[i];
            printParticle(particle, attributes, absPath, i, siblings, choice);
        }
        if (!recursive) {
            //The first to move to parent
            SikuliCommons.collapseNode(screen);
            //The second to actually collapse
            SikuliCommons.collapseNode(screen);
        }
    }
    
    /**
     * Parse the model group declaration. 
     * @param modelGroupDecl    The model group declaration
     * @param attributes        The attributes of the parent element
     * @param absPath           The xpath of the element
     * @throws Exception        Exception
     */
    private void printGroupDecl(XSModelGroupDecl modelGroupDecl, Collection<? extends XSAttributeUse> attributes, 
            String absPath) throws Exception {
        printGroup(modelGroupDecl.getModelGroup(), attributes, absPath);
    }
    
    /**
     * Parse the complex type.
     * @param complexType       The complex type
     * @param attributes        The attributes of the parent element
     * @param absPath           The xpath of the element
     * @param siblings          Siblings
     * @param choice            If the element is in a choice
     * @param order             The element order
     * @throws Exception        Exception
     */
    private void printComplexType(XSComplexType complexType, Collection<? extends XSAttributeUse> attributes,
            String absPath, int order, ArrayList<String> siblings, boolean choice) throws Exception {
        XSParticle particle = complexType.getContentType().asParticle();
        if (particle != null) {
            printParticle(particle, attributes, absPath, order, siblings, choice);
        }
    }
    
    /**
     * Parse the element with all its data elements.
     * @param element       The element
     * @param absPath       The parent path
     * @param min           The min cardinality
     * @param max           The max cardinality
     * @param choice        If the element is in a choice
     * @param order         The element order
     * @param siblings      Siblings
     * @throws Exception    Exception
     */
    private void printElements(XSElementDecl element, String absPath, int min, int max, int order, ArrayList<String> siblings, 
            boolean choice) throws Exception {
        String prefix = "mms";
        String sufix = "";
        if (element.getTargetNamespace().equals("urn:simml")) {
            prefix = "sml";
        }
        if (element.getTargetNamespace().equals("http://www.w3.org/1998/Math/MathML")) {
            prefix = "mt";
            sufix = "ML";
        }
        absPath += "/" + prefix + ":" + element.getName();
        if (element.getName().equals("simml") && simml == null) {
            simml = element;
        }
        
        NodeList nodes = Utils.find(doc, absPath);
        
        //Information from all the nodes
        for (int i = 0; i < nodes.getLength(); i++) {
            Element node = (Element) nodes.item(i);
            String value = "";
            if (node.hasChildNodes() && (node.getChildNodes().item(0).getNodeType() == Node.TEXT_NODE 
                    || node.getChildNodes().item(0).getNodeType() == Node.CDATA_SECTION_NODE)) {
                value = node.getChildNodes().item(0).getNodeValue();    
            }
            printElement(element, absPath + sufix + "[" + (i + 1) + "]", i, value, min, max, order, choice, siblings);
        }
        //Empty node with Cardinality one or more must be have one empty element 
        if (nodes.getLength() == 0 && min == 1 && !choice) {
            printElement(element, absPath + sufix + "[1]", 0, "", min, max, order, choice, siblings);
        }
    }

    /**
     * Parse the element which may be an single data element.
     * @param element       The element
     * @param absPath       The xpath 
     * @param inputValue    The value of the element data
     * @param min           The min cardinality
     * @param max           The max cardinality
     * @param order         The element order
     * @param siblings      Siblings
     * @param choice        If the element is in a choice
     * @throws Exception    Exception
     */
    private void printElement(XSElementDecl element, String absPath, int repetition, String inputValue, 
            int min, int max, int order, boolean choice, ArrayList<String> siblings) throws Exception {
        //Optional element or repeated one
        if (choice || min == 0 || repetition > 0) {
            if (!emptyComplex) {
                SikuliCommons.collapseNode(screen);
            }
            emptyComplex = false;
            SikuliCommons.openContextualMenu(screen);
            SikuliCommons.selectOption(screen, siblings.indexOf(element.getName()) + 1);
            if (repetition + 1 == max) {
                siblings.remove(element.getName());
            }
        }
        //Existing element
        else {
            SikuliCommons.moveTreeDown(screen, 1);
        }
        
        //Pure type for visualization from annotation
        XSAnnotation annotation = element.getAnnotation();
        boolean uneditable = false;
        if (annotation != null) {
            String[] annotations = (String[]) annotation.getAnnotation();
            for (int i = 0; i < annotations.length; i++) {
                if (annotations[i].equals("uneditable")) {
                    uneditable = true;
                }
            }
        }
        if (!uneditable) {
            //Special case Math
            if (element.getName().equals("math")) {
               // SikuliCommons.modifyValue(screen, escapeSpecial(inputValue));
                SikuliCommons.modifyValue(screen, (inputValue));
            }
            else {   
                //Special case Simml
                if (element.getName().equals("simml")) {
                    
                    if (Utils.find(doc, absPath).getLength() > 0) {
                        Element root = (Element) Utils.find(doc, absPath).item(0);
                        printChildrenSimML(root);
                    }
                }
                else {
                    if (element.getType().isComplexType()) {
                        SikuliCommons.expandNode(screen);
                        printComplexType(element.getType().asComplexType(), element.getType().asComplexType().getAttributeUses(), 
                                absPath, order, siblings, false);
                    }
                    else {
                        //UUID type, open the document manager and select the model
                        if (element.getType().getName().equals("UUID")) {
                            SikuliCommons.modifyUUIDIntegration(screen);
                            uuids.add(inputValue);
                        }
                        else {
                            //Input is a UUID value, select from list
                            if (uuids.contains(inputValue)) {
                                SikuliCommons.modifyValueSelect(screen, uuids.indexOf(inputValue) + 1);
                            }
                            else {
                                //Normal cases
                                if (inputValue != "" && !element.getType().getName().equals("date")) {
                                    SikuliCommons.modifyValue(screen, (inputValue));
                                   // SikuliCommons.modifyValue(screen, escapeSpecial(inputValue));
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    /**
     * Checks id a particle list is empty.
     * @param list      The particle list.
     * @return          True if empty
     */
    private boolean isEmtyComplex(XSParticle[] list) {
        for (int i = 0; i < list.length; i++) {
            XSParticle particle = list[i];
            int min = particle.getMinOccurs();
            if (min > 0) {
                return false;
            }
        }
        return true;
    }
   
    /**
     * Returns the list of siblings of a node.
     * @param nodeName  The node name
     * @return          The list of siblings
     */
    private ArrayList<String> getSiblingsSimml(String nodeName) {
        XSModelGroup modelGroup = getModelGroup(simml, nodeName);
                
        if (modelGroup == null) {
            return new ArrayList<String>();
        }
        
        return getSiblings(modelGroup);
    }
        
    /**
     * Gives the model group of a node provided.
     * @param elem          The current element (is recursive)
     * @param nodeName      The name of the element to find
     * @return              The group
     */
    private XSModelGroup getModelGroup(XSElementDecl elem, String nodeName) {
        if (elem.getType().isComplexType()) {
            return printComplexType(elem.getType().asComplexType(), nodeName, elem.getName().equals(nodeName));
        }
        else {
            return null;
        }
    }
    
    /**
     * Gives the model group of a node provided.
     * @param complexType   The complex type
     * @param nodeName      The name of the element to find
     * @param found         True if already found
     * @return              The group found
     */
    private XSModelGroup printComplexType(XSComplexType complexType, String nodeName, boolean found) {
        XSParticle particle = complexType.getContentType().asParticle();
        if (particle != null) {
            return printParticle(particle, nodeName, found);
        }
        return null;
    }
   
    /**
     * Gives the model group of a node provided.
     * @param particle      The particle
     * @param nodeName      The name of the element to find
     * @param found         True if already found
     * @return              The group found
     */
    private XSModelGroup printParticle(XSParticle particle, String nodeName, boolean found) {
        XSTerm term = particle.getTerm();
        if (term.isModelGroup()) {
            if (found) {
                return term.asModelGroup();
            }
            else {
                return printGroup(term.asModelGroup(), nodeName, found);
            }
        }
        else if (term.isModelGroupDecl()) {
            return printGroupDecl(term.asModelGroupDecl(), nodeName, found);
        }
        else if (term.isElementDecl()) {
            return getModelGroup(term.asElementDecl(), nodeName);
        }
        return null;
    }
    
    /**
     * Gives the model group of a node provided.
     * @param modelGroup    The model group
     * @param nodeName      The name of the element to find
     * @param found         True if already found
     * @return              The group found
     */
    private XSModelGroup printGroup(XSModelGroup modelGroup, String nodeName, boolean found) {
        XSParticle[] list = modelGroup.getChildren();
        for (int i = 0; i < list.length; i++) {
            XSParticle particle = list[i];
            XSModelGroup result = printParticle(particle, nodeName, found);
            if (result != null) {
                return result;
            }
        }
        return null;
    }
    
    /**
     * Gives the model group of a node provided.
     * @param modelGroupDecl    The model group
     * @param nodeName          The name of the element to find
     * @param found             True if already found
     * @return                  The group found
     */
    private XSModelGroup printGroupDecl(XSModelGroupDecl modelGroupDecl, String nodeName, boolean found) {
        if (found) {
            return modelGroupDecl.getModelGroup();
        }
        else {
            return printGroup(modelGroupDecl.getModelGroup(), nodeName, found);
        }
    }
    
    /**
     * Get the siblings of a model group.
     * @param modelGroup    The model group
     * @return              The list of siblings
     */
    private ArrayList<String> getSiblings(XSModelGroup modelGroup) {
        XSParticle[] list = modelGroup.getChildren();
        boolean choice = modelGroup.getCompositor().toString().equals("choice");
        ArrayList<String> siblings = new ArrayList<String>();
        for (int i = 0; i < list.length; i++) {
            XSParticle particle = list[i];
            int min = particle.getMinOccurs();
            int max = particle.getMaxOccurs();
            if (choice || min == 0 || (max > 1 || max == -1)) {
                siblings.addAll(getParticleName(particle));
            }
        }
        Collections.sort(siblings);
        
        return siblings;
    }
    
    /**
     * Get the particle name.
     * @param particle  The particle
     * @return          The name
     */
    private ArrayList<String> getParticleName(XSParticle particle) {
        ArrayList<String> siblings = new ArrayList<String>();
        XSTerm term = particle.getTerm();
        if (term.isModelGroup()) {
            siblings.addAll(getGroupName(term.asModelGroup()));
        }
        else if (term.isModelGroupDecl()) {
            siblings.addAll(getGroupDeclName(term.asModelGroupDecl()));
        }
        else if (term.isElementDecl()) {
            siblings.add(term.asElementDecl().getName());
        }
        return siblings;
    }
    
    /**
     * Gives the group name of a model group.
     * @param modelGroup    The model group
     * @return              The group name
     */
    private ArrayList<String> getGroupName(XSModelGroup modelGroup) {
        ArrayList<String> siblings = new ArrayList<String>();
        XSParticle[] list = modelGroup.getChildren();
        for (int i = 0; i < list.length; i++) {
            XSParticle particle = list[i];
            siblings.addAll(getParticleName(particle));
        }
        return siblings;
    }
    
    /**
     * Get the group declaration name.
     * @param modelGroupDecl    The group declaration
     * @return                  The group declaration name
     */
    private ArrayList<String> getGroupDeclName(XSModelGroupDecl modelGroupDecl) {
        return getGroupName(modelGroupDecl.getModelGroup());
    }
    
    /**
     * Replaces the special non-Ascii characters with their unicode representation.
     * @param in    input string
     * @return      output string
     */
    /*private String escapeSpecial(String in) {
        StringBuilder out = new StringBuilder(in.length());
        for (int i = 0; i < in.length(); i++) {
            char charAt = in.charAt(i);
            if (charAt > 127) {
                out.append("\\u").append(StringUtils.leftPad(Integer.toHexString(charAt).toUpperCase(), 4, '0'));
            } 
            else {
                out.append(charAt);
                //Escape special character \
                if (charAt == '\\') {
                    out.append(charAt);
                }
            }
        }
        return out.toString();
    }*/
    
    /**
     * Parse the simML elements who need special treatment.
     * @param elem      The element
     * @throws InterruptedException 
     */
    private void printChildrenSimML(Element elem) throws InterruptedException {
        NodeList nodes = elem.getChildNodes();
        
        ArrayList<String> siblings = getSiblingsSimml(elem.getLocalName());
        //Information from all the nodes
        for (int i = 0; i < nodes.getLength(); i++) {
            Element node = (Element) nodes.item(i);
            String value = "";
            if (node.hasChildNodes() && (node.getChildNodes().item(0).getNodeType() == Node.TEXT_NODE 
                    || node.getChildNodes().item(0).getNodeType() == Node.CDATA_SECTION_NODE)) {
                value = node.getChildNodes().item(0).getNodeValue();    
            }
            
            printSimML(node, value, siblings);
        }
    }
    
    /**
     * Parse a simML element.
     * @param elem          The element
     * @param inputValue    The value of the element
     * @param siblings      Siblings
     * @throws InterruptedException Exception
     */
    private void printSimML(Element elem, String inputValue, ArrayList<String> siblings) throws InterruptedException {
        SikuliCommons.openContextualMenu(screen);
        SikuliCommons.selectOption(screen, siblings.indexOf(elem.getLocalName()) + 1);

        //Special case Math
        if (elem.getLocalName().equals("math")) {
          //  SikuliCommons.modifyValue(screen, escapeSpecial(inputValue));
            SikuliCommons.modifyValue(screen, (inputValue));
        }
        else {  
            if (!inputValue.equals("")) {
                SikuliCommons.modifyValue(screen, (inputValue));
             //   SikuliCommons.modifyValue(screen, escapeSpecial(inputValue));
            }
            else {
                if (!elem.getLocalName().equals("math") && inputValue.equals("")) {
                    printChildrenSimML(elem);
                }
            }
        }
        //The first to move to parent
        SikuliCommons.collapseNode(screen);
        //The second to actually collapse
        SikuliCommons.collapseNode(screen);
    }
    
    /**
     * Parses the xsd with an xml data document to json.
     * @throws Exception Exception
     */
    public void execute() throws Exception {
        String absPath = "/mms:" + rootElement;
        String content = preprocess(xml);
        doc = Utils.stringToDom(content);
        Utils.mathToAscii(doc);
        ArrayList<String> siblings = new ArrayList<String>();
        
        //Opening editor
        screen.click("add.png");
        SikuliCommons.selectOption(screen, Utils.getMenuOption(CollectionType.valueOf(rootElement)));
        screen.wait("simflownyEditor.png", 60);
        
        printComplexType(parser.getResult().getSchema(1).getElementDecl(rootElement).getType().asComplexType(), 
                parser.getResult().getSchema(1).getElementDecl(rootElement).getType().asComplexType().getAttributeUses(), 
                absPath, 0, siblings, false);

        //Closing editor
        //Worst case 15 seconds to save
        Thread.sleep(15000);
        screen.type("w", Key.CTRL);
    }

    /**
     * Preprocess the xml document with the following procedures.
     * 1 - Remove whitespaces
     * 3 - Add sml namespace to the root element
     * @param problem   The document
     * @return          The document preprocessed
     */
    static String preprocess(String problem) {
        
        //1 - Remove whitespaces
        String tmp = problem.replaceAll(">(\\s+|\\n|\\r\\n)<", "><").replaceAll("\n\r", "").replaceAll("\n", "");
        tmp = tmp.replaceAll("(\\s+|\\n|\\r\\n)", " ");
        tmp = tmp.replaceAll("\\s+>", ">");
  
        //3 - Add sml namespace to the root element
        String patternString = "(<\\w+:\\w+(\\s+(\\w+:)?\\w+=\"[\\w:/.]+\"?)*)>";
        Pattern pattern = Pattern.compile(patternString);
        StringBuffer sb = new StringBuffer();
        Matcher matcher = pattern.matcher(tmp);
        if (matcher.find() && !matcher.group(1).contains("simml")) {
            String newString = matcher.group(1) + " xmlns:sml=\"urn:simml\">";
            matcher.appendReplacement(sb, newString);
        }
        
        matcher.appendTail(sb);

        return sb.toString();
    }
}
