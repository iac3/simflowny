/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.guiTesting;

import com.sun.xml.xsom.XSFacet;
import com.sun.xml.xsom.XSParticle;
import com.sun.xml.xsom.XSRestrictionSimpleType;
import com.sun.xml.xsom.XSSimpleType;
import com.sun.xml.xsom.XSTerm;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.util.Iterator;
import java.util.Vector;

import javax.xml.namespace.NamespaceContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;


/**
 * This auxiliary class collects some common XSOM procedures.
 * @author bminano
 *
 */
public final class Utils {
    static XPath xpath;
    static XPathFactory factory = XPathFactory.newInstance();
    static TransformerFactory tFactory;
    static final String XSL = "common" + File.separator + "XSLTmathmlToasciiML" + File.separator + "mmlascii.xsl";
    static Transformer transformer = null;
    static final int IND = 4;
    public static final String MMSURI = "urn:mathms";
    /**
     * Constructor.
     * @throws TransformerConfigurationException 
     */
    public Utils() throws TransformerConfigurationException {
        factory = new net.sf.saxon.xpath.XPathFactoryImpl();
        xpath = factory.newXPath();
        tFactory = new net.sf.saxon.TransformerFactoryImpl();
        transformer = tFactory.newTransformer(new StreamSource(XSL));
        NamespaceContext ctx = new NamespaceContext() {
            public String getNamespaceURI(String prefix) {
                String uri;
                if (prefix.equals("sml")) {
                    uri = "urn:simml";
                }
                else {
                    if (prefix.equals("mms")) {
                        uri = "urn:mathms";  
                    }    
                    else {
                        if (prefix.equals("mt")) {
                            uri = "http://www.w3.org/1998/Math/MathML";
                        }
                        else {
                            if (prefix.equals("scm")) {
                                uri = "http://purl.oclc.org/dsdl/schematron";
                            }
                            else {
                                uri = null;
                            }
                        }
                    }
                }
                return uri;
            }
            // Dummy implementation - not used!
            public Iterator < ? > getPrefixes(String val) {
                return null;
            }
            // Dummy implemenation - not used!
            public String getPrefix(String uri) {
                return null;
            }
        };
    
        xpath.setNamespaceContext(ctx);
    }
    
    /**
     * Removes the namespaces from an XML string.
     * @param xmlData   The XML string
     * @return          The XML string without namespaces
     */
    public static String removeAllXmlNamespace(String xmlData) {
        String xmlnsPattern = "\\sxmlns[^\"]+\"[^\"]+\"";
        return xmlData.replaceAll(xmlnsPattern, "");
    }
    
    
    /** 
     * It returns the struct in json for the model/problem specified.
     * @param xsdName       The schema to obtain the struct from
     * @return              The json struct file
     * @throws Exception    Exception
     */
    public String getStruct(String xsdName) throws Exception {
        String schemaPath = "common" + File.separator + "XSD" + File.separator + xsdName + ".xsd";
        String schematronPath = "common" + File.separator + "schematron" + File.separator + xsdName + ".sch";
        
        XSDTagStructureGenerator tagStruct = new XSDTagStructureGenerator();
        tagStruct.addStruct(schemaPath, schematronPath, xsdName);
        return tagStruct.getStruct();
    }
    
    /**
     * XSD simple type restriction class. 
     * @author bminano
     *
     */
    public static class SimpleTypeRestriction {
        public String[] enumeration = null;
        public String   maxValue    = null;
        public String   minValue    = null;
        public String   length      = null;
        public String   maxLength   = null;
        public String   minLength   = null;
        public String[] pattern     = null;
        public String   totalDigits = null;

        /**
         * Returns the restriction.
         * @return  The restriction.
         */
        public String toString() {
            String enumValues = "";
            if (enumeration != null) {
                for (String val : enumeration) {
                    enumValues += val + ", ";
                }
                enumValues = enumValues.substring(0, enumValues.lastIndexOf(','));
            }
            
            String patternValues = "";
            if (pattern != null) {
                for (String val : pattern) {
                    patternValues += "(" + val + ")|";
                }
                patternValues = patternValues.substring(0, patternValues.lastIndexOf('|'));
            }
            String retval = "";
            //retval += maxValue    == null ? "" : "[MaxValue  = "   + maxValue      + "]";
            //retval += minValue    == null ? "" : "[MinValue  = "   + minValue      + "]";
            //retval += maxLength   == null ? "" : "[MaxLength = "   + maxLength     + "]";
            //retval += minLength   == null ? "" : "[MinLength = "   + minLength     + "]";
            //retval += pattern     == null ? "" : "[Pattern(s) = "  + patternValues + "]";
            //retval += totalDigits == null ? "" : "[TotalDigits = " + totalDigits   + "]";
            //retval += length      == null ? "" : "[Length = "      + length        + "]";
            
            if (enumeration != null) {
                retval += "[Values = " + enumValues + "]";
            }

            return retval;
        }
    }
    
    /**
     * Gets the simple restrictions for a XSD simple type.
     * @param xsSimpleType          The type
     * @param simpleTypeRestriction The restriction
     */
    public static void initRestrictions(XSSimpleType xsSimpleType, SimpleTypeRestriction simpleTypeRestriction)
    {
        XSRestrictionSimpleType restriction = xsSimpleType.asRestriction();
        if (restriction != null) {
            Vector<String> enumeration = new Vector<String>();
            Vector<String> pattern     = new Vector<String>();

            for (XSFacet facet : restriction.getDeclaredFacets()) {
                if (facet.getName().equals(XSFacet.FACET_ENUMERATION)) {
                    enumeration.add(facet.getValue().value);
                }
                if (facet.getName().equals(XSFacet.FACET_MAXINCLUSIVE)) {
                    simpleTypeRestriction.maxValue = facet.getValue().value;
                }
                if (facet.getName().equals(XSFacet.FACET_MININCLUSIVE)) {
                    simpleTypeRestriction.minValue = facet.getValue().value;
                }
                if (facet.getName().equals(XSFacet.FACET_MAXEXCLUSIVE)) {
                    simpleTypeRestriction.maxValue = String.valueOf(Integer.parseInt(facet.getValue().value) - 1);
                }
                if (facet.getName().equals(XSFacet.FACET_MINEXCLUSIVE)) {
                    simpleTypeRestriction.minValue = String.valueOf(Integer.parseInt(facet.getValue().value) + 1);
                }
                if (facet.getName().equals(XSFacet.FACET_LENGTH)) {
                    simpleTypeRestriction.length = facet.getValue().value;
                }
                if (facet.getName().equals(XSFacet.FACET_MAXLENGTH)) {
                    simpleTypeRestriction.maxLength = facet.getValue().value;
                }
                if (facet.getName().equals(XSFacet.FACET_MINLENGTH)) {
                    simpleTypeRestriction.minLength = facet.getValue().value;
                }
                if (facet.getName().equals(XSFacet.FACET_PATTERN)) {
                    pattern.add(facet.getValue().value);
                }
                if (facet.getName().equals(XSFacet.FACET_TOTALDIGITS)) {
                    simpleTypeRestriction.totalDigits = facet.getValue().value;
                }
            }
            if (enumeration.size() > 0) {
                simpleTypeRestriction.enumeration = enumeration.toArray(new String[] {});
            }
            if (pattern.size() > 0) {
                simpleTypeRestriction.pattern = pattern.toArray(new String[] {});
            }
        }
    }
    
    /**
     * Checks when a particle is ordered.
     * @param particle  The particle
     * @return          True not ordered
     */
    public static boolean isNotOrdered(XSParticle particle) {
        if (particle != null) {
            XSTerm term = particle.getTerm();
            if (term.isModelGroup()) {
                if (term.asModelGroup().getCompositor().toString().equals("choice")) {
                    return true;
                }
                XSParticle[] list = term.asModelGroup().getChildren();
                XSParticle particleN = list[0];
                return isNotOrdered(particleN); 
            }
            else {
                if (term.isModelGroupDecl()) {  
                    XSParticle[] list = term.asModelGroupDecl().getModelGroup().getChildren();
                    if (term.asModelGroupDecl().getModelGroup().getCompositor().toString().equals("choice")) {
                        return true;
                    }
                    XSParticle particleN = list[0];
                    return isNotOrdered(particleN); 
                }
            }
        }
        return false;
    }
    
    /**
     * Transform a schematron xpath assert to jsonPath format.
     * Or operator is not available in xpath, so it is implemented as different jsonPaths
     * @param xpath     The xpath
     * @return          The jsonPaths
     */
    public static String xpathToJsonpath(String xpath) {
        String jsonPath = "";
        String[] separated = xpath.split(" or ");
        for (int j = 0; j < separated.length; j++) {
            String xpathInd = separated[j].trim();
            //Or operator not implemented, create different jsonPath
            String[] paths = xpathInd.substring(0, xpathInd.lastIndexOf(" =")).split("\\|");
            for (int i = 0; i < paths.length; i++) {
                if (paths[i].matches("'[A-Za-z0-9_]*'")) {
                    jsonPath = jsonPath + "$" + paths[i];
                }
                else {
                    String replacedXpath = paths[i].replaceAll("/", ".").replaceAll("mms:([a-zA-Z]*)", "children[?(@.tag == '$1')]");
                    jsonPath = jsonPath + "$" + replacedXpath + ".input";
                }
            }
        }
        return jsonPath;
    }
    
    /**
     * Parses a simple type element.
     * @param simpleType    The simple type object
     * @return              The type
     */
    public static String printSimpleType(XSSimpleType simpleType) {
        SimpleTypeRestriction restriction = new SimpleTypeRestriction();
        Utils.initRestrictions(simpleType, restriction);
        return restriction.toString();
    }
    
    /**
     * Gets the schematron content restriction of an element.
     * @param schematron        The schematron content
     * @param absPath           The path of current element
     * @return                  The restriction
     * @throws Exception        Exception
     */
    public static String schematronRestriction(Document schematron, String absPath) throws Exception {
        if (schematron != null) {
            String tmpPath = absPath.replaceAll("\\[[0-9]*\\]", "");
            NodeList rules = find(schematron, "//scm:rule[ ends-with('" + tmpPath + "', replace(@context, '//', '/'))]");
            for (int i = 0; i < rules.getLength(); i++) {
                NodeList asserts = find(rules.item(i), "./scm:assert");
                String restriction = asserts.item(0).getAttributes().getNamedItem("test").getTextContent();
                if (restriction.matches("((\\sor\\s)?(([|]?(\\/[\\w]*:[\\w]*)*[\\s]*)|('[A-Za-z0-9_]*'))*?=[\\s]*\\.)+")) {
                    String assertion = Utils.xpathToJsonpath(restriction);
                    return "[Content = '" + assertion + "']";
                }
            }
        }
        return "";
    }
    
    /**
     * Executes an xpath query in the document.
     * @param node          The document to find in
     * @param xpathQuery    The query
     * @return              A nodelist with the results
     * @throws Exception    Exception
     */
    public static NodeList find(Node node, String xpathQuery) throws Exception {
        XPathExpression expr = xpath.compile(xpathQuery);

        return (NodeList) expr.evaluate(node, XPathConstants.NODESET);
    }
    
    /**
     * Converts a string to Dom.
     * 
     * @param str               The string
     * @return                  The Dom document
     * @throws Exception    Exception
     */
    public static Document stringToDom(String str) throws Exception {
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        docFactory.setNamespaceAware(true);
        docFactory.setIgnoringElementContentWhitespace(true);
        docFactory.setIgnoringComments(true);
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
        InputStream is = new ByteArrayInputStream(str.getBytes("UTF-8"));
        Document result = docBuilder.parse(is);
        removeWhitespaceNodes(result.getDocumentElement());
        return result;
    }
    
    /**
     * Remove the white spaced nodes from a Dom element.
     * 
     * @param e The element to delete the whitespaces from
     */
    public static void removeWhitespaceNodes(Element e) {
        NodeList children = e.getChildNodes();
        for (int i = children.getLength() - 1; i >= 0; i--) {
            Node child = children.item(i);
            if (child instanceof Text && ((Text) child).getData().trim().length() == 0) {
                e.removeChild(child);
            }
            else if (child instanceof Element) {
                removeWhitespaceNodes((Element) child);
            }
        }
    }
    
    /**
     * Create an element in the Uri namespace.
     * @param doc           The document where to create the element
     * @param uri           The namespace uri
     * @param tagName       The tag name
     * @return              The element
     */
    public static Element createElement(Document doc, String uri, String tagName) {
        Element element;
        if (uri != null) {
            element = doc.createElementNS(uri, tagName);
            if (uri.equals("http://www.w3.org/1998/Math/MathML")) {
                element.setPrefix("mt");
            }
            if (uri.equals("urn:mathms")) {
                element.setPrefix("mms");
            }
            if (uri.equals("urn:simml")) {
                element.setPrefix("sml");
            }
        }
        else {
            element = doc.createElement(tagName); 
        }
        return element;
    }
    
    /**
     * Reads a file.
     * @param path          The path of the file
     * @return              The string
     * @throws Exception    Exception
     */
    public static String readFile(String path) throws Exception {
        FileInputStream file = new FileInputStream(path);
        byte[] b = new byte[file.available()];
        file.read(b);
        file.close();
        return new String(b);
    }
    
    /**
     * Converts a Dom node to string.
     * 
     * @param node              The dom node
     * @return                  The String
     * @throws Exception    Exception
     */
    public static String domToString(Node node) throws Exception {
        Transformer trans = TransformerFactory.newInstance().newTransformer();
        trans.setOutputProperty(OutputKeys.INDENT, "yes");
        StreamResult result = new StreamResult(new ByteArrayOutputStream());
        DOMSource source = new DOMSource(node);
        trans.transform(source, result);
        return new String(((ByteArrayOutputStream) result.getOutputStream()).toByteArray(), "UTF-8");
    }
    
    /**
     * Converts a document manager tree into json.
     * @param tree  The tree
     * @return      Json object from the tree
     * @throws Exception    Exception
     */
    public static String docManagerTreeToJSON(Node tree) throws Exception {
        JSONObject jsonTree = new JSONObject(); 
        
        jsonTree.put("text", "/");
        NodeList children = tree.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            jsonTree.accumulate("children", getJSONCollection(children.item(i)));
        }
        
        return jsonTree.toString();
    }
    
    /**
     * Recursive generation of json object from a collection xml element.
     * @param collection    The collection element
     * @return              The json object
     * @throws Exception    Exception
     */
    private static JSONObject getJSONCollection(Node collection) throws Exception {
        JSONObject child = new JSONObject(); 
        child.put("text", collection.getFirstChild().getTextContent());
        child.put("expanded", true);
        if (collection.getChildNodes().getLength() > 1) {
            NodeList children = collection.getLastChild().getChildNodes();
            for (int i = 0; i < children.getLength(); i++) {
                child.accumulate("children", getJSONCollection(children.item(i)));
            }
        }
        return child;
    }
    
    /**
     * Converts a document manager list into json.
     * @param list  The list
     * @return      Json object from the tree
     * @throws Exception    Exception
     */
    public static String docManagerListToJSON(Node list) throws Exception {
        JSONObject jsonTree = new JSONObject(); 
        
        jsonTree.put("success", true);
        NodeList children = list.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Element childElem = (Element) children.item(i);
            JSONObject child = new JSONObject(); 
            child.put("dataindex", false);
            child.put("type", childElem.getElementsByTagNameNS(MMSURI, "documentType").item(0).getTextContent());
            child.put("name", childElem.getElementsByTagNameNS(MMSURI, "name").item(0).getTextContent());
            child.put("author", childElem.getElementsByTagNameNS(MMSURI, "author").item(0).getTextContent());
            child.put("id", childElem.getElementsByTagNameNS(MMSURI, "id").item(0).getTextContent());
            child.put("version", childElem.getElementsByTagNameNS(MMSURI, "version").item(0).getTextContent());
            child.put("date", childElem.getElementsByTagNameNS(MMSURI, "date").item(0).getTextContent());
            child.put("Description", childElem.getElementsByTagNameNS(MMSURI, "description").item(0).getTextContent());
            jsonTree.accumulate("results", child);
        }
        
        return jsonTree.toString();
    }
    
    /**
     * Reads a file.
     * @param is    The input strem
     * @return      The file
     */
    public static String getStringFromInputStream(InputStream is) { 
        BufferedReader br = null;
        StringBuilder sb = new StringBuilder();
 
        String line;
        try {
            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line + "\n");
            }
        } 
        catch (IOException e) {
            e.printStackTrace();
        } 
        finally {
            if (br != null) {
                try {
                    br.close();
                } 
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return sb.toString();
    }
    
    /**
     * Copies a file to a new location.
     * @param fis           The file input stream
     * @param destFile      The destination
     * @throws Exception    Exception
     */
    public static void copyFile(FileInputStream fis, String destFile) throws Exception {
        FileOutputStream fos = null;

        fos = new FileOutputStream(destFile);
        
        byte[] buffer = new byte[1024];
        int noOfBytes = 0;

        // read bytes from source file and write to destination file
        while ((noOfBytes = fis.read(buffer)) != -1) {
            fos.write(buffer, 0, noOfBytes);
        }
    }
    
    /**
     * Returns the index of the menu option depending on the schema.
     * @param schemaName    The schema name
     * @return              The index
     * @throws Exception    Exception
     */
    public static int getMenuOption(CollectionType schemaName) throws Exception {
        switch (schemaName) {
            case physicalModel:
                return 1;
            case simulationProblem:
                return 2;
            case discretizationSchema:
                return 3;
            case discretizationPolicy:
                return 4;
            case agentBasedModelGraph:
                return 5;
            case agentBasedModelSpatialDomain:
                return 6;
            case agentBasedProblemGraph:
                return 7;
            case agentBasedProblemSpatialDomain:
                return 8;
        }
        throw new Exception("Schema name not allowed");    
    }
    
    /**
     * Replaces the mathML with ASCII in the whole document.
     * @param doc           The document
     * @throws Exception    Exception
     */
    public static void mathToAscii(Document doc) throws Exception {
        NodeList mathExpressions = find(doc, "//mt:math");
        for (int i = mathExpressions.getLength() - 1; i >= 0; i--) { 
            Element math = (Element) mathExpressions.item(i);
            Element parent = (Element) math.getParentNode();
            Element newMath = (Element) math.cloneNode(false);
            newMath.setTextContent(replaceMath(math));
            parent.replaceChild(newMath, math);
        }
    }
    
    /**
     * Replaces a MathML expression with its ASCII expression using an xslt. 
     * @param e             The element to replace
     * @return              The ASCII value
     * @throws Exception    Exception
     */
    public static String replaceMath(Element e) throws Exception {
        StringWriter output = new StringWriter();
        DOMSource domSource = new DOMSource(e);
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        transformer.transform(domSource, new StreamResult(output));
        return stringToDom(output.toString()).getDocumentElement().getTextContent();
    }
}
