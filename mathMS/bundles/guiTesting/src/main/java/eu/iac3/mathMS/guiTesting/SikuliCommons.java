/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.guiTesting;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;

import org.apache.commons.io.FileUtils;
import org.apache.xml.serialize.OutputFormat;
import org.apache.xml.serialize.XMLSerializer;
import org.sikuli.basics.Settings;
import org.sikuli.script.FindFailed;
import org.sikuli.script.IRobot;
import org.sikuli.script.Key;
import org.sikuli.script.KeyModifier;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/**
 * Sikuli shortcuts to common Simflowny operations.
 * @author bminano
 *
 */
public final class SikuliCommons {
    
    private static final int SHORT = 200;
    private static final int HALFSECOND = 500;
    private static final int ALMOSTSECOND = 700;
    private static final int SECOND = 1000;
    private static final int INDENT = 4;
    private static final int HALFSECONDDELAY = 500;
    
    
    /**
     * Private constructor.
     */
    private SikuliCommons() {
        
    }

    /**
     * Moves downwards in a tree.
     * @param s     Screen
     * @param reps  Number of displacements
     */
    public static void moveTreeDown(Screen s, int reps) {
        for (int i = 0; i < reps; i++) {
            slowType(s, Key.DOWN);
        }
    }
    
    /**
     * Moves upwards in a tree.
     * @param s     Screen
     * @param reps  Number of displacements
     */
    public static void moveTreeUp(Screen s, int reps) {
        for (int i = 0; i < reps; i++) {
            slowType(s, Key.UP);
        }
    }
    
    /**
     * Collapses an extended element.
     * @param s                         Screen
     * @throws InterruptedException     Exception
     */
    public static void collapseNode(Screen s) throws InterruptedException {
        Thread.sleep(SHORT);
        slowType(s, Key.LEFT);
        Thread.sleep(SHORT);
    }
    
    /**
     * Expands the selected node if not already expanded.
     * @param s                         Screen
     * @throws InterruptedException     Exception
     */
    public static void expandNode(Screen s) throws InterruptedException {
        Thread.sleep(SHORT);
        if (s.exists(new Pattern("collapsed.png").similar((float) 0.9)) != null) {
            slowType(s, Key.RIGHT);
            Thread.sleep(ALMOSTSECOND);
        }
    }

    /**
     * Modify the value of a node.
     * @param value                     The new value
     * @param s                         Screen
     * @throws InterruptedException     Exception
     */
    public static void modifyValue(Screen s, String value) throws InterruptedException {
        slowType(s, Key.INSERT);
        Thread.sleep(HALFSECOND);
        s.paste(value);
        Thread.sleep(HALFSECOND);
        slowType(s, Key.ENTER);
        Thread.sleep(HALFSECOND);
    }
    
    /**
     * Modify the value of a list.
     * @param s         Screen
     * @param reps      The index of the new value
     */
    public static void modifyValueSelect(Screen s, int reps) {
        slowType(s, Key.INSERT);
        for (int i = 0; i < reps; i++) {
            slowType(s, Key.DOWN);
        }
        slowType(s, Key.ENTER);
    }

    /**
     * Modifies the document id from a node.
     * @param tree                      Index of tree collection
     * @param document                  Index of document inside the collection
     * @param s                         Screen
     * @throws InterruptedException     Exception
     * @throws FindFailed               Exception
     */
    public static void modifyUUID(Screen s, int tree, int document) throws InterruptedException, FindFailed {
        slowType(s, Key.INSERT);
        s.wait("uuidPopup.png");
        Thread.sleep(SECOND);
        for (int i = 0; i < tree; i++) {
            slowType(s, Key.DOWN);
        }
        Thread.sleep(HALFSECOND);
        slowType(s, Key.ENTER);
        slowType(s, Key.TAB);
        Thread.sleep(HALFSECOND);
        for (int i = 0; i < document; i++) {
            slowType(s, Key.DOWN);
        }
        slowType(s, Key.ENTER);
        Thread.sleep(HALFSECOND);
    }

    /**
     * Modifies the document id from a node in the integration test.
     * @param s                         Screen
     * @throws InterruptedException     Exception
     * @throws FindFailed               Exception
     */
    //Set any document id from the root folder, which is the place for the models in the integration test
    //When comparing, the id will be removed, so it doesn't matter if the selected model is correct or not
    public static void modifyUUIDIntegration(Screen s) throws FindFailed, InterruptedException {
        slowType(s, Key.INSERT);
        s.wait("uuidPopup.png");
        Thread.sleep(SECOND);
        slowType(s, Key.ENTER); 
        Thread.sleep(HALFSECOND);
        slowType(s, Key.TAB);
        Thread.sleep(HALFSECOND);
        s.doubleClick("Model.png");
        Thread.sleep(HALFSECOND);
    }

    /**
     * Returns the list of documents of a collection.
     * @param s     Screen
     */
    public static void getDocumentList(Screen s) {
        slowType(s, Key.ENTER);
    }

    /**
     * Selects an option in the contextual menu.
     * @param s                     Screen
     * @param num                   The option index
     * @throws InterruptedException Exception
     */
    public static void selectOption(Screen s, int num) throws InterruptedException {
        Thread.sleep(SHORT);
        for (int i = 0; i < num; i++) {
            slowType(s, Key.DOWN);
        }
        slowType(s, Key.ENTER);
        Thread.sleep(HALFSECOND);
    }

    /**
     * Opens a contextual menu.
     * @param s     Screen
     * @throws InterruptedException 
     */
    public static void openContextualMenu(Screen s) throws InterruptedException {
        s.type(Key.F7);
    }

    /**
     * Types slowly.
     * @param s     Screen
     * @param val   The text to type
     */
    public static void slowType(Screen s, String val) {
        Settings.TypeDelay = HALFSECONDDELAY;
        s.type(val);
    }

    /**
     * Compares two files.
     * @param fileA                         File A
     * @param fileB                         File B
     * @return                              True if equals
     * @throws IOException                  Exception
     * @throws ParserConfigurationException Exception
     * @throws SAXException                 Exception
     */
    public static boolean compareFiles(String fileA, String fileB) throws IOException, ParserConfigurationException, SAXException {
        File file1 = new File(fileA);
        File file2 = new File(fileB);
        boolean isTwoEqual = FileUtils.contentEquals(file1, file2);
        return isTwoEqual;
    }
    
    /**
     * Compares two XML files.
     * @param fileA                         File A
     * @param fileB                         File B
     * @return                              True if equals
     * @throws IOException                  Exception
     * @throws ParserConfigurationException Exception
     * @throws SAXException                 Exception
     */
    public static boolean compareFilesXML(String fileA, String fileB) throws IOException, ParserConfigurationException, SAXException {
        String file1 = readFile(fileA);
        String file2 = readFile(fileB);
        return indent(file1).equals(indent(file2));
    }

    /**
     * Compares file size.
     * @param fileA                         File A
     * @param fileB                         File B
     * @return                              True if equal size
     */
    public static boolean compareFileSize(String fileA, String fileB) {
        long sizeinfoA = new File(fileA).length();
        long sizeinfoB = new File(fileB).length();
        return sizeinfoA == sizeinfoB;
    }

    /**
     * Extracts a zip file.
     * @param file          The zip file
     * @param folder        The destination folder
     * @throws ZipException Exception
     */
    public static void extractZip(String file, String folder) throws ZipException {
        if (!new File(folder).exists()) {
            (new File(folder)).mkdirs();
        }
        ZipFile zipFile = new ZipFile(file);
        zipFile.extractAll(folder);
    }

    /**
     * Removes irrelevant information before the comparisons.
     * @param file              File to filter.
     * @throws IOException      Exception
     */
    public static void cleanDocumentRefs(String file) throws IOException {
        List<String> lines = new ArrayList<String>();
        String line = null;
        File f1 = new File(file);
        FileReader fr = new FileReader(f1);
        BufferedReader br = new BufferedReader(fr);
        while ((line = br.readLine()) != null) {
            if (line.contains("<mms:id>")) {
                line = line.replaceAll("<mms:id>[0-9A-Za-z\\-]*<\\/mms:id>", "<mms:id/>");
            }
            if (line.contains("<mms:modelId>")) {
                line = line.replaceAll("<mms:modelId>[0-9A-Za-z\\-]*<\\/mms:modelId>", "<mms:modelId/>");
            }
            if (line.contains("<mms:date>")) {
                line = line.replaceAll("<mms:date>[0-9\\-]*<\\/mms:date>", "<mms:date/>");
            }
            lines.add(line + "\n");
        }
        fr.close();
        br.close();

        FileWriter fw = new FileWriter(f1);
        BufferedWriter out = new BufferedWriter(fw);
        for (String s: lines) {
            out.write(s);
        }
        out.flush();
        out.close();
    }
    
    /**
     * Reads a file to a String.
     * @param path          The file path
     * @return              The string content
     * @throws IOException  Exception
     */
    private static String readFile(String path) throws IOException {
        String line = null;
        File f1 = new File(path);
        FileReader fr = new FileReader(f1);
        BufferedReader br = new BufferedReader(fr);
        StringBuilder sb = new StringBuilder();
        while ((line = br.readLine()) != null) {
            sb.append(line);
        }
        fr.close();
        br.close();

        return sb.toString();
    }

    /**
     * Removes a folder and its contents.
     * @param folder        The folder
     * @throws IOException  Exception
     */
    public static void removeFolder(String folder) throws IOException {
        FileUtils.deleteDirectory(new File(folder));
    }
    
    /**
     * Implements the paste as Sikuli's paste does not work.
     * This is basically a copy of their source code.
     * @param s     Screen
     * @param text  The text to paste.
     */
    public static void paste(Screen s, String text) {
        //Copy text to clipboard
        StringSelection stringSelection = new StringSelection(text);
        Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
        clpbrd.setContents(stringSelection, null);
        
        //Paste
        int mod = Key.getHotkeyModifier();
        IRobot r = s.getRobot();
        r.keyDown(mod);
        r.keyDown(KeyEvent.VK_V);
        r.keyUp(KeyEvent.VK_V);
        r.keyUp(mod);
    }
    
    /**
     * A method for the format of the output.
     * 
     * @param document          The document to be formatted
     * @return                  The document formatted
     * @throws ParserConfigurationException 
     * @throws IOException 
     * @throws SAXException 
     */
    private static String indent(String document) throws ParserConfigurationException, SAXException, IOException  {
        DocumentBuilder docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        Document doc = docBuilder.parse(new ByteArrayInputStream(document.getBytes("UTF-8")));

        OutputFormat format = new OutputFormat(doc);
        format.setIndenting(true);
        format.setIndent(INDENT);
        Writer output = new StringWriter();
        XMLSerializer serializer = new XMLSerializer(output, format);
        serializer.serialize(doc);

        return output.toString();
    }
}
