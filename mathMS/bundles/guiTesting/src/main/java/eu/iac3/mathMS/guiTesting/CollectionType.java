/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.guiTesting;

/** 
 * SchemaType is an enum type used in the
 * {@link ModelManager} interface.
 *  lists the collections supported by the plugin
 * @author      bminano
 * @version     ${project.version}
 */
public enum CollectionType {
	physicalModel,
	simulationProblem,
	discretizationSchema,
	discretizationPolicy,
	agentBasedModelGraph,
	agentBasedModelSpatialDomain,
	agentBasedProblemGraph,
	agentBasedProblemSpatialDomain
};
