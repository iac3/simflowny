/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.guiTestingTest;

import eu.iac3.mathMS.guiTesting.SikuliCommons;
import eu.iac3.mathMS.guiTesting.SikuliTransformer;

import java.awt.Desktop;
import java.io.File;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.jar.Manifest;

import org.apache.commons.io.FileUtils;
import org.osgi.framework.Constants;
import org.sikuli.basics.Debug;
import org.sikuli.script.ImagePath;
import org.sikuli.script.Key;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;
import org.springframework.osgi.test.AbstractConfigurableBundleCreatorTests;
import org.springframework.osgi.test.platform.OsgiPlatform;
import org.springframework.osgi.test.platform.Platforms;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;



/** 
 * This class tests the GUI in a real environment using Sikuli.
 * IMPORTANT REMARK!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 * Sikuli must be already set up by sikulixsetup-1.1.0.jar script.
 * Version must be 201510051707, otherwise it won't work with the current sikulixapi bundle.
 * For some reason, version 201510061722 generates  sikulixapi version 201510051707,
 * so a manual rename in .Sikulix for folder SikulixLibs must be done to fit 201510051707 version.
 *  
 * @author      ----
 * @version     ----
 */
public class GuiTestingTest extends AbstractConfigurableBundleCreatorTests {

    static boolean initialized = false; 
    static boolean lastTest = false; 
    static String PORT = "8080";
    
    long startTime;
    
    //Delays used in the tests
    private static final int SHORT = 300;
    private static final int HALFSECOND = 500;
    private static final int SECONDANDHALF = 1500;
    private static final int SECOND = 1000;
    private static final int SAFESAVE = 15000;
    private static final int TWOSECONDS = 2000;
    private static final int FIVESECONDS = 5000;
    private static final float ACC098 = (float) 0.98;
    
    /**
     * This method is used to set Felix as the 
     * testing platform.
     * Spring-osgi-test
     * 
     * @return OsgiPlatform
     */
    protected String getPlatformName() {
        return Platforms.FELIX;
    }
    /**
     * This method is used to set the system parameters
     * to the testing platform.
     * Spring-osgi-test
     * 
     * @return OsgiPlatform
     */
    protected OsgiPlatform createPlatform() {
        System.setProperty("file.encoding", "UTF-8");
        System.setProperty("org.osgi.service.http.port", PORT);
        System.setProperty("derby.system.home", "target/db/Derby");
        System.setProperty("xindice.db.home", "target/db/xindiceDB");
        return super.createPlatform();
    } 
    /**
     * This method is used to set the boot 
     * delegation packages.
     * Spring-osgi-test
     * 
     * @return The boot delegation Packages
     */
    protected List<String> getBootDelegationPackages() { 
        List<String> defaults = new ArrayList<String>(); 
        defaults.add("java.*");
        return defaults; 
    } 
    /**
     * This method is used to set the manifest.
     * Spring-osgi-test
     * 
     * @return The manifest for the test bundle
     */
    protected Manifest getManifest() {
        // let the testing framework create/load the manifest
        Manifest mf = super.getManifest();
        mf.getMainAttributes().putValue(Constants.BUNDLE_REQUIREDEXECUTIONENVIRONMENT, "J2SE-1.5");
        mf.getMainAttributes().putValue(Constants.IMPORT_PACKAGE, ""
                + "javax.xml, javax.xml.transform, javax.xml.parsers, javax.xml.validation, javax.xml.transform.dom, "
        		+ "javax.xml.transform.stream,org.sikuli.basics,org.sikuli.script,"
        		+ "org.w3c.dom, javax.xml.namespace, javax.xml.xpath, org.apache.commons.io,"
        		+ "org.xml.sax,org.xml.sax.ext,org.apache.commons.lang3, net.lingala.zip4j.core,net.sf.saxon, " 
        		+ "net.sf.saxon.xpath, org.apache.xml.serialize,com.google.gson,org.json, org.json.zip,"
        		+ "uk.ac.ed.ph.snuggletex.utilities, uk.ac.ed.ph.snuggletex.upconversion,com.sun.xml.xsom.parser,com.sun.xml.xsom,"
                + "junit.framework, org.apache.commons.logging, org.springframework.util, org.springframework.osgi.service, "
                + "org.springframework.osgi.util, org.springframework.osgi.test, org.springframework.context, "
                + "org.springframework.osgi.test.platform,javax.swing,javax.swing.border,javax.swing.event, javax.swing.plaf,javax.swing.plaf.basic,"
                 + "org.osgi.service.log,org.osgi.util.tracker,org.osgi.framework;version=\"1.4.0\"");
        return mf;
    }
    /**
     * This method is used to set bundles
     * to be loaded in the test.
     * Spring-osgi-test
     * 
     * @return The bundles for the test
     */
    protected String[] getTestBundlesNames() {
        return new String[] {  
            "org.apache.felix,org.osgi.compendium,1.2.0",
            "org.apache.commons,commons-lang3,3.4",
            "eu.iac3.thirdParty.bundles,xml-apis,1.0",
            "eu.iac3.thirdParty.bundles,xerces,2.6.0",
            "eu.iac3.thirdParty.bundles,xmlrpc,1.1",
            "eu.iac3.thirdParty.bundles.xmldb,xmldb-api,20030701",
            "eu.iac3.thirdParty.bundles.xmldb,xmldb-api-sdk,20030701",
            "eu.iac3.thirdParty.bundles.xmldb,xmldb-common,20030701",
            "eu.iac3.thirdParty.bundles.xmldb,xmldb-xupdate,20040205",
            "eu.iac3.thirdParty.bundles.xindice,xindice-lib,1.1", //Xindice lib
            "org.apache.servicemix.bundles,org.apache.servicemix.bundles.xmlresolver,1.2_5",
            "org.apache.servicemix.bundles,org.apache.servicemix.bundles.saxon,9.5.1-6_1",
            "org.apache.commons,com.springsource.org.apache.commons.logging,1.1.1",
            "org.apache.felix,org.apache.felix.configadmin,1.0.10",
            "org.ops4j.pax.confman,pax-confman-propsloader,0.2.2",
            "org.ops4j.pax.web,pax-web-service,0.5.2",
            "org.ops4j.pax.web-extender,pax-web-ex-war,0.5.0",
            "org.ops4j.pax.web,pax-web-jsp,0.5.2",
            "eu.iac3.thirdParty.bundles.xindice,xindice,1.1", //Xindice bundle
            "de.twentyeleven.skysail,org.json-osgi,20080701",
            "org.apache.geronimo.specs, geronimo-ws-metadata_2.0_spec, 1.1.2",
            "org.apache.geronimo.specs, geronimo-javamail_1.4_spec, 1.2",
            "org.jdom, com.springsource.org.jdom, 1.0.0",
            "org.apache.servicemix.bundles, org.apache.servicemix.bundles.xmlschema, 1.4.3_1",
            "org.apache.servicemix.bundles, org.apache.servicemix.bundles.neethi, 2.0.4_1",
            "org.apache.servicemix.bundles, org.apache.servicemix.bundles.xmlsec, 1.3.0_1",
            "org.apache.servicemix.specs, org.apache.servicemix.specs.jaxb-api-2.1, 1.3.0",
            "org.apache.servicemix.bundles, org.apache.servicemix.bundles.jaxb-impl, 2.1.6_1",
            "org.apache.servicemix.specs, org.apache.servicemix.specs.jaxws-api-2.1, 1.3.0",
            "org.apache.servicemix.bundles, org.apache.servicemix.bundles.wsdl4j, 1.6.1_1",
            "org.apache.cxf.dosgi, cxf-dosgi-ri-discovery-distributed, 1.1",
            "org.apache.cxf.dosgi, cxf-dosgi-ri-discovery-distributed-zookeeper-wrapper, 1.1",
            "org.apache.cxf.dosgi, cxf-dosgi-ri-discovery-local, 1.1",
            "org.apache.cxf.dosgi, cxf-dosgi-ri-dsw-cxf, 1.1",
            "org.apache.cxf, cxf-bundle-minimal, 2.2.4",
            "com.google.code.gson, gson, 2.3.1",
            "org.apache.servicemix.bundles, org.apache.servicemix.bundles.relaxngDatatype, 2011.1_1",
            "eu.iac3.thirdParty.bundles.xsom, xsom, 20081112",
            "eu.iac3.thirdParty.bundles.snuggletex, snuggletex-core-upconversion, 1.2.2",
            "eu.iac3.mathMS,simflownyUtilsPlugin, 2.0", 
            "org.apache.derby,com.springsource.org.apache.derby,10.4.1000003.648739",
            "eu.iac3.mathMS,codesDBPlugin, 2.0",
            "eu.iac3.mathMS,AGDM, 2.0",
            "eu.iac3.mathMS,latexPdfGeneratorPlugin, 2.0",
            "eu.iac3.mathMS,codeGeneratorPlugin, 2.0",
            "eu.iac3.mathMS,documentManagerPlugin, 2.0",
            "eu.iac3.mathMS,mathMSGui, 2.0",
            "net.lingala.zip4j, zip4j, 1.3.2",
            "commons-io, commons-io, 2.4",
            "eu.iac3.thirdParty.bundles.sikulix,sikulixapi,1.1.0"
        };  
    }
    
    /**
     * Initial setup for the tests.
     */
    public void onSetUp() {
        startTime = System.currentTimeMillis();
        if (!initialized) {
            //Set image path
            Debug.setDebugLevel(3);
            ImagePath.add("./TestSikuli");
            try {
                //Open browser with the app URL
                Desktop.getDesktop().browse(new URI("http://localhost:" + PORT + "/mathMSGui/index.html"));
                Screen s = new Screen();
                //It must be chromium since chrome does not show contextual menu using SHFT + F10
                s.click("chrome.png");
                Thread.sleep(TWOSECONDS);
            }
            catch (Exception e) {
                e.printStackTrace(System.out);
            }
            initialized = true;
        }
    }
    
    /**
     * Close browser when last test.
     */
    public void onTearDown() {
        if (lastTest) {
            //Close gui
            Screen s = new Screen();
            s.type("w", Key.CTRL);
        }
        System.out.println(this.getName() + ": " + (((float)(System.currentTimeMillis() - startTime)) / 60000));
    }
    
    /**
     * Create collection test.
     */
    public void testCreateCollection() {
        try {
            Screen s = new Screen();
            s.click("createCollection.png");
            SikuliCommons.openContextualMenu(s);
            SikuliCommons.selectOption(s, 1);
            s.type("newCollection" + Key.ENTER);
            Thread.sleep(SECOND);
            if (!new File("target/db/xindiceDB/db/docManager/testCreateCollection/newCollection").exists()) {
                fail("New collection folder not found");
            }
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Error: " + e.getMessage());
        }
    }
    
    public void testRenameCollection() {
        try {
            Screen s = new Screen();
            s.click("renameCollection.png");
            s.type(Key.DOWN);
            SikuliCommons.openContextualMenu(s);
            SikuliCommons.selectOption(s, 2);
            s.type("renamedCollection" + Key.ENTER);
            Thread.sleep(TWOSECONDS);
            if (!new File("target/db/xindiceDB/db/docManager/testRenameCollection/renamedCollection").exists()) {
                fail("The new collection does not exist");
            }
            if (new File("target/db/xindiceDB/db/docManager/testRenameCollection/collectionToRename").exists()) {
                fail("The old collection still exists");
            }
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Error: " + e.getMessage());
        }
    }
    
    public void testDeleteCollection() {
        try {
            Screen s = new Screen();
            s.click("deleteFolder.png");
            s.type(Key.DOWN);
            SikuliCommons.openContextualMenu(s);
            SikuliCommons.selectOption(s, 3);
            s.type(Key.ENTER);
            Thread.sleep(SECONDANDHALF);
            if (new File("target/db/xindiceDB/db/docManager/testDeleteCollection/folderToDelete").exists()) {
                fail("The collection still exists");
            }
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Error: " + e.getMessage());
        }
    }

    public void testMoveCollection() {
        try {
            Screen s = new Screen();
            s.dragDrop("collectionSource.png", new Pattern("collectionTarget.png").similar((float) 0.9));
            Thread.sleep(SECONDANDHALF);
            if (!new File("target/db/xindiceDB/db/docManager/testMoveCollection/collectionTarget/collectionSource").exists()) {
                fail("The collection is not when it is supposed to be");
            }
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Error: " + e.getMessage());
        }
    }
   
    public void testMoveDocument() {
        try {
            Screen s = new Screen();
            s.click(new Pattern("moveDocument.png").similar((float) 0.8));
            s.type(Key.DOWN);
            s.type(Key.DOWN);
            s.type(Key.ENTER);
            Thread.sleep(HALFSECOND);
            s.dragDrop("collectiveMotion.png", "collectionB.png");
            Thread.sleep(SECOND);
            s.waitVanish("movingDocuments.png", 60);
            Thread.sleep(HALFSECOND);
            s.click("collectionB.png");
            Thread.sleep(HALFSECOND);
            s.type(Key.TAB);
            s.click("download.png");
            Thread.sleep(SECOND);
            s.wait(new Pattern("home.png").similar(ACC098));
            
            s.click(new Pattern("home.png").similar(ACC098));
            s.type("testMoveDocument" + Key.ENTER);
            Thread.sleep(SECOND);
            if (!new File(System.getProperty("user.home") + "/testMoveDocument.zip").exists()) {
                fail("The document has not been downloaded");
            }
            else {
                new File(System.getProperty("user.home") + "/testMoveDocument.zip").delete();
            }
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Error: " + e.getMessage());
        }
    }

    public void testDeleteDocument() {
        try {
            Screen s = new Screen();
            s.click("deleteDocument.png");
            Thread.sleep(HALFSECOND);
            s.type(Key.TAB);
            Thread.sleep(SECOND); 
            s.click("modification.png");
            Thread.sleep(SECOND); 
            s.click("delete.png");
            Thread.sleep(SECOND);
            s.type(Key.ENTER);
            Thread.sleep(SECOND);
            s.waitVanish("deletingDocuments.png", 60);
            Thread.sleep(SECOND);
            if (s.exists("modification.png") != null) {
                fail("The document has not been deleted");
            }
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Error: " + e.getMessage());
        }
    }

    public void testEditDocument() {
        try {
            Screen s = new Screen();
            s.click("editDocument.png");
            Thread.sleep(HALFSECOND);
            s.type(Key.TAB + Key.ENTER);
            s.wait("simflownyEditor.png");
            SikuliCommons.moveTreeDown(s, 1);
            SikuliCommons.expandNode(s);
            SikuliCommons.moveTreeDown(s, 1);
            SikuliCommons.modifyValue(s, "Modified problem");
            //Worst case 15 seconds to save
            Thread.sleep(SAFESAVE);
            s.type("w", Key.CTRL);
            if (s.exists("modifiedProblem.png") == null) {
                fail("The document has not been modified properly");
            }
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Error: " + e.getMessage());
        }
    }
  

    public void testImportDocument() {
        try {
            Screen s = new Screen();
            s.click("import.png");
            s.type(Key.DOWN + Key.DOWN + Key.ENTER);
            Thread.sleep(SHORT);
            s.click("upload.png");
            Thread.sleep(HALFSECOND);
            s.type("l", Key.CTRL);
            s.paste(ImagePath.getBundlePath() + "/TestSikuli/referenceFiles/testImportDocument.xml");
            s.type(Key.ENTER);
            Thread.sleep(SECOND);
            s.waitVanish("wait.png");
            if (s.exists("ECA.png") != null) {
                s.click("ECA.png");
                s.click("download.png");
                Thread.sleep(1);
                s.wait(new Pattern("home.png").similar(ACC098));
            
                s.click(new Pattern("home.png").similar(ACC098));
                s.type("testImportDocument" + Key.ENTER);
                Thread.sleep(TWOSECONDS);
                if (new File(System.getProperty("user.home") + "/testImportDocument.zip").exists()) {
                    SikuliCommons.extractZip(System.getProperty("user.home") + "/testImportDocument.zip", 
                            System.getProperty("user.home") + "/testsSikuliGUI");
                    SikuliCommons.cleanDocumentRefs(System.getProperty("user.home") + "/testsSikuliGUI/Elementary Cellular Automata.xml");
                    if (!SikuliCommons.compareFiles(System.getProperty("user.home") + "/testsSikuliGUI/Elementary Cellular Automata.xml", 
                            "TestSikuli/referenceFiles/testImportDocument.xml")) {
                        fail("File is different than expected");
                    }
                    SikuliCommons.removeFolder(System.getProperty("user.home") + "/testsSikuliGUI");
                    new File(System.getProperty("user.home") + "/testImportDocument.zip").delete();
                }
                else {
                    fail("File not downloaded properly");
                }
            }
            else {
                fail("File not imported properly");
            }
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Error: " + e.getMessage());
        }
    }
   
    public void testImportX3D() {
        try {
            Screen s = new Screen();
            s.click("importFolder.png");
            s.type(Key.DOWN + Key.ENTER);
            Thread.sleep(SHORT);
            s.click("upload.png");
            Thread.sleep(HALFSECOND);
            s.type("l", Key.CTRL);
            s.paste(ImagePath.getBundlePath() + "/TestSikuli/referenceFiles/testImportX3D.x3d");
            s.type(Key.ENTER);
            s.wait("simflownyEditor.png");
            SikuliCommons.moveTreeDown(s, 1);
            SikuliCommons.expandNode(s);
            SikuliCommons.moveTreeDown(s, 1);
            SikuliCommons.modifyValue(s, "Imported x3d");
            SikuliCommons.moveTreeDown(s, 2);
            SikuliCommons.modifyValue(s, "Test");
            SikuliCommons.moveTreeDown(s, 1);
            SikuliCommons.modifyValue(s, "Test");
            //Worst case 15 seconds to save
            Thread.sleep(SAFESAVE);
            s.type("w", Key.CTRL);
            s.click("importedX3d.png");
            Thread.sleep(SHORT);
            s.click("download.png");
            Thread.sleep(SECOND);
            s.wait(new Pattern("home.png").similar(ACC098));
            s.click(new Pattern("home.png").similar(ACC098));
            s.type("testImportX3D" + Key.ENTER);
            Thread.sleep(TWOSECONDS);
            if (new File(System.getProperty("user.home") + "/testImportX3D.zip").exists()) {
                SikuliCommons.extractZip(System.getProperty("user.home") + "/testImportX3D.zip", System.getProperty("user.home") + "/testsSikuliGUI");
                if (!SikuliCommons.compareFiles(System.getProperty("user.home") + "/testsSikuliGUI/Imported x3d.x3d", ImagePath.getBundlePath() 
                    + "/TestSikuli/referenceFiles/testImportX3D.x3d")) {
                    fail("File is different than expected");
                }
                SikuliCommons.removeFolder(System.getProperty("user.home") + "/testsSikuliGUI");
                new File(System.getProperty("user.home") + "/testImportX3D.zip").delete();
            }
            else {
                fail("File not imported properly");
            }
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Error: " + e.getMessage());
        }   
    }
   
    public void testCreateDocument() {
        try {
            Screen s = new Screen();
            s.click("createDocument.png");
            s.click("add.png");
            SikuliCommons.selectOption(s, 5);
            s.wait("simflownyEditor.png");
            SikuliCommons.moveTreeDown(s, 1);
            SikuliCommons.expandNode(s);
            SikuliCommons.moveTreeDown(s, 1);
            SikuliCommons.modifyValue(s, "New document");
            SikuliCommons.moveTreeDown(s, 2);
            SikuliCommons.modifyValue(s, "Test");
            SikuliCommons.moveTreeDown(s, 1);
            SikuliCommons.modifyValue(s, "Test");
            //Worst case 15 seconds to save
            Thread.sleep(SAFESAVE);
            s.type("w", Key.CTRL);
            if (s.exists("newDocument.png") == null) {
                fail("File not created properly");
            }
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Error: " + e.getMessage());
        }
    }

    public void testXmlLatex() {
        try {
            Screen s = new Screen();
            s.click("xmlLatex.png");
            Thread.sleep(HALFSECOND);
            s.type(Key.TAB);
            Thread.sleep(HALFSECOND);
            s.click("export-latex.png");
            Thread.sleep(SECOND);
            s.wait(new Pattern("home.png").similar(ACC098));
            s.click(new Pattern("home.png").similar(ACC098));
            s.type("testXmlLatex" + Key.ENTER);
            Thread.sleep(SECOND);
            if (new File(System.getProperty("user.home") + "/testXmlLatex.tex").exists()) {
                if (!SikuliCommons.compareFiles(System.getProperty("user.home") + "/testXmlLatex.tex", 
                        ImagePath.getBundlePath() + "/TestSikuli/referenceFiles/testXmlLatex.tex")) {
                    fail("File is different than expected");
                }
                new File(System.getProperty("user.home") + "/testXmlLatex.tex").delete();
            }
            else {
                fail("File not downloaded properly");
            }
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Error: " + e.getMessage());
        }
    }
    

    public void testXmlPdf() {
        try {
            Screen s = new Screen();
            s.click("xmlPdf.png");
            Thread.sleep(HALFSECOND);
            s.type(Key.TAB);
            Thread.sleep(HALFSECOND);
            s.click("export-pdf.png");
            Thread.sleep(SECOND);
            s.wait(new Pattern("home.png").similar(ACC098));
            s.click(new Pattern("home.png").similar(ACC098));
            s.type("testXmlPdf" + Key.ENTER);
            Thread.sleep(SECOND);
            if (new File(System.getProperty("user.home") + "/testXmlPdf.pdf").exists()) {
                //All generated pdfs have different bytes, so compare sizes instead of content
                if (!SikuliCommons.compareFileSize(System.getProperty("user.home") + "/testXmlPdf.pdf", 
                        ImagePath.getBundlePath() + "/TestSikuli/referenceFiles/testXmlPdf.pdf")) {
                    fail("File is different than expected");
                }
                new File(System.getProperty("user.home") + "/testXmlPdf.pdf").delete();
            }
            else {
                fail("File not downloaded properly");
            }
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Error: " + e.getMessage());
        }
    }
   

    public void testDiscretize() {
        try {
            Screen s = new Screen();
            s.click("discretize.png");
            Thread.sleep(HALFSECOND);
            s.type(Key.TAB);
            s.click("policy.png");
            Thread.sleep(HALFSECOND);
            s.click("discretize-button.png");
            Thread.sleep(SECONDANDHALF);
            s.waitVanish("running.png", 60);
            Thread.sleep(SECOND);
            s.click(new Pattern("description.png").targetOffset(-49, -1));
            Thread.sleep(HALFSECOND);
            s.type(Key.DOWN + Key.DOWN + Key.DOWN + Key.DOWN + Key.RIGHT);
            Thread.sleep(HALFSECOND);
            s.type(Key.DOWN + Key.DOWN + Key.ENTER);
            Thread.sleep(SECOND);
            if (s.exists("problem.png") == null) {
                fail("File not created properly");
            }
            //Resetting the filters
            s.type(Key.ENTER);
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Error: " + e.getMessage());
        }
    }
    

    public void testDiscretizationPolicy() {
        try {
            Screen s = new Screen();
            s.click("discPolicy.png");
            Thread.sleep(HALFSECOND);
            s.type(Key.TAB);
            s.type(Key.DOWN);
            Thread.sleep(HALFSECOND);
            s.click("create-policy.png");
            Thread.sleep(SECONDANDHALF);
            s.click(new Pattern("description.png").targetOffset(-49, -1));
            Thread.sleep(HALFSECOND);
            s.type(Key.DOWN + Key.DOWN + Key.DOWN + Key.DOWN + Key.RIGHT);
            Thread.sleep(HALFSECOND);
            s.type(Key.ENTER);
            Thread.sleep(SECOND);
            if (s.exists("discPolicyResult.png") == null) {
                fail("File not created properly");
            }
            //Resetting the filters
            s.type(Key.ENTER);
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Error: " + e.getMessage());
        }
    }
   

    public void testGenerateSAMRAI() {
        try {
            Screen s = new Screen();
            s.click("codeSAMRAI.png");
            Thread.sleep(HALFSECOND);
            s.type(Key.TAB);
            s.click("selectSAMRAIproblem.png");
            Thread.sleep(HALFSECOND);
            s.click("generate-code.png");
            Thread.sleep(TWOSECONDS);
            s.waitVanish("runningCode.png");
            Thread.sleep(SECOND);
            if (s.exists(new Pattern("codeSAMRAICreated.png").similar(ACC098)) != null) {
                File folder = new File(ImagePath.getBundlePath() 
                        + "/codeFiles/Advection with Sinus 2D/Carles Bona Jr./1/simPlatSAMRAI/AdvectionwithSinus2D/src");
                File[] listOfFiles = folder.listFiles();
                ArrayList<String> files = new ArrayList<String>();
                for (int i = 0; i < listOfFiles.length; i++) {
                    files.add(listOfFiles[i].getName());
                }
                
                ArrayList<String> expected = new ArrayList<String>(Arrays.asList(new String[] {"Makefile", "Problem.h", "MainRestartData.h", 
                    "SAMRAIConnector.cpp", "Problem.cpp", "problem.input", "Functions.cpp", "MainRestartData.cpp", "Functions.h", "README", "run.sh"}));
                Collections.sort(files);
                Collections.sort(expected);
                if (!files.equals(expected)) {
                    fail("Generated files different from expected");
                }
                new File(ImagePath.getBundlePath() + "/codeFiles").delete();
            }
            else {
                fail("Code not created properly");
            }
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Error: " + e.getMessage());
        }
    }
   

    public void testGenerateBOOST() {
        try {
            Screen s = new Screen();
            s.click("generateBoost.png");
            Thread.sleep(HALFSECOND);
            s.type(Key.TAB);
            s.click(new Pattern("selectBoostProblem").similar(ACC098));
            Thread.sleep(HALFSECOND);
            s.click("generate-code.png");
            Thread.sleep(TWOSECONDS);
            s.waitVanish("runningBoost.png");
            Thread.sleep(HALFSECOND);
            if (s.exists(new Pattern("codeBoostCreated.png").similar(ACC098)) != null) {
                File folder = new File(ImagePath.getBundlePath() + "/codeFiles/Cash and goods/Miquel Trias/1/simPlatSAMRAI/Cashandgoods/src");
                File[] listOfFiles = folder.listFiles();
                ArrayList<String> files = new ArrayList<String>();
                for (int i = 0; i < listOfFiles.length; i++) {
                    files.add(listOfFiles[i].getName());
                }
                
                ArrayList<String> expected = new ArrayList<String>(Arrays.asList(new String[] {"Cashandgoods.cpp", "iac3_plod_generator.hpp",
                    "Makefile", "problem.input", "README", "run.sh"}));
                Collections.sort(files);
                Collections.sort(expected);
                if (!files.equals(expected)) {
                    fail("Generated files different from expected");
                }
            }
            else {
                fail("Code not created properly");
            }
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Error: " + e.getMessage());
        }
    }
    
   
    public void testValidate() {
        //Correct validation
        try {
            Screen s = new Screen();
            s.click("validationTest.png");
            Thread.sleep(HALFSECOND);
            s.doubleClick(new Pattern("selectValid").similar(ACC098));
            s.wait("simflownyEditor.png", 60);
            s.click("validationButton.png");
            Thread.sleep(TWOSECONDS);
            if (s.exists(new Pattern("validationOK.png").exact()) == null) {
                s.type("w", Key.CTRL);
                fail("Validation went wrong on a valid document");
            }
            s.type("w", Key.CTRL);
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Error: " + e.getMessage());
        }
        //Wrong validation
        try {
            Screen s = new Screen();
            Thread.sleep(HALFSECOND);
            s.doubleClick(new Pattern("selectInvalid").similar(ACC098));
            s.wait("simflownyEditor.png", 60);
            s.click("validationButton.png");
            Thread.sleep(TWOSECONDS);
            if (s.exists(new Pattern("validationWrong.png").exact()) == null) {
                s.type("w", Key.CTRL);
                fail("Validation of a wrong document failed");
            }
            s.type("w", Key.CTRL);
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Error: " + e.getMessage());
        }
    }
    
    public void testIntegration1() {
        try {
            Screen s = new Screen();
            //First model
            s.click("root.png");
            SikuliTransformer st = new SikuliTransformer(s, 
                    "../../../integration tests/1 - Basic/models/Advection Equation 2D_1_Carles Bona.xml");
            st.execute();
            Thread.sleep(FIVESECONDS);
            s.click("rootSelected.png");
            s.wait("integrationTest1Model.png", 60);
            s.click("integrationTest1Model.png");
            Thread.sleep(SECOND);
            s.click("download.png");
            Thread.sleep(SECOND);
            s.wait(new Pattern("home.png").similar(ACC098));
            s.click(new Pattern("home.png").similar(ACC098));
            s.type("testIntegrationModel" + Key.ENTER);
            Thread.sleep(SECOND);
            if (!new File(System.getProperty("user.home") + "/testIntegrationModel.zip").exists()) {
                fail("The document has not been downloaded");
            }
            else {
                SikuliCommons.extractZip(System.getProperty("user.home") + "/testIntegrationModel.zip", System.getProperty("user.home") 
                        + "/testIntegrationModel");
                FileUtils.copyFile(new File("../../../integration tests/1 - Basic/models/Advection Equation 2D_1_Carles Bona.xml"), new File("target/Advection Equation 2D.xml"));
                SikuliCommons.cleanDocumentRefs(System.getProperty("user.home") + "/testIntegrationModel/Advection Equation 2D.xml");
                SikuliCommons.cleanDocumentRefs("target/Advection Equation 2D.xml");
                if (!SikuliCommons.compareFilesXML(System.getProperty("user.home") + "/testIntegrationModel/Advection Equation 2D.xml",
                        "target/Advection Equation 2D.xml")) {
                    fail("File is different than expected");
                }
                SikuliCommons.removeFolder(System.getProperty("user.home") + "/testIntegrationModel");
                new File(System.getProperty("user.home") + "/testIntegrationModel.zip").delete();
            }
            
            //Problem
            s.click("rootSelected.png");
            st = new SikuliTransformer(s, "../../../integration tests/1 - Basic/problem/Advection with Sinus 2D.xml");
            st.execute();
            Thread.sleep(FIVESECONDS);
            s.click("rootSelected.png");
            s.wait("integrationTest1Problem.png", 60);
            s.click("integrationTest1Problem.png");
            Thread.sleep(SECOND);
            s.click("download.png");
            Thread.sleep(SECOND);
            s.wait(new Pattern("home.png").similar(ACC098));
            s.click(new Pattern("home.png").similar(ACC098));
            s.type("testIntegrationProblem" + Key.ENTER);
            Thread.sleep(SECOND);
            if (!new File(System.getProperty("user.home") + "/testIntegrationProblem.zip").exists()) {
                fail("The document has not been downloaded");
            }
            else {
                SikuliCommons.extractZip(System.getProperty("user.home") + "/testIntegrationProblem.zip", System.getProperty("user.home") 
                        + "/testIntegrationProblem");
                FileUtils.copyFile(new File("../../../integration tests/1 - Basic/problem/Advection with Sinus 2D.xml"), new File("target/Advection with Sinus 2D.xml"));
                SikuliCommons.cleanDocumentRefs(System.getProperty("user.home") + "/testIntegrationProblem/Advection with Sinus 2D.xml");
                SikuliCommons.cleanDocumentRefs("target/Advection with Sinus 2D.xml");
                if (!SikuliCommons.compareFilesXML(System.getProperty("user.home") + "/testIntegrationProblem/Advection with Sinus 2D.xml",
                        "target/Advection with Sinus 2D.xml")) {
                    fail("File is different than expected");
                }
                SikuliCommons.removeFolder(System.getProperty("user.home") + "/testIntegrationProblem");
                new File(System.getProperty("user.home") + "/testIntegrationProblem.zip").delete();
            }
            lastTest = true;
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Error: " + e.getMessage());
        }
    }
    
    /**
     * Remove the whitespaced nodes from a Dom element.
     * 
     * @param e The element to delete the whitespaces from
     */
    public static void removeWhitespaceNodes(Element e) {
        NodeList children = e.getChildNodes();
        for (int i = children.getLength() - 1; i >= 0; i--) {
            Node child = children.item(i);
            if (child instanceof Text && ((Text) child).getData().trim().length() == 0) {
                e.removeChild(child);
            }
            else if (child instanceof Element) {
                removeWhitespaceNodes((Element) child);
            }
        }
    }
}
