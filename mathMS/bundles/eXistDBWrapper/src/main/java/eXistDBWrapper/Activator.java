/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/

package eXistDBWrapper;

import org.exist.xmldb.DatabaseImpl;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

/** 
 * Activator implements the BundleActivator interface 
 * This class is used to register the
 * {@link DocumentManager} as an OSGI Bundle webservice.
 * 
 * @author      ----
 * @version     ----
 */
public class Activator implements BundleActivator {
    @SuppressWarnings("rawtypes")
	private ServiceRegistration registration;
    
	/**
    * Method called by the OSGI manager when the 
    * PhysicalModels Bundle are started.
    * @param context        Bundle context
    * @throws Exception     Bundle exception
    */
    public void start(BundleContext  context) throws Exception {

    	//Registration as a web service.
    	registration = context.registerService(DatabaseImpl.class, new DatabaseImpl(), null);
        
    }
    /**
     * Method called by the OSGI manager when the 
     * DiscModelsPlugin Bundle are stopped.
     * @param context       Bundle context
     * @throws Exception    Bundle exception
     */
    public void stop(BundleContext context) throws Exception {
    	registration.unregister();
    }
}
