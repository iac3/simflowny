/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package xindice;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/** 
 * Activator implements the BundleActivator interface 
 * This class is used to register the
 * {@link ModelManager} as an OSGI Bundle webservice.
 * 
 * @author      ----
 * @version     ----
 */
public class Activator implements BundleActivator {

    
	/**
    * Method called by the OSGI manager when the 
    * PhysicalModels Bundle are started.
    * @param context        Bundle context
    * @throws Exception     Bundle exception
    */
    public void start(BundleContext  context) throws Exception {
        
        
    }
    /**
     * Method called by the OSGI manager when the DiscModelsPlugin Bundle are stopped.
     * Shutdown the database.
     * @param context       Bundle context
     */
    public void stop(BundleContext context) {
    }
}
