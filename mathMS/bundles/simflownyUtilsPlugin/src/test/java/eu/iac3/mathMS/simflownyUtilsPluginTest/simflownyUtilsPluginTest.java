/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.simflownyUtilsPluginTest;

import eu.iac3.mathMS.simflownyUtilsPlugin.SUException;
import eu.iac3.mathMS.simflownyUtilsPlugin.SimflownyUtils;
import eu.iac3.mathMS.simflownyUtilsPlugin.SimflownyUtilsImpl;
import uk.ac.ed.ph.asciimath.parser.AsciiMathParser;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.math.BigDecimal;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;
import org.junit.runner.RunWith;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import com.sun.org.apache.xml.internal.serialize.OutputFormat;
import com.sun.org.apache.xml.internal.serialize.XMLSerializer;

import static org.junit.Assert.*;
import static org.ops4j.pax.exam.CoreOptions.*;
import org.ops4j.pax.exam.Configuration;
import org.ops4j.pax.exam.Option;
import org.ops4j.pax.exam.junit.PaxExam;
import org.ops4j.pax.exam.spi.reactors.ExamReactorStrategy;
import org.ops4j.pax.exam.spi.reactors.PerSuite;

/** 
 * PhysicalModelsTest class is a junit testcase to
 * test the {@link eu.iac3.mathMS.modelManagerPlugin.ModelManager}.
 * The class has two single test with multiple assertions:
 * The first test checks the PhysicalModels methods with the
 * phisModel collection. It uses the modelWaves2ndOrder.xml file
 * The second test checks the PhysicalModels methods with the
 * phisCase collection. It uses the problemWaves.xml file
 * @author      ----
 * @version     ----
 */
@RunWith(PaxExam.class)
@ExamReactorStrategy(PerSuite.class)
public class simflownyUtilsPluginTest {
    
    long startTime;
    
    @Rule 
    public TestName name = new TestName();
    
	
    /**
     * Pax exam framework configuration.
     * @return	options
     */
    @Configuration
    public Option[] config() {
        return options(
    		bootDelegationPackages(
    				 "com.sun.*"
    				),
        	systemProperty("org.osgi.service.http.port").value("8080"),
        	systemProperty("file.encoding").value("UTF-8"),
        	mavenBundle("org.apache.felix", "org.osgi.compendium", "1.4.0"),
        	mavenBundle("org.apache.commons", "commons-lang3", "3.7"),
            mavenBundle("org.apache.commons", "commons-text", "1.3"),
            mavenBundle("org.apache.servicemix.bundles", "org.apache.servicemix.bundles.relaxngDatatype", "2011.1_1"),
            mavenBundle("org.apache.servicemix.bundles", "org.apache.servicemix.bundles.xmlresolver", "1.2_5"),
            mavenBundle("org.apache.servicemix.bundles", "org.apache.servicemix.bundles.saxon", "9.8.0-10_1"),
            mavenBundle("eu.iac3.thirdParty.bundles.xsom", "xsom", "20140925"),
            mavenBundle("com.google.code.gson", "gson", "2.8.4"),
            mavenBundle("com.fasterxml.jackson.core", "jackson-core", "2.10.3"),
            mavenBundle("com.fasterxml.jackson.core", "jackson-annotations", "2.10.3"),
            mavenBundle("com.fasterxml.jackson.core", "jackson-databind", "2.10.3"),
            mavenBundle("eu.iac3.thirdParty.bundles.underscore", "underscore", "1.60"),
            mavenBundle("org.yaml", "snakeyaml", "1.26"),
            mavenBundle("com.fasterxml.jackson.dataformat", "jackson-dataformat-yaml", "2.10.3"),
            mavenBundle("eu.iac3.thirdParty.bundles.snuggletex", "snuggletex-core-upconversion", "1.2.2"),
            mavenBundle("org.mozilla", "rhino", "1.7.12"),
            junitBundles()
            );
    }
 
    /**
     * Initial setup for the tests.
     */
    @Before
    public void onSetUp() {
        startTime = System.currentTimeMillis();
    }
    
    /**
     * Close browser when last test.
     */
    @After
    public void onTearDown() {
        System.out.println(name.getMethodName() + ": " + (((float)(System.currentTimeMillis() - startTime)) / 60000));
    }
    
    @Test
    public void testXML2YAML() {
        try {
            String yaml = getStringFromInputStream("/heat.yaml");
            String xml = getStringFromInputStream("/HeatEquation.xml");
         //   System.out.println(SimflownyUtils.xmlToYAML(SimflownyUtils.xmlToJSON(xml, false)));
            assertEquals(yaml, new String(SimflownyUtils.xmlToYAML(SimflownyUtils.xmlToJSON(xml, false)), "UTF-8"));
        }
        catch (Exception e) {
        	e.printStackTrace();
            fail("Error converting to YAML: " + e.toString());
        }	
    }
    
   // @Test
    public void testYAML2XML() {
        try {
            String yaml = getStringFromInputStream("/heat.yaml");
            String xml = getStringFromInputStream("/HeatEquation.xml");
            //System.out.println(SimflownyUtils.yamlToXML(yaml));
            assertEquals(indent(xml), indent(SimflownyUtils.yamlToXML(yaml)));
        }
        catch (SUException e) {
        	e.printStackTrace();
            fail("Error converting to YAML: " + e.toString());
        }	
    }
    
    /**
     * Test of {@link eu.iac3.mathMS.simflownyUtilsPlugin.JSONXMLConverter #toXML()}.
     * Includes 2 tests:
     * 1 - JSON to XML
     * 2 - JSON incorrect format
     * 
     */
    @Test
    public void testJSON2XML() {
        try {
            //Test 1: JSON to XML
            String json = getStringFromInputStream("/Collective_motion_problem.json");
            String comp = getStringFromInputStream("/Collective_motion_problem.xml");
       //     System.out.println(indent(su.jsonToXML(json)));
            assertEquals(indent(comp), indent(SimflownyUtils.jsonToXML(json)));
        }
        catch (SUException e) {
            fail("Error converting to XML: " + e.toString());
        }
        try {
            //Test 1: JSON incorrect format
            String json = getStringFromInputStream("/Collective_wrong.json");
            SimflownyUtils.jsonToXML(json);
            fail("Test 1. JSON incorrect format converted!!!!");
        }
        catch (SUException e) {
            assertEquals(SUException.SU001, e.getErrorCode());
        }
    }

    /**
     * Reads a file.
     * @param path  The file path
     * @return      The file as string
     */
    private String getStringFromInputStream(String path) {
        InputStream is = getClass().getResourceAsStream(path);
 
        BufferedReader br = null;
        StringBuilder sb = new StringBuilder();
 
        String line;
        try {
 
            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line + "\n");
            }
 
        } 
        catch (IOException e) {
            e.printStackTrace();
        } 
        finally {
            if (br != null) {
                try {
                    br.close();
                } 
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
 
        return sb.toString();
 
    }
    
    /**
     * A method for the format of the output.
     * 
     * @param document          The document to be formatted
     * @return                  The document formatted
     * @throws SUException        Error
     */
    public String indent(String document) throws SUException {
        final int indentation = 4;
        try {
            DocumentBuilder docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document doc = docBuilder.parse(new ByteArrayInputStream(document.getBytes("UTF-8")));
    
            OutputFormat format = new OutputFormat(doc);
            format.setIndenting(true);
            format.setIndent(indentation);
            Writer output = new StringWriter();
            XMLSerializer serializer = new XMLSerializer(output, format);
            serializer.serialize(doc);
    
            return output.toString();
        }
        catch (Exception e) {
            throw new SUException(SUException.SU00X, e.toString());
        }
    }
    
    /**
     * An auxiliar function to read a file as String.
     * 
     * @param is  The input stream of the file
     * @return      The resource as string
     * @throws UnsupportedEncodingException Error reading UTF-8
     */
    public static String convertStreamToString(InputStream is) throws UnsupportedEncodingException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } 
        catch (IOException e) {
            e.printStackTrace();
        } 
        finally {
            try {
                is.close();
            } 
            catch (IOException e) { 
                e.printStackTrace();
            }
        }
        return sb.toString();
    }
    
    /**
     * Test of {@link eu.iac3.mathMS.simflownyUtilsPlugin.XMLJSONConverter}.
     * Includes 3 tests:
     * 1 - XML to JSON with data
     * 2 - XML to JSON empty data
     * 3 - Wrong XML
     * 4 - Wrong setup for the converter
     */
    @Test
    public void testXML2JSON() {
        try {
            //XML to JSON with data
            String json = getStringFromInputStream("/Collective_motion_problem.json");
            String xml = getStringFromInputStream("/Collective_motion_problem.xml");
       //     System.out.println(su.xmlToJSON(xml, false));
            assertEquals(json.replaceAll("(\\s+|\\n|\\r\\n)", ""), SimflownyUtils.xmlToJSON(xml, false).replaceAll("(\\s+|\\n|\\r\\n)", ""));
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Error converting to JSON: " + e.toString());
        }
        try {
            //XML to JSON empty data
            String json = getStringFromInputStream("/empty_problem.json");
   //         System.out.println(su.xmlToJSON("agentBasedProblemSpatialDomain", true));
            assertEquals(json.replaceAll("(\\s+|\\n|\\r\\n)", ""), 
            		SimflownyUtils.xmlToJSON("agentBasedProblemSpatialDomain", true).replaceAll("(\\s+|\\n|\\r\\n)", "").replaceAll("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}", "#DATE#"));
        }
        catch (Exception e) {
            fail("Error converting to JSON: " + e.toString());
        }
        try {
            //Wrong XML
            String xml = getStringFromInputStream("/Wrong.xml");
            SimflownyUtils.xmlToJSON(xml, false);
            fail("Test 2 Wrong XML converted to JSON!!!!!!");
        }
        catch (SUException e) {
            assertEquals(SUException.SU001, e.getErrorCode());
        }
        //XSD schema does not exist
        try {
            String xml = getStringFromInputStream("/Collective_motion_problem_setuperror.xml");
            SimflownyUtils.xmlToJSON(xml, false);
            fail("Test 2 Non existent schema model converted to JSON!!!!!!");
        }
        catch (SUException e) {
        	e.printStackTrace();
            assertEquals(SUException.SU004, e.getErrorCode());
        }
    }
    
    /**
     * Test of {@link eu.iac3.mathMS.simflownyUtilsPlugin.XSDTagStructureGenerator}.
     * 1 - Generate structure
     * 2 - Wrong struct
     */
    @Test
    public void testGetStruct() {
        try {
            //Test 1 Generate structure
            assertNotNull(SimflownyUtils.getStruct("agentBasedProblemSpatialDomain"));
        }
        catch (Exception e) {
            fail("Error getting struct: " + e.toString());
        }
        try {
            //Test 2 Wrong struct
            SimflownyUtils.getStruct("agentBem");
            fail("Test 3 Wrong struct generated struct!!!!!!");
        }
        catch (SUException e) {
            assertEquals(SUException.SU005, e.getErrorCode());
        }
    }
    
    /**
     * Test infix to mathML content conversion.
     * 1 - Conversion
     */
    @Test
    public void testConversion() {
        //1 - Conversion
        try {
            assertNotNull(SimflownyUtils.infixToMathML("<math title=\" (5x)/(1-x) \" xmlns=\"http://www.w3.org/1998/Math/MathML\">\n"
                    + "  <mstyle mathcolor=\"blue\" fontfamily=\"serif\" displaystyle=\"true\">\n" 
                    + "    <mfrac>\n" 
                    + "      <mrow>\n"
                    + "        <mn>5</mn>\n" 
                    + "        <mi>x</mi>\n" 
                    + "      </mrow>\n"
                    + "      <mrow>\n" 
                    + "        <mn>1</mn>\n" 
                    + "        <mo>-</mo>\n"
                    + "        <mi>x</mi>\n"
                    + "      </mrow>\n"
                    + "    </mfrac>\n" 
                    + "  </mstyle>\n" 
                    + "</math>"));
        }
        catch (SUException e) {
            fail("Error converting infix to mathML: " + e.toString());
        }
    }
    
    /**
     * Tests the conversion of a document manager Tree to JSON.
     */
    @Test
    public void testTreeToJSON() {
        //1 - Conversion
        try {
            DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                + "<docManagerTree xmlns=\"urn:mathms\">"
                + "<collection>"
                + "<name>Coleccion padre</name>"
                + "<collections>"
                + "<collection>"
                + "<name>Hijo 1</name>" 
                + "</collection>" 
                + "<collection>" 
                + "<name>Hijo 2</name>" 
                + "<collections>" 
                + "<collection>" 
                + "<name>Nieto 2.1</name>"
                + "</collection>" 
                + "</collections>" 
                + "</collection>" 
                + "</collections>" 
                + "</collection>" 
                + "</docManagerTree>"));

            Document doc = db.parse(is);
            
            assertNotNull(SimflownyUtilsImpl.docManagerTreeToJSON(doc.getDocumentElement()));
        }
        catch (Exception e) {
            fail("Error converting tree to json: " + e.toString());
        }
    }
    
    /**
     * Manual test for convenience.
     */
    //@Test
    public void testManual() {
        try {
          //  String xml = getStringFromInputStream("/prueba.xml");
       //    System.out.println(su.xmlToJSON(xml, false));
        //   String json = su.xmlToJSON(xml, false);
       //     String json = getStringFromInputStream("/manual.json");
       //     System.out.println(su.jsonToXML(su.xmlToJSON(xml, false)));
     //       System.out.println(su.getStruct("genericPDEModel"));
           // System.out.println(su.xmlToJSON("discretizationSchema", true));
            //System.out.println(SimflownyUtils.infixToMathML("<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mi>K</mi></math>"));

        	
       /* 	String xml = getStringFromInputStream("/palen_1d.xml");
            FileWriter fichero = new FileWriter("palen_1d.yaml");
            PrintWriter pw = new PrintWriter(fichero);
            String yaml = new String(SimflownyUtils.xmlToYAML(SimflownyUtils.xmlToJSON(xml, false)), "UTF-8");
            pw.write(yaml);
            fichero.close();*/
            
        	/*String yaml = getStringFromInputStream("/leakage.yaml");
        	FileWriter fichero = new FileWriter("leakage_back.xml");
            String kessence = SimflownyUtils.yamlToXML(yaml); 
            PrintWriter pw = new PrintWriter(fichero);
            pw.write(kessence);
            fichero.close();*/
            
          //  assertEquals(kessence, SimflownyUtils.yamlToXML(new String(SimflownyUtils.xmlToYAML(SimflownyUtils.xmlToJSON(kessence, false)), "UTF-8")));
            
        	/*String asciimath = "min(p_kappa_cc,p_kappa_cc*((R_0/max(0.0000000001,sqrt(x^2+y^2+z^2)))^eta_damping_exp))";
        	
            final AsciiMathParser parser = new AsciiMathParser();
            final Document result = parser.parseAsciiMath(asciimath);
            String mathmlTmp = SimflownyUtilsImpl.infixToMathML(result);
            System.out.println(mathmlTmp);*/
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            fail("Error converting to XML: " + e.toString());
        }
    }
}
