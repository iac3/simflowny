/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.simflownyUtilsPlugin;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sun.xml.xsom.XSAnnotation;
import com.sun.xml.xsom.XSAttributeDecl;
import com.sun.xml.xsom.XSAttributeUse;
import com.sun.xml.xsom.XSComplexType;
import com.sun.xml.xsom.XSElementDecl;
import com.sun.xml.xsom.XSModelGroup;
import com.sun.xml.xsom.XSModelGroupDecl;
import com.sun.xml.xsom.XSParticle;
import com.sun.xml.xsom.XSSimpleType;
import com.sun.xml.xsom.XSTerm;
import com.sun.xml.xsom.parser.XSOMParser;

import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * Class that parses the XSD and XML into JSON.
 * @author bminano
 *
 */
class XMLJSONConverter {
    private Document doc;
    private XSOMParser parser;
    private String rootElement;
    private Document schematron;
    JsonArray struct;
    private boolean printAttributes;
    private ArrayList<String> recursionStack;
    
    private ArrayList<String> seqList;
    
    /**
     * Constuctor.
     * @param xsdContent        The XSD content
     * @param schematronContent The schematron content
     * @param rootElement       The root of the xsd to parse.
     * @throws SUException      MM00X - External error
     */
    XMLJSONConverter(String xsdContent, String schematronContent, String rootElement) throws SUException {
        try {
            seqList = new ArrayList<String>();
            SAXParserFactory factory = SAXParserFactory.newInstance();
            parser = new XSOMParser(factory);
            parser.setAnnotationParser(new XSOMAnnotationParser());
            parser.parse(xsdContent);            
            this.rootElement = rootElement;
            if (parser.getResult().getSchemaSize() <= 1 || parser.getResult().getSchema(1).getElementDecl(rootElement) == null) {
                throw new SUException(SUException.SU004, "Element " + rootElement + " does not exist in the schema " + xsdContent); 
            }
            if (!schematronContent.equals("")) {
                String content = Utils.readFile(schematronContent);
                schematron = Utils.stringToDom(content);
            }
            else {
                schematron = null;
            }
            XSDTagStructureGenerator tagStruct = new XSDTagStructureGenerator();
            tagStruct.addStruct(xsdContent, schematronContent, rootElement);
            struct = new JsonParser().parse(tagStruct.getStruct()).getAsJsonArray();
        }
        catch (SAXException e) {
            e.printStackTrace(System.out);
            throw new SUException(SUException.SU004, "Error with Schema " + xsdContent + ". Please, check the schema.");
        }
    }
    
    /**
     * Parse the particle element. 
     * @param particle      The particle
     * @param attributes    The attributes of the parent element
     * @param absPath       The xpath of the element
     * @param indent        The current indent
     * @param order         The element order
     * @param choice        If there is an ancestor choice
     * @param recursiveTag	When tag is recursive or not
     * @return              The json object
     * @throws SUException  MM008 Schematron not valid
     *                       MM00X External error
     */
    private StringWriter printParticle(XSParticle particle, Collection< ? extends XSAttributeUse> attributes, 
            String absPath, String indent, int order, boolean choice, String recursiveTag) throws SUException {
        //writer
        StringWriter w = new StringWriter();
        int min = particle.getMinOccurs().intValue();
        int max = particle.getMaxOccurs().intValue();
        if (choice) {
            min = 0;
        }
        //occurs = "  MinOccurs = " + particle.getMinOccurs() + ", MaxOccurs = " + particle.getMaxOccurs();
        XSTerm term = particle.getTerm();
        if (term.isModelGroup()) {
            w.write(printGroup(term.asModelGroup(), attributes, absPath, indent, recursiveTag).toString());
        }
        else if (term.isModelGroupDecl()) {
            w.write(printGroupDecl(term.asModelGroupDecl(), attributes, absPath, indent, recursiveTag).toString());
        }
        else if (term.isElementDecl()) {
            w.write(printElements(term.asElementDecl(), absPath, indent, min, max, order, recursiveTag).toString());
        }
        return w;
    }
    
    /**
     * Parse the model group. 
     * @param modelGroup    The model group
     * @param attributes    The attributes of the parent element
     * @param absPath       The xpath of the element
     * @param indent        The current indent
     * @param recursiveTag	When tag is recursive or not
     * @return              The json object
     * @throws SUException  MM008 Schematron not valid
     *                       MM00X External error
     */
    private StringWriter printGroup(XSModelGroup modelGroup, Collection< ? extends XSAttributeUse> attributes, 
            String absPath, String indent, String recursiveTag) throws SUException {
        //writer
        StringWriter w = new StringWriter();
        boolean rec = false;
        if (seqList.contains(absPath)) {
            rec = true;
        }
        else {
            seqList.add(absPath);
        }
        String newIndent = indent;
        boolean choice = modelGroup.getCompositor().toString().equals("choice");
        //Only set children info if there is a sequence. When it is a choice just print all the elements without any metadata
        if ((modelGroup.getCompositor().toString().equals("sequence") || modelGroup.getCompositor().toString().equals("choice")) && !rec) {
            w.write(", \"children\":[\n");
            newIndent = newIndent + "\t";
        }
        boolean initial = true;
        //Print attributes
        String result = "";
        if (printAttributes) {
            result = printAttributes(attributes, newIndent, absPath).toString();
        }
        printAttributes = false;
        if (!result.equals("")) {
            w.write(result);
            initial = false;
        }
        //Print children
        XSParticle[] list = modelGroup.getChildren();
        for (int i = 0; i < list.length; i++) {
            XSParticle particle = list[i];
            String printed = printParticle(particle, attributes, absPath, newIndent, i, choice, recursiveTag).toString();
            if (!printed.trim().equals("") && !initial) {
                w.write(",\n");
            }
            if (!printed.trim().equals("") && initial) {
                initial = false;
            }
            w.write(printed);
        }
        w.write("\n");
        if ((modelGroup.getCompositor().toString().equals("sequence") || modelGroup.getCompositor().toString().equals("choice")) && !rec) {
            w.write(indent + "]");
        }
        return w;
    }
    
    /**
     * Parse the model group declaration. 
     * @param modelGroupDecl    The model group declaration
     * @param attributes        The attributes of the parent element
     * @param absPath           The xpath of the element
     * @param indent            The current indent
     * @param recursiveTag		When tag is recursive or not
     * @return                  The json object
     * @throws SUException      MM008 Schematron not valid
     *                           MM00X External error
     */
    private StringWriter printGroupDecl(XSModelGroupDecl modelGroupDecl, Collection< ? extends XSAttributeUse> attributes, 
            String absPath, String indent, String recursiveTag) throws SUException {
        //writer
        StringWriter w = new StringWriter();
        w.write(printGroup(modelGroupDecl.getModelGroup(), attributes, absPath, indent, recursiveTag).toString());
        return w;
    }
    
    /**
     * Parse the attributes.
     * @param attributes    The attributes
     * @param indent        The current indent
     * @param absPath       The path of the parent element.
     * @return              The json attributes
     * @throws SUException  MM00X External error
     */
    private StringWriter printAttributes(Collection< ? extends XSAttributeUse> attributes, String indent, String absPath) throws SUException {
        StringWriter w = new StringWriter();
        Iterator< ? extends XSAttributeUse> i = attributes.iterator();
        String acc = "";
        while (i.hasNext()) {
            XSAttributeUse attUse = i.next();
            XSAttributeDecl attributeDecl = attUse.getDecl();
            XSSimpleType xsAttributeType = attributeDecl.getType();
       
            String value = null;
            NodeList nodes = Utils.find(doc, absPath + "/@" + attributeDecl.getName());
            if (nodes.getLength() > 0) {
                value = nodes.item(0).getTextContent();
            }
       
            String type;
            String restriction = SimflownyUtilsImpl.printSimpleType(xsAttributeType.asSimpleType());
            String schematronRestriction = SimflownyUtilsImpl.schematronRestriction(schematron, absPath);
            if (restriction.equals("")) {
                type = xsAttributeType.getName() + schematronRestriction;
            }
            else {
                type = xsAttributeType.getBaseType().getName() + restriction + schematronRestriction;
            }
            
            if (nodes.getLength() > 0 || attUse.isRequired()) {
                acc = acc + indent + "{\"tag\":\"" + attributeDecl.getName() + "\""
                        + ", \"jsonPath\": \"" + absPath + "/mms:" + attributeDecl.getName() + "[1]" + "\""
                        + ", \"pureType\": \"attribute\""
                        + ", \"input\": \"" + value + "\""
                        + ", \"type\": \"" + type + "\""
                        + ", \"expanded\": false";
                if (attUse.isRequired()) {
                    acc = acc + ", \"minCard\": 1";
                }
                else {
                    acc = acc + ", \"minCard\": 0";
                }
    
                acc = acc + ", \"maxCard\": 1"
                        + ", \"leaf\": true"
                        + ", \"attribute\": true"
                        + "}, \n";
            }
        }
        if (acc.length() > 0) {
            acc = acc.substring(0, acc.lastIndexOf(","));
            w.write(acc);
        }
        return w;
    }
    
    /**
     * Parse the complex type.
     * @param complexType       The complex type
     * @param attributes        The attributes of the parent element
     * @param absPath           The xpath of the element
     * @param indent            The current indent
     * @param order             The element order
     * @param choice            If there is an ancestor choice
     * @param recursiveTag		When tag is recursive or not
     * @return                  The json object
     * @throws SUException      MM008 Schematron not valid
     *                           MM00X External error
     */
    private StringWriter printComplexType(XSComplexType complexType, Collection< ? extends XSAttributeUse> attributes,
            String absPath, String indent, int order, boolean choice, String recursiveTag) throws SUException {
        XSParticle particle = complexType.getContentType().asParticle();
        if (particle != null) {
            return printParticle(particle, attributes, absPath, indent, order, choice, recursiveTag);
        }
        //writer
        StringWriter w = new StringWriter();
        return w;
    }
    
    /**
     * Parse the element with all its data elements.
     * @param element       The element
     * @param absPath       The parent path
     * @param indent        The current indent
     * @param min           The min cardinality
     * @param max           The max cardinality
     * @param order         The element order
     * @param recursiveTag	When tag is recursive or not
     * @return              The json object
     * @throws SUException  MM008 Schematron not valid
     *                       MM00X External error
     */
    private StringWriter printElements(XSElementDecl element, String absPath, String indent, int min, int max, int order, String recursiveTag) throws SUException {
        //writer
        StringWriter w = new StringWriter();
        String prefix = "mms";
        String sufix = "";
        if (element.getTargetNamespace().equals("urn:simml")) {
            prefix = "sml";
        }
        if (element.getTargetNamespace().equals("http://www.w3.org/1998/Math/MathML")) {
            prefix = "mt";
            sufix = "ML";
        }
        absPath += "/" + prefix + ":" + element.getName();

        NodeList nodes = Utils.find(doc, absPath);
        
        //Information from all the nodes
        for (int i = 0; i < nodes.getLength(); i++) {
            Element node = (Element) nodes.item(i);
            String value = "";
            if (node.hasChildNodes() && (node.getChildNodes().item(0).getNodeType() == Node.TEXT_NODE 
                    || node.getChildNodes().item(0).getNodeType() == Node.CDATA_SECTION_NODE)) {
                value = node.getChildNodes().item(0).getNodeValue();    
            }
            w.write(printElement(element, absPath + sufix + "[" + (i + 1) + "]", indent, value, min, max, order, recursiveTag).toString());
            if (i < nodes.getLength() - 1) {
                w.write(",\n");    
            }
        }
        //Empty node with Cardinality one or more must be have one empty element 
        if (nodes.getLength() == 0 && min == 1) {
            w.write(printElement(element, absPath + sufix + "[1]", indent, "", min, max, order, recursiveTag).toString());
        }
        return w;
    }

    /**
     * Parse the element which may be an single data element.
     * @param element       The element
     * @param absPath       The xpath 
     * @param indent        The current indent
     * @param inputValue    The value of the element data
     * @param min           The min cardinality
     * @param max           The max cardinality
     * @param order         The element order
     * @param recursiveTag	When tag is recursive or not
     * @return              The json object
     * @throws SUException  MM008 Schematron not valid
     *                       MM00X External error
     */
    private StringWriter printElement(XSElementDecl element, String absPath, String indent, String inputValue, 
            int min, int max, int order, String recursiveTag) throws SUException {

        printAttributes = true;
        //writer
        StringWriter w = new StringWriter();
        w.write(indent + "{\"tag\":\"" + element.getName() + "\"");
        w.write(", \"jsonPath\": \"" + absPath + "\"");
        //Pure type for visualization from annotation
        XSAnnotation annotation = element.getAnnotation();
        boolean uneditable = false;
        String pureType = "none";
        if (annotation != null) {
            String[] annotations = (String[]) annotation.getAnnotation();
            for (int i = 0; i < annotations.length; i++) {
                if (annotations[i].equals("uneditable")) {
                    uneditable = true;
                }
                else {
                    pureType = annotations[i];
                }
            }
        }
        w.write(", \"order\": " + order + "");
        boolean recursive = false;
        if (!element.getName().equals("math") && !element.getName().equals("simml") && !element.getName().equals("algorithm")) {
            w.write(", \"pureType\": \"" + pureType + "\"");
            if (element.getType().isComplexType()) {
                //Checking recursion before accessing the children.
                String complexTypeName = element.getType().asComplexType().getName();
                if (complexTypeName != null && !recursionStack.contains(complexTypeName)) {
                    recursionStack.add(complexTypeName);
                }
                else if (complexTypeName != null && recursionStack.contains(complexTypeName)) {
                    recursive = true;
                    recursiveTag = element.getName();
            
                }
                //Recursive property
                if (recursiveTag != null) {
                    w.write(", \"recursive\": \"" + recursiveTag + "\"");
                }
                //Null input in complex types
                w.write(", \"input\": null");
                if (absPath.lastIndexOf("/") == 0) {
                    w.write(", \"expanded\": true");
                }
                else {
                    w.write(", \"expanded\": false");
                }
                if (element.getName().equals("instructionSet")) {
                    w.write(", \"type\": \"instructionSet\"");
                }
                w.write(", \"minCard\": " + min);
                w.write(", \"maxCard\": " + max);
                w.write(printComplexType(element.getType().asComplexType(), element.getType().asComplexType().getAttributeUses(), 
                        absPath, indent, order, false, recursiveTag).toString());
                //Undo recursion stack
                if (complexTypeName != null && !recursive) {
                    recursionStack.remove(complexTypeName);
                }
            }
            else {
                //Recursive property
                if (recursiveTag != null) {
                    w.write(", \"recursive\": \"" + recursiveTag + "\"");
                }
                //Set the value for the simple element when possible
                //No value
                if (inputValue.equals("")) {
                    //Empty date should be current date
                    if (element.getType().getName().equals("dateTime")) {
                        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                        Date date = new Date();
                        w.write(", \"input\": \"" + dateFormat.format(date) + "\"");
                    }
                    else {
                        w.write(", \"input\": null");
                    }
                }
                else {
                    w.write(", \"input\": \"" + inputValue.replaceAll("\\\\", "\\\\\\\\").replaceAll("\"", "\'") + "\"");
                }
                //Also print if there are restrictions
                String restriction = SimflownyUtilsImpl.printSimpleType(element.getType().asSimpleType());
                String schematronRestriction = SimflownyUtilsImpl.schematronRestriction(schematron, absPath);
                if (uneditable) {
                    w.write(", \"type\": \"uneditable\"");
                }
                else {
                    if (restriction.equals("")) {
                        w.write(", \"type\": \"" + element.getType().getName() + schematronRestriction + "\"");
                    }
                    else {
                        w.write(", \"type\": \"" + element.getType().getBaseType().getName() + restriction + schematronRestriction + "\"");
                    }
                }
                w.write(", \"minCard\": " + min);
                w.write(", \"maxCard\": " + max);
                w.write(", \"leaf\": true");
                
            }
        }
        //Special treatment to mathML elements
        else if (element.getName().equals("math")) {
            //Recursive property
            if (recursiveTag != null) {
                w.write(", \"recursive\": \"" + recursiveTag + "\"");
            }
            //Set the value for the simple element when possible
            //No value
            if (inputValue.equals("")) {
                w.write(", \"input\": null");
            }
            else {
                w.write(", \"input\": \"" + inputValue.trim().replaceAll("\"", "\\\\\"") + "\"");
            }
            w.write(", \"type\": \"mathML\"");
            w.write(", \"pureType\": \"mathML\"");
            w.write(", \"minCard\": " + min);
            w.write(", \"maxCard\": " + max);
            w.write(", \"leaf\": true");
        }
        //Special treatment to simml elements
        else {
            //Recursive property
            if (recursiveTag != null) {
                w.write(", \"recursive\": \"" + recursiveTag + "\"");
            }
            w.write(", \"pureType\": \"" + pureType + "\"");
            w.write(", \"input\": null");
            //Also print if there are restrictions
            if (uneditable) {
                w.write(", \"type\": \"uneditable\"");
            }
            else {
                w.write(", \"type\": \"" + getType(element.getName(), absPath) + "\"");
            }
            w.write(", \"minCard\": " + min);
            w.write(", \"maxCard\": " + max);
            if (Utils.find(doc, absPath).getLength() > 0) {
                Element root = (Element) Utils.find(doc, absPath).item(0);
                w.write(printChildrenSimML(root, indent, absPath).toString());
            }
        }
        w.write("}");
        seqList.remove(absPath.substring(absPath.lastIndexOf(":") + 1));
        return w;
    }
    
    /**
     * Parse the simML elements who need special treatment.
     * @param elem      The element
     * @param indent    The current indent
     * @param absPath   The xpath of the parent
     * @return          The json object
     */
    private StringWriter printChildrenSimML(Element elem, String indent, String absPath) {
        //writer
        StringWriter w = new StringWriter();
        NodeList nodes = elem.getChildNodes();

        
        Hashtable<String, Integer> indexes = new Hashtable<String, Integer>();
        String attributes = printAttributesSimML(elem, indent + "\t", absPath).toString();
        if (nodes.getLength() == 0 && attributes.equals("")) {
            w.write(", \"leaf\": true");
            return w;
        }
        w.write(", \"children\":[\n");
        if (!attributes.equals("")) {
            w.write(attributes);
            if (nodes.getLength() > 0) {
                w.write(",\n");
            }
        }
        
        //Information from all the nodes
        for (int i = 0; i < nodes.getLength(); i++) {
            Element node = (Element) nodes.item(i);
            String value = "";
            if (node.hasChildNodes() && (node.getChildNodes().item(0).getNodeType() == Node.TEXT_NODE 
                    || node.getChildNodes().item(0).getNodeType() == Node.CDATA_SECTION_NODE)) {
                value = node.getChildNodes().item(0).getNodeValue();    
            }
            Integer rep = indexes.get(node.getLocalName());
            if (rep == null) {
                rep = 0;
            }
            rep++;
            indexes.put(node.getLocalName(), rep);
            String prefix = "sml";
            String jsonPath = "";
            if (node.getNamespaceURI().equals("http://www.w3.org/1998/Math/MathML")) {
                prefix = "mt";
                jsonPath = absPath + "/" + prefix + ":" + node.getLocalName() + "ML[" + rep + "]";
            }
            else {
                jsonPath = absPath + "/" + prefix + ":" + node.getLocalName() + "[" + rep + "]";
            }
            w.write(printSimML(node, indent + "\t", value, jsonPath).toString());
            if (i < nodes.getLength() - 1) {
                w.write(",\n");    
            }
            else {
                w.write("\n");    
            }
        }
        //Empty node with Cardinality one or more must be have one empty element 
      /*  if (nodes.getLength() == 0 && getminCardSimml(elem.getLocalName(), elem.getParentNode().getLocalName()) == 1) {
            String prefix = "sml";
            String jsonPath = "";
            if (elem.getNamespaceURI().equals("http://www.w3.org/1998/Math/MathML")) {
                prefix = "mt";
                jsonPath = absPath + "/" + prefix + ":" + elem.getLocalName() + "ML[" + rep + "]";
            }
            else {
                jsonPath = absPath + "/" + prefix + ":" + elem.getLocalName() + "[" + rep + "]";
            }
            w.write(printSimML(elem, indent + "\t", "", jsonPath).toString());
        }*/
        w.write(indent + "]");
        return w;
    }
    
    /**
     * Parse simML attributes.
     * @param elem      The element
     * @param indent    The current indent
     * @param absPath   The parent xpath
     * @return          The json object
     */ 
    private StringWriter printAttributesSimML(Element elem, String indent, String absPath) {
        StringWriter w = new StringWriter();
        NamedNodeMap attributes = elem.getAttributes();
        String acc = "";
        for (int i = 0; i < attributes.getLength(); i++) {
            Node attribute = attributes.item(i);
            if (attribute.getNamespaceURI() == null) {
                String attPath = absPath + "/sml:" + attribute.getLocalName() + "[1]";
       
                acc = acc + indent + "{\"tag\":\"" + attribute.getLocalName() + "\""
                        + ", \"jsonPath\": \"" + attPath + "\""
                        + ", \"pureType\": \"attribute\""
                        + ", \"input\": \"" + attribute.getTextContent() + "\""
                        + ", \"type\": \"" + getType(attribute.getLocalName(), attPath) + "\""
                        + ", \"order\": -1"
                        + ", \"expanded\": false"
                        + ", \"minCard\": " + getminCardSimml(attribute.getLocalName(), elem.getLocalName())
                        + ", \"maxCard\": " + getmaxCardSimml(attribute.getLocalName(), elem.getLocalName())
                        + ", \"leaf\": true"
                        + ", \"attribute\": true"
                        + "}, ";
            }
        }
        if (acc.length() > 0) {
            acc = acc.substring(0, acc.lastIndexOf(","));
            w.write(acc);
        }
        return w;
    }
    
    /**
     * Gets the pure type for the tag name from the struct.
     * @param tagName   The element
     * @param path      The json path
     * @return          The type
     */
    private String getPureType(String tagName, String path) {
        String cleanPath = path.replaceAll("\\[[0-9]*\\]", "");
        Iterator<JsonElement> it = struct.iterator();
        while (it.hasNext()) {
            JsonObject el = it.next().getAsJsonObject();

            if (((path.contains("sml:") && el.get("jsonPath").getAsString().contains("sml:"))
                    || el.get("jsonPath").getAsString().equals(cleanPath)) && el.get("tag").getAsString().equals(tagName)) {
                if (el.has("pureType")) {
                    return el.get("pureType").getAsString();
                }
                else {
                    return "none";
                }
            }
        }
        return "none";
    }
    
    /**
     * Gets the type for the tag name from the struct.
     * @param tagName   The element
     * @param path      The json path
     * @return          The type
     */
    private String getType(String tagName, String path) {
        String cleanPath = path.replaceAll("\\[[0-9]*\\]", "");
        Iterator<JsonElement> it = struct.iterator();
        while (it.hasNext()) {
            JsonObject el = it.next().getAsJsonObject();

            if ((path.contains("sml:") || el.get("jsonPath").getAsString().equals(cleanPath)) && el.get("tag").getAsString().equals(tagName)) {
                return el.get("type").getAsString();
            }
        }
        return "string";
    }
    
    /**
     * Parse a simML element.
     * @param elem          The element
     * @param indent        The current indent
     * @param inputValue    The value of the element
     * @param absPath       The xpath
     * @return              The json object
     */
    private StringWriter printSimML(Element elem, String indent, String inputValue, String absPath) {
        //writer        
        StringWriter w = new StringWriter();
        
        w.write(indent + "{\"tag\":\"" + elem.getLocalName() + "\"");
        w.write(", \"jsonPath\": \"" + absPath + "\"");
        w.write(", \"order\": " + getOrderSimml(elem.getLocalName(), elem.getParentNode().getLocalName())); 
        if (elem.getLocalName().equals("math")) {
            w.write(", \"type\": \"mathML\"");
            w.write(", \"pureType\": \"mathML\"");
            w.write(", \"leaf\": true");
        }
        else {
            w.write(", \"type\": \"" + getType(elem.getLocalName(), absPath) + "\"");
            w.write(", \"pureType\": \"" + getPureType(elem.getLocalName(), absPath) + "\"");
        }
        if (!inputValue.equals("")) {
            w.write(", \"input\": \"" + inputValue.trim().replaceAll("\"", "\\\\\"") + "\"");
        }
        else {
            w.write(", \"input\": null");
        }
        w.write(", \"minCard\": " + getminCardSimml(elem.getLocalName(), elem.getParentNode().getLocalName()));
        w.write(", \"maxCard\": " + getmaxCardSimml(elem.getLocalName(), elem.getParentNode().getLocalName()));
        if (!elem.getLocalName().equals("math") && inputValue.equals("")) {
            w.write(printChildrenSimML(elem, indent, absPath).toString());
        }
        w.write("}");
        return w;
    }
    
    /**
     * Gets the order from the element name in the parent context.
     * @param tagName       The name
     * @param parentTag     The parent tag
     * @return              The order of the element.
     */
    private int getOrderSimml(String tagName, String parentTag) {
        Iterator<JsonElement> it = struct.iterator();
        while (it.hasNext()) {
            JsonObject el = it.next().getAsJsonObject();
            if (el.get("tag").getAsString().equals(parentTag)) {
                Iterator<JsonElement> childrenIt = el.getAsJsonArray("children").iterator();
                while (childrenIt.hasNext()) {
                    el = childrenIt.next().getAsJsonObject();
                    if (el.get("tag").getAsString().equals(tagName)) {
                        if (el.has("order")) {
                            return el.get("order").getAsInt();
                        }
                        else {
                            return -1;
                        }
                    }
                }
            }
        }
        return -1;
    }
    
    /**
     * Gets the minimum occurrences of the element name in the parent context.
     * @param tagName       The name
     * @param parentTag     The parent tag
     * @return              The cardinality of the element.
     */
    private int getminCardSimml(String tagName, String parentTag) {
        Iterator<JsonElement> it = struct.iterator();
        while (it.hasNext()) {
            JsonObject el = it.next().getAsJsonObject();
            if (el.get("tag").getAsString().equals(parentTag)) {
                Iterator<JsonElement> childrenIt = el.getAsJsonArray("children").iterator();
                while (childrenIt.hasNext()) {
                    el = childrenIt.next().getAsJsonObject();
                    if (el.get("tag").getAsString().equals(tagName)) {
                        if (el.has("minCard")) {
                            return el.get("minCard").getAsInt();
                        }
                        else {
                            return 1;
                        }
                    }
                }
            }
        }
        return 1;
    }
    
    /**
     * Gets the maximum occurrences of the element name in the parent context.
     * @param tagName       The name
     * @param parentTag     The parent tag
     * @return              The cardinality of the element.
     */
    private int getmaxCardSimml(String tagName, String parentTag) {
        Iterator<JsonElement> it = struct.iterator();
        while (it.hasNext()) {
            JsonObject el = it.next().getAsJsonObject();
            if (el.get("tag").getAsString().equals(parentTag)) {
                Iterator<JsonElement> childrenIt = el.getAsJsonArray("children").iterator();
                while (childrenIt.hasNext()) {
                    el = childrenIt.next().getAsJsonObject();
                    if (el.get("tag").getAsString().equals(tagName)) {
                        if (el.has("maxCard")) {
                            return el.get("maxCard").getAsInt();
                        }
                        else {
                            return 1;
                        }
                    }
                }
            }
        }
        return 1;
    }
    
    /**
     * Parses the xsd with an xml data document to json.
     * @param document      The document
     * @return              The json object
     * @throws SUException  MM008 Schematron not valid
     *                       MM00X External error
     */
    public String xsomNavigate(String document) throws SUException {
        recursionStack = new ArrayList<String>();
        try {
            String absPath = "";
            String indent  = "\t";
            StringWriter w = new StringWriter();
            String content = preprocess(document);
            doc = Utils.stringToDom(content);
            w.write("{\"text\":\".\",\"children\": [");
            w.write(printElements(parser.getResult().getSchema(1).getElementDecl(rootElement), absPath, indent, 1, 1, 0, null).toString());
            w.write("\n");
            w.write("]}");
            return w.toString();
        }
        catch (SAXException e) {
            e.printStackTrace(System.out);
            throw new SUException(SUException.SU00X, e.toString());
        }
    }
    
    /**
     * Parses the xsd with no data.
     * @return  The json object
     * @throws SUException  MM008 Schematron not valid
     *                       MM00X External error
     */
    public String xsomNavigate() throws SUException {
        String absPath = "";
        String indent  = "\t";
        recursionStack = new ArrayList<String>();
        try {
            //writer
            StringWriter w = new StringWriter();
            //Empty document
            DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = builderFactory.newDocumentBuilder();
            doc = builder.newDocument();
            
            w.write("{\"text\":\".\",\"children\": [");
            w.write(printElements(parser.getResult().getSchema(1).getElementDecl(rootElement), absPath, indent, 1, 1, 0, null).toString());
            w.write("\n");
            w.write("]}");
            return w.toString();
        }
        catch (SAXException e) {
            e.printStackTrace(System.out);
            throw new SUException(SUException.SU00X, e.toString());
        }
        catch (ParserConfigurationException e) {
            e.printStackTrace(System.out);
            throw new SUException(SUException.SU00X, e.toString());
        }
    }

    /**
     * Preprocess the xml document with the following procedures.
     * 1 - Remove whitespaces
     * 2 - Encapsulate mathML data in CDATA objects
     * 3 - Add sml namespace to the root element
     * @param problem   The document
     * @return          The document preprocessed
     */
    static String preprocess(String problem) {
        
        //1 - Remove whitespaces
        String tmp = problem.replaceAll(">(\\s+|\\n|\\r\\n)<", "><").replaceAll("\n\r", "").replaceAll("\n", "");
        tmp = tmp.replaceAll("(\\s+|\\n|\\r\\n)", " ");
        tmp = tmp.replaceAll("\\s+>", ">");
  
        //2 - Encapsulate mathML data in CDATA objects
        String patternString = "<mt:math((\\s+(\\w+:)?\\w+=\"[\\w:/.]+\")?)*\\s*>";
        Pattern pattern = Pattern.compile(patternString);
        Matcher matcher = pattern.matcher(tmp);
        StringBuffer sb = new StringBuffer();
        while (matcher.find()) {
            //By-pass the attributes from math
            String attributes = "";
            String patternString2 = "(\\S+)=[\"']?((?:.(?![\"']?\\s+(?:\\S+)=|[>\"']))+.)[\"']?";
            Pattern pattern2 = Pattern.compile(patternString2);
            Matcher matcher2 = pattern2.matcher(matcher.group(0));
            while (matcher2.find()) {
                if (!matcher2.group(1).startsWith("xmlns")) {
                    attributes = attributes + " " + matcher2.group(0);
                }
            }
            String newString = "<mt:math xmlns:mt=\"http://www.w3.org/1998/Math/MathML\"><![CDATA[<mt:math" + attributes + ">";
            matcher.appendReplacement(sb, newString);
        }
        matcher.appendTail(sb);
                
        patternString = "</mt:math>";
        pattern = Pattern.compile(patternString);
        matcher = pattern.matcher(sb.toString());
        sb = new StringBuffer();
        while (matcher.find()) {
            String newString = "</mt:math>]]></mt:math>";
            matcher.appendReplacement(sb, newString);
        }
        matcher.appendTail(sb);
                
        //3 - Add sml namespace to the root element
        patternString = "(<\\w+:\\w+(\\s+(\\w+:)?\\w+=\"[\\w:/.]+\"?)*)>";
        pattern = Pattern.compile(patternString);
        matcher = pattern.matcher(sb.toString());
        sb = new StringBuffer();
        if (matcher.find() && !matcher.group(1).contains("simml")) {
            String newString = matcher.group(1) + " xmlns:sml=\"urn:simml\">";
            matcher.appendReplacement(sb, newString);
        }
        
        matcher.appendTail(sb);

        return sb.toString();
    }
}
