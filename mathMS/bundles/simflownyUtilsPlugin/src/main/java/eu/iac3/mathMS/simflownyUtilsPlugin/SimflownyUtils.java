/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.simflownyUtilsPlugin;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.UnsupportedEncodingException;

import javax.jws.WebService;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;

/**
 * Interface for utilities.
 * @author bminano
 *
 */
@WebService
public interface SimflownyUtils {

    /**
     * Converts an asciiMathML equation to MathML content.
     * @param infix             The equation
     * @return                  The mathML 
     * @throws SUException      SU006    Incorrect asciiMath expression
     */
    static String infixToMathML(String infix) throws SUException {
        try {
            DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document asciiMathMLDocument = documentBuilder.parse(new ByteArrayInputStream(infix.getBytes("UTF-8")));
            String cMathMLDocument = SimflownyUtilsImpl.infixToMathML(asciiMathMLDocument);
            return Utils.removeAllXmlNamespace(cMathMLDocument);
        } 
        catch (Exception e) {
            e.printStackTrace(System.out);
            throw new SUException(SUException.SU006, e.getMessage());
        }
    }
    
    /**
     * Converts a json document to YAML
     * @param json	input
     * @return		output
     * @throws SUException 
     */
    static byte[] xmlToYAML(String json) throws SUException {
    	try {
			return YAMLUtils.fromXML(jsonToXML(json)).getBytes("UTF-8");
		} catch (UnsupportedEncodingException | SUException e) {
			e.printStackTrace(System.out);
            throw new SUException(SUException.SU00X, e.getMessage());
		}
    }
    
    /**
     * Converts an YAML document to XML
     * @param yaml	input
     * @return		output
     * @throws SUException 
     */
    static String yamlToXML(String yaml) throws SUException {
    	return YAMLUtils.toXML(yaml);
    }
    
    /**
     * Converts an xml document to JSON.
     * @param xml               The xml document of schemaName
     * @param empty             True if the document is empty
     * @return                  The JSON file 
     * @throws SUException      SU001    Not a valid Document
     *                           SU002    Schematron problem
     *                           SU004    XSD schema does not exist
     */
    static String xmlToJSON(String xml, boolean empty) throws SUException {
        String schemaName;
        if (empty) {
            schemaName = xml;
        }
        else {
            schemaName = Utils.stringToDom(xml).getDocumentElement().getLocalName();
        }
        String schemaPath = "common" + File.separator + "XSD" + File.separator + schemaName + ".xsd";
        String schematronPath = "common" + File.separator + "schematron" + File.separator + schemaName + ".sch";
        XMLJSONConverter nav = new XMLJSONConverter(schemaPath, schematronPath, schemaName);
        if (empty) {
            return nav.xsomNavigate();
        }
        else {
            return nav.xsomNavigate(xml);
        }
    }
    
    /**
     * Converts an json document to XML.
     * @param json              The json document
     * @return                  The XML file 
     * @throws SUException      SU001    Not a valid Document
     *                           SU002    Schematron problem
     */
    static String jsonToXML(String json) throws SUException {
        JSONXMLConverter jsonNav = new JSONXMLConverter(json);
        return Utils.domToString(jsonNav.toXML());
    }
    
    /** 
     * It returns the struct in json for the model/problem specified.
     * @param xsdName       The schema to obtain the struct from
     * @return              The json struct file
     * @throws SUException   SU005    Setup error in the struct generator
     */
    static String getStruct(String xsdName) throws SUException {
        String schemaPath = "common" + File.separator + "XSD" + File.separator + xsdName + ".xsd";
        String schematronPath = "common" + File.separator + "schematron" + File.separator + xsdName + ".sch";
        
        XSDTagStructureGenerator tagStruct = new XSDTagStructureGenerator();
        tagStruct.addStruct(schemaPath, schematronPath, xsdName);
        return tagStruct.getStruct();
    }
}

