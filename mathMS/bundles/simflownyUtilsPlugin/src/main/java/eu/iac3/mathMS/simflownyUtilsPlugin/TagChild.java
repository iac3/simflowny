/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.simflownyUtilsPlugin;

/**
 * Class representing the children of a json struct element.
 * @author bminano
 *
 */
public class TagChild {
    String tag;
    String jsonPath;
    int minCard;
    int maxCard;
    String type;
    String pureType;
    boolean choice;
    boolean leaf;
    boolean attribute;
    boolean expanded;
    int order;
    String recursive;
    /**
     * Constructor.
     * @param name          The name
     * @param jsonPath      The jsonPath
     * @param minCard       The min cardinality
     * @param maxCard       The max cardinality
     * @param order         The order
     * @param expanded      expanded
     * @param type          type
     * @param pureType      pureType
     * @param leaf          leaf
     * @param attribute     attribute
     * @param choice		choice
     * @param recursive		recursive
     */
    public TagChild(String name, String jsonPath, int minCard, int maxCard, int order, String type, String pureType, 
            boolean leaf, boolean attribute, boolean expanded, boolean choice, String recursive) {
        this.tag = name; 
        this.jsonPath = jsonPath;
        this.minCard = minCard;
        this.maxCard = maxCard;
        this.order = order;
        this.type = type;
        this.pureType = pureType;
        this.leaf = leaf;
        this.attribute = attribute;
        this.expanded = expanded;
        this.choice = choice;
        this.recursive = recursive;
    }
    
    @Override
    public boolean equals(Object object) {
        if (object instanceof TagChild) {
            TagChild comp = (TagChild) object;
            return this.tag.equals(comp.tag);
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        return tag.hashCode();
    }
}
