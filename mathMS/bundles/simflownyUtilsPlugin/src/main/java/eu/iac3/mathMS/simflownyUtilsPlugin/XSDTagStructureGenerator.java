/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny-copyright-and-license
*/
package eu.iac3.mathMS.simflownyUtilsPlugin;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sun.xml.xsom.XSAnnotation;
import com.sun.xml.xsom.XSAttributeDecl;
import com.sun.xml.xsom.XSAttributeUse;
import com.sun.xml.xsom.XSComplexType;
import com.sun.xml.xsom.XSElementDecl;
import com.sun.xml.xsom.XSModelGroup;
import com.sun.xml.xsom.XSModelGroupDecl;
import com.sun.xml.xsom.XSParticle;
import com.sun.xml.xsom.XSSimpleType;
import com.sun.xml.xsom.XSTerm;
import com.sun.xml.xsom.parser.XSOMParser;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashSet;

import javax.xml.parsers.SAXParserFactory;

import org.w3c.dom.Document;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

/**
 * Class that creates the structs from XSD.
 * @author bminano
 *
 */
class XSDTagStructureGenerator {
    LinkedHashSet<TagStruct> struct;
    private XSOMParser parser;
    private Document schematron;
    private ArrayList<String> recursionStack;
    
    /**
     * Constructor.
     */
    XSDTagStructureGenerator() {
        struct = new LinkedHashSet<TagStruct>();
    }
    
    /**
     * Error handler.
     * @author bminano
     *
     */
    class MyErrorHandler implements ErrorHandler {
        /**
         * Warning.
         * @param e             Error
         * @throws SAXException Exception
         */
        public void warning(SAXParseException e) throws SAXException {
            show("Warning", e);
            throw (e);
        }

        /**
         * Error.
         * @param e             Error
         * @throws SAXException Exception
         */
        public void error(SAXParseException e) throws SAXException {
            show("Error", e);
            throw (e);
        }

        /**
         * Fatal error.
         * @param e             Error
         * @throws SAXException Exception
         */
        public void fatalError(SAXParseException e) throws SAXException {
            show("Fatal Error", e);
            throw (e);
        }

        /**
         * Show the error.
         * @param type          Error type
         * @param e             Error
         */
        private void show(String type, SAXParseException e) {
            System.out.println(type + ": " + e.getMessage());
            System.out.println("Line " + e.getLineNumber() + " Column " + e.getColumnNumber());
            System.out.println("System ID: " + e.getSystemId());
        }
    }
    
    /**
     * Adds and parse a new schema to the struct list. 
     * @param xsdFile           The schema
     * @param schematronFile    The schematron with restrictions
     * @param rootElement       The root element of the schema
     * @throws SUException      SU005    Setup error in the struct generator
     */
    public void addStruct(String xsdFile, String schematronFile, String rootElement) throws SUException {
        try {
            recursionStack = new ArrayList<String>();
            SAXParserFactory factory = SAXParserFactory.newInstance();
            parser = new XSOMParser(factory);
            parser.setAnnotationParser(new XSOMAnnotationParser());
            parser.setErrorHandler(new MyErrorHandler());
            parser.parse(xsdFile);
            if (!schematronFile.equals("")) {
                String content = Utils.readFile(schematronFile);
                schematron = Utils.stringToDom(content);
            }
            else {
                schematron = null;
            }
            if (parser.getResult().getSchema(1).getElementDecl(rootElement) == null) {
                throw new SUException(SUException.SU005, "Element " + rootElement + " does not exist in the schema " + xsdFile); 
            }
            printElement(parser.getResult().getSchema(1).getElementDecl(rootElement), "", 1, 1, 0, true, false, null);
        }
        catch (SAXException e) {
            throw new SUException(SUException.SU005, e.toString());
        }
    }
    
    /**
     * Parse the particle element. 
     * @param particle          The particle
     * @param attributes        The attributes of the parent element
     * @param absPath           The xpath of the element
     * @param order             The order of the element
     * @param parentOrdered     True if the children must maintain the order
     * @param parentNamespace   The namespace of the parent element
     * @param isChoice          If the element is a child of a choice
     * @param recursive			
     * @return                  The Struct
     * @throws SUException      MM008 Schematron not valid
     *                           MM00X External error
     */
    private LinkedHashSet<TagChild> printParticle(XSParticle particle, Collection< ? extends XSAttributeUse> attributes, 
            String absPath, int order, boolean parentOrdered, String parentNamespace, boolean isChoice, String recursive) throws SUException {
        LinkedHashSet<TagChild> children = new LinkedHashSet<TagChild>();
        //writer
        int min = particle.getMinOccurs().intValue();
        if (isChoice) {
            min = 0;
        }
        int max = particle.getMaxOccurs().intValue();
        XSTerm term = particle.getTerm();
        if (term.isModelGroup()) {
            return printGroup(term.asModelGroup(), attributes, absPath, parentOrdered, parentNamespace, isChoice, recursive);
        }
        else if (term.isModelGroupDecl()) {
            return printGroupDecl(term.asModelGroupDecl(), attributes, absPath, parentOrdered, parentNamespace, isChoice, recursive);
        }
        else if (term.isElementDecl()) {
            TagChild child = printElement(term.asElementDecl(), absPath, min, max, order, parentOrdered, isChoice, recursive);
            children.add(child);
        }
        return children;
    }
    
    /**
     * Parse the group. 
     * @param modelGroup        The group
     * @param attributes        The attributes of the parent element
     * @param absPath           The xpath of the element
     * @param parentOrdered     True if the children must maintain the order
     * @param parentNamespace   The namespace of the parent element
     * @param isChoice          If the element is a child of a choice
     * @param recursive			It the element is recursive
     * @return                  The Struct
     * @throws SUException      MM008 Schematron not valid
     *                           MM00X External error
     */
    private LinkedHashSet<TagChild> printGroup(XSModelGroup modelGroup, Collection< ? extends XSAttributeUse> attributes, 
            String absPath, boolean parentOrdered, String parentNamespace, boolean isChoice, String recursive) throws SUException {
        boolean choice = modelGroup.getCompositor().toString().equals("choice") || isChoice;
        LinkedHashSet<TagChild> children = new LinkedHashSet<TagChild>();
        //Print attributes
        LinkedHashSet<TagChild> attributeChildren = printAttributes(attributes, absPath, parentNamespace);
        if (attributeChildren.size() > 0) {
            children.addAll(attributeChildren);
        }
        XSParticle[] list = modelGroup.getChildren();
        for (int i = 0; i < list.length; i++) {
            XSParticle particle = list[i];
            children.addAll(printParticle(particle, attributes, absPath, i, parentOrdered, parentNamespace, choice, recursive));
        }
        return children;
    }
    
    /**
     * Parses the attributes of an element.
     * @param attributes        The attributes
     * @param absPath           The path of the element
     * @param parentNamespace   The namespace of the parent element
     * @return                  The struct
     * @throws SUException      SU008 Schematron not valid
     *                           SU00X External error
     */
    private LinkedHashSet<TagChild> printAttributes(Collection< ? extends XSAttributeUse> attributes, String absPath, String parentNamespace)
        throws SUException {
        LinkedHashSet<TagChild> children = new LinkedHashSet<TagChild>();
        Iterator< ? extends XSAttributeUse> i = attributes.iterator();
        while (i.hasNext()) {
            XSAttributeUse attUse = i.next();
            XSAttributeDecl attributeDecl = attUse.getDecl();
            XSSimpleType xsAttributeType = attributeDecl.getType();
  
            String attPath = absPath + "/mms:" + attributeDecl.getName() + "[1]";
            int min = 0;
            if (attUse.isRequired()) {
                min = 1;
            }
            int max = 1;
            String type;
            String restriction = SimflownyUtilsImpl.printSimpleType(xsAttributeType.asSimpleType());
            String schematronRestriction = SimflownyUtilsImpl.schematronRestriction(schematron, attPath);
            if (restriction.equals("")) {
                type = xsAttributeType.getName() + schematronRestriction;
            }
            else {
                type = xsAttributeType.getBaseType().getName() + restriction + schematronRestriction;
            }
            boolean ordered = true;
            String pureType = "attribute";
            if (parentNamespace.equals("urn:simml")) {
                attPath = "/sml:" + attributeDecl.getName();
            }
            TagStruct elem = new TagStruct(attributeDecl.getName(), attPath, type, pureType, ordered, -1);
            if (!struct.contains(elem)) {
                struct.add(elem);
            }
            children.add(new TagChild(attributeDecl.getName(), attPath, min, max, -1, type, pureType, true, true, false, false, null));
        }
        return children;
    }
       
    /**
     * Parse the group declaration. 
     * @param modelGroupDecl    The group declaration
     * @param attributes        The attributes of the parent element
     * @param absPath           The xpath of the element
     * @param parentOrdered     True if the children must maintain the order
     * @param parentNamespace   The namespace of the parent element
     * @param isChoice          If the element is a child of a choice
     * @param recursive			It the element is recursive
     * @return                  The Struct
     * @throws SUException      MM008 Schematron not valid
     *                           MM00X External error
     */
    private LinkedHashSet<TagChild> printGroupDecl(XSModelGroupDecl modelGroupDecl, Collection< ? extends XSAttributeUse> attributes, 
            String absPath, boolean parentOrdered, String parentNamespace, boolean isChoice, String recursive) throws SUException {
        return printGroup(modelGroupDecl.getModelGroup(), attributes, absPath, parentOrdered, parentNamespace, isChoice, recursive);
    }
    
    /**
     * Parse the complex type.
     * @param complexType       The complex type
     * @param attributes        The attributes of the parent element
     * @param absPath           The xpath of the element
     * @param parentOrdered     True if the children must maintain the order
     * @param parentNamespace   The namespace of the parent element
     * @param order             The element order
     * @param recursive			It the element is recursive
     * @return                  The Struct
     * @throws SUException      MM008 Schematron not valid
     *                           MM00X External error
     */
    private LinkedHashSet<TagChild> printComplexType(XSComplexType complexType, Collection< ? extends XSAttributeUse> attributes, 
            String absPath, boolean parentOrdered, int order, String parentNamespace, String recursive) throws SUException {
        XSParticle particle = complexType.getContentType().asParticle();
        if (particle != null) {
            return printParticle(particle, attributes, absPath, order, parentOrdered, parentNamespace, false, recursive);
        }
        else {
            //Print attributes if the element has no children
            if (!attributes.isEmpty()) {
                LinkedHashSet<TagChild> children = new LinkedHashSet<TagChild>();
                //Print attributes
                LinkedHashSet<TagChild> attributeChildren = printAttributes(attributes, absPath, parentNamespace);
                if (attributeChildren.size() > 0) {
                    children.addAll(attributeChildren);
                }
                return children;
            }
        }
        return new LinkedHashSet<TagChild>();
    }

    /**
     * Parses the element.
     * @param element       The element
     * @param absPath       The xpath of the parent element
     * @param min           The min cardinality
     * @param max           The max cardinality
     * @param parentOrdered True if the children must maintain the order
     * @param order         The element order
     * @param choice      	If the element is a child of a choice
     * @param recursiveTag	It the element is recursive
     * @return              The struct
     * @throws SUException  MM008 Schematron not valid
     *                       MM00X External error
     */
    private TagChild printElement(XSElementDecl element, String absPath, int min, int max, int order, boolean parentOrdered, boolean choice, String recursiveTag) throws SUException {
        absPath += "/mms:" + element.getName();
    //    System.out.println(absPath + " " + choice);
        String type = "";
        boolean expanded = false;
        boolean leaf = false;
        boolean ordered = true;
        LinkedHashSet<TagChild> children = new LinkedHashSet<TagChild>();

        if (!parentOrdered) {
            order = -1;
        }
        //Pure type for visualization from annotation
        XSAnnotation annotation = element.getAnnotation();
        boolean uneditable = false;
        String pureType = "none";
        if (annotation != null) {
            String[] annotations = (String[]) annotation.getAnnotation();
            for (int i = 0; i < annotations.length; i++) {
                if (annotations[i].equals("uneditable")) {
                    uneditable = true;
                }
                else {
                    pureType = annotations[i];
                }
            }
        }
        if (!element.getTargetNamespace().equals("http://www.w3.org/1998/Math/MathML")) {
            if (element.getType().isComplexType()) {
                if (element.getTargetNamespace().equals("urn:simml")) {
                    type = "instructionSet";
                    absPath = "/sml:" + element.getName();
                }
                if (absPath.lastIndexOf("/") == 0) {
                    expanded = true;
                }
                if (SimflownyUtilsImpl.isNotOrdered(element.getType().asComplexType().getContentType().asParticle())) {
                    ordered = false;   
                }
            }
            else {
                if (element.getTargetNamespace().equals("urn:simml")) {
                    absPath = "/sml:" + element.getName();
                }
                //Also print if there are restrictions
                String restriction = SimflownyUtilsImpl.printSimpleType(element.getType().asSimpleType());
                String schematronRestriction = SimflownyUtilsImpl.schematronRestriction(schematron, absPath);
                if (restriction.equals("")) {
                    type = element.getType().getName() + schematronRestriction;
                }
                else {
                    type = element.getType().getBaseType().getName() + restriction + schematronRestriction;
                }
                leaf = true;
            }
            if (uneditable) {
                type = "uneditable";
            }
            boolean recursive = false;
            TagStruct elem = new TagStruct(element.getName(), absPath, type, pureType, ordered, order);
            if (!struct.contains(elem)) {
                struct.add(elem);
                if (element.getType().isComplexType()) {
                    //Checking recursion before accessing the children.
                    String complexTypeName = element.getType().asComplexType().getName();
                    if (complexTypeName != null && !recursionStack.contains(complexTypeName)) {
                        recursionStack.add(complexTypeName);
                    }
                    else if (complexTypeName != null && recursionStack.contains(complexTypeName)) {
                        recursive = true;
                        elem.setRecursive(element.getName());
                        recursiveTag = element.getName();
                    }
                    if (!recursive) {
                        children = printComplexType(element.getType().asComplexType(), element.getType().asComplexType().getAttributeUses(), 
                                absPath, ordered, order, element.getTargetNamespace(), recursiveTag);
                    }
                    if (complexTypeName != null && !recursive) {
                        recursionStack.remove(complexTypeName);
                    }
                }
                elem.addChildren(children);
                if (recursive) {
                    struct.remove(elem);
                }
            }
            return new TagChild(element.getName(), absPath, min, max, order, type, pureType, leaf, false, expanded, choice, recursiveTag);
        }
        else {
            TagStruct elem = new TagStruct("math", "/mt:mathML", "mathML", "mathML", ordered, order);
            if (!struct.contains(elem)) {
                struct.add(elem);
            }
            return new TagChild("math", "/mt:mathML", min, max, order, "mathML", "mathML", true, false, false, choice, null);
        }

    }
    
    /**
     * Converts struct to JSON.
     * @return  Json String
     */
    public String getStruct() {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(struct);
    }
    
}
